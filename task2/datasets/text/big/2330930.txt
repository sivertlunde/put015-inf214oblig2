The Road to Hong Kong
{{Infobox film
| name           = The Road to Hong Kong
| image          = RoadToHongKong 1962.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Norman Panama
| producer       = Melvin Frank
| writer         = {{Plainlist|
* Norman Panama
* Melvin Frank
}}
| starring       = {{Plainlist|
* Bing Crosby
* Bob Hope
* Joan Collins
}}
| music          = Robert Farnon
| cinematography = Jack Hildyard
| editing        = Alan Osbigton 
| studio         = Melnor Films MGM (2003, DVD)
| released       = March 1962, UK
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

The Road to Hong Kong is a 1962 British comedy film directed by Norman Panama and starring Bing Crosby, Bob Hope, as well as Joan Collins, with a cameo featuring Dorothy Lamour.    This was the last in the long-running Road to … series and the only one not produced by Paramount Pictures, though references to the others in the series are made in the film and shown in Maurice Binders opening title sequence.

==Plot== flashback as Diane (Joan Collins) explains to American Intelligence how transmissions from passengers picked up from a missile to the moon are by Americans rather than Russians.

Harry Turner and Chester Babcock are defrauding people in Calcutta by selling a "Do-it-yourself interplanetary flight kit" that ends up injuring Chester, giving him amnesia. An Indian doctor (Peter Sellers) says the only way for Chesters amnesia to be cured is through help from monks in a lamasery in Tibet.

At the airport, Chester mistakenly picks up a suitcase with a marking designed to be a point of contact between agents of a SPECTRE-type spy organization called "The Third Echelon."  Diane, a Third Echelon secret agent, is supposed to give plans of a Russian rocket fuel stolen by the Third Echelon to the man with the suitcase, who will be taking them to headquarters in Hong Kong. She mistakenly thinks Chester is the contact.

In Tibet, the two make their way to the lamasary in Lost Horizon fashion. Not only do the lamas cure Chester, but they have a Tibetan tea leaf that gives super memory powers to those who consume it.  Chester and Harry observe as great works of Western literature in the manner of Fahrenheit 451 are committed to memory, one giggling lama (David Niven) memorizes Lady Chatterleys Lover. The scheming Harry decides to steal a bottle to give Chester the power of photographic memory for lucrative nefarious purposes.

Returning to Calcutta, followed by Diane, Harry has Chester test the results of the memory herb by memorizing the rocket formula that Diane placed in Chesters coat.  Not knowing what it is, Harry destroys it after Chester has successfully memorized it.  Diane arrives too late, but after seeing Chester recite the formula, she offers them $25,000 to meet her in Hong Kong. On the way to Hong Kong, an agent of the High Lama replaces the stolen Tibetan herbs with a similar bottle containing ordinary tea leaves.

The Third Echelon is seeking the fuel for its own spacecraft with an underwater launching pad in Hong Kong. The goal is to be the first on the moon, where a base is to be established to launch nuclear weapons against Earth and to bring survivors under the agencys control.

With a Russian launch to the moon carrying two apes imminent, the Third Echelon, which was going to emulate the Soviet achievement, decides to gain respect at the United Nations by launching two human astronauts, Chester and Harry, instead of apes.  The two are used as guinea pigs (and fed with bananas) to test the capabilities of the spacecraft and the effects of spaceflight upon humans. The mission is successful, with moonlight bringing back Chesters photographic memory.

Diane decides to leave the Third Echelon when she discovers that once her colleagues have extracted the final formula from Chester, they plan to dissect Chester and Harry to see the effects of space travel on their bodies.  Diane helps the boys escape. They are pursued through Hong Kong, eventually leading Diane to the authorities. Chester and Harry happen to meet Dorothy Lamour at a nightclub where they are recaptured by the Third Echelon.

Chester, Harry and Diane all end up in a rocket bound for another planet. They think theyre alone after landing, but theyre not—Chester calls out, "The Italians!" as they are joined by Frank Sinatra and Dean Martin.

==Cast==
* Bing Crosby as Harry Turner
* Bob Hope as Chester Babcock
* Joan Collins as Diane (3rd Echelon agent)
* Robert Morley as Leader of the 3rd Echelon
* Peter Sellers as Indian physician
* Walter Gotell as Dr. Zorbb (3rd Echelon scientist) Sir Felix Aylmer as Grand Lama
* Alan Gifford as American official
* Michele Mok as Mr. Ahso
* Katya Douglas as 3rd Echelon receptionist
* Roger Delgado as Jhinnah Robert Ayres as American official
* Mei Ling as Ming Toy
* Jacqueline Jones as Blonde at airport
* Yvonne Shima as Poon Soon
* Dorothy Lamour as Herself Bob Simmons as Third Echelon astronaut
* Nosher Powell as Third Echelon astronaut

==Production== Jerry Colonna, Frank Sinatra, as well as Dean Martin. In order to preserve the feel of the 1940s and 50s Road films, the movie was shot in black and white.

Hopes character is named "Chester Babcock," an in-joke because songwriter Jimmy Van Heusen was born Edward Chester Babcock. The lamasery where Hope goes to restore his memory is reused from Black Narcissus.   Peter Sellers appearance as an Indian physician involves extended interplay with Crosby and Hope.
 Bob Simmons as an astronaut. The films art director is another Bond film regular, Syd Cain.

Although the movie features the same kind of antics and gags as previous episodes, with all characters trying their utmost to help each other, the film was not as well-received as its predecessors. Some critics felt that the 59-year old Hope and Crosby couldnt pull off the part credibly at their age and that it was unfair for them to dump their old partner Lamour (with whom they had excellent screen chemistry) for the more youthful Collins. Others thought the decade-long gap since the last Road movie wrecked the momentum of the series and that Peter Sellers came off as more fresh and funny than the aging stars of the film.

In 1977, Sir Lew Grade had planned to reunite Hope, Crosby and Lamour in The Road to the Fountain of Youth that Melville Shavelson had completed the script for,  but Crosby died before production.  Crosby was rumored to have asked the writers for a Monty Python-esque script in order to keep the series fresh for 1970s audiences.

This is the only Road film to have its rights retained by the original producer/distributor (where all the previous films are now at the hands of other companies), although today Metro-Goldwyn-Mayer (UAs sister studio) handles distribution and marketing on behalf of UA.

==Quotes==
* "Thats the plot so far? Id better hide you ... from the critics!" – Dorothy Lamour

==DVD & Blu-ray==
The Road to Hong Kong was released to DVD by MGM Home Video in a Region 1 DVD on April 1, 2003.

The Road to Hong Kong was released to Blu-ray (Region A) by Olive Films on Feb 17, 2015. 

==References==
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 