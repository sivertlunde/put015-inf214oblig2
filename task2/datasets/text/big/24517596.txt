The Wild and Wonderful Whites of West Virginia
{{Infobox film
| name           = The Wild and Wonderful Whites of West Virginia
| image          = The Wild and Wonderful Whites of West Virginia poster.jpg
| director       = Julien Nitzberg
| producer       =   Paige Hess-Hill   Jeffrey Yapp
| starring       = Jesco White
| music          = Deke Dickerson Hank Williams III
| cinematography = 
| editing        =  
| language       = English
| studio         = Dickhouse Productions MTV Studios
| distributor    = Tribeca Film
| released       =  
| runtime        = 88 minutes
}}
The Wild and Wonderful Whites of West Virginia is a 2009 documentary film directed by Julien Nitzberg, chronicling the White Family of Boone County, West Virginia.

==Synopsis==
The film follows the White family over the course of a year in their daily life through first person interviews. The film mentions the details of the death of Donald Ray D. Ray White|"D. Ray" White, the patriarch of the family. It also mentions D. Ray Whites rise to stardom as one of the most famous mountain dancers of his time. Bertie Mae White, D. Rays widow, is also featured; her illness is documented throughout the course of the film. Bertie is considered "The Miracle Woman" by locals due to her lifelong dedication to raising abandoned children. Throughout the film, Bertie is seen supporting her family despite her intolerance of their dangerous and reckless choices. The younger generations of Whites are followed to drug deals, criminal trials, hospital beds, and jail cells to recount the wild and outlandish events in their lives. A group of local professionals in Boone County speak about the Whites, acting as a Greek chorus. Most of them criticize the Whites and their negative influence on the community.  
 coal miners social security checks monthly from the government due to their inability to hold employment from psychiatric disability.  

==The Whites==
Six of D. Ray and Berties thirteen children are featured in the film. 

===D. Ray and Berties Children=== mountain dancer, he was previously the subject of the documentary film The Dancing Outlaw.

*Mamie White – oldest daughter of D. Ray and Bertie; girlfriend of Billy Hastings; she introduces the family at the start of the film. Mamie tells the story of her brother Dorsey White, who was shot in the face during a dispute with neighbors and lost an eye. He later died of an unintentional self-inflicted gunshot wound.  Mamies boyfriend Billy Hastings is not related to the Whites, but he is a central figure in the familys past and present. His involvement in a dispute led to the shooting death of D. Ray White by Steve Roe.  His altercation with Brandon Poe is described in detail in the film.

*Ona Fontaine White - 1951-1971 - daughter of D. Ray and Bertie; murdered by ex-husband Clyde Davis.

*Bo White – daughter of D. Ray and Bertie; mother of Kirk White and Derek Castle.

*Poney White – the only one of D. Ray and Berties children to leave Boone County. He moved to Minneapolis and is a house painter.  Poney states he felt he needed to leave West Virginia to improve his life, a decision he made after a prescription fraud conviction. Despite leaving the public school system in seventh grade, Poney is one of the few employed members of the family. His daughter, Virginia, recounts her inability to obtain employment due to her last name before they relocated. His son, Jerry, rehashes mistreatment from educators in the local school simply due to his lineage.

*Sue Bob White – the youngest of D. Ray and Berties children; she is the mother of Brandon and Ashley Poe. (According to the website, Sue Bob was arrested shortly after filming ended, and has been in jail ever since.) 

===Grandchildren/Cousins===
*Kirk White – daughter of Bo White; and sisters of Derek Castle. Kirks children, Monica and Tylor, are featured in the film. She gives birth to a child during the film, and the child is taken away by Child Protective Services. Kirk checks herself into an alcohol and drug rehab facility in order to regain custody.

*Derek Castle – son of Bo White; brother of Kirk White.

*Brandon Poe – son of Sue Bob White; he is sentenced to 50 years in prison for the attempted murder of Billy Hastings.

*Mousie White – eldest daughter of Mamie White; she is shown being released from prison.

*Terri Lynn White – only daughter of Carly White; shown doing jello shooters at the courthouse.  Shes the tallest of the White clan.

==Critical Response==
The film received mixed-to-positive reviews, garnering a 63% "Fresh" rating on Rotten Tomatoes. 

==Soundtrack==
{{tracklist
 music_credits = yes title1 = Simple Gifts music1 = Greg Herzenach & Al Wolovitch

 title2 = D-Ray White music2 = Hank Williams III

 title3 = Jessico music3 = The Kentucky Headhunters

 title4 = Mama music4 = Deke Dickerson

 title5 = Train to Nowhere music5 = Deke Dickerson

 title6 = Cha Cha Cha-Ching! music6 = Phil Gough

 title7 = Oh Dem Pills music7 = Deke Dickerson
 title8 = Straight to Hell music8 = Hank Williams III

 title9 = Theme of Violence music9 = Deke Dickerson

 title10 = Party at My Pad music10 = Deke Dickerson

 title11 = Happy Birthday music11 = Jesco & Mamie White

 title12 = Lightning When I Need music12 = Five Horse Johnson
 title13 = No Rules music13 = GG Allin
 title14 = Whose Baby Are You, Baby? music14 = Deke Dickerson
 title15 = Sorrow And Pain   music15 = Deke Dickerson
 title16 = West Virginia White Boy music16 = Deke Dickerson
 title17 = Diggin’ It music17 = Deke Dickerson
 title18 = I Love My Job music18 = Deke Dickerson
 title19 = Mountain Lullaby music19 = Benedikt Brydern
 title 20 = Vinum Sabbathi music 20 = Electric Wizard
 title21 = Pine Tree music21 = Ponty’s Camper
 title22 = Long Day music22 = Jay Hill and The Dirty Coal River Band
 title23 = Hook and Line music23 = Ponty’s Camper
 title24 = Darkness Breeds Contempt music24 = Deke Dickerson
 title25 = Fortified Wine music25 = Deke Dickerson
 title26 = Big Fat Woman Blues music26 = Voodoo Whiskey
 title27 = Asphalt Aisle music27 = Deke Dickerson
 title28 = Double Dealin’ Man music28 = Heather Marie Marsden and Phil Gough
 title29 = P.F.F music29 = Hank Williams III
 title30 = Wedding March music30 = Richard Hardelstein
 title31 = Wedding March Recessional music31 = Felix Mendelssohn
 title32 = Plague of Angels music32 = Earth
 title33 = William Morgan music33 = John Haywood
 title34 = Coal Miner’s Daughter music34 = Mamie White
 title35 = Coda Maestoso in F music35 = Earth
 title36 = Wild Wild Party music36 = Charlie Feathers
 title37 = Sick, Sober and Sorry music37 = Lefty Frizzell with Johnny Bond
 title38 = Fugue for Two Guitars and Spoons music38 = Deke Dickerson
 title39 = Moss on the Trees music39 = Deke Dickerson
 title40 = Lonely Holler music40 = Deke Dickerson
 title41 = Sorrow and Light music41 = Deke Dickerson
 title42 = Mama   music42 = Deke Dickerson
 title43 = Big Ass Happy Family music43 = Roger Alan Wade


}}

==See also==
*Jesco White
*D. Ray White

==References==
 

==External links==
* 
* 
* 
 

 
 
 
 
 
 
 
 
 