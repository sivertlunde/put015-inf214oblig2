Chor Machaye Shor
 
{{Infobox film
| name           = Chor Machaye Shor
| image          = 
| image_size     = 
| caption        = 
| director       = Ashok Roy
| producer       = N.N. Sippy
| writer         = S.M. Abbas Dhruva Chatterjee Tarun Ghosh
| narrator       = 
| starring       = Shashi Kapoor Mumtaz (actress)| Mumtaz Asrani Danny Denzongpa
| music          = Ravindra Jain
| cinematography = K.H. Kapadia
| editing        = Waman B. Bhosle Gurudutt Shirali
| studio         = Chandivali Studio Filmalaya Studio Filmistan Studio K. Asif Studios
| distributor    = N.N. Sippy Productions Ultra Distributors
| released       = 18 March 1974
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
 Fakira (1976), which also became a box office hit.
"Le Jayenge Le Jayenge" is a very popular song from the film, which had a key tagline Dilwale Dulhaniya Le Jayenge turned into a later blockbuster film title.  
 Telugu as Bhale Dongalu.

==Plot==

Vijay (Shashi Kapoor) is an engineer, who is in love with a rich girl named Rekha (Mumtaz (actress)|Mumtaz).  Rekhas snobby father disapproves because Vijay is not wealthy, and so he has arranged for her to be married to a son of an evil politician. The evil politician is played by Madan Puri. Rekhas father and the politician frame Vijay for a crime that he didnt commit, and he goes to jail. When hes in jail, he becomes friends with three other prisoners. All four men escape from jail. After Vijay reconnects with Rekha, they all go to a small village called Shantinagar and help save the village from the evil politician and the bandits that terrorize the town. The evil politician is arrested. Rekhas father feels remorse and accepts Vijay. Vijay and his three prisoner friends go back to jail. However, the film ends on an optimistic tone indicating that their good deeds will be rewarded (i.e. that their prison sentences will be shortened).

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Ghungaroo Ki Tarah"
| Kishore Kumar
| 04:45
|-
| 2
| "Ek Daal Par Tota Bole"
| Mohammad Rafi, Lata Mangeshkar
| 05:12
|- 
| 3
| "Le Jayenge Le Jayenge"
| Kishore Kumar, Asha Bhosle
| 03:53
|- 
| 4
| "Yeh Mera Jadoo" 
| Asha Bhosle
| 06:18
|- 
| 5
| "Paon Mein Dori"
| Mohammad Rafi, Asha Bhosle
| 03:55
|- 
| 6
| "Agre Se Ghaghro Manga De"
| Asha Bhosle
| 05:07
|}

==References==
 

== External links ==
*  
 
 
 
 
 