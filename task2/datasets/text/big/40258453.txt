S.I.N. Theory
 
{{Infobox film
| name           = S.I.N. Theory
| image          = S.I.N. Theory Official Poster.jpeg
| alt            =
| director       = Richie Mitchell
| producer       = Richie Mitchell Jeremy Larter Casper Lundbak
| writer         = Richie Mitchell
| starring       = Jeremy Larter Allison Dawn Doiron Farid Yazdani Richard Guppy Kevin Stonefield Stephen Jacob Hogan Ed Lewis 
| music          = Peter Rankin Transits Of Mercury
| editing        = Lachey Wall, Luke Higginson
| distributor    = MCTV/Continuum Motion Pictures
| released       =  
| runtime        = 70 minutes
| country        = Canada
| language       = English
}}

S.I.N. Theory (abbreviation for Social Insurance Number Theory   ) is a 2012 Canadian science fiction drama film about a mathematics professor creating an algorithm capable of predicting an individuals future. The film was written and directed by Richie Mitchell, and stars Jeremy Larter and Allison Dawn Doiron. S.I.N. Theory was produced on a shoestring budget and makes notable use of existing mathematical theories to affirm the concepts plausibility.    The film has been screened at science fiction film festivals in Canada and the US, whereas it has been picked up for distribution by MCTV with an anticipated release Fall 2013.   

== Plot ==
Michael, a mathematics professor having dedicated his career to creating the ultimate game theory, is finally let go for being a long time black mark of the faculty. Despite becoming a laughing stock, Michael continues his work from home and is now free from legal parameters the faculty has enforced. Determined to prove all naysayers wrong, and to reclaim if not heighten his name, Michael gives the go-ahead to his anonymous colleague, a hacker, to obtain access to the populous’ full credit and health report information. This illegal database proves to be the last piece of the puzzle, and with it, Michael is able to accurately calculate and therefore predict the outcome to nearly any situation. As it was initially thought to be of use for stock markets and political predicaments, Michael however curiously formulates the life span of his favorite student, a young woman he loves, and finds out she only has days to live. The further he unravels the possibilities and dangers the algorithm beholds, his morality, its proper use, and reporting of it comes into question. Each time he uses the algorithm, he taps into the credit report database and his illegal presence becomes vulnerable, and ultimately discovered by a competing-dirty corporation, hell bent on the same goal for monetary purposes. Michael must choose what to do with this illegal and very powerful equation; either to publish it for his own glorious demise, or save the woman he loves and risk annihilation by the threatening competitor.

== Cast ==
* Jeremy Larter as Dr. Michael Liemann
* Allison Dawn Doiron as Evelyn Palmer
* Farid Yazdani as David
* Richard Guppy as Sean
* Stephen Jacob Hogan as Thug 1
* Ed Lewis as Thug 2
* Kevin Stonefield as The Dean

== Production ==
In writing the script, a contained sci-fi written explicitly for the accommodation of the budget, Mitchell wished to incorporate existing mathematical theories to showcase the plausibility of such an equation, and how to a certain extent, already exists today. In a viral video interview posted on the films website,  Mitchell talks of drawing comparisons to observing human behavior to the study of fluid behavior, as depicted by the Navier-Stokes Equations, an attribution to Mitchells engineering education.

Having undergone development hell on a couple other of his projects, Mitchell decided to finance the film along with support of close friends and family.  It was shot in Little Italy Toronto, the residing neighborhood of Mitchell, Larter and Doiron. Often the trio would simply lug gear to site, film their scenes quickly and without a permit, and move on, accumulating to a 2 week production schedule plus a summer of weekend pick-ups.   
Since all locations were filmed at no cost, the location of the Deans office proved to be the most difficult. It wasnt until that Larter and Mitchell coincidentally vacationed on Prince Edward Island at the same time were they able to swoop in at an elementary school and film the scene with Kevin Stonefield, a retired school principal and a long time friend of Mitchells whos been in several of Mitchells short films.

An earlier version of the film was screened at the 2013 Boston Science Fiction Film Festival. After a year since post production began, Mitchell hired a new editor Luke Higgenson to perform a final edit to strengthen pacing and also to add a fresh voice to the project.  Upon completion of the new and final edit, the film was picked up for distribution by MCTV (Continuum Motion Pictures) and is intended to be released on VOD, Dish Channel and various platforms Fall 2013.   
== Release ==
{| class="wikitable"
|-
! Festival !! City !! Year
|-
| Boston Science Fiction Film Festival || Boston, USA, || 2013
|-
| Philip K. Dyck Science Fiction Film Festival || New York City, USA, || 2012
|-
| Moving Image Film Festival ||Toronto, Canada, || 2012
|}

== Reception ==

Rogue cinema has called it "one of the most compelling indie flicks of the year"   while others such as Filmmaking Review have applauded the directors work given the budgetary restraints.   

== References ==
 

== External links ==
*  
*  
* http://www.peifilmandmedia.com/s-i-n-theory
* http://www.quietearth.us/articles/2012/01/SIN-Theory-is-an-algorithm-to-predict-your-future-trailer



 
 