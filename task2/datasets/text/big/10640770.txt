Siva (1989 Tamil film)
{{Infobox film|
| name = Siva
| image = Siva Rajini.jpg
| caption = Poster
| director = Amirjan
| writer = K. Kannan
| screenplay = Ananthu
| story = Rakesh Kumar Madhuri
| producer = Rajam Balachander Pushpa Kandaswamy
| music = Ilaiyaraaja
| cinematography = C. S. Ravibabu
| editing = S. S. Nazir
| studio = Kavithalayaa Productions
| distributor = Kavithalayaa Productions
| released = 5 May 1989
| runtime = 157 mins
| country =   India Tamil
| budget =
}}

Siva is a 1989 Tamil film directed by Amirjan. The film features Rajinikanth, Raghuvaran, Sowcar Janaki and Shobana in lead roles. The film is a remake of the 1977 film Khoon Pasina, which starred Amitabh Bachchan. The film was block buster. 
==Plot==
Rajinikanth and Raghuvaran are close friends like their fathers, in spite of their difference in caste Hindu and Christian. But a villain kills family except Rajinikanth, Raghuvaran and  Raghuvarans mother, one twist is  Raghuvaran does not knows that Rajinikanth and his mother was alive and vice versa. Years run, film opens after 20 years. Rajinikanth falls in love with Shobana. He marries her. Then Raghuvaran is a hired goon will do only good deeds. Raghuvaran is hired to kill Rajinikanth and during the fight both get injured and finally they come to know that they are childhood friends. Finally, both of them unite and fight the bad guy who killed their family.

==Cast==
* Rajinikanth
* Raghuvaran
* Shobana
* Sowcar Janaki
* Radha Ravi
* Vinu Chakravarthy
* Janagaraj
* Charle Madhuri
* Disco Shanti
* Delhi Ganesh Thyagu
* Ilavarasan
* Poornam Viswanathan Vijayakumar  

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Pulamaipithan || 06:15
|-
| 2 || Adi Kannaathaal || S. P. Balasubrahmanyam || 04:36
|-
| 3 || Adi Vaanmathi || S. P. Balasubrahmanyam, K. S. Chithra|K.S Chithra || 04:29
|-
| 4 || Iruvizhiyin || S. P. Balasubrahmanyam, K. S. Chithra || 04:25
|- Vaali || 04:32
|}

==References==
 

==External links==
*  

 

 

 
 
 
 
 
 
 


 