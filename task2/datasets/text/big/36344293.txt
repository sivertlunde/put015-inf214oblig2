The Eddie Cantor Story
{{Infobox film
| name           = The Eddie Cantor Story
| image          = 
| caption        = 
| director       = Alfred E. Green
| producer       = Sidney Skolsky
| writer         = 
| starring       = Keefe Brasselle Marilyn Erskine Aline MacMahon
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Warner Brothers
| released       = 25 December 1953 (NYC) 20 January 1954 (US)
| runtime        = 115 minutes
| country        = United States
| language       = English
| gross = $2.3 million (US) 
}}
The Eddie Cantor Story is a 1953 American film about the life of Eddie Cantor, starring Keefe Brasselle as Cantor, and released by Warner Brothers. 

==Plot==
Raised by his grandmother on New Yorks East Side, 13-year-old Eddie sings while another neighborhood kid, Rocky Kramer, and his gang pick pockets. Eddie is sent by Grandma Esther to a boys camp, where he entertains the others with his songs and routines.

Ida Tobias, daughter of a local merchant, elopes with Eddie a few years later. Rocky is now a local politician and gets Eddie a job in a nightclub. Eddie tells the family hes the star performer there, but hes actually a singing waiter. But piano player Jimmy Durante helps land him a job in a California show.

A headline performer envious of Eddies popularity pulls a prank, telling him Flo Ziegfeld wants him for the Follies show in New York. It turns out Ziegfeld has never heard of Eddie when he arrives at the theater, but an audition by Eddie is so good, Ziegfeld does indeed hire him.

Ida gives birth to several children while Eddie becomes a big success. Shes upset that his family doesnt seem to come first, and matters are complicated when Eddies fortune is lost in the 1929 stock-market crash. A heart attack slows Eddie as well, but he prospers on the radio as his health improves, and soon he is happy at work and at home.

==Cast==
* Keefe Brasselle as Eddie Cantor
* Marilyn Erskine as Ida
* Gerald Mohr as Rocky
* Aline MacMahon as Grandma Esther William Forrest as Flo Ziegfeld
* Eddie Cantor (cameo)

==References==
 

==External links==
* 
* 

 

 
 
 
 

 