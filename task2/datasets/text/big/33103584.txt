Yachts and Hearts, or The Opium Smugglers
 

 
{{Infobox film
  | name     = Yachts and Hearts, or The Opium Smugglers
  | image    = 
  | caption  = 
  | director = Charles Byers Coates		
  | producer = 
  | writer   = 
  | based on = 
  | starring = Beryl Clifton Chris Olsen
  | music    = 
  | cinematography = G.L. Gouday
  | editing  = 
| studio = Antipodes Films
  | distributor = 
  | released = 25 March 1918
  | runtime  = 5 reels
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

Yachts and Hearts, or The Opium Smugglers is a 1918 Australian silent film about opium smugglers in Sydney. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 79 

It is considered a lost film.

==Plot==
Opium smugglers work in Sydney. There is a car chase which ends in a crash, a cabaret which turns into a church, a yacht race in Sydney harbour, and 40 bathing beauties.

==Cast==
*Beryl Clifton as Ella Deane 
*Chris Olsen as Maurice Dean
*Arthur Spence as detective
*Clare St Clair as Mrs Friedman
*Billie Monckton as crippled boy
*Edith Clarke
*Vera Chamberlain
*Melville Stevenson
*Dorothy and Lola Campbell
*David Edelsten
*Marjorie Sergeant

==Production==
This was the second movie from Antipdes Films, who had previously made A Romance of Burke and Wills Expedition of 1860. The movie was shot in January 1918 and seems to have made little impact. Antipodes made no more movies. 

==References==
 

==External links==
*  
*   at National Film and Sound Archive

 
 
 
 
 
 
 


 