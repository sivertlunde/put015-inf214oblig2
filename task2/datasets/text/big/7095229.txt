Cyborg Cop
 

{{Infobox Film
| name = Cyborg Cop
| image = Cyborg Cop DVD.jpg
| caption = DVD cover art
| director = Sam Firstenberg
| producer =
| writer = Greg Latter
| narrator = David Bradley  John Rhys-Davies  Todd Jensen  Alonna Shaw Ron Smerczak
| music =
| cinematography =
| editing =
| distributor =
| released =
| runtime = 95 minutes
| country = United States
| language =
| budget =
}} David Bradley, directed by written by Greg Latter.

== Brief summary ==
 David Bradley) receives an emergency call from his brother Philip (Todd Jensen). he needs him to go to St. Keith Island in the Caribbean. There a wealthy mad scientist (John Rhys-Davies) is transforming men into killer cyborgs and selling them on the black market.

Unknown to Jack, the scientist has already operated on his brother, changing Philips torso and right arm into mechanically assisted bulletproof steel, making him another killing machine.

The film has two sequels, Cyborg Cop II and III. The last was released as Terminal Impact in 1995. {{cite web
 | title=Terminal Impact (1995) | work=The New York Times
 | url=http://movies.nytimes.com/movie/263078/Terminal-Impact/overview
}} 

==Critical reception==

Critical reception was generally, although not universally, poor.  Halliwells Film Guide, for example, described it as "dim standard robot action fodder" with a "violent, cliché-ridden plot." 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 