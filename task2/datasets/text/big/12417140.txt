The Unknown Terror
{{Infobox film
| name           = The Unknown Terror
| director        = Charles Marquis Warren
| producer       = Charles Marquis Warren Kenneth Higgins
| starring       = John Howard Mala Powers
| cinematography = Joseph F. Biroc
| editing        = Michael Luciano
| music          = Raoul Kraushaar Regal Films
| released       =  
| country        = United States
| language       = English
}}

The Unknown Terror is a 1957 American science fiction/horror film. The film was written by Kenneth Higgins and directed by Charles Marquis Warren.

==Plot==

The mysterious disappearance of Jim Wheatley (Charles Gray), while exploring the "cave of the dead" near a Mexican village, brings his sister, Gina (Mala Powers), and her husband, Dan Matthews (John Howard), to the territory to search for him. Embittered, crippled Pete Morgan (Paul Richards), insists on going along and reminds Dan that his condition is Dans fault since it happened in an accident in which Pete saved Dans life. Plus, Gina was Petes sweetheart before the accident.  Things become tense when native wife Concha (May Wynn) arranges for the men to be led to a place where they can hear the voices of the dead crying from beneath the earth and, while they are gone, a grotesque, demented man apparently covered with a foamy fungus attacks Gina and chases her into the jungle.  This creature is run off, but the party determines to find a way to the source of the underground sounds.  They do, and find a cave filled with a fast-growing parasitic fungus, some humans who have gotten in contact with it and been turned into monsters, and a stairwell leading to the house of the thuggish researcher, who in fact has created this monster-making fungus artificially and does not plan to stop experimenting with it.

==References==
 
*  

 

 
 
 
 
 
 
 


 
 