Reality Horror Night
{{Infobox film
| name = Reality Horror Night
| story = Sean Pomper
| screenplay = Douglas Elford Argent Nicholas Mark Harding
| director = Douglas Elford Argent
| producer = Karen Katz Neal Bellin Gregg Luckman Seth Zuckerman
| released =  
| runtime = 80 minutes
| country = United States
| language = English
}} Glen Cove, New York at the Glen Cove Mansion. The film was released on October 31, 2009.

==Plot==
The cast enters a mansion to compete on a new reality show for $1 million but with an unexpected twist. Mysteriously, a cast member disappears but without a formal elimination ceremony. When more and more castmates start disappearing in this fashion it is up to the remaining cast to discover how this game is really played.

==Cast==
*Matthew Underwood Destiney Moore
*Erik Chopin
*Gina Lynn
*Paul Grassi
*Billy Garcia
*Cathy Nardone
*Angelique Morgan
*Elizabeth Gasinski
*Gary Garver
*Joseph Gannascoli
*Danielle Dipietro
*Robert Smith
*Jason Prager
*David Frank Fletcher Jr.

==References==
 

==External links==
*  
*  

 
 
 
 
 

 