Excision (film)
{{Infobox film
| name           = Excision
| image	         = Excision poster.jpg
| image_size     = 215px
| alt            =  
| caption        = Release poster
| director       = Richard Bates, Jr.
| producer       = Dylan Hale Lewis
| writer         = Richard Bates, Jr.
| starring       = {{plainlist|
* AnnaLynne McCord
* Traci Lords
* Ariel Winter
* Roger Bart
* Jeremy Sumpter
* Malcolm McDowell
* Matthew Gray Gubler
* Marlee Matlin
* Ray Wise John Waters
}}
| music          = {{plainlist|
* Steve Damstra II
* Mads Heldtberg
}}
| cinematography = Itay Gross
| editing        = {{plainlist|
* Yvonne Valdez
* Steve Ansell
}}
| studio         = BXR Productions
| distributor    = Anchor Bay Films
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} John Waters. The film is a feature-length adaptation of the 2008 short film of the same name. Excision premiered at the 2012 Sundance Film Festival.  Excision played in the category of Park City at Midnight. 

==Plot==
The film depicts the life of a disturbed and delusional high school student, Pauline (AnnaLynne McCord), with aspirations of a career in medicine and the extremes to which she goes to earn the approval of her controlling mother. Pauline has a younger sister named Grace (Ariel Winter) who suffers from cystic fibrosis. Pauline has vivid dreams about herself and others being mutilated with excessive amounts of blood; and after each dream wakes panting in an orgasmic state.

Pauline decides to lose her virginity to a boy named Adam - explaining what she wants from him and gives him her number. Because of her fascination with blood she arranges their meeting while she is on her period. While having an orgasm she imagines herself choking Adam and the bed filling with blood. At the end she asks Adam to go down on her, which he obliges. After a few moments he runs to the bathroom disgusted by the blood, as he was unaware she was on her period.

During sex class, Pauline takes a sample of her blood and puts it under a microscope to check for STDs. Later on she sees Adam on the bleachers with his girlfriend. She tells Adam she doesnt have any STDs and asks his girlfriend if she has any, who states that she doesnt. Pauline then tells them that if the girlfriend doesnt have anything neither will she. After she walks away Adams girlfriend deduces that he was unfaithful.
 TP Paulines house and spray paint cunt and whore on it. Pauline, on an angry tirade at school, pushes Adam to the ground and slams his ex-girlfriends face into her locker, resulting in her being expelled from school.

While in her room she overhears her parents saying Graces doctor wants her on the lung transplant list. During dinner that night her sister has a severe coughing fit, which concerns her parents. In the morning their mother leaves and the father stays home. Pauline drugs her father with tea and when he falls asleep, ties him up and gags him. Pauline then lures the jump roping girl to the backyard under the false premise that there are several jump ropes she can have that Grace can no longer use because of her lungs. She places a cloth over her mouth and after a brief struggle the girl loses consciousness. Pauline talks to Grace and lovingly strokes her hair, stating that Grace may be confused by what is going to happen but she has only her best interest in mind. She drugs Grace with the same cloth, then cuts and shaves her own hair.

In the garage, Pauline has cut open both girls and continues with her surgery, moving the healthy girls lungs into her sister and placing the other lungs on ice, then sewing them both up. Paulines mother arrives home and sees her husband tied up. She panics and runs through the house frantically screaming for Grace. She opens the garage door and finds Pauline with the bodies. Pauline explains that it is her first surgery, and although it is messy, it is wonderful, and that she was unsure what to do with the jump roping girls body so she used her to practice her incisions. She then urges her mother to take a closer look, who runs towards her and grabs her, holding her and screaming hysterically. Pauline at first seems proud, but then begins to sob.

==Cast==
* AnnaLynne McCord as Pauline
* Traci Lords as Phyllis
* Ariel Winter as Grace
* Roger Bart as Bob
* Jeremy Sumpter as Adam
* Malcolm McDowell as Mr. Cooper
* Matthew Gray Gubler as Mr. Claybaugh
* Marlee Matlin as Amber
* Ray Wise as Principal Campbell John Waters as William
* Molly McCook as Natalie
* Natalie Dreyfuss as Abigail
* Cole Bernstein as Kimberly
* Emily Bicks as Breanna
* Brennan Bailey as Sebastian
* Matthew Fahey as Nathan
* Sidney Franklin as Timothy

==Production==
Excision is Richard Bates, Jr.s first feature film. Bates, a Virginia native and graduate of the New York University Tisch School of the Arts, filmed Excision over 28 days in and around Los Angeles, California. 

==Reception==
Excision received generally positive reviews. Review website Rotten Tomatoes reports that 81% of critics gave the film positive reviews, based on 21 reviews with an average rating of 6.7/10.   Robert Koehler of Variety (magazine)|Variety called it "technically polished juvenilia that provokes without resonance".   At The A.V. Club, two critics reviewed it at Sundance.  Noel Murray rated it B+ and wrote, "This is one of the damnedest adolescent misfit movies youll ever see—for those who can stomach the splatter, that is."   Nathan Rabin rated it B+ and called it "a supremely nasty piece of work in the best way possible."   Steve Rose of The Guardian rated it 2/5 stars and wrote, "This body-horror teen effort could have Cronenberg meets Solondz – but it isnt".   Witney Seibold of IGN rated it 7.5/10 and wrote, "Excision is twisted, ballsy, nasty, kind of gross, and more than a little bit disturbing."  Streiber states it "revels a little too much in its gore" and seems as though it is a splatter film "only intended to shock", though the ending "clearly accentuates the tragedy of its clearly insane main character." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 