The Bridesmaid (film)
{{Infobox film
| name           = The Bridesmaid 
| image          = The Bridesmaid film.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = Claude Chabrol
| producer       = Françoise Galfré Patrick Godeau
| screenplay     = Claude Chabrol Pierre Leccia
| based on  =       Ruth Rendell
| narrator       = 
| starring       = Benoît Magimel Laura Smet
| music          = Matthieu Chabrol   
| cinematography = Eduardo Serra
| editing        = Monique Fardoulis Canal Diffusion France 2 Cinéma Integral Film
| distributor    = First Run Features
| released       = 7 September 2004 (Venice Film Festival) 28 July 2006 (USA)
| runtime        = 111 minutes
| country        = France Italy Germany
| language       =
| budget         = €5,950,000 
| gross          = $3,162,662  
| preceded_by    = 
| followed_by    = 
}}
The Bridesmaid is a 2004 film co-written and directed by Claude Chabrol. Its title in French is La Demoiselle dhonneur. The film is based on the novel The Bridesmaid by Ruth Rendell.

==Plot== Roman goddess Flora that Philippe had given her which was in the family garden.

Not too long after receiving the gift, Gerard appears to vanish without a trace. Philippe makes it his mission to recover the sculpture. He finally tracks it down and places it in his closet without telling anyone. Later, at his sisters wedding, Philippe meets attractive bridesmaid Senta (Laura Smet|Smet) and the two quickly fall for each other passionately. She claims to be a model and aspiring actress who lives in a huge villa which she says she inherited from her father.  The sexy Senta may be beautiful and irresistible, yet she also seems to have several macabre ideas about life, love, and death. As their affair intensifies, she asks him to kill a stranger to prove his love. He at first thinks she is joking but then realizes she is actually serious about carrying out the plan.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Benoît Magimel || Philippe Tardieu 
|-
| Laura Smet || Stéphanie "Senta" Bellange 
|-
| Aurore Clément || Christine
|-
| Bernard Le Coq || Gérard Courtois 
|-
| Solène Bouton || Sophie Tardieu 
|-
| Anna Mihalcea || Patricia Tardieu 
|-
| Thomas Chabrol || Lieutenant José Laval 
|}

==Critical reception==
The film was well received by critics. Website metacritic.com assigned a 74 out of 100 based on 20 reviews, indicating "generally favorable reviews." 

Desson Thomas of The Washington Post:
 

Ty Burr of The Boston Globe:
 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 