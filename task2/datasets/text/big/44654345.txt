A Prisoner of the Harem
{{infobox film name = A Prisoner of the Harem image =  caption =  director = Sidney Olcott producer = Kalem Company writer = Gene Gauntier starring = Jack J. Clark Gene Gauntier distributor = General Film Company cinematography = George K. Hollister
| released =  
| runtime = 1000 ft
| country = United States language = Silent film (English intertitles) 
}}

A Prisoner of the Harem is a 1912 American silent film produced by Kalem Company and distributed by General Film Company. It was directed by Sidney Olcott with himself, Gene Gauntier and Jack J. Clark in the leading roles.

==Cast==
* Gene Gauntier - Alice Durand
* Jack J. Clark - The tourist
* Robert Vignola - The Pacha
* Alice Hollister - Zorah

==Production notes==
The film was shot in Luxor, Egypt.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 
 
 


 