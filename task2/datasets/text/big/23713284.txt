No Funny Business
 
  British comedy film directed by Victor Hanbury and starring Laurence Olivier, Gertrude Lawrence, Jill Esmond and Edmund Breon.  The film is a comedy of errors set in a divorce case.  It was made at Ealing Studios.  Olivier had returned to Britain after his career, following an initial move to Hollywood, had faltered. 

==Cast==
* Gertrude Lawrence - Yvonne
* Laurence Olivier - Clive Dering
* Jill Esmond - Anne
* Edmund Breon - Edward
* Gibb McLaughlin - Florey
* Muriel Aked - Mrs Fothergill

==References==
 

==Bibliography==
* Munn, Michael. Lord Larry: The Secret Life of Laurence Olivier. Robson Books, 2007.

 

 

 
 
 
 
 


 