Antaheen
{{Infobox film
| name           = Antaheen (The Endless Wait)
| image          = Antaheen.jpg
| image_size     = 250px
| caption        = Film poster
| director       = Aniruddha Roy Chowdhury
| producer       = Jeet Banerjee Aniruddha Roy Chowdhury Indrani Mukerjee Executive producer - Neha Rungta
| writer         = Shyamal Sengupta
| starring       = Radhika Apte Rahul Bose Mita Vashisth Aparna Sen Sharmila Tagore
| music          = Shantanu Moitra
| cinematography = Abhik Mukhopadhyay
| editing        = Arghakamal Mitra
| distributor    = Mumbai Mantra
| released       =  
| runtime        = 120 minutes
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}} Bengali film directed by Aniruddha Roy Chowdhury. The film stars Radhika Apte, Rahul Bose, Mita Vashisth, Aparna Sen, Kalyan Ray and Sharmila Tagore. 

==Plot==
Abhik Chowdhury is an IPS officer with a heart - honest, upright, yet laidback. Having lost faith in the real relationships that he sees around him, Abhik seeks solace in the virtual world. While navigating through this virtual world, Abhik develops an online relationship with a young woman, without knowing anything about her. She is Brinda - a young, dynamic television journalist. She comes from a conventional middle-class home, and her parents live in Jamshedpur.  For both Abhik and Brinda, who do not know each others real names and whereabouts, this online communication soon becomes an increasingly intense relationship, more real than virtual. While this relationship blossoms within the confines of two computer screens, Abhik gets his six minutes of fame on national television, when he successfully masterminds a raid on a consignment of illegal arms. Brinda telephones him to ask for an on-camera interview, but Abhik declines, stating that did not want to sensationalize the event any further. Ironically the virtual lovers, Abhik and Brinda have an acrimonious first meeting in the real world. It happens at the launch of a controversial mega project of the big-time real estate entrepreneur, Vijay Ketan Mehra. Unaware that she knows Abhik so well on the Internet, Brinda, still smarting from Abhiks refusal, gets into a bitter exchange of words with him. The argument veers to issues like sensationalism and soon enough, it is clear that Brinda and Abhiks real-life interaction has started off on a sour note. Before leaving in a huff, Brinda overhears a piece of conversation between two men about Mehras project. This gives her a lead to a potentially big scoop.

Ranjan and Paromita, an estranged couple, become the bridge between Brinda and Abhik. Ranjan is Abhiks cousin, but he is more of a friend, philosopher and guide. Ranjan is now a stockmarket addict and leads a lonely life after having separated from Paromita a few years ago. Ranjan is as acutely sensitive and perceptive as he is bitter and cynical on the surface. Paromita (Paro to friends and colleagues), is a senior marketing executive with the channel where Brinda works. For Brinda and Abhik, things take a different turn – from hostility to a grudging interest in each other – when they bump into each other at Ranjans birthday party, which is secretly organized as a surprise by Paro. At the party, Brinda and Abhik get to know each other a little better. The mood of the party turns romantic yet poignant with Paro singing Ranjans favourite song at his insistence. She has never sung that song ever since she left Ranjans home. In the virtual world of the Internet, Brinda and Abhiks online chatting continues unabated, even though their identities remain undisclosed.

Abhik confides in Ranjan that he is probably falling in love, although he does not know with whom. Ranjan warns Abhik with his usual cynicism and reminds him of the perils and pains that often define love. Behind his sardonic comments Abhik gets a glimpse of Ranjans sensitivity and loneliness. After hearing his cousins advice, Abhik leaves feeling confused yet still not convinced enough to stop falling further in love.Paro gets a rather lucrative offer from Mumbai. But she is in two minds between upgrading her career and staying back for lost love. As the crisis deepens in her mind, she seeks Ranjans advice. Although it is bound to intensify his loneliness, Ranjan encourages her to shift to Mumbai. Paro gets confused by Ranjans pragmatic, well-meaning advice, as she had hoped and expected him to want her to stay back in the city. Meanwhile, a series of upsetting events, including Paros talk about moving to Mumbai and her final break up with her boyfriend, weigh Brinda down. She feels torn between opposite poles of love and friendship. The only thing she can find solace is with her virtual friend, who seems to be her only source of comfort. At work, Brinda hits a stumbling block while doing an investigative story on V.K Mehras El Dorado project. At this point, she turns to Abhik for help. Brinda notices some uncanny similarities between Abhik and her anonymous chat friend, in the way they talk, and in their choice of phrase. Something about Abhik reminds her of her online friend. As she follows the leads given by Abhik, she manages to get an important interview lined up which can give her the proof she needs to wrap up her story. The night before her interview, a particular phone conversation with Abhik strikes her. She gets onto the net, and tells her chat friend that they should meet. But this is a meeting which is not destined to happen as things take an unexpected turn. During reaching office for night shift Brinda died in a massive car accident and the meeting never happens. Thats the thing is Endless waiting for love. Paro leaves for Mumbai. The film ends with the song Bhindesi Tara.

==Cast==
* Radhika Apte as Brinda
* Rahul Bose as Abhik
* Biswajit Chakraborty as Mr. Saha
* Barun Chanda as Dibakar
* Kaushik Ganguly as Mrinmoy
* Rudranil Ghosh as Tanmoy
* Shauvik Kundagrami as Mr. Mehra
* Kunal Padhy as Mr. Mukherjee
* Kalyan Ray as Ranjan
* Saswati Guhathakurta as Brindas Mother
* Aparna Sen as Paro
* Arindam Sil as Sabya
* Sharmila Tagore as Abhiks aunt
* Parmeet Sethi as Kuljeet Singh
* Mita Vasisht as Mrs. Mehra
* Sanjay Bhattacharya as Dr. Sanjay Bhattacharya
* Sugata Ghosh as Sugata Ghosh
* Ekavali Khanna as Ekkavali Khanna
* Jai Ranjan Ram as Dr. Jai Ranjan Ram
* Sabyasachi Sen as Dr. Sabyasachi Sen

:Other Casts

* Anushree Acharya
* Sukanya Bhattacharya
* Jayshree Dasgupta
* Rajkumar Dutta
* Diya Guha
* Aparajita Majumdar
* Sudip Majumdar
* Pradip Rai
* Subhash Sarkar

==Production==
The film was shot on a limited budget on location in Kolkata. Rahul Bose and Sharmila Tagore worked on the film for free and composer Shantanu Moitra waived his fee for composing the films music. 

==Awards== National Film Award   Best Film Best Cinematography - Abhik Mukhopadhyay Best Lyrics - Anindya Chatterjee & Chandril Bhattacharya for "Pherari Mon..." Best Female Playback Singer - Shreya Ghoshal for "Pherari Mon..."

==Soundtracks==
The music of the film has been written by Anindya Chatterjee and Chandril Bhattacharya. Shantanu Moitra is the music composer, whille, Shaan, Babul, Shreya Srikant, Antara and Pranab have lend their voice for this musical extravaganza.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track No. !! Title !! Writer !! Music Composer !! Performed By !! Courtesy(TM/C)
|-
| 01.
| Antaheen
| Anindya Chatterjee and Chandril Bhattacharya
| Shantanu Moitra
| Shaan
| Saregama HMV
|-
| 02.
| Bhindeshi Tara
| Anindya Chatterjee and Chandril Bhattacharya
| Shantanu Moitra
| Anindya Chatterjee
| Saregama HMV
|-
| 03.
| Jao Pakhi
| Anindya Chatterjee and Chandril Bhattacharya
| Shantanu Moitra
| Shreya Ghoshal and Pranab Biswas
| Saregama HMV
|-
| 04.
| Muthor Rumaal
| Anindya Chatterjee and Chandril Bhattacharya
| Shantanu Moitra
| Antara Choudhary and Srikanta Acharya
| Saregama HMV
|-
| 05.
| Pherari Mon
| Anindya Chatterjee and Chandril Bhattacharya
| Shantanu Moitra
| Shreya Ghoshal and Babul Supriyo
| Saregama HMV
|-
| 06.
| Shokal Ashe Na
| Anindya Chatterjee and Chandril Bhattacharya
| Shantanu Moitra
| Shreya Ghoshal
| Saregama HMV
|-
|}

==See also==
* Bengali films of 2009

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 