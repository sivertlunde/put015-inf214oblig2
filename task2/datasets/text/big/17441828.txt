Waltz with Bashir
 
{{Infobox film
| name                     = Waltz with Bashir
| image                    = Waltz with Bashir Poster.jpg
| alt                      =
| caption                  = Theatrical release poster
| film name                = {{Infobox name module
| original                 = ואלס עם באשיר
}}
| director                 = Ari Folman
| producer                 = {{Plainlist|
* Ari Folman
* Serge Lalou
* Gerhard Meixner
* Yael Nahlieli
* Roman Paul
}}
| writer                   = Ari Folman
| starring                 = Ari Folman Max Richter
| editing                  = Nili Feller
| production companies     = {{Plainlist|
* Bridgit Folman Film Gang
* Les Films dIci
* Razor Film Produktion
}}
| distributor              = Sony Pictures Classics
| released                 =  
| runtime                  = 90 minutes  
| country                  = {{Plainlist|
* Israel
* Germany
* France
}}
| language                 = Hebrew
| budget                   = $2 million 
| gross                    = $11,125,849 
}} animated War war documentary film written and directed by Ari Folman. It depicts Folman in search of his lost memories of his experience as a soldier in the 1982 Lebanon War. 
 premiered at NSFC Award IDA Award for Feature Documentary, and was nominated for an Academy Award for Best Foreign Language Film, a BAFTA Award for Best Film Not in the English Language and an Annie Award for Best Animated Feature.
 banned in Lebanon. 

==Plot== Lebanon War. Folman is surprised to find that he remembers nothing from that period. Later that night he has a vision from the night of the Sabra and Shatila massacre, the reality of which he is unable to clearly recall. In his memory, he and his soldier comrades are bathing at night by the seaside in Beirut under the light of flares descending over the city.

Folman rushes off to meet a childhood friend, who advises him to seek out other people who were in Beirut at the same time in order to understand what happened there and to revive his own memories. Folman converses with friends and other soldiers who served in the war, a psychologist, and the Israeli TV reporter Ron Ben-Yishai who covered Beirut at the time, amongst others. Folman eventually realizes that he "was in the second or third ring" of soldiers surrounding the Palestinian refugee camp where the carnage was perpetrated, and that he was among those soldiers firing flares into the sky to illuminate the refugee camp for the Lebanese Christian Phalange militia perpetrating the massacre inside. He concludes that his amnesia had stemmed from his feeling as a teenage soldier that he was as guilty of the massacre as those who actually carried it out. The film ends with animation dissolving into actual footage of the aftermath of the massacre.

==Cast==
The film contains both fictional composites of real life figures and actual living people.
* Ari Folman, an Israeli filmmaker who recently finished his military reserve service. Some twenty years before, he served in the IDF during the Lebanon War.
* Miki Leon as Boaz Rein-Buskila, an Israeli Lebanon War veteran accountant suffering from nightmares.
* Ori Sivan, an Israeli filmmaker who previously co-directed two films with Folman and is his long-time friend.
* Yehezkel Lazarov as Carmi Canan, an Israeli Lebanon War veteran who once was Folmans friend and now lives in the Netherlands.
* Ronny Dayag, an Israeli Lebanon War veteran high food engineer. During the war, he was a Merkava tank crewman.
* Shmuel Frenkel, an Israeli Lebanon War veteran. During this war he was the commander of an infantry unit.
* Zahava Solomon, an Israeli psychologist and researcher in the field of psychological trauma.
* Ron Ben-Yishai, an Israeli journalist who was the first to cover the massacre.
* Dror Harazi, an Israeli Lebanon War veteran. During the war, he commanded a tank brigade stationed outside the Shatila refugee camp.

==Title== Waltz in Bashir Gemayel. Thematically, the films title is a symbolic description of the "Waltz", which is a dance for a couple, that Folman is "dancing" with his memories from the war in Lebanon.  The title also refers to Israels short-lived political waltz with Bashir Gemayel as president of Lebanon.

==Production== documentary made almost entirely by the means of animation. It combines classical music, 1980s music, realistic graphics, and surrealistic scenes together with illustrations similar to comics. The entire film is animated, excluding one short segment of news archive footage.

The animation, with its dark hues representing the overall feel of the film, uses a unique style invented by Yoni Goodman at the Bridgit Folman Film Gang studio in Israel. The technique is often confused with rotoscoping, an animation style that uses drawings over live footage, but is actually a combination of Adobe Flash cutouts and classic animation.  Each drawing was sliced into hundreds of pieces which were moved in relation to one another, thus creating the illusion of movement. The film was first shot in a sound studio as a 90-minute video and then transferred to a storyboard. From there 2,300 original illustrations were drawn based on the storyboard, which together formed the actual film scenes using Flash animation, classic animation, and 3D technologies. 
 Max Richter OMD ("Enola Enola Gay"), PiL ("This is Not a Love Song"), Navadey Haukaf (נוודי האוכף Cake song "I Bombed Korea", retitled "Beirut"). Some reviewers have viewed the music as playing an active role as commentator on events instead of simple accompaniment. 

The comics medium, in particular Joe Sacco,  the novels Catch-22, The Adventures of Wesley Jackson, and Slaughterhouse-Five,  and painter Otto Dix  were mentioned by Folman and art director David Polonsky as influences on the film. The film itself was adapted into a graphic novel in 2009. 

==Release==
Waltz with Bashir opened in the United States on 25 December 2008 in a mere five theaters, where it grossed $50,021 in the first weekend. By the end of its run on 14 May 2009, the film had grossed $2,283,849 in the domestic box office. Overseas, Waltz earned $8,842,000 for a worldwide total of $11,125,849.   

===Perception of the film===
 . March 2009. 

As of September 2014, the film holds a 96% "fresh" rating on review aggregator website Rotten Tomatoes based on 143 critics for an average of 8.4/10; the general consensus states: "A wholly innovative, original, and vital history lesson, with pioneering animation, Waltz With Bashir delivers its message about the Middle East in a mesmerizing fashion." 

===Lebanon screening===
The film is banned in some Arab countries (including Lebanon), with the most harsh critics in Lebanon, as the film depicts a vague and violent time in Lebanons history. A movement of bloggers, among them the Lebanese Inner Circle, +961 and others have rebelled against the Lebanese governments ban of the film, and have managed to get the film seen by local Lebanese critics, in defiance of their governments request on banning it. The film was privately screened in January 2009 in Beirut in front of 90 people.    Since then many screenings have taken place. Pirated copies are also available in the country. Folman saw the screening as a source of great pride: 
 

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2008.   

 
 
*1st - Ella Taylor, LA Weekly 
*1st - Stephen Farber, The Hollywood Reporter 
*2nd - Andrea Gronvall, Chicago Reader 
*2nd - Andrew OHehir, Salon.com|Salon 
*4th - Lisa Schwarzbaum, Entertainment Weekly 
*5th - J. Hoberman, The Village Voice 
*5th - Lawrence Toppman, The Charlotte Observer 
*6th - Liam Lacey, The Globe and Mail 
*7th - David Edelstein, New York (magazine)|New York magazine 
*7th - Marc Savlov, The Austin Chronicle 
 
*8th - Sheri Linden, The Hollywood Reporter 
*9th - David Ansen, Newsweek (tied with WALL-E) 
*9th - Frank Scheck, The Hollywood Reporter 
*9th - Joe Morgenstern, The Wall Street Journal 
*9th - Peter Rainer, The Christian Science Monitor 
*9th - Rick Groen, The Globe and Mail 
*10th - Kenneth Turan, Los Angeles Times (tied with WALL-E) 
*10th - Lou Lumenick, New York Post 
*10th - Scott Foundas, LA Weekly (tied with WALL-E) 
 

It was also ranked #34 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010,  and #4 in Current TVs "50 Documentaries to See Before You Die" in 2011.

===Awards and nominations=== Best Foreign Language Film since The Policeman (1971), and the first documentary film to win the award. 

It was unsuccessfully submitted for an Academy Award for Best Animated Feature nomination, and became ineligible for the Academy Award for Documentary Feature when the Academy announced its new rule to nominate only documentaries which have had a qualifying run in both New York and Los Angeles by August 31. 
 National Board of Reviews Top Foreign Films list. Folman won the Writers Guild of America|WGAs Best Documentary Feature Screenplay award and the Directors Guild of America|DGAs Outstanding Directorial Achievement in Documentary award for creating the film. Folman also received nominations for Annie Awards and BAFTA Awards for Best Animated Feature, but lost both awards to Kung Fu Panda and WALL-E respectively.
 
 
  Academy Awards:
** Best Foreign Language Film (nominated)
*  Animafest Zagreb:
** Grand Prize (won) Annie Awards:
** Best Animated Feature (nominated)
** Directing in an Animated Feature Production (Ari Folman, nominated) Max Richter, nominated)
** Writing in an Animated Feature Production (Ari Folman, nominated)
*  Asia Pacific Screen Awards:
** Best Animated Film (won) BAFTA Awards:
** Best Film Not in the English Language (nominated)
** Best Animated Film (nominated)
*  Bodil Awards:
** Best Non-American Film (won) Boston Society of Film Critics Awards:
** Best Foreign Language Film (runner-up)
** Best Animated Film (runner-up) British Independent Film Awards:
** Best Foreign Independent Film (won) Broadcast Film Critics Awards:
** Best Foreign Language Film (won)
** Best Animated Film (nominated) Cannes Film Festival:
** Palme dOr (nominated)
*  César Awards 2009|César Awards:
** Best Foreign Film (won) Chicago Film Critics Association Awards:
** Best Animated Film (nominated)
*  Dallas-Fort Worth Film Critics Association Awards 2008|Dallas-Fort Worth Film Critics Association Awards:
** Best Documentary (nominated)
** Best Foreign Language Film (nominated) Directors Guild of America Awards:
** Outstanding Directorial Achievement in Documentary (Ari Folman, won) European Film Awards:
** Best Film (nominated)
** Best Composer (Max Richter, won)
** Best Director (Ari Folman, nominated)
** Best Screenwriter (Ari Folman, nominated)
*  Festival du Nouveau Cinéma:
** Daniel Langlois Innovation Award (won)
 
*  Gijón International Film Festival:
** Best Art Direction (Yoni Goodman, won)
** Youth Jury, Feature (won) Golden Globe Awards:
** Best Foreign Language Film (won)
* International Cinephile Society Awards:
** Best Film Not in the English Language (won)
** Best Animated Film (won)
** Best Documentary (won) International Documentary Association Awards:
** Feature Documentary (won, tied with Man on Wire)
* International Film Music Critics Association Awards:
** Breakout Composer of the Year (Max Richter, nominated)
** Best Original Score for an Animated Feature (Max Richter, nominated) Los Angeles Film Critics Association Awards:
** Best Animated Film (won)
** Best Documentary/Non-Fiction Film (runner-up) National Society of Film Critics Awards:
** Best Film (won)
*  Ophir Awards:
** Best Film (won)
** Best Director (Ari Folman, won)
** Best Screenplay (Ari Folman, won)
** Best Artistic Design (David Polonsky, won)
** Best Editing (Nili Feller, won)
** Best Sound Design (Aviv Aldema, won)
** Best Cinematography (Yoni Goodman, nominated)
*  Palić Film Festival:
** Golden Tower (won) Satellite Awards:
** Best Animated or Mixed Media Film (nominated)
** Best Documentary Film (nominated)
*  Tallinn Black Nights Film Festival:
** Special Jury Prize (won)
*  Tokyo Filmex:
** Grand Prize (won)
*  Utah Film Critics Association Awards:
** Best Non-English Language Feature (runner-up)
** Best Documentary Feature (runner-up) Writers Guild of America Awards:
** Best Documentary Feature Screenplay (Ari Folman, won)
 

==See also==
* Beaufort (film)|Beaufort Cup Final
* Lebanon (2009 film)|Lebanon
* Sabra and Shatila massacre
* Bashir Gemayel
* List of animated feature-length films
* Post traumatic stress disorder

==References==
 

==External links==
*  
*  
*  
*  
*  
*  , "france24english", May 16, 2008.

   
{{succession box NSFC Award for Best Film
| years = 2008
| before= There Will Be Blood
| after = The Hurt Locker
}}
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 