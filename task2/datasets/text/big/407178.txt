The Day After Tomorrow
 
{{Infobox film
| name           = The Day After Tomorrow 
| image          = The Day After Tomorrow movie.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Roland Emmerich
| producer       = Roland Emmerich Mark Gordon
| writer         = Roland Emmerich Jeffrey Nachmanoff
| starring       = Dennis Quaid Jake Gyllenhaal Ian Holm Emmy Rossum Sela Ward 
| music          = Harald Kloser
| cinematography = Ueli Steiger David Brenner Lionsgate The Mark Gordon Company
| distributor    = 20th Century Fox
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $125 million
| gross          = $544.2 million  . Box Office Mojo. IMDb. Retrieved April 16, 2011. 
}}
The Day After Tomorrow is a 2004 American climate fiction-disaster film co-written, directed, and produced by Roland Emmerich and starring Dennis Quaid, Jake Gyllenhaal, Ian Holm, Emmy Rossum, and Sela Ward. The film depicts fictional catastrophic climatic effects in a series of extreme weather events that usher in global cooling and leads to a new ice age. The film was made in Toronto and Montreal and is the highest-grossing Hollywood film to be made in Canada (if adjusted for inflation).

Originally planned for release in the summer of 2003, The Day After Tomorrow premiered in Mexico City on May 17, 2004 and was released worldwide from May 26 to May 28 except in South Korea and Japan, where it was released June 4–5, respectively.

==Plot== NOAA when the shelf breaks off.
 North Atlantic North Atlantic paleoclimatological weather model shows how climate changes caused the first Ice Age. His team, along with NASAs meteorologist Janet Tokada, builds a forecast model.
 FAA to eyes holding   temperatures that freezes anything it comes in contact with. The three cells are located over Northern Canada, Siberia, and Scotland.

The weather becomes increasingly Violent storm|violent, causing traffic-jammed Manhattan streets to become flooded knee-deep. Jacks son Sam, visiting New York City as he is participating in an academic decathlon, calls his father, promising to be on the next train home, but flooding closes the subways and Grand Central Terminal. As the storm worsens, a massive wave hits Manhattan. Sam and his friends seek shelter with a large group of people in the New York Public Library, but not before his friend and love interest, Laura, gets injured.

President Blake orders the evacuation of the southern states of the United States, causing almost all of the refugees to head to Mexico. Jack and his team set out for Manhattan to find his son. Their truck crashes into another vehicle just past Philadelphia so the group continues on snowshoes.

Most of the group taking shelter in the library leaves when the water outside freezes, leaving just Sam and a few others. They burn books to stay alive and break into a vending machine for food. While journeying to New York, Frank falls through the glass roof of a snow-covered shopping mall. As Jason and Jack try to pull him up, the glass under them continues cracking and Frank sacrifices himself by cutting the rope. Laura appears to have a cold, so Sam comforts her and confesses his feelings for her. In Mexico, Vice President Becker hears from the Secretary of State that President Blakes motorcade was caught in the super storm before it could make it to Mexico causing Vice President Becker to be sworn in as the new President.

The next morning, the group determine that Laura has blood poisoning from the cut on her leg, so Sam and two others search for penicillin in a derelict Russian cargo-ship that drifted inland. The eye of the super storm passes over the city and the three barely return to the library with the medicine in time. During the deep freeze, Jack and Jason take shelter in an abandoned Wendys restaurant.
 subarctic city, completely frozen by reaching  . They radio this to the government-in-exile in Mexico and President Becker orders helicopters flown into New York, finding more survivors. Becker orders search-and-rescue teams to look for other survivors as he gives his first address to the nation. The movie concludes with the astronauts looking down at Earth from the Space Station, showing most of the northern hemisphere covered in ice and snow, with one of the astronauts stating "Look at that....Have you ever seen the air so clear?"

==Cast==
* Dennis Quaid as Professor Jack Hall
* Jake Gyllenhaal as Sam Hall
* Emmy Rossum as Laura Chapman
* Ian Holm as Professor Terry Rapson
* Sela Ward as Dr. Lucy Hall Christopher Britton as Vorsteen
* Arjay Smith as Brian Parks
* Dash Mihok as Jason Evans
* Jay O. Sanders as Frank Harris
* Sasha Roiz as Parker
* Austin Nichols as J.D.
* Adrian Lester as Simon
* Tamlyn Tomita as Janet Tokada
* Glenn Plummer as Luther
* Perry King as President Blake
* Kenneth Welsh as Vice President/President Raymond Becker
* Amy Sloan as Elsa
* Sheila McCarthy as Judith
* Nestor Serrano as Tom Gomez
* Christian Tessier as Aaron

==Production==
 
The film was inspired by The Coming Global Superstorm, a book co-authored by Coast to Coast AM talk radio host Art Bell and Whitley Strieber.  Strieber also wrote the films novelization. The book "The Sixth Winter" written by Douglas Orgill and John Gribbin and published in 1979, follows a similar theme. So does the novel Ice!, by Arnold Federbush, published in 1978.

Shortly before and during the release of the film, members of environmental and political advocacy groups distributed pamphlets to moviegoers describing what they believed to be the possible effects of global warming. Although the film depicts some effects of global warming predicted by scientists, such as rising sea levels, more destructive storms, and disruption of ocean currents and weather patterns, it depicts these events happening much more rapidly and severely than is considered scientifically plausible, and the theory that a "superstorm" will create rapid worldwide climate change does not appear in the scientific literature. When the film was playing in theaters, much criticism was directed at U.S. politicians concerning their rejection of the Kyoto Protocol and climate change. The films scientific adviser was Dr. Michael Molitor, a leading climate change consultant who worked as a negotiator on the Kyoto Protocol.

==Reception==

===Box office===
Over its four-day Memorial Day opening, the film grossed $85,807,341; however, it still ranked #2 for the weekend, behind Shrek 2  s $95,578,365 four-day tally. However, The Day After Tomorrow led the per-theater average chart with a four-day average of $25,053, compared to Shrek 2 s four-day average of $22,633. At the end of its box office run, the film grossed $186,740,799 domestically and $544,272,402 worldwide. 
 Casino Royale.

===Critical reaction===
The Day After Tomorrow generated mixed reviews from both the science and entertainment communities. The online entertainment guide Rotten Tomatoes rated the film at 45%, with an average rating of 5.3/10. The sites general consensus states that it was "A ludicrous popcorn flick filled with clunky dialogues, but spectacular visuals save it from being a total disaster."  Roger Ebert of the Chicago Sun-Times, praised the films special effects, giving the film three stars out of four. Environmental activist and The Guardian columnist George Monbiot called The Day After Tomorrow "a great movie and lousy science." 

In a USA Today editorial by Patrick J. Michaels, who until 2007 was a Research Professor of Environmental Sciences at the University of Virginia and who rejects the scientific evidence for  global warming, Michaels called the film "propaganda," noting, "As a scientist, I bristle when lies dressed up as science are used to influence political discourse."  In a Space Daily editorial by Joseph Gutheinz, a college instructor and retired NASA Office of Inspector General, Senior Special Agent, Gutheinz called the film "a cheap thrill ride, which many weak-minded people will jump on and stay on for the rest of their lives." 

Paleoclimatologist William Hyde of   is to heart transplant surgery", as quoted in New Scientist.

However, Stefan Rahmstorf of the Potsdam Institute for Climate Impact Research, expert for thermohaline ocean circulation and its effects on climate, was impressed how the script writer Jeffrey Nachmanoff was well informed about the science and politics of global climate change after the talk with him at the preview of the film in Berlin.
He stated: "Clearly this is a disaster movie and not a scientific documentary, the film makers have taken a lot of artistic license. But the film presents an opportunity to explain that some of the basic background is right: humans are indeed increasingly changing the climate and this is quite a dangerous experiment, including some risk of abrupt and unforeseen changes. After all - our knowledge of the climate system is still rather limited, and we will probably see some surprises as our experiment with the atmosphere unfolds. Luckily it is extremely unlikely that we will see major ocean circulation changes in the next couple of decades (I’d be just as surprised as Jack Hall if they did occur); at least most scientists think this will only become a more serious risk towards the end of the century. And the consequences would certainly not be as dramatic as the ‘super-storm’ depicted in the movie. Nevertheless, a major change in ocean circulation is a risk with serious and partly unpredictable consequences, which we should avoid. And even without events like ocean circulation changes, climate change is serious enough to demand decisive action.
I think it would be a mistake and not do the film justice if scientists simply dismiss it as nonsense. For what it is, a blockbuster movie that has to earn back 120 M$ production cost, it is probably as good as you can get. For this type of movie for a very broad audience it is actually quite subversive and manages to slip in many thought-provoking things. Im sure people will not confuse the film with reality, they are not stupid - they will know it is a work of fiction. But I hope that it will stir their interest for the subject, and that they might take more notice when real climate change and climate policy will be discussed in future." 
 meteorological phenomena occurring over the course of hours, instead of the possible time frame of several decades or centuries. 

===Awards and nominations===
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Award
! Subject
! Nominee
! Result
|- The Saturn Saturn Awards Saturn Award Best Science Fiction Film
|
| 
|- Saturn Award Best Special Effects Karen E. Goulekas, Neil Corbould, Greg Strause and Remo Balcells
| 
|- British Academy BAFTA Awards Best Visual Effects
| 
|- Visual Effects VES Awards
| 
|- Best Single Visual Effect
| 
|- MTV Movie Awards MTV Movie Best Action Sequence
|"The destruction of Los Angeles"
| 
|- Best Breakthrough Performance Emmy Rossum
| 
|- Irish Film & Television Awards Best International Actor Jake Gyllenhaal
| 
|- Golden Trailer Awards Best Action Film
|
| 
|- Environmental Media Awards Best Film
|
| 
|- Broadcast Music BMI Film Awards Best Music Harald Kloser
| 
|- Golden Reel Golden Reel Awards Best Sound Editing - Effects & Foley Mark P. Stoeckinger, Larry Kemp, Glenn T. Morgan, Alan Rankin, Michael Kamper, Ann Scibelli, Randy Kelley, Harry Cohen, Bob Beher and Craig S. Jaeger
| 
|-
|}

==Criticism== environmental policies of the presidency of George W. Bush. The White House did not respond to requests for comment on the film. Bowles, Scott (May 26, 2004).  . USAToday.com. Retrieved January 12, 2009. 

In response to accusations of insensitivity by including scenes of New York City being devastated less than three years after the September 11 attacks, Emmerich claims that it was necessary to depict the event as a means to showcase the increased unity people now have when facing a disaster, because of 9/11.         

A number of scientists were critical of the scientific aspects of the film:

* Daniel P. Schrag, a paleoclimatologist and professor of Earth and planetary sciences at Harvard University, expressed both support and concern about the film, stating that "On the one hand, Im glad that theres a big-budget movie about something as critical as climate change. On the other, Im concerned that people will see these over-the-top effects and think the whole thing is a joke... We are indeed experimenting with the Earth in a way that hasnt been done for millions of years. But youre not going to see another ice age – at least not like that."

* Marshall Shepherd, a research meteorologist at the NASA Goddard Space Flight Center expressed similar sentiments, stating that "Im heartened that theres a movie addressing real climate issues. But as for the science of the movie, Id give it a D minus or an F. And Id be concerned if the movie was made to advance a political agenda."
 Andrew Weaver, a climatologist at the University of Victoria said, "Its The Towering Inferno of climate science movies, but Im not losing any sleep over a new ice age, because its impossible." 

==Home media==
 
 
The Day After Tomorrow was first released on   versions. A 2-disc "collectors edition" containing production featurettes, two documentaries (a "behind-the-scenes" and another called "The Forces of Destiny"), storyboards and concept sketches was released on May 24, 2005.

The film was released in high-definition video on Blu-ray Disc in North America on October 2, 2007, and in the United Kingdom on April 28, 2008, in full 1080p with a lossless DTS-HD Master Audio track but with few bonus features.

The film made $110 million in DVD sales, bringing its total film gross to $652,771,772. 

==See also==
 

;Historical events cyclonic storm that occurred on March 12–13, 1993, on the East Coast of North America

;Books and literature
* The Coming Global Superstorm, a book on which the movie is based
* Fifty Degrees Below, a Kim Stanley Robinson novel in which greenhouse warming similarly disrupts the Gulf Stream; the rate of cooling is somewhat less exaggerated
*  , a non-fiction book
* Time of the Great Freeze, a novel by Robert Silverberg about a second Ice Age John Christopher about the beginning of a new ice age

;Film
* Ice (1998 film)|Ice, a 1998 film with a similar premise starring Grant Show, Udo Kier, and Eva La Rue 
* 2012 (film)|2012
* Knowing (film)|Knowing
* List of films featuring space stations

;Television
* "The Midnight Sun", an episode of The Twilight Zone in which Earth is rapidly heating
* Superstorm (docudrama)|Superstorm, a 2007 BBC miniseries
* "Two Days Before the Day After Tomorrow", an episode of South Park that parodies the film

==References==
 
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 