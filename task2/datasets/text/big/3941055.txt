Personal Velocity: Three Portraits
{{Infobox film
| name           = Personal Velocity: Three Portraits
| image          = Personal velocity poster.jpg
| caption        = Promotional poster
| director       = Rebecca Miller
| producer       = Alexis Alexanian Caroline Kaplan Jonathan Sehring John Sloss
| writer         = Rebecca Miller
| starring       = Kyra Sedgwick Parker Posey Fairuza Balk
| music          = Michael Rohatyn
| cinematography = Ellen Kuras
| editing        = Sabine Hoffmann
| distributor    = United Artists
| released       =      
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = $125,000
| gross          = $811,299 http://boxofficemojo.com/movies/?id=personalvelocity.htm 
}}
Personal Velocity: Three Portraits is a 2002 American independent film written and directed by Rebecca Miller.

== Plot ==
Personal Velocity is a tale of three women who have reached a turning point in their lives. Delia is a spirited, working-class woman from a small town in New York state who leaves her abusive husband and sets out on a journey to reclaim the power she has lost. Greta is a sharp, spunky editor who is rotten with ambition. To spite the hated unfaithful ways of her father, she has settled into a complacent relationship and is struggling (not too hard) with issues of fidelity to her kind but unexciting husband. Finally Paula, who ran away from home and got pregnant, is now in a relationship she doesnt want. Shes a troubled young woman who takes off on a journey with a hitchhiker after a strange, fateful encounter on a New York street.

==Awards==
Personal Velocity won the Grand Jury Prize for Dramatic Film and the Cinematography Award at the 2002 Sundance Film Festival.

==References==
 

==External links==
*  
*  
* 

 
 
{{succession box
| title= 
| years=2002 The Believer American Splendor}}
 

 

 
 
 
 
 


 