Scary Movie 3
{{Infobox film
| name = Scary Movie 3
| image = Scary-movie-3-poster-3.jpg
| caption = Theatrical poster of Scary Movie 3.
| writer = Craig Mazin Pat Proft
| based on = Characters Created by Shawn Wayans Marlon Wayans Buddy Johnson Phil Beauman Jason Friedberg Aaron Seltzer Kevin Hart Camryn Manheim George Carlin Queen Latifah Pamela Anderson Jenny McCarthy Drew Mikuska Jianna Ballard Denise Richards  David Zucker David Zucker
| editing = Jon Poll
| studio = Dimension Films Brad Grey Pictures
| distributor = Miramax Films
| released =  
| runtime = 85 minutes
| country = United States Canada
| language = English
| music = James L. Venable
| budget = $48 million 
| gross = $220.6 million
}} science fiction horror comedy mystery genres, David Zucker. Scary Movie Wayans family. The Ring, 8 Mile. It is also the first film of two in the series to star Leslie Nielsen. Scary Movie 3 opened to mixed reviews from critics, who praised its consistent humor and satire, but criticized many other aspects such as casting, plot and pacing. The film was a box office success, grossing $220,673,217 worldwide.

==Plot== The Ring). After several odd occurrences, they die. In a farm outside Washington, D.C., widowed farmer and former reverend Tom Logan (Charlie Sheen) and his clumsy brother George (Simon Rex) discover a crop circle, saying "Attack Here!" (Signs (film)|Signs).
 8 Mile). George competes and proves to be actually quite talented, but due to unintentional racist blunders, hes thrown out.

Later, Brenda asks Cindy to keep her company, since she watched the cursed videotape. After playing several pranks on Cindy, she gets the rest of the popcorn in the lounge, when the TV turns on. Brenda fails to turn it off. Tabitha (Marny Eng) climbs out of the well and the TV, Brenda gets into a fight with Tabitha. Tabitha ends up killing Brenda, since Cindy is ignoring the ruckus. George receives a phone call about the death, and Tom meets with Sayaman, who apologizes for the accident involving himself and Toms wife Annie (Denise Richards).
 Orpheus (Eddie Griffin) agree to watch the tape. Shaneequa discovers the hidden image of a lighthouse, and gets into a fight with Tabithas mother. Shaneequa tells Cindy to find the lighthouse to break the curse. When Cindy returns home, she finds Cody watched the tape.
 Edward Moss), The Architect Blockbuster instead, unleashing the curse.

Returning home, Cindy discovers her station has been broadcasting the evil tape for hours, and there have been various sightings of aliens around the world. Worse, Cody is missing. Cindy manages to track him back to the Logan farm, where he has taken refuge with George. Tom orders everybody into the basement for safety, as he, George and Mahalik go outside to fight off the extraterrestrials. The aliens (voiced by Tom Kenny) arrive but reveal they are friendly and have come to stop Tabitha, since they accidentally watched the tape on a broadcast they had intercepted, thinking it was Pootie Tang.

In the basement, Cindy realizes the farms cellar is what is seen on the tape and she finds the well where Tabitha was drowned under the cellar floor. Suddenly, Tabitha appears behind her. A short fight ensues, during which Tabitha takes Cody hostage. Cindy and George appeal to her, offering her a place in their family. Tabitha appears to accept the offer, but then changes back to her monstrous form and advances to Cindy and the others, only to be accidentally knocked back into the well by President Harris. The aliens leave in peace, and Cindy and George get married. Leaving for their honeymoon, they realize they forget to take Cody with them. At first, Cody is about to get hit by Cindys car in an intersection, but she put the brakes this time to prevent it from happening, much to Codys relief. Unfortunately, another car passes by on the intersection and hits Cody.

==Cast==
* Anna Faris as Cindy Campbell
* Simon Rex as George Logan
* Regina Hall as Brenda Meeks
* Charlie Sheen as Tom Logan
* Leslie Nielsen as President Baxter Harris Aunt Shaneequa/The Oracle
* Anthony Anderson as Mahalik Kevin Hart as CJ
* Camryn Manheim as Trooper Champlin The Architect Orpheus
* Pamela Anderson as Becka Kotler
* Jenny McCarthy as Kathy Embry
* Drew Mikuska as Cody
* Denise Richards as Annie Logan
* D. L. Hughley as John Wilson
* Ja Rule as Agent Thompson
* Darrell Hammond as Father Muldoon
* Jeremy Piven as Ross Giggins
* Simon Cowell as Himself Tabitha
* Edward Moss MJ Alien
* Ajay Naidu as Sayaman
* Tom Kenny as Aliens (voice, uncredited)
* Jianna Ballard as Sue Logan

===Rapper cameos===
In "The Rap Battle", several actual rappers assist in the confrontation with the aliens and a subsequent shootout amongst themselves.
* Master P RZA
* Raekwon
* Method Man Redman
* Macy Gray
* U-God
* Fat Joe

==Posters== The Ring where there are two rings with a small circle inside each ring and at the bottom of the poster is an upside down triangle. The header reads "Youd die to see these rings". The third poster spoofed Catch Me If You Can where a woman in her bikini, who is screaming is running away from a blue alien. The header reads "catch her if you can". The fourth and final poster is the original poster as in the first two Scary Movie films where sitting in the back are Orpheus, Oracle and President Baxter Harris and sitting in the front row are Simon Rex, Mahalik, Cindy Campbell and Tom Logan with Annie Logan sitting on his right knee. MJ alien is sitting in the far back and the background spoofing the signs poster. The header reads "Great trilogies come in threes".

==Alternate scenes==
The DVD edition includes a directors audio commentary, several deleted scenes and alternative endings (with optional commentary). A "3.5" special DVD was also released, and contained several more deleted scenes than the original DVD, with an unrated version of the film.
 The Hulk. President Harris tries to hulk out, but ends up soiling his pants. Cindy enters the Logan House, where she is attacked by Tabitha. She is teleported away to Aunt Shaneequa, who teaches her how to defeat Tabitha. Cindy must then confront hundreds of Tabithas. She wins the battle by performing moves from The Matrix and teleports back to the Logan House. The cast then gets into a car with the President, but are horrified to learn that the driver happens to be M. Night Shyamalan.

One of the scenes that appeared on the Extended DVD named Scary Movie 3.5 was part of the unrated feature. After Pamela Anderson and Jenny McCarthy shut off the TV, the two compliment each other on their good looks. Anderson then asks if McCarthy wants her "shaved pussy", but this turns out to be a furless kitten.

In an extended scene, the person who runs Cody down at the end is shown to be Michael Jackson.

==Parodies== The Ring, 8 Mile. Minority Report, The Lord The Others, Air Force One.  http://www.rogerebert.com/reviews/scary-movie-3-2003
 

==Music==
The score for the film was composed by James L. Venable. The original soundtrack was released on October 24, 2003, and features hip hop artists such as Buku Wise, Delinquent Habits, Dame Lee, Kebyar, and others.  Frank Fitzpatrick served as music supervisor for the film and soundtrack. Fitzpatrick produced and co-wrote the majority of original songs used for the feature. 

==Reception== previous installment but $58 million less than the Scary Movie|first.

Reviews were mixed but the consensus was that it was a marked improvement on the previous installment. The film received a 36% rating on Rotten Tomatoes and 49 out of 100 on Metacritic.   This is the first film in the series to be rated PG-13. The first two films were R-rated.

==References==
 

==External links==
 
*  
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 