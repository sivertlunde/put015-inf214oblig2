De l'autre côté du lit
{{Infobox film
| name           = De lautre côté du lit
| image          = De lautre (film).jpg
| caption        = Thatrical release poster
| director       = Pascale Pouzadoux
| producer       = Fidélité Productions
| script         = Pascale Pouzadoux
| starring       = Dany Boon Sophie Marceau
| music          = Éric Neveux
| cinematography = Pierre Gill
| editing        = Sylvie Gadmer
| studio         = Fidélité Films
| distributor    = Mars Distribution
| released       = 10 December 2008 (France)
| runtime        = 93 minutes
| country        = France
| language       = French
| budget         = $9,354,376
| gross = $28,367,035 
}}

De lautre côté du lit ( ) is a 2008 French comedy film directed by Pascale Pouzadoux and starring Sophie Marceau and Dany Boon. Adapted from the novel of the same name by Alix Girod de lAin, the film is about a husband and wife who decide to exchange their lives for a year in order to save their marriage.    De lautre côté du lit was filmed on location in Paris.   

==Plot==
When routine sets into Hugo and Arianes relationship after ten years of marriage, the couple decides to swap lives. Hugo looks after the house and kids and takes up his wifes career as a door-to-door jewelry salesman, and Ariane assumes control of a building rental company.

==Cast==
* Sophie Marceau as Ariane
* Dany Boon as Hugo
* Roland Giraud as Nicard
* Antoine Duléry as Maurice
* Anny Duperey as Lise
* Juliette Arnaud as Charlotte
* Béatrice Michel as Isabelle
* Ninon Mauger as Louise
* Clémot Couture as Hector
* Flanvart Francois Vincentelli as Nicolas
* Delphine Rivière as Samia
* Arnaud Lemaire as Kévin, Samias friend
* Arsène Mosca as Goncalvo
* Armelle as Schools director   

==References==
 

==External links==
*  
*  
*   at uniFrance

 
 
 
 
 
 
 
 