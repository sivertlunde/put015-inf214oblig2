The Butterfly Effect
 
{{Infobox film
| name           = The Butterfly Effect
| image          = Butterflyeffect poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = {{Plainlist|
* Eric Bress
* J. Mackye Gruber
}}
| producer       = {{Plainlist|
* Anthony Rhulen Chris Bender
* Ashton Kutcher JC Spink
* A.J. Dix
}}
| writer         = {{Plainlist|
* Eric Bress
* J. Mackye Gruber
}}
| starring       = {{Plainlist|
* Ashton Kutcher
* John Patrick Amedori
* Amy Smart
* Elden Henson
* William Lee Scott
* Eric Stoltz
* Ethan Suplee
* Logan Lerman
* Melora Walters
}}
| music          = Michael Suby
| cinematography = Matthew F. Leonetti
| editing        = Peter Amundson Katalyst
| distributor    = New Line Cinema 
| released       =  
| runtime        = 114 minutes 120 minutes  
| country        = United States
| language       = English
| budget         = $13 million   
| gross          = $96,060,858 
}}
The Butterfly Effect is a 2004 American psychological thriller film that was written and directed by Eric Bress and J. Mackye Gruber, starring Ashton Kutcher and Amy Smart. The title refers to the butterfly effect, a popular hypothetical example of chaos theory which illustrates how small initial differences may lead to large unforeseen consequences over time.
 sadistic brother Tommy, and Elden Henson as their neighbor Lenny. Evan finds he has the ability to travel back in time to inhabit his former self (that is, his adult mind inhabits his younger body) and to change the present by changing his past behaviors. Having been the victim of several childhood traumas aggravated by stress-induced memory losses, he attempts to set things right for himself and his friends, but there are unintended consequences for all. The film draws heavily on flashbacks of the characters lives at ages 7 and 13, and presents several alternate present-day outcomes as Evan attempts to change the past, before settling on a final outcome.

The film received a poor critical reception, but was nevertheless a commercial success, producing gross earnings of $96 million from a budget of $13 million. The film won the Pegasus Audience Award at the  .

==Plot==
 

Evan Treborn frequently suffers from Amnesia|blackouts, often at moments of high Stress (medicine)|stress. As a young child (played by Logan Lerman) and adolescent (played by John Patrick Amedori), Evan suffered many severe sexual and psychological traumas. These traumas include being coerced to take part in child pornography by neighbor George Miller (Eric Stoltz) (father of Kayleigh and Tommy); being nearly strangled to death by his institutionalized, mentally estranged father, Jason Treborn (Callum Keith Rennie), who is then killed in front of him by guards; accidentally killing a mother and her infant daughter while playing with dynamite with his friends; and seeing his dog being burned alive by Tommy.
 travel back in time and is able to redo parts of his past. His time traveling episodes account for the frequent blackouts he experienced as a child. However, there are consequences to his revised choices of early actions that propagate forward in time to his present life. For example, editing his personal time-line leads to alternative futures in which he finds himself, variously, a college student in a fraternity, an inmate imprisoned for murdering Tommy, and an amputee. His efforts are driven by the desire to undo the most unpleasant events of his childhood which coincide with his mysterious blackouts, and as he continues to do this, he realizes that, even though his intentions are good, his actions have unforeseen consequences. Moreover, the assimilation of dozens of years worth of new memories from the alternative timelines causes him brain damage and severe nosebleeds. Ultimately, he decides that his attempts to alter the past end up only harming those he cares about, and thus reaches the conclusion that the main cause of everyones suffering in all the different timelines is himself.

Evan purposely travels back in time one final time to the day he first meets Kayleigh as a child. He intentionally upsets her so that she will choose to live with her mother, in a different neighborhood, instead of with her father when they divorce. She and her brother are never subjected to a destructive upbringing and go on to be successful in life, at the cost of Evans friendship with her.

Eight years later in New York City, an adult Evan passes by Kayleigh on the street. Though a brief look of recognition passes over Kayleighs face, it quickly fades as she walks away without talking to Evan.
 

==Cast==
* Ashton Kutcher as Evan Treborn
** John Patrick Amedori as Evan,  age 13 
** Logan Lerman as Evan,  age 7 
* Amy Smart as Kayleigh Miller
** Irene Gorovaia as Kayleigh,  age 13 
** Sarah Widdows as Kayleigh,  age 7 
* Elden Henson as Lenny Kagan Kevin G. Schmidt as Lenny,  age 13 
** Jake Kaese as Lenny,  age 7 
* William Lee Scott as Tommy "Tom" Miller Jesse James as Tommy,  age 14 
** Cameron Bright as Tommy,  age 8 
* Melora Walters as Andrea Treborn
* Eric Stoltz as George Miller
* Ethan Suplee as Thumper
* Kevin Durand as Carlos
* Callum Keith Rennie as Jason Treborn
* Lorena Gale as Mrs. Boswell
* Nathaniel DeVeaux as Dr. Redfield Tara Wilson as Heidi
* Jesse Hutch as Spencer
* Jacqueline Stewart as Gwen

==Reception==
===Critical reception===
Critical reception for The Butterfly Effect was generally poor. On  , another review aggregator, it has a score of 30 out of 100, indicating "generally unfavorable reviews". 
 prison sex, too) prevent Effect from becoming the unintentional howler it might otherwise be."  Additionally, Ty Burr of The Boston Globe went as far as saying, "whatever train-wreck pleasures you might locate here are spoiled by the vile acts the characters commit." 

Some critics enjoyed the movie. Matt Soergel, of The Florida Times-Union, rated it 3 stars out of 4, writing, "The Butterfly Effect is preposterous, feverish, creepy and stars Ashton Kutcher in a dramatic role. Its a blast... a solidly entertaining B-movie. Its even quite funny at times..."  The Miami Herald said, "The Butterfly Effect is better than you might expect despite its awkward, slow beginning, drawing you in gradually and paying off in surprisingly effective and bittersweet ways," and added that Kutcher is "appealing and believable... The Butterfly Effect sticks to its rules fairly well... overall the film is consistent in its flights of fancy."  The Worcester Telegram & Gazette praised it as "a disturbing film" and "the first really interesting film of 2004," adding that Kutcher "carries it off":  

===Box office===
Despite the critical failure, the film was a commercial success, earning $17,065,227 and claiming the #1 spot in its opening weekend.  Against a $13 million budget, The Butterfly Effect grossed around $57,938,693 at the U.S. box office and $96,060,858 worldwide. 

===Awards and nominations===
;2004 Academy of Science Fiction, Fantasy & Horror Films (Saturn Awards) Best Science Fiction Film - nominated

;2004 Brussels International Festival of Fantasy Film
* Pegasus Audience Award — Eric Bress, J. Mackye Gruber - won

;2004 Teen Choice Awards
* Choice Movie: Thriller - nominated 

==Home media==

===Release=== documentaries ("The filmmaker commentary deleted and alternate scenes, and a short feature called "The Creative Process" among other things.  

===Alternate endings===
The Butterfly Effect has four different endings that were shot for the film:
#The theatrical release ending shows Evan passing Kayleigh on the sidewalk, he sees her, and recognizes her, but keeps walking.
#The "happy ending" alternate ending shows Evan and Kayleigh stopping on the sidewalk when they cross paths. They introduce themselves and Evan asks her out for coffee. 
#The "open-ended" alternate ending is similar to the one where Evan and Kayleigh pass each other on the sidewalk and keep walking, except this time Evan, after hesitating, turns and follows Kayleigh.  This ending was utilized in the films novelization, written by James Swallow and published by Black Flame.
#The "directors cut" alternate ending shows Evan turning on the home movies, only this time instead of watching a home movie at a neighborhood gathering, hes watching the video of his own birth. He travels back to when he is about to be born and commits suicide by strangling himself with his own umbilical cord. Therefore he was never there to change the timeline in the first place and this explains why Evans mother had two still-born children before him.

==Sequels==
The Butterfly Effect 2 was released on DVD on October 10, 2006. It was directed by John R. Leonetti and was largely unrelated to the original film. It features a brief reference to the first film in the form of a newspaper headline referring to Evans father, as well as using the same basic time travel mechanics.

The third installment in the series,  , was released by After Dark Films in 2009. This sequel follows the life of a young man who journeys back in time in order to solve the mystery surrounding his high school girlfriends death. This film has no direct relation to the first two and uses different time travel mechanics.

== Allusions == influential story, "A Sound of Thunder", appears on a pennant reading "Bradbury" in Evans dorm room.

==See also==
* List of films featuring time loops

==References==
 

==External links==
 
*   ( )
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 