Anna Karenina (1935 film)
{{Infobox film
| name           = Anna Karenina
| image          = Anna Karenina 1935 poster.jpg
| caption        = 1935 German Theatrical Poster
| director       = Clarence Brown
| producer       = David O. Selznick
| writer         = S. N. Behrman  Clemence Dane  Salka Viertel Leo Tolstoy (novel)
| starring       = Greta Garbo  Fredric March  Maureen OSullivan Freddie Bartholomew  Basil Rathbone  Reginald Owen
| music          = Herbert Stothart
| cinematography = William H. Daniels
| editing        = Robert Kern
| distributor    = Metro-Goldwyn-Mayer
| released       =   }}
| runtime        = 95&nbsp;minutes
| country        = United States
| language       = English
| budget         =
}}
 film adaptations of the novel.
 Capitol Theatre, New York Film Critics Circle Award for Best Actress for her role as Anna. In addition, the film was ranked #42 on the American Film Institutes list of AFIs 100 Years... 100 Passions.

==Plot summary==
 
Anna Karenina (Greta Garbo) is the wife of Czarist official Karenin (Basil Rathbone). While she tries to persuade her brother Stiva (Reginald Owen) from a life of debauchery, she becomes infatuated with dashing military officer Count Vronsky (Fredric March). This indiscreet liaison ruins her marriage and position in 19th century Russian society; she is even prohibited from seeing her own son Sergei (Freddie Bartholomew), with eventual dire results. 

==Cast==
* Greta Garbo — Anna Karenina
* Fredric March — Count Vronsky
* Freddie Bartholomew — Sergei
* Maureen OSullivan — Kitty
* May Robson — Countess Vronsky
* Basil Rathbone — Karenin
* Reginald Owen — Stiva Reginald Denny — Yashvin
* Gyles Isham - Levin
* Joan Marsh — Lili
* Ethel Griffies — Mme. Kartasov
* Harry Beresford — Matve
* Mary Forbes — Princess Sorokina
* Constance Collier — Countess Lidia (uncredited)

==Production==

 

==Notes==
Garbo also was the lead in the 1927 version of Anna Karenina, released under the title Love (1927 film)|Love.

==References==
 

==External links==
 
*  
*  
*   at Allmovie
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 