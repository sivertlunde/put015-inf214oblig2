Chō Gekijōban Keroro Gunsō 2: Shinkai no Princess de Arimasu!
 
{{Infobox film
| name           = Chō Gekijōban Keroro Gunsō 2: Shinkai no Princess de Arimasu!
| image          = Keroro Movie 2.jpg
| director =  Junichi Sato (chief) Susumu Yamaguchi
| producer = 
| writer = Masahiro Yokotani
| starring = 
| music = 
| cinematography = 
| editing = 
| released       =  
| runtime        = 77 minutes
| country        = Japan
| language       = Japanese
| gross          = $3,877,862 
}}

  is a 2007 comedy, adventure film based on the anime and manga series Sgt. Frog. It is an adaptation of Volume 20 of the manga series.

==Summary== Keroro suffers Koyuki and Angol Mois came along. After a day of fun, Keroro reveals that he is planning to conquer Earth with the help of Kirurus overcomers, much to Giroros surprise. That night, the group finishes dinner as Natsumi calls her mother until she was attacked by a giant crab that kidnaps her and summons creatures with the same energy as those who defeated Kiruru. Keroro accidentally uses the Kero Ball to rocket himself in the ocean. The team leaves to travel through in a submarine.

Natsumi suffers a dream of herself as a lonely child until awakened to discover an alien named Maru who presents to Natsumi Prince Meru, who reveals Natsumi is to be his princess and that they had captured Keroro, who pleads to them to assist the Keroro Platoon, only to be kicked out. Meru uses a Kero Ball-like device called the Mer Ball to form Natsumis favorite town. Discovered and rescued by the team, Keroro uses the added brain power from the peaches to develop a strategy. After touring her kingdom, Natsumi, Meru, and Maru visit a depot to play but Meru got lost so Maru and Natsumi went to search for him. Meru is whimpering alone in the elevator, unable to get out but he was freed by Natsumi and suddenly hugged her tightly while crying, where Natsumi discovers to have the same event as a child. When visiting the upstairs carnival, Natsumi and Meru discover the arrival of the Keroro Platoon that offers other princesses until Giroro starts a fight, revealing Maru is a very skilled fighter and defeated Giroro that he is captured in as the platoon escapes as Giroro notices the Mer Ball. With no way of escape, the Keroro Platoon take shelter in the depot.

After a long analysis, Kululu concludes that they are Maronians, similar race to the Keronians, are flooding the Earth and that this world is formed from Natsumis dreams of her lonely childhood at this depot. As Meru harasses Giroro, Natsumi notices Marus feelings for Meru and states that she should be princess. That entertaining night of dinner and Merus harassment in Giroros princess costume, Natsumi and Meru stroll around the ocean, where Meru sings a lullaby from his forgotten childhood as he pleads to Natsumi to marry him, leaving him angered to know everyone ignoring him until Natsumi accepts and joins Meru in recording data of Maron and returning to marry Meru, who strikes down an attacking Giroro. With the use of new equipment, the platoon infiltrates the castle and destroys all decoys, including a possessed Giroro. Revealing she has no memory of Maron, Maru takes Keroro and Fuyuki to a corrupted Meru using Natsumi as a power source so that he can create a new Maron from Earth.

With their forces futile, the Keroro Platoon watches as Keroro jumps to save Natsumi, who remembers how she was lost as a child at the depot until found by Aki and Fuyuki, bringing her to as she is pried from the corrupted Meru, who loses control of his power and begins to absorb the dream world and the defiant Maru. Through teamwork, the platoon free, Meru, Maru, Natsumi, Keroro, and Fuyuki and dispose of the corrupted Mer Ball, only to destroy the world as Natsumi pleads to fulfill her promise to Aki to return home, forming a replication of the Hinata house, where Keroro suffers a near-death scare. The platoon returns safely to the surface, where Meru and Maru realise their feelings for each other as Natsumi passes her title of princess to Maru. Wishing their friends farewell, the couple leaves in a ship found by Kululu to take them to Maron to marry. The vacation ends as the platoon happily and safely return home as Meru and Maru arrive at Maron, standing together in love.

==Chibi Kero: Secret of the Kero Ball!?==
In addition to the feature film, a CGI animated short is played entitled  .

A young Keroro tries to impress Pururu by showing off the Kero Ball, a ball borrowed by Giroro from his brother Garuru. While testing the buttons, he accidentally transports the 4 tadpoles inside the ball. To get out, they must win the pinball maze.  

In the outside world, a young blue tadpole Kululu picks up the ball and hears the cries of help. He makes a deal to free those trapped inside if he gets a plate of curry rice.

A deal is a deal and with the Kero Ball, a plate of curry rice is produced for Kululu. Unfortunately, the plate grows bigger and bigger and bigger. It got so big, that Kululu was immersed in the curry. The yellow curry slowly transforms the blue Kululu into the "yellow devil" and as the film is about to fade into black, "Kukuku" is heard.

: 

==Cast==
{| class="wikitable" 
|-
!Character 
!Voice actor 
 |- Keroro
|Kumiko Watanabe
|-
!Private Second Class Tamama Etsuko Kozakura
|-
!Corporal Giroro
|Jōji Nakata
|-
!Sergeant Major Kululu Takehito Koyasu
|- Dororo
|Takeshi Kusao
|-
!Fuyuki Hinata Tomoko Kawakami
|-
!Natsumi Hinata Chiwa Saitō
|-
!Aki Hinata Akiko Hiramatsu
|-
!Saburo Akira Ishida
|-
!Momoka Nishizawa Haruna Ikezawa
|-
!Angol Mois Mamiko Noto
|-
!Koyuki Azumaya
|Ryō Hirohashi
|-
!Meru
|Hōko Kuwashima
|-
!Maru Nozomi Tsuji
|-
!Kiruru Yoshinori Fujita
|-
!Pururu (Secret of the Kero Ball) Satsuki Yukino
|-
!Narration Paul Moriyama Keiji Fujiwara
|}

==References==
 

 

 
 
 