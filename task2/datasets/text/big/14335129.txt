Back to the Woods (1918 film)
 
{{Infobox film
| name           = Back to the Woods
| caption        = Film poster
| image	         = Back to the Woods FilmPoster.jpeg
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film featuring Harold Lloyd. It was produced by Goldwyn Pictures when it and many other early film studios in Americas first motion picture industry were based in Fort Lee, New Jersey at the beginning of the 20th century.    A print of the film survives in the Archiva Nationala de Filme film archive.   

==Cast==
* Harold Lloyd as A Millionaire
* Snub Pollard as His Valet
* Bebe Daniels as Jeanne, Belle o the Woods
* Arthur Housman
* Bud Jamison
* T. Henderson Murray

==See also==
* List of American films of 1918
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 