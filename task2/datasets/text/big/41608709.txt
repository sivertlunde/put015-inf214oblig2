Karaiyellam Shenbagapoo
{{Infobox film
| name = Karaiyellam Shenbagapoo
| image = KaraiyellamShenbagapoo.jpg
| caption = LP Vinyl Records Cover
| director = G.N.Rangarajan Sujatha
| Prathap Pothan Manorama
| producer = C. Shanmuga Sundaram for Sundari Art Creations
| music = Illayaraja
| cinematography = N. K. Viswanathan
| editing        = K. R. Ramalingam
| studio         = Sundari Art Creations
| distributor    = Sundari Art Creations
| released       =  
| runtime =
| country        = India Tamil
| budget =
}}
 directed by Prathap Pothan and Sripriya in lead roles.     The film, had musical score by Ilaiyaraaja and was released on 14 August 1981.   The movie is based on Sujathas novel Karaiyellam Shenbaga Poo.

==Cast==
 Prathap Pothan
*Sripriya
*Sumalatha
*Sundar Manorama
*Pushpa
*K. A. Thangavelu

==Soundtrack ==
{{Infobox album Name     = Karaiyellam Shenbagapoo Type     = film Cover    = Released =  Music    = Illayaraja Genre  Feature film soundtrack Length   = 20:18 Label    = EMI-The Gramaphone Company of India Limited
}}

The music composed by Illayaraja.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics ||
|-
| 1 || Yerupudichavare... || Malaysia Vasudevan, P. Susheela || Gangai Amaran || 04:24
|-
| 2 || Yeriyile... || S. Janaki, Illayaraja & Chorus || Panchu Arunachalam || 04:46
|-
| 3 || Kadellaam... || Illayaraja & Chorus || Panchu Arunachalam || 04:34
|-
| 4 || Kalyanraman... || SP Balasubramanyam, S. Janaki || Gangai Amaran ||  04:25
|-
| 5 || Villu Paattu || Chorus || Gangai Amaran || 03:21
|-
|}

==References==
 

==External links==

 
 
 
 
 


 