The Legend of Pancho Barnes and the Happy Bottom Riding Club
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Legend of Pancho Barnes and the Happy Bottom Riding Club
| image          = 
| alt            =  
| caption        = Pilot. Adventurer. Legend.
| director       = Amanda Pope
| producer       = Nick T. Spark
| story          = Nick T. Spark
| narrator       = Tom Skerritt
| starring       = Buzz Aldrin Robert Cardenas Bob Hoover Lauren Kessler Barbara Schultz Chuck Yeager
| music          = Nathan Wang Knox Summerour
| cinematography = Clay Westervelt
| editing        = Monique Zavistovski
| studio         = 
| distributor    = Nick Spark Productions LLC
| released       =  
| runtime        = 64 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}}
The Legend of Pancho Barnes and the Happy Bottom Riding Club is a 2009 documentary film that chronicles the life of aviation pioneer Florence Lowe "Pancho" Barnes.

==Production==
The Legend of Pancho Barnes and the Happy Bottom Riding Club was made on a non-profit basis through the Orange County PBS station KOCE-TV. It was written and produced by Nick T. Spark and directed by University of Southern California professor Amanda Pope. The film features interviews with many of Pancho Barnes surviving friends including astronaut Buzz Aldrin and test pilots Robert Cardenas, Bob Hoover and Chuck Yeager. Biographers Lauren Kessler and Barbara Schultz also appear in the documentary. The film is narrated by Tom Skerritt, with Kathy Bates as the voice of Pancho Barnes.

==Distribution and awards==
The film premiered at the NewFest film festival in Manhattan in June, 2009, and was subsequently shown at the American Cinematheque, the Los Angeles International Womens Film Festival, the San Francisco Womens Film Festival, the Hot Springs Documentary Film Festival and the San Luis Obispo Film Festival, where it won the Audience Award.  It also won the Audience Award at the Big Muddy Film Festival  and Best Documentary Film at the Los Angeles Womens Film Festival. 

The documentary won the 2010 L.A. Area Emmy Award in the Arts & Culture / History category. 

A shortened, broadcast version of The Legend of Pancho Barnes and the Happy Bottom Riding Club is currently being distributed by American Public Television  and will be seen on over 100 public television stations in the United States of America.

The film was also shown at the Experimental Aircraft Associations AirVenture, the AOPA National Convention, the Northwest EAA Fly-In, Sun N Fun and dozens of air museums across the United States.

==Home media release==
The documentary was released on DVD in October, 2009. The DVD features both the broadcast (57 minute) version of the film and an extended version (64 minute), as well as deleted scenes.

==References==
 

== External links ==
*  
*  

 
 
 