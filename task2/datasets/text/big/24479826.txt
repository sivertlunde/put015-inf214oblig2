Full Blast (1999 film)
{{Infobox Film
| name           = Full Blast
| image          = Full-blast-rodrigue-jean.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Rodrigue Jean
| writer         = Nathalie Loubeyre  Adapted from the novel LEnnemi que je connais by Martin Pitre 
| starring       = David La Haye Martin Desgagné
| producer       = Ian Boyd
| music          = Robert-Marcel Lepage
| cinematography = Stefan Ivanov
| editing        = Mathieu Bouchard-Malo
| distributor    = K-Films Amérique
| released       = 1999
| runtime        = 95 minutes
| country        = Canada French
| awards         = 
| gross          = 
}}

Full Blast is a 1999 film by Canadian director Rodrigue Jean, his first long feature. The film was written by Nathalie Loubeyre as an adaptation of Martin Pitres Acadian novel LEnnemi que je connais. Filmed in Bathurst, New Brunswick|Bathurst, New Brunswick, Canada, it was tipped as the first-ever New Brunswick Acadian movie. 

==Cast==
*David La Haye : Steph
*Martin Desgagné : Piston
*Louise Portal : Rose
*Marie-Jo Therio : Marie-Lou
*Patrice Godin : Charles
*Daniel Desjardins : Chico
*Luc Proulx : Stephs father
*Danica Arsenault : Juliette

==Plot==
A strike at a sawmill in a small fictional community in New Brunswick puts Steph (David La Haye) and Piston (Martin Desgagné) out of work. They want to resurrect their band Lost Tribe but Marie-Lou (Marie-Jo Therio), Pistons ex-wife and the bands former lead singer, is not enthusiastic about the idea.

Meanwhile, Steph is having relationship trouble with Rose (Louise Portal), an older woman that hes been seeing and drifts first to Marie-Lou and then to Charles (Patrice Godin), who once left town but is now back.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 