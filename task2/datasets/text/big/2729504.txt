Nighthawks (film)
{{Multiple issues|
 
 
 
}}
{{Infobox film
| name           = Nighthawks
| image          = Nighthawks movie.jpg
| caption        = Theatrical release poster
| director       = Bruce Malmuth
| producer       = Martin Poll
| screenplay     = David Shaber
| story     = {{plainlist|
* David Shaber
* Paul Sylbert
}}
| starring       = {{plainlist|
* Sylvester Stallone
* Billy Dee Williams
* Rutger Hauer
* Lindsay Wagner
* Persis Khambatta
* Nigel Davenport
}}
| music          = Keith Emerson
| cinematography = James A. Contner
| editing        = {{plainlist|
* Christopher Holmes
* Stanford C. Allen
}}
| studio         = {{plainlist|
* Martin Poll Productions
* The Production Company
}} Universal Pictures
| released       =  
| runtime       = 99 minutes
| country       = United States http://www.afi.com/members/catalog/DetailView.aspx?s=&Movie=56666  
| language      = English
| budget        = $5 million  
| gross         = $19.9 million 
}}
Nighthawks is a 1981 American thriller film directed by Bruce Malmuth and starring Sylvester Stallone, Rutger Hauer, Billy Dee Williams, Lindsay Wagner, Persis Khambatta and Nigel Davenport. The original music score was composed by Keith Emerson.

Nighthawks is known for its production problems, re-editing of the film by Universal Studios and Stallone for different reasons,  and the poor working relationship between Stallone and Hauer.

== Plot == NYPD police terrorist Heymar Reinhardt, alias Wulfgar (Rutger Hauer) bombs a department store. Meanwhile, back in New York City, DaSilva and Fox serve a high-risk warrant in the Bronx. They raid a known drug distribution spot, where they discover corrupt police officers amongst the dealers.
 aerial tram and confronts Wulfgar face-to-face. DaSilva demands to know why Wulfgar killed the woman.

The police agree to Wulfgars demands for a MTA New York City Transit bus to escort him  and the hostages to the airport, where a jet will be waiting for takeoff. Wulfgar and Shakka hide among the crowd of hostages from the tram. DaSilva waits until they try to board before making his move. He plays back a recording of Hartmans lecture in which the terrorist expert denounces Shakka. In a rage, Shakka breaks from the hostages and is gunned down by Fox. Wulfgar escapes by driving the bus off a ramp into the East River.

A search of the wreckage shows no sign of Wulfgar. The team unearths the store where Wulfgar has been staying and DaSilva finds that Wulfgar has gathered information on all participating members including Fox, Hartman, and DaSilva: Irenes address is found on one of the printouts. Wulfgar watches Irene walk up to her house and go in. He breaks in, finds her washing dishes and sneaks up behind her brandishing a knife. But DaSilva has made it to Irenes first; he turns around, wearing his ex-wifes housecoat and a blonde wig, brandishing a gun. With nowhere to go, Wulfgar lunges at DaSilva who fires his revolver twice into the terrorist, sending his dead body crashing back into the street.

== Cast ==
* Sylvester Stallone as Detective Sergeant Deke DaSilva
* Billy Dee Williams as Detective Sergeant Matthew Fox
* Lindsay Wagner as Irene DaSilva
* Persis Khambatta as Shakka Holland/Shakka Kapoor
* Nigel Davenport as Peter Hartman
* Rutger Hauer as Heymar "Wulfgar" Reinhardt/Eric
* Hilary Thompson as Pam
* Joe Spinell as Lieutenant Munafo Walter Mathews as Commissioner
* E. Brian Dean as Sergeant
* Caesar Cordova as Puerto Rican Proprietor
* Charles Duval as Dr. Ghiselin
* Tony Munafo as Big Mike
* Howard Stein as Disco Manager
* Tawn Christian as Disco Hostess
* Jamie Gillis as Designer
* Catherine Mary Stewart as London shop assistant
* Clebert Ford as Nigerian Ambassador 
* Brian Osborne as Orchard (London) 
* Jean-Pierre Stewart as Rene Marigny 
* Catherine Mary Stewart as Sales Girl (London)  
* Patrick Fox as Reporter  Edward Fox as A.T.A.C. Man

== Production ==
The story was originally planned as The French Connection III by screenwriter David Shaber at Twentieth Century Fox, and would have seen Gene Hackmans Popeye Doyle team up with a wisecracking cop, to be possibly played by Richard Pryor. The main plot was the same but when Hackman showed reluctance to do a third film as Doyle the idea was scrapped and Universal acquired the rights to the storyline, which Shaber then reworked into Nighthawks.

Originally the script was based on a real character – the international terrorist Carlos the Jackal, which is why there are some similarities between the things that main villain from the film terrorist Wulfgar does and real life events connected to Carlos.

The film had two working titles: Attack and Hawks. Pre-production began in 1979. Principal photography took place from January 1980 when the final draft of the script was completed and production ended in April 1980. Because of the numerous problems with first Stallones and later studios interference in post production, film was heavily re-edited many times, losing lot of the scenes and plot in process, and was eventually released a year after it was finished.
 director was Gary Nelson, Directors Guild of America (DGA) has the rule "anyone signed to work on a movie before the director was engaged cannot replace a fired director, except in an emergency", so this was taken to arbitration which resulted in a fine. 

In preparation for their roles as New York street cops, actors Billy Dee Williams and Sylvester Stallone spent a number of weeks working at night with the New York Street Crime Unit. Stallone reportedly turned down several other big roles to do Nighthawks because he said the material resonated with him.

Nighthawks marked the American debut of Dutch actor Rutger Hauer.

Before accepting to play Wulfgars character, Hauer found himself faced with a choice. He had been offered to act in The Sphinx, which was – contrary to Nighthawks – a big production, made by a major studio, and they were also offering him twice the salary he got for Nighthawks, in addition to a well-known director to work with. But he chose Wulfgars role. http://www.rutgerhauer.org/plots/night.php 

Hauer lost both his mother and his best friend during the production of the film. He returned to his native Netherlands for both their respective funerals, but returned to the production each time within a few days. Despite all the personal drama and all the difficulties on the set, Hauer stated in his autobiography that he was happy he stayed aboard, as this film caused him to be noticed in Hollywood, and started an impressive international career.

First scene that Hauer had to film was his death scene that happens in ending of the film. While filming the scene, Hauer was injured two different times. In one instance a squib meant to simulate a gunshot wound exploded on the wrong side and severely burned him. In other, a cable that would yank him to simulate the force of being shot was pulled too hard, straining his back. Afterward, Hauer discovered that the cable was pulled with such force on Stallones orders. This was the last straw for Hauer, who then threatened Stallone that hes gonna "Break his balls" if he ever does something like that again; their working relationship afterward was marked by numerous arguments.
 American star, who is known for his competitive streak (see also Escape to Victory|Victory).

Although stories about their on-set fights are still talked about amongst fans of the film and both actors, Hauer has said in recent interviews that he actually didnt take their arguments personally and that biggest problem during filming was that it was very difficult film to make. 

In another, more recent interview, Hauer said that he and Stallone actually met somewhere after a long time and had a conversation. Hauer  said that if Stallone called him for some Expendables sequel he would probably refuse but not because of their past but just because he is not interested in doing something like that. 

Stallone himself has mentioned in several interviews in later years that Hauers character and his performance in the film is one of the best things about it. In 1993 interview about films which he has done till then he said about Nighthawks:

:"It was a little bit ahead of its time in that I was dealing with urban terrorism. Now, with the World Trade Center, its happening. At the time, people couldnt relate to it, and the studio   didnt believe in it. Rutger Hauers performance held it together — he was an excellent villain."   

Hauer also always thought that with "Nighthawks" they missed a good opportunity. According to him, the issue of international terrorism could have been handled more accurately. Instead, "We had only to play tags - the written story was much more dangerous". 

The subway train used in the chase sequence consisted of retired IND equipment that had been preserved as a museum train.  Of the cars whose numbers are visible, 800 is now at the Seashore Trolley Museum in Kennebunkport, Maine.  1802, the last prewar NYC subway car built, is owned by Railway Preservation Corporation and remains in New York, where it operates several times a year on museum fantrips along with other preserved cars.  1208 has since been scrapped.  The IND Hoyt-Schermerhorn station in Brooklyn served as both the 57th and 42nd St. stations (a Hoyt-Schermerhorn sign is briefly visible when Stallone tries to pry the doors open as the train is pulling out).  The train operated on the unused outer track that leads from the Court St. station, now the New York Transit Museum.

The London department store blown up at the beginning of the film was actually Arding and Hobbs, located at Clapham Junction, SW11 - which at the time of filming belonged to the Allders group. The store is now owned by Debenhams. During the 2011 England Riots the shop again had its windows smashed.

== Stunts ==

The films stunt coordinator was Dar Robinson. 
 producers headaches by insisting on doing his own stunts. According to actor Nigel Davenport in an interview for the BBCs Wogan show, Stallone performed the scene where he was winched up to the Roosevelt Island Tram without a double. In a Q&A session on Aint It Cool News   website Stallone said about the stunts in the film:

 The stunts in the film were pretty extraordinary because they were invented along the way. Running through the tunnels of an un-built subway station was very dangerous, but exciting and we were only given one hour to do it. So that made for an interesting evening. Hanging from the cable car was probably one of the more dangerous stunts I was asked to perform because it was untested and I was asked to hold a folding Gerber knife in my left hand so if the cable were to snap, and I survived the 230 foot fall into the East River with its ice cold 8 mile an hour current, I could cut myself free from the harness because the cable when stretched out weighed more than 300 lbs. I tell you this because its so stupid to believe that I would survive hitting the water so to go beyond that is absurd. So I actually thought the smart move would be to commit hari-kari on the way down and let the cards fold as they may. P.S. Several years later this cable did snap while testing it on a 100lb bag of sand.    

In interview with Roger Ebert in 1980, Stallone mentioned some problems with stunts he wanted to perform himself in Nighthawks. For the scene where his character jumps onto a moving subway train and kicks out the window the glass was wire-reinforced and would take a good kick. Stallone insisted on using the real glass all the same. But when he braced and kicked the glass, it fell out at a touch, throwing him off balance and almost off the train. Due to a lifelong fear of heights, Stallone said of the helicopter stunt, "Ive never been so scared in my life".  In same interview, Stallone also said that he spent 15 weeks in almost total seclusion in his hotel room between scenes and that those were the most stressful moments of his life. 

== Studio Interference, Censorship, Original Version and Deleted Scenes ==

Prior to its theatrical release, Nighthawks was severely cut for violence by both Universal studio and the MPAA. Amongst the cut scenes were a longer disco shootout which had Wulfgar shooting and killing more people, and Wufgars death scene was almost completely omitted. In the uncut scene Wulfgar was shot five times (instead of twice) in slow-motion by DaSilva, with the sixth and final shot hitting him in the head, blowing his brains out. An animatronic cast of Rutger Hauers head was made by special makeup artist Dick Smith and used in this scene. Although no uncut version of the film exists, the soundtrack release for the film included a track called "Face To Face" which was meant for this ending.

In the same year, some other films also suffered heavy cuts on graphic an gory scenes because of the backlash on film violence at that time. Most infamously, two cult horror films My Bloody Valentine (film) and Friday the 13th Part 2 were very severely cut down after numerous submissions to the MPAA which kept giving the films an X rating.

Reportedly, the original cut of the film was just short of 2 1/2 hours long.  Numerous scenes between Stallone and Wagner, as well as Rutger Hauer and Persis Khambatta, which fleshed out both characters and plot were deleted because Universal wanted a fast-paced action film. Stallone also had a hand in re-editing of the film. He was jealous of Hauer, whose performance he felt dominated the film. Two versions of the film were shown to test audiences, one with more emphasis on Stallones character and the other on Hauers. The version featuring more of Hauers scenes was better received by audiences. Stallone then removed some of Hauers scenes from what became the final version of the film. This has also been confirmed in Frank Sanellos book "Stallone: A Rocky Life".

Many US and international lobby cards and stills show several deleted scenes. These include:

* Aftermath of London bombing scene with victims lying on ground (this was one of the scenes which were cut because of the violence and gore)
* DaSilva and Fox on another drug bust
* Wulfgar and Shakka showing the plastic surgeon photos of Wulfgars desired look
* a couple of deleted scenes between Deke and Irene including a scene during a party at her house
* Wulfgar picking up a MAC-10 submachine gun from his suitcase
* Deke and Irene talking in her bedroom (possibly part of a love scene between them which was cut out)
* a love scene between Wulfgar and Shakka
* a longer version of the scene at the Metropolitan Museum of Art
* Deke talking with the French ambassador whose wife was killed by Wulfgar on the tram car
* extended finale with Deke and Irene holding each other on the stairs inside her house after he killed Wulfgar.

Theatrical trailer shows three extended scenes; Wulfgar looking at city map in subway while being observed by street cop, Wulfgar slowly moving towards the building which he is about to blow up, and Deke and Matt walking across the street while searching for Wulfgar in disco bars.

Some of the Keith Emersons score for the film was also cut down due to the many re-editing problems that film had. Most of the tracks from soundtrack are longer than their film versions, and there are some which were intended for some longer scenes, ones of many which were deleted. Best example of it is "Face To Face" track which is the music that Emerson composed for infamous original ending where Wulfgar is torn to pieces in slow-mo with his flesh and blood flying around the room while Deke shoots at him.

Emerson did said some interesting things in soundtrack notes about composing the music for deleted scenes between Deke and Irene (in notes he refers to character DaSilva as De Soto):

:"That bit when De Soto tries to get back with his ex-wife (Lindsay Wagner) we need some love interest there" said Harry. I made notes (Humm, love interest). Sly, whom i felt comfortable adressing as such, felt uncomfortable watching the rushes. His comments upon viewing the love scenes between Lindsay and himself led him to say, "Thats the last time i go in Billy Dees trailer". Most of it was cut along with the love interest music."

Emerson also said some things about his music being edited for final theatrical version of the film:

"Universal got some old dyke as music editor that had worked on Jaws. She was a minimalist in maximalist clothing and immediately set about stripping everything down apart from my underwear in order for my entire score to reach the big screen as half the man i might have been. Sly, upset about Raging Bull was already working on another Rocky sequel, and couldnt be bothered."

Song "Im a Man" by Chicago was one of Stallones favourites and he used that song as temp track in famous disco scene where Deke recognizes Wulfgar. Keith Emerson recorded his version of the song which was used in final cut of the film. Unfortunately, most of the DVD versions have different music in disco scene instead of Emersons "Im a Man" song.

In the Aint It Cool News Q&A session, Stallone said that Nighthawks "was a very difficult film to make namely because no one believed that urban terrorism would ever happen in New York thus felt the story was far fetched. Nighthawks was an even better film before the studio lost faith in it and cut it to pieces. What was in the missing scenes was extraordinary acting by Rutger Hauer, Lindsay Wagner, and the finale was a blood fest that rivaled the finale of Taxi Driver. But it was a blood fest with a purpose". 

While he was disappointed with the way Universal studio re-edited the film (despite the fact that he did his share of re-editing on the film prior to studios interference), Stallone was really upset because of the removal of his dramatic scenes with Lindsay Wagner, including an emotional scene between him and Wagner in restaurant (only mentioned in final version of the film) where his character breaks down and cries after his wife refuses to remarry with him.

Lindsay Wagner said in interview that because of the many problems during production it was Stallone who took over and directed the film, including the scenes between two of them which he demanded to be added into the film in order for his character to have more screentime. This was something that Stallone was known for, for example he also ghost directed   and Cobra (1986 film).   In interview, Wagner said this about Nighthawks and working with Stallone:

:"He was really incredible. That film – I mean, history has shown that hes so talented in so many different ways. He had made "Rocky" obviously before that. But, it was just incredible. They had some difficulties. Whatever they were, I wasnt privy to the inside information about it. We started with one director, and all of the sudden there was some problems, and Sylvester ended up having to take over the film and he ended up directing it. So just spontaneously, he just jumped into that role, and after that directed it. And, it was incredible watching him and his multi-talented self whip that film into shape. It was quite educational in some ways. But, just kind of awe inspiring watching him work on so many levels at one time. Thats not easy. Not many actors can do that." 

== Reception == The Day of the Jackal might have looked like if filmed by the producers of Baretta. In order to facilitate a grandstanding, harebrained heroic role assigned to Sylvester Stallone, the filmmakers brush off every opportunity for intelligent dramatization and authentic suspense that the plot would seem to possess".   

It currently has a rating of "73 Fresh" on the critics site Rotten Tomatoes. 

== DVD == Universal Pictures Brown Sugar" by the Rolling Stones and the second one is "Im A Man" by Keith Emerson. Earlier VHS releases from Universal Home Video and some TV versions also featured the altered songs. The fullscreen DVD release by GoodTimes Entertainment contains the restored songs.

Some DVD and Blu-ray versions of the film have two different versions of ending credits. While in some versions film ends with freeze frame of DaSilva sitting on steps and looking at Wulfgars dead body after he killed him, in some other versions scene continues with DaSilva moving his hands and head (probably in guilt because of the brutal way he killed Wulfgar) while end credits roll. It is not known why there are two different versions of end credits.

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 