List of American films of 1905
 
A list of American films released in 1905 in film|1905. 

 
{| class="wikitable"
|-
! Title !! Director !! Cast !! Genre !! Notes
|-
|Adventures of Sherlock Holmes; or, Held for Ransom||J. Stuart Blackton|| || ||
|-
|Airy Fairy Lillian Tries on Her New Corsets|| || || ||
|-
|A Ballroom Tragedy|| || || ||
|- Burglar Bill|| || || ||
|-
|Coney Island at Night|| || || ||
|-
|A Dog Lost, Strayed or Stolen|| || || ||
|-
|Hippodrome Races, Dreamland, Coney Island|| || || ||
|-
|Escape from Sing Sing|| || || ||
|-
|The Green Goods Man; or, Josiah and Samanthys Experience with the Original American Confidence Game|| || || ||
|-
|A Gentleman of France|| || || ||
|-
|The Great Jewel Mystery|| || || ||
|-
|I.B. Dam and the Whole Dam Family|| || || ||
|-
|The Kleptomaniac|| || || ||
|-
|Impersonation of Britt-Nelson Fight|| || || ||
|- New York Subway|| || || ||
|-
|The Nihilists|| || || ||
|-
|Moving Day; or, No Children Allowed|| || || ||
|-
|License No. 13; or, The Hoodoo Automobile|| || || ||
|-
|Life of an American Policeman|| || || ||
|- Edwin Stanton Porter|| || ||
|- The Night Edwin Stanton Porter|| || ||
|-
|A Kentucky Feud|| || || ||
|-
|Peeping Tom in the Dressing Room|| || || ||
|-
|The Newsboy|| || || ||
|-
|A Policemans Love Affair|| || || || 
|-
|The Rat Trap Pickpocket Detector|| || ||Comedy||
|-
|Reuben in the Opium Joint|| || || ||
|-
|Raffles the Dog|| || || ||
|- Raffles the Broncho Billy Anderson|| ||Adventure/Romance||
|- The Seven Ages|| || || ||
|-
|The Servant Girl Problem|| || || ||
|-
|The Train Wreckers|| || || ||
|-
|Tom, Tom, the Pipers Son (1905 film)|Tom, Tom, the Pipers Son|| || || ||
|-
|Watermelon Patch|| || || ||
|-
|The Whole Dam Family and the Dam Dog|| || || ||
|-
|The Vanderbilt Auto Race|| || || ||
|}

==External links==
 
*  at the Internet Movie Database

 

 
 
 
 