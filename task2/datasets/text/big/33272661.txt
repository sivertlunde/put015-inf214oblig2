El sacerdote
{{Infobox film
| name           = El sacerdote
| image          = Sacerdote.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Eloy de la Iglesia
| producer       = Óscar Guarido    Carlos Goyanes
| writer         = 
| screenplay     = Enrique Barreiro
| story          = 
| based on       =  
| narrator       = 
| starring       = Simón Andreu    Emilio Gutiérrez Caba    Esperanza Roy
| music          = Carmelo A. Bernaola
| cinematography = Magín Torruella
| editing        = Julio Peña
| studio         = 
| distributor    = 
| released       = 1 May 1978
| runtime        = 109 min
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}} 1978 cinema Spanish film directed by Eloy de la Iglesia and starring Simón Andreu, Emilio Gutiérrez Caba and Esperanza Roy. The plot centers in a catholic priest who suffers a personal crisis when his sexuality is suddenly awaken. Unable to reconcile his deep conservative religious faith with his sexual obsession he spiral in a path of self punishment. The script was first offered to Pilar Miro. Bentley,  A Companion to Spanish Cinema, p. 235 

==Plot==
Father Miguel is a conservative thirty six years old Catholic priest. In the changing Spain of 1966 Miguel’s religious community tries to implement the doctrines given by the Second Vatican Council and adapt to the new realities of the country. Father Miguel opposes any form of religious modernization. He clashes with, Father Luis, a fellow priest of his community who employs more modern tactics of religious instruction. Miguel begins to experience a personal crisis when his repressed sexuality is awaken by a road sign, displaying a beautiful woman wearing a provocative bikini. He begins to be constantly tormented by sexual thoughts. His sexuality is further arouse by Antonia, a beautiful married woman in her thirties, who in her confessions tells father Miguel about her intense sexual encounters with her husband. Antonia’s confessions disturb father Miguel even more. He thinks about sex all the time to the point of impairing his work as a priest.

In his disturbed state of mind, father Miguel seeks advice from the more worldly and modern father Luis. Miguel entered religious life at age fourteen and, unlike Father Luis, never had sexual experiences with women. However Miguel feels ashamed asking about sex and he tells Luis to forget the matter before revealing what is tormenting him.

Alarmed by Miguel’s behavior, father Alfonso, the leader of the congregation, separates father Miguel from normal services to the community. Antonia is given a new confessor while Miguel is placed in charge of classes at the local school. He begins to give religious instruction to children who are getting ready for their first communion. However, Miguels sexual obsessions continue. Even the sight of one of the young boys playing with a pellet between his tights brings sexual images to Miguel’s mind. To calm down his sexual desires, Miguel resorts to penitence and self-flagellation punishing his own flesh with a cilice. This weakens his health

The boy is Antonia’s son and Miguel sees her again during the child’s first communion. Seeing her with her husband Miguel imagines Antonia having sex with her husband as she used to describe to him during confession. Miguel visits a bar, makes arrangements to have sex with a prostitute, but back down at the last minute. He is so disturbed by his sexuality, he falls sick. Father Francisco sends him to recuperate at his native village where Miguel’s widow mother still lives.

Once in his hometown father Miguel begins to recover his strength. While visiting his mother, he walks across town and remembers events from his childhood. His irascible father who beat him merciless and the sexual adventures of his classmates, who went skinny seeping in a river comparing the size of their penises and have sex with farm animals.                                                                       
Back in the city and his religious community things has changed. One of the priests had a girlfriend and has left the order to marry her. Miguel seems initially better but he is still tormented by constant sexual thoughts. He sees again Antonia whose marriage is not going well. She wants to divorce her husband. Miguel visits her at her house. Antonia tells him that all along she has been in love with him. Her confession where aimed to arouse Miguel’s interest. Miguel and Antonia have sex but he is overwhelmed by guilt for what he has done. During Christmas celebrations Miguel, tormented and deeply disturbed, locks himself in his room. He uses garden scissors to castrates himself.

Miguel survives his castration. He is sent to a mental institution. Once deemed healthy again he returns to his community were many things have changed. He sees Antonia once again. She is now separated from her husband and would like to rekindle their relationship. Miguel tells her that is too late for that. Finally at ease, Miguel leaves his religious life and his community. He has lost his faith.

==Cast==
*Simón Andreu – Father Miguel
*Emilio Gutiérrez Caba – Father Luis
*Esperanza Roy – Irene
*José Franco – Father Alfonso
*Ramón Reparaz – Father Manuel
*José Vivó- Bishop
*África Pratt - Prostitute
*Queta Claver - Miguel’s mother

==Notes==
 

== References ==
* Bentley, Bernard P.E. A Companion to Spanish Cinema. Tamesis Books, 2008. ISBN 978-1-85566-176-9

==External links==
*  

 
 
 