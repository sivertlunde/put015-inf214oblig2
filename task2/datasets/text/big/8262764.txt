The Alphabet Murders
 
{{Infobox film
| name = The Alphabet Murders
| image = Alphabet murders223.jpg
| caption = Theatrical release poster
| director = Frank Tashlin
| writer = David Pursall (screenplay) Jack Seddon (screenplay) Agatha Christie (novel)
| starring = Tony Randall Anita Ekberg Robert Morley
| music = Ron Goodwin
| cinematography = Desmond Dickinson
| distributor = Metro-Goldwyn-Mayer
| released =  
| country = United Kingdom
| runtime = 90 min
| language = English
}}
The Alphabet Murders is a British detective film based on the novel The A.B.C. Murders by Agatha Christie, starring Tony Randall as Hercule Poirot.

==Plot==
Albert Aachen, a clown with a unique diving act, is found dead, the murder weapon a poison dart. When a woman named Betty Barnard becomes the next victim, detective Hercule Poirot suspects that Sir Carmichael Clarke could be in grave danger.

As he and Captain Hastings look into the crimes, a beautiful woman with an interesting monogram named Amanda Beatrice Cross becomes the focus of their investigation, at least until she leaps into the Thames.

==Cast==
* Tony Randall as Hercule Poirot
* Anita Ekberg as Amanda
* Robert Morley as Hastings
* Maurice Denham as Japp
* Guy Rolfe as Duncan Doncaster Sheila Allen as Lady Diane
* James Villiers as Franklin
* Julian Glover as Don Fortune
* Grazina Frame as Betty Barnard
* Clive Morton as X
* Cyril Luckham as Sir Carmichael Clarke
* Richard Wattis as	Wolf David Lodge as Sergeant
* Patrick Newell as Cracknell

==Production background==
The part of Poirot had originally been intended for Zero Mostel but the film was delayed because Agatha Christie objected to the script. The film varies significantly from the novel and emphasises comedy, the specialty of director Frank Tashlin. Poirot is given buffoonish characteristics, while still remaining a brilliant detective.

The film features a cameo appearance by Margaret Rutherford as Miss Marple and Rutherfords husband Stringer Davis as Mr. Stringer.

==In popular culture==

This book is referenced in the Detective Conan story "The Red Horse within the Flames".

==External links==
* 

{{Navboxes|list1=
 
  
 
}}
 
 
 
 
 
 
 
 

 