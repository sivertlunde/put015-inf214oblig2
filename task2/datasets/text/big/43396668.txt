Union Square (film)
{{Infobox film
| name = Union Square
| image =
| alt =
| caption = 
| director =  Nancy Savoca
| producer = 
| writer =  Nancy Savoca Mary Tobler
| starring = Mira Sorvino Tammy Blanchard Patti LuPone Mike Doyle Michael Rispoli Daphne Rubin-Vega Christopher Backus Harper Dill, and Michael Sirow
| music = 
| cinematography = 
| editing = 
| studio = 
| distributor = 
| released =  
| runtime = 80 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}
Union Square is a 2011 comedy-drama film. The film stars Mira Sorvino,  Tammy Blanchard, and Patti LuPone and displays the inner lives of women. The movie was discussed at the Toronto film festival on September 16, 2011.

== Plot ==
Two sisters have a reunion together. One is about to become married while the other has a stressful life. They visit unforeseen places and construct their worlds together while having a reunion.*  

==Cast==
*Mira Sorvino ...  Lucy
*Mike Doyle ...	Bill
*Tammy Blanchard ...  Jenny
*Patti LuPone ...  Lucia
*Michael Rispoli ...   Nick
*Christopher Backus ...	Andy
*Daphne Rubin-Vega ...	Sara
*Michael Sirow ... Jay (voice)
*Harper Dill ...	Trish
*Holden Backus ... Mike

== References ==
 

==External links==
*  

 
 

 