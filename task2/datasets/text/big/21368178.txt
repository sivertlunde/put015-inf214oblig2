Dalibor (film)
{{Infobox film
| name           = Dalibor
| image          = 
| caption        = 
| director       = Václav Krška
| producer       = 
| writer         = Josef Wenzig (libretto) Václav Krška (screenplay)
| starring       = Václav Bednár
| music          = Bedřich Smetana
| cinematography = Ferdinand Pečenka
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}
 opera of the same name by Bedřich Smetana.  Directed by Václav Krška, the film was entered into the 1956 Cannes Film Festival.     

==Cast==
* Václav Bednář as King Vladislav
* Karel Fiala as Dalibor
* Věra Heroldová as Milada
* Přemysl Kočí as Budivoj
* Josef Celerin as Benes
* Josef Rousar as Vítek
* Jana Rybárová as Jitka
* Milo Červinka as Zdenek
* Luděk Munzar

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 