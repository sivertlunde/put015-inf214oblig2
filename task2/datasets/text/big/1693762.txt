Willard (1971 film)
{{Infobox film
| name           = Willard
| image          = Willard 1971.jpg
| caption        = Promotional poster
| director       = Daniel Mann
| producer       = Charles A. Pratt Mort Briskin
| writer         = Gilbert Ralston
| starring       = Bruce Davison Elsa Lanchester Ernest Borgnine Sondra Locke
| music          = Alex North
| cinematography = Robert B. Hauser
| editing        = Warren Low Bing Crosby Productions
| distributor    = Cinerama Releasing Corporation 
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| gross          = $14,545,941 
}}
 directed by Stephen Gilbert, and was nominated for an Edgar Award for best picture. The supporting cast included Elsa Lanchester in one of her last performances, and Sondra Locke in one of her first. The film was a summer hit in 1971; opening to good reviews and high box office returns.  It inspired other horror films with wild animals as predators, as well as psychological thrillers with social outcasts as the protagonists, climaxing in 1975 and 1976 with the hit films Jaws (film)|Jaws and Carrie (1976 film)|Carrie.

==Plot==
Willard is a meek social misfit who later develops a strange affinity for rats. He lives in a large Victorian home, with only his cranky and decrepit mother for company.  On his 27th birthday, he is humiliated to come home to a birthday party thrown by his mother, where all of the attendees are her own aging friends.  After having left the party in embarrassment, he notices a rat in his backyard and tosses it pieces of his birthday cake. Later, his mother gets upset with him for leaving the party and she scolds him later while also discussing how badly the house is falling apart. The next morning he goes out and feeds another rat (this one has babies with it) while imitating their squeaks. His mother starts telling him that he needs to kill the rats that have been running around their yard, to which Willard refuses.

When Willard goes to work he is promptly scolded by his boss Al Martin. Later he returns home and sets about killing the rats as his mother ordered.  He puts food on a center rock in a large well, placing a wooden plank to act as a bridge for the rats.  When the rats have gathered on the rock, he removes the plank, trapping the rats. He then turns on the water, intending to let the well fill up and drown the animals.  However, his guilt will not allow Willard to carry through the plan, and he turns off the water and returns the plank to its place, allowing the rats to return. When his mother asks if he killed the rats he lies and tells her he did.

That afternoon he begins playing with a rat he names Queenie, and begins teaching the rats words like "food" and "empty". He sees a white rat and immediately takes a liking to it. The white rat becomes his best companion and he names it Socrates for his wisdom. Numerous other rats quickly emerge, one of which is a bigger black specimen whom he names Ben.

At work, Mr. Martin continues to antagonize Willard, telling him he will not give him a raise and then urging him to sell his mothers house. Willard sneaks up to a party Mr. Martin is throwing, opens his suitcase which has rats in it, and then urges them to go get the food and ruin the party. The guests begin screaming and Willard laughs behind the bushes where he is hiding.

The next day Willards mother dies. He discovers that the house is heavily mortgaged. After this Willard is further pressured by the banks to give up the house.

Willard decides to bring Socrates and Ben to the office with him. He sets them on some shelves and tells them to be good. One of his friends at work gives him a cat named Chloe. Chloe constantly claws at the suitcase where Ben and Socrates are hiding. Willard hands her off to a complete stranger and drives away. 

The rat population is getting too big, and Willard can not afford to feed them much longer. He decides to steal money from his boss, using his now-trained rats. He orders the rats to "tear it up" and puts them in front of the office door. 

Later, at home, Willard gets mad at Ben and starts putting him outside the bedroom, but Ben persists in sleeping in his room. 

The next day he again takes Ben and Socrates to work. One of the workers spots the rats and Mr. Martin bludgeons Socrates to death, leaving Willard devastated. After  Mr. Martin confronts Willard over the theft, Willard instructs the rats, led by Ben, to kill Mr. Martin.  Slightly unnerved by Martins gruesome death, Willard then abandons Ben, goes home and begins sealing up any holes through which the rats could gain entry.  He also puts as many rats as he can into cages and drowns them in the small pool outside.

Willard has dinner with Joan, a co-worker he likes, but is startled to look up and see Ben back in the house staring at him from a corner shelf. He gets up and notices hordes of rats running up the stairs from the basement. He orders Joan to leave and locks the door before confronting Ben. Willard stalls and begins mixing rat poison, but Ben reads the box and squeals loudly, alerting the others, some of whom attack Willard. In an act of desperation, Willard tries to hit the rats with a broom, but misses. He runs upstairs but the other rats come after him. Shutting the door, he stands there terrified. The rats begin to gnaw at the door and eventually break in and devour him, and he says to Ben, "I was good to you". The camera zooms into a close-up of Ben and the credits roll.

==Cast==
*Bruce Davison as Willard Stiles
*Sondra Locke as Joan
*Elsa Lanchester as Henrietta Stiles
*Ernest Borgnine as Al Martin

==Awards==
*Willard was nominated for the Eddie award in Best Edited Feature Film at the 1972 American Cinema Editors Awards.
*Willard was also nominated for the Edgar award in Best Motion Picture at the 1972 Edgar Awards.

==Legacy==
*A seven page satire by Mort Drucker and Dick DeBartolo titled "WILLIES" appeared in Mad (magazine) #149, dated March 1972.  The cover of the magazine announced "IN THIS ISSUE WE TEAR APART WILLARD" and featured artwork by Jack Rickard.  The cover art portrays Alfred E. Neuman as Willard siccing an army of rodents (all dressed in Mickey Mouse pants and shoes with Mouseketeer Ears) on a hapless Ernest Borgnine.
*A sequel called Ben (film)|Ben was released in 1972.
*Willard serves as the opening anecdote to a chapter, "Becoming-Intense, Becoming-Animal, Becoming-Imperceptible..." in Gilles Deleuze and Felix Guatarris A Thousand Plateaus.
*A remake also titled Willard (2003 film)|Willard was released in 2003, with Crispin Glover playing Willard. Bruce Davison is also featured in the film as Willards father, appearing in a portrait.

==In popular culture==
*In the Season 1 Episode 8 "Bugs" of the television show "Supernatural (U.S. TV series)", Dean and Sam hunt a killer who uses bugs to kill his victims. They refer to him as a "Willard", a person who uses a psychic connection to animals for murder.
*In the video game " ", the film is mentioned in the vehicle chart under the car called "Willard". Its listing says that the car is like the movie in that it "squeaks and its hard to get rid of". boss monster named Willard, a giant rat.
*The Reaper (TV series)|Reaper episode "All Mine" featured an escaped soul who set bugs to attack those who got near her former lover.

==References==
 

==External links==
* 

 

 

 
 
 
 
 
 
 
 
 
 
 