The Storage
{{Infobox film
| name           = The Storage
| image          = Varasto.jpg
| caption        = Film poster
| director       = Taru Mäkelä
| producer       = Markku Tuurna
| writer         = Veli-Pekka Hänninen Arto Salminen
| starring       = Kari-Pekka Toivonen
| music          = 
| cinematography = Jouko Seppälä
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}

The Storage ( ) is a 2011 Finnish comedy film directed by Taru Mäkelä.    

==Cast==
* Kari-Pekka Toivonen as Antero Rousku
* Minttu Mustakallio as Karita
* Aku Hirviniemi as Raninen
* Esko Salminen as Kataja
* Juha Muje as Store manager
* Tomi Lauri as Rofa
* Vesa Vierikko as Jylhäkorpi
* Hannele Lauri as Aino
* Vesa-Matti Loiri as Mynttinen
* Jope Ruonansuu as Ykä

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 