The Very Thought of You (film)
{{Infobox film
| name           = The Very Thought of You
| image          =
| image_size     =
| caption        =
| director       = Delmer Daves
| producer       = Jerry Wald
| writer         =
| based on       = an original story by Lionel Wiggam Alvah Bessie Delmer Daves
| starring       = Dennis Morgan Eleanor Parker Dane Clark
| music          = Franz Waxman
| cinematography = Bert Glennon
| editing        = Alan Crosland Jr.
| distributor    = Warner Bros.
| released       =  
| runtime        = 99 minutes
| country        = United States English
| budget         =
| gross          =
}}
:For the 1998 British romantic comedy, see Martha, Meet Frank, Daniel and Laurence.

The Very Thought of You is a 1944 romantic drama film starring Dennis Morgan and Eleanor Parker. In World War II, an American soldier on a short leave falls in love with and marries a woman.

==Plot==
Away for a year and a half serving their country, Army Sergeants Dave (Dennis Morgan) and "Fixit" (Dane Clark) spend a three-day pass in Pasadena, California|Pasadena, also visiting Daves alma mater, Caltech.

They meet two young women who work in a parachute factory. Cora (Faye Emerson) quickly catches Fixits eye, while Janet (Eleanor Parker) remembers Dave from school days. Upon realizing that Dave has no family nearby, Janet invites him home for Thanksgiving dinner.

Her family does not treat him kindly. Janets mother does not approve of getting involved with a military man whos away all the time. One reason for that is Janets sister Molly (Andrea King), who is married to a sailor but seeing other men behind his back. Janets brother, classified 4-F, is rude to Dave as well. Only her father makes their dinner guest feel welcome.
 Mount Wilson runs long and causes them to be late getting Janet back home, but the couple cant bear to part, so Janet and Dave proceed to Coras apartment and fall asleep. It is 3 a.m. when he takes her home, where Janets mother slaps her.
Dave must report for duty in San Diego, but is in love and marries Janet, enjoying a brief honeymoon. Molly disapproves and intercepts Daves letters to Janet.
Janet decides to move out and live in Coras apartment.
 William Prince) for forgiveness and they reconcile. It takes months more, but Dave finally returns to rejoin his wife and meet their new baby boy.

==Cast==
* Dennis Morgan as Sergeant David Stewart
* Eleanor Parker as Janet Wheeler
* Dane Clark as Sergeant "Fixit" Gilman
* Faye Emerson as Cora Colton
* Beulah Bondi as Harriet Wheeler
* Henry Travers as Pop Wheeler William Prince as Fred
* Andrea King as Molly Wheeler John Alvin as Cal Wheeler
* Marianne OBrien as Bernice
* Georgia Lee Settle as Ellie Wheeler
* Richard Erdman as Soda Jerk

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 