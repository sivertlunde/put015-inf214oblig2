Parthiban Kanavu (1960 film)
{{Infobox film
| name           = Parthiban Kanavu 
| image          = parthibankanavu.jpg
| image_size     = 
| caption        = 
| director       = Dasari Yoganand|D. Yoganand
| producer       = V. Govindarajan
| writer         = Vindhan
| screenplay     = Dasari Yoganand|D. Yoganand Kalki
| based on       =  
| narrator       = 
| starring       = Vyjayanthimala Gemini Ganesan S. V. Ranga Rao
| music          = Vedha
| cinematography = K. S. Selvaraj
| editing        = V. B. Natarajan Pazhani R. Rajan
| studio         = Jubilee Films
| distributor    = Jubilee Films
| released       =  
| runtime        = 219 minutes
| country        = India Tamil Telugu Telugu Sinhala Sinhala
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 multilingual directed by Dasari Yoganand|D. Yoganand.  The film starred Vyjayanthimala, Gemini Ganesan and S. V. Ranga Rao in the lead with Ragini (actress)|Ragini, S. V. Subbiah, Kumari Kamala, T. S. Balaiah, P. S. Veerappa and S. A. Ashokan forms an ensemble cast. 

The film was based on 1942 Historical novel Parthiban Kanavu written by Kalki Krishnamurthy.  The film also had actor K. Balaji in special appearance and actress B. Saroja Devi as Extra (actor)|extra.
 Telugu and Sinhala by Best Feature Film in Tamil at the 8th National Film Awards. With its huge budget, the film failed to perform well at box office where it ended as box office bomb.
 Veerapandiya Kattabomman and Thillaanaa Mohanambal for its historical inaccuracies.   

==Plot==
7th century AD. Parthiban, the Chola King, dies in battle leaving incomplete his desire to be free from the yoke of the Pallavas. His son Vikraman (Gemini Ganesh) is determined to fulfill his fathers dream. He is arrested by the Pallava king, Narasimhavarman and exiled to an island where he is chosen the king. Though he has banished him, Narasimhavarman in fact cares a great deal about Vikraman as the latter loves his daughter Kundhavi (Vyjayanthimala). Vikraman returns to the mainland to see his mother and is attacked by robbers. Narasimhavaraman, in the guise of  a sage and who has been helping Vikraman constantly rescues him. Vikraman weds Kundhavi and rules over the independent Chola Kingdom thus fulfilling his fathers dream.

==Cast==
* Vyjayanthimala as Princess Kundhavi
* Gemini Ganesan as Vikraman
* S. V. Ranga Rao as Maamalar (Narasimhavarman I) Ragini as Valli
* S. V. Subbiah as Ponnan
* Kumari Kamala as Sivakami
* T. S. Balaiah as Marappa Bhupathi
* P. S. Veerappa as Gapala Bhairavan
* S. A. Ashokan as Parthiban
* K. Malathi as Arulmozhi
* Javar Seetharaman as Siruthondar
* Radhabai as Thiruvenkada Nangai

Special appearances:
* B. Saroja Devi as Princess Kundhavis accompanied
* K. Balaji as Mahendran (Mahendravarman II) 

==Crew==
*Producer: V. Govindarajan
*Production Company: Jubilee Films
*Director: Dasari Yoganand|D. Yoganand
*Music: Vedha
*Lyrics: Kannadasan, Vindhan & A. Maruthakasi
*Story: Kalki Krishnamurthy
*Screenplay: Dasari Yoganand|D. Yoganand
*Dialogues: Vindhan
*Art Direction: Maniam
*Editing: V. B. Natarajan & Pazhani R. Rajan
*Choreography: G. Ellappa & Muthusami Pillai
*Cinematography: K. S. Selvaraj
*Stunt: Shyam Sundar
*Audiography: Mukul Bose
*Dance: None

==Production== Telugu and Sinhala language|Sinhala.  Maniyam who was an associate of Kalki Krishnamurthy was chosen as the art director who brings the historical characters alive. 

==Soundtrack==
The films soundtrack was compose by Vedha with the lyrics were penned by Kannadasan,  Viduvan and A. Maruthakasi. The album had A. M. Rajah, P. Susheela, M. L. Vasanthakumari, Jamuna Rani and P. Leela as the singers.  The song "Pazhagum Thamizhe" which was a duet filmed on Vyjayanthimala and Gemini Ganesan becomes popular. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Picturization || Length (m:ss) ||Lyrics ||Notes
|-
| 1|| "Andhi Mayanguthadi" || M. L. Vasanthakumari  || Feature dancer Kumari Kamala || 03:29 || Vindhan|| 
|-
| 2|| "Idhaya Vaanin" || A. M. Rajah, P. Susheela || Featuring actress Vyjayanthimala and Gemini Ganesan || 03:25 ||  Vindhan || 
|-
| 3|| "Kannale Naan Kanda" || A. M. Rajah, P. Susheela || Picturisation on Vyjayanthimala and Gemini Ganesan || 03:23 || A. Maruthakasi ||
|-
| 4|| "Malligai Poo" ||   || 
|-
| 5|| "Thanga Churangam" ||   || 
|-
| 6|| "Pazhagum Thamizhe" ||   ||
|-
| 7|| "Munnam Avan" || P. Leela || Kumari Kamala in an item number || 04:45 || A. Maruthakasi || 
|-
| 8|| "Vazhi Maele" || M. L. Vasanthakumari ||  || 03:02 || A. Maruthakasi || 
|}

==Reception==

===Commercial response===
Despite the excellent star cast and good direction by Dasari Yoganand|D. Yoganand, the film did not fare well at box office.   

===Critical response===
The movie generally received positive review among critics for the direction of Dasari Yoganand|D. Yoganand and the impressive performance by the star cast, Gemini Ganesan, Vyjayanthimala and S. V. Ranga Rao. On the other hand, the reveal of the yogis identity in the beginning of the film was criticized as it did not hold the suspense among the audience which was the plus point of the Parthiban Kanavu|novel.  The lead pair Vyjayanthimala and Gemini Ganesan was proved to be an attractive pair and their scenes sustained interest in the cinema.  The success of the lead pair continued through another film, Then Nilavu in the 1960.  Randor Guy from The Hindu had conclude that the film is "remembered for the glamour of Vyjayanthimala, the winsome lead pair, exquisite sets and pleasing music". 

==Awards== National Film Awards Best Feature Film in Tamil for Dasari Yoganand|D. Yoganand and K. M. Govindarajan.   

==Controversy== 2011 Tamil Tamil science science fiction thriller film Tamil films Veerapandiya Kattabomman Tamil History film historian S. Theodore Baskaran had quoted that, "The crew of Parthiban Kanavu — a film on the Pallava dynasty — did not even visit Mahabalipuram ruled by the Pallavas" while criticizing the film makers that "They do not even do basic research". 

==See also==
* List of longest films in India by running time

==References==
 

==External links==
*  
*   at Upperstall.com
*   at The Hindu

 

 
 
 
 
 
 
 
 