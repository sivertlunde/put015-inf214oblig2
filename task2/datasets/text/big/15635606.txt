21 Days
 
 
{{Infobox film
| name           = 21 Days
| image          = 21_days_poster.jpg
| image size     =220px
| caption        = Theatrical release poster
| director       = Basil Dean
| producer       = Alexander Korda
| based on       = 
| writer         = Graham Greene Basil Dean
| narrator       = 
| starring       = Vivien Leigh Laurence Olivier Leslie Banks
| music          = Muir Mathieson John Greenwood
| cinematography = Jan Stallich
| editing        = Charles Crichton William Hornbeck London Film Productions, Ltd.
| distributor    = Columbia Pictures
| released       =  
| runtime        = 72 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} The First and the Last by John Galsworthy. It was directed by Basil Dean and stars Vivien Leigh, Laurence Olivier and Leslie Banks. The film was re-named as 21 Days Together for the US market.

==Plot==
Larry Durrant (Laurence Olivier) is a bit of a disappointment to his family, and even more so when he kills Henry Wallen (Esme Percy), the husband of his lover Wanda (Vivien Leigh). The husband, showed up on Wanda’s doorstep after being missing for a long time. Henry threatens to kill Wanda and Larry accidentally kills him in the ensuing fight. 

Larry stows Henry’s corpse away in an abandoned archway at Glove Lane. Afterwards he goes to his do-good brother Keith (Leslie Banks) for some advice. Keith is a successful attorney with a brilliant mind, well on his way to becoming a judge. When Larry tells him what he’s done, Keith wants him to leave the country for a while, and spare them both some trouble, not spoiling Keith’s career by having a murderer for a brother and saving Larry from going to jail.  

However, Larry refuses to leave, and returns to the alley where he left the body. There he encounters John Evan (Hay Petrie), a former minister turned bum. Evan unfortunately picks up the gloves Larry had dropped in the street, which results in him later being arrested for Wallens murder. The police claims there is enough circumstantial evidence with the bloody gloves he had on him. 

When Larry learns of Evans arrest, he considers himself a free man and decides to marry Wanda. For the next three weeks before Evan goes on trial, they plan to squeeze 30 years of idyllic life because Larry will then turn himself in for murder. On the day that Evan is sentenced to hang, Keith begs his brother to remain silent and let the condemned man die. Larry, set on doing the right for once in his life, refuses and leaves for the police station, only to be stopped on the steps by Wanda. She has read the newspaper, telling of Evan’s demise from a heart attack on his way to jail.

==Cast==
 
* Vivien Leigh as Wanda Wallen
* Laurence Olivier as Larry Durrant
* Leslie Banks as Keith Durrant
* Francis L. Sullivan as Mander David Horne as Beavis
* Hay Petrie as John Aloysius Evan
* William Dewhurst as the Lord Chief Justice
* Esme Percy as Henry Wallen
* Frederick Lloyd as Swinton
* Robert Newton as Tolly
* Victor Rietti as Antonio
* Morris Harvey as Alexander MacPherson
* Elliott Mason as Frau Grunlich Arthur Young as Ascher
* Meinhart Maur as Carl Grunlich
 

==Production==
Producer Alexander Korda tooled 21 Days to be a star vehicle for Vivien Leigh, but his constant interference caused great problems on the set. He rearranged shooting schedules and even added a sequence; director Basil Dean reputedly never saw a rough cut or the finished product. The title change to 21 Days was attributed to Korda.   Principal photography took place in 1937 at Denham Film Studios, with the production shot in black and white. Vincent Korda, Alexanders younger brother, was the art director on 21 Days and responsible for the sets. 

==Reception== Gone with the Wind (1939), Korda shelved 21 Days for two years before releasing it to Columbia Pictures.  Bosley Crowther in his review for The New York Times, said, "True, it is no deathless drama—is little more than a cultivated penny-thriller, in fact—and Miss Leigh, as the party of the second part, is required to devote her charm and talents to nothing more constructive than making the apparently inevitable parting from poor Mr. Olivier seem exceedingly painful, indeed. But it is a highly charged "meller," rigid throughout with suspense and nicely laced with much tender emotion." 

==References==
Notes
 

Bibliography
 
* Alexander Walker (critic)|Walker, Alexander. Vivien: The Life of Vivien Leigh. New York: Grove Press, 1987. ISBN 0-8021-3259-6.
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 