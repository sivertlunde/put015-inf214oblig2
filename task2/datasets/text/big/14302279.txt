List of Bollywood films of 2002
 
 
 
 
This article contains a list of films produced by the Bollywood film industry based in Mumbai in 2002.

==Top grossing films==
The top grossing films at the Indian box office in 2002. 

{| class="wikitable sortable"
! Rank
! Title
! Nett gross
! Cast
|-
| 1 ||Devdas (2002 Hindi film)|Devdas ||   34,50,00,000 || Shahrukh Khan, Madhuri Dixit, Aishwarya Rai, Jackie Shroff, Kirron Kher 
|-
| 2 ||Aankhen (2002 film)|Aankhen ||   29,25,00,000 || Amitabh Bachchan, Akshay Kumar, Arjun Rampal, Sushmita Sen, Paresh Rawal 
|-
| 3 ||Raaz (2002 film)|Raaz ||   25,00,00,000 || Dino Morea, Bipasha Basu, Ashutosh Rana, Malini Sharma 
|-
| 4 ||Kaante||   19,00,00,000 || Amitabh Bachchan, Sanjay Dutt, Sunil Shetty, Mahesh Manjrekar, Malaika Arora, Lucky Ali, Gulshan Grover 
|-
| 5 || Hum Tumhare Hain Sanam ||   15,75,00,000 || Madhuri Dixit, Shahrukh Khan, Salman Khan 
|-
| 6 || Humraaz ||   14,75,00,000 || Bobby Deol, Akshaye Khanna, Ameesha Patel, Johnny Lever 
|-
| 7 || Mujhse Dosti Karoge! ||   14,50,00,000 || Hrithik Roshan, Rani Mukerji, Kareena Kapoor 
|-
| 8 ||Saathiya (film)|Saathiya ||   18,75,00,000 || Rani Mukerji, Vivek Oberoi 
|-
| 9 || Awara Paagal Deewana||    13,00,00,000 || Akshay Kumar, Sunil Shetty, Aftab Shivdasani, Amrita Arora, Aarti Chabria, Preeti Jhangiani  
|-
| 10 || Deewangee ||   11,25,00,000 || Ajay Devgan, Akshaye Khanna, Urmila Matondkar 
|}

==Filmfare==

The below given are the top grossers of 2002 listed by Filmfare magazine. 
 16 December   Mere Yaar Ki Shaadi Hai   Yeh Dil Aashiqanaa

==2002==
{| class="wikitable"
! Title !! Director !! Cast !! Genre
|- 16 December || Mani Shankar || Milind Soman, Dipannita Sharma, Gulshan Grover, Aditi Govitrikar, Sushant Singh || Action
|-
|   ||  Guddu Dhanoa ||  Sunny Deol, Bobby Deol, Divya Dutta, Aishwarya Rai, Amrita Singh  || Historical Drama 
|-
| Aakhri Inteqam || || ||
|-
| Aankhen (2002 film)|Aankhen ||  Vipul Shah ||  Amitabh Bachchan, Akshay Kumar, Paresh Rawal, Arjun Rampal, Sushmita Sen, Kashmira Shah, Bipasha Basu  || Thriller, Action 
|-
| Aap Mujhe Achche Lagne Lage ||  Vikram Bhatt ||  Hrithik Roshan, Amisha Patel  || Romance, Drama 
|-
| Ab Ke Baras || Raj Kanwar  ||  Amrita Rao, Arya Babbar, Ashish Vidyarthi  || Romance, Drama
|-
| Agni Varsha || Arjun Sajnani ||  Raveena Tandon, Akkineni Nagarjuna, Prabhu Deva, Milind Soman, Jackie Shroff, Sonali Kulkarni, Amitabh Bachchan  || Historical 
|-
| Amma (2002 film)|Amma || || ||
|- Ajay Kashyap || Vinay Anand, Rana Jung Bahadur, Rajat Bedi, Sanober Kabir||
|- Anita and Me || Metin Hüseyin || Kabir Bedi, Max Beesley, Sanjeev Bhaskar, Anna Brewster, Meera Syal  || Comedy, Drama 
|-
| Akhiyon Se Goli Maare || Harmesh Malhotra || Govinda Ahuja|Govinda, Raveena Tandon, Kader Khan || Comedy
|- Ravi Dewan|| Sunil Shetty, Sanjay Dutt, Ashutosh Rana, Preeti Jhangiani, Johny Lever, Tinu Anand || Action, Thriller 
|- Om Puri, Ashutosh Rana, Abbas (actor)|Abbas, Sharbani Mukherji || Action Thriller||
|- Vikram Bhatt||  Akshay Kumar, Sunil Shetty, Aftab Shivdasani, Amrita Arora, Rahul Dev, Preeti Jhangiani  || Action, Comedy 
|-
| Be-Lagaam || Ram Lakhan || Aryan Vaid, Pramod Moutho, Tanveer Zaidi  || Action
|-
| Badhaai Ho Badhaai || Satish Kaushik || Anil Kapoor, Shilpa Shetty, Kirti Reddy || Comedy,Drama
|-
| Bharat Bhagya Vidhata || || ||
|-
| Bheeru No || || ||
|-
| Bollywood Queen || || ||
|-
| Bomb the System || || ||
|-
| Chalo Ishq Ladaaye ||  || Govinda (actor)|Govinda, Rani Mukerji  || Comedy 
|-
| Chhal || || ||
|-
| Chor Machaaye Shor || Aziz Sejawal ||  Shilpa Shetty, Bobby Deol, Bipasha Basu  || Comedy, Romance, Crime 
|-
| Company (2002 film)|Company ||  Ram Gopal Varma ||  Ajay Devgan, Manisha Koirala, Vivek Oberoi, Antara Mali  || Crime, Drama 
|-
| Danger (2002 film)|Danger || || ||
|-
| Darwaza (2002 film)|Darwaza || || ||
|-
| Deewangee ||  ||  Urmila Matondkar, Ajay Devgan, Akshaye Khanna  || Thriller
|-
| Desh Devi || || ||
|-
| Devdas (2002 Hindi film)|Devdas ||  Sanjay Leela Bhansali ||  Shahrukh Khan, Madhuri Dixit, Aishwarya Rai, Kiron Kher, Jackie Shroff  || Romance, Drama, Musical
|-
| Dil Hai Tumhaara ||  Kundan Shah ||  Rekha, Preity Zinta, Mahima Chaudhary, Arjun Rampal, Jimmy Shergill  || Family Drama 
|-
| Dil Vil Pyar Vyar || Anant Mahadevan || Jimmy Shergill, Sanjay Suri, Namrata Shirodkar, R. Madhavan|Madhavan, Sonali Kulkarni, Hrishita Bhatt  || Romance, Drama
|-
|   || || ||
|-
| Ek Aur Visphot || || ||
|-
| Ek Chotisi Love Story || Shashilal K. Nair ||  Manisha Koirala  || Erotic 
|-
|   || || ||
|-
| Filhaal ||  Meghna Gulzar ||  Tabu (actress)|Tabu, Sushmita Sen, Sanjay Suri  || Drama 
|-
| Gangobai || || ||
|-
|   || Kumar Jay || Pankaj Berry, Rahul Bhatt, Seema Biswas, Gayatri||
|- Amol Shetge || Dino Morea, Bipasha Basu, Irrfan Khan || Thriller, Romance
|-
| Haan Maine Bhi Pyaar Kiya || Dharmesh Darshan  ||  Akshay Kumar, Karisma Kapoor, Abhishek Bachchan  || Drama 
|-
| Haathi Ka Anda || || ||
|- Mahesh Manjrekar ||  Sanjay Dutt, Shilpa Shetty, Namrata Shirodkar  || Drama 
|- Ram Asra, Neelima Azeem, Hemant Birje, Mohan Joshi ||
|- Hum Kisise Kum Nahin || David Dhawan ||  Amitabh Bachchan, Ajay Devgan, Sanjay Dutt, Aishwarya Rai || Action, Comedy
|-
| Hum Pyar Tumhi Se Kar Baithe ||  ||  Jugal Hansraj, Tina Rana || Romance 
|-
| Hum Tumhare Hain Sanam ||  K. S. Adhiyaman ||  Salman Khan, Shahrukh Khan, Madhuri Dixit, Atul Agnihotri, Aishwarya Rai  || Drama, Romance 
|-
| Humraaz (2002 film)|Humraaz ||  Abbas-Mustan ||  Bobby Deol, Akshaye Khanna, Amisha Patel  || Thriller 
|-
| Inth Ka Jawab Patthar || || ||
|-
|   || Rajkumar Kohli || Sunny Deol, Akshay Kumar, Sunil Shetty, Sonu Nigam, Arshad Warsi, Aftab Shivdasani, Manisha Koirala || Fantasy 
|-
| Jackpot Do Karode || || ||
|-
| Jeena Sirf Merre Liye ||  Satish Kaushik ||  Tusshar Kapoor, Kareena Kapoor, Mallika Sherawat  || Romance 
|-
| Junoon (2002 film)|Junoon || || Shilpa Shetty, Chandrachur Singh, Sonali Kulkarni ||
|- Rajat Bedi, Sudesh Berry, Mushtaq Khan, Inder Kumar, Sayaji Shinde, Shweta Menon || Action
|-
| Kaante (2002 film)|Kaante ||  Sanjay Gupta ||  Amitabh Bachchan, Sanjay Dutt, Sunil Shetty, Mahesh Manjrekar, Lucky Ali, Kumar Gaurav, Lisa Ray, Isha Koppikar, Malaika Arora, Rati Agnihotri  || Action, Thriller 
|-
| Kabhie Tum Kabhie Hum || || ||
|-
| Kali Salwaar || || ||
|- Harry Baweja||Sunny Deol, Sunil Shetty, Shilpa Shetty|| Action, Thriller 
|-
| Kehtaa Hai Dil Baar Baar ||  Rahul Dholakia ||  Jimmy Shergill, Kim Sharma, Paresh Rawal  ||
|-
| Khalnayakon Ka Khalnayak || || ||
|-
| Kitne Door Kitne Paas ||  || Fardeen Khan, Amrita Arora, Sonali Kulkarni  || 
|-
| Koi Mere Dil Se Poochhe ||  Boney Kapoor ||  Jaya Bachchan, Esha Deol, Aftab Shivdasani, Sanjay Kapoor, Anupam Kher  || Drama, Romance, Social 
|-
| Kranti (2002 film)|Kranti || || Bobby Deol, Amisha Patel ||
|-
| Kuch Tum Kaho Kuch Hum Kahein ||  ||  Fardeen Khan, Richa Pallod  || Romance 
|-
| Kya Yehi Pyaar Hai ||  ||  Amisha Patel, Aftab Shivdasani, Jackie Shroff  || 
|-
| Kyaa Dil Ne Kahaa ||  ||  Tusshar Kapoor, Esha Deol  || Romance 
|-
| Kuch Dil Ne Kaha ||  ||  Rakesh Bapat, Reema Sen  || Romance 
|- Lal Salaam Gaganvihari Borate || Nandita Das, Sharad Kapoor, Makrand Deshpande, Vijay Raaz ||
|-
| The Legend of Bhagat Singh ||  Rajkumar Santoshi ||  Ajay Devgan, Amrita Rao, Sushant Singh  || Historical, Drama 
|-
| Leitin að Rejeev || || ||
|- Maa Tujhhe Arbaaz Khan  || Drama 
|-
| Mahima Kaali Maa Ki || || ||
|-
| Maine Dil Tujhko Diya || Sohail Khan || Sohail Khan, Sameera Reddy, Sanjay Dutt, Kabir Bedi || Action, Romance
|-
| Makdee || Vishal Bhardwaj ||  Shabana Azmi, Makarand Deshpande  || 
|- Partho Ghosh|| Sunil Shetty, Namrata Shirodkar || Action
|-
| Mere Yaar Ki Shaadi Hai ||  Sanjay Gadhvi ||  Uday Chopra, Bipasha Basu, Tulip Joshi, Jimmy Shergill  || Romance 
|- Revathi || Shobhana, Matt Phillips  || Drama
|-
| Mujhse Dosti Karoge! ||  Kunal Kohli ||  Hrithik Roshan, Rani Mukerji, Kareena Kapoor  || Romance 
|-
| Mulaqaat || || Vinay Anand, Arun Bakshi, Rita Bhaduri, Milind Gunaji ||
|-
| Na Tum Jaano Na Hum ||  Arjun Sablok ||  Saif Ali Khan, Hrithik Roshan, Esha Deol  || Romance 
|-
|   || || ||
|-
| Nishad (2002 film)|Nishad || || ||
|-
| Om Jai Jagadish ||  Anupam Kher ||  Waheeda Rehman, Anil Kapoor, Fardeen Khan, Abhishek Bachchan, Mahima Chaudhry, Urmila Matondkar, Tara Sharma  || Family, Drama 
|-
| Pitaah || || Sanjay Dutt, Nandita Das, Jackie Shroff, Om Puri ||
|-
| Pratha || || ||
|-
| Pyaar Diwana Hota Hai ||  ||  Govinda (actor)|Govinda, Rani Mukerji  || Romance 
|-
| Pyaar Ki Dhun || || ||
|-
| Pyaasa (2002 film)|Pyaasa ||  ||  Yukta Mookhey, Aftab Shivdasani  || Action, Crime 
|-
| Raaz (2002 film)|Raaz ||  ||  Bipasha Basu, Dino Morea  || Horror 
|-
| Rishtey ||  ||  Anil Kapoor, Karisma Kapoor, Shilpa Shetty  || Drama 
|-
| Road (2002 film)|Road ||  ||  Vivek Oberoi, Antara Mali, Manoj Bajpai  || Thriller 
|- Tabu  || Drama, Romance, Social 
|-
| Shakti - The Power ||  Boney Kapoor ||  Karisma Kapoor, Nana Patekar, Sanjay Kapoor, Shahrukh Khan  || Drama 
|-
| Shararat (2002 film)|Shararat ||  ||  Abhishek Bachchan, Hrishita Bhatt  || Comedy, Drama 
|- Arbaaz Khan, Raveena Tandon ||
|-
|   || Tanuja Chandra || Lucky Ali, Gauri Karnik  || Musical
|-
| Swaraaj || || ||
|-
|Tere Pyaar Ki Kasam || Mahesh Manjrekar || Sanjay Dutt, Sunil Shetty, Mahima Chaudhry, Hrishita Bhatt, Kabir Bedi, Om Puri || 
|-
| Truth Yathharth, The || || ||
|- Ramesh Puri Pran (actor)|Pran ||
|-
| Tumko Na Bhool Paayenge ||  ||  Salman Khan, Dia Mirza, Sushmita Sen  || Drama 
|-
| Tumse Achcha Kaun Hai || || Rati Agnihotri, Aarti Chabria, Ali Asghar ||
|-
| Vadh || || Nana Patekar, Anupama Verma, Puru Raajkumar, Nakul ||
|-
| Waah! Tera Kya Kehna || || Govinda (actor)|Govinda, Raveena Tandon, Preeti Jhangiani ||
|-
| Yeh Dil Aashiqanaa || Kuku Kohli || Karan Nath, Jividha Sharma ||
|-
| Yeh Hai Jalwa || David Dhawan ||  Rishi Kapoor, Salman Khan, Amisha Patel, Rinke Khanna, Rati Agnihotri, Sanjay Dutt, Anupam Kher   || Comedy 
|-
| Yeh Kaisi Mohabbat || || ||
|-
| Yeh Kya Ho Raha Hai? || Hansal Mehta || Prashant Chianani, Aamir Ali, Vaibhav Jhalani, Yash Pandit, Payal Rohatgi || Comedy, Romance
|-
| Yeh Mohabbat Hai || || Rahul Bhatt, Johnny Lever ||
|-
| Zindagi Khoobsoorat Hai || Manoj Punj || Gurdas Maan, Tabu (actress)|Tabu, Divya Dutta, Rajat Kapoor||
|}

==Notes==
 

==External links==
*   at the Internet Movie Database
*   - A look back at 2002 with a focus on the Hindi film song

 
 
 

 
 
 
 
 