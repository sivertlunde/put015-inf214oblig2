Big Daddy (1999 film)
{{Infobox film
| name = Big Daddy 
| image = Big Daddy film.jpg
| alt = 
| caption = Theatrical release poster
| director = Dennis Dugan
| producer = Allen Covert Adam Sandler Robert Simonds Jack Giarraputo
| writer = Steve Franks Tim Herlihy Adam Sandler
| starring = Adam Sandler  Joey Lauren Adams Jon Stewart Rob Schneider Cole Sprouse Dylan Sprouse Leslie Mann Steve Buscemi Kristy Swanson Josh Mostel
| music = Teddy Castellucci
| cinematography = Theo van de Sande
| editing = Jeff Gourson Jack Giarraputo Productions
| distributor = Columbia Pictures
| released =  
| runtime        = 93 minutes
| country = United States
| language = English Italian
| budget = $34.2 million   
| gross = $234.8 million 
}}
Big Daddy is a 1999 American comedy film  directed by Dennis Dugan and starring Adam Sandler and the Sprouse twins. The film was produced by Robert Simonds and released on June 25, 1999, by Columbia Pictures where it opened  #1 at the box office with a $41,536,370 first weekend     as well as a score of 41% on Metacritic.    It was Adam Sandlers last film before starting his production company, Happy Madison Productions.

==Plot==
Sonny Koufax (Adam Sandler) is an unreliable, unmotivated bachelor who lives in New York City and has declined to take on adult responsibility. He has a degree in law but has chosen not to take the bar exam since he was awarded $200,000 in a vehicle accident compensation 2 years prior, and lives off his restitution. He is employed, on a Part-time job|part-time basis only, as a tollbooth attendant, and more or less does this job just to get out of the apartment once in a while.

Finally having enough of Sonnys refusing to grow up, his girlfriend, Vanessa (Kristy Swanson), tells Sonny that shes going to break up with him unless he can prove to her that he can be a responsible adult. Meanwhile, Sonnys friends, all former schoolmates, are "moving ahead" in their lives. His roommate, Kevin Gerrity (Jon Stewart), decides to ask his podiatrist girlfriend Corinne (Leslie Mann) to marry him the day before he is leaving for China for his law firm.

Sonny awakes to find five-year-old Julian (Cole Sprouse and Dylan Sprouse) abandoned at his and Kevins apartment, with a written explanation that Julians mother is now declining to care further for Julian, and that Kevin is his biological father. Sonny notifies Kevin of the situation, which he denies (to the best of his knowledge), and Sonny offers to handle it until he returns from China.  Sonny poses as Kevin and uses Julian as a means to win Vanessa back. However, when Sonny introduces Julian to Vanessa, he finds out that shes already broken up with Sonny and shes now dating a much older man, Sid (Geoffrey Horne), whos more mature and career-oriented, even having a "5-year plan". 

Sonny then takes Julian to his social worker, Arthur Brooks (Josh Mostel), telling him that Julian should return to his mother. Brooks tells Sonny that Julians mother had died of cancer, thus explaining why she sent Julian to be with Kevin. He tells Sonny that they could take Julian, but he would have to live in a group home, which Sonny realizes is the equivalent of an orphanage. Sonny offers to keep Julian temporarily until Brooks finds a foster home for Julian. Sonny then calls his father, Lenny (Joseph Bologna), whos one of Floridas most respected lawyers, and informs him of his situation. Lenny tells Sonny that hes going to fail taking care of Julian, to which Sonny takes offense. After a conversation with Julian, Sonny decides to raise Julian his own way, giving Julian options instead of orders to let Julian decide for himself, all while teaching Julian new things such as baseball and pro wrestling. Julian also helps Sonny get a potential new girlfriend in Layla (Joey Lauren Adams), Corinnes sister, whos also a lawyer. This creates friction as Sonny and Corinne do not get along, with Corinnes former occupation as a Hooters waitress being the primary butt of his jokes.

Soon, Brooks leaves messages for Sonny, saying that a foster home has been found for Julian, but Sonny refuses to call back, having become attached to Julian. Later, at an open house at the school Julian now attends, the teacher tells Sonny that Julian has taken up many bad habits of Sonnys, which causes him to rethink his parenting methods. Hes able to turn himself and Julian around, but then Brooks arrives, having learned that Sonnys been posing as Kevin. He orders Sonny to surrender Julian to social services, lest Sonny be arrested for having (technically) kidnapped Julian. Sonny reluctantly gives Julian up and then promptly arranges a court case to determine Julians future.

In court, with himself and Layla as his counsels, numerous people that Sonny knows testify, telling the judge that hes a suitable father. Even Corinne, who harbors a deep hatred for Sonny, admits that he cares for Julian. Julian testifies as well, not only talking about all the good times hes had with Sonny, but also revealing relevant information regarding his original heritage. As a final attempt to win custody of Julian, Sonny calls himself to the stand and asks his father, whos attending as well, to question him. Lenny quickly gets Sonny to admit to several occasions where he was responsible for someone and was terribly unsuccessful, which he cites as proof that Sonnys not father material. At this point, Sonny quietly tells Lenny that the only reason hes doing this is because Lennys scared that Sonny will fail, which Sonny assures him that he wont because he loves Julian too much to fail, just like Lenny loves Sonny enough to be scared for him. Seeing Sonnys sincerity, Lenny vouches for Sonny as well. 

Nonetheless, the judge is unconvinced, citing that Sonny, according to law, still kidnapped Julian and orders for his arrest. Kevin, having recalled a drunken fling with a Hooters girl in Toronto during the 1993 World Series upon hearing Julians testimony of being five years old with a birthday in July, and originally being from Toronto, realizes that he is Julians biological father and declares that he will not allow charges against Sonny. Social service and the court agree to award custody to Kevin (pending DNA results). Julian tearfully hugs Sonny, wanting Sonny to be his father. Sonny promises Julian that he will always be his friend and he will always consider Julian part of his family. With that Sonny hands Julian off to Kevin, watching the two slowly start to bond. 
 
One year, three months, and six days later, Sonnys revealed to have turned his life around. Hes finally taken his bar exam, is now a lawyer himself and is married to Layla with a child of their own. Julian is also happy with his new life with Kevin and Corinne, whos also more friendly to Sonny now. At Sonnys surprise birthday party at a Hooters, Sonny finds Vanessa working there as a waitress, with Sid working as a cook, revealing that Sids "five-year-plan" has gone awry.

==Cast==
* Adam Sandler as Sonny Koufax
* Cole Sprouse and Dylan Sprouse as Julian Frankenstein McGrath
* Joey Lauren Adams as Layla Maloney
* Jon Stewart as Kevin Gerrity
* Leslie Mann as Corinne Maloney
* Rob Schneider as Nazo
* Jonathan Loughran as Mike
* Allen Covert as Phil DAmato
* Peter Dante as Tommy Grayton
* Kristy Swanson as Vanessa
* Joseph Bologna as Lenny Koufax
* Steve Buscemi as Homeless Guy
* Josh Mostel as Arthur Brooks
* Edmund Lyndeck as Mr. Herlihy
* Geoffrey Horne as Sid
* Harold OLena as Scuba Steve
* Method Man as Man #7

==Release==

===Critical response===
Big Daddy received generally mixed reviews from critics. On Rotten Tomatoes the film had an 40% rating by the critics.  The Rotten Tomatoes consensus says "Adam Sandler acquits himself admirably, but his charm isnt enough to make up for Big Daddys jarring shifts between crude humor and mawkish sentimentality." The film received a score of 41% on Metacritic. 

 
The film won the Peoples Choice Awards for Favorite Comedy Motion Picture in 2000. 
For its shortcomings however, the film was also nominated for five Razzie Awards including Worst Picture, Worst Director, Worst Supporting Actor for Rob Schneider and Worst Screenplay, with Adam Sandler winning Worst Actor. This was the first Adam Sandler film to be nominated for Worst Picture. 

==Soundtrack== BMI Film Music Award.  The soundtrack included the following:

;Track listing   
# "Sweet Child o Mine" by Sheryl Crow (a Guns N Roses cover) When I Garbage
# "Peace Out" by Adam Sandler (a sound clip from a scene in the movie)
# "Just Like This" by Limp Bizkit Everlast (a Neil Young cover)
# "Ga Ga" by Melanie Chisholm
# "What Is Life" by George Harrison, covered in movie by Shawn Mullins
# "The Kiss" by Adam Sandler (a sound clip from a scene in the movie) Instant Pleasure" by Rufus Wainwright Wise Guys
# "Sid" by Adam Sandler (a sound clip from a scene in the movie)
# "If I Cant Have You" by Yvonne Elliman
# "Smelly Kid" by Adam Sandler (a sound clip from a scene in the movie)
# "Passing Me By" by The Pharcyde (a sound clip from a scene in the movie)
# "Rush" by Big Audio Dynamite
# "Hooters" by Allen Covert (a sound clip from a scene in the movie) Styx
# "Overtime" by Adam Sandler (a sound clip from a scene in the movie)
# "The Kangaroo Song" by Tim Herlihy (made specifically for the movie) Styx (only a portion of the song)

;Other songs used in the film
* "Dancing in the Moonlight" by The CrownSayers (originally done by King Harvest) 1983 album by Eurythmics)
* "Growin Up" (a song from Greetings from Asbury Park, N.J.#"Growin Up" by Bruce Springsteen)
* "Instant Pleasure" by Rufus Wainwright
* "Sweet Child o Mine" a re-recorded version taken from a live version played by Guns N Roses mixed with a recording with the 1999 Guns N Roses lineup When I Garbage
* "Jump (Van Halen song)|Jump" by Van Halen background music on the answering machine message in Sonnys apartment
* "Growin Up" by Bruce Springsteen
* "Save It For Later" by Harvey Danger (originally by The English Beat) Styx
* Survivor
* "Nights Interlude" by Nightmares on Wax (Song played during opening credits)
* "Passing Me By" by Tha Pharcyde (Song played during opening credits) Sampled/cover of song above.

;Songs from the theatrical trailer not in the film
*"Do Wah Ditty" by Zapp and Roger
* "You Get What You Give" by New Radicals

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 