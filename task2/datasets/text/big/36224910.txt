2:22 (2008 film)
{{Infobox film
| name           = 2:22
| image          = 2,22 film poster 2008, Canada.jpeg
| caption        = 
| producer       = Manny Barbosa  (Associate producer)   Lenny Bitondo and Mike Guzzo  (Executive producers)   Mike Masters  (Line producer)   Philip Roy Mick Rossi Phillip Guzman
| director       = Phillip Guzman	 	
| screenplay     = Mick Rossi Phillip Guzman Eric Saavedra 	
| starring       = Mick Rossi Robert Miano Aaron Gallagher Jorge A. Jiminez Val Kilmer
| music          = Danny Saber	 
| cinematography = Philip Roy
| editing        = Philip Roy
| studio         = Inception Media Group
| distributor    = Eagle Entertainment
| released       =  
| budget         = 
| gross          = 
| runtime        = 104 minutes
| language       = English
| country        = Canada
}}

2:22 is a 2008 Canadian low-budget crime thriller. The film was directed by Phillip Guzman, and starred Mick Rossi, Robert Miano, Aaron Gallagher, and Jorge A. Jiminez.

==Plot==
Gully Mercer (Mick Rossi) leads a group of pro-league criminals (Robert Miano, Aaron Gallagher, Jorge A. Jiminez, and Val Kilmer) in the city of Toronto. They plan a seemingly "foolproof" heist for a lazy high-class hotel on New Years Eve, set to take place at exactly 2:22 A.M. But when things go horribly wrong, friendship, loyalty and trust are pushed to the limit.

==Cast==
* Mick Rossi as Gully Mercer
* Robert Miano as Willy
* Aaron Gallagher as Finn
* Jorge A. Jiminez as Gael 
* Peter Dobson as Curtis
* Val Kilmer as Maz
* Bruce Kirby as Norman Penn
* Gene Burns as Rodney Rooney
* Luis Caldeira as Miller
* Sean Power as Rudy
* Sile Bermingham as Jody

==Reception==
Steve Power of DVD Verdict gave a mostly positive review of 2:22, writing that the film compensated for a lack of budget with some striking visual flair, closing his statement with "2:22 is a surprisingly entertaining and gritty production. It doesnt add anything new to the crime genre, and the uneven performances certainly dont do the film any favors, but the direction is sound, the script is solid, and the flick has a great look for low budget fare."  Mac McEntire, also from DVD verdict, stated that 2:22 was an "okay" film, but one "that could have been a great movie". McEntire added that the part on the planned heist was cool and fun, but the drama that follows was cliché and hackneyed.  Paul Mavis, a critic from DVD Talk, wrote a negative review of the film. He stated that the film was "tedious, overlong, and overly familiar", and ended with "everyone else can safely skip 2:22."  Film Critics United called 2:22 a "solid entry into the heist genre with its gritty look, well realized characters and its judicious use of violent imagery."  Filmink was another reviewer which gave a negative review. It gave the film 1.5 (out of 5).   The LA Times praised the visuals but found it unengaging and disappointing. 

==Awards==
Following is a list of awards that 2:22 or its cast have won or for which they have been nominated.

===Wins===
* 2009 Malibu Film Festival
** Best Picture Award  

* 2010 Milan Film Festival
** Best Cinematography&nbsp;— Philip Roy

==References==
  

==External links==
*  
*  

 
 
 
 
 