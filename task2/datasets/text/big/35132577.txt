South 5
 
South 5 is a 2007 comedy film written and directed by Tacoma, Washington resident Bryan Johnson.  The movie’s cast features South Sound area residents Jason Gordon, Chris Martin, Kyle Price, Mitch and Eli Robinson and Doug Thayer.  The film was entered in the 2007 Grand Cinema “72 Hour Film Competition” and won the prestigious “Audience Choice” award.     It quickly became a viral sensation and local favorite, due to its quirky humor, engaging plot, and the variety of locations featured from Tacoma, Washington.  In May 2012, the film will celebrate its five-year anniversary with special celebrations and activities planned.

== Plot == Seattle Center and tries via his cell phone to convince multiple friends join him on his first trip to Tacoma.  None of them are interested, including some who refuse due to the perceived dangerous nature of the city.  Luke decides to take a trip on his own and as he takes an on-ramp onto Interstate 5, the namesake of the movie, a sign for “South 5” appears.
The first stop for Luke is the Point Defiance Zoo.  As he starts to enjoy his experience, he is suddenly hit in the face by a banana, and then immediately attacked by a gorilla. A zookeeper apologizes, offering to show the man the polar bears.  Luke quickly refuses and heads for downtown Tacoma.

Upon strolling through downtown, Luke is tripped and attacked by a mysterious figure wearing a Shriner’s Cap and then bitten on his leg by a small masked boy wearing a cape.  He escapes the two aggressors by jumping onto Tacoma’s light rail train, and again begins to relax, taking in the sights.
 Chihuly Bridge of Glass, when he is suddenly chased by a man wearing an eye patch, who bears a resemblance to legendary Studio Glass pioneer Dale Chihuly (who was born and raised in Tacoma).

As Luke is chased through Tacoma, the gorilla, zookeeper, Shriner and boy, all join in.  Luke, who only wanted to experience Tacoma for the first time, exclaims he is going back to Seattle and speeds away in his car.

As the film ends, the Shriner calls The Greater Seattle Chamber of Commerce and informs a shadowy figure that the team’s mission has been accomplished.  The team congratulates itself on frightening the man away from enjoying Tacoma and begins to prepare for a family from Queen Anne (Seattle) that is coming to Tacoma to look for a home.

== Reception ==
 
The short film immediately gained positive attention and reviews as the dynamic between Tacoma and Seattle has long been a subject of controversy in the area.  According to an article in the University of Puget Sound alumni publication, "South 5 quickly became the talk of the town. Posted on YouTube and Exit133, thousands of people viewed it and talked about it on blogs. Elected officials e-mailed it to colleagues. The Tacoma City Council screened it at one of its meetings.”.   

The film became a viral sensation as residents of Tacoma and Seattle both laughed at, and in some cases, seriously discussed the premise. The actors in the film suddenly became minor local celebrities recognized at everything from commuter train stops to local parades.  The city of Tacoma Fire Department and other local groups starting showing the film as a humorous break during training and other activities.

== Cast ==
 
* Chris Martin as Luke
* Kyle Price as the zookeeper
* Doug Thayer as the gorilla
* Mitch Robinson as the Shriner
* Eli Robinson as the young, masked boy
* Jason Gordon as the Chihuligan

== Film festivals ==
* Opening Night Selection Tacoma Film Festival 2007
* Official Selection Port Townsend Film Festival 2008
* Official Selection Gig Harbor Film Festival 2009

== References ==
 

 
 