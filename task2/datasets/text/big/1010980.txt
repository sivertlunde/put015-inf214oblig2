Neria
:For the genus of Micropezidae|tilt-legged flies, see Neria (fly).  For the Israeli village, see Neria, Mateh Binyamin.
{{Infobox film
| name           = Neria
| image          = NeriaPoster.jpg
| image_size     =
| caption        = Movie Poster
| director       = Godwin Mawuru
| producer       = John Riber Louise Riber
| writer         = Tsitsi Dangarembga (story) Louise Riber (screenplay)
| narrator       =
| starring       = Jesese Mungoshi Anthony Chinyanga Dominic Kanaventi
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 103 minutes
| country        = Zimbabwe English
| budget         =
| gross          =
}}

Neria is a Zimbabwean film made in 1993, written by the novelist Tsitsi Dangarembga. It is directed by Goodwin Mawuru and the screenplay was written by Louise Riber. It is the highest grossing film in Zimbabwean history. 

The film concerns the struggles of a woman in a rural town in Zimbabwe when she is widowed after her husband is killed in an accident.  The soundtrack of the film was sung by Oliver Mtukudzi.

==Cast==
*Anthony Chinyanga (Mr. Chigwanzi)
*Dominic Kanaventi (Phineas)
*Claude Maredza (Mr. Machacha)
*Emmanuel Mbrirmi (Patrick)
*Jesese Mungoshi (Neria)
*Violet Ndlovu (Ambuya)
*Sharon Malujlo (Canadian Tourist)
*garikai mudzamiri

==References==
 

==External links==
* 

 
 
 


 