Hangman's House
{{Infobox film
| name           = Hangmans House
| image          = Hangmanshouse.jpg
| caption        = Original Theatrical Poster
| director       = John Ford
| producer       = John Ford
| writer         = {{plainlist|
*Brian Oswald Donn-Byrne
*Malcolm Stuart Boylan
*Philip Klein (adaptation)
*Marion Orth (scenario)
}}
| starring       = {{plainlist|
*Victor McLaglen
*June Collyer Larry Kent
*Earle Foxe
*Hobart Bosworth
*John Wayne
}}
| cinematography = George Schneiderman
| editing        = Margaret C. Clancy Fox Film Corporation
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = {{plainlist|
*Silent film
*English intertitles
}}
}}
 romantic drama genre silent film set in County Wicklow|Co. Wicklow, Ireland, directed by John Ford (uncredited) with inter-titles written by Malcolm Stuart Boylan. It is based on a novel by Brian Oswald Donn-Byrne.  It was adapted by Philip Klein with scenarios by Marion Orth.  The film is also notable for containing the first confirmed appearance by John Wayne in a John Ford film. 

==Plot==
  as the films heroic figure Hogan.]] Larry Kent). 

Hogan returns to Ireland and disguises himself as a holy man.  On his way to the OBriens house he is recognised by a gatekeeper, whom he reveal his intentions to kill a man to.  Hogan meets Dermot McDermot and the three men witness the lights of Glenmalure|Glenmalures chapel being lit, signifying a wedding is taking place.  At this time a group of soldiers ask the gate keeper if he has seen Hogan.  Later that night, after Connaught and DArcy have been wed, the Baron dies.  On the night of his funeral Hogan sneaks about the grounds of Hangmans House and is spotted by DArcy.  DArcy is startled by the appearance of Hogan.  At bedtime DArcy tries to sleep with Connaught but she rejects his advances. 

  (left) and June Collyer (right) as the films romantic couple.]]
A community race is held on St. Stephens Day and Connaughts horse The Bard is due to race. The horses jockey goes missing just before the race because of interference from DArcy who has bet against the horse.  Dermot is required to jockey the horse and he wins the race leading a drunken DArcy to shoot The Bard.  DArcy is ostracised by the community because of this.  Hogan is arrested at the race.  At night Dermot and DArcy meet in a pub where DArcy reveals that he had an affair with Hogans sister.  Dermot gives DArcy money to leave Ireland and threatens him that if he ever sees him again he will kill him. 

Hogan escapes from prison and a gunfight erupts between his men and the guards. Later Dermot and Connaught visit Hogans hideout and Hogan reveals that his sister died following DArcys desertion.  Connaught returns to Hangmans House to discover that DArcy has returned.  After a struggle she flees to Dermots house.  Hogan and Dermot go to Hangmans House and confront DArcy. During a fight between the men a fire breaks out and burns down the house.  Hogan and Dermot escape but DArcy falls to his death as a balcony collapses.  Connaught and Dermot see Hogan off at the port as he returns to Algiers.  Connaught gives Hogan a kiss and Dermot shakes his hands and thanks him.  Connaught and Dermot walk away together as Hogan watches them.

==Cast==
* Victor McLaglen as Citizen Denis Hogan
* June Collyer as Connaught "Conn" OBrien
* Earle Fox as John DArcy Larry Kent as Dermot McDermot
* John Wayne as Horse Race Spectator/Condemned Man in Flashback (uncredited)
* Brian Desmond Hurst as Horse Race Spectator and witness to the horse shooting

==Production==

The film began production in January 1928 and took seven weeks to film. 

==Reception==

The film received positive reviews, Wilfred Beaton of Film Spectator called it "the finest program picture ever turned out by a studio".  In particular he praised the photography which he said "almost outdoes for sheer beauty the shots in  ".   , which they often resemble."   However the film was not a box office success as Fox Film Corporation did not promote the film. 

==DVD release==

The film was released on DVD in North America by Fox on December 4, 2007.  The film can be obtained three different ways:
* In the Ford at Fox - The Collection box set which is a 21 disc collection containing every film John Ford made at Fox. The Iron Horse and 3 Bad Men.  
* As a separate release also containing 3 Bad Men on the opposite side of the disc.

The DVD begins with a disclaimer stating that the film has been brought to DVD using the best surviving elements possible. The DVD has an option to view the film accompanied with a musical score by Tim Curran.

==See also==
* John Wayne filmography
* List of American films of 1928

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 