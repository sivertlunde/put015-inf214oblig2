Gloria (2013 film)
 
{{Infobox film
| name           = Gloria
| image          = Gloria poster.jpg
| caption        = Theatrical release poster
| director       = Sebastián Lelio
| producer       = {{Plainlist|
* Juan de Dios Larraín
* Pablo Larraín
* Sebastián Lelio
* Gonzalo Maza
|}}
| writer         = {{Plainlist|
* Gonzalo Maza
* Sebastián Lelio
|}}
| starring       = Paulina García
| music          = 
| cinematography = Benjamín Echazarreta
| editing        = {{Plainlist|
* Sebastián Lelio
* Soledad Salfate
|}}
| distributor    = 
| released       =  
| runtime        = {{Plainlist|
* 105 minutes (Berlin)
* 110 minutes
|}}
| country        = {{Plainlist|
* Chile
* Spain
|}}
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 86th Academy Awards,    but it was not nominated.

==Plot==
Gloria (García) is a 58-year-old divorcée. Her grown son and daughter have their own lives. She meets Rodolfo (Hernández), seven years her senior who, like her, is seeking companionship, but he cannot give up his other relationships.

==Cast==
* Paulina García as Gloria Cumplido Sergio Hernández as Rodolfo Fernández
* Diego Fontecilla as Pedro
* Fabiola Zamora as Ana
* Alejandro Goic as Daniel
* Coca Guazzini as Luz
* Hugo Moraga
* Luz Jiménez as Victoria
* Cristián Carvajal 
* Liliana García as Flavia
* Antonia Santa María as María
* Eyal Meyer as Theo
* Marcial Tagle
* Marcela Said as Marcela
* Pablo Krögh

==Reception==
The film received excellent reviews when it premiered at the 2013 Berlin International Film Festival, topping both Screen International s Screen jury poll of international critics,  and IndieWires critics poll of the best films screened at Berlin in 2013.  The film currently holds a 99% of positive reviews in the film critics site Rotten Tomatoes, being classified as "Certified Fresh" based on 107 critic reviews, with an average rating of 7.9/10. The general critical consensus is "Marvelously directed by Sebastian Lelio and beautifully led by a powerful performance from Paulina Garcia, Gloria takes an honest, sweetly poignant look at a type of character thats all too often neglected in Hollywood." The film also holds a 100% of positive reviews from Top Critics. 

David Rooney of The Hollywood Reporter writes "it’s hard to imagine anyone with a heart and a brain not responding to the quiet delights and stunning intimacy of Chilean director Sebastian Lelio’s account of the personal evolution of a 58-year-old divorcee, played with scrupulous honesty and intelligence by the wonderful Paulina Garcia" and "Funny, melancholy and ultimately uplifting, Sebastian Lelios enormously satisfying spell inside the head and heart of a middle-aged woman never puts a foot wrong". 

Variety (magazine)|Variety writes "Perceptive and unerringly sympathetic, Gloria has the makings of an arthouse sleeper". 

Mark Adams of Screen International writes "A delightfully astute and compassionate delve into the life of a 58 year-old divorcee looking for company, romance and perhaps even love, director Sebastián Lelio’s engaging, amusing and oddly uplifting Gloria is a film that will strike a chord with audiences of a certain age. It is driven by a quite wonderful performance from Paulina Garcia, who should snag best actress awards at every festival the film plays at."   

Eric Kohn of IndieWire gave the film an A and describes it as a "breakthrough" for actress Paulina Garcia. 

==Awards==
{| class="wikitable"
! Year !! Event !! Recipient !! Award !! Result
|-
| 2012
| San Sebastian International Film Festival
| Sebastián Lelio (director) Pablo Larraín (producer)
| Films in Progress Award - Best Film   
|  
|- 2013
| rowspan=3| 63rd Berlin International Film Festival
| Paulina García
| Silver Bear for Best Actress   
|  
|-
| Sebastián Lelio
| Golden Bear 
|  
|-
| Sebastián Lelio Jury Prize
|  
|-  Hawaii International Film Festival
| Sebastián Lelio
| Best Film
|  
|-
| Paulina García
| Best Actress
|  
|- Independent Spirit Awards
|Sebastián Lelio Independent Spirit Best Foreign Film
| 
|- London Film Critics Circle
|Sebastián Lelio Best Foreign Language Film
| 
|-
| rowspan=2| 2014 28th Goya Goya Awards
|Sebastián Lelio Goya Award Best Spanish Language Foreign Film
| 
|- National Board National Board of Review Awards
|Sebastián Lelio Top Foreign Films
| 
|}

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Chilean submissions for the Academy Award for Best Foreign Language Film
* Cinema of Chile

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 