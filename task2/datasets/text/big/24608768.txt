The One and Only (1978 film)
{{Infobox film
| name           = The One and Only
| image          = The One and Only 1978.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Carl Reiner
| producer       = David V. Picker Steve Gordon
| narrator       =
| starring       = Henry Winkler Kim Darby Gene Saks Patrick Williams
| cinematography = Victor J. Kemper
| editing        = Bud Molin
| studio         = First Artists
| distributor    = Paramount Pictures
| released       = 3 February 1978
| runtime        = 97 minutes
| country        = United States English
| budget         =
| gross          = $16,928,137 (USA)
| preceded_by    =
| followed_by    =
}} Steve Gordon.

==Plot==
At a midwestern college, student Mary Crawford has the combined good and bad fortune to meet Andy Schmidt, a remarkably conceited young man who is convinced that he is tremendously talented and has every intention of becoming a star. Andy is so obnoxious, he wants Mary to marry him fast before he becomes too famous to give her a second look. She cant resist him, try as she might. Mary takes her new boyfriend home to Columbus, Ohio, to meet her strait-laced parents. Andy proceeds to annoy them in every possible way, hugging them, calling them "Tom and Mom," interrupting dinner to do a series of impressions, thoroughly ruining the visit.

"Oh, God, why him?" Mary rhetorically asks. Nevertheless, she elopes to a justice of the peace, marries Andy and moves with him to New York City, where he is certain Broadway or the movies will beckon within a matter of weeks. Half a year passes and Andy gets nowhere. His ego isnt bruised and he remains his same insufferable self. Mary takes an office job to support them. She gets pregnant and still loves her husband, but her parents are terribly worried for Mary and she has to ask them for money because Andy isnt making any.

Andy does manage to make a friend, Milton Miller, a  . He doesnt have the build for it, but by behaving and dressing in the manner of a Gorgeous George-type, he creates a character known as "The Lover" who drives both opponents and audiences crazy. At last, Andy has thousands of fans, even if theyre not the kind he had in mind.

==Cast==
* Henry Winkler as Andy Schmidt
* Kim Darby as Mary Crawford
* Gene Saks as Sidney Seltzer
* William Daniels as Mr. Crawford
* Hervé Villechaize as Milton Miller
* Ed Begley, Jr. as Arnold (The King)
* Warren Stevens as Hector Moses
* H.B. Haggerty as Captain Nemo
* Ralph Manza as Bellman
* Dennis James in a cameo role

==Reception==
Vincent Canby of The New York Times did not enjoy the film much although he appreciated earlier works by Reiner. "The One and Only is more of that sort of safe, schmalzy comedy, but fatally lacking the presence of someone like the great George Burns."  Roger Ebert of Chicago Sun-Times gave it 2  out of 4 stars. "Its a pleasant movie, it has some genuinely funny moments." 

==References==
 

==External links==
*  
*  
*  
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 