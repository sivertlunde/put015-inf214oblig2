Goltzius and the Pelican Company
{{Infobox film
| name           = Goltzius and the Pelican Company
| image          = Goltzius-and-The-Pelican-Company-poster.jpg
| caption        = Theatrical poster
| director       = Peter Greenaway
| producer       = Kees Kasander Catherine Dussart Mike Downey Sam Taylor Igor Nola
| writer         = Peter Greenaway
| starring       = Ramsey Nasr F. Murray Abraham Giulio Berruti
| music          = Marco Robino
| cinematography = Reinier van Brummelen
| editing        = Elmer Leupen
| costumes    = Marrit van der Burgt
| distributor    = Kasander Film Company   Film and Music Entertainment   Catherine Dussart Productions   MP Film   Nederkands Fonds voor de Film   Rotterdam Film Fund   Centre National du Cinéma   Eurimages   Head Gear Films   Metrol Technology   Bankside Films {{cite web
  | url = http://www.screendaily.com/news/uk-ireland/bankside-slate-adds-peter-greenaway-famke-janssen-antonia-bird-films/5013744.article 
  | first         = Geoffrey
  | last          = Macnab
  | title         = Bankside slate adds Peter Greenaway, Famke Janssen, Antonia Bird films
  | accessdate    = 6 October 2011
  | publisher     = Screen Daily
  | date = 13 May 2010
 }} 
| released       =  
| runtime        = 
| country        = Netherlands France United Kingdom Croatia
| language       = English
| budget         = €2,05 million {{cite web
  | url           = http://www.screendaily.com/news/europe/greenaway-readies-goltzius-eisenstein-projects/5023673.article
  | first         = Geoffrey
  | last          = Macnab
  | title         = Greenaway readies Goltzius, Eisenstein projects
  | accessdate    = 6 October 2011
  | publisher     = Screen Daily
  | date = 13 February 2011
 }} 
| gross          = 
}}
Goltzius and the Pelican Company is a historical film by writer-director Peter Greenaway.    

==Plot== Lot and Joseph and Potiphars wife, Samson and Delilah, and John the Baptist and Salome. To tempt the Margrave further, Goltzius and his printing company will offer to perform dramatizations of these erotic stories for his court.

Goltzius and the Pelican Company is the second feature in Greenaways film series "Dutch Masters", which includes the previous film Nightwatching. {{cite journal
  | last = Morgan
  | first = Nesta
  | authorlink = 
  | coauthors = 
  | title = Nightwatching
  | journal = film&festivals
  | volume = 2
  | issue = 2
  | pages = p.5
  | publisher = Wallflower Press, Film Culture Ltd. 
  | location = United Kingdom
  | date = 
  | url = 
  | format = 
  | issn = 1755-5485
  | accessdate = }}  The third entry in the series will focus on Hieronymus Bosch, and its release is planned to coincide with the 500th anniversary of Boschs death in 2016.  

==Cast==
* Ramsey Nasr as Hendrick Goltzius
* F. Murray Abraham as The Margrave of Alsace 
* Goran Bogdan as Gottlieb
* Truus de Boer as Sophie
* Nada Abrus as Marie
* Dusko Valentic as Priest
* Milan Plestina as Priest 3
* Enes Vejzovic as Churchman 1
* Vedran Zivolic as Joachim
* Katija Zubcic as Beatrice Fereres 
* Boris Bakal as Messenger 
* Vedran Komericki as Churchman
* Samir Vujcic as Priest 2
* Tvrtko Juric as Priest 4

; The Pelican Company:
* Giulio Berruti as Thomas Boethius
* Kate Moran as Adaela
*   as Eduard Hansa
* Anne Louise Hassing as Susannah
* Hendrik Aerts as Strachey 
* Halina Reijn as Portia 
*   as Quadfrey 
*   as Samuel van Gouda 

; The Margrave’s family and court:
* Maaike Neuvilleas Isadora
*   as Ebola Goyal 
* Vincent Riotta as Ricardo del Monte 
*   as Rabbi Moab 
*   as Johannes Cleaver 

==Release== 
Goltzius and the Pelican Company  was released on 17th September 2012. 

==Awards==
* Peter Greenaway was nominated and won the FICE Award for Best European Director at the FICE- Federazione Italiana Cinema d’Essai in 2014.   
* Peter Greenaway was nominated for the New Visions Award for Best Motion Picture at the Sitges- Catalonian International Film Festival  in 2013. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 