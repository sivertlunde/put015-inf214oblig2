Patala Bhairavi
{{Infobox Film
  | name = Patala Bhairavi
  | image = Pathala Bhairavi.jpg
  | caption = Film poster
  | director = Kadri Venkata Reddy
  | producer =B. Nagi Reddy Aluri Chakrapani
  | writer = Kamalakara Kameswara Rao (screenplay) Pingali Nagendra Rao (Dialogue, Lyrics) Girija
  | music = Ghantasala Venkateswara Rao
  | cinematography =
  | editing =
  | distributor =Vauhini Studios
  | released = 15 March 1951
  | runtime = 195 minutes
  | language =  Telugu
  | budget =
  | preceded_by =
  | followed_by =
    }}

Patala Bhairavi ( , fantasy film produced by Vijaya Pictures. The ensemble cast film was directed by Kadri Venkata Reddy, with screenplay adaptation by Kamalakara Kameswara Rao. The film is listed among CNN-IBNs list of hundred greatest Indian films of all time. 

The film is based on a story from Kasi Majilee Kathalu, written by Madhira Subbanna Deekshitulu. It was also remade in Tamil language|Tamil.
It was screened successfully in 28 centers for more than 100 days. The film got critical acclaim  at the first India International Film Festival held in Mumbai on 24 January 1952. 
  

==Plot==
The son of a gardener, Tota Ramudu (N. T. Rama Rao) falls in love with Indumati, the princess of Ujjain (Malathi). When he faces resistance from the king, he goes off into the world to return as a successful man.  He is approached by a sorcerer, Nepala Mantrikudu (S. V. Rangarao), who actually plans to sacrifice a young, brave lad to the goddess Patala Bhairavi (Girija (actress)|Girija) to attain a magic statuette, which grants any wish. Ramudu fits the profile; and unwittingly, agrees to help the sorcerer so that he can attain the riches etc. the king asked for his daughters hand in marriage.
 Krishna Kumari), because of a curse she turned into a crocodile. He sacrifices the sorcerer and obtains Patala Bhairavi. Ramudu wishes to be a king, have a palace larger than the king has now and etc. to be welcomed by the Ujjain royalty.  The king lives up to his promise and grants him his daughter in marriage.

Sadajapa (B. Padmanabham), the sorcerers apprentice finds his master at the Patala Bhairavi site and brings him back to life using sanjeevini (Elixir of life). During this time, the kings brother-in-law (Relangi Venkata Ramaiah) is upset at upcoming wedding and determines to hang himself instead of witnessing the marriage.  The sorcerer catches him in time and promises him everything Ramudu has including Indumati in exchange for the small statue which Ramudu has hidden in a room.  He manages to steal the Patala Bhairavi and unwittingly changes Ramudus fate.

The sorcerer then wishes to kidnap the princess and takes all of Tota Ramudus wealth, leaving him in his original state. Pledging to bring back his love, Ramudu and his cousin, Anji (Bala Krishna), travels to the sorcerers lair. The story ends with the marriage of Ramudu and Indumati with the elders blessing. As it turns out, Anji, marries the princess servant cum best friend. The sorcerer is dead.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| N. T. Rama Rao || Tota Ramudu
|-
| S. V. Ranga Rao || Nepala Mantrikudu
|-
| K. Malathi || Indumati
|-
| Chilakalapudi Seetha Rama Anjaneyulu || King of Ujjaini
|- Girija || Patala Bhairavi
|-
| Balakrishna || Anji
|-
| Surabhi Kamalabai || Kantamma
|- Krishna Kumari || Angel
|-
| Balijepalli Lakshmikantam
|-
| B. Padmanabham || Dingiri
|-
| Hemalatamma Rao || Indumatis mother
|- Savitri || Dancer in the song "Ranante Rane Ranu" 
|-
| Relangi Venkata Ramaiah || Kings brother-in-law
|-
| T.G.Kamala Devi || Veera Katha performer
|}

==Trivia==
The kitschy imagery and studio sets provide an appropriate style for this emphatically orientalist fairy tale. Ghantasala (singer)|Ghantasalas music is a key contribution to the film’s success. The Hindi version dubbed by Gemini Film Circuit included a specially shot colour sequence with a dance by Lakshmikantam. The Telugu film consolidated a local version of the folklore film, a swashbuckling orientalist fantasy evoking both Alexandre Dumas and Douglas Fairbanks films. 

==Soundtrack== Ghantasala

Telugu songs:
Lyrics were written by Pingali Nagendra Rao.
* "Tiyyani Oohalu" by P. Leela.
* "Itihasam Vinara" by Kamala Chandrababu.
* "Kalavaramaaye madhilo", by Ghantasala and P. Leela.
* "Enta ghaatu premayo", by Ghantasala and P. Leela.
* "Vinave Baala Naa Prema Gola" by Relangi.
* "Vagaloy Vagalu Taluku Beluku Vagaulu" by Jikki.
* "Prema Kosamai Valalo Padene Paapam Pasivadu", by V. J. Varma.
* "Haayigaa Manaminkaa" by Ghantasala and P. Leela.
* "Taalalene Ne Taalalene" by Relangi
* "Kanugonagalano Leno" by Ghantasala.
* "Ranante Rane Ranu" by Pithapuram Nageswara Rao and T. K. Savitri.


Tamil songs:
Lyrics were written by Tanjai N. Ramaiah Doss.
* "Aanandhame tahrum kaanagam" by P. Leela
* "Kanindha Kaadhalarku.... Kaadhale Dheiveega Kaadhale" by Ghantasala and P. Leela
* "Ennathaan Un Preamaiyo" by Ghantasala and P. Leela
* "Prema Paasatthaal" by V. J. Varma
* "Amaidhiyilladhen Maname En Maname" by Ghantasala and P. Leela

==References==
 

==External links==
*   at Internet Movie Database

 
 
 
 
 