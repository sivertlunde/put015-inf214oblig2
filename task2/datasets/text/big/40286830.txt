Koothara
 
{{Infobox film
| name           = Koothara
| image          = Koothara_poster.jpg
| alt            = 
| caption        = Poster of the film featuring Mohanlal
| director       = Srinath Rajendran 
| Assi.director  = Unaiz Nazeer 
| producer       = Shahul Hammeed Marikar
| narrator       = Dulquer Salmaan 
| writer         = Vini Vishwa Lal
| starring       = Mohanlal Bharath Sunny Wayne Tovino Thomas
| music          = Gopi Sundar
| cinematography = Pappu
| editing        = Praveen K. L. N. B. Srikanth
| studio         = Marikar Films
| distributor    = UTV Motion Pictures 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =  
| gross          = 
}}
 2014 Malayalam Malayalam coming mystery thriller thriller film directed by Srinath Rajendran and written by Vini Vishwa Lal. It stars Bharath, Sunny Wayne, and Tovino Thomas in the lead roles along with Mohanlal in an extended cameo.  The film is set in an untold period, focuses the life and adventures of three engineering students, Koobrin (Bharath), Tarun (Tovino Thomas)and Ram (Sunny Wayne), in their college campus and the life after that. 

The film is produced by Shahul Hammeed Marikar under the banner Marikar Films and distributed by UTV Motion Pictures. The soundtrack and background score were composed by Gopi Sundar. The film was released on 13 June 2014. Upon release the film received mixed reviews. 

==Cast==
* Mohanlal as Siren Semiramis Aegean Derketto/Ustaad Saali, a mysterious fisherman and the leader of the coastal community, who has a secret identity as a sinned merman displaced to land as human decades ago. But ultimately gets salvation at the end after a long humanly life. Siren Semiramis Aegean Derketto is his real name as a merman and he adapted the name Ustaad Saali in his human life. Ustaad Saali owns an inactive old mysterious fishing boat called AK 47 which almost killed everyone who travelled in it, and the locals believe to be haunted.
*Bharath as Koobrin/Kalikkaran
*Tovino Thomas as Tharun
*Sunny Wayne as Ram Bhavana as Swathi
*Janani Iyer as Noora
*Gauthami Nair as Roshni
*Shritha Sivadas as Shilpa
*Madhurima as Sheistha Josekutty Valiyakallumkal as Chandran Sir
*Anjali Aneesh Upasana as Priya Miss Baburaj as Principal Ranjini as Rams mother
*Neena Kurup as Ammini
*Sasi Kalinga as Sulaiman
*Master Elhan Riyaz Shah as son of Tharun and Swati
*Arun Benny as Kannan
*Prem Kumar as himself
*Subbalakshmi as Valyammachi
*Sunil Sukhada as House Owner
*Abbi as Thufailikka
*Kollam Thulasi as Bar Manager
*Lishoy as Koobrins father
*Urmila Unni as Tharuns mother
*Nilumbur Ayisha as Muslim Lady

==Production==

Bharath was signed as one of the lead actors for the film. Ranjini was cast for an important role. Madhurima stated that she will essay the role of Shaista, an NRI in the film.  Shritha Sivadas said that her character Shilpa was a simple village girl and that she was paired opposite Sunny Wayne, who plays a college student. 

The shooting started at Calicut University Institute of Engineering and Technology, Thenhipalam on 25 September.  The film also have shoot at Calicut, Ernakulam, Thodupuzha and Andaman and Nicobar Islands.

Kootharas poster designed by YellowTooth was shortlisted for International Movie Poster (IMP) awards. 

==Soundtrack==
{{Infobox album
| Name = Koothara
| Type = Soundtrack
| Artist = Gopi Sundar, Thakara 
| Cover = 
| Caption = 
| Released = 2014
| Recorded =  Feature film soundtrack
| Length =
| Online Promotion =
| Label = 
| Producer = Shahul Hammeed Marikar 
| Last album = 
| This album = 
| Next album =
}}

The films soundtrack is composed by Gopi Sundar and Thakara Band.A promo song was released prior to the release of the movie.
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration !! Lyricist
|- 1 || Rita Tyagaragan and Gopi Sundar || 3:34 || Hari Narayanan 
|- 2 || Hari Narayanan
|- 3 || Sharan Raj || 3:22 || Hari Narayanan 
|- 4 || Hari Narayanan
|- 5 || Pavithra Menon,Sam Hari Narayanan
|- 6 || James Thakara Thakara
|- 7 || Thakara
|}

==Critical reception==
The film received mixed reviews. 

Times of India rated it 2.5/5 and said "It cannot be called an enjoyable movie despite a rich verve of youthful energy that is pumped into it. Often it is low, degrading and utterly disappointing rarely eliciting any real laughter." 

Gayathry of Oneindia.in said, "Koothara is a one time watchable flick. There are lot to see and laugh during the first half but, the movie disappoints during the second half and reviewed the movie 3/5." 

Raj Vikaram of metromatinee said, "Negative title will only serve to accentuate the movies collapse not that koothara, is most badly made movie which is insufficient to the hilt. But its attempt to be a surprise packet, doesnt bear fruit beyond a point." 

Deccan Chronicle stated " Koothara is good in parts, just like a use-and-throw razor" and appreciated the technical aspects of the movie. 

The IB Times applauded the cinematography and Mohanlals cameo. 

==Boxoffice==

International Business Times reported that  Koothara had a first day box office collection of 1.3 Crores.  . Ibtimes.co.in (14 June 2014). Retrieved on 20 June 2014. 

==References==
 
 

==External links==
*  

 
 
 
 