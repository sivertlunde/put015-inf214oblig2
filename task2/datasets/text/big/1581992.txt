Deewana (1992 film)
 
 
 
{{Infobox film
| name = Deewana दीवाना  
| image = Deewana.jpg
| director = Raj Kanwar 
| producer = Guddu Dhanoa   Lalit Kapoor
| music = Nadeem Shravan
| distributor = DEI
| starring =Shah Rukh Khan Divya Bharti  Rishi Kapoor  Amrish Puri
| country = India
| released = 25 June 1992
| runtime = 185 minutes
| language = Hindi
| budget =
| gross   =  (domestic gross 1992) 
}}
 Musical Romantic Romantic Action film directed by Raj Kanwar, produced by Guddu Dhanoa and Lalit Kapoor.

It starred Shah Rukh Khan, Divya Bharti, and Rishi Kapoor. This was Shah Rukh Khans debut release, though he was not the sole lead and only appears in the second half. He replaced Armaan Kohli, who walked out of the project due to creative differences after the first schedule. The film released on June 25, 1992.

==Plot==
Kajal ( ) and cousin Narender (Mohnish Behl), who are determined to lay their hands on Ravis wealth. Narender first tries to rape Kajal but fails when he is caught. Pratap then hire hoodlums to murder Ravi, and he is thrown off a cliff. Along with Ravi, Prataps son, Narender, also dies.

Kajals mother-in-law (Sushma Seth) takes Kajal away from the depression of losing her husband to start a new life. In a new city, the widowed and depressed Kajal tries to get over her pain. One day a young man, Raja (Shah Rukh Khan), accidentally hits Kajals mother-in-law, meets Kajal and falls in love with her. When he tells Kajal he loves her, Kajal tells him that she is a widow. Rajas father tells a bunch of thugs to get rid of Kajal and Raja severs ties with his father after finding out. He begs Kajals mother-in-law to have Kajal marry him. As her mother-in-law feels that Kajal should marry again, she persuades her.

Raja and Kajal get married but Raja tells her that he will not touch her until she accepts him. He looks for a job and his friends open a garage with him. One day, Raja goes to check out a jeep but has an accident when the brakes fail. Kajal becomes shocked when she hears this and runs to see him. After returning from the hospital, Kajal accepts him and falls in love with him, and the two are finally happy together. Things are well until one day, Raja rescues a man from hoodlums. He gets him treated and befriends the man, whose name is Ravi.

When Raja brings Ravi to Kajal, she is shocked to see that her husbands new friend is none other than her first husband, who apparently survived his uncles attempt to murder him. When the truth is revealed however, Kajal stays with Raja, and Ravis mother lives with Ravi. Ravis uncle figures out that Ravi is alive and has Raja and Kajal kidnapped, saying that he will let them go if Ravi signs the property papers. Raja escapes and he and Ravi beat Pratap, after which they find Kajal, tied up with a bomb strapped around her. Ravi manages to switch off the bomb and take it off her, but Pratap appears and says he will kill Raja and make Kajal a widow again. Ravi protects Raja and he and Pratap fall. Ravi sets the bomb off, causing a large explosion, killing Pratap and him once and for all. Kajal and Raja stay together and live happily ever after, honoring Ravis memory.

== Cast ==
*Rishi Kapoor as Ravi
*Divya Bharti as Kajal
*Shahrukh Khan as Raja Sahai
*Amrish Puri as Dhirendra Pratap
*Mohnish Behl as Narendra Pratap (Dhirendras son)
*Alok Nath as Mr. Sharma
*Sushma Seth as Ravis Mother
*Dalip Tahil as Ramakant Sahai (Rajas father)

==Reception==
Deewana was the second highest grossing film of 1992, grossing at   170&nbsp;million and was given the verdict of a blockbuster by Box Office India.   

==Soundtrack==
{{Infobox album 
| Name = Deewana
| Type = soundtrack
| Artist = Nadeem-Shravan
| Cover = 
| Released =
| Genre = Film soundtrack
| Length = 
| Producer    = 
| Label  = Venus Music
| Reviews =
| Last album = Paayal (1992)
| This album = Deewana (1992)
| Next album = Bekhudi (1992)
}} Filmfare Award and Kumar Sanu won Filmfare Award for Best Male Playback Singer in a row after Aashiqui (1990) and Saajan (1991). Shah Rukh Khans career debut begins with the song "Koi Na Koi Chahiye" along with singer Vinod Rathod.

{| class="wikitable "
|-
! No !! Title !! Singer(s) !! Lyrics !! Length
|-
| 1. || "Aisi Deewangi" || Vinod Rathod, Alka Yagnik || Sameer || 06:59
|-
| 2. || "Sochenge Tumhe Pyar" || Kumar Sanu || Sameer || 06:03
|-
| 3. || "Teri Umeed Tera Intezar" || Kumar Sanu, Sadhna Sargam || Sameer || 06:19
|-
| 4. || "Payaliya" || Kumar Sanu, Alka Yagnik || Sameer || 07:57
|-
| 5. || "Tere Dard Se Dil" || Kumar Sanu || Sameer || 04:51
|-  
| 6. || "Teri Isi Ada Pe Sanam" || Kumar Sanu, Sadhna Sargam || Sameer || 05:12
|-
| 7. || "Koi Na Koi Chahiye" || Vinod Rathod || Sameer || 06:23
|-
| 8. || "Teri Umeed Tera Intezar (I)" || Kumar Sanu, Sadhna Sargam || Sameer || 02:13
|}

== Awards ==
;Filmfare awards Best Lyricist Sameer for "Teri Umeed Tera Intezar" Best Male Playback – Kumar Sanu for "Sochenge Tumhe Pyar" Best Music Director – Nadeem-Shravan Lux New Face – Divya Bharti Best Debut – Shah Rukh Khan

==Sequel==
Deewana sequel is planned by the producers and directors it will go on floors in 2013 or 2014. The story of the sequel will be different from the original film.  Casting is on for the film and it has been announced.
Nadeem-Shravan the original musical duo will once again return to compose for the sequel.

==References==
 

== External links ==
*  

 
 
 
 
 