Rewind Life
{{Infobox film
| name = Rewind Life
| image = Rewind_Life.jpg
| caption =  Official poster
| director = Luiz Saneti
| producer = Luiz Saneti
| writer = Luiz Saneti 
| starring = Stela Marianno Adriano Baluz Marina Vásques Emanuel Risaint
| music = Luiz Fernando Saneti
| cinematography = Luiz Saneti
| editing = Luiz Saneti
| studio = Luiz Saneti Entertainment
| released =  
| runtime = 94 min
| country = Brazil
| language = Portuguese
}}
Rewind Life ( ) is a 2011 Brazilian mystery film|mystery-supernatural film directed and written by Luiz Sanetia. It stars Stela Marianno, Adriano Baluz, Marina Vásques and Emanuel Risain.

==Plot==
The film tells the story of two journalists investigating the mysterious death of a woman who got in the 20s to discover the source of eternal life using some rare books stored inside the National Library. Vatican officials have come to Brazil to warn that the angel of darkness Lucifer is acting on the earth through some followers with the intent to possess immortality. However, the code is stored in one place in the confines of the human mind. And the reversal of life is only option to save humanity.

==Cast==
* Stela Marianno ... Ana
* Adriano Baluz ... Beto
* Emanuel Risaint ... Husayn
* Calu Lobo ... Laura
* Samuel Cruz ... Priest Bruna
* João Valzack ... Pedro
* Gastón Stefani ... Lucas
* Viviane Adriano ... Teacher Débora
* Esther Delamare ... Dona Torres (Jéssica adult)
* Helena Dias ... Nurse
* Greyce Kelly ... Jéssica
* Marina Vásques ... Maria
* Reinaldo Fusco ... Librarian

==External links==
*  
*  

 
 
 
 
 
 


 
 