The Joe Louis Story
{{Infobox film
| name           = The Joe Louis Story
| image          =Poster of the movie The Joe Louis Story.jpg
| image_size     =
| caption        = Robert Gordon
| producer       = Stirling Silliphant (producer)
| writer         = Robert Sylvester (writer)
| narrator       =
| starring       = See below
| music          = George Bassman
| cinematography = Joseph C. Brun
| editing        = Dave Kummins
| distributor    =
| released       = 1953
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Robert Gordon.

== Plot summary ==
This biographical movie tells the story of JOE LOUIS rising from poverty, and becoming known as the "Brown Bomber" - Heavyweight Champion of the world!
... Includes some great, real-life film clips of Louis in Action! - Directly from the DVD box.

== Cast ==
*Coley Wallace as Joe Louis
*Hilda Silmms as Marva Trotter Louis Paul Stewart as Tad MclGeehan James Edwards Jack "Chappie" Blackburn
*John Marley as Mannie Selamon
*Dots Johnson as Julian Black
*Evelyn Ellis as Mrs. Barrows
*Carl Rocky Latimer as Arthulr Pine John Marriott as Sam Langford
*Ike Jones as Johnny Kingston
*P. Jay Sidney as John Roxborough, Handler Mike Jacobs
*Herbert Ratner as Newspaper man
*Ruby Goldstein as Himself
*Norman Rose as Lieutenant
*David Kurlan as Bartender
*Ralph Stantley as Nick, the Announcer
*Shorty Linton as Himself Anita Ellis as Herself - Nightclub Vocalist
*Ellis Larkins as Himself
<!--*William Thourlby appears uncredited as Max Schmeling.
*Abe Vigoda Uncredited (Cornerman in boxing scene)
-->
== Soundtrack == Anita Ellis and the Ellis Larkins Trio - "Ill Be Around" (by Alec Wilder)

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 




 