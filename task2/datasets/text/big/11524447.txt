Szerelmi álmok – Liszt
{{Infobox Film
| name = Szerelmi álmok – Liszt
| director = Márton Keleti
| writer = D. Delj
| starring =  Imre Sinkovits Ariadna Shengelaya Sándor Pécsi
| music = Franz Liszt Ferenc Farkas
| cinematography = István Hildebrand
| editing = Mihály Morell
| studio = MAFILM Lenfilm
| distributor = Ellman Film Enterprises (USA)
| released = 1970
| runtime = 174 minutes
| country = Hungary Soviet Union
| language = Hungarian English German French Russian
}}                   
                                produced and directed by Márton Keleti, based on the biography of the Hungarian composer and pianist Franz Liszt.

While the movie was criticized for some of its historical inaccuracies, its epic scope and intense scenes of virtuoso musical performances won wide praise and has been credited with affecting the cultural landscape of the 1970s Eastern Europe.

==Plot summary==
An epic film about the Hungarian virtuoso pianist and composer Franz Liszt. He is an international star giving performances all over Europe and goes on a concert tour to Saint Petersburg|St. Petersburg, Russia. Liszts brilliant piano playing impressed the Russian royalty and aristocracy. Even the Russian Tsar stops talking when Liszt plays his piano. Liszt becomes a friend of the Russian composer Mikhail Glinka|Glinka. Liszts beautiful music touches everyones heart. Women are pursuing him and his lengthy affair with countess Marie dAgoult is in trouble.
 Princess Carolyne, they fall in love, and she soon leaves her husband for Liszt. She becomes a muse and inspiration for Liszt, and his last and strongest love. Inspired by his love for Carolyne, Liszt creates the most beautiful romantic piano composition, "Liebesträume|Liebestraum" (also known as "Dream of Love") dedicated to her, and the piece becomes a classic hit. But the church does not allow Liszt to marry Carolyne, because she could not terminate her first marriage. The unmarried couple moves to the city of Weimar, where Liszt becomes the music director for the royal orchestra. This becomes the most productive and happy period in Liszts life.

The brilliant pianist and composer Franz Liszt becomes a superstar. He tours many countries and makes people happy with his music, albeit his love life is in trouble. Carolyne cannot terminate her marriage while her husband is alive. Her relatives are against Liszt. She and Liszt remain unmarried, and Liszt suffers from emotional pain until the end of his life. Being loved by the public, Liszt is never really happy in his personal life, so he expresses himself making beautiful music.

== Cast ==
* Imre Sinkovits – Liszt Carolyne
* Sándor Pécsi – Belloni
* Klara Luchko – Marie dAgoult
* Igor Dmitriev – Wittgenstein
* Larissa Trembovelskay – Lola Montez
* Irina Gubanova – Olga Janina
* Tamás Major – Pope Pius IX
* Klári Tolnay – Cosima
* Lajos Básti – Ágost Trefort, minister of culture
* Ferenc Bessenyei – Vörösmarty
* Ádám Szirtes – Miska
* Petr Shelokhonov – Mikhail Glinka|Glinka, Russian composer
* Sándor Suka – Haydn, Austrian composer
* Natalya Baytalskaya
* Gennadi Bednostin
* Tibor Bitskey
* Kornél Gelley
* Peter Huszti
* Sergei Ivanov
* György Kálmán
* Sergei Karnovich-Valua
* Zsolt Kocsy
* Ivan Kolejev
* Valentin Kulik
* Vasili Leonov
* Gábor Mádi Szabó
* László Márkus
* Sergei Polezhayev
* Emmanuil Shvartsberg
* Anatoli Shvedersky
* Bertalan Solti
* Vera Szemere
* Géza Tordy
* Marina Yurasova
* Éva Ruttkai (voice dubbing for the character of Carolyne in the Hungarian version)
* Ildikó Pécsi (voice dubbing for the character of Lola Montez in the Hungarian version)

==Production==
* This is a joint Hungarian-Soviet production of MAFILM Studio 3 and Lenfilm Studio.
* Production dates: 1968–1970.
* Filmed in the Soviet Union, East Germany and Hungary.
* Liszts historic performance in Russia was filmed at St. Petersburg Bolshoi Philharmonic Hall.
* Sviatoslav Richter plays piano for the character of Liszt, including études and the famous "Liebesträume|Liebestraum".
* The Hungarian version runtime is 174 minutes. 
* The Soviet version is reduced down to 150 minutes, with some scenes deleted.
* The American version is reduced to 130 minutes, with many scenes deleted.
* Released in 1970 in Hungary and the Soviet Union.
* Released on September 29, 1972, in Finland, and in December 1975, in the USA.

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 