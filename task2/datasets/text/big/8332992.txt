Billa (2007 film)
 
 
{{Infobox film
| name = Billa 
| image = Billa 2007 poster.jpg image_size =
| caption = Promotional Poster
| writer = Rajkannan (dialogues) Vishnuvardhan
| story = Salim Khan Javed Akhtar Prabhu Nayantara Rahman Adithya_(actor)|Adithya Menon Vishnuvardhan
| producer = L. Suresh
| music = Yuvan Shankar Raja
| cinematography = Nirav Shah
| editing = A. Sreekar Prasad
| studio = Ananda Picture Circuit
| distributor = Pyramid Saimira Ayngaran International
| released =   
| runtime = 128 minutes
| country = India
| language = Tamil
| budget  20 crores
| gross = 70 Crores

}} Tamil mob Tamil film Adithya Menon Santhanam were also part of the cast. The film was produced by L. Suresh, while featuring musical score and soundtrack by Yuvan Shankar Raja, cinematography by Nirav Shah and editing by A. Sreekar Prasad.
 61st Cannes Film Festival.  

==Plot==
The underworld don David Billa (Ajith Kumar), featured on Interpols criminal list, is hiding and operating out of Malaysia. DSP Jayaprakash (Prabhu) has spent the last few years looking for Billa, leaving behind a life in India.

During a chase with the police Billa is severely wounded after an accident and dies in front of the DSP. The DSP then secretly holds a burial of Billa. Interpol officer Gokulnath (Rahman (actor)|Rahman) is assigned to work with DSP Jayaprakash to capture elusive Billa as no-one knows of Billas death. DSP Jayaprakash keeps the death of Billa as a secret even from his fellow officers, and tracks down a look-alike called Saravana Velu (Ajith Kumar), a lowly pick-pocket. He asks Velu to infiltrate Billas gang by pretending to be Billa. In return he will make sure that the child Velu adopted, Karan, gets a proper education.

The DSP trains Velu and sends him back to Billas gang as a person who has lost his memory. Slowly Velu starts to learn about Billas gang and even speaks to Jagdish, Billas boss, on the phone. Velu provides a pen drive with the secret information of the crime network to the DSP, but he is about to be killed by Sasha (Nayantara) because her brother Rajesh as well as his fiancee Rhea (Rose Dawn) were killed by Billa. At this juncture, the DSP arrives and tells her that he is Velu and not Billa. Later before a party, Velu secretly provides information to the DSP about a meeting of Billas network and C.J. (Namitha), Billas girlfriend, overhears his conversation. She challenges Velu but in the fight, he accidentally kills her. A shootout occurs at the party and the DSP is secretly killed by Jagdish, leaving his gun behind. Velu finds the DSPs body and the gun, but is taken into the custody of the police team, now headed by Interpol Officer Gokulnath. He argues during interrogation that he is Velu and not Billa to Gokulnath. Velu mentions a piece of evidence – the pen drive which may prove his innocence but the pen drive is nowhere to be found.

Unable to prove his innocence, he escapes from a police van. He phones Gokulnath and ask him to meet at the Aero bridge. Here it is revealed that Gokulnath is none other than the Underworld Crime Don Jagdish(Rahman) and he is the one who killed Jayprakash.
Officer Anil Menon apparently had the pen drive all along and strikes a deal with Velu to get hold of Jagdish. Meanwhile, Sasha and Karan have been kidnapped by Jagdish and wants the pen drive in return for it. Velu meets Ranjith and gives him a second pen drive with the same data, but corrupted. When Ranjith tries to kill Velu, a scuffle ensues making Ranjith fall from the top. In a final confrontation, Jagdish fights with Velu. Jagdish posing as Gokulnath asks the police to arrest Velu as Billa but gets shot by the squad of police and dies as the police have wired the entire conversation between Jagdish and Velu, thus proving his innocence. Velu finally hands over the original pen drive to Officer Menon as the film ends.

 

==Cast== David Billa and Saravana Velu Prabhu as DSP Jayaprakash Sasha
* Namitha as C. J. Rahman as Jagdish/Gokulnath Santhanam as Krishna Adithya Menon as Anil Menon
* Vimalraj Ganesan as John
* Yog Japee as Ranjith
* Rajesh Manikka
* Avnish Kalla R Parekh as Karan Dippa

* T. Balasubramaniam as Rajesh
* Hazel Keech as Rhea, Rajeshs fiancé

==Production==

===Development=== Vishnuvardhan had directed three films, Kurumbu, Arinthum Ariyamalum and Pattiyal, two of which were successful. Billa, was started after a debacle surrounding his other venture, Sarvam, which was eventually postponed to make way for Billa.
 yesteryear film Naan Avanillai.
 Vishnuvardhan and Tamil New AVM studios in Vadapalani, Chennai.  Among the attendees were the stars of the old cast, Rajinikanth, Sripriya, Suresh Balaji as well as noted directors Mani Rathnam, Dharani (director)|Dharani, K. S. Ravikumar, and Saran (director)|Saran. The confirmed cast until the date of the launch also were invited, among them being Ajith Kumar along with his wife, Shalini; Nayanthara, Namitha and Prabhu Ganesan.

===Filming===
 
Apart from the casting of  , she refused to accept the film. Despite other actreses such as Trisha Krishnan,       Reema Sen,    Asin Thottumkal    and Bhanu being considered,    the role was eventually given to Pooja Umashankar.    However, she refused the role, citing that she was reluctant to appear in a bikini, as the role required. Subsequently, Namitha, another actress was signed up for the role.      
 Santhanam replaced Helen in the original was originally given to Mumaith Khan,    but was later changed to newcomer, Rose Dawn, for unknown reasons.

For his crew, Vishnuvardhan picked his preferred technicians; with Rajkannan as the dialogue writer, Nirav Shah as the cinematographer, William Ong as the stunt master, Thotta Tharani as the art director, Pa. Vijay as the chief lyricist and A. Sreekar Prasad as the editor. Vishnuvardhans orthodox music composer, Yuvan Shankar Raja was chosen, creating great expectations for the project, while Vishnuvardhans wife, Anu, debuted as a costume designer.  The film was entirely shot in Malaysia at locations including Langkawi, Kuala Lumpur overlooking the Petronas Towers and other parts.

==Soundtrack==
{{Infobox album Name = Billa Type = soundtrack Artist = Yuvan Shankar Raja Cover = Released = 26 November 2007 Recorded = 2007 Genre = Feature film soundtrack Length = 24:30 Label = Ayngaran Music Producer = Yuvan Shankar Raja Reviews = Last album = Machakaaran (2007) This album = Billa (2007) Next album = Vaazhthugal (2008)
|
}}
For the films music and soundtrack, Vishnuvardhan renewed his previous association ( .    Nearly 22,000 CDs and cassettes were said to be sold out by the first day evening all over Tamil Nadu. 

The album received very positive reviews. Pavithra Srinivasan from Rediff noted that Billas music "rocks" and gave it four out of five. The reviewer wrote: "Yuvan Shankar Raja has etched an intricate musical feast to compliment the tale – and quite a feast its turned out to be, a la the original". She concluded  The guitar-ridden theme song was the most appreciated song, with a reviewer from Indiaglitz.com writing that the track was a "masterly work from young Yuvan...  ... takes us to a different world",  while another reviewer from Behindwoods wrote that it was the "soul of the film" which "stands out as the highlight of the album, giving that eerie feel required of a film about a don" and an "interesting composition with arrangements that spell international quality". 

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
! No. !! Song !! Singers !! Length (m:ss) !! Lyrics !! Notes
|- original film, composed by M. S. Viswanathan and written by Kannadasan with new lyrics by Pa.Vijay
|-
| 2|| "Naan Meendum" || Deepika || 04:46 || Picturized on CJ trying to impress "Billa (character)|Billa"
|-
| 3|| "Sei Yedhavathu Sei" || Neha Bhasin, Preethi Balla || 04:50 || Picturized on Rhea seducing Billa, before attempting to arrest him
|-
| 4|| "Seval Kodi" || Vijay Yesudas & Chorus || 04:53 || The introduction song for Velu, the Billa look-alike
|-
| 5|| "Vethayala Pottendi" ||  , composed by M. S. Viswanathan and written by Kannadasan.
|-
| 6|| "Theme Music" || Instrumental || 01:44 || – ||
|}

==Release==
The satellite rights of the film were acquired by Kalaignar TV|Kalaignar. The film was given a "U/A" certificate by the Indian Censor Board.

==Reception==

===Box office===
The film was released in 200 plus screens in Tamil Nadu and 50 screens overseas.  Made on a budget of  150&nbsp;million and sold to Ayngaran International.  In its opening weekend, Billa collected abot  3 crores.  In Chennai alone it collected  5.20&nbsp;million. The film completed a 100-day run at theatres and was declared a blockbuster (entertainment)|blockbuster. It is reported to have grossed more than  40&nbsp;crores worldwide.  In Kerala, it was supposed to have collected more than top Malayalam films of the year. 

===Critical response===
Billa opened to primarily positive critical response.  Indiaglitz called it a "stylish adaption" and wrote: "The movie does impress in its stunningly impressive making with awesome cinematography and sleek editing. It outsmarts the original in these areas   Even the stunt scenes are far better than the original..." The reviewer further noted: "Vishnu s presentation is stylish and sleek. But the crime thriller lacks credibility in some areas".  Sify lauded the film, writing: Billa delivers the goods with its great star cast, a designer look, technical glitz, perfect chemistry making it an entertainment extravaganza". The reviewer claimed that it was the "first designer-look Kollywood film with classy action cuts" and a "technically chic, racy, engrossing entertainer with a Hollywood look", going on to call it "racy & rocking". The critic also heaped praise on the lead actor: "Ajith looks sensational and clearly he is at home, playing dual roles of Billa and Velu. He is suave, dashing, and debonair and has terrific screen presence which makes the film work big time. You just cannot think of any other actor in Tamil donning the role made memorable by Rajinikanth".  Behindwoods rated the film 3.5 out of 5 and wrote: "...where Vishnu Vardhan scores is in his crystal clear vision. He doesn’t want to challenge the original nor change its content. All he wants to do is to repackage it stylishly for today’s trend with some present avant-garde styles in film making. The deliberate attempts by the director not to follow the super star’s style are palpable in every frame which has made Billa stand out tall and high". In regard of Ajith Kumars presence, the reviewer said: "Ajith as the ruthless David Billa is a revelation. Stylish, menacing and electrifying, he seems to have thoroughly enjoyed the challenge and has completely lived up to it. Rajinikanth can definitely be proud of his successor he has chosen for the tough job". 

Mythili Ramachandran of Nowrunning.com wrote: Billa   lives up to its hype and Ajith simply rocks", rating it 3 out of 5. Another reviewer from the same site, gave the same rating, citing: "Billa   is slick and stylish in keeping with the present modern world...View the movie from technical angle and it is undoubtedly in par with Hollywood movies. What a pulsating thriller it has turned out to be".  In contrast, TSV Hari from Rediff wrote that the film "disappoints" and gave it 2.5 out of 5, further claiming: "Director Vishnuvardhan seems to have been in a dilemma as to whether to focus on Ajith or give the film well-etched characters. There are too many diversions in the form of female cleavages in the rain forests of Malaysia and garish sets". 

==Prequel  ==
 
In 2008, reports claimed that, following the films commercial success, Soundarya Rajinikanth was planning to make a sequel, to be produced by Ocher Studios in association with Warner Bros.   However, the sequel did not materialise and the idea was dropped, with Ajith Kumar, Vishnuvardhan and Soundarya getting busy with other projects.
 Hinduja group company.   However, in a turn of events, Vishnuvardhan was replaced by Chakri Toleti,   and a new script was written by Toleti and his assistants.  The prequel was released at 13 July 2012.    

==References==
 

==External links==
*  
*  

==Bibliography==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 