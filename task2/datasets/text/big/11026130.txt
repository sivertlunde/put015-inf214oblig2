Decaying Orbit (film)
 
{{Infobox film
| name           = Decaying Orbit
| image          = Decorbpic.jpg
| director       = Tim Pyle
| writer         = Tim Pyle
| starring       = Darren Schnase Osa Wallander Denise Gossett Andy Allen David Paterson Erin Shull Bill Guzenski
| cinematography = Tom Phillips
| distributor    = Arsenal Pictures

| released       =  
| runtime        = 90 minutes
| country        = United States

| language       = English
}}
Decaying Orbit is a 2007 independent film directed by Tim Pyle and produced by Hogofilm, LLC., a production company in Southern California. It was filmed in the San Fernando Valley, with hundreds of computer graphics (CG) special effects produced in-house by Hogofilm.

==Plot==
When a mysterious explosion destroys a space station on the edge of the galaxy, five survivors manage to board an escape shuttle. Left adrift without communications, food or any means of leaving the system, the survivors are forced to work together to survive. But soon evidence surfaces that one of them may actually be the saboteur who caused the stations destruction. Can the dysfunctional group band together long enough to be rescued, when no one knows who they can trust?

==Availability==
In 2007, the film was sold to Arsenal Pictures for distribution to Japan and Brazil. In 2008, Angerman Distribution picked up the film for distribution in Australia and New Zealand. The film was distributed on DVD in the U.S. through Hogofilm, LLC.

==Awards==
The film was nominated for three awards at the 6th annual Shockerfest Film Festival:  Best Sci-Fi Feature Film, Best Actress in a Sci-Fi/Fantasy Film (Osa Wallander), and Best Actor in a Sci-Fi/Fantasy Film (Andy Allen). Osa Wallander won the Best Actress award.

The film was also nominated for Best Feature Film at the 2007 Terror Film Festival. 

==Cast and crew== Telly Award, a CINE Golden Eagle, and 2006 and 2008 NASA awards for producing CG animation.

The actress Denise Gossett is the founder of Shriekfest, a Los Angeles-based horror/sci-fi/fantasy film festival. She has appeared in numerous TV and film productions including Veronica Mars, Drake & Josh and Zoey 101.

The actor David Paterson has appeared in many TV and film productions including the series Lost (TV series)|Lost, The Bold and the Beautiful and the long-running Australian soap opera, Neighbours.

==References==
 

== External links ==
*  
*  

 
 
 


 