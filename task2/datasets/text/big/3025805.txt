Undertow (2004 film)
{{Infobox film
| name           = Undertow 
| image          = Undertow.jpg
| image_size     = 
| caption        = 
| director       = David Gordon Green
| producer       = Terrence Malick Edward R. Pressman Lisa Muskat
| writer         = Lingard Jervey  Joe Conway  David Gordon Green
| narrator       = 
| starring       = Jamie Bell  Dermot Mulroney  Devon Alan  Shiri Appleby Josh Lucas
| music          = Philip Glass
| cinematography = Tim Orr
| editing        = Zene Baker  Steven Gonzales
| distributor    = United Artists
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $143,597
| preceded_by    = 
| followed_by    = 
}}
Undertow is a 2004 thriller film directed by David Gordon Green, starring Jamie Bell, Devon Alan, Dermot Mulroney and Josh Lucas. Taking place in Georgia (U.S. state)|Georgia, the film tells the story of two boys pursued by a murderous uncle. 
 
Undertow is Greens third feature film. Met with a mixed response from critics,  .   .  the film received special recognition for excellence in filmmaking from the National Board of Review of Motion Pictures.     In addition, Jamie Bell and Devon Alan won Young Artist Awards for their roles in the film.  

==Plot==
The protagonist is Chris Munn (Jamie Bell), a troubled and restless teen. His family consists of his younger brother Tim (Devon Alan) and their father John (Dermot Mulroney). They live in an isolated rural house in Georgia (U.S. state)|Georgia.
 
The story of the film is one of greed and family hatred. It begins when Johns brother Deel (Josh Lucas) visits the Munn family, stirring up unease among them. It turns out that Deel wishes to reclaim a hoard of gold coins from John. He eventually finds them hidden behind Johns family portrait. John refuses to give them up. In the ensuing struggle, Deel murders him. He tries to kill Chris and Tim too, but they escape him and run away from home. Chris brings the gold coins along with him.

On the run, the boys meet an assortment of fairytale-like characters. Deel pursues them, eventually catching up. Wading into a river, Chris throws away the gold coins into the water. Enraged by their loss, Deel struggles with Chris and tries to drown the boy. In turn, Deel receives a fatal stab wound in the chest.
 
Chris appears to wake up in hospital. There, he is reunited with Tim and their grandparents.

==Cast==
* Jamie Bell - Chris Munn 
* Dermot Mulroney - John Munn 
* Devon Alan - Tim Munn 
* Shiri Appleby - Violet
* Josh Lucas - Deel Munn 
* Terry Loughlin - Officer Clayton 
* Robert Longstreet - Bern 
* Eddie Rouse - Wadsworth Pela 
* Patrice Johnson - Amica Pela 
* Charles "Jester" Poston - Hard Hat Dandy
* Kristen Stewart - Lila

==Critical reception==
The film received mixed reviews from film critics, whose responses ranged from admiration to derision.   On Rotten Tomatoes, it currently holds a "rotten" rating of 55% based on 116 reviews.  On Metacritic, the film earned a metascore of 63% based on 30 reviews. 

Among the critics who gave the film a positive review were Roger Ebert, who praised the film, giving it a full four stars. He wrote of the director, "Green has a visual style that is beautiful without being pretty. We never catch him photographing anything for its scenic or decorative effect."
 {{cite news
  | last = Ebert
  | first = Roger
  | title = Movie Reviews: Undertow (R)
  | date = 22 October 2004
  | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20041021/REVIEWS/40923002/1023
  | accessdate = June 2008 The Night of the Hunter, and wrote, "the movie builds slowly to its grinding climax, and the suspense – the standard by which a thriller must primarily be judged – is first-rate." 

==Awards==
{| class="wikitable"
|-
!Event !! Award !! Winner/Nominee !! Result
|-valign="top" National Board 2004 National Board of Review Awards {{cite web
  | title = The National Board of Review: Awards for 2004
  | url = http://www.nbrmp.org/awards/past.cfm?year=2004
  | accessdate = June 2008}}   Special Recognition For Excellence In Filmmaking
|Undertow
| 
|-valign="top" 2004 Deauville American Film Festival  Grand Special Prize David Gordon Green 
| 
|-valign="top" 2005 Young Artist Awards {{cite web
  | title = 26th Annual Young Artist Awards
  | url = http://www.youngartistawards.org/noms26.htm
  | accessdate = June 2008}}   Best Performance in a Feature Film:  Leading Young Actor Jamie Bell 
| 
|-valign="top" Best Performance in a Feature Film:  Supporting Young Actor Devon Alan 
| 
|-valign="top" Best Performance in a Feature Film:  Supporting Young Actress Kristen Stewart 
| 
|}

==References==
 

==External links==
* 
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 