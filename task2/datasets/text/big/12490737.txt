Molière (2007 film)
{{Infobox Film 
| name           = Molière
| image          = Moliere film 2007.jpg
| caption        = Original film poster
| director       = Laurent Tirard
| producer       = Olivier Delbosc Marc Missonnier 
| writer         = Laurent Tirard Grégoire Vigneron 
| starring       = Romain Duris Laura Morante Ludivine Sagnier 
| music          = Frédéric Talgorn
| cinematography = Gilles Henry  
| editing        = Valérie Deseine
| studio         = Fidélité Productions France 2 Cinéma France 3 Cinéma Wild Bunch
| released       =  
| runtime        = 120 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Molière is a 2007 film by French director Laurent Tirard and starring Romain Duris as Molière. It was released in Europe in January 2007 and in the United States in July 2007. It was entered into the 29th Moscow International Film Festival where Fabrice Luchini won the Silver George for Best Actor.    The screenplay was co-written by Tirard and Grégoire Vigneron.

==Plot== flashback to 1645.  Following an unsuccessful run as a tragic actor, Molière is released from debtors prison by Monsieur Jourdain (Fabrice Luchini), a wealthy commoner with social pretensions, who agrees to pay the young actors debts if Molière teaches him to act. 

Jourdain, a married man with two daughters, hopes to use this talent to ingratiate himself with Célimène (Ludivine Sagnier), a recently widowed aristocrat with whom he has become obsessed. He hopes to perform a short play he has written for the occasion. Molière, however, who has been presented to the family and staff of Monsieur Jourdain as Tartuffe, a priest who is to serve as tutor for the Jourdains younger daughter. As the story progresses Molière proceeds to fall in love with Jourdains neglected wife, Elmire (Laura Morante).  Sub-plots involve the love life of the Jourdains older daughter, and the intrigues of the penniless and cynical aristocrat Dorante (Édouard Baer) at the expense of the gullible Jourdain.  

The story is mostly fictional and many scenes follow actual scenes and text in Molières plays including Tartuffe, Le Misanthrope, The Imaginary Invalid, and Le Bourgeois Gentilhomme, whose principal character is also named Jourdain. It is implied that these "actual" events in his life inspired the plays of his maturity.

==Cast==
* Romain Duris as Jean-Baptiste Poquelin
* Fabrice Luchini as M. Jourdain
* Laura Morante as Elmire Jourdain
* Edouard Baer as Dorante
* Ludivine Sagnier as Célimène
* Fanny Valette as Henriette Jourdain
* Gonzague Montuel as Valère
* Gilian Petrovski as Thomas
* Sophie-Charlotte Husson as Madeleine Béjart
* Anne Suarez as Catherine de Brie
* Annelise Hesme as Marquise du Parc

==Critical reception==
The film holds a "certified fresh" rating of 70% on review aggregator website Rotten Tomatoes, based on 86 reviews, with a rating average of 6.4 out of 10. The sites consensus reads: "Molière is a sophisticated, witty biopic of the great satirist." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 