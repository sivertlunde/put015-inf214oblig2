The Damned (1947 film)
 
{{Infobox film
| name           = Les Maudits (English: The Damned)
| image          =
| caption        =
| director       = René Clément
| producer       = André Paulvé Jacques Rémy
| starring       = Henri Vidal
| music          =
| cinematography = Henri Alekan
| editing        = Roger Dwyre Gaumont
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
}}
The Damned ( ) is a 1947 French drama film directed by René Clément.  It was entered into the 1947 Cannes Film Festival.    The film is notable for its depiction of the interior of a wartime submarine and for its tracking shots through the length of the U-boat.

==Plot==
As Germany is in the throes of losing World War II, a number of wealthy Nazis and some French sympathizers head for South America in a German submarine leaving from Oslo. The films narrator is a French doctor (Henri Vidal) who has been kidnapped to tend a sick woman, Hilde Garosi (Florence Marly), the wife of one man and the lover of another, both aboard. The doctor realizes he will be murdered at any point once the woman has recovered so he tries various stratagems to escape. All fail. 

The mission slowly disintegrates as the war ends and its reasons for being dissipate, with some passengers either trying to escape or committing suicide. Forster (Jo Dest well-cast as a True Believer in Nazism) tries to continue the mission even after Berlin has fallen and orders have gone out for all U-boats to surrender at the nearest port. Part of the crew finally mutinies against the insane ones still fighting the war. The doctor ends up alone on the Nazi sub for days writing his memoirs until an American ship rescues him and finally sinks his infamous abode at sea. 

In addition to Dest, Michel Auclair impresses favorably as Willy Morus, a young man happily following any orders amorally until the inanity of his position overcomes him and he kills Forster. Marcel Dalio also has a small but important role as Larga, a South American importer, who supports whatever faction is best for his business. Lastly, the film is interesting for its seeming accuracy of life aboard a wartime submarine right down to the food served the passengers as it was written and filmed shortly after the war ended. 

==Cast==
* Marcel Dalio as Larga (as Dalio)
* Henri Vidal as Docteur Guilbert
* Florence Marly as Hilde Garosi
* Fosco Giachetti as Garosi Paul Bernard as Couturier
* Jo Dest as Forster (as Jodest)
* Michel Auclair as Willy Morus
* Anne Campion as Ingrid Ericksen
* Andreas von Halberstadt (as A. Von Halberstadt)
* Lucien Hector as Ericksen
* Jean Lozach (as Lozach)
* Karl Münch (as Karl Munch)

==Restoration==
In 2013, the Cohen Film Collection released The Damned on Blu-ray and DVD in the US,  using a restoration carried out by the French distributor, Gaumont Film Company|Gaumont.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 