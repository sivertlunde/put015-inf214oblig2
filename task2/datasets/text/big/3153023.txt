Beverly Hills Ninja
{{Infobox film
| name           = Beverly Hills Ninja
| image          = Beverly Hills Ninja poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Dennis Dugan
| producer       = Bradley Jenkel Brad Krevoy Steven Stabler Mitch Klebanoff
| writer         = Mark Feldberg Mitch Klebanoff
| starring       = Chris Farley Nicollette Sheridan Robin Shou Nathaniel Parker Chris Rock Soon-Tek Oh
| music          = George S. Clinton
| cinematography = Arthur Albert
| editing        = Jeff Gourson
| studio         = Motion Picture Corporation of America
| distributor    = TriStar Pictures 
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English Japanese
| budget         = $18 million
| gross          = $31.4 million 
}}
 Beverly Hills to investigate a murder mystery. It was the last film released in Farleys lifetime. 

==Plot==
  Keith Cooke money counterfeiting Beverly Hills to search for Sally. Gobei (Robin Shou), Harus adoptive brother, is sent by the clans sensei (Soon-Tek Oh) to watch over and protect Haru, without letting Haru know of his presence.
 Little Tokyo, William Sasso) to help counterfeit money. Haru then disguises himself as Walters to gain access to Tanleys warehouse. Harus identity is revealed after he fails to counterfeit the money correctly, and Tanley captures him. While Tanley succeeds in getting the other half of the plates that night from the rival gang, Alison rescues Haru, however, gets herself kidnapped by Tanley. The next day Haru enlists Joeys help in finding the warehouse. After they fail, Gobei intervenes without Harus knowledge and leads them back to the warehouse.

Tanley locks Alison in a room with a bomb. Haru attempts to intervene but is overwhelmed by Tanleys guards. Gobei reveals himself to Haru, and is able to distract the guards, allowing Haru to rescue Alison. Haru attempts to defuse the bomb but fails. On hearing Gobei become overwhelmed by Tanleys guards, Haru leaves Alison to help Gobei. Haru saves Gobeis life and successfully defeats several guards himself. Haru and Gobei are left facing off Nobu and two guards. Joey, attempting to enter the building, crashes through a window and knocks himself and one of the guards unconscious. Haru and Gobei defeat Nobu and the remaining guard. Tanley then confronts Haru and Gobei. In the fight that follows, Haru accidentally knocks Gobei unconscious, but forces Tanley to flee afterwards. Haru returns to attempt to rescue Alison. Using a large harpoon gun mounted on a cart, Haru shoots a harpoon through the room which inadvertently lands in the back of the truck which Tanley is trying to escape in. The harpoon drags the bomb into Tanleys truck and explodes. Haru successfully rescues Alison, then Tanley and his surviving hitmen are captured by the LAPD.

Sometime later back in Japan, Haru tells his sensei he will be returning to Beverly Hills to live with Alison. Haru and Alison leave together on a bus. A grappling hook tied to a rope has fallen from the bus and hooks onto Gobeis wheelchair, causing him to be thrown into the ocean. Haru shouts an apology to Gobei.
 

==Cast==
* Chris Farley as Haru 
* Nicollette Sheridan as Alison Page/Sally Jones
* Robin Shou as Gobei
* Nathaniel Parker as Martin Tanley
* Chris Rock as Joey Washington Keith Cooke Hirabayashi as Nobu
* Soon-Tek Oh as Sensei William Sasso as Chet Walters
* François Chau as Izumo
* Jason Tobin as Busboy
* John P. Farley as Policeman
* Kevin Farley as Policeman
* Billy Connolly as Japanese Antique Shop Proprietor
* Patrick Breen as Desk Manager (uncredited) Steve Terada as Martial Artist (uncredited)

==Reception==
Beverly Hills Ninja received generally negative reviews. It holds a 14% "rotten" rating at review aggregator Rotten Tomatoes,  and holds a 27/100 score at Metacritic.   James Berardinelli panned the film, stating that "Beverly Hills Ninja is essentially a one-joke film. That joke has to do with Chris Farley  , who plays one of the clumsiest men on Earth, crashing into objects or having things fall on his head" and concluded that it "isnt just juvenile, its lackluster and unfunny."  Bruce Fretts of Entertainment Weekly criticized the film as well, complaining it had "...a yawner plot about Farley busting up a yen-counterfeiting ring" and that "...when the writers run out of ideas, they simply have Farley walk into a lamppost, or cop from old SNL skits." 

A favorable review came from Mick LaSalle of the San Francisco Chronicle who wrote that it is "not the kind of picture that gets respect from New York critics, but its funny.   This is a movie in which the audience knows half the gags in advance, but thanks to director Dennis Dugans timing and Farleys execution, the audience doesnt just laugh anyway, but laughs harder... hes too good, too funny and too in control of his out-of-controlness to be a mere buffoon." 

==Soundtrack==
{{Infobox album  
| Name        = Beverly Hills Ninja
| Type        = Soundtrack | Longtype = to Beverly Hills Ninja
| Cover       =
| Released    = January 14, 1997
| Genre       = Soundtrack
| Length      = 34:14
| Label       = EMI Records
| Producer    =
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =   
}}
;Track listing
# "Youre a Ninja?..."
# "Kung Fu Fighting" – Patti Rothberg Blondie
# "...We Are in Danger..."
# "Tsugihagi Boogie Woogie" – Ulfuls Low Rider" War
# "The blackness of my belt..."
# "Tarzan Boy" – Baltimora
# "...my identity must remain mysterious..."
# "Turning Japanese" – The Hazies
# "Youre the big, fat Ninja, arent you?"
# "Kung Fu Fighting" – Carl Douglas
# "Im Too Sexy" – Right Said Fred
# "...close to the temple, not inside"
# "I Think Were Alone Now" (Japanese version) – Lene Lovich
# "Finally Got It" – Little John
# "...Yes, I guess I did"
# "The End" – George Clinton & Buckethead

==Sequel==
Beverly Hills Ninja 2, a sequel written by Mitch Klebanoff and co-directed by Klebanoff and Kelly Sandefur. The film would feature David Hasselhoff and Lucas Grabeel, and began shooting scenes in South Korea in October 2008. {{cite news 
| url=http://www.variety.com/article/VR1117992322 
| title=Ninja 2 to shoot in South Korea  Variety 
| date=September 16, 2008 
| accessdate=August 19, 2011 
| author=Han Sunhee}}  During filming, the name was changed to Dancing Ninja and released in 2010. {{cite web
| url= http://www.dancingninjastudios.com/download_content.php?id=352 
| title= The Dancing Ninja (Beverly Hills Ninja 2) 
| publisher= dancingninjastudios.com 
| accessdate= August 19, 2011}} 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 