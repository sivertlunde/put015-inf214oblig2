Finders Keepers (1921 film)
{{Infobox film
| name = Finders Keepers
| caption =
| image	=
| director = Otis B. Thayer
| story = Robert Ames Bennett
| screenplay =
| adaptation = Art-O-Graf
| starring =
| music =
| cinematography =
| editing =
| distributor = Pioneer Pictures
| released =  
| runtime =
| language =
| country = United States
| budget =
}}
Finders Keepers  is a 1921 western silent film based on a book by Robert Ames Bennett and directed by Otis B. Thayer, starring Edmund Cobb and Violet Mersereau. The film was shot in Denver, Colorado by the Thayers Art-O-Graf film company.   

==Plot==

Amy Lindel, a church choir singer heads to the city to make a fortune with her voice and finds out she can only get jobs cabaret singing. Two men fall for her, one of which plants stolen diamonds on her. Threatened with arrest she throws herself in a lake, she is saved by the good guy who she marries.

==Cast==
*Edmund Cobb as Paul Rutledge
*Violet Mersereau as Amy Lindel
*Dorothy Bridges as Oliva Satterlee (née and credited as Dorothy Simpson)
* Verne Layton as Hobart Keith 
* S. May Stone as Mrs. Satterlee

==Crew==
*Otis B. Thayer Managing Director
*Vernon L. Walker Head Camerman
* H. Haller Murphy Cameraman

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 