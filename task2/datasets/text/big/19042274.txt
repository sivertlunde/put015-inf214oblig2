The Love Test
{{Infobox film
| name = The Love Test
| image = The Love Test.jpg
| caption = Judy Gunn and Louis Hayward
| director = Michael Powell
| producer = Leslie Landau
| writer = Jack Celestin (story) Selwyn Jepson (script)
| starring =Judy Gunn Louis Hayward David Hutcheson Googie Withers Bernard Miles
| cinematography = Arthur Crabtree
| editing =
| music = Charles Cowlrick
| distributor = Fox Film Company
| released = 1 July 1935 UK
| runtime = 63 min
| country = United Kingdom English
| budget =
}} Quota quickie.

==Plot==
When a woman is made the head of a chemistry laboratory, her colleagues hatch a plot to make her fall in love, and neglect her work duties. 

==Production==
The chemistry laboratory is trying to find a way to make the cellulose used to make toy dolls in a non-flammable form. There are obvious resonances to the problem with the highly flammable celluloid used to make films like this one. 

Withers made four quota quickies with Powell, who she found a "difficult man",. 

==Cast==
* Judy Gunn as Mary Lee
* Louis Hayward as John Gregg
* David Hutcheson as Thompson (as Dave Hutcheson)
* Googie Withers as Minnie
* Morris Harvey as Company President
* Aubrey Dexter as Vice-President
* Eve Turner as Kathleen
* Bernard Miles as Allan
* Jack Knight as Managing Director
* Gilbert Davis as Hosiah Smith, Chief Chemist
* Shayle Gardner as Night Watchman
* James Craig as Boiler Man
* Thorley Walters as Chemist Ian Wilson as Chemist

===Bibliography===
 
* Chibnal, Steve. Quota Quickies : The Birth of the British B Film. London: BFI, 2007. ISBN 1-84457-155-6
* Powell, Michael. A Life in Movies: An Autobiography. London: Heinemann, 1986. ISBN 0-434-59945-X.
 

==Notes==
 

== References ==
 

==External links==
*  
*  
*  
*   reviews and articles at the  

 

 
 
 
 
 
 