Madame Aema 2
{{Infobox film
| name           = Madame Aema 2
| image          = Madame Aema 2.jpg
| caption        = Theatrical poster for Madame Aema 2 (1984)
| film name      = {{Film name
 | hangul         =   2
 | hanja          =   2
 | rr             = Aemabuin 2
 | mr             = Aemapuin 2}}
| director       = Jeong In-yeob 
| producer       = Choe Chun-ji
| writer         = Park Yeong
| starring       = Oh Su-bi
| music          = Shin Byung-ha
| cinematography = Lee Seok-ki
| editing        = Kim Hee-su
| distributor    = Yeon Bang Films Co., Ltd.
| released       =  
| runtime        = 102 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Madame Aema 2 (hangul|애마부인 2 - Aemabuin 2) is a 1984 South Korean film directed by Jeong In-yeob.

==Plot== Jeju Island, Aema contemplates her current love affairs, and returning to her husband, who is now living with another woman. Resolving to become independent, Aema declares that love and marriage are separate things.   

==Cast==
* Oh Su-bi: Madame Aema 
* Ha Jae-young: Dong-yeob
* Sin Il-ryong: Sang-yeon
* Choe Yun-seok: Hyeon-woo
* Kim Ae-kyung: Erika
* Bang Hee: Hye-ryeon
* Hyeon Ji-hye: Ji-hee

==Bibliography==

===English===
*  
*  
*  

===Korean===
*  
*  
*  
*  

==Notes==
 
 
 
 
 
 
 


 
 