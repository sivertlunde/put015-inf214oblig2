Aa Nimisham
{{Infobox film
| name           = Aa Nimisham
| image          = AaNimisham.jpg
| image_size     =
| caption        = LP Vinyl Records Cover
| director       = IV Sasi
| producer       = KJ Joseph
| writer         = Sherif
| screenplay     = Sherif Madhu Sheela Sridevi Kaviyoor Ponnamma
| music          = G. Devarajan
| cinematography = C Ramachandra Menon
| editing        = K.Narayanan
| studio         = Cherupushpam Films
| distributor    = Cherupushpam Films
| released       =  
| runtime        = 130 min.
| country        = India Rs 9 Lakhs Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film,  directed by IV Sasi and produced by KJ Joseph. The film stars Madhu (actor)|Madhu, Sheela, Sridevi and Kaviyoor Ponnamma in lead roles. The film had musical score by G. Devarajan.    

==Plot==
Aa Nimisham is an emotional family drama.

==Cast==
  Madhu
*Sheela
*Sridevi
*Kaviyoor Ponnamma
*Ashalatha
*Francis Baby Sumathi
*Chandralekha
*Kuthiravattam Pappu Ravikumar
*Sarala
*Alex
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ayalathe Janalil || K. J. Yesudas || Yusufali Kechery ||
|-
| 2 || Chaayam Thecha || K. J. Yesudas, P. Madhuri || Yusufali Kechery ||
|-
| 3 || Malare Maathalamalare || K. J. Yesudas || Yusufali Kechery ||
|-
| 4 || Manasse Neeyoru || K. J. Yesudas || Yusufali Kechery ||
|-
| 5 || Paarilirangiya || P Jayachandran, P. Madhuri, Shakeela Balakrishnan || Yusufali Kechery ||
|}

==References==
 

==External links==
*  

 
 
 


 