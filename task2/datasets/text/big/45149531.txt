Jane Eyre (1910 film)
 
 
{{Infobox film
| name           = Jane Eyre
| image          = Jane Eyre Thanhouser.jpg
| caption        = A film still from the lost work
| director       =
| producer       = Thanhouser Company
| writer         =
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent film  English inter-titles
}} silent short short classic drama produced by the Thanhouser Film Corporation.   Adapted from Charlotte Brontës 1847 novel, Jane Eyre, the film mirrors the events and plot of the titular character and original book. The writer of the scenario is unknown, but Lloyd Lonergan probably adapted the work. The films director is often and erroneously claimed to be Theodore Marston,  but Barry ONeil or Lloyd B. Carleton are possible candidates. The cast of the film was credited, an act rare and unusual in the era.
 on his own: “I was like, holy crap, there are a lot of Jane Eyres out there." }}

The single reel film, approximately 1000 feet long, was released on May 6, 1910. It was later credited by Edwin Thanhouser as marking the assured success of the company. The popularity of the production resulted in the production of additional copies, so the Thanhouser company had more orders than could be filled. Critical reception to the film was generally positive, but with some minor criticisms. The film is presumed lost film|lost.

== Plot ==
This shortened and streamlined version of   by her unfeeling aunt. Five years later she leaves the asylum to   the position of governess to Lord Rochesters little niece. The child is the daughter of Rochesters dead brother. Her mother has become insane and is living in Lord Rochesters home, under his protection." 

"Jane is engaged by Lord Rochesters housekeeper, during his absence from home, and her first meeting with her employer is both exciting and romantic. She is sitting by the edge of the road reading when Lord Rochester rides up to his ancestral home. The sight of his huge dog, coming upon her suddenly, so startles Jane that she jumps to her feet, causing Lord Rochesters horse to shy and throw its rider. He injures his ankle, and has to be assisted to remount by the little witch, as he calls her, who is the cause of his accident. One evening the maniac escapes from her nurse and sets fire to the room in which Lord Rochester has fallen asleep. He is saved from a horrible death by Jane. When next Janes haughty aunt and cousins call upon Lord Rochester, they are just in time to be introduced to his bride, who is none other than the despised Jane Eyre." 

== Cast == Jane Eyre    
*Gloria Gallop  as Georginia Reed 
*Frank H. Crane as Lord Rochester  (Mr. Rochester in original)
*Amelia Barleon as Bertha Mason|Mrs. Rochester 
*Charles Compton as John Reed 
*Martin Faust as Uncle Reed 
*Irma Taylor 
*Alphonse Ethier 
*William Garwood 

== Production ==
The scenario for the film was adapted from Charlotte Brontës 1847 novel Jane Eyre. Brontës work was modeled after her own life. The book was considered a classic for many decades before the Thanhouser adaptation.  The film adaptation was not the first, the earliest known adaption being a 1909 Italian silent film.     The writer of the scenario is unknown, but it may have been Lloyd Lonergan. Lonergan was an experienced newspaperman still employed by The New York Evening World while writing scripts for the Thanhouser productions. He was the most important script writer for Thanhouser, averaging 200 scripts a year from 1910 to 1915. 
 Romeo and Juliet. Lloyd B. Carleton was the stage name of Carleton B. Little, a director who would stay with the Thanhouser Company for a short time, moving to Biograph Company by the summer of 1910.    Bowers does not attribute either as the director for this particular production nor does Bowers credit a cameraman.    Blair Smith was the first cameraman of the Thanhouser company, but he was soon joined by Carl Louis Gregory who had years of experience as a still and motion picture photographer. The role of the cameraman was uncredited in 1910 productions.   
 The Winters Tale as her second and last.  Charles Compton may have had his film career start in this Thanhouser production, but he was better known for his juvenile roles on the stage.  Bowers credits Martin J. Faust as one of the most important actors in for Thanhouser in 1910 and 1911, but Fausts role in productions were often went uncredited.  Both Irma Taylor and Alphonese Ethier were actors that appeared in Thanhouser productions with few credits.   The last identified member of the cast is William Garwood, who was among the most important actors at Thanhouser. He joined the company in late 1909 and remained until 1911 before returning in 1912. This is known as his first credited work with Thanhouser. 

== Release and reception ==
 
The single reel film, approximately   long, was released on May 6, 1910.  Publicity for the release of this film was handled by Bert Adler and was successful in generating trade interest and promised a better work then Thanhousers St. Elmo (1910 Thanhouser film)|St. Elmo from the previous month. The high expectations for the film were picked up and included in subsequent articles in The Moving Picture World and The New York Dramatic Mirror in advance of its release. Also, the players in the production were credited for their work, something which was rare and unusual at the time. Edwin Thanhouser would later mark Jane Eyre as the point in which he became confident in the success of the company. The release saw the new company suddenly having more orders than it could fill and the laboratory had to work overtime to produce additional prints to meet the demand.    The popularity of the stage production in advertisements makes identifying the showings of the film more difficult than other Thanhouser productions of the time, but theaters across the nation displayed advertisements for the film. Theaters include Indiana,  Kansas,  Missouri, 
and North Carolina. 

Jane Eyre helped secure the future of the Thanhouser Company and reviewers were largely positive with only minor criticism about the acting or photography.  The Morning Telegraph said the production was excellent save for the lack of emotion displayed over the death of Uncle Reed. There were two reviews of the film in The Moving Picture World, both of which were positive. The first review was positive for its acting and clear adaption, but cautioned itself against further flattery of Edwin Thanhousers new company. The reviewer noted there were signs of inexperience the companys best work   in the fall from the horse having been depicted as instead as a clumsy dismount. The second review was much more positive, it praised the adaptation and acting, but found the photography to not be of the same standard as its previous work.  The film is presumed lost film|lost. 

==See also==
*Adaptations of Jane Eyre

==References==

===Notes===
 

===Citations===
 

==Further reading==
* 
* 224 |isbn=0-19-284035-5 |id=ISBN 978-0-19-284035-6 |work=Oxford World’s Classics |location=Oxford |publisher=Oxford University Press |accessdate=15 February 2015}}
* 

 

 
 
 
 
 
 
 
 
 
 