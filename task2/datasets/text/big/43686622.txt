Premanu Apedevaru
{{Infobox film
| name           = Premanu Apedevaru
| image          = 
| alt            = 
| caption        = Theatrical poster
| director       = Sreekanth Vanga
| producer       = IQlick Movies & V.Pictures
| writer         = Sreekanth Vanga
| screenplay     = Sreekanth Vanga
| story          = Sreekanth Vanga
| starring       = Varun Rao   Divya Neelakanta 
| cinematography = Shiva Shankara Vara Prasad
| editing        = D. Mallesh
| studio         = 
| distributor    = 
| released       =   
| runtime        = 13 minutes
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}}

Premanu Apedevaru is a 2014 psychological drama Telugu short film written and directed by Sreekanth Vanga. It starred Varun Rao, Divya Neelakanta in the lead roles. The film explores real human emotions through the relationship between the main character and the heroine.The film employs a nonlinear narrative structure, with the true story based upon things that never actually happened, and some that did.
The major highlight of this film is dialogues and screenplay.

==Cast==

* Varun Rao
* Divya Neelakanta

==Reception==
The film was heavily panned by the critics at the time of its release. the famous Telugu cinema website chekodi.com given rating of 4/5, the director said in an interview, "the theme is philosophical, but when I said it in a different way, some people were unable to digest it. 

==References==
 

==External links==
*  

 
 
 


 