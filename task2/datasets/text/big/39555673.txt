The Stranger from Alster Street
{{Infobox film
| name           = The Stranger from Alster Street
| image          = 
| image_size     = 
| caption        = 
| director       = Alfred Tostary 
| producer       = 
| writer         = Demy Passau
| narrator       = 
| starring       = Margit Barnay   Olaf Storm   Eduard von Winterstein   Maria Forescu
| music          = 
| editing        =
| cinematography = Curt Courant
| studio         = Olaf-Film 
| distributor    = Globus-Film 
| released       = 7 June 1921
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent drama film directed by Alfred Tostary and starring Margit Barnay, Olaf Storm and Eduard von Winterstein. It premiered in Berlin on 7 June 1921. 

==Cast==
* Margit Barnay  
* Olaf Storm  
* Eduard von Winterstein  
* Maria Forescu   
* Ilka Grüning   
* Frida Richard   
* Wilhelm Diegelmann   
* Georg John   
* Hermann Picha   
* Josefine Dora   
* Emil Mamelok   
* Harry Gondi   
* Berta Christians-Klein

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 


 
 