Band of the Hand
 
 
{{Infobox film
| name           = Band of the Hand
| image          = Band of the hand poster.jpg
| caption        = Theatrical release poster
| director       = Paul Michael Glaser Michael Mann
| writer         = Leo Garen Jack Baran
| starring = {{Plainlist|
* Stephen Lang
* James Remar
}}
| music          = Michel Rubini
| cinematography = Reynaldo Villalobos
| editing        = Jack Hofstra
| distributor    = TriStar Pictures
| released       =  
| runtime        = 109 minutes
| country        = United States
| awards         =
| language       = English
| budget         = $8.7 million
| gross          = $4,865,912 
}}
Band of the Hand is an American 1986 crime film directed by Paul Michael Glaser.  The film turned into a theatrical release after it failed as a television pilot. 
 title track is written by Bob Dylan, and while it appeared on the soundtrack album and as a single, it has never been released on one of his own albums.

==Plot==
The story involves a group of juvenile delinquents in their teens who are doomed to be prosecuted as adults for their crimes unless they take part in a new and experimental "program" led by a Vietnam veteran Native American (Lang) from the Everglades. The teens must learn to survive in the dangerous swamp and how to work together. Upon completion of the program, the group buys a vacant house in a dangerous part of Miami and slowly rebuilds the neighborhood, kicking out the pimps, prostitutes and drug dealers. The films climax has the group taking the fight directly to a drug manufacturing facility that is equipped with an M-134 Minigun.

==Cast== Native American. Having lost all the members of his fire team on a mission in Vietnam he was awarded the Medal of Honor and honorably discharged. He wants to help troubled teens, having served jail time himself but in a previous experimental program they all reverted to their old ways and were all killed or locked back up. He is highly trained in infiltration tactics, a skilled tracker, and formidable in hand to hand combat, showing the ability to disarm and incapacitate threats easily. Even though he appears to be cold to the teens at first, he grows to love them like a family even until his death.
*Michael Carmine - Ruben Julian Pacecho AKA "Mira Primo": Leader of the "Home Boys" of Miami and main rival of Moss. He was arrested for Aggravated Assault and Armed Robbery and sentenced to 3 years in prison. The more jovial of the five he tends to be more sarcastic and patronizing about situations but ends up getting on Moss nerves easily causing fights between the two in the beginning but they end up becoming friends.
*Leon Robinson - Moss Roosevelt AKA "Warlord": Leader of the "27th Avenue Players" of Miami and rival of Ruben. He was also arrested for assault and robbery during the same riot as Ruben and sentenced to 3 years as well. The most aggressive of the five he tends to be angered easily especially when teased about anything mostly his race. He tries to be the alpha male with J.L. and threatens Joe with a knife during a rainstorm but is put in his place both times. he becomes friends with Ruben after they realize they have a lot in common.
* , arson and destruction of public property, sentenced to ten years. Known as "Crazy" for his grunts, screams and howls when he is hampered by Selective mutism that make him seem harmless but he is extremely dangerous. He shows his intelligence after he overcomes it during an argument between Moss and Ruben, screaming at the two to help the group survive in the swamp, and talking his way out of being found out for buying items to make illegal explosives. He has a knack for being able to build almost anything like shelters, snares, pipe bombs and other explosives for the assault on Nestors compound.
*Danny Quinn - Carlos Rene Aragon AKA "The Lover": Arrested for Drug Trafficking and Weapons Possession he is sentenced to four years. He is the most calm and is very arrogant at times looking down upon the rest at first because of the money and power he once had as a major drug dealer. A very studious person, he quickly learns things when he listens but his arrogance often impedes him. He has a girlfriend (Nikki) with whom he is very attached to but this relationship causes him more trouble than good at times yet he stays by her side. He takes control after Joe dies and keeps the group together as leader. drifter before being arrested.  He is very sensitive about not being able to read but Joe and J.L. help him learn to. He stays with the group even though he has multiple opportunities to escape having found the family he never had with the "Band". He becomes the risk taker of the group showing little sign of self-preservation when trying to escape and when the group assaults Nestors compound running through crossfire to plant his explosives. getaway driver. She reluctantly has a relationship with Nestor thinking that he will free his one time prodigy from prison after he set him up but ends up being kept as an amusement. She escapes him and during the assault on the compound runs into a fleeing Nestor.  As he tries to disarm her thinking he still holds sway she allows him to get close enough she fires point blank with a shotgun and kills Nestor.
*Larry Fishburne - Cream - A local pimp and drug dealer in Little Havana. He crosses paths with the teens after a failed attempt to kick the group out of the neighborhood with threats that lead to his broken arm and an unintentional weapons supply for Joes group from his bodyguards. He is ordered by Nestor to kill them all and continue business, in the ensuing battle he and Joe kill one another  after Creams failed assault causing the kids to fully band together and take down Nestor.
*James Remar - Nestor Quintana - The drug lord of Miami and target of the five after Joes death. He is a sadist who takes Nikki as his prize after betraying Carlos to the authorities. He thinks nothing of the kids attempt to clean up the neighborhood and sees it as a disruption of his business. After Joes death he tells his main henchmen to find and kill Nikki but to ignore the kids, thinking they will scatter. After the kids dismantle his drug operation he confronts Nikki outside his compound as he tries to escape.  Trying to use the influence he thinks he holds over her, she kills him as he tries to disarm her.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 