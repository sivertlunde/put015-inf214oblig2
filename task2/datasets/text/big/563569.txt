Blondie (1938 film)
{{multiple issues|
 
 
}}

{{Infobox film
| name = Blondie
| image_size =
| image	=	Blondie FilmPoster.jpeg
| alt =
| caption =
| director = Frank R. Strayer
| producer = Robert Sparks
| writer = Richard Flournoy (screenplay)  Chic Young (characters)
| narrator =
| starring = (See article)
| music = Leigh Harline  Ben Oakland
| cinematography = Henry Freulich
| editing = Gene Havlick
| studio = King Features Syndicate
| distributor = Columbia Pictures
| released =  
| runtime = 70 min.
| country = United States
| language = English
| budget =
| gross =
}}
 comic strip of the same name. The screenplay was written by Chic Young and Richard Flournoy.

This was the first of 28 films based on the comic strip; Columbia Pictures produced them from 1938 to 1943, and popular demand brought them back in 1945. When the Blondie film series came to an end with Beware of Blondie in 1950, it was announced that it would be replaced with a series of Gasoline Alley movies. However, only two such films were made, Gasoline Alley (1951) and Corky of Gasoline Alley (1951). Columbia then reissued the Blondie features, beginning with the very first film in the series.
 Stanley Brown, John Tyrrell, Alyn Lockwood, Jimmy Lloyd, Gay Nelson and Ross Ford.

==Cast==
* Penny Singleton as Blondie Bumstead Arthur Lake as Dagwood Bumstead
* Larry Simms as Dagwood "Baby Dumpling" Bumstead, Jr.
* Marjorie Kent as Cookie Bumstead
* Gene Lockhart as Clarence Percival "C.P." Hazlip
* Jonathan Hale as J.C. Dithers
* Jerome Cowan  as Mr. Radcliffe
* Gordon Oliver as Chester Franey
* Danny Mummert as Alvin Fuddle
* Kathleen Lockhart as Mrs. Miller (Blondies mother)
* Ann Doran as Elsie Hazlip Dorothy Moore as Dorothy "Dot" Miller (Blondies sister)
* Fay Helm as Mrs. Fuddle

==Blondie film series==
 
*Blondie (1938)
*Blondie Meets the Boss (1939)
*Blondie Takes a Vacation (1939)
*Blondie Brings Up Baby (1939)
*Blondie on a Budget (1940)
*Blondie Has Servant Trouble (1940)
*Blondie Plays Cupid (1940)
*Blondie Goes Latin (1941)
*Blondie in Society (1941)
*Blondie Goes to College (1942)
*Blondies Blessed Event (1942)
*Blondie for Victory (1942)
*Its a Great Life (1943)
*Footlight Glamour (1943)
  
*Leave It to Blondie (1945)
*Life with Blondie (1945)
*Blondies Lucky Day (1946)
*Blondie Knows Best (1946)
*Blondies Big Moment (1947)
*Blondies Holiday (1947)
*Blondie in the Dough (1947)
*Blondies Anniversary (1947)
*Blondies Reward (1948)
*Blondies Secret (1948)
*Blondies Big Deal (1949)
*Blondie Hits the Jackpot (1949)
*Blondies Hero (1950)
*Beware of Blondie (1950)
  

==Further reading==
Blondie Goes to Hollywood, by Carol Lynn Scherling. Albany, 2010. BearManor Media. ISBN 978-1-59393-401-9.

==External links==
* 
*  
*  

 
 
 
 
 
 
 