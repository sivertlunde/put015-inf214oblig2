Cocaine (film)
{{Infobox film
| name           = Cocaine
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Graham Cutts
| producer       = Harry B. Parkinson Frank Miller
| narrator       =  Ward McAllister
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Astra Films
| released       = June 1922
| runtime        = 
| country        = United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Ward McAllister and Cyril Raymond. A melodrama – it depicts the distribution of cocaine by gangsters through a series of London nightclubs and the revenge sought by a man after the death of his daughter. 
 drug use, censors in June 1922 and released in some cinemas under the alternative title While London Sleeps. 

The Chinese gangster Min Fu was reportedly based on a real-life criminal Brilliant Chang. 

==Cast==
* Hilda Bayley – Jenny
* Flora Le Breton – Madge Webster Ward McAllister – Min Fu
* Cyril Raymond – Stanley  
* Tony Fraser – Loki  
* Teddy Arundell – Montagu Webster

==References==
 

==Bibliography==
* Robertson, James Crighton. The Hidden Cinema: British Film Censorship in Action, 1913–1975. Routledge, 1993.
* Sweet, Matthew. Shepperton Babylon: The Lost Worlds of British Cinema. Faber and Faber, 2005.

==External links==
* 

 

 
 
 
 
 


 