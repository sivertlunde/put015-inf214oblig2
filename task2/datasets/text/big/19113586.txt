Eye in the Labyrinth
{{Infobox film
| name           = Eye in the Labyrinth
| image          = Eye-in-the-labrynth-1972-italian-poster.jpg
| caption        = Film poster
| director       = Mario Caiano
| producer       = Hans Pflüger Lionello Santi
| writer         = Mario Caiano Horst Hächler Antonio Saguera
| starring       = Rosemary Dexter
| music          = 
| cinematography = Giovanni Ciarlo
| editing        = Renato Cinquini
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Eye in the Labyrinth ( ) is a 1972 Italian horror film directed by Mario Caiano and starring Rosemary Dexter.   

==Plot==
Julie (Rosemary Dexter) is disturbed by the disappearance of her psychiatrist boyfriend Luca (Horst Frank) following a bizarre dream where she witnessed him murdered.  She travels to a seaside village where he might be and encounters Frank (Adolfo Celi), who tells her Luca has indeed been there.  Julies investigation leads her the house of Gerta (Alida Valli), where the mystery deepens among the odd characters residing at this artists enclave.

==Cast==
* Rosemary Dexter - Julie
* Adolfo Celi - Frank
* Alida Valli - Gerda
* Horst Frank - Luca
* Sybil Danning - Toni
* Franco Ressel - Eugene
* Michael Maien - Louis
* Benjamin Lev - Saro
* Gigi Rizzi - Thomas
* Peter Kranz
* Gaetano Donati
* Elisa Mainardi - Orphanage Director
* Mario Cantatore
* Rosita Torosh - Lucas Secretary

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 