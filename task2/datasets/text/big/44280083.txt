Chaubees Ghante
{{Infobox film
| name = Chaubees Ghante
| image = 
| image size = 
| caption = 
| director = Dwarka Ghosla
| producer = 
| writer = 
| starring = Premnath   Shakeela   K. N. Singh   Maruti   Nishi   Shammi   Sheela Vaz   Kanchanmala
| music = Bipin Babul   Raja Mehdi Ali Khan (lyrics)
| studio   = Zar Productions
| editing = 
| released = 1958
| runtime = 
| country = India Hindi
}}

Chaubees Ghante aka 24 Ghante is a 1958 Hindi film directed by Dwarka Ghosla. It stars Premnath, Shakeela, Nishi, K. N. Singh, Maruti, Shammi, Sheela Vaz, Kanchanmala. It had music by Bipin Babul and lyrics written by Raja Mehdi Ali Khan. 

==Plot==

 

==Cast==
 
* Prem Nath Shakila
* K.N. Singh Maruti Rao Shammi
* Raj Kapoor Nishi
* Samson
 

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! #!!Title !! Singer(s) !! Duration
|-
| 1
| "Humein Haal-E-Dil Tumse Kehna Hain"
| Talat Mahmood, Asha Bhosle
|-
| 2
| "Duniya Mujhko Paagal Samjhe" Mukesh
|-
| 3
| "Aaj Ka Salaam Lo"
| Asha Bhosle
|-
| 4
| "Chhan Chhan Karti Daulat"
| Mohammad Rafi, Asha Bhosle
|-
| 5
| "Ek Dil Humare Paas Hain"
| Mohammed Rafi, Asha Bhosle
|-
| 6
| "Haye Kisika Rangin Aanchal"
| Asha Bhosle
|-
| 7
| "Humein Haal-E-Dil Tumse Kehna Hain"
| Talat Mahmood
|-
| 8
| "Jawani Jalake Bedardi Raah"
| Shamshad Begum, Asha Bhosle
|-
| 9
| "Nigahein Milake Huyi Main Deewani"
| Asha Bhosle
|-
|}

 

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 


 