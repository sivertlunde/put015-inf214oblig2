Blue Eyes (film)
{{Infobox film
| name           = Blue Eyes
| image          = Blue Eyes film poster.jpg
| caption        = 
| director       = José Joffily
| producer       = 
| writer         = 
| starring       = David Rasche  Irandhir Santos  Cristina Lago Erica Gimpel Frank Grillo
| music          = Jaques Morelenbaum	
| cinematography = 
| editing        = Pedro Bronz
| studio         = Coevos Filmes
| distributor    = Imagem Filmes (Brazil)   Schröder Media (Germany)
| released       =  
| runtime        = 111 minutes
| country        = Brazil
| language       = Portuguese  English  Spanish
| budget         = 
}}

Blue Eyes ( ) is a 2009 Brazilian drama film directed by José Joffily.

== Plot ==
Not satisfied with the retirement and on his last day at the job, a U.S. immigration officer holds arbitrarily Latin passengers inside a room of a New York airport. There he conducts interrogations that takes a surprising and tragic course. 

== Cast ==
*David Rasche as Marshall
*Irandhir Santos as Nonato
*Cristina Lago as Bia
*Frank Grillo as Bob Estevez
*Erica Gimpel as Sandra
*Hector Bordoni as Augustín
*Valeria Lorca as Assumpta
*Branca Messina as Calypso
*Everaldo Pontes as Bias grandfather
*Pablo Uranga as Martín

==References==
 

==External links==
*   
*  

 
 
 
 
 
 
 
 


 
 