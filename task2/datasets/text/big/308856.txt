The Phantom of the Opera (2004 film)
 
{{Infobox film
| name           = The Phantom of the Opera
| image          = Poto2.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Joel Schumacher
| producer       = Andrew Lloyd Webber
| screenplay     = {{Plainlist|
* Joel Schumacher
* Andrew Lloyd Webber}}
| based on       =  
| starring       = {{Plainlist|
* Gerard Butler
* Emmy Rossum Patrick Wilson
* Miranda Richardson
* Minnie Driver
* Simon Callow
* Ciaran Hinds}}
| music          = Andrew Lloyd Webber  John Mathieson
| editing        = Terry Rawlings
| studio         = {{Plainlist| Joel Schumacher Productions
* Odyssey Entertainment Really Useful Films
* Scion Films}} Warner Bros. Pictures
| released       =  
| runtime        = 143 minutes  
| country        = {{Plainlist|
* United Kingdom
}}
| language       = English
| budget         = $70 million   
| gross          = $154.6 million 
}} musical of Le Fantôme de lOpéra by Gaston Leroux.
 Patrick Wilson as Raoul, Miranda Richardson as Madame Giry, and Minnie Driver as Carlotta Giudicelli.

The film was announced as early as 1989, but production only started in 2002 due to Lloyd Webbers divorce and Schumachers busy career. It was entirely shot at Pinewood Studios, with sceneries also being depicted with the help of miniatures and computer graphics. Rossum, Wilson, and Driver had singing experience, but Butler had no experience and had to receive music lessons. The Phantom of the Opera grossed approximately $154 million worldwide, and received mixed reviews, praising the visuals and acting but criticising the writing and directing.

==Plot==
In 1919, the dilapidated Paris Opera House holds an auction. The elderly   in pieces which has been restored and newly wired with electricity. As the auctioneers display the restored chandelier, it illuminates and slowly rises to its old place in the rafters as the opening crescendo of music wipes away the years of decay from the opera house. The black and white turns into color, and the audience is transported back in time to 1870, when the opera house was still in its prime.

The opera house is purchased by two new owners, Richard Firmin (Ciarán Hinds) and Gilles André (Simon Callow), who are of the "scrap metal" industry and have no experience in theater. While the cast are rehearsing Hannibal, Madame Giry, who is the ballet mistress and the mother of Meg Giry (Jennifer Ellison), introduces them to Christine Daaé (Emmy Rossum), a ballet dancer and young but talented singer. The young viscount, Raoul, is introduced to the cast, and Christine recognizes him as her childhood love. He does not see her, however, and she says nothing to get his attention, assuming he would not recognize her. While performing an aria, a backdrop falls from the ceiling and almost crushes Carlotta Giudicelli (Minnie Driver), the soloist and lead soprano, who immediately resigns from the house. Meanwhile, a dark figure leaves the spot where the backdrop used to be and an envelope falls to the floor. Madame Giry opens it and reads the letter signed from the "Opera Ghost", a specter-like entity who lives somewhere within the opera house and is believed to be a ghost. He apparently watches every show and was paid twenty thousand francs a month by the previous owner of the house. Firmin and André scramble to replace Carlotta, and Christine is chosen after singing for them. That night she sings beautifully, and the Opera Ghost hears her through the vents. 

During Christines performance, Raoul recognizes her from his childhood. After the show, Christine goes to the chapel to light a candle for her father, who died when she was six years old. Meg asks Christine how she learned to sing so well. Christine explains that the Angel of Music comes to her and tutors her. She has never met him and thinks her father sent this "angel" to help her, but in fact it is the Opera Ghost, or Phantom of the Opera (Gerard Butler), who teaches her. Later, she is in her dressing room, where she reunites with Raoul. He plans to take her to supper, but she declines, saying that the Angel is very strict. Raoul ignores her and leaves to prepare for their date. The Phantom locks Christine in her room and sings to her about his displeasure that Raoul is trying to court her. Christine apologizes, asking him to come to her.  He reveals himself through her mirror and leads her away.

Christine goes with the Phantom to his lair underneath the opera house. He reveals to her that he loves her and wants her to love him back. He shows Christine a bust of herself, wearing a wedding dress and veil, causing her to faint, and the Phantom places her in a bed. The next morning she awakes to find the Phantom writing music. She approaches him and removes his mask out of curiosity. He bursts into a fit of rage, covering his face with his hand. He at first says she must stay forever because she saw his deformities, revealing that he "dreams of beauty". Pitying him Christine hands him back his mask and the two have a moment of understanding. He then decides to return her to the opera house. 

That morning, the two managers lament Christines disappearance, as well as series of notes they received from the Opera Ghost trying to blackmail them for his payment and ordering them on how to run the opera house. When Carlotta returns, she is furious to find a note sent to her saying if she sang as the countess in Il Muto that night instead of Christine, then disaster "beyond   imagination" would occur. Firmin and André ignore the ghosts warnings and give Carlotta the lead role. That night, the Phantom interrupts the performance and criticizes their failure to follow his orders.

Carlotta continues to sing, but her voice croaks and the lead role is given to Christine. While the ballet is being performed, the Phantom hangs Buquet (Kevin McNally), chief of the flies, and drops him from above, creating chaos. Christine flees to the roof with Raoul. She reveals to him that she has seen the Phantoms face and fears him, but also pities him because of his sadness. Raoul tells Christine he loves her and will protect her forevermore. Christine returns his love, kissing him passionately and they both leave the roof. The Phantom, who witnessed the scene, becomes heartbroken. He then hears them both singing together. Growing furious at Raoul, he vows revenge on them both.
 Red Death. The Phantom brings his own composition, Don Juan Triumphant, and orders the managers to stage the opera. Raoul exits the room and Christine approaches the Phantom. At the sight of the engagement ring, the Phantom rips it from Christine and disappears into a trap on the floor. Raoul tries to follow him but is stopped by Madame Giry, who privately tells him the story of the Phantoms past. When she was a little girl, she went to a freak circus where they featured a deformed child in a cage. The child was beaten while everyone watched and laughed. The ringmaster then removed a burlap sack covering the childs face, revealing his deformity. Only the young Madame Giry pitied him. She was the last to leave and saw the child strangle the ringmaster with a rope. Chased by the police, Madame Giry helped him escape and found shelter for him beneath the opera house, where she has hidden him from the world ever since. 

Christine takes a carriage to visit her fathers grave, but the Phantom secretly takes over the reins. Raoul follows when he realizes shes gone. Christine arrives and laments over her fathers death. The Phantom tries to win her back by pretending to be her fathers angel, but Raoul arrives and stops him. A sword fight ensues in the cemetery, where Raoul eventually disarms the Phantom and is about to kill him, but Christine pleads for him not to. His rage seemingly augmented, the Phantom watches angrily as Christine and Raoul ride away.

Christine admits she is afraid of the Phantom and tells Raoul he will never stop trying to recapture her. Raoul realizes that they can use the Phantoms opera to capture him, as he will surely attend. Don Juan Triumphant is performed, and the Phantom makes his entrance as the lead with Christine. Raoul can do nothing but watch from his box as Christine falls for the Phantom yet again. However, she once again removes his mask, revealing his deformities to the entire audience, who scream in fear. He escapes with her by dropping the chandelier and setting the opera house on fire. 

The Phantom brings Christine back down to his lair. Madame Giry shows Raoul where the Phantom lives, and he goes to rescue Christine. The Phantom forces Christine to don the wedding dress and once again professes his love, and orders Christine to marry him. Christine tries to convince the Phantom that she does not fear his ugliness, but rather his anger and willingness to kill to get what he wants. Just then, Raoul enters the lair, and the Phantom ties him to a gate and threatens to kill him if Christine refuses to marry him. Christine reflects over the impossible choice before passionately kissing the Phantom to show him he is not alone in the world. The Phantom is shocked from experiencing real human love for the first time in his life. Ashamed of his murderous actions, he allows Christine and Raoul to leave and orders them to never return. He finds comfort in a little monkey music box. Christine approaches the Phantom, who tells her that he loves her, and she silently gives him the diamond ring from her finger to remember her by. After Christine and Raoul leave, the Phantom smashes every mirror in his underground lair and disappears through a secret passage behind a velvet curtain just before the police arrive. Upon entering, Meg finds only the Phantoms white mask.

Back in the present, the elderly Raoul goes to visit Christines tomb, which reveals that she died only two years before, in 1917, at age 63. Her tombstone says "Countess of Chagny" and "beloved wife and mother", revealing she married Raoul and had children. He lays the monkey music box at her grave site and notices a red rose with a black ribbon tied around it (a trademark of the Phantom) with the engagement ring attached to it, implying that the Phantom is still alive, and will always love Christine.

==Cast== The Phantom
* Emmy Rossum as Christine Daaé  Patrick Wilson as Viscount Raoul de Chagny
* Miranda Richardson as Madame Giry
* Minnie Driver as Carlotta Giudicelli
** Margaret Preece as Carlottas singing voice
* Simon Callow as Gilles André
* Ciarán Hinds as Richard Firmin
* Victor McGuire as Ubaldo Piangi
* Jennifer Ellison as Meg Giry
* Murray Melvin as Monsieur Reyer
* Kevin McNally as Joseph Buquet
* James Fleet as Monsieur Lefèvre

==Production==
===Development===
Warner Bros. purchased the film rights to The Phantom of the Opera in early 1989, granting Andrew Lloyd Webber total artistic control.  Despite interest from A-list directors, Lloyd Webber and Warner Bros. instantly hired Joel Schumacher to direct; Lloyd Webber had been impressed with Schumachers use of music in The Lost Boys.  The duo wrote the screenplay that same year,  while Michael Crawford and Sarah Brightman were cast to reprise their roles from the original stage production. Filming was set to begin at Pinewood Studios in England in July 1990, under a $25 million budget. 

However, the start date was pushed to November 1990 at both   Celebration. 

Schumacher and Lloyd Webber restarted development for The Phantom of the Opera in December 2002.  It was then announced in January 2003 that Lloyd Webbers Really Useful Group had purchased the film rights from Warner Bros. in an attempt to produce The Phantom of the Opera independently.    As a result, Lloyd Webber invested $6 million of his own money.  The Phantom of the Opera was produced on a $55 million budget. A further $15 million was used for marketing, bringing the final budget to $70 million.  Warner Bros. was given a first look deal for distribution; the studio did not sign on until June 2003, when the principal cast was chosen.   

===Casting=== Van Helsing. "They rang to ask about my availability", Jackman explained in an April 2003 interview, "probably about 20 other actors as well. I wasnt available, unfortunately. So, that was a bummer."    "We needed somebody who has a bit of rock and roll sensibility in him", Andrew Lloyd Webber explained. "Hes got to be a bit rough, a bit dangerous; not a conventional singer. Christine is attracted to the Phantom because hes the right side of danger." DVD production notes   Director Joel Schumacher had been impressed with Gerard Butlers performance in Dracula 2000.  Prior to his audition, Butler had no professional singing experience and had only taken four voice lessons before singing "The Music of the Night" for Lloyd Webber.   
 camp performance Veronica Guerin. The Making of The Phantom of the Opera,  , Warner Home Video  Ramin Karimloo also briefly appears as the portrait of Gustave Daaé, Christines father. Karimloo later played the Phantom as well as the role of Raoul on Londons West End.

===Filming===
Principal photography lasted from September 15, 2003 to January 15, 2004. The film was shot entirely using eight sound stages at Pinewood Studios,  where, on the Pinewood backlot, the bottom half exterior of the Palais Garnier was constructed. The top half was implemented using a combination of computer-generated imagery (CGI) and a scale model created by Cinesite. The surrounding Paris skyline for "All I Ask of You" was entirely composed of matte paintings.  Cinesite also created a miniature falling chandelier, since a life-size model was too big for the actual set. 
 Charles Garnier, Beauty and the Beast (1946), where a hallway is lined with arms holding candelabra. The cemetery was based on the Père Lachaise Cemetery|Père Lachaise and Montparnasse Cemetery|Montparnasse.  Costume designer Alexandra Byrne utilised a limited black, white, gold and silver colour palette for the Masquerade ball. 

==Reception==
===Release and awards=== expanding to 907 screens on January 14, 2005  the film obtained the 9th spot at the box office,  which it retained during its 1,511 screens wide release on January 21, 2005.   The total domestic gross was $51,225,796. With a further $107 million earned internationally, The Phantom of the Opera reached a worldwide total of $158,225,796.  A few foreign markets were particularly successful,  such as Japan, where the films Japanese yen|¥4.20 billion ($35 million) gross stood as the 6th most successful foreign film and 9th overall of the year.   The United Kingdom  and South Korea both had over $10 million in receipts, with $17.5 million and $11.9 million, respectively.    
 The Aviator. Charles Hart The Motorcycle Golden Globe same ceremony, Best Actress Best Performance Best Action/Adventure/Thriller Costume Design. 

The soundtrack of the film was released in two separate CD formats on November 23, 2004, as a two-disc deluxe edition which includes dialogue from the film and a single-disc highlights edition.

The film had its initial North America video release on DVD and VHS on May 3, 2005, following its first digital release on HD-DVD on April 18, 2006 and a Blu-ray edition on October 31, 2006.

===Critical reception===
The film received generally mixed reviews from film critics. Even though 86% of the general audience liked the film, based on 384,463 audience reviews collected by Rotten Tomatoes, only 33% of the critics enjoyed The Phantom of the Opera, with an average score of 5/10. "The music of the night has hit something of a sour note: Critics are calling the screen adaptation of Andrew Lloyd Webbers popular musical histrionic, boring and lacking in both romance and danger", the consensus read. "Still, some have praised the film for its sheer spectacle".  By comparison, Metacritic calculated an average score of 40/100 from its 39 reviews collected. 

{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 90%; background:#0; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "The film looks and sounds fabulous and I think its an extraordinarily fine document of the stage show. While it doesnt deviate much from the stage material, the film has given it an even deeper emotional centre. Its not based on the theatre visually or direction-wise, but its still got exactly the same essence. And thats all I could have ever hoped for."
|-
| style="text-align: left;" | — Andrew Lloyd Webber 
|}

Despite having been impressed with the cast, Jonathan Rosenbaum of the Chicago Reader wrote that "Teen romance and operetta-style singing replace the horror elements familiar to film-goers, and director Joel Schumacher obscures any remnants of classy stage spectacle with the same disco overkill he brought to Batman Forever."  Stephanie Zacharek of Salon.com believed that Phantom of the Opera "takes everything thats wrong with Broadway and puts it on the big screen in a gaudy splat." 
 Puccinian songs are reprised and reprised and reprised until youre guaranteed to go out humming."  Owen Gleiberman of Entertainment Weekly believed Schumacher did not add enough dimension in adapting The Phantom of the Opera. "Schumacher, the man who added nipples to Batmans suit, has staged Phantom chastely, as if his job were to adhere the audience to every note". 

Roger Ebert reasoned that "part of the pleasure of movie-going is pure spectacle—of just sitting there and looking at great stuff and knowing it looks terrific. There wasnt much Schumacher could have done with the story or the music he was handed, but in the areas over which he held sway, he has triumphed."  In contrasting between the popularity of the Broadway musical, Michael Dequina of Film Threat magazine explained that "it conjures up this unexplainable spell that leaves audiences sad, sentimental, swooning, smiling—in some way transported and moved. Now, in Schumachers film, that spell lives on." 

==See also==
* The Phantom of the Opera (2004 soundtrack)
* The Phantom of the Opera

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 