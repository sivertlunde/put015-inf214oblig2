Offside (2006 Iranian film)
{{Infobox Film
| name           = Offside
| image          = offside poster.jpg
| caption        = 
| director       = Jafar Panahi
| producer       = 
| writer         = Jafar Panahi Shadmehr Rastin
| starring       = Shima Mobarak-Shahi Safar Samandar Shayesteh Irani Ayda Sadeqi Golnaz Farmani
| music          = Yuval Barazani Korosh Bozorgpour	
| cinematography = Rami Agami Mahmoud Kalari
| editing        = Jafar Panahi
| distributor    = Sony Pictures Classics
| released       = 17 February 2006
| runtime        = 93 minutes
| country        = Iran
| language       = Persian
| budget         = $2,500 (estimated)
| gross          = 
}}
 World Cup qualifying match but are forbidden by law because of their sex. Female fans are not allowed to enter football stadiums in Iran on the grounds that there will be a high risk of violence or verbal abuse against them. The film was inspired by the directors daughter, who decided to attend a game anyway. The film was shot in Iran  but its screening was banned there. 

==Plot==

Most of the characters in the film are not named.
 2006 World qualifying match Iran and Bahrain national football team|Bahrain. She travels by bus with a group of male fans, some of whom notice her gender, but do not tell anyone. At the stadium, she persuades a reluctant ticket tout to sell her a ticket; he only agrees to do so at an inflated price. The girl tries to slip through security, but she is spotted and arrested. She is put in a holding pen on the stadium roof with several other women who have also been caught; the pen is frustratingly close to a window onto the match, but the women are at the wrong angle to see it.

The women are guarded by several soldiers, all of whom are just doing their national service; one in particular is a country boy from Tabriz who just wants to return to his farm. The soldiers are bored and do not particularly care whether women should be allowed to attend football matches; however, they guard the women carefully for fear of their "chief", who could come by at any moment. They occasionally give commentary on the match to the women.

One of the younger girls needs to go to the toilet, but of course there is no womens toilet in the stadium. A soldier is deputed to escort her to the mens toilet, which he does by an increasingly farcical process: first disguising her face with a poster of a football star, then throwing a number of angry men out of the toilet and blockading any more from entering. During the chaos, the girl escapes into the stadium, although she returns to the holding pen shortly after as she is worried about the soldier from Tabriz getting into trouble.

Part of the way through the second half of the game, the women are bundled into a bus, along with a boy arrested for carrying fireworks, and the soldiers ordered to drive them to the Vice Squad headquarters. As the bus travels through Tehran, the soldier from Tabriz plays the radio commentary on the match as it concludes. Iran defeats Bahrain 1-0 with a goal from Nosrati just after half time and wild celebrations erupt within the bus as the women and the soldiers cheer and sing with joy. The girl whose story began the film is the only one not happy. When asked why, she explains that she is not really interested in football; she wanted to attend the match because a friend of hers was one of seven people killed in a scuffle during the recent Iran-Japan match, and she wanted to see the match in his memory.

The city of Tehran explodes with festivity, and the bus becomes caught in a traffic jam as a spontaneous street party begins. Borrowing seven sparklers from the boy with the fireworks, the women and the soldiers leave the bus and join the party, holding the sparklers above them.

The film was filmed at an actual stadium, at a real life qualifying match for the Iranian National team. And Panahi had two separate outcomes to the film depending on the turnout of the match. 

==Cast==
* Sima Mobarak-Shahi as first girl
* Shayesteh Irani as smoking girl
* Ayda Sadeqi as soccer girl
* Golnaz Farmani as girl with chador
* Mahnaz Zabihi	as female soldier
* Nazanin Sediq-zadeh as young girl
* Hadi Saeedi  as a soldier

==Critical reception==
The film received very positive reviews from critics. The review aggregator Rotten Tomatoes reported that 97% of critics gave the film positive reviews, based on 76 reviews.  Metacritic reported the film had an average score of 85 out of 100, based on 25 reviews. 

===Top ten lists===
The film appeared on several critics top ten lists of the best films of 2007. 

*First - Ed Gonzalez, Slant Magazine
*2nd - Noel Murray, The A.V. Club
*6th - J. Hoberman, The Village Voice
*9th - Peter Rainer, The Christian Science Monitor
*9th - Tasha Robinson, The A.V. Club

==Awards== New York and Toronto Film Festivals.

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
*The Azadi Stadium is at coordinates  . Aerial images of the area also show the circular stairs shown in the movie, as the soldier takes the girl away.
*  

 

 
 
 
 
 
 
 
 
 
 
 
 