Night Rehearsal
 
{{Infobox film
| name           = Night Rehearsal
| image          = 
| caption        = 
| director       = Miklós Szurdi
| producer       = 
| writer         = István Verebes Miklós Szurdi
| starring       = Sándor Szakácsi
| cinematography = András Szalai
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Night Rehearsal ( ) is a 1983 Hungarian comedy film directed by Miklós Szurdi. It was entered into the 13th Moscow International Film Festival.   

==Cast==
* Sándor Szakácsi as Horkai Ádám
* Dorottya Udvaros as Andrea
* György Linka as Gál Szabó Endre
* Zsuzsa Töreky as Ica
* Enikö Eszenyi as Kati
* Tamás Végvári as Theatre Director
* Emese Balogh as Baba
* Erika Bodnár as Erika
* Kati Egri as Kriszta
* Márta Egri as Vajda Ági
* Athina Papadimitriu as Edit

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 