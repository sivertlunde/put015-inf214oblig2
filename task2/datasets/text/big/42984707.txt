Camp 14: Total Control Zone
Camp 14: Total Control Zone is a 2012 German/South Korean documentary film directed by German filmmaker Marc Wiese. It features interviews with Shin Dong-hyuk who was born in the Kaechon internment camp (known as "Camp 14") in North Korea. Through interviews and animated sequences, the film details human rights abuses inflicted on him and witnessed by him as prisoner there, including the public execution of his mother and brother. Also interviewed in the film are a former commander of the guards at Camp 22 and an ex-secret policeman, both of whom admit to committing various crimes including torture, rape and murder.  

==External links==
* 
* 

 
 
 
 

 