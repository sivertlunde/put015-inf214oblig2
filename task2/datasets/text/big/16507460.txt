Sargam (1992 film)
{{Infobox film
| name = Sargam
| image = Sargam (1992 film).jpg Hariharan |
| writer = Hariharan   Chowallur Krishnankutty   (dialogues )  | Rambha Nedumudi Venu Manoj K. Jayan
| music = Bombay Ravi Yusuf Ali Kechery   (lyrics)  
| cinematography = Shaji N. Karun
| editing = M. S. Mani
| studio = Gayathri Enterprises
| distributor = Manorajyam Release
| released = 1992
| runtime =
| country = India
| language = Malayalam
| budget =
}}
 Hariharan  released in 1992. Dialogues was written by Chowallur Krishnankutty. The movie was produced by Bhavani Hariharan, (wife of the director Hariharan (director)|Hariharan), under the banner of Gayathri Enterprises. The cast of the movie included Vineeth, Manoj K. Jayan, Rambha (actress)|Rambha, Nedumudi Venu, Urmila Unni, Thilakan, and Oduvil Unnikrishnan. 

Manoj K. Jayans role as Kuttan Thampuran won the hearts of audiences and critics. Noted director Shaji N. Karun did the cinematography for the movie. Music director Bombay Ravi created the music for the film.

The film won the National Film Award for Best Popular Film Providing Wholesome Entertainment. The film was remade in Telugu as Sarigamalu with Vineeth, Manoj K. Jayan, Rambha reprising their roles.

==Plot==
Kuttan Thampuran (Manoj K Jayan) is the son of Subhadra Thampurati and Kochanyian Thampuran of the Maangaattu kovilkkam. Kuttan has suffered from epilepsy from childhood and is rough and violent in his character and is feared by all in the village. Haridas (Vineeth) is his classmate despite being two years younger and develops a special bond with Thampuran from childhood. Haridas always accompanies Kuttan and carries iron keys to assist Kuttan in case the seizure develops. Both Hari and Kuttan grow up to be unsuccessful in their respective lives and Hari is criticized for this by his father Bagavathar who is a well known classical singer, but has not been financially successful. Hari had a liking for music, but Bagavathar discourages him from music and persuades him into a professional degree course. Kuttan meanwhile had frequent seizures from Epilepsy and is a nuisance in both home and village, despite seeking various medical treatments.

Unknown to Bhagavathar, Haridas has natural talent as a singer and has abundant raw talent which he displays at the local temple. Thankamni (Rambha) who is dependent on Illam is a student of Bhagavathar and falls for Haridas after she hears him singing. Haridas though initially reluctant towards Thankamani soon develops a passionate relationship with her based on the mutual interest in music. Kuttan discloses to Haridas that he is the only person who loves him and they reaffirm their brotherly love for each other. Meanwhile Thekkemadom Nampoothiri advises that the only treatment for Kuttan is to get married. Valiya Thampuran and Subhadra Thampurati plan to get Thankamani as a wife for Kuttan. Kuttan agrees to this,  unbeknownst about the affair between Haridas and Thankamani, and Subhadra Thampurati persuades Haridas to forget Thankamani and leave the village for Kuttans sake. Kuttan is devastated after coming to know about everything after their marriage and commits suicide.

Years later aging Subhadra Thampurati calls for Haridas, who has grown up to be a well known singer in India, to pay a visit to her. Haridas visit to the village, and subsequent happenings, form the rest of the story. Thankamani is paralysed and unable to speak probably from the shock of her forced marriage and then the subsequent suicide of her husband Kuttan.

When Haridas sings "Raga Sudha Rasa.." for Subhadra Thampuratti, then hearing his voice, Thankamani attempts to sing along.
Soon she displays signs of getting cured and now Subhadra Thampuratti is relieved that Thankamani can be reunited with haridas.

==Cast==
* Vineeth ... Haridas
* Manoj K. Jayan ... Kuttan Thampuran  Rambha ... Thankamani (as Amrutha) 
* Soumini ... Nandini
* Urmila Unni ... Subhadra Thampurati
* Nedumudi Venu ... Haridas father
* Oduvil Unnikrishnan ... Valiya Thampuran
* V. K. Sriraman ... Kochanyian Thampuran
* Jagannatha Varma ... Warriar Master Renuka ... Kunjulakshmi
* Thilakan ... Thekkemadom Nampoothiri Santhakumari ... Thankamanis mother
* Ravi Vallathol ... Kunjulakshmis husband
* Harikumar ... Sheshadri
* Anila Sreekumar ... Maid
* Mariza ... Mariza

==Soundtrack==
The acclaimed soundtrack of this movie was composed by Bombay Ravi for which the lyrics were penned by Yusuf Ali Kechery and also selections from traditionals. All the songs of this movie were instant hits.
{| class="wikitable"
|-
! Track !! Song Title !! Singer(s) !! Lyricist !! Raga
|- Chorus || Shuddha Dhanyasi
|- Kalyani
|- Kedara Gowla
|- Todi
|-
| 5 || "Krishna Kripa Sagaram" || K. J. Yesudas, K. S. Chithra || Yusuf Ali Kechery || Charukesi
|-
| 6 || "Kannadi Adyamayen" || K. J. Yesudas || Yusuf Ali Kechery || Kalyani
|-
| 7 || "Minnum Ponnin Kireedam" || K. S. Chithra || Traditional (C. V. Vasudeva Bhattathiri) || Chakravakam
|-
| 8 || "Raaga Sudharasa" || K. J. Yesudas, K. S. Chithra || Traditional (Tyagaraja) || Andholika
|-
| 9 || "Yadhukulothama" || K. J. Yesudas, Chorus || Traditional (Purandara Dasa) || Malahari
|-
| 10 || "Sree Saraswathi" || K. S. Chithra || Traditional (Muthuswami Dikshitar) || Arabhi
|-
| 11 || "Sangeethame" || K. J. Yesudas || Yusuf Ali Kechery || Natabhairavi
|}

==Awards== National Film Awards Best Popular Film Providing Wholesome Entertainment 

;Kerala State Film Awards Best Director Hariharan  Second Best Actor - Manoj K. Jayan  Best Music Director - Bombay Ravi

==References==
 

==External links==
 

 

 
 
 
 
 
 
 
 
 