John and Michael
{{Infobox film
| name = John and Michael
| image =
| caption =
| director = Shira Avni
| producer = Shira Avni Michael Fukushima
| writer =
| narrator = Brian Davis
| music =
| cinematography =
| editing =
| studio = National Film Board of Canada
| distributor =
| released =  
| runtime = 10 minutes 28 seconds
| country = Canada
| language = English
| budget =
| gross =
}}
John and Michael is a 2004 animated short by Shira Avni about two men with Downs syndrome who share a loving relationship.

The film was animated with clay backlit on plexiglas, to produce a stained glass effect.    Avni made over 14,000 paintings to create the film. It is narrated by Brian Davis, who is himself intellectually challenged.    John and Michael was co-produced by Avni and Michael Fukushima of the National Film Board of Canada. 

==Awards==
John and Michael received a dozen awards and honours at film festivals, including a Golden Sheaf Award, the award for best original screenwriting at the Inside Out Film and Video Festival, the short documentary award at DOXA Documentary Film Festival, the short film award at the Pink Apple film festival, a Silver Remi Award at the WorldFest-Houston International Film Festival as well as the award for best animated short at the Cinequest Film Festival.   
* an Honorable Mention at the Lesbian & Gay Film Festival
:October 13 to 22 2006, Seattle - USA
* the Jury Prize - Category: Shorts at the OutFlix Film Festival - International GLBT Film Festival
:August 4 to 10 2006, Memphis - USA
* the Golden Sheaf Award - Category: Animation at the Golden Sheaf Awards /Short Film and Video Festival
:May 25 to 28 2006, Yorkton - Canada
* the Doxa Short Documentary Award at the Doxa - Documentary Film and Video Festival
:May 23 to 28 2006, Vancouver - Canada
* the JWR Award for Best Original Screenwriting at the Inside Out Toronto Lesbian and Gay Film and Video Festival
:May 18 to 28 2006, Toronto - Canada
* the Short Film Award at the Pink Apple Film Festival
:May 4 to 21 2006, Zurich - Switzerland
* the Silver Remi Award at the WorldFest - International Film Festival
:April 21 to May 1, 2006, Houston - USA
* the Award for Best Animated Short at Cinequest
:March 1 to 12 2006, San Jose - USA
* the Award - Category: Animation 10 – 30 minutes on a theme of disability Picture This Film Festival
:February 6 to 10 2006, Calgary - Canada
* the Special Distinction Award by Jury Classe L CAV Rencontres Internationales du Cinéma dAnimation
:November 14 to 22 2005, Wissembourg - France
* an Honorable Mention - Catégorie: Arts at the International Film and Video Festival
:November 9 to 13 2005, Columbus - USA
* Best Animated Short Award at the Q Cinema: Fort Worths Gay & Lesbian International Film Festival
:May 19 to 22 2005, Fort Worth - USA

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 