Shaolin and Wu Tang
 
 
{{Infobox film
  | name = Shaolin and Wu Tang
  | image = Shaolin and Wu Tang.jpg
  | caption = Region 1 DVD cover
| film name = {{Film name|traditional=少林與武當
 |simplified=少林与武当 pinyin = Shào Lín yǔ Wǔ Dāng jyutping = Siu 3  Lam 4  jyu 5  Mou 5  Dong 1 }}
  | director = Gordon Liu
  | producer = 
  | writer = 
  | starring = Gordon Liu Adam Cheng Idy Chan Shen Chan Hoi-Shan Kwan Hoi San Lee Elvis Tsui Wang Lung Wei
  | music = 
  | cinematography = 
  | editing =  
  | distributor =  1983
  | runtime = 87 min. Cantonese Mandarin Mandarin
  | budget = 
  }}
 1983 film martial artist Shaolin and Wu Tang. It is also called Shaolin Vs. Wu-Tang in the Master Killer Collection.

The film inspired the name of the hip-hop group Wu-Tang Clan, who used several audio samples from the English dub of the film in their 1993 debut album Enter the Wu-Tang (36 Chambers).

==Plot summary==

Master Liu and Master Law are rival masters of Shaolin style kung fu and Wu-Tang style sword fighting, running schools in the same city.  Their top students, Chao Fung-wu (Adam Cheng) and Hung Jun-kit (Gordon Liu), are actually close friends, with Jun-kits sister, Yan-ling, having a crush on Fung-wu.  After observing the two students fighting at a brothel, two of the local Qing Lords (Wang Lung Wei) soldiers report the power of the styles to him.  The Lord determines that the two styles are dangerous and that he must learn both.

After poisoning Master Law and having Yan-ling killed, the Qing Lord is able to learn both Wu-Tang Sword style and Shaolin Chin kang fist from a spy.  Fung-wu and Jun-kit both return to their respective temples for training as a priest and a monk, respectively.  Jun-kit hopes to avenge his sisters death, and Fung-wu wants to avenge his masters death.

The Qing Lord has since learned both the styles; but, because he did not learn either from a master, his grasp on both styles is imperfect. To overcome this deficiency, he decides to have the Wu-Tang and the Shaolin destroy each other, so he may be the only master of both styles. To do this, he stages a martial arts contest between the two temples, hoping to appeal to the traditional rivalry between the Shaolin and the Wu-Tang.  Jun-kit (now called Tat-chi), and Fung-wu (now called Ming-kai), are selected by their respective temples as the representatives.

The Qing Lord, in his impatience to see both Wu-Tang and Shaolin destroyed, admits his true motives, and his role in Yan-ling and Master Laws deaths. Tat-chi and Ming-kai must then combine Shaolin Chin kang fist and Wu-Tang Sword style to defeat him.

==External links==
* 

 
 
 
 
 
 


 
 