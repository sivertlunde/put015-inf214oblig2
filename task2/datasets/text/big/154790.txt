Quo Vadis (1951 film)
{{Infobox film
| name  = Quo Vadis
| image   = Poster - Quo Vadis (1951) 01.jpg
| image_size = 225px
| caption  = theatrical release poster
| director  = Mervyn LeRoy
| producer  = Sam Zimbalist
| based on  =  
| screenplay = S. N. Behrman Sonya Levien John Lee Mahin Robert Taylor Deborah Kerr Leo Genn Peter Ustinov
| narrator  = Walter Pidgeon
| music   = Miklós Rózsa Robert Surtees William V. Skall
| editing  = Ralph E. Winters
| distributor  = Metro-Goldwyn-Mayer
| released  =  
| runtime  = 171 minutes
| country  = United States
| language  = English
| budget   = $7,623,000 Sheldon Hall, Epics, Spectacles, and Blockbusters: A Hollywood History Wayne State University Press, 2010 p 137   . 
| gross   = $21,037,000 
}}
 MGM in Quo Vadis Quo Vadis Robert Surtees and William V. Skall. The title refers to an incident in the apocryphal Acts of Peter; see Quo vadis?.
 Robert Taylor, Carlo Pedersoli were both cast in the movie as uncredited extras, and Sergio Leone worked on it as an assistant director of the Italian company. 

==Plot==
The action takes place in ancient Rome from AD 64–68, a period after Emperor Claudius illustrious and powerful reign, during which the new corrupt and destructive Emperor Nero ascends to power and eventually threatens to destroy Romes previous peaceful order. The main subject is the conflict between Christianity and the corruption of the Roman Empire, especially in the last period of the Julio-Claudian line. The characters and events depicted are a mixture of actual historical figures and situations and fictionalized ones.
 Robert Taylor), legate of XIV Gemina, returning from the wars, who falls in love with a devout Christian, Lygia (Deborah Kerr), and slowly becomes intrigued by her religion. Their love story is told against the broader historical background of early Christianity and its persecution by Nero (Peter Ustinov). Though she grew up Roman as the adopted daughter of a retired general, Aulus Plautius (Felix Aylmer), Lygia is technically a hostage of Rome. Marcus persuades Nero to give her to him for services rendered. Lygia resents this, but still falls in love with Marcus.
 burns Rome crucified upside-down as an ironic twist at the whim of Neros guard.
 Nicholas Hannen), Phaon (D. A. Clarke-Smith), Lucan (Alfredo Varelli), and Terpnos (Geoffrey Dunn) vouch for the mobs demands by putting their thumbs up as well. Marcus then breaks free of his bonds, leaps into the arena, frees Lygia with the help of his loyal troops from his legion, and announces that General Galba is at that moment marching on Rome, intent on replacing Nero.
 Acte (Rosalie Crutchley), a palace slave who was once in unrequited love with Nero, appears and offers to aid him in ending his own life before the mob storms the palace. The cowardly Nero cannot bring himself to do it, so Acte drives the dagger into his chest, weeping over his demise.

Marcus, Lygia and Ursus are now free and leave Rome. By the roadside, Peters Crozier|crook, which he had left behind when he returned to Rome, has miraculously sprouted flowers. The radiant light intones, "I am the way, the truth, and the life."

==Cast==
 
 Robert Taylor as Marcus Vinicius 
* Deborah Kerr as Lygia 
* Leo Genn as Petronius 
* Peter Ustinov as Nero
* Patricia Laffan as Poppaea 
* Finlay Currie as  Peter 
* Abraham Sofaer as Paul 
* Marina Berti as Eunice 
* Buddy Baer as Ursus 
* Felix Aylmer as Plautius 
* Nora Swinburne as Pomponia 
* Ralph Truman as  Tigellinus 
* Norman Wooland as Nerva Peter Miles as  Nazarius 
* Geoffrey Dunn as  Terpnos  Nicholas Hannen as  Seneca 
* D.A. Clarke - Smith as  Phaon
* Rosalie Crutchley as  Acte 
* John Ruddock as  Chilo 
* Arthur Walge as  Croton 
* Elspeth March as  Miriam 
* Strelsa Brown as  Rufia 
* Alfredo Varelli as  Lucan  Roberto Ottaviano as  Flavius
* William Tubbs as  Anaxander
* Pietro Tordi as  Galba
* Clelia Matania as  Parmenida
 

==Music==
The musical score by Miklós Rózsa is notable for its attention to historical authenticity. Rozsa incorporated a number of fragments of ancient Greek melodies into his own choral-orchestral score. New recordings were made by Rózsa with the Royal Philharmonic Orchestra (1977) and by Nic Raine, conducting the City of Prague Philharmonic (2012).
Composer Bernard Hermann called it "The score of a lifetime."

==Production notes==
  from the trailer for the film Quo Vadis]]

* The film was originally cast in 1949 with Elizabeth Taylor as Lygia and Gregory Peck as Marcus Vinicius. When the production changed hands the following year, the roles went to Deborah Kerr and Robert Taylor. Elizabeth Taylor was also a Christian prisoner in arena, but uncredited.
* Sophia Loren briefly appears uncredited as a slave. The Italian actor Bud Spencer also had an uncredited extra role as a Praetorian Guardsman.
* The film holds a record for the most costumes used in one movie; 32,000.
* The film was shot on location in Rome and in the Cinecittà|Cinecittà Studios.
* Peter Ustinov relates in his autobiography, Dear Me, that director Mervyn LeRoy summarized the manner in which he envisioned Ustinov should play the Emperor Nero, very salaciously, as "Nero...He plays with himself, nights." Ustinov, getting the directors gist, thereafter notes that this depraved manner was the basis of his creation of the character of Nero for the film.
* At one point in the film Nero shows his court a scale model illustrating his plans for rebuilding Rome. This model was originally constructed by Mussolinis government for a 1937 exhibition of Roman architecture—the films producers borrowed it from the postwar Italian government.  
*The first usage of the phrase Hollywood on the Tiber, which has since come to refer to a golden era of American runaway film production in Italy was used as the title of an article in the June 26, 1950 issue of Time Magazine|Time. 

==Reception==

===Box office performance=== highest grossing film of 1951, and resulting in a profit to MGM of $5,440,000. 

===Awards=== Best Art Best Cinematography, Best Costume Best Film Best Music, Best Picture. However, the movie did not win a single Academy Award.   
 Best Supporting Actor. The Golden Globe for Best Cinematography was won by Robert Surtees and William V. Skall. The film was also nominated for Best Motion Picture – Drama.

==Home media==
* A 2-Disc Special Edition of the movie was released on DVD in the US on November 11, 2008 after a long photochemical restoration process.  A high definition Blu-ray version was released March 17, 2009. 

==Trivia== director George Pals 1961 MGM production Atlantis, the Lost Continent. Reportedly, at a preview screening, when patrons were asked to answer the question "What part of the film did you like best?" one responded by writing, "The part where Robert Taylor rescues Deborah Kerr." (Neither actor was in the new film.) 
==See also==
* List of epic films
* List of films set in ancient Rome
* List of historical drama films

==References==
 

==External links==
 
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 