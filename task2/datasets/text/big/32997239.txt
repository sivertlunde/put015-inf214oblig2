A Rough Passage
 
{{Infobox film
| name           = A Rough Passage
| image          = 
| image_size     =
| caption        = 
| director       = Franklyn Barrett
| producer       = Franklyn Barrett Arthur Wright
| narrator       =
| starring       = Stella Southern Hayford Hobbs
| music          =
| cinematography = Franklyn Barrett
| editing        = 
| studio         = Barrets Australian Productions
| distributor    = Franklyn Barrett
| released       = 22 July 1922 
| runtime        = 6,000 feet
| country        = Australia
| language       = Silent film English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Arthur Wright. It was Barretts final feature and is considered a lost film. 

==Plot==
Laurie Larand (Hayford Hobbs) returns from the war and finds himself jilted and broke. He goes to work for a horse trainer who he discovers to be in league with a book maker to fleece the horse owners.

He also comes across a Shakesperean actor, Poverty Point (Arthur Albert), who becomes his friend, and the beautiful Doiya (Stella Southern), who he falls in love with.

In the finale, Larland exposes the villains and is united with Doris. 

==Cast==
*Hayford Hobbs as Laurie Larand
*Stella Southern as Doiya Reylen
*Elsa Granger as Belle Delair Gilbert Emery as Jiggy Javitts
*Arthur Albert as Poverty Point
*Alma Rock Phillips
*Robert McKinnon
*David Edelsten
*Sybil Shirley
*Billy Ryan

==Original Novel==
{{Infobox book|  
| name          = A Rough Passage
| title_orig    =
| translator    =
| image         =
| caption = Arthur Wright
| illustrator   =Percy Lindsay
| cover_artist  =
| country       = Australia
| language      = English
| series        =Bookstall series
| genre         = 
| publisher     = NSW Bookstall
| release_date  = 1921
| english_release_date =
| media_type    =
| pages         = 189
| isbn          =
| preceded_by   =
| followed_by   =
}}
Arthur Wrights original novel was published in 1921. 

===Plot===
Laurie Larand, a returned soldier, discovers that the barmaid he has entrusted with his money is missing. After a bad day at the races he has no money. He goes to live in the Domain but is helped by a trainer and an actor friend to get back on his feet. He discovers the trainer is in cahoots with bookmakers.

===Reception===
The novel appears to have been well received. "He shows to advantage as a writer of humor", said one critic.  Another stated that, "Not many Australians, perhaps, are writing "literature", but quite a fair number are turning out readable and respectable yarns, and Arthur Wright is one of the number." 

==Production==
The film was made with Wrights close involvement.  Hayford Hobbs was an English actor touring Australia when the film was made.  

==Reception==
The movie was distributed by Barrett himself, due in part to his difficulties with the Australasian Films monopoly, and was not widely seen. 

Arthur Wright later said the film was:
 Produced and photographed excellently by Franklyn Barrett, but bringing little grist to the mill of movie  picture production. It was a flop financially, as were practically all the latter day local silents, which were never given the chance they deserved. Fate and oversea interests were against the Industry, which went into a decline.    
Barretts company soon wound up and he left filmmaking to go into cinema management. 

===Critical===
The Advertiser called the movie "a delightful comedy-drama" in which Arthur Albert "is excellently cast".  The Register called it "a stirring racing film" which "cannot fail to please the most exacting. In addition the comedy in the picture is exceedingly clever, and productive of many hearty laughs."  The Launceston Daily Telegraph said that "from the very first moment that the screen reflected the delightful panorama of our bush land I knew that here at last I had found a picture which, would prove worth while the time it had taken to produce." 

==See also==
*List of lost films

==References==
 

==External links==
* 
*  at National Archives of Australia
*  at National Film and Sound Archive
*  at AustLit
* 
*Story was serialised in 1939 –  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,   
 
 

 
 
 
 
 
 
 