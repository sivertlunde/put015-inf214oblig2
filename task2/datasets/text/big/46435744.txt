My Golden Years
 
{{Infobox film
| name           = My Golden Years
| image          = 
| caption        =
| director       = Arnaud Desplechin
| producer       = Oury Milshtein Tatiana Bouchain
| writer         = Arnaud Desplechin Julie Peyr
| starring       = Quentin Dolmaire Lou Roy-Lecollinet Mathieu Amalric
| music          = Grégoire Hetzel
| cinematography = Irina Lubtchansky 
| editing        = Laurence Briaud
| studio         = Why Not Productions France 2 Cinéma
| distributor    = Le Pacte
| released       =  
| runtime        = 120 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

My Golden Years (French title: Trois souvenirs de ma jeunesse) is an upcoming French drama film directed by Arnaud Desplechin. It is a prequel to the 1996 film My Sex Life... or How I Got into an Argument.  It has been selected to be screened as part of the Directors Fortnight section of the 2015 Cannes Film Festival. 

== Cast ==
 
* Quentin Dolmaire as Paul Dédalus 
* Lou Roy-Lecollinet as Esther 
* Mathieu Amalric as Paul (adult)
* Dinara Droukarova as Irina
* Cécile Garcia-Fogel as Jeanne Dédalus
* Françoise Lebrun as Rose
* Irina Vavilova as Mme Sidorov
* Olivier Rabourdin as Abel Dédalus
* Elyot Milshtein as Marc Zylberberg 	
* Pierre Andrau as Kovalki
* Lily Taieb as Delphine Dédalus
* Raphaël Cohen as Ivan Dédalus
* Clémence Le Gall as Pénélope
* Théo Fernandez as Bob
* Anne Benoît as Louise
* Yassine Douighi as Medhi
* Ève Doé-Bruce as Professor Béhanzin
* Mélodie Richard as Gilberte
* Éric Ruf as Kovalki (adult)
 

== References ==
 

== External links ==
*   
*  

 

 
 
 
 
 
 
 
 
 