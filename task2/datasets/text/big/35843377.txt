Lilies of the Field (1930 film)
{{Infobox film
| name           = Lilies of the Field
| image          = Lilies_of_the_Field_1930_Poster.jpg
| image_size     =
| caption        =
| director       = Alexander Korda
| producer       = Walter Morosco
| writer         = William J. Hurlbut(play)   John F. Goodrich
| narrator       = John Loder Eve Southern
| music          =
| editing        =
| cinematography = Lee Garmes
| studio         = First National Pictures
| distributor    = First National Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} American talkie|all-talking John Loder. Lilies of the Field, in which Griffith had played the same role.  Both films were based on a 1921 play of the same name by William J. Hurlbut. Lilies of the Field was Corrine Griffiths first all-dialogue film.

==Cast==
* Corinne Griffith&nbsp;– Mildred Harker
* Ralph Forbes&nbsp;– Ted Willing John Loder&nbsp;– Walter Harker
* Eve Southern&nbsp;– Pink
* Jean Laverty&nbsp;– Gertie
* Tyler Brooke&nbsp;– Bert Miller
* Freeman Wood&nbsp;– Lewis Conroy
* Patsy Page&nbsp;– Baby
* George Beranger&nbsp;– Barber
* Douglas Gerrard&nbsp;– Headwaiter
* Rita La Roy&nbsp;– Florette
* Betty Boyd&nbsp;– Joyce
* May Boley&nbsp;– Maizie
* Virginia Bruce&nbsp;– Doris
* Charles Hill Mailes&nbsp;– Judge
* Raymond Largay&nbsp;– Harkers lawyer
* Joseph E. Bernard&nbsp;– Mildreds lawyer
* Tenen Holtz&nbsp;– Paymaster
* Wilfred Noy&nbsp;– Butler
* Anne Schaefer&nbsp;– First maid
* Clarissa Selwynne&nbsp;– Second maid
* Alice Moe&nbsp;– Third maid

==Synopsis==
Walter Hawker (Loder), who is married to Mildred Harker (Griffith), falls in love with another women and wants to separate from his wife without losing custody of his daughter. He frames his wife and files for divorce, and Griffith ends up losing her daughter. 
 Broadway showgirl and drowns her depression in a life of alcohol and jazz. Ted Willing (Forbes), a wealthy man, becomes her devoted admirer, but after her experience with her ex-husband, Mildred finds it hard to trust anyone. When Willing offers Mildred financial help, she refuses to accept anything, fearing that her daughter may hear about it. Eventually she realizes that her daughter has completely forgotten about her and allows Willing to take care of her. One day, while she is at a party, Mildred hears of her daughters death and has a breakdown. She is eventually jailed for vagrancy and disorderly conduct. Willing comes to the police station and rescues her.

==Songs== The Tenderfoot.

==Preservation==
No copies of this film are known to exist and it is believed that the film is now lost film|lost. Fragments of the  "Mechanical Ballet"  sequence are preserved in the 1932 Joe E. Brown comedy film entitled The Tenderfoot (1932 film).

==See also==
*List of lost films

==References==
 

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.

==External links==
* 

 

 
 
 
 
 
 
 
 
 