Six Cylinder Love
 

{{infobox film
| name           = Six Cylinder Love
| image          = File:6 Cylinder Love poster.jpg
| imagesize      =
| caption        = Film poster
| director       = Elmer Clifton William Fox
| writer         =
| starring       = Ernest Truex Florence Eldridge Donald Meek
| cinematography = Alexander G. Penrod
| editing        = Ralph Spence
| distributor    = Fox Film Corporation
| released       = November 4, 1923
| runtime        = 7 reels
| country        = United States
| language       = Silent film (English intertitles)}}
 1923 silent film comedy produced and distributed by Fox Film and directed by Elmer Clifton. It is based on a popular 1921 Broadway play and stars Ernest Truex from the play.  Also appearing in the film from the Broadway play were Donald Meek and Ralph Sipperly.
 Thomas Mitchell. early sound remake starring newcomer Spencer Tracy. As 20th Century Fox the studio remade the story over again in 1939 as The Honeymoons Over. 

==Cast==
*Ernest Truex - Gilbert Sterling
*Florence Eldridge - Marilyn Sterling
*Donald Meek - Richard Burton
*Maude Hill - Geraldine Burton
*Ann McKittrick - Phyllis Burton
*Marjorie Milton - Marguerite Rogers Thomas Mitchell - Bertram Rogers
*Ralph Sipperly - William Donroy
*Berton Churchill - George Stapleton
*Helen Morgan (unbilled extra)

==References==
 

==External links==
* , imdb.com; accessed November 16, 2014. 
* , allrovi.com; accessed November 16, 2014.

 
 
 
 
 
 
 
 
 


 