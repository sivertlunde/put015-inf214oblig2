Car Babes
{{Infobox film
| name           = CarBabes
| image          = CB1.jpg
| caption        = Theatrical poster
| alt            = 
| director       = Nick Fumia Chris Wolf
| producer       = Ben Rekhi Elizabeth Destro
| writer         = Blake Dirickson Nick Fumia Chris Wolf
| starring       = Ben Savage Jon Gries Blake Clark Donnell Rawlings
| music          = Dino Campanella
| cinematography = Oden Roberts
| editing        = Cary Gries
| released       = Hollywood Film Festival October 22, 2006 United States December 26, 2006
| runtime        = 87 minutes
| country        = United States
| language       = English 
}}
CarBabes   is a 2006 independent comedy film starring Ben Savage and directed by Nick Fumia and Chris Wolf.

The title of producer Ben Rekhis CarBabes comes from the term car salesmen use for each other. He realizes that some might suspect otherwise, but promises "its in no way a bikini car wash movie".

The film is a coming-of-age comedy starring Boy Meets World s Ben Savage as a recent college grad living at home who reluctantly goes to work at his dads car dealership. "The original idea emerged from stories our co-writer Blake Dirickson entertained us with about working for his father on his car lot", say co-directors Nick Fumia and Chris Wolf, who jack up their films tension with the threat of a hostile takeover and the requisite romantic interest. The soundtrack ranges from hip-hop to country.

CarBabes had its premiere on December 26, 2006.

==Overview==
Directed and produced by five Bay Area filmmakers, CarBabes is the first feature-length film written, directed, and produced by all five filmmakers, who channeled their love for the art of making films to produce this slapstick comedy. 

 
 Santa Clara, where he worked for 18 months. He would often share stories with his friends about the car babes he worked with. He pitched the concept to first-time filmmakers Chris Wolf, 27, Nick Fumia, 27, Ben Rehki, 28, and Liz Destro, 27, and they loved the idea.

"The biggest challenge we faced with this project was convincing friends and strangers to invest in a group of filmmakers who didnt have much under their belts in the realm of filmmaking," Wolf said.   

"It was the most emotionally and physically draining thing Ive ever done and I loved every minute of it," said Fumia. 

The 35mm CarBabes, was shot at Moore Pontiac & Buick in Los Gatos, California, for 26 days late Summer, 2005, with feature film cinematographer Oden Roberts. Co-starring in CarBabes are Jon Gries, Blake Clark, Donnell Rawlings, Carolina Garcia and David Shackelford.
 Stanford graduate explains. "Like that two- or three-year period right out of college where everythings a possibility, everythings terrifying. You dont really have any sense of stability. Its the first time youre out on your own in life and I think thats such an interesting time."   

Savage is in almost every scene of the film and he appreciates the level of collaboration involved. "Working with these guys, it really makes you feel like its a group project. Theyre all young (and) they are very open and receptive to everyones ideas. Its a really great set to be around." 

Having to put the film together on a "spit and glue" budget (an industry term for low budget), covering 126 days of script in 26 days, caused the cast to improvise and adjust along the way, according to Wolf. He said it is under these circumstances, however, that the most creative things happen. 

Much like the car babes they portray in the film, Wolf said being a filmmaker also starts and ends with the ability to sell yourself. He said CarBabes is a stepping-stone to something bigger for each of the filmmakers. 

"Watching a little dream turn into something that is essentially immortal and the experience of creating something that starts on piece of paper and ends up on the big screen is really rewarding," Wolf said. 

==Plot==
Ford Davis, a 24-year-old recent college graduate with no direction in his life, is forced to live with his parents and work for his dad, Big Len, at his car dealership to pay off a loaner car he damaged. On the lot, Ford quickly learns that the game of car sales is not easy. His only saving grace becomes his co-workers, the heartwarming and irreverent CarBabes, who teach him that selling cars is about selling yourself. After an unforgettable test drive with a self-assured hairdresser, Ford begins to gain confidence not only selling cars, but with his new sexy girlfriend.

But back on the lot, mobile home tycoon Ron Hamper, owner of the neighboring Hamper’s Campers, has his evil eye on Big Lens property. After an uncalled-for visit, Hamper threatens to shut Big Len down and turn Davis Automotive into a parking lot for his used campers.

With the days at Davis Automotive numbered, Big Len and the CarBabes have all but given up, but Ford comes up with an idea to save the dealership. Using the wisdom he has gained from his father and the rest of the team, Ford concocts one final plan: a blowout sale to sell 300 cars in a month.

==Cast==
 , Donnell Rawlings and David Shackelford.]]
* Ben Savage as Ford Davis
* Jon Gries as Gary
* Blake Clark as "Big Len" Davis
* Donnell Rawlings as Julius Jefferson
* Kevin Blackton as Ron Hamper
* Stephanie Keeney as Judy Gary
* Arj Barker as Jimmy
* David Shackelford as Pat Skis
* John Campo as Dicky Deluna
* Marshall Manesh as Babu Gulab
* Carolina Garcia as Alicia Santos
* Amy Resnick as Linda Davis

==Home media==
Car Babes was released in Region 1 DVD on June 10, 2008, by Vivendi Visual Entertainment.

==References==
 

==External links==
*  

 
 
 
 
 
 