Oedipus Rex (film)
{{Infobox film name = Oedipus Rex image = StefanoBattaglia Pasolini.jpg director = Pier Paolo Pasolini producer = Alfredo Bini writer = Sophocles (tragedy) Pier Paolo Pasolini starring = Silvana Mangano Franco Citti Alida Valli Carmelo Bene Julian Beck Luciano Bartoli Francesco Leonetti Ahmed Belhachmi Giovanni Ivan Scratuglia Giandomenico Davoli Ninetto Davoli cinematography = Giuseppi Ruzzolini editing = Nino Baragli distributor = Euro International Films released = 3 September 1967 (premiere at Venice Film Festival|VFF) runtime = 104 minutes country = Italy language = Italian
}} Italian film Greek tragedy Oedipus the King written by Sophocles in 428 BC. The film was mainly shot in Morocco.

==Plot==
A son is born to a young couple in pre-war Italy. The father, motivated by jealousy, takes the baby into the desert to be abandoned, at which point the film’s setting changes to the ancient world. The child is rescued, named Edipo by King Polybus (Ahmed Belhachmi) and Queen Merope (Alida Valli) of Corinth and raised as their own son. When Edipo (Franco Citti) learns of a prophesy foretelling that he will kill his father and marry his mother, he leaves Corinth believing that Polybus and Merope are his true parents.

On the road to Thebes, Edipo meets Laius (Luciano Bartoli), his biological father, and kills him after an argument. Later Edipo solves the riddle of the Sphinx. For freeing the kingdom of Thebes from the Sphinxs curse Edipo is rewarded with kingship and marriage to queen Jocasta (Silvana Mangano), who is his biological mother. When they discover what they have done, fulfilling the prophecy, Edipo blinds himself and Jocasta commits suicide.

==Awards==
Italian National Syndicate of Film Journalist 
Won Silver Ribbon 
Alfredo Bini for Best Producer and Luigi Scaccianoce for Best Production Design 
1968

Kinema Junpo Awards 
Won Best Foreign Language Film 
1970

Venice Film Festival 
Nominated for Golden Lion 
1967

==References==
 

==External links==
* 
* 
*  at  

 
 

 
 
 
 
 
 
 
 