Love Life (2007 film)
{{Infobox film
| name           = Love Life   (Liebesleben)
| image          = Liebesleben-poster.jpg
| caption        = Original film poster
| director       = Maria Schrader
| screenplay     = Maria Schrader and Laila Stieler
| story          = Zeruya Shalev  
| starring       = Netta Garti  Rade Sherbedgia
| producer       = 
| music          = Niki Reiser
| cinematography = Benedict Neuenfels
| editing        = Antje Zynga
| distributor    = 
| released       = 2007 
| runtime        = 
| country        = Israel / Germany
| language       = 
| budget         = 
| gross          = 
}}
Love Life (known as Liebesleben in German language|German) is a 2007 German/Israeli film directed Maria Schrader and shot in Israel. It is based on a novel by Zeruya Shalev, with the screenplay written by Schrader and Laila Stieler. The film won two 2008 Bavarian Film Awards for Best Cinematography (Benedict Neuenfels) and  Best Music (Niki Reiser). Neuenfels also won the 2008 German Film Award for Best Cinematography.

==Cast==
*Netta Garti as Yaara
*Rade Sherbedgia as Arie
*Tovah Feldshuh as Hannah
*Stephen Singer as Leon
*Ishai Golan as Joni
*Arie Moskonaas Nathan
*Caroline Silhol as Josephine
*Assi Dayan as Jaras Professor
*Clara Khoury as Shira
*Gillian Buick as Vivien Zach Cohen as Cat Killer
*Leonie Kranzle as Kenneth
*Zeruya Shalev as Librarian
*Esther Zewko as Guest

==Awards and nominations==
*2008: Benedict Neuenfels won Best Cionematography award for the film in German Film Awards
*2008: Christian M. Goldbeck nominated for Best Production Design for the film in German Film Awards
*2008: Benedict Neuenfels won Best Cionematography award for the film in the Bavarian Film Awards
*2008: Niki Reiser won Best Music award for the film in the Bavarian Film Awards

==Soundtrack==
{{Infobox album 
| Name        = Liebesleben
| Type        = soundtrack
| Artist      = 
| Cover       = CD
| Recorded    =
| Genre       = Soundtrack
| Length      = 
| Label       = Normal (Indigo)
| Producer    = 
}}
A soundtrack album was released containing the following:
#Love Life
#Attracted to Arie
#"If She Had Chosen You..."
#"This Is My Life"
#Jara
#"Dont Forget to Exhale"
#Abused
#Finding the Truth
#The Desert
#Bewildered
#Caught
#The Photo
#Jaras Quest
#Road to Akko

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 

 