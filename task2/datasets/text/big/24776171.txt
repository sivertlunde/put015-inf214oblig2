The Big Idea (1934 film)
{{Infobox Film |
  | name           = The Big Idea
  | image          = Bigidea34colorcard.jpg
  | image size     = 190px
  | caption        =  
  | director       = William Beaudine
  | writer         = Ted Healy Matty Brooks Henry Taylor Tut Mace MGM Dancing Girls
  | music          = L. Wolfe Gilbert
  | cinematography = 
  | editing        = 
  | producer       = 
  | distributor    = Metro-Goldwyn-Mayer
  | released       = May 12, 1934 (US)
  | runtime        = 18 40"
  | country        = United States
  | language       = English
}}
The Big Idea is a  , Larry Fine, and Curly Howard. 

==Production==
Like other shorts Healy and the Stooges filmed at MGM, stock footage was utilized to fill out the 20 minutes of time. For The Big Idea, MGM used musical numbers edited out of the feature film Dancing Lady (1933), which ironically had a supporting role by Healy and a cameo by the Stooges. 
 The March of Time (1930), which later became the title of a long-running newsreel series. 

By the time of the release of The Big Idea, the Three Stooges had signed a new contract with Columbia Pictures to do a series of comedy short films without Healy, beginning with Woman Haters (1934).

==References==
 

==See also==
*The Three Stooges filmography

==External links==
* 
*  at  
 


 
 
 
 
 
 
 
 

 