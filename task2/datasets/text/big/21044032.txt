Reckless (1951 film)
{{Infobox film
| name           = Reckless
| image          = 
| caption        = 
| director       = José Antonio Nieves Conde
| producer       = 
| writer         = Vicente Escrivá
| starring       = Manolo Morán
| music          = 
| cinematography = José F. Aguayo Manuel Berenguer
| editing        = Juan Serra
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Reckless ( ) is a 1951 Spanish drama film directed by José Antonio Nieves Conde. It was entered into the 1951 Cannes Film Festival.     

==Cast==
* Manolo Morán - Desiderio
* Jesús Tordesillas - Carlos
* Dina Sten - Lina
* Luis Prendes - Fernando
* José Bódalo - Presidente del club
* Maruchi Fresno - Elena
* María Rosa Salgado - Mayte
* Eduardo Fajardo - Mario
* Fernando Fernán Gómez - Javier Mendoza Balarrasa
* Fernando Aguirre - Valentin
* Francisco Bernal - Emiliano
* Mario Berriatúa - Teniente Hernández
* Julia Caba Alba - Faustina
* Chano Conde - Teniente
* Alfonso de Córdoba - Alférez
* Gérard Tichy - Zanders

==References==
 

==External links==
* 

 
 
 
 
 
 
 