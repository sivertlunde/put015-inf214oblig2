Take Care of Your Scarf, Tatiana
{{Infobox film
| name           = Take Care of Your Scarf, Tatiana 
| image          = Take Care of Your Scarf, Tatiana DVD cover.jpg
| image_size     = 
| caption        = Take Care of Your Scarf, Tatiana DVD cover
| director       = Aki Kaurismäki 
| producer       = Aki Kaurismäki
| writer         = Aki Kaurismäki Sakke Järvenpää
| starring       = Kati Outinen Matti Pellonpää Kirsi Tykkyläinen Mato Valtonen Elina Salo
| music          = Veikko Tuomi
| cinematography = Timo Salminen
| editing        = Aki Kaurismäki
| released       = 14 January 1994
| runtime        = 65 mins (Canada) 60 min (Germany)
| country        = Finland / Germany Finnish / Russian
| budget         = 
}} German film directed, produced and co-written by Aki Kaurismäki. The film tells the story of two shy and unaccomplished middle-aged men who run away from their mothers homes and drive aimlessly on the back roads of Finland.

==Plot==
Valto (Mato Valtonen) lives with his mother, who runs a clothing business. Valto is seen sewing clothes for his mother at the beginning of the film. When he discovers the coffee has run out, and his mother refuses to make him new one right away, he locks his mother in a cupboard, steals her money and goes out. Valto finds Reino (Matti Pellonpää), who has been fixing Valto’s car. They then go on a road trip with no apparent destination. With music coming out of a in-car vinyl player of Valto’s car and Reino’s story about how he got into some legal trouble after he punched someone, the two seemingly enjoy the road trip. This is a typical Kaurismäki’s ordinary-scene-made-interesting moment. 

When they made a stop at a bar, they met Tatiana (Kati Outinen) from Estonia and Klavdia (Kirsi Tykkyläinen) from Russia. Tatiana and Klaydia asked Valto and Reino to drive them to a harbor. Since Valto and Reino’s road trip does not have a clear destination at the first place, Valto and Reino agree to take Tatiana and Klaydia. 

Throughout the journey, Valto has been drinking coffee and Reino drinking vodka. Although Reino is reserved in his nature, he sits down next to Tatiana during one of the stops of the journey. Tatiana relaxes her head on Reino’s shoulder. The film suggests the two have fallen in love at this point. 

At the end of the film, Reino and Tatiana remain in love with each other and Valto returns home, releases his mother out of the cupboard and resumes to sewing clothes. 

==Beyond the Plot==
As always, Kaurismäki manages to make the ordinary objects and characters speak. In this film, the supposedly complex emotion, love, is made to seem simple and yet irresistible through Kaurismäkis masterful use of angles of shots, lights, and his daringly but brilliantly prolonged shots on simple objects and dialogues. 

==Cast and characters==
* Kati Outinen - Tatjana
* Matti Pellonpää - Reino
* Kirsi Tykkyläinen - Klavdia
* Mato Valtonen - Valto
* Elina Salo - Hotel Receptionist
* Irma Junnilainen - Valtos mother

==External links==
* 
*  

 

 
 
 
 
 

 