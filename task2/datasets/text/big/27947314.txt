Requiem pro panenku
 
{{Infobox film
| name           = Requiem pro panenku
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Filip Renč
| producer       = 
| writer         = 
| narrator       = 
| starring       = Anna Geislerová Soňa Valentová Eva Holubová
| music          = Ondřej Soukup
| cinematography =
| editing        = 
| studio         = 
| distributor    = 
| released       = 1992
| runtime        = 94 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Czech drama film. It was released in 1991.

==Cast==

* Aňa Geislerová
* Eva Holubová
* Barbora Hrzánová
* Filip Renč
* Soňa Valentová
* Jaroslava Hanušová Jan Schmid
* Věra Nováková
* Eduard Cupák
* Stanislav Tříska
* Vlasta Mecnarowská
* Zuzana Dančiaková
* Josef Klíma

==External links==
*  
 
 
 
 