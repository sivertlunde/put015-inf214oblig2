Den sommeren jeg fylte 15
{{Infobox film
| name           = Den sommeren jeg fylte 15
| image          = 
| image size     =
| caption        = 
| director       = Knut Andersen
| producer       = 
| writer         = Knut Faldbakken (novel)   Knut Andersen
| narrator       =
| starring       = Steffen Rothschild
| music          = Eyvind Solås
| cinematography = 
| editing        = 
| distributor    = 
| released       = 4 March 1976
| runtime        = 90 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama  film directed by Knut Andersen, starring Steffen Rothschild, and music composed by Eyvind Solås. The film is based on Knut Faldbakkens novel Insektsommer (insect summer). Peter (Rothschild), who was a teenager in the 50s, is reminiscing about the days of his youth, and his awakening sexuality.

==External links==
*  
*  
*   at the Norwegian Film Institute

 
 
 
 
 

 