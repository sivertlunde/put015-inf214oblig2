China Lake (film)
China Charles Napier as a deranged police officer, who rides around on a motorbike "targeting" people that have offended him in some way while vacationing near the films titular location of China Lake. 

The short film was directed by Robert Harmon, who funded this movie personally to promote himself as a director.

In 1990, the film was made into a feature-length made-for-TV movie, called The China Lake Murders, which is considered to be a remake of this film, rather than being an actual sequel. 

==Plot summary==

Donnelly is a disturbed cop on vacation leave.  Despite being off-duty, he is still in his uniform. As he rides through the desert on his Harley-Davidson|Harley, he stops a lady whom he accuses of being drunk; while exhausted from driving continuously for hours, she is obviously anything but drunk. Donnelly proceeds to lock her in the trunk of her car, leaving her on a quiet road where no one is likely to pass by for some time.

Continuing on, he stops at a closed petrol station in the middle of the night. The pumps are padlocked, so he takes out his revolver and shoots the lock off so he can refill his bike with fuel. The next day, Donnelly goes to China Lake for some relaxation and then goes to a nearby diner for lunch and coffee, which he refers to as Black Mud.

Two cement workers are sitting at the main counter and they arouse his ire to the point that he completely forgets the (somewhat pleasant) conversation he was having with the waitress. Later that same day, he notices a car on the side of the road belonging to one of the truckers, as it has the same trademark on it. After the other trucker leaves his friend to repair the car by himself, Donnelly, who has been watching and waiting nearby for the men to return, makes a reappearance and chases the confused man before knocking him down. He then locks the man in the trunk of his car and pushes the car over the stretch of desert next to the road.

Donnelly is next shown returning to work.  When the watch commander asks Donnelly how his vacation was, he responds that he enjoyed it. The film comes to an end as the credits roll.

==References==
 

 

 
 
 
 

 