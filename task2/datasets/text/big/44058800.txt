Saritha (film)
{{Infobox film
| name = Saritha
| image =
| caption =
| director = P Govindan
| producer = Suvarna Regha
| writer = JC George
| screenplay = JC George
| starring = Vidhubala Mohan Sharma Bahadoor Manju Bhargavi Shyam
| cinematography = Madhu Ambatt
| editing =
| studio =
| distributor =
| released =  
| country = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by P Govindan and produced by Suvarna Regha. The film stars Vidhubala, Mohan Sharma, Bahadoor and Manju Bhargavi in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Vidhubala 
*Mohan Sharma 
*Bahadoor 
*Manju Bhargavi  Madhu 
*MG Soman

==Soundtrack== Shyam and lyrics was written by Sathyan Anthikkad. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Hemanthathin || S Janaki || Sathyan Anthikkad || 
|- 
| 2 || Mazhathullithulli || K. J. Yesudas || Sathyan Anthikkad || 
|- 
| 3 || Ormayundo || P Jayachandran, Mallika Sukumaran || Sathyan Anthikkad || 
|- 
| 4 || Pooveyil Mayangum || P Susheela || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 


 