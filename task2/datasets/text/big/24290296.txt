The Captain's Table (1936 film)
 
{{Infobox film
| name           = The Captains Table
| image          = 
| image_size     = 
| caption        = 
| director       = Percy Marmont
| producer       = James A. Fitzpatrick
| writer         = John Paddy Carstairs
| narrator       = 
| music          = Gideon Fagan  Marian Spencer   Louis Goodrich   Daphne Courtney
| studio         = FitzPatrick Pictures  MGM
| released       = November 1936
| runtime        = 55 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British crime Marian Spencer liner is found strangled, leading to a police investigation.  The film was made at Shepperton Studios as a quota quickie for distribution by Metro-Goldwyn-Mayer. 

==Cast==
* Percy Marmont - John Brooke Marian Spencer - Ruth Manning
* Louis Goodrich - Captain Henderson
* Daphne Courtney - Mary Vaughan Mark Daly - Sanders
* Phillip Brandon - Eric Manning Hugh McDermott - Inspector Mooney

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The British of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
 

 
 
 
 
 
 
 
 
 

 
 