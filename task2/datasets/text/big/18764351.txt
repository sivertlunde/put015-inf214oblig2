Mother Goose Goes Hollywood
{{multiple issues|
 
 
}}
 
{{ Infobox Hollywood cartoon 
| cartoon name       = Mother Goose Goes Hollywood
| series             = Silly Symphony
| image              = File:Mother Goose Goes Hollywood poster.jpg
| image size         = 
| alt                = 
| caption            = Theatrical release poster
| director           = Wilfred Jackson
| producer           = Walt Disney
| story artist       = 
| narattor           =  Danny Webb
| musician           = Edward H. Plumb  Don Patterson Robert Stokes
| layout artist      = 
| background artist  =  Walt Disney Productions RKO Radio Pictures
| release date       =  
| color process      = Technicolor 
| runtime            = 7 minutes
| country            = United States 
| language           = English 
| preceded by        = Merbabies
| followed by        = The Practical Pig
}} Walt Disney RKO Radio Pictures. The film parodies several Mother Goose nursery rhymes using caricatures of popular Hollywood film stars of the 1930s. The film was directed by Wilfred Jackson and was the last to have a Silly Symphony title card, even though it was the third to last film of the series.

==Plot== Leo the Lion from the MGM title cards. Underneath the goose is written, in Pig Latin, "Nertz to You". The opening disclaimer states that "any resemblance to characters herein portrayed to persons living or dead, is purely coincidental." Little Bo Peep (Katharine Hepburn) claims she "lost her sheep, really I have". After performing a few ballet steps she looks behind the next page of the book, which is turned around.

The next scene shows Old King Cole (Hugh Herbert) excited when his fiddlers arrive (The Marx Brothers). The trio starts playing their violins, but then break them over their knees. The king enjoys this very much, but his court jester (Ned Sparks) obviously does not. The king commands "off with their heads." Then Joe Penner brings the king a bowl and, in reference to his famous catch phrase, asks "wanna buy a duck?" Donald Duck appears out of the water in the bowl, repeats Penners catchphrase and starts laughing. The king then replaces the lid on the bowl, much to the chagrin of Donald.
 Captains Courageous) Captains Courageous and is dressed as Little Lord Fauntleroy, which he also starred in). Just like a similar scene in "Captains Courageous" Bartholomew falls overboard, but Tracy pulls him back in. Then Katharine Hepburn passes by on an outboard motor still looking for her sheep. The tub overturns when the trio tries to hitch a ride with Hepburn.

W. C. Fields plays Humpty Dumpty. He inspects a birds nest with the words, "My Little Chickadee", but discovers Charlie McCarthy sitting in it. He insults Fields who tries to attack him, but then falls off the wall onto a mushroom which then resembles an egg cup.
 Simple Simon (Stan Laurel) is seen fishing with a fish on his hook and catching worms instead of the other way around. The Pieman (Oliver Hardy) is busy tending a large stack of his pies on a wagon.  Laurel refuses an offered pie, but picks one from the middle of the stack, which alarms Hardy, fearing the stack will collapse. Nothing happens, however and a reassured Hardy tries to do the same. When the stack collapses and one of the pies lands on his head, he looks angrily at Laurel. Laurel swallows his pie in one piece and then snickers at Hardy. Hardy throws one of his pies at Laurel, who ducks, and the pie lands in the face of Katharine Hepburn. The pie transforms her face into a blackface and she starts speaking in African-American slang.

See Saw Margery Daw is performed by Edward G. Robinson and Greta Garbo on a seesaw. Garbo says: "I want so much to be alone", to which Robinson replies: "O.K., babe, you asked for it!". He leaves and Garbo falls off the seesaw.

Little Jack Horner (Eddie Cantor) opens the next scene, a big musical sequence. He sings Sing a Song of Sixpence and when he mentions the line, "twenty black birds baking a pie" several African-American jazz and swing musicians stick their head out of a large pie. One of them is Cab Calloway (singing "Hi-de-Ho!") who invites Little Boy Blue (Wallace Beery) to blow his horn. When this takes some time, Fats Waller asks "Wheres that boy?" to which Stepin Fetchit replies "What boy?" Beery finally wakes up and blows his horn until hes out of breath.

The book pops open to reveal a big shoe (a reference to There was an Old Woman Who Lived in a Shoe) and all the characters start singing, dancing and playing instruments. The camera zooms in on three trumpet playing ladies (Edna May Oliver, Mae West and ZaSu Pitts), a flute player (Clark Gable) and a saxophonist (George Arliss). Oliver Hardy plays trombone and Stan Laurel clarinet, whose repeated notes annoy Hardy so much he hits Laurel over the head with a hammer. Laurels clarinet then sounds like a bass clarinet.
 Joe E. Brown are seen dancing and laughing so loud that their mouths are opened wide. When Raye kisses Joe E. Brown (leaving a large lipstick smear) his mouth opens so wide that the camera tracks inside. There, Katharine Hepburn is still looking for her sheep.

==Cultural references==
* The cartoon was nominated for an Academy Award for Best Animated Short, but lost to another Disney cartoon short,   Ferdinand the Bull (film)|Ferdinand The Bull
* Katharine Hepburns type of speech, which pronounces the word "really" as "rally", has been spoofed in other 1930s and 1940s era cartoons, including Red in Tex Averys Red Hot Riding Hood
* Donald Duck makes his third and final appearance in a Silly Symphonies cartoon. (The first one was his debut in The Wise Little Hen (1934). He made a cameo in a second silly symphonies cartoon Toby Tortoise Returns (1936).  
* Charles Laughtons role as the captain references his starring role as Captain Bligh in Mutiny on the Bounty (1935 film). Captains Courageous (film).
* When Hepburn sails by on sea she quotes the poem The Rime of the Ancient Mariner. Alice in Wonderland. one of his famous films two years later.
* W. C. Fields was often ridiculed by Charlie McCarthy on the NBC-Red "Chase and Sanborn Hour." Laurel and Hardy Signature Tune, the famous theme music of the Laurel and Hardy movies.
* Laurel and Hardys pie fight is a reference to similar scenes in their films.
* Greta Garbos famous quote: "I want to be alone" is a reference to her role in Grand Hotel (film).
* The joke where someone hits a clarinet player on the head with a hammer, causing his   (1935)
* When Chico Marx plays Wallers piano he points his hand like a pistol, in reference to his famous piano playing style.
* Cab Calloway sings "Hi-de-hoo" and "Zah-zu-zah-zu-zah", references to his songs "Keep that Hi-de-hoo in your soul" and "Zah-zu-zah".
* Martha Raye and Joe E. Brown are dancing together, because they were both famous for their big mouths.
* An official Disney page (see below) claims Rudy Vallee is also caricatured in this cartoon.
* Katharine Hepburn appears in the same scene as Spencer Tracy even though they had never met or worked together before. After 1942, they made ten movies together and secretly had an affair for 25 years.

==Censorship==

Since the 1960s this cartoon has not been broadcast very often on television, due to the stereotypical depictions of blacks in some scenes. Sometimes it has been broadcast minus the scenes with African Americans but as animation critic Charles Solomon noted in his book: "Enchanted Drawings: History of Animation", the caricatures of Fats Waller and Cab Calloway dont poke fun at their race and are treated just as good or bad like the other caricatured celebrities spoofed in this cartoon. 

==See also==

* Mickeys Gala Premiere
* Mickeys Polo Team
* The Autograph Hound
* Hollywood Steps Out
* Slick Hare
* Hollywood Daffy
* Felix in Hollywood

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 