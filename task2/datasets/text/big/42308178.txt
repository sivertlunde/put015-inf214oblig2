Mrs. Brown's Boys D'Movie
 
{{Infobox film
| name           = Mrs Browns Boys DMovie
| image          = Mrs_Brown_movie_poster.jpg
| caption        = Theatrical release poster
| alt            = A man dressed as an old woman, with a curly wig, and oval glasses, and large mole on his chin. His hands are crossed under his chin and he is smiling. 
| director       = Ben Kellett
| producer       = Stephen McCrum Brendan OCarroll
| writer         = Brendan OCarroll
| based on       =  
| starring       = {{Plain list | 
* Brendan OCarroll
* Eilish OCarroll
* Nick Nevern
* Paddy Houlihan
* Fiona OCarroll
* Jennifer Gibney
* Sorcha Cusack
* Keith Duffy
* Danny OCarroll
* Amanda Woods
* Chris Patrick-Simpson
* Robert Bathurst
* Dermot Crowley
* Gary Hollywood 
* Henry Sasnauskas
}}
| cinematography = Martin Hawkins
| editor         = Mark Lawrence
| music          = Andy OCallaghan
| studio         = Thats Nice Films Penalty Kick Films BocFlix BBC Films
| distributor    = Universal Pictures
| released       =  
| runtime        = 94 minutes 
| country        = Ireland United Kingdom
| language       = English
| budget         = £3.6 million 
| gross          = $28,840,379 
}} sales agent and it was distributed by Universal Pictures.    It was written by series creator (and company director of both Thats Nice Films and Bocflix) Brendan OCarroll, who also plays the lead role.
 Moore Street market from a corrupt Russian businessman who wishes to convert it into a shopping centre.

The film was released on 27 June to negative reviews from critics. It topped the UK and Ireland box office with £4.3 million in its opening weekend, on a budget of £3.6 million, and retained top spot for a second week. On 27 October it was released on home media, again topping the charts.

==Plot== Moore Street market. It has been under attack from P.R. Irwin (Dermot Crowley), a TD (PRIC) who is in an arrangement with a ruthless Russian businessman who wants to put all the market stalls out of business and open a shopping centre on the site. Her stall is the next to be targeted, being sent a bill for unpaid tax left by her grandmother, and a man (working for Irwin) appears offering to buy her stall and make the bill disappear. Agnes nearly accepts, but Winnie (Eilish OCarroll) reveals this news to the locals, forcing Agnes into defending her stall from the developers while they look for ideas on how to raise the money. Agnes’ friend Philomena Nine Warts informs her that her grandmother, Mary Moccasin, was next to Agnes’ grandmother at the tax office when she paid the bill and therefore no money is owed. Unfortunately Philomenas grandmother is hit by a bus on the way to the courtroom before she can testify.

Agnes court case attracts a lot of attention from the media, portraying her as the greatest mother in Ireland. This leads her to go to confession, where she admits (unknowingly also to a Russian mobster) that she briefly put her children in care when her husband died, but continued to claim the child support money. This is used against her in the witness box during questioning by Irwin in court(Irwin being the opposing Barrister, and she runs out in shame. Eventually being found by the river by her daughter Cathy (Jennifer Gibney), she admits all in a tearful moment on the Hapenny Bridge, telling her how she told the nuns that she thought she could look after two of the six children, but when asked to pick she was unable to.

Meanwhile, Buster (Danny OCarroll) and Agnes’ son Dermot (Paddy Houlihan) try to get the receipt. After failing to break into the restricted area of the NRS they recruit a troop of blind trainee ninjas, led by Mr. Wang (also played by O’Carroll). The Russians have already found and destroyed the original receipt, but Buster and Dermot learn the receptionist that took the payment was blind, so that there exists a braille version of the receipt. They find it and let Agnes know, telling her Tourette Syndrome|Tourettes-suffering barrister (Robert Bathurst) to stall the case. After navigating air ducts out of the NRS, Agnes, Buster and Dermot are chased by the mobsters and the Garda Síochána|Garda, jumping in the River Liffey. Agnes separates from the pair and returns with the "receipt" but it turns out Buster accidentally gave her a betting slip instead. At this point Cathy stands up and gives a speech on how special Moore Street and its market is, and her intention to run her mother’s stall when her time comes, to Agnes’ joy. After their pursuit continues in a jeep and finally a dash on a stolen horse, Buster and Dermot deliver the receipt to the court room just in time to have the case against Agnes dropped. They all celebrate by dancing on the steps of the courtroom.

==Cast==
* Brendan OCarroll as Agnes Brown/Mr Wang
* Nick Nevern as Gregor
* Eilish OCarroll as Winnie McGoogan
* Paddy Houlihan as Dermot Brown
* Jennifer Gibney as Cathy Brown
* Danny OCarroll as Buster Brady
* Dermot Crowley as P.R. Irwin
* Robert Bathurst as Maydo Archer
* Dermot ONeill as Harold "Grandad" Brown
* Fiona OCarroll as Maria Brown
* Simon Delaney as Tom Crews
* Chris Patrick-Simpson as Ninja Joe
* Keith Duffy as John
* Martin Delany as Trevor Brown
* Frank Kelly as Justice Cannon
* Rory Cowan as Rory Brown
* Gary Hollywood as Dino Doyle
* Pat Shields as Mark Brown
* Amanda Woods as Betty Brown
* Eamonn Holmes as Himself
* Sorcha Cusack as Justice Dickie
* June Rodgers as Fat Annie McCrum
* Joe Duffy as Himself
* Mark Coney as Clerk of the Court
* Jamie OCarroll as Bono Brown (Cameo)
* Henry Sasnauskas as Special Boy (uncredited)
* Emily Regan as Barbara
* Fiona Gibney as Sharon McGoogan
* Maire Hastings as Philomena Nine Warts
* Helen Spain as Maggie May
* Raj Ghatak as Rab Patel
* Laurie Morton as Mary Moccasin


==Production==
In September 2012 it was reported that an estimated £3.6 million deal was in place with Universal Studios to start production on the film version of Mrs. Browns Boys. A spokesman for OCarroll said that the film would have a distinctly Dublin flavour, "Its Dublin humour so youre going to need Dublin actors and technicians to get it right for the big screen."  Despite funding being secured, OCarroll later confirmed that a script had not been written prior to the deal. "I havent written it yet. Thats what success does, they give you money and say Whatever you think. So Ive taken the money!".  At the 2013 National Television Awards OCarroll confirmed the film had been written and would begin shooting the following autumn. 

Principal photography for Mrs. Browns Boys DMovie began on 1 September 2013 in Dublin.  Filming locations included Moore Street, Dublin quays, Father Matthew Square, The Customs House and Wimbledon Studios. Filming was completed on 25 October 2013.

The Script recorded a song for the film called "Hail, Rain or Sunshine". 

==Release==
The film had its world premiere on 25 June 2014 at the Savoy Cinema in Dublin, with Brendan OCarroll and the rest of the cast in attendance.   

The distributors of the film did not screen the film for critics in advance of its release. 

==Reception==

===Box office===
The film earned £4.3 million in the UK and Ireland in its opening weekend, breaking records in Ireland for the highest box office gross on the opening day of an Irish-made film. It was top of the UK and Ireland box office in this opening weekend, ahead of   and How to Train Your Dragon 2. 

With a total gross of £14.7 million, it was the third highest grossing British or Irish film in the domestic market in 2014, behind The Inbetweeners 2 and Paddington (2014 film)|Paddington.   

===Critical response===
The film  received a negative reception from critics. On review aggregator website Rotten Tomatoes it holds a 7% approval rating with an average score of 3.1/10 based on 14 reviews.  

Mike McCahill of   also gave the film 1 star out of 5 and said that it will "leave even the most fervent of fans disappointed by its abattoir of wit" because "while the TV show possesses a warm, ramshackle appeal, this story of granny Agnes Brown trying to save a Dublin market from dforeigners (boo!) is not only out of its comfort zone, but full of cold, mean-spirited gags about the blind, an Indian man everyone thinks is Jamaican (um, LOL?) and   Chinese caricature so dazzlingly racist it beggars belief." 

  of The Independent said that he "couldnt argue" with actor Rory Cowan who said that the views of critics were "totally irrelevant", but still found the film "dreadful... slow, sentimental, and altogether cynical", saying that "Im afraid I cant say I laughed, or even smiled, once in the whole godforsaken 93 minutes." He concluded by saying that "I don’t see why anyone is obliged to like   just because it does well at the box office... In any case, it doesnt really matter. It is now absolutely clear that Mrs Brown is a slating-proof juggernaut." 

In one of the few positive reviews, James Ward of the   will say   For the critics D’Movie may be a D’Isaster – but for the rest of us it’s a D’Light". 

 

==Possible sequels==
Brendan OCarroll announced there would be a sequel to DMovie at the National Television Awards in January 2014, whilst speaking to the Radio Times, OCarroll said "Were already working on a sequel – Mrs Browns Boys DMovie 2". 

On 20 June 2014, OCarroll announced that he was working on two spin-off feature films following on from Mrs. Browns Boys DMovie. The first spin-off film is to be entitled Wash and Blow if it is produced and it will see OCarroll take on the role of salon owner, Mario, alongside Rory Brown and Dino Doyle. The second spin-off film in development is to be entitled Mr Wang, who is a character introduced in DMovie. It was confirmed that English actor Burt Kwouk had been asked to take the title role but was unable to travel to Dublin, so OCarroll will take on the role himself if the film gets produced. It will also co-star Buster Brady and Dermot Brown working for him under a detective agency. It is currently not yet known when, or if, the spin-off films will be filmed nor released. 

However, during an interview in January 2015 OCarroll revealed that a sequel to DMovie is doubtful due to the long amounts of time it took to write the first film. He also stated that a fourth television series was also highly unlikely and that the show would only continue with Christmas Specials and that he was planning to work on something new.  

==Home media==
Mrs. Browns Boys DMovie was released on DVD and Blu-ray in the UK and Ireland on 27 October 2014.  Within the first week of release in the UK, the film sold 315,981 units across DVD, Blu-ray and digital downloads, topping the home entertainment charts with nearly twice as many sold as second-placed Godzilla (2014 film)|Godzilla. OCarroll said  “I’m always surprised, to this day, about how much people love the show and have embraced it into their lives and their homes. We never take it for granted and always just aim to make the audience and fans laugh out loud in everything we do.” 

==See also==
 
* List of films based on British sitcoms
*List of 2014 box office number-one films in the United Kingdom
* Cinema of Ireland

 

==References==
 

== External links ==
*  
*  
*   at BBC Films
*  

 
 
 
 
 
 
 
 
 
 
 