Orukkam
{{Infobox film
| name           = Orukkam ( ഒരുക്കം preparation )
| image          = Orukkam_Poster.jpg
| caption        =
| alt            =
| director       = K Madhu
| producer       = John Paul (screenplay) Ranjini  Murali
| Johnson
| Ashok Kumar
| editing        = V P Krishnan
| studio         =
| distributor    =
| released       = 1990
| runtime        =
| country        =  
| language       = Malayalam
| budget         =
| gross          =
}} Ranjini and Murali in lead roles. The film was directed by K Madhu.

==Plot== Punjab but lost everything in the riots there. He opens a small tea-shop in the village.
There he meets his old flame Bhaagi (Sithara (actress)|Sithara) who is now married and settled.
He also meets his uncle Koyikal Madhava Kurup (M S Thripunithura) who was responsible for his fleeing from the village and losing his girl. He returned to take revenge on his uncle but by the time his uncle already lost all his wealth. In the village he also becomes friends with Anthrappayi (Jagathy Sreekumar) and Paramu (Mamukkoya). His happy second life in the village ends one night with the arrival of Chandru (Murali (Malayalam actor)|Murali) from Mumbai|Bombay. It was revealed that Sethu was actually a henchman of Chandru in Bombay. Chandru asked him to take one more assignment else he will reveal Sethus real past to the villagers. Sethu had no choice but to take up the assignment and starts to Kodaikanal.

In Kodaikanal he stays with Ramu (Vijayaraghavan (actor)|Vijayaraghavan) who was an old associate of his in Bombay. Sethus assignment was to kill Radha (Ranjini (actress)|Ranjini) who teaches in a boarding school in Kodaikanal managed by Fr. Francis Arackal (Jose Prakash). One night Sethu went to murder Radha but was shocked to find her trying to commit suicide. Sethu instead saved her life and tries to found out who wants to kill her. Sethu understands that the person who wants to finish off Radha is actually Narayan Mehta (Lalu Alex) who is the boss of Chandru and Ramu. Narayan Mehta earlier married Radha as Narayanankutty for her money but later left her and made her believe that he died in an accident in Dubai. Radha met Narayanankutty, who is now  a big businessman called Narayan Mehta, in Kodaikanal. Narayan Mehta orders Chandru to have Radha killed, to prevent his secrets getting revealed. The cruel Mehta also destroyed the life of Kausalya (Parvathy Jayaram|Parvathy) by killing her rich father, grabbing all her wealth and reducing her to his handicapped wife in a wheel chair. Sethu develops sympathy towards Radha, fals in love with her and decides to protect her from Chandru and Narayan Mehta with the help of Ramu and Albert (Saikumar (Malayalam actor)|Saikumar).

They ended up having an open fight and Chandru was killed by Ramu who in turn was killed by Narayan Mehta. Sethu and Radha were chased by Narayan Mehta and finally Sethu kills Mehta with the help of Albert who also dies in the attempt.
The film ends with Sethu being taken by the police with Fr. Francis assuring him of getting acquitted considering the circumstances leading to the murder and Mehtas criminal past.

==Cast==
*Suresh Gopi     ...     Sethumadhava Kurup Murali        ...     Chandru Vijayaraghavan    ...     Ramu
*Lalu Alex    ...     Narayan Mehta (Narayanankutty) Saikumar    ...     Freddy
*Jose Prakash    ...     Fr. Francis Arackal
*Jagathy Sreekumar    ...     Anthrappayi
*Bahadoor        ...     Kumaran Master
*M. S. Thripunithura    ...     Koyikal Madhava Kurup
*Mamukkoya    ...     Paramu
*Prem Prakash    ...     Kuttikrishnan
 Ranjini        ...     Radha Sithara        ...     Bhaagi Parvathy    ...     Kausalya Kalpana        ...     Alice

==References==
* 
* 

 
 
 
 
 