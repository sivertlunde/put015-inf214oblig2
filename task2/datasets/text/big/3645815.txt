Ingmar Bergman Makes a Movie
{{Infobox film
| name           = Ingmar Bergman Makes a Movie
| image          = Ingmar Bergman Makes a Movie.jpg
| caption        = The Criterion Collections DVD cover for the documentary.
| director       = Vilgot Sjöman 
| producer       =
| writer         =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1963
| runtime        = 146 minutes
| country        = Sweden
| language       = Swedish
| budget         =
}}

Ingmar Bergman Makes a Movie ( ) is a 1963 Swedish documentary film directed by Vilgot Sjöman which depicts the making of Ingmar Bergmans film Winter Light from screenwriting to the films premiere and critical reaction.
 Through a The Silence (1963).

==Production==
Sjöman was beginning to write screenplays, and wishing to learn about directing, asked Bergman if he could observe him making a film.  Sjöman then approached Swedish Television who were interested in airing a documentary about the process.  Bergman allowed for Sjömans crew to film rehearsals, but not the actual shooting.  Bergman was also interviewed for the project.  Sjöman himself would go on to direct films such as I Am Curious (Yellow) (1967).

==Reception==
Writing for Epinions in 2007, Stephen O. Murray called the film "A valuable document of a master craftsman working in top form."  He also stated that "Sjöman comes across as very earnest and relatively probing."   Brian Burke of DVD Verdict called it the "best documentary Ive ever seen on the filmmaking process," adding Bergman appears "candid, relaxed, and animated." 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 


 
 