Chinthamani Kolacase
 
{{Infobox film
| name     = Chinthamani Kolacase
| image    = Chinthamani Kolacase.jpg
| director = Shaji Kailas
| producer = M. Renjith
| screenplay = A. K. Sajan Bhavana Thilakan Biju Menon Kalabhavan Mani
| Story    = A. K. Sajan
| music    = Shaan
| cinematography    = Raja 
| released = 31 march 2006
| language = Malayalam
| budget   =  2 Crores
| gross    =  16 Crores
}} The Veteran. Bhavana playing the central character, this film narrates the story of a Lal Krishna Viradiyar, a brahmin lawyer and his style of law enforcement and criminal verdict for the criminals after the court sessions. The film was a Blockbuster at the Box-office. It also stars Thilakan, Kalabhavan Mani, Biju Menon, Rekha etc.

The style of the movie is non-linearity that is maintained throughout the entire film. The basic story-telling style of Shaji Kailas is spinned on and off by the writer A.K.Sajan.

==Plot==
The basic premise is of a lawyer doling out his brand of justice on criminals whom he himself had freed from the courts.

The story is centered on Lal Krishna Viradiar (Suresh Gopi), the enigmatic criminal lawyer with an even more enigmatic mission. Lal Krishna helps out hardened criminals to get away from the courts by telling lie that there are no proofs for their work. But later he pursues and takes them out in a bizarre show of vigilant justice.

The film begins with the acquittal of  Isra Khureshi (Baburaj (actor)|Baburaj), accused of raping and killing Rasiya, his Arabic teacher. Isra Khureshi calls up Lal Krishna Viradiyar, his lawyer, asking him to come to meet him at an isolated place for a celebration. Lal Krishna arrives at the spot and kills Khureshi, claiming that its his duty to wipe out evil forces to maintain the cosmic law. He, then kills David Rajarathnam, a Tamil businessman, who was accused of raping his own daughter. Jagannivasan (Biju Menon), the superintendent of police is suspicious of the moves of Lal Krishnan, and is following him for long time. It was then, the case of case of  Mirchi Girls, a band of spoilt, rich NRI girls, reaching him. They are the main accused in the murder of Chinthamani (Bhavana (actress)|Bhavana), their college mate, an innocent girl from conservative back ground. Viramani Varier (Thilakan), her father is fighting hard for justice and is represented by Kannayi Parameshwaran (Saikumar (Malayalam actor)|Saikumar), a public prosecutor, who is famous for his unique style of argument.  After a long court battle, Lal Krishna succeeds in bringing the judgement in favor Mirchi Girls. LK finds out that it was none other than Kannayi Parameshwaran, who has committed murder of Chinthamani, while attempting to rape her. Lal Krishna, in his own violent way kills Kannayi to deliver justice to Chinthamani and her old father Viramani Varier

==Response==

This film was released a few months after the success of Shaji Kailas - Suresh Gopi teams The Tiger. Chinthamani Kolacase went on become one of the biggest money grosser of the year 2006. This film also  reaffirmed the chair of Shaji Kailas and Suresh Gopi in Malayalam cinema.The film ran for 100 days in major centres of its release. The script of this film was done by A. K. Sajan, who had earlier done films like Butterflies, Janathipathyam, Crime File and Stop Violence. This was his first film with Shaji Kailas. Later they did Red Chillies (2009) with Mohanlal in lead and Drona 2010 (2010).

==Cast==
* Suresh Gopi .... Lal Krishna Viradiyar Bhavana .... Chinthamani
* Thilakan .... Viramani Varier
* Bheeman Reghu...david manikyam
* Baburaj as ziya khureshi
* Biju Menon .... SP Jagannivasan
* Kalabhavan Mani....CI Ayyappadas Saikumar .... Kannai Parameswaran
* Vani Viswanath .... Adv.Pattammal
* Vinayakan .... Uchandi
* Prem Prakash .... Kim Sudarsan
* Maniyanpilla Raju .... CI Bava
* Jibin Thomas(achayan)
* Poornima Anand
* Deepika Mohan

==Remakes==
It was remade in Telugu as Sri Mahalakshmi starring Srihari and remade as Ellam Avan Seyal with same director.
Newcomer RK play Lal Krishna and Shamna play Chitamani.

 
 
 
 
 