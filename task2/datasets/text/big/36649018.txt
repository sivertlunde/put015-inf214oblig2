Trade of Innocents
{{Infobox film
| name           = Trade of Innocents
| image          = 
| caption        = 
| director       = Christopher Bessette
| producer       = Bill Bolthouse  Laurie Bolthouse  Jim Schmidt
| writer         = Christopher Bessette
| starring       = Dermot Mulroney Mira Sorvino John Billingsley Trieu Tran
| music          = Timothy Hosman
| editing        = Diane Brunjes Robb
| studio         = The Bicycle Peddler LLC
| distributor    = Monterey Media  
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
}} thriller independent film written and directed by Christopher Bessette, and starring Dermot Mulroney, Mira Sorvino, John Billingsley, Trieu Tran.

Trade of Innocents was shot on location in Bangkok, Thailand. The film will look to bring awareness and involvement to work against human trafficking. 

==Plot==
In the back streets of a tourist town in present-day Southeast Asia, we find a filthy cinder block room; a bed with soiled sheets; a little girl waits for the next man to use her. Alex, a human trafficking investigator plays the role of her next customer as he negotiates with the pimp for the use of the child. Claire, Alexs wife, is caught up in the flow of her new life in Southeast Asia and her role as a volunteer in an aftercare shelter for rescued girls. She, and Alex both still are dealing with their grief of losing a child years earlier. As both of them struggle in their own way to overcome the pain of their past and realities of child exploitation where they now live and work, they find themselves being pulled together into the lives of local neighborhood girls, whose freedom and dignity are threatened.

==Cast==
* Dermot Mulroney as Alex Becker
* Mira Sorvino as Claire Becker
* John Billingsley as Malcolm Eddery
* Trieu Tran as Duke
* Rena Yamada as little girl in the start

==Production==

===Development===
The inspiration of Trade of Innocents came from a combination of the experience of the director (Christopher Bessette) and his trip to Phnom Penh, and the producers Bill and Laurie Bolthouse experience on their trip Phnom Penh. Christopher Bessette and Bill and Laurie later came together to make the film.   

Mira Sorvino has a longtime interest in supporting the cause of ending human trafficking. When asked about being in Trade of Innocents she said that "I felt it could be a powerful combination of my activist efforts and my artistic ventures." 

==Release==
In January 2011, Monterey Media acquired the United States and Canada distribution rights for the film from Bicycle Peddler LLC. 

===Festivals===
Trade of Innocents was selected to screen at the following film festivals:
*2012 Breckenridge Festival of Film 
*2012 Toronto Cornerstone Film Festival {{cite web|url=http://www.cornerstonefestival.com/tradeOfInnocents.php|title=Trade of Innocents is a fictional story
|accessdate=6 September 2012}} 

===Theatrical release===
The film will begin its theatrical released in on October 5 at the Quad Cinema in New York. 

===Critical Reception===
The film received a positive reaction from Entertainment Tonight calling it a "powerful and important new film".  Media Mikes also called the film "a rare gem that will probably not be seen by many but it really deserves mainstream shot." 

==Awards==
{| class="wikitable"
|-
! Festival
! Category
! Won
|- Toronto Cornerstone International Film Festival Best Picture Yes       
|- International Christian Visual Media Best Picture Yes     
|- Breckenridge Festival of Film Best Drama Yes     
|- Breckenridge Festival of Film Audience Award - 2nd Place Yes   
|- Breckenridge Festival of Film Best Best Director Yes 
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 