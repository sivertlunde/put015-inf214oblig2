Anbu Karangal
{{Infobox film
| name           = Anbu Karangal
| image          =
| caption        =
| director       = K. Shankar
| producer       = N. Periannana
| writer         = Bala Murugan (dialogues)
| screenplay     = K. Shankar
| story          = Bala Murugan
| starring       = Sivaji Ganesan Devika K. Balaji Nagesh
| music          = R. Sudarsanam
| cinematography = Thambu
| editing        = K. Narayanan
| studio         = Shanthi Films
| distributor    = Shanthi Films
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil language|Tamil}}
 1965 Cinema Indian Tamil Tamil film,  directed by K. Shankar and produced by N. Periannana. The film stars Sivaji Ganesan, Devika, K. Balaji and Nagesh in lead roles. The film had musical score by R. Sudarsanam.  

==Cast==
 
*Sivaji Ganesan
*Devika
*K. Balaji
*Nagesh
*V. K. Ramasamy (actor)|V. K. Ramasamy
*O. A. K. Devar
*P. D. Sambhantham
*Subbiah
*Manimala Manorama
*Seethalakshmi
*G. Sakunthala
*Lakshmi Prabha
*S. D. Subbulakshmi
*Kamakshi
*Mallika
*Baby Shakeela
*M. N. Krishnan
*S. A. Kannan
*Narayanan
*S. A. G. Samy
*Subbaraman
*Rajappa
*Sakthivel
*Prabhakaran
*Krishnamoorthy
*L. Dhanaraj
*Raju
*P. S. Rajan
*Kadhar
*Dasarathan
 

==Soundtrack==
The music was composed by R. Sudarsanam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || Vaali || 03.16
|- Susheela || Vaali || 03.54
|- Susheela || Vaali || 03.58
|- Susheela || Vaali || 03.15
|- Vaali || 04.36
|- Vaali || 04.27
|- Susheela || Vaali || 05.08
|}

==References==
 

==External links==
*  

 

 
 
 
 


 