California Solo
 

{{Infobox film
| name           = California Solo
| image          = California Solo Poster.jpg
| caption        =
| director       = Marshall Lewy
| producer       = Mynette Louie
| writer         = Marshall Lewy
| starring       = Robert Carlyle Alexia Rasmussen Kathleen Wilhoite A Martinez Michael Des Barres Danny Masterson Savannah Lathem
| music          = T. Griffin
| cinematography = James Laxton
| editing        = Alex Jablonski
| distributor    = Strand Releasing
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
}}
California Solo is an American independent feature film written and directed by Marshall Lewy and starring Robert Carlyle. It made its world premiere at the 2012 Sundance Film Festival, and its international premiere at the 2012 Edinburgh Film Festival.  The film was acquired by Strand Releasing for the U.S., and was given a limited theatrical release on November 30, 2012.

==Plot==
Robert Carlyle plays Lachlan MacAldonich, a former Britpop rocker-turned-agricultural worker, who gets caught driving drunk and faces deportation after living in Los Angeles for 12 years.  His efforts to stay in the U.S. force him to confront his past and current demons. The film addresses immigration issues, alcoholism, and personal redemption.

==Production==
Lewy wrote the part of Lachlan MacAldonich with Robert Carlyle in mind. Carlyle has remarked, "It was an easy sell to get me to do it"    and he drew inspiration for the character from his friends, the Gallagher brothers of Oasis (band)|Oasis.   
 Silver Lake, Sun Valley, Highland Park, Hollywood, and Culver City.     Its title song was written specifically for the film by Adam Franklin.   

==Reception & Awards==
"California Solo" is a New York Times Critics Pick.   
Critics have praised Robert Carlyles performance, calling it "open and utterly human",    "effortlessly engaging",    "soulful and layered",    and "stunning...perhaps his best since Trainspotting (film)|Trainspotting’s Begbie".     The Huffington Post called the film a "touching drama"    and the Hollywood Reporter called it a "fragile drama with emotional heft." 
 Best Supporting Young Actress in a Feature Film.   

==References==
 

==External links==
*  
*  
*  


 
 
 
 
 
 
 
 