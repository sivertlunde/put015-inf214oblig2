Wild Cherry (film)
{{Infobox film
| name           = Wild Cherry
| image          = Wild cherry film.jpg
| caption        = Theatrical Poster
| director       = Dana Lustig
| producer       = Kyle Bornais Michael A. DiManno Byron A. Martin R. Scott Reid Gavin Wilding
| writer         = Grant Vetters David Kolbowicz 
Chris Charney (screenplay) John White Tegan Moss Tia Carrere Rob Schneider
| music          = Chris Ainscough
| cinematography = Michael Marshall
| editing        = Michael Doherty
| studio         = Redwood Palms Pictures Farpoint Films Rampage Entertainment
| distributor    = Anchor Bay Entertainment Image Entertainment
| released       =  
| runtime        = 120 minutes
| country        = United States Canada
| language       = English
| budget         = $3.5 million
| gross          = 
}}
Wild Cherry is a 2009 American high school comedy film directed by Dana Lustig.

==Plot==
Helen McNichol (Tania Raymonde), a high school senior, is in love with Stanford Prescott (Ryan Merriman), her jock boyfriend, as well as has decided to give it all up to him. At least until she learns that her name is written in the high school football teams secret Bang Book and it is Stanfords job to deflower her.  The tables are turned and the battle begins when she and her two best friends Katelyn Chase and Trish Van Doren (Rumer Willis and Kristin Cavallari) form a pact to maintain their virginity, embarrass the team and foil the plot against them.  Helen gets Standford by ramming him off of a go-cart track and then She along with Katelyn and Trish trick him and his two best friends (Who are also Katelyn and Trishs boyfriends) into accidentally making out with each other. Then eventually at a party they slip a sex pill into the punch and gives Standford and his friends a Raging Boner. Helen then confronts Katelyn and Trish about the sex pills and blames them for everything and then visits Standford at the hospital tries to apologize and make it up to him, but he turns her down and tells her to go away. At the football game Helen apologizes to Katelyn and Trish and they forgive her, however, Standfords team is losing the game. During Halftime Helen takes the microphone and apologizes for everything and helps Standford and his team into realizing why they are losing and Standford forgives her. When the next match comes, Standford and his team follow Helens advice and beat the other team. Everyone then goes to the victory party where Helen and Standford finally have Sex. The film ends with Helen coming home after the party and sees her father Nathan (Rob Schneider) sitting on a toilet playing a trombone through the window in which she smiles at.

==Cast==
* Tania Raymonde as Helen McNichol
* Rumer Willis as Katelyn Chase
* Kristin Cavallari as Trish Van Doren
* Ryan Merriman as Stanford Prescott
* Jesse Moss as Brad Skeets Skeetowski John White as Franklin Peters
* Rob Schneider as Nathan McNichol
* Tia Carrere as Ms. Haumea
* Tegan Moss as Hannah	
* Elle King as Sabrina

==Production==
Film production took place in Winnipeg, Manitoba from May 7, 2008 to May 29, 2008. School scenes were filmed at Technical Vocational High School.

==References==
 

==External links==
*  
*   of Wild Cherry

 
 
 
 
 
 
 