Outlaw (1999 film)
{{ Infobox film
| name           = Ormai è fatta!
| image          = Outlaw (1999 film).jpg
| caption        = 
| director       = Enzo Monteleone
| producer       = Hera International Film Radiotelevisione Italiana
| writer         = Horst Fantazzini Angelo Orlando Enzo Monteleone
| starring       = Stefano Accorsi Giovanni Esposito
| music          =  Pivio and Aldo De Scalzi
| cinematography     =   Arnaldo Catinari
| distributor    =
| released       =  
| runtime        = 98 minutes
| country        = Italy
| language       = Italian
}}

Outlaw ( ) is a 1999 Italian drama film directed by Enzo Monteleone. It is based on the book by the Italian anarchist Horst Fantazzini. It was entered into the 21st Moscow International Film Festival.   

==Cast==
* Stefano Accorsi as Horst Fantazzini
* Giovanni Esposito as Di Gennaro
* Emilio Solfrizzi as Loiacono
* Antonio Catania as Sostituto Procuratore
* Antonio Petrocelli as Ridolfi
*Fabrizia Sacchi as Anna, moglie di Fantazzini
*Paolo Graziosi as  Tagliaferri,  Carabinieri Colonel
*Alessandro Haber as  Fantazzinis Lawyer
*Francesco Guccini as  Alfonso Fantazzini

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 