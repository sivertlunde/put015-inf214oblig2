Nazhikakkallu
{{Infobox film 
| name           = Nazhikakkallu
| image          =
| caption        =
| director       = Sudin Menon
| producer       = Vasudevan Nair
| writer         = Sudin Menon Sreekumaran Thampi (dialogues)
| screenplay     = Sudin Menon
| starring       = Prem Nazir Sheela Ramachandran Sankaradi
| music          = Kanu Ghosh
| cinematography = Vasudevan Nair
| editing        = Devadas
| studio         = VS Pictures
| distributor    = VS Pictures
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed by Sudin Menon and produced by Vasudevan Nair. The film stars Prem Nazir, Sheela, Ramachandran and Sankaradi in lead roles. The film had musical score by Kanu Ghosh.   

==Cast==
 
*Prem Nazir
*Sheela
*Ramachandran
*Sankaradi
*T. R. Omana
*Bahadoor GK Pillai
*Kaduvakulam Antony
*ML Saraswathi
*Madhubala
*Nellikode Bhaskaran
*Omana
*PR Menon
*S. P. Pillai
*Shyam Kumar
*Sreekumar
*Treesa
*Usha
*Usharani Vincent
 

==Soundtrack==
The music was composed by Kanu Ghosh and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chandanathottililla || S Janaki || Sreekumaran Thampi || 
|-
| 2 || Chempavizhachundil || P Jayachandran || Sreekumaran Thampi || 
|-
| 3 || Ee maruboovil (Chandanathottililla   ) || S Janaki || Sreekumaran Thampi || 
|-
| 4 || Etho Raavil || S Janaki || Sreekumaran Thampi || 
|-
| 5 || Kaneerilalle Jananam || Kamukara || Sreekumaran Thampi || 
|-
| 6 || Kanneerilalle Jananam   || Kamukara || Sreekumaran Thampi || 
|-
| 7 || Nin Padangalil || P Jayachandran, T. R. Omana || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 


 