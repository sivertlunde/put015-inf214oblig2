Congo Maisie
{{Infobox film
| name           = Congo Maisie
| image	         = Congo Maisie FilmPoster.jpeg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = H. C. Potter
| producer       = J. Walter Ruben
| writer         = Mary C. McCall Jr.
| based on       =   John Carroll Edward Ward
| cinematography = Charles Lawton Jr.
| editing        = Fredrick Y. Smith
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Congo Maisie is a 1940 comedy-drama film directed by H. C. Potter and starring Ann Sothern for the second time in the ten film Maisie series as showgirl Maisie Ravier.

==Plot==
 
Maisie (Ann Sothern) hides aboard a West African steamer after she discovers that she cannot pay her hotel tab. She winds up in a hospital upon a rubber plantation, which she must save from a native attack.

==Cast==
* Ann Sothern as Maisie Ravier John Carroll as Dr. Michael Shane
* Rita Johnson as Kay McWade
* Shepperd Strudwick as Dr. John Jock McWade
* J.M. Kerrigan as Captain Finch
* E.E. Clive as Horace Snell
* Everett Brown as Jallah
* Tom Fadden as Nelson
* Lionel Pape as British Consul
* Nathan Curry as Luemba
* Leonard Mudie as Farley
* Martin Wilkins as Zia
* Ernest Whitman as Varnai
* William Broadus as Third Witch Doctor (uncredited)
* Buddy Harris as Second Witch Doctor (uncredited)
* Darby Jones as First Witch Doctor (uncredited)

==References==
 	

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 

 