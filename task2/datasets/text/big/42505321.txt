The Reincarnation of Golden Lotus
 
 
{{Infobox film name       = The Reincarnation of Golden Lotus image      =  caption    =  traditional = 潘金蓮之前世今生 simplified = 潘金莲之前世今生 pinyin     = Pān Jīnlián zhī Qiánshìjīnshēng }} director   = Clara Law producer   = Teddy Robin
| writer    = Lilian Lee
| based on  =  starring   = Joey Wong Eric Tsang Wilson Lam Pal Sinn Ku Feng music      = Wei Peng Lu Shijie cinematography = Jingle Ma editing    = Jin Ma studio     = Youhe Film Production co., LTD Jiafeng Film co., LTD.
|distributor= Orange Sky Golden Harvest released =   runtime =  99 minutes country = Hong Kong language = Cantonese   Mandarin budget   =  gross    = HK $8,160,911.00
}}
 sex film directed by Clara Law and produced by Teddy Robin, and written by Lilian Lee. The film stars Joey Wong, Eric Tsang, Wilson Lam, Pal Sinn, and Ku Feng.   The film premiered in Taiwan on 4 August 1989.

==Plot==
During Song Dynasty, Pan Jinlian was beheaded by the warder, she is reborn into the body of a baby girl named Shan Yulian, in Shanghai, after the Chinese Communist Revolution.

The war orphaned Shan Yulian at an early age. She graduated from Shanghai Arts School, majoring in Ballet.
 CPC Government, she was raped by the President of Shanghai Dance Troupe. During the Down to the Countryside Movement, Shan Yulian was sent to the May Seventh Cadre Schools to work, she married a stupid farmer Wu Da, but she falls in love with Wu Das brother, Wu Long, at the same time, Simon, who is a local playboy start to pursue her, and she falls into a love triangle with Wu Long and Simon.

==Cast==
* Joey Wong as Pan Jinlian/ Shan Yulian.  
* Eric Tsang as Wu Da.
* Wilson Lam as Wu Long.
* Pal Sinn as Simon.
* Ku Feng

==Release==
The film was first released in Taiwan on 4 August 1989, and it was given a wider release on 16 February 1990.

The film was screened at the Toronto Film Festival.

The film grossed $8,160,911.00 million.

==Award==
{| class="wikitable"
|-
! Year !! Work  !! Award  !! Result !! Notes
|-
| 1990   || The Reincarnation of Golden Lotus  || Hong Kong Film Award for Best New Performer - Pal Sinn  ||     ||  
|}

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 