Out of the Past
 
{{Infobox film
| name           = Out of the Past
| image          = Outofthepast.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jacques Tourneur
| producer       = Warren Duff
| screenplay     =  Daniel Mainwaring
| based on       =  
| starring       =  
| music          = Roy Webb
| cinematography = Nicholas Musuraca
| editing        = Samuel E. Beetley
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
|}} Frank Fenton and James M. Cain, from his novel Build My Gallows High (also written as Homes).
 dark cinematography Cat People. In 1991, Out of the Past was added to the United States National Film Registry by the Library of Congress as being deemed "culturally, historically, or aesthetically significant." 

==Plot== Richard Webb), a local police officer who is a long-time admirer of Ann.
 Steve Brodie) New York. Jeff had been hired by Whit to find his girlfriend, Kathie Moffat (Jane Greer). Whit claimed she shot him and stole $40,000. He assured Jeff he just wants her back, and will not harm her.

  as Jeff Bailey and Jane Greer as Kathie Moffat]] love affair soon develops, and Jeff ultimately tells her that he has been sent by Whit to find her. Kathie denies taking Whit’s money and pleads with Jeff to run away with her.

 

Preparing to leave with her the next morning, Jeff is surprised by the arrival of Whit and Stefanos, who are checking up on his lack of progress. He asks Whit to take him off the case, but Whit refuses. Jeff then tells Whit that Kathie slipped past him and is on a steamer going south. Whit instructs Jeff to track her down. Instead Jeff takes Kathie north to San Francisco.

They live as inconspicuously as possible. Over time, they relax, but an outing to the race track goes bad when they are spotted by Jeff’s old partner.  Jeff and Kathie split up, with Jeff trying to throw Fisher off their trail.  Jeff rejoins Kathie at a rural cabin, only to find Fisher already there.  Fisher demands money to keep quiet. The two men start fighting. Then Kathie shoots Fisher, then drives away. Jeff finds Kathie’s bankbook; its shows a deposit of $40,000.

Returning to the present, Ann drops Jeff off at Whits estate. Surprisingly, Whit seems genuinely glad to see him. Kathie is there as well. She had gone back to Whit. Whit tells Jeff he wants to hire him for a new job. He says it is the only way to make things right between them. Leonard Eels (Ken Niles), Whits lawyer, has been blackmailing him. Whit wants to recover incriminating income tax records. Jeff tries to turn down the job, but Whit insists he take it. Sensing a trap, Jeff tries to warn Eels when he first meets him, but when he returns later, he finds Eels dead. He hides the body.
 Dickie Moore), Jeffs deaf young assistant, so he can kill Jeff. The Kid drives to a fishing spot near Jeffs hideout and prepares to cast his line. Above him, Stefanos takes aim at Jeff.  The Kid sees it and hooks Stefanos with his fishing line, causing Stefanos to lose his balance and fall to his death.

Jeff goes back to Lake Tahoe and reveals to Whit Kathies double cross. Whit is convinced he must turn Kathie over to the police for Fisher’s murder. However, when Jeff returns to Tahoe, he discovers that Kathie has killed Whit.  She gives Jeff the choice of running away with her or taking the blame for all three murders.  He agrees to go with her, but secretly makes a phone call. The police are waiting for them at a roadblock. Realizing Jeff has betrayed her, Kathie shoots him. The police begin firing. The car careens off the road and crashes. Inside the wreck, the police find the bodies of Kathie and Jeff.

After Jeffs funeral, Ann asks the Kid if Jeff had been planning to run away with Kathie. The Kid nods his head. As Ann drives off with Jim, the Kid looks up at the gas station sign with Jeffs name on it, smiles and nods.

==Cast==
* Robert Mitchum as Jeff Bailey
* Jane Greer as Kathie Moffat
* Kirk Douglas as Whit Sterling
* Rhonda Fleming as Meta Carson Richard Webb as Jim Steve Brodie as Jack Fisher
* Virginia Huston as Ann Miller
* Paul Valentine as Joe Stefanos Dickie Moore as The Kid
* Ken Niles as Leonard Eels

==Background and production==
Out of the Past was produced by RKO Pictures, and the key personnel — director Jacques Tourneur, cinematographer Nicholas Musuraca, actors Mitchum and Greer, along with Albert S. DAgostinos design group — were long-time RKO collaborators. Although the studio had focused on making the more lucrative B-films during the early 1940s,   Out of the Past was given an A-budget.
 Western The The Way West alongside Richard Widmark two decades later.

==Reception==

===Critical response===
Out of the Past is considered one of the greatest of all films noir.    Robert Ottoson hailed the film as "the ne plus ultra of forties film noir". 

Film critic Bosley Crowther wrote, "However, as we say, its very snappy and quite intriguingly played by a cast that has been well and smartly directed by Jacques Tourneur. Robert Mitchum is magnificently cheeky and self-assured as the tangled private eye, consuming an astronomical number of cigarettes in displaying his nonchalance. And Jane Greer is very sleek as his Delilah, Kirk Douglas is crisp as a big crook and Richard Webb, Virginia Huston, Rhonda Fleming and Dickie Moore are picturesque in other roles. If only we had some way of knowing whats going on in the last half of this film, we might get more pleasure from it. As it is, the challenge is worth a try." 

The staff at Variety (magazine)|Variety wrote, "Out of the Past is a hardboiled melodrama   strong on characterization. Direction by Jacques Tourneur pays close attention to mood development, achieving realistic flavor that is further emphasized by real life settings and topnotch lensing by Nicholas Musuraca...Mitchum gives a very strong account of himself. Jane Greer as the baby-faced, charming killer is another lending potent interest. Kirk Douglas, the gangster, is believable and Paul Valentine makes role of henchman stand out. Rhonda Fleming is in briefly but effectively." 

In a 2004 review of the film, critic Roger Ebert wrote "Out of the Past is one of the greatest of all film noirs, the story of a man who tries to break with his past and his weakness and start over again in a town, with a new job and a new girl. The film stars Robert Mitchum, whose weary eyes and laconic voice, whose very presence as a violent man wrapped in indifference, made him an archetypal noir actor. The story opens before weve even seen him, as trouble comes to town looking for him. A man from his past has seen him pumping gas, and now his old life reaches out and pulls him back."  Ebert also called it, "The greatest cigarette-smoking movie of all time." Ebert, Roger -   Chicago Sun, February 26, 1999. This review also later appeared in the book by Roger Ebert, "I Hated Hated Hated HATED this movie"   "The trick, as demonstrated by Jacques Tourneur and his cameraman, Nicholas Musuraca, is to throw a lot of light into the empty space where the characters are going to exhale. When they do, they produce great white clouds of smoke, which express their moods, their personalities and their energy levels. There were guns in Out of the Past, but the real hostility came when Robert Mitchum and Kirk Douglas smoked at each other." 

===American Film Institute Lists===
*AFIs 100 Years...100 Movies - Nominated 
*AFIs 100 Years...100 Thrills - Nominated 
*AFIs 100 Years...100 Passions - Nominated 
*AFIs 100 Years...100 Heroes and Villains:
**Whit Sterling - Nominated Villain 
*AFIs 100 Years...100 Movie Quotes:
**"You know, maybe I was wrong and luck is like love. You have to go all the way to find it." - Nominated   
**KATHIE MOFFAT: "I think we deserve a break." JEFF BAILEY: "We deserve each other." 
*AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated 

==Adaptations== Against All Odds (1984) with Rachel Ward in the Greer role, Jeff Bridges filling in for Mitchum, and James Woods as a variation of Kirk Douglas villain, with Jane Greer as the mother of her original character in Out of the Past and Richard Widmark in a supporting role.

==References==
;Notes
{{reflist|2|refs=
   
   
   
   
   
  , table 6.3. 
   
   
   
}}

;Bibliography
 
 
 

==External links==
 
 
*  
*  
*  
*   AMC Filmsite.org
*   essay by Michael Mills at Moderntimes.com
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 