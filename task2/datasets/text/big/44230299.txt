Edge of Fury
{{Infobox film
| name           = Edge of Fury
| image          = Edge of Fury poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert J. Gurney Jr. Irving Lerner
| producer       = Robert J. Gurney Jr. 
| screenplay     = Robert J. Gurney Jr.
| story          = Ted Berkman Raphael Blau
| based on       =   Michael Higgins Lois Holmes Jean Allison Doris Fesette Malcolm Lee Beggs
| music          = Robert Sydney
| cinematography = Jack Couffer Conrad Hall Marvin R. Weinstein
| editing        = Sidney Meyers
| studio         = Wisteria Productions
| distributor    = United Artists
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Michael Higgins, Lois Holmes, Jean Allison, Doris Fesette and Malcolm Lee Beggs. The film was released on May 1, 1958, by United Artists.    

==Plot==
The film was summarized in TV Guide as "Sick low-budget grossness about a psychopathic young beachcomber, Higgins, who pretends to befriend a mother and two daughters living at their summer home. He talks the family into letting him rent the adjacent guest cottage, then he slaughters the whole family." 

== Cast ==	 Michael Higgins as Richard Barrie
*Lois Holmes as Florence Hackett
*Jean Allison as Eleanor Hackett
*Doris Fesette as Louisa Hackett
*Malcolm Lee Beggs
*Craig Kelly	 John Harvey 	
*Beatrice Furdeaux
*Mary Boylan

==Production==
Edge of Fury was one of Conrad Halls first films as a principal cinematographer. In 2003, he was included in a listing of historys ten most influential cinematographers by the International Cinematographers Guild. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 