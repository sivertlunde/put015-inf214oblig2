On the Run (1982 film)
 
{{Infobox film
| name           = On the Run
| image          =
| caption        =
| director       = Mende Brown
| producer       = Mende Brown
| writer         = Michael Fisher
| narrator       =
| starring       = Rod Taylor Paul Winfield Beau Cox Ray Meagher
| music          = 
| cinematography = Paul Onorato
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Australian
| language       = English
| budget         =
| gross          =
}}
On the Run is a 1982 Australian thriller film.

==Plot==
A small boy (Beau Cox) is orphaned and sent to live with his uncle (Rod Taylor), who is a hitman. When the boy witnesses his uncle kill some people, his uncle orders that his assistant (Paul Winfield) help shoot the boy; the assistant refuses and takes off with the boy.

==Production==
The movie marked the first time Rod Taylor had played an Australian character in an Australian film. 

==Release==
The film was not released theatrically. 

==References==
 

==External links==
*  at IMDB
*  at National Film and Sound Archive

 

 