Crosstrap
{{Infobox film
| name           = Crosstrap
| image          =
| image_size     =
| caption        =
| director       = Robert Hartford-Davis
| producer       = Michael Deeley George Mills
| writer         = Philip Wrestler John Newton Chance
| starring       = Laurence Payne Jill Adams Gary Cockrell
| music          = Steve Race Eric Cross
| editing        = Harry Booth
| distributor    = Unifilms Ltd.
| released       = January 1962
| runtime        = 62 minutes
| country        = United Kingdom
| language       = English
}}

Crosstrap is a 1962 British B-movie crime film starring Laurence Payne, Jill Adams and Gary Cockrell and marking the directorial debut of Robert Hartford-Davis.  The screenplay was adapted from a novel by John Newton Chance and the film was reportedly unusually graphic for its time in its depiction of on-screen violence, with one reviewer describing a "climactic blood-bath where corpses bite the dust as freely as Indians in a John Ford western".   BFI National Archive. Retrieved 09-09-2010 

==Plot==
Novelist Geoff (Cockrell) and his wife Sally (Adams) rent an isolated countryside bungalow to enable Geoff to finish his latest book without the distractions of life in London.  On their arrival, they are horrified to find a dead man in the property and before they can report the discovery they are confronted by Duke (Payne), a gangland boss, and his henchmen who have, it transpires, been using the empty property as a hide-out for stolen valuables which they are planning to smuggle out of the country.  A rival gangster, Juan (Derek Sydney), also has his eye on the goods and has discovered their whereabouts.  The dead man is one of his minions.

Geoff and Sally find themselves being held captive, and matters take a turn for the worse when Juan and his men also arrive on the scene, forcing a stand-off between the two factions during which Geoff and Sally are roughly-treated by both sides.  Duke starts to fall for Sally, and his obvious interest in her antagonises his girlfriend Rina (Zena Marshall).  Eventually there is a bloody shoot-out between the rival gangs, with Dukes men getting the better of the exchange.  Duke boards a plane to make good his escape with the valuables, but the plane is shot down by the jealous and vengeful Rina.

==Cast==
* Laurence Payne as Duke
* Jill Adams as Sally
* Gary Cockrell as Geoff
* Zena Marshall as Rina Bill Nagy as Gaunt
* Robert Cawdron as Joe Larry Taylor as Peron Max Faulkner as Ricky
* Derek Sydney as Juan Michael Turner as Hoagy

==Reception==
Crosstrap appears to have received a mainly negative critical reception, with verdicts such as "overacted, ludicrous and amateurish" (Monthly Film Bulletin) and "brawny but brainless" (Kine Weekly).  The Daily Cinema was less dismissive, labelling it an "incredible but lively tale of gang-warfare, packed with hearty action and intrigue, plus a spot of sex for flavour" offering "robust...programme support". 

==Later history== Night of stills or other publicity material.
 The Sandwich 75 Most Wanted" list of missing British feature films. 

==See also==
*List of lost films

==References==
 

== External links ==
*  
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 
 
 