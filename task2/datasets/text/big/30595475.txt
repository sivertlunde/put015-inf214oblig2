The Good Soldier Schweik (1931 film)
 
{{Infobox film
| name           = The Good Soldier Schweik
| image          = 
| caption        = 
| director       = Martin Frič
| producer       = 
| writer         = Václav Wasserman Jaroslav Hasek
| starring       = Saša Rašilov
| music          = 
| cinematography = Jan Stallich
| editing        = Martin Frič
| distributor    = 
| released       =  
| runtime        = 57 minutes
| country        = Czechoslovakia
| language       = Czech
}}
 Czechoslovakian black-and-white comedy film directed by Martin Frič,    based on Jaroslav Hašeks novel The Good Soldier Švejk.

==Cast==
* Saša Rašilov as Josef Svejk Oscar Marion as Lieutenant Karel Lukás (as Oskar Marion)
* Jan Richter as Palivec, innkeeper
* Hugo Haas as MUDr. Katz
* Antonie Nedošinská as Mrs. Müllerová, landlady
* Josef Rovenský as MUDr. Grünstein
* Jaroslav Marvan as Plukovník Kraus
* Jarmila Vacková as Irena Krausová
* Alexander Trebovský as Bretschneider, secret agent
* Milka Balek-Brodská as Countess von Botzenheim
* Eduard Slégl as Chamberlain
* Felix Kühne as Doctor

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 