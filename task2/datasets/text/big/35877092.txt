Tomboys (film)
{{multiple issues|
 
 
}}

 

{{Infobox film
| name           = Tomboys
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Nathan Hill
| producer       = Nathan Hill
| writer         = 
| screenplay     = Fabian Lapham Stuart Van Eysden Nathan Hill
| story          = 
| based on       =  
| narrator       = 
| starring       = Candice Day Naomi Davis
| music          = Jamie Murgatroyd Asher Pope
| cinematography = Stuart Van Eysden
| editing        = Thomas Anderson Stuart Van Eysden
| studio         = 
| distributor    = 
| released       =  
| runtime        = 79 min
| country        = USA Australia
| language       = English
| budget         = 
| gross          = 
}}

Tomboys is a 2009 Australian horror film, directed by Nathan Hill. The film follows five women who brutally attack a man who raped them.

==Plot==
 
Kat has been the victim of repeated sexual assault and has had enough. Along with her group of mates, she has decided that since the law wont hand out any justice, then its time to take matters into her own hands and distribute some payback on her own terms. It helps that one of her friends has a disused barn on her isolate property that should afford some privacy from prying eyes.

A party provides the opportunity for Kat and her friends to kidnap the intended target, who is a serial rapist, and immobilise him in the barn soon to drip blood.

==Cast==
*Kat – Candice Day 
*Naomi – Naomi Davis
*Emily – Sash Milne 
*Imogen – Allie Hall
*Crystal – Sarah Hill
*Kyle – Daniel Rankin

==Reviews==
 
The film received various reviews.

==External links==
 

==References==
 

 
 
 
 


 
 