Tank Commandos
{{Infobox film
| name           = Tank Commandos
| image          = Tankcompos.jpg
| image_size     =
| caption        = Film poster by Albert Kallis
| director       = Burt Topper
| producer       = Burt Topper
| writer         = Burt Topper
| based on       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = American International Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Tank Commandos is a 1959 war film produced, directed and written by Burt Topper. New War Bill Teamed on Many Local Screens
Stinson, Charles. Los Angeles Times (1923-Current File)   27 Feb 1959: B7. American International Pictures released the film as a double feature with Operation Dames.


==Plot==
During the Italian campaign the American army sends out a demolition team to discover how the Germans are reinforcing their positions.  The team is led to its destination by an Italian boy.

==Cast==
Donato Farretta  ...  Diano    
Robert Barron  ...  Lt. Jim Blaine    
Maggie Lawrence  ...  Jean, the nurse    
Wally Campo  ...  Pvt. Sonny Lazzotti    
Jack B. Sowards  ...  Pvt. Todd    
Leo V. Matranga  ...  Shorty   
Anthony Rich  ...  Sands     
Larry Hudson  ...  Capt. Praxton    
Maria Monay  ...  Italian Woman  


==References==
 
==External links==
* 

 

 

 
 
 
 
 
 
 