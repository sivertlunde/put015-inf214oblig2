One Good Turn (1931 film)
 
 
{{Infobox film
| name = One Good Turn
| image = L&H One Good Turn 1931.jpg
| caption = Theatrical release poster
| director =  James W. Horne
| producer = Hal Roach
| writer = H.M. Walker
| starring = Stan Laurel Oliver Hardy
| music = Marvin Hatley Leroy Shield
| cinematography = Art Lloyd
| editing = Richard C. Currier
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 20 35"
| country = United States
| language = English
}}
One Good Turn is a 1931 short comedy film starring Laurel and Hardy.

== Plot ==
Stan and Ollie are victims of the Great depression and are begging for food. A friendly old lady provides them with some sandwiches. Enjoying their meal they hear that the old lady will be thrown out of her house because she is robbed and cannot pay her Mortgage loan|mortgage. They dont know that the old lady is rehearsing a play. Stan and Ollie decide to repay the old lady by selling their car. During the auction a drunken man (Billy Gilbert) puts a wallet in Stans pocket. Ollie accuses Stan of having robbed the old lady but when they return to the old ladys place they hear the truth. Stan takes revenge on Ollie in a violent way.

== Cast ==
*Stan Laurel as Stanley
*Oliver Hardy as Ollie
*Mary Carr as the old lady
*Jimmy Finlayson as A Community Player who plays the landlord
*Billy Gilbert as the Drunk Gordon Douglas as A Community player
*Dorothy Granger as A Community Player
*Snub Pollard as A Community Player
*Lyle Tayo as A Community Player

== Notes ==
Stan Laurels daughter Lois was fearful of Oliver Hardy (known to her as "Uncle Babe") when her father was hit by Hardy in many Laurel and Hardy films. So, Laurel wrote a scene in which Hardy was hit by him.

==References==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 

 