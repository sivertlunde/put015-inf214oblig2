Thanks for the Memory (film)
{{Infobox film
| name           = Thanks for the Memory
| image          = File:Thanks_for_the_Memory_lobby_card_(house_husband_version).jpg
| caption        = Lobby card featuring stars Bob Hope and Shirley Ross in a posed production still
| director       = George Archainbaud
| producer       = Mel Shauer
| writer         = Lynn Starling Frances Goodrich (play) Albert Hackett (play)
| narrator       = Charles Butterworth Otto Kruger Hedda Hopper Charles Bradshaw (uncredited)
| cinematography = Karl Struss
| editing        = Alma Macrorie
| distributor    = Paramount Pictures
| released       =  
| runtime        = 75 min.
| country        = United States
| language       = English
| budget         =
}}

Thanks for the Memory is a 1938 film starring Bob Hope and Shirley Ross, and directed by George Archainbaud.

==History== previously filmed by Paramount in 1931, with Norman Foster and Carol Lombard) dealt with an out of work writer who stays home and plays house husband while his wife goes to work for her former fiancé. 

==Cast==
*Bob Hope as Steve Merrick
*Shirley Ross as Anne Merrick Charles Butterworth as Biney
*Otto Kruger as Gil Morrell
*Hedda Hopper as Polly Griscom
*Laura Hope Crews as Mrs. Kent
*Emma Dunn as Mrs. Platt
*Roscoe Karns as George Kent Eddie Rochester Anderson as Janitor
*Edward Gargan as Flanahan
*Jack Norton as Bert Monroe
*Patricia Wilder as Luella
*William Collier, Sr. as Mr. Platt

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 


 