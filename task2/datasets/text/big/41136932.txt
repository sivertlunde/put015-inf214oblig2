The Black Decameron
:Not to be confused with "El Decameron Negro", a classical guitar suite composed by Leo Brouwer.

{{Infobox film
 | name =The Black Decameron
 | image = Decamerone_nero.jpg
 | image_size =
 | caption =
 | director = Piero Vivarelli
 | producer =Alfredo Bini
 | writer = Ottavio Alesi Piero Vivarelli
 | based on =  
 | starring = Beryl Cunningham (La regina bella) Djibril Diop Mambéty (Che cosa non ha fatto)
 | music = Luciano Michelini
 | cinematography = Roberto Gerardi
 | editing =
 | studio=
 | distributor = 
 | released = 15 October 1972 	
 | runtime = 98 min 
 | country = Italy
 | language = Italian
 | budget =
 }} Italian costume drama comedy film directed by Piero Vivarelli. 

==Background==
An adaptation of five stories from the anthology Der schwarze Dekameron: Belege und Aktenstücke über Liebe, Witz und Heldentum in Innerafrika (1910) by ethnologist Leo Frobenius with a short final vignette, The Black Decameron is essentially a cross between Decamerotici films of the early commedia sexy allitaliana genre and Mondo films in featuring gratuitous pseudo-ethnographic nudity.
 Haitian actor couple Jacqueline Scott and Lucien Lemoine. 

==Synopsis== korafola Bana Cissokho.

La regina bella (The Beautiful Queen): A queen is single and decides to marry and she puts her suitors on harsh trials of strength and courage. Knowing that all former candidates had perished this way, Nsani plans to win the queens hand by cunning and trickery.

Guarigione di una pazza per gli uomini (Healing of a Woman Crazy for Men): A man is married to a nymphomaniac and asks his friends help to solve this problem.

Gli amanti puniti (The Punished Lovers): A fisherman suspects that his wife Adu is cheating on him and pretends to be blind to learn the truth.

Vendetta di prostituta (Vengeance of the Prostitute): Elders in a village punish men who sleep with prostitute Mande and she sets a trap to the elders in order to expose their hypocrisy.
 toddy and wears womens clothes. He soon contemplates that his drag may also bring some advantages and arrives at the house of a village judge to be his "new wife" but ends up sleeping with all women of the household.

==Cast==
*Beryl Cunningham: the queen
*Serigne Ndiaye Gonzales: Nsani
*Jacqueline Scott: the dignitary of the queen 
*Josy McGregory: Adu
*Line Senghor: Mande
*Djibril Diop Mambéty: Simoa
*Lucien Lemoine: the judge
*Isseu Niang (credited as Issa Niang): the judges wife
*Isabelle Diallo: Antha, a daughter of the judge
*Bintha Gassama: the girl in the final vignette

==References==
 
==External links==
* 
 
 
 
 
 
 
 
  
 
 