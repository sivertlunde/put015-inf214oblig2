Friend Zone (film)
{{Infobox film
| name           = Friend Zone
| image          = 
| alt            =
| caption        = 
| film name      = Pagafantas
| director       = Borja Cobeaga
| producer       = 
| writer         = Borja Cobeaga Diego San José
| screenplay     = 
| story          = 
| based on       = 
| starring       = Gorka Otxoa Sabrina Garciarena Óscar Ladoire Michel Brown
| narrator       = 
| music          = Aránzazu Calleja Manos de Topo Antonna Los Punsetes
| cinematography = Alfonso Postigo
| editing        = 
| production companies = Antena 3 Films Manga Films
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Spain
| language       = Spanish
| budget         = € 2.5 million 
| gross          = 
}}

Friend Zone ( , roughly "Drinkbuyer") is a 2009 Spanish film directed by Borja Cobeaga and starring 
Gorka Otxoa and Sabrina Garciarena.

==Plot==
Chema (Gorka Otxoa) has dumped his longtime girlfriend because he thinks he can do better, but so far hes not being quite successful when trying to date new girls. So when he meets Claudia (Sabrina Garciarena) he thinks his luck has changed. Shes pretty, funny, and seems interested in him. This apparent luck runs out when Sebastián (Michel Brown), Claudias actual boyfriend, comes over from Argentina and it becomes apparent that Claudia only wants Chema as her friend, so hes faced with the choice of ditching a girl he doesnt stand a chance of getting, or waiting for the opportunity to present itself. 

==Cast==
*Gorka Otxoa as Chema
*Sabrina Garciarena as Claudia
*Óscar Ladoire as Jaime
*Michel Brown as Sebastián
*María Asquerino as Begoña
*Kiti Manver as Gloria
*Julián López as Rubén
*Ernesto Sevilla as Chemas cousin
*Bárbara Santa-Cruz as Elisa
*Mauro Muñiz as Iñaki

==Awards and nominations==

===24th Goya Awards===
{| class="wikitable"
! Category !! Recipient !! Result
|- Goya Award Best New Actor Gorka Otxoa
| 
|- Goya Award Best New Director Borja Cobeaga
| 
|}

==References==
 

 

 