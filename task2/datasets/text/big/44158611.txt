Daughter (2014 film)
{{Infobox film
| name           = Daughter
| image          = 
| director       = Ku Hye-sun
| producer       = Choi Seo-young   Son Gui-yong
| writer         = Ku Hye-sun
| starring       = Ku Hye-sun   Shim Hye-jin
| cinematography = Yoon Ju-hwan
| music          = Choi In-young
| editing        = Hyun Jung-hoon
| distributor    = M-Line Distribution
| studio         = Yes Production
| country        = South Korea
| language       = Korean
| runtime        = 84 minutes
| budget         = 
| released       =  
| gross          = 
}}
Daughter ( ) is a 2014 South Korean drama film directed, written, and starring Ku Hye-sun.     

Daughter premiered at the 19th Busan International Film Festival ahead of its theatrical release on November 6, 2014.  

==Plot==
San experienced a grim childhood and adolescence with her emotionally, verbally and physically abusive mother. Unrelentingly critical and demanding, Sans mother was particularly obsessed with preventing her teenage daughter from taking full possession of her sexuality. Sans only reprieve was her piano lessons with a kindly next-door neighbor, Jeong-hee. Years later, when the adult San learns that shes pregnant, she visits her estranged, terminally ill mother at the hospital. Despite awaiting her imminent death, her mother has not changed at all, and San wonders if she will ever be free of the painful memories.

==Cast==
* Ku Hye-sun as San
* Shim Hye-jin as Mom
* Hyun Seung-min as young San
* Yoon Da-kyung as Jeong-hee
* Lee Hae-woo as Jin-woo
* Yang Hyun-mo

==References==
 

== External links ==
*  
*  

 
 
 
 

 