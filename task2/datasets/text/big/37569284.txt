Fukrey
 
 
{{Infobox film
| name           = Fukrey
| image          = Fukrey film poster.jpg
| caption        = film poster
| director       = Mrighdeep Singh Lamba
| producer       = Farhan Akhtar Ritesh Sidhwani
| screenplay     = Vipul Vig Mrighdeep Singh Lamba
| story          = Vipul Vig
| starring       = Pulkit Samrat Ali Fazal Manjot Singh Priya Anand Richa Chadda Vishakha Singh Varun Sharma
| music          = Ram Sampath
| cinematography = K.U. Mohanan
| editing        = Anand Subaya
| studio         = Excel Entertainment
| distributor    =
| released       =  
| runtime        = 139 minutes  
| country        = India
| language       = Hindi
| budget         = 
| gross          =   (Lifetime domestic nett.) http://www.boxofficeindia.com/boxdetail.php?page=shownews&articleid=5854&nCat= 
}} coming of age comedy film directed by Mrighdeep Singh Lamba and starring Pulkit Samrat, Varun Sharma, Ali Fazal, Manjot Singh, Richa Chadda, Vishakha Singh, and Priya Anand in the lead roles.   The story and dialogues are written by Vipul Vig and the screenplay was written by Vig and Lamba. Produced under the Excel Entertainment banner, the film was released on 14 June 2013.  Despite a poor opening, the film has gone on to become a sleeper hit.  

==Plot== Punjabi term for slackers) boys,  come together to make easy money.

The group includes two school backbenchers, Hunny (Pulkit Samrat) and Choocha (Varun Sharma), who are perpetually dreaming of making it big. They want to get into the local college but dont have the grades. The college guard Pandit (Pankaj Tripathi) tells them that he can leak to them the class XII papers at a rate of   50,000 per subject. Through Pandit (Pankaj Tripathi) they meet Zafar (Ali Fazal), a former student of the college and a struggling musician. The fourth character is Lali (Manjot Singh), whose father runs a sweet shop. Lali is currently pursuing his degree through correspondence and like Hunny and Choocha also wants to get into the same college but doesnt have the marks. Pandit tells Lali to donate   250,000 in the college development fund to get a seat in return. Zafars girlfriend, Neetu (Vishakha Singh), has broken up with him and his father is critically ill and needs money for his treatment.

The four meet up at Pandits office where Hunny explains his scheme. Each night Choocha has a dream, which Hunny later interprets as a lottery number, which they buy and win. And for every rupee invested, they get ten rupees (basically ten times) back. However since now they all together need a lot of money, they need someone to invest lakhs. So, on Zafars insistence, Pandit takes them to meet a local gangster, Bholi Punjaban (Richa Chadda) where Hunny tells them their scheme. Bholi is ready to invest and Lali also gives his shop papers as mortgage. Bholi tells them to come the next day with the number. However, the same night everyone except Choocha falls asleep.

The next morning, Choocha narrates a fake dream which Hunny interprets and gets a number. They go to Bholi and tell the number. But it backfires and Bholi loses her money. Now to recover her money, she gives them a packet containing drug pills which Zafar, Hunny and Lali have to sell at a rave party the same night. She keeps Choocha as security with her. At the party, Police and Narcotics teams conduct a raid and Lali escapes with packet with the police chasing him. Lali manages to fool the police and reaches Neetus place. He also sees Zafar there. Neetu throws the drugs down the drain just as the police are about to search her home. The police leave her place warning Lali. Zafar in the meanwhile tells the whole story to Neetu. Lali also calls Choocha telling him to run from Bholis house which he succeeds in doing.

The next morning, all the five land up at Bholis place. She gives them 24 hours to pay all the money due otherwise she would take the money by selling Lalis shop. The four are fighting at Zafars place where Choocha tells them the truth about not seeing the dream. However, he tells them that he slept last night and he saw a dream. Zafar is not interested while Lali, Hunny and Neetu are. Hunny interprets the dream but they are short of cash even after Neetu is ready to give her savings. However, next morning Lali is able to borrow cash from a homeless man who used to remove parts from Lalis motorbike. They invest and win a huge amount.

They go to Bholis house and give her the money. Then Hunny tells Bholi that he wants to sell those drug pills again and gives her an advance. Impressed Bholi goes inside and returns with the pills only to be caught red handed by the police and Narcotics who have raided her home. In a flashback, it is shown that while Hunny, Choocha, Lali and Neetu were investing the money, Zafar met the Narcotics division and made a plan to catch Bholi. Now with Bholi behind bars, the police pardon the four and they are free.

Three months later, it is shown that Hunny and Choocha are entering the same college on horses as they had planned in the beginning of the film while Pandit is shocked and Lali, Zafar and Neetu are having a good laugh.

==Cast==
* Pulkit Samrat as Hunny (Vikas Gulaati)
* Ali Fazal as Zafar
* Manjot Singh as Lali
* Varun Sharma as Choocha (Dilip Singh)
* Priya Anand as Priya
* Vishakha Singh as Neetu Singh
* Richa Chadda as Bholi Punjaban
* Bhupesh Rai (NIPER) as Bhuppa Ali
* Pankaj Tripathi as Pandit
* Anurag Arora as Narco Officer
* Ajay Trehan as Inspector Khanna
* Pooja Kalra – Fat woman on bus
* V.K Sharma as Teacher
* Smarty – Young boy in Gurudwara
* S.K. Lalwani as Zafars Dad
* Kumkum Ajit Kumar Das as Zafars mother
* Divya Phadnis as Shalu
* Tejeshwar Singh as Monty
* Sanjeeya Vats as Lottery Seller
* Shijkant as Ministers P.A
* Arun Verma as Pradhan
* Ashraf Ul Haque as Shakiya (stealer of Lalis bike)
* Jatinder Bakshi as Jagrata Singer
* Mehak Manwani as Lalis Hot Girl
* Michael Obidke as Eddie
* Mohammed Yusuf as Bobby

==Production==
;Filming
The film was shot extensively in Delhi at various locations, including Miranda House, where most of the college sequences and opening song was shot.   

==Re-release==
The film was rereleased in theatres due to public demand and popularity. The PVR Cinemas showed FUKREY along with other movies in September 2013.      

The sequel of the film has been hinted for public demand as said by Ali Fazal at the success party.
   
   

The movie was well-received and the audience completely enjoyed the film. Also the characters had become memorable. Thus keeping in mind all these factors, the makers feel it will be worth it to have another script like this in place. {{Cite web  | title = Fukrey 2 in the making? | url = http://www.glamsham.com/movies/scoops/13/oct/08-news-fukrey-2-in-the-making-1013013.asp
 | publisher = GlamSham | date = 8 October 2013 | accessdate = 10 October 2013 }} 

==Soundtrack==
The soundtrack of Fukrey, released by T-Series on 14 April 2013, featured music composed by Ram Sampath. Dubstep music entered the Bollywood Music foray with the song Fuk
Fuk Fukrey which blends the Genres Bhangra with occasional Dubstep.  

{{Infobox album |  
|  Name        = Fukrey
|  Type        = Soundtrack
|  Artist      = Ram Sampath
|  Cover       = Fukrey 2013.jpg
|  Released    =  14 April 2013 (India) Feature film soundtrack
|  Length      = 25:24
|  Label       =   T-Series
|  Producer    = Ritesh Sidhwani Farhan Akhtar
|  Last album  =   (2012)
|  This album  = Fukrey (2013)
|  Next album  = Purani Jeans (2014)
}}

{{track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length =
| title1 = Fuk Fuk Fukrey
| extra1 = Ram Sampath, Yash Divecha, Amjad Bagadwa & Vrashal Chavan
| length1 = 3:26
| title2 = Beda Paar
| extra2 = Tarannum Malik, Mika Singh
| length2 = 4:03
| title3 = Lag Gayi Lottery
| extra3 =  Tarannum Malik, Mimosa Pinto, Neisha Maskarenhas, Clint Cabral, Rana Mazumdar & Ram Sampath
| length3 = 3:18
| title4 = Karle Jugaad Karle
| extra4 = Kailash Kher, Pradipta Guha & Keerthi Sagathia
| length4 = 6:01
| title5 = Rabba
| extra5 = Ram Sampath, Clinton Cerejo & Keerthi Sagathia
| length5 = 4:23
| title6 = Ambarsariya
| extra6 = Sona Mohapatra
| length6 = 4:08
}}

===Reception===
The music of the film received good music reviews, especially for the song "Ambarsariya", an adaptation of a traditional Punjabi folk song, performed by Sona Mohapatra.  

==Critical reception==
Taran Adarsh of Bollywood Hungama rated it 3.5/5 and quoted, FUKREY is a twisted and delectably uproarious take on the shortcuts the youth of today indulge in.  Rajeev Masand of CNN-IBN rated the movie 2.5/5 and noted that, A tighter script and more screen time for the excellent Pankaj Tripathi, as enterprising campus security guard Panditji, might have helped turn this moderately entertaining film into a rollicking good caper.  Saibal Chatterjee of NDTV gave the film 3/5 rating. 

==Celebrity Reviews==
Amitabh Bachchan admired the teamwork of Fukrey especially Richa Chadda, who earned rewards for Gangs of Wasseypur.
"She was so good in Gangs of Wasseypur and I believe has done very good work in Fukrey too," Big B posted on his blog. 

Fukrey took Shahrukh Khan to his younger college days. "Mrigh, way to go. Transported me back to Delhi days. Varun (Sharma), Pulkit (Samrat), Manjot (Singh), Ali (Fazal), Pankaj (Tripathi), Richa (Chadda) and all of Fukrey, thanks. Screen mein chhedh ho gaya," tweeted Shah Rukh.
 

==Boxoffice==
The first four-day total of fukrey was around 115.0&nbsp;million nett .  Fukrey grossed nett.  in 2 weeks domestic     The business of the film was varied across India. It was a super hit in Delhi/UP and East Punjab, hit in Rajasthan but average to flop in the rest of the country.  The film has collected around 20&nbsp;million nett as per early estimates on its second Friday which is just 20% less than the first day. The second weekend and second week business will be excellent for the film.  Fukrey has collected 360&nbsp;million nett approx in four weeks. 

==Awards & Nominations==
{| class="wikitable"
|-
! Award
! Category
! Result
! Recipient
! Ref
|- Screen Awards Best Actor in a Comic Role (Male/Female)
|  Richa Chadda (Bholi Punjaban)
|   
|- Best Actor in a Comic Role (Male/Female)
|  Varun Sharma (Choocha)
|
|- Best Ensemble Cast
|  Fukrey
|
|- Best Screenplay
|  Mrigdeep Singh Lamba & Vipul Vig 
|   
|- Apsara Film Big Star Awards BIG Star Most Entertaining Comedy Film
|  Fukrey
|   
|- BIG Star Most Entertaining Singer
|  Sona Mohapatra (Ambarsariya)
|
|- Star Guild Awards Best Dialog
|  Vipul Vig & Mrighdeep Singh Lamba
|
|- Best Performance In A Negative Role
|  Richa Chadda
|
|- Most Promising Debut - Male
|  Varun Sharma
|
|- Best Performance In A Comic Role
|  Varun Sharma (Choocha)
|   
|- Best Female Singer
|  Sona Mohapatra (Ambarsariya)
|
|-
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 