The Blue Hour (2007 film)
 
{{Infobox Film |
  name           = The Blue Hour |
  image          = Poster of the movie The Blue Hour.jpg|
  caption        =  |
  writer         = Eric Nazarian |
  starring       = Alyssa Milano Yorick van Wageningen Emily Rios |
  director       = Eric Nazarian |
  producer       = Brian Knappmiller Lynnette Ramirez |
  distributor    =  |
  released   =   |
  runtime        =  |
  language = English |
  budget         = $500.000 |
}}
 2007 Torino Film Festival, in Turin, Italy.

The Blue Hour takes place in Los Angeles and follows four strangers whose lives cross paths, unaware of the ties that bind them together.

==Plot== Mexican graffiti Armenian camera English pensioner living near the Los Angeles River.

Happy is a talented teenage graffiti muralist with a passion for spray paint and hip hop. Her playground is the concrete banks of the Los Angeles River. While painting a mural of her trademark Payasa, a sad-faced Lady Clown, she encounters Sal, a mentally challenged homeless man who attempts to make contact with her.

Unable to communicate with Happy, Sal then crosses paths with Avo, a vintage camera repairman living with his wife Allegra on the East Bank of the river. Their apartment overlooks Happy’s Payasa mural near the area where their four-year-old daughter Heidi recently drowned. Since Heidi’s death, Avo and Allegra have not spoken. As Happy toils on the Payasa, Avo attempts to reconcile with his wife in the wake of the family tragedy.

A block away from Avo’s apartment, Ridley is a struggling blues guitarist staying in an old hotel by the river. He has returned temporarily to Los Angeles to care for his mother. One night, Ridley hears an enigmatic voice coming from somewhere inside the hotel. Haunted by its mysterious presence, Ridley sets out to discover the source of the voice, running into Sal in the wake of a hit-and-run accident.

Humphrey is an aging pensioner living in an apartment overlooking the ‘islands’ in the river. One morning he wakes up to the sound of Sal screaming on the sidewalk. Having recently lost his wife Ethel, Humphrey spends his days eating lunch by her grave, a few feet from Heidi’s resting place where he sees Allegra. Unsure when his time will come, Humphrey readjusts to everyday life, crossing paths with Happy as he wanders the riverside neighborhood.

Connecting each character peripherally through the Los Angeles River, The Blue Hour explores the delicate ties and common humanity among strangers in a seemingly disconnected landscape.

==Reception==
" ...Largely dialogue-free, the pic shuns histrionics, instead generating its gathering emotional force via carefully crafted images and sharp editing... a strong calling card for debutante Eric Nazarian.” –Variety (magazine)|Variety

“Nazarian demonstrates an uncanny affinity for the language of cinema. ...this is clearly another filmmaker to watch out for in the coming years. ..like the late John Cassavetes|Cassavetes, Nazarian demonstrates an uncanny ability to compose the most striking images and memorable performances on a shoe string budget.” -Turin Film Festival, 2007

“Eric Nazarian’s work is a film of the senses.  The film speaks almost without words, replacing them with a colorful cinematic language well adapted to the four different stories told which form a unity in a very natural way.” –European Network of Young Cinema

== Production ==
=== Genesis === director Eric Nazarian moved to Los Angeles from the former Soviet Union. He was raised in a neighborhood near the Los Angeles River. The rivers unique landscape inspired him to make the film.

==Cast==
*Alyssa Milano - Allegra
*Yorick van Wageningen - Avo
*Emily Rios - Happy
*Derrick OConnor - Humphrey
*Clarence Williams III - Ridley
*Paul Dillon - Sal
*Sophia Malki - Heidi
*Rachel Miner - Julie Sarah Jones - Young Ethel
*Eric Burdon - Bar Singer

==External links==
* 
* 

 
 
 
 
 
 
 