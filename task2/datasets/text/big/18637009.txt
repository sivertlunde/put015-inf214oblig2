The Nazi Plan
{{Infobox film
| name           = The Nazi Plan
| image          =
| image_size     =
| caption        =
| director       = George Stevens
| producer       =
| writer         = Budd Schulberg
| narrator       =
| starring       = See below
| music          =
| cinematography =
| editing        = Robert Parrish
| studio         =
| distributor    =
| released       = 1945
| runtime        =
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Nazi Plan is a 1945 American documentary film directed by George Stevens. The film, compiled from extensive footage of captured Nazi propaganda and newsreel image and sound recordings, was produced and presented as evidence at the Nuremberg Trials for Hermann Göring and twenty other Nazi leaders.

==Background==
"The Nazi Plan" was shown as evidence at the International Military Tribunal (IMT) in Nuremberg on December 11, 1945. It was compiled by Budd Schulberg and other military personnel, under the supervision of Navy Commander James Donovan. The compilers took pains to use only German source material, including official newsreels and other German films (1919-45). It was put together for the US Counsel for the Prosecution of Axis Criminality and the US Office of the Chief Counsel for War Crimes."  

"In the course of this work, Budd Schulberg apprehended Leni Riefenstahl at her country home in Kitzbühl, Austria, as a material witness, and took her to the Nuremberg editing room, so she could help Budd identify Nazi figures in her films and in other German film material his unit had captured. Stuart Schulberg   took possession of the photo archive of Heinrich Hoffmann, Hitler’s personal photographer, and became the film unit’s expert on still photo evidence. Most of the stills presented at the trial carry his affidavit of authenticity." 

== Cast ==
*Adolf Hitler as Himself (archive footage)
*Hermann Göring as Himself (archive footage)
*Rudolf Hess as Himself (archive footage)

== Plot summary ==
 
The films central footage and themes "relied heavily" on the work of German film maker Leni Riefenstahl, in particular the 1935 movie Triumph des Willens (Triumph of the Will).

==Media== The Living Dead written and directed by Adam Curtis.

== External links ==
* 
* 

==References==
 

 
 
 
 
 
 
 
 


 
 