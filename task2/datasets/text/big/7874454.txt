Frozen Land
 
{{Infobox film
| name           = Frozen Land
| image          = Pahamaa.jpg
| caption        = Poster
| writer         = Aku Louhimies
| starring       = Jasper Pääkkönen, Mikko Leppilampi, Pamela Tola
| director       = Aku Louhimies
| producer       = Markus Selin
| music          = 
| cinematography = Rauno Ronkainen
| editing        = Samu Heikkilä
| distributor    = 
| released       = 2005
| runtime        = 130 minutes
| country        = Finland
| language       = Finnish
}}
Frozen Land ( ) is a 2005 Finnish drama film directed and written by Aku Louhimies starring Jasper Pääkkönen, Mikko Leppilampi and Pamela Tola.

==Plot==
When a schoolteacher is sacked, he projects his bad mood at his troubled teenage son. The son, in turn, buys a CD player from a pawnshop with counterfeit money. This starts a chain reaction of misery as every victim projects his problems on to another person.

The film plot is based on (but not credited)  on Leo Tolstoys "The Forged Coupon" part 1 like Robert Bressons "LArgent (1983 film)|LArgent".

==Awards==
The film has won several awards:
*Athens International Film Festival 2005: Best Screenplay
*Bergen International Film Festival 2005: Jury Award
*Göteborg Film Festival 2005: Nordic Film Prize Jussi 2006:
**Best Costume Design
**Best Direction
**Best Editing
**Best Film
**Best Script
**Best Sound Design
**Best Supporting Actor
**Best Supporting Actress
*Leeds International Film Festival 2005: Golden Owl Award
*Lübeck Nordic Film Days 2005: NDR Promotion Prize - Honorable Mention
*27th Moscow International Film Festival 2005: Special Jury Prize   

==Cast==
*Jasper Pääkkönen as Niko Smolander
*Mikko Leppilampi as Tuomas Mikael Saraste
*Pamela Tola as Elina Oravisto
*Petteri Summanen as Antti Arhamo
*Matleena Kuusniemi as Hannele Arhamo
 
*Mikko Kouki as Isto Virtanen
*Sulevi Peltola as Teuvo "Teukka" Hurskainen
*Pertti Sveholm as Pertti Smolander
*Samuli Edelmann as car seller "Jartsa" Matikainen
*Saara Pakkasvirta as shopkeepers mother
*Pekka Valkeejärvi as shopkeeper
*Susanna Anteroinen as Headmaster
*Niklas Hellakoski as Konsta
*Emilia Suoperä as Maria, angel in the ambulance
*Jonathan Kajander as Joonatan "Jonttu" Arhamo

==References==
 

== External links ==
*  

 
 
 
 
 
 
 


 
 