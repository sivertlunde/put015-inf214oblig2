Meat Weed Madness
{{Infobox film
| name           = Meat Weed Madness
| image          = 
| caption        = 
| director       = Aiden Dillard
| producer       = 
| writer         = Aiden Dillard
| starring       = Carey Sveen, Carl Skoggard, Dennis Palazzolo, Ann Liv Young, Alon Belua-Balvg, Liz Santoro, Renee Archibald   
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Troma Entertainment 
| released       =   
| runtime        = 85 minutes 
| country        = United States
| language       = English
| budget         =  
| gross          = 
}}
Meat Weed Madness is a 2006 film written and directed by North Carolina-based writer/director Aiden Dillard.   

==Background==
In 2004, Dillard had submitted a short film to the TromaDance festival, named "The Battle Between the Burps and Farts". Troma Entertainment released Meat Weed Madness despite executive Lloyd Kaufman proclaiming in the "extras" section of the DVD for Meat Weed Madness that the earlier film was the only film in the history of the festival that was booed by everybody in attendance. 

==Plot== cannabis is cultivated out of human flesh,  placing its users under a sex-fuelled frenzy called "Meat Weed Madness".    One night, Jessie Bell runs on to the manor in an effort to find help and is convinced to stay for a bit by Lord Meat Weed.

Further into the film, three girls, billing themselves the Hells Belles  from the local high school show up, having murdered their teacher  and start playing in the manor. One by one, they are hypnotised by the evil minotaur Bullpucky  and raped. Jessie learns of his antics, discovers that he requires a virgin to release him from his murderous behaviour,  that she is the reincarnated Jezebel Meetweed  who mated with the aforementioned cow and gave birth to Bullpucky  and runs as fast as she can away from the manor;  this isnt fast enough as she is caught and the final quarter of the film is spent in the wedding between Jessie and Bullpucky. 

==Critical reception==
Film Threat called the film was a "curious fusion of Alice in Wonderland, Reefer Madness and Herschell Gordon Lewis Two Thousand Maniacs. Its shot directly onto video and contains lots of gore, bizarre situations and female nudity. Its not particularly funny or gross or sexy, but it was gratifying in some sort of weird way." Bill Gibron of DVD Talk called the film "insane", and said of the director, "Dillard may be the first filmmaker who avoids the whole kitchen sink concept of cinema. Instead, his is more of a tainted toilet bowl approach."  Judge David Johnson of DVD Verdict was even less diplomatic, and wrote that it was an "incredible waste of time".  He added,
 
From start to finish, I never felt this flick was anything more than an assemblage of Dillards pals screwing around with rudimentary special effects work and aping for the camera. They scream and dance around and shove their hands into deli meats and stifle their laughter and look at the camera and, yes, take their clothes off. None of it makes sense, none of it is funny, and none of it is entertaining.   
 

==References==
 

 