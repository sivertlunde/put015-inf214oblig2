The Spider (1940 film)
 
{{Infobox film
| name =  The Spider
| image = "The_Spider"_(1940).jpg
| image_size =
| caption = Original British trade ad
| director = Maurice Elvey
| producer = Victor M. Greene Kenneth Horne Reginald Long
| based on = novel Night Mail by Henry Holt
| narrator = Diana Churchill  Derrick De Marney Jean Gillie Cecil Parker
| music =  Ernest Palmer 
| editing =  
| studio = Victor M. Greene Productions (as Admiral Films)
| distributor = General Film Distributors  (UK)
| released = 15 April 1940 (UK)	
| runtime = 81 minutes
| country = United Kingdom English
| budget =
| gross =
}} produced by Admiral Films.

==Synopsis== thriller features a talent agent, whose corrupt actions are uncovered by his business partner. The corrupt sinks deeper by murderng him. Unfortunately for both of them, a young woman witnesses the murder. The talent agent attacks her, she is traumatised and loses her memory. After she is released from hospital, the talent agent resumes his chase, but a detective and his wife intervene with thrilling consequences. 

==Cast==	
*Gilbert Silver - Derrick De Marney	 Diana Churchill	
*Clare Marley - Jean Gillie	
*Lawrence Bruce - Cecil Parker	 Frank Cellier	
*George Hackett - Allan Jeayes	
*Inspector Horridge - Edward Lexy	
*Duke - Jack Melford	 Jack Lambert	 Anthony Holles	
*Nurse - Moira Lynd

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 