Meesa Madhavan
{{Infobox film
| name           = Meesa Madhavan
| image          = Meesha Madhavan.jpg
| image_size     =
| alt            =
| caption        = VCD cover
| director       = Lal Jose
| producer       = Subair Sudhish 
| writer         = Ranjan Pramod
| narrator       = Dileep  Indrajith
| Vidyasagar
| cinematography = S. Kumar (cinematographer)|S. Kumar
| editing        = Ranjan Abraham 
| studio         = Moviekshetra
| distributor    = Kalasangam Kas Varnachithra
| released       =  
| runtime        = 165 minutes
| country        = India
| language       = Malayalam
| budget         =  2.6 crores
| gross          =  15.7 crores
}} Malayalam romantic Dileep in the title role. The film went on to become a blockbuster and the highest grossing Malayalam film of the year, and also established Dileeps status as a star.
 Telugu as Kalyani    and in Kannada as Hori starring Vinod Prabhakar.  It was also unofficially remade in Tamil as Kollaikaran starring Vidharth.

==Plot==
Madhavan (Dileep (actor)|Dileep) is a clever thief who does robbery for a living. He is following the principles of his mentor Mullani Pappan (Mala Aravindan). Meesa Madhavan got his name by the popular saying that if Madhavan rolls his Mustache (Meesa in Malayalam) looking at someone, he will rob his house that night. His enemy was a local money lender Bhageerathan Pillai (Jagathy Sreekumar) who refused to give back his fathers property.

Madhavan falls in love in Bhageerathan Pillais daughter Rukmini (Kavya Madhavan). The sub inspector in the village Eappen Pappachi (Indrajith Sukumaran|Indrajith) has an eye on Rukmini. He steals the idol from the local Temple with the intention of selling it and puts the blame on Madhavan. It becomes Madhavans responsibility to find the culprits and he does that with his mentors help and thus uniting with his girl friend.

==Cast== Dileep ...  Meesha Madhavan Indhrajith ...  Eappan Pappachi (S.I. of Police)
*Kavya Madhavan ...  Rukmini
*Jagathy Sreekumar ...  Krishnavilasom Bhageerathan Pillai
*Sukumari ... Madhavans Mother
*Harisree Ashokan ...  Sugunan (Madhavans friend)
*Cochin Haneefa ...  Thrivikraman
*Mala Aravindan ...  Mullani Pappan
*Oduvil Unnikrishnan ...  Constable Achuthan Nampoothiri (Prabhas father)
*Jyothirmayi ...  Prabha Karthika ...  Malathi (Madhavans younger sister) James ... Pattalam Purushu
*Salim Kumar ...  Advocate Mukundanunny
*Machan Varghese ...  Line Man Lonappan
* Manikandan Pattambi ... as Vazhipaadu Announcer
*Ajaykumar ...  Tea Boy (as Unda Pakru)
*Sanusha ...  Rukmini jr.
*Kalabhavan Prachod ...  Prabhas husband
*Ambika Mohan ... Santhamma (Bhageerathan Pillais wife)
* Gayathri ... as Sarasu (Pattalam Purushus wife)
* Meena Ganesh
* Yamuna ... Madhavans sister in law
*Kozhikode Narayanan Nair ... Advocate
*Lal Jose ... cameo

== Reception ==
The  film was the highest grossing film of the year 2002 with a collection of over 15 crores. It had completed 100 days in all the major cinemas and ran over 250 days.It gave its lead actor Dileep the name Janapriyanaayakan (Popular Star). 

==Soundtrack==
The music album of Meesa Madhavan happens to be one of the most popular works of Vidyasagar (music director)|Vidyasagar. The lyrics were written by Gireesh Puthenchery.

{|class="wikitable" width="70%"
! Song Title !! Singers !! Cast
|- Dileep & Kavya Madhavan
|-
| "Karimizhi Kuruvi" || Sujatha Mohan
|- Dileep & Kavya Madhavan
|-
| "Elavathooru" || Madhuri
|- Dileep & Jyothirmayi
|-
| "Penne Penne" || M. G. Sreekumar, K. S. Chitra, Kalyani Menon || Dileep (actor)|Dileep, Kavya Madhavan, Jyothirmayi & Harisree Asokan
|- Dileep & Kavya Madhavan
|- Dileep & Kavya Madhavan
|-
| "Pathiri" || Machad Vasanthy
|-
| "Theme Music" || Instrumental
|-
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 