X-Large (film)
 
{{Infobox film
| name           = X-Large 
| image          = X-Large poster.jpg
| alt            = 
| caption        = 
| director       = Sherif Arafa
| producer       = 
| writer         = Ayman Bahgat Amar
| starring       = Ahmed Helmy Donia Samir Ghanem Ibrahim Nasr
| music          = Hisham Nazeeh
| cinematography = Ayman Abu El Makarem
| editing        = 
| studio         = 
| distributor    = Brothers United for Cinema
| released       =  
| runtime        = 130 minutes
| country        = Egypt
| language       = Arabic
| budget         = 
| gross          =
}}

X-Large ( ) is a 2011 Egyptian Comedy film. 

==Plot ==

Magdy is an overweight man with a bunch of female friends who all see him as one of the girls,  he’s about as sexually appealing to them as a teddy bear. All he wants is a woman who can see past his unsightly exterior and see him for who he is inside. He reconnects with Dina, a girl he used to crush on back when he was a skinny kid, via Facebook and they get to know each other over the phone, without ever having seen any pictures of each other.

Dina, upon returning to Egypt, asks Magdy to pick her up from the airport which he agrees to do while secretly praying that she doesn’t turn out to be too conventionally attractive. When Magdy sees how beautiful Dina is, he lacks the confidence to out himself as Magdy and instead introduces himself as Magdy’s cousin Adel and informs her that Magdy had to travel abroad. With his friends’ help, he proceeds to try and get her to love him as Adel, fat and all.

==Cast==

* Ahmed Helmy as Magdy
* Donia Samir Ghanem as Dina
* Ibrahim Nasr as Azmy
* Emy Samir Ghanem as mai

==References==
 

 
 
 
 
 


 
 