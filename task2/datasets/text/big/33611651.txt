The Racing Strain (1932 film)
{{Infobox film
| name           = The Racing Strain
| image_size     = 
| image	=	The Racing Strain FilmPoster.jpeg
| caption        = 
| director       = Jerome Storm
| producer       = Willis Kent (producer)
| writer         = Dorothy Davenport (story, uncredited) Betty Burbridge (adaptation) and Willis Kent (adaptation, uncredited)
| narrator       = 
| starring       = See below
| music          =  William Nobles
| editing        = Ethel Davey
| studio         = Willis Kent Productions
| distributor    = Maxim Productions
| released       = 31 December 1932
| runtime        = 58 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Racing Strain is a 1932 American film directed by Jerome Storm.

== Plot summary ==
A race-car driver whose career is on the skids because of his drinking falls for a rich society girl. That motivates him to clean up his act and resume his career, but it may be too late for that.

== Cast ==
*Wallace Reid Jr. as Bill Westcott aka The Big Shot Dickie Moore as Bill Westcott as a Little Boy
*Phyllis Barrington as Marian Martin
*Paul Fix as King Kelly
*J. Farrell MacDonald as Mr. Martin Eddie Phillips as "Speed" Hall
*Ethel Wales as Aunt Judy
*Otto Yamaoka as Togo
*Mae Busch as Tia Juana Lil
*J. Frank Glendon
*Lorin Raker as Jack Westcott
*Donald Reed
*James P. Burtis
*Kit Guard as Kings Mechanic

== Preservation status ==
This film is in the public domain, and is available for free download at the Internet Archive.

==See also==
*List of films in the public domain in the United States

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 