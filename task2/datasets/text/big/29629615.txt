Shadow of the Boomerang
{{Infobox film
| name           = Shadow of the Boomerang
| image          =
| image size     =
| caption        =
| director       = Dick Ross
| producer       = Dick Ross
| writer         = Dick Ross John Ford
| narrator       =
| starring       = Georgia Lee Dickie Jones Jimmy Little
| music          =Ralph Carmichael
| cinematography = Mark McDonald James B. Drought
| editing        = Irvin Berlin
| studio = Word Wide Pictures
| distributor    = MGM
| released       = 17 August 1961
| runtime        = 98 minutes
| country        = Australia
| language       = English
| budget         = £75,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 230  
| preceded by    =
| followed by    =
}} Australian drama film directed by Dick Ross and written by Dick Ross and John Ford. It was a Christian Western about a cattle station manager who learns to overcome his prejudice against aboriginals.

==Plot==
An American brother and sister, Bob and Kathy Prince, have come to Australia to manage a cattle station owned by their father Bob. Bob is prejudiced against aboriginals. He refuses to let stockman Johnny attend Billy Grahams 1959 crusade of Australia. However Johnny is fatally gored to death after saving Bob from being attacked by a bull. Bob overcomes his prejudice.

==Cast==
* Georgia Lee – Kathy Prince
* Dickie Jones – Bob Prince
* Jimmy Little – Johnny
* Marcia Hathaway – Penny
* Ken Frazer as stockman
*Keith Buckley as stockman
* Vaughan Tracey – Dr Cornell
* Billy Graham – Himself
*Hugh Sanders
*Maurice Manson
*Orville Sherman
*Vicky Simms 

==Production==
The film was inspired by Billy Grahams 1959 crusade and was made by World Wide Pictures, the film arm of the Billy Graham Evangelistic Association.

In October 1959, the American director, Dick Ross, and stars, arrived in Sydney. Ross co-wrote the script with Australian author John Ford. Filming started in November and mostly took place near Camden.  The movie was made with funds raised during the crusade and took 25 days to shoot. 

The film starred Marcia Hathaway, who was killed by a shark in 1963. She had become a born again Christian during Billy Grahams visit to Australia in 1959.  She is the last person to date to be killed by a shark in Sydney Harbour.  

Aboriginal singer Jimmy Little made his film debut.

==References==
 

==External links==
*  
*   at Creative Spirits
*  at TCMDB
*  at Oz Movies
 

 
 
 
 


 