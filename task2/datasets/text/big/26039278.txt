Troublesome Night 11
 
 
{{Infobox film
| name = Troublesome Night 11
| image =
| caption =
| film name = {{Film name| traditional = 陰陽路十一之撩鬼條命
| simplified = 阴阳路十一之撩鬼条命
| pinyin = Yīn Yáng Lù Shí Yī Zhī Liào Guǐ Tiáo Mìng
| jyutping = Jam1 Joeng4 Lou6 Sap6 Jat1 Zi1 Liu4 Gwai2 Tiu4 Meng6}}
| director = Yeung Wan-king
| producer = Nam Yin
| writer = Jameson Lam
| starring = 
| music = Mak Jan-hung
| cinematography = Joe Chan
| editing = Eric Cheung
| studio = Nam Yin Production Co., Ltd. East Entertainment Limited B&S Limited
| distributor = B&S Films Distribution Company Limited
| released =  
| runtime = 90 minutes
| country = Hong Kong
| language = Cantonese
| budget =
| gross = HK$21,525
}}
Troublesome Night 11 is a 2001 Hong Kong horror comedy film produced by Nam Yin and directed by Yeung Wan-king. It is the 11th of the 19 films in the Troublesome Night film series.

==Plot==
A hustler tricks a restaurateur into lending him HK$2 million, drugs her, brings her to a deserted beach and kills her. A group of friends doing voluntary work at the beach discover the corpse and report to the police, but the body had disappeared when they come back. The vengeful spirit of the restaurateur possesses the other girls and returns to take her revenge on the hustler.

==Cast==
* Law Lan as Mrs Bud Lung
* Halina Tam as Moon
* David Lee as Tom Lee
* Tong Ka-fai as Bud Gay
* Ronnie Cheung as Bud Yan
* Teresa Mak as Lau Sau-wan
* Bessie Chan as Eva Chan
* Vivian Lok as Jenny
* Kwai Chung as Lai Chor-pat
* Kau Man-lung as Lai Chor-kau
* Jameson Lam as Toms master
* Jeff Kam as Jeffrey
* Au-yeung Hoi-suen
* Benny Law as Keung

==External links==
*  
*  

 

 
 
 
 
 
 
 


 
 