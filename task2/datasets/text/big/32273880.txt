The Warring States
{{Infobox film
| name = The Warring States
| film name = {{Film name| traditional = 戰國
| simplified = 战国
| pinyin = Zhàn guó}}
| image = The-warring-states-2011-1.jpg
| caption = Promotional poster
| director = Chen Jin
| producer = Lu Zheng
| screenplay = Shen Jie
| starring = Francis Ng Jing Tian Sun Honglei Kim Hee-sun Kiichi Nakai Jiang Wu
| music = S.E.N.S.
| cinematography = Hyung-ku Kim
| editing = 
| studio = Beijing Starlit Movie and TV Culture Co. Ltd.
| distributor = China Lion Film Distribution
| released =  
| runtime = 130 minutes
| country = China
| language = Mandarin
| budget = ¥150 million (~United_States_dollar|US$24 million)
| gross = $11,868,961
}}
The Warring States  is a 2011 Chinese film directed by Chen Jin. The story takes place during the Warring States period, but is only very loosely based on the actual history.    The plot focuses on the rivalry between military generals Pang Juan and Sun Bin,  both disciples of Guiguzi.

== Cast ==
*Francis Ng as Pang Juan
*Jing Tian as Tian Xi
*Sun Honglei as Sun Bin
*Kim Hee-sun as Pang Fei King of Qi
*Jiang Wu as Tian Ji

== Production ==
Production for The Warring States began in March 2010.   

== Reception ==

=== Critical reception ===
The Warring States has been described as "sweeping" and "melodramatic".      It has received mixed reviews from critics, with Rotten Tomatoes giving the movie a 40% rating (with an average score of 5.9/10), based on reviews from 5 critics.   

Mike Hale of The New York Times found it difficult to follow the "compressed, chaotic narrative" without a "familiarity with the byzantine history of China". Hale noted that the film included "huge numbers of arrows, soldiers, dead bodies and palace steps". 

Robert Abele of the Los Angeles Times described the film as "mildly confusing" and "unremarkable" overall, though he praised the "nifty fight sequences, clever bits of wartime subterfuge, scenic outdoor photography and Hongleis quirky charm".   

Similarly, Brent Simon of Screen International said the "historical epic" displayed "impressive costume design and much natural scenic beauty", but called it not "particularly persuasive or enrapturing".   

Nick Schager of The Village Voice described the films opening as "invigorating", but overall "tedious" and "a morass of melodramatic romance, torture, and suicide". Schager praised Sun Hongleis performance as the "films only truly epic element". 

Avi Offer of NYC Movie Guru gave The Warring States a very positive review. Offer praised the "thrilling war film" for its "exhilarating action sequences", "compelling dramatic scenes", and "exquisite set design, cinematography, musical score and costume design".   

=== Box office ===
The Warring States earned $11.8 million worldwide.    After six days of release, the film reached first place in Chinas box office chart.   

== References ==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 