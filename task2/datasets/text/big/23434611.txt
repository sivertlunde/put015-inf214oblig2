Fire Festival (film)
 
{{Infobox film
| name           = Fire Festival
| caption        = A poster with the transliterated version of the original Japanese title: Himatsuri
| image	=	Fire Festival FilmPoster.jpeg
| director       = Mitsuo Yanagimachi
| producer       = Kazuo Shimizu
| writer         = Kenji Nakagami
| starring       = Kinya Kitaoji
| music          = Tōru Takemitsu
| cinematography = Masaki Tamura
| editing        = Sachiko Yamaji
| studio         = Seibu Saizon Group Cine Saizon Production Gunrō
| distributor    = 
| released       =  
| runtime        = 125 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
  is a 1985 Japanese drama film directed by Mitsuo Yanagimachi. It was screened in the Un Certain Regard section at the 1985 Cannes Film Festival.   

==Cast==
* Kinya Kitaoji - Tatsuo
* Kiwako Taichi - Kimiko
* Norihei Miki - Yamakawa
* Junko Miyashita - Sachiko, Tatsuos wife
* Ryota Nakamoto - Ryota
* Aiko Morishita - Nursery school teacher
* Rikiya Yasuoka - Toshio
* Kenzo Kaneko - Kimikos brother-in-law
* Sachiko Matsushita - Tatsuos Sister
* Aoi Nakajima - Kimikos sister
* Gozo Soma - (as Gôzô Sôma)
* Kin Sugai - Tatsuos Mother
* Masako Yagi - Tatsuos Sister

==References==
 

==External links==
* 

 

 
 
 
 
 
 