El tesoro de la isla Maciel
{{Infobox film
| name           = El tesoro de la isla Maciel
| image          =El tesoro de la isla maciel.jpg
| image_size     =
| caption        =
| director       = Manuel Romero
| producer       =
| writer         = Manuel Romero
| narrator       =
| starring       = Luis Arata, Severo Fernández and Alberto Bello
| music          =Rodolfo Sciammarella
| cinematography = Hugo Chiesa
| editor       = Juan Soffici
| studio = Lumiton
| distributor    =
| released       = 
| runtime        = 73 minutes
| country        = Argentina Spanish
| budget         =
}} 1941 Argentina|Argentine comedy film.

==Production==

The 73-minute black and white Lumiton film was directed and written by Manuel Romero. 
It is an adaptation of a popular stage play. 
Music is by Rodolfo Sciammarella and cinematography by Hugo Chiesa.
The film was released in Argentina on 8 July 1941.
It stars Luis Arata, Severo Fernández and Alberto Bello. 

==Synopsis==

A retired sea captain is profoundly bored. His sons hope to wake him up by staging a fake treasure hunt.
This turns into a difficult journey to the Río de la Plata islands.
The family are unaware that their backyard holds a valuable oil well.
The farce is fast-paced, only slightly hampered by a romantic subplot. 

==Reception==

El Mundo said the film achieved some popular comic effect. Although void of any cinematographic merit, the dynamism of its action could not be denied.
The show was amusing. Manrupe and Portela said of the film that it was a simple work derived from a farce, but there were some moments of humor from a runaway Luis Arata and an insufferable Severo Fernández. 
Another critic called the film puerile and verbose. 

==Cast==
The cast included: 
 
* Luis Arata as Lorenzo García
* Severo Fernández as Juan Tunín
* Alberto Bello as Captain Pedro Santini
* Silvana Roth as María Santini
* Juan Mangiante as Bianchi
* Alfredo Jordan as Alejandro
* María Armand as Antonia Santini
* Gerardo Rodríguez as Doctor Carlos Juárez
* Cayetano Biondo as Professor Curot
* Fernando Campos as manager of a shipping company
* Jimmy Hart as American engineer
 

==References==
Citations
 
Sources
 
* |url=http://www.imdb.com/title/tt0316747/
 |title=El tesoro de la isla Maciel|work=IMDb|accessdate=2014-06-08}}
*{{cite journal|ref=harv|url=http://www.nytimes.com/movies/movie/235014/El-Tesoro-de-la-Isla-Maciel/overview
 |last=Erickson|first=Hal|year=2010|title=El Tesoro de la Isla Maciel|journal=The New York Times|accessdate=2014-06-08}}
*{{cite book|ref=harv
 |last1=Manrupe|first1=Raúl |last2=Portela|first2=María Alejandra|year=2001
 |title=Un diccionario de films argentinos (1930-1995) |location=Buenos Aires|publisher=Corregidor |ISBN=950-05-0896-6}}
*{{cite book|ref=harv
 |last=Núbila|first=Domingo di|title=La época de oro: historia del cine argentino I
 |url=http://books.google.com/books?id=LZsuAAAAYAAJ|accessdate=2014-06-08
 |date=1998-01-01|publisher=Ediciones del Jilguero|isbn=978-987-95786-5-0}}
 

 
 
 
 
 
 
 
 
 