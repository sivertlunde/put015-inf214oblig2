None but the Lonely Heart (film)
 
{{Infobox film
| name           = None But the Lonely Heart
| image          = None but the Lonely Heart VideoCover.jpeg
| image_size     = 
| caption        = 
| director       = Clifford Odets
| producer       = David Hempstead
| writer         = Clifford Odets
| based on       =  
| narrator       = 
| starring       = Cary Grant Ethel Barrymore Barry Fitzgerald June Duprez Jane Wyatt George Coulouris Dan Duryea
| music          = Hanns Eisler George Barnes
| editing        = Roland Gross
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

None but the Lonely Heart is a 1944 American film which tells the story of a young Cockney drifter who returns home with no ambitions but finds that his family needs him. Adapted by Clifford Odets from the novel by Richard Llewellyn and directed by Odets, the movie stars Cary Grant, Ethel Barrymore, and Barry Fitzgerald.

The title of the film is taken from one of Pyotr Ilyich Tchaikovsky|Tchaikovskys best-known None but the lonely heart|songs, which is featured in the background music.

==Plot==
Ernie Mott (Cary Grant) is a restless, irresponsible, wandering cockney with a good musical ear. On Armistice Day, Ernie visits the tomb of The Unknown Warrior, which memorializes those who died in World War I, including his father. Ernie wants a better life but doesnt want to settle down or work for it. When he returns home, Ma Mott (Ethel Barrymore) asks why he has returned after so long, and she gives him an ultimatum that he must stay home now or leave forever.  He informs her that he will then be leaving next morning, and goes out to get a drink.  He meets fellow musician Aggie Hunter (Jane Wyatt) outside the bar, but instead prefers the company of a gangsters fickle ex-wife Ada Brantline (June Duprez). However, when Ernie becomes smitten with Ada, she rejects his offer of a date when he tells her he will be leaving town tomorrow.

The next morning, Ma Mott tells her pawnbroker friend, Ike Weber (Konstantin Shayne), that she has cancer.  Ma and Ernie get into another fight, but after he storms out, Ike shares with him that his mother needs him in her battle with cancer. Ernie returns and says that he will stay with her at home, and help her run her shop.
 police chase until his car collides with a truck and explodes into flames. Ada implores him to run away with her, but he does not want to leave his dying mother.

When Ernie is eventually bailed out of jail by Ike, he finds out that after the police find Ernies platinum cigarette case—his birthday gift from Ma—was stolen, the police arrest her and put her in prison. She begs for forgiveness for shaming the family, and dies in prison hospital. When he returns home, he learns via a letter from Ada that she decided to stay with Mordinoy because that would make her life easier.  Ernie is crushed, and walks along the street until he gets to Aggies door and walks in.

==Cast==
 
* Cary Grant as Ernie Mott
* Ethel Barrymore as Ma Mott
* Barry Fitzgerald as Henry Twite
* June Duprez as Ada Brantline
* Jane Wyatt as Aggie Hunter
* George Coulouris as Jim Mordinoy
* Dan Duryea as Lew Tate
* Roman Bohnen as Dad Pettyjohn
* Konstantin Shayne as Ike Weber
 

==Production==
None but the Lonely Heart and 1935 in film|1935s Sylvia Scarlett were the only two films in which Cary Grant used a Cockney accent,  though that was not his original accent, as he was from Bristol.  The unique vocal intonations with which he spoke in every other film were the results of an unsuccessful attempt to go from an English to an American accent so that he wouldnt be limited to playing British roles in American movies. 

 . Retrieved March 1, 2014. 

According to The Hollywood Reporter, the East End London road set in this film was the largest and most complete external set constructed inside a sound stage at that time. The set measured 800 feet long and extended the length of two sound stages. 

==Reception==
Lela Rogers, the mother of Ginger Rogers, denounced the script of None but the Lonely Heart at a House Un-American Activities Committee hearing as a "perfect example of the propaganda that Communists like to inject" and accused Odets of being a Communist. Rogers cited the line spoken by "Ernie" to his mother, "youre not going to get me to work here and squeeze pennies out of little people who are poorer than I am," as an example of Communist propaganda.  Hanns Eisler, who was nominated for an Academy Award for composing the films score, was also interrogated by HUAC and was designated as an unfriendly witness for his refusal to cooperate. 

== Academy Awards==
17th Academy Awards:
;Wins    Best Actress in a Supporting Role: Ethel Barrymore
;Nominations Best Actor: Cary Grant Best Film Editing: Roland Gross
*  

==Legacy==
Musical comedian and parodist Spike Jones recorded a three-minute spoof of radio soap operas entitled None but the Lonely Heart (A Soaperetta) in the 1940s.

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 