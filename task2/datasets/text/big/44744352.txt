The Divorce (1970 film)
{{Infobox film
 | name = The Divorce
 | image = The Divorce (1970 film).jpg
 | caption =
 | director =Romolo Guerrieri
 | writer = Alberto Silvestri Franco Verucci
 | starring =  Vittorio Gassman
 | music = Fred Bongusto
 | cinematography = Sante Achilli
 | editing =   
 | producer = Mario Cecchi Gori
 | language = Italian 
 | released = 1970
 }} 1970 Italian comedy film directed by Romolo Guerrieri.    

== Cast ==

* Vittorio Gassman: Leonardo Nenci
* Anna Moffo: Elena, Leonardos wife
* Nino Castelnuovo: Piero 
* Anita Ekberg: Flavia  Riccardo Garrone: Umberto 
* Claudie Lange: Sandra, Umberto s wife 
* Alessandro Momo: Fabrizio, Leonardos son
* Francesco Mulé: Friar Leone 
* Helena Ronée: Daniela Gherardi
* Massimo Serato: Mario, Danielas father
* Clara Colosimo: Isolina / Mafalda 
* Nadia Cassini: Carolina  
* Lars Bloch: Alex Bjørnson 
* Renzo Marignano: Marco 
* Umberto DOrsi:  Doctor 
* Tiberio Murgia: Man at phone
* Mario Brega:  News-vendor 
 
==References==
 

==External links==
* 

 
 
 
 
 
 

 
 