The Pink Panther (2006 film)
{{Infobox film
| name = The Pink Panther
| image = Pinkpanther_mp.jpg
| caption = Theatrical release poster
| border = yes
| director = Shawn Levy
| producer = Robert Simonds
| screenplay = Len Blum Steve Martin
| story = Michael Saltzman Len Blum
| based on =  
| starring = Steve Martin Kevin Kline Jean Reno Emily Mortimer Henry Czerny Beyoncé|Beyoncé Knowles
| music = Christophe Beck Henry Mancini   Jonathan Brown
| editing = George Folsey, Jr. Brad E. Wilhite Robert Simonds Productions
| distributor = Metro-Goldwyn-Mayer Columbia Pictures 20th Century Fox (UK cinema release)
| released =  
| runtime = 93 minutes
| country = United States France Czech Republic
| language = English
| budget = $80 million
| gross = $158.9 million 
}} detective comedy reboot of The Pink Panther franchise, marking the tenth installment in the series. In this film, Inspector Jacques Clouseau is assigned to solve the murder of a famous soccer coach and the theft of the famous Pink Panther diamond. The film stars Steve Martin as Clouseau and also co-stars Kevin Kline, Jean Reno, Emily Mortimer, and Beyoncé|Beyoncé Knowles. Despite a negative reception from  critics, the film was a commercial success, as well as one of Martins most successful movies to date, grossing $158.9 million worldwide.   

==Plot==
The film opens as Chief Inspector Charles Dreyfus narrates a flashback of a soccer match semi-final between France and China. French coach Yves Gluant comes down the stadium, wearing the Pink Panther diamond ring, and kisses his girlfriend, pop star Xania, after whispering something to her. After France wins the game, Yves dies with a poison dart in his neck and the Pink Panther missing from his hand. Eager to win the Medal of Honor (the Légion dhonneur), Dreyfus promotes a clumsy policeman and "village idiot", Jacques Clouseau, to the rank of Inspector and assigns him to the Pink Panther case. Meanwhile, Dreyfus assembles a secret team of top investigators to crack the case, allowing Clouseau to serve as the public face of the investigation so that media attention is focused on Clouseau rather than Dreyfuss team. Dreyfus assigns Gilbert Ponton as Clouseaus assistant and instructs Ponton to keep him informed of Clouseaus actions; however, Dreyfus does not count on Ponton becoming good friends with Clouseau, and becoming remorseful of his orders.

Bizu, a French footballer who had an affair with Xania - and therefore is the prime suspect in Gluants murder - is shot in the head by an unknown assassin in the teams locker room. While at a casino to gain information, Clouseau encounters British Secret Agent 006, Nigel Boswell. Boswell foils a robbery at the casino by the notorious "Gas-Masked Bandits", using Clouseaus trenchcoat to hide his identity. Clouseau mistakenly receives credit for the deed in the press and is nominated for the Medal of Honour, much to Dreyfus dismay.

Clouseau follows Xania to New York City, suspecting that she knows more than she is telling. However, despite Pontons insistence that she is most likely a suspect because Gluant cheated on her, Clouseau decides Xania is innocent. Meanwhile, based on the fact that the poison that killed Gluant was derived from Chinese herbs, Dreyfus concludes that the killer is a Chinese envoy named Dr. Pang. Now ready to take charge of the case and win the Medal of Honour, Dreyfus has one of his officers switch Clouseaus bag with one full of weapons at the airport for his return flight to France. The bag sets off the metal detector at the security gate and Clouseau is arrested due to his inability to pronounce "hamburger" correctly. Upon his return to France, the press vilifies him and Dreyfus strips him of his rank for "trying to become a hero". The Chief Inspector now plots to publicly arrest Dr. Pang at the Presidential Ball, where Xania will also be performing.

At home, Clouseau is reading an online article about his arrest when he notices something significant in the photo that he believes may solve the case. From the photo, he deduces that the murderer will next try to kill Xania, and contacts Ponton. The two detectives rush to the Élysée Palace and sneak into the Presidential Ball. While Dreyfus publicly arrests Dr. Pang for double murder, Clouseau and Ponton save Xanias life by capturing her would-be assassin, Yuri, the soccer teams Russian trainer; Yuri believed he was the one who helped the team succeed, though Gluant took all the credit. Clouseau explains that every national soccer teams trainer is required by statute to have a knowledge of Chinese herbs (accounting for the dart which Yuri has jabbed into Gluants neck). He then explains that Yuri killed Bizu, because he blackmailed him for overhearing his rants against Gluant. Clouseau then reveals that Yuri tried to kill Xania out of revenge for her dismissal of him while she was dating Gluant. Clouseau also concludes that the Pink Panther diamond was never stolen, but is sewn into the lining of Xanias purse; Xania received it from Gluant as an engagement ring. She had kept it secret, believing that if she came forward with the diamond, everyone would think she did it, which could have destroyed her singing career. Clouseau explains that he saw the diamond in the online photograph of his arrest, which showed Xanias purse on the airports luggage scanner, but concludes that Xania is the rightful owner of the ring, as Gluant gave it to her before his death.

For successfully solving the case, Clouseau wins the Medal of Honour. While leaving with Ponton, following the ceremony, Clouseau accidentally gets Dreyfuss jacket caught on his car; he remains oblivious to Dreyfuss screams as he drives away. Clouseau and Ponton later visit him in the hospital. After Clouseau wishes him well, he accidentally releases the brake on Dreyfuss bed; the bed races through the hospital and throws Dreyfus into the Seine River|Seine, shouting Clouseaus name in anger as he takes the plunge.

==Cast==

===Main cast=== Jacques Clouseau
*Kevin Kline as Chief Inspector Charles Dreyfus
*Jean Reno as Gendarme Gilbert Ponton
*Emily Mortimer as Nicole Durant
*Henry Czerny as Yuri the Trainer
*Kristin Chenoweth as Cherie (bit-part)
*Roger Rees as Raymmond Laroque (bit-part)
*Beyoncé|Beyoncé Knowles as Xania

===Supporting cast===
*William Abadie as Bizu
*Scott Adkins as Jacquard
*Charlotte Maier as Dialect Coach
*Anna Katarina as Agent Corbeille
*Alice Taglioni as Female Reporter
*Kristi Angus as Mysterious Woman
*Chelah Horsdal as Security Guard
*Delphine Chanéac as Ticket Checker (as Délphine Chaneac)
*Yvonne Sciò as Casino Waitress
*Jason Statham as Yves Gluant (uncredited)
*Clive Owen as Nigel Boswell/Agent 006 (uncredited)
*Paul Korda as Pierre Fuquette (uncredited)

Of note, Jackie Chan was originally offered the part of Clouseaus manservant, Cato Fong, but turned it down due to his perceived political incorrectness. Gilbert Ponton serves as Catos stand-in. 

==Production== Cheaper by the Dozen, The Pink Panther had a production budget of United States dollar|US$80 million.  Filming began on May 10, 2004. 

==Reception==

===Critical response===
The Pink Panther received generally unfavorable reviews. On Rotten Tomatoes, the film has a rating of 22%, based on 139 reviews, with an average rating of 4.1/10. The sites critical consensus reads, "Though Steve Martin is game, the particulars of the Inspector Clouseau character elude him in this middling update." On Metacritic, the film has a score of 38 out of 100, based on 35 critics, indicating "generally unfavorable reviews". 
 Razzies in 2006, one in the category "Worst Remake or Rip-off", and one in the category "Worst Supporting Actress" for Kristin Chenoweth.

===Box office===
The Pink Panther opened at #1 in the United States, grossing $20.2 million from 3,477 theaters, and took in an additional $20.9 million over the four-day Presidents Day weekend the following weekend.  The film closed in US theaters on April 16, 2006, having grossed $82.2 million in its ten weeks of release. Overseas, the film took $76.6 million, resulting in total box office gross revenue of $158.9 million. US screenings made up 51.8% of box office takings, with international viewings responsible for 48.2%.    The Pink Panther is the highest-grossing film in reboot of The Pink Panther franchise. 

===Home media===
The Pink Panther was released for home viewing on June 13, 2006, and sold 693,588 DVD copies, worth $9,391,182. To date the film has sold 1,579,116 copies&mdash;$23,216,770 of consumer spending. 

==Music== A Woman Like Me" and U.S. number-one hit, "Check on It". The latter serves as the films theme song aside from the Pink Panther theme.

Numerous other songs were used in small parts, but only Becks original score was included on the soundtrack album.  Shuman, I., Simonds, R., Trench, T. (Producers), & Levy, S. (Director). (2006). The Pink Panther. Los Angeles, CA: Metro-Goldwyn-Mayer. 

==Sequel==
  Beyonce Knowles did not return for the sequel. The film features Clouseau and a "Dream Team" of the worlds best detectives formed to catch the international mastermind thief El Tornado, who has stolen several valuable treasures, including the Shroud of Turin, the Popes Ring and the Pink Panther diamond. The film grossed $75,946,615 at the box office. 

==Notes==
 

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 