Lego DC Comics Super Heroes: Justice League vs. Bizarro League
{{Infobox film
| name           = Lego DC Comics Super Heroes: Justice League vs. Bizarro League
| image          = Lego DC Comics Super Heroes Justice League vs. Bizarro League Cover.jpg
| caption        = Blu-ray Cover featuring Batman & Superman 
| alt            = 
| director       = Brandon Vietti
| screenplay     = Michael Jelenic
| based on       = 
| starring       = {{Plain list |  
* Diedrich Bader 
* Troy Baker
* Nolan North
* Khary Payton
* Kari Wahlgren
}}
| music          = {{Plain list | 
* Tim Kelly
}} LEGO DC DC Entertainment
| distributor    = Warner Home Video
| released       =  
| runtime        = 49 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}
Lego DC Comics Super Heroes: Justice League vs. Bizarro League is a   and  .

==Synopsis==
The films synopsis, coinciding with the films trailer, states:
 Metropolis keep confusing Bizarro with Superman, the Man of Steel decides it’s time to find a new home for him....on another planet! It’s up to the Justice League to come to terms with their backward counterparts and team up with them to stop Darkseid and save the galaxy! 

==Cast==
* Nolan North as Superman, Bizarro Bruce Wayne/Batman,  Batzarro  Green Lantern (Guy Gardner), Greenzarro
* Khary Payton as Cyborg (comics)|Cyborg, Cyzarro The Flash,  Desaad 
* Kari Wahlgren as Wonder Woman, Bizarra
* Tony Todd as Darkseid
* John DiMaggio as Lex Luthor and Deathstroke
* Tom Kenny as Penguin (comics)|Penguin, Plastic Man Phil Morris as Green Arrow, Hawkman
* Kevin Michael Richardson as Captain Cold, Gorilla Grodd
* April Winchell as Giganta 

==References==
 

==External links==
*  
*   at Rotten Tomatoes

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 