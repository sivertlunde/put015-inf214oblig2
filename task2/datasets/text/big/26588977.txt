The Last Exorcism
{{Infobox film
| name = The Last Exorcism
| image = The Last Exorcism Poster.jpg
| caption = Theatrical Release Poster
| director = Daniel Stamm Eric Newman Eli Roth Marc Abraham Thomas A. Bliss
| writer = Huck Botko Andrew Gurland
| starring = Patrick Fabian Ashley Bell Iris Bahr
| music = Nathan Barr
| cinematography = Zoltan Honti
| editing = Shilpa Sahi
| studio = Strike Entertainment StudioCanal Arcade Pictures Lionsgate
| released =  
| runtime = 87 minutes 
| country = United States
| language = English
| budget = $1.8 million     . Box Office Mojo. Retrieved 2010-12-21. 
| gross = $67.7 million 
}} found footage supernatural horror film directed and edited by Daniel Stamm. It stars Patrick Fabian, Ashley Bell, Iris Bahr, and Louis Herthum. 
 evangelical minister, who after years of performing exorcisms decides to participate in a documentary chronicling his last exorcism while exposing the fraud of his ministry. After receiving a letter from a farmer asking for help in driving out the devil, he meets the farmers afflicted daughter.  
This film received positive reviews from critics and was a box-office success.

==Plot==
The Reverend Cotton Marcus (Patrick Fabian) lives in Baton Rouge, Louisiana with his wife and son. Marcus, who lost his faith after the birth of his ill son, is accustomed to performing fake exorcisms on "possessed" individuals. He chooses an exorcism request sent by farmer Louis Sweetzer (Louis Herthum), who claims his livestock is being slaughtered by his daughter Nell (Ashley Bell) who is possessed.

After listening to the details of the case, Marcus claims Nell is possessed by a powerful demon named Abalam. Prior to the exorcism, Marcus bamboozles the family into believing he is driving out a demon. After the ritual, Marcus and his film crew leave, believing they have cured her of a mental state that was misdiagnosed as a Demonic possession|possession. That night, Nell appears in Marcus motel room, apparently unwell. Marcus takes Nell to the hospital for tests, which conclude that Nell is in perfect physical condition. Marcus goes to see Louis former pastor, Joseph Manley. Manley informs Marcus that he has not seen Nell for two years. In the morning, Louis takes Nell home, but chains her to her bed after she slices her brother Calebs (Caleb Landry Jones) face with a knife. That night, the crew find a drawing of a dead cat.

Nell steals their camera and goes into her fathers barn, where she brutally smashes a cat to death while filming it with a camera. She returns to the house and approaches Marcus with the camera, and just as shes about to make contact, the crew stop her. The crew then discover two more of her paintings. The first depicts Marcus standing before a large flame, holding up a crucifix.
When everyone learns Nell is pregnant, Marcus accuses Louis of incest, which he denies, insisting that Nell is a virgin and has been defiled by the demon. The crew has a confrontation with Nell, who slashes Marcuss hand with a pair of scissors and then flees. The crew decide to leave, and when they see Nell sitting on the porch, Marcus approaches her and she tackles him. Louis is about to kill Nell with his shotgun, while Nell is begging him to. To keep Louis from killing her, Marcus offers to attempt a second exorcism.
 countercultural symbols on the walls, and Nell and Louis missing.

Marcus and the crew wander into the woods, where they see a large fire and a congregation of hooded cultists, led by Pastor Manley. Louis is tied up, gagged, and blindfolded while hooded figures pray around an altar, atop which Nell is bound. She gives birth to an inhuman child. Manley throws the child into the fire, which causes the fire to grow rapidly as demonic roars emanate from within. At that moment, Marcus faith is resolved as he grabs his cross and rushes towards the fire to combat the evil. Iris and Daniel are discovered; Iris is tackled by a member of the congregation and killed with an axe. Daniel continues to run, until he stops to take a deep breath. He then looks around to see if he was followed, before turning back to find Caleb, who is wearing clean clothes and a bandage covering his wound. Caleb then decapitates Daniel and the camera falls to the ground, before it cuts out.

==Cast==
* Patrick Fabian as Cotton Marcus 
* Ashley Bell as Nell Margaret Sweetzer 
* Iris Bahr as Iris Reisen 
* Louis Herthum as Louis Sweetzer 
* Caleb Landry Jones as Caleb Sweetzer 
* Tony Bentley as Pastor Manley 
* Shanna Forrestall as Mrs. Marcus 
* Becky Fly as Becky 
* Denise Lee as Nurse 
* Logan Craig Reid as Logan 
* Adam Grimes as Daniel Moskowitz
* Jamie Alyson Caudle as Satanic Worshipper 
* Allen Boudreaux as Satanic Worshipper 

==Production== Eric Newman, found footage style (Stamm has previously directed A Necessary Death, another found footage film).  Strike Entertainment and StudioCanal hold the theatrical rights. 

==Release==
The film was slated to be a part of the South by Southwest Film Festival 2010.  However, on February 12, 2010, Lionsgate purchased the rights for the US Distribution  and pulled the film from the SXSW fest and set the release of the film for August 27, 2010. 
 world premiere Film4 FrightFest 2010. 
 2010 San Diego Comic-Con International  and the second screening on 24 July 2010 is narrated by Eli Roth. 

===Poster controversy=== Advertising Standards Agency decided that the image could not be used on a publicly visible poster since that was an untargeted medium but was acceptable on the back cover of Cineworld magazine.  

===Viral campaign===
The Last Exorcism used Chatroulette as the medium of its viral campaign involving a girl who pretends to unbutton her top seductively, then stops and turns into a monster. At the end, the URL of the films official website is flashed on screen. 

===Home media===
The Last Exorcism was released on DVD and Blu-ray Disc|Blu-ray on January 4, 2011. The Blu-ray includes the DVD of the film and a digital copy as well. 

==Reception==

===Critical Reception===
The Last Exorcism has received generally positive reviews from critics, garnering a 73% "Fresh" rating on review aggregator Rotten Tomatoes based on 148 reviews, with the sites consensus being "It doesnt fully deliver on the chilly promise of its Blair Witch-style premise, but The Last Exorcism offers a surprising number of clever thrills."  The film received a 63 out of 100 on Metacritic, indicating "generally favorable reviews".  At Yahoo! Movies the film holds a B- based on twelve reviews. 

===Box office===
The Last Exorcism opened at #2 at the U.S. box office the weekend of August 27, 2010, behind Takers. It grossed $20,366,613 from 2,874 theaters in its first three days.  The Last Exorcism had a budget of $1.8 million.  The film remained in the top five, falling to number four in its second weekend. The film went on to gross $41 million domestically and $26.7 million foreign to total $67.7 million worldwide. 

===Awards and nominations===
{| class="wikitable"
|-
! Year !! Award !! Category !! Recipient !! Result
|-
|rowspan="4"| 2011
| Peoples Choice Award Favorite Horror Movie
| 
|  
|-
| Independent Spirit Award Best First Feature
| Daniel Stamm
|  
|-
| MTV Movie Award Best Scared-As-Shit Performance
| Ashley Bell
|  
|- Empire Awards Best Horror
|
|  
|}

==Sequel==
 
On August 23, 2011,  ,    with Damien Chazelle (Guy and Madeline on a Park Bench) providing the screenplay.  Ashley Bell reprises her role as Nell.  On January 2, 2013, the poster for the film was released, revealing the final title to be The Last Exorcism Part II.  The film is again produced by Eli Roth.

==References==
 

==External links==
*  
*  
*  
*   on StudioCanal

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 