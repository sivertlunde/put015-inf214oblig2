Castle Sinister
{{Infobox film
| name           = Castle Sinister
| image          = 
| caption        = 
| director       = Widgey R. Newman
| producer       = Widgey R. Newman
| writer         = Widgey R. Newman
| starring       = Haddon Mason Eric Adeney Wally Patch
| cinematography = 
| music          = 
| distributor    = Filmophone
| released       = March 1932
| runtime        = 50 minutes
| country        = United Kingdom English
}}
Castle Sinister was a 1932 British horror film produced, written and directed by Widgey R. Newman.  Very little is known of either the film or the director, although available information suggests Newman to have been something of a maverick in the British film industry of the time. Castle Sinister is classed as a lost film.    

==Overview==
The film was apparently set in a lonely mansion in Devon, and was marketed with the tagline "Mad doctor tries to put girls brain into apemans head!"

A brief synopsis in the cinema magazine The Bioscope (6 April 1932) offers a plot outline:  "The story of a young mans adventures in a large country mansion with a scientist who is engaged in forwarding his theory that rejuvenation by the transfer of certain glands is more than a possibility. A girl is, of course, involved to supply the necessary love interest, and a misshapen creature, victim of the scientist, supplies the thrills."  It also mentions "a constantly howling wind and a generous sprinkling of skulls and skeletons".

==Cast==
* Haddon Mason as Roland Kemp
* Eric Adeney as Professor Bandov
* Wally Patch as Jorkins
* Ilsa Kilpatrick as Jean
* Edmund Kennedy as Father

==Survival status==
 75 Most Wanted" list of missing British feature films. 

==References==
 

== External links ==
*  , with extensive notes
*  

 
 
 
 
 
 
 
 