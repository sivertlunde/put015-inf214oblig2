Kakkathollayiram
{{Infobox film 
| name           = Kakka Thollayiram
| image          =
| caption        =
| director       = VR Gopalakrishnan
| producer       =
| writer         = VR Gopalakrishnan
| screenplay     = Mukesh Urvashi Urvashi Chithra Chithra Sukumari Johnson
| cinematography =
| editing        =
| studio         = Navayuga Arts
| distributor    = Navayuga Arts
| released       =  
| country        = India Malayalam
}}
 1991 Cinema Indian Malayalam Malayalam film, Chithra and Sukumari in lead roles. The film had musical score by Johnson (composer)|Johnson.   

==Cast==
  Mukesh
*Urvashi Urvashi
*Chithra Chithra
*Sukumari
*KPAC Lalitha
*Sankaradi
*Appa Haja
*Bobby Kottarakkara
*Ganesh Kumar
*Mamukkoya
*Oduvil Unnikrishnan Saikumar
*Santha Devi
*Santhakumari
*Shivaji
*Thesni Khan
*Biyon
 

==Soundtrack== Johnson and lyrics was written by Kaithapram. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Madanappoo || KS Chithra || Kaithapram || 
|-
| 2 || Paalaruvi Kuliraniyum || MG Sreekumar, Sujatha Mohan || Kaithapram || 
|-
| 3 || Thaanaaro || Krishnachandran || Kaithapram || 
|}

==References==
 

==External links==
*  

 
 
 

 