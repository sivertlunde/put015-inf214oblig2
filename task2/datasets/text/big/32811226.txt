Within the Whirlwind
{{Infobox film
| name           = Within the Whirlwind
| image          = Within the Whirlwind FilmPoster.jpeg
| caption        = 
| director       = Marleen Gorris
| writer         = Nancy Larson
| starring       = Emily Watson
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Germany
| language       = English
| budget         = 
| gross          = 
}}
Within the Whirlwind is a 2009 film directed by Marleen Gorris and based on the book by Eugenia Ginzburg. It stars Emily Watson and Pam Ferris.  Watson has described the film as "the most stretching thing I’ve done as a mature actress."    

==Plot== Soviet concentration camp. Having lost everything, and no longer wishing to live, she meets Dr. Anton Walter (Ulrich Tukur), a Crimean German political prisoner who is a camp doctor in Kolyma. He recommends her for a position as a nurse in the camp infirmary. They fall in love and, slowly, Evgenia begins to come back to life.

==Cast==
*Emily Watson as Evgenia Ginzburg 
*Pam Ferris as Genias mother
*Ian Hart as Beylin
*Ben Miller as Krasny
*Ulrich Tukur as Dr. Anton Walter

==Distribution== market crashed so nobody was buying anything. You have to be very Zen. Walk away and say: I have no idea what’s going to happen and it’s not in my hands.’ At the same time you never quite think that’s going to happen." 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 

 