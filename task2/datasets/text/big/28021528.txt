Mardi Gras (1958 film)
 
{{Infobox film
| name           = Mardi Gras
| alt            =  
| image	=	Mardi Gras FilmPoster.jpeg
| caption        = 
| director       = Edmund Goulding
| producer       = Jerry Wald
| writer         = Hal Kanter Winston Miller based on = story by Curtis Harrington
| starring       = Pat Boone
| music          = Lionel Newman
| cinematography = 
| editing        = 
| studio         = Jerry Wald Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 107 min.
| country        = USA
| language       = English
| budget         = $1.69 million 
| gross          = $2.5 million 
}}

Mardi Gras was a 1958 musical comedy film starring Pat Boone and Christine Carère.

==Plot== French movie goddess (Carère) who happens to be the queen of the "Mardi Gras" parade. The two fall in love, but Carères movie studio wants to capitalize on this newly found love for publicity.

==Cast==
*Pat Boone as Paul Newell 
*Christine Carère as Michelle Marton  Tommy Sands as Barry Denton
*Sheree North as Eadie West Gary Crosby as Tony Collins 
*Fred Clark as Al Curtis 
*Dick Sargent as Dick Saglon
*Barrie Chase as Torchy Larue 
*Jennifer West as Sylvia Simmons 
*Geraldine Wall as Ann Harris
*King Calder as Lt. Col. Vaupell Robert Burton as Comdr. Tydings
* The Corps of Cadets of the Virginia Military Institute
*Christine Carères and Sheree Norths singing voices was dubbed by Eileen Wilson

==Production==
Jerry Wald arranged for second unit filming done of Virginia Military Institute even before a director had been arranged. He originally wanted Gene Kelly but Kelly was too expensive. He eventually decided on Edmund Goulding, whose career was in decline and was therefore cheap, because Wald had admired his films when he was younger.   accessed 31 August 2014 

Pat Boones casting was announced in February 1958. PRODUCER SCORES CHICAGO FILM BAN: Hartman Hits Restriction of ONeill Movie to Adults -Policemans Book Bought
By THOMAS M. PRYORSpecial to The New York Times.. New York Times (1923-Current file)   11 Feb 1958: 36.  Shirley Jones, who had co-starred with Boone in April Love, was meant to play the female lead but had to drop out due to pregnancy. Karen Steele to Co-Star in Bullfighters Story
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   05 Apr 1958: 12  Instead the studio cast French actress Christine Carere, who has just made A Certain Smile for Fox. French Doll: Meet Christine Carere, a Tiny Bundle of Sheer Acting Talent Mardi Gras Is Next for Christine Incomplete Source
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   15 June 1958: g16. 
 The Best In Love and War (1958).
 Deluxe color, this was director Gouldings final film.

==Reception==
Despite generally good notices ("makes for sprightly, gay entertainment" - Los Angeles Times Mardi Gras Teen Delight With Pat, Tommy, Gary
George, Wally. Los Angeles Times (1923-Current File)   09 Nov 1958: E3. ) Mardi Gras failed to do well at the box-office.

North was then released from her studio contract.  Fox seemed to have lost interest in her in 1956 when they signed Jayne Mansfield to a six year contract.

===Awards===
Composer Lionel Newman was nominated for the Academy Award for Best Original Score (Scoring of a Musical Picture) for this film.

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 