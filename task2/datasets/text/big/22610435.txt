Prince Daewon
{{Infobox film
| name           = Prince Daewon 
| image          = King Daewon (1968 film).jpg
| caption        = Theatrical poster for King Daewon (1968)
| director       = Shin Sang-ok 
| producer       = Lee Su-kil
| writer         = Yu Ju-hyeon
| starring       = Shin Young-kyun Kim Ji-mee
| music          = Jeong Yun-ju
| cinematography = Choi Seung-woo
| editing        = Oh Seong-hwan
| distributor    = Anyang Films
| released       =  
| runtime        =
| country        = South Korea
| language       = Korean
| budget         =
| gross          =
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Daewongun
 | mr             = Taewŏn’gun
 | context        =}}
}} 1968 South Korean film directed by Shin Sang-ok. It was chosen as Best Film at the Grand Bell Awards.     

==Synopsis==
A historical drama depicting power struggles in the last days of the Joseon Dynasty. 

==Cast==
* Shin Young-kyun 
* Kim Ji-mee
* Choi Nam-Hyun
* Heo Jang-gang
* Park Am
* Kim Dong-won
* Yoon In-Ja
* Song Mi-nam
* Kim Dong-hun
* Gang Mun

==Bibliography==
*  
*  
*  
*  

===Contemporary reviews===
*1968-04-14. "「동시녹음 첫 실현  」". Hankook Ilbo. 

==Notes==
 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 


 