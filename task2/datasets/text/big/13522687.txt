Hearts Up
 
{{Infobox film
| name           = Hearts Up
| image          = 
| caption        = 
| director       = Val Paul
| producer       =  Harry Carey Val Paul
| starring       = Harry Carey
| music          = 
| cinematography = Harry M. Fowler
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}
 Western film Harry Carey.   

==Plot==
As summarized in a film publication, Jim Drew (Millett),    who is a squaw man (a disparaging term for a white man with an Indian wife), receives word that his wife whom he had long deserted had died and that his daughter was coming by train to live with him. Jim is injured when his cabin catches fire, and dies just as David Brent (Carey) arrives to repay a debt of gratitude. Reading the girls letter stating that she will meet her father in San Francisco, David decides to meet the girl and tell her of her fathers death. But when Lorelei (Golden) arrives, she mistakes David for her father and is so happy with her beautiful home that David cannot bring himself to tell her the truth. She met Gordon Swayne (Braidwood), a surveyor, on the train and retains his friendship, which makes David unhappy. When Gordon discovers that David is not Loreleis father, he threatens him. When Lorelei learns the truth, David decides to go away and leave the girl as mistress of the cabin. Lorelei stops him and tells him she loves him only.

==Cast== Harry Carey as David Brent
* Arthur Millett as Jim Drew Charles Le Moyne as Bob Harding
* Frank Braidwood as Gordon Swayne
* Mignonne Golden as Lorelei Drew

==Critical reception==
While a review found the film pleasing, it noted the age difference between the older Carey and his love interest Golden,  who was 16 years old during filming.

==See also==
* Harry Carey filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 
 