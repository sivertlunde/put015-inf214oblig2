Ullam Ketkumae
{{Infobox film
| name = Ullam Ketkumae
| image = Ullam Ketkumae DVD Cover.jpg
| caption = DVD Cover Jeeva
| Sujatha
| Arya Shaam Laila Asin Asin Pooja Pooja
| producer = Mahadevan Ganesh Usha Venkatramani
| music = Harris Jayaraj
| cinematography = Jeeva
| editing = V. T. Vijayan
| distributor =APINT Ltd
| released = 3 June 2005
| runtime = 156 mins
| country = India
| language = Tamil
| budget = 
| gross = 
}}
 Tamil romance Pooja and Laila  in the lead roles as five college students. The films score and soundtrack are composed by Harris Jayaraj, while Vairamuthu penned the lyrics for the songs. The story tells the reunion of five friends who were together at college and exploring their relationships during the years. The film released after several delays in 2005 and went on to win critical and commercial success.

==Plot==
The movie is all about a group of five college friends - Emaan (Arya (actor)|Arya), Shaam (Shaam), Pooja (Laila Mehdin|Laila), Priya (Asin Thottumkal|Asin), and Irene (Pooja (actress)|Pooja). The friends gather for the wedding of Emaan, years after everyone has parted ways.

The movie starts with Pooja leaving USA to go to her college friend Emmans wedding. Meanwhile,back in India everyone else is helping Emaan with the preparations for the wedding and meeting all their long lost college friends. The movie goes through series of flashbacks of old memories during their college days. Emaan & Irene meet for the first time in a long time, and the first flashback occurs. They were once in love, however it didnt turn as Emman had expected.

Another flashback occurs when Shaam remembers Pooja. Pooja is a fun-loving tomboy who was best friends with Shaam and was treated as if shes one of them. Shaam realizes hes in love with Priya, and Pooja realizes shes in love with Shaam. Shaam and Pooja decides to reveal their love to the ones they love on Valentines Day. Pooja learns that Shaam is in love with Priya, so she kept her love a secret heartbroken. When Priya finds out Shaams in love with her, she rejects his love, for she is a traditional girl and admits that she is also going to get married after college and her parents have decided her husband.

At last when we are taken back to the present day, the wedding. Everyone meets each other, and Pooja arrives. Pooja is no longer the same girl she was back then.She was more mature and not a tomboy at the same time. Pooja finally reveals her love to Shaam using a Valentine Card that she had been waiting for in college but couldnt as she had found out he was in love with pooja. Shaam realizes that Pooja is the one hes supposed to be with her all this time rather than being with priya as he knew she would not accept it because she was a cultra and inncoent.

In the end Shaam accepted Poojas love and end the movie by hugging each other.

==Cast== Arya as Emaan
*Shaam as Shaam Laila as Pooja
*Asin as Priya Pooja as Irene Murali as Poojas father

==Production== Pooja was also signed on to make her début and play Irene in the film, although the films delay meant that several of her other films released before Ullam Ketkumae hit the screens. 

Art director Thotta Tharani created a college campus for the film to be shot in, with his work drawing appreciation.  A scene featuring an inter college day cultural had 100 students from various states take part in the shooting while decorations and a podium were put up for a marriage scene in Taj Connemara hotel.  Raju Sundaram choreographed five songs including a song shot at hot water springs in New Zealand. 

The film went through production hell, with three years being spent on the announcement till release.  The delay also led to the actors featuring in other projects during the period and Jeeva moving on to direct a Hindi film, Run (2004 film)|Run. Despite giving indications that the film would release after delays on April 14, 2004 it was further pushed back due to financial issues.  In May 2005, the Supreme Court finally cleared the decks for the release, as earlier the Madras High court by a February 25 order appointed “Joint Receivers” for the film, which was objected by Prasad Labs who had gone to Supreme Court. It was agreed that the collections from the film will be received by “joint receivers” who will pay Prasad Labs for 72 prints of the film.  The film subsequently opened in Tamil Nadu on June 3, 2005.

==Release==
The film released on June 3, 2005 and became a commercial success at the box office with pundits describing the film as a "sleeper hit". The film took a 90 percent opening in four Chennai screens with large collections reported at the Devi theatre in Chennai in the opening weekend.  The film surprisingly opened at number two at the Chennai box office despite competition, just below the Rajinikanth starrer Chandramukhi. 
 Arya and Asin were able to consolidate their positions of rising actors.    The success of the film later prompted a dubbed Telugu version being released on March 30, 2006 as Preminchi Choodu. 

==Music==
The music and background score were composed by Harris Jayaraj. Lyrics were written by Vairamuthu.(exceptions are noted).
{{Infobox album|  
  Name        = Ullam Ketkumae
|  Type        = Soundtrack
|  Artist      = Harris Jayaraj
|  Cover       = Ullam_Ketkumae_movie_poster.jpg
|  Released    = 2005
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       = Vega Music Bayshore
|  Producer    =
|  Reviews     =
|  Last album  = Thotti Jaya (2005)
|  This album  = Ullam Ketkumae (2005)
|  Next album  = Anniyan (2005)
}}
{| class="wikitable" style="width:50%;"
|- "
! Title !! Singers
|-
| Ennai Pandhada || Srinivas & Srimathumitha
|-
| Kanavugal || Karthik (singer)|Karthik, Arunmozhi, Tippu (singer)|Tippu, Premji Amaran, Febi Mani, Pop Shalini & Suchitra
|-
|O Maname || Hariharan
|-
| Mazhai Mazhai || Unnikrishnan, Harini
|-
| Dho Dho || Franko
|- Ranjith
|}

===Telugu Tracklist=== Telugu as Preminchi Choodu. All Lyrics are penned by Bhuvanachandra.

{| class="wikitable" style="width:50%;"
|- "
! Title !! Singers
|-
| Enno Janmala Bandham || Srinivas & Saindhavi
|-
| Enno Ennenno Kalale || Karthik (singer)|Karthik, Arun,  Premji Amaran, Febi Mani, Pop Shalini & Suchitra
|-
|Oh Manasa || Madhu Balakrishnan
|-
| Shodhinchuko || Unni Krishnan & Harini
|-
| Sie Sie Sie ||  Franko
|- Ranjith
|}

==References==
 

==External links==
* 

 

 
 
 
 
 