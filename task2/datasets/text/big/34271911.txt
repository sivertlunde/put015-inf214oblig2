Manila Calling
{{Infobox film
| name           = Manila Calling
| image          =
| caption        =
| director       = Herbert I. Leeds
| producer       = Sol M. Wurtzel John Larkin (story and screenplay)
| starring       = 
| music          =
| cinematography = Lucien N. Andriot
| editing        = Alfred Day
| distributor    = 20th Century Fox
| released       = 16 October 1942
| runtime        = 81 min.
| country        = United States
| language       = English 
| budget         =
}}

Manila Calling is a 1942 American wartime drama film starring Lloyd Nolan and Carole Landis.

== Cast ==
* Lloyd Nolan as Lucky Matthews
* Carole Landis as Edna Fraser
* Cornel Wilde as Jeff Bailey
* James Gleason as Tim ORourke
* Martin Kosleck as Heller
* Ralph Byrd as Corbett
* Charles Tannen as Fillmore
* Ted North as Walter Jamison
* Elisha Cook Jr. as Gillman
* Harold Huber as Santoro
* Lester Matthews as Wayne Ralston
* Louis Jean Heydt as Harold Watson
* Victor Sen Yung as Armando Leonard Strong

==Plot==
American civil engineering forces struggle to establish an operational radio base on the Philippines, with Japanese forces resisting, and with the complication of a beautiful nightclub singer on site.

== External links ==
*  
*  
*  


 

 
 
 
 
 
 


 