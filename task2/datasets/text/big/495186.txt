Nana (manga)
{{Infobox animanga/Header
| name            = Nana
| image           =  
| caption         = Cover of the Japanese volume 1, featuring Nana Osaki
| ja_kanji        = ナナ
| ja_romaji       = 
| genre           = Drama, romance novel|Romance, Music
}}
{{Infobox animanga/Print
| type            = anime
| author          = Ai Yazawa
| publisher       = Shueisha
| publisher_en    =  
| demographic     = Josei manga|Josei Cookie
| magazine_en     =  
| first           = 2000
| last            = 2009 (on hiatus)
| volumes         = 21
| volume_list     = List of Nana chapters
}}
{{Infobox animanga/Video
| type            = live film
| director        = Kentaro Otani Nana
| producer        = 
| writer          = 
| music           =  TBS
| released        = September 3, 2005
| runtime         = 113 minutes
}}
{{Infobox animanga/Video
| type            = tv series
| director        = Morio Asaka
| producer        = 
| writer          = 
| music           =  Madhouse
| licensor        =  
| network         = Nippon Television|NTV, Animax
| network_en      =  
| first           = April 5, 2006
| last            = March 27, 2007
| episodes        = 47
| episode_list    = List of Nana episodes
}}
{{Infobox animanga/Video
| type            = live film
| title           = Nana 2
| director        = Kentaro Otani
| producer        = 
| writer          = 
| music           = 
| studio          = Toho Company
| released        = December 9, 2006
| runtime         = 130 minutes
}}
 
  is a Japanese josei manga series written and illustrated by Ai Yazawa, serialized in Cookie (manga)|Cookie published by Shueisha. The manga derives its title from the name of the two main characters, both of whom are called Nana. Nana Komatsu is a small town girl who goes to Tokyo to follow her boyfriend and college friends, with the hope of having her dream life. Nana Osaki was in a popular punk band in her home town. She goes to Tokyo with the goal of making it big as a singer. The two Nanas meet on the train ride to the city. Later, they run into each other again when they happen to check out the same apartment, and the girls decide to become roommates. The series chronicles their friendship and their lives as each chases her dreams.

The author of the manga is sick and is still recovering.    It was reported in late April 2010 that she returned home from the hospital and she does not know if or when she will return to work.   
 a sequel released on December 9, 2006, and an anime adaptation that premiered on April 5, 2006. The anime adaptation has subsequently been announced as licensed for release in North America by Viz Media.   Funimation got the broadcast rights to Viz Medias dub and it premiered on the Funimation Channel on September 19, 2009. 

==Plot==
  punk singer who wants to debut with her band, Black Stones (BLAST for short), where she is the lead vocalist and her boyfriend, Ren, is the bassist. Nana and Ren have lived together as lovers since she was 16. When Ren is offered a chance to debut in Tokyo as a replacement member of the popular band, Trapnest (Toranesu in Japanese), Nana chooses to continue on with BLAST and to cultivate her own career instead of following Ren, as she has too much ambition to simply be a rockstars girlfriend. She eventually leaves for Tokyo at the age of twenty to start her musical career.
 Nana Komatsu, the other Nana, has a habit of falling in love at first sight all the time, and depending on other people to help her. When her friends, and then her boyfriend, leave for Tokyo, she decides to join them a year later after having saved enough money at the age of twenty.

The two Nanas meet on a train by chance, both on their way to Tokyo. After a string of coincidences, they come to live together in an apartment numbered 707 (nana means "seven" in Japanese). Despite having contrasting personalities and ideals, the Nanas respect each other and become close friends.

Nana Osaki gives Nana Komatsu the nickname Hachi (after Hachikō, because she is weak-willed and has characteristics that resemble a puppy, and also as a joke since hachi means "eight" and nana means "seven" in Japanese).

While BLAST begins to gain popularity at live gigs, the two Nanas face many other issues together, especially in the areas of friendship and romance. The story of Nana revolves heavily around the romance and relationships of the two characters as one seeks fame and recognition while the other seeks love and happiness.

==Media==

===Manga===
 
Written and illustrated by Ai Yazawa, the individual chapters of Nana premiered in Cookie (manga)|Cookie in 2000 where it ran until June 2009, when the series was put on hiatus due to Yazawa being ill.  Yazawa returned from hospital in early April 2010, though has not specified when or if she will resume the manga.  The chapters have been collected and published in 21 tankōbon volumes in Japan by Shueisha.

Nana is licensed for English-language release in North America by Viz Media. It was serialized in Vizs manga anthology Shojo Beat, premiered in the launch July 2005 issue where chapters appeared until the August 2007 issue.   

===Films===
Two film adaptations have been made for Nana. The first, Nana (2005 film)|Nana, was released on September 3, 2005. The film stars Mika Nakashima as the punk star Nana Osaki, Aoi Miyazaki as Hachi (Nana Komatsu), Ryuhei Matsuda as Ren Honjo, Tetsuji Tamayama (known from tokusatsu followers for having played Tsukumaro Ogami/Shirogane a.k.a. GaoSilver in Hyakujuu Sentai Gaoranger) as Takumi Ichinose, Hiroki Narimiya as Nobuo Terashima, and Matsuyama Kenichi as  Shinichi Okazaki.  The DVD edition was released on March 3, 2006. The film did quite well at the Japanese box office, grossing more than 4 billion yen, and staying in the top 10 for several weeks.   
A sequel, Nana 2, was announced right after the debuted. However, on August 4, 2006, Toho stated that shooting would begin mid-September and that the film was to be released on December 9, 2006. Aoi Miyazaki and Ryuhei Matsuda would not be reprising their respective roles as Nana Komatsu and Ren Honjo; as such, their roles were assigned to Yui Ichikawa and Nobuo Kyou (who also played Isshu Kasumi/KuwagaRaiger in Ninpuu Sentai Hurricaneger), respectively. Some locations from the manga had been changed for the film, and there also were many plot differences. Additionally, the films ending is not the actual end of the manga; "Nana" is an ongoing story. 

===Anime===
  Olivia sings the second opening and first and second endings for the band Trapnest as Reira Serizawa.  The first DVD release was on July 7, 2006.
The anime series was intended to be equal to the manga and it was adapted until the 12th tankoubon to avoid filler. According to Junko Koseki (editor of Nana in Shueisha) and Masao Maruyama (president of Madhouse) a second season is probably going to be aired once the manga series is finished. 
Two girls, both named Nana and of the same age, coincidentally meet on a train trip to Tokyo. They soon find themselves living with each other under the same roof because of an even bigger coincidence. Though they share the same name and age, they differ in just about everything else. Even so, through hard experiences in love and life, a strong friendship is born between them, as both Nanas grow through their hardships and struggle to win the odds.
 . In Italy was broadcast on January 23, 2007, on MTV Italy and on October 24, 2010, on Rai 4.

===Albums inspired by Nana ===
Nana has inspired several studio albums, the most notable being Love for Nana. Several famous artists contributed to it, including Glen Matlock and various Japanese artists. Punk Knight from Nana and Nanas Song Is My Song were albums made up of mostly unknown artists.

====Love for Nana====
01. BEAT 7 ~The Theme of LOVE for NANA~ / 高見沢俊彦 (Takamizawa Toshihiko of The Alfee)
 Tommy heavenly for BLACK STONES

03. Twinkle / 木村カエラ (Kimura Kaela) for BLACK STONES

04. REVERSE / TETSU69 for TRAPNEST

05. stay away / abingdon boys school for BLACK STONES

06. I miss you? / Do As Infinity for BLACK STONES

07. バンビーノ (Bambino) / 布袋寅泰 (Hotei Tomoyasu) featuring もりばやしみほ (Miho Moribayashi) for TRAPNEST

08. Sleepwalking / Glen Matlock & The Philistines featuring Holly Cook (from SEX PISTOLS) for BLACK STONES

09. Sugar Guitar / Skye Sweetnam for TRAPNEST

10. 黎明時代-レイメイジダイ- (reimei jidai) / ジャパハリネット (Japaharinet) for BLACK STONES

11. BLACK CROW / SEX MACHINEGUNS for BLACK STONES
 ZONE for TRAPNEST
 Otsuka Ai) for TRAPNEST

===Video games=== PlayStation 2 (PS2) platform. The PS2 game was produced by Konami and released on 17 March 2005. A PlayStation Portable (PSP) game,   was released on 6 July 2006.

A Nintendo DS game,   was released by Konami in June 2007.

==Reception==
 
Volumes 19 and 20 were the third and fifth highest selling (respectively) manga books of 2008.  Volumes 1 and 2 were listed on YALSAs "2007 Great Graphic Novels for Teens" list. 
The first twelve volumes of the manga series have cumulatively sold over 22 million copies.  As of 2008, it has sold over 43,600,000.  In 2002, the manga won the Shogakukan Manga Award for shōjo.   

==References==
 

==Further reading==
*  of vol 1 at Anime News Network
*  of vol 15 at Anime News Network
*  of vols 16–18 at Anime News Network
*  of DVD box set 1 at Anime News Network
*  of DVD box set 2 at Anime News Network

==External links==
*    
*     (dead link)
*    
*     (advertising site)
*  
*   at Viz Media

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 