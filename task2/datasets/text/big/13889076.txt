Jeanne Dielman, 23 quai du Commerce, 1080 Bruxelles
{{Infobox Film
| image          = 
| name           = Jeanne Dielman, 23, Quai du Commerce, 1080 Bruxelles
| aka            =
| director       = Chantal Akerman
| producer       = Corinne Jénart Evelyne Paul
| writer         = Chantal Akerman
| starring       = Delphine Seyrig Jan Decorte Jacques Doniol-Valcroze
| cinematography = Babette Mangolte
| editing        = Patricia Canino
| distributor    = The Criterion Collection (USA DVD) Janus Films (USA)
| released       =  
| runtime        = 201 min
| budget         = $120,000
| country        = Belgium France
| language       = French
}}
Jeanne Dielman, 23, Quai du Commerce, 1080 Bruxelles ( , "Jeanne Dielman, 23 Commerce Quay, 1080 Brussels") is a 1975 film by Belgian filmmaker Chantal Akerman.

Upon the films release, The New York Times called Jeanne Dielman the "first masterpiece of the feminine in the history of the cinema". Chantal Akerman scholar Ivone Margulies says the picture is a filmic paradigm for uniting feminism and anti-illusionism.  The film was named the 19th-greatest film of the 20th century by The Village Voice.   

==Plot==
 prostitutes herself to a male client daily for her and her sons subsistence. Like her other activities, Jeannes prostitution is part of the routine she performs every day by rote and is uneventful. But on the second day, Jeannes routine begins to unravel subtly, as she drops a newly washed spoon and overcooks the potatoes that shes preparing for dinner. These alterations to Jeannes existence prepare for the climax on the third day, when she unexpectedly has an orgasm with the days client, after which she stabs him fatally with a pair of scissors. 

==Cast==
* Delphine Seyrig as Jeanne Dielman
* Jan Decorte as Sylvain Dielman
* Henri Storck as the first client
* Jacques Doniol-Valcroze as the second client
* Yves Bical as the third client

==Production==
After establishing herself as a major film director with Je, tu, il, elle (1974), Akerman said that she "felt ready to make a feature with more money" and applied for a grant from the Belgian government for financial support, submitting a script that Jane Clarke described as portraying "a rigorous regimen   around food&nbsp;... and routine bought sex in the afternoon". This script would only be the rough basis for Jeanne Dielman, 23 quai du Commerce, 1080 Bruxelles because after Akerman received the government grant of $120,000 and began production, she threw the script out and began a new film instead. Wakeman, John. World Film Directors, Volume 2. The H. W. Wilson Company. 1988. pp. 4, 5.  Akerman also explained that she was able to make a female-centric film because "at that point everybody was talking about women" and that it was "the right time". 

Shooting Jeanne Dielman, 23 quai du Commerce, 1080 Bruxelles took five weeks and Akerman called it "a love film for my mother. It gives recognition to that kind of woman".  Akerman used an all female crew for the film, which she later said "didnt work that well - not because they were women but because I didnt choose them. It was enough just to be a woman to work on my film&nbsp;... so the shooting was awful". Akerman further explained that "a hierarchy of images" that places a car accident or a kiss "higher in the hierarchy than washing up&nbsp;... And its not by accident, but relates to the place of woman in the social hierarchy&nbsp;... Womans work comes out of oppression and whatever comes out of oppression is more interesting. You have to be definite. You have to be". 

The film depicts the life of Jeanne Dielman in real time, which Akerman said "was the only way to shoot the film - to avoid cutting the action in a hundred pieces, to avoid cutting the action in a hundred places, to look carefully and to be respectful. the framing was meant to respect her space, her, and her gestures within it".  The long static shots ensure that the viewer "always knows where I am."   

==Reception==
Jeanne Dielman, 23 quai du Commerce, 1080 Bruxelles premiered at the Directors Fortnight at the 1975 Cannes Film Festival and was financially successful in Europe. Writer Peter Handke and filmmaker Alain Tanner have cited it as influential on their work. It was not released in the United States until 1983. 

Film critic John Coleman said that "the films time span covers Tuesday (stew and potatoes), Wednesday (wiener schnitzel) and heady Thursday (meat loaf and Jeanne has an orgasm and kills her client with a pair of scissors). This orgasm bit is bound to strike the serious-minded as an unfortunate bow of crass commercialism".  Jonathan Rosenbaum defended the film and said that it "needs its running time, for its subject is an epic one, and the overall sweep&nbsp;... trains one to recognize and respond to fluctuations and nuances. If a radical cinema is something that goes to the roots of experience, this is at the very least a film that shows where and how some of these roots are buried".  Critic Gary Indiana said that "Akermans brilliance is her ability to keep the viewer fascinated by everything normally left out of movies". 

Feminist film critics immediately praised the film. B. Ruby Rich said that "never before was the materiality of womans time in the home rendered so viscerally&nbsp;... She invents a new language capable of transmitting truths previously unspoken".  Marsha Kidder called it "the best feature that I have ever seen made by a woman".  Akerman was reluctant to be seen as a feminist filmmaker, stating that "I dont think womans cinema exists". 

==References==
 

== External links ==
*   at The Criterion Collection
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 