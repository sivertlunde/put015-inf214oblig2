Snehana Preethina
{{Infobox film name           = Snehana Preethina image          = director       = Shahuraj Shindhe producer       = Anaji Nagaraj writer         = Rajeev Koul Praful Parekh screenplay     = Shahuraj Shinde based on       =   starring  Darshan  Aditya   Sindhu Tolani   Lakshmi Rai music          = V. Harikrishna cinematography = Anaji Nagaraj  editing        = T. Shashikumar studio         = Namana Films distributor    = released       =   runtime        = country        = India language  Kannada
}}
 Darshan and Aditya in lead roles along with Sindhu Tolani and Lakshmi Rai. The film is a remake of 1997 Bollywood film Ishq (1997 film)|Ishq starring Aamir Khan, Ajay Devgan, Juhi Chawla and Kajol in leading roles      

==Cast== Darshan
* Aditya
* Sindhu Tolani
* Lakshmi Rai
* Jennifer Kotwal 
* Rangayana Raghu
* Rajeev
* Sadhu Kokila
* Abhijeeth
* Mukhyamantri Chandru
* Doddanna
* Sundar Raj
* Tennis Krishna
* Sathyajith
* Ramesh Bhat
* Siddarth
* Gazar Khan

==Soundtrack==
{{Infobox album
| Name        = Snehana Preethina
| Type        = Soundtrack
| Artist      = V. Harikrishna
| Cover       = Kannada film Snehana Preethina album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 13 July 2007
| Recorded    =  Feature film soundtrack
| Length      = 28:19
| Label       = Jhankar Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

V. Harikrishna composed music for the soundtracks and the lyrics were written by V. Nagendra Prasad. The album consists of six soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 28:19
| lyrics_credits = yes
| title1 = Dhava Dhava
| lyrics1 = V. Nagendra Prasad
| extra1 = Kunal Ganjawala, Anuradha Sriram
| length1 = 4:52
| title2 = Jagave Barali
| lyrics2 = V. Nagendra Prasad
| extra2 = S. P. Balasubrahmanyam, Srinivas, K. S. Chithra|Chithra, Charan, Janani
| length2 = 4:38
| title3 = Nanna Chanchale
| lyrics3 = V. Nagendra Prasad
| extra3 = S. P. Balasubrahmanyam, Shreya Ghoshal
| length3 = 4:50
| title4 = Osi Osi
| lyrics4 = V. Nagendra Prasad
| extra4 = Karthik (singer)|Karthik, Tippu (singer)|Tippu, Shreya Ghoshal, Anuradha Sriram
| length4 = 4:36
| title5 = Sakku Sakku
| lyrics5 = V. Nagendra Prasad Chaitra
| length5 = 4:38
| title6 = Yaaru Ee Bhoomige
| lyrics6 = V. Nagendra Prasad
| extra6 = Chithra
| length6 = 4:45
}}

==References==
 

==External links==
*  

 
 
 
 
 
 


 