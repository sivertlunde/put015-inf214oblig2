Ndoto Za Elibidi
 

{{Infobox film
| name           = Ndoto Za Elibidi
| image          = 
| alt            =  
| caption        = 
| director       = Kamau Wa Ndungu, Nick Reding
| producer       = S.A.F.E.
| writer         = 
| screenplay     = Kamau Wa Ndungu, Nick Reding
| story          = 
| starring       = Juma Williams, Sharleen Njeri, Mercy Wanjiru, Krysteen Savane, Ednah Daisy
| music          = 
| cinematography = Guy Wilson
| editing        = Carole Gikandi Omondi
| studio         = 
| distributor    = 
| released       =   
| runtime        = 75
| country        = Kenya
| language       = 
| budget         = 
| gross          = 
}}

Ndoto Za Elibidi  is a 2010 Kenyan film.

== Synopsis ==
Ndoto Za Elibidi was devised originally as a stage play for actors from the Nairobi slums. The story pivots around the theme of acceptance and love as its colorful protagonists - parents, four daughters and their lovers - come to terms with HIV and ghetto life. Cutting back and forth from fiction to documentary, from the original stage play to the actual locations, it takes us on two parallel journeys: we watch the story, but we are also watching it through the eyes of the ghetto audience.

== Awards ==
* Zanzíbar 2010
* Kenya 2010

== References ==
 

 
 
 


 