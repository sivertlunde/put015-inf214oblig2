The Busy Barber
{{Infobox Hollywood cartoon|
| cartoon_name = The Busy Barber
| series = Oswald the Lucky Rabbit
| image = Busybarber1932.jpg
| caption = Oswald stealing a tigers tail for a barber pole. Bill Nolan Bill Nolan Fred Avery Bill Weber Jack Carr Charles Hastings
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release_date = September 12, 1932
| color_process = Black and white
| runtime = 6:16 English
| preceded_by = Day Nurse
| followed_by = Carnival Capers
}}

The Busy Barber, also known as Busy Barbers in some reissues, is a short animated film by Walter Lantz Productions, starring Oswald the Lucky Rabbit. It is the 64th Oswald short by Lantz and the 116th in the entire series.

==Plot==
Oswald opens a barber shop but it lacks a barber pole. In this, he goes around to buy one. On his way, he finds a tiger sleeping on the ground. Not wishing to spend money on a real pole, Oswald finds the tigers tail a good substitute. The rabbit then picks up a saw and cuts the tail off the big cat.

Oswald returns to the location of his shop, and stucks the tail on the street just in front. In no time, customers start to come inside. The first one is a bloodhound who comes for a haircut despite not having too much hair. After receiving the service, the bloodhound attempts to leave without paying. This patron, however, gets grabbed by the cash register which appears to have a mind of its own and shakes every cent it could get.

Next to come in is a small fuzzy spaniel. All Oswald wants to is trim the length of its hairs. As a mistake, he shaves too much on the little dogs rear, making it bald in that area. Nevertheless, the spaniel shows little emotion, and therefore leaves the shop quietly.

The third visitor is a rat who doesnt seem to need any service. However, Oswald takes off the rats nose and puts in a lightbulb. To the rats amusement, the rabbit lights up the bulb by inserting the rodents tail into a socket. The last client was a hippo who wants a shave. Although, the hippos beard keeps growing back quickly, Oswald completes the task in three different methods.

Things are going well for Oswald until the tiger, which awakened already, walks into the shop. The tiger is pretty annoyed about what it lost earlier and is seeking vengeance. When the rabbit tries to flee, the big cat nabs and swallows him, although Oswald is able to crawl his way out. Understanding the tigers disgruntlement, Oswald finally picks up the tail and puts it back on the striped beast. The tiger is then overjoyed and runs off peacefully, no longer wanting to wreak havoc. Relieved of his trouble as well as feeling happy for the big cat, Oswald simply laughs.

==See also==
*Oswald the Lucky Rabbit filmography

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 


 