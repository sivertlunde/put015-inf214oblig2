Choothaattam
{{Infobox film 
| name           = Choothaattam
| image          =
| caption        =
| director       = K Sukumaran Nair
| producer       = TMN Charley
| writer         = P Ayyaneth
| screenplay     =
| starring       = Prem Nazir Jayabharathi Jose Prakash Prameela Shyam
| cinematography = N Karthikeyan
| editing        = G Venkittaraman
| studio         = Ramla Productions
| distributor    = Ramla Productions
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by K Sukumaran Nair and produced by TMN Charley. The film stars Prem Nazir, Jayabharathi, Jose Prakash and Prameela in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Prem Nazir
*Jayabharathi
*Jose Prakash
*Prameela
*Sathaar
*Achankunju
*Kuthiravattam Pappu Ravikumar

==Soundtrack== Shyam and lyrics was written by Chunakkara Ramankutty. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Koottilirunnu paattukal paadam || S Janaki, Ambili || Chunakkara Ramankutty || 
|-
| 2 || Maadaka lahari pathanju || P Jayachandran, Lathika || Chunakkara Ramankutty || 
|-
| 3 || Poovine chumbikkum || Ambili, Sreekanth || Chunakkara Ramankutty || 
|-
| 4 || Vaaridhiyil thira pole || K. J. Yesudas || Chunakkara Ramankutty || 
|}

==References==
 

==External links==
*  

 
 
 

 