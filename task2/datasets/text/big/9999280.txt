Plaza de Almas
{{Infobox film
| name           = Plaza de almas
| image          = Plaza de-almas poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Fernando Díaz
| producer       = Daniel Burman Fernando Díaz Diego Dubcovsky
| writer         = Fernando Díaz
| narrator       =
| starring       = Alejandro Gance Olga Zubarry Norman Briski Vera Fogwill
| music          = Luis Robinson
| cinematography = Abel Peñalba
| editing        = Luis César DAngiolillo
| distributor    = BD Cine
| released       =  
| runtime        = 100 minutes
| country        = Argentina Spanish
| budget         =
}} Argentine drama film, written directed by Fernando Díaz. 
The picture was produced by Daniel Burman, Fernando Díaz, and Diego Dubcovsky.
It stars Olga Zubarry, Norman Briski, Vera Fogwill, and others.

==Plot==
Marcelo makes a living as a painter in a Buenos Aires square, with other street artists.
Hes sad and lonely because his familys does not get along.
He lives with his grandfather and becomes romantically involved with a budding actress. He devotes much of his time to her, and dreams of a happy future together.

She, however, has other plans, and events take a dramatic turn when she is forced to undergo an abortion.
Marcelo also discovers the reasons for his familys separation and makes Marcelo face reality more clearly.

==Cast==
* Alejandro Gance as Marcelo
* Olga Zubarry as grandmother
* Norman Briski as grandfather
* Vera Fogwill as Laura
* Thelma Biral as mother
* Villanueva Cosse as the director
* Roberto Carnaghi
* Rolly Serrano
* Guadalupe Martínez Uría
* María L. Cali
* Maximiliano Ghione

==Distribution==
The film was first presented at the Mar del Plata Film Festival in November 1997.  It opened wide in Argentina on June 4, 1998.

In the United States it screened at the Miami Hispanic Film Festival on April 24, 1999.

The picture was shown at various film festivals, including: the Biarritz International Festival of Latin American Cinema, France; the Gramado Film Festival, Gramado, Brazil; and others.

==Awards==
Wins
* Mar del Plata Film Festival: Best Ibero-American Film, Fernando Díaz; 1997.
* Biarritz International Festival of Latin American Cinema: Audience Award; Fernando Díaz; 1998.
* Argentine Film Critics Association Awards: Silver Condor; Best Supporting Actress, Olga Zubarry; 1999.
* Lleida Latin-American Film Festival: ICCI Screenplay Award Fernando Díaz; 1999.

Nominations
* Argentine Film Critics Association Awards: Silver Condor, Best New Actor, Alejandro Gancé; Best Supporting Actor, Norman Briski; 1999.
* Gramado Film Festival, Gramado, Brazil: Golden Kikito, Best Film, Fernando Díaz; 1998.

==References==
 

==External links==
*  
*   at the cinenacional.com  
*  

 
 
 
 
 
 


 