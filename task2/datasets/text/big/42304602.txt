Kung Fu Man (film)
{{Infobox film name       = Kung Fu Man image      = Kung Fu Man film poster.jpg caption    =  traditional = 功夫俠 simplified = 功夫侠 pinyin     = Gōngfū Xiá }} director   = Yuen Cheung-yan Ning Ying producer   = Han Sanping Li Shaohong Jimmy Lui Li Xiaowan Zhang Daxing screenplay = Tiger Chen Wang Qian Zhang Daxing based on   =  starring   = Tiger Chen Jiang Mengjie Arman Darbo Chyna Mccoy Vanessa Branch music      =  cinematography = Sean ODea editing    = Li Tung-Chuen Zhou Ying studio     = Beijing Rongxinda Film and Television art co., LTD China Film Group Corporation
|distributor= Inlook Media China Film Group Corp Beijing Film Studio released   =   runtime    = 90 minutes country    = China United States language   = Mandarin English budget     =  gross      = 
}}
Kung Fu Man also known as Kung Fu Hero is a 2012 Chinese-American Kung Fu film. This film was directed by Yuen Cheung-yan and Ning Ying and produced by Keanu Reeves,   and starring Tiger Chen, Jiang Mengjie, Arman Darbo, Chyna Mccoy and Vanessa Branch.   

==Plot==
It tells the story of the Chinese Kung Fu Man Chen Ping who protects a young boy named "Christophe" from his kidnappers.

== Cast ==
* Tiger Chen as Chen Ping, the Kung Fu Man.
* Jiang Mengjie as Liu Jie, an English teacher in a local school.
* Arman Darbo as Christophe, the boy who was kidnapped.
* Yuen Cheung-yan
* Vanessa Branch as the main members of the kidnapping group. 
* Igor Darbo as the main members of the kidnapping group.
* Andre McCoy as the main members of the kidnapping group.
* Lin Shen as Liu Jies boyfriend.

==Production==
The film shot the scene in Dali City, Yunnan Province, China.  

The film fared poorly at the box office.  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 