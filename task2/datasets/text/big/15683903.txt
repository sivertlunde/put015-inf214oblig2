The New Exploits of Elaine
 
{{Infobox film
| name           = The New Exploits of Elaine
| image          = Pearl White The New Exploits of Elaine.jpg
| caption        = Screenshot of Pearl White
| director       = Louis J. Gasnier Leopold Wharton Theodore Wharton
| producer       = Leopold Wharton Theodore Wharton
| writer         = Arthur B. Reeve
| starring       = Pearl White Creighton Hale
| music          =
| cinematography =
| editing        = Wharton Studios
| released       =  
| runtime        = 10 episodes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}} action film serial directed by Louis J. Gasnier, Leopold Wharton and Theodore Wharton.    It is presumed to be Lost film|lost. 

==Cast==
* Pearl White as Elaine Dodge
* Creighton Hale as Walter Jameson
* Arnold Daly as Detective Craig Kennedy Washington and wrote America, through the spectacles of an Oriental diplomat (excerpted in The New York Times, 22 March 1914). This was published less than a year before Arthur B. Reeve created the villain character. {{Cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | pages = 39
 | chapter = 2. The Perils of Success
 }} 
* M.W. Rale as Long Sin. Long Sin, the Yellow Peril character from the first serial, became just an agent of Wu Fang in this serial. 
* Bessie Wharton as Aunt Josephine
* Gazelle Marche as Innocent Inez
* Ah Ling Foo as a Chinese Heavy
* Howard Cody

==Chapter titles==
# The Serpent Sign
# The Cryptic Ring
# The Watching Eye
# The Vengeance of Wu Fang
# The Saving Circles
# Spontaneous Combustion
# The Ear In The Wall
# The Opium Smugglers
# The Tell-Tale Heart
# Shadows of War

==See also==
*List of lost films
*List of partially lost films

==References==
 

==External links==
 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 
 