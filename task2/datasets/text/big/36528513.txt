Lilies of the Field (1924 film)
{{Infobox film
| name           = Lilies of the Field
| image          =File:Lilies of the Field poster.jpg
| caption        =Film poster John Francis Dillon
| producer       = Corinne Griffith
| writer         = William J. Hurlbut (play: Lilies of the Field) Marion Fairfax (writer) Adelaide Heilbron (writer)
| starring       = Corinne Griffith
| music          = 
| cinematography = James Van Trees
| editing        = Arthur Tavares
| distributor    = Associated First National (*later First National Pictures)
| released       =  
| runtime        = 90 minutes; 9 reels
| country        = United States
| language       = Silent (English intertitles)
}} lost  Associated First sound film in 1930.  

A trailer to this film exists at the Library of Congress.

==Cast==
*Corinne Griffith - Mildred Harker
*Conway Tearle - Louis Willing
*Alma Bennett - Doris
*Sylvia Breamer - Vera
*Myrtle Stedman - Mazie
*Crauford Kent - Walter Harker Charles Murray - Charles Lee
*Phyllis Haver - Gertrude
*Cissy Fitzgerald - Florette
*Edith Ransom - Amy
*Charles K. Gerrard - Ted Conroy(*as Charles Gerrard)
*Dorothy Brock - Rose
*Mammy Peters - Mammy
*Anna May Wong - uncredited role

==References==
 

==External links==
*  
* 
* 

 

 
 
 
 
 
 
 
 
 


 