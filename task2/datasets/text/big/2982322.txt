To Gillian on Her 37th Birthday
{{Infobox film
| name           = To Gillian on Her 37th Birthday
| image          = Thirty seventh birthday.jpg
| caption        = Theatrical release poster
| director       = Michael Pressman
| producer       = Marykay Powell David E. Kelley
| screenplay     = David E. Kelley
| based on       =  
| starring       = Peter Gallagher Claire Danes Kathy Baker Wendy Crewson Bruce Altman Michelle Pfeiffer
| music          = James Horner
| cinematography = Tim Suhrstedt
| editing        = William Scharf
| studio         = Rastar
| distributor    = Triumph Films
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| gross          = $4,137,645 (USA)
}}
To Gillian on Her 37th Birthday is a 1996 American romantic drama film directed by Michael Pressman, and starring Peter Gallagher and Claire Danes as a father and daughter struggling to come to terms with the tragic death of wife and mother Gillian (Michelle Pfeiffer). The original score was composed by James Horner.
 play of the same name by Michael Brady.

==Plot==
David Lewis (Peter Gallagher) is so affected by the death of his beautiful wife Gillian (Michelle Pfeiffer), who fell from the mast pole of their boat on a sailing trip, that he turns their summer cottage in Nantucket, Massachusetts into a permanent home and spends most of his time on the beach there, communicating with his dead wifes spirit and unwittingly neglecting his teenage daughter Rachel (Claire Danes).

On the second anniversary of Gillians death, David invites Gillians sister Esther Wheeler (Kathy Baker) and her husband Paul (Bruce Altman) to stay for the weekend. Esther insists on bringing a female friend named Kevin Dollof (Wendy Crewson) whom she hopes David will become romantically interested in. David, however, ignores her in proceeding with a ritualistic celebration of Gillians birthday.

The events of the weekend cause the adults to re-examine their relationships; Esther and Paul have to deal with the problem posed to their marriage by Rachels provocative young teenage friend, while, most importantly, David comes to realize that he can be a loving and attentive father to Rachel without betraying the memory of Gillian.

==Cast==
* Peter Gallagher as David Lewis
* Michelle Pfeiffer as Gillian Lewis
* Claire Danes as Rachel Lewis
*Laurie Fortier as Cindy Bayles
* Wendy Crewson as Kevin Dollof
* Bruce Altman as Paul Wheeler
* Kathy Baker as Esther Wheeler
* Freddie Prinze, Jr. as Joey Bost
*Rachel Seidman-Lockamy as Megan Weeks
* Seth Green as Danny

==Production==
To Gillian on Her 37th Birthday was filmed on location in Nantucket, Massachusetts,  although some of the beach scenes were filmed at Long Beach, California and the sailing accident was filmed at Marina del Rey, California; the obvious differences between Massachusetts and California were noticeable enough to be mentioned by the New York Times reviewer. 

==Reception==
To Gillian on Her 37th Birthday currently holds a rating of 15% on Rotten Tomatoes, indicating an overwhelmingly negative critical response. 

  wrote that, "for all the moonlight and magic, the film scares up little in the way of enchantment."   

Many critics found it difficult to accept the basic premise, that the main characters continued mourning of his deceased wife was so detrimental to those around him.   wrote: "Despite its apparent parallels to Ghost, Gillian takes an entirely opposite path. Throughout Ghost, we were made to feel desperate for a reunion of Patrick Swayzes roaming spirit with a mourning Demi Moore. In Gillian, the whole purpose is to get David to give up the ghost."    Mick LaSalle in the San Francisco Chronicle thought the film lacked dramatic impetus: "the grieving husband never quite seems crazy enough - and the sister is never angry enough... Drama is avoided. Issues are muddy. And everyone stays a nice person... In fact, typical of the films undramatic choices - its ungenerous unwillingness to commit to the extreme - the husband knows she   is an illusion. So hes not crazy. Shes not a ghost. And the sister-in-law, far from evil, is merely concerned. So why are we watching these people?" 

One aspect of the film that garnered unanimous praise was the performance of Claire Danes as the troubled daughter. Variety wrote: "Danes proves again that shes one of the most naturally gifted actresses of her generation."  The New York Times described her as "especially expressive in the films later scenes, demonstrating a rare ability to seem fresh and honest when her material quite clearly is not."  The Los Angeles Times wrote: "Danes is terrific playing an awkward teenager trying to understand her fathers problems while feeling the first stirrings of passion in herself."  According to the Washington Post, "the gifted actress steals the show." 

==Awards and nominations==
Claire Danes won a Young Artist Award for Best Performance in a Feature Film - Supporting Young Actress. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 