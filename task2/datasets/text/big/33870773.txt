Parie
 
{{Infobox film
| name = Parie
| image = 
| caption = 
| director = Sudhir Attavar
| presenters = Trivikrama, Lingappa Sandur
| producer =  TRIVIKRAMA BELTANGADI,LINAPPA SANDOOR,Arun tumati, Chandra Sindhogi, Divija K, Gowda MC, Mogan Babu, Nithyananda N, Ramakrishna Bhatt, Rajshekar Mangalgi, 
| writer = Sampanna Mutalik
| dialogue writer = Vijay Bharamasagar
| starring = Rakesh Adiga Naga Kiran Nivedhitha
| music = Veer Samarth
| art director =M. S. Sathyu
| cinematography = Ananth Urs
| editing = Vidyadhar Shetty
| background score = 
| distributor =  
| released =  
| runtime = 
| country = India
| language = Kannada
| budget = 
| gross = 
}} Kannada romance film starring Rakesh Adiga and Nivedhitha in the lead roles. The film is directed by Sudhir Attavar. Veer Samarth is the music director of the film. In total, there are 7 producers for this movie which makes it special.The other highlight of the movie is Veterans M. S. Sathyu and Nimay Ghosh are roped in for art direction and cinematography respectively. The film made its theatrical release on 27 April 2012. 

== Cast ==
* Rakesh Adiga
* Nivedhitha
* Usha Uthup
* Naga Kiran
* Harshika Poonacha
* Vikram Udayakumar
* Sharath Lohitashwa
* Srinivas Prabhu
* Hemangini Kaj as cameo appearance

==Premier Screening at Mumbai==
The film, for the first time in Kannada film industry, had a special pre-release premier show held at Mumbai on April 15, 2012. Arranged and hosted by M. S. Sathyu and the director Sudhir Attavar, the venue was at Cinemax Multiplex in Andheri. It was attended by well-known personalities like: Udit Narayan, Sadhana Sargam, Sushant Singh, Akhilendra Mishra, Hemangini, Nivedita, Rakesh Adiga, Sathya, Rageshwari Sachdev, Varun Badola, Naga Kiran, Ranjeet, Raman Kumar and many others. 

==Soundtrack==

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Mugilina Maathu
| lyrics1 	= Sudhir Attavar
| extra1        = Udit Narayan, Sadhana Sargam
| length1       = 
| title2        = Mirugutide Yedeyolage
| lyrics2 	= Sudhir Attavar
| extra2        = Shaan (singer)|Shaan, Gayatri Iyer
| length2       = 
| title3        = Kandikeri Hudugaranna
| extra3        = Priya Himesh
| lyrics3 	= Sudhir Attavar
| length3       = 
| title4        = Ninna Premada Pariya
| extra4        = S. P. Balasubramanyam
| lyrics4 	= K S Narasimha swamy
| length4       = 
| title5        = Ashada Kaledaithe
| extra5        = B. K. Sumitra, Manikka Vinayagam, Mysore Janni, Samanvitha
| lyrics5 	= Sudhir Attavar
| length5       = 
| title6        = Jhoom Jhoom Zara
| extra6        = Usha Uthup
| lyrics6 	= Sudhir Attavar
| length6       = 
}}
 

==References==
 

 
 
 
 


 