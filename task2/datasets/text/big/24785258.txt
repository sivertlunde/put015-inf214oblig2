Tinker Bell and the Lost Treasure
{{Infobox film
| name           = Tinker Bell and the Lost Treasure
| image          = Tinker_bell_and_the_lost_treasure_filmposter.jpg
| image_size     =
| caption        = DVD Cover
| director       = Klay Hall
| producer       = Sean Lurie
| writer         = Evan Spiliotopoulos
| narrator       =
| starring       = Mae Whitman Jesse McCartney Raven-Symoné Lucy Liu Kristin Chenoweth Angela Bartys Anjelica Huston
| music          = Joel McNeely
| cinematography =
| editing        = Jeremy Milton
| studio         = DisneyToon Studios Prana Studios Walt Disney Walt Disney Studios Motion Pictures  (International) 
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $30—$35 million   
| gross          = $8,582,265  
}}
 Tinker Bell, and revolves around Tinker Bell, a fairy character created by J. M. Barrie in his play Peter Pan, or The Boy Who Wouldnt Grow Up, and featured in subsequent adaptations, especially in Disneys animated works. The film was produced using digital 3D modeling. It was released on Blu-ray and DVD by Walt Disney Studios Home Entertainment on October 27, 2009.   

==Plot==
The  . Meanwhile, Tinker Bell (Mae Whitman) and dust-talent fairies like Terence (Jesse McCartney) are staying in Pixie Hollow. Tinker Bell is trying to make a "Pixie Express". But it fails just as she is called to meet Queen Clarion (Anjelica Huston), Fairy Mary (Jane Horrocks), and Redleaf, the Minister of Autumn (John DiMaggio).
 blue harvest moon in Pixie Hollow. When the light of this rare blue moon passes through the moonstone, it creates blue-colored pixie dust to strengthen and rejuvenate the pixie dust tree. The Autumn Revelry is the associated event during which the fairies gather to collect the dust.

A new scepter is to be made to raise the moonstone, and Tinker Bell has been recommended. Although Tinker Bell has made mistakes in the past, Fairy Mary explains that tinker fairies learn from them, most of the time. Tinker Bell accepts the task, as well as help from Terence. But as the work on the scepter progresses, Tinker Bell begins to have trouble with Terence, who is trying too hard to be helpful. When Tinkerbell asks Terence to go find something sharp, Terence brings a compass to her workshop, irritating Tinkerbell. She kicks the compass, causing it to roll over and break her newly completed scepter. After a row with her friend, Tinker Bells furious antics result in her accidentally smashing the moonstone as well. Tinker Bell sets out on in a balloon shes created to find a magic mirror, which, according to legend, granted two of three wishes before becoming lost. Tinker Bell intends to use the third and last wish to repair the shattered moonstone to its original form.

While trying to evade a hungry bat, a firefly named Blaze crash lands into Tinker Bells balloon. Tink orders him to leave, but he truly wants to tag along with Tink on her quest to find the magical mirror. After Blazes apparent exit, Tink tries to read her map but its too dark to see. Blaze then sheds light on the map to help Tink, and the tinker finally allows him to stay. As the duos adventure continues, Tink thinks she has stumbled upon the stone arch that is said to lead way to the mirror. She leaves the balloon to make sure of this and leaves Blaze to watch over it. Once Tink flies off, however, the balloon begins to stray away. After unsuccessfully trying to anchor it, Blaze rushes to tell Tink, though she is too busy trying to figure out why she stumbled upon a bent tree instead of the stone ark to notice Blaze. When she finally sees the balloon floating off, she gives chase, Blaze in tow, but the harsh winds knocks them down. The next morning, Tinker Bell awakens, hungry and lost.

Blaze scouts out to rally some forest insects that provide food and water for Tink. They also lead her and Blaze to the stone arch, and the adventure continues. They find the shipwreck that is said to house the mirror and head inside Tinker Bell finally discovers the mirror. Just as she is about to make the wish, Blaze keeps getting in her face, causing her to blurt out her wish for the firefly to be quiet for one minute, accidentally wasting her third wish. She blames Blaze for distracting her, but then, realizing that her temper is what had gotten her in trouble in the first place, she apologizes and breaks down crying. She is found by Terence, who has been following her after discovering her plans and the fragments of the moonstone in her empty house. They reunite, but then they are chased by rats.

Tinker Bell and Terence start back to Pixie Hollow. Along the way, Tinker Bell fixes the scepter using a white gem from the top of the mirror, the scepter pieces Terence has wisely brought, and the moonstone pieces, all set at just the right angle. She discovers the magic of true friendship, humility, and love. Thanks to inspired teamwork with Terence, she is ready to give the scepter to Queen Clarion.

When she unveils the scepter, the assembled fairies are all shocked and alarmed to see the fragments of the precious moonstone. However, the broken moonstone shards create an unexpected benefit: they drastically magnify and increase the surface area through which the rays of the blue moon could pass, creating the largest supply of blue-colored pixie dust ever seen in Pixie Hollow.

==Cast==
The voice actors and actresses are largely the same as in the previous film.    America Ferrera did not return to voice Fawn and was replaced by newcomer Angela Bartys.

* Mae Whitman as Tinker Bell, a tinker fairy
* Jesse McCartney as Terence, the pixie-dust keeper
* Jane Horrocks as Fairy Mary, the overseer of all tinker fairies
* Lucy Liu as Silvermist, a water fairy
* Raven-Symoné as Iridessa, a light fairy
* Kristin Chenoweth as Rosetta, a garden fairy
* Angela Bartys as Fawn, an animal fairy
* Rob Paulsen as Bobble, a wispy tinker fairy with large glasses / Grimsley, a tall troll / Mr. Owl
* Jeff Bennett as Clank, a large tinker fairy with a booming voice / Fairy Gary, the overseer of the pixie-dust keepers / Leech, a short troll
* Grey DeLisle as Lyria, a storytelling fairy / Viola, a summoning fairy / Narrator
* John DiMaggio as Redleaf, the Minister of Autumn
* Eliza Pollack Zebert as Blaze, a firefly
* Bob Bergen as Bugs / Creatures
* Roger Craig Smith as Bolt, a pixie-dust keeper / Stone, a pixie-dust keeper
* Allison Roth as French Fairy
* Thom Adcox-Hernandez as Flint, a pixie-dust keeper
* Anjelica Huston as Queen Clarion, the queen of all Pixie Hollow

* Vidia a fast flying fairy appeared in the beginning of the movie and the end of the movie but had no lines.

==Crew==
* Director - Klay Hall
* Writer - Evan Spiliotopoulos 

==Production==
Because the film takes place in the cooler weather of autumn, costume design for Tinker Bell called for a more realistic outfit. Designers added a long-sleeve shirt, shawl, leggings and boots to her costume. Said director Klay Hall, "In the earlier films, she wears her iconic little green dress. However, it being fall and there being crispness in the air, in addition to this being an adventure movie, her dress just wouldnt work". 

==Music==
The score to the film was composed by Joel McNeely, who scored the first Tinker Bell film. He recorded the music with an 82-piece ensemble of the Hollywood Studio Symphony and Celtic violin soloist Máiréad Nesbitt at the Sony Scoring Stage. 

===Gift of a Friend===
{{Infobox song 
| Name           = Gift of a Friend
| Cover          = 
| Border         = yes
| Artist         = Demi Lovato Here We Go Again
| Type           = Promotional single
| Released       =   Digital download
| B-side         = 
| Recorded       = 2009
| Length         = 3:25
| Genre          = Pop rock, teen pop Adam Watts, Andy Dodd, Demi Lovato
| Producer       = Adam Watts, Andy Dodd
| Misc          =  |Type=song}}
}} Here We Go Again.

===Soundtrack===
The soundtrack was released on September 22, 2009 and contains songs from and inspired by the film. The soundtrack also contains "Fly to Your Heart" from the first film.  The lead single from the soundtrack is "Gift of a Friend" by Demi Lovato.

#"Gift of a Friend" - Demi Lovato
#"Take to the Sky" - Jordan Pruitt
#"Where the Sunbeams Play" - Méav Ní Mhaolchatha
#"Road to Paradise" - Jordin Sparks
#"Ill Try" - Jesse McCartney
#"If You Believe" - Lisa Kelly
#"Magic Mirror" - Tiffany Thornton
#"The Magic of a Friend" - Hayley Orrantia
#"Its Love That Holds Your Hand" - Jonatha Brooke
#"A Greater Treasure Than a Friend" - Savannah Outen
#"Pixie Dust" - Ruby Summer
#"Fly Away Home" - Alyson Stoner
#"Fly to Your Heart" - Selena Gomez
 You Were..." was chosen as the theme song for the Japanese language version of the movie. 

===Score===

Intrada Records released an album of Joel McNeelys score on February 2, 2015 through the labels co-branding arrangement with Walt Disney Records. Unlike the first movie, none of McNeelys score has been previously released.

# Tapestry
# If You Believe/Main Title - Lisa Kelly
# Pixie Dust Factory
# Where Are You Off To?
# Pixie Dust Express
# The Hall of Scepters
# Maybe I Can Help
# The Fireworks Launcher
# The Finishing Touch/I Had a Fight with Tink
# Fairy Tale Theatre - Grey DeLisle and Julie Garnyé
# Tink Sails Away
# Tink Tries for More Pixie Dust
# Im On My Own
# Sailing Further North
# Blaze the Stowaway
# Ill Take First Watch
# The Lost Island
# Tink Finds the Arch
# Troll Bridge Toll Bridge
# The Ship That Sunk
# Searching the Ship
# They Find the Mirror of Encanta
# I Was Wrong
# Rat Attack
# I Cant Do This Without You
# Presenting the Autumn Scepter
# Our Finest Revelry Ever
# If You Believe, Part 2 - Lisa Kelly
# The Gift of a Friend - Demi Lovato
# Where the Sunbeams Play - Méav Ni Mhalchatha

===Chart performance===
{| class="wikitable sortable"
! scope="col" | Chart (2009)
! scope="col" | Peak position
|- US Kid Digital Songs (Billboard (magazine)|Billboard) 
| style="text-align:center;"| 8
|}

==Release==
The film premiered at the United Nations Headquarters on October 25, 2009. Kiyotaka Akasaka, Under-Secretary-General for Communications and Public Information, named Tinker Bell the "honorary Ambassador of Green" to help promote environmental awareness among children.  

The film was released on DVD and Blu-ray by Walt Disney Home Video in the United States on October 27, 2009.  and in the United Kingdom on November 16, 2009.  It debuted on the Disney Channel on November 29, 2009. In its first two months of release, DVD sales brought in about $50 million in revenue for 3.25 million units sold. 

==Video game==
{{Infobox video game
| title = Disney Fairies: Tinker Bell and the Lost Treasure
| caption = 
| image =  
| developer = EA Bright Light Studio
| publisher = Disney Interactive EA Distribution
| designer = 
| series = 
| engine = 
| released = October 26, 2009
| genre = Adventure
| modes = Single-player, multiplayer
| platforms = Nintendo DS
| media = DS Game Card
}} Tinker Bell in a free-roaming Pixie Hollow, using the touch screen to maneuver the character, move to other maps and play various minigames. The player must, for example, touch an arrow on the screen to move to another map or characters to speak to them. The touch screen is used in the item repair minigames as well. For example, the player must trace the pattern of a groove to clear it or rub the item to clean stains. The DS microphone is used to create wind to loosen leaves and petals or blow dust from an item being repaired. The highest rank on Tinker bell is Champion of the Craft.

Different gameplay mechanics can also be acquired in-game, which require specific use of the touch screen. These include:

*the ability to glow by holding the stylus directly above Tinker Bell. This can be used to reveal hidden items.

*drawing a circle on-screen to perform a somersault. Used to collect falling items.

*drawing a triangular shape on-screen to awaken plants throughout the game.

*petting or tickling insects. Used to collect lost insects and awaken sleeping insects. Can also be used on random insects that roam about the maps. Items will be awarded.

Also present in the game is a "Friendship Meter", which serves as an indicator to measure the players relationship with other characters. It can be filled by presenting the respective character with their favorite item, accomplishing tasks or even simply speaking to them. The meter can also be depleted, however, by not speaking to the character for extended periods of time, giving an unwanted gift or missing a repair deadline.

Features:

*Create unique dresses, outfits and accessories

*Mini-games, such as catching dew drops, painting ladybugs and colleting threads from sleeping silkworms

*Multiplayer modes

*DGamer functionality

*Pixie Hollow integration

==Other media==
A 32-page interactive digital childrens book was released by Disney Digital Books in September 2009. 

==Additional sequels==
 
Four additional sequels titled Tinker Bell and the Great Fairy Rescue,   Pixie Hollow Games, Secret of the Wings, and The Pirate Fairy    have all been released, while one additional sequel Tinker Bell and the Legend of the Neverbeast, was released in Spring 2015. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 