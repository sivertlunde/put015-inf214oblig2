Song of Arizona
{{Infobox film
| name           = Song of Arizona
| image          = Poster of the movie Song of Arizona.jpg
| image_size     =
| caption        = Frank McDonald
| producer       = Edward J. White (associate producer)
| writer         = Bradford Ropes (story) M. Coates Webster (writer)
| narrator       =
| starring       = See below
| music          = R. Dale Butts Joseph Dubin Mort Glickman
| cinematography = Reggie Lanning Arthur Roberts
| distributor    =
| released       =  
| runtime        = 68 minutes (original version) 54 minutes (edited version)
| country        = United States English
| budget         =
| gross          =
| website        =
}}
 1946 United American Western Western film Frank McDonald and starring Roy Rogers.

== Cast ==
*Roy Rogers as Roy Rogers Trigger as Himself - Roys Horse
*George "Gabby" Hayes as Gabby Whittaker
*Dale Evans as Clare Summers
*Lyle Talbot as King Blaine Tommy Cook as Chip Blaine
*Johnny Calkins as Clarence Sarah Edwards as Dolly Finnuccin
*Tommy Ivo as Jimmy
*Michael Chapin as Cyclops
*Dick Curtis as Henchman Bart
*Edmund Cobb as Sheriff Jim Clark Tom Quinn as Henchman Tom
*Noble Kid Chissel as Henchman Jim
*The Robert Mitchell Boy Choir as Boys at ranch
*Bob Nolan as Ranch hand
*Sons of the Pioneers as Ranch hands / Musicians

== Soundtrack == John Elliott as Jack Elliott)
*Roy Rogers with the Robert Mitchell Boys Choir - "When a Fellow Needs a Friend"
*"Michael OLeary OBrien OToole" (Written by Gordon Forster) James Cavanaugh)
*"Round and Around - The Lariat Song" (Written by John Elliott as Jack Elliott)
*"Way Out There" (Words and Music by Bob Nolan)
*"Will You Be My Darling" (Written by Mary Ann Owens)
*"Half a Chance Ranch" (Written by John Elliott as Jack Elliott)
*"Mr. Spook Steps Out" (Written by John Elliott as Jack Elliott)

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 