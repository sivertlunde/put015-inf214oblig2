Man in the Chair
{{Infobox film
| name           = Man in the Chair
| image          = Man in the chair.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Michael Schroeder
| producer       = Michael Schroeder Sarah Schroeder Randolf Turrow Peter Samuelson Steve Matzkin
| writer         = Michael Schroeder
| narrator       =
| starring       = Christopher Plummer Michael Angarano M. Emmet Walsh Robert Wagner
| music          = Laura Carpman
| cinematography = Dana Gonzales
| editing        = Terry Cafaro
| distributor    = Outsider Pictures
| released       = December 7, 2007
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Michael Schroeder. The film stars Christopher Plummer, Michael Angarano, M. Emmet Walsh, and Robert Wagner.

==Premise==
The drama stars Christopher Plummer as Flash, a man who longs for the days when he worked as a crew member on such cinematic masterpieces as Citizen Kane. When Flash meets teenage film fanatic Cameron Kincaid (played by Michael Angarano), he becomes an unlikely mentor and agrees to help Cameron make a film to compete in a student competition where the top prize is a film school scholarship and, for Cameron, a ticket out of his difficult home life. Flash, who sees his own life drawing to a close recruits the support of his eccentric friends at the Motion Picture home and helps Cameron make his film and chase his dream.

==Cast==
* Christopher Plummer as Flash Madden
* Michael Angarano as Cameron Kincaid
* M. Emmet Walsh as Mickey Hopkins
* Robert Wagner as Taylor Moss

==External links==
* 
* 

 
 
 
 
 
 
 

 
 