Jack Frost (1997 film)
:For the 1998 comedy starring Michael Keaton, see Jack Frost (1998 film).
 
{{Infobox film
 | name = Jack Frost
 | director = Michael Cooney
 | producer = Jeremy Paige Vicki Slotnick
 | writer = Michael Cooney Jeremy Paige Scott MacDonald
 | music = Chris Anderson Carl Schurtz
 | cinematography = Dean Lent
 | editing        = Terry Kelley
 | distributor    = A-Pix Entertainment, Inc.
 | released = November 18, 1997
 | runtime = 89 minutes
 | country = United States
 | language = English
 | image= Jack Frost VideoCover.png
}}
 horror comedy Scott MacDonald) execution crashes into a genetics truck. The genetic material causes Jacks body to mutate and fuse together with the snow on the ground. Jack is presumed dead and his body melts away. However, he comes back as a killer snowman and takes revenge on the man who finally caught him, Sheriff Sam Tiler (Christopher Allport). The film has since become a cult classic.

==Plot== Scott MacDonald), who eluded police for years and left a trail of thirty-eight bodies across five states before finally being arrested, by the sheriff of Snowmonton. Frost is scheduled to be executed at midnight but due to the weather, a tanker containing genetic material collides with the prison truck, freeing Jack. As he tries to escape, the acid contained within the tanker breaks free, melting Jacks skin and skeleton. Shortly after his death, Jacks remains are fused with the snow.

Sheriff Sam Tiler (Christopher Allport) is still haunted by his memories of Jack Frost. Despite news reports of Jacks demise, Sam cannot forget Jacks threats of vengeance. The sheriffs fears soon prove to be founded when Old Man Harper is found murdered. Soon afterwards, a local bully named Billy (Nathan Hague) and his ski patrol gang are also killed. According to the sheriffs son, Ryan, a snowman caused the deaths. Billys parents Jake (Jack Lindine) and Sally (Kelly Jean Peters) are then murdered. Paul Davrow, the sheriffs friend, witnesses the murders, but no one believes him. Sheriff Tiler has his deputies lock him up.

FBI Agents Manners (Stephen Mendel) and Stone (Rob LaBelle) arrive in Snowmonton and convince Sheriff Tiler to put the town on 24 hour curfew, sending his officers out to gather all the townspeople. One of the officers (Brian Leckner) is killed when Jack Frost runs the officer over with a police cruiser. Billys older sister Jill (Shannon Elizabeth) and her boyfriend sneak into the sheriffs home to steal his wine and have sex as payback for her brothers death. Jack kills Jills boyfriend and rapes Jill in the shower, ultimately killing her.
 antifreeze in the oatmeal, believing it could help keep the oatmeal from getting cold.

Sheriff Tiler tells his friend, Paul Davrow (F. William Parker) to fill the bed of his truck with antifreeze. Jack chases Sam through the halls of a church and finally catches him, driving an icicle into his chest and almost killing him. The truck full of antifreeze arrives just in time, however, and Jack and Sam crash through a window and into the trucks bed. Jack Frost melts in the antifreeze, and afterwards, the antifreeze is poured back into the containers, and then buried deep under the ground of Snowmonton. Tilers wife Anne (Eileen Seeley), realizes that the state police are on their way. When Paul asks Tiler what they are going to tell them, Tiler says, "well tell them that its too late". However, one of the containers is shown to be bubbling, suggesting that Jack is contained as a liquid, and is still alive.

==Cast== Scott MacDonald...Jack Frost
* Christopher Allport...Sam Tiler (as Chris Allport)
* Stephen Mendel...Agent Manners
* F. William Parker...Paul Davrow
* Eileen Seeley...Anne Tiler
* Rob LaBelle...Agent Stone
* Zack Eginton...Ryan Tiler
* Jack Lindine...Jake Metzner
* Kelly Jean Peters...Sally Metzner
* Marsha Clark...Marla
* Chip Heller...Deputy Joe Foster
* Brian Leckner...Deputy Chris Pullman
* Darren O. Campbell...Tommy Davrow
* Shannon Elizabeth...Jill Metzner (as Shannon Elizabeth Fadal)
* Paul Keith...Doc Peters
* Charles C. Stevenson Jr...Father Branagh
* Nathan Hague...Billy Metzner

==Location==
Portions of the film were filmed at the Fawn Lodge in Fawnskin, California, on the north west shore of Big Bear Lake.

==Reception== an unrelated family film of the same name was released within one year of it, also featuring the concept of a living snowman. The film is commonly regarded as Camp (style)|so-bad-its-good. The concept of rape by a carrot-wielding snowman has become a classic scene in the film. In addition, the film is also known for being the first movie featuring actress Shannon Elizabeth (credited as Shannon Elizabeth Fadal), who plays a minor role in the movie. It has also become quite a popular mock on I-Mockery.

Three years later, the film would spawn a sequel,  .

==External links==
*  
*  

 

 
 
 
 
 
 
 