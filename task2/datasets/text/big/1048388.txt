Hot Shots!
 
 
{{Infobox film
| name        = Hot Shots!
| image       = Hot_Shots 2.jpg
| caption     = Theatrical release poster
| writer      = Jim Abrahams Pat Proft
| starring    = Charlie Sheen Cary Elwes Valeria Golino Lloyd Bridges Jon Cryer Kevin Dunn Bill Irwin
| director    = Jim Abrahams
| producer    = Bill Badalato Pat Proft
| music       = Sylvester Levay Bill Butler
| editing     = Jane Kurson Eric A. Sears
| distributor = 20th Century Fox
| released    =  
| runtime     = 84 minutes
| country     = United States
| language    = English
| music       = 
| budget      = $26 million   
| gross       = $181,096,164 
}} spoof film starring Charlie Sheen, Cary Elwes, Valeria Golino, Lloyd Bridges, Kevin Dunn, Jon Cryer and Ryan Stiles. It was directed by Jim Abrahams, and was written by Abrahams and Pat Proft.  Carrying the same comedic vein as Airplane!, a film Abrahams co-directed, the film primarily spoofs the 1986 action film Top Gun, but also draws material from films like Rocky and Superman (film)|Superman. It spawned a sequel in 1993, Hot Shots! Part Deux. Sheen and Cryer would later co-star in the television series Two and a Half Men, in which Stiles also went on to play a recurring role.

==Plot summary==
 

Hot Shots has been described as "Top Gun meets Airplane!".  The film begins at Flemner Air Base 20 years prior. A pilot named Leland "Buzz" Harley (Bill Irwin) loses control of his plane and ejects, leaving his co-pilot Dominic "Mailman" Farnum (Ryan Stiles) to crash alone; although Mailman survives, hes mistaken for a deer owing to the branches stuck to his helmet and is shot by a hunter. Topper Harley (Charlie Sheen) wakes up from a nightmare hes having about the event when Lt. Commander Block (Kevin Dunn) asks him to return to active duty as a pilot in the U.S. Navy, to help on a new top secret mission: Operation Sleepy Weasel.
Harley starts to show some psychological problems, especially when his father is mentioned. His therapist, Ramada (Valeria Golino), tries to keep Topper from flying, but he relents, and also starts to build a budding romance with her. Meanwhile, Topper gets into a rivalry with another fighter pilot, Kent Gregory (Cary Elwes), who hates Topper because of the loss of his father "Mailman" to Buzz Harley, and believes Topper may do the same to him.
 William OLeary) and Jim "Wash-Out" Pfaffenbach (Jon Cryer) occurs, leaving Dead Meat dead and Wash Out reassigned to radar operator. Block believes this is enough to convince the Navy to buy new fighters, but Wilson brushes it aside as a "minor incident," and the planes need to fail in combat to take notice. 

Meanwhile, Topper starts to show more and more feelings to Ramada, but she is also smitten with Gregory, who believes Topper cannot handle combat pressure. On the carrier U.S.S. Essess, Block reveals the mission to be an attack of an Iraqi nuclear plant and assigns Topper to lead the mission, much to Gregorys chagrin. Meanwhile, Wilson, who is also on board, coerces a crew member to sabotage the planes, putting the pilots lives at risk. At first, the mission goes according to Blocks plan. He mentions Buzz Harley to Topper, who becomes overcome with emotion and unable to lead the mission. Block just starts to call out for the mission to be aborted when Iraqi fighters attack the squadron. All the planes weapons fail and Block realizes what has happened. He then tells Topper that he saw what really happened with Buzz and Mailman, that Buzz tried to do everything possible to save Mailman, but ended up falling out of the plane, failing in his attempts. Inspired, Topper single-handedly beats the Iraqi fighters and bombs the nuclear plant, despite sustaining heavy damage. Back aboard ship, Block decides that American planes will always be superior with pilots like Topper (and German parts). Wilsons plan is revealed and his standing with the military is lost.

Back in port, Gregory accepts Topper as a great pilot and lets Ramada be with Topper and the two begin a loving new relationship.

==Cast==
* Charlie Sheen as LT Sean "Topper" Harley
* Cary Elwes as LT Kent Gregory
* Valeria Golino as Ramada Thompson
* Lloyd Bridges as RDML Thomas "Tug" Benson
* Kevin Dunn as LCDR James Block
* Jon Cryer as LT Jim "Wash Out" Pfaffenbach William OLeary as LT Pete "Dead Meat" Thompson
* Kristy Swanson as Kowalski
* Efrem Zimbalist, Jr. as Wilson
* Bill Irwin as Leland "Buzz" Harley
* Ryan Stiles as Dominic "Mailman" Farnum
* Heidi Swedberg as Mary Thompson
* Rino Thunder as Owatonna The Old One
* Charles Barkley as Himself
* Bill Laimbeer as Himself

==Reception==

The film debuted at number one. 
 
   Hot Shots was both a critical and commercial success, grossing over $180 million worldwide  and tallying 82% positive reviews on review-collection website Rotten Tomatoes.  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 