The Delicious Little Devil
{{Infobox film
| name           = The Delicious Little Devil
| image_size     =
| image	         = The Delicious Little Devil FilmPoster.jpeg
| caption        =
| director       = Robert Z. Leonard
| producer       =
| writer         = Harvey F. Thew (scenario)
| story          = John B. Clymer Harvey F. Thew
| starring       = Mae Murray Rudolph Valentino William V. Mong
| music          =
| cinematography = Allen Siegler
| editing        =
| distributor    = Universal Film Manufacturing Company
| released       =  
| runtime        = 53 minutes
| country        = United States
| language       = Silent English intertitles
}} silent comedy-drama film starring Mae Murray and Rudolph Valentino. A 35&nbsp;mm print of the film is housed at the EYE Film Instituut Nederland,  Nederlands Filmmuseum.

==Plot== New York tenement and supports her mother and her shiftless father and uncle.
 King of roadhouse this side of Monte Carlo." The Peach Trees ad seeks a female hostess and dancer: "A Good Future For A Girl With A Past."

Mary applies for the job. To help cinch the deal, Mary tells Peach Tree manager Larry McKean (William V. Mong) that shes really Gloria du Moine. Larry asks her why shes dressed so shabbily. Mary replies that her servant absconded with all of her clothing, leaving her to wear the servants clothes.
 Edward Jobson). The young Calhoun meets Gloria and finds her enchanting. He tells his father hed like to propose to her. Michael Calhoun arranges a small, private dinner party at the Peach Tree Inn in honor of Gloria. The elder Calhoun hopes that Gloria will make some sort of faux pas that will discourage his son from seeking her hand in marriage.

Meanwhile, the Duke de Sauterne (Bertram Grassby) has arrived in New York from Europe and noted the press announcements touting Gloria du Moines performances at the Peach Tree Inn. The duke sets out to see her at the roadhouse, and his arrival coincides with Michael Calhouns dinner party. The duke is escorted into Calhouns private room. He gives no indication that Gloria du Moine is an imposter.

At sunrise, the dinner party guests are still at the Peach Tree Inn, sleeping off the drinks they consumed during the evening. Gloria/Mary wakes up and hurries upstairs to her lavish private suite. The duke also wakes up and follows her, and Jimmy follows him. Jimmy and the duke get into a fight, and the duke sends Jimmy tumbling down the staircase.

Mary runs outside, gets into a car and heads for her familys tenement apartment in New York City. The duke and Jimmy follow her separately in their own cars. The duke arrives first, follows Mary up the stairs to the apartment and forces his way in. He grabs Mary and tries to kiss her. Jimmy arrives and engages the duke in another fight. A detective arrives and apprehends the duke for deportation to Europe on accusations of being a swindler. Jimmys father arrives, notes Marys humble surroundings and grants his blessing for Jimmy to marry her.

==Principal cast==
* Mae Murray - Mary McGuire
* Rudolph Valentino - Jimmy Calhoun
* Bertram Grassby - Duke de Sauterne
* William V. Mong - Larry McKean, Peach Tree Inn manager Edward Jobson - Michael Calhoun, Jimmys father
* Burt Woodruff - Musk, Peach Tree Inn proprietor
* Ivor McFadden - Percy, neighborhood butcher boy and Marys friend
* Gertrude Astor - Woman with the talcum powder (uncredited)

==References==
 

==External links==
 
* 
*  

 

 
 
 
 
 
 
 
 
 
 