Pure Shit
 
{{Infobox Film name           = Pure Shit image          = Pure_s_1975_dvd_cover.jpeg caption        = DVD release cover from 2010 producer       = Bob Weis director       = Bert Deling  writer         = Bert Deling Anne Hetherington Alison Hill John Hooper Ricky Kallend John Laurie David Shepherd John Tulip Bob Weis starring       = Garry Waddell Anne Hetherington Carol Porter music          = Martin Armiger Red Symons cinematography = Tom Cowan editing        = John Scott
| studio = Apogee Films distributor    =   released       = 15 August 1975 (Perth International Film Festival)  7 May 1976 (Australia) runtime        = 83 minutes country        = Australia language       = English  budget         = AU$28,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p291 
}}
Pure Shit (censored as Pure S) is a 1975 Australian drama film directed by Bert Deling. Beryl Donaldson & John Langer, "Bert Deling", Cinema Papers, April 1977 p 316-319, 377 
 Playbox in R certificate, and the title was changed from Pure Shit to Pure S.

The low-budget film provoked a hostile reaction from the mainstream media on its initial release. This film is now considered an "underground" classic. 

==Plot summary==
A young woman dies of a heroin overdose. Four junkies who knew her commandeer her car and spend 24 hours searching the streets of Melbourne for good quality heroin, and excitement.

==Cast==
*Gary Waddell as Lou
*Ann Heatherington as Sandy
*Carol Porter as Gerry
*John Laurie as John
*Max Gillies as Dr Wolf
*Tim Robertson as TV interviewer
*Helen Garner as Jo
*Phil Motherwell
*Russell Kirby as Shoplifter / TV Interviewee 
*Greig Pickhaver as Record shop worker
*Vicki Heal as Girl on Phone

==Production==
The films budget was partly provided by the Film, Radio and Television Board of the Australia Council and partly by the Buoyancy Foundation, an organisation to help drug takers.  Bert Deling says he was particularly influenced by Jean Renoir and Howard Hawks.   accessed 2 October 2012 

Lead actor Garry Waddell says he helped with the script:
 It was really good having Bert there because he helped me a lot. If you werent sure of anything you could always get reassurance from him or the cameraman, Tom Cowan. It wasnt a hard movie to work on because it was so enjoyable. The relationships between people on the film were always good.  

==Release==
The Commonwealth film censors initially banned the movie but allowed it to be released with an "R" rating provided the title was changed from Pure Shit to Pure S.  Deling later said that the film "played two weeks at Melbourne’s Playbox and had a short Sydney run … but very few people got to see it, and we didn’t make a cent from it."  The movie was polarising, with the critic of the Herald calling it "the most evil film that Ive ever seen" David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p278  but others such as Bob Ellis championing it.  

It was released on DVD in 2009.

==See also==
*Cinema of Australia

==References==
 
* 

==External links==
* 
*Pure Shit at the  
*  at Oz Movies
 
 
 
 
 
 
 