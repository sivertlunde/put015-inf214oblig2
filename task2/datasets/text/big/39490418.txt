Utsavamelam
{{Infobox film
| name           = Utsavamelam
| image          =
| caption        =
| director       = Suresh Unnithan
| producer       = Urvashi
| studio         = Anugraha Cine Arts
| distributor    = Casio Productions Urvashi  Innocent  Jagathy Sreekumar
| released       =  
| language       = Malayalam 
| editing        =
| runtime        = 160 minutes
| country        = India
}}
Utsavamelam  (The Sound of Festival) is a 1992 Malayalam film directed by Suresh Unnithan and written by Urvashi (actress). The rivalry between two family and the unexpected middle man in these issues are the main theme of the film.

The film was a moderate success.   


== Plot ==
Thekumpuram and Vadakumpuram are rival families in the village, and the focal point for their clashes is who has the right over the village temple and its festivals. It is apparent that there is no near resolution of the conflict as well as no room for negotiation between them.
However the annual festival in the village must go on and every year and it renews the confrontation between these two families and their supporters which apparently divided the village dangerously between two rivaling factions. The rights for the year’s festival go to Vadakumpuram family in a negotiation proposed by respected “Thirumeni” of the village.

==Cast (in credits order)==
*Suresh Gopi as Jayadevan
*Urvashi as  Kanaka Prabha Innocent as Kamalasananan
*Sankaradi as   
*Manoj K Jayan
*Jagannatha Varma as
* Babu Namboothiri as Velichapad
* Narendra Prasad as Thirumeni
* Jagathy Sreekumar as Thankappan / Manoj Kumar
*Indrans
*Unnimary as Sreedevi
*Mamukkoya as Nair
*KPAC Lalitha as Kalyani ammayi
*Kollam Thulasi as Sankaran Usha as Ahwathi
* Valsala Menon
* Nayana	
* Poojappura Radhakrishnan
* Seenath
* Meena Ganesh	
* NL Balakrishnan
* Mala Aravindan
* Ravi Vallathol as Madhavankutty
* Jagannadhan
* Kuthiravattam Pappu
* Alummoodan as PKP
* Babu Namboothiri as Velichappadu Syama as Shalini
* Aliyar
* Haisham

== Soundtrack ==
The film includes songs written by lyricist ONV Kurup and composed by Mohan Sithara.

==References==
 

==External links==
* 

 
 


 