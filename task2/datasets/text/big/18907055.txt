The Green Helmet
{{Infobox Film
| name           = The Green Helmet
| caption        = 
| image	=	The Green Helmet FilmPoster.jpeg
| director       = Michael Forlong
| producer       =  
| writer         = Jon Cleary
| based on   = novel by Jon Cleary
| starring       = Bill Travers Sid James
| music          = Ken Jones
| cinematography = 
| editing        =  
| distributor    = Metro-Goldwyn-Mayer
| released       = 1961
| runtime        = 
| country        = 
| language       = English
| budget         = $378,000  . 
| gross          = $950,000 
| preceded_by    = 
| followed_by    = 
}}
The Green Helmet is a 1961 British film starring Bill Travers, Ed Begley and Sid James. The film is centred on a British motor racing team. It is based on a novel by Australian author Jon Cleary.

==Original novel==
Cleary wrote the original novel in the 1950s. He had written a book about Australian politics, The Mayors Nest, but his English publisher was worried it would not appeal to an international audience, and suggested a book on motor racing. Cleary and his wife had lived in Italy for a year and became familiar with the motor races there. He wrote it in Spain in twenty days at a chapter a day, and it became a best seller on its publication in 1957. 

==Production==
Film rights were bought by MGM who hired Cleary to adapt his own novel. Although most of the movie was set in Italy, it was shot in Wales.   at National Film and Sound Archive   Sid James was cast as an Australian.

==Cast==
* Bill Travers as Rafferty
* Ed Begley as Bartell
* Sid James as Richie Launder
* Nancy Walters as Diane
* Ursula Jeans as Mrs. Rafferty
* Megs Jenkins as Kitty Launder
* Jack Brabham as Himself Sean Kelly as Taz Rafferty
* Tutte Lemkow as Carlo Zaraga
* Gordon Tanner as Hastrow
* Ferdy Mayne as Rossano
* Peter Collingwood as Charlie
* Roland Curram as George
* Diane Clare as Pamela
* Harold Kasket as Lupi

==Reception==

===Box Office===
According to MGM records the film earned $375,000 in the US and $575,000 internationally, making a profit of $124,000. 

==References==
 

==External links==
* 
*  at AustLit (subscription required)

 

 
 
 
 
 
 
 


 