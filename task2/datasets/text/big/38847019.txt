Dumbbells in Ermine
 

{{Infobox film name = Dumbbells in Ermine image = Dumbbells_In_Ermine_1930_Poster.jpg producer  = director = John G. Adolfi writer = James Gleason Harvey F. Thew based on the play Weak Sisters by Lynn Starling starring = Robert Armstrong Barbara Kent Beryl Mercer James Gleason Claude Gillingwater music = cinematography = Devereaux Jennings editing = distributor = Warner Bros. released =   runtime = 70 Minutes language = English country = United States
}} Robert Armstrong, Barbara Kent, Beryl Mercer, James Gleason, and Claude Gillingwater.

==Cast== Robert Armstrong as Jerry Malone 
*Barbara Kent as Faith Corey 
*Beryl Mercer as Grandma Corey
*James Gleason as Mike 
*Claude Gillingwater as Uncle Roger
*Julia Swayne Gordon as Mrs. Corey  
*Arthur Hoyt as Siegfried Strong 
*Mary Foy as Mrs. Strong  
*Charlotte Merriam as Camilla

==Synopsis==
In a small town in Virginia, Barbara Kent, is being forced into a marriage with a missionary reformer by her socially prominent parents. Kent meets Robert Armstrong, a prizefighter, and falls in love with him. Armstrongs manager, played by James Gleason, tries to dissuade Armstrong from the relationship. 

Nevertheless, Kents grandmother, played by Beryl Mercer, and her uncle, played by Claude Gillingwater, do their best to help the romance between Kent and Armstrong. Eventually Kent and Armstrong quarrel, and this leads Kent to agree to her mothers request that she marry the missionary (Arthur Hoyt). When the missionary invites some weak sisters to a revival meeting one of them, a showgirl, accuses him of being responsible for her downfall. 

Because of this, the missionary is publicly disgraced and the marriage cancelled. Gleason helps Armstrong become reconciled with Kent and they marry with the blessings of the family.

==Preservation==
The film is believed to be Lost film|lost. No copies are known to exist.

==See also==
*List of lost films

== External links ==
*  

 
 
 
 
 
 
 
 
 


 