Scared to Death (1981 film)
{{Infobox film
| name           = Scared to Death
| image          = Scared to Death-1981-Poster.jpg
| caption        = Promotional poster artwork.
| director       = William Malone
| producer       = Rand Marlis Gilbert M. Shilton Robert Short
| starring       = John Stinson
| music          = Thomas Chase Dell Hake
| cinematography = Patrick Prince
| editing        = Warren Chadwick 
| distributor    =
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $74,000 Director Interview on Retromedia DVD, 2007. Last accessed: September 2009. 
}}
 B horror science fiction William Malone. 

==Plot==
 
A monster stalks Los Angeles as a Bio-Engineered creature called a Syngenor (which stands for SYNthesized GENetic ORgansism) takes refuge in the citys sewer system and then hits the streets at night in search of human spinal fluid. 

==Production==
Wanting to become a director, William Malone decided to make a monster movie because it was the type of film one could get a lot of production value for very little money. He also had experience with monster designs as he had previously worked as a designer at a Halloween mask factory so he knew he could design the monster himself. In order to raise enough money for the film Malone had to sell most of his personal belongings including his car and mortgaging his house. After raising enough money he began building and sculpting the monster suit. Being inspired by H.R. Gigers design from the movie "Alien" he took 3 months to build the suit. During this process Malone began casting the film and surprisingly he originally cast actor/pop star Rick Springfield in the lead role. Springfield however called up Malone the night before filming began saying he could not be in his film because he was going to miss too many acting classes. Malone then called up actor John Stinson whom he remembered from an improv class and begged him to be in the film, to which he agreed. Filming began in February 1979 and the shoot lasted a total of 4 weeks, a rather long time for a low-budget film.  

Lone Star Pictures, a Texas based company, provided $40,000 for the budget and picked up worldwide distribution. After the premier, the first sale made by Lone Star Pictures made was to Malaysia for $90,000.
They were already making a profit since the film only cost $74,000 to make. 

==Release==
The film was first released by Lone Star Pictures International Inc. in all worldwide markets.

The film was released on VHS by Media Home Entertainment in the 1980s.  

The film was released on  .

==Reception==
 

==References==
 

== External links ==
*  

 

 
 
 
 