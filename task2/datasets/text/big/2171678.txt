Trouble Every Day (film)
 
{{Infobox film
| name           = Trouble Every Day
| image          = Trouble every day ver2.jpg
| alt            = 
| caption        = French theatrical poster
| director       = Claire Denis
| producer       = Georges Benayoun
| screenplay     = {{Plainlist|
* Claire Denis
* Jean-Pol Fargeau}}
| starring       = {{Plainlist|
* Vincent Gallo
* Tricia Vessey
* Béatrice Dalle
* Alex Descas
* Florence Loiret
* Nicolas Duvauchelle
* Aurore Clément}}
| music          = Tindersticks
| cinematography = Agnès Godard
| editing        = Nelly Quettier
| production companies = {{Plainlist| arte France Cinéma
* Canal+
* Centre national de la cinématographie
* Dacia Films
* Messaouda Films
* Zweites Deutsches Fernsehen}}
| distributor    = {{Plainlist|
* Rézo Films  
* Kinetique  }}
| released       =  
| runtime        = 101 minutes  
| country        = {{Plainlist|
* France
* Germany
* Japan}}
| language       = {{Plainlist|
* French
* English}}
| budget         = 
| gross          = $9,184 
}} erotic horror soundtrack is provided by Tindersticks.

==Plot==
Shane and June Brown are an American tourist couple holidaying in Paris. Gradually, June becomes aware that Shane is visiting a mysterious clinic and decides to investigate it herself, although ambivalent about the merits of her fraying marriage. The resident doctor, Leo, has a similarly ambiguous relationship with Core, his wife, whom he keeps secluded from the clinics clients...until Shane encounters her, one day, and finds the tools to possibly restore his marriage to June. 

==Cast==
* Vincent Gallo as Shane Brown
* Tricia Vessey as June Brown
* Béatrice Dalle as Coré
* Alex Descas as Léo
* Florence Loiret as Christelle
* Nicolas Duvauchelle as Erwan
* Raphaël Neal as Ludo
* José Garcia (actor)|José Garcia as Choart
* Hélène Lapiower as Malécot
* Marilu Marini as Friessen
* Aurore Clément as Jeanne

==Release==
Trouble Every Day was screened out of competition at the 2001 Cannes Film Festival.   

==Reception==
The film received mixed reactions from critics. On   reports a 40/100 rating based on 16 critics, indicating "mixed or average reviews". 

Variety (magazine)|Variety wrote that it is "over-long, under-written and needlessly obscure instead of genuinely atmospheric".  The Boston Globe was more positive, but concludes by calling the film "a success in some sense, but its hard to like a film so cold and dead". 
 horror genre as well as gender roles. It was given an in depth analysis by Salon.com which looked at the intricacies of the film, particularly the metaphorical nature of the narrative. At Film Freak Central, Walter Chaw said, "Plaintive and sad, Claire Denis Trouble Every Day is a rare combination of honesty, beauty, and maybe even genius."  The film has been associated with the New French Extremity. Quandt, James, "Flesh & Blood: Sex and violence in recent French cinema", ArtForum, February 2004   Accessdate: 10 July 2008. 

==See also==
* Vampire film

==References==
 

==External links==
*  
*  
*  
*  
*  
*   of  , who also named the film  .

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 