Florian (film)
{{Infobox film
| name           = Florian
| image          = File:Florian poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Edwin L. Marin John E. Burch (assistant)
| writer         = Noel Langley James Kevin McGuinness
| narrator       =  Robert Young
| music          = Franz Waxman
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       = November 11, 1940
| runtime        = 91 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Robert Young and Helen Gilbert. 

==Cast== Robert Young as Anton Erban 
*Helen Gilbert as Duchess Diana
*Charles Coburn as Dr. Johannes Hofer
*Lee Bowman as Archduke Oliver
*Reginald Owen as Emperor Franz Josef

==References==
 

==External links==
* 

 

 
 
 
 