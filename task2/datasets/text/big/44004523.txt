Agni Pareeksha (1968 film)
{{Infobox film 
| name           = Agni Pareeksha
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = M Krishnan Nair
| writer         = Tagore Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Sathyan Sheela Sharada
| music          = G. Devarajan
| cinematography = SJ Thomas
| editing        =
| studio         = Azeem Company
| distributor    = Azeem Company
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, Sharada in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir as Ramesh Sathyan as Dr. Mohan
*Sheela as Hema Sharada as Hemalatha
*Adoor Bhasi as Unni
*Pattom Sadan as Manoharan
*T. R. Omana as Shankari
*T. S. Muthaiah as Raghavan
*Aranmula Ponnamma as Dr. Mohans Mother
*CA Balan GK Pillai as Krishna Kurup
*K. P. Ummer as Gopal
*Panjabi
*Sree Subha as Sumathi
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kairali Kairali Kaavya Kairali || P Susheela, Chorus, Renuka || Vayalar Ramavarma || 
|-
| 2 || Muthuvaaraan Poyavare || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Thinkalum Kathiroliyum || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Urangikidanna Hridayam || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 