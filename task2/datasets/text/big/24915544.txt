The Green Domino
{{Infobox film
|image=Le Domino vert 1935.jpg
|writer=Marcel Aymé  Harald Bratt  Emil Burri  Erich Ebermayer (play) 
|producer=Alfred Greven  
|music=Gottfried Huppertz  Walter Schütze    
|cinematography=Otto Baecker Günther Rittau 
|released=27 December 1935
|runtime=84 minutes
|country=France
|language=French Universum Film (UFA)
|distributor=LAlliance Cinématographique Européenne (ACE)
}} German drama film directed by Henri Decoin and Herbert Selpin, based on play by Erich Ebermayer. It tells the story of a rich heiress who falls in love with an art critic after his wife has been murdered. It was released on DVD in France on 5 April 2007.

==Cast==
*Danielle Darrieux as Hélène de Richemond / Marianne 
*Charles Vanel as Nebel 
*Maurice Escande as Henri Bruquier, le critique dart 
*Daniel Lecourtois as Naulin 
*Marcel Herrand as M. de Richemond 
*Jany Holt as Lily Bruquier 
*Jeanne Pérez 

==External links==
* 
*  at Cinema.nl
*  at AlloCiné  
*  at Evene.fr

 
 
 
 
 
 

 
 