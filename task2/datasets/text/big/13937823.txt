Mumbai Salsa
 
{{Infobox film
| name           = Mumbai Salsa
| image          = Mumbai_Salsa_(film).jpg
| caption        = 
| director       = Manoj Tyagi
| producer       = Surendra Sharma,  Amita Bishnoi
| writer         = Manoj Tyagi
| starring       = Vir Das Linda Arsenio Manjari Phadnis Dilip Thadeshwar
| cinematography = Kalpesh Bhandarkar
| music          = Adnan Sami
| editing        = Kuldip K. Mehan
| distributor    = BVG Films ASA Productions and Enterprises Pvt. Ltd.
| released       =  
| country        = India
| language       = Hindi
}}
Mumbai Salsa is a 2007 Bollywood film directed by Manoj Tyagi.


==Plot==
Summary: This movie is about four pairs who meet accidentally in a Mumbai bar called Mumbai Salsa and get hooked on each other for life. It captures the urban qualities of life and love. 2015 Bengali film Jamai 420

Maya Chandhok lives a wealthy lifestyle in Delhi along with her parents. Refusing to buckle down to her mothers demands to get married, she re-locates to Kolkata, obtains her MBA, and then goes to reside in Mumbai where she gets a job in a bank, and shares a flat with two room-mates, Zenobia and Neha. She meets with Sanjay and both fall in love. Then one day he dramatically and publicly dumps her. A few days later she meets with Rajeev Sharma, who is heartbroken after his fiancée, Pooja, dumped him to re-locate to America, and both are attracted to each other. She is delighted when he proposes to her, but her delight turns sour when he asks her to re-locate with him to Singapore, drop her career, and be a home-maker for the rest of her life. Maya must now make up her mind amidst chaos and heartbreak that also threaten to tear apart lives of both Neha and Zenobia, who are also dating Rajeevs friends, Karan Kapoor and Shaji, while conservative Tyagraj, Rajeevs pal, struggles with his feelings about a much liberated Caucasian co-worker, Pamela.

==Cast==
* Vir Das as Raj
* Linda Arsenio as Pamella
* Manjari Phadnis as Maya
* Amruta Khanvilkar as Neha
* Dilip Thadeshwar as Subbu
* Indraneil Sengupta as Karan
* Ray Irani as Shaji
* Neelam Chauhan as Xenobia
* Alisha Chinai

==External links==
*  

 
 
 
 
 
 


 