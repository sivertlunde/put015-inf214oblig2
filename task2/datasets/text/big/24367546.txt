Rajdrohi
{{Infobox film
| name           = Rajdrohi : Fight Against The System
| image          = Rajdrohi poster.jpg
| alt            =
| caption        = Theatrical Poster
| stars          = Anshuman, Swati, Manali
| director       = Tapan Banerjee
| producer       = Rajkumar Tiwari, Sadhna Movies
| writer         = Avik Banerjee
| screenplay     = Tapan Banerjee
| cinematography =
| music          = Babul Bose
| editing        = Swapan Guha
| distributor    =
| country        = India
| language       = Bengali
| released       =  
| runtime        =
| budget         =
| gross          =
}} Bengali film Manali and Rajatava Dutta. This film is slated to release on 25 December 2009 and is the first science fiction film of its kind in Bengali till date. The film deals with invisibility of human being and is quite similar to H.G. Wells The Invisible Man

==Synopsis==
Deep (Anshuman) gets in a fix when a laboratory test on him makes him invisible. Rest of the film shows his search for his father who invented the antidote and comes to know about many shocking secrets.

==Cast==
* Anshuman Swati
* Manali Dey
* Arun Banerjee
* Rajatava Dutta
* Amit Daw

==Crew==
* Director: Tapan Banerjee
* Producer :Rajkumar Tiwary
* Story : Avik Banerjee
* Presenter :Sadhna Movies
* Music Director: Babul Bose
* Cinematographer: Babul Roy
* Editor : Swapan Guha
* Playback Singer : Shaan, Jaaved Ali, Mahalaxmi Iyer, Anwesha, Udit Narayan, Vinod Rathod & Asha Bhonsle

==Production==
It was fall 2008 when the story was proposed by Avik Banerjee to director Tapan Banerjee about a man who falls in a dilemma after getting invisible. The story however was in a short form which was later worked on to make a film on it. Due to high budget and "not so contemporary" storyline, many producers stepped back from the project. It was the start of 2009 when Rajkumar Tiwari decided to make a film on the story which impressed him as being a science fiction fan.

The film started on 27 April 2009 with almost a new starcast. The film while in shooting faced a real problem as everyone in the floor was not able to adjust with the special effects and stumbled on their natural acting. Eventually the cast became habituated with the shoot and became comfortable.

The film was completed at various location of Kolkata and Vizag.
The Director Tapan Banerjees 6th.Film Rajdrohi.
1) Jai Ma Hangeswari year 1980
2) Prem Pratidan     Year 2001
3) Idiot             year 2004
4) Nari              Year 2005
5) Prem              Year 2007
6) Rajdrohi          Year 2009

==Music==
Songs are composed by Babul Bose and music will be released on SaReGaMa. Asha Bhonsle singing in this film after a break of 15 years.

==References==
{{Reflist|refs=

   
}}

  

 
 
 