Heidenlöcher
{{Infobox film
| name           = Heidenlöcher
| image          = 
| image size     = 
| caption        = 
| director       = Wolfram Paulus
| producer       = Peter Voiss Axel von Hahn
| writer         = Wolfram Paulus
| starring       = Florian Pircher
| music          = 
| cinematography = Wolfgang Simon
| editing        = Wolfgang Simon
| distributor    = 
| released       = 1986
| runtime        = 100 minutes
| country        = Austria
| language       = German
| budget         = 
}}

Heidenlöcher is a 1986 Austrian drama film directed by Wolfram Paulus. It was entered into the 36th Berlin International Film Festival.   

==Cast==
* Florian Pircher as Santner
* Albert Paulus as Ruap
* Helmut Vogel as Jacek
* Matthias Aichhorn as Dürlinger
* Rolf Zacher as Aufseher
* Claus-Dieter Reents as Gestapomann
* Maria Aichhorn as Frau Dürlinger
* Gerta Rettenwender as Frau Santner
* Joanna Madej as Agnes
* Franz Hafner as Forstmeister
* Doris Kreer as Lisabeth
* Hubsi Aichhorn as Festl
* Darius Polanski as Staschek
* Piotr Firackiewicz as Kowal

==References==
 

==External links==
* 

 
 
 
 
 
 
 