Scout's Honor (film)
{{Infobox television film
| name = Scouts Honor
| image =
| image_size =
| caption =
| format = Family
| runtime = 100 minutes
| creator =
| distributor = NBC
| director = Henry Levin
| producer = Jimmy Hawkins
| writer = Bennett Foster
| editing = Ed Cotter
| cinematography = Gary Graver Pat OBrien Harry Morgan
| music = Mike Post
| country = United States
| language = English
| network = NBC
| released = September 30, 1980
}}

Scouts Honor is a 1980 made-for-TV film, starring Gary Coleman.

==Cast==
*Gary Coleman as Joey Seymour
*Katherine Helmond as Pearl Bartlett
*Wilfrid Hyde-White as Uncle Toby Nuncle Bartlett Pat OBrien as Mr. Caboose
*Harry Morgan as Mr. Briggs
*Eric Taslitz as Grogan
*Meeno Peluce as Big Ace
*Marcello Krakoff as Little Ace
*John Louie as Patrick
*Angela Cartwright as Alfredos Mom
*Lauren Chapin as Aces Mom
*Jay North as Grogans Dad
*Paul Petersen as Aces Dad
*Joanna Moore as Ms. Odom Peter Hobbs as U.S. President
*Wesley Pfenning as Ms. Lopes (as Wesley Ann Pfenning)
*Robert Trujillo as Boy Scout
*Basil Hoffman as Alexander
*Rance Howard as Captain
*Al Fann as Mr. Prewitt
*Shelley Oloport as Linda
*Bobby Lento as Football Player
*Shavar Ross as Joeys Schoolmate

==External links==
*  

 

 
 
 
 
 

 