Let Me Out (film)
{{Infobox film
| name           = Let Me Out 
| image          = File:Let_Me_Out_poster.jpg
| director       = Kim Chang-rae   Jae Soh
| producer       = Jae Soh   Irene Cho   Min Soh
| writer         = Kim Chang-rae   Jae Soh
| starring       = Kwon Hyun-sang   Park Hee-bon
| music          =  
| cinematography = Kim Seung-hoon  
| editing        = 
| distributor    = Baekdu-Daegan Film Company
| released       =  
| runtime        = 97 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          = 
| film name = {{Film name
| hangul         = 렛 미 아웃 
| hanja          = 
| rr             = Let-mi-aut
| mr             = }}
}}
 indie is about a film students struggles with making a movie for the first time, and captures the harsh realities of the Korean filmmaking industry.    Produced by the Seoul Institute of the Arts and the Baekdu-Daegan Film Company, it was directed by Soh Jae-young (or Jae Soh) and Kim Chang-rae,  and starred Kwon Hyun-sang in his first leading role.  

Let Me Out won the Gold Medal Award at the 57th New York Festivals International Television & Film Awards in 2014.  

==Plot==
Mu-young (Kwon Hyun-sang) is a 4th-year film student and convenience store clerk. A know-it-all who is quick to pick apart and ridicule the work of others, he is endlessly complaining about the state of the Korean film industry and showing off his vast knowledge of cinema. When a famous indie director (Yang Ik-june) visits his university, his fellow students are starstruck, but Mu-young is unimpressed. At a Q&A session, he mocks the directors most recent work as a commercial sellout. Similarly unimpressed with Mu-youngs attitude, the director throws it back at him, unexpectedly "awarding" him a   production grant and challenging him to go out and actually shoot a decent film.

The problem is, Mu-young is nowhere close to being prepared, with his unfinished screenplay languishing in a drawer. With no other choice, he gathers together a cast and crew with varying experience, and casts Ah-young (Park Hee-bon), the girl he loves, as the protagonist. He sets out to shoot his dream project, a zombie melodrama. But his inexperience, demands and impatience cause everyone around him to go crazy, turning his project into a nightmare. Everything that can go wrong, does, and the shoot faces a series of obstacles and accidents, from casting actors to securing locations and funds. Mu-young comes to realize that the reality of filmmaking is very different from theory, and like love, isnt easy at all. 

==Cast==
*Kwon Hyun-sang as Mu-young
*Park Hee-bon as Ah-young 
*Han Geun-sup as Yong-woon
*Yu Mou-young as professor
*Yang Ik-june (cameo)
*Lee Myung-se (cameo)

==Release==
Let Me Out premiered at the 2012 Puchon International Fantastic Film Festival. Then in 2013, it was the first Korean independent film to receive a simultaneous theater release in South Korea and the United States. 
 CGV Movie Collage, and Artplus Cinema Network. It made its U.S. premiere at the The Downtown Independent in Los Angeles on August 16, and was released by distributor Funimation on September 25 in San Francisco, San Diego and Dallas, on October 23 in Atlanta, and on October 24 in Chicago.  

==References==
 

==External links==
* 
* 
* 

 
 
 
 