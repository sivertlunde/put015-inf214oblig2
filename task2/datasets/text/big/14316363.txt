Knutzy Knights
{{Infobox Film |
  | name           = Knutzy Knights
  | image          = Knutzynightsandasys.jpg
  | caption        = 
  | director       = Jules White  Felix Adler Ruth Godfrey Vernon Dent Philip Van Zandt Jock Mahoney Joe Palma 
  | cinematography = Ray Cory |
  | editing        = Edwin H. Bryant 
  | producer       = Jules White 
  | distributor    = Columbia Pictures 
  | released       =  
  | runtime        = 15 22" 
  | country        = United States
  | language       = English
}}

Knutzy Knights is the 156th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are troubadours sent to cheer up the brokenhearted Princess Elaine (Christine McIntyre). Her father, the King, (Vernon Dent) has pledged her hand in marriage to the Black Prince (Philip Van Zandt), but she loves Cedric, the local blacksmith (Jock Mahoney).

The Stooges try to intervene for Cedric by serenading Elaine (they sing a variation on the Sextette from "Lucia di Lammermoor", with lyrics telling Elaine that Cedric is present and warning of the Black Princes plot). They are captured by the king’s guards and condemned to be beheaded. Eventually, the King realizes the plot and jails the Black Prince and his fellow plotter. Elaine is allowed to marry Cedric, and they all live happily ever after.

==Production notes== Hot Stuff and Guns a Poppin. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 