Dongodu
{{Infobox film
|  name           = Dongodu
|  image          =  Dongodu DVD.jpg
|  caption        =  DVD Cover Art
|  director       = Bhimaneni Srinivasa Rao
|  producer       = Rushita Sai
|  writer         = Ranjan Pramod Kalyani Rekha Sunil M S Narayana Shakeela Vidyasagar
|  cinematography =  Ramana Raju
|  editing        =  Gowtam Raju
|  released       =  
|  runtime        = 140 minutes
|  country        = India
|  language       = Telugu
|  budget         =
|  gross          =
}} Kalyani in the lead roles. Music has been composed Vidyasagar (music director)|Vidyasagar. The movie released on 7 August 2003 was a decent Boxoffice success. 

==Cast==
* Ravi Teja as Madhava Kalyani as Rukhmini
* Rekha Vedavyas
* Tanikella Bharani as Naidu
* Brahmanandam as Sastry
* Uttej as Hanumanthu
* Dharmavarapu Subramanyam Sunil as Justice Chowdary
* M S Narayana
* Paruchuri Venkateswara Rao
* Shakeela

==Box office==

The movie was a decent hit at boxoffice. The comedy track between Tanikella Bharani and Dharmavarapu Subramanyam was very well received. The success of the movie helped Ravi Teja to establish himself as Mass Hero.

==Soundtrack==
 Vidyasagar and Lyrics are written by Sirivennela Sitaramasastri, Chandrabose (Lyricist)|Chandrabose, Bashasree and Bandaru Daanayya.
All songs are re-used from the Malayalam original, Meesha Madhavan except "Meesala Gopala" which was re-used from "Panchangam Paarkathe" from Thavasi.

{{Infobox album
| Name= Dongodu
| Type = soundtrack Vidyasagar
| Cover =  
| Released =  
| Genre = Film soundtrack
| Length =
| Producer = Rushita Sai
| Label  = Supreme Music
| Reviews = 
}}

{{tracklist
| headline        = Tracklist
| all_music       = Vidya Sagar 
| music_credits   = no
| extra_column    = Artist(s)
| title1          = Dum Dum Dum 
| extra1          = K. S. Chithra|Chitra, Valisha, Babji 
| title2          = Entha Pani Chesindi Sujatha
| title3          = Kodi
| extra3          =  Rimmi Tommy, Shankar Mahadevan 
| title4          = Meesala Gopala
| extra4          = Udit Narayan, Srivarthini
| title5          = Sotta Buggala Rukmini
| extra5          = Karthik Raja, Swarnalatha
| title6          = Theme Music
| extra6          = Vidya Sagar
}}

==References==
 

 

 
 
 
 

 