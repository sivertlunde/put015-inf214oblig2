Catch as Catch Can (1967 film)
 
{{Infobox film
| name           = Catch As Catch Can (Lo scatenato)
| image          = Catch as Catch Can (1967 film).jpg
| caption        = Film poster
| director       = Franco Indovina
| producer       = Mario Cecchi Gori
| writer         = Franco Indovina Tonino Guerra Luigi Malerba
| starring       = Vittorio Gassman Martha Hyer Carmelo Bene
| music          = Luis Enríquez Bacalov
| cinematography = Aldo Tonti
| editing        = Marcello Malvestito
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Catch As Catch Can ( ) is a 1967 Italian comedy film directed by Franco Indovina. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Plot==
Bob is a famous actor of various advertising, but slowly begins to go crazy. In fact, he seems to be haunted by normal animals that do not take kindly. A bull for example, Bob throws in a river, while the mice gnaw a rope that held him suspended from a helicopter to shoot a commercial. When Bob refuses to turn advertising with other animals as extras, it is immediately fired. On the verge of despair, enters an ordinary fly, which begins to haunt the lives of the poor Bob who tries to kill her in every way, to no avail. When the fly disappears, Bob also continues to hear the annoying buzzing, beating their hands everywhere like a real fool.

==Cast==
* Vittorio Gassman - Bob Chiaramonte
* Martha Hyer - Luisa Chiaramonte
* Gila Golan - Emma
* Karin Skarreso - Girl Model
* Massimo Serato - Agent
* Carmelo Bene - Priest
* Steffen Zacharias - Police Inspector
* Jacques Herlin - Zoology Professor
* Claudio Gora - Cabinet Minister
* Gigi Proietti - Make-up man
* Giovanni Ivan Scratuglia - Gianfranco (as Ivan Scratuglia)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 