Dolphin Tale 2
 
{{Infobox film
| name    = Dolphin Tale 2
| image   = Dolphin Tale 2.jpg
| alt     = 
| caption = Theatrical release poster
| director = Charles Martin Smith Andrew A. Kosove
| writer   = Charles Martin Smith
| starring = Harry Connick, Jr. Ashley Judd Nathan Gamble Kris Kristofferson Cozi Zuehlsdorff Morgan Freeman  
| music    = Rachel Portman 
| cinematography = Daryn Okada 
| editing  = Harvey Rosenstock 
| studio   = Alcon Entertainment
| distributor = Warner Bros. Pictures Summit Entertainment
| released =  
| runtime  = 107 minutes  
| country  = United States
| language = English
| budget   = $36 million   
| gross    = $52.4 million 
}} family drama film written and directed by Charles Martin Smith and sequel to his 2011 film Dolphin Tale. Harry Connick, Jr., Ashley Judd, Nathan Gamble, Cozi Zuehlsdorff, Kris Kristofferson, Morgan Freeman, Juliana Harkavy, Austin Stowell, Betsy Landin, and Austin Highsmith all reprise their roles from the first film while Lee Karlinsky, Julia Jordan, Taylor Blackwell, and Bethany Hamilton join the cast. The film was released on September 12, 2014  and tells the story of another dolphin at the hospital named "Hope". After Winters elderly companion and surrogate mother, Panama, passes away, Winters future is put in jeopardy, unless Sawyer, Clay, Hazel, and the rest of the Clearwater Marine Aquarium team can find a new companion for her.

==Plot== SEA Semester CMA dolphin he helped save three years earlier, who has lost her tail. Winter needs attention and shows signs of stress and loneliness, especially after the death of her older dolphin friend, Panama. George Hatton, A USDA inspector, requires that, for health reasons, Winter must be matched with a new female companion within thirty days or be transferred elsewhere. However, Winters behavior has become unpredictable, even dangerous: when Sawyer tries to get her out of the tank to prepare to make her a new prosthetic tail, she becomes aggressive and knocks him into the water before Phoebe jumps into the pool and rescues him. Dr. Clay prohibits Sawyer from swimming with Winter until further notice.

Before this, CMA rescues a severely sunburned and beached dolphin, named Mandy after a little girl who finds her, which Sawyer, Clay and his daughter, Hazel, hope to be Winters companion. CMA begins to rehabilitate Mandy, contemplating releasing her soon. Hazel becomes greatly unhappy with her father, and she consults with Sawyers mother, Lorraine, who explains to her what hospitals are for and encourages her to treat her dad the way she would want to be treated. Thus, Hazel understands that whats best for Mandy is just as important as whats best for Winter. Clay, Sawyer and Hazel give Mandy three live fish, which she catches and eats effortlessly, so they decide to release her, with Hazel assertively taking charge. Mandy receives a goodbye "ceremony" while she swims out to sea.

Hazel and Sawyer break the rule and swim with Winter, who seems improved and is no longer aggressive. Dr. Clay catches them and scolds them, but then states that it does not matter, because George Hatton issued a transfer order and Winter is to be transferred to a marine park in Texas since CMA has no companion for her.

Meanwhile, Sawyer wins the coveted scholarship to attend SEA Semester so his mother and friends throw him a party, which cheers him up a little bit, but he still cant decide whether to go because he is too upset. Dr. McCarthy shows him an old watch which must be tapped to keep ticking. Using this as an example, he encourages Sawyer to "shake it up now and then" and try new things in life.

During the party, Dr. Clay receives word that another female dolphin has been rescued. This one is so young that she has not yet learned how to hunt, so she cannot be returned to the wild. Thus she represents another hope of companionship for Winter. They name her "Hope." Also, in a side plot, Sawyer and Hazel rescue a sea turtle from some fishing line and name her Mavis. Rufus, on the other hand, takes a liking to Mavis, even watching her during a CT Scan. As Sawyer, Hazel, and the CMA staff release Mavis, Rufus follows her out to sea.

Dr. Clay gets an extension to keep Winter from being transferred, and CMA tries to introduce Hope as her new companion. But Hope panics and circles the pool at high speed, causing Winter to go into a dangerous panic as well. The staff quickly separates the two dolphins.

To see what went wrong, the CMA staff analyzes the video recording. They realize that Hope did not know what to make of Winter because Hopes echolocation sensed Winter had no tail and moved differently from other dolphins. Sawyer wants to try a new prosthetic tail, but some think it could make Hope panic even more. After debate, everyone finally agrees to try it.

In her second meeting with Hope, Winter wears the new prosthetic tail, designed by Dr. McCarthy. Mandy and her family, Bethany Hamilton (who earlier came to CMA and swam with Winter and Sawyer), Phil Hordern, and George Hatton are present to see that it will go well. Again things get a bit rough, but eventually the two dolphins accept each other. Everyone rejoices that Winter can stay at CMA now that she has a companion. With his uncertainty about Winter resolved, Sawyer decides to go to the SEA Semester program, but he reassures her that hell be back. Hazel says goodbye to him by releasing helium balloons as Rufus returns to the aquarium.

Real-life footage shown at the end includes Mandys release, Hopes rescue, a sea turtles named Mavis and her release, Winter wearing the new prosthetic tail, and amputees interacting with Winter.

==Cast==
* Harry Connick, Jr. as Dr. Clay Haskett
* Ashley Judd as Lorraine Nelson
* Nathan Gamble as Sawyer Nelson
* Kris Kristofferson as Reed Haskett
* Cozi Zuehlsdorff as Hazel Haskett
* Morgan Freeman a Dr. Cameron McCarthy
* Denisea Wilson as Julia Julia Jordan as Mandy
* Austin Stowell as Kyle Connellan
* Juliana Harkavy as Rebecca
* Austin Highsmith as Phoebe
* Betsy Landin as Kat
* Lee Karlinsky as Troy
* Taylor Blackwell as Susie Winter as herself Hope as herself
* Bethany Hamilton as herself
* Tom Hillmann as Mel Prince
* Tom Nowicki as Philip J. Hordern
* Charles Martin Smith as George Hatton

==Production== Filming began on October 7, 2013, at Clearwater Marine Aquarium in Clearwater, Florida.  

Although they were filming on the site, Clearwater Marine Aquarium is open on weekends to the public. Cozi Zuehlsdorff tweeted on her Twitter account that she is excited that Bethany Hamilton has joined the "Dolphin Tale 2 family".

According to Clearwater Marine Aquarium CEO, David Yates, filming ended on January 22, 2014. It was released on September 12, 2014. 

===Marketing=== teaser trailer was released on April 11, 2014, attached with theatrical screenings of Rio 2.  A second trailer was released on June 13, attached with How to Train Your Dragon 2. 

==Reception==

===Critical reception===
Dolphin Tale 2 received generally positive reviews from critics. On   playing"  On Metacritic, the film has a rating of 58 out of 100, based on 27 critics, indicating "mixed or average reviews". 

===Home media===
Dolphin Tale 2 was released on Blu-ray, DVD and Digital media on December 9, 2014.

==Sequel==
At a press conference speaking about the possibilities for a new Clearwater Marine Aquarium, David Yates revealed that discussions have begun towards the development of a sequel and television series.   

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 