Young and Dangerous 3
 
 
{{Infobox film
| name           = Young and Dangerous 3
| image          = Young and Dangerous 3.jpg
| image size     =
| caption        =
| director       = Andrew Lau
| producer       = Manfred Wong
| writer         = Manfred Wong
| narrator       =
| starring       = Ekin Cheng Jordan Chan
| music          = Clarence Hui Ronald Ng
| cinematography = Andrew Lau
| editing        = Marco Mak Ma Ko
| distributor    = Orange Sky Golden Harvest| Golden Harvest Company BoB and Partners Co. Ltd.
| released       =  
| runtime        =
| country        = Hong Kong Cantonese Chaozhou Chaozhao English English
| budget         =
}} triad film Golden Harvest Company.

==Plot== Taiwanese "San no prior memories to her meeting with Ho Nam for the first time. Regardless, Ho Nam assures her he and his friends will protect her. To add in a stick of comedy, Father "Lethal Weapon" Lam (Spencer Lam) introduces his daughter Shuk Fan (Karen Mok) to Chicken, having been good friends and a source of advice for him.

During a business trip to Amsterdam with his mistress and Ho Nam, Chairman Chiang is assassinated by thugs. While the rest of Hung Hing believes the hit was orchestrated by Ho Nam, it is the deranged Crow who ordered the chairmans death, using Chiangs mistress to falsify evidence, framing Ho Nam. While Ho Nam goes into hiding back in Hong Kong, Crow is reprimanded by Camel; to add to his insanity, Crow kills his own boss and makes it look like a Hung Hing assassination. Drunk with power, Crow wants nothing more than to destroy Hung Hing and orders his men to search frantically for Ho Nam. Fortunately, Ho Nam is quick to realize the ambush and escapes with Smartie, until Crows men manages to separate the two. In their attempt, Smartie is captured but suffers a blow to the head, restoring her memories. Crow tells Ho Nam if he wants his name cleared and his woman back, he must meet him alone.

Yet, the crazed Crow does not keep his word and kills Smartie in cold blood in front of Ho Nam. Just as Crow is about to finish him, Chicken bursts in and reaches a stalemate with Crow to ensure Ho Nams safety. The saddened Ho Nam carries Smarties body out with him and gives her a proper funeral. Now fueled solely on vengeance, Ho Nam decides to march into Tung Sing territory and kill Crow at Camels funeral haphazardly. Fortunately, Ho Nams friends and the rest of Hung Hing manage to capture and threaten Tung Sing member "Tiger" (Ng Chi Hung), who tells all of Crows madness in killing both their societies leaders. Crow is left nowhere to run from his enemies, and in the midst of a Hung Hing/Tung Sing brawl, he is killed in the funeral pyre. With Crow dead, Tung Sing is left in disarray, and Hung Hing re-establishes control in its territories it was officially reinnaugurated by Young and Dangerous 4 it was officially premiered on 28 March 1997.

==Cast==
 
* Ekin Cheng - Chan Ho Nam
* Jordan Chan - Chicken Chiu
* Gigi Lai - Smartie/Stammer
* Karen Mok - Shuk-Fan / Wasabi
* Jerry Lamb - Bou Pei
* Michael Tse - Dai Tin-Yee
* Simon Yam - Chiang Tin-Sung
* Jason Chu - Banana Skin
* Roy Cheung - Crow
* Ng Chi Hung - Tiger
* Spencer Lam - Father Lam
* Anthony Wong Chau-sang - Tai Fai
* Chan Wai-Man - Camel Lok
* Blackie Ko - Blackie Ko Chi-Wah
* Halina Tam - K.K.
* Law Lan - Granny
* Victor Hon - Uncle Eight Fingers
* Chingmy Yau
* Lee Siu-kei - Key Danny Chan Kwok Kwan
* Sammuel Leung - Fat Sze
* Daniel Ng
* Nelson Wai Chow - 14K Gangsta
* Brendan Chow - Black Boy
* Oscar Au - Retard Boy (9 Long)
 

== See also ==
* Young and Dangerous

==External links==
*  

 

 
 
 
 
 
 
 