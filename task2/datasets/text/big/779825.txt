Three Businessmen
Three Businessmen is a film made in 1998. It is a British-Dutch-Spanish-Japanese coproduction directed by Alex Cox. The screenplay is by Tod Davies, who was also the film producer|producer.

It is quirky philosophical comedy of three businessmen, art dealers Benny (Miguel Sandoval) (American) and Frank (Alex Cox) (British), who set out in Liverpool in search of a meal, and end up in a whirlwind trip around the Earth in search of food, meeting businessman Leroy (Robert Wisdom) in the desert, then discovering that they are present at the birth of the new female Messiah... and promptly forgetting again.

The film premiered at the Rotterdam Film Festival in 1999. Like most of Coxs films, it received a limited theatrical release. The author/producer was Tod Davies, with whom Cox also collaborated on the scripts of Backtrack/Catchfire (Dennis Hopper, 1989) and Keith Moon was here (unproduced screenplay for Roger Daltrey, 2001).  She also appears in one scene in the film.
 Pray for Deborah Harry singing a techno version "Ghost Riders in the Sky".

==Filming locations==
Locations include London Victoria Station, Kruiskade in Rotterdam, the Star Ferry in Hong Kong, Tokyo Japan and an unnamed desert.

==External links==
* 
* 

 

 
 
 