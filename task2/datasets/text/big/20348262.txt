Moon (film)
 
 
{{Infobox film
| name           = Moon
| image          = Moon (2008) film poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Duncan Jones
| producer       = {{Plainlist|
* Stuart Fenegan
* Trudie Styler
}}
| screenplay  = Nathan Parker
| story          = Duncan Jones
| starring       = {{Plainlist|
* Sam Rockwell
* Kevin Spacey
* Dominique McElligott
* Kaya Scodelario
* Benedict Wong
* Matt Berry Malcolm Stewart
 
}}
| music          = Clint Mansell
| cinematography = Gary Shaw
| editing        = Nicolas Gaster
| studio         = Stage 6 Films
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = $5 million 
| gross          = $9,760,104 
}} science fiction New York and Los Angeles on 12 June 2009. The release was expanded to additional theatres in the United States and Toronto on both 3 and 10 July and to the United Kingdom on 17 July.

==Plot==
 
In 2035, Lunar Industries have made a fortune after an oil crisis by building Sarang, an automated lunar facility to mine the alternative fuel helium-3. Helium-3 is used primarily to power fusion reactors and other things that run on fusion energy.

Sam Bell, the astronaut who maintains operations at Sarang, nears the end of a three-year work contract as the facilitys sole resident. Sam oversees automated harvesters and launches canisters bound for Earth containing the extracted helium-3. Chronic communication problems have disabled his live feed from Earth and limit him to occasional recorded messages to his wife Tess, who was pregnant with their daughter Eve when he left. His only companion is an artificial intelligence named GERTY, who assists with the bases automation and provides comfort for him.

Two weeks before his return to Earth, Sam suffers from hallucinations of a teenage girl. One such image distracts him while he is out recovering a helium-3 canister from a harvester, causing him to crash his lunar rover into the harvester. Rapidly losing cabin air from the crash, Sam falls unconscious.

Sam awakes in the base infirmary with no memory of the accident. He overhears GERTY receiving instructions from Lunar Industries to prevent him leaving the base and to wait for the arrival of a rescue team. His suspicions aroused, he manufactures a fake problem to convince GERTY to let him outside. He travels to the crashed rover, where he finds his unconscious doppelgänger.
 clone of the other. After a heated argument and physical altercation, they together coerce GERTY into revealing that they are both clones of the original Sam Bell. GERTY activated the newest clone after the rover crash, and convinced him that he was at the beginning of his three-year contract.

The two Sams search the facility.  They find that live communications are jammed by transmitters located beyond the outermost perimeter of the base. They also discover that four previous clones physically deteriorated three years after awakening. Told they would hibernate briefly for the journey home, they were actually euthanized and incinerated.

They find a secret vault containing hundreds of hibernating clones. They realize that Lunar Industries manufactures clones to avoid paying for new astronauts. The elder Sam drives past the interference radius in a second rover and tries to call Tess on Earth. He instead makes contact with Eve, now 15 years old, who says Tess died "some years ago."  He hangs up when Eve tells a male voice that someone is calling regarding Tess.

The two Sams realize that the incoming "rescue" team will kill them both if they are found together. The newer Sam suggests sending the other to Earth in one of the helium-3 transports, but the older Sam, already badly deteriorated, knows that he will not live much longer. He suggests the younger Sam leave instead, and alert the public to Lunar Industries unethical practices. The older Sam plans to die by the crashed rover so Lunar Industries will not suspect anything until it is too late.
 
The younger clone orders GERTY to revive a seventh clone to greet the rescuers, then programs a harvester to crash and wreck a jamming antenna, thereby enabling live communications with Earth. GERTY advises the younger Sam to reboot him, erasing its records of the event, and Sam does so.  The older Sam, back in the crippled rover, remains conscious long enough to watch the launch of the transport carrying the younger Sam to Earth.

As the credits roll, the helium transport is depicted entering Earths upper atmosphere. News reports describe how Sams testimony on Lunar Industries activities has stirred up an enormous controversy, and the companys  unethical practices cause a significant dip in stock value.

==Cast==
* Sam Rockwell as Sam Bell
* Kevin Spacey as GERTY (voice) 
* Dominique McElligott as Tess Bell
* Kaya Scodelario as Eve Bell
* Benedict Wong as Thompson
* Matt Berry as Overmeyers Malcolm Stewart as the technician
* Robin Chalk as Sam Bell clone

==Production==
This is the first feature film directed by commercial director Duncan Jones, who co-wrote the script with Nathan Parker.    The film was specifically written as a vehicle for actor Sam Rockwell.  The film pays homage to the films of Jones youth, such as Silent Running, Alien (film)|Alien, and Outland (film)|Outland.   

Jones described the intent: "  wanted to create something which felt comfortable within that canon of those science fiction films from the sort of late seventies to early eighties."  The director spoke of his interest in the lunar setting: "for me, the Moon has this weird mythic nature to it.... There is still a mystery to it. As a location, it bridges the gap between science-fiction and science fact. We (humankind) have been there. It is something so close and so plausible and yet at the same time, we really dont know that much about it."

The director described the lack of romance in the Moon as a location, citing images from the Japanese lunar orbiter SELENE: "Its the desolation and emptiness of it.... it looks like some strange ball of clay in blackness.... Look at photos and youll think that theyre monochrome. In fact, theyre not. There simply are no primary colours." Jones made reference to the photography book Full Moon by Michael Light in designing the look of the film. 
 TV advertisements, he drew on his experience to create special effects within a small budget. 

==Release==
  and Duncan Jones at the 2009 Tribeca Film Festival screening]]
International sales for Moon are handled by the Independent sales company.    Sony Pictures Worldwide Acquisitions Group acquired distribution rights to the film for English-speaking territories.  Sony Pictures Worldwide Acquisitions Group was considering making Moon a direct-to-DVD release; however, after Moon premiered at the 2009 Sundance Film Festival in January 2009, Sony Pictures Classics decided to handle this films theatrical release for Sony Pictures Worldwide Acquisitions Group. 
 Harry Potter and the Half-Blood Prince.  The Australian release was on 8 October.   

===Box office===
Moon grossed £700,394 from its domestic release,  $3,370,366 from its North American release and $9,760,104 worldwide     making the film a modest financial success.

===Critical reception===
Moon received positive reviews. Film review aggregator  , which assigns a normalized rating out of 100 based on reviews from critics, the film has a score of 67 based on 29 reviews, considered to be "generally favorable reviews".  Damon Wise of The Times praised Jones "thoughtful" direction and Rockwells "poignant" performance. Wise wrote of the films approach to the science fiction genre: "Though it uses impressive sci-fi trappings to tell its story—the fabulous models and moonscapes are recognisably retro yet surprisingly real—this is a film about what it means, and takes, to be human." 
 Big Brother themes. Byrge also believed that cinematographer Gary Shaws work and composer Clint Mansells music intensified the drama. Byrge wrote: “Nonetheless, Moon is darkened by its own excellencies: The white, claustrophobic look is apt and moody, but a lack of physical action enervates the story thrust.” The critic felt mixed about the stars performance, describing him as “adept at limning his characters dissolution” but finding that he did not have “the audacious, dominant edge” for the major confrontation at the end of the film. 

Empire (magazine)|Empire magazine praised Rockwells performance, including it in 10 Egregious Oscar Snubs—The worthy contenders that the Academy overlooked feature and referred to his performance as "one ... of the best performances of the year". 

 , the godfather of this genre, would have approved. The movie is really all about ideas. It only seems to be about emotions. How real are our emotions, anyway? How real are we? Someday I will die. This laptop Im using is patient and can wait.”  Moon also received positive reviews at the Sundance Film Festival. 

===Reception from the scientific community=== lunar regolith and ice water from the moons polar caps.  

==Accolades==
{| class="wikitable" width="90%"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| Austin Film Critics Association Awards 15 December 2009
| Austin Film Critics Award for Best Film
| Moon
|  
|- British Academy BAFTA Awards   63rd British 21 February 2010
| BAFTA Award for Outstanding Debut by a British Writer, Director or Producer
| Duncan Jones
|  
|- BAFTA Award for Outstanding British Film
| Stuart Fenegan, Trudie Styler, Duncan Jones, Nathan Parker
|  
|- British Independent Film Awards   British Independent 6 December 2009
| BIFA Award for Best British Independent Film
| Moon
|  
|-
| Douglas Hickox Award Duncan Jones
|  
|-
| BIFA Award for Best Director
|  
|-
| BIFA Award for Best Performance by an Actor in a British Independent Film
| Sam Rockwell
|  
|-
| BIFA Award for Best Screenplay
| Nathan Parker
|  
|- BIFA Award for Best Technical Achievement
| Clint Mansell
|  
|-
| Tony Noble
|  
|-
| Chicago Film Critics Association   21 December 2009
| Most Promising Filmmaker
| Duncan Jones
|  
|-
| Edinburgh International Film Festival  
| 28 June 2009
| Best New British Feature
| rowspan="2"|Moon
|  
|-
| Empire Awards   28 March 2010
| Empire Award for Best Sci-Fi/Fantasy
|  
|-
| Espoo Ciné International Film Festival  
| 29 August 2013
| Grand Prize of European Fantasy Film in Gold
| Duncan Jones, Stuart Fenegan
|  
|-
| rowspan="2"| Evening Standard British Film Awards 
| rowspan="2"| 8 February 2010
| Evening Standard British Film Award for Most Promising Newcomer
| Duncan Jones
|  
|-
| Evening Standard British Film Award for Best Technical Achievement
| Tony Noble
|  
|- Festival international du film fantastique de Gérardmer|FantasticArts  31 January 2010
| Jury Prize Duncan Jones
|  
|-
| Special Prize
|  
|-
| Gaudí Awards  
| 1 February 2010
| Gaudí Award for Best European Film
|  
|- 
| Hugo Awards  5 September 2010 Hugo Award for Best Dramatic Presentation - Long Form
| Nathan Parker, Duncan Jones
|  
|- 
| Irish Film & Television Awards  20 February 2010
| IFTA Award for Best International Actor
| Sam Rockwell
|  
|- London Film London Film Critics Circle Awards   London Film 18 February 2010
| ALFS Award for British Director of the Year Duncan Jones
|  
|-
| ALFS Award for British Director of the Year
|  
|-
| ALFS Award for British Film of the Year
| Moon
|  
|- National Board of Review of Motion Pictures  National Board 12 January 2010
| NBR Award for Best Directorial Debut
| Duncan Jones
|  
|-
| NBR Award - Top Independent Films
| rowspan="2"|Moon
|  
|-
| Phoenix Film Critics Society Awards 
| 22 December 2009
| Overlooked Film of the Year
|  
|- Saturn Awards  36th Saturn 24 June 2010
| Saturn Award for Best Actor
| Sam Rockwell
|  
|-
| Saturn Award for Best Science Fiction Film
| Moon
|  
|-
| Seattle International Film Festival 
| 14 June 2009
| Golden Space Needle Award for Best Actor Sam Rockwell
|  
|- Sitges Film Festival  11 October 2009
| Best Actor
|  
|-
| Best Film
| Moon
|  
|-
| Best Production Design
| Tony Noble
|  
|-
| Best Screenplay
| Nathan Parker
|  
|-
| Writers Guild of Great Britain 
| 22 November 2010
| Writers Guild of Great Britain Award for Best First Feature-Length Film Screenplay
| Duncan Jones, Nathan Parker
|  
|}

==Sequels== cameo in the next film", says Jones, who ultimately hopes to complete a trilogy of films set in the same fictional universe.      

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 