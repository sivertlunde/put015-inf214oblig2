Johnny Dangerously
 
 
{{Infobox film
| name           = Johnny Dangerously
| image          = Johnny Dangerously movie poster.jpg
| alt            = 
| caption        = Theatrical release poster by Drew Struzan
| director       = Amy Heckerling
| producer       = Michael Hertzberg
| writer         = {{Plainlist|
* Harry Colomby
* Jeff Harris
* Bernie Kukoff
* Norman Steinberg}}
| starring       = {{Plainlist|
* Michael Keaton
* Joe Piscopo
* Marilu Henner
* Maureen Stapleton
* Peter Boyle
* Griffin Dunne
* Glynnis OConnor
* Dom DeLuise
* Richard Dimitri
* Danny DeVito
}} John Morris
| cinematography = David M. Walsh
| editing        = Pembroke J. Herring
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 minutes  
| country        = United States
| language       = English
| budget         = $9 million 
| gross          = $17.1 million
}}
Johnny Dangerously is a 1984 American parody of 1930s Crime film|crime/gangster movies. It was directed by Amy Heckerling; its four screenwriters included Bernie Kukoff and Jeff Harris.
 Alan Hale, Jr..

==Plot==
 
1935. A pet shop owner catches a young boy shoplifting a puppy. To discourage the kid from a life of crime, the owner tells a story.

1910. Young Johnny Kelly is a poor but honest newsboy in New York City. Johnny beats up Danny Vermin in self-defense and discovers Ma Kelly needs an operation they cannot afford. Since the execution of Johnnys father, Killer Kelly, his widow has supported Johnny and his younger brother, Tommy, who is fascinated by the law.

Johnnys fight with Vermin attracted the notice of local crime boss Jocko Dundee and, seeing no honest way to earn the operation money, agrees to work for Dundee, even though it probably means "breaking his mothers heart." He helps rob the nightclub belonging to Dundees rival, Roman Moronie. When asked his name, Johnny coins "Johnny Dangerously", but Moronie, a malapropist of swearwords, never "forgets a fargin face."

Years pass. With Mas continuing medical problems, Johnny goes to work for Dundee full-time. The whole neighborhood (including the Pope) knows that Kelly is really Johnny Dangerously, except for Ma and Tommy, who think he is a nightclub owner. Similarly, the gang knows nothing of Johnnys mother and brother. 

Johnny comes to Dundees headquarters&mdash;he is still involved in a running feud with Moronie&mdash;to find he has taken on two new gang members: Danny Vermin and his sidekick Dutch. Danny has lived up to his potential and become a total scumbag, with a taste for using opera audiences as shooting galleries with his .88 Magnum pistol. 

The two gangs war. In the meantime, Johnny falls for Lil Sheridan, a young showgirl new to the big city. ("Do you know your last name is an adverb?" she asks.) They go for a long walk together, ending in sexual fireworks. Eventually, Johnny negotiates a truce with Moronie.

A running gag has Ray Walston playing the owner of a newsstand who is repeatedly knocked out by a pile of newspapers flung from a delivery truck.  He temporarily loses one of his primary senses whenever he comes to.  At various points throughout the movie, his character alternates between blindness, deafness, and amnesia.

Meanwhile, Tommy graduates from law school (funded by Johnnys illicit earnings), and he goes to work for the District Attorney. D.A. Burr, on Johnnys payroll, tries to sidetrack Tommy, but he becomes a major public figure. After hearings looking into Moronies activities, the rival crime boss is deported to Sweden, though he protests that hes "not from there."

Burr and Vermin conspire to kill Tommy. Tommy is badly injured but survives. Johnny has Burr killed, but this leaves Tommy as the new D.A.

Vermin discovers that Dangerously is the D.A.s brother&mdash;and Tommy overhears Vermin chortling about it. Tommy confronts Johnny, who agrees to turn the evidence against himself to the Crime Commissioner&mdash;whom Vermin killed, framing Johnny. Not only that, Vermin steals Johnnys prized bubble gum case!

Johnny is arrested but says the holder of the case is the guilty party. Johnny is found guilty, sentenced to the electric chair and sent to death row. But when Vermin congratulates Tommy, Tommy notices that he has Johnnys case. Ma Kelly sucker punches Vermin in the crotch, and they realize that "Johnny didnt do it."

Johnny arrives on Death Row, where he receives rock star treatment from the starstruck warden. He receives word of Tommys danger, and plots an escape, prevailing on the warden to move up his execution. As he is taken to the chair, Johnny assembles what looks like a tommy gun from parts handed to him by inmates. He escapes in a laundry truck driven by Lil.

Johnny, through a wild car chase involving peeling layers of wallpaper from a car to keep changing its color, arrives at the movie theatre where Tommy is to be killed. He shoots and wounds Vermin, saving Tommy. The governor pardons Johnny as Vermin is arrested.

Back to 1935. The young shoplifter is round eyed. He is given a kitten as Johnny says "Crime doesnt pay." The kid goes on his way. Johnny, dressed in a tux, heads off in a limo with Lil, looks at the camera and admits, "Well, it paid a little!"

==Cast==
 
* Michael Keaton as Johnny Kelly (a.k.a. Johnny Dangerously)
** Byron Thames as young Johnny
* Joe Piscopo as Danny Vermin Georg Olden as young Danny
* Marilu Henner as Lil Sheridan
* Maureen Stapleton as Ma Kelly
* Peter Boyle as Jocko Dundee
* Richard Dimitri as Roman Troy Moronie
* Griffin Dunne as Tommy Kelly
** Troy W. Slaten as young Tommy
* Dom DeLuise as The Pope
* Danny DeVito as Burr
* Dick Butkus as Arthur
* Alan Hale, Jr. as the Desk Sergeant
* Glynnis OConnor as Sally Ron Carey as Pat
* Ray Walston as Vendor
* Neal Israel as Dr. Zilman
* Joe Flaherty (uncredited) as Death Row inmate
 

==Music== This Is the Life" was written for the film by "Weird Al" Yankovic, though for legal reasons, the song was not featured on home video releases of the film, until the DVD was released in 2002. The VHS home video version of the film featured a version of the Cole Porter song "Lets Misbehave".  The music video for Yankovics song incorporates scenes from the movie. 

==Critical reception== gangster comedy and both a clever homage to 1930s gangster films. But perhaps too clever for a mainstream audience. Foster (1995), p. 175  According to Leigh Hallisey, the film is a parody of "old-school" gangster movies and reveals Heckerlings awareness of their conventions and stereotypes. Hallisey (2002), p. 232  Foster finds the comedies of Amy Heckerling to be relying on "fast-paced, witty repartee and droll humor". She finds Heckerlings sense of comedy similar to those of Frank Tashlin and Jerry Lewis. Foster (1995), p. 175 

Martin F. Norden considers the film to be part of a trend in 1980s comedies, linking disability to humor. He notes that the film contains numerous gags of this nature. He spotlights a running gag, concerning a blind newspaper vendor played by Ray Walston. He starts out Blindness| blind but a bundle of newspapers hits him on the head, causing him to regain his sight. A repetition of the accident then leaves him Deafness|deaf. Norden (1994), p. 290 

==References==
 

;Bibliography
*  
*  
*  
*  

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 