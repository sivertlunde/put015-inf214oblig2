Perfect (film)
 
{{Infobox Film
| name           = Perfect
| image          = Perfect poster.jpg
| image_size     =
| caption        = Promotional poster
| director       = James Bridges
| producer       = James Bridges
| writer         = Aaron Latham (article) James Bridges
| narrator       = 
| starring = {{Plainlist|
* John Travolta
* Jamie Lee Curtis
* Anne De Salvo
* Marilu Henner
* Laraine Newman
* Matthew Reed
* Jann Wenner
}}
| music          = Narada Michael Walden
| cinematography = Gordon Willis
| editing        = Jeff Gourson
| studio         = Delphi III Pluperfect
| distributor    = Columbia Pictures
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = $20 million 
| gross          = $12,918,858 (US)
| preceded_by    = 
| followed_by    =
}}
Perfect is a 1985 American film drama, directed by James Bridges and starring John Travolta and Jamie Lee Curtis.

The film was based on a series of articles that appeared in Rolling Stone magazine in the late 1970s, chronicling the popularity of Los Angeles health clubs amongst single people.

==Plot==
Rolling Stone reporter Adam Lawrence (John Travolta) is sent from New York to Los Angeles to write an article about a businessman arrested for a drug dealing. During his stay in L.A., Adam sees a chance to collect material for another story about how "Fitness clubs are the singles bars of the 80s".

He visits "The Sport Connection," a popular gym where he meets workout instructor Jessie Wilson (Jamie Lee Curtis) and asks for an interview. Because of a previous bad experience with the press when she was a competitive swimmer, Jessie declines.

Adam joins the fitness club and soon coaxes other club members to tell him about the gym and its impact on their love lives. Some, such as fun-loving Linda and Sally, are all too candid about their experiences with the opposite sex.
 objective point of view.

Jessie comes to trust him. Less cynical than before, Adam makes a concerted effort to show Jessie that not all journalists are out for the cheap sensation. He writes an in-depth, fair-minded analysis of fitness clubs as a singles meeting scene. But it is deemed unacceptable by his boss, Rolling Stones editor in chief Mark Roth (Jann Wenner). 

Adams article is turned over to others for editing, using material supplied by colleague Frankie, a photographer.  She finds Jessies long-ago appearance in a magazine with embarrassing details about a romance. Adam travels for another assignment and is unaware of the changes being made in his story and too late to stop it. This has devastating impact on Jessie, as well as on others like Sally and Linda, described as "the most used piece of equipment in the gym." 
 the First Amendment, he decides not to comply with a judge who orders Adam to hand over tapes from the businessmans interview. Adam is jailed for contempt of court.

Jessie can see that Adam is a man of his word and believes him that he did not write the article the way it appeared in Rolling Stone.

==Cast==
 
* John Travolta – Adam Lawrence 
* Jamie Lee Curtis – Jessie Wilson
* Jann Wenner – Mark Roth
* Marilu Henner – Sally
* Laraine Newman – Linda
* Anne De Salvo – Frankie
* Mathew Reed – Roger
* John Napierala – City News Editor
* Stefan Gierasch – Charlie
* Ramey Ellis – City News Receptionist
* Alma Beltran – Grieving Woman
* Perla Walter – Grieving Woman
* Gina Morelli – Grieving Woman
* Philippe Delgrange – Maitre d in New York
* Tom Schiller – Carly Simons Friend Paul Kent – Judge
* Murphy Dunne – Peckerman
* Kenneth Welsh – Joe McKenzie
* Michael Laskin – Government Prosecutor
* Robert Stark – Government Prosecutor
* Laurie Burton – Mrs. McKenzie
* Ann Travolta – Mary
* Nanette Pattee-Francini – Nanette
* Robin Samuel – Robin
* Robert Parr – Robert
* Rosalind Allen – Sterling (as Rosalind Ingledew)
* Chelsea Field – Randy
* Dan Lewk – Steve
* Kenny Griswold – Kenny

 

==Reception==
The film was neither a commercial nor a critical success.    Perfect maintains a 19% rating on   for Worst Picture.  In a 1994 interview with Rolling Stone Magazine, Quentin Tarantino claimed this movie was "greatly underappreciated due Curtis very tight performance."
 John Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made. 

==Soundtrack==
{{Infobox album
| Name        = Perfect
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = June 4, 1985
| Recorded    = Pop
| Length      = 41:20
| Label       = Arista Records
| Producer    = Ralph Burns
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}

The soundtrack to Perfect was initially released in 1985 as a 12" vinyl record, and later re-released on CD.

;Side A
#"(Closest Thing To) Perfect" (Jermaine Jackson) – 3:50
#"I Sweat (Going Through the Motions)" (Nona Hendryx) – 3:54
#"All Systems Go" (Pointer Sisters) – 3:48
#"Shock Me" (Jermaine Jackson and Whitney Houston) – 5:08
#"Wham Rap! (Enjoy What You Do)" (Wham!) – 4:43

;Side B
#"Wear Out the Grooves" (Jermaine Stewart) – 4:33
#"Hot Hips" (Lou Reed) – 3:33
#"Talking to the Wall" (Dan Hartman) – 3:59
#"Masquerade" (Berlin (band)|Berlin) – 3:48 Lay Your Hands on Me" (Thompson Twins) – 4:11

==Filming locations== Sports Connection fitness club (now Sports Club/LA, a gigantic athletic facility in West Los Angeles), known for a place where the single persons were meeting. Jersey City.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 