Hell Below Zero
{{Infobox film
| name           = Hell Below Zero
| director       = Mark Robson
| image	         = Hell Below Zero FilmPoster.jpeg
| producer       = Irving Allen  Albert R. Broccoli
| writer         = Richard Maibaum Alec Coppel Max Trell
| based on       = novel by Hammond Innes
| starring       = Alan Ladd Stanley Baker
| music          = Clifton Parker
| cinematography = John Wilcox
| editing        = John D. Guthridge
| distributor    = Columbia Pictures
| country        = United Kingdom
| language       = English
| released       = 1954
| runtime        = 90 minutes
| budget         = 
| gross          = 
}}
Hell Below Zero (1954) is a murder mystery film, starring Alan Ladd in the second of his films for Warwick Films.

The film was directed by Mark Robson, and was written by Alec Coppel and Max Trell. The film was produced in colour, based on the novel The White South by Hammond Innes, and presents interesting footage of whaling fleets in action. 

==Plot==
The plot revolves around the death of Captain Nordahl, on a factory ship in Antarctic waters, lost overboard in mysterious circumstances. Captain Nordahl is an associate in a Norwegian whaling company, Bland-Nordahl. 

Duncan Craig, an American, played by Ladd, meets Judie Nordahl (Joan Tetzel), the captains daughter on his way to South Africa where he gets even with a business partner who cheated him. With little money left and a desire to see Judie again, Craig signs on to be a mate on the ship taking Judie to Antarctica.

On arrival in Antarctic waters, Craig finds suspicious evidence that seems to implicate Erik Bland (Stanley Baker), the new captain of the factory ship, in a conspiracy. Another murder follows and the film concludes with a dramatic showdown on the ice.

==Cast==
* Alan Ladd as Duncan Craig
* Joan Tetzel as Judie Nordhal
* Basil Sydney as Bland
* Stanley Baker as Erik Bland
* Joseph Tomelty as Capt. McPhee
* Niall MacGinnis as Dr. Howe Jill Bennett as Gerda Petersen
* Peter Dyneley as Miller
* Susan Rayne as Kathleen
* Philo Hauser as Sandeborg
* Ivan Craig as Larsen
* Paddy Ryan as Manders
* Cyril Chamberlain as Factory Ship Radio Operator
* Paul Homer as Kista Dan Radio Operator
* Edward Hardwicke as Ulvik
* John Witty as Martens
* Brandon Toomey as Christiansen
* Genine Graham as Stewardess
* Basil Cunard as Office Manager
* Fred Griffiths as Drunken Sailor
* John Warren as Hotel Receptionist
* Philip Ray as Capt. Petersen
* Paul Connell as Svensen
* Glyn Houston as Borg

==Production==
Shooting took place at Pinewood Studios. "The Future Programme", Kinematograph Weekly, 31 May 1956 p 14 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 