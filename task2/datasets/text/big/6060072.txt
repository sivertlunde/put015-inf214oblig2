Rubber Carpet
 
{{Infobox Film
| name           =  Rubber Carpet
| image          = 
| image_size     = 
| caption        =  John May
| producer       =  Suzanne Bolch John May
| narrator       = Jonathan Wilson Carlos Lopes Glenn Warner
| editing        =   
| studio         =  
| distributor    =  
| released       =  17 April 1997 (Canada)
| runtime        =  82 min. Canada
| English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Rubber Carpet is a 1997  )  but failed to find a distribution company. It was filmed in Toronto, Ontario for a very small budget (approximately $10,000 CDN)  and released in April, 1997.

== Plot ==
Ansel (Wilson) is a wanna-be artist, full-time dishwasher who isnt nearly as good a painter as he thinks he is. When he quits his menial but stable job to pursue his art career, his girlfriend Tallulah (Coffey) is furious. They break up and Ansel spends his days creating horrible art (like a puck floating in urine) and trying to earn government grants.

Meanwhile, Talluah is being driven crazy by her upstairs neighbour who constantly plays Eric Claptons "Layla"  at a loud volume. Banadek (Richard Sali) is Ansels former co-worker and dishwasher. He spends the course of the film talking about the process of dishwashing. To him it is a religious experience.

The film consists of the two main characters discussing their breakup in monologue, mixed with scenes of the two going through typical post-break up rituals, like giving back each others possessions. Eventually, they come to a realization about themselves, and their roles in each other lives.

== Cast == Jonathan Wilson
* Tallulah – Judy Coffey
* Banadek – Richard Sali 

== Crew == John May & Suzanne Bolch
*Produced by Suzanne Bolch Carlos Lopes Glenn Warner
*Assistant Director: Daniel J. Murphy 

==References==
 

==External links==
* 

 
 

 