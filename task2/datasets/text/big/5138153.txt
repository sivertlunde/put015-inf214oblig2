Enchanted (film)
 
{{Infobox film
| name           = Enchanted
| image          = Enchantedposter.jpg
| caption        = Theatrical release poster
| director       = Kevin Lima
| producer       =  
| writer         =  
| narrator       = Julie Andrews
| starring       =  
| music          = Alan Menken   Don Burgess
| editing        =  
| studio         = Walt Disney Pictures   Josephson Entertainment Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $85 million 
| gross          = $340.5 million 
}} musical live-action animated fantasy fantasy romantic Bill Kelly and directed by Kevin Lima, the film stars Amy Adams, Patrick Dempsey, James Marsden, Timothy Spall, Idina Menzel, Rachel Covey, and Susan Sarandon. The plot focuses on Giselle, an archetypal Disney Princess, who is forced from her traditional animated world of Andalasia into the live-action world of New York City. Enchanted was the first Disney film to be distributed by Walt Disney Studios Motion Pictures, instead of Buena Vista Pictures Distribution.
 homage to, parody of, Stephen Schwartz, who had written songs for previous Disney films, produced the songs of Enchanted, with Menken also composing its score.
 James Baxter Best Original Song nominations at the 80th Academy Awards.

==Plot==
  true love". Knowing Narissas wishes, Nathaniel sets the troll free to get rid of Giselle, but Edward saves her just in time. When they meet, they instantly fall in love and plan to get married the following day.

However, Narissa had witnessed everything, so she meets Giselle while the peasant runs off to get wed to Edward. Narissa, disguised as an old hag, tricks Giselle and exiles her to the real world of Earth by falling through a portal that is under a sewer in New York Citys Times Square. Meanwhile, a divorce lawyer named Robert prepares to propose to his long time girlfriend Nancy, much to the dismay of Morgan, his young daughter. While Robert and Morgan are heading home, they see a confused Giselle trying to enter a brightly lit pink castle on a billboard which she has mistaken for the Andalasian palace. Luckily, Robert rescues her after she falls off, and he begrudgingly permits Giselle to stay at his apartment at the insistence of his daughter.

Pip, Giselles chipmunk friend from Andalasia, witnessed the events of Giselles fall through the portal and alerts Edward to it, so they embark on a rescue mission. Edward chances upon Giselles whereabouts via a TV news report of her being interviewed. However, he is unaware that Nathaniel, who had been dispatched to New York City by Narissa, is trying to eliminate Pip after the chipmunk discovered a poisoned-apple plot that Narissa and Nathaniel had concocted to kill Giselle. Meanwhile, Robert decides to keep Giselle close by, given her naivete and lack of means to survive on her own. Giselle questions the divorce lawyer about his relationship with Nancy, and decides to help the pair reconcile by sending flowers and tickets to the "King and Queens Costume Ball". An angered Narissa, who has continued to spy on events, makes plans to come to New York City after Nathaniel failed twice to poison Giselle.

As they spend more time together, Giselle and Robert begin to develop feelings for each other while Edward continues to look for Giselle, eventually finding her at Roberts apartment. While Edward is eager to take Giselle home to Andalasia and finally marry, she insists that they should first go on a date, still conflicted about her feelings. Giselle promises to return to Andalasia after ending their date at the Kings and Queens Ball, which Robert and Nancy are also attending. Giselle and Robert finally dance as if they were the only couple in the room. From afar, Edward and Nancy sense the attraction between Giselle and Robert, and also discover a mutual attraction between themselves. At the climax of the ball, Narissa appears as the old hag and offers the last poisoned apple to Giselle, who was so bereft at the thought of leaving her true love behind that she takes a bite and falls to the floor unconscious.

Nathaniel, sick of being manipulated by Narissa, reveals their plot. Robert realizes that true loves kiss is the only force that will break the poison apples spell before midnight. After Edwards kiss fails to wake Giselle, he prompts Robert to do so instead. Giselle awakens as the clock strikes twelve, but Narissa uses this moment of distraction to break free from Nathaniels grasp and transform into a giant blue dragon. When Robert shields Giselle after Narissa threatens to kill her, the evil queen takes Robert hostage instead.  Giselle takes Edwards sword then follows Narissa out the window and up to the top of the Woolworth Building. With Pips help, Giselle successfully defeats Narissa, who plunges to her death. After almost falling off the roof themselves, Robert and Giselle share a passionate kiss and embrace, while Edward and Nancy, finding themselves in love, depart to Andalasia via manhole.

A montage of the protagonists new lives follows, showing Edward and Nancy marrying, Giselle running a successful fashion business and forming a happy family with Robert and Morgan, while Nathaniel becomes a best-selling author on Earth, as does Pip in Andalasia.

==Cast== Disney princess" Snow White, Cinderella and Sleeping Beauty... Ariel a.k.a. The Little Mermaid."    She is "eternally optimistic and romantic" but is also "very independent and true to her convictions".  Over the course of the film, she becomes more mature but maintains her innocence and optimism. straight man to Adams and Marsdens more outrageous characters.     
* James Marsden as Prince Edward   A narcissistic and athletic, yet good-hearted, prince who ends up confused with the world of New York once entering it. Marsden was announced to have been cast on December 6, 2005.  At the time Marsden was auditioning, the role of Robert had not been cast but he decided to pursue the role of Prince Edward because he was "more fun and he responded more to that character."    Edward is a prince in Andalasia and the stepson of Narissa. He is "very pure, very simple-minded and naive, but innocently narcissistic."  Alice in Wonderland in which he was the voice of Bayard the Bloodhound.
*   in Frozen (2013 film)|Frozen, during which she worked with director Chris Buck, who previously directed Tarzan (1999 film)|Tarzan with Lima in 1999.
* Rachel Covey as Morgan   Morgan is Roberts six-year-old daughter. Despite her father misunderstanding her and telling her otherwise, she believes in fairy tales and believes that magic exists. Disney villains Evil Queen Snow White Maleficent from Sleeping Beauty. 
* Jeff Bennett and Kevin Lima as Pip   Bennett provided the voice for the hand drawn animated Pip in the animated segment while Lima provided the voice for the computer-generated Pip in the live-action segment. Pip, a chipmunk friend of Giselle who has no trouble expressing himself through speech in Andalasia, loses his ability to speak in the real world and must communicate by acting. So Close" at the ball while Robert and Giselle dance together as do Edward and Nancy.
* Fred Tatasciore as the Troll from Andalasia who tried to eat Giselle
Several actresses who have played characters in Disney films have cameo appearance|cameos: Belle in Beauty and the Beast. Ariel in The Little Mermaid; she also voiced the various Barbie dolls in Toy Story 2 and Toy Story 3.
*  . title character Mary Poppins The Princess Diaries series as Queen Clarisse Renaldi.

==Production==

===Development=== Bill Kelly, Josephson Productions National Treasure franchise. Adam Shankman became the films director in 2003, while Bob Schooley and Mark McCorkle were hired by Disney to rewrite the script once again.  At the time, Disney considered offering the role of Giselle to Kate Hudson or Reese Witherspoon.  However, the project did not take off.
 Walt Disney Studios, he received the green light for the project and a budget of $85 million.     Lima began designing the world of Andalasia and storyboarding the movie before a cast was chosen to play the characters. After the actors were hired, he was involved in making the final design of the movie, which made sure the animated characters look like their real-life counterparts. 

===Filming===
Enchanted is the first feature-length Disney live-action/traditional animation hybrid since Disneys   logo and Enchanted storybook are shown, and then switches to a smaller 1.85:1 aspect ratio for the first animated sequence. The film switches back to 2.35:1 when it becomes live-action and never switches back, even for the remainder of the cartoon sequences. When this movie was aired on televised networks, the beginning of the movie (minus the logo and opening credits) was shown in standard definition; the remainder of the movie was shown in high definition when it becomes live-action. Lima oversaw the direction of both the live-action and animation sequences, which were being produced at the same time.  Enchanted took almost two years to complete. The animation took about a year to finish while the live-action scenes, which commenced filming on location in New York City during the summer of 2006 and were completed during the animation process, were shot in 72 days. 

====Animation==== traditional cel Sleeping Beauty, Snow White Old Yeller, The Shaggy Swiss Family Bon Voyage!, computer graphics James Baxter James Baxter. Jessica Rabbit Belle (Beauty Beauty and Rafiki (The The Hunchback of Notre Dame).  
 Disney princess. And not a caricature." Seeing Giselle as "a forest girl, an innocent nymph with flowers in her hair" and "a bit of a hippie", the animators wanted her to be "flowing, with her hair and clothes. Delicate."    For Prince Edward, Baxters team "worked the hardest on him to make him look like the actor" because princes "in these kinds of movies are usually so bland."  Many prototypes were made for Narissa as Baxters team wanted her face to "look like Susan Sarandon. And the costumes had to align closely to the live-action design." 

To maintain continuity between the two media, Lima brought in costume designer Mona May during the early stages of the films production so the costumes would be aligned in both the animated and live-action worlds. He also shot some live-action footage of Amy Adams as Giselle for the animators to use as reference, which also allowed the physical movement of the character to match in both worlds. Test scenes completed by the animators were shown to the actors, allowing them to see how their animated self would move. 

====Live-action====
  and James Marsden during filming in Columbus Circle.]]
Principal photography began in April 2006 and ended in July 2006.  Because of the sequence setting, the live action scenes are filmed in New York City. However, shooting in New York became problematic as it was in a "constant state of new stores, scaffolding and renovation". 
 Moulin Rouge beforehand, and included 300 extras and 150 dancers. 
 Riverside Drive 116th Street, which is the residence of the films characters Robert and Morgan.

===Costume design===
 .]] The Haunted Mansion. To create the costumes, May spent one year in pre-production working with animators and her costume department of 20 people, while she contracted with five outside costume shops in Los Angeles and New York City.    She became involved in the project during the time when the animators are designing the faces and bodies of the characters as they had to "translate the costumes from two-dimensional drawings to live-action human proportion".    Her goal was to keep the designs "Disneyesque to the core but bring a little bit of fashion in there and humor and make it something new".  However, May admitted this was difficult "because theyre dealing with iconic Disney characters who have been in the psyche of the viewing audience for so long".   
 hoop that holds up 20 layers of petticoats and ruffles.  Altogether, 11 versions of the dress are made for filming, each comprised 200 yards (183 m) of silk satin and other fabric, and weighed approximately 40 pounds (18&nbsp;kg).   On the experience of wearing the wedding dress, Amy Adams described it as "grueling" since "the entire weight was on her hips, so occasionally it felt like she was in traction". 

Unlike Giselle, Prince Edward does not adapt to the real world and James Marsden, who plays Edward, had only one costume designed for him. Mays aim was to try "not to lose Marsden in the craziness of the outfit... where he still looks handsome".  The costume also included padding in the chest, buttocks and crotch, which gave Marsden the "same exaggerated proportions as an animated character"  and "posture – his back is straight, the sleeves are up and never collapse". 
 crown that horns during Narissas transformation into a dragon. 

===Music===
  Disney films Stephen Schwartz The Hunchback of Notre Dame.
 Snow White Schwartz because Snow White and the Seven Dwarfs and Cinderella (1950 film)|Cinderella.  Accordingly, Amy Adams performed the first song in an operetta style in contrast to the Broadway style of the later songs. 
 Snow White Mary Poppins) The Little Beauty and Schwartz admitted So Close" and the country/pop number "Ever Ever After" (sung by Carrie Underwood as a voice-over). 

Out of the six completed songs written and composed by Menken and Schwartz, five remained in the finished film. The title song, "Enchanted," a duet featuring Idina Menzel and James Marsden, was the only song of Menkens and Schwartzz authorship and composition that was deleted from the movie. 

===Effects=== CG characters that performed alongside real actors, namely the animated animals during the "Happy Working Song" sequence, Pip and the Narissa dragon during the live action portions of the film. CIS Hollywood was responsible for 36 visual effects shots, which primarily dealt with wire removals and composites. Reel FX Creative Studios did four visual effects shots involving the pop-up book page-turn transitions while Weta Digital did two.   

Out of all the animals that appear in the "Happy Working Song" sequence, the only real animals filmed on set were rats and pigeons. The real animals captured on film aided Tippett Studio in creating CG rats and pigeons, which gave dynamic performances such as having pigeons that carried brooms in their beaks and rats that scrubbed with toothbrushes. On the other hand, all the cockroaches were CG characters.   
 Maya and Furrocious.  When visual effects supervisor Thomas Schelesny showed the first animation of Pip to director Kevin Lima, he was surprised that he was a looking at CG character and not reference footage.    To enhance facial expressions, the modelers gave Pip eyebrows, which real chipmunks do not have.  During the filming of scenes in which Pip appears, a number of ways were used to indicate the physical presence of Pip. On some occasions, a small stuffed chipmunk with a wire armature on the inside was placed in the scene. In other situations, a rod with a small marker on the end or a laser pointer would be used to show the actors and cinematographer where Pip is. 

Unlike Pip, the Narissa dragon was allowed to be more of a fantasy character while still looking like a living character and a classic Disney villain.   The CG dragon design was loosely based on a traditional Chinese dragon and Susan Sarandons live-action witch.  When filming the scene which sees the transformation of Narissa from a woman into a dragon, a long pole was used to direct the extras eyelines instead of a laser pointer. Set pieces were made to move back and forth in addition to having a computer-controlled lighting setup and a repeatable head on the camera that were all synchronized together. In the films final sequence, in which Narissa climbs the Woolworth Building while clutching Robert in her claws, a greenscreen rig was built to hold Patrick Dempsey in order to film his face and movements. The rig was a "puppeteering" approach that involved a robotic arm being controlled by three different floor effects artists. 

==Release==
The film was distributed by Walt Disney Studios Motion Pictures to 3,730 theaters in the United States.    It was distributed worldwide by Walt Disney Studios Motion Pictures International to over 50 territories around the world  and topped the box office in several countries including the United Kingdom and Italy.   It is the first movie to be released under the Walt Disney Studios Motion Pictures name following the retirement of the previous Buena Vista Pictures Distribution.

=== Merchandising ===
Disney had originally planned to add Giselle to the Disney Princess line-up, as was shown at a 2007 Toy Fair where the Giselle doll was featured with packaging declaring her with Disney Princess status, but decided against it when they realized they would have to pay for lifelong rights to Amy Adams image.  While Giselle is not being marketed as one of the Disney Princesses, Enchanted merchandise was made available in various outlets with Adams animated likeness being used on all Giselle merchandise. Giselle led the 2007 Hollywood Holly-Day Parade at Disneys Hollywood Studios.  She was also featured in the 2007 Walt Disney World Christmas Day Parade in the Magic Kingdom with the official Disney Princesses.
 video game based on the film was released for Nintendo DS and mobile phones in addition to a Game Boy Advance title, Enchanted: Once Upon Andalasia, which is a prequel to the film, about Giselle and Pip rescuing Andalasia from a magic spell.

=== Home media === DVD by I Am Legend, the Blu-ray Disc sales of I Am Legend were nearly four times the number of Blu-ray Disc sales of Enchanted.  The DVD was released in United Kingdom and Europe on April 7, 2008,  and in Australia on May 21, 2008. 

The bonus features included on both the DVD and Blu-ray Disc are "Fantasy Comes to Life", a three-part behind-the-scenes feature including "Happy Working Song", "Thats How You Know" and "A Blast at the Ball"; six   stores contain a bonus DVD with a 30-minute long making-of documentary titled Becoming Enchanted: A New Classic Comes True. This DVD is also sold with certain DVDs at HMV stores in the United Kingdom.

==Reception==

=== Box office performance ===
Enchanted earned $7,967,766 on the day of its release in the United States, placing at #1. It was also placed at #1 on Thanksgiving Day, earning $6,652,198 to bring its two-day total to $14.6 million. The film grossed $14.4 million on the following day, bringing its total haul to $29.0 million placing ahead of other contenders. Enchanted made $34.4 million on the Friday-Sunday period in 3,730 theaters for a per location average of $9,472 and $49.1 million over the five-day Thanksgiving holiday in 3,730 theaters for a per location average of $13,153.  Its earnings over the five-day holiday exceeded projections by $7 million.  Ranking as the second-highest Thanksgiving opening after Toy Story 2, which earned $80.1 million over the five-day holiday in 1999, Enchanted is the first film to open at #1 on the Thanksgiving frame in the 21st century. 

In its second weekend, Enchanted was also the #1 film, grossing a further $16,403,316 at 3,730 locations for a per theater average of $4,397. It dropped to #2 in its third weekend, with a gross of $10,709,515 in 3,520 theaters for a per theater average of $3,042. It finished its fourth weekend at #4 with a gross of $5,533,884 in 3,066 locations for a per theater average of $1,804. Enchanted earned a gross of $127,807,262 in the United States and Canada as well a total of $340,487,652 worldwide.  It was the 15th highest-grossing film worldwide released in 2007.

===Critical response===
Enchanted received very positive reviews from critics. As of September 2014, the movie review aggregate website Rotten Tomatoes had tallied the film at an overall 93% approval rating (based on 187 reviews, with 174 "fresh" and 13 "rotten"),  while Metacritic gave it a rating of 75 out of 100 based on 32 reviews.  Rotten Tomatoes ranked the film as the ninth best reviewed film in wide release of 2007 and named it the best family film of 2007.  

Positive reviews praised the films take on a classic Disney story, its comedy and musical numbers as well as the performance of its lead actress, Amy Adams. Roger Ebert of Chicago Sun-Times gave the film three stars out of four, describing it as a "heart-winning musical comedy that skips lightly and sprightly from the lily pads of hope to the manhole covers of actuality" and one that "has a Disney willingness to allow fantasy into life".  Film critics of Variety (magazine)|Variety and LA Weekly remarked on the films ability to cater for all ages. LA Weekly described the film as "the sort of buoyant, all-ages entertainment that Hollywood has been laboring to revive in recent years (most recently with Hairspray (2007 film)|Hairspray) but hasnt managed to get right until now",  while Todd McCarthy of Variety commented, "More than Disneys strictly animated product, Enchanted, in the manner of the vast majority of Hollywood films made until the 60s, is a film aimed at the entire population – niches be damned. It simply aims to please, without pandering, without vulgarity, without sops to pop-culture fads, and to pull this off today is no small feat."    Enchanted was the Broadcast Film Critics Associations choice for Best Family Film of 2007, while Carrie Rickey of The Philadelphia Inquirer named it the 4th best film of 2007. 

Rolling Stone, Premiere (magazine)|Premiere, USA Today, and The Boston Globe all gave the film three out of four,         while Baltimore Sun gave the film a B grade.    They cited that although the story is relatively predictable, the way in which the predictability of the film is part of the story, the amazingly extravagant musical numbers, along with the way in which Disney pokes fun at its traditional line of animated movies outweighs any squabbles about storyline or being unsure of what age bracket the film is made for. Michael Sragow of Baltimore Sun remarked that the films "piquant idea and enough good jokes to overcome its uneven movie-making and uncertain tone",  while Claudia Puig of USA Today stated that "though its a fairly predictable fish-out-of-water tale (actually a princess-out-of-storybook saga), the casting is so perfect that it takes what could have been a ho-hum idea and renders it magical." 
 Michael Phillips, who gave the film positive reviews on At the Movies with Ebert & Roeper, emphasized the effect of Adams performance on the film with remarks like "Amy Adams is this movie" and "Amy Adams shows how to make a comic cliché work like magic." However, both agreed that the final sequence involving the computer-generated dragon of Narissa "bogged down" the film. 

Empire (magazine)|Empire stated that the film was targeted at children but agreed with other reviewers that the "extremely game cast" was the films best asset. It gave the film three out of five.  TIME gave the film a C-, stating that the film "cannibalizes Walts vault for jokes" and "fails to find a happy ending that doesnt feel two-dimensional".  Similarly, Peter Bradshaw of The Guardian commented that the film "assumes a beady-eyed and deeply humourless sentimentality" and that Adams performance was the "only decent thing in this overhyped family movie covered in a cellophane shrink-wrap of corporate Disney plastic-ness". Bradshaw gave the film two out of five. 

===Accolades===
{| class="wikitable sortable" width="100%"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|- Academy Awards  80th Academy February 24, 2008 Academy Award Best Original Song Stephen Schwartz
|  
|- So Close" – Alan Menken and Stephen Schwartz
|  
|-
| "Thats How You Know" – Alan Menken and Stephen Schwartz
|  
|-
| Costume Designers Guild  January 17, 2008
| Excellence in Fantasy Film
| Mona May
|  
|-
| rowspan="4"|Critics Choice Movie Awards    13th Critics January 7, 2008 Best Actress
| Amy Adams
|  
|-
| Best Film - Family
|
| 
|- Best Composer
| Alan Menken
|  
|- Best Song
| Thats How You Know - Alan Menken
|  
|-
| Detroit Film Critics Society
| December 21, 2007
| Best Actress
| Amy Adams
|  
|- Golden Globe Awards  65th Golden January 13, 2008 Best Actress – Motion Picture Musical or Comedy
| Amy Adams
|  
|- Best Original Song
| "Thats How You Know" – Alan Menken and Stephen Schwartz
|  
|-
| Golden Trailer Awards 
| 2007
| Best Animation/Family Feature Film 
|
|  
|- Grammy Awards  51st Annual February 8, 2009 Grammy Award Best Song Written for Motion Picture, Television or Other Visual Media
| "Ever Ever After"- Alan Menken and Stephen Schwartz 
| 
|-
| "Thats How You Know" - Alan Menken and Stephen Schwartz 
|  
|-
| Motion Picture Sound Editors 
| 2008
| Best Sound Editing: Music in a Musical Feature Film
| Kenneth Karman, Jermey Raub and Joanie Diener
|  
|- MTV Movie Awards   2008 MTV June 1, 2008
| Best Female Performance 
| Amy Adams
|  
|-
| Best Comedic Performance 
| Amy Adams
|  
|-
| Best Kiss
| Amy Adams and Patrick Dempsey
|  
|-
| Ohio Film Critics Association
| January 11, 2008
| Best Actress
| Amy Adams
|  
|-
| Phoenix Film Critics Society 
| December 18, 2007
| Best Live Action Family Film
|
| 
|- Satellite Awards  12th Satellite December 16, 2007 Best Actress – Motion Picture Musical or Comedy
| Amy Adams
|  
|- Best Visual Effects
| Thomas Schelesny, Matt Jacobs and Tom Gibbons
|  
|- Saturn Awards  34th Saturn June 24, 2008 Best Fantasy Film
|
|  
|- Best Actress
| Amy Adams
|  
|- Best Music
| Alan Menken
|  
|- Teen Choice Awards   2008 Teen August 4, 2008
| Choice Movie: Chick Flick
|
|  
|-
| Choice Movie Actress: Comedy
| Amy Adams
|  
|-
| Choice Movie Actor: Comedy
| James Marsden ( also for 27 Dresses )
|  
|-
| Choice Movie: Villain
| Susan Sarandon
|  
|-
| Utah Film Critics Association
| December 28, 2007
| Best Actress
| Amy Adams
|  
|-
| Visual Effects Society  February 10, 2008
| Outstanding Animated Character in a Live Action Motion Picture
| Thomas Schelesny, Matt Jacobs and Tom Gibbons
|  
|}

==Disney references==
 
  Walt Disney Bill Kelly, the writer, to inject Disney references to the plot, it became "an obsession"; he derived the name of every character as well as anything that needed a name from past Disney films to bring in more Disney references. 
 The Little Thumper and Flower from Snow White Sleeping Beauty.  Dick Cook, the chairman of Walt Disney Studios, admitted that part of the goal of Enchanted was to create a new franchise (through the character of Giselle) and to revive the older ones.  

==Sequel==
In February 2010, Variety (magazine)|Variety reported that Walt Disney Pictures planned to film a sequel with Barry Josephson and Barry Sonnenfeld producing again. Jessie Nelson was attached to write the screenplay and Anne Fletcher to direct. Disney hoped the cast members from the first film would return and for a release as early as 2011. 

On January 12, 2011, composer Alan Menken was asked about the sequel in an interview. His reply was, "I’ve heard things but theres nothing yet. I don’t know much about what’s happening with that. Honestly, I don’t know what the studio wants to do next. I presume there will be some future projects for me to work on. I love doing that, I really do. But I’m not frustrated that it isn’t one of them. At the moment I have a lot of stage things happening and I’m busy enough with that, so I really don’t need more on my plate." 

On March 28, 2011, in an interview for his latest film, Hop (film)|Hop, James Marsden was asked about the sequel. 
 

As of July 2014, Disney had hired screenwriters J. David Stem and David N. Weiss to write a script for a sequel and also hired Anne Fletcher to direct the film. 

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  
*  
*  

{{Navboxes|list1=
 
 
 
 
 
 
 
 }}

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 