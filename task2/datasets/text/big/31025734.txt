El Seductor
{{Infobox film
| name           =El Seductor
| image          = 
| image size     =
| caption        =
| director       =Chano Urueta
| producer       = Guillermo Calderónm, Luis García de León
| writer         =  Rafael García Travesi
| narrator       =
| starring       =  Ramón Gay, Amanda del Llano, Ana Luisa Peluffo |
| music          = Antonio Díaz Conde
| cinematography = Agustín Jiménez
| editing        = Jorge Bustos
| distributor    = 
| released       = 29 September 1955 (Mexico)
| runtime        = 81 min 
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1955 Mexico|Mexican film. It was directed by 
Chano Urueta.

==Cast==
	 
* Ramón Gay	 ...	Juan Alberto
* 	 Amanda del Llano	 ...	Aurora; Angélica
* 	 Ana Luisa Peluffo	 ...	Raquel
* 	 José Luis Jiménez	 ...	Guillermo
* 	 Emma Roldán	 ...	Nana
* 	 Miguel Manzano	 ...	Diputado Gómez
* 	 Mercedes Soler		
* 	 Antonio Raxel	 ...	Esposo de Aurora
* 	 Roy Fletcher		
* 	 Rebeca Sanromán	 ...	(as Rebeca San Roman)

==External links==
*  

 
 
 
 
 


 