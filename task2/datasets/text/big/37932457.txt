Agaya Gangai (film)
 
 
{{Infobox film
| name     =  Agaya Gangai
| image   =  Aagayagangaifilm.jpg
| image_size     =
| caption        =
| director       = Manobala
| producer       = M. Nachiappan
| writer         = Manivannan Karthik Suhasini Suhasini  Haja Shareef Goundamani
| music          = Ilaiyaraaja
| cinematography = Robert Rajasekaran
| editing        = R. Bhaskaran
| studio         = Sreeni Enterprises
| distributor    = Sreeni Enterprises
| released       =  
| country        = India Tamil
}}
 1982 Cinema Indian Tamil Tamil film, directed by Manobala,  starring Karthik and Suhasini in lead roles. The film had musical score by Ilayaraja and was released on 1982.

==Cast== Karthik
*Suhasini Suhasini
*Haja Shareef
*Goundamani Thyagu
*Kamala Kamesh
*S.N.Parvathi
*Usha

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Mama Mama || S. P. Sailaja || Gangai Amaran || 04:20
|-
| 2 || Mega Deepam || Malaysia Vasudevan || Na. Kamarasan || 04:09
|-
| 3 || Poongum Agaya Gangai || S. Janaki, Gangai Amaran || Vairamuthu || 02:46
|-
| 4 || Then Aruviyil || S. P. Balasubrahmanyam, S. Janaki || Mu. Metha || 04:54
|}

==References==
 

 
 
 
 
 


 