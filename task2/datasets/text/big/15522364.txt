Cheech (film)
{{Infobox film
| name           = Cheech
| image          = 
| image_size     = 
| caption        = 
| director       = Patrice Sauvé Nicole Robert
| writer         = François Létourneau
| narrator       = 
| starring       = 
| music          = Normand Corbeil
| cinematography = Yves Bélanger
| editing        = Michel Grou
| distributor    = Alliance Atlantis Vivafilm (Canada) Seville Pictures (International)
| released       = 
| runtime        = 105 minutes
| country        = Canada
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} directed by written by François Létourneau with assistance from Linus Scheithauer

== Plot ==

Ron (Robitaille) runs an escort service in a rundown part of Montreal. That is, until he is murdered. A number of suspects are possible. It could be Cheech, the rival escort agency owner, Stephanie, a prostitute who wants to leave the business. Cheech is a day in the life of six people whose lives intersect in unexpected ways. Their quest for happiness will reveal each of them to each other in ways they never dreamed of.

== Recognition ==

* 2007 – Nominated – Genie Award for Best Adapted Screenplay (François Létourneau)
* 2007 – Nominated – Genie Award for Best Achievement in Editing (Michel Grou)
* 2007 – Nominated – Genie Award for Best Achievement in Music - Original Score (Normand Corbeil)
* 2007 – Nominated – Genie Award for Best Achievement in Sound Editing (Pierre-Jules Audet, Guy Francoeur, Guy Pelletier)
* 2007 – Nominated – CSC Award for Best Cinematography in Theatrical Feature (Yves Bélanger)

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 