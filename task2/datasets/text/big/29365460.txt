Turquoise (film)
{{Infobox film
| name           = Turquoise
| image          = TurquoiseFilmPoster.jpg
| alt            = 
| caption        = Theatrical Poster
| director       = Kadir Balcı
| producer       = Dirk Impens   Gülin Üstün
| writer         = Kadir Balcı
| starring       = Burak Balcı   Charlotte Vandermeersch   Nihat Alptuğ Altınkaya   Tilbe Saran   Sinan Vanden Eynde   Hilal Sönmez
| music          = Bert Ostyn
| cinematography = Ruben Impens
| editing        = Nico Leunen
| studio         = MMG Film & TV Production
| distributor    = MMG Film & TV Production
| released       =  
| runtime        =
| country        = Belgium   Turkey
| language       = Flemish   Turkish
| budget         = 
| gross          = 
}} Turkish drama film written and directed by Kadir Balcı about the identity crisis experienced by three Turkish brothers who return to the Belgian city of Ghent after they bury their father in Istanbul. The film, which went on nationwide release across Belgium on  , was selected for the 16th London Turkish Film Festival.   

==Production==
The film was shot on location in Istanbul, Turkey and Ghent, Belgium.

==Release==

=== General release ===
The film opened on general release in 9 screens across Belgium on   at number twelve in the national box office chart with an opening weekend gross of US$63,177.   

=== Festival screenings ===
* 16th London Turkish Film Festival (4–18 November 2010)   

==See also==
* 2010 in film
* Turkish films of 2010

==References==
 

==External links==
*  
*  

 

 
 
 
 
 