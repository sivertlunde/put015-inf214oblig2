Paths to Paradise
{{infobox film
| name           = Paths to Paradise
| image          =
| imagesize      =
| caption        =
| director       = Clarence Badger
| producer       = Adolph Zukor Jesse Lasky
| writer         = Paul Armstrong(play:The Heart of a Thief) Keene Thompson(scenario)
| starring       = Raymond Griffith Betty Compson Thomas Santschi Edgar Kennedy
| music          =
| cinematography = H. Kinley Martin
| editing        =
| distributor    = Paramount Pictures
| released       = June 29, 1925
| runtime        = 7 reels (6,741 feet)
| country        = United States
| language       = Silent film (English intertitles)
}} 1925 silent silent comedy directed by Clarence Badger, produced by Famous Players-Lasky and distributed by Paramount Pictures. The film is based on a 1914 play, The Heart of a Thief, by Paul Armstrong and stars Raymond Griffith and Betty Compson. The film was lost for many decades until an incomplete print surfaced in the 1970s. Essentially complete, the film is missing its final reel which is usually filled in with synopsis by historians and film fans.  
 

==Cast==
*Raymond Griffith - The Dude from Duluth
*Betty Compson - Molly
*Thomas Santschi - Chief of Detectives Callahan
*Bert Woodruff - Brides Father
*Fred Kelsey - Confederate

uncredited
*Clem Beauchamp

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 