Naan (film)
 
{{Infobox film
| name           = Naan
| image          = Naan (film).jpg
| director       = Jeeva Sankar 
| producer       = Fathima Vijay Antony
| writer         = Jeeva Sankar Neelan.K.Sekar (dialogue)
| screenplay     = Jeeva Sankar 
| story          = Jeeva Sankar 
| starring       = Vijay Antony Siddharth Venugopal Rupa Manjari
| designs        =  
| music          = Vijay Antony
| cinematography = Jeeva Shankar
| editor         = Surya
| studio         = Vijay Antony Film Corporation
| distributor    = Vijay Antony Film Corporation
| released       =  
| runtime        = 143 minutes
| language       = Tamil
| budget         =  
| gross          = 
}}

Naan ( ) is a 2012 Tamil crime thriller film directed by Jeeva Shankar is inspired by Hollywood flick The talented Mr.Ripley. It has composer Vijay Antony in the lead role as a psychopath killer, who also produced it and worked as the music director. Siddharth Venugopal and Rupa Manjari appeared in supporting roles. The filming of Naan began in April 2010.  The film was released on 15 August 2012 and received positive response. This film was declared a "hit" in the box-office at the end of its run. It is being remade in Bengali as Amanush 2 which is slated to release in April 2015.

==Plot==
Karthik(Vijay Antony) is a brilliant student.  The school head master catches him once for forging the signature of his friends parent in the mark sheet. The head master sends for his father. He goes back to his house and knocks the door, but there is no response. So he peeps through the window and is shocked to find his mom with his uncle. His mother opens the door and pleads him not to tell his father, but he does and so his father commits suicide.  Karthiks mother continues her relationship which leads  Karthik to kill his mother and his uncle by setting the house in fire.  He is sent to juvenile home where he grows up. On release, the jail warden gives him the address of his uncle (fathers younger brother) and asks him to concentrate on his studies. He goes to this address and realises that his uncles wife is not interested in giving shelter to Karthik. So he leaves to Chennai by bus to start a new life.

As fate would have it, the bus meets with an accident and his co-passenger Salim dies. Karthik picks up his certificates and joins a medical college by changing his identity . He develops friendship with a rich guy in his college, named Ashok (Siddharth Venugopal) and also with his girlfriend Rupa (Rupa Manjari) who are befriended with a guy named Suresh  . Ashok allows Karthik to stay in his house,suggested by Rupa so that Karthik need not have to shell out his hard-earned part-time money for his rent. One day in a while the jail warden (of his juvenile home), recognizes him when he was with Ashok, as Karthik, but Karthik having impersonated himself as Salim, discards him and picks up a quarrel with him that he is not Karthik. This creates a dilemma in the mind of Ashok of his identity. One day, Ashok and Suresh plan to go for an excursion to a farm house, where Rupa denies to come. Suresh suggests him to take some other girls instead, to have a ball there with some female companions, thereby discarding Karthik to the trip. Rupa though she loves Ashok,suspects his fidelity as she came to know through Karthik that he went to farm house with other girls. When this is known to Ashok, he gets enraged. Thinking that Karthik has betrayed him to Rupa, he slaps him and asks him to vacate his place. Karthik, seeing no other way apologizes to him and looks for another lodging. But Ashok doubts his identity, triggered by the quarrel at that time with that jail warden and decides to confirm it. He opens the briefcase of Karthik while he was at shower. He finds a photograph and realizes that the father was different from the one he had introduced to Ashok earlier. He confirms that Karthik is not Salim. Suddenly Karthik enters the room and asks Ashok to give back the photo. But Ashok refuses and  removes the towel that Karthik was wearing and confirms that he is not a Muslim as he was not circumcision|circumcised. Karthik gets angry and pushes Ashok in a rage which ultimately kills him. At first, he is shocked by this unexpected accident but later covers up the murder so intelligently, no one ever found out that Ashok is dead. Now Karthik impersonates himself as Ashok, when a family friend of Ashoks father (who has seen Ashok only in his childhood), plan to meet him and mimics (as he is good at it) as Ashok, to Ashoks parents and Rupa, running the show as if Ashok is still alive. He is able to cover it up, till Suresh finds this out. Karthik kills Suresh. The next day the police comes and say that they have found Sureshs corpse.

They interrogate everyone from the college professor to Ashoks family friends daughter Priya(who was befriending Karthik impersonated as Ashok for a short while) including Karthik but could never find out the truth. They presumed that Ashok has killed Suresh in a quarrel and is absconding and concluded that Karthik is innocent and a studious person. Karthik continues to live as after seeking permission from Salims dad and promises to take care of him. Thus Karthik continues to use his identity. The film ends with the tagline "to be continued".

==Cast==
* Vijay Antony as Karthik 
* Siddharth Venugopal as Ashok Ravindran
* Rupa Manjari as Rupa
* Anuya Bhagvath as Priya
* Vijay Victor as Suresh
* Vibha Natarajan as Uma
* Krishnamurthy
* Pramod as inspector Duraipandiyan Charmila as Karthiks mother

==Production== Ananda Thandavam, the producer Oscar Ravichandran called off the project.  The film restarted with Vijay Antonys intervention in December 2010 who also took the lead role from Siddharth Venugopal.

==Soundtrack==
{{Infobox album 
| Name       = Naan
| Longtype   = to Naan
| Type       = Soundtrack
| Artist     = Vijay Antony
| Cover      =
| Border     = yes
| Alt        = 
| Caption    = Front CD Cover
| Released   =  
                
| Recorded   = 2011
| Genre      = Film soundtrack
| Length     =  Tamil
| Label      = Gemini Music
| Producer   = Vijay Antony
| Last album = Velayudham (2011)
| This album = Naan (2011)
| Next album = Yuvan Yuvathi (2011)
}}
 single tracks in Vaanam and Mankatha, Vijay Antony too released a single track "Makkayala Makkayala" in June 2011, which is one of Vijay Antonys famous hits and the soundtrack also received very well. All the songs in the album are successful hits composed by Vijay Antony. 

Tracklist
{{track listing
| extra_column    = Singer(s)
| total_length    = 24:25
| lyrics_credits  = yes

| title1          = Makkayala Makkayala
| lyrics1         = Priyan
| extra1          = Krishan Maheson, Mark Thomas, Shakthisree Gopalan
| length1         = 4:52

| title2          = Thapellam Thape Illai
| lyrics2         = Asmin
| extra2          = Hip Hop Thamizha Aathi, Santhosh Hariharan 
| length2         = 4:27

| title3          = Ulaginil Miga Uyaram
| lyrics3         = Annamalai 
| extra3          = Vijay Antony 
| length3         = 4:49

| title4          = Dinnam Dinnam
| lyrics4         = Annamalai 
| extra4          = Deepak 
| length4         = 4:35

| title5          = No One Is Perfect (Theme)
| lyrics5         = 
| extra5          = Instrumental 
| length5         = 2:56

| title6          = Thappela Thappeillai (Version 2)
| lyrics6          = Asmin 
| extra6          = Vijay Antony 
| length6         = 2:36
}}

==Release==

===Critical Reception===
The film received mostly positive reviews from critics. Malathi Rangarajan from   gave Naan 3.5 out of 5 stars, writing that "but for some minor blips, the writing (and therefore the movie too) is pretty gripping throughout, and keeps the audience on the edge of the seats", calling it " he perfect thriller to spice up the weekend".  IBNLive writing that "Naan is racy and appealing"  and also adding that "it is a racy crime thriller that is quite an appealing effort from Jeeva Shankar and Vijay Antony."  The Behindwoods Review Board gave the film 2.5 out of 5 calling it a "suspense thriller that works for most parts."  Deccan Chronicle stated that "Vijay Antony changes beat".  Sify rated it average calling it a "decent psychological thriller." 

Upon release, it was noted that the film had been inspired from The Talented Mr. Ripley.  

===Other languages===
Following its success in the Tamil market, the film was dubbed in Telugu language|Telugu, to be released under the title Nakili. 

==Awards==

{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 2nd South Indian International Movie Awards Best Music Director Vijay Antony
| 
|- Best Debutant Producer
| 
|- Best Male Debutant
| 
|-
|}

==Sequel==

Within days after the films release rumours ran rapid stating that the work on Naan 2 has begun but the films director Jeeva Sankar and Vijay Antony rubished the rumours and they say that they are working on different projects and Naan 2 will take more time to come.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 