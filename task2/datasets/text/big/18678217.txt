Rachel Getting Married
{{Infobox film
| name           = Rachel Getting Married
| image          = Rachel getting married.jpg
| caption        = Theatrical release poster
| director       = Jonathan Demme
| producer       = Jonathan Demme Neda Armian Marc E. Platt
| writer         = Jenny Lumet
| starring       = Anne Hathaway Rosemarie DeWitt Bill Irwin Anna Deavere Smith Tunde Adebimpe Debra Winger
| music          = Donald Harrison Jr. Zafer Tawil
| cinematography = Declan Quinn
| editing        = Tim Squyres
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = $12 million 
| gross          = $17 million 
}} Best Actress for her performance in the film.

==Plot== rehab for toasts from friends and family, takes the microphone to offer an apology for her past actions, as part of her twelve-step program.

Underlying the familys dynamic is a tragedy that occurred years previously, which Kym retells at a Narcotics Anonymous meeting. As a teenager, Kym was responsible for the death of her young brother Ethan, who was left in her care one day; driving home from a nearby park, an intoxicated Kym had lost control of the car, driving over a bridge and into a lake, where her brother drowned.
 molested by an uncle and having cared for her sister, who was anorexia nervosa|anorexic. Rachel, hearing this, storms out of the hair salon. The story, it turns out, was all a lie &mdash; an apparent attempt by Kym to evade responsibility for her addiction.

The tension between the sisters comes to a head later that night at their fathers house, when Kym comes home. Rachel reveals she has never forgiven Kym for their brothers death, and now makes the point that Kyms rehab has been a hoax since she has been lying about the cause of her problems. Kym finally admits responsibility for Ethans death and reveals that she had been relapsing in order to cope. She gets into her fathers car and leaves.

Kym heads to the home of their mother Abby (Debra Winger), hoping to find solace with her. However, a fight breaks out between them, when Kym asks Abby why she left Ethan in her care on the night of his death despite knowing that she was often on drugs. She makes the point on how better off it wouldve been if she had left him in Rachels care. Abby tells Kym she left Ethan with her because "you were good with him" and that she thinks her sister is a hypocrite for her accusations. When Kym makes it clear she thinks her mothers decision was in part responsible for Ethans death, Abby becomes furious and punches Kym in the face. Kym hits her mother back and drives off in her fathers car. While driving away, Kym begins sobbing uncontrollably because in her mind, she feels Abby has not accepted appropriate responsibility for her part in the actions which ultimately caused Ethans death. Kym drives the car off the road and crashes into a boulder. Rather than summon help, she spends the night in the car while everyone at home worries about what has become of her.
 sobriety test, which she passes.  She gets a ride home with the driver of the tow truck who is towing the wrecked car. She makes her way to Rachels room, as Rachel prepares for the wedding. Seeing Kyms bruised face from a fight she had with their mother prompts her anger of the previous night to vanish, and Rachel tenderly bathes and dresses her sister.
 Indian theme, Rachel and her fiancé are wed. Kym is the maid of honor, and is overcome with emotion as the couple exchanges their vows. Kym tries to enjoy herself throughout the wedding reception but continues to feel out of place and is nagged by the unresolved dispute with her mother. Ultimately, her mother leaves the party early, despite Rachels effort to bring the two together, and the gulf between Kym and Abby is left unreconciled - suggesting Abbys emotional distance and unwillingness to accept responsibility is the root cause of the familys problems.

The next morning, Kym returns to rehab. As she is leaving, Rachel runs out of the house to hug her.

==Production== naturalistic style. The working title for the film was originally Dancing with Shiva. 

Sidney Lumet himself approached Demme about his daughter Jennys script. Demme has commented that he loved Jennys flagrant disregard for the rules of formula, her lack of concern for making her characters likable in the conventional sense, and for what he considered to be her bold approach to truth, pain, and humor.   

Filming took 33 days and occurred in late 2007. 

==Cast==
* Anne Hathaway as Kym Buchman
* Rosemarie DeWitt as Rachel Buchman
* Bill Irwin as Paul Buchman
* Debra Winger as Abby Buchman
* Tunde Adebimpe as Sidney
* Mather Zickel as Kieran
* Anna Deavere Smith as Carol
* Anisa George as Emma
* Victoria Haynes as Victoria
* Jerome LePage as Andrew
* Carol-Jean Lewis as Sidneys Mother
* Fab 5 Freddy as Himself
* Robyn Hitchcock as Wedding Guest/Performer
* Sister Carol East as Wedding Guest
* Beau Sia as Wedding Czar
* Andre Blake as Inspired Stylist
* Roger Corman as Wedding Guest
* Tamyra Gray as Singing Friend
* Kyrah Julian as Sidneys Sister
* Roslyn Ruff as Rosa
* Sebastian Stan as Walter

===Casting===
Demme had wanted to work with   Manhattan, just pacing back and forth between the kitchen table and the couch. I somehow wound up on the floor sobbing by the last page."   

Rosemarie DeWitt was considered by the films casting directors. Demme and the rest of the crew were impressed and immediately wanted her to play Rachel. Bill Irwin is a personal friend of Demmes.

Tunde Adebimpes role, Sidney, was originally offered to American film director Paul Thomas Anderson while he was working on the post-production of the movie There Will Be Blood. 

Demme was concerned about Debra Wingers interest in doing the film, but he pumped up his courage to ask her because they had met several times before at the Jacob Burns Center, a film center close to their homes. Winger later accepted the role of Abby. 

== Music ==
The music-loving director Demme invited musicians to compose the score live on set, to support the films storyline. {{cite web
| url = http://www.independent.co.uk/arts-entertainment/music/features/rachel-getting-married--lights-camera-music-1380275.html| title = Rachel Getting Married - Lights, camera, music...| author = Bray, Elisa| date = January 16, 2009| accessdate = March 2, 2015| publisher = independent.co.uk
}} 

"For the longest time," Demme has said, "Ive had this desire to provide the musical dimension of a movie without traditionally scored music. I thought: wait a minute; in the script, Paul   is a music-industry bigwig, Sidneys a record producer, many of his friends will be gifted musicians, so of course there would be non-stop music at this gathering. We have music playing live throughout the weekend, but always in the next room, out on the porch or in the garden." 
 Palestinian musician Iraqi Amir ElSaffar, who played the score of Demmes documentary Man from Plains, compose the score on set. Always present at the filming, the musicians had the freedom – and were encouraged – to play whenever they were inspired to, and to ignore the camera.

According to Demme on the DVD, during filming of a dramatic scene, Hathaway complained about the music interfering with the mood, to which Demme responded "Tell her to do something about it!" Hathaway, in that scene, responded by improvising the line, "Can you tell them to knock it off?!" to which another actor not heavily involved in the scene went off-screen and told the band to stop. 
 New Orleanian saxophonist Donald Harrison Jr., and the Brooklyn-based TV on the Radios lead singer Tunde Adebimpe.
 dancehall singers Sister Carol, ElSaffar and Tawil.

Hitchcock recalled,

 

For Demme, it was about creating evocative music in the moment.  

== Reception ==
=== Critical response === Michael Phillips of the Chicago Tribune called the film "a triumph of ambience," and that Hathaway, DeWitt, Irwin and especially Winger are working at a very high level.  Roger Eberts four-star rating added, "apart from the story, which is interesting enough, Rachel Getting Married is like the theme music for an evolving new age."  Other critics praised Jonathan Demme. Andrew Sarris noted in the New York Observer "his career of cinematic good works"  and Owen Gleiberman of Entertainment Weekly observed "a fight scene thats as raw as Ingmar Bergman and as operatic as Mildred Pierce" . . . and "Demmes finest work since The Silence of the Lambs. 

Peter Travers of Rolling Stone noted that Rachel Getting Married is "a home run…   deep into the joy and pain of being human."  A.O. Scott of The New York Times said that the film "has an undeniable and authentic vitality, an exuberance of spirit, that feels welcome and rare". 

Many reviewers praised the film for its organic feel;   noted:

 
 narcissistic Tasmanian Devil not just believable, but somehow likable."  

 , the film holds an overall 85 percent rating at Rotten Tomatoes  The film received a score of 82, indicating "universal acclaim", on the critical aggregator website Metacritic. 

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2008.   

 
  New York  
*1st: Ed Gonzalez, Slant Magazine
*1st: Keith Phipps, The A.V. Club 
*1st: Nathan Rabin, The A.V. Club 
*1st: Scott Tobias, The A.V. Club  David Denby, The New Yorker  
*2nd: Nell Minow, The Movie Mom
*3rd: Owen Gleiberman, Entertainment Weekly 
*3rd: Rick Groen, The Globe and Mail  Daily News  Shawn Levy, The Oregonian 
*5th: Kimberly Jones, The Austin Chronicle 
 
*6th: Kenneth Turan, Los Angeles Times  
*6th: Robert Mondello, NPR 
*7th: Carrie Rickey, The Philadelphia Inquirer 
*7th: Michael Sragow, The Baltimore Sun 
*7th: Noel Murray, The A.V. Club 
*7th: Ty Burr, The Boston Globe 
*8th: Ann Hornaday, The Washington Post 
*8th: Rene Rodriguez, The Miami Herald 
*9th: A. O. Scott, The New York Times 
*9th: Kyle Smith, New York Post  
*9th: Peter Travers, Rolling Stone  
*Top 20: Roger Ebert, Chicago Sun-Times   (Ebert gave an alphabetically listed top 20)
 

==Awards and Nominations==
{| class="wikitable sortable"
|- Year
!align="left"|Ceremony Category
!align="left"|Recipients Result
|- 2008
|rowspan="2"|13th Satellite Awards Best Actress - Drama Anne Hathaway
| 
|- Best Supporting Actress Rosemarie DeWitt
| 
|- 14th Critics Choice Awards Best Actress Anne Hathaway
| 
|- Best Cast Acting Ensemble
| 
|- 15th Screen Actors Guild Awards Best Female Actor in a Leading Role Anne Hathaway
| 
|- 24th Independent Spirit Awards Best Film
|Rachel Getting Married
| 
|- Best Director Jonathan Demme
| 
|- Best Female Lead Anne Hathaway
| 
|- Best First Screenplay Jenny Lumet
| 
|- Best Supporting Female Rosemarie DeWitt
| 
|- Best Supporting Female Debra Winger
| 
|- 66th Golden Globe Awards Best Actress - Drama Anne Hathaway
| 
|- 81st Academy Awards Best Actress Anne Hathaway
| 
|- 2008 New York Film Critics Circle Awards Best Film
|Rachel Getting Married
| 
|- Best Actress Anne Hathaway
| 
|- Best Supporting Actress Rosemarie DeWitt
| 
|- Best Supporting Actress Debra Winger
| 
|- Best Screenplay Jenny Lumet
| 
|- Austin Film Critics Association Awards 2008 Best Actress Anne Hathaway
| 
|- Central Ohio Film Critics Association Best Actress Anne Hathaway
| 
|- Chicago Film Critics Association Awards 2008 Best Actress Anne Hathaway
| 
|- Best Supporting Actor Bill Irwin
| 
|- Best Supporting Actress Rosemarie DeWitt
| 
|- Best Original Screenplay Jenny Lumet
| 
|-
|rowspan="2"|Dallas-Fort Worth Film Critics Association Awards 2008 Best Actress Anne Hathaway
| 
|- Best Supporting Actress Rosemarie DeWitt
| 
|- Detroit Film Critics Society Best Actress Anne Hathaway
| 
|- Best Supporting Actress Rosemarie DeWitt
| 
|- Best Newcomer Rosemarie DeWitt
| 
|- Best Ensemble Acting Ensemble
| 
|- Gotham Independent Film Awards 2008 Breakthrough Performer Rosemarie DeWitt
| 
|- Best Ensemble Cast Acting Ensemble
| 
|- Houston Film Critics Society Awards 2008 Best Actress in a Leading Role Anne Hathaway
| 
|- Best Cast Acting Ensemble
| 
|- International Cinephile Society Awards 2009 Best Picture
|Rachel Getting Married
| 
|- Best Actress Anne Hathaway
| 
|- Best Supporting Actress Rosemarie DeWitt
| 
|- Best Ensemble Acting Ensemble
| 
|- London Film Critics Circle Actress of the Year Anne Hathaway
| 
|- National Board of Review Awards 2008 Best Actress Anne Hathaway
| 
|- Online Film Critics Society Awards 2008 Best Actress Anne Hathaway
| 
|- Palm Springs International Film Festival Best Actress Anne Hathaway
| 
|- Santa Barbara International Film Festival Virtuoso Award Rosemarie DeWitt
| 
|- Southeastern Film Critics Association Awards 2008 Best Actress Anne Hathaway
| 
|-
|St. Louis Gateway Film Critics Association Awards 2008 Best Actress Anne Hathaway
| 
|- Toronto Film Critics Association Awards 2008 Best Film
|Rachel Getting Married
| 
|- Best Director Jonathan Demme
| 
|- Best Actress Anne Hathaway
| 
|- Best Supporting Actress Rosemarie DeWitt
| 
|- Best Screenplay Jenny Lumet
| 
|- Utah Film Critics Association Awards 2008 Best Picture
|Rachel Getting Married
| 
|- Best Actress Anne Hathaway
| 
|- Best Supporting Actress Rosemarie DeWitt
| 
|- Best Screenplay Jenny Lumet
| 
|- Vancouver Film Critics Circle Awards 2008 Best Supporting Actress Rosemarie DeWitt
| 
|- Venice Film Festival Golden Lion
|Rachel Getting Married
| 
|- Washington D.C. Area Film Critics Association Awards 2008 Best Supporting Actress Rosemarie DeWitt
| 
|- Best Screenplay - Original Jenny Lumet
| 
|}

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 