Dante's Inferno: An Animated Epic
 
{{Infobox film
| name           = Dantes Inferno: An Animated Epic
| image          = Dantes Inferno AAE.jpg
| image size     =
| alt            =
| caption        =
| director       =  
| producer       =  
| writer         = Brandon Auman
| based on       = Divine Comedy by Dante Alighieri and Dantes Inferno (video game)|Dantes Inferno by Visceral Games
| narrator       =
| starring       =  
| music          = Christopher Tin
| cinematography =
| editing        =
| studio         =   Starz
| released       =  
| runtime        = 88 minutes
| country        = United States Japan South Korea
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Dantes Inferno: An Animated Epic is a direct to DVD animated dark fantasy action film released on February 9, 2010. The film is based on Dantes Inferno (video game)|Dantes Inferno video game.

==Plot==
 
The movie is separated into several parts. Each chapter is animated with different styles. These vary in degree of difference and depict Dante with differing features, such as hair length, bodily proportions and armor.

The film starts with Dantes return from the Third Crusade. Speaking in inner monologue, he describes the forests as gloomy and nearly worse than death. He can detect someone following him, but each time he tries to approach, his pursuer vanishes. Upon arriving, he finds his servants slain, his father dead and his beloved fiancee Beatrice lying on the ground, dying of a stab wound to the stomach. As she dies, she turns into a spirit and begins to ascend into Heaven. However Lucifer, as a shadow, comes and takes her through the gates into Hell. In pursuit, Dante comes to the gates and Virgil offers to guide him into hell. After Dante invokes his faith he is able to tear open the gates and enter hell.

Upon Entry, Dante and Virgil board Charon (mythology)|Charon, a demonic ferry that takes souls across into the First Circle of Hell. Charon and sends demons to attack the mortal Dante. Dante fights them off, but loses his sword in the process and so takes up one of the demons scythe to defend himself from then on and kills Charon, crashing him into the coasts off the first circle.
 pagans and unbaptized babies. It is here Dante learns Beatrice was pregnant with his child while he was away, but was miscarried in the womb. Without time for sorrow, he is attacked by demonic children. As he and Virgil escape into a large building, they come across a hall of great rulers, philosophers and thinkers such as Aristotle, Plato and Socrates and Saladin, whose forces Dante had battled during his crusade. They move on, and eventually encounter King Minos whose task is to judge all condemned souls to their specific circle of hell. When he denies Dante access, they battle. Dante kills Minos by dropping him onto his own spinning wheel of judgment. Meanwhile, Lucifer tortures Beatrice in a cycle of killing her, tricking her endlessly with the hope of rescue and taunting her that Dante had never kept his promises after he left.

Falling onto the storm-ravaged shores of the second circle, Dante notices bodies flying through the wind, intertwined. Virgil explains the island is the second circle of Lust and those in the wind are caught in a never-ending storm of passion and may never know rest. Following Beatrices cries from the distance, Dante ends up in a room of succubi who transform into hideous demons. As they try to seduce him, he finally realizes he did break his promise to Beatrice; during the Crusade a woman offerd "comfort" to save her husband from being beaten to death. Having been under the illusion he was absolved of all sin, he accepted. Upon hearing this, Beatrice begins to lose her faith yet refuses Lucifers offers her his hand in marriage.

Coming to a grotto of men and women who had lived their lives without knowing fulfillment, so they suffer lacking it in death. Many starving individuals are caught and devoured by Cerberus and Virgil tells Dante the only way to the next circle is from within the beast. Dante allows himself to be eaten and enters of the hound of hell.  In order to escape Cerberus belly, Dante attacks and destroys the beasts heart, causing the demon to spit him up and spew him out in a river of blood that flows down into the next circle.

Dante and Virgils next circle is the ring of hell to men and women who wasted their lives in pursuit of material possessions. The condemned souls are tortured by being sheared in money presses, boiled in melted gold and buried in heavy gold coins. Within the circle, Dante confronts his father, having been promised a thousand years free of torture and endless gold if he would murder his own son. The pair battle fiercely, but Dante gains the upperhand, kicking his father into a vat of boiling gold.

The fifth circle of hell is Anger.Virgil and Dante can sense the very rage in the air. They proceed to the River Styx in which violence is still running rampant among the spirits fighting in the shallow waters. They climb aboard Phlegyas, a demonic giant who traverses the river while men and women who know of Dante taunt him from below. Dante has Phlegyas charge the city when he sees Lucifer within, announcing his intent to marry Beatrice to the damned souls within. When he strikes Phlegyas down, Dante chases after the devil.

The sixth circle of hell is for the heretics, people who have gone against the teaching of their churches. As they travel through halls of men and women who forever burn in fire and are forever tortured by various implements, Dante comes across Farinata, another man Dante hated in life, who taunts Dante by revealing Lucifers plan to wed Beatrice and how he would be trapped in hell forever. Dante angrily kills Farinata just before fleeing the sixth circle and before it collapses from the force of Christs death which, Virgil explains, quakes the circle eternally.

Virgil helps Dante face the   Nessus (mythology)|Nessus. Entering the Forest of Suicides, Dante hears a familiar cry and finds his mother growing from the sapling of a tree, forever in pain for killing herself and not finding the strength to stand up against her husband, Dantes father, she eventually hanged herself. Dante had been told she died of a fever. Having been overwhelmed with sorrow, Dante uses his cross to free her soul. They move onto a graveyard within the Abominable Sands where his one time comrades and one of his close friends Francesco rise from the graves as undead warriors. The graveyard is where souls are condemned for committing acts of violence in the name of God.  Dante  defeats Francesco by slicing his face in half. It is in this that Dante reflects upon slaughtering several heretics including men, women and children without mercy.

After being carried by the geryon, Virgil parts ways with Dante upon entering the realm of Fraud, the eighth circle, telling him he only needs to cross the bridge in order to stop the marriage of Beatrice and Lucifer who are on the opposite end of the bridge. As Dante starts crossing, he begins to reflect upon his own sins. He realizes his father, family servants and Beatrice were slain by the husband of the woman he had sex with and thus blames their deaths on himself. Beatrice finally gives in to her sorrow of Dantes betrayal, wedding Lucifer and fully becoming a demon, losing her wings and rights to heaven. The demonized Beatrice proceeds to attack Dante, overpowering him and forces him to look on his greatest sin, letting him peer into the ninth circle of Treachery. He allowed her brother to take the blame for his slaughtering of prisoners. Overwhelmed with grief, he presents Beatrice her cross, which he had promised to give back to her upon his return from the crusade. She relents as he begs for forgiveness and pleads her to once again accept the love of God and she forgives him, causing her to return to her former angelic appearance as she kisses Dante.  An angel descends from heaven to take Beatrice. Beatrice promises they will be together soon, but in order to leave hell he will need to face Lucifer alone.

Descending into the cold underground of traitors, after wandering in the dark he comes across a cavern filled with large, frozen chains and he mows through them, only to encounter a three-faced demon in the center who appears to be Lucifers corporeal form, having been freed by the breaking of his chains he attacks Dante. Dante slays the beast and is within inches of entering Purgatory where his salvation awaits; however, Lucifer, now freed from his frozen form and reveals his true form, breaks free and easily overpowers Dante, threatening to enter Purgatory and Paradise to bring hell to heaven itself. Dante realizes he cannot stop Lucifer on his own, and he begs to sacrifice his own soul to prevent Lucifer from moving into Purgatory, begging for the power to trap Lucifer with him forever. Lucifer runs back, trying to stop Dante from making this pact; however, he is stopped by a powerful force of light that freezes him solid. Free to move on, Dante dives into the well that would send him to Purgatory, to be with Beatrice. "Neither completely living, nor completely dead" as he puts it. That night the sins he ripped off his chest had transformed into a serpent who is believed to be Lucifer waiting to get his revenge.

==Cast==
* J. Grant Albrecht - Ciacco, Farinata Uberti
* Stephen Apostolina -
* Steven Blum - Lucifer
* Vanessa Branch - Beatrice
* Charlotte Cornwell - Nessus (mythology)|Nessus, Lust Minion #3
* Wendy Cutler -
* Grey DeLisle - Lust Minion #1, Dante (as 10 years old) Greg Ellis - Plato
* Nika Futterman - Female Prisoner, Lust Minion #4 King Richard I
* Nicholas Guest - Demon Priest
* Mark Hamill - Alighiero (Dantes father)
* Peter Jessop - Virgil
* John Paul Karliak - Avenger
* Vanessa Marshall - Lust Minion #2, Frozen Prisoner
* Bart McCarthy - Fillipo Agenti
* Graham McTavish - Dante
* Shelley ONeil - Child
* Kevin Michael Richardson - King Minos, Phelgyas
* Lia Sargent -
* Mark Sussman -
* Tom Tate - Francesco
* Victoria Tennant - Bella
* Dave Zyler -

==Crew==
* Charlie Adler - Voice Director

==Development==
 
The film was animated by  , which was also based on an EA game. The Japanese animation studio Production I.G helped animate hell. A total of six animation studios were involved with the film. It was released on February 9, 2010.

==Reception==
 
 
Anime News Network gave the movie an Overall : B-.

==External links==
*  
* 
* 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 