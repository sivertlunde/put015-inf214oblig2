Luitok Bhetibo Kune
{{Infobox film
| name = Luitok Bhetibo Kune
| image =  
| caption =  
| director = Prabin Bora
| producer = Prabin Bora
| screenplay =  
| based on = 
| narrator = 
| starring = 
| music          = Dipak Sarma
| cinematography = Naba Kumar Das  Benu Neog
| editing =  P. Chandrababu (Chennai)
| studio =
| distributor = 
| released =  
| runtime =  
| country = India Assamese
| budget = 
| gross = 
}} Assamese drama film directed by Prabin Bora, with his own screenplay and produced by himself under the banner of Bistirna Films.    The story, script and dialogues of Luitok Bhetibo Kune are by Prabin Bora himself. Cinematography by Naba Kumar Das and Benu Neog. Music is scored by the renowned flautist Dipak Sarma. Background music is scored by Abani Ranjan Pathak. The lyrics are written by Prabin Bora and Bikash Barua. The songs are rendered by Parinita, Samrat Bora, Dibyajyoti Barua, Dr. Gunin Basumatary, Dil Mohammed and others.    On June 15, 2012 the film was censored.   After Parinam (1974), Angikar (1985) and Prem Bhora Chakolu (2003) Luitok Bhetibo Kune is the fourth film directed by Prabin Bora.    

==Plot==
The story highlights growing influence of western culture into the Assamese society, which is a serious threat to our traditional culture and beliefs.  Its a sentimental story that realistically depicts the extent to which western culture has influenced our traditions and all other local art forms. 

==Cast==
Biplab Bora, Dr. Gunin Basumatary, Padum Gogoi, Aparajita Dutta, Rita Bora, Abhijit Mazumdar, Biman Barua, Padum Nath, Raktim Dutta, Deben Sarma, Pinki Bhattacharjya, Ranjan Bora (Dadu), Milan Kakati, Sailen Hazarika, Bhupen Bhuyan, Manuranjan Dev Sarma, Nishanta Sarma, Ramen Talukdar, Nivedita Barua Saikia, Vivek Lekharu, Brishti Naina Das, Alpana Talukdar Sarma, Sanjiv Borthakur, Olympic Saikia, child artistes Dibyashree Saikia and Srijan Saikia etc. 

==References==
 

 
 
 
 

 