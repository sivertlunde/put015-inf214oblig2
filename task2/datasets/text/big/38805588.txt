In the Aleutians – Isles of Enchantment
 

Private Snafu in The Aleutians—Isles of Enchantment (Oh Brother!) is a short cartoon in the Private Snafu series, directed by Chuck Jones. Warner Bros. produced this film for the United States armed forces and issued it to service branches in February 1945, to inform personnel posted in the Aleutian Islands of what was then the Alaska Territory.

The film describes the hardships of a posting in the Aleutians, describing the severe climactic conditions in an ironic voiceover commentary. During the Aleutian Islands Campaign in World War II, thousands of U.S. servicemen were stationed in the islands.

==Description==
  
This film starts with a brief overview of the Aleutian Islands chain. A diminutive caricature of a Japanese serviceman hops from island to island until he reaches a doorway, as the narrator explains that during the Aleutian Islands Campaign, the Japanese saw the Aleutians as a potential back door for invading the United States. Once he reaches the door, the "Jap" encounters a large American soldier who quickly puts him to retreat as the narrator describes the new status of the Aleutians as a front door to Tokyo.

The remainder of this cartoon discusses the harsh and widely varying environment—rain, snow, thunder, whirlwind gales—which the narrator attributes to   replies with Jimmy Durante style, "Nevertheless, thats the conditions that prevail."

Next, two servicemen emerge from thick mud, but a third man they are riding on is never seen. A "williwaw" strikes suddenly, stripping buildings and even hills off the landscape. Difficulty of airstrip maintenance due to excess moisture is the next topic; airmen (including Private Snafu, in his lone appearance in the short, donning a diving suit) dive into a pond to retrieve a B-17. The aircraft rises from water and takes flight, opening its bomb bay to discharge a fish. This animal drops into the gaping maw of the seal, who repeats his earlier dialogue.

==External links==
* 
* 

 
 
 
 


 