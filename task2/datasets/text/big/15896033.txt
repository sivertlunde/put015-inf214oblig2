Madame Bovary (1991 film)
{{Infobox film
| name           = Madame Bovary
| image          = Madame Bovary (1991 film).jpg
| image_size     = 
| caption        = 
| director       = Claude Chabrol
| producer       = Marin Karmitz
| screenplay     = Claude Chabrol
| based on       = Madame Bovary by Gustave Flaubert
| narrator       = François Périer
| starring       = Isabelle Huppert Jean-François Balmer Christophe Malavoy
| music          = Jean-Michel Bernard Matthieu Chabrol Maurice Coignard
| cinematography = Jean Rabier
| editing        = Monique Fardoulis
| distributor    = MK2 Diffusion
| released       =  
| runtime        = 143 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Madame Bovary is a 1991 French film directed by Claude Chabrol and based on the novel Madame Bovary by the 19th century French author Gustave Flaubert. It was nominated for the Golden Globe Award for Best Foreign Language Film as well as for the Academy Award for Costume Design. It was also entered into the 17th Moscow International Film Festival where Isabelle Huppert won the award for Best Actress.   

==Cast==
*Isabelle Huppert as Emma Bovary
*Jean-François Balmer as Charles Bovary
*Christophe Malavoy as Rodolphe Boulanger
*Jean Yanne as M. Homais
*Lucas Belvaux as Leon Dupuis
*Christiane Minazzoli as Widow Lefancois
*Jean-Louis Maury as Merchant Lheureux
*Florent Gibassier as Hippolyte
*Jean-Claude Bouillaud as Monsieur Rouault
*Sabeline Campo as Felicite
*Yves Verhoeven as Justin
*Marie Mergey as Charles Bovarys Mother
*François Maistre as Lieuvain
*Thomas Chabrol as Vicomte
*Jacques Dynam as Father Bournisien
*Henri Attal as Maître Hareng
*Dominique Zardi as Blind Man

==See also==
* Isabelle Huppert filmography
* French films of 1991

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 


 