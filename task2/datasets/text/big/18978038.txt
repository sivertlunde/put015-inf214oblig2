Heart o' the Hills
{{Infobox film 
 | name = Heart o the Hills
 | image = Heart o the Hills DVD cover.jpg
 | caption = DVD cover Sidney Franklin 
 | writer = Bernard McConville Novel John Fox, Jr.
 | starring = Mary Pickford
 | producer = 
 | distributor =
 | budget = 
 | released =  
 | country = United States English intertitles
 | runtime = 87 minutes
}}
 1919 silent Sidney Franklin, written by Bernard McConville and based on a John Fox, Jr.s novel.

==Plot== Harold Goodwin) is a young boy who lives with his stepfather chief Steve Honeycutt (Sam De Grasse) at the ancestral Honeycutts home. One day the chief is looking for the 13-year-old mountain girl Mavis Hawn (Mary Pickford), who is shooting bullets in the woods. Mavis desires for revenge after a few gang members attack her home and shot and killed her father. One of her only friends is geologist and school teacher John Burnham (Fred Warren). He suggests her to get an education instead of teaching herself to use a gun.
 John Gilbert) and his sweetheart Marjorie Lee (Betty Bouton), who are looking for the town.

Back at home, Mavis is disappointed Steve is still there. Later that night, Mavis visits a party and meets Gray for the second  time. He flirts with her, which makes Jason jealous. Gray forces himself up to Mavis, which makes her upset and angry. She leaves the party and finds out her mother has left her to marry Steve. She decides to marry as well and proposes to Jason. However, they soon find out they are too young.

When word hits town that a man named Morton Sanders (Henry Hebert) is planning to take over the city, some of the inhabitants, including Mavis, threaten him to force him go away. Later that night, Morton is found dead and the police are looking after everyone who was involved. The police visits the Hawn house, but Mavis grandfather (Fred Huntley) forces them to go away. While holding them with his shotgun Mavis packs her stuff and goes hiding in the forest. The next day, John Burnham visits her and convinces her to go to trial to proof her innocence.

In court, the lawyer of the other party demands for her to be hanged. The town folks try to defend her by all admitting they have shot Morton. Mavis is discharged and finally decides to go to school. Mr. Burnham, Gray and Marjorie are all pleased with Mavis decision. Jason however, becomes jealous again when she starts hanging out with Gray at school and leaves her.

Six years pass. Mavis has been adopted by the rich Colonel Pendleton (W.H. Bainbridge). One day she receives a letter from her mother, announcing she is getting old and will most-likely die soon. She decides to visit her mom and finds out Steve killed her father. He has become violent  and takes it out on Martha. Mavis tries to help her and shoots Steve. Martha survives the incident and takes Mavis in to live with her. Mavis is reunited with a grown-up Jason and they marry.

==Cast==
* Mary Pickford - Mavis Hawn Harold Goodwin - Young Jason Honeycutt
* Allan Sears - Jason Honeycutt
* Fred Huntley - Granpap Jason Hawn
* Claire McDowell - Martha Hawn
* Sam De Grasse - Steve Honeycutt
* W.H. Bainbridge - Colonel Pendleton John Gilbert - Gray Pendleton
* Betty Bouton - Marjorie Lee
* Henry Hebert - Morton Sanders Fred Warren - John Burnham

==External links==
 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 