Behind the Lines (film)
 
{{Infobox film
| name           = Behind the Lines
| image          = Behind the Lines.jpg
| caption        = Film poster
| director       = Henry MacRae
| producer       = Universals "Bluebird Photoplays" unit Walter Woods Harry Carey
| music          =
| cinematography = Harry A. Gant
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        = 5 reels
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 silent drama Harry Carey.

==Cast==
* Edith Johnson - Nina Garcia Harry Carey - Dr. Ralph Hamlin
* Ruth Clifford - Camilla
* Mark Fenton - Señor Garcia (as Marc Fenton)
* Miriam Shelby - Señnora Cano
* William Human - Carlos (as Bill Human)
* Lee Shumway - Jose (as L.C. Shumway)
* Edwin Wallock - General Dominguez (as E.N. Wallack)
* L. M. Wells - General Nomonza
* Ray Hanford - Torrenti Lee Hill - Fred Williams
* Ernest Shields - Bit Role (uncredited)

==See also==
* List of American films of 1916
* Harry Carey filmography

==External links==
* 

 
 
 
 
 
 
 
 
 

 