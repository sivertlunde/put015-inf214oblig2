Prizzi's Honor
 
{{Infobox film
| name        = Prizzis Honor
| image       = Prizzis_honor.jpg
| caption     = Theatrical poster
| director    = John Huston John Foreman
| writer      = Richard Condon Janet Roach
| starring = {{Plainlist|
* Jack Nicholson
* Kathleen Turner
}}
| music       = Giacomo Puccini Alex North
| cinematography = Andrzej Bartkowiak
| editing     = Kaja Fehr Rudi Fehr ABC Motion Pictures
| distributor = 20th Century Fox   Producers Sales Organization  
| released    =  
| runtime     = 130 minutes
| country     = United States
| language    = English
| budget = $16 million 
| gross       = $26,657,534 (US)  
}}

Prizzis Honor is a 1985 American film directed by John Huston. It stars Jack Nicholson, Kathleen Turner, Robert Loggia and, in an Academy Award-winning performance, the directors daughter Anjelica Huston.

The film was adapted by Richard Condon and Janet Roach from Condons 1982 novel of the same name.  Its score, composed by Alex North, adapts the music of Giacomo Puccini and Gioachino Rossini.

==Plot==

Charley Partanna is a hit man for a New York crime organization headed by the elderly Don Corrado Prizzi, whose business is generally handled by his sons Dominic and Eduardo and by his longtime right-hand man, Angelo, who is Charleys father.

At a family wedding, Charley is quickly infatuated with a beautiful woman he doesnt recognize. He asks Maerose Prizzi, estranged daughter of Dominic, if she recognizes the woman, oblivious to the fact that Maerose still has feelings for Charley, having once been his lover. Maerose is in disfavor with her father for running off with another man after the end of her romance with Charley.  

Charley discovers that the mysterious woman, Irene, is a "contractor" who, like himself, performs assassinations for the mob. He flies to California to spend time with her and quickly falls in love. Unaware she is married, Charley also carries out a contract to kill Irenes husband, Marksie Heller, for robbing a Nevada casino. She repays some of the money Marksie stole and in Mexico marries Charley.

Charley is unaware that Irene is suspected by the mob of having the rest of the money Marksie took. A jealous Maerose travels there on her own to establish for a fact that Irene has double-crossed the organization. The information restores Maerose to good graces somewhat with her father and the don.

Dominic, acting on his own, wants Charley out of the way and hires someone to do the hit, not knowing that he has just given the job to Charleys own wife. Angelo sides with his son, and Eduardo is so appalled by his brothers actions that he helps set up Dominics permanent removal from the family.

Irene and Charley team up on a kidnapping that will enrich the family, but she shoots a police captains wife in the process, endangering the organizations business relationship with the cops. The don is also still demanding a large sum of money from Irene for her unauthorized activities in Nevada, which she doesnt want to pay. In time, the don tells Charley that his wifes "gotta go."

Things come to a head in California when, acting as if everything is all right, Charley comes home to his wife. (A famous line from the movie, spoken by Charley, is "Do I marry her?  Do I ice her?  Which one of these?")  Each pulls a weapon simultaneously in the bedroom. Irene ends up dead, and Charley ends up back in New York, missing her, but consoled by Maerose.

== Cast ==
*Jack Nicholson as Charley Partanna
*Kathleen Turner as Irene Walker
*Anjelica Huston as Maerose Prizzi
*Robert Loggia as Eduardo Prizzi John Randolph as Angelo "Pop" Partanna William Hickey as Don Corrado Prizzi Lee Richardson as Dominic Prizzi
*Michael Lombard as Rosario "Finlay" Filangi
*C. C. H. Pounder as Peaches Altamot
*George Santopietro as Plumber
*Ann Selepegno as Amalia Prizzi
*Lawrence Tierney as Lt. Hanley
*Vic Polizos as Phil Vittimizzare
*Dick ONeil as Bluestone
*Sully Boyar as Casco Vasorne
*Stanley Tucci as Soldier

==Critical reception==
  acted out by The Munsters. Jack Nicholsons average-guyness as Charley, the clans enforcer, is the films touchstone: this is a baroque comedy about people who behave in ordinary ways in grotesque circumstances, and it has the juice of everyday family craziness in it."  

== Awards ==
===Academy Awards===
The film won the Academy Award for Best Supporting Actress (Huston).

It was also nominated for:
 Best Picture Best Director Best Actor (Nicholson) Best Supporting Actor (Hickey) Best Screenplay Based on Material from Another Medium Best Costume Design (Donfeld) Best Film Editing

===American Film Institute===
*AFIs 100 Years...100 Laughs - Nominated 
*AFIs 100 Years...100 Passions - Nominated 
*AFIs 10 Top 10 - Nominated (Gangster Film) 

===Golden Globes===
Won:
* Golden Globe for Best Motion Picture - Comedy/Musical
* Golden Globe for Best Director (John Huston)
* Golden Globe for Best Performance by an Actor in a Motion Picture - Comedy/Musical (Nicholson)
* Golden Globe for Best Performance by an Actress in a Motion Picture -- Comedy/Musical (Turner)
Nominated:
* Golden Globe for Best Supporting Performance by an Actress (Anjelica Huston)
* Golden Globe for Best Screenplay - Comedy/Musical (Adapted)

==See also==
*1985 in film

== References ==
 

== External links ==
 
*   at MGM.com
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 