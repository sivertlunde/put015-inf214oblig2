Rugged Bear
 
{{Infobox Hollywood cartoon
| cartoon name      = Rugged Bear
| series            = Donald Duck
| image             = Rugged Bear.jpg
| image size        = 
| alt               = 
| caption           = Theatrical release poster
| director          = Jack Hannah
| producer          = Walt Disney
| story artist      = Al Bertino David Detiege
| narrator          =  Jimmy MacDonald Clarence Nash
| musician          = Oliver Wallace
| animator          = Bob Carlson Volus Jones George Kreisl Dan MacManus (effects)
| layout artist     = Yale Gracey
| background artist = Ray Huffine Walt Disney Productions RKO Radio Pictures
| release date      =   (USA)
| color process     = Technicolor
| runtime           = 6 minutes
| country           = United States
| language          = English
| preceded by       = The New Neighbor
| followed by       = Working for Peanuts
}} Walt Disney RKO Radio Donald Ducks Jimmy MacDonald as Humphrey, and an uncredited narrator.

Rugged Bear was nominated for the Academy Award for Best Animated Short Film at the 26th Academy Awards in 1954, but lost to another Disney film, Toot, Whistle, Plunk and Boom. This was the eighth of nine nominations received by the Donald Duck film series. 

==Plot== hunting trophies hanging on the walls. As he starts to leave, he sees Donald Duck coming toward the cabin carrying a rifle. Humphrey desperately looks for a hiding place inside the cabin, and finally notices a large bear skin rug in front of the fireplace. He quickly rolls up the rug, stows it in an empty trunk, and lays out flat on the floor in the rugs place.  Just then, Donald enters unaware that his rug has been replaced, or that it is alive. Humphrey nervously endures several uncomfortable and painful experiences, all the while being very careful not to let Donald know that he is a real bear. These include hiccups, a burning ember from the fireplace falling on his fur, going through Donalds washer-dryer, and being mowed with a reel mower, among other things.

Off screen Humphrey spends the rest of hunting season in Donalds cabin carrying on his rug masquerade. When hunting season is over, Donald finally leaves and Humphrey breaths a sigh of relief. But just then, he hears a sound from the trunk in the corner and learns, much to his surprise, that the bearskin rug which he had rolled up and stowed earlier is actually another bear who had also masqueraded as a rug. The film ends with the other bear leaving and thanking Humphrey for taking his place.

==Censorship==
Some edited versions of Rugged Bear have been released with omit the scene in which Donald points his rifle directly at Humphrey, as well as the scene where Donald presses the buttons on the clothes washer. 

==Releases==
*1953 &ndash; Theatrical release Walt Disney Presents, episode #5.18: "Duck Flies Coop" (TV) Walt Disneys Wonderful World of Color, episode #8.5: "The Hunting Instinct" (TV) Walt Disneys Wonderful World of Color, episode #14.22: "The Ranger of Brownstone" (TV)
*c. 1983 &ndash; Good Morning, Mickey!, episode #17 (TV)
*1984 &ndash; " " (VHS)
*c. 1992 &ndash; Mickeys Mouse Tracks, episode #15 (TV) The Ink and Paint Club, episode #1.30: "50s Donald" (TV) 
*2008 &ndash; " " (DVD) 

==Notes==
 
 
 
 
 
 
 