Vienna Tales
{{Infobox film
| name = Vienna Tales
| image =
| image_size =
| caption =
| director = Géza von Bolváry
| producer = Heinrich Haas
| writer =  Hans Gustl Kernmayr (novel)   Harald Bratt   Ernst Marischka
| narrator = Hans Moser   Paul Hörbiger
| music = Bruno Uher  
| cinematography = Willy Winterstein 
| editing =  Alice Ludwig
| studio = Wien-Film   Styria-Film  
| distributor = Terra Film
| released = 8 August 1940   
| runtime = 100 minutes
| country = Austria (Part of Greater Germany)  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Hans Moser.  The film is set in Imperial Vienna at the beginning of the twentieth century. The films sets were designed by Hans Ledersteger and Ernst Richter.

==Cast==
* Marte Harell as Christine Lechner  
* Olly Holzmann as Mizzi  Hans Moser as Josef 
* Paul Hörbiger as Ferdinand
* Hedwig Bleibtreu as Baronin Neudegg 
* Siegfried Breuer as Egon von Brelowsky 
* Fritz Diestel as Kaffeehauskoch 
* Lola Hübner as Mathilde  
* Fritz Imhoff as Grünberger 
* Gisela Kolbe as Baronin Redwitz
* Anita Koller as Küchenmädchen 
* Alfred Neugebauer as Horat Kümmler 
* Hans Olden as Graf Riedl-Steinbach 
* Hans Schott-Schöbinger as Fritz Seidl 
* Oskar Sima as Stangelberger 
* Egon von Jordan as Carlo

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 
 
 
 


 