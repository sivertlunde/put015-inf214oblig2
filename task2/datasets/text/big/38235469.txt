Switch (2013 film)
 
 
{{Infobox film
| name           = Switch
| image          = Swtich2013.jpg
| alt            = 
| caption        = Official film poster
| film name      = {{Film name
 | traditional    = 天機：富春山居圖
 | simplified     = 天机：富春山居图
 | pinyin         = Tiān Jī: Fù Chūn Shān Jū Tú
 | jyutping       = Tin1 Gei1: Fu3 Ceon1 Saan1 Geoi1 Tou4}}
| director       = Jay Sun
| producer       = {{plainlist|*Zhao Haicheng
*Shen Xue
*He Lichang}}
| writer         = Jay Sun
| starring       = {{plainlist|*Andy Lau
*Tong Dawei
*Zhang Jingchu
*Lin Chi-ling}}
| music          = Roc Chen
| cinematography = Shao Dan
| editing        = Du Hengtao China Film Media Asia Films Phoenix Satellite Television Media Asia Distribution
| released       =  
| runtime        = 122 minutes
| country        = China Hong Kong
| language       = {{plainlist|*Mandarin
*Japanese
*English }}
| budget         = Renminbi|¥160 million 
| gross          = ¥302.6 million  
}}
Switch is a 2013 Chinese-Hong Kong action film written and directed by Jay Sun and starring Andy Lau, Tong Dawei, Zhang Jingchu and Lin Chi-ling.    

==Synopsis==
A famous Chinese Yuan Dynasty painting known as "Dwelling in the Fuchun Mountains" is stolen and sold on the black market led by a mysterious business magnate (Tong Dawei) and it is up to special agent Jinhan (Andy Lau) to recover it. Jinhan and his wife (Zhang Jingchu) have drifted apart due to the secret nature of his work and Jinhan is unaware that is wife is also a special agent who was tasked with protecting the painting.

==Cast==
* Andy Lau as Special Agent Xiao Jinhan
* Tong Dawei as Yamamoto Toshio
* Zhang Jingchu as Lin Yuyan
* Lin Chi-ling as Lisa
* Siqin Gaowa as The Empress
* Ariel Aisin-Gioro as The Princess

==Release==
Switch was originally set to be released in 2012, but was delayed partially due to the decision to convert the film to 3D film|3-D.  The film was released in China on 9 June 2013  and in Hong Kong on 12 June, where its running time was trimmed by 9 minutes with several scenes cut out. 

In China, the film grossed RMB 49 million (US$8 million) in its opening day. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 