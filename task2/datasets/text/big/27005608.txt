Jag är din krigare
{{Multiple issues
| 
 
}}

{{Infobox Film 
| name           = Jag är din krigare
| image          = 
| caption        = 
| director       = Stefan Jarl
| producer       = Staffan Hedqvist
| writer         = Stefan Jarl
| narrator       = 
| starring       = Robin Milldoff John Belindo Jan Malmsjö Mikael Persbrandt
| music          = Ulf Dageby
| cinematography = 
| editing        = Anette Lykke-Lundberg SF
| released       = 21 February 1997
| runtime        = 96 min.
| country        = Cinema of Sweden|Sweden, Denmark Swedish
| followed by    = 
| budget         = 
| gross          = 
}}
Jag är din krigare ("I Am Your Warrior") (also known as Bjørnens søn ("Son of the Bear") and Natures Warrior) is a 1997  Swedish/Danish action film directed by Stefan Jarl.

The movie mainly takes place in the nature around Lidköping.

== Plot ==
13 year old Kim feels at home in the wilderness. One night, natures soul appears to him in the form of an Indian, and designates him natures protector. Kim decides to remain out in the forest and live off what nature provides. Soon, however, Kims assignment becomes more serious. He has to try and save as many animals as he can from mans pointless killing, and he will stop at nothing.

== Cast ==
*Robin Milldoff - Kim
*John Belindo - The Indian
*Jan Malmsjö - Chief of police
*Mikael Persbrandt - Hjorth
*Peter Harryson - Estate owner
*Pierre Lindstedt - Foreman
*Anders Granell - Fisher
*Lena Nilsson - Mom
*Johan Paulsen - Father
*Hedvig Hedberg - Sister
*Viggo Lundberg - Edward
*Ebba Hernevik - Indian girl
*Thorsten Flinck - Interrogator
*Lakke Magnusson - Guard
*Kenneth Milldoff - Excavator driver

== External links ==
* 
* 

 
 
 


 