Thina Sobabili: The Two of Us
   }}

{{Infobox film
| name           = Thina Sobabili: The Two of Us
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Ernest Nkosi
| producers       = Ernest Nkosi, Mozzie Pheeha, Enos Manthata, Mpho "Popps" Modikoane, Mpho Nthangeni, Katleho Twala
| writer         = Ernest Nkosi Mozzie Pheeha
| narrator       = 
| starring       = Emmanuel Nkosinathi Gweva, Busisiwe Mtshali, Richard Lukunku, Zikhona Sodlaka, Mpho "Popps" Modikoane, Thato Dhladla
| music          = Mpho Nthangeni
| cinematography = 
| editing        = Warwick Allan
| studio         = 
| distributor    = 
| released       = 2014
| runtime        = 94 minutes
| country        = South Africa Zulu
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Thina Sobabili: The Two of Us debuted at the Durban International Film Festival in 2014. The independent film was picked as one of the 12 best films to show at the African Diaspora International Film Festival in New York. The film won the 2015 Audience Award at the Pan African Film Festival. 

The film is set in the Alexandra township of Johannesburg where Zanele (Busisiwe Mtshali) is raised by her criminal older brother. The film explores issues such as sugar daddies and marital rape.

The film was made on a shoestring budget as "the team had tried for four years to attract funding and in the end, they sold T-shirts, organised a campus comedy tour and pitched in their savings to raise the budget." 

==External links==
*  

==References==
 

 
 
 
 

 