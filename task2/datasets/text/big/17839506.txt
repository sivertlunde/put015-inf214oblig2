Strangers of the Night
 
{{Infobox film
| name           = Strangers of the Night
| image          =
| caption        =
| director       = Fred Niblo
| producer       = Louis B. Mayer Fred Niblo
| writer         = Lenore J. Coffee Bess Meredyth Renaud C. Gardner Sullivan
| based on       = play, Captain Applejack, by Walter C. Hackett Matt Moore Enid Bennett
| music          =
| cinematography = Alvin Wyckoff
| editing        = Lloyd Nosler
| distributor    =
| released       =  
| runtime        = 80 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 silent comedy film directed by Fred Niblo. It was produced by Louis B. Mayer and released through Metro Pictures.   

It is based on a 1921 stage play, Captain Applejack, by Walter C. Hackett starring Wallace Eddinger.  It was refilmed as a talkie by Warner Brothers in 1931 under the Captain Applejack title. The 1923 film is now lost. 

==Cast== Matt Moore - Ambrose Applejohn
* Enid Bennett - Poppy Faire
* Barbara La Marr - Anna Valeska Robert McKim - Borolsky
* Mathilde Brundage - Mrs. Whatcombe
* Emily Fitzroy - Mrs. Pengard
* Otto Hoffman - Horace Pengard
* Tom Ricketts - Lush (as Thomas Ricketts)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 