Ruma Maida
{{Infobox film
| name      = Ruma Maida
| image     = Ruma Maida.jpg
| image size   =
| border     =
| alt      =
| caption    = Promotional poster
| director    = Teddy Soeriaatmadja
| producer    = {{plain list|
* P. Setiono
* Teddy Soeriaatmadja
}}
| writer     = Ayu Utami
| starring    = {{plain list|
* Atiqah Hasiholan
*  
* Nino Fernandez
* Frans Tumbuan
}}
| music     = {{plain list|
* Bobby Surjadi
* Didit Saad Naif
}}
| cinematography = Ical Tanjung
| editing    = Waluyo Ichwandiardono
| studio     = {{plain list|
* Lamp Pictures
* Karuna Pictures
}}
| distributor  =
| released    =  
| runtime    = 90 minutes
| country    = Indonesia
| language    = Indonesian
| budget     =
| gross     =
}}
Ruma Maida (released internationally as Maidas House) is a 2009 Indonesian film written by Ayu Utami, directed by Teddy Soeriaatmadja and starring Atiqah Hasiholan,  , Nino Fernandez, and Frans Tumbuan. It details a womans struggle to save a historic house from a developer; it also shows the life of the houses original owner.
 Naif and a song written by Utami&nbsp;– premiered on 28 October 2009, the anniversary of the 1928 Youth Pledge; it was later shown in film festivals in Singapore, Australia, and Italy.

Ruma Maida, which uses different filming styles for scenes in the past and present, deals with the importance of education, history, and pluralism. Critical reception to the film was mixed; reviewers praised the visuals but disapproved of the plot and dialogue. It was nominated for twelve Citra Awards at the 2009 Indonesian Film Festival, of which it won one.

==Plot== Indo composer and pilot, and his Muslim wife Nani Kuddus (Imelda Soraya); Pahing wrote the song "Pulau Tenggara" ("The South-Eastern Island"), which inspired President Sukarno to help form the Non-Aligned Movement, while living in the house. As Maida learns about Pahing, she decides to write her undergraduate thesis about his life.
 rioting breaks Chinese Maida, then tells her that he will help her keep the house, although it is scheduled to be demolished within a week.
 traditional musical group distantly related to Pahing, to discover that the house has a secret underground bunker, in which she and Sakera&nbsp;– with whom she has begun to fall in love&nbsp;– find documents showing the history of the house. With the help of her mothers former lover Kuan (Henky Solaiman), she discovers the true ownership of the house.
 nascent independence Japanese spy a flight carrying medical supplies over Yogyakarta when the flight was shot down. Meanwhile, his son was raised by Maruyama&nbsp;– the kidnapper&nbsp;– and had his name changed to Dasaad Muchlisin.

With this information, Maida, Sakera, and Kuan approach Muchlisin and tell him how the house features in his history. After a short silence, Muchlisin tells them to leave. Several months later, on Maida and Sakeras wedding day&nbsp;– when they are married at both a mosque and a church&nbsp;– Muchlisin comes to the church and says that he has abandoned his plans to demolish the house. Instead, he renovates the building and dedicates it as a school for street children.

==Production==
  wrote the script over a period of six months. Ruma Maida was her first screenplay.]]
The screenplay for Ruma Maida was written by Ayu Utami, her first such work;  mainly known for her novels, she had avoided screenplays as she thought they were generally too commercially oriented.   She wrote the screenplay over a period of six months beginning in 2008, when Lamp Pictures &nbsp;– which produced the film with Karuna Pictures &nbsp;– requested that she write a story about nationalism;   according to the director Teddy Soeriaatmadja, who was brought in while the screenplay was still on its first draft,  he and Utami read seven drafts of the screenplay before they agreed on the story.  Considering the screenplay a way to encourage the younger generation to study Indonesian history, which she said could be fun,  Utami decided to focus on education, diversity, and history. 
 Academy Award-submitted Jamila dan Sang Presiden (Jamila and the President, 2009), was cast as Maida.   Yama Carlos, who played Sakera, was initially cast for another role but received the leading male role after a last-minute switch.  The actor cast as Muchlisin, Frans Tumbuan, was the only one auditioned as Soeriaatmadja thought the role was perfect for him.  Soeriaatmadja later recalled that, including Extra (actor)|extras, Ruma Maida had the largest cast of any film he had made to that point. 
 cinematographer Ical Tanjung said that Soeriaatmadja was still open to feedback from the cast and crew.  Editing, which was done by Waluyo Ichwandiardono, took another three months.  
 Naif cover covered several Ibu Pertiwi" ("Motherland").  The covers were recorded over a period of five days.  Utami wrote "Pulau Tenggara", which was sung by Imelda Soraya. 

==Themes==
Benny Benke, writing in   of Indonesia, implying that, if the country was not maintained, it would fall apart. 

In Media Indonesia, Yulia Permata Sari wrote that Soeriaatmadja seemed to be promoting the need to remember and respect history through the plot and characterisations.  The film showed "Indonesia Raya" composer W.R. Supratman, Japanese admiral Maeda, Vice President Mohammad Hatta, President Sukarno, and Prime Minister Sutan Sjahrir.   Hasiholan considered the film a warning against repeating past mistakes. 

Triwik Kurniasari, writing in The Jakarta Post, described the inclusion of the May 1998 riots and subsequent fall of Suharto as touching on pluralism issues.  Utami, in an interview with the Jakarta Globe, stated that she had meant to show diversity by giving the characters different ethnic, religious, and socio-economic backgrounds,  and later explained that the film was meant to show Indonesias motto Bhinneka Tunggal Ika (Unity in Diversity) as it is applied in the country.  Another reviewer, Dewi Anggraeni, wrote that Ruma Maida "paints a more realistic picture of Indonesia’s society, where people do not necessarily fit into neat social, racial or economic categories", with its characters not fitting into any traditional stereotypes. 

==Style==
  flashbacks interspersed throughout Maidas struggle to retake the house.   The film is paced slowly, and shots are taken from "unique" angles. 

In Tempo (Indonesian magazine)|Tempo magazine, Kurie Suditomo wrote that Ruma Maida intertwined several sub-plots, including the depiction of the 1928 Youth Conference, the education of street children, and a scene where Sakera discusses architecture with Muchlisin; the review stated that these detracted from the films comprehensibility.  Jakarta Globe reviewer Armando Siahaan noted that several plot lines run parallel, including riots following the Japanese surrender in 1945 and those in May 1998. 

==Release and reception==
Ruma Maida premiered on 28 October 2009, coinciding with events that celebrated the 1928 Youth Pledge &nbsp;– this release date was planned from early in production, because of the dates historical significance.  It received a wide release on the following day.  The film was screened at the Singapore International Film Festival in April 2010.  That August Ruma Maida had three screenings in the "Education" category of the Indonesian Film Festival in Melbourne, Australia.   In November it was screened at the Asiatica Film Mediale in Rome, Italy, under the title La Casa Di Maida. 

Ruma Maida received mixed reception. Kurniasari described the film as "an enjoyable way to learn more about   long history."  Benke wrote that the film had good visuals, but the dialogue at times "went over viewers heads".   Sari called the cinematography well done, but found that the plot could confuse viewers.  Suditomo thought that the film was well visualised but lost much of its impact owing to its extraneous subplots. 

Anggraeni, covering the Indonesian Film Festival in Australia, described Ruma Maida as cleverly weaving the plot into Indonesias independence struggle, although she felt that several plot twists "rather stretch  the audiences imagination".  Siahaan wrote that the film "may have limitations in its execution and presentation, but is highly commendable for its ability to raise social questions and delve into the nation’s history."  The review in Republika (Indonesian newspaper)|Republika suggested that the film may be too boring for the general public owing to its slow-moving plot. 

The film was released on VCD and DVD in Indonesia on 14 July 2010 by EZY Home Entertainment, after passing through the censorship board in February. The DVD featured English-language subtitles and a behind the scenes documentary.  

==Awards==
Ruma Maida was nominated for twelve Citra Awards at the 2009 Indonesian Film Festival, winning one. 

{| class="wikitable plainrowheaders" style="font-size: 95%;"
|-
! scope="col" | Award Year
! scope="col" | Category
! scope="col" | Recipient
! scope="col" | Result
|-
! scope="row" rowspan="12" | Indonesian Film Festival
| rowspan="12" | 2009 Best Picture
|
| 
|- Best Director
| Teddy Soeriaatmadja
| 
|-
| Best Original Screenplay
| Ayu Utami
|  
|- Best Leading Actor
| Yama Carlos
|  
|- Best Leading Actress
| Atiqah Hasiholan
|  
|-
| Best Supporting Actor
| Frans Tumbuan
|  
|-
| Best Supporting Actor
| Verdi Solaiman
|  
|-
| Best Cinematography
| Ical Tanjung
|  
|-
| Best Artistic Direction
| Indra Tamoron Musu
|  
|-
| Best Editing
| Waluyo Ichwandiardono
|  
|-
| Best Musical Direction
| Bobby Surjadi, Didit Saad
|  
|-
| Best Sound Arrangement
| Shaft Daultsyah, Khikmawan Santosa
| 
|}

 

==Notes==
 

==References==
===Footnotes===
 

===Bibliography===
 
* {{cite news
 |last=Anggraeni
 |first=Dewi
 |title=RI film festival takes on Australian silver screen
 |url=http://www.thejakartapost.com/news/2010/09/08/ri-film-festival-takes-australian-silver-screen.html
 |work=The Jakarta Post
 |date=8 September 2010
 |accessdate=7 April 2012
 |archiveurl=http://www.webcitation.org/67VeJmiBQ
 |archivedate=7 April 2012
 |ref= 
}}
* {{cite news
 |url=http://suaramerdeka.com/v1/index.php/read/cetak/2009/10/26/85418/Membebaskan-Sejarah
 |work=Suara Merdeka
 |title=Membebaskan Sejarah
 |trans_title=Freeing History
 |language=Indonesian
 |date=26 October 2009
 |last=Benke
 |first=Benny
 |accessdate=8 May 2012
 |archiveurl=http://www.webcitation.org/67VXav4rQ
 |archivedate=8 May 2012
 |ref= 
}}
* {{cite AV media notes
 |title=Dekade
 |others=Chrisye
 |year=2002
 |publisher=Musica Studios
 |id=MSCD 0306
 |ref= 
}}
* {{cite news
 |last=Kurniasari
 |first=Triwik
 |title=Ruma Maida portrays the countrys history
 |url=http://www.thejakartapost.com/news/2009/11/01/ruma-maida039-portrays-country039s-history.html
 |work=The Jakarta Post
 |date=1 November 2009
 |accessdate=7 April 2012
 |archiveurl=http://www.webcitation.org/66jvX9Z6z
 |archivedate=7 April 2012
 |ref= 
}}
* {{cite web
 |title=Ruma Maida : Suguhan Apik Dari Teddy Soeriaatmadja
 |trans_title=Ruma Maida : A Good Work from Teddy Soeriaatmadja
 |language=Indonesian
 |last=M. P.
 |first=Erfanintya
 |url=http://www.21cineplex.com/slowmotion/ruma-maidasuguhan-apik-dari-teddy-soeriaatmadja,986.htm
 |work=21cineplex.com
 |date=22 October 2009
 |publisher=21 Cineplex
 |location=Jakarta
 |accessdate=9 May 2012
 |archiveurl=http://www.webcitation.org/67WFEoI6d
 |archivedate=9 May 2012
 |ref= 
}}
* {{cite news
 |title=Jamila dan Sang Presiden ready for Oscar
 |url=http://www.thejakartapost.com/news/2009/10/31/%E2%80%98jamila-dan-sang-presiden%E2%80%99-ready-oscar.html
 |work=The Jakarta Post
 |date=31 October 2009
 |accessdate=1 April 2012
 |archiveurl=http://www.webcitation.org/66b186bRv
 |archivedate=1 April 2012
 |ref= 
}}
* {{cite news
 |url=http://majalah.tempointeraktif.com/id/arsip/2006/11/06/CTP/mbm.20061106.CTP122195.id.html
 |work=Tempo
 |title=K.A
 |trans_title=
 |language=Indonesian
 |date=6 November 2006
 |last=Mohamad
 |first=Goenawan
 |authorlink=Goenawan Mohamad
 |accessdate=12 May 2012
 |archiveurl=http://www.webcitation.org/67bHFlT4a
 |archivedate=12 May 2012
 |ref= 
}}
* {{cite web
 |title=More than a Learning Experience
 |url=http://www.indonesianfilmfestival.com.au/documents/IFF10_Ruma%20Maida_Educational_Program.pdf
 |work=indonesianfilmfestival.com.au
 |publisher=Indonesian Film Festival
 |accessdate=13 May 2012
 |archiveurl=http://www.webcitation.org/67cPsJSB6
 |archivedate=13 May 2012
 |ref= 
}}
* {{cite web
 |title=Penghargaan Ruma Maida
 |trans_title=Awards for Ruma Maida
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-r009-09-263777_ruma-maida/award#.T6mtvlKITMw
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=9 May 2012
 |archiveurl=http://www.webcitation.org/67WBjFCVE
 |archivedate=9 May 2012
 |ref= 
}}
* {{cite news
 |title=Rediscovering world cinema in Singapore
 |url=http://new.thejakartapost.com/news/2010/04/18/rediscovering-world-cinema-singapore.html
 |work=The Jakarta Post
 |date=18 April 2010
 |accessdate=8 May 2012
 |archiveurl=http://www.webcitation.org/67VfZOwXw
 |archivedate=8 May 2012
 |ref= 
}}
* {{cite web
 |title=Ruma Maida
 |url=http://www.asiaticafilmmediale.it/en_schedafilm_view.php?id_config=5&id_scheda=339
 |work=asiaticafilmmediale.it
 |publisher=Asiatica Film Mediale
 |accessdate=13 May 2012
 |archiveurl=http://www.webcitation.org/67cQKgjzI
 |archivedate=13 May 2012
 |ref= 
}}
* {{cite web
 |title=Ruma Maida
 |language=Indonesian
 |url= http://www.disctarra.com/index.php/front/detail/793
 |publisher=Disc Tarra
 |accessdate=14 August 2012
 |archiveurl=http://www.webcitation.org/69ubGPza8
 |archivedate=14 August 2012
 |ref= 
}}
* {{cite news
 |url=http://www.republika.co.id/berita/senggang/film-musik/09/10/24/84369-ruma-maida-pijakan-pengingat-sejarah
 |work=Republika
 |title=Ruma Maida, Pijakan Pengingat Sejarah
 |trans_title=Ruma Maida, A Way to Remember History
 |language=Indonesian
 |date=24 October 2009
 |accessdate=8 May 2012
 |archiveurl=http://www.webcitation.org/67VbaWiTR
 |archivedate=8 May 2012
 |ref= 
}}
* {{cite news
 |url=http://www.mediaindonesia.com/mediaperempuan/read/2009/10/10/2283/11/Sejarah_dalam_Balutan_Romantisme
 |work=Media Indonesia
 |last=Sari
 |first=Yulia Permata
 |title=Sejarah dalam Balutan Romantisme
 |trans_title=History from a Romantics Perspective
 |language=Indonesian
 |date=23 October 2009
 |accessdate=8 May 2012
 |archiveurl=http://www.webcitation.org/67VclFdup
 |archivedate=8 May 2012
 |ref= 
}}
* {{cite news
 |last=Sembiring
 |first=Dalih
 |title=A Modern Film Reflecting On Indonesia’s Storied Past
 |url=http://www.thejakartaglobe.com/home/a-modern-film-reflecting-on-indonesias-storied-past/332489
 |work=The Jakarta Globe
 |date=29 September 2009
 |accessdate=8 May 2012
 |archiveurl=http://www.webcitation.org/67Vaehx9a
 |archivedate=8 May 2012
 |ref= 
}}
* {{cite news
 |last=Setiawan
 |first=Iwan
 |title=Ayu Utami, nationalism and Ruma Maida
 |url=http://www.thejakartapost.com/news/2009/11/01/ayu-utami-nationalism-and-ruma-maida039.html
 |work=The Jakarta Post
 |date=1 November 2009
 |accessdate=8 May 2012
 |archiveurl=http://www.webcitation.org/67VcEXKiP
 |archivedate=8 May 2012
 |ref= 
}}
* {{cite news
 |last=Siahaan
 |first=Armando
 |title=Ruma Maida A Potent Tangle
 |url=http://www.thejakartaglobe.com/artsandentertainment/ruma-maida-a-potent-tangle/337944
 |work=The Jakarta Globe
 |date=27 October 2009
 |accessdate=8 May 2012
 |archiveurl=http://www.webcitation.org/67VgNQBjG
 |archivedate=8 May 2012
 |ref= 
}}
* {{cite AV media notes
 |title=Ruma Maida
 |last=Soeriaatmadja
 |first=Teddy (director and producer)
 |year=2010
 |oclc=706774253
 |type=DVD liner notes
 |publisher=EZY Home Entertainment
 |location=Jakarta
 |ref= 
}}
* {{cite AV media
 | last =Soeriaatmadja
 |first=Teddy (director and producer)
 | year =2010a
 | title =Di Balik Layar Part 1
 | format =
 | medium =
 | language =Indonesian
 | trans_title =Behind the Screen Part 1
 | publisher =EZY Home Entertainment
 | location =Jakarta
 | oclc =706774253
 | ref =harv
}}
* {{cite AV media
 | last =Soeriaatmadja
 |first=Teddy (director and producer)
 | year =2010b
 | title =Di Balik Layar Part 2
 | format =
 | medium =
 | language =Indonesian
 | trans_title =Behind the Screen Part 2
 | publisher =EZY Home Entertainment
 | location =Jakarta
 | oclc =706774253
 | ref =harv
}}
* {{cite news
 |url=http://majalah.tempointeraktif.com/id/arsip/2009/11/09/FL/mbm.20091109.FL131878.id.html
 |work=Tempo
 |last=Suditomo
 |first=Kurie
 |title=Ruma Itu Punya Cerita
 |trans_title=The House has a Story
 |language=Indonesian
 |date=9 November 2009
 |accessdate=8 May 2012
 |archiveurl=http://www.webcitation.org/67VdMMgiF
 |archivedate=8 May 2012
 |ref= 
}}
 

==External links==
 
*  
*  
*  

 

 
 

 

 

 
 
 
 
 