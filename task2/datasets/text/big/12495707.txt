Weeds (film)
{{Infobox film
| name           = Weeds
| image          = Weedsfilmposter.jpg
| image size     =
| caption        = Theatrical release poster
| director       = John D. Hancock
| producer       = Bill Badalato
| writer         = John D. Hancock Dorothy Tristan
| narrator       = William Forsythe Rita Taggart Mark Rolston Lane Smith Joe Mantegna Anne Ramsey
| music          = Angelo Badalamenti Melissa Etheridge Orville Stoeber
| cinematography = Jan Weincke
| editing        = David Handman Jon Poll Chris Lebenzon
| distributor    = De Laurentiis Entertainment Group (DEG)
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1987 US|American drama feature film about a prison inmate who writes a play that catches the attention of a visiting reporter. The film was directed by John D. Hancock, and stars Nick Nolte, Ernie Hudson, and Rita Taggart.

==Plot==
Lee Umstetter (Nick Nolte) is incarcerated in San Quentin for armed robbery, serving "life without possibility" (with no chance of parole). After two failed suicide attempts, Lee begins to read books from the prison library. He attends a performance of Waiting for Godot given for the prisoners and is deeply moved. He begins to write plays about imprisonment and then stages them, too.

One is a social-protest musical extravaganza about life in the penitentiary. It attracts visitors and earns Lee the regard of a San Francisco theatre reviewer (Rita Taggart) who persuades the governor to release him.
 shoplifter (William William Forsythe), flasher (Mark Rolston), and others.

Lees work doesnt make the same impact outside the prison as it did inside. Touring in a camper, with no money, the men are torn by impulses to revert to their former criminal behaviour. 

==Production==
The New Yorkers film critic Pauline Kael observed, "The film is about their efforts to become professional men of the theatre. Its about the ways in which working together changes them and the ways in which it doesnt."  

Hancock, who was the director of the San Francisco Actors Workshop, did some work with the convict Rick Cluchey and his San Quentin Drama Group (whose late 1960s show The Cage toured the US and Europe).  The film grew out of Hancocks contact with Clucheys company and out of his and Dorothy Tristans research into other prison theatre groups. 

== Reception ==
The movie received mixed reviews.  

The movie was a moderate success at the box office. 

It did well on VHS. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 