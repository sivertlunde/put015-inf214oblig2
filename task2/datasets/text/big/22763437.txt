City Beneath the Sea (1953 film)
{{Infobox film
| name           = City Beneath the Sea
| image          =
| image_size     =
| caption        =
| director       = Budd Boetticher
| producer       = Albert J. Cohen Jack Harvey (screenplay)
| starring       = Robert Ryan Anthony Quinn
| cinematography = Charles P. Boyle
| editing        = Edward Curtiss	 	
| distributor    = Universal-International
| released       = March 11, 1953
| runtime        = 87 mins.
| country        = United States English
| gross = $1.25 million (US) 
}}
City Beneath the Sea is a 1953 adventure film directed by Budd Boetticher and starring Robert Ryan and Anthony Quinn. The film is based on the book Port Royal: The Ghost City Beneath the Sea by Harry E. Rieseberg.

==Plot synopsis== earthquake in 1692. The $1,000,000 in gold bullion was in fact lost in a modern-day quake and is part of an insurance fraud by Trevor. He doesn’t want the divers to find the gold and will stop at nothing to ensure the success of his plan.

==Cast==
*Robert Ryan - Brad Carlton
*Anthony Quinn - Tony Nartlett
*Mala Powers - Terry McBride
*Suzan Ball - Venita George Mathews - Capt. Meade
*Karel Stepanek - Dwight Trevor
*Hilo Hattie - Mama Mary
*Lalo Rios - Calypso
*Woody Strode - Djion John Warburton - Captain Clive
*Peter Mamakos - Captain Pedro Mendoza Barbara Morrison - Madame Cecile

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 