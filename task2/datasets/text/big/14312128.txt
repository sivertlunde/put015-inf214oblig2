The Blue Umbrella (2005 film)
 
 
 
{{Infobox film
| name           = The Blue Umbrella
| image          = Blueumbrella.jpg
| caption        = Theatrical release poster
| director       = Vishal Bhardwaj
| producer       = Ronnie Screwvala Vishal Bhardwaj
| story          = Vishal Bhardwaj Abhishek Chaubey Minty
| based on       = The Blue Umbrella by Ruskin Bond
| starring       = Pankaj Kapoor Shreya Sharma Deepak Dobriyal
| music          = Vishal Bhardwaj
| cinematography = Sachin Kumar Krishnan
| editing        = Aarif Shaikh
| distributor    = UTV Classics
| released       =  
| runtime        = 90 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
The Blue Umbrella (  based on the novel, The Blue Umbrella, by Ruskin Bond and directed by Vishal Bhardwaj. It starred Shreya Sharma and Pankaj Kapur in lead roles. The music was by Bhardwaj and lyrics were penned by Gulzar (lyricist)|Gulzar.

==Plot== Japanese umbrella while herding her family cows. The umbrella is revealed to be belonging to a group of Japanese tourists. Biniya develops a liking for the umbrella and trades it with her bear claw necklace called Yantra with the Japanese tourists. Wearing a bear claw neckles is considered to be auspicious and bring good luck. For that she is scolded by her mother. Everyone in the village is amazed by the beauty of the umbrella and Biniya acquires a celebrity like status. This makes some people jealous of her. The shopkeeper Khatri and Lajvanti (wife of village school teacher) covet for such an umbrella but find themselves unable to buy due to its high cost. Lajvanti even hints toward the possibility of stealing Biniyas umbrella. Khatri tries to buy it from Biniya but she refuses. 
 wrestling competition. Biniya continue her investigation and finds that umbrella is actually sent by a textile dyer from a nearby town Banikhet. She accompanies the village policeman to Banikhet to find out the truth. At the same time, Khatri is presiding over the competition and it rains during his speech. The dye from the umbrella comes off and Kahtri is revealed to be the thief. Due to his theft and lying, Kahtri is boycotted by the village.

Post boycott, Khatris life becomes miserable as no one visit his shop. Even, Rajaram who was his accomplice abandons him. He is usually made fun of village people of passing school children. Biniya sympathies with Khatri and decides that Khatri is the real owner of umbrella not her. She gives the umbrella to Khatri and who is then accepted into the village.

==Cast==
* Pankaj Kapur       ...	Nandakishore Nandu Khatri
* Shreya Sharma      ...	Biniya Rahul Kumar ...	Tikku
* Paramjit Singh Kakran  ...	Rajaram
* Piu Dutt               ...	Binyas mother
* Samrat Mukerji         ... Bijju
* Kamal Tiwari           ... Mukhiya
* Dolly Ahluwalia    ... Lillavati
* Alok Mathur            ... Masterji
* Rajesh Sharma          ... Policeman

==Awards==
* 2007: National Film Award for Best Childrens Film   

==Soundtrack==
{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Track !! Singer(s)
|-
| 1|| Aasmani Chhatri|| Upagna Pandya
|- Sukhwinder
|- Instrumental
|- Upagna Pandya
|- Instrumental
|- Instrumental
|- Upagna Pandya
|- Instrumental
|- Upagna Pandya
|- Instrumental
|- Instrumental
|- Instrumental
|- Instrumental
|- Instrumental
|-
|}

== References ==
 
==External links==
* 

 
 

 
 
 
 
 
 
 
 