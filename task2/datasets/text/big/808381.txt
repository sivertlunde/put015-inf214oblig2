À Hauteur d'homme

{{Infobox film
| name           = À Hauteur dhomme
| image          =À Hauteur dhomme.jpg
| image_size     = 
| caption        =
| director       = Jean-Claude Labrecque
| producer       = 
| writer         = Jean-Claude Labrecque
| narrator       = 
| starring       = Bernard Landry
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = November 11, 2003
| runtime        = 95 min.  Canada
| French
| budget         = 
}} 2003 Cinema Canadian political documentary directed 2003 general election in Quebec, Canada. It won a Jutra Award for Best Documentary (tie) in 2004. Its style belongs to the Quebec cinéma vérité|cinéma direct school of filmmaking.

== Overview ==
À Hauteur dhomme is a political documentary film revolving first around one man, Bernard Landry, leader of the Parti Québécois (PQ), and second around the re-election campaign of his party in 2003. The movie shows an intimate, never-before-seen look at the works of an election campaign. The finality of the story, the defeat of the party, gives this work a mood of tragedy, but with final acceptance. It also features, amongst others, Landrys girlfriend, Chantal Renaud, and press attaché Hubert Bolduc.

Landry, the protagonist, is an   of the Parti Libéral du Québec (PLQ) and Mario Dumont of the Action démocratique du Québec (ADQ), are rarely seen in the film.

Along with his team, he goes through an intense experience in two periods. The first half of the campaign goes smoothly: Landry is relaxed and confident. After having fought for its very life, the party is popular again and leads the polls. The televised leaders debate is the turning point. During the debate Charest confronts Landry with a news report purportingly quoting Jacques Parizeau, the former PQ premier, as being unrepentant for his 1995 referendum evenings unfortunate remarks. In the following days, this sparks a controversy that will be known as the Parizeau Affair. From then on, a second period begins. The PQ loses some steam. Charest slowly surpasses Landry in the polls.

Often trapped by insistent, forceful questions by reporters, Landry shares with the team his impression that journalists are unjustly harassing him and the partys campaign. After being, in turn, anxious, angry, and sometimes morose, he accepts the coming ineluctable defeat with serenity, but with much emotion, with the comfort of his loved ones and colleagues.

The documentary features the following politicians: Pauline Marois, André Boisclair and François Legault.
 Pierre Langlois, Éric Côté and Jacques Wilkins.

==Context==
:Main article: Quebec general election, 2003

In 2002, the poll numbers for the Parti Québécois fell sharply. The PQ government had been in power for two mandates and was seen as worn-out by some. An important part of the PQs support went to the Action Démocratique du Québec and its young leader, Mario Dumont, and some to the Liberal Party of Quebec. It is under this dramatic situation for the PQ followers that Landry underwent a revitalization of the party and its image. The PQ was aided by the fall popularity of the ADQs ideas as their conservative nature was uncovered, and by social democratic measures taken by the PQ government like the passing of the Act to combat poverty and social exclusion. The Parti Québécois succeeded in gaining back popularity in the beginning of 2003 to take the lead in the public opinion polls again. The PQ felt confident again to take upon a singular task: to become the first Quebec government in more than forty years to win a third mandate.
 sovereigntist movement, perhaps halting it for years.
 war in media and the population. Landry became known for his custom of wearing the white ribbon worn by Quebecers in favour of peace. The wearing of this ribbon was soon adopted by the two other main party leaders, Charest and Dumont.

Despite a PQ comeback, Charest presented himself as a viable alternative for people in desire of change, especially during the leaders debate. Also, the Parizeau Affair is said to have harmed Landrys campaign up to election day.  The PQ lead vanished mid-campaign, and the Parti Libéral won the election.

==Impact== press within the movie, along with the occasional swearing of Landry in Quebec joual, was noted by many journalists.  Some reporters considered Landrys reactions to the attitude of the press to be excessive.  Also, the few swear words of Landry from the movie were played in loop on news channels.

The publicity acquired by the initial controversy partly assured the movie an immense success in theatres. In its first days, at the Ex-Centris theatre, where it was first shown in Montreal, the movie was often sold-out several hours before presentation. After it had opened, the consensus of the viewing public (along with the opinion of some critics and other journalists) was that the initial media presentation of the movie was misrepresentative of the complete work.  The movie managed to spark public debate in the media about the (often described as unfair and aggressive) attitudes of Quebec journalists towards politicians and politics (and vice versa).
 Latin phrases.

The way Landry acts in this movie surprised many. Landry is seen as a man of pride and high culture by many Quebecers, something that sometimes puts a distance between a public figure and the people in Quebec. Rather than hurting his reputation, his Quebec French profanity|Québécois swearing broke that bourgeois image in some ways.  Also, his sense of humour and humanity throughout the movie was felt by many viewers.  Those seldom before seen aspects of the man subsequently inspired a popular wave of sympathy for him. 

==Production==
Critics praised the work for its evocative, classy musical ambiance, and its lavish visual style. 

===General===
For months, Landry was filmed everywhere he went, up to voting day. The fact that a politician had accepted to be filmed in such privacy impressed many and was therefore seen as an historical feat: few other movies have had such access to a political figure before.

===Audio===
These two previously existing (that is to say that they were not created for the movie soundtrack) musical compositions of the soundtrack are the backbone of the musical backdrop of the film. Audio excerpts from Amazon.com.
*Facades by Philip Glass returns in leitmotiv throughout the movie. It was chosen by Labrecque for its repetitive nature, representative of the 2003 political campaign in some ways.
*Fur Alina by Arvo Pärt and its very melancholic ambiance is used in the final scene where Landry conceded defeat and is comforted by the people close to him.

===Visuals=== tight shots, waist shot talking heads type of clips that politicians are mostly seen through.

==Other version==
Labrecque has always said that his favourite version of the film was the cut of over three hours (this version is 104 minutes long). He has stated that he plans to one day send this version to the wide screen. 

==See also==
*Cinema of Quebec
*Cinéma vérité|Cinéma direct
*Culture of Quebec
*List of Quebec movies National Question
*Politics of Quebec
*Quebec general election, 2003
*Quebec nationalism Quebec sovereigntism

==External links==
*   
*   
* 
* 

==References==
 

 
 

 
 
 
 
 
 
 
 