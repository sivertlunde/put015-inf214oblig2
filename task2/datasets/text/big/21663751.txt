Menschen im Hotel
{{Infobox film
| name           = Menschen im Hotel
| image          = 
| image_size     = 
| caption        = 
| director       = Gottfried Reinhardt
| producer       = Artur Brauner 
| writer         =      
| narrator       = 
| starring       = O.W. Fischer, Michèle Morgan, Heinz Rühmann, Gert Fröbe
| music          =  
| cinematography =  
| editing        =  
| studio         = CCC Films|CCC-Filmkunst GmbH   Les Films Modernes S.A.
| distributor    = Gloria Filmverleih
| released       =  
| runtime        = 105 minutes
| country        = West Germany, France
| language       = German 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1959 German Grand Hotel.

==Plot==
The film shows two days and nights in a Berlin luxury hotel and tells the story of how the paths of various quite different characters cross. It begins with an attempted suicide by the famous dancer Grusinskaja. She is rescued by an impoverished noble, Baron von Gaigern, and falls in love with him. Von Gaigern, who makes a living as a hotel thief, learns about the corrupt dealings by businessman Preysing. He attempts to blackmail Preysing, with tragic results.   

==Cast==
*O.W. Fischer as  Baron von Gaigern 
*Michèle Morgan as  Grusinskaja 
*Heinz Rühmann as  Kringelein 
*Sonja Ziemann as  Flämmchen 
*Gert Fröbe as  Preysing 
*Dorothea Wieck as  Suzanne 
*  as  Max, the chauffeur 
*Friedrich Schoenfelder as  Receptionist
*  as  1st doorman
*  as  2nd doorman
*Siegfried Schürenberg as  Dr. Behrend 

==Production== Grand Hotel, starring Greta Garbo and John Barrymore.

Filming took place from 15 February to 31 March 1959 at the CCC-Studios in Berlin. 

==Release==
Menschen im Hotel at the time received a FSK rating of "18 and older".  The film premiered on 23 September 1959 at Gloria Palast in Munich. The premiere of the French (dubbed) version was in Paris, on 25 March 1960. 

==References==
 

==External links==
* 
* 
*  at CinEmotions  
* 
*  

 
 
 
 
 
 
 
 
 


 