Chill Out, Scooby-Doo!
{{Infobox film
| image          = Chill Out SD.jpg
| caption        = 
| director       = Joe Sichta
| producer       = 
| writer         = Adam Scheinman  Joe Sichta Jeff Bennett James Sie Rene Auberjonois Alfred Molina Kim Mai Guest James Hong
| music          = Thomas Chase Jones
| cinematography = 
| editing        = 
| distributor    = Warner Home Video
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
}}
Chill Out, Scooby-Doo! is the eleventh   carried a dedication to him). 

==Plot== Abominable Snowman, who lives in the mountains. Hes convinced that the Snowman lives close by, but Pemba tells him he wont lead Jeffries anymore, as it would be intruding on the territory of the Abominable Snowman. Jeffries, who seems to care about the Snowman over the safety of Pemba, decides to cut the rope and continue. As soon as Jeffries fades from view, Pemba sees the outline of a large creature in front of him.
 Velma and Daphne are Shaggy havent arrived yet, and the gang wonders where they are. Shaggy and Scooby are in a small plane that they think is going to Paris, but is really going to the Himalayas to drop off Alphonse LaFleur, a French hunter and trapper. LaFleur wants to find and kill the Snowman, and is taking Shaggy and Scooby along as bait.  LaFleur then locks Shaggy and Scooby up with his equipment, and throws them off the plane. Shaggy, realizing they are not going to Paris, manages to get a quick phone call through to Fred before the phone goes dead. Fred uses the GPS on his phone to track them, and then he, Velma, and Daphne head to the Himalayas to find them. 

Meanwhile, Shaggy and Scooby manage to land near a small village on the mountain, where Professor Jefferies and Pemba currently are. Jeffries tells Pemba he never should have left him, and that he almost died on the mountain. Many of the other villagers are leaving, fearing the creature and its wrath. Shaggy and Scooby go to the High Lama to ask for a phone to call their friends. The High Lama, a strange person, tells them there is only a phone on the weather station nearby. The High Lama is uneasy about letting them go in, but lets them. In the room is a statue of the Snowman holding a very large crystal, and Jeffries asks if he could see it, but the High Lama says no and closes the door. They then meet Pembas sister Minga, who has decided to stay in the village. Minga constantly listens to the radio, and has a crush on the DJ from the station that shes listening to. Pemba tells her to leave the village, and he, Shaggy, Scooby, and Jeffries (who says that everyone should stick together), decide to go to the weather station. LaFleur arrives, and decides to accompany them.
 TNT and Del Chillman, whom the gang had meet before. Del takes them to the weather station, where he works. Chillman tells them he decided to take the job so he could find out if the legend of the creature is true. He also reveals that hes the DJ/weatherman. He had the radio just to do weather reports, but plays songs to pass the time.

Meanwhile, Fred, Velma, and Daphne get to the village and see it is deserted. They follow Shaggy’s tracks and see where the party camped the night before, and also the footprints of the Snowman. Velma notes that they are not very deep in the snow. They find Pemba, who was caught by one of LaFleurs traps. Minga is nowhere to be found and Jeffries is still gone, so the gang decides to split up and search for everyone else.  At the weather station, Del goes out to look for the others. Once he leaves, the Snowman appears and attacks Shaggy and Scooby. As they run away, LaFleur shows up and tries to capture the creature, but falls of a cliff, apparently to his death. Scooby and Shaggy manage to escape and find their way to the lost kingdom of Shangri-La, but are pursued by the Snowman.

Del catches up to the rest of the gang, but finds the weather station destroyed and some helium tanks missing. As Scooby and Shaggy walk around the city, the Abominable Snowman appears and chases them. Daphne and Pemba find a large cave and go inside, and conclude that is where the Snowman lives. They also find an empty helium case. Shaggy and Scooby lose the Snowman again, and everyone meets up in an old Mining|mine, each coming from a different direction. There, they see Jeffries mining for lots of crystals like the one on the Yeti statue. They conclude he is the Snowman and capture him, although he denies it. Then the Snowman appears and chases everyone, while Jeffries gets free and follows them. The Snowman chases Shaggy, Scooby, and Jeffries down the mountain. Jeffries attempts to get the crystals  in the mine cart that Scooby and Shaggy are riding in. The rest of the gang prepares a trap for the Snowman, but Shaggy, Scooby, and Jeffries get caught in it. The High Lama comes out to see what happened. Then an avalanche starts, and almost crushes Velma and Del, but the Snowman saves them at the last moment. The Snowman is revealed to be Minga, who has been behind the mystery from the very beginning. She used the helium to fly, which caused the footprints to not be as deep. Minga confesses that she did it so that Del wouldnt stop broadcasting his radio show (also her way of admitting that she has feelings for Del). Dels touched by what Minga says, and finds it romantic. Jeffries is taken to jail because he was taking the crystals for his own gain. The gang wonders if there really is a Snowman, but then LaFleur appears and tells them that something saved him from his fall and brought him to the village (assumably the real Snowman).

The gang, along with Del and Minga (whore now boyfriend and girlfriend), return to Paris for their vacation. Unfortunately, Fred gets on the wrong plane and somehow ends up in the Amazon. The others go to find Fred, with Daphne complaining, "Id like to have a vacation that actually stays a vacation."

==Featured Villains==
* Professor Jeffries
* Abominable Snowman/Minga (Actually a hero)

==Cast== Fred Jones
* Casey Kasem as Shaggy Rogers
* Mindy Cohn as Velma Dinkley
* Grey DeLisle as Daphne Blake
* James Sie as Pemba Sherpa Jeff Bennett as Del Chillman and Pilot
* René Auberjonois (actor)|René Auberjonois as Alphonse LaFleur
* Alfred Molina as Professor Jeffries
* Kim Mai Guest as Minga Sherpa
* James Hong as The High Lama

==Follow-up film==
Scooby-Doo! and the Goblin King

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 