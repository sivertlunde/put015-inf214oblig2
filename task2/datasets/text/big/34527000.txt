Puss in Boots: The Three Diablos
 
{{Infobox film
| name           = Puss in Boots: The Three Diablos
| image          = Puss in Boots The Three Diablos poster.jpg
| caption        = DVD cover
| director       = Raman Hui
| producer       = Gina Shay Tripp Hudson
| writer         = 
| screenplay     = Tom Wheeler Chris Miller Walt Dohrn
| music          = Matthew Margeson Henry Jackman
| cinematography = 
| editing        = Bret Marnell
| studio         = DreamWorks Animation
| distributor    = Paramount Home Entertainment
| released       =  
| runtime        = 13 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Puss in Boots. It was directed by Raman Hui and features Antonio Banderas as the voice of the title character. The short was released on February 24, 2012, attached as a bonus feature to the Puss in Boots DVD and Blu-ray (3D) release.    The short tells a story of Puss in Boots on a mission to recover a princess stolen ruby from the notorious French thief the Whisperer. Reluctantly accompanied by three cute little kittens called the Three Diablos, Puss must tame them before they endanger the mission.

==Plot==
After the events of the film, Puss is riding his horse through the desert when he is captured by Italian knights. He is then taken to Princess Alessandra Belagomba, whose "Heart of Fire" Ruby, the crown jewel of her kingdom, is missing. At first, it is believed that Puss is being wrongfully charged for the theft, but it later turns out that the Princess only wants to hire him based on his reputation, revealing that a thief called "The Whisperer" was the one who committed the crime and that the Princess knights have captured three of his henchmen. The henchmen turn out to be three kittens called the Three Diablos. Though Puss cannot believe that such innocent creatures could be thieves, the princess and her guards are terrified of them. The kittens kindly agree to help Puss on the premise that they will be free if they return the ruby.

When Puss takes the kittens to the desert, the kittens quickly turn on him (revealing their backstabbing nature) and bury him alive. Puss later escapes and recaptures the kittens using his wide eyes against theirs. Later, he talks about sending them back to jail for double-crossing him, but he learns that they have no family and are orphans like him. He then sympathetically tells them how he also knows its tough not knowing whom to trust and being led to the wrong path, making an example of how Humpty betrayed Puss, just as the Whisperer has done to them. Puss then decides to point the Diablos in the right direction and trains them how to fight and plays with them, becoming friends. He also gives them names: Perla (because she is one of a kind), another Gonzalo (for his scrappy temper) and the other Sir Timoteo Montenegro the Third (a title is all he needs).

The next day, the kittens, turning over a new leaf, show Puss to The Whisperers secret hideout, and are immediately confronted by The Whisperer himself, who, by his name, has a low voice volume and uses his hat as a cone to speak clearly. It is also revealed that The Whisperer himself has used the heart as a decoration for his own belt. After learning that the Diablos brought Puss to him to recover the heart, the Whisperer is about to make them pay for their betrayal, but Puss fights him and lets the kittens escape. They, however, return to help Puss with what they learned from him and The Whisperer falls into a bottomless pit to his death. Puss then returns the heart to the Princess and is rewarded with gold, and he gives the Princess the kittens as her new personal bodyguards. They then say their goodbyes and Puss claims "He will never forget them, just as he is sure they will never forget the name of Puss in-"; unfortunately, the guards slam the doors before he can finish his goodbye.

==Voice cast== Puss in Boots
* Gilles Marini as Captain of the Guard / Paolo the Squire
* Charlotte Newhouse as Princess Alessandra Bellagamba Chris Miller as Food Prisoner
* Walt Dohrn as Water Prisoner
* Bret Marnell as Toilet Paper Prisoner
* Miles Christopher Bakshi as Gonzalo / Sir Timoteo Montenegro the Third
* Nina Zoe Bakshi as Perla
* Guillaume Aretos as Le Chuchoteur the Whisperer

==Release==

===Home media=== Puss in Boots on February 24, 2012. 

==Reception==
Bret Marnell was nominated for Outstanding Achievement, Editorial in an Animated Television or other Broadcast Venue Production for his work on Puss in Boots: Three Diablos at the 40th Annual Annie Awards. 

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 