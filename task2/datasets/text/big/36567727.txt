Nobody Walks
{{Infobox film
| name = Nobody Walks
| image = Nobody_Walks_movie_poster.jpg
| image_size = 
| alt = 
| caption = Official film poster
| director = Ry Russo-Young
| producer = 
| writer = Lena Dunham Ry Russo-Young
| starring = John Krasinski  Olivia Thirlby  Rosemarie DeWitt Jane Levy
| music = 
| cinematography = Christopher Blauvelt
| production design =
| editing =
| studio =
| distributor = Magnolia Pictures
| released =  
| runtime =
| country = United States
| language = English
| gross    = $25,342
}} independent drama film directed by Ry Russo-Young. The film premiered in Competition at the 2012 Sundance Film Festival and won a special Jury Prize.  

The film stars John Krasinski, Olivia Thirlby, Rosemarie DeWitt, India Ennenga, Jane Levy and Justin Kirk, and was co-written by Russo-Young and Lena Dunham. 

Magnolia Pictures released the film on VOD September 6, 2012 and in theaters October 12, 2012.

== Plot ==
   Silver Lake area of Los Angeles when she moves into a wealthy familys pool house, and begins working to complete work on her art film. Meanwhile, Peter (Krasinski), a laid-back father of two, agrees to his wifes request to help their young guest complete the project. The more time Martine spends with her surrogate family, the more apparent it becomes no one will walk away from this situation unchanged.

==Reception==
Rotten Tomatoes gives the film a score of 38% based on 39 reviews. 

==References==
 
  
==External links==
*  
*  
*  

 
 
 
 
 
 
 


 