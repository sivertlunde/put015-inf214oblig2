Uncle Kent
 
{{Infobox film
| name = Uncle Kent
| image = Uncle Kent film movie poster.jpg
| director = Joe Swanberg
| producer = Kent Osborne Joe Swanberg
| writer = Kent Osborne Joe Swanberg
| starring = Kent Osborne Jennifer Prediger Kevin Bewersdorf Joe Swanberg
| cinematography = Joe Swanberg
| editing = Joe Swanberg
| distributor = IFC Films
| released =  
| runtime = 72 minutes
| country = United States
| language = English
}}

Uncle Kent is a 2011 American film directed by Joe Swanberg.  

==Synopsis==
Kent Osborne is a forty-year-old single animator who meets Kate, a New York journalist, through Chatroulette. Kate accepts Kents invitation to visit L.A. for the weekend. Kate reveals that her heart belongs to another man. Frustrated, Kent attempts to make sense of the situation.

== Cast ==
* Kent Osborne — Kent
* Jennifer Prediger — Kate
* Josephine Decker — Josephine
* Joe Swanberg — Joe
* Kevin Bewersdorf — Kev

== References ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 


 