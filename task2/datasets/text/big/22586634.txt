Aloïse (film)
 
{{Infobox film
| name           = Aloïse
| image          =
| caption        =
| director       = Liliane de Kermadec
| producer       = Alain Dahan
| writer         = Liliane de Kermadec André Téchiné
| starring       = Isabelle Huppert
| music          =
| cinematography = Jean Penzer
| editing        = Claudine Merlin
| distributor    =
| released       =  
| runtime        = 115 minutes
| country        = France
| language       = French
| budget         =
}}

Aloïse is a 1975 French drama film directed by Liliane de Kermadec. It was entered into the 1975 Cannes Film Festival.   

==Plot==
The unsuccessful Swiss artist Aloïse Corbaz finds work at the court of the German emperor and gets infatuated. Showing frank symptoms of insanity she is hospitalised.

==Cast==
* Isabelle Huppert - Aloïse jeune / Aloïse as a child
* Delphine Seyrig - Aloïse adulte / Aloïse as an adult
* Marc Eyraud - Le père dAloïse / Aloïses father
* Michael Lonsdale - Le médecin directeur / The second doctor
* Valérie Schoeller - Élise jeune / Élise as a child
* Monique Lejeune - Élise adulte / Élise as an adult
* Julien Guiomar - Le directeur de théâtre
* Roger Blin - Le professeur de chant / The singing teacher
* Jacques Debary - the old director
* Roland Dubillard - the teacher
* Jacques Weber -the engineer
* Nita Klein - the head nurse
* Hans Verner - the chaplain
* Alice Reichen - la microphonée
* François Chatelet (actor)|François Chatelet - the priest
* Fernand Guiot - the priest in the asylum

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 