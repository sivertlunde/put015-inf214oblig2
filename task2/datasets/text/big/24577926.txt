The Boy Friend (1926 film)
{{Infobox film
| name           = The Boy Friend
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Monta Bell
| producer       = 
| writer         = Alice D.G. Miller (adaptation)
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Marceline Day John Harron Gwen Lee
| music          =  Henry Sharp
| editing        = Blanche Sewell
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 65 mins.
| country        = United States Silent English English intertitles
| budget         = 
| gross          =
}}
 1926 American Elizabeth Patterson. 

==Plot==
Comedy about a small-town girl unhappy with her family, and a boy trying to please her by throwing a big party.

==Cast==
* Marceline Day – Ida May Harper
* John Harron – Joe Pond
* George K. Arthur – Book Agent
* Ward Crane – Lester White
* Gertrude Astor – Mrs. White
* Otto Hoffman – Mr. Harper
* Maidel Turner – Mrs. Wilson
* Gwen Lee – Pettie Wilson Elizabeth Patterson – Mrs. Harper

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 