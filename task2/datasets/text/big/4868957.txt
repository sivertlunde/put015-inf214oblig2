The Story of Mankind (film)
 
{{Infobox film
| name = The Story of Mankind
| image = The Story of Mankind 1957 Film.jpg
| image_size =
| caption = 1957 US Theatrical Poster
| director = Irwin Allen
| producer = Irwin Allen George E. Swink Charles Bennett
| based on = The Story of Mankind by Hendrik Willem van Loon
| starring =Ronald Colman Vincent Price 
| music = Paul Sawtell
| cinematography = Nicholas Musuraca
| editing = Roland Gross Gene Palmer
| studio = Cambridge Productions
| distributor = Warner Brothers
| released =  
| runtime = 100 minutes
| language = English
| country  = United States
}}

The Story of Mankind (1957 in film|1957) is an American fantasy film, very loosely based on the nonfiction book The Story of Mankind (1921) by Hendrik Willem van Loon. 

==Production background== documentaries The Sea Around Us and The Animal World. 

Like Allens previous two films, it features vast amounts of stock footage, in this case, battles and action scenes culled from previous Warner Bros. costume films, coupled with cheaply shot close-ups of actors on much smaller sets. This was the last film picture to feature the three Marx Brothers (and their only film in Technicolor), although they are seen in separate scenes rather than acting together.  This was also the last film of star Ronald Colman, the last film of character actor Franklin Pangborn, and the last American film of Hedy Lamarr.

Years later, The Story of Mankind was included as one of the choices in the 1978 book The Fifty Worst Films of All Time.

== Plot ==
Scientists have developed a weapon, called the "Super H-bomb", which if detonated will wipe out the human race entirely.  A "High Tribunal" in "The Great Court of Outer Space" is called upon to decide whether divine intervention should be allowed to stop the bombs detonation.  The devil (Vincent Price), who goes by the name of Old Scratch|Mr. Scratch, prosecutes mankind while the Spirit of Man (Ronald Colman) defends it.  

Price and Colman are allowed to take the tribunal to any period of time to present evidence for mankinds salvation or damnation.  They take the tribunal from prehistory through Egyptian, Greco-Roman, medieval, Renaissance, Enlightenment, and modern times, looking at historical figures played by a host of guest stars.

Ultimately the tribunal is asked to rule.  The high judge, facing Mr. Scratch and the Spirit, with a large assemblage of peoples in their native costumes behind them, declares that the good and evil of mankind is too finely balanced.  A decision is suspended until they return.  When they do come back they expect to see a resolution of humanitys age old struggle with itself.

== Cast ==
*Ronald Colman as The Spirit of Man
*Vincent Price as The Devil|Mr. Scratch
*Hedy Lamarr as Joan of Arc
*Groucho Marx as Peter Minuit
*Harpo Marx as Sir Isaac Newton
*Chico Marx as Monk
*Virginia Mayo as Cleopatra
*Agnes Moorehead as Queen Elizabeth I
*Peter Lorre as Nero
*Charles Coburn as Hippocrates
*Sir Cedric Hardwicke as High Judge
*Cesar Romero as Spanish Envoy
*John Carradine as Khufu
*Dennis Hopper as Napoleon Bonaparte  Marie Wilson as Marie Antoinette
*Helmut Dantine as Marc Antony
*Edward Everett Horton as Sir Walter Raleigh
*Reginald Gardiner as William Shakespeare
*Marie Windsor as Joséphine de Beauharnais
*George E. Stone as Waiter
*Cathy ODonnell as Early Christian Woman
*Franklin Pangborn as Marquis de Varennes
*Melville Cooper as Major Domo Bishop Cauchon
*Francis X. Bushman as Moses
*Jim Ameche as Alexander Graham Bell David Bond as Early Christian
*Nick Cravat as Devils Assistant
*Dani Crayne as Helen of Troy
*Richard H. Cutting as Court Attendant
*Anthony Dexter as Christopher Columbus
*Toni Gerry as Wife
*Austin Green as Abraham Lincoln
*Eden Hartford as Laughing Water
*Alexander Lockwood as Promoter
*Melinda Marx as Early Christian Child
*Bart Mattson as Cleopatras Brother
*Don Megowan as Early Man Marvin Miller as Armana
*Nancy Miller as Early Woman
*Leonard Mudie as Chief Inquisitor
*Burt Nelson as Second Early Man Tudor Owen as High Tribunal Clerk
*Ziva Rodann as Egyptian Concubine
*Harry Ruby as Indian Brave
*William Schallert as Earl of Warwick
*Reginald Sheffield as Julius Caesar
*Abraham Sofaer as Indian Chief Bobby Watson as Adolf Hitler Sam Harris as Nobleman in Queen Elizabeths Court (uncredited)
*Angelo Rossitto as Dwarf in Neros Court (uncredited)
*Paul Zastupnevich as Apprentice (uncredited)

==Home media==
Warner Home Video released the film as part of its Warner Archive made-to-order DVD line on July 20, 2009 in the United States.

==See also==
* List of American films of 1957

==References==
 

==External links==
* 
*  
* 
*   (The Irwin Allen News Networks Story of Mankind page)

 
 

 

 
 
 
 
 
 
 
 
 