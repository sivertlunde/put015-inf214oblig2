Bungalow 13
{{Infobox film
| name           = Bungalow 13
| image          =
| caption        =
| director       = Edward L. Cahn
| producer       = Sam Baerwitz
| writer         = 
| starring       = 
| music          = Edward J. Kay
| cinematography = Jackson Rose
| editing        = Louis Sackin
| distributor    = 20th Century Fox
| released       = 18 November 1948
| runtime        = 78 min
| country        = United States
| language       = English 
| budget         =
}}

Bungalow 13 is a 1948 American film.
 Richard Cromwell.

== Cast ==

* Tom Conway as Christopher Adams Margaret Hamilton as Mrs. Theresa Appleby Richard Cromwell as Patrick Macy
* James Flavin as Lt. Sam Wilson
* Marjorie Hoshelle as Alice Ashley
* Frank Cady as Gus Barton
* Eddie Acuff as José Fernando
* Robert Alda appears
* Jody Gilbert as Mrs. Martha Barton
* Juan Varro as Pedro Gomez
* Lyle Latell as Willie
* Mildred Coles as Hibiscus John Davidson as Mr. Eden
* Cy Kendall as Police Officer (unbilled)
* Robert Malcolm as Sergeant Cox (unbilled)
* Anne Nagel as Henrietta (unbilled)

== External links ==

*  
*  

 
 
 
 


 