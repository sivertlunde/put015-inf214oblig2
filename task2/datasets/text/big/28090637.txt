Jannie totsiens
Jannie South African psychological horror film directed by Jans Rautenbach and starring Cobus Rossouw, Katinka Heyns, Jill Kirkland and Don Leonard.  A new arrival to a mental institute is ostracised by the other patients, until they use him as a scapegoat when another patient dies. It has been viewed as representing an allegory of South African society at the time. 

==Main cast==
* Hermien Dommisse - Magda 
* Katinka Heyns - Linda 
* Jill Kirkland - Liz 
* Patrick Mynhardt - George 
* Cobus Rossouw - Jannie Pienaar 
* Don Leonard
* Dulcie Van den Bergh

==References==
 

==Bibliography==
* Tomaselli, Keyan. The cinema of apartheid: race and class in South African film. Routledge, 1989.

==External links==
* 

 
 
 
 
 
 