Diva (1981 film)
 
 
{{Infobox film
| name           = Diva
| image          = DivaPoster.jpg
| caption        = Film poster
| director       = Jean-Jacques Beineix
| producer       = Claudie Ossard Irène Silberman Serge Silberman
| screenplay     = Jean-Jacques Beineix Jean Van Hamme
| based on       =  
| starring       = Frédéric Andréi Wilhelmenia Wiggins Fernandez Richard Bohringer 
| music          = Vladimir Cosma
| cinematography = Philippe Rousselot
| editing        = Monique Prim Marie-Josèphe Yoyotte
| studio         = Les Films Galaxie Greenwich Film Productions
| distributor    = Compagnie Commerciale Française Cinématographique
| released       =  
| runtime        = 117 minutes  
| country        = France
| language       = French English FRF  then about $1.5m (USA)
| gross          = $2,678,103 (USA)   
}} thriller film adapted from realist mood of 1970s French cinema and return to a colourful, melodic style, later described as cinéma du look.
 cult classic and was internationally acclaimed.

==Plot==
Young postman Jules is obsessed with Cynthia Hawkins, a beautiful and celebrated opera singer who has never had a performance of hers recorded. He attends her performance, secretly and illegally records it. He also steals the gown she wore from her dressing room.

Jules also comes into possession of a tape that contains the testimony of a prostitute which exposes Saporta, a high-ranking policeman, as the boss of various rackets. The prostitute, Nadia, drops the recording in the bag of the postmans moped moments before she is murdered.
 bohemian Serge Gorodish, to whom she introduces Jules. 

Meanwhile, feeling guilty, Jules returns Hawkins dress. She is initially angry, but eventually, forgives him. Cynthia is intrigued by the young Jules adoration and a kind of romantic relationship develops, expressed by the background of the piano instrumental, "Promenade Sentimentale" of Vladimir Cosma, as they walk around Paris in the Jardin des Tuileries early one morning. The Taiwanese try to blackmail Cynthia into recording for them as they claim that they have a copy of her performance. 

Meanwhile Saporta has sent his henchmen to take care of Jules and the other tape. After a chase through the Parisian subway system Jules is rescued from them  by Gorodish. Later Jules returns to his home where Saporta tries to kill him. Once again Gorodish saves the day by making Saporta fall down an elevator shaft. 

In the films final scene Jules plays his tape of Cynthias performance for her and she expresses her nervousness over hearing it, as she "never heard   sing."

==Cast==
* Frédéric Andréi as Jules Wilhelmenia Wiggins Fernandez as Cynthia Hawkins
* Roland Bertin as Weinstadt
* Richard Bohringer as Gorodish
* Gérard Darmon as L Antillais
* Chantal Deruaz as Nadia
* Jacques Fabbri as Jean Saporta
* Patrick Floersheim as Zatopek
* Thuy An Luu as Alba
* Dominique Pinon as Le Curé
* Dominique Besnehard as the record store employee

==Soundtrack==
Highlights of the soundtrack include the aria Ebben? Ne andrò lontana from Alfredo Catalanis opera La Wally, and a pastiche of Erik Saties Gnossiennes composed by Vladimir Cosma.

==Reception==

===Initial reaction=== David Denby, New York, upon its 1982 American release, wrote "One of the most audacious and original films to come out of France in recent years...Diva must be the only pop movie inspired by a love of opera." 

Film critic Roger Ebert gave it four out of four stars and praised its cast of characters.    He called Beineix "a director with an enormous gift for creating visual images" and elaborated on his filmmaking:

 
 The French Connection, and Bullitt." 

===Retrospect=== average score of 8 out of 10.  Lisa Schwarzbaum of Entertainment Weekly gave it an A rating and praised its "voluptuous romanticism". She wrote of the films visual ties to cinéma du look, "the movies mad excitement hinges entirely on the pleasure to be had in moving our eye from one gorgeously composed stage set of artifice to another." 

==Awards==
* César Awards:
**  
**  
**  
** César Award for Best Sound|Sound: Jean-Pierre Ruh
 Best Foreign Language Film at the 54th Academy Awards, but was not accepted as a nominee. 

==See also==
* List of submissions to the 54th Academy Awards for Best Foreign Language Film
* List of French submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  
*   by Roger Ebert

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 