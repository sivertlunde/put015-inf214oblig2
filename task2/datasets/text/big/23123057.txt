Fanfan
{{Infobox film
| name           = Fanfan
| image          = Fanfan.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Alexandre Jardin
| producer       = Alain Terzian
| screenplay     = Alexandre Jardin
| based on       =  
| starring       = {{Plainlist|
* Sophie Marceau
* Vincent Perez
* Marine Delterme
}}
| music          = Nicolas Jorelle
| cinematography = Jean-Yves Le Mener
| editing        = {{Plainlist|
* Joëlle Hache
* Claire Pinheiro
}}
| studio         = {{Plainlist|
* Alter Films Gaumont
* Canal+
}}
| distributor    = Gaumont
| released       =  
| runtime        = 85 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Fanfan (Fanfan & Alexandre) is a 1993 French romantic comedy film written and directed by Alexandre Jardin and starring Sophie Marceau and Vincent Perez.    This film is based on the directors best-selling 1990 novel, which was translated into almost two dozen languages.   

==Plot==
Alexandre (Vincent Perez) is frustrated with his fiancé, Laure (Marine Delterme), whose idea of a romantic Valentines Day gift is a pair of slippers. The passion and magic of their initial courtship has been replaced by routine and boredom. Needing to get away, Alexandre visits the beachfront cottage of his friends, Ti (Gérard Séty) and Maude (Micheline Presle). There he meets a beautiful young woman, Fanfan (Sophie Marceau), who is Maudes granddaughter and a student at Maudes school, studying to become a perfumer or Nose. Although they sleep in the same bed that night, Alexandre avoids any physical contact.

The next day they are joined by Ti and Maude, who become confidants to their guests. Fanfan takes Alexandre to an old abandoned house where they learn more about each other in the quiet comfort of her childhood "hideout". Back at his apartment, Alexandre discovers his fiancé in the bedroom dressed as an Italian call girl, hoping the charade will excite his libido and rejuvenate their relationship—but the charade does not work. Later at her parents house, Alexandre becomes even more discouraged by Laures fathers talk of him joining the funeral business and the gloomy outlook on marriage.

Meanwhile, Fanfan confesses to a friend that she is in love with Alexandre. For Alexandre, however, things are more complicated. He explains to Ti that his past relationships have all followed the same disappointing pattern—the seduction and passion at the start is inevitably replaced with routine and boredom once a relationship becomes sexual. When asked what the alternative is, Alexandre offers his solution: Platonic love. To avoid the banality of "coupledom" in his new relationship with Fanfan, he will court her forever without ever revealing his love for her. Hell never kiss her or take their relationship to a physical level, reasoning that excitement is found in unfulfilled desire. He will stay with Laure physically, but will stay in love with Fanfan forever.

Alexandre and Fanfan meet for a date at a beautiful upscale apartment, which Alexandre says belongs to his father. While Fanfan reveals personal and intimate details about her life, Alexandre prepares all her favorite foods from childhood, and the two share a romantic evening by candlelight. The perfect evening is interrupted, however, when the apartments owner returns home with his wife, who proceeds to call the police as Alexandre and Fanfan make their escape.

Alexandre blindfolds Fanfan and drives her to a television studio to a set designed to be Vienna in 1813. He dresses her in a ballroom gown, removes the blindfold, and they waltz together, gazing lovingly into each others eyes. They end their magical evening with a bottle of champaigne on an imaginary balcony overlooking an imaginary Vienna, with Alexandre proposing a toast to their "friendship". Fanfan is clearly disappointed and annoyed with Alexandres talk of "friendship", and she asks to be dropped off at the home of her friend Paul—a sculptor doing a nude study of her.

Alexandre returns home to an equally annoyed Laure, who suspects hes been with another woman. Alexandre admits it, but assures her that nothing will ever come of it. As Laure plans out their wedding, Alexandre can actually see them turning into her parents. Laure knows something is wrong, saying, "I wish I werent yours so you would want me again." Meanwhile, Fanfan complains to Maude about Alesandres strange behavior. Unlike Alexandre, Fanfan believes in "eternal passion for life." Fanfan learns from Maude that Alexandre has been living with Laure for five years.

Alexandre and Fanfan continue to socialize as friends, but the "friendship" only creates more frustration. They meet at a restaurant and Fanfan gives him an ultimatum: he must choose between Laure and her. While Alexandre claims to want only friendship, Fanfan knows he is attracted to her by his scent. Later when she isnt looking, Alexandre pours sleeping powder in her drink, and she falls into a deep sleep on the ride home. She wakes up on the beach at the cottage, where Alexandre watches over her and applies suntan lotion sensually on her back. Tempted by the half-naked Fanfan, he moves to kiss her when suddenly Laure arrives by boat and announces that the wedding plans are set and that she is pregnant. After congratulating the couple, Fanfan leaves in disgust.

Unable to concentrate on her studies, Fanfan fails her perfumer final exams. Meanwhile, Alexandre discovers that Laure lied about being pregnant, and after he throws away the wedding invitations, Laure calls off the wedding. Alexandre then sees Fanfan boarding a bus to Italy, and he chases after her. When he reaches the bus at the next stop, he does not get on, and the bus pulls away. Later, Ti warns him that Fanfan will not wait forever. When Alexandre learns that she will be in Italy for ten days, he sets in motion his next plan. He rents the apartment next to Fanfans apartment, knocks out the dividing wall, and installs a two-way mirror so he can see into her apartment while remaining hidden.

Back from Italy, Fanfan and Alexandre arrange to spend the weekend at the beach cottage as friends. At the train station, however, Fanfan surprises Alexandre by bringing her sculptor friend, Paul. Clearly disappointed and jealous, Alexandre tells her that he and Laure have broken up and that her pregnancy was a lie. Fanfan simply responds, "Too late." At the beach, Paul announces that he and Fanfan are getting married, but back at the apartment, Alexandre discovers through the two-way mirror that their wedding plans were just a ruse to make Alexandre jealous, and that Fanfan is still in love with him.

Later, after learning about the two-way mirror, Fanfan confronts Alexandre who is hidden behind the mirror. He confesses he did not get on the bus to Italy that day because he knew that in five years he would no longer be running after the bus. They kiss and embrace each other tenderly through the glass, but Fanfan says she needs more. "I need to feel you in me," she says, but Alexandre cannot respond. Exasperated, Fanfan gives him another ultimatum: if he is not in her room by 10:00 pm, hell never see her again. Alexandre does not show, and when he returns to his apartment, he sees that shes moved out and left a note that she is going to see her sister. Devastated that shes gone, Alexandre calls Maude only to find out that Fanfans sister is dead, and she could not see her other than by committing a suicide. Alexandre envisions trying to get to Fanfan through endless bridal veil, and cries for being too late to touch her living body. Just then the mirror is cracked—Fanfan has broken the wall of glass between them. The two kiss and embrace, and Fanfan tells him he has until the morning to win her back.

==Cast==
* Sophie Marceau as Fanfan
* Vincent Perez as Alexandre
* Marine Delterme as Laure 
* Gérard Séty as Ti
* Bruno Todeschini as Paul
* Arielle Séménoff
* Marcel Maréchal as Le père de Fanfan
* Gérard Caillaud as Le père de Laure
* Micheline Presle as Maude
* Patrick Aubrée
* Jean-Marie Cornille
* Béatrice Esterle as La mère de Fanfan
* Pierre Gérald
* Thierry Lhermitte
* Maxime Lombard
* Samuel Sogno
* Mathilde Vitry   

==Reception==

===Box office===
The film topped the box office on its opening weekend in France, playing at 204 screens and selling 188,577 admissions.    In total the film sold 1,020,679 tickets after its run in French theaters.   

===Critical response===
Fanfan received mixed reviews. In his review in Films de France, James Travers found the first half of the film "unconvincing and painfully superficial in places," but thought the film improved significantly in the second half:
  }}

Fanfan received an 81% positive audience rating on the aggregate reviewer web site Rotten Tomatoes based on 1,457 user ratings.   

==References==
;Citations
 
;Bibliography
 
*  
 

==External links==
*  
*  
*  

 
 
 
 