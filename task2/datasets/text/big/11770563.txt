Custer of the West
{{Infobox film
| name           = Custer of the West
| image          =
| caption        =
| director       = Robert Siodmak
| producer       = Irving Lerner Philip Yordan  Louis Dolivet Bernard Gordon  Julian Zimet Robert Shaw Jeffrey Hunter Ty Hardin Mary Ure
| music          = Bernardo Segall
| cinematography = Cecilio Paniagua
| editing        = Peter Parasheles Maurice Rootes
| studio         = Security Pictures MGM (2004, DVD)
| released       =   (World Premiere, London)
| runtime        = 141 minutes
| country        = United States
| language       = English
| budget         =
}} Robert Shaw as Custer, Robert Ryan, Ty Hardin, Jeffrey Hunter and Mary Ure. The film was shot entirely in Spain. 

The plot of the film was very close to that of the 1941 film They Died with Their Boots On, in which Errol Flynn played Custer.

==Plot==
With no better offers to be had, famous American Civil War upstart officer George Armstrong Custer takes over the Western Cavalry maintaining the peace in the Dakotas. He soon learns that the U.S. treaties are a sham, that Indian lands are being stolen and every excuse for driving them off their hunting grounds is being encouraged. With his wife Elizabeth (Mary Ure) Custer goes in and out of favor in Washington, D.C.|Washington, while failing to keep wildcatting miners like his own deserting Sergeant Mulligan (Robert Ryan) from running off to prospect for gold in Indian country. After trying to humble the prideful Indian warrior Sitting Bull (Kieron Moore), Custer leads the 7th Cavalry into defeat.

==Reception== native Americans by American troops, and the portrayal of Custer as an American hero who was not to blame for the disaster. The general inaccuracies of the film were also questioned, particularly the portrayal of the Battle of the Little Bighorn.

== Cast ==
 Robert Shaw as General George Armstrong Custer
* Mary Ure as Elizabeth Custer
* Ty Hardin as Major Marcus Reno
* Jeffrey Hunter as Captain Frederick Benteen
* Lawrence Tierney as General Philip Sheridan
* Marc Lawrence as the gold miner
* Kieron Moore as Chief Sitting Bull
* Charles Stalmaker as Lt. Howells
* Robert Hall as Sgt. Buckley
* Robert Ryan as Sgt. Mulligan

==DVD==
Custer of the West was released to DVD by MGM Home Video on May 25th, 2004, as a Region 1 widescreen DVD.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 