Duet for One
{{Infobox film
|  name     = Duet for One
|  image          = Duetforoneposter.jpg
|  caption       = Theatrical release poster
|  director       = Andrei Konchalovsky
|  writer         = Tom Kempinski
|  starring       = Julie Andrews Alan Bates Max von Sydow
|  distributor    = Golan-Globus|Golan-Globus Productions Ltd.
|  released   =25 December 1986 |    
  runtime        = 107 minutes
|  country        = United Kingdom
| language = English 
|gross= $8,736  
}}
Duet for One (1986) is a British film adapted from the play, a two-hander by Tom Kempinski, about a world-famous concert violinist named Stephanie Anderson who is suddenly struck with multiple sclerosis.  It is set in London and directed by Andrei Konchalovsky.  The story is based on the life of cellist Jacqueline du Pré, who was diagnosed with MS, and her husband, conductor Daniel Barenboim, and only marginally fictionalized.

==Synopsis==
Stephanie Anderson is suffering from multiple sclerosis and she is slipping into the depths of depression. She begins seeing a psychiatrist and despises him for not being able to feel her pain. Her conductor husband is also drifting away from her, having an affair with his secretary. Stephanie shuts herself away from the world, once locking her door and replaying her old concert tapes, watching despairingly as her on-screen self plays music that she will never be able to create again. She attempts suicide but fails when her maid rescues her. Soon, however, she comes to terms with the facts of her bitter end and realizes that life must go on.

==Primary cast==
*Julie Andrews : Stephanie Anderson 
*Alan Bates : David Cornwallis
*Max von Sydow : Dr. Louis Feldman
*Rupert Everett : Constantine Kassanis Margaret Courtenay : Sonia Randvich
*Cathryn Harrison : Penny Smallwood
*Macha Méril : Anya
*Liam Neeson : Totter
*Paula Figgett : Totters Daughter

==Reception==
The movie gained positive reviews.   

==Awards==
Golden Globe for Best Actress in a Drama Role : Julie Andrews (Nominated) 

==Stage play== West End and on Broadway theatre|Broadway.  A major revival was staged by the Almeida Theatre in 2009, starring Juliet Stevenson and Henry Goodman. This revival too was lauded by the critics, and it subsequently transferred to the Vaudeville Theatre in the West End. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 