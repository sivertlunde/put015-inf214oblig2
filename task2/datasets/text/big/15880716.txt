American Beer (film)
{{Infobox Film
| image =American Beer film poster.jpg
| name = American Beer: A Bockumentary
| director = Paul Kermizian
| producer = Paul Kermizian
| starring = Jeremy Goldberg Paul Kermizian Jon Miller Robert Purvis Richard Sterling
| cinematography = Jon Miller
| editing = Paul Kermizian
| released = 2004
| runtime = 100 min.
| country = United States
| language = English
}}
 documentary directed craft brewing industry. The film was shot in the spring of 2002. Kermizian and a group of four left New York City and traveled by minivan across the United States visiting 38 craft and independent breweries in 40 days.  

Breweries featured in the film include Dogfish Head Brewery, Shipyard Brewing, Victory Brewing Company, McNeills Brewery, Climax Brewing, Sierra Nevada Brewing Company, Anchor Brewing Company, New Glarus Brewing Company, New Belgium Brewing Company, Bells Brewery and others.

The film screened at various film festivals throughout 2004 and 2005  and was released on DVD towards the end of 2005. A special edition of the DVD released shortly afterward contains more than 80 minutes of deleted scenes from breweries featured in the film along with other scenes deleted from the original cut.

As a result from their experiences from the film, Paul Kermizian ended up opening Barcade, a chain of hybrid arcade and craft beer bars in New York, New Jersey and Philadelphia,  and Jeremy Goldberg started a brewery called Cape Ann Brewing in Gloucester, Massachusetts. 

==See also==
* Beer Wars

==References==
 

==External links==
* 
* 
*  

 
 
 
 
 
 
 


 