By og land hand i hand
{{Infobox film
| name           = By og land hand i hand
| image          = 
| image size     =
| caption        = 
| director       = Olav Dalgard
| producer       = 
| writer         = Olav Dalgard
| narrator       =
| starring       = Hans Bille   Lars Tvinde 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1937
| runtime        = 58 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama 1937 local elections.

The wealthy landowner Hans Bjørnstad (Bille) is approached by his workers for a raise, but is shocked by their radical socialist ideas. Later he talks to another landowner, Nils Tveit (Tvinde), who is more sympathetic to the workers case. Through conversations with family members in Oslo, the two realise the advantages Labour government has brought, and the necessity of cooperation between town and country for the prosperity of the country.

==External links==
*  
*   at the Norwegian Film Institute  

 
 
 
 