Bombay Priyudu
{{Infobox film
| name           = Bombay Priyudu
| image          = Bombay Priyudu Telugu DVD.jpg
| caption        =
| director       = K. Raghavendra Rao
| producer       = K. Krishna Mohana Rao RK Film Associates
| writer         = Satyanand Rambha Vanisri Betha Sudhakar Brahmanandam Tanikella Bharani Chitti Babu Punyamurthula
| music          = M. M. Keeravani
| cinematography = Chota K. Naidu
| editing        = Marthand K. Venkatesh
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu romance Rambha in the lead roles.   

==Plot== falls in love with her at first sight. Bombay (Brahmanandam), who steals a gold chain from a jewellers, drops it in Raga Sudhas bag while being chased by the police. The bag goes into Chitti Babus gypsy, who finds his dream girls photograph in it.

Chitti Babu dons the guise of a richman and makes her come to different places to collect her bag but does not give it. Chitti Babu meets her and tells that he will help her in getting her bag. She falls for Chitti Babu.  Later, she finds that the bag is with Chitti Babu and accuses him of cheating her. In the process of appeasing his lady love, Chitti Babu meets with an accident and ends up in a hospital. This rekindles Raga Sudhas love for him.

Raga Sudhas mother Dhana Lakshmi (Vanisri) returns from abroad with a prospective groom (Sivaji Raja) for her daughter. Coming to know of her daughters love affair, she takes her away to Hyderabad and sets up a date for her wedding because Dhana Lakshmis love is an anathema. Chitti Babu saves Dhana Lakshmi from goons sent by her manager (Benarjee). He is introduced himself as J.D. to Dhana Lakshmi. She appoints him as the bodyguard of Raga Sudha. The manager kidnaps Raga Sudha while they are going to  Srisailam for marriage by bus. Raga Sudhas maternal uncle Buchiki (Amanchi Venkata Subrahmanyam|A.V.S.) reveals that J.D. is none other than Chitti Babu. The manager wants to marry Raga Sudha and tries to rape her in the running bus. Chitti Babu fights Benarjee and marries Raga Sudha with the blessings of Dhana Lakshmi.

==Cast==
* J. D. Chakravarthy as Chitti Babu/J.D. Rambha as Raga Sudha
* Vanisri as Dhana Lakshmi
* M. Balaiah as Dhana Lakshmis father
* Betha Sudhakar as Pyarelal
* Brahmanandam as Bombay
* Tanikella Bharani as Kaima Patel
* Amanchi Venkata Subrahmanyam|A.V.S. as Buchiki
* Babu Mohan as P.K. Rao
* Sivaji Raja as P.K. Raos son
* Chitti Babu Punyamurthula as singer
* Gundu Hanumantha Rao as Pandu
* Benarjee as Dhana Lakshmis manager

==Awards==
*Nandi Award for Best Female Playback Singer - K. S. Chitra - Pranayama

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 