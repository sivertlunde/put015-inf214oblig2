Ragdoll (film)
{{Infobox film
| name           = Ragdoll
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Ted Nicolaou
| producer       = Kirk Edward Hansen
| writer         = Benjamin Carr
| screenplay     = 
| story          = Charles Band
| based on       =  
| starring       =  
| narrator       = 
| music          = Booker T. Jones III
| cinematography = Mac Ahlberg
| editing        =
| studio         = Full Moon Features
| distributor    = Full Moon Features
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} American horror Devil Dolls. 

==Plot==
A teenage rapper uses his Grans magic powers to help him in his revenge against those who put her in the hospital.

==Cast==
 
* Russell Richardson as Kwame
* William L. Johnson as Gene
* Jennifer Echols as Woman Detective
* Derrick Jones as Man
* Rick Michaels as Second Detective
* Freda Payne as Gran
* Jay Williams as Emcee
* Rejjie Jones as Third Detective
* Jennia Fredrique as Teesha
* Tarnell Poindexter as Little Mikey 
* William Stanford Davis as Pere
* Danny Wooten as Gem
* Troy Medley as Louis
* Frederic Tucker as Shadow Man
* Lamar Haywood as Agent
* Jemal McNeil as Bartender 
* Renee ONeil as Sylvie
 

==Production==
The film was originally announced in 1992,  but it did not begin pre-production until 1999.   Charles Band stated that rumors that Paramount had shut down production of the film were untrue; he chose not to shoot the film, as he felt that it needed more time to develop. 

==Release==
Big City Records, a music label owned by Full Moon, released an associated soundtrack, Ragdoll: Music Inspired By The Motion Picture.  This was the labels first release. 

==Reception==
Dread Central praised the edited version of the film for Devil Dolls, writing "here’s a halfway decent story here and some enjoyably hammy acting bolstering it".   

==References==
 

==External links==
*  

 

 
 
 
 
 