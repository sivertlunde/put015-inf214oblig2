White Valentine
 
{{Infobox film
| name = White Valentine
| image = White Valentine Poster.jpg
| caption = Promotional poster for White Valentine
| director = Yang Yun-ho
| producer = Kim Yong-kuk
| writer = Lee Eun-kyeong   Lee Byeong-ryul
| starring = Park Shin-yang  Jun Ji-hyun
| music = Park Ki-young
| cinematography = Lee Seok-gi
| editing = Kyung Min-ho
| distributor = 
| released =  
| runtime = 101 min.
| language = Korean
| budget = 
| film name      = {{Film name
 | hangul =  
 | rr = Hwaiteu ballenta-in
 | mr = Hwait‘ŭ palrent‘ain}}
}}
White Valentine ( ) is a 1999 Korean romantic film directed by Yang Yun-ho. It stars Park Shin-yang with Jun Ji-hyun in her movie debut.
 military service epistolary exchange he sent to his dead girlfriend by a carrier pigeon.

==Plot== soldiers because they never write her back once they learn her age. So instead, she pretends to be a teacher, and becomes pen pals with Park Hyun-jun (Park Shin-yang). They plan to meet in person at a train station, but Jeong-min never shows up, and thus their correspondence ends. 

Later when Jeong-min reaches the age of twenty (though when this movie was shot Jun was only 17), Hyun-jun moves into her hometown. Since her parents died when she was little, Jeong-min has been living with her grandfather who owns the bookstore Somang Books. She also works at this bookstore but dreams of becoming a painter. Hyun-jun has become the owner of a pet shop for birds, and grieving over the death of his girlfriend in a car accident, he keeps sending her letters via carrier pigeon. While painting outdoors, Jeong-min sees Hyun-jun feeding some pigeons. As she watches him care for one wounded pigeon then give an apple to a child playing nearby, she falls in love with him at first sight. But the apple reminds her of the same apple painted on the envelopes from her yet unknown pen pal.

==Cast==
*Park Shin-yang - Park Hyun-jun
*Jun Ji-hyun - Kim Jeong-min
*Jeon Moo-song - Jeong-mins grandfather
*Kim Young-ok - flower shop lady
*Yang Dong-geun - Han-seok
*Kim Se-joon - Ji-seok

== External links ==
*  
*  
*  

 
 
 
 

 