Alaipayuthey
{{Infobox film
| name           = Alaipayuthey
| image          = Alaipaytheyfrontbig.jpg
| caption        = 
| director       = Mani Ratnam
| writer         = Mani Ratnam (dialogue)
| screenplay     = Mani Ratnam
| story          = Mani Ratnam R. Selvaraj
| starring       = R. Madhavan Shalini
| producer       = Mani Ratnam G. Srinivasan
| distributor    = Madras Talkies Ayngaran International
| music          = A. R. Rahman
| cinematography = P. C. Sriram
| editing        = A. Sreekar Prasad
| released       =  
| runtime        = 157 minutes
| country        = India
| language       = Tamil
}}
 Tamil romantic drama film directed by Mani Ratnam. Starring R. Madhavan and Shalini, the film explores the tensions of married life and the maturing of love. The score and soundtrack were composed by A. R. Rahman.
 recollected in flashbacks by the character Karthik (Madhavan). Karthik and Shakthi (Shalini) fall in love against the backdrop of Chennai and its suburban trains, against the wishes of their parents. The film was well received by critics and it did well at the Tamil and Telugu box-offices primarily for its music and realistic portrayal of romance and married life. It was dubbed into Telugu as Sakhi.

The film made its European premiere at the Berlin International Film Festival in 2001. It was shown at various film festivals nationally and internationally. It also carried a very popular soundtrack, composed by A. R. Rahman. Alaipayuthey was later remade in Hindi cinema as Saathiya (film)|Saathiya, directed by Shaad Ali in 2002, starring Rani Mukherjee and Vivek Oberoi.

==Plot==
Karthik Varadarajan (R. Madhavan|Madhavan) is an independent and free-spirited software engineering graduate establishing a software start-up with his friends. At a friends wedding, he meets Shakti Selvaraj  (Shalini), a student of medicine at Madras Medical College. They continue to bump into each other on local trains that they both use on their daily commute, and fall in love. Karthik pursues Shakti aggressively and proposes marriage; Shakthi, however, is reluctant. Karthik manages to convince Shakthi and requests his parents to formally ask Shaktis parents for her hand in marriage; however, when the parents meet, they do not get along, and Shakti calls off the relationship altogether and leaves for an extended medical camp in Kerala. 

While apart, both Karthik and Shakti realize that they are desperately in love and decide to get married without the knowledge or consent of their parents. They continue living separate lives after marriage, meeting outside of their homes and hoping that their parents will see eye to eye at some point in the future and can be informed of the marriage. However, when Shaktis older sister Poorni (Swarnamalya) is on the verge of getting engaged, developments ensue resulting in Shaktis parents attempting to fix her marriage to the groom Raghus younger brother Shyam (Karthik Kumar). Shakti confesses to her parents and Raghus family that she is already married; Poornis engagement is called off and her parents throw her out of the house. Karthik too divulges the same to his parents and is also asked by his father to leave his house. 

Karthik and Shakti start living together and while all goes well for a while, they soon find that marriage is not as easy as they expected and living under the same roof results in a large number of conflicts. The marriage gets increasingly tense as both cope with frustrations and disappointments. 

Shakti learns that her father is seriously ill and requests Karthik to visit him. Karthik declines, citing her fathers hatred of him as the main reason. He eventually agrees, but by the time they reach her house, Shakti father passes away. Wracked with guilt, the two return home, their relationship taking a turn for the worse.

The two stop talking to each other. Meanwhile, Karthik takes it upon himself to sort out Poornis love life and her broken engagement to Raghu. He arranges a blind date between the two, which initially fails, backfiring on Karthik, however, with more meetings, Poorni and Raghu become closer. This development takes place without Shakthis knowledge. Karthik waits until Poorni and Raghus marriage is confirmed before deciding to tell Shakti. However, Shakti witnesses Poorni hugging Karthik in gratitude at the train station and unable to see Poornis face, misunderstands what she has seen to be Karthik hugging a woman she assumes he is having an affair with. 

Shakthi eventually learns from Poorni of her husbands efforts in getting her married, and is overcome with guilt and love. Karthik sets off on the same evening to the railway station to pick up his wife as is their usual routine. However, Shakti in her rush to get home and make up with Karthik meets with a serious accident. Karthik waits for his wife, and as she fails to turn up searches desperately for her throughout the city, ultimately discovering her in the ICU of a hospital, registered under another name and having undergone brain surgery. Shakti is in a coma.  

An IAS officer Ram (Arvind Swamy) admits that he caused the accident and admitted Shakti in the hospital. Karthik is inconsolable, wracked with grief and anger, and takes his frustration out on Ram. However, Rams wife (Kushboo) intervenes and lets Karthik know that she was the person who caused the accident and injured Shakti, and her husband was merely trying to protect her by taking the blame himself. Karthik observes Ram and notes that he has a lot to learn from the latter. He proceeds to Shaktis bedside and admits that he could have been a better husband. Shakti proceeds to wake up from her coma and the two reconcile, admitting their enduring love for one another.

==Cast==
 
* R. Madhavan as Karthik Varadharajan
* Shalini as Shakthi Selvaraj
* Jayasudha as Saroja
* Swarnamalya as Poorni Vivek as Sethu
* Pyramid Natarajan as Varadharajan
* Raviprakash as Selvaraj
* Venu Arvind as Arumugam
* K. P. A. C. Lalitha
* Sukumari
* Azhagam Perumal as Nayar
* Hari Nair as Raghuraman
* Karthik Kumar as Shyam
* Arvind Swamy as Ram (guest appearance)
* Kushboo as Meena (guest appearance)
* Sophiya Haque in a special appearance
 

==Production==
  dynasty, in Maheshwar.]] Madhavan to Vikram was Sriranjini made Raviprakash appeared as Shalinis father thus making his acting debut with the film.   Venu Aravind and Pondy Ravi appeared as police officers.  Mani roped in producer Pyramid Natarajan to portray the character of Madhavans father.  Azhagam Perumal who was one of the assistant directors in the film was chosen to portray the small role of an house owner as Mani was looking for "someone like Jagathy Sreekumar to play the quirky house owner". 

The film also required two leading actors to appear in supporting roles with Kushboo roped in to do a role. After considering either Shahrukh Khan, Mammooty or Mohanlal, Mani Ratnam signed Arvind Swamy to play another role, with Alaipayuthey becoming the pairs fourth production together.  P. C. Sriram renewed his collaboration with Mani Ratnam after seven years, with the director toggling between Santosh Sivan and Rajiv Menon for his other projects.  A. R. Rahman was initially signed on just to compose the background score for the film as the film was originally planned to be "songless", however after a change of heart, nine songs were recorded.   

The film began without an official launch, like other Mani Ratnam projects, and it was initially expected that filming would be wrapped up under four months. During the first seven days of the shoot, Mani Ratnam filmed portions featuring Shalini and made Madhavan stay on the sets and watch his process of film-making.  The first scene the actor shot was the post-interval scene featuring Shakthis mother played by Jayasudha. The team shot in Srinagar in late for 25 days, becoming the last production team to shoot in the area until 2003 as a result of the Kashmir conflict.   A "meet the stars" publicity event was held at Music World in Spencer Plaza in March 2000, with the gathering being described as a success.   About the production process, Madhavan revealed that he learnt about the technical aspects of film-making from the director and mentioned that he even learned the entire script of the film, irrespective of whether I was in the scene or not claiming that working with Mani Ratnam inspires that sort of involvement and dedication. 

==Release==
The Hindu cited the film saying, "The wavy movements are not restricted to the title card alone. Alaipayuthey goes backward and forward in time and the movement holds a thin thread of suspense too. The oscillation from joy and levity to seriousness and sorrow creates impressive waves", The lead pair performance was praised saying, "Shalini once again proves that she is a natural performer while Madhavan sails through the litmus test with ease".  Similarly Indolink.com mentioned that "Manis directorial mind and heart sways beautifully like his film" and that the film is "worth seeing with the whole family".  However Rediff.com gave the film a middling review citing that the film is "old wine in an old bottle" and that "the only person who scores good marks in the film is P. C. Sreeram", describing "he has used his camera like a paint brush and the strokes are so stunningly beautiful that, once the film is over, one remembers only the visual treat".    In regard to performances, the critic mentions that Madhavan "looks pleasant and handsome and does his job splendidly until the end, where he looks totally lost in the most crucial scene" and that Shalini "is very beautiful but not as open as she used to be as a child star". 

Alaipayuthey was dubbed and released as Sakhi in Telugu and was later remade in Hindi cinema as Saathiya (film)|Saathiya, by Mani Ratnams assistant Shaad Ali in 2002, starring Rani Mukherjee and Vivek Oberoi.  It was the first time where the director had sold off production rights of his films to be remade in another language as he had previously opted to dub and release the film himself. 

==Legacy==
The film began a successful film career for Madhavan and launched him as a "romantic hero".  He has since gone on to become a regular part of the cast in Mani Ratnams productions and featured in leading roles in Dumm Dumm Dumm (2001), Kannathil Muthamittal (2002), Aaytha Ezhuthu (2004) and Guru (2007 film)|Guru (2007).  Meanwhile Shalini had already agreed to call time on her career before release due to her pending marriage with Ajith Kumar and Alaipayuthey became her penultimate release. Swarnamalya also received several film offers after her critically acclaimed performance, but consecutive failures of eight of her ten movies since failed to catapult her into the leading bracket of actors.  Post-release, the actress had also expressed her disappointment at several of her scenes being edited out of the film.  In July 2011, Janani Iyer said she considered a role like Shalinis character from the film as "really challenging".  Gautham Menon revealed that the scene prior to the song "Evano Oruvan" was "almost straight out of real life" and that he "tried to incorporate such moments" in his films.  The film created an interest for weddings held in temple. 

==In popular culture== Arya tries to do the same with Nayantara but fails. When Madhavan saw that film, he said "It was a plesant shock to see this clip feature in BEB. It was such a sweet tribute to me. After watching this sequence, I was quite amused to see how thin I was back then!"  In Budget Padmanabhan (2000), Vivek speaks to Bhuvaneswari through cups attached with wires. Vivek utters Madhavans dialogue from the film by mimicing his voice, he then hurts Theni Kunjarammas eye by singing the films song.  In Shahjahan (film)|Shahjahan (2001), Vivek and Kovai Sarala sings "Snegithane" in a humorous vein. 

Hindi Television series Beintehaa was dubbed in Tamil as Alaipayuthe.  Songs from the film inspired several film titles - Kadhal Sadugudu (2003), Evano Oruvan (2007), Pachai Nirame (2008) and Endrendrum Punnagai (2013). Posters of Vinnaithaandi Varuvaaya (2010) were inspired from various films including Alaipayuthey. 

==Soundtrack==
{{Infobox album
|  Name        = Alaipayuthey
|  Type        = Soundtrack
|  Artist      = A. R. Rahman
|  Cover       = Alaiost.jpg
|  Background  = Gainsboro
|  Released    =  March 14, 2000
|  Recorded = Panchathan Record Inn
|  Genre       = Soundtrack
|  Length      =  42:52
|  Label       =  Sa Re Ga Ma
|  Producer    =  A. R. Rahman
|  Reviews     = 
|  Last album  =  Pukar (2000 film)|Pukar   (2000)
|  This album  =  Alaipayuthey  (2000)
|  Next album  =  Kandukondain Kandukondain   (2000)
}}
 Filmfare Award raagam Kaanada). The song "Yaro Yarodi" later appeared in the 2008 Hollywood film, The Accidental Husband. The films background score and songs are considered as one of Rahmans finest works and are still very popular throughout Tamil Nadu. The audio rights were sold to Sa Re Ga Ma. The song "Pachai Nirame" is based on Kharaharapriya Raga. 
 Karthik worked as chorus singer for the film while Clinton Cerejo made his debut as playback singer.   Song "Kadhal Sadugudu" provided major breakthrough for its singer S. P. Charan. 

{{tracklist
| headline = Track listing
| extra_column = Singer(s)
| title1 = Endendrum Punnagai
| extra1 = Clinton Cerejo, Srinivas (singer)|Srinivas, Shankar Mahadevan, A.R.Rahman
| length1 = 4:00
| title2 = Pachchai Nirame
| extra2 = Hariharan (singer)|Hariharan, Clinton Cerejo, Dominique Cerejo
| 5:58
| title3 = Kadhal Sadugudu
| extra3 = S. P. B. Charan|S.P.B. Charan
| length3 = 4:35
| title4 = Evano Oruvan
| extra4 = Swarnalatha
| length4 = 5:56
| title5 = Alaipayuthey
| extra5 = Harini, Kalyani Menon, Neyveli Ramalaxmi
| length5 = 3:34
| title6 = Snehithane Snehithane
| extra6 = Sadhana Sargam, Srinivas
| length6 = 6:05
| title7 = Maangalyam
| extra7 = Clinton Cerejo, Srinivas, A.R.Rahman
| length7 = 1:41
| title8 = Yaro Yarodi Richa Sharma
| length8 = 5:46
| title9 = September Madham
| extra9 = Asha Bhonsle, Shankar Mahadevan
| length9 = 5:08
| title10 = Snehithane Snehithane II
| extra10 = Ustad Sultan Khan, Sadhana Sargam, Srinivas
| length10 = 6:05
}}

==Filmfare Awards South== Filmfare Best Music Director Award (2000)
* R. Madhavan won Filmfare Award for Best Male Debut – South (2000)

==References==
 

==External links==
*  

==Bibliography==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 