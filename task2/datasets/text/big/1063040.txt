A Year and a Half in the Life of Metallica
{{Infobox album  
| Name        = A Year and a Half in the Life of Metallica
| Type        = video
| Artist      = Metallica
| Cover       = Metallica - A Year and a Half in the Life of Metallica cover.jpg
| Released    = November 17, 1992  Heavy metal
| Language    = English
| Length      = 236 min.
| distributor = Elektra Entertainment
| Director    = Adam Dubin
| Producer    = Juliana Roberts 
| Chronology  = Metallica video
| Last album  = 2 of One (1989)
| This album  = A Year and a Half in the Life of Metallica (1992)
| Next album  =   (1993)
}} documentary about the process of making the Metallica (album)|Metallica album (or "The Black Album"), and the following tour. It was produced by Juliana Roberts and directed by Adam Dubin.

A Year and a Half in the Life of Metallica was released as a double VHS pack. Both parts are available on a single DVD, but only in region 1.

==Part 1==
This 90-minute video shows how Metallica and their producer Bob Rock worked their way through making the Metallica album. It also includes the making of the video for "Enter Sandman" and also a listening party for invited fans to come and listen to the album in full.

This video also includes three of the music videos they shot for that album:
* "Enter Sandman" The Unforgiven"
* "Nothing Else Matters"

==Part 2== For Whom the Bell Tolls" from Donington on August 17, 1991, "Enter Sandman" from the MTV Video Music Awards on September 5, "Harvester of Sorrow" from Moscow on September 28, "Sad but True" from the Day on the Green festival in Oakland, California on October 12, "Enter Sandman" from the Freddie Mercury Tribute Concert on April 20, 1992, and "Nothing Else Matters" from Phoenix on August 25.

Jason Newsted after a concert in Portland, Maine is seen making sandwiches to take back to a hotel with him. His response to being called a cheapskate is "I got plans for those millions and it aint for fucking sandwiches!".
 Spinal Tap, who jokingly ask them about the similarities between their albums covers.

This part of the documentary also includes two music videos:
* "Wherever I May Roam"
* "Sad but True"

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 