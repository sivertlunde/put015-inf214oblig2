Gassan
{{Infobox film
| name           = Gassan
| image          = 
| caption        = 
| film name =  
| director       = Tetsutaro Murano
| producer       = 
| writer         = 
| starring       = 
| music          = Teizo Matsumura
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}}
 Japanese film directed by Tetsutaro Murano. It was Japans submission to the 52nd Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.   
 ]]

==Cast==
* Hisashi Igawa as Iwazo
* Yūko Katagiri as Kayo
* Atsuko Kawaguchi as Wife
* Chōichirō Kawarazaki as Karasu
* Kin Sugai as Kane
* Chikako Yuri as Fumiko
* Yūsuke Takita as Tasuke

==See also==
*List of Japanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 
 
 
 