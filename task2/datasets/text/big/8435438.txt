Future War
{{Infobox Film
| name           = Future War
| image          = Future war.jpg
| image_size     = 
| caption        = VHS film cover
| director       = Anthony Doublin
| producer       = Dave Eddy Tim Ubels
| story          = David Huey Dom Magwili
| screenplay     = Dom Magwili
| narrator       = Travis Brooks Stewart
| starring       = Daniel Bernhardt Travis Brooks Stewart Robert ZDar Mel Novak
| music          = Arlan H. Boll
| cinematography = Cory Geryak Ed Tillman
| editing        = Dave Eddy
| distributor    = Screen Pix Home Video
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
}}

Future War is a 1997 direct-to-video American science fiction film about an escaped human slave fleeing his cyborg masters and seeking refuge on Earth. It was lampooned in a 1999 episode of Mystery Science Theater 3000.

==Plot==
Future War begins aboard a spaceship undergoing a revolt. A man enters and activates an escape pod which travels to Earth and crashes into the Pacific Ocean. The pod contains “The Runaway,” a human slave played by Daniel Bernhardt. He is being pursued by cyborg slavers and dinosaurs that they use as “trackers.” Since he was kidnapped some time from Earth’s past, the Runaway is familiar with the English language and the King James Bible and he regards Earth as a literal heaven.

The Runaway finds refuge with novice nun Sister Ann (Travis Brooks Stewart), whose past involved dealing drugs and prostitution. Together, they fight the dinosaurs and their robotic masters, seeking help from a street gang. Future War features star Daniel Bernhardt’s kickboxing skills in several fight sequences, including against the Cyborg Master (Robert ZDar).

After being arrested as a suspect in a rash of deaths due to strange animal attacks, The Runaway is interrogated by federal agents. They present to him a dinosaur collar found on the beach. The Cyborg Master, breaks into the police station during the interrogation and the Runaway manages to escape in the confusion. He returns to Sister Ann and her gang friends with a plan to attack the dinosaurs where they live, as Runaway simply explains, "Near water...".

Using dynamite, Runaway successfully destroys a water treatment plant, killing the dinosaurs. Later, though, the surviving Cyborg Master attacks Runaway while he watches Sister Ann make her vows to become a nun. After Runaway finally kills the Cyborg Master, he becomes a counselor for runaway teens, working closely with Sister Ann.

==Cast==
* Daniel Bernhardt as The Runaway
* Travis Brooks Stewart as Sister Ann Robert Z’Dar as Cyborg Master
* Mel Novak as SWAT Leader
* Forrest J Ackerman as Park victim
* Arlan Boll music

==Production notes==
This was the directorial debut of Anthony Doublin, the award winning special effect/miniature model maker from films like Bride of Re-Animator and the Carnosaur (film)|Carnosaur films. The actual "tracker" feet and mechanical tracker from Future War were taken by Doublin for use in Carnosaur 3.

==Mystery Science Theater 3000== Mike Nelson and his crew commented on the low-budget look (several scenes are in a warehouse where people are stacking apparently empty boxes), the use of forced perspective for the dinosaur sequences and the nonsensical nature of the film. As Crow T. Robot remarked, “You know, I could point out that it’s not the future, and there isn’t a war, but you know me; I dont like to complain.”

During the movies production crew members joked that the film should end up on Mystery Science Theater 3000. When Doublin finished his work, he turned in a directors cut that ran with 40 minutes of mostly dialog. Future War was screened for several members of the crew. After the screening a discussion was held and Doublin quit, forcing a "Damage Control Unit" to be organized. Johnny Saiko from Steve Wang FX was brought in to take over the "trackers" and the film was completed and screened at AFM.   

==Release history==
Future War was released directly to home video on January 28, 1997 by Screen Pix Home Video. It was later released on Region 1 DVD by Trinity Home Entertainment on May 4, 2004.  

There was also a 2002 DVD release by EVG Digital Entertainment.  This release contained no bonus materials.  Despite the film itself being unrated, the Film Advisory Board rated this DVD EM or "Extremely Mature"  (the equivalent of an R rating from the MPAA).

The film was also released on DVD as part of the Mystery Science Theater 3000: 20th Anniversary Edition DVD set released by Shout! Factory on October 8, 2008. 

The original theme song may be purchased here: https://itunes.apple.com/us/album/future-war-theme-song-ominous/id948349290

==Notes==
 

==External links==
*  
*  
*  

===Mystery Science Theater 3000===
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 