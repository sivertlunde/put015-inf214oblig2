Annakodi
{{Infobox film
| name = Annakodi
| image = Annakodi Poster.jpg
| caption = Film poster
| director = Bharathiraja
| producer = Bharathiraja
| writer = Bharathiraja Lakshman Narayan Karthika Nair Manoj Bharathiraja
| music = G. V. Prakash Kumar
| cinematography = Saalai Sahaadevan
| editing = Pazhanivel
| studio = Manoj Creations
| distributor = Manoj Creations
| runtime =
| released = June 28, 2013
| country = India
| language = Tamil
| budget =
| gross =
}}
Annakodi is a 2013 Tamil film directed and written by Bharathiraja.  It features Lakshman Narayan, Karthika Nair and Manoj Bharathiraja in the lead roles. The film has music by G. V. Prakash Kumar.  The film was earlier titled Annakodiyum Kodiveeranum. 

==Cast==
* Karthika Nair as Annakodi Lakshman Narayan as Kodiveeran
* Manoj Bharathiraja as Sadayan Renuka
* Meenal as Narthanga
* Subiksha

==Production== Parthiepan was Ameer to portray the title roles, with Parthiepan admitting he was left in the dark about the decision.   
 Roja was Renuka stepped into the shoes of Roja. 

===Filming===
The movie started to roll from November 17, 2011. As announced, the film’s inaugural pooja took place in Theni and several biggies from the film industry participated in it. Directors K. Balachander, Mani Ratnam, Balu Mahendra were at this launch which made the event at the Veerappa Ayyanar Temple in Alli Nagaram, Theni.  

==Soundtrack==
{{Infobox album|  
| Name = Annakodi
| Type = Soundtrack
| Artist = G. V. Prakash Kumar
| Cover  =
| Released = January 20, 2013 Feature film soundtrack
| Length = Tamil
| Label =
| Producer = G. V. Prakash Kumar
| Last album =Ongole Githa (2013)
| This album =Annakodi (2013)
| Next album =Udhayam NH4 (2013)
}}
Music was composed by G. V. Prakash Kumar collaborating with Bharathiraja for first time.  After 21 years Gangai Amaran joined with Bharathiraja by writing lyrics for the film along with Vairamuthu, Egadesi and Kavingar Arivumathi.  The audio released on January 20, 2013 at Railway grounds, Arasaradi, Madurai. Ilaiyaraaja, directors J. Mahendran, Balu Mahendra, K. Bhagyaraj, Vasanth, Vikraman, S. A. Chandrasekhar, Agathiyan, actors Raadhika Sarathkumar|Radhika, Radha, Sathyaraj, Saranya Ponvannan, composers Yuvan Shankar Raja, Karthik Raja, Bhavatharini, S. Thanu, Ramesh Khanna graced the event along with the film crew. 

{{Track listing
| headline =
| extra_column = Singer(s)
| total_length =
| lyrics_credits = yes
| title1 = Aavarangaatukulla
| extra1 = Sathya Prakash, Chinmayi
| lyrics1 = Vairamuthu
| length1 =
| title2 = Pothi Vecha
| extra2 = G. V. Prakash Kumar, Prashanthini
| lyrics2 = Arivumathi
| length2 =
| title3 = Nariga Uranga
| extra3 = Santhosh, Pooja, Harini Sudhakar
| lyrics3 = Vairamuthu
| length3 =
| title4 = Poraale
| extra4= S. P. B. Charan, Manasi MM
| lyrics4 = Gangai Amaran
| length4 =
| title5 = Annamae
| extra5= G. V. Prakash Kumar, Pooja Vaidyanath
| lyrics5 = Egadesi
| length5 =
| title6 = Kola Vaala Edungada
| extra6= Padayapa Sriram, A. R. Reihana, Maya
| lyrics6 = Egadesi
| length6 =
}}

==Release== Sun TV. The film was given a "U/A" certificate by the Indian Censor Board and released on June 28, 2013 alongside Thulli Vilayadu.

===Critical reception===
Rediff wrote:"film lacks depth and fails to ignite the passion needed for such an emotionally compelling story".  Indian express wrote:"Long and dreary, and testing one’s patience at times, it’s a disappointing fare from the ace director".  Behindwoods wrote:"To sum up, this melodramatic movie doesnt have a new story to tell and the closing message about love being beyond all such issues like caste, creed and religion is again a really dated thought".  In.com wrote:"Overall, Annakodi is typical Bharathiraja style village story but it lacks the intensity and soul seen in the directors earlier ones". 

==References==
 

 

 
 
 
 