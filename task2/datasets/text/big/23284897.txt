Vasya (film)
{{Infobox Film
| name           = Vasya
| image          = vasya.jpg
| image_size     = 
| caption        = 
| director       = Andrei Zagdansky
| producer       = Andrei Zagdansky (Producer) Andrei Razumovsky (Co-producer)
| writer         = Andrei Zagdansky
| narrator       = 
| starring       = 
| music          =  Yevgeni Smirnov
| editing        = Andrei Zagdansky
| distributor    = Facets Multimedia Inc.
| released       = 2002
| runtime        = 60 minutes
| country        = United States
| language       = Russian, English
| designer       = 
| gross          = 
}}
 Russian underground underground artist Soviet authorities. A man without a passport, in and out of mental asylums, he was the key and often "larger than life" figure of the nonconformist art movement in the Soviet Union.
In 1975 fearing prosecution and another involuntary commitment to a mental asylum he immigrated to Austria and then to the United States. He died virtually unknown in 1987 in NYC. 

A number of prominent artists appear in the film, such as Dmitri Plavinsky Vladimir Titov, Kevin Clarke, poet and publisher Konstantyn K. Kuzminsky and art collector Norton Dodge, who has amassed one of the largest collections of Soviet-era art outside the Soviet Union. 

==Additional Credits==
*Animation by Signe Baumane
*Original score by Alexander Goldstein
*AZ Films L.L.S. and Fora Film
*© 2002 AZ Films L.L.C.

==External links==
* 
* 

 
 
 
 
 
 
 
 


 