All in the Family (film)
 
 
{{Infobox film
| name           = Hua Fei Man Cheng Chun
| image          = AllintheFamily.jpg
| caption        = 
| director       = Mu Zhu
| producer       = Raymond Chow
| writer         = Ken Suma
| narrator       =  James Tien Dean Shek
| music          = 
| cinematography = 
| editing        =  Golden Harvest
| released       =  
| runtime        =  Hong Kong Cantonese
| budget         = 
| gross          = HK $1,062,710.50
| preceded_by    = 
| followed_by    =
}} 1975 Hong Golden Harvest productions. Despite starring in the film, Jackie Chan does not appear until 1 hour into the movie.

==Plot==
A family gathers to be with its dying father. The reunion brings old rivalries to the surface.

Mr. Hu is dying, so calls his family to his bedside. After his death, his three sons divide Mr. Hus belongings, leaving their mother and sister with nothing. Between them, the sister and mother come up with a plan to get revenge. The sister starts a rumour that her mother is actually very rich. On hearing the rumour, the three sons quickly return home and vie for their mothers favor in the hope of getting the money from her.

Spring Lady is tiring of her husband, Ma, and has an eye on Little Tang, a rickshaw boy. Little Tang is in love with Lin-tze, but as Lin-tze is happily married to Chang Hsun, his attentions are soon swayed by Spring Lady. Although Ma discovers the affair, he is too busy to care.

==Cast==
*Jackie Chan
*Sammo Hung
*Dean Shek James Tien

==Overview==
 rickshaw driver. It contains one of only two sex scenes which Chan has done, the other one being in Shinjuku Incident. It is the only film Chan has starred in where there wasnt a single fight or stunt sequence.

In 2006, Information Times quoted Chans response to an article in the Hong Kong media regarding the film. {{cite web
  | title = Hua Fei Man Cheng Chun
  | publisher = CSFD - Česko-Slovenská Filmová Databáze
  | url = http://www.csfd.cz/film/11386-hua-fei-man-cheng-chun/
  | accessdate = 2007-08-29}} 
 

  that Jackie Chan has done.]]

==See also==
* Jackie Chan filmography
* Sammo Hung filmography

==References==
 

==External links==
* All in the Family (film) at  
*  

 
 
 
 


 
 