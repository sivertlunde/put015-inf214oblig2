A Gentleman of Paris (1931 film)
{{Infobox film
| name = A Gentleman of Paris
| director = Sinclair Hill
| based on =  
| released =  
| country = United Kingdom
| language = English
}}
A Gentleman of Paris is a 1931 crime drama film directed by Sinclair Hill, based on the story "His Honour, the Judge" by Niranjan Pal.

==Cast==
* Arthur Wontner as Judge Le Fevre
* Vanda Gréville as Paulette Gerrard
* Hugh Williams as Gaston Gerrard
* Phyllis Konstam as Madeleine
* Sybil Thorndike as Lola Duval
* Arthur Goullet as Bagot George Merritt as M. Duval
* Frederick Lloyd as Advocate
* George De Warfaz as Valet
* Florence Wood as Concierge

==External links==
*  
* British Film Institute Review of the film http://www.bfi.org.uk/whatson/bfi_southbank/events/seniors_free_matinee_a_gentleman_of_paris

 

 
 
 
 
 
 
 
 


 