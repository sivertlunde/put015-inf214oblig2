Tarzan the Tiger
{{Infobox film
| name           = Tarzan the Tiger
| image          = Tarzan the Tiger (movie poster).jpg
| image_size     =
| caption        = poster
| director       = Henry MacRae
| producer       =
| writer         = Ian McClosky Heath (screenplay)
| based on       =   Frank Merrill as Tarzan Natalie Kingston as Jane Al Ferguson Kithnou Sheldon Lewis
| music          =
| cinematography = Wilfred M. Cline
| editing        = Malcolm Dewar
| distributor    = Universal Pictures
| released       =  
| runtime        = 15 chapters (266 min)
| country        = United States English
| budget         =
}} Universal Serial movie serial Frank Merrill as Tarzan, Natalie Kingston as Jane Porter (Tarzan)|Jane, and Al Ferguson.  It was written by Ian McClosky Heath and directed by Henry MacRae.
 lost at one time but a copy has since been found.  Today the serial is available on DVD and, in the public domain, available for download on the internet.

==Synopsis==
Lord Greystoke (Tarzan) returns to Africa, with Lady Jane and friend Albert Werper, in order to return to Opar (fictional city)|Opar.  He needs the treasure of Opar in order to secure his estates in England.  Werper, however, is actually interested in the gold himself.  He is in league with Arab slave trader Achmet Zek who wishes revenge on Tarzan and Lady Jane for himself.

==Cast== Frank Merrill as "The Lord of the Manor—known to London as the Earl of Greystoke—and to the Jungle as Tarzan, the Tiger!"  Frank Merrill reprised his role as Tarzan from Tarzan the Mighty.  His performance in these two serials makes him the last silent Tarzan and the first sound Tarzan.  Merrill did his own stunts and devised the original Tarzan Yell.       Lady Jane, his wife, who has left the gaiety of London Society to share his life on the Jungle plantation"  Natalie Kingston was again cast as the love interest but this time played the traditional character of Lady Jane instead of Mary Trevor (from Tarzan the Mighty). The change was not explained in the serial.   Soldier of Fortune—a guest at Greystoke Manor in the guise of a friendly Scientist"  Al Ferguson was also again cast as the villain of the story but not the same character (or even a slightly renamed character, as with Jane.  In Tarzan the Mighty he played the pirate Black John). 
*Kithnou as "The High Priestess of the Sun Worshipers—La (Tarzan)|La, who has sworn that she will have no other mate than Tarzan"  Mademoiselle Kithnou was a dancer and actress of mixed Indian and European descent from Puducherry, at that time in French India, or possibly from Mauritius.    
*Sheldon Lewis as "Achmet Zek, a Nomad Chief, against whose traffic in slaves Tarzan has waged relentless war"

Quoted text from the opening credits for each character.
  Frank Merrill
Image:Tarzan the Tiger Kingston1.JPG | Opening credit for Natalie Kingston
Image:Tarzan the Tiger Ferguson1.JPG | Opening credit for Al Ferguson
Image:Tarzan the-Tiger Kithnou.JPG | Opening credit for Kithnou
 

==Production== Queen La of Opar]]Tarzan the Tiger was a sequel based on the success of Tarzan the Mighty.  
 topless in a swimming sequence in chapter 8. "It is said that fathers sometimes accompanied their sons to the showings." 

A further sequel, to create a trilogy of Frank Merrill Tarzan serials, was planned.  The third entry would have been called Tarzan the Terrible. However, Merrills voice was deemed unsuitable for sound films and the sequel was cancelled.   Merrill made personal appearances in costume to promote the serial. During these, he realised how much influence he had on children. Combined with the issues over his voice this led him to retire after this serial and devote his life to children. He become a Recreational Director for the Parks commission of the Los Angeles city administration. 
 Tarzan the Ape Man.
  

==Chapter titles==
  as Tarzan]]
#Call of the Jungle
#The Road to Opar
#The Altar of the Flaming God
#The Vengeance of La
#Condemned to Death
#Tantor the Terror
#The Dealy Peril
#Loop of Death
#Flight of Werper
#Prisoner of the Apes
#The Jaws of Death
#The Jewels of Opar
#A Human Sacrifice
#Tarzans Rage
#Tarzans Triumph

==See also==
* Nudity in film
==References==
 

==External links==
 
* 
* 
* 

===Download or view online===
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 