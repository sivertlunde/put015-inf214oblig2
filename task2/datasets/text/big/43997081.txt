Seetha (1980 film)
{{Infobox film
| name           = Seetha
| image          =
| caption        =
| director       = P Govindan
| producer       =
| writer         = M Mukundan
| screenplay     = M Mukundan Ambika
| music          = M. K. Arjunan
| cinematography = Madhu Ambatt
| editing        = P Raman Nair
| studio         = Chanthu Films
| distributor    = Chanthu Films
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, Ambika in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Sukumari
*Thikkurissi Sukumaran Nair
*Nagavally R. S. Kurup Ambika
*Aranmula Ponnamma
*Muralimohan
*Sasi
*Vidhubala
*Sukumaran

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ammayum Makalum || Vani Jairam || Sreekumaran Thampi || 
|-
| 2 || Naazhikakal than changalakal || Jolly Abraham || Sreekumaran Thampi || 
|-
| 3 || Prabhaathamenikku nee || P Jayachandran || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 


 