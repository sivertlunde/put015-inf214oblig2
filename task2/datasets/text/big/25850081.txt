List of Malayalam films of 1990
 

The following is a list of Malayalam films released in the year 1990.

{| class="wikitable sortable"
!  width="25%" | Title
!  width="18%" | Director
!  width="15%" | Story
!  width="18%" | Screenplay
!  width="30%" | Cast
|- valign="top"
| Four First Nights
| Khomineni
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Varthamana Kalam
| I. V. Sasi
| Rare Malayalam film depicting both the strengths and weakness of women
| Prasanth
| Urvashi(actress)|Urvashi, Suresh Gopi, Balachandramenon, Jayaram, Lalu Alex, Jagannathavarma
|- valign="top"
| Enquiry (film)|Enquiry
| U. V. Ravindranath
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Aye Auto
| Venu Nagavally
| Venu Nagavalli
| Venu Nagavalli Thikkurisi
|- valign="top"
| Orukkam (film)|Orukkam
| K. Madhu
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Purappadu (1990 film)|Purappadu
| Jeassy
| &nbsp;
| &nbsp;
| Mammootty
|- valign="top"
| Chuvanna Kannukal
| Sasi Mohan
| &nbsp;
| &nbsp;
| Sugandhi, Shyamala
|- valign="top"
| Malootty
| Bharathan John Paul John Paul Baby Shamili
|- valign="top"
| Akkare Akkare Akkare
| Priyadarshan
| &nbsp; Sreenivasan
| Parvathy
|- valign="top"
| Sunday 7 PM
| Shaji Kailas
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Sasneham
| Sathyan Anthikkad
| Lohithadas
| Lohithadas
| Balachandra Menon, Shobhana
|- valign="top"
| No.20 Madras Mail
| Joshy
| Dennis Joseph
| Dennis Joseph Suchitra
|- valign="top"
| Maanmizhiyaal
| Krishnaswamy
| &nbsp;
| &nbsp;
| &nbsp;

|- valign="top"
| Naale Ennundengil Sajan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Urvashi (film)|Urvashi
| P. Chandrakumar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Nidrayil Oru Rathri
| Asha Khan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kottayam Kunjachan
| T. S. Suresh Babu
| Muttathu Varkey
| Dennis Joseph Ranjini
|- valign="top"
| Vachanam
| Lenin Rajendran
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Commander (film)|Commander
| Cross Belt Mani
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Niyamam Enthu Cheyyum
| Arun
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Shesham Screenil
| P. Venu
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Anantha Vruthantham
| P. Anil
| &nbsp;
| &nbsp; Ranjini
|- valign="top"
| Mrudula
| Antony Eastman
| &nbsp;
| &nbsp;
| Captain Raju, Master Raghu
|- valign="top"
| Raja Vazcha Sasi Kumar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| His Highness Abdullah
| Sibi Malayil
| A. K. Lohithadas
| A. K. Lohithadas
| Mohanlal, Nedumudi Venu, Sreenivasan (actor)|Sreenivasan, Gauthami
|- valign="top"
| Mouna Daaham
| K. Balakrishnan
| &nbsp;
| &nbsp;
| Harish 
|- valign="top"
| Midhya
| I. V. Sasi
| M.T.Vasudevan Nair
| M.T.Vasudevan Nair
| Mammooty, Suresh Gopi

|- valign="top"
| Mukham Mohan
| &nbsp;
| &nbsp; Ranjini
|- valign="top"
| Kadathanadan Ambadi
| Priyadarsan
| &nbsp;
| Saaragapani
| Mohanlal, Prem Nazir
|- valign="top"
| Brahma Rakshass
| Vijayan Karote
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Aaram Vardil Aabhyanthara Kalaham Murali
| &nbsp;
| &nbsp;
| Vineeth, Thilakan, Siddique (actor)|Siddique, Priya, Sugandhi
|- valign="top"
| Aadhi Thaalam
| Jayadevan
| &nbsp;
| &nbsp;
| Jayalalitha (actress)|Jayalalitha, Arya
|- valign="top"
| Innale
| P. Padmarajan
| &nbsp;
| &nbsp;
| Jayaram, Shobana, Suresh Gopi
|- valign="top"
| Pavam Pavam Rajakumaran Kamal
| &nbsp;
| &nbsp; Siddique
|- valign="top"
| Dr. Pasupathy
| Shaji Kailas
| &nbsp;
| Renji Panicker
| Innocent (actor)|Innocent, Mamukkoya, Rizabawa, Parvathy Jayaram|Parvathy, Jagadish
|- valign="top"
| Thooval Sparsam Kamal
| &nbsp;
| &nbsp;
| Jayaram, Mukesh (Malayalam actor)|Mukesh, Saikumar (Malayalam actor)|Saikumar, Ranjini (actress)|Ranjini, Urvashi (actress)|Urvashi, Suresh Gopi
|- valign="top"
| Mathilukal
| Adoor Gopalakrishnan
| Vaikom Muhammed Basheer
| Adoor Gopalakrishnan
| Mammootty
|- valign="top"
| Veena Meettiya Vilangukal
| Cochin Haneefa
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Chuvappu Naada
| K. S. Gopalakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Thaazhvaaram
| Bharathan
| &nbsp;
| M. T. Vasudevan Nair
| Mohanlal, Sumalatha
|- valign="top"
| Apsarassu
| K. S. Gopalakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Noottonnu Raavukal
| Sasi Mohan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Samrajyam
| Jomon
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Nanma Niranjavan Srinivasan
| Viji Thampi
| &nbsp;
| &nbsp;
| Jayaram
|- valign="top"
| Superstar (1990 film)|Superstar
| Vinayan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Rathilayangal
| Khomineni
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Aalasyam
| P. Chandrakumar
| &nbsp;
| &nbsp; Abhilasha
|- valign="top"
| Kalikkalam
| Sathyan Anthikkad
| S. N. Swamy
| S. N. Swamy
| Mammootty, Shobana
|- valign="top"
| Pavakkoothu
| K. Sreekkuttan
| &nbsp;
| &nbsp; Ranjini
|- valign="top"
| Kattukuthira
| P. G. Viswambharan
| S. L. Puram Sadanandan
| S. L. Puram Sadanandan
| Thilakan, Vineeth, Anju (actress)|Anju, Kaviyoor Ponnamma
|- valign="top"
| Ee Kanni Koodi
| K. G. George
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Cheriya Lokavum Valiya Manushyarum
| Chandrasekharan
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Jagathy, Innocent (actor)|Innocent, Mamukkoya, Geetha Vijayan
|- valign="top"
| Minda Poochakku Kalyanam
| Alleppey Ashraf
| &nbsp;
| &nbsp; Lizy
|- valign="top"
| Rosa I Love You
| P. Chandrakumar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Thaalam (film)|Thaalam
| T. S. Mohan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Shankaran Kuttikku Pennu Venam
| K. S. Sivachandran
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Paadatha Veenayum Paadum Sasi Kumar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Arhatha
| I. V. Sasi
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Nammude Naade
| K. Suku
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Oliyambukal Hariharan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Shubha Yathra Kamal
| &nbsp;
| &nbsp; Parvathy
|- valign="top"
| Thalayanamanthram
| Sathyan Anthikkad
| &nbsp; Sreenivasan
| Urvashi (actress)|Urvashi, Sreenivasan (actor)|Srinivasan, Jayaram, Parvathy Jayaram|Parvathy, K.P.A.C. Lalitha
|- valign="top"
| Iyer The Great
| Bhadran
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Indrajaalam
| Thampy Kannanthanam
| &nbsp;
| &nbsp;
| Mohanlal, Geetha (actress)|Geetha, Anupam Kher
|- valign="top"
| Vasavadatta (film)|Vasavadatta
| K. S. Gopalakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kuttettan
| Joshi
| &nbsp;
| &nbsp;
| Mammootty, Saritha, Maathu
|- valign="top"
| Ponnaranjanam
| P. R. S. Babu
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kouthuka Varthakal
| Thulasidas
| &nbsp;
| &nbsp; Ranjini
|- valign="top"
| Saandhram
| Ashokan Thaha
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kuruppinte Kanakku Pustakom
| Balachandra Menon
| &nbsp;
| &nbsp;
| Balachandra Menon, Geetha (actress)|Geetha, Jayaram
|- valign="top"
| Marupuram
| Viji Thampi
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Radha Madhavam
| Suresh Unnithan
| &nbsp;
| &nbsp; Parvathy
|- valign="top"
| Kshanakkathu
| Rajeev Kumar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Thrisandhya (film)|Thrisandhya
| Raj Marbros
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Appu (1990 film)|Appu
| Dennis Joseph
| &nbsp;
| &nbsp; Sunitha
|- valign="top"
| Gajakesariyogam
| P. G. Viswambaran
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Sunitha (actress)|Sunitha, Innocent (actor)|Innocent, K. P. A. C. Lalitha
|- valign="top"
| In Harihar Nagar
| Siddiqe, Lal
| Siddiqe, Lal
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Jagadeesh, Siddique (actor)|Siddique, Ashokan (actor)|Ashokan, Geetha Vijayan
|- valign="top"
| Champion Thomas
| Rex
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Keli Kottu
| T. S. Mohan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Vyuham (film)|Vyuham
| Sangeeth Sivan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ee Thanutha Veluppan Kalathu
| Joshy
| &nbsp;
| P. Padmarajan
| Mammootty, Nedumudi Venu, Sumalatha, Lakshmi
|- valign="top"
| Malayogam
| Sibi Malayil
| &nbsp;
| &nbsp; Innocent
|- valign="top"
| Randam Varavu
| K. Madhu
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| May Dinam
| A. P. Satyan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top" Lal Salam
| Venu Nagavally
| &nbsp;
| Cherian Kalpakavadi
| Mohanlal, Murali (Malayalam actor)|Murali, Geetha (actress)|Geetha, Urvashi(actress)|Urvashi, Rekha(Malayalam actress)|Rekha, Jagathy Sreekumar
|- valign="top"
| Vidhyarambham
| Jayaraj
| &nbsp;
| &nbsp; Murali
|- valign="top"
| Apoorva Sangamam
| Sasi Mohan
| &nbsp;
| &nbsp; Sandhya
|- valign="top"
| Judgement (1990 film)|Judgement
| K. S. Gopalakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Parampara (1990 film)|Parampara
| Sibi Malayil
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kanakachilanka (1990 film)|Kanakachilanka
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ghazal (1990 film)|Ghazal
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Prosecution (film)|Prosecution
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Beauty Palace
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Raagam Sreeragam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Unnikuttanu Joli Kitti
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kanninilavu
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Sthreekku Vendi Sthree
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Indhanam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Avalkkoru janmam koodi
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ammayude Swantham Kunju Mary
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|}

==Dubbed films==
{| class="wikitable sortable"
!  width="25%" | Title
!  width="18%" | Director
!  width="15%" | Story
!  width="18%" | Screenplay
!  width="30%" | Cast
|- valign="top"
| Sabarimala Sri Ayyappan
| Renuka Sharma
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Avasanathe Rathri
| K. S. Gopalakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Geethanjali (1989 film)|Geethanjali
| Mani Ratnam
| &nbsp;
| &nbsp; Girija
|}

==References==
 
* 
* 
* 

 
 
 

 
 
 
 
 