The Wedding Ring (1944 film)
{{Infobox film
| name           = The Wedding Ring
| image          = 
| caption        = 
| director       = Martin Frič
| producer       = 
| writer         = Josef Neuberg Josef Hlavác
| starring       = Otomar Korbelár Eman Fiala
| cinematography = 
| editing        = Jan Kohout
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

The Wedding Ring ( ) is a 1944 Czech comedy film directed by Martin Frič.   

==Cast==
* Otomar Korbelár as Jan Sochor
* Marie Blazková as Sochorova druhá zena
* Nadezda Vladyková as Sochorova první zena
* Hermína Vojtová as Sochorova první tchyne
* Vlasta Fabianová as Krezna
* Frantisek Smolík as Kníze Ferdinand Andres
* Zdenek Díte as Robert, mladý kníze
* Jana Dítetová as Baruska Sochorová
* Ruzena Slemrová as Baronka
* Jindrich Plachta as Doktor
* Jaroslav Marvan as Farár
* Vladimír Repa as Lesní
* Ferenc Futurista as Kocí Václav

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 