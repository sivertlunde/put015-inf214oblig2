Beyond the Farthest Star (film)
{{Infobox film
| name           = Beyond the Farthest Star
| image          = 
| alt            = 
| caption        = 
| director       = Andrew Librizzi
| producer       = Benjamin Dane Sally Helppie Andrew Librizzi B. Scott Senechal
| writer         = Andrew Librizzi
| starring       = Renée OConnor Todd Terry Cherami Leigh
| music          = 
| cinematography = Ron Gonzalez
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Beyond the Farthest Star is a 2013 limited-release faith-based film directed by Andy Librizzi starring Renée OConnor, Todd Terry, and Cherami Leigh. The film was shot on location in Leonard, Texas.   

The film was distributed via the Seatzy method of crowdsourcing audiences, under which at least 500 people must make a reservation to watch the film before it can be brought to a cinema. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 