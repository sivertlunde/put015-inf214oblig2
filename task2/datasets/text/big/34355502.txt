Sang Penari
 
 
 
 
{{Infobox film
| name           = Sang Penari
| image          = Sang Penari poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Promotional poster
| director       = Ifa Isfansyah
| producer       = Shanty Harmayn
| writer         = Salman Aristo Ifa Isfansyah Shanty Harmayn
| screenplay     = 
| story          = 
| based on       =  
| starring       = Nyoman Oka Antara Prisia Nasution
| music          = Wong Aksan Titi Sjuman
| cinematography = Yadi Sugandi
| editing        = 
| studio         = Salto Films
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Indonesia Banyumasan
| budget         = 
| gross          = 
}}
Sang Penari (The Dancer) is a 2011 Indonesian film based on the trilogy of novels Ronggeng Dukuh Paruk by Ahmad Tohari and directed by Ifa Isfansyah. Starring Nyoman Oka Antara and Prisia Nasution, it tells the story of a young man and his friendship with his small villages new ronggeng.
 New Order ethnic Javanese. Nasution, cast in her debut role, is Batak (Indonesia)|Batak, while Antara is Balinese people|Balinese.
 Best Picture, out of nine nominations.

==Plot==
In 1953, two tempeh bongkrèk makers in Dukuh Paruk, a small hamlet in Banyumas, Central Java, accidentally sell poisoned tempeh, which kills many residents, including the much respected ronggeng (local traditional dancer). The residents of the hamlet begin panicking and rioting, causing the tempeh makers to commit suicide. Their daughter, Srintil, survives and is raised by her grandfather Sakarya (Landung Simatupang).

Ten years later in 1963, Srintil (Prisia Nasution) and Rasus (Nyoman Oka Antara) are fast friends. Rasus also has romantic feelings for her. With the hamlet starved and in a depression since the loss of its ronggeng, Sakarya receives a vision that Srintil will become a great ronggeng, capable of saving the hamlet from starvation. He then convinces Srintil to become a ronggeng. She then tries to proves herself to Kartareja (Slamet Rahardjo), the hamlets ronggeng caretaker and his wife (Dewi Irawan) by dancing at the grave of Ki Secamenggala, the hamlets founder. Her attempt is only successful after Rasus gave her the ronggeng amulet belonged to the late ronggeng of Dukuh Paruk. Seeing this amulet, Kartareja then announces that Srintil has been chosen by the founders spirit. Meanwhile, Indonesian Communist Party member Bakar (Lukman Sardi) arrived in the hamlet and convincing local farmers to join the party, saying that the Communist party are the only ones who can help save the wong cilik (underclass) of Dukuh Paruk and their starved hamlet.

After the success of her dance at the grave of Ki Secamenggala, Srintil is told that she must undergo a ritual before she can truly become a ronggeng, called bukak klambu (literally "opening the veil"); her virginity will be sold to the highest bidder. This upsets Rasus, who tells Srintil that he is not comfortable with her becoming a ronggeng. Srintil says that she will give her virginity to Rasus, and on the day of the bukak klambu they have sex in a goat shack; that evening, Srintil has sex with two other "highest bidders" and becomes a full ronggeng.
 army base, where he befriends Sergeant Binsar (Tio Pakusadewo). Binsar teaches him to read and wins Rasus trust. Meanwhile, the residents of Dukuh Paruk start to embrace communism under Bakars leadership, despite their unawareness of political knowledge. During Rasus military time, Dukuh Paruks ronggeng troupe which includes Kartareja, Sakarya, Sakum the blind kendhang player, and Srintil becomes increasingly popular and become involved in many rally events organised by the communist party.

Two years later, following the failed 30 September Movement|Communist-led coup détat in Jakarta. Rasus is sent by Binsar in operations to clear the presence of Communists in the area. However, when Dukuh Paruks turn comes in the massacre, Rasus hurries back, leaving his army comrades to his hamlet to find and save Srintil. He found Dukuh Paruk have been destroyed and void of its inhabitants, leaving only Sakum, the blind kendhang player. His continued effort ended in vain as Rasus arrives in a hidden army concentration camp just as Srintil is taken away by the army and disappear along with the rest of Dukuh Paruks residents.

Ten years later, Rasus met a street dancer and a blind man in a village close to Dukuh Paruk who resemble Srintil and Sakum. He quickly stops her, giving her the amulet of Dukuh Paruks ronggeng which he found in Dukuh Paruk during his search for Srintil ten years ago. The dancer nervously accepts it and leave Rasus, who smiles, signalling his recognition of his love Srintil. The movie ends with the dancer and the blind man dancing away and disappear in the horizon.

==Production==
Sang Penari was directed by Ifa Isfansyah.  Husband and wife team Wong Aksan and Titi Sjuman were chosen to do the scoring, which they spent a month and a half on; they later said that work on the film brought them closer together.  Shanty Harmayn, who had previously worked on Pasir Berbisik (Whispering Sands; 2004), was chosen to produce, while Salman Aristo, known for his scripts for Ayat-Ayat Cinta (Verses of Love; 2008) and Laskar Pelangi (Rainbow Warriors; 2009) spearheaded the writing.  The screenplay went through twelve drafts and took two years of research. 

Sang Penari is based on the Ronggeng Dukuh Paruk (Ronggeng of Paruk Hamlet) by Ahmad Tohari;  it is the second adaptation of the work, after Darah dan Mahkota Ronggeng (Blood and Crown of a Dancer), directed by Yazman Yazid and starring Ray Sahetapy and Enny Beatrice, in 1983.  The film was shot mostly in Banyumas, Central Java.  Director Ifa Isfansyah attempted to cast the lead role of Srintil there, but failed after several months looking. 
 Balinese actor Nyoman Oka Antara, who had previously played in Ayat-Ayat Cinta (Verses of Love; 2008) and Perempuan Berkalung Sorban (The Girl With the Keffiyeh Around Her Neck; 2009), was cast in the leading male role.  The film also featured Slamet Rahardjo, Dewi Irawan, Hendro Djarot, Tio Pakusadewo, Lukman Sardi, and Teuku Rifnu Wikana in supporting roles; Happy Salma also had a cameo as a dancer. 

==Style and themes== New Order government would have had him shot. 

Sang Penari features many spoken lines in the Banyumasan language spoken in the area.  It also features several aspects of Indonesian culture, including batik  and Javanese music. 

==Release and reception==
Sang Penari was released on 10 November 2011.  Tohari, who had refused to watch the first adaptation, enjoyed Sang Penari and reportedly considered it a "sublime adaptation of his work".  Triwik Kurniasari, writing for The Jakarta Post, described the film as "artistically stunning" and that Isfansyah "smoothly translates the sinister moment and the vicious attempts taken by the military in handling possible traitors".  Labodalih Sembiring, writing for the Jakarta Globe, said the socio-cultural elements in the film were worthy of a Shakespearean tragedy and that it featured good acting and direction; however, the films soundtrack was considered lacking. 

==Awards== Indonesian entry Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist. 

{| class="wikitable" style="font-size: 95%;"
|-
! scope="col" | Award Year
! scope="col" | Category
! scope="col" | Recipient
! scope="col" | Result
|-
! scope="row" rowspan="9"  | Indonesian Film Festival
| rowspan="9" | 2011 Citra Award Best Picture 
|  
| 
|- Best Director 
| Ifa Isfansyah
|  
|-
| Best Screenplay 
| Salman Aristo, Ifa Isfansyah, and Shanty Harmayn
|  
|-
| Best Cinematography 
| Yadi Sugandi
|  
|-
| Best Artistic Direction 
| Eros Eflin
|  
|- Best Leading Actor 
| Nyoman Oka Antara
|  
|- Best Leading Actress 
| Prisia Nasution
|  
|-
| Best Supporting Actor 
| Hendro Djarot
|  
|- Best Supporting Actress 
| Dewi Irawan
|  
|-
! scope="row" | Academy Award 2012
|Academy Best Foreign Language Film 
|  
| 
|-
|}

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Indonesian submissions for the Academy Award for Best Foreign Language Film

==References==
;Footnotes
 

;Bibliography
 
*{{cite news
  |last=
  |first=
  |ref=  title = Film brings Titi closer to husband
  |trans_title= url =http://www.thejakartapost.com/news/2011/11/04/film-brings-titi-closer-husband.html
  |work=The Jakarta Post location = Jakarta
  |accessdate=24 December 2011 date = 4 November 2011
  |archivedate=24 December 2011
  |archiveurl=http://www.webcitation.org/64A0xrrwb
}}
* {{cite news
 | work=The Jakarta Post
 | title = Ahmad Tohari: The return of the people’s writer
 | trans_title = 
 | language = 
 | url =http://www.thejakartapost.com/news/2011/11/03/ahmad-tohari-the-return-people-s-writer.html
 | last=Krismantari
 | first=Ika
 | date =3 November 2011
 | accessdate=12 January 2012
 | archivedate =12 January 2012
 | archiveurl =  http://www.webcitation.org/64dAu5NwZ
 | ref = 
}}
* {{cite news
 | work=The Jakarta Post
 | title = A vibrant year for the film industry
 | trans_title = 
 | language = 
 | url =http://www.thejakartapost.com/news/2011/12/18/a-vibrant-year-film-industry.html
 | last=Kurniasari
 | first=Triwik
 | date =18 December 2011
 | accessdate=12 January 2012
 | archivedate =12 January 2012
 | archiveurl =  http://www.webcitation.org/64dAu5NwZ
 | ref = 
}}
* {{cite news
 | work=The Jakarta Post
 | title = Ronggeng 2.0
 | trans_title = 
 | language = 
 | url =http://www.thejakartapost.com/news/2011/11/13/ronggeng-20.html
 | last=Kurniasari
 | first=Triwik
 | date =13 November 2011
 | accessdate=12 January 2012
 | archivedate =12 January 2012
 | archiveurl =  http://www.webcitation.org/64dCwoIVF
 | ref = 
}}
*{{cite news
 |url=http://www.variety.com/article/VR1118059731
 |ref= 
 |archiveurl=http://www.webcitation.org/6D6Brti3K
 |archivedate=22 December 2012
 |title=Indonesia introduces Oscar to Dancer
 |accessdate=24 September 2012
 |date=24 September 2012
 |work=Variety
 |publisher=Reed Business Information
 |last=de Leon
 |first=Sunshine Lichauco
}}
* {{cite news
 | work=The Jakarta Post
 | title = Ifa Isfansyah: The man behind ‘Sang Penari’
 | trans_title = 
 | language = 
 | url =http://www.thejakartapost.com/news/2011/11/22/ifa-isfansyah-the-man-behind-sang-penari.html
 | last=Maryono
 | first=Agus
 | date =22 November 2011
 | accessdate=12 January 2012
 | archivedate =12 January 2012
 | archiveurl =  http://www.webcitation.org/64dCUuXFk
 | ref = 
}}
* {{cite news
 | work=Jakarta Globe
 | title = Gripping Drama Shines Light on Indonesian Dark Past
 | trans_title = 
 | language = 
 | url = http://www.thejakartaglobe.com/lifeandtimes/gripping-drama-shines-light-on-indonesian-dark-past/479888
 | last=Sembiring
 | first=Lebodalih
 | date =21 November 2011
 | accessdate = 12 January 2012
 | archivedate = 12 January 2012
 | archiveurl = http://www.webcitation.org/64dMYuzL7
 | ref = 
}}
* {{cite news
 | work=Jakarta Globe
 | title = Film: Dancing Through Indonesia’s Dark Days
 | trans_title = 
 | language = 
 | url =http://www.thejakartaglobe.com/lifeandtimes/film-dancing-through-indonesias-dark-days/477138
 | last=Siregar
 | first=Lisa
 | date =8 November 2011
 | accessdate=12 January 2012
 | archivedate =12 January 2012
 | archiveurl =  http://www.webcitation.org/64dFwktdb
 | ref = 
}}
* {{cite news
 | work=Kompas
 | title = Oka Antara Bersaing dengan Tio Pakusadewo
 | trans_title = Oka Antara Against Tio Pakusadewo
 | language = Indonesian
 | url = http://entertainment.kompas.com/read/2011/12/01/15275286/Oka.Antara.Bersaing.dengan.Tio.Pakusadewo
 | last=Sofyan
 | first=Eko Hendrawan
 | date =1 December 2011
 | accessdate=18 March 2012
 | archivedate =18 March 2012
 | archiveurl =  http://www.webcitation.org/66FaxjtQ5
 | ref = 
}}
* {{cite news
 | work=Kompas
 | title = "Sang Penari", Potret Buram Pasca Tragedi
 | trans_title = "Sang Penari", Blurred Portrait after the Tragedy
 | language = Indonesian
 | url = http://oase.kompas.com/read/2011/11/05/21104215/.Sang.Penari.Potret.Buram.Pasca.Tragedi
 | last=Subagyo
 | first=Rz.
 | date =5 November 2011
 | accessdate=12 January 2012
 | archivedate =12 January 2012
 | archiveurl =  http://www.webcitation.org/64dOQnlDV
 | ref = 
}}
 

==External links==
* 

 
 

 
 
 
 
 
 
 