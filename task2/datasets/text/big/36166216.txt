L'iguana dalla lingua di fuoco
{{Infobox film
| name           = The Iguana with the Tongue of Fire
| image          = Liguana dalla lingua di fuoco poster.jpg
| alt            = 
| director       = Riccardo Freda  (as Willy Pareto) 
| writer         = André Tranché
| screenplay     = Sandro Continenza  (as Alessandro Continenza)  Riccardo Freda  (as Willy Pareto) 
| based on       = the novel A Room Without a Door by Richard Mann
| starring       = Luigi Pistilli Dagmar Lassander 
| music          = Stelvio Cipriani
| cinematography = Silvano Ippoliti
| editing        = Riccardo Freda  (as Willy Pareto) 
| studio         = Les Films Corona Oceania Produzioni Internazionali Cinematografiche Terra-Filmkunst
| released       = 24 August 1971
| runtime        = 92 min.
| country        = Italy France West Germany
| language       = Italian
}}

Liguana dalla lingua di fuoco (internationally released as The Iguana with the Tongue of Fire) is a 1971 Italian giallo film. It is directed by Riccardo Freda, who was unhappy with the film and had his name replaced with the pseudonym "Willy Pareto".    The film is loosely based on the novel A Room Without a Door written by Richard Mann. 

== Cast ==
* Luigi Pistilli as Detective John Norton
* Dagmar Lassander as Helen Sobiesky 
* Anton Diffring as  Ambassador Sobieski
* Arthur OSullivan as  insp. Lawrence 
* Werner Pochath as  Marc Sobiesky 
* Dominique Boschero as  Ambassadors mistress
* Valentina Cortese as  Mrs. Sobiesky

== Critical reception ==
 AllMovie wrote, "Stylish director Riccardo Freda gave Italy some of its greatest horror films. This gory but preposterous giallo thriller is not one of them", calling it "one of a great directors most blatant misfires." 

== References ==
 

== External links ==

*  
 
 
 
 
 
 

 


 