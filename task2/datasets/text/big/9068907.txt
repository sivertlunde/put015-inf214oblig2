Brother, Can You Spare a Dime? (film)
{{Infobox film
| name = Brother, Can You Spare a Dime?
| image = BrotherSpareDime.jpg
| image_size =
| caption =
| director = Philippe Mora
| producer = David Puttnam Sandy Lieberson
| writer = Philippe Mora
| narrator =
| music =
| cinematography = Jeremy Thomas
| starring = Walt Disney Bing Crosby Charlie Chaplin Andrews Sisters Fred Astaire Shirley Temple Eleanor Roosevelt Franklin Delano Roosevelt
| studio = Goodtimes Enterprises Visual Programme Dimension Pictures (US)
| released =  
| runtime = 110 min.
| country = United Kingdom
| language = English
| budget =
}}

Brother, Can You Spare a Dime? is a 1975 documentary film produced by Image Entertainment. It consisted largely of newsreel footage and contemporary film clips to portray the era of the Great Depression.

==Cast==
{{columns col1 = 
* Marlene Dietrich
* John Dillinger
* Walt Disney James Dunn
* Cliff Edwards
* Dwight D. Eisenhower Bill Elliot
* Madge Evans
* Shirley Temple
* Stepin Fetchit
* W. C. Fields
* Dick Foran
* Gerald Ford
* Clark Gable
* Benny Goodman
* Cary Grant
* Woody Guthrie
* Gabby Hayes
* Billie Holiday
* Herbert Hoover
* J. Edgar Hoover
 col2 = 
* George Burns
* The Andrews Sisters
* Fred Astaire
* Warner Baxter
* Jack Benny
* Busby Berkeley
* Willie Best
* Humphrey Bogart
* James Cagney
* Cab Calloway
* Eddie Cantor
* Hobart Cavanaugh
* George Chandler
* Charlie Chaplin
* Winston Churchill
* Betty Compson
* Gary Cooper
* Bing Crosby
* Frankie Darro
* Cecil B. DeMille
}}

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 
 