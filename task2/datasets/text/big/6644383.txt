Dixie Chicks: Shut Up and Sing
 
{{Infobox film
| name           = Dixie Chicks: Shut Up and Sing 
| image          = DixieChicks-PressConference.jpg
| caption        = Shut Up and Sing Press Conference Upon Release of Documentary 
| director       = Barbara Kopple Cecilia Peck 
| producer       = Barbara Kopple Cecilia Peck David Cassidy Claude Davies (co-producer) David Becker  (associate producer)  Kelly Brennan  (associate producer)  Craig Hymson  (associate producer)  Daniel Voll  (consulting producer) 
| writer         =
| starring       = Martie Maguire Natalie Maines Emily Robison Adrian Pasdar Rick Rubin George W. Bush  (footage)  Simon Renshaw Gareth Maguire
| music          = Dixie Chicks
| cinematography = Tamara Goldsworthy Chris Burrill Joan Churchill Seth Gordon
| editing        = Bob Eisenhardt Aaron Kuhn Emma Morris Jean Tsien Michael Culyba  (Co-Editor) 
| distributor    = The Weinstein Company
| released       =      
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
}}
Dixie Chicks: Shut Up and Sing (also known simply as Shut Up and Sing) is a 2006 documentary film produced and directed by  director Barbara Kopple and Cecilia Peck (daughter of actor Gregory Peck). Singer, Leigh   

The film follows the Dixie Chicks, an all-woman Texas-based country music trio, over a three-year period of intense public scrutiny, fan backlash, physical threats, and pressure from both corporate and conservative political elements in the United States after lead singer Natalie Maines publicly criticized then President of the United States George W. Bush during a live 2003 concert in London as part of their Top of the World Tour.

==Synopsis==
 Dixie Chicks single from that album "Travelin Soldier", a sensitive depiction of a soldiers life during the Vietnam War era, and the young woman who waited for him, finding he was killed in battle, had peaked at #1 on the US Billboard Hot Country Songs Chart.
 George W. Bushs authorization of the invasion of Iraq.  Approximately 1 million people had recently demonstrated in London against the impending war.  During the introduction to their song "Travelin Soldier", Natalie Maines, a Texas native, says:
 
  and a firestorm of anger and criticism followed.

The film shows the bands reaction to the open hostility, political and corporate backlash, and physical threats directed at the group. The band did not expect such a strong reaction, and they are unsure if they should "shut up and sing", apologize, or stand by their convictions and let more sparks fly.

The film follows the day-to-day life of the Chicks.  It shows them with their husbands and their children, at home in Texas and in the recording studio in Los Angeles, getting their hair and makeup done before appearances, exchanging ribald remarks with each other, writing song lyrics and working on musical arrangements.  Simon Renshaw, the groups longtime manager, is the focus of many scenes as he attempts to guide the Chicks through the vicissitudes of the music industry.
 Texas Rangers that they advised the Chicks to cancel a concert in Dallas, Texas, and they were shown the original letter that specified a date, time, and location at which lead singer Natalie Maines would be shot dead, unless she "shut up and sang".  However, the show took place without incident.  Living in a constant state of fear took an emotional toll on the Chicks, in particular because they toured with their children. Cohn, Angel   

The song, "Not Ready to Make Nice" includes a reference to that very real death threat:

 
And how in the world can the words that I said  
Send somebody so over the edge   
That theyd write me a letter  
Saying that I better   
shut up and sing or my life will be over?! 
 

Commentator  .
The tagline of the film, "freedom of speech is fine as long as you dont do it in public", is a reference to a scene in which an interviewed protester says "freedom of speech is fine but by God you dont do it outside of the country and you dont do it in mass publicly".

==Release dates== world premiere)
 San Sebastián International Film Festival)

*  September 30, 2006 (Aspen Film Festival)

*  October 15, 2006 (Woodstock Film Festival)

*  October 17, 2006 (Rome Film Fest)

*  October 19, 2006 (Austin Film Festival)

*  October 25, 2006 (London Film Festival)

*  October 27, 2006 (limited release)

*  November 10, 2006 (wide release)

*  March 16, 2007 (Thessaloniki Documentaries Festival)

*  March 29, 2007

*  April 26, 2007 (limited)

*  June 15, 2007 (Sydney Film Festival)

*  June 29, 2007

*  August 9, 2007

*  August 11, 2007 (Jecheon International Music & Film Festival)

*  August 15, 2007

*  October 3, 2007

==Television advertisements==
In October 2006, the films distributor, The Weinstein Company, announced that NBC had refused to air the TV advertisements for the film, stating that it was following a "policy of not broadcasting ads that deal with issues of public controversy". NBC publicly acknowledged the decision but claimed that it was willing to work with Weinstein to find an acceptable alternative.  At the same time, the distributor also claimed that The CW had refused to air these advertisements, citing "concerns we do not have appropriate programming in which to schedule this spot". That network later said its statement was merely an opinion on whether its target audience would respond to the ad, and that it would have accepted the ads if Weinstein had actually bought commercial time. 
 ABC and Fox Network|Fox.  It is not clear which decision either network ultimately made. However, individual stations affiliated with all five networks, including some owned by NBC, aired the ad during local ad time.

==Reception==

===Box office===
The film opened in New York City and Los Angeles on October 27, 2006 in only 4 theatres. In its first week it grossed an average of US dollar|US$50,103. In its sixth week (38 days after its original release) the film expanded to its widest release, being shown at 84 theaters.

*Total domestic gross: US$1,215,045 (Estimate). 

===Reviews===
Critical reaction to the film has been extremely positive. At the time of its release, it had a 93% rating at the Rotten Tomatoes. As of August 5, 2014, it had an 88% rating, being "certified fresh." 
 Ebert & Roeper. 

===Awards and nominations===
*Aspen Film Festival:
**Audience Award - Favorite Documentary Boston Film Critics:
**Best Documentary Feature Broadcast Film Critics:
**Best Documentary Feature
**Best Song ("The Neighbor")
*Chicago International Film Festival:
**Special Jury Prize Online Film Critics:
**Best Documentary Feature San Diego Film Critics:
**Best Non-Fiction Film Southeastern Film Critics:
**Wyatt Award for the film that best captures the "spirit of the South" (won)
*Woodstock Film Festival:
**Audience Award - Best Documentary Feature
*Sydney Film Festival:
**Audience Award - Documentary Category

==Poster==
 
 
The theatrical poster of the film borrowed a picture of the band from a memorable Entertainment Weekly cover, in which they are featured in the nude, with their privates cleverly hidden. On the U.S. version of the poster, however, the "nudity" was edited out and towels are seen over their nude bodies. The writings, which were originally on their bodies, were transferred to the towels. The Canadian poster used the original photo, with no towels added. 

The original photograph included the words "Dixie Sluts" but for the promotional poster, a more demure "Dixie Bimbos" replaced the message on Emily Robisons arm.

==Rating== R rating from the Motion Picture Association of America for strong language. 

==DVD release==
The DVD of the film was released on February 20, 2007,  and the UK in September 2007. On September 9, 2007, it ranked at #2 in the UK Top 20 DVD Chart.

==References==
 

==External links==
 
 
*  
* 
* 
* 
*  at the Rotten Tomatoes

 

 
 
 
 
 
 
 
 
 
 
 
 