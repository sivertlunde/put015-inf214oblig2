Valar Pirai
{{Infobox film
| name           = Valar Pirai
| image          =
| image_size     =
| caption        =
| director       = Dasari Yoganand|D. Yoganand
| producer       = Padma Films
| writer         = Javar Seetharaman
| screenplay     = Javar Seetharaman
| story          = A. S. Nagarajan
| starring       = Sivaji Ganesan B. Saroja Devi M. R. Radha K. A. Thangavelu
| music          = K. V. Mahadevan
| cinematography = W. R. Subba Rao
| editing        =
| studio         = Padma Films
| distributor    = Padma Films
| released       =   
| country        = India Tamil
}}
 1962 Cinema Indian Tamil Tamil film,  directed by Dasari Yoganand|D. Yoganand. The film stars Sivaji Ganesan, B. Saroja Devi, M. R. Radha and K. A. Thangavelu in lead roles. The film had musical score by K. V. Mahadevan.  

==Cast==
*Sivaji Ganesan
*B. Saroja Devi
*M. R. Radha
*K. A. Thangavelu
*T. S. Balaiah
*M. V. Rajamma
*M. Saroja

==Soundtrack==
The music composed by K. V. Mahadevan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics
|- Kannadasan
|-
| 2 || Koondu Thirandhamma || T. M. Soundararajan
|-
| 3 || Mounam Mounam || P. Susheela
|-
| 4 || Naangu Suvarkalukkul || P. Susheela
|-
| 5 || Pachchaikodiyil || P. Susheela
|-
| 6 || Pojjiyathukkule Oru || T. M. Soundararajan
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 


 