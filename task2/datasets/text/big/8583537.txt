At the Edge of the Law
{{Infobox film
| name = Al filo de la ley
| image = 
| caption =
| director = Juan Carlos Desanzo
| producer = Juan Carlos Desanzo
| writer = Juan Carlos Desanzo   José Pablo Feinmann
| starring = Rodolfo Ranni  Gerardo Romano
| music = Emilio Kauderer
| cinematography = Yito Blanc
| editing = Norberto Rapado
| distributor = Argentina Video Home
| released = 7 May 1992
| runtime = 90 minutes
| country = Argentina
| language = Spanish
| budget =
| followed_by =
}} Argentine action action thriller thriller film directed and written by Juan Carlos Desanzo. The film starred Rodolfo Ranni and Gerardo Romano.

==Synopsis==
A pair of swindlers robs a hotel in Miami. Both are persecuted by the one in charge of security, and they take revenge.

==Cast==
*Rodolfo Ranni as Rodolfo Rivas
*Gerardo Romano as Raúl Fontana
*Katja Alemán as Mónica Ferraro
*Ulises Dumont as Gauna
*Jorge Sassi as Oficial Sánchez
*Vando Villamil as Beany
*Marcos Woinsky as Gerente
*Theodore McNabney
*Enrique Mazza
*Daniel Ripari

==Release and acclaim==
The film premiered on 7 May 1992.

==External links==
*  

 
 
 
 
 
 

 
 