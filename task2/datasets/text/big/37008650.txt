Very Good Girls
{{Infobox film
| name           = Very Good Girls
| image          = Very Good Girls.jpg
| size           = 220 
| caption        = Official movie poster
| director       = Naomi Foner
| producer       = Norton Herrick Michael London Mary Jane Skalski
| writer         = Naomi Foner
| starring       = Dakota Fanning Elizabeth Olsen Boyd Holbrook Demi Moore Richard Dreyfuss Ellen Barkin Peter Sarsgaard Clark Gregg
| music          = Jenny Lewis 
| cinematography = Bobby Bukowski
| editing        = Andrew Hafitz Dylan Tichenor
| studio         = Groundswell Productions Herrick Productions
| distributor    = Tribeca Film MG Film
| released       = June 24, 2014
| runtime        = 91 minutes  
| country        = U.S.
| language       = English
| budget         = 
| gross          = $6,940  
}}

Very Good Girls is the first feature directed by American screenwriter Naomi Foner, whose script for drama Running on Empty was Oscar-nominated. First screened publicly in early 2013, the coming-of-age drama stars Dakota Fanning and Elizabeth Olsen as two friends who fall for the same man (Boyd Holbrook). The film premiered at the Sundance Film Festival on January 22, 2013; it was given release on home formats on June 24, 2014.

The supporting cast includes Demi Moore, Richard Dreyfuss, Ellen Barkin, Clark Gregg, and Peter Sarsgaard. The film was produced by Norton Herrick, Michael London, and Mary Jane Skalski.

==Synopsis==
 
Best friends Lily (Fanning) and Gerry (Olsen), home for one last New York summer, make a pact to lose their virginity before leaving for college. But when they both fall for the same handsome artist (Holbrook) and Lily starts seeing him in secret, a lifelong friendship is tested.   

==Cast==
{{Quote box| quote  = The film deals with female sexuality and friendship in a way we haven’t seen before. These girls will be stunning young women in a couple of years, but they’ve struggled through high school with only each other. Most of us have been there. This is the summer where they finally get to touch real life. source = —Naomi Foner  width  = 300px
}}

 
* Dakota Fanning as Lilly Berger
* Elizabeth Olsen as Gerry
* Boyd Holbrook as David Avery
* Demi Moore as Kate, Gerrys Mother
* Richard Dreyfuss as Danny, Gerrys father
* Ellen Barkin as Norma, Lillys mother
* Clark Gregg as Edward, Lillys father
* Kiernan Shipka as Eleanor, Lillys sister
* Peter Sarsgaard as Fitzsimmons, Lillys summertime job boss

==Production==
Production company Herrick Entertainment announced on July 20, 2012, that principal photography had begun in New York City.    Herrick was producing and financing the project, with Michael Londons Groundswell Productions also producing. 

Shooting locations included Ditmas Park, Brooklyn. 

Cast-member Peter Sarsgaard is the son-in-law of writer-director Naomi Foner, who is the mother of his wife, Maggie Gyllenhaal.

In January 2012, Anton Yelchin was in final negotiations to play a part in the film, but he is not listed in the final cast.   

==Release==
The film was released on June 6, 2014 through iTunes, Amazon Instant Video, Vudu, and Video on demand|VOD. The film started a limited theatrical release in the United States on July 25, 2014. 

==Reception==
The film received negative reviews from critics. It has accumulated a 19% rotten rating on Rotten Tomatoes based on 31 reviews. The Hollywood Reporter described the film as "a limp directing debut" for Foner. The films outdated "counter-culture" sensibilities made it appear "frozen in time". It found the characters unconvincing, criticizing the "artificiality" of the love interest who asks the lead character to "read a couple of lines of Sylvia Plath before their first kiss".  We Got This Covered called the film "insincere from bottom to top," expressing disappointment that "its innate lousiness" deflated the dramatic efforts of its leads.  Variety criticized the "really bad" drama, particularly the "vapid and cliched" screenplay. "This all-around misfire ... may go directly to home formats." 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 