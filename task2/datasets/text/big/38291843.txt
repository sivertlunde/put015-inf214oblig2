Ranbhool
 
{{Infobox film
| name           = Ranbhool
| image          = Ranbhool Marathi Movie.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Sanjay Surkar
| producer       = Pratibha Mendhekar
| screenplay     = 
| story          = 
| starring       = Subodh Bhave Tejaswini Pandit Mohan Joshi Vinay Apte
| music          = Narendra Bhide
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Ranbhool is a Marathi movie released on 14 May 2010.The movie has been produced by Pratibha Mendhekar and directed by Sanjay Surkar. The plot of the movie is about a deranged killer whose traumatic childhood and obsession with music drives him down the path of psychosis.

== Synopsis ==
Lokesh, an innocent kid who loves playing his guitar, is turned into a psychotic killer thanks to a traumatic childhood experience, courtesy his ruthless father. Lokesh’s passion for playing the guitar turns into an obsession for making disturbing tunes for spreading a message to the world, a message by God, and Lokesh believes himself to be God’s Messenger. Whoever is unfortunate enough not to understand his music and his message doesn’t deserve the right to live.

Reva, a lonely young girl, torn apart by her parent’s separation, feels the pain and the passion behind Lokesh’s music, but oblivious to his psychotic killing spree, she puts herself, her friend and her kid sister, in danger of Lokesh’s wrath!

== Cast ==

The cast includes Subodh Bhave, Tejaswini Pandit, Mohan Joshi, Vinay Apte & Others.

==Soundtrack==
The music is provided by Narendra Bhide.

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 


 