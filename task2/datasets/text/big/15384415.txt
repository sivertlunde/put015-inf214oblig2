Christmas Is Here Again
{{Infobox film
| name           = Christmas Is Here Again
| image = Christmas-is-Here-Again.jpg
| director       = Robert Zappia
| producer       = Robert Zappia Jim Praytor
| writer         = Robert Zappia Marco Zappia
| narrator       = Jay Leno
| starring       = Edward Asner Brad Garrett Kathy Bates Andy Griffith Norm Macdonald Madison Davenport
| music          = John Van Tongeren
| studio         = Renegade Animation
| distributor    = Screen Media Films
| released       =   }}
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
}} animated Christmas film released on DVD by Screen Media Films.  The first feature production from the Renegade Animation studio,    it was co-written, co-produced and directed by Robert Zappia.  Narrated by Jay Leno, the film features the voices of Edward Asner, Kathy Bates, Madison Davenport, Colin Ford, Brad Garrett, Andy Griffith, Shirley Jones, Norm Macdonald, and Daniel Roebuck, and marked Asners second role in a Christmas-themed film after the 2003 film Elf (film)|Elf.

==Synopsis==
Sophianna, a disabled orphan girl, sets out to find Santas toy sack, (which is a magical source of toys since it was made from the baby Jesus swaddling clothes) which is stolen by Krad  who wants revenge after Santa stopped handing out coal to naughty children. She is helped in her quest by Paul Rocco, one of Santas elves, Dart, a reindeer calf, Buster the fox, and his friend, Charlie the polar bear.

==Cast==
*Madison Davenport as Sophianna
*Andy Griffith as Santa Claus
*Shirley Jones as Mrs. Victoria Claus
*Daniel Roebuck as Paul Rocco / Jacque
*Brad Garrett as Charlie
*Norm Macdonald as Buster
*Colin Ford as Dart
*Ed Asner as Krad
*Kathy Bates as Miss Dowdy
*Jay Leno as the Narrator

==Production==
Renegade Animation, an animation company located in Glendale, California and known for the TV series Hi Hi Puffy AmiYumi and The Mr. Men Show, teamed up with Easy to Dream Entertainment to create Christmas Is Here Again. A small crew spent nine months on the principal animation, which was completed in mid-2006. 

One of the songs in the films soundtrack (as well as the films working title) was "Who Stole Santas Sack?"

Co-producer Darrell Van Citters, a Renegade staff member, once directed a Disney short called Fun with Mr. Future (1982).

==Awards and release==
Christmas Is Here Again premiered on October 20, 2007, as an official selection at the Heartland Film Festival.     The following year, it received an Annie Award nomination for Best Voice Acting in an Animated Television Production (Madison Davenport as Sophianna).  Additionally, in 2009, Christmas is Here Again was nominated for the Annie Award for Best Animated Home Entertainment Production. Colin Ford, the voice of Dart, was also nominated for a Young Artist Award in 2009 for Best Performance in a Voice-Over Role - Young Actor.

The film was originally slated for a theatrical release in November 2007,  but ultimately received its DVD debut in the U.S. on November 4, 2008. The disc contains a behind-the-scenes featurette, cast interviews and "Name the Reindeer", as extras. 

Originally, the reindeer on the DVD cover had a red nose akin to Rudolph the Red-Nosed Reindeer|Rudolphs.  On the final version of the cover, it is black.

==Reception==
Richard Propes of The Independent Critic website gave it an A and 3.5 stars, calling it "an ideal choice for families, children and for Scrooges like myself who, somewhere deep inside, still want to believe". 

==Stage version== Pacific Conservatory Theatre presented the premiere of a stage adaptation of Christmas Is Here Again by Brad Carroll and Jeremy Mann in November and December 2014. 

==See also==
*List of animated feature-length films

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 