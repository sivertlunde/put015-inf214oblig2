How to Make a Monster (1958 film)
{{Infobox film
| name           = How to Make a Monster
| image          = Howtomakeamonster.jpg
| director       = Herbert L. Strock
| writer         = Herman Cohen Aben Kandel
| starring       = Robert H. Harris Gary Conway Gary Clarke Morris Ankrum
| producer       = Herman Cohen James H. Nicholson
| music          = Paul Dunlap
| cinematography = Maury Gertsman
| distributor    = American International Pictures
| released       =  
| runtime        = 73 minutes
| language       = English
| country        = United States
}}
How to Make a Monster is a 1958 American horror film released by American International Pictures as a double feature with Teenage Cave Man.  The film is a Fictional crossover|follow-up to both I Was a Teenage Werewolf and I Was a Teenage Frankenstein. Like Teenage Frankenstein, a black & white film that switched to color for the final moments, How to Make a Monster was filmed in black & white, with the entire last reel filmed in color.

==Plot==
Pete Dumond, Chief Make-up Artist for 25 years at "American International Studios," is fired after the studio is purchased by "NBN Associates" and the new management from the East, Jeffrey Clayton and John Nixon, plan to make musicals and comedies instead of the horror pictures for which Pete has created his remarkable monster make-ups and made the studio famous. In retaliaton, Pete vows to use the very monsters these men have rejected to destroy them. By mixing a numbing ingredient into his foundation cream and persuading the young actors that their careers are through unless they place themselves in his power, he hypnotizes both Larry Drake and Tony Mantell, who are playing the characters Teenage Werewolf and Teenage Frankenstein in the picture Werewolf Meets Frankenstein currently shooting on the lot.

Through hypnosis, Pete causes Larry in werewolf make-up to kill Nixon in the studio projection room, and later he wills the unknowing Tony to wait for Clayton in his garage at night and brutally choke him to death. Studio guard Monahan, a self-styled detective, stops in at the Make-up Room on his rounds one evening and shows Pete and Rivero—Petes reluctant assistant and accomplice—his little black book in which he has jotted down many facts such as the late time Pete and Rivero checked out the night of the first murder. By this show of initiative he plans to get a promotion. Apprehensive, Pete, made up as a terrifying primitive monster, one of his own creations, kills Monahan in the studio commissary at a later point on his beat.

Richards, the older guard sees and hears nothing until he uncovers Monahans body. Police investigators uncover two clues: a maid, Millie, describes the Monster Frankenstein (Tony, in make-up) who struck her down as he fled from Claytons murder, and the Police Laboratory Technician discovers a peculiar ingredient in the make-up left on Claytons fingers from his death struggle with Tony. The formula matches bits found in Petes old Make-up Room, and the police head for Petes house—where Pete has taken Rivero, Tony and Larry for a grim farewell party to his home which is a museum of all the monsters that he has created in the 25 years in the studio. (As Pete lights the candles for the party, the movie switches from black and white to color.) Pete has stabbed Rivero to death secretly in the kitchen and hidden his body. Finding Tony and Larry trying to escape the locked living room, he attacks them with a knife, but Larry knocks over a candelabra, setting the living room on fire and Pete is burned to death trying to save the lifelike heads of his monster "children" mounted on the wall. The police break through the door before the flames reach the boys.

==Production notes==
Many of Pete Dumonds "children" destroyed in the fire were props originally created by Paul Blaisdell for earlier AIP films. They include The She-Creature (1956), It Conquered the World (1956), Invasion of the Saucer Men (1956) and Attack of the Puppet People (1958). Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p94 

Herman Cohen says he cast John Ashley as a singer at the request of James H. Nicholson, who had put Ashley under long-term contract with the studio. Ashley was having some minor success as a recording artist at the time. 
 Ziv Studios. A sign was put up calling it "American International Pictures". 

==Legacy== TV movie 2004 album by The Cramps; for a documentary on special make-up effects applications in 2005; and for an 8-minute short film in 2011.

==References==
 
 
 

==External links==
*  

 
 
 
 
 
 
 
 