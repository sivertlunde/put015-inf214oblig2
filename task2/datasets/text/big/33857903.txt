Mickey's Good Deed
 
{{Infobox Hollywood cartoon
| cartoon name      = Mickeys Good Deed
| series            = Mickey Mouse
| image             = Mickeys Good Deed.jpg
| image size        = 
| alt               = 
| caption           = Theatrical release poster
| director          = Burt Gillett
| producer          = Joseph Schenck
| story artist      = 
| narrator          = 
| voice actor       = Pinto Colvig Walt Disney
| musician          =  Norm Ferguson, Dick Lundy, Tom Palmer, Ben Sharpsteen   at The Encyclopedia of Disney Animated Shorts 
| layout artist     = 
| background artist =  Walt Disney Productions
| distributor       = United Artists
| release date      =   (USA)
| color process     = Black-and-white
| runtime           = 8 minutes
| country           = United States
| language          = English
| preceded by       = The Wayward Canary
| followed by       = Building a Building
}} Walt Disney Christmas season and the contemporary Great Depression, the cartoon centers on Mickeys act of charity to bring Christmas to a poor family. The film was directed by Burt Gillett and features the voices of Walt Disney as Mickey and Pinto Colvig as Pluto (Disney)|Pluto. 

Mickeys Good Deed was Mickeys second Christmas themed film after Mickeys Orphans (1931).

==Plot== street performer Pluto howls along. Several passers-by appear to throw coins in Mickeys collection cup and Mickey wishes them a merry Christmas. But when Mickey goes to buy food, he discovers his cup is full of nails, nuts, and bolts. Mickey walks away disgusted.

Eventually Mickey comes to the home of a rich family and begins to play outside. Inside the home, a crying child (named Adelbert) keeps throwing away toys given to him by his father and a butler. When Adelbert hears Pluto outside, he demands his father buy the dog. The butler goes outside and persistently offers Mickey money for Pluto, but Mickey refuses. As he runs away, Mickey drops his cello and a horse-drawn sleigh runs it over destroying it. The apparently oblivious party in the sleigh call out a cheerful "Merry Christmas!" to Mickey.

Mickey and Pluto later come across the home of a poor cat family. A mother sits at the table crying and Mickey and Pluto see that the father, Pete (Disney)|Pete, is in jail and she has no money for food or toys. Emotionally moved by the scene, Mickey returns to the rich home and reluctantly sells Pluto. With the money, Mickey buys toys and food for the cat family and their mom, who is now snoring as he makes it back to their house and he delivers the goods dressed as Santa Claus. He barely manages to keep it quiet until he has snuck out of the house, just in time to see the children wake up to celebrate Christmas. Mickey is then satisfied that he helped the cat family have a happy Christmas, but he still misses Pluto.

Meanwhile back at the rich home, Adelbert is tormenting Pluto and tying objects, including a roasted chicken onto Plutos tail. The frustrated father finally has the butler throw the dog out, while he gives the misbehaving Adelbert a much deserved spanking. Pluto then follows Mickeys tracks to where he finds the lonely mouse sitting in front of a fire along with a snow sculpture of Pluto. Pluto burrows through the snow and pops his head out the top of the sculpture surprising Mickey. The two of them share the chicken which Adelbert had tied to Plutos tail. Reunited, Mickey happily wishes merry Christmas to Pluto.

==Releases==
*1932 &ndash; Original theatrical release Jiminy Crickets Christmas" (VHS) 
*2004 &ndash; " " (DVD) Holiday Celebration with Mickey & Pals" (colorized, DVD) 

==Notes==
 

==External links==
* 
*  at the Disney Film Project
*  at Animation Backgrounds
*  reviewed by Dennis Grunes

 
 

 
 
 
 
 
 