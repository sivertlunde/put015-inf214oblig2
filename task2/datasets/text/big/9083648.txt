Raja Paarvai
{{Infobox film
| name           = Raja Paarvai
| image          = Raaja Paarvai.jpg
| caption         = Promotional Poster
| director       = Singeetam Srinivasa Rao
| writer         = Ananthu Kamal Haasan Balakumaran Santhana Bharathi Madhavi Y. Gee. Mahendra L.V. Prasad  KPAC Lalitha
| cinematography = Barun Mukherjee
| editing        = V. R. Kottagiri
| producer       = Chandra Haasan Charuhasan Kamal Haasan
| music          =Ilaiyaraaja
| studio         = Haasan Brothers
| distributor    = Haasan Brothers
| released       = 10 April 1981
| runtime        = 144 min Tamil
}} directed by Singeetham Srinivasa Rao. The story was written by Kamal Haasan, for whom the film was also his 100th starrer and first production. The score and soundtrack was composed by Ilaiyaraaja. The film was simultaneously made and released as Amavasya Chandrudu in Telugu.

==Plot==
This drama tells the tale of a blind violinist, Ragu (Kamal Haasan), and  a young lady, Nancy (Madhavi (actress)|Madhavi) who is keen on chronicling Ragus inspiring life as a visually impaired but independently living person. Their relationship blossoms into a romance that is supported by Nancys lighthearted and comical grandfather (L.V. Prasad).

==Soundtrack==
{{Infobox album Name     = Raja Paarvai Type     = film Cover    =  Released =  Artist   = Illayaraja Genre  Feature film soundtrack Length   = 16:56 Label    = Polydor
}}
The music composed by Ilaiyaraaja.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Andhi Mazhai Pozhikirathu || S. P. Balasubrahmanyam, S. Janaki || Vairamuthu || 4:35
|-
| 2 || Azhagae Azhagu || K. J. Yesudas || Kannadasan || 4:28
|-
| 3 || Vizhiyorathu Kanavum || Kamal Haasan, Uma Ramanan || Gangai Amaran || 3:39
|-
| 4 || Theme Music || Instrumental || || 3:52
|}

==Reception==
The film was a box office failure but is widely considered a cult film. Kamal Hassans performance earned him a Filmfare Award. 
   Haasan had to work seven to eight years to compensate for the losses he faced through this film. 

==References==
 

== External links ==
*  

 
 
 

 
 
 
 
 
 