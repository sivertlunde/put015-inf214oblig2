Son of Sardaar
 
 

{{Infobox film
| name           = Son of Sardaar
| image          = Son Of Sardar poster.jpg
| caption        = Theatrical release poster
| director       = Ashwni Dhir
| producer       = Ajay Devgan N.R.Pachisia Pravin Talreja Sunil Lulla Viacom 18 Motion Pictures
| co-producer    = Vikrant Shrama Tanisha Mukerji Kumar Mangat
| writer         = Robin Bhatt Ashwni Dhir   (dialogue) 
| screenplay     = Robin Bhatt Ashwni Dhir S. S. Rajamouli
| story          = S. S. Kanchi Vara Mullapoodi   (story idea) 
| based on       = 
| starring       = Ajay Devgn Sonakshi Sinha Juhi Chawla Sanjay Dutt Mukul Dev Vindu Dara Singh
| music          = Songs   Himesh Reshammiya Guest Composers   Sajid-Wajid Background score   Sandeep Chowta
| cinematography = Aseem Bajaj
| choreographer  = Ganesh Acharya
| editing        = Dharmendra Sharma
| production designer = Sabu Cyril
| sound designer = Nakul Kamte
| action         = Jai Singh Nijjar
| studio         = Ajay Devgan Ffilms YRV Infra Media
| distributor    = Viacom 18 Motion Pictures Eros International
| released       =  
| runtime        = 141 minutes   
| country        = India
| language       = Hindi
| budget         =     
| gross         =   (worldwide) 
}}

Son of Sardaar, also known by the abbreviation SOS, is a 2012 Bollywood action comedy film directed by Ashwni Dhir.  The film features Ajay Devgan, Juhi Chawla, Sonakshi Sinha and Sanjay Dutt in lead roles and released on 13 November 2012. Salman Khan appears in a cameo role. 
 Telugu film Sunil and Yash Raj film Jab Tak Hai Jaan, Son of Sardaar managed to do good business at the box office worldwide.  Box Office India declared it a Hit in India and an average grosser in the overseas markets.     It went on to gross   worldwide.   

==Plot==
The story revolves around two families, whose patriarch killed each other many years ago. Their respective sons, Jaswinder Randhawa and Balwinder Singh, are sworn enemies. Jaswinder has been in hiding in London, whereas Balwinder is waiting for his return in Punjab so he can avenge the death of his father.

In the present day, Jassi Randhawa (Ajay Devgan) is an unemployed good-for-nothing man who receives a letter from the Punjab government about buying his property in Punjab. To sell the property, Jassi leaves for Punjab. On the train, he meets and falls in love with Sukhmeet (Sonakshi Sinha). In Punjab, Jassi accidentally meets Billu (Sanjay Dutt) and, seeing he is new to the area, Billu invites Jassi to his home, where he treats Jassi like a god. There, Jassi realises that Billu is Sukhmeets brother. Soon enough, it is revealed that Billu is Balwinder Singh and Jassi is short for Jaswinder. However, Billus family has to follow one punjabi rule: A guest in the house should never be harmed. Now, Billu must wait for Jassi to leave the house to kill him. Jassi learns about it and comes up with a hilarious plan to foil Billus shenanigans.

==Cast==

* Ajay Devgn as Jaswinder Singh Randhawa (Jassi)
* Sonakshi Sinha as Sukhmeet Kaur Sandhu (Sukh)
* Juhi Chawla as Pammi   
* Sanjay Dutt as Balwinder Singh Sandhu (Billu)
* Salman Khan as Pathan (extended appearance) 
* Puneet Issar as Inspector Sardar 
* Mukul Dev as Tony
* Vindu Dara Singh as Titu
* Tanuja as Bebe
* Arjan Bajwa as Bobby
* Sanjai Mishra as club owner in London

==Development== Telugu comedy Sunil and Saloni Aswani in lead roles. In an interview, director Ashwni Dhir replied that it was only the plot that was remade; the rest was changed.
 Kannada as Komal Kumar Bengali as Faande Poriya Boga Kaande Re starring Soham Chakraborty and Srabanti Chatterjee.   In fact, each and every antic in Son of Sardaar is prominently depicted in the Bengali remake.  The movie was also re-made in Tamil Vallavanukku Pullum Aayudham starring comedian Santhanam in a hero role with new-comer Asna as the heroine.

==Production==
  was spent on making Son of Sardar.     However, an all-inclusive price tag of   was also announced in the media.

The Punjabi Cultural Heritage Board raised disagreements due to some objection regarding certain scenes and dialogues. The producer and lead actor Ajay Devgn understood the objections and edited the scenes.  "The creativity of the film has not been affected in any way. Every film is rectified, sometimes its made even better," said Devgn during a press conference. "The objectionable things were very small. The writer and director of the film is Punjabi and we took care of things. Even the guy who tied my turban was from Punjab", he added.

==Marketing== Big Boss, hosted by Salman Khan in October 2012.   Ajay and Sonakshi also promoted it at Indian Grand Prix, Buddh International Circuit on 28 October 2012.  
 Sony TV.      Ajay Devgn and Sonakshi Sinha were celebrity guests to promote Son of Sardaar in Diwali special episode of Hero Sa Re Ga Ma Pa 2012 on 10 November 2012 at 9 PM.  The creators have released a set of images with Ajay Devgns character Jassi posing at several well-known landmarks around the world – Niagara Falls, Big Ben in London, to the tallest building in the world, the Burj Khalifa in Dubai, and the Sydney Opera House.  A special screening of Son of Sardaar was organised in Mumbai on 9 November 2012.  Ajay Devgn and Sonakshi Sinha promoted the film in Patna.  A special screening of Son of Sardaar was done for members of Indian cricket team. 

==Release==
Son of Sardaar released in 2,000 screens in India and 350 screens overseas.       Son of Sardaar got 300 more screens in its second week.  

===Controversy===
 
Two weeks before Diwali, Ajay Devgn sent an intimation notice to Yash Raj Films through their lawyer at the Competition Commission of India. The notice accused Yash Raj Films of monopolistic business practices and stated that they used "their dominant position in the Bollywood film market" to secure many high quality single-screens for their release Jab Tak Hai Jaan. 

Yash Raj Films responded by saying that they were shocked and questioned Devgns motives. The studio dismissed Devgns claim that high-quality single-screens were unavailable by pointing out that they had only booked 1,500 single-screens for Jab Tak Hai Jaan out of the 10,500 available in India. The studio added that Ajay could have changed the release of his film as Yash Chopra announced on 27 June 2011 Jab Tak Hai Jaan would come out on Diwali 2012. Ajay announced the date of Son of Sardaar on 29 May 2012 and finalised distribution deals on 4 October 2012. He had a year to avoid a simultaneous release. Lastly, Yash Raj Films stated it was unfair to say they were in a dominant position. The studio only released a handful of films in 2012 while the makers of Son of Sardaar, Viacom 18 and Eros International, released dozens the same year.   

After the statement made by Yash Raj Films, Ajay Devgn responded by saying he only managed to book 600 single-screens for Son of Sardaar and would take legal action if not allotted more. He denied having anything against Shahrukh Khan and said that distributors cannot be allowed to enter into an arrangement which adversely affects competition. Yash Raj Films entered into a tie-in arrangement with exhibitors that made it compulsory for them to show an untitled Yash Chopra film on Diwali and keep it in cinemas for at least two weeks thereafter. Devgn claimed that this violated provisions of the 2002 Indian Competition Act.   Senior film critic Vinod Mirani said that YRFs move of blocking single screens may not help much. "Single screens watchers will have more affinity towards an action film such as SOS. Moreover, JTHJs length and the fact that music has still not picked up, will not go down well with the movie goers." 
The Competition Commission dismissed the case against Yash Raj Films on 6 November 2012.  Ajay Devgn has appealed to Competition Appellate Tribunal after rejection of petition against Yash Raj Films.  The Competition Appellate tribunal bench headed by chairman Justice V. S. Sirpurkar refused Devgns stay against Jab Tak Hai Jaan but stated that the case against Yash Raj Films will be reviewed again. Both films would release on 13 November 2012.  

==Reception==

===Critical reception===
Son of Sardaar received mixed to negative reviews. Taran Adarsh of Bollywood Hungama gave it a score of 4 out of 5 stars and said "Son of Sardaar is for lovers of hardcore masala movies. If you liked Wanted, Dabangg and Rowdy Rathore, chances are you will relish Son of Sardaar as well. The North Indian audiences in particular and those residing abroad will be simply delighted by this chatpata, masaledaar fare."  Gayatri Sankar of Zee News gave it 3 out of 5 stars while commenting "Certain portions are a bit of a drag, but the comic sequences will make up for the monotony. The cast has put up a good show. Overall, SOS is worth a watch, a complete family entertainer.  Rediff gave 2 out of 5 and stated Son of Sardaar has a lot for the masses but it lacks a good story.  Filmfare gave 2 out of 5 stars and stated "Comedy is more than just gags and slapstick. Sadly SOS gets it wrong."  Rajeev Masand of CNN-IBN gave 2 out of 5 stars and stated Son of Sardaar "is only sporadically entertaining, and peddles the same tired stereotypes of Punjab and Sikhs. A cameo by Salman Khan, sadly, doesn’t make up for the film’s many flaws."  Hindustan Times gave the film 2 out of 5 stars, describing it as "exhausting, painfully loud and way too long, with too few laughs",  while the Telegraph stated that it was "run-of-the-mill", but that it could be a "guilty pleasure" for some viewers.  Like the Hindustan Times, IBNLive gave the movie 2 out of 5,  with a slightly lower grade coming from Saibal Chatterjee of NDTV, who rated the film 1.5 out 5, stated that it was "A tangled mess that has no way of working its way around the sloppy screenplay."  The Hindu stated that there’s nothing more torturous than watching an unfunny film that’s trying so hard to be funny. 

==Box office==

===India===

Son of Sardaar had opening of around 70% on average at multiplexes, especially in North India. The single screens opening was similar.  It earned around   on its opening day.    It shows very good growth of 70% with a collection of around   net on its second day.  It netted   nett in its first three-day weekend.    Son of Sardaar net grossed   in first five days of release.  The film held up well with a collection of   in its extended week of six days.    The movie brought its extended first-week collection to   nett.   

After its first week, the film continued its successful run and collected   nett in ten days.    Son of Sardaar had netted   after its second weekend.  Son of Sardaar dropped in week two, collecting around   net.    The film netted a total of   in four weeks.    Son of Sardaar has distributor share of 49–500&nbsp;million.   

===Overseas===

Son of Sardaar grossed $2 million in seven days.  Son of Sardaar did a business of around $3 million overseas, which BoxofficeIndia.com called it "average". 

==Soundtrack==
{{Infobox album 
| Name = Son of Sardaar
| Longtype = to Son of Sardaar
| Type = Soundtrack
| Artist = Himesh Reshammiya
| Cover = Son_of_Sardaar.jpg
| Border = Yes
| Alt = Yes
| Caption =
| Released =  
| Recorded =
| Length =   Hindi  Punjabi
| Label = T-Series
| Producer = Himesh Reshammiya Oh MY God  (2012)
| This album = Son of Sardaar (2012)
| Next album = Khiladi 786 (2012)
}}

The music of the film was composed by Himesh Reshammiya while the lyrics were penned by Sameer (lyricist)|Sameer, Shabbir Ahmed, Irshad Kamil & Manoj Yadav. Only one song, "Yeh Jo Halki Halki Khumariya", composed by Sajid-Wajid was also included in the album. 

===Track listing===

{{Tracklist
| headline        =
| extra_column    = Artist(s)
| total_length    = 47:45
| all_lyrics      =
| title1          = Son of Sardaar
| extra1          = Aman Trikha , Himesh Reshammiya
| length1         = 05:23
| note1           =
| title2          = Rani Tu Mein Raja
| extra2          = Mika Singh, Bhanvya Pandit, Yo Yo Honey Singh
| length2         = 05:28
| title3          = Po Po
| extra3          = Vikas Bhalla, Aman Trikha, Himesh Reshammiya
| length3         = 04:32
| title4          = Tu Kamaal Di Kudi
| extra4          = Vineet Singh, Mamta Sharma
| note4           =
| length4         = 05:16
| title5          = Bichdann
| extra5          = Rahat Fateh Ali Khan
| length5         = 05:21
| title6          =  Yeh Jo Halki Halki Khumariya
| extra6          = Rahat Fateh Ali Khan
| length6         = 03:53
| title7          = Kabhi Kabhi Mere Dil Mein Ye Sawaal Aata Hai (Funk)
| extra7          = Ajay Devgan
| length7         = 01:06
| title8          = Son of Sardaar (Remix)
| extra8          = Aman Trikha , Himesh Reshammiya
| length8         = 03:31
| title9          = Rani Tu Mein Raja (Remix)
| extra9          = Aman Trikha, Himesh Reshammiya
| length9         = 02:15
| note9           =
| title10         = Kabhi Kabhi Mere Dil Mein Ye Sawaal (Trance)
| extra10         = Ajay Devgan
| length10        = 02:15
| note11          =
| title11         = Bichdann (Reprise)
| extra11         = Rahat Fateh Ali Khan
| length11        = 02:15
| note11          =
}}

===Soundtrack reception===
{{Album ratings
| rev1 = IBNLive (IANS)
| rev1score =  
| rev2 = Abid (Glamsham)
| rev2score =  
| rev3 = Mohit Kapur (Koimoi)
| rev3score =  
| rev4 = Joginder Tuteja (Bollywood Hungama)
| rev4score =  
}}
The album received mixed to positive reviews. Joginder Tuteja of Bollywood Hungama gave the album 3.5 out of 5 stars and stated that "Son of Sardaar lives up to its promise of a high-on-energy soundtrack with not many dull moments in its 50-odd minutes duration. "  Koimoi gave it 3 out of 5 stars and said, "Overall this album is more than a decent affair where Punjabi tadka makes it reasonably happening. While the tracks Son of Sardaar and Bichdann will certainly do well, Kabhi Kabhi Mere Dil Mein and Rani Tu Mein Raja also carry potential to make their presence felt."  IBNLive gave it 3 out of 5 stars and said, "The album is woven around the theme of the film and manages to justify it well, although the sound gets a bit monotonous at times." 

==See also==
* Bollywood films of 2012

==References==
 

==External links==
*  
*  

 
 
 
 