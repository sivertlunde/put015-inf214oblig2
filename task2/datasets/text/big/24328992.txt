Voice of Silence (1953 film)
 Silent Voices}}
 
{{Infobox film
| name           = Voice of Silence
| image          = Voice of Silence.jpg
| caption        = Film poster
| director       = Georg Wilhelm Pabst
| producer       = Silvio Raffaele DAmico
| writer         = Georg Wilhelm Pabst, Giuseppe Berto
| starring       = Aldo Fabrizi
| music          = Enzo Masetti
| cinematography = Gábor Pogány
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Italy, France
| language       = Italian
| budget         = 
}}

Voice of Silence ( ,  ) is a 1953 Italian drama film directed by Georg Wilhelm Pabst, written by Giuseppe Berto, starring Aldo Fabrizi and Jean Marais.   

==Cast==
* Aldo Fabrizi - Pio Fabiani
* Jean Marais - Lancien maquisard
* Daniel Gélin - Lancien prisonnier
* Cosetta Greco - Femme du prisonnier
* Franck Villard - Lécrivain
* Antonio Crast
* Eduardo Ciannelli
* Paolo Panelli
* Fernando Fernán Gómez
* Maria Grazia Francia
* Checco Durante
* Paolo Stoppa
* Rossana Podestà
* Enrico Luzi
* Franco Scandurra
* Pina Piovani

==References==
 

==External links==
*  
*   at the Films de France

 

 
 
 
 
 
 
 
 
 