Les Mystères de Paris (1962 film)
{{Infobox film
| name           = Mystères de Paris
| image          =
| caption        =
| director       = André Hunebelle
| producer       = Da.Ma. Cinematografica   P.C.A.
| writer         = Diego Fabbri Pierre Foucaud
| starring       = Jean Marais
| music          = Michel Magne
| cinematography =
| editing        =
| distributor    =
| released       = 2 October 1962 (France)
| runtime        = 110 minutes
| country        = France, Italy
| awards         =
| language       = French
| budget         =
}}

Les Mystères de Paris (French:  ) is a French-Italian film from 1962, set in Paris. It was directed by André Hunebelle, written by Diego Fabbri  and Pierre Foucaud, starring Jean Marais.  The scenario was written on the basis of the novel Les Mystères de Paris. It has several precursors "Les mystères de Paris" (1912), "Les mystères de Paris" (1935). 

== Cast ==
* Jean Marais : Rodolphe de Sambreuil, lover of Irène
* Raymond Pellegrin : Baron de Lansignac, the enemy of Rodolphe
* Jill Haworth: Marie Godin
* Dany Robin : the teacher of Rodolphe
* Pierre Mondy : Chourineur
* Georges Chamarat : Jérôme Foulon
* Noël Roquevert : Mr. Pipelet, the concierge
* Jean Le Poulain : The Teacher
* Renée Gardès : the Owl
* Alain Decock : the young person Fanfan Morel
* Robert Dalban: the boss of the inn of the Destructives
* Paulette Dubost: Mrs Pipelet, the concierge
* Benoîte Lab : Louise Morel, the woman of the suit
* Gabriel Gobin : Mr. Morel, the suit
* Madeleine Barbulée : Mrs Godin
* Maria Meriko : Mrs Georges
* Charles Bouillaud : Mr. Godin
* Raoul Billerey : Amédée
* Guy Delorme : policeman

== References ==
 

 
 
 
 
 
 
 


 