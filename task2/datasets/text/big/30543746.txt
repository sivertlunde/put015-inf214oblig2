Der Rosenkavalier (1926 film)
{{Infobox film
| name           =  
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene
| producer       = Robert Wiene
| writer         = Richard Strauss (opera) and Hugo von Hofmannsthal; Ludwig Nerz Robert Wiene
| narrator       =  Paul Hartmann Jaque Catelain
| music          = 
| editing        = 
| cinematography = Hans Androschin Ludwig Schaschek Hans Theyer
| studio         = Pan Film
| distributor    = Filmhaus Bruckmann
| released       =  
| runtime        = 
| country        = Austria Silent German intertitles
| budget         = 
| gross          = 
}} Austrian silent opera of the same name by Richard Strauss (music) and Hugo von Hofmannsthal (libretto). Directed by Robert Wiene, it premiered on 10 January 1926 at the Dresden Semperoper, which had also hosted the actual operas premiere 15 years earlier. Hofmannstahl considerably changed the storyline for the film version (which included a final scene in the formal gardens behind the Field Marshals residence) and Strauss; score included music not only from the opera but also sections of his Couperin Suite and a march for the Field Marshal, who appears in this version.

The music during the films performances was provided by an orchestra. At the premiere, this was conducted by Richard Strauss himself. The films projection speed had to be adjusted by the projector in order to fit the speed of the orchestra. This task fell to the films camerman, Hans Androschin, because only he knew the exact length of each scene and cut. In later performances, a special recording, also conducted by Strauss, provided the music. A planned tour of the United States in 1927 by Strauss and his orchestra failed to go ahead because of the emergence of sound films.

Strauss conducted the Vienna and London premieres (and recorded excerpts from the film score on the Victrola label at that time). The American premiere took place at Yale Universitys Woolsey Hall with the Yale Symphony conducted by John Mauceri (who received special permission from Strauss son) on March 29, 1974. A copy of the film was found in the Czech National Archive and Mr. Mauceri translated the titles with Glenn Most into English. The final sequence was missing from the print and was performed with orchestral music and titles alone. The score and parts were held by the Library of Congress. The audience at Yale included the famed Strauss soprano Maria Jeritza, who was living in New jersey at the time.,   

==Cast==
*Michael Bohnen as Ochs von Lerchenau
*Huguette Duflos as Marschallin Paul Hartmann as Marschall
*Jaque Catelain as Octavian
*Elly Felicie Berger as Sophie
* Carmen Cartellieri as Annina
* Karl Forest as Herr von Faninal
* Friedrich Feher as Valzacchi

==References==

 
* , Arte TV

http://www.johnmauceri.com/biography/?ch=2

http://www.johnmauceri.com/cv/

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 
* , Houston Symphony
* , Staatskapelle Dresden, Semperoper

 

 
 
 
 
 
 
 
 
 


 
 