The Kingfisher Caper
{{Infobox film
| name           = The Kingfisher Caper
| image          = The Kingfisher Caper poster.jpg
| image_size     = 
| caption        = Movie poster
| director       = Dirk DeVilliers
| producer       = Ben Vlok
| writer         = Roy Boulting Lee Marcus
| based on = the novel The Diamond Hunters by Wilbur Smith (uncredited)
| narrator       = 
| starring       = Hayley Mills David McCallum Jon Cypher Bill McNaught
| music          = John Dankworth
| cinematography = Ivo Pellegrini
| editing        = Kenneth Connor
| distributor    = Cinema Shares International Distribution Corporation (USA)
| released       = 1975
| runtime        = 86 minutes
| country        = South Africa
| language       = English
| budget         = 
| gross          =  1975 South African film directed by Dirk DeVilliers for Kavalier Films Ltd. It stars Hayley Mills (as Tracey van der Byl), David McCallum (Benedict van der Byl), Jon Cypher (Johnny Lance), Volente Bertotti (Ruby Lance), Barry Trengove (Cappy) and Bill McNaught (Hendrich van der Byl).

==Cast==
===Main cast===
* Hayley Mills as Tracey Van Der Byl
* David McCallum as Benedict Van Der Byl
* Jon Cypher as Johnny Lance
* Barry Trengove as Cappy
* Volenté Bertotti as Ruby Lance
* Bill McNaught as Hendrich Van Der Byl
* Don Furnival as Mac
* Gordon van Rooyen as Kramer
* Stanley Leeshaw as Osaki
* Pieter Geldenhuys as Attorney
* Ray Novak as Joe
* Ian Botha as Sampson

===Supporting cast===
* Dirk de Villiers as Les (uncredited)

==Remake==
It was remade as the 2001 miniseries The Diamond Hunters with Alyssa Milano, Roy Scheider, Sean Patrick Flanery and Michael Easton in the Mills, McNaught, Cypher and McCallum roles respectively; Jolene Blalock, Armin Rohde and Hannes Jaenicke also featured.

==Production notes==
Kingfisher Caper writer Roy Boulting was married to star Hayley Mills at the time of the filming. This was his final writing credit.

==External links==
*  at the Internet Movie Database
 

 
 
 
 
 
 


 
 