Admiral (film)
 
{{Infobox film
| name           = Admiral
| image          = Admiral (film) poster.jpg
| image_size     = 
| format         = Historical Drama
| caption        = 
| director       = Andrei Kravchuk
| producer       = Janik Faysiev Konstantin Ernst
| writer         = Zoya Kudrya Vladimir Valutsky
| narrator       = 
| starring       = Konstantin Khabensky Elizaveta Boyarskaya Sergey Bezrukov Anna Kovalchuk
| released       =  
| runtime        = 123 minutes
| country        = Russia Russian French French
| budget         = $20 million
| gross          = $38,135,878
}}
Admiral ( ) is a 2008 biopic about Alexander Kolchak, a Vice-Admiral in the Imperial Russian Navy and leader of the anti-communist White Movement during the Russian Civil War. The film also depicts the love triangle between the Admiral, his wife, and the poet Anna Timiryova.

According to director Andrei Kravchuk,  "  about a man who tries to create history, to take an active part in history, as he gets caught in the turmoil. However, he keeps on struggling, he preserves his honour and his dignity, and he continues to love."  

==Plot== War and Russian noblewoman is set to appear as a film extra until her past comes to light. Although the films political commissar demands her dismissal since she is a "wife of an enemy of the revolution", director Sergei Bondarchuk is adamant that he needs faces like hers for the production. As the commissar realises the difficulty of identifying her using the file he has, he immediately recognises the elderly woman behind him as the woman he is searching for, while the elderly woman is looking at her own 1910s photo.
 Captain Alexander Kolchak (Konstantin Khabensky) is laying naval mines from his ship in German territorial waters when he runs across SMS Friedrich Carl, an armoured cruiser of the German Imperial Navy. As chaos reigns on his ship, Kolchak sights in one of the guns and succeeds in seriously damaging the bridge of the German vessel. This buys him only a brief respite, however.

Realising that the enemy ship is blocking his line of escape, Kolchak informs his men that the only way to return to safety is to lure the Germans onto one of his mines. As the ship steams into the mines they have just laid, Kolchak leads his men in Russian Orthodox prayers for Gods protection. Although they barely avoid hitting their own mines, the German ship is not so lucky and sinks with almost all hands.

Later, at their naval base in the Grand Duchy of Finland, now promoted to Rear Admiral, Kolchak is introduced to Anna Timireva (Elizaveta Boyarskaya), the wife of his subordinate officer and close friend Captain Sergei Timirev. The strong attraction between them immediately becomes apparent. Although Sergei reminds his wife that they took vows before God, Anna is unmoved and wants nothing more than to be with the Admiral.

Although terrified of losing him, Sofia Kolchak (Anna Kovalchuk) offers to leave for Petrograd and let her husband be with Anna if he so desires. The Admiral, however, firmly tells her, "You are my wife and I am your husband. That is how it always shall be."

His feelings for Anna continue to grow, however. When she approaches him to deliver a love letter, Kolchak informs her that they cannot ever meet again. When Anna demands to know why, the Admiral responds, "Because I love you."

Later, he is informed that the Tsar (Nikolai Burlyayev) has promoted him to Vice Admiral in command of the Black Sea Fleet at Sevastopol. After receiving a last minute letter from Kolchak, Anna rushes to the train station to see off her beloved. She is too late, however, and experiences only an uncomfortable look from Sofia.
 summarily executed at the Kronstadt naval base. Sergei barely escapes the island with Anna. Meanwhile, a group of enlisted men, now with their cockades on their sailor caps changed from a Tsarist insignia to a red revolutionary cockade,  arrive aboard Kolchaks flotilla in Sevastopol and demand that all officers surrender their arms. To avoid bloodshed, Kolchak orders his subordinates to obey. However, he throws his own sword into the harbour rather than hand it over, but the sailors didnt complain about it.

Later, Kolchak is summoned to Petrograd by Alexander Kerensky (Viktor Verzhbitsky), who offers to appoint him Minister of Defence. Kolchak, however, sharply criticises Kerensky for promoting indiscipline in both the Russian Army and Navy. He states that the only way he will accept is if he is given a free hand to restore old practices. Kerensky, enraged by Kolchaks "counterrevolutionary sympathies," exiles him to the United States, excusing that "the allies needs more experts".

After the October Revolution, Anna and Sergei Timiriov are travelling on the Trans-Siberian Railroad when she learns that Kolchak has returned and is leading a detachment of the anti-communist White Army at Omsk. Sergei, who has become a Red Commissar, is shocked when she returns to their carriage and announces that she is leaving him. After commenting about what a year of Revolution it has been, Sergei helps Anna to pack her things.

After hearing Kolchaks speeches about defeating the Bolsheviks and "restoring Russia," Anna is deeply moved and goes to work as a nurse among the wounded of the Russian Civil War. Meanwhile, Kolchak is informed that the Red Army is advancing on Omsk, assisted by sympathizers behind White lines. Although his advisors all suggest defending Omsk to the last, Kolchak decrees that they will instead evacuate Omsk and seize Irkutsk as the new capital of anti-communist Russia.

During the evacuation of Omsk, Anna is recognised by a White officer who informs Kolchak. Deeply moved, the Admiral goes to her and announces that, although he made the mistake of leaving her once, he will never do so again. As the train steams toward Irkutsk, Kolchak informs Anna that he has written to his wife Sofia, who now lives in Paris, and formally asked for a divorce. Although he asks Anna to marry him, she insists that there is no need of marriage and that what matters is that they are together now. Eventually, she relents and they are seen attending the Divine Liturgy together.

Meanwhile, Irkutsk is under the nominal control of the French General Maurice Janin and the Czechoslovak Legions. With their defences disintegrating, the Red Army offers them only one way out alive. As a result, General Janin agrees to hand over Admiral Kolchak.

As a massive land force of White soldiers rides and marches toward Irkutsk, Kolchak is placed under arrest by the Czechs and handed over to the Reds. Despite Kolchaks attempts to shield her, Anna insists that, as his wife, she must be arrested, too.
 soviet and executed with his former Prime Minister by a firing squad along the banks of the frozen Angara River. His last words are, "Send word to my wife in Paris that I bless our son."

Kolchaks body is then dumped into a hole drilled into the ice by the local Orthodox clergy for the Great Blessing of Waters on Epiphany (holiday)#Eastern Orthodox Christian churches|Epiphany. 

The film then returns to the present, 44 years later at the Mosfilm Studios. It is revealed that Anna - now in her 70s and identified as the elderly noblewoman - survived more than 30 years in the Gulag and was only released during the Khrushchev thaw. Anna Timiorova witnesses a rehearsal for one of the ballroom scenes from War and Peace, as she accidentally bumps into an actor playing as a waiter, breaking a glass of wine, which reminds her of the first time she meets Kolchak, then the film shifts to her imagination as she dreams of the formal dance she was never able to share with her beloved. The fates of the films main characters are then revealed in an epilogue:
*Anna Vasilyevna Timiryova was arrested numerous times following Admiral Kolchaks execution, and survived nearly 40 years in the Gulag before being released in 1960. She died in Moscow in January 1975, aged 81.
*Annas former husband Sergei Timirev became a rear-admiral commanding the White Russian navy in Siberia before fleeing to China, where he captained Chinese steamers. He settled in the White Russian community in Shanghai, where he died in 1932.
*In her Paris exile, Sophia Kolchak, the Admirals wife, devoted her life to her son. She died an old lady in the Longjumeau Hospital in 1956.  
*Rostislav Kolchak, the Admirals son, fought with the Free French Forces during the Second World War. He died in Paris in 1965. 

==Release==
The first screening of the film took place on October 9, 2008. 

==Themes== Doctor Zhivago, she stated,  "The only thing that these two films share consists in the love which the Russian women can carry; it is a topic approached by many novels. They love up to the last drop of blood, till the most dreadful end, to the death; they are capable of leaving family and children for the love of the man which they have chosen. 

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Player !! Role
|-
| Konstantin Khabensky || Admiral Alexander Kolchak
|-
| Sergei Bezrukov || General Vladimir Kappel
|-
| Vladislav Vetrov ||  Captain Sergey Timirov
|-
| Elizaveta Boyarskaya || Anna Timiryova
|-
| Anna Kovalchuk || Sofia Kolchak
|-
| Yegor Beroyev  || Mikhail Smirnov
|-
| Richard Bohringer || General Maurice Janin
|-
| Viktor Verzhbitsky || Alexander Kerensky
|-
| Nikolai Burlyayev || Nicholas II of Russia
|-
| Fyodor Bondarchuk || Director Sergei Bondarchuk
|-
|}

==Soundtrack==
The main original song for the film is called "Anna". It is performed by Russian singer Victoria Dayneko. The music for the song was composed by Igor Matvienko and the poem itself was written by Anna Timireva in memory of Admiral.

The other song "Vopreki" ("Despite") was written by Konstantin Meladze. This song is performed by Russian star Valery Meladze.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 