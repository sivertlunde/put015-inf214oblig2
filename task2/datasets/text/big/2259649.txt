Maniac Cop
{{Infobox film
| name           = Maniac Cop
| image          = Maniac Cop Movie Poster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = William Lustig
| producer       = Larry Cohen
| writer         = Larry Cohen Tom Atkins William Smith Laurene Landon Richard Roundtree
| music          = Jay Chattaway
| cinematography = James Lemmo   Vincent J. Rabe
| editing        = David Kern
| studio         = Shapiro-Glickenhaus Entertainment
| distributor    = Shapiro-Glickenhaus Entertainment
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $1,100,000   
| gross          = $671,382 
}}

Maniac Cop is a 1988 action film|action/slasher film directed by William Lustig, and written by Larry Cohen. 

== Plot ==
 Tom Atkins), who was told by his superiors to suppress eyewitness accounts that the killer was wearing a police uniform, to pass on information to a journalist, in an attempt to protect civilians. Unfortunately, this causes panic and dissent among the city and results in innocent patrolmen being shot to death by paranoid people.

Ellen Forrest (Victoria Catlin), who suspects that her husband Jack (Bruce Campbell) may be the Maniac Cop, follows him to a motel, where she catches him in bed with a fellow officer, Theresa Mallory (Laurene Landon). Distraught, Ellen runs out of the room, and is slain by the murderer. Jack is arrested under suspicion of murder, but McCrae believes Jack has been framed. McCrae gets Jack to tell him about his relationship with Mallory, who is attacked by the Maniac Cop while working undercover as a prostitute. Mallory and McCrae fight off the killer, who is deathly cold even through his gloves and does not appear to breathe; though they shoot him several times, the killer appears unfazed.

Mallory hides out in McCraes apartment while he investigates Sally Noland (Sheree North), the only person Mallory told about her affair. McCrae follows Noland to a warehouse, where she meets with the maniac cop and refers to him as "Matt". Returning to police headquarters, McCrae discovers files on Matthew Cordell, a fellow officer who was imprisoned in Sing Sing for police brutality and closing in on corruption in city hall. While McCrae is looking into his past, Cordell flashes back to being mutilated and killed in a shower room in Sing Sing.

When McCrae and Mallory visit Jack, they tell him that they think Cordell is the real killer and plan to visit the chief medical examiner at Sing Sing. McCrae leaves to go to the clerical room, and he is attacked by Sally, who is in hysterics, convinced that Cordell is going to turn on her. After finding a policeman hanging in a noose, Sally is grabbed by Cordell and beaten to death. Hearing the commotion, Jack and Mallory break out of the interrogation room and find the corpses of numerous officers strewn about the halls of the building. Jack tells Mallory to go to McCraes car while he searches for Cordell, who disappears after throwing McCrae out a window, killing him. Jack, who looks like the one responsible for the carnage to responding officers, flees with Mallory.
 William Smith) about Cordell, but the two refuse to believe her and have her arrested. Cordell stabs Pike and Ripley to death, then targets Mallory, knifing the policeman left to guard her. Mallory escapes through a window, while Jack is arrested and placed in a van, which Cordell hijacks.

Mallory and another officer chase the van, which Cordell takes to his warehouse hideout, running over the watchman on the way in. Cordell attacks Mallory and Jack, kills the other officer, and tries to escape in the van when backup arrives. Jacks clings to the side of the van and fights for control of it, distracting Cordell and causing him to drive into a suspended pipe, which impales him. Cordell loses control of the vehicle, which crashes into the river, and sinks. The van is fished out, and, as it is searched, Cordells hand shoots out of the water.

In the extended cut, corrupt mayor Jerry Killium relaxes in his office, content Cordell is gone. After Killiums assistant leaves, Cordell, who was hiding behind a curtain, murders the mayor offscreen as the credits roll.

==Cast== Tom Atkins as Detective Lieutenant Frank McCrae
* Bruce Campbell as Officer Jack W. Forrest, Jr.
* Laurene Landon as Officer Theresa Mallory
* Richard Roundtree as Commissioner Pike William Smith as Captain Ripley
* Robert ZDar as Officer Matthew Cordell
* Nina Arvesen as Regina Sheperd
* Sheree North as Officer Sally Noland
* Victoria Catlin as Ellen Forrest
* Ron Holmstrom as Building Superintendent (uncredited)

Director William Lustig makes a cameo appearance as a Motel Manager.  Director Sam Raimi appears as "Parade Reporter."

== Soundtrack ==
 LP in 1988 by Phoenix Records.

Track listing
# "Main Title"
# "A Night in the Park"
# "Flashback Montage"
# "The Chase"
# "Morning Train"
# "Face Down"
# "Sallys Drive"
# "On the Ledge"
# "Splash Dance"
# "Epilogue"

== Release ==
Maniac Cop was released May 13, 1988.  It played in 50 theaters and had a domestic gross of $671,382. 

=== Home video release ===
 DTS soundtrack.  In October 2011 Synapse Films released a Blu-ray edition of the film. 

== Reception ==
 Friday the Time Out London criticized the film as formulaic and said that it might have been better had writer-producer Cohen directed it himself.   Richard Harrington of The Washington Post called the script "undernourished and obvious". 

Reviewing the Blu-ray release, J. Hurtado of Twitch Film wrote that despite its faults, Maniac Cop deserves mention as one of the last grindhouse films set in New York City.   Tom Becker of DVD Verdict called it "a fun, mindless gorefest".   Bill Gibron of DVD Talk rated it 4.5/5 stars and called it "one of the eras finest forgotten gems", deserving of a critical reappraisal.   Noel Murray of The A.V. Club rated it B- and called it a goofy film that was always meant to inhabit the shelves of independent video rental stores.   Gareth Jones of Dread Central rated it 4/5 stars and called it a cult film that is "amongst the cream of the crop of late-eighties low-budget horror".   Bloody Disgusting rated it 5/10 stars and questioned why the film has a cult following when it has a poor script and direction, uneven tone, and boring kills. 

Bruce Campbell said of the film that it was "not a good movie" when viewed in hindsight, but it initially struck him as "perfectly legit." 

== Remake ==
In 2012, the Los Angeles Times reported that William Lustig and Nicholas Winding Refn are attached to a remake. 

== Sequel ==
It was followed by two sequels,   in 1993. 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 