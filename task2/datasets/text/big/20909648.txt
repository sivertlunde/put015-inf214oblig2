The Carpetbaggers (film)
{{Infobox film
| name           = The Carpetbaggers
| image          = The Carpetbaggers 1964 poster.jpg
| image_size     = 
| caption        = U.S. poster art
| director       = Edward Dmytryk
| producer       = Joseph E. Levine
| writer         = John Michael Hayes Harold Robbins  (novel) 
| narrator       = 
| starring       = George Peppard Alan Ladd Carroll Baker Bob Cummings Martha Hyer
| music          = Elmer Bernstein
| cinematography = Joseph MacDonald
| editing        = Frank Bracht
| distributor    = Paramount Pictures
| released       =  
| runtime        = 150 minutes
| country        = United States
| language       = English
| budget         = 
$3.3 million "The fruitful labours of Levine." Sunday Times   11 Oct. 1964:  . The Sunday Times Digital Archive. Web. 29 Mar. 2014.
    IMDb. Retrieved 16 July 2013. | gross = $40,000,000 
}}

The Carpetbaggers is a 1964 American film directed by Edward Dmytryk, based upon the best-selling novel The Carpetbaggers by Harold Robbins, and starring George Peppard as Jonas Cord, a character based loosely on Howard Hughes, and Alan Ladd - in his last role - as Nevada Smith, a former western gunslinger turned actor. Carroll Baker portrays an actress. The film is a landmark film of the sexual revolution of the 1960s, venturing further than most films of the period with its heated sexual embraces, innuendo, and sadism between men and women,  much like the novel, where "there is sex and/or sadism every 17 pages".   
 Where Love Western prequel entitled Nevada Smith.

==Plot==
Jonas Cord becomes one of Americas richest men, inheriting an explosives company from his late father. Cord resents his father bitterly and is psychologically scarred by the death of a twin brother. Believing his family has insanity in its blood, he does not want children of his own. 

Cord buys up the company stock held by Nevada Smith, a former gunslinger. He had practically raised Cord in the absence of his father. Cord pays off his fathers widow Rina Marlowe, who he believes had married his father strictly for money. She is portrayed as a gold-digger and sexually assertive. 

Cord becomes an aviation pioneer and his wealth grows. He ruins a business rival named Winthrop, then seduces and marries the mans daughter Monica. He quickly abandons her and demands a divorce.

Nevada Smith finds work in western films. Rina resurfaces to become a movie star for a studio owned by Bernard Norman. Cord seeks to buy that studio, but Norman refuses until after learning that Rina, by then alcoholic, has died in a car crash. A public relations man, Dan Pierce, betrays his employer Cord, who pays Norman more money than the studio is worth absent its chief box-office draw.

Cord goes on an alcoholic binge and disappears. Upon his return, he decides to run the studio, even directing films. He casts an attractive call girl, Jennie Denton, as the studios new girl star. Cord cuts his ties with aviation partner Buzz and longtime lawyer Mac. He so badly mistreats Jennie that his old friend Nevada Smith challenges him to a fistfight and badly beats him. 

A contrite Cord returns to Monica, with whom he has a child. He has learned from Nevada that there was no insanity in his family after all. His and Monicas daughter grows up as a  healthy, normal girl (as much as is possible given her parents pasts).

== Cast ==
{{columns-list|2|
* George Peppard as Jonas Cord
* Alan Ladd as Nevada Smith 
* Carroll Baker as Rina Marlowe Cord 
* Robert Cummings as Dan Pierce (billed as Bob Cummings)
* Martha Hyer as Jennie Denton
* Elizabeth Ashley as Monica Winthrop
* Lew Ayres as Mac McAllister
* Martin Balsam as Bernard B. Norman
* Ralph Taeger as Buzz Dalton
* Archie Moore as Jedediah Leif Erickson as Jonas Cord, Sr.
* Arthur Franz as Morrissey
* Tom Tully as Amos Winthrop
* Audrey Totter as Prostitute
* Anthony Warde as Moroni Charles Lane as Denby
* Tom Lowell as David Woolf John Conte as Ed Ellis Vaughn Taylor as Doctor
* Francesca Bellini as Cynthia Randall
* Lisa Seagram as Moronis Secretary
* Victoria Jean as Jo Ann Cord}}

==Release and reception==
 

The Carpetbaggers was released theatrically in North America on April 9, 1964, and was a massive commercial success. It grossed $28,409,547 at the domestic box office,  making it the  . Retrieved July 16, 2013.  Due to its success, a prequel was filmed and released two years later. Ladds part was taken by Steve McQueen.  

The movie was one of the 13 most popular films in the UK in 1965.  However, many critics frowned upon the film, considering it to be "vulgar and tasteless" or "an upscale dirty movie".     The film became one of the targets for the negative impact of films on society. Bosley Crowther cited the film, along with Kiss Me, Stupid, for giving American movies the reputation of "deliberate and degenerate corruptors of public taste and morals".   
 Jimmy Smith arranged by Lalo Schiffrin.

In her 1978 autobiography Past Imperfect, Joan Collins claims she had a firm offer to play Rina Marlowe but had to decline because of pregnancy.

==Soundtrack==
Elmer Bernstein re-recorded his music for the movie as an album on Ava Records. In 2013 Intrada Records issued the complete original soundtrack on CD, pairing it with the CD premiere of the Ava re-recording (tracks 22-31).

# Seal / Main Title 2:26 
# A Maverick 0:52 
# Rinas Record 3:32
# The Forbidden Room 2:42 
# Sierra Source (Alternate) 1:41
# Sierra Source 2:39
# Separate Trails 2:03
# Monicas Shimmy 0:31
# Lots Of Lovely Ceilings 2:02 
# Nevadas Trouble 7:12 
# Get A Divorce 1:35 
# Movie Mogul 0:35 
# Two Of A Kind 5:11
# Sierra Source Pt. 2 2:14 
# Rinas Dead 1:02 
# Speak Of The Devil 1:29 
# New Star 3:05 
# Bad Bargain 0:51 
# Jonas Hits Bottom 5:40 
# Finale 1:26
# Love Theme From The Carpetbaggers 3:10 
# The Carpetbaggers 2:31 
# Love Theme From The Carpetbaggers 2:40 
# Speak Of The Devil 2:01 
# Forbidden Room 2:19 
# The Carpetbagger Blues 3:52 
# Main Title From The Carpetbaggers 2:10 
# New Star 2:16 
# The Producer Asks For A Divorce 2:39 
# Jonas Hits Bottom 2:50 
# Finale 1:44 

==Legacy==
*Nevada Smith (1966) was conceived as a prequel to the The Carpetbaggers. Steve McQueen was chosen for the role of Smith as a much younger man named Max Sand who makes up the name Nevada Smith when he infiltrates the gang of a man he intends to kill.

==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 