The Other Half (1919 film)
 
{{Infobox film
| name           = The Other Half
| image          = The Other Half (1919) - Ad 1.jpg
| caption        = Advert for film with Charles Meredith and Zasu Pitts, shown
| director       = King Vidor
| producer       = Joe Pasternak
| writer         = King Vidor		 Charles Meredith
| cinematography = Ira H. Morgan
| editing        = 
| studio         = Brentwood Film
| distributor    = Exhibitors Mutual
| released       =  
| runtime        = 50 minutes
| country        = United States 
| language       = Silent
| budget         = 
}}

The Other Half is a 1919 American film directed by King Vidor.    The survival status of the film is classified as unknown,  suggesting that it is likely a lost film.

==Cast==
* Florence Vidor as Katherine Boone Charles Meredith as Donald Trent
* Zasu Pitts as Jennie Jones, The Jazz Kid David Butler as Cpl. Jimmy Alfred Allen as J. Martin Trent
* Frances Raymond as Mrs. Boone
* Hugh Saxon as James Bradley Thomas Jefferson as Caleb Fairman
* Arthur Redden as The Star Reporter

==See also==
* List of American films of 1919

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 


 