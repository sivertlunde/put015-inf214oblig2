Raasukutti
{{Infobox film 
| name           = Rasukutty
| image          = 
| caption        = 
| director       = K. Bhagyaraj
| producer       = Meena Panchu Arunachalam
| writer         = K. Bhagyaraj (dialogues)
| screenplay     = K. Bhagyaraj
| story          = K. Bhagyaraj Aishwarya Manorama Manorama Kalyan Kumar
| music          = Ilayaraja
| cinematography = M. C. Sekar
| editing        = B. K. Mohaan
| studio         = P. A. Art Productions
| distributor    = P. A. Art Productions
| released       =  
| runtime        = 149 minutes
| country        = India  Tamil
}}
 1992 Cinema Indian Tamil Tamil film, Manorama and Raja Babu with Govinda, in Telugu as Abbayigari Pelli with Suman and in Kannada as Patela with Jaggesh.  

==Cast==
* Bhagyaraj as Raasukutti Aishwarya as Raasukuttis Lover Manorama as Raasukuttis Mother
* Kalyan Kumar as Raasukuttis Father Periyapannai
* Assistant Director Jagan as Sembuli Mounika as Raasukuttis Uncle Daughter
* Nalinikanth as Periyapannais Younger Brother
* Suryakanth as Nalinikanths Elder Son
* Junior Baliah as Soona Baana
* Bailvan Rangaathan as Raasukuttis Uncle
* Nandhagopal as Ammavasai- Nalinikanths Younger Son

==Soundtrack==
* Adi Naan Pudicha - S.P. Balasubrahmanyam
* Holi Holi - S.P. Balasubrahmanyam, S. Janaki
* Palayathu Ponnu - K. S. Chitra
* Vaadi Sengamalam - Minmini

==References==
 

==External links==
*   
*  

 

 
 
 
 
 
 


 