Before the Deluge
 
{{Infobox film
| name           = Before the Deluge
| image          =
| caption        =
| director       = André Cayatte
| producer       = François Carron 
| writer         = André Cayatte Charles Spaak
| starring       = Antoine Balpêtré
| music          = Georges Van Parys
| cinematography = Jean Bourgoin
| editing        = Paul Cayatte
| distributor    =
| released       = 26 February 1954
| runtime        = 138 minutes
| country        = France
| language       = French
| budget         =
}}

Before the Deluge ( ) is a 1954 French drama film directed by André Cayatte. It was entered into the 1954 Cannes Film Festival.     
==Plot==
Four boys and a girl want to get away from their parents and their country because they are afraid of an atomic war. They plan to use a boat to get to an idyllic island. When they realise their savings arent sufficient they feel it was justified to obtain the required money by committing a crime.

==Cast==
* Antoine Balpêtré - Monsieur Albert Dutoit (as Balpetre)
* Paul Bisciglia - Jean-Jacques Noblet
* Bernard Blier - Monsieur Marcel Noblet
* Jacques Castelot - Serge de Montesson
* Jacques Chabassol - Jean Arnaud
* Clément Thierry - Philippe Boussard (as Clement-Thierry)
* Roger Coggio - Daniel Epstein
* Léonce Corne - Commissaire Auvain (as Leonce Corne)
* Jacques Fayet - Richard Dutoit
* Paul Frankeur - Monsieur Boussard
* Isa Miranda - Madame Françoise Boussard
* Carlo Ninchi -  the presiding judge

* Line Noro - Madame Arnaud (as Line Noro de la Comédie Française)
* Marcel Pérès - Inspecteur Mallingré (as Marcel Peres)
* Albert Rémy - waiter at the café (as Albert Remy)
* Delia Scala - Josette
* André Valmy - the second police inspecteur (as Andre Valmy)
* Julien Verdier - the night watchman
* Marina Vlady - Liliane Noblet
* Maria Zanoli - Madame Dutoit (as Maria Emma Zanolli)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 