Khandhar
{{Infobox film
| name           = Khandahar
| image          = Khanda4f.jpg
| image_size     =
| caption        = French poster of the film.
| director       = Mrinal Sen
| producer       = Jagadish Chokhani
| screenplay     = Mrinal Sen Premendra Mitra
| based on       =  
| narrator       =
| starring       = Shabana Azmi Naseeruddin Shah  Sreela Majumdar
| music          = Bhaskar Chandavarkar
| cinematography = K. K. Mahajan
| editing        = Mrinmoy Chakraborty
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}}

Khandahar (translation: "The Ruins") is a 1984 Hindi film directed by Mrinal Sen, based on a Bengali short story, Telenapota Abishkar (Discovering Telenapota) by Premendra Mitra.  The film stars Shabana Azmi, Naseeruddin Shah and Pankaj Kapoor. It was screened in the Un Certain Regard section at the 1984 Cannes Film Festival.   

== Plot ==
Three friends from the city visit some ruins where an aged mother (Gita Sen) and her daughter Jamini (Shabana Azmi) live. Mother awaits the arrival of a distant cousin to marry Jamini, but the man is already married and living in Calcutta. The photographer Subhash (Naseeruddin Shah) takes pity on the family and pretends to be the awaited suitor. The mother dies contented. When the threesome leave again, Jamini stays behind, facing a life of loneliness in the ruins. 

== Cast ==
* Shabana Azmi ... Jamini
* Naseeruddin Shah ... Subhash
*  Gita Sen ... The Mother
* Pankaj Kapoor ... Dipu
* Annu Kapoor ... Anil
* Sreela Majumdar ... Gory

== Awards ==
* 1985: Chicago International Film Festival: Best Film
* 1985  
* 1984  
* 1984  
* 1984 National Film Award for Best Editing : Mrinmoy Chakraborty

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 