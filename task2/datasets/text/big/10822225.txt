Golden Swallow (1968 film)
 
 
{{Infobox film name = Golden Swallow image =  alt =  caption =  traditional = 金燕子 simplified = 金燕子 pinyin = Jīn Yàn Zǐ}} director = Chang Cheh producer = Runme Shaw writer = Chang Cheh Tu Yun-chih starring = Jimmy Wang Lo Lieh Chao Hsin-yen music = Wang Fu-ling cinematography = Pao Hsueh-li editing = Chiang Hsing-lung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 104 minutes country = Hong Kong language = Mandarin
|budget =  gross = 
}} Jimmy Wang, Lo Lieh and Chao Hsin-yen. Cheng Pei-pei reprises her role from Come Drink with Me. The film has been released on DVD from Dragon Dynasty.

== Plot ==
Golden Swallow revolves around the further adventures of its title character. This time around, she is forced into violence when a figure from her mysterious past goes on a killing rampage while leaving evidence that holds her responsible. Golden Swallow also makes room for a love triangle involving a mad, but righteous, swordsman named Silver Roc and a gentle warrior named Golden Whip. The three team up to conquer the evil forces of the martial world, but their joint venture only lasts so long, due to the two mens egos. Ultimately, a duel to the death is planned between them, leaving Golden Swallow caught between two men, both of whom she admires.

==Cast==
*Cheng Pei-pei as Golden Swallow Jimmy Wang as Silver Roc
*Lo Lieh as Golden Whip
*Chao Hsin-yen as Mei Niang
*Wu Ma as Flying Fox
*Yeung Chi-hing as Poison Dragon
*Hoh Ban as Golden Dragon Branch chief
*Lau Gong as Li Wan
*Cheng Miu as Cao Tien-lung
*Tang Ti as Caos brother
*Ku Feng as Chang Shun
*Nam Wai-lit as Iron Face Sheng Yongs brother
*Lam Kau as Golden Dragon Branch leader
*Lau Kar-leung as Golden Dragon Branch leader Mars as Chang Shuns son
*Bak Yu as Chang Shuns wife
*Wang Kuang-yu as Lin Qian
*Cliff Lok as Fang Ying
*Tung Choi-bo as Revenger after Golden Swallow
*Ng Yuen-fan as Golden Dragon Five Heroes (in red)
*Lau Kar-wing as Golden Dragon Five Heroes (in black)
*Yuen Cheung-yan as Golden Dragon Five Heroes (in white)
*Yuen Woo-ping as Golden Dragon Five Heroes (in green)
*Hsu Hsia as Golden Dragon Five Heroes (in brown)
*Hung Ling-ling as Prostitute
*David Chiang as Brothel clerk
*Wong Ching-wan as Courtesan with Cao Tien-lung
*Yee Kwan as Waiter
*Chui Chung-hok as Golden Dragon thug in restaurant
*Tong Gai as Golden Dragon thug
*Wong Chung as Golden Dragon thug
*Yen Shi-kwan as Golden Dragon thug
*Chan Chuen as Golden Dragon thug Lo Wai as Golden Dragon thug
*Yeung Pak-chan as Golden Dragon thug
*Wong Ching
*Fung Hap-so
*Law Kei

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 

 