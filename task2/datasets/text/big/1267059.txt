The Sea Inside
{{Infobox film
| name         = The Sea Inside
| image        = Mar adentro poster.jpg
| caption      = Theatrical poster by Toni Galingo.
| director     = Alejandro Amenábar
| producer     = Alejandro Amenábar Fernando Bovaira
| writer  = Alejandro Amenábar Mateo Gil
| starring     = Javier Bardem Belén Rueda Lola Dueñas Mabel Rivera Celso Bugallo Clara Segura Joan Dalmau Alberto Jiménez Tamar Novas
| music        = Alejandro Amenábar
| cinematography = Javier Aguirresarobe
| editing       = Alejandro Amenábar
| distributor  = Fine Line Features
| released     =  
| runtime      = 125 minutes
| country      = Spain France Italy Galician Catalan Catalan
| budget       = €10 million
| gross        = $38,535,221
}} Spanish film quadriplegic after right to end his life.

==Plot summary==
  Cadasil syndrome, who supports his cause, and Rosa, a local woman who wants to convince him that life is worth living. Through the gift of his love, these two women are inspired to accomplish things they never previously thought possible.

==Cast==
===Sampedro family===
*Javier Bardem as Ramón Sampedro
*Celso Bugallo as José Sampedro, his brother
*Mabel Rivera as Manuela, Josés wife and Ramóns caregiver
*Tamar Novas as Javier Sampedro, Ramóns nephew
*Joan Dalmau as Joaquín Sampedro, Ramón and Josés father

===Ramons friends===
*Belén Rueda as Julia
*Alberto Jiménez as Germán, her husband
*Lola Dueñas as Rosa
*Nicolás Fernández Luna as Cristian, Rosas elder son
*Raúl Lavisier as Samuel, her younger son
*Clara Segura as Gené
*Francesc Garrido as Marc, her husband

===Others===
*Josep Maria Pou as Padre Francisco, also a quadriplegic
*Alberto Amarilla as Hermano Andrés
*Andrea Occhipinti as Santiago
*Federico Pérez Rey as Conductor (Driver)
*Xosé Manuel Olveira as Juez 1 (Judge 1)
*César Cambeiro as Juez 2
*Xosé Manuel Esperanto as Periodista 1 (Reporter 1)
*Yolanda Muiños as Periodista 2
*Adolfo Obregón as Ejecutivo (Executive)
*José Luis Rodríguez as Presentador (TV host)
*Julio Jordán as Encuadernador (Bookbinder)
*Juan Manuel Vidal as Amigo Ramón (Ramóns friend)
*Marta Larralde as Muchacha en la playa (Girl on beach)

==Reception==
The film received positive reviews from critics. It currently holds an 84% rating on Rotten Tomatoes based on 131 reviews, with an average rating of 7.6/10. Its summary states: "Held aloft by a transfixing performance from Javier Bardem as a terminally ill man who chooses to die, The Sea Inside transcends its melodramatic story with tenderness and grace."  

==Awards== Oscar for Best Foreign Golden Globe for Best Foreign Language Film, and 14 Goya Awards including awards for Best Film, Best Director, Best Lead Actor, Best Lead Actress, Best Supporting Actor, Best Supporting Actress and Best Original Screenplay.  

*Academy Awards:
**Best Foreign Language Film (winner) Manolo García, nominee)
*Belgian Syndicate of Cinema Critics Grand Prix (nominee)
*Broadcast Film Critics Association:
**Best Actor (Javier Bardem, nominee)
**Best Foreign Language Film (winner)
*Cinema Writers Circle:
**Best Actor (Javier Bardem, winner)
**Best Cinematography (Javier Aguirresarobe, winner)
**Best Director (Alejandro Amenábar, nominee)
**Best Editing (Alejandro Amenábar, nominee)
**Best Film (nominee)
**Best New Artist (Belén Rueda, winner)
**Best Score (Alejandro Amenábar)
**Best Screenplay - Original (Alejandro Amenábar and Mateo Gil, nominee)
**Best Supporting Actor (Celso Bugallo, nominee)
**Best Supporting Actress (Lola Dueñas, winner)
**Best Supporting Actress (Mabel Rivera, nominee)
*César Awards:
**Best Foreign Film (nominee)
*European Film Awards:
**Best Actor (Javier Bardem, winner)
**Best Cinematographer (Javier Aguirresarobe, nominee)
**Best Director (Alejandro Amenábar, winner)
**Best Film (nominee)
**Best Screenwriter (Alejandro Amenábar and Mateo Gil, nominee)
*Film Critics Circle of Australia:
**Best Foreign Language Film (winner)
*Golden Globe Awards:
**Best Actor - Motion Picture Drama (Javier Bardem, nominee)
**Best Foreign Language Film (winner)
*Goya Awards:
**Best Actor (Javier Bardem, winner)
**Best Actress (Lola Dueñas, winner)
**Best Cinematography (Javier Aguirresarobe, winner)
**Best Director (Alejandro Amenábar, winner)
**Best Film (winner) Manolo García, winner)
**Best New Actor (Tamar Novas, winner)
**Best New Actress (Belén Rueda, winner)
**Best Original Score (Alejandro Amenábar, winner)
**Best Production Design (Benjamín Fernández, nominee)
**Best Production Supervision (Emiliano Otegui, winner)
**Best Screenplay - Original (Alejandro Amenábar and Mateo Gil, winner)
**Best Sound (Juan Ferro, Alfonso Raposo, María Steinberg, Ricardo Steinberg, winner)
**Best Supporting Actor (Celso Bugallo, winner)
**Best Supporting Actress (Mabel Rivera, winner)
*Independent Spirit Awards:
**Best Foreign Film (winner)
*London Film Critics Circle:
**Best Foreign Language Film (nominee)
*National Board of Review:
**Best Foreign Language Film (nominee)
*San Diego Film Critics Society:
**Best Foreign Language Film (nominee)
*Sant Jordi Awards:
**Best Film (Alejandro Amenábar, nominee)
**Best Spanish Actor (Javier Bardem, nominee)
**Best Spanish Actress (Mabel Rivera, nominee)
*Satellite Awards:
**Best Actor - Motion Picture Drama (Javier Bardem, nominee)
**Best Foreign Film (winner)
*Spanish Music Awards:
**Best Score (Alejandro Amenábar, winner)
*Venice Film Festival:
**Grand Special Jury Prize (Alejandro Amenábar, winner)
**Golden Lion (nominee)
**Volpi Cup Best Actor (Javier Bardem, winner)
**Best International Film (winner)
*World Soundtrack Awards:
**Best Original Soundtrack of the Year (Alejandro Amenábar, nominee)

==In popular culture==
In 2010, New Zealand post-punk band Body 125 released a song entitled A Sea Inside written in response to the film.

The 2010 Bollywood film Guzaarish featuring Hrithik Roshan and directed by Sanjay Leela Bhansali was an adaptation of The Sea Inside.

==See also==
*Guzaarish (2010)

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 