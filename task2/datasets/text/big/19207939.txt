The Unstoppable Man
The 1960 British Cameron Mitchell, Harry H. Corbett, Marius Goring and Lois Maxwell.

==Plot==
A gang of criminals kidnaps the son of James Kennedy, who is an American executive of a London-based chemical company.

Kennedy ignores the advice of Inspector Hazelrigg of Scotland Yard to try a plan of his own. He doubles the ransom amount, expecting the thieves to have a falling-out over how to divide it. One is indeed killed, and evidence at the crime scene leads Kennedy to a home in Hamstead where the mastermind, Feist, is keeping Kennedys son.

Hazelrigg comes along, but agrees to give Kennedy a few minutes to enter the house alone. Armed with a flamethrower, Kennedy is able to take his son to safety while the police close in on Feist.

==Cast== Cameron Mitchell as James Kennedy
* Marius Goring as Inspector Hazelrigg
* Harry H. Corbett as Feist
* Lois Maxwell as Helen Kennedy
* Denis Gilmore as Jimmy Kennedy
* Humphrey Lestocq as Sergeant Plummer
* Ann Sears as Pat Delaney
* Timothy Bateson as Rocky
* Kenneth Cope as Benny
* Brian Rawlinson as Moonlight Jackson

==External links==
* 

 
 
 
 
 
 
 


 
 