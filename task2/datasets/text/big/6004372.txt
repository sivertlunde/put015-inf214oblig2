Grace of My Heart
 
 
{{Infobox film
| name           = Grace of My Heart
| image          = Graceheartposter.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Allison Anders
| producer       = Ruth Charny Daniel Hassid Martin Scorsese
| writer         = Allison Anders
| narrator       =
| starring       = Illeana Douglas Matt Dillon Eric Stoltz and John Turturro
| music          = Larry Klein
| cinematography = Jean-Yves Escoffier
| editing        = James Y. Kwei Harvey Rosenstock Thelma Schoonmaker
| distributor    = Gramercy Pictures
| released       =  
| runtime        = 116 minutes
| country        = United States English
| budget         = United States dollar|$5,000,000 (estimated)
| gross          = $617,632 (USA)
}}
Grace of My Heart is a 1996 film written and directed by Allison Anders, set in the pop music world, starting in New Yorks Brill Building early 1960s era, weaving through the California Sound of the mid 60s and culminating with the Adult contemporary music|adult-contemporary scene of the early 1970s.

The plot follows the life and career trajectory of its protagonist, Denise Waverly. The soundtrack features songs by artists  Burt Bacharach, Elvis Costello, Joni Mitchell, and Jill Sobule, replicating the musical style that emerged from the Brill Building, New Yorks  music factory in the heyday of girl groups and "pre-fab" acts like The Monkees.

==Plot== Chestnut Hill, Philadelphia, who wants to be a singer and enters a talent contest. She plans to sing "Youll Never Walk Alone (song)|Youll Never Walk Alone," until, backstage, she meets a blues singer named Doris Shelley (Warren) who is belting out "The Blues Aint Nothin (But a Woman Cryin for her Man)." Doris advises Edna to follow her heart, so Edna sings "Hey There" instead and wins the contest. She uses her own money to record a demo of her first original song, "In Another World". Record producer Joel Milner (John Turturro) likes the demo but explains that he cannot market a girl singer-songwriter. He becomes her agent, renames her "Denise Waverly" and invents a blue-collar persona for her.  Milner reworks her song for a male doo-wop group, the Stylettes, and the song becomes a hit.

Denise moves to New York and becomes a songwriter in the Brill Building. She worries that she cannot be write a follow-up to "In Another World," but Milner encourages her to look at the world around her. She meets songwriter Howard Caszatt (Stoltz), and after a difficult first encounter they become professionally and romantically involved. She then meets Doris, an unsuccessful young singer, and persuades Milner to audition Doris and her group. Milner likes the group and the song Denise has written, and renames them the Luminaries.

The group has some success with their record, and disc jockey John Murray (Bruce Davison) credits Denise with "sparking the craze for girl groups." Denise and Howard write a song about working class black girls in New York City. Denise then suggests that she and Howard should write a wedding-themed song for the Luminaries. Howard refuses, but when Denise reveals that she is pregnant with Howards child - and is herself an heiress - they are married and have a daughter. However, Howard begins flirting with Cheryl Steed (Patsy Kensit), a newly hired English songwriter.

Joel asks Cheryl and her husband Matthew (Chris Isaak) to write a song for the Luminaries. Their song becomes a hit. Howard, annoyed, concedes that Denises instincts were right. Then Joel asks Denise and Cheryl to collaborate on a song for closeted lesbian ingenue singer Kelly Porter (Bridget Fonda). Denise agrees, though she dislikes Cheryl. Denise arrives home unexpectedly and finds Howard in bed with another woman, she takes their child  to the studio and tells Cheryl what happened.  Cheryl comforts Denise and the two become friends. She learns that she is pregnant with Howards second baby; Cheryl convinces her to see an obstetrician, who safely performs an illegal abortion.

Denise throws herself into her work and becomes highly successful.  Having broken up with Howard, she has a brief but unhappy affair with the married John Murray, which ends when he moves with his family to Chicago.

With the British Invasion, the Brill Building songwriting machine has become obsolete. Milner tells Denise she should not be sad, because she forced him to take chances he would have never tackled alone. He finally allows her to become a singer, and introduces her to Jay Phillips (Matt Dillon), the singer, songwriter and producer of a popular surf-rock group. Denise initially hates Jays music, and he becomes her producer. She writes and sings "God Give Me Strength," and is delighted when JAY gives the song a skilful orchestral arrangement. Her record bombs, and Denise blames herself for making the song too personal. Denise and Jay become a couple and resettle in California at the height of the hippie movement. Cheryl is songwriting in Los Angeles. She and Denise collaborate on songs for a Bubblegum pop TV show called Where the Action Is.
 Palm Springs  and adopts yet another father-figure in the communes guru.

Joel Milner visits Denise at the commune and takes her and the children to dinner. That night, he criticises her reliance on men for guidance and her failure to be responsible for her talent. Denise  angrily lashes out, and she tells Milner that he is a "fucking leech" who exploited her. He agrees, and the more he agrees with her the angrier she becomes, then he provokes her by throwing his drink in her face. She strikes him then collapses in tears, grieving for Jay. Milner consoles her and the two are reconciled.

In the closing sequence, Denise is confidently producing and recording her first solo album Grace of My Heart with her extended family and friends in attendance.

==Cast==
*Illeana Douglas as Edna Buxton/Denise Waverly
*Matt Dillon as Jay Phillips
*Eric Stoltz as Howard Caszatt 
*John Turturro as Joel Milner 
*Patsy Kensit as Cheryl Steed 
*Bruce Davison as John Murray 
*Jennifer Leigh Warren as Doris Shelley 
*Bridget Fonda as Kelly Porter
*Chris Isaak as Matthew Lewis
*David Clennon as Dr. Jonesy Jones
*Lucinda Jenney as Marion
*Christina Pickles as Mrs. Buxton
*Lynne Adams as Kindly Nurse
*Richard Schiff as Audition Record Producer
*Peter Fonda as Guru Dave (voice only)

==Closing credits==
Over the credits, Burt Bacharach and Elvis Costello are seen singing and playing their orchestrated co-penned work "God Give Me Strength", which received greater hit status in the real world than it did in the movie. Over the ensuing two years, Burt Bacharach and Elvis Costello expanded their collaboration to record an album Painted from Memory, which was covered successfully by jazz guitarist Bill Frisell.

Released in 1999 on Decca Records, The Sweetest Punch consisted of jazz arrangements of the Painted From Memory songs done by Frisell and his studio group, featured vocals by Costello on two songs, and jazz singer Cassandra Wilson on two songs, one being a duet employing both singers.

==Music==
Though actress Illeana Douglas apparently sings throughout the movie, her singing was dubbed by singer Kristen Vigard, notable as first girl to portray  Annie (musical)|Annie in the 1976 workshop production before going to Broadway the following year.

In the beginning, Edna/Denise performs a version of "Hey There," which was originally heard in the musical The Pajama Game, and popularized by singers such as Rosemary Clooney. Another of Denises big musical moments occurs in the studio to sing tracks for "God Give Me Strength," an expensively produced single that fails to generate excitement on the charts, alluding to Spectors recording of "River Deep, Mountain High" for Tina Turner (written by Spector, Greenwich and Barry). Singer Elvis Costello, who co-wrote "God Give Me Strength" (with Burt Bacharach) for the film, also wrote "Unwanted Number," which, in the movie, is crafted by Denise and Cazsatt for The Luminaries. The song causes a scandal because it tells a sympathetic story of an unmarried pregnant preteen — bold for the early 60s, though comparable to similarly real-life songs of the era such as "He Hit Me (It Felt Like A Kiss)" about Little Eva, the Goffin-Kings babysitter who was being beaten by her boyfriend at the time.

===Soundtrack CD===

====Exclusions====
Although Grace of My Heart has many musical sequences, the selections were pared down for the soundtrack CD. For instance, the fictional Luminaries, dubbed by girl group  , nephews of Andy Williams perform two songs in the film, Heartbreak Kid and Love Doesnt Ever Fail Us, but only the latter song is on the soundtrack disc. Both Kristen Vigards renditions of Hey There  in the contest version and the polished demo are excluded from the CD, and her In Another World is jettisoned in favor of the fictional Stylettes rendition (via Portrait (group)|Portrait). Vigards performance of God Give Me Strength is  not on the soundtrack; instead the Elvis Costello/Burt Bacharach performance is seen and heard.

====Inclusions====
Also on the CD, Jill Sobule sings the countrified waltz "Truth Is You Lied," complete with easy listening-style background chorus reminiscent of The Anita Kerr Singers.

=====Joni Mitchell=====
"Man From Mars" was written by Joni Mitchell, and the song appears on the CD with Kristen Vigard singing the vocal from the film (dubbing Illeana Douglass performance). A version of the song featuring Joni Mitchells vocal, backed with the same music track, was on the initial pressing of 40,000 soundtrack CD copies. This CD version was recalled and the soundtrack was re-released a week later with Kristen Vigards vocal, as heard in the movie.  Mitchell later re-recorded the song with different-styled music for her 1998 album Taming the Tiger. The Mitchell version of "Man from Mars" from Grace of My Heart is very hard to come by.

The soundtrack was produced by Larry Klein, who had been Joni Mitchells husband and producer but divorced her prior to making this soundtrack. He contributed to the writing of several songs on the soundtrack and appears briefly several times in the movie as a recording engineer.

==Additional credits== The Aviator, and The Departed. Francois Sequin is the production designer, and the costumes are by Susan Bertram. The cast is rounded out by Lynne Adams, Peter Fonda, Chris Isaak, Lucinda Jenney, Patsy Kensit, Christina Pickles and Richard Schiff.

==Impact==
The film was released in the fall of 1996, just ahead of Oscar winning actor Tom Hanks directorial debut That Thing You Do!, which likewise covered the early to mid-1960s pop music scene and featured original, retro-styled songs on the soundtrack.

Grace of My Heart was Anderss fourth feature film, and followed her Border Radio (1987), Gas Food Lodging (1992),  and Mi Vida Loca (1993).

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 