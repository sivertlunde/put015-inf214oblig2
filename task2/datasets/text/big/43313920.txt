The Captain from Köpenick (1941 film)
 
{{Infobox film
| name           = The Captain from Köpenick
| image          = 
| caption        = 
| director       = Richard Oswald
| producer       = John Hall
| writer         = {{plainlist|
* Albright Joseph
* Ivan Goff
}}
| based on       =  
| starring       = {{plainlist|
* Albert Bassermann
* Mary Brian
* Eric Blore
* Herman Bing
}} Daniel Amfitheatrof
| cinematography = John Alton
| editing        = Dorothy Spencer Talisman Studios
| distributor    = Film Classics, Inc. and Producers Releasing Corporation
| released       =   
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = U$350,000 
| gross          =
}} The Captain Captain of Köpenick.

==Plot==
 
 
Shoemaker Wilhelm Voigt is released from prison after many years of hard labor. His freedom is new to him and, as he tries to navigate this strange new world, he promptly finds himself in the midst of a Prussian Catch-22 (logic)|catch-22: To get a residence permit (passport), he must have a job, but he can only get a job if he has a residence permit. No one in the Prussian-German bureaucracy feels compelled to help him, everything must go by the book. To escape this vicious circle, out of desperation Voigt breaks into a police station to forge the much needed permit.

Unfortunately he gets caught and again has to spend many more years behind bars. The prisons warden subjects the prisoners to the whims of his militarism. The warden loves everything military and has the prisoners re-enact famous battles. When Voigt is released, he still doesnt have his permit, but now he has a deep knowledge of military uniforms, military ranks and military speak that he can use to his advantage. In Berlin he buys and wears a used captains uniform, then marches towards a platoon of soldiers standing guard and commands them to immediately follow him to Köpenick, a suburb of Berlin. He is so convincing that they actually do! When they arrive, he has the soldiers stage a coup-like takeover of the Town Hall so he can commandeer his much sought-after permit, but is informed by the staff the permits are now only issued in Berlin. After he pockets all of the cash in the municipal treasury, he orders his soldiers to take the train back to their original posts in Berlin and then absconds with the cash.
 Koepenick Caper, he goes to the Chief of Police, confesses and returns all the money.  The police in the station all erupt in fits of laughter, offer him drinks and congratulate him for the best practical joke they have ever heard of. Voigt is now famous and even the Kaiser wants to hear his story.

== Cast ==
* Albert Bassermann as shoemaker Wilhelm Voigt
* Mary Brian as Mrs. Obermueller
* Eric Blore as Mayor Obermueller
* Herman Bing as City Hall guardian Kilian
*George Chandler as Kallenberg
* Luis Alberni as prison guard
* Wallis Clark as Friedrich Hoprecht Else Bassermann as Mrs. Marie Hoprecht

==Production==
The American Film Institute quotes PMs Weekly as citing the budget at $350,000. 
 Oswald is attempting to make the film exactly as before ... now none of his German comics can open their mouths without uttering something with a political meaning.  They say what they said in 1931, but their every utterance is fraught with ideals. Still, Albert Bassermann plays a captain in uniform magnificently. John Hall is producing the picture and it better be good.  Else John Hall, himself, in person, stands to lose a sizeable chunk of his money.  So do Messrs. Bassermann, Oswald, et al." ... "You may never see The Captain Of Koepenick, but we hope its good enough that you will. The manufacturers compose a brave crew."  

==Release==
In his biography of Oswald,   in the leading role, is of any relevance. As U.S. audiences had a hard time relating to themes of Prussian militarism, subservience and lack of democracy, the film went without a distributior for many years and only premiered at the beginning of 1945". 

The Captain from Koepenick was screened at the Syracuse Cinefest on 14 March 2013 as Passport to Heaven. 

==Critical reception== Bassermann is angry, always rebelling and defying the powers that be, channelling his own experience as a Nazi exile: A powerful performance, making this film perhaps the most German of all exile films.“ 

==See also==
* The Captain from Köpenick (1931 film)
* The Captain from Köpenick (1956 film)

==References==
 

== External links ==
*  
*  
*  
*  at  

 
 
 
 
 
 
 
 
 
 
 