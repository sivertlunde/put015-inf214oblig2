The Running Man (1987 film)
 Running Man}}
{{Infobox film
| name           = The Running Man
| image          = Running Man Theatrical Poster.jpg
| caption        = Theatrical release poster
| director       = Paul Michael Glaser
| producer       = George Linder Tim Zinnemann
| based on       =  
| screenplay     = Steven E. de Souza
| starring       = {{Plain list | 
* Arnold Schwarzenegger
* Richard Dawson
* María Conchita Alonso
* Yaphet Kotto
}}
| music          = Harold Faltermeyer
| cinematography = Thomas Del Ruth
| editing        = {{Plain list | John Wright Mark Warner
* Edward A. Warschilka
}}
| studio         = Republic Pictures
| distributor    = TriStar Pictures
| released       =  
| runtime        = 101 minutes
| language       = English
| country        = United States
| budget         = $27 million
| gross          = $38,122,105
}}
 science fiction The Running Andrew Davis was fired one week into filming and replaced by Glaser. Schwarzenegger has stated this was a "terrible decision" as Glaser "shot the movie like it was a television show, losing all the deeper themes." Schwarzenegger believes this hurt the movie.  Paula Abdul is credited with the choreography of the Running Man dance troupe. 

The film, set in a dystopian America between 2017 and 2019, is about a television show called The Running Man, where convicted criminal "runners" must escape death at the hands of professional killers.

==Plot== totalitarian police state, censoring all cultural activity. The government pacifies the populace by broadcasting game shows where convicted criminals fight for their lives, including the gladiator-style The Running Man, hosted by the ruthless Damon Killian, where "runners" attempt to evade "stalkers" and near-certain death for a chance to be pardoned.

In 2019, Ben Richards, a police pilot wrongly convicted of a massacre, escapes from a labor camp and seeks shelter at his brothers apartment. He finds it is now occupied by Amber Mendez, a composer for ICS, the network that broadcasts The Running Man. Richards asks Mendez about the whereabouts of his brother, and she says that he was taken for "reeducation," possibly hinting at his fate.

Taking Amber hostage, Richards attempts to flee to Hawaii, but she alerts airport security and Richards is captured and taken to ICS.  There, Killian tries to convince him to participate in The Running Man, saying that if he refuses, William Laughlin and Harold Weiss, members of a resistance movement that Richards had met, will be forced to participate instead.  Reluctantly, Richards agrees, but learns that Killian had enrolled Laughlin and Weiss as runners anyway.
 uplink facilities, which they realize are in the game zone. Amber sees a falsified news report on Richards capture and, suspicious of the medias Veracity (ethics)|veracity, does some investigating. She learns the truth about the massacre of which Richards was convicted, but is captured and sent into the game zone.  

The runners split up, each pair pursued by a different stalker. "Buzzsaw" critically wounds Laughlin and is killed by Richards.  Weiss and Amber locate the uplink and learn the access codes, but "Dynamo" finds them and electrocutes Weiss. Ambers screams lead Richards to her, and as the two evade Dynamo, the stalkers buggy flips, trapping him inside. Refusing to kill a helpless man, Richards leaves Dynamo alive as the studio and home audiences watch. He and Amber return to Laughlin who, before dying, says that the resistance has a hideout within the game zone.

Back at ICS, Killian sees Richards popularity growing, with viewers betting on him to win instead of the stalkers. Off-camera, Killian tries to offer Richards a job as a stalker, but when Richards refuses, Killian sends the next stalker, "Fireball," after him and Amber.  Fireball chases them into an abandoned factory, where Amber discovers that the games previous "winners" were actually killed, never having truly won in the first place.  Fireball goes after Amber, but Richards rescues her and kills him.  

Frustrated and running out of options, Killian fakes the deaths of Richards and Amber at the hands of "Captain Freedom," a retired stalker who has refused to go after them. In the game zone, Richards and Amber are captured and taken to the resistances hideout, where they learn of their "deaths." Using the access codes, the rebels get into ICS control room, broadcasting footage that exonerates Richards and reveals the truth about the games previous "winners." As Richards heads to the main studio floor, shocking the audience who had watched him supposedly die, Amber fights and kills Dynamo, the last remaining stalker. 

Richards confronts Killian, who – after his bodyguard, Sven, decides hes taken enough abuse from Killian and leaves the studio – says he created the show to appease Americas love of reality television and televised violence. In response, Richards decides to give the audience what Killian says they want by sending him to the game zone in a rocket sled.  The sled hits a billboard and explodes, killing Killian to the delight of the audience. Richards and Amber share a kiss as they walk out of the studio.

==Cast==
* Arnold Schwarzenegger as Ben Richards.
* María Conchita Alonso as Amber Mendez.
* Richard Dawson as Damon Killian.
* Yaphet Kotto as William Laughlin, a resistance member who allies himself with Richards.
* Marvin J. McIntyre as Harold Weiss, a resistance member who also joins Richards.
* Mick Fleetwood as Mic, the secret leader of the resistance. Professor Toru Tanaka as Professor Subzero, a stalker who uses a hockey-stick capable of cutting steel, exploding pucks, and a hockey rink zone specially fitted for him.
* Gus Rethwisch as Eddie "Buzzsaw" Vatowski, a stalker who uses a specially reinforced chainsaw and a motorcycle.
* Jesse Ventura as Captain Freedom, a ten-time former Running Man stalker champion.
* Jim Brown as Fireball, a stalker who is armed with a flamethrower and jetpack. arc electricity.
* Dweezil Zappa as Stevie, one of Mics men.
* Kurt Fuller as Tony, a talent agent for The Running Man.
* Rodger Bumpass as Phil, the studio announcer for The Running Man.
* Sven-Ole Thorsen as Sven, Killians head of security.

==Reception==
 
Although most critics praised Richard Dawsons performance as Killian, overall critical reaction to the film was mixed. Review aggregation website Rotten Tomatoes gives the film a score of 61% based on reviews from 33 critics, with an average score of 5.5/10.   

 
In The Running Mans opening weekend, it was released in 1,692 theaters and grossed $8,117,465.  The films total domestic gross was $38,122,105.   

==Video game==
A video game based on the film was released for the MSX,  ZX Spectrum,  Commodore 64,  Amstrad CPC,  Amiga, and Atari ST.  The game was developed by Emerald Software Ltd and published by Grandslam Entertainment. The 1990 video game Smash TV was inspired by The Running Man.

== See also ==
* The 10th Victim
* Das Millionenspiel – German TV movie from 1970
* List of American films of 1987
* Le Prix du Danger, French movie from 1983
* The Prize of Peril, short story by Robert Sheckley published in 1958
* Ta fête, 2014 song and music video By Belgian singer Stromae

==References==
 

==External links==
 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 