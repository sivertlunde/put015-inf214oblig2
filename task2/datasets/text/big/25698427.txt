Letters to Juliet
{{Infobox film
| name           = Letters to Juliet
| image          = Letters to juliet poster.jpg
| caption        = Theatrical release poster
| director       = Gary Winick
| producer       =  
| writer         =  
| starring       =   Andrea Guerra
| cinematography = Marco Pontecorvo
| editing        = Bill Pankow
| distributor    = Summit Entertainment
| released       =  
| runtime        = 105 minutes
| country        = United States Italy
| language       = English, Italian
| budget         = $30 million 
| gross          = $79,181,750   
}}

Letters to Juliet is a 2010 American romantic drama film starring Amanda Seyfried, Christopher Egan, Vanessa Redgrave, Gael García Bernal, and Franco Nero. This was the final film of director Gary Winick. The film was released theatrically in North America and other countries on May 14, 2010. The idea for the film was inspired by the 2006 non-fiction book, Letters to Juliet, by Lise Friedman and Ceil Friedman, which chronicles the phenomenon of letter writing to Shakespeares most famous romantic heroine.

== Plot ==
Sophie (Amanda Seyfried) is a young American woman who works for The New Yorker as a fact checker. She goes on a pre-honeymoon with her chef fiancé Victor (Gael García Bernal) to Verona, Italy. Victor is unmoved by the romance of Italy and uses his time to research his soon-to-open restaurant, often neglecting Sophie. Sophie accidentally discovers an unanswered "letter to Juliet" by a Claire Smith from 1957, one of thousands of missives left at the fictional lovers Verona courtyard that are typically answered by the "secretaries of Juliet". She answers it and within a week the now elderly Claire (Vanessa Redgrave) arrives in Verona with her handsome barrister grandson Charlie (Christopher Egan). Claire and Sophie take an instant liking to each other, but Charlie and Sophie do not get along.

Following the advice in Sophies reply, Claire decides to look for her long-lost love, Lorenzo Bartolini (Franco Nero). Sophie, thinking Claires story might help her with her writing career, helps Claire. The two find out that there are multiple Lorenzo Bartolinis living in the area. After many days of searching for the right Lorenzo, they find that one is dead. Charlie blames Sophie for his grandmothers sadness. He accuses her of not knowing what real loss is. Claire, witnessing the dispute, tells Charlie he was wrong and Sophies mother had walked away from her when she was a little girl. The next day, Claire insists that Charlie apologize to Sophie at breakfast, which he does. After dinner, Sophie goes out with Charlie and talks to him about love, and the two kiss. The next morning is their last day of searching for Lorenzo. On a whim, Claire points out a vineyard to Charlie and asks if he could stop so they can have a farewell drink for Sophie. As Charlie drives down the road, Claire sees a young man who looks exactly like her Lorenzo. They discover the man is Lorenzo Bartolinis grandson, and Claire and Lorenzo reunite.

Back in New York, Sophie breaks up with Victor before returning to Verona to attend Claire and Lorenzos wedding. She finds Charlie with another woman, Patricia, and runs out crying. Charlie comes out to find her, and she admits she loves him but tells him to go back to Patricia. Charlie tells Sophie that the woman is his cousin Patricia and tells Sophie he loves her. He climbs up the vine to the balcony but accidentally falls down, and they kiss as he lies on the ground.

== Cast ==
* Amanda Seyfried as Sophie Hall, a fact checker living in New York. 
* Christopher Egan as Charlie Wyman, Claires grandson, who has troubles coming to terms with his grandmother loving anyone other than his late grandfather.
* Vanessa Redgrave as Claire Smith-Wyman, the girl who wrote the letter to Juliet 50 years before, and is hoping to find her Lorenzo.
* Franco Nero as Lorenzo Bartolini, Claires love interest. Nero is Redgraves real life husband. Roger Ebert, having interviewed both Nero and Redgrave on the set of Camelot (film)|Camelot, noted how much of the love story between their characters is nearly autobiographical.   
* Gael García Bernal as Victor, Sophies chef fiancé who is easily preoccupied with anything having to do with food, cooking, and the opening of his restaurant.
* Luisa Ranieri as Isabella, the most important of the four original Club di Giulietta|Juliets secretaries in the film and a friend of Sophies.
* Marina Massironi as Francesca, one of Juliets secretaries.
* Lidia Biondi as Donatella, one of Juliets secretaries.
* Milena Vukotic as Maria, one of Juliets secretaries.
* Oliver Platt as Bobby, the editor of The New Yorker who wants Sophie to remain a fact-checker.
* Daniel Baldock as Lorenzo Jr., the older of Lorenzos sons.
* Stefano Guerrini as Lorenzo III, grandson of Lorenzo.
* Ashley Lilley as Patricia, Charlies cousin who has the same name as his ex-girlfriend.
* Luisa De Santis as Angelina, Isabellas mother.

== Release and reception ==

=== Critical reception ===
Letters to Juliet received mixed reviews from critics. Review aggregate Rotten Tomatoes reports that 40% of critics have given the film a positive review based on 146 reviews, with an average score of 5.1/10. Metacritic gave it an average score of 50 out of 100 from the 34 reviews it collected.

=== Box office === Robin Hood.  In its second weekend, the film dropped 33.5% with $9,006,266 into #4.  The film eventually grossed $53,032,453 domestically and $79,181,750 worldwide. 

== Soundtrack Lists == You Got Me" – Colbie Caillat
* "Chianti Country"
* "Verona" – Andy Georges
* "Un Giorno Così" – 883
* "Per Avere Te" – Franco Morselli Chris Mann
* "Variations On A Theme By Mozart" – from The Magic Flute, Opus 9 Pacifico
* "Per Dimenticare" – Zero Assoluto
* "Sono Bugiarda (Im A liar)" – Caterina Caselli
* "Guarda Che Luna" – Fred Buscaglione Love Story" – Taylor Swift What If" – Colbie Caillat

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 