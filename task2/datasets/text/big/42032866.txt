Tamu Agung
{{Infobox film
| name           = Tamu Agung
| image          = Tamu Agung Sewindu Perfini p5.jpg
| alt            = 
| caption        = Film still
| director       = Usmar Ismail
| producer       = Usmar Ismail
| writer         = 

| starring       = {{plainlist|
*Cassin Abbas
*Nina Amora
*M. Pandji Anom
}}
| music          = Sjaiful Bachri
| cinematography = Max Tera
| editing        = Soemardjono 
| studio         =Perfini
| distributor    =
| released       =   
| runtime        = 
| country        = Indonesia
| language       = Indonesian
| budget         = 
| gross          = 
}}
Tamu Agung (Exalted Guest) is a 1955 Indonesian dramatic comedy film directed by Usmar Ismail. It stars Cassin Abbas, Nina Amora, M. Pandji Anom, and Chitra Dewi.  The satirical political comedy, about the anticipation of the visit of a dignitary to a small isolated village in East Java, was critically acclaimed, although was disliked by the government. The film, produced under the Perfini banner, was shot by cinematographer Max Tera.

==Plot==
The village of Sukaslamet is scheduled to receive an honoured guest from afar. Midi, a villager, is sent to the city to escort him. Distracted and confused by the lively city, Midi comes across a salesman offering a "potent" hair-growing tonic, which&nbsp;– unknown to Midi&nbsp;– is actually ineffective. To speed the onset of the tonics effects, Midi arranges for a car to take him and the salesman back to Sukaslamet. There, the salesman is welcomed as the honored guest, and he takes advantage of the warm welcome. However, his fun is cut short when the actual guest arrives in the village, and the salesman is driven out of the village.   

==Production==
Tamu Agung was directed by Usmar Ismail for his company Perfini; Misbach Yusa Biran served as assistant director. Cinematography of this black-and-white film was completed by Max Tera, with R Husein as a second cameraman. Artistic direction was handled by R Hibnu DJ, Ardi Ahmad, and Djajeng Winoto, with make-up by Hanida Arifin. Music was provided by Sjaiful Bachri. Soemardjono served as editor, with Janis Badar as his assistant.   

The film starred Cassin Abbas, Nina Amora, M. Pandji Anom, Chitra Dewi, Kuntjung, Tina Melinda, Hassan Sanusi, Sulastri, and Udjang. 

==Reception==
Tamu Agung was a commercial failure upon release on 21 May 1955, making only Rp. 147,301 during its run, the lowest of any Perfini production to that point. {{cite book
 |title=10 Tahun Perfini
 |trans_title=10 Years of Perfini
 |language=Indonesian
 |editor1-last=Perfini
 |publisher=Perfini
 |location=Jakarta
 |year=1960
 |page=26
}}  However, it received considerable acclaim from critics, and although it was frowned upon by Sukarnos government, they did not ban it.     The film was screened at the 1956 Asia Film Festival in Hong Kong, winning Best Comedy.  

Monash University notes the way that the film "brilliantly interweaves modern political discourse (including a militant feminism that appears to have emerged with the Indonesian revolution) with archaic Javanese rhetoric, taken from the narratives of the Wayang (traditional Javanese shadow play)."  Author William Van der Heide considers the film to have an ideological view of development, and notes that the film delivers a scathing critique of government corruption and party politics and that the "exalted guest" is in fact a "little-disguised caricature of President Sukarno".   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 