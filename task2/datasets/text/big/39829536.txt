Chor Chor Super Chor
{{Infobox film
| name=Chor Chor Super Chor 2013
| image= Chor Chor Super Chor 2013.Jpeg
| caption= DVD Cover
| director=K. Rajesh
| producer=Ved Kataria Renu Kataria
| story=
| based on=
| screenplay=K. Rajesh Ani Thomas V Radhakrishnan
| starring=Deepak Dobriyal Anshul Kataria Priya Bathija Paru Uma Alok Chaturvedi Bramha Mishra Jagat Rawat Chandrahas Tiwari Shrikant Verma Anurag Arora Avtar Sahni Kafil Ahmad
| music=Mangesh Dhakde
| cinematography=Rakesh Haridas
| editing=
| studio=
| distributor=Multimedia Combines
| released= 
| runtime=
| country=India
| language=Hindi
| festivals=Stuttgart Indian international Film Festival 2013
}}
 2013 Hindi Comedy film directed by K. Rajesh and produced by Ved Kataria and Renu Kataria.  The film features Deepak Dobriyal, Anshul Kataria and Priya Bathija as main characters. The film released on August 2, 2013. 

==Story Line==
Small time theft and robbery is the central theme of this drama set in Delhi. Shukla (Avtar Sahni) runs a photo studio, which in reality is an undercover agency employing a collection of young petty thieves who work the streets committing petty crime.

The story revolves around Satbir (Deepak Dobriyal), one of the young thieves who wants to get out of the life of crime and become a good citizen. In his attempts to find normal employment, he stumbles across a young attractive woman Neena (Priya Bathija), whom he then attempts to court. When his friends from the agency notice his new love interest, they steal Neenas purse in the mall where Satbir is now working. Satbir gets the purse back and returns it to Neena the next time they meet. 

When Neena discovers that Satbir knows some petty thieves personally, she is very curious to meet some. She promises Satbir to spend the day with him on the streets of Delhi, if he can show her some of the action. Satbir thinks about the offer, but has misgivings about the idea of letting an outsider onto the inner workings of the petty crime scene. However, the temptation of Neenas company is too strong to resist, and he agrees. But Satbir does not know that Neena works at the local TV station as an investigative reporter.  

Neena then disappears and Satbir next sees her on the local TV channel in an ad announcing next weeks investigative report on street crime in Delhi. Much to his horror, the ad also features Satbir himself, who has been secretly recorded all day as he goes about town showing Neena various incidents of petty crime taking place. Satbir now realizes that he has been set-up by Neena and as a result of his folly, all of his friends are going to be exposed to the public on TV.

As a result, Satbir has to go into hiding, but he is soon caught by Shukla and brought to the studio, where he explains his innocence. While he is forgiven for his folly, that does not really help the cause of the gang, who now face public humiliation and likely arrest. Satbir vows to Shukla that no matter what, the TV episode will not air next week. 

Things, however, look grim for the small street gang trying to take down a nationally advertised ready-to-run episode off the air with only a week to go. But Satbir has an inspiration, that leads to a plan which revolves around creating doubt in the TV station managers mind about the authenticity of Neenas recordings. 

To make the plan work, the gang first break into the TV station and steal a copy of the recording that is going to be aired. They then cut each of the several incidents of street theft in the recording and repackage them as part of a false reality TV show called "Chor, Chor, Super Chor". After each incident, the gang (masquerading as a TV crew) is now shown going to the home of the victim and returning the stolen items along with a "surprise you are on TV" ending. So what was originally footage of several incidents of petty street theft, now resembles a series of episodes of a reality TV show where the "supposed" petty theft is followed by a visit to the victim and a happy ending. 

To make this work more effectively, the gang tracks down the manager of the TV station and robs him off his car on the street in an elaborately planned scam. Recording the event on camera all this time, they then show up outside the TV station later in the day, pretending to be a TV crew. They then proceed to return the car to the TV manager as he finally arrives to work after his long frustrating day on the streets of Delhi.

Initially, the manager is upset and confused, more so since he has never heard of the reality show "Chor, Chor, Super Chor". The crew explain that they are new on the scene and are in fact looking for a station that would want to air the show. The manager, unaware that the crew are actual petty criminals in real life, is impressed with their performance.

He invites Satbir and the crew inside. Once in his office, the crew is able to convince him that Neena is a fraud and her entire episode on street crime was supposedly their own film work which she must have stolen from them. She is accused of having cut-up their work and then passed it off to the station manager as real investigative reporting. The manager is initially skeptical of their claim, but is convinced in the end since he himself has just been the victim of the supposed shows prank. He now decides to abort the airing of the report.

The manager now fires Neena and hires the crew and their reality show instead. The show becomes a big hit across Delhi. In a bizarre and ironic twist, petty crime, instead of going down as Neena hoped, goes up across the city as people on the street become openly receptive to being robbed of their possessions, hoping the person robbing them is perhaps part of the TV show and will show up at their door later on with a prize and a chance to be on national TV.

==Cast==
*Deepak Dobriyal
*Priya Bhatija
*Anshul Kataria

*Paru Uma
*Alok Chaturvedi
*Bramha Mishra
*Jagat Rawat
*Chandrahas Tiwari
*Nitin Goel
*Shrikant Verma
*Anurag Arora
*Avtar Sahni
*Kafil ahmad

==References==
 

==External links==
*  
*  at MyPrimeNews.com

 
 
 
 
 


 