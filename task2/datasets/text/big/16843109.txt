Európa expressz
 
{{Infobox film
| title            =Európa Expressz
| image            =Europa_Expressz.jpg
| caption          =
| director         =Horváth Csaba
| producer         =
| writer           =Dóra György Horváth Csaba Kata Dobó Iván Kamarás Rátóti Zoltán Stohl András
| music            =Dobos Gyula
| cinematography   =Gurbán Miklós
| jelmeztervező    =Horváth Kati
| editing          =Eszlári Beáta
| distributor      =
| Released         =March 18, 1999 (Hungary)
| runtime          =97 mins 
| language         =Hungarian/Russian
| budget           =
| gross            =
}}
Európa Expressz (alt. Europa Express) is a Hungarian action-thriller made in 1999.

==Plot== Russian thief Hungarian train station as if it were in Nickelsdorf, Austria (the actual station filmed is in Szabadbattyán). This fools Zavarov into thinking he is in Austria, and when he exits the train, the police surround him.

==Cast==
* András Stohl - Béci Kata Dobó -  Edit
* Iván Kamarás -  Jimmy
* Tibor Szilágyi - Lieutenant Colonel Papp
* Zoltán Rátóti - Zavarov
* László Jászai Jr. - Golyó
* András Gáspár -  Kenõ
* Géza Kaszás - Second in Command
* Ödön Rubold -  Ticket inspector
* András Schlanger - Jenõke
* Ádám Rajhona -  Civilian
* Péter Végh -  Hadházy
* Dorka Gryllus - Student

==Awards== Kata Dobó). 

==References==
 

==External links==
*  

 
 
 
 
 

 
 