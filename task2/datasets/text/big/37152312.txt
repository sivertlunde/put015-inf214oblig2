Queensland (film)
{{Infobox film
| name           = Queensland
| image          =
| alt            =
| caption        = John Ruane
| producer       = Chris Fitchett
| writer         = John Ruane Ellery Ryan
| based on       =
| narrator       =
| starring       = John Flaus Robert Karl
| music          =
| cinematography = Ellery Ryan
| editing        = Mark Norfolk
| studio         = Film Noir Productions
| distributor    = the Vincent Library
| released       =  
| runtime        = 52 mins
| country        = Australia
| language       = English
| budget         = AU$12,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 302 
| gross          =
}}
Queensland is a 1976 film.

==Plot==
Doug is a factory worker living in Melbourne who dreams of moving from Melbourne to Queensland. He attempts to reconnect with an old flame, Marge, and move to Queensland together.

==Cast==
*John Flaus as Doug
*Bob Karl as Aub
*Alison Bird as Marge
*Tom Broadbridge as Mick

==Production==
John Ruane says he was inspired by a newspaper article about a slaughter man who killed his de facto wife and then got drunk for two days. He decided to remove the killing aspect, concentrate on the relationship. Ruane: Northcote version of Midnight Cowboy - not the story, but the fact that they were headed for a dream. Their dream was Miami. Our film was obviously about heading to Queensland... Its about a vanishing breed of Australians.  
The film was made with money from the Experimental Film and Television Fund while John Ruane was a film student at the Swinburne College of Technology in Melbourne. 

==Release==
The movie was released through the co-operative movement. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive
*  at Oz Movies
 

 
 

 