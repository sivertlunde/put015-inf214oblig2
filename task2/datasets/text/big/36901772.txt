The Goose Girl (1915 film)
 
{{infobox_film
| name           = The Goose Girl
| image          = The Goose Girl 1915 flyer.jpg
| imagesize      =190px
| caption        =Ad for film
| director       = Frederick A. Thomson
| producer       = Jesse Lasky
| writer         = William C. deMille (scenario)
| based on       =  
| starring       = Marguerite Clark Monroe Salisbury
| music          =
| cinematography = Percy Hilburn
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent English intertitles

}} silent drama film directed by Frederick A. Thomson and distributed by Paramount Pictures. The film is based on the 1909 novel of the same name by Harold McGrath and starred Marguerite Clark and Monroe Salisbury. 

==Cast==
* Marguerite Clark - Gretchen
* Monroe Salisbury - King Frederick
* Sydney Deane - Prince Regent of Jugendheit
* E. N. Dunbar - Grand Duke of Ehrenstein
* James Neill - Count Von Herbeck
* Lawrence Peyton - Von Wallenstein
* P. E. Peters - Carmichael 
* Horace B. Carpenter|H. B. Carpenter - Torpete The Gypsy 
* Ernest Joy - Hans
* J. M. Casidy - Gottfried
* Miss Johnson - Princess Hildegarde
* Jane Darwell - Irma

==Plot==
Count Von Herbeck (Neill), an ambitious chancellor to the Grand Duke of Ehrenstein (Dunbar), secretly marries and has a daughter. At the urging of his dying wife, the count kidnaps the dukes infant daughter (Clark) and substitutes his own in the castle so that she may live in the style of a great lady. 

The real princess, abandoned by the gypsies who abducted her for the count, is raised by peasants and given the name "Goose Girl." The young King Frederick (Salisbury) is betrothed to the impostor princess of Ehrenstein, whom he has never seen, but before the wedding takes place, he runs away and roams the countryside, where he encounters and falls in love with the Goose Girl. 

After a series of adventures, during which Frederick decides to wed the false princess for the good of the country, the Goose Girls true identity is revealed, and Frederick is delighted to learn that he is now betrothed to the girl of his heart.

==Preservation status==
This is now considered a lost film.   

==See also==
*List of lost films

==References==
 

==External links==
 
* 
* 
*  at SilentHollywood.com

 
 
 
 
 
 
 
 
 
 

 