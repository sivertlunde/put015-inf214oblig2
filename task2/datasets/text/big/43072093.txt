Rich People (film)
{{Infobox film
| name           = Rich People
| image          =
| caption        =
| director       = Edward H. Griffith E. J. Babille (assistant)
| producer       = Ralph Block
| based on       =  
| writer         = A. A. Kline
| starring       = Constance Bennett
| music          =
| cinematography = Norbert Brodine
| editing        = Charles Craft
| distributor    = Pathé Exchange
| released       =  
| runtime        = 8 reels
| country        = United States
| language       = English
}}
Rich People is a 1929 talking picture directed by Edward H. Griffith and starring Constance Bennett.  It was produced by Ralph Block and distributed through Pathé Exchange.  It is based on a short story from the March–July 1928 Good Housekeeping magazine.

==Cast==
*Constance Bennett - Connie Hayden
*Regis Toomey - Jef MacLean
*Robert Ames - Noel Nevins
*Mahlon Hamilton - Beverly Hayden
*Ilka Chase - Margery Mears John Loder - Captain Danforth
*Polly Ann Young - Sally Vanderwater

==Preservation status==
A print is preserved in the Library of Congress film collection.  

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 