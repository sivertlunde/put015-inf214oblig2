We Were Dancing (film)
{{Infobox film
| name           = We Were Dancing
| image_size     =
| image	         = 
| caption        =
| director       = Robert Z. Leonard
| producer       = Robert Z. Leonard Orville O. Dull
| writer         =  play by Noël Coward
| starring       = Norma Shearer Melvyn Douglas
| music          = Noël Coward Bronislau Kaper
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       = 1942
| runtime        = 
| country        = United States English
| budget         = $1,085,000  . 
| gross          = $1,079,000 
| preceded_by    =
| followed_by    =
}}
We Were Dancing is a 1942   play cycle, and Cowards Private Lives. It was directed by Robert Z. Leonard, written by Claudine West, Hans Rameau and George Froeschel, and starred Norma Shearer and Melvyn Douglas.  

==Synopsis==
Vicki Wilomirska, an impoverished Polish princess, falls madly in love while dancing with the charming but penniless Austrian baron Nicki Prax. She ends her engagement to a wealthy lawyer Hubert Tyler. They marry secretly but are exposed by one of Nickis ex-girlfriends, decorator Linda Wayne. The two support themselves by being professional house guests in the homes of star-struck American nouveau riche. Eventually Nicki decides to do the unthinkable and get a job. Linda pursues Nicki, and Vicki, brokenhearted, sues for divorce. Hubert represents Vicki in the case, and the two again become engaged.

Hubert builds a new home for Vicki, and Nicki turns up as a member of the firm decorating the new house (Linda helped him get the job). He begins by behaving professionally, but he eventually confesses that he loves only Vicki. She tells him that he is too late.  At the fancy betrothal party for Hubert and Vicki, Nicki comes to say goodbye. They dance together to the same waltz that had ignited their passion when they first met, and the magic returns.  They kiss and elope once more.

==Cast==
*Norma Shearer as Vicki Wilomirska, a penniless Polish princess
*Melvyn Douglas as Nicki Prax, an impoverished Baron
*Gail Patrick as Linda Wayne, a fashionable decorator Linda Wayne
*Lee Bowman as Hubert Tyler, a wealthy lawyer
*Marjorie Main as Judge Sidney Hawkes
*Reginald Owen as Major Tyler-Blane
*Alan Mowbray as Grand Duke Basil
*Florence Bates as Mrs. Vanderlip
*Heather Thatcher as Mrs. Tyler-Blane
*Connie Gilchrist as Olive Ransome
*Nella Walker as Mrs. Bentley
*Florence Shirley as Mrs. Charteris

==Reception==
According to MGM records the film made $581,000 in the US and Canada and $498,000 elsewhere, making the studio a loss of $409,000. 

==References==
 

==External links==
*  at TCMDB
* 

 
 
 
 
 
 