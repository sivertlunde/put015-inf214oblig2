End of the Line (2007 film)
{{Infobox film
| name           = End of the Line
| image          = 
| image_size     = 
| caption        = 
| director       = Maurice Devereaux
| producer       = Maurice Devereaux
| writer         = Maurice Devereaux
| narrator       = 
| starring       = Ilona Elkin Nicolas Wright Neil Napier Emily Shelton Tim Rozon Nina Fillis 
| music          = Martin Gauthier   
| cinematography = Denis-Noel Mostert   
| editing        = Maurice Devereaux    
| distributor    = Anchor Bay Entertainment
| released       = 2007
| runtime        = 95 minutes
| country        = Canada
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
End of the Line is a 2007 Canadian horror film written, produced and directed by Maurice Devereaux. 

==Plot==
  flashback narrative plot follows her trapped in a subway. A Christian doomsday cult, which has been consuming and distributing hallucinogen-laced muffins that make people see visions of demons. On a texted signal, they take over services and begins massacring non-believers throughout the city, believing it is their mission to "save" the souls of humanity for God, which can only be accomplished by killing people with swords and daggers. A group of surviving train passengers and subway workers try to fight off and escape the cultists, but die one by one, leaving only Karen and two other people alive when the cultists are signaled to commit a mass suicide. 

==Cast==
* Ilona Elkin as Karen
* Nicolas Wright as Mike
* Neil Napier as Neil
* Emily Shelton as Julie
* Tim Rozon as John
* Nina Fillis as Sarah
* Joan McBride as Betty
* Danny Blanco Hall as Davis
* John Vamvas as Frankie
* Robin Wilcock as Patrick

==Soundtrack==
The soundtrack composed by Martin Gauthier was released on July 20, 2010, by 2m1 Records. 

==Reception==
 
The film debuted in limited release, but it garnered mostly favorable reviews from the few critics who saw it.  The Village Voice called it "scary as hell and impressively unrelenting."   C. Robert Cargill from Aint It Cool News|Aint It Cool praised it as a "truly inspired original effort," noting its modest budget and its daring, unusual premise.   The film also won prizes at several festivals including Fantastic Fests Special Jury Prize.  

== References ==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 