Quad (film)
{{Infobox film
| name = Quad
| image = 
| director = Michael Uppendahl
| producer = Gary Gilbert Jordan Horowitz Brett Johnson Robin Veith Mike Young
| starring = Aaron Paul Jeff Daniels Tom Berenger
| music = 
| cinematography = Tobias Datum
| editing = 
| distributor = 
| released =  
| runtime = 
| country = United States
| language = English
| budget = 
| gross =   
}}
Quad (also known as Cripple) is a 2013 drama film starring Aaron Paul, Jeff Daniels and Tom Berenger. Filming took place in Michigan.

==Plot==
A hard-living salesman becomes a quadriplegic after an accident.

==Cast==
* Aaron Paul as Adam Niskar
* Jeff Daniels as Mickey
* Tom Berenger as Jerry 
* Lena Olin as Yevgeina 
* Tom Sizemore as Lucky 
* Shannon Lucio as Christine
* Michael Weston as Ross
* Stephanie Koenig as Brandy
* Celia Weston as Arlene
* Yuri Sardarov as Nick Khan

==External links==
*  

 
 
 
 
 

 