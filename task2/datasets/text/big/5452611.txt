Johnny Reno
{{Infobox Film
| name           = Johnny Reno
| image          = Johnny Reno 1966 poster.jpg
| image_size     = 300
| caption        = 1966 US Theatrical Poster
| director       = R.G. Springsteen
| producer       = A.C. Lyles
| writer         = Andrew Craddock Steve Fisher  A.C. Lyles
| narrator       = 
| starring       = Dana Andrews  Jane Russell  Lon Chaney Jr.
| music          = Jimmie Haskell
| cinematography = Harold E. Stine
| editing        = Bernard Matis
| distributor    = Paramount Pictures
| released       = March 9, 1966
| runtime        = 83 mins
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Johnny Reno is a 1966 American western film made by A.C. Lyles Productions and released by Paramount Pictures. It was directed by R.G. Springsteen, produced by A.C. Lyles, with a screenplay by Andrew Craddock, Steve Fisher and A.C. Lyles.

==Synopsis==
The film features Dana Andrews, Jane Russell, and Lon Chaney, Jr., and involves a frontier marshal (Andrews) who finds himself desperately trying to stop a man he had brought to jail from being lynched by the furious townspeople.

==Plot== lawman despatched on their trail, they try to kill him. He manages to evade them, creeps up them behind and kills one of them, wounding the other whom he takes prisoner. 
 posse and Native American war party both of whom are out for their blood. Reno takes the man prisoner and heads for the town of Stone Junction, where the Connors were alleged to have committed the murder of the son of a local Indian chief. 
 Kansas City for trial, as the murder of an Indian is a federal crime. This displeases the townsmen, who quickly grow hostile to Reno, and urge him to leave town quickly. 
 old flame lynch Connors. He shoots them and hurries back to the town just in time to prevent the hanging. 

Previously the townsmen had shrunk at the idea of killing a US Marshal, as that was likely to bring down a heavy reaction from the government. Now they grow increasingly desperate, as they fear that the Indians will attack the town. They order all woman, children and old men to be evacuated leaving only the young men who pledge to defend the town. Nona Williams refuses to leave, and stays to tend to her saloon.

Reno then discovers that it was in fact the townsmen and not the guileless Connors who had committed the murder. They had done so because the daughter of a prominent local, Jess Yates (Lyle Bettger|Bettger), had fallen in love with the chief’s son. They had killed him to stop a marriage taking place. He released Connors who helps to fight alongside Reno and the Sheriff, who has decided to accept the responsibilities that his badge entails and help uphold the law. 

The townsmen surround the sheriff’s office and attack, losing a number of men. The Sheriff is also killed in the fighting, leaving just Connors and Reno against the man armed townsmen who are baying for their blood. 

As they are about to be cut down, the Indians arrive with a captive who has revealed under torture what had really happened. The Indians and the townsmen begin to fight. In the resulting shootout they are all killed, save for Reno and Connors. At the end they meet up with the woman and children who are sheltering in a nearby abandoned fort. Reno and Nona Williams depart into the sunset to start a new life together.

==Cast==
*Dana Andrews as Johnny Reno
*Jane Russell as Nona Williams
*Lon Chaney, Jr. as Sheriff Hodges
*John Agar as Ed Tomkins
*Lyle Bettger as Jess Yates
*Tom Drake as Joe Conners
*Richard Arlen as Ned Duggan Robert Lowery as Jake Reed

==External links==
* 

 
 
 
 
 