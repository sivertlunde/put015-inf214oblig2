Saheb, Biwi Aur Gangster Returns
 
 
{{Infobox film
| name = Saheb Biwi Aur Gangster Returns
| image = Saheb Biwi Aur Gangster Returns.jpg
| caption =
| director = Tigmanshu Dhulia
| producer = Tigmanshu Dhulia Rahul Mittra  Nitin Tej Ahuja
| writer = Kamal Pandey
| screenplay = Tigmanshu Dhulia
| story = 
| starring = Jimmy Shergill Mahi Gill Irrfan Khan Soha Ali Khan
| music = Sandeep Chowta Mukhtar Sahota ("Dukh Thor Ditte Jugni")
| cinematography = Yogesh Jani
| editing        = Rahul Srivastava
| studio         = Brandsmith Motion Pictures Viacom 18 Motion Pictures
| distributor    = ShowMaker Pictures
| released       =  
| Shoting place  = Devgadh Baria Near Godhra Gujarat (www.devgadhbaria.com)
| country        = India
| language       = Hindi  budget          =    gross           =   
}}
 2013 Bollywood drama film directed by Tigmanshu Dhulia.  It is the sequel to the 2011 film, Saheb, Biwi Aur Gangster.  The film stars Jimmy Shergill and Mahi Gill, who reprised their roles from the previous film, while new additions to the cast include Irrfan Khan and Soha Ali Khan.  The film was released on 8 March 2013.   Retrieved 11 March 2013 Jimmy Shergill won Best Actor Award for this film at 11th Norway Bollywood Film Festival in Oslo. 

==Synopsis==
The Royal Scandal, the war for power, and fight for money continues with the return of Saheb Biwi Aur Gangster. Aditya Pratap Singh (Jimmy Shergill) is crippled and is trying to recover from the physical disability and his wifes betrayal. The lover cum seductress Madhavi Devi (Mahie Gill) is now an MLA, her relationship with Aditya may have broken to shambles but her relation with alcohol is deep, dark and daunting. Indrajeet Singh (Irrfan Khan), a ragged prince who has lost everything but his pride, pledges to get back his familys respect which was once destroyed by Adityas ancestors. Ranjana (Soha Ali Khan) is a modern ambitious girl who is madly in love with Indrajeet Singh. The story takes a new turn when Aditya falls in love with Ranjana and forces Birendra, her father, for their marriage. In this game of live chess between Saheb, Biwi and Gangster, the winner, the survivor, takes it all. 

==Cast==
* Jimmy Shergill as Aditya Pratap Singh
* Mahi Gill as Madhavi Devi
* Irrfan Khan as Indrajeet Pratap Singh aka Raja Bhaiyya
* Soha Ali Khan as Ranjana
* Pravesh Rana as Param Pratap Singh
* Deepraj Rana as Kanhaiya
* Raj Babbar as Birendra Pratap aka Bunny
* Rajeev Gupta
* Charanpreet Singh as A journalist
* Sitaram Panchal
* Rajesh Khera
* Sujay Shankarwar as Rudy
* Anjana Sukhani in a special appearance
* Mugdha Godse in a special appearance
* Santosh Maurya as Thakur

==Production==

===Casting===
The sequel was announced following the huge critical success of the first part, Saheb Biwi Aur Gangster.  It was announced that Jimmy Shergill and Mahi Gill would be reprising their roles from the original film. In January 2012, Neil Nitin Mukesh was signed on to play the new gangster.  However, due to differences between Mukesh and the films producers, Mukesh was dropped from the part and Irrfan Khan was cast instead.  Later in March, Soha Ali Khan was confirmed to play Irrfan Khans love interest.  Raj Babbar was soon cast to play the role of Soha Alis father. Actress Neha Dhupia was to appear in an item number, but due to date issues, Mugdha Godse was signed instead.  

===Filming===
Filming began in April 2012 in Gujarat in Santarampur and devgadh baria of panchmahals district  and was wrapped up in October 2012, working on an intended budget of Rs 70&nbsp;million.   

==Release==
The film was released on 8 March 2013.

===Critical reception===
The film was well received by critics.  The performances of the main cast and the dialogues especially met with universal acclaim.

Taran Adarsh of Bollywood Hungama gave the film 3.5/5 stars praising the performances of the main cast and the intricate character developments.  
Raja Sen of Rediff.com gave the film 4/5 stars and said Saheb. Biwi Aur Gangster Returns is better, sharper and more assured than the prequel.  He called the dialogues "applause seeking" and also added, "Saheb Biwi Aur Gangster Returns is a theatrically indulgent entertainer, one that makes no bones about its pulpiness and stays well and truly juicy."     
Ananya Bhattacharya of Zee News gave the film 4/5 stars and called it a masterpiece.  
Anupama Chopra of Hindustan Times gave the film 3/5 stars saying that the film should be seen for its characters and dialogue. 
Rajeev Masand of IBN Live gave the film 3/5 stars saying that the film is an engaging watch.  Yahoo India awarded the film 4.5/5 stars, praising Irrfan Khans performance and calling the film overall "a compelling watch". 

===Box office===
Saheb Biwi Aur Gangster Returns had a slow start but grew up as time proceed and managed   on its opening day while around   in its first weekend to make a good total.    It eventually netted   in domestic market. 

==Soundtrack==
{{Infobox album
| Name       = Saheb Biwi Aur Gangster Returns
| Type       = Album
| Artist     = Sandeep Chowta
| Cover      =
| Released   =  
| Recorded   = Feature film soundtrack
| Length     =
| Label      = Junglee Music
| Producer   =
| Reviews    =
}}

The films soundtrack was released on 10 February 2013. The album contains five songs composed by Sandeep Chowta and lyrics penned by Sandeep Nath.  A bonus song was later added to the film cut, "Dukh Thor Ditte Jugni", by music director Mukhtar Sahota, which became the opening song of the film.

===Tracklist===
{{track listing
| extra_column = Singer(s)
| title1 = Idhar Gire
| extra1 = Sowmya Raoh
| length1 = 04:24
| title2 = Jugni
| extra2 = Jazzy B
| length2 = 03:28
| title3 = Media Se
| extra3 = Paroma Dasgupta
| length3 = 03:22
| title4 = Bas Chal Kapat
| extra4 = Piyush Mishra
| length4 = 04:03
| title5 = Kona Kona Dil Ka
| extra5 = Devaki Pandit
| length5 = 04:56
| title6 = Dukh Thor Ditte Jugni (music Mukhtar Sahota)
| extra6 = Harbhajan Shera
}}

==Sequel==
After two successful films director and producer are planning a third installment of this series. 

==References==
 

==External links==
*  

 
 
 
 