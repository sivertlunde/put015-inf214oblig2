Until They Sail
{{Infobox film
| name           = Until They Sail 
| image          = UntilTheySail.jpg
| image_size     = 200px
| caption        = Original poster
| director       = Robert Wise
| producer       = Charles Schnee
| based on       =   Robert Anderson
| narrator       = 
| starring       = Jean Simmons Joan Fontaine Paul Newman Piper Laurie Sandra Dee
| music          = David Raksin
| cinematography = Joseph Ruttenberg   
| editing        = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $1,841,000  . 
| gross          = $1,420,000 
}}
 1957 American Robert Anderson, Return to Paradise, focuses on four New Zealand sisters and their relationships with U.S. Marines during World War II.  

==Plot== flash back to the events that led to the trial. She and her sisters Anne (Joan Fontaine), Evelyn (Sandra Dee), and Delia (Piper Laurie) live in Christchurch, where most of the male residents, including their brother Kit and Barbaras new husband Mark, are preparing to leave for World War II duty. Delia announces her engagement to Phil Friskett (Wally Cassell), known as "Shiner", who is one of the citys few remaining bachelors, but word of Kits death dampens the celebration. Repressed and judgmental spinster sister Anne disapproves of the upcoming nuptials, but Barbara defends Delias decision.
 United States Marines are shipped to Christchurch following the 1941 attack on Pearl Harbor, the lonely local women are flattered by the attention they pay them. When Evelyn invites Capt. Richard Bates (Charles Drake) to dinner, he declines the offer, but not without attracting Annes eye.

Concerned about Delia, Anne sends Barbara to Wellington, where she discovers her sister is registered at the St. George Hotel, Wellington|St. George Hotel under her maiden name. Shiner is now a prisoner of war, and Delia has become involved with an American lieutenant named Andy. She plans to divorce Shiner and emigrate to the United States. Andy introduces Barbara to his friend Jack Harding (Paul Newman), a soldier investigating the prospective New Zealand brides of American soldiers. Although Barbara intends to remain faithful to her husband, she finds herself attracted to Jack.

Back in Christchurch, Anne is outraged by the lewd comments made by American servicemen in the lingerie shop where she works and writes a letter of complaint to the local paper. Following its publication, Richard is sent to the Leslie home to deliver a formal apology on behalf of the Marine Corps. Anne invites him to dinner, and Richard arrives with a gift of perfume for each sister. Anne accuses him of trying to seduce them.

Soon after, Barbara and Anne learn of Marks death in North Africa and Richards departure for active duty. He eventually returns to New Zealand to recuperate from an injury, and a romance between him and Anne blossoms. He proposes, but before the required marital investigation can take place, he is given offshore duty, leaving Anne expecting their child and unsure of what the future holds for them.

Jack arrives at the Leslie home to conduct his investigation of Anne, and he advises her that wartime romances stem from loneliness rather than love. Barbara tells him his assessment is heartless. Shortly after she discovers Richards name on the latest casualty list in. Weeks later, Jack meets Barbara at a local dance, where she suggests he uses alcohol to avoid intimacy. He breaks down in her arms, and a strong friendship between the two blossoms.

Jack celebrates Christmas Eve with the Leslie family, which now includes Annes newborn son. When he announces his imminent departure to, he and Barbara share an amorous embrace. Months later, Evelyns sweetheart Tommy returns from war and proposes to her. Barbara sees an ad from Richards mother in a newspaper column containing personal notices from American families and writes to her. Mrs. Bates sends money to finance Anne and her babys move to Oklahoma to live with Richards family.

As Annes departure approaches, and the aftermath of the end of the war, Delia goes to Wellington to see her off, only to meet Shiner, who has just returned from war. He accuses her of infidelity and she demands a divorce so she can leave for America with her lover. Infuriated, Shiner kills his wife with a Japanese sword he brought back from the war.

Weeks later, during the murder trial, Jack is forced to reveal his investigation report detailing Delias many affairs with American soldiers. Upset that her sisters infidelities seemingly have justified her savage murder, Barbara refuses Jacks invitation to leave New Zealand with him. Upon reflection, she packs her belongings and arrives at Jacks hotel to tell him shes ready to embark upon a new life with him.

==Cast==
* Jean Simmons as Barbara Leslie Forbes 
* Joan Fontaine as Anne Leslie 
* Paul Newman as Capt. Jack Harding 
* Piper Laurie as Delia Leslie Friskett 
* Charles Drake as Capt. Richard Bates 
* Sandra Dee as Evelyn Leslie
* Wally Cassell as Phil Friskett
* Alan Napier as Prosecution Attorney
* Ralph Votrian as Max Murphy John Wilder as Tommy
* Tige Andrews as US Marine #1
* Adam Kennedy as Lt. Andy
* Mickey Shaughnessy as US Marine #2

==Music==
The score for the film was composed and conducted by David Raksin.  The title song included lyrics by Sammy Cahn and was performed under the main titles by vocalist Eydie Gorme. {{cite AV media notes
| title      = David Raksin at MGM (1950-1957)
| others    = David Raksin
| date      = 2009
| url       = http://www.filmscoremonthly.com/notes/until_they_sail.html
| last      = Bettencourt
| first     = Scott
| pages     = 
| type    = CD online notes
| publisher = Film Score Monthly
| id       = Vol. 12, No. 2
| location  = Los Angeles, California, U.S.A.
| language  = 
}} 

The complete score was issued on CD in 2009, on Film Score Monthly records.

==Production==
Robert Wise and Mark Robson had originally purchased the rights for Micheners story when they were at RKO.  Casting problems forced them to delay the filming when the rights went to Hill-Hecht-Lancaster Productions who were going to cast Burt Lancaster.  When the company made The Kentuckian instead, MGM acquired the rights, first intending their contract lead Glenn Ford playing the lead Marine.  Robert Wise then reacquired the film through MGM in his last film of his contract with the studio. 

Wise visited New Zealand to familiarise himself with the nation and the people, but filmed the movie on the MGM back lot. 
He had originally intended to shoot the film in colour.  It was Sandra Dees first film.

Stewart Granger was once announced for the lead. 

==Box Office==
According to MGM records the film earned $745,000 in the US and Canada and $675,000 elsewhere, resulting in a loss of $1,055,000. 

==Critical reception==
Bosley Crowther of The New York Times observed, "The genuine tugs at the heart are few and far between in this bittersweet but basically restrained chronicle. Robert Andersons adaptation . . . is honest and straightforward . . . Unfortunately there is a good deal of introspective soul-searching before this narrative arrives at its sad and happy endings."  

William K. Zinsser of the New York Herald Tribune said the film "has moments of genuine tenderness and truth."  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 