Lady Windermere's Fan (1925 film)
{{Infobox film
| name           = Lady Windermeres Fan
| image          = LadyWindermeresFan1925Poster.jpg
| image_size     =
| caption        = US Poster
| director       = Ernst Lubitsch
| producer       = Ernst Lubitsch (producer) Darryl F. Zanuck (producer)  (uncredited)
| writer         = Oscar Wilde (play Lady Windermeres Fan) Julien Josephson (adaptation) Maude Fulton (titles) and Eric Locke (titles)
| narrator       =
| starring       =
| music          = Yati Durant
| cinematography = Charles Van Enger
| editing        = Ernst Lubitsch
| distributor    = Warner Brothers
| released       = December 26, 1925
| runtime        = 120 minutes (Denmark) 89 minutes (2004 National Film Preservation Foundation print)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Lady Windermeres Fan is a   (sister company Warner Brothers) with the film being preserved by several archives. 

== Cast ==
*Ronald Colman as Lord Darlington
*May McAvoy as Lady Windermere
*Bert Lytell as Lord Windermere
*Irene Rich as Mrs. Erlynne
*Edward Martindel as Lord Augustus Lorton
*Carrie Daumery as The Duchess of Berwick

==See also==
*Treasures from American Film Archives

== References ==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 