Little Nicholas
{{Infobox film
| name           = Little Nicholas
| image          = Le Petit Nicolas poster.jpg
| caption        = Film Poster
| director       = Laurent Tirard
| producer       = Olivier Delbosc Marc Missonnier Genevieve Lemal Alexandre Lippens	
| screenplay     = Laurent Tirard Grégoire Vigneron Alain Chabat	 
| based on       =  
| starring       = Maxime Godart Kad Merad Valérie Lemercier
| music          = Klaus Badelt
| cinematography = Denis Rouden	 
| editing        = Valérie Deseine	 	
| studio         = Fidélité Productions Wild Bunch M6 Films Mandarin Films Scope Pictures Wild Bunch Distribution Central Film EOne Films
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = $22.7 million 
| gross          = $100,812,504 
}} series of childrens books by René Goscinny. The film was released in France on 30 September 2009.  A sequel to the film, Nicholas on Holiday, was released on 9 July 2014.

==Plot==
This comedy is based on a little boy called Nicolas that gets up to all sorts of mischief with his friends. Some are unintentional while others are intended. Matters get worse when Nicolas thinks his mother is pregnant and thanks to his friends thinks this means that his parents dont like him anymore. Before any drastic decisions are taken Nicolas understands what it really is like to be a big brother. But things dont turn out quite as planned. During this time he thinks he is going to get a younger brother but instead gets a sister. 

== Cast ==
* Maxime Godart as Little Nicolas
* Valérie Lemercier as Nicolass mother
* Kad Merad as Nicolass father
* Sandrine Kiberlain as the teacher
* François-Xavier Demaison as Mr. Dubon
* Daniel Prévost as M. Moucheboume, Nicolass fathers boss
* Victor Carles as Clotaire, the worst student in class
* Damien Ferdel as Agnan, the hypocrite and best student in class glutton
* Charles Vaillant as Geoffrey, the richest student in class
* Benjamin Averty as Eudes, the fighter
* Germain Petit Damico as Rufus, the son of policeman
* Virgil Tirard as Joachim
* Michel Duchaussoy as the school director
* Michel Galabru as the Education Minister
* Anémone as the substitute teacher

==Production==

===Development===
Producers Olivier Delbosc and Marc Missonnier from Fidelity Films offered Laurent Tirard the project, who immediately accepted it as he has grown up with the characters from the story. Talking about that he said that "It is, however, struck me as obvious. I grew up with Le Petit Nicolas. I read (it) when I was a teenager. This work represents me and speaks to me. I immediately knew what the film would look like."   

He further added that the character of the Nicolas was very personal to René Goscinny, "I knew that the key would be to adapt the both in his work and in his life, so I tried to understand the character of René Goscinny . This was someone who was looking for his place in society, and he had to win through laughter. At the time he was an accountant, his pleasure was to think it was the grain of sand that would make all the rails. He had a taste for the mess and realized that laughter could be both a defense a society where you do not feel out of place and a way to insert. These are things that I read between the lines of his biographies and spoke to me. The little boy looking for his place in society has become the axis on which to build the story." 

===Casting===
On 8 April 2008 it was announced that Valerie Lemercier and Kad Merad have joined the cast of the film as Nicloass mother and father.  Maxime Godart was cast as the main protagonist Nicolas. Tirard said about his casting that "Maxime Godart has a very clear vision of the place he wants to be in the company of what he wants to do with his life. With his outgoing personality, I thought he would not be afraid in front of the camera. But it happened the other way around. The first day, when huge crane arm with a camera approached him for a first round crank, he was petrified! Chez Maxime, even more than in other children, the desire and the pleasure of playing were great. He never gave any sign of fatigue or expressed the need to stop."  Triard also cast his own son Virgil Triard as Joachim, one of Nicolass friend and classmate. 

===Filming===
Filming begin on 22 May 2008 in Paris and continued till 11 October 2008. Most of the filming took place at Studio Monev at Sint-Pieters-Leeuw. Scenes were also shot at Laeken near the old school of boatmen on a vacant lot and at the corner of the street Claessens and Rue Dieudonné Lefèvre.  

==Music==
{{Infobox album|  
| Name        = Le Petit Nicolas: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Klaus Badelt
| Cover       = Le Petit Nicolas soundtrack.jpg
| Released    = September 28, 2009  
| Length      = 40:23
| Label       = EmArcy Records
| Producer    = Klaus Badelt
| Chronology  = Klaus Badelt Dragon Hunters (2008)
| This album  = Le Petit Nicolas (2009) Solomon Kane (2009)
}} score for Le Petit Nicolas was composed by Klaus Badelt and performed by Geert Chatrou, Dirk Brossé and Loïc Pontieux.   It was released on September 28, 2009 by EmArcy Records.   Renan Luces second single On n’est pas à une bêtise près (was not a mistake near) from his 2009 album Clan miros appears at the end credit of the film but it is not part of the album. It was later released by Luce in October 2009.  

The album received positive response on its release. Movienthusiast gave the album a positive review and awarded it three out of five stars by saying that "Music of this film is able to fill a variety of themes, scenes usually pose a ticklish feeling in the audience themselves. But for serious little scene, Badelt uses intelligent sound combining bass drums, strings of a violin with the sound of the harmonica, triangle even whistle. Overall, every scene is filled by a variety of sounds from various instruments giving them extra charm." 

{{track listing
| headline = Tracklist
| total_length =
| all_lyrics =
| lyrics_credits = no
| title1 = Un drôle de sujet de rédaction (A funny essay topic)
| lyrics1 =
| length1 = 07:42
| title2 = Générique (Generic)
| lyrics2 =
| length2 = 02:46
| title3 = Mord aux prof (Kill the teacher)
| lyrics3 =
| length3 = 01:14
| title4 = La Roulette (Roulette)
| lyrics4 =
| length4 = 0:58
| title5 = Les Filles, cest pas intéressant (Girls are not interesting)
| lyrics5 =
| length5 = 01:00
| title6 = Papa et Maman se disputent souvent (Mom and Dad often argue)
| lyrics6 =
| length6 = 01:43
| title7 = 3 Francs par rose (3 Francs for a rose)
| lyrics7 =
| length7 = 03:02
| title8 = Un Jeu drôlement compliqué (An awfully complicated game)
| lyrics8 =
| length8 = 01:40
| title9 = Une Balade en forêt (A Walk in the forest)
| lyrics9 =
| length9 = 01:21
| title10 = Le Spectacle (The Show)
| lyrics10 =
| length10 = 01:11
| title11 = Je vais avoir un petit frère! (Im having a little brother!)
| lyrics11 =
| length11 = 01:09
| title12 = Ménage (Cleaning)
| lyrics12 =
| length12 = 01:19
| title13 = Gangster-à-louer (Gangster to rent)
| lyrics13 =
| length13 = 01:08
| title14 = Et en plus, cest un sale cafard! (And besides, its a dirty cockroach!)
| lyrics14 =
| length14 = 01:11
| title15 = Potion Magic (Magic Potion)
| lyrics15 =
| length15 = 03:50
| title16 = Rivalités fraternelles (Sibling rivalry)
| lyrics16 =
| length16 = 02:05
| title17 = Rolls Folle (Rolls mad)
| lyrics17 =
| length17 = 03:58
| title18 = Neuf Mois (Nine Months)
| lyrics18 =
| length18 = 02:04
| title19 = On dirait un poivron confit (It looks like a pepper confit)
| lyrics19 =
| length19 = 01:02
}}

==Release==

===Box office===
In its first week of release Le Petit Nicolas sold over a million tickets in France.    The film grossed $48,398,428 in France, and $11,088,066 in international territories for a total of $59,486,494. 

===Critical reception===
  in Paris, September 20, 2009.]] Empire Online The Guardian gave four stars out of five by saying that "It presents a gently humorous, beautifully shot idyllic version of childhood, all blue skies, good manners and not a hair out of place. Its a nice place to visit for the duration."  Omer Ali of Little White Lies praising the film, said that "A diverting alternative to more high-octane kiddie fare."  Amber Wilkinson of Eye for Film praised the actors by saying that "In a refreshing change from Hollywood films aimed at this market there is a blissful lack of toilet humour and theres plenty of fun to be had for an older audience in watching Nicholas hapless father (Kad Merad) attempt to win a promotion from his boss by bringing him home to dinner. The acting from the adults has a slight pantomime edge to it, but this complements the source material and gives a real sense of the way in which children tend to view grown ups as larger than life. The children, meanwhile, form a sweet and believable ensemble, with Maxime Godart in the central role and Victor Carles as class clot Clotaire, in particular, likely to crop up in other films."  Similarly Bernard Besserglik of The Hollywood Reporter said that "This screen version, "Little Nicolas," technically proficient and featuring two of Frances best comic actors." 
 Variety criticized the film and said that "The clan of boys, and especially Nicolas himself, are too impeccably coiffed, dressed and mannered to resemble the ruffians depicted in Sempe’s drawings, or anything like real kids at all. Along with Francoise Dupertuis’ flamboyant sets and tidy lensing by Denis Rouden (“MR 73″), the result is a look of squeaky-clean postwar nostalgia, closer to Christophe Barratier’s “The Chorus” than to Truffaut’s “The 400 Blows,” which was set around the same time period."  Robbie Collin of The Daily Telegraph also gave a negative review to the film by saying that "English-speaking children will have to read very quickly indeed to keep up with the subtitles in this meek French family entertainment, based on a series of children’s books by René Goscinny, original writer of the Asterix strips. Just William in mid-century Paris is a fair indication of tone, if perhaps not entertainment value." 

===Home media===
The film was released on DVD on 3 February 2010 by Wild Side Video.  Bonus features include a Booklet with a history of Petit Nicolas and commentary featuring the child artists of the film. 

==Accolades==
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="3"| 2010
| César Award Best Writing – Adaptation
| Laurent Tirard Gregoire Vigneron
|  
|-
| European Film Awards
| 23rd European Film Awards#Peoples Choice Award for Best European Film|Peoples Choice Award for Best European Film
| Laurent Tirard
|  
|-
| Cinéfranco
| TFO Prize for Best Youth Film
| Laurent Tirard
|  
|-
| 2011
| Cinema Brazil Grand Prize
| Best Foreign-Language Film
| Laurent Tirard
|  
|}
 

==Sequel==
In August 2013, it was confirmed that the sequel to the film titled Nicholas on Holiday (Les Vacances du Petit Nicolas) will be released on 9 July 2014.  Valerie Lemercier and Kad Merad reprised their roles in the sequel, with the character of Nicolas played by newcomer Mathéo Boisselier. 

==See also==
*Le petit Nicolas, series of French childrens books.

==References==
 

==External links==
 
 
*    
*  

 
 
 
 
 
 
 
 
 

 