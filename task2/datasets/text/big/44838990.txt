Aurora de esperanza
{{Infobox film
| name           = Dawn of Hope
| image          = Aurora de esperanza.jpg
| alt            =
| caption        = 
| film name      = Aurora de esperanza
| director       = Antonio Sau
| producer       = SIE Films
| writer         = Antonio Sau
| screenplay     = Antonio Sau
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = Jaume Pahissa
| cinematography = Adrién Porchet
| editing        = Juan Pallejá
| studio         =       
| distributor    = 
| released       =      
| runtime        = 60 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| gross          =  
}}
Aurora de esperanza (Down of Hope) is a 1937 black-and-white film directed by Antonio Sau and produced by SIE Films. 

== Plot ==

Drama about the economic situation of the working-class and the start of the social revolution.

== Cast ==

* Félix de Pomés as Juan
* Enriqueta Soler as Marta
* Pilar Torres as La tanguista
* Ana María Campoy as Pilarín
* Román González "Chispita" as Antoñito

== References ==
 

== See also ==
* List of films produced in the Spanish Revolution
* List of Spanish films of the 1930s

==External links==

*  

 
 
 
 
 
 
 