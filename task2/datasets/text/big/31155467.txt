Manikyakottaram
{{Infobox film 
| name           = Manikyakottaram
| image          =
| caption        =
| director       = U Rajagopal
| producer       = HH Abdulla Settu
| writer         = MM Ebrahimkutty
| screenplay     = MM Ebrahimkutty Madhu Sharada Sharada Kamalam PK Sathyapal
| music          = MS Baburaj
| cinematography = U Rajagopal
| editing        =
| studio         = Rubeena Films
| distributor    = Rubeena Films
| released       =  
| country        = India Malayalam
}}
 1966 Cinema Indian Malayalam Malayalam film,  directed by U Rajagopal and produced by HH Abdulla Settu. The film stars Madhu (actor)|Madhu, Sharada (actress)|Sharada, Kamalam and PK Sathyapal in lead roles. The film had musical score by MS Baburaj.   

==Cast== Madhu
*Sharada Sharada
*Kamalam
*PK Sathyapal Baby Padmini
*Bahadoor
*K. P. Ummer
*Nellikode Bhaskaran
*Philomina
*Suprabha

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Kaniyapuram Ramachandran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kallante Peru || S Janaki || Kaniyapuram Ramachandran || 
|-
| 2 || Manassinte Malarani || K. J. Yesudas || Kaniyapuram Ramachandran || 
|-
| 3 || Nakshathrappunnukal Aayiram || Kozhikode Abdul Khader || Kaniyapuram Ramachandran || 
|-
| 4 || Pachamarakkaadukale || K. J. Yesudas || Kaniyapuram Ramachandran || 
|-
| 5 || Pennu Kelkkaan || LR Eeswari || Kaniyapuram Ramachandran || 
|}

==References==
 

==External links==
*  

 
 
 


 