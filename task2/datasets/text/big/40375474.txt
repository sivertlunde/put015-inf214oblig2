Rhymes for Young Ghouls
 
 
{{Infobox film
| name           = Rhymes for Young Ghouls
| image          = Rhymes for Young Ghouls poster.jpg
| caption        = Theatrical poster
| director       = Jeff Barnaby
| producer       = Aisling Chin-yee John Christou
| writer         = Jeff Barnaby
| based on       = 
| starring       = Kawennahere Devery Jacobs Glen Gould Brandon Oakes
| music          = Jeff Barnaby Joe Barrucco
| cinematography = Michel St Martin
| editing        = Jeff Barnaby Mathieu Belanger
| distributor    = Les Films Séville monterey media inc. (usa)
| released       =     
| runtime        = 88 minutes
| country        = Canada
| language       = English Mikmaq language|Mikmaq
| budget         = $1,500,000 
}}
 residential school system. Although it tells the fictional story of a teenager named Aila and her plot for revenge, it is based on the history of abuse of the First Nations people by government agents, including a large number of reported cases of the mental and physical abuse of residential school children.    It is presented from the perspective of teenage girl. 

==Production==
Prospector Films of Montreal produced the film, which was shot from 21 October to 22 November 2012.   

==Cast==
* Kawennahere Devery Jacobs as Aila
* Glen Gould as Joseph
* Brandon Oakes as Burner
* Mark Antony Krupa as Popper
* Roseanne Supernault as Anna

==Plot==
The film is set in 1976 at the fictional Red Crow Reserve, a Mikmaq people|Mikmaq Indian reserve. Aila (Kawennahere Devery Jacobs), a Mikmaq youth, loses her mother to suicide and her father Joseph (Glen Gould) to a prison term. She assumes her fathers drug dealing business while in the care of her uncle Burner (Brandon Oakes) who himself consumes and sells drugs. Josephs release from prison interrupts Aila and Burners drug business, while they all face the schemes of the abusive Indian agent Popper (Mark Antony Krupa). Aila plots revenge against Popper after she is thrown into the reserves residential school.        

==Reaction== 2013 Vancouver International Film Festival.  As of 26 September 2013, IMDb reported a user rating of 8.1/10, coupled with 10 highly favourable critical reviews.  Its first theatrical release was in Toronto, Ontario on 31 January 2014. 
 Best Performance by an Actress in a Leading Role at the Canadian Screen Awards.

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 

 
 