Dr. Akagi
{{Infobox film
| name = Dr. Akagi
| image = Dr_Akagi_1998.jpg 220px
| image_size =
| caption =
| director = Shohei Imamura
| producer = Hiso Ino Koji Matsuda
| writer = Ango Sakaguchi (novel Doctor Liver) Shohei Imamura Daisuke Tengan
| starring = Akira Emoto Kumiko Aso Juro Kara Masanori Sera Jacques Gamblin Keiko Matsuzaka Misa Shimizu Yukiya Kitamura Masato Yamada Tomorowo Taguchi Masatô Ibu
| music = Yosuke Yamashita
| cinematography = Shigeru Komatsubara
| editing = Hajime Okayasu Kadokawa Shoten Toei Co. Kino International (USA)
| released =  
| runtime = 129 min. (Japan)
| country = Japan
| language = Japanese
| budget =
}}

Dr. Akagi, known in Japan as  , is a 1998 Japanese film by director Shohei Imamura.

== Plot ==
The film concerns Dr. Akagi, a doctor on an island in the Seto Inland Sea area during World War II. He runs into conflict with the military while trying to combat a hepatitis epidemic. Akagi earns the nickname "Dr. Liver" (カンゾー先生 Kanzō-sensei) because of his work.

==See also==
* 1998 in film

==External links==
*  
*   (film review) at chicagoreader.com.
 

 
 
 
 
 

 