Before Sunrise
 
{{Infobox film
| name           = Before Sunrise
| image          = Before Sunrise poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Richard Linklater
| producer       = Anne Walker-McBay
| writer         = Richard Linklater Kim Krizan
| starring       = Ethan Hawke Julie Delpy
| music          = Fred Frith 
| cinematography = Lee Daniel
| editing        = Sandra Adair
| studio         = Castle Rock Entertainment
| distributor    = Columbia Pictures
| released       =  
| runtime        = 101 minutes  
| country        = United States Austria Switzerland
| language       = English German French
| budget         = $2.5 million 
| gross          = $5,535,405 
}} romantic drama film directed by Richard Linklater and written by Linklater and Kim Krizan. The film follows Jesse (Ethan Hawke), a young American man, and Céline (Julie Delpy), a young French woman, who meet on a train and disembark in Vienna, where they spend the night walking around the city and getting to know each other.

The plot is minimalist, since not much happens, aside from walking and talking. The two characters ideas and perspectives on life and love are detailed. Jesse is a romantic disguised as a Cynicism (contemporary)|cynic, and Céline is seemingly a romantic, albeit with some doubts. Taking place over the course of one night, their limited time together is always on their minds, and leads to their revealing more about themselves than they normally would, since both believe they will never see one another again.
 Before Midnight, picks up the story eighteen years on.

==Plot==
The film starts on June 16, 1994 with Jesse meeting Céline on a train from Budapest and striking up a conversation with her. Jesse is going to Vienna to catch a flight back to the United States, whereas Céline is returning to university in Paris after visiting her grandmother. When they reach Vienna, Jesse convinces Céline to disembark with him, saying that 10 or 20 years down the road, she might not be happy with her marriage and might wonder how her life would have been different if she had picked another guy, and this is a chance to realize that he himself is not that different from the rest; in his words, he is "the same boring, unmotivated guy." Jesse has to catch a flight early in the morning and does not have enough money to rent a room for the night, so they decide to roam around in Vienna.

After visiting a few landmarks in Vienna, they share a kiss at the top of the Wiener Riesenrad at sunset and start to feel a romantic connection. As they continue to roam around the city, they begin to talk more openly with each other, with conversations ranging from topics about love, life, religion, and their observations of the city. Céline tells Jesse that her last boyfriend broke up with her six months ago, claiming that she "loved him too much". When questioned, Jesse reveals he had initially come to Europe to spend time with his girlfriend who was studying in Madrid, but they had broken up when she was avoiding him while he was there. He decided to take a cheap flight home, via Vienna, but it did not leave for two weeks so he bought a Eurail pass and traveled around Europe.
 David Jewell). In a traditional Viennese café, Jesse and Céline stage fake phone conversations with each other, playing each others friends they pretend to call. Céline reveals that she was ready to get off the train with Jesse before he convinced her. Jesse reveals that after he broke up with his girlfriend, he bought a flight that really was not much cheaper, and all he really wanted was an escape from his life.
 train station, where the two hastily decide not to exchange any contact information but instead agree to meet together at the same place in six months time, just as the train is about to leave.

==Cast==
* Ethan Hawke as Jesse Wallace
* Julie Delpy as Céline
* Andrea Eckert as Wife on train
* Hanno Pöschl as Husband on train
* Karl Bruckschwaiger as Guy on bridge 
* Tex Rubinowitz as Guy on bridge 
* Erni Mangold as Palm reader
* Dominik Castell as Street poet
* Haymon Maria Buttinger as Bartender 
* Bilge Jeschim as Belly dancer 
* Adam Goldberg (uncredited) as Man sleeping on train

==Production== Dazed and Confused.  According to Linklater, he "loved the way her mind worked – a constant stream of confident and intelligent ideas".   

Linklater and Krizan talked about the concept of the film and the characters for a long time.  He wanted to explore the "relationship side of life and discover two people who had complete anonymity and try to find out who they really were".    He decided to put Jesse and Céline in a foreign country because "when youre traveling, youre much more open to experiences outside your usual realm".  He and Krizan worked on an outline. They wrote the actual screenplay in 11 days. 

Linklater spent nine months casting the film because he saw so many people but had trouble finding the right actors for the roles of Jesse and Céline.    When Linklater first considered casting Hawke, he thought that the actor was too young for the part.    Linklater saw Hawke at a play in New York City and reconsidered after talking to the actor. For Céline, Linklater met Julie Delpy and liked her personality. After they did a final reading, Linklater knew that Delpy and Hawke were right for the roles.  Once Delpy and Hawke agreed to do the film, they went to Austin and talked with Linklater and Krizan for a few days. 

==Themes==
Before Sunrise revolves largely around the twin themes of self-fulfillment and self-discovery through a significant other, charging the concept through the introduction of a twelve-hour time constraint in which the goals implicit to the two themes have to be realized. They are underlined by the poem "Delusion Angel", which evokes a longing for complete and unifying, possibly even redeeming, understanding between two partners in a world which is itself unknowable, and over which one can exercise no control.
 stream of consciousness, initiated by a previously unmeditated decision to leave the train together, allows them to temporarily detach themselves from the world, and enter a realm where only the others company is of importance. It is worth noting that when the morning arrives Jesse remarks that he and Céline have again entered "real time". 

It could be argued that Before Sunrise subsumes its main themes under that of life. In one scene, Céline and Jesse visit the Friedhof der Namenlosen, the Cemetery of the Nameless in Simmering (Vienna)|Simmering. The people buried in the cemetery have found anonymity in death; by learning to know and understand one another, Céline and Jesse experience and embrace life, suspending their own mortality. 
 Robin Wood has written that after he published an essay on the film (in a 1996 issue of CineAction), Linklater wrote him to say that "neither he nor the two actors ever doubted that the date would be kept."   

The film takes place on June 16, Bloomsday. 

==Inspiration==
The story of Jesse and Céline was inspired by an evening that Richard Linklater spent with a young woman named Amy Lehrhaupt, whom he met during a day he spent in Philadelphia traveling from New York to Austin.  
Not until 2010 was Linklater to be informed that Lehrhaupt had actually died in a motorcycle accident prior to the release of Before Sunrise.

==Release== world premiere at the 1995 Sundance Film Festival.  It was released in the United States on January 27, 1995.

===Critical reception===
The film was entered into the 45th Berlin International Film Festival where Linklater won the Silver Bear for Best Director.   
 English so well the screenplay has to explain it (she spent some time in the States)".    In her review for The New York Times, Janet Maslin wrote, "Before Sunrise is as uneven as any marathon conversation might be, combining colorful, disarming insights with periodic lulls. The film maker clearly wants things this way, with both these young characters trying on ideas and attitudes as if they were new clothes".    Hal Hinson, in his review for The Washington Post wrote, "Before Sunrise is not a big movie, or one with big ideas, but it is a cut above the banal twentysomething love stories you usually see at the movies. This one, at least, treats young people as real people".   

In his review for the Los Angeles Times, Peter Rainer wrote, "Its an attempt to make a mainstream youth movie with a bit more feeling and mysteriousness than most, and, in this, it succeeds".    Marjorie Baumgarten, in her review for The Austin Chronicle, wrote, "Before Sunrise represents a maturation of Linklaters work in terms of its themes and choice of characters".    In his review for The New Yorker, Anthony Lane wrote, "Just once, for a single day, Jesse and Céline have given life the sort of shape and charge that until now they have found only in fiction, and may never find again".    Entertainment Weekly gave the film an "A-" rating and Owen Gleiberman wrote, "Small movies can be as daring as big ones, and Linklater, in his offhand way, is working without a net here. Before Sunrise may be the closest an American has come to the discursive talk gamesmanship of Eric Rohmer".   

Online film critic James Berardinelli has cited the film as "the best romance of all time".  Entertainment Weekly rated Before Sunrise #25 on their Top 25 Modern Romances list.    In a 2008 Empire (film magazine)|Empire poll, Before Sunrise was ranked as the 200th greatest movie of all time.  In 2010 British newspaper The Guardian ranked Before Sunrise/Before Sunset #3 on their critics list of 25 best romantic films of all time, and #2 in an online readers poll.  

===Box office===
The film grossed $1.4 million in 363 theaters on its opening weekend, and went on to make $5.5 million, more than double its $2.5 million budget.   

==Sequel== Before Midnight, was released in 2013, again to rave reviews.

Jesse and Céline also had a very short scene together in Linklaters 2001 animated film, Waking Life. In this scene, the two are together in bed talking, though since the film is all about lucid dreaming, and from the plot points established in Before Sunset, the thought is that this scene never existed in reality.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 