Rune (film)
Rune linguist who the source of all modern languages.

Special effects in the film were done by Santa Monicas Radium, a firm with credits ranging from Target commercials to feature films like Spider-Man 2.

According to online press releases, the film was shot over the course of 2 months, a fairly long shoot by the standards of independent film, and was entirely independently financed.

The films cast is international and diverse, with German ingénue Anna Bäumer as the linguist, and veteran character actor Bill Wise, veteran of a number of Richard Linklater films.

The movie also used several local actors from the states of Texas and Oklahoma. Oklahoma City was used for a lot of the scene in the movie and housed the Isis Production Company office for the shoot of the film.

==External links==
*  Hollywood Reporter Article
*  Newsguide Article
*  
*  Official Site

 
 


 