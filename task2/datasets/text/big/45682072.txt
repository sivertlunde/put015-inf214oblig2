Princess Trulala
{{Infobox film
| name           = Princess Trulala 
| image          =
| caption        =
| director       = Erich Schönfelder   Richard Eichberg
| producer       = Richard Eichberg 
| writer         = Hans Stürm 
| starring       = Lilian Harvey   Dina Gralla   Harry Halm
| music          = Hans May 
| cinematography = Willy Hameister 
| editing        = 
| studio         = Richard Eichberg-Film 
| distributor    = Süd-Film
| released       = 15 April 1926
| runtime        = 
| country        = Germany
| awards         =
| language       = Silent   German intertitles
| budget         =
}} silent comedy Kurt Richter. As was common in her silent films, Harveys heroine has to choose between several different suitors.     

==Cast==
*  Lilian Harvey as Prinzessin Trulala 
* Dina Gralla as Prinzessin Hopsassa  
* Harry Halm as Prinz Arnulf   Hans Junkermann as Hofmarschall  
* Teddy Bill as Lackei  
* Lucie von Wartberg as Prinzessin Lola  
* Eva Schmid-Kayser as Prinzessin Lila 
* Leopold von Ledebur as Fürst Arnuld von Leinefeld 
* Julia Serda as Fürstin  
* Emmy Wyda as Tante Eugenie  
* Mira Brandt as Gesellschaftsdame  
* Hans Stürm as Wirt "Zum goldenen Kreuz"  
* Victor Colani as Forstmeister  
* Georg Gartz as Sekretär  

==References==
 

==Bibliography==
*Ascheid, Antje. Hitlers Heroines: Stardom & Womanhood In Nazi Cinema. Temple University Press, 2010.

==External links==
*  

 
 
 
 
 
 
 
 

 