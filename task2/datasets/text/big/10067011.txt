BabaKiueria
 
 
{{Infobox film
| name           = BabaKiueria
| image          = BabaKiueria title card.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        =  Don Featherstone
| producer       = Julian Pringle
| writer         = 
| screenplay     = Geoffrey Atherden
| story          = 
| based on       =  
| narrator       = 
| starring       = {{plainlist| Michelle Torres
* Bob Maza
* Kevin Smith}}
| music          = Peter Crosbie
| cinematography = Julian Penney
| editing        = Michael Honey
| studio         = 
| distributor    = 
| released       = 1986
| runtime        = 29:00
| country        = Australia
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    =
}} satirical film Aboriginal Australians Australians of European descent.

==Synopsis== colonised the fictitious country of BabaKiueria, a land that has long been inhabited by white natives, the BabaKiuerians. 

The opening scene depicts a group of Aboriginal Australians in military uniforms coming ashore in a land they have not previously been to. In this land, they discover a number of European Australians engaged in stereotypical European Australian activities. The Aboriginal Australian explorers approach the group and the expeditions leader asks them, "What do you call this place"? One of the Europeans replies, "Er... Barbecue Area".
 Michelle Torres).         
 Sir Joh Bjelke-Petersen.         

The inversion of reality in BabaKiueria highlights the unfairness of Australias past and present Aboriginal policies and the entrenched racism in society. This subversion of normality allows viewers to see what is wrong when one group tries to control and dominate another and questions the fairness of the current power structure in Australia. 

==Production== AFL Game). It was screened in the Australian Museum for many years. 

==Impact and Reception==
Babakiueria has been used at times to educate police officers. 

After it was screened at the Message Sticks Festival in 2012, Mahjid Heath noted that "issues we were struggling with in the early 80s are still relevant and still define the political and national discourses today." 

==Cast== Michelle Torres - (Presenter Duranga Manika)
* Bob Maza - (Wagwan - Government Minister)
* Kevin Smith - (Police Superintendent) 
* Tony Barry - (Father (Mr. Smith))
* Cecily Polson - (Mother)
* Kelan Angel - (Son)
* Marguerita Haynes - (Daughter)
* Garry Williams - (Explorer)
* Soul Beliear - (Police Sergeant #1)
* Terry Reid - (Police Sergeant #2)
* Athol Compton - (Newsreader)
* Kati Edwards - (Grandmother)
* Yvonne Shipley - (Wealthy Woman)

==Awards==
{| class="wikitable"  
|-
! Ceremony
! Year
! Result
|- United Nations Media Peace Prize
| 1987
|    
|}

==Broadcasting==
{| class="sortable wikitable"
|-
! Country
! Network(s)/Station(s)
|- ABC
|}

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 