Ponmana Selvan
{{Infobox film
| name           = Ponmana Selvan
| image          = 
| caption        = 
| director       = P. Vasu
| producer       = V. N. Selvakumar
| writer         = P. Vasu
| starring       = Vijayakanth Shobana Gemini Ganesan B. Saroja Devi Goundamani Jaishankar Radha Ravi
| music          = Ilaiyaraaja
| cinematography = M. C. Sekar
| editing        = P. Mohan Raj
| studio         = V. N. S. Films
| distributor    = V. N. S. Films
| released       =  
| runtime        = 139 mins
| country        =   India Tamil
}}
 1989 Tamil language drama film directed by P. Vasu. The film features Vijayakanth, Shobana, Gemini Ganesan and B. Saroja Devi in lead roles. 
The film, had musical score by Ilaiyaraaja and was released on August 15, 1989.  

==Cast==
* Vijayakanth
* Shobana
* Gemini Ganesan
* B. Saroja Devi
* Goundamani
* Jaishankar
* Radha Ravi
* V. K. Ramasamy (actor)|V. K. Ramasamy
* S. S. Chandran
* Ravikiran
* Udayashankar
* Pradeep
* Vaithi
* Dr. Muthu
* Rafi
* Vidhya
* Sindhu
* Anjana
* Srilatha

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali || 04:35
|- Gangai Amaran || 04:30
|-
| 3 || Kana Karunguyile || Mano (singer)|Mano, K. S. Chithra || 04:28
|-
| 4 || Nee Pottu Vacha || Malaysia Vasudevan, Mano (singer)|Mano, K. S. Chithra || 04:26
|-
| 5 || Poovana || Mano (singer)|Mano, Vani Jayaram || 04:38
|-
| 6 || Thoppile Irunthaalum || Malaysia Vasudevan || Ilaiyaraaja || 04:33
|}

==References==
 

==External links==
 

 

 
 
 
 
 
 


 