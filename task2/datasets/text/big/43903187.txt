She Was Like a Wild Chrysanthemum
{{Infobox film
| name           = She Was Like a Wild Chrysanthemum 野菊の如き君なりき Nogiku no gotoki kimi nariki
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Keisuke Kinoshita
| producer       = 
| writer         = 
| screenplay     = Keisuke Kinoshota  (screenplay)  Saicho Ito  (novel) 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = Hiroshi Kusuda
| editing        = 
| studio         = Shochiku films
| distributor    = 
| released       =   
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
She Was Like a Wild Chrysanthemum (野菊の如き君なりき Nogiku no gotoki kimi nariki), also known as You Were Like a Wild Chrysanthemum or My First Love Affair, is a Japanese film directed by Keisuke Kinoshita which was initially released in 1955.  It is based on a novel by Saicho Ito.       The film is set in the Meiji period.     The story tells of the forbidden romance between teenaged cousins Masao and Tamiko.   The lovers were also inhibited by a marriage arranged by her family for Tamiko and Masao needing to go away to school.   The story is essentially a flashback as remembered by the 73 year old Masao, played by Chishū Ryū, as he rides a boat back to his home village where they first fell in love.          The flashback scenes are filmed using and oval-shaped mask typically associated with silent films.    According to Alexander Jacoby, this masking gives the film "an appropriately nostalgic tone."   Film critic Donald Richie describes the film style as representing "Meiji daguerrotypes."   

Jacoby rates She Was Like a Wild Chrysanthemum to be "among the most purely moving of Japanese films," despite its "occasional naivety."   He attributes this particularly to Kinoshotas "simple techniques," including "judicious choice of camera position," and to the excellent performances.   Richie regards the film as one of Kinoshitas "most successful" in his later style.   Joseph L. Anderson praises the films photography, particularly the "rich blacks" and Kinoshitas "evocation of   area."   Jacek Kloiowski, et al, regard the film as "one of the most sincere and purest films of its type in Japanese cinema," noting that it marks a return to "pastoral lyricism" for Kinoshota after focusing his films on social issues for the previous few years. 
 Blue Ribbon Award for cinematography for the same two films. 

==References==
 

 

 
 
 
 
 
 
 
 