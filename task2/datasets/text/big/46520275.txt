Promises in the Dark (film)
{{Infobox film
| name           = Promises in the Dark
| image          = Promises in the Dark poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jerome Hellman
| producer       = Jerome Hellman 
| writer         = Loring Mandel
| starring       = Marsha Mason Ned Beatty Susan Clark Michael Brandon Kathleen Beller Paul Clemens
| music          = Leonard Rosenman
| cinematography = Adam Holender 
| editing        = Bob Wyman 
| studio         = Jerome Hellman Productions
| distributor    = Warner Bros. Orion Pictures
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Promises in the Dark is a 1979 American drama film directed by Jerome Hellman and written by Loring Mandel. The film stars Marsha Mason, Ned Beatty, Susan Clark, Michael Brandon, Kathleen Beller and Paul Clemens. The film was released by Warner Bros. and Orion Pictures on November 2, 1979.  

==Plot==
 

== Cast == 
*Marsha Mason as Dr. Alexandra Kendall
*Ned Beatty as Bud Koenig
*Susan Clark as Fran Koenig
*Michael Brandon as Dr. Jim Sandman
*Kathleen Beller as Elizabeth (Buffy) Koenig
*Paul Clemens as Gerry Hulin
*Donald Moffat as Dr. Walter McInerny
*Philip Sterling as Dr. Frucht
*Bonnie Bartlett as Nurse Farber James Noble as Dr. Blankenship
*Arthur Rosenberg as Emergency Room Doctor
*Peggy McCay as Miss Pritikin
*Robert Doran as Alan
*Lenora May as Sue
*Alexandra Johnson as Ellie
*Fran Bennett as Emergency Room Nurse
*Eloise Hardt as Woman in Restaurant
*Bernie Kuby as Tony in Buds Office
*Karen Anders as Secretary in Buds Office
*Edith Fields as Mrs. Gans
*Alice Beardsley as Mrs. Keyes 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 