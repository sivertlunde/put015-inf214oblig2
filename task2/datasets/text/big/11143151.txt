Uruvangal Maralam
 
{{Infobox film
| name           = Uruvangal Maralam
| image          = Uruvangalfilm.jpg
| image_size     =
| caption        = Cover of Vinyl Record
| director       = S.V.Ramanan
| producer       = S.V.Ramanan Ramji Raghu
| writer         =
| narrator       =
| starring       = Sivaji Ganesan Kamal Haasan Rajinikanth Jaishankar Y. Gee. Mahendra Suhasini
| music          = S.V.Ramanan
| cinematography =
| editing        =
| distributor    =
| released       = 14 January 1983
| runtime        =
| country        =  India Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1983 Cinema Tamil Cinema Indian feature directed by S.V.Ramanan, starring Y. Gee. Mahendra in lead role paired by Suhasini. The film has a galaxy of supporting stars like Sivaji Ganesan, Kamal Haasan, Rajinikanth and Jaishankar playing different forms of GOD. This movie is literally a remake of the English movie Oh, God!.

==Summary==

The story revolves around Y.G.Mahendra who believes in God more than anything else in this world. He is once called by God Sivaji Ganesan who tells him that he has to inform people about the various things about to happen in the future. Y.G starts his task of telling people, but no one believes him in the beginning. Slowly when what he says starts occurring people term him a powerful monk and start coming to him. God takes several other famous actor forms Kamal Hassan, Rajinikanth, Jaishankar and keeps hinting the world through YGM. One day, God tells YGM that his son is going to die. The tables turn and YGM starts cursing God. Does God win YGM,s faith back is the story. Rajinikant coming out of Raghavendra temple, an atheist Kamal playing the role of God himself and finally God trying to prove to the court that he indeed is God by doing the disappearing act are some of the highlight moments of the movie. 

==Cast==
*Sivaji Ganesan
*Kamal Haasan
*Rajinikanth
*Jaishankar
*Y. G. Mahendran
*Suhasini

==References==
 

==External links==
*  

 
 
 
 


 