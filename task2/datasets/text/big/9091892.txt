Dancers in the Dark
{{Infobox Film
| name           = Dancers in the Dark
| image          = Dancersinthedark.jpg
| image_size     = 
| caption        =  David Burton
| producer       = 
| writer         = James Ashmore Creelman (play) Herman J. Mankiewicz
| narrator       = 
| starring       = Miriam Hopkins Jack Oakie George Raft
| music          = Dana Suesse
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| released       = 1932
| runtime        = 74 min.
| country        =    English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Dancers in the Dark is a 1932 film about a taxi dancer (Miriam Hopkins), a big band leader (Jack Oakie), and a gangster (George Raft). Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 21   The movie was written by Herman J. Mankiewicz (Citizen Kane), Brian Marlow, and Howard Emmett Rogers from the play Jazz King by James Ashmore Creelman, and directed by David Burton.

==Cast==
*Miriam Hopkins as Gloria Bishop
*Jack Oakie as Duke Taylor
*George Raft as Louie Brooks
*William Collier, Jr. as Floyd Stevens
*Eugene Pallette as Gus
*Lyda Roberti as Fanny Zabowolski
*Paul Fix as Benny
*Adelaide Hall (Halls singing voice is used but she is uncredited on the movie credits)

==References==
 

==External links==
*  

 
 
 
 
 
 


 