Lifeguard (film)
 
{{Infobox film
| name           = Lifeguard
| image          = Lifeguardfilm.jpg
| image_size     =
| caption        =
| director       = Daniel Petrie
| producer       = Ron Silverman
| writer         = Ron Koslow
| narrator       = Stephen Young Parker Stevenson Kathleen Quinlan
| music          = Dale Menten
| cinematography = Ralph Woolsey
| editing        = Art J. Nelson
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 96-minute
| country        = United States
| language       = English
| budget         =
}}

Lifeguard is a 1976 drama movie made by Paramount Pictures. It was directed by Daniel Petrie, based upon a screenplay by Ron Koslow.  It stars Sam Elliott, Anne Archer, Steve Burns, Parker Stevenson, and Kathleen Quinlan.    

Sam Elliott plays Rick Carlson, a 32-year-old lifeguard on a Southern California beach who is prompted to question his goals in life when he receives an invitation to his 15-year high school reunion. At this reunion, he meets his high-school sweetheart, Cathy (played by Anne Archer), now the divorced mother of a young son. They resume their past relationship and Cathy encourages Rick to take a job as a Porsche salesman, offered to him by another high-school classmate. Meanwhile, Rick must deal with Wendy (played by Kathleen Quinlan), a lonely teenage girl who has developed a crush on him.

==Principal cast==
*Sam Elliott as Rick Carlson
*Anne Archer as Cathy Stephen Young as Larry
*Parker Stevenson as Chris
*Kathleen Quinlan as Wendy
*Steve Burns as Machine Gun (Harold) Sharon Weber as Tina

==See also==
* Baywatch, TV film and TV series starring Lifeguard star Parker Stevenson

==Notes==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 

 