The Sting II
 
{{Infobox film
| name           = The Sting II
| image          = The Sting II DVD cover.jpg
| caption        = DVD cover
| director       = Jeremy Kagan
| producer       = Jennings Lang
| writer         = Dean Riesner David S. Ward
| starring = {{plainlist|
* Jackie Gleason
* Mac Davis
* Teri Garr
* Karl Malden
* Oliver Reed
* Ron Rifkin John Hancock
}}
| music          = Lalo Schifrin Bill Butler
| studio         = Universal Studios
| editing        = David Garfield
| distributor    = Universal Studios
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $6,347,072 (USA)
}}

The Sting II is a 1983 film sequel to The Sting. Directed by Jeremy Kagan and written by David S. Ward (also author of the original movie), it stars Jackie Gleason, Mac Davis, Teri Garr, Karl Malden and Oliver Reed. The film was nominated for an Oscar for Best Musical Score, which was composed by Lalo Schifrin.

In their featured review, The New York Times stated that "The Sting II is better than might have been anticipated." The review went on to say that "Teri Garr lights up the film as a kind of small-time Mata Hari, and Karl Malden amusingly plays a swell named Macalinski."

==Plot==
The Great Depression is over. King of the con men Fargo Gondorff is released from prison and reassembles his cronies for another con, out to avenge the murder of his lifelong pal Kid Colors.

Gondorffs young protege Jake Hooker attempts to pull a scam on wealthy "Countess Veronique," who instead pulls one on him and turns out to be a grifter herself named Veronica.

Coming up with a boxing con, Gondorffs goal is to sting both Lonnegan, the notorious banker and gangster who wants revenge from a previous con, and Gus Macalinski, a wealthy local racketeer. One or both of them is behind Kid Colors death.

Hooker pretends to be a boxer who is about to throw a big fight. Macalinski is not only hoodwinked into losing hundreds of thousands of dollars, he is talked into changing his original wager by Lonnegan. While one gangster takes care of the other, Gondorff and Hooker head for the train station with a bag full of money, tickets out of town and a final twist from Veronica.

==Cast and crew==
* Jackie Gleason as Fargo Gondorff
* Mac Davis as Jake Hooker
* Teri Garr as Veronica
* Oliver Reed as Doyle Lonnegan
* Karl Malden as Gus Macalinski
* José Pérez as Carlos
* Ron Rifkin as Eddie
* Larry Hankin as Handicap John Hancock as Doc
* Tim Rossovich as Typhoon Taylor
* Val Avery as OMalley
* Adam Hollander as Delivery Boy
* Dave Cadiente as Chico Torres
* Michael Alldredge as Big Ohio
* Bert Remsen as Kid Colors
* Francis X. McCarthy as Lonnegans Thug (credited as Frank McCarthy)
* Cynthia Cypert as Girl in Club 
A Jennings Lang Production. Written by David S. Ward. Music by Lalo Schifrin. Special Visual Effects by Albert Whitlock. Director of Photography Bill Butler, A.S.C. Produced by Jennings Lang. Directed by Jeremy Paul Kagan.

==Continuity==
The Sting II is not a direct sequel to The Sting in that the former applies a number of retroactive continuity changes to the characters and events depicted in the latter, particularly with regard to the three primary returning characters.  Henry Gondorff from the original film is now named Fargo Gondorff.  Johnny Hooker is now named Jake Hooker and has an extensive background in boxing, of which there was no suggestion in the first film. Doyle Lonnegan from The Sting is referred to only as "Lonnegan" in The Sting II; his first name is never mentioned.  However, he is now from Philadelphia, whereas he was from New York in the first film.  His basic persona is changed also, as he is depicted in the sequel as enjoying the art of running his own con game as part of an elaborate and slowly unwinding plot for vengeance upon Gondorff and Hooker.  By contrast, the Lonnegan depicted in the original would have been much more likely to exact his revenge upon the pair by simply and immediately having them killed.

Director Jeremy Kagan said, "The Sting II is inspired by and is an expansion of the first Sting, rather than a continuation. The principal characters of Fargo Gondorff and Jake Hooker are based on two very famous real-life con men, and are totally different from the two characters in the original." Gaul Lou. (1983, February 18).
 ", Beaver County Times  

==Production==
The Sting II was partially filmed at the Santa Cruz Beach Boardwalk.  The Giant Dipper roller coaster is renamed Cyclone for the movie.

==Awards==
The film was nominated for an Oscar in 1984 for Best Music, Original Song Score and Its Adaptation or Best Adaptation Score by
Lalo Schifrin.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 