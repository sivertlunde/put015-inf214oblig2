Lady Libertine
{{Infobox Film  |
  name     = Lady Libertine|
  image          = Ladylibertine.jpg|
  caption       = Lady Libertine DVD cover|
  director       = Gérard Kikoïne |
  writer         = Harry Alan Towers|
  starring       = Christopher Pearson Jennifer Inch Sophie Favier Alain Dumaurier|
  producer         = Wilfrid Dodd Harry Alan Towers|
  music         = Marc Hillman|
cinematography = Gérard Loubeau|
  distributor    = Playboy Enterprises  |
  released   = 1983 |
  runtime        = 83 minutes|
country = United Kingdom|
language = English |
}} 1983 soft core erotic film by Gérard Kikoïne. This Playboy production, released theatrically in Europe and on television in the US, is an adaptation of the Victorian novel (1902) Frank and I by Bill Adler.  French actress turned famous TV host   unsuccessfully sued to stop its release. 

Gérard Kikoïne had quite a large budget for this film and was assisted by French set designer Jean-Charles Dedieu. Therefore production values are quite good, especially since they involve a lot of period costumes and sets.   Jennifer Inch is playing the role of Frank, the orphan boy, and of Frances. Sophie Favier plays the role of the mistress of Charles de Beaumont.

==Plot==
In the 1880s, a rich nobleman from London, Charles de Beaumont (Christopher Pearson), meets "Frank", an adolescent, and offers hospitality to the young orphan. According to the usages of his caste, he decides to educate him by himself. But Frank is not an obedient pupil and Charles is surprised when he discovers that Frank is not a boy as he believed, but a beautiful young girl named Frances with a dark secret.

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 


 