The Missing Picture (film)
 
{{Infobox film
| name           = The Missing Picture
| image          = The Missing Picture 2013 poster.jpg
| caption        = Film poster
| director       = Rithy Panh
| producer       = Catherine Dussart 
| writer         = Rithy Panh Christophe Bataille
| narrator       = Randal Douc Jean-Baptiste Phou 
| music          = Marc Marder
| cinematography = Prum Mesa
| editing        = Rithy Panh Marie-Christine Rougerie 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Cambodia France 
| language       = French   
| budget         = 
}}

The Missing Picture ( ) is a 2013 Cambodian-French  documentary film directed by Rithy Panh about the Khmer Rouge.    It was screened in the Un Certain Regard section at the 2013 Cannes Film Festival    where it won the top prize.    It was also screened in the World Cinema section at the 2013 Cinemanila International Film Festival where it won the Grand Jury Prize.   
 Best Foreign Language Film at the 86th Academy Awards, and was nominated.    Approximately half of the film is news and documentary footage, while the other half uses clay figurines to dramatise what happened in Cambodia when Pol Pot came to power.    

==Cast==
* Randal Douc as the narrator (original French version)
* Jean-Baptiste Phou (English version)  

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Cambodian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 