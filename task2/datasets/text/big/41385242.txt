What We Do in the Shadows
 
 
{{Infobox film
| name = What We Do in the Shadows
| image = What We Do in the Shadows poster.jpg
| alt = 
| caption = Theatrical release poster
| director = {{Plainlist|
* Taika Waititi
* Jemaine Clement}}
| producer = {{Plainlist|
* Taika Waititi
* Chelsea Winstanley
* Emanuel Michael}}
| writer = {{Plainlist|
* Taika Waititi
* Jemaine Clement}}
| starring = {{Plainlist|
* Taika Waititi
* Jemaine Clement
* Rhys Darby
* Jonathan Brugh
* Cori Gonzalez-Macuer
* Stu Rutherford}}
| music = Plan 9 Music
| cinematography = {{Plainlist|
* Richard Bluck
* D.J. Stipsen}}
| editing = {{Plainlist|
* Tom Eagles
* Yana Gorskaya
* Jonathan Woodford-Robinson}}
| production companies = {{Plainlist|
* Funny or Die
* New Zealand Film Commission}}
| distributor = Madman Entertainment
| released =  
| runtime = 85 minutes  
| country = New Zealand
| language = English
| budget = $1.6 million
| gross = $6.6 million 
}} New Zealand mockumentary horror comedy film about a group of vampires who live together in Wellington. Directed and written by Taika Waititi and Jemaine Clement, who also star in the film, it premiered at the Sundance Film Festival in January 2014.      

==Plot==
Viago, Vladislav, Deacon, and Petyr are four vampires who share a flat in the Wellington suburb of Te Aro. Although Viago, Vladislav, and Deacon are all several centuries old, they have retained normal human appearances, but the 8,000-year-old Petyr resembles Count Orlok and acts more savagely than the younger vampires. Each night, Viago, Vladislav, and Deacon prowl the streets of Wellington searching for people to kill, but must stay in the flat during the day to avoid sunlight—which is lethal to vampires—therefore they have not adapted to 21st century life. Deacon has a human servant—Jackie—who runs errands for the vampires, but she is frustrated that Deacon will not turn her into a vampire. Jackie leads her ex-boyfriend Nick to the vampires flat so they can drink his blood, but he escapes before they kill him. As he leaves the flat, Petyr attacks and accidentally turns Nick into a vampire.

Two months later, the vampires accept Nick into their group, and also bond with his friend Stu, a computer programmer who shows them how to use modern technology. Nick struggles to adapt to life as a vampire and carelessly reveals his secret to strangers he meets in bars and clubs. One of these strangers turns out to be a vampire hunter, who breaks into the flat during the day and kills Petyr by exposing him to sunlight. The vampires are furious when they discover Nick has indirectly caused Petyrs death, and banish him from the flat. Some time later, the vampires are invited to a masquerade ball, where they meet other undead and supernatural beings such as zombies and witches, as well as Vladislavs ex-girlfriend Pauline, who he nicknames "The Beast" due to their hard break-up. Nick, Stu and Jackie are also in attendance, and to Deacons annoyance Nick has turned Jackie into a vampire. When Pauline realises Stu and the camera crew are human, the other guests try to kill them and Vladislav fights Paulines new boyfriend. The vampires manage to escape the ball with Stu and the film crew, but encounter a group of werewolves who are about to transform in a forest. Stu and one of the cameramen are caught and attacked by the werewolves, and assuming Stu is dead, the vampires grieve for him.

A while later, Stu reappears, having been turned into a werewolf himself—eventually reconciling them with the vampires, as do Nick and the other vampires. Despite their differences with Nick, the other vampires have adapted to early twenty-first century life, relationships, and technology by the films end.

==Cast==
* Taika Waititi as Viago, aged 379
* Jemaine Clement as Vladislav, aged 862
* Jonathan Brugh as Deacon, aged 183
* Ben Fransham as Petyr, aged 8,000
* Jackie Van Beek as Jackie
* Cori Gonzalez-Macuer as Nick
* Stu Rutherford as Stu
* Rhys Darby as Anton
* Ethel Robinson as Katherine

==Production==
The film is based on a 2006 short film of the same name by Waititi and Clement. The new film was shot in Wellington in September 2012, and is Waititis first feature since Boy (2010 film)|Boy.  

==Release==
The film was released in a limited release on February 13, 2015 in New York and Los Angeles, followed by a rare screening in San Francisco, Irvine, Philadelphia, Boston, Seattle, and Washington, D.C.  The film received a regional release in the U.S. in March 2015, by Unison Films in association with Funny or Die and Paladin Pictures. 

===Critical response===
What We Do in the Shadows received very positive reviews from critics  and has a "Certified Fresh" score of 96% on Rotten Tomatoes based on 118 reviews with an average rating of 7.8 out of 10. The critical consensus states "Smarter, fresher, and funnier than a modern vampire movie has any right to be, What We Do in the Shadows is bloody good fun."  The film also has a score of 75 out of 100 on Metacritic based on 31 critics indicating "generally favourable reviews. 
 mockumentaries such Best in The Telegraph s Tim Robey found it "desperately funny". 

Film International, in a positive review, commended the film for noting, with a double of Count Orlok locked in the vampires basement, that the true vampire film tradition is repressed by the current craze. 

Variety (magazine)|Variety was more critical, writing that "Some genre fans who prefer the silly to the satiric may bite, but the anemic pic isn’t remotely weird or witty enough for cult immortality." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 