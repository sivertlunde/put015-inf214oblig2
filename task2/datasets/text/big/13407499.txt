Asfaltevangeliet
{{Infobox film
| name         = Asfaltevangeliet
| image        = 
| caption        =
| writer       = David Åleskjær (also novel)
| starring     = Tommy Karlsen, Per Christian Ellefsen, Brit Elisabeth Haagensli, Helene Rask, Bjørn Sundquist, Sverre Anker Ousdal
| director     = David Åleskjær
| producers     = David Åleskjær
| cinematography = Svein-Erik Bjørnstad
| composer      =
| editor       =
| distributor  =
| released     = Norway 2004
| runtime      =
| language     = Norwegian
| budget       =
| music        =
}}

Asfaltevangeliet (The Gospel of Asphalt) is a Norwegian independent movie from 2004, directed by David Åleskjær, son of Åge Åleskjær. It is based on David Åleksjærss book by the same name. It stars many know Norwegian actors and celebrities, among them Per Christian Ellefsen, Brit Elisabeth Haagensli, Helene Rask, Bjørn Sundquist and Sverre Anker Ousdal. Tommy Karlsen (known from the film Døden på Oslo S) plays the role of Jesus.

==Plot==
Asfaltevangeliet shows the scenario of Jesus Christ coming to Oslo, Norway in present day. Jesus preached a radical message of love, forgiveness and the Kingdom of God. He is disputed by the authorities and the religious elite, but gets a following among sinners, prostitutes and drug addicts. Jesus also says that he have to die for the sins of mankind, and in the films climax he is killed by a mob in Oslos main street.

==External links==
*  

==Main actors and actresses==
*Tommy Karlsen (Jesus)
*David Åleskjær (also the writer and editor)
*Lene Alexandra (TV-Shop lady)
*Nina Andresen Borud (Maya)
*Håvard Bakke (well known Norwegian actor that plays Brother of lost son)
*Lene Elise Bergum (Woman who bothers police officer)
*Per Christian Ellefsen (well known Norwegian actor that plays a Workaholic)
*Dag Håvard Engebråten
*Brit Elisabeth Haagensli (well known Norwegian actress)
*David Hasseløy
*Sophia Kaushal
*Caroline Lervaag
*Sverre Anker Ousdal (well known Norwegian actor that plays Father of the lost son)
*Helene Rask
*Klaus Sandvik (Man / soldier who loses an ear)
*Kari Simonsen (well known Norwegian actress)
*Eirik Stillingen
*Hans Ivar Stordal (Jonas)
*Bjørn Sundquist (well known Norwegian actor that plays an important Police officer)
*Charlotte Sveinsen
*Espen Thoresen (well known Norwegian comedian and radio- / TV-host). Espen is also famous for adding a radio show program title to his own name - now his name is Espen Thoresen Vaersågod Takkskalduha, named after his own radio show Espen Thoresen Værsågod at Norwegian BroadCasting radio NRK)
*Bente Wethal

 
 
 


 