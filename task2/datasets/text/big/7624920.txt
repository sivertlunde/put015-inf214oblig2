Naa Autograph
{{Infobox film
| name        = Naa Autograph
| image       = Naa Autograph poster.jpg Cheran
| Mallika Kanika Kanika Prakash Raj
| director    = S. Gopal Reddy
| producer    = Bellamkonda Suresh
| distributor =
| released    = 11 August 2004
| runtime     =
| music       = m.m.keeravani
| language    = Telugu
| country     = India
| budget      =
}} Tollywood film Mallika and Tamil film Cheran and Sneha in the lead roles.

==Plot==
The film begins with Seenu (Ravi Teja), who runs an advertising agency in Hyderabad, India|Hyderabad, setting off on a journey, distributing wedding invitations for his forthcoming wedding. Along the way, he encounters various individuals from his past, who bring back memories of three women that have had an impact on his love life.

First, Seenu goes to his village in Andhra Pradesh where he spent his childhood. He remembers the antics he had done during his childhood, his school and his first love Vimala (Mallika (actress)|Mallika). After finishing school, Vimala got married and she never met Seenu again. In the present day, Seenu invites everyone from his village for his wedding including his friends, his old school master and Vimala, her husband and her three children.

Then, Seenu goes to a village in Kerala where he moved after his father Sriram (Paruchuri Venkateswara Rao), a postmaster, was transferred there. He had his college education there. He was constantly bullied by the students there as he did not know Malayalam, but eventually managed to become friends with a fellow student Satyam (Sunil (actor)|Sunil), whose mother tongue was also Telugu language|Telugu. Satyam served him as a translator. Seenu soon fell in love with Lathika (Gopika), a Malayali girl. Lathika too reciprocated his feelings and soon their relationship becomes intimate. However, when Lathikas family came to know of this, they drove out Seenus family from the village and got her married to a local youth who also had a crush on her. In the present day, Seenu reunites with Satyam in Kerala and invites him for his wedding. When they arrive at Lathikas house to invite her too, Seenu becomes upset to see that his former love had become a widow (her husband had died in a boating accident a couple of years prior).

After they were thrown out of the village in Kerala, Seenu and his parents moved to Hyderabad. Seenu had not got over Lathika and became an emotional wreck, and started to smoke and drink. He remained an emotional wreck until he met Divya (Bhumika Chawla), who worked in an advertisement agency headed by one Prakash (Prakash Raj). Divya, in fact, secured this job for Seenu after she saw him and his friend Bhagavan (Krishna Bhagavan) distributing pamphlets to the passengers in a bus, seeking job opportunities, hoping one of them may help them out in finding a job. Soon Seenu and Divya became close friends and Divya instilled confidence, unearthed his hidden talents and taught him the lesson that one has to go ahead in life without looking back. Soon, Divya revealed that her widowed mother was a paralytic patient and that she worked to take care of her mother. After Divyas mother died, Seenu, who had fallen in love with Divya, proposed her, but she rejected it, saying that she was happy to be just a good friend.

Finally, Seenu decided to get married to a girl of his parents choice, Sandhya (Kanika (actress)|Kanika), for which he is distributing invitations in the present day. Everyone he invited, including Vimala, Lathika and Divya, attend his wedding. The film ends with Seenu reflecting on his lifes journey to the audience.

==Cast==
*Ravi Teja as Seenu
*Bhumika Chawla as Divya
*Gopika as Lathika Mallika as Vimala Kanika as Sandhya Sunil as Satyam
*Prakash Raj as Prakash
*Krishna Bhagavan as Bhagavan
*Paruchuri Venkateswara Rao as Sriram

==Soundtrack==

{{tracklist
| extra_column = Singer(s)
| all_music = M.M. Keeravani Chandrabose
| title1 = Mounamgane Chitra
| length1 = 5:19 
| title2 = Gurtukostunnayi KK
| length2 = 4:56
| title3 = Duvvina Talane
| extra3 = M.M. Keeravani, Sumangali
| length3 = 3:09
| title4 = Manmadhude
| extra4 = Sandeep Bhowmik, Ganga, M.M. Keeravani
| length4 = 4:35
| title5 = Gamma Gamma Hangamma 
| extra5 = S. P. Balasubrahmanyam, Sri Vardhini, Poornima
| length5 = 4:42
| title6 = Nuvvante Pranamani
| extra6 = Vijay Yesudas
| length6 = 3:57 
}}

==Box Office==
The film, despite being a remake, was highly successful at the box office.

==Trivia== Tamil Version Autograph (2004 film)|Autograph.

==External links==
* 
*  

 

 
 
 
 
 