The Assassination of Matteotti
{{Infobox film
| name = The Assassination of Matteotti
| image = Il delitto Matteotti.jpg
| caption =
| director = Florestano Vancini
| writer =
| starring = Mario Adorf Franco Nero
| music = Egisto Macchi
| cinematography = Dario Di Palma
| editing = Nino Baragli
| producer =
| distributor =
| released =  
| runtime = 120 minutes
| country = Italy
| language = Italian
| budget =
}}
The Assassination of Matteotti ( ) is a 1973 Italian historical drama film directed by Florestano Vancini. The film tells the events that led to the tragic end of Giacomo Matteotti and to the establishment of the dictatorship of Benito Mussolini in Italy.     It was awarded with the Special Jury Prize at the 8th Moscow International Film Festival.     

== Cast ==
*Mario Adorf as Benito Mussolini
*Franco Nero as Giacomo Matteotti
*Umberto Orsini as Amerigo Dumini
*Vittorio De Sica as Mauro Del Giudice
*Renzo Montagnani as Umberto Tancredi
*Gastone Moschin as Filippo Turati
*Stefano Oppedisano as Piero Gobetti
*Manuela Kustermann as Ada Gobetti
*Riccardo Cucciolla as Antonio Gramsci
*Damiano Damiani as Giovanni Amendola
*Giulio Girola as Vittorio Emanuele III Cesare Rossi
*Pietro Biondi as Filippo Filippelli
*Giorgio Favretto as Giovanni Gronchi Archbishop Pietro Gasparri
*Ezio Marano as Alcide De Gasperi
*José Quaglio as Questor Bertini
*Gianni Solaro as General Attorney Crisafulli
*Gino Santercole as General of the Militia
*Piero Gerlini as Giuseppe Emanuele Modigliani
*Franco Silva as Benedetto Fasciolo
*Maurizio Arena as "Compagno" Romolo

==References==
 

==Further reading==
* Lucio Battistrada, Florestano Vancini, Il delitto Matteotti, Capelli, 1973

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 