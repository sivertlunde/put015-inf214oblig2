Quigley Down Under
 
{{Infobox film name          =Quigley Down Under image         =Quigley down under.jpg image_size    = caption       =Theatrical release poster by Steven Chorney director      =Simon Wincer producer      =Stanley OToole Alexandra Rose Megan Rose writer  John Hill starring      =Tom Selleck Laura San Giacomo Alan Rickman music         =Basil Poledouris
|cinematography=David Eggby editing       =Peter Burgess studio        =Pathé|Pathé Entertainment distributor   =Metro-Goldwyn-Mayer released      =17 October 1990 runtime       =119 minutes country       =Australia/United States language      =English budget        =$18 million  gross         =$21,413,105
}}
 Australian Western western film. Starring Tom Selleck, Alan Rickman and Laura San Giacomo, it was directed by Simon Wincer.

==Plot== weapon of Sharps Buffalo Rifle. He answers a newspaper advertisement that asks for men with a special talent in long-distance shooting with four words, "M. Quigley 900 yards," written on a copy of the advertisement, punctuated by several closely spaced bullet holes.
 station in the Western Australian outback. Upon seeing Quigley, Cora consistently calls him "Roy," much to Quigleys annoyance.
 Dodge City. sharpshooting skills will be used to eradicate the increasingly elusive native Indigenous Australians|Aborigines. Marstons killing of aborigines is technically illegal, but he implies that it is tolerated by the local military police with whom Marston has "an arrangement." Quigley, who believed he was hired to shoot dingoes, finds the idea abhorrent.

Quigley not only turns down the job offer, he throws Marston out of his house twice. Marstons Aborigine manservant knocks Quigley over the head and Marstons men beat Quigley and Cora into unconsciousness and dump them in the Australian Outback two days away with no water and little chance of survival. However, Quigley manages to kill the two men who took them into the Outback, after which he and Cora are rescued by Aborigines. Cora reveals that she was from Texas. When her home was attacked by Comanches, she hid in the root cellar. To prevent her crying infant son from revealing their hiding place, she covered the babys mouth and unintentionally suffocated him. When her husband, Roy, arrived home and learned of the childs death, he took Cora to Galveston, Texas and put her alone on the first ship leaving, which happened to be bound for Australia. Recovering, they witness an attack by Marstons men on the Aborigines who helped them. Quigley kills three of Marstons men; a fourth escapes.

Escaping on a single horse, they encounter Marstons men driving Aborigines over a cliff. Quigley kills three more of the men and Cora finds an orphaned baby among the dead Aborigines. Caring for the baby helps Cora overcome her tragic past and she slowly begins to recognize Quigley as his real self and stops calling him Roy. Quigley rides alone to a nearby town leaving Cora and the infant Aborigine in the desert with food and water. In town, he obtains new ammunition from a local German gunsmith named Grimmelman (Ron Haddrick) who is eager to help when he learns Quigley plans to kill Marston. Quigley also learns that he has become a legendary hero among the Aborigines. Marstons men recognize Quigleys horse and attack him, cornering him in a burning building. Escaping through a skylight, Quigley returns to the gunsmith and learns Grimmelmans wife was killed in the crossfire. He kills all but one of Marstons men; he sends the sole survivor back to Marston to tell him that Quigley is coming for him. While he is away, Cora and the baby are attacked by dingoes. The baby cries and Cora nearly repeats her actions from the past; however she overcomes her insecurity and tells the baby to cry as loud as he wants. Using guns taken from Marstons men, Cora shoots the dingoes and saves herself and the baby.

Quigley returns to Cora and baby Little Bitty with supplies, a fresh horse, and a dress for Cora. Though initially concerned at the sight of the dead dingoes, Quigley is relieved to find both Cora and the child unharmed. He is bemused that Cora continues to call him Roy, but she seems to be less separated from reality, which heartens Quigley. Cora gives the baby to Aborigines in the town after Quigley tells her that she has a right to happiness. The next morning, Quigley safely leaves Cora with Grimmelman and after telling her that she looks pretty in the sunshine, turns and rides away to Marstons ranch. Marston, alerted to Quigleys return by the injured man from town, prepares his men to kill Quigley. At first Quigley maintains his distance and shoots Marstons men from his location in the hills. He even saves a round by patiently waiting until one of Marstons men walks in front of another man, at which point Quigley shoots through them both. Marston gradually loses more and more men to Quigley, until Quigley is eventually captured by Marstons last two men. Marston, who has noticed that Quigley only ever carries a rifle, decides to give him a lesson in the "quick-draw" style of gunfighting. As the two face off, Marston makes the first move, but is beaten to the draw by Quigley, who shoots Marston and his two remaining men. As Marston lies dying, referring to an early conversation concerning Colt revolvers, Quigley tells him, "I said I never had much use for one. Never said I didnt know how to use it."
 British Major Ashley-Pitt (Chris Haywood), the official with whom Marston had "an arrangement" and whom Quigley had met before, arrives. Ashley-Pitt informs Quigley that he is under arrest for murder and that he will be hanged. Quigley says that he "wont swing from no gallows," and Ashley-Pitt replies that in that case, Quigley will be shot "while bearing arms against the forces of Her Majesty, the Queen; while trying to escape, of course." As the troopers raise their rifles, a strong wind sweeps over the plain and suddenly the surrounding hills are lined with Aborigines, including Marstons servants. Though they take no direct hostile action, the troopers seem convinced that the Aborigines will attack if Quigley is killed, so they leave. After the troopers are gone, Quigley bandages his wounded leg and looks up to the hills to see that the Aborigines have vanished.

The next scene shows Quigley seeking to buy passage back to America. The ticket clerk has a wanted poster beneath his desk identifying Quigley. He holds a pistol beneath the desk and asks for the passengers name. Before Quigley can answer, Cora comes into the ticket office and stands just inside the door. They exchange a long glance, and Quigley tells the clerk that he is "Roy Cobb" and asks for two tickets. The clerk then puts the pistol down.

As Quigley and Cora walk along the wharf to the ship, she reminds him that he once told her she had to say two words before he would make love to her. Quigley stops, looking confused, and asks her what the words were. As she walks past, she says, smiling broadly, "Matthew Quigley." Quigley turns Cora around, she removes his hat and runs her fingers through his hair, and they embrace.

==Cast==
* Tom Selleck as Matthew Quigley
* Laura San Giacomo as Crazy Cora
* Alan Rickman as Elliott Marston
* Chris Haywood as Major Ashley-Pitt
* Ron Haddrick as Grimmelman
* Tony Bonner as Dobkin
* Jerome Ehlers as Coogan
* Conor McDermottroe as Hobb
* Roger Ward as Brophy
* Ben Mendelsohn as OFlynn
* Steve Dodd as Kunkurra
* Karen Davitt as Slattern
* Kylie Foster as Slattern
* William Zappa as Reilly
* Jonathan Sweet as Sergeant Thomas
* Ollie Hall as Carver

==Production== John Hill first began writing Quigley Down Under in 1978, and both Steve McQueen and Clint Eastwood were considered for the lead, but by the time production began in 1980, McQueen was too ill and the project was scrapped.  In the mid-1980s Tom Selleck heard of it and UAA got involved; the film was almost set up at Warner Bros with Lewis Gilbert as director but it fell over during pre-production. Simon Wincer then became director, who felt a good story had been ruined by numerous rewrites from people who knew little about Australian history, so he brought on Ian Jones as writer. They went back to the original draft, re-set it from the 1880s to the 1860s and made it more historically accurate. Scott Murray, "Simon Wincer: Trusting His Instincts", Cinema Papers, November 1989 pp. 6–12, 78 

The film was made by the newly formed Pathe Group, then under Alan Ladd Jnr. It was Ladds enthusiasm for the project which helped get it financed. 

The firearm used by Quigley (Selleck) is a custom 13.5 pound (6&nbsp;kg), single-shot, 1874 Sharps Rifle, with a 34-inch (860&nbsp;mm) barrel.  The rifle used for filming was a replica manufactured for the film by the Shiloh Rifle Manufacturing Company of Big Timber, Montana.  In 2002 Selleck donated the rifle, along with six other firearms from his other films, to the National Rifle Association, as part of the NRAs exhibit "Real Guns of Reel Heroes" at the National Firearms Museum in Fairfax, Virginia. 

The movie was filmed entirely in Australia. Scenes were filmed in and around Warrnambool and Apollo Bay, Victoria. 

Although several scenes of the story depict violence and cruelty toward and involving animals, a film spokesperson explained that no animal was harmed, and special effects were used. For example, Quigley and Cora are reduced to consuming "grub worms" (actually blobs of dough) for survival. A pack of dingoes attacks Cora, and she finally saves herself by shooting the animals. Those animals were specially trained, and were actually "playing" for that scene, which was later enhanced by visual and sound effects. Several scenes involve falling horses; they were performed by specially-trained animals and were not hurt. When a horse falls off a cliff, the "horse" was a mechanical creation. The films producer stated that a veterinarian was on the set whenever animals were being used in filming. 

==Reception==
Critical responses were mixed, with Quigley having a 56% rating on Rotten Tomatoes.  Roger Ebert gave the film two-and-a-half out of four stars, arguing that it was a flawed but respectable neo-western, and particularly praising San Giacomos performance: " his may be the movie that proves her staying power.   She has an authority, a depth of presence, that is attractive, and her voice is deep and musical." 

The film, however, was not a financial success in theaters, roughly recouping its budget.

The film, specifically the protagonists skill with his rifle, has led snipers to refer to the act of killing two targets with a single bullet as a Quigley. 

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- London Film Critics Circle Award British Actor of the Year Alan Rickman
| 
|- Motion Picture Sound Editors Award Best Sound Editing - Foreign Feature Tim Chau
| 
|- Frank Lipson
| 
|- Political Film Society Award Political Film Human Rights
| 
|-
|}

==Quigley Sharps rifle==

Quigley says of his gun:
 
 Shiloh Rifle Co. of Big Timber, Montana, United States. They also had a   inch length of pull to fit Sellecks tall frame, a full octagon heavy barrel with a blue finish, and weighed   pounds. Due to the weight, one of the rifles was sent back to Shiloh to be refitted with an aluminum barrel so it could be swung faster (as a club) in fight scenes.

Though Quigley calls it a lever-action rifle, this is actually an error. While it is operated with a lever, the Sharps rifle is actually a Falling Block Action, not a Lever Action.

After the filming concluded, Selleck kept all three rifles, and had two of them reconditioned by Shiloh Rifle Co.  In 2006 Selleck donated one of the rifles used in filming to the NRA for a fundraising raffle. In March 2008 that rifle was sold for $69,000 through the James D. Julia auction house.  The company which created the rifle for the movie (Shiloh Rifle Co.) also offers production models (1874 Sharps Buffalo – "Quigley") for sale to the public, with an approximate $3,300 price.  An Italian company (Davide Pedersoli & C.) sells a copy of the Shiloh rifle under the name S.789  1874 Sharps Quigley Sporting. 

An annual Matthew Quigley Buffalo Rifle Match is held in Forsyth, Montana (180 miles from Big Timber) on Fathers Day weekend. The shoot is the largest of its kind in America, attended by around 600 shooters, with targets out to 800 yards. 

Ron Haddrick plays Grimmelman, the old shop keeper who befriends Matthew Quigley and reloads cartridges for him.  Ron Haddricks real life wifes maiden name is Margaret Lorraine Quigley.

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
* 
* 
* 
*  updated 22 April 2012
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 