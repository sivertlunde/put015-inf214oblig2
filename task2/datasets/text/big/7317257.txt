Jukti Takko Aar Gappo
{{Infobox film
| name           = Jukti Takko Aar Gappo
| image          = Jukti Takko Aar Gappo DVD cover.jpg
| image_size     = 200px
| border         = 
| alt            = Jukti Takko Aar Gappo DVD cover
| caption        = Jukti Takko Aar Gappo DVD cover
| director       = Ritwik Ghatak
| producer       = Rita Productions, Ritwik Ghatak
| writer         = Ritwik Ghatak
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  See below
| music          = Ustad Bahadur Khan
| cinematography = Baby Islam
| editing        = Amalesh Sikdar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 120 mins   
| country        = India Bengali
| budget         = 
| gross          = 
}}
{{Listen
| image = none
| filename = Jukti Takko Aar Gappo scene.ogv
| title = Jukti Takko Aar Gappo scene (1 min 28 secs)
| alt = Jukti Takko Aar Gappo scene
| description =Nilkantha Bagachi (character played by Ritwik Ghatak) is talking to Shatrujit Basu (character played by Utpal Dutt). In this scene Bagchi is narrating that his life is wasted (like many others lives).
}} Bengali film National Film Awards Rajat Kamal Award for Best Story in 1974. 

The film is considered technically superior to other films of that era due to its camera work. As Ghataks last film, it is placed in the league of Jean Cocteaus Testament of Orpheus and Nicholas Ray and Wim Wenderss noted documentary film,  Lightning Over Water. 

==Synopsis==
In this film Ghatak plays role of Nilkantha Bagchi, an alcoholic, disillusioned  broken intellectual,    in the characters own words "a humbug". His  wife leaves him because of his insufferable alcoholism. After losing his wife and being forced from his home, he wanders through the countryside and meets unusual folks along the way. Nilkantha meets Bongobala, who was driven away from Bangladesh and does not have any shelter in Kolkata; he gives her shelter. He meets Jagannath Bhattacharjee, a village school teacher of Sanskrit. Jagannaths school was closed after political killings and he came to Kolkata in search of a job. Nilkantha meets Naxalites whom he describes as the "frame of Bengal", but misguided, successful and unsuccessful at the same time. The film, adventurous and revolutionary, is an exceptional mix of images that challenge the limits of narrative storytelling.

==Themes==
The film deals with various ideas and themes. Set against the backdrop of the first Naxalite wave of rebellion in India,    the film is considered to be Ghataks autobiographical film,    an anti-climax.    Ghatak himself explained, "In it   the political backdrop of West Bengal from 1971 to 1972 as I saw it has been portrayed. There is no ideology. I saw it from a point of view of not a politician. I am not supposed to please a political ideology". Ghatak was aware of a complete breakdown of moral values around him, especially among the younger generation. He tried to portray these issues in this film (and also in his unfinished film Sei Vishnupriya).    Ghatak, both in real life and in this film, tried to find some meaning for the political and cultural turmoil overtaking his country.   

;The Great Mother Image
In an interview Ghatak mentioned that "The Great Mother Image" in its duality exists in every aspects of our being, and he incorporated this image into films like Meghe Dhaka Tara and Jukti Takko Aar Gappo.   

;Allegorical characters
The characters in this film have been portrayed allegorically. 
* Ghatak played the character of Nilkantha Bagchi, his alter ego. Nilkantha is the name of Hindu god Shiva, who drank poison that emerged from the oceans to prevent it from destroying everything in the world.
* Tripti Mitra played the role of Nilkantha Bagchis wife Durga (influenced by Hindu goddess Durga|Durga; also according to Hindu mythology, Durga is a consort of Shiva). In an interview Ghatak described his wife Surama Ghatak as a Sati (goddess)|Sati (a very pious woman, and consort of Shiva).
* Ritaban Ghatak (Ghataks real life son) played the character of Nikantha Bagchis son Satya. In Sanskrit and Bengali the word Satya means "true" or "real".
* The character Nachiketa is inspired by the Hindu mythological character Nachiketa. Vedic civilization.
* Panchanan Ustad is representative of rural people, not very educated, but very rich with their own (folk) culture and heritage.

==Credits==

===Cast===
*  
* Durga: Tripti Mitra
* Satya: Ritaban Ghatak
* Bongobala: Shaonli Mitra
* Nachiketa: Saugata Burman
* Jagannath Bhattacharjee: Bijon Bhattacharya
* Shatrujit: Utpal Dutt
* Panchanan Ustad: Gyanesh Mukherjee
* Leader of Naxalites:  Ananya Ray
* Police inspector: Shyamal Ghoshal
* Alcoholic person in village: Jahor Roy
;Others
*Satindra Bhattacharya
*Tarak Chattopadhyay
*Nani Chattpadhyay
*Subrata Sensharma
*Tapan Chattopadhyay
*Munir Choudhury.

===Technical team===
* Direction: Ritwik Ghatak
* Assistant directors: Deb Dutta, Ananya Roy, Nikhil Bhattacharya, Dilip Mukhopadhyay, Tapan Saha      
* Story, screenplay, music and executive producer: Ritwik Ghatak
* Cinematography: Baby Islam
* Assistant cinematographer: Shankar Chattopadhyay, Dipak Das
* Editorial team
** Editor: Amalesh Sikdar
** Assistant: Kali Prasad Roy
** Supervisor: Ramesh Joshi
* Art direction: Rabi Chattopdahyay
* Assistant: Surath Das, Suresh Chandra Chanda, Somnath Chakraborty
* Dance choreography: Shambhu Bhattacharya
* Publicity layouts: Khaled Choudhury
* Music: Ustad Bahadur Khan
* Playback singers: Debabrata Biswas, Ranen Ray Choudhury, Arati Mukhopadhyay, Binpani Roy Choudhury
* Sound recording: Shyam Sundar Ghosh

==Soundtracks==
# Keno cheye achho go Ma (singer Sushil Mallick)
# Amar onge onge ke bajaye banshi (Rabindra Sangeet)
# Namaz aamar hoilo na adaay
# Chhau dance
# Janti gachhe janti phal je

== Remarks ==

* "What has been thematised in the whole movie is the decadence. In multiple ways, so many lives are being wasted. May be the whole country is being wasted." –Amalendu Basu
* "When Mrinal Sen opened up a new dimension in the Bengali cinema by making political films, Ritwik showed how he could give a new shape even to this genre...to make a film simultaneously political and autobiographical...the more important thing is that here autobiography is no mere hankering for recollection of the past, but a self-analysis, self-criticism...this film enables us to confront ones own self...but Ritwiks arguments, Ritwiks story all seem to be the prattling of an alcoholic, but the person who has the guts to strip himself before the truth, is not Ritwik, the alcoholic, but Ritwik, a true revolutionary." –Dipendu Chakrabarty
* "Jukti Takko Aar Gappo, a film so daring in its complete disregard of the very language and grammar of cinema he had mastered and developed that it is difficult to understand how it achieves its intense intimacy with the audience. It is as if the characters step out of the screen to talk and to you and you are forced to respond to them, to react very sharply for or against them. The central character played by Ghatak himself parodies his real life in such a way that it compels the audience to reflect and criticise him. Perhaps this is just what Ritwik had been struggling to do through cinema all his life. Ironically, perhaps, he wanted to see that it could be achieved only through a conscious rejection of much of what has come to be accepted as the language of cinema." –Safdar Hashmi
* "In Jukti Takko Aar Gappo, the elements were presented in their new raw form — reason, argument, story, song that hunger which is the basis of human creativity. So that Riwik calls himself a humbug a civilisation sees how it is reduced to ashes. Yet there was no nihilism; he dies pleading with those who would annihilate their compassion along with their enemy. Our heroism will find itself trapped in mechanical crossfire of gunpowder, if it refuses to nourish itself on nature and history. For Ritwik, the heroic act the ultimate and the first sacrifice, has to be the act of union." –Kumar Sahani

==References==

===Citations===
 

===Sources===
*  
 
*  

==External links==
* 

 

 
 
 
 
 
 