Mountain Strawberries 4
{{Infobox film
| name           = Mountain Strawberries 4
| image          = Mountain Strawberries 4.jpg
| caption        = Theatrical poster for Mountain Strawberries 4 (1991)
| film name = {{Film name
 | hangul         =   4
 | hanja          =   4
 | rr             = Aemabuin 4
 | mr             = Aemapuin 4}}
| director       = Kim Su-hyeong 
| producer       = Kim Su-hyeong
| writer         = Yoo Ji-hoeng
| starring       = Gang Hye-ji
| music          = Lee Jong-sik
| cinematography = Chun Jo-myuong
| editing        = Ree Kyoung-ja
| distributor    = Tae Kwang Films Co., Ltd.
| released       =  
| runtime        = 95 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} 
Mountain Strawberries 4, (hangul|산딸기 4 - Sanddalgi 4) also known as Wild Strawberries 4, is a 1991 South Korean film directed by Kim Su-hyeong. It was the fourth entry in the Mountain Strawberries series.

==Synopsis==
In 1950 an unsatisfied widow makes her living selling medicine from village to village while  searching for an energetic man to wed. She meets a strong but unintelligent man who works on a boat and wrestles and plays the drum in his spare time. After living with him happily for a short time and bearing him a daughter, the widows unsatisfied spirit leads her astray. Years later she secretly watches her daughters wedding, and leaves, beating a drum.   

==Cast==
* Gang Hye-ji 
* Yu Jang-hyeon
* Cho Ju-mi
* Kim Kuk-hyeon
* Kook Jong-hwan
* Chung Kyoo-young
* Ju Sang-ho
* Han Tae-il
* Yoo Myeong-sun
* Pak Yae-sook

==Bibliography==

===English===
*  

===Korean===
*  
*  

==Notes==
 

 
 
 
 


 
 