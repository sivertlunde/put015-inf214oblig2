Lord Byron of Broadway
{{Infobox film
| name           = Lord Byron of Broadway
| director       = Harry Beaumont William Nigh
| image	=	Lord Byron of Broadway FilmPoster.jpeg
| producer       = 
| writer         = Nell Martin (novel) Crane Wilbur Clifton Edwards Henry Sharp
| music          = Songs:: Nacio Herb Brown (music) Arthur Freed (lyrics)
| editing        = Anne Bauchens
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 66 or 77 minutes (sources differ)
| country        = United States
| language       = English
}}

Lord Byron of Broadway (1930 in film|1930), also known as What Price Melody?, is an American musical drama film, directed by Harry Beaumont and William Nigh. It was based on a best selling book by Nell Martin, which "was widely praised by critics as an extremely true and amusing romance of stage life."  It was filmed in black and white with two-color Technicolor sequences.

==Cast==
*Charles Kaley as Roy Erskine
*Ethelind Terry as Ardis Trevelyn
*Marion Shilling as Nancy Clover
*Cliff Edwards as Joe Lundeen
*Gwen Lee as Bessie ("Bess")  
*Benny Rubin as Phil
*Jack Byron as Mr. Millaire (as John Byron)
*Gino Corrado as Riccardi
*Iris Adrian (uncredited)
*Jack Benny (uncredited)
*Ann Dvorak (uncredited)

==Soundtrack==
Charles Kaley recorded two of the songs for Brunswick Records (Record Number 4718). These songs were "Should I" and "A Bundle of Love Letters". Both of these songs proved to be major song hits in late 1929 and early 1930 and were recorded by numerous artists. For example, James Melton and Lewis James recorded vocal versions of "A Bundle of Love Letters" while Frank Munn recorded "Should I". 

* "A Bundle of Love Letters"
::(1930)
::Music by Nacio Herb Brown
::Lyrics Arthur Freed
::Played on piano by Marion Shilling and sung by Charles Kaley
::Played on piano by Marion Shilling and sung by Cliff Edwards and Charles Kaley in a vaudeville show
* "The Japanese Sandman"
::(1920)
::Music Richard A. Whiting
::Lyrics by Ray Egan
::Sung by Cliff Edwards in his vaudeville show
* "The Doll Dance"
::Music by Nacio Herb Brown
::Lyrics Arthur Freed
::Danced to by Rita Flynn and Hazel Craven in a vaudeville show
* "Blue Daughter of Heaven"
::(1930)
::Music by Dimitri Tiomkin
::Lyrics by Ray Egan
::Sung offscreen by James Burroughs and danced to by Albertina Rasch Ballet\
* "Should I?"
::(1930)
::Music by Nacio Herb Brown
::Lyrics Arthur Freed
::Sung by Charles Kaley at a nightclub
::Reprised by Ethelind Terry at a recording studio
::Played at the end
* "The Woman in the Shoe"
::(1930)
::Music by Nacio Herb Brown
::Lyrics Arthur Freed
::Sung by Ethelind Terry, an offscreen male singer and the chorus in a show
::Danced to by the chorus
::Reprised by male trio
* "Old Pal, Why Did You Leave Me?"
::(1930)
::Music by Nacio Herb Brown
::Lyrics Arthur Freed
::Played on piano and sung by Charles Kaley and Benny Rubin
* "Only Love Is Real"
::(1930)
::Music by Nacio Herb Brown
::Lyrics Arthur Freed
::Played on piano by Marion Shilling on radio and sung by an unidentifed male singer
::Reprised by Ethelind Terry on radio
* "Youre the Bride and Im the Groom"
::(1930)
::Music by Nacio Herb Brown
::Lyrics Arthur Freed
::Played on piano and sung by Charles Kaley
* "Love Aint Nothin But the Blues"
::(1930)
::(From "Chasing Rainbows (1930)")
::Music by Louis Alter
::Lyrics by Joe Goodwin
::Played instrumentally

==Production== Earl Carrolls Rio Rita. At that time, Kaley and Terry were well-known stage stars. 

MGM used the "Woman in the Shoe" musical segment in two short films, Nertsery Rhymes (1933) and Roast Beef and Movies (1934).

==Critical response==
The expensive film received mixed reviews, mainly due to the lackluster direction of William Nigh and Harry Beaumont. Its Technicolor sequences and musical score, however, were universally praised.  "The storys strong enough to be festooned with Technicolored girls, ballets, songs and effects without breaking down," said Photoplay "Youll like this." 

Sheet music sales and sales of phonograph records with songs from the film were brisk. "Should I" and "A Bundle of Old Love Letters" proved to be among the most popular song hits of the year 1930.

==See also==
*List of early color feature films

==References==
;Notes
 

==External links==
*  
*   at Allmovie
*   at TCM
*   review by The New York Times
*   at Answers.com

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 