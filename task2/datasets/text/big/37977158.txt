Jazz All Around
 
{{Infobox film
| name           = Jazz All Around
| image          = 
| caption        = 
| director       = Knud Leif Thomsen
| producer       = Lars Kolvig
| writer         = Knud Sønderby Knud Leif Thomsen
| starring       = Finn Storgaard
| music          = 
| cinematography = Henning Kristiansen
| editing        = 
| studio         = Saga Studios
| released       =  
| runtime        = 97 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

Jazz All Around ( ) is a 1969 Danish drama film directed by Knud Leif Thomsen. It was entered into the 6th Moscow International Film Festival.   

==Cast==
* Finn Storgaard as Peter Hasvig
* Lotte Væver as Ellen
* Anne-Lise Gabold as Vera Bagger
* Torben Jetsmark as Hugo
* Elsebeth Reingaard as Ida Schmidt
* Gitte Reingaard as Esther Schmidt
* Sisse Reingaard as Stuepige
* Susanne Heinrich as Eva Bagger
* Steen Frøhne as Johannes
* Søren Rode as Hjalmer
* Søren Strømberg as Kontorelev

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 