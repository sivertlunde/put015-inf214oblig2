Partysaurus Rex
{{Infobox film
| name           = Partysaurus Rex
| image          = Partysaurus Rex poster.jpg
| border         =
| alt            = 
| caption        = 
| director       = Mark Walsh
| producer       = Kim Adams   
| writer         = Mark Walsh 
| story          = Mark Walsh John Lasseter 
| starring       = Wallace Shawn Corey Burton Tim Allen Tom Hanks Brian Transeau 
| editing        = Axel Geddes 
| studio         = Pixar Animation Studios Walt Disney Pictures Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 7 minutes 
| country        = United States
| language       = English
}} computer animated Toy Story Small Fry and Hawaiian Vacation. The short involves Rex getting left in the bathroom and making friends with bath toys. 

==Plot==
Rex disrupts the other toys blowing soap bubbles, worried they may ruin Bonnies house. He is chastised by the other toys, with Mr. Potato Head calling him "Partypooper Rex". The other toys sense Bonnie approaching, and scatter, leaving Rex alone when she enters. She takes him to play with while she takes a bath. Rex enjoys being played with along with the other bath toys, but soon Bonnies mom comes to turn off the bath and take her out. Once alone, the bath toys wish they could continue to have fun, but all of them lack arms and can only function if afloat in the water. Rex initially thinks drawing a new bath is a bad idea, but recalling Mr. Potato Heads insult, he asserts that he is "Partysaurus Rex", and helps the bath toys to start a new bath.

The toys quickly start to sing and dance in a rave-like fashion, aided by a carefree Rex throwing a bubble bath solution into the tub, and blocking the overflow drain with a sponge. However, he suddenly realizes that if the tub overflows, it will leak out into the hall; the bath toys care little if this happens and continue to party. He tries to turn off the water, but only causes the handle to fall off, and the drain stays plugged when he tries to pull it out. Finally, he spots the knob on the faucet that stops the flow from the faucet, but realizes too late that this only diverts the water to the shower head. The tub starts to overflow, and the other bath toys are oblivious to the problem. Outside the bathroom, Woody, Buzz, and some of the other toys arrive to check on Rex when the bathroom door suddenly bursts open and a flood of water pours out.

Later, Bonnies mom is paying to have plumbing repairs done to the house. Though he knows it was his fault, Rex enjoys the short-lived fame with the other toys in Bonnies room as Mr. Potato Head is still getting the water out of him. Outside, several pool toys have heard of Rexs exploits from the bath toys and get him to agree to help turn on the outside faucet to let them party as well. Rex quickly joins in on their fun.

==Voice cast==
  Rex  Woody
* Tim Allen as Buzz Lightyear
* Corey Burton as Captain Suds Tony Cox as Chuck E. Duck Don Fullilove as Chuck E. Duck  Emily Hahn Bonnie
* Don Rickles as List of Toy Story characters#Mr. Potato Head|Mr. Potato Head
* Lori Alan as Bonnies Mom
* Estelle Harris as List of Toy Story characters#Mrs. Potato Head|Mrs. Potato Head Hamm
* Mark Walsh as Drips the Whale Faucet Cover
* Timothy Dalton as List of Toy Story characters#Mr. Pricklepants|Mr. Pricklepants  Jessie 
* Sherry Lynn as Cuddles the Alligator
* Lori Richardson as Babs the Octopus

==Music== Brian Transeau composed the music for the short.    He said in an interview: "Im in the middle of scoring a film for Pixar right now. Its a short for Toy Story, and Im not allowed to say the whole story, but quite literally, its like a Toy Story rave, and Im actually not kidding either. Like, the toys get into all these shenanigans and its like pounding club music, this thing. So its really not very Pixar, but in like a really hysterical way, everyone laughs so hard when they see it. Its really exciting to work with those guys." 

The song from the short, titled "Partysaurus Overflow", was released as a digital download on November 19, 2012, on iTunes and Amazon.com|Amazon. Beside BT, the cover art also credits electronic dance producer Au5. 

==Release== Amazon  and YouTube.  The short made its home video debut as a special feature on the 3D Blu-ray release of Monsters, Inc., which was released on February 19, 2013.   

==Reception==
The short was well received. Ben Kendrick of The Christian Science Monitor said that Partysaurus Rex is "easily the most enjoyable franchise spin-off to date." 

==References==
 

==External links==
*  
*  
*  

 
{{succession box La Luna Pixar Animation short films
 | years = 2012
 | after = The Legend of Mordu}}
 

 
 
 

 
 
 
 
 
 