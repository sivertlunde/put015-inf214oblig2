Kan du vissla Johanna? (film)
{{Infobox film
| name           = Kan du vissla Johanna?
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Rumle Hammerich
| producer       = Agneta Jansson
| writer         = Ulf Stark
| narrator       = 
| starring       = Tobias Swärd Jimmy Sandin Per Oscarsson
| music          = Jacob Groth
| cinematography = Andra Lasmanis
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 55 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} Swedish TV 1992 book with the same name. Since 1994 it has been broadcast at Christmas. 

The title comes from a 1932 song with the same name by Åke Söderblom and Sten Axelson.

==Plot==
The film takes place during the 1950s. Berra, a 7-year-old boy, wishes for a grandfather who he can love, who can invite him for coffee and who can teach him how to whistle. His friend Ulf tells him that he can look at the retirement home, where he finds an old man called Nils who becomes Berras stepgrandfather.

==Cast==
*Tobias Swärd as Berra (Bertil)
*Jimmy Sandin as Ulf
*Per Oscarsson as Nils
*Helena Kallenbäck as Tora, woman working at the retirement home
*Thomas Roos as Mr Gustavsson
*Gunilla Abrahamsson as teacher
*Gustav Levin as Priest
*Mats Bergman as Tobacco merchant

==References==
 

==External links==
* 
*  
*  

 
 
 
 
 

 