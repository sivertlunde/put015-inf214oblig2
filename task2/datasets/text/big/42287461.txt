Amour Fou (film)
 
{{Infobox film
| name = Amour Fou
| image = Amour Fou (film) POSTER.jpg
| alt = 
| caption = 
| director = Jessica Hausner
| producer = Martin Gschlacht Antonin Svoboda Bruno Wagner Bady Minck Alexander Dumreicher-Ivanceanu
| writer = Jessica Hausner
| starring = Birte Schnöink  Christian Friedel Stephan Grossmann
| cinematography = Martin Gschlacht
| editing = Karina Ressler
| studio = Coop 99   Amour Fou Luxembourg   Essential Filmproduktion 
| distributor = Coproduction Office
| released =  
| runtime = 96 minutes
| country = Austria Germany Luxembourg
| language = German
| budget = 
| gross = 
}}
Amour Fou ("mad love") is a 2014 Austrian film directed by Jessica Hausner, starring Christian Friedel and Birte Schnöink. The story is set in Berlin in 1810 and 1811, and follows the German writer Heinrich von Kleist and his lover Henriette Vogel in the final stages of their lives. It was screened in the Un Certain Regard section at the 2014 Cannes Film Festival.   

==Cast==
* Christian Friedel as Heinrich von Kleist
* Birte Schnöink as Henriette Vogel
* Stephan Grossmann as Vogel
* Sandra Hüller as Marie
* Sebastian Hülk as Pfuehl
* Peter Jordan as Adam Müller

==Production==
The film was produced by Coop99 Filmproduktion in collaboration with Germanys Essential Filmproduktion and Luxembourgs Amour Fou.  A great deal of research was put into the costumes and production design in order to avoid clichés seen in many period films. Hausner wanted a clear aesthetic with a lot of yellow and red colours. One source of inspiration for the visual style was the paintings of Johannes Vermeer.  Filming took place in Luxembourg, Germany and Austria from February to October 2013. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 