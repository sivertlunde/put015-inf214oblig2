Flatland: The Movie
 
{{Infobox Film
| name = Flatland: The Movie
| image = FlatlandTheMovie.jpg
| image_size = 
| caption = 
| director = Dano Johnson  Jeffrey Travis
| producer = Seth Caplan Will Wallace
| screenplay = Seth Caplan   Dano Johnson   Jeffrey Travis
| based on =  
| narrator =  Michael York 
| music = Kaz Boyle 
| cinematography = 
| editing = 
| distributor = Flat World Productions
| released = 2007 - DVD 2011 - IMAX 3D
| runtime = 34 minutes
| country = USA English
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}

Flatland: The Movie is a short animated film which was released to video in 2007. The cast includes the voices of actors   written by Edwin A. Abbott.

== Plot ==
Flatland: The Movie begins with the 2-dimensional Arthur Square awaking from a dream of strange, glowing symbols. He lives with his wife, Arlene Square, and his curious granddaughter Hex, a hexagon. He rushes Hex to school and along the way they discuss the laws of inheritance: how each new generation of Flatlanders, beginning with triangles, gains a new side until the shapes become indistinguishable from circles. They also discuss how a citizens shape affects their job, with triangles performing menial labor and circles ruling Flatland in the priest class. They witness a cruel incident where a Circle Priest arrests a slightly irregular octagon child, prompting Hex to yet again wonder about what happened to her pentagonal parents. Arthur tells her that he will tell her someday, and rushes her off to school.

Arthur arrives at his Ministry of Regularity job only to be harassed by his circular superior, Miss Helios. He is about to confide his dream of the symbols to his brother Abbott Square when the employees are called to a meeting. The head circle ruler of Flatland, Pantocyclus, ushers a new edict that bans discussion of heretical topics such as a third dimension or the ruins at Area 33H. When Arthur sees an image of the ruins, he recognizes them from his dream.

At dinner, Hex asks Arthur about the Circles proclamation and to define dimension. Arthur gives her a geometry lesson showing how powers in arithmetic can be translated to geometrical dimensions. When Hex speculates on a third dimension, Arthur becomes infuriated and sends her to her room. She is only calmed when Arlene talks to her and gives her a gift of some of her mothers belongings – a box containing books and a model of the symbols at Area 33H.

That night, Arthur Square is plucked from his bedroom and taken to a strange, alien landscape. He encounters the insane, babbling King of Pointland, a being of zero dimensions. Then he encounters Lineland, a universe of one dimension that is populated by an arrogant line segment King who cannot imagine a new dimension that he cannot see. Finally, Arthur is whisked back to his living room. Suddenly he hears a voice booming out and sees a point grow to a circle and then back to a point. The being identifies himself as Spherius, a three-dimensional solid from Spaceland. After failed attempts to explain the third dimension, Arthur is popped into Spaceland and taken on a journey by Spherius. They stop at Area 33H and Arthur realizes that the symbols show a progression from point to line to square to cube, and that the constantly changing shape in the center is actually a cube, halfway though the plane of Flatland and spinning along all three axes. Hex was right! Arthur excitedly asks Spherius to show him the fourth dimension and the fifth dimension. Spherius laughs, saying there couldnt possibly be a higher dimension. He tells Arthur to spread the word of the third dimension and drops Arthur back into bed. Meanwhile, at the Ministry, Pantocyclus is ordering the triangle guards and circles, including Miss Helios, to beware any employees who mention the third dimension and to guard Area 33H.

When Arthur awakes, he rushes to Hexs room to tell her the news. He finds her missing and Arlene tells him that she left very early. Meanwhile, Hex is entering Area 33H and discovering the symbols for herself. However, a triangle guard spots Hex. At the Ministry, Arthur arrives and begins talking to his brother when an alarm goes off. Over the intercom Miss Helios orders all guards to catch an intruder at Area 33H. Arthur realizes it might be Hex at the ruins and persuades Abbott to help him steal a Ministry car.

Abbott and Arthur arrive at the ruins moments before the triangle guards. With Abbott distracting the guards, Arthur goes to the symbols and finds Hex terrified. She apologizes but Arthur finally admits to her that he had been protecting her from the knowledge that her parents were arrested and killed for their theories on the third dimension. With the guards closing in, Arthur pushes Hex into the cube symbol, which pops her up into the third dimension. Arthur is arrested by Miss Helios.

Arthur is hastily brought into a courtroom with Pantocyclus proceeding over his trial for heresy. Arlene arrives, asking where Hex is. In his cell, Arthur assures her that Hex escaped upward into the third dimension and is safe. A nearby pentagon begins broadcasting the trial. Pantocyclus challenges Arthur to show everyone the third dimension, or as he calls it “upwards, not northwards.” Arthur admits that he cant and pleads with his fellow Flatlanders that reason dictates a third dimension. He challenges them to aspire to be greater than their shapes, angering the Circles immensely. The crowd is growing unruly as Arthurs words take hold. In a fit of rage, Pantocyclus sentences Arthur to death and the crowd gasps. Just as Arthur loses hope, Spherius interrupts the proceedings and pops Arthur out of Flatland.

Arthur thanks Spherius but feels he has failed to persuade people of the third dimension. Spherius surprises Arthur with the solution: he intended Hex, not Arthur to be the prophet of the Third Dimension. Hex embraces Arthur and they fly back down to the courtroom, surprising everyone as they materialize out of nowhere. The Circles are speechless and lose control of the courtroom, evading the reporters questions about the third dimension. Arthur, Arlene, Hex, and Abbott are reunited. The reporter asks Hex if she has also visited the fourth dimension. Spherius angrily comes down to Flatland to scoff at the idea and flies off. But off on the horizon, a light is shining. The symbols at Area 33H are glowing and as the camera dips below Flatland we see that the spinning cube is actually part of a larger installation: eight cubes spinning and orbiting a 4D cube – a tesseract – which, in turn, is rotating around its fourth axis.

== Cast ==

*Kristen Bell ... Hex
*Lee Eddy ... Helios
*Joe Estevez ... Abbott Square
*Tony Hale ... King of Pointland
*Curtis Luciani ... King of Lineland
*Shannon McCormick ... Triangle Guard
*T. Lynn Mikeska ... Heptagon Mother Robert Murphy ... Pentagon Reporter Garry Peters ... Pantocyclus
*Martin Sheen ... Arthur Square
*Danu Uribe ... Arlene Square Michael York ... Spherius

== Business ==

Producer Seth Caplan revealed that Flatland was more profitable than other projects hed worked on, despite the fact that it was self-distributed online through digital sales and DVD. 

== Awards ==
* Winner of Best Animated Film, EcoVision Film Festival, Italy 2009
* Winner of Berlin Special Jury Award, MathFilm Festival 2008
* Runner Up for Best Independent Short Ages 5–12, Kids First! Film Festival 2008

==See also==
*Flatland (2007 film) another film based on the book, released in the same year by Ladd Ehlinger Jr.

==References==
 

==External links==
* 
*  
* 
* 
* 
* 
* 
* 
* 
* , a review of both Flatland: The Movie and Flatland (2007 film)|Flatland the Film by Ryan Somma
*"Two reviews in one", a careful, balanced review of both Flatland: The Movie and  ’s  
* 
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 