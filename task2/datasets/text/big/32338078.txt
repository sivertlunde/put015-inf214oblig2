U R My Jaan
{{Infobox film
| name = U R My Jaan - Movi
| image =U_R_My_Jaan.jpg
| alt =
| caption = Theatrical release poster
| director = Aron Govil
| producer = Aron Govil
| screenplay =Aron Govil
| starring = Mikaal Zulfiqar Preeti Soni Himani Shivpuri Anil Dhawan Rajesh Khera Aman verma Ravi Dubey
| music = Sanjeev Darshan
| cinematography = Shirsha Ray
| editing = Sanjay Ingle
| studio =
| released =  
| country = India
| language = Hindi
}}
U R My Jaan ( ) is a Hindi romantic film and a remake of 1990s movie Pretty Woman (starring Richard Gere, Julia Roberts and Jason Alexander), released worldwide on 23 September 2011.   The film was written and directed by the debuting Aron Govil,

== Synopsis ==
U R My Jaan is a love story about Reena (Preeti) and Akash (Mikaal) whore opposites. Reena is a vivacious middle-class girl who have dream to be an actor  from Chandigarh, who nurtures the dream of making it big in Bollywood. Akash, from New York, is a renowned, suave & ruthless billionaire, who will go to any lengths when its his business. They are brought  together by fate in Mumbai, and in their brief encounter, they manage to change  each others lives forever. 

== Cast ==
* Mikaal Zulfiqar as Akash
* Priti soni as reenaa
* Himani Shivpuri as Reenas Mom
* Anil Dhawan as Reenas Father
* Rajesh Khera as Sandy
* Aman Verma as Rahul
* Abhilasha Das	as Priya
* Aishwarya Sakhuja as Nisha
* Nathassha Sikka as Donna
* Vipul Roy as Rajiv
* Partha Ray as Parag
* Narendra Jaitley as Ravi
* Sunil Nagar as Rajivs Dad
* Neha Gupta as Rajivs Mom

Below is the first weekend territorial breakdown of the film U R My Jaan
 
Mumbai Circuit - 1.14 crore
 Delhi/UP - 57  lakhs
 West Bengal - 16 lakhs
 Bihar - 2 lakhs
 CP Berar - 17 lakhs
 CI - 18 lakhs
 Rajasthan - 21 lakhs
 Others - 6 lakhs
TOTAL:   2.51 crore

==Soundtrack==
The films soundtrack is composed by Sanjeev Darshan with lyrics by Sameer.
{{Infobox album 
| Name = U R My Jaan
| Type = soundtrack
| Artist = Sanjeev Darshan
| Cover =
| Released =
| Recorded =
| Producer = Aron Govil
| Genre = Film soundtrack
| Length =
}}

===Track listing===
{{Track listing
| extra_column = Performer(s)
| title1 = U R My Jaan | extra1 = Sanjeev Rathod, Shilpa Rao | length1 = 5:16
| title2 = Mera Maula Kare | extra2 = Roop Kumar Rathod | length2 = 5:22 Shaan | length3 = 4:20 Richa Sharma | length4 = 5:44
| title5 = Mein Zamin pe Hun | extra5 = Shreya Ghoshal | length5 = 4:59
| title6 = Chand Wahi Hai | extra6 = Javed Ali, Shreya Ghoshal | length6 = 4:43
}}

== References ==
 
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 
 