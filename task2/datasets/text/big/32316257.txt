Beeswax (film)
{{Infobox film
| name           = Beeswax
| image          = Beeswax_poster.jpg
| alt            =  
| caption        = Promotional film poster
| director       = Andrew Bujalski Dia Sokol Ethan Vogt
| writer         = Andrew Bujalski
| starring       = Tilly Hatcher Maggie Hatcher Alex Karpovsky
| music          = 
| cinematography = Matthias Grunsky
| editing        = Andrew Bujalski
| studio         = 
| distributor    = The Cinema Guild
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $46,590
}}
Beeswax is a 2009 American mumblecore film written and directed by Andrew Bujalski. The film examines a few days in the life of twins, played by real-life sisters Tilly and Maggie Hatcher.

It premiered at the Berlin International Film Festival  and had a limited theatrical release in the United States on August 7, 2009. 

==Cast==
* Tilly Hatcher as Jeannie
* Maggie Hatcher as Lauren
* Anne Dodge as Amanda
* Alex Karpovsky as Merrill
* Katy OConnor as Corinne
* David Zellner as Scott

==Critical response==

The film opened to mixed reviews. Review aggregate Rotten Tomatoes reports that 73% of 45 critics gave the film a positive review, with an average score of 6.7/10. The website reported the critical consensus warned that "Andrew Bujalskis third effort will test the patience of some filmgoers", but lauded the film as "a warm, funny, and honest introduction to the mumblecore movement".  Review aggregator Metacritic assigned the film a weighted average score of 70 (out of 100) based on 14 reviews from mainstream critics, considered to be "generally favorable reviews." 

==Home media==
Beeswax was released on DVD by The Cinema Guild on April 6, 2010. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 