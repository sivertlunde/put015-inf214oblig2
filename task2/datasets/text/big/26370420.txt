Rasmus på luffen
{{Infobox film
| name           = Rasmus på luffen
| image          = Rasmus på luffen.jpg
| image size     = 
| alt            = 
| caption        = Swedish DVD-cover
| director       = Olle Hellbom
| producer       = Olle Hellbom
| writer         = Astrid Lindgren
| narrator       =  Erik Lindgren Jarl Kulle Håkan Serner
| music          = Gösta Linderholm Björn Isfält
| cinematography = 
| editing        = 
| studio         = 
| distributor    = AB Svensk Filmindustri
| released       = 12 December 1981
| runtime        = 105 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} Rasmus på luffen by Lindgren was published in 1956. The story is set in the year 1910 and is about the child Rasmus who runs away from the orphanage Västerhaga and meets the tramp Oskar. It was re-released on DVD in 2003.   

This was the final film of director Hellbom, who died the next year.

==Selected cast==
*Allan Edwall as Oskar Erik Lindgren as Rasmus
*Jarl Kulle as Hilding Lif
*Håkan Serner as Liander
*Olof Bergström as Länsman (policeman)
*Rolf Larsson as policeman Bergqvist
*Roland Hedlund av policeman Andersson
*Lena Brogren as Martina, Oskars wife Tommy Johnson as Nilsson
*Lottie Ejebrant as Nilssons wife
*Lars Amble as merchant
*Lena Nyman as merchants wife
*Georg Adelly as Lusknäpparn, tramp
*Göran Graffman as Rosasco, tramp
*Gösta Linderholm as Sju Attan, a tramp
*Ulla-Britt Norrman-Olsson as the first woman visited by Oskar and Rasmus
*Emy Storm as the principal of Västerhaga
*Svea Holst as Lille-Sara
*Bertil Norström as mayor
*Pål Steen as Gunnar, child at Västerhaga
*Fredrik Ljungdahl as child at Västerhaga
*Stefan Delvin as child at Västerhaga
*Jonas Karlsson as child at Västerhaga

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 