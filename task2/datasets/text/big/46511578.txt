Josette (1938 film)
{{Infobox film
| name = Josette 
| image =
| image_size =
| caption =
| director = Allan Dwan
| producer = Gene Markey   Darryl F. Zanuck
| writer =  Paul Frank (play)   Georg Fraser (play)   László Vadnay   James Edward Grant
| narrator =
| starring = Don Ameche   Simone Simon   Robert Young   Joan Davis
| music = Walter Scharf
| cinematography = John J. Mescall     Robert L. Simpson
| studio = Twentieth Century Fox 
| distributor = Twentieth Century Fox 
| released = June 3, 1938 
| runtime = 73 minutes
| country = United States English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Josette is a 1938 American comedy film directed by Allan Dwan and starring Don Ameche, Simone Simon and Robert Young.  Two brothers fall in love with the same nightclub singer.

==Main cast==
* Don Ameche as David Brassard Jr. 
* Simone Simon as Renee LeBlanc  Robert Young as Pierre Brassard 
* Joan Davis as May Morris 
* Bert Lahr as Barney Barnaby  Paul Hurst as A. Adolphus Heyman 
* William Collier Sr. as David Brassard Sr. 
* Tala Birell as Mlle. Josette 
* Lynn Bari as Mrs. Elaine Dupree 
* William Demarest as Joe, Diner Owner 
* Ruth Gillette as Belle 
* Armand Kaliz as Thomas 
* Maurice Cass as Ed, the Furrier 
* George H. Reed as Butler 
* Paul McVey as Hotel manager 
* Fred Kelsey as Hotel detective 
* Robert Kellard as Reporter  Robert Lowery as Rufe, Boatman 
* Lon Chaney Jr. as Boatman 
* Slim Martin as Orchestra leader 
* June Gale as Cafe girl 
* Ferdinand Gottschalk as Papa Le Blanc

==References==
 

==Bibliography==
* Tucker, David C. Joan Davis: Americas Queen of Film, Radio and Television Comedy. McFarland, 2014.

==External links==
*  

 

 
 
 
 
 
 

 