The Magnificent Dope
{{Infobox film
| name           = The Magnificent Dope
| image_size     =
| image          = The Magnificent Dope FilmPoster.jpeg
| caption        =
| director       = Walter Lang
| producer       = William Perlberg
| writer         = Joseph Schrank (story) George Seaton
| narrator       =
| starring       = Henry Fonda Lynn Bari Don Ameche
| music          = Emil Newman Leigh Harline Cyril J. Mockridge David Raksin
| cinematography = J. Peverell Marley
| editing        = Barbara McLean
| studio         = Twentieth Century-Fox
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 83-84 minutes
| country        =
| language       = English
| budget         =
| gross          =
}}

That Magnificent Dope (1942 in film|1942) is a comedy film released by Twentieth Century-Fox. It is also known as Lazy Galahad, Strictly Dynamite and The Magnificent Jerk.

==Plot==

Dwight Dawson is a man with quite some oomph who has come up with a high-faluting system for teaching successful business economics. Unfortunately he and his partner Horace Hunter havent done their marketing job properly, and their own business is on its knees in the muck. Their chief of marketing, Claire Harris, who is also Dwights fiancée, comes up with an idea to announce a contest on who is the biggest loser in the country. The prize is $500 and Dwights course in successful business. The idea is that the contest will draw interest to Dwights teaching system.

The contest starts and soon a winner is chosen: Thadeus Winship Page from the small town of Upper White Eddy in Vermont. He is running a not overly successful business of renting out boats during summer time.

Tad comes down from his town to New York City to collect his prize, determined to use the money for a fire-engine to give to the small town. The happy and contented Tad is not interested in taking Dwights business course though, and has to be persuaded by Claire to do so. Tad is charmed by Claire during a night out in the city and falls in love with her. After the night out, Tad finally resigns and agrees to take the course, just to be close to Claire.

Claire gets to spend some time with Tad during the course, and she realizes that he isnt the failure they thought him to be. After a while Tad tells Claire that he is in love, but he doesnt dare tell her she is the subject of his affection. Instead he says its a girl from his hometown, a girl named "Hazel".

When Dwight hears about this, he tells Tad that the business course will help him in his quest to win Hazels heart. Tad believes Dwight and continues the course until he hears that Claire is in love, although not with him. Dwight has to persuade him once again to stay, telling Tad that the man Claire is in love with is an ugly and stupid man who can be out-conquered.

The publicity from Tads course makes it a success and attendance becomes much higher. Dwight convinces Tad to get a job to prove his success to the various magazines  covering the course progress, so he does. He is hired as an insurance salesman, but is soon discouraged when he is unsuccessful. Dwight secretly helps out by making his friend buy an insurance policy from Tad, unaware that his friend, Frank Mitchell, has high blood pressure and wouldnt pass the physical to get to buy a policy.

When Tad finally proposes to Claire, she says she loves him but is engaged to Dwight. Tad is upset, feeling deceived by the couple. He uses a special relaxation technique on Frank to get him to pass the necessary physical, then happens to overhear an argument between Claire and Dwight, where she scolds him for treating Tad badly. Tad regains his trust for Claire, and uses the commission from the sale of the policy to buy a fire-engine. Then he waits for Claire outside the school building and when she comes out they drive off to Vermont together as a couple. Dwight moves on to teach relaxation, using the technique Tad showed him. 

==Cast==
* Henry Fonda as Thadeus Winship "Tad" Page
* Lynn Bari as Claire Harris
* Don Ameche as Dwight Dawson
* Edward Everett Horton as Horace Hunter George Barbier as James Roger Barker
* Frank Orth as Messenger
* Roseanne Murray as Dawsons Secretary
* Marietta Canty as Jennie
* Hobart Cavanaugh as Albert Gowdy
* Hal K. Dawson as Charlie
* Josephine Whittell as Mrs. Hunter
* Arthur Loft as Mr. Morton, Fire Engine Salesman Paul Stanton as Peters
* Claire Du Brey as Peters Secretary
* William B. Davidson as Mr. J. D. Reindel
* Harry Hayden as Frank Mitchel
* Pierre Watkin as Bill Carson

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 