Thiruvabharanam (film)
{{Infobox film 
| name           = Thiruvabharanam
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = EK Thyagarajan
| writer         = J. Sasikumar Jagathy NK Achari (dialogues)
| screenplay     = Jagathy NK Achari Madhu Jayabharathi Kaviyoor Ponnamma
| music          = R. K. Shekhar
| cinematography = CJ Mohan
| editing        = K Sankunni
| studio         = Sree Murugalaya Films
| distributor    = Sree Murugalaya Films
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by EK Thyagarajan. The film stars Prem Nazir, Madhu (actor)|Madhu, Jayabharathi and Kaviyoor Ponnamma in lead roles. The film had musical score by R. K. Shekhar.   

==Cast==
*Prem Nazir Madhu
*Jayabharathi
*Kaviyoor Ponnamma
*Thikkurissi Sukumaran Nair
*Sreelatha Namboothiri
*K. P. Ummer
*Vijayasree

==Soundtrack==
The music was composed by R. K. Shekhar and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ambalamettile Thamburaatti || K. J. Yesudas, P. Madhuri || Sreekumaran Thampi || 
|-
| 2 || Ettupaaduvan Mathramaay || K. J. Yesudas, P. Leela || Sreekumaran Thampi || 
|-
| 3 || Swarnam Chirikkunnu || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Thaazhvara Charthiya || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 5 || Thalakku Mukalil || P Jayachandran || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 