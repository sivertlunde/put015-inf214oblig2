I Lost My Heart in Heidelberg (1926 film)
{{Infobox film
| name           = I Lost My Heart in Heidelberg 
| image          =
| image_size     = 
| caption        = 
| director       = Arthur Bergen
| producer       = 
| writer         = Max Ferner
| narrator       = 
| starring       = Emil Höfer   Gertrud de Lalsky   Werner Fuetterer   Mary Parker
| music          = Hans May   
| editing        =  Franz Koch
| studio         = Bavaria Film
| distributor    = Bavaria Film
| released       = 13 July 1926
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent I Lost Old Heidelberg.

==Cast==
* Emil Höfer as Pastor Schönhoff 
* Gertrud de Lalsky as Sophie, seine Frau 
* Werner Fuetterer as Rudolf - sein Sohn  Mary Parker as Charlotte, seine Tochter 
* Sylvester Bauriedl as Fritz Merkelbach - Cand.med. Erstchargierter 
* Harry Halm as Alex Winkler, Fuchsmajor 
* Karl Platen as Georg Schröder - Corpsdiener 
* Dorothea Wieck as Klärchen - seine Tochter 
* Viktor Gehring as Ingenieur Frank 
* Carla Färber as Trude - Klärchens Freundin 
* I.W. Lautsch as Bornschläger 
* Maria Meyerhofer as seine Frau 
* Josef Eichheim as Schneidermeister Stenglein 
* Else Kündinger as seine Frau 
* Frau Heuberger-Schönemann as Frau Klinger 
* Georg Irmer as Fritz merkelbach

==References==
 

==Bibliography==
* Lamb, Andrew. 150 Years of Popular Musical Theatre. Yale University Press, 2000.

==External links==
* 

 
 
 
 
 
 


 