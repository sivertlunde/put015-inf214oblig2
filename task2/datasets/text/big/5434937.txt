Sleep (film)
{{Infobox film
| name           = Sleep
| image          = 
| caption        = 
| director       = Andy Warhol
| producer       = 
| writer         = 
| narrator       = 
| starring       = John Giorno
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 321 minutes
| country        = United States
| language       = 
| budget         = 
}}

Sleep is a 1963 American film by Andy Warhol consisting of long take footage of John Giorno, his close friend at the time, sleeping for five hours and 20 minutes. The film was one of Warhols first experiments with filmmaking, and was created as an "anti-film". Warhol would later extend this technique to his eight-hour-long film Empire (1964 film)|Empire. 

Sleep premiered on January 17, 1964, presented by Jonas Mekas at the Gramercy Arts Theater as a fundraiser for Film-makers Cooperative. Of the nine people who attended the premiere, two left during the first hour. 

==See also==
* Andy Warhol filmography
* List of longest films by running time

==References==
 

==External links==
* 
*  
* 
* 

 

 
 
 
 
 
 

 