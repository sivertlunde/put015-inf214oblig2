Manitha Jaathi
{{Infobox film
| name           = Manitha Jaathi
| image          =
| image_size     =
| caption        =
| director       = P. Kalaimani
| producer       = P. Kalaimani
| writer         = P. Kalaimani
| screenplay     = P. Kalaimani Ravichandran
| music          = Ilayaraja
| cinematography = 
| editing        = 
| studio         = Everest Films
| distributor    = Everest Films
| released       =  
| country        = India Tamil
}}
 1991 Cinema Indian Tamil Tamil film, Ravichandran in lead roles. The film had musical score by Ilayaraja.  

==Cast==
*Sivakumar
*Ramki
*Nirosha Ravichandran

==Soundtrack==
The music was composed by Ilaiyaraaja.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || Vaali || 04.31 
|-  Chithra || Vaali || 04.38 
|-  Vaali || 04.48 
|-  Janaki || Vaali || 04.19 
|-  Chithra || Thirupathooran || 04.21 
|-  Vaali || 03.48 
|}

==References==
 

==External links==

 
 
 
 
 


 