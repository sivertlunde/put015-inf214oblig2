Muckman
 

{{Infobox film name        = Muckman image       = caption     = director    = Brett Piper writer      = Brett Piper  Mark Polonia starring    = A.J. Khan Allison Whitney Ian Piper producer    = Mark Polonia Ken Van Sant budget      = 50,000 runtime     = 80 minutes distributor = Chemical Burn Entertainment budget      = 500 released    =   country     = United States language    = English
}}

Muckman is a 2009 direct-to-video horror film B movie, directed by Brett Piper. It stars A.J. Khan, Allison Whitney,and Ian Piper .

== Plot ==
A failed, TV reporter (Steve Diasparra) is sent to the swamps of Pennsylvania to set up a story on the fabled creature present there, the Muckman. Initially it seems the reporter, Mickey, is having good luck, but it is later revealed that the "creature" he was seeing was a hoax. Desperate for actual leads, Mickey and his partner Asia (A.J. Khan), assemble a team to lead them to the actual creature. The team composed of Billie (Allison Whitney), Pauline (Danielle Donahue), Curly (Ian Piper) and Drew (Jared Warren), goes off to local resident, Cletus (Ken Van Sant), cabin to await the arrival of the Muckman.  Soon enough Billie finds the Muckmans eggs, and tries to hatch them. But the Muckman becomes severely angered by this, and starts to attack the crew, Cletus, and his family. 

==Cast==
* Steve Diasparra
* A.J. Khan
* Danielle Donahue
* Ian Piper
* Alison Whitney
* Ken Van Sant
* Jared Warren

==Screenings==

On May 7, 2011, Muckman debuted at the 15th installment of the Weekend of Fear festival in Erlangen/Germany.

==References==
 

==External links==
* 
*http://horrornews.net/38324/film-review-muckman-2009/
*http://www.brettpiper.com/

 
 
 
 

 