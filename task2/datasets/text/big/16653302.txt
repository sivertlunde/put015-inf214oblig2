Yellowstone Kelly
{{Infobox film
| name           = Yellowstone Kelly
| image          = Yellowstone Kelly (movie poster).jpg
| image size     =
| caption        = Theatrical release poster Gordon Douglas
| producer       =
| screenplay     = Burt Kennedy
| based on       =  
| narrator       =
| starring       = Clint Walker
| music          = Howard Jackson
| cinematography = Carl E. Guthrie
| editing        = William H. Ziegler
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.7 million (est. US/ Canada rentals) 
}} Western Technicolor Gordon Douglas.  The film was originally supposed to be directed by John Ford with John Wayne in the Clint Walker role but Ford and Wayne opted to make The Horse Soldiers instead.

At the time the film was notable for using the leads of then popular Warner Bros. Television shows, Cheyenne (TV series)|Cheyenne (Walker), Lawman (TV series)|Lawman (Russell),  77 Sunset Strip (Byrnes), and The Alaskans (Danton) as well as Warners contract stars such as Andra Martin, Claude Akins, Rhodes Reason and Gary Vinson.

The novel was based on the real life Luther Kelly. 

==Cast==
* Clint Walker as Luther "Yellowstone" Kelly
* Edd Byrnes as Anse Harper John Russell as Gall, Sioux Chief
* Ray Danton as Sayapi, Galls Nephew 
* Claude Akins as Sergeant
* Rhodes Reason as Major Towns
* Andra Martin as Wahleeah, Sayapis Arapaho Captive
* Gary Vinson as Lieutenant
* Warren Oates as Corporal
* Harry Shannon as Captain

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 