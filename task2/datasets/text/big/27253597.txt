Speed Track
{{Infobox film
| name           = Speed Track 
| image          = Speed Track.jpg
| alt            = 
| caption        = 
| director       = Jayasurya
| producer       = Subair 
| writer         = Jayasurya Dileep  Vijayaraghavan Captain Raju Arun Kalabhavan Prajod Bindu Panicker
| music          = Songs:  
| cinematography = P. Sukumar
| editing        = Ranjan Abraham
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget   = 3.7 Cr
| gross    = 5.2 Cr.
}}
Speed Track is a 2007 Malayalam film by Jayasurya starring Dileep (actor)|Dileep, Gajala, Riyaz Khan and Madhu Warrier in the lead roles.

==Plot==
The movie begins with the selection of new students in BCM college which is famous for sports and athletics. Arjun (Dileep (actor)|Dileep) is a young athlete who is all set to create history with his speed and spirits. He is going through tough times, mentally and financially following his fathers demise. He has got a share of tension for his family who badly needs him to keep them going. Arjun is also there join in BCM college  for the new academic year. But evermore he has some specific tasks and targets in behalf of joining the college.

Everyone in the campus except Tinu (Riyaz Khan), who was the unchallenged hero hailing from bigger family, welcomed Arjuns talents as he beats Tinus record in highjump.

Arjun who made consistent performances on the track put Tinu on the back seat, as Arjun become the heartthrob of many within a short time. These defeats were more than something Tinu could handle and started taking it on Arjun on a very personal level apart from sporting spirits. This leads to nasty street fights between the two which resulted in the principal (Captain Raju) calling both of them for talks. The principal challenges them both to take it on the field rather than going at each other. And both then prepares to settle the scores in the big race that follows.

==Cast== Dileep as  Arjun
*Gajala as  Gouri
*Madhu Warrier as Rahul
*Riyaz Khan as Tinu Nalinakshan
*Jagathy Sreekumar as  K. T. Kunjavara Vijayaraghavan as  Chandradas, Gouri and Rahuls Father
*Arun Kumar as Arun, Arjuns Younger Brother
*Ambika Mohan as  Arjuns Mother
*Captain Raju as  College Principal
*Salim Kumar as  Lali
*Yadu Krishnan as  Hari
*Kalabhavan Prajod as Vinod
*Bindu Panicker as M. Treesa Saikumar as  Doctor
*Kalaranjini as  Gouris Mother

==Trivia==
Speed Track is considered as the first sports(Athletic) movie in Malayalam.

== Soundtrack ==
Deepak Dev composed music for the movie and Ouseppachan done background score.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Nerathe Kalathethi"
| Deepak Dev, Jassie Gift, George Peter
|-
| 2
| "Ko Ko Ko Kozhi"
| Vineeth Sreenivasan, Rimi Tomy
|-
| 3
| "Pattum Padi"
| K. J. Yesudas
|-
| 4
| "Oru Kinnaraganam"
| Udit Narayan, Sujatha Mohan
|-
| 5
| "Ko Ko Ko Kozhi  "
| Vineeth Sreenivasan, Rimi Tomy, Jyathish 
|}

==Critical response==
The film received mixed to positive response from the audience.The film released on 2 March 2007 was ran almost 75 days and was declared as a box office hit.

==Satellite rights==
Amrita TV bought the satellite rights of the movie.

==External links==
*  
* http://www.nowrunning.com/movie/2527/malayalam/speed-track/index.htm
* http://www.indiaglitz.com/channels/malayalam/preview/7836.html

 
 
 
 
 
 
 