Key Witness (1947 film)
 
{{Infobox film
| name           = Key Witness
| image          = Key witness 1947 poster.jpg
| image_size     = 
| alt            =
| caption        = Theatrical release poster
| director       = D. Ross Lederman Rudolph C. Flothow
| screenplay     = Edward Bock
| story          = J. Donald Wilson
| narrator       = John Beal Trudy Marshall Jimmy Lloyd Helen Mowery
| music          = Mischa Bakaleinikoff
| cinematography = Philip Tannura
| editing        = Dwight Caldwell
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} John Beal, Trudy Marshall, Jimmy Lloyd and Helen Mowery. 

==Plot==
A man runs away to avoid suspicion of murder and ends up in more trouble.

==Cast== John Beal as Milton Higby
* Trudy Marshall as Marge Andrews
* Jimmy Lloyd as Larry Summers
* Helen Mowery as Sally Guthrie
* Wilton Graff as Albert Loring
* Barbara Read as Martha Higby
* Charles Trowbridge as John Ballin
* Harry Hayden as Custer Bidwell William Newell as Smiley
* Selmer Jackson as Edward Clemmons Robert Williams as Officer Johnson

==Reception==

===Critical response===
When the film was released in 1947, critic Bosley Crowther, was sly in his negative review, "The moral of Key Witness, which came to the Rialto yesterday, it says, is that no man can escape trouble by trying to run away from it. This wisdom is demonstrated in the adventures of a desperate young man who attempts to get out of one involvement by changing his identity—and runs into others thereby ... There might also be drawn this moral from the evidence presented here: you cant often be sure of entertainment from that which is claimed to be." 

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)
*  

 
 
 
 
 
 
 
 
 
 