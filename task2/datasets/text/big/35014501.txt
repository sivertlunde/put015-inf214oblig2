Le Fleuve Niger se meurt
 
 
 
{{Infobox film
| name           = Le Fleuve Niger se meurt
| image          = 
| caption        = 
| director       = Adam Aborak Kandine
| producer       = CRPF/CIRTEF Niamey
| writer         = 
| starring       = 
| distributor    = 
| released       = 2006
| runtime        = 7 minutes
| country        = Niger
| language       = 
| budget         = 
| gross          = 
| screenplay     = Adam Aborak Kandine, Ali Oumarou
| cinematography = Adam Aborak Kandine
| editing        = Adam Aborak Kandine
| music          = Issoufou Chanayé (grupo de teatro de Gaya)
}}

Le Fleuve Niger se meurt is a 2006 documentary film.

== Synopsis ==
This short documentary film tells the story of Alfari, who lives on the bank of the Niger River|Niger, a river which is slowly running dry due to climate change. Alfari had to give up fishing to become a gardener, fighting against the hippopotamus that devastate his plantations.

== Awards ==
* Nord Sud Ginebra 2006
* Angers 2007
* Quintessence de Ouidah 2008

== References ==
 

 
 
 
 
 
 
 
 
 


 
 