When the Love Is Gone
{{Infobox film
| image            = When_the_Love_is_Gone.jpg
| caption          = Theatrical movie poster
| name             = When the Love Is Gone
| director         = Andoy Ranay {{cite web|url=http://www.mb.com.ph/when-the-love-is-gone-a-big-challenge-for-ranay|title=
‘When The Love Is Gone,’ a big challenge for Ranay|publisher=Manila Bulletin|accessdate=November 18, 2013}} 
| based on         = Nagalit ang Buwan sa Haba ng Gabi (1983 film)   (English: The Moon Got Mad at the Long Night)   
| producer         =  
| music            = Terresa Barozo
| cinematography   = Pao Orendain
| story            = Keiko Aquino
| screenplay       = Keiko Aquino
| writer           = Keiko Aquino  
| editing          = Marya Ignacio
| starring         =  
| studio           =  
| distributor      = Viva Films
| released         =  
| country          = Philippines
| language         =  
| runtime          = 110 minutes
| budget           =
| gross            = PHP 37,412,869 

 
}}
 romantic drama film directed by Andoy Ranay,  starring Cristine Reyes, Gabby Concepcion, Alice Dixson, Andi Eigenmann, and Jake Cuenca. The film is distributed by Viva Films with co-production of Multivision Pictures and was released November 27, 2013 nationwide as part of Viva Films 32nd anniversary.               

The movie is a remake of the 1983 blockbuster movie directed by Danny Zialcita entitled Nagalit ang Buwan sa Haba ng Gabi which stars Dindo Fernando, Gloria Diaz, Laurice Guillen, Janice de Belen, and Eddie Garcia.  

== Cast ==

=== Main cast ===
*Cristine Reyes     as Cassandra "Cassie"
*Gabby Concepcion  as Emmanuel "Emman" Luis
*Alice Dixson   as Audrey Luis
*Andi Eigenmann  as Jenny Luis
*Jake Cuenca  as Yuri

=== Supporting cast ===
*Dina Bonnevie as Zelda Kagaoan-Luis
*Pilar Pilapil as Cynthia
*Anton Revilla as Gabe Kagaoan

=== Special participation ===
*Hideo Muraoka as Paolo
*Lander Vera Perez as Noel
*DJ Durano as Miko
*Shy Carlos as Chloe
*Jaime Fabregas as Yuris Father
*Ana Abad Santos as Grace
*Thou Reyes as Petra

==Awards and nominations==
2014 16th Gwad PASADO Awards Pinakapasadong Katuwang na Aktor Jake Cuenca - Won
Alice Dixson - Nominee

== International screening ==
Starting December 6, 2013, When the Love Is Gone will be shown in selected theaters in Los Angeles, San Francisco, San Diego, Las Vegas, Virginia, Texas, Arizona, Nevada, Washington and Hawaii. 

== Reception ==
=== Critical response ===
Bernie Franco of PEP.ph gave the film a positive review, stating "When The Love Is Gone can hardly be accused of simply riding on the bandwagon because it is a remake of the 1983 Filipino movie "Nagalit ang Buwan sa Haba ng Gabi".    He also stated that The twists in the story are definitely something to watch for and these separate When The Love is Gone from other drama movies also tackling the same theme and  is worth watching despite the proliferation of movies about extramarital affairs nowadays.  He also praised the casts performance and the films script. 

Pablo Tariman of Philippine Star also gave the film a positive review, stating that When the Love Is Gone is another polished variation of a beautiful love story sensually told with wit and finesse.    He also praised Ranays astounding performance as a director. 

However, Phylber Ortiz Dy gave a negative review gave the film 1 out of 5 stars, stating that When the Love Is Gone is all just nonsense. The relationships dont make any sense, and the people do not act like real human beings.    He also stated that When the Love is Gone only gets worse in its endgame. There it disregards whatever trauma the characters went through to provide a shade of a happy ending   Nothing really happened. The whole thing was just an exercise in creating scenes where women yell at each other and generally behave badly. It is toxic and hateful and generally just terrible. 

== See also ==
* My Neighbors Wife In the Name of Love
* No Other Woman The Mistress
* A Secret Affair One More Try
* Seduction (2013 film)|Seduction
* The Bride and the Lover Trophy Wife
* Once a Princess The Gifted The Trial

== References ==
 

== External links ==
* 
*  at VIVA Films
* 

 
 
 
 
 
 