Escanaba in da Moonlight
 
{{Infobox film |
  name=Escanaba in da Moonlight|
  image=Escanaba in da moonlight.JPG|
  caption = Film poster|
  writer=Jeff Daniels   Guy Sanville |
  starring=Jeff Daniels   Harve Presnell |
  director=Jeff Daniels |
  music=Alto Reed | 	 
  released=January 26, 2001 |
  runtime=91 minutes |
  country=United States|
  language=English|
  movie_series=|
  awards=|
  producer=Robert L. Brown   Tom Spiroff |
}} Yooper for Jeff Daniels Purple Rose Theatre in Chelsea, Michigan.

==Plot==
 Menominee (Wayne David Parker).  If Reuben, now 43, doesnt manage to shoot a buck by the end of the season, he will become the oldest Soady in recorded history not to have achieved this task, a taboo that leads people in the community to believe he is jinxed.
 Native American DNR officer, Tom T. Treado (Randall Godwin), who claims to have literally seen God on the ridge.

At various times, Reuben, Jimmer, and ranger Tom all get possessed by spirits. Eventually, Reuben runs out into the cold wearing only his long underwear and a hat, and finds himself face-to-face with his dead great-grandfather Alphonse, who guides him to shooting a buck sent for him by the spirits.  Reuben returns triumphantly.

==Yooper/Michigan Culture==

A significant portion of the movie involves references to elements of Upper Peninsula of Michigan|"Yooper" (slang reference to residents of the U.P. or Upper Peninsula of Michigan) and broader Michigan culture.  Some references are obscure to viewers unfamiliar with this culture and might be considered in-jokes.

Some examples include:

*  Pasty#In other Cornish-influenced regions|Pasties—the traditional foodstuff at the Soady deer camp, and food commonly associated with Yooper culture
*  Jacob Leinenkugel Brewing Company|Leinenkugels Beer—Remnar brings a case to deer camp, a reminder of Escanabas proximity to Wisconsin, where Leinenkugels is produced
*  Mackinac Bridge—simply referred to as "the Bridge" throughout the film, the bridge that connects Michigans peninsulas Mackinac Island Fudge—Albert refers to tourists from the Lower Peninsula of Michigan as "those fudgesuckers," a reference to the fudge made on Mackinac Island, a considerable draw for tourism from within the state The Superior State—used a few times to refer to the films location; although a nickname for the state of Michigan as a whole, Superior is also the name of a once-proposed 51st state formed from the Upper Peninsula and, in some iterations, parts of Wisconsin US Highway&nbsp;41—an old shield for this highway hangs on the wall in the Soady cabin; an important north-south highway in the western to central U.P.
*  Pictured Rocks National Lakeshore—natural formation along lake shore mentioned briefly
*  Euchre—a card game popular in the Midwest, possibly introduced to the United States by the early German settlers of Michigan
*  M-35 (Michigan highway)|M35—a state highway starting at Menominee in the south, passing through Escanaba, and ending at US41/M28 between Marquette and Negaunee in the north


==See also==

*Yooper dialect

==External links==
* http://dexter.patch.com/articles/review-jeff-daniels-escanaba-in-da-moonlight-comes-home-to-the-purple-rose
*  

 
 
 
 
 
 
 