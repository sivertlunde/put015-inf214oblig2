The Mystery of Edwin Drood (1993 film)
 
{{Infobox film
| name           = The Mystery of Edwin Drood
| image          = The Mystery of Edwin Drood 1993 DVD.jpg
| image_size     = 
| caption        = A&E DVD cover
| director       = Timothy Forder
| producer       = Keith Hayley, Jo Gilbert
| writer         =     Timothy Forder Jonathan Phillips Robert Powell Finty Williams
| music          = Kick Production
| cinematography = Martin McGrath
| editing        = Sue Alhadeff    
| distributor    = A&E Home Video (USA) Curzon Gold Video (AUS)
| released       = 23 April 1993  	
| runtime        = 112 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
}}
 unfinished novel of the same title.

==Cast==
* Robert Powell as John Jasper Jonathan Phillips as Edwin Drood
* Peter Pacey as Septimus Crisparkle
* Nanette Newman as Mrs. Crisparkle
* Freddie Jones as Sapsea
* Gemma Craven as Miss Twinkleton
* Marc Sinden as Mr Honeythunder
* Rosemary Leach as Mrs. Tope
* Glyn Houston as Grewgious
* Andrew Sachs as Durdles Barry Evans as Bazzard Ronald Fraser as Dean
* Finty Williams as Rosa

==Locations==
Many scenes were filmed in Rochester, Kent|Rochester, including Minor Cannon Row and Rochester Cathedral which doubled as Cloisterham Cathedral.  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 