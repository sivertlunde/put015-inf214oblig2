Wild 90
{{Infobox film
| name           = Wild 90
| image          =Wild 90.jpg
| image_size     =
| caption        = dvd cover
| director       = Norman Mailer
| producer       = Norman Mailer
| writer         = Improvised by the cast
| narrator       = Mickey Knox
| music          =
| release        =
| cinematography = D.A. Pennebaker
| editing        =
| released       = January 8, 1968 (USA)
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| distributor    = Supreme Mix Inc.
}}
 1968 experimental film directed and produced by U.S. novelist Norman Mailer, who also plays the starring role.

==Plot== Mickey Knox)—are hiding in a warehouse.  They have surrounded themselves with guns and liquor, and they kill time by joking and bickering with scatological language. But as their isolation from the world progresses, their drinking and arguing intensifies. They are briefly visited by a man with a barking dog—the canine is silenced when The Prince outbarks him—and by two women, one of whom gives The Prince a knife for committing suicide. The police arrive at the warehouse and the gangsters are taken away.   

==Production==
Wild 90 was the first attempt by Norman Mailer to create a motion picture. The concept for the film came when Mailer and several actors who were appearing in an Off-Broadway adaptation of his novel The Deer Park engaged in an acting game where they pretended they were gangsters. The title Wild 90 is a reference to alleged Mafia slang term for being in deep trouble.   

Mailer spent $1,500 of his own money to finance Wild 90. D.A. Pennebaker, the documentary filmmaker, was the cinematographer and shot the film in black-and-white 16mm.  The production took place over four consecutive nights and the entire film was improvised by Mailer and his cast. The resulting dialogue was unusually heavy with profanities and Mailer later claimed that Wild 90 "has the most repetitive, pervasive obscenity of any film ever made." 

The Puerto Rico-born boxer José Torres appeared as the man with the barking dog and Beverley Bentley (Mailer’s wife) played the woman with the knife. Mailer did not allow any retakes during the shoot. 

Mailer wound up with 150 minutes of film, which was edited down to 90 minutes.   Due to a technical glitch during the production, roughly 25 percent of the film’s soundtrack came out muffled. Mailer refused to redub the problem patches on the soundtrack and later joked the film “sounds like everybody is talking through a jockstrap.” 

==Release==
Pennebaker tried to convince Mailer not to put Wild 90 into theatrical release because of the problematic nature of its soundtrack.  Mailer disregarded that suggestion and went forward by self-distributing the film. He also promoted the film extensively, which included writing a self-congratulatory essay on the film that appeared in Esquire magazine.   

Reviews for Wild 90 were overwhelmingly negative.  , stated that the film was “rambling, repetitious...incoherent and inept.”  Stanley Kauffmann, writing in The New Republic, said that “I cannot say that Mailer was drunk the whole time he was on camera. I can only hope he was drunk.” 

Mailer responded to the bad reviews by including them in the original theatrical poster.  Wild 90 was a commercial failure, but Mailer followed up the production with two additional improvised experimental films, Beyond the Law (1968) and Maidstone (film)|Maidstone (1970). 

==References==
 

==External links==
* 

 
 
 
 