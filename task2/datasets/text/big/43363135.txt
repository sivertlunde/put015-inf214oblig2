Paths of War
{{Infobox film
 | name = Paths of War -Franco e Ciccio sul sentiero di guerra-
 | image = Paths of War.jpg
 | caption =
 | director = Aldo Grimaldi
 | writer =   Bruno Corbucci  Giovanni Grimaldi 
 | starring =  Ciccio Ingrassia & Franco Franchi 
 | music =  Roberto Pregadio
 | cinematography =  Fausto Zuccoli
 | editing = Daniele Alabiso
 | producer = Sergio Bonotti
 | released =   
 | language =   Italian country = Italy
 | runtime = 95 min
 }} 1970 Cinema Italian Spaghetti Western|western-comedy film directed by Aldo Grimaldi.         

== Plot summary == Bourbon army to prevent the unification of Italy built by Giuseppe Garibaldi. However, when the troops of Garibaldi defeate the Bourbons, Franco and Ciccio escape, taking refuge in a box, which is delivered in America. In the Far West, Franco and Ciccio find themselves involved in the American War of Independence against the Apache Indians. They, camouflage, disguise themselves first by warlike Americans, and then by Indian holy men, being able to save their skin.

== Cast ==

* Franco Franchi: Franco
* Ciccio Ingrassia: Ciccio
* Renato Baldini: Jeff
* Adler Gray: Lucy Foster
* Joseph P. Persaud: Indian Chief 
* Alfredo Rizzo: Sgt. Douglas
* Lino Banfi: Mormone
* Luigi Bonos: Barman

==References==
 

==External links==
* 

 
 
 
 
  
 
 
 
 


 
 