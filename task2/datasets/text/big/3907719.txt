The Beast in the Heart
 
{{Infobox Film name    = The Beast in the Heart (Dont Tell) image   =The Beast in the Heart.jpg writer  = Cristina Comencini Giulia Calenda Francesca Marciano starring = Giovanna Mezzogiorno Alessio Boni Luigi Lo Cascio  director = Cristina Comencini producer = Marco Chimenz Giovanni Stabilini distributor = Lionsgate Films released = 9 September 2005 (Italy) March 17, 2006(Usa) runtime  = 120 min language = English
|budget   = ~ Euro|€6,000,000|}}
 2005 film directed by Cristina Comencini, based on the novel written by herself.
 Venice International Best Foreign Language Film category in the 78th Academy Awards.

==Synopsis==
Sabina has a regular life. She is satisfied with her job and her love for Franco. But lately nightmares start disturbing her, just as she discovers she is pregnant. Her childhood, spent with an older brother, has a dark secret hidden within her heart, and she goes to visit her brother in America, to try to understand what happened in their past. What is the secret? She is determined to bring clarity and serenity in her life. She finally manages to free herself from her "beast in the heart".

==Cast==
*Giovanna Mezzogiorno as Sabina
*Alessio Boni as Franco
*Stefania Rocca as Emilia, long-time friend of Sabina
*Angela Finocchiaro as Maria, friend and co-worker of Sabina 
*Luigi Lo Cascio as Daniele, Sabinas brother 
*Francesca Inaudi as Anita, Danieles wife
*Giuseppe Battiston as Andrea Negr
*Valerio Binasco as Father 
*Simona Lisi as Mother 

==Awards==
* David di Donatello Awards for the Best Supporting Actress: Angela Finocchiaro
* 3 Nastro dArgento Prizes (Best Supporting Actress: Angela Finocchiaro - Best Cinematography: Fabio Cianchetti - Best Producer: Riccardo Tozzi, Marco Chimenz and Giovanni Stabilini)
* Volpi Cup for the Best Actress at the Venice Film Festival: Giovanna Mezzogiorno

==External links==
* 
*  
*  on European-films.net

 
 
 
 
 
 
 
 
 

 