Zoop in Africa
{{Infobox film
| name           = Zoop in Africa
| image          = File:ZOOP in Afrika promotie bus Vivienne van den Assem Sander Jan Klerk.JPG
| caption        = Zoop in Africa
| director       = Johan Nijenhuis
| producer       = Alain De Levita Johan Nijenhuis
| writer         = Anya Koek Wijo Koek Johan Nijenhuis
| narrator       =
| starring       = Sander Jan Klerk Nicolette van Dam
| music          = Martijn Schimmer Mario Zapata
| cinematography =
| editing        = Sandor Soeteman
| distributor    =
| released       =  
| runtime        = 85 minutes
| country        = Netherlands English
| budget         =
}}

Zoop in Africa ( ) is a 2005 Dutch adventure film, directed by Johan Nijenhuis. The film is based on the TV series Zoop (TV series)|Zoop and is followed by Zoop in India (2006) and Zoop in South America (2007). The film was recorded on multiple locations in South Africa.
 
The film premiered on july 10th 2005 at the Tuschinski theatre in Amsterdam. On November 10, 2005 the film was released on dvd. Zoop in Africa  was sold to 44 countries, including Argentina, Brazil, Chile, China, Columbia, Germany, France, Mexico, Poland, Russia and Turkey.

== Plot ==
Eight youngsters studying for zookeeper in the Netherlands travel to Africa, to work in a wildpark and enhance their knowledge. During the flight to their destination their plane crashes in the middle of the jungle and they are completely dependent on each other. They decide to split up in two groups in the search for help. Aside from the survival challenge, the owners of the wildpark want to get rid of the rangers too. When Bionda gets lost, things go from bad to worse. Then they encounter an African tribe who doesnt have good intentions either...

== Cast ==
The Rangers:
* Viviënne van den Assem (Elise Pardoel)
* Juliette van Ardenne (Sira Schilt)
*Nicolette van Dam (Bionda Kroonenberg)
* Ewout Genemans (Bastiaan van Diemen)
* Jon Karthaus (Moes Brinksma)
*Sander Jan Klerk (Aaron Zomerkamp)
* Patrick Martens (Mike Bosboom)
* Monique van der Werff (Taffie Arends)
Other:
* Sylvana Simons (Safira)
* Sabine Koning (Gaby Komproe)
* Raymi Sambo (Bowey Berenger)
* Ernst Löw (Siegfried Schilt)
* Pieternel Pouwels (Maxime Schilt)
* Kim Boekhoorn (Filo Schilt)

 

==Awards==
The film received a Golden Film, the "Nickelodeon Kids Choice Award", an award for the best film,
and the "Prijs van succes" of the Nederlands Filmfonds.

==External links==
* 

 
 
 
 
 
 
 
 