The Plot Against Harry
 
{{Infobox film
| name           = The Plot Against Harry
| image          = Poster of The Plot Against Harry.jpg
| caption        = 
| director       = Michael Roemer
| producer       = Michael Roemer
| writer         = Michael Roemer
| starring       = Martin Priest
| music          =  Robert M. Young
| editing        = Georges Klotz
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
}}

The Plot Against Harry is a 1989 American comedy film directed by Michael Roemer and filmed in 1969.     It was screened out of competition at the 1990 Cannes Film Festival.   

==Cast==
* Martin Priest as Harry Plotnick
* Ben Lang as Leo
* Maxine Woods as Kay
* Henry Nemo as Max
* Jacques Taylor as Jack
* Jean Leslie as Irene
* Ellen Herbert as Mae
* Sandra Kazan as Margie
* Ronald Coralian as Mel Skolnik
* Max Ulman as Sidney

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 