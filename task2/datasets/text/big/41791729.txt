The Garden Murder Case (film)
{{Infobox film
| name           = The Garden Murder Case
| image          = The-Garden-Murder-Case poster.jpg
| caption        = theatrical poster
| image_size     = 250px
| director       = Edwin L. Marin
| producer       = Lucien Hubbard Ned Marin
| screenplay     = Bertram Millhauser
| based on       =  
| starring       = Edmund Lowe Virginia Bruce
| music          = William Axt
| cinematography = Charles Clark
| editing        = Ben Lewis
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} The Casino Douglas Walton 1935 book of the same name by S. S. Van Dine.

==Plot==
Society swell and dilettante detective Philo Vance investigates a number of murders, beginning with the apparent suicide of a jockey during a "gentlemans race", which is followed by the sudden collapse of his father, Dr. Garden, supposedly from the shock of his sons death.  Then, Edgar Lowe Hammle, who seems to be at the center of much of the intriguing going on in his mansion, is shot dead.  At first suicide is suspected, until Vance notices that theres no powder burns on the body.  From the choice of the weapon used, Vance suspects that one of the women involved is the murderer, but when questioned each of them tries to throw suspicion on one of the others.

At the end of his investigation, however, all the clues point to the man calling himself "Major Fenwicke-Ralston", who is in reality a hypnotist, fakir and charlatan.  When Vance confronts him, the "Major" attempts to hypnotize Vance into killing himself, but Vance is not easily put under, and merely pretends, and the Major is shot by Sergeant Heath.

==Cast==
  
*Edmund Lowe as Philo Vance
*Virginia Bruce as Zalia Graem
*Benita Hume as Nurse Gladys Beeton Douglas Walton as Floyd Garden
*Nat Pendleton as Sergeant Ernest Heath
*Gene Lockhart as Edgar Lowe Hammle
*H. B. Warner as Major Fenwicke-Ralston
 
*Kent Smith as Woode Swift Grant Mitchell as District Attorney Markham
*Frieda Inescort as Mrs. Madge Fenwicke-Ralston
*Henry B. Walthall as Dr. Garden
*Jessie Ralph as Mrs. Hammle
*Charles Trowbridge as Inspector Colby
*Etienne Girardot as Dr. Doremus (coroner)
 

==Production==
The racetrack scene in The Garden Murder Case was filmed at   

MGM originally considered   

==Critical response==
On Allmovie, Craig Butler wrote that "Although filmed on a budget, production values are decent, with some especially noteworthy art deco settings" but calls Edwin L Marins direction "rather pedestrian".  Edmund Lowes performance as Philo Vance "bring  his personal style to the role and giv  it a small twist here and there that adds fun without violating the characters inner nature."   Turner Classic Movies comments that Lowe was "well past his leading man heyday and ten years older than Philo Vances stated age." 

==References==
Notes
 

==External links==
* 
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 