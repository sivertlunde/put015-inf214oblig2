Tu Tithe Mee
{{Infobox film
| name           = Tu Tithe Mee - Marathi Movie
| image          = Tu Tithe Mee.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Sanjay Surkar
| producer       = Smita Talwalkar
| screenplay     = 
| story          = 
| starring       = Mohan Joshi Suhas Joshi
| music          = Anand Modak
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}} Marathi movie Best Feature Film in Marathi award at the 46th National Film Awards. 

== Plot ==
Nanasaheb Date retires at the age of 60 and realizes that he has missed out on simple pleasures of life while attending to household duties and work responsibilities. He decides to enjoy whatever has been left out, in whichever manner possible, with his wife. His content upper-middle-class family consists of his wife, sons, daughters-in-law and grandchildren.

Suddenly, his younger son is transferred to another town. The sons decide with their wives that their father should stay back with the elder son, while their mother should accompany the younger son. The parents are disappointed with this decision of separation, but hesitate to say it openly. They ultimately find a solution.

== Cast ==
* Mohan Joshi as Nanasaheb Date
* Suhas Joshi
* Smita Talwalkar
* Prashant Damle
* Kavita Lad
* Sudhir Joshi
* Sunil Barve
* Sharvani Pillaai

==Soundtrack==
The music has been provided by Anand Modak. 

===Awards & Accolades=== Best Feature Film in Marathi category 
*Winner of 12 State Awards 
*Winner of 5 Filmfare Awards 

== References ==
 

== External links ==

 

 
 