In the Days of Buffalo Bill
 
{{Infobox film
| name           = In the Days of Buffalo Bill
| image          = Days-Of-Buffalo-Bill.jpg
| caption        = Advertisement in The Film Daily, July 2, 1922
| director       = Edward Laemmle
| producer       = Carl Laemmle Robert Dillon
| starring       = Art Acord Duke R. Lee
| cinematography = Herbert Kirkpatrick Howard Oswald
| editing        =  Universal Film Manufacturing Co.
| released       =  
| runtime        = 18 episodes
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 western film serial directed by Edward Laemmle. The film is considered to be lost film|lost.   

==Cast==
* Art Acord as Art Taylor
* Duke R. Lee as Buffalo Bill Cody George A. Williams as Calvert Carter
* Jay Morley as Lambert Ashley
* Otto Nelson as Alden Carter
* Pat Harmon as Gaspard
* Jim Corey as Quantrell
* Burton Law as Allen Pinkerton (as Burton C. Law)
* William De Vaull as Edwin M. Stanton (as William P. Devaull)
* Joel Day as Abraham Lincoln
* J. Herbert Frank as Abraham Lincoln (as J. Herbert Frank)
* Clark Comstock as Thomas C. Durant
* Charles Colby as William H. Seward
* Joseph Hazelton as Gideon Welles (as Joe Hazleton)
* John W. Morris as Gen. U. S. Grant
* Lafe McKee as Gen. Robert E. Lee (as Lafayette McKee)
* G.B. Philips as Montgomery Blair
* Tex Driscoll as Gen. U.S. Grant (as John W. Morris)
* Harry Myers as Andrew Johnson (as Henry Myers)
* Ruth Royce as Aimee Lenard
* Chief Lightheart as Sitting Bull
* William Knight as Jack Casement
* Elsie Greeson as Louise Frederici
* Buck Connors as Hank Tabor
* Millard K. Wilson as Tim OMara (as M.K. Wilson)
* William F. Moran as John Wilkes Booth (as William Moran)
* Silver Tip Baker as Gen. Grenville M. Dodge (as Silvertip Baker) Charles Newton as Maj. North
* Alfred Hollingsworth as Chief Justice Chase
* Lester Cuneo
* Marion Feducha as Andrew Johnson as a boy
* Helen Farnum (uncredited)
* Joe Miller (uncredited)
* Dorothy Wood (uncredited)

Note: Lincoln and Grant are each played by two actors.

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 
 