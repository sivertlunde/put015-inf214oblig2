War of the Arrows
{{Infobox film
| name           = War of the Arrows
| image          = War of the Arrows film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =    
| hanja          =   활
| rr             = Choejongbyeonggi Hwal 
| mr             = Choechongpyŏngki Hwal}}
| director       = Kim Han-min
| producer       = Jang Won-seok Kim Sung-hwan
| writer         = Kim Han-min 
| starring       = Park Hae-il Moon Chae-won  Ryu Seung-ryong 
| music          = Kim Tae-seong
| cinematography = Kim Tae-seong   Park Jong-chul 
| editing        = Kim Chang-ju   Steve M. Choe   Heo Sun-mi
| distributor    = Lotte Entertainment
| released       =  
| runtime        = 122 minutes
| country        = South Korea Korean  Manchu
| budget         = 
| gross          = 
}} South Korean period action film starring Park Hae-il, Ryu Seung-ryong and Moon Chae-won. Set after the Second Manchu invasion of Korea, the film is about an archer who risks his life to save his sister from slavery under Prince Dorgons rule.  
 highest grossing Korean film of 2011.  It was also honored at the 48th Grand Bell Awards and the 32nd Blue Dragon Film Awards, including Best Actor for Park, Best Supporting Actor for Ryu, and Best New Actress for Moon.  

The films alternate title is Arrow: The Ultimate Weapon.

==Plot== King Injos King Gwanghae and a skilled archer. He sends his own children to find a place of refuge with his best friend Kim Mu-seon. As they escape crying, Ja-in begs her brother to go back to their father but their father is killed in front of Nam-yi. Nam-yi, though bitten by the guard dogs, kills them and escapes with Ja-in. Nam-yi becomes the only family Ja-in has. 13 years later Nam-yi (Park Hae-il) is now a skilled archer and hunter. He learns from Mu-seons son Seo-goon (Kim Mu-yeol) that he and Ja-in (Moon Chae-won) plan to get married, with the approval of Mu-seon who is also Ja-ins godfather.

During the wedding, Nam-yi is up in the mountains hunting deer. He hears the rumble of the invading forces. When Nam-yi makes it back to the village, he finds his step-father slaughtered and his sister taken away. Nam-yi then sets out to find the Qing army and take out their army with his bow, killing a great many of them, including the prince. The great commander of the Qing army, Jyuushinta (Ryu Seung-ryong) discovers the mysterious man trailing his men and taking them out one by one. Jyuushinta then sets out to find Nam-yi. Though Nam-yi is shot in the arm, he shoots back at Jyuushinta and one of his men who both fall. Nam-yi rides away to find Ja-in as he thinks that Jyuushinta is dead - but he isnt. Meanwhile Nam-yi finds Ja-in in a field. They are about to reunite when Ja-in sees Jyuushinta aiming at Nam-yi from a cliff. But before the arrow hits, Ja-in shoots the horse and Nam-yi falls. As Nam-yi and Jyuushinta face off, Ja-in runs in between them. Nam-yis arrow barely touches Ja-ins dress, but Jyuushintas finds its mark. Despite Ja-ins protests, Nam-yi lethally pulls it out and shoots Jyuushinta who falls to the ground, finally killed. Nam-yi falls down as well but Ja-in catches him in her lap. Nam-yi says that they should go back to their old home in Seoul. His eyelids then flutter and close—forever. Then Ja-in lays Nam-yi into a boat and she and Seo-goon set sail for Seoul, just as Nam-yi requested.

==Cast==
*Park Hae-il ... Choi Nam-yi 
*Moon Chae-won ... Choi Ja-in 
*Kim Mu-yeol ... Kim Seo-goon 
*Ryu Seung-ryong ... Jyuushinta 
*Park Ki-woong ... Dorgon, Prince of Qing Manchu-China
*Ryohei Otani ... Nogami 
*Kim Ku-taek ... Gang-du 
*Lee Han-wi ... Gab-yong 
*Lee Geung-young  ... Kim Mu-seon 
*Kang Eun-jin ... Eun-yi Lee Seung-joon ... Wan-han
*Lee Jae-gu ... Hoo-man
*Park No-shik ... Jang-soon
*Lee David ... young Nam-yi
*Jeon Min-seo ... young Ja-in 
*Yoon Dong-hwan  ... Choi Pyeong-ryung

===English dubbed cast===
*Travis Willingham ... Choi Nam-yi (voice)
*Alexis Tipton ...  Choi Ja-in (voice)
*Mirra Capper ... Ja-in as a small girl (voice)

==Awards and nominations==
{| class="wikitable"
|-
!Year
!Award
!Category
!Recipient
!Result
|- 2011
| rowspan=8|  48th Grand Bell Awards 
| Best Film
| War of the Arrows
|  
|-
| Best Actor
| Park Hae-il
|  
|-
| Best New Actress
| Moon Chae-won
|  
|-
| Best Cinematography
| Kim Tae-seong, Park Jong-chul
|  
|-
| Best Art Direction
| Jang Chun-seop
|  
|-
| Best Costume Design
| Kwon Yu-jin
|  
|-
| Best Visual Effects
| Han Young-woo
|  
|-
| Best Sound Effects
| Choi Tae-young
|  
|-
| rowspan=2|  31st Korean Association of Film Critics Awards
| Best Cinematography
| Kim Tae-seong, Park Jong-chul
|  
|-
| Best Visual Effects
| Han Young-woo
|  
|- 32nd Blue Dragon Film Awards 
| Best Film
| War of the Arrows
|  
|-
| Best Director
| Kim Han-min
|  
|-
| Best Actor
| Park Hae-il
|  
|-
| Best Supporting Actor
| Ryu Seung-ryong
|  
|-
| Best New Actress
| Moon Chae-won
|  
|-
| Best Cinematography
| Kim Tae-seong
|  
|-
| Best Art Direction
| Jang Chun-seop
|  
|-
| Best Lighting
| Kim Gyeong-seok
|  
|-
| Best Music
| Kim Tae-seong
|  
|-
| Technical Award
| Oh Se-young
|  
|-
| Audience Choice Award for Most Popular Film
| War of the Arrows
|  
|-
| rowspan=3|  19th Korean Culture and Entertainment Awards
| Grand Prize ("Daesang") for Film
| Park Hae-il
|  
|-
| Top Excellence Award, Actor in a Film
| Ryu Seung-ryong
|  
|-
| Excellence Award, Actress in a Film
| Moon Chae-won
|  
|- 2012
| 6th Asian Film Awards  Best Actor 
| Park Hae-il
|  
|-
| Peoples Choice Award for Favorite Actor
| Park Hae-il
|  
|- 48th Baeksang Arts Awards
| Best Actor
| Park Hae-il
|  
|}

==References==
 

== External links ==
*    
*    
*   at Naver  
*  
*  
*  

 
 
 
 
 
 
 
 
 