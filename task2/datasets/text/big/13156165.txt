Hidden Track (film)
 
 
{{Infobox film
| name = Hidden Track
| film name = {{Film name| traditional = 尋找周杰倫
| simplified = 寻找周杰伦}}
| director = Audrey Lam
| producer = Teddy Chan
| writer = Audrey Lam David Wu Eason Chan Emme Wong Denise Ho Ah Niu Jay Chou
| distributor = Jin Chuan Pictures
| released =  
| runtime = 100 minutes
| country = Hong Kong Cantonese Standard Mandarin
}}
Hidden Track (尋找周杰倫) is a film based in Hong Kong with a mixture of Mandarin and Cantonese speakers.

==Cast==
* Po Lok-Tung - Pu Pu
* Shawn Yue - Yu Wenle
* Daniel Wu  David Wu - Ng Dai-Wai
* Ah Niu - Chan Hing-Cheung
* Denise Ho - Wan-Si
* Eason Chan (Yik-Shun) - Chen Yixun
* Emme Wong - Yi-Man
* Tang Siu-Yun
* Jay Chou - Himself

==Plot==
Pu Pu is dumped by her boyfriend whom she loves. Before she moves out, she asks to listen to "their song" just one more time, that is the hidden track by Jay Chou. Then she leaves him and goes to her sisters place in Hong Kong. All the while she is there, she searches for the same song, the "hidden track", and from this it leads her onto a journey of discovering love and a new beginning. Despite the whole movie revolving around Jay Chous song, Jay Chou plays only a cameo part.

==Awards==
23rd Annual Hong Kong Film Award 
• Nomination - Best New Artist (Po Lok-Tung)

==External links==
*  
*  

 
 


 