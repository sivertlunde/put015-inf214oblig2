A Beast at Bay
{{infobox film
| title          = A Beast at Bay
| image          =
| imagesize      =
| caption        =
| director       = D. W. Griffith
| producer       = Biograph
| writer         = George Hennessy
| starring       = Mary Pickford
| music          =
| cinematography = G. W. Bitzer
| editing        =
| distributor    = Biograph
| released       = May 27, 1912
| runtime        = 17 minutes
| country        = United States
| language       = Silent
}}
A Beast at Bay is a 1912 silent short film directed by D. W. Griffith.

==Cast==
*Mary Pickford - The Young Woman
*Edwin August - The Young Womans Ideal
*Alfred Paget - The Convict
*Mae Marsh - The Young Womans Friend

===Rest of cast===
*Elmer Booth - ?unconfirmed
*Christy Cabanne - Station Master
*William A. Carroll - Guard
*Francis J. Grandon - unconfirmed
*Robert Harron - A Farmer
*J. Jiquel Lanoe - At Station
*Henry Lehrman - A Guard
*Charles Hill Mailes - Guard
*Marguerite Marsh - (*billed Marguerite Loveridge)
*Lottie Pickford - unconfirmed
*W. C. Robinson - Guard

==References==
 

==External links==
* 
* 
*  available for free download at  

 

 
 
 
 
 
 
 


 