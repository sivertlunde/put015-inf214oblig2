April (1998 film)
::Aprile redirects here, for the fictional character see Richie Aprile

{{Infobox film name = Aprile image = Aprile.jpg caption = French film poster writer = Nanni Moretti starring = Nanni Moretti Silvio Orlando Silvia Nono Daniele Luchetti director = Nanni Moretti producer = Angelo Barbagallo Jean Labadie Nanni Moretti distributor = released = 20 May 1998 (France) runtime = 78 minutes cinematography = Giuseppe Lanci editing = Angelo Nicolini Daniele Sordoni music = Ludovico Einaudi country = Italy language = Italian budget =
}} Italian semi-autobiographical film directed by Nanni Moretti. Moretti also played the central character, a filmmaker who has to deal with Italys political situation, his own goals as an artist and becoming a father.

"He wants to make a musical comedy but he cant get started. So instead he talks about his life, his new baby, his desires and fears. Finally in the very last scene he begins." 

Morettis wife, son and mother appear as themselves.

==Plot==
Moretti is a director in crisis and would like to make a musical film that speaks to the history of a pastry in the   of Silvio Berlusconi and his corrupt colleagues (including Emilio Fede in the TV news) in the government. Moretti, a Communist Party, would like a jolt did topple the government of Berlusconi, but to no avail. After a few days he really wants to make a movie about all the misdeeds of the Prime Minister, but his plans are thwarted by the birth of his child, to whom he devotes himself with an obsessive love. Not knowing what to do, as he seriously risking to fail as a film director, Moretti takes up the idea of the film on the pastry singer.

==Cast==
* Nanni Moretti - Himself
* Silvio Orlando - Himself
* Silvia Nono - Herself
* Pietro Moretti - Himself
* Agata Apicella Moretti - Herself
* Nuria Schoenberg - Herself
* Silvia Bonucci - Herself
* Quentin de Fouchécour - Himself
* Renato De Maria - Himself
* Claudio Francia - Himself
* Jacopo Francia - Himself
* Matilde Francia - Herself
* Daniele Luchetti - Himself
* Giovanna Nicolai - Herself
* Nicola Piepoli - Himself

==Awards==
* David di Donatello 1998: Best Supporting Actor (Silvio Orlando) Cannes Film Festival - Palme dOr (nominated)   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 
 