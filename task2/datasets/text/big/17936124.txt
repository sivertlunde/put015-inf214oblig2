Sista dansen
{{Infobox film
| name           = Sista dansen
| image          = Sista Dancen poster.jpg
| caption        = Swedish theatrical release poster
| director       = Colin Nutley
| producer       = Katinka Faragó
| writer         = Colin Nutley
| starring       = Helena Bergström Reine Brynolfsson
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 113 minutes (TV-version 109 minutes)
| country        = Denmark Norway Sweden
| language       = Swedish
| budget         =
| gross          =
}}
Sista dansen (en. The Last Dance) is a Danish-Norwegian-Swedish 1993 film directed by Colin Nutley.

==Plot==
A Swedish woman is found dead in Blackpool. Was she murdered?

==Production==
Filming took place between 19 April and 9 July 1993 in Stockholm, Blackpool and Barbados. The Swedish Film Institute suggested the film for the category of best foreign film to the Academy Award of 1994, but the film was not nominated. 504 580 saw the film  .

==Cast==
*Helena Bergström - Tove Särlefalk
*Reine Brynolfsson - Claes Särlefalk
*Ewa Fröling - Liselott Waltner Peter Andersson -  Lennart Waltner
*Rikard Wolff - Vicar
*Stellan Skarsgård - Host in Norrköping
*Sverre Anker Ousdal - Opera Manager
*Ernst Günther - Opera Director
*Anneli Alhanko - Ballet Dancer gynaecologist
*Pia Johansson - Parking Attendant
*Hans Bergström - Ellys husband

==Music in the film== Ingela "Pling" Forsman, vocals by Ann Breen
*"Dancing Queen"
*"Birthday Boy", music by Denis King
*"Swan Lake. Svanarnas dans/Lebedinoe ozero, op. 20" Uti vår hage", arranged by Hugo Alfvén
*"Min soldat"
*"Summertime Blues"/"Soleil de lété"
*"La vie en rose"/"I rosenrött jag drömmer", Swedish lyrics by Roland Levin
*"En månskenspromenad", music by Thore Ehrling, lyrics by Nils Hellström
*"Waltz für Elise", music by Denis King
*"Red, Red Wine"
*"About Face", music and lyrics by Denis King
*"Welcome to La Tigre", music, lyrics and vocals by Eddy Grant Blueboy (pseudonym for Austin Lyons) Mona Lisa", Swedish lyrics by Karl-Ewert
*"Ay ay ay!"/"Aj, aj, aj", music and Spanish lyrics by Osmán Pérez Freire, Swedish lyrics by Björn Halldén, Sven-Olof Sandberg and Erik Fridén
*"DKs Tango", music by Denis King
*"The Silver Ballroom", music by Sam Fonteyn
*"The Mountains of Mourne", music by W. Houston Collisson, lyrics by Percy French
*"Here Today, Gone Tomorrow"
*"Pir atmosfär", music by Lars Liljeholm
*"The Way You Look Tonight"/"Lika söt som nu i kväll", Swedish lyrics by Sven Paddock
*"Cheek to Cheek"/"Kind mot kind", Swedish lyrics Ronald Sjögren och Lennart Reuterskiöld
*"The Winner Takes It All"

==Awards== Guldbagge Award - Best Actress in a leading role, Helena Bergström (nominated)   
*1994 - Guldbagge Award - Best Cinematography, Jens Fischer (won) 
*1994 - Montreal World Film Festival - Best Actress in a leading role, Helena Bergström  
*1995 - Istanbul International Film Festival - Jurys special award, Helena Bergström

==References==
 

==External links==
* 
* 
 

 
 

 
 
 
 
 
 
 
 
 
 


 
 