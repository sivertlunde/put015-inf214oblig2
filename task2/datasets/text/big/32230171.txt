Jaal (1986 film)
{{Infobox film
 | name = Jaal
 | image = Jaal1986.jpg
 | caption = DVD Cover
 | director = Umesh Mehra
 | producer = F.C. Mehra Parvesh C. Mehra
 | writer = 
 | dialogue =  Mandakini Sharat Saxena Vinod Mehra Tanuja
 | music = Anu Malik
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  February 14, 1986   (India) 
 | runtime = 130 min.
 | country   = India Hindi
 Rs 3.5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1986 Hindi Indian feature directed by Umesh Mehra, starring Jeetendra, Mithun Chakraborty, Rekha, Moon Moon Sen, Mandakini (actress)|Mandakini, Vinod Mehra, Sharat Saxena  and Tanuja.

==Plot==

Jaal is an action thriller, featuring Mithun Chakraborty, Moon Moon Sen, Mandakini (actress)|Mandakini, Vinod Mehra, Ranjeet, Sharat Saxena  and Tanuja.

==Summary==

Shankar Vermas father leaves the family for a courtesan, Meenabai. Now Shankar has grown into a handsome young man and he is loved by Sunita and Madhu. Shankar is hired to do spywork on Bhanu Pratap and his family by Amita. One day Shankar finds about the whereabouts of his father and his search leads him to central jail, but is devastated to know that, his father has been already dead. Now Shankar sets out to find out Meenabai, but he did not know that, Meenabai and Amita are the same person. Would Shankar forgive Meenabhai forms the Climax. 

==Cast==

*Jeetendra ...  Shashi Pratap Singh 
*Mithun Chakraborty ...  Shankar S. Verma 
*Rekha ...  Amita S. Singh / Sundari / Meenabai 
*Vinod Mehra ...  Sathpal Verma 
*Tanuja ...  Shanti  Mandakini ...  Madhu 
*Moon Moon Sen ...  Sunita P. Singh 
*Gulshan Grover ...  Balram 
*Roopesh Kumar ...  Kedar 
*Jagdeep ...  Khatarnak Khan - Shankars friend 
*Sharat Saxena ...  Hariya 
*Tej Sapru ...  Jackie 
*Amrit Pal ...  Bhanu Pratap Singh 
*Ila Arun ...  Tara 
*Birbal ...  Dalanani 
*Mohan Choti ...  Khatarnaks assistant 
*Javed Khan ...  Gundhu 
*Mahesh   
*Makhija   
*Yunus Parvez ...  Doodhwala 
*Praveena   
*Baby Shabana ...  Child artist 
*Sukhminder   
*Master Vijay ...  Child artist

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Jaal+(1986)

==External links==
*  

 
 
 
 
 