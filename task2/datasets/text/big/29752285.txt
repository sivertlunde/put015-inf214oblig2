Das Autogramm
{{Infobox film
| name           = Das Autogramm
| image          = 
| caption        = 
| director       = Peter Lilienthal
| producer       = 
| writer         = Peter Lilienthal Osvaldo Soriano
| starring       = Juan José Mosalini
| music          = 
| cinematography = Michael Ballhaus
| editing        = Siegrun Jäger
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Das Autogramm is a 1984 West German drama film directed by Peter Lilienthal. It was entered into the 34th Berlin International Film Festival.   

==Cast==
* Juan José Mosalini as Daniel Galvan, der Sänger
* Ángel Del Villar as Tony Basilio Rocha, der Boxer (as Angel Del Villar)
* Anna Larreta as Ana Gallo, Tochter
* Hanns Zischler as Leutnant Suarez (as Hans Zischler)
* Nikolaus Dutsch as Sanchez, Bahnwärter (as Nicolas Dutsch)
* Georges Géret as Dr. Gallo (as Georges Geret)
* Pierre Bernard Douby as Ignaz Zuckermann Mingo
* Vito Mata as Dicker
* Luís Lucas as Schwarzhaariger (as Luis Lucas)
* Dominique Nato as Sepulveda Marcial
* Agostinho Faleiro as Schiedsrichter
* Asdrubal Pereira as Morales
* Roman Pallares as Sänger

==References==
 

==External links==
* 

 
 
 
 
 
 
 