Made in Italy (film)
{{Infobox film
| name           =Made in Italy
| image          =Made in italy pos.jpg
| image_size     =
| caption        =
| director       =Nanni Loy
| producer       =
| writer         = Nanni Loy
| narrator       =
| starring       =
| music          =
| cinematography =
| editor       =
| distributor    =
| released       = 1965
| runtime        = 100 minutes
| country        = Italy Italian
| budget         =
}}
 1965 Italy|Italian anthology comedy film directed by Nanni Loy.

==Cast==

=== Usi e costumi ===
* Lando Buzzanca
* Aldo Giuffrè
* Walter Chiari
* Lea Massari
* Claudio Gora
* Marina Berti
* Renzo Marignano

=== Le donne ===
* Virna Lisi
* Giulio Bosetti
* Catherine Spaak
* Fabrizio Moroni
* Mario Meniconi
* Sylva Koscina
* Jean Sorel

=== Il lavoro ===
* Gino Mucci
* Milena Vukotic
* Aldo Fabrizi
* Nino Castelnuovo
* Mario Pisu
* Enzo Liberti

=== Il cittadino, lo stato, la Chiesa ===
* Nino Manfredi Carlo Pisacane
* Gigi Reder
* Carlo Taranto
* Ugo Fangareggi
* Enzo Petito

=== La famiglia ===
* Peppino De Filippo
* Tecla Scarano
* Alberto Sordi
* Rossella Falk
* Claudie Lange
* Marcella Rovena
* Anna Magnani
* Andrea Checchi
* Antonio Casagrande
* Franco Balducci

=== Emigranti ===
* Giampiero Albertini
* Aldo Bufi Landi
* Adelmo Di Fraia
* Renato Terra

==External links==
*  

 

 
 
 
 
 
 


 
 