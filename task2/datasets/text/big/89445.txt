Practical Magic
 
{{Infobox film
| name           = Practical Magic
| image          = Practical magicposter.jpg
| image_size     =
| caption        = Promotional poster
| director       = Griffin Dunne
| producer       = Denise Di Novi
| screenplay     = {{Plain list |
* Robin Swicord
* Akiva Goldsman Adam Brooks
}}
| based on       =  
| starring       = {{Plain list |
* Sandra Bullock
* Nicole Kidman
* Goran Visnjic
* Stockard Channing
* Dianne Wiest
* Aidan Quinn
}}
| music          = Alan Silvestri Andrew Dunn
| editing        = Elizabeth Kling
| studio         = Village Roadshow Pictures Di Novi Pictures
| distributor    = Warner Bros. Roadshow Entertainment  (Australia & New Zealand) 
| released       =  
| runtime        = 103 minutes 
| country        = United States
| language       = English
| budget         = United States dollar|$75 million
| gross          = United States dollar|$68,336,997 
}}
 novel of the same name by Alice Hoffman. The film was directed by Griffin Dunne and stars Sandra Bullock, Nicole Kidman, Stockard Channing, Dianne Wiest, Aidan Quinn and Goran Visnjic. The film score was composed by Alan Silvestri.

Bullock and Kidman play sisters Sally and Gillian Owens, who have always known they were different from each other. Raised by their aunts after their parents death, the sisters grew up in a household that was anything but typical—their aunts fed them chocolate cake for breakfast and taught them the uses of practical magic. But the invocation of the Owens sorcery also carries a price—some call it a curse: the men they fall in love with are doomed to an untimely death. Now adult women with very different personalities, the quiet Sally and the fiery Gillian must use all of their powers to fight the family curse and a swarm of supernatural forces that could take away all the Owens lives.

==Plot==
 

Maria Owens, a young witch, is exiled to Marias Island in Massachusetts with her unborn child for escaping her execution. When her lover does not come to rescue her, she desperately casts a spell upon herself to stop falling in love due to heartbreak, only to die soon after. The spell becomes a curse for several years. Gillian and Sally Owens, two descendants of the Owens family, are taken in by their aunts Frances and Jet after the death of their parents. Sally is the more gifted of the two while Gillians talents are more in charm and persuasion, and have been subject to ridicule during their youth. After witnessing their aunts cast a spell on a man for a woman who seems obsessed with having his love, Gillian decides to fall in love and Sally casts a true love spell to protect herself.

The sisters cast an oath to each other using blood from both of their hands and Gillian leaves for California. Sally meets and marries Michael, a local apple salesman. Years later, the two open their botanical shop Verbena and have two young daughters, Kylie and Antonia. Michael is killed after being hit by a truck. Sally and her daughters return to the Owens home to live with the aunts, and realize that the aunts cast a spell so she could fall in love. Sally decides that she and her daughters will not perform magic. As Gillian begins a relationship with Jimmy Angelov in California, Sally is devastated by her husbands death. Gillian feels that Sally needs her and drugs Jimmy to return to Massachusetts.

Gillian returns to Sally after Jimmy becomes abusive, but the sisters are kidnapped. Sally puts belladonna into Jimmys tequila, but inadvertently kills him. The sisters attempt to resurrect him using the forbidden spell from their aunts book of spells, but Jimmy attempts to kill Gillian after being revived. Sally kills him again, and the sisters bury his body in their homes garden. State investigator Gary Hallett arrives from Tucson, Arizona in search of Jimmy, who is also a killer. As Gary begins to suspect Sally, Gillian, Kylie and Antonia creates a potion to banish Gary; however, the girls realize he is the one described in Sallys true love spell, and remove the potion. Later, Sally has Gary record her testimony and sees the letter she had once written Gillian, and realizes he must have read it more times than he had let on. Unable to deny their feelings for each other, they kiss and Sally realizes that he was there because of the spell she cast years earlier.

Sally discovers that Jimmys spirit has possessed Gillians body and Gary sees Jimmys spirit emerge. Jimmy attempts to possess Gary, only to be hurt by his silver star-shaped badge and is temporarily exiled. Later, Sally tells Gary that he is there because of her spell and the feelings they have for each other are not real. Gary replies that curses are only true if one believes in them and reveals that he also wished for her, before returning to Tucson.

Jimmy possesses Gillian again and attempts to kill Sally before Frances and Jet return. Realizing she must embrace magic to save her sister, Sally asks the aid of the townswomen. The once-reluctant townswomen now decide to help the sisters and form a coven to exorcise Jimmys spirit, but Sally makes them stop when she sees that the effort might kill Gillian. Getting inside the circle, Sally and the townswomen reenact her oath with Gillian. They are able to break the Owens curse, exorcising Jimmys spirit and allowing the coven to exile him permanently. Sally receives a letter from Gary telling her that she and her sister are cleared of any suspicion of wrongdoing in Jimmys case and Gary returns to Massachusetts to be with Sally. The Owens women celebrate All Hallows Eve dressed up in witch costumes, and are embraced and welcomed by the townsfolk.

==Cast==
* Sandra Bullock as Sally Owens, a witch who becomes widowed after the Owens’ curse kills her husband.
* Nicole Kidman as Gillian Owens, sister of Sally, who grows bored with small town life and becomes the victim of an abusive relationship.
* Goran Visnjic as James Jimmy Angelov, boyfriend of Gillian, who becomes abusive and kidnaps the sisters, and is ultimately killed by them, twice.
* Stockard Channing as Aunt Frances Owens, aunt of Sally and Gillian, who tends to be more outgoing and fun-loving. She also loves to meddle in peoples love lives.
* Dianne Wiest as Aunt Bridget Jet Owens, aunt of Sally and Gillian, who tends to be more tenderhearted and quiet.
* Aidan Quinn as Investigator Gary Hallet, a lawman who investigates Sally and Gillian in the murder of Jimmy Angelov and falls in love with Sally.
* Caprice Benedetti as Maria Owens, matriarch of the Owens clan.
* Evan Rachel Wood as Kylie Owens, daughter of Sally Owens, who lives with her mom and aunts after the death of her father, Michael. She looks and acts a lot like her Aunt Gillian.
* Alexandra Artrip as Antonia Owens, daughter of Sally Owens, who lives with her mom and aunts after the death of her father, Michael. She looks a lot like her mother.
* Mark Feuerstein as Michael, husband of Sally Owens, and father of Kylie and Antonia Owens. He is a victim of the "Owens Curse", which resulted in his death.
* Lora Anne Criswell as young Gillian Owens.
* Camilla Belle as young Sally Owens.
* Peter Shaw as Jack, Sally and Gillians father, who died from the Owens curse when they were children.
* Caralyn Kozlowski as Regina, Sally and Gillians mother, who died of a broken heart after losing her husband to the Owens curse.
* Chloe Webb as Carla, a friend of Sallys, and works at her shop.
* Lucinda Jenney as Sara, one of the town women, who initially fears the Owens but later responds to Sallys call for help.
* Margo Martindale as Linda Bennett, one of town women who responds to Sallys call for help.
* Martha Gehman as Patty, one of town women who responds to Sallys call for help.

==Production==
Practical Magic was partially filmed on an artificial set in California. The films producers said the house was a big part of the depiction of the Owens culture, so they knew they had to build a house to accurately depict this. They built it on Coupeville, Washington (state)|Washington.  They brought much of the set from California and placed it inside the house, but it still took almost a year to perfect the image of the house and the interior.  The house used is owned by the Sundstrom Family and is located on San Juan Valley Road, San Juan Island. They built a replica of the outside of the house on the west side of San Juan Island so that it looked like the house was on the waterfront, but in actuality it is in the valley. They built the house in San Juan County Park but since the house was built only for this filming, it was torn down after the movie was released. Many of the small town scenes were filmed in downtown Coupeville Washington located on Whidbey Island.

According to Sandra Bullock in the DVD commentary, in the scene where the Owens women are drunk and slinging insults, the actresses actually got drunk on very bad tequila brought by Kidman. The cast also thinks that the supernatural elements of the house started to affect them; the cast and crew say that they have heard ghost noises while filming the coven scene at the end of the film. For the final scene with all of the townspeople at the Owens home, the entire population of the town where filming took place was invited to show up in costume and appear as townsfolk.

==Music==
{{Infobox album  
| Name        = Practical Magic
| Type        = Soundtrack
| Longtype    =
| Artist      = Various artists
| Cover       = Practicalmagicalbum.jpg
| Cover size  = 200
| Caption     = Photo by Suzanne Tenner
| Released    = October 6, 1998 (original pressing)
| Recorded    = August 15–16, 1998, Abbey Road Studios (Michael Nyman tracks)
| Genre       = Soundtrack, Pop music|pop, Minimal music|minimalism, orchestral
| Length      = 56:58 (Nyman pressing)
| Language    = English WEA
| Producer    = Danny Bramson, Sandra Bullock
| Chronology  = Michael Nyman
| Last album  = Strong on Oaks, Strong on the Causes of Oaks (1998)
| This album  = Practical Magic (1998)
| Next album  = Ravenous (soundtrack)|Ravenous (1999)
| Misc        = {{Extra chronology
  | Artist      = Alan Silvestri
  | Type        = soundtrack The Parent Trap (1998)
  | This album  = Practical Magic (1998) Stuart Little (1999)
  }}
}}

Composer   and  , and music that uses material related to this piece has not been used elsewhere.

Singer Stevie Nicks headlined the soundtracks published advertisements, promoting her songs "If You Ever Did Believe" and a new recording of her song "Crystal", both songs featuring Sheryl Crow on back-up vocals.

 
; Track listing
# "If You Ever Did Believe" – Stevie Nicks and Sheryl Crow This Kiss" – Faith Hill Got to Give It Up (Pt.1)" – Marvin Gaye
# "Is This Real?" – Lisahall
# "Black Eyed Dog" – Nick Drake
# "A Case of You" – Joni Mitchell
# "Nowhere and Everywhere" – Michelle Lewis
# "Always on My Mind" – Elvis Presley
# "Everywhere" – Bran Van 3000
# "Coconut (song)|Coconut" – Harry Nilsson
# "Crystal" – Stevie Nicks and Sheryl Crow
# "Practical Magic" – Alan Silvestri / "Convening the Coven" – The Michael Nyman Orchestra
# "Amas Veritas" – Alan Silvestri / "Maria Owens" – The Michael Nyman Orchestra

==Reception==

===Box office===
Practical Magic opened at #1 with $13.1 million in ticket sales. The film went on to gross $68.3 million worldwide, less than its $75 million production budget.   

===Critical reception===
Practical Magic received negative reviews from film critics.    , which assigns a weighted average score out of 1–100 reviews from film critics, calculated an average rating score of 55 based on 22 reviews. 

Owen Gleiberman of Entertainment Weekly gave Practical Magic a negative review, calling it "a witch comedy so slapdash, plodding, and muddled it seems to have had a hex put on it."  Roger Ebert of the Chicago Sun-Times said that the film "doesnt seem sure what tone to adopt, veering uncertainly from horror to laughs to romance." 

===Accolades  ===
{| class="wikitable sortable" style="text-align:center;"
! Year
! Nominated work
! Award
! Result
|-
| 1999
| Funniest Supporting Actress in a Motion Picture   Dianne Wiest American Comedy Award
|  
|- 1999
|Favorite Supporting Actress – Comedy/Romance   Stockard Channing 
| Blockbuster Entertainment Award
| 
|-
| 1999
| Favorite Actor – Comedy/Romance   Aidan Quinn 
| Blockbuster Entertainment Award
|  
|-
| 1999 This Kiss".  
| Blockbuster Entertainment Award
|  
|-
| 1999
| Favorite Supporting Actress – Comedy/Romance  Dianne Wiest
| Blockbuster Entertainment Award
|  
|-
| 1999
| Best Performance in a Feature Film – Supporting Young Actress   Camilla Belle
| Young Artist Award
|  
|-
| 1999
| Best Performance in a Feature Film – Supporting Young Actress   Evan Rachel Wood
| Young Artist Award
|  
|}

==Differences from the novel==
* Sally lives with her husband Michael in the aunts home until he dies. She then moves to New York to raise her children.
* Gillian does not show up until Sallys children are 16 and 13 (they are much younger in the film).
* Jimmy Angelov (Jimmy Hawkins in the book) is already dead in Gillians car when she arrives at Sallys. Sally never meets him and she is not responsible for his death.
* Jimmys death is accidental: Gillian begins slipping nightshade into his drinks so he will stop beating her at night. She believes it accumulated in his blood and he died spontaneously. In the film, Sally poisons him while he holds them hostage on the road.
* The aunts virtually disappear from the book until the end when they return to get rid of Jimmys ghost.
* The book delves in-depth into the lives of the teenage sisters Antonia and Kylie and their fluctuating relationship.
* Gary Hallet plays a minor role in the book. He is not, however, a manifestation of a spell by Sally to never fall in love (as in the film).
* Sally works for the school district, she does not own her own store.
* In the book, Gillian does not get possessed by Jimmy Angelov. He simply wreaks havoc on Sallys house and taunts Kylie (who has psychic powers and can see him).
* Gillian meets a local biology teacher named Ben Fry and marries him.

==Efforts for television series==
In 2004, Warner Bros. and CBS produced Sudbury, a television pilot written by Becky Hartman Edwards, starring Kim Delaney in the role played by Bullock in the film, and Jeri Ryan in the role played by Kidman.  The series, named for the Sudbury, Massachusetts location of the novel and film, was not picked up.

In 2010, Warner Bros. and ABC Family attempted to develop a reboot television series. 

==See also==
* List of ghost films

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 