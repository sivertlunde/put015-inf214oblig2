Kirai Dada
{{Infobox film
| name           = Kirai Dada
| image          =
| caption        =
| director       = A. Kodandarami Reddy
| producer       = V. Doraswamy Raju
| writer         = Satyanand  
| screenplay     = A. Kodandarami Reddy Amala Akkineni Krishnam Raju Jaya Sudha Chakravarthy
| cinematography = N. Sudhakar Reddy
| editing        = D. Venkataratnam
| studio         = V.M.C Productions
| distributor    =
| released       =  
| runtime        = 175 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Amala Akkineni, Jaal 1986.   

==Plot== Amala Akkineni) loves Vijay but Vijay loves Lata. One day Vijay comes to know the truth that Naga Raja Varmas younger brother Krishna Raja Varma (Krishnam Raju) is Malinis husband who was killed by Naga Raja Varma and Vijays father Satyam (Murali Mohan) is trapped in the case and Lata is Malinis daughter and rest of the story goes on how Vijay defeats Naga Raja Varma in the climax.

==Cast==
 
* Akkineni Nagarjuna as Vijay Amala Akkineni as Lata Khusboo as Rekha
* Krishnam Raju as Krishna Raja Varma
* Jaya Sudha as Rani Malini / Arunabai
* Rao Gopala Rao as Naga Raja Varma
* Gollapudi Maruti Rao as Damodaram
* Murali Mohan as Satyam Sudhakar as Jackie Sreedhar as Koti Rallapalli as Shivaiah
* Mada Venkateswara Rao as Chitti
* Suthi Veerabhadra Rao as Gadala Gavaraju 
* KK Sarma as Ratio shop owner Annapoorna as Parvatamma
* Varalakshmi as Lakshmi
* Anita as Janaki
* Maheeja as Chilaka
 

==Soundtrack==
{{Infobox album
| Name        = Kirai Dada 
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1989
| Recorded    = 
| Genre       = Soundtrack
| Length      = 24:49
| Label       = Saptaswar Audio
| Producer    = 
| Reviews     =
| Last album  = Agni Putrudu   (1987)
| This album  = Kirai Dada   (1987)
| Next album  = Jebu Donga   (1987)
}}

The music was composed by K. Chakravarthy|Chakravarthy. Lyrics written by Veturi Sundararama Murthy. Music released on SAPTASWAR Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Nee Buggapandu SP Balu, S. Janaki
|4:29
|- 2
|Kurise Megalu SP Balu, S. Janaki
|4:22
|- 3
|Nalanti Majnulu SP Balu
|3:50
|- 4
|1 2 3 Vateseyi SP Balu, S. Janaki
|4:15
|- 5
|Gumthalakadi SP Balu
|4:03
|- 6
|Rathrivelaku SP Balu, P. Susheela
|3:50
|-
|}

== References ==
 

==External links==
* 

 
 
 
 
 

 