The Brotherhood (2001 film)
{{Infobox film
| name           = The Brotherhood
| image          = brohoodone.jpg
| image size     = 190px
| caption        = DVD release cover 
| director       = David DeCoteau
| producer       = David DeCoteau
| writer         = Barry L. Levy Matthew Jason Walsh
| starring       = Samuel Page Josh Hammond Bradley Stryker
| music          = Jeffrey Walton
| cinematography = Howard Wexler
| editing        = J.R. Bookwalter
| studio         = Rapid Heart Pictures Regent Entertainment
| distributor    = Artist View Entertainment
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
}} homoerotic horror films.

==Plot==
Young Chris Chandler, a student at Drake University, gets a new roommate, Dan, and becomes friends with him. Meanwhile, the fraternities are pledging and a student said to have been interested in Doma Tau Omega is found dead on the campus.

Chris, who has no high opinion of fraternities, gets to know a student of psychology, Megan, who invites him and Dan to a party of Doma Tau Omega, saying that she does not want to go there alone. However, when they arrive at the party, Chris seems to be interested only in the leader of DTO, Devon Eisley, and approaches him. After waiting for Chris a long time, Megan and Dan leave the party. Chris, meanwhile, is made drunk by Devon, who then makes him a member of DTO by drinking a bit of his blood while, in turn, Chris has to drink a bit of Devons.

Shortly after that, Chris is introduced to parties in the DTO house, where he and Devon drink blood from a girl called Sandy in a way reminiscent of sexual intercourse. Dan is unsettled by the change in Chris since joining the fraternity, and breaks into the DTO house. Investigating the book of DTO, Dan is shocked to find photos of Devon throughout the decades of the fraternitys history, completely unchanged. He persuades Megan to go with him to Chris´ room, which they find totally ravaged. They decide to look for Chris at the fraternity house.
 
At the DTO house, Devon tells Chris that he lives eternally as long as he transforms his soul into a new body every century, and that this time, Chris would be the one whose body is used. The student found dead on campus was made to commit suicide, as he was afraid to participate in the ceremony and threatened to go public. Chris tries to flee but is knocked out by the other DTO members, Barry and Jordan. Dan and Megan try to enter the fraternity house, but their way is blocked by DTO member Mikhail who threatens them with an axe. Dan grabs the needle used by the fraternity members to get blood from their victims and thrusts in into Mikhails neck, killing him.

In the house, Dan and Megan meet Berry, Jordan, Devon and Chris, who are already beginning the ceremony. Megan reveals herself as a decoy who has worked for Devon over the last 70 years and was tasked with luring Chris. She threatens Dan with the axe, saying that she will kill him if Chris tries to resist the ritual. Chris stops fighting and kneels down in front of Devon. While everyone is concentrating on Chris, Dan suddenly breaks free of Megan. He takes the axe, and attacks Devon with it. Devon, the other fraternity members and Megan all die, leaving Chris and Dan standing amongst the carnage. Dan asks why Chris did not die, and Chris answers, "I told you I would never join a fraternity." The two leave the house.

==Cast==
*Samuel Page as Chris Chandler
*Josh Hammond as Dan
*Bradley Stryker as Devon Eisley
*Elizabeth Bruderman as Megan
*Forrest Cochran as Barry
*Michael Lutz as Jordan
*Donnie Eichar as Mikhail
*Christopher Cullen as Frat Slob #1
*Brandon Beemer as Frat Slob #2
*Brian Bianchini as Frat Slob #3
*Chloe Cross as Sandy
*Rebekah Ryan as Female Partygoer

==Sequels==
*   (2001)
*   (2003)
*   (2005)
*   (2009)
*   (2009)

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 