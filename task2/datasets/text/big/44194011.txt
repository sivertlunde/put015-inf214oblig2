La hija de Moctezuma
{{Infobox film
| name           = La hija de Moctezuma
| image          = La_hija_de_Moctezuma_poster.jpg
| alt            =
| caption        = Theatrical release poster
| film name      = 
| director       = Iván Lipkies
| producer       = Ivette Lipkies
| writer         = 
| screenplay     = {{Plainlist|
*Ivette Lipkies
*Iván Lipkies
*Raúl S. Figueroa
*M. E. Velasco}}
| story          = 
| based on       =  
| starring       = {{Plainlist|
*María Elena Velasco
*Eduardo Manzano
*Rafael Inclán
*Raquel Garza
*Ernesto Pape}}
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Vlady Realizadores
| distributor    = Star Castle Distribution
| released       =  
| runtime        = 100 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = $3.8 million   
}}
La hija de Moctezuma ("Moctezumas Daughter") is a 2014 Mexican comedy film directed by Iván Lipkies. It stars María Elena Velasco (as La India María), Eduardo Manzano, Rafael Inclán, Raquel Garza, and Ernesto Pape.

==Synopsis== Moctezuma Xocoyotzin, La India María must find Tezcatlipocas magical black mirror in order to prevent the destruction of Mexico. A Spanish archaeologist (Alonso), a tricky treasure hunter (Bianchi), and a greedy governor (Brígida Troncoso) all find out about the existence of the black mirror and embark on a frenetic chase to obtain it.

==Cast==
* María Elena Velasco as La India María
* Eduardo Manzano as Xocoyote Moctezuma
* Raquel Garza as Brígida Troncoso
* Federico Villa
* Ernesto Pape as Alonso
* Irma Dorantes
* Armando Silvestre
* Alfredo Sevilla

==Production==

===Casting===
Rafael Inclán had to learn Náhuatl to play Moctezuma II|Moctezuma.   

Actress and singer Irma Dorantes wanted to participate in the film and was given a small part as a secretary.   

==Release==
The film premiered in Mexico on 9 October 2014 with 370 copies distributed by Star Castle Distribution.   

==Critical response==
La hija de Moctezuma received good reviews from critics. Lucero Calderón of  , why cant they give an opportunity to a well-made film that appeals to the nostalgia of popular cinema and that was done through the efforts and sacrifice of many people."   

==References==
 

==External links==
*  
*  

 
 