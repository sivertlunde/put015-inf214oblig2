Back Then (film)
{{Infobox film
| name = Back Then
| image =
| image_size =
| caption = Rolf Hansen
| producer = Walter Bolz   Bert Roth   Peter Groll   Rolf Hansen
| narrator =
| starring = Zarah Leander   Hans Stüwe   Rossano Brazzi
| music = Ralph Benatzky   Lothar Brühne  
| cinematography = Franz Weihmayr  
| editing =  Anna Höllering       UFA 
| distributor = Deutsche Filmvertriebs 
| released =3 March 1943
| runtime = 94 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Rolf Hansen and starring Zarah Leander, Hans Stüwe and Rossano Brazzi.  The films sets were designed by Walter Haag.

It was made at the Babelsberg Studio, by Universum Film AG, Germanys largest film company. It was Leanders final film of the Nazi era, as she returned to Sweden shortlly afterwards. This was a blow for the German film industry, as she was the most popular and highest-paid star. Leanders next film was not for another seven years, when she made a comeback in Gabriela (1950 film)|Gabriela (1950).

==Cast==
*    Zarah Leander as Vera Meiners/Gloria OConnor 
* Hans Stüwe as Jan Meiners, Reichsanwalt 
* Rossano Brazzi as Pablo, Radrennfahrer und Clown 
* Jutta von Alpen as Brigitte Meiners 
* Hilde Körber as Frau Gaspard, Mutter des operierten Kindes 
* Elisabeth Markus as Dr. Gloria OConnor 
* Hermann Bräuer as Batejo, Manager 
* Hans Brausewetter as Corbeau, Friseur 
* Otto Graf as Dr. Lugeon, Chirurg 
* Karl Haubenreißer as Mendoza 
* Emil Heß as Alvarez, Veras Verteidiger 
* Herbert Hübner as Professor Rigaud, Klinikleiter  
* Victor Janson as Kabarettdirektor  Karl Martell as Frank Douglas, Versicherungsagent 
* Giacomo Moschini as Fernandez, Marktbudenbesitzer 
* Alfred Schieske as Bassist 
* Erich Ziegel as Sanitätsrat Petersen 
* Curt Ackermann as Etagenkellner im Hotel 
* Olaf Bach as Hoteldiener Pedro Bicerkvez
* Hellmuth Bergmann as Gefängniswärter 
* Peter Busse as Polizeikommissar 
* Vera Complojer as Frau Januschko  
* Karl Etlinger as Journalist am Tatort 
* Hanne Fey as Dienstmädchen bei Meiners 
* Hugo Flink as Diener bei Professor Rigaud 
* Lothar Geist as Zeitungsverkäufer 
* Fritz Gerlach  as Zeitungsverkäufer  
* Fred Goebel as Journalist   Walter Gross as Kellner in der Hotelbar   Knut Hartwig as Herr Januschko 
* Friedrich Honna as Billardspielender Kellner in der Espresso-Bar  
* Ernst Karchow as Vorsitzender der Einwanderungsbehörde  
* Paul Klinger as Voice of Pablo (voice) 
* Karin Luesebrink as Junge Krankenschwester  
* Kurt Mikulski as Journalist 
* Leo Peukert as Rasierter Kunde beim Friseur  Hermann Pfeiffer as Nachtportier im Hotel Delfino  
* Gustav Püttjer as Mann auf der Bahnstation nach dem Zugunglück 
* Erik Radolf as Staatsanwalt  
* Ernst Rotmund as Leiter der Kapelle in der Hotelbar 
* Hermann Sattler as Mitglied der Einwanderungsbehörde 
* Just Scheu as Polizeifotograf Vigo  
* Lili Schoenborn-Anspach as Emigrantenfrau in der Quarantäne  
* Hans Joachim Schölermann as Bahnstationsbeamter nach dem Zugunglück  
* Margarete Schön as Gast beim Abendempfang  
* Walter Steinweg as Journalist 
* Theodor Thony as Protokollführer des Staatsanwalts  
* Eva Tinschmann as Arzthelferin Fräulein Gonzales  
* Margarethe von Ledebur as Oberschwester  
* Agnes Windeck as Gast beim Abendempfang

== References ==
 

== Bibliography ==
* OBrien, Mary-Elizabeth. Nazi Cinema as Enchantment: The Politics of Entertainment in the Third Reich. Camden House, 2006.

== External links ==
*  

 
 
 
 
 
 
 

 