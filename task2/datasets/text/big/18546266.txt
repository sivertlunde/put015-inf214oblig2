Night Must Fall (1964 film)
 
 
{{Infobox film
| name           = Night Must Fall
| image size     = 
| image	=	Night Must Fall FilmPoster.jpeg
| caption        = 
| director       = Karel Reisz
| producer       = Albert Finney  Karel Reisz
| writer         = Emlyn Williams (play)  Clive Exton
| narrator       = 
| starring       = Albert Finney Mona Washbourne
| music          = Ron Grainer
| cinematography = Freddie Francis
| editing        = Phillip Barnikel Fergus McDonell
| distributor    = MGM
| released       =  
| runtime        = 101 minutes (US)
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} remake of film of 1935 play by Emlyn Williams. It was directed by Karel Reisz from a script by Clive Exton and starred Albert Finney, Mona Washbourne, and Susan Hampshire, but was not as successful as the original film. The film was entered into the 14th Berlin International Film Festival.   

==Plot==
Danny is a Welsh hotel bellboy who commits an axe murder near the home of Mrs. Bramson, a well-to-do widow. Danny disposes of the body in a nearby lake and charms Mrs. Bramson and her maid Dora into allowing him to stay with them. At Mrs. Bramsons home, Danny plays psychological games with Mrs. Bramson while seducing her daughter Olivia. Meanwhile, alone in his room, Danny engages in strange rituals with the severed heads of his victims, which he keeps in a black hatbox. But the police have uncovered the axe and the headless corpse from the lake, and the authorities begin closing in on Danny, whose psychopathic tendencies are beginning to manifest themselves at Mrs. Bramsons home. 

==Cast==
*Albert Finney as Danny
*Mona Washbourne as Mrs. Bramson
*Susan Hampshire as Olivia
*Sheila Hancock as Dora
*Michael Medwin as Derek
*Joe Gladwin as Dodge
*Martin Wyldeck as Inspector Willett
*John Gill as Foster

==Reception==

Paul Mavis, of DVDTalk.com, reviewing the 2014 Warner Archive Collection DVD release, wrote, "Angry Young Psychopath. Director Karl Reisz and Albert Finney transform Emlyn Williams old chestnut into a startlingly off-putting psychological shocker, making the charmingly jumped-up killer terrifyingly opaque, while the victims are all too readable. An intense, (mostly) sustained vision of inexplicable murderous frustration, with Albert Finney giving one of his greatest performances." 

==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 