Aleesha (film)
 
  Konkani film released in 2004.

==Synopsis==
The film is about a mine-owner family and its interaction with the residents of the mining belt of Goa.

The bountiful nature and the pristine beauty of Goa is a joy for Goans and nature lovers all over the globe. Known all over the world as a popular tourist destination, Goa remains a dream holiday destination for everybody. But little known is the fact that the mining industry is an important part of Goa’s economy. It could be the story of any of the mining families in Goa, with a young scion taking learning the ropes the newest member of the family, a pretty, educated daughter-in-law, Aleesha.

Aleesha is an artist photographer who is an ardent lover of nature. Married in the family of industrialists, she finds herself in a conflict of interests. Her struggle to set right the blatant violation and flouting of rules brings her in direct confrontation with her familys business interests. Moreover, her hard stance of exposing the facts does not go down well with the involved parties. In the film, Aleesha depicts her persuasiveness and the manner in which she manages to prevail over her husband and in-laws who themselves are mine owners. Aleeshas message is that of co-existence of an eco-friendly industry with nature being the need of the hour.

Priyanka Bidye plays the lead role well, a bit dramatised for the effect. It is Prashanti Talpankar, lecturer in Dnyanaprasarak Mandals College on the outskirts of Mhapsa and a writer in her own rights, who grabs the attention in her role as "Philo", the wife of a deceased truck driver in the mining belt. The film ably depicts the neglect of the environment by industrialists, changing trend in mining, with afforestation and social concern increasing in the mining companies. The role in real life that is played by qualified and professional Environmental Managers working under an ISO 14001 regime is shown in reel life as led by the daughter-in-law. Prince Jacob and Lorna have also made guest appearances, where Lorna sings Portun Aikat Mozo Tavo and Prince Jacob as a government officer / inspector.

==Cast and crew==
* Cast: Priyanka Bidye, Tapan acharya, Dr. Vijay Thali, Prashanti Talpankar, Rajeev Hede, Dr. Ajay Vaidya, Anil Raikar, Keshav Nadkarni, Neema Kamat Lorna
* Director: Rajendra Talak
* Producer: Rajendra Talak (Rajendra Talak Creations)
* Director of Photography: Debu Deodhar

==Awards== National Award for Best Feature Film in Konkani at the 52nd National Film Awards. Aleesha was the first film to win this award, although Nirmon had earlier won a Certificate of Merit for Regional Languages in 1965.

==Music==
The soundtrack of this film was not released. Only the song Kali Kali sung by Devaki Pandit and its instrumental version were released in a combination album with Rajendra Talaks production Daryachea Deger.

==Notes and references==
*  
*  

 
 
 
 