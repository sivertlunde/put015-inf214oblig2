Ponnaranjanam
{{Infobox film 
| name           = Ponnaranjanam
| image          =
| caption        =
| director       = Babu Narayanan
| producer       = Hameed
| writer         = Purushan Alappuzha Babu Narayanan (dialogues)
| screenplay     = Purushan Alappuzha Mahesh Usha Usha Innocent Innocent Adoor Bhavani
| music          = Kozhikode Yesudas
| cinematography = V Aravindakshan
| editing        = G Murali
| studio         = Gemi Movies
| distributor    = Gemi Movies
| released       =  
| country        = India Malayalam
}}
 1990 Cinema Indian Malayalam Malayalam film, Innocent and Adoor Bhavani in lead roles. The film had musical score by Kozhikode Yesudas.   

==Cast== Mahesh
*Usha Usha
*Innocent Innocent
*Adoor Bhavani
*Mala Aravindan
*Mamukkoya

==Soundtrack==
The music was composed by Kozhikode Yesudas and lyrics was written by RK Damodaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Pennil pennaay || K. J. Yesudas || RK Damodaran || 
|-
| 2 || Ponnaranjaanam   || KS Chithra || RK Damodaran || 
|-
| 3 || Ponnaranjaanam   || K. J. Yesudas || RK Damodaran || 
|-
| 4 || Theeram prakrithi || KS Chithra || RK Damodaran || 
|}

==References==
 

==External links==
*  

 
 
 

 