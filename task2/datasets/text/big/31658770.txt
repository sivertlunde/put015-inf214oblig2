Melba (film)
 
 
{{Infobox film
| name           = Melba
| image          = Melba poster.jpg
| image size     =
| alt            =
| caption        = theatrical poster
| director       = Lewis Milestone
| producer       = Sam Spiegel
| screenplay     = Harry Kurnitz
| story          = Harry Kurnitz John McCallum
| music          = Muir Mathieson Mischa Spoliansky
| cinematography = Arthur Ibbetson Edward Scaife
| editing        = Bill Lewthwaite
| studio         = Horizon Pictures
| distributor    = United Artists
| released       =  
| runtime        = 112 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 musical biopic drama film of the life of Australian-born soprano Nellie Melba, written by Harry Kurnitz and directed by Lewis Milestone for Horizon Pictures,   marking the film debut of the Metropolitan Operas Patrice Munsel.  {{cite news|url=http://news.google.com/newspapers?id=d75TAAAAIBAJ&sjid=kYkDAAAAIBAJ&pg=1851,1643502&dq=melba+lewis-milestone&hl=en
|title=Munsel is Melba
|date=4 December 1953
|work=Val-dOr|Val-dOr Star
|publisher=Google News Archive
|pages=8
|accessdate=4 May 2011}}  {{cite news 
| url=http://news.google.com/newspapers?id=8Hs_AAAAIBAJ&sjid=AFUMAAAAIBAJ&pg=592,3473079&dq=lewis-milestone&hl=en 
| title=Film at Foster Traces Life of Brilliant Star 
| work=Youngstown Vindicator 
| date=26 May 1954 
| accessdate=4 May 2011 
| pages=14 
| publisher=Google News Archive}} 

==Background== London with the script in-hand to acquire film rights for her story from Melbas estate. {{cite news
|url=http://news.google.com/newspapers?id=OEQxAAAAIBAJ&sjid=_A8EAAAAIBAJ&pg=7198,2844367&dq=melba+patrice-munsel&hl=en
|title=Annette Kellerman sees life story set
|last=Louella O. Parsons
|date=22 January 1952
|work=Milwaukee Sentinel
|publisher=Google News Archive
|pages=3
|accessdate=4 May 2011}}   In May 1952, Spiegel reported that the film was to be shot on locations in Australia and Britain with Patrice Munsel cast as Nellie Melba.  Upon release of the information, representatives of the J. Arthur Rank organization expressed surprise, as they had themselves been planning a film about Melba.  They had hoped to get Marjorie Lawrence for their own film and, when unable to do so, had even considered using actual recordings Melbas voice dubbed over that of an actress. They decided to put their plans on hold until they could find a suitable singer/actress for the lead role. {{cite news
|url=http://news.google.com/newspapers?id=n65VAAAAIBAJ&sjid=GcQDAAAAIBAJ&pg=7182,521622&dq=melba+patrice-munsel&hl=en
|title=A New York Soprano will be Filmed in the Mantle of Melba
|date=5 May 1952
|work=The Age
|publisher=Google News Archive
|pages=2
|accessdate=4 May 2011}}   One of Speigels early choices for director was Charles Vidor. In July 1952, Speigel flew to Paris to speak with him. {{cite news
|url=http://news.google.com/newspapers?id=YnxWAAAAIBAJ&sjid=7-UDAAAAIBAJ&pg=7050,6974185&dq=melba+patrice-munsel&hl=en
|title=Bearded Scot to be Monarch
|date=29 July 1952
|work=Spokesman-Review
|publisher=Google News Archive
|accessdate=4 May 2011}}   Speigel then hired Edmond Goulding as director.  Pre-production for the film was being done in Paris, France|Paris, and Patrice Munsel and her husband arranged a second honeymoon in that city.  When problems arose with Goulding, Speigel fired him and hired Lewis Milestone to direct. {{cite book
|last=Natasha Fraser-Cavassoni
|title=Sam Spiegel
|publisher=Simon and Schuster
|year=2003
|pages=141–142
|isbn=0-684-83619-X
|url=http://books.google.com/books?id=TtYex8ky_MEC&pg=PA141&lpg=PA141&dq=Melba,+Lewis+Milestone&source=bl&ots=F1K8EUEKMH&sig=iHz2BH8QyR_jYL8XOKnq0c6YVCk&hl=en&ei=mPTATbqfN4e4sQOWx-TgBw&sa=X&oi=book_result&ct=result&resnum=4&ved=0CCgQ6AEwAzgK#v=onepage&q=Eddie%20Goulding&f=false
|accessdate=4 May 2011}}   The change of directors disheartened Munsel, as she had been looking forward to working with Goulding, and she found Milestone to be "more traffic cop than auteur". She had also anticipated a script that would allow her to play Melba as the "gutsy, difficult, strong-minded" person she really was, and was disappointed that the Kurnitz script "was essentially a plot-less, soft-centered love story built around a long string of opera sequences.". {{cite news
|url=http://www.operanews.com/Opera_News_Magazine/2011/2/Departments/Stars_of_Stage_and_Screen__Program_Notes.html
|title=Stars of Stage and Screen: Opera on Stage and on Film
|last=Brian Kellow
|work=Opera News Wiltshire plains.  While Munsels voice impressed critics, she did not resemble Dame Melba, with the biggest contrast being her American accent. When asked of her role, Munsel stated   "We are not attempting to re-create Melba.  We are simply paying tribute to her as a great singer in a way that will bring popular entertainment to the masses." {{cite news
|url=http://news.google.com/newspapers?id=R_RjAAAAIBAJ&sjid=GrADAAAAIBAJ&pg=4086,1677000&dq=milestone+to+direct+melba&hl=en
|title=Melba with an accent
|last=staff correspondent
|date=14 September 1952
|work=Sydney Morning Herald
|pages=3
|accessdate=4 May 2011}}  

Theodore Bikel wrote in his autobiography that because of Milestones background, this was a felicitous choice by Sam Spiegel, as Milestone was an award-winning director with a great touch.  There was however, a growing frustration between Spiegel and Milestone, both on set and off.  After months of filming with growing tensions, and three days before the last scene was to be shot, a confrontation between the two caused Milestone to walk of the set and not return. {{cite book
|last=Theodore Bikel
|title=Theo: the autobiography of Theodore Bikel
|publisher=Univ of Wisconsin Press
|year=2002
|pages=121–122
|isbn=0-299-18284-3
|url=http://books.google.com/books?id=BYdxfm4zSyoC&pg=PA121&dq=%22Lewis+Milestone%22,+%22Sam+Spiegel%22&hl=en&ei=r-LATbvIGpL6swOQ29jgBw&sa=X&oi=book_result&ct=result&resnum=2&ved=0CDcQ6AEwAQ#v=onepage&q=%22Lewis%20Milestone%22%2C%20%22Sam%20Spiegel%22&f=false
|accessdate=4 May 2011}} 

==Plot== Paris to receive vocal training, meets a new suitor, and debuts her talent in Brussels.  As her success grows, her former suitor from Australia arrives in Monte Carlo, convinces her to marry him, but then finds himself placed in the position of being "Mr. Melba".  When he leaves her to return to Australia, Melba remains in Europe to continue singing.

==Partial cast==
* Patrice Munsel as Nellie Melba 
* Robert Morley as Oscar Hammerstein I  John McCallum as Charles Armstrong
* John Justin as Eric Walton
* Alec Clunes as Cesar Carlton
* Martita Hunt as Blanche Marchesi 
* Sybil Thorndike as Queen Victoria 
* Joseph Tomelty as Thomas Mitchell
* Beatrice Varley as Aunt Catherine 
* Theodore Bikel as Paul Brotha

==Critical response==
The New York Times reported that the film was essentially one song after another and, listing the numerous works that were shared in the film, wrote "Even though jumbled together in an excessive, haphazard way, this massive mélange of mighty music is the meager salvation of the film."  The reviewer, Bosley Crowther, offered that, while the task of portraying the life of Nellie Melba was laudable as a concept, the "storybook tale of an opera singer that Harry Kurnitz has put together is a mere offense to the taste and credulity of an average numskull, and it is assembled and played with little more sense."  They disapproved of the plot which portrayed Melba as "silly and vapid", whose character then "comes out flat and gauche".  The offered the storyline was "sticky and banal" and "overloaded with such obvious twaddle about ambition and success." {{cite news
|url=http://movies.nytimes.com/movie/review?res=9B04E0DD143EE43ABC4D51DFB0668388649EDE
|title=review: Melba (1953), 
|last=Bosley Crowther
|date=25 June 1953
|work=The New York Times|accessdate=4 May 2011}} 

Conversely, Spokesman-Review felt the film was a "worthy climax to her   spectacular opera and concert career." They wrote that of the many films that attempted to adapt opera to the film media, this one is the best. They acknowledged that while the film was "broadly sentimental", it was appealing.  And that even though the storyline did not factually follow the life of Nellie Melba, "it suggests its glory and glamour and completely enthralls the audience." They write that "Munsel is magnificent", and that even while not being photogenic in the Australian shot scenes depicting Melbas life as a cowgirl, once Melba is brought to Europe, "she is most attractive." Listing the many songs sung by Munsel, they made note of the attending audience applauding each, and wrote "Her singing is magnificent, and she is an actress of sufficient poise to carry complete conviction."  The praised the supporting cast and Lewis Milestones craftmanship as a director. {{cite news
|url=http://news.google.com/newspapers?id=bS9WAAAAIBAJ&sjid=JeYDAAAAIBAJ&pg=7009,2570401&dq=melba+lewis-milestone&hl=en
|title=Patrice Superb in Lavish Film
|last=Margaret Bean
|date=16 September 1953
|work=Spokesman-Review
|publisher=Google News Archive
|pages=14
|accessdate=4 May 2011}} 

==Soundtrack== Robert le Diable, composed by Giacomo Meyerbeer
* "Comin Through the Rye", words by Robert Burns Ave Maria", composed by Johann Sebastian Bach and Charles Gounod
* "O luce di quest anima", from Linda di Chamounix, composed by Gaetano Donizetti, libretto by Gaetano Rossi
* "List of compositions by Charles Gounod|Serenade", composed by Charles Gounod
* "Caro nome", from Rigoletto, composed by Giuseppe Verdi, libretto by Francesco Maria Piave The Mad Scene", from Lucia di Lammermoor, composed by Gaetano Donizetti, libretto by Salvadore Cammarano Chacun le sait", from La fille du régiment, composed by Gaetano Donizetti, libretto by Jules-Henri Vernoy de Saint-Georges and Jean-François Bayard
* "Una voce poco fa", from Il barbiere di Siviglia, composed by Gioachino Rossini, libretto by Cesare Sterbini O soave fanciulla", from La Bohème, composed by Giacomo Puccini, libretto by Giuseppe Giacosa and Luigi Illica Voi che sapete", from Le nozze di Figaro, composed by Wolfgang Amadeus Mozart, libretto by Lorenzo da Ponte
* "Aria", from Faust (opera)|Faust, composed by Charles Gounod, libretto by Jules Barbier and Michel Carré
* "Vissi darte", from Tosca, composed by Giacomo Puccini, libretto by Giuseppe Giacosa and Luigi Illica
* "Brindisi", from La traviata, composed by Giuseppe Verdi, libretto by Francesco Maria Piave O pur bonheur", from Roméo et Juliette, composed by Charles Gounod, libretto by Jules Barbier and Michel Carré
* "On Wings of Song", composed by Felix Mendelssohn-Bartholdy
* "Dreamtime", composed by Mischa Spoliansky, lyrics by Norman Newell
* "Is This the Beginning of Love?", written by Mischa Spoliansky

==References==
 

==External links==
*  

 

 
 
 

 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 