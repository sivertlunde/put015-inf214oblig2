Summer '04
{{Infobox film
| name           = Summer 04
| image          = Summer 04.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Stefan Krohmer
| producer       = Katrin Schlösser
| writer         = Daniel Nocke
| starring       = {{Plainlist|
* Martina Gedeck
* Robert Seeliger
* Svea Lohde
}}
| music          = Ellen McIlwaine
| cinematography = Patrick Orth
| editing        = Gisela Zick
| studio         = Ö-Filmproduktion
| distributor    = {{Plainlist|
* Alamode Film  
* The Cinema Guild   Koch Lorber  
}}
| released       =  
| runtime        = 97 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = $632,017  
}}
Summer 04 ( ) is a 2006 German drama film directed by Stefan Krohmer and starring Martina Gedeck, Robert Seeliger, and Svea Lohde.  Written by Daniel Nocke, the film is about a middle-aged woman coming to terms with her sons sexual awakening and who ends up involved in some extramarital exploration of her own. The film was presented at the 2006 Cannes Film Festival, the 2006 Toronto International Film Festival, and the 2007 Berlin International Film Festival.    

==Plot==
Miriam Franz (Martina Gedeck) has just turned forty and is starting a seaside vacation with her husband André (Peter Davor) and fifteen-year-old son Nils (Lucas Kotaranin) on the Schlei, a narrow inlet of the Baltic Sea in Schleswig-Holstein in northern Germany. André has given Niels permission to bring his twelve-year-old girlfriend Livia (Svea Lohde) along for the trip. Miriam is concerned they may take their relationship to a physical level, but André seems unconcerned.

One day, Nils and Livia go sailing, but Nils returns alone saying that Livia is still sailing with a man they met, Bill Ginger (Robert Seeliger), an American of German heritage. Bill brings Livia home and seems quite taken with her, and vice versa. Miriam is concerned by Bills flirtation with the underage girl, finding it "a little weird"; André is unimpressed with Bills fawning compliments. Soon Livia is spending all her time with Bill, while Nils sits at home watching war documentaries.

When Livia fails to come home one night, Miriam, feeling responsible for her summer guest, drives to Bills home to confront them about their inappropriate behavior. When she arrives, Bill casually tells Miriam that Livia went for a walk after an argument. While they wait, Bill admits how much hes enjoyed his time and conversations with Livia, after his years in America where all talk is about "money and stupidity." He assures Miriam that he knows "when to stop" with the young girl. When Livia finally returns, Miriam drives her home and learns that Livia was disappointed that they didnt have sex.

The next day, Miriam, André, and Livia join Bill for a day of sailing; Miriam and Bill take the C55 catamaran, while André and Livia take the sailboat Bénéteou. During a break from sailing, Miriam expresses interest in Bills sex life, and as they relax on a beach she starts to seduce him, but he resists her advances. The next day, while Nils and Livia are out sailing, Miriam drives to Bills house and they have sex. Afterwards, they see Livia waiting downstairs—apparently having heard their lovemaking. 

In the coming days, the family spends time at a fair and visits the Landesmuseum at the Schloß Gottorf to visit the Bog People Exhibit. The relationship between André and Nils continues to grow strained. Mariam returns to Bills house and they have sex again. Afterwards, Bill breaks off their relationship, saying he is in love with Livia. The next day, Miriam and Livia are out sailing when Livia is struck in the head by the main sail boom. Livia says she is okay, and they continue sailing. Miriam informs Livia she can no longer see Bill. When Livia says shes tired and asks to turn back, Miriam dismisses her, thinking she just wants to meet Bill. Later, Livia says she feels sick and collapses, hitting her head again. 

Miriam returns to shore and brings Livia to the hospital, where she dies. At home, Nils asks his father why he lied about the timing of the incident, noting if Miriam had turned back earlier, Livia would still be alive. After Nils leaves, Miriam tells André that their son is right—that when Livia was initially struck, they were still fairly close to shore. That night, Miriam packs her things and André drops her off at Bills house. Initially, Bill does not want to see her, but later they talk about Livia. After Miriam confesses there is no one shed rather be with, they have sex.

A few years later, Miriam and Bill, who are now a couple, visit with Livias mother and her new suitor in Germany. Miriam and Bill left Germany in 2004 and have been living in the United States and traveling. Livias mother reads them a letter her daughter wrote to a friend in August 2004. In the letter, Livia mentions how Miriam and her husband do not belong together, and she thinks Miriam and Bill would make a good match. She thinks she can help bring them together, to be "the happiest couple in the world." Livias mother asks them if they are now indeed happy, and Miriam says, "Yes, were very happy."   

==Cast==
* Martina Gedeck as Miriam Franz
* Robert Seeliger as Bill Ginger
* Svea Lohde as Livia
* Peter Davor as André
* Lucas Kotaranin as Nils
* Nicole Marischka as Grietje
* Gábor Altorjay as Daniel
* Michael Benthin as Arzt 

==Production==
===Filming locations===
* Schleswig, Schleswig-Holstein, Germany
* Schlei, Schleswig-Holstein, Germany 

==Reception==
===Critical response===
Upon its release, Summer 04 received generally positive reviews in the United States and Europe. In his review in The New York Times, Matt Zoller Seitz wrote, "Mr. Krohmers film is distinguished by very long takes, ominous cutaways to undulating river reeds, near-somnambulist underplaying by a first-rate cast and nail-on-the-head dialogue about materialism, hypocrisy, morality and mortality."   

In his review in the Los Angeles Times, Kenneth Turan wrote, "Director Krohmer creates a fine sense of erotic tension, and actress Gedeck provides more than enough emotional resonance. This is one tricky film, but it stays with you nevertheless." Turnan concluded:
  }}

In her review in The Village Voice, Michelle Orange wrote, "Elements of LAvventura, Swimming Pool, and even A Place in the Sun materialize in the films sophisticated layering of theme and counter-theme, and Gedeck in particular successfully invests her lightly defiant sensuality in this thorough and thoroughly engaging investigation of age (and entitlement) before beauty."   

In his review in Salon.com, Andrew OHehir applauded Martina Gedecks "tremendous" performance and called the film a "well-crafted and deceptively leisurely film, with a heart of ice."   

In his review in the New York Observer, Andrew Sarris called Summer 04 a "marvelous German film" with a "warmly stirring romantic maelstrom of five multigenerational libidos let loose at a lakeside vacation site." Sarris concluded:
  }}

In his review in Slant Magazine, Ed Gonzalez gave the film three of four stars and wrote that the film "exudes the fleeting quality of a summer breeze, exploring with unpretentious candor—and very little skin—how a young girls sexual agency rebukes an older generations notions of right and wrong."   

The film received an 80% positive rating from top film critics based on 20 reviews.   

===Box office===
Summer 04 did not do well at the box office, earning only $632,017 in total worldwide gross sales. In the United States, the film was released in only two theaters, generating $20,474 in gross domestic revenue.   

==Awards and nominations==
* 2006 Cannes Film Festival Film Presented 
* 2006 Toronto International Film Festival Film Presented
* 2007 Berlin International Film Festival Film Presented 
* 2007 Karlovy Vary International Film Festival Film Presented 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 