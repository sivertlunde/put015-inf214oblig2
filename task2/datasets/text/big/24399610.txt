Long Weekend (1978 film)
{{Infobox film
| name           = Long Weekend
| image          = Long-Weekend.jpg
| caption        =
| director       = Colin Eggleston
| producer       = Colin Eggleston Richard Brennan
| writer         = Everett De Roche John Hargreaves Briony Behets
| music          = Michael Carlos
| cinematography = Vincent Monton Brian Kavanagh
| studio    = Dungog Films Australian Film Commission Victorian Film Corporation
| distributor = Roadshow Films
| released       =  
| runtime        = 92 minutes 
| country        = Australia
| language       = English
| budget = AU$425,000  David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p247-248  or $270,000 
}}  horror film John Hargreaves and Briony Behets.

==Plot==
The story concerns a couple, Peter (Hargreaves) and Marcia (Behets), who, along with their dog, go for a weekend camping trip. The pair show incredible disrespect for nature, especially Peter, such as polluting, killing a dugong, throwing lit cigarette butts in dry bush, and spraying insecticide, among other transgressions.  As tensions between the couple escalate, nature is not pleased with their environmental wrongdoing and starts to strike back, first by an eagle and possum attacking Peter, and then through more insidious means... 

==Cast== John Hargreaves - Peter
* Briony Behets - Marcia Mike McEwen - Truck Driver
* Roy Day -	Old Man Michael Aitkens - Bartender
* Sue Kiss von Soly - City girl

==Production==
The script was the first feature script written by Everett De Roche, an experienced Australian TV writer. He was inspired by a trip he took on an Easter weekend to an isolated beach in New South Wales:
 I started LW as a way to avoid the TV-cop-show doldrums while still convincing myself I was “working”. LW was a unique project because I began with no outline, no notes or research, very little idea as to where the story was going, and absolutely zero knowledge of screenplays. I simply started at page 1, scene 1, and made it up as I went. I had only a vague plan to write a kind of environmental horror story. My premise was that Mother Earth has her own auto-immune system, so when humans start behaving like cancer cells, She attacks. I also wanted to avoid a Jaws (film)|JAWS-like critter film. I wanted the LW beasties to all be benign-looking and not overtly aggressive.   accessed 19 October 2012  
De Roche wrote the script in ten days.   accessed 26 October 2012  He showed it to Colin Eggleston, who had worked with him at Crawfords, and Eggleston decided to make the movie.  Funds were obtained from Film Victoria and the Australian Film Commission.

Shooting took place in March–April 1977 in Melbourne and Phillip Island. The ending was originally different according to De Roche:
  I wrote an enormously complicated sequence for near the end where the animals give Peter a second chance. They want him to wise up, and he is at the point of doing so when he hears a truck in the distance. He dashes off to the highway, and the animals decide there is no hope. Poetically, they leave it to another man to kill him.  
However his scene was too difficult to shoot because it involved animals and was cut. 

==Release==
The movie was not shown until 1979 and was a commercial disappointment in Australia. 

The film tied with Invasion of the Body Snatchers to win the Antennae II Award at the Avoriaz Fantastic Film Festival, won the Special Jury Award at 1978s Paris Film Festival and won Best Film, Prize of the International Critics Jury for director Eggleston and Best Actor for Hargreaves. 

=== Critical reception === AllMovie wrote, "Long Weekend is little more than an extended cautionary tale about the karmic foolishness of disrespecting nature.   DVD Times praised the film, and also commented on its obscurity: "when an obviously well made and executed little thriller comes along, an exercise in controlled dread and eerie atmosphere thats really effective, you have to ponder the reasons why the vast majority passed on it. Early Australian cinema seems cursed in this category." 

De Roche later expressed some dissatisfaction with the film:
 Unfortunately, the bush comes across as a threat too early; it should have emerged as a threat only after the audience had sympathized with the animals. And I don’t think that sympathy is there. Long Weekend would have been much better if the audience had been told at the beginning that Peter and Marcia were going to die. This way, it wouldn’t have had to sympathize with them, and could have concerned itself solely with when this was going to happen. Such is the essence of suspense.  

== Remake ==
 
In 2008, Australian director Jamie Blanks shot a remake of the film (alternately titled Natures Grave). The film starred James Caviezel and Claudia Karvan. 

==See also==
*Cinema of Australia

==References==
 

== External links ==
*  
*  at Oz Movies
*  at Australian Screen Online
*  at AustLit
 

 
 
 
 
 
 
 
 
 
 