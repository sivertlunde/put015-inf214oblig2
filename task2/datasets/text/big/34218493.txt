Maaran (film)
{{Infobox film
| name           = Maaran
| image          = Maaran DVD cover.svg
| caption        = DVD cover
| director       = A. Jawahar
| producer       = J. S. Pankaj Mehta
| writer         = A. Jawahar Pattukottai Prabhakar (dialogues)
| starring       =   Deva
| cinematography = U. K. Senthilkumar
| editing        = Sai Elango
| studio         = Power Media
| distributor    = 
| released       =  
| country        = India
| runtime        = 150 min
| language       = Tamil
}}
 2002 Cinema Indian  Tamil film, Seetha and Deva and was released on 20 September 2002.   The film enjoyed a moderate recognition at the box office.

==Plot==

Maaran (Sathyaraj) is a clerk in the district collector’s office. His life centers round his loving wife (Seetha (actress)|Seetha), his son (Raguvannan) and his daughter (Preethi Varma). Soft spoken, and a Gandhian to the hilt, Maaran does not stand by any smear on the Mahatma’s name. Being a staunch patriot, he had even named his son Sudhandhiram.

After topping at the state level, Maarans son Sudhandhiram wins admission to a medical college, where he is in for a rough time, ragged mercilessly by the sadistic Shivadas (Robert) and his cronies. Nonetheless, Anjali (Santhoshi) falls in love with Sudhandhiram. Matters run wild when Sudhandhiram returns Shivadas’ humiliations with a tight slap, and defeats him in the college elections. The humiliation of Shivadas makes matters even wild when Sudhandhiram pays for it with his life. Shivadas packs the corpse in a suitcase and disposes of it. But soon, Shivadas is hauled up for the murder. However, Shivadas manages to get a clean-chit of the case, thanks to his influential father. Seetha becomes lunatic due to the emotional shock. The grieving father, Maaran, takes it on himself to justify his son’s murder.

Maaran traces out the conspirators one by one, and have their evil terminate themselves, justifying his own way of justice. 
At the end of Maaran kills the doctor and gets surrounded in the court. He gets hook in jail.

==Cast==

*Sathyaraj as Maaran Seetha as Seetha, Maarans wife
*Raguvannan as Sudhandhiram, Maarans son
*Preethi Varma as Amudha, Maarans daughter Robert as Shivadas
*Santhoshi as Anjali
*Delhi Ganesh as Ganesan
*Vinu Chakravarthy as Rangaswamy
*Cochin Haneefa as a Central Bureau of Investigation|C.B.I officer Madanagopal
*Ravikumar as Shivadas father
*Ilavarasu as Inspector Irulandi
*Thalaivasal Vijay as Doctor Prakash Devan as a Central Minister Raj Kapoor as Lawyer Subramaniam
*Sarath Babu as a District collector Pallavi as a doctor (guest appearance)
*Abhinayashree as Item number

==Production==
Maaran’ is directed by A. Jawahar, who had apprenticed with directors Senthilnathan & Sunder C. This is his first film. While Deva composes the music, cinematography is by U.K.Senthilkumar, editing by Sai Illango, art by R.K.Nagu and stunt arrangements by Super Subbarayan.

The dialogue of the film written by Pattukottai prabhakar was well received.

Playing the role of Satyarajs son is debutant Raghu, son of actor-director Manivannan. Raghu is paired with Santhoshi, an actress of the small screen and one of the leads in Agathyan’s ‘Kaadhal Samrajyam’. Sharat Babu, Hanifas, Devan, Pallavi, Delhi Ganesh, Raj Kapoor, Ilavarasu and Robert, brother of dancer-actress Alphonsa play the supporting roles. 

== Soundtrack ==

{{Infobox album |  
  Name        = Maaran |
  Type        = soundtrack | Deva |
  Cover       = |
  Released    = 2002 |
  Recorded    = 2002 |  Feature film soundtrack |
  Length      = 25:01 |
  Label       = Five Star Audio | Deva |  
  Reviews     = |
  Last album  = |
  This album  = |
  Next album  = |
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 2002, features 5 tracks with lyrics written by P. Vijay.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration 
|-  1 || "Aanantham Aanantham" || Unni Menon, Sujatha Mohan || 5:07
|- 2 || "Felomina Ni Enthan" || S. P. Balasubrahmanyam, S. P. B. Charan, Mathangi || 4:08
|- 3 || "Kannukulle" || Unni Menon || 5:33 
|- 4 || "Pudi Pudi Kabadi" || Anuradha Sriram || 5:14 
|- 5 || "Queen Marys" || Silambarasan Rajendar || 4:59 
|}

==Reception==

The film received generally mixed to positive reviews. AllIndianSite.com cited the film as "a good movie and worth watching".  Malathi Ragarajan of hindu.com said : "Maaran takes off so well and so differently but the maker obviously got jittery mid-way and allows regular formula fare to take over, with revenge as the pivot. Category:The second part of the film has many thrilling scenes where by Sathyaraj employs simple methods to punish those responsible for Raguvannans death.  

==References==
 

==External links==
*  

 
 
 
 
 