Alchemy (film)
 
{{Infobox film
| name           = Alchemy
| alt            =  
| image	=	Alchemy FilmPoster.jpeg
| caption        = 
| director       = Evan Oppenheimer
| producer       = Lorraine Galler Dan OMeara
| writer         = Evan Oppenheimer
| starring       = Michael Ian Black Tom Cavanagh Sarah Chalke James Stacy Barbour
| music          = Peter Lurye
| cinematography = Luke Geissbuhler
| editing        = Allison Eve Zell
| studio         = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Alchemy is a 2005 film starring Tom Cavanagh and Sarah Chalke. It premiered at the Tribeca Film Festival in 2005. The film did not have a wide release in movie theaters, so its big public premiere was on television, on ABC Family on October 7, 2005. It was written and directed by Evan Oppenheimer.

==Plot==
A university computer scientist tries to make a woman fall in love with his interactive computer before she succumbs to a well-known lothario professor.

==Cast==
*Tom Cavanagh as Mal Downey    
*Sarah Chalke as Samantha Rose 
*James Stacy Barbour as Dr. Troy Rollins
*Michael Ian Black as Jerry (voice)
*Illeana Douglas as KJ
*Nadia Dajani as Jane
*Logan Marshall-Green as Martin
*Wil Horneff as Dave
*Celeste Holm as Iris
*Shannon McGinnis as Barbara
*Anna Belknap as Marissa
*Tovah Feldshuh as Senior Editor
*Daphne Rubin-Vega as Belladonna Editor
*Susan Misner as Associate Editor
*Erik Palladino as Groom

==External links==
*  
*  

 

 

 