Price Check
 
{{Infobox film
| name           = Price Check
| image          = Price check poster.jpg
| caption        = Promotional poster Michael Walker
| producer       = Dolly Hall
| writer         = Michael Walker
| starring       = {{Plainlist|
* Parker Posey
* Eric Mabius}}
| music          = Matt Kollar
| cinematography = Sam Chase
| editing        = {{Plainlist|
* Jennifer Lame
* Michael Taylor}}
| distributor    = IFC Films (US)
| released       =  
| runtime        = 
| country        = United States
| language       = English
}} Michael Walker, and stars Parker Posey and Eric Mabius.

==Plot==
Pete Cozy (Eric Mabius) has found himself a house in the suburbs and a job in the pricing department of a middling supermarket chain.  Petes job allows him to spend quality time with his wife (Annie Parisse) and young son and, despite the fact that they are drowning in debts, they appear happy. 
  
Everything changes when Pete gets a new boss, the beautiful, high-powered, fast talking Susan Felders (Parker Posey). With Susans influence, Pete finds himself on the executive track, something that both surprises and excites him. The more his salary increases, the more he has to perform at work… and the less time he gets to spend with his family.  At the same time, his relationship with his boss begins to cross the line of professional etiquette. Both become enamored with one another – creating tension in the workplace and in his personal home life.

==Cast==
*Parker Posey as Susan Felders
*Eric Mabius as Pete Cozy
*Annie Parisse as Sara Cozy
*Josh Pais as Doug
*Edward Herrmann as Bennington
*Remy Auberjonois as Todd Kenner
*Jayce Bartok as Bobby McCain
*Samrat Chakrabarti as Eddie
*Cheyenne Jackson as Ernie
*Stephen Kunken as Cartwright
*Amy Schumer as Lila
*Matt Servitto as Jim Grady
*Mackenzie Smith

==External links==
*  

 
 
 
 


 