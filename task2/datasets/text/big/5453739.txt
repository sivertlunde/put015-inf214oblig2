The Computer Wore Tennis Shoes
 
{{Infobox film
| name           = The Computer Wore Tennis Shoes
| image          = computer wore tennis shoes ver1.jpg
| caption        = 1969 film poster Robert Butler
| producer       = Bill Anderson
| writer         = Joseph L. McEveety
| starring       = {{Plain list | 
* Kurt Russell
* Cesar Romero Joe Flynn
* William Schallert
* Alan Hewitt
}}
| music          = Robert F. Brunner
| cinematography = Frank V. Phillips
| editing        = Cotton Warburton Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 91 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
| gross          = $5.5 million (US/ Canada rentals) 
}} Joe Flynn Walt Disney Productions and distributed by Buena Vista Distribution Company as part of "The Last Laughs of the 1960s".

It was one of several films made by Disney using the setting of Medfield College, first used in the 1961 Disney film The Absent-Minded Professor and its sequel Son of Flubber. Now You See Him Now You Dont and The Strongest Man in the World, both sequels to The Computer Wore Tennis Shoes, were also set at Medfield.

== Plot ==
Dexter Reilly (Kurt Russell) and his friends attend small, private Medfield College, which cannot afford to buy a computer. The students persuade wealthy businessman A.J. Arno (Cesar Romero) to donate an old computer to the college. Arno is the secret head of a large illegal gambling ring, which used the computer for its operations.
 read and remember the speak a quiz tournament with a United States dollar|$100,000 prize.

Reilly single-handedly leads Medfields team in victories against other colleges. During the tournament, a trigger word causes Reilly to unknowingly recite on television details of Arnos gambling ring. Arnos henchmen kidnap Reilly and plan to kill him, but his friends help him escape. During the escape Dexter suffers a concussion which, during the tournament final against rival Springfield State, gradually returns his mental abilities to normal; one of Reillys friends, however, is able to answer the final question ("What is the geographic center of the contiguous United States?"). Medfield wins the $100,000 prize, and Arno is arrested.

==Cast==
* Kurt Russell as Dexter
* Cesar Romero as A.J. Arno Joe Flynn as Dean Higgins
* William Schallert as Professor Quigley
* Alan Hewitt as Dean Collingsgood
* Richard Bakalyan as Chillie Walsh
* Debbie Paine as Annie Hannah
* Frank Webb as Pete
* Michael McGreevey as Schuyler
* Jon Provost as Bradley
* Frank Welker as Henry
* W. Alex Clarke as Myles
* Bing Russell as Angelo Pat Harrington as Moderator
* Fabian Dean as Little Mac
* Fritz Feld as Sigmund van Dyke Pete Ronoudet as Lt. Charles "Charlie" Hannah
* Hillyard Anderson as J. Reedy
* David Canary* as Walski
* Robert Foul* as Police desk sergeant
* Ed Begley, Jr.* as a Springfield State panelist

 * Not credited on-screen. 

==Reception==
The film received a mixed reception.  

==Legacy==

===Sequels===
* Now You See Him, Now You Dont (1972)
* The Strongest Man in the World (1975)

===TV movies=== The Computer Wore Tennis Shoes in 1995 starring Kirk Cameron as "Dexter Riley".
 Not Quite series of novels with the same name.

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 