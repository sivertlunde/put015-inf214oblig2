Keep Fit
{{Infobox film
| name           = Keep Fit
| caption        = 
| image	         = "Keep_Fit"_(1937).jpg	 
| director       = Anthony Kimmins
| producer       = Basil Dean   Jack Kitchin
| writer         = Anthony Kimmins  Austin Melford
| narrator       = 
| starring       = George Formby Kay Walsh   Guy Middleton   Evelyn Roberts
| cinematography = John W. Boyle   Gordon Dines   Ronald Neame
| music          = Ernest Irving
| editing        = Ernest Aldridge
| studio         = Associated Talking Pictures ABFD
| released       = 1937 
| runtime        = 82 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British comedy film directed by Anthony Kimmins and starring George Formby, Kay Walsh and Guy Middleton. It was made at Ealing Studios.  Formby was at his British top box-office peak when this comedy was made. 

==Synopsis== George Formby again plays his working class underdog (competition)|underdog, gormless, gullible, indefatigable and triumphant hero.

Here Formbys character is a weakling who overcomes obstacles to beat a corrupt rival in the boxing ring. He plays a scrawny barbers assistant who, in response to the keep fit fad sweeping through Britain at the time, dreams of a better physique, and sings of it in the catchy ‘Biceps, Muscle and Brawn’. He falls in love with a beautiful manicurist, and competes for her affections with a muscle bound thug. The manicurist is more attracted to the brute until the barber can prove that he is a crook, and defeat him in the boxing ring.  

==Cast== George Formby as George Freen
* Kay Walsh as Joan Allen
* Guy Middleton as Hector Kent George Benson as Ernie Gill
* Gus McNaughton as "Echo" Publicity Manager
* Evelyn Roberts as Mr Barker
* Aubrey Mallalieu as Magistrate
* Edmund Breon as Sir Augustus Marks  
* Hal Gordon as Reporter  
* Hal Walters as Racing Tough 
* C. Denier Warren as Editor 
* Edgar Driver as Boat Hire Owner 
* Leo Franklyn as Racing Tough
* Robert Nainby as Judge At The Gym 
* Julian Vedey as Hairdressing Dept Head  
* Jack Vyvian as Boat Hire Man 
* D.J. Williams (actor)|D.J. Williams as Editor of The Gazette

==Critical reception==
Sky Movies wrote " its a bouncy, confidently made comedy thats fun throughout and pretty hilarious in its boxing-ring conclusion." 

==References==
 

==Bibliography==
 
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 