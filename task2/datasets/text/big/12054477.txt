You Are My Sunshine (2005 film)
{{Infobox film
| name           = You Are My Sunshine
| image          = You Are My Sunshine film poster.jpg
| caption        = Promotional poster for You Are My Sunshine 
| film name      = {{Film name
| hangul         =      
| hanja          = 너는 내  
| rr             = Neoneun nae unmyeong
| mr             = Nŏnŭn nae unmyŏng}}
| director       = Park Jin-pyo
| producer       = Oh Jeong-wan
| writer         = Park Jin-pyo
| starring       = Jeon Do-yeon
| music          = Bang Jun-seok
| cinematography = Seong Seung-taek
| editing        = Moon In-dae
| distributor    = CJ Entertainment
| released       =  
| runtime        = 121 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =  
}} 2005 South Korean film written and directed by Park Jin-pyo, and starring Jeon Do-yeon.       It was released in Korea on 23 September 2005. 

The official English title is named after the Jimmie Davis song "You Are My Sunshine," which is used on the soundtrack.

== Plot == Filipino Mail-order bride|bride, he falls head over heels in love with local dabang delivery girl Eun-ha, and starts showering her with gifts. Although Eun-ha is initially unimpressed, she is eventually won over by his kindhearted nature, and the two get married. The couples marital bliss is short lived, however, as Eun-ha tests positive for HIV/AIDS, and is then tracked down by her abusive ex-husband, who takes her away and forces her back into prostitution.

== Cast ==
* Jeon Do-yeon ... Eun-ha
* Na Moon-hee ... Seok-joongs mother
* Hwang Jung-min ... Seok-joong
* Ryu Seung-soo ... Chul-kyu
* Seo Joo-hee ... Kyu-ri
* Jeong Yu-seok ... Chun-soo
* Yoon Je-moon ... Jae-ho
* Go Soo-hee ... Hwang Yu-sun Kim Sang-ho ... Kim Kyung-bae
* Kim Boo-seon ... Kim Yeo-in

==Reception== Maundy Thursday a year later.

==Awards and nominations==
2005 Chunsa Film Art Awards
* Best Actress - Jeon Do-yeon

2005 Busan Film Critics Awards
* Best Supporting Actress - Na Moon-hee

2005 Blue Dragon Film Awards  
* Best Actor - Hwang Jung-min
* Best Director - Park Jin-pyo
* Nomination - Best Film
* Nomination - Best Actress - Jeon Do-yeon
* Nomination - Best Supporting Actress - Na Moon-hee
* Nomination - Best Screenplay - Park Jin-pyo
* Nomination - Best Music - Bang Jun-seok

2005 Korean Film Awards 
* Best Actor - Hwang Jung-min
* Best Actress - Jeon Do-yeon
* Nomination - Best Film
* Nomination - Best Director - Park Jin-pyo
* Nomination - Best Supporting Actress - Na Moon-hee

2006 Baeksang Arts Awards 
* Nomination - Best Actor - Hwang Jung-min
* Nomination - Best Actress - Jeon Do-yeon
* Nomination - Best Director - Park Jin-pyo

2006 Grand Bell Awards
* Best Actress - Jeon Do-yeon
* Nomination - Best Film
* Nomination - Best Director - Park Jin-pyo
* Nomination - Best Actor - Hwang Jung-min
* Nomination - Best Supporting Actress - Na Moon-hee

==See also==
* Ticket Dabang

== References ==
 

== External links ==
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 