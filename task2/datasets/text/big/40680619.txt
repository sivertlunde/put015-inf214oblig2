Too Much Johnson (1919 film)
{{infobox film
| title          = Too Much Johnson
| image          =
| imagesize      =
| caption        =
| director       = Donald Crisp
| producer       = Adolph Zukor Jesse Lasky
| writer         =  (film scenario) Lois Wilson
| cinematography = Charles Schoenbaum
| editing        = Nan Heron Dorothy Arzner
| distributor    = Paramount Pictures - Artcraft
| released       = December 1919
| runtime        = 5 reels
| country        = USA

}} Lois Wilson.  

This film is based on William Gillettes 1894 play Too Much Johnson and is the second film attempt at the play, the first having been a short in 1900.  Famously, Orson Welles made a short and unfinished amateur version of the play and he may have not known of this feature silent from 1919.

==Cast==
*Bryant Washburn - Augustus Billings Lois Wilson - Mrs. Billings
*Adele Farrington - Mrs. Batterson Charles H. Geldart - Joseph Johnson
*Monte Blue - Leon Dathis
*Elsie Lorimer - Mrs. Dathis
*Gloria Hope - Leonora Faddish
*George Hackathorn - Henry McIntosh
*Phil Gastrock - Francis Faddish

==References==
 

==External links==
* 
* 
* (Univ. of Washington, Sayre collection)
* (sold on worthpoint)

 

 
 
 
 
 
 
 
 


 