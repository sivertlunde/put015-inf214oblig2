Little Black Sambo (film)
{{Infobox Hollywood cartoon|
| cartoon_name = Little Black Sambo
| series = ComiColor Cartoons
| image =File:Little Black Sambo poster 1935.jpg
| caption =Poster
| director = Ub Iwerks
| story_artist =
| animator = Ub Iwerks
| voice_actor = Possibly Ub Iwerks
| musician = Carl Stalling
| producer = Ub Iwerks
| distributor = Celebrity Productions
| release_date = February 6, 1935
| color_process = Cinecolor
| runtime = 8 minutes English
}}

Little Black Sambo was a 1935 Cinecolor, sound animated film, based on the controversial Little Black Sambo|childrens book of the same name. This film was created at the Ub Iwerks Studio, and released by Celebrity. The film marked the first appearance of an unnamed dog, who appeared in three of Iwerks films.

==Plot== mother is bathing him, and she dries and clothes him as their dog watches. After that, his mother warns, in dialect, "Now, go along and play, honey child. But watch out for that bad, old tiger." and the controversial line, "That old tiger sure do like dark meat." As Sambo goes out to play, the dog sneaks out the window with a fiendish idea. He uses undried brown paint on a fence for stripes and a paint brush for them on his tail. He sees his teeth, and finds a bear trap to resemble sharp teeth. He tests his appearance in a mirror and walks away, although it was an actual tiger.

Sambo is whistling, as the dog is hiding in a tree, sneaking on him. The dog follows him, until Sambo runs away. He finally hides on a coconut tree, and throws coconuts at him, until he grabs a monkeys ear, who throws him out to the ground. Then, the dog tells Sambo that he is not a tiger. Then, Sambo plays fetch, and when the dog retrieves, the real tiger appears, and chases them home. They block the door, and the tiger uses a rock to reproduce a banging sound. He creeps in the house, and they use molasses to trap him. Sambo grabs a skillet and burns the tiger, and he is chased away.

==Availability==
 The film was available for purchase for home viewing between 1942 and 1968 from Castle Films, the company having acquired the rights for it among the many acquired from the defunct Ub Iwerks Studio in 1941. It was listed as #757 in its catalog in 1948 in various formats, including 8mm and 16mm, in color and in black-and-white, condensed to 3:30 minutes or full-length, with sound or silent with minimal dialog on interstitial cards. 

It is currently available on video and DVD in several public domain cartoon anthologies, including "Johnny Legend Presents: The Complete Weird Cartoons." The Castle Films color/sound version has been digitized and is available as part of the collection at the Internet Archive, mislabeled there as "Loittle Black Sambo." 

==In popular culture==
Clips of the cartoon was seen in Bamboozled, a Spike Lee movie about black stereotypes.
 Spinning Into Butter, starring Sarah Jessica Parker and Victor Rasuk.

==External links==
*  

==References==
 

 
 
 
 
 
 
 
 
 
 