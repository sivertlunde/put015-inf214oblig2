People Will Talk
 
{{Infobox film
| name           = People Will Talk
| image          = People will talkdvd.jpeg
| caption        = People Will Talk DVD cover
| director       = Joseph L. Mankiewicz
| producer       = Darryl F. Zanuck
| based on       =  
| writer         = Joseph L. Mankiewicz
| starring       = Cary Grant Jeanne Crain Hume Cronyn Finlay Currie Walter Slezak Sidney Blackmer Alfred Newman Milton Krasner
| editing        = Barbara McLean
| distributor    = 20th Century Fox
| released       =  
| runtime        = 110 min.
| country        = United States
| awards         = English
| budget         =
| gross          = $2.1 million (US rentals) 
}}

People Will Talk (1951 in film|1951) is a romantic comedy/drama directed by Joseph L. Mankiewicz and produced by Darryl F. Zanuck from a screenplay by Mankiewicz, based on the German play by Curt Goetz, which had been made into a movie in Germany (Frauenarzt Dr. Prätorius, 1950). Released by Twentieth Century Fox, the film stars Cary Grant and Jeanne Crain, with supporting performances by Hume Cronyn, Finlay Currie, Walter Slezak, and Sidney Blackmer.

The film was nominated for the Writers Guild of America screen Award for Best Written American Comedy (Joseph L. Mankiewicz).

== Plot == bass viol in the student/faculty orchestra conducted by Praetorius.

=== Elwells misconduct suit === Margaret Hamilton) who once worked for Praetorius reacts visibly when Elwell asks her about Praetoriuss mysterious friend Mr. Shunderson, who rarely leaves Praetoriuss side and has a deep, intuitive understanding of human and animal nature.   

Elwells detective discovers that Shunderson was once tried for murder, and Elwell calls for a misconduct hearing against Praetorius. At the hearing, Praetorius explains that he started his career in a small town by opening a butcher shop as a front for his undeclared medical practice, because the people of the town didnt trust doctors. Elwell accuses Praetorius of  "quackery", but Praetorius defends himself with the fact that he was a licensed practitioner, describing how he was forced to leave town after his maid discovered his medical degree.

Praetorius refuses to answer questions about Shunderson, but Shunderson explains that he served 15 years in prison for the alleged death of a man who had tried to murder him, then somehow survived being hanged after actually murdering the man, who had gone into hiding during the first trial. When he woke up, he was lying on a table in front of Praetorius, who was at that time a medical student examining what he believed was a cadaver. Praetorius kept Shundersons survival a secret, and Shunderson became Praetoriuss devoted friend. After this story is told, the chairman concludes the hearing in Praetoriuss favor, and Elwell walks away alone and discredited.

=== Deborah Higgins ===
Meanwhile, Deborah enters Praetoriuss life, displaying signs of emotional distress. After she faints during a lecture, Praetorius examines her and informs her that shes pregnant. Upset by this news, "Mrs. Higgins" admits that shes not really married. The unborn childs father is dead, and knowing about her condition would be too much for her own father to bear. In a hallway near Praetoriuss office, she shoots herself.

After successfully operating on Deborah, Praetorius tries to calm her by telling her there was a mistake in her pregnancy test, but she has fallen in love with him, and becomes upset at her own embarrassing behavior.  She runs away from the clinic, forcing him to find her so he can tell her she really is pregnant.  

Praetorius and Shunderson drive out to where Deborah and her father Arthur live, a farm owned by Arthurs brother John. Deborah and Praetorius hide Deborahs shooting incident from her father, who is a failure in life and lives unhappily as a dependent of his stingy brother. Deborah is his only pride in life, which might become intolerable for him with a baby to take care of and his daughters reputation ruined.

While showing Praetorius the farm, Deborah admits her love for him. She also wonders why he is visiting and begins to suspect that he is attracted to her. After she seductively interrogates him, they share a passionate kiss. They soon get married, and Arthur comes to live with them. A few weeks later, Deborah suggests to Noah that she may be pregnant, and he admits that she was pregnant all along. She ruefully concludes that he married her out of pity, but he convinces her that he really did fall in love with her.

For some reason, Praetoriuss misconduct hearing was scheduled for the same time as the student/faculty orchestras concert.  After the hearing and Praetorius acquittal, the film ends with Deborah, Shunderson, and Barker in the audience watching Praetorius conduct the orchestra in the finale of   ! .

== Cast ==
 
* Cary Grant as Dr. Noah Praetorius
* Jeanne Crain as Deborah Higgins
* Finlay Currie as Shunderson
* Hume Cronyn as Prof. Rodney Elwell
* Walter Slezak as Prof. Barker
* Sidney Blackmer as Arthur Higgins
* Basil Ruysdael as Dean Lyman Brockwell
* Katherine Locke as Miss James
* Parley Baer as Toy Store Salesman (uncredited)
* Bess Flowers as Concert audience member (uncredited) Margaret Hamilton as Housekeeper (uncredited)
* Stuart Holmes as Board member (uncredited) Jack Kelly as Student in classroom (uncredited) Will Wright as John Higgins (uncredited)
 

=== Political Overtones ===
According to a review at the Films de France website,  the movie has a number of political overtones.  The review says the film is a reaction to "Mankiewicz’s own experiences during the communist witch hunts of the late 1940s, early 1950s which were initiated by Senator Joseph McCarthy. Whilst President of the Director’s Guild, Mankiewicz was openly attacked by Cecil B. DeMille, a once great filmmaker and hard-line conservative, for his unwillingness to support the anti-communist campaign.  Like many of his fellow directors and screenwriters, Mankiewicz was a liberal who refused to denounce others working in Hollywood who had communist sympathies."

The films investigative trial has parallels to the congressional hearings by anti-communist crusaders. And just as some refused to name names in such hearings, the lead character, played by Cary Grant, declines to clear his own name by revealing the private business of another person, in this case a convicted murderer.

The review said that although the movie is meant as a cautionary tale about the dangers of witch hunts it deals with many other issues, including the pregnancy of a single woman, the "corrosive effect of unfettered capitalism, the human cost of the Korean war, among others."

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 