Napoleon's Barber
 
{{Infobox film
| name           = Napoleons Barber
| image          = 
| caption        = 
| director       = John Ford 
| producer       = 
| writer         = Arthur Caesar
| starring       = Otto Matieson Natalie Golitzen
| music          = 
| cinematography = 
| editing        =  Fox Film Corporation
| released       =  
| runtime        = 32 minutes 
| country        = United States 
| language       = English
| budget         = 
}}
 Fox Movietone sound-on-film system.

The film, Fords first talkie, is now considered to be a lost film.   

==Cast==
* Otto Matieson as Napoleon
* Natalie Golitzen as Empress Josephine
* Frank Reicher as Napoleons Barber
* Helen Ware as The Barbers Wife
* Philippe De Lacy as The Barbers Son
* DArcy Corrigan as Tailor
* Russ Powell as Blacksmith Michael Mark as Peasant
* Buddy Roosevelt as French Officer
* Ervin Renard as French Officer
* Youcca Troubetzkoy as French Officer
* Joseph Waddell as French Officer
* Henry Hebert as Soldier

==See also==
*List of lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 