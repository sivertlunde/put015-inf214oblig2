Vampire Girl vs. Frankenstein Girl
{{Infobox film
| name           = Vampire Girl vs. Frankenstein Girl
| image          = VampireVsFrankenstein.jpg
| caption        = 
| director       = Yoshihiro Nishimura Naoyuki Tomomatsu
| producer       = Masatsugu Asahi
| screenplay     = Yoshihiro Nishimura
| based on       =  
| starring       = Yukie Kawamura Eri Otoguro Takumi Saito
| music          = Kou Nakagawa
| cinematography = Shu G. Momose
| editing        = 
| studio         = 
| distributor    = Excellent Film Eleven Arts
| released       =  
| runtime        = 85 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
Vampire Girl vs. Frankenstein Girl (吸血少女対少女フランケン, Kyūketsu Shōjo tai Shōjo Furanken) is a 2009 Japanese gore film.  It was directed by Yoshihiro Nishimura and Naoyuki Tomomatsu and premiered at the New York Asian Film Festival in June 2009.  It is based on a manga of the same name by Shungiku Uchida. 

== Plot ==
In a typical Tokyo high school a perpetually teenage vampire named Monami (Yukie Kawamura) falls for her classmate, Mizushima (Takumi Saito), who happens to already be the reluctant boyfriend to the vice principal/science professors daughter, Keiko (Eri Otoguro), a leader of a Sweet Lolita gang. The ensuing love triangle leads Keiko to seek the assistance of her father who, unbeknown to his daughter, moonlights as a Kabuki-clad mad scientist with the school nurse as his assistant. The pair experiment on students in the school basement hoping to discover the secret of reanimating corpses (akin to the work of Victor Frankenstein). Their hopes are answered when they discover a solution of Monamis blood holds the properties to bring life to dead body parts and inanimate objects.

The story begins to unfold after Mizushima carelessly accepts a honmei choco spiked with Monamis blood, causing him to become a half vampire. When Keiko discovers their secret, she attacks Monami but accidentally throws herself off the school roof in the process. Her premature death leads to her father using the blood solution to transform her into a vicious Frankensteins monster determined to get revenge against Monami. From then on Monami and Keiko battle each other in the pursuit of winning Mizushimas heart, regardless of his feelings towards either of them. Monami ultimately kills Keiko by using her powers to turn droplets of her blood into spikes that rip the flesh off the latters body and leaves her skeleton impaled at the top of Tokyo Tower. At the end Keikos father turns himself in a Franken Advanced Composite Life Form with use of Monamis blood, and it is revealed that Igor was turned into a vampire by Monami a hundred years ago, and that Mizushima is only one in a long succession of boyfriends.

== Cast ==
*Yukie Kawamura as Monami / Vampire Girl
*Eri Otoguro as Keiko / Frankenstein Girl
*Takumi Saito as Jyugon Mizushima (as Takumi Saitô)
*Eihi Shiina as Monamis Mother
*Takashi Shimizu	as Chinese Professor
*Jiji Bû as Igor
*Erina
*Sayaka Kametani as Nurse Midori
*Sayako Nakoshi
*Aya Nishisaki
*Sayo
*Kanji Tsuda
*Ewan Anning as Francis Xavier

== Subculture references == Wrist cutting is a theme that returns from Nishimuras 2008 film, Tokyo Gore Police. 

==Release==
In the United States, the film was released on 26 June 2009 from American Film Market. 

== References ==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
  
 
  
 