The Diamond Queen (1953 film)
{{Infobox film
| name           = The Diamond Queen
| image          = 
| image_size     = 
| caption        = 
| director       = John Brahm
| writer         = 
| narrator       = 
| starring       = Fernando Lamas
| music          = 
| cinematography = 
| editing        = 
| studio         = Melson Productions 
| distributor    = Warner Bros. 
| released       = November 28, 1953
| runtime        = 80 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Diamond Queen is a 1953 film directed by John Brahm. It stars Fernando Lamas and Arlene Dahl. 

==Plot==
Adventurer and gem expert Jean Baptiste Tavernier promises a diamond to cap King Louis XIVs crown for the coronation, but the reckless haste of the kings emissary, Baron Paul de Cabannes, causes the jewel to be cut badly and ruined.

Jean volunteers to travel to India to bring back another worthy stone. Cabannes insists on coming along and complicates the journey more than once before saving Jeans life and earning his respect.

The men are caught leering at a lovely woman bathing in a waterfall and are taken prisoner by her men. She is Queen Maya of Nepal. In the temple, Jean and Cabannes learn of the Eye of the Goddess, a rare blue diamond. It is in the possession of the Mogul of Golconda, who promises to give it to Queen Maya as a wedding gift, but secretly plans to take rule of her country.

With the use of a new "secret weapon," a prototype of hand grenade, the Frenchmen are able to overcome the Moguls men in battle. The queen offers to give them the diamond, so in return they invite her to Louiss coronation.

==Cast==
*Fernando Lamas as Jean Baptiste Tavernier
*Arlene Dahl as Queen Maya
*Gilbert Roland as Baron Paul de Cabannes
*Sheldon Leonard as Mogul
*Jay Novello as Gujar, Mayas steward

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 