Town on Trial
{{Infobox film
| name           = Town on Trial
| image          = "Town_on_Trial".jpg
| image_size     =
| caption        = UK quad format poster
| director       = John Guillermin
| producer       = Maxwell Setton
| writer         = Ken Hughes Robert Westerby
| starring       = John Mills Charles Coburn Barbara Bates Derek Farr Alec McCowen
| music          = Tristram Cary
| cinematography = Basil Emmott
| editing        = Max Benedict
| studio = Marksman Films   Linked 2015-03-01  Columbia Picture UK
| released       =  
| runtime        = 96 mins  
| country        = United Kingdom
| language       = English
| budget         =
}} British mystery film directed by John Guillermin and starring John Mills, Charles Coburn, Barbara Bates and Derek Farr. A whole town comes under suspicion when two grisly murders are carried out—particularly members of the local sports club.

==Plot==
While playing tennis at a posh club in a town near London called Oakley Park, young and flirty Molly Stevens attracts considerable interest. The men ogle her and the women detest her. She is later found dead, strangled with a stocking. The local police chief requests the help of Scotland Yard, so Detective Superintendent Mike Halloran is sent to investigate. The locals, however, resent having an outsider poking into their affairs.

A book of love poems, including an inscription from a Peter Crowley, is found in the victims flat. There is also a photo of a group of men together with Molly and Fiona Dixon, a young woman from the prominent Dixon family. Her father, although chocked to learn that his daughter knew the "trashy* Molly, will not let Halloran question her.

Halloran eventually learns that Molly left Peter Crowley for a married man, Mark Roper, who is the clubs secretary. Roper denies any involvement, and also claims that he was giving nurse Elizabeth Fenner a lift to the hospital at the time of the murder.
 expat physician who certified the death, is asked by Halloran why he neglected to report that fact. Fenner claims he was trying to avoid a scandal for Roper and the club.

Elizabeth turns out to be the doctors niece and also a Canadian expat. She confirms that Roper gave her a lift, but this later turns out to be a lie - a result of her attempts to protect the reputation of her uncle, who left his practice in Toronto after a misdiagnosis led to a patients death. This is something the locals are unaware of, but Roper knows and is using this secret to blackmail the doctor.

Roper, always bragging about his wartime heroics as a RAF fighter pilot, is revealed by Halloran to have been nothing but a lowly ground crew member, who was dishonourably discharged after theft and is currently heavily in debt. The club demands Ropers resignation. He turns up at a party, gets drunk and starts a fight. Leaving the party and going for a walk, Fiona is ambushed and strangled to death. Her body is placed in the trunk of Dr. Fenners car.

Halloran finds out that Peter Crowley has been treated for schizophrenia. Peter flees to a church, climbing to the top and threatening to jump. Halloran ascends to the steeple to prevent the suicide, risking his own life in the process, and manages to convince Peter not to kill himself. A fire brigade turntable ladder rescues the two just as they are about to fall.

==Main cast==
 
* John Mills as Superintendent Mike Halloran
* Charles Coburn as Dr. John Fenner
* Barbara Bates as Elizabeth Fenner 
* Derek Farr as Mark Roper 
* Alec McCowen as Peter Crowley 
* Fay Compton as Mrs. Crowley
* Elizabeth Seal as Fiona Dixon 
* Geoffrey Keen as Charles Dixon 
* Margaretta Scott as Helen Dixon  Meredith Edwards as Sergeant Rogers 
* Harry Locke as Sergeant Beale 
* Raymond Huntley as Dr. Reese, pathologist
* Harry Fowler as Bandleader
* Maureen Connell as Mary Roper
* Magda Miller as Molly Stevens
* Newton Blick as Assistant Commissioner Beckett 
* Oscar Quitak as David 
* Dandy Nichols as Mrs. Wilson
 

==Trivia==
A close look at the Ordnance Survey map of "Oakley Park" in the opening credits, shows it to be of Banbury—see Lower Middleton Cheney, Marston St.Lawrence et al. to the east.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 