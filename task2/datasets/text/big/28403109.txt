Jhummandi Naadam
 
{{Infobox film
| name = Jhummandi Naadam
| image = Jhummandi Naadam.jpg
| alt =  
| caption =
| director = K. Raghavendra Rao
| producer = Lakshmi Manchu
| writer = Bhupati Raja  Gopi Mohan  B.V.S.Ravi  Raja Simha Suman
| music = M. M. Keeravani
| cinematography = S. Gopal Reddy
| editing = Marthand K. Venkatesh
| studio =Sree Lakshmi Prasanna Pictures
| distributor =
| released =  
| runtime =
| country = India
| language = Telugu
| budget =  11 Crores
| gross = 11 Crores
}}
Jhummandi Naadam is a 2010 Telugu film produced by Lakshmi Manchu and directed by veteran K. Raghavendra Rao. The film stars Manoj Manchu and Taapsee Pannu in the lead roles and Mohan Babu in a supporting role. The film has music scored by M.M. Keeravani. The film was released on July 1, 2010.

==Plot==
Balu (Manoj Manchu) has only one mission in his life - to become a great playback singer like SP Balasubramanyam. He challenges a landlord in his village and comes to Hyderabad to become a singer. Captain Rao (Mohan Babu) stays in the opposite house. He is an old-fashioned man who hates the lifestyle of new generation. Sravya (Taapsee Pannu|Tapsee) is an NRI girl who stays in her father’s friend Captain Rao’s house. She is in India to do documentary on traditional Telugu music. Balu acts as a local guide to her and in the process, they fall in love. Captain Rao doesn’t like them falling in love with each other. The rest of the story is all about Captain Rao’s restrictions and how the lovers emerge unscathed.

==Cast==
* Manoj Manchu .... Balu Tapsee .... Sravya
* Mohan Babu .... Captain Rao Suman .... Sravyas Father
* Brahmanandam
* MS Narayana Ali
* Dharmavarapu Subramanyam
* Ahuti Prasad
* Tanikella Bharani
* Sudha Aishwarya
* Pragathi
* Vithika Sheru

==Soundtrack==
{{Infobox album
| Name = Jhummandi Naadam
| Artist = various artists
| Type = soundtrack
| Image =
| Cover =
| Released =  28th May 2010 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =   Aditya Music
| Music = M.M.Keeravani Lakshmi Manchu|
}}
The soundtrack of the film was released worldwide on 28 May 2010 . It had music scored by composer M. M. Keeravani. S.P.Balasubramanyam sang 5 songs after a long time.

{| class="wikitable"
|- style="background:#ccc; text-align:center;"
! Song !! Singer(s)!! Duration !! Lyrics
|-
| "Sarigamapadanee"
| S.P.Balasubramanyam
| 4:43
| Vedavyasa
|-
| "Laali Paaduthunnadi"
| S.P.Balasubramanyam, Geetha Madhuri, MK Balaji|Balaji, Deepu
| 5:13
| Suddala Ashok Teja
|-
| "Govinda Harigovinda"
| S.P.Balasubramanyam
| 2:30
| Vedavyasa
|-
| "Yem sakkagunnavro"
| Anuj Gurwara, Chaitra
|
| Suddala Ashok Teja
|-
| "Sannayi Mrogindi"
| S.P.Balasubramanyam, Sunitha Upadrashta|Sunitha, Malavika (singer)|Malavika, Chaitra
| 5:52
| Suddala Ashok Teja
|-
| "Deshamante"
| S.P.Balasubramanyam, Chaitra, Mounika
| 5:00 Chandrabose
|-
| "Nigraham" Ranjith
|
| Chandrabose
|-
| "Balamani"
| Karthik (singer)|Karthik, Shivani
|
| Chandrabose
|-
| "Entha Entha"
| Krishna Chaitanya, Sunitha
|
| Chandrabose
|}

==References==
 
http://www.idlebrain.com/audio/areviews/vedam-jhummandinaadam.html

http://popcorn.oneindia.in/title/7449/jhummandi-naadam.html

http://www.idlebrain.com/movie/archive/mr-jhummandinaadam.html

http://www.idlebrain.com/celeb/interview/lakshmimanchu1.html

http://www.telugufilms.org/wiki/index.php?title=Jhummandi_Naadam_Telugu_Film_Review

==External links==
 

 
 
 
 
 