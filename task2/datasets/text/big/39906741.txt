The Thunderstorm
 
 
{{Infobox film
| name           = The Thunderstorm
| image          = TheThunderstorm.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster of the 1970s re-release version
| film name      = {{Film name
| traditional    = 雷雨
| simplified     = 雷雨
| pinyin         = Léi Yǔ
| jyutping       = Leoi4 Jyu2 }}
| director       = Ng Wui
| producer       = 
| writer         = 
| screenplay     = Ching Kong
| story          = 
| based on       =  
| narrator       = 
| starring       = Bruce Lee Pak Yin Cheung Ying Kong Duen-yee
| music          = Wu Dajiang
| cinematography = Suen Lun
| editing        = Choi Cheong
| studio         = Wa Kiu Film Company
| distributor    = Tai Sang Films
| released       =  
| runtime        = 124 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}
 1957 Cinema Hong Kong Mandarin for re-release during the 1970s in Hong Kong when Lee shot to super stardom during the time when Mandarin films dominated Hong Kong cinema.

==Cast==
*Bruce Lee as Chow Chung
*Pak Yin as Lui Shi Ping
*Cheung Ying as Chow Ping
*Kong Duen-yee as Tse Fung
*Wong Man Lei as Fan Yee 
*Ng Wui as Lu Kuei
*Lo Tan as father
*Lo Tun as Chow Pok Yuen
*Lee Ching as Lo Tai Hoi
*Law Lan
*Yip Ping
*Lee Yuet Ching
*Lee Pang Fei

==See also==
*Bruce Lee filmography
*Thunderstorm (play)|Thunderstorm

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 

 