Angels & Demons (film)
 
{{Infobox film
| name           = Angels & Demons
| image          = angels_and_demons.jpg
| caption        = Theatrical poster
| alt =
| director       = Ron Howard
| producer       = Brian Grazer Ron Howard John Calley
| screenplay     = David Koepp Akiva Goldsman
| based on       =  
| narrator       = Alfred Molina
| starring       = Tom Hanks Ewan McGregor Ayelet Zurer Stellan Skarsgård Pierfrancesco Favino Nikolaj Lie Kaas Armin Mueller-Stahl
| music          = Hans Zimmer 
| cinematography = Salvatore Totino Mike Hill
| studio         = Imagine Entertainment Skylark Productions Panorama Films
| distributor    = Columbia Pictures
| released       =  
| runtime        = 138 minutes

144 minutes (Extended Edition)
| country        = United States
| language       = English
| budget         = $150 million 
| gross          = $485.9 million 
}} thriller film novel of The Da Culver City, California.

Tom Hanks reprises his role as Professor Robert Langdon. Producer Brian Grazer, composer Hans Zimmer and screenwriter Akiva Goldsman also return, with David Koepp coming on board to help the latter.  As of 2015, the film remains Hanks only live-action sequel.

==Plot== vials of Camerlengo Patrick the favourite cardinals to be elected pope) before the conclave enters seclusion and threaten to kill one candidate every hour and destroy all of Vatican City at midnight, using the missing vial of antimatter as a bomb. The Vatican summons symbologist Robert Langdon (Tom Hanks) from Harvard University and Vittoria from CERN to help them save the four preferiti and locate the vial.
 cardinals will banned book Vatican Gendarmerie ambigrammatic word "Earth (classical element)|Earth". They verify the second location is Saint Peters Square but are unable to save Cardinal Lamassé (Franklin Amobi); his lungs punctured and his body branded with "Air (classical element)|Air".
 Water altar, Fountain of hidden passageway car explodes.
 a remote recess in the building where he is able to commit suicide by setting himself on fire.
 named Pope Luke, with Cardinal Strauss as the new camerlengo. Strauss thanks Langdon for his assistance and gives Langdon Galileos "Diagramma Veritatis" for his research, requesting only that Langdons will contain a request that it be returned to the Vatican, and that any future references he makes about the Catholic Church in his future publications be done gently, to which Langdon replies, "Ill try."

==Cast==
 
* Tom Hanks as Robert Langdon, a Harvard University professor of symbology. Camerlengo Patrick McKenna.
* Ayelet Zurer as Dr. Vittoria Vetra, a CERN scientist whose antimatter experiment has been stolen by the Illuminati.
* Stellan Skarsgård as Commander Maximilian Richter, head of the Swiss Guard. Gendarme Corps of Vatican City State.
* Nikolaj Lie Kaas as The Assassin.
* Armin Mueller-Stahl as Cardinal Strauss, Dean of the College of Cardinals and the Papal Conclave.
* Thure Lindhardt as Lieutenant Chartrand, officer of the Swiss Guard.
* David Pasquesi as Claudio Vincenzi, a Vatican police officer sent to summon Robert Langdon.
* Cosimo Fusco as Archbishop Simeon, Prefect of the Papal Household. Fountain of the Four Rivers.
* Carmen Argenziano as Father Silvano Bentivoglio, a Catholic priest and a CERN scientist who performed the antimatter experiment along with Dr. Vetra.
* Marco Fiorini as Cardinal Baggia/Pope Luke I, one of the four Preferiti and a cardinal from Milan, Italy and the favorite to succeed as the new pope.
* Bob Yerkes as Cardinal Guidera, one of the four Preferiti and a cardinal from Barcelona, Spain.
* Franklin Amobi as Cardinal Lamassé, one of the four Preferiti and a cardinal from Paris, France.
* Curt Lowens as Cardinal Ebner, one of the four Preferiti and a cardinal from Frankfurt, Germany. Fountain of the Four Rivers.
* Alfred Molina  (opening narration, uncredited) 

==Production==

===Development=== the 2006 film adaptation of The Da Vinci Code, Sony hired screenwriter Akiva Goldsman, who wrote the film adaptation of The Da Vinci Code, to adapt Angels & Demons.  Filming was originally to begin in February 2008 for a December 2008 release,  but because of the 2007–2008 Writers Guild of America strike, production was pushed back for a May 15, 2009 release.  David Koepp rewrote the script before shooting began. 

Director Ron Howard chose to treat Angels & Demons as a sequel to the previous film, rather than a prequel, since many had read the novel after The Da Vinci Code. He liked the idea that Langdon had been through one adventure and become a more confident character.    Howard was also more comfortable taking liberties in adapting the story because the novel is less popular than The Da Vinci Code.    Producer Brian Grazer said they were too "reverential" when adapting The Da Vinci Code, which resulted in it being "a little long and stagey." This time, "Langdon doesnt stop and give a speech. When he speaks, hes in motion."    Howard concurred "its very much about modernity clashing with antiquity and technology vs. faith, so these themes, these ideas are much more active whereas the other one lived so much in the past. The tones are just innately so different between the two stories." 

===Differences between novel and film=== 2005 election of Pope Benedict XVI, this was judged to be out of date.  There are many differences between the novel and the movie. The character of CERN Director Maximillian Kohler does not appear in the movie. The Italian Camerlengo Carlo Ventresca is changed to the Irish Patrick McKenna, portrayed by Ewan Mcgregor. The Boeing X-33 that takes Langdon from the United States to Geneva and then to Rome is absent in the film. In the novel, Commander Olivetti is the commander of Swiss Guard, and his second in command is Captain Rocher, whereas in the film, Richter is the head of the Swiss Guard. In the novel, the Assassin contacts members of the BBC in order to influence how they present the story of his activities, but this does not happen in the movie. The character Leonardo Vetra is named Silvano Bentivoglio in the film, and is not related to Vittoria. In the novel Camerlengo Carlo Ventresca is revealed to be the late popes biological son, in the film this is absent. 
In the movie the assassin is killed by a car bomb, whereas in the book he falls from a balcony at the top of the Castel Sant Angelo and breaks his back on a pile of marble cannonballs which eventually kills him.
In the book, all four preferiti are killed by the assassin and eventually the high elector is elected as the new pope whereas in the movie, the fourth preferiti is saved by Langdon and is elected as the new pope. The high elector becomes the Camerlengo to the new pope.
In the end, the new Camerlengo hands over Galileos book to Langdon instead of a swiss guard handing the 5th brand, the Illuminati diamond.

===Filming=== fake working Biblioteca Angelica was used for the Vatican Library.  Filming took place at the University of California, Los Angeles in July.  Sony and Imagine Entertainment organized an eco-friendly shoot, selecting when to shoot locations based on how much time and fuel it would save, using cargo containers to support set walls or chroma key|greenscreens, as well as storing props for future productions or donating them to charity. 

 ]]
 naturalism he had employed on his previous film Frost/Nixon (film)|Frost/Nixon, often using handheld cameras to lend an additional energy to the scenes.

Hanks interrupted filming of one scene in order to help Australian bride Natalia Dearnley get through the crowds to her wedding on time.  McGregor said the Popes funeral was the dullest sequence to film, as they were just walking across staircases. Then, "Someone started singing Bohemian Rhapsody   it became the funeral theme tune." 
 before it Sala Regia was made smaller to fit inside the stage.    

The Saint Peters Square and the Piazza Navona sets were built on the same backlot; after completion of scenes at the former, six weeks were spent converting the set, knocking down the Basilica side and excavating 3½ feet of tarmac to build the fountain. As there had been filming at the real Piazza Navona, the transition between it and the replica had to be seamless. To present the Santa Maria del Popolo undergoing renovation, a police station in Rome opposite the real church was used for the exterior; the scaffolding would hide that it was not the church. Cameron built the interior of Santa Maria del Popolo on the same set as the recreated Santa Maria della Vittoria to save money; the scaffolding also disguised this. The films version of Santa Maria della Vittoria was larger than the real one, so it would accommodate the cranes used to film the scene. To film the Pantheon, Rome|Pantheons interior, two aediculae and the tomb of Raphael were rebuilt to scale at a height of 30 feet, while the rest was greenscreen. Because of the buildings symmetrical layout, the filmmakers were able to shoot the whole scene over two days and redress the real side to pretend it was another.  The second unit took photographs of the Large Hadron Collider and pasted these in scenes set at CERN. 

==Music==
{{Infobox album  
|  Name        = Angels & Demons: Original Motion Picture Soundtrack
|  Type        = Soundtrack
|  Artist      = Hans Zimmer
|  Cover       = 
|  Released    = May 22, 2009
|  Recorded    =
|  Genre       = Soundtrack
|  Length      =
|  Label       = Columbia Pictures Industries, Inc.
|  Producer    =
|  Reviews     =
}}

Angels & Demons: Original Motion Picture Soundtrack was released on May 22, 2009. 

Hans Zimmer returned to compose the score for the sequel. He chose to develop the "Chevaliers de Sangreal" track from the end of The Da Vinci Code as Langdons main theme in the film. The soundtrack also features violinist Joshua Bell.

{{Track listing
| title1          = 160 BPM
| length1         = 6:41
| title2          = God Particle
| length2         = 5:20
| title3          = Air
| length3         = 9:07
| title4          = Fire
| length4         = 6:51
| title5          = Black Smoke
| length5         = 5:45
| title6          = Science and Religion
| length6         = 12:27
| title7          = Immolation
| length7         = 3:39
| title8          = Election By Adoration
| length8         = 2:12
| title9          = 503
| length9         = 2:14
| title10          = H2O (Bonus downloadable track)
| length10         = 1:51

}}

==Home media and different versions==
The DVD was released on November 24, 2009 in several countries as a theatrical version and extended cut.

The extended cut includes violent scenes which had been cut out to secure a PG-13 rating.  In the UK, the already censored US theatrical version had to be censored further in order to obtain a BBFC 12 rating.  The Blu-ray includes the original theatrical version and is classified BBFC 15.

==Reception==

===Catholic controversy===
CBS News interviewed a priest working in Santa Susanna, who stated the Church did not want their churches to be associated with scenes of murder. A tour guide also stated most priests do not object to tourists who visit out of interest after reading the book, a trend which will continue after people see the film. "I think they are aware that its, you know, a work of fiction and that its bringing people into their churches."  Grazer deemed it odd that although The Da Vinci Code was a more controversial novel, they had more freedom shooting its film adaptation in London and France.  Italian authorities hoped the filmmakers corrected the location errors in the novel, to limit the amount of explaining they will have to do for confused tourists. 
 Catholic League, has not called for a boycott, but has requested that Catholics inform others about anti-Catholic sentiments in the story. "My goal... is to give the public a big FYI: Enjoy the movie, but know that it is a fable. It is based on malicious myths, intentionally advanced by Ron Howard." A Sony executive responded they were disappointed Donohue had not created attention for the film closer to its release date.  Howard criticized Donohue for prejudging the film, responding it could not be called anti-Catholic since Langdon protects the Church, and because of its depiction of priests who support science. 
 Archbishop Velasio boomerang effect" of drawing more attention to Angels & Demons and making it more popular. 

In a FAQ titled Angels & Demons: from the Book to the Movie  Massimo Introvigne, Director of CESNUR (Center for the Study of New Religions) points out crucial factual errors in Dan Browns original novel and the film version. Introvigne also criticizes the Illuminati mythology that is treated as historical fact.

====Banned in Samoa====
In Samoa, the film was banned by popular film censor Leiataua Oloapu. Oloapu stated that he was banning the film because it was "critical of the Catholic Church" and so as to "avoid any religious discrimination by other denominations and faiths against the Church." The Samoa Observer remarked that Oloapu himself is Catholic.  The Censorship Board had previously banned the film The Da Vinci Code,    for being "contradictory to Christian beliefs." 

===Critical response===
The film received mixed reviews from critics. Review aggregation website Rotten Tomatoes reported that only 37% of 246 critics have given the film a positive review, with an average rating of 5.1/10.  The sites general consensus is that "Angels and Demons is a fast-paced thrill ride, and an improvement on the last Dan Brown adaptation, but the storyline too often wavers between implausible and ridiculous, and does not translate effectively to the big screen."  Metacritic has a rating score of 48 out of 100 based on 36 reviews.   BBC critic Mark Kermode criticized the films "silliness", saying "Whereas the original movie featured Hanks standing around in darkened rooms explaining the plot to anyone who was still awake, this second salvo cranks up the action by having Tom explain the plot while running - a major breakthrough." 
 Rolling Stone gave the film a 2.5/4 stars claiming "the movie can be enjoyed for the hell-raising hooey it is."  Joe Morgenstern of The Wall Street Journal gave the movie a mixed review claiming the film "manages to keep you partially engaged even at its most esoteric or absurd." 

Neil Smith from   awarded it 3 out of 5 stars, stating: "every supporting character acts like an unhelpful idiot to keep the plot stirring, while yet again a seemingly all-powerful conspiracy seems to consist of two whole evil guys." 

===Box office===
Overseas Angels & Demons maintained the #1 position for the second weekend as well even with the release of   which opened at #2. The film opened with $46 million at the domestic box office. The Da Vinci Code had opened domestically to $77.1 million, but the sequels opening met Columbia Pictures $40–50 million prediction, since the films source material was not as popular as its predecessors. Within more than a month, the film grossed $478,869,160 worldwide, making it the largest grossing film of 2009 until it was surpassed by  .   Of this $478 million, just over 27% of it is from domestic venues, giving the film high worldwide totals, with over $30 million in the UK, $21 million in Spain, $13 million in Brazil, $13 million in Russia, $34 million in Japan and $47 million in Germany.  Angels & Demons was the ninth-highest-grossing film of 2009, with box-office figures of $485,930,810 worldwide. 

==See also==
* Conflict thesis|Draper&ndash;White thesis
* Particle accelerators in popular culture

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

{{Navboxes|list1=
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 