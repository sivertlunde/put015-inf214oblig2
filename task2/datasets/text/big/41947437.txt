The Witch's Cradle
 
 
The Witchs Cradle is an unfinished, silent short film written and directed by Maya Deren and released in 1943. 

==Synopsis==
The surrealist film shows repetitive imagery involving a string fashioned in a bizarre, almost spiderweb-like pattern over the hands of several individuals, most notably an unnamed young woman (Pajorita Matta) and an elderly gentleman (Marcel Duchamp). It also shows a shadowy darkness and people filmed at odd angles, an exposed human heart, and other occult symbols and ritualistic imagery which evokes an unsettling and dream-like aura.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 