Sunset Trail
{{Infobox film
| name           = Sunset Trail
| image          = Sunset Trail poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lesley Selander
| producer       = Harry Sherman 
| screenplay     = Norman Houston  William Boyd Robert Fiske Kenneth Harlan
| music          = Gerard Carbonara
| cinematography = Russell Harlan
| editing        = Robert B. Warwick Jr. 
| studio         = Harry Sherman Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, Robert Fiske and Kenneth Harlan. The film was released on February 24, 1939, by Paramount Pictures.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*George "Gabby" Hayes as Windy Halliday 
*Russell Hayden as Lucky Jenkins
*Charlotte Wynters as Ann Marsh
*Jan Clayton as Dorrie Marsh  Robert Fiske as Monte Keller
*Kenneth Harlan as John Marsh
*Anthony Nace as Henchman Steve Dorman
*Kathryn Sheldon as Abigail Snodgrass
*Maurice Cass as E. Prescott Furbush 
*Alphonse Ethier as Superintendent
*Glenn Strange as Bouncer
*Claudia Smith as Mary Rogers

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 