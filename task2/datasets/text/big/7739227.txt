The Heart of the Matter (film)
{{Infobox film
| name = The Heart of the Matter
| image =
| caption =
| director = George More OFerrall
| producer = Ian Dalrymple
| writer = Lesley Storm Ian Dalrymple Graham Greene (The Heart of the Matter|novel)
| starring = Trevor Howard Elizabeth Allan Maria Schell Denholm Elliott Gérard Oury Peter Finch George Coulouris
| music =
| cinematography = Jack Hildyard
| editing =
| distributor = British Lion Films
| released = 1953
| runtime =
| country = UK English
| budget =
| gross = £123,479 (UK) 
| preceded_by =
| followed_by =
}}
 1953 British British film the book of the same name by Graham Greene. It was directed by George More OFerrall for London Films. It was entered into the 1953 Cannes Film Festival.   

==Plot, cast and production== Catholic faith leaves him tormented with guilt (emotion)|guilt.

The film also stars Denholm Elliott, Peter Finch, Gérard Oury, George Coulouris and Michael Hordern.
 original score, location filming took place. The interiors were filmed at Shepperton Studios in London. It was lensed in black and white by noted cinematographer Jack Hildyard.

==Differences between film and book==
The main difference between the film and the book is in the ending, which is almost equally bleak, but reversed from Greenes original story. In the book, Scobies servant is killed (apparently an act of revenge by Yusef, here played by Gérard Oury). Scobie commits suicide. In the film, Scobie intends to kill himself, but is interrupted by a fight breaking out. He intervenes and is shot. The servant (John Akar) does not die, but instead Scobie dies in his servants arms.

==References==
 

==External links==
*  
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 