King of the Pecos
 
{{Infobox film
| name           = King of the Pecos 
| caption        = Film poster
| image          = King of the Pecos FilmPoster.jpeg
| director       = Joseph Kane
| producer       = Trem Carr
| writer         = {{plainlist|
*Bernard McConville
*Dorrell McGowan
*Stuart E. McGowan
}}
| starring       = {{plainlist|
*John Wayne
*Muriel Evans
}}
| cinematography = Jack A. Marta
| editing        = Lester Orlebeck
| released       =  
| runtime        = 54 minutes
| country        = United States
| language       = English
}}
 Western film starring John Wayne and Muriel Evans.

==Cast==
* John Wayne as John Clayborn
* Muriel Evans as Belle Jackson
* Cy Kendall as Alexander Stiles
* Jack Clifford as Henchman Ash
* Arthur Aylesworth as Hank Mathews  Herbert Heywood as Josh Billings
* J. Frank Glendon as Lawyer Brewster  Edward Hearn as Eli Jackson John Beck as Mr. Clayborn
* Mary MacLaren as Mrs. Clayborn
* Bradley Metcalfe as Little John
* Yakima Canutt as Henhcman Pete

==See also==
* John Wayne filmography

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 