Sunglass (film)
{{Infobox film
| name           = Sunglass
| image          = Sunglass poster.jpg
| caption        = Theatrical poster
| director       = Rituparno Ghosh
| producer       = Arindam Chowdhuri
| screenplay     = Rituparno Ghosh
| narrator       = 
| starring       = R. Madhavan Tota Roy Chowdhury Konkona Sen Sharma Raima Sen Jaya Bachchan Naseeruddin Shah
| music          = Sanjoy Dazz Raja Narayan Deb
| cinematography = Abhik Mukhopadhyay
| editing        = Arghyakamal Mitra 
| studio         = Planman Motion Pictures
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Bengali Hindi
| budget         = 
| gross          = 
}}

Sunglass (Taak Jhaank in Hindi) is a 2013 bilingual Indian film written and directed by Rituparno Ghosh. The film is a satirical comedy about the seven-year itch between a husband (R. Madhavan|Madhavan/Tota Roy Chowdhury) and wife (Konkona Sen Sharma) and how their life turns topsy-turvy with the entry of what seems to be an ordinary pair of antique sunglasses. The film premiered at the 19th Kolkata International Film Festival in 2013.

==Cast==
* R. Madhavan as Sanjay (Hindi version)
* Tota Roy Chowdhury as Sanjay (Bengali version)
* Konkana Sen Sharma
* Raima Sen
* Naseeruddin Shah
* Jaya Bachchan

==Production==
Konkana Sen Sharma signed on to play the leading female role in the Hindi and Bengali versions of the film in June 2006 and revealed that director Rituparno Ghosh had originally approached  Aparna Sen with the script in the 1990s. Arshad Warsi was initially selected to play the lead role in the Hindi version of the film but walked out demanding higher remuneration and was replaced by Sanjay Suri.  Tota Roy Chowdhury signed on to play the lead role in the Bengali version of the film, and along with Suri, began attending final script reading sessions with Ghosh.  Jaya Bachchan was added to the cast to play Konkanas mother, while Raima Sen agreed to play a guest role in the films.  Suri then opted out of the project noting that he had "reservations about the character" and was subsequently replaced in the films Hindi version by R. Madhavan|Madhavan. 

Madhavan shot for the film simultaneously in late 2006 alongside Mani Ratnams Guru (2007 film)|Guru (2007), and expressed his joy at working alongside Ghosh and Bachchan for the first time.  The entire team charged little fees for the project, with Konkana rejecting the opportunity to have a vanity van on the sets.  The films shoot was delayed for a month due to Bachchans illness, though scenes for both versions were subsequently shot rapidly in Calcutta in October 2006.  By November 2006, the Hindi version of the film was reported to be "ready for release".  However the film failed to get a theatrical release, with the films cast and director regularly stating that the film was completed. 

The films director Rituparno Ghosh died following a heart attack in May 2013 and prior to his death had cited that he was not bothered whether Sunglass would release or not.  As a tribute to the director, the film was selected to premiere at the 19th Kolkata International Film Festival in 2013 and was subsequently censored and retitled as Taak Jhaank for the Hindi version.  Both versions were screened and were well received at the film festival and industry pundits expressed their desire at the films being released in theatres soon afterwards. However, technicians from the film including editor Arghyakamal Mitra and cinematographer Abhik Mukhopadhyay stated that they had yet to receive any remuneration for their work in the film, further hampering chances of a theatrical release. Similarly the films original composer duo Sanjoy Dazz and Raja Narayan Deb, both previously of 21 Grams, expressed their confusion at how the re-recording eventually was completed. Shubho Shekhar Bhattacharjee, who creatively executed Ghoshs film had coordinated with several festivals including KIFF to have it screened and remains hopeful of a theatrical release in 2014. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 