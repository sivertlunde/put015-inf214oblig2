Love Bite
{{Infobox film
| name           = Love Bite
| image          = Love bite poster.jpg
| caption        = UKs Theatrical released   poster
| director       = Andy De Emmonyy
| producer       = Joni Levin
| screenplay     = Cris Cole   Ronan Blaney
| starring       = Ed Speleers Jessica Szohr Timothy Spall Luke Pasqualino Kierston Wareing
| music          = Burkhard Dallwitz
| cinematography = Tat Radcliffe
| editing        = Matt Platts-Mills
| studio         = Spitfire Pictures Imagenation Abu Dhabi Film Fund Luxembourg
| distributor    = Newmarket Films
| released       =  
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         = £2,500,000 (estimated)
| gross          = $201,794 
}}
Love Bite is a British comedy horror film distributed by Newmarket Films. It is directed by Andy De Emmony and is based on a screenplay by Cris Cole and Ronan Blaney. The film revolves around a mysterious Traveller girl who was suspected of being a werewolf. The film stars Ed Speleers, Jessica Szohr, Luke Pasqualino and Timothy Spall. The film was released on 9 November 2012 

Four young lads set off to get laid in order to stay alive. However, the boys have to attract the girls first in order to complete their mission. Otherwise, if remaining a virgin they will be killed by wolves.

== Cast ==
*Jessica Szohr as Juliana
*Ed Speleers as Jamie
*Timothy Spall as Sid
*Luke Pasqualino as Kevin
*Kierston Wareing as Natalie
*Imogen Toner as Mandy
*Robert Pugh as Sergent Rooney
*Paul Birchard as Reverend Lynch
*Adam Leese as Malik
*Ben Keaton as Father John
*Robin Morrissey as Bruno
*Daniel Kendrick as Spike

==Production==
Shooting for Love Bite took place in Clacton-on-Sea, Essex, Glasgow, Largs, Millport, Cumbrae and near North Berwick, East Lothian.    Filming started on 15 September 2011.

==Release==
The film was released on 9 November 2012.

==References==
 

==External links==
*  at IMDb

 
 
 
 
 
 
 
 

 