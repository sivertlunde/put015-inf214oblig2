Muita Calma Nessa Hora 2
{{Infobox film
| name           = Muita Calma Nessa Hora 2
| image          = Muita Calma Nessa Hora 2.jpg
| caption        = Theatrical release poster
| director       = Felipe Joffily
| producer       = 
| writer         = Bruno Mazzeo   Lusa Silvestre
| starring       = Bruno Mazzeo   Marcelo Adnet   Fernanda Souza   Andréia Horta
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Downtown Filmes Paris Filmes
| released       =  
| runtime        = 
| country        = Brazil
| language       = Portuguese
| budget         =
| gross          = 
}}
Muita Calma Nessa Hora 2 is a 2014 Brazilian comedy film directed by Felipe Joffily, written by Bruno Mazzeo and Lusa Silvestre, and starring Mazzeo, Marcelo Adnet, Fernanda Souza and Andréia Horta. It is a sequel of the 2010 film Muita Calma Nessa Hora.

==Plot==
Three years after the trip to Búzios, four friends meet in Rio de Janeiro. Estrella (Deborah Lamm) just got back from Argentina, Aninha (Fernanda Souza) is uncertain with the appointment of a seer, Tita (Andréia Horta) has returned from Europe in search of a job as photographer, and Mari (Gianne Albertoni) is working on production of a music festival. Together again, they will embark on new adventures. 

==Cast==
 
* Andréia Horta as Tita

* Fernanda Souza as Aninha

* Gianne Albertoni as Mari

* Débora Lamm as Estrella

* Marcelo Adnet as Augusto Henrique

* Bruno Mazzeo as Renan

* Heloísa Périssé

* Nelson Freitas

* Alexandre Nero

* Maria Clara Gueiros

* Daniel Filho

* Nizo Neto

* Lucio Mauro Filho

* Alexandra Richter

* Paulo Silvino

* Marcelo Tas

* Marco Luque

* Hélio de LaPeña

* Luis Lobianco

* Raphael Infante
 

==References==
 

==External links==
*  



 
 
 
 
 
 