Mr. Romeo
{{Infobox film
| name           = Mr. Romeo
| image          =  Album-mrromeo-cover.jpg
| alt            =
| caption        = Film poster
| image size     = 220px
| director       = K. S. Ravi
| producer       =  
| writer         =  
| screenplay     = K. S. Ravi
| story          = K. S. Ravi
| starring       =  
| music          = A. R. Rahman
| cinematography = V. Manikandan
| editing        =  B. Ramesh Super Good Films
| distributor    = Super Good Films
| released       =  
| runtime        = 162 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          =
}}
 Vijayakumar play supporting roles, while A. R. Rahman scored music for the film.  The film opened in November 1996 to moderate response at the box office.

== Plot ==
Romeo lives a privileged life as a celebrity with his widowed mother, in Bangalore, India. He receives considerable assistance and encouragement from Raj Mohan to dance and sing. He meets and is attracted to Madhoo, who is also his fan. His mother meets with Madhoos grandfather and both arrange their wedding. Then Romeo finds out that Raj Mohan has been stealing body parts, especially kidneys, from patients in the Satya Moorthy Hospital, and then getting them killed via bus accidents. He meets with Raj and warns him that he is going to expose him publicly. Before he could that Romeo is shot, taken by ambulance, and thrown off of a cliff. His friend, Prakash, takes the blame; he is arrested, tried in Court, found guilty, and sentenced to life imprisonment. But Romeo survives, is looked after by tribal people, and several months later returns to Bangalore to expose Raj - only to find a look-alike, a Chandaal (Hindu Undertaker) named Madras, from Bombay, has taken over his life, identity, love, and is residing in his very own home!

== Cast ==
* Prabhu Deva as Madras and Romeo
* Madhoo as Madhoo
* Shilpa Shetty as Shilpa
* Vadivelu as Vavval Vijayakumar as DGP Vijayakumar
* Surendra Pal as Sathyamoorthy
* Venniradai Moorthy as Madhus grandfather
* Delhi Ganesh as Romeos father
* Madhan Bob as Sabapathy Mohan Ram
* Crazy Mohan

==Production==
Prabhu Deva signed the film after securing a blockbuster with S. Shankar|Shankars Kaadhalan (1994) and demanded   for his work in the film. North Indian actress Shilpa Shetty was roped in to star in her first Tamil film, while Madhoo, who appeared in Mani Ratnams Roja (film)|Roja and Shankars Gentleman (1993 film)|Gentleman was signed on to play second lead. The producer of the film asked the director, K. S. Ravi to be credited simply as Ravi in the film, to avoid confusion with another director K. S. Ravikumar.    Mitchell camera weighed 50 kg was used for all the “double action” shots. 

==Release== Love Birds.      

A.R. Rahman played a role in recommending the director K. S. Ravi to work in En Swasa Katre, a production of Rahmans acquaintances. Mr. Romeo was also belatedly dubbed and released in Telugu and Hindi under the same title.   

== Soundtrack ==
{{Infobox album |
  Name        = Mr. Romeo|
  Type        = Soundtrack |
  Artist      = A. R. Rahman |
  Cover       = Mr.Romeocover.jpg |
  Background  = Gainsboro |
  Released    = 1996 |
  Recorded    = Panchathan Record Inn |
  Genre       = Soundtrack |
  Length      =  | Pyramid |
  Producer    =  A. R. Rahman|
  Reviews     =  |
  Last album  =  Fire (1996 film)|Fire (1996)|
  This album  = Mr. Romeo (1996) |
  Next album  =  Anthimanthaarai (1996)|
}}
 Vaali and Pyramid AV International. The lyrics for the Hindi version was penned by P. K. Mishra. The lyrics for the Telugu version was penned by Bhuvanachandra and Vennalakanti

; Original Version
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track# !! Song !! Artist(s)
|-
| 1
| "Romeo Aatam Potal"
| Hariharan (singer)|Hariharan, Udit Narayan
|-
| 2
| "Yar Adhu?"
| Noel James, Anto, Srinivas (singer)|Srinivas, Chandran
|-
| 3
| "Mel Isaiyae" Sujatha
|-
| 4
| "Thaneerai Kaadhalikum"
| Sangeetha Sajith
|-
| 5
| "Muthu Muthu Mazhai" Chorus
|-
| 6
| "Mona Lisa"
| Malaysia Vasudevan, Nagoor Mohammad Ali
|}

; Hindi Version (Mr. Romeo)
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track# !! Song !! Artist(s)
|-
| 1
| "Romeo Teri Kismat"
| Hariharan (singer)|Hariharan, Udit Narayan
|-
| 2
| "Paas Aaja Baalam" Chorus
|-
| 3
| "Kaun Hai Yeh Apsara"
| Noel James, Anto, Srinivas (singer)|Srinivas, Chandran
|-
| 4
| "Mona Lisa"
| Sonu Nigam, Raqueeb Alam
|-
| 5
| "Machli Paani Bina"
| Kavita Krishnamurthy
|-
| 6
| "Mil Hi Gayi"
| Swarnalatha, S. P. Balasubrahmanyam, Sadhana Sargam
|}

; Telugu Version (Mr. Romeo)
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track# !! Song !! Artist(s)
|-
| 1
| "Romeo Natyam Chesthe"
| Hariharan (singer)|Hariharan, Udit Narayan
|-
| 2
| "Mallikale Naa Aashala" Sujatha
|-
| 3
| "Evaradhi Evaradhi"
| Noel James, Anto, Srinivas (singer)|Srinivas, Chandran
|-
| 4
| "Mona Lisa"
| Malaysia Vasudevan, Vandemataram Srinivas
|-
| 5
| "Egireti Aashalen Dhole"
| Sangeetha Sajith
|-
| 6
| "Arere Rang Dhole" Chorus
|}

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 