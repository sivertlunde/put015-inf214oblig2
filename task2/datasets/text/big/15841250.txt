Long Pants
{{Infobox film
| name           = Long Pants
| image          = LongpantsPoster.jpg
| caption        = Theatrical Poster
| director       = Frank Capra
| producer       = Harry Langdon
| writer         = Screenplay: Robert Eddy  
| starring       = Harry Langdon Gladys Brockwell
| cinematography = Glenn Kershner Elgin Lessley
| editing        = Harry Langdon
| distributor    = First National Pictures Kino Video
| released       =  
| runtime        = 60 minutes
| country        = United States English intertitles
}} American comedy silent film starring Harry Langdon and directed by Frank Capra.  Additional cast members include Gladys Brockwell, Alan Roscoe, Priscilla Bonner, and others. 

==Plot==
The silent tells the story of Harry Shelby (Langdon) who has been kept in knee-pants for years by his mother.  One day, however, Harry finally gets his first pair of long pants.

Immediately, his family expects him to marry his childhood sweetheart Priscilla (Priscilla Bonner).  Yet, Harry soon falls for Bebe Blair (Alma Bennett), a femme fatale from the big city who has a boyfriend in the mob.

Harry thinks that Bebe is interested in him as well, so he risks everything when Bebe ends up in jail. This leads to a lot of trouble for Harry. Throughout the whole ordeal Priscilla waits for Harry to face reality.

==Cast==
* Harry Langdon as Harry Shelby
* Gladys Brockwell as His Mother
* Alan Roscoe as His Father
* Priscilla Bonner as Priscilla, His Bride
* Alma Bennett as Bebe Blair, His Downfall
* Betty Francisco as His Finish

==Critical reception== Charles Spencer Chaplins sincerest flatterer. His short coat reminds one of Chaplin, and now and again his footwork is like that of the great screen comedian." 

Film historian David Kalat reports that Buster Keaton, a long-time fan of Langdons known for his own morbid jokes about death and killings, criticized a scene in which Langdons character tries to kill Priscilla as "going too far" in making light of murder. 

More recently, critic Maria Schneider reviewed Langdons work and wrote, "Long Pants (1927), also directed by Capra, was a peculiar change of pace for Langdon, and possibly an attempt to poke fun at his baby-faced image by casting him as a would-be lady-killer; sporting little of the ingenuity of The Strong Man, it was a box-office failure that set off the comedians quick decline into obscurity. An acquired taste, Harry Langdons gentle absurdities and slow rhythms take some getting used to, but patient viewers will be rewarded." 

Film critic Hal Erikson wrote of the film, "Few comedies of the 1920s were as bizarre and surreal as Harry Langdons Long Pants...Written by future director Arthur Ripley, Long Pants is as kinky as any of Ripleys film noirs of the 1940s. Long Pants represents the second and final collaboration between star Harry Langdon and director Frank Capra, who was fired when Langdon wrong-headedly decided to become his own director, resulting in a series of career-destroying flops." 

==See also==
* List of United States comedy films

==References==
 

==External links==
*  .
*  .
*  .
*   images at Silents are Golden.
*  .

 

 
 
 
 
 
 
 