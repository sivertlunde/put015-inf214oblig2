Grant Morrison: Talking with Gods
 
{{Infobox film
| name = Grant Morrison: Talking with Gods
| image =
| alt =
| caption =
| director = Patrick Meaney
| producer = Julian Darius F. J. DeSanto Patrick Meaney Mike Phillips Jordan Rennert Amber Yoder
| writer =
| music =
| cinematography = Jordan Rennert
| editing = Patrick Meaney
| studio = Sequart Organization, Respect! Films
| distributor = Halo-8 Entertainment
| released =  
| runtime = 80 minutes
| country = United States
| language = English
| budget =
| gross =
}}
Grant Morrison: Talking with Gods is a feature-length documentary that takes an in depth look at the life, career and mind of the Scottish comic book writer Grant Morrison. Talking with Gods features interviews with Morrison and many his most collaborators, such as artists, editors and other industry professionals.

==Background==
 Sequart and Respect Films. The film grew out of Our Sentence is Up: Seeing The Invisibles, a book length exploration of Morrisons seminal comic book series.  After completing the book, Sequart pitched Morrison on the idea of a documentary chronicling his life and work. He agreed, and filming began in April 2009. Over the next year, the filmmakers traveled to Los Angeles, New York, London and Glasgow to interview Morrisons friends and collaborators. 
 Steve Cook and many others. The film also features interviews with Morrisons collaborators Geoff Johns and Mark Waid, as well as counterculture personalities like Richard Metzger and Douglas Rushkoff. The actress Amber Benson is interviewed briefly on the subject of Morrisons comic series, We3.

==Plot==

The film tracks chronologically through Morrisons life, emphasizing the connections between his life and writing. It follows Morrison as he moves from a shy and sometimes depressed teenager, through his years as a comic book rock star in the 1990s, and ultimately to a happy and well adjusted life as he looks to the future.

==Release==

News broke in July 2010 that indie film distributor Halo-8 Entertainment had picked up the film for a November 2010 release at New York Comic-Con, followed by a theatrical release run. 

The film premiered to general critical acclaim. It is currently available on DVD, and also free to stream via   (in North America), as well as  . The film was followed by a similarly themed documentary on  .  The same team is currently working on The Image Revolution, a documentary history of Image Comics.

==See also==
* The Mindscape of Alan Moore
*  

==References==
 
 

==External links==
*  
*  
*   on producer Sequarts site
*   on Documentary Storm
*  
*  —The Sci-Fi Block
*  —Matinee Idles

 

 
 