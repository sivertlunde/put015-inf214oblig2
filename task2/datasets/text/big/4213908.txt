Hercules and the Lost Kingdom
 
{{Infobox Television episode|
 Title= Hercules and the Lost Kingdom |
 Series= |
 Image=|
 Caption=|
 Season=0|
 Episode=H02|
 Airdate=May 2, 1994|
 Production= |
 Writer=Christian Williams|
 Director=Harley Cokeliss|
 Guests=Renée OConnor (Deianira|Deianeira) Robert Trebor (Waylin) Nathaniel Lees (Blue Priest)|
 Prev=Hercules and the Amazon Women|
 Next=Hercules and the Circle of Fire|
}}
Hercules and the Lost Kingdom is the second TV movie in the series  .

==Overview==
Hercules comes to the aid of a young woman who is seeking the lost city of Troy. Eventually, Hercules leads her to a camp of refugees from the city, which has been taken over by Heras Blue Priests. Hercules helps the refugees take back the city.

==Plot synopsis==
 giant is coming. The giant follows the woman to the village and rips the roof of the tavern. Hercules presents himself to the giant and the giant challenges Hercules to a fight. Hercules goes outside and he beats the giant. As the village celebrates the defeat the man from earlier in the film arrives in the village, he asks for Hercules help. He explains to Hercules that his people have been driven from their city, the lost city of Troy.
 virgin to their water god. Hercules saves her from being sacrificed, while unknowingly being watched by a mysterious robed figure. The woman tells Hercules that her name is Deianira|Deianeira. Later in the evening as they camp for the night, Deianeira tells her a story that she is the daughter of a King. She sees the dark figure and asks who he is and why she is being followed, but the figure does not answer. Later Hercules and Deianeira arrive at the slave market looking for Queen Omphale of Lydia, the last person to have possessed the compass. In order to get to see the Queen, Hercules sells himself as a slave, and the Queen buys him. After spending the night with the Queen Hercules gets the compass and he and Deianeira continue searching for Troy. Deianeira gets attacked by some men but the figure in the dark robe saves her and tells her to follow her destiny.

Hercules and Deianeira head on to Troy, they arrive at the ocean and the figure standing on the cliffs summons a sea serpent to do Heras bidding. It swallows Hercules and Deianeira. Hercules kills the monster from inside and they are washed up on the shore. Deianeira sees Troy in the distance and tells Hercules that she now remembers and this is where she is from. Deianeira and Hercules get caught in a trap and taken to the Laomedon|king. The king is ill and he and his daughter are reunited. He tells her that the Cult of the Blue Priests have taken over the city and the people have taken refuge in the woods, he tells Deianeira to rule them well, and then dies.

Hercules tutors the people of Troy and prepares them to battle to take back Troy. Deianeira realises that the people cannot beat the Cult of the Blue Priests and goes looking for the Blue Priest. The people notice that Deianeira is missing and Hercules and the people use an underground tunnel to get into the city. As the people fight the cult members Hercules goes looking for Deianeira, he finds her about to be sacrificed to Hera and saves her. The Blue Priest and Hercules fight and Hercules cuts off the Priests head. A huge storm approaches, and Zeus tells Hercules that Hera is coming for Deianeira, Hercules saves her and Hera takes Hercules instead. As Deianeira is crowned Queen of Troy, we see Hercules thrown down from out of the sky, a man approaches him asking him for help, Hercules agrees and the two men walk off to the next adventure.

==Rating information==

*First Airing: Unknown
*Second Airing: 5.8

==Guest roles==

*Waylin (Robert Trebor) was originally intended to become a recurring character in the television series, but the producers felt that the character runs its course in the movie and had limited potential after, so the character of money-making Salmoneus was created for Trebor instead.
*This is Reneé OConnors first appearance in the shared Hercules-Xena universe
*Elizabeth Hawthorne, who played Queen Omphale, went on to play Alcmene|Hercules mother in the TV series.
* ,   and Young Hercules.

==Main cast==

*Kevin Sorbo as Hercules
*Renée OConnor as Deianeira
*Anthony Quinn as Zeus

==Notes== Trojan King Laomedons daughter is Hesione.
*In "Hercules and the Circle of Fire", it is apparent that Hercules and Deianeira were meeting for the first time.

==External links==
* 

 
 

 
 
 
 
 
 