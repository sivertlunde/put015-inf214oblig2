Why Would I Lie?
{{Infobox film
| name           = Why Would I Lie?
| image          = Why Would I Lie.jpg 
| image_size     =
| caption        =
| director       = Larry Peerce
| producer       = Pancho Kohner Shelly Abend Stuart Wallach Rich Irvine James L. Stewart
| writer         = Peter Stone Hollis Hodges (novel)
| narrator       = Gabriel Swann Susan Heldfond Anne Byrne Hoffman Valerie Curtin Jocelyn Brando Nicolas Coster Charles Fox
| cinematography = Gerald Hirschfeld
| editing        = John C. Howard
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        =
| country        = United States English
| budget         =
| gross          = $1,175,855
}}
 1980 American comedy/drama film about a compulsive liar named Cletus (Treat Williams).  The film, which was directed by Larry Peerce and shot in Spokane, Washington, is based on the novel The Fabricator by Hollis Hodges.

==Plot==
Cletus, a compulsive liar, is employed as a social worker. He tries to find a home for a young boy named Jorge and, in so doing, falls in love with Jorges social worker.

==Cast==
* Treat Williams as Cletus
* Lisa Eichhorn as Kay
* Gabriel Macht as Jorge (credited as Gabriel Swann)
* Valerie Curtin as Mrs. Bok

==Reception==
  mold. It never becomes clear quite what he is, though. And before the audience even has time to get used to him, he has become involved in a convoluted plot that probably worked better on the page than it does on the screen. ...As directed by Larry Peerce, Why Would I Lie? isnt often funny, especially since Cletuss tall tales generally have a macabre ring. ...Mr. Williams can be charming, but he has none of the whimsical nature that might make Cletuss exploits believable." 

==Awards==
*Nominee Best Comedy Picture - Young Artist Awards
*Nominee Best Young Actor - Young Artist Awards (Gabriel Macht)

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 


 