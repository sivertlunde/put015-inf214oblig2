Chetniks! The Fighting Guerrillas
{{Infobox film
| name           = Chetniks! The Fighting Guerrillas
| image          = Chetniks guerillas poster.jpg
| image size     = 
| alt            = 
| caption        = Lobby card
| director       = Louis King
| producer       = Bryan Foy Sol M. Wurtzel
| writer         = Jack Andrews (story)
| screenplay     = Jack Andrews Edward E. Paramore Jr.
| story          = Jack Andrews
| narrator       = 
| starring       = Philip Dorn Anna Sten Shepperd Strudwick Virginia Gilmore Martin Kosleck
| music          = Hugo Friedhofer
| cinematography = Glen MacWilliams
| editing        = Alfred Day
| studio         = 20th Century Fox
| distributor    = 
| released       =  
| runtime        = 73 min 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Chetniks! The Fighting Guerrillas is a war film made by Twentieth Century Fox in 1943. The film starred Philip Dorn, Anna Sten, and Martin Kosleck. 

It was originally titled The Seventh Column, was directed by  , Yugoslav guerilla leader." The movie appears in the American Film Institute (American Film Institute|AFI) catalogue for American feature films made between 1941–1950. 

The movie was advertised in an original print ad as follows:
 "Announcing -- The most stirring picture released this year! Thrill follows thrill in this living drama...that flames out of todays electrifying headlines! This very moment...a Nazi troop train is being destroyed...! Live, love, fight with Draja Mihailovitch and his fighting guerrillas." 

==Plot==
In the opening scene, German troops and tanks are shown invading the Kingdom of Yugoslavia while bombers attack the capital Belgrade. When the Germans, Italians, Hungarians, and Bulgarians invade Yugoslavia on 6 April 1941, Serbian army colonel Draža Mihailović forms a band of guerrillas known as the Chetniks, who launch a resistance movement against the Axis occupation. Mihailovićs forces then engage in an attack on the German and Italian forces, forcing them to employ seven Axis divisions against them.

The Chetniks capture an Italian supply convoy. Mihailović then radios German headquarters in the nearby coastal town of Kotor in Montenegro and offers to exchange Italian POWs for gasoline. Infuriated, general Von Bauer refuses, but when Mihailović threatens to notify the Italian High Command of his decision, Gestapo colonel Wilhelm Brockner orders Von Bauer to comply.

Brockner, who has been unable to capture Mihailović, is convinced that the Yugoslav leaders wife Ljubica and their two children, Nada and Mirko, are hiding in Kotor. He plans to use them as hostages to blackmail Mihailović into surrendering. Brockner warns the townspeople that anyone caught aiding the Mihailović family will be executed, and prepares the deportation of 2,000 men from Kotor to Nazi Germany.

Brockners secretary Natalia, however, is a spy for the Chetniks and is in love with Alexa, one of Mihailovićs aides. Forewarned by Natalias information, the Chetniks attack the train transporting the two thousand prisoners and free them. In retaliation, Brockner decrees that no food will be distributed to the citizens of Kotor until Lubitca and her children are turned over to the Germans. Lubitca tries to surrender to Brockner but is stopped by Natalia, after which Mihailović asks to meet with Von Bauer and Brockner.

After Mihailović arrives at German headquarters, however, Von Bauer declares that, since the official Yugoslav government had capitulated, international law does not prevent him from killing Mihailović, even though they are meeting under a flag of truce. Mihailović then reveals to the general that the Chetniks are holding his wife and daughter as hostages, as well as Brockners mistress, and that they will be executed unless the citizens of Kotor are given food. The general angrily releases Mihailović and provides rations for Kotor.

Mihailovićs son Mirko, demonstrating his patriotism, betrays his true identity to his German schoolteacher. After taking Mirko into custody, Von Bauer and Brockner escort Ljubica to Mihailovićs mountain stronghold and then inform him that every man, woman, and child in Kotor would be executed unless the Chetniks surrender within 18 hours.

Mihailović informs Ljubica that he cannot surrender. She then returns to Kotor to comfort their children. Mihailović immediately organizes a plan of attack and sends some of his men to the mountain pass to Kotor, where they trick the Germans into thinking that they are surrendering, while the rest of the Chetniks attack the town from the mountains on the other side.

Even though Aleksa, who was assigned to infiltrate the German artillery, is taken prisoner by the Germans, Mihailovićs plan succeeds. After an intense battle, the Chetniks gain control of Kotor and free all of the hostages, including Mihailovićs family.

In the final scene, Mihailović broadcasts a radio message to his fellow Yugoslavs that the guerrillas will continue fighting until they have regained complete freedom for their people and driven out the invading Axis troops.

==Critical Reception==
 The New York Times reviewed the movie favorably on March 19, 1943 after it was shown in New York at the Globe in a review by “T.M.P.”, Thomas M. Pryor. Pryor wrote that the movie was “splendidly acted” and that it had “the right spirit”. 
{{cite news
|title=Chetniks - The Fighting Guerrillas (1943) At the Globe 
|first=T.M.P. 
|newspaper=The New York Times
|date=March 19, 1943  
|url=http://movies.nytimes.com/movie/124425/Chetniks-The-Fighting-Guerrillas/overview
}} 

Hal Erickson of All Movie Guide (AMG) reviewed the movie favorably also, describing how Draža Mihailović was vindicated and exonerated by events after the war. Erickson wrote that the movie portrayed Draža Mihailović as “a selfless idealist, leading his resistance troops, known as the Chetniks, on one raid after another against the Germans during WWII.” 
{{cite web
|url=http://www.allrovi.com/movies/movie/chetniks--the-fighting-guerrillas-v124425
|title=Chetniks - The Fighting Guerrillas
|accessdate=2011-10-22
}} 

The movie was reviewed favorably in the Los Angeles entertainment trade paper The Hollywood Reporter when released in 1943: "Seldom has Hollywood given attention to a motion picture that offered more stirring material than this first feature about a living military hero of World War II."

In a review in the Chicago Daily Tribune on April 1, 1943, "Chetniks Story Is Dramatically Told in Movie CHETNIKS", Mae Tinee wrote: "This is a fiercely satisfying picture. We all know about the Chetniks, fighting guerrillas of JugoSlavia. We devour every word we can find to read about them--and a lot of us dream of them.... Now comes the movie ..."

The movie was shown in movie theaters nationwide in the U.S. in 1943. The movie was shown at the Globe in New York City on March 18, the B & K Apollo in Chicago, the Williamsburg Theatre in Virginia on Sunday, February 21, 1943 as The Fighting Guerrillas: ‘Chetniks’, at the Stanford Theatre in Palo Alto California, and the Quilna Theatre in Lima, Ohio. The film was shown as a double feature in some theaters in 1943, paired with We Are the Marines (1942), a documentary on the U.S. Marine Corps. 

According to a story in the April 3, 1943 Boxoffice magazine, "Chicago Mayor in PA For Chetniks Debut", Chicago Mayor Edward J. Kelly attended a debut showing at the B & K Apollo theater after proclaiming "Chetnik Day" in Chicago on April 1.

==Post-War Legacy==

After the war, the movie was pulled from circulation after Mihailović was accused of war crimes and executed. 
{{cite book
|title=The Hollywood Propaganda of World War II
|first=Robert 
|last=Fyne 
|publisher=Scarecrow Press 
|year=1994 
|isbn= 978-0-8108-2900-8
|page=144}}  The movie was, however, rebroadcast on the rerun circuit in all the major television markets in Canada and the United States in the 1960s and 1970s.  The film was shown on Yugoslavian television in the early 1980s on the TV movie program 3, 2, 1... Kreni. In October, 2009, the film was featured at the Zagreb Film Festival in Croatia. Twentieth Century Fox has not released the movie on DVD as of 2014.

U.S. Navy Specialist 1st Class Arthur "Jibby" Jibilian, who saw the movie in 1943 before going to German-occupied Yugoslavia where he met Draza Mihailovich as part of Operation Halyard, reviewed the film favorably on IMDB in 2010.

==Cast==
* Philip Dorn as Col. Draža Mihailović
* Anna Sten as Ljubica Mihailović
* Shepperd Strudwick (credited as John Shepperd) as Lt. Aleksa Petrović, Mihailovićs aide
* Martin Kosleck as Gestapo Col. Wilhelm Brockner
* Virginia Gilmore as Natalia, Brockners secretary
* Felix Basch as Gen. von Bauer
* Frank Lackteen as Maj. Danilo
* LeRoy Mason as Capt. Sava
* Patricia Prest as Nada, Mihailovićs daughter
* Merrill Rodin as Mirko, Mihailovićs son
* Lisa Golm (uncredited) as Frau Spitz, a schoolteacher
* John Banner (uncredited) as Gestapo agent
* Gino Corrado (uncredited) as Italian Lieutenant
* Nestor Paiva (uncredited) as Italian Major
* Richard Ryen (uncredited) as radio announcer

==References==
 

==Sources==
*  
* Evans, Alun, editor. Brasseys Guide to War Films. Dulles, VA: Potomac Books, Inc., 2000.
*  
*  
* New York Times movie review, Movie Review, Chetniks - The Fighting Guerrillas (1943), March 19, 1943 by NYT movie critic T.M.P., Thomas M. Pryor.
* Hal Erickson review of Chetniks! The Fighting Guerrillas on All Movie Guide (AMC).
* Dick, Bernard F. The Star-Spangled Screen: The American World War II Film. Lexington, KY: University Press of Kentucky, 1985, reprinted in 1996. Chetniks! The Fighting Guerrillas is analyzed on pp.&nbsp;163–165.
* Lees, Michael. The Rape of Serbia: The British Role in Titos Grab for Power, 1943-1944. New York: Harcourt Brace Jovanovich, 1991.
* Chetniks! The Fighting Guerrillas: A Critical Reappraisal in 2008 by Carl Savich.
* "The Chetniks", Treasury Star Parade, radio recording, episode #101 starring Orson Welles and Vincent Price, produced by the U.S. Treasury Department, 1942, written by Violet Atkins, produced by William A. Bacher.
* LIFE magazine, pages 32–33, July 15, 1946, "Mihailovich Awaits the Verdict". Photo essay entitled "Mihailovich: Chetnik leader fights for his life before open Yigoslav court-martial." LIFE photographs by John Phillips.
* Smith, Richard Harris. OSS: The Secret History of Americas First Central Intelligence Agency. Guilford, CT: The Lyons Press, 2005.
* Jareb, Mario. (2006). "How the West Was Won: The Yugoslav Government-in-Exile and the Legend of Draza Mihailovich." Journal of Contemporary History, 3.
* Carl Savich|Savich, Carl. (2003).  , Serbian Unity Congress.
* Real Heroes Comics, #6, September, 1942. "Chief of the Chetniks: Draja Mihailovich." New York: Parents Magazine Institute, pages 13–18.
* Real Life Comics, #8, November, 1942, Vol. 3, No. 2, "Draja Mihailovitch: The Yugoslav MacArthur." Mew York: Nedor Publishing.
* Goulart, Ron. Ron Goularts Great History of Comic Books: The Definitive Illustrated History from the 1890s to the 1980s. NY: McGraw-Hill/Contemporary, 1986. "Draza Mihajlovic", index entry, p.&nbsp;202.
* Sava, George. The Chetniks. London, UK: Faber and Faber, Ltd., 1942.
* Tamas, Istvan. Sergeant Nikola; A Novel of the Chetnik Brigades. NY: L.B. Fischer Publishing Corporation, 1942.
* Blockbuster listing for Chetniks! The Fighting Guerrillas.
* Inks, Major James M. Eight Bailed Out. NY: Norton, 1954.
* Felman, U.S. Air Force Major Richard. Mihailovich and I. Tucson, AZ: Self-published by author, copyright, 1964. Serbian Democratic Forum, October, 1972.
* Freeman, Gregory A. The Forgotten 500: The Untold Story of the Men Who Risked All For the Greatest Rescue Mission of World War II. NAL, 2007.
* Roberts, Walter, Tito, Mihailovic and the Allies. Duke University Press, 1987.
* "The Chetniks Of Yugoslavia." The War Illustrated, Volume 6, #146, January 22, 1943.
* Deroc, Milan. British Special Operations Explored: Yugoslavia in Turmoil, 1941–1943, and the British Response. Boulder, CO: East European Monographs/New York: Columbia University Press, 1988.
* Ford, Kirk. OSS and the Yugoslav Resistance, 1943-1945. Texas A & M University Press, 1992.
* Low, Robert. "Hitlers No.1 Headache: The Story of Draja Mihailovitch - Fighter for Freedom." Liberty magazine cover, April 25, 1942, page 18.
* "Mihailovich: Yugoslavias Unconquered. He watches from his mountain walls. (World Battlefronts)."   by Vuk Vuchinich (1901–1974), Monday, May 25, 1942, Vol. XXXIX, No. 21.
* Boxoffice, May 30, 1942, page 16.
*  , Time magazine, Monday, May 25, 1942.
* Sindbaek, Tea. (2009). "The Fall and Rise of a National Hero: Interpretations of Draza Mihailovic and the Chetniks in Yugoslavia and Serbia Since 1945." Journal of Contemporary European Studies, 17, 1, 47-59.
* Ove, Torsten. "93-year-olds WWII feats are hidden no longer." Pittsburgh Post-Gazette, Sunday, November 23, 2008.
* Kurapovna, Marcia. Shadows on the Mountain: The Allies, the Resistance, and the Rivalries that Doomed World War II Yugoslavia. Hoboken, NJ: John Wiley and Sons, 2009.
* Pavelic, Boris, and Bojana Oprjan-Ilic. "USA Nevertheless Decorates Chetnik Leader Draza Mihailovic." Novi List, Rijeka, Croatia, May 10, 2005.
* "Chicago Mayor in PA For Chetniks Debut". Boxoffice, April 3, 1943, p.&nbsp;52.

==External links==
*  
*  
*  
*  , New York Times, Thomas M. Pryor
*  

 
 
 
 
 