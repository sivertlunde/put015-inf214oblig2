Rog
 
{{Infobox film
| name           = Rog
| image          = Rog Poster.jpg
| image_size     =
| caption        = 
| director       = Himanshu Brahmbhatt
| producer       = Pooja Bhatt
| writer         = Mahesh Bhatt
| story          = Mahesh Bhatt
| dialogue       = Niranjan Iyengar, Subodh Chopra    
| starring       = Irrfan Khan Ilene Hamann Himanshu Malik
| music          = M. M. Kreem
| lyrics         = Neelesh Misra, Sayeed Quadri
| cinematography = Anshuman Mahaley   
| editing        = Akiv Ali
| distributor    = Shreya Creations
| released       = January 7, 2005
| runtime        = 119 minutes
| country        = India
| language       = Hindi
}} 2005 Hindi-language thriller film directed by Himanshu Brahmbhatt, written by Mahesh Bhatt and produced by Pooja Bhatt. The film stars Irrfan Khan and former model, Ilene Hamann in the lead roles. Its plot is based on the 1944 Hollywood suspense thriller, Laura (1944 film)|Laura. 

== Plot ==
Uday Singh Rathod (Irrfan Khan) is a law-abiding police officer, famous for his extraordinary investigations, but is experiencing insomnia. When Maya Solomon (Ilene Hamann), a famous model is murdered, Rathod is given the custody of the case and he is asked to solve it within a week. Three people are shortlisted as prime suspects; Harsh (Suhel Seth), a famous journalist, Ali (Himanshu Malik), a millionaire and Shyamoli (Shyamoli Verma), his partner.

When investigation begins, Harsh offers his help to Rathod in hunting down the murderer, while drawing his attention to the fact that Maya was about to get married to Ali, but because Ali was a womanizer, he couldnt keep up with one woman. So he, together with Shyamoli, killed Maya. Rathod theorizes on these lines and goes to Alis house with Harsh.

He questions Ali and reaches Mayas house to gather further evidences on the case. While leading the investigations and running through the past of Maya, Rathod starts to fall in love with the image of the dead woman. To his shock, as he is gathering evidences, Maya appears in the house. This leaves Rathod confused as to whose dead body was found in the house and who is the killer.

All this while, Mayas simplicity and subtle beauty keeps drawing Rathod towards her. He finally makes up his mind and arrests Maya. In the interrogation room, he reveals that before the night of murder, Maya left for two days on a holiday. Ali meanwhile brought another girl in her house to have some fun. That girl was killed, but Ali wasnt involved in the killing. He tells Maya that for the lack of evidences, all suspicions go on her and that she would be prosecuted for the crime. Rathod calms for a second and tells Maya that regardless of everyones opinion, he thinks that she is innocent. He advises her to flee, saying that he can make all arrangements.

Maya seeing this helping hand and faith in the heart of an unknown person, starts falling in love with Rathod. They both get out of the interrogation room and spend the night together. In the morning, Rathod visits Mayas house one last time to recover the weapon used in the killing, believing that if it is found, things would get clear. He is able to find the weapon and then he suddenly realizes, who is the actual murderer. He goes back to the house, only to find Harsh already there trying to kill Maya. After some resistance, Rathod gets hold of Harsh.

Later it is revealed that Maya was afraid of her beauty because everyone used to insanely approach her. She was looking for trust and faith. When she met Harsh, he offered her his riches, but couldnt offer faith. She got destitute and started having an affair with Ali, who in turn was also unfaithful. Things kept on going and she was about to get married to Ali, when Harsh got into their plans and tried killing Maya, but ended up killing the other girl who was with Ali that night.

At the end, Rathod is being shown congratulated by fellow policemen, as he gazes into the eyes of Maya.   - Movietalkies.com 

== Cast ==
*Irrfan Khan as Inspector Uday Singh Rathod
*Ilene Hamann as Maya Solomon
*Himanshu Malik as Ali
*Suhel Seth as Harsh
*Ankur Desai
*Shyamoli Verma as Shyamoli
*Sridhar Gorthi
*Munish Makhija
*Denzil Smith

==Soundtrack==
{{Infobox album| 
| Name        = Rog
| Type        = soundtrack
| Artist      = M. M. Kreem
| Cover       = 
| Released    = November 2004
| Recorded    = 
| Producer     = M. M. Kreem Feature film soundtrack
| Length      = 
| Label       = Saregama-HMV 
}}
{{Album ratings
| rev1 = Bollywood Hungama
| rev1Score =    
| noprose = yes
}}

===Tracklist===
The films soundtrack is composed by M. M. Kreem with lyrics penned by Neelesh Misra and Sayeed Quadri. M. M. Kreem reused the tune of "Vellipothe Ila" from his Telugu movie Okariki Okaru for "Guzar Naa Jaaye". Songs like "Maine Dil Se Kaha" & "Khoobsoorat" were popular.

{| class="wikitable"
|-
! Track No !! Song !! Singer(s) !! Duration
|- KK || 5:11
|-
| 2 || "Guzar Na Jaaye (Duet)" || KK & Shreya Ghoshal || 5:31
|-
| 3 || "Tere Is Jahan Mein" || KK || 5:31
|-
| 4 || "Rog (Theme)" || Instrumental || 4:31
|-
| 5 || "Khoobsoorat" || Udit Narayan || 5:25
|-
| 6 || "Guzar Na Jaaye" || Shreya Ghoshal || 5:33
|-
| 7 || "Sufani" || Instrumental || 5:36
|-
| 8 || "Khoobsoorat (Version 2)" || M. M. Kreem || 5:26
|}

== References ==
 

==External links==
*  
*  

 

 
 
 
 