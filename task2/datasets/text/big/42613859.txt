Track Aduowan
{{Infobox film name       = Track Aduowan Hands up 2 image      =  caption    =  traditional = 舉起手來：追擊阿多丸 simplified = 举起手来：追击阿多丸 pinyin     = Jǔqǐ Shǒulaí Zhuījī Āduōwán }} director   = Feng Xiaoning producer   = Han Sanping
| writer    = Feng Xiaoning
| based on  =  starring  Guo Da Liu Wei music      =  cinematography = Feng Xiaoning Zheng Jie editing    =  studio     = China Film Group Corporation
|distributor= China Film Group Corporation released =   runtime = 98 minutes country = China language = Mandarin  budget   =  gross    = 
}} Hands Up!. Guo Da, and Liu Wei.   It based on the Second Sino-Japanese War. It was released in China on September 9, 2010 to commemorate the 65th anniversary of the victory of the Second Sino-Japanese War.   

==Cast==
* Pan Changjiang as the Imperial Japanese Army officer. Guo Da as the cook Guo, an underground Communist in Aduowan.
* Liu Wei as "Me"/ Mother/ Grandmother, the narrator of the film.

===Other===
* Feng Weiduo as the granddaughter of cook Guo.
* Dai Feifei as Miss Zhao, a Chinese espionage.
* Hu Xiaoguang as the tycoon.
* Yin Guohua as the interpreter of Imperial Japanese Army.
* Bao De as the captain of Eighth Route Army.
* Zhang Xiaoning as an Imperial Japanese Army soldier.
* Stephen as the captain of the United States Navy submarine.

==Release==
It was shown at the 2010 Beijing Film Festival.  

==References==
 

==External links==
*  
*  

 
 
 
 
 

 
 