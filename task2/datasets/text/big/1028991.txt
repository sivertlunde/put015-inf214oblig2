The Barber
 
{{Infobox film
| name           = The Barber
| image          = Barbermp.jpg
| image_size     =
| caption        = Promotional movie poster for the film
| director       = Michael Bafaro
| producer       = Melanie Kilgour Petros Tsaparas Evan Tylor
| writer         = Warren Low Michael Bafaro
| narrator       =
| starring       = Malcolm McDowell Jeremy Ratchford Garwin Sanford C. Ernst Harth Peter Allen
| cinematography = Adam Sliwinski
| editing        = Grace Yuen
| distributor    =
| released       =
| runtime        = 94 min.
| country        = Canada English
| budget         = United States dollar|USD$1,000,000 
| gross          =
}} psychopath and the minds of ordinary people who are fascinated by them. It tells the story of local barber (and serial killer) Dexter Miles (Malcolm McDowell) in a town in Alaska.  The geographic location features 24-hour darkness, which serves as a metaphor for psychological darkness that drives Miles to go on a murderous rampage.

The movie blends the genres of horror, thriller, psychological study, and occasional black comedy. Miles expresses the essence of the movie in these words:
 

==Cast==
*Dexter Miles &ndash; Malcolm McDowell
*Chief Vance Corgan &ndash; Jeremy Ratchford
*Agent Crawley &ndash; Garwin Sanford
*Buffalo Sedwick &ndash; C. Ernst Harth

==Awards==
The Barber was nominated for nine awards from the Directors Guild of Canada and the Leo Awards: 

==Locations==
This movie was filmed largely in the small town of Revelstoke, British Columbia in Canada.

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 