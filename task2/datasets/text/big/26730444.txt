Romeoo
 
 
{{Infobox film
| name           = Romeoo 
| image          = Romeoo.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Rajasenan
| producer       = Rafi Mecartin
| writer         = Rafi Mecartin
| narrator       =  Dileep  Vimala Raman  Sruthi Lakshmi  Samvrutha Sunil
| music          = 
| cinematography = K. P. Nambiathiri
| editing        = Raja Mohammad   
| studio         = 
| distributor    = 
| released       = 2007
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Romeoo is a 2007 Malayalam romantic comedy film written by Rafi Mecartin and directed by Rajasenan, starring Dileep (actor)|Dileep, Vimala Raman, Sruthi Lakshmi and Samvrutha Sunil in the lead roles. The film received mostly negative reviews upon release but managed to become an above average grosser at the box office.

==Plot==
Manu Krishnan (Dileep (actor)|Dileep) is a male nurse in a reputed hospital. He is the Son of Ratheesh Kumar (Cochin Haneefa), a film junior artist. He has a special relationship with Dr. Priya (Vimala Raman), but is also in love with Leena (Samvrutha Sunil), the star contestant in the popular music reality show. Manu later in search of a job goes to an Agraharam to take care of a mentally ill patient and a girl from the Agraharam, Bhama (Sruthi Lakshmi) falls in love with him. Now the three girls want to marry Manu but none of their parents want to have Manu as son-in-law. The rest of the movie is about the trouble Manu has to take when the three meet and whom does he marry. He ends up with Dr. Priya.

==Cast== Dileep ...  Manu Krishnan/Manuel/Subramanian - A son of a singer who faces problems with three women
*Vimala Raman ...  Dr. Priya  - Manus love interest and a Medical Student
*Samvrutha Sunil ...  Leena - A Singing Contest winner
*Sruthi Lakshmi ...  Bhama  - An Iyer girl who wants to become a Singer
*Madhu Warrier ... Manuel - Leelas boyfriend of 5 years Ashokan ...  Subramanian - A rival of Manu who fights for Bhama
*Cochin Haneefa ...  Ratheesh Kumar (Manus father)
*Salim Kumar ...  Naranimangalam Narayanan
*Salu Kuttanadu ...  Passenger on the bus
*Mallika Sukumaran ...  Lakshmikutty  - Manus mother and ex-singer
*Suraj Venjaramood ...  Prasanth
*Rizabawa ...  Ramanathan - Bhamas father who tried to cheer his daughter
*Bheeman Raghu ...  Avaran - Leelas father
*Ambika Mohan ...  Priyas mother
*Nandu Poduval ...  Priest
*Kalamandalam Geethanandan ...  Vaidyar
*Rema Devi C. ...  Bhamas mother
* Subbalakshmi

==External links==
*  
* http://www.indiaglitz.com/channels/malayalam/preview/9767.html
* http://www.rediff.com/movies/2007/dec/17ssromeo.htm
* http://www.nowrunning.com/movie/4483/malayalam/romeo/index.htm

 
 
 
 
 
 

 