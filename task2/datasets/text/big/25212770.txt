I Spit on Your Grave (2010 film)
{{Infobox film
| name           = I Spit on Your Grave
| image          = I Spit on your Grave.jpg
| alt            = 
| caption        = Theatrical Release Poster
| director       = Steven R. Monroe Lisa Hansen Paul Hertzberg Meir Zarchi
| screenplay     = Stuart Morse
| story          = Meir Zarchi
| starring       = Sarah Butler Jeff Branson Rodney Eastman Daniel Franzese Andrew Howard Chad Lindberg Tracey Walter
| music          = Corey Allen Jackson
| cinematography = Neil Lisk
| editing        = Daniel Duncan
| studio         = CineTel Films
| distributor    = Anchor Bay Entertainment
| released       =   }}
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $2 million   
| gross          = $1,278,466 
}} rape and Day of the Woman (better known by its re-release title, I Spit on Your Grave).  It was directed by Steven R. Monroe,  and stars Sarah Butler, Chad Lindberg, Daniel Franzese, Rodney Eastman, Jeff Branson, and Andrew Howard.   

==Plot==
Jennifer Hills (Sarah Butler) goes to a cabin in the woods to write her second novel. Lost, she pulls into a gas station where there are three men loitering. They make small talk until she accidentally hits the panic button on her car, causing employee Johnny (Jeff Branson) to stumble backward into a bucket of water. Jennifer finds the cabin. Later, Matthew (Chad Lindberg), who has a social interaction disability, is contracted to fix the cabins bathroom sink and toilet, which he successfully does. Jennifer kisses him on the cheek in gratitude, stating that he saved her from having to bathe in the swampy lake. He later boasts to the three men, revealed to be brash, reckless, and sadistic (when we see them again they are beating a fish with a bat), that "she likes me".
 anally rapes her. The others also rape her, and Stanley (Daniel Franzese) records everything on his video camera. Afterward Jennifer walks onto a bridge and falls into the river just as Storch is about to shoot her, and doesnt surface. The men search for her body but do not find it. Storch tells Stanley to get rid of the evidence on his video camera.
 stalks her bear trap, sets up his camera, and holds his eyes open with fish hooks. She gets a fish and spreads the guts in his eyes and face. Crows come and peck on his face and eyeballs. Jennifer knocks Andy (Rodney Eastman) out with a baseball bat and ties him up over a bathtub which is filling with water. She puts lye in the water and when his strength gives, he collapses into the water. Each time he pulls himself up, his face becomes more corroded and his tongue keeps melting therefore bleeding to death. Jennifer strings Johnny up naked, pulls his teeth out with pliers, then forces him to perform fellatio on his pistol. Finally she cuts off his penis, sticks it in his mouth, and leaves him to bleed to death.

After killing Johnny, Jennifer visits Storchs family. Storch races home and learns Jennifer has taken his daughter to the park. Storch goes to the park but no one is there.  He gets back in his car and Jennifer knocks him unconscious. When he wakes, Jennifer rapes him with his shotgun and reminds him that she was just as innocent as his own daughter. She has attached one end of a string to the trigger and the other end to Matthews wrist. Matthew is alive but unconscious, and Jennifer tells Storch, "If I were you, Id tell him not to move". When Matthew wakes up, he triggers the shotgun, killing himself and Storch. Jennifer sits outside and smiles.

==Cast==
* Sarah Butler  as Jennifer Hills
* Rodney Eastman  as Andy
* Daniel Franzese  as Stanley
* Jeff Branson  as John "Johnny" Miller
* Andrew Howard  as Sheriff Storch
* Chad Lindberg as Matthew
* Tracey Walter  as Earl
* Mollie Milligan  as Mrs. Storch
* Saxon Sharbino  as Chastity Storch
* Amber Dawn Landrum  as Girl at Gas Station

==Production==
Principal photography began on November 2, 2009, under Steven R. Monroes direction.  It was distributed by CineTel, and produced by the companys president and CEO Paul Hertzberg.  Lisa Hansen, Jeff Klein, Alan Ostroff, Gary Needle, and Meir Zarchi (director of the original) served as executive producers.  Location filming took place in Louisiana.  The film was written by Stuart Morse, based on Zarchis screenplay.

==Release==
CineTel Films planned a 2010 worldwide theatrical release.  The film was part of the Texas Frightmare Weekend on May 1, 2010 in the Sheraton Grand Hotel in Irving, Texas,  and part of the Film4 Frightfest on August 29, 2010.  The Canadian unrated version premiered  at the Fantasia Festival on July 20, 2010.  The complete cast and Meir Zarchi, director of the I Spit on Your Grave|original, were present.  An unrated version was released in limited release on October 8, 2010, in the United States. Coincidentally, another horror film opened a week beforehand also unrated, called Hatchet II.  

===Home media=== UK by Lisa Hansen, teasers and trailers.

The DVD was banned in New Zealand, just before its April release. 

==Critical reception==
Critical reaction to the film has been generally negative, with a 34% rating on Rotten Tomatoes.  On Metacritic the film was assigned a rating of 27% based on reviews from 14 critics, indicating "generally unfavorable reviews".  Roger Ebert gave the film a zero-star rating (the same one hed given the original) and described it as the "despicable remake of the despicable 1978 film". He claimed that it added "a phony sense of moral equivalency", stating "If I rape you, I have committed a crime. If you kill me, you have committed another one. The ideal outcome would be two people unharmed in the first place." While he allowed that the movie was made "professionally," as opposed to the original version that was devoid of any kind of skills or artistry, he considered this largely in the service of the truly offensive material, the rape scenes, later summarizing "this   is more offensive, because it lingers lovingly and at greater length on realistic verbal, psychological and physical violence against the woman, and then reduces her revenge to cartoonish horror-flick impossibilities.  

Mick LaSalle of the San Francisco Chronicle wrote, "In addition to all the obvious things that are disgusting about this movie, I Spit on Your Grave is trying to get us to hate each other. Hate it instead. It makes more sense and the hatred is much more deserved."  Critic Andrew OHehir also denounced the notion that the film has a message, writing that it "just piles imaginary atrocities on top of real ones, and then halfheartedly claims that it means something. Well, it doesnt." 

Some support for the film in mainstream press came from The New York Times, which wrote, "Female-empowerment fantasy or just plain prurience, Grave is extremely efficient grindhouse."  V.A. Musetto, from The New York Post, also offered some support for the film, stating, "If you can handle it, see it." 
 Fangoria Magazine wrote that I Spit on Your Grave "...is just as raw and upsetting in its onscreen brutality as the original. Perhaps more so, since the acting is significantly better this time around". 

==Sequel==
A sequel, I Spit on Your Grave 2, directed by Steven R. Monroe was produced and was given a limited theatrical release on September 20, 2013. The films plot concerns a young aspiring model named Katie (Jemma Dallender) who accepts an offer to have photos taken for her modeling portfolio. Unfortunately for Katie, little does she know the event turns into a nightmare of torture, rape and kidnapping. The story has a rape-revenge plot similar to the first film.

==References==
{{Reflist|2|refs=
   
}}

==External links==
*    (US)
*    (UK)
*   (Russia)
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 