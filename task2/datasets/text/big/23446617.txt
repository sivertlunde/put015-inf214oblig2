The Rowdyman
{{Infobox_Film  
| name           = The Rowdyman
| image          = 
| caption        = 
| director       = Peter Carter
| producer       = Lawrence Z. Dane
| writer         = Gordon Pinsent
| narrator       = 
| starring       = Gordon Pinsent Will Geer Frank Converse Linda Goranson
| music          = Ben McPeek
| cinematography = Edmund Long
| editing        = Michael Manne
| distributor    = Crawley Films
| released       = May 18, 1972 (Canada) July 1973 (USA)
| runtime        = 95 minutes
| country        = Canada
| language       = English
| budget         = $350,000 (est)
| gross          = 
|}}
The Rowdyman is a comedy film with moralistic overtones, set in Newfoundland and Labrador|Newfoundland. It was written by and starred Gordon Pinsent, a native Newfoundlander. The film became a commercial hit and landed Pinsent a Canadian Film Award for Best Leading Actor. 

The film is about Will Cole (Pinsent), a man in his thirties who sees no reason to take life seriously, but his antics bring pain and tragic consequences to his friends and family. He is sexually liberated and has sex with a stranger (Dawn Greenhalgh) on a train and his carelessness at work causes pain for his best friend and co-worker (Frank Converse).

The Rowdyman was filmed in Corner Brook and St. Johns, Newfoundland and Labrador|St. Johns, and is regarded as one of the best films made about life in Newfoundland. 

It was featured in the Canadian Cinema television series which aired on CBC Television in 1974.   

==References==
* 
* 
* 
 

 
 
 
 
 
 