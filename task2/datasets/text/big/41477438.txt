Il Futuro
{{Infobox film
| name           = Il Futuro
| image          = Il Futuro.jpg
| alt            =
| caption        =
| director       = Alicia Scherson
| producer       = {{plainlist|
* Bruno Betatti
* Christoph Friedel
* Mario Mazzarotto
* Claudia Steffen
* Emanuele Nespeca
* Alvaro Alonso
* Luis Angel Ramirez
}}
| writer         = Alicia Scherson
| based on       =  
| starring       = {{plainlist|
* Manuela Martelli
* Rutger Hauer
* Luigi Ciardo
* Nicolas Vaporidis
* Alessandro Giallocosta
}}
| music          = {{plainlist|
* Caroline Chaspoul
* Eduardo Henríquez
}}
| cinematography = Ricardo DeAngelis
| editing        = {{plainlist|
* Soledad Salfate
* Ana Alvarez Ossorio
}}
| studio         = {{plainlist|
* Movimento Film
* Jirafa
* Pandora Films
* La Ventura
* Astronauta Films
* Jaleo Films
}}
| distributor    = Strand Releasing
| released       =  
| runtime        = 94 minutes
| country        = {{plainlist|
* Italy
* Chile
* Germany
* Spain
}}
| language       = {{plainlist|
* Italian
* Spanish
* English
}}
| budget         =
| gross          = $14,001 (US) 
}} Una novelita lumpen, the film stars Manuela Martelli and Rutger Hauer. The film was shot in Italy, Chile, and Germany,  and it is an Italian-Chilean-German-Spanish production.

== Plot ==
Narrated from the future, Bianca tells the story of how she and her younger brother, Tomas, are orphaned after their parents die in a car accident.  The children of Chilean immigrants, they have no family in Italy.  A social worker explains that a bureaucratic problem prevents them from accessing their mothers pension, so they will only receive funds from their fathers account.  Bianca, old enough to become her brothers guardian, takes a job as an apprentice at a hair salon, though the owner will not allow her to become a hairdresser until she has three years of experience.  Tomas, who believes that accidents cause supernatural change to reality, shifts his interest from computers to bodybuilding, and he begins to skip school to hang out at a local gym, where he takes a job as an unpaid intern.

Two local personal trainers from the gym befriend Tomas and teach him how to get subscription pornographic content for free.  After Tomas invites them to the house, Bianca allows them to stay overnight, as they have nowhere else to stay.  The personal trainers ingratiate themselves into the household by cleaning and cooking, and they eventually begin to have sex with Bianca.  Eventually, they approach her with a plan: seduce and rob an retired actor and bodybuilder who is rumored to have hidden his wealth in his decaying mansion.  Nicknamed Maciste after his most popular character, he has become reclusive and only sees prostitutes.  The personal trainers believe Bianca perfect for the part, as she is young, pretty, and can speak English.

Bianca soon learns that Maciste is blind.  Although she does not back out, she and Tomas become annoyed that the personal trainers did not explain this fact.  Bianca and Maciste meet several more times, and they engage in many conversations.  Although the personal trainers warn her to avoid asking Maciste about how he lost his vision, Bianca insists that he would never hurt her.  They become concerned that she has come to develop feelings for Maciste and urge her to move forward with the plan.  Maciste reveals to Bianca that he lost his vision in a car accident; when she asks if he was driving the car, he refuses to answer.  They grow closer emotionally, and Bianca becomes more confident and assertive.  Bianca admits to Maciste that she has fallen in love with him, but he says that he feels nothing for her.  Upset that her feelings are unrequited, she leaves in tears.

After a competition at the gym goes poorly, the personal trainers again urge Bianca to steal Macistes money.  On her next visit, Maciste is sick and does not want to have sex with her.  Instead, she cares for him, and, when he falls asleep, she pickpockets his keys.  Bianca looks in a locked room but finds no evidence of a hidden fortune.  As she leaves the mansion, Maciste calls for her to stay, but she politely declines.  Maciste leaves his mansion and wanders into the street after her, but she continues on her way.  When she arrives back at her own apartment, she demands that the personal trainers leave.  Although she fears for the worst, she says that she does not find any news reports about Maciste in the papers.

== Cast ==
* Luigi Ciardo as Tomas
* Manuela Martelli as Bianca
* Rutger Hauer as Maciste
* Nicolas Vaporidis as Libio (the Libyan), a personal trainer
* Alessandro Giallocosta as Boloñes (the Bolognesi), a personal trainer

== Release ==
Il Futuro premiered at the 2013 Sundance Film Festival.   It opened in Chile on 6 June 2013. 

== Reception == The Shining, the film is a good-looking series of ambitious tropes and contrivances that dont seem to express much significance, even when it settles into its own haunted-house setting halfway through." 

Martelli was nominated for an Altazor Award for her performance. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 