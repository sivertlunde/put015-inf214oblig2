Presto (film)
 
{{Infobox film
| name           = Presto
| image          = Presto_poster.jpg
| alt            = Movie poster shows a man in a tuxedo holding a smiling rabbit in one of his hands, while the other is raised as if to present the rabbit to an audience. Text at the top of the image states "Pixar Presents A Magical Motion Picturette", followed by the films title. Near the bottom of the image, is three circles, each containing scenes from the short film.
| director       = Doug Sweetland
| producer       = Richard Hollander 
| writer         = Doug Sweetland
| starring       = Doug Sweetland 
| music          = Scot Blackwell Stafford
| editing        = Katherine Ringgold
| studio         = Pixar Animation Studios Walt Disney Pictures Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 5:17
| country        = United States
| language       = English
}} short film shown in theaters before their feature-length film WALL-E. The short is about a magician trying to perform a show with his uncooperative rabbit and is a gag-filled homage to classic cartoons such as Tom and Jerry and Looney Tunes. Presto was directed by veteran Pixar animator Doug Sweetland, in his directorial debut.

The original idea for the short was a magician who incorporated a rabbit into his act who suffered from stage fright. This was considered to be too long and complicated, and the idea was reworked. To design the theater featured in Presto, the filmmakers visited several Opera Houses and theaters for set design ideas. Problems arose when trying to animate the theaters audience of 2,500 patrons—which was deemed too expensive—and was solved by showing the back of the audience.
 Animation Show of Shows in 2008.

==Plot== hat trick wherein he pulls his rabbit Alec Azam  out of his top hat. The short begins with an unfed and irritated Alec locked in a cage, unable to reach his carrot.  After Presto returns from eating a meal, he begins practicing his act with Alec, revealing that his top hat is magically connected to a wizards hat kept backstage with Alec, so that when Presto reaches into the top hat, his hand appears out of the wizards hat, allowing him to grab Alec and pull him out of the top hat. He intends to feed Alec the carrot, but realizes that he is late for the show and rushes off to the stage without doing so, much to Alecs anger. Presto tries to start the performance, but Alec becomes reluctant to cooperate until he is given the carrot. Presto then spends the rest of the show trying to catch Alec through the opening between his top hat and the wizards hat.
 slacks which leaves him in his underwear. Furious, Presto wants to attack Alec, but in an act to defend himself, Alec puts an offstage ladder into the wizard hat, and Presto is hit between its legs. Trying to get revenge, Presto attempts to hit Alec with it, but the plan backfires when it misses and hits a door, and he is sent hitting his chin on the ladder. Presto antagonizes Alec again, covering the carrot with a cloth before smashing it into a pulp with a piece of the ladder. Alec then retaliates by aiming the opening of the wizards hat towards an electrical socket which Prestos finger goes into, causing him to dance wildly to bluegrass music while electrocuted. The audience interprets the commotion as being parts of the act and applauds with increasing approval.
 fly space above the stage when he accidentally releases the weights holding down some stage props whilst trying to catch Alec by force. When his foot comes loose from the rope, he falls, along with suspended scenery. Alec, realizing that Presto will be crushed, uses the magic hat to save him, earning the audiences wild approval for both himself and Presto. Presto gives Alec the carrot (returned to its original shape), as well as second billing on the posters advertising the show (as seen in the promotional poster above) and they are rewarded for each show they do, with roses and carrots for Presto and Alec respectively.

==Production==
 

Presto was directed by veteran Pixar animator Doug Sweetland, in his directorial debut. Sweetland provides the dialogue-free voice acting for both of the movies characters.    He pitched the film at the start of 2007 and began production late in the year, completing it in May 2008.  Presto s  gag-based format was heavily influenced by classic cartoons. Looney Tunes cartoons directed by Tex Avery were a major influence, with Alec being easily compared to Bugs Bunny. Other influences include Tom and Jerry, the Marx Brothers, and Charlie Chaplin. The character design for Presto was based on William Powell.      
 A Star Is Born. The idea was reworked due to being too long and complicated, taking an estimated three minutes longer to tell. 
 Paris Opera House and classic vaudeville theaters like the Geary in San Francisco—which the crew took a tour through—for set design ideas. Animating the theaters audience of 2,500 patrons proved an expensive proposition, even with the help of the crowd-generating MASSIVE (software)|MASSIVE software. Early suggestions were to show cutaways of just a small portion of the audience, but the full effect was achieved by only showing the back of the audience.  To save time, most of the audience models were borrowed from the previous Pixar film, Ratatouille (film)|Ratatouille. Additionally, Prestos body (from the neck down) is Skinners lawyer, and the carrot was one of the many food props from that film.

==Reception== The Prestige. 36th Annie Animated Short Film, but lost to La Maison en Petits Cubes. 

==Notes==
 

:a : A play on the word prestidigitation.
:b : A play on the magic word Magic word|alakazam.

 

==References==
 

==External links==
*  
*  
*  

 
{{succession box
 | before = Your Friend the Rat short films
 | years  = 2008
 | after  = Rescue Squad Mater}}
 

 
 
 

 

 
 
 
 
 
 
 
 
 
 