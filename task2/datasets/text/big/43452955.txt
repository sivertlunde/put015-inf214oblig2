Wars and Treasure
{{Infobox film
| name           = Wars and Treasure
| image          = Wars_and_Treasure_Movie_Poster.jpg
| alt            = 
| caption        =poster movie 
| film name      =  
| director       = Hossein Shahabi
| producer       = Hossein Shahabi
| writer         = Hossein Shahabi
| starring       =  
| music          = hossein shahabi
| cinematography = bahman znoozi
| editing        = Hossein Shahabi
| studio         = Rain Home Movie
| distributor    = baran film house
| released       =  
| runtime        = 100 minutes
| country        = Iran
| language       = Persian
| budget         = 
| gross          = 
}} Iranian drama film directed by Hossein Shahabi (Persian: حسین شهابی)       

==Background==
Release of the film is banned for 15 years issued by the Government of Iran for screening film festivals.

==Cast==
* Fariba Mortazavi
* Rasool Honarmand
* Parvin Solymani
* Manoochehr Farhang
* Mohammad Taheri Azar
* Yaser Rakh
* Arezoo Kheyroshar
* Farrokhe Yazdanfar

==Crew==
* Director Of Photography: Bahman Zonoozi
* Sound Recorder: karim esbati
* Editor:Hossein Shahabi
* Music: Hossein Shahabi
* Costume Designer:Hossein Shahabi
* family Of Consulting: Fariba Poursaberi
* Planner: Mohammad Taheri Azar
* Assistsnts Director: Siavash Shahabi - Ahmad Shahabi

==References==
 
 
 
 