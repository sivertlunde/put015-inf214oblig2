Manushyan
{{Infobox film 
| name           = Manushyan
| image          =
| caption        =
| director       = P Ravindran
| producer       =
| writer         =
| screenplay     = Madhu Kuthiravattam Pappu Vidhubala
| music          = V. Dakshinamoorthy
| cinematography =
| editing        = G Venkittaraman
| studio         = Kalasakthi Films
| distributor    = Kalasakthi Films
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by P Ravindran. The film stars Madhu (actor)|Madhu, Kuthiravattam Pappu and Vidhubala in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast== Madhu
*Kuthiravattam Pappu
*Vidhubala

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Bharanikkavu Sivakumar and ONV Kurup. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aadiyushassil  || K. J. Yesudas || Bharanikkavu Sivakumar || 
|-
| 2 || Aakashame || K. J. Yesudas || ONV Kurup || 
|-
| 3 || Etho sandhyayil || K. J. Yesudas || ONV Kurup || 
|-
| 4 || Hamsapadangalil || Vani Jairam || Bharanikkavu Sivakumar || 
|}

==References==
 

==External links==
*  

 
 
 

 