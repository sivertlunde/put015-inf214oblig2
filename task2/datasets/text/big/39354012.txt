Aroused (film)
 
{{Infobox film
| name = Aroused
| image = Aroused (film).jpg
| border = yes
| director = Deborah Anderson
| writer =
| starring =
| music = Damion Anderson
| cinematography = Christopher Gallo
| editing = David Schenk 
| producer = Deborah Anderson, Christopher Gallo, Mike Moz, Trina Venit
| distributor =
| released =    
| runtime = 66 min.
| awards =
| country = United States
| language = English
| budget = $150 000  (estimated)  
}} 
Aroused is a 2013 feature length documentary film directed by the photographer Deborah Anderson, in her directorial debut. 

== Content ==
The film profiles the life of 16 notable adult film actresses. 

== Reception == Village Voice as "a tack better suited to Andersons famed photo collections than a narrative medium" in which the interviews "are cut into such narrow snippets its impossible to piece together anyone here into a coherent personality".    

The Hollywood Reporter wrote that the film "lacks the depth to be much more than a glossily sexy curiosity".  According the Washington Post film critic Michael OSullivan, despite being in some moments "surprisingly moving" the film "arouses curiosity without really satisfying it". 

==Cast== Belladonna
* Kayden Kross
* Lisa Ann
* Katsuni
* Teagan Presley
* Ash Hollywood 
* Misty Stone
* Tanya Tate
* Asphyxia Noir  
* Jesse Jane
* Francesca Lé
* Lexi Belle April ONeil
* Brooklyn Lee
* Allie Haze  
* Alexis Texas

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 