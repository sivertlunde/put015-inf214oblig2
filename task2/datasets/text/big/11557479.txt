Dream of Love
{{Infobox film
| name           = Dream of Love
| image          = 600full-dream-of-love-poster.jpg
| image_size     =
| caption        = Original Film Poster
| director       = Fred Niblo
| producer       =
| writer         = Dorothy Farnum Marion Ainslee (titles) Ruth Cummings (titles)
| based on       =  
| starring       = Joan Crawford Nils Asther Aileen Pringle Warner Oland
| music          =
| cinematography = Oliver T. Marsh William H. Daniels
| editing        = James McKay
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = Silent English intertitles
| budget         = $221,000  . 
| gross          = $571,000 
}}
 silent Biographical biographical drama French tragedy Adrienne Lecouvreur by Eugène Scribe and Ernest Legouvé. 
 Gypsy performer, in a tale of lost love and revenge. Dream of Love is now considered lost film|lost.  

==Plot== stage actress and again meets the prince.  Coincidentally, she is appearing in a play which resembles the sad story of her earlier relationship with the prince. Maurice is struggling to win his throne from a usurping dictator. With Adriennes help, he dodges an assassination attempt and becomes king.

==Cast==
*Nils Asther - Prince Maurice de Saxe
*Joan Crawford - Adrienne Lecouvreur
*Aileen Pringle - The Duchess
*Warner Oland - The Duke, Current Dictator
*Carmel Myers - The Countess
*Harry Reinhardt - Count
*Harry Myers - The Baron
*Alphonse Martell - Michonet
*Fletcher Norton - Ivan

===Box Office===
According to MGM records the film earned $339,000 in the US and Canada and $232,000 elsewhere resulting in a profit of $138,000. 

==References==
 

==External links==
* 
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 