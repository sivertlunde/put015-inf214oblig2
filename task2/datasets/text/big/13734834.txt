Okka Magaadu
{{Infobox film
| name           = Okka Magaadu
| image          = OkkaMagaadu_logo.jpg
| caption        =
| writer         = Chintapalli Ramana  
| story          = YVS Chowdary
| screenplay     = YVS Chowdary
| producer       = YVS Chowdary
| director       = YVS Chowdary Simran Anushka Shetty Nisha Kothari
| music          = Mani Sharma
| cinematography = Madhu A Naidu C. Ram Prasad
| editing        = Kotagiri Venkateswara Rao
| studio         = Bommarillu   
| released       =  
| runtime        = 171 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Bharateeyudu (1996), starring Kamal Haasan, directed by S. Shankar which was a blockbuster a box office. People never really understood why Chowdhry would spend his hard earned money and squander away his wealth by making a disaster.The movie was made solely to improve Balayyas political image but it actually had the opposite effect.Simran and Balayyas makeup were criticised universally.         

==Plot==
Veeravenkata Satyanarayana Swamy (Nandamuri Balakrishna) is beloved by villagers whose lives were forever changed by his presence. His grandmother, Baby (Simran Bagga), whose husband had dedicated his life to gaining independence for his country, was taken as a prisoner of war and declared Missing In Action. Meanwhile, corrupt doctors, lawyers, and journalists are allegedly killed but their bodies were never found. Suspected of the murders, Swamy is arrested. But an old man named Okka Magadu (Nandamuri Balakrishna) comes forward and claims to have committed the alleged murders. Okka Magadu looks like the older version of Swamy whose sworn enemy is Namboodriyar (Ashutosh Rana)--a politician whose son elopes with a middle-class girl under Swamys protection.

==Cast==
{{columns-list|3|
* Nandamuri Balakrishna as Raghupati Raghava Rajaram & Veera Venkata Satyanarayana Swamy (Dual role) Simran as Sarada a.k.a. Baby
* Anushka Shetty as Bhavani
* Nisha Kothari as Alivelu
* Ashutosh Rana as Namboodriyar
* Ravi Kale as CBI Officer
* Salim Baig
* Kota Srinivasa Rao Chandra Mohan  Ranganath 
* Giri Babu 
* Raghunath Reddy 
* Chalapathi Rao 
* Subbaraju 
* Ravi Prakash 
* G. V. Sudhakar Naidu 
* Sivaji Raja  Chinna 
* Sameer 
* Prabhakar 
* Ping Pong Surya 
* Krishna Bhagawan 
* Raghu Babu 
* Lakshmipathi 
* Bandla Ganesh 
* Prasad Babu 
* Narayana Rao 
* Hari Prasad 
* Siva Prasad 
* Devdas Kanakala 
* Narsingh Yadav  Annapurna 
* Siva Parvathi  Rajyalakshmi 
* Alapathi Lakshmi 
* Haritha 
* Karishma
}}

==Soundtrack==
{{Infobox album
| Name        = Okka Maggadu
| Tagline     = 
| Type        = film
| Artist      = Mani Sharma
| Cover       = 
| Released    = December 21, 2007
| Recorded    = 
| Genre       = Soundtrack
| Length      = 35:57
| Label       = Supreme Music
| Producer    = Mani Sharma
| Reviews     =
| Last album  = Godava   (2007)  
| This album  = Okka Maggadu   (2008)
| Next album  = Ontari (film)|Ontari   (2008)
}}

Music composed by Mani Sharma. Lyrics were written by Chandrabose (lyricist)|Chandrabose.  All songs are hit tracks. Music released on Supreme Music Company. The music was released on December 21, 2007. The Audio Launch party was held at Cyber Gardens, Hyderabad with much celebrated fanfare. Dr.Dasari Narayana Rao was honored at the function as the Guest of honor.
 
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length =
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Okka Magaadu
| extra1  = Rita, Bhargavi
| length1 = 5:41

| title2  = Devadi Deva
| extra2  = Vijay Yesudas,Ranjith
| length2 = 5:47

| title3  = Nanu Paalinchaga
| extra3  = Mallikarjun,Rita
| length3 = 5:04

| title4  = Aa Aaa Ee Eee
| extra4  = Rahul Nambiar,Jey
| length4 = 4:58

| title5  = Ammatho Anushka
| length5 = 4:57

| title6  = Rey  
| extra6  = Rita
| length6 = 4:37

| title7  = Pattuko Pattuko  SP Balu, Chitra 
| length7 = 4:53
}}

==Others== Hyderabad

==References==
 

==External links==
* 

 

 
 
 