How Much Do You Love Me?
{{Infobox film
| name           = How Much Do You Love Me?
| image          = Combien_tu_m_aimes.jpg
| caption        = Theatrical release poster
| director       = Bertrand Blier
| producer       = Olivier Delbosc Marc Missionier
| writer         = Bertrand Blier
| starring       = Monica Bellucci Bernard Campan Gérard Depardieu
| cinematography = François Catonné
| editing        = Marion Monestier
| distributor    = Pan Européenne
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
}}

How Much Do You Love Me? ( ) is a 2005 French romantic comedy film written and directed by Bertrand Blier. It was released on 26 October 2005 in France and Belgium, and had a limited United States release on 18 March 2006. It was entered into the 28th Moscow International Film Festival where Blier won the Silver George for Best Director.   

==Plot==
François (Bernard Campan) is a bored office worker with health problems. He visits a brothel and claiming to have won the lottery and millions of euros, he propositions Daniela (Monica Bellucci), one of the prostitutes to live with him for the sum of 100,000 euros a month until his money runs out. She accepts. François first doctor, André, is worried that Danielas volcanic sexuality will cause François heart to give out. The next day she takes François to beach and he loves that fresh air. They both starts running with hands wide out and cheering in joy. Then they both involve in sex inside Françoiss car. That night André inquires them and finds their sexual activity that evening. Then André gives a moving speech about an idealistic and unrequited love he has for a nurse who later became his patient. Later, when Daniela suffers food poisoning and André is summoned to treat her, when she undresses he suffers a stroke and dies. At the funeral, François learns that Andrés nurse/patient/girlfriend had died some years before. The arrangement between Daniela and François seems to work for a little while until Danielas former lover, Charly (Gérard Depardieu) demands money to allow the relationship to continue. François refuses and Daniela resumes being with Charly. While Daniela is having sex with Charly, she realizes she has really fallen in love with François who has meanwhile, started an affair with his next-door neighbor. The film concludes with a surreal pastiche of fantasy and conjecture which begins with François admitting that he never won the lottery.
As the film has started somewhat as a complex story, it continues in the same way; till the last ten minutes wherein a comedic party starts. Daniela entices a friend of François and moves him away to sex. Seeing that, everyone comments her; but François is unmoved. Then the film ends, with the final shot picturing Daniela and François dining together. A new life starts for them.

==Cast==
* Monica Bellucci as Daniela de Montmartre
* Bernard Campan as François
* Gérard Depardieu as Charly
* Sara Forestier as Muguet
* Farida Rahouadj as the neighbor
* Michel Vuillermoz as the doctor

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 