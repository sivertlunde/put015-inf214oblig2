Los Angeles Now
{{multiple issues|
 
 
}}

Los Angeles Now is a 60 minute documentary by producer/director Phillip Rodriguez. It first aired in November 2004 on Public Broadcasting Service|PBS’s Independent Lens series.

The documentary investigates the city of Los Angeles as it comes of age and wrestles with its history and its future. The film includes conversations with a broad range of Los Angeles figures, from actress Salma Hayek and businessman/philanthropist Eli Broad to author and essayist Richard Rodriguez and Cardinal Roger Mahony.

==External links==
* 

 
 
 


 