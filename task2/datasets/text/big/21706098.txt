The Good Love
{{Infobox Film
| name           = The Good Love
| image          = 
| image_size     = 
| caption        = 
| director       = Francisco Regueiro
| producer       = 
| writer         = Francisco Regueiro
| narrator       = 
| starring       = Simón Andreu
| music          = 
| cinematography = Juan Julio Baena
| editing        = Pablo González del Amo
| distributor    = 
| released       = 21 November 1963
| runtime        = 90 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Good Love ( ) is a 1963 Spanish drama film directed by Francisco Regueiro. It was entered into the 1963 Cannes Film Festival.   

==Cast==
* Simón Andreu - José
* Marta del Val - Mari Carmen
* Enriqueta Carballeira - Juanita (as Mª Enriqueta Carballeira)
* Luisa Muñoz
* Enrique Pelayo - Guardia Civil 1
* Chiro Bermejo - Guardia Civil 2
* Esmeralda Adan
* Wifredo Casado - Novio tren
* Juan Torres
* Milagros Guijarro Francisco Serrano
* Dolores García Morales
* Francisco Guijar
* Luis Rico
* Mari Paz Yañez

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 