The Shanghai Spell
{{Infobox film
| name           = The Shanghai Spell El embrujo de Shanghai
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Fernando Trueba
| producer       = 
| writer         = 
| screenplay     = Fernando Trueba
| based on       =  
| starring       = Fernando Tielve Aida Folch Fernando Fernán Gómez Ariadna Gil
| music          = Antoine Duhamel
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}}
The Shanghai Spell ( ) is a 2002 Spanish film written and directed by Fernando Trueba. The film is based on the novella of the same name written by Juan Marsé and published in 1993.

==Plot==
The film is set in the Gràcia district of Barcelona, in the wake of the Spanish Civil War.    Fourteen-year-old Dani (Fernando Tielve) is a budding artist, who looks after Captain Blay (Fernando Fernán Gómez), an ageing civil war veteran. Blay suggests Dani draw local girl Susana (Aida Folch) as the subject of a poster warning of the dangers of factory smoke causing Tuberculosis|consumption. Dani and Susana begin a tentative romance, as they hear stories of Susanas fathers exploits as a secret agent in the Chinese city of Shanghai from one of his wartime colleagues.  , SBS Films. 

==Awards== Best Cinematography, and Best Production Supervision.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 