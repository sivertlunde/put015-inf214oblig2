No Holds Barred (1952 film)
For the 1989 film of the same name starring Hulk Hogan, see No Holds Barred (1989 film).
{{Infobox film
| name           = No Holds Barred
| image_size     =
| image	         = No Holds Barred FilmPoster.jpeg
| caption        =
| director       = William Beaudine Jerry Thomas Tim Ryan Bert Lawrence Jack Crutcher
| narrator       =
| starring       = Leo Gorcey Huntz Hall David Gorcey Bernard Gorcey
| music          = Edward J. Kay Ernest Miller William Austin
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

No Holds Barred is a 1952 comedy film starring The Bowery Boys. The film was released on November 23, 1952 by Monogram Pictures and is the twenty-eighth film in the series.

==Plot==
Sachs head becomes hard and he feels no pain.  As a result, Slip enters him into a wrestling match hoping to win $1,000.  However, Sachs strength has disappeared from his head.  Luckily they discover that it has traveled to his finger, and he wins the match.  Slip decides to enter Sach into more matches, but a rival manager wants to take control.  Slip resists the offer and Sach goes onto become world champion, with his power traveling to various parts of his body, including his elbow and toes.  The rival manager prevents another wrestler from fighting Sach in a charity match so that his wrestler can go against him.  The boys are then kidnapped in the hopes that they will reveal where Sachs power has traveled to.  The escape and Sach enters the ring, not knowing where his power is.  Slip discovers that it in on Sachs derriere, and uses that knowledge to win the match.  In the end, when Slip is about to give Sach a new nickname based upon where his power now lies, Sach says to him, "You say it and were out of pictures!"

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*David Gorcey as Chuck (Credited as David Condon)
*Bennie Bartlett as Butch

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski 
*Leonard Penn as Peter Taylor
*Marjorie Reynolds as Rhonda Nelson
*Hombre Montana as himself

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume One" on November 23, 2012.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Feudin Fools 1952 Jalopy 1953}}
 

 
 

 
 
 
 
 
 
 