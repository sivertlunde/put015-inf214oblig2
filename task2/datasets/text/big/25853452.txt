Anderson's Cross

{{Infobox film
| name           = Andersons Cross
| image          = AndersonsCross.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Jerome Elston Scott
| producer       =  
| writer         = Jerome Elston Scott
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =  
| music          = Cris Velasco
| cinematography = Oktay Ortabasi
| editing        = Leslie Ortabasi
| studio         = Illumination Pictures
| distributor    = TLA releasing
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Andersons Cross is a 2010 comedy-drama film that was written and directed by Jerome Elston Scott,  who also starred in the movie, as his first writing and directing project.     Production for the movie began in 2003 and the movie had its world premiere on May 20, 2010, when it was given a limited theatrical release. It stars Elston Scott, Nicholas Downs, and Heather Bergdahl as three best friends whose evolving relationships cause tension in their senior year of high school.   The film was released on DVD November 22, 2010.   

==Plot==
Nick (Jerome Elston Scott), Kevin (Nicholas Downs) and Tracy (Heather Bergdahl) are all the best of friends and are seemingly inseparable. Theyve spent most of their lives together and are so close that Kevin and Tracy (who are a couple) even allow Nick to watch while they have sex. This works out well until Nick meets Trevor (Micah Stuart) and begins questioning his own sexuality, as hes very attracted to Trevor. Things are made more tense by the fact that the three friends are in their senior year of high school and are rapidly approaching adulthood.

== Cast == Michael Warren as Mr. Anderson
* Joanna Cassidy as Mrs. McCarthy
* Joyce Guy as Mrs. Anderson
* Jerome Elston Scott as Nick Anderson
* Nicholas Downs as Kevin Daniels
* Heather Bergdahl as Tracy Green
* Bill Moseley as Mr. Daniels
* Micah Stuart as Trevor
* Art Evans as Grandfather
* Ryan Carnes as David
* Taran Killam as Austin Wilson
* Alan Blumenfeld as George Green
* Mary Jo Catlett as Mrs. Elway James Snyder as Ben Carter
* Ryan Carnes as David
* Taran Killam as Austin Wilson
* Jack Donner as Dr. Landry
* Kimmy Robertson as Teacher #1
* Rocky Marquette as Shawn Jenkins Brad Yoder as Detective Marshall
* Josh Wells as Peter Anson

==Production==
Jerome Elston Scott used the services of casting director Mark Sikes, and related that throughout the filmmaking process, the greatest challenge was financing.    In 2003, their official website offered that Kenneth "Tennessee" England was to direct and that casting was not yet announced.  By June 2007, casting was complete and a trailer had been released.   By July 2011 the film had screened at multiple film festivals and received positive response. 

==Reception==
DVD Verdict gave a mostly positive review for Andersons Cross, stating that while it " wont prove terribly memorable" it was overall a "small, sweet movie with a good attitude and a lot of heart",as "a solid film; not a great one, but a solid debut."   While offering "standard late teen fodder",  the film includes actors whose abilities "raise the film above your average indie drama."   While Jerome Elston Scott, Nicholas Downs, and Heather Bergdahl have chemistry as a threesome, it was the "supporting cast gives the film its life."   Special note is made of the work of Michael Warren, Joanna Cassidy, and Bill Moseley. While the film itself is "fairly forgettable", the "performances are downright excellent" and the film can be recommended.  The reviewer concluded, "Andersons Cross is a small, sweet movie with a good attitude and a lot of heart. It wont prove terribly memorable, but its a fine first feature."   
 Moving Pictures Peyton Place in "its spotlight on alcoholism, absentee fathers, statutory rape, homosexuality and death",  but presented in a sanitized version which diminishrd dramatic potential.  The reviewer noted that director Scott had written the lead role of Nick Anderson with thought toward playing that role himself but was too old by the time of filming, which resulted in the role being miscast and becoming the films greatest problem. As played by Michael Warren, the character of Nick was not compelling, being "just too dopey, too hangdog, too hopelessly bland" and lacking in magnetism and charisma for the other characters, as shared in the film, to have such romantic interest in him.  It was offered that despite its flaws "the script has nuggets of genuineness, and Micah Stuart as Trevor, the paperboy who romances Nick, is natural and dimensional",  but the films quality moments were "not enough to cut through the sentimental facileness and surface melodrama of an ensemble of characters many of whose names we can’t even remember."   

==Recognition==

===Awards and nominations=== Bridgetown Film Festival in Barbados    
 Independent Black Film Festival in Atlanta, Georgia|Atlanta,  

==Future plans==
In November 2010, Jerome Elston Scott revealed that Illumination Pictures was developing a prequel television series based on Andersons Cross for cable television, aimed at a younger demographic to depict the growing relationships of the original films main characters.  In a press release, Illumination Pictures announced that the series would center around the Anderson family and the teen friends as adolescents. Negotiations have begun with original cast members Michael Warren, Joanna Cassidy, and Joyce Guy to reprise their roles as the parents. Due to the prequel series taking place in an earlier timeline than the original, the roles of the teens will be cast with younger actors. Screenwriter Jerome E. Scott is expected to "helm the pilot and the first six episodes."   

== References ==
 

== External links ==
*   as archived by Wayback Machine
*  

 
 
 
 
 
 
 
 
 