Fading of the Cries
 
 
{{Infobox film
| name           = Fading of the Cries
| image          = Fading of the Cries.jpg
| caption        = Film poster
| producer       = Brian Metcalf Karoline Kautz Thomas Ian Nicholas Ben Chan
| writer         = Steven McGuire
| starring       = Brad Dourif Thomas Ian Nicholas Mackenzie Rosman Elaine Hendrix
| music          = Nathaniel Levisay
| cinematography = Brad Rushing
| editing        = Steven McGuire
| studio         =
| distributor    = Lionsgate Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Fading of the Cries is a 2010 American fantasy film directed by Steven McGuire, produced by Metcalf, Karoline Kautz, Ben Chan and Thomas Ian Nicholas.  It stars Brad Dourif, Thomas Ian Nicholas, Mackenzie Rosman and Elaine Hendrix. 

==Plot==
 
Jacob is a young man who defends his town from evil forces, aided by a magic sword. He saves a girl called Sarah from a horde of the reanimated dead, and they escape through the floor of a church. An evil necromancer named Mathias confronts Sarah and demands an amulet given to her by her uncle before he died. Sarah refuses, and after threatening to unleash all the evils he can conjure, Mathias disappears.

In the morning, Sarah and Jacob return to Sarahs house through streets, fields, churches and underground tunnels, pursued by hordes of demonic creatures. They arrive around midday to find Mathias is already there, holding a sword to Sarahs little sister Jills throat. Mathias threatens to kill Jill unless Sarah gives him the amulet. Sarah makes Mathias promise he will let Jill go, and gives him the amulet. Mathias disappears with both Jill and the necklace, and Sarah begs Jacob to go save Jill from Mathias. Jacob reluctantly agrees.

Jacob arrives at Sarahs dead uncles house and fights Mathias. He manages to destroy the amulet, defeating Mathias, but Jill falls through a hole in the floor. Jacob dives after her and breaks her fall with his own body, unharmed himself because he cannot die due to a protection spell that Sarahs uncle cast before he died. They return to Jills home. Her mother and her sister Sarah are dead, killed by the reanimated dead while Jacob was not there to protect them.

Jacob returns to Sarahs uncles house to find Mathias reading through his necromancer spells, looking for a way to restore his power. Mathias laughs and boasts that he cannot be killed because he is already dead. Jacob says grimly that he is not there to kill Mathias; instead he will torture Mathias for all eternity. The movie fades out with Jacob repeatedly slashing Mathias with his sword and the sounds of Mathiass screams filling the air. 

==Cast==
* Brad Dourif as Mathias
* Thomas Ian Nicholas as Michael
* Elaine Hendrix as Maggie
* Hallee Hirsh as Sarah
* Mackenzie Rosman as Jill
* Jordan Matthews as Jacob
* Lateef Crowder as Sylathus
* Jessica Morris as Malyhne
* Julia Whelan as Emily

==Release==
 
==Reception==
 
==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 