Tizoc (film)
 
{{Infobox film
| name           = Tizoc
| image          = Tizoc57.jpg
| caption        = Film poster
| director       = Ismael Rodríguez
| producer       = Antonio Matouk
| writer         = Manuel R. Ojeda Carlos Orellana Ricardo Parada de León Ismael Rodríguez
| starring       = Pedro Infante María Félix
| music          = 
| cinematography = Fernando Martínez Álvarez
| editing        = Fernando Martínez Álvarez
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

Tizoc is a 1957 Mexican drama film directed by Ismael Rodríguez. It was entered into the 7th Berlin International Film Festival, where Pedro Infante won the Silver Bear for Best Actor.    The film also won the Golden Globe Award for Best Foreign Language Film at the 15th Golden Globe Awards.   

==Cast==
* Pedro Infante - Tizoc
* María Félix - María
* Alicia del Lago - Machinza
* Eduardo Fajardo - Arturo
* Julio Aldama - Nicuil
* Andrés Soler - Fray Bernardo
* Carlos Orellana - Don Pancho García
* Miguel Arenas - Don Enrique del Olmo
*Manuel Arvide - Cosijope
*Guillermo Bravo Sosa - Sorcerer
* Polo Ramos
* Paco Crow - Dancer

==References==
 

==External links==
* 

 
 
 
 
 
 


 