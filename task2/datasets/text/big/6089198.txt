Thou Shalt Not Kill... Except
{{Infobox Film
| name           = Thou Shalt Not Kill... Except
| image          = Thou Shalt Not Kill Except.jpg
| caption        = DVD release cover
| image_size     = 250px
| director       = Josh Becker
| producer       = Shirley Becker Scott Spiegel
| screenplay     = Josh Becker Scott Spiegel
| story          = Josh Becker Bruce Campbell Sheldon Lettich
| starring       = Robert Rickman John Manfredi Timothy Patrick Quill Sam Raimi Ted Raimi
| music          = Joseph LoDuca
| cinematography = Josh Becker
| editing        = Josh Becker
| studio         = Action Pictures Renaissance Pictures
| distributor    = Film World Distributors (USA)
| released       = October 13, 1985 (Warren, Michigan) 21 May 1987 (West Germany/Japan)
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $200,000
| gross          = 
}} action horror film directed by Josh Becker and starring Robert Rickman, John Manfredi, Tim Quill, Cheryl Hausen, Perry Mallette and Sam Raimi. It was written by Becker and Scott Spiegel from a story by Becker, actor Bruce Campbell, and Sheldon Lettich.

The cast of the film consists largely of unknowns, because most of the smaller roles are filled by Beckers friends and relatives. Some, however, have been featured in other films, largely those produced by Renaissance Pictures or otherwise involving Sam Raimi, Bruce Campbell or Josh Becker.
 underground cult classic.

== Plot synopsis ==
Having come home after half of his squadron was killed during the Vietnam War, Sergeant Jack Stryker (portrayed by Brian Schulz), given an honorable discharge due to his injuries, attempts to get his life back together. Finding himself reunited with an old girlfriend, Sally (Cheryl Hausen) and his war buddies, he feels he may have successfully re-established his life. However, this happiness is quickly cut short when a murderous cult led by an enigmatic but unnamed Charles Manson-like figure, portrayed by director and writer Sam Raimi, comes into town to continue their rampage.
 trailer puts it - "break the laws of both God (the title is a reference to one of the Biblical Ten Commandments) and man" and fight back. What follows is a war between the two groups, ending in numerous deaths, including the cult leaders; the exchange between the cult leader and Stryker is as follows:

 Cult Leader: "I am Jesus Christ!" 
Stryker: "No, youre not &mdash; Youre dead." 

Upon which Stryker shoots Raimis character in the chest, and he careens into a river, eventually being impaled on a motorcycle, and their brutal war is ended.

== Cast ==
=== Strykers group ===
* Robert Rickman as Sgt. Walker J. Jackson
* John Manfredi as 2nd Lt. David Miller
* Tim Quill as Lt. Cpt. Tim Tyler
* Cheryl Hausen as Sally
* Perry Mallette as Otis
* Pam Lewis as Mom
* Jim Griffen as Dad

=== Cult Members ===
* Sam Raimi as Cult Leader
* Connie Craig as Bald cult girl
* Ivitch Fraser as Young cult girl
* Terry-Lynn Brumfield as Sleazy cult girl
* Ted Raimi as Chain Man
* Kirk Haas as The Stabber
* Al Johnston as Big Biker
* Chuck Morris as Puke Biker
* Scott Mitchell as Madhatter
* Scott Spiegel as Pin Cushion
* Glenn Barr as Archer
* Marek Pacholec as Bat Man

=== Others/Uncredited === David Kelly Gary Jones
* Josh Becker ... Hood Crushed Under the Car/Shot Cult Leader (uncredited)
* Bruce Campbell ... Video Newscaster (uncredited) Don Campbell ... Marine (uncredited) Brian Schulz ... Jack Stryker (uncredited)

==Production==
Thou Shalt Not Kill... Except was originally produced in 1980 as a Super-8 film entitled Strykers War, designed to get interest from investors; Campbell and Becker drafted the story ideas while returning home from the Tennessee set of The Evil Dead. The interior sets were primarily Bruce Campbells garage in suburban Detroit, Michigan, dressed up as either a military base or Strykers house. The Vietnam scenes were filmed in Hartland Township, Michigan|Hartland, though the overhead shots consist solely of stock footage.
 Foley effects created for The Evil Dead. The films release was, like The Evil Dead, handled primarily by press agent Irvin Shapiro. Shapiro suggested the final title, over Beckers objections; this is similar to Shapiros summary retitling of The Book of the Dead to The Evil Dead.

== Release ==
Thou Shalt Not Kill... Except was given a limited release theatrically in the United States by Film World Distributors in 1985. 

The film was released on VHS by Starmaker Video in the late 1980s. It was later released on DVD in the United States by Anchor Bay Entertainment in 2002.   This version is currently out of print. On April 10, 2012 Synapse released a Blu-ray/DVD combo pack of the film containing a new transfer and extras.

== Legacy ==
=== Following ===
A small but devoted cult following has arisen around this film. Josh Beckers website is notable for comments of some fans of the film, who hail it as one of the great works in American filmography. Some groups have even devoted significant new artwork to the film, including reimaging the soundtrack, re-editing the film and other "tributes". One of the best known fans group, based in Stockton, California, have commemorated the film through communal art projects, featuring extravagant parties to debut their creations; a tradition that has now lasted over 15 years.

=== In popular culture === Entombed song Out of Hand, from the album Wolverine Blues.

== References ==
 

== External links ==
*  
* Becker, Josh. Campbell, Bruce: Thou Shalt Not Kill... Except DVD audio commentary.

 

 
 
 
 
 
 
 
 