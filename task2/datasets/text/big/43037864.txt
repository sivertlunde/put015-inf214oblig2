Community Service: The Movie

 
 
 

{{Infobox film
| name           = Community Service The Movie
| image          = File:Community Service The Movie Poster.png
| alt            = 
| caption        = 
| film name      = 
| director       = Joseph Patrick Kelly
| producer       = 
| writer         = Joseph Patrick Kelly, David Berry
| screenplay     = 
| story          = 
| based on       =  
| starring       = Christopher Woods, William Meyers, Joseph P. Kelly
| narrator       = 
| music          = John Centrone
| cinematography = Don Grimble, Ben Struble
| editing        = Joseph P. Kelly, Franklin Kielar, Korey L. Owens
| studio         = Rusty Nail Productions
| distributor    = 
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American slasher film written and directed by Joseph Patrick Kelly and is his feature film directorial debut. The movie was released on July 21, 2012 and stars Chris Woods Marlins as a community service officer that has to deal with an escaped serial killer who dealing with his own posttraumatic stress disorder.    The project was begun while the filmmaker was in college, and serves as a prequel to Bloody Island, which has a projected release date of 2016.     

==Synopsis==
Years ago Billy Fouls (William Meyer) was teased and tormented to the point where he had to be committed to a psychiatric center. Now an adult, Billy decides to break out of the center after hearing that one of his bullies, Bob (Christopher Woods), is now a community service officer and will be holding a program at a campground located very near to his institution. Eager to exact his revenge, Billy escapes and begins to plan a series of bloody murders.

==Cast==
*Christopher Woods as Officer Bob Butterfield
*William Meyer as Billy Fouls
*Joseph P. Kelly as 
*Iliana Garcia as Danelia
*Caitlin Kenyon as 
*Hope Tomaselli as Paige
*Renell Edwards as Smith
*Marissa Mynter as Molly
*Daniel Trinh as Dakota
*Tristan MacAvery as Officer Jim Springfield

==Controversy==
In 2014 Community Service received some controversy over allegations that the films director had tried to extort money from the R. J. Reynolds Tobacco Company, which he would use towards the movie.       Kelly received an indictment at the U.S. District Court in Greensboro, on one charge of "extortion by interstate communication".   The indictment states that Kelly had been hired by Reynolds in 2012 to film a mock trial, which the companys attorneys would use in preparation for an impending court case. Lawyers for Reynolds stated that Kelly had not returned a provided confidentiality agreement and that the company had received emails in January 2013 that they claim are Kelly asking for money, one of which stated that he was "willing to take a contribution of $15,000 to a Kickstarter campaign for a horror film titled “Community Service The Movie.”    The filmmaker states he was interviewed by FBI agents and that his hard drives and equipment were seized.ex 

==Recognition==
===Awards and nominations===
* 2012, won Best Grindhouse Film at Buffalo Niagara Film Festival
* 2012, nominated Best Feature at New Orleans Film Festival
* 2013, won Excellence in the Art of Filmmaking at Palm Beach International Film Festival

==References==
 

==External links==
* 
* 

 