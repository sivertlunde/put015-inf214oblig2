Murders in the Rue Morgue (1971 film)
 
 
{{Infobox film name = Murders in the Rue Morgue image = Murders in the Rue Morgue 1971.jpg caption = director = Gordon Hessler producer = Louis M. Heyward writer = Edgar Allan Poe (story) Christopher Wicking Henry Slesar starring = Michael Dunn music = Waldo de los Ríos cinematography = Manuel Berenguer editing = Max Benedict distributor = American International Pictures released =   runtime = 87 min / 98 min restored directors cut country = United States language = English
|budget =
}} story of the same name, although it departs from the story in several significant aspects, at times more resembling Gaston Lerouxs Phantom of the Opera.  In an interview on the films DVD, Hessler said that he thought everyone already knew the ending of the story, so he felt it necessary to reinvent the plot. According to IMDB.com, the film was banned in Finland in 1972.

==Plot==
 
The story revolves around an early 20th century theatre troupe in Paris specializing in gory, naturalistic horror plays in the fashion of the Grand Guignol.  The director, Cesar Charron (Jason Robards), is presenting Poes "Murders in the Rue Morgue".  Cesars wife, the actress Madeline (Christine Kaufmann), whose mother (Lilli Palmer) had been murdered by axe, is haunted by nightmares of an ax-wielding man.  Then, suddenly, Rene Marot (Herbert Lom), a former lover of Madelines mother thought long dead after being horribly disfigured on stage, mysteriously returns and begins murdering members and ex-members of the acting troupe, confounding the Paris police, who initially suspect Cesar.

==Cast==
* Jason Robards as Cesar Charron
* Herbert Lom as Rene Marot
* Christine Kaufmann as Madeleine Charron
* Adolfo Celi as Inspector Vidocq
* Maria Perschy as Genevre Michael Dunn as Pierre Triboulet
* Lilli Palmer as Mrs. Charron
* Peter Arne as Aubert
* Rosalind Elliot as Gabrielle
* Marshall Jones as Luigi Orsini
* María Martín as Madam Adolphe
* Ruth Plattes as Orsinis Assistant Rafael Hernández as Member of Repertory Company
* Pamela McInnes as Member of Repertory Company
* Sally Longley as Member of Repertory Company
* John Mansell as Member of Repertory Company

==Restored directors cut==
 
American International Pictures cut the film against Gordon Hesslers wishes, removing 11 minutes of footage and adding color tints to the flash forwards and Flashback (narrative)|flashbacks.  The ending was also slightly altered.  In 2002, a directors cut appeared on American cable television, restoring the missing footage and the films ending.  The MGM DVD release of the film is in the restored directors cut format.  A featurette is included with director Hessler describing filming, working with the actors, restoration, etc.

==References==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 