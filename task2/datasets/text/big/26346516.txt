Valley of the Wolves: Gladio
{{Infobox film
| name           = Valley of the Wolves: Gladio
| image          = VotWGladioFilmPoster.jpg
| caption        = Theatrical poster
| director       = Sadullah Şentürk
| producer       = Raci Şaşmaz 
| writer         = Cüneyt Aysan Bahadır Özdener Raci Şaşmaz  
| starring       = Musa Uzunlar Ayfer Dönmez Tuğrul Çetiner
| music          = Gökhan Kırdar
| cinematography = Selahattin Sancaklı
| editing        = Kemalettin Osmanlı
| studio         = Pana Film
| distributor    = Özen Film Maxximum Film und Kunst
| released       =  
| country        = Turkey
| language       = Turkish
| runtime        = 122 minutes
| gross          = $4,703,086
}}
Valley of the Wolves: Gladio ( ) is a 2009   (2006) and   (2010).            

== Synopsis ==
Retired security intelligence agent Iskender Buyuk (Alexander the Great) has kept his governments secrets for years, but when the men he has sworn to protect suddenly deserts him, Isekender finds himself in the defendants chair, with only young and inexperienced lawyer Ayse to represent his interests. Angered by this turn of events, Iskender decides to strike back against his one-time employers by revealing all he knows about covert op missions.

== Cast ==
*Musa Uzunlar – İskender Büyük
*Tuğrul Çetiner – Bülent Fuat Aras
*Ayfer Dönmez – Ayse
*Ali Başar – Shahid Major Ahmet Cem Ersever
*Sezai Aydın – Turgut Özal 
*Işıl Ertuna – Semra Özal
*Hakan İlçin – Gendarmerie Commander
*Köksal Engür – Process notes General
*Ali Rıza Soydan – Şener Pasha 
*Sinan Pekinton – Judge
*Uğur Taşdemir – Announcer
*Hakan Vanlı – Journalist Ali
*Celalettin Demirel – Head guards
*Yurdaer Tosun – Ercüment
*Buket Aslan – İskenders Girlfriend 
*Hüseyin Yirik – İskenders lawyer 
*Erol Alpsoykan – Av. Haşim İçtürk 
*Sinan Altuntaş – Security
*Mustafa Develi – Abdullah Öcalan 
*Serap Ergen – Kurdish Women
*Rafet Özdemir – Prime Minister 
*Cengiz Gürkısmet – Maestro 
*Sinem Öçalır – Clerk 
*Yıldıray Yıldızoğlu – Usher
*Ülkü Şahiner – Pashas wife notes client

==Release==
The film opened in 311 screens across Turkey on   at number 2 in the box office chart with an opening weekend gross of $1,510,896.   

==Reception==

===Box office=== Turkish film of 2009 with a total worldwide gross of $4,703,086. 

===Reviews===
Turkish Daily News reviewer Emrah Güler, states that the film "comes to theaters in the heat of the Ergenekon investigation, an alleged ultra-nationalistic organization with ties in the military, media and justice, and accused of terrorism, a media-favorite for the last six months," and "real questions on the terrorist organization PKK, coups in the last half-a-century, and alleged assassinations against previous presidents are answered through a fictitious deep throat". He recommends the film to "those who couldn’t get more of the original series, and those who feed on state conspiracies", but says that "the movie features a plethora of plot holes, inconsistencies within the script, with real time events, and with its predecessors. Those who are hoping for impressive action scenes like in Kurtlar Vadisi – Irak go home empty-handed as well". He finishes by saying: "those who refrain from subjective political dramas, and those who’re tired of hearing and reading about the never ending Ergenekon stories" should avoid it.  

According to Betül Akkaya Demirbaş in Todays Zaman: "It addresses Turkey’s years-long adventure with the deep state and illegal formations nested within the state and aims to provide an opportunity for movie fans to closely look at the “deep gangs” that attempted to stir and divide Turkey with subversive plots".

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 