The Mailbox (film)

{{Infobox film
| name           = The Mailbox
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = David K. Jacobs
| producer       = David K. Jacobs
| writer         = 
| screenplay     = David K. Jacobs
| story          = Florence Doyle Putt
| based on       =  
| starring       =  
| narrator       = 
| music          = Merrill Jenson Reed Smoot
| editing        = James W. Dearden BYU Motion Picture Studios
| distributor    = The Church of Jesus Christ of Latter-day Saints
| released       = 1977  
| runtime        = 24 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Mailbox is a 1977   films.       

==Plot==
An old womans loneliness is amplified as she daily walks to the mailbox, only to find nothing there for her. Her neighbors and the mailman provide some relief, but her family doesnt seem to care.

==Cast==
* Lethe Tatge as Lethe Anderson
* Rachel Jacobs as Rachel Johnson
* Rebbeca Glade as Sharon Johnson
* Alan Nash as Mike the Mailman
* Martha Henstrom as Myra (voice)
* Winkie Horman as Susan (voice)

==Reception==
Considered as among the best known films produced at BYU,    and "It is clear that the tragedy is not in the death, but in the emptiness of the mailbox."   

==See also==
 
* Chris Conkling

==References==
 

==External links==
*  
*  
*   at LDSFilm.com
*   on YouTube from the Mormon Channel

 

 
 
 


 