Indian Summer (1993 film)
 
{{Infobox film
| name        = Indian Summer
| image       = Indian Summer 1993.jpg
| image size  =
| caption     = Theatrical poster
| director    = Mike Binder
| producer    = Jim Kouf Lynn Kouf Robert F. Newmyer Jeffrey Silver
| writer      = Mike Binder Kimberly Williams
| music       = Miles Goodman
| cinematography = Newton Thomas Sigel
| editing     = Adam Weiss
| studio      = Touchstone Pictures Outlaw Productions Buena Vista Pictures
| runtime     = 97 minutes
| country     = United States
| released    =  
| language    = English
| budget      = $9 million
| gross       = $14,904,910
}}

Indian Summer is a 1993 comedy drama film written and directed by Mike Binder. The movie was filmed and set on-location at Camp Tamakwa, a summer camp located in Ontario, Canada, where Binder himself had attended in his childhood. The film features an ensemble cast, including film director Sam Raimi, a childhood friend of Binders, who plays a supporting role as handyman Stick Coder.

==Plot==
"Unca" Lou Handler (Alan Arkin), the beloved camp director and owner of Camp Tamakwa, invites eight former campers, all now adults, back to the camp to announce his retirement. Lou claims to have chosen these friends as they were the group from the camps "golden years" 20 years previously. Once there, the group comes to feel nostalgic memories of their youth and the unresolved feelings for each other begin to surface.

==Cast==
* Alan Arkin as "Unca" Lou Handler
* Matt Craven as Jamie Ross
* Diane Lane as Beth Warden
* Bill Paxton as Jack Belston
* Elizabeth Perkins as Jennifer Morton
* Kevin Pollak as Brad Berman
* Sam Raimi as Stick Coder
* Kimberly Williams-Paisley as Gwen Daugherty
* Julie Warner as Kelly Berman
* Vincent Spano as Matthew Berman

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 

 

 