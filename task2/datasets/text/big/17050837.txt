Suez (film)
{{Infobox film
| name           = Suez
| image          = NTA Suez 1957.jpg
| caption        = Advertisement for the 1957 TV airing of Suez on the NTA Film Network
| director       = Allan Dwan
| producer       = Darryl F. Zanuck (in charge of production) Gene Markey (associate producer) Philip Dunne Julien Josephson Annabella
| music          = 
| cinematography = 
| editing        = Barbara McLean
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} French diplomat Ferdinand de Lesseps.  The screenplay is so highly fictionalized that, upon the films release in France, de Lesseps descendants sued (unsuccessfully) for libel. 
 Original Music Sound Recording (uncredited Edmund H. Hansen).   

==Plot== Leon Ames). Bonaparte sees to it that both she and de Lesseps are invited to his reception. At the party, a fortuneteller predicts that Eugenie will have a troubled life, but also wear a crown, and that de Lesseps will dig a ditch. Entranced by Eugenies beauty, Bonaparte arranges for his romantic rival to be assigned to a diplomatic post in Egypt, joining his father, Count Mathieu de Lesseps (Henry Stephenson), the Consul-General. De Lesseps impulsively asks Eugenie to marry him immediately, but she turns him down.

In Egypt, de Lesseps befriends two people who will have a great influence on his life: Toni Pellerin (Annabella (actress)|Annabella), a tomboy being raised by her grandfather, French Sergeant Pellerin (Sig Rumann); and Prince Said (J. Edward Bromberg), the indolent heir of his father, Mohammed Ali (Maurice Moskovitch), the Viceroy (ruler) of Egypt. Toni makes it clear that she has fallen in love with him, but de Lesseps still pines for Eugenie. Count de Lesseps leaves for France, leaving his son to take his place.

One day, after a brief rainstorm in the desert, de Lesseps sees the water draining into the sea and comes up with the idea for the Suez Canal. He departs for Paris to raise the necessary funding; Toni goes along as well. He presents his proposal to Bonaparte, but is rejected. He is also disheartened to learn that Eugenie is now very close to Bonaparte.

France is on the verge of civil war between Bonaparte and the French Assembly, led by Count de Lesseps and others. Eugenie persuades Ferdinand de Lesseps to pass along Bonapartes proposal asking the Assembly to disband, giving Bonapartes promise to reconvene it once the civil unrest has been defused. Despite their misgivings, the members of the Assembly agree, only to be betrayed and arrested. Bonaparte assumes the throne of the revived French Empire, just as Count de Lesseps had feared. The news causes the count to suffer a fatal stroke. Ferdinand de Lesseps is outraged, but Toni persuades him to do nothing. In return for de Lesseps help, Bonaparte (now Emperor Napoleon III), withdraws his objections to the canal, and construction commences under de Lesseps direction.

The building of the canal progresses despite Turkish sabotage. However, Napoleon unexpectedly withdraws his support out of political necessity; he needs to appease Great Britain, and the British Prime Minister (George Zucco) is firmly opposed to the project. Prince Said bankrupts himself to keep the venture going, but it is not enough. De Lesseps goes to England to plead his case. The Prime Minister is unmoved, but the leader of the opposition, Benjamin Disraeli (Miles Mander), is enthusiastic about the project. Disraeli tells him to return to Egypt and pray that Disraeli wins the upcoming general election. He does, and funding is assured.

As the canal nears completion, an enormous sandstorm threatens everything. When de Lesseps is knocked unconscious by flying debris, Toni rescues him by tying him to a wooden post, but is herself swept away and killed. De Lesseps finishes the canal and is honored by Eugenie, now Empress of France after her marriage to Napoleon III. 

==Cast==
 
 
*Tyrone Power as Ferdinand de Lesseps
*Loretta Young as Countess Eugenie De Montijo Annabella as Toni Pellerin
*J. Edward Bromberg as Prince Said
*Joseph Schildkraut as Vicomte Rene De Latour
*Henry Stephenson as Count Mathieu de Lesseps
*Sidney Blackmer as Marquis Du Brey Mohammed Ali
*Sig Rumann as Sergeant Pellerin
*Nigel Bruce as Sir Malcolm Cameron
*Miles Mander as Benjamin Disraeli
*George Zucco as Prime Minister
  Leon Ames as Louis Napoleon
*Rafaela Ottiano as Maria De Teba
*Victor Varconi as Victor Hugo
*Georges Renavent as Bank President
*Frank Reicher as General Changarnier
*Carlos De Valdez as Count Hatzfeldt
*Jacques Lory as Millet
*Albert Conti as Joseph Fourier|M. Fourier
*Brandon Hurst as Franz Liszt
*Marcelle Corday as Mme. Paquineau
*Odette Myrtle as Duchess
*Egon Brecher as Doctor
*Alphonse Martell as General St. Arnaud
*Montague Shaw as Elderly Man
*Leonard Mudie as Campaign Manager
 

==Production== Annabella were cast in the same month, thereby ruling out a role for Simon.  On April 23, 1939, six months after Suezs premiere, Tyrone Power, age 24, and Annabella, age 31, were married (his first marriage, her third), with the union lasting until 1948.

==Evaluation in film guides==
Leonard Maltins Movie Guide gives Suez 3 stars (out of 4) and, in its early editions, states, "elaborate Darryl Zanuck production surrounds good cast with 1850s France and Egypt". Since 1993, however, the write-ups final lines have been revised to read, "entertaining and elaborate hokum which apparently bears no resemblance to history. Power and Annabella later wed in real life". Steven H. Scheuers Movies on TV also gives it 3 stars (out of 4), commenting, "well-photographed and lavish film which is supposed to tell the true story of how the Suez Canal was built. If it had done that, it might have been a great film instead of another colorful epic."

TimeOut Film Guide observes that "this highlights both Dwans virtues and his flaws. The action/catastrophe are marvellously assured without ever going over the top, as is the handling of the human drama." It concludes that "Dwan—who is concerned with the modest virtues of honesty and fairness—is unable, indeed unwilling, to so combine both strands of his story. Accordingly, Suez is a series of incidents unconnected by dramatic urgency; Dwan, quite simply, is unconcerned with the building of the canal."

Assigning 3½ stars (out of 5), The Motion Picture Guide described it as "typically lavish Hollywood biography that bears even less relation to the truth than usual for the genre" and later went on to state that "inane dialog is the biggest culprit in this ridiculous view of 19th-Century French politics".  The write-up also mentions that in his 1971 biography by Peter Bogdanovich, The Last Pioneer, Allan Dwan expresses admiration for Annabellas professionalism, in particular while filming the epic sandstorm. It goes on to state that "when the film was shown in France, the descendants of de Lesseps sued Fox, claiming that the engineer had been 54 when he first went to Egypt, and never had an affair with the Empress Eugenie. A French court threw out the case, determining that the film brought more honor to France than dishonor on the family. The film follows the formula of the other Zanuck biographical bowdlerizations: smart sets, great costumes, romance, fine special effects, and complete disregard of fact. Original release prints were sepia tinted." 

==References==
 

==Reviews==
* 
* 
* 
* 

==External links==
*  
*  
* 
*  at TV Guide (1987 write-up was originally published in The Motion Picture Guide)
* 
* 
* 
*  Silver Lode, Slightly Scarlet and 1917s A Modern Musketeer; sections focusing on themes common to Dwans work are highlighted by sub-headers "Water Works", "Politics: Rivalries Between Government Parties", "Camera Movement", "Symmetry" and "Hats"

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 