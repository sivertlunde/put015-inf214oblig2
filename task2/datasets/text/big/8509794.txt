Meet the Deedles
{{Infobox film
| name = Meet the Deedles
| image = MeetTheDeedles.jpg
| caption = Theatrical release poster
| director = Steve Boyum
| producer = Dale Pollock Aaron Meyerson
| writer = Jim Herzfeld John Ashton Robert Englund Dennis Hopper
| music = Steve Bartek
| cinematography = David Hennings
| editing = Alan Cody
| studio =  Walt Disney Pictures DIC Entertainment Peak Productions Buena Vista Pictures
| released =  
| runtime = 93 minutes
| country = United States
| language = English
| budget = $24 million
| gross = $4,356,216 
}}
 original films Johnny Tsunami (which Boyum also directed and Van Wormer co-starred in) and Rip Girls, released in 1999 and 2000 respectively. Despite portraying teens, Walker and Van Wormer were in their early twenties when the movie was filmed.

==Plot==
Phil Deedle (Paul Walker) and Stew Deedle (Steve Van Wormer) are two fraternal twin brothers who are complete surf bums living in Hawaii off of their wealthy father. After the two decide to skip out on school to go on a special surfing expedition with friends for their 18th birthday, the twins are expelled from their high school for truancy. Their father Elton Deedle (Eric Braeden) has become tired of their insolence and lazy lifestyle and thus decides to ship them to a boot camp to straighten them out. The boys are horrified that they’re going to have to fly state-side in order for this to happen, as the camp is located in Yellowstone National Park in Wyoming.
The boys arrive in Wyoming ill prepared for the far-cooler weather, disembarking from their plane dressed in shorts, flip-flops and hula shirts. They attract the attention of a small crowd when they summon a remote controlled crate containing a mini-bar that they use to create hula drinks. Afterwards they leave the terminal, noticing a gruff looking mountain man whom they make fun of, to their shock the man reveals a sign with their last names written on it, indicating he’s their ride. Major Flowers (M. C. Gainey) is already disgusted with their behavior, on the drive back to Yellowstone from the airport, Flowers reveals that his once successful camp was shut down by the children’s parents and their lawyers but the camp wasn’t necessary anymore because he intended for the Deedles to undergo survival training. Flowers becomes distracted in describing his plan and fails to pay attention to the road, eventually driving over the edge of a hillside and crashing into a river. Thinking he had killed the Deedles, he runs off thinking he’ll hide from the authorities.
The Deedles awaken from their crash and find some women’s clothing that had caught onto Flower’s truck before it crashed, since it was their only option for dry clothes they opted to wear them. After, they begin to hike towards the parks main entrance, they find some of their gear and decide to land-luge down a long hill with their hula crate remotely in tow. They bypass a long line of traffic awaiting entry into Yellowstone but unable to slow down or gain enough control of their boards they crash into the park’s sign and are then collided into by the hula machine—cracking their helmets off. When the Park Rangers arrived, the only identity the boys had on them were the names “Mel” written on Phil’s sweatshirt tag and “Mo” written on Stew’s. After the incident the boys are carted off to the Ranger medical center and nursed back to health and decide to assume the false identities as it appears to be a one-way ticket to success and approval of their father.

The boys find it difficult to live in their new identities, as the park rangers have already learned that the “real” Mel and Mo are vegetarians, thus the boys are forced to become such as well in order to maintain their cover. Because of their dimwitted surfer nature, the boys do not click well with the commanding park ranger, Captain Pine, although Phil falls for his stepdaughter Lt. Jesse Ryan whom the Captain is very protective of. This leads to a series of accidents all of which the Deedles are to blame for and land them in disfavor with Captain Pine.
Through their trials, the Deedles inadvertently discover that an ex-park ranger named Slater (Dennis Hopper) feels he’s been betrayed by the park, the Rangers and mainly Captain Pine and so he has created a secret bunker underground near Old Faithful and has devised a plan to divert Old Faithful’s waterflow into a new geyser he’s created on his own land which he is christened “New Faithful”. Unable to win any support for investigation from their fellow rangers, the Deedles take matters into their own hands and attempt to thwart Slater’s plan.
However, the real Mel and Mo arrive at the Ranger’s base at a very untimely moment to reveal that Phil and Stew are not who they say they are. Pine, Jesse and the other rangers are disgusted with their dishonesty and banish them from Yellowstone.
As the Deedles are walking down the highway attempting to hitch-hike, the big picture dawns on Phil and they devise a plan to get back into Yellowstone in order to stop Slater once-and-for-all. They rig a grappling hook into a grate in the roadway, and as an unsuspecting car passes it would catch the undercarriage and tow the Deedles (who are wearing parachutes), essentially a land version of parasailing. From a few hundred feet up in the air, they both notice a familiar stench and discover the person towing them is none other than Major Flowers—who becomes distracted by his stowaways and subsequently drives over a cliff (again) landing himself in a river.
The boys parachute to the entrance for Slaters underground tunnel network and go underground, where they locate and subdue Slater’s two cronies Mr. Crabbe (Richard Lineback) and Mr. Nemo (Robert Englund). They devise a plan to re-divert the water from New Faithful back to Old Faithful, and to escape by donning heat-resistance suits and escaping through the plumbing and through Old Faithful’s spout.
The plan succeeds, Slater is exposed and the Deedles become heroes for saving Old Faithful. In a fortunate turn of events, the force of the explosion of water was so great it created a small lake which naturally created waves large enough to surf on, and the boys were able to create a park out of it. Earning the respect of their father for their success, from Captain Pine for their help in saving Yellowstone, and for Phil the love of Jesse.

==Cast==
* Paul Walker as Phil Deedle (Phillip Andrew Preston Deedle III)
* Steve Van Wormer as Stew Deedle (Stuart Michael James Deedle Esquire)
* A. J. Langer as Lt. Jesse Ryan John Ashton as Capt. Douglas Pine
* Robert Englund as Nemo
* Dennis Hopper as Frank Slater
* Eric Braeden as Elton Deedle
* Richard Lineback as Crabbe
* M. C. Gainey as Major Flowers
* Ana Gasteyer as Mel
* Megan Cavanagh as Mo
* Hattie Winston as Jo-Claire

The film also features cameos from former Oingo Boingo members Steve Bartek, Johnny "Vatos" Hernandez, Carl Graves, and Sam "Sluggo" Phipps as the band at the luau.

==Reception==
Reviews for the film were almost unanimously negative, claiming the film was nothing more than a poor attempt to revive the goofball duo genre of films such as Bill & Teds Excellent Adventure and Dumb & Dumber. Areas of the film that drew the most criticism were its two-dimensional characters, overuse of surfer slang, ludicrous plot, and questionable morals.   

==Soundtrack==
 
The original music for the film was composed by former Oingo Boingo member Steve Bartek. Most of the songs featured on the soundtrack were from third-wave ska bands, as the genre was at the peak of its popularity at the time of the films release.

*"Wrong Thing Right Then" - The Mighty Mighty Bosstones
*"Lady Luck" - Dance Hall Crashers Goldfinger
*"Dr. Bones" - Cherry Poppin Daddies Hepcat
*"Psycho Gremmie" - Gary Hoey
*"For You" - Save Ferris
*"Go Where You Go" - Geggy Tah Radish
*"American Homie
*"Hawaii Five-O" - Perfect Thyroid
*"Who Are Those Guys?" - Steve Bartek

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 