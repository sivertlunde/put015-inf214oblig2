Anna Pavlova (film)
{{Infobox film
| name           = Anna Pavlova
| image          = 
| caption        = 
| director       = Emil Loteanu
| producer       = 
| screenplay         = Emil Loteanu
| story         = Emil Loteanu
| starring       = {{plainlist|
* Galina Belyayeva
* Lina Buldakova
* Sergey Shakurov
* Vsevolod Larionov
* James Fox }}
| music          = Eugen Doga
| cinematography = Yevgeny Guslinsky
| editing        = {{Plainlist|
* Irina Kalatikova
* Elena Galkina
* Jim Connock }}
| studio         = {{plainlist|
* Poseidon Films
* Mosfilm
* Sovinfilm }}
| distributor    = Poseidon Film Distributors
| runtime        = 155 minutes 275 minutes (5 parts) released        = 1983
| country        = {{plainlist|
* United Kingdom
* Soviet Union }}
| language       = {{Plainlist|
* English
* Russian }}
| budget         =
| gross          =
}} Britain and the Soviet Union. 

==Main cast==
* Galina Belyayeva - Anna Pavlova 
* Lina Buldakova - Pavlova as a Child 
* Sergey Shakurov - Mikhail Fokine 
* Vsevolod Larionov - Sergei Diaghilev 
* James Fox - Victor DAndre 
* Jacques Debary - Saint-Saëns 
* Georgio Dimitriou - Enrico Cecchetti 
* Martin Scorsese - Gatti-Cassaza
* Roy Kinnear - Gardener
* Bruce Forsyth - Alfred Batt
* Michael Kradunin - Vaslav Nijinsky

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 