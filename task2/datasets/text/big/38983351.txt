Arcadia (film)
{{Infobox film
| name           = Arcadia
| image          = 
| caption        = 
| director       = Olivia Silver
| producer       = Julien Favre Jai Stefan Silenn Thomas
| writer         = Olivia Silver John Hawkes Ryan Simpkins Ty Simpkins Kendall Toole
| music          = The Low Anthem
| cinematography = Eric Lin
| editing        = Jennifer Lee
| studio         = DViant Films Madrose Productions Poisson Rouge Pictures Shrink Media
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Arcadia is a 2012 American drama film written and directed by Olivia Silver, produced by A Poisson Rouge Pictures and DViant Films. It won the Crystal Bears, Generation Kplus at 2012 Berlinale  and the Grand Prize at the Cine Junior Festival. 

==Plot==
Twelve-year-old Gretas dad Tom is moving the kids cross-country, promising a California paradise and packing half the household into a dented station wagon. Mom is supposed to join them later. But as they travel through forests, plains and deserts, stopping at fast food joints, shoddy motels, and a poor substitute for the Grand Canyon, Greta gradually realizes that her family is falling apart.

==Cast== John Hawkes as Tom
*Ryan Simpkins as Greta
*Ty Simpkins as Nat
*Kendall Toole as Caroline

==Critical reception==
Arcadia scored highly with critics, and played to sold out theaters at the Berlin Film Festival. The New York Times Manohla Dargis wrote: "Ms. Silver’s ability to translate the liminal into cinematic terms, to catch those moments between innocence and knowing, childhood and adulthood, unforgiving and forgiving, makes her someone to watch"

==Awards and nominations==
* 62nd Berlin International Film Festival
** Crystal Bears, Generation Kplus    
* 23rd Cine Junior Festival
** Grand Prize at the Cine Junior Festival   
** "Grain à Démoudre" Prize (from young people aged 10–25 years) 
* 13th Woodstock Film Festival
** Official selection 

==Box office==
 

==DVD release==
 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 