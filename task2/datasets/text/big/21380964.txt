Ski School (film)
{{Infobox Film name = Ski School image = Poster of the movie Ski School.jpg caption =  director = Damian Lee writer = David Mitchell starring = Mark Thomas Miller Spencer Rochfort John Pyper-Ferguson distributor = Moviestore Entertainment released =   runtime = 95 minutes (USA), 89 minutes elsewhere country = Canada language = English budget =  gross = $18,476 
}}

Ski School is a 1991 comedy film about a fictional ski school starring Dean Cameron. A sequel, Ski School 2, followed in 1994, also starring Cameron.

==Plot== Mark Thomas Miller) to save their jobs. They recruit hotshot newcomer, John Roland (Tom Breznahan) to help them win an end-of-the-season skiing competition and also play a series of hilarious pranks on Reid and his cronies (especially, Derek and Eric) along the way.

==Cast==
* Dean Cameron as Dave Marshak
* Tom Breznahan as John E. Roland
* Patrick Labyorteaux as Ed Young Mark Thomas Miller as Reid Janssens
* Spencer Rochfort as Derek Stevens
* Darlene Vogel as Lori
* Stuart Fratkin as Fitz Fitzgerald
* Charlie Spradling as Paulette
* Ava Fabian as Victoria
* Gaetana Korbin as Bridget
* Mark Brandon as Anton Bryce
* John Pyper-Ferguson as Erich Blor
* Johnny Askwith as Bart

==Production notes==
Ski School was filmed in Los Angeles, California and on location in Mount Hood, Oregon and Whistler, British Columbia.
 Lock Up, a late-80s musical outfit featuring Tom Morello on guitar, pre-dating his Rage Against the Machine fame.

==Release==

The movie was released theatrically in the United States on January 11, 1991, although released in Canada the previous year by Cineplex Odeon. Later that year it was released on videocassette in the United States by HBO Video. In 2007, MGM released the movie on DVD. Despite the fact that the back of the DVD says the movie is presented in widescreen, it is actually presented in pan and scan.

==References==
 

==External links==
* 
*  at Allrovi
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 