The Hidden Half
{{Infobox film
| name = The Hidden Truth  Nimeh-ye Penhan 
| image = 
| caption = 
| director = Tahmineh Milani
| producer = Tahmineh Milani 
| starring = Niki Karimi Mohammad Nikbin Atila Pesiani Soghra Obeisi Akbar Moazezi
| music = Amir Moini
| cinematography = Mahmoud Kalari
| sound = Parviz Abnar
| sound mixer = Massoud Behnam
| runtime = 103 minutes
| country = Iran
| language = Persian
}}
The Hidden Half (Nimeh-ye Penhan; نيمه پنهان) is a 2001 controversial drama film, written and directed by Iranian feminist Tahmineh Milani. The film stars Niki Karimi, Mohammad Nikbin and Atila Pesiani. The story is told as a manuscript of Fereshteh Samimis (Niki Karimi) life, revealing her politically active and romantic past life, to her husband (Mohammad Nikbin).
  Golden Pyramid award from the Cairo International Film Festival, and won the award for Best Artistic Contribution, as well as being nominated for the PFS award from the Political Film Society in 2002.

==Plot==
The story is told through a manuscript which Fereshteh (Niki Karimi) leaves for her husband (Atila Pesiani) to read. This manuscript tells the story of her life when she was 18 years old and attending the University of Tehran. Fereshteh becomes interested in the fall of the Shah and other such political topics, and so joins a revolutionary, communist group. She gives out fliers, which was illegal as it was  the Islamic Revolution period of 1978. Through the group, the young girl meets an older man, a pro-Mossadeq-era intellectual and magazine editor, Roozebeh (Mohammad Nikbin), who is intrigued by her “naive dedication to a noble cause".    While government authorities inspect the university and streets of Tehran for revolutionary activists, Roozebeh presents Fereshteh with an opportunity to flee abroad to England. However, when Roozebeh’s wife finds out about the young lady, she informs her of Roozebeh’s family. Thus, when Fereshteh finds out about the intellectual’s wife and son, as well as the fact that she is a lookalike of his childhood sweetheart, she cuts all contact with him. She then marries her husband and does not inform him of her hidden past, only to reveal it to him when he tells her about the woman who is sentenced to death, whom he is interviewing for an appeal. Fereshteh does this in hopes that her husband will listen to the woman in a less critical and more understanding view, and to reveal to him her hidden half.

==Cast==
*Niki Karimi as Fereshteh Samimi
*Mohammad Nikbin as Roozebeh Javid
*Atila Pesiani as Khosro Samimi
*Soghra Obeisi
*Akbar Moazezi as Mr. Rastegar
*Afarin Obeisi

==Crew==
*Tahmineh Milani – Producer
*Mohammad Nikbin – Producer
*Amir Moini – Original music
*Mahmoud Kalari – Cinematography
*Bahram Dehghani – Film editing
*Iraj Raminfar – Production design
*Mehri Shirazi – Makeup artist
*Parviz Abnar – Sound
*Massoud Behnam – Sound mixer

==Production==
Because of The Hidden Half, Tahmineh Milani was arrested in 2001 for two weeks by Irans Islamic Revolutionary Court, accusing her of supporting counterrevolutionaries through film. She was then out on bail.

==Reception==
Deborah Young of Film Reviews wrote, "The Hidden Half reprises the theme of womens complexly interwoven intellectual and sentimental lives, boldly touching on many controversial subjects as far as Iranian attitudes in regard to women go."

David Ehrenstein of The New York Times wrote 2001, "Out of prison, Milani is still not allowed to leave Iran. Whether she will ever get the chance to make another film there is doubtful, all the more reason not to miss this one."

==References==
 

==External links==
* 
*http://web.archive.org/web/20091028182146/http://www.geocities.com/polfilms/hiddenhalf.html
*http://www.answers.com/topic/the-hidden-half
*http://www.variety.com/review/VE1117797328/
*http://www.metacritic.com/movie/the-hidden-half

 
 
 