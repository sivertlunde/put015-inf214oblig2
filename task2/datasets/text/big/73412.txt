North by Northwest
 
{{Infobox film
| name           = North by Northwest
| image          = Northbynorthwest1.jpg
| image_size     =
| caption        = Theatrical release poster.
| alt            =
| director       = Alfred Hitchcock
| producer       = Alfred Hitchcock
| writer         = Ernest Lehman
| starring       = Cary Grant  Eva Marie Saint  James Mason Jessie Royce Landis 
| music          = Bernard Herrmann
| cinematography = Robert Burks
| editing        = George Tomasini
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 136 minutes
| country        = United States
| language       = English
| budget         = $4,326,000  . 
| gross          = $9,840,000 
}}
 spy thriller thriller film directed by Alfred Hitchcock and starring Cary Grant, Eva Marie Saint and James Mason. The screenplay was written by Ernest Lehman, who wanted to write "the Hitchcock picture to end all Hitchcock pictures".   

North by Northwest is a tale of mistaken identity, with an innocent man pursued across the United States by agents of a mysterious organization who want to stop his interference in their plans to smuggle out microfilm containing government secrets.

This is one of several Hitchcock films with a music score by Bernard Herrmann and features a memorable opening title sequence by graphic designer Saul Bass. This film is generally cited as the first to feature extended use of kinetic typography in its opening credits. 
 greatest films of all time. In 1995, North by Northwest was selected for preservation in the National Film Registry by the United States Library of Congress, as being "culturally, historically, or aesthetically significant".

== Plot ==
 
Advertising executive Roger O. Thornhill is mistaken for George Kaplan and kidnapped by Valerian and Licht. The two take him to the Long Island estate of Lester Townsend. He is interrogated by a man he assumes to be Townsend, but who is actually spy Phillip Vandamm. Vandamms "associate" Leonard intends to get rid of Thornhill once they finish questioning him.

Thornhill is forced to drink bourbon, but manages to escape a staged driving accident. He is unable to get the authorities or even his mother to believe what happened, especially when a woman at Townsends residence says he got drunk at her dinner party; she also remarks that Townsend is a United Nations diplomat.

Thornhill and his mother go to Kaplans hotel room. While there, Thornhill answers the phone; it is one of Vandamms henchmen. Avoiding recapture, he goes to the United Nations Headquarters|U.N. General Assembly building to see Townsend, but finds that the diplomat is a stranger. Valerian throws a knife which hits Townsend in the back. He falls dead into Thornhills arms. Without thinking, Thornhill removes the knife, making it appear that he is the killer. He is forced to flee.

 

Knowing that Kaplan has a reservation at a Chicago hotel the next day, Thornhill sneaks onto the 20th Century Limited. He meets Eve Kendall, who hides Thornhill from policemen searching the train. Unknown to Thornhill, Eve is working with Vandamm and Leonard, who are in another compartment with Valerian. In Chicago, Kendall tells Thornhill she has arranged a meeting with Kaplan.

  crop duster dives toward Thornhill, narrowly missing him. He hides in a cornfield after the assailants fire at him with an automatic weapon, but the airplane dusts it with pesticide, forcing him out. He steps in front of a speeding tank truck, which stops barely in time. The airplane crashes into the tanker.
 Tarascan statue and departs. Thornhill tries to follow, only to find the exits covered by Valerian and Leonard. Trapped, he places nonsensical bids so that the police will be called to escort him away. Thornhill identifies himself as the fugitive wanted for Townsends murder and demands to be jailed, but the arresting officers clandestinely take him to the Professor instead. The Professor reveals that Kaplan does not exist. He was invented to distract Vandamm from the real government agent: Kendall. As he has inadvertently put Kendalls life in danger, Thornhill agrees to help maintain her cover.

At the Mount Rushmore visitor center, Thornhill poses as Kaplan to negotiate Vandamms turn over of Kendall for her prosecution as a spy. The deal is derailed when "Kaplan" confronts Kendall; she fires a handgun (later revealed to have been loaded with Blank (cartridge)|blanks) at him and flees. Paramedics load Thornhill in an ambulance then take him to a forest where he gets out of the ambulance uninjured. Thornhill and Kendall have a romantic goodbye in the forest, but Thornhill discovers she must not only return undercover, but go with Vandamm and Leonard on a plane to rendezvous "over there". The Professor has his driver stop Thornhill from preventing Kendall from returning, but Thornhill evades the Professors custody and finds Vandamms lair to rescue Kendall.

At Vandamms house Thornhill overhears that the sculpture holds microfilm and that Leonard knew Kendalls gun was loaded with blanks, and reveals such to Vandamm. Vandamm implies he will kill Kendall during the flight. Thornhill is able to inform Kendall they plan to kill her, but is captured. His escape from the house provides a distraction for Kendall to take the sculpture and run to Thornhill. Thornhill and Kendall realize they are on top of Mount Rushmore and climb down the mountain sculpture to escape. Thornhill fights Valerian, who falls from the cliff, but Leonard pushes Kendall over the side. While Thornhill is holding onto Kendall and the cliff face, Leonard stomps on his hand. Leonard is killed by a gunshot from a park ranger in a group including the Professor and the captured Vandamm. Later, Thornhill invites Kendall, as the new Mrs. Thornhill, onto the upper berth of a train that then enters a tunnel.

== Cast ==
 
 
* Cary Grant as Roger Thornhill
* Eva Marie Saint as Eve Kendall
* James Mason as Phillip Vandamm
* Jessie Royce Landis as Clara Thornhill
* Leo G. Carroll as The Professor
* Josephine Hutchinson as Mrs. Townsend
* Philip Ober as Lester Townsend
* Martin Landau as Leonard Adam Williams as Valerian
* Edward Platt as Victor Larrabee
* Robert Ellenstein as Licht
* Les Tremayne as Auctioneer
* Philip Coolidge as Dr. Cross
* Patrick McVey as Sergeant Flamm
* John Berardino as Sergeant Emile Klinger (uncredited)  
* Edward Binns as Captain Junket
* Ken Lynch as Charlie
* Malcolm Atterbury as Man at the crossroads (uncredited) 
 
List of Alfred Hitchcock cameo appearances|Hitchcocks cameo appearances are a signature occurrence in most of his films. In North by Northwest, he is seen getting a bus door slammed in his face, literally just as his credit is appearing on the screen.  There has been some speculation as to whether he made one of his rare second appearances, this time at around the 44 minute mark in drag. 

MGM wanted Cyd Charisse for the role played by Eva Marie Saint. Hitchcock stood by his choice. 

==Origins==
 
  during the scripting of another film project:

 Alfred Hitchcock had agreed to do a film for MGM, and they had chosen an adaptation of the novel The Wreck of the Mary Deare by Hammond Innes. Composer Bernard Herrmann had recommended that Hitchcock work with his friend Ernest Lehman. After a couple of weeks, Lehman offered to quit saying he didnt know what to do with the story. Hitchcock told him they got along great together and they would just write something else. Lehman said that he wanted to make the ultimate Hitchcock film. Hitchcock thought for a moment then said he had always wanted to do a chase across Mount Rushmore. 
 Lehman and Hitchcock spitballed more ideas: a murder at the United Nations Headquarters; a murder at a car plant in Detroit; a final showdown in Alaska. Eventually they settled on the U.N. murder for the opening and the chase across Mount Rushmore for the climax. 
 For the central idea, Hitchcock remembered something an American journalist had told him about spies creating a fake agent as a decoy. Perhaps their hero could be mistaken for this fictitious agent and end up on the run. They bought the idea from the journalist for $10,000. 

Lehman would sometimes repeat this story himself, as in the documentary   that accompanied the 2001 DVD release of the film. In his 2000 book Which Lie Did I Tell?, screenwriter William Goldman, commenting on the film, insists that it was Lehman who created North by Northwest and that many of Hitchcocks ideas were not used. Hitchcock had the idea of the hero being stranded in the middle of nowhere, but suggested the villains try to kill him with a tornado. John Brady, "The craft of the screenwriter", 1981. Page 202  Lehman responded, "but theyre trying to kill him. How are they going to work up a cyclone?" Then, as he told an interviewer; "I just cant tell you who said what to whom, but somewhere during that afternoon, the cyclone in the sky became the crop-duster plane." 
  fictitious agent and watched as the Germans wasted time following him around. Guernsey turned his idea into a story about an American traveling salesman who travels to the Middle East and is mistaken for a fictitious agent, becoming "saddled with a romantic and dangerous identity." Guernsey admitted that his treatment was full of "corn" and "lacking logic." He urged Hitchcock to do what he liked with the story. Hitchcock bought the sixty pages for $10,000.

Hitchcock often told journalists of an idea he had about Cary Grant hiding out from the villains inside Abraham Lincolns nose and being given away when he sneezes. He speculated that the film could be called "The Man in Lincolns Nose" (Lehmans version is that it was "The Man on Lincolns Nose" John Brady, "The craft of the screenwriter", 1981. Page 201 ) or even "The Man who Sneezed in Lincolns Nose," though he probably felt the latter was insulting to his adopted America.  Hitchcock sat on the idea, waiting for the right screenwriter to develop it. At one stage "The Man in Lincolns Nose" was touted as a collaboration with John Michael Hayes. When Lehman came on board, the traveling salesman – which had previously been suited to James Stewart – was adapted to a Madison Avenue advertising executive, a position which Lehman had formerly held.  In an interview in the book Screenwriters on Screenwriting (1995), Lehman stated that he had already written much of the screenplay before coming up with critical elements of the climax.   

== Production ==
This was the only Hitchcock film released by MGM. It is owned by Turner Entertainment – since 1996 a division of Warner Bros. – which owns the pre-1986 MGM library.

===Filming===
  is the site of a scene in the film]]
 High Society.) 

The car chase scene in which Thornhill is drunkenly careening along the edge of cliffs high above the ocean, supposedly on Long Island, was actually shot on the California coast, and in Griffith Park in Los Angeles, according to DVD audio commentary.

The  s "Le Paquebot ou LEstran", which features "alternating strips of sand and ocean blue bands stretch  to the edge of the canvas." 
 Stearman (Boeing Model 75) trainer. Like its N3N lookalike, many were used for agricultural purposes through the 1970s. The plane was piloted by Bob Coe, a local cropduster from Wasco.  Hitchcock placed replicas of square Indiana highway signs in the scene. In an extensive list of "1001 Greatest Movie Moments" of all time, the British film magazine Empire (film magazine)|Empire in its August 2009 issue ranked the cropduster scene as the best. 
{{cite journal
 | title = 1001 Greatest Movie Moments
 | journal = Empire
 | location = London, England
 | pages = 89–113
}} The cropduster scene was ranked number one with a full-page explanation and stills from the movie. 

===Set design===
The house at the end of the film was not real. Hitchcock asked the set designers to make the set resemble a house by Frank Lloyd Wright, the most popular architect in America at the time, using the materials, form and interiors associated with him.   The set was built in Culver City, where MGMs studios were located.  House exteriors were matte paintings.   

===Costuming===
 
A panel of fashion experts convened by GQ in 2006 said the gray suit worn by Cary Grant throughout almost the entire film was the best suit in film history, and the most influential on mens style, stating that it has since been copied for Tom Cruises character in Collateral (film)|Collateral and Ben Afflecks character in Paycheck (film)|Paycheck. {{cite web
| author = Reuters
| date = October 16, 2006
| title = Cary Grant’s gray suit tops movie clothing list. GQ rates the most chic men’s clothing on film
| url = http://www.msnbc.msn.com/id/15295247/
| work = MSNBC
}} Vanity Fair magazine, it was Norton & Sons of London,  although according to The Independent it was Quintino of Beverly Hills. 

Eva Marie Saints wardrobe for the film was originally entirely chosen by MGM. Hitchcock disliked MGMs selections and the actress and director went to Bergdorf Goodman in New York to select what she would wear. 

===Editing and post-production===
In François Truffauts book-length interview, Hitchcock/Truffaut (1967), Hitchcock said that MGM wanted North by Northwest cut by 15 minutes so the films length would run under two hours. Hitchcock had his agent check his contract, learned that he had absolute control over the final cut, and refused. 

One of Eva Marie Saints lines in the dining-car seduction scene was redubbed. She originally said "I never make love on an empty stomach", but it was changed in post-production to "I never discuss love on an empty stomach", as the censors considered the original version too risqué. 

== Release ==
The trailer for North by Northwest features Hitchcock presenting himself as the owner of Alfred Hitchcock Travel Agency and telling the viewer he has made a motion picture to advertise these wonderful vacation stops. 

The world premiere took place at the San Sebastián Film Festival. 

== Reception ==
Time (magazine)|Time magazine called the film "smoothly troweled and thoroughly entertaining."  A. H. Weiler of The New York Times made it a "Critics Pick" and said it was the "years most scenic, intriguing and merriest chase"; Weiler complimented the two leads:   Cary Grant, a veteran member of the Hitchcock acting varsity, was never more at home than in this role of the advertising-man-on-the-lam. He handles the grimaces, the surprised look, the quick smile, ... and all the derring-do with professional aplomb and grace, In casting Eva Marie Saint as his romantic vis-à-vis, Mr. Hitchcock has plumbed some talents not shown by the actress heretofore. Although she is seemingly a hard, designing type, she also emerges both the sweet heroine and a glamorous charmer.  

===Box office===
During its two-week run at Radio City Music Hall, the film grossed $404,056, setting a record in that theaters non-holiday gross. 

According to MGM records the film earned $5,740,000 in the US and Canada and $4.1 million elsewhere, resulting in a profit of $837,000. 
 Time Out magazine, reviewing the film nearly a half-century after its initial release, commented: Fifty years on, you could say that Hitchcocks sleek, wry, paranoid thriller caught the zeitgeist perfectly: Cold War shadiness, secret agents of power, urbane modernism, the ant-like bustle of city life, and a hint of dread behind the sharp suits of affluence. Cary Grants Roger Thornhill, the films sharply dressed ad exec who is sucked into a vortex of mistaken identity, certainly wouldnt be out of place in Mad Men. But theres nothing dated about this perfect storm of talent, from Hitchcock and Grant to writer Ernest Lehman (Sweet Smell of Success), co-stars James Mason and Eva Marie Saint, composer Bernard Herrmann and even designer Saul Bass, whose opening-credits sequence still manages to send a shiver down the spine.  

Author and journalist Nick Clooney praised Lehmans original story and sophisticated dialogue, calling the film "certainly Alfred Hitchcocks most stylish thriller, if not his best". 

North by Northwest currently holds a List of films with a 100% rating on Rotten Tomatoes|100% approval rating on the review aggregator Rotten Tomatoes, based on 61 reviews. The sites consensus calls the film "Gripping, suspenseful and visually iconic" and claims it "laid the groundwork for countless action thrillers to follow". 

The film ranks at number 98 in Empire (magazine)|Empire magazines list of the 500 Greatest Films of All Time.  The Writers Guild of America ranked the screenplay #21 on its list of 101 Greatest Screenplays ever written.  It is ranked the 40th greatest American film by the American Film Institute. 

== Themes and motifs   ==
Hitchcock planned the film as a change of pace after his dark romantic thriller Vertigo (film)|Vertigo a year earlier. In his book-length interview Hitchcock/Truffaut (1967) with François Truffaut, Hitchcock said that he wanted to do "something fun, light-hearted, and generally free of the symbolism permeating his other movies."  Writer Ernest Lehman has also mocked those who look for symbolism in the film.  Despite its popular appeal, the film is considered to be a masterpiece for its themes of deception, mistaken identity, and moral relativism in the Cold War era.

The title North by Northwest is a subject of debate. Many have seen it  as having been taken from a line ("I am but mad north-north-west: when the wind is southerly I know a hawk from a handsaw.") in Hamlet, a work also concerned with the shifty nature of reality.   Hitchcock noted, in an interview with Peter Bogdanovich in 1963, "Its a fantasy. The whole film is epitomized in the title—there is no such thing as north-by-northwest on the Points of the compass#Compass points|compass."  Lehman states that he used a working title for the film of "In a Northwesterly Direction," because the films action was to begin in New York and climax in Alaska.  Then the head of the story department at MGM suggested "North by Northwest," but this was still to be a working title.  Other titles were considered, including "The Man on Lincolns Nose," but "North by Northwest" was kept because, according to Lehman, "We never did find a   title."  The Northwest Airlines reference in the film plays on the title.

The films plot involves a "  containing government secrets out of the country. They have been trying to kill Thornhill, whom they believe to be the agent on their trail, George Kaplan.

 

North by Northwest has been referred to as "the first James Bond film" {{cite web
| title = Hitching a ride with the Master of Suspense
| url = http://www.guardian.co.uk/film/2009/jun/13/alfred-hitchcock-north-by-northwest
| work = The Guardian
| last = Patterson
| first = John
| date=2009-06-13
| accessdate = 2013-02-18
| deadurl = no
}}  due to its similarities with splashily colorful settings, secret agents, and an elegant, daring, wisecracking leading man opposite a sinister yet strangely charming villain.  The crop duster scene inspired the helicopter chase in From Russia with Love. 
 Freudian symbolism reflecting Hitchcocks mischievous sense of humor. In the book Hitchcock / Truffaut (p.&nbsp;107-108), Hitchcock called it a "phallic symbol... probably one of the most impudent shots I ever made."

== Home media ==
North by Northwest was released on the Blu-ray Disc format in the United States on November 3, 2009 by Warner Bros. with a 1080p VC-1 encoding.       This release is a special 50th anniversary edition. A 50th anniversary edition on DVD was also released by Warner Bros.   

== Awards == Best Film Best Production Best Original Pillow Talk. The film also won a 1960 Edgar Award for Best Motion Picture Screenplay, for Lehman.

In 1995, North by Northwest was selected for preservation in the National Film Registry by the United States Library of Congress as being "culturally, historically, or aesthetically significant."

In June 2008, the AFI revealed its "Ten top Ten" – the best ten films in ten "classic" American film genres – after polling over 1,500 people from the creative community. North by Northwest was acknowledged as the seventh-best film in the mystery genre.   

American Film Institute recognition 
* AFIs 100 Years...100 Movies – #40
* AFIs 100 Years...100 Thrills – #4
* AFIs 100 Years...100 Heroes & Villains: Phillip Vandamm – Nominated Villain
* AFIs 100 Years of Film Scores – Nominated
* AFIs 100 Years...100 Movies (10th Anniversary Edition) – #55
* AFIs 10 Top 10 – #7 Mystery Film

== Influences ==
The films title is reported to have been the influence for the name of the popular annual live music festival  , March 18, 2010 

The third episode of the Doctor Who serial "The Deadly Assassin" includes an homage to North by Northwest, when the Doctor, who like Hitchcocks hero is falsely accused of a politically motivated murder, is attacked by gunfire from a biplane piloted by one of his enemys henchmen. 

== References ==
 

== External links ==
 
 
*  
*  
*  
*  
*  
* {{cite web
| date = July 14, 1975
| author = Pauline Kael
| title = American Masters. Cary Grant
| url = http://www.pbs.org/wnet/americanmasters/database/grant_c.html
| archiveurl = http://web.archive.org/web/20050525055737/http://www.pbs.org/wnet/americanmasters/database/grant_c.html
| archivedate = 2005-05-25
| work = New Yorker magazine
| page = 3
}}
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 