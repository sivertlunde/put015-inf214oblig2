From the Life of the Marionettes
 
{{Infobox film
| name           = From the Life of the Marionettes
| image          = From_the_Life_of_the_Marionettes.jpg
| director       = Ingmar Bergman
| producer       = Konrad Wendlandt Horst Wendlandt Ingmar Bergman
| writer         = Ingmar Bergman
| starring       = Robert Atzorn Heinz Bennent Martin Benrath Toni Berger Christine Buchegger
| music          = Rolf A. Wilhelm
| cinematography = Sven Nykvist
| editing        = Petra von Oelffen
| distributor    =
| released       = 3 November 1980 (German TV) 7 November 1980 (German cinema)
| runtime        = 104 min
| country        = West Germany Sweden
| language       = German
}} directed by Ingmar Bergman. The film was produced in West Germany with a German language screenplay and soundtrack while Bergman was in "tax exile" from his native Sweden. It is filmed in black and white apart from two colour sequences at the beginning and end of the movie. It is set in Munich. The title is a quotation excerpted from a passage in The Adventures of Pinocchio by Carlo Collodi:

:"Most unfortunately in the lives of the Marionettes there is always a BUT that spoils everything".

Unlike Collodis story, however, Bergmans is unremittingly bleak in tone.

The film charts the disintegration of the relationship of Katarina and Peter Egermann, the feuding couple seen briefly in Bergmans earlier Scenes From a Marriage. As Katarina seeks other lovers, the emotionally repressed and despondent Peter descends into neuroses, eventually leading him to tearfully murder a prostitute (played by Rita Russek), with the same name as his wife, at a Munich peep show before sodomising her dead body. In the closing sequence he is incarcerated in a mental asylum. An odd counterpoint to the depressing tone of the film is the sprightly disco soundtrack over the end credits.

==Production==
The film was shot in the Bavaria Film Studios in Munich from October to December 1979. It begins in colour, but all the flashbacks are in black and white. The film was originally made for television and had its TV premiere on German ZDF on 3 November 1980. The very first showing, however, was at a small film festival in Oxford 13 July 1980, followed by festival showings in Paris  and the Netherlands  in October 1980. After the German TV premiere the film went on general cinema release, starting with Zoo Palast in Berlin on 7 November 1980.

==Cast==
* Robert Atzorn – Peter Egermann
* Heinz Bennent – Arthur Brenner
* Martin Benrath – Mogens Jensen
* Toni Berger – The Guard
* Christine Buchegger – Katarina Egerman
* Gaby Dohm – Secretary
* Erwin Faber		
* Lola Müthel – Cordelia Egermann
* Ruth Olafs – Nurse
* Karl-Heinz Pelser – The Interrogator
* Rita Russek – Ka
* Walter Schmidinger – Tim
* Michel Wagner - The Bartender

==Response==
The film opened to good and occasional cautionary reviews. Many felt Bergmans film was too heavy to be considered real drama, but they did like the performances of the German cast, including Robert Atzorn.

==References==
 

==External links==
* 
* 
* 
*  Retrieved 2011-07-11
*  Retrieved 2011-07-11

 

 
 
 
 
 
 