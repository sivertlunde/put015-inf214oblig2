Walk the Line
 
{{Infobox film
| name           = Walk the Line
| image          = Walk the line poster.jpg
| caption        = North American release poster, designed by Shepard Fairey
| director       = James Mangold
| producer       = James Keach Cathy Konrad
| writer         = Gill Dennis James Mangold
| based on       =  
| starring       = Joaquin Phoenix Reese Witherspoon Ginnifer Goodwin Robert Patrick Dallas Roberts
| music          = T Bone Burnett
| cinematography = Phedon Papamichael
| editing        = Michael McCusker
| distributor    = 20th Century Fox
| studio         = Fox 2000
| released       =  
| runtime        = 136 minutes (theatrical cut) 153 minutes (extended cut)
| country        = United States
| language       = English
| budget         = $28 million
| gross          = $186.4 million
}} biographical drama film directed by James Mangold and based on the early life and career of country music artist Johnny Cash. The film stars Joaquin Phoenix, Reese Witherspoon, Ginnifer Goodwin, and Robert Patrick.
 June Carter, and his ascent to the country music scene, as based on his autobiographies. The screenplay was written by Mangold and Gill Dennis. The films production budget is estimated to have been US$28 million. 
 Best Actor Best Actress Best Costume Design (Arianne Phillips). The film grossed more than $186 million worldwide.

==Plot== 
The film opens in 1968, as an audience of inmates at Folsom State Prison cheer for Johnny Cashs band as he waits backstage near a table saw, reminding him of his early life.

In 1944, Johnny, then known as J.R., grows up the son of a sharecropper on a cotton farm in Dyess, Arkansas. He is known for his singing of hymns, while his brother Jack is training himself to become a pastor. While Jack is sawing wood for a neighbor, J.R. goes fishing until his brother finishes. But, Jack has an accident with his saw and dies of his injuries. Cashs strained relationship with his father Ray, becomes much more difficult after Jacks death. In 1950, J.R. enlists in the United States Air Force as Johnny Cash, and is posted in West Germany. He buys a guitar to play and in 1952, finds solace in writing songs, one of which he develops as "Folsom Prison Blues".

After his discharge, Cash returns to the United States and marries his girlfriend Vivian Liberto. The couple move to Memphis, Tennessee, where Cash works as a door-to-door salesman to support his growing family. He walks past a recording studio, which inspires him to organize a band to play gospel music. Cashs band auditions for Sam Phillips, the owner of Sun Records. After they play, "Folsom Prison Blues," the band receives a contract.
 Tennessee Two. June Carter, Carl Smith. After his attempt to woo June backfires, Cash starts abusing drugs and alcohol. After his behavior reaches a bottom during a performance with June, they separate.
 Ring of Fire", describing her feelings for Cash and her pain at watching him descend into addiction.

Returning to California, Cash travels to Mexico to purchase more drugs and is arrested. Cashs marriage to Vivian crumbles; the pair divorce and Cash moves to Nashville in 1966. Trying to reconcile with June, he buys a large house near a lake in Hendersonville, Tennessee|Hendersonville. His parents and the extended Carter family arrive for Thanksgiving, at which time Ray dismisses his sons achievements and behavior. After the meal, Junes mother encourages her daughter to help Cash. He goes into detox and wakes with June; she says they have been given a second chance. Although not married, the two begin to spend most of their time with each other.
 album live inside Folsom Prison. Despite Columbias doubts, Cash says that he will perform, and his label can use the tapes if they wish. At the Folsom Prison concert, Cash says that he has been sympathetic to prisoners, explaining that his arrest for drug possession helped him to relate to them. With this success, Cash embarks on a tour with June and his band. On the bus, he stops to talk with June and proposes to her, but she turns him down. At the next concert, June says she can only talk with him on stage. Cash invites her to a duet and stops in the middle, saying he cannot sing "Jackson (song)|Jackson" any more unless June agrees to marry him. June accepts and they share a passionate embrace on stage.

==Cast==
 
* Joaquin Phoenix as Johnny Cash
* Reese Witherspoon as June Carter
* Ginnifer Goodwin as Vivian Liberto, Johns first wife
* Robert Patrick as Ray Cash, Johns father
* Hailey Anne Nelson as Rosanne Cash, Johns oldest daughter
* Dallas Roberts as Sam Phillips, the producer of Sun Records
* Dan John Miller as Luther Perkins, Johns first guitarist
* Johnny Holiday as Carl Perkins
* Larry Bagby as Marshall Grant, Johns bassist Carrie Cash, Johns mother
* Tyler Hilton as Elvis Presley
* Waylon Payne as Jerry Lee Lewis
* Shooter Jennings as Waylon Jennings, Johns future bandmate Fluke Holland, Johns drummer
* Johnathan Rice as Roy Orbison
* Lucas Till as Jack Cash, Johns brother
* Ridge Canipe as young Johnny Cash
 

==Development and pre-production== pitch to a studio. Sony and others turned it down, but Fox 2000 agreed to make the film. 

The film was in part based on two autobiographies, both of which were   (1975) and   (1997), though the film "burrows deep into painful territory that Mr. Cash barely explored." 

Phoenix met Cash months before hearing about the film. When he read the script, he felt there were at least 10 other actors who would be better in the role.  To prepare for her role as June Carter, Witherspoon watched several videos of the singer; she also listened to her singing and telling stories to get her voice right. 

==Release==

===Box office===
Walk the Line was released on November 18, 2005, in 2,961 theaters, grossing US dollar|$22.3 million on its opening weekend. It went on to earn $119.5 million in North America and $66.9 million in the rest of the world for a total of $186.4 million, well above its $28 million budget, making it a box office success.   

===Reception===
Critics generally responded with positive reviews, garnering an 82% on Rotten Tomatoes. Witherspoons performance received rave reviews, and Phoenixs performance inspired film critic Roger Ebert to write, "Knowing Johnny Cashs albums more or less by heart, I closed my eyes to focus on the soundtrack and decided that, yes, that was the voice of Johnny Cash I was listening to. The closing credits make it clear its Joaquin Phoenix doing the singing, and I was gob-smacked".    In her review for the Los Angeles Times, Carina Chocano wrote, "Joaquin Phoenix and Reese Witherspoon do first-rate work — they sing, they twang, they play new-to-them instruments, they crackle with wit and charisma, and they give off so much sexual heat its a wonder they dont burst into flames".   

  wrote, "A lot of credit for Phoenixs performance has to go to Mangold, who has always been good at finding the bleak melodrama in taciturn souls ... If Mangolds new movie has a problem, its that he and co-screenwriter Gill Dennis sometimes walk the lines of the inspirational biography too rigorously". 

Andrew Sarris, in his review for The New York Observer praised Witherspoon for her "spine-tingling feistiness", and wrote, "This feat has belatedly placed it (in my mind, at least) among a mere handful of more-than-Oscar-worthy performances this year".  Entertainment Weekly gave the film a "B+" rating and Owen Gleiberman wrote, "while Witherspoon, a fine singer herself, makes Carter immensely likable, a fountain of warmth and cheer, given how sweetly she meshes with Phoenix her romantic reticence isnt really filled in".  Baltimore Sun reviewer Michael Sragow wrote, "What Phoenix and Witherspoon accomplish in this movie is transcendent. They act with every bone and inch of flesh and facial plane, and each tone and waver of their voice. They do their own singing with a startling mastery of country musics narrative musicianship".  In his review for Sight and Sound, Mark Kermode wrote, "Standing ovations, too, for Witherspoon, who has perhaps the tougher task of lending depth and darkness to the role of June, whose frighteningly chipper stage act - a musical-comedy hybrid - constantly courts (but never marries) mockery". 

Some critics found the film too constrained by Hollywood plot formulas of love and loss, ignoring the last twenty years of Cashs life and other more socio-politically controversial reasons he was considered "the man in black". 
 Her brother was instrumental in having the filmmakers remove two scenes that were not flattering to her mother. Furthermore, she said, "The movie was painful. The three of them   were not recognizable to me as my parents in any way. But the scenes were recognizable, and the storyline, so the whole thing was fraught with sadness because they all had just died, and I had this resistance to seeing the screen version of my childhood". 

===Accolades===
 
{| class="infobox" style="width: 23em; font-size: 85%;"
|-  style="background:#ccc; text-align:center;"
! colspan="2" | Academy Awards
|- Best Actress in a Leading Role (Reese Witherspoon)
|-  style="background:#ccc; text-align:center;"
! colspan="2" | Golden Globe Awards
|- Best Motion Picture – Musical or Comedy
|- Best Actor in a Leading Role – Musical or Comedy (Joaquin Phoenix)
|- Best Actress in a Leading Role – Musical or Comedy (Reese Witherspoon)
|-  style="background:#ccc; text-align:center;"
! colspan="2" | BAFTA Awards
|-
| 1. Best Actress (Reese Witherspoon)
|-
| 2. Best Sound
|}
Witherspoons performance was repeatedly recognized, including an Academy Award for Best Actress    and other awards such as:
* BAFTA Award as Best Actress Broadcast Film Critic Award as Best Actress
* Golden Globe Award as Best Actress – Musical or Comedy
* National Society of Film Critics Award as Best Actress
* Online Film Critics Society award as Best Actress Satellite Award as Best Actress – Musical or Comedy Screen Actors Guild Award as Best Actress

Film critic Andrew Sarris ranked Walk the Line number seven in top films of 2005 and cited Reese Witherspoon as the best female performance of the year.  Witherspoon was also voted Favorite Leading Lady at the 2006 Peoples Choice Awards.  David Ansen of Newsweek ranked Witherspoon as one of the five best actresses of 2005. 

==Home media==
On February 28, 2006, a single-disc DVD and a two-disc collector edition DVD were released; these editions sold three million copies on their first day of release.  On March 25, 2008, a two-disc extended cut DVD was released for region one. The feature on disc one is 17 minutes longer than the theatrical release, and disc two features eight extended musical sequences with introductions and documentaries about the making of the film. The film has been released on Blu-ray Disc in France and Sweden in the form of its extended cut. The American Blu-ray features the shorter theatrical cut.

==Soundtrack==
  Best Compilation Soundtrack Album for a Motion Picture, Television or Other Visual Media.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 