Mike Bassett: England Manager
{{multiple issues|
 
 
}}

{{Infobox film
|  name         = Mike Bassett: England Manager
|  image        = Mike Bassett cover.jpg
|  caption      = John R. Smith Rob Sprackling
|  starring     = Ricky Tomlinson Amanda Redman Bradley Walsh
|  director     = Steve Barron Colin Green
|  producer     = Steve Barron Neil Peplow
|  studio       = Artists Independent Productions Film Council Hallmark Entertainment
|  distributor  = Entertainment Film Distributors
|  released     =  
|  runtime      = 89 minutes
|  country      = United Kingdom
|  language     = English
|  gross        =£3,443,174 
}}
 satirical comedy football (English English footballs Norwich City, Mike Bassett, who having led his side to the Mr Clutch Cup, is appointed England manager. It received mixed reviews. 

The film takes the form of a fly-on-the-wall fictional documentary (mockumentary) following Bassett (played by Ricky Tomlinson) as he starts his international management career. Martin Bashir, well known as a journalist and presenter in real life, plays the interviewer and provides the voice-over, and the film features cameo appearances from Pelé and Ronaldo Luís Nazário de Lima|Ronaldo. The film satirises many targets, such as the mysterious figures who run the Football Association, the stereotypical view of an old-fashioned manager, and the tabloid presss unfailing habit of building the England team up so they can knock them down hard.

The world cup being played is referred to in the film as World Cup XVII; the 17th World Cup was the tournament played in 2002.

The film was followed by a TV series,   in 2005.  

And in 2014 a second film Mike Bassett: Interim Manager is trying to get £250,000 funding on the website Kickstarter 

== Plot ==
 Sir Alex Division One, Norwich City.
 Philip Jackson) Colchester United Graham Taylor World Cup Slovenia in Luxembourg over Turkey sees Keith Allen.
 Scottish and Irish teams.  Egypt before flaming sambucas with anti-depressants, it seems things can get no worse.
 Argentina to Romania and 1990 England finished fourth). This implies that England lost the third-place playoff, which is not seen in the film. The team return to England to a cheering crowd and Bassett announces that he will not quit.

== Cast ==
*Ricky Tomlinson – Mike Bassett
*Amanda Redman – Karine Bassett
*Bradley Walsh – Dave Dodds Philip Jackson – Lonnie Urquart
*Phill Jupitus – Tommo Thompson
*Dean Lennox Kelly – Kevin Tonkinson
*Martin Bashir – Interviewer
*Robbie Gee – Rufus Smalls Geoff Bell - Gary Wackett
*Pelé – Himself
*Kevin Piper – Norwich newsreader
*Robert Putt – Jack Marshall
*Malcolm Terris – Phil Cope
*Philip Dunbar – Sussex rep
*Ulrich Thomsen – Dr. Hans Shoegaarten
*Lloyd McGuire – Midlands rep
 

== England under Mike Bassett ==

{| class="wikitable" style="font-size:90%; text-align:center"
|-
! Fixture !! colspan="3" | Result | Date
|- Poland (Home - WCQ8 - Group 3)
| style="background:pink; text-align:center; width:150px;"| Lost 1-2 | 12 April 2001
|- Belgium (Away - WCQ9 - Group 3)
| style="background:pink; text-align:center; width:150px;"| Lost 0-3 | 20 September 2001
|- Slovenia (Home - WCQ10 - Group 3)
| style="background:#ABCDEF; text-align:center; width: 150px;" | Drew 0-0 | 18 November 2001
|- Egypt (Neutral - World Cup R1 - Group 6)
| style="background:#ABCDEF; text-align:center; width: 150px;" | Drew 0-0 | 15 June 2002
|- Mexico (Neutral - World Cup R1 - Group 6)
| style="background:pink; text-align:center; width:150px;"| Lost 0-4 | 19 June 2002
|- Argentina (Neutral - World Cup R1 - Group 6)
| style="background:#BFFFC0; text-align:center; width: 150px;" | Won 1-0 |  7 July 2002
|- Romania (Neutral - World Cup - Last 16)
| style="background:#BFFFC0; text-align:center; width: 150px;" | Won 3-0 | 12 July 2002
|- France (Neutral - World Cup - Quarter Finals)
| style="background:#BFFFC0; text-align:center; width: 150px;" | Won 2-0 | 16 July 2002
|- Brazil (Away - World Cup - Semi Finals)
| style="background:pink; text-align:center; width:150px;"| Lost 0-1 | 21 July 2002
|}

== After the films events ==
  2004 European Newcastle United Norwich and Colchester lead to him taking over at his fathers former club, Wirral County.

== Reception ==

The movie received a mixed reception from critics.    

==References==
 

==External links==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 