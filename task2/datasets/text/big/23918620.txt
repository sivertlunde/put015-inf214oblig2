Captain Calamity (film)
{{Infobox Film
| name           = Captain Calamity
| image          = Capcalpos.jpg
| image_size     = 
| caption        = Original film poster John Reinhardt
| producer       = George A. Hirliman (producer) Charles J. Hunt (associate producer) Louis Rantz (associate producer) Gordon Ray Young (story) Crane Wilbur (screenplay)
| narrator       =  George Houston
| music          = 
| cinematography = Mack Stengler
| editing        = Tony Martinelli
| distributor    = Grand National Films Inc.
| released       = 17 April 1936 (premiere)
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} South Seas John Reinhardt George Houston Grand National Pictures. It was filmed in an early colour process called Hirlicolor at Talisman Studios and Santa Catalina Island, California.

==Plot==
A penniless ships captain is taking a passenger ashore after their voyage.  The passenger throws a gold coin in the ocean that he says has been bad luck to him as it was a gift from his ex-fiancee. The Captain sends one of his men, an experienced underwater diver, to successfully retrieve it.  The passenger explains that is a gold Spanish doubloon and the Captain is more than welcome to it.

Going ashore, the Captain decides to have some fun by telling stories indicating that he discovered four chests full of the coins, which were pirate treasure.  The news spreads throughout the island and leads to kidnapping, torture, murder and an attack on the Captains ship.

== Cast == George Houston as Captain Bill Jones / Captain Calamity
*Marian Nixon as Madge Lewis
*Vince Barnett as Burp
*Juan Torena as Mike
*Movita as Annana
*Crane Wilbur as Dr. James Kelkey
*George J. Lewis as Black Pierre (as George Lewis)
*Roy DArcy as Samson
*Margaret Irving as Mamie Gruen
*Barry Norton as Carr
*Louis Natheaux as E.D. Joblin - Store Owner
*Lloyd Ingraham as Trader Jim
*Alberto Gandero as Gandero - a Sailor (as Albert Gandero)
*Harold Howard as Guy Warren
*Charles Moyer as Mac McKenzie - a Sailor
 Gordon Jones, Maria Kalamo and John Van Pelt appear uncredited. The pig that appeared in the film was barbecued and served to the cast. 

== Soundtrack ==
*George Houston - "Riders of the Rolling Seas" (Written by Jack Stern and Harry Tobias)
*George Houston - "A Drunken Sailor" (Written by Jack Stern and Harry Tobias)
*George Houston - "Tell Me Why" (Written by Jack Stern and Harry Tobias)
*George Houston - "Drop Your Anchor" (Written by Jack Stern and Harry Tobias)

==Spanish Version==
A Spanish-language version, El capitan Tormenta was filmed alongside Captain Calamity. Roy DArcy Juan Torena, Barry Norton and Movita reprised their roles (though most of the character names were changed). Fortunio Bonanova replaced Huston in the title role and Lupita Tovar served as Nixons Spanish-language alternative, renamed Magda. Supposedly, producer Hirliman had wanted Tovar to play the female lead in both productions, but she only appeared in the Spanish version.

==Notes==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 