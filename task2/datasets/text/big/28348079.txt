Pars: Operation Cherry
{{Infobox film
| name           = Pars: Operation Cherry
| image          = ParsKirazOperasyonuFilmPoster.jpg
| caption        = Theatrical poster
| director       = Osman Sınav
| producer       = Osman Sınav
| writer         = Osman Sınav Aybars Bora Kahyaoğlu
| starring       = Mehmet Kurtuluş Nida Şafak Selçuk Yöntem Uğur Polat Pelin Batu Haluk Piyes Duygu Şen Udo Kier
| Music          = Srdjan Kurpjel  Steve Wellington
| cinematography = Torben Forsberg
| editing        = 
| studio         = Sinegraf
| distributor    = UIP Filmcilik
| released       =  
| runtime        = 121 mins
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = US$2,093,804
}}
 Turkish film of 2007.   

==Production==
Production began in October 2006 and the film was shot on location in Istanbul, Amsterdam and Monaco. An 89-strong crew along with 73 actors and around 1,000 extras took part in the production.    

==Synopis==
In a very dangerous police operation famous undercover Narcotic agent Ertugrul faces with a puzzling situation. Unfortunately he is killed along with his wife in front of their children before he solves the secret in the dark labyrinths of the world of drugs. Sixteen years later, his older son Attila, who has grown up to be a Narcotic officer with the nickname "Panther", will seek his revenge. In a narcotic operation, Attila along with his partner Asena, who is a female narcotic officer, they seize a huge amount of drugs that belongs to infamous drug kingpin Hashasi who is disguised himself as a respectable industrialist Vahdet Bozcan. Vahdet who has strong ties with politicians, pressures the authorities to post Attila to another job. Attila being demoted and striped from all his privileges desperately tries to find his way out. On top of everything, Atilla loses his only brother Tayfun to a drug-related crime and gets into depression. With the support of Asena and his colleagues Atilla puts himself together and unofficially starts pursuing Hashasi known as Vahdet Bozcan. In this long run, Attila pursues Vahdet from Turkey to Holland from France to Greece and from Monaco to mysterious streets of Istanbul and the drug infested schools of Istanbul. This time Attila is determined to find the killers of both his father and his brother while fighting against the drug network that starts from the top officials of the country and ends up in schools. With the help of Inci, a guidance teacher in the school of his late brother Tayfun, and Asena, Attila finds his way out but only to be lost in a love triangle between these two women. But he has a mission to complete. Attilla is ready to sacrifice everything even his life to the fight against drugs.

==References==
 

==External links==
*   for the film
*  

 
 
 
 
 
 
 
 