La Caravana del manuscrito andalusí
{{Infobox film
| name           = La Caravana del manuscrito andalusí
| image          = 
| caption        = 
| director       = Lidia Peralta García
| producer       = CEDECOM S.L.
| writer         = 
| starring       = 
| distributor    = 
| released       = 2007
| runtime        = 49 minutes
| country        = Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = Lidia Peralta García
| cinematography = Lidia Peralta García
| editing        = Lidia Peralta García, David Moya
| music          = Kristian Hernández, Yengo-Le
}}

La Caravana del manuscrito andalusí is a 2007 documentary film directed by Lidia Peralta García. 

== Synopsis == Muslim Spain, Toledo to Timbuktu, this documentary follows their traces. Its protagonist, Ismael Diadié Haidara, owner of the Andalus Library of Timbuktu, has spent years trying to retrieve his familys manuscripts and with them, his own Al Andalus past.      

==Production==

The film was produced with the assistance of CEDECOM Malaga.
The director, Lidia Peralta García, traveled for five months and covered over   on the route from Toledo in Spain to Timbuktu in Mali to make the documentary.
According to Peralta, "a documentary like this makes you very aware that for many centuries we have shared the same history, while today some have white skin and others black". {{cite web
 |url=http://www.webislam.com/articulos/32979-la_caravana_del_manuscrito_andalusi_la_memoria_historica_de_alandalus_yace_en_bi.html
 |title=La Caravana del Manuscrito Andalusí. La memoria histórica de al-Andalus yace en bibliotecas del desierto africano
 |date=2008-02-15
 |author=Tania Ouariachi
 |work=Web Islam
 |accessdate=2012-03-11}} 

==Awards==

La Caravana del manuscrito andalusí won the Best Documentary Film prize at the 10th International Panorama of Independent Films. {{cite web
 |url=http://www.theatre-entropia.gr/index.php?option=com_content&view=article&catid=40%3Afes&id=54%3A6th-alternative-stage-festival&Itemid=63&lang=en
 |title=6th Alternative Stage Festival
 |date=December 9–14, 2008
 |work=Theatre Entropia
 |accessdate=2012-03-11}} 
It was also nominated for Best Documentary Film at the 2008 Marbella International Film Festival. {{cite web
 |url=http://www.marbellafilmfestival.com/archives/awards-2008/
 |publisher=Marbella International Film Festival
 |title=Awards 2008
 |accessdate=2012-03-11}} 

== References ==
 
 

 
 
 
 
 
 
 
 
 


 
 
 