Go Go 70s
{{Infobox film
| name           = Go Go 70s 
| image          = File:Go Go 70s poster.jpg
| film name = {{Film name
 | hangul         =  70 
 | rr             = Gogo 70
 | mr             = Kogo 70}}
| director       = Choi Ho 
| producer       = Shim Bo-kyeong   Lee Jong-ho   Park Jae-hyun
| writer         = Choi Ho   Bang Jun-seok   Baek Bae-jeong
| starring       = Jo Seung-woo  Shin Mina 
| music          = Bang Jun-seok 
| cinematography = Kim Byeong-seo    
| editing        = Kim Sang-beom   Kim Jae-beom
| distributor    = Showbox/Mediaplex  
| released       =  
| runtime        = 118 minutes
| country        = South Korea
| language       = Korean
| admissions     = 581,468
| budget         =  
}}
Go Go 70s ( ) is a 2008 South Korean drama film|drama/musical film set in the 1970s.

==Plot==
South Korea in the 1970s was in the Dark Ages of Park Chung-hees military dictatorship, but it was also an era of revolutionary upheaval with regards to culture. After wandering the shabby clubs of a U.S. military base, vocalist Sang-kyu and guitarist Man-sik form the indie rock band The Devils with four other members. After entering a rock band contest and making a strong impression with their shocking yet entertaining performance, The Devils achieve stardom and begin playing at a club called Nirvana. Mimi, a groupie who follows the Devils from town to town, also becomes an icon with her dance moves and fashion sense. However, their heyday doesnt last long as one of the band members gets killed in a fire at the club. To make matters worse, many clubs are being forced to shut down due to military oppression, which would fundamentally take away the opportunity for bands to perform. Despite their despair and looming disbandment, Sang-kyu plans one last concert for The Devils. 

==Cast==
* Jo Seung-woo ... Sang-kyu, The Devils vocalist and guitarist
* Shin Mina ... Mimi, leader of Wild Girls
* Cha Seung-woo ... Man-sik, The Devils guitarist
* Song Kyung-ho ... Dong-geun, The Devils drummer
* Choi Min-cheol ... Dong-soo, The Devils trumpet player
* Kim Min-kyu ... Kyung-goo, The Devils bassist
* Hong Kwang-ho ... Joon-yeob, The Devils saxophonist Lee Sung-min ... Lee Byeong-wook
* Im Yeong-sik ... Byeong-tae
* Kim Soo-jeong ... Young-ja
* Yoon Chae-yeon ... Ki-bok
* Min Bok-gi ... boss of club Nirvana
* Yoo Chang-sook ... Dong-soos mother
* Lee Sang-yong ... Nam-dae criminal investigator 
* Hong Seok-bin ... Criminal investigator of The Devils 1 
* Jo Deok-jae ... Criminal investigator of The Devils 2 
* Kwon Jeong-min ... Jailhouse police 
* Jin Yong-gook ... Folk song singer Teacher Kim 
* Kim Jae-rok ... Recording engineer 
* Kwon Hyeok-poong ... Music salon owner
* Hwang Yeon-hee ... President of American club
* Yoon Jeong-yeon ... Mimis backup dancer
* Jang Mi-yeon ... Mimis backup dancer
* Oh Ji-eun ... Fighting prostitute
* Lee Jong-yoon ... Gambling band 
* Geum Gi-jong ... Gambling band 
* Park Soo-jo ... Jailhouse detective 
* Kim Jong-eon ... Bat group 
* Lee Ha-neul ... Bat group 
* Kim Hyeong-jin ... Bat group 
* Lee Si-eun ... Criminal investigator of Lee Byeong-wook 
* Im Hyeong-tae ... Young-jas father 
* Kim Moon-yeong ... Lee Byeong-wooks fan 
* Lee Malg-eum ... Lee Byeong-wooks fan 
* Hong Sang-jin ... Scottman 
* Choi Pyeong-woong ... Daehan news announcer

==Production==
The screenplay was co-written by director Choi Ho, composer Bang Jun-seok and Baek Bae-jeong, based on Bangs experiences as a child listening to The Beatles. Lead actor Jo Seung-woo took guitar lessons to prepare for his role.

==Box office==
The film sold 595,156 tickets nationwide and earned $3,433,279.  

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 
 
 


 
 