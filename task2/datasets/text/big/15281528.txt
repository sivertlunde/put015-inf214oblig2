Man of the Moment (1935 film)
{{Infobox film
| name           = Man of the Moment
| image          = 
| image_size     = 
| caption        = 
| director       = Monty Banks
| producer       = Irving Asher
| writer         = Yves Mirande (play) Guy Bolton   Roland Pertwee   A.R. Rawlinson
| narrator       = 
| starring       = Douglas Fairbanks, Jr. Laura La Plante   Margaret Lockwood   Claude Hulbert
| music          = 
| cinematography = Basil Emmott   Leslie Rowson
| editing        = Bert Bates
| studio         = Warner Brothers
| distributor    = Warner Brothers
| released       = September 1935
| runtime        = 82 minutes
| country        = United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Man of the Moment is a 1935 British romantic comedy film directed by Monty Banks and starring Douglas Fairbanks, Jr., Laura La Plante and Margaret Lockwood. It was made at Teddington Studios by the British subsidiary of Warner Brothers.  The films art direction was by Peter Proud.

==Plot==
Office worker Mary Briany (Laura La Plante) finds out she is being demoted by the boss she secretly loves in order to make room for his girlfriend. She tries to commit suicide by jumping into the river. Tony Woodward (Douglas Fairbanks, Jr.) is driving by and rescues her, much to her annoyance.

He takes her back to his mansion, but he and his butler Godfrey (Donald Calthrop) have great difficulty getting her to behave. Meanwhile, Tony is to be married the next day to childish heiress Vera Barton (Margaret Lockwood). She reveals to Tonys friend Lord Rufus Paul (Claude Hulbert) that she plans to change Tonys lifestyle completely - no more smoking or drinking, among other things. Her millionaire father (Peter Gawthorne) promises his nearly penniless future son-in-law 5000 pounds to pay for a partnership in a company.
 drag in his younger brothers clothes. The next day, Vera and her father find Tony, Mary and his friends passed out on the floor. As a result, Vera breaks off the wedding.

With only £300 and deeply in debt, Tony proposes a suicide pact to Mary. They will fly to Monte Carlo to try to win a fortune at the casino. If they lose, they will kill themselves. The first day does not go well. They are ready to jump off a cliff when a gentleman finds them and gives them £20  they didnt know they had won. On their second chance, they split up to gamble. Tony loses, but Mary has an incredible lucky streak and wins a large amount of money.

Meanwhile, Vera decides she wants to marry Tony after all. Rufus brings news about Tonys whereabouts and they go to Monte Carlo. Vera embraces Tony before Mary can tell him the good news.
Heartbroken, Mary climbs out on the hotel ledge, but Tony finds her and tells her he loves her. (Annoyed at being jilted, Vera decides that she wants to marry a man that no other woman would desire; she picks Rufus.)

==Cast==
* Douglas Fairbanks, Jr. as Tony Woodward
* Laura La Plante as Mary Briany
* Claude Hulbert as Lord Rufus Paul
* Margaret Lockwood as Vera Barton
* Peter Gawthorne as Mr. Barton
* Donald Calthrop as Godfrey
* Morland Graham as Mr. Rumcorn
* Eve Gray as Miss Madden
* Margaret Yarde as Landlady  
* Wyndham Goldie as Jason Randall
* Patrick Ludlow as Roulette Player 
* Martita Hunt as Roulette Player 
* Tony Hankey as Party Guest  
* Monty Banks as Doctor 
* Hal Gordon as Police Sergeant  Charles Hawtrey as Tom 
* Victor Rietti as Hotel Manager 
* John Singer as Small Boy

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 