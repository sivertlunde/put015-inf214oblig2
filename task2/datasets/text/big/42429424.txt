The Last Days of Pompeii (1908 film)
{{Infobox film
| name           = The Last Days of Pompeii 
| image          =
| caption        =
| director       = Arturo Ambrosio   Luigi Maggi 
| producer       = Arturo Ambrosio 
| writer         = Edward Bulwer-Lytton (novel)   Roberto Omegna
| starring       = Luigi Maggi   Lydia De Roberti   Umberto Mozzato
| music          = 
| cinematography = Roberto Omegna   Giovanni Vitrotti
| editing        = 
| studio         = Ambrosio Film 
| distributor    = Ambrosio Film
| released       = December 1908
| runtime        = 
| country        = Italy
| awards         =
| language       = Silent   Italian intertitles
| budget         =
}} silent historical novel of the same title by Edward Bulwer-Lytton. The film was a success on its release, and its popularity is credited with starting a fashion for epic historical films. 

The film was made by the Turin-based Ambrosio Film.

==Cast==
* Luigi Maggi as Arbace  
* Lydia De Roberti as Nidia  
* Umberto Mozzato as Glauco 
* Ernesto Vaser as Il padrone di Nidia 
* Mirra Principi
* Cesare Gani Carini

== References ==
 

== Bibliography ==
* Brunetta, Gian Piero. The History of Italian Cinema: A Guide to Italian Film from Its Origins to the Twenty-first Century. Princeton University Press, 2009. 
* Moliterno, Gino. The A to Z of Italian Cinema. Scarecrow Press, 2009.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 