The Spanish Main
 
{{Infobox film
| name           = The Spanish Main
| image          = Binnie Barnes in The Spanish Main trailer.jpg
| image_size     = 180px
| caption        = Binnie Barnes in the film
| director       = Frank Borzage
| producer       = Frank Borzage (uncredited) Robert Fellows (executive producer) Stephen Ames (associate producer)
| writer         = Aeneas MacKenzie (story) George Worthing Yates Herman J. Mankiewicz
| starring       = Maureen OHara Paul Henreid Walter Slezak Binnie Barnes
| music         = Hanns Eisler
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 100 min. gross = 2,819,971 admissions (France)  English
}} Becky Sharp ten years before.
 Academy Award Best Color Cinematography. Though a box office hit upon its first release, the film is chiefly remembered today for its lavish and intricate score by 
Hanns Eisler.

==Plot==
 s Du Billar and Capt. Black.

The film includes the character Anne Bonny (Barnes), in a fictionalized account of the real-life well-known female pirate.

==Principal cast==
* Maureen OHara as Francisca Alvarado
* Paul Henreid as Capt. Laurent Van Horn
* Walter Slezak as Don Juan Alvarado
* Binnie Barnes as Anne Bonny John Emery as Du Billar
* Barton MacLane as Capt. Black

==Release==
The film was successful and made a profit of $1,485,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p. 206 

==See also==
*Women in piracy

==Notes==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 