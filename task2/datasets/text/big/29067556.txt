Bye Bye Blackbird (film)
{{Infobox film
| name           = Bye Bye Blackbird
| image          = File:Poster bye bye blackbird (french release).jpg
| alt            =  
| caption        = 
| director       = Robinson Savary
| producer       = Christine Alderson Adam Betteridge
| writer         = Arif Ali Shah
| screenplay     = Robinson Savary Patrick Faure
| starring       = James Thiérrée Izabella Miko Derek Jacobi Jodhi May
| music          = Mercury Rev
| cinematography = Christophe Beaucarne
| editing        = Emmanuelle Castro Claire Ferguson
| studio         = 
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Luxemburg
| language       = English
| budget         = 
| gross          = 
}}

Bye Bye Blackbird is a 2005 Drama film directed by Robinson Savary and starring James Thiérrée, Derek Jacobi and Izabella Miko.

== Plot ==
Josef (James Thiérrée) is a former construction worker who now works as a sweeper at the circus, and falls for the aerialist, Alice (Izabella Miko) and is befriended by the horseback performer, Nina (Jodhi May). One day, he defies death and gravity by doing an aerial display on the trapeze. When he is spotted by the big top’s owner, Lord Dempsey (Derek Jacobi), he is paired with Alice in a dangerous aerial display as part of a new act for the circus.
However, things turn tragic as an accident happens and Alice is declared dead, with the circus turned topsy turvy with the loss of their only profitable act and Josef going mad with grief, destroying the “White Angels” act.

==Cast==
* James Thiérrée ...  Josef
* Derek Jacobi ...  Lord Dempsey
* Izabella Miko ...  Alice
* Jodhi May ...  Nina
* Michael Lonsdale ...  Robert
* Andrej Aćin ...  Roberto
* Chris Bearne ...  Lord Strathclyde
* Niklas Ek ...  Djamako Claire Johnston ...  Emma
* Carlos Pavlidis ...  Jenkins
* Claudine Peters ...  Miss Julia

==Soundtrack==
The soundtrack for the film, named Hello Blackbird, by the American alternative rock band Mercury Rev, was released in 2006. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 