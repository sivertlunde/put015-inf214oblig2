Chicken Feed
 
 
{{Infobox film
| name           = Chicken Feed
| image          = 
| caption        = 
| director       = Robert A. McGowan Charles Oelze
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Hal Roach Studios
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent comedy film directed by Robert A. McGowan.       It was the 66th Our Gang short subject released.

==Cast==

===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Allen Hoskins as Farina
* Jannie Hoskins as Mango
* Scooter Lowry as Skooter
* Jay R. Smith as Jay Bobby Young as Bonedust

===Additional cast===
* Jean Darling as Jean
* Johnny Downs as Prof. Presto Misterio (magician)
* Bobby Hutchins as Toddler chasing the monkey
* Harry Spear as Audience member
* Ham Kinsey as Animal trainer
* Jimsy Boudwin as Audience member
* Bobby Mallon as Audience member
* Davey Monahan as Audience member
* Andy Shuford as Audience member
* Pal the Dog as Himself

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 