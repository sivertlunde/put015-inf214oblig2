Aayushkalam
{{Infobox film
| name           = Aayushkalam 
| image          = Aayushkalam.gif
| image_size     = 
| alt            = 
| caption        =  Kamal
| producer       = 
| writer         = Vinu Kiriyath Rajan Kiriyath
| narrator       =  Mukesh  Sreenivasan  Saikumar  Maathu Kaviyoor Ponnamma                                     | music          = Ouseppachan
| cinematography = Saloo George
| editing        =  K. Rajagopal  
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Aayushkalam (Malayalam:ആയുഷ്കാലം)  Sreenivasan and Gavin Packard. This movie is an uncredited remake of the 1990 Hollywood film  Heart Condition  

==Synopsis==
Balakrishnan (Mukesh (actor)|Mukesh) is a heart patient. He urgently needs a heart transplant and luckily he gets the heart of Aby Mathew (Jayaram) who was dead a few moments ago. After the operation, Balakrishnan discovers that he can see Aby Mathew while nobody else can. Aby Mathew is now a ghost and he wants to bring his killers in front of the law. He takes Balakrishnans help and start investigating. At first, nobody is ready to believe Balakrishnan as no one believes in ghosts. Balakrishnan also takes the help of Sub Inspector Damu (Sreenivasan (actor)|Sreenivasan) who also at first does not believe him. Balakrishnan discovers that Aby Mathew was murdered by his adopted brother and confidant Alex Chandanavelil (Saikumar (Malayalam actor)|Saikumar) to take over his business and that the lives of Abys wife (Maathu)and mother (Kaviyoor Ponnamma) are also in danger. The rest of the plot is Balakrishnans attempt to save them.

==Cast== Mukesh ...  Balakrishnan
* Jayaram ...  Aby Mathew Sreenivasan ...  Damu
* Maathu(actress)|Maathu..  Sobha Saikumar ...  Alex
* Gavin Packard ...  Benjamin Bruno
* K.P.A.C. Lalitha ...  Dakshayani Siddique ...  Hariprasad
* Oduvil Unnikrishnan ...  Menon Innocent ...  Gopala Menon
* Zeenath ...  Geetha
* Alummoodan ...  Velu Mooppan Rudra ...  Sujatha
* Idavela Babu ...  Gopi Abu Salim
* Mammukoya ...  Varghese
* Sankaradi ...  Fernandez
* Kaviyoor Ponnamma...aby mathews mother

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 