Just Before I Go
{{Infobox film
| name           = Just Before I Go
| image          = Just Before I Go poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Courteney Cox
| producer       = {{plainlist|
* Gabriel Cowan
* Courteney Cox
* Thea Mann
}}
| writer         = David Flebotte
| starring       = {{plainlist|
* Seann William Scott
* Olivia Thirlby
* Garret Dillahunt Kate Walsh
}}
| music          = Erran Baron Cohen
| cinematography = Mark Schwartzbard
| editing        = Roger Bondelli
| studio         = {{plainlist|
* New Artists Alliance
* Coquette Productions
}}
| distributor    = Anchor Bay Films
| released       =  
| runtime        = 90 minutes 
| country        = United States
| language       = English
| budget         =
| gross          = $8,129 (US) 
}} Kate Walsh.

The film premiered at the Tribeca Film Festival on April 24, 2014,    and it was released in select theaters on April 24, 2015.   

== Plot ==
Ted (Seann William Scott) decides to commit suicide; but before he goes, he returns to his hometown to settle some scores. Things dont go as planned.

== Cast ==
* Seann William Scott as Ted Morgan
* Olivia Thirlby as Greta
* Garret Dillahunt as Lucky Morgan Kate Walsh as Kathleen Morgan
* Kyle Gallner as Zeke
* Rob Riggle as Rawly Stansfield
* Evan Ross as Romeo
* Cleo King as Berta
* Missi Pyle as Officer CT
* Elisha Cuthbert as Penny Morgan
* Mackenzie Marsh as Vickie
* Connie Stevens as Teds Mom
* David Arquette as Vickies Husband

== Production ==
As of July 10, 2013, filming was under way in Los Angeles. 

== Release ==
The premiere took place at the 2014 Tribeca Film Festival.  The film was  released in select theaters on April 24, 2015 before a video on demand, digital store, DVD and Blu-Ray release on May 12, 2015. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 14% of seven surveyed critics gave the film a positive review; the average rating was 2.5/10.    Metacritic rated it 30/100 based on five reviews.   Justin Chang called it "a dismal, tonally disastrous small-town farce".     Frank Scheck of The Hollywood Reporter described it as "a serious misfire" whose tonal shifts would be difficult for a veteran director to manage.   Ethan Alter of Film Journal International wrote, "Cox must have seen something in this screenplay that encouraged her to film it, but whatever that critical element was, it’s not apparent in the finished product."   Stephen Holden of The New York Times wrote that the film "lurches along a wobbly line between salacious comic nastiness and nauseating sentimentality" without properly integrating them into a cohesive whole.   Gary Goldstein of the Los Angeles Times wrote, "Anchored by a nicely understated performance by Seann William Scott, Just Before I Go effectively juggles a wealth of genuine, at times profound, emotion with quite a bit of nutty-raunchy humor." 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 