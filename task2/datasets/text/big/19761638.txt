Don McKay (film)
 
{{Infobox film
| name           = Don McKay
| image          = Don McKay poster.jpg
| caption        = 
| director       = Jake Goldberger
| producer       = Jim Young
| writer         = Jake Goldberger
| starring       = Thomas Haden Church Elisabeth Shue Melissa Leo James Rebhorn Keith David M. Emmet Walsh
| music          = Steven Bramson
| cinematography = Phil Parmet
| editing        = Andrew Dickler
| studio         = Animus Films
| distributor    = Image Entertainment
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
}} independent drama 8th Annual Tribeca Film Festival in April 2009  and received a limited release on April 2, 2010.

==Plot==
Don McKay (Thomas Haden Church) is a lonely high school janitor who one day receives a letter from his high school sweetheart, Sonny (Elisabeth Shue). In it, she asks him to come visit her back in their home town, because she is dying of an unnamed disease. At first he is reluctant because he had been a suspect in a murder case there years before, but Don decides to go. He arrives and gets a ride to Sonnys house by an eccentric cab driver named Samuel (M. Emmet Walsh).

Don meets Sonny, as well as her strange caregiver Marie (Melissa Leo). Maries coldness towards Don makes it clear that she doesnt approve of his presence. Don spends the night, and he and Sonny make love. The next morning, Sonnys Doctor, Lance Pryce (James Rebhorn) visits. While Marie and Sonny are out, Pryce attacks Don, and after a struggle, Don kills the man, and hides the body in a bed of leaves behind the garden. However, Don had just suffered an allergic reaction to a bee sting, and blacks out shortly after hiding the body. He awakens in the hospital, where Sonny proposes marriage and claims that she had recently spoken to Pryce. 

Don goes to see his old friend Otis (Keith David), and tells him what happened. Otis agrees to help Don get rid of the body that night, but when he arrives it has vanished. Sonny rushes out, thinking the men are burglars, and Otis runs off. After an argument, Don returns home for a few days, eventually getting another letter from Sonny asking him to come back to her. He does.

Samuel picks Don up again, but they are intercepted and kidnapped by a man named Mel (Pruitt Taylor Vince). Mel takes Don to a hotel room, where its revealed that both Mel and Marie were planning all along to kill Don and take an inheritance Don would receive upon Dons marriage. Don tries to reason with the two stating that there is no inheritance, and that he has no idea what they are talking about.  Marie refuses to believe him and tells him to go along with the plan, or they will go to the police about the death of Pryce.  With Samuel in Mels trunk, the four drive back to Sonnys house, and demand that Don go in wearing a wire, threatening to go to the police if he doesnt cooperate with their plan.

Upon arriving at the house, Don hastily proposes to Sonny, stating that he was confused before, but realizes now that he loves her. Sonny quickly peels away Dons story, realizes he is wearing a recording device, and calls his bluff.  The doorbell rings; Its Marie and Mel, who enter the house. After a long confrontation, Sonny hits Marie over the head with a frozen ham, and kills her. Sonny then grabs the phone and calls 911 for help, stating that a man pretending to be a private detective just killed someone in her house.  

She tells Don to kill Mel, and Don refuses. She grabs an ax and again asks him to kill Mel, but he refuses.  Sonny then asks Mel to kill Don, and a struggle over the ax ensues between the men.  Sonny kills Mel with the same frozen ham she killed Marie with, and tries to concoct a story to cover all of it. She tells Don that shed been planning all along to take a large inheritance from Pryce, not Don, that he had received from a relative. Its also revealed that Pryce was her husband, and that the two were going to kill him together, but Pryce jumped the gun on the scam. Don then tells her that he knows that she wasnt actually Sonny, as he had killed his girlfriend in an accident before leaving town years ago, but gotten away with it.

The police arrive and Sonny lets them into the house.  They find the bodies of Marie and Mel, and immediately arrest Don.  Sonny is taken away in a police car, and the police start to take Don away.  The sound of screaming starts to erupt from Mels trunk.  The police find Samuel and question Don about Samuels involvement.  Don asks Samuel to explain his knowledge of the situation to the police. In the ensuing investigation, Samuel backs up Dons story, and it seems that he will be okay. The detective in charge asks him why he would go through all of it if he knew that it wasnt Sonny, and Don answers that he was "lonely".  

Don receives a ride to the bus station from Samuel, who tells him to contact him if he ever needs anything.  Don takes Samuels phone number and boards the bus back home.  At home, Don is seen sitting in a chair, a wasp lands on his cheek and the movie ends, leaving Dons fate unknown.

==Cast==
* Thomas Haden Church as Don McKay   
* Elisabeth Shue as Sonny 
* James Rebhorn as Dr. Lance Pryce 
* Melissa Leo as Marie 
* M. Emmet Walsh as Samuel 
* Keith David as Otis Kent 
* Pruitt Taylor Vince as Mel
* Robert Wahlberg as Alfred

==Production==
Filming took place north of Boston in Massachusetts. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 