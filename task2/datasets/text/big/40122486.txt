Take One False Step
{{Infobox film
| name           = Take One False Step
| image          = Take one false step 1949 poster small.jpg
| image_size     = 200px
| alt            =
| caption        = Theatrical release poster
| director       = Chester Erskine
| producer       = Chester Erskine
| screenplay     = Chester Erskine Irwin Shaw
| story          =
| based on       =   
| narrator       =  Marsha Hunt Dorothy Hart
| music          = Walter Scharf
| cinematography = Franz Planer
| editing        = Russell F. Schoengarth Universal International Pictures
| distributor    = Universal Pictures
| released       =   
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Marsha Hunt and Dorothy Hart. 

==Plot==
A married college professor agrees to have a drink with an old girlfriend; the next day hes being hunted for her murder. 

==Cast==
* William Powell as Professor Andrew Gentling
* Shelley Winters as Catherine Sykes Marsha Hunt as Martha Wier
* Dorothy Hart as Helen Gentling
* James Gleason as Captain Gledhill
* Felix Bressart as Professor Morris Avrum Art Baker as Dr. Henry Pritchard
* Sheldon Leonard as Detective Pacciano
* Howard Freeman as Dr. Markheim
* Houseley Stevenson as Dr. Montgomery Thatcher Paul Harvey as A.K. Arnspiger
* Francis Pierlot as Prof. Herbert Watson
* Jess Barker as Arnold Sykes
* Mikel Conrad as Freddie Blair

==Reception==
===Critical response=== Thin Man films is flashed by him on a few occasions in the Rivolis new Take One False Step, a curiously mixed-up mystery picture which Chester Erskine produced, directed and helped to write. But for the most part our erstwhile detective and comedian is forced to play a role of rather painful proportions with forbidding austerity in this film ... a little more of Miss Winters—as an active participant, that is—might have rendered a rather drab picture more decorative, at least." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 