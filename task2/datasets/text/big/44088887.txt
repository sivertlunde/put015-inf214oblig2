Iniyethra Sandhyakal
{{Infobox film 
| name           = Iniyethra Sandhyakal
| image          =
| caption        =
| director       = K Sukumaran Nair
| producer       =
| writer         = Parasala Divakaran
| screenplay     = Parasala Divakaran Madhu Jayabharathi Jose
| music          = G. Devarajan
| cinematography = Anandakkuttan
| editing        = G Venkittaraman
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Jose in lead roles. The film had musical score by G. Devarajan.   

==Cast== Madhu
*Jayabharathi
*Thikkurissi Sukumaran Nair Jose
*Shubha Shubha
*Sathaar

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Hamsagaanamaalapikkum || P. Madhuri || Mankombu Gopalakrishnan || 
|-
| 2 || Paalaruvee naduvil || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 3 || Samkrama Snaanam Kazhinju || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 4 || Sreevidyaam (Slokam) || P. Madhuri || Mankombu Gopalakrishnan || 
|-
| 5 || Thaalam thakathaalam || P Jayachandran, Vani Jairam, CO Anto, Karthikeyan || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 