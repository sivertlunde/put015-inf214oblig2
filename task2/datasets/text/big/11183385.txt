Doberman Deka
{{Infobox animanga/Header
| image           =  
| caption         = Cover artwork of the first volume from the Jump Comics edition
| ja_kanji        = ドーベルマン刑事
| ja_romaji       = Dōberuman Deka
| genre           = Detective fiction
}}
{{Infobox animanga/Print
| type            = manga
| author          = Buronson
| illustrator     = Shinji Hiramatsu
| publisher       = Shueisha
| demographic     = Shōnen manga|Shōnen
| magazine        = Weekly Shōnen Jump
| first           = 1975
| last            = 1979
| volumes         = 29
| volume_list     = 
}}
 

  is a hardboiled manga series written by Buronson and drawn by Shinji Hiramatsu. It was serialized in the Weekly Shōnen Jump manga anthology from 1975 (Issue 39) throughout 1979 (Issue 48) and was originally collected in a 29-volume tankōbon edition. A later aizōban edition was published, as well as a bunkoban edition, which compressed the number of volumes to 18. The manga was also adapted into two live-action feature films (one of them starring Sonny Chiba and directed by Kinji Fukasaku) and a TV series.

==Manga==
The protagonist of the story is  , a detective employed by the Tokyo Metropolitan Police Departments special crimes division, which handles serious criminal cases. His gun of choice is a customized .44 Magnum-caliber Ruger Blackhawk. Even though Kano and his co-workers are criticized by the media and society, he doesnt mind the criticism at all. Even though Kano has no pity for serious criminals, he holds a gentle respect for children and elderly people, as well as former criminals who want to redeem themselves from their past behavior.

At the beginning of the manga, the only members of the Special Crimes Division were Kano himself and Superintendent  , but they would gradually be joined by additional members such as Detective  , a gang specialist who was transferred to Shinjuku from Osaka; female detective  ; and from America, female detective  . While the series had a hardboiled atmosphere, it gradually became more light-hearted during the titles four-year run.
 

==First film==
{{Infobox film
| name           = Doberman Deka
| caption        = Toei Company
| image	         = The Doberman Cop FilmPoster.jpeg
| director       = Kinji Fukasaku
| producer       = Norimichi Matsudaira Kyō Namura
| writer         = Kōji Takada
| starring       = Sonny Chiba
| music          = Kenjiro Hirose
| cinematography =
| editing        = Isamu Ichida
| distributor    = Toei Company (Japan)
| released       = July 2, 1977 (Japan)
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} Toei in Japan on July 2, 1977. The film was directed by Kinji Fukasaku and starred Sonny Chiba as  .

=== Story ===
The burned remains of a young woman discovered in Shinjuku, Tokyo. The police comes to the conclusion that it is the work of a pyromaniacal serial murderer. The murder victim is identified as Mayumi Tamaki, a native of Ishigaki, Okinawa. As the investigation unfolded, her former boyfriend, a former rider of a motorcycle named Chōei Mikawa emerged as a suspect. But Kano did not agree Mikawa is the suspect. Kano started conducting the investigation himself to search the real criminal person.
 

=== Cast ===
*Sonny Chiba as  
*Janet Hatta as Miki Harukaze (singer)
*Eiko Matsuda as Kosode Murasaki
*Takuzō Kawatani as Hideyoshi Kinoshita
*Hideo Murota as Jirō Takamatsu (officer of Shinjuku)
*Koichi Iwaki as Chōei Mikawa 
*Nenji Kobayashi as Katsuo Kōyama (Mikawas friend)
*Seizō Fukumoto as one of Hidemoris follower
*Hiroki Matsukata as Kaiji Hidemori (Yakuza and Mikis manager)
 

==TV series==
A TV series based on the manga, titled  , aired on TV Asahi affiliates in 1980. 22 episodes were produced, which aired from April 7 throughout October 27. Other than the main character, Joji Kano (portrayed by Toshio Kurasawa), very few elements from the manga were adapted to the series. In the TV series, Kano was a member of a motorcycle cop unit.

==Second film==
A V-Cinema|straight-to-video film was released by Gaga Communications in 1996 starring Riki Takeuchi in the title role.

==External links==
* 
*  at    
*  at    

 
 

 
 
 
 
 
 
 
 
 
 
 
 