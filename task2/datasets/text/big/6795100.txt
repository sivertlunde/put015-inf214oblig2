Uyirodu Uyiraga
{{Infobox film
| name = Uyirodu Uyiraga
| image =
| director = Sushma Ahuja
| writer =
| starring = Ajith Kumar Richa Ahuja Sarath Babu Srividya
| Producer = 
| banner =
| editing = A. Sreekar Prasad
| cinematography = K. Aravind Vidyasagar
| distributor =
| released = November 21, 1998
| runtime = Tamil
}}
Uyirodu Uyiraga ( ) is a 1998 Tamil film directed by Sushma Ahuja starring Ajith Kumar and Richa Ahuja in the main roles. The film which told the tale of a couples struggle to deal with terminal illness, had a successful music soundtrack composed by Vidyasagar (music director)|Vidyasagar. The film was released in November 1998 to positive reviews. 

==Plot==
The film details on how the parents of a young teen aged boy diagnosed with chronic brain tumor accept and deal with his sickness, Anjali (Richa Ahuja)s undeterred love for the optimistic Ajay who is on the verge of death is beautifully depicted throughout the film.

==Cast==
*Ajith Kumar as Ajay
*Richa Ahuja as Anjali
*Sarath Babu as Chandrasekhar
*Srividya Ambika
*Mohan

==Production== Vidyasagar won positive reviews. 

==Release==
The film released to positive reviews from film critics. A reviewer praised the film as "a clean movie with no masala stuff" but criticized the "weak story-line", while also praising the performances in the film.  Another critic drew particular praise to the role of Srividya, claiming the film was "a pleasant experience, the crowds cheered Srividya almost as much as Ajith".  The film good commercially,  

==Soundtrack==
The songs composed by Vidyasagar (music director)|Vidyasagar, won positive acclaim from reviewers. 
{|class="wikitable" width="60%"
! Song Title !! Singers
|- Nandini Srikar
|-
| "Nothing Nothing" || Harini
|-
| "Nadhi Engae" || Ghansham Vasvani
|- KK
|-
| "Anbae Anbae" || Hariharan (singer)|Hariharan, K. S. Chitra
|-
|}

==References==
 

 
 
 
 