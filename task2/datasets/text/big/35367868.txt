Ora (film)
  3D dance film filmed Thermographic camera|thermographically, capturing images of the dancers using only their body heat. The 15-minute short film was directed by Philippe Baylaucq as part of his two-year residency at the National Film Board of Canada (NFB) in Montreal. Ora was choreographed by José Navas, who had previously worked with Baylaucq on the NFB experimental dance film, Lodela.      
 Narcissus and Prometheus.   

==Production==
Baylaucq had experimented with standard-definition thermal imaging cameras, but wasnt satisfied with the results. Commercially available thermography produces brightly coloured images, while Baylaucq wanted high-definition cameras that could shoot in black and white, so images could be colourized afterwards. The director learned of an engineer based in California, Arn Adams, who had developed an HD thermographic system for the company Focalplane, owned by Lockheed Martin. 

This technology is used by the United States military for night vision and military operations, as well as for scientific and medical applications, but was never used to film dance until Ora. While Baylaucq had "certain misgivings" about using military technology for his film, he secured permission from Lockheed Martin and the U.S. military as well as Canadian dance, production and directors unions to shoot the film in the United States, as this technology was not allowed to leave the country.  

Production took place in an unheated warehouse in Vermont, close to the Canadian border, with floor-to-ceiling sets made of heat-reflecting aluminum panels, which were constructed and shipped by truck from Montreal. Cameras had to be cooled by liquid nitrogen so they wouldn’t generare a heat signature themselves. They were also computer-dependent and could only record 2.5 minutes of footage before data needed to be downloaded, which also meant that dancers, wearing little in the cold building, had to stop every two minutes.  The six dancers also had to perform without lighting or music.   

Ora is also the first NFB film recorded in 7.1 surround sound. The film was shot in 3D as well as 2D, for wider distribution in areas of the world where 3D cinemas are not as popular. The NFB executive producer of Ora is René Chénier.    

==Release==
The film premiered at the 2011 Toronto International Film Festival in the Short Cuts Canada section before being screened at the Festival du nouveau cinema with Wim Wenders’ 3-D dance film Pina (film)|Pina. In December 2011, it was shown with Pina in Quebec cinemas.   

In March 2012, Ora received the international short documentary audience award at the Thessaloniki Documentary Festival.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 