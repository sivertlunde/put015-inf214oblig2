The War of the Roses (film)
{{Infobox film
| name = The War of the Roses
| image = Waroftherosesposter.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Danny DeVito
| producer = James L. Brooks Arnon Milchan
| screenplay = Michael J. Leeson
| based on =  
| starring = Michael Douglas Kathleen Turner Danny DeVito G.D. Spradlin David Newman
| cinematography = Stephen H. Burum
| editing = Lynzee Klingman
| studio = Gracie Films
| distributor = 20th Century Fox
| released =  
| runtime = 116 minutes  
| country = United States
| language = English
| budget = $50 million
| gross = $160,188,546 
}} black comedy film based upon the 1981 novel The War of the Roses (novel)|The War of the Roses by Warren Adler. The film follows a wealthy couple with a seemingly perfect marriage. When their marriage begins to fall apart, material possessions become the center of an outrageous and bitter divorce battle.

This is the third film to co-star Michael Douglas, Kathleen Turner, and Danny DeVito, after Romancing the Stone and its sequel, The Jewel of the Nile. DeVito directed the film, which also had producer James L. Brooks and actor Dan Castellaneta working on a project outside of The Simpsons. The opening title sequence was created by Saul Bass.
 the battles between the Houses of York and Lancaster at the end of the Middle Ages.

==Plot==
Lawyer Gavin DAmato is in his office discussing a divorce case with a client. Noticing the mans determination to divorce his wife, Gavin decides to tell him the story of one of his clients, a personal friend of his.

Oliver Rose, a student at Harvard Law School, meets Barbara at an auction, where they bid on the same antique. Oliver chats Barbara up and they become friends. When Barbara misses her ferry home, the two end up spending the night together. Eventually the two marry and have two children, Josh and Carolyn. Over the years, the Roses grow richer, and Barbara finds an old mansion whose owner has recently died, purchases it and devotes her time to making a home there. However, cracks seem to be forming in the family. As Oliver becomes a successful partner in his law firm, Barbara, who was a doting and loving wife early in the marriage, appears to grow restless in her life with Oliver, and begins to dislike him immensely.
 heart attack, (actually a hiatal hernia) the day after an argument, Barbara (after her initial shock and concern) realizes she felt a sense of relief that he might be dead. She tells him so, adding that she no longer loves him and wants a divorce. Oliver accepts, but tension arises between the two when it becomes clear that Barbara wants the house and everything in it, and when her lawyer uses Olivers final love note to her (which he had written in the hospital) as leverage against him in their legal battle. Barbara initially throws Oliver out of the house, but he moves back in after discovering a loophole in the divorce that allows him to stay. As a result, Barbara immediately begins plotting to remove Oliver herself, even trying to seduce Olivers lawyer Gavin into siding with her instead.
 Staffordshire ornaments, and plates. Another fight results in a battle where Barbara nearly kills Oliver by using her monster truck to run over Olivers prized sports car, a classic Morgan 4/4. In addition, Oliver accidentally runs over Barbaras cat in the driveway with his car. When Barbara finds out, she retaliates by trapping him inside his in-house sauna, where he nearly succumbs to heatstroke and dehydration.

While the kids are away at college, Oliver eventually calms down and attempts to make peace with Barbara over an elegant dinner, but reaches his breaking point when Barbara serves him a pâté which she implies was made from his dog (the dog is later seen to actually be alive and well outside). Oliver attacks Barbara, who flees into the attic. Oliver boards up the house to prevent Barbara from escaping, while Barbara loosens the chandelier to drop on Oliver in yet another attempt to kill him. When their live-in housekeeper Susan returns home in the middle of the climactic battle, she senses something is terribly wrong and discreetly contacts Gavin for help. By the time Gavin arrives, Oliver and Barbaras quarrel has culminated in the two hanging dangerously from the unsecured chandelier. During this time, Oliver admits to Barbara that despite their hardships, he always loved her, but Barbara does not respond. Before Gavin can come inside with a ladder, the combined weight of Barbara, Oliver and the chandelier is too much for the electrical wires, which snap and sends them crashing violently to the floor. In his final breaths, Oliver reaches out to touch Barbaras shoulder, but Barbara uses her last ounce of strength to knock his hand away, firmly asserting her feelings for him even in death.

Finishing his story, Gavin presents his client with two options: either proceed with the divorce and risk facing a horrific bloodbath in court, or go home to his wife to settle their differences properly. The client chooses the latter, and Gavin, satisfied, calls his wife to tell her he is on his way home and that he loves her.

==Cast==
* Michael Douglas as Oliver Rose
* Kathleen Turner as Barbara Rose
* Danny DeVito as Gavin DAmato
* Marianne Sägebrecht as Susan
* Dan Castellaneta as Gavins client
* Sean Astin as 17-year-old Josh Rose
* Trenton Teigen as 10-year-old Josh Rose
* Heather Fairfield as 17-year-old Carolyn Rose
* G.D. Spradlin as Harry Thurmont
* Peter Donat as Jason Larrabee David Wohl as Dr. Gordon

==Home media== theatrical trailers, and 6 TV advertisements

==Reception==
Upon its release, the film was a success with critics and a box office hit, bringing in $83.7 million domestically in U.S. box office receipts, and $160,188,546 worldwide.   

The film maintains a positive 82% rating on Rotten Tomatoes. 

==Awards and nominations==
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- BAFTA Award BAFTA Award Best Adapted Screenplay Michael J. Leeson
| 
|- 40th Berlin Berlin International Film Festival Golden Bear    Best Director Danny DeVito
| 
|- Golden Globe Award Golden Globe Best Motion Picture – Musical or Comedy James L. Brooks
| 
|- Arnon Milchan
| 
|- Golden Globe Best Actor – Motion Picture Musical or Comedy Michael Douglas
| 
|- Golden Globe Best Actress – Motion Picture Musical or Comedy Kathleen Turner
| 
|-
|}

==Cultural impact== custody of the children)  with most speakers completely unaware of the words origins.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 