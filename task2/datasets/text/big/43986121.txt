My Dictator
{{Infobox film
| name           = My Dictator
| image          = 
| director       = Lee Hae-jun
| producer       = Kim Moo-ryoung
| writer         = Lee Hae-jun   Baek Cheol-hyeon
| starring       = Sol Kyung-gu Park Hae-il
| cinematography = Kim Byeong-seo
| music          = Lee Jin-hee   Kim Hong-jib
| editing        = Nam Na-yeong
| distributor    = Lotte Entertainment
| studio         = banzakbanzak Film Production
| country        = South Korea
| language       = Korean
| runtime        = 127 minutes
| budget         =   
| released       =  
| gross          = 
}}
My Dictator ( ) is a 2014 South Korean film directed and co-written by Lee Hae-jun, starring Sol Kyung-gu and Park Hae-il.   

==Plot== Korean Central president Park Chung-hees meeting with the reclusive and potentially dangerous North Korean leader Kim Il-sung. To prepare Park for this conference, they decide to stage an imaginary summit so that they can simulate every scenario and plan for all eventualities. So the KCIA looks for an unknown (and bad) actor to play the body double for Kim Il-sung.
 The Fool in Shakespeares King Lear, but he forgets his lines due to stage fright. Sung-geun not only ruins the show, but he feels shamed in front of his son Tae-sik, who was in the audience and thought his father was the greatest actor in the world.

Backstage, the devastated Sung-geun is approached by Professor Heo, who praises his performance and tells him to show up at a "special audition." At the audition, Sung-geun is confused when the casting directors all seem to be government scientists, and even when his performance goes terribly, he gets told that its exactly what they were looking for. He and 11 other actors pass the first round, then theyre put on a bus and taken to the basement of the KCIA building where they are stripped, dragged into separate rooms and tortured. Too dumb to lie to his interrogators, Sung-geun is the only one left and gets chosen for the job. For Sung-geun, this is the role of a lifetime, and he puts his heart and soul into the performance, determined to have his acting acknowledged and to impress his son.

But when the summit gets cancelled, Sung-geun cannot let go of his Kim Il-sung persona and continues to act like the North Korean leader. Frustrated at his father, the adult Tae-sik places Sung-geun in a nursing home and refuses to visit him for the next 20 years. But when Tae-sik gets in trouble with loan sharks in the 1990s, he is forced to reconnect with his estranged father, whose dilapidated home could solve his problem.

==Cast==
* Sol Kyung-gu as Kim Sung-geun
* Park Hae-il as Tae-sik
* Yoon Je-moon as KCIA section chief Oh
* Lee Byung-joon as Professor Heo
* Ryu Hye-young as Yeo-jung
* Lee Kyu-hyung as Cheol-ju
* Bae Sung-woo as President Baek

==Production==
According to director Lee Hae-jun, "A few years ago, I came across an old photograph of my father and stared at it for a long time. In the picture, my father looked so young and strong. Is this really my father? He looked like a completely different person from the strict, somber, dictatorial dad Ive known my entire life. I was suddenly curious to know what made my father the man he is now. I realized that, for one thing, he lived through a period filled with dictators, both in North and South Korea. The son never gets to see his father as a young man, but a father is only ever an old man to his children. Its a lonely fate, and he must blame and resent others for it. This film is a belated confession to my weak, frail, dictator of a father, and to fathers like him all over the world." 

My Dictator was among the 32 films chosen by the Hong Kong Asian Film Financing Forum (Hong_Kong_International_Film_Festival#About_the_organiser|HAF) in 2012.   Financed by Lotte Entertainment with a budget of  , filming began at a small theater in Namwon, North Jeolla Province on March 26, 2014.   The three-month shoot finished on July 8, 2014 in Changsin-dong, Seoul.

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan=3| 2014
| 22nd Korea Culture and Entertainment Awards
| Grand Prize (Daesang) in Film
| Sol Kyung-gu
|  
|-
| rowspan=2| 35th Blue Dragon Film Awards
| Best New Actress
| Ryu Hye-young
|  
|-
| Technical Award
| Song Jong-hee (special make-up)
|  
|-
| rowspan=2| 2015
| 10th Max Movie Awards
| Best New Actress
| Ryu Hye-young
|  
|- 51st Baeksang Arts Awards
| Best Actor
| Sol Kyung-gu
|  
|-
|}

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 