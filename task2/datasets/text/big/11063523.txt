Even Money (film)
 
{{Infobox film
| name           = Even Money
| image          = Even money.jpg
| caption        = Theatrical Release Poster
| director       = Mark Rydell
| producer       = David S. Greathouse
| writer         = Robert Tannen
| starring       = Kim Basinger Danny DeVito Kelsey Grammer Nick Cannon Ray Liotta Forest Whitaker Carla Gugino  Jay Mohr
| music          = Dave Grusin
| cinematography = Robbie Greenberg
| editing        = Hughes Winborne
| distributor    = Yari Film Group Freestyle Releasing 
| released       =  
| country        = United States
| language       = English
| gross          = $111,974
}}
Even Money is a 2006 American crime film.

The story concerns three strangers who are addicted to gambling and how their lives come to be intertwined. They are a novelist who struggles to write her follow-up book, a former stage magician and an older brother of a college basketball star. The film was directed by Mark Rydell, and stars Forest Whitaker, Nick Cannon, Kim Basinger, Danny DeVito, Kelsey Grammer, Tim Roth, Carla Gugino, Jay Mohr and Ray Liotta. It was released on May 18, 2007 in theaters.

==Plot==
 
Carolyn Carver is a published author whose husband and daughter believe she is working on a new book, when in fact she is gambling away their life savings. She is befriended by Walter, a has-been magician with a gambling habit who entertains casino guests for tips. Clyde Snow, meanwhile, is deeply in debt to gamblers, to the extent that he must ask his younger brother Godfrey, a college basketball star, to shave points at games.

The violent and verbally abusive Victor is a criminal who may or may not be the front for Ivan, a mob boss no one actually has seen. A crippled detective named Brunner is investigating a murder that Victors thugs no doubt committed, possibly on Ivans orders.

Victor also must deal with Augie, who wants to cut in on his bookmaking business. Augie is secretly wearing a wire for the detective, looking to get an incriminating statement from Victor on tape.

Carolyns husband is upset with her constant absences and ultimately catches her gambling. He wants a divorce. Walter gets a tip from Victor that a basketball game is fixed. He decides to wager everything he owns on the outcome, and persuades Carolyn to do the same.

Unbeknownst to them, Clyde has told his brother that his debt is paid off, meaning there is no need to lose the game on purpose. It is not true. Clydes life has been threatened by Victor if this game goes wrong.

Victor arranges for Augie to be killed. He attends the basketball game in person. Clydes brother hits a game-winning shot, which makes Clyde proud but places his life in imminent danger. He leaves the arena and is gunned down by Victors men on the road.

The games outcome devastates Carolyn, who has lost everything, including her family. Walter goes to Victors house to kill him. The detective Brunner witnesses this but, sympathetically, lets Walter go free. The magicians fate is nonetheless sealed.

In a final twist, it turns out that Ivan is very real, and that Detective Brunner is an accomplice.

==Cast==
*Kim Basinger as Carolyn Carver
*Danny DeVito as Walter
*Forest Whitaker as Clyde Snow
*Ray Liotta as Carolyns Husband
*Kelsey Grammer as Brunner
*Tim Roth as Victor
*Jay Mohr as Augie
*Carla Gugino as Veronica
*Nick Cannon as Godfrey Snow Charles Robinson as Coach Washington

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 