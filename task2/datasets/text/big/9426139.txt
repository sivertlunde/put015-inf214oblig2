Nice Guys Finish First
 
  
Nice Guys Finish First (BBC Horizon (BBC TV series)|Horizon television series) is a 1986 documentary by Richard Dawkins which discusses selfishness and cooperation, arguing that evolution often favors co-operative behaviour, and focusing especially on the tit for tat strategy of the prisoners dilemma game theory|game.  The film is approximately 45 minutes long and was produced by Jeremy Taylor.

The twelfth chapter in Dawkins book The Selfish Gene (added in the second edition, 1989) is also named Nice Guys Finish First and explores similar material.

== Overview ==

In the opening scene, Richard Dawkins responds very precisely to what he views as a misrepresentation of his first book The Selfish Gene. In particular, the response of the right wing for using it as justification for social darwinism and laissez-faire economics (free-market capitalism). Richard Dawkins has examined this issue throughout his whole career and focused much of the recent documentary, The Genius of Charles Darwin on this very issue.

The concept of reciprocal altruism is a central theme of this documentary. Dawkins also examines the tragedy of the commons, and the dilemma that it presents. He uses the large area of common land Port Meadow in Oxford, England which has been hurt by overgrazing as an example of the tragedy of the commons. Fourteen academics as well as experts in game theory submitted their own computer programs to compete in a tournament to see who would win in the prisoners dilemma. The winner was tit for tat, a program based on "equal retaliation", and Dawkins illustrates the four conditions of tit for tat.
 
# Unless provoked, the agent will always cooperate.
# If provoked, the agent will retaliate.
# The agent is quick to forgive.
# The agent must have a good chance of competing against the opponent more than once.

In a second trial, this time of over sixty applicants, tit for tat won again.

==See also==
* Altruism in animals
* Ethology
* Evolutionarily stable strategy
* Evolutionary game theory
* Natural selection
* Prisoners Dilemma
* Sociobiology
* The Selfish Gene
* Tit for Tat
* Tragedy of the commons
*  

== External links ==
*  

 

 
 
 
 

 