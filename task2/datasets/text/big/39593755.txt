Four Sisters and a Wedding
{{Infobox film
| image            = Four Sisters and a Wedding.jpg
| caption          = Theatrical movie poster
| name             = Four Sisters and A Wedding
| director         = Cathy Garcia-Molina
| producer         = Charo Santos-Concio Malou N. Santos
| music            = Raul Mitra
| cinematography   = Noel Teehankee	
| story            = 
| screenplay       = Vanessa Valdez
| writer           = Vanessa Valdez
| editing          = Marya Ignacio
| starring         =Toni Gonzaga Bea Alonzo Angel Locsin Shaina Magdayao Enchong Dee
| studio           = Star Cinema TFC Films (International) 
| released         =  July 5, 2013 (International) 
| country          = Philippines
| language         =  
| runtime          = 120 mins
| budget           = PHP 20 Million (estimated)
| gross            = P345,029,261  
}}

Four Sisters and A Wedding is a 2013  , Bea Alonzo, Angel Locsin and Shaina Magdayao, as the four sisters attempting to stop the wedding of their younger brother played by Enchong Dee. 

The film is part of Star Cinemas 20th anniversary presentation in collaboration with Republic Biscuit Corporation|Rebiscos 50th anniversary. Four Sisters and a Wedding was released in the Philippines on June 26, 2013, and internationally on July 5, 2013  

==Plot==
When CJ (Enchong Dee), the youngest of the family, announced that he is getting married, his sister Gabbie (Shaina Magdayao), convinced the other sisters to come back home for the wedding as requested by their mother Grace (Coney Reyes). Teddie (Toni Gonzaga), the eldest, is working as a waitress and housekeeper in Madrid; the second sister Bobbie (Bea Alonzo), works as a corporate communications manager in New York and is living with her boyfriend Tristan (Sam Milby) and his daughter Trixie (Samantha Faytaren); the third sister Alex (Angel Locsin) is living independently and works as a film director; while Gabbie is a school teacher.

Bobbie, having a difficult relationship with Trixie, is pressured by the eagerness of Tristan to get married but she informs him that they should pave way for CJ first, as he is getting married in two weeks time. Alex, on the other hand, is having a hard time with her film making gig as well as her relationship with Bobbies ex - Chad (Bernard Palanca). Meanwhile, Teddie has not earned enough to buy a plane ticket so she asked help from her housekeeping colleague, Frodo (Janus del Prado) and convinced him that they go back to Manila together for the wedding and pretend to her family that they are a couple.

The family got reunited together soon enough and during the evening dinner, the sisters expressed their opinion on CJs abrupt decision of getting married. Offended by the remarks, CJ left the table in dismay. The sisters went on to ask for forgiveness and said that they are just concerned about the sudden decision. CJ then requested her sisters to behave themselves by tomorrow as they will have a dinner with the family of his fiance, Princess (Angeline Quinto).

During the gathering at the Bayags abode, the sisters were alarmed that CJ is forced to sign a contract, which assures that he would be able to secure Princess a good life. It was also explained that the wedding should take place as soon as possible because Princess’ grandfather wants to see her grandchild have children. Upon returning home, the sisters gathered together to formulate a plan to stop the wedding from happening. The following day, Teddie, who misunderstood Bobbies suggestion, asked their tomboy housekeeper, Toti Marie (Cecil Paz), to introduce CJ to a lot of girls. Toti Marie then came up with a boys’ night out with hookers that ended up in them getting drunk; and the plan to change CJs mind about the wedding failed. 

While the boys were getting sober, Princess arrived at the Salazars to surprise the family for breakfast. Alex invited Chad over, which made the whole moment awkward, especially for Bobbie. The following day, as Tristan was about to leave overseas for business, he tells Bobbie that by the time he gets back, she should now be willing to marry him. Alex, who overheard the incident, gave a comment with which Bobbie dismissed. Later on, Alex went to see Chad and asked him to marry her. Chad disagreed with the idea.

Meanwhile, Teddie and Frodo spent their day scrutinizing any filthy secrets that has to do with the Bayags. They went on to one of the Bayags’ family businesses - a spa which has an obscene tag line and offers a “happy ending” to their customers. Thinking that they’d be able to find anomalies, Teddie advised Frodo to avail one of the services, and that he should call her when the “happy ending” is about to happen. As Frodo was moaning in one of the massage areas, Teddie immediately called the police to have the area inspected. They found out that the “happy ending” was in fact, thinking of happy thoughts after a massage.

Jeanette / Salarzara (Carmi Martin) arrived on the scene and mentioned to Teddie that she is unhappy of what she has done and that she’ll also have the Salazars investigated on whatever dirty secrets that they are keeping. On the way home, Teddie, troubled that her secrets might surface, had an argument with Frodo because he was suggesting that she should just admit it that she is not really working as a teacher in Spain.

CJ confronted his sisters because of the spa incident and assured them that the wedding will take place no matter what. The sisters also squabbled with one another on a personal level but they were interrupted by the arrival of their mother. Later on that evening, Bobbie saw Chad flirting with another woman while buying condoms at a convenience store.

The following morning, Bobbie initiated to talk to Alex about what she saw but they ended up arguing with each other because of it. On the other hand, Teddie went to see Frodo and asked for forgiveness for what happened the other day. When she returned home, she was welcomed by the Bayags who came over to show the gowns that they are going to wear for the wedding. The Salazars did not like the gown designs so Honey Boy (Boboy Garovillo) decided that they play charades - wherein the family who wins gets to decide what gown designs to wear on the wedding day.

During the game, the Bayags kept on pointing hints that Teddie is a maid. Noticing this, Grace immediately disrupted them and had the Bayags leave for insulting her daughter inside their own home. Jeanette mentioned before leaving that Teddie is a “maid in Spain”. Grace confronted Teddie about it, who admitted that when Spain was in crisis, she was one of the teachers who was laid off. The confrontation also opened the opportunity to discuss their family’s problems and the envy that the sisters feel with one another. In the end, they all hugged each other and found peace.

The following day, Alex and Bobbie spoke to one another sincerely and they’ve decided that Alex should confront Chad and end things. They went to see him and caught him with another woman. Alex ended her relationship with him and also warned Chad’s other woman. Later that day, the sisters surprised Grace with piled crackers as a gift for her when suddenly, CJ called in to inform that Princess’ grandfather passed away. The family went to express their condolences to the Bayags and they apologized to one another for all the troubles that they have brought in their respective families.

Due to a family member’s death, CJ and Princess’ wedding did not push through. In order for the preparations and expenses not to go to waste, Bobbie used it as a chance to fulfill Tristan’s dream of marrying her.

==Cast==

===Main Cast===
*Toni Gonzaga as Theodora Grace "Teddie" Salazar
*Bea Alonzo as Roberta Olivia "Bobbie" Salazar
*Angel Locsin  as Alexandra Camille "Alex" Salazar
*Shaina Magdayao as Gabriella Angela "Gabbie" Salazar
*Enchong Dee as CJ "Reb Reb" Salazar  

===Supporting Cast===
*Coney Reyes as Grace Salazar
*Sam Milby as Tristan Harris
*Angeline Quinto as Princess Antoinette Mae Bayag
*Carmi Martin as Jeanette Bayag / Salarzara
*Boboy Garovillo as Honey Boy Bayag
*Janus del Prado as Frodo
*Bernard Palanca as Chad
*Vangie Labalan as Manang
*Joy Viado as Sassa
*Cecil Paz as Toti Marie
*Samantha Faytaren as Trixie Harris

==Accolades==
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Category
! Recipients and nominees
! Outcome
|- FAMAS Awards   
| Best Picture
| Charo Santos-Concio, Malou N. Santos
|  
|-
| Best Director
| Cathy Garcia-Molina
|  
|- Best Actress
| Bea Alonzo
|  
|-
| Toni Gonzaga
|  
|-
| Angel Locsin
|  
|-
| Best Supporting Actress
| Coney Reyes
|  
|-
| Best Screenplay
| Vanessa Valdez
|  
|-
| Best Film Editing
| Marya Ignacio
|  
|-
| Best Story
| Vanessa Valdez
|  
|-
| Best Sound
| Raul Mitra
|  
|-
| Best Musical Score
| Raul Mitra
|  
|- Gawad PASADO Awards     Best Supporting Actress
| Toni Gonzaga
|  
|-
| Bea Alonzo
|  
|-
| Angel Locsin
|  
|-
| Shaina Magdayao
|  
|- Star Awards for Movies    
| Movie of the Year
| Charo Santos-Concio, Malou N. Santos
|  
|- Movie Actress of the Year
| Bea Alonzo
|  
|-
| Angel Locsin
|  
|-
| Movie Director of the Year
| Cathy Garcia-Molina
|  
|-
| Movie Screenwriter of the Year
| Vanessa R. Valdez
|  
|-
| Movie Supporting Actress of the Year
| Conie Reyes
|  
|-
| Movie Editor of the Year
| Marya Ignacio
|  
|-
| Movie Cinematographer of the Year
| Manuel Teehankee
|  
|-
| Movie Musical Scorer of the Year
| Raul Mitra
|  
|-
| Movie Sound Engineer of the Year
| Aurel Claro Bilbao
|  
|-
| Movie Production Designer of the Year
| Winston Acuyong
|  
|-
|}

==References==
 

==External links==
 

 
 
 
 
 
 