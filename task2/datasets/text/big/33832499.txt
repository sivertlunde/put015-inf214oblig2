The Hotel Mouse
{{Infobox film
| name           = The Hotel Mouse
| image          = 
| image_size     = 
| caption        = 
| director       = Fred Paul
| producer       = G.B. Samuelson
| writer         = Paul Armont (play)   Marcel Gerbidon (play) Walter Summers
| starring       = Lillian Hall-Davis Campbell Gullan Warwick Ward   Josephine Earle
| music          = 
| cinematography = 
| editing        = 
| studio         = British-Super Films 
| distributor    = Jury Films
| released       = July 1923
| runtime        = 
| country        = United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 silent crime film directed by Fred Paul and starring Lillian Hall-Davis, Campbell Gullan and Warwick Ward.  It was based on a play by Paul Armont and Marcel Gerbidon.

==Cast==
* Lillian Hall-Davis - Mauricette 
* Campbell Gullan - Merchant 
* Warwick Ward - Estaban 
* Josephine Earle - Lola 
* Morgan Wallace - Honorable Harry Hurlingham

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 