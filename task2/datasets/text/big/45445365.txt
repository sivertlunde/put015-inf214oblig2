Goreyan Nu Daffa Karo
{{Infobox film
| name           = Goreyan Nu Daffa Karo
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Pankaj Batra
| producer       = Suresh Kumar Monty Shoor Sukhjinder Bhachu Aman Khatkar Manmord Sidhu
| writer         = Aman Khatkar Romy Kendola Sukhjinder Bhachu
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Amrinder Gill Amrit Maghera Yograj Singh Aman Khatkar Binnu Dhillon Rana Ranbir Karamjit Anmol Sardar Sohi Terence H. Winkless
| music          = Jatinder Shah
| cinematography = Vineet Malhotra
| editing        = Parveen Kathikuloth
| studio         = Speed Records
| distributor    = 
| released       =  
| runtime        = 
| country        = India Punjabi
| budget         = 
| gross          =   
}}
 Punjabi film Punjabi singer-actor Amrinder Gill and British Indian actress Amrit Maghera in the lead roles.  

==Plot==
For Kala, a boy from a village in Punjab, India, it is a love at first sight when he sees Aleesha, an Indo-Canadian girl. Kala also helps clear obstacles in the way of marriage of his brother Roop to his white Canadian girlfriend, Julia. Kala fights the cultural and historical differences to bring together and unite the two families.

The film is shot at locations in India and Canada.

==Cast==
* Yograj Singh as Najar Singh Maan
* Amrinder Gill as Kalajeet Singh Maan alias Kala
* Amrit Maghera as Alisha
* Binnu Dhillon as Jaggi Maan alias Maan Saab
* Aman Khatkar as Roop Maan
* Terence H. Winkless as Albert 
* Rana Ranbir as Jeeja Ji
* Sardar Sohi as Fuffar
* Karamjit Anmol as Chacha

==Awards==
PTC Punjabi Film Awards 2015

* Won - PTC Punjabi Film Award for Best Screenplay - Amberdeep Singh

==References==
 

 
 
 