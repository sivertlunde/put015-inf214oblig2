House of Cards (1993 film)
{{Infobox Film
| name           = House of Cards
| image          = Houseofcardsdvd.jpg
| image_size     = 
| caption        = 
| director       = Michael Lessac
| producer       = Lianne Halfon Wolfgang Glattes Dale Pollock
| writer         = Michael Lessac Robert Jay Litz (story)
| starring       = Kathleen Turner Tommy Lee Jones
| music          = James Horner
| cinematography = Victor Hammer
| editing        = Walter Murch
| distributor    = Miramax Films
| released       = June 25, 1993
| runtime        = 109 minutes
| country        =  
| language       = 
| budget         = 
| gross          = $322,871
}}
House of Cards is a 1993 drama film directed by Michael Lessac and starring Kathleen Turner and Tommy Lee Jones. It follows the struggle of a mother to reconnect with her daughter who has been traumatized by the death of her father. The film premiered at the 1993 Sundance Film Festival before being acquired by Miramax Films for distribution in June of the same year. 

==Synopsis==
Following the death of her husband, Ruth Matthews moved her family to a quiet suburb, hoping to put the past behind them. While her son Michael is able to adapt, her daughter, Sally, is apparently traumatized by the experience and starts displaying unusual behavior. Ruth is later court mandated to see Jake Beerlander, an expert in child autism, to help Sally.

==Cast==
* Kathleen Turner as Ruth Matthews
* Tommy Lee Jones as Jake Beerlander
* Asha Menina as Sally Matthews
* Shiloh Strong as Michael Matthews
* Esther Rolle as Adelle
* Park Overall as Lillian Huber
* Michael Horse as  Stoker
* Jacqueline Cassell as Gloria Miller

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 