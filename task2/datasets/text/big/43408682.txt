Play Safe
{{Infobox film
| title          = Play Safe
| image          = 
| caption        = 
| director       = Joseph Henabery
| producer       = Monty Banks Howard Estabrook
| writer         = Charles Horan Henry Sweet
| narrator       = 
| starring       = Monty Banks Virginia Lee Corbin
| music          = 
| cinematography = Blake Wagner
| editing        = 
| studio         = Monty Banks Productions
| distributor    = Pathe Exchange
| released       = January 30, 1927
| runtime        = 50 minutes (5 reels)
| country        = United States English intertitles
| budget         = 
| gross          = 
| awards         = 
}}
Play Safe (1927 in film|1927) is a silent film comedy starring Monty Banks.   An abridged two-reel version was shown in the US as Chasing Choo Choos.   

==Cast==
*Monty Banks as Monty
*Virginia Lee Corbin as Virginia Craig
*Charles Hill Mailes as Silas Scott
*Charles K. Gerrard as Scotts son
*Bud Jamison as Big Bill
*Max Ascher (billed as Max Asher) Fatty Alexander
*Rosa Gore 
*Syd Crossley

==Plot==
A gang of bad guys menace a mans girlfriend. She hides in a freight car and a misstep sends the otherwise-empty train out of the station with the lever pushed to full speed.

As the train gains speed, the captives boyfriend must board the runaway train, repel the pursing gang, get his girl out of the boxcar, and somehow get the two of them to safety. Tunnels, a water tower, a steep grade, and a frayed rope complicate the heros task.

==Preservation status==
Prints in 35mm and 16mm exist. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 