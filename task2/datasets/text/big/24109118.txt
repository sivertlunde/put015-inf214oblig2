Breakfast for Two
{{Infobox film
| name           = Breakfast for Two
| caption        = 
| image	=	Breakfast for Two FilmPoster.jpeg
| director       = Alfred Santell Charles Kaufman Paul Yawitz Viola Brothers Shore
| starring       = Barbara Stanwyck Herbert Marshall Glenda Farrell Eric Blore
| producer       = Edward Kaufman
| music          = 
| cinematography = J. Roy Hunt
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| country        = United States
| released       =  
| runtime        = 67 minutes
| language       = English
| budget         = $500,000
}}
 directed by Stella Dallas (1937), receiving an Oscar nomination. Stanwyck and Marshall worked together once more, immediately following this film, on the 20th-Century-Fox drama Always Goodbye (1938). 

==Plot== bit in his mouth and make him like it". In her way is Jonathans girlfriend, actress Carol Wallace (Glenda Farrell).

Jonathan is dismayed to discover that his neglected family shipping firm is in dire trouble, and that he will not be receiving his usual check, leaving him broke. Valentine decides to use this news to ignite his ambition. She buys up controlling interest in the company and moves into his home as the new tenant. When he discovers the identity of the new owner, he wrongly assumes she went out with him solely to learn what she could about the company. Furious, he tells her that he will fight to get the company back, but later, to his valet, Butch (Eric Blore), he admits he is beaten, as nobody will lend him the money he needs to make the attempt. Butch, who approves of Valentine, informs her of this. She makes Jonathan vice president, but he visits the office only to inform her that Carol has asked him to marry her, and that he has accepted.

That afternoon, Valentine tries her best to disrupt the ceremony (with the help of noisy bearded window washers), presided over by an increasingly frustrated justice of the peace (Donald Meek). Finally, Sam Ransome bursts in and declares that Carol is the mother of his children. The wedding is off, but one of the guests (Etienne Girardot) recognizes Sam and informs Jonathan.

The next day, Jonathan outlines to the firms receivership board his bold new plan to get the company back on its financial feet. The board members vote to accept his scheme and return control of the business to him. Valentine is pleased by his display of initiative and drive ... until he tells her that the wedding with Carol is back on. In desperation, Butch produces a forged marriage certificate showing that Valentine and Jonathan are husband and wife. Carol leaves in a huff.

After Butch informs Valentine of the deception, she continues the masquerade, much to Jonathans discomfort. When Butch confesses the truth to Jonathan, however, the tables are turned. She flees from her suddenly amorous "husband". However, at the train station, they make peace and get married for real.

== Cast ==
* Barbara Stanwyck as Valentine Ransome
* Herbert Marshall as Jonathan Blair
* Glenda Farrell as Carol Wallace
* Eric Blore as Butch, Blairs Valet
* Donald Meek as Justice of the Peace
* Etienne Girardot as Mr. Meggs
* Frank M. Thomas as Sam Ransome
* Pierre Watkin as Gordon Faraday
* Tom Ricketts as Receivership Hearing Member (uncredited)
* Sidney Bracey as Clarence, Blairs Butler (uncredited) Harold Goodwin as Joe, Blairs Chauffeur (uncredited)  George Irving as Receivership Hearing Judge (uncredited)
* Edward LeSaint as Receivership Hearing Member (uncredited) Edmund Mortimer as Man in Waiting Room (uncredited)
* Larry Steers as Receivership Hearing Member (uncredited)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 