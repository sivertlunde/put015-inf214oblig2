This Way of Life
 
 
 
{{Infobox film
| name           = This Way of Life
| image          = this_way_of_life.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Thomas Burstyn
| producer       = Barbara Sumner-Burstyn
| writer         =
| starring       =
| music          = Joel Haines
| cinematography = Thomas Burstyn
| editing        = Cushla Dillon
| studio         = Cloud South Films
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = New Zealand
| language       = English
| budget         = personal budget of 
| gross          =
}}
This Way of Life is a New Zealand documentary film about a horse breeding family living in the wild near the Ruahine Ranges, resisting the call to a more "modern" lifestyle. It was directed by Thomas Burstyn and produced by Barbara Sumner-Burstyn.

It opened theatrically in Vancouver on 3 October 2009 at the Vancouver International Film Festival, Seattle on 11 June 2010 at the Seattle International Film Festival, and in New York City on 30 July 2010. It opened in Los Angeles on 6 August 2010 at the 14th Annual DocuWeeks.   

This Way of Life made the 2011 Oscar Documentary long list.
 

==Cast==
* Peter Ottley-Karena
* Colleen Ottley-Karena
* Llewelyn Ottley-Karena
* Aurora Ottley-Karena
* Malachi Ottley-Karena
* Elias Ottley-Karena
* Corban Ottley-Karena
* Salem Ottley-Karena

==References==
 

==External links==
*  
*  
*  “This way of life” – a Karena viewpoint

 
 
 
 

 
 