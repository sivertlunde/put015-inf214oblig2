Mr. Moto in Danger Island
{{Infobox film
| name           = Mr. Moto in Danger Island
| image          =
| image_size     =
| caption        =
| director       = Herbert I. Leeds John Stone Peter Milne John Reinhardt  George Bricker Jack Jungmeyer Edith Skouras 
| based on       =  
| narrator       =
| starring       = Peter Lorre Jean Hersholt Amanda Duff Warren Hymer
| music          = Samuel Kaylin 
| cinematography = Lucien N. Andriot  Harry Reynolds    
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       = April 7, 1939
| runtime        = 70 minutes
| country        = United States English 
| budget         = 
| gross          = 
}}

Mr. Moto in Danger Island is a 1939 American mystery film directed by Herbert I. Leeds and starring Peter Lorre, Jean Hersholt and Amanda Duff. It is part of the Mr. Moto series of films.  
 Murder in Trinidad, but the setting was moved to Puerto Rico. 

==Plot==
Mr Moto arrives in Puerto Rico to investigate the murder of an American agent and uncovers a smuggling ring.

==Main cast==
*  Peter Lorre as Mr. Moto 
* Jean Hersholt as Sutter 
* Amanda Duff as Joan Castle 
* Warren Hymer as Twister McGurk   Richard Lane as Commissioner Gordon 
* Leon Ames as Commissioner Madero 
* Douglass Dumbrille as La Costa
* Charles D. Brown as Col. Thomas Castle   Paul Harvey as Gov. John Bentley  Robert Lowery as Lt. George Bentley  
* Eddie Marr as Capt. Dahlen   Harry Woods as Grant

==Production==
The novel Murder in Trinidad has been filmed in 1934 starring Nigel Bruce. It was then adapted as a Charlie Chan film before being converted into a Moto story.

It was also known as Mr Moto in Trinidad, Mr Moto in Puerto Rico, and Mr Moto in Terror Island.

Filming started 25 November 1938. SCREEN NEWS HERE AND IN HOLLYWOOD: RKO and United Artists Seek Anna Neagle--Metro Plans Remake of Desert Song NEW ROLE FOR MISS FAYE Slated for Life of William Tell--Goldwyn Prepares to Film Beach Boy Plans for Alice Faye Coast Scripts Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   22 Nov 1938: 26 

==Reception==
The new York Times said the film has "some humour... a bit of action, but little suspense." THE SCREEN: Concentration Camp Opens at Waldorf Theatre-- Mr. Moto in Danger Island at the Central At the Teatro Hispano At the Central
New York Times (1923-Current file)   20 Mar 1939: 18.  

==References==
 

==Bibliography==
* Backer, Ron. Mystery Movie Series of 1930s Hollywood. McFarland, 2012. 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 