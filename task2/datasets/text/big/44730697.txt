Ankusham
{{Infobox film|
| name = Kodi Ramakrishna
| image = 
| caption = 
| director = Kodi Ramakrishna
| story  = M. S. Art Movies Unit
| writer = 
| starring = Dr. Rajashekar
| producer = M. Shyam Prasad Reddy
| music = Chellapilla Satyam
| cinematography = K. S. Hari
| editing = K. A. Marthand
| studio =
| released =  
| runtime = 135 minutes Telugu
| country = India
| budget =
}}
Ankusam is 1990 Telugu action film directed by Kodi Ramakrishna and produced by Shyam Prasad Reddy. The film stars Rajasekhar and his wife Jeevitha in lead roles while Rami Reddy and MS Reddy play supporting roles. The film is about an honest cop who fight against his enemies and saves the life of his mentor who is the chief minister by sacrificing his life. The film become a blockbuster at box-office and provided major breakthrough in the career of Rajasekhar and Kodi Ramakrishna.  The film was dubbed and released in Tamil as Idhuthaanda Police which also became successful. Ravi Raja Pinisetty directed the Hindi and Kannada remakes - Pratibandh and Abhimanyu (1990 film)|Abhimanyu with Chiranjeevi and V. Ravichandran respectively in 1990.  

==Cast== Rajasekhar
*Jeevitha Ramireddy
*M.S. Reddy
*Babu Mohan
*Prasadbabu

==Legacy==
The film became successful at box office and provided major breakthrough in the career of Rajasekhar. Rajasekhars portrayal of honest and short-tempered police officer received critical acclaim and he went on to be typecasted in similar roles throughout his career.  Both director and actor again teamed up for Nayakudu which failed to repeat the success of Ankusham.  Ramireddy also became famous as villain and did similar characters in the films. 

Srinu Vaitla said that:"Two films – RGV’s Shiva and Kodi’s Ankusam – deeply influenced me and directly or indirectly prompted me to become a director". 

== References ==
 

 
 
 
 
 

 