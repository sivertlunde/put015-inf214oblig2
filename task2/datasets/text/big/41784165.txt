Genome Hazard
{{Infobox film
| name           = Genome Hazard
| image          = 
| alt            = 
| caption        =  Kim Sung-su
| producer       = Hidemi Satani   Lee Geun-wook   Yang Gwang-deok   Cha Won-cheon
| writer         = Kim Sung-su
| based on       =   Hidetoshi Nishijima Kim Hyo-jin
| music          = Kenji Kawai
| cinematography = Choi Sang-muk
| editing        = Park Gyeong-suk
| studio         = Lotte Entertainment Sovik Global Contents Investment Fund Happinet
| distributor    = Happinet (Japan)   Lotte Entertainment (South Korea)
| released       =   
| runtime        = 120 minutes
| country        = Japan South Korea
| language       = Japanese   Korean
| budget         = 
| gross          =   ( )
}} action thriller thriller film Kim Sung-su based on a novel by Shiro Tsukasaki.   In 2014, the film was released theatrically in Japan on January 24, and South Korea on May 29. 

==Cast== Hidetoshi Nishijima as Ishigami Taketo
* Kim Hyo-jin as Kang Ji-won
* Yōko Maki (actress)|Yōko Maki as Miyuki
* Yuri Nakamura as Han Yu-ri
* Manabu Hamada as Ibuki
* Masatō Ibu as Dr. Sato
* Lee Geung-young 
* Nahana as Koyoko

==Reception==
The film grossed   ( ) at the Japanese box office. 

Choi Sang-muk won the Bronze Medal for Cinematography at the 2014 Golden Film Festival.

==References==
 

==External links==
*   
* 
* 
* 

 
 
 
 
 
 
 

 
 