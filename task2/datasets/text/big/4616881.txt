Slam Dunk Ernest
 
{{Infobox film
| name=Slam Dunk Ernest
| image=Slam_Dunk_Ernest.jpg
| caption=
| director=John R. Cherry III
| writer= John R. Cherry III  Daniel Butler
| starring= Jim Varney Kareem Abdul-Jabbar Jay Brazeau Louise Vallance Cylk Cozart Miguel A. Núñez, Jr.
| producer= George Horie Stacy Williams
| studio = Touchstone Pictures Emshell Producers
| released=  
| runtime= 93 minutes
| country= United States
| language= English
| budget=
}}
Slam Dunk Ernest is a 1995 sports comedy film, and the eighth full-length feature film starring Jim Varney as Ernest P. Worrell.  It was released direct-to-video, and was directed by long-time Ernest collaborator John R. Cherry III. In this movie, Ernest joins his employers basketball team and later becomes a star with the help of an angel (Kareem Abdul-Jabbar). It was third and final "Ernest" movie to be filmed in Vancouver, British Columbia.

The magic shoes concept was later used for the film Like Mike. 

==Plot synopsis==

Ernest (Jim Varney) takes a job with a cleaning service at the local mall, and he soon seeks to join his co-workers basketball team, "Clean Sweep", as they compete in the city league tournament. He is reluctantly accepted by the team, but given only a minor role as their cheerleader and mascot. In his despair, he is visited by an angel (played by basketball legend Kareem Abdul-Jabbar), and given a pair of magical shoes, but is warned, "Dont misuse the shoes." As a matter of fact, the shoe stores owner, Zamiel Moloch happens to be a demon in disguise. He would eventually prevent Ernests sportsmanship with the basketball players by luring Ernest to arrogance. 
 exhibition contest against the Hornets, but suffers turmoil as Ernests teammates soon grow weary of his ball hog|ball-hogging antics.

While Ernest is in the zone and the team does nothing but sit around, Ernest decides to let the team do the work and get rid of the shoes, but Ernest is needed again and scores (along with causing some mishaps) the game-winning point and the members of the team get drafted into the NBA.

==Cast==
*Jim Varney as Ernest P. Worrell
*Kareem Abdul-Jabbar as The Archangel of Basketball
*Jay Brazeau as Mr. Zamiel Moloch Louise Vallance as Miss Erma Terradiddle
*Cylk Cozart as Barry Worth
*Miguel A. Núñez, Jr. as T.J.

==Reception==
The film currently has a 20% "rotten" rating on Rotten Tomatoes.

==DVD availability== Hey Vern, Its My Family Album on June 5, 2012.

== External links ==
*  

 
 

 
 
 
 