Pinky (film)
{{Infobox film
| name           = Pinky
| image          = Pinky 1949 poster.jpg
| image_size     = 200px
| caption        = Theatrical release poster
| director       = Elia Kazan
| producer       = Darryl F. Zanuck Philip Dunne  Dudley Nichols
| based on       =  
| starring       = Jeanne Crain  Ethel Barrymore  Ethel Waters Alfred Newman
| cinematography = Joseph MacDonald
| editing        = Harmon Jones
| distributor    = 20th Century Fox
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Best Actress Best Actress in a Supporting Role.
 Philip Dunne and Dudley Nichols and directed by Elia Kazan.
 passing for white. Pinky was released by Twentieth Century Fox to both critical acclaim and controversy.

==Plot==
  black heritage.
 Kenny Washington), a black physician, asks Pinky to train black students who want to become nurses, but Pinky tells him she plans to return North.

Dicey asks her to stay temporarily to care for her ailing, elderly white friend and neighbor, Miss Em (Ethel Barrymore). Pinky has always disliked Miss Em and lumps her in with the other bigots in the area. Pinky relents and agrees to tend Miss Em after learning that she personally cared for Dicey when she had pneumonia. Pinky nurses the strong-willed Miss Em, but does not hide her resentment. As they spend time together, however, she grows to like and respect her patient.

Miss Em bequeaths Pinky her stately house and property when she dies, but greedy relative Melba Wooley (Evelyn Varden) challenges the will. Everyone advises Pinky that she has no chance of winning, but something she herself does not fully comprehend makes her go on. Pinky begs retiring Judge Walker (Basil Ruysdael), an old friend of Miss Ems, to defend her in court. With great reluctance, he agrees to take the case. Pinky washes clothes by hand when her grandmother is sick in order to pay his expenses. At the trial, despite hostile white spectators and the non-appearance of the only defense witness, presiding Judge Shoreham unexpectedly rules in Pinkys favor. When Pinky thanks her attorney, he coldly informs her that justice was served, but not the interests of the community in his opinion.

Tom, who has tracked Pinky down, wants her to sell the inherited property, resume her masquerade as a white woman, marry him and leave the South, but she refuses, firmly believing that Miss Em intended her to use the house and property for some purpose. As a result, they part. In the end, Pinky establishes "Miss Ems Clinic and Nursery School" for blacks.

==Cast==
*Jeanne Crain as Patricia "Pinky" Johnson
*Ethel Barrymore as Miss Em
*Ethel Waters as Dicey Johnson
*William Lundigan as Dr. Thomas "Tom" Adams
*Basil Ruysdael as Judge Walker
*Kenny Washington as Dr. Canady
*Nina Mae McKinney as Rozelia
*Griff Barnett as Dr. Joe McGill
*Frederick ONeal as Jake Walters
*Evelyn Varden as Melba Wooley
*Raymond Greenleaf as Judge Shoreham
*Arthur Hunnicutt as Police Chief (uncredited)

==Production notes==
John Ford was originally hired to direct the film but was replaced after one week because producer Darryl F. Zanuck was unhappy with the dailies. 

===Casting===
Both Lena Horne and Dorothy Dandridge were interested in playing the role of Pinky.   In the end, Jeanne Crain was chosen for the role. Elia Kazan, who took over directing duties after John Ford was fired, was not happy with the casting choice. He later said, "Jeanne Crain was a sweet girl, but she was like a Sunday school teacher. I did my best with her, but she didnt have any fire. The only good thing about her was that it went so far in the direction of no temperament that you felt Pinky was floating through all of her experiences without reacting to them, which is what passing is." 

==Critical reception== First Amendment did not protect movies, per Mutual Film Corporation v. Industrial Commission of Ohio (1915).

The City Commission of Marshall "reactivated" the Board of Censors, established by a 1921 ordinance, and designated five new members who demanded the submission of the picture for approval. The board disapproved its showing, stating in writing its "unanimous opinion that the said film is prejudicial to the best interests of the citizens of the City of Marshall." Gelling nonetheless exhibited the film and was charged with a misdemeanor.

Three members of the Board of Censors testified that they objected to the picture because it depicts (1) a white man retaining his love for a woman after learning that she is a Negro, (2) a white man kissing and embracing a Negro woman, (3) two white ruffians assaulting Pinky after she has told them she is colored. Gelling was convicted and fined $200. He appealed the conviction all the way to the U.S. Supreme Court.
 First Amendment protection to films.  The Court then overturned Gellings conviction. 

==References==
 

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 