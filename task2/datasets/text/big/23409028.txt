Shall We Kiss?
Shall We Kiss? (French Title: Un baiser sil vous plaît) is a 2007 French romantic comedy film directed by Emmanuel Mouret and starring Virginie Ledoyen, Mouret, Julie Gayet, Michaël Cohen, Frédérique Bel and Stefano Accorsi. 

==Synopsis== platonic kiss. She accepts, and subsequently what should have been a simple act between friends without consequences becomes complicated, and the situation quickly becomes out of control.

==Cast==
* Virginie Ledoyen: Judith (scientist)
* Emmanuel Mouret: Nicolas Gimas (mathematics professor), Judiths best friend
* Julie Gayet: Émilie (creative writer), friend of Judiths
* Michaël Cohen: Gabriel (art restorer), Émilies love interest
* Frédérique Bel: Caline (airline hostess), Nicolas girlfriend
* Stefano Accorsi: Claudio (pharmacist), Judiths husband
* Marie Madinier: Églantine (prostitute)
* Mélanie Maudran: Pénélope
* Lucciana de Vogue: Louise
* Jacques Lafoly: waiter

==Reception==
The film received mostly positive reviews, scoring 76% on Rotten Tomatoes. 

==References==
 

==External links==
*  

 
 
 
 
 


 
 