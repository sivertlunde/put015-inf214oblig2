Apna Sapna Money Money
{{Infobox film
| name             = Apna Sapna Money Money
| image            = ApnaSapnaMoneyMoneyimage.jpg
| image_size       =
| caption          = Theatrical release poster
| director         = Sangeeth Sivan
| producer         = Subhash Ghai Raju Farooqui
| story            = 
| screenplay       = Pankaj Trivedi sachin shah
| starring         = Sunil Shetty Ritesh Deshmukh Jackie Shroff Celina Jaitley Koena Mitra Riya Sen Shreyas Talpade
| music            = Pritam
| cinematography   = Ramji
| editing          = Bunty Nagi Chirag Jain
| distributor      = Mukta Arts
| released         = November 10, 2006
| runtime          = 138 mins
| country          = India
| language         = Hindi
| budget           =
| gross            = 
| preceded_by      =
| followed_by      =
}}
Apna Sapna Money Money (English: Money Is Our Dream) is a Bollywood film released on 10 November 2006. The movie revolves around a group of characters that eat, drink, sleep, and worship money.

Directed by  , Celina Jaitley, Koena Mitra, Jackie Shroff, Sunil Shetty, Shreyas Talpade, Bobby Darling, Riya Sen, Anupam Kher, Rajpal Yadav and Chunky Pandey.

==Plot==

6 Alvarez House in Bandra, Mumbai, is the residence of myopic, widowed, devout Hindu, Satyabol Shashtri (Anupam Kher), who lives there with his daughter, Shivani (Riya Sen). Shivani is in love with her Christian neighbor, Arjun Fernandes (Shreyas Talpade), who is a mechanic and lives with his wanna-be singer sister, Julie (Koena Mitra), and a younger ailing sister, Titli. Satyabol disapproves of Arjun and wants his daughter to get married to Sarju Maharaj Banaraswales son. Other than Satyabol chasing Badshah, the pet dog of Arjun, and ending up in the womans bathroom, the area is fairly peaceful. 

Then Arjun and his friends try to think of a plan to stop Sarju from marrying his son to Shivani but cant. So Arjun calls his cousin Kishan (Ritesh Deshmukh) to help them. Kishan comes disguised as Sarju Maharaj Banaraswale by tricking the real Sarju into getting of the train and as soon as he gets of the train, some goons who think he is a relative of Kishan catch him and take him to find out where Kishan is. Meanwhile Kishan becomes Sarju and convinces Satyabol that his (Sarjus) son is not good for his (Shashtris) daughter by dancing in the bar and kissing Julie who was in the act. Then Shashtri informs the fake Sarju that he does not want to marry his daughter to Sarjus son and that he can go now. So having finished his mission, Kishan prepares to leave when Arjun tells him to stay back as he loves Julie but Kishan refuses saying that he is not destined for julie. Then after Arjun leaves and Rana (Chunky Pandey) turns up with the real Sarju but when Kishan says that all the is with Sarju they leave Kishan and run after Sarju while Kishan escapes, disguises himself as a woman named Sunaina and says she is Arjuns aunty and soon Shashtri falls in love with her. Then in the neighbourhood also comes Matha Prasad (Rajpal Yadav) who runs a dairy farm and moonlights as the hit man of Bangkok based underworld Don, Carlos (Jackie Shroff). The once honest cop Namdev Mane (Sunil Shetty) ( pronounced as Maa-ne ) teames up with Carlos girlfriend Sania (Celina Jaitley) who is on the lookout for hidden diamonds and facing bankruptcy - Carlos himself - as they face off in one of the most hilarious stand-offs to seek wealth and to fulfill their individual dreams.

==Cast==
* Sunil Shetty as Namdev Mane, an honest cop dedicated to putting criminals behind bars. Some clever criminals who work in disguise have kept him on his toes.
* Ritesh Deshmukh as Kishan, a young man from Goa whose brain is full of every conceivable trick to make a quick buck. He cons people by slipping into different outfits and by assuming many guises. He is interested in Julie
* Celina Jaitley as Sania Badnaam, a con-woman who uses her beauty to lure men and leave them penniless later. Beautiful and unwilling to surrender to emotions easily, Sania craves only money and power. 
* Koena Mitra as Julie Fernandez. Julie is a club dancer with quite a reputation, but behind her raunchy exterior is a compassionate, kind-hearted woman who goes out of her way to help the needy. She feels something for Kishan.
* Shreyas Talpade as Arjun Fernandez, an honorable man who is content making a simple living with whatever money he earns from his garage. And later on he meets the woman of his dreams Shivani Shastri.
* Riya Sen as Shivani Shastri who is a simple girl from a Brahmin family, but her strict father, Pandit Satyabol Shastri, would never let her marry Arjun. It will take a lot of effort to win her hand from her father.
* Anupam Kher as Pandit Satyabol Shastri, the stubborn, overprotective, and aging father of Shivani who will not give away his daughter easily.
* Jackie Shroff as Danny Carlos is a merciless underworld don with wacky fashion sense. He works from Bangkok and operates in India through Sania.
* Chunky Pandey as Rana Jang Bahadur
* Rajpal Yadav as Matha Prasad
* Sunil Pal as Matha Prasads assistant
* Bobby Darling as Bobby Mohabbati

==Music==
All track composed by Pritam and lyrics penned by Shabbir Ahmed apart from the song Paisa Paisa which was written by Mayur Puri.

{{tracklist
| headline = Tracklist   
| music_credits = no
| lyrics_credits = yes
| extra_column = Artist(s)
| title1 = Dil Mein Baji Guitar
|
| extra1 = Mika Singh
| length1 = 4:26
| title2 = Dil Mein Baji Guitar - 1
|
| extra2 = Amit Kumar
| length2 = 4:25
| title3 = Gustakh Nigah
|
| extra3 = Sukhwinder Singh, Alisha Chinoy
| length3 = 5:37
| title4 = Gustakh Nigah - 1
|
| extra4 = Sukhwinder Singh, Alisha Chinoy
| length4 = 4:35
| title5 = Jai Jai Money
|
| extra5 = Sukhwinder Singh, Rana Mazumder, Bob
| length5 = 4:17
| title6 = Jai Jai Money - 2
|
| extra6 = Sukhwinder Singh, Rana Mazumder, Bob
| length6 = 4:47
| title7 = Jai Jai Money - 3
|
| extra7 = Sukhwinder Singh, Rana Mazumder, Bob
| length7 = 4:12
| title8 = Paisa Paisa
| lyrics8 = Mayur Puri
| extra8 = Suzanne DMello, Hamza Faruqui
| length8 = 4:06
| title9 = Paisa Paisa - 1
| lyrics9 = Mayur Puri
| extra9 = Suzanne DMello, Hamza Faruqui
| length9 = 4:32
| title10 = Paisa Paisa - 2
| lyrics10 = Mayur Puri
| extra10 = Suzanne DMello, Hamza Faruqui
| length10 = 4:30
| title 11 = Saanya Badnaam
}}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 