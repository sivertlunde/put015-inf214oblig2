Karulina Kudi
{{Infobox film|
| name = Karulina Kudi
| image = 
| caption =
| director = V. P. Sarathy
| writer = V. P. Sarathy Vishnuvardhan  Sithara 
| producer = P. Dhanraj
| music = Rajan-Nagendra 
| cinematography = P. N. Sundaram
| editing = Shyam
| studio = Sri Dhanalakshmi Creations
| released = 1995
| runtime = 140 minutes
| language = Kannada
| country = India
| budgeBold textt =
}} Kannada drama drama film Sithara in the lead roles. The film was widely popular for the songs composed by Rajan-Nagendra upon release.

== Cast == Vishnuvardhan 
* Ambareesh Sithara
* Shamili
* Jai Jagadish
* Doddanna
* Rajanand
* Sathyapriya
* Sriraksha
* Master Anand

== Soundtrack ==
The music of the film was composed by Rajan-Nagendra. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| collapsed = no
| title1 = Nanagagi Neenu
| extra1 = S. P. Balasubrahmanyam & S. Janaki
| lyrics1 = 
| length1 = 
| title2 = Krishna Krishna Ellige Hode
| extra2 = S. Janaki
| lyrics2 =
| length2 = 
| title3 = Rattho Rattho
| extra3 = S. P. Balasubrahmanyam & S. Janaki
| lyrics3 =
| length3 = 
| title4 = Pakshigale Keli Nannaya Katheya
| extra4 = S. P. Balasubrahmanyam & S. Janaki
| lyrics4 =
| length4 = 
| title5 = Amma Amma Ellige Hode
| extra5 = S. Janaki
| lyrics5 = 
| length5 = 
| title6 = O Malle Hoove Rajkumar
| lyrics6 = 
| length6 =
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 