Jack the Ripper (1959 film)
{{Infobox film
 | name           = Jack the Ripper
 | image          = 
 | caption        =  Peter Hammond Monty Berman
 | based on       = 
 | starring       = Lee Patterson Eddie Byrne Betty McDowall John Le Mesurier Ewen Solon
 | director       = Monty Berman Robert S. Baker
 | producer       = 
 | studio         = Tempean Films|Mid-Century Film Productions
 | distributor    = Regal Film Distributors (UK) Joseph E. Levine (US)
 | released       =  
 | music          = Stanley Black (UK) Jimmy McHugh (US) Pete Rugolo (US)
 | cinematography = Robert S. Baker Monty Berman
 | editing        = Peter Bezencenet
 | runtime        = 84 min.
 | country        = United Kingdom
 | language       = English  budget = £50,000 John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 56-61  gross = $1.1 million (US) 
}}
Jack the Ripper was a 1959 film produced and directed by Monty Berman and Robert S. Baker and is loosely based on Leonard Matters theory that Jack the Ripper was an avenging doctor.  The  black-and-white film starred Lee Patterson and Eddie Byrne and co-starred Betty McDowall, John Le Mesurier, and Ewen Solon.

The film borrowed icons from previously successful horror films, such as Dracula (1958 film)|Dracula (1958) and The Curse of Frankenstein (1957), by giving the Ripper a costume of a top hat and cape.  The plot is a standard "whodunit" with the usual false leads and a denouement in which the least likely character, in this case "Sir David Rogers" played by Ewen Solon, is revealed as the culprit.  As in Matters book, The Mystery of Jack the Ripper, Solons character murders prostitutes to avenge the death of his son. However, Matters used the ploy of the son dying from venereal disease, while the film has him committing suicide on learning his lover is a prostitute.  

==Plot==
 
In 1888, Jack the Ripper is on his killing spree. Scotland Yard Inspector ONeill (Byrne) welcomes  a visit from his old friend New York City detective Sam Lowry (Patterson) who agrees to assist with the investigation. Sam becomes attracted to modern woman Anne Ford (McDowall) but her guardian, Dr. Tranter (Le Mesurier) doesnt approve. The police slowly close in the killer as the public becomes more alarmed. The killers identity is revealed and the killer meets a ghastly end.

==Cast==
* Lee Patterson as Sam Lowry
* Eddie Byrne as Inspector ONeill
* Betty McDowall as Anne Ford
* Ewen Solon as Sir David Rogers
* John Le Mesurier as Dr. Tranter George Rose as Clarke
* Philip Leaver as Music Hall Manager
* Barbara Burke as Kitty Knowles
* Anne Sharp as Helen
* Denis Shaw as Simes Jack Allen as Assistant Commissioner Hodges
* Jane Taylor as Hazel
* Dorinda Stevens as Margaret
* Hal Osmond as Snakey the pickpocket

==Production==
The films budget was raised from a combination of pre-sales to Regal Film Distributors at the National Film Finance Corporation. 

==Release==
Joseph E. Levine bought the US rights for £50,000. He later claimed he spent $1 million on promoting the movie and earned $2 million in profit on it. 

According to Variety, the film earned rentals of $1.1 million in North America on initial release. "Rental Potentials of 1960", Variety, 4 January 1961 p 47. Please note figures are rentals as opposed to total gross. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 

 