Marupadiyum
{{Infobox film
| name = Marupadiyum
| image = Marupadiyum dvd.jpg
| caption = Official DVD Cover
| director = Balu Mahendra   
| producer = Ashwin Kumar
| writer = Balu Mahendra
| story = Mahesh Bhatt
| narrator = Rohini Vinothini
| music = Ilaiyaraaja
| cinematography = Balu Mahendra
| editing = Balu Mahendra
| studio = Ashwin International
| distributor = Ashwin International
| released = 14 January 1993
| runtime = 139 minutes
| country = India
| language = Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}}			 Rohini and top 5 Tamil films of the year.
__TOC__

==Plot==
Marupadiyum is an emotional and psychological drama that focuses on the central female protagonist, a wife caught up in marital discord played by Revathi and her life henceforth. Revathi is married to Ravi, a movie director who has an extra-marital affair with his star heroine, played by Rohini. Revathi discovers this intimacy between her husband and Rohini but is treated with indifference by her husband. Ravi moves on to get a divorce from Revathi. Revathi pleads with Ravi not to proceed with the divorce and asks Rohini to leave her husband but both disagree to do so. Arvind Swamy, a stranger, helps and befriends Revathi after she leaves her husband.

Rohini faces mental anguish as she feels guilty with another womans husband and becomes psychologically affected. Ravi ends up in dilemma between his wife and his lover. (Aravind Swamy) turns into a dependable friend and asks Revathi to marry him but Revathi has to choose her own path.   

==Cast==
*Nizhalgal Ravi as Murali Krishna
*Revathi as Thulasi Rohini as Kavitha
*Arvind Swamy as Gowri Shankar

==Soundtrack==
{{Infobox album  	
| Name        = Marupadiyum	
| Type        = 	soundtrack
| Artist      = Ilaiyaraaja	
| Cover       = 	
| Caption     = 	
| Released    = 	 
| Recorded    = 	
| Genre       = 	
| Length      = 	 Tamil
| Label       = 	
| Producer    = 	
| Reviews     = 	
| Compiler    = 	
| Misc        = 	
}}	

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Lyrics
|-
| 1
| "Aasai Athigam"
| S. Janaki
| Ravi Bharathi
|-
| 2
| "Ellorukum Nalla Kaalam"
| K. J. Yesudas Vaali
|-
| 3
| "Ellorum Sollu Pattu"
| S. P. Balasubrahmanyam Vaali
|-
| 4
| "Nalam Vazha"
| S. P. Balasubrahmanyam Vaali
|-
| 5
| "Nallathor Veenai"
| S. Janaki
| Ravi Bharathi
|}

==References==
 

==External links==
* 			
			
 

 
 
 
 
 
 
 
 


 