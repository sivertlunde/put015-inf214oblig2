DOA: Dead or Alive
 
 
 
{{Infobox film
| name           = DOA: Dead or Alive
| image          = DOA.jpg
| alt            = Three female fighters standing ready   Tina and Kasumi
| director       = Corey Yuen
| producer       = {{Plainlist|
* Paul W. S. Anderson
* Jeremy Bolt}}
| writer         = {{Plainlist|
* Adam Gross
* Seth Gross
* J.F. Lawton}}
| based on       =  
| starring       = {{Plainlist|
* Devon Aoki
* Jaime Pressly
* Holly Valance
* Sarah Carter
* Natassia Malthe
* Kane Kosugi
* Eric Roberts
* Matthew Marsden Steve Howey
* Collin Chou
* Kevin Nash
* Brian J. White}}
| music          = Junkie XL
| cinematography = {{Plainlist|
* Chi Ying Chan
* Kwok-Man Keung}}
| editing        = {{Plainlist|
* Ka-Fai Cheung
* Eddie Hamilton
* Angie Lam}}
| studio         = {{Plainlist|
* Mindfire Entertainment
* Team Ninja
* VIP 4 Medienfonds}}
| distributor    = {{Plainlist|
* Dimension Films
* The Weinstein Company  
* Constantin Film   Universal Pictures  }}
| released       =  
| runtime        = 86 minutes  
| country        = {{Plainlist|
* United States
* Germany
* United Kingdom }}
| language       = English 
| budget         = $21 million
| gross          = $7.5 million 
}} ensemble martial Dead or Alive. It was directed by Corey Yuen and written by J. F. Lawton, Adam Gross, and Seth Gross.
 Christie Allen Kasumi (Devon Ayane (Natassia Malthe) and Helena Douglas (Sarah Carter).

==Plot== shinobi ninja-princess looking for her brother Hayate (who was competing in the last tournament), Tina, a professional wrestler setting out to prove she has more potential (complicated by her father Bass  being one of the contenders), Christie, a master thief and assassin, her treacherous partner Maximillian ‘Max’ Marsh, and Hayabusa, a friend of Kasumi and Hayate who follows Kasumi to keep her safe, using the invitation to DOA for this. A final competitor is Helena Douglas, daughter of the tournament’s late founder. When they arrive, they are monitored by the island’s supervisor, Dr. Victor Donovan, who, aided by egghead Weatherby, is gathering data (using injected nano-sensors) from the fights for some mysterious project. To add to the situation, an assassin from Kasumis colony named Ayane, has followed Kasumi to kill her and wipe away the disgrace the princess has caused to the clan.

The contest plays out, with multiple contestants fighting and being defeated (including Gen Fu, Bayman, Leon, and Zack), until only Kasumi, Christie, Hayabusa and Tina are left, with Helena being defeated by Christie. During the course of the film, Max and Christie form a plan to steal the fortune stowed away inside a hidden vault. During her fight, Christie sees that the key to finding and unlocking the vault is a tattoo on the back of Helena’s neck. Meanwhile, Kasumi begins to suspect Donovan of lying about her brother being killed in the previous tournament, and Hayabusa, infiltrating the main facility to find the truth, is captured. She is more than once confronted and nearly killed by Ayane, who Kasumi tries to convince that Hayate is alive (since it is eventually clear that Ayane loves Hayate). Also, Weatherby begins to fall for Helena, and in the end tells her about what he knows of the mystery project, and that before Helena’s father could shut the project down, he died (indicating that he was murdered).

On the final day of the tournament, wondering where Hayabusa is, Kasumi, Christie and Tina look for him and discover a secret entrance to the main complex, where they find Hayabusa unconscious. They are then gassed and captured. Meanwhile, Helena resolves to stop the mystery project, and has to fight the armed staff of the island, sent to kill her and Weatherby by Donovan. They are followed inside by Max, who finds his way to the vault, and is then knocked out by Bayman, who is working for Donovan. Inside the main complex, Donovan shows the four semifinalists the project he has been developing; an advanced form of neural interface that allows him and others to use the fighters combined skills to become the ultimate fighter. After ‘downloading’ the data into the device (shaped like a pair of sunglasses), he then shows that he kept Hayate alive and in peak condition to test the technology. He challenges Hayate to fight and win, if the others are to survive. Hayate accepts and is defeated, then thrown through a wall to die. Hayate is saved by Ayane, and the two of them apparently accept each other.

With the successful demonstration, Donovan prepares to sell the technology around the world, and begins downloading it (sic) to the watching buyers. Weatherby stops the broadcast and alerts the CIA, which provokes Donovan to head for them. Helena keeps Donovan back while Weatherby frees the others, but both are defeated and Donovan activates a self-destruct sequence which will obliterate the base. Kasumi, Helena, Christie, Tina, Ayane, and Hayate launch a combined attack on Donovan, while Hayabusa and Weatherby find Max and escape with him, despite Maxs urge to go back for the money. During the battle with the fighters, Donovan’s ‘glasses’ are knocked off and he is easily paralyzed by Hayate and Kasumi. The fighters then all escape as the base explodes and Donovan is consumed by the flames, making their escape by a hijacked pirates boat.

In the final scene, Helena, Ayane, Christie, Tina, and Kasumi are shown together again preparing to fight an army of ninja in Kasumis palace.

==Cast==
  Kasumi
* Jaime Pressly as Tina Armstrong Christie
* Sarah Carter as Helena Douglas Ayane
* Kane Kosugi as Ryu Hayabusa
* Eric Roberts as Dr. Victor Donovan
* Matthew Marsden as Maximillian Marsh Steve Howey as Weatherby Hayate
* Kevin Nash as Bass Armstrong Zack
* Derek Boyer as Bayman Leon
* Song Lin as Brad Wong
* Fang Liu as Gen Fu Hitomi
* Ying Wang as Leifang
 

==Production==
Principal photography commenced on May 4, 2005, and concluded on July 19, 2005. Filming locations include Bangkok, Guilin, Hengdian, and Hong Kong. The production budget was estimated to be $21 million.   

==Release== Canadian provinces gave it a PG except for Quebec which gave it a G with warning, and the Motion Picture Association of America (MPAA) PG-13. The film was released in North America on June 15, 2007, without press screenings. 

===Box office===
In non-North American markets, the film was relatively successful, making over $6,000,000 at the international box office, with almost $1 million in both the UK and Australia. In the USA though, the film was a box-office bomb, making only $260,000 its first week. It was released into 505 theaters, and spent 21 days in theaters closing on July 5 with a domestic gross of $480,813 (or about 7.2% of worldwide gross as of 7/5/07). The film grossed a total of $7,516,532, making it a box office bomb.   

===Critical response===
 
Reviews were generally negative. Rotten Tomatoes gives a rating of 34% based on 44 reviews.  At Metacritic, the film scored 38% based on reviews from 8 critics. 

L.A. Weekly gives the film a favorable review comparing it to Charlies Angels (film)|Charlies Angels and praising the director for providing "one of the year’s purest entertainments" and "pretty much nonstop fighting ...  with the flair you expect from a master choreographer like Yuen. It’s awesome."  Michael Ferraro from Film Threat credited the film with the unusual accomplishment of following the plot of the game series it is based on, even including a volleyball sequence, although he is critical there is not all that much plot. He described the acting and dialog as atrocious, and is bored by the action and fighting but calls the end result hilarious, and suggested it as the kind of film you might watch with a group of drunken friends, adding your own commentary track.  Joe Leydon of Variety (magazine)|Variety called it a "whirring blur of hot babes and cool fights" but complained the film is insubstantial and not enough even to appeal to genre fans with lowered expectations, suggesting it belongs in the video store and on late night television. He described the film as "a caffeinated mash-up" of Enter the Dragon, Mortal Kombat, Charlies Angels and a few other films, but did praise Yuen for his show-stopping spectacles and compared an outdoor sword fight saying it "looks like a collaboration between Gene Kelly and Bruce Lee".   

===Home media===
 
The DVD was released on September 11, 2007. In the first week, 68,578 DVD units were sold. The film opened in twelfth place, earning $1,370,874. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 