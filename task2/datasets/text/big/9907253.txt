Carmen (1983 film)
{{Infobox film
| name           = Carmen
| image          = Carmen by Saura.jpg
| caption        = Spanish film poster
| director       = Carlos Saura
| producer       = 
| writer         = 
| narrator       = 
| starring       = Laura del Sol
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}} Bodas de El amor brujo.

The films basic plot line is that the modern dancers re-enact in their personal lives Bizets tragic love affair, up to its lethal climax.

==Cast==
* Paco de Lucía as Paco
* Laura del Sol as Carmen
* Antonio Gades as Antonio   Marisol as Pepa Flores 
* Cristina Hoyos as Cristina 
* Juan Antonio Jiménez as Juan 
* José Yepes as Pepe Girón 
* Sebastián Moreno as Escamillo 
* Gómez de Jerez as Singer 
* Manolo Sevilla as Singer 
* Antonio Solera as Singer  
* Manuel Rodríguez as Guitarist 
* Lorenzo Virseda as Guitarist 
* M. Magdalena as Guest artist 
* La Bronce as Guest artist

==Awards==
The film won the BAFTA Award for Best Foreign Language Film. It was entered into the 1983 Cannes Film Festival where it won the Technical Grand Prize and the award for Best Artistic Contribution.    It was nominated for the Academy Award for Best Foreign Language Film.   

==See also==
* List of submissions to the 56th Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 