Ninja: Shadow of a Tear
{{Infobox film
| name           = Ninja: Shadow of a Tear
| image          = Ninja II - Shadow of a Tear.jpg
| border         = yes
| caption        = Official poster
| director       = Isaac Florentine
| producer       =  
| writer         = Boaz Davidson (as David White)
| starring       =  
| music          =  
| cinematography = Ross W. Clarkson
| editing        = Irit Raz
| studio         =  
| distributor    = Millennium Films
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English Japanese
| budget         = 
| gross          = 
}}  martial arts / action thriller film directed by Isaac Florentine and starring Scott Adkins, Kane Kosugi, Mika Hijii and Shun Sugata. It is the sequel to the 2009 film Ninja (film)|Ninja. The film was shot in Bangkok, Thailand,    and it made its premiere at the 2013 Fantastic Fest in Austin, Texas.    It was released for download through iTunes on December 17, 2013  and for Blu-ray Disc and DVD on December 31. 

==Plot==
American martial artist Casey Bowman has settled down at the Kōga-ryū|Kōga ninja dojo and married Namiko Takeda, who is pregnant with their first child. One day, while shopping for a pendant in town, he encounters and fends off against two knife-wielding muggers. Later that night, Casey goes grocery shopping, but when he remembers that the muggers took his wallet, he rushes home only to find Namiko slain, with markings of a barbed wire weapon around her neck. On the day of the funeral, the dojo is visited by a former student named Nakabara, who offers Casey to train at his dojo in Thailand to ease his pain, but Casey declines the offer. Remembering the fighting style of the muggers he encountered, Casey heads to the Azuma dojo to know their whereabouts before ambushing and killing them in a dark alley.
 fire walk practice, but Casey stops halfway through the walk due to memories of Namiko lingering in his mind. He goes on a drinking binge at a nearby bar and gets into a fight with several drunk guests. The next morning, Lucas is killed by the same barbed wire weapon used on Namiko. Nakabara reveals to Casey that his father and Sensei Takeda (Namikos father), along with a man from Nagoya named Isamu, were the three top students of the Kōga dojo. When their sensei died, Isamu challenged Takeda for control of the dojo; Takeda killed Isamu in the fight and continued as sōke. Isamus younger brother Goro witnessed the fight and swore to avenge his death, even if it took three generations. Years later, Goro became head of one of the largest drug cartels in Burma|Myanmar. Nakabara urges Casey to return to the U.S., as being Takedas son-in-law has made him a target of Goro. Casey instead asks Nakabara to help him find Goro. Nakabara gives him an old map of Burma from his fathers days in World War II, with markings that indicate locations of ninja weapons.

Casey heads to Myanmar, where he befriends a cab driver named Mike. Later that night, he enters a bar and fights a group of drug dealers. He returns to his hotel room to rest, only to find himself arrested by the State Peace and Development Council|SPDC, who accuse him of being an American spy and torture him. He escapes from his cell and extracts information on Goros whereabouts from SPDC General Sung before Mike drives him to the jungle. There, Casey finds the cemetery of Japanese soldiers and arms himself with a boxful of ninja weapons buried under a wooden gravemarker with the Nakabara clan symbol. He sneaks into Goros hideout, setting the complex on fire before facing Goros right-hand man Myat. The fight ends with Casey stabbing Myat in the heart and breaking his neck. He then squares off against Goro before slashing him in the midsection. In the middle of the fight, Goro wraps his barbed wire Kusari-fundo|manriki around Caseys neck, but Casey uses his strength to free himself and throw Goro to the ground before decapitating him.

Casey returns to Nakabaras dojo, only to discover that Nakabara was the one who murdered Namiko and Lucas. Nakabara is revealed to be a drug lord himself, and he used Casey to wipe out Goros cartel to monopolize the Southeast Asian drug trade. He then gives Casey the choice to either join him or die. Both men engage in an intense fight until Casey kicks Nakabara through a thin wall, revealing a room full of ancient artifacts. They continue the fight in the room with weapons, with Casey slashing Nakabara in the midsection and Nakabara impaling Caseys left shoulder with his katana. Nakabara lunges toward Casey, but Casey grabs a manriki and wraps it around Nakabaras neck for the kill. Later, Casey reveals Nakabaras true motives to Toji, one of the dojos students. Knowing that such actions would ruin the dojos reputation, Toji states the incident never happened and bids Casey farewell. Casey returns to Japan and drops Namikos pendant in a pond, bringing closure to his loss.

==Cast==
*Scott Adkins as Casey Bowman
*Kane Kosugi as Nakabara
*Mika Hijii as Namiko Takeda
*Shun Sugata as Goro
*Vithaya Pansringarm as General Sung
*Mukesh Bhatt as Mike
*Jawed El Berni as Lucas
*Futoshi Hashimoto as Toji
*Tim Man as Myat
*Takato as Hiroshi
*Ron Smoorenburg as a dojo fighter

==Production==
On November 2012, Nu Image and Millennium Films announced that Ninja 2 is in production, with Isaac Florentine returning to the directors chair. Scott Adkins and Mika Hijii have been confirmed to reprise their roles from the first film. Kane Kosugi confirmed on his website that he will play the new antagonist in the sequel. Fight choreographer Akihiro "Yuji" Noguchi is replaced by Chinese-Swedish martial artist Tim Man (Kill Bill). 

Filming completed in Bangkok, Thailand, on February 2013 and the film is currently undergoing post-production before a slated release later this year.  On July 2013, Adkins revealed on his Facebook page that the film has officially been titled Ninja: Shadow of a Tear. 

==Reception==
Fred Topel of CraveOnline gave the film a rating of 7.5 out of 10, citing that it is "loaded with fights: really awesome work whether Casey is sparring, picking fights in a rage or actually going up against bad guys. Hollywood movies don’t give you this much value." 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 