Violette Nozière
{{Infobox film name            = Violette
 image           = Violette Nozière-poster.jpg caption         = French poster director        = Claude Chabrol producer        = Eugène Lepicier Denis Héroux writer          = Odile Barski Hervé Bromberger Frédéric Grendel starring        = Isabelle Huppert Stéphane Audran Jean Carmet Jean-François Garreaud Guy Hoffman Lisa Langlois music           = Pierre Jansen cinematography  = Jean Rabier editing         = Yves Langlois distributor     = Koch-Lorber Films | released        = Cannes Film Festival: 20 May 1978 France: 24 May 1978 United States: 7 October 1978 runtime         = 124 minutes country         = France Canada language        = French budget          = $CAD1,360,000
|}}
Violette Nozière is a 1978 French crime film directed by Claude Chabrol and starring Isabelle Huppert and Stéphane Audran. The film, based on a true French murder case in 1933,    is about an eighteen-year-old girl named Violette and her encounters with a number of older men. The film had a total of 1,074,507 admissions in France.   

==Plot==
Violette Nozière (Isabelle Huppert) is a French teen in the 1930s who secretly works as a prostitute while living with her unsuspecting parents, father Baptiste Nozière (Jean Carmet) and mother Germaine Nozière (Stéphane Audran).  Rebelling against her "mean and petty" petit-bourgeois parents, she falls in love with a spendthrift young man, whom she virtually supports with thefts from her parents as well as her prostitution earnings.  

Meanwhile, her parents are informed by Violettes doctor that she has syphilis. Violette manages to half-persuade her suspicious mother and indulgent father that she has somehow inherited the disease from them. On this pretext, she tricks them into taking "medicine" that is actually poison, killing her father; her mother, however, survives, and Violette is arrested and charged with murder.  She defends herself by alleging that her father had molested her; Chabrols abrupt use of flashbacks makes it uncertain whether Violette is simply lying or telling a half-truth.  She is convicted of murder and sentenced to die by guillotine, but a voiceover at the end tells us that her sentence was commuted by degrees to the point that she ultimately left prison, married, and had five children.

==Cast==
*Isabelle Huppert as Violette Nozière
*Jean Carmet as Baptiste Nozière
*Stéphane Audran as Germaine Nozière
*Jean-Francois Garreaud as Jean Dabin
*Zoe Chauveau as Zoe the Maid
*Jean-Pierre Coffe as Dr. Deron
*Jean Dalmain as Mr. Emile
*Guy Hoffman as the Judge
*Henri-Jacques Huet as Commissioner Guilleaume
*Bernadette Lafont as Violettes Cellmate
*Bernard Lajarrige as Andre De Pinguet
*Lisa Langlois as Maddy
*Fabrice Luchini as Camus
*Dominique Zardi as Boy in Cafe

==Awards and nominations== Best Actress.    At the César Awards 1979|César Awards, Stéphane Audran was awarded Best Supporting Actress. The film was also nominated in three other categories: Best Actress (Isabelle Huppert), Best Music (Pierre Jansen) and Best Production Design (Jacques Brizzio).

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 