The Ragged Edge (film)
{{Infobox film
| name           = The Ragged Edge
| image          = HaroldMacgrath-RaggedEdge.jpg
| caption        = open leaf of the novel with illustration 
| director       = F. Harmon Weight
| producer       = Distinctive Pictures(founded by George Arliss)
| writer         =  (adaptation)
| starring       = Alfred Lunt Mona Palma
| music          =
| cinematography =?none listed but probably Harry Fischbeck from Lunts first film Backbone (1923 film)|Backbone
| editing        = Cosmopolitan Productions
| released       =  
| runtime        = 7 reels;6,800 feet
| country        = United States
| language       = Silent (English intertitles)
}} Goldwyn and Cosmopolitan productions. It was directed by F. Harmon Weight and starred Alfred Lunt.  

==Cast==
*Alfred Lunt - Howard Spurlock
*Mimi Palmeri - Ruth Endicott (*this actress also called Mona Palma)
*Charles Fang - Ah Cum
*Wallace Erskine - The doctor
*George MacQuarrie - McClintock
*Charles Slattery - OHiggins
*Christian Frank - The Wastrel
*Grace Griswold - Prudence Jedson
*Alice May - Angelina Jedson
*Percy Carr - Hotel Manager
*Marie Day - The Aunt Charles Kent - Reverend Luther Enschede
*Hattie Delaro - Mrs. Dalby

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 
 