Blue Puppy
{{Infobox film
| name           = Blue Puppy
| image          = 
| image_size     = 
| caption        = 
| director       = Yefim Gamburg
| producer       = 
| writer         = Yuri Entin
| narrator       =  Andrei Mironov
| music          = Gennadi Gladkov 
| cinematography = Mikhail Druyan
| editing        = 
| distributor    = Soyuzmultfilm
| released       = November 28, 1976
| runtime        = 19 min 09 s
| country        = Soviet Union
| language       = Russian English
| budget         = 
| gross          = 
}} Soviet animated film. The lyrics and screenplay were written by Yuri Entin, loosely based on the tale by Gyula Urban.

Several quotes from the films lyrics became common colloquial sayings, for example, "What trouble can we stir up?" and "Ah, how Im angry, ooh, how Im angry!" voiced by Mikhail Boyarsky, and the phrase "One ought to live skilfully."

The film was unique in utilizing a non-traditional animation technique where spots of colorful India ink were used to form the characters. This gave a great deal of plasticity to the animated characters. For example, the Black Cat would vanish from the foreground only to reappear immediately in the back, or the evil Pirate would transform into a thundercloud by inflating himself with his own malice.

== Plot summary ==
 
 Black Cat, a dodger and a faker, is the only one that feigns interest. Unexpectedly, the Pirate attacks the island and using the Black Cat, kidnaps the Blue Puppy.

Along comes the Good Sailor, who always helps ones in trouble and does good things. As the Sailor sails the sea, the Black Cat tells him about the abduction of the Puppy. The Sailor decides to rescue the puppy and begins chasing the Pirate; the Pirate eventually attacks the Seaman with a sawfish which sinks the Sailors ship. The Pirate ties the Seaman and the Puppy together with shackles, but the Puppy manages to free both himself and the Sailor.

Meanwhile, the Pirate and the Black Cat are celebrating their victory and sing a song about their spurious friendship. Their celebration is cut short, however, as the Seaman confronts and defeats the Pirate. He and the Puppy return to the island as heroes. Now, nobody tries to avoid the Puppy. More importantly, however, the Puppy now has a real friend. The moral of the story is that it is not so awful to be different from others, as long as one has friends, and the respect of ones peers.

== Publications == records and, later, on a CD. The soundtrack includes not only the film songs, but also the plot narration in rhyme. Some singles from the soundtrack are included in special children audio-collections.

At the end of the 1990s, the tale was reborn on the theater stage (theatre)|stage. Plays loosely based on the film were staged at the St. Petersburg Marionette Theater and at the Moscow Chamber Music Puppet Theater. A musical version has also appeared at a Krasnoyarsk night club.

A computer video-game Birthday of the Blue Puppy was released by Fargus.

== Ambiguous meaning of the tale ==
In the early 1990s, Yuri Entin was under constant pressure over his film. This is because the word goluboy (blue) is the modern Russian slang term for homosexual orientation|gay. This misconception was helped along by some of the quotes from the songs in the movie, for example, when the Blue Puppy is teased by the other dogs: "You are blue!  You are blue!  We dont want to play with you!" or in the subsequent song by the Puppy, which has the line, "I am hurt by an evil fate, oh, why am I blue?"

However, the additional connotation to the Russian word "blue" is a recent phenomenon, and writer Yuri Entin has pointed out that he never thought of such an association in 1976, when the film was made.

In the original work of the Hungarian writer, Gyula Urban, the Puppy was black, but in the Soviet tale, Entin wanted the Puppy to be a non-traditional color.

== Voice cast ==
* Mikhail Boyarsky as a Pirate
* Alisa Freindlich as Blue Puppy
* Alexander Gradsky as a Sailor and the Sawfish Andrei Mironov as a Black Cat

== External links ==
* 
*  at Animator.ru

 

 
 
 
 
 
 
 