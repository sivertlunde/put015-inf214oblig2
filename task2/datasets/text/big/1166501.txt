Mifune's Last Song
{{Infobox film
| alt            =
| image	=	Mifunes Last Song FilmPoster.jpeg
| caption        =
| director       = Søren Kragh-Jacobsen
| producer       = Birgitte Hald Morten Kaufmann
| writer         = Søren Kragh-Jacobsen Anders Thomas Jensen
| starring       =
| music          = Thor Backhausen Karl Bille Christian Sievert
| cinematography = Anthony Dod Mantle
| editing        = Valdís Óskarsdóttir
| studio         =
| distributor    =
| released       =  
| runtime        = 98 minutes
| country        = Denmark Sweden
| language       = Danish
| budget         =
| gross          =
}}
Mifunes Last Song ( ; in   group rules. It was directed by Søren Kragh-Jacobsen. The film was a great success in Denmark and an international blockbuster, ranked among the ten best-selling Danish films worldwide.
It was produced by Nimbus Film.
 Silver Bear - Special Jury Prize and Iben Hjejle won an Honourable Mention.   

==Plot==
Kresten had moved from his parents farm on Lolland, an out-of-the-way small Danish island, to Copenhagen to pursue his working career. When his father dies, he has to move back to the farm, where nothing much has happened since he left. He places an ad in the local newspaper to get help running the farm and taking care of his retarded brother. The prostitute Liva, who is running away from harassing telephone calls, takes the job. But running away from ones past isnt easy.

==Cast== 
*Iben Hjejle
*Anders W. Berthelsen
*Jesper Asholt
*Emil Tarding
*Anders Hove
*Sofie Gråbøl
*Paprika Steen
*Susanne Storm
*Ellen Hillingsø
*Sidse Babett Knudsen
*Søren Fauli
*Søren Malling
*Kjeld Nørgaard
*Kirsten Vaupel
*Torben Jensen
*Klaus Bondam
*Sofie Stougaard

==Confession==
The "confession" is an idea adapted by Thomas Vinterberg in the first Dogme 95 film: Make a confession if elements of the film do not comply with the strict interpretation of the Dogme-rules. It is written from the directors point of view.

 "As one of the DOGME 95 brethren and co-signatory of the Vow of Chastity I feel moved to confess to the following transgressions of the aforesaid Vow during the production of Dogme 3 - Mifune. Please note that the film has been approved as a Dogme work, as only one genuine breach of the rules has actually taken place. The rest may be regarded as moral breaches."  

* I confess to having made one take with a black drape covering a window. This is not only the addition of a property, but must also be regarded as a kind of lighting arrangement.
* I confess to moving furniture and fittings around the house.
* I confess to having taken with me a number of albums of my favourite comic book series as a youth, Linda & Valentin (Valérian and Laureline).
* I confess to helping to chase the neighbours free-range hens across our location and including them in the film.
* I confess that I brought a photographic image from an old lady from the area and hung it in a prominent position in one scene: not as part of the plot, but more as a selfish, spontaneous, pleasureable whim.
* I confess to borrowing a hydraulic platform from a painter, which we used for the only two birds-eye overview shots in the film.
* I do solemnly declare that in my presence the remainder of Dogme 3 - Mifune was produced in accordance with the vow of chastity.
* I also point out that the film has been approved by DOGME 95 as a Dogme film, as in real terms no more than a single breach of the rules has been committed. The rest may be regarded as moral transgressions. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 