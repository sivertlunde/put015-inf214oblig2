Predator (film)
{{Infobox film
| name                 = Predator
| image                = Predator Movie.jpg
| caption              = Theatrical release poster
| director             = John McTiernan
| producer             = {{Plain list| Lawrence Gordon
* Joel Silver John Davis
}}
| writer               = {{Plain list| Jim Thomas John Thomas
}}
| starring             = {{Plain list|
* Arnold Schwarzenegger
* Carl Weathers
}}
| music                = Alan Silvestri
| cinematography       = Donald McAlpine
| editing              = {{plainlist| Mark Helfrich
* John F. Link
}}
| production companies = {{plainlist|
* Lawrence Gordon Productions
* Silver Pictures
* Davis Entertainment
}}
| distributor          = 20th Century Fox
| released             =  
| runtime              = 107 minutes 
| country              = United States
| language             = English
| budget               = $15–18 million      
| gross                = $98.3 million  
}} science fiction titular antagonist, Jim and John Thomas in 1985, under the working title of Hunter. Filming began in April 1986 and creature effects were devised by Stan Winston.

The films budget was around $15 million. Released in the   (2007), have been produced. Another entry in the series directed by Shane Black is in the works at 20th Century Fox.

==Plot==
 , Shane Black|Black, Arnold Schwarzenegger|Schwarzenegger, Bill Duke|Duke, Carl Weathers|Weathers, Sonny Landham|Landham, and Richard Chaves|Chaves.]]
 CIA with Val Verde. Agent George Dillon, an old friend of Schaefers, is assigned to supervise. The team is inserted into a remote jungle and begins the mission.
 invisible entity thermal imaging.

Hawkins chases a fleeing Anna when they are both suddenly confronted by the creature. The latter is spared, but the former is swiftly killed and dragged away. Schaefer organizes a manhunt for the killer, during which Cooper is killed with a plasma cannon. An ensuing firefight fails to draw out the creature, so the unit regroups and questions Anna, learning that their stalker is a non-human hunter who has been killing humans for decades as sport. An attempt to entrap the creature fails, leaving Poncho badly wounded. Mac and Dillon attempt an ambush on the Predator, but Mac is ambushed first by the Predator and is killed off by its plasma cannon. Dillon sees it in the trees and fires at it but the Predator shoots first taking his arm off. It then impales him with the blades on its forearm.

The survivors try to escape, but the Predator catches up, killing Billy, who stays behind to battle the alien, blasts the injured Poncho in the head, and wounding Schaefer. Realizing the creature targets only armed and hostile prey, Schaefer sends Anna to the chopper alone. He then jumps off a waterfall and uses mud as camouflage, as it blocks his bodys infrared radiation from the Predators thermal sensor. Seeking to avenge his men, Dutch uses his knowledge of jungle warfare to craft a series of traps. Covered in mud and armed with improvised weapons, he lures the Predator with a war cry.

Utilizing his preparations, Dutch beats the Predator at its own game, disabling its cloaking device and inflicting minor injuries. Nevertheless, the Predator tracks him down. Acknowledging Schaefer as a worthy foe, the Predator discards its equipment and challenges him to hand-to-hand combat, where it has the advantage. Dutch narrowly defeats the creature by using a counterweight to crush it. He demands that the Predator identify itself, but the alien merely repeats his question in broken English before activating a self-destruct device on its wrist and breaking into maniacal laughter, intending to take Dutch with it. Dutch flees and takes cover just before the self-destruct device explodes, killing the Predator (apparently a Nuclear weapon yield|low-yield  nuclear weapon, with mushroom cloud and nuclear electromagnetic pulse damaging the electronics of the rescue helicopter). Dutch, the last man standing, is picked up shortly afterwards by his commander, General Phillips, and Anna.

==Cast==
* Arnold Schwarzenegger as Major Alan "Dutch" Schaefer
* Carl Weathers as George Dillon
* Elpidia Carrillo as Anna
* Bill Duke as Mac Eliot
* Jesse Ventura as Blaine Cooper
* Sonny Landham as Billy Sole
* Richard Chaves as Jorge "Poncho" Ramirez
* Shane Black as Rick Hawkins
* R. G. Armstrong as Major General Homer Phillips The Predator / Helicopter Pilot
* Peter Cullen as The Predator (Voice, uncredited)
* Sven-Ole Thorsen as Soviet Military Adviser

==Production==

===Development=== Jim and John Thomas Lawrence Gordon as co-producer and John McTiernan was hired as director for his first studio film. New Zealand director Geoff Murphy was also considered to direct. 
 Region 1 release of the special edition, the original monster suit was vastly different from the final product, designed by Stan Winston. The original monster was a disproportionate, ungainly creature  with large yellow eyes and a dog-like head, and it was nowhere near as agile as later portrayed. McTiernan consulted Winston after production became troubled.  While on a plane ride to Fox studios alongside Aliens director James Cameron, Winston sketched monster ideas. Cameron suggested he had always wanted to see a creature with Mandible (arthropod)|mandibles, which became part of the Predators iconic look.

===Casting===
  as the Predator]]
Silver and Gordon first approached Arnold Schwarzenegger with the lead role.
 Stallone did that with Rocky. Predator was one of the scripts I read, and it bothered me in one way. It was just me and the alien. So we re-did the whole thing so that it was a team of commandos and then I liked the idea. I thought it would make a much more effective movie and be much more believable. I liked the idea of starting out with an action-adventure, but then coming in with some horror and science fiction." 
 John Davis, Navy UDT Jesse Ventura was hired for his formidable physique as Blain. Native Americans Sonny Landham and Richard Chaves, and African-American Bill Duke, who co-starred alongside Schwarzenegger in Commando, provided the ethnic balance. As a favor to writer Shane Black, whose first screenplay had become Silvers blockbuster Lethal Weapon a few months earlier, Silver hired Black to play a supporting role in the film, which also allowed him to watch McTiernan direct. 

Jean-Claude Van Damme was originally cast as the Predator creature, the idea being that the physical action star would use his martial arts skills to make the Predator an agile, ninja-esque hunter.  When compared to Schwarzenegger, Weathers, and Ventura, actors known for their bodybuilding regimens, it became apparent a more physically imposing man was needed to make the creature appear threatening. Additionally, it was reported that Van Damme constantly complained about the monster suit being too hot, causing him to pass out. He also had allegedly voiced his reservations on numerous occasions regarding the fact he would not be appearing on camera without the suit. Van Damme was removed from the film and replaced by Kevin Peter Hall. 

===Filming===
Commitments by Schwarzenegger delayed the start of filming by several months. The delay gave Silver enough time to secure a minor rewrite from screenwriter David Peoples. Principal photography eventually began in the jungles of Palenque, Mexico, near Villahermosa, Tabasco, during the second week of April 1986, but the film overall was filmed in Puerto Vallarta, Mexico. Much of the material dealing with the units deployment in the jungle was completed in a few short weeks and both Silver and Gordon were pleased by the dailies provided by McTiernan. On Friday, April 25, production halted so that Schwarzenegger could fly to Hyannis Port in a Lear jet chartered by Silver in order to get to his wedding on time. He was married on April 26, 1986, to Maria Shriver, and honeymooned for only three days while the second unit completed additional lensing. The production resumed filming on May 12.

Both director McTiernan and Schwarzenegger lost 25 pounds during the film.  Schwarzeneggers weight loss was a professional choice. McTiernan lost the weight because he avoided the food in Mexico due to health concerns.  Unlike McTiernan, most of the cast and crew suffered from travelers diarrhea since the Mexican hotel in which they were living was having problems with the water purification.  In an interview, Carl Weathers said the actors would secretly wake up as early as 3:00&nbsp;a.m. to work out before the days shooting. Weathers also stated that he would act as if his physique was naturally given to him, and would work out only after the other actors were nowhere to be seen. 

According to Schwarzenegger, filming was physically demanding as he had to swim in very cold water and spent three weeks covered in mud for the climactic battle with the alien.    In addition, cast and crew endured very cold temperatures in the Mexican jungle that required heat lamps to be on all of the time. Cast and crew filmed on rough terrain that, according to the actor, was never flat, "always on a hill. We stood all day long on a hill, one leg down, one leg up. It was terrible."  Schwarzenegger also faced the challenge of working with Kevin Peter Hall, who could not see in the Predator suit. The actor remembers, "so when hes supposed to slap me around and stay far from my face, all of a sudden, whap! There is this hand with claws on it!"  Hall stated in an interview that his experience on the film, "wasnt a movie, it was a survival story for all of us."    For example, in the scene where the Predator chases Dutch, the water was foul, stagnant and full of leeches.  Hall could not see out of the mask and had to rehearse his scenes with it off and then memorize where everything was. The outfit was difficult to wear because it was heavy and off-balance. 

The film was dedicated to the memories of Agustin Ytuarte and Federico Ysunza, who were both killed on March 31, 1986, in the crash of Mexicana Flight 940.

===Special effects=== Total Recall). The film was nominated for an Academy Award for Visual Effects. 

===Soundtrack===
{{Infobox album  
| Name        = Predator Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Alan Silvestri
| Cover       =
| Released    = August 19, 2003
| Recorded    =
| Genre       = Orchestral Film score
| Length      = 73:08
| Label       = Varèse Sarabande
| Producer    =
}} the sequel, making him the only composer to have scored more than one film in either the Alien (franchise)|Alien or Predator series.
 CD Club collection; the album also includes the Elliot Goldenthal arrangement of the Fox fanfare used on Alien 3.

# "Twentieth Century Fox Fanfare"   (0:27)
# "Main Title" (3:51)
# "Something Else" (3:34)
# "Cut Em Down" (1:56)
# "Payback Time" (2:09)
# "The Truck" (4:22)
# "Jungle Trek" (1:47)
# "The Girls Escape" (6:00)
# "Blaines Death" (2:47)
# "Hes My Friend" (1:26)
# "Were All Gonna Die" (3:32)
# "Building a Trap" (3:02)
# "The Waiting" (3:27)
# "The Hunt Is On" (4:51)
# "Dillon Is Disarmed" (2:07)
# "Billy Stands Alone" (2:34)
# "Battle Plans" (9:24)
# "Wounded Predator" (4:14)
# "Hand to Hand Combat" (3:12)
# "Predators Big Finish" (3:42)
# "The Rescue and End Credits" (4:44)

In 2010, the same year Predators (film)|Predators featured an adaptation of Silvestris score by John Debney,  Intrada Records released the album in a 3000-copy limited edition with remastered sound, many cues combined and renamed, and most notably (as with Intradas release of Basil Poledouriss score for RoboCop) presenting the original end credits music as recorded (the film versions are differently mixed). This release is notable for having sold out within a day. 

# "Fox Logo"   (0:26)
# "Main Title" (3:52)
# "Something Else; Cut Em Down; Payback Time" (7:37)
# "The Truck" (4:23)
# "Jungle Trek" (1:48)
# "Girls Escape; Blaines Death" (6:40)
# "What Happened?" (2:01)
# "Hes My Friend" (1:26)
# "Were Gonna Die" (3:29)
# "Building the Trap" (3:06)
# "The Waiting" (3:27)
# "Can You See Him?" (4:52)
# "Dillons Death" (2:05)
# "Billy and Predator" (2:32)
# "Dutch Builds Trap" (9:28)
# "Predator Injured; Hand to Hand Combat" (7:22)
# "Predators Death" (3:43)
# "The Pick-Up and End Credits" (5:58)

== Reception ==

=== Box office ===
Released on June 12, 1987, Predator was #1 at the US box office in its opening weekend with a gross of $12 million, which was second to only Beverly Hills Cop II for the calendar year 1987.  The film grossed $98,267,558, of which $59,735,548 was from the US & Canadian box office. $38,532,010 was made overseas. 

=== Critical response ===

 

Initial critical reaction to Predator was generally mixed. Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film an average score of 36 based on 11 reviews from 1987, with the review opinions summarized as "generally unfavorable".    Elvis Mitchell of The New York Times described it as "grisly and dull, with few surprises."  Dean Lamanna wrote in Cinefantastique that "the militarized monster movie tires under its own derivative weight."  Variety (magazine)|Variety wrote that the film was a "slightly above-average actioner that tries to compensate for tissue-thin-plot with ever-more-grisly death sequences and impressive special effects."  Michael Wilmington of the Los Angeles Times proclaimed it "arguably one of the emptiest, feeblest, most derivative scripts ever made as a major studio movie."  Feminist Susan Faludi called it one of "an endless stream of war and action movies" in which "women are reduced to mute and incidental characters or banished altogether."   Critic Roger Ebert who was more complimentary of the film, rating it three out of four stars and saying, "it supplies what it claims to supply: an effective action movie."   
 James Rolfe enjoyed the film, noting that it had the suspenseful tones of Alien (film)|Alien and also the action elements of Aliens (film)|Aliens.  The film has been a perennial cable favorite outside of America, in India and other countries.   }} 

==Legacy== 100 Years... Predator creature 100 Years... 100 Heroes and Villains.  In 2007, C. Robert Cargill of RealNetworks|Film.com ranked Predator as the seventh best film of 1987, calling it "one of the great science fiction horror films, often imitated, but never properly duplicated, not even by its own sequel."  Entertainment Weekly named it the 22nd greatest action movie of all time in 2007, and the 14th among "The Best Rock-em, Sock-em Movies of the Past 25 Years" in 2009, saying "Arnold Schwarzenegger has never been as manly as he was in this alien-hunting testosterone-fest."   IGN proclaimed it the 13th greatest action movie of all time.  In 2008, Empire (magazine)|Empire magazine ranked it 336th on their list of The 500 Greatest Movies of All Time.  Review aggregator Rotten Tomatoes gives the film a 77% "Fresh" rating based on 39 critical reviews and reports a ratings average of 6.8 out of 10. 
 NECA released action figure collectables of Major Alan "Dutch" Schaefer and the Predator, marketed as "Jungle Hunter Dutch Schaefer", "Jungle Disguise Dutch Schaefer", "Jungle Extraction Dutch Schaefer", "Jungle Patrol Dutch Schaefer", "Jungle Hunter Predator", "Water Emergence Predator", and "Cloaked Classic Predator." 
 converted into 3D for Blu-ray release. 
 John Davis. The film is expected to be written by Fred Dekker, with Shane Black writing a film treatment, as well as having the option to direct.   It was later specified by Black that the film will be a sequel to the previous Predator films. 

==See also==
*List of monster movies

 

== References ==
 

==External links==
 
* 
* 
* 
 
 

 
 
 
 
 
 
 
 
 
 
 
 