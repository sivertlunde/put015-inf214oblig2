La Noche de Walpurgis
 
{{Infobox Film
| name           = La Noche de Walpurgis
| image          = The Werewolf Vs. Vampire Woman.jpg
| caption        = Theatrical release poster
| director       = León Klimovsky
| producer       = Salvadore Romero
| writer         = Jacinto Molina, Hans Munkel
| starring       = Paul Naschy, Gaby Fuchs, Barbara Capell
| music          = Antón García Abril
| cinematography = Leo Williams
| editing        = Tony Grimm
| distributor    = Hispamex  (Spain) , Universal Entertainment Corp  (USA, theatrical) 
| released       =  
| runtime        = 86 min
| country        = Spain  West Germany Spanish
| budget         = 
}}

La Noche de Walpurgis (Walpurgis Night, released in the United States as The Werewolf vs. The Vampire Woman and in the UK as The Werewolfs Shadow) is a 1970 Spanish horror movie starring Paul Naschy, the fifth in a series about the werewolf Waldemar Daninsky.  This film was directed by León Klimovsky and written by Naschy and Hans Munkel, and is generally regarded to have kickstarted the Spanish horror film boom of the Seventies, due to its awesome box office success upon its release. Patty Shepard was so convincing as the vampiric Countess, it was thought at the time that she might replace actress Barbara Steele as Europes reigning horror queen. Klimovsky filmed many of the scenes in slow motion, to add to the otherworldliness of the film. 
Naschy followed up this film with a sequel entitled Dr. Jekyll and the Wolf Man. 
Note* - There is a scene in this film that obviously inspired Spanish director Amando de Ossorio to write Tombs of the Blind Dead, which was made just a few months later in 1971. A skeletal zombie in a monks garments assaults Naschy in a cemetery in one scene, bearing a strong resemblance to de Ossorios Templar Knights in his "Blind Dead" films.

== Plot ==
Following the events in Fury of the Wolfman, the deceased lycanthrope Waldemar Daninsky is revived to life when two doctors surgically remove two silver bullets from his heart while performing an autopsy on him. Waldemar transforms into a werewolf, kills the doctors and escapes from the morgue. Some time later, two students, Elvira and her friend Genevieve, go searching for the tomb of medieval murderess (and possible vampiress) Countess Wandessa de Nadasdy. They find a possible gravesite in the vicinity of Waldemar Daninskys castle, and he invites the girls to stay for a few days. 
When Waldemar leads them to the grave of the countess Wandessa, Elvira accidentally revives her by bleeding on the corpse. The vampire woman turns the girls into creatures of the night like herself, and they roam the forest at night, killing people in eerie slow motion. Daninsky later turns into the Wolf Man, is forced to battle and destroy the vampire woman at the end of the film, and then is himself destroyed by Elvira, a woman who loves him enough to end his torment. (An interesting note - Naschys real life wife of 40 years was named Elvira.) 

Daninskys lycanthropy is not given a specific origin in this film; the events of the film are assumed to have followed from the ending of Fury of the Wolf Man (1970), which involved a yetis bite as the cause of Daninskys curse. How Daninsky went from being a college professor in Fury to being a castle-owning count in "Walpurgis is never addressed. 
It is explained that Waldemar was raised in this castle from infancy by an old woman who cared for him after his real parents died. The local townspeople consider her a witch, and rumors abound about a werewolf living in the castle. Apparently after Waldemar escaped from the morgue in the beginning of the film, he returned to his castle & his stepmother. This backstory totally contradicts Waldemars being a college professor in the previous film, and this is why most Naschy fans dont even try to relate the Hombre Lobo films to each other storywise.  

== Cast ==
* Paul Naschy as Waldemar Daninsky
* Gaby Fuchs as Elvira
* Barbara Capell as Genevieve Bennett
* Andrés Resino as Inspector Marcel
* Yelena Samarina as Elizabeth Daninsky
* José Marco as Pierre
* Betsabé Ruiz as Pierres girl
* Barta Barri as Muller
* Luis Gaspar as Distraught man
* Ruperto Ares
* María Luisa Tovar as First female victim
* Julio Peña as Dr. Hartwig (coroner)
* Patty Shepard as Countess Wandesa Dárvula de Nadasdy
* Eduardo Chappa as Tramp / Monster

== Production == series of films to feature the werewolf Waldemar Daninsky. 

==Release==
The film was released theatrically in its native Spain as La Noche de Walpurgis in May 1971, and was released theatrically in the United States as The Werewolf vs the Vampire Woman by the Universal Entertainment Corporation in 1972.

=== Home media ===
The film was released on VHS in the 1980s as both Blood Moon and The Werewolf vs the Vampire Woman, and was later released on a special edition DVD in 2007 by Deimos Entertainment, a subdivision of BCI Eclipse, as Werewolf Shadow (with extras).

== References ==
{{Reflist |refs= 
 
{{cite book
 | last = Lazaro-Reboll
 | first = Antonio
 | title = Spanish Horror Film
 | url = http://books.google.com/books?id=Sw92cuklpNMC&pg=PA200&dq=%22La+Noche+de+Walpurgis%22+Naschy&hl=en&sa=X&ei=2Sk_UvK0OtOLqQHnvYCADA&ved=0CC0Q6AEwAw
 | accessdate = September 22, 2013
 | edition = illustrated
 | date = November 20, 2012
 | publisher = Edinburgh University Press
 | location = Edinburgh, Scotland, UK
 | isbn = 9780748636389
 | oclc = 806492157
 | page = 200
}}
 

}}

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 