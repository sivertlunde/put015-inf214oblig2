Lost in the Stratosphere
{{Infobox film
| name           = Lost in the Stratosphere
| image          = June Collyer and William Cagney in Lost in the Stratosphere (1934).jpg
| image_size     = thumb
| caption        = June Collyer and William Cagney, in "Lost in the Stratosphere" (1934)
| director       = Melville W. Brown
| producer       = William T. Lackey
| writer         = Albert DeMond (writer) Tristram Tupper (story)
| narrator       =
| starring       = William Cagney
| music          =
| cinematography = Ira H. Morgan
| editing        = Carl Pierson
| distributor    = Monogram Pictures
| released       = November 15, 1934
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Lost in the Stratosphere is a 1934 American film directed by Melville W. Brown and starring William Cagney, the lookalike brother of James Cagney.

== Plot summary ==
Two United States Army Air Corps|U.S. Army pilots, in the early days of military aviation, in the mid-1930s, in an era of open cockpits and biplanes, are always trying to do each other, in a friendly rivalry.

William Cagney (James Cagneys lookalike brother, in one of his few roles in front of the cameras), as 2nd Lt. Tom Cooper, gets the nickname "Soapy", from his friend, 1st Lt. Richard "Dick" Wood, "Woody". 

Tom finds out that "Ida Johnson", the girl hes been seeing, while Dicks been off the base, is really Evelyn Worthington, June Collyer, Dicks fiancée, who introduced herself, using her maids name, played by Hattie McDaniel,  .

But, its no joke, when Dick finds the tell-tale bar of soap.

The two pilots are picked to go on a dangerous mission into the stratosphere, with a balloon, to test high altitude flight; but, the friendship, and the engagement are off, before they get off the ground.

The Generals keep reminding them that the equipment is more important than they are.

It doesnt look like either of them will survive; until, Evelyn begs them, to save the love of her life and his friend, by ordering them to bail out. 

Unfortunately, the two are "lost in the stratosphere", when a thunderstorm takes them thousands of miles off course.

== Cast ==
*William Cagney Lt. Tom Soapy Cooper
*Edward J. Nugent as Lt. Richard Woody Wood
*June Collyer as Evelyn Worthington
*Edmund Breese as Col. Brooks John Mack as Sgt. Baker
*Russ Clark as Sgt. Enfield
*Matt McHugh as Matt OToole
*June Gittelson as Gretchen
*Lona Andre as Sophie
*Hattie McDaniel as Ida Johnson

== Home Media ==
The film was released on DVD on 23 August 2005.

The film is in the Public Domain; so, is also available on-line, including to download, for free, at Internet Archive.

==References==
 

== External links ==
 
* 
* 

 
 
 
 
 
 
 


 