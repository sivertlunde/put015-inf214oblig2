Crimes and Misdemeanors
{{Infobox film
| name           = Crimes and Misdemeanors
| image          = Crimes_and_misdemeanors2.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Woody Allen
| producer       = Robert Greenhut
| writer         = Woody Allen
| starring       = Caroline Aaron Alan Alda Woody Allen Claire Bloom Mia Farrow Joanna Gleason Anjelica Huston Martin Landau Jenny Nichols Jerry Orbach Sam Waterston
| music          = Franz Schubert
| cinematography = Sven Nykvist
| editing        = Susan E. Morse
| distributor    = Orion Pictures
| released       =  
| runtime        = 104 minutes   
| country        = United States
| language       = English
| budget         = $19 million
| gross          = $18,254,702 
}}
Crimes and Misdemeanors is a 1989 existential drama written, directed by and co-starring Woody Allen, alongside Martin Landau, Mia Farrow, Anjelica Huston, Jerry Orbach, Alan Alda, Sam Waterston and Joanna Gleason.
 Best Actor Best Writing, Screenplay Written Directly for the Screen.

==Plot==
The story follows two main characters: Judah Rosenthal, a successful ophthalmologist, and Clifford Stern, a small-time filmmaker.

Judah, a respectable family man, is having an affair with flight attendant Dolores Paley. After it becomes clear to her that Judah will not end his marriage, Dolores, scorned, threatens to inform his wife of their affair. Doloress letter to Miriam is intercepted and destroyed by Judah, but she sustains the pressure on him with her threats of revelation. She is also aware of some questionable financial deals Judah has made, which adds to his stress. He confides in a patient, Ben, a rabbi who is rapidly losing his eyesight. Ben advises openness and honesty between Judah and his wife, but Judah does not wish to imperil his marriage. Desperate, Judah turns to his brother, Jack, who hires a hitman to kill Dolores. Before her corpse is discovered, Judah retrieves letters and other items from her apartment (where he sees her bloody corpse) in order to cover his tracks. Stricken with guilt, Judah turns to the religious teachings he had rejected, believing for the first time that a just God is watching him and passing judgment.

Cliff, meanwhile, has been hired by his pompous brother-in-law, Lester, a successful television producer, to make a documentary celebrating Lesters life and work. Cliff grows to despise him. While filming and mocking the subject, Cliff falls in love with Lesters associate producer, Halley Reed (Mia Farrow).
 Martin S. Bergmann ), a renowned philosopher. He makes sure Halley is aware that he is shooting Lesters documentary merely for the money so he can finish his more meaningful project with Levy.

Cliffs dislike for Lester becomes evident during the first screening of the film. It juxtaposes footage of Lester with clownish poses of Benito Mussolini addressing a throng of supporters from a balcony. It also depicts Lester yelling at his employees and clumsily making a pass at an attractive young actress.

Cliff learns that Professor Levy, whom he had been profiling on the strength of his celebration of life, has committed suicide, leaving a curt note, "Ive gone out the window." When Halley visits to comfort him, he makes a pass at her, which she gently rebuffs, telling him she isnt ready for romance in her life.
 plagiarized from James Joyce.

In the final scene, Judah and Cliff meet by happenstance at the wedding of the daughter of rabbi Ben, who is Cliffs brother-in-law and Judahs patient. Once deeply anguished by the murder he arranged, Judah has worked through his guilt and is enjoying life once more; the murder had been blamed on a drifter with a record. He draws Cliff into a supposedly hypothetical discussion that draws upon his moral quandary. Judah says that with time, any crisis will pass; but Cliff morosely claims instead that one is forever fated to bear ones burdens for "crimes and misdemeanors."

==Cast==
* Martin Landau as Judah Rosenthal
* Woody Allen as Cliff Stern
* Mia Farrow as Halley Reed
* Anjelica Huston as Dolores Paley
* Alan Alda as Lester
* Jerry Orbach as Jack Rosenthal
* Joanna Gleason as Wendy Stern
* Claire Bloom as Miriam Rosenthal
* Sam Waterston as Ben
* Caroline Aaron as Barbara
* Victor Argo as Police Detective
* Daryl Hannah (uncredited) as Lisa Crosley
* Mercedes Ruehl (uncredited) as Party Guest

==Production==
After viewing the first cut of the film, Allen decided to throw out the first act, call back actors for reshoots, and focus on what turned out to be the central story.    

===Music=== classical and String Quartet No. 15 (a recording by the Juilliard Quartet), which is used in the scenes leading up to Dolores death, and Judah discovering her body.

===Influences=== moral dilemma - whether a person can continue with his everyday life with knowledge of having committed a murder - evokes   the pivotal idea of Crime and Punishment by Fyodor Dostoyevsky (and provides a resolution opposite to one in the novel). The theme would be revisited by Allen in his films Match Point and Cassandras Dream. The character of both Judah and his gangster brother were said to be influenced by a Jewish medical student who attended NYU with one of Marshall Brickmans relatives.

==Reception==
===Box office===
The film grossed a domestic total of $18,254,702.   

===Critical response===
Crimes and Misdemeanors received positive reviews; it currently holds a 93% "fresh" rating on Rotten Tomatoes, and a 77/100 weighted average score on Metacritic, which translates to "generally favorable reviews". 

===Accolades===
In a poll of  ), and Best Supporting Actor (Martin Landau).  It also received three additional nominations for Best Director (Woody Allen), Best Supporting Actor (Jerry Orbach) and Best Supporting Actress (Anjelica Huston).  In October 2013, the film was voted by the Guardian (newspaper)|Guardian readers as the third best film directed by Woody Allen. 

==Release==

===Home media=== Twilight Time on February 11, 2014. 

==References==
 

==Further reading==
*  

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 