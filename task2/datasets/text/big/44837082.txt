Adavilo Abhimanyudu
{{Infobox film
| name           = Adavilo Abhimanyudu
| image          =
| caption        =
| writer         = Satyanand  
| story          = Ashok
| screenplay     = Anil
| producer       = M. Venkataratnam
| director       = Anil Aishwarya
| music          = K. V. Mahadevan
| cinematography = Jayanan Vincent
| editing        = K. Narayanan
| studio         = Pallavi Purna Pictures   
| released       =  
| runtime        = 112 minutes
| country        = India
| language       = Telugu
| budget         =
}}   Aishwarya in Malayalam movie Douthyam.   

==Plot==
The films story is based on the Indian Armys "One Man Commando Operation", undergone by Captain Abhimanyu (Jagapathi Babu), in order to find a missing military aircraft. The aircraft is presumed to be in a forest and carrying confidential military secret documents by Captain Suresh (Vinod Kumar).

==Cast==
*Jagapathi Babu as Captain Abhimanyu
*Vinod Kumar as Captain Suresh Aishwarya as Shanti Gummadi
*Ranganath Ranganath
*Babu Antony
*Sanjeevi as Captain Raju Ashok Kumar
*KK Sarma
*Kuali
*Prinyanka
*Kalpana Rai

==Soundtrack==
{{Infobox album
| Name        = Adavilo Abhimanyudu
| Tagline     = 
| Type        = Film
| Artist      = K. V. Mahadevan
| Cover       = 
| Released    = 1989
| Recorded    = 
| Genre       = Soundtrack
| Length      = 12:36
| Label       = AVM Audio
| Producer    = K. V. Mahadevan
| Reviews     =
| Last album  = Atta Mechina Alludu   (1989)
| This album  = Adavilo Abhimanyudu   (1989)
| Next album  = Sutradharulu   (1989)
}}
 Aachari Aatreya. The soundtrack was released by AVM Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Pachahani Pachhika SP Balu, Chitra
|4:08
|- 2
|Puttameedha Palapitta SP Balu, Chitra
|4:13
|- 3
|Dammu Okati Kotteyara Chitra
|4:15
|}

==References==
 

 
 
 
 
 

 