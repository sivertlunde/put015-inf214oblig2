Tyrannens fald
 
{{Infobox film
| name           = Tyrannens fald
| image          = Tyrannens fald.jpg
| caption        = Film poster
| director       = Jon Iversen Alice OFredericks
| producer       = Henning Karmark
| writer         = Svend Rindom
| starring       = Eyvind Johan-Svendsen
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Marie Ejlersen
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Tyrannens fald is a 1942 Danish family film directed by Jon Iversen and Alice OFredericks. 

==Cast==
* Eyvind Johan-Svendsen - Viktor Frandsen
* Karin Nellemose - Ida Frandsen
* Birthe Scherf - Karen Frandsen
* Anker Ørskov - Frederik Frandsen
* Ib Schønberg - Ove Frandsen
* Lily Weiding - Ulla Kryer
* Mathilde Nielsen - Hushjælpen Mads
* Carlo Wieth - Skovrider Axel Høegh
* Poul Reichhardt - Mogens Høegh
* Astrid Villaume - Frk. Anne Høegh / Søster
* Nicolai Neiiendam - Dr. Ulllerup
* Erik Voigt - Fru Danielsen
* Petrine Sonne - Fru Hansen
* Randi Michelsen - Frk. Beck Henry Nielsen - Stationsforstander
* Lis Løwert - Ullas veninde Else
* Kathe Hollesen - Ullas veninde Inge

==References==
 

==External links==
* 

 

 
 
 
 
 


 