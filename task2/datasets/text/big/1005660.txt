Mambo Italiano (film)
{{Infobox film
| name           = Mambo Italiano
| image          = Mambo italiano.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Émile Gaudreault
| producer       = Daniel Louis Denise Robert
| screenplay     = Steve Galluccio Émile Gaudreault
| based on       =   Luke Kirby Peter Miller Mary Walsh
| music          = FM Le Sieur
| cinematography = Serge Ladouceur
| editing        = Richard Comeau
| studio         = Cinémaginaire
| distributor    = Equinox Films   Samuel Goldwyn Films  
| released       =  
| runtime        = 88 minutes  
| country        = Canada
| language       = English Italian French
| budget         =
| gross          =  6,253,026 
}}

 

Mambo Italiano is a 2003 Canadian comedy-drama film directed by Émile Gaudreault. The screenplay was written by Gaudreault and Steve Galluccio, based on Galluccios theatrical play by the same name. Both the play and the film are based on Galluccios own life and experiences.

==Plot== Italian immigrants reveals he closet - Sicilian mother, Lina.

==Cast== Luke Kirby as Angelo Barberini
* Claudia Ferri as Anna Barberini Peter Miller as Nino Paventi
* Paul Sorvino as Gino Barberini
* Ginette Reno as Maria Barberini Mary Walsh as Lina Paventi
* Sophie Lorain as Pina Lunetti
* Tim Post as Peter
* Tara Nicodemo as Yolanda/Woman in Airplane/Jolene
* Pierrette Robitaille as Rosetta
* Dino Tavarone as Giorgio
* Mark Camacho as Johnny Christofaro
* Michel Perron as Father Carmignani
* Lou Vani as Marco
* Diane Lavallée as Mélanie

==Production==
Mambo Italiano was based on a play by Steve Galluccio, which was written based on his own real-life experiences growing up in an immigrant community in Montreal.    

==Reception==
Mambo Italiano received mixed to negative reviews, currently holding a 32% rating on  , the film has a 41/100 rating, indicating "mixed or average reviews." 

Scott Brown from Entertainment Weekly wrote “This is feel-good filmmaking, to be sure, but the culture clash here is more than a meaningless vehicle for fizzy wish fulfillment. The not-unpleasant result is hearty Italian fare with the half-life of Chinese takeout.”    

Janice Page from the Boston Globe wrote “No sophisticated dance, but it moves about with an open heart. And hey, it’s at least as funny as that Greek thing.”
Chicago Sun-Times  Roger Ebert gave the film a modest two stars and refers to the film "in convenience" as "My Big Fat Gay Wedding". 

==Follow-up== Ciao Bella, which explored similar themes of culture clash that are examined in this film and the play it was based upon. Claudia Ferri, who played Angelos sister Anna in Mambo Italiano, played the lead role in Ciao Bella.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 