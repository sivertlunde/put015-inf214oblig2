Donald and Pluto
{{Infobox Hollywood cartoon
|cartoon_name=Donald and Pluto
|series=Mickey Mouse
|image=
|image_size=
|caption=
|director=Ben Sharpsteen
|producer=Walt Disney Bill Roberts 
|voice_actor=Clarence Nash
|distributor=United Artists
|release_date=September 12, 1936 
|color_process=Technicolor
|runtime=
|movie_language=English
}} Walt Disney Pluto as his assistant. The film was directed by Ben Sharpsteen and featured the voice of Clarence Nash as Donald.

Donald and Pluto is the only installment of the Mickey Mouse (film series)|Mickey Mouse series in which Mickey does not appear as a character. The cartoon also introduced Fred Spencers new design for Donald Duck, which included a slimmer body, shorter neck, rounder feet, and a shorter bill.

==Plot== Pluto and later accidentally pulls Plutos bone away from him. While Pluto wrestles with the magnet to get his bone back, he swallows the magnet and gets his bone stuck to his bottom. As he fights to get the bone, he tumbles into the pile of furniture Donald is standing on, causing Donald to come crashing to the ground. Pluto eventually runs into the kitchen and the magnet inside him causes many cooking items to be pulled onto his rear.

Plutos erratic actions eventually cause the dishes to fall off, but his bone continues getting stuck to him and annoying him. As he tries to get it off, he backs into the clock and gets stuck to it. After breaking free of the clock by destroying it, he pulls a much smaller alarm clock to him. He engages in a fight with the clock, soon realizing that if he makes minor movements along the wall rear-end first the clock will not come to him. However, he trips over a rolling pin and the clock sticks to him again, but he loses it in a polar bear rug. His dish sticks to him again and the magnetism causes knives and forks to come out of a drawer and chase him. The chase eventually causes Pluto to end up in the basement again. There, the magnet inside him sucks the nails out of the ladder Donald is standing on, causing it to fall apart under his feet. Donald falls into a tank and is pulled out through a wringer. After an angry outburst he gets stuck to Plutos bottom and is dragged into the roof of the basement.

Pluto is chased by the angry Donald through the house and onto the roof, where the magnetism causes Donald to be pulled into the roof and along the ceiling with the floor separating the two (it looks as if there is an invisible track Donald is hanging from). Donald is dragged into a ceiling fan, activating it and spinning both Donald and Pluto around. Donald gets an electric shock when he pulls out a lamp and is later bumped across the ceiling when Pluto crawls over a ladder. Pluto eventually makes his way downstairs where Donald falls to the ground and bounces back into the basement when he is pinned to the wall by his own tools. Pluto finds him and begins licking him as he squawks angrily.

==DVD Release==
Donald and Pluto is available on   in the Walt Disney Treasures series along with many other early Donald short films

==External links==
* 
* 

 
 
 
 