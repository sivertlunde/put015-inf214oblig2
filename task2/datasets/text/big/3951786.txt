A Dog's Breakfast
 
{{Infobox film
| name        = A Dogs Breakfast
| image       = A Dogs Breakfast DVD cover.jpg
| caption     = DVD cover
| director    = David Hewlett
| producer    = {{plainlist|
* John Lenic
* Jane Loughman }}
| writer      = story and screenplay by David Hewlett
| starring    = {{plainlist|
* David Hewlett
* Kate Hewlett
* Paul McGillion
* Rachel Luttrell
* Amanda Byram
* Michael Lenic
* Christopher Judge
* Mars the Dog }}
| music       = Tim Williams
| editing     = Jason Schneider
| distributor = Independent & MGM Studios
| released    = Nov 16, 2006 (screening) Jul 4, 2007 (Internet) Sep 18 2007 (RC1 DVD)
| runtime     = 88 minutes
| rating      =
| country     = United States Canada
| language    = English
| budget      = Approximately $120,000
| gross       =
| followed by =
}}

A Dogs Breakfast is a Canadian comedy independent film produced in 2006. It was the first film to be written and directed by British-born Canadian actor David Hewlett, who is best known for his role of Dr. Rodney McKay in the TV series Stargate Atlantis. Hewlett created the film as a private off-season project and stars alongside his real-life sister Kate Hewlett and Stargate actors Paul McGillion, Christopher Judge and Rachel Luttrell. The film was produced by John Lenic and Jane Loughman.

Due to its strong affiliation with the Stargate franchise, the film generated considerable buzz within the Stargate fandom. It had several screenings in selected major towns in the US and the UK in late 2006 and 2007. MGM picked up worldwide television and home video rights to the film in early December 2006. The film was released on DVD on September 18, 2007 in The United States and Canada.

==Plot==
Patrick (David Hewlett) is single, loves his dog (Mars the Dog) and still lives in his parents house ten years after their death. Shortly before Christmas, Patricks sister Marilyn (Kate Hewlett) visits Patrick to introduce him to Ryan (Paul McGillion), a science fiction television star.

After accidentally knocking Ryan out with a   between Marilyn and Chris (Christopher Judge) to get time to dispose the body in the garden and in a lake nearby. But Ryans dead body reappears each time. When Marilyn alerts the police that Ryan is missing, Ryans aunt investigates Ryans disappearance. After first suspecting Marilyn, Patricks cover blows.

Because it looks bad for Marilyn, the siblings decide to dismember Ryans body and give it to Mars and the neighbors dogs as food. Finally, when Patrick admits that Ryan has basically always been a friend to him, Marilyn reveals her plan: She and Ryan just faked his death, and the body that Patrick has been trying to get rid of has been Marilyns sex doll all the time. Ryan has assumed the role of his aunt.

Some time later, when Patrick grows comfortable with the idea to accept Ryan as his brother-in-law, Ryans sister Elise (Amanda Byram) arrives but is not enthused with the upcoming wedding. A love at first sight between Patrick and Elise is apparent. While Marilyn shows her sister-in-law to-be the house, Ryan leads Patrick to the lake, with a moose figure behind his back.

==Cast==
* David Hewlett as Patrick
* Kate Hewlett as Marilyn
* Paul McGillion as Ryan / Colt / Detective Morse
* Christopher Judge as Chris
* Rachel Luttrell as Ratcha
* Amanda Byram as Elise
* Michael Lenic as Zero
* Mars the Dog as Mars

==Production==

===Conception===
Inspired by other actors pursuing a career in directing, David Hewlett wanted to spend his hiatus from Stargate Atlantis with other projects.
He wrote three scripts of the horror and thriller genre before he got the idea for A Dogs Breakfast. He chose a more humorous subject because Stargate Atlantis generally involves peril despite its comedy elements. Hewlett considered the movie as a personal challenge and did not have wide-distribution plans initially.       
 English slang phrase meaning "a mess or muddle".  In an online radio interview, David Hewlett joked that he chose this title so that the movie would have the right title if it didnt work out. He also said that the main character makes a mess out of everything, including murder, so the title was in fact carefully chosen. 

Although David Hewlett had been rather focused on the actors and the acting before, he started paying more attention to old style comedy film-making such as works of Monty Python and director Blake Edwards. In general, old-fashioned comedies would appeal more to him than comedy films of recent history. Hewlett said he copied some old-fashioned techniques into his own project.    He specifically mentioned A Fish Called Wanda and Fawlty Towers as major influences. 

Despite A Dogs Breakfasts murder theme, Hewlett wanted to avoid violence and gore as audience attraction, and rather produce a family film for all ages. He thinks that younger members of the audience will enjoy the cartoon-like elements of the movie. 

Hewlett further mentioned Buster Keaton and Harold Lloyd of the silent era of movie-making as major influences, who he says reflect a simpler method of story-telling and comedy. Similarly, he did not want to overdo cutting and manipulation, as he feared this would endanger the comedy effect. 

===Filming=== Paramount Production HD cameras for two weeks for a days rate.  

The majority of A Dogs Breakfast was filmed on a fourteen-day schedule and with extremely limited funds in January 2006 when Stargate Atlantis (the TV show David Hewlett stars in) was on hiatus between seasons, although some scenes had already been filmed on Saturdays during the Atlantis season.    David Hewlett had originally planned to shoot at his own house in Washington (U.S. state)|Washington, but legal problems due to its U.S. location forced the otherwise Canadian production to rent a house near Burnaby, British Columbia, Canada.    Vancouver proved convenient as an alternative.

Many of the local actors and crew were eager to participate in the project, since the Vancouver winter weather creates a yearly lull in the local acting business.  As such, A Dogs Breakfast featured many actors, crew members and pieces of equipment that are usually associated with the Stargate franchise. David Hewlett plays Rodney McKay in Stargate Atlantis, Kate Hewlett plays McKays sister in several Atlantis episodes, Paul McGillion and Rachel Luttrell play Carson Beckett and Teyla Emmagan in Atlantis, respectively, and Christopher Judge is known as Tealc from Stargate SG-1. Hewlett joked that McGillion joined the project because "he   nothing else to do," and that his sister, Kate Hewlett, "was really hard to get a hold of, because shes got a writing/acting career herself."  Hewlett considered Christopher Judge for the role of the "Internet-dating loser" because "he seemed so incredibly inappropriate for the part that I thought it would be very funny to have him play it." Luttrell played "a space princess on the cheesy show."   
 Behind the Timothy Williams.

Hewlett wrote the script especially around those availabilities.  This made it possible to produce the film with a budget of $120,000 total to film and produce, mostly privately raised.      

===Marketing===
 
The script was written with the Stargate audience in mind and pet lovers as a secondary audience.    David Hewlett stated that he consciously wrote for certain audiences, as he had seen too many independent film makers fail who did not do so.  convention attendance around the world. 

Inspired by The Long Tail, Hewlett started an internet marketing campaign by setting up a YouTube channel and his own producer blog for $8 a month to keep promotion costs down.  Hewlett discussed marketing strategies with eager fans online.  He called this Squirrel Marketing.    

Promoting the movie from a Stargate angle plus high YouTube sneak preview viewing numbers and a full theater for the test screening helped to convince Stargates distributor MGM to make a deal with A Dogs Breakfast in November 2006.      MGM in turn took advantage of Hewletts grassroots support and wanted to make the movie into a test-run for direct-to-download releases.  MGM officially announced the pick-up for worldwide television and home video rights on December 4, 2006.  

==Releases== Creation Stargate Cinequest San Jose Film Festival in early March 2007,  and two screenings took place in the city of Vancouver on March 25, 2007.  In the screening at the 2007 Stratford-Upon-Avon Film Festival on May 24 and 26, the film was awarded Best Feature Film, and David Hewlett won the award for Best Actor.   The Glastonbury Festival screened A Dogs Breakfast on June 22, 2007. 
 iTunes and Amazon Unbox beginning their release on July 4, 2007. MGM senior executive vice president Charles Cohen considered this as a natural move due to the high amount of interest among the Stargate fan online community.  A release on hulu.com followed later. 

The film was released on  .    However, as of May 2009, MGM held off an international release for up to December 2009. A UK release date remains unknown.    The DVD contains special features, including a commentary track, deleted scenes, behind-the-scenes, and interviews with the cast and crew. 

==Reception==
GateWorld gave four out of four stars for the "mix of physical comedy and goofy sound effects, smart storytelling and a good deal of warmth". The reviewer labeled the movie a must-see for David Hewlett fans, and considered the acting "very real", with "delightful bonuses to Stargate fans".    Bostennow thought the movie was "well worth watching" and "quirky, funny and, that rarest of finds in comedy, original." They liked the comic timing of the actors, especially Hewlett.  DVDSnapshot further found the low-budget indie project "a surprisingly good   while not relying on any big name stars or special effects." The Hewlett siblings were considered to have a "great onscreen brother-sister chemistry", and Paul McGillions performance was lauded despite the few scenes he appears in. DVDSnapshot also stated that "the obvious jabs at Stargate are a lot of fun." 
 Arsenic and Old Lace gone maniacally modern.   Breakfast reveals how great comedy is not dependent on cost – put together an intelligent script and a talented cast, and look how solid the results can be." The character work and beats, the direction and Hewletts performance as "a master of wild overreaction and cartoonish screams" receive further positive mention.    The Leader-Post found "what it may lack in polish it more than makes up for in wit and fun. The result is a quirky black comedy with plenty of Monty Python-esque humour and a hint of "The Tell-Tale Heart".   The movies originality and simplicity is an asset as the spotlight is reserved fully for David Hewlett and his great acting abilities." 

==Starcrossed and sequels== Cinequest San Jose Film Festival with David Hewlett and Jane Loughman, on March 3, 2007.  David Hewlett wrote a draft for the Starcrossed pilot in late January 2007 and sent it to the network,  who bought the pilot in return.  A press release by the Sci Fi Channel described Starcrossed as a high touch, high-tech half-hour comedy about life behind the camera at a long-running sci-fi space soap, similar to the movie Galaxy Quest.    David Hewlett further outlined Starcrossed as a "situation comedy about the making of a science fiction show, because theres really nothing sillier than serious science fiction.   Its very loosely based on a Stargate kind of thing, but its not Stargate. Its not a spoof. Its just the silliness of the day-to-day life of trying to make a television series". 
 Larry Sanders-type show about making science fiction." Other possibilities are producing webisodes.  In March 2008, The Hollywood Reporter announced that Starcrossed was planned to debut in the fourth quarter of 2008 as an original web series, produced by Universal Media Studios, Jane Loughman and John Lenic.  However, none of the plans were implemented. In April 2009, David Hewlett said via his Twitter account that Sci Fi would return the rights of Starcrossed to him and that he planned to shoot a two-hour pilot episode himself.  

In his blogs and in an interview, David Hewlett has announced that a sequel is planned named A Dogs Breakfast 2: Heir of the Dog.  He also announced that he would like to bring back Christopher Judge, Rachel Luttrell, and Paul McGillion in the new movie. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 