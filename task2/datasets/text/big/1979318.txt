The Cookout
{{Infobox film
| name           = The Cookout
| image          = Cookout_poster.jpg
| caption        = Theatrical release poster
| director       = Lance Rivera
| producer       = Shakim Compere Queen Latifah
| screenplay     = Laurie B. Turner Ramsey Gbelawoe Jeffrey Brian Holmes
| story          = Queen Latifah Shakim Compere Darryl French Eve Danny Glover Queen Latifah
| music          = Camara Kambon
| cinematography = Tom Houghton
| editing        = Patricia Bowers Jeff McEvoy
| studio         = Flavor Unit Entertainment
| distributor    = Lions Gate Entertainment
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $16 million 
| gross          = $12,009,070 
}}

The Cookout is a 2004 comedy film, directed by Lance Rivera. It is co-written by and features Queen Latifah, and is also the feature film debut for her mother Rita Owens. This was the last film for Farrah Fawcett due to her death in 2009.

==Plot==
When Todd Anderson (played by Quran Pender) has just signed a $30 million contract with his hometown basketball team the New Jersey Nets, he purchases many new luxuries for himself and his family including a new house in a well established, high class neighborhood for him and his gold digging girlfriend Brittany (played by Meagan Good). Keeping with family tradition, he decides to host a regular family reunion cookout in his new place, however, not planning for it to clash with an important business meeting for an endorsement deal. 

Meanwhile, Bling Bling (played by Ja Rule) is jealous that Todd has made it with his basketball contract after being insulted and embarrassed in front of the neighborhood when Todd did not recognize who he was. He plans to get many pairs of sneakers signed by the upcoming star to sell on eBay and become rich. Whilst on his way to Todds new house, he crashes his car due to his clumsy friend and then tries to steal a new Mercedes from a car park without realizing the car belongs to Todds girlfriend Brittany. After finding out he holds her at gunpoint and forces her to drive him there.

Although the meeting is scheduled to take place in the morning, and the guests are to arrive in the afternoon, one by one members of Todds eccentric family begin to arrive before expected disrupting his business interview. The neighbors are drawn to the cookout, and Todd is concerned mainly about his image, as his familys antics are making a poor impression on his neighbors. 

When Bling Bling and his criminal sidekick invade the cookout so they can get Todds autograph, the ensuing chaos makes Todd realize how much he needs his family. He realizes that he loves the family for the way they are and gets a shock by the endorsement interviewer. Todd breaks up with Brittney and marrys Becky and scored 26 points in his debut in New Jersey.

==Cast==
*Quran Pender &mdash; Todd Andersen (credited as Storm P.)
*Jenifer Lewis &mdash; Lady Em/Emma Andersen
*Meagan Good &mdash; Brittany
*Ja Rule &mdash; Bling Bling/Percival "Assmackey" Ashmokeem
*Jonathan Silverman &mdash; Wes Riley
*Tim Meadows &mdash; Leroy
*Farrah Fawcett &mdash; Mrs. Crowley
*Ruperto Vanderpool &mdash; Wheezer
*Frankie Faison &mdash; JoJo Andersen
*Vincent Pastore &mdash; Horse shit salesman (credited as "Poo Salesman") Eve &mdash; Becky
*Danny Glover &mdash; Judge Crowley
*Queen Latifah &mdash; Security Guard Roberto Roman &mdash; Danny
*Reg E. Cathey --- Frank Washington
*Jerod Mixon &mdash; Willie
*Jamal Mixon &mdash; Nelson
*Gerry Bamman &mdash; Butler
*Wendy Williams &mdash; Piers Gabriel

== The Music and soundtrack ==

While there was no official soundtrack commercially released for the film,
there were original songs and remakes of R&B classics included in the picture.

The Cookout main theme "Family Reunion" performed by Noel Gourdin
Produced by Kaygee & Terence "Tramp Baby" Abney
& written by Balewa Muhammad, Keir Gist, Terence "Tramp Baby" Abney

The Cookout End theme "The Cookout" Performed by Treach (Of Naughty By Nature) & Duganz
Produced by Kaygee & Terence "Tramp Baby" Abney
& written by Nastacia Kendall, Anthony Treach Criss, Duganz, Keir Gist, Terence "Tramp Baby" Abney

"The Closer I Get to You" Performed by Barbara Wilson, and Romeo Johnson.
written by Reggie Lucas and James Mtume
Courtesy of Inflx Entertainment. Published by Ensign Music Corp. o/b/o itself and Scarab Publishing Corp. (BMI)

& Cheryl Lynns "Got To Be Real"
Written by David Paich, David Foster, and Cheryl Lynn

==Box office==
The film opened on 1,303 screens, and opened at #8 in the box office with a gross of $5,000,900. After seven weeks, it ended with a domestic gross of $11,814,019 and made $195,051 from foreign countries, for a total of $12,009,070 world-wide. 

==Sequel==
On September 3, 2011, a sequel, The Cookout 2 premiered on Black Entertainment Television|BET. Quran Pender, Ja Rule and Frankie Faison were the only cast members to reprise their roles. Despite the title, the plot itself is not about a cook-out at all. Talk show host Wendy Williams had a brief cameo as reporter Piers Gabriel in the original. In the sequel, she makes a special guest appearance as herself. Charlie Murphy co-stars in the film as Coach Ashmokeem, the brother of Bling Bling (Rule). Rapper Rick Ross also makes a special guest appearance in the film.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 