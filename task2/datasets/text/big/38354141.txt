Neelakasham Pachakadal Chuvanna Bhoomi
{{Infobox film
| name           = Neelakasham Pachakadal Chuvanna Bhoomi
| image          = NPCB_poster.jpg
| caption        = Official Poster
| director       = Sameer Thahir
| producer       = Sameer Thahir
| writer         = Hashir Mohamed Surja Bala Hijam Ena Saha
| music          = Rex Vijayan
| cinematography = Gireesh Gangadharan
| editing        = A. Sreekar Prasad
| studio         = Happyhours Entertainment E4 Entertainment
| Distributed By = E4 Entertainment & PJ Entertainments Europe 
| stills         = Vishnu Thandassery
| released       =  
| language       = Malayalam
| country        = India
| budget         = 3.5 crore
| gross          = 5 crore 
}}
 thriller road Surja Bala Hijam & Ena Saha in the lead roles. 

The film began shooting in the north-eastern state of Nagaland in February 2013 and completed its shoot in June 2013.  The film was shot at real locations in seven Indian states, Kerala, Karnataka, Andhra Pradesh, Orissa, West Bengal, Nagaland and Sikkim.   
 Into the The Motorcycle Diaries.

==Plot==
The movie begins when Kasi (Dulquer Salmaan) tells Suni (Sunny Wayne) that he is going for a long road trip. Suni says that he will join the trip no matter where he goes. Suni in fact knows where Kasi was planning to go. The rest of the movie is about the road trip they make.
 Surja Bala Hijam), a girl from Nagaland. Nagaland is a place of political unrest and her parents were killed during the fights. Kasi takes her to his home to get married. His mom did not like the relation because Assi does not know Malayalam and she was not a Muslim. Initially his father agreed to their relation but due to pressure from his wife and the political party in which he was part of, he was forced to disapprove the relation. Assi returns to Nagaland without telling Kasi. Now Kasi is on his way to Nagaland to bring her back.

During their trip, they also end up in a remote village outside Kolkota because Sunis headlight stops working. They stay with the locals for a while where Suni falls in love with a girl called Gowri (Ena Saha). After leaving that village and riding for a while, Suni decides to stay back after they nears Nagaland. At Nagaland Kasi meets Assi and tells her that he wants to take her back. That night militants try to kill them during their dinner but they manage to escape. The movie ends when Kasi and Assi starts their bike ride together.

==Cast==
* Dulquer Salmaan as Kasim aka Kasi
* Sunny Wayne as Sunil aka Suni Surja Bala Hijam as Assi
* Ena Saha as Gowri
* Dhritiman Chatterjee
* Shane Nigam as Shyam
* Joy Mathew as Abdul Haji
* Vanitha Krishnachandran as Azma
* Paloma Monappa as Ishita
* Avantika Mohan as Fatima
* Baby Anikha as WafaMol
* Ajay Nataraj
* Master Reinhard Abernathy as Balu
* K. T. C. Abdullah
* Madhubaladevi
* Bobby Zachariah Abraham
* Pearle Maaney
* Avinash Sivadas Vettiyattil

==Production== Manipuri actress Surja Bala was signed to play Assi, a student who comes to Kerala to study engineering under the north east quota. The actress who makes her debut in Malayalam plays Dulquer Salmaans love interest in the film.  18 -year -old Bengali actress Ena Saha was roped in to play Sunny Wayne’s lady love. She played a village belle, Gauri and her shoot happened in Visakhapatnam.  Her acting was immensely praised & received positive acclaim from allover. Joy Mathew was cast to play Dulquer Salmaans fathers role.   Paloma Monappa said that she had done the role of a surfer, also making her Malayalam debut.    A Kochi-based adventurer Bobby Zachariah Abraham had done the role of a biker. 

According to sources in the industry, despite its picturesque landscape, filmmakers have always stayed away from the north-east region of India citing security reasons. They also point out that the hard terrain with its hilly regions make it difficult to shoot there. However, director Sameer was all set to take up the challenge and was looking forward to the Nagaland shoot.  The film was shot in Government Engineering College, Thrissur and a few portions in Ayyanthole including the strike scene of students. This films first official teaser released on June 22 in YouTube and got largely positive reviews from the viewers. The movie was released all over Kerala on 9 August. 

==Soundtrack==
{{Infobox album
| Name = Neelakasham Pachakadal Chuvanna Bhoomi 
| Type = Soundtrack
| Artist = Rex Vijayan
| Cover = 
| Caption = 
| Released = 2013
| Recorded =  Feature film soundtrack
| Length =
| Online Promotion =
| Label = 
| Producer = Sameer Thahir
| Last album = English (film)|English (2013)
| This album = Neelakasham Pachakadal Chuvanna Bhoomi   (2013)
| Next album =North 24 Kaatham   (2013)
}} 

The music of the film has been composed by Rex Vijayan and lyrics penned by Vinayak Sasikumar.
{{tracklist
| headline     =
| extra_column = Artist(s)
| total_length =
| title1       = Doore Doore
| extra1       = Suchith Suresan
| length1      = 4:19
| title2       = Neelakasham
| extra2       = Rex Vijayan
| length2      = 3:41
| title3       = Thazhvaram
| extra3       = Sushin Shyam 
 
 
| length3      = 4:08
| title4       = Neerppalunkukal
| extra4       = Saju Sreenivas 
{{cite web
|url=https://www.facebook.com/pages/Pathayam/360050850776219 title = Member of Pathayam Band}}
 
| length4      = 3:13
| title5       = Ami Hitmajare
| extra5       = Traditional Baul Song
| length5      = 4:40
}}

==Critical reception==
IndiaGlitz gave a rating of 7/10, stating that "is a movie that must be watched for its solid, though diluted content."  The Times of India rated the film as a 3.5/5, praising some of the voice acting, but questioning the relevance of some of the characters political stances.  The Hindu praised the entertaining nature of the film, the acting and the script, whilst criticizing the second half of the film and its climax. 

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 