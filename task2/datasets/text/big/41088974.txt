Ambush at Tomahawk Gap
{{Infobox film
| name           = Ambush at Tomahawk Gap
| image          = Ambush at Tomahawk Gap.jpg
| caption        = 
| director       = Fred F. Sears
| producer       = Wallace MacDonald
| writer         = 
| starring       = John Hodiak
| music          =   
| cinematography = Henry Freulich
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}}
Ambush at Tomahawk Gap is a 1953 American western film directed by Fred F. Sears. 

==Cast==
*John Hodiak: McCord
*David Brian: Egan
*John Derek: Kid
*Ray Teal: Doc
*María Elena Marqués: Navaho girl 
*John Qualen: Jonas P. Travis
*Otto Hulett: Stranton
*Trevor Bardette: sheriff of Twin Forks 
*Percy Helton: Marlowe
*Harry Cording: ostler
*John Doucette: Burt 
*John War Eagle: Indian Chief

==References==
 

==External links==
* 

 
 