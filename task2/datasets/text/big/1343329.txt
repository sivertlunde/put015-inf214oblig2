Ben Hur (1907 film)
 Ben-Hur}}
{{Infobox film
| name           = Ben Hur
| image          = File:Buyers_Gallery_Video_Cover_for_Ben-Hur_(1907_Film).jpg
| caption        = Home video cover Frank Rose (uncredited)
| producer       = Lawrence Bender
| writer         = Gene Gauntier Lew Wallace
| starring       = Herman Rottger William S. Hart
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Kalem Company
| released       =  
| runtime        = 15 minutes
| country        = United States Silent
| budget         = $500
| gross          = 
| followed_by    =
}}

Ben Hur is a 15-minute-long 1907  , one of the best-selling books at that time.

==Development==
 
The film is most notable as a precedent in copyright law. The movie was made without the permission of the authors estate, which was common practice at that time. The screenwriter, Gene Gauntier, remarked in her 1928 autobiography how the film industry at that time infringed upon everything. As a result of the production of Ben Hur, Harper & Brothers and the authors estate brought suit against Kalem Studios, the Motion Picture Patents Company, and Gauntier for copyright infringement. The United States Supreme Court ultimately ruled against the film company in 1911.  This ruling established the precedent that all motion picture production companies must first secure the film rights of any previously published work still under copyright before commissioning a screenplay based on that work.
 Canadian film director Sidney Olcott. At fifteen minutes long, only a small portion of the story was put on screen. The focus of the piece was the chariot race, which was filmed on a beach in New Jersey with local firemen playing the charioteers and the horses that normally pulled the fire wagons pulling the chariots.

In 1908, perhaps seeking to capitalize on the publicity of the case and the film, Harper & Brothers published a lavishly designed and illustrated book, The Chariot Race from Ben-Hur, which excerpted only the race from Lew Wallaces novel.  Accompanying the text were color illustrations by Sigismond Ivanowski.
 silent film Messala from the 1899 Broadway premiere and 1900 season Broadway play. Herman Rottger is Judah Ben-Hur|Ben-Hur, where as in the 1900 play William Farnum, another soon to be famous silent cowboy, played Ben Hur against Harts Messala.   

==References==
 

==External links==
 
*   
* , from the book Select Cases on the Law of Torts, by John Henry Wigmore.
*     website dedicated to Sidney Olcott
*  
 

 
 
 
 
 
 
 
 
 