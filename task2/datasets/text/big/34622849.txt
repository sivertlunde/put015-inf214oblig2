The Big Racket
{{Infobox film
| name = The Big Racket aka Il grande racket
| image = The Big Racket.jpg
| director = Enzo G. Castellari
| writer =
| starring = Fabio Testi
| music = Maurizio De Angelis, Guido De Angelis
| cinematography =  Marcello Masciocchi
| editing =
| producer =
| distributor = Titanus
| released =  
| runtime =
| awards =
| country = Italy
| language = Italian
| budget =
}} 1976 Cinema Italian poliziottesco film directed by Enzo G. Castellari. Fabio Testi stars as a police inspector who takes on a gang of hoodlums who terrorise an Italian city by extorting cash from local shop and bar owners.

== Cast ==
*Fabio Testi: Inspector Nico Palmieri
*Vincent Gardenia: Pepe
*Renzo Palmer: Luigi Giulti
*Orso Maria Guerrini: Gianni Rossetti
*Glauco Onorato: Piero Mazzarelli
*Marcella Michelangeli: Marcy
*Romano Puppo: Doringo
*Antonio Marsina: Attorney Giovanni Giuni
*Salvatore Borghese: Sgt. Salvatore Velasci
*Joshua Sinclair: Rudy 
*Daniele Dublino: Commissioner 
*Anna Zinnemann: Anna Rossetti

==Reception==
The film received mixed reviews. Film critic Morando Morandini started his review writing ""Its a fascist film. Its a vile film. Its an idiot film" and subsequently criticized the violent and "reactionary" morality of the film. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 