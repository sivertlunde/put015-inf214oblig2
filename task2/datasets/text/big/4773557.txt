Slither (1973 film)
{{Infobox film
| name           = Slither
| image          = Slither (1973 film).jpg
| image_size     =
| caption        =
| director       = Howard Zieff
| producer       = Jack Sher
| writer         = W.D. Richter
| narrator       = James Caan Peter Boyle Sally Kellerman Louise Lasser Allen Garfield Richard B. Shull
| music          = Tom McIntosh
| cinematography = László Kovács (cinematographer)|László Kovács
| editing        = David Bretherton
| distributor    = Metro-Goldwyn-Mayer
| released       = March 7, 1973
| runtime        = 96 minutes
| country        = United States English
| budget         =
| gross = $1,355,000 (US/ Canada rentals) 
}} James Caan. It was directed by Howard Zieff. 

This was the first screenplay by W. D. Richter, who went on to adapt stories like Invasion of the Body Snatchers and Big Trouble in Little China for the screen and directed the cult film The Adventures of Buckaroo Banzai Across the 8th Dimension.

Caan plays an ex-convict, one of several people trying to find a stash of stolen money. Peter Boyle and Sally Kellerman co-star.

==Plot==
Car thief Dick Kanipsia gets a parole from a penitentiary. He intends to go straight, but first he goes directly to see an old friend, Harry Moss, only to be shocked to see Harry get shot. Harrys dying words tell Dick to go find Barry Fenaka, a guy who supposedly knows where to find a stash of stolen cash that Harry has hidden. Instead of dying slowly, Harry blows himself up with dynamite. As Dick flees the scene, a black van lurks in the trees.

Dick hitches a ride with Kitty Kopetzky, who starts out as a friendly free spirit, then turns into a nut case who robs a diner where she and Dick go to eat. Dick flees during her robbery and catches a passing bus.

Fenaka turns out to be a small-time bandleader. He explains to Dick that he and Harry embezzled $320,000, and paid a man named Palmer to stash it for them. He and his wife take Dick to go get the money. They travel by car, with an Air Stream Land Yacht in tow. At Palmers office, they find a man named Holdebrooke who tells them that Palmer moved to Pismo Beach.

As they follow Palmers trail, the trio worry about the black camper van that seems to follow them. It is labelled Willow Camp for Boys and Girls. At the beach, Barry finds out that Palmer is now in Susanville. Somehow, Kitty tracks Dick down and joins the crew. An identical black van joins the first, and when Barry disappears, Dick and the women become convinced that he is in one of the vans.

They track the vans to a trailer camp, and Dick realizes that they are being followed by people he has encountered throughout the entire trip. Kitty creates a distraction which allows them to evade capture. One of the black vans leaves the camp in a hurry, and Dick pursues them. In the ensuing chase, the other black van eventually catches up and drives Dick and the trailer off the road. He creates a roadblock and forces the black van to crash into a waste pit. 

In a shootout with one of the men from the van, Dick wounds his attaker and tracks him down to a roadside vegetable stand. The wounded man is Holdebrooke, who confesses that he is really Palmer. He put all the money into the camp, but the location wasnt suitable, and the business failed. Barry arrives in a tow truck, revealing that he had simply gone for a tuna sandwich when Dick and the women thought he was abducted. He is thrilled to learn that the money was used to buy land. Dick walks away in disgust at the whole mess while Barry plots with his wife about how to manage the land.

==Cast==
* James Caan as Dick Kanipsia
* Peter Boyle as Barry Fenaka
* Sally Kellerman as Kitty Kopetzky
* Louise Lasser as Mary Fenaka
* Allen Garfield as Vincent J. Palmer
* Richard B. Shull as Harry Moss
* Alex Rocco as Ice Cream Man

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 