The Crucible (2011 film)
{{Infobox film
| name           = The Crucible 
| name           = Silenced
| image          = The Crucible-poster.jpg
| caption        = Promotional poster for The Crucible 
| film name      = {{Film name
| hangul         =  
| rr             = Dogani
| mr             = Togani}}
| director       = Hwang Dong-hyuk
| producer       = Uhm Yong-hun Bae Jeong-min Na Byung-joon
| based on       =  
| writer         = Hwang Dong-hyuk Jung Yu-mi  
| music          = Mogue
| cinematography = 
| editing        = 
| studio         = Samgeori Pictures
| distributor    = CJ Entertainment
| released       =  
| runtime        = 
| country        = South Korea Korean and Korean Sign Language
| budget         = 
| gross          =   
}}
 the novel Jung Yu-mi. deaf students were the victims of repeated sexual assaults by faculty members over a period of five years in the early 2000s.  
 National Assembly, where a revised bill, dubbed the Dogani Bill, was passed in late October 2011 to abolish the statute of limitations for sex crimes against minors and the disabled. 

==Background== Gwangju City officially shut down the school in November 2011.  In July 2012, the Gwangju District Court sentenced the 63-year-old former administrator of Gwangju Inhwa School to 12 years in prison for sexually assaulting an 18-year-old student in April 2005. He was also charged with physically abusing another 17-year-old student who had witnessed the crime (the victim reportedly attempted to commit suicide afterward). The administrator, only identified by his surname Kim, was also ordered to wear an electronic anklet for 10 years following his release.  

==Plot==
Kang In-ho ( ). But he and Yoo-jin soon realize the school’s principal and teachers, and even the police, prosecutors and churches in the community are actually trying to cover up the truth.     In addition to using "privileges of former post" (Jeon-gwan ye-u) the accused do not hesitate to lie and bribe their way to get very light sentences.

==Cast==
* Gong Yoo - Kang In-ho Jung Yu-mi - Seo Yoo-jin Kim Hyun-soo - Yeon-doo
* Jung In-seo - Yoo-ri
* Baek Seung-hwan - Min-soo Kim Ji-young - In-hos mother
* Jang Gwang - headmaster twin brothers
* Im Hyeon-seong - Young-hoon
* Kim Joo-ryung - Yoon Ja-ae
* Eom Hyo-seop - police officer Jang
* Jeon Kuk-hwan - Attorney Hwang
* Choi Jin-ho - prosecutor
* Kwon Yoo-jin - judge
* Park Hye-jin - headmasters wife Kim Ji-young - Sol-yi
* Eom Ji-seong - Young-soo
* Lee Sang-hee - auto repair shop owner
* Nam Myung-ryul - Professor Kim Jung-woo
* Jang So-yeon - courtroom sign language interpreter
* Hong Suk-youn - school custodian/guard

==Reception==
 

In Korea the film ranked #1 for three consecutive weeks and grossed   in its first week of release   The Hankyoreh. 28 September 2011. Retrieved 2011-10-15   and grossed a total of   after ten weeks of screening.  

After the films release, the bestselling book of the same name by author Gong Ji-young, which first recounted the crimes and provided the bulk of the film’s content, topped national bestseller lists for the first time in two years.  Ruling conservative political party Grand National Party (GNP) then called for an investigation into Gong Ji-young for engaging in "political activities", a move that was met with public derision. 

It received the Audience Award at the 2012 Udine Far East Film Festival in Italy. 

Conversations about the film and its impact reemerged when the Samsung Economic Research Institute (SERI) released its annual survey of the year’s top ten consumer favorites on December 7, 2011. Based on a poll of market analysts and nearly 8,000 consumers, SERI’s "Korea’s Top Ten Hits of 2011" ranked The Crucible among the year’s top events.   

==International release== San Jose, Huntington Beach, New Jersey, Philadelphia, Atlanta, Dallas, Chicago, Seattle, Portland, Oregon|Portland, Las Vegas, Toronto and Vancouver. It has been reviewed by The Wall Street Journal,  The Economist  and The New York Times. 

==See also==
* Cinema of Korea
* List of South Korean films
* Gwangju Inhwa School

==References==
 

== External links ==
*    
*    
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 