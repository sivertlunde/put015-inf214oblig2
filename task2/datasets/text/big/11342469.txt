Down and Outing
 
{{Infobox Hollywood cartoon
|cartoon_name=Down and Outing
|series=Tom and Jerry
|image=Down and Outing Title.JPG
|caption=The title card of "Down and Outing"
|director=Gene Deitch
|story_artist=Larz Bourne sound efects=Tod Dockstader

|voice_actor=Allen Swift
|musician=Steven Konichek
|producer=William L. Snyder
|studio=Rembrandt Films
|distributor=Metro-Goldwyn-Mayer
|release_date=  October 26, 1961
|color_process=Technicolor
|runtime=7:00
|preceded_by=Switchin Kitten
|followed_by=Its Greek to Me-ow! English
}}
Down and Outing is a 1961 cartoon directed by Gene Deitch and produced by William L. Snyder. It is the second of the thirteen cartoons made in Czechoslovakia.

==Plot==
Tom is peacefully sleeping when he sees that his master, Clint Clobber is going fishing, so he gets his gear and tags along for the trip. Jerry sees Tom heading outside and gets his own gear. Jerry throws his fishing line into the car, which it hits Tom in the back of the head. Jerry tries to get into the car by using a small fence as a ladder, but Tom destroys it and the car begins to pull out of the driveway. Jerry manages to get in by going under the car and coming in from an opening in the floor of the trunk. Jerry then kicks Tom out of the car, causing Tom’s head to skid along the road. Jerry taunts Tom as he chases down the car, but the car stops at a red light, and Tom hits and bends his neck on the open trunk door. The light turns green again and drives away and, after running toward it, Tom manages to hop back into the car and captures Jerry.

Jerry manages to escape from Tom’s clutches and Tom grabs a frying pan from the gear to whack him with it, but Jerry jumps out of the way, causing Tom to hit Clint in the back of the head, in turn causing him to stop the car and start a traffic jam. Clint Clobber, his face instantly turning red, grabs the pan and repeatedly hits him on the head with it. Jerry then sneaks under Clint’s foot and pushes down on the accelerator pedal, causing the car to speed up so fast, that the speedometer breaks. Clint looks down and sees Tom’s hand, making him believe that Tom was the culprit, and angrily stomps on it, causing it to swell. Jerry offers Tom a first aid kit, but Tom tries to smack him with his swollen hand, but misses and causes his hand to hurt more.

Tom chases Jerry outside of the car and onto the roof, but he falls off and onto the front of the car, blocking Clint’s view. The irate man then stops the car, causing Tom to fly off and skid his bottom against the road. Tom sees that his bottom is on fire from the skidding and jumps into a nearby like to put the fire out, evaporating the water in the process. Tom runs back to the car and Clint, tired of Tom’s tomfoolery, straps him to the passenger seat very tightly to keep him out of trouble. Tom manages to escape his confinement and searches around for Jerry. Meanwhile, Jerry is using the first aid kit to disguise one of the Clint’s shoes as a mouse. The car finally makes it to the fishing grounds, and Tom grabs an oar to whack Jerry with when he sees him. As Clint prepares to exit the car, Tom sees the disguised shoe and, believing it to be Jerry, whacks Clint on the foot with the oar, causing him to jump up in pain and hit his head on the car roof, creating a large dent in it. A frightened Tom tries to flee, but Clint catches Tom by the trunks with his fishing line and gives him a long offscreen thrashing.

Tom is then forced to carry the heavy load of equipment to the boat as punishment, not knowing that Jerry is hiding out in the picnic basket. As Tom loads the boat, then Clints sour frown gradually becomes a smile, and the cat is back on the masters good graces. The two then set off for their fishing trip and while on the boat, Tom finds Jerry in the basket and tries to use an air pump to suck him out, sucking out the packed lunch in the process, and drops the mouse into the water. To retaliate, Jerry swims under the boat and ties Tom’s hook to Clint’s foot. Jerry tugs Tom’s line and Tom, believing he caught a fish, reels in his catch, only for his joy to turn to fear when he sees he’s caught Clint, and he once again gets brutally beaten. Jerry and Clint Clobber are then seen happily fishing, while Tom is tied up and placed into a bucket. Their catches are thrown into Tom’s face while he sobs at his predicament, and the cartoon ends.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 