The Bait (1921 film)
{{infobox film
| name           = The Bait
| image          = Thebait-lobbycard-1921.jpg
| image_size     = 220px
| caption        = A lobby card.
| director       = Maurice Tourneur
| producer       = Hope Hampton
| based on       =   John Gilbert (scenario)
| starring       = Hope Hampton
| music          =
| cinematography = Alfred Ortlieb
| language       = Silent (English intertitles)
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 mins.
}} silent crime John Gilbert, New Years 1921. The Bait is now considered to be a lost film.   

==Plot== mastermind of a band of crooks of which Simpson (Singleton) is a member. Joan accepts Bennetts assistance when he sends her to Europe and later joins her. The live in luxury when she meets John Warren (Woodward), a wealthy American. Joan receives her first jar of suspicion when Bennett introduces her to John as being Bennetts daughter. Bennett later tells her the plan is for her to marry John so that they will have access to the money. The girl rebels but Bennett threatens to send her back to prison or, still worse, expose her to John, with whom she has now fallen in love. The entire party return to the United States where Bennett forces Joan to accept Johns proposal of marriage. In the meantime some members of Bennetts gang have double-crossed him and tell Joan of the original theft Frameup|frame-up. A signed confession is secured from the girl that did the framing. In the effort to secure the confession, Bennett is killed by Simpson, who had been after the "goods" on Bennett. John is willing to have Joan despite all of this and they are happy.

==Cast==
*Hope Hampton - Joan Grainger
*Harry Woodward - John Warren, The Fish Jack McDonald - Bennett Barton, The Fisherman
*James Gordon - John Garson, The Game Warden
*Rae Ebberly - Dolly, The Hooked
*Joe Singleton - Simpson, The Bait-Catcher
*Poupee Andriot - Madeline, The Minnow
*Dan Crimmins, Jr. - Jimmy, The Bullfish

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 