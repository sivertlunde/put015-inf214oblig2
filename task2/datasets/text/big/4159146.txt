Threat (film)
 
 
{{Infobox film|
 name = Threat |
 image = Threat DVD-cover.jpg|
 caption     = DVD box. Designed by Robert Anthony Jr. Photos: James Dimaculangan, Jason Rose.|
  writer = Matt Pizzolo, Katie Nisa|
 starring = Carlos Puga, Keith Middleton, Rebekka Takamizu, Katie Nisa, Kamouflage, Neil Rubenstein, David R. Fisher, Tony Dreannan |
 director = Matt Pizzolo |
 producer = Katie Nisa, Matt Pizzolo, Kings Mob Productions |
 distributor = HALO 8 Entertainment |
 released = January 13, 2006|
 runtime = |
 language = English |
 music = |
 awards = |
 budget = |
}}

Threat ( .    

==Summary==
White, straight edge hardcore kid, Jim (Carlos Puga), and black, hip-hop radical, Fred (Keith Middleton), become friends living on New Yorks Lower East Side - both of them with the hope that their newfound brotherhood will bring solidarity to their disparate communities. Instead, the alliance triggers a violent race riot that spills into the city streets with devastatingly tragic consequences.

==Production== Lower East NYU business Ben Knight who was still a teenager at the time and put him to work as a production designer for the film. Nisa also cast Keith Middleton when she saw him walking on St. Marks Place (Manhattan)|St. Marks Place. Unknown to Nisa, Middleton was on his way to perform in the popular dance show Stomp (dance troupe)|Stomp.   
 DIY style that sharply contrasted with other more polished independent films of the mid 90s (sometimes referred to as Indiewood). The DIY style focused less on aesthetic and more on authenticity. This style later picked up traction with various DiY-Video movements including the mumblecore scene of the 2000s. Unlike most movies of the DIY-Video era to follow, Threat was shot on 16mm film.
 East Village diner 7A to pay for production supplies that "couldnt be borrowed or stolen." Initially, sound recording was to be handled by one of Nisas film student friends. When he couldnt make it to the first day of production, he instead gave Nisa a 15 minute lesson on how to run the nagra. She went on to be the films sole sound recordist for the first months of production.  

At the start of production, the crew consisted solely of Pizzolo, Nisa, Brancato, and Knight but over the course of production it grew to include over 200 young people from 5 different countries.  
 Suburbia  .  Critical response ranged from "easily one of the most important films of the decade"   to "there is no explanation, no logic, and no reckoning."   
 New York Most Precious Most Precious Blood, Bleeding Through, Eighteen Visions, and Terror (band)|Terror. Most of the films score, however, was composed by Alec Empire and his band Atari Teenage Riot. The score was constructed by Drum and bass|jungle-music producer queque.
 punk and DiY ideologies of the movie and their production company, Pizzolo and Nisa eschewed distribution offers from Hollywood studios.    Initially, the film was released as an underground VHS tape and toured across the US and Europe, playing at non-traditional venues such as record stores, hip hop clubs, skateparks, and music festivals. One of the more notable non-traditional screenings took place during the Sundance Film Festival at a Doc Martens shoe store across the street from Sundances flagship Egyptian Theater. International press for the Sundance screenings launched years of touring which culminated with an appearance at the Coachella Valley Music and Arts Festival, where music-video distributor HIQI Media signed on to distribute the film to theaters. Soon after, Pizzolo formed the punk rock cinema label HALO 8 Entertainment and released Threat on DVD.      

==Awards==
In October 2006, Threat won the Grand Prize for Best Feature at the Lausanne Underground Film and Music Festival in Lausanne, Switzerland.  

In April 2007, Threat won the "First Feature Film - Special Mention" prize at the Rome Independent Film Festival in Rome, Italy.  

==Soundtracks==

===Threat: Original Motion Picture Soundtrack === DHR label, and metalcore courtesy of Trustkill Records. The soundtrack was released by HALO 8 Entertainment in January 2006. 

====Track listing====
# "Night Of Violence" - Alec Empire
# "Start The Riot" - Atari Teenage Riot
# "Into The Death" - Atari Teenage Riot
# "Rage" - Atari Teenage Riot feat. Tom Morello & D-Story
# "Sick To Death" - Atari Teenage Riot
# "Get Up While You Can" - Atari Teenage Riot
# "Gotta Get Out" - Alec Empire
# "Common Enemy" - Panic DHH
# "Wanna Peel" - EC8OR
# "Number Seven With A Bullet" - Bleeding Through Most Precious Blood
# "One Hell Of A Prize Fighter" - Eighteen Visions Terror
# "Drone" - Eyes Like Knives
# "mPathik" - Queque
# "heVn" - Queque
# "I Am A Threat" - King David
# "Kids Are United" - Atari Teenage Riot

===Threat: Music That Inspired the Movie=== Judgment Night, mashups of hardcore punk and metalcore with breakcore. The album was released by HALO 8 Entertainment in January 2006. 

====Track listing==== Most Precious Blood vs. Alec Empire Schizoid
# Inside Out vs. Oktopus from Dälek
# "World Ablaze" (Threat mix) - Killswitch Engage vs. Edgey Terror vs. Enduser
# "Champagne Enemaz" - Eighteen Visions vs. Otto Von Schirach Defragmentation
# The End The Tyrant Judge vs. Bill Youngman
# "Stalwart Carapace" - Youth Of Today vs. Edgey Hecate
# "I Know That Youre Lying" - Today Is The Day vs. darph/nadeR Enduser
# Holocaust

== Notes ==
 

==External links==
* 
*  Synopsis and reviews of the movie at threatmovie.com

 
 
 
 