Zorro Rides Again
{{Infobox film
| name           = Zorro Rides Again
| image          = zorroridesagain.jpg
| image size     =
| caption        = John English
| producer       = Sol C. Siegel
| writer         = Franklin Adreon Morgan Cox Ronald Davidson John Rathmell Barry Shipman Johnston McCulley (original Zorro novel)
| narrator       = John Carroll Richard Alexander William Nobles Alberto Colombo Walter Hirsch Eddie Cherkose (aka Eddie Maxwell) Lou Handman
| distributor    = Republic Pictures
| editing        = Helene Turner Edward Todd
| released       =   {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 26–27
 | chapter =
 }} 
| runtime         = 12 chapters (212 minutes (serial)  68 minutes (feature)  6 26½-minute episodes (TV) 
| country        = United States
| language       = English
| budget          = $98,110 (negative cost: $110,753) 
}} film serial. western theme John English John Carroll who also sang the title song as a modern descendant of the original Zorro with Carroll stunt doubled by Yakima Canutt.  The plot is a fairly standard western storyline about a villain attempting to illicitly take valuable land (in this case a new railroad).  The setting is a hybrid of modern (1930s) and western elements that was used occasionally in B-Westerns (such as the western feature films also produced by Republic).

==Plot==
In contemporary (for the 1937 production) California, villain J. A. Marsden aims to take over the California-Yucatan Railroad with the aid of his henchman El Lobo.  The rightful owners, Joyce and Phillip Andrews, naturally object.  Their parter, Don Manuel Vega summons his nephew, James Vega, to help them as he is the great grandson of the original Zorro, Don Diego de la Vega.  He is disappointed, however, to find that his nephew is a useless fop (presumably Don Manuel had not paid too much attention to his family history).

Nevertheless, James Vega installs himself in the original Zorros hideout and adopts the Zorro identity to defeat Marsden and El Lobo. This Zorro uses twin pistols and (like the original Zorro) a whip as his main weapons of choice, rather than a more traditional sword.

==Cast== John Carroll as James Vega and his masked alter ego Zorro.  Despite being the same character and actor, the secret identity of the title character is extended to the opening credits wherein "Zorro" and "James Vega" are credited as separate characters.
* Helen Christian as Joyce Andrews
* Reed Howes as Phillip Andrews
* Duncan Renaldo as Renaldo
* Noah Beery, Sr. as J. A. Marsden Richard Alexander as Brad "El Lobo" Dace
* Nigel De Brulier as Don Manuel Vega
* Robert Kortman as Trelliger Jack Ingram as Carter Roger Williams as Manning
* Edmund Cobb as Larkin
* Mona Rico as Carmelita
* Tom London as OShea
* Harry Strang as OBrien
* Jerry Frank as Duncan

==Production==
Zorro Rides Again was budgeted at $98,110 although the final negative cost was $110,753 (a $12,643, or 12.9%, overspend).  It was filmed between 8 September and 5 October 1937.   The serials production number was 423.  Zorro Rides Again was influenced by the Singing Cowboy trend of the time. Carrolls "best moments" in costume were singing (Lyrics include "Zorro rides again into the night...") {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | origyear = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | page = 110
 | chapter = 4. Perilous Saturdays
 }} 
 Red Rock Canyon State Park, Angeles National Forest, and Chatsworth, Los Angeles. 

===Stunts===
In the opinion of Cline, one of the most memorable stunt scenes in the history of film serials is shown in Zorro Rides Again.  Stuntman Yakima Canutt plays Zorro as he gallops up to the cab of a moving truck and swings from the saddle to its running board.  Even a small mistake during this sequence would have been lethal for Canutt. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | page = 41
 | chapter = 3. The Six Faces of Adventure
 }} 

==Release==
===Theatrical===
Zorro Rides Agains official release date is 20 November 1937, although this is actually the date the sixth chapter was made available to film exchanges.  A 68-minute feature film version, created by editing the serial footage together, was released on 22 September 1938 and re-released on 16 January 1959.  The feature film had a working title of Mysterious Don Miguel before returning to the original name Zorro Rides Again.  This was one of fourteen feature films Republic made from their serials. 

===Television===
In the early 1950s, Zorro Rides Again was one of fourteen Republic serials edited into a television series.  It was broadcast in six 26½-minute episodes. 

==Chapter titles==
#Death from the Sky  (29 min 41s)
#The Fatal Minute   (18 min 1s)
#Juggernaut  (16 min 18s)
#Unmasked  (16 min 19s)
#Sky Pirates  (16 min 54s)
#The Fatal Shot  (16 min 32s)
#Burning Embers  (15 min 30s)
#Plunge of Peril  (17 min 10s)
#Tunnel of Terror  (17 min 07s)
#Trapped  (17 min 23s)
#Right of Way  (15 min 47s)
#Retribution  (15 min 47s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 219–220
 | chapter = Filmography
 }} 

==Clffhangers==
#Death from the Sky: Zorro, Joyce and Philip, aboard a train, are bombed from the air by El Lobo.
#The Fatal Minute: Knocked unconscious in a warehouse, Zorro is caught in the detonation of a hidden bomb.
#Juggernaut: Zorros foot is caught in the tracks of a railroad, helpless before an oncoming Express Train.
#Unmasked: Under cover of his heavies guns, El Lobo reaches to remove Zorros mask.
#Sky Pirates: Zorros plane comes under fire as it taxies for takeoff.
#The Fatal Shot: Fighting Trelliger, Zorro falls to the courtyard.  El Lobo pulls a gun on the prone vigilante.
#Burning Embers: Zorro is caught in a burning building when the floor gives way beneath him. funicualr railway, Zorro plummets down a cliff.
#Tunnel of Terror: Zorro is trapped atop the carriage of a train as it enters a tunnel - which explodes.
#Trapped: In a rooftop chase, Zorro loses his balance and falls from the skyscraper.
#Right of Way: Zorro, in a truck, is set for a collision with El Lobo, in a train.

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 