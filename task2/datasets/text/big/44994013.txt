Sandy (film)
{{Infobox film
| name           = Sandy
| image          =
| alt            = 
| caption        = 
| director       = George Melford
| producer       = Jesse L. Lasky
| screenplay     = Alice Hegan Rice Edith M. Kennedy 
| starring       = Jack Pickford Louise Huff James Neill Edythe Chapman Julia Faye George Beranger
| music          = 
| cinematography = Paul P. Perry 
| editor         =
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by George Melford and written by Alice Hegan Rice and Edith M. Kennedy. The film stars Jack Pickford, Louise Huff, James Neill, Edythe Chapman, Julia Faye and George Beranger. The film was released on July 14, 1918, by Paramount Pictures.  

==Plot==
 

==Cast==
*Jack Pickford as Sandy Kilday
*Louise Huff as Ruth Nelson
*James Neill as Judge Hollis
*Edythe Chapman as Mrs. Hollis
*Julia Faye as Annette Fenton
*George Beranger as Carter Nelson 
*Raymond Hatton as Ricks Wilson
*Clarence Geldart as Dr. Fenton
*Louise Hutchinson as Aunt Nelson Jennie Lee as Aunt Melvy
*J. Parks Jones as Jimmy Reed
*Don Likes as Sid Gray

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 