Garibon Ka Daata
{{Infobox film
 | name = Garibon Ka Daata
 | image = GaribonKaDaata.jpg
 | caption = VCD Cover
 | director = Ramesh Ahuja
 | producer = Rajiv Kumar
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Bhanupriya Sumeet Saigal Kader Khan Prem Chopra Divya Rana Aruna Irani Shakti Kapoor Lalita Pawar Asrani Dhananjay Singh
 | music = Bappi Lahiri
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =   May 26, 1989 (India)
 | runtime = 135 min.
 | language = Hindi Rs 5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1989 Hindi Indian feature directed by Ramesh Ahuja, starring Mithun Chakraborty, Bhanupriya, Sumeet Saigal, Kader Khan, Prem Chopra, Divya Rana, Aruna Irani, Shakti Kapoor, Lalita Pawar, Asrani and Dhananjay Singh

==Plot==

Garibon Ka Daata is an action film with  Mithun Chakraborty and Sumeet Saigal playing the lead roles, supported by Bhanupriya, Kader Khan, Prem Chopra and Shakti Kapoor.

==Cast==
*Mithun Chakraborty
*Bhanupriya
*Sumeet Saigal
*Kader Khan
*Prem Chopra
*Divya Rana
*Aruna Irani
*Shakti Kapoor
*Lalita Pawar
*Asrani
*Dhananjay Singh

==References==
* http://www.bollywoodhungama.com/movies/cast/5275/index.html
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Garibon+Ka+Daata -

==External links==
*  

 
 
 
 

 