The Red Sense
 

{{Infobox film
| name           = The Red Sense
| image          = Redsense.jpg
| caption        = One of eight theatrical posters
| director       = Tim Pek
| producer       = Tim Pek
| writer         = Rithy Dourg Tim Pek
| starring       = Sarina Luy Kaply Mon
| music          = Robert John Sky
| cinematography =
| editing        =
| distributor    = Transparent Pictures
| released       =  
| runtime        = 80 minutes
| country        = Cambodia Australia Khmer English
| budget         =
}}
 
 
 thriller film. Bokator and Office of Film and Literature Classification of Cinema of Australia|Australia. 

==Plot==
The film is a mystery story set in Cambodia in the Khmer Rouge era.

==Cast==
* Rithy Dourng   
* Niki Davis   
* Narith Eng   
* Sarina Luy   
* Kaply Mon   
* Vicheth Muy   
* Paly Net

==Production==
In 2004 Khmer graphic designer Tim Pek took a small role for a short film called Chhay. The film was about two brothers and focused on the pre- and post-Khmer Rouge era. Exposed for the first time to how films were produced, Tim started to contemplate various ways to improve the concepts of film making.

A few months later, Tim received a Cambodian Fashion Show video from Adelaide with an extra feature of a five minutes short film, directed by Chhai Thach. This film, entitled ‘Bugger!’, was made in New Zealand and had won the best short film award. Impressed with the simple story, Tim got in contact with Chhai and discussed the making of a new short film as a team. A month later, Chhai came up with another mystery story, also set during the Khmer Rouge era. After some discussion, both Tim and Chhai agreed to make three parts of the story. Tim managed to produce a script within a few weeks for a 15-minute short film intended for entry into film festivals.

The film project was created in early 2005 and became ‘The Red Sense’, or ‘Vignean Krohom’ in Khmer. Pek said of the film: "I had the opportunity to work with various non-actors which I thought would be a terrific opportunity as a director. The casts and crews are very supportive, which continues to keep my spirit high". 

==Release==

===Premier===
Audiences were given a preview of the film in Adelaide, Australia. Its official release, however, was in Melbourne on 3 August 2008, with special guest Mr Yuhorn Chea, the former mayor of the City of Greater Dandenong. There were also interviews with several media outlets.  

The film was banned for release in Cambodia as the Cambodian Government does not allow films pertaining to the Khmer Rouge. This has become an issue among the films fans: " I really want to watch the Red Sense. I have heard of it for a long time, and I have watched the trailer. It seems interesting. Some people told me that it’s not the same as other Khmer Rouge movies, filmed in the past."said a Khmer monk now living abroad, while a foreigner working in the Khmer film industry said, "It’s a shame that Tim can’t get the film screened over here, but then this is the country where the Khmer Rouge period has been erased from school text books." 

Despite its being banned, the film was screened in Siem Reap during the second CamboFest, Cambodia Film Festival|Cambofest. 

==Reception==
In Angkorian Society, Hemara In said, "Tim Peks film is very promising. He has been through the Pol Pot, Khmer Rouge and understands how this effect him, very proud of Tim Pek and a great Cambodian community here will support him and would like to wish Tim Pek a great future" (sic) while two critics of Anonymous, Chris & Margot, stated that "The emotionals were there and come from the heart. It deserves all the praise that it gets" Says Chris." The Khmer Rouge result was some consequences and I congratulate Tim Pek on The Red Sense." Says Margot. Clayton MP, Mr. Hong Lim also described the film to positive reviews. He said: "The way he approaches the film is very opportunistic, the way that Tim portrays comes straight from the human heart. These story brings out the Khmer Rouge quite well, the way he put in with the past and present. All Cambodian communities should be very proud of this film". 

In the Adelaide premier, the film also received A reviews. A film critic, Paul Millson, said: "Many people were very happy that Tim made a film for the thousands of family members that want this story to be told and shown all around the world for all to see". 

===Awards and nominations===
As well as these reviews, the film received awards at CamboFest film and video festival in Siem Reap, Cambodia. It was named the Golden Buffalo winner as the best local film. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 