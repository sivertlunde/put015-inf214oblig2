Holi (1984 film)
{{Infobox film
| name           = Holi
| image          = 
| image_size     = 
| caption        = 
| director       = Ketan Mehta
| producer       = Pradeep Uppoor
| writer         = Mahesh Elkunchwar (Play & screenplay)  Ketan Mehta
| narrator       = 
| starring       = Aamir Khan Om Puri Naseeruddin Shah Ashutosh Gowariker Raj Zutshi
| music          = Rajat Dholakia
| cinematography = Jehangir Choudhary    
| editing        = Subhash Sehgal    
| distributor    = 
| released       = 1984
| runtime        = 120 minutes
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
|}}
 coming of age drama film directed by Ketan Mehta, whose socially conscious work has been compared to American director Spike Lee.  It is based on eponymous play by Marathi writer, Mahesh Elkunchwar.  The film starred Aamir Khan, Ashutosh Gowariker, Om Puri, Shreeram Lagoo, Deepti Naval and Naseeruddin Shah.

==Plot summary==
In a typical college in a typical Indian city, the hostel boys Madan Sharma (Aamir Khan) and his friends including Ranjeet Prakash (Ashutosh Gowariker) are a rowdy and troublesome lot. But on one day, when Madan and his friends find out there will not be a holiday for them on the day of Holi, the festival of colors, the boys decide not to attend classes. 

The hostel superintendent Professor Singh (Naseeruddin Shah), the only lecturer with some links with the students, watches with apprehension their growing restlessness. A notice announcing a further postponement of examinations adds to the bitterness. A fight erupts out of nowhere between principal Phandes (Om Puri) nephew and another student; the principals nephew is hurt and the other boy is promptly rusticated. This is seen as a drastic punishment, and the news spreads like wildfire to all the students of the college. Resistance is organized in the library, in the laboratory, in the classrooms and the college grounds as the students rebel against the principal.

==Production information==
Holi was shot mainly on the campus of the Film and Television Institute of India (FTII), Pune. The iconic FTII banyan tree had just fallen and Ketan Mehta, who had studied at FTII, improvised and picturised a song with the main characters on the tree singing about the falling of a symbolic system. This film was a part of student project.

==Cast==
* Aamir Khan as Madan Sharma
* Ashutosh Gowariker
* Om Puri
* Shreeram Lagoo
* Deepti Naval as Professor Sehgal
* Naseeruddin Shah as Professor Singh
* Paresh Rawal
* Mohan Gokhale
* Rajendranath Zutshi
* Dina Pathak
* Kitu Gidwani
* Himanshu Trivedi

==Awards==
* 1985: National Film Award for Best Cinematography: Jehangir Choudhary 

==References==
 

==External links==
* 
*   on Passion for Cinema.

 
 
 
 
 
 
 