Blood Diner
{{Infobox Film
| name           = Blood Diner
| image          = Blood Diner Movie.jpg
| image_size     = 
| caption        = "First They Greet You... Then They Eat You"
| director       = Jackie Kong
| producer       = Lawrence Kasanoff Jackie Kong Jimmy Maslon Ellen Steloff
| writer         = Michael Sonye
| narrator       = 
| starring       = Rick Burks Carl Crew Roger Dauer LaNette LaFrance Lisa Guggenheim
| music          = Don Preston
| cinematography = Jurg Walther
| editing        = Thomas Meshelski & Marc Grimpe
| distributor    = Lightning Pictures
| released       = July 10, 1987
| runtime        = 88 Min.
| country        = USA
| language       = English
| budget         = Unknown
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 black Horror comedy horror film, a loose sequel to the 1963 film Blood Feast. The movie was written by Michael Sonye and directed by Jackie Kong.  

==Plot== brainwashed by their serial killer uncle Anwar Namtut (Drew Godderis) into completing his task of resurrecting the Ancient Lumerian goddess Sheetar (Tanya Papanicolas). Their mission is given to them once they resurrect him from his grave. Anwar Namtut is from then on a brain in a mason jar that commands the brothers. In order to complete their mission, the brothers must collect different body parts from many immoral women, stitch them together, and then call forth the goddess at a "blood buffet" with a virgin to sacrifice ready for her to eat. The brothers choose women for their "blood buffet" from those that enter into their wildly popular vegetarian restaurant. Meanwhile, two mismatched detectives (LaNette LaFrance and Roger Dauer) work together to try to track them down before more carnage can ensue.

==Cast==
*Rick Burks as Michael Tutman
*Carl Crew as George Tutman
*LaNette LaFrance as Sheba Jackson
*Roger Dauer as Mark Shepard
*Lisa Guggenheim as Connie Stanton
*Max Morris as Chief Miller
*Roxanne Cybelle as Little Michael
*Sir Rodenheaver as Little George
*Drew Godderis as Anwar Namtut
*Tanya Papanicolas as Sheetar/Bitsy
*Michael Barton as Vitamin
*John Barton Shields as Little Jimmy Hitler
*Effie Bilbrey as Peggy
*Karen Hazelwood as Babs
*Bob Loya as Stan Saldin
*Alisa Alvarez-Wood as Aerobic Girl
*Al Davis as Blonde Dancer

==Release==
The film was given a limited release theatrically in the United States by Lightning Pictures Inc. in July 1987.  It was released on VHS the same year by Vestron Video. 

The film has been released on DVD by Lions Gate. 

==Reception==
The film on IMDB currently has 5.0 based on 2,200 users.  The Film has a 55% audience score on Rotten Tomatoes.  Also the horror review site Horrornews.net gave it a positive review, stating that "I had a blast watching Blood Diner. It is funny, full of some excellent death scenes, and is just a lot of fun to watch in general."" 

==References==
 

==External links==
* 
*  
*  at Rotten Tomatoes

 
 
 
 
 
 


 