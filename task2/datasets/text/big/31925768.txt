The Unfortunate Policeman
{{Infobox film
| name           = The Unfortunate Policeman
| image          =
| image_size     = 
| caption        = 
| director       = 
| producer       = Robert W. Paul
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Pauls Animatograph Works
| distributor    =
| released       =  
| runtime        = 3 mins 37 secs
| country        = United Kingdom Silent
| budget         =
}}
 1905 UK|British short  silent comedy film, produced by Robert W. Paul, featuring a policeman chasing a young painter after he tips a pot of paint over him. The film is an, "elaborate chase comedy," which according to Michael Brooke of BFI Screenonline, "is an example of the increasing use of real locations in R.W. Pauls work." Film historian Ian Christie adds, "the irreverent and disrespectful treatment of the policeman would soon become impossible in British films, thanks to the notorious list of proscriptions laid down by the British Board of Film Censors shortly after its creation in 1912."   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 