Black-Eyed Susan (2004 film)
{{Infobox film
| name = Black-Eyed Susan
| image = Black-eyed-susan-by-james-riffel.gif
| caption = Theatrical release poster
| director = James Riffel
| producer = James Riffel (producer) John Conte, Terry Fearon, Terry Fearon, David Sperling (associate producers)
| writer = James Riffel
| starring = 
| music = John Conte
| cinematography = David Sperling
| editing = Rob Forlenza   Jerry Heer
| distributor = Blue Crash Film
| released = 
| runtime = 
| country = United States
| awards = English
| budget = 
| preceded_by =
| followed_by =
}}
Black-Eyed Susan was a 2004 drama film directed by James Riffel. 

==Synopsis==
With the subtitle Theres No Such Thing as a Perfect Plan, it tells the story of four young men who rob an apartment. Everything goes as planned until the "relatives" of the mysterious foreigner they robbed show up.

==Screenings and awards==
The premiere was on 26 March 2004 during the Garden State Film Festival and was screened again on 14 October 2004 during Sometime in October Film Festival.

The film received in 2004 the Jury Award during the Garden State Film Festival held in Asbury Park, New Jersey, USA. 

==References==
 

==External links==
* 

 
 
 
 
 

 