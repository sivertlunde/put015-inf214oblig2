Bride 13
 
{{Infobox film
| name           = Bride 13
| image          = Bride 13 Episode 9 Hurled from the clouds (1920 film poster).jpg
| caption        = Film poster
| director       = Richard Stanton William Fox
| writer         = Edward Sedgwick E. Lloyd Sheldon
| starring       = Marguerite Clayton John B. OBrien
| cinematography =
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 15 episodes
| country        = United States Silent English intertitles
| budget         =
}}
 thriller film serial directed by Richard Stanton. The film is considered to be lost film|lost.   

==Cast==
* Marguerite Clayton as Bride 13
* John B. OBrien as Lt. Bob Norton
* Gretchen Hartman as Zara
* Arthur Earle
* Lyster Chambers as Stephen Winthrop
* Mary Christensen
* Justine Holland as Bride 2
* Dorothy Langley as Bride 1
* Mary Ellen Capers as Bride 8
* Martha McKay as Bride 5
* Helen Johnson as Bride 6
* Leona Clayton as Bride 4
* Florence Mills as Bride 9
* W. E. Lawrence (as William Lawrence)

==Plot==
 

==Promotion==
From an ad for the film:
"Do you want to be thrilled as you never have been thrilled since as a boy or girl you first read Jules Verne, Dumas, Poe or Conan Doyle?

If you do, dont miss the first or any succeeding episodes of BRIDE 13. Beginning with the abduction for ransom of wealthy brides by a cutthroat band of submarine pirates from Tripoli, carrying you from palatial homes to the sun-scorched sands of Northern Africa. Bride 13 piles crisis upon crisis, climax upon climax, thrill upon thrill.

Each episode leaves you feeling that you could not endure the excitement of another reel, yet in tremendous suspense to know what happens next.

Si stupendous is the situation created by the plot of Bride 13 that it could only be solved by the most powerful actor in the world!

OUR NAVY, with its dreadnoughts, destroyers, submarines, seaplanes, blimps, officers and men, is one of the most important actors in Bride 13, through the special courtesy of the Government.

The fierce combats on sea and land, the pursuits by sea, air and land, and hundreds of other incidents are made absolutely realistic because enacted by genuine naval officers, sailors and marines."  

==See also==
* List of American films of 1920
* List of film serials
* List of film serials by studio
* List of lost films

==References==
This article cites public domain text published in 1920, as referenced below.
 

==External links==
* 

 
 
 
 
 
 


 
 