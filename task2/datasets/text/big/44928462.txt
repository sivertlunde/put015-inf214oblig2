Jil (film)
{{Infobox film
| name           = Jil
| image          = File:Jill_Telugu_Poster.png.jpg
| caption        = Jill Telugu Poster
| director       = Radha Krishna
| producer       = Vamsi Krishna Reddy  Pramod Uppalapati
| writer         = Radha Krishna  
| narrater       =
| story          = Radha Krishna Gopichand Rashi Khanna
| music          = Mohamaad Ghibran
| cinematography = Sakthi Saravanan
| editing        = Kotagiri Venkateshwara Rao
| studio         = UV Creations
| distributor    = Lorgan Entertainment   
| released       =   
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}} Telugu film Gopichand and Rashi Khanna played the lead roles.   
==Plot summary==
The story follows Jai, a firefighter, who becomes the target of a criminal gang because they think a member has exposed them in his dieing words to Jai.
==Cast== Gopichand as Jai
* Rashi Khanna as Savithri
* Chalapathi Rao as Jais uncle
* Posani Krishna Murali
* Srinivas Avasarala as Ajay
* Kabir Duhan Singh  as Chota Naik Urvashi as  Jais aunt
* Brahmaji as Ranganath
*  Bharat Reddy as Ali 
* Supreet Reddy as Ghora
* Prabhas Seenu

==Promotion and release==
The first look teaser of the film was released on March 6, 2015 occasion of festival Holi.  The movie was released on March 27.   

==Reception==
idlebrain.com gave a review rated it 3 out of 5 and stated that first half of the film is nice compared to the second half. It identified  technical standards, production values,  and songs as being strong components. 
The Times of India states that the film  " flip-flops from being drab to extremely violent"   

==Soundtrack==
Music has been composed by Ghibran and the audio released on March 12, 2015.
*01 – Man on Fire – Yazin Nizar & Bianca Gomes
*02 – Jil Jil Jil – Yazin Nizar & Shalmali Kholgade
*03 – Swing Swing Swing – Blaaze & Sangita Santhosham
*04 – Emaindi Vela – Clinton Cerejo & Sharanya Gopinath
*05 – Pori Masala Pori – Nivin Bedford & Sharanya Gopinath

==References==
 

==External links==
*  

 
 
 
 


 