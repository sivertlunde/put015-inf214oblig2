Anna Lucasta (1959 film)
{{Infobox film
| name           = Anna Lucasta 
| image          = 
| image_size     = 
| caption        = 
| director       = Arnold Laven
| writer         = Philip Yordan
| producer       = Sidney Harmon
| starring       = Eartha Kitt Sammy Davis Jr. Henry Scott
| music          = Elmer Bernstein  (Lee Osborne music editor) 
| cinematography = Lucien Ballard Robert Lawrence
| studio         = Longridge Enterprises
| distributor    = United Artists
| released       =     February 1959  (general release)  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| website        = 
| amg_id         = 
}} 1949 version stage play.

==Plot==
At the family home in Los Angeles, patriarch Joe Lucasta learns that his friend Otis has sold a farm, distributing the proceeds to his children.  Otis son Rudolph is bringing his share to California, where Otis hopes that Joe can find a wife for Rudolph.   Joes son Stanley and son-in-law Frank hope to get the money by being the ones to find Rudolph a wife. Though uninterested in the money, Joes wife Theresa suggests youngest daughter Anna, whom Joe put out of the house some time earlier.  Theresa believes Anna is good and sees a chance for her to get a fresh start.  Stanley, Frank and Franks wife Stella regard Anna as a "slut", but will try to make her seem respectable to deceive Rudolph.  Joe objects to their designs on Rudolphs money and to Anna returning, as he also holds her in low regard.  Eventually, Frank browbeats Joe into going to Annas last known whereabouts, a dockside cafe in San Diego, to bring her home.

The cafe is Annas regular haunt, as she leads a day-to-day existence seducing sailors for meals, drinks and board.  As the cafe is closing, she has no place to spend the night.  Just then, Danny Johnson arrives looking to renew his acquaintance with Anna.  His naval enlistment ending, he asks her to move in with him, though hes uninterested in marriage.  As Anna parties with Danny, Joe arrives and asks her to come home. Anna consents, but after her arrival, Joe angrily rebuffs her efforts to rebuild their relationship.  

Soon thereafter, Rudolph arrives in town.  Having expected Rudolph to be a rube, Frank is disappointed to discover that hes a college graduate, unlikely to be fooled by Annas guise of respectability.  Furthermore, Rudolphs only interest is getting a job, the search for a wife having been more his fathers concern than his own.  When Rudolph meets Anna, however, hes smitten and they begin a romance.  Anna tells him of her ejection from the household -- Joe had become angrily jealous when boys took interest in her, so he falsely accused her of promiscuity and threw her out -- and confesses the truth of her life in San Diego.  Rudolph says that he still loves her, and Anna agrees to marry him.

As Anna is alone in the family home after the wedding, Danny arrives, responding to her letter asking him to come. She informs him that after sending the letter, she fell in love and got married.  Danny protests, but Anna is insistent and he surrenders. As Danny is leaving, Joe comes home, declaring his intention to break up Annas relationship with Rudolph.  Anna begs Joe to allow her to begin a new life and be happy.  Danny sticks up for Anna, offering to beat up Joe to prevent him from disrupting her marriage.  But Joe says that hes ruined Rudolphs job opportunity by informing the employer about Annas past.  He says that hell do the same any place Rudolph seeks work.  As Anna breaks down into tears and Joe flies into a rage, Danny pulls Anna away and takes her out of the house.

Danny and Anna head back to San Diego and spend days partying before deciding to move to Brazil.  Danny needs time to raise money for ships passage, but Anna wants to leave immediately and remembers she has money at the Lucasta home.  When she and Danny return to retrieve it while the family is at church, they find Joe on his deathbed.  While Danny goes to call a doctor, Anna stays with Joe as he deliriously addresses her as though she were a little girl, calling her his "angel", as he had before their relationship soured.  Joe implores Jesus to watch over Anna, who is so overwhelmed that shes unable to respond to him before he dies.

Danny observes Anna weep inconsolably over Joe and looks at a joyful picture of the two of them during Annas childhood.  He then quietly exits the house.  As hes about to drive away, he smiles upon seeing the family, including Rudolph, returning from church.  As Danny drives off, Rudolph notices him and realizes that Anna is in the house.  He excitedly rushes past the others and runs inside.

==Cast==
*Eartha Kitt as Anna Lucasta
*Sammy Davis Jr. as Danny Johnson
*Frederick ONeal as Frank Henry Scott as Rudolph Slocum Rex Ingram as Joe Lucasta
*Alvin Childress as Noah James Edwards as Eddie
*Rosetta LeNoire as Stella
*Isabelle Cooley as Katie Lucasta
*Georgia Burke as Theresa Lucasta
*Claire Leyba as Blanch John Proctor as Stanley Lucasta
*Charles Swain as Lester Isaac Jones as  Police officer
*Wally Earl as Secretary
*Eileen Harley

==Production== 
The film was shot at Samuel Goldwyn Studio from early May through early June 1958. 

==Reception==
According to Robert Osborne, the film was unsuccessful at the box office after United Artists gave it little promotion and only a limited release. 

==Film credits==
Additional film credits: 
*Music: Elmer Bernstein (composer); Sammy Cahn (composer); Lee Osborne (music editor)
*Art Director: John S. Poplin Jr.
*Graphic Artist: Charles White
*Visual Effects: Jack Rabin and Louis DeWitt (special photography effects); Irving Lerner (montage conceived by)
*Make up: Ted Coodley (makeup artist) and Helene Parrish (hairstylist)
*Costume-wardrobe: Virginia Dey (wardrobe stylist); Sophia Stutz (women’s wardrobe); Norman Martien (men’s wardrobe)
*Art Department: Lyle B. Reifsnyder (set dresser); Richard Rubin (prop master)
*Sound: Jack Solomon
*Film Production (main): Leon Chooluck (production supervisor); James Yarbrough (script supervisor)

==References==
 
* 
* 

==External links==
* 

 

 
 
 