Crossed Swords (1977 film)
{{Infobox film
| name           = Crossed Swords
| image_size     = 
| image	=	Crossed Swords FilmPoster.jpeg
| caption        = 
| director       = Richard Fleischer
| producer       = Pierre Spengler
| writer         = Mark Twain (novel) Berta Dominguez D. &  Pierre Spengler (original screenplay) George MacDonald Fraser (final screenplay)
| narrator       = 
| starring       = Mark Lester Ernest Borgnine Oliver Reed Raquel Welch Rex Harrison
| music          = Maurice Jarre
| cinematography = Jack Cardiff
| editing        = Ernest Walter
| studio         = 
| distributor    = Warner Bros. (USA)
| released       = 3 June 1977 (London) March 17, 1978 (US)
| runtime        = 113 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Crossed Swords (  adventure film directed by Richard Fleischer, based on The Prince and the Pauper by Mark Twain. It stars Mark Lester, Oliver Reed, Ernest Borgnine, Raquel Welch and Sir Rex Harrison.

The film was notable for featuring an all star cast of British and American actors in the supporting parts.

==Plot== The Duke of Norfolk (Sir Rex Harrison) to be arrested during the masked ball that evening.

In his royal chamber, Prince Edward of Wales (Mark Lester) insists on not wearing a costume to the masked ball and his dressers leave him. Tom falls down the chimney into the chamber and Edward demands to know who he is. Tom introduces himself and explains his situation. Interested that Tom looks like him, Edward decides they swap appearances and clothes to attend the masked ball, but adds that the Princes Seal stays with the true Prince. However, Edward is mistaken for Tom by The Duke of Norfolk, who orders for him to be escorted out of the palace. Outside, Edward is rescued by skilled swordsman Sir Miles Hendon (Oliver Reed). At the masked ball, The Duke of Norfolk is arrested and Henry VIII and the guests laugh at Toms dancing, despite Tom repeatedly claiming that hes not the Prince of Wales.

Meanwhile, despite Edward repeatedly claiming that he is the Prince of Wales, Miles says he believes him, but actually doesnt and takes him to John Canty where Edward discovers what Toms life is like. But when John attempts to beat Edward, Miles intervenes and a fight breaks out, resulting in John pushing Miles off a roof into a stream. John is declared a murderer and he flees London with Edward. At the castle, Henry VIII has fallen ill since the morning after the masked ball and he orders for no one to declare that Tom is not the Prince, not even Tom himself, though some of the lords and ladys do notice changes in the Prince, but they dismiss it as a brief phase. During a banquet, Henry VIII dies in his royal chamber and Tom commands that The Duke of Norfolk shall not die.

In some woods, John and Edward are escorted by some unfriendly men to an underground cavern where Rufflers (George C. Scott) gang hide out. However, they are unfriendly and reveal that word has reached them of Henry VIIIs death. Edward discovers how many of the gang members are former nobles who were driven out of London by Henry VIIIs rule, and says he will give back what was taken from them. One of the gang members challenges Edward to a flight, which Ruffler takes great interest in watching but John is confused as to how his "son" became such a good fighter. Edward wins and leaves the cavern. John goes after him to beat him again, but is killed by another member of Rufflers gang. Outside, Edward meets Miles, having survived his fall. Miles takes Edward to Hendon Hall, where Miles is outraged to discover his brother Hugh Hendon (David Hemmings) has married Miles love interest Lady Edith (Raquel Welch) and taken Hendon Hall for himself. Hugh has Miles and Edward captured, but Edith helps them escape and Edward convinces Miles that he (Edward) really is the rightful King and offers to restore him to his honour as a Knight.
 Princess Elizabeth (Lalla Ward), though not originally heir to the throne, becomes Queen in her own right at the age of 25 and rules for 45 years. As she had promised earlier in the film, "She took good care of England."

==Cast==
*Oliver Reed as Sir Miles Hendon
*Raquel Welch as Lady Edith Prince Edward / Tom Canty
*Charlton Heston as Henry VIII
*Ernest Borgnine as John Canty
*George C. Scott as The Ruffler The Duke of Norfolk
*David Hemmings as Hugh Hendon Hertford
*Julian Orchard as St. John
*Murray Melvin as Princes Dresser Princess Elizabeth
*Felicity Dean as Lady Jane Grey
*Sybil Danning as Mother Canty
*Graham Stark as Jester
*Preston Lockwood as Father Andrew
*Arthur Hewlett as Fat Man
*Tommy Wright as Constable
*Harry Fowler as Nipper Archbishop Cranmer
*Don Henderson as Burly Ruffian
*Dudley Sutton as Hodge
*Ruth Madoc as Moll

==Production==
George MacDonald Fraser was hired to rewrite the script. Shooting took place in France. George MacDonald Fraser, The Lights On at Signpost, HarperCollins 2002 p26-43 

==Reception==
The movie did not do well in the theaters. In fact, the critiques of his performance were so savage that Mark Lester gave up acting.

==See also==
*Cultural depictions of Henry VIII of England

==References==
 

==External links==
* 
* 
 
 
 

 
 
 
 
 
 
 
 
 
 
 