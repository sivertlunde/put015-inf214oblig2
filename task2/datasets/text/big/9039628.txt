Echo (2003 film)
{{Infobox film
| name           = Echo
| image          = Echoposter.jpg
| image size     = 
| alt            = 
| caption        = Film poster
| director       = Tom Oesch
| producer       = 
| writer         =
| screenplay     = 
| narrator       = 
| starring       = Haven Pell
| music          =
| cinematography = 
| scenography    = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2003
| runtime        =  
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Echo is a 2003 short film, written and directed by Tom Oesch, starring Haven Pell, Sean Le and Armand Kirshman. 

==Overview==
The film tells the story of a young American soldiers heartbreak, guilt and eventual madness while fighting in the jungle of Vietnam. 

It features black-and-white cinematography by Kevin Oeser and a film score by Sasha Ivanov. 

In 2004 Echo won at the "46th Rochester International Film Festival".

== References ==
* 
*  

 
 
 

 
 