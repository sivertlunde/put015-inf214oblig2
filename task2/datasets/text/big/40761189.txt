Le Guetteur
{{Infobox film
| name           = Le Guetteur (Il cecchino)
| image          = Le Guetteur.jpg
| caption        = 
| director       = Michele Placido
| producer       = Fabio Conversi
| writer         = Cédric Melon Denis Brusseaux
| starring       = Daniel Auteuil, Luca Argentero, Violante Placido
| music          = Nicolas Errèra
| cinematography = Arnaldo Catinari	
| editing        =
| distributor    = Babe Films of Rome
| released       =  
| runtime        = 89 min
| country        = France Italy
| language       = French and Italian
| budget         = 15 million €
| gross          =
}} Cannes Marché du Film.  The film was marketed as The Lookout in English-speaking territories.

== Synopsis ==
Mattei is about to arrest a gang of bankrobbers, when an unknown sniper shoots most of the policemen on site under his responsibility.

== Cast==
* Daniel Auteuil : Mattei
* Mathieu Kassovitz : Vincent Kaminski
* Olivier Gourmet : Franck Francis Renaud : Eric
* Nicolas Briançon : Meyer
* Luca Argentero : Nico
* Violante Placido : Anna
* Arly Jover : Kathy
* Christian Hecq : Gerfaut
* Sébastien Lagniez : Ryan
* Michele Placido : Giovanni
* Fanny Ardant : Giovannis wife
* Géraldine Martineau : Sonia

==References==
 

== External links ==
* 

 
 
 
 
 
 


 