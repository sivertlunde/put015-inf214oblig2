Penpuli
{{Infobox film
| name = Penpuli
| image =
| caption =
| director = Crossbelt Mani
| producer =
| writer = Jagathy NK Achari
| screenplay = Jagathy NK Achari
| starring = KPAC Lalitha Adoor Bhasi Unnimary Rajakokila
| music = G. Devarajan
| cinematography = EN Balakrishnan
| editing = Chakrapani
| studio = Rose Movies
| distributor = Rose Movies
| released =  
| country = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed Crossbelt Mani. The film stars KPAC Lalitha, Adoor Bhasi, Unnimary and Rajakokila in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*KPAC Lalitha 
*Adoor Bhasi 
*Unnimary 
*Rajakokila 
*Vijaya Vincent

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Mankombu Gopalakrishnan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Palliyarakkaavile || P. Madhuri || Mankombu Gopalakrishnan || 
|- 
| 2 || Raathri Raathri || P. Madhuri || Mankombu Gopalakrishnan || 
|- 
| 3 || Sahyaachalathile || Jolly Abraham, Karthikeyan || Mankombu Gopalakrishnan || 
|- 
| 4 || Varavarnnini || K. J. Yesudas || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 


 