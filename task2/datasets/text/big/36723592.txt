The Lifted Veil (film)
{{infobox film
| name           = The Lifted Veil
| image          = Ethel Barrymore c1916.png
| imagesize      =
| caption        = Ethel Barrymore, 1916.
| director       = George D. Baker
| producer       = B. A. Rolfe
| writer         = Basil King (novel: The Lifted Veil) Albert S. LeVino (scenario)
| starring       = Ethel Barrymore William B. Davidson Frank Gillmore
| music          =
| cinematography = Joseph Schelderfer
| editing        =
| distributor    = Metro Pictures
| released       = September 10, 1917
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}}
The Lifted Veil is a 1917 American silent film produced by B. A. Rolfe and distributed by  , an author popular with women readers. Stage star Ethel Barrymore, under contract to Metro, appears in her eighth silent film which is now lost film|lost.   

==Cast==
*Ethel Barrymore - Clorinda Gildersleeve
*Frank Gillmore - Reverend Arthur Bainbridge
*William B. Davidson - Malcolm Grant Robert Ellis - Leslie Palliser
*Ilean Hume - Pansy Wilde
*Maude Hill - Margaret Palliser
*Ricca Allen - Mrs. Scattergood
*Myra Brooks - Mrs. Wedlock(*as Myra Brooke)
*George Stevenson - Butler

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 