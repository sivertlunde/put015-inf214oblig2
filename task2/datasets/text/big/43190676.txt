I Am Eleven
{{Infobox film
| name           = I Am Eleven
| image          = I_Am_Eleven_theatrical_poster.jpg
| alt            = 
| caption        = Theatrical poster 
| director       = Geneviève Bailey
| producer       = Geneviève Bailey, Henrik Nordstrom
| writer         = 
| starring       = 
| music          = Nick Huggins 
| cinematography = Genevieve Bailey, Henrik Nordstrom
| editing        = Geneviève Bailey
| studio         = Proud Mother Pictures
| distributor    = 
| released       =  
| runtime        = 
| country        = Australia
| language       = English
| budget         = 
| gross          = US$387,950 (as of 19 Oct. 2014) 
}}
 7 Up documentary series and won awards in the USA, Australia, Brazil, France and Spain.

==Background==
Following the completion of a Bachelor Of Creative Arts Honours from the Victorian College of the Arts, Melbourne University in 2003, Bailey completed a series of short films, music videos and a video installation, while also tutoring at Melbourne University and Deakin University. Between 2003 and 2008, Bailey completed 45 projects, many of which are collaborations with fellow Australian filmmaker Jarrah Gurrie. In May 2008, Baileys work for I Am Eleven was categorised as "In Production", with a working title of "Eleven". 

==Production==
At the time that the film was conceived, Bailey was in a difficult period of her life and was creatively inspired by a particularly memorable time during her childhood years:

 
I was going through a difficult time, being in a serious car accident and my body was not feeling very happy, and my dad passed away. I wanted to make something quite simply that would make me happy and make audiences happy … I thought back to my favourite age in life and that was when I was eleven and I thought I wondered what would it be like to be eleven today.    

Bailey explained in 2014 that she commenced shooting the film without any funding: “I’d run out of money, come back and work two or three jobs to save up the money for another ticket. I was doing that every year ... It was like having an addiction.” Nearing the completion of the production, Bailey turned down funding that was offered by a Victoria, Australia organisation, as she was not comfortable with handing over the ownership of the film to the funding body: "We weren’t very comfortable with that given the amount of time, energy and money we’d invested." 

==Participants==
*Jiter - India
*Goh - Thailand
*Siham - Morocco
*Giorgi - Bulgaria
*Jack - Thailand
*Remi - France
*Billy - England
*Obey - England
*Oliver - USA
*Jamira - Australia
*Remya - India
*Rika - Japan
*Vandana - India
*Priya - India
*Dagan - USA
*Sree Kuti - India
*Sam - The Netherlands
*Sahin - Sweden
*Ginisha - India
*Luca - Germany
*Fang - China
*Osama - Sweden
*Kim - USA
*Grace - Czech Republic
*Sharif - England 

==Release==
The global premiere of I Am Eleven occurred at the 2011 Melbourne International Film Festival (MIFF), where the film also received the Peoples Choice Award.  Following its official release in July 2012, the documentary was screened at 22 cinemas around Australia, from 2012 to 2013, and played for 26 weeks at Melbourne, Australia’s Cinema Nova venue.    The film was featured as part of the 2014 Portland Childrens Film Festival that was held in early April in the American city of Portland, Maine. 

The "On Demand" rights for the film was acquired by the Gathr Films company in early June 2014. Gathr will commence distributing the film through its "Theatrical On Demand" platform in August 2014.   

==Critical reception==
Following a screening in Perth, Australia, Alexander Dermer wrote in a review for the Weekend Notes: "Some of what the kids say can be immensely thought provoking and at other times downright hilarious, however I Am Eleven thoroughly manages to keep you entertained either way. This film achieves what it sets out to do and more." Dermer also identified what he perceived as "odd moments of slow pacing and sidestepped direction", but concludes that the film is a "must see for everyone". 

Writing for Meld, a publication for international students that is based in Melbourne, Australia, Hieu Chau stated in an April 2013 review: "It offers up an engaging look at how children of different cultural backgrounds grow up in the world that they inhabit today ... it’s a stunning and successful effort for the documentary, especially when one considers that it had very little funding and no distributor." 

The documentary was identified as a New York Times "Critics Pic" in Neil Genzlingers September 2014 review, in which he explains: "The project could easily have seemed like a smarmy Nickelodeon special or some variant of Kids Say the Darndest Things, but Ms. Bailey’s willingness to let the children talk and to let the viewer impose broader meaning elevates it." 

==Awards==
*Outstanding Documentary Award at the Newport Beach Film Festival, USA
*Audience Award at the São Paulo International Film Festival, Brazil
*Peoples Choice Award at Melbourne International Film Festival, Australia
*Best Documentary of the Year at the IF Awards, Australia
*Audience Award at the European Independent Film Festival, France
*Best Documentary of The Year Nominee by the Film Critics Circle of Australia
*Independent Spirit Award at the IF Awards, Australia
*Best Documentary nominee at the Seminci Valladolid Film Festival, Spain 

==See also==
*Documentary
*Independent film
*List of film festivals

==References==
 

==External links==
* 

 
 