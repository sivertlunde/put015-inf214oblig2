10 Promises to My Dog
{{Infobox film
| name           = 10 Promises to My Dog
| image          = 10 Promises to My Dog film poster.jpg
| alt            = 
| caption        = Japanese film poster 
| writer         = {{Plainlist|
* Hare Kawaguchi
* Yoshimitsu Sawamoto
}}| screenplay     = 
| story          = Hare Kawaguchi
| narrator       = 
| starring       = {{Plainlist|
* Rena Tanaka
* Mayuko Fukuda
* Etsushi Toyokawa
}}
| music          = Chu Sosou
| cinematography = 
| editing        = Sao Kawasei
| studio         = 
| director       = Katsuhide Motoki
| producer       = 
| distributor    = Shochiku
| released       =  
| runtime        = 117 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = US Dollar|US$15,332,225   
}}
  is a 2008 Japanese film. The film was directed by Katsuhide Motoki, and stars Rena Tanaka, Mayuko Fukuda and Etsushi Toyokawa.  Rena Tanaka and Mayuko Fukuda play the adult and the young version of Akari Saito respectively, while actor Etsushi Toyokawa stars as Akaris father. 

Based on a novel by Hare Kawaguchi, this film tells the story of Akari Saito and her dog named Socks.    The pair supported each other as they grew up together.  This story is closely tied to "The Ten Commandments of Dog Ownership", a list of ownership rules written from a dogs point of view. 

10 Promises to My Dog was released in the Japanese box office on 15 March 2008.    The film grossed a total of US Dollar|US$15,332,225 in 6 countries,  and was the 19th-highest grossing Japanese film of 2008.   

==Plot==
Akari is neglected by her father, a top surgeon who works long hours and puts his career before family. Her mother is hospitalized because of an incurable disease. Therefore, Akari longs for a dog who can keep her company. One day, a Golden Retriever puppy unexpectedly appears in the garden of Akaris house, and Akari immediately decides to adopt it. At her mothers suggestion, Akari christens it "Socks", because the puppys white paws made it look like it was wearing white socks. Her mother also made Akari promise that she will follow the The Ten Commandments of Dog Ownership when she takes care of Socks. Akari also has a friend, Susumu, who is groomed by his family to become a professional guitarist. Susumu soon forms an attachment with Socks as well.

A few months later, Akaris mother died, and Akari grieves over her death for two days. After that, her neck became very stiff and she is unable to move it. With Socks help, she discovers that this stiffness is actually due to her own imagination, and is thus "cured". She also finds out that Socks was actually placed in the garden by her mother. Not long after Akaris mother died, the family moves to Sapporo because her father was given a lecturer post at a university there. However, they cannot bring Socks along because their dormitory does not allow pets. Hence, Akari has to reluctantly entrust Socks to the care of Susumu.

Another problem surfaced when Susumu was accepted into a prestigious music school in Paris. On the day that he was leaving, Akaris father, who was supposed to be on leave that day, was suddenly called back to the hospital to do an "emergency" operation. This caused Akari to be late in seeing Susumu off. The "emergency" operation turned out to be a minor one, and he resigned after feeling guilty about disappointing his daughter. The family later moved back to their old home in Hakodate, and Socks came back to stay with them. Akaris father later set up a clinic in the home, which proved to be popular with the locals.

10 years later, Akari is a university student studying to become a vet. By chance, she happens to see a poster advertising Susumus upcoming performance in the city. The pair had a tearful reunion, and they soon started dating. During this time, she starts to feel that Socks is a constrain to her. She starts to bemoan the sacrifices that she have to make because of Socks. After Akari graduates from university, she becomes a zookeeper at Asahiyama Zoo. Akari seldom returns to her home, and neglects Socks in the process. However, she was reminded of the good friend Socks was again when it helped Susumu gain confidence to play the guitar again after his accident. 

As time passes, Socks starts to age and becomes weaker. Akari was shocked at how much weaker Socks looked on one of her rare visits home, and promises to visit it often. However, due to her heavy workload, she is unable to fulfill that promise. One day, her father phoned her urgently to say that Socks was dying. Managing to get away from her work, she rushes back just in time. As its energy saps away, Akari read the Ten Commandments of Dog Ownership again to see if she had done what she had promised 10 years ago.

After Sockss death, Akari and her father found long-lost photographs of Socks, and a letter from her mother. The letter tells Akari that Socks was meant to replace herself, though she added that Socks will not live as long as Akari. Akari was also reminded of the fact that her father sacrificed his career for her. Not long after, Akari and Susumu got married together.

==Cast==
* Rena Tanaka as the adult Akari Saito, a veterinarian at the Asahiyama Zoo.
* Mayuko Fukuda as the young Akari Saito.
* Etsushi Toyokawa as Yuichi Saito, Akaris father
* Shota Sato as the young Susumu Hoshi, Akaris childhood friend
* Ryo Kase as the adult Susumu Hoshi, a distinguished guitarist who later opens his own music school. Susumu later married Akari.
* Chizuru Ikewaki as Yuko Inoue, Akaris best friend in university. She later become a sales clerk at a departmental store.
* Reiko Takashima as Fumiko Saito, Akaris mother. She died due to an incurable disease.
* Akira Fuse as Shinichi Hoshi, Susumus father. He is a professional guitarist like his son.
* Akiko Aitsuki as Susumus mother
* Akane Osawa
* Takashi Sasano
* Pierre Taki as the zookeeper
* Fujii Mina as the passerby who takes a photo of Akari and her mother
* Hana Ebise

==Production==
===Development===
10 Promises to My Dog is based on a novel authored by Hare Kawaguchi.  This novel was in turn inspired by a set of rules entitled "The Ten Commandments of Dog Ownership". 

==Reception==
===Critical reception===
Kevin Ma, reviewer for Love HK Film.com, describes director Katsuhide Motokis approach to the story "very low-key and matter-of-fact", and added that "it almost feels like he decided to tell the story in a way that any director-for-hire would".    He also criticized some of the characters.  In particular, he said that Rena Tanaka "barely registers as the central character" and that "Ryo Kase fares even worse  , but mostly because of his poorly-written plot device of a character than his acting skills in general."  However, he did praise the child actors, saying that Mayuko Fukuda gives "the best performance as the child Akari".  He also praised the ending of the film, describing it as "emotionally powerful and yet brilliantly understated". 

== References ==
 

==External links==
*   on TV Tokyo  
*  
*  

 
 
 