Let's Be Cops
 
 
{{Infobox film
| name           = Lets Be Cops
| image          = Lets Be Cops poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Luke Greenfield
| producer       = Luke Greenfield Simon Kinberg
| writer         = Luke Greenfield Nicholas Thomas
| starring       = Jake Johnson Damon Wayans, Jr. Nina Dobrev Rob Riggle Keegan-Michael Key Andy García
| music          = Christophe Beck Jake Monaco
| cinematography = Daryn Okada
| editing        = Bill Pankow Jonathan Schwartz
| studio         = WideAwake
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 minutes  
| country        = United States
| language       = English
| budget         = $17 million 
| gross          = $137.6 million   
}} buddy cop pretend to be Los Angeles    police officers. Co-starring Nina Dobrev, Rob Riggle, and Keegan-Michael Key, the film was released on August 13, 2014.

==Plot==
Two longtime pals, Justin, a reject   by the time they were thirty, they would head back to their Columbus, Ohio|Columbus, Ohio hometown. While exiting a bar, their car is hit by a vehicle full of Albanians, who intimidate them into doing nothing.

Justin attempts to pitch a game about policemen but is dismissed by his employer. Later, Ryan convinces him to use the police uniforms from his presentation as costumes for their college reunion party. Upon attending, both are confronted with their failures and mutually accept to honor their pact. As they walk home, they are treated like real cops and decide to enjoy the gag. It allows Justin to finally get the attention of Josie, a waitress to whom he is attracted and who works at a local diner, Georgies.
 Albanian mobsters. Once more, the pair are intimidated into doing nothing.

Via Segars, Ryan obtains surveillance equipment to gather evidence and put Mossi away, along with an unidentified partner who has been investigating the pair. Ryan convinces Justin to do an undercover operation to obtain information on an incriminating shipment of crates. During the mission, they discover the crates full of SWAT equipment, along with secret tunnels in which they are shipped that run between Mossis club and Georgie’s restaurant. This necessitates the acquisition of the restaurant, explaining the blackmail. After a few close encounters, they barely escape. Fed up, Justin insists on mailing the evidence anonymously, but Ryan, finding purpose in his life again, is set on delivering it personally. They fight, and part ways.

Ryan brings his evidence to Segars, who recommends it go to the highest authority, which is Detective Brolin. Unfortunately, Brolin is actually Mossis partner. After instantly recognizing each other, Ryan makes it out of the station, but his sudden threat has blown their cover. Meanwhile, Justin decides to man up and, in uniform, assertively pitches his game again. One of Brolins officers shows up to try and kill him, inadvertently helping to sell the pitch. Ryan is abducted, and Mossi sends a threatening message to Justin. Overwhelmed, Justin pleas to Segars for help after admitting everything. He also confesses to Josie, which he had made previous attempts to do, and she disgustedly leaves him.

Justin goes into the tunnels alone while Ryan pits Mossi and Brolin against each other, prompting Mossi to shoot and kill the detective. Justin attempts to save his friend, but is overpowered. Segars arrives, causing Mossi and his crew to retreat. Segars admonishes the duo for their deception and orders them to leave before going after the mobsters without waiting for backup. Ryan and Justin agree they can’t abandon him, and suit up with the SWAT equipment. They save Segars, but he becomes incapacitated. The pair then face Mossi alone, during which the two reconcile. They fail to take him out, but luckily, Segars is able to show up and shoots Mossi in the back of the chest, saving Justin and Ryan.

Thanks to the respective confidence and motivation gained during their impersonations, Justin has become a successful game developer, while Ryan graduates from the police academy as a true, fully-fledged member of the LAPD. Justin apologizes to Josie, and after she forgives him, they rekindle their relationship. Ryan, however, still has not given up on their fun as cops together, and convinces Justin to don the fake uniform once again and join him on patrol.

==Cast==
  at a panel for the film at San Diego Comic-Con International in July 2014]]
 
* Damon Wayans, Jr. as Justin Miller
* Jake Johnson as Ryan OMalley 
* Rob Riggle as Patrol Officer Segars
* Nina Dobrev as Josie
* James DArcy as Mossi Kasic
* Keegan-Michael Key as Pupa
* Andy García as Detective Brolin
* Jon Lajoie as Todd Connors
* Tom Mardirosian as Georgie
* Natasha Leggero as Annie
* Rebecca Koon as Lydia
* Nelson Bonilla as Pasha
* Jeff Chase as Leka
* Jwaundace Candece as JaQuandae
* Briana Venskus as Precious
* Alec Rayme as Misha
* Ron Caldwell as Ron
 

==Production==
Principal photography began in May 2013 in Atlanta, Georgia (U.S. state)|Georgia, and wrapped up in July.  

==Release==
The film was released on August 13, 2014 with a rating of R in America and 12a in the UK. 

===Box office===
Lets Be Cops grossed $82.4 million in North America and $55.2 million in other territories for a total gross of $137.6 million, against its $17 million budget. 
 Teenage Mutant Guardians of the Galaxy ($25.1 million), but the first among the weekends new releases. 

===Critical reception===
Lets Be Cops received generally negative reviews from critics. On   the film has a score of 30 out of 100, based on 23 critics, indicating "generally unfavorable reviews". 

Critic David Palmer gave the film 7/10, stating that Johnson and Wayans, Jr.s comedic chemistry made the film. 
 TIME magazine placed Lets Be Cops fifth in their ranking of the 10 worst movies of 2014, stating: "The initial setup of Lets Be Cops could have been a funny premise for a B-plot on a show ... but instead, the idea is stretched into a never-ending feature that fails to find any chemistry between Johnson and Wayans". 

Fans of the web-series Dick Figures, criticized the film for copying the plot of the episode "Were Cops!", by having the two main characters act the same as Red & Blue, and that the plot was very similar.

==References==
 

==External links==
 
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 