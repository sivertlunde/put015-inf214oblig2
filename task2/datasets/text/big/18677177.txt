Siamo tutti inquilini
{{Infobox film
| name           = Siamo tutti inquilini
| image          = Siamo tutti inquilini.jpg
| caption        = Film poster
| director       = Mario Mattoli
| producer       = 
| writer         = Vittorio Calvino Ruggero Maccari
| starring       = Aldo Fabrizi
| music          = Pippo Barzizza
| cinematography = Marco Scarpelli
| editing        = Giuliana Attenni
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Siamo tutti inquilini is a 1953 Italian comedy film directed by Mario Mattoli and starring Aldo Fabrizi.

==Cast==
* Aldo Fabrizi - Augusto
* Anna-Maria Ferrero - Anna Perrini
* Enrico Viarisio - Sassi
* Tania Weber - Lulù
* Nino Pavese - Talloni
* Maria-Pia Casilio - Una cameriera
* Alberto Talegalli - Il venditore di uova
* Giuseppe Porelli
* Bice Valori - Una cameriera
* Maurizio Arena - Carlo
* Gemma Bolognesi
* Turi Pandolfini
* Peppino De Filippo - Antonio Scognamiglio

==External links==
* 

 

 
 
 
 
 
 
 


 
 