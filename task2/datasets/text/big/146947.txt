Spider-Man (2002 film)
 
{{Infobox film
| name           = Spider-Man
| image          = Spider-Man2002Poster.jpg
| caption        = Theatrical release poster
| director       = Sam Raimi
| producer       = {{plainlist|
* Avi Arad
* Laura Ziskin
* Ian Bryce
}}
| screenplay     = David Koepp
| based on       =    
| starring       = {{plainlist|
* Tobey Maguire
* Willem Dafoe
* Kirsten Dunst
* James Franco
* Cliff Robertson
* Rosemary Harris
* J.K. Simmons
}}
| music          = Danny Elfman Don Burgess
| editing        = {{plainlist|
* Bob Murawski
* Arthur Coburn
}}
| studio         = Marvel Enterprises
| distributor    = Columbia Pictures
| released       =  
| runtime        = 121 minutes
| country        = United States  
| language       = English
| budget         = $140 million
| gross          = $821.7 million   
}}
 of the same name, the film stars Tobey Maguire as Peter Parker, a high school student who turns to crimefighting after developing spider-like super powers. Spider-Man also stars Willem Dafoe as Norman Osborn (a.k.a. the Green Goblin), Kirsten Dunst as Peters love interest Mary Jane Watson, and James Franco as his best friend Harry Osborn.
 Chris Columbus, and David Fincher were considered to direct the project before Raimi was hired as director in 2000. The Koepp script was rewritten by Scott Rosenberg during preproduction and received a dialogue polish from Alvin Sargent during production.

Filming of Spider-Man took place in Los Angeles, and New York City from January 8 to June 30, 2001. Spider-Man premiered in the Philippines on April 30, 2002, and had its general release in the United States on May 3, 2002. It became a critical and financial success. For its time, it was the only film to reach $100 million in its first weekend, had the largest opening weekend gross of all time, and was the most successful film based on a comic book. With $821.7 million worldwide, it was 2002s third-highest-grossing film and is the List of highest-grossing films|44th-highest-grossing film of all time (7th at the time of release).
 Best Visual Best Sound Marvel created The Amazing Spider-Man which was released on July 3, 2012. A sequel to the reboot titled, The Amazing Spider-Man 2, was released in 2014. A second reboot to the franchise, set within the Marvel Cinematic Universe, is set to be released on July 28, 2017. 

==Plot==
  Peter Parker genetically engineered Norman Osborn, metamorphosized into a more muscular physique. At school, he finds that his body is able to produce webs and his quickened reflexes help him avoid injury during a confrontation with bully Flash Thompson. Peter later discovers that he has also developed superhuman speed, strength, the ability to stick to surfaces, and a heightened ability to sense danger.
 thief suddenly raids the promoters room, Peter allows the thief to escape as revenge. He later discovers Ben has been carjacked and killed. Peter chases down the carjacker and confronts him in a warehouse only to realize it was the same thief he let get away. After Peter disarms him, the fleeing carjacker falls out of a window to his death. Meanwhile, Norman Osborn kills several scientists and the militarys General Slocum at Quest Aerospaces testing field.
 freelance photographer, since Peter is the only one providing clear images of Spider-Man.
 Thanksgiving dinner, Norman deduces Spider-Mans true identity; the Green Goblin subsequently attacks and hospitalizes Aunt May.

Mary Jane admits she has a crush on Spider-Man, who has rescued her on numerous occasions, and she asks Peter whether Spider-Man ever asked about her. Harry, who loves Mary Jane, arrives and learns she does not feel likewise toward him. Devastated, Harry tells his father that Peter loves Mary Jane, unintentionally revealing Spider-Mans biggest weakness.

The Goblin kidnaps and holds Mary Jane and a Roosevelt Island Tram car full of children hostage alongside the Queensboro Bridge. There, he forces Spider-Man to choose who he wants to save, and drops Mary Jane and the children. Spider-Man manages to save both Mary Jane and the tram car, while the Goblin is distracted by civilians on the bridge who throw various objects at him, showing loyalty to Spider-Man and contempt for the Goblins evil deeds. The Goblin grabs Spider-Man and throws him into an abandoned building where they battle, with the Goblin gaining the upper hand. When the Goblin boasts of how he will later subject Mary Jane to a slow and painful death, Spider-Man overpowers the Goblin, unmasking him.

Norman begs for forgiveness, but his Goblin persona attempts to remote-control his glider to impale Spider-Man. The superhero avoids the attack using his "spider sense", and the glider accidentally kills Norman instead by stabbed him to death. In his dying breath, Norman tells Peter not to reveal his crimes to Harry. Spider-Man takes Normans body back to Osborns house and hides the Green Goblins equipment, though not before Harry finds him with his fathers body. At Norman’s funeral, Harry vows to exact revenge on Spider-Man, believing him responsible for killing his father, and asserts that Peter is all he has left. Mary Jane confesses to Peter she is in love with him, but Peter, feeling that he must protect her from the unwanted attention of Spider-Mans enemies, hides his true feelings and tells her that they can only be friends. As Peter leaves the funeral, he recalls Bens words about responsibility, and accepts his new life as Spider-Man.

==Cast==
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "I felt like I was an outsider. I think what happened to me made me develop this street sense of watching people and working out what made them tick, wondering whether I could trust them or not. I went to a lot of schools along the coast in California, made few friends and stayed with aunts, uncles and grandparents while my folks tried to make ends meet. It was tough. We had no money."
|-
| style="text-align: left;" | — Tobey Maguire on identifying with Peter Parker.   
|} The Cider House Rules.  The studio was initially hesitant to cast someone who did not seem to fit the ranks of "adrenaline-pumping, tail-kicking titans",  but Maguire managed to impress studio executives with his audition. The actor was signed for a deal in the range of $3 to $4&nbsp;million with higher salary options for two sequels.    To prepare, Maguire was trained by a physical trainer, a yoga instructor, a martial arts expert, and a climbing expert, taking several months to improve his physique.    Maguire studied spiders and worked with a wire man to simulate the arachnidlike motion, and had a special diet.  Freddie Prinze, Chris Klein, Wes Bentley, and Heath Ledger.  Edward Furlong had been considered by James Cameron for the role in 1996,  while Raimi joked of Prinze that "  wont even be allowed to buy a ticket to see this film."    In addition, actors Scott Speedman, Jay Rodan, and James Franco were involved in screen tests for the lead role. 
*  , John Malkovich, and John Travolta turned down the role.   Dafoe insisted on wearing the uncomfortable costume as he felt that a stuntman would not convey the characters necessary body language. The 580-piece suit took half an hour to put on. 
*  .  Dunst decided to audition after learning Maguire had been cast, feeling the film would have a more independent feel.  Dunst earned the role a month before shooting in an audition in Berlin. 
* James Franco as Harry Osborn:   Before being cast as Peters best friend and flatmate, Franco had screen tested for Spider-Man himself. 
*   who is trying to find a new job. He is killed by a carjacker whom Peter failed to stop, and leaves Peter with the message, "With great power comes great responsibility."
* Rosemary Harris as May Parker:   Ben Parkers wife and Peter Parkers aunt  who is supportive of Peters love for Mary Jane.
*   who despises Spider-Man. Nonetheless, he has a good side and pays Peter for photos of Spider-Man, and refuses to tell the Green Goblin the identity of the photographer. Eugene "Flash" Thompson:   A repugnant high school bully who bullies Peter, and is defeated in a fight after Peter inherits his spider powers.
* Bill Nunn as Joseph "Robbie" Robertson:   The kindly editor at the Daily Bugle, who on occasion helps Peter. the Carjacker:   The criminal who robs the wrestling manager who refuses to pay Peter Parker for his ring performance and later murders Ben Parker when he carjacks him in the course of his escape. He is killed in a fall from a window when confronted by Peter.
* Elizabeth Banks as Betty Brant:   As seen in past Spider-Man comics, Betty Brant is Jamesons secretary who has a bit of a soft spot for Peter.

  (produced by Raimi), also appears as a punk rock girl. One of the stunt performers in the film is actor Johnny Tri Nguyen.     Octavia Spencer has a brief appearance as the check-in girl who signs Peter Parker into the wrestling match and warns him that they are not liable for the injuries he will sustain.

==Production==

===Development===
  Sony Pictures Chris Columbus, and David Fincher as potential directors. Fincher did not want to depict the origin story, pitching the film as being based on The Night Gwen Stacy Died storyline, but the studio disagreed.  Sam Raimi was attached to direct in January 2000,    for a summer 2001 release.  He had been a big fan of the comic book during his youth, and his passion for Spider-Man earned him the job.   

Camerons work became the basis of David Koepps first draft screenplay, often word for word.    Camerons versions of the Marvel villains Electro and Sandman remained the antagonists. Koepps rewrite substituted the Green Goblin as the main antagonist and added Doctor Octopus as the secondary antagonist.  Raimi felt the Green Goblin and the surrogate father-son theme between Norman Osborn and Peter Parker would be more interesting.    In June, Columbia hired Scott Rosenberg to rewrite Koepps material. Remaining a constant in all the rewrites was the "organic webshooter" idea from the Cameron "scriptment".  Raimi felt he would stretch the audiences suspension of disbelief too far to have Peter invent mechanical webshooters. 
 WGA as sole screenwriter, despite the fact that it had acquired Camerons script and hired two subsequent writers. Without reading and comparing any of the material, the Writers Guild approved sole credit to Koepp. 

===Filming=== terrorist attacks Twin Towers Forest Hills home, and Stage 27 was used for the wrestling sequence where Peter takes on Bonesaw McGraw (Randy Savage). Stage 27 was also used for the complex Times Square sequence where Spider-Man and the Goblin battle for the first time, where a three-story set with a breakaway balcony piece was built. The scene also required shooting in Downey, California. DVD Booklet (2002), p.2–3  On March 6,  forty-five-year-old construction worker Tim Holcombe was killed when a forklift modified as a construction crane crashed into a construction basket that he was in. The following court case led to the California Division of Occupational Safety and Health to fine Sony $58,805. 
 Natural History Museum (for the Columbia University lab where Peter is bitten and receives his powers), the Pacific Electricity Building (the Daily Bugle offices) and Greystone Mansion (for the interiors of Norman Osborns home). In April, 4 of the Spider-Man costumes were stolen, and Sony put up a $25,000 reward for their return.  They were recovered after 18 months and a former movie studio security guard and an accomplice were arrested.  Production moved to New York City for two weeks, taking in locations such as the Queensboro Bridge, the exteriors of Columbia Universitys Low Memorial Library and the New York Public Library, and a rooftop garden in the Rockefeller Center.  The crew returned to Los Angeles where production and filming ended in June. The Flatiron Building was used for the Daily Bugle. 

===Design===
Although it wound up being faithful to the comics, many designs were made for Spider-Mans costumes: one concept costume designer James Acheson became fond of had a red emblem over a black costume.  To create Spider-Mans costume, Maguire was fitted for the skintight suit, being covered with layers of substance to create the suits shape.  It was designed as a single piece, except for the mask. The webbing, which accented the costume, was cut by computer. The mask eye lenses were designed to have a mirror look. 

===Effects===
Visual effects supervisor  , while the Green Goblin was shot against bluescreen. Shooting them together would have resulted in one character being erased from a shot. 

Saki said the biggest difficulty of creating Spider-Man was that as the character was masked, it immediately lost a lot of characterization. Without the context of eyes or mouth, a lot of body language had to be put in so that there would be emotional content. Raimi wanted to convey the essence of Spider-Man as being, "the transition that occurs between him being a young man going through puberty and being a superhero." Dykstra said his crew of animators had never reached such a level of sophistication to give subtle hints of still making Spider-Man feel like a human being.  When two studio executives were shown shots of the computer generated character, they believed it was actually Maguire performing stunts.  In addition, Dykstras crew had to composite areas of New York City and replaced every car in shots with digital models. Raimi did not want it to feel entirely like animation, so none of the shots were 100% computer generated. 

==Release==
  (the World Trade Center is reflected in Spider-Mans eyes)]]
After the  .  The trailer and poster were pulled after the events of the attacks, but can be found on the internet on websites such as YouTube. 

Before the films British theatrical release in June 2002, the British Board of Film Classification (BBFC) gave the film a "12" certificate. Due to Spider-Mans popularity with younger children, this prompted much controversy. The BBFC defended their decision, arguing that the film could have been given a "15". Despite this, North Norfolk and Breckland District Councils, in East Anglia, changed it to a "PG", and Tameside council, Manchester, denoted it a "PG-12". The U.S. rated it "PG-13"  for "stylized violence and action". In late August, the BBFC relaxed their policy to "12A", leading Sony to re-release the film. 

==Reception==

===Box office performance===
Spider-Man was a major commercial success, becoming the first film to pass the $100 million mark in a single weekend. With the release in the United States and Canada on May 3, 2002 on 7,500 screens at 3,615 theaters, the film earned $39,406,872 on its opening day, averaging $10,901 per theater ($5,524.25 per screen). This was the highest opening day at the time until it was surpassed by its sequel,   in 2006.  The film stayed at the top position in its second weekend, dropping only 38% in its second weekend, grossing another $71,417,527, averaging $19,755.89 per theater ($9,522.34 per screen) and bringing the ten-day total to $223,040,031. The film dropped to the second position in its third weekend, behind  , but still made $45,036,912, dropping only 37%, averaging $12,458 per theater, and bringing the seventeen-day tally to $285,573,668. It stayed at the second position in its fourth weekend, grossing $35,814,844 over the four-day Memorial Day frame, dropping only 21% while expanding to 3,876 theaters, averaging $9,240 over four days, and bringing the twenty-five day gross to $333,641,492.  In the box office, Spider-Man became 2002s highest-grossing film with $403,706,375 in the U.S. and Canada, defeating   and Star Wars Episode II: Attack of the Clones.  Spider-Man currently ranks as the  , and the List of highest-grossing films|44th-highest-grossing film of all time.

International markets which generated grosses in excess of $10 million include Australia ($16.9 million), Brazil ($17.4 million), France, Algeria, Monaco, Morocco and Tunisia ($32.9 million), Germany ($30.7 million), Italy ($20.8 million), Japan ($56.2 million), Mexico ($31.2 million), South Korea ($16.98 million), Spain ($23.7 million), and the United Kingdom, Ireland and Malta ($45.8 million).   
 The Dark The Avengers. Spider-Man currently ranks as the sixth-highest-grossing superhero film of all time.

The films U.S. television rights (Fox Broadcasting Company|Fox, TBS (TV channel)|TBS/TNT (TV channel)|TNT) were sold for $60 million.    Related gross toy sales were $109 million.  Its U.S. DVD revenue as of July 2004 stands at $338.8 million.  Its U.S. VHS revenue as of July 2004 is $89.2 million. 

===Critical response===
Spider-Man received positive reviews. The review aggregator website Rotten Tomatoes calculated an 89% overall approval based on 216 reviews.  The casting, mainly Tobey Maguire, is often cited as one of the films high points. Eric Harrison, of the Houston Chronicle, was initially skeptical of the casting of Tobey Maguire, but, after seeing the film, he stated, "within seconds, however, it becomes hard to imagine anyone else in the role."    USA Today critic Mike Clark believed the casting rivaled that of Christopher Reeve as 1978s Superman (1978 film)|Superman.    Owen Gleiberman, of Entertainment Weekly, had mixed feelings about the casting, particularly Tobey Maguire. "Maguire, winning as he is, never quite gets the chance to bring the two sides of Spidey&nbsp;– the boy and the man, the romantic and the avenger&nbsp;– together."    The Hollywood Reporters Kirk Honeycutt thought, "the filmmakers imaginations work in overdrive from the clever design of the cobwebby opening credits and Spider-Man and M.J.s upside down kiss&nbsp;– after one of his many rescues of her&nbsp;– to a finale that leaves character relationships open ended for future adventures."   

Conversely, LA Weeklys Manohla Dargis wrote, "It isnt that Spider-Man is inherently unsuited for live-action translation; its just that hes not particularly interesting or, well, animated."  Giving it 2.5/4 stars, Roger Ebert felt the film lacked a decent action element; "Consider the scene where Spider-Man is given a cruel choice between saving Mary Jane or a cable car full of school kids. He tries to save both, so that everyone dangles from webbing that seems about to pull loose. The visuals here could have given an impression of the enormous weights and tensions involved, but instead the scene seems more like a bloodless storyboard of the idea."    Stylistically, there was heavy criticism of the Green Goblins costume, which led IGNs Richard George to comment years later, "Were not saying the comic book costume is exactly thrilling, but the Goblin armor (the helmet in particular) from Spider-Man is almost comically bad... Not only is it not frightening, it prohibits expression." 

Entertainment Weekly put "the kiss in Spider-Man" on its end-of-the-decade "best-of" list, saying, "Theres a fine line between romantic and corny. And the rain-soaked smooch between Spider-Man and Mary Jane from 2002 tap-dances right on that line. The reason it works? Even if she suspects hes Peter Parker, she doesnt try to find out. And thats sexy."  Empire magazine ranked Spider-Man 437 in its 500 Greatest Movies of All Time list the following year.

===Awards===
 
The film won several awards ranging from   and Chicago (2002 film)|Chicago, respectively.       While only Danny Elfman brought home a Saturn Award, Raimi, Maguire, and Dunst were all nominated for their respective positions. It also took home the Peoples Choice Awards|Peoples Choice Award for "Favorite Motion Picture."  The film was nominated for Favorite Movie at the Nickelodeon Kids Choice Awards, but lost to Austin Powers in Goldmember.

{| class="wikitable" style="text-align:center;" width="99%"
|+
! scope="col" | Award
! scope="col" | Date of ceremony
! scope="col" | Category
! scope="col" | Recipients and nominees
! scope="col" | Outcome
|-
| rowspan="2"| Academy Awards  March 23, 2003 Best Sound Mixing Kevin OConnell, Greg P. Russell and Ed Novick
|  
|- Best Visual Effects
| John Dykstra, Scott Stokdyk, Anthony LaMolinara and John Frazier
|  
|-
|  
|date=May 14, 2003 |accessdate=June 23, 2011}} 
| May 14, 2003
| scope="row" style="text-align:center"| BMI Film Music Award
| Danny Elfman
|  
|-
| British Academy Film Awards  February 23, 2003 Best Achievement in Special Visual Effects
| John Dykstra, Scott Stokdyk, John Frazier, Anthony LaMolinara, John Dykstra, Scott Stokdyk, John Frazier and Anthony LaMolinara
|  
|-
| Broadcast Film Critics Association  January 17, 2003
| scope="row" style="text-align:center"| Best Song
| Chad Kroeger ("Hero")
|  
|-
| Empire Awards  February 5, 2003
| scope="row" style="text-align:center"| Best Actress
| Kirsten Dunst
|  
|-
| rowspan="4"| Golden Trailer Awards 
| rowspan="4"| March 14, 2002
| scope="row" style="text-align:center"| Best Action
| Spider-Man
|  
|-
| scope="row" style="text-align:center"| Best Music
| Spider-Man
|  
|-
| scope="row" style="text-align:center"| Best of Show
| Spider-Man
|  
|-
| scope="row" style="text-align:center"| Best Voice Over
| Spider-Man
|  
|-
| rowspan="2"| Grammy Award  February 23, 2003
| scope="row" style="text-align:center"| Best Score Soundtrack Album for a Motion Picture, Television or Other Visual Media
| Danny Elfman
|  
|-
| scope="row" style="text-align:center"| Best Song Written for a Motion Picture, Television or Other Visual Media
| Chad Kroeger ("Hero")
|  
|-
| Hugo Awards 
| August 30, 2003
| scope="row" style="text-align:center"| Best Dramatic Presentation – Long Form
| Spider-Man
|  
|-
| rowspan="5"| MTV Movie Awards  May 31, 2003 Best Female Performance
| Kirsten Dunst
|  
|- Best Kiss
| Kirsten Dunst and Tobey Maguire
|  
|-
| scope="row" style="text-align:center"| Best Male Performance
| Tobey Maguire
|  
|- Best Movie
| Spider-Man
|  
|- Best Villain
| Willem Dafoe
|  
|-
| Peoples Choice Awards 
| January 12, 2003
| scope="row" style="text-align:center"| Favorite Motion Picture
| Spider-Man
|  
|-
| rowspan="2"| Satellite Awards  January 12, 2003
| scope="row" style="text-align:center"| Best Film Editing
| Eric Zumbrunnen
|  
|-
| scope="row" style="text-align:center"| Best Visual Effects
| John Dykstra
|  
|-
| rowspan="6"| Saturn Awards  May 18, 2003
| scope="row" style="text-align:center"| Best Fantasy Film
| Spider-Man
|  
|-
| scope="row" style="text-align:center"| Best Actor
| Tobey Maguire
|  
|-
| scope="row" style="text-align:center"| Best Actress
| Kirsten Dunst
|  
|-
| scope="row" style="text-align:center"| Best Director
| Sam Raimi
|  
|-
| scope="row" style="text-align:center"| Best Music
| Danny Elfman
|  
|-
| scope="row" style="text-align:center"| Best Special Effects
| John Dykstra, Scott Stokdyk, Anthony LaMolinara and John Frazier
|  
|-
| World Soundtrack Awards 
| October 19, 2002
| scope="row" style="text-align:center"| Best Original Soundtrack of the Year – Orchestral
| Danny Elfman
|  
|-
| World Stunt Awards 
| June 1, 2003
| scope="row" style="text-align:center"| Best Fight
| Chris Daniels, Zach Hudson, Kim Kahana Jr., Johnny Nguyen and Mark Aaron Wagner
|  
|-
| Young Artist Awards 
| March 29, 2003
| scope="row" style="text-align:center"| Best Family Feature Film - Fantasy
| Spider-Man
|  
|}

==Sequels==
 

In January 2003, Sony revealed that a sequel to Spider-Man was in development, and would be produced and directed by Sam Raimi. On 15 March 2003, a trailer revealed that the film, Spider-Man 2, would be released in 2004. Spider-Man 3, the second sequel to Spider-Man and the final film in the series to be directed by Raimi, was released on 4 May 2007.

==See also==
* Spider-Man (2002 video game)|Spider-Man (2002 video game)
*  

 

==References==
 

==External links==
 
 
* 
* 
* 
* 

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 