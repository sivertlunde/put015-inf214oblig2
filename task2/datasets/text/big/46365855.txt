The Bottle (1915 film)
{{Infobox film
| name           = The Bottle
| image          =
| caption        =
| director       = Cecil M. Hepworth
| producer       =
| writer         = Arthur Shirley (play)
| starring       = Albert Chevalier Stewart Rome Alma Taylor
| cinematography =
| editing        =
| studio         = Hepworth Pictures
| distributor    = Hepworth Pictures
| released       =  
| runtime        =
| country        = United Kingdom
| awards         =
| language       = Silent English intertitles
| budget         =
}}
The Bottle is a 1915 British silent drama film directed by Cecil M. Hepworth and starring Albert Chevalier, Stewart Rome and Alma Taylor.  It was based on a play by Arthur Shirley.

==Cast==
*    Albert Chevalier as Harry Ashford  
* Ivy Millais as Mary Ashford  
* Harry Brett as Jim Brewster  
* Stewart Rome as Barman 
* John MacAndrews 
* Alma Taylor 

==References==
 

==Bibliography==
* Palmer, Scott. British Film Actors Credits, 1895-1987. McFarland, 1988. 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 