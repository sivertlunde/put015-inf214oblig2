All Mine to Give
{{Infobox Film
| name           = All Mine to Give
| image          = All Mine to Give VideoCover.png
| image_size     = 
| caption        = 
| director       = Allen Reisner
| producer       = Sam Wiesenthal
| writer         = Dale Eunson (novel) Cosmopolitan (1946)
| screenplay     = Katherine Albert
| narrator       = Rex Thompson Cameron Mitchell Rex Thompson
| music          = Max Steiner
| cinematography = William V. Skall
| editing        = Bettie Mosher
| studio         = RKO Radio Pictures
| distributor    = Universal-International
| released       =  
| runtime        = 100-103 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Cameron Mitchell, and Rex Thompson. When first one parent, then the other dies, six children have to look after themselves in the American west of the mid-19th century.

This story is based on a true-life story set in Wisconsin, based on an article The Day They Gave Babies Away by Dale Eunson and Katherine (Albert) Eunson, which first appeared in the December 1946 issue of Cosmopolitan (magazine)|Cosmopolitan. 

A year later, the story would be published as a book with the same title. Original US TV version The Day They Gave Babies Away, starring Brandon deWilde, aired on the CBS anthology show Climax! on December 22, 1955.

Eunson and his wife Katherine also went on to write the screenplay for the movie adaptation.  

The exteriors of the film were filmed in Big Bear, California, Idyllwild, California and Mt. Hood, Oregon.

==Plot== Cameron Mitchell Scots who have just landed in America (the year is 1856), having been invited there by Mamies uncle. They arrive in the tiny logging village of Eureka, only to be informed that both uncle and his cabin have been incinerated in a house fire. The Eunsons are assisted by the friendly locals in reconstructing the house and Robert takes to tipping timber.

Mamie is heavily pregnant upon their reaching Eureka; she delivers baby Robbie (Rex Thompson) soon after the cabin is completed. Robert eventually starts a successful boat building business and Mamie gives birth to five more children: Jimmy (Stephen Wootton), Kirk (Butch Bernard), Annabelle (Patty McCormack), Elizabeth (Yolanda White), and Jane (Terry Ann Ross). The Eunsons are prospering and happy until little Kirk is diagnosed with diphtheria. Mamie and Kirk are quarantined while Robert takes the other children away. The boy recovers, but the goodbye kiss Kirk gave his Dadda before his departure proves fatal, and Robert succumbs.

Mamie takes to working as a seamstress and Robbie becomes the man of the house. Things stabilize, but only briefly: tired and work-worn, Mamie contracts typhoid. Knowing she will not survive, she charges Robbie, her eldest, with finding good homes for his siblings. After Mamies death, Robbie places his brothers and sisters with kindly townsfolk as Christmas approaches. Stoic and resigned during the process, he does break down when he is alone and sees the tree outside the homestead where his father had carved the names of the children into the bark. Baby Jane is the last to be handed over — Robbie stands at the door of a house and asks the woman who answers, "Please, maam, I was wondering if youd care to have my sister."

==Cast==
* Glynis Johns as Mamie Cameron Mitchell as Robert
* Rex Thompson as Robbie
* Patty McCormack as Annabelle
* Ernest Truex as Doctor Delbert
* Hope Emerson as Mrs. Pugmire
* Alan Hale, Jr. as Tom Cullen (as Alan Hale)
* Sylvia Field as Lelia Delbert
* Royal Dano as Howard Tyler
* Reta Shaw as Mrs. Runyon
* Stephen Wootton as Jimmy
* Butch Bernard as Kirk
* Yolanda White as Elizabeth
* Rita Johnson as Katie Tyler
* Ellen Corby as Mrs. Raiden
* Rosalyn Boulter as Mrs. Stephens  Francis De Sales as Mr. Stephens 
* Jon Provost as Robbie Eunson - age 6

==References==
 	

==External links==
*  
*  
*  
*  


 
 
 
 
 
 
 
 
 
 
 