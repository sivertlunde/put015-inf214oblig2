Romance in Rhythm
{{Infobox film
| name =  Romance in Rhythm
| image =
| image_size =
| caption =
| director = Lawrence Huntington
| producer = Lawrence Huntington 
| writer =  Lawrence Huntington 
| narrator = David Burns
| music = 
| cinematography = Desmond Dickinson
| editing = Challis Sanderson
| studio = Allied Film Productions
| distributor = Metro-Goldwyn-Mayer
| released = 1934
| runtime = 73 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} David Burns. It was made at Cricklewood Studios.  A songwriter is accused of murdering a nightclub manager. 

==Cast==
*   Phyllis Clare as Ruth Lee  
* David Hutcheson as Bob Mervyn   David Burns as Mollari 
* Queenie Leonard 
* Paul Tillett 
* Geoffrey Goodheart 
* Philip Strange 
* Julian Vedey  
* Carroll Gibbons as Himself with his Savoy Orpheans

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 

 