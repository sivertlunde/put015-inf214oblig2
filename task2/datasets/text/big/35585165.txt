El Palomo cojo
{{Infobox film
| name           = El palomo cojo
| image          = El Palomo cojo poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Jaime de Armiñán
| producer       = Luis Méndez
| writer         = 
| screenplay     = Jaime de Armiñán
| story          = 
| based on       = El palomo cojo by Eduardo Mendicutti 
| narrator       = 
| starring       = María Barranco Francisco Rabal Carmen Maura  Miguel Ángel Muñoz
| music          = Jesús Yanes
| cinematography = Fernando Arribas
| editing        = José Luis Matesanz
| studio         = Lotus Films
| distributor    = 
| released       = 6 October 1995
| runtime        = 114 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}} 1995 Spanish drama film written and directed by Jaime de Armiñán, based on the eponymous novel by Eduardo Mendicutti published in 1991. Benavent, Cine Español de los Noventa , p. 440   It stars María Barranco, Francisco Rabal, Carmen Maura and  Miguel Ángel Muñoz. It is a coming of age story of a ten-year-old boy sent to the house of his maternal grandparents, which is full of eccentric characters. The boy has to deal with the realization of his sexual identity. The title makes reference to the analogy of a lame pigeon with homosexuality.  The film is an exploration of class and sexual identity in Andalusia, Spain, in the 1950s.

==Plot==
In 1958, Felipe, a ten year old boy, is sent by his mother to spend the summer with his maternal relatives in Sanlúcar de Barrameda while he recuperates his frail health. His grandparents are wealthy and live in a vast residence that serves as the center that reunites a large family and many visitors. The house is dominated by the frequent howls of the boys ancient great-grandmother, who is senile and is taken care by Adoración, a strict nurse, who endures the old womans endlessly repeated recollection of the four bandoleros who killed each other for her love.

Pampered on his arrival by the women in the house, Felipe is ordered to stay in bed by the familys doctor. He is installed in the bedroom of his absent uncle Ramón and spends his time reading Little Women. Felipe quickly strikes a close friendship with Mari, the talkative and meddlesome maid. She keeps four lovers on a string, but refuses to let go of her virginity. An expert on mens matters, Mari shamelessly flirts with the boy even when she has the impression that he might be gay. In secrecy, she shows Felipe some items that she has found in uncle Ramóns bedroom: a suggesting photograph of Ramón in swimming wear revealing his anatomy and a postcard from 1936 signed by someone named Federico that depicts a dog looking at a dove with what Mari describes a lovesick face. The message in the postcard and its image imply that there was a homoerotic relationship between Ramón and Federico. Felipe points out that the picture was taken in the terrace of the bedroom and both Mari and the boy are fascinated by Ramóns revealing attire in the photograph.

Felipe also befriends Uncle Ricardo, the eccentric brother of the boys grandfather, who trains pigeons and dreams of finding a treasure at sea with his boat. Felipe tells him of Maris believe that lame doves are queers and then inquires his great uncle if he might be queer. Although Ricardo denies it, he does not do so when the boy ask the same question regarding uncle Ramón, evasively responding that he does not longer remembers.

Peace in the house is frequently disturbed by many visitors like Reglita, a mature spinster, and there is general commotion with the arrival of Felipes cosmopolitan Aunt Victoria, a singer who sweeps into town with Luigi, her muscle-bound Italian "secretary" in tow.  A liberal free thinker who has travelled Europe extensively, Victoria scandalizes Felipes conservative grandmother with her recitation of erotic and anti-Franco poetry that makes the neighbors blushed but fascinates Reglita.  With her free way of living and her expertise on men, Victoria draws the attention of Mari who asks her about her lovers and Victoria shows her a valuable ring that one of them gave her.

The uncertainty of Felipes blossoming sexuality is subjected to further confusion with the surprising arrival of the charismatic Communist uncle Ramón, who takes back possession of his bedroom. Both Felipe and Mari are fascinated by him. Felipe asks Ramón which tastes better, a man, a lady, or a young gal? to which the worldly Uncle Ramón responds, Ladies and young gal taste great, and I once met a man who tasted like Manchego cheese. Felipe immediately identifies this man as Federico. The boy then compliments his uncle on having gorgeous eyes.

There is new commotion in the house with the elopement of Luigi with one of Maris lovers, stealing Victorias valuable ring, and with the death of Felipes senile great grandmother. Mari tells Felipe that he can spy from his bedroom uncle Ramón naked at night, but instead she shows up dancing naked by his window and with Victorias ring in her hand. The next day Felipe accuses Mari of having stealing the ring and she is fired by the grandfather after Felipe reveals where she hides the ring. Felipe goes to Maris room before her departure to reconcile. To Felipes sadness uncle Ramón has left as sudden as he arrived. Victoria, Felipe and Uncle Ricardo set out to sea on a treasure seeking adventure.

==Cast==
*Maria Barranco as Mari
*Francisco Rabal as uncle Ricardo
*Carmen Maura  as aunt Victoria
*Miguel Ángel Muñoz as Felipe
*Joaquin Kremel as uncle Ramón
*Ana Torrent  as Adoración, the nurse
*Maria Galiana as the grandmother
*Amparo Baro  as Reglita
*Tomás Zori as the Great-grand mother
*Valeriano Andres as the Grandfather
*Maria Massip as aunt Blanca
*Asuncion Balaguer as  Tata Caridad
*Ofelia Angelica as Esther
*Paolo Stella as Luigi

==Production==
El Palomo cojo, Jaime de Armiñáns fourteenth feature film, is based on the semiautobiographical novel of the same title written by Eduardo Mendicutti  published in 1991.   The film was shot in Mendicuttis hometown of Sanlúcar de Barrameda and many of the interiors were filmed in the house that belonged to the authors grandparents. Deveny, Contemporary Spanish Film from Fiction , p. 272 

The film is a period piece set in the 1950s, portraying the region of Andalucia at that time. Benavent, Cine Español de los Noventa , p. 441   The grandfather asks if President Eisenhower is going to visit Spain.   Mendicutti commented that "Armiñán changed things in the screen version, since my novel is narrated in the first person and is based more on sensations than on actions". Deveny, Contemporary Spanish Film from Fiction , p. 275  The film is notable for its large cast of some of Spains best regarded actors. The role of uncle Ricardo was expanded from what it was on the book, to take advantage of the acting skills of veteran actor Francisco Rabal who went on to play a similar role on Carlos Sauras film Pajarico. Particularly noteworthy is  the performance of Maria Barranco as the talkative and foul mouthed maid. She speaks with her strong Andalutian accent. 

==Notes==
 

==References==
*Benavent, Francisco Maria. Cine Español de los Noventa. Ediciones Mensajero, 2000.
* Devny, Thomas G. Contemporary Spanish Film from Fiction. The Scarecrow Press,1999.

== External links ==
*  

 
 
 
 
 
 
 
 