Bhale Adrushtavo Adrushta
{{Infobox film 
| name           = Bhale Adrushtavo Adrushta
| image          =  
| caption        = 
| director       = K. S. L. Swamy (Ravi)
| producer       = 
| writer         = Valampuri Somanathan
| screenplay     = Valampuri Somanathan Kalpana B. Gangadhar Srinath
| music          = Vijaya Bhaskar
| cinematography = R N K Prasad
| editing        = Bal G Yadav
| studio         = Raghunandan Movies
| distributor    = Raghunandan Movies
| released       =  
| country        = India Kannada
}}
 1971 Cinema Indian Kannada Kannada film, Gangadhar and Srinath in lead roles. The film had musical score by Vijaya Bhaskar.  

==Cast==
  Kalpana
*B. V. Radha Gangadhar
*Srinath
*K. S. Ashwath
*Sampath Balakrishna
*Dwarakish
*Thoogudeepa Srinivas
*Manjunath
*C. L. Narayana Rao
*Thimmayya
*M.N Lakshmi Devi
*Advani Lakshmi Devi
*Rama
*B. Jaya (actress)|B. Jaya
*B. Jayashree
*Ramadevi
*Chi. Udaya Shankar
*Shivaram
*Smt Ragini
*Ramesh in Special Appearance
 

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Maithri Apoorva Maithri || PB. Srinivas || R. N. Jayagopal || 03.20
|-
| 2 || Navu Haaduvude || PB. Srinivas, AL. Ragavan || Chi. Udaya Shankar || 03.33
|-
| 3 || Balegaara Chenniaiah || PB. Srinivas || R. N. Jayagopal || 03.05
|-
| 4 || Daaha Daaha || K. J. Yesudas || R. N. Jayagopal || 03.36
|-
| 5 || Kalpana Roopa Rasi || PB. Srinivas || KSL. Swamy || 03.27
|- Janaki || Purandaradasa || 03.51
|-
| 7 || Kannadathi O Gelathi || PB. Srinivas, L. R. Eswari || KSL. Swamy || 03.23
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 


 