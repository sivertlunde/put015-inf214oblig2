Women's Prison (1955 film)
{{Infobox film
| name           = Womens Prison
| image	         = Womens Prison FilmPoster.jpeg
| image size     =
| alt            =
| caption        = Theatrical release poster
| director       = Lewis Seiler
| producer       = Bryan Foy
| screenplay     = Crane Wilbur Jack DeWitt
| story          = Jack DeWitt
| starring       = Ida Lupino Jan Sterling Cleo Moore
| music          = Mischa Bakaleinikoff
| cinematography = Lester White
| editing        = Henry Batista
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} film prison noir directed by Lewis Seiler and starring Ida Lupino, Jan Sterling and Cleo Moore. 

The movie is noted today for the appearance of Moore, and for Lupinos performance as the aggressively cruel warden.  In the 1980s this movie became rather popular,   and Over-Exposed.

==Plot==
A ruthless superintendent of a prison (Lupino) makes life hell for the female inmates.

==Cast==
* Ida Lupino as Amelia van Zandt
* Jan Sterling as Brenda Martin
* Cleo Moore as Mae
* Audrey Totter as Joan Burton
* Phyllis Thaxter as Helene Jensen
* Howard Duff as Dr. Crane
* Warren Stevens as Glen Burton
* Barry Kelley as Warden Brock
* Gertrude Michael as Matron Sturgess
* Vivian Marshall as Dottie LaRose
* Mae Clarke as Matron Saunders
* Ross Elliott as Don Jensen
* Adelle August as Grace
* Don C. Harvey as Chief Guard Tierney
* Juanita Moore as "Polly" Jones

==Reception==

===Critical response===
The staff at Variety (magazine)|Variety magazine praised some of the actors in the film, "Sterling scores nicely as a tough moll, Cleo Moore is a typical femme inmate and Vivian Marshall, as an ex-stripteaser gone wrong, shines in some amusing impersonations." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 