Kabuliwala (1957 film)
 
{{Infobox film
| name           = Kabuliwala 
| image          = Kabuliwala1956cover.jpg
| image size     =
| caption        = Kabuliwala (1957) cover
| director       = Tapan Sinha
| producer       = Charuchitra
| screenplay     = Tapan Sinha
| story          = Rabindranath Tagore
| narrator       = 
| starring       = Chhabi Biswas Oindrila Tagore (Tinku) Asha Devi
| music          = Ravi Shankar
| cinematography = Anil Banerjee
| editing        = Subodh Ray
| distributor    = 
| released       =  
| runtime        = 116 minutes
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Tapan Sinha and based on the eponymous story by the Bengali writer Rabindranath Tagore.

==Plot==
Rahmat (Chhabi Biswas), a middle-aged fruit seller from Afghanistan, comes to Calcutta to hawk his merchandise and befriends a small Bengali girl called Mini (Oindrila Tagore aka Tinku Tagore) who reminds him of his own daughter back in Afghanistan. He puts up at a boarding house along with his countrymen.

One day Rehmat receives news of his daughter’s illness through a letter from his country and he decides to leave for his country. Since he is short of money he decides to sell his goods on credit for increasing his business. Later, when he goes to collect on his money, one of his customers abuses him and in the fight that ensues Rehmat warns that he will not tolerate abuse and stabs the guy when he does not stop the abuse.

In the court Rehmats lawyer tries to obfuscate the facts but in his characteristic and simple fashion Rehmat states the truth in a matter of fact way. The judge, pleased with Rehmats honesty, gives him 10 years rigorous imprisonment instead of the death sentence. On the day of his release he goes to meet Mini but discovers that she has grown up into a 14-year old girl and is about to get married. Mini does not recognize Rehmat, who realizes that his own daughter must have forgotten him too. Minis father gives Rehmat the money for travel out of Minis wedding budget to which Mini agrees; she also sends a gift for Rehmats daughter.

==Cast==

* Chhabi Biswas as Rehmat
* Tinku as Mini
* Radhamohan Bhattacharya as Girls Father
* Manju Dey as Girls Mother
* Jiben Bose as Jailor
* Asha Devi as Maid
* Kali Banerjee
* Jahar Ray
* Nripati Chatterjee

==Awards==
 National Film Awards    1956 - National Film Award for Best Feature Film
**1956 - National Film Award for Best Feature Film in Bengali
 Silver Bear Extraordinary Prize of the Jury - 7th Berlin International Film Festival   

==See also==
 
* Kabuliwala (1961 film)

==References==
 

==External links==
*  
*  
*   detailed info at  
*   Translation

 
 
 

 
 
 
 
 
 
 
 
 


 
 