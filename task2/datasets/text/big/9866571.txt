Adam at 6 A.M.
{{Infobox film
| name           = Adam at 6 A.M.
| image          = AdamAtSixAM1970Poster.jpg
| alt            =  
| caption        = original film poster
| director       = Robert Scheerer
| producer       = Robert W. Christiansen Robert E. Relyea Rick Rosenberg
| writer         = Elinor Karpf, Stephen Karpf
| starring       = Michael Douglas Lee Purcell Joe Don Baker Louise Latham Charles Aidman  and Grayson Hall
| music          = Dave Grusin
| cinematography = Charles Rosher, Jr.
| editing        = John McSweeney, Jr.  
| studio         = Cinema Center Films Solar Productions
| distributor    = National General Pictures (1970,original)
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $1.4 million Film Star of Year Turns to Creative Extension: McQueens Creative Film Kick
Warga, Wayne. Los Angeles Times (1923-Current File)   21 Sep 1969: u1.  
| gross          = 
}}
Adam at 6 A.M. is a 1970 American film directed by Robert Scheerer.  The film did not receive much attention when it was released, but was the second starring role for Michael Douglas.  The movie was filmed almost entirely on location in the small Midwest town of Excelsior Springs, Missouri,  as well as Cameron, Missouri,  also filmed in Orrick Missouri.

==Plot==

The film revolves around Adam Ganes (Douglas), a semantics professor at a California college.  He becomes complacent in his life and hears about the death of a relative in Missouri.  He drives cross country to attend the funeral and pay his respects, and decides to spend the summer there working as a laborer.  He meets Jerri Jo Hopper (Purcell) and falls in love, along the way developing new friendships with the town locals.  He then must decide what direction he wants his life to go, whether to stay in Missouri or return to California.

==Production==
Steve McQueens film company, Solar Productions, signed a multi-picture deal with Cinema Center Films. This was its first production. 

==See also==
* List of American films of 1970
 
==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 