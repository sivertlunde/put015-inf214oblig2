The Message (1976 film)
{{Infobox film name           = The Message (aka Mohammad, Messenger of God) image          = The-Message.jpg alt            =  caption        = "The Message promotional film poster director       = Moustapha Akkad producer       = Moustapha Akkad, Mohammed Sanousi & Harold Buck writer         = H.A.L. Craig A.B. Jawdat al-Sahhar Tawfiq al-Hakim A.B Rahman al-Sharkawi Mohammad Ali Maher screenplay     = H.A.L. Craig story          =  based on The Islamic prophet Muhammad narrator  Richard Johnson starring       = Anthony Quinn Irene Papas Michael Ansara Johnny Sekka Michael Forest music          = Maurice Jarre cinematography = Said Baker Jack Hildyard Ibrahim Salem editing  John Bloom Hussein Afifi studio         = Filmco International Productions Inc. distributor    = Tarik Film Distributors Anchor Bay Entertainment released       =  Arabic  English runtime        = 178 minutes English 198 min Arabic country        = Morocco Libya Lebanon United Kingdom United States language       = English Arabic budget         = $10 million gross          = $15 million
|}}
 Arabic (1976) English (1977), Islamic history. 
 Best Original Star Wars (composed by John Williams).

==Plot== exodus to Medina, and ending with the Muslims triumphant return to Mecca. 
 Hamza ibn `Abd al-Muttalib (Muhammads uncle), Bilal and Zayd (two of the Prophets closest companions), and on the other side Abu Sufyan (the leader of Mecca) and his wife Hind bint Utbah (initially, enemies of Islam who later become Muslims themselves).

==Production==
 Al Azhar" in Egypt but was rejected by the Muslim World League in Mecca, Saudi Arabia. Akkad had to go outside the United States in order to raise the production money needed for the film. Lack of financing nearly shut down the film because its initial backers pulled out. 
 Emir Sabah III Al-Salim Al-Sabah of Kuwait withdrew financial support. King Hassan II of Morocco gave Akkad full support for the production, while King Khalid bin Abdulaziz of Saudi Arabia and then-Libyan leader Muammar al-Gaddafi provided financial support too. 

The film was shot in Morocco and Libya, with production taking four and a half months to build the cities of Mecca and Medina as they looked in Muhammads time. Production took one year, Akkad filmed for six months in Morocco, but had to stop when the Saudi government exerted great pressure on the Moroccan government to stop the project. Akkad went to al-Gaddafi for support in order to complete the project, and the Libyan leader allowed him to move the filming to Libya for the remaining six months.
 Western and Islamic worlds, stating in a 1976 interview:

 "I did the film because it is a personal thing for me. Besides its production values as a film, it has its story, its intrigue, its drama. Besides all this I think there was something personal, being a Muslim myself who lived in the west I felt that it was my obligation my duty to tell the truth about Islam. It is a religion that has a 700 million following, yet its so little known about which surprised me. I thought I should tell the story that will bring this bridge, this gap to the west." 

Akkad also filmed an Arabic version of the film (in which Muna Wassef played Hind) simultaneously with an Arab cast, for Arabic-speaking audiences. He felt that dubbing the English version into Arabic would not be enough, because the Arabic acting style differs significantly from that of Hollywood. The actors took turns doing the English and Arabic versions in each scene, and both are now sold together on some DVDs.
 siege against the Washington, D.C. chapter of the Bnai Brith under the mistaken belief that Anthony Quinn played Mohammed in the film, threatening to blow up the building and its inhabitants unless the films opening was cancelled. The standoff was resolved" after the deaths of a journalist and a policeman, but "the films American box office prospects never recovered from the unfortunate controversy." 

===Depiction of Muhammad===
In accordance with Muslim beliefs regarding depictions of Muhammad, he was not depicted on-screen nor was his voice heard because Islamic tradition generally forbids any direct representation of religious figures. At the beginning of the film, the following disclaimer is displayed:

 
 his wives, first caliphs Ali ibn Badr and Uhud depicted in the movie, Hamza was in nominal command, even though the actual fighting was led by Muhammad.
 point of view. Others in the scene nodded to the unheard dialogue or moved with the camera as though moving with Muhammad.
 his immediate family are the view of Alis famous two-pronged sword Zulfiqar during the battle scenes, a glimpse of a staff in the scenes at the Kaaba or in Medina, and Muhammads camel, Qaswa.

==Cast==
===English version===
*Anthony Quinn as Hamza ibn Abdul-Muttalib|Hamza.
*Irene Papas as Hind bint Utbah|Hind.
*Michael Ansara as Abu Sufyan ibn Harb|Bu~sofyan.
*Johnny Sekka as Bilal ibn Rabah|Bilal.
*Michael Forest as Khalid ibn al-Walid|Khalid.
*André Morell as Abu Talib ibn ‘Abd al-Muttalib|Bu~Talib.
*Garrick Hagon as Ammar ibn Yasir|Ammar.
*Damien Thomas as Zayd ibn Harithah|Zayd. Martin Benson as Abu Jahl ibn Hisham|Bu~Jahl. Robert Brown as Utbah ibn Rabiah|Utbah.
*Rosalie Crutchley as Sumayyah bint Khayyat|Sumayyah.
*Bruno Barnabe as Umayyah ibn Khalaf|Umayyah.
*Neville Jason as Ja`far ibn Abī Tālib|Jafar. John Bennett as Abd-Allah ibn Ubayy|Ibn~Saloul.
*Donald Burton as Amr ibn al-As|`Amr. Earl Cameron as Aṣḥama ibn Abjar|Najashi.
*George Camiller as Walid ibn Utbah|Al-Waleed.
*Nicholas Amer as Suhayl ibn Amr|Suhayl.
*Ronald Chenery as Mus`ab ibn `Umair|Mus`ab.
*Michael Godfrey as Al-Bara ibn Al-Marour|Bara.
*John Humphry as Ubayda ibn as-Samit|Ubada.
*Ewen Solon as Yasir ibn Amer|Yasir.
*Wolfe Morris as Abu Lahab|Bu~Lahab.
*Ronald Leigh-Hunt as Heraclius.
*Leonard Trolley as Silk Merchant.
*Gerard Hely as Poet Sinan.
*Habib Ageli as Abu Hudhayfah ibn Utbah|Hudhayfah. Peter Madden as Toothless Man. Hassan Joundi as Khosrau II|Kisra.
*Abdullah Lamrani as Ikrimah ibn Abi Jahl|Ikrimah.
*Elaine Ives-Cameron as Umm Jamil|Arwa.
*Mohammad Al Gaddary as Money Lender.

===Arabic version===
*Abdullah Gaith as Hamza ibn Abdul-Muttalib|Hamza.
*Muna Wassef as Hind bint Utbah|Hind.
*Hamdi Gaith as Abu Sufyan ibn Harb|Bu~sufyan.
*Ali Ahmed Salem as Bilal ibn Rabah|Bilal.
*Mahmud Saeed as Khalid ibn Al-Walid|Khalid.
*Ahmed Marey as Zayd ibn Harithah|Zayd.
*Mohammed Al-Arabi as Ammar ibn Yasir|Ammar.
*Hassan Joundi as Abu Jahl|Bu~jahl.
*Sana Jamil as Sumayyah bint Khayyat|Sumayyah.

==Awards and nominations== Oscar in 1977 for Best Music, Original Score for the music by Maurice Jarre. 

==Music==
The musical score of The Message was composed and conducted by Maurice Jarre, and performed by the London Symphony Orchestra.

===Soundtrack===

;Track listing for the first release on LP

Side One
# The Message (03:01)
# Hegira (04:24)
# Building the First Mosque (02:51)
# The Sura (03:34)
# Presence of Mohammad (02:13)
# Entry to Mecca (03:15)

Side Two
# The Declaration (02:38)
# The First Martyrs (02:27)
# Fight (04:12)
# Spread of Islam (03:16)
# Broken Idols (04:00)
# The Faith of Islam (02:37)

;Track listing for the first release on CD

# The Message (03:09)
# Hegira (04:39)
# Building the First Mosque (02:33)
# The Sura (03:32)
# Presence of Mohammad (02:11)
# Entry to Mecca (03:14)
# The Declaration (02:39)
# The First Martyrs (02:26)
# Fight (04:11)
# The Spread of Islam (03:35)
# Broken Idols (03:40)
# The Faith of Islam (02:33)

==Remake==
In October 2008, producer Oscar Zoghbi revealed plans to "revamp the 1976 movie and give it a modern twist," according to the Internet Movie Database and the World Entertainment News Network.     He hopes to shoot the remake, tentatively titled The Messenger of Peace, in the 
cities of Mecca and Medina in Saudi Arabia.
 The Lord of the Rings film trilogy Barrie M. Osborne was attached to produce a new movie about Muhammad. The film is to be financed by a Qatari media company and will be supervised by Sheikh Yusuf al-Qaradawi. 

==See also==
 
*Battle of Badr
*Battle of Uhud
*List of films about Muhammad
*List of historical drama films

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 