The First Deadly Sin
 
{{Infobox film
| name        = The First Deadly Sin
| image       = First deadly sin poster.jpg
| caption     = Theatrical release poster
| director    = Brian G. Hutton
| producer    = Frank Sinatra Elliott Kastner George Pappas Mark Shanker
| writer      = Novel:  
| music       = Gordon Jenkins
| starring    = Frank Sinatra Faye Dunaway David Dukes George Coe Brenda Vaccaro James Whitmore Martin Gabel
| distributor = Filmways Pictures
| cinematography = Jack Priestley
| released    = October 3, 1980 (premiere)
| runtime     = 112 min.
| country     = United States
| language    = English
| budget      =
}}
The First Deadly Sin is a book written by Lawrence Sanders in 1973 and a 1980 movie produced by and starring Frank Sinatra. The film also features Faye Dunaway, David Dukes, Brenda Vaccaro, James Whitmore, Martin Gabel in his final role and Bruce Willis in his film debut.

The First Deadly Sin was based on the first of a series of popular novels by Sanders. The screenplay, which was adapted from Sanders work, was written by Mann Rubin.    The film was originally slated to be directed by Roman Polanski, who was dropped by Columbia Pictures after statutory rape charges were brought against him. Director Brian G. Hutton took over the production after Polanski fled to France.

The last of nine films produced by Sinatra, and his final starring role,   he plays a troubled New York City homicide cop, Captain Edward X. Delaney. In a small role, Dunaway is the detectives ailing wife, hospitalized during the entire story with a rare kidney affliction. A then-unknown Willis has a bit part, virtually unrecognizable as a hat covers most of his face.

The First Deadly Sin was the third production by Sinatras Artanis production company and was shot on location in New York City. It premiered on October&nbsp;23, 1980 at Loews State Theatre (New York City)|Loews State Theatre in Times Square as part of a benefit for the Mother Cabrini Medical Center. The musical score was by composer and arranger Gordon Jenkins, who had first worked with Sinatra on the 1957 album "Where Are You?"

==Plot==
New York police Captain Edward X. Delaney is at the scene of a brutal murder when news comes that his hospitalized wifes kidney illness has worsened after an operation. Approaching retirement, with a growing depression in the face of his wifes condition, Delaney throws himself into the murder case. He is fascinated by a crime committed with what seems to be a very unusual weapon.

Delaney tries to cross-reference the recent killing with other murders in various parts of the city, looking for a common link. A lack of solid leads leaves Delaney at loose ends until he recruits assistance from an enthusiastic museum curator, a coroner, and the victims wife.

The investigation leads Delaney to a man named Blank, a seemingly normal businessman who is leading a secret life. Delaney uses psychological warfare in an attempt to trap the killer, all while the condition of the policemans wife deteriorates by the hour.

==Cast==
*Frank Sinatra as Edward X. Delaney
*Faye Dunaway as Barbara Delaney
*David Dukes as Daniel Blank
*George Coe as Dr. Bernardi
*Brenda Vaccaro as Monica Gilbert
*Martin Gabel as Christopher Langley
*Bruce Willis as Man Entering Diner (uncredited)

==Critical reception==
The First Deadly Sin failed to make much of an impression at the box office, while some critics were left cold by a slow-burning picture that tried to focus more on character and plot without succumbing to action scenes and shootouts. 

The ending was changed from the novel in which the killer Daniel Blank retreated to a bluff called Devils Needle in upstate New York where he died of dehydration before Delaney and the State Troopers were able to bring him down. Here, a more subtle approach allowed the ending to be more in tune with the rest of the film. Critics Roger Ebert and Leonard Maltin both praised Sinatras performance as one of his best, while daughter Nancy Sinatra commented in her book Sinatra: An American Legend that this was a film her father was very excited about. 

"Who would have thought, in all honesty, that Frank Sinatra still had this performance in him?" wrote Roger Ebert in the Chicago Sun-Times. "The movie is one of the seasons pleasant surprises." Leonard Maltin wrote: "Sinatra in good form in one of his better serious vehicles." 

==Accolades==
* Nominated: Academy of Science Fiction, Fantasy & Horror Films, USA Best Supporting Actor, Martin Gabel
* Nominated: Edgar Allan Poe Awards, The Edgar Award for Best Picture 1st Golden Raspberry Award Worst Actress (Faye Dunaway)

==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 