Making Photoplays in Egypt
 

{{infobox film name = Making Photoplays in Egypt image =  caption =  director = Sidney Olcott producer = Kalem Company writer =  starring =  distributor = General Film Company cinematography = George K. Hollister
| released =  
| runtime = 
| country = United States language = Silent film (English intertitles) 
}}
Making Photoplays in Egypt is a 1912 American silent documentary produced by Kalem Company and distributed by General Film Company. It was directed by Sidney Olcott.

==Production notes==
The documentary was shot in Luxor, Egypt.

==References==
* The Moving Picture World, Vol 4 n°7, p 179; n°8, p 212-213. 
* The New York Dramatic Mirror, February 27, 1909. 

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 

 