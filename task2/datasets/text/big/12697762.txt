Aviv (film)
{{Infobox Film
| name           = Aviv
| image          = 
| image_size     =
| caption        = Aviv Geffen
| director       = Tomer Heymann
| producer       = 
| writer         = Tomer Haymann
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        =
| distributor    =
| released       = 1 April 2003 
| runtime        = 80 minutes
| country        = Israel
| language       = Hebrew with English subtitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 documentary about Israeli singer/songwriter Aviv Geffen.

  Israeli singer/songwriter hash than toys” into a nationally celebrated musician. The film combines old home videos, footage from on stage performances and private interviews in order to track Aviv Geffens musical success and explain his complex personality.

==References==
*{{cite web
  | last = Robinson
  | first = George
  | title = Facts Tell The Stories
  | publisher = The Jewish Week
  | date =March 25, 2005
  | url =http://www.thejewishweek.com/news/newscontent.php3?artid=10666&print=yes
  | accessdate = August 13  }}
*{{cite web
  | title =Israel Rocks: A Journey Through Music of Visions and Divisions
  | publisher= The National Center for Jewish Film
  | url= http://www.brandeis.edu/jewishfilm/Catalogue/films/isrocks.html
  | accessdate = August 13 }}

==See also==
* Israeli rock

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 
 