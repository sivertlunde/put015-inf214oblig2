Alice (2002 film)
{{Infobox film
| name           = Alice
| image          = Alice 2002 film cover.jpg
| alt            = 
| caption        = Film cover.
| director       = Sylvie Ballyot
| producer       = Jean-Philippe Labadie Nathalie Eybrard
| screenplay     = Laurent Larivière
| sound          = Jérôme Florenville(sound)
                   Emmanuel Soland(sound editor)
| starring       = Anne Bargain Lei Dinety Élodie Mennegand
| cinematography = 
| editing        = 
| studio         = Catncage Pictures
| distributor    = Epicentre Films
| released       =  
| runtime        = 48 minutes
| country        = France United Kingdom
| language       = French
}}

Alice is a 2002 French-British film directed by Sylvie Ballyot and produced by Nathlie Eybrard and Jean philippe Labadie about Alice and her sister Manon who is about to be married.

==Synopsis==
Alice has deeply buried feelings and memories for her sister Manon, who is about to be married. During their childhood, the two sisters were extremely close not only Emotional attachment|emotionally, but also sexual attraction |sexually. With news of the wedding, the pain of the past resurfaces and Alices relationship with her girlfriend Elsa start to fall apart. As the wedding approaches, Alice reminisces about her childhood, and has trouble letting go of her incestuous relationship with her sister Manon.

==Cast==
*   as Alice
* Valentine Dubreuil as Young Alice
*   as Manon
* Lucie Lessieur as Young Manon
* Lei Dinety as Elsa
* David Kammenos as Atom
* Alain Lahaye as the Father
* Armelle Legrand as the Mother
* Violetta Ferrer as the Grandmother

==External links==
*  

 
 
 
 
 
 
 

 