Star Wars: Storm in the Glass
 

{{Infobox film
| name           = Star Wars: Storm in the Glass image = Storm in the Glass cover.jpg
| caption        =
| director       =
| producer       =
| eproducer      =
| aproducer      = Dmitry "Goblin" Puchkov
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 2004 (Russia)
| runtime        = 133 minutes
| country        = Russia
| language       = Russian
| budget         =
| followed_by    =
}}
 Dmitry "Goblin" Puchkov. In dubbing the film into Russian, Puchkov altered the plotline, character names, music, and certain visual effects to provide a different (and funny) experience to Russian-speaking audiences.

==Synopsis==
The operational conditions on the boundless open spaces of the Far-Northern Galaxy have rapidly become complicated. On secret planets, persons of unknown nationality (a reference to many Georgian-Armenian and Moldovan illegal immigrants in Russia) have set up manufacture of illegal alcohol on a galactic scale and are preparing an act of aggression against the peaceful planet Marabu.
 Hare Krishna Chukchi rapper, bourgeois occupy the peaceful planet.
 Babooine (which Russian share taxi drivers), during which he is nearly beaten by Grytsko Schumacher. On Babooin, the jedi also fight Javded (a strange man with red skin and small horns, who attacked the jedi after not being satisfied by them not wearing any hats.
 Duma (Russian Operation Storm in the Desert"; also the Russian idiomatic equivalent to "Tempest in a Teapot"). Two divisions of underwater deer-herders, hidden in a fighter Anykey - everything sets up against the provisional government of bourgeois and their army of electronic dummies. During one of the fights, the jedi once again meet with the "red unmannered man" who attacks them once again (supposedly) for not wearing any hats. He is behalved and his lower half of the body (lower abdomen and legs) are identified, while it is a mystery to whom the other half belongs. Only in the "Gods Spark" version of the film does it become clear who is Anykeys real father - it is Pogon, who admits to have "known" (commonly understood as "have/d sex with") Anykeys mother, and later openly explains that Anykey is his only son.

==Characters==
 

*Pogon (Russian for "rank insignia") - one of two jedi sent to Marabu. Original: Qui-Gon Jinn
*Pavian iz Nairobi (Russian for "baboon from Nairobi") - padavan-jedi sent to Marabu. Original: Obi-Wan Kenobi
*Djaga-Djaga Bzdinks (refers to the chorus of  
*"Slimy Swamp Toad" (nicknamed so by surface-dwellers) - chairman of the "Underwater Deer" sovkhoz, likes  
*Queen Zadolbala (Russian slang meaning "sick of  ") - rightful ruler of planet Marabu. Rumored to be a  
*Maid Padla (Russian slang meaning "bastard") - servant girl of Queen Zadolbala, a former  
* 
*E2-E4 (common opening move in  
*Robot Chatterbird (refers to the talking bird in  
*Anykey Skovorodker (first name refers to "press  
*Boris Abramovich (refers to  
* 
*Grytsko Schumacher ("Grytsko" is a common  
*Darth bin Laden (refers to  
*Javdeth (refers to never appeared character of the extremely popular soviet movie  
*Boris Nikolayevich (refers to  
*Cheburan Vissarionovich (refers to  

==Planets==
*Marabu (reference to the  
*Planet of the State Duma - capital planet of the Republic, featuring the planet-wide city of  
*Babooine - desert world populated by  
** 
*Mars - an arid world populated by American robots.

==Other replacements==
*The Crimson   soundtrack ("Prelude Fight") overlaid with a   Neimoidians

==Trivia== Neo can be seen flying by during the Crazy Taxi Races.
*The translation makes numerous references to the Russian science-fiction film Kin-dza-dza!
*The parade scene at the end features a Yoda-like George W. Bush standing near Yoda. Goblin himself can be seen standing behind Mace Windu in the same scene.

==See also==
*Hardware Wars - a spoof of Star Wars.
*Spaceballs - a spoof of Star Wars and several other science fiction films
* Storm in a teacup
*Troops (fan film)|Troops - a 1997 Internet mockumentary parody of Cops (1989 TV series)|COPS set in the Star Wars universe.

==References==
* See generally  
*   - Russian to English translation of   by babelfish on December 27, 2006.
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 