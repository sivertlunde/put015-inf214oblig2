Demonlover
 Demon Lover}}

{{Infobox film
| name           = DEMON
| image          = Demonlover.jpg
| image_size     = Cannes promotional poster.
| director       = Olivier Assayas
| producer       = 
| writer         = Olivier Assayas
| starring       = Connie Nielsen Gina Gershon Chloë Sevigny
| music          = Sonic Youth Darkthrone
| cinematography = Denis Lenoir
| editing        = Luc Barnier
| released       = May 19, 2002 (Cannes Film Festival|CFF) November 6, 2002 (France) September 19, 2003 (United States—limited release)
| runtime        = 115 min. (R-rated) 117 min (unrated cut).
| country        = France Japanese
| budget         = €7,032,000 (estimated) 
}}
 2002 technological thriller film French writer/director Olivier Assayas.  The film stars Connie Nielsen, Charles Berling, Chloë Sevigny, and Gina Gershon with a musical score by Sonic Youth.  It premiered at the 2002 Cannes Film Festival,    although it was more widely released several months later.
 desensitization to noir thrillers. R for strong violence, sexual content and some language.  It was released in both R-rated and unrated directors cut versions on DVD.
 English and some in Japanese Language|Japanese. It has been considered an example of New French Extremity by some journalists. In recent years the film has gained a cult following for its post-modern aesthetics and soundtrack by American rock group, Sonic Youth.

==Plot==
Diane de Monx (Nielsen) is an executive trying to negotiate a deal to acquire the rights to the productions of a Japanese anime studio, which will soon include three-dimensional hentai, for the Volf Corporation.  To facilitate the acquisition, she eliminates her superior, Karen (Dominique Reymond), and assumes control of her portfolio, her business partner Hervé (Berling), and her assistant Elise (Sevigny).  Elise, however, despises Diane and works to frustrate her negotiations at every opportunity. Diane and Hervé travel to Japan to close the deal, and they enjoy a sexual flirtation which is unfulfilled at that time and seem to grow to like one another.

Having acquired the rights, the Volf Corporation attempts to enter into a deal for distribution with an American Internet company called Demonlover, represented by Elaine Si Gibril (Gershon). Diane, however, has actually been a spy all along for Demonlovers main competition, Mangatronics, meeting with a mysterious handler on occasion to pass along information on the Demonlover deal. Meanwhile, Diane discovers that Elaines company is a front for a website called the Hellfire Club, an interactive torture web site dealing with extreme sadomasochism broadcast in Real-time (media)|real-time. When confronted with these charges, Demonlover praises Hellfire Club but claims no ties to it whatsoever.

In order to seal the deal for Mangatronics, Diane is sent by her handler to steal data from the computer in Elaines hotel room. Before Diane can download the information, Elaine enters the hotel room and notices Dianes presence. They struggle, eventually culminating in the suffocation of Elaine. Diane checks to see if she can make an escape, but then discovers Elaines body is missing. Elaine, who was not dead, uses the last of her strength to club Diane over the head. Diane is knocked unconscious, and Elaine passes out due to blood loss sustained from injuries in the struggle. When Diane awakens, she is in Elaines hotel room, and everything is completely cleaned up. There is no evidence of a murder, burglary, or struggle.

At this point, the narrative structure of the film more or less breaks down, although we do learn a great deal more about the characters. It is revealed that Demonlover does indeed own the Hellfire Club. It is also revealed that Elise, who it is suggested is a spy for Demonlover, actually works for Hervé, who is also likely associated with Demonlover and by extension the Hellfire Club. Hervé admits as much on a date with Diane later in the film. The two become intimate in bed, but Hervé goes too far and rapes Diane. However, the second time she is raped, Diane manages to reach over into her handbag, into which she had previously placed a shotgun. Diane fires at Hervés temple and he dies instantly. In the end, Diane herself is forced into the Hellfire Club. She awakens in a dungeonlike room, on a mattress, dressed in a vinyl suit and with a wig. Beside the mattress there are pictures of Diana Rigg as Mrs. Peel in the Avengers (a 1960s television show, which had an episode entitled "The Hellfire Club" that was subsequently banned from American TV for the so-called "provocative" nature of Mrs. Peels costume). Diane attempts to escape, and is almost successful. However, upon driving her getaway car she is involved in a car accident. The escape fails.

The final scene takes place in an American household. A teen-aged boy logs on to the Hellfire Club website using his fathers credit card. He then fills out a detailed fantasy of what he would like done to the woman on the screen, who turns out to be Diane. He then allows it to play in the background as he does his science homework. Diane looks up at the camera in her room, helpless, but also, in a sense, suggesting an indictment of the character. The final shot pans from the computer screen – a window from which Diane looks out helplessly – to the boys science homework – answering problems while handling a DNA model that he seems to have constructed.

==Cast==
* Connie Nielsen as Diane de Monx
* Charles Berling as Hervé Le Millinec
* Chloë Sevigny as Elise Lipsky
* Dominique Reymond as Karen
* Jean-Baptiste Malartre as Henri-Pierre Volf
* Gina Gershon as Elaine Si Gibril
* Edwin Gerard as Edward Gomez
* Thomas M. Pollard as Avocat américain
* Abi Sakamoto as Kaori—la traductrice
* Naoko Yamazaki as Eiko
* Nao Omori as Shoji (as Nao Ohmori)
* Jean-Pierre Gos as Verkamp—Contact Diane
* Julie Brochen as Gina—Amie de Diane
* Randall Holden as Ray
* Alexandre Lachaux as Erwan—Broker #1

==Alternate versions== R rating due to the highly explicit and sexual nature of some of the scenes.  Additionally, this R-rated release featured heavy pixelization over the hentai scenes shown at the Japanese animation studio.

When the film was released on Region 1 DVD on March 16, 2004, it was in this R-rated cut.  Several months later, a two-disc "unrated directors cut" appeared.  This cut removed most of the hentai pixelization (although penetration scenes are still blurred) and restored some scenes of footage from the Hellfire Club website.  This cut runs 117 minutes as opposed to the R-rated versions running time of 115 minutes.  This version was released on Region 4 DVD with an MA15+ rating and later aired on Australian television with the equivalent Television content rating systems|AV15+ rating.  As a bonus feature on the two-disc edition, a secret code (found in the text printed on the DVD itself) can be entered to gain access to the unedited Hellfire Club footage.

Internet rumors indicated that a third DVD edition of the film would be released, featuring the original Cannes cut of the film, and without any scenes pixelated, but this has so far failed to materialize.

==Themes==
One of the themes of the film is the desensitization to violent or disturbing imagery, both real and simulated, in the modern viewer. This is evident from the first scene of the movie, in which high-salaried executives are discussing a business deal on an airplane, completely unfazed by the explosions on the small video screens hanging from the ceiling. When Diane watches schoolgirl pornography in her hotel room in Japan or first checks out the Hellfire Club website, she hardly even stirs. Similar non-reactions can be seen in the characters when the two- and three-dimensional hentai animations are demoed, Elise plays video games in her bedroom, and in the final scene of the film.

==Reception==
Reviews were decidedly mixed: the website   aggregated a 48% rating from critics.

==References==
 

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 
 