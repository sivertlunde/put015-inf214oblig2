On the Job (2013 film)
{{Infobox film
| name         = On the Job
| image        = On the Job Philippine theatrical poster.jpg
| border       = yes
| alt          = 
| caption      = Theatrical poster for the Philippine release
| director     = Erik Matti   
| producer     = 
{{ plainlist |
* Leonardo T. Po 
* Dondon Monteverde
* Malou Santos
* Charo Santos-Concio
}}
| writer       = 
{{ plainlist |
* Michiko Yamamoto
* Erik Matti
}}
| music        = Erwin Romulo
| cinematography = Francis Ricardo Buhay III
| editing      = Jay Halili
| starring     = 
{{ plainlist |
* Piolo Pascual
* Gerald Anderson
* Joel Torre
* Joey Marquez
}}
| studio =
{{ plainlist |
* Reality Entertainment
* Star Cinema
}}
| distributor  = 
{{ plainlist |
* Star Cinema  (Philippines) 	
* Wild Side Films  (France) 	
* Well Go USA Entertainment  (North America) 	
* Madman Entertainment  (Australia) 
* XYZ Films  (worldwide) 
}}
| released     =  
| runtime     = 121 minutes
| country     = Philippines
| language    =  
| budget      = 
{{ plainlist |
* Philippine peso|₱ 47 million   
*  (United States dollar|US$ 1.08 million) 
}}
| gross      = PHP13,459,037 (Philippines)    $164,620 (US) 
}} Filipino action action thriller film directed by Erik Matti. The film stars Piolo Pascual, Gerald Anderson, Joel Torre and  Joey Marquez.The film is a co-production of Star Cinema and Reality Entertainment as part of Star Cinemas 20th year anniversary presentation.

==Plot== NBI Agent Coronel through Congressman Manrique, who is Coronels father-in-law. Coronels father, also a policeman, died amid rumors of corruption. When Coronel and his partner, Bernabe, arrive at the local precinct, Sgt. Acosta, a 20-year veteran who feels the case was taken from him for political reasons, resists them.

Mario and Daniel are released and kill a woman named Linda. Upon hearing of his wifes murder, Lindas husband, Pol,  immediately calls Acosta. Acosta reveals that Tius murder is just one of several murders ordered by General Pacheco, a high-ranking military officer who is running for the Senate, and a close friend of Manriques. Pacheco runs a murder-for-hire operation using prisoners, killing anyone else involved to protect his campaign. Acosta agrees to protect Pol and heads to the station. While Coronel and Bernabe  confront Acosta, Daniel shoots Pol, but his gun jams before he can deliver the fatal shot. The three officers converge on them, forcing Daniel and Mario to flee. At a hospital, Daniel creates a distraction and allows Mario to kill Pol. The two split up and flee. Coronel and Bernabe chase Daniel, and Acosta goes after Mario. As the two escape, Bernabe is shot and Mario sprains his ankle.

When Coronel discovers Marios identity, he visits Marios wife, who is with her lover, Bhoy. Marios wife does not talk, but Coronel relays the information about her affair to Acosta, who unsuccessfully attempts to use it as leverage against Mario during an interrogation in jail.

Coronel confronts Manrique at a Reform Party presentation and explains that he intends to arrest Pacheco, but Manrique warns Coronel that they will both fall with Pacheco, as Manrique was one of Pachecos clients. Coronel leaves in frustration and decides to continue working with Acosta. Acosta tells Coronel that his father was not corrupt and was killed while trying to expose corruption; Acosta himself was demoted for his part. 

Coronel and Acosta discover that Tius father can provide them with the evidence they 
need to arrest Pacheco. On the way, Coronel abandons Acosta to visit Pacheco, who admits that he killed Coronels father. Coronel is secretly recording this conversation with Pacheco on his cell phone. After recording this conversation, Coronel goes home and tells his wife "Im sorry, Pacheco is going down." She slaps him repeatedly. Pachecos men kill Tius father. 

The next day, Daniel murders Coronel in front of the police headquarters. It is not clear whether Pacheco decided to kill Coronel because they had bugged his car and discovered the cell phone recording, because they received a tip from Coronels wife, or simply because they did not trust him. Enraged, Acosta assaults Manrique and Pachecos security detail, which results in a brief shootout until Pacheco orders them to cease fire. On the day Mario is to be released, he realizes that he has nothing on the outside, as his family no longer wants anything to do with him. In order to stay in jail, he stabs Daniel to death. The people mourn Coronels death, and Acosta is investigated for Coronels murder. Mario visits his home, kills Bhoy, and returns to prison. This is how the international version of the film ends. 

The Filipino version of the film ends with Bernabe looking through Coronels possessions and finding the cell phone that Coronel used to secretly record an incriminating conversation with Manrique and Pacheco. Bernabe requisitions the cell phone as evidence.

==Cast==
 
* Piolo Pascual as Attorney Francis Coronel, Jr.
* Gerald Anderson as Daniel Benitez
* Joel Torre as Mario "Tatang" Maghari
* Joey Marquez as Sergeant Joaquin Acosta
* Michael de Mesa as Congressman Manrique
* Leo Martinez as General Pacheco
* Angel Aquino as Lolet
* Vivian Velez as Thelma
* Shaina Magdayao as Nicky Coronel William Martinez as Rex
* Rayver Cruz as Bernabe
* Empress Schuck as Tina
* Dawn Jimenez as Diana
* Rosanna Roces as Joaquins wife
* JM de Guzman as Boyet, Joaquins son
* Lito Pimentel as Paul
* Michael Flores as Freddie
* Al Tantay as Niel Salcedo, Chief of Police
* Niño Muhlach as Ramon
* Baldo Maro as Esteban
* Mark Andaya as Simeon
* Mike Castillo as Badong
* Joseph Marco as Ruel
* Stephen Ku as Johnny Tiu
* Joger Go as Charlie Tiu
* Cristy Fulgar as Linda Carag
 

==Soundtrack==
* Juan Dela Cruz – Maskara
* Dong Abay - Perpekto
* Dong Abay - Mateo Singko
* Juan Dela Cruz – Pinoy Blues
* Bent Lynchpin – Deep Seated Contempt
* Bent Lynchpin - Terraform Archivist
* Bent Lynchpin - Things Unknown
* Bent Lynchpin - Between Two Points
* Caliph8 – Rested Note
* Caliph8 – Struck a Responsive Chord
* Caliph8 – Quadrille for Five
* Fred Sandoval – Year of the RatFred Sandoval - ClapFred Sandoval - Grit (Dindi’s Blues)
* Pasta Groove – Gabi
* Francis M. and Hardware Syndrome – Ayoko sa Dilim
* Armi Millare and Ely Buendia – This Will Be Broken
* Armi Millare and Ely Buendia – Pasion

==Development==

===Production===
Erik Matti was encouraged by Todd Brown of Twitch Film to start the screenplay. Brown also promised Matti that he will provide potential investors to the project. This however, was not pushed through after the first draft of the script was not yet completed. Star Cinema initially declined to co-produce the film. However, the company had changed its mind after Matti had revised the script. Later on, Matti asked Piolo Pascual of the lead role. Pascual was responsible for Gerald Andersons casting in the film. Joel Torre, was already offered for his role in the film even prior to the completion of the screenplay.  Four uncredited consultants were also asked to help and thoroughly shape up the some details in the story. 

===Distribution and release===
 
The film was screened and had a world premiere in the Directors Fortnight at the 2013 Cannes Film Festival in Cannes, France.     The Directors Fortnight is a noncompetitive, independently programmed section of the film festival. 

The film will be distributed by Star Cinema locally starting on August 28, 2013.   It will also be released in North America in the fall of 2013 and will be distributed by Well Go USA Entertainment.       The deal was brokered by Pfardrescher of Well Go USA Entertainment, and Nate Bolotin and Aram Tertzakian of XYZ Films (producer for the American adaptation).    "On The Job reiterates that it is an exciting time for Filipino cinema," said Doris Pfardrescher. "We are thrilled we were able to acquire the movie before its premiere in Cannes and can’t wait to bring it to audiences in North America.    Aside from North America, it will also be released in France by Wild Side Films,  and in Australia by Madman Entertainment. 
 US dollars or 12 million Philippine peso|pesos. Also included in the deal with the North American distribution company, Well Go USA Entertainment, is that the company will be responsible for DVD/Blu-Ray and video on demand distributions. 

==Reception==
It was well-received both by local and foreign critics. A US remake is currently under pre-production.

===Critical responses and reviews===
Rotten Tomatoes, a review aggregator, reports that List of films with a 100% rating on Rotten Tomatoes|100%  of fourteen surveyed critics gave it a positive review; the average rating was 6.7/10.   According to Justin Chang, a senior film critic in Variety (magazine)|Variety, "...On the Job is a gritty, convoluted but steadily engrossing crime thriller from Filipino genre maven Erik Matti. Although this fast-paced actioner takes a while to sort out its parallel plotlines, extending from an unusually porous prison system to the highest political offices, it ultimately fires on all cylinders as a tense, well-acted B-movie whose strong local flavor is unlikely to survive the inevitable offshore remake."  According to a review by Film Business Asia, "Its a movie that slowly sucks the audience in as the structure becomes clearer — a gamble by Matti that pays off in the end but could still take a few trims and clearer organisation in the first half. What keeps the viewer hooked are the performances". 
 Screen Daily said that, "Director and co-writer Erik Matti seems more comfortable with staging action sequences and generating noir-like atmosphere than creating nuanced characters or subtle dialogue but the basic story of crime and corruption at the heart of a rotten society is involving enough to forgive any infelicities."  Hunter called it "essential viewing on its home territory". 

In the Philippines, GMA Network said that "the underlying pleasure here lies in seeing mainstream actors performing against type in a story that leaves no character unscathed." 

===Accolades===

====Awards====
Aside from the Cannes Film Festival, the film was also screened at the 17th Puchon International Fantastic Film Festival in Bucheon, South Korea.  At the said film festival, Joel Torre received the Best Actor award, and the film receiving the Jury Prize.  

====Recognitions====
The film was one of twenty one selected feature films screened in the Directors Fortnight at the 2013 Cannes Film Festival in Cannes, France from May 16 until 26, 2013.   Though it did not win the Caméra dOr prize, the film was well received by the viewers and was given a two-minute standing ovation.  

The film was also reported to be part of the second installment of the The ABCs of Death, an  American horror film anthology produced by Ant Timpson and Tim League. 

==Future==
The film is reported to have a US adaptation and will be directed by Baltasar Kormákur, the person who also directed Contraband (2012 film)|Contraband and 2 Guns.       The remake will be produced by Kormákurs Blueeyes Productions. XYZ Films, the production and sales company that represents the international rights to the film, will also co-produce and will release the remake film worldwide. 

Matti is also planning to make a sequel to the film. 

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 