Naughty Marietta (film)
 

{{Infobox film | name = Naughty Marietta
  | image = Poster - Naughty Marietta 05.jpg
  | caption = Lobby card
  | director = Robert Z. Leonard W.S. Van Dyke
  | producer = Hunt Stromberg W.S. Van Dyke
  | writer = Frances Goodrich Albert Hackett John Lee Mahin
  | starring = Jeanette MacDonald Nelson Eddy Elsa Lanchester Douglass Dumbrille
  | music = Dimitri Tiomkin Victor Herbert
  | cinematography = William H. Daniels
  | editing = Blanche Sewell
  | distributor = Metro-Goldwyn-Mayer (MGM)
  | released =  
  | runtime = 105 minutes
  | country = United States
  | language = English
  | budget = $782,000   
  | gross = $1,058,000 (Domestic earnings)  $999,000 (Foreign earnings) 
  }}
 Naughty Marietta by Victor Herbert. Jeanette MacDonald stars as a princess who flees an arranged marriage. She sails for New Orleans and is rescued from pirates by Captain Richard Warrington (Nelson Eddy).

Five of Victor Herberts most famous songs come from the score of Naughty Marietta, with words by lyricist Rida Johnson Young:
* "Ah! Sweet Mystery of Life"
* "Italian Street Song"
* "Neath the Southern Moon"
* "Im Falling in Love With Someone"
* "Tramp! Tramp! Tramp! (Along the Highway)"

Additional lyrics for several of Herberts songs were penned for the film by Gus Kahn. The film was written by Frances Goodrich, Albert Hackett, John Lee Mahin and Rida Johnson Young.

==Plot==

To avoid an arranged marriage to Don Carlos, an elderly Spanish duke, Princess Marie masquerades as her uncles former servant, Marietta, and escapes from France on a ship with casquette girls who are traveling to New Orleans to marry colonists.  On board, Marietta befriends Julie.

En route, the women discuss what type of man they want to marry. "Marietta" shocks the other girls by stating that she does not intend to get married to anyone.  Shortly after, the ship is boarded by pirates, who kill the entire crew and take the girls ashore.

After the pirates divide the loot, they turn their attention to the girls.  Just then, singing is heard ("Tramp! Tramp! Tramp!").  The pirates extinguish their torches and fire to try to avoid detection, but Marietta takes one of the torches and runs towards the sound of the singing, crying out "help, help". Mercenaries rout the pirates and rescue the women.

The mercenaries leader, Captain Richard Warrington, sings "Neath a Southern Moon" to Marietta.  Despite his attraction to her, however, Warrington declares that he does not intend to get married.

Warrington and his men take the casquette girls to New Orleans, where they are welcomed by the Governor. The women are housed in the convent while they get to know their potential husbands.  When some men approach Marietta, she declares that she does not want to marry any of them. The Governor feels that he has seen Marietta before in Paris, but she denies it. When she pretends to have a disreputable past, the Governor orders a pair of soldiers to escort her away in disgrace. Warrington relieves them of their duty and finds her a place to stay, even paying the first months rent.  Though Marietta tries to rid herself of Warrington, he is undaunted. Just then, a group of gypsies stroll by, advertising their Marionette Theater. The gypsy leader, Rodolpho, has his daughter sing, and Warrington joins in ("Italian Street Song").  Stung by Warringtons remark that she might not be able to sing as well as the gypsy, Marietta surprises him by doing so beautifully. While he is distracted getting rid of three would-be suitors, she slips away.

The following day, Warrington discovers that Marietta is working at the Marionette Theater.  When he visits her after the performance, Marietta tells him that his presence is "most unwelcome". The captain asks her if he would be welcome "somewhere else"; Marietta answers "yes".  As she goes out for lunch, Warrington joins her, noting that he is "somewhere else", and "Here I am – welcome me".  She is unable to suppress a smile, indicating she has changed her opinion of him. Soon after, however, a large award is offered for information about her whereabouts.  Warrington persuades her to trust him, and takes her away by boat. During this time together, they discover that they are falling in love with each other ("Im Falling in Love with Someone"). When Warrington asks Marietta to sing the song back to him, she says that she has a song she knows better. Later, however, they are found by French soldiers, and her true identity is revealed. Her uncle and Don Carlos are expected on the next ship to take her back.

Marietta is to attend a ball arranged by the Governor in her honor.  Julie comes to see her; she tells Marietta that Warrington had been ordered to leave New Orleans that day, but intends to come to the ball.  Her uncle warns her that "if Warrington attempted to see her again, he would be arrested for treason and shot".  Marietta asks Julie to stop Warrington from coming, but they realize it is too late when they hear him and his men singing ("Tramp! Tramp! Tramp!").

When Warrington enters the ballroom, the Governor tries to get him to leave in order to save his life.  After the captain dances with Marietta, she tells him that she will sing her song to him the following evening. She pretends to have been toying with him to deceive her uncle. When Warrington is leaving, Marietta sings "Ah, Sweet Mystery of Life", joined by Warrington. The lovers then flee to the wild frontier.

==Cast==
* Princess Marie de Namour de la Bonfain (Marietta Franini) – Jeanette MacDonald
* Captain Richard Warrington – Nelson Eddy
* Governor Gaspard dAnnard – Frank Morgan
* Madame dAnnard – Elsa Lanchester Douglas Dumbrille
* Herr Schuman – Joseph Cawthorne
* Julie – Cecilia Parker
* Don Carlos de Braganza – Walter Kingsford
* Frau Schuman – Greta Meyer
* Rudolpho – Akim Tamiroff
* Abraham "Abe" – Harold Huber
* Ezekial "Zeke" Cramer – Edward Brophy
* Servant to Princess Maries uncle (the real Marietta Franini) – Helen Shipman

==Awards==
It was nominated for the Academy Award for Best Picture. Douglas Shearer won the Academy Award for Sound for his work on the picture.    In 2003, was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant."

==In popular culture== Thoroughly Modern Millie.

In the 4th season episode "Archie The Gambler" of All In the Family, "Ah! Sweet Mystery of Life" is sung by cast members Jean Stapleton, Rob Reiner, and Sally Struthers. In the Woody Allen film Bananas, a political prisoner is tortured by being forced to listen to the score of Naughty Marietta over and over.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 