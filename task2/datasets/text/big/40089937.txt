Vu (film)
{{Infobox film
| name           = Vu
| image          = 
| alt            = 
| caption        = 
| director       = Ashik H
| producer       = Nandha Kumar,  B. Ramaswami,  Ashok Kumar
| writer         = Ashik H
| starring       = Thambi Ramaiah Varun Madhan Simile Selva Kaali Venkat
| music         = Abhijith Ramaswami
| cinematography = Jaya Prakash. N
| editing        = Kamal G 
| studio         = Phoenix Pictures
| distributor    = 
| released       = February 7, 2014
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
Vu is a 2014 Tamil comedy film directed by debutante Ashik, a student of Film technology from MGR Govt. Film and TV Institute in Chennai. This film features Thambi Ramiah for first time in lead role,  alongside newcomers Varun, Madhan Gopal, Kuran, Simile Selva, Sathyasai. The film was released in February 7, 2014.

==Cast==
* Thambi Ramaiah as Ganesh
* Varun
* Madhan
* Simile Selva
* Neha
* Sathya Sai
* Rajkamal
* Soundararaj
* Kaali Venkat
* RajaSiva
* Deepz
* Madhumitha
* "Super Singer" Aajeedh Khalique
* Rishikanth
* Doomz Khanna

==Production==
Producer and director Ashik: I graduated in Visual Communication and followed it up with a diploma in film technology at the Chennai Film Institute. My short film Vanjam got the best screenplay award at the Hungary Film Festival. I have also made eight other short films. When I narrated this script to National award winner Thambi Ramaiah, he not only appreciated it but also gave me the dates to shoot his portions, Although he is the main protagonist, the role cannot be considered so as there is no hero in this film. As for the story, Thambi Ramaiah’s character aims at achieving something he himself knows will be difficult.  As for the title, people write ‘Vu’ to denote the Pillaiyar Suzhi.

Hero Varun: Director Ashik and I were friends in Vaishnava College. I was part of the theatre group Koothu-p-Pattarai for some time but this is the first time I am facing the camera. The heroine and I sport three looks in the film.

Music director Abhijith Ramaswami: I was part of my college band. I learnt the nuances of music direction from Sadanandam, a guitarist in Ilaiyaraja’s orchestra. I scored the music for Ashik’s short film Vanjam and he liked my work and gave me the chance to work on his feature film. 

Heroine Neha: A Chennai girl, she was studying for her BBA in Puducherry when she got the opportunity to fulfil her dream. A mutual friend introduced her to director Ashik who signed her up for his Vu. 

==Soundtrack==
Music is composed by debutant Abijith Ramaswami, lyrics for all songs were written by Murugan Manthiram. Thambi Ramaiah has sung a song called "Oru Padi Mela".  "Thikki Thenarudhu" was sung by Aajeedh, winner of Vijay TV Airtel Super Singer Junior 2012 apart from acting in the film.   The newly elected Directors’ Union president, director Vikraman released the audio, while  the south head of UTV Motion Pictures Dhananjayan received it, PL Thenappan, SS Kumaran, GNR Kumaravelan graced the event.  Behindwoods wrote:"Sparkles in bits". 

* Kaalin Keezhey - CG Krishnan
* Thikki Thenurudhu - Ajeedh, Sruthi
* Oru Padi Mela - Thambi Ramaiah, Arun, Ashik, CG Krishnan
* Aahaa Idhu Cinema - Mukesh
* Chinna kuzhandhai - Velmurugan
* Thikki Thenurudhu (Seniors) - Aditya Kashyap, Vandhana

==Critical reception==
*Indian Express wrote:" Innovative in his thinking, debutant director Ashik seems to be bursting with ideas. If only they were all put together in a more coherent and a consistent manner". 
*Times of India wrote:"Its premise is filled with the promise of a madcap film that could also serve as a commentary on the industrys follies. But what we get is an amateurish comedy that is only infrequently funny and often exasperating". 



==References==
 

==External links==
*  

 
 
 