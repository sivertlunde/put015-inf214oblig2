Sunil Vayassu 20
 

{{Infobox film
| name           = Sunil Vayassu 20
| image          =
| alt            =
| caption        =
| director       = K. S. Sethumadhavan
| producer       = Kamal  Sujatha
| narrator       = Rahman Urvashi Uravashi Venu Nagavalli M. G. Soman
| music          = Jerry Amaldev
| cinematography = J. Williams
| editing        = N. Gopalakrishnan
| studio         =
| distributor    =
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by KS Sethumadhavan. The film stars  Rahman, Urvashi (actress)|Urvashi,  and Venu Nagavally in lead roles. The film had musical score by Jerry Amaldev.    It is based on a story with same name by Tamil Writer Sujatha which was serialized in a Tamil Magazine. The movie is about a neglected teenager who kidnaps a young girl.

==Plot==
Sunil, a drug-addicted spoiled brat kidnaps five-year-old girl called Neela from a middle-class family. Later Sunil makes friendship with the girl who calls him uncle. Neelas father was a policeman. In the end, Sunil surrenders himself. Sunil promises to Neela that he will return to her after 10 years.

==Cast== Rahman as  Sunil Urvashi as  Premalatha
*Venu Nagavalli as  Jayakumar
*M. G. Soman as  Somashekharan
*Nedumudi Venu as  Military Sir Innocent as  Anthony
*K.P.A.C. Sunny as  Police Officer
*T. P. Madhavan as  Sunils Father
*Valsala Menon as  Sunils Mother

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by P. Bhaskaran.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kadha kadha || K. J. Yesudas || P. Bhaskaran ||
|-
| 2 || Naadam madhuram || K. J. Yesudas, Chorus || P. Bhaskaran ||
|- Vincent || P. Bhaskaran ||
|-
| 4 || Thennale thennale || KS Chithra || P. Bhaskaran ||
|}

==References==
 

==External links==
*  

 
 
 
 


 
 