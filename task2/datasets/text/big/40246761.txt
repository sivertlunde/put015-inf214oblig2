A Heartbeat Away
{{Infobox film
| name           = A Heartbeat Away
| image          = 
| image size     =
| caption        = 
| director       = Gale Edwards
| producer       =Chris Fitchett
| writer         = Julie Kincaide
| based on = 
| narrator       =
| starring       = William Zappa Isabel Lucas Sebastian Gregory Tammy McIntosh
| music          = 
| cinematography = 
| editing        = Matt Villa
| studio = Pictures in Paradise
| distributor    = Hoyts
| released       = 2011
| runtime        = 
| country        = Australia English
| budget         = $7 million  
| gross = $118,658   accessed 24 May 2014 
| preceded by    =
| followed by    =
}}
A Heartbeat Away is a 2011 Australian musical comedy film about a marching band in a small town.
==Production==
Filming started in March 2010. The film was financed by Screen Australia, Screen Queensland, Cutting Edge Pty Ltd and Quickfire Films (UK).   accessed 14 August 2013 

==Reception==
The film was a box office disappointment, widely considered a commercial and critical flop. During its opening weekend it took $44,204 across 77 screens, giving it a screen average of just $574.  

==References==
 

==External links==
*  at At the Movies
* 
* 
* 

 
 
 
 
 

 