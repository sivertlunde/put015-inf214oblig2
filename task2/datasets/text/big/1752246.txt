Live Free or Die Hard
{{Infobox film
| name            = Live Free or Die Hard
| image           = Live Free or Die Hard.jpg
| caption         = Theatrical release poster
| director        = Len Wiseman
| producer        = Michael Fottrell
| screenplay      = Mark Bomback
| story           = {{Plainlist |
*Mark Bomback
*David Marconi
}}
| based on        = {{Plainlist |
* 
* 
}}
| starring        = {{Plainlist |
*Bruce Willis
*Justin Long
*Timothy Olyphant
*Cliff Curtis
*Maggie Q
*Mary Elizabeth Winstead
}}
| music           = Marco Beltrami  
| cinematography  = Simon Duggan
| editing         = Nicolas De Toth
| studio          = {{Plainlist |
*Cheyenne Enterprises
*Dune Entertainment
*Ingenious Film Partners
}}
| distributor     = 20th Century Fox
| released        =  
| runtime         = 130 minutes 
| budget          = $110 million   
| gross           = $383.5 million 
| country         = United States
| language        = English
}}

Live Free or Die Hard (released as Die Hard 4.0 outside North America) is a 2007 American action film, and the fourth installment in the Die Hard (film series)|Die Hard film series. The film was directed by Len Wiseman and stars Bruce Willis as John McClane. The films name was adapted from New Hampshires state motto, "Live Free or Die".

McClane is attempting to stop cyber terrorists who hack into government and commercial computers across the United States with the goal to start a "fire sale" of financial assets. The film was based on the 1997 article "A Farewell to Arms" written for Wired (magazine)|Wired magazine by John Carlin.    The films North American release date was June 27, 2007. 
 CGI in his stunt double were injured.

Unlike the prior three films in the series, the Motion Picture Association of America film rating system|U.S. rating was PG-13 rather than R. An unrated version contained more strong profanity and violence not shown in the theatrical version, and was included in the DVD release.

Reviews were positive with an 81% approval rating on Rotten Tomatoes and 69/100 from Metacritic. The film earned total international box office gross receipts of $383.4 million, making it the highest-grossing film in the Die Hard series. It debuted at #2 at the U.S. box office.   
 rip discs by offering them a downloadable version with studio-imposed restrictions. The score for the film was released on July 2, 2007. The fifth film in the series, titled A Good Day to Die Hard, was released on February 14, 2013.   

==Plot== FBI responds computer hackers, finding several of them have been killed. Taking others into protective custody, the FBI asks New York City Police Department detective John McClane (Bruce Willis) to bring in Matthew "Matt" Farrell (Justin Long), one of the targeted hackers. McClane finally arrives just in time to prevent Farrell from being killed by assassins working for a mysterious cyber-terrorist named Mai Linh (Maggie Q), working for her boss and love interest, Thomas Gabriel (Timothy Olyphant).

En route to Washington, D.C. with McClane, Farrell reveals that he received a large sum of money from Mai to write an algorithm that can crack a security system. As McClane and Farrell arrive in Washington, Gabriel orders his own crew of hackers to take control of the transportation grids and stock market, while nationally broadcasting a message threatening the United States. Farrell recognizes this as the start of a "fire sale", an attack designed to target the nations reliance on computer controls, such that "everything must go". McClane and Farrell are taken by police escort to the secure FBI headquarters, but Mai reroutes the convoy into the path of an assault helicopter. McClane takes down the helicopter by launching a police car over a damaged toll booth into it.

As McClane and Farrell finally recover, Gabriel initiates a second broadcast by showing a simulated explosion of the United States Capitol|U.S. Capitol building, sending the public into panic. Farrell recognizes that his next target of the "fire sale" is likely the power grid, and the two drive to a utility superstation in West Virginia. When they arrive, a team led by Mai is breaking in and taking over the stations controls. McClane kills all of them, and sends Mai to her death in a vehicle at the bottom of an elevator shaft. He obtains video footage of Gabriel which he relays to the FBI. Enraged over Mais death, Gabriel takes remote control of the natural gas distribution system and redirects all of the gas into the station. McClane and Farrell manage to escape before the station explodes, leaving much of the eastern seaboard without power.
 NSA facility, designed by Gabriel when he worked for the NSA as a backup facility for the nations entire personal and financial records to be used in the event of a cyber attack similar to the one Gabriel has created. Warlock tells McClane and Matt that Gabriel is an extremely talented programmer who was a top security expert for the United States Department of Defense|U.S. Defense Department. Gabriel tried to alert the politicians and military leadership to critical weaknesses that left Americas network infrastructure open to cyberwarfare, but his methods led to his dismissal. Warlock runs a traceroute and attempts to identify Gabriels location, but Gabriel detects the intrusion.
 semi and enlists Warlock to trace Gabriels vehicle. With McClane in pursuit, Gabriel accesses the communication system of a United States Marine Corps|U.S. Marine Corps F-35B|F-35B Lightning II.

Pretending to be an air traffic controller, Gabriel orders the pilot to attack the truck McClane is driving. McClane manages to escape the assault when debris from the damaged truck fall into the jet intake of the aircraft from a highway exit ramp. McClane makes his way to a nearby warehouse where Warlock has successfully tracked Gabriel and is installing the remaining files, holding Lucy. Although McClane kills most of Gabriels remaining henchmen, Emerson (Edoardo Costa) shoots him in the right shoulder, injuring him.

While Gabriel tries to hold McClane from behind, McClane manages to gain control of Gabriels weapon and fatally shoots him through his own shoulder, killing him, but he survives his injury. Farrell then kills Emerson in the ensuing confusion. As the FBI finally arrives to tend to his wounds and clean up the scene, recovering the stolen files in the process, McClane pretends to be displeased with the romantic feelings developing between Farrell and Lucy as he sits with her in the ambulance van.

==Cast==
* Bruce Willis as Detective John McClane
* Justin Long as Matthew "Matt" Farrell, a computer hacker from Camden, New Jersey
* Timothy Olyphant as Thomas Gabriel, a former United States Department of Defense|U.S. Defense Department analyst who leads a group of cyber-terrorists systematically shutting down the entire U.S. infrastructure. Olyphant filmed his role within three weeks.   
*  .    It was speculated that Willis real life daughter Rumer Willis|Rumer, who was born the same year that Die Hard was released, was a prime candidate for the part of McClanes daughter.    Jessica Simpson,    Britney Spears,    and Taylor Fry,    who played McClanes daughter in Die Hard, had all previously auditioned for the role.
* Maggie Q as Mai Linh, Gabriels primary accomplice and girlfriend
* Kevin Smith as Frederick "Warlock" Kaludis, Farrells hacker friend. Smith, a noted writer and director, did uncredited rewrites to the scenes in which he appears, as Willis and Wiseman thought the dialogue and exposition "didnt make sense".
* Cliff Curtis as Miguel Bowman, Deputy Director of the F.B.I.s Cyber Crime Division
* Jonathan Sadowski as Trey, Gabriels main hacker
* Edoardo Costa as Emerson, Gabriels main henchman
* Cyril Raffaelli as Rand, Gabriels henchman
* Yorgo Constantine as Russo, Gabriels henchman
* Chris Palermo as Del, Gabriels henchman
* Andrew Friedman as Casper, a computer hacker who is working with Gabriel
* Željko Ivanek as Agent Molina, Bowmans assistant
* Christina Chang as Taylor, an F.B.I. agent working for Bowman
* Sung Kang as Raj, an Asian-American F.B.I. employee
* Allen Maldonado as Goatee
* Tim Russ as Agent Summer
* Matt McColm as Gabriels henchman

==Production==

===Script and title=== Enemy of the State.    Using John Carlins Wired magazine article entitled "A Farewell to Arms", Marconi crafted a screenplay about a cyber-terrorist attack on the United States.     The attack procedure is known as a "fire sale", depicting a three-stage coordinated attack on a countrys transportation, telecommunications, financial, and utilities infrastructure systems. After the September 11, 2001 attacks, the project was stalled, only to be resurrected several years later and rewritten into Live Free or Die Hard by Doug Richardson and eventually by Mark Bomback.   

Willis said in 2005 that the film would be called Die Hard 4.0, as it revolves around computers and cyber-terrorism.   later announced the title as Live Free or Die Hard and set a release date of June 29, 2007 with filming to begin in September 2006.       The title is based on New Hampshires state motto, "Live Free or Die", which is attributed to a quote from General John Stark. International trailers use the Die Hard 4.0 title,    as the film was released outside North America with that title. Early into the films DVD commentary, both Wiseman and Willis note a preference for Die Hard 4.0, and subtly mock the Live Free or Die Hard title.   

===Visual effects=== The Orphanage, R!ot, Pixel Magic, and Amalgamated Pixels assisted in the films visual effects. 

Digital Dimension worked on 200 visual effects shots in the film,    including the sequence that shows characters John McClane and Matt Farrell crouching between two cars as another car lands on top of the other cars. To achieve this effect, a crane yanked the car and threw it in the air onto the two cars that were also being pulled by cables. The shot was completed when the two characters were integrated into the footage of the car stunt after the lighting was adjusted and CGI glass and debris were added.  In the same sequence, John McClane destroys a helicopter that several of Gabriels henchman are riding in by ramming it with a car. This was accomplished by first filming one take where one of Gabriels henchman, Rand, jumps from the helicopter, and in the next take the car is propelled into the stationary helicopter as it is hoisted by wires. The final view of the shot overlays the two takes, with added CGI for the debris and moving rotor blades.  The company also assisted in adding cars for traffic collisions and masses of people for evacuations from several government buildings. 
 Maya to blur the background and create a heat ripple effect. 

===Filming and injuries===
 
Filming for Live Free or Die Hard started in downtown Baltimore, Maryland on September 23, 2006.    Eight different sets were built on a large soundstage for filming many scenes throughout the film.    When recording the sound for the semi trailer used in one of the films final scenes, 18 microphones were used to record the engine, tires, and damage to the vehicle.  Post-production for the film only took 16 weeks, when it was more common for similar films to use 26 weeks. 
  (Ford Crown Victoria Police Interceptor|CVPI) car used during filming.]]
In order to prevent possible injuries and be in peak condition for the film, Willis worked out almost daily for several months prior to filming.    Willis was injured January 24, 2007 during a fight scene, when he was kicked above his right eye by a stunt double for actress Maggie Q who was wearing stiletto heels. Willis described the event as "no big deal" but when Len Wiseman inspected his injury, he noticed that the situation was much more serious than previously thought—in the DVD commentary, Wiseman indicates in inspecting the wound that he could see bone. Willis was hospitalized and received seven stitches which ran through his right eyebrow and down into the corner of his eye. Due to the films non-linear production schedule, these stitches can accidentally be seen in the scene where McClane first delivers Farrell to Bowman.   

Throughout filming, between 200 and 250 stunt people were used.  Bruce Willis stunt double, Larry Rippenkroeger, was knocked unconscious when he fell   from a fire escape to the pavement. Rippenkroeger suffered broken bones in his face, several broken ribs, a punctured lung, and fractures in both wrists. Due to his injuries, production was temporarily shut down. Willis personally paid the hotel bills for Rippenkroegers parents and visited him a number of times at the hospital.   

During filming, Bruce Willis reportedly had plenty of creative control and considered himself the "gatekeeper of the Die Hard mythos." The Warlock scene was rewritten several times. Kevin Smith recalls the entire process on his third Q&A DVD, Sold Out: A Threevening with Kevin Smith. An entire day of shooting the Warlock scene was lost due to rewrites requested by Willis.

==Rating==
In the United States, the first three films in the Die Hard (film series)|Die Hard series were rated R by the Motion Picture Association of America. Live Free or Die Hard, however, was edited to obtain a PG-13 rating. In some cases, alternate profanity-free dialogue was shot and used or swearing was cut out in post-production to reduce profanity. Director Len Wiseman commented on the rating, saying "It was about three months into it  , and I hadnt even heard that it was PG-13... But in the end, it was just trying to make the best Die Hard movie, not really thinking so much about what the rating would be."  Bruce Willis was upset with the studios decision, stating, "I really wanted this one to live up to the promise of the first one, which I always thought was the only really good one. Thats a studio decision that is becoming more and more common, because they’re trying to reach a broader audience. It seems almost a courageous move to give a picture an R rating these days. But we still made a pretty hardcore, smashmouth film."    Willis said he thought that viewers unaware that it was not an R-rated film would not suspect so due to the level and intensity of the action as well as the usage of some profanity, although he admitted these elements were less intense than in the previous films.       He also said that this film was the best of the four: "Its unbelievable. I just saw it last week. I personally think, its better than the first one."   

In the United Kingdom, the British Board of Film Classification awarded the film a 15 rating (including the unrated version, released later), the same as Die Hard with a Vengeance and Die Hard 2, albeit both were cut for both theatrical and video release, (the first film in the series originally received an 18 certificate). All films have been re-rated 15 uncut. Die Hard 4.0 was released with no cuts made and the cinema version (i.e. US PG-13 version) consumer advice read that it "contains frequent action violence and one use of strong language".  The unrated version was released on DVD as the "Ultimate Action Edition" with the consumer advice "contains strong language and violence". 

In Australia, Die Hard 4.0 was released with the PG-13 cut with an M rating, the same as the others in the series (The Australian Classification Board is less strict with regards to language and to a lesser extent, violence). The unrated version was later released on DVD and Blu-ray also with an M rating. The film notably was never released in home media with its theatrical cut, and has only been released in Australia as the extended edition.

==Release==

===Box office===
Live Free or Die Hard debuted at #2 at the box office and made $9.1 million in its first day of release in 3,172 theaters, the best opening day take of any film in the Die Hard series (not taking inflation into account).     On its opening weekend Live Free or Die Hard made $33.3 million ($48.3 million counting Wednesday and Thursday).    The film made $134.5 million domestically, and $249.0 million overseas for a total of $383.5 million, making it the twelfth highest-grossing film of 2007.  To date, it is the most successful film in the series.   

===Critical reception=== Rocky Balboa, Ebert & Roeper, film critic Richard Roeper and guest critic Katherine Tulich gave the film "two thumbs up", with Richard Roeper stating that the film is "not the best or most exciting Die Hard, but it is a lot of fun" and that it is his favorite among the sequels to the original Die Hard. Roeper also remarked, "Willis is in top form in his career-defining role."    Michael Medved gave the film three and a half out of four stars, opining, "a smart script and spectacular special effects make this the best Die Hard of em all."   

Among the more unfavorable reviews, Lawrence Toppman of   with better one-liners".   

==Soundtrack==
{{Infobox album
| Name        = Live Free or Die Hard Score
| Type        = Soundtrack
| Artist      = Marco Beltrami
| Length      = 63:06
| Released    = July 2, 2007
| Label       = Varèse Sarabande
}} Fortunate Son" by Creedence Clearwater Revival and "Im So Sick" by Flyleaf (band)|Flyleaf. Eric Lichtenfeld, reviewing from Soundtrack.net, said of the scores action cues "the entire orchestra seems percussive, flow well together." {{cite news|last=Lichtenfeld|first=Eric|title=Live Free or Die Hard|publisher=SoundtrackNet|url=http://www.soundtrack.net/albums/database/?id=4514|archiveurl=
http://www.webcitation.org/603ok5Iab|archivedate=July 9, 2011}} 

# "Out of Bullets" (1:08)
# "Shootout" (3:41)
# "Leaving the Apartment" (2:08)
# "Dead Hackers" (1:31)
# "Traffic Jam" (4:13)
# "Its a Fire Sale" (2:57)
# "The Break-In" (2:28)
# "Farrell to D.C." (4:36)
# "Copter Chase" (4:41)
# "Blackout" (2:03)
# "Illegal Broadcast" (3:48)
# "Hurry Up!" (1:23)
# "The Power Plant" (2:01)
# "Landing" (2:28)
# "Cold Cuts" (2:00)
# "Break a Neck" (2:47)
# "Farrell Is In" (4:22)
# "The F-35" (4:13)
# "Aftermath" (3:12)
# "Live Free or Die Hard" (2:56)

==Home media release==
The Blu-ray and DVD were released on October 29, 2007, in the United Kingdom,    on October 31 in Hungary,    November 20 in the United States,    and December 12 in Australia. The DVD topped rental and sales charts in its opening week of release in the U.S. and Canada.         There is an unrated version, which retains much of the original R-rated dialogue, and a theatrical version of the film. However, the unrated version has a branching error resulted in one of the unrated changes to be omitted. The film briefly switches to the PG-13 version in the airbag scene; McClanes strong language is missing from this sequence (although international DVD releases of the unrated version are unaffected).  The Blu-ray release features the PG-13 theatrical cut which runs at 128 minutes, while the Collectors Edition DVD includes both the unrated and theatrical versions. Time (magazine)|Time magazines Richard Corliss named it one of the Top 10 DVDs of 2007, ranking it at #10.   
The German Blu-ray release of the "Die Hard Legacy Collection" features the unrated version for the first time in HD, and the disc is region-free.  . Amazon.de. Retrieved on 2014-03-02. 

The DVD for the film was the first to include a Digital Copy of the film which could be played on a PC or Mac computer and could also be imported into several models of portable video players.    Mike Dunn, a president for 20th Century Fox, stated "The industry has sold nearly 12 billion DVDs to date, and the release of Live Free or Die Hard is the first one that allows consumers to move their content to other devices." 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*   questions/answers at the SoundtrackINFO project
*  , the Wired article on which the films script was based

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 