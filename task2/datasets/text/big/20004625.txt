The True Story of Jesse James
{{Infobox film
| name           = The True Story of Jesse James
| image          = True.Story.of.Jesse.James.poster.jpg
| caption        = Theatrical release poster
| director       = Nicholas Ray
| producer       = 
| writer         = Screenplay:  
| narrator       = 
| starring       = Robert Wagner Jeffrey Hunter Hope Lange Agnes Moorehead
| music          = Leigh Harline
| cinematography = Joseph MacDonald Robert L. Simpson
| distributor    = 20th Century Fox
| released       = February 22, 1957
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $1,585,000 
| gross          = $1,500,000 (US rentals) 
}} Western drama Henry Kings Jesse James, which was only loosely based on James life. {{cite news 
  | title = The True Story of Jesse James (1957)
  | publisher = The New York Times
  | url = http://movies.nytimes.com/movie/114592/The-True-Story-of-Jesse-James/overview
  | accessdate = 2008-10-30 }}  It was directed by Nicholas Ray, with Robert Wagner portraying Jesse James and Jeffrey Hunter starring as Frank James. Filming took place during 1955. {{cite news 
  | title = The True Story of Jesse James (1955)
  | publisher = Cinemagora.co.uk
  | url = http://www.cinemagora.co.uk/movie-3650-the-true-story-of-jesse-james.html
  | accessdate = 2008-10-30 }}   Titled The James Brothers for release in the United Kingdom, the film focused on the relationship between the two James brothers during the last 18 years of Jesse James life.  

==Plot==
Jesse (Robert Wagner) and Frank James (Jeffrey Hunter) ride with their gang into Northfield, Minnesota for a raid. While robbing a bank, gun fighting breaks out and two of the gang are killed. The James brothers and another gang member head out of town and hide out while investigators from the Remington Detective Agency search for James to receive a $30,000 reward. While the three are hiding, the film tells the story of how the James brothers came to be criminals in flashback.

==Cast==
*Robert Wagner as Jesse James
*Jeffrey Hunter as Frank James Zerelda "Zee" James, wife of Jesse Zerelda Cole James, mother of the James brothers
*Alan Hale, Jr. as Cole Younger
*John Carradine as Rev. Jethro Bailey
*Biff Elliot as Jim Younger 
*Frank Gorshin as Charley Ford  Robby Ford Adam Marshall as Dick Liddell 
*Anthony Ray as Bob Younger 
*Louis Zito as Clell Miller
*Clegg Hoyt as Tucker (uncredited)

==Production==
Shortly after his success with 1955s Rebel Without a Cause, Ray was hired to direct this movie based on Jesse James later life. He had only one movie left under his contract with 20th Century Fox, before he would depart for Europe and film Bitter Victory. The studio suggested a remake of Kings 1939 biography of Jesse James. {{cite web 
  | title = THE TRUE STORY OF JESSE JAMES
  | publisher = Electric Sheep Magazine
  | url = http://www.electricsheepmagazine.co.uk/reviews/2007/10/04/the-true-story-of-jesse-james/
  | accessdate = 2008-10-30 }} 

It is speculated that had James Dean not died in a car crash before production began, he would have starred in this film as Jesse James. {{cite news 
  | title = Wanted: Jesse James
  | publisher = New York Sun
  | url = http://www.nysun.com/arts/wanted-jesse-james/62907/
  | accessdate = 2008-10-30 }}  {{cite news 
  | title = The Assassination of Jesse James by the Coward Robert Ford
  | publisher = The Observer
  | url = http://www.guardian.co.uk/film/2007/dec/02/bradpitt.drama
  | accessdate = 2008-10-30
| location=London
| first=Philip
| last=French Love Me Tender. {{cite book 
  | title = Live Fast, Die Young
  | publisher = Simon and Schuster
  | url = http://books.google.com/books?id=ia7z479WXpIC&pg=PA269&dq=james+dean+true+story+of+jesse+james&client=opera
  | accessdate = 2008-10-30 }}  Rays son Tony also was cast in the film as Bob Younger, the first time he appeared in one of his fathers films. 
 Peyton Place. John Carradine had appeared in the first Jesse James film as Bob Ford and appears in the 1957 version as Rev. Jethro Bailey. 
 the earlier James film which inspired this one was re-used and reconfigured for CinemaScope. 

==Characterizations== King of Kings. 

==See also==
* The Assassination of Jesse James by the Coward Robert Ford

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 