Getting Acquainted
{{Infobox film
| name = Getting Acquainted AKA A Fair Exchange
| image = Getting acquainted.jpg
| imagesize= 400px
| caption = Clockwise from top: Phyllis Allen, Mack Swain, Charles Chaplin and Mabel Normand
| director =  Charles Chaplin
| producer = Mack Sennett
| writer = Charles Chaplin
| starring = Charles Chaplin Mabel Normand Phyllis Allen Mack Swain Harry McCoy Edgar Kennedy Cecile Arnold 
| music = Frank D. Williams
| editing =
| distributor = Keystone Studios
| released =  
| runtime = 16 minutes English (Original titles)
| country = United States
| budget =
}}
  1914 United American comedy silent film written and directed by Charles Chaplin, starring Chaplin and Mabel Normand, and produced by Mack Sennett at Keystone Studios .

==Synopsis==
Charlie and his wife are walking in the park when they encounter Ambrose and his wife where they become attracted to each others wife and start chasing them around the park.  The policeman is looking for a masher.

==Cast==
* Charles Chaplin - Mr. Sniffels
* Mabel Normand - Ambroses wife
* Phyllis Allen - Mrs. Sniffels
* Mack Swain - Ambrose
* Harry McCoy  - Flirt in park
* Edgar Kennedy - Policeman
* Cecile Arnold - Mary

==See also==
* List of American films of 1914

==External links==
* 
* 
*   on YouTube
 

 
 
 
 
 
 
 
 
 
 


 