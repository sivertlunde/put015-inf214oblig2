Ca-bau-kan
{{Infobox Film
| name           = Ca-bau-kan
| image          = Ca-bau-kan.jpg
| caption        = The Indonesian movie poster.
| director       = Nia Dinata
| producer       = Nia Dinata
| writer         = Nia Dinata
| starring       = Niniek L. Karim Ferry Salim  Lola Amaria
| music          = Andi Rianto
| cinematography = German G. Mintapradja 
| editing        = Sastha Sunu
| distributor    = Kalyana Shira Film
| released       = February 7, 2002
| runtime        = 120 minutes
| country        = Indonesia
| language       = Indonesian
}} 2002 Cinema Indonesian romantic drama film, directed by Nia Dinata, and starring Niniek L. Karim, Ferry Salim and Lola Amaria. It was distributed by Kalyana Shira Film and released on February 7, 2002 in Jakarta. The film was screened at the 2003 Palm Springs International Film Festival.

==Plot==
Ca Bau Kan relates the saga of Giok Lan, an Indonesian woman living in the   culture is largely based on the Benteng Chinese culture. 

 

==Cast==
*Ferry Salim ...  Tan Peng Liang 
*Lola Amaria ...  Tinung 
*Niniek L. Karim ...  Giok Lan 
*Irgi A. Fahrenzi ...  Tan soen Bie 
*Alex Komang ...  Rahardjo Soetardjo 
*Robby Tumewu ...  Thio Boen Hiap 
*Ananda George ...  Max Awuy 
*Tutie Kirana ...  Jeng Tut 
*Henky Solaiman ...  Liem Kiem Jang 
 
*Lulu Dewayanti ...  Saodah 
*Chossy Latu ...  Nyoo Tek Hong 
*Alvin Adam ...  Timothy Wu 
*Maria Oentoe ...  Tan Peng Liangs mother 
*Billy Glenn ...  Tja Wan Sen 
*Joseph Ginting ...  Oey Eng Goan 
*Moelyono ...  Tan Peng Liang Tamim 
*Yongki Komaladi ...  Kwee Tjwie Sien

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 