Recess Christmas: Miracle on Third Street
{{Infobox Film
| name           = Recess Christmas: Miracle on Third Street
| image          = Recess Christmas.jpg
| image_size     = 
| caption        = VHS cover
| director       = {{Plainlist|
* Chuck Sheetz
* Susie Dietter }}
| producer       = 
| writer         = 
| narrator       = 
| starring       = {{Plainlist| Andrew Lawrence
* Rickey DShon Collins Jason Davis
* Ashley Johnson
* Courtland Mead
* Pamela Adlon
* James Earl Jones }}
| music          = 
| cinematography = 
| editing        = 
| studio         = Paul & Joe Productions
| distributor    = Walt Disney Television Animation
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
}}
Recess Christmas: Miracle on Third Street is a  , and  .

== Plot ==
Prickly, Grotke and Finster head home during a snowstorm and remember the times with T.J. and the gang. When they get stuck in a snowbank, Prickly blames it on T.J. and the gang, although Grotke says there is a logical explanation, considering that T.J. and the kids are not around the snowbank. As Grotke and Finster tell their stories about their experiences with T.J. and the gang, they are rescued, from where they are trapped, by T.J. and the gang, and Prickly admits T.J. and the others arent so bad, to which Finster agrees, saying it really makes her feel more warm and fuzzy than before. The film ends with T.J. and the gang singing their version of Jingle Bells, describing their classmates and teachers.

==Cast== Andrew Lawrence as T.J. Detwiler
*Rickey DShon Collins as Vince LaSalle
*Ashley Johnson as Gretchen Grundler
*Courtland Mead as Gus Griswald
*Dabney Coleman as Principal Prickly
*Pamela Adlon as Ashley Spinelli
*Erik Von Detten as Erwin Lawson Jason Davis as Mikey Blumberg
*Allyce Beasley as Miss Alordayne Grotkey
*Robert Goulet as Mikey Blumberg (singing voice)
*April Winchell as Muriel P. Finster
*James Earl Jones as Santa Claus/ Mister 
*Ross Malinger as other voice of T.J Detwiler
*Dick Clark as himself appears in Christmas episode
*Michael McKean as Mr Bream The Producer Of The Christmas Pageant
* E.G. Daily as Trisha/Kid 2/ Mall Elf/ Captain Sticky 
* Jess Harnell as Ricky Mccloud
* Ronnie Schell as Mayor Fitzhugh
* Ryan ODonohue as Randall Weems/Kid 1
* Paul Dooley as Hank/Mall Santa
* Michael Schulman as Hustler Kid
* Anndi McAfee as Ashley Armbuster
* Gregg Berger as Robotic Santa/TV Editor/Astronaut White Christmas"

== Trivia ==
*The title of this film is a reference to Miracle on 34th street. Recess

==References==
 

==External links==
*  
*  
* 

 
 

 
 
 
 
 
 
 
 


 

 