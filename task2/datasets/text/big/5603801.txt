A/k/a Tommy Chong
   italic title=no
| name  = a/k/a Tommy Chong
| image  = aka_Tommy_Chong.jpg
| caption = Theatrical release poster Josh Gilbert
| writer    = Josh Gilbert
| starring   = Tommy Chong Eric Schlosser Lou Adler Bill Maher Jay Leno Cheech Marin
| distributor = Blue Chief Entertainment
| producer    = Josh Gilbert
| released   = June 14, 2006
| runtime   = 78 min.
| language = English
}} Josh Gilbert, is a documentary film that chronicles the Drug Enforcement Administration raid on comedian Tommy Chongs house and his subsequent jail sentence for trafficking in illegal drug paraphernalia. He was sentenced to nine months in federal prison.  DEA agents raided Chongs Pacific Palisades, California home on the morning of February 24, 2003. The raid was part of Operation Pipe Dreams and "Operation Headhunter," which resulted in raids on 100 homes and businesses nationwide that day and indictments of 55 individuals.
 Showtime cable network on November 9, 2008. {{cite news
 |title=The Watcher recommends |newspaper=Chicago Tribune|date=November 9, 2008 |author=Anonymous |page=1 |url=http://pqasb.pqarchiver.com/chicagotribune/access/1591379751.html?dids=1591379751:1591379751&FMT=ABS&FMTS=ABS:FT&type=current&date=Nov+09%2C+2008&author=Anonymous&pub=Chicago+Tribune&desc=The+Watcher+recommends&pqatl=google }}   

==Synopsis== The Department of Justice. Filmmaker Josh Gilbert follows the tale of Chong as he becomes a target in a government sting, code named "Operation Pipe Dreams". Tommy Chong was the only defendant without a prior conviction to receive a jail sentence.  The federal government stated that the reason that it sought this harsh punishment was because of the comedy movie Up In Smoke trivialized the governments anti-drug efforts.

The documentary discloses that Tommy Chongs son, who actually ran the company which sold the paraphernalia was never charged or indicted by the Federal government.  In May 2008 federal agents raided the owner of the distribution rights to this DVD.

The documentary is critical of the prosecution of Chong by the U.S. federal government led by the U.S. attorney for western Pennsylvania, Mary Beth Buchanan.

==Production credits==
*Director – Josh Gilbert
*Screenplay – Josh Gilbert
*Producer – Josh Gilbert
*Executive Producers – A. D. Sinha, Cheryl Chapman, Matt Stephens
*Co-Producers – Will Becton, David Hausen, Brandie Knight
*Co-Executive Producers – Jeffrey Gordon, John Mato, Jay Tobin
*Associate Producers – Bonita DeWolf, Precious Chong
*Cinematography – John Ennis, Josh Gilbert, Jonathan Schell, Tim Huber, Sean McCall, Melik
*Editing – Will Becton, Howard Leder, Tom Walls Jr.
*Sound – Sabrina Buchanek, Grant Johnson, Bruce Maddocks
*Music – Oz Noy
*Sound Supervisor – Michael Mancini 

==Reception==

===Reviews===
The film received positive reviews.     In the New York Daily News,  Elizabeth Weltzmen gave the film  2 1/2 stars (of 4), writing, "... even those unimpressed with   genially lowbrow work will be intrigued by the political tenor of this portrait."  She continues, "Gilbert blatantly takes Chongs side, so your level of empathy will rise or fall depending on how strongly you connect with his subject."   
Wesley Morris of the Boston Globe wrote, "This isnt a great piece of nonfiction filmmaking, but it has its moments", stating that Chongs presence in the film lent "a serene counterpoint to the farce Gilbert makes of the Justice Department...", but, "the movie does succeed in showing us the graying cult star as a gratuitous drug-war casualty".  At review aggregator Rotten Tomatoes, the film has a score of 71% among 21 selected critic reviews. 

===Recognition===
*Official Selection – Toronto International Film Festival 
*Official Selection – IDFA: International Documentary Festival Amsterdam
*Official Selection – Palm Springs International Film Festival 
*Official Selection – Miami International Film Festival 
*Official Selection – SXSW Film Festival
*Official Selection – Full Frame Documentary Festival
*Winner, Best Documentary – HBO US Comedy Arts Festival 
*Audience Award, Best Documentary – San Francisco Independent Film Festival
*Audience Award Runner Up, Best International Film – Vancouver International Film Festival  

==Seizure of DVDs==
On May 7, 2008, federal agents raided Spectrum Labs as part of an investigation related to "drug masking products" used to fool drug tests. Chong alleges that 8,000 to 10,000 copies of his yet-to-be released documentary, which he claims were seized by the authorities, were the actual focal point of the raid. "Its a way to punish the distributor financially," Mr. Chong said. "Theres no way to get the DVDs back until the investigation is over." However, attorneys for Spectrum Labs have said no copies of the documentary were seized. U.S. Attorney Mary Beth Buchanan, who led the investigation, refused to comment on Mr. Chongs allegation. 

==References==
 

==External links==
*  
*  

 
 
 