Oothikachiya Ponnu
{{Infobox film
| name           = Oothikachiya Ponnu
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = P. K. Joseph
| producer       = Shanmugha Priya Films
| writer         = V. K. Pavithran
| narrator       =  Shankar  Mohanlal  Mammootty
| music          = M.K. Arjunan
| cinematography = B.R. Ramakrishna
| editing        = K. Sankunny   
| studio         = 
| distributor    = 
| released       = December 11, 1981
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Shanker and Poornima Jayaram after Manjil Virinja Pookkal (1980). it also stars Mammootty.   

==Plot==
A Poor young woman must succeed her dream  as saleswoman

==Cast==
*Poornima Jayaram as   Sukumari Shankar as   Vishwanathan
*Mohanlal as   Nandan
*K.P. Ummer as   Mathachan
*Mammootty as   Thoman Kutty Santhakumari as   Sukumaris mother
*Sreenath as   Dr. Samuel
*Nellikode Bhaskaran as   Sukumaris father
*Master Kumar
*Roja Ramani as  Shalini
*Jagathi Sreekumar as  Vasu
*Indrapani
*Suchitra

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Poovachal Khader and .
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amritha Kalayaayi Nee || K. J. Yesudas || Poovachal Khader ||
|-
| 2 || Ee Raavil Ninte Kaamukiyaavaam || S Janaki || Poovachal Khader ||
|-
| 3 || Etho Oru Vazhiyil || K. J. Yesudas || Poovachal Khader ||
|-
| 4 || Theme Music || || ||
|}

==References==
 

==External links==
*  

 
 
 
 

 
 