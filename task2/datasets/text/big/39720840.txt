If I Stay (film)
 
{{Infobox film
| name = If I Stay
| image = If I Stay poster.jpg
| alt = 
| caption = Theatrical release poster
| director = R. J. Cutler
| producer = Denise Di Novi Alison Greenspan
| screenplay = Shauna Cross
| based on =  
| starring = Chloë Grace Moretz Mireille Enos Jamie Blackley Joshua Leonard Stacy Keach Aisha Hinds
| music = Heitor Pereira
| cinematography = John de Borman
| editing = Keith Henderson
| studio = DiNovi Pictures Metro-Goldwyn-Mayer New Line Cinema 
| distributor= Warner Bros. Pictures
| released =  
| runtime = 106 minutes  
| country = United States
| language = English
| budget = $11 million   
| gross = $78.3 million 
}} novel of the same name by Gayle Forman. The film stars Chloë Grace Moretz, Mireille Enos, Jamie Blackley, Joshua Leonard, Stacy Keach, and Aisha Hinds. It was released on August 22, 2014 and received mixed reviews from critics and grossed $78.3 million worldwide.

==Plot==
Mia Hall (Chloë Grace Moretz) and her family are getting ready to go on with their normal day activities when it is announced on the radio that school has been cancelled. Mias dad Denny (Joshua Leonard) is a teacher and as a result of the snow day does not have to go to work. Mias mom Kat (Mireille Enos), a travel agent, decides to call in sick and along with her family visit Mias grandparents who live on a farm. It is also revealed that Mia is dating an older, popular up-and-coming rock-star named Adam Wilde (Jamie Blackley).

The story flashes back to Mias early life with a rocker dad and an inexperienced mom; one day they take Mia to a music class where Mia decides that she wants to start playing the cello. After her parents see that Mia is passionate about the cello they hire a teacher to help her play. Kat is shown pregnant with Teddy (Jakob Davies) whom Mia thinks is the reason for her fathers departure from his band.

Years later, an adolescent Mia is still passionate about the cello and is playing at school in the band room where Adam peeks in on her playing, seeing her for the first time, leading him to ask Kim (Liana Liberato), Mias best friend, about her. Adam then asks Mia out on a date to a cello recital, where they show mutual affection towards one another.

The story flashes back to current time with the family driving to the grandparents farm when their car collides with an oncoming truck. Mia appears to have an out-of-body experience and sees her body lying on the road while paramedics are trying to help her and her family. Mia tries to ask a paramedic what is happening, but realizes that no one can see or hear her. Mia, now in the hospital, panics as she does not know the current status of any of her family members when they take her in for surgery. A sympathetic nurse (Aisha Hinds) tells Mia that it is up to her whether or not she wants to stay.

The story flashes back once again to Mia attending one of Adams concerts with his band Willamette Stone, where she doesnt seem to fit in. Adams band is steadily gaining more recognition and gets signed to a label, which starts to put a strain on their relationship due to the travel schedule. While at dinner, Mias Grandpa suggests that she should apply to Juilliard School|Juilliard, an idea which she initially rebuffs, but later further researches. She eventually lands an audition in San Francisco. It takes a while for Mia to tell Adam about her audition and when she does he becomes upset and leaves for a week-long gig.

In the present day, Mia undergoes surgery. She sees a doctor speaking with her grandparents who are standing outside of a pediatric room where Teddy lays in a coma-like state. Adam comes to the hospital to see Mia, but is not allowed in as he is not an immediate family member. It is then revealed that Mias mom Kat was pronounced dead on arrival, and her dad Denny died on the operating table. She later finds out that Teddy has died from an epidural hematoma. Grandpa talks to Mia, revealing that her father gave up being in a band because he wanted to give her a better life and be a better father. He also, tearfully, gives Mia permission to move on from this life if she wants, which influences Mia to decide that she wants to die so that she can join her family on the other side. 

The story flashes back to Mia at her audition for Juilliard, where she plays the best she has ever played, leading her to think that if accepted she will go. After a reconciliation with Adam, they talk about the huge possibility of her going to Juilliard, which ultimately leads to them breaking up and going their separate ways.

After some time, Mia seems stable enough to receive more visitors. Mia is shown symbolically about to let go when she hears the music performed at the cello recital that she attended with Adam. It is revealed that Adam has come to see Mia and that he is playing his iPod for her. He brings with him a letter Mia has received from Juilliard saying that she has been accepted, and lets her know that he will do whatever she wants if she stays. Mia, after flashing back through all the happy moments in her life, opens her eyes and sees Adam as he hovers over her and says,"Mia?"

==Cast==
* Chloë Grace Moretz as Mia Hall
* Jamie Blackley as Adam Wilde
* Mireille Enos as Kat Hall
* Joshua Leonard as Denny Hall
* Stacy Keach as Grandpa
* Lauren Lee Smith as Willow
* Liana Liberato as Kim Schein
* Aisha Hinds as Nurse Ramirez
* Aliyah OBrien as EMT
* Jakob Davies as Teddy Hall

==Production==
In December 2010, it was announced that a film based on the novel If I Stay was in the works at Summit Entertainment, and that Dakota Fanning, Chloë Grace Moretz, and Emily Browning were in talks to play Mia.  Catherine Hardwicke was attached to direct, but was replaced by Brazilian filmmaker Heitor Dhalia,  who also left the film later. On January 24, 2013, Moretz was officially cast to play the lead and R. J. Cutler was announced as the new director of the film.  The shooting of the film began on October 30, 2013 in Vancouver. 

In January 2014, Metro-Goldwyn-Mayer and Warner Bros. were announced to be distributing the film, and the release was set for August 22, 2014. 

===Music===
 
The music was composed by Heitor Pereira. The soundtrack was released on August 19, 2014, by WaterTower Music.  It peaked at number 54 on the Billboard 200|Billboard 200 in the United States,  and number 77 in Australia. 

All the songs for Adam Wildes band Willamette Stone, including the cover of The Smashing Pumpkins song "Today (The Smashing Pumpkins song)|Today", were produced by indie rock producer Adam Lasus.

{{Track listing
| headline     = If I Stay: Original Motion Picture Soundtrack 
| collapsed    = no
| extra_column = Artist
| total_length = 72:57

| title1       = Who Needs You
| extra1       = The Orwells
| length1      = 3:19
| title2       = Until We Get There Lucius
| length2      = 3:28
| title3       = I Want What You Have
| extra3       = Willamette Stone
| length3      = 3:30
| title4       = All of Me
| extra4       = Tanlines
| length4      = 3:50
| title5       = Promise
| extra5       = Ben Howard
| length5      = 6:21
| title6       = Never Coming Down
| extra6       = Willamette Stone
| length6      = 3:23 Halo
| extra7       = Ane Brun and Linnea Olsson
| length7      = 3:52
| title8       = I Will Be There
| extra8       = Odessa
| length8      = 4:35
| title9       = Mind
| extra9       = Willamette Stone
| length9      = 3:13
| title10      = Morning
| extra10      = Beck
| length10     = 5:22
| title11      = I Never Wanted to Go
| extra11      = Willamette Stone
| length11     = 3:33
| title12      = Karen Revisited
| extra12      = Sonic Youth
| length12     = 11:12 Today
| extra13      = Willamette Stone
| length13     = 2:42
| title14      = Heart Like Yours
| extra14      = Willamette Stone
| length14     = 3:20
| title15      = Heal
| note15       = If I Stay version
| extra15      = Tom Odell
| length15     = 3:13
| title16      =  
| note16       = deluxe edition bonus track
| extra16      = Composed by Johann Sebastian Bach
| length16     = 2:58 Cello Concerto in A Minor, op. 33
| note17       = deluxe edition bonus track
| extra17      = Composed by Camille Saint-Saëns
| length17     = 1:20 Sonata in B Minor for Solo Cello, op. 8
| note18       = deluxe edition bonus track
| extra18      = Alisa Weilerstein (composed by Zoltán Kodály)
| length18     = 3:46
}}

==Reception==

===Box office===
As of November 20, 2014, If I Stay has grossed a worldwide total of $78,396,071 against a budget of $11 million. 
 Guardians of Teenage Mutant Ninja Turtles, which were in their fourth and third weeks respectively. On its opening day the film earned $6,827,007 earning it the top spot for the day. 

===Critical response=== YA framework, If I Stay is ultimately more manipulative than moving."  On Metacritic, the film holds a score of 46 out of 100, based on 36 critics, indicating "mixed or average reviews". 

In his review for The New York Times, A.O. Scott praised Moretzs acting. A.O. Scott,  , The New York Times, August 21, 2014  He added that the "music is both the best and the corniest part of If I Stay, which makes excellent use of the classical cello repertoire." 

Writing for Variety (magazine)|Variety, Justin Chang criticized Moretzs acting, explaining that "she comes off as a bit too self-assured to play the nerdy misfit." Justin Chang,  , Variety, August 13, 2014 

Anthony Lane of The New Yorker wrote a critical review of the movie, saying, "The saddest thing about If I Stay is that it affords Moretz so little opportunity to be non-sad."
In the Los Angeles Times, Olivier Gettell summed up that critics saw the film was "clunky and uninspired." Oliver Gettell,  , The Los Angeles Times, August 21, 2014 

==Home media==
 
If I Stay was released on DVD and Blu-rays on November 18, 2014. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 