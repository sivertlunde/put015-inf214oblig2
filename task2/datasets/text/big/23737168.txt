Fast Company (1953 film)
{{Infobox film
| name           = Fast Company
| image          =
| image_size     =
| caption        =
| director       = John Sturges
| producer       = Henry Berman
| writer         =   Story: Eustace Cockrell
| narrator       =
| starring       = Howard Keel Polly Bergen
| music          =
| cinematography = Harold Lipstein
| editing        = Joseph Dervin
| distributor    = Metro-Goldwyn-Mayer (U.S.)
| released       =  
| runtime        = 67 mins.
| country        = United States
| language       = English
| budget         = $584,000  .  Glenn Lovell, Escape Artist: The Life and Films of John Sturges, University of Wisconsin Press, 2008 p77 
| gross          = $523,000 
| website        =
}} American filmmaker John Sturges.

==Plot==
Carol Maldon leaves New York to run her recently deceased fathers stable. Rick Grayton is the trainer and jockey of her horse Gay Fleet.  It is an exceptional horse, but no one yet knows Gay Fleet because it is still young.  Rick has been intentionally losing races to make the horse seem inferior so that he can but it from Carol cheaply.  However, he is discovered by Mercedes, a rival stable owner, who tells Ricks plan to Carol.

==Cast==
* Polly Bergen as Carol Maldon
* Howard Keel as Rick Grayton
* Nina Foch as Mercedes Bellway
* Carol Nugent as Jigger Parkson
* Marjorie Main as Ma Parkson
* Horace McMahon as Two Pair Buford
* Iron Eyes Cody as Ben Iron Mountain
==Reception==
According to MGM records the film earned $392,000 in the US and Canada and $131,000 elsewhere, resulting in a loss of $275,000. 
==References==
 

==External links==
* 
*  
* 

 

 
 
 
 


 