The Taebaek Mountains
{{Infobox film name           = The Taebaek Mountains image          = The Taebaek Mountains.jpg caption        = Poster to The Taebaek Mountains (1994) director       = Im Kwon-taek producer       = Lee Tae-won writer         = Cho Jeong-lae Song Neung-han  starring       = Ahn Sung-ki Bang Eun-jin music          = Kim Soo-chul cinematography = Jung Il-sung editing        = Park Sun-duk distributor    = Taehung Pictures released       =   runtime        = 168 minutes country        = South Korea language       = Korean budget         = 
| film name = {{Film name hangul         =   hanja          =   rr             = Taebaek Sanmaek mr             = T‘aebaeksanmaek}}
}}
The Taebaek Mountains ( ) is a 1994 South Korean film directed by Im Kwon-taek.

==Plot==
The film originates from the great river story Taebaegsanmaek consisting of 10 volumes and is written by Cho Jeongrae. The story tries to describe and reveal a few generations-lasting conflicts between the haves (proprietors) and have-nots (peasants), which develop into right wings and left wings respectively. While revealing why and how the conflicts come about, the story depicts every corner of real life—romantic, shamanic, and Confucian aspects of the contemporaries. It provides a further insight into the politically controversial ideological issue on which the viewpoint is virtually hardened among over 40s in South Korea. This ideological issue survives even in the digital age to have a substantial effect on presidential elections. The author dares to show what the ideological conflict derives from and tries to describe it in detail and with artistic skill of commanding Korean colloquial language supplying its readers the true taste of Korean dialect expressions especially in its southern part Jeolla province.

==Reception==
Korean film scholar, Kim Kyung-hyun describes the reception of The Taebaek Mountains by South Korean audiences and critics as "lukewarm." 

==Awards==

===Wins===
* Blue Dragon Film Awards Best Film (1994) 

===Nominations===
* Golden Bear, 45th Berlin International Film Festival (Im Kwon-taek) (1995)   

===Presented===
* Telluride Film Festival (1999)

==Notes==
 

==Sources==
*  
* 
*  
*  
* 
* 

  
 
 
 

 
  
 
 

 

 
 
 
 
 
 


 