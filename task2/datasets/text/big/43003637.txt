Serial (Bad) Weddings
{{Infobox film
| name           = Serial (Bad) Weddings
| image          = Quest-ce quon a fait au bon dieu? poster.jpg
| border         = yes
| caption        = French theatrical release poster
| director       = Philippe de Chauveron
| producer       = Romain Rojtman 
| writer         = Philippe de Chauveron Guy Laurent 
| screenplay     = Philippe de Chauveron Guy Laurent
| starring       = Christian Clavier Chantal Lauby Ary Abittan Frédéric Chau Frédérique Bel Élodie Fontan
| music          = Marc Chouarain 
| cinematography = Vincent Mathias 
| editing        = Sandro Lavezzi 
| studio         = Les films du 24 UGC Distribution
| released       =  
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         = $13,000,000
| gross          = $174.1 million
}}

Serial (Bad) Weddings ( )  is a French comedy film directed by Philippe de Chauveron released in 2014 in film|2014.

== Plot == Sephardi Jew, communitarist remarks to each other.
 black and racist views heats up the situation. When chances of the wedding taking place appear slim, unexpectedly André Kofi and Claude Verneuil finds common ground in their dislikes and develops friendship. Finally, Laures and Charles love life heads to a happy marriage.

== Cast ==
 

* Christian Clavier as Claude Verneuil
* Chantal Lauby as Marie Verneuil
* Medi Sadoun as Rashid Ben Assem 
* Ary Abittan as David Benichou
* Frédéric Chau as Chao Ling
* Noom Diawara as Charles Kofi
* Frédérique Bel as Isabelle Ben Assem Verneuil 
* Julia Piaton as Odile Benichou Verneuil 
* Émilie Caen as Ségolène Ling Verneuil  
* Élodie Fontan as Laure Verneuil
* Élie Semoun as the Psychologist
* Pascal Nzonzi as André Kofi, Father of Charles
* Salimata Kamate as Madeleine Kofi, Mother of Charles 
* Tatiana Rojo as Viviane Kofi, Sister of Charles
* Loïc Legendre as the Priest of Chinon
* Yvonne Gradelet as a tourist
* David Salles as a gendarme
* Axel Boute as the young person in cell

== Production ==
Although set in Chinon, the film was mainly filmed in Paris and in Normandy. A few shots of the castle in Chinon were taken on 10 December 2013. 

== Reception ==
  and Élodie Fontan at the 2014 Cannes Film Festival.]]

=== Critical response ===
The film received positive reviews with an average grade of 4.2/5 from AlloCiné for over 9,200 votes on May 30, 2014    as well as press critics with an average grade of 3.7/5 for 7 comments. 

=== Box office === The Artist. The film grossed a total of US$174.1 million internationally. 

==Accolades==
{| class="wikitable plainrowheaders sortable"
|- style="background:#ccc; text-align:center;" Award / Film Festival Category
! Recipients and nominees Result
|- 29th Goya Awards Goya Award Best European Film
|Serial (Bad) Weddings
|  
|- 20th Lumières Awards Best Screenplay Philippe de Chauveron and Guy Laurent
|  
|-
|}

== See also ==
*Lists of highest-grossing films in France

== References ==
 

== External links ==
 
* 

 
 
 
 
 
 
 
 
 
 
 
 