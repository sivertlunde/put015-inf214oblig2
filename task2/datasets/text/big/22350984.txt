Pizzicato Pussycat
 
{{Infobox Hollywood cartoon|
| cartoon_name = Pizzicato Pussycat
| series = Merrie Melodies 150px
| caption = Screenshot of Pizzicato Pussycat title card
| director = Friz Freleng|I. Freleng Manuel Perez
| story = Warren Foster
| layout = Hawley Pratt
| background_artist = Richard H. Thomas
| voice_actor = Mel Blanc (as Mr. Jones, Mouse, Cat, Doctors, Concert audience members) Marian Richman (as Mrs. Jones) (uncredited) Norman Nesbitt (as Narrator, Concert audience member, Doctor) (uncredited)
| musician = Milt Franklyn
| film_editor = Treg Brown
| producer = Edward Selzer
| studio = Warner Bros. Cartoons
| distributor = Warner Bros. Pictures, 1955 Warner Home Video, 2006
| release_date = January 1, 1955 (USA) Color
| runtime = 7 min
| movie_language = English
}}

Pizzicato Pussycat is a Merrie Melodies animated short released January 1, 1955. Directed by Friz Freleng and with voices by Mel Blanc, the cartoon is about a talented piano-playing mouse becoming a slave to a house cat.

==Story==
Mr. and Mrs. John and Vy Jones, a typical suburban couple, keep hearing piano music playing, but cant figure out where its coming from ... even days after their daughter Mary Lous toy piano (and sheet music) had gone missing.  One day, their cat catches a mouse, but the mouse pleads with the cat to spare him his life if he can prove to the cat that hes "...a very fine pianist".  The cat agrees, so the mouse asks the cat to retrieve the piano and gives the mouse some sheet music to play.

Once the cat hears the music, he is astounded.  When Mr. and Mrs. Jones hear the music from the other room and approach, the cat hides the mouse and the little piano inside the real piano, and pretends to play it as the Joneses look on. Surprised by what they see, Mrs. Jones immediately calls the United Press. The cat promises to spare the mouse if he can play the piano while the cat continues to pretend, hoping that his success might make him famous.

Suddenly there is a flurry of media activity around the house.  The cat is examined by scientists, but they are unable to find anything abnormal about the cat; for instance, an x-ray of his brain shows it (humorously) to be the size of a peanut.

Soon, there are agents obtaining signed contracts for the cat to appear in public performances.  One night, the cat performs at Carnegie Hall, displacing "Leopold Stabowski" (a play on "Leopold Stokowski", the famous orchestral conductor).  The cat is dubbed "Miracle Cat" and while a disbelieving audience awaits the cats debut, the cat drags the mouse on stage behind the curtain and puts him into the piano and tells him that when he gives the signal, to start playing.  The mouse agrees.

The curtain rises, the Joneses sit proudly in a theater box, the cat walks proudly out on stage and takes his seat at the piano.  The signal is given (a tap on the mouses head by a piano key), and the mouse starts playing "Hungarian Rhapsody" by Franz Liszt.  The mouse pauses playing after the first few bars, and at the same time the cat falls from his stool; the cat again taps the mouse on the head, but this time, the mouses head is turned, and his glasses are broken by the piano key, and he can no longer read the sheet music in front of him.  He plays on, despite not being able to read the music, and it turns out to be a disaster.  The cat is truly embarrassed, and audience members walk out of the Hall while calling the performance "ridiculous", "insulting", and "preposterous," and the Joneses narrowly escape out the stage entrance. The cat is exposed as a fraud which causes an uproar in the music world.

Home again, the Joneses are relaxing, as the cat and mouse become typical enemies again, but this time, the cat ends up trying to hit the mouse with drumsticks on the drum set; the cat becomes intrigued by the beat.  He starts playing the drums and the mouse pulls his piano out of his hole, and the pair begin playing jazz.  Mr. and Mrs. Jones witness this, and Mrs. Jones tries to call the United Press again until Mr. Jones says: "Oh, no.  No you dont!  Were not going through that again!" and hangs up the phone.

Mr. and Mrs. Jones continue to enjoy the music as we see them relaxing to it in their easy chairs. The narrator closes by saying the couple dare never to tell anyone of their unusual house pets.

==Music==
* "Waltz Op. 64 No. 1 in D flat major" (uncredited) aka "Minute Waltz" by Frédéric Chopin Joseph Meyer and Roger Wolfe Kahn
* "Me-ow" (uncredited), by Mel B. Kaufman
* "Home Sweet Home" (uncredited) aka "Theres No Place Like Home", by H.R. Bishop
* "Liebestraum No. 3" (uncredited), by Franz Liszt
* "Hungarian Rhapsody No. 14" (uncredited), by Franz Liszt

==External links==
* 

 

 
 
 
 
 
 
 