Kutsal Damacana 2: İtmen
{{Infobox film
| name           = Kutsal Damacana 2: İtmen
| image          = KutsalDamacana2FilmPoster.jpg
| alt            = 
| caption        = Film Poster
| director       = Korhan Bozkurt
| producer       =  
| writer         = Safak Sezer
| narrator       = 
| starring       =  
| music          = Ovunc Danacioglu
| cinematography = Selahattin Sancakli
| editing        =  
| studio         =  
| distributor    =  
| released       =  
| runtime        = 98 mins
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $4,974,673
}}

Kutsal Damacana 2: İtmen is a 2010 Turkish   (2011).

==Production==
The film was shot on location in Istanbul, Turkey.   

==Cast==
* Şafak Sezer as Fikret
* Mustafa Üstündağ as Kurt Adam
* Tuğba Karaca as Melis
* Burcu Suna as Aydemirin Eşi

==Release==
The film opened across Germany on   and Turkey and Austria on   at number one in the Turkish box office chart with an opening weekend gross of $1,429,779.   

==Reception==

===Box office===
The film has made a total worldwide gross of $4,974,673. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 
 