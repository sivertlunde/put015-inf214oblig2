Splitz
 

{{Infobox film
| name = Splitz
| caption =
| director = Domonic Paris
| producer = Stephen Low Kelly Van Horn
| writer = Domonic Paris Kelly Van Horn Harry Azorin Bianca Littlebaum
| starring = Robin Johnson Shirley Stoler Raymond Serra Chuck McQuary Forbes Riley Barbara Bingham Patti Lee Tom McCleister George Small
| cinematography = Ronnie Taylor
| editing = Rick Shaine
| studio =
| distributor = Film Ventures International
| released =  
| runtime = 86 min. (Home Video)
| country = United States
| awards = English
| budget =
| image= Splitz Poster.jpg
}}
 1984 film starring Robin Johnson, Shirley Stoler and Forbes Riley.  The film is about an all-girl rock band who joins up with an underdog sorority to help them win a series of athletic competitions.

==Synopsis==
Hooter College student Chuck (Chuck McQuary) has decided academics aren’t going to get him anywhere in life, so he’s taken to managing a band his classmates have formed called The Splitz, which consists of lead singer Joan (Patti Lee), guitarist Gina (Robin Johnson) and drummer Susie (Barbara Bingham).  The Splitz struggle to make a name for themselves and resort to playing in dive bars where the patrons are more interested in boozing and brawling than appreciating the music.

The day after a disastrous show, Chuck escorts Gina to her home, where he meets her former-mobster father, who becomes obsessed with the percentage of the band’s income that Chuck is claiming.  Chuck also meets Gina’s cousin Vinnie,  a sweet but oversexed meathead who can’t score a date, so Chuck encourages him to try hypnosis.

Meanwhile the evil Dean Hunta (Shirley Stoler) informs the heads of three sororities that they’ll have to compete in a trio of events to determine who’s going to lose their house to make way for a new sewage treatment plant.  The dean favors Sigma Phi’s Lois Scagliani (Forbes Riley) and Delta Phi’s Fern Hymenstein (Tara King) and informs them that the Phi Betas have to lose.  When asked if she has an axe to grind with the Phi Beta sorority, the Dean replies that it’s “just another act of random, senseless violence perpetrated against the underdogs.”
	
At the first competition, a soccer game, Gina is disgusted to see the way that Phi Beta’s Midge (Amelia David) and her peers are being trampled by their competitors, so she gets into the game herself and the other Splitz quickly follow suit.   Although the Phi Betas lose the game, they gain an all-girl rock band, who immediately become part of their sorority.

Gina enlists Warwick (Tom McCleister), a Neanderthal classmate with a crush on Susie, to coach the Phi Betas.  It still seems like they might lose the next competition, so the Splitz pay a visit to the Dean’s husband, who is a lecherous dentist.  They lure him into women’s clothing and snap a series of photos of him, which they send to the Dean with instructions that she’s to let the Phi Betas make their own rules for the forthcoming tournaments.

At the wrestling match, the Phi Betas make the Sigma Phis don skimpy lingerie, and they win the game when one of the ladies’ bras pops off, leaving her dazed and easily pinned.  The third and final competition is basketball, which the Phi Betas have deemed “strip-basketball.”  Unfortunately, Fern Hymenstein traps Joan in the lockerroom just prior to the game, so it looks like the underdogs will lose, but Warwick bursts in and rescues her halfway through the game.

Although the Phi Betas were victorious in the final two competitions, Dean Hunta retaliates by expelling Chuck and the Splitz, so Chuck turns to Gina’s family for help.  He gets Vinnie to use his newfound skills on the Dean and Gina’s uncles to book the band into the hottest club in town.

The Splitz are shocked to discover the entire school board is present at their big gig, and even more surprised that their opening act is a hypnotized Dean Hunta, who rips open her dress and sings a bawdy tune to the board members.  Then the Splitz take the stage and are instantly a hit, so the manager offers them a contract to headline at the club for the next year.

==Music==
The music was prominently featured in the films marketing, but no soundtrack album was issued and several songs have never been commercially available.  "Were a Miracle" is credited to singer Diane Scanlon, who has stated shes not the actual performer.

*Busy Boy - Arlene Gold The Olympics Heart of Blondie
*Am I Asking Too Much? - Jana Jillio
*Ride of the Valkyries - Richard Wagner
* Mistake Magnifique - Rick Derringer
* Crash Your Party - John Hiatt
* Sues Gotta Be Mine - Del Shannon
* On Hold - The Clonetones
* So Long Baby - Del Shannon
* Give It All You Got - American Patrol
* The Prowler - American Patrol
* Suburban Nights - Diane Scanlon Blondie
* When Love Attacks - Rick Derringer & Bonnie Tyler
* Running in Space - Sarah Larson
* The Deans Song - Shirley Stoler
* Were a Miracle

==Home Media==
The film was first issued on VHS and Beta in 1985 by Vestron Home Video.  The cover features the poster art (a cheerleader doing the splits) printed sideways.  In 1994 it was re-released on VHS by Avid Home Entertainment.  This version features a cover photo of a cheerleader who does not appear in the film.  Unlike the theatrical print which attained an R-rating, both of these releases boast a PG-13 rating (though nudity is included).

In 2003 the film was issued on DVD by Liberty International Entertainment.  Duped from a full-screen video print, this is also the PG-13 version, and the sole extra is a Spanish audio track.

==External links==
*  
*  

 
 
 
 
 