Salaryman Kintarō
{{Infobox animanga/Header
| name            = Salaryman Kintarō
| image           = 
| caption         = 
| ja_kanji        = サラリーマン金太郎
| ja_romaji       = Sarariiman Kintarō
| genre           = Comedy-drama
}}
{{Infobox animanga/Print
| type            = manga
| author          = Hiroshi Motomiya
| publisher       = Shueisha
| demographic     = Seinen manga|Seinen
| magazine        = Weekly Young Jump
| first           = 1994
| last            = 2002
| volumes         = 30
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| director        = Takashi Miike
| producer        = 
| writer          = 
| music           = 
| studio          = Toho
| released        = November 13, 1999
| runtime         = 110 minutes
}}
{{Infobox animanga/Video
| type            = drama
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          =  TBS
| first           = 1999
| last            = 2004
| episodes        = 44 (+ 1 special)
| episode_list    = 
}}
{{Infobox animanga/Video
| type            = tv series
| director        = Tomoharu Katsumata
| producer        = 
| writer          = 
| music           = 
| studio          = 
| network         = BS-i
| first           = February 18, 2001
| last            = March 18, 2001
| episodes        = 20
| episode_list    = 
}}
 

  is a manga series by Hiroshi Motomiya. It has been serialized in Weekly Young Jump since 1994, but has gone through many periods of inactivity.

The manga follows Kintarō Yajima, a former Bōsōzoku leader who, as a promise to his late wife, has become a salaryman. In 2005, Kintarō went from working as a salaryman to working for a foreign bank.

In 2005, Salaryman Kintaro began appearing as an online comic and eventually returned to Young Jump.

While the manga is not available in print in English, all 30 volumes and part of the sequel had been translated into English, and were available through the Comic Friends Facebook app. However, Comic Friends recently announced it will no longer be doing business in America. 

== Characters ==
; 
: The protagonist of the series, a high school dropout and former Bōsōzoku who retired to become a fisherman, but after he saved Morinosuke Yamato he was offered a job at Yamato Construction in the sales department.

; 
: Née  . A high class woman who once had an affair with the late politician  .  She still has strong political and financial connections and later becomes Kintarōs wife.

; 
: The son of Kintarō and his late wife Akemi.

; 
: The daughter of Misuzu from her affair with Kuroda.

;  blind woman who dies giving birth to Ryuta.

; 
:One of the many men that Kintarō saves. Chairman of Yamato Construction. He believes in Kintarō all the way.

; 
:

; 
:

; 
:

== Media ==

=== Live-action film ===
;Staff
* Director: Takashi Miike
* Producers: Morihiro Kodama, Mitsunori Morita, Kazuya Hamana
* Screenplay: Kenji Nakazono, Naoko Harada
* Music: Kouji Endou Hideo Yamamoto
* Editing: Yasushi Shimamura
* Assistant Director: Bunmei Katō

;Cast
*Kintaro Yajima: Katsunori Takahashi
*Misuzu Suenaga: Yoko Saito
*Mimi Suenaga: Kanako Enomoto
*Ryota Yajima: Chinosuke Shimada
*Ryunosuke Yamato: Masahiko Tsugawa
*Genzo Oshima: Shuichiro Moriyama
*Yusaku Kurokawa: Taisaku Akino
*Yozo Igo: Tsutomu Yamazaki
*Kayo Nakamura: Yoko Nogiwa
*Masumi Nakamura: Michiko Hada
*Ichiro Maeda: Toshiaki Megumi
*Hitomi Aihara: Miki Mizuno
*Masakazu Tanaka: Masanobu Katsumura
*Takatsukasa: Naoki Hosaka
*Seishiro Tanioka: Shingo Yamashiro
*Fumihiko Handa: Hiromasa Taguchi
*Tadashi Shiina: Satoshi Uzaki

;Theme Song
*In My Dream (Tube (band)|TUBE)
The film is available on DVD through Pathfinder Home Entertainment, and has been retitled to "White Collar Worker Kintaro".

=== Live-action TV series ===
;Seasons
*January 10~March 21, 1999: Salaryman Kintarō (11 Episodes, Average Rating: 19.0%)((cn))
*October 3, 1999: Salaryman Kintarō Special
*April 9~July 2, 2000: Salaryman Kintarō 2 (12 Episodes, Average Rating: 16.3%)((cn))
*January 6~March 17, 2002: Salaryman Kintarō 3 (11 Episodes, Average Rating: 15.5%)((cn))
*January 15~March 18, 2004: Salaryman Kintarō 4 (10 Episodes, Average Rating: 11.2%)((cn))

;Cast
The main cast from the film version returned for the television series.

;Theme Songs
*Season 1:   (The Alfee)
*Season 2:   (Tetsurō Oda)
*Season 3:   (Katsunori Takahashi)
*Season 4:   (The Alfee)

=== Anime TV series ===
;Cast
*Kintaro Yajima: Taisei Miyamoto Atsuko Tanaka
*Mimi Suenaga: Ryōka Yuzuki
*Morinosuke Yamato: Kiyoshi Kawakuba
*Genzo Oshima: Takeshi Watabe
*Yusaku Kurokawa: Nachi Nozawa
*Ryuzo Igo: Kōsei Tomita
*Kayo Nakamura: Seiko Tomoe
*Ichiro Maeda: Kōichi Nagano
*Masakazu Tanaka: Tomoyuki Kōno
*Hiroshi Kaminaga: Katsuhisa Hōki
*Takashi Shiina: Kunihiko Yasui
*Mamoru Mizuki: Nobuaki Sekine
*Kyoko Sakurai: Masako Katsuki

;Theme Songs
*Opening Theme:   (Yumi Matsuzawa)
*Ending Theme:   (Norishige Takahashi)

The anime is available subtitled on DVD through Arts Magic.

==References==
 

== External links ==
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 