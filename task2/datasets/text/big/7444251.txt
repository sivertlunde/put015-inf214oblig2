Love and Other Catastrophes
 
 
{{Infobox film
| name           = Love and Other Catastrophes
| image          = Love and Other Catastrophes.jpg
| image size     =
| caption        =
| producer       = Helen Bandis Stavros Kazantzidis Yael Bergman
| director       = Emma-Kate Croghan
| writer         = Story: Stavros Kazantzidis Screenplay: Yael Bergman Emma-Kate Croghan Helen Bandis
| starring       = Frances OConnor  Alice Garner  Radha Mitchell Matthew Dyktynski Matt Day Kym Gyngell
| music          = Oleh Witer
| cinematography = Justin Brickle
| editing        = Ken Sallows
| distributor    = Fox Searchlight Pictures
| released       = 1 August 1996 (Australia) 28 March 1997 (US)
| runtime        = 79 minutes
| country        = Australia
| language       = English
| budget         = $250,000 (est.)
| gross          = $431,000 (US thru April 1997)
}}
Love and Other Catastrophes is a quirky 1996 Australian romantic comedy film featuring Frances OConnor, Radha Mitchell, Alice Garner, Matthew Dyktynski, Matt Day and Kym Gyngell. The film was the first full length release by director Emma-Kate Croghan and is set and filmed at Melbourne University where she studied writing and film directing.

The film was nominated for five Australian Film Institute awards, including best film, best original screenplay, best actress, best supporting actress, and editing. Garner won a Film Critics Circle of Australia award for best supporting actress for her role in the movie.

==Plot==
The story revolves around University students and roommates Mia (Frances OConnor) and Alice (Alice Garner), each of whom is experiencing various upheavals. Mia and Alice have just moved into a trendy apartment but are in desperate need of a housemate. Mias girlfriend Danni (Radha Mitchell) is keen to move in, but Mia fears commitment.

Obsessed with her favourite lecturer, Mia becomes embroiled in a war of paperwork with the University administration as she attempts to pursue him to his new department. She is hampered in her efforts to transfer by her current supervisor (Kym Gyngell). To add to her woes she then breaks up with her girlfriend, Danni. Danni pursues another love interest, in part to get back at Mia.

Alice, a habitual perfectionist, is four years late with her thesis on Doris Day as Feminist Warrior. She is looking for the perfect man but cant find anyone who fits her strict criteria. Frustrated, she falls for the most unsuitable male possible... Ari (Matthew Dyktynski), a Classics student and part-time gigolo. However she is the object of desire of shy medical student, Michael (Matt Day).

As the day ends and the party begins events begin to unscramble in unexpected ways. Omnia Vincit Amor... Love Conquers All.

==Music==
Love and Other Catastrophes features music by many bands including:

* Daryl McKenzie - "Manhattan Walk"
* Velvet Underground - "Sunday Morning" The Cruel Sea - "Just a Man" Dave Graney & The Coral Snakes - "Youre Just Too Hip Baby"
* Underground Lovers - "Recognize" Godstar - "Pushpin"
* Rebeccas Empire - "Empty" Tumbleweed - "TV Genocide"
* Spdfgh - "Steal Mine"
* Tex, Don and Charlie - "Fake That Emotion"
* Monday Michiru - "Rainy Daze"
* The Boners - "Perils of Mia"
* The Cardigans - "Carnival"
* Bellydance - "Bubbles (Pigs Will Fly)"
* Blue Mink - "Can You Feel It Baby" Simon Holmes and Morgana Ancone - "Lets Do It (Lets Fall In Love)"
* Died Pretty - "Good At Love"

==Box Office==
Love and Other Catastrophes grossed $1,687,929 at the box office in Australia,. 

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 