The Inner Circle (1912 film)
 
{{Infobox film
| name           = The Inner Circle
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = George Hennessy
| starring       = Mary Pickford Blanche Sweet
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 17 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent drama film directed by D. W. Griffith, starring Mary Pickford and Blanche Sweet. A print of the film survives in the film archive of the Library of Congress.   

==Cast==
* Adolph Lestina - The Widower
* Jack Pickford - The Messenger
* J. Jiquel Lanoe - The Rich Italian
* Mary Pickford - The Rich Italians Daughter
* Charles Hill Mailes - A Gangster Joseph McDermott - Police Agent
* Alfred Paget - Police Agent
* Christy Cabanne - In Gang (as W. Christy Cabanne)
* Donald Crisp
* Gladys Egan Charles Gorman - Accident Witness
* Robert Harron - In Crowd / Accident Witness
* Mae Marsh Baden Powell
* Blanche Sweet
* Kate Toncray - In Crowd
* Henry B. Walthall Charles West

==See also==
* D. W. Griffith filmography
* Mary Pickford filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 