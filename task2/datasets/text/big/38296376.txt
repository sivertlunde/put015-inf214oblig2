Shahanpan Dega Deva
{{Infobox film
| name           = Shahanpan Dega Deva - Marathi Movie
| image          = Shahanpan Dega Deva.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Sudesh Manjrekar
| producer       = Mirah Entertainment Pvt. Ltd
| screenplay     = 
| story          = 
| starring       = Bharat Jadhav Ankush Choudhary Siddharth Jadhav Santosh Juvekar Vaibhav Mangale Kranti Redekar Manava Naik
| music          = Ajit-Sameer
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Shahanpan Dega Deva is Marathi movie released on 21 January 2011. The movie has been produced by Mirah Entertainment Pvt. Ltd and directed by Sudesh Manjrekar. The plot of the movie is a comical ride as the city’s safety rests on the shoulders of 5 lunatics from a mental asylum.

== Synopsis ==
5 mad men, Bharat, Ankush, Siddhu, Sanjay and Vaibhav are inmates in a lunatic asylum and are each battling their own lives trying to accept what life has given them. However one incident changes everything and one day they are accidentally transported to the city where the thick lines between sanity and insanity are blurring.

One funny incident after the other make them realize that this world isnt too different from the world that they have come from and people here dont  behave too differently from their inmates in the asylum. However they dont have too much time on them as a vicious and sinister plan is being hatched to blow up the city. Their escape from the jail and a near fatal attack of their guardian, the kind hearted Dr. Subodh is all a part of this plan. The 5 men not only have to save Dr. Subodh but the entire city as well.

== Cast ==

The cast includes Bharat Jadhav, Ankush Choudhary, Siddharth Jadhav, Santosh Juvekar, Vaibhav Mangale, Kranti Redekar, Manava Naik  Purva Pawar 
& Others.

==Soundtrack==
The music is provided by Ajeet and Sameer. 

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 


 