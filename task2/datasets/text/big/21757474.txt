Dead Woman from Beverly Hills
 
{{Infobox film
| name           = Dead Woman from Beverly Hills
| image          = 
| caption        = 
| director       = Michael Pfleghar
| producer       =  
| writer         = Curt Goetz Peter Laregh Michael Pfleghar Hansjürgen Pohland
| starring       = Heidelinde Weis
| music          = 
| cinematography = Ernst Wild
| editing        = Margot von Schlieffen
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Dead Woman from Beverly Hills ( ) is a 1964 West German drama film directed by Michael Pfleghar. It was entered into the 1964 Cannes Film Festival.     

==Cast==
* Heidelinde Weis – Lu Sostlov
* Klausjürgen Wussow – C.G.
* Horst Frank – Manning / Dr. Steininger
* Wolfgang Neuss – Ben
* Ernst Fritz Fürbringer – Professor Sostlov
* Peter Schütte – Swendka
* Bruno Dietrich – Peter de Lorm
* Ellen Kessler – a Tiddy Sister
* Alice Kessler – a Tiddy Sister
* Herbert Weissbach – Priest
* Walter Giller

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 