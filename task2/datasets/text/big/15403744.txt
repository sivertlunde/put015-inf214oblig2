Barakat!
 
 
{{Infobox Film
| name           = Barakat!
| image          = Barakat.jpg
| image_size     = 
| caption        = 
| director       = Djamila Sahraoui
| producer       = Richard Copans
| writer         = Djamila Sahraoui Cécile Vargaftig
| narrator       = 
| starring       = Rachida Brakni Alla
| cinematography = Katell Djian
| editing        = Catherine Gouze
| distributor    = Pierre Grise Distribution
| released       = 16 February 2006
| runtime        = 95 mins.
| country        = Algeria France Arabic
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Barakat! is a 2006 France|French/Algerian drama film directed by Djamila Sahraoui. It premiered at the Berlin International Film Festival on 16 February 2006.

==Plot==
During the Algerian Civil War, Amel (Rachida Brakni) is a doctor who, on returning home from work one day, discovers that her journalist husband has gone missing. Receiving no help from the authorities, she decides to look for him herself. She is helped by another woman, Khadidja. {{Citation
  | last = Southern
  | first = Natalie
  | title = Plot Synopsis
  | publisher = Allmovie
  | url = http://www.allmovie.com/cg/avg.dll?p=avg&sql=1:346143
  | accessdate =24 January 2008 }} 

==Cast==
*Rachida Brakni as Amel
*Fattouma Ousliha Bouamari as Khadidja
*Zahir Bouzerar as Le vieil homme
*Malika Belbey as Nadia
*Amine Kedam as Bilal
*Ahmed Berrhama as Karim
*Abdelbacet Benkhalifa as Lhomme du barrage
*Abdelkrim Beriber as Le policier
*Ahmed Benaissa as Homme accueil hôpital
*Mohamed Bouamari as Hadj Slimane

==Awards==
At the 2007 Panafrican Film and Television Festival of Ouagadougou, Barakat! won the Oumarou Ganda Award for the Best First Work, the award for Best Music and the award for Best Screenplay. {{Citation 
  | title = Child soldiers struggle takes top prize at Africas Fespaco film fest
  | newspaper = Canadian Broadcasting Centre
  | date = 5 March 2007
  | url = http://www.cbc.ca/news/story/2007/03/05/fespaco-africa-prize.html
  | accessdate=24 January 2008}}  It also won the prize for Best Arab Film at the third Dubai International Film Festival. {{Citation
  | last = Jaafar
  | first = Ali
  | title = Dubai festival lauds Algerian Barakat! Variety
  | date = 18 December 2006
  | url = http://www.variety.com/article/VR1117956011.html?categoryid=13&cs=1
  | accessdate=24 January 2008}} 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 


 
 