The Poseidon Project
{{Infobox film
| name           = The Poseidon Project
| image          =  
| director       = Arthur Jones, Luther Jones
| producer       = Arthur Jones, Luo Tong
| starring       = Steven Schwankert
| music          = Albert Yu
| cinematography = 
| editing        = Guy Rahamim
| studio         = LostPensivos Films
| distributor    = 
| released       = 
| runtime        = 82 minutes, 60 minutes
| country        = 
| language       = English, Mandarin
| budget         = 
| gross          = 
}}
The Poseidon Project is a 2013 documentary film about the search for a lost British submarine called HMS Poseidon. The submarine sank after a collision off the coast of China in 1931. It was widely assumed to be still present in the Bohai Sea, 20 miles off the city of Weihai in Chinas Shandong Province until American author and scuba diver Steven Schwankert discovered that it had been salvaged by a Chinese salvage team in 1972. Schwankerts work and the history of the submarine and its crew is the subject of The Poseidon Project. The film was directed by British brothers Arthur Jones and Luther Jones.

==Synopsis==
The film is about Beijing-based author and scuba instructor Steven Schwankerts search for the British submarine HMS Poseidon. Schwankert was looking for interesting wrecks to dive in northern China when he found HMS Poseidon on a list of unexplored dive sites. He spent the next six years working on the project, bringing together research in China and the UK to piece together the history of the submarine.

HMS Poseidon collided with a cargo ship off the coast of China on June 9, 1931. Thirty-one of the submarines crew managed to scramble into the water before the submarine sank to the seabed 130&nbsp;ft (40 m) below. But 25 were trapped inside. The submarine hit the headlines within hours because five men managed to escape from the submarine using the new Davis Submerged Escape Apparatus. They were the first people to escape from a downed submarine using proto-scuba devices.

The film charts Schwankerts efforts to examine the escape and the accident in unprecedented depth. It also follows him as he uncovers the secret salvaging of the submarine in 1972 by one of Chinas new salvage teams. The story is also told in Schwankerts book Poseidon: Chinas Secret Salvage of Britains Lost Submarine. 

==Production==
Arthur Jones and Luther Jones shot the first scenes of The Poseidon Project in Weihai, China in early 2009. Several return trips to Weihai ensued as Steven Schwankert came closer to discovering the fate of HMS Poseidon, and the location of the British cemetery that was originally located off the coast on Liugong Island. In China, other filming took place in Beijing and Shanghai.

Further scenes were shot in the UK, specifically at Gosports Royal Navy Submarine Museum, at the Dock Museum in Barrow-in-Furness, at the National Archives in Kew, London, and in and around south London, where the grave of Poseidon escapee Patrick Willis was assumed to be located.

Postproduction took place during 2012 and early 2013. The Poseidon Project premiered at the  Hoboken International Film Festival on 3 June 2013. 

==Awards and nominations==

* WINNER: Best Documentary Feature -  
* Official Selection: Hoboken International Film Festival (2013)  
* Official Selection: Oaxaca Film Festival (2013)  
* Official Selection: Friday Harbor Film Festival (2013)  

==See also==
* China Station
* List of submarine classes of the Royal Navy

==References==
 

==External links==
*  
*  
*   

 
 
 