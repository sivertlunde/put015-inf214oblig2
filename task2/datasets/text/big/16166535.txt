Seerivarum Kaalai
{{Infobox film
| name           = Seerivarum Kaalai
| image          = 
| caption        = 
| director       = Ramarajan
| writer         = Ramarajan Mansoor Ali Manorama
| producer       = Nalini
| music          = Soundaryan
| editor         = 
| released       = 13 April 2001
| runtime        = 
| language       = Tamil
| country        = India
| cinematography = 
}} Sethu fame. Mansoor Ali Khan, Anandaraj.

==Plot==
Kaalai is the do-gooder always ready with a helping hand. Literally, because when women are pushed around and shoved, it is Kaalai who rushes with his arms readily extended to support them. And this happens a bit too often. Kaalai falls for his sisters friend Kamakshi (Abita) who is tortured and harassed by her sister-in-law Nandini (Vichitra), and Kamakshis mother and brother (Manorama - Mansur) are helpless spectators. Once when Kamakshis son gives her a violent shove, it is Kaalai who rushes and supports her in his arms. He sternly warns people that if they harass Amma, they would one day regret it.

After the initial hesitation the timid Kamakshi reciprocates Kaalais love. The duo elopes and gets married. After a violent bashing up of Kaalai & Kamakshi by Nandinis men, the doctors warn Kaalai that Kamakshis health would be endangered if she were to bear a child. But Kamakshi drugs Kaalai and seduces him. She conceives, but there is nothing to worry. For, as usual the doctors are proved wrong and she delivers twins. It is time for Kaalai and the gang to hold up their two fingers in victory style. 

==Cast==
*Ramarajan
*Abitha Alex
*Anandaraj
*Senthil
*S S Chandran Manorama
*Vichithra 

==References==
 

 
 
 
 


 