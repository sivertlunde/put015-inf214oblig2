Haji Agha, the Cinema Actor
 

{{Infobox film
| name           = Haji Agha, the Cinema actor
| image          = Haji Agha, the Cinema Actor.webm
| alt            = 
| caption        = Full film
| director       = Ovanes Ohanian
| producer       = Ovanes Ohanians   Habibollah Morad   Maghasedzadeh Foroozin
| writer         = Ovanes Ohanian
| starring       = Habibollah Morad   Asia Qostanian   Zema Ohanians   Abbas khan Tahbaz
| music          = 
| cinematography = Paolo Potomkin   Ebrahim Moradi   
| editing        = Ovanes Ohanian
| studio         = Pers Film
| distributor    = 
| released       = 1933
| runtime        = 75 minutes
| country        = Iran
| language       = silent
| budget         = 
| gross          = 
}} Iranian comedy Ovannes Oganians and one of a few remaining Iranian silent films.  This was Ohaninas second film in Iran after the success of "Abi and Rabi", his first silent film. The film reflects the clash between tradition and modernity in Iranian society in the early 1930s.  While Abi va Rabi did well commercially, Haji Agha Aktor-e Cinema did not succeed at the box office due to its technical shortcomings and the fact that its release coincided with the first Persian talkie, Dokhtar-e Lorr. 

The film tells the story of a director (played by Ohanians) is searching for a subject for his film, when he receives a suggestion to film Haji Agha, a wealthy conservative man. Haji’s daughter, son-in-law, and servant help the director orchestrate a series of events that enable the director to film Haji in action. When the film is finished and Haji views it, he sees his own image on the screen and, enthralled by it, begins to appreciate the merits of cinema. 

==Plot==
A director (Ovanes Ohanians) looks for a subject for his movie and someone suggests that he films Haji Agha secretly. Haji is very rich and frowns upon cinema. Hajis daughter, son-in-law, and servant help the director with the film as well. Hajis watch gets lost, and he suspects his servant. Haji and his son-in-law start chasing him. At first, they tail him to the dentists, and then they meet a fakir who claims he can find the lost watch. He does some strange things. The director photographs Haji all the time. Then Haji watches the film and becomes aware of the true merits of cinema. 

==Cast ==
*Abbas-Gholi Edalatpour	as Puri	
*Asia Ghostantin as Parvin		
*Habibollah Morad as Haji Agha		
*Ovanes Ohanian	as The director	
*Zema Ohanian as Pari		
*Gholam-Hossein Sohrabi-Fard as Monsieur Abi	
*Abbas-Kahn Tahbaz as Parviz

==Production==
Shooting started in June 1932 and the camera used was a "Pathé|Pathe" camera which was more than 20 years old. 

==References==
 

==External links==
*  in IMDb

 
 
 
 
 
 
 