Brewster McCloud
{{Infobox film
| name           = Brewster McCloud
| image          = Brewster McCloud.jpg
| writer         = Doran William Cannon Michael Murphy
| director       = Robert Altman
| music          = Gene Page
| cinematography = Lamar Boren Jordan Cronenweth Lou Lombardo
| producer       = Lou Adler
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 105 minutes
| language       = English
| budget         =
}}
Brewster McCloud is a 1970 film|movie, directed by Robert Altman, about a young recluse (Bud Cort, as the titular character) who lives in a fallout shelter of the Houston Astrodome, where he is building a pair of wings so he can fly. He is helped by his comely and enigmatic "fairy godmother", played by Sally Kellerman. The film was shot on location in Houston, Texas. During the opening credits, shots of the downtown Houston skyline (with One Shell Plaza under construction) zoom toward the Houston Astrodome and Astrohall, with the emerging Texas Medical Center in the background. It was the first film shot inside the Astrodome.

==Plot==
The film opens with the MGM logo, as usual, but with the voice of Rene Auberjonois saying, "I forgot the opening line," replacing the lion roar  and proceeds with The Lecturer (Auberjonois) regaling his unseen students with a wealth of knowledge of the habits of birds. Owlish Brewster (Bud Cort), living hidden and alone under the Houston Astrodome, dreams of creating wings that will help him fly like a bird. His only assistance comes from Louise (Sally Kellerman), a beautiful woman who wants to help. Wearing only a trench coat, Louise has unexplained scars on her shoulder blades, suggestive of a fallen angel. She warns him against having sexual intercourse, as this could kill his instinct to fly.

While Brewster works to complete his wings and condition himself for flight, Houston suffers a string of unexplained murders, the work of a serial killer whose victims are found strangled and covered in bird droppings.  Haskell Weeks (William Windom) a prominent figure in Houston, pulls strings to have the Houston Police call "San Francisco super cop" Frank Shaft (Michael Murphy) to investigate.  Shaft immediately fixates on the bird droppings and soon finds a link to Brewster. Brewster eludes the police with the apparent help of Louise but he eventually drives her away—and dooms himself—when he ignores her advice about sex by hooking up with Astrodome usher Suzanne (Shelley Duvall). Suzanne saves Brewster by out-driving Shaft in her Plymouth Road Runner. Severely injured after losing Brewster, Shaft kills himself. Despite her sweetness, Suzanne will not give up her comfortable home to fly with Brewster. Sensing something very wrong with Brewster, Suzanne betrays him to the police.

In the climactic scene, a small army of policemen enter the Astrodome but fail to nab Brewster before he takes flight using his completed wings.  Although Brewster escapes the police, he cannot escape the human beings inherent unsuitability for flight.  Exhausted by the effort, he falls out of the air, crashing in a heap on the floor of the Astrodome. The film ends with a Circus entering the Astrodome, played by the cast of the film, costumed as clowns, strongmen and other circus performers.  The Ringmaster (played by William Windom) announces the names of each cast member, finishing with Brewster, who remains crumpled on the floor.

==Cast==
 
* Bud Cort as Brewster McCloud
* Sally Kellerman as Louise Michael Murphy as Detective Frank Shaft William Windom as Weeks
* Shelley Duvall as Suzanne Rene Auberjonois as The Lecturer Margaret Hamilton as Daphne Heap
* Corey Fischer as Officer Hines
* Stacy Keach as Abraham Wright
* John Schuck as Officer Johnson
* Bill Adair as detective
* Bert Remsen as Officer Breen
* Jennifer Salt as Hope
 

==Cultural references== California Split. {{cite news
  | last = McGilligan
  | first = Patrick
  | coauthors = 
  | title = Robert Altman: Jumping Off the Cliff
  | work = 
  | pages = 376
  | language = 
  | publisher = St. Martins Press
  | year = 1989
  | url = 
  | accessdate = }} 

Visual references to  , who played the Wicked Witch of the West, appears wearing ruby slippers. Hope (Jennifer Salt), who supplies Brewster with health food, resembles Dorothy Gale|Dorothy, with her distinctive gingham dress, pigtails and basket. At the end of the film, Hope is shown in the cast as Dorothy, carrying Toto (Oz)|Toto.

==Production==
This film marks the first feature produced by Altmans Lions Gate Films.  The film records landmarks and streetscapes that later were demolished or radically changed. The hotel Frank Shaft checks into was part of the Astrodome complex; it has gone through several changes.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 