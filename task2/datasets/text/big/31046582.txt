Love and Action in Chicago (film)
{{Infobox film
| name           = Love And Action In Chicago
| image          = loveandactioninchicago.jpg
| caption        = Theatrical poster
| director       = Dwayne Johnson-Cochran
| producer       = David Forrest Beau Rogers Betsey Chasse Danny Gold
| writer         = Dwayne Johnson-Cochran
| starring       = Courtney B. Vance Regina King Kathleen Turner
| music          = Russ Landau
| cinematography = Phil Parmet
| editing        = J. Kathleen Gibson
| distributor    = MTI Home Video
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
}} Windy City. 

==Plot==
Eddie (Courtney B. Vance) is trained killer for the Eliminator Corp, a shadowy government organization dedicated to wasting societys undesirables. Since he is a devout Catholic, he reconciles his beliefs and career choice by taking a vow of celibacy. Sensing that her underling could use some fun, Eddies boss—known only as Middleman (Kathleen Turner) -- sets him up on a blind date with no-nonsense Lois Newton (Regina King). The tenacious Lois is struck by the gloomy loner and sets about trying to break Eddies vow of chastity and his homicidal line of work. In the process, Eddie begins to rethink his life, much to the dismay of Middleman and his work associates. Jason Alexander, Michael Gilio, and Ed Asner co-star in this debut effort by writer-director Dwayne Johnson-Cochran. Love and Action in Chicago premiered at the 1999 Toronto Film Festival.

==Cast==
*Courtney B. Vance ...  Eddie Jones 
*Regina King ...  Lois Newton 
*Kathleen Turner ...  Middleman 
*Jason Alexander ...  Frank Bonner 
*Edward Asner ...  Taylor 
*Robert Breuler ...  Oli 
*Michael Gilio ...  Martin 
*David Basulto ...  Bob 
*Mindy Bell ...  Secretary 
*JoBe Cerny ...  Mr. Jensen 
*Patrice Pitman Quinn ... Linda

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 