The Mosquito Net
{{Infobox film
| name           = The Mosquito Net
| image          = Lamosquitera1.jpg
| alt            =  
| caption        = Promotional poster
| director       = Agustí Vila
| producer       = Luis Miñarro
| writer         = Agustí Vila
| starring       = Emma Suárez Geraldine Chaplin Eduard Fernández Martina García
| music          = Alfons Conde
| cinematography = Neus Ollé-Soronellas
| editing        = Martí Roca
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Spain
| language       = Catalan Spanish
| budget         = 
| gross          = 
}}
The Mosquito Net (Original title: La mosquitera) is a 2010 Spanish drama film about a dysfunctional family. It was written and directed  by Agustí Vila. It stars  Emma Suárez, Geraldine Chaplin, Eduard Fernández and Martina García.

==Plot== Catalan family are in a state of disarray. They are Miguel, his wife Alicia, their son Lluís, and Miguels elderly parents. Lluís collects stray cats and dogs that now dominate the family apartment, much to his fathers chagrin. Alicia is incapable of challenging  the whims of her son and ponders separating from her husband. Miguels mother, Maria suffers from Alzheimers and her husband believes suicide may be the solution to their problems. Meanwhile Alicias friend, Raquel believes in a tough love method of parenting her daughter, albeit a method that leads to abuse. 

==Cast==
*Emma Suárez as Alícia
*Geraldine Chaplin as María
*Eduard Fernández as Miguel
*Marcos Franz as Lluís
*Martina García as Ana
*Anna Ycobalzeta as Raquel
*Àlex Brendemühl as Editor
*Fermí Reixach as Robert
*Àlex Batllori as Sergi

==Production==
Filming began in Barcelona on 9 November 2009. The film represents the linguistic texture of the city, with the use of both Spanish and Catalan. Madrid actress, Emma Suárez had to learn Catalan especially for her role. Geraldine Chaplin: "La Mosquitera es el mejor guión que he leído en mi vida". EFE Newswire. 18 November 2009 

Chaplin described the enthusiasm she had for the project, revealing that it was "the best script that I have ever read in my life". 

==Reception==
 
The Hollywood Reporter described the film as "intriguing" and that "the questions it raises stick in the mind. It should engage audiences with a taste for complex urban quandaries presented with Spanish flair." 

Suárez was nominated for the Goya Award for Best Actress and won the Best Actress Award at the Valladolid International Film Festival.  The film also won the Crystal Globe at the Karlovy Vary International Film Festival. 

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 