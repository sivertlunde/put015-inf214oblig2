Welcome to Purgatory
{{Infobox film
| name           = Welcome to Purgatory
| image          = Welcome to Purgatory film poster.png
| alt            =
| caption        = Film poster
| director       = Gene Fallaize
| producer       = Tony Cook Gene Fallaize
| screenplay     = Tony Cook Gene Fallaize
| story          = Marcus Ako Tony Cook
| starring       =  
| music          = Joseph Bennie
| cinematography = Geoff Boyle
| editing        = Damian Knight
| studio         = Cupsogue Pictures
| distributor    = Cupsogue Pictures
| released       =  
| runtime        = 112 minutes
| country        = United Kingdom United States
| language       = English
| budget         = £7.3 million 
| gross          =  
}}
Welcome to Purgatory is an upcoming film directed by Gene Fallaize  and starring Jillian Murray, Tony Cook and Serena Lorien. It is set to release in October 2015.

==Summary==
Three new arrivals in Purgatory must find their way around the afterlife with the help of St. Paul. Unfortunately for them, the gates separating heaven and hell have been broken, leaving the world of angels and demons in ruins. Soon, they are tasked with putting things right to avert a war between God’s celestial hosts and Satan’s devilish army. 

==Cast==
* Jillian Murray as Danni 
* Tony Cook as Taylor
* Serena Lorien as Nina   
* Brian Blessed as Guardian Paul 
* Peter Stormare as Lucius   
* Nathan Jones as Goroh James Buckley  as Luke
* Bruce Campbell    as Man in Hawaiian Shirt
* Jack OHalloran as Guardian Simon
* Georgina Sherrington  as Skinny Shaitan

==Production==
The film was planned to be shot at Pinewood Studios in London.   

==References==
 

==External links==
*  
* 

 
 
 

 