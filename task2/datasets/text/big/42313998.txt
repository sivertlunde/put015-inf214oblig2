The End is Known
{{Infobox film
 | name = The End is Known
 | image =The End is Known.jpg
 | caption =
 | director = Cristina Comencini
 | writer =  Cristina Comencini  Suso Cecchi DAmico
 | starring =  
 | music =  Fiorenzo Carpi Alessio Vlad Claudio Capponi
 | cinematography =   Dante Spinotti
 | editing =    Nino Baragli
 | producer =  
 }}
The End is Known ( ,  ) is a 1992 Italian-French mystery film directed by Cristina Comencini. It is an adaptation of the novel with the same name by Geoffrey Holiday Hall, in which the setting is moved from post-war America to 1980s Italy.      

== Cast ==

*Fabrizio Bentivoglio: Lawyer Bernardo Manni
*Valérie Kaprisky: Maria Manni
*Carlo Cecchi: "Cervello" (the brain) 
*Mariangela Melato: Elena Malva
*Valeria Moriconi: Elvira Delogu
*Massimo Wertmüller: Carlo Piane
*Corso Salani: Rosario
*Daria Nicolodi: Lawyer Mila
*Valeria Milillo: Archivista 
*Stefano Viali: Lawyer Anselmi
*Marina Perzy: Miss Gerli

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 