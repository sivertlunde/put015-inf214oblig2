Hyde Park on Hudson
 
 
{{Infobox film
| name           = Hyde Park on Hudson
| image          = Hyde park on hudson poster.jpg
| director       = Roger Michell
| producer       = David Aukin Kevin Loader Richard Nelson
| starring       = Bill Murray Laura Linney 
| music          = Jeremy Sams
| cinematography = Lol Crawley
| editing        = Nicolas Gaster Daybreak Pictures Film4 Productions Free Range Films
| distributor    = Focus Features
| released       =  
| runtime        = 95 minutes  
| country        = United Kingdom
| language       = English
| gross          = $8,887,603 
}}
 biographical Historical historical comedy-drama Margaret "Daisy" Suckley, respectively. It was based on Suckley’s private journals and diaries, discovered after her death,          about her love affair with and intimate details about President Roosevelt.  

==Plot== Margaret "Daisy" country estate in Hyde Park, New York. Although Daisy and Roosevelt had not seen each other for years, the distant relatives form a romantic relationship, and Roosevelt often asks Daisy to visit Hyde Park when he stays with his mother. Daisy becomes one of the several women close to Roosevelt, including Sara; Missy LeHand, the presidents secretary; and Eleanor Roosevelt|Eleanor, the presidents wife. Despite his power, the president is often unable to control the other women; the quiet, shy Daisy is his confidante, and he tells her that Top Cottage will be their shared refuge after his presidency.
 Queen Elizabeth, visit the future war inability to walk, and observes that others do not see their handicaps because "its not what they want to see." The president tells the king that he hopes to overcome United States non-interventionism#Non-interventionism shortly before World War II|Americans reluctance to help Britain.

The night the king and queen arrive, Daisy sees LeHand having an affair with Roosevelt. LeHand tells a shocked Daisy that their respective relationships with the president are not his only ones, mentioning Dorothy Schiff and Lucy Mercer Rutherfurd, and that Daisy must accept sharing Roosevelt with other women. At the picnic the next day, the king successfully eats a hot dog for a photo op, and Daisy in a voiceover states that the visit helped the two countries form a Special Relationship. Daisy rejects Roosevelts requests to see her until he visits her himself; they reconcile, and Daisy accepts her role as one of the presidents mistresses. As years pass, Daisy watches Roosevelt become frail as a wartime leader; nonetheless everyone, she says, "still   to him, still seeing whatever it was they wanted to see."

==Main cast==
 
*Bill Murray as Franklin D. Roosevelt Margaret "Daisy" Suckley King George VI Queen Elizabeth
*Elizabeth Marvel as Marguerite LeHand
*Olivia Williams as Eleanor Roosevelt Sara Delano
*Martin McDougall as Thomas Gardiner Corcoran James Cameron
 

==Production== Richard Nelson Rhinebeck and had even met Daisy briefly before she died in 1991.

Originally conceived as an idea for a film, Nelsons choice of director, Roger Michell, proved immediately unavailable. Nelson re-worked the script as a radio play, which was produced by the BBC in 2009, directed by Ned Chaillet. Once Michell became available, production began on the film. 

In early March 2011, director Michell started searching for U.S. actors to play President Roosevelt and Eleanor Roosevelt.  Bill Murray agreed to play Roosevelt in late March.  Production designer Simon Bowles created upstate New York in England where the entire film was shot.

==Release== world premiere at the 2012 Telluride Film Festival on 31 August 2012, then at the 2012 Toronto International Film Festival on 10 September 2012, and again at the 2012 Savannah Film Festival on 31 October 2012, with limited release in the United States on 5    and 7 December 2012, and wide release in January 2013. The UK release followed on 1 February 2013.

===Critical reception===
The film has received mixed reviews from critics. On Rotten Tomatoes, it has a 37% "rotten" rating based on 150 reviews.  On Metacritic, the film holds a 55/100 rating, indicating "mixed or average reviews." 

The A.V. Club named it one of the worst movies of 2012, criticizing "the slapdash manner in which it’s assembled is genuinely shocking" and its "prevailing idiocy."  , The A.V. Club, 20 December 2012, Retrieved 20 December 2012. 

  also praises film, saying although the movie is not great, it "has a particular kind of merit... It conveys something of a transparent experience, suggesting that the power of the subject escapes the attempt to contain it in a film and makes its way directly—albeit incidentally or even accidentally—to the viewer." 

Bill Murrays performance as Roosevelt did garner praise, as he received a Golden Globe Award nomination for Best Actor in a Motion Picture - Musical or Comedy.

Production designer Simon Bowles won an award from the British Film Designers Guild on 27 January 2013 for his production design on this film.

===Criticism of the depiction of history=== Lucy Mercer during World War I. However, there is no direct evidence that he had a similar relationship with Suckley,    though there was an emotional connection.   Roosevelt apparently instructed Suckley to burn at least some of the letters he wrote to her,  which has fueled speculation about their content.

Focusing on how the historical events and people are portrayed, Conrad Black, author of  Franklin Delano Roosevelt: Champion of Freedom, said the film took "large, ... sometimes scurrilous, liberties with historical facts."    In particular, he stated the movie erred in its depiction both of Roosevelts relationship with women and of Eleanor Roosevelts sexuality. 

Footage of the real events was filmed by Pathe News. 

==See also==
*The Kings Speech
*Warm Springs (film)

==References==
 

==External links==
*  
*  
*   at Rotten Tomatoes
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 