84C MoPic
 
{{Infobox film
| name           = 84C MoPic
| image          = 
| alt            =  
| caption        = 
| director       = Patrick Sheane Duncan
| producer       = Michael Nolin
| writer         = Patrick Sheane Duncan
| starring       = 
| music          = Donovan
| cinematography = 
| editing        = Stephen Purvis
| studio         = 
| distributor    = New Century Vista Film Company
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English Vietnamese
| budget         = 
| gross          = $154,264 (USA)
}}
 American independent war film drama by Patrick Sheane Duncan.

==Story==
The film is created as a mock-up documentary of a Long Range Reconnaissance Patrol (LRRP) mission during the Vietnam War. The point of view is from a camera following a LRRP team on a five-day patrol deep in "Indian Country" (territory controlled by the North Vietnamese). The cameraman is nicknamed "MoPic" by the team, because of his alphanumeric military occupational specialty, 84C20, Motion Picture|Motion Picture Specialist. The supposedly routine mission, however, goes wrong and eventually turns into a struggle for survival.

==Cast== LT
*Nicholas Cascone as Easy
*Jason Tomlins as Pretty Boy
*Christopher Burgard as Hammer Cracker
*Richard Richard Brooks as OD
*Byron Thames as MoPic

==Production== found footage, a style famously implemented by Blair Witch Project and Paranormal Activity.

==Reception==
The film received three nominations:
*1989 - Sundance Film Festival, Grand Jury Prize, Dramatic (Patrick Sheane Duncan)
*1990 - Independent Spirit Award, Best First Feature, Patrick Sheane Duncan (Director); Michael Nolin (Producer)
*1990 - Independent Spirit Award, Best Screenplay, Patrick Sheane Duncan

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 