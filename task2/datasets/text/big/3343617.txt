A Little Princess (1995 film)
{{Infobox film
| name           = A Little Princess
| image          = Alittleprincessposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Alfonso Cuarón Mark Johnson
| screenplay     = Richard LaGravenese Elizabeth Chandler
| based on       =   Liam Cunningham Vanessa Lee Chester
| music          = Patrick Doyle 
| cinematography = Emmanuel Lubezki
| editing        = Steven Weisberg
| studio         = Baltimore Pictures
| distributor    = Warner Bros. Family Entertainment
| released       =  
| runtime        = 97 minutes   
| country        = United States
| language       = English
| budget         = $17 million
| gross          = $10,015,449 
}} Liam Cunningham, 1939 cinematic version and takes creative liberties with the original story.

Due to poor promotion by Warner Bros.,   the film hardly made back half its budget. However, the film was critically acclaimed  and given various awards, such as two Academy Award nominations   for its significant achievements in art direction and cinematography, among other aspects of its production.

==Plot== British as an officer in World War I. He enrolls her at Miss Minchins Seminary for Girls in New York, and instructs the headmistress to spare no expense making sure his daughter will be comfortable while he is gone. He has reserved her the schools largest suite, and gives Sara a specially-made French doll, named Emily, telling her that if she want to talk to him, just speak to Emily and he will hear it. Though she finds the strict rules and harsh attitude of the headmistress stifling, Sara quickly becomes popular among the girls, including the African-American servant girl Becky, for her kindness and strong sense of imagination. She writes constant letters to her father, which are a a great source of happiness for him on the battlefields of Europe. 

Due to a body being misidentified, Captain Crewe is declared dead, when he is actually seriously injured and suffering from amnesia, and the British government seizes his company and assets. When the headmistress hears the news, she is in the middle of throwing Sara a very lavish and expensive birthday party, hoping to extort more money from her father. When Crewes solicitor arrives and tells her there will be no more money, she is furious. Since Sara is now penniless and has no known relatives, she decides to move her into the attic with Becky to work as a servant.
 missing in action. He is asked to identify a soldier suffering from amnesia, but is disappointed to discover it is not John. His Indian assistant Ram Dass encourages him to take in the man anyway, reminding him that he may know what happened to his son.

Though her life is bleak, Sara remains kind to others and continues to hold onto her belief that all girls are princesses. When her friends sneak up to see her and are caught by Miss Minchin, she protects them by saying she invited them. As punishment, Miss Minchin locks Becky into her room and assigns Sara to perform both Beckys and her own chores for the next day, without anything to eat for both of them. To distract them from their hunger, they imagine a huge banquet. The next morning they wake to find their dream has come true, having secretly been left there by Ram Dass.

When Miss Minchin discovers all the finery in their rooms, she assumes they somehow stole it and summons the police. Sara narrowly avoids arrest by perilously climbing over to the Randolph house. While hiding from the police searching the house, she comes across the soldier and realizes it is her father. Captain Crewe, though sympathetic to the girl, does not recognize her at all. As she tries to make him remember, Miss Minchin and the police arrive. Though the headmistress clearly recognizes Crewe, she claims that Sara has no father and has the officers drag her out. As the police are about to take Sara away, along with Becky, Crewe suddenly regains his memory and rescues his daughter.  

The Crewes wealth is restored to them, and they adopt Becky. The Captain tells Mr. Randolph his son died in a gas attack, giving the man closure. The boarding school is removed from Miss Minchins care and given to Mr. Randolph, becoming a much happier place. Miss Minchin is later seen reduced to a chimney sweeper working for a boy she previously mistreated. After saying goodbye to all the girls, Sara leaves with her family to return to India.

==Cast==
* Liesel Matthews as Sara Crewe. She is sent to live in a boarding school while her father goes off to fight in the war. She becomes known as a storyteller and a princess. 
* Eleanor Bron as Miss Minchin, a cruel and selfish woman who forces Sara to work as a servant and tries to encourage her that she is no longer a princess but she is later seen reduced to a Chimney Sweeper. She is Amelias older sister. Liam Cunningham Prince Rama, a wealthy British aristocrat.
* Vanessa Lee Chester as Becky, Miss Minchins servant who lives in the attic of the school.
* Rusty Schwimmer as Amelia, Miss Minchins long suffering sister. She later falls in love with the milkman and escapes with him.
* Arthur Malet as Charles Randolph, a kind older man who lives next door to the school.
* Errol Sitahal as Ram Dass, Randolphs servant who sailed to America on the same ship as Sara and her father. Princess Sita
* Taylor Fry as Lavinia
* Heather DeLoach as Ermengarde
* Peggy Miley as Mabel
* Darcie Bradford as Jesse
* Rachael Bella as Betsy
* Alexandra Rea-Baum as Gertrude
* Camilla Belle as Jane
* Lauren Blumenfeld as Rosemary
* Kelsey Mulrooney as Lottie
* Kaitlin Cullum as Ruth
* Jonás Cuarón as Chimney-Sweep (uncredited)

==Soundtrack==
{{Infobox album
| Name = A Little Princess
| Type = Film Score
| Artist = Patrick Doyle
| Cover = AlbumALP95Cover.jpg
| Released = May 9, 1995
| Length = 49:57
| Label = Varèse Sarabande Exit to Eden (1994)
| This album = A Little Princess (1995) Sense and Sensibility (1995)
}}

All of the tracks were composed by Patrick Doyle. Three of the tracks feature soloists. The "String Quintet in C major Perger 108, MH 187" by Michael Haydn is also used in the film. The film also features the New London Childrens Choir.
 
# "Ramayana: A Morning Raga" (2:03)
# "Children Running" (0:53)
# "Cristina Elisa Waltz" (3:03)
# "The Miss Minchin School for Girls" (1:40)
# "Knowing You by Heart" (2:32)
# "Breakfast" (0:55)
# "Letter to Papa" (1:38)
# "Angel Wings" (1:07)
# "False Hope" (2:05)
# "The Trenches" (1:00)
# "Crewe and the Soldier" (1:22)
# "Alone" (1:19)
# "The Attic" (2:00)
# "On Anothers Sorrow" — Catherine Hopper (1:16)
# "The Shawl" (0:54)
# "Tyger Tyger" (0:32)
# "Compassion" (0:37)
# "For the Princess" (1:38)
# "Kindle My Heart" — Abigail Doyle (the daughter of the composer) (3:00)
# "The Locket Hunt" (3:02)
# "Midnight Tiptoe" (1:13)
# "I Am a Princess" (1:14)
# "Just Make Believe" (1:33)
# "Touched by an Angel" (1:43)
# "Emilia Elopes" (1:38)
# "The Escape" (2:58)
# "Papa!" (2:32)
# "Kindle My Heart" — Liesel Matthews (4:19)
 

==Reception==
A Little Princess holds a 97% Fresh rating on Rotten Tomatoes based on 32 reviews with the critics consensus, "Alfonso Cuarón adapts Frances Hodgson Burnetts novel with a keen sense of magic realism, vividly recreating the world of childhood as seen through the characters."   

Janet Maslin called the film "a bright, beautiful and enchantingly childlike vision", one that "draw  its audience into the wittily heightened reality of a fairy tale" and "takes enough liberties to re-invent rather than embalm Miss Burnetts assiduously beloved story."  She concludes: 
 "From the huge head of an Indian deity, used as a place where stories are told and children play, to the agile way a tear drips from Saras eye to a letter read by her father in the rain, A Little Princess has been conceived, staged and edited with special grace. Less an actors film than a series of elaborate Tableau vivant|tableaux, it has a visual eloquence that extends well beyond the limits of its story. To see Sara whirling ecstatically in her attic room on a snowy night, exulting in the feelings summoned by an evocative sight in a nearby window, is to know just how stirringly lovely a childrens film can be." 

Rita Kempley of The Washington Post called the film Cuaróns "dazzling North American debut" and wrote it "exquisitely re-creates the ephemeral world of childhood, an enchanted kingdom where everything, even make-believe, seems possible....Unlike most distaff mythology, the film does not concern the heroines sexual awakening; its more like the typical heros journey described by scholar Joseph Campbell. Sarah, the adored and pampered child of a wealthy British widower, must pass a series of tests, thereby discovering her inner strengths." 

==Awards==
{| class="wikitable" style="text-align:center" 90%"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards
|- align="center"
! Year !! Award !! Category !!  Result
|- 1995
|Los Los Angeles Film Critics Association Award Best Music Best Production Design Best Picture (2nd place) New Generation Award (Alfonso Cuarón)
| 
|- 1996
|68th Academy Award Best Art Direction and Set Decoration
| 
|- 68th Academy Academy Award Best Cinematography
| 
|- 17th Youth Young Artist Award Best Family Feature - Drama Best Young Leading Actress - Feature Film (Vanessa Lee Chester) Best Young Leading Actress - Feature Film (Liesel Matthews)
| 
|}

==Home video release== home video in August 1995. In 2004 it was released on DVD.

==See also==
* List of book-based war films (1898–1926 wars)
* Sarah... Ang Munting Prinsesa - a 1995 Filipino film adaptation of A Little Princess.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 