Tora-san's Runaway
{{Infobox film
| name = Tora-sans Runaway
| image = Tora-sans Runaway.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Akira Miyazaki
| starring = Kiyoshi Atsumi Aiko Nagayama
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 88 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  aka Tora-san Homebound  is a 1970 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Aiko Nagayama as his love interest or "Madonna".  Tora-sans Runaway is the fifth entry in the popular, long-running Otoko wa Tsurai yo series.

==Cast==
* Kiyoshi Atsumi as Torajiro 
* Chieko Baisho as Sakura
* Aiko Nagayama as Setsuko Miura
* Hisashi Igawa as Tsuyoshi Kimura
* Gin Maeda as Hiroshi Suwa
* Taisaku Akino as Noboru Kawamata
* Masamichi Matsuyama as Sumio Ishida
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Hisao Dazai as Tarō Ume
* Tokuko Sugiyama as Setsukos mother, Tomiko
* Gajirō Satō as Genkichi (Man at the Temple)
* Michio Kida as Masayoshi Takioka

==Critical appraisal==
Chieko Baisho was given the Best Actress award at both the Mainichi Film Awards and the Kinema Junpo Awards for her roles in Tora-sans Runaway and Kazoku. Yoji Yamada and Akira Miyazaki were also given the Best Screenplay award at those two ceremonies for their work on those two films. 

The German-language site molodezhnaja gives Tora-sans Runaway three out of five stars.   

==Availability==
Tora-sans Runaway was released theatrically on August 26, 1970.  In Japan, the film has been released on videotape in 1983 and 1995, and in DVD format in 2005 and 2008. 

==References==
 

==Bibliography==

===English===
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 


 