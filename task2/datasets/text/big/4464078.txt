Attilas '74
 
{{Infobox film
| name       = Attila 74 – The Rape of Cyprus
| image      =
| director   = Michael Cacoyannis
| producer   = Michael Cacoyannis
| writer     = Michael Cacoyannis
| editing    = Michael Cacoyannis Vivien Sammut Smith
| cinematography = Sakis Maniatis
| distributor = Fox Lorber
| released    = 1974
| runtime     = 141 minutes
| language    = English Greek
}}

Attila 74: The Rape of Cyprus is a 1974 documentary film by Michael Cacoyannis about the Turkish invasion of Cyprus in 1974. It was filmed on location in Cyprus and Greece in the immediate aftermath of the two Turkish invasions and subsequent occupation of approximately the northern third of the island.
 President Makarios of the Republic, Nikos Sampson – the man the Greek junta imposed as leader after their attempted coup, and many Greek Cypriot victims of the Turkish invasion.

Academy Award-nominated director Cacoyannis provides insight and analysis about events leading up to the invasion, and the subsequent events and ramifications of Turkeys occupation of Cyprus . Including interviews with pro-Makarios and pro-junta supporters, this documentary also explores the anti-American and British imperialist sentiment on the island and outrage at the lack of action taken by them and the United Nations to protect Cyprus as Turkey was effectively given carte blanche to invade and occupy despite numerous United Nations Security Council resolutions demanding their withdrawal.

==See also==
* Cinema of Greece
* Akamas (2006 film)|Akamas (2006 film)

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 

 
 