Scandal (1989 film)
{{Infobox film
| name           = Scandal
| image          = Scandal-Poster.jpg
| caption        = Theatrical release poster
| producer       = Stephen Woolley
| director       = Michael Caton-Jones Michael Thomas Joanne Whalley-Kilmer Ian McKellen Bridget Fonda Britt Ekland Roland Gift Jeroen Krabbé
| music          = Carl Davis Mike Molloy
| editing        = Angus Newton
| studio         = Palace Pictures, British Screen Productions
| distributor    = Miramax Films
| released       = 3 March 1989
| runtime        = 115 min. (UK) 106 min (US) 111 min (Canada)
| country        = United Kingdom
| language       = English
| budget         = $3 million
| gross          = £3,705,065 (UK)  $8,800,000 (US)   24 Sept. 1995: 9 . The Sunday Times Digital Archive.] Web. 29 Mar. 2014. 
}}
 British drama film, a fictionalised account of the Profumo Affair based on 1987 Anthony Summers book Honeytrap.
 British Prime Conservative Party at the following years general election.
 Lord Astor, and Roland Gift as Johnnie Edgecombe.

The films theme song "Nothing Has Been Proved" was written and produced by Pet Shop Boys and sung by Dusty Springfield.

The film was screened out of competition at the 1989 Cannes Film Festival.    The films original trailer on UK television commercials never showed any clips of the film but just a blank screen featuring the word "SCANDAL" in white text, with a voiceover saying "Its a scandal!, keep watching!". Another trailer was featuring clips was subsequently shown, as a follow-on from the original.

==Plot summary==
An English bon-vivant osteopath is enchanted with a young exotic dancer and invites her to live with him. He serves as friend and mentor, and through his wide range of contacts and his parties she and her friend meet and date members of the Conservative Party. A scandal develops when her affair with the Minister of War comes to public attention.

==Cast==
* John Hurt - Stephen Ward
* Joanne Whalley - Christine Keeler
* Bridget Fonda - Mandy Rice-Davies
* Ian McKellen - John Profumo Lord Astor
* Britt Ekland - Mariella Novotny Daniel Massey - Mervyn Griffith-Jones
* Roland Gift - Johnnie Edgecombe
* Jean Alexander - Mrs. Keeler
* Deborah Grant - Valerie Hobson
* Alex Norton - Detective Inspector Ronald Fraser - Justice Marshall
* Paul Brooke - John, Detective Sgt. Eugene Ivanov Keith Allen - Kevin, Reporter Sunday Pictorial
* Ralph Brown - Paul Mann Lord Hailsham
* Johnny Shannon - Peter Rachman

==Filming locations==
Part of Scandal was filmed at Brook Drive SE11, 42 Bathurst Mews, Bayswater, London, W2 2SB although Stephen Wards house was actually at 17 Wimpole Mews, Paddington, London W1G 8PG. 

==Soundtrack==
The soundtrack, and soundtrack album, included a specially written song by the Pet Shop Boys "Nothing Has Been Proved" for Dusty Springfield. 

==Reception==
The film received positive reviews. At the movie review aggregator Rotten Tomatoes, Scandal received an overall approval rating of 91% based on 32 reviews. 

It was rated number 1 in the Top 10 Best British Films by the Record Press website in February 2013. 

The movie made a comfortable profit. 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 