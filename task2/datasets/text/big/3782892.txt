Pack Up Your Troubles
 
{{Infobox film 
| image          = Pack_Up_Your_Troubles_LC.jpg
| caption        = US lobby card
| name           = Pack Up Your Troubles George Marshall Raymond McCarey
| producer       = Hal Roach
| writer         = H. M. Walker James Finlayson Don Dillaway
| music          = Marvin Hatley
| cinematography = Art Lloyd
| editing        = Richard C. Currier
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 67 45"
| country        = United States
| language       = English 
}} George Marshall Raymond McCarey, Pack Up Your Troubles in Your Old Kit-Bag, and smile, smile, smile." It is the teams second feature-length picture.

==Plot== James Finlayson) and wind up in kitchen police. Misunderstanding the cooks instructions on disposing of the garbage cans results in dumping the garbage in the generals private dining room. While in prison, they are threatened by the cook (George Marshal), also jailed due to their "snitching", with violence, once he is out and has his knife again. While serving in the trenches of France, the pair befriend a man named Eddie Smith, who is killed in action by the Imperial German Army.In the same battle, the boys manage to capture several enemy soldiers, in a rather unusual way.

After the War is over, Stan and Ollie venture to New York City, where they begin a quest to reunite Eddies baby daughter (Jacquie Lyn) with her rightful family. The task proves both monumental and problematic as the boys discover just how many people in New York have the last name "Smith". Two attempts at face-to-face meetings, result in riling up a prize fighter and disrupting a wedding, so the boys resort to telephoning.
 Charles Middleton) who asks them to hand Eddies baby over to him, to be placed in the orphanage. The boys refuse, and insult him. The man says he will return with police to take Eddies baby, and have the boys arrested.They try to secure a loan, but the banker has no desire to loan money to the operators of a mere diner, so when he is knocked out by ceiling plaster, they steal the money.

The boys head home to hide Eddies baby in a dumbwaiter, but she is found anyhow. Brought to the banker so he could identify them as the thieves, it is discovered that HE is the Mr. Smith they had been looking for. There is a happy re-union, and The banker drops charges and asks that they be guests for dinner. Noise from the kitchen indicates that the chef has not taken this change of plans well. He storms out to tell his boss that hes not going to cook for guests on a moments notice, recognizes the two as the "snitches" (and is recognized by them as the angry army cook) and, big knife in hand, chases after the two, ending the movie.

==Cast (in credits order)==
*Stan Laurel as Stan
*Oliver Hardy as Ollie
*Don Dillaway as Eddie Smith
*Jacquie Lyn as Eddies Baby
*Mary Carr as Old Woman With Letter James Finlayson as General
*Richard Cramer as Uncle Jack Tom Kennedy as Recruiting Sergeant Charles Middleton as Welfare Assistance Officer Richard Tucker as Mr Smith
*Muriel Evans as Wrong Eddies Bride
*Grady Sutton as The Wrong Eddie
*C. Montague Shaw as Wrong Eddies Father
*Billy Gilbert as Mr Hathaway

==External links==
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 