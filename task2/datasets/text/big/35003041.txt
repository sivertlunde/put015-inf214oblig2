Ceux de la colline
 
{{Infobox film
| name           = Ceux de la colline
| image          = 
| caption        = 
| director       = Berni Goldblat
| producer       = MirFilms Cinédoc film Les Films du Djabadjah TV8 Mont-Blanc
| writer         = 
| starring       = 
| distributor    = 
| released       =  
| runtime        = 72 minutes
| country        = Burkina Faso France Switzerland
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = Michel K. Zongo
| editing        = François Sculier
| music          = 
}}

Ceux de la colline (Those of the Hill) is a 2009 Burkina Faso|Burkinabé documentary film.

== Synopsis ==
The documentary depicts an ephemeral town made up by men, women and children who all arrived with the same goal: To find gold and make it rich. The Diosso Hill, in Burkina Faso, was transformed by the presence of thousands of people, often without their families knowing that they were even there. Prospecting|Prospectors, dynamite blasters, retailers, prostitutes, healers, etc., all risk their lives daily, fight against themselves and amongst themselves and, finally, seem unable to leave this place set apart from time.

== Awards ==
* Festival International du Film Francophone de Namur (Belgium) 2009
* Brooklyn International Film Festival (USA) 2009

== References ==
 

 
 
 
 
 
 
 


 
 