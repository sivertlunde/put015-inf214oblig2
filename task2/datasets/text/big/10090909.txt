Time Limit (film)
{{Infobox_Film |
 name = Time Limit |
 image = Timelimitpost.jpg|
 caption = Theatrical release poster|
   producer = Richard Widmark William H. Reynolds|
 director = Karl Malden |
 writer = Henry Denker|
 starring = Richard Widmark Richard Basehart|
 music = Fred Steiner|
cinematography = Sam Leavitt|
 editing = Aaron Stell |
 distributor = United Artists |
 released = October 23, 1957 (U.S. release) |
 runtime = 96 minutes |
 country = United States| English Korean Korean |
gross = $1.25 million (US rentals) |
}}
 1957 legal drama film directed by Karl Malden, his only directing credit. In his autobiography, Malden stated that he "preferred being a good actor to being a fairly good director." Richard Widmark co-produced the film and stars. It is based on the Broadway play of the same name, written by Henry Denker and Ralph Berkey.

==Plot==
Army Colonel William Edwards (Richard Widmark) is investigating the case of Major Harry Cargill (Richard Basehart), accused of collaborating with the enemy while he and his unit were held captive in a North Korean prisoner of war camp. Cargill willingly admits his guilt and brings forth evidence that proves that he signed a germ-warfare confession and broadcast anti-American speeches over the radio, seemingly an act of treason. 

It seems to be an open-and-shut case, were it not for Cargills inexplicable refusal to defend himself. Arousing further suspicion is the fact that his collaboration immediately followed the deaths of two of his soldiers, and the units survivors all recite an identical, rehearsed account of those deaths. Edwards commander, General Connors (Carl Benton Reid), has a strong personal interest -- his son, Captain Joe Connors (Yale Wexler), was one of those who died -- and presses Edwards to recommend a court-martial, but Edwards delves into the mystery, refusing to accept the facile explanations.

In the end, the shocking truth comes out. Lieutenant George Miller (Rip Torn) reveals that after Lieutenant Harvey (Manning Ross) was killed trying to escape, the rest of the men discovered that, under torture, Captain Connors had betrayed him. Over Cargills strong objections, they decided to execute Connors. Drawing the short straw, Miller had to strangle him. Meanwhile, their captor, Colonel Kim (Khigh Dhiegh), had given Cargill an ultimatum: give in, or all his men would be executed. 

General Connors calls his son a traitor. Cargill argues, stating that there must be a time limit on being a hero.  He denounces the unwritten "code" espoused by General Connors for demanding too much from soldiers, but the general reminds him that while Cargill anguished over the lives and families of 16 men, that many commanders had to anguish over the effect of their orders on the lives and families of thousands.

Edwards agrees with General Connors that although Cargill acted out of a humane selflessness, Cargills judgment was flawed. He recommends that all charges be dropped, but warns Cargill that there will be a court-martial. Edwards himself will defend Cargill.  Maybe they wont come up with all the answers, Edwards tells him, but "theyll know we asked the questions."

==Cast==
*Richard Widmark as Colonel William Edwards
*Richard Basehart as Major Harry Cargill
*Dolores Michaels as Corporal Jean Evans
*June Lockhart as Mrs. Cargill
*Carl Benton Reid as General Connors
*Martin Balsam as Sergeant First Class Baker
*Rip Torn as Lieutenant George Miller
*Khigh Dhiegh as Colonel Kim (as Kaie Deei)
*Yale Wexler as Captain Joe Connors
*Alan Dexter as Mike
*Manning Ross as Lieutenant Harvey
*Joe Di Reda as Gus (as Joe Di Rida) James Douglas as Steve
*Kenneth Alton as Boxer
*Jack Webster as Lieutenant Harper

==Production==
In addition to playing the lead, Richard Widmark also co-produced Time Limit. Widmark reportedly paid $100,000 to The Theatre Guild for the film rights to the play Time Limit. It was the first picture for Widmarks independent production company, Heath Productions, Inc.

It was also his idea to have his friend and colleague, Karl Malden, direct it. In a 1988 interview about the film, Malden said, "Widmark thought Id be good directing it, and I said Sure, Id take a crack at it. I liked what it had to say." Critics gave Malden good reviews for his first directorial effort. (It turned out to be his only directing credit, with the exception of some scenes he filmed for Delmer Daves in The Hanging Tree, released in 1959.) One reviewer praised the movie for its "taut direction and vigorous performances drawn not only from principals, but a supporting cast of promising new-comers."

Most of the film was shot on location at Fort Jay on Governors Island, New York. The opening scene in the film shows Col. Edwards walking through the island, and passing through Liggett Hall. Additional scenes were also shot at the Conejo Ranch near Agoura, California.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 