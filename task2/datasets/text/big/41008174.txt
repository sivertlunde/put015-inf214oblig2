New Initial D the Movie
{{Infobox film
| name           = New Initial D the Movie
| image          = File:New-Initial-D-the-Movie-Legend1-Awakening-poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Japanese release poster
| director       = Masamitsu Hidaka
| producer       = 
| writer         = 
| screenplay     = Mayori Sekijima
| story          = 
| based on       =  
| narrator       =  Yuichi Nakamura Daisuke Ono Hiroaki Hirata
| music          = Akio Dobashi
| cinematography = 
| editing        = 
| studio         = Sanzigen Liden Films
| distributor    = Avex Entertainment
| released       = Legend 1:   Legend 2:   
| runtime        = 62 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 2014 Japanese anime film based on the manga series Initial D.  The movie is a retelling of the early stages and planned to be split into three parts with the first part, Legend 1: - Awakening released on August 23, 2014.  The full trailer was revealed on 16 May 2014, containing an entirely new cast. 

Part 2 of the series, named "Legend 2: - Racer", will be released on May 23, 2015. 

==Plot==
 Initial D}}
===Legend 1: Awakening=== Koichiro Iketani Keisuke Takahashi Akagi RedSuns Toyota Sprinter AE86 (nicknamed "Eight-Six"), which outmaneuvers Keisuke.
 Takumi Fujiwara Itsuki Takeuchi to a meet with his street racing team, the List of Initial D characters#Akina Speed Stars|Speedstars, on Mt. Akina, but are surprised to see the whole Akagi Red Suns already there. Keisuke wanted to find and race the "ghost" Eight-Six whereas List of Initial D characters#Ryosuke Takahashi|Ryosuke, Keisukes brother and leader of the Red Suns, uses the meet to initiate his plans to dominate the Kanto region. The Speedstars are humiliated by the speed of the RedSuns, but this doesnt stop Iketani from trying to keep up. As he starts another run downhill, he crashes into the guardrail after running over a sharp bump.
 Bunta Fujiwara. Bunta refuses his offer. A few days later, Takumi approaches Bunta to borrow the car to go to the coast with his friend, List of Initial D characters#Natsuki Mogi|Natsuki. Bunta consents, under the condition that Takumi goes to Mt. Akina on Saturday night and defeat Keisuke on the downhill. Unbeknownst to everyone else, it was Takumi who defeated Keisuke at that first encounter as he was returning from the daily morning delivery run.

Takumi attends the race to the bewilderment of the Speedstars, but Iketani consents to Takumi racing when he realises who Takumi was. Initially, Keisuke pulls away, but Takumi catches up as the race goes on. Eventually, Takumi passes Keisuke on one of the five-consecutive hairpins by hooking his inside tires in the inside gutter. After the race, Keisuke issues the ultimatum that Takumi does not lose until he has the chance to race him again. Takumi initially rebukes this, claiming hes not a racer, but Keisuke rejects this reason and stresses that Takumi is a racer and it is his pride as a racer to be the best before storming off. 

Takumi ponders this through during his date with Natsuki and prepares himself for the battles to come.
 Nissan Skyline Myogi NightKids Takeshi Nakazato, storms up Mt. Akina looking for Takumis Eight-Six.

==Cast==

===Japanese Cast===
* Mamoru Miyano - Takumi Fujiwara Yuichi Nakamura - Keisuke Takahashi
* Daisuke Ono - Ryosuke Takahashi
* Maaya Uchida - Natsuki Mogi
* Minoru Shiraishi - Itsuki Takeuchi
* Hiroaki Hirata - Bunta Fujiwara
* Junichi Suwabe - Takeshi Nakazato
* Hiroshi Tsuchida - Koichiro Iketani

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 


 