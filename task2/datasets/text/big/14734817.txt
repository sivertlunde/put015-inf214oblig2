Outer Space Jitters
{{Infobox Film |
  | name           = Outer Space Jitters
  | image          = Outerspace.JPEG
  | caption        = 
  | director       = Jules White Jack White 
  | starring       = Moe Howard Larry Fine Joe Besser Philip Van Zandt Emil Sitka Dan Blocker Gene Roth Harriette Tarler Diana Darrin Arline Hunter Joe Palma William Bradford  Harold White
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 16 17"
  | country        = United States
  | language       = English
}}
Outer Space Jitters is the 182nd short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges tell their infant sons (also the Stooges) a story about the time they blasted to outer space. In this story, the Stooges are assistants to Professor Jones (Emil Sitka) who travel to the planet Sunev (Venus spelled backwards). The planets leader, the Grand Slitz of Sunev (Gene Roth) greets them cordially enough, but it soon becomes apparent that he has plans to bring prehistoric men to life and take over the planet Earth. No sooner does Professor Jones catch onto the Grand Slitzs plan does he end up being tied up.
 , billed as "Don Blocker")]]

In the interim, the Stooges engage in some flirtatious activity with several Sunevian girls (Harriette Tarler, Diana Darrin, and Arline Hunter). At dinner, an alien leader, known officially as The High Mucky Muck (Philip Van Zandt) tells the Stooges to eat heartily and enjoy their meal, for it will be their last. The trio make a quick dash for the space ship, but not before encountering a prehistoric goon (Dan Blocker). The boys manage to free Professor Jones and destroy the equipment that would have conquered the Earth.

==Production notes==
Outer Space Jitters features Moe and Larrys more "gentlemanly" haircuts, first suggested by Joe Besser. These had to be used sparingly, as most of the shorts with Besser were remakes of earlier films, and new footage had to be matched with old. In Outer Space Jitters, however, Larrys frizz is combed back, while Moe retained his sugarbowl bangs. This seeming inconsistency (which would occur in future films) accommodated the gag of a frightened Moe with hair standing on end. 

==References==
 

== External links ==
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 