Talentime
 
 
{{Infobox film
| name           = Talentime
| image          =
| caption        =
| director       = Yasmin Ahmad
| producer       = Ahmad Puad Onah
| writer         = Yasmin Ahmad
| starring       = Mahesh Jugal Kishor Pamela Chong Ida Nerina Adibah Noor Jaclyn Victor
| music          = Pete Teo
| cinematography = Keong Low
| editing        = Raja Affandy Affandi Jamaludin 
| studio         = Grand Brilliance
| distributor    = Cathay-Keris Films Grand Brilliance
| released       =  
| runtime        = 120 minutes
| country        = Malaysia Hindi
| budget         = RM 1.3 million
| gross          = RM 420,000
}}

Talentime is a 2009 Malaysian drama film written and directed by Yasmin Ahmad. Yasmin, in her blog, has described it "as a story full of joy and pain, hope and despair, a host of beautifully-written songs, and rich, rich characters".  A Hindu open cremation and a scene reminiscent of the Kampung Medan incident are included in the film. 

The film was released on 26 March 2009 in Malaysia and marks Yasmins last feature film prior to her death on 25 July 2009. 

==Plot==
"A music teacher, who is herself a great performer is organising an inter-school Talent show|talentime. Through the days of auditions, rehearsals and preparations, running up to the big day of the contest, the characters get embroiled in a world of heightened emotions - ambition, jealousy, human comedy, romance, heartbreak - all of which culminate in a day of great music and performances. Yasmin also mentioned that the idea behind Talentime was that as humans, we have to go through a lot of pain and some measure of suffering before we can reach greater heights. 

A talent search competition has matched two hearts - that of Melur, a Malay-mixed girl and an Indian male student, Mahesh. Melur, with her melodious voice, singing whilst playing the piano is one of the seven finalists of the Talentime competition of her school organised by Cikgu Adibah. Likewise Hafiz, enthralling with his vocalist talent while playing the guitar, dividing his time between school and mother, who is hospitalised for brain tumor.

It all started after Mahesh, amongst the students assigned to get the finalists to school for practice, delivered the notice of successful audition to Melurs house. His handsome looks attracted the girl. Early on of their relationship, tragedy struck Maheshs family when his uncle Ganesh who had been the care-taker of the family since the loss of Maheshs father, was stabbed to death on his wedding day. Melur thinking that Maheshs silence was due to his grief over the tragedy became furious when she was continuously ignored. She regretted it however after Hafiz revealed Maheshs situation.

That changed Melurs perception of Mahesh. Likewise Mahesh, who grew comfortable with the presence of the girl who often quotes beautiful poetry. Mahesh, realising that the relationship will be opposed, kept it hidden from his mother, still grieving over the death of Ganesh. Alas, the secret was exposed and Mahesh was assaulted before Melurs very eyes. Just a day before the competition, is Melur resilient enough to sing the poetic lyrics of her song when her heart is tormented by the thoughts of Mahesh? What about Mahesh who has found his first love? On Talentime night, everything unfolds.

==Cast== Eurasian girl in the Talentime finals who sings and plays the piano.
* Mahesh Jugal Kishor as Mahesh: A hearing impaired Indian boy who becomes Melurs love interest.
* Mohd Syafie Naswip as Hafiz: A Malay boy in the Talentime finals who sings and plays the guitar. Chinese boy in the Talentime finals who plays the erhu and resents Hafiz.
* Elza Irdalynna as Mawar: Melurs younger sister.
* Azean Irdawaty as Embun: Hafizs mother who is diagnosed with terminal brain cancer.
* Harith Iskander as Harith: Melurs comical father.
* Jit Murad as Ismael: A patient who befriends Embun at the hospital during her final days.
* Mislina Mustaffa as Melurs mother.
* Tan Mei Ling as Mei Ling: A Chinese Muslim convert who works as a maid for Melurs family.
* Ida Nerina as Datin Kalsom: A friend of Melurs mother who distrusts Mei Ling.
* Adibah Noor as Cikgu Adibah: The teacher in charge of organising the Talentime.
* Sukania Venugopal as Maheshs mother.
* Jaclyn Victor as Bhavani: Maheshs elder sister who has a penchant for picking on him.

Sharifah Amani was supposed to be cast as Melur in the film. However, due to clash of schedules, she was replaced by Pamela Chong.  She did, however, play a role as the 3rd Assistant Director for the film. This would probably mark the first time that Sharifah Amani has played a behind-the-scene role in Yasmin Ahmads films.

==Music==

Film score by Pete Teo. Songs include:
* O Re Piya  
* I Go  
* Just One Boy  
* Angel  
* Kasih Tak Kembali  

Original soundtrack album released by Universal Music includes Malay language versions of many of the principal songs in the film including Pergi  , Angel and Itulah Dirimu  . The song "Ore Piya" was taken from the Bollywood movie, Aaja Nachle. Available online and in shops all over Malaysia.

==Screening== Tamil - பிஸ்மில்லாஹிர்ரஹ்மானிர்ரஹீம்

==Awards and nominations==
;22nd Malaysian Film Festival
* Best Director - Yasmin Ahmad
* Best Actor - Mahesh (nominated)
* Best Supporting Actor - Syafie (nominated)
* Best Screenplay - Yasmin Ahmad
* Best Original Story - Yasmin Ahmad (nominated)
* Best Cinematography - Soon Keong (nominated)
* Best Editing - Affandi Jamaludin (nominated)
* Best Original Music Score - Pete Teo (nominated)
* Art Direction - Nick (nominated)
* Best Promising Actress - Jaclyn Victor
* Special Jury Prize for Implementing Humanitarian Elements In A Motion Picture

;23rd Song Champion Awards of Malaysia
* Winner - Best Song for Pergi by Pete Teo / Amran Omar. Performed by Aizat Amdan

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 