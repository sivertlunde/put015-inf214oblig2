Italian for Beginners
{{Infobox film
| name           = Italian for Beginners
| image          = ItalianforBeginners.jpg
| image_size     =
| caption        = DVD cover
| director       = Lone Scherfig
| producer       = Karen Bentzon Gert Duve Skovlund Peter Aalbæk Jensen
| writer         = Lone Scherfig Maeve Binchy
| narrator       =
| starring       = Anders W. Berthelsen Anette Støvelbæk Ann Eleonora Jørgensen
| music          = Niels W. Gade (Non-original)
| cinematography = Jørgen Johansson
| editing        = Gerd Tjur
| distributor    =
| released       =  
| runtime        = 118 minutes
| country        = Denmark Sweden Danish Italian Italian English English
| budget         =
}}

Italian for Beginners ( ) is a 2000 Danish romantic comedy film written and directed by Lone Scherfig. The film stars Anders W. Berthelsen, Lars Kaalund and Peter Gantzler. The film was made by the austere principles of the Dogme 95 movement, including the use of hand held video cameras and natural lighting, and is known as Dogme XII. However, in contrast to most Dogme films which are harsh and serious in tone, Italian for Beginners is a light-hearted comedy. Made on a low budget of $600,000, the film ranks as the most profitable Scandinavian film in history. 
 Evening Class by Maeve Binchy. Zentropa has agreed to pay a non-disclosed compensation to Binchy. 

== Plot summary == Italian course in a Danish village. The class serves as a way of bringing these various residents of the town, each of whom is dealing with loss or pain, out of their loneliness and into interactions with other people. When the teacher suffers a heart attack during class and ends up dying, the six classmates hold the class anyway and eventually take a vacation to Italy.

== Cast ==
* Anders W. Berthelsen as Andreas
* Anette Støvelbæk as Olympia
* Ann Eleonora Jørgensen as Karen
* Peter Gantzler as Jørgen Mortensen
* Lars Kaalund as Hal-Finn
* Sara Indrio Jensen as Giulia
* Karen-Lise Mynster as Kirsten, the real estate dealer
* Rikke Wölck as Lise, the nurse
* Elsebeth Steentoft as Kirketjener
* Bent Mejding as Reverend Wredmann
* Lene Tiemroth as Karens Mother
* Claus Gerving as Klaus Graversen
* Jesper Christensen as Olympias Father

== Accolades == Berlin Film Festival, the Golden Spike Award for the best film of the year at the Seminci film festival in Valladolid, Spain, and the Audience Award at the Warsaw International Film Festival in Poland. Peter Gantzler won the award for Best Actor at the Seminci festival. The film also won the Gold Dolphin (Best Film) at the Festroia International Film Festival in 2001.
It currently holds an 88% certified "Fresh" rating at Rotten Tomatoes.

== References ==
 
 

== Literature ==
* Mette Hjort, Lone Scherfigs Italian for Beginners, Museum Tusculanum Press, 2010. ISBN 978-87-635-3483-3.

== External links ==
*  
*  
*   New York Times October 2, 2001

 
 

 
 
 
 
 
 
 
 
 
 