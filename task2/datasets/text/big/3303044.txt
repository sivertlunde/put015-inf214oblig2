Son of Dracula (1943 film)
{{Infobox film
| name = Son of Dracula
| image = Son of Dracula movie poster.jpg
| caption =Theatrical release poster
| director = Robert Siodmak
| producer = Ford Beebe
| writer = Curtis Siodmak (story) Eric Taylor
| starring =Lon Chaney, Jr. Robert Paige Louise Allbritton Evelyn Ankers
| music = Hans J. Salter
| cinematography =
| editing =
| distributor = Universal Pictures
| released =  
| runtime = 80 minutes
| country = United States
| language = English
| budget =
}}

Son of Dracula is a 1943 American horror film directed by Robert Siodmak – his first film for Universal studios – with a screenplay based on an original story by his brother Curt Siodmak|Curt. The film stars Lon Chaney, Jr. and his frequent co-star Evelyn Ankers. Notably it is the first film where a vampire is actually shown physically transforming into a bat on screen. It is the third in Universal Studios Dracula trilogy, preceded by Dracula (1931 English-language film)|Dracula and Draculas Daughter, though Dracula appeared in subsequent Universal films teamed with other monsters.

==Plot== Count Alucard George Irving). estate "Dark Oaks." Katherine, a woman with a taste for the morbid, has been secretly dating Alucard and eventually marries him, shunning her long-time boyfriend Frank Stanley. Frank confronts the couple and tries to shoot Alucard but the bullets pass through the Counts body and hit Katherine, seemingly killing her.

A shocked Frank runs off to Dr. Brewster, who visits Dark Oaks and is welcomed by Alucard and a living Katherine. The couple instruct him that henceforth they will be devoting their days to scientific research and only welcome visitors at night. Frank goes on to the police and confesses to the murder of Katherine. Brewster tries to convince the Sheriff that he saw Katherine alive and that she would be away all day, but the Sheriff insists on searching Dark Oaks. He finds Katherines dead body and has her transferred to the morgue.

Meanwhile, Hungarian Professor Lazlo arrives at Brewsters house. Brewster has noticed that Alucard is Dracula spelled backwards and Lazlo suspects vampirism. A local boy brought to Brewsters house confirms this suspicion—there are bite marks on his neck. Later, the Count appears to Brewster and Lazlo but is driven away by a cross.

Vampiric Katherine enters Franks cell as a bat and starts his transformation. After he awakens, she explains that she still loves him—she married Alucard (who is really Dracula himself) only to attain immortality and wants to share said immortality with Frank. He is initially repulsed but then yields to her. As she explains that she has already drank some of his blood, she advises him on how to destroy Alucard. He breaks out of prison, seeks out Alucards hiding place and burns his coffin; with no daytime sanctuary, Alucard is destroyed. Brewster, Lazlo, and the Sheriff arrive at the scene, only finding Alucards remains. They then go to Dark Oaks, where they find out that Frank has also set Katherines coffin on fire, destroying her.

==Cast== Count Alucard Larry Talbot The Wolf Man. 
 Count Alucard (Dracula)
* Robert Paige as Frank Stanley
* Louise Allbritton as Katherine Kay Caldwell
* Evelyn Ankers as Claire Caldwell
* Frank Craven as Dr. Harry Brewster
* J. Edward Bromberg as Professor Lazlo
* Adeline De Walt Reynolds as Madame Queen Zimba Patrick Moriarity as Sheriff Dawes
* Etta McDaniel as Sarah, Brewsters Maid  George Irving as Colonel Caldwell

== Themes ==
Son of Dracula dates the original Count Dracula as being destroyed in the 19th century, when the original novel was set.
 House of Frankenstein, which starred John Carradine as the original Count Dracula.  The famous arrival of Draculas coffin by train was reprised in the Abbott and Costello film Abbott and Costello Meet Frankenstein (1948). 

While Brewster and Lazlo speculate that he might be a descendant of the original Dracula, congruent with the films title, Katherine tells Frank specifically that Count Alucard is really Dracula himself. However, throughout the film the vampire is referred to as Alucard.

Both the Universal press book and Realart re-release press book state that SON is NOT a continuation of DRACULA and that Alucard is a descendant.  Chaney is NOT playing the original DRACULA in spite of what some believe.  A Son of Dracula would also be known as Count Dracula too.

This is the first Universal Dracula film to take the Count out of Europe and bring him to America.

== Production == Eric Taylor. 

===Effects=== The Invisible Man. 

==See also==
* Vampire film

== References ==
{{Reflist |refs= 
 
{{cite book
 | last = Smith
 | first = Don G.
 | title = Lon Chaney, Jr.: Horror Film Star, 1906-1973
 | url = http://books.google.com/books?id=HSPwUaOuQNUC&pg=PA89&dq=%22Son+of+Dracula%22+1943&hl=en&sa=X&ei=xgQ0UdCLLuLniwK_5oGoBw&ved=0CC8Q6AEwBA
 | accessdate = March 4, 2013
 | edition = illustrated
 | date = May 1, 2004
 | publisher = McFarland & Company
 | location = Jefferson, North Carolina, USA
 | isbn = 9780786418138
 | page = 89
}}
 

 
{{cite book
 | last1 = Browning
 | first1 = John Edgar
 | last2 = Picart
 | first2 = Caroline Joan (Kay)
 | title = Draculas, Vampires, and Other Undead Forms: Essays on Gender, Race and Culture
 | url = http://books.google.com/books?id=7yHqm9bfwfIC&pg=PA17&dq=%22Son+of+Dracula%22+1943&hl=en&sa=X&ei=xgQ0UdCLLuLniwK_5oGoBw&ved=0CCQQ6AEwAQ
 | accessdate = March 4, 2013
 | edition = illustrated
 | date = April 8, 2009
 | publisher = Scarecrow Press
 | location = Lanham, Maryland, USA
 | isbn = 9780810869233
 | oclc = 371085890
 | page = 17
}}
 

 
{{cite book
 | last = Guiley
 | first = Rosemary
 | title = The Encyclopedia of Vampires, Werewolves and Other Monsters
 | url = http://books.google.com/books?id=5soL2qxSBDgC&pg=PA63&dq=%22Lon+Chaney,+Jr.%22+%22the+wolf+man%22+1941&hl=en&sa=X&ei=lSQ0UZShHo_yiQKnk4GIDQ&ved=0CCEQ6AEwAA
 | accessdate = March 4, 2013
 | year = 2004
 | publisher = Infobase Publishing
 | location = New York City, New York, USA
 | isbn = 9781438130019
 | oclc = 593218217
 | page = 63
}}
 

 
{{cite book
 | last = Dixon
 | first = Wheeler W.
 | title = A History of Horror
 | url = http://books.google.com/books?id=5CtYoSSxomcC&pg=PA34&dq=%22Son+of+Dracula%22+1943+bat-to-man+transformation+Fulton&hl=en&sa=X&ei=bCc0UeyAK4XUiwK81IHYAg&ved=0CCEQ6AEwAA
 | accessdate = March 4, 2013
 | edition = illustrated
 | date = September 1, 2010
 | publisher = Rutgers University Press
 | location = New Brunswick, New Jersey, USA
 | isbn = 9780813547961
 | oclc = 461324157
 | page = 34
}}
 

 
{{cite book
 | last1 = Weaver
 | first1 = Tom
 | last2 = Brunas
 | first2 = Michael
 | last3 = Brunas
 | first3 = John
 | title = Universal Horrors: The Studios Classic Films, 1931-1946
 | url = http://books.google.com/books?id=Wut4jYBtUdsC&pg=PA502&dq=%22House+of+Frankenstein%22+%22John+Carradine%22&hl=en&sa=X&ei=kZs1UffVGIblyQHX2ICQAg&ved=0CCcQ6AEwAQ
 | accessdate = March 5, 2013
 | edition = 2nd
 | year = 2007
 | publisher = McFarland & Company
 | location = Jefferson, North Carolina, USA
 | isbn = 9780786491506
 | oclc = 812193275
 | page = 502
}}
 

 
{{cite book
 | last = Nollen
 | first = Scott Allen
 | title = Abbott and Costello on the Home Front: A Critical Study of the Wartime Films
 | url = http://books.google.com/books?id=rloU-4ntD_EC&pg=PA152&dq=%22Abbott+and+Costello+Meet+Frankenstein%22+Dracula+coffin+train&hl=en&sa=X&ei=0p41UcfaLuPSyAGTwYHYAw&ved=0CCUQ6AEwAQ
 | accessdate = March 5, 2013
 | date = January 1, 2009
 | publisher = McFarland & Company
 | location = Jefferson, North Carolina, USA
 | isbn = 9780786453252
 | oclc = 431511689
 | page = 152
}}
 

 
{{cite book
 | last = Miller
 | first = Ron
 | title = Special Effects: An Introduction to Movie Magic
 | url = http://books.google.com/books?id=JTIMIDNIVg8C&pg=PA25&dq=%22John+P.+Fulton%22+Universal+%22special-effects%22+1933+%22The+Invisible+Man%22&hl=en&sa=X&ei=NKE1Uev4LaXUygHPxIG4AQ&ved=0CCkQ6AEwAg
 | accessdate = March 5, 2013
 | edition = illustrated
 | date = March 1, 2006
 | publisher = Twenty-First Century Books
 | location = Minneapolis, Minnesota, USA
 | isbn = 9780761329183
 | oclc = 60419490
 | page = 25
}}
 

 
{{cite journal
 | author = TV Guide staff
 | year = 1989
 | title = (unknown)
 | journal = TV Guide
 | volume = 37
 | issue = 9-12
 | location = Radnor, Pennsylvania, USA
 | publisher = Triangle Publications
 | issn = 0039-8543
 | accessdate = March 5, 2013
 | url = http://books.google.com/books?id=bt84AAAAIAAJ&q=%22John+P.+Fulton%22+%22Academy+Award%22+1957+%22Ten+Commandments%22+%22Red+Sea%22&dq=%22John+P.+Fulton%22+%22Academy+Award%22+1957+%22Ten+Commandments%22+%22Red+Sea%22&hl=en&sa=X&ei=6aU1UZyOJ8jRyAGWuYDgAg&ved=0CCkQ6AEwAg
}}
  -->

}}

==External links==
*   – The Universal Dracula series
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 