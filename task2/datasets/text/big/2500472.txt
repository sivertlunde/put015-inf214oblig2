Fun with Dick and Jane (1977 film)
{{Infobox film
| name = Fun with Dick and Jane
| image = Fun With Dick And Jane DVD George Segal Jane Fonda.jpg
| image_size = 215px
| alt = 
| caption = DVD cover
| director = Ted Kotcheff
| producer = Peter Bart Max Palevsky
| screenplay = David Giler Jerry Belson Mordecai Richler
| story = Gerald Gaiser
| starring = George Segal Jane Fonda Ernest Gold
| cinematography = Fred J. Koenekamp
| editing = Danford B. Greene
| distributor = Columbia Pictures
| released =  
| runtime = 99 minutes  
| country = United States
| language = English
| budget = United States dollar|USD$3 million
}}
Fun with Dick and Jane is a 1977 American comedy film starring George Segal and Jane Fonda. Directed by Ted Kotcheff, the film is caustically critical of the anarchy of the American way of life.

The character names come from the Dick and Jane series of childrens educational books and the title is taken from the title of one of the books in the series.

==Plot==
 
An upper-middle-class couple, Dick and Jane Harper, hits a hump in their living the good life when the husband loses his job. Falling through the cracks of society in the United States, they then become high-class thieves to get back all they lost.

==Cast==
* George Segal as Dick Harper
* Jane Fonda as Jane Harper
* Ed McMahon as Charlie Blanchard Dick Gautier as Dr. Will
* Allan Miller as Loan company manager
* Hank Garcia as Raoul Esteban
* John Dehner as Janes father Mary Jackson as Janes mother
* Walter Brooke as Mr. Weeks
* Sean Frye as Billy
* Fred Willard as Bob
* Burke Byrnes as Roger Dewayne Jesse as Robber
* Anne Ramsey as Employment applicant Jon Christian Transsexual
* Jay Leno (uncredited) as Carpenter

==Remake== Fun with Dick and Jane was released in 2005, starring Jim Carrey and Téa Leoni.

==References==
 

=== Bibliography ===
*{{cite book
 | last1 = McMahon
 | first1 = Ed
 | last2 = Fisher
 | first2 = David
 | title = For Laughing Out Loud: My Life and Good Times
 | url = http://books.google.com/books?id=iRv_TqNmvgQC&pg=PT249&dq=%22Fun+with+Dick+and+Jane%22+%22George+Segal%22+-%22Jim+Carrey%22&hl=en&sa=X&ei=8UA7UdDeG4KjPeCmgPgE&ved=0CCYQ6AEwAQ
 | accessdate = March 9, 2013
 | date = January 1, 2001
 | publisher = Hachette Digital
 | location = New York City, New York, USA
 | isbn = 9780759520738
}}

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 