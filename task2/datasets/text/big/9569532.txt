The Infernal Cauldron
{{Infobox film
| name           = Le Chaudron infernal
| image          = Le Chaudron infernal.jpg
| image_size     = 
| caption        = Frame from the film
| director       = Georges Méliès
| producer       = Georges Méliès
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Star Film Company
| released       = 1903
| runtime        = 36 meters/116 feet   
| country        = France
| language       = Silent
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}

Le Chaudron infernal, released in Britain as The Infernal Cauldron and in the United States as The Infernal Caldron and the Phantasmal Vapors, is a 1903 French silent film directed by Georges Méliès. It was released by Mélièss Star Film Company and is numbered 499–500 in its catalogues.   

==Plot==
In a Renaissance chamber decorated with devilish faces and a warped coat of arms, a gleeful Satan throws three human victims into a cauldron, which spews out flames. The victims rise from the cauldron as nebulous ghosts, and then turn into fireballs. The fireballs multiply and pursue Satan around the chamber. Finally Satan himself leaps into the infernal cauldron, which gives off a final burst of flame.

==Versions== pirated by negatives of each film he made: one for domestic markets, and one for foreign release.    To produce the two separate negatives, Méliès built a special camera that used two lenses and two reels of film simultaneously. 
 stereo film 3D versions The Oracle of Delphi, at a January 2010 presentation at the Cinémathèque Française. According to the film critic Kristin Thompson, "the effect of 3D was delightful … the films as synchronized by Lobster looked exactly as if Méliès had designed them for 3D."  Bromberg screened both films again—as well as the 1906 Méliès film The Mysterious Retort, similarly prepared for 3D—at a September 2011 presentation at the Academy of Motion Picture Arts and Sciences. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 