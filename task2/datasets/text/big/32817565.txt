Rangle River
 
{{Infobox film
| name           = Rangle River
| image          = Rangrivpos.jpg
| image_size     = 
| caption        = Film poster
| director       = Clarence G. Badger
| producer       =  Charles Chauvel   Elsa Chauvel
| based on    = story by Zane Grey
| narrator       = 
| starring       = Victor Jory   Robert Coote
| music          = 
| cinematography = Errol Hinds
| editing        = Frank Coffey
| studio         = Columbia Pictures National Studios
| distributor = Columbia Pictures (Aust & UK) J.H. Hoffberg (US)
| released       = 19 December 1936 (Australia)  1937 (UK) 1939 (US)
| runtime        = 86 mins
| country        = Australia
| language       = English
| budget         = ₤20,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 176.  
}} western directed by Clarence G. Badger based on a story by Zane Grey.

==Synopsis==
Marion Hastings returns to her father Dans cattle property in western Queensland after being away in Europe for fifteen years. She is treated with hostility by her fathers foreman, Dick Drake, and her fathers neighbour, Don Lawton.

The river on the Hastings property keeps drying up. An English house guest, Reggie Mannister discovers that the river is being dammed by Donald Lawton. Marion goes to investigate as Lawton dynamites his dam. Marion is trapped in the flood. Drake comes her to aid, rescues Marion from drowning and helps defeat Lawton in a whip duel.

Dick and Marion are reunited and walk off into the sunset, with Marion holding the whip, literally.

==Cast==
*Victor Jory as Dick Drake
*Margaret Dare as Marion Hastings
*Robert Coote as Reggie Mannister
*Cecil Perry as Donald Lawton
*George Bryant as Dan Hastings
*Leo Cracknell as Barbwire
*Georgie Stirling as Minna
*Rita Pauncefort as Aunt Abbie
*Stewart McColl as Black

==Production== National Studios, who owned Pagewood Film Studios and had links with National Productions, the company that made The Flying Doctor (1936). 
 Bermagui during White Death Charles and Elsa Chauvel. It features a number of stock characters from Australian films and theatre of the time, such as the "The Squatters Daughter (play)|squatters daughter" and the "English new chum".

The role of Marion Hastings was originally offered to Nancy ONeil, an Australian actor living in England.  The director was imported from Hollywood, as was star Victor Jory. 

The female lead was given to a Sydney girl, Peggy Barnes, who changed her name to Margaret Dare.  She was signed to a three year contract with National Studios but asked to be released from it. 

Although there was some talk the film would be made in Queensland  it was eventually shot on location near Gloucester and in the Burragorang Valley.  

Victor Jory was fined for speeding while driving in Sydney. 

==Reception==
The movie enjoyed a reasonably successful run in Australia. The critic from the Sydney Morning Herald described it as "the best film that has been produced in Australia so far". 

It was released in the UK after some cuts were made by the censor  (notably of the whip fighting scene STUDIO AND SCREEN: Censorship--Document and story--GarboS next Film
The Manchester Guardian (1901-1959)   24 June 1937: 12. ) and was issued in the US in 1939 as Men with Whips.

National Studios were keen to do a sequel. A shooting script was written, Clarence Badger agreed to return and by December 1936 an agreement had almost been formed with Columbia Pictures. Then the government announced that the New South Wales Film Quota Act would be not be enforced and Columbia withdrew. Said Frederick Davies of National Studios: The Burgomeister was rejected by the advisory board, with the consequence that it had to be shelved at a total loss, the public shied away from the business side of Australian motion pictures.  

Robert Coote went to Hollywood after filming and enjoyed a long career there. Margaret Dare also left for Los Angeles but seems to have been less successful. 

Clarence Badger settled in Australia but only made one more feature, That Certain Something (1941).

==References==
 

==External links==
*  in the Internet Movie Database
*  at Australian Screen Online
*  at Oz Movies
* 

 

 
 
 
 
 