Chopper (film)
 
 
 
{{Infobox film
| name            = Chopper
| image           = Choppermovie.jpg
| caption         = Theatrical release poster
| director        = Andrew Dominik
| producer        = Michele Bennett
| screenplay      = Andrew Dominik
| story           = Mark "Chopper" Read David Field
| music           = Mick Harvey
| cinematography  = Geoffrey Hall Kevin Hayward
| editing         = Ken Sallows
| studio          = Australian Film Finance Corporation Mushroom Pictures Pariah Entertainment Group
| distributor     = First Look Pictures
| released        = 3 August 2000
| country         = Australia
| language        = English
| runtime         = 94 minutes
}} autobiographical books David Field. It has a cult following.

==Plot== standover man Mark Brandon "Chopper" Read (Eric Bana) is serving a 16-year sentence for kidnapping a supreme court judge to get his childhood friend, Jimmy Loughnan (Simon Lyndon), out of the notorious H Division of maximum security Pentridge Prison. To become leader of the division, he ignites a power struggle which gains him more enemies than admirers. Eventually, even his gang turn their backs on him and Loughnan stabs him several times in a failed assassination attempt. Chopper voluntarily has his ears cut off by a fellow inmate in order to be transferred out of the H Division; this also gains him recognition in and out of the prison.

  junkie fiancée who is pregnant with another child.

He kills a criminal known as Siam "Sammy the Turk" Ozerkam at a bar, but gets away with it by claiming it was self-defence. He eventually ends up back in prison where he writes a book about his experiences in the Melbourne underworld. The book becomes a best-seller and Chopper becomes a criminal legend.    

The film ends with Chopper in his prison cell in 1992, watching himself being interviewed on television. He is proud of the interview among those watching with him, but when they leave he goes quiet and the film ends with him sitting in his cell alone.

==Cast== Mark "Chopper" Read
* Vince Colosimo as Neville Bartos
* Simon Lyndon as Jimmy Loughnan David Field as Keith George
* Kate Beahan as Tanya Paice
* Dan Wyllie as "Bluey"
* Fletcher Humphrys as "Bucky"
* Robert Rabiah as Nick
* Brian Mannix as Ian
* Serge Liistro as Siam "Sammy the Turk" Ozerkam
* Skye Wansey as Mandy
* Renée Brack as Television Interviewer
* Richard Sutherland as Prison Officer

==Production==
 
The biggest production difficulty was being allowed to use the Pentridge Prison in Coburg, Victoria for the shooting. The prison was being closed down and while the negotiations were underway, the funding for production was delayed. This put off the starting of the shoot.
 paranoid and agoraphobic atmosphere, called "visual overload" by the director Andrew Dominik. This was attained by lighting, choice of film stock used and colours chosen for set decoration. Part one of the production ran from 3 May until 26 May with part two continuing from 28 June until 21 July 2000.
 extras were hired from former inmates and tattoo parlors. Bana spent two days with Chopper to gain an insight into the role he was to play and many of Choppers friends, enemies and old associates were interviewed.

==Reception==

===Reviews===
Chopper was received with positive reviews. Review-based rating site Rotten Tomatoes gave the film a 71% "Fresh" rating.  Roger Ebert of the Chicago Sun-Times gave the film 3 stars out of 4, praising Eric Bana for his performance, saying, "He has a quality no acting school can teach and few actors can match." 
 SBS gave the film four-and-a-half stars out of five, commenting that what director Andrew Dominik "achieved is extraordinary." David Stratton, in the same review, remarked "theres no doubting the intelligence of Andrew Dominiks direction" and declared Eric Banas performance as "astonishing."  

===Reaction from Mark "Chopper" Read=== Full Frontal. Bana spent two days living with Read to help him practise for the role. Read later praised Banas performance on the 20 to 1 episode Great Aussie Films, where Chopper came 17th. Several of Banas meetings with Read can be viewed in the DVD Special Features.

==Awards and nominations==

===Awards won===
*Australian Film Institute (AFI) (2000):
**Best Achievement in Direction: Andrew Dominik
**Best Performance by an Actor in a Leading Role: Eric Bana
**Best Performance by an Actor in a Supporting Role: Simon Lyndon
*IF Awards (2000)
**Best Actor: Eric Bana
**Best Independent New Filmmaker: Andrew Dominik
*Stockholm Film Festival (2000)
**Best Actor: Eric Bana
*Cognac Festival du Film Policier (2001)
**Critics Award: Andrew Dominik
**Grand Prix Award: Andrew Dominik
*Film Critics Circle of Australia (FCAA) (2001):
**Best Male Actor: Eric Bana
**Best Director: Andrew Dominik
**Best Film
**Best Male Supporting Actor: Simon Lyndon

===Awards nominated===
*Australian Film Institute (AFI) (2000):
**Best Achievement in Cinematography: Geoffrey Hall, Kevin Hayward
**Best Achievement in Editing: Ken Sallows
**Best Achievement in Production Design: Paddy Reardon
**Best Achievement in Sound: Frank Lipson, Glenn Newnham, Steve Burgess, John Schiefelbein
**Best Film: Michele Bennett
**Best Original Music Score: Mick Harvey
**Best Screenplay Adapted from Another Source: Andrew Dominik
*Stockholm Film Festival (2000)
**Bronze Horse Award: Andrew Dominik
*British Independent Film Awards (BIFA) (2001):
**Best Foreign Independent Film
*Film Critics Circle of Australia (FCAA) (2001):
**Best Cinematography: Kevin Hayward, Geoffrey Hall
**Best Editing: Ken Sallows
**Best Music Score: Mick Harvey
**Best Screenplay: Andrew Dominik
**Best Female Supporting Actor: Kate Beahan

==Music==
*"Dont Fence Me In" - Frankie Laine Chain
*"Sweet Love" - Renee Geyer
*"Bad Boy for Love" and "Stuck on You" - Rose Tattoo Forever Now" - Cold Chisel The Birthday Party The Saints
*"Ever Lovin Man" - The Loved Ones

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
* 
* 
* .
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 