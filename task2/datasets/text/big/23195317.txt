Tokyo: The Last Megalopolis
 

{{Infobox film
| name           = Tokyo: The Last Megalopolis
| image          = TeitoPoster1988.jpg
| caption        = Theatrical poster
| writer         = Kaizo Hayashi Hiroshi Aramata  (Novel) 
| starring       = Shintaro Katsu Kyūsaku Shimada Mieko Harada Junichi Ishida
| director       = Akio Jissoji
| producer       = Takashige Ichise Akio Jissoji
| cinematography = Masao Nakabori
| music          = Maki Ishii
| editing        = Keniichi Uraoka
| studio         = Exe
| distributor    = Toho
| released       =  
| runtime        = 135 minutes
| country        = Japan
| language       = Japanese
| budget         = JPY|¥800,000,000  (USD|$8,000,000) 
| gross          = ¥1,790,000,000  ($17,900,000) 
}}

  is a   and distributed by Toho Studios.  It is the first cinematic adaptation of the award winning historical fantasy novel Teito Monogatari by Hiroshi Aramata.  The movie was a notable success in Japan and helped propel the careers of some important figures in the Japanese film industry, such as Takashige Ichise (the producer responsible for the contemporary J-Horror boom who financed such films as Ring (film)|Ringu). Patrick Macias. "Tokyo: The Last Megalopolis", Tokyoscope, page 79-80, VIZ Media LLC., November 2001, 

==Synopsis==
The live-action film is an adaptation of the first 1/3rd of the original novel or the first four volumes (out of a total of 12).
 Tsuchimikado Family monoimi ceremony In a Year of the Pig.

The narrative moves to 1923, Tokyo. Kato retreats to Dalian, China and he and his followers use magic to cause artificial earthquake waves that are amplified to Japan. Kato returns to Tokyo to awaken Masakados spirit by himself, but is interrupted by Koda Rohan and Junichi Narutaki, who use Kimon Tonkou magic against him. Kato fails to awaken Masakado, but manages to stimulate the Great Kanto Earthquake.
 Noritsugu Hayakawa Tokyo Metro Ginza Line. Hayakawas construction workers run into Katos shikigami provoking Terada to seek out the aid of Dr. Makoto Nishimura to use his creation Gakutensoku to finish construction for them. Masakado summons Keiko Mekata, a miko, to defend his grave from Kato. Keiko joins forces with feng shui master Shigemaru Kuroda, who discovers the location of Katos hideout. While Kuroda fights an Asura statue guarding the place, Keiko rushes to stop Kato, but Kato summons his gohō dōji to fend her off. Kato attempts to awaken Masakado through Yukaris child, Yukiko, but even this is unsuccessful. Keiko explains to Kato that Yukiko is not his child, but rather the result of an incestuous union between Yukari and her brother Yoichiro making her uncontrollable by Kato. Gakutensoku self-destructs, cutting off the spiritual energy veins connected to Katos temple. Kato tries to use onmyodo magic one last time to stimulate an earthquake, but this is insufficient and he is severely wounded from the effort. Though his plans are foiled, Kato kidnaps Keiko and takes her with him to Manchuria. The film ends with the Tatsumiya Family hoping for Keikos return, and Kyoka Izumi predicting Katos return.

==Cast==
* , an ambitious enterprise calling upon the minds of specialists from a variety of fields, with the ultimate goal of making Tokyo the most powerful city in East Asia.
*  wielding the power of several long dead mystics (such as Abe no Seimei) who wants to destroy Tokyo and cripple the Japanese Empire in order to fulfill a 2000 year old curse.
*  summoned by the spirit of Taira no Masakado to defend the Tatsumiya Family.
* .  He is the direct descendent of Taira no Masakado, the husband of Keiko Tatsumiya, the brother of Yukari Tatsumiya and the secret father of Yukiko Tatsumiya.
*Shiro Sano as Junichi Narutaki: The close friend of Yoichiro Tatsumiya, he is in love with Yoichiros sister, Yukari.  He participates in the defense of the city by fighting with the Tsuchimikado Clan against Kato as well as joining Koda Rohan in the defense of Masakados grave.
*  whose work contributed to the reformation of modern Japanese literature   and who also was a renowned scholar of the supernatural.   Determined to stop Kato and protect Yukari, he joins the Tsuchimikado Clan as a student of the onmyoji.  After the death of Yasumasa Hirai, he spends several years mastering the secret mystical techniques for the purpose of defending Tokyo.  During the Year of the Boar, he attempts to halt Katos advance to Masakados grave.
*  and a psychic.  Due to her heritage and supernatural abilities, Kato abducts her and uses her as a medium to communicate with the spirit of Masakado.  She gives birth to Yukiko Tatsumiya, who is believed to be the result of Kato using his magic on her, but revealed later to be the consequence of a secret incestuous encounter between her and her brother.
* .  In the story, he uses his creation to help finish the construction of Japans first subway tunnel.  In the film, Makoto is portrayed by his real-life son, Ko Nishimura.
*Ken Teraizumi as Torahiko Terada: The famous physicist and essayist renowned for his eccentric ideas, wide range of studies and considered the father of "nonlinear physics" in Japan.   At the Tokyo Improvement Project committee, he suggests the radical concept of creating an underground city as a backup plan in the event earthquakes should strike.  He is laughed off by the other members. Later however, Noritsugu Hayakawa hears of his reputation and appoints him as one of the heads of the construction of Japans first subway system. Tsuchimikado Clan and the direct descendant of the legendary Abe no Seimei.  For the first part of the story, he serves as the primary foil to Yasunori Kato, having all his knowledge and understanding of his magics.  After the death of the Meiji Emperor, he performs a fatal act of divination to predict year of Tokyos destruction.
*  expert, who in investigating strange spiritual disturbances beneath Tokyo.  He serves as Keikos assistant in the final battle against Kato.
*  system.  During construction of the railway, his engineers run afoul of shikigami set by Kato.  Faced with this obstruction, Hayakawa must seek out aid to see his project go through to completion.
*Katsuo Nakamura as Ogai Mori:  Legendary writer and surgeon in the Japanese army.  In the story, he performs an unsuccessful abortion on Yukari Tatsumiya after she has supposedly been impregnated with Katos child.
*Tamasaburo Bando V as Izumi Kyoka: Mysterious writer and fortune teller with cryptic information about Keikos destiny.  Legendary Kabuki performer Bando V is a noted Izumi Kyoka scholar who has produced many plays based on his works. 

Other cast members include Seiko Ito as Wajiro Kon, Hideji Ōtaki as Oda Kanno, Hisashi Igawa as Ryokichi Tagami and Ai Yasunaga as Azusa Nishimura. Hiroshi Aramata does a cameo as a brassery client.

==Production==
 " Kato summons in the film were influenced from classic depictions of oni, such as the prints of Katsushika Hokusai]]

  dismembers Katos hand was inspired from the kabuki play "Modoribashi", about Watanabe no Tsuna and his encounter with the oni Ibaraki ]]

The film began going into production in 1987.  At the time, it was one of the most expensive fantasy productions that Japanese studios had ever undertaken, with a budget of around 1,000,000,000 yen.  An open set of 150 meters in length of the early Showa era Ginza district was created just for the film and cost around 300,000,000 yen.  This open set alone also featured about 3000 Extra (actor)|extras.

H. R. Giger was commissioned to design creatures for the movie.   Originally, he showed interest in working directly on set, however his schedule would not permit it.   His main contribution was the conceptual art for the gohō dōji.

The movie was also one of the first Japanese productions to employ Sony HDVS technology for filming.  Approximately six minutes of the final movie was filmed using this equipment.

==Music==
In addition to an original score, composer Maki Ishii remixed a few pieces of classical music for use in the film.  These include:
*The prelude of Wagners Das Rheingold (used for the opening theme) Symphony No. 2 by Gustav Mahler (used for Katos passage over the Sumida River) Johann Strauss (used for the ending credits)

==Reception==
When first released theatrically in Japan, the film was met with positive critical reception and went on to become a commercial success. Harper, Jim.  Flowers from Hell: The Modern Japanese Horror Film  Noir Publishing. (ISBN 0953656470)   Peer Magazine, one of Japans leading cinema publications, went so far as to praise the movie as the "best Japanese science fiction production of all time".   The movie had an annual revenue of 1.79 billion yen making it the third highest grossing Japanese produced film of that year, John A. Lent. The Asian Film Industry, pg. 41, Christopher Helm Publishers Ltd, February 22, 1990  and the eighth highest grossing film in Japan overall. 

Although the film was never released theatrically overseas, critical reception of the VHS and DVD release of the film in the UK was also generally positive.  Ian Shutter of the website videovista.net gave the film an 8/10 describing it as a "surreal yet always fascinating gothic urban nightmare" with "a blend of urban historical and fantasy horror centered on the great disaster of 1923, which plays like Capra meets Argento, with an oriental twist.".   Lee Broughton of the website DVD Savant rated the film as "Excellent", claiming it was "a highly original mystical epic" containing "great characters that we really come to care about and take an interest in as they weave in and out of each others lives.", and even compared its ambition to Terry Gilliams Brazil (1985 film)|Brazil.   French website DeVilDead.com pointed out that the films plot, being very compressed and very ambitious, might be "indigestable" for viewers not already familiar with the original novel.  However their review was also positive, describing it as a "visually elegant film" containing a "rich history", with Kyusaku Shimada "incredible" in his role as Yasunori Kato. 

Reception to the North American release, by contrast, has been mixed.  Some of the negative reception was the result of bad timing as the movie was exported to North America years after the anime  ,   Anthony Romero, Toho Kingdom, September 22, 2006  but didnt note any other comparisons.  Anime fans tended to dismiss Tokyo: The Last Megalopolis as a mediocre live action adaptation of Doomed Megalopolis    Judge Joel Pierce, DVD Verdict, April 16, 2004  instead of correctly noting that the live action film was created first and its success paved the way for the anime.

Practically all negative reviews tended to focus on the highly convoluted narrative (a consequence of adapting four books into a 2 hour timespan) as the source of the movies troubles.  Anthony Romero of Toho Kingdom described the film as having “production values being noticeably high for a 1980s Japanese film” but “simply tries to cover far too much ground in too short a time”.   Japanese enthusiast website GenjiPress chided the film as "absolutely ridiculous from beginning to end" with a plot that was amazingly "confusing".   Some reviewers also expressed disappointment with the relative lack of horror elements in the story, as the English promotional material misleadingly advertised the film as an "...experience in supernatural terror" instead of a complicated historical fantasy epic. 

Positive reviews tended to focus on aspects apart from the narrative, especially noting the detailed sets, costumes, high production values, and dramatic performances as the films main strengths.  The website Sarudama praised the movie calling it "incredibly ambitious and well-cast" with "superb" scenery and acting.   Author Patrick Macias in his book Tokyoscope also gave the movie a positive review, describing it as "overcooked", but "far from a bust".   Jim Harper in his book Flowers from Hell: The Modern Japanese Horror Film concedes that, had it not been for some pacing problems and a "ponderous plot", the movie could have been a "bona fide cult classic". 

==Accolades==
;Wins
* 18th  , 1988

;Nominations Japanese Academy Awards: Award of the Japanese Academy, Best Editing, Keiichi Uraoka; Best Art Direction, Takeo Kimura, Noriyoshi Ikeya; 1989 

==Legacy==
  as Yasunori Kato]]
 OVA remake; two direct to video spinoff titles Teito Monogatari Gaiden (1995), Sim-Feng Shui (1997), and a theatrical spinoff The Great Yokai War (2005).  Its success also prompted author Hiroshi Aramata to continue the literary franchise, which now consists of 6 spinoff series in addition to the main novel.  The movie was also the most financially successful production for avant garde director Akio Jissoji and marked a revival of his career in the mainstream Japanese film industry.   Independent film director Go Shibata has cited the movie as an influence in his work, such as his 2009 film Doman Seman. 

 Vega of the Street Fighter video game series.  . Retrieved on 2012-8-07.   

==Home Releases==
In Japan, the film is available in both VHS and DVD editions.  In 1995, Manga Live released a VHS edition of the film in the UK which was edited, as well as dubbed.  In 1998, ADV Films released a translated VHS copy of the film in the North American market.  In 2003, ADV Films released a proper subtitled DVD edition of the film to the North American market.

==See also==
  film dealing with some of the same subject matter.  The novels the respective films were based on were released only a few years apart and thus are considered part of the same "boom". 

==References==
 

==Sources==
*  
*  

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 