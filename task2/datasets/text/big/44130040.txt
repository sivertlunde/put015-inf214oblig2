Irattimadhuram
{{Infobox film 
| name           = Irattimadhuram
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       =
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi
| starring       = Prem Nazir Krishnachandran K. R. Vijaya Master Rajakumaran Thampi Shyam
| cinematography = C Ramachandra Menon
| editing        = K Narayanan
| studio         = Hemnag Films
| distributor    = Hemnag Films
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by Sreekumaran Thampi. The film stars Prem Nazir, Krishnachandran, K. R. Vijaya and Master Rajakumaran Thampi in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Prem Nazir as Achuthan Nair
*Krishnachandran as Ramu
*K. R. Vijaya as Madhavikutty
*Master Rajakumaran Thampi as Ravikuttan
*Shanavas as Surendran Shivaji as Balan
*Sumalatha as Sangeetha
*Sukumari as Kalyaniyamma
*Adoor Bhasi as Thorappan Panikkar
*Jagathy Sreekumar as Unnikrishnan
*Balan K Nair as K.B. Menon
*P. K. Abraham as Vakkel Mahadevan
*Vijayalakshmi as Mrs Panikkar
*Guddi Maruti as Omana

==Soundtrack== Shyam and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme amme amme Ennaanente Kalyanam || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Ithiri paattunden nenchil || K. J. Yesudas, Sujatha Mohan || Sreekumaran Thampi || 
|-
| 3 || Madhuram Madhuram Irattimadhuram || P Jayachandran, Vani Jairam, Chorus || Sreekumaran Thampi || 
|-
| 4 || Onnalla randalla moonnalla nangal || Unni Menon, Chorus, Jolly Abraham || Sreekumaran Thampi || 
|-
| 5 || Oru kudukka Ponnutharaam  || P Susheela, Vani Jairam || Sreekumaran Thampi || 
|-
| 6 || Vandi Vandi Vandi Ithu Valiya || P Jayachandran, Sujatha Mohan, Vani Jairam, Jolly Abraham || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 