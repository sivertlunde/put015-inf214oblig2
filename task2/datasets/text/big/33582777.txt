Terror of the Bloodhunters
{{Infobox film
| name           = Terror of the Bloodhunters
| image          = Terror_of_the_bloodhunters.jpg
| image_size     = 
| caption        = Theatrical poster.
| director       = Jerry Warren
| producer       = Jerry Warren (producer)
| writer         = Jerry Warren
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Bill William
| editing        = Jerry Warren
| studio         = 
| distributor    = 
| released       = 1962
| runtime        = 
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Terror of the Bloodhunters is a 1962 American film directed by Jerry Warren.

== Plot summary ==
An escaped prisoner flees to the South American jungle, where he must survive not only wild animals and disease, but a ferocious Indian tribe.

Dir
== Cast ==
*Robert Clarke as Steve Duval
*Dorothy Haney as Marlene
*Robert Christopher as Whorf
*William White as Dione
*Steve Conte as Cabot
*Niles Andrus as Commandant
*Herbert Clarke as Muller
*Darrold Westbrooke as Jacobe
*Mike Concannon as Lt. Vaardo
*Charles Niles Sr. as Estes
*Alberto Soria as Officer

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 

 