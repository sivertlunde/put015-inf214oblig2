More (1969 film)
 
{{Infobox film
| name           = More
| image          = More (film).jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Barbet Schroeder
| producer       = Barbet Schroeder
| writer         = Barbet Schroeder Paul Gégauff
| starring       = Mimsy Farmer Klaus Grünberg
| music          = Pink Floyd
| cinematography = Néstor Almendros
| editing        = Denise de Casabianca Rita Roland
| distributor    = Jet Films
| released       =  
| runtime        = 110 minutes
| country        = West Germany France Luxembourg English
| budget         =
| gross          =
}}
 addiction on the island of Ibiza. It features a soundtrack written and performed by Pink Floyd, released as the album Soundtrack from the Film More. The film was selected to be screened in the Cannes Classics section of the 2015 Cannes Film Festival.   

==Synopsis==
A German student, Stefan, who has finished his mathematics studies, decides to have an adventure, rid himself of his inhibitions  and discard his personal commitments. After hitch-hiking to Paris, he makes a friend playing cards in a bar and they decide to commit a burglary to get some money. He meets a free-spirited American girl called Estelle and follows her to Ibiza. He discovers Estelle is involved with an ex-Nazi German man called Dr. Wolf. Stefan saves Estelle from Dr. Wolf only to find she does not really want to be saved, and she introduces him to heroin (referred to by the old street name, "horse") which she has stolen from Dr. Wolf. Stefan is initially against Estelle using heroin, but having used it previously, she persuades him to try it. Soon Stefan and Estelle are both heavily addicted to heroin. They try to break the addiction using LSD and initially manage to stay clean. However, after a while theyre both using heroin again. Unable to break free of the addiction, it quickly spirals out of control leading to a tragic end for Stefan.

==Production==
The French film censorship board in 1969 insisted that some of the dialog be censored around the 81 minute mark before the film could be released. In the film, as the couple mix up a hallucinogenic concoction in the kitchen, the ingredients "benzedrine" and "banana peel" are deleted from the audio track. On the DVD the words have been re-added as subtitles.

Most of the movie was shot on the island of Ibiza. The castle of Ibiza Town|Ibiza, which dominates the harbour and the town, is the scene for the final act. The location of Stefans death, a tunnel near the castle, has since become a place of pilgrimage for addicts. 

==Soundtrack==
 
 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 