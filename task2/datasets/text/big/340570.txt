Simon, Simon
 
{{Infobox film
| name           = Simon, Simon
| image          =
| image_size     = 
| caption        =
| director       = Graham Stark
| producer       = Peter Shillingford
| writer         = Graham Stark and Dave Freeman
| narrator       =
| starring       = Graham Stark Norman Rossington John Junkin Julia Foster
| music          = Denis King
| cinematography = Harvey Harrison
| editing        = Bunny Warren
| distributor    = Digital Classics DVD
| released       = 1970
| runtime        = 32 min.
| country        = U.K. English
| budget         =
| preceded_by    =
| followed_by    =
}} 1970 Sound Effect comedy short film directed by Graham Stark.

==Synopsis==
Two handymen, (Graham Stark and John Junkin) cause chaos on a new crane while haphazardly trying to accomplish jobs for their ever more frustrated boss. This silent comedy features a host of cameos from the likes of Peter Sellers, Michael Caine, Bob Monkhouse, Eric Morecambe, Ernie Wise and Tony Blackburn.

==Cast==
*Graham Stark
*John Junkin
*Julia Foster
*Norman Rossington
*Paul Whitsun-Jones
*Audrey Nicholson
*Kenneth Earle
*Tommy Godfrey
 Pete Murray, Peter Sellers, Bernie Winters, and Ernie Wise

==External links==
* 

 

 
 
 
 
 
 
 


 
 