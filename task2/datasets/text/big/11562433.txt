Colossal Youth (film)
{{Infobox film
| name           = Colossal Youth
| caption        = 
| image	=	Colossal Youth FilmPoster.jpeg
| director       = Pedro Costa
| producer       = Francisco Villa-Lobos
| writer         = Pedro Costa
| starring       =  
| music          = Nuno Carvalho
| cinematography =  
| editing        = Pedro Marques 
| distributor    = Memento Films
| released       =  
| runtime        = 155 minutes
| country        = Portugal
| language       =  
| budget         = 
| gross          = 
}}
Colossal Youth ( , literally "Youth on the March") is a 2006 docufiction feature film directed by Portuguese director Pedro Costa. The film was shot on DV in long, static takes and mixes documentary and fiction storytelling. The third feature by Costa set in Lisbons Fontainhas neighborhood (after In Vandas Room and Ossos|Bones), Colossal Youth is a meditation on the aftermath of the Carnation Revolution and its consequences for Portugals poverty-stricken Cape Verdean immigrants. It was part of the Official Competition at the 2006 Cannes Film Festival.   

==Plot==
After the Portuguese government demolishes his slum and relocates him to a housing project on the outskirts of Lisbon, 75-year-old Cape Verde immigrant Ventura wanders between his new and old homes, reconnecting with people from his past.

==See also==
* Docufiction
* List of docufiction films
* Ethnofiction

==References==
 

==External links==
*  at Memento Films International
* 
* 
* 
*  at GreenCine Daily
*  
*  

 
 
 
 
 
 
 
 
 
 
 