Best Friend Forgotten
 
{{Infobox film
| name           = Best Friend Forgotten
| image          = movie_poster_best-friend-forgotten.jpg
| caption        = 
| director       = Julie Lofton
| producer       = Julie Lofton
| writer         = Julie Lofton
| narrator       = David Duchovny
| starring       = David Duchovny Clover, the Dog Oreo, the Cat
| music          = Matt Mariano Keith Walker J.B Letchinger
| editing        = Jan Sutcliff
| distributor    = PBS
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
}} pet overpopulation. The documentary, hosted by David Duchovny (Californication (TV series)|Californication, X-files), tells the stories of Oreo the cat and Clover the dog as they face the harsh realities of pet overpopulation. Viewers get a thoughtful and balanced look at the controversial practice of euthanasia and the alternative no-kill movement. Candid interviews reveal common myths about spaying and neutering, and leaders from government to animal rights groups discuss the plague of pet overpopulation and the impact on our society.

==Creators== TV and Documentary from the Humane Society of the United States (HSUS), the largest animal protection organization in the US.

Following her success with the film, Ms. Lofton was recruited by HSUS to spearhead the creation of their first TV/Film division: Animal Content in Entertainment (ACE), whose mission is to develop and support Animal content in television and film.  Each year ACE awards production grants to professional and student documentary filmmakers who highlight animal rights issues in short and feature-length films. {{cite web
|url= http://www.afi.com/silverdocs/2006/confbio2.aspx
|title= SILVERDOCS International Documentary Conference
|publisher= American Film Institute, 2006
|archiveurl= http://www.webcitation.org/5twqBKo2a
|archivedate=2010-11-02}}  {{cite web
|url= http://www.humanesociety.org/about/departments/hollywood/ace/about_ace.html
|title= HSUSs Animal Content in Entertainment (ACE) program supports animal protection issues in entertainment media
|publisher= HSUS, October 9, 2009}} 

==References==
 

==External links==
 
*  
*  
*   OBryanville Animal Rescue
* {{cite web
|url= http://www.animalsheltering.org/publications/magazine/back_issues/asm_jul_aug_2004.pdf
|title= The Woman Behind the Camera 
|publisher= Animal Sheltering Magazine, 2004}} 
* {{cite web
|url= http://www.animalsheltering.org/publications/magazine/back_issues/asm_nov_dec_2003_ed.pdf
|title= Filmmaker Asks:"Do You Know What Pet Overpopulation Is?"
|publisher= Animal Sheltering Magazine, 2003}}

 
 
 
 
 
 
 
 
 


 