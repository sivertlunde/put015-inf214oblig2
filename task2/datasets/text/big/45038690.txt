Aparadhi (1976 film)
{{Infobox film 
| name           = Aparadhi
| image          =  
| caption        = 
| director       = Y. R. Swamy
| producer       = S Heerabai
| writer         = Om Shakthi Creations
| screenplay     = Y. R. Swamy Balakrishna Narasimharaju Narasimharaju
| music          = Chellapilla Satyam
| cinematography = R Madhusudan
| editing        = P Bhakthavathsalam
| studio         = Om Shakthi Creations
| distributor    = Om Shakthi Creations
| released       =  
| country        = India Kannada
}}
 1976 Cinema Indian Kannada Kannada film, Balakrishna and Narasimharaju in lead roles. The film had musical score by Chellapilla Satyam.  

==Cast==
 
*Srinath
*Aarathi Balakrishna
*Narasimharaju Narasimharaju
*Thoogudeepa Srinivas
*Prabhakar
*Jr Shetty
*N. S. Rao
*Sharapanjara Iyanger
*Thyagaraja Urs
*Srinivasa Murthy
*A. Padmanabha Rao
*Kannada Raju
*Sunitha
*Mamatha Shenoy
*B. Jayashree
*Shanthi
*Vajramuni in Guest Appearance
*Lokanath in Guest Appearance
 

==Soundtrack==
The music was composed by Chellapilla Satyam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || Chi. Udaya Shankar || 04.24
|- Susheela || Chi. Udaya Shankar || 03.21
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 


 