Habit (1997 film)
{{Infobox film
| name           = Habit
| image          = 
| alt            = 
| caption        = 
| director       = Larry Fessenden
| producer       = Dayton Taylor Robin OHara
| writer         = Larry Fessenden
| starring       = Larry Fessenden
| music          = Geoffrey Kidde
| cinematography = Frank G. DeMarco
| editing        = Larry Fessenden
| studio         = Glass Eye Pix
| distributor    = Glass Eye Pix
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Habit is a 1997 vampire horror film starring Larry Fessenden, who also wrote and directed the film. It received rave reviews at the Chicago and Los Angeles International Film Festivals.  It is a remake of Fessendens 1985 film of the same title.

==Plot==
Sam is a self-destructive, vaguely artistic New York bohemian who has recently lost his father and his long-time girlfriend. At a Halloween party he meets a mysterious, beautiful, androgynous woman named Anna. He embarks on a kinky, sex-charged relationship with her; but soon he suffers from a mysterious illness, and eventually comes to believe that Anna is a vampire.

==Awards==
{| class="wikitable"
|-
! Award !! Category !! Nominee !! Result
|- Austin Film Festival Feature Film Award Larry Fessenden
| 
|- Independent Spirit Awards  Producers Award Robin OHara
| 
|- Best Cinematography Frank G. DeMarco
| 
|- Best Director Larry Fessenden
| 
|- Someone to Watch Award Larry Fessenden
| 
|- Williamsburg Brooklyn Film Festival  Feature Film Larry Fessenden
| 
|- Best Actor Larry Fessenden
| 
|- Best Editing Larry Fessenden
| 
|}

==See also==
*Vampire film

==External links==
* 

==References==
 

 
 
 
 
 


 