Hoo Anthiya Uhoo Anthiya
{{Infobox film name           = Hoo Anthiya Uhoo Anthiya image          = image_size     = caption        = director       = K. Praveen Kumar producer       = Usha Sandeep Kumar writer         =  narrator       = starring       = Ramesh Aravind Anu Prabhakar Isha Koppikar music          = Karthik Raja cinematography =  editing        =  studio         =  released       =   runtime        =  country        = India language  Kannada
|budget         =
}}
 Kannada romantic drama film directed by K. Praveen Kumar and produced by N. Bharathi Devi. The film has cast comprising Ramesh Aravind, Isha Koppikar and Anu Prabhakar in the lead roles. 

The film released on 13 October 2001 to generally positive reviews from critics who lauded the lead actors performance and the musical score by Karthik Raja, making his debut in Kannada cinema. 

==Cast==
* Ramesh Aravind 
* Isha Koppikar
* Anu Prabhakar

==Soundtrack==
{{Infobox album  
| Name        = Hoo Anthiya Uhoo Anthiya
| Type        = Soundtrack
| Artist      = Karthik Raja
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Alt         = 
| Released    = 
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = 
}}

The music of the film was composed by Karthik Raja.  Bollywood playback singer Sadhana Sargam made her first Kannada language recording with this soundtrack.

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Chanda Ee Chanda
| lyrics1 	= K. Kalyan
| extra1        = Madhu Balakrishnan
| length1       = 
| title2        = Minchulli Kenne 
| lyrics2 	= Prakash
| extra2        = Mano (singer)|Mano, Febi Mani
| length2       = 
| title3        = Chandada Chandani
| lyrics3       = Doddarangegowda Sujatha
| length3       = 
| title4        = Baana Kolminchu Hariharan
| lyrics4 	= Doddarangegowda
| length4       = 
| title5        = Kanasugala
| extra5        = S. P. Balasubrahmanyam
| lyrics5       = M. N. Vyasa Rao
| length5       =
| title6        = Belli Chandiranu
| extra6        = Madhu Balakrishnan, Bhavatharini
| lyrics6       = Doddarangegowda
| length6       = 
| title7        = Chanda Ee Chanda (female)
| extra7        = Sadhana Sargam
| lyrics7       = K. Kalyan
| length7       =
}}

==References==
 

==External source==
*  

 
 
 
 
 


 