Song of the Plough
{{Infobox film
| name = Song of the Plough
| image =
| image_size =
| caption = John Baxter
| producer = Ivar Campbell 
| writer = Reginald Pound
| starring = Stewart Rome   Rosalinde Fuller   Allan Jeayes   Hay Petrie
| music = Colin Wark
| cinematography = George Stretton
| editing = David Lean Sound City
| distributor = Metro-Goldwyn-Mayer 
| released = December 1933
| runtime = 68 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} John Baxter and starring Stewart Rome, Rosalinde Fuller and Allan Jeayes. An English farmer is saved from financial ruin when his dog wins at a sheepdog trials. It was later re-released with the alternative title Country Fair.
 distribution by location shooting on a Sussex farm. It received a poor review from The Observer critic C. A. Lejeune, but she was forced to withdraw this following a large number of letters in support of the film. The film proved unexpectedly popular when it was released. 

==Cast==
* Stewart Rome as Farmer Freeland 
* Rosalinde Fuller as Miss Freeland 
* Allan Jeayes as Joe Saxby  
* Hay Petrie as Farmhand  
* Kenneth Kove as Archie  
* Jack Livesey as Squires Son  
* Edgar Driver as Barber  
* James Harcourt as Doctor 
* Freddie Watts as Bandsman 
* Albert Richardson as Singer

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The British of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 