Chernobyl: The Final Warning

{{Infobox Film| name           = Chernobyl:  The Final Warning
| director       = Anthony Page
| writer         = Robert Peter Gale Thomas Hauser Ernest Kinoy
| starring       = Jon Voight Jason Robards Sammi Davis
| released       = April 22, 1991
| runtime        = 95 minutes
| country        = United States
| language       = English
}}
Chernobyl:  The Final Warning is a 1991 made for television movie. The film chronicles the Chernobyl disaster.  , retrieved 4 Dec 2012 

==Plot==
Based on a true account of events, the plot interweaves the stories of a fireman at the nuclear power plant, his pregnant wife, the government officials whose policies helped and hindered rescue efforts and Americas Robert Gale, M.D.|Dr. Robert Gale (Jon Voight), who led the international medical team that helped treat survivors of the disaster.  Robert Gale published the original account of his experiences under the same title in 1988 with Thomas Hauser;     it was adapted by Ernest Kinoy for the screenplay. 

==Cast of characters==
 
*Jon Voight as Robert Gale, M.D.|Dr. Robert Gale
*Jason Robards as Dr. Armand Hammer
*Sammi Davis as Yelena Mashenko
*Annette Crosbie as Dr. Galina Petrovna
*Ian McDiarmid as Dr. Vatisenko
*Vincent Riotta as Valery Mashenko
*Steven Hartley as Aleksandr Mashenko
*Jim Ishida as Dr. Terasaki
*Alex Norton as Dr. Andreyev
*Trevor Cooper as Feodor Lashelya
*Jack Klaff as Dr. Pieter Claasen Chris Walker as Grisha
*Lorcan Cranitch as Chernov
*Yuri Petrov as Viktor Vasilichyov
*Karen Meagher as Anna
*Caroline Milmoe as Sonya
*Debora Weston as Tamar Gale
*Jonathan Hachett as George Castle
*Keith Edwards as Champlin
*Vladimir Troshin as Mikhail Gorbachev
*Vadim Ledogorov as Leonid Scherchenko
*Nicholas Locker as Dr. Gales Son
*Shir Gale as Dr. Gales Daughter
 

==External links==
*  

==References==
 

 
 
 
 

 