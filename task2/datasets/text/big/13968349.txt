Tupac: Assassination
{{Infobox Film
| name           = Tupac: Assassination
| image          = 
| caption        = Tupac Assassination DVD Cover
| director       = Richard Bond
| producer       = Frank Alexander and Richard Bond
| writer         = R.J. Bond
| starring       = Tupac Shakur
| music          = Olivia 
| cinematography = Ric Hine
| editing        = Richard Bond
| distributor    = New Video
| released       =  
| runtime        = 88 minutes
| country        = US
| language       = English
| budget         = 
}}

Tupac Assassination: Conspiracy or Revenge is a documentary film about the unsolved murder of rapper Tupac Shakur produced by Frank Alexander, a Shakur bodyguard who was with the rapper at the time of the shooting, produced and directed by Richard Bond. The film was released October 23, 2007 on DVD. 

==Storyline==
The film is a series of interviews with key players and witnesses, as well as what the producer told Rolling Stone magazine were experts in the fields of law enforcement and rap. 

 .

According to former bodyguards who heard this altercation between Tupac and Suge, they didnt realize at the time what Reggie  was planning even when he was demanding that all bodyguards leave their weapons, now reassigning normal Tupac bodyguards elsewhere (leaving Tupac with only Frank) and taking their communication devices, although this is contradicted by bodyguard Frank Alexander’s previous statements that he did have his gun in his car.

Bodyguard Michael Moore refused to give up his weapon and said on film that it was Kevin Hackie (former Compton PD) who had his cell that night. However, in Frank’s book Got Your Back, Frank said the bodyguards were told not to have their guns only at Club 662 in fear the authorities may shut the club down, but was told they could have their guns in their hotel rooms or cars.

Frank said in an interview with HitEmUp.com in January, 2002, “We were told to either leave them in our room or leave them in our car. And me, knowing I was going to the club, stuck my gun in my car up under the seat cause thats where I was gonna go and park my car in the front.”

“I think they removed me   because I wouldnt let them take away my weapon,” Moore said about why on the day of Tupacs murder he was reassigned to Club 662. “I told Tupac…and walked him to his room. I didnt want to leave him (I was the only one with a weapon) and at first I was going to stay, but the club kept calling me. I said, its 8 p.m. and Im not due until 11 p.m. dont call me no more. Then Reggie   called, so I went.”

In the film, Michael Moore expressed his frustration at the time, saying, “Why would I leave my weapon and Im guarding the biggest rapper in the world. I said, Im not taking my weapon away for no one!”

Shortly after bodyguard Moore left Tupac in the Vegas hotel, Tupac got into a car driven by Suge Knight (which they said he never does) and while waiting at a light (boxed in at the left by Trevon Lane, boxed in the back by Franks car and to the right by the killers car) the killer(s) shot him .

==Cast==
The cast includes Cathy Scott, author of The Killing of Tupac Shakur,  Mopreme Shakur, Tupacs stepbrother, bodyguard Michael Moore, Brent Becker, a Las Vegas police detective, and Donald Erath Jr., a Los Angeles Police Department investigator.

== Post release ==

===Press screening controversy===
*Even before the film had been released, at the press screening of the film, breaking news sparked regarding Death Row and the death of Tupac Shakur. Former bodyguard Kevin Hackie, admitted while working at Death Row Records prior to the murder of Tupac that he was an FBI informant and even went further and stated "an arrest will be made in this case", which has yet to happen.

*Kevin Hackie issued a challenge to Reggie Wright, he set up Tupacs death.

*Kevin Hackie has offered Wright $100,000 to take a lie detector test to prove he had nothing to do with Shakur’s still unsolved murder. Due to high tensions rising Wright in an unwilling manner agreed to the test.

*Wright, who has been investigated by the FBI and officially cleared of any wrongdoing, has accepted the challenge, telling AllHipHop.com:

“The day Kevin Hackie puts up the money to pay for a reputable board-licensed polygrapher to conduct a lie detector test for him and I, people will find out who the liar is.” Wright also told AllHipHop.com in a statement, “I had nothing to do with Pacs death.”

A Reggie Wright also told AllHipHop.com, “The reason Pac is dead is because Frank Alexander, the guy who wrote and produced this new DVD failed to bodyguard Pac properly on the night he was shot.” “Kevin   and Frank   ought to be ashamed of themselves. The truth is these guys had almost nothing to do with Pacs life, but they never cease to come up with new ways to exploit his death for profit or fame.” 

===Criticism===
 Got Your Before I Wake.... Alexander said in his book that “I personally don’t believe Suge put the hit on pacs baws”, and that the reason he didn’t have his gun was because he left it in his car, rather than being ordered not to have it. This misunderstanding stems from the lack of clarity of Alexander.

Both are true; he was told not to carry a weapon, but like Michael Moore, he intended to disregard that order and keep his weapon within feet of him (his car was usually parked next to the door) while working at 662 after the fight. Alexander left the weapon in the car, which was parked on the other side of the Luxor casino- a 45-minute round trip hike. Because of the Anderson altercation, the crew was in a hurry to leave and Alexander was never able to get back to his car.

===Legal issues surrounding distribution===

In early 2008, the producers sued the label Eyecon Enterprises for non-payment of royalties. Eyecon had secured $75,000 in advances from Liberation Entertainment before the movie was released and never told the producers. Eyecon and its founder Stephanie "Bright" Riley also took $135,000 from Phildelpia Phillies Shortstop Jimmy Rollins for what he was told was "production costs" related to Tupac Assassination. Rollins received a $200,000 court judgment against Riley and Eyecon and the distribution agreement was voided by the court. The case was settled out of court for an undisclosed sum.
Camelot Entertainment and Incentive Capital released assumed claims of movie license.  However the producers of the movie still maintain copyright and currently have a digital distribution deal with Cinedigm, formerly New Video.

==Tupac Reckoning==
Tupac: Assassination II: Reckoning was released on January 27, 2009. The movie features Gloria Cox, Shakurs Aunt, Leila Steinberg, Shakurs manager, and Tracey Robinson, his video producer.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 