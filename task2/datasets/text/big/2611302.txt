Once Upon a Forest
{{Infobox film
|name=Once Upon a Forest
|image=OUAF poster.jpg
|director=Charles Grosvenor
|producer=William Hanna David Kirschner Jerry Mills Paul Gertz
|screenplay=Mark Young Kelly Ward
|story=Rae Lambert (original Welsh story, A Furlings Story,    and Furlings characters)
|starring=Michael Crawford Ellen Blain Benji Gregory Paige Gosney Elisabeth Moss Ben Vereen
|music=James Horner
|editing=Pat A. Foley HTV Cymru/Wales
|distributor=20th Century Fox
|released= 
|runtime=72 minutes
|country=United States United Kingdom
|language=English
|budget=$13 million Beck (2005), p. 184. 
|gross=$6,582,052
}}
 HTV Cymru/Wales Hocus Pocus).

It tells the story of three forest denizens that go on an expedition to cure their friend, Michelle, who became sick from chemical fumes. The films environmental theme divided critics at the time of its release, along with the animation and story. The film was a box office bomb , grossing only US$6.6 million against a budget of 13 million.

==Plot==
The story opens in a forest known as Dapplewood, where "Furlings" (a term for animal children) live alongside their teacher, Cornelius (Michael Crawford). The four Furlings central to the story are Abigail (Ellen Blain), a woodmouse; Russell (Paige Gosney), a hedgehog; Edgar (Benji Gregory), a mole (animal)|mole; and a badger named Michelle (Elisabeth Moss), who is Cornelius niece.

One day, the Furlings go on a trip through the forest with Cornelius, where they see a road for the first time. Russell is almost run over by a careless driver, who throws away a glass bottle that shatters in the middle of the road. Cornelius orders the Furlings to forget the road altogether. The ramble ends in a boat ride. Afterward, they go back to the forest to find out that it has been destroyed with   and euphrasia|eyebright. With limited time, they head off for their journey the next day.

After encountering numerous dangers including a hungry barn owl, a flock of religious wrens including their preacher Phineas (Ben Vereen), and intimidating construction equipment, which the wrens call "yellow dragons", the Furlings make it to the meadow with the herbs they need. There, they meet the bully squirrel Waggs (who insulted Michelle), and Willy, a tough but sensible mouse who grows a liking to Abigail. After getting the eyebright, they discover that the lungwort is on a giant cliff making it inaccessible by foot. Russell suggests they use Cornelius airship, the Flapper-Wing-a-Ma-Thing, to get to the lungwort, which he unknowingly brought along to be a bag for food he decides to bring.

The Furlings manage to get the lungwort after a dangerous flight up the cliff, then steer their airship back for Dapplewood. They crash-land back in the forest after a storm, and bring the herbs to Michelle and Cornelius. A group of humans who come to clean up the gas mess appear. The animals, thinking the humans mean them harm, escape through the backdoor of Corneliuss house. Unfortunately, Edgar gets separated from the group, and (after losing his glasses), gets caught in an old trap. When one of the workers finds him, the animals are at first worried about their friend, but are surprised when he frees Edgar and smashes the trap before stuffing it in his trash bag, an act Cornelius never expected a human to do, making him realize that there are good humans in the world.

The next day, Michelle is given the herbs and awakened from her coma. Cornelius sees the Flapper-Wing-a-Ma-Thing and becomes amazed on how the Furlings have grown-up. The Furlings families and many of the other inhabitants arrive as well, except for Michelles mother and father, who were killed in the gas accident, but Cornelius promises to do his best on taking care of her. The Furlings happily reunite with their families, who are relieved to see that their children are alright. Michelle asks Cornelius if anything will ever be the same again. Cornelius looks at the dead trees in the forest and says to her that if everyone works as hard to save Dapplewood as the Furlings did to save Michelle, it will be.

==Cast==
*Michael Crawford as Cornelius the badger
* Ellen Blain as Abigail the woodmouse
*Benji Gregory as Edgar the mole
* Paige Gosney as Russell the hedgehog
*Elisabeth Moss as Michelle, Corneliuss niece
*Ben Vereen as Phineas the wren preacher
*Will Estes as Willy, Abigails love interest
*Charlie Adler as Waggs the bully squirrel
*Rickey DShon Collins as Bosworth
*Don Reed - Marshbird
*Robert David Hall as Truck driver
*Paul Eiding as Abigails father
*Janet Waldo as Edgars mother
*Susan Silo as Russells mother
* Angel Harper as Bosworths mother
* Benjamin Kimball Smith as Russells brother
* Haven Hartman as Russells sister.
*Glenn Close as Herself/Narrator (uncredited/deleted scenes)

==Production== Mark Young and Kelly Ward, the project started as a made-for-TV movie with The Endangered as its new name. Beck (2005), p. 184.   . Retrieved February 9, 2007.  With 20th Century Fox on board, it was re-designed as a theatrical feature, with a US$13 million cost attached. Beck (2005), p. 184.  The producer was David Kirschner, former chairman and CEO of Hanna-Barbera. Beck (2005), p. 184. 

At the suggestion of Liz Kirschner, the wife of the films producer, The Phantom of the Opera (1986 musical)|The Phantom of the Opera  s Broadway star Michael Crawford was chosen to play Cornelius. Members of South Central Los Angeles First Baptist Church were chosen to voice the chorus accompanying the preacher bird Phineas (voiced by Ben Vereen). While filming the live-action references, the crew "was thrilled beyond   expectations   started flipping their arms and moving their tambourines", recalls Kirschner. Beck (2005), p. 184. 

William Hanna, co-founder and chairman of Hanna-Barbera was in charge of the films production. "  the finest feature production   ever done," he told The Atlanta Journal-Constitution in May 1993. "When I stood up and presented it to the studio, my eyes teared up. It is very, very heartwarming." Beck (2005), p. 184. 
 Jaime Diaz Studio of Argentina; Denmarks A. Film; Phoenix Animation Studios in Toronto, Canada; and The Hollywood Cartoon Company. Mark Swanson Productions did computer animation for the "Yellow Dragons" and the Flapper-Wing-a-Ma-Thing.  . Retrieved July 6, 2006. 

Because of time constraints and budget limitations, over ten minutes were cut from the film before its release. One of the deleted scenes featured the voice of Glenn Close, whose character was removed entirely from the final storyline. Beck (2005), p. 184.    At around the same time, the Fox studio changed the name of The Endangered to the present Once Upon a Forest, for fear audiences would find the former title too sensitive for a childrens film. 
 Times Union Albany found it misleading, hoping instead for the likes of Don Bluth or Steven Spielberg.  
 The Flintstones and Scooby-Doo (film)|Scooby-Doo before being dissolved in 2001).

==Release and reception==
  based on 21 reviews.

In spite of the financial death and criticisms, Forest soon gained a cult following among its fans.  Fox Videos original VHS and laserdisc issue of the film, released on September 21, 1993, proved successful on the home video market for several months. Beck (2005), p. 184.   On October 28, 2002, it premiered on DVD, also available on VHS in the UK with the content presented in fullscreen and widescreen formats.   The original trailer was included as the only extra on the Australian Region 4 version. 

Once Upon a Forest was nominated for an Annie Award for Best Animated Feature in 1993. It won an MPSE Golden Reel Award for Best Sound Editing. 

==Soundtrack==
 
The score for Once Upon a Forest was among the last that composer  , has been out of print since its publisher went out of business in the mid-1990s. 

==Merchandise== Turner Publishing and distributed by Andrews McMeel, a month prior to the films release (ISBN 1-878-68587-2).

The multimedia company Sanctuary Woods also released a MS-DOS point-and-click adventure game based on the film, on CD-ROM and floppy disk for IBM computers; Beth Agnew served as its adapter.  Many elements of the game stayed faithful to the original source material.  

==See also==
*  - Another Fox animated film with an environmental theme.
*List of animated feature-length films
*List of works produced by Hanna-Barbera

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 