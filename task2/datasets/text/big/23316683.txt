Retrograde (film)
{{Infobox film
| name           = Retrograde
| image          = Retrograde.jpg
| caption        = DVD cover
| director       = Christopher Kulikowski
| producer       = Gianluca Curti Tom Reeve Jamie Treacher
| screenplay     = Tom Reeve Gianluca Curti
| story          = Christopher Kulikowski
| starring       = Dolph Lundgren Gary Daniels
| music          = Emilio Maccolini
| cinematography = Carlo Thiel
| editing        = Peter Davies
| distributor    = Franchise Pictures
| released       = 2004
| runtime        = 93 minutes
| country        = Italy Luxembourg
| language       = English
| budget         = 
}} science fiction action film directed by Christopher Kulikowski and starring Dolph Lundgren. The film was released theatrically in South Korea on 14 January 2005. It was shot in Italy and Luxembourg.

==Cast==
*Dolph Lundgren as John Foster
*Silvia De Santis as Renee Diaz
*Joe Montana as Dalton
*Gary Daniels as Markus
*Joe Sagal as Andrew Schrader
*Ken Samuels as Captain Robert Davis
*David Jean Thomas as Jefferson
*Jamie Treacher as Mackenzie
*Marco Lorenzini as Bruce Ross
*Scott Joseph as Greg
*Adrian Sellars as Keith
*James Chalke as Vacceri
*Nicolas de Pruyssenaer as Ichek
*Dean Gregory as Central Command Leader
*Derek Kueter as Charley

==External links==
* 

 
 
 
 
 
 


 
 