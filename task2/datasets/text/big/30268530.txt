Giant (2009 film)
{{Infobox film
| name           = Giant
| image          = Giant (2009 film).jpg
| alt            = 
| caption        = Film poster for United States release
| director       = Adrián Biniez
| producer       = Fernando Epstein Christoph Friedel Hernán Musaluppi Frans van Gestel
| screenplay     = Adrián Biniez
| starring       = Horacio Camandule Leonor Svarcas
| music          = Harald Kloser Thomas Wander
| cinematography = Arauco Hernández Holz
| editing        = Fernando Epstein 
| studio         = Ctrl Films
| distributor    = Filmmovement.com
| released       =  
| runtime        = 84 minutes
| country        = Uruguay
| language       = Spanish
| budget         = 
| gross          = 
}}
Giant ( ) is a 2009 comedy film, written and directed by Adrían Biniez, an Argentinian film director living in Uruguay.

==Synopsis==
Jara (Horacio Camandule) is a security guard at a supermarket who falls in love with Julia (Leonor Svarcas), a cleaning worker on the night shift. Jara is about 30 years old, solitary, quiet and big. That is why before approaching to Julia, he watches her via the television cameras monitoring the supermarket, and then pursues her across the city of Montevideo, where the film is set. 

== Cast==
*Horacio Camandule as  Jara
*Leonor Svarcas as Julia 
*Diego Artucio as Omar 
*Ariel Caldarelli as Jaras boss 
*Fabiana Charlo as Mariela 
*Andrés Gallo as Fidel
*Federico García as Matías 
*Néstor Guzzini as Tomás
*Esteban Lago as Gustavo
*Ernesto Liotti as Danilo
*Carlos Lissardy as Kennedy

==Awards and nominations==

=== Film Awards===
{| border="1" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|-  style="background:#b0c4de; text-align:center;"
! Year !! Film Festival !! Award !! Category
|- 2009 || Berlin Film Best Film  
|- 2009 ||  Berlin Film Festival || Alfred Bauer Award || Best Debut Film
|- 2009 ||  Berlin Film Festival || Gold Hugo || New Directors Competition
|- 2009 || Havana Film Festival || Grand Coral || First Work
|- 2009 || Lima Film Festival || Special Jury Prize || Best Film
|- 2009 || San Sebastián International Film Festival || Horizons Award || Best Film
|- 2010 || AFM International Independent Film Festival || !f Inspired Award ||
|}

===Submissions===
*Berlin Film Festival
**Golden Bear (nominated)
* Goya Awards
** Best Spanish Language Foreign Film (nominated)

==See also==
*List of films featuring surveillance

==References==
 

==External links==
*  

 
 
 
 
 


 
 