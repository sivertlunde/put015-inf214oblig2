Up in Smoke (1957 film)
{{Infobox film
| name           = Up in Smoke
| image_size     =
| image          = Up in Smoke FilmPoster.jpeg
| caption        =
| director       = William Beaudine
| producer       = Richard Heermance
| writer         = Bert Lawrence (story) Jack Townley (screenplay) Elwood Ullman (story)
| starring       = Huntz Hall Stanley Clements David Gorcey Eddie LeRoy Dick Elliott
| music          = Marlin Skiles
| cinematography = Harry Neumann William Austin Allied Artists Pictures
| distributor    = Allied Artists Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Up in Smoke is a 1957 film starring the comedy team of The Bowery Boys.  The film was released on December 22, 1957 by Allied Artists and is the forty-seventh film in the series.

==Plot==
The Bowery Boys have been collecting money to help a young polio victim in the neighborhood. At Mike Clancys café, Sach is entrusted with taking the ninety dollars they collected to the bank. Sam, a new customer of Mikes, offers to give Sach a ride to the bank, but takes him instead to a phony bookie joint where, unaware that the operation is not legitimate, he loses all the money to con men Tony and Al. After Duke berates him for losing the cash, Sach tells Blinky that he would give his very soul to get even with the bookies. Seconds after Blinky leaves, Sach receives a visit from the devil, sporting a morning coat and two small horns under his hat. The devil offers Sach a deal: he will provide Sach with the name of a winning horse every day for a week in return for Sachs soul. Although scared, Sach ultimately agrees and, after signing the devils contract, is provided with his winner of the day.

When Sach returns to Tony and Al to make a bet, they ask him for cash. Sach then decides to sell a jalopy belonging to the boys and takes it to a used car dealer, unaware that Duke has just sold the car to a patrol officer. The dealer virtually tears the car apart to reduce its value and offers Sach a dime for it, just as the policeman finds and arrests him. The devil visits Sach in jail and gives him the name of another winning horse. When Sachs horse wins, Tony and Al wonder if he might have inside information. With Mikes help, Duke and the others bail Sach out and the next day, the devil reappears in the form of an organ grinders monkey with another tip. After the boys discover Sach chatting with the monkey, they arrange for him to visit a psychiatrist, but Sach only confuses the doctor, who ends up asking him for tips on winning horses. With twenty dollars borrowed from Mike, Sach returns to the phony bookie joint to bet on the newest horse the devil gave him, and Tony and Al decide to lay legitimate bets on the same horse. At the last moment, however, Sach is persuaded by a tout to bet on a different horse and everybody loses. Tony and Al are mystified by Sachs inside information and persuade Mabel, Tonys girl friend, to take a waitress job at Mikes to keep tabs on Sach and to find out the source of his tips. On the last day of their agreement, the devil gives Sach a hundred dollar bill and tells him to go to the racetrack and await word on the winning horses name.

Sach and Duke go to the track, accompanied by Mabel, and are followed by Tony, Al and Sam. Just after Sach receives the horses name, "Rubber Check," from the devil, disguised as a soft drinks salesman, Chuck arrives with the news that the Polio Fund has agreed to pay for the boys treatment. Sach then realizes that they now do not need the money and he can cancel the devils contract. Duke, however, insists on betting on Rubber Check and Mabel tips off Tony and the others. Sach talks with the devil and attempts to break the contract, but the devil refuses and points out that if the horse wins, Sachs soul is his. Sach then convinces Duke to help him to disable Rubber Checks jockey after which Sach takes his place in the race, but is unable to stop the horse from winning. Just after Sach explains the full dimensions of his problem to Duke, the devil reappears to claim him, but is thwarted by an official track announcement that Rubber Check is disqualified as he had an unauthorized jockey, thereby nullifying the devils contract with Sach and causing Tony and his gang to lose all their money.

Back in the Bowery, Sach is surprised to find the disenfranchised devil working as a busboy at Mikes. After the devil tells him that he can regain his "horns" by securing new clients, Sach directs him towards the bookies.

==Production== Allied Artists.
*This is the only film in which Sach refers to Duke as Chief. Typically, Sach would only refer to Slip (Leo Gorcey) as Chief, and would refer to Duke as Dukey.

==Cast==

===The Bowery Boys===
*Huntz Hall as Horace Debussy Sach Jones
*Stanley Clements as Stanislaus Duke Covelske
*David Gorcey as Charles Chuck Anderson
*Eddie LeRoy as Blinky

===Remaining cast===
*Dick Elliott as Mike Clancy June Bamber as Mabel
*Byron Foulger as Satan|Mr. Bubb
*Ralph Sanford as Sam
*Ric Roman as Tony Joe Devlin as Al
*Fritz Field as Dr. Bluzak
*Benny Rubin as Bernie
*James Flavin as Policeman
*Earle Hodgins as Friendly Frank
*John Mitchum as Desk Sergeant
*Jack Mulhall as Police Clerk
*Wilbur Mack as Druggist

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Three" on October 1, 2013.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Looking for Danger 1957
| after=In the Money 1958}}
 

 
 

 
 
 
 
 
 
 
 