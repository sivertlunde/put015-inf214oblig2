El Condor (film)
{{Infobox film
| name           = El Condor
| image_size     =
| image	         = El Condor FilmPoster.jpeg
| caption        =
| director       = John Guillermin
| producer       = André De Toth
| screenplay     = Larry Cohen Steven Carabatsos
| story          = Steven Carabatsos Patrick ONeal Marianna Hill Iron Eyes Cody
| music          = Maurice Jarre
| cinematography = Henri Persin
| editing        = Walter Hannemann William H. Ziegler
| distributor    = National General Pictures
| released       =  
| runtime        = 102 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
El Condor is a 1970 western war film directed by John Guillermin.
 Patrick ONeal. Emperor Maximilian.
 35mm Technicolor Conan the March or Die (1977).
 rated R (for violence, explicit language, and nudity).

==Plot==
Luke, an escaped convict, and Jaroo, a loner gold prospector, team up with a band of Apache Indians in 19th century Mexico to capture a large, heavily armed fortress for the millions—or billions—of dollars in gold that are rumored to be stored within. 

==VHS and DVD releases==
The movie was released on a   VHS in 1994, and a widescreen DVD by Warner Archive in 2009.

==Critical reaction==
Roger Eberts review of August 27, 1970 in the Chicago Sun-Times began: "Lee Van Cleefs last words in El Condor are, What am I doing here? Amen, brother."

==References==
 
*The American Film Institute Catalog of Motion Pictures Produced in the United States – Feature Films, 1961-1970.  University of California Press, 1997, page 298.
*Greenspun, Roger “Screen: `El Condor’ Bows, Mexican Treasure Tale Comes to the Forum”, New York Times, June 20, 1970, page 22.

==External links==
* 

 

 
 
 
 
 
 
 


 