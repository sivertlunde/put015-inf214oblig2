Simón Bolívar (1969 film)
 
{{Infobox film
| name           = Simón Bolívar
| image          = Simón Bolívar (1969 film).jpg
| caption        = Film poster
| director       = Alessandro Blasetti
| producer       = 
| writer         = José Luis Dibildos Enrique Llovet
| starring       = Maximilian Schell
| music          = 
| cinematography = Manuel Berenguer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Simón Bolívar is a 1969 Spanish drama film directed by Alessandro Blasetti. It was entered into the 6th Moscow International Film Festival.   

==Cast==
* Maximilian Schell as Simón Bolívar
* Rosanna Schiaffino as Consuelo Hernandez
* Francisco Rabal as José Antonio Del Llano
* Barta Barri
* Elisa Cegani as Conchita Diaz Moreno
* Ángel del Pozo Luis Dávila as Carlos
* Manuel Gil
* Sancho Gracia
* Tomás Henríquez as Negro Primero
* Julio Peña as señor Hernandez
* Conrado San Martín as gen. José Antonio Paez
* Fernando Sancho as Fernando Gonzales

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 