Love Comes Along
{{Infobox film name            = Love Comes Along image           = LoveComesAlongGermanFilmPoster.jpg caption         = Theatrical poster for German release of the film director        = Rupert Julian producer  Henry Hobart writer  Wallace Smith (screenplay) starring        = Bebe Daniels Lloyd Hughes Montagu Love music           = Victor Baravalle (director) Sidney Clare (lyrics) Oscar Levant (music) cinematography  =   J. Roy Hunt editing         = Archie Marshek studio  RKO Radio Pictures distributor     = RKO Radio Pictures released        =   }} country         = United States runtime         = 72 minutes language        = English budget          = $220,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  gross           = $478,000 
}} 1930 American romantic film Wallace Smith, based on the uncompleted play Conchita by Edward Knoblock.  It was a vehicle specifically picked to highlight the vocal talents of Bebe Daniels, which also starred Lloyd Hughes and Montagu Love. It made a profit of $258,000.  

An incomplete print has long been preserved in the Library of Congress collection. 

==Plot==
An actress, Peggy, is stranded on the island of Caparoja, which is ruled by a local dictator, Sangredo. For a living, she sings in the local tavern, where she is seen by two sailors from a tramp steamer who are visiting the port, Johnny and Happy.  Johnny falls in love with Peggy and plans to marry her, rescuing her from her exile.  However, Sangredo hires Peggy to perform at a party he is throwing, when the original singer, Carlotta, backs out.  When Johnny finds out about the agreement, he misunderstands their relationship, and blows up at her.  Peggy gets furious in turn over the fact he could believe that about her, and calls the wedding off.

At the party, Peggy relents, and sings a love song directly to Johnny, which angers Sangredo.  He orders that Johnny be arrested, but Peggy steps forward to intercede on his behalf.  She offers to spend the night with Sangredo, if he will release Johnny and let him sail with his steamer.  He agrees, and Johnny is escorted to his ship.  However, Johnny and Happy, sneak back to the town and break Peggy out of Sangredos house.  Fleeing, they board the steamer, escaping from the island.

==Cast==
* Bebe Daniels - Peggy
* Lloyd Hughes - Johnny
* Montagu Love - Sangredo
* Ned Sparks - Happy
* Lionel Belmore - Brownie
* Alma Tell - Carlotta
* Evelyn Selbie - Bianca
* Sam Appel - Gómez

==Songs==
* "Night Winds" - performed by Bebe Daniels
* "Until Love Comes Along" - performed by Bebe Daniels
* "I Am a Simple Maid" - performed by Bebe Daniels
* "A Sailors Life" - Performed by Lloyd Hughes; reprised by Bebe Daniels and Lloyd Hughes

==Notes==
This film is based on the play Conchita by Edward Knoblock, which according to his papers, which are saved on the campus of Harvard University in the rare books collection in the Houghton Library, was never completed, with only an outline existing. 

The film is also known by its Italian title, Ecco lamore!.   

==References==
 

==External links==
*  at IMDB
*  at TCMDB
* 

 
 
 
 
 
 
 
 
 