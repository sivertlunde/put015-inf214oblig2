Jaanam
 
{{Infobox film
| name           = Jaanam
| image          = Jaanam_1992.jpg
| caption        = Poster for Jaanam
| director       = Vikram Bhatt
| producer       = Mukesh Bhatt
| writer         = 
| starring       = Rahul Roy  Pooja Bhatt
| music          = Anu Malik
| lyricist       = 
| cinematography = Jagdish Nivergi
| editing        = B. Prasad
| distributor    = 
| released       = 1992
| runtime        = 
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Jaanam is a 1992 Bollywood film directed by Vikram Bhatt. It stars Rahul Roy and Pooja Bhatt in the lead roles. It has a critically and publicly acclaimed soundtrack by Anu Malik.

==Plot summary==
Paradise Builders is owned by Dhanraj, who lives a very wealthy lifestyle with his wife, Radha; son, Rajan; and daughter, Anjali. His aim to take possession of the nearby village, which is now occupied by fishermen, headed by Shankar Rao, who lives there with his wife and two sons, Amar and Arun. Dhanrajs attempts to take possession prove to be in vain. Then the inevitable happens when Amar and Anjali meet and fall in love with each other, much to the chagrin of both Shankar and Dhanraj. While Shankar wants Amar to continue to follow his profession, Dhanraj wants Anjali to wed U.S.-based Shekhar Gupta, the son of the owner of Gupta Investments. Push leads to shove, and Dhanraj asks Rajan to set fire to the village. As a result, Rajan and Arun get killed. A grief-stricken Dhanraj swears on his sons ashes that he will avenge this death by killing Amar - at any and all costs. Watch what impact this has on the Rao family, as well as on Amar and Anjali.

==Cast==
* Rahul Roy... Amar S. Rao
* Pooja Bhatt... Anjali
* Paresh Rawal... Shankar Rao
* Ashutosh Gowariker... Arun S. Rao
* Sulabha Arya... Kamla
* Avtar Gill... Police Inspector
* Vikram Gokhale... Dhanraj 
* Bashir Khan... Rajan 
* Reema Lagoo... Shankars wife
* Nandita Thakur... Radha

==Soundtrack==
 Love Story Where Do I Begin".

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Pagalpan Chha Gaya Dil Tumpe Aa Gaya"
| Vipin Sachdeva, Anuradha Paudwal
|-
| 2
| "Teri Chahat Ke Siva Jaanam Meri"
| Amit Kumar, Anuradha Paudwal
|-
| 3
| "Mere Dil Ka Pata Tumhein Kisne Diya" Vipin Sachdeva, Anuradha Paudwal
|-
| 4
| "Dil Jigar Ke Jaan Achchha Hai"
| Amit Kumar, Anuradha Paudwal
|-
| 5
|"Dil Kyon Dhadakta Hai Kyon Pyar Hota Hai"
| Vipin Sachdeva, Anuradha Paudwal
|-
| 6
| "Maari Gayi"
| Anu Malik, Anuradha Paudwal
|}

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 