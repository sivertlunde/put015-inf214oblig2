Running with Scissors (film)
{{Infobox film
| name           = Running with Scissors
| image          = Running with Scissors (2006 movie poster).jpg
| alt            =
| caption        = Theatrical release poster Ryan Murphy
| producer       = {{Plain list | 
* Ryan Murphy
* Brad Pitt
* Brad Grey
* Dede Gardner
}}
| screenplay     = Ryan Murphy
| based on       =  
| starring       = {{Plain list |
* Annette Bening Brian Cox
* Joseph Fiennes
* Evan Rachel Wood
* Alec Baldwin
* Jill Clayburgh Joseph Cross
* Gwyneth Paltrow
}}
| cinematography = Christopher Baffa
| editing        = Byron Smith
| production companies= {{Plain list |
* Plan B Entertainment Ryan Murphy Productions
}}
| distributor    = TriStar Pictures
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $7,022,827   
}}
 memoir of Ryan Murphy, Joseph Cross, Brian Cox, Joseph Fiennes, Evan Rachel Wood, Alec Baldwin, Jill Clayburgh, and Gwyneth Paltrow.

==Plot==
 Joseph Cross) Brian Cox), the eccentric patriarch of an oddball family, which consists of his submissive wife Agnes (Jill Clayburgh), religious daughter Hope (Gwyneth Paltrow), and his rebellious youngest child Natalie (Evan Rachel Wood).

Augusten finds it hard to adjust to living with the doctor’s family, and is subject to irregular weekend visits by his increasingly unsound mother. After confessing to Natalie that he is gay, Augusten befriends Neil Bookman (Joseph Fiennes), Finchs adopted 33-year-old son. The two begin an erratic sexual relationship quickly after meeting, but Augusten finds it difficult to cope with their age difference.

Finch manipulates Deirdre into signing over her money to him. Deirdre finds temporary stability with her living companion Dorothy Ambrose (Gabrielle Union), but Augusten feels like his mother no longer wants him, and deals with the negative effects of Neils schizophrenia.

At the end of the movie, Augusten leaves for New York to become a writer. He says goodbye to his mother and goes to the bus station. Agnes, with whom he has developed a caring relationship, arrives and gives him some money she has saved up.

==Cast==
* Annette Bening as Deirdre Burroughs Brian Cox as Dr. Finch
* Joseph Fiennes as Neil Bookman
* Evan Rachel Wood as Natalie Finch
* Alec Baldwin as Norman Burroughs
* Jill Clayburgh as Agnes Finch Joseph Cross as Augusten Burroughs
* Jack Kaeding as 6-year-old Augusten
* Gwyneth Paltrow as Hope Finch
* Gabrielle Union as Dorothy Ambrose Patrick Wilson as Michael Shephard
* Kristin Chenoweth as Fern Stewart
* Dagmara Dominczyk as Suzanne
* Colleen Camp as Joan
* Marianne Muellerleile as Nurse
* Augusten Burroughs (uncredited) as Himself
* Leslie Grossman (uncredited) as Sue

==Reception==
===Critical response=== normalized rating, gave the film a score of 52 out of 100, based on 32 critics, indicating "mixed or average reviews".  On Rotten Tomatoes, the film holds a 31% approval rating, based on 130 reviews, with an average score of 5/10. The sites consensus states: " Despite a few great performances, the film lacks the sincerity and emotional edge of Burroughs well-loved memoir". 

===Accolades===
{| class="wikitable"
|+
! Award !! Category !! Recipients and nominees !! Result
|-
| Boston Society of Film Critics Best Supporting Actor
| Alec Baldwin
|  
|-
| Critics Choice Movie Awards Best Young Performer
| Joseph Cross
|  
|-
| GLAAD Media Award Outstanding Film – Wide Release
| 
|  
|-
| Golden Globe Award Best Actress in a Motion Picture - Musical or Comedy
| Annette Bening
|  
|-
| Hollywood Film Awards
| Breakthrough Directing
| Ryan Murphy
|  
|- Satellite Award Best Actor – Motion Picture Musical or Comedy
| Joseph Cross
|  
|- Best Actress – Motion Picture Musical or Comedy
| Annette Bening
|  
|-
|rowspan="3"|St. Louis Gateway Film Critics Association Best Actress
| Annette Bening
|  
|- Best Supporting Actress
| Jill Clayburgh
|  
|-
| Best Overlooked Film
| 
|  
|-
|}

==Soundtrack==
The soundtrack for the film was released on September 26, 2006, a month prior to the films release. 
 Pick Up the Pieces" – Average White Band
# "Blinded by the Light" – Manfred Manns Earth Band The Things We Do for Love" – 10cc
# "Mr. Blue" – Catherine Feeny
# "One Less Bell to Answer" – The 5th Dimension
# "Quizás, Quizás, Quizás" (Perhaps, Perhaps, Perhaps) – Nat King Cole Phoebe Snow
# "Bennie and the Jets" – Elton John
# "Year of the Cat" – Al Stewart
# "O Tannenbaum" – Vince Guaraldi Trio
# "A Great Ocean Liner" – James S. Levine
# "Stardust (song)|Stardust" – Nat King Cole
# "Teach Your Children" – Crosby, Stills, Nash & Young

An adaptation of   are used in the film as well. The song playing in the "Stew" scene is "d-moll" by the duo Tosca off of their album Delhi 9; this theme is repeated through the film.

==See also==
* List of lesbian, gay, bisexual, or transgender-related films by storyline

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 