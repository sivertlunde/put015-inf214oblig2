Robinson Crusoe on Mars
{{Infobox film
| name           = Robinson Crusoe on Mars
| image          = Robinson crusoe on mars movie poster.jpg Theatrical release poster
| director       = Byron Haskin
| producer       = Aubrey Schenck
| based on       =  
| writer         = John C. Higgins  Ib Melchior
| starring       = Paul Mantee  Victor Lundin  Adam West  Barney the Wooly Monkey
| music          = Nathan Van Cleave
| cinematography = Winton C. Hoch
| editing        = Terry O. Morse
| studio         = Devonshire Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 110 minutes English
| budget =         $1,200,000 (estimated) 
}}

Robinson Crusoe on Mars is a 1964 independently made color Techniscope science fiction film distributed by Paramount Pictures that was produced by Aubrey Schenck, directed by Byron Haskin, and stars Paul Mantee, Victor Lundin, and Adam West. 

Robinson Crusoe on Mars is a science fiction retelling of the classic novel Robinson Crusoe by Daniel Defoe. 

==Plot==
Commander Christopher "Kit" Draper (Paul Mantee) and Colonel Dan McReady (Adam West) reach the Red Planet in their spaceship, Mars Gravity Probe 1. They are forced to use up their remaining fuel in order to avoid an imminent collision with a large orbiting meteor; they descend in their one-man lifeboat pods, becoming the first humans on Mars.

Draper eventually finds a rock face cave for shelter. He then figures out how to obtain the rest of what he needs to survive: he burns some coal-like rocks for warmth and discovers by accident that heating them releases trapped oxygen. This allows him to refill his air tanks with a hand pump and to move around in the thin Martian atmosphere. On one of his excursions, he finds McReadys crashed pod and dead body. He then finds their flight-test monkey, Mona, alive and returns with her to the cave. Draper then constructs a crude sand alarm clock to awaken him for periodic doses of oxygen.

Later, he notices that Mona keeps disappearing and is uninterested in their dwindling supply of food and water. He gives her a salty cracker but no water. When Mona gets very thirsty, he lets her out and follows her to a cave and an underground spring, which also has edible plant "sausages" growing in the water.

As the days grow into months, Draper slowly begins to crack from the prolonged isolation. He watches helplessly as his mothership, an inaccessible "supermarket", periodically orbits overhead; without fuel, the spaceship cannot respond to his radioed order to land.

While walking about, Draper comes upon a dark rock slab standing almost upright. Curious, he digs in the ground around it, exposing a skeletal hand and arm wearing a black bracelet. He uncovers the rest of the humanoid skeleton and determines that the alien was murdered; the front of the skull shows heavy charring. To hide his presence on Mars, Draper signals his low-orbiting mothership to self-destruct on its next overhead pass.

Just in time, as Draper sees a spaceship descend and land just over the horizon. Believing it might be a rescue ship from Earth, the following morning he heads towards the landing site, only to see alien spacecraft in the sky. He approaches cautiously and sees slave labor being used for mining. One of the slaves (Victor Lundin) escapes, running into Draper; an alien ship blasts their area as the two escape. Draper notices the stranger is wearing black bracelets just like the one he found in the grave. The aliens bombard the mine area that night and then depart. When he and the stranger investigate, they find the bodies of the other slaves.
 polar icecap. snow shelter. Draper finally succeeds in cutting off Fridays bracelets shortly before the orbiting meteor crashes into the ice cap; the resulting explosion and firestorm melts the ice and snow.

Later, Draper detects an approaching spaceship. He fears it is the returning aliens, but is relieved when his portable radio picks up an English-speaking voice. Draper and Friday watch a descending rescue capsule; Mars recedes in the distance as the film credits scroll.

==Cast== 
* Paul Mantee as Commander Christopher "Kit" Draper
* Victor Lundin as Friday
* Adam West as Colonel Dan McReady. 
* Barney the Woolly Monkey, as Mona

==Production==
Robinson Crusoe on Mars was shot on location, much of it at Zabriskie Point in Death Valley National Park, California.  

Special effects by Lawrence Butler and Academy Award-winning matte artist Albert Whitlock, gave the film the benefit of "big-studio resources usually lacking in movies about outer space". Whitlock provided the matte paintings used in Robinson Crusoe on Mars. "The vegetation-free canyons of Death Valley stood in as the surface of Mars. Some scenes of spacecraft in motion were created with the kind of flat animation seen in official NASA promotional films." Erickson, Glenn.   DVD Savant, January 9, 2011. Retrieved: January 9, 2015. 

During the film, when Commander Draper and Friday can finally communicate with each other, Draper points to a location in the Martian night sky and identifies Earth as his origin world. In return Friday points to a different part of the same night sky and identifies a world orbiting the bright blue star Alnilam, in the distant constellation of Orion (constellation)|Orion. 

==Songs==
Two songs were inspired by and named after the film. One was sung by Johnny Cymbal, the other by Victor Lundin. Lundin wrote the song "Robinson Crusoe on Mars" to perform during his science fiction convention appearances. He recorded it for his 2000 album Little Owl. "Music Video: Robinson Crusoe on Mars (supplementary material made for DVD release)". Criterion Collection DVD, 2007. 

==Reception==
Despite positive critic reaction at the time, Robinson Crusoe on Mars did not do well at the box office. Film reviewer Glenn Erickson opined, "Despite laudable efforts from all concerned, the film didnt click with audiences. Indifferent distribution was blamed, but its also likely that the public preferred to see its astronauts on the 6 OClock News."  Film historian Leonard Maltin considered Robinson Crusoe on Mars, "a surprisingly agreeable reworking of the classic Defoe  story ... beautifully shot in Death Valley by Winston C. Hoch; films intimate nature help it play better on TV than most widescreen space films." 

In the Time Out review editor John Pym saw Robinson Crusoe on Mars as "... intelligently imaginative sci-fi ... most remarkably (director) Haskin avoids sentimentality when dealing with the monkey, such is the assured sensitivity of the film." 

==Home media==
===Criterion restoration=== high definition video transfer, created on a Spirit 4K Datacine, from a 35 mm film print, was struck from the films original negative. Thousands of pieces of dirt, debris, and scratches were removed using the MTI Digital Restoration System. For optimal image quality, Criterion also encoded the dual-layer DVD-9 at the highest possible bit rate. The films original Monaural soundtrack was remastered at 24 bit, and audio restoration tools were used to eliminate clicks, pops, hisses, and crackles. 

Criterion added a number of bonus features on the releases of the film: a "stills" gallery from both the film itself, as well as behind-the-scenes shots. There is also the original theatrical trailer and an audio interview with director Byron Haskin recorded in 1979. A music video for Victor Lundins song "Robinson Crusoe on Mars" was created in 2007 specifically for the films DVD release. A full color booklet is also included with various facts about the film. 

==References==
Notes
 

Bibliography
 
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
* Parish, James Robert and Michael R. Pitts. The Great Science Fiction Pictures. Jefferson, North Carolina: McFarland & Company, 1977. ISBN 0-8108-1029-8.
* Pym, John, ed. "Robinson Crusoe on Mars." Time Out Film Guide. London: Time Out Guides Limited, 2004. ISBN 978-0-14101-354-1.
* Strick, Philip. Science Fiction Movies. London: Octopus Books Limited. 1976. ISBN 0-7064-0470-X.
* Warren, Bill. Keep Watching the Skies: American Science Fiction Films of the Fifties, 21st Century Edition. Jefferson, North Carolina: McFarland & Company, 2009. ISBN 978-0-78644-230-0.
 

== External links ==
*  
*  
*  
*  , at the Criterion Collection
*  , by John Gosling
*  , essay, at Bright Lights Film Journal, by Walter Rankin
*   restoration review, by Robert Blevins, at Newsvine

 
 

 

 
 
 
 
 
 
 
 