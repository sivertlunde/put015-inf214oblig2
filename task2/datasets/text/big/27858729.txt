DC Showcase: Green Arrow
{{Infobox film
| name        = DC Showcase: Green Arrow
| image       = Green arrow title banner.png
| caption     = Green Arrow title banner
| alt         = 
| director    = Joaquim Dos Santos
| producer    = Bobbie Page Alan Burnett Joaquim Dos Santos Sam Register Bruce Timm
| writer      = Greg Weisman
| starring    = Neal McDonough Malcolm McDowell
| music       = Benjamin Wynn Jeremy Zuckerman
| studio      = Warner Bros. Animation Warner Premiere DC Comics
| distributor = Warner Home Video
| released    =  
| runtime     = 12 minutes
| country     =  
| language    = English
| budget      =
}}

DC Showcase: Green Arrow is a 2010   DVD, was the third of the DC Showcase series and was included on the compilation DVD DC Showcase Original Shorts Collection in an extended version. 

==Plot== Merlyn to Vlatava due to the king, Perditas father, and Count Vertigos older brother, being assassinated by Count Vertigo. While Perdita is at an airport in America, Merlyn attacks her. Green Arrow discovers Count Vertigos plan and is forced to intervene. Green Arrow dispatches several henchmen of Merlyn while keeping Princess Perdita safe before having to duel Merlyn himself. After Merlyn is defeated, Count Vertigo steps in and attempts to kill both the hero and Perdita, only to be defeated when Black Canary, who is revealed to be the arriving girlfriend named Dinah, knocks Count Vertigo unconscious with her "Canary Cry". In the aftermath of the battle with Vertigo, Green Arrow proposes marriage to Black Canary with Princess Perditas eager support. The film ends with Black Canary accepting Green Arrows proposal and the two share a passionate kiss.

==Cast== Green Arrow / Oliver Queen  Merlyn the Magnificent   
* Steven Blum as Count Vertigo  Black Canary / Dinah Laurel Lance 
* John DiMaggio as Merc #1 
* Ariel Winter as Princess Perdita 

==References==
 

==External links==
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 