Crimes at the Dark House
{{Infobox film
| name           = Crimes at the Dark House
| image_size     =
| image	=	Crimes at the Dark House FilmPoster.jpeg
| caption        = George King George King (producer) Odette King (producer) The Woman In White) Edward Dryhurst (writer) Frederick Hayward (writer) H.F. Maltby (writer)
| narrator       =
| starring       = See below
| music          = Jack Beaver
| cinematography = Hone Glendinning Jack Harris
| distributor    =
| released       = 1940
| runtime        = 69 minutes
| country        = UK
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} George King The Woman in White by Wilkie Collins.

==Plot summary==
In this lurid melodrama Tod Slaughter plays a villain who murders the wealthy Sir Percival Glyde in the gold fields of Australia and assumes his identity in order to inherit his estate in England. On arriving in England he schemes to marry an heiress for her money and, with the connivance of the enigmatic Count Fosco, embarks on a killing spree of all who suspect him to be an imposter and get in the way of his plans to be the Lord the Manor.

==Cast==
*Tod Slaughter as The False Percival Glyde
*Sylvia Marriott as Laurie Fairlie / Anne Catherick
*Hilary Eaves as Marion Fairlie
*Geoffrey Wardwell as Paul Hartwright
*Hay Petrie as Dr. Isidor Fosco
*Margaret Yarde as Mrs. Bullen
*Rita Grant as Jessica, the Maid David Horne as Frederick Fairlie
*Elsie Wagstaff as Mrs. Catherick
*David Keir as Lawyer Merriman

==Soundtrack==
 

==External links==
* 
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 


 