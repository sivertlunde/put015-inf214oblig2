Heebie Jeebies (film)
{{Multiple issues
| 
 

 
}}
{{Infobox film
| name           = Heebie Jeebies
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Doug Evans Michael Hawkins-Burgos
| producer       = Jono Adelman Anwar Floyd-Pruitt
| writer         = Doug Evans J.J. Shebesta
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Alex Balestrieri Deb Billing
| music          = 
| cinematography = David Dreckmann
| editing        = 
| studio         = Oak Hill Pictures
| distributor    = Artisan Lions Gate Films Home Entertainment
| released       =  
| runtime        = 90 min
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}

Heebie Jeebies is a 2005 film written by Doug Evans along with J.J. Shebesta, and directed by  Evans and Michael Hawkins-Burgos.

==Synopsis==
Cassandra once dreamt of her mothers death, only to have the dream come true in shockingly accurate detail. Now, several years later, the dreams return, each involving some of her friends from high school. In a desperate attempt to save them, she invites them all to a weekend getaway where she hopes to warn them of their fate and save their lives. However, death doesnt always stick to the plan as Cassie soon finds out. Her friends began to die in horrible and painful ways, but not the ways she dreamt of. As she learns, there have been some unexpected twists to her dreams and maybe what she had seen in slumber was the truth after all, maybe all she is seeing now is a nightmare she cannot escape.

==Cast==
*Alex Balestrieri
*Deb Billing
*Amy Booth
*Fermin D. Burgos
*Doug Evans
*Amy Geyser
*Michael Hawkins-Burgos
*Angela Kane
*Jeff Lee
*John Lee
*Lance Marsh
*Reaca Pearl
*J.J. Shebesta
*Roberto Soto
*Johathan Wainwright
*Bobby Jo Westphal
*Tony Wood
*Vanessa Yuille

==External links==
* 

 
 
 
 


 