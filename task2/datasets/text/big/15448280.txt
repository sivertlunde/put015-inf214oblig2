The Rejected Woman
 
{{Infobox film
| name           = The Rejected Woman
| image          = 
| caption        =  Albert Parker
| producer       = 
| screenplay     = John Lynch
| story          = John Lynch
| starring       = Alma Rubens Conrad Nagel
| cinematography = J. Roy Hunt
| editing        = 
| studio         = Distinctive Pictures Cosmopolitan Distributing Corporation
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          = 
}} silent society Albert Parker Cosmopolitan Distributing Corporation. 

A print of The Rejected Woman is preserved at the George Eastman House. 

==Synopsis==
John Leslie (Nagel) is a rich, New York City man who leads a brilliant life. While piloting his plane in Canada, he meets Diane Du Prez (Rubens) while seeking refuge from a storm. Shortly after John returns to New York City, Diane moves to town and the two began dating. Leslies friends are scandalized by the relationship as Diane is poor, shabbily dressed and unsophisticated. Unbeknownst to John, his business manager James Dunbar (Wyndham Standing) offers Diane financial assistance so that she can buy the clothing and receive the proper training to fit in with Johns upper class friends. Dianes father Samuel (George MacQuarrie) attempts to dissuade Diane from accepting the offer but she disregards her fathers advice as she is convinced that John will never love her unless she becomes well dressed and sophisticated.    

Shortly thereafter, John and Diane marry. After John learns of the arrangement Diane has made with his business manager, he becomes angry and the two quarrel. The couple eventually reconcile after realizing their love is greater than their differences. 
 
==Cast==
* Alma Rubens as Diane Du Prez
* Conrad Nagel as John Leslie
* Wyndham Standing as James Dunbar
* Béla Lugosi as Jean Gagnon
* George MacQuarrie as Samuel Du Prez Frederick Burton as Leyton Carter Antonio DAlgy as Craig Burnett 
* Aubrey Smith|C. Aubrey Smith as Peter Leslie
* Juliette La Violette as Aunt Rose
* Leonore Hughes as Lucille Van Tuyl (as Leonora Hughes)

==See also==
* Béla Lugosi filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 