Dark Corners
 
 
{{Infobox Film
| name = Dark Corners
| image = Dark corners poster.jpg
| caption = Film poster
| director = Ray Gower
| producer = Nigel Thomas
| writer = Ray Gower
| starring = Thora Birch Toby Stephens Christien Anholt Andrew Pearce
| distributor = Shoreline Entertainment
| released =  
| runtime = 92 minutes
| country = United States United Kingdom
| language = English
}}

Dark Corners is a 2006 horror film|horror-thriller film directed by Ray Gower and starring Thora Birch.

==Plot==
Birch plays two characters, alternating between them each time she falls asleep, each of whom believes that the other is a dream. The first of them, Karen Clarke, is a mortuary worker who awakes to find that she has injuries which she does not recall receiving, and the second is Susan Hamilton, an office worker who is preparing to undergo artificial insemination. As time passes Clarkes world becomes increasingly nightmarish, with a corpse coming to life on her table and a serial killer stalking her, and the line between the two worlds becomes increasingly fragile.

==Cast==
*Thora Birch as Susan Hamilton / Karen Clarke
*Toby Stephens as Dr. Woodleigh
*Christien Anholt as David Hamilton
*Joanna Hole as Elaine Jordan Glenn Beck as Mr. Saunders

==External links==
*  
*  

 
 
 
 
 
 

 