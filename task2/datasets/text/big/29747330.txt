Teri Meherbaniyan
 
{{Infobox film
| name           = Teri Meherbaniyan
| image          = Teri Meherbaniyan.jpg
| caption        = Teri Meherbaniyan poster
| director       = Vijay Reddy
| producer       = K.C. Bokadia
| writer         = Jagdish Kanwal, Rajesh Vakil (Dialogue)
| screenplay     = S. Sundaram
| story          = 
| narrator       =  Raj Kiran Swapna
| music          = Laxmikant-Pyarelal
| cinematography = R. Chittibabu
| editing        = Subhash Sehgal
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Hindi super hit film by K.C. Bokadia. Dog Brownie played the lead role while Jackie Shroff and Poonam Dhillon played second fiddle.

==Plot== Raj Kiran), a man who is mute. Sardari and Munim clash with Ram, but retreat, beaten, to their boss. Ram helps Gopi and Sharda and assists them in their life, encouraging them to marry.

One day, Ram returns to the city for some business. Before leaving, he asks Moti to guard Bijli. But Bijli tires of the dogs constant attention and locks him up. Thakur Vijay Singh, along with Sardari and Munim, comes and tries to rape Bijli. Finding herself cornered, rather than let him touch her, she stabs herself, leaving her father (Satyen Kappu) distraught and close to insanity. An enraged and heartbroken Ram whips Moti for failing to guard Bijli, but Sharda and Gopi restrain him, telling him that Bijli herself locked the dog up. Eventually Ram himself is gruesomely strangled and stabbed to death by the three villains, with Thakur framing Gopi for the murder, who is arrested by the police and Jailed.

With Bijli dead, Thakur now turns his immoral attentions on Sharda and kidnaps heri. Thakur, Sardari and Munim celebrate, now that their chief opponent and tormentor is out of the way.

Now the real hero of the movie takes centre stage - Rams faithful dog Moti!

Moti who had witnessed his masters brutal murder, recollects every incident that precedes his masters killing - including his singing and dancing with Bijli - bites and barks his way into inflicting terror in the hearts and minds of his masters killers. With deadly canines and sharp claws, the dog bumps off each and every one of them - first Sardari, then Munim, and finally Thakur, in a hurricane mission to avenge Rams murder. He has a little help from Gopi (who has escaped from the police), and with him rescues Sharda from Thakurs clutches. The revenge drama as it unfolds tugs at the heart-strings too, with scenes of Moti lying near his masters grave yearning to be with him again interspersed with the soulful, melodious title song Teri Meherbaniyan sung by Jackieda to the dog in flahsback. Moti also displays a sharp mind with him leading the cops to the evidence of Rams gruesome murder caught on tape by Rams camcorder.

==Cast==
* Brownie  as  Moti
* Amrish Puri  as  Thakur Vijay Singh
* Jackie Shroff  as  Ram
* Poonam Dhillon  as  Bijli
* Asrani  as  Munim Banwarilal
* Sadashiv Amrapurkar  as  Sardari Raj Kiran  as  Gopi Swapna  as  Sharda Devi
* Satyen Kappu  as  Bijlis father
* Vikas Anand  as  Villager

==Crew==
*Director : B. Vijay Reddy
*Producer : K. C. Bokadia
*Screenplay : S. Sundaram
*Dialogue : Jagdish Kanwal, Rajesh Vakil
*Editor : Satish, Subhash Sehgal

==Soundtrack==
{{Track listing
| headline        = Songs
| extra_column    = Playback
| total_length    = 
| all_lyrics      = S. H. Bihari
| all_music       = Laxmikant-Pyarelal

| title1 = Aag Lage Tanman Mein
| extra1 = Asha Bhosle

| title2 = Aai Jawani Mori Chunariya 	
| extra2 = Kavita Krishnamurthy

| title3 = Aanchal Udaya Maine
| extra3 = Shabbir Kumar, Kavita Krishnamurthy

| title4 = Dil Bekraar Tha Dil Bekraar Hai
| extra4 = Shabbir Kumar, Anuradha Paudwal

| title5 = Teri Meherbaniyan
| extra5 = Shabbir Kumar
}}

==External links==
* 

 
 
 

 