Right Now (film)
{{Infobox film
| name           = Right Now
| image          = 
| image_size     = 
| caption        = 
| director       = Benoît Jacquot
| producer       = Georges Benayoun
| writer         = Benoît Jacquot Elisabeth Fanger
| narrator       = 
| starring       = Isild Le Besco
| music          = 
| cinematography = Caroline Champetier
| editing        = Luc Barnier
| distributor    = 
| released       = 8 December 2004
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2004 cinema French film by director Benoît Jacquot. It was screened in the Un Certain Regard section at the 2004 Cannes Film Festival.   

==Synopsis==
In 1975, a young bourgeois woman falls in love with a bank robber. She follows him and his partner on the run after a bank heist resulted in a death and hostage taking. Using fake IDs, they leave Paris and travel to Spain, Morocco, and Greece.

==Cast==
* Isild Le Besco
* Ouassini Embarek
* Nicolas Duvauchelle
* Laurence Cordier
* Odile Vuillemin
* Emmanuelle Bercot : Laurence

==See also==
* Élisabeth Fanger

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 
 