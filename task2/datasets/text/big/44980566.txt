WWE Workout Series
{{Infobox film
| name           = WWE Workout Series
| image          = 
| caption        = 
| director       = 
| producer       = 
| writer         =  NXT Superstars NXT Divas
| music          = 
| cinematography =
| editing        = 
| distributor    = Lionsgate Home Entertainment WWE Home Video
| studio         = WWE Studios
| released       =      
| runtime        = Power Series 100 minutes  Fit Series 65 minutes    DVD Region 1
| language       = English
| budget         =
}} Region 1. The Workout Series was designed by Joe DeFranco and based on similar workouts he designed for Paul Levesque (better known as Triple H) and Stephanie McMahon. Unlike other WWE Home Video releases, the Workout Series has nothing to do with either professional wrestling nor WWE Studios, but rather physical fitness.

==Background==
Levesque serves as the trainer for the male-centric WWE Power Series while McMahon serves as the trainer for the female-centric WWE Fit Series. Both Workout Series videos take place at the   (12/02/2014) 

In their respective Workout Series videos, Levesque and McMahon discuss challenges they were facing (Levesques in-ring career winding down and McMahon going through three Pregnancy|pregnancies) and needing to get back into shape. Both hired renowned trainer Joe DeFranco to get back into peak physical condition and decide to design similar workout plans for the general public. Aside from archival workout clips, DeFranco doesnt appear in the Workout Series videos. 

Both Workout Series videos contain a warm-up video and a series of exercises, as well as a calendar for people to follow in order to get the proper fitness. 

==Reception== heel faction The Authority, of which Levesque and McMahon are a part of on WWE television.
 trainers and are willing to help the people watching get back into shape. Other reviews have also praised how with the exception of Dumbbell|dumbbells, the Workout Series doesnt require the purchase of fitness equipment.     Caliber Winfield of Renegade Cinema praised the series, saying it is a fantastic set for beginners, although more experienced body builders might find it lacking. 

==References==
 
 
 
 