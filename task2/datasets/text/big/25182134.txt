Peter Pan (1976 musical)
{{Infobox film 
| name           = Peter Pan
| image          = 
| caption        = 
| director       = Dwight Hemion 
| producer       = Dwight Hemion, Gary Smith
| writer         = Screenplay:  
| starring       = Mia Farrow Danny Kaye 
| music          =  Anthony Newley Leslie Bricusse
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = December 12, 1976
| runtime        = 
| country        = United States
| language       = English 
| budget         = 
| gross          =
}}
 Mary Martin version which had previously been televised many times on NBC. Instead, it featured 14 new and now forgotten songs, written for the production by Anthony Newley and Leslie Bricusse.  The story was adapted by Andrew Birkin (who would subsequently become a leading Barrie scholar) and Jack Burns.  Although it was an American production with two American stars, most of the cast was British.

This version of Peter Pan won an Emmy for Outstanding Individual Achievement in Childrens Programming for Jenn de Jouxs and Elizabeth Savels visual effects, and was nominated for Outstanding Childrens Special, however it was not rebroadcast. But it was featured in 2011 at the Paley Center in New York City as part of the New York Musical Theatre Festival. 

==Cast==

*Mia Farrow	... 	Peter Pan
*Danny Kaye	... 	Captain Hook and Mr. Darling
(alphabetically)
*Lynsey Baxter	... 	Jane
*Peppi Borza	...  Pirate Michael Crane	... 	Pirate
*Michael Deeks	... 	Curly Fred Evans	... 	Pirate Wendy
*John Gielgud	... 	Narrator George Harris	... 	Pirate Paula Kelly Tiger Lily
*Max Latimer	... 	Pirate
*Nicholas Lyndhurst	... 	Tootles
*Virginia McKenna	... 	Mrs. Darling
*Briony McRoberts	... 	Wendy Darling
*Joe Melia	... 	Starkey
*Peter OFarrell	... 	Nana
*Adam Richens	... 	Nibs
*Ian Sharrock	... 	John Darling Adam Stafford	... 	Michael Darling
*Tony Sympson	... 	Smee
*Jerome Watts	... 	Slightly

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 