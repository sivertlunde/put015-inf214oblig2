Memory (2006 film)
{{Infobox film
| name           = Memory 
| image          = 
| caption        = Film poster
| director       = Bennett Joshua Davlin
| producer       = Bennett Joshua Davlin Jesse Newhouse Anthony Badalucco
| starring       = Billy Zane Tricia Helfer Dennis Hopper  
| writer         = Bennett Joshua Davlin Anthony Badalucco
| narrator       = 
| music          = Anthony Marinelli
| cinematography = 
| editing        = Allison Grace
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
}}
Memory (also billed as mem-(o)-re and Memore) is a 2006 American techno-thriller film written by Bennett Joshua Davlin, and starring Billy Zane, Tricia Helfer and Terry Chen.

==Plot==
While lecturing in Brazil, Dr. Taylor Briggs an American authority on memory consults on a patient found deep in the Amazon. During the exam, Taylor is accidentally exposed to a mysterious substance which unlocks a series of memories in his brain. Memories that are not his. The memory of a killer who committed crimes before Taylor was even born. A killer who is much closer than you may think.

The mystery will rip open Taylor Brigg’s well crafted life, estranging his relationship with his best friend Dr. Deepra Chang, leading him into romance with a beautiful, enigmatic painter Stephanie Jacobs, straining his relationship with his mom’s best friend Carol Hargrave, causing him to question the surrogate father figure in his life, Dr. Max Lichtenstein.

==Writers comments==
 
“Memory is really about seeing through someone else’s eyes” says writer /producer /director/ novelist Bennett Davlin. “and doing it in a way that uses cutting edge science”. One of the minority investors, Dr. Robert Voogt, a leading head injury/ Alzheimer’s expert explains that “I was consulted on the film and loved the story so much I invested in the movie. All the science used is accurate.” 

“What attracted me most to the project was being able to see through someone else’s eyes” explains actor Billy Zane who catapulted to fame in the top grossing movie “Titanic”. “Taylor,” explains Zane “is a good guy who now has the memories of committing horrible acts. It opposites, a good and very bad person getting slammed together in one mind that made it most interesting.”  
 
  
“Deepra is the level headed one,” explains Terry Chen, “always using grounded science to explain away this whole thing. In a way he gets to be the audience second guessing Taylor.” “They grew up together, so these are best friends with history that goes all the way back to the baseball fields of their youth”. 

“Carol is a real loner”, explains two time Oscar nominee Ann-Margret, “a quiet, contained person who’s always been there for Taylor, but yet is always on the periphery of his life: standing in the background watching from afar.”  

“Max,” explains two time Oscar nominee Dennis Hopper, “is a guy whose wife has died and he’s sort of coasting through life a bit like a ghost. But it’s a really warm relationship that he has with Taylor—something genuine and real.” 

“Stephanie,” explains Tricia “has painted this image that is exactly like what Taylor is seeing she’s a woman with conflicts that run so deep; she can’t even quite touch them. It gives her character a constant state of tension which was something that made all her scenes so interesting”.  

== About the production ==
Bennett Davlin wrote the novel first with the intention of directing and producing the film. “These characters were so real to me that I just knew I had to bring them to the big screen”. 

“One of the most appealing things to me,” explains Billy Zane “is that so much of Taylor is internalized. He says very little, but we’re in his head the whole time.” 

“I hate dialogue,” co-writer Anthony Badalucco adds, “I think it’s much more interesting to see Taylor doing things that explain and explore his psyche than for him to babble his thought aloud in some exposition”.

After viewing George Lucas’s “Star Wars” at the age of 6, Bennett Davlin knew he wanted to direct films. By the time he had started his professional career as a screenwriter, he had made hundreds of films and written sixteen novels before he was seventeen years old. “By writing novels and making films, I have a profound respect for creating real worlds with real people and doing it as quickly and succinctly as possible. Books demand details. Films demand pacing. It’s a great combination.” 

“We have been very lucky to get such a marvelous cast together” says executive producer Robert J. Monroe. “I especially adore Ann-Margret who just lights up the screen. Dennis is amazing. And Billy is fantastic as Taylor. He’s in practically every scene of the film too.” 
“Memory turned out to be a difficult novel to interpret on the screen,” explains Davlin, “so I turned to my producing and writing partner Anthony Badalucco for help. He helped breathe a real breath of fresh air into the work.”

“We made a lot of changes,” explains Badalucco, “all the while trying to keep the audience in Taylor’s head. I wanted the audience to be one with him.”
 
“Davlin storyboards everything,” explains associate producer Jeannette Weinstein, “and then shoots the storyboards and adds sound to create a complete animatic. This film was totally organized and a pleasure to work on”. 

“I don’t like to waste people’s time,” explains Davlin, “and we were trying to make a small independent film look like a big budget movie. I also didn’t want to look like an idiot, standing around figuring out how to shoot or stage something.”

“All that preparation,” says cinematographer Peter Benison “gives this movie a polish that you just don’t find in independent film.”

“It’s like working with a young Alfred Hitchcock,” explains associate producer Jeannette Weinstein, “he lets his cast and crew in on the storyboards, so it’s a very quick and collaborative effort”.

“Because the material was based on a novel,” explains Terry Chen, “there was a lot to pull from that wasn’t even in the script. I’m talking about rich details that can only exist in a novel. But this whole shared history between these characters was so intimate and interwoven. It gave the cast so much to subconsciously pull from.”

Then there is that bathroom that gets wiped out by a tidal wave. “It was a nightmare,” explains screenwriter Anthony Badalucco, “the water tank had to be rebuilt and rebuilt. It was one of those technical things you just couldn’t control. But the shot is amazing when you see it on screen.”

“It’s a miracle we got that shot,” Davlin smiles, “hell, it’s amazing we finished the thing on that budget and still kept all the details I wanted to bring to the screen.”

“I love the lair that’s revealed at the end,” explains Weinstein, “it’s a totally different place, more of an art installation than a killer’s lair.” 

== Cast ==
*Billy Zane as Taylor Briggs
*Tricia Helfer as Stephanie Jacobs
*Ann-Margret as Carol Hargrave
*Dennis Hopper as Max Lichtenstein
*Terry Chen as Dr. Deepra Chang
*Dan Pelchat as Spectre 
*Emily Hirst as Bonnie McHale

== References ==
 

== External links ==
*  
*  

 
 
 
 
 