Bright Lights, Big City (film)
{{Infobox film
| name = Bright Lights, Big City
| image = Bright Lights Big City.jpg
| caption = Theatrical release poster
| director = James Bridges
| producer = Mark Rosenberg Sydney Pollack
| screenplay = Jay McInerney
| based on =  
| starring = Michael J. Fox Kiefer Sutherland Phoebe Cates Dianne Wiest
| music = Donald Fagen
| additional music = Rob Mounsey
| cinematography = Gordon Willis John Bloom Mirage
| distributor = United Artists
| released =   
| runtime = 107 minutes
| country = United States Japan
| language = English
| budget = United States dollar|$25 million
| gross = United States dollar|$16,118,077
}}
Bright Lights, Big City is a 1988 American drama film directed by James Bridges and starring Michael J. Fox, Kiefer Sutherland, Phoebe Cates, Dianne Wiest and Jason Robards, based on the novel by Jay McInerney, who also wrote the screenplay. It was the last film directed by Bridges before his death in 1993.

==Synopsis== New York model (Phoebe tabloid story pregnant woman in a coma. The movie captures some of the glossy chaos and decadence of the New York nightlife during the 1980s and also its look at a man desperately trying to escape the pain in his life.

==Cast==
*Michael J. Fox as Jamie Conway
*Kiefer Sutherland as Tad Allagash
*Phoebe Cates as Amanda Conway
*Dianne Wiest as Mrs. Conway
*Swoosie Kurtz as Megan
*Frances Sternhagen as Clara
*John Houseman as Mr. Vogel
*Tracy Pollan as Vicky
*Jason Robards as Mr. Hardy
*David Hyde Pierce as bartender at fashion show

==Production and development== Lost Weekend".  The studio agreed to make the film with Jerry Weintraub producing and Joel Schumacher directing. McInerney wrote a draft of the screenplay and, soon afterward, Schumacher started rewriting it.  Actor Emilio Estevez was interested in adapting it into a film.    He met with McInerney while he was still working on the screenplay. Tom Cruise was offered first refusal on the script while McInerney and Schumacher were attempting to capture the novels distinctive voice. McInerney, Cruise and Schumacher scouted locations in New York City and checked out the atmosphere of the club scenes described in the novel.  At one point, Judd Nelson, Estevez, and Rob Lowe were all considered for the role of Allagash. 

In 1985, Weintraub took the property to United Artists when he became chief executive there.  The film needed a new producer so Sydney Pollack and Mark Rosenberg took over. They hired writer Julie Hickson to write a script. Cruise and Schumacher grew tired of waiting for a workable script, but before they could be replaced, Weintraub left United Artists.  The project became entangled in a complicated settlement with the studio, months being lost before it finally stayed at United Artists. A decision was made to shoot the film in Toronto and cast an unknown in the leading role. 

Joyce Chopra was hired to co-write the script, with her husband Tom Cole, and also direct it. She had her agent send a copy of McInerneys novel to Michael J. Fox.  The actor won the leading role and, at his request, the part of Tad Allagash went to a Kiefer Sutherland.    Foxs casting increased the budget to $15 million and principal photography was moved to New York City.  The producers hired a crew, many of whom had worked with Pollack, while Chopra brought along the cinematographer from her first film, Smooth Talk, James Glennon. 

Fox had to be back in Los Angeles to start filming his TV series Family Ties by mid-July, giving Chopra only ten weeks to finish the film. It was rumored that she was indecisive, relying too much on consulting with Glennon and Cole, wasting time over a single shot.  It was also rumored that she panicked while shooting on the streets of New York as fans of Fox disrupted filming. Chopra claims, "I kept insisting that we take time each day to give the actors a chance to find their way, in spite of the panic caused all around us by the morning calls from United Artists asking if I had taken my first shot yet. Working collaboratively with my cameraman seemed to drive the producers into a sort of frenzy". 

Studio executives did not like what Chopra was shooting and, a week into filming, the studios chairman and its president of production flew from L.A. to New York to check on the film. Both executives had not read the script and were unaware of how different it was from the novel. 

McInerney claims that Cole wrote all the drugs out of the script while Cole claims that he did this on instructions from Pollack, who was worried that the film would hurt Foxs wholesome image with audiences. Cole recalls, "There was definitely pressure and concern at that time about how Michael was seen by America."  The studio announced that "a more experienced director" was needed as a result of an impending strike by the Directors Guild of America. On the short list of possible replacements were Ulu Grosbard, Bruce Beresford, and James Bridges.  Bridges received a call on a Friday that the film was in trouble, read the novel that night and flew to New York on Sunday. He saw Chopras footage and agreed to direct if he could start from scratch and hire Gordon Willis as his cinematographer. 

In seven days, Bridges wrote a new draft and got rid of six actors, casting instead Jason Robards, John Houseman, Swoosie Kurtz, Frances Sternhagen, and Tracy Pollan while keeping Sutherland and Dianne Wiest.  The new cast members read the novel because there was no script at the time. Chopra had worked on the film for only a month, which Fox described as "a rehearsal period, though it wasnt meant to be."  The strike forced the production to shoot in seven weeks and use McInerneys first draft, which Bridges liked the best.  Bridges worked on the script on weekends with McInerney, who was enlisted to help with revisions. The two agreed to share screenwriting credit but the Writers Guild of America decided to give it to McInerney only. 

The cocaine that Fox snorts in the film was a prop called milk sugar.  The filmmakers shot two different endings - one where Foxs character decides to start his life all over and an alternate one, to please the studio, where he has finished writing a novel to be called Bright Lights, Big City with a new girlfriend who is proud of what he has written. 

==Reception==
Bright Lights, Big City was released on April 1, 1988 in 1,196 theaters where it grossed USD $5.1 million on its opening weekend. The film went to make $16.1. million domestically, below its budget of $25 million.   

The film received mixed reviews from critics and has a 61% rating on   magazines Richard Schickel felt that the film, "arrives, however, looking like something that has been kicking around too long in the dead-letter office".   

===Home video===
A special edition DVD version of Bright Lights, Big City was released on September 2, 2008. In her review for the Washington Post, Jen Chaney wrote, "In the end, thats what is most disappointing about this DVD. What could have become a compelling look at a seminal novel of the 1980s and its rocky path through Hollywood ends up being a rudimentary release with a couple of decent commentary tracks and two forgettable featurettes".   

===Remake=== Gossip Girls co-creator Josh Schwartz is remaking an updated version of the film.

== Bright Lights, Big City: Original Motion Picture Soundtrack   ==
{{Infobox album  
| Name        = Bright Lights, Big City: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various artists
| Cover       =  Bright Lights, Big City (Soundtrack).jpg
| Alt         = 
| Released    = 1988
| Recorded    = 
| Genre       = 
| Length      = 49:14 Warner Bros.
| Producer    = Joel Sill  (Compilation producer) 
| Italic title = no
}}

{{Album ratings
| state = plain
 
| rev1 = Allmusic
| rev1Score =    
}}

=== Track listing ===
{{Track listing
| extra_column    = Artist
| writing_credits = yes
| total_length    = 49:14
| title1          = Good Love Prince
| Prince
| length1         = 5:12
 True Faith Stephen Morris, Bernard Sumner
| extra2          = New Order
| length2         = 5:54

| title3          = Divine Emotions
| writer3         = Jeffrey Cohen, Narada Michael Walden Narada
| length3         = 4:27
 Kiss and Tell
| writer4         = Bryan Ferry
| extra4          = Bryan Ferry
| length4         = 4:06

| title5          = Never Let Me Down Again|Pleasure, Little Treasure (Glitter Mix)
| writer5         = Martin Gore
| extra5          = Depeche Mode
| length5         = 5:36

| title6          = Centurys End
| writer6         = Donald Fagen, Timothy Meher
| extra6          = Donald Fagen
| length6         = 5:36

| title7          = Obsessed
| writer7         = Oliver Leiber
| extra7          = The Noise Club
| length7         = 5:40

| title8          = Love Attack
| writer8         = Shannon Dawson, G. Love Jay Konk
| length8         = 4:00

| title9          = Ice Cream Days
| writer9         = Jennifer Caron Hall, Alan Tarney Jennifer Hall
| length9         = 4:38
 Pump Up the Volume
| writer10        = Martyn Young, Steve Young
| extra10         = MARRS
| length10        = 4:06
}}

==References==
{{Reflist|refs=
   | work=Allmusic | publisher=Rovi Corporation | accessdate=30 November 2011}} 
}}

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 