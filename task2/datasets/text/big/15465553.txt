Aatish: Feel the Fire
 

{{Infobox film
| name           = Aatish: Feel the Fire
| image          = Aatish,_Feel_the_Fire.jpeg
| caption        = Sanjay Gupta
| producer       = G. P. Sippy, Vijay Sippy
| writer         = Robin Bhatt and Sujit Sen (story), Kamlesh Pandey (dialogue)
| starring       = Sanjay Dutt Aditya Pancholi Karishma Kapoor Raveena Tandon
| music          = Nadeem-Shravan
| cinematography = Najeeb Khan
| editing        = Afaq Hussain
| distributor    = Sippy Films
| released       =  June 17, 1994
| runtime        = 155 Minutes
| based on = A Better Tomorrow by Jon Woo
| country        = India
| language       = Hindi
| Budget         =
| preceded by    =
| followed by    =
| awards         =
| Gross          =
}}
 Sanjay Gupta and starring Sanjay Dutt, Aditya Pancholi, Karishma Kapoor and Raveena Tandon in lead roles. Other cast include Atul Agnihotri, Shakti Kapoor, Gulshan Grover, Kader Khan, Ajit Khan|Ajit, Tanuja, Ram Mohan, Vishwajeet Pradhan, Dinesh Hingoo, Tiku Talsania, Mushtaq Khan. The film is an unofficial remake of John Woos A Better Tomorrow

==Plot==
Baba and Avinash are brothers who lives in a shanty house with their widowed mother, who makes a living as a housemaid. When a stalker attempts to rape their mother, Baba knives him to death, and the three, along with an orphan named Nawab, take shelter with an underworld don named Uncle. Baba would like Avinash to study and become a better person, and in order to do this he decides to make crime his career. When Avinash completes his studies and wants to enroll himself in the police academy, Baba helps him monetarily by accepting his first contract killing. Avinash does complete his training at the police academy and soon becomes a police inspector. One of his first assignments is to be apprehend and arrest Baba and Nawab - much to his shock, as he had never associated his very own brother of having any criminal background. Avinash must now decide to proceed on with apprehending Baba and Nawab, or quit from the police force.

==Cast==
*Sanjay Dutt ...  Baba
*Aditya Pancholi ...  Nawab
*Raveena Tandon ...  Nisha
*Karisma Kapoor ...  Pooja (as Karishma Kapoor)
*Atul Agnihotri ...  Inspector Avinash Avi
*Shakti Kapoor ...  Sunny
*Gulshan Grover ...  Kaniya
*Ajit ...  Uncle (underworld don)
*Tanuja ...  Babas Mother
*Vishwajeet Pradhan ...  Gulam (Kanias brother)
*Kader Khan ...  Kadar bhai
*Ram Mohan ...  Police Commissioner
*Dinesh Hingoo ...  Aar Paar
*Tiku Talsania ...  Jarnail Singh
*Mushtaq Khan ...  Bhiku
* Ayman Sayyed ... Master Tinu

==Soundtrack==

The song "Hasratein Hain" was one of the most popular songs of the film and the year 1994 while "Kash Tum Mujhse" was also a hit.
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aa Aa Mere Dilruba"
| Kumar Sanu, Sapna Mukherjee
|-
| 2
| "Hasratein Hain Bahut Magar"
| Kumar Sanu, Sadhana Sargam
|-
| 3
| "Kaash Tum Mujhse Ek Baar Kaho"
| Kumar Sanu
|-
| 4
| "Khaate Hain Hum Kasam"
| Kumar Sanu, Alka Yagnik
|-
| 5
| "Dil Dil Dil Main Tere Pyar Mein"
| Jolly Mukherjee, Alka Yagnik
|- 6
|"Ya Mustafa" Jolly Mukherjee, Mukul Aggarwal, Alka Yagnik  
|- 
| 7
| "Baarish Ne Aag Lagayi"
| Udit Narayan, Alka Yagnik
|-
| 8
| "Dheela Pajama"
| Kumar Sanu, Alka Yagnik
|}

==Remake Anthology==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;" A Better Tomorrow (2010) (Korean cinema|Korean) 
|-
| John Woo || Sanjay Dutt || Joo Jin-mo
|- Kim Ji-yung
|}

==External links==
*  

 
 
 
 
 