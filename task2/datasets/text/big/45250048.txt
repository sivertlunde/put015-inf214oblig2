Perfect Hideout
{{Infobox film
| name           = Perfect Hideout
| image          = Perfect Hideout dvd cover.jpg
| caption        = DVD cover
| alt            = 
| director       = Stephen Manuel 
| producer       = Ica Souvignier Michael Souvignier Rolant Hergert 
| writer         = Andreas Brune Sven Frauenhoff  
| starring       = Billy Zane Cristian Solimeno Melinda Y. Cohen 
| music          = Eckart Gadow 
| cinematography = Oliver Staack 
| editing        = 
| studio         = Zeitsprung Entertainment
| distributor    = Acteurs Auteurs Associés, Indies Home Entertainment, Los Banditos Films, Red Ant Enterprises, Well Go USA Entertainment
| released       =  
| runtime        = 94 minutes  (DVD cut) 
| country        = Germany
| language       = English
| budget         = 
| gross          = 
}}

Perfect Hideout is a 2008 German action movie directed by Stephen Manuel and starring Billy Zane, Cristian Solimeno, and Melinda Y. Cohen.   

==Plot==
A loving German couple has big plans. Nick and Celia dream about a better life in another country. However, a Nicks partner from his criminal past is in no hurry to let him go—at least until Nick pays off all the debts. During a gas station robbery, Nick accidentally shoots a young policeman. Now it is really a big problem. Running from the police chase, the young people hide in the nearby posh villa. They take the owner hostage, who seems to be the only chance for them to stay alive and escape from the place of siege. But things get worse when Nick finds bodies of brutally killed people. So who is their hostage: a loving family man or a serial killer? 

==Cast==
*Billy Zane ... as Victor 
*Cristian Solimeno ... as Nick 
*Melinda Y. Cohen ... as Celia 
*Scarlett Sabet ... as Nadine
*Ken Bones ... as Roth

==References==
 

==External links==
* 
* 
* 

 
 
 