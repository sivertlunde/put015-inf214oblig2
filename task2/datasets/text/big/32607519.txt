Rahtree Reborn
{{Infobox film
| name           = Rahtree Reborn
| image          = Rahtree Reborn-p2.jpg
| caption        = Thai film poster.
| director       = Yuthlert Sippapak
| producer       = 
| writer         = Yuthlert Sippapak
| starring       = Mario Maurer Laila Boonyasak
| music          = 
| cinematography = 
| editing        = Tawat Siripong
| distributor    = Sahamongkol Film International
| released       =   
| runtime        = 90 minutes
| country        = Thailand
| language       = Thai
| budget         =
}}
Rahtree Reborn ( ) (also known as Buppah Rahtree 3.1) is a 2009 Thai  .  

==Plot==

Ten years passing by, Buppha is reincarnated as a young girl named Pla who is abandoned by her mother, leaving her with her barber stepfather, who often beats her in his anger. As a result, she becomes a problem child who is bullied by her classmates at school. One day, she takes a razor from her stepfathers barbershop and starts attacking people at school, after which she leaves and goes to Bupphas apartment, where she unexpectedly encounters a man who is masturbating. Pla is murdered by him and becomes a ghost which also haunts Bupphas apartment, and she awakes Buppha’s ghost and uses her to take her revenge against all men.
After word gets out that the apartment block is haunted, the unoccupied flats become an illegal casino, and the good-looking Rung, who has a sixth sense which enables him to see ghosts, moves in after his girlfriend breaks up with him. The death toll in the apartment block rises, and Rung and his friends are also chased by the girl’s ghost, but one day, Rung meets Buppha, who used to be his tutor when he was a kid, in the communal space in the building. Soon, he falls in love with her. However, Rang isnt aware that his crush indeed is the haunting and dangerous spirit that he needs to avoid. But he is too late and his sweet dream turns to nightmare as Buppha is now on the hunt again.

==Cast==
* Laila Boonyasak as Buppah Rahtree
* Mario Maurer as Rung
* Santisuk Promsiri as Father
* Somlek Sakdikul as Master Tong
* Nudtawat Saksiri as Pla
* Chantana Kittiyapan as Sister Three
* Kristine Vongvanij as Ying
* Aang Terdterng as Security Guard (as Suton Vechkama)
* Saichia Wongwirot as Neung
* Piya Chanasattu as Peud
* Supapit Kokphon as Moo
* Yosawat Sitiwong as Tee

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 