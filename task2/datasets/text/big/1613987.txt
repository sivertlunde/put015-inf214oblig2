Hairspray (1988 film)
:For the 2007 film of the same name, see Hairspray (2007 film)
{{Infobox film
| name = Hairspray
| image = Hairspray poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = John Waters
| producer = John Waters Robert Shaye Rachel Talalay
| writer = John Waters Divine Debbie Colleen Fitzpatrick Michael St. Gerard
| music = Kenny Vance
| cinematography = David Insley
| editing = Janice Hampton Palace Pictures
| distributor = New Line Cinema
| released =  
| runtime = 92 minutes  
| country = United States
| language = English
| budget = $2 million
| gross = $8,271,108 
}} romantic Musical musical comedy Divine (in Colleen Fitzpatrick, PG is X by the Motion Picture Association of America|MPAA.  Set in 1962 Baltimore, Maryland, the film revolves around self-proclaimed "pleasantly plump" teenager Tracy Turnblad as she pursues stardom as a dancer on a local TV show and rallies against racial segregation.

Hairspray was only a moderate success upon its initial theatrical release, earning a modest gross of $8 million. However, it managed to attract a larger audience on home video in the early 1990s and became a cult classic.     Most critics praised the film, although some were displeased  with the overall Camp (style)|campiness.
 Broadway musical Best Musical second film adaptation of the stage musical, was also released by New Line Cinema in 2007, which included many changes of scripted items from the original. The film also ranks #444 on Empire (film magazine)|Empire magazines 2008 list of the 500 greatest movies of all time. 

==Plot==
Tracy Turnblad (Ricki Lake) and her best friend, Penny Pingleton (Leslie Ann Powers), audition for The Corny Collins Show, a popular Baltimore teenage dance show based on the real-life Buddy Deane Show. Penny is too nervous and stumbles over her answers, and another girl, Nadine, is cut for being black (there is a Negro Day on the show on the last Thursday of every month, she is told). However, despite being overweight, Tracy is a strong enough dancer to become a regular on the show, infuriating the shows reigning queen, Amber Von Tussle (Colleen Fitzpatrick), a mean, privileged, beautiful high school classmate whose pushy stage parents, Velma (Debbie Harry) and Franklin Von Tussle (Sonny Bono), own Tilted Acres amusement park (based on Baltimores Gwynn Oak Amusement Park, where racial problems occurred). Tracy steals Ambers boyfriend, Link Larkin (Michael St. Gerard), and competes against her for the title of Miss Auto Show 1963, fueling Ambers hatred of her.
 interracial romance Joann Havrilla), brainwash her into dating white boys and oppose integration with the help of a quack Psychiatry|psychiatrist, Dr. Fredrickson (director John Waters). Seaweed later helps her break out of the house and run away. It is implied that she will never return, as she has finally broken free from her mother. 

Undeterred, Tracy uses her newfound fame to champion the cause of racial integration with the help of Motormouth Maybelle, Corny Collins (Shawn Thompson), his assistant Tammy (Mink Stole), and Tracys Agoraphobia|agoraphobic, slightly overbearing, and overweight mother, Edna (Divine (actor)|Divine). After a race riot at Tilted Acres results in Tracys arrest, the Von Tussles grow more defiant in their opposition to racial integration. They plot to sabotage the Miss Auto Show 1963 pageant by planting a bomb in Velmas bouffant hairdo. The plan literally blows up in Velmas face when the bomb detonates prematurely, resulting in the Von Tussles arrest by the Baltimore police after it lands on Ambers head. Tracy, who had won the crown but was disqualified for being in reform school, dethrones Amber after the governor of Maryland pardons her; Tracy then shows up at the competition, integrates the show, and encourages everyone to dance.

==Cast==
* Ricki Lake as Tracy Turnblad Divine as Edna Turnblad / Arvin Hodgepile
* Debbie Harry as Velma Von Tussle
* Sonny Bono as Franklin Von Tussle
* Jerry Stiller as Wilbur Turnblad
* Leslie Ann Powers as Penny Pingleton Colleen Fitzpatrick as Amber Von Tussle
* Michael St. Gerard as Link Larkin
* Clayton Prince as Seaweed J. Stubbs
* Ruth Brown as Motormouth Maybelle Stubbs
* Shawn Thompson as Corny Collins
* Mink Stole as Tammy Joann Havrilla as Prudence Pingleton  
* Alan J. Wendl as Mr. Pinky
* Toussaint McCall as himself
* John Waters as Dr. Fredrickson

;Council members
* Josh Charles as Iggy
* Jason Downs as Bobby
* Holter Graham as I.Q. Jones
* Dan Griffith as Brad
* Regina Hammond as Pam
* Bridget Kimsey as Consuella
* Frankie Maldon as Dash
* Brooke Stacy Mills as Lou Ann Levorowski
* John Orofino as Fender
* Kim Webb as Carmelita
* Debra Wirth as Shelly

;Special appearances
* Ric Ocasek as Beatnik cat
* Pia Zadora as Beatnik chick

==Production==
John Waters wrote the screenplay under the title of White Lipstick, with the story loosely based on real events. The Corny Collins Show is based on the real-life Buddy Deane Show, a local dance party program which pre-empted Dick Clarks American Bandstand in the Baltimore area during the 1950s and early 1960s.    Waters had previously written about "The Buddy Deane Show" in his 1983 book Crackpot: The Obsessions of John Waters.

Principal photography took place in and around the Baltimore area during the summer of 1987.    The school scenes were filmed at Perry Hall High School with set locations including the library, a first-floor English class, and the principals office.  In the scene set in the principals office, the Harry Dorsey Gough (see Perry Hall Mansion) coat-of-arms that once hung in the main lobby can be seen through the doorway.  The scenes set at Tilted Acres amusement park were filmed at Dorney Park in Allentown, Pennsylvania.

The film was Divine (actor)|Divines final film and his only film with Waters in which he didnt play the lead.  Originally, Divine was considered to play both Tracy Turnblad and her mother Edna.  Executives from New Line Cinema, the films distributor, discouraged this concept, and it was eventually dropped.  

===Deleted scenes=== scenes were cut while in post-production, some of which explained certain elements seen in the film. One involved Tracy breaking into the Von Tussles home, stealing Ambers hair bleach, and bleaching her hair in Ambers sink, thus explaining Tracys change of hair color later in the film. 
 Bob Shaye, the head of New Line, probably correctly, said, This doesnt work. What is this, a Luis Buñuel|Buñuel movie?   And he was probably right."  An alternate scene is instead included in which Amber claims to have seen a roach in Tracys hair, but she is presumed to be joking and/or lying.

The final deleted scene was a musical number which involved the teens performing an obscure 1960s dance called "The Stupidity" at the auto show just prior to Tracy being released from reform school, but again, Waters ultimately decided it wasnt appropriate, stating, " asically, I thought, you know, you dont want your leading man to look stupid right in the big finale." 

==Reception==
===Critical reception===
Hairspray received three stars from critics Gene Siskel and Roger Ebert. 

The film currently holds a 97% "fresh" rating on Rotten Tomatoes; it is Waters second-highest-rated film (behind Multiple Maniacs); the sites consensus states "Hairspray is perhaps John Waters most accessible film, and as such, its a gently subversive slice of retro hilarity." 

===Box office=== expanded to 227 theaters, where it grossed $966,672 from March 11–13. It ended its theatrical run with $8,271,108. 

===Awards===
The film was nominated for six Independent Spirit Awards, and the Grand Jury Prize at the Sundance Film Festival. 

==Other works==
===Broadway musical===
  Thomas Meehan Best Musical, in 2003. The show closed on January 4, 2009.

===2007 adaptation===
 
In 2006, New Line joined forces with Adam Shankman to adapt the Broadway show into a movie musical. The film was released July 20, 2007, starring John Travolta as Edna, Michelle Pfeiffer as Velma, Christopher Walken as Wilbur, Amanda Bynes as Penny Pingleton, Brittany Snow as Amber Von Tussle, Queen Latifah as Motormouth Maybelle, James Marsden as Corny, Zac Efron as Link, and newcomer Nikki Blonsky as Tracy. The film had a $75 million budget and earned over $200 million worldwide. 

==Soundtrack==
 
The soundtrack was released in 1988 by MCA Records. The album featured one original song by Rachel Sweet and eleven other songs mostly from the early 1960s by Gene Pitney, Toussaint McCall and The Ikettes, among others. Two songs, "You Dont Own Me" and "Mama Didnt Lie" came out in 1964 and 1963

;Additional songs
Other songs appear in the film, but are not on the soundtrack, due to licensing restrictions, because many of the songs listed were on the Cameo Parkway Label, a recording label owned by Allen Klein.

* "Limbo Rock" – Chubby Checker
* "Lets Twist Again" - Chubby Checker
* "The Banana Boat Song|Day-O" – Pia Zadora
* "Duke of Earl" – Gene Chandler
* "Train to Nowhere" – The Champs
* "Dancin Party" – Chubby Checker
* "The Fly" – Chubby Checker
* "The Bird" – The Dutones
* "Pony Time" – Chubby Checker
* "Hide and Go Seek" – Bunker Hill
* "Mashed Potato Time" – Dee Dee Sharp
* "Gravy (For My Mashed Potatoes)" – Dee Dee Sharp
* "Waddle, Waddle" – The Bracelets
* "Do the New Continental" – The Dovells
* "You Dont Own Me" – Lesley Gore
* "Lifes Too Short" – The Lafayettes

==Home media== New Line reissued the film on VHS in 1996.
 theatrical trailer. It was released on Blu-ray Disc|Blu-ray on March 4, 2014. 

==See also==
* Cross-dressing in film and television
* Racial integration
* African-American Civil Rights Movement in popular culture
* Multiculturalism

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 