Doppelgänger (1969 film)
 
{{Infobox film
| name           = Doppelgänger
| image          = Journeyfarsidesun.jpg
| alt            = A bold title at the base of the image reads "Journey to the Far Side of the Sun". An image above it depicts two Earths in space, spread apart with the Sun in between, and a spacecraft travelling from one of the planets to its counterpart. Figures in space suits line images to the left of the main picture.
| caption        = Film poster for US release, featuring alternative title Journey to the Far Side of the Sun
| director       = Robert Parrish
| producer       =   Gerry and Tony Williamson
| story          = Gerry and Sylvia Anderson
| starring       = Roy Thinnes Ian Hendry Lynn Loring Patrick Wymark
| music          = Barry Gray John Read
| editing        = Len Walter Century 21 Cinema
| distributor    = Rank Organisation (UK) Universal Pictures (US)
| released       = 27 August 1969 (US) 8 October 1969 (UK)
| runtime        = 101 minutes United Kingdom
| language       = English
}} mirror image of Earth.
 Century 21 Gerry and John Read resulted in his resignation from Century 21. 
 cult film. 
 A and, later, PG certificate from the BBFC. The film has had only a limited DVD run.

==Plot== West must be the first to send a mission to investigate the planet. With EUROSEC member states France and Germany unwilling to provide financial support, Webb obtains majority funding from NASA; American astronaut Colonel Glenn Ross (Roy Thinnes) and British astrophysicist Dr John Kane (Ian Hendry), the head of the Sun Probe project, are assigned to the mission.
 stasis with lander shuttle, which crashes in a mountainous region that is revealed to be Ulan Bator, Mongolia. When an air-sea rescue unit returns Ross and Kane, the latter critically injured, to the Space Centre, it is apparent that the Phoenix mission has come to an untimely end after three weeks and that the astronauts have returned to Earth.

Neuman and EUROSEC official Lise Hartman (Loni von Friedl) interrogate Ross, who denies that he aborted the mission. Shortly after, Kane dies from his injuries. Eventually, Ross concludes that he is not on Earth, but indeed on the unknown planet – a Counter-Earth that is a mirror image of his. (Signs of this reversal include a clock whose hands move anticlockwise, a tape decks reels that turn clockwise and an oscilloscope that scans from right to left. In addition, while driving at night, Ross almost collides with another vehicle that he believes to be on the wrong side of the road.)

Many at EUROSEC, including Rosss wife, Sharon (Lynn Loring), are baffled by the astronauts claims that all aspects of life on the planet are reversed. However, Webbs view starts to change when Ross demonstrates the ability to read aloud from a sign, without hesitation, when it is reflected in a mirror; Webb is later convinced of the truth when X-rays from Kanes post-mortem examination reveal that his internal organs are positioned on the "wrong" side of his body. Ross conjectures that the two Earths lie Parallel universe (fiction)|parallel, inferring that his counterpart from this world is experiencing similar events on the far side of the Sun.

Webb suggests that Ross recover the flight recorder from Phoenix and return to his Earth. EUROSEC builds a replacement for Dove designed to be compatible with the reversed technologies of Phoenix. Modifications include the reverse-polarisation of the electric circuits, although no one is certain that the differences between the two Earths extend to the direction of current. Ross christens the new shuttle Doppelganger, a German word denoting a duplicate of a person or object.
 automatic landing control. EUROSEC is unable to repair the fault from the ground, and Doppelganger crashes into a parked spacecraft. Ross is incinerated in the collision and a chain reaction destroys much of the Space Centre, killing personnel and destroying all records of Rosss presence on the Counter-Earth.

Many years later, an elderly and embittered Jason Webb, long since dismissed from EUROSEC, has been admitted to a nursing home. In his dementia, he spies his reflection in a mirror mounted in front of a window. Rolling forwards in his wheelchair, and reaching out to touch his reflected image, Webb crashes through the mirror and dies. 

==Production== B feature, Century 21 ]  studio was a big success story." 

===Writing===
With the assistance of scriptwriter  , it is the point of your death. Following that legend, clearly, I had to steer the film so that I could end it illustrating the meaning of that word." 

When Kanter expressed dissatisfaction with the draft, Gerry hired   as a result of his apparent insanity, while Ross regains consciousness to find that the accident has left him blind.  The return mission to Phoenix fails due not to an electrical fault, but to a structural defect in the second Dove module, which disintegrates in the atmosphere of the Counter-Earth with Kane trapped inside.  EUROSEC Headquarters is left intact, and Kanes funeral is attended by his wife, the Rosses and Jason Webb. 
 David Lane, Body and James Bond Casino Royale. Archer and Hearn 2002, p.&nbsp;174.  Anderson remembered Parrish as being "very ingratiating", stating that he "told us he loved the script and said it would be an honour to work with us. Jay Kanter gave Bob the thumbs up and we were in business." Archer and Hearn 2002, p.&nbsp;173.  Although the box office failure of Casino Royale had prompted Anderson to question Parrishs ability, he stated that Doppelgänger could not have been made without his recruitment:  "It wasnt a question of, Will we get on with him? or, Is he the right man? He was a name director, so we signed him up immediately." 

===Casting===
{| class="wikitable" style="font-size:100%; float:right; margin: 0.75em;" align="right" border="1"
|-
|style="text-align:center; background:#E5E4E2" colspan="2"|Supporting cast 
|- Character
|- Keith Alexander Keith Alexander||Flight Director
|- Peter Burton||Medical Technician 1
|- Anthony Chinn||Air-Sea Rescue Operator
|- Nicholas Courtney||Medical Technician 2
|- John Clifford||Gantry Technician
|- Peter van Bonn Delegate Mallory
|- Cy Grant||Dr Gordon
|- Alan Harris||Public Relations Photographer
|- Jon Kelley||Male Nurse
|- Annette Kerr||Female Nurse
|- Martin King Martin King||Dove Service Technician
|- Herbert Lom||Dr Kurt Hassler
|- Philip Madoc||Dr Pontini
|- George Mikell||Paris Delegate Clavel
|- Basil Moss||Monitoring Station Technician
|- Norma Ronald||Secretary Pam Kirby
|- Vladek Sheybal||Psychologist Dr Beauville
|- John Stone John Stone||London Delegate
|- Jeremy Wilkin||Launch Control Technician
|}
 walk on respiratory health, continuity purposes was too much." 
 The Avengers (1961–69) and, according to Anderson, "was always drinking",  performed the stunt sequence depicting the aftermath of the Dove crash while drunk: "...&nbsp;he was pissed as a newt, and it was as much as he could do to stagger away. Despite all that, it looked exactly as it was supposed to on-screen!" Archer and Hearn 2002, p.&nbsp;175.  In the draft script, Kanes first name is Philip, and he has a wife called Susan.  In scenes deleted from the completed film, a romance between Kane and Lise Hartman, a EUROSEC official portrayed by Austrian actress Loni von Friedl, is played out at Kanes villa and on a beach in Portugal. 
 The F.B.I. typecast as X certificate from the British Board of Film Censors (BBFC) for adult content, he replied, "We want to work with live artists doing subjects unsuitable for children."  For the final cut of the film, the original nude shots were replaced with softer alternatives depicting Sharon stepping into and out of a shower. 

The draft script describes Sharon as the daughter of a United States Senator, and she is said to be in a romantic affair with EUROSEC public relations officer Carlo Monetti.  In the completed film, Italian actor Franco De Rosa Archer and Hearn 2002, p.&nbsp;193.  briefly stars as Paulo Landi. The affair is implied in one scene but not explored further,  prompting Simon Archer and Marcus Hearn, authors of What Made Thunderbirds Go! The Authorised Biography of Gerry Anderson, to suggest that De Rosa starred in a role "all but cut from Doppelgänger". Archer and Hearn 2002, p.&nbsp;190.  In a deleted scene, on finding Paolo and Sharon in bed together at the Rosses villa, Glenn angrily ejects the couple from the room and throws them both into a swimming pool.  Archer and Hearn note an additional subplot concerning the Rosses attempts to conceive a child and the deceit of Sharon, who has been using birth control pills to inhibit pregnancy without Glenns knowledge. 

Completing the main cast, Patrick Wymark stars as Jason Webb, director of EUROSEC. Having selected him on the basis of his performance as John Wilder in the television series The Plane Makers (1963–65) and The Power Game (1965–70), Anderson stated that Wymarks acting impressed him as much as Hendrys, but also that his similar drinking habits resulted in slurred lines on set.  During the filming of one scene, Wymark "had to list these explanations&nbsp;... and on take after take he couldnt remember that two followed one. We had to do it over and over again."  Archer and Hearn identify Wymarks portrayal of Webb, a character described as "John Wilder (2069 model)" in publicity material,  as the dominant performance of the film.  The draft script describes Webb as a former British Minister of Technology, who is now romantically involved with his secretary, Pam Kirby. 

Among the supporting cast, George Sewell stars as Mark Neuman, a German Operations Chief in EUROSEC who uncovers Dr Hasslers dealing with Communist China and whose parallel self directs the interrogation of Ross after the Dove crash.  His surname in the draft script is Hallam.  Finally, Ed Bishop stars as David Poulson, a NASA official. Bishop replaced English actor Peter Dyneley, who had voiced characters for Thunderbirds (TV series)|Thunderbirds (1965–66), after the producers decided that Dyneley bore too much of a resemblance to Wymark and that scenes featuring both the characters of Poulson and Webb would confuse audiences. 

===Filming=== Antonio Salazar, Parrish fearing that the coup détat would cause the production of Doppelgänger to fall behind schedule.  Filming in Borehamwood, Hertfordshire used the exterior of Neptune House (now part of the BBC Elstree Studios) as a double for EUROSEC Headquarters in Portugal.  Heatherden Hall (part of the Pinewood complex) appears as the old Webbs nursing home. 

  and metallic materials, besides other modifications to the set, to realise the EUROSEC teleconference scene at a cost lower than that of filming with actual monitor screens. ]]

To create the illusion of the   onboard Phoenix are seen to be connected first to Ross and Kanes left wrists, then their right. 
 eyelines strengthen the audiences perception that each delegate is facing a camera rather than the other actors in the scene, and are in different locations around the world.  Archer and Hearn promote the teleconference scene as an example of how Anderson "proved once again that his productions were ahead of their time." 

During the course of the production, the creative styles of Anderson and Parrish came into conflict. Anderson remembered that on several occasions Kanter was called on to mediate: "  both knew how important the picture was to our careers, and we both desperately wanted to be in the big time." Archer and Hearn 2002, p.&nbsp;176.  During one session, Parrish refused to follow the shooting script, having determined independently that not all the scripted scenes were essential to the plot.  When Anderson reminded Parrish of his contractual responsibilities, the director announced to the cast and crew, "Hell, you heard the producer. If I dont shoot these scenes which I dont really want, dont need and will cut out anyway, Ill be in breach of contract. So what well do is shoot those scenes next!"  Anderson discussed how the production of Doppelgänger presented new challenges, explaining, "I had worked for so many years employing directors to do what I told them&nbsp;... Suddenly I came up against a Hollywood movie director who didnt want to play and we ended up extremely bad friends."  In his 2002 biography, Anderson stated that his sole regret about the film "  that I hired Bob Parrish in the first place."  Sylvia Anderson comments that Parrishs direction was "uninspired. We had a lot of trouble getting what we wanted from him." 

One dispute among the founders of Century 21 – Gerry and   a spacecraft will travel as straight as a die&nbsp;...   told me that people were not familiar with space travel and therefore they would expect to see this kind of movement." Archer and Hearn 2002, p.&nbsp;178.   Refusing to re-film the scenes on the basis that Parrishs instructions had precedence over Andersons, Read resigned from both Century 21 and the production of Doppelgänger at the Andersons and Hills request.  Anderson elaborated: "Clearly John was in a difficult position. I do now understand how he must have felt, but in my heart I feel he couldnt play a double role." 

===Effects===
  and special effects shots has been positive, and the designs of the Phoenix and Dove spacecraft have been praised.  The Doppelganger shuttle that Ross uses to return to Phoenix is identical to Dove.]]
 Century 21 Studios in Slough, Berkshire,  which had been prepared for filming on the last Supermarionation series, The Secret Service.  Supervising director Derek Meddings oversaw the shooting of more than 200 effects shots, including the destruction of EUROSEC Headquarters at the end of the film.  A six-foot (1.8&nbsp;m) Phoenix scale model, which emulated the design of the NASA multi-stage Saturn V rocket, had to be rebuilt after unexpectedly igniting and nearly injuring a technician.  For authenticity, the effects staff mounted the shots of the Phoenix lift-off outdoors in a section of the Century 21 car park so as to film against a genuine sky backdrop.   Archer and Hearn describe the sequence as "one of the most spectacular" of its kind produced by Century 21.  Sylvia Anderson, who considers is indistinguishable from a Cape Kennedy launch, comments that she is "still impressed by the magic of the effects. Technology has come a long way since the early Seventies, but Dereks effects have endured." 
 James Bond film Moonraker (film)|Moonraker, and praises his efforts all the more for the absence of computer animation in the late 1960s. 

===Post-production=== artificial eye. 

==Distribution==
 
 A certificate from the British Board of Film Classification (BBFC) on 26 March 1969,     dispelling rumours of an X rating and fulfilling the Andersons objective that Doppelgänger be suitable for children accompanied by adults.  To secure an A certificate, brief cuts were made to shots of contraceptive pills, shortening the running time from the original 104 minutes.    
 Odeon Cinema in Londons Leicester Square on 8 October 1969,  having premiered on 27 August in the United States.    On 1 November, it debuted in Detroit, Michigan, commencing a second round of presentations in American cinemas.  The film received a disappointing box office reception on general release. 
 Rank released the film under its original name in the UK and the rest of Europe.  The title Journey to the Far Side of the Sun was adopted in the United States and Australia,  since it had been determined by Universal that the audiences of these countries might not understand the meaning of the term "doppelganger". Archer and Nicholls 1996, p.&nbsp;138.  Simon Archer and Stan Nicholls, authors of Gerry Anderson: The Authorised Biography, concede that Journey to the Far Side of the Sun – which has superseded Doppelgänger as the more popular title  – provides a clearer explanation of the plot, but argue that it lacks the "intrigue and even poetic quality of Doppelgänger". 

===TV broadcasts===
Two prints of Doppelgänger in its original 35 mm film|35&nbsp;mm format, for UK release, are known to exist.    One is retained by the British Film Institute (BFI), the other by Fanderson, the official fan society dedicated to the Gerry Anderson productions.  The original prints of Doppelgänger position Ian Hendry before Roy Thinnes in the opening credits; in the Journey to the Far Side of the Sun format, Thinnes is billed before Hendry.  Certain UK prints alter the final scene featuring the old Jason Webb with the addition of a short voice-over from Thinnes in character as Ross, who is heard speaking a line that he says to Webb earlier in the film: "Jason, we were right. There are definitely two identical planets." 

In the UK, Doppelgänger has been aired on TV under the title Journey to the Far Side of the Sun and has been formatted accordingly.  Broadcasts have often contained inverted picture due to a mistake made in transferring the original print to videotape.  Prior to a screening in the 1980s, a telecine operator viewed the print and, being unfamiliar with the premise of the film, concluded that the scenes set on the parallel Earth had been reversed in error.  An additional "Flopped image|flop-over" edit restored the image to normal, which became the standard for all broadcasts but compromised the plot: if Doppelgänger is screened in this modified form, the viewer is led to conclude that the parallel Ross has landed on the non-parallel, normal Earth. 

===Home video=== G since its 1969 theatrical release,  with the 2008 home video release the BBFC re-rated Doppelgänger PG from the original A for "mild violence and language". 
 Region A on 7 April 2015 under the Journey to the Far Side of the Sun title. 

==Reception== cult status. speculative fantasy in the late 60s".    He expresses satisfaction with Thinnes and Wymarks performances, the characterisation (and the themes entailed, including adultery, infertility and corruption) and the "Fourth of July-style" special effects, calling the film "enigmatic". 

===1960s and 70s===
 
 New York magazine in November, Judith Crist introduced Doppelgänger as "a science-fiction film that comes up with a fascinating premise three-quarters of the way along and does nothing with it."    She praised the production as being "nicely gadget-ridden" and raising questions on the conflict between politics and science, but criticised the editing.  Variety (magazine)|Variety magazine cited a confusing plot, and related the crash of the Dove module to the coherence of the scriptwriting in its declaration that, "Astronauts take a pill to induce a three-week sleep during their flight. Thereafter the script falls to pieces in as many parts as their craft."   

In his 1975 work A Pictorial History of Science Fiction Films, Jeff Rovin stated that the film was "confusing but colourful", and commended it for its "superb special effects". Rovin 1975, p.&nbsp;223.  Although it was argued to be better than average for the genre in The Miami News in September 1969    and The Montreal Gazette in April 1972,    a December 1969 edition of the Pittsburgh Press dismissed it as "a churned out science-fiction yarn&nbsp;... Lets hope theres only one movie like this one", and ranked it among the worst films of the year.    The The Montreal Gazette review maintained that, although the quality deteriorates towards the end of the film, "until then its a reasonably diverting futuristic melodrama." 

===Post-1970s=== metaphysical ponderings".  Anderson also expressed concern about the editing, stating that every effects shot precedes another shot "with that Hornby Railways|Hornby factor, slowing up the narrative unnecessarily".  Doppelgänger is awarded a rating of three stars out of five, and is summarised as "an interesting journey with many rewards".  Glenn Erickson, commenting in 2008 on the website DVD Talk, argued that Doppelgänger "takes an okay premise but does next to nothing with it. We see 100 minutes of bad drama and good special effects, and then the script opts for frustration and meaningless mystery."  He complained of unappealing cinematography, comparing it to the premise of Thunderbirds (TV series)|Thunderbirds in so far as "people stand and talk a lot", while defining the script as being composed of "at least 60 percent hardware-talk and Exposition (literary technique)|exposition&nbsp;... How people move about – airplane, parachute, centrifuge – is more important than what theyre doing." 

  |width=17.5%|align=left|salign=right}}
 parallel Earth concept were squandered in the determination to turn the production into "an excuse to show cool rocket toys". 

Doppelgänger is given a rating of two-and-a-half stars out of five in a negative review published on the   never fell or the Nazis won World War II, here the shocking discovery is that people write backwards. Thats it."  Doppelgänger is only recommended for fans of the Anderson productions, and is considered "an occasionally interesting failure". 

 

Gary Westfahl of the webzine SF Site asserts that the use of a near-perfect parallel Earth is uninspired, referring to the setting as "the most boring and unimaginative alien world imaginable".    Among other reviews, TV Guide magazine describes Doppelgänger as a "strange, little film" with an "overwritten script", and considers the subplot concerning Dr Hasslers treachery to be distracting.    It awards a rating of two stars out of four.  To Chris Bentley, writer of episode guides on the Anderson productions, Doppelgänger is a "stylish and thought-provoking science-fiction Thriller (genre)|thriller".   
 Century 21, Typecasting is the lazy mans friend, and boy, were we typecast in Britain."  On her feelings about Doppelgänger, she commented in 1992, "I saw it on TV a couple of years ago and I was very pleased with it. I thought it came over quite well."   

===Interpretation=== Buzz and Mike did it better on TV." 

  film,  .  Critic  ." ]]
 Planet of the Apes established an unattainable standard for other films of the science-fiction genre.   Erickson argues that the film is inferior to 2001 for its depiction of a realistic "working future" in which humans remain attached to commercialism.  Comparing the visual style of Doppelgänger to that used by film director Stanley Kubrick, he notes similarities in the use of close-up eye shots and various "Psychedelia|psychedelic" images, regretting that "all these borrowings are fluff without any deeper meaning."  Film4s review describes the final scenes featuring the character of Jason Webb as "hell-bent on recreating the enigmatic finale of 2001 by using a mirror, a wheelchair and a tartan blanket." 

Martin Anderson discusses connections between Doppelgänger and other science-fiction films of the 1960s and 1970s, such as Solaris (1972 film)|Solaris, acknowledging a "lyrical" tone in the dialogue.  Ultimately, however, Doppelgänger "doesnt bear comparison with Kubrick or   Andrei Tarkovsky|Tarkovsky."  Comparing Doppelganger to 2001, Rovin writes that the effects of the former "  outshine" those of the latter." Rovin 1975, p.&nbsp;124.  He goes onto state that the film "attempts to kindle a profundity similar to that of   in its abstract philosophising about the dichotomy of dual worlds, but fails with a combination of meat-and-potatoes science fiction and quasi-profound themes." Rovin 1975, p.&nbsp;127.  He suggests that it is "neither a kids film nor a cult film", but rules that "the elements that comprise the finished effort are more than individually successful." 
 Pod People, an extraterrestrial species with the power to create doppelgangers that are nearly indistinguishable from humans.   

===Legacy===
Despite the polarised critical reception and commercial failure of Doppelgänger,  .    UFO featured actors, costumes, props, locations and music that had previously appeared in Doppelgänger. Archer and Hearn 2002, p.&nbsp;188.  Of the films cast,   except Alexander, who had voiced characters for the penultimate Supermarionation series, Joe 90. With 11 other cast members, all but Grant and King appeared in at least one episode of UFO, in which Bishop had in the lead role of Commander Ed Straker. 
 Zephyr Zodiac) Archer and Nicholls 1996, p.&nbsp;146.  and jeeps (adapted from British Leyland Mini Mokes) were also re-used.  Neptune House, one of the filming locations for Doppelgänger, became the face of the Harlington-Straker Film Studios where the SHADO Organisation is headquartered.  Tracks from Barry Grays score that were recycled for UFO included "Sleeping Astronauts" and "Strange Planet", the latter serving as the ending theme music.  The teleprinter images that served as the focus of the films titles formed a creative element that was imitated in the opening titles of UFO. Archer and Hearn 2002, p.&nbsp;192. 

In a retrospective of Andersons career published on the  -induced imagery and   of 2001." 

==See also==
 
*Another Earth, a 2011 film with a similar premise The Stranger, a 1973 TV film with a similar premise
*Through the Looking-Glass, an 1871 novel that reverses many plot elements of its precursor, Alices Adventures in Wonderland (1865)
 
==References==
 

;Bibliography
 
*  
*  
*  
*  
*  
*  
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 