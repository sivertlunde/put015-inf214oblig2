The Wild Man of Borneo (film)
{{Infobox film
| name           = The Wild Man of Borneo
| image          = The Wild Man of Borneo poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert B. Sinclair 	
| producer       = Joseph L. Mankiewicz
| screenplay     = Waldo Salt John McClain 
| based on       =  
| starring       = Frank Morgan Mary Howard Billie Burke Donald Meek Marjorie Main Connie Gilchrist Bonita Granville Dan Dailey David Snell
| cinematography = Oliver T. Marsh  Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Wild Man of Borneo is a 1941 American comedy film directed by Robert B. Sinclair and written by Waldo Salt and John McClain. The film stars Frank Morgan, Mary Howard, Billie Burke, Donald Meek, Marjorie Main, Connie Gilchrist, Bonita Granville and Dan Dailey. The film was released on January 24, 1941, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Frank Morgan as J. Daniel Dan Thompson
*Mary Howard as Mary Thompson
*Billie Burke as Bernice Marshall
*Donald Meek as Professor Charles W. Birdo
*Marjorie Main as Irma
*Connie Gilchrist as Mrs. Evelyn Diamond
*Bonita Granville as Francine Frankie Diamond
*Dan Dailey as Ed LeMotte
*Andrew Tombes as Doc Dunbar
*Walter Catlett as Doc Skelby
*Joseph J. Greene as Mr. Robert Emmett Ferderber
*Phil Silvers as Murdock

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 