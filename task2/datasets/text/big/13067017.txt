Little Red Flowers
{{Infobox film
| name         = Little Red Flowers
| image        = Little Red Flowers poster.JPG
| caption      = Promotional poster
| writer       = Ning Dai Zhang Yuan Wang Shuo (novel) Zhao Rui Li Xiao Chen Li
| director     = Zhang Yuan
| producer     = Li Bo Wen Allen Chan Zhang Yuan Marco Mueller Wang Shuo
| editing      = Jacopo Quadri Yang Tao
| distributor  = Fortissimo Films
| released     =  
| runtime      = 91 minutes
| country      = China
| language     = Mandarin Carlo Crivelli
| budget       =
}} Chinese film directed by Zhang Yuan. The film was a co-production between Chinas Beijing Century Good-Tidings Cultural Development Company LTD and Italys Downtown Pictures. The Dutch company, Fortissimo Films handled worldwide sales.

The film, based on author Wang Shuos Autobiographical novel|semi-autobiographical novel, Could Be Beautiful, follows a young four-year-old boy, Fang Qiang Qiang, at a kindergarten boarding school. Deposited into a world that demands conformity (rewarded by the titular little red flowers), Qiang suffers for his bullying.
 I Love You. 

==Cast==
*Dong Bo Wen - Fang Qiang Qiang, the four-year-old protagonist of the film Zhao Rui - Miss Li, head teacher and disciplinarian Li Xiao Feng - Miss Tang, a younger, more kindly teacher Chen Li - Principal Kong

==Reception== Sundance (as part of the World Cinema competition), and Cannes where it was in-competition.

==Notes==
 

==External links==
* 
* 
* 
*  official site for Little Red Flowers

 

 
 
 
 
 
 
 
 

 

 