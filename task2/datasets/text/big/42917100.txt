Chimères (film)
{{Infobox film
| name           = Chimères
| image          = File:Chimères movie poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Olivier Beguin
| producer       = 
| writer         = Olivier Beguin, Colin Vettier
| screenplay     = 
| story          = 
| based on       =  
| starring       = Jasna Kohoutova, Yannick Rosset, Catriona MacColl
| narrator       = 
| music          = 
| cinematography = Florian N. Gintenreiter
| editing        = Olivier Beguin
| studio         = Chaoticlock Films, VM Broadcast Services Global
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Switzerland
| language       = French
| budget         = 
| gross          = 
}}
Chimères (English: Chimeras) is a 2013 drama/horror film and the feature film directorial debut of Olivier Beguin. The movie had its world premiere on July 5, 2013 at the Neuchâtel International Fantastic Film Festival, where it won a special mention for best feature film.  It stars Yannick Rosset as a young man that ends up contracting vampirism after receiving a blood transfusion.

==Synopsis==
Alex (Yannick Rosset) and Livia (Jasna Kohoutova) are a young couple on vacation in Romania. Their trip is uneventful until Alex severely injures himself while drunk and requires a trip to the hospital. His injuries necessitates a blood transfusion and Alex initially seems like he will be fine. However when he and Livia return home, Alex begins to develop strange new symptoms. He has an all new aversion to light, he is unable to eat anything, and develops insomnia. Alex and Livia initially believe this to be brain damage, but they soon discover the reality of the situation: that Alex is turning into a vampire.

==Cast==
*Jasna Kohoutova as Livia
*Yannick Rosset as Alex
*Catriona MacColl as Michelle
*Ruggero Deodato as Butcher Paulo Dos Santos as Fred
*Derek Robin as Doctor

==Production==
Beguin had initially planned for his first feature film to be a "mix of thriller and road-movie" but found that the concept would be more expensive than hed expected.    He chose to instead direct a monster movie that centered around vampirism, as it would fit both his budget and the type of story he wanted to tell.  The movies plot was originally plotted out as a horror comedy, but Beguin soon found that it worked well as a love story.  Rosset was intrigued by how his character was written, as Alex sees himself as violent and beastly when he looks in the mirror "but in reality he’s just sick, he’s simply tired, injured". 

==Reception==
Fangoria and Twitch Film both gave Chimères positive reviews,  and Twitch Film praised it for "really   how to play to its audience. Especially if said audience is seeing it at the late show, beer in hand and thirsty for blood."  In contrast, Dread Central rated the film at two and a half blades, stating that ot was a "respectable effort at doing something different with the genre, but those looking for a much more well-rounded and confident approach would be better served checking out Scott Leberechts excellent Midnight Son instead." 

===Awards===
*Special Mention for Best Feature Film at the Neuchâtel International Fantastic Film Festival (2013, won) 
*Narcisse Award for Best Feature Film at the Neuchâtel International Fantastic Film Festival (2013, nominated)
*Grand Prize of European Fantasy Film in Silver at the Sitges - Catalonian International Film Festival (2013, nominated)
*Festival Trophy for Best Editing at the Screamfest Horror Film Festival (2013, won) 
*International Fantasy Film Special Jury Award for Features at Fantasporto (2014, won)   
*International Fantasy Film Award for Best Film at Fantasporto (2014, nominated)
*Best Director Award at Horrorant Film Festival (2014, won)
*Best Actress Award at Horrorant Film Festival (2014, won)

==References==
 

==External links==
*  

 
 
 
 
 
 