Diabolical Tales
  comedy short short film alternate history based around conspiracy theories that involves some notable figures from the recent world history, yet has very tongue-in-cheek dry humor based partly around the low-budget limitations the series was obviously produced with.

== Background ==
Diabolical Tales was written, edited and directed by Brandon Kane, and produced by Kane and Don Gerron.  The original score is by Troy Sterling Nies.  The sound design is by James T. Rieder.  The visual effects and animation is by Don Gerron. The first three of the six installments were filmed simultaneously.  Diabolical Tales: Part I has a running length of 42 minutes and was completed in 2005. Diabolical Tales: Part II has a running length of 47 minutes and was completed in August 2007. Diabolical Tales: Part III runs 54 minutes and was completed in September 2007.

== Plot ==
Inspired by low-budget sci-fi films,  .  Rookie FBI Agent Cooper and his partner Agent Thompson are on a stakeout when they encounter a mysterious man dressed in a black cloak.  Minutes later, Agent Thompson has been vaporized by the villains "electro-incinerator" and Agent Cooper has been stunned unconscious.  He is teamed up with a cryptic US government agent known only as Operative-132 who works for the newly formed National Security Agency.  Together they set out to put the stops on the evil Zong, a saboteur who hails from an ancient, hidden underground empire called Agartha.

Diabolical Tales: Part II is set in December 1954, two years after the events of Diabolical Tales: Part I. A group of evil hench-women, led by the partially deaf arch-villain Zerg (Sparky Schneider), are sent up from their underground fiefdom to capture the Sapphire of Agartha, a mystical relic that was stolen and handed over to heroes Agent Cooper (Brian Bedell) and Operative-132 (Mike Larose).

Diabolical Tales: Part III is set in July 1955, and picks up with Agent Cooper as his wife Kate (Rachel Knutton) is kidnapped. Meanwhile, the evil men from within the earth, this time led by the cunning and maniacal Zerrath (Brian Van Kay), launch a two-pronged attack against the surface-dwellers in an effort to retrieve the still-missing Sapphire of Agartha and conquer the world.  The events that unfold will solidfy Agent Coopers destiny.

== Cast ==
Part I
*Brian Bedell — Agent Cooper
*Mike Larose — Operative-132
*S. Matthew Aod — Zong
*Rachel Knutton — Kate Cooper
*Don Gerron — Master Zun
*Ted Cunningham — FBI Assistant Director Smith
*Brian Van Kay — Dr. Ernst Schafer
*Joe Mahoney — Agent Thompson
*Ken Wester — Dr. Helmut
*Erika L. McCauley — AEC Employee
Part II
*Brian Bedell — Agent Cooper
*Mike Larose — Operative-132
*Sparky Schneider — Zerg Kris Williams — Zella
*Rachel Knutton — Kate Cooper
*Don Gerron — Master Zun/Agent Nicholson
*Ted Cunningham — FBI Assistant Director Smith
*Michelle Whitaker — Ziva
*Lara Schneider — Zeena
*Melissa Pare — Zera
*Jared Alcorn — Zellor
Part III
*Brian Bedell — Agent Cooper
*Brian Van Kay — Zerrath
*Rachel Knutton — Kate Cooper
*Juliette Rose — Zella
*Ted Cunningham — FBI Assistant Director Smith
*Don Gerron — Master Zun/Zorak
*Tom Weaver — Operative-1138
*Steve Lopresti — Agent White
*Mike Larose — The Ghost of Operative-132
*Matt Knutton — Zenab/G-Man #1
*Ken Svehla — G-Man #3
*Christian Wheeler — G-Man #4

== DVD information==
Part I
*Schlock Treatment! The Secret History of the Men From Within the Earth documentary
*The Tales Begin: The Making of Diabolical Tales Part I documentary
*The Tales Continue: Diabolical Tales Part II featurette
*12 Diabolical Tales Part I Web-Movies
Part II
*The Hunt for the MacGuffin: The Making of Diabolical Tales Part II documentary
*Stunts, Schlock-Style: The Fights of Diabolical Tales featurette
*...And The Plot Thickens: Diabolical Tales Part III featurette
*12 Diabolical Tales Part II Web-Movies
Part III
*The End of the Beginning: The Making of Diabolical Tales Part III documentary
*Score, Schlock-Style: The Music of Diabolical Tales featurette
*Diabolical Unleashed! featurette
*12 Diabolical Tales Part III Web-Movies

==External links==
*   Official web site
*   Internet Movie Database page
*   Internet Movie Database page
*   Internet Movie Database page
*  Vimeo Channel page

 
 
 