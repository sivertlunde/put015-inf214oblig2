Allan Quatermain and the Temple of Skulls
{{Infobox film
| name           = Allan Quatermain and the Temple of Skulls
| image          = quartermainskulls.jpg
| caption        = US DVD cover Mark Atkins
| producer       = David Michael Latt David Rimawi Paul Bales
| writer         = David Michael Latt
| based on       =   Christopher Adamson Sanaa Lathane Daniel Bonjour Wittly Jourdan
| music          = Mark Atkins
| editing        =
| distributor    = The Asylum
| released       =  
| runtime        = 90 minutes
| country        = United States Zulu
| budget         = 
| gross          =
}} American studio The Asylum. The film follows the adventures of explorer Allan Quatermain, and was filmed entirely on location in South Africa.  

== Overview ==

The film was released shortly after the premiere of Indiana Jones and the Kingdom of the Crystal Skull, and while the film contains some elements similar to Crystal Skull (Quatermain himself resembles Indiana Jones on the DVD cover), the film itself is a loose adaptation of the novel King Solomons Mines by H. Rider Haggard.

== Plot ==
 American expedition in search of a fabled treasure deep within unexplored Africa.

Throughout the film, Quatermain must avoid hidden dangers, violent natives and other unseen traps during their quest for the treasure of the Temple of Skulls, travelling by train, river and air to reach his goal, while being pursued by rival treasure-seekers and unfriendly natives who wish to sabotage his expedition.

== Main cast == Sean Michael - Allan Quatermain Christopher Adamson - Hartford
*Natalie Stone - Lady Anna
*Daniel Bonjour - Sir Henry
*Wittly Jourdan - Umbopa

== References ==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 