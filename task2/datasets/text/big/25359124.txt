The Infidel (2010 film)
{{Infobox film
| name           = The Infidel
| image          = The Infidel.jpg
| caption = Theatrical release poster 
| director       = Josh Appignanesi
| producer       = Arvind Ethan David
| writer         = David Baddiel
| narrator       = 
| starring       =  
| music          = Erran Baron Cohen
| cinematography = Natasha Braier
| editing        = Kim Gaster
| studio         =  
| distributor    = Revolver
| released       =  
| runtime        = 105 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = £1,748,853 
}} British Muslim who goes through an identity crisis when he discovers he was adopted as a child and born to a Jewish family. 

==Plot== James Floyd), and occasionally drinks alcohol. His son, Rashid, wishes to get married to Uzma, but he and Uzma need the blessing of her devout Muslim cleric stepfather, Arshad Al-Masri (Yigal Naor), whose actions and beliefs have earned him the contempt of many British Muslims. Mahmud reluctantly agrees to put on the act of devout Muslim for the occasion. 

Things change when Mahmud, while clearing out his recently deceased mothers house, stumbles across an adoption certificate. Mahmud learns he was actually adopted by his Muslim parents when he was two weeks old, his birth parents are Jewish, and his real name is Solomon "Solly" Shimshillewitz. This comes as a shock to Mahmud, who is somewhat anti-Semitic, exemplified by his relationship with his American Jewish neighbour, Leonard "Lenny" Goldberg (Richard Schiff). His wife suspects his strange behaviour may be due to Mahmud being gay and speaks to the imam of their local mosque. The imam tells Mahmud that the Koran doesnt permit a man to sleep with another man.

During an argument with Lenny, Mahmud lets slip his religion and his real name, and Lenny mentions a similarity to the name, Isaac "Izzy" Shimshillewitz, a local man, who may be Mahmuds biological father. Mahmud tracks his father to a Jewish old age home. He tries to visit, but a rabbi (Matt Lucas) guarding Izzys soul refuses him entry, saying it would be a shock for Izzy, a Jewish man, to see his son, a Muslim, and advises him to learn to act more like a Jew if he desires to see his father. 
 Jewish Shma or name the Five Books of Moses in Hebrew.

On the way home, Mahmud and Lenny have a bitter argument and Mahmud storms off, vowing to tell his family the truth immediately, but when he gets home, he sees that Arshad, Uzma and their friends are already there on a visit. Arshad, impressed with Mahmuds supposed devotion to Islam after having seen him burn the kippah on a TV news broadcast, proudly gives his blessing to Rashid and Uzmas union, but the police arrive, along with the media and a crowd of angry Jews and supportive Muslims, to arrest Mahmud for burning the kippah (though Arshad publicly insinuates that the Jews have bribed the police). In desperation, he yells out in front of everyone that he is Jewish, exonerating him of the crime. A disgusted Arshad leaves with Uzma and his friends.

Mahmuds family leaves him for his dishonesty, one of his colleagues at work resigns, and he starts drinking. He becomes almost suicidal but is found and rescued by Lenny, who saw his announcement on the news. Mahmud angrily goes to the old age home and demands to see his father, but learns that his father has already died. Heartbroken, Mahmud is allowed inside Izzys room where he finds a video of his announcement in Izzys video machine, which Lenny had sent Izzy. Mahmuds only solace is a sticker on the video with the name "Solly" on it, indicating that even after all these years, Izzy still remembered his long-lost son.

Mahmud appears at Arshads next rally and delivers a speech on behalf of himself, Jewish citizens and Muslims. Mahmud also tells the crowd of another discovery which hes made: Arshad is actually Gary Page (whose own parents were Scientologists), who staged his own death following his fall from fame after a racist remark, resurfacing much later with a whole new identity as a devout Muslim cleric. Arshad escapes from the rally, comically dressed in his old Gary Page clothes. At the films conclusion, Rashid and Uzma are married in a Pakistani Interfaith wedding, attended by both Muslims and Jews. Lenny - his friendship with Mahmud sustained - has taken a job with the mostly-Muslim taxi firm at which Mahmud works.

==Cast==

* Omid Djalili as Mahmud Nasir/Solly Shimshillewitz
* Yigal Naor as Arshad Al-Masri
* Matt Lucas as Rabbi Amit Shah as Rashid
* Soraya Radford as Uzma
* Archie Panjabi as Saamiya Nasir
* Richard Schiff as Lenny Goldberg
* Miranda Hart as Mrs. Keyes David Schneider as Monty
* Tracy-Ann Oberman as Montys wife
* Mina Anwar as Muna James Floyd as Gary Page
* Shobu Kapoor as Kashmina
* Sartaj Garewal as Wasif
* Ricky Sekhon as Hazeem

==Production==
David Baddiel wrote The Infidel because he has "always been a fan of life-swap comedy (Big, Trading Places, etc)";
he "think  that people are terrified about race and religion, especially issues surrounding Muslims and Jews, and when people are terrified, what they really should do is laugh"; and he "love  Omid Djalili and his big funny face.   hoping that people recognise that underneath the comedy, the message of the film is one of mutual tolerance: if not,   hoping to find a new identity." 

BBC Films helped develop the films script. They withdrew shortly after the Russell Brand Show prank telephone calls row|"Sachsgate" scandal, in which the BBC were criticised for offensive content. 

== Release ==
The film was released 9 April 2010 in the United Kingdom. Distribution rights to the film were sold to 62 different countries around the world, including many Arab and Muslim countries such as the United Arab Emirates, Bahrain, Qatar, Lebanon, Oman, Iran and Saudi Arabia. The film has been shown in Iran but was not picked up by Israeli distributors.   

== Critical reception ==
65% of the 23 approved Rotten Tomatoes critics gave the film a positive review. 

==Bollywood remake==
The film was remade in India with the title of Dharam Sankat Mein (Crisis of faith) where the basic premise remains same with a man raised by a Hindu family finds out he had been born into a Muslim family.

==References==
 

== External links ==
*  
*  
*   in TC Jewfolk magazine

 
 
 
 
 
 
 
 
 