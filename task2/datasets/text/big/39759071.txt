The Trunks of Mr. O.F.
{{Infobox film
| name           = The Trunks of Mr. O.F.
| image          = 
| image_size     = 
| caption        = 
| director       = Alexis Granowsky 
| producer       = Mark Asarow   Hans Conradi   Ernst Nölle
| writer         = Alexis Granowsky   Hans Hömberg   Léo Lania
| narrator       = 
| starring       = Alfred Abel   Peter Lorre   Harald Paulsen   Hedy Lamarr
| music          = Karol Rathaus   Kurt Schröder Paul Falkenberg   Conrad von Molo
| cinematography = Heinrich Balasch   Reimar Kuntze
| studio         = Tobis Film
| distributor    = Deutsche Lichtspiel-Syndikat 
| released       = 2 December 1931
| runtime        = 80 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German comedy Viennese premiere on 4 June 1932. 

It received a mixed reception from critics who were divided over its non-realism (arts)|realist, fantastical elements.  The films screenwriter Léo Lania intended it to be a critique of capitalism. 

==Synopsis==
When thirteen large suitcases arrive at a hotel in a small town, labeled as belonging to a mysterious O.F., they provoke curiosity and speculation. Rumours begin to spread like wildfire that they belong to a millionaire. Although O.F. fails to appear, his anticipated arrival is a catalyst for a series of dramatic changes in the town.

==Cast==
* Alfred Abel as Bürgermeister 
* Peter Lorre as Redakteur Stix 
* Harald Paulsen as Baumeister Stark 
* Ludwig Stössel as Hotelier Brunn 
* Hedy Lamarr as Helene - Tochter des Bürgermeisters  Margo Lion as Viola Volant 
* Ilse Korseck as Mayors Wife 
* Liska March as Eve Lune 
* Gaby Karpeles as Gehilfin von Eve Lune 
* Hadrian Maria Netto as Friseur Jean 
* Hertha von Walther as Jeans Frau  Franz Weber as Schneider Dorn 
* Maria Karsten as Frau Dorn 
* Alfred Döderlein as Alexander, Sohn des Bürgermeisters 
* Bernhard Goetzke as Prof. Smith 
* Josefine Dora as Jeans Schwiegermutter 
* Friedrich Ettel as Apotheker 
* Aenne Goerling as Zimmervermieterin Beck 
* Rudolf Hofbauer as Filmregisseur 
* Arthur Mainzer as Filmdirektor 
* Meinhart Maur as Arzt
* Aribert Mog as Assistant von Stark 
* Max Ralph-Ostermann as Maître dHôtel 
* Henry Pleß as Bankdirektor 
* Gertrud Ober as Sekretärin des Bankdirektors 
* Eduard Rothauser as Reisebürodirektor 
* Elsa Wagner as Sekretärin des Reisebürodirektors 
* Hans Hermann Schaufuß as Hausdiener Peter 
* Franz Schönemann as Agenturdirektor 
* Gerta Rozan as 1. Sekretärin des Agenturdirektors 
* Ursula Urdang as 2. Sekretärin des Agenturdirektors 
* Fanny Schreck as Dorns Schwiegermutter 
* Franz Stein as Gesangslehrer 
* Ernst Wurmser as Dirigent der Feuerwehrkapelle 
* Maria Forescu as Strafende Mutter 
* Eva LArronge as Sekretärin des Parfümeriefabrikanten 
* Hanns Waschatko as Parfümeriefabrikant

==References==
 

==Bibliography==
* Barton, Ruth. Hedy Lamarr: The Most Beautiful Woman in Film. University Press of Kentucky, 2010.

==External links==
* 

 
 
 
 
 
 
 

 