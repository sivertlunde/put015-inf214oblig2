Soft Fruit
{{Infobox Film
| name           = Soft Fruit 
| image_size     = 
| image	=	Soft Fruit FilmPoster.jpeg
| caption        = 
| director       = Christina Andreef
| producer       = Helen Bowden Jane Campion (executive producer)
| writer         = Christina Andreef
| narrator       = 
| starring       = Jeanie Drynan Russell Dykstra Sacha Horler Genevieve Lemon Linal Haft
| music          = Antony Partos
| cinematography = Laszlo Baranyai
| editing        = Jane Moran
| distributor    = Fox Searchlight Pictures
| released       = 1999
| runtime        = 101 minutes
| country        = Australia
| language       = English
| budget         = 
| gross = A$598,704 (Australia)
| preceded_by    = 
| followed_by    = 
}}

Soft Fruit is a 1999 drama about a dying mother, and her children who come together to fulfill her last wishes. It is an Australian American co-production produced by Australian filmmaker Jane Campion and directed by Christina Andreef.

Andreef later talked about the themes of the film:
 As you grow older, its so difficult to stay in relationship with your brothers and sisters and your parents. When you get into your thirties and forties, paths are dividing. Thats not the case in countries like Spain. They remain very attached to their families. We dont so much. So Soft Fruit is about that family struggle. You think you dont care when you have a fight and fall out. Then you realise that youre suffering profoundly because youre on the outer. Its about that struggle to get back on the inner, on the inside.  

==Cast==
*Jeanie Drynan as Patsy
*Russell Dykstra as Bo
*Linal Haft as Vic
*Genevieve Lemon as Josie
*Sacha Horler as Nadia
*Alicia Talbot as Vera

==Awards==
  Australian Film Institute Award for Best Actor - Russell Dykstra (winner) Australian Film Institute Award for Best Supporting Actress - Sacha Horler (winner)
*Australian Film Institute Award for Best Film (nominated) Australian Film Institute Award for Best Actress - Jeanie Drynan (nominated) Australian Film Institute Award for Best Director - Christina Andreef (nominated)
*Australian Film Institute Award for Best Screenplay - Christina Andreef (nominated)
*Australian Film Institute Award for Best Original Music Score - Antony Partos (nominated)

==Box Office==
Soft Fruit grossed $598,704 at the box office in Australia,. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 

 
 

 