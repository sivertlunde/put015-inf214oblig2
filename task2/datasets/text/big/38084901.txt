The Lady in Question (1999 film)
{{Infobox television film
| name         = The Lady in Question
| image        = The-Lady-in-Question 1999.jpg  
| image_size   = 240 px
| caption      =  Period Mystery mystery
| runtime      = 100 minutes Granada Entertainment in association with the Stan Margulies Company and Crystal Sky Communications
| director     = Joyce Chopra
| producer     = Stan Margulies Craig McNeil Steven Paul
| writer       = Gilbert Pearlman Gene Wilder Mike Starr Cherry Jones Barbara Sukowa John Benjamin Hickey Michael Cumpsty Claire Bloom
| editing      = Angelo Corrao John Morris
| budget       = 
| country      = United States
| language     = English
| network      = A&E Network|A&E
| released     = 
| first_aired  =  
| last_aired   = 
| num_episodes = 
| preceded_by  = Murder in a Small Town
| followed_by  = 
}}
 1999 American thriller television film directed by Joyce Chopra. It represents the last leading role and film for Gene Wilder and his last credit as screenwriter. As the previous Murder in a Small Town Wilder plays the amateur detective Larry "Cash" Carter.    
It was broadcast by A&E (TV channel)|A&E December 12, 1999. 

== Cast ==
* Gene Wilder 	as Larry "Cash" Carter Mike Starr	as Det. Tony Rossini
* Cherry Jones as  	Mimi Barnes 
* Barbara Sukowa		as  	Rachel Singer
* John Benjamin Hickey	as  Paul Kessler	 
* Claire Bloom		as 	Emma Sachs
* Michael Cumpsty		as 	Klaus Gruber
* Dixie Seatle as Gertie Moser

==Production==
After the high ratings A&E received for Murder in a Small Town, the first Cash Carter mystery, The Lady in Question began filming in Toronto in May 1999. Dempsey, John, "A&E commits more Murder". Daily Variety, April 6, 1999 

Although A&E and Granada Entertainment USA planned to develop the Gene Wilder character as a franchise,  DePalma, Anthony,   The New York Times, January 10, 1999  only two Cash Carter films were produced. On January 30, 2000, Wilder was admitted to Memorial Sloan–Kettering Cancer Center for a stem-cell transplant, a followup to treatment he received in 1999 for non-Hodgkin lymphoma. Wilder checked in under the name Larry Carter, his characters name in the two A&E films.  

==Home video releases==
* 1999, A&E Home Video, VHS (AAE-17606), ISBN 0-7670-2316-1
* 2002, A&E Home Video, DVD (AAE-72223), ISBN 0-7670-6956-0

==References==
 

==External links==
* 
*  
*   at the University of Iowa — Scripts and correspondence for The Lady in Question

 

 
 
 
 
 
 
 
 