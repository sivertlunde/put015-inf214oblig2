Diamonds for Breakfast (film)
 
{{Infobox film
| name           = Diamonds for Breakfast
| image          = 
| caption        = 
| director       = Christopher Morahan
| producer       = Carlo Ponti
| writer         = Ernesto Gastaldi Ronald Harwood Pierre Rouve N. F. Simpson
| starring       = Marcello Mastroianni
| music          = 
| cinematography = Gerry Turpin
| editing        = Peter Tanner
| studio         = ABC Films Bridge Films
| distributor    = Paramount Pictures
| released       =  
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         = $1.3 million "ABCs 5 Years of Film Production Profits & Losses", Variety, 31 May 1973 p 3 
}}
Diamonds for Breakfast is a 1968 British comedy film directed by Christopher Morahan.   

==Cast==
* Marcello Mastroianni - Grand Duke Nikolay Vladimirovich Godunov
* Rita Tushingham - Bridget Rafferty Elaine Taylor - Victoria
* Margaret Blye - Honey
* Francesca Tu - Jeanne Silkingers
* The Karlins - Triplets
* Warren Mitchell - Popov
* Nora Nicholson - Anastasia Petrovna
* Bryan Pringle - Police Sergeant
* Leonard Rossiter - Inspector Dudley
* Bill Fraser - Bookseller David Horne - Duke of Windemere
* Charles Lloyd Pack - Butler
* Anne Blake - Nashka

==Reception==
The film opened in London but was never released in the US. It recorded an overall loss of $1,445,000. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 
 