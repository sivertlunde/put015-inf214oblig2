A Kid from Tibet
 
 
{{Infobox film
| name           = A Kid from Tibet
| image          = AKidFromTibet DVDcover.jpg
| image_size     = 
| caption        = US DVD cover
| director       = Yuen Biao Chan Man-Ching Raymond Lee (art)  Lau Chau Sang
| producer       = Yuen Biao
| writer         = Barry Wong Sam Chi-leung Chan Kam-cheong
| narrator       = 
| starring       = Yuen Biao Michelle Reis Yuen Wah Nina Li Chi Wu Ma
| music          = Violet Lam
| cinematography = Arthur Wong Chan Tung-chuen
| editing        = Marco Mak Golden Harvest
| released       =  
| runtime        = 97 min
| country        = Hong Kong Cantonese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Peacock King). It was filmed in Taiwan, Hong Kong and partly on location in Tibet.

==Plot== Esoteric Buddhism" had tried to invade Tibet years ago, the Tibetan monks used a powerful magical item, the "Babu Gold Bottle" to expel them. The Tibetan master (Wu Ma) has the bottles cap and wishes to reunite it with the bottle as the Black Section are stirring once more. He sends a young monk, Wong La (Yuen Biao) to Hong Kong to recover the sacred bottle, which is in the possession of a crippled lawyer.

Wong meets and protects a woman, Chiu Seng-Neng (Michelle Reis) who is acting as the agent for the lawyer, and the Black Section fight to gain the magical bottle for themselves.

The leader of the Black Section (Yuen Wah) learns of the intended hand-over, and seeks to get the Babu Gold Bottle for himself.

==Cast==
* Yuen Biao as Lo Ba Wong La
* Michelle Reis as Chiu Seng-Neng
* Yuen Wah as Black Section Sorcerer Nina Li Chi as Sorcerers Sister
* Roy Chiao as Lawyer Robinson
* Michael Dingo as Michael
* Wu Ma as Wongs Master
* Billy Lau as Airport Security Guard / Jail Warden 
* Lau Chau Sang (Fung Lee) as Tibetan henchman
* Lay Kah as Tibetan henchman 
* Jackie Chan as Airport Passenger
* Gabriel Wong as Mimis lover
* Anthony Carpio as Black Section Sorcerers Henchman
* Jack Wong
* Corey Yuen
* Lee Ming Yeung
* Kingdom Yuen
* Bruce Law

==See also==
* List of Hong Kong films
*Jackie Chan filmography

==External links==
*  
*  

 
 
 
 
 
 
 


 
 