The Double (2013 film)
 
 
{{Infobox film
| name           = The Double
| image          = TheDouble2013Poster.jpg
| caption        = UK release poster
| director       = Richard Ayoade
| producer       = {{Plainlist|
* Robin C. Fox
* Amina Dasmal
|}}	
| writer         = {{Plainlist|
* Richard Ayoade
* Avi Korine
|}}
| based on       =  
| starring       = {{Plainlist|
* Jesse Eisenberg
* Mia Wasikowska
* Wallace Shawn
* Noah Taylor
* Yasmin Paige 
* James Fox
|}}          
| music          = Andrew Hewitt
| cinematography = Erik Wilson
| editing        = {{Plainlist|
* Chris Dickens
* Nick Fenton
|}} Film4 British Film Institute
| distributor    = StudioCanal UK  
| released       =  
| runtime        = 93 minutes 
| country        = United Kingdom 
| language       = English
| budget         =
| gross          = $1.7 million    	
}}

Not to be confused with The Double (2011 film)
 The Double by Fyodor Dostoyevsky. It is about a man driven to breakdown when he is usurped by a doppelgänger.    The film was produced by Alcove Entertainment, with Michael Caine, Graeme Cox (Attercop), Tessa Ross (Film4) and Nigel Williams as executive producers.

==Plot==
Downtrodden Simon James (Jesse Eisenberg) has worked at his office for seven years, but he is ignored by his boss and colleagues. From his apartment he spies on a beautiful coworker he admires, Hannah (Mia Wasikowska), who lives in the apartment opposite. He sees her throwing away art and secretly retrieves and admires it.

Simon sees a man jump to his death from the floor above Hannahs apartment. He talks to detectives, who explain that if the man had jumped a few feet to the right, he would have been badly hurt but would have survived.

Simons boss (Wallace Shawn) announces the arrival of a new employee, James Simon, who looks identical to Simon, which causes Simon to faint upon first glance. Assertive and charming, James is Simons polar opposite. Much to Simons annoyance, James not only gets respect from their co-workers but no one seems to notice the striking resemblance between the two. James, on the other hand, does notice this and sees Simons pain. Out of pity, he decides to buddy-up with Simon and give him advice about how to seduce Hannah. Hannah then asks him on a date through Simon. On the date, Simon pretends to be James, with the real James giving him instructions via earpiece. When Simon becomes nervous, the two swap places and James kisses Hannah, angering Simon. James asks Simon to take an aptitude test in his place and seduces their bosss surly, rebellious daughter, Melanie (Yasmin Paige), whom Simon was reluctantly hired to tutor.

Simon gets his revenge on James by revealing to Hannah that James is cheating on her with Melanie. Furious, James blackmails Simon for his apartment keys using explicit photos he took of himself with Melanie, knowing their boss will believe it is Simon in the photos. At work, Simon accuses James of being an imposter and is fired after going on a maniacal tirade. As he is about to kill himself, he sees Hannah lying unconscious in her apartment. He calls an ambulance and accompanies her to the hospital, where its revealed that she not only overdosed but also miscarried (she had become pregnant after a sexual encounter with James). Simon then takes Hannah back home, relieved that shes alright. Still upset, she states that she wanted to die and suggest that Simon kill himself. She goes through Simons jacket pockets and discovers her art.

Simon learns his mother has died and finds James at her funeral. Simon punches him and discovers that they share injuries; as Jamess nose bleeds, so does Simons. He finds Hannah and tells her he wants to be noticed. He goes to his apartment and handcuffs the sleeping James to his bed, then goes to the ledge above Hannahs apartment, steps to the right, and jumps. He survives but is badly hurt. Hannah runs to him and an ambulance arrives. Lacking medical attention, the handcuffed James appears to be on the brink of death as he lies motionless on the apartment floor.

Within the ambulance The Colonel (James Fox) and Hannah watch over Simon. The Colonel tells Simon that he is "special", to which the latter responds with a half-smile, "Id like to think Im pretty unique".

==Cast==
 
* Jesse Eisenberg as Simon James/James Simon   | publisher=IndieWire | date=11 May 2012 | accessdate=23 July 2013}} 
* Mia Wasikowska as Hannah
* Wallace Shawn as Mr. Papadopoulos
* Noah Taylor as Harris
* Yasmin Paige as Melanie Papadopoulos
* Cathy Moriarty as Kiki
* Phyllis Somerville as Simons Mother 
* James Fox as The Colonel
* Kobna Holdbrook-Smith as Guard / Doctor
* Tony Rohr as Rudolph
* Susan Blommaert as Liz
* Jon Korkes as Detective
* Tim Key as Care Worker
* Lloyd Woolf as Investigator
* Lydia Ayoade as Test Invigilator
* Sally Hawkins as Receptionist at Ball
* J Mascis as Janitor Christopher Morris as Official
* Chris ODowd as Nurse
* Craig Roberts as Young Detective
* Kierston Wareing as Funeral Date
* Jeanie Gold as Waitress
* Paddy Considine as Jack as PT Kommander
* Gemma Chan as Glamorous Judge
* Rade Serbedzija as Frightening Old Man
* Catherine Delaloye as Air Hostess
* Georgie-May Tearle as Air Hostess

 

==Production==
In February 2012 it was reported that Richard Ayoade would direct The Double, starring Jesse Eisenberg and Mia Wasikowska, in the United Kingdom.     Principal photography began on 20 May 2012 in London.    

==Soundtrack==
The score by Andrew Hewitt features a recurring progression of heavy chords played by strings.  The chord progression comes from the song "Der Doppelgänger" by Franz Schubert.

==Release==
The film premiered at the 2013 Toronto International Film Festival on 7 September 2013.      On 15 October 2013 it was announced that Magnolia Pictures had acquired the US rights to the film for a 2014 release. 
The Double opened in two cinemas in the US and grossed $14,646. It ended up grossing $145,511 in North America and $1,306,588 internationally for a total of $1,452,099 

==Reception==

===Critical response===
The Double received positive reviews from critics and has a "Certified Fresh" rating of 82% on Rotten Tomatoes based on 117 reviews, with an average rating of 6.8 out of 10. The consensus states "Hauntingly bleak and thrillingly ambitious, The Double offers Jesse Eisenberg a pair of compelling roles while reaffirming writer-director Richard Ayoades remarkable talent."  The film also has a score of 68 out of 100 on Metacritic based on 31 critics, indicating "Generally favorable reviews". 

Jon Espino from TheYoungFolks.com gave the film 9 out of 10 stars, stating: 
 The Double, is a pure artistic vision. The dark tones are only enhanced by the light notes of humor throughout the film. Ayoades avant garde, experimental style and minimalist set design sound like an odd couple, but they actually work great together.  

He also goes on to praise Eisenbergs performance, saying: 

 In his best work since The Social Network, Eisenberg portrays the polarized personalities of Simon and James with such ease. The story basically rests on his shoulders as we follow him on his descent into madness. In between the fast-paced dialogue and front-lit shots there is also a very complex story that leaves the ending open to interpretation.  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 