Kokey (film)
{{Infobox film
| name           = Kokey
| image          =  
| caption        = 
| director       = 
| producer       = 
| based on       = 
| screenplay     =
| starring       = Carlo Aquino Ani Pearl Alonso Cherry Pie Picache Ricky Davao
| music          = 
| cinematography =
| editing        = 
| distributor    = Star Cinema
| released       =  
| runtime        = 
| country        = Philippines
| language       = Tagalog
| budget         =
}}

The first Kokey encounter happened last 1997 when we were first introduced to the cute alien who had a penchant for befriending human beings. And his best friend then was the pre-pubescent Carlo Aquino in the movie entitled Kokey.It was directed by the same man who brought Cedie and Sarah, Ang Munting Prinsesa on the big screen---Romy Suzara. 

==About==
On their way home, Bong and Anna discover Kokey, an extraterrestrial whose space ship explodes upon landing on earth. Without the knowledge of their parents, the children keep Kokey at home. The three soon become the best of friends with Kokey turning out to be a big help to the family and their business, attracting customers using his extraterrestrial powers. But Kokey becomes homesick, pointing to the sky in silent tears, missing his parents. He tries to contact them but with no success. Will he ever be reunited to his family? How long can the children keep Kokey a secret? 

==Cast==
* Carlo Aquino as Bong
* Ani Pearl Alonso as Anna
* Mahal as Kokey
* Ricky Davao as Nanding
* Cherry Pie Picache as Trining
* Paquito Diaz as Marcial
* Nova Villa as Mrs. Querubin
* Ernie Zarate as Dr. Perez
* L.A. Lopez as Ruel
* Danny Labra as Antonio
* Eddie Nicart as Banderas

==See also==
*Kokey
*Kokey @ Ako

==References==
 


 
 
 
 
 

 