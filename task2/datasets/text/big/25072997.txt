Prom Night in Mississippi
{{Infobox film
| name           = Prom Night in Mississippi
| image          = Prom Night in Mississippi.jpg
| image_size     = 
| alt            = 
| caption        = film poster
| director       = Paul Saltzman
| producer       = Paul Saltzman Patricia Aquino
| writer         = Paul Saltzman
| narrator       = 
| starring       = 
| music          = Asher Lenz Jack Lenz
| cinematography = Paul Kolycius Don Warren
| editing        = Stephen Philipson David Ransley Kevin Schjerning
| studio         = Return to Mississippi Productions
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States English
| budget         = $750,000
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 
 2009 Canadian-American Charleston High racially integrated prom in Charleston history. 
{{cite news
|url=http://www.thenation.com/article/charlestons-first-integrated-prom
|title=Charlestons First Integrated Prom 
|last=Sood
|first=Suemedha 
|date=June 11, 2008
|publisher=The Nation
|accessdate=2011-10-27}}  
{{cite news
|url=http://www.voanews.com/english/archive/2009-03/2009-03-09-voa52.cfm?moddate=2009-03-09
|title=Prom Night in Mississippi Makes History |last=Poulou
|first=Penelope 
|date=March 9, 2009
|publisher=VOA News
|accessdate=2009-11-17}} 

==Background and production== Canadian film director Paul Saltzman with his wife Patricia Aquino acting as Film producer|producer.  It was filmed over a four-month period on a budget of $750,000 of the directors own money. The crew shot over 165 hours of footage, 89 minutes of which were used in the final version.   It was shown on HBO,    premiered in Toronto on November 12, 2009, and had its theatrical release on December 11, 2009. 

==Documentary==
The documentary is about the senior prom in Charleston, Mississippi.  The high school in Charleston (a community of 2,100 residents) has an average of 80 graduates per year, and up until 2008 had separate, segregated proms for black students and white students, 
{{cite news
|url=http://www.theglobeandmail.com/news/arts/morgan-freeman-talks-about-his-prom-night-in-mississippi/article1362414/
|title=Morgan Freeman talks about his Prom Night in Mississippi
|last=Schneller
|first=Johanna 
|date=November 13, 2009
|publisher=The Globe and Mail
|accessdate=2009-11-13}}  despite Mississippi fully integrating their schools in 1970. 
{{cite news
|url=http://www.npr.org/templates/story/story.php?storyId=91371629
|title=Mississippi School Holds First Interracial Prom
|date=June 11, 2008
|publisher=National Public Radio
|accessdate=2009-11-13}}   In 1997 Morgan Freeman (a resident of Charleston since 1991) approached the school and offered to pay for the prom, provided it be racially integrated.  The school declined Freemans offer.  In 2008 Freeman offered again, and the school agreed to move forward with an integrated prom. 

Saltzman follows a group of students, both black and white, over four months as they prepare for their senior prom.  The students discuss segregation in Charleston and how they feel about it. The documentary also explores issues such as interracial relationships, and what the parents think about an integrated prom. The integrated prom is successful despite some parents forbidding their children to attend it, and that a white only prom was held by some of the parents.   The film brought some racial tension to the town of Charleston, mostly from  the parents and school authorities concerned with "tradition and security issues", 
{{cite news
|url=http://www.nowtoronto.com/movies/story.cfm?content=172294
|title=Prom Night In Mississippi
|last=Wilner
|first=Norman
|date=November 11–18, 2009 NOW
|accessdate=2009-11-13}}  with a group of parents planning a separate prom for white students only. 
{{cite news
|url=http://www.metronews.ca/ottawa/entertainment/article/368108--prom-night-in-mississippi
|title=Prom Night in Mississippi
|last=Gow
|first=Steve
|date=November 12, 2009
|publisher=Metro News
|accessdate=2009-11-13}} 

==Recognition==

===Awards & nominations=== AFI Dallas International Film Festival 
{{cite news
|url=http://www.cbc.ca/mobile/text/story_arts.html?/ept/html/story/2009/04/30/documentaries-canadian.html
|title=Two Canadian docs make international waves CBC
|accessdate=2009-11-17
| date=2009-04-30}} 
*2009, Won Audience Choice Award, Jackson Crossroads Film Festival Oxford Film Festival, Mississippi
*2009, Nominated for Grand Jury Prize for World Cinema &ndash; Documentary at Sundance Film Festival

==See also==
 
* Segregated prom
 

==References==
 

==External links==
* 
* 

 
 
 