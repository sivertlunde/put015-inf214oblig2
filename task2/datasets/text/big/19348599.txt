The Secret (1974 film)
  1974 French film, directed by Robert Enrico.   

==Synopsis==
At the start we see "David" Jean-Louis Trintignant as a prisoner in some sort of torture chamber, a row of cells whose occupants, in straightjackets, are chained to their beds. David manages to escape from custody.  On the run, he arrives at the home of a couple living in an isolated farmhouse (Marlène Jobert and Philippe Noiret).  He claims that he is in possession of an important secret, one that he came across by chance, that is so terrible that the authorities will do anything to protect it. He predicts, correctly, that the state will soon mobilize all its resources to find him, using as a cover story the claim that a paranoid killer is on the loose from an asylum.  The couple are unsure whether to believe him, but give him some assistance. Whether David is an unlucky but innocent fugitive or is the psychopath described by the police and press remains an mystery until the last moment of the film.

==Other==
Étienne Becker is the Director of Photography.

==External links==
* 

 
 
 
 
 
 
 


 
 