Beneath the Planet of the Apes
{{Infobox film
| name           = Beneath the Planet of the Apes
| image          = Beneath-the-Planet-of-Apes.jpg
| border         = yes
| caption        = Film poster by Tom Chantrell
| director       = Ted Post
| screenplay     = Paul Dehn
| story          = Paul Dehn Mort Abrahams
| based on       =   Maurice Evans Linda Harrison Charlton Heston
| producer       = Arthur P. Jacobs
| music          = Leonard Rosenman
| cinematography = Milton R. Krasner
| editing        = Marion Rothman
| studio         = APJAC Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English |
| budget         = $4,675,000 
| gross          = $18,999,718   Box Office Mojo. Retrieved May 14, 2012. 
}} Planet of Maurice Evans, Linda Harrison, and features Charlton Heston in a supporting role.

In this sequel, another spacecraft crashes on the planet ruled by apes, carrying astronaut Brent who searches for Taylor and discovers an underground city inhabited by mutated humans with psychic powers. Beneath the Planet of the Apes was a success at the box office but met with mixed reviews from critics. It was followed by Escape from the Planet of the Apes.

==Plot== Planet of Taylor (Charlton Nova (Linda Linda Harrison) Forbidden Zone. Without warning, fire shoots up from the ground and deep chasms open. Confused by the strange phenomenon, Taylor investigates a cliff wall and disappears before Novas eyes.
 Brent (James General Ursus James Gregory) Maurice Evans). Cornelius (David Zira (Kim Hunter), who treat his wound and tell him of their time with Taylor. The humans hide when Dr. Zaius arrives and announces that he will accompany Ursus on the invasion of the Forbidden Zone.
 Queensboro Plaza telepathic humans nuclear bomb.

Brent and Nova are captured and telepathically interrogated, and Brent reveals the apes are marching on the Forbidden Zone. The telepaths attempt to repel the apes by projecting illusions of fire and other horrors, as they had done to Taylor and Nova. Dr. Zaius sees through the illusions, however, and leads the ape army to the ruined city. With the apes closing in, the telepaths plan to detonate their "Divine Bomb" as a last resort. They hold a religious ceremony, at the height of which they remove their masks to reveal that they have been grotesquely mutated by centuries of exposure to nuclear fallout and they are the descendants of its 20th century survivors.

Brent is separated from Nova and taken to a cell, where he finds Taylor. The mutant Ongaro ( , capable of destroying the planet, marked with the Greek letters Alpha and Omega on its casing.
 Paul Richards), who raises the bomb into activation position before being gunned down. Brent and Taylor attempt to stop Ursus from accidentally setting off the weapon, but Taylor is shot. Brent manages to kill Ursus before being shot dead by the gorillas. The mortally wounded Taylor pleads with Dr. Zaius for help, but Zaius refuses, saying that man is only capable of destruction. In his last moment, Taylor brings his hand down on the activation switch, triggering the bomb and destroying the Earth. The film ends with a voice-over saying, "In one of the countless billions of galaxies in the universe, lies a medium-sized star, and one of its satellites, a green and insignificant planet, is now dead".

==Cast==
* James Franciscus as Brent David Watson as Cornelius
* Kim Hunter as Zira Maurice Evans as Dr. Zaius Linda Harrison as Nova Paul Richards as Mendez
* Natalie Trundy as Albina
* Jeff Corey as Caspay
* Gregory Sierra as Verger
* Victor Buono as Adiposo (credited as Fat Man) Handley, Rich. Timeline of the Planet of the Apes: The Definitive Chronology. Hasslein Books, 2009. ISBN 978-0-615-25392-3. Page 195: "Ongaro and Adiposo are identified as "Negro" and "Fatman" in the films credits; their names appear in The Mutant News, a promotional newspaper distributed to moviegoers during the release of Beneath the Planet of the Apes." 
* Don Pedro Colley as Ongaro (credited as Negro)  James Gregory as General Ursus
* Charlton Heston as Taylor
* Tod Andrews as Skipper
* Thomas Gomez as Minister
* Roddy McDowall as Cornelius (uncredited, opening archive footage from Planet of the Apes)
* Paul Frees (uncredited end narration)

==Production==
===Development and writing=== original novel, 1945 atomic bombings and the fear of nuclear warfare on the story. One of the elements thought up by Abrahams and Dehn was a half-human, half-ape child, but despite even going through make-up tests this was dropped due to the implication of bestiality. "From Alpha to Omega: Building a Sequel", Beneath the Planet of the Apes Blu-ray  According to screenwriter Dehn the idea for Beneath came about from the end of the first movie which suggested that New York City was buried underground. 

Although Charlton Heston showed little interest in reprising his role as Taylor, studio head Richard Zanuck thought the actor was essential to the sequel. After some disagreement with the actors agents, Heston agreed to briefly appear with the provision that Taylor be killed and that Hestons pay go to charity. The writers decided to have Taylor disappear at the storys start and only return by the films ending, and have a new protagonist for the major part of the story.  James Franciscus accepted the role of Brent as a break from his usual TV fare. 

Director Franklin J. Schaffner was invited to return to the series, but declined due to a commitment to Patton (film)|Patton. Television and film director Ted Post was approached, and while objecting to the script for "not making a point at all", the producers asked what he did not like. Post then wrote a letter saying that "the loss of a planet is the loss of all hope". Post tried to get the other writer of the original, Michael Wilson, but a budget cut prevented him from doing so.  Post and Franciscus - who wanted to help clarify the actions of and give depth to the character of Brent - spent a week rewriting the script, leading to over fifty pages of notes suggesting story ideas to fix some of the narrative problems in Paul Dehns script.  
 Tam Lin. James Gregory. 

Before Zanuck was fired as studio president during production, he suggested that Post add an element suggested by Heston, the Alpha Omega doomsday bomb, to end the series.  However, before the films release, the producers were considering ideas for another sequel. 

===Filming===
Production began in February 1969. The sequel, now titled Beneath the Planet of the Apes, had its budget reduced from $5 million to $2.5 million because Fox had suffered recently from several underperforming big-budget films, like Star! (film)|Star!, Hello, Dolly! (film)|Hello, Dolly! and Tora! Tora! Tora!.  Nonetheless, the studio expected the film to return Fox to profitability. 
 Grand Central – 42nd Street station and hotel lobby sets from the film Hello, Dolly! 

===Music=== Moog pioneer Paul Beaver. 

==Novelization==
The novelization of the film by Michael Avallone retained the original scripted ending. Brent does not kill General Ursus. Taylor confronts him and Dr. Zaius. As Taylor tries to reason with Zaius, Zaius condemns him and Ursus repeatedly shoots Taylor with his pistol; Brents rifle empties and the gorillas kill him. Ursus is horrified, telling Zaius that he has emptied the pistol into Taylor; he should be dead, but he still lives. Knowing he is dying, Taylor (after Zaius refuses to help him) decides to stop the violence by detonating the bomb. This he does, destroying the Earth itself. 

==Comic book adaptations==
Gold Key Comics produced an adaptation of Beneath the Planet of the Apes in 1970. This was the first comics publication in the Planet of the Apes franchise.  Later, Marvel Comics published a different version in two series (b/w magazine 1974-77,  color comic book 1975-76 ). Malibu Comics reprinted the Marvel adaptations when they had the license in the early 1980s.

==Reception== IMDb page. 

== Notes and references ==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 