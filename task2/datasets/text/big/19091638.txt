Dunya and Desi
Dunya 2008 Cinema Dutch film official submission to the 81st Academy Awards for Best Foreign Language Film.  It has received the Golden Film, a film award recognizing domestic box office achievements in the Netherlands. 

==Cast==
* Maryam Hassouni (Dunya El-Beneni)
* Eva van de Wijdeven (Desie Koppenol)
* Tygo Gernandt (Pim)
* Theo Maassen (Jeff Schouten)
* Christine van Stralen (Monique Koppenol) 
* Ilias Addab (Samir)
* Mahjoub Benmoussa (Nabil El-Beneni)
* Micha Hulshof (Maik)
* Rachida Iaallala (Kenza El-Beneni)
* Iliass Ojja (Souffian Zoef El-Beneni)
* Abdullah Ahmed Saleh (Dunyas grandfather)
* Marcel Musters (Hans Schakel)
* Alix Adams (Mrs. Schakel)

==Synopsis==
Dunya El-Benini and Desie Koppenol are teenagers and best friends, each nearly 18 years old.  Dunya is of Moroccan ancestry, but was born in the Netherlands.  She narrates the film, and begins by noting how Desie juggles multiple boyfriends.  As a surprise birthday present for Dunya, Desie arranges for a driving lesson with a boyfriend, Pim.  Dunyas parents generally object to Desies behaviour.  

Desie works as a hairdresser and is also involved with her boss, Mike.  However, in a moment of anger at Mike, she quits her job.  When Desie misses her period, she gets a home pregnancy test, and it comes up positive.  She thinks that Pim is the father.  She learns from her mother, Monique Koppenol, that she was the result of an unplanned pregnancy when her mother was young.  Her father abandoned them both soon after Desie was born, and moved to Morocco.  Desie contemplates having an abortion and gets a referral, but at the last moment decides not to have an abortion.

On her 18th birthday, Dunya hears that she is to be married by arrangement to a cousin in Morocco.  She and her family move to a new house in Morocco, which they find is uncompleted after they have arrived.  Dunya sees her cousin to whom the marriage is to be arranged, and is not attracted to him at all.  However, she sees a young man, Shamir, who is part of the house building crew, and finds him more attractive than her cousin.

Desie digs up a secret "treasure chest" of personal items of interest to both her and Dunya.  In it is the address in Morocco of her father, Hans Schakel.  Desie decides to go to Morocco to track down her father.  She surprises Dunya and her family when she arrives unannounced at their house in Morocco.  During her brief stay with Dunyas family, she scandalises the family by her behaviour and her dress.  Tension develops between Dunya and Desie, and ultimately Dunya decides to accompany Desie on her search for her father, as she feels that Desie will not survive alone in Morocco.  

The two of them travel to Casablanca and into rural Morocco to search for Desies father.  They suffer the loss of Desies luggage to two street con-men, and spend one evening in a Casablanca hospital after Desie shows signs of bleeding from the pregnancy.  They learn that Desies father did work in the city at the docks, but left that job years earlier and opened a gasoline station and restaurant away from urban areas.  They continue to have fights and almost split up, but eventually reconcile and keep on the search for Hans Schakel.  In the meantime, Desies mother and her boyfriend Jeff are concerned over Desies disappearance, and travel to Morocco in search of her.

==References==
 

==External links==
*  

 
 
 