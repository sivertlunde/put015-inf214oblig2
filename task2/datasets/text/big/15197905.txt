The Gentleman from America
 
{{Infobox film
| name           = The Gentleman from America
| image          = The Gentleman from America (1923) - Ad 1.jpg
| caption        = Ad for the film
| director       = Edward Sedgwick
| producer       =
| writer         = George C. Hull Raymond L. Schrock
| starring       = Hoot Gibson
| cinematography = Virgil Miller
| editing        = Universal Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States Silent (English intertitles)
| budget         =
}}
 silent comedy film directed by Edward Sedgwick and featuring Hoot Gibson.    It also featured Boris Karloff in an uncredited role. Its survival status is classified as unknown,  which suggests that it is a lost film.

==Cast==
* Hoot Gibson as Dennis OShane (credited as Ed "Hoot" Gibson) Tom OBrien as Johnny Day
* Louise Lorraine as Carmen Navarro
* Carmen Phillips as The Vamp
* Frank Leigh as Don Ramón Gonzales
* Jack Crane as Juan Gonzales Robert McKenzie as San Felipe (credited as Bob McKenzie)
* Albert Prisco as Grand Duke
* Rosa Rosanova as Old Inez
* Ricardo Cortez as Bit Role (uncredited)
* Boris Karloff as Bit Role (uncredited)

==See also==
* Hoot Gibson filmography
* Boris Karloff filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 