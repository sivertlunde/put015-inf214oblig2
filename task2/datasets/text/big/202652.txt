Romeo + Juliet
 
 
 
{{Infobox film
| name = Romeo + Juliet
| image = william_shakespeares_romeo_and_juliet_movie_poster.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Baz Luhrmann
| producer = Baz Luhrmann Gabriella Martinelli
| screenplay = Craig Pearce Baz Luhrmann
| based on =  
| starring = Leonardo DiCaprio Claire Danes Brian Dennehy John Leguizamo Pete Postlethwaite Paul Sorvino Diane Venora Craig Armstrong Donald M. McAlpine
| editing = Jill Bilcock Bazmark Productions

| distributor = 20th Century Fox
| released =  
| runtime = 120 minutes  
| country = United States
| language = English
| budget = $14.5 million
| gross = $147,554,999 
}} romantic drama adaptation of Montagues and Capulets are represented as warring mafia empires (with legitimate business fronts) and swords are replaced with guns (with brand names such as "Dagger" and "Sword").

Some of the characters names are also changed. Lord and Lady Montague and Lord and Lady Capulet are given first names (as opposed to the Shakespeare original where their first names are never mentioned), Friar Lawrence becomes Father Lawrence, and Prince Escalus is renamed Captain Prince. There is also no Friar John, who was in the original play. Also, some characters were switched from one family to the other—in the original, Gregory and Sampson are Capulets, but in the film, they are Montagues. (Abram, as Abra, and Petruchio, conversely, are shifted from the Montague to the Capulet family.) In addition, a few plot details are shifted, most notably near the ending.
 Golden Bear Catherine Martin Best Art Direction/Set Decoration. 

==Plot==
In the fictional "Verona Beach", the Capulets and the Montagues are arch-rivals.    The animosity of the older generation—Fulgencio and Gloria Capulet and Ted and Caroline Montague—is felt by their younger relatives. A gunfight between the Montague boys led by Benvolio, Romeos cousin, and the Capulet boys led by Tybalt, Juliets cousin, creates chaos in the city. The Chief of Police, Captain Prince, reprimands the families, warning them that if such behavior continues, their lives "shall pay the forfeit of the peace".

Benvolio meets with Romeo on a beach. While playing a game of pool they learn of a party being held by the Capulets that evening which they decide to gate-crash. Romeo agrees to come after discovering that Rosaline, with whom he is in love, is attending. 
 Ecstasy Mercutio gave him and they proceed to the Capulet mansion. The effects of the drug and the party overwhelm Romeo, who goes to the restroom. While admiring an aquarium, he and Juliet see each other and fall instantly in love. Tybalt spots Romeo and vows to kill him for invading his familys home, but Fulgencio stops him.
 Father Lawrence, telling him he wants to marry Juliet. He agrees to marry the pair in hopes that their marriage will help ease the tensions between the families. Romeo passes the word on to Juliet’s nurse and the lovers are married.

Tybalt encounters Mercutio just as Romeo arrives. Romeo attempts to make peace, but Tybalt assaults him. Mercutio intervenes and batters Tybalt, and is about to finish him off by hitting him with a log when Romeo stops him. Tybalt mortally wounds Mercutio with a shard of glass. Mercutio curses both the Montagues and the Capulets before dying in Romeos arms. Enraged, Romeo chases after a fleeing Tybalt and guns him down.
 consummate their Dave Paris, the governors son.

The next morning, Romeo narrowly escapes as Juliets mother tells her that the family has promised she will marry Paris. She refuses to marry, so her father threatens to disown her. Her mother and nurse insist it would be in her best interest to marry Paris. Juliet sees Father Lawrence, imploring him to help her and threatening to commit suicide. Father Lawrence proposes she fake her own death and be put in the Capulet vault to awaken 24 hours later. Romeo will be told of the plot, sneak into the vault, and once reunited the two can travel to Mantua. He gives her the potion which mimics death. After saying goodnight to her mother, Juliet drinks the potion. She is found in the morning, declared dead, and placed in the vault. Balthasar, one of Romeos cousins, learns that Juliet is dead and tells Romeo, who is not home when the messenger arrives to tell him of the plan.

Romeo returns to Verona, where he buys poison. As he goes to the church, Captain Prince finds out he is back, and tries to capture him, without success. Father Lawrence learns that Romeo has no idea Juliet is alive. Romeo enters the church where Juliet lies and bids her goodbye. She awakens just as Romeo takes the poison; the two thus see each other before he dies. After he dies, Juliet picks up Romeos gun and shoots herself in the head, dying instantly. The two lovers are discovered in each others arms. Prince condemns both families, whose feuding led to such tragedy, and coroners are shown removing the two bodies.

==Cast==
 
 
;The House of Montague Ted Montague, Romeos father  Caroline Montague, Romeos mother Romeo Montague Benvolio Montague, Romeos cousin Balthasar Montague, Romeos cousin Gregory Montague, Romeos cousin Sampson Montague, Romeos cousin
 
;The House of Capulet Fulgencio Capulet, Juliets father Gloria Capulet, Juliets mother Juliet Capulet Tybalt Capulet, Juliets cousin Abra Capulet, Juliets cousin Petruchio Capulet, Juliets cousin
* Miriam Margolyes as Nurse (Romeo and Juliet)|Nurse, Juliets nanny
 
;Others
* Harold Perrineau as Mercutio, Romeos best friend Father Lawrence, the priest who marries Romeo and Juliet Dave Paris, the governors son and Juliets fiance Captain Prince, the chief of police Apothecary
* Quindon Tarver as Choir Boy, the singer at Romeo and Juliets wedding
* Edwina Moore as the Anchorwoman, who assumes the role of the Chorus
 

Natalie Portman had been considered for the role of Juliet but, after production began, it was felt that the footage looked as though DiCaprio was "child molestation|molesting" her.    

DiCaprio proclaimed Danes be cast, as "she was the only one who looked me in the eye in auditions." 

==Production==
After the success of Strictly Ballroom, Luhrmann took some time over deciding what his next project would be:
 Our philosophy has always been that we think up what we need in our life, choose something creative that will make that life fulfilling, and then follow that road. With Romeo and Juliet what I wanted to do was to look at the way in which Shakespeare might make a movie of one of his plays if he was a director. How would he make it? We dont know a lot about Shakespeare, but we do know he would make a `movie movie. He was a player. We know about the Elizabethan stage and that he was playing for 3000 drunken punters, from the street sweeper to the Queen of England - and his competition was bear-baiting and prostitution. So he was a relentless entertainer and a user of incredible devices and theatrical tricks to ultimately create something of meaning and convey a story. That was what we wanted to do.   accessed 19 November 2012  
Luhrmann obtained some funds from Fox to do a workshop and shoot some teaser footage in Sydney. Leonardo DiCaprio agreed to pay his own expenses to fly to Sydney and be part of it. Once Fox saw the footage - of the fight scene - they agreed to support it. 
 Chapultepec Castle Churubusco Studios; Del Valle neighborhood. 

==Reception== premiered on November 1, 1996 in the United States and Canada in 1,276 theaters and grossed $11.1 million its opening weekend, ranking #1 at the box office. It went on to gross $46.3 million in the United States and Canada    with a worldwide total of United States dollar|USD$147,554,998.   

Review aggregator Rotten Tomatoes rated the film "Fresh", with 72% of 53 critics giving positive reviews.  James Berardinelli gave the film 3 out of 4 stars and wrote, "Ultimately, no matter how many innovative and unconventional flourishes it applies, the success of any adaptation of a Shakespeare play is determined by two factors: the competence of the director and the ability of the main cast members. Luhrmann, Danes, and DiCaprio place this Romeo and Juliet in capable hands." 
 51st BAFTA Best Direction. Best Adapted Best Film Best Production Design. The film was also nominated for Best Cinematography, Best Editing, and Best Sound. 
 Golden Bear Catherine Martin Best Art Direction/Set Decoration. 
 samurai drama Mafia story, West Side China Girl”), but I have never seen anything remotely approaching the mess that the new punk version of “Romeo & Juliet” makes of Shakespeares tragedy. 

==Soundtrack==
 
 Garbage
# Everclear
# "Angel" – Gavin Friday One Inch Punch Kissing You (Love Theme from Romeo & Juliet)" – Desree
# "Whatever (I Had a Dream)" – Butthole Surfers
# "Lovefool" – The Cardigans
# "Young Hearts Run Free" – Kym Mazelle
# "Everybodys Free (To Feel Good)" – Quindon Tarver
# "To You I Bestow" – Mundy Talk Show Host" – Radiohead
# "Little Star" – Stina Nordenstam
# "You and Me Song" – The Wannadies

==References==
 

;General
* Lehmann, Courtney.   Shakespeare Quarterly. 52.2 (Summer 2001) pp.&nbsp;189–221.

==External links==
 
 
*   (Archive)
*  
*  
*  
*  
*   at Virtual History

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 