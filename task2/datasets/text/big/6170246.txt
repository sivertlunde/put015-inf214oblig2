Khiladiyon Ka Khiladi
 
{{Infobox Film
| name           = Khiladiyon Ka Khiladi
| image          = Khiladiyon Ka Khiladi.jpg
| caption        = Film poster
| director       = Umesh Mehra
| producer       = 
| writer         = 
| narrator       =  Brian Adams Gulshan Grover
| music          = Anu Malik
| cinematography = 
| editing        = 
| distributor    = 
| released       = 14 June 1996
| runtime        = 192 min.
| country        =  Hindi
| budget =  
| gross =  (worldwide)
}} Brian Lee as "The Undertaker". It was the 5th highest grossing movie of the year 1996 and was declared SuperHit by Box Office India.  It was the fourth installment in the Khiladi (film series).

==Plot==
Khiladiyon Ka Khiladi is about the deadly game of survival in a ruthless world of crime and sleaze.  A criminal don, Maya (Rekha), hosts illegal wrestling matches in Canada (Toronto) and has the full support of the local Police Commissioner. Ajay Malhotra has relocated to Canada and has started his own orchestra with the help of some of his friends.

His brother, Akshay (Akshay Kumar), decides to visit him on hearing that he wants to marry his beloved in Canada; on the airplane he meets Priya (Raveena Tandon), and both fall in love. Once in the U.S., Akshay finds out that the police have a warrant for the arrest of Ajay and want to question him. Akshays attempts to locate Ajay lands him with Maya, who happens to be Priyas sister. Apparently Maya is holding Ajay and will only release him after he hands over incriminating documents. Akshay soon wins Mayas confidence by rescuing her from attempts on her life made by King Don (Gulshan Grover), and Maya begins to like him and trust him. Akshay then proposes to her, to which Maya agrees, much to the disappointment of Priya.

Soon Akshay kills Mayas men when they get to know of his true identity. He also organises a fake kidnap drama with Ajays friends, who kidnap him and demand that Maya come to meet them with Ajay. By now Maya realizes that Akshay is Ajays brother, and Priya actually loves Akshay. At the end, Maya commits suicide, and before dying she hands over her sister Priya to Akshay.

==Cast==
*Akshay Kumar as Akshay 
*Raveena Tandon as Priya
*Rekha as Madam Maya
*Inder Kumar as Ajay 
*Dolly Bindra as Bhagwanti Brian Adams as Crush (WWE character)
*Gulshan Grover as King Don Brian Lee as The Undertaker (WWE character)

==Music==
The film soundtrack contains 7 songs composed by Anu Malik. The song In The Night No Control was as usual copied by Anu Malik from 1984 song Self Control (song) by Laura Branigan and the song Tu Waaqif Nahi Meri Deewangi Se was copied from 1976 Song Fernando (song) by ABBA

{| class="wikitable"
|-
! Song
! Singer(s)
|-
|Tu Kaun Hai Tera Naam Kya Kumar Sanu, Sadhana Sargam
|-
|Aaj Meri Zindagi Babul Supriyo, Alka Yagnik
|-
|In The Night No Control Sumitra
|-
|Itna Mujhe Pata Hai Abhijeet Bhattacharya|Abhijeet, Kavita Krishnamurthy
|-
|Maa Sherawaliye Sonu Nigam
|-
|Mera Baba Shahi Fakir Bali Brahmabhatt
|-
|Tu Waaqif Nahi Meri Deewangi Se Kumar Sanu, Sadhana Sargam
|}

==Awards==
*Filmfare Best Supporting Actress Award – Rekha
*Star Screen Award Best Villain – Rekha
*Filmfare Best Action Award – Akbar Bakshi

==References==
 

==External links==
* 
 

 
 
 
 
 