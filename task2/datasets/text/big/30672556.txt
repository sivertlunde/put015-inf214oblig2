Mowgli's Brothers (1976 TV Special)
Mowglis Brothers is a 1976 television animated special created by legendary animator Chuck Jones.  It is based from the first chapter of Rudyard Kiplings The Jungle Book of the same name.  The special was narrated by Roddy McDowall who does all the male characters in the film.  It originally aired on CBS on February 11, 1976.  The special was released on VHS by Family Home Entertainment in 1985 and in 1999 and released on DVD by Lionsgate.

==Plot==
Father Wolf and Mother Wolf are raising a family of cubs, are furious to learn that Shere Khan the lame tiger is hunting in their part of the jungle because he might kill men and bring human retribution upon the jungle. Tabaqui the jackal (who looks a lot like Wile E. Coyote) helps out with the tiger. Father Wolf hears something approaching their den which turns out not to be the tiger but a naked baby. Mother Wolf decides to adopt the hairless "man-cub". Her determination is only strengthened by the arrival of Shere Khan who demands the cub for his meal. The wolves drive off the tiger and Raksha names him Mowgli the Frog because of his hairlessness.

At the wolf packs meeting at Council Rock Baloo the bear speaks for the man-cub and Bagheera the panther buys his life with a freshly killed bull. Baloo and Bagheera undertake the task of educating Mowgli as he grows. Meanwhile Shere Khan plans to take revenge on the wolf pack by persuading the younger wolves to depose their leader Akela.

When Mowgli is about 11 or 12 Bagheera tells him of Shere Khans plan. Mowgli, being human, is the only creature in the jungle that does not fear fire, so he steals a pot of burning coals from a nearby village in order to use it against Shere Khan.

The young wolves prevent Akela from catching his prey, and at that nights meeting Shere Khan and Tabaqui demand Akela to be killed and the man-cub given to him. Mowgli, despite being naked and unprotected, attacks Shere Khan with a burning branch and drives him and his allies away, but realises to his sorrow that he must now leave the pack and return to humanity.

==External links==
* 

 
 

 
 

 