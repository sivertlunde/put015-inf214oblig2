Black Dawn (film)
{{Infobox Film
| name           = Black Dawn
| image          = BlackDawn2005.StevenSeagal.png
| caption        = DVD cover
| director       = Alexander Gruszynski
| producer       = Kamal Aboukhater Steven Seagal Andrew Stevens
| writer         = Martin Wheeler
| starring       = Steven Seagal Tamara Davies John Pyper-Ferguson
| music          = David Wurst Eric Wurst
| cinematography = Bruce McCleery
| editing        = Todd C. Ramsay
| studio         = Sony Pictures Home Entertainment
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $8 million
}} The Foreigner.

==Plot== CIA agent now working for himself and offering his services to the highest bidder.

Jon is hired to break James Donovan (John Pyper-Ferguson) out of prison. After a successful break, Jon takes James to see his brother, arms dealer Michael Donovan (Julian Stone), who had hired Jon to break James out.

In gratitude, the Donovans hire Jon to help sell parts for a small nuclear bomb to Nicholi (Nicholas Davidoff), the leader of a Chechen terrorist group planning to blow up Los Angeles because the CIA killed the groups previous leader.

Meanwhile, Jons former protégé, agent Amanda Stuart (Tamara Davies), is spying on the Donovans for the CIA. The Donovans see her spying on them while dealing with Nicholi.

Jon rescues Amanda and takes Michael hostage. Nicholi does not care what happens to Michael, so that does not work as well as it should. Jon and Amanda go on the run, trying to keep the Donovans out of the way and stop Nicholi from blowing up LA.

Also, Jon has been framed for the killings of some former CIA co-workers, so there is a shoot-on-sight directive on his head. It will be a miracle if Jon can clear his name and keep Los Angeles from being blown up.

==Cast==
* Steven Seagal as Jonathan Cold
* Tamara Davies as Agent Amanda Stuart
* John Pyper-Ferguson as James Donovan
* Julian Stone as Michael Donovan
* Nicholas Davidoff as Nicholi
* Roman Varshavsky as Fedor
* Conal Duffy as Cab driver
* Andrew Stevens has a bit part as a security guard

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 