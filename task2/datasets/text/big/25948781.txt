Pig (1998 film)
 
 
{{Infobox film
| name           = Pig
| image          = Pig1998VHScover.jpg
| caption        = Original 1998 VHS cover of Pig.
| director       = Rozz Williams Nico B.
| producer       = Nico B.
| writer         = 
| starring       = Rozz Williams James Hollan
| cinematography = 
| editing        = 
| studio         = Open Eye Productions Cult Epics
| distributor    = CAV Distributing 
| released       =  
| runtime        = 23 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}} short silent silent film produced by Nico B. It was distributed by the CAV Distributing company in South San Francisco and produced by the studios Open Eye Productions and Cult Epics. (The latter of which Nico B. is the founder and owner.)
 horror and surround soundtrack on a BLU RAY/DVD combo with Williams posthumous sequel film 1334, also directed by Nico B., and released through Cult Epics.

The DVD edition contains a large assortment of featured content not previously seen on the VHS version, especially in commemoration of Williams death. This includes a commentary and Rozz Williams memorial video by Nico B., "behind-the-scenes" footage, outtakes and optional commentary, a Rozz Williams photo gallery, a previously unreleased music track by Rozz Williams, actor biographies, chapter selections, excerpts from the last interview with Rozz conducted by John E. Ellenberger, a copy of the original "super-8" test footage of Pig with commentary by former Christian Death drummer and friend of Rozz, Ignacio Segovia, and finally a 40-page booklet and adaptation of the fictional book featured in the film, Why God Permits Evil. The booklet was Williams only published written work.

The soundtrack accompanying the film—Pig: Original Soundtrack—was composed entirely by Rozz, and included some of his last musical output.

== Plot ==

Running about 23 minutes, Pig is a disjointed, heavily stylized and minimalistic film depicting a serial killer (Williams) journeying out to an abandoned house in Death Valley where he proceeds to ritualistically murder an unidentified man (Hollan). The film details their unspoken relationship through sadomasochism and symbolism.

The movie opens with the scene of the black-garbed killer preparing to disembark from a small house. The individual places several items into a brief case, appearing among them a deck of cards, a notebook, and a copy of the childrens novel Mr. Pig and Sonny Too — an actual publication written in 1977 by the American author Lillian Hoban. (The books title bears an obvious connection to the films.)

The individual closes up his brief case, standing upright and turning to reveal on camera the bleeding, half-naked body of another man lying prostrate on the floor. The living man kicks aside the corpse of the dead one and continues downstairs, placing his belongings into a black car and driving away along a deserted road.

The driver continues through the outstretched desert with power lines lining the side of the dirt trail. His vehicle passes an outcrop of rocks with an inscription painted on its face. Although perhaps being unintelligible, it appears to read either as "ELLE" or "ELLIE". Another sign crops up immediately after, written in black, reading Dead Man’s Point though this is almost unreadable.

The film cuts to another individual, the unidentified man (Hollan), masked in white bandages covering his head. He wanders through the desert until reaching a telephone pole and sitting down, cross-legged.

The drivers car pulls up to the side of the road, and the masked figure enters on the passengers side. The two drive down another path and stop at a small house. Upon exiting the vehicle the film continues to a shot of the driver/killer leading the masked figure by a rope which binds his wrists, all the while carrying his brief case. The dilapidated doorway to the house is shown to have the number "1334" laid out in small bones at the top, otherwise adorned with dominoes, a crucifix, caution tape, a glove, a knife, and various photographs of someones arms and head being bound.
 gauze tape, and then finally at the bottom of the doorway a box of dominoes opens up to reveal a keyhole. Through the keyhole a pig mask can be seen sticking through a broken wall. It is revealed that the killer is wearing the mask and pacing about the house.
 die with a chaosphere engraved on one side, a bondage mask, gloves, a chain, a bowie knife and other assorted cutlery, a cleaver, a pair of pliers, papers, at least two pairs of scissors, a plastic funnel with a feeding tube, and (perhaps most importantly) a book entitled Why God Permits Evil, among other items.

The captor places his hand on the head of the now once-more bound-to-the-ceiling victim and pulls part of the bandage-mask from his eye, exposing his darting gaze and sense of fear. A collection of pliers, knives, scissors, syringes, and other instruments from the briefcase can be seen below.

In the next scene the killer is turning through the ages of Why God Permits Evil, revealing strange, graphic art and religious quotes. The camera then cuts to the victim, his head still gauzed, with the feeding tube in his mouth. The killer pours what appears to be blood through the funnel, into victims mouth, causing him to gag and choke, spitting it up onto his naked torso.

== External links ==

*  
*  
*  

 

 
 
 
 
 
 