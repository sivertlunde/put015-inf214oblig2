List of horror films of 1984
 
 
A list of horror films released in 1984 in film|1984.

{| class="wikitable sortable" 1983
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | The Barbaric Beast of Boggy Creek, Part II
| Charles B. Pierce || Charles B. Pierce, Cindy Butler, Chuck Pierce ||   || {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:Boggy Creek II (USA), Boggy Creek II: And the Legend Continues (USA).}} 
|-
!   | Bog (film)|Bog
| Don Keeslar || Gloria DeHaven, Aldo Ray, Marshall Thompson ||   ||  
|- Children of the Corn
| Fritz Kiersch || Peter Horton, Linda Hamilton ||   ||  
|-
!   | C.H.U.D. John Heard, Daniel Stern ||   ||  
|-
!   | The Company of Wolves David Warner, Stephen Rea ||   ||  
|-
!   | Deadly Intruder Daniel Greene ||   ||  
|-
!   | Dont Open Till Christmas
| Edmund Purdom || Ricky Kennedy, Ray Marioni, Kelly Baker ||   ||  
|-
!   |   Barbara Howard ||   ||  
|-
!   | Gremlins
| Joe Dante || Hoyt Axton, Zach Galligan, Corey Feldman ||   ||  
|-
!   | Hunters of the Night
| Visa Mäkinen || Sari Sarmas, Reijo Kokko, Sirpa Taivainen ||   ||
|- The Initiation
| Larry Stewart || Vera Miles, Daphne Zuniga, James Read ||   ||  
|-
!   | Innocent Prey
| Colin Eggleston || P.J. Soles, Martin Balsam ||   ||
|-
!   | Invitation to Hell
| Wes Craven || Robert Urich, Joanna Cassidy ||   ||  
|-
!   | Monster Dog
| Claudio Fragasso || Alice Cooper, Victoria Vera, Carlos Sanurio ||    ||
|-
!   | Night of the Comet
| Thom Eberhardt || Kelli Maroney, Catherine Mary Stewart ||   ||  
|-
!   | A Nightmare on Elm Street John Saxon, Ronee Blakley ||   ||  
|- The Prey
| Edwin Brown || Debbie Thureson, Steve Bond ||   ||
|-
!   | Purana Mandir
| Shyam Ramsay, Tulsi Ramsay || Mohnish Bahl, Arti Gupta, Ajay Agarwal ||   ||  
|-
!   | Razorback (film)|Razorback
| Russell Mulcahy || Gregory Harrison ||   ||  
|-
!   | Satans Blade
| L. Scott Castillo Jr. || Tom Bongiorno, Stephanie Leigh Steel, Thomas Cue ||   ||  
|-
!   | Scream for Help
| Michael Winner || Rachael Kelly, Marie Masters, David Allen Brooks ||   ||  
|-
|The Sea Serpent || Amando de Ossorio || Timothy Bottoms, Taryn Power, León Klimovsky ||   ||
|-
!   | Silent Madness
| Simon Nuchtern || Belinda Montgomery, Viveca Lindfors, Katherine Kamhi ||   ||  
|-
!   | Silent Night, Deadly Night
| Charles E. Sellier Jr. || Lilyan Chauvin, Leo Geter, Linnea Quigley ||   ||  
|-
!   | Splatter University
| Richard Haines || Francine Forbes, Cathy Lacommaro, Ric Randig ||   ||  
|-
!   | Strangler vs. Strangler
| Slobodan Sijan || Tasko Nacic, Nikola Simic, Srdjan Saper ||   ||  
|-
!   | Superstition
| James W. Roberson || James Houghton, Albert Salmi ||   ||  
|-
!   | The Tenant
| Ronny Yu || Yun-Fat Chow, Sally Yeh, Melvin Wong ||   ||
|-
!   | Terror in the Aisles Nancy Allen ||   ||  
|}

==References==
 

 
 
 

 
 
 
 