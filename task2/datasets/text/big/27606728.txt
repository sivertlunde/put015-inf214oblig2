Todu Dongalu (1954 film)
{{Infobox film
| name           = Todu Dongalu
| image          =
| image_size     =
| caption        =
| director       = D. Yoganand National Art Theater, Madras
| writer         = Samudrala Ramanujacharya
| narrator       =
| starring       = N.T. Rama Rao Gummadi Venkateswara Rao T. G. Kamala Devi Chalam Maddali Krishnamurthy Atluri Pundarikakshaiah Hemalatha Sivaramakrishnaiah
| music          = T. V. Raju
| cinematography = M. A. Rehman
| editing        =
| studio         =
| distributor    =
| released       = April 15, 1954
| runtime        = 142 minutes
| country        = India Telugu
| budget         =
}}
Todu Dongalu is a 1954 Telugu drama film directed by D. Yoganand. The title roles of Todu Dongalu (meaning Joint Thieves) are played by N. T. Rama Rao and Gummadi Venkateswara Rao. 

==Plot==
Lokanatham (Gummadi) is the proprietor of Annapurna Rice Mill, and Paramesam (N.T. Ramarao) works as Gumasta under him. Both characters compose the team Todu Dongalu and commit various crimes. One day, one of the factory employees Ramudu (Maddali) dies of electrocution. Todu Dongalu throw his dead body from the hill top without the knowledge of the other employees. Paramesam feels guilty and suffers from a heart problem, and the ghost of Ramudu tries to empower Paramesam to stay alive. Several doctors declare that he would not live more than a month, so Paramesam goes to a nearby town for peace of mind. He encounters an employee who is living an idealist life. This transforms him and he returns to the village, convinces his proprietor and sanctions compensation of 2,000   to the family of the bereaved Ramudu. This relieves him from the mental anguish he suffered. The proprietor hands over the factory keys to him with confidence.

==Soundtrack==
* "Unnateeru Neevunnadi Undi" (Singer: Ghantasala Venkateswara Rao) 

==Awards==
*2nd National Film Awards (1954) - National Film Award for Best Feature Film in Telugu - Certificate of Merit    

==References==
 

==External links==
*  
 

 
 
 
 
 


 