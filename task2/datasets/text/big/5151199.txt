Riens du tout
{{Infobox film
| name           = Riens du Tout
| image          = 
| caption        =
| director       = Cédric Klapisch
| producer       = Adeline Lecallier 
| writer         = Cédric Klapisch Jackie Berroyer
| narrator       =
| starring       = Fabrice Luchini Daniel Berlioux
| music          = Jeff Cohen 
| cinematography = Dominique Colin 
| editing        = Francine Sandberg 
| distributor    = MKL Distribution
| released       =  
| runtime        = 95 minutes
| country        = France French
| budget         =
}}
Riens du tout (English:Little Nothings) is a 1992 French comedy film directed by Cédric Klapisch. The screenplay was written by himself with Jackie Berroyer. It features Fabrice Luchini, Daniel Berlioux, Marc Berman, Jean-Pierre Darroussin, Odette Laure, Zinedine Soualem, Simon Abkarian and others.

== Plot ==
For his first full-length film, Cédric Klapisch relates the story of a Parisian department store through its employees. A new director, Monsieur Lepetit, is named at the head of the company so as to save it from bankruptcy. To this end, he decides to create a company spirit with group psychotherapy, bungee jumping and other tricks. In this first film Klapischs main purpose was to show the difference between the behavior of a person when he is alone and when he is in a group. In order to show this, he filmed the employees during their commute, at work and in other scenes of daily life, which made Klapisch styles reputation. This topic will become recurrent in many of his films. At the end of the film he succeeds in saving the company, nevertheless the store is sold to build a hotel. This theme is still of topicality today and makes this film interesting although it is over 20 years old.

==Cast==
* Fabrice Luchini as Lepetit
* Daniel Berlioux as Jacques Martin 
* Marc Berman as Pizzuti 
* Olivier Broche as Lefèvre 
* Antoine Chappey as François 
* Jean-Pierre Darroussin as Domrémy 
* Aurélie Guichard as Vanessa 
* Billy Komg as Mamadou 
* Odette Laure as Madame Yvonne  
* Elisabeth Macocco as Madame Dujardin 
* Marc Maury as Johnny Bonjour 
* Pierre-Olivier Mornas as Roger 
* Jean-Michel Martial as Hubert
* Maïté Nahyr as La directrice de coordination 
* Fred Personne as Monsieur Roi 
* Lucette Raillat as Micheline  
* Eric Forterre as Fred  
* Nathalie Richard as Claire  
* Marie Riva as Zaza  
* Sophie Simon as Pat   
* Zinedine Soualem as Aziz   
* Marina Tomé as Monitrice sourire   
* Karin Viard as Isabelle 
* Coraly Zahonero as Véronique 
* Simon Abkarian as Le danseur grec
* Olivier Rabourdin as Chanteur métro 

==External links==
* 

 

 
 
 
 
 
 
 

 