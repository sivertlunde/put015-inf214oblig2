Andaz (1994 film)
{{Infobox film
| name           = Andaz 
| image          = 
| caption        =
| director       = David Dhawan|
| producer       = Pahlaj Nihalani
| writer         = K. Bhagyaraj (original story), Anees Bazmee (screenplay & dialogue)
| starring       = Anil Kapoor Juhi Chawla Karishma Kapoor Kader Khan
| music          = Bappi Lahiri, Amar Mohile
| cinematography = Siba Mishra
| editing        = Nand Kumar
| distributor    = 
| released       =  8 April 1994
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}}

Andaz is 1994 Hindi Movie directed by David Dhawan and starring Anil Kapoor, Juhi Chawla, Karishma Kapoor, Kader Khan. 
Other cast members include Raj Babbar, Shakti Kapoor, Satish Kaushik, Ishrat Ali, Mahesh Anand, Vikas Anand, Tej Sapru. It is a Remake of 1992 Telugu film Sundarakanda starring Venkatesh, which was a remake of Tamil film Sundara Kandam starring K. Bhagyaraj and Bhanupriya.

==Plot==

Ajay (Anil Kapoor) an intelligent ex-student of the S. T. School is appointed in the same school as a teacher. Jaya (Karisma Kapoor) who studies in his class harasses him by playing mischievous pranks. Ajays refusal to enter into a romantic relationship with Jaya upsets her. She challenges him that she will one day become his wife. In a hurry he marries Saraswati (Juhi Chawla), an illiterate orphan girl. Jaya befriends Saraswati by teaching her everything from cooking to reading and writing. One day, some terrorists attack the school and take the children as hostages. Ajay, Jaya and all are trapped. In between Jaya gives her life to save Ajay, there Saraswati reveals that she knows about Jayas love for Ajay and the same feelings were in Ajays heart which he never discloses due to his faithfulness towards his wife. Jaya dies at the end of the film.

==Cast==

* Anil Kapoor - Ajay Kumar
* Juhi Chawla - Saraswati
* Karisma Kapoor - Jaya
* Raj Babbar - Captain
* Kader Khan - Principal
* Shakti Kapoor - Shagun
* Satish Kaushik - Panipuri Sharma
* Ishrat Ali - Indian Army Officer

== External links ==
*  

 

 

 
 
 
 
 
 


 