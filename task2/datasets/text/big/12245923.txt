16 Days in Afghanistan
{{Infobox Film
| name           = 16 Days in Afghanistan 
| image          = 16_days_poster.jpeg
| caption        = Film poster
| director       = Anwar Hajher
| producer       = Mithaq Kazimi
| writer         = 
| starring       = 
| music          = Ahmad Shah Hassan, Larry Porter, Hariprasad Chaurasia
| cinematography = Karim Hamdard
| editing        = Mithaq Kazimi
| distributor    = KDK Factory
| released       =  
| runtime        = 60 minutes
| country        = United States, Afghanistan, India Dari 
| budget         = 100,000 USD  
| gross          = 
}}
16 Days in Afghanistan is a 2007 documentary about the journey of Afghan-American Anwar Hajher (director) traveling to his homeland Afghanistan after 25 years to rediscover his country. The film is produced by Mithaq Kazimi and is the first documentary since the fall of Taliban to be shot in those provinces which remain under the heavy influence of the Taliban. It is also the first professionally produced documentary about Afghanistan by a team of Afghan filmmakers and has become a reference film used by many, including Penguin Bookss study guides about Afghan-related books.

It is perhaps one of the earliest and most prominent documentaries to date that has been made in and about Afghanistan and shows in many top lists, including IMDbs Most Popular Titles by country  and on Amazons bestselling movies and TV on Afghanistan right after the Oscar nominee, Restrepo (film)|Restrepo.  It was selected as part of the first Afghan art exhibit show in the British Museum opened by president Hamid Karzai. 

== Cultural heritage ==
In 2011, the film was selected by the   March–May, 2011.  

==Impact==
16 Days in Afghanistan has also been shown in many television stations, film festivals and non-profit events  around the world. It is in circulation in many public school and university libraries.  It has become one of the major and key documentaries about Afghanistan and is used by many people and organizations as a resource. Some include the official study guide from Penguin Group for Khaled Hosseinis novel, A Thousand Splendid Suns.  The China International Television Corporation have used the documentary to explore and better understand the culture of Afghanistan to conduct their videoconferencing on Afghanistan. 

Afghan critics and scholars have hailed the film for being neutral to the political and tribal issues while other documentaries often take sides. Wasef Bakhtari, renowned poet and historian has commented on the ability of the director to speak both Pashtu and Dari natively, while also communicating perfectly in English.  Other publications covering the film and the filmmakers include a through interview in Peyk magazine. 
 Raindance selection, Where My Heart Beats. 
 Beyond Belief Sundance selection, Afghan Star.
 National Geographic. 

==Plot==
The film is divided into 16 days in which the director discusses a different issue with the Afghan people.

Day 1 : He arrives in Kabul Airport and meets the family. A short introduction of the history of Afghanistan.
 Blue Mosque of Mazar-e Sharif and discusses the situation with the caretakers, guards, and the ulemas who are there. An inside look into the historical artifacts of the mosque. An interview with a westerner who visited Afghanistan in the 1970s and discusses Islam and the Taliban.

Day 3 : Interview with the colonel of the Afghan National Army about the status of women in Afghanistan. Interview with a businesswoman who sells mantu on the street. Interview with street children testing their education and questioning them about school. Interview with a traffic police on how the economy has effected the number of cars and drivers.

Day 4 : Interviews with a kebab seller and an electronic seller about their business. Interview with a street woman and her view about the government and president Hamid Karzai. Interview with a blind street singer. He has his fortune read by a woman fortune teller.

Day 5 : Interview with a doctor and pharmacist about medicine and foreign medical aid in Afghanistan. Interview with a sickle-maker about his business and what he thinks of life.

Day 6 : Interview with a former representative of Iranian Cinema about her perception of the people. Interview with a street photographer, comparing standard film to digital photography.  Interview with a butcher and his customer about business and health.

Day 7: On the way to Kabul, he eats and introduces Afghan cuisine. Interview with a UN representative and employee about UN activities in Afghanistan. The representative discusses land mines and how they affect peoples lives. Hajher recalls his last days in Afghanistan during the Cold War.

Day 8: Interviews with many day laborers and how the current political and economic system affects their lives.

Day 9:Interview with the founder of Afghan Human Rights Committee about the involvement of United States and western powers in Afghanistan and terrorism.

Day 10: Hajher visits an illegal local hashish bar to interview hashish sellers, users and addicts. People discuss why they smoke, how it affects their lives and why they dont drink alcohol.

Day 11: Visiting the businesses in the popular Chicken Street and how it has changed or remain the same over the years. Interview with the sellers and western visitors who speak about the culture of Afghanistan.

Day 12: Hajher visits his village to meet his extended family. Greetings and interviews with the family about religion, politics and the way of life in the United States.

Day 13: Hajher faces death for a moment when he thinks that his village barber is going to cut his neck. The head of the village, the religious figure and other known figures come to meet and question him.

Day 14: He visits his dads enemy to make peace and visit his old house which was taken over by the enemies of his father.

Day 15: Leaving Afghanistan, farewell to family and friends.
 Georgia airport. A montage of his recollections of the people he met and the places he visited.

==Cast and crew==
The film is directed by Anwar Hajher, an Afghan-American anthropologist and current Afghan cultural advisor and professor in Georgia. Mithaq Kazimi, has produced the film. The score is composed by Ahmad Shah Hassan and Larry Porter with additional music by Hariprasad Chaurasia. Cast include Peace Corps volunteers, spokesperson for the Afghan National Army, former representative of Iranian cinema in Afghanistan and former western photographers and professional working in Afghanistan.

The film also includes interviewees with the Afghan people from bread sellers to fortune tellers to heroin users to doctors to Mullahs to UN officials and ordinary school children.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 