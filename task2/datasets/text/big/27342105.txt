Noy (film)
{{Infobox film
| name           = Noy
| image          = Noy the movie poster.jpg
| caption        = Theatrical poster
| director       = Dondon Santos
| producer       =  
| eproducer      = 
| aproducer      = 
| writer         = Shugo Praico
| starring       = Coco Martin Erich Gonzales Cherry Pie Picache Joem Bascon Cheska Billiones Vice Ganda Baron Geisler
| music          = 
| cinematography = 
| editing        = 
| studio         = CineMedia
| distributor    = Star Cinema
| released       =  
| runtime        = 
| country        = Philippines
| language       =  
}}
 Best Foreign Language Film at the 83rd Academy Awards.    However, the film didnt make the final shortlist.   

==Plot== Benigno "Noynoy" 2010 Philippine National elections.

It started when Noy, who has ambition to be a news reporter, faked his school records to enter a major TV station, owned by Jane. As a reporter, he was assigned to cover Senator Benigno Aquino, Sr.|Sen. Noynoys presidential campaigns everywhere. Meanwhile, his girlfriend, Divine, initially discouraged him,was forced to agree. He covered Senator Aquinos campaigns from Luzon, Visayas and Mindanao, everytime, from sunrise to midnight, from live coverage to record editing. His older brother, Bong, crippled by polio, jealous at Noy at his lucky streak, unintentionally joined a notorious group at drug dealing.

Meanwhile, some jealous TV presenters investigated Noys background. They reported it on Jane. Noy was immediately summoned and fired when they found out about his fake identity, but gave him one last shot to cover Sen. Aquinos campaign in Tarlac.

He returned in his home in Artex Compound, just to see his brother being mauled by two thugs. He was spotted and killed by one of the thugs, falling his body in the floodwater.

In the end of the film, Noys family observed his death by lighting in the front of his portrait. Simultaneously, Senator Noynoy made his speech in front of the crowds gathered during his campaign.

The film, infused with actual documentary footage inter-cut with dramatic scenes mixed with countless presidential campaign slogans, deals with themes of poverty, survival and hope for the Filipino family.

==Cast==
*Coco Martin  as Manolo "Noy" Agapito
*Cherry Pie Picache as Letty
*Joem Bascon as Bong
*Erich Gonzales  as Divine
*Cheska Billiones as Tata
*Baron Geisler as Caloy
*Vice Ganda as Jane
*Ketchup Eusebio as Harold
*Pen Medina as Nick
*Jhong Hilario as Drug Thug 1
*Kristofer King as Drug Thug 2
*Tess Antonio
*Janus del Prado
*Ping Medina
*Neil Ryan Sese
*Karen Davila
*Liz Uy

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Philippine submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 