The Dying Detective (film)
{{Infobox film
| name           = The Dying Detective
| image          = 
| image_size     = 
| caption        = 
| director       = Maurice Elvey
| producer       = 
| writer         = Arthur Conan Doyle (story "The Adventure of the Dying Detective") William J. Elliott (scenario)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Germain Burger
| editing        = 
| studio         = 
| distributor    = 
| released       = 1921
| runtime        = 28 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Dying Detective is a 1921 British short film directed by Maurice Elvey.

== Plot summary ==
 

=== Differences from source ===
 

== Cast ==
*Eille Norwood as Sherlock Holmes
*Hubert Willis as Dr. John Watson
*Cecil Humphreys as Culverton Smith
*Joseph R. Tozer as His Servant
*Mme. dEsterre as Mrs. Hudson

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 
 