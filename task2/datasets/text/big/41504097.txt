Marumagan
{{Infobox film
| name           = Marumagan
| image          = 
| image_size     =
| caption        = 
| director       = Manivasagam
| producer       = K. Balu
| writer         = Manivasagam K. J. Anumohan  (dialogues) 
| starring       =   Deva
| cinematography = A. Venkatesh
| editing        = P. Mohanraj
| distributor    =
| studio         =  K. B. Films
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1995 Tamil Tamil drama Karthik and Meena in Deva and was released on 7 July 1995. The film, released to negative reviews, failed at the box office.     

==Plot==

After working in Bombay as a tailor, Thangarasu (Karthik (actor)|Karthik) comes back to home and opens a tailor shop 
in his neighborhood. His speciality is taking measurements without touching the women, his shop becomes quickly very popular among the women. Thangarasu falls in love with Manjula (Meena (actress)|Meena), the daughter of Mayilsamy Gounder (Radha Ravi). Mayilsamy Gounder, an ex-Member of Parliament and an illicit liquor smuggler,  plans a return to politics. In the meantime, Thangarasu and Manjula announce their love on a TV Channel. Mayilsamy Gounder is first angry but he uses this opportunity to win the upcoming election. Everything was fine until that Thangarasus mother Thaiyamma (Manorama (Tamil actress)|Manorama) learns the news and she objects for the marriage. What transpires later forms the crux of the story.

==Cast==
  Karthik as Thangarasu Meena as Manjula
*Radha Ravi as Mayilsamy Gounder
*Nagesh
*Goundamani as Govindsamy Senthil
*Manorama Manorama as Thaiyamma
*R. Sundarrajan as Muniyandi Ponnambalam
*Kumarimuthu as Arumugam
*Suryakanth as Sengaliappan 
*Chitti Sangeeta
*Kokila
*Lalitha Kumari as Kannamma
*Singamuthu
*Tirupur Ramasamy
*Kovai Senthil
*Chitraguptan
*Veerapathiran
*Periya Karuppu Thevar
*Jeeva
*Rajavanan
 

==Soundtrack==

{{Infobox Album |  
| Name        = Marumagan
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1995
| Recorded    = 1995 Feature film soundtrack |
| Length      = 28:21
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1995, features 6 tracks with lyrics written by Vairamuthu.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || En Rasave || S. Janaki || 4:21
|- 2 || Laila Laila || S. P. Balasubrahmanyam, K. S. Chithra || 5:28
|- 3 || Manorama || 4:49
|- 4 || Manjula Manjula || Suresh Peters, Malgudi Subha || 5:04
|- 5 || Oh Ragini  (solo)  || S. P. Balasubrahmanyam || 3:26
|- 6 || Oh Ragini  (duet)  || S. P. Balasubrahmanyam, S. Janaki || 5:13
|}

==Reception==
The film received negative reviews. Balaji Balasubramaniam stated: "Marumagan is a movie where the whole somehow seems better than the sum of its parts. Seen individually, the movie offers nothing new in any department. The romance is uninteresting, the lovers face the age-old problem of opposition from their parents and the mother sentiment is put to full use. But the movie as a whole offers passable entertainment   until the last quarter when things completely fall apart." 

==References==
 

 
 
 
 
 