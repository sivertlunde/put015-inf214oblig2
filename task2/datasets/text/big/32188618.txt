Anatomy of a Marriage: My Days with Jean-Marc
{{Infobox film
| name           = Anatomy of a Marriage: My Days with Jean-Marc
| image          =
| caption        =
| director       = André Cayatte
| producer       = Raymond Froment (producer)
| writer         = Maurice Aubergé (collaboration) André Cayatte (adaptation) Louis Sapin (adaptation dialogue)
| starring       = 
| music          = Louiguy
| cinematography = Roger Fellous
| editing        = Paul Cayatte
| distributor    =
| released       = 1964
| runtime        = 112 minutes
| country        = France Italy West Germany
| language       = French
| budget         =
| gross          =
}}

Anatomy of a Marriage: My Days with Jean-Marc (Jean-Marc ou La vie conjugale) is a 1964 French film directed by André Cayatte telling the story of a marriage break-up told from the womans point of view.

The films companion piece   tells the story from the mans point of view.

== Cast ==
*Jacques Charrier as Jean-Marc
*Marie-José Nat as Françoise
*Michel Subor as Roger
*Macha Méril as Nicole
*Alfred Adam as Fernand Aubry
*Giani Esposito as Ettore
*Jacques Monod as Rouquier
*Yves Vincent as Granjouan
*Blanchette Brunoy as Mme Aubry
*Jacqueline Porel as Line
*Jean-Henri Chambois as Le président
*Rosita Fernández
*Anne Caprile as Mme Monier
*Yvan Chiffre as Christian
*Jean Léon
*Corinne Armand as Christine
*Julien Verdier as Un portier
*Marcel Pérès as Le locataire
*Madeleine Suffel as La locataire
*Micheline Sandrel as Une speakerine
*Henri Crémieux as Rancoule
*Michel Tureau as Milou
*Marie-Claude Breton as Minouche
*Michèle Girardon as Patricia
*Georges Rivière as Philippe

== Reception ==
In a joint review of the two films, Bosley Crowther of the New York Times wrote that the two main actors "skillfully   the characteristics of nobility and selflessness or pettiness and shame" but that "the two main characters in these films are distinctly commonplace people, inadequate to responsibility, immature and hardly worth the exceptional attention that is given to them". 

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 


 
 