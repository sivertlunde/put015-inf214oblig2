Manzil (1979 film)
{{Infobox film
| name           = Manzil
| director       = Basu Chatterjee
| producer       = Jai Pawar Raj Prakash Rajiv Suri
| writer         = Ashish Burman
| starring       = Amitabh Bachchan Moushumi Chatterjee
| music          = Rahul Dev Burman
| cinematography = K.K. Mahajan
| editing        = G.G. Mayekar	 	
| released       =    
| country        = India Hindi
}}
 Indian Bollywood film directed by Basu Chatterjee. The film was similar to the Bengali film Akash Kusum (1965), directed by the veteran parallel film maker Mrinal Sen and starring Soumitra Chatterjee and Aparna Sen in the lead. Manzil stars Amitabh Bachchan and Moushumi Chatterjee in pivotal roles. The film won critical acclaim for its story, while Bachchan was praised for his performance, although, the film did only average business at the box office.

==Plot==
The story of the movie starts with a young man Amitabh having big dreams. He falls for a rich girl Moushumi. She falls for him as well. He pretends to be rich not to disappoint her and takes help from his friend for a suit, the car and the flat claiming all his own. He wants to start a galvanometer business by buying old ones and refurbishing them to workable ones with the help of A.K. Hangal. Unfortunately the big cats try to buy him out but Amitabh doesnt budge. So, they buy A.k. Hangal by bribing him with money for his daughters wedding. This leads to a big failure for Amitabh whose mother Lalita Pawar has to give the insurance money to her son to bail him. It still doesnt work and Amitabh is taken to court by Moushumis father, a lawyer. Lalita Pawar encourages her son to repair the galvanometers himself and when he falls short of money, sells her gold jewellery. The prosecutor withdraws the case as the galvanometers are now repaired. His friend Prakash Mariwalla also helps him with ten thousand rupees in disguise of an order.

==Cast==
* Amitabh Bachchan as Ajay Chandra
* Moushumi Chatterjee as Aruna Khosla
* Rakesh Pandey as Prakash Mariwalla
* Satyendra Kappu as Mr. Khosla
* A.K. Hangal as Anokhelal
* Lalita Pawar as Mrs. Chandra
* Shreeram Lagoo as Mr. Kapoor
* Urmila Bhatt as Mrs. Khosla
* Chandrashekhar Dubey as Tolaram Chhaganmal

==External links==
* 

 

 
 
 
 


 