Sleeping Beauty (1942 film)
{{Infobox film
| name = Sleeping Beauty 
| image =
| image_size =
| caption =
| director = Luigi Chiarini
| producer = 
| writer =  Pier Maria Rosso di San Secondo  (play)   Umberto Barbaro     Vitaliano Brancati   Luigi Chiarini 
| narrator =
| starring = Luisa Ferida   Amedeo Nazzari   Osvaldo Valenti   Teresa Franchini 
| music = Achille Longo  
| cinematography = Carlo Montuori 
| editing = Maria Rosada  
| studio =   Società Italiana Cines ENIC 
| released = 6 September 1942
| runtime = 90 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Sleeping Beauty (Italian:La bella addormentata) is a 1942 Italian drama film directed by Luigi Chiarini and starring Luisa Ferida, Amedeo Nazzari and Osvaldo Valenti.  The film was screened at the 1942 Venice Film Festival. It is based on a 1919 play by Pier Maria Rosso di San
Secondo.

==Cast==
* Luisa Ferida as Carmela 
* Amedeo Nazzari as Salvatore detto Il Nero della solfara  
* Osvaldo Valenti as Don Vincenzo Caramandola  
* Teresa Franchini as Zia Agata  
* Pina Piovani as Nunziata  
* Margherita Bossi as Donna Concetta, la barista 
* Giovanni Dolfini as Isidoro 
* Guido Celano as Lo solfataro 
* Angelo Dessy as Un altro solfataro 
* Fiorella Betti as Erminia detta "Pepespezie" 
* Gildo Bocci as Un mercante

== References ==
 

== Bibliography ==
* Brunetta, Gian Piero. The History of Italian Cinema: A Guide to Italian Film from Its Origins to the Twenty-first Century.  Princeton University Press, 2009. 
* Moliterno, Gino. The A to Z of Italian Cinema. Scarecrow Press, 2009. 
 
== External links ==
*  

 

 

 
 
 
 
 
 
 