The Cyclops (film)
 
{{Infobox film
| name           = The Cyclops
| image          = The Cyclops.jpg
| image_size     =
| caption        = Theatrical poster to The Cyclops (1957)
| director       = Bert I. Gordon
| producer       = Bert I. Gordon
| writer         = Bert I. Gordon
| narrator       = James Craig Gloria Talbott and Duncan Parkin
| music          = Albert Glasser
| cinematography = Ira H. Morgan
| editing        = Carlo Lodato Allied Artists
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} James Craig, Lon Chaney Jr. and Gloria Talbott. Typical of the "B" movie features of the period, a modicum of production values was involved.

==Plot== James Craig), mining expert Martin "Marty" Melville (Lon Chaney Jr.) and pilot Lee Brand (Tom Drake) fly into an unknown territory. While searching the area, however, they uncover giant mutated earth animals such as giant snakes and lizards, behemoth bugs, oversized mammals. 

More importantly, they encounter a mutated 50-ft tall, one-eyed human monster who became disfigured due to an exposure to radioactivity from massive radium deposits in the area. He kills Melville but appears to recognize the girl. When the cyclops tries to prevent the rest of the group from flying to safety, he is gravely wounded, and dies.

==Cast==
As appearing in The Cyclops, (main roles and screen credits identified):   Turner Classic Movies. Retrieved: April 19, 2012.  James Craig &mdash; Russ Bradford
* Gloria Talbott &mdash; Susan Winter
* Lon Chaney Jr. (credited as Lon Chaney) Martin "Marty" Melville 
* Tom Drake &mdash; Lee Brand
* Duncan Parkin &mdash; Bruce Barton/The Cyclops
* Vincente Padula &mdash; The governor
* Marlene Kloss &mdash; Salesgirl
* Paul Frees &mdash; vocal effects for The Cyclops

==Production==
Duncan Parkin also played Col. Manning in the War of the Colossal Beast, which is the sequel to The Amazing Colossal Man, basically playing the same disfigured giant in both films. 

A clip of "The Cyclops" was used as part of the original opening sequence of WPIX Channel 11 New Yorks "Chiller Theatre" back in the 1960s. 

Production effects were limited to backscreen projection, rudimentary matte work, and incorporating large images into the scenes. In the film, there is a scene in which the creature grabs Susan, but he also grabs the background as well, revealing the black color behind it. The discovery of the test pilots aircraft involves dissimilar and haphazard parts scattered about debris in the form of a light aircraft wing, a North American P-51 Mustang|P-51 Mustang canopy and a radial engine.

==Release==
Released as a double-feature with Daughter of Dr.Jekyll.

==References==
 

==Further reading==
* Wingrove, David. Science Fiction Film Source Book. London: Longman Group Limited, 1985. ISBN 978-0582893108.

== External links ==
*  
*  

 

 
 
 
 
 
 
 