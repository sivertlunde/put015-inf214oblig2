Jeevana Jokali
{{Infobox film
| name           = Jeevana Jokali
| image          =
| caption        = 
| director       = Geethapriya
| producer       = 
| writer         = 
| screenplay     =  Gangadhar Bharathi Bharathi Suma T. N. Balakrishna
| music          = Vijaya Bhaskar
| cinematography = V Manohar
| editing        = 
| studio         = 
| distrubutor    = 
| released       =  
| country        = India Kannada
}}
 1972 Cinema Indian Kannada Kannada film, directed by Geethapriya. The film stars Gangadhar (Kannada actor)|Gangadhar, Bharathi Vishnuvardhan|Bharathi, Suma and T. N. Balakrishna in lead roles. The film had musical score by Vijaya Bhaskar.   

==Cast== Gangadhar
*Bharathi Bharathi
*Suma
*T. N. Balakrishna
*M. N. Lakshmi Devi

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || Geetha Priya || 03.30
|- Susheela || R. N. Jayagopal || 04.52
|-
| 3 || Kannallindu || K. J. Yesudas || Chi. Udaya Shankar || 04.40
|}

==References==
 

==External links==

 

 
 
 
 


 