Poor Baby
 
{{Infobox film
| name           = Poor Baby 
| image          = 
| image size     = 
| caption        = 
| director       = Will Louis
| producer       = 
| writer         = Elizabeth Miller
| narrator       = 
| starring       = Raymond McKee
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}
 silent comedy film starring Raymond McKee and featuring Oliver Hardy in a bit role.   

==Cast==
* Raymond McKee - Pete, a tramp
* Lucille Allen - Mrs. Smith
* Lou Gorey - Mrs. Jones
* Leonora Van Benschoten - Margy, her baby
* Guido Colucci - Paul Jones
* Alice Grey - Servant girl
* Harry Eytinge - The Sheriff
* Andy Clark - Tommy (as Andrew J. Clark)
* Caroline Rankin - Matilda Jenkins
* Oliver Hardy - Matildas sweetheart (as O.N. Hardy)

==See also==
* List of American films of 1915
* Oliver Hardy filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 