Yahudi Ki Ladki (1957 film)
{{Infobox film
| name           = Yahudi Ki Ladki
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = S.D. Narang
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Madhubala Pradeep Kumar
| narrator       = 
| music          = Hemant Kumar S. H. Bihari  (lyrics) 
| cinematography = 
| editing        = 
| studio         = New Oriental Pictures
| distributor    = 
| released       = 1957 
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          =  
}} 1957 Indian Hindi drama film directed by S.D. Narang, starring Madhubala and Pradeep Kumar as leads.      director and producer, S.D. Narang is known for Shehnai (1964 film)|Shehnai (1964) and Dilli Ka Thug (1958).

==Cast==
* Madhubala
* Pradeep Kumar Krishna Kumari
* Smriti Biswas
* Gajanan Jagirdar
* Amar
* Hiralal Helen
* Tun Tun
* Sheela Vaz 
* Premlata

==Music==
The film had music by Hemant Kumar with lyrics by S. H. Bihari.
* "Hum Kisi Se Na Kahenge Chup Chup Se rahenge" - Geeta Dutt, Shamshad Begum
* "Ye Chand Bata Tune Kabhi Pyar Kiya Hai" - Asha Bhosle
* "Duniya Se Dil Lagake, Humse Aankhe Mila Ke" - Geeta Dutt
* "Aa Humse Pyar Kar Le" - Geeta Dutt
* "Ae Khuda Tere Bando Ke Dil" - Ravi (music director)|Ravi, Hemant Kumar
* "Dil Bekarar Mera Kare Intezar Tera" - Hemant Kumar, Asha Bhosle
* "Kar Le Dil Ka Sauda Dil Se" - Geeta Dutt
* "Na Ho Dil Jiske Dil Me" - Geeta Dutt

== References ==
 
==External links==
*  
*  

 
 
 
 
 
 

 