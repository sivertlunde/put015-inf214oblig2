A Shot in the Dark (1964 film)
{{Infobox film
| name           = A Shot in the Dark
| image          = Shot_in_the_dark.jpg
| caption        = Theatrical release poster
| director       = Blake Edwards
| producer       = Blake Edwards
| writer         = Harry Kurnitz
| based on       = LIdiote by Marcel Achard
| screenplay     = Blake Edwards William Peter Blatty
| starring       = Peter Sellers Elke Sommer George Sanders Herbert Lom
| music          = Henry Mancini
| cinematography = Christopher G. Challis
| editing        = Bert Bates Ralph Winters
| studio         = The Mirisch Corporation
| distributor    = United Artists
| released       = June 23, 1964
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $12,368,234 
}}
 Inspector Jacques Clouseau of the French Sûreté.
 Commissioner Dreyfus, and Burt Kwouk as his long-suffering servant, Pink Panther#Cato Fong|Cato, who would both become series regulars. Elke Sommer plays Maria Gambrelli.
 The Pink Panther.

== Plot == Spanish chauffeur, Miguel. The chauffeur was having an affair with the maid, Maria Gambrelli, who claims that he often beat her. Although all the evidence points to Gambrelli as the killer, Clouseau refuses to admit her guilt after he develops an instant attraction to her.

For the real culprits to keep the truth hidden from Clouseaus boss, Commissioner Charles Dreyfus, they must commit more murders. With the murders of Georges the gardener, Dudu (Ann Lynn) the maid, and Henri LaFarge, the head butler, Maria is arrested. Clouseau sets her free each time, but is himself arrested police four times in quick succession (first for selling balloons without a license, then for selling paintings without a license, then for hunting without a license, and finally with Maria Gambrelli for public nudity, after they flee a nudist community). In order to keep his mind sharp for the case, Clouseau eventually instructs his manservant Kato to attack him at every opportunity (which becomes a staple running gag for the rest of the Pink Panther films featuring Sellers).
 doorman instead, extramartial affairs within the household), Maurice the manservant is a blackmailer, and Maria innocent of any crime. The guilty escape in Clouseaus car, but it is destroyed by a bomb set by Commissioner Dreyfus.

At the end of the film, Clouseaus aide-de-camp Hercule LaJoy arrests Dreyfus, who has gone mad over his repeated failures in killing Clouseau. Clouseau and Maria celebrate the clearing of her name with a long and passionate kiss - which is swiflty interrupted by another sneak attack by Kato.

== Cast ==
 
* Peter Sellers as Inspector Jacques Clouseau
* Elke Sommer as Maria Gambrelli
* George Sanders as Benjamin Ballon
* Herbert Lom as Commissioner Charles Dreyfus Tracy Reed as Dominique Ballon
* Graham Stark as Hercule LaJoy
* Moira Redmond as Simone
* Vanda Godsell as Madame LaFarge
* Maurice Kaufmann as Pierre
* Ann Lynn as Dudu David Lodge as Georges
* André Maranne as The_Pink_Panther#François|François Martin Benson as Maurice Kato (spelled films of the series)
* Reginald Beckwith as Receptionist	
* Douglas Wilmer as Henri LaFarge
* Bryan Forbes (credited as Turk Thrust) as Camp Attendant
 

== Production == The Party, and on three more "Pink Panther" films in the 1970s.
 animated opening Inspector Clouseau Pink Panther character in the opening titles. Henry Mancinis theme for this film serves as opening theme and incidental music in "The Inspector" cartoon shorts made by DePatie-Freleng.
 Robert Wells). Meglio Stasera (It Had Better Be Tonight)" in the film The Pink Panther. 

== Reception ==
The movie was one of the 13 most popular films in the UK in 1965. 

The film was well received by critics.  , it has 93% favourable reviews on review aggregator Rotten Tomatoes out of 29 reviews counted. The average rating given by critics is 8 out of 10. 

== Awards and honors ==
American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs - #48

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 