Zone of the Dead
{{Infobox Film
| name = Zone of the Dead
| image = Zone-international.jpg
| producer = Milan Todorović Loris Curci Vukota Brajovic Pierfrancesco Fiorenza
| director = Milan Konjević Milan Todorović
| writer = Milan Konjević Vukota Brajović Milan Todorović
| starring = Ken Foree Kristina Klebe Emilio Roso
| produced by = Talking Wolf Productions
| distributor = Epic Pictures Group
| released = February 22, 2009
| runtime = 96 min. (101 min. directors cut)
| country = Serbia Italy Spain
| language = English 
}}
 2009 Serbian directors Milan Konjević and Milan Todorović.  It stars Ken Foree, most well known for his starring role in Dawn of the Dead in 1978.

==Plot==
The film starts off with a police-escorted prisoner, who is being transported under the supervision of Interpol. The route leads them through the city of Pančevo, where they encounter an ecological holocaust and the people infected by the biological hazard|biohazard, who are trying to kill them. Two Interpol agents, Mortimer Reyes and Mina Milius, soon realize that their only chance for escape from the zombie hordes lies in allying with the dangerous, mysterious prisoner.   

==Cast==
*Ken Foree as Agent Mortimer Reyes 
*Kristina Klebe as Agent Mina Milius 
*Emilio Roso as Prisoner 
*Miodrag Miki Krstović as Belic 
*Vukota Brajović as Armageddon  Bojan Dimitrijević as Rookie Cop
*Ariadna Cabrol as Angela 
*Steve Agnew as Professor
*Iskra Brajović as Yovana 
*Marko Janjić as Jan 
*Živko Grubor as Petrovic
*Nenad Ćirić as president
*Maria Kawecka as presidents secretary
*Vahidin Prelic as agent Bottin
*Vibrator u rikverc as band in the party
*Danilo Bećković as zombie
*Antoni Solé as TV director

==Release==
The film was released on February 22, 2009.  On 1 March 2010 Metrodome released the DVD in the UK and Ireland as Apocalypse of the Dead.  On 1 September 2012 Epic Pictures released it on DVD in North America as Apocalypse of the Dead.  It is interesting that the trailer for this film has more than 10.8 million views on YouTube (as of Nov. 2014), which makes it one of the most popular zombie trailers of all times.

==Sequel==
Zone of the Dead 2 has been planned since 2009. 

==References==
{{Reflist|refs=
   

   
   
   
   
   
   
   
   
   
   
   
}}

== External links ==
* 
 

 
 
 
 
 
 
 
 