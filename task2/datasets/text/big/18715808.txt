Shark City
 

{{Infobox film
| name           = Shark City
| image          = Poster of the movie Shark City.jpg
| caption        = 
| director       = Dan Eisen
| producer       = Justin L. Levine, Howard Kerbal, Steve Muzzo
| writer         = Evan Shear David Phillips  Carlo Rota Corey Haim Vivica A. Fox Jordan Madley Samantha Gutstadt  Tony Nappo  Skye Collyer Michael Gelbart Adam Rodness Sean Tweedley Dylan Ramsey
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} David Phillips, Carlo Rota, Corey Haim, Vivica A. Fox,  Jordan Madley, Samantha Gutstadt,  and Tony Nappo; with Skye Collyer, Adam Rodness, Sean Tweedley, Michael Gelbart, and Dylan Ramsey. 

==Plot==
At the infamous club Shark City, successful roommates – real estate agent  David Phillips) and stock broker Kenny (Jefferson Brown), are the 
popular guys who get all the girls.  
 
One night Dagen meets Samantha (Jordan Madley) 
who he begins to pursue, but as she rejects him time 
and time again, he begins to actually fall in love with 
her though she is the daughter of the very dangerous and powerful mob 
boss Ventura Ritt (Carlo Rota).

As luck would have it, despite the declining housing 
market as reported by trusted news anchor Veronica 
Wolf (Vivica Fox), Dagen is able to sell a million dollar 
home to none other than Ventura Ritt, who is purchasing it for his 
daughter, whom Dagen admires.  
 
Meanwhile, after losing his job as a day-trader, his best friend Kenny, an 
amazing poker player, is in need of a new opportunity to make some cash.  
The two friends scheme a way to get Ventura Ritt to 
play in a poker game to earn their trust.  
 
Their plans succeed, and making some risky bets with 
help of his old secretary, Jen (Sam Gutstadt), Kenny 
starts to make Ventura a lot of money – so Ventura then 
trusts him with a million dollars.  Big mistake.  Kennyʼs luck 
changes, and in an attempt to make Ventura even more 
money, he loses every penny.  
 
After much work Dagen convinces Samantha that he is not 
the club scumbag she thinks he is, and they begin 
successfully dating until a girl from Dagenʼs past shows up unexpectedly 
and kisses him as Samantha sees, confirming her past fears about him. 

The remainder of the movie involves Dagen and Kenny trying to secure the money for the mob boss and for Dagen to regain Samanthas trust, and for the boys to discover each others friendship again.

==Production==
The language of the film is English. 

It was filmed in Toronto, Canada.

The world premiere of the film took place on March 28, 2009 in Los Angeles. In Canada, the film was screened during the Toronto Film Festival (was not part of the TIFF) on Friday, September 11, 2009.

This film has screenings during the 2009 Toronto Film Festival (was not part of the TIFF), Chicago International Film Festival (was not part of the CIFF), and the Temecula Valley International Film Festival (was not part of TVIFF).

The film makes repeated references to Texas Hold Em poker.

== References ==
 
http://www.citytv.com/Toronto/citynews/entertainment/movies/article/4050—shooting-in-sixteen-days-cast-and-crew-of-shark-city-have-no-time-to-waste

http://www.sugarcainentertainment.com/?p=822

http://www.highbeam.com/doc/1G1-182168991.html

== External links ==
*  
*  
* http://photosbytrayc.wordpress.com/2009/09/15/tiff-shark-city/ Photos from Toronto Premiere
* http://www.hype1.com/Toronto/event_pictures/4978/vivica_fox_at_tryst_nightclub.html - Photos from Toronto afterparty
* http://www1.wireimage.com/GalleryListing.asp?navtyp=CAL====390041&str=&styp=&sfld=&nbc1=1&sortval=3a&PageNum=2 - wireimage photos

 
 
 