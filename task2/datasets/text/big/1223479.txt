Central Station (film)
 
{{Infobox film name            = Central Station image           = Central-do-brasil-poster04.jpg caption         = Theatrical release poster director        = Walter Salles producer        = {{Plainlist|
*Arthur Cohn
*Martine de Clermont-Tonnerre
}} screenplay      = {{Plainlist|
*João Emanuel Carneiro
* Marcos Bernstein
}} story           = Walter Salles starring        = {{Plainlist|
*Fernanda Montenegro
* Vinícius de Oliveira
* Marília Pêra
}} music           = {{Plainlist| Antonio Pinto
* Jaques Morelenbaum
}} cinematography  = Walter Carvalho editing         = Felipe Lacerda distributor     = Europa Filmes  released        =   runtime         = 110 minutes country         ={{Plainlist|
*Brazil
*France
}} language        = Portuguese budget          = $2.9 million http://www.the-numbers.com/movies/1998/0CLSN.php  gross           = $22 million {{cite web|url=http://www.asaeca.org/imagofagia/sitio/index.php?option=com_content&view=article&id=132%3Aa-comercializacao-de-um-filme-internacional-central-do-brasil-&catid=40&Itemid=77|title=A comercialização de um filme internacional: Central do Brasil
|work=asaeca.org/|language=|accessdate=}} 
|}}
 French drama main railway station.

==Plot==
 , the most famous and important railway station in Brazil. Also served as the setting and title of the famous film.]] Rio de Janeiros Central Station, writing letters for illiterate customers, in order to make ends meet. She can be impatient with her customers and sometimes does not mail the letters she writes, putting them in a drawer or even tearing them up. Josué is a poor 9-year-old boy who has never met his father, but hopes to do so. His mother sends letters to his father through Dora, saying that she hopes to reunite with him soon, but when she is killed in a bus accident just outside the train station, the boy is left homeless. Dora takes him in and traffics him to a corrupt couple, but she is made to feel guilty by her neighbor and friend Irene and later steals him back.
 Northeast Brazil in order to find his fathers house and leave him there.
 Bom Jesus do Norte. They find his fathers address in Bom Jesus, but the current residents say that Jesus won a house in the lottery, and now lives in the new settlements, adding that he lost the house and money through drinking. With no money, Josué saves them from destitution by suggesting Dora write letters for the pilgrims who have arrived in Bom Jesus for a massive pilgrimage. This time she posts the letters.

They take the bus to the settlements, but when they locate the address they have for Josués father, they are told by the new residents that he no longer lives there and has disappeared. Josué tells Dora that he will wait for him, but Dora invites him to live with her. She calls Irene in Rio and asks her to sell her refrigerator, sofa and television. She says that she will call when she gets settled somewhere. After she hangs up, she learns that there are no buses leaving until the next morning.

Isaías, one of Josués half-brothers, is working on a roof next to the bus stop, and learns that they are looking for his father. After introducing himself, Dora says that she is a friend of his father and was in the area. Isaías insists that she and Josué, who, suspicious of the stranger, has introduced himself as Geraldo, come to dinner. They return to his house, where they meet Moisés, the other half-brother. Later, Isaías explains to Dora that their father married Ana, who he doesnt know is Josués mother, after their mother died, and that nine years ago, while pregnant, Ana left her drunken lover for Rio and never returned. Isaías asks Dora to read a letter his father wrote to Ana when he disappeared, six months ago, in case she returned. In the letter, the boys father explains that he has gone to Rio to find Ana and the son he has never met. He promises to return, asks her to wait for him, and says they can all be together—himself, Ana, Isaías and Moisés. At this point Dora pauses, looks at Josué and says, "and Josué, whom I cant wait to meet." Isaías and Josué both say that he will return, but Moisés doesnt think so. The next morning, while the sons sleep, Dora sneaks out to catch the bus for Rio.  She first leaves beside the letter from Jesus the one from Ana to Jesus, the one Dora carried with her from the Central Station but never mailed, expressing Anas wish for the family to be reunited. Josué wakes up too late to prevent her departure. Dora writes a letter to Josué on the bus. Both are left with the photos they had taken by which to remember one another.

=== Production === French Ministry of Culture to receive resources of Fonds Sud Cinema, for their funding. 

===Cast===
* Fernanda Montenegro as Isadora Teixeira Dora
* Vinícius de Oliveira as Josué Fontenele de Paiva
* Marília Pêra as Irene
* Soia Lira as Ana Fontenele
* Othon Bastos as César
* Otávio Augusto as Pedrão
* Stela Freitas as Yolanda
* Matheus Nachtergaele as Isaías Paiva
* Caio Junqueira as Moisés Paiva

=== Release ===
Central Station, was first shown in a regional film festival in Switzerland on January 16, 1998, on January 19 was shown at the Sundance Film Festival in the United States. On February 14, 1998, was shown at the Berlin Film Festival, launched in Brazil occurred only on the 3rd of April of the same year.

===Reception=== grade of De Sica and Jean Renoir|Renoir, displays a pure and unpatronizing feel for the poetry of broken lives. His movie is really about that most everyday of miracles: the rebirth of hope."
 

The film is ranked No. 57 in Empire (magazine)|Empire magazines "The 100 Best Films of World Cinema" in 2010. 

== Awards ==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#B0C4DE" align="center"
! Year
! Awards 
! Category 
! Result
|- 1999 || 71st Academy Awards || Academy Award for Best Foreign Language Film ||  
|-
|| Academy Award for Best Actress for Fernanda Montenegro ||  
|- 56th Golden Best Actress in a Motion Picture Drama for Fernanda Montenegro ||  
|-
|| Golden Globe Award for Best Foreign Language Film ||  
|- Best Foreign Film ||  
|- Best Film Not in the English Language ||  
|-
|| 24th César Awards || César Award for Best Foreign Film ||  
|-
|rowspan="3"|Associação Paulista de Críticos de Arte|São Paulo Association of Art Critics || Best Film ||  
|-
|| Best Director for Walter Salles ||  
|-
|| Best Actress for Fernanda Montenegro ||  
|- 1998 || Golden Satellite Golden Satellite Awards || Best Foreign Language Film ||  
|-
|| Best Actress in a Motion Picture Drama for Fernanda Montenegro ||  
|-
|| Best Original Screenplay ||  
|- 48th Berlin International Film Festival || Golden Bear ||  
|-
|| Silver Bear for Fernanda Montenegro ||  
|- National Board Best Actress for Fernanda Montenegro ||  
|-
|| National Board of Review Award for Best Foreign Language Film ||  
|- Best Actress for Fernanda Montenegro ||  
|- Best Actress for Fernanda Montenegro ||  (2nd place)
|- Havana Film Festival || Best Film ||  
|-
|| Best Actress for Fernanda Montenegro ||  
|-
|| Association of Film Critics Spain || Best Foreign Language Film ||  
|-
|| Association of Film Critics in Poland || Best Foreign Language Film ||  
|-
|| National Association of Italian Critic || Best Foreign Language Film ||  
|-
|| Sundance Film Festival || Best Screenplay ||  
|-
|| San Sebastián International Film Festival || Audience Award ||  
|-
|| Association of Film Critics of Rio de Janeiro || Film of the Year ||  
|-
|}

==References==
 

==External links==
*  
*  
*  
*  
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 