Madana (film)
{{Infobox film
| name = Madana
| image =
| caption = Jai Jagadish
| producer = Vijayalakshmi Singh
| writer = Silambarasan Rajendar Aditya Samiksha Saaniya Shobaraj Ramesh Bhat Bullet Prakash
| music = Yuvan Shankar Raja
| cinematography = R. Giri
| editing = B. S. Kemparaj
| studio = Sri Lakshmi Entertainers
| distributor =
| released =  
| runtime =
| country = India
| language = Kannada
| budget =
| gross =
}} Kannada film Tamil film Aditya in the title role with Samiksha, Saniya, Shobaraj, Ramesh Bhat and Bullet Prakash playing supporting roles. The film was released in November 2006 to poor reviews and failed to attract the audience, ending up as box office bomb.

==Cast== Aditya as Madana / Putta
* Samiksha as Urvashi
* Saaniya as Vaishnavi
* Shobaraj
* Ramesh Bhat
* Bullet Prakash
* Vishwas
* Lakshmi Musuri

==Soundtrack==
{{Infobox album
| Name = Madana
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover =
| Released =  
| Recorded = 2006
| Genre = Film soundtrack
| Length = 28:06
| Label = Ashwini Audio
| Producer = Yuvan Shankar Raja
| Last album  = Paruthiveeran (2006)
| This album  = Madana (2006)
| Next album  = Thaamirabharani (2006)
}} original version. The lyrics were provided by K. Kalyan, V. Nagendra Prasad and V. Manohar.

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes Kay Kay | length1 = 6:46 | lyrics1 = K. Kalyan
| title2 = Minchu Ondu | extra2 = Anoop, Chetan Sosca | length2 = 1:49 | lyrics2 = K. Kalyan
| title3 = Onde Balli | extra3 = Rajesh Krishnan, Chetan Sosca | length3 = 4:39 | lyrics3 = V. Manohar
| title4 = Taathai Taathai | extra4 = Anoop, Chaitra H. G. | length4 = 5:53 | lyrics4 = V. Nagendra Prasad
| title5 = Tentalli Titanic | extra5 = Shankar Mahadevan, Shamita Malnad | length5 = 4:24 | lyrics5 = V. Nagendra Prasad
| title6 = Yaarivanu | extra6 = Shreya Ghoshal | length6 = 4:35 | lyrics6 = K. Kalyan
}}

==References==
 

 
 
 
 
 
 


 