Exhumed (film)
{{Infobox film
| name           = Exhumed
| image          =
| alt            =
| caption        =
| director       = Brian Clement
| producer       = Brian Clement
| writer         = Brian Clement
| starring       = {{plainlist|
* Masahiro Oyake
* Hiroaki Itaya
* Claire Westby
* Moira Thomas
* Chelsey Arentsen
* Chantelle Adamache
* Chuck Depape
* Denys Melanson
}}
| music          = Justin Hagberg
| cinematography = Brian Clement
| editing        = Brian Clement
| studio         = Frontline Films
| distributor    = Frontline Films
| released       =  
| runtime        = 87 minutes
| country        = Canada
| language       = English Japanese
| budget         = $5000 
| gross          =
}} horror anthology film directed and written by Brian Clement.  Set in three different time periods and locales, the film tells the story of an artifact that can return the dead to life.

== Plot ==
In the first story, a samurai and a monk work together to defeat zombies in a Japanese forest.  In the second story, a female private detective discovers that the Japanese artifact is responsible for string of zombie attacks in the 1940s.  The third story is set in a post-apocalyptic wasteland where vampires and werewolves fight each other.  Humans capture them and force them to fight against zombies.

== Cast ==
* Masahiro Oyake as Ryuzo
* Hiroaki Itaya as Zentaro
* Claire Westby as Jane Decarlo
* Moira Thomas as Vivian Von Prowe
* Chelsey Arentsen as Cherry
* Chantelle Adamache as Zura
* Chuck Depape as General Deus
* Denys Melanson as Detective Frank Sterner

== Reception ==
In a mixed review, David Johnson of DVD Verdict called the film ambitious but flawed.   Michael Ferraro of Film Threat rated it 3.5/5 stars and wrote that it is worth viewing despite its flaws.   Peter Dendle wrote in The Zombie Movie Encyclopedia, Volume 2 that the film is creative but becomes increasingly lost amid the diverse characters and discourses. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 


 