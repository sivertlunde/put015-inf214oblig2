Hatley High
{{Infobox Film 
| name = Hatley High
| image = 
| director = Phil Price
| screenplay = Myles Hainsworth
| story = Myles Hainsworth  Phil Price
| starring = Nicolas Wright   Rachelle Lefevre   James A. Woods
| producer = Brandi-Ann Milbradt
| distributor = Ardustry Home Entertainment  Seville Picutres 
| released =  
| runtime = 90 minutes
| country = Canada
| language = English
| music = Oliver Sasse
| cinematography = John Ashmore
| editing = David Eberts  Yvann Thibaudeau    
| gross  =      
| budget = $300,000 USD
}}
Hatley High is a 2003 independent comedy film directed by Phil Price and written by Myles Hainsworth.  It was shown at various film festivals from September 2003 to 2005 including the San Diego Film Festival and the U.S. Comedy Arts Festival.

==Plot==
Tommy Linklater is an eighteen-year-old magician. The magic he performs is often minor - re-directing croquet balls, making cards appear in closed purses - it is always genuine; he actually does the impossible. He will soon learn that his hobby for playing chess while his mother was alive is another gift she left him. His father, Herman Linklater, is a physicist who believes that all the universes mysteries will eventually be explained away. Tommys mother and Hermans wife, Melanie, has died and left them a house they did not know she owned. It is in a small lake-side town called North Hatley. They decided to move there for a year so that Herman can finish his book on cosmology. Once they arrive, the Linklaters discover that Melanie was something of a local legend. She left North Hatley after throwing a game against the towns greatest chess rivals, the Russians. The town was forced to realize their mistakes. They pushed her too hard to win, undermining the soul of the game. Her pictures and trophies are everywhere in the small town. Tommy attends Hatley High, whose main claim to fame is the Knights, an internationally ranked chess team. He befriends a basketball prodigy named Julius, whose secret passion is surfing, as well as Trevor and Darryl two charming and ambitious guys who call themselves the Syndicate. He also meets, and ends up going out with, Hyacinthe Marquez, the beautiful and witty cheerleader who does cartwheels as effortlessly as she quotes Oscar Wilde. Herman, meanwhile, is being pestered by the town priest Lorne Granger. Lorne is a physics buff who actually, and casually, talks to God. Herman is very uncomfortable around priests, but is eventually won over by Lornes enthusiasm for physics. Herman undergoes a sort of epiphany when Lorne provides him with an equation that isnt supposed to exist. The equation enables him to finish his book, but he is forced to confront the possibility that he not living in a clockwork universe after all. Shaun Rodes, egomaniac and captain of the chess team continually challenges Tommy to a chess match, wishing to find out if Tommy inherited his mothers skills. Tommy refuses. However, Hyacinthe sets up an after school game of chess between Shaun, and Tommy. Tommy loses and chides Hyacinthe for selling him out. However, Shaun has a feeling Tommy threw the game. Hyacinthe tries to win Tommy back, and does so by singing to him outside his window. Tommy is moved to forgive her. Shaun, still suspicious from his victory, invites Tommy to an underground high-stakes chess club, rife with shady characters, hot babes, and money. Shaun, hoping to prove his suspicions of Tommys monumental talent, volunteers Tommy to play a blitz game against the top player in the club. Tommy, though reluctant, wins easily. When it is announced that the Russian Junior Chess Team is coming to play Hatley High, the whole town is thrown into a frenzy. An opportunity for redemption. At the pre-game dance, Anya, captain of the Russian chess team, easily seduces Shaun, ties him up, and locks him in the schools attic, solidifying a Russian win. The Russians are winning. During halftime Tommy performs a spectacular half-time magic show. When Shaun does not escape from his bonds in time to play the remaining match, the coach is forced to substitute Tommy in his place.

==Awards==
The film won two awards at the U.S. Comedy Arts Festival; "Best Director" for Phil Price, and "Best Screenplay" for Myles Hainsworth. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 