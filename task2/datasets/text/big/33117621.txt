Inthi Ninna Preethiya
{{Infobox film
| name           =  Inthi Ninna Preethiya
| image          =  
| caption        =  
| director       =  Duniya Soori
| producer       =  
| eproducer      =
| aproducer      =  
| writer         =  
| screenplay     =   Pawan Kumar
| music          = Sadhu Kokila
| cinematography = Satya Hegde
| editing        =  
| distributor    =  
| released       =  April 24, 2008
| runtime        =
| country        = India
| awards         = Kannada
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 Bhavana and Sonu in pivotal roles. The soundtrack was composed by Sadhu Kokila. The film was released on April 24, 2008.

==SoundTrack==

{{Infobox album  
| Name        = Inti Ninna Preethiya
| Type        = Soundtrack
| Artist      = Sadhu Kokila
| Cover       =
| Released    =  2008
| Recorded    = Feature film soundtrack
| Length      =
| Label       =
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|-
| 1 || Inthi Ninna || Sadhu Kokila ||
|-
| 2 || Madhuvana Karedare || Chinmayi || 
|-
| 3 || Mugiyada Kavite Neenu || Hemanth Kumar ||
|-
| 4 || Ondondu  || Rajesh Krishnan, Raksha Aravind ||  
|-
| 5 || Yaro Yaro || Mohammed Aslam ||
|-
| 6 || Yendu Yendu || Rajesh Krishnan ||
|- Nanditha ||
|-
| 8 || Madhuvana Karedare || Vaani Harikrishna ||
|-
|}

==References==
 

 

 
 
 


 