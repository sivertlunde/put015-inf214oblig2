Heart's Desire (film)
{{Infobox film
| name           = Hearts Desire
| image          =
| caption        =
| director       = Paul L. Stein
| producer       = Walter C. Mycroft Jack Davies   Bruno Frank   Clifford Grey   DuGarde Peach   Lioni Pickard
| starring       = Richard Tauber Leonora Corbett Carl Harbord   Paul Graetz
| music          = G.H. Clutsam 
| cinematography = John C. Cox Leslie Norman
| studio         = British International Pictures
| distributor    = Wardour Films 
| released       = August 1935
| runtime        = 82 minutes
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British musical musical drama film directed by Paul L. Stein and starring Richard Tauber, Leonora Corbett, Kathleen Kelly, Diana Napier 
and Frank Vosper.  Its plot involves a young opera singer who is discovered in Vienna and brought to London where he rises to stardom. 
The film was made at Elstree Studios in April/May 1935, and had its charity premiere at the Regal Cinema, Marble Arch, London on 17 October that year. It was part of a cycle of British operetta films.

==Cast==
* Richard Tauber as Karl August Franz Ludwig Josef Steidler 
* Leonora Corbett as Frances Wilson 
* Carl Harbord as Oliver Desmond 
* Paul Graetz as Florian 
* Kathleen Kelly as Anna  George Graves as Granville Wilson 
* C. Denier Warren as Ted Mayer 
* Diana Napier as Diana Sheraton 
* Frank Vosper as Van Straaten 
* Viola Tree as Lady Bennington
* Hilda Campbell-Russell as Steidlers Maid

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 