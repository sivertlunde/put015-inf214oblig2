Aattakatha (2013 film)
{{Infobox film
| name = Aattakatha
| image =
| caption =
| director = Kannan Perumudiyoor
| producer = 
| screenplay = Kannan Perumudiyoor
| based on = Cholliyattam by Kannan Perumudiyoor
| starring =  Vineeth Meera Nandan Eereena Malavika Wales
| cinematography = 
| editing = 
| music = Raveendran Mohan Sithara (BGM)
| studio = Harisri Films International
| distributor = Mahadeva Cinemas
| released =  
| runtime = 
| country = India
| language = Malayalam
}}
Aattakatha: The Final Rehearsal is a 2013 Malayalam musical romance film written and directed by Kannan Perumudiyoor. The film marks his directorial debut and is based on his own novel Cholliyattam. He had earlier produced films such as Ee Puzhayum Kadannu (1996) and Nakshatratharattu (1998). Set in the background of Kathakali, Aattakatha depicts the power of love which crosses boundaries. 

The film has six songs composed by late music maestro Raveendran and penned by late poet Gireesh Puthenchery.  A long delayed project, it released on 24 May 2013.

==Plot==
The main plot of the film involves a French woman, played by German actor Irina Jacobi, who comes to Kerala to study Kathakali, and her relationship with a Kathakali actor called Unni (Vineeth).

==Cast==
* Vineeth as Unni
* Meera Nandan as Sethulakshmi
* Eereena as the French woman
* Malavika Wales as Meleena Raghavan as Unnis Father
* Shivaji Guruvayoor as Sekharan Nair
* Kalamandalam Gopi
* Kalashala Babu as Ravunni Nair
* Madampu Kunjukuttan
* Kalaranjini

==References==
 

 
 
 
 
 
 
 
 

 