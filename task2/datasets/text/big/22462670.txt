Bookwars
 
{{Infobox film
| name           = BookWars
| image          =
| image_size     =
| alt            =
| caption        = 
| director       = Jason Rosette
| producer       = James and John Montoya, Michel Negroponte, Jason Rosette, Lee Clark
| writer         = Jason Rosette
| narrator       = Jason Rosette
| starring       = Peter Whitney, Rick Sherman, Ron Harris
| original music = Little Muddy
| other music    = Willis Jackson, Jack McDuff (courtesy 32 Jazz  )
| audio mix          = John Travis
| cinematography = Jason Rosette
| editing        = Jason Rosette, Greg Janza
| motion graphics        = Eric Schirra
| technical director        = Dennis Muldrow
| studio         = Camerado, S.A.I.D. Communications
| distributor    = Avatar Films, Camerado
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 10,000
| gross          = 250,000
| preceded_by    =
| followed_by    =
}} Rudolph Giulianis controversial "Quality of Life" campaign, which sought to limit and control individuals engaged in informal economic activities on the streets of New York City.

BookWars was released in June 2000, winning the Best Documentary Award at the 2000 New York Underground Film Festival   and premiering theatrically at New Yorks Cinema Village  . Despite its minuscule budget (estimated at $10,000, prerelease) BookWars enjoyed numerous domestic and international TV sales and has to date generated revenue of several hundred thousand dollars.

The movie is currently available on DVD, VOD, VCD (Asia), digital download, theatrical film prints, and other formats.

==Synopsis==
" BookWars is: The gritty, untamed world of street booksellers, exposed in a remarkable feature film that chronicles their lives and loves and their unique perspectives on life. Shot entirely on location by fellow street bookseller and filmmaker Jason Rosette, and produced by Emmy Award-winning filmmaker Michel Negroponte, BookWars explores the other side of the book tables that line the streets of New York City’s Greenwich Village, the Lower East Side, 6th avenue, and elsewhere in New York City." – New Filmmakers, New York

==Plot==
BookWars is a creative documentary which is told in an unconventional, narrative style. The film opens with the narrator (who is also the films director) driving out West along a desert highway, relating to the audience his previous experiences as a streetside bookseller in New York City. The entire documentary – including the central events involving his experiences among the street booksellers in New York – is thus "told" as a long conversation on a cross-country roadtrip out West.

The narrator describes his post-graduation years in New York, and how he ended up at one point virtually penniless. Driven by a desperate need to pay the rent, he resorts to wheeling his own books out to the street to try to sell them. He reveals that he was not only successful in making a significant amount of cash on that first day, but he has also met a variety of interesting and strange characters of the streets of New York – including other street booksellers.

A motley assortment of street booksellers on West 4th street, in Greenwich Village, New York City, are first introduced. Among them: “Slick” Rick Sherman, a semi-professional magician; Al Mappo, so named because he only sells maps and atlases; Emil, who says only he "escaped”, though we do not know from where; and Pete Whitney: King of the booksellers, toad collector, and collage artist.

BookWars next introduces another group of street booksellers who hawk their trade on nearby 6th Avenue. Mainly black and minority individuals, they ply books and magazines in parallel fashion to the nearby West 4th street booksellers, who are primarily white. The booksellers on 6th Avenue suffer greater exposure to the law, with many claiming this to be due to racial profiling.

Some of the significant personalities that are introduced on 6th Avenue include: Marvin, always wearing his trademark black hat; and Ron, from Jamaica – charismatic, streetwise and outspoken.

After the introduction of the primary characters (including the narrator bookseller himself), BookWars discusses, mainly through informal testimony, the various aspects of the street bookseller’s life in chapter-by-chapter fashion.

The tools and tricks of the street booksellers trade are revealed: ways and techniques to maximize income; how to deal with difficult, and sometimes dangerous customers; where and how to get more books; how the booksellers have a right to distribute literature (commercially or otherwise) in public, as per the First Amendment to the United States Constitution; and so on.

BookWars is structured as a “year in the life” style movie, although it was actually produced over several seasons, from 1995 to 1999. When Winter comes, and the streets are too snow-filled and cold to hawk books, the booksellers are shown in their various off-season modes and occupations. “Slick” Rick performs card and magic tricks at parties; Pete Whitney grooms cats for old ladies; and the narrator heads out to New Mexico to work on a Western (genre)|Western*.

(*Which was actually the movie The Desperate Trail  )

Marv and Ron, however, continue to sell books throughout the winter on busy 6th Avenue, and the film follows them as they scour for books and pornographic magazines in the trash in Soho.

Finally, Spring comes, and the booksellers emerge from their off-season to sell books as usual for another season – or so they think. BookWars proceeds to reveal the street-level effects of then-New York City Mayor Rudolph Giuliani’s controversial “Quality of Life” campaign, which sought to remove informal, unregistered entrepreneurs and other individuals from the streets of New York City.

The NYPD   begins to enforce obscure technicalities which govern the uses and dimensions of the sidewalks, thereby making it more difficult to earn a livelihood. A new tax identification number requirement is introduced, creating bureaucratic obstacles, especially for those street booksellers who are marginal or virtually homeless. Nearby New York University unlawfully places imposing, massive planters on the sidewalk in an attempt to drive the street booksellers away; and finally, especially on 6th Avenue where the majority of black street booksellers are active, the NYPD comes to haul away books.

The street booksellers resist and assemble to form an unlikely common front to protest against the actions of the city. Others, who have had their books confiscated, wait for hours at the police station to get them back. Still others, like Ron, rail against the futility of the citys efforts to stop New Yorkers from reading, because of their virtual addiction to books.

In the end, the street booksellers stand their ground against the Mayor, and are able to continue selling with minor adjustments to their way of life.

In the closing moments of BookWars, the narrator admits that after all the recent problems with the city, he has grown restless; he realizes that he wants to do something different, and wants to change his occupation at last. A single massive rainstorm is enough to convince him to give up his street bookselling activities.

He sells the last of his books off to his fellow street booksellers, and heads out West, on a cross-country road trip, with the audience in the passenger seat sharing the ride.

==Cast==
The Street Booksellers of New York City

==Production==
The concept for BookWars was developed after a chance meeting between filmmaker Jason Rosette—who had been selling used and out of print books at a streetside bookstand to generate cash between film production and editing jobs—and Emmy Award-winning New York-based documentary filmmaker Michel Negroponte, at Mr. Rosette’s sidewalk bookstand in 1995. Mr. Negroponte, who had previously directed numerous documentaries including Jupiters Wife  and Space Coast , agreed to be a co-producer on the movie.

Mr. Rosette commenced to bring a video camera to the bookstand daily to document the sights and sounds of the city of New York as seen from the perspective of a sidewalk bookseller. Because this initial phase of shooting was funded completely out of pocket, BookWars was produced in a variety of film and video formats depending on whatever camera was available at the time. Formats included: Mini DV, Super 8 film, Regular 8 and Hi-8 video, and Super VHS. The aesthetic goals of immediacy and a natural, unobtrusive presence of the camera also demanded the use of small format film and video.

Initial shooting was financed by the sale of the various used and out-of-print books at the filmmakers bookstand. As more books were sold, more film and video stock could be bought, and production would continue.

Following the initial stage of shooting in New York, the director drove from New York to New Mexico where he had edited an earlier movie at a colleagues Albuquerque-based production studio. In New Mexico in 1996, the first assembly of the movie, called The Book Wars, was edited on a tape-to-tape analogue system; that rough assembly was screened for the first time in on Super Bowl Sunday as part of an exhibition produced by Basement Films   of Albuquerque.
 Avid or Media 100 system.

While the filmmaker sought post-production equipment in Los Angeles, a San Francisco- based production company, S.A.I.D. Communications, viewed the assembly edit of BookWars and agreed to provide additional editing and production in order to complete the picture. Mr. Rosette subsequently departed Los Angeles for San Francisco, and commenced to work on editing the documentary there full-time.

There was no up-front money to continue editing and still no funding, so the director worked a variety of jobs to sustain himself and production of the movie, working as a freelance film editor, voice-over artist, and an assortment of temporary and odd jobs.

A rough cut of BookWars was finally achieved in 1998. However, additional material was required to create a coherent feature edit. S.A.I.D Communications provided a camera on loan, and the director headed back to New York to spend time once again with his fellow booksellers to continue documenting their lives.

After this second stage of shooting in New York was complete, in 1998, the director returned to San Francisco to include the additional essential sequences in the final edit. While the filmmaker was away, however, S.A.I.D. had already commenced postproduction on another documentary called Live Nude Girls Unite, and the BookWars team had to defer to their the stripper-activist production colleagues.

The filmmaker had by now spent nearly three years in San Francisco, endeavoring to complete BookWars without having received any grants or external funding. A grant administrator from The Pacific Pioneer Fund explained that, while he considered BookWars to be a worthy and compelling project, he felt that the filmmaker “would have to be a genius to pull it off” (funding was denied).
 Beat writer and publisher Lawrence Ferlinghetti of City Lights Bookstore viewed the cut of BookWars, and applauded it as being “Anarchistic”.

Delays continued and post production paused while an online editing system in the San Francisco area was sought, unsuccessfully. Finally, co-producer Michel Negroponte contacted the filmmaker regarding an editing station that had become available in New York City; payment to the machine’s owners at Copacetic Pictures could be deferred until the movie was completed and sold.

The filmmaker thus drove back to New York to complete the final edit of the documentary.  During this time, German-French broadcasting group Arte/ZDF received the nearly-completed cut of the BookWars and committed to purchasing the hour-long broadcast version for airing, while providing an advance to  enable completion of the final edit, mix, narration, and postproduction of the program. Shortly after announcing this key initial TV sale, The Playboy Foundation and the Experimental TV Center subsequently awarded modest postproduction grants.

The work in progress of BookWars screened at the Independent Feature Project’s Independent Feature Film Market, while the nearly complete fine cut of BookWars screened at the New Filmmakers, New York series at Anthology Film Archives in the Spring of 2000.

Several months later, the final theatrical version of Bookwars was completed. The movie had its world premiere at the 2000 New York Underground Film Festival, where it won the Best Documentary Award. Though the filmmaker was subsequently banned from the festival venue (Anthology Film Archives) due to over-zealous guerilla promotional activities, the BookWars nonetheless soon enjoyed a New York theatrical premiere at the Cinema Village  , and went on to screen theatrically and air at numerous international and domestic channels and venues.

In total, production of BookWars had taken five years and 20,000 miles of driving across the USA to complete.

==Influences and approach==
There are no formal interviews in BookWars although the booksellers at times speak informally to the filmmaker. There are no subtitles or title cards within the film itself either, except for the opening titles and credits. Rather, the narrators voiceover is designed to take over this role while conveying a more intimate, personal quality to the story.
 Western genre elements were included as well – particularly in the conceptual story component of a “man heading out West to start anew”.

BookWars veers stylistically from straight or journalistic documentaries through its use of creative devices. Slow motion is often employed, the narration is non-standard, the movie follows a distinct narrative structure, and dream sequences are utilized. Often, the subjects of the documentary take over as camera operators and record themselves and their surroundings at will.
 Willis Jackson and Jack McDuff were licensed from the 32 Jazz   label.

==Festivals and broadcasts==
BookWars has screened at festivals, theatrical venues, and broadcast outlets including:   (New York)  , the Danish National Film Institute  /, Brotfabrik (Berlin)  , the 911 Media Arts Center   in Seattle, amongst others.

The feature version also pioneered internet releases when they were still uncommon (in 2001), streaming on a pay per view basis on the Cinemapop media portal.

==Awards==
BookWars received the Best Documentary Award upon its world premiere at the 2000 New York Underground Film Festival. The movie was also nominated for an IFP Gotham Award.

==Reception==
BookWars garnered generally positive reviews, starting with its first review by   review   by critic Elvis Mitchell soon followed which was not supportive of the film. Numerous other positive reviews followed, however, which cast doubt on some motivations behind the early critiques of BookWars.

Ultimately, most of the street booksellers appearing in the movie – as well as those not appearing - welcomed it as an agreeable depiction of their lives, and several street booksellers to this day continue to keep copies of the film on their sidewalk bookstands as a grass-roots, participatory distribution mechanism.

==Reviews==
 
 
 
 
 
 
 
 
 
 
 
 
 

 

==External links==
 
*BookWars  
*Production Company  
*   (film)
*BookWars DVD with limited public performance rights   VOD at iTunes  
*Interview with BookWars director Jason Rosette  
*Interview with BookWars director Jason Rosette  
*Coverage of BookWars at the 2000 New York Underground Film Festival  

 
 
 
 
 
 