The Wandering Jew (1923 film)
The British silent silent fantasy film directed by Maurice Elvey and starring Matheson Lang, Hutin Britton and Malvina Longfellow.  It was based on a play by E. Temple Thurston. A Jewish man is condemned to wander aimlessly through the ages. It was remade in 1933 as The Wandering Jew.

==Cast==
* Matheson Lang - Mattathias 
* Hutin Britton - Judith 
* Malvina Longfellow - Granella 
* Isobel Elsom - Olalla Quintane 
* Florence Saunders - Joanne 
* Shayle Gardner - Pietro Morelli 
* Hubert Carter - The Ruler 
* Jerrold Robertshaw - Juan de Texada 
* Winifred Izard - Rachel 
* Fred Raynham - Inquisitor  Lewis Gilbert - Mario 
* Hector Abbas - Zapportas 
* Lionel dAragon - Raymond 
* Gordon Hopkirk - Lover

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 