To All a Goodnight
 
 
{{Infobox film
| name           = To All a Good Night
| image          = To All a Good Night.jpg
| alt            =
| caption        = Theatrical release poster
| director       = David Hess
| producer       = Sandy Cobe Jay Rasumny Alex Rebar Sharyon Reis Cobe Rick Whitfield
| writer         = Alex Rebar    
| starring       = Jennifer Runyon Forrest Swanson Linda Gentile
| music          = Richard Tufo
| cinematography = Bil Godsey
| editing        = William J. Waters
| studio         = Four Features&nbsp;Partners Intercontinental Releasing Corporation (IRC)
| distributor    = Intercontinental Releasing Corporation (IRC)
| released       = January 30, 1980  (United States) 
| runtime        = 90 minutes
| country        = United States
| language       = English
}}
 slasher film, directed by David Hess. It was written by Alex Rebar, produced by Sandy Cobe, Jay Rasumny, Alex Rebar, Sharyon Reis Cobe and Rick Whitfield and stars Jennifer Runyon and Forrest Swanson. It was released on January 30, 1980. It revolves around a group of sorority girls and their boyfriends being killed off during a Christmas party by a vengeful psychopath dressed as Santa Claus.
==Plot==
Two years after a child is killed during a prank, a group of sorority girls at the Calvin Finishing School secretly smuggle in their boyfriends for a late night Christmas party, and are murdered by a mysterious killer in a Santa Claus outfit.

== Cast ==
* Jennifer Runyon as Nancy
* Forrest Swanson as Alex
* Linda Gentile as Melody
* William Lauer as T. J.
* Judith Bridges as Leia
* Katherine Herrington as Mrs. Jensen
* Buck West as Ralph
* Sam Shamshak as Polansky
* Angela Bath as Trisha
* Denise Stearns as Sam
* Solomon Trager as Tom
* Jeff Butts as Blake
* Harry Reems (credited as Dan Stryker) as Pilot

==Release==
 

The film was released to home video in the United States by Media Home Entertainment in 1983. Scorpion Releasing, under license from current rights holder MGM, has released the film for the first time on DVD and Blu-ray on October 21, 2014. Special features include interviews and the original theatrical trailer.

== Critical reception ==
  AllMovie called it an "undistinguished, clichéd slasher film". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 