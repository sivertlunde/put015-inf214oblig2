The Juniper Tree (film)
{{Infobox film
| name           = The Juniper Tree
| image          = The Juniper Tree.jpg
| writer         = Nietzchka Keene
| starring       = Björk|Björk Guðmundsdóttir Bryndis Petra Bragadóttir Guðrún Gísladóttir Valdimar Örn Flygenring Geirlaug Sunna Þormar
| director       = Nietzchka Keene
| producer       = Nietzchka Keene
| distributor    = Rhino Home Video
| released       = US: 10 April 1990 Iceland: 12 February 1993
| runtime        = 78 minutes approx.
| language       = English
| music          = Larry Lipkis
| cinematography = Randolph Sellars
| editing        = Nietzchka Keene
| country        = Iceland United States
|}}
 The Juniper Tree" collected by the Brothers Grimm.

The film was shot in Iceland with an extraordinarily small budget in the summer of 1986, but because of financial problems later on in the editing room it was not released until 1990, when it screened for the "Grand Jury Prize" at the Sundance Film Festival. Rhino Home Video released the film on VHS in 1995 and on DVD in 2002.

==Plot==
  stoned and burned for witchcraft. They go where no one knows them, and find Jóhann (Valdimar Örn Flygenring), a young widower who has a son called Jónas (Geirlaug Sunna Þormar) . Katla uses magical powers to seduce Jóhann and they start living together. Margit and Jónas become friends. However, Jónas does not accept Katla as his stepmother and tries to convince his father to leave her. Katlas magic power is too strong and even though he knows he should leave her, he cant. Margits mother appears to her in visions and Jónas mother appears as a raven and to bring him a magical feather.

==Comments review==
Labeled as a fairy tale, science-fiction, fantasy, drama or art house film, The Juniper Tree was shot in black and white to highlight its dramatic content and as a resource to place the story in the Middle Ages. This film has become famous among Björks admirers after she gained international renown by the early nineties . Despite some critics remarks about the protagonists performances , some reviewers consider it is a good film in terms of the screenplay and cinematography. 

==Credits==
*Director: Nietzchka Keene
*Producer: Nietzchka Keene
*Assistant producer: Allison Powell
*Cinematography: Randolph Sellars
*Film editor: Nietzchka Keene
*Art director: Dominique Polain
*Assistant art director: Ólafur Engilbertsson
*Costume design: Nanna Luisa Zophaniasdóttir
*Composer: Larry Lipkis
*Sound recording: Patrick Moyroud
*Sound re-recording mix: Jeffrey Perkins
*Sound assistant: Helgi Sverrisson
*Special effects: Dominique Polain
*Special effects (makeup): Élin Sverrisdóttir
*Stunt: rider: Ásta Pálmadóttir
*Gaffer: Robert Field
*Bird trainer: Þorleifur Geirsson
*Still photography: Róbert Guillemette
*Assistant camera: Halldór Gunnarsson
*Production assistant: Nicole Hanset
*Pre-production office: Alix Jackson and Julia Parker
*Folk music collector: Helga Jóhannsdóttir
*Herbal consultant: Helga Mogensen
*Craft consultant: Elizabeth Rowe
*Catering: Solveig Gissel Stáhl
*Nursery consultant: Ásgeir Svanbergsson

==DVDs technical data==

===US DVD===
*Date of release: 23 April 2002
*Distributor: Rhino Home Video
*Region: 1 NTSC
*Subtitles: N/A
*Audio: English
*Sound: Stereo Sound
*Aspect ratio: 1.33:1 of the original film presentation
*Chapter Stops: 12
*DVD packaging: Amaray Keep Case
*Extras: a 15-minute director Interview, about 3 minutes of deleted scenes accompanied by director’s comments, and about 3 minutes of slide show of promotional photographs.

===Japanese DVD===
*Date of release: 10 February 2001
*Distributor: unknown
*Price: Japanese yen|¥2,940
*Region: 2 NTSC
*Subtitles: Japanese
*Audio: English, Japanese (dubbed)
*Sound: Stereo Sound
*Aspect ratio: 1.33:1 of the original film presentation
*Chapter Stops: 9
*DVD packaging: Amaray Keep Case
*Extras: Original trailer (about 2 minutes) and Japanese trailer (about 1 minute).

==External links==
 
* 
*  
* 
* 
* 


 
 

 
 
 
 
 
 