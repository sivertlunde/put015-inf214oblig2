Conflict (1945 film)
{{Infobox film
| name           = Conflict
| image          = Conflict 1945 movie poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Curtis Bernhardt
| producer       = William Jacobs
| screenplay     = Arthur T. Horman Dwight Taylor
| based on       =   
| narrator       = 
| starring       = Humphrey Bogart Alexis Smith Sydney Greenstreet
| music          = Frederick Hollander
| cinematography = Merritt B. Gerstad
| editing        = David Weisbart
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Dwight Taylor, Alfred Neumann and Robert Siodmak. It starred Humphrey Bogart, Alexis Smith and Sydney Greenstreet. The film is the only one in which Bogart and Greenstreet co-starred where Bogart, not Greenstreet, is the villain or corrupt character. 

==Plot==
On the surface, Richard (Humphrey Bogart) and Kathryn Mason (Rose Hobart) appear to be a happily married couple. But on their fifth wedding anniversary, Kathryn accuses Richard of having fallen in love with her younger sister, Evelyn Turner (Alexis Smith), who is visiting them. He does not deny it, but has resigned himself to leaving things as they are, since he is certain Kathryn would not give him a divorce. At a party celebrating the couples anniversary hosted by family friend and psychologist Dr. Mark Hamilton (Sydney Greenstreet), Evelyn meets with Marks handsome young colleague, Professor Norman Holdsworth (Charles Drake). On the way home, Kathryn suggests to Evelyn that their mother is lonely, so Evelyn decides to move home. Distracted by this unwelcome news, Richard crashes their car and suffers a broken leg. He then decides to take desperate action.
 Grant Mitchell), diagnoses the problem as psychological, not physical. He suggests exercise, so a car trip to a mountain resort is arranged. At the last minute, Richard has to stay home to do some work; he has Kathryn go on ahead by herself. She is blocked on a narrow deserted mountain road by a parked car. Richard walks unexpectedly out of the fog and kills her. Afterward, he pushes her car down a steep slope; it dislodges some logs which crash down and hide the automobile. He returns home in time to set up an alibi by meeting with employees he had summoned. He then notifies the police that she is missing.

However, things happen to make Richard wonder if Kathryn somehow survived. First, a hobo is picked up by the police; when Richard and Evelyn identify a cameo ring found in his possession as belonging to the missing woman, the tramp admits to having stolen it from a woman matching Kathryns description after her disappearance. Then Richard smells Kathryns perfume in their bedroom. He later finds her key to a home safe; when he opens the safe, her wedding ring is inside.

Mark suggests Richard and Evelyn join him on a fishing vacation to relieve the strain. Mark also invites Holdsworth, who takes the opportunity to ask Evelyn to marry him. She is undecided. When she tells Richard, he believes her hesitation is because of him. He tells her he loves her, and that she must feel the same about him, but she strongly denies it. Later realizing his mistake, he encourages Holdsworth to try again.

Then a pawn shop claim ticket is mailed to Richard, addressed in what appears to be his wifes handwriting. When he goes to the pawn shop, he finds Kathryns locket. Finally, a woman looking and dressed like his wife passes by on the street while he is in a shop. He follows her to an apartment, but when he has the owner show him the place a few minutes later, there is no one there.  

Unable to reconcile these occurrences any longer, Richard returns to site of the crash to see once and for all if Kathryns body is inside the car.  On his arrival, Hamilton and the police are waiting for him and tell him they had long since found the crash and removed Kathryns body.  As Richard is arrested, Hamilton reveals that he had been onto Richard since Richards initial interview with the police, as Richard had mentioned in it that Kathryn was wearing a rose when last he saw her.  Hamilton had given her the rose after she left Richard, so if Richards alibi were true, he would not have known about it.  Knowing that detail would be insufficient to secure a conviction, however, Hamilton and the police then worked together to stage the events that made Richard suspect Kathryn was still alive, specifically to get him to return to the scene of the crime to look for her body, thus proving that hed known all along what happened to her.

==Cast==
* Humphrey Bogart as Richard Mason
* Alexis Smith as Evelyn Turner 
* Sydney Greenstreet as Dr. Mark Hamilton 
* Rose Hobart as Kathryn Mason 
* Charles Drake as Prof. Norman Holsworth Grant Mitchell as Dr. Grant 
* Patrick OMoore as Det. Lt. Egan 
* Ann Shoemaker as Nora Grant
* Edwin Stanley as Phillips

==Reception==

===Critical response===
Film historians Alain Silver and Elizabeth Ward comment, "The film is particularly memorable for the use of the song Tango of Love as leitmotif to indicate the putative reappearance of Katherine, with the background strings translating the scent of perfume; the opening trucking shot through the rain-soaked night up to the window of the Mason house, which allows the audience to eavesdrop on the dinner party; and the sinister appearance of Bogart as he steps out of the shadows to murder his wife." 

Film critic Dennis Schwartz gave the film a mixed review , writing, "Humphrey Bogart plays a wife murderer in this flawed film noir. Director Curtis Bernhardt leaves the plot with too many artificial devices to be effective ... The only thing that cant be faulted was the earnest performances of Bogie as the tortured killer and the supporting cast of Warner Brothers regulars." 

==References==
 

==External links==
*  
*  
*  
*   informational site at DVD Beaver (includes images)
*  

 

 
 
 
 
 
 
 
 
 
 
 