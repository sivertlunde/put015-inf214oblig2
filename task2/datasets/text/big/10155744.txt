Salween (film)
{{Infobox film
| name           = Salween
| image          = Salween movie poster.jpg
| caption        = The Thai DVD cover.
| director       = Chatrichalerm Yukol
| producer       = Kamla Sethi
| writer         = Stirling Silliphant Chatrichalerm Yukol
| narrator       = 
| starring       = Sorapong Chatree Siricoup Metanee Chatchai Plengpanich
| music          = Pisate Sungsuwan
| cinematography = Anupard Buerchan
| editing        = Chatrichalerm Yukol
| distributor    = Mangpong
| released       =   
| runtime        = 128 min.
| country        = Thailand Thai
| budget         = 
}}
Salween ( ,  ) is a 1994 Thai action film|action-drama film directed by Chatrichalerm Yukol and starring Sorapong Chatree and Chatchai Plengpanich. Sombat Metanees son, Siricoup Metanee, also stars as a young police lieutenant, newly arrived at a rough-and-tumble post in a Thailand|Thai-Myanmar border town on the Salween River. The screenplay is co-written by Chatrichalerm and Stirling Silliphant.

==Plot==
  SLORC and the Karen National Liberation Army have been engaged in a war that is little known to outside people.

A Thai timber baron, Tweepong, exploits the situation by courting both the KNLA and SLORC in order to have the unrest continue while his company logs the teak forests in the border region of the Salween River.
 wild west" atmosphere comes a young, idealistic police lieutenant, Danai, whos to take charge of the police station at the border town of Fah Soong Pha Sak. He rides into town on his motorcycle as a gun battle has erupted between the police, led by tough Sergeant Ram, against some gun thugs. Danai, wearing his crisp police uniform, ends up dumped in a pig pen, covered in mud. He then gives the unkempt, undisciplined local policemen a dressing down, saying they all must clean up and wear their uniforms.

The gunmen who started the fight had been sent to kill Ram by Somsak Tweepong, son of the local "godfather". Somsak later visits Danai to invite him to his fathers birthday party.
 butterflies with a net. The man is the elder Tweepong, who gives Danai some salve.
 SLORC colonel and gains intelligence about the Myanmar governments fight against the Karen.

Danai attends the birthday party for Tweepong, and meets Somsaks wife, who was a former beauty queen and actress from Bangkok. She is unhappily married to Somsak and has become an alcoholic.

Danai continues his tour of the district, meeting Nid, who is a teacher at a school for Thai hill-tribe children. She is also the daughter of Sergeant Ram. Her mother was a Karen woman, who was raped and killed by Somsak Tweepongs men, hence the deep resentment Ram has against Tweepong.

Further education comes for Danai when fighting between the KNLA and the SLORC spills over into Thailand. As Karen troops, led by KNLA Lieutenant Tulay, are being attacked by a SLORC helicopter, they attempt to wade the Salween River and cross over into Thailand. Danai at first orders Ram to turn the Karen back, but after seeing the helicopter gunship decimate the Karen, he relents, even growing so frustrated as to fire upon the helicopter, which then fires back. However, nothing more comes of this incident.

But there is more drama in store, as Somsak discovers his wife having an affair. She accuses Somsak of being a homosexual, and Somsak grows enraged. His wife pulls out a gun, and Somsak tries to take it from her, shooting her in the process. She escapes and drives to the police station, where she dies after saying Somsak had shot her.
 extradite Somsak, but Ram convinces him otherwise, and they take a party into Myanmar to get Somsak back by force.
 Stinger missiles in exchange for saving his son. Tulay and his men sneak up on Danais party at night, but let them live in exchange for saving their lives during the earlier incident at the river. Danai tells Tulay that Somsak had committed murder and must be brought back to Thailand for trial. "This is Karen people#Kawthoolei|Kawthoolei. Thai laws have no meaning here," Tulay replies. The next morning, the Karen attack. Ram is fatally shot, but he is able to get off a shot that injures Tulay. Danai and the survivors, including Rams daughter, take Somsak and head back to Thailand.

Just as Danai and Nid are crossing a small river bridge back to Thailand, they come under attack again by Tulay, who, though injured, was waiting under the water just upstream to ambush them. However, then SLORC troops show up and shoot Tulay. The SLORC colonel says he has made a deal with Tweepong for Somsak, and orders Danai to drop his weapons. Tulay, though, is not yet dead. He floats downstream under the bridge and fatally shoots the colonel. Somsak then runs back towards Myanmar, but Nid still has her pistol. She hands it to Danai, who then shoots Somsak in the back to death.

==Cast==
* Sorapong Chatree as Sergeant Ram
* Chatchai Plengpanich as Tulay
* Siricoup Metanee as Police Lieutenant Danai
* Nappon Gomarachun as Somsak Teeweepong
* Chalita Pattamapan as Nid SLORC colonel

==Soundtrack== Pleng phua cheewit ("songs for life") band, Carabao (band)|Carabao, rhapsodizing the river and the people that live along it.

==Gunman series==
Salween was also entitled Gunman 2 ( , or Mue puen 2) as a followup to Chatrichalerms 1983 film, Gunman (film)|Gunman. However, aside from the presence of Sorapong Chatree, who starred in the earlier film, as well as Ron Rittichai, the stories and characters of the two films have no relation, so Salween is a sequel to Gunman in name only.

==DVD==
A DVD of the film was released in Thailand on an all-region PAL disc with English subtitles by the Mangpong video retail chain.

==External links==
*  

 
 
 
 
 
 