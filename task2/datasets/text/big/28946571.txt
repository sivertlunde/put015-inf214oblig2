Glamour (1934 film)
{{Infobox film
| name           = Glamour
| image          = Glamour-1934.jpg
| image_size     =
| caption        = Film poster
| director       = William Wyler
| producer       = B.F. Zeidman
| writer         =
| narrator       =
| starring       = Paul Lukas Constance Cummings Phillip Reed
| music          = Howard Jackson
| cinematography = George Robinson
| editing        = Ted J. Kent
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Glamour is a 1934 American drama film directed by William Wyler and starring Paul Lukas, Constance Cummings and Phillip Reed. 

==Plot==
An ambitious chorus girl marries an up-and-coming composer.

==Cast==
* Paul Lukas ...  Victor Banki
* Constance Cummings ...  Linda Fayne
* Phillip Reed ...  Lorenzo Valenti
* Joseph Cawthorn ...  Ibsen
* Doris Lloyd ...  Nana
* Lyman Williams ...  Forsyth
* Phil Tead ...  Jimmy
* Luis Alberni ...  Monsieur Paul
* Yola dAvril ...  Renee
* Alice Lake ...  Secretary
* Louise Beavers ...  Millie

==References==
 

==External links==
* 

 

 
 
 
 
 


 