Sadako 3D 2
{{Infobox film
| name           = Sadako 3D 2
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Tsutomu Hanabusa
| producer       = 
| writer         = 
| screenplay     = Daisuke Hosaka Noriaki Sugihara
| story          = 
| based on       =  
| narrator       = 
| starring       = Miori Takimoto Kōji Seto Kokoro Hirasawa Satomi Ishihara
| music          = Kenji Kawai
| cinematography = Nobushige Fujimoto
| editing        = 
| studio         = Kadokawa Shoten Tohokushinsha Film
| distributor    = Kadokawa Shoten
| released       =   
| runtime        = 96 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = US$7,067,401
}}

  is a 2013 Japanese horror film directed by Tsutomu Hanabusa and the second installment of the Sadako 3D series.  

==Plot== the previous film, Takanori and Akanes daughter, Nagi (Kokoro Hirasawa) is cared by Takanoris younger sister, Fuko (Miori Takimoto); Takanori himself is now working at Asakawa General Hospital since Akanes death by childbirth, distancing himself from Fuko and Nagi as he blames Nagi for Akanes death. Fuko is certain that something is wrong with Nagi as she distances herself from her friends, likes to draw strange imagery, and is always present near people who committed suicide. She consults with her psychiatrist, Dr. Fumika Kamimura (Itsumi Osawa), but she only states that Fukos anxiety comes because she has not moved from her mothers suicide years ago, whose death she could not prevent. While playing at a park, one of Nagis friends, Yuna, teases Akanes death. Moments later, she is found dead in the nearby river. Fuko becomes even more anxious as more deaths start to occur, from Nagis babysitter to a train full of people, which Nagi had foreshadowed in her drawings she drew during her psychological test with Dr. Kamimura. She also says that Dr. Kamimura, as well as everyone else, will die.

Detective Mitsugi Kakiuchi (Takeshi Onishi) is assigned to investigate the case. He questions Detective Yugo Koiso (Ryosei Tayama), who is disabled following his own investigations of a similar case five years before. Koiso tells Kakiuchi that the suicides are linked to Sadako Yamamura, who almost possessed Akane as her vessel so she might be reborn in the world. Seconds later, a force pushes Koisos wheelchair through a set of stairs to his death. Kakiuchi finds the security camera recording before the train accident and spots Nagi looking at the camera. He tries to question Takanori about Nagi and Akane, but he refuses to disclose anything other than Akanes death. As he hurries up ahead, he accidentally drops his trash bags, revealing bundles of black hair. While Fuko is cleaning Takanoris room, she finds a locked up wardrobe that contains a photo of Takanori, Akane, and baby Nagi, as well as several letters for Takanori from Seiji Kashiwada asking Nagis well-being. Fuko visits Kashiwada (Yusuke Yamamoto), the latter awaiting his execution for his murder of young women five years before. Describing himself as Nagis "fan", Kashiwada reveals that Nagi is not Akanes child, but Sadakos, and that the only way to stop Nagi is for Fuko to kill herself or to kill Nagi. On the way home, Fuko receives a call from Dr. Kamimura asking her to meet her, but finds that she has been possessed, and while escaping from her attack, Fuko relives her experiences of seeing her mothers suicide and becomes cursed.

At home, Fuko experiences nightmares of Nagi and Takanori attacking her. She contemplates on killing Nagi by throwing her to the sea, but decides not to. She meets Takanori and after urging him to help Nagi, he shows Fuko that Akane (Satomi Ishihara) is still alive, albeit in a comatose state. He tells Fuko that since Sadako attempted to be reborn through Akane in the first film, Akane has allowed herself to be possessed by Sadako so she can fight her from the inside. Eventually, Akane became pregnant and delivered Nagi, but she can never reunite with her mother again lest Sadako will be reborn. Takanori also reveals that Nagi is not responsible for the deaths. The next day, Fuko discovers Nagi is missing, while Takanori finds her roaming in the Asakawa Hospital. He is attacked and brutally beaten by Kakiuchi, who wants to kill Nagi and Akane to stop the curse, revealing that his wife (the woman killed in the beginning) had died from the curse and he himself is also cursed. Nagi escapes and reunites with Fuko, who allows Nagi to reunite with her mother. However, Akane is shot before she could reach Nagi by Kakiuchi, who promptly commits suicide. As the whole room is flooded by blood pouring from Sadakos well, Nagi is taken by Sadako, but Fuko manages to save her.

Several days later, Fuko and Nagi have a picnic at a meadow, while the woman in white, still sitting in the garden, reads Kashiwadas last letter before his execution. The film ends with the camera recording that Kakiuchi watches, which reveals that Sadakos child is not Nagi.

==Cast==
*Miori Takimoto as Fuko Ando, a young woman who takes care of her niece, Nagi, following her brothers abandonment.
*Kōji Seto as Takanori Ando, Fukos older brother who blames his daughter, Nagi, for his lovers death.
*Itsumi Osawa as Fumika Kamimura, Fukos psychiatrist.
*Kokoro Hirasawa as Nagi Ando, Takanori and Akanes quiet daughter who is suspected of hosting Sadakos powers.
*Takeshi Onishi as Mitsugi Kakiuchi, a detective whose wifes death results in him taking Sadakos case.
*Yusuke Yamamoto as Seiji Kashiwada, an online artist working for Sadako. He is currently awaiting execution. the previous film.
*Satomi Ishihara as Akane Ayukawa, Takanoris lover and Nagis mother, who has been hosting Sadako since the end of the previous film.

==Reception==
The film grossed US$7,067,401 by September 29. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 