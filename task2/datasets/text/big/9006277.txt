Sunshine State (film)
{{Infobox film
| name           = Sunshine State
| image          = Sunshine State movie poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = John Sayles
| producer       = Maggie Renzi
| screenplay     = John Sayles
| narrator       = 
| starring       = Angela Bassett Edie Falco
| music          = Mason Daring
| cinematography = Patrick Cady
| editing        = John Sayles
| studio         = Anarchists Convention Films
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 141 minutes
| country        = United States
| language       = English
| budget         =
| gross          =  $3,369,654 
}} Alan King, Timothy Hutton, Mary Steenburgen, Bill Cobbs, and others.   The movie was filmed on Amelia Island, Florida, which includes settings in historic Fernandina Beach. Amelia Island is located 30 or so miles north of Jacksonville.

Set in a small town in northern Florida, the main two interwining stories focus on two women at crucial points in their lives, and also comments on such issues as race relations and commercial property development.

==Plot==
As the primarily white town of Delrona Beach, Florida is preparing for its annual festival, one of the parade floats is set afire. A young black boy named Terrell is found guilty of the deed, and he is sentenced to community service with a local community theater.  An orphan, Terrell is in the care of an elderly relative, Eunice Stokes, who lives in the neighboring, primarily black community of Lincoln Beach.

Eunice is being visited by her actress daughter, Desiree, a former beauty queen who left town while she was still in high school. At the time, she caused a scandal because she was pregnant by her boyfriend and her parents sent her to live with an aunt in Georgia until the baby came. She has returned to make amends to her mother and also to introduce her new husband, Reggie. While in town, she becomes re-acquainted with her old high-school paramour, Flash, who was a star football player and is now a promoter for a property development scheme.

Back in Delrona Beach, Marly Temple runs a motel and cafe owned by her elderly father and drama-instructor mother. Marly feels shackled by the arrangement and is tempted to sell the hotel to developers, but she assumes  her father will never agree. Marly wanted to be a marine biologist and was once an underwater performer, but when her twin older brothers died in an accident she reluctantly became her fathers heir. Marly must also deal with her former husband, Steve, a slacker who is always looking to make some quick money. Marly additionally has a boyfriend, Scotty, who is struggling to become a golf pro and travel the tour circuit. Marly then becomes romantically involved with Jack, a landscape architect affiliated with the property developers.

Offering commentary on the story are a group of golfers, who act as an updated iteration of the Greek chorus.

==Cast==
 
 
* Alex Lewis as Terrell Wilkins Alan King as Murray Silver
* Cullen Douglas as Jefferson Cash
* Clifton James as Buster Bidwell
* Eliot Asinof as Silent Sam
* James McDaniel as Reggie Perry
* Angela Bassett as Desiree Stokes Perry
* Edie Falco as Marly Temple
* Timothy Hutton as Jack Meadows
* Perry Lang as Greg
* Miguel Ferrer as Lester
* Gordon Clapp as Earl Pinkney
* Mary Steenburgen as Francine Pinkney
 
* Bill Cobbs as Dr. Elton Lloyd
* Mary Alice as Mrs. Eunice Stokes
* Michael Greyeyes as Billy Trucks
* Sam McMurray as Todd Northrup
* Tim Powell as Buyer
* Brett Rice as Buyer #2
* Marc Blucas as Scotty Duval
* Charlayne Woodard as Loretta Tom Wright as Lee "Flash" Phillips
* Ralph Waite as Furman Temple
* Richard Edson as Steve Tregaskis
* Jane Alexander as Delia Temple
* Ashley Brumby as April
 

==Awards==

===Wins===
* Los Angeles Film Critics Association Awards: LAFCA Award - Best Supporting Actress, Edie Falco; 2002. National Board of Review: Special Recognition - For excellence in filmmaking, 2002.
* Black Reel Awards: Black Reel - Theatrical - Best Actress, Angela Bassett; 2003. Florida Film Critics Circle Awards: Golden Orange Award - John Sayles; For his witty satire that insightfully examines Floridas historic past, expanding present and uncertain future; 2003.
* NAACP Image Award: Image Award - Outstanding Actress in a Motion Picture, Angela Bassett; 2003.
* Satellite Award: Golden Satellite Award - Best Performance by an Actress in a Supporting Role, Drama, Edie Falco; 2003.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 