Deedar (1992 film)
{{Infobox film
| name           = Deedar 
| image          = Deedar_(1992_film).jpg
| caption        =
| director       = Pramod Chakravorti|& vikram chakravarty
| producer       = Pramod Chakravorti  Lakshmi Chakravorti (associate producer)
| writer         = Sachin Bhowmick  Mir Muneer (dialogue)
| starring       = Akshay Kumar Karishma Kapoor Anupam Kher Tanuja
| music          = Anand-Milind
| cinematography = V.K. Murthy
| editing        = Praful Vyas
| distributor    = 
| released       = 14 August 1992
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = }}

Deedar is 1992 Hindi movie directed by Pramod Chakravarti and starring debutant Akshay Kumar,  Karishma Kapoor, Anupam Kher, Tanuja. 
Other cast members include Laxmikant Berde, Seema Deo, Ajit Vachhani, Anjana Mumtaz, Rajeev Varma, Dan Dhanoa, Priya Arun, Rajesh Puri and Viju Khote.

Cast

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ab To Kahin Tere Bin"
| Udit Narayan, Sadhana Sargam
|-
| 2
| "Deedar Ho Gaya Mujhko Pyar Ho Gaya"
| Udit Narayan
|- 
| 3
| "Din Ba Din Mohabbat"
| Udit Narayan, Sadhana Sargam
|-
| 4
| "Hum Apni Mohabbat Ka"
| Udit Narayan
|-
| 5
| "Jaanam Mere Jaanam"
| Sadhana Sargam
|-
| 6
| "Kya Dharti Kya Aasman"
| Udit Narayan
|-
| 7
| "Tera Mera Mera Tera Sapna Hai"
| Udit Narayan, Sadhana Sargam
|}

== External links ==
*  

 
 
 
 


 