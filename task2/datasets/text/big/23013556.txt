Sitting Ducks (film)
 
{{Infobox film
| name           = Sitting Ducks
| image          = Sitting_Ducks_Jaglom_Film.jpg
| caption        = Film/DVD promotional art
| director       = Henry Jaglom
| producer       = Meira Attia Dor Michael Jaglom
| writer         = Henry Jaglom
| starring       = Michael Emil
| music          =
| cinematography = Paul Glickman
| editing        =
| distributor    = United Film Distribution Co.
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
}}

Sitting Ducks is a 1980 American comedy film directed by Henry Jaglom. The film follows the adventures of two small-time hoods (Zack Norman and Michael Emil, Jaglom’s real-life brother) who steal a considerable amount of cash from a gambling syndicate. While fleeing by car down the U.S. eastern seaboard for a chartered airplane that will take them to Central America, they pick up a pair of vivacious young ladies and an unsuccessful singer-songwriter.   

The film competed in the Un Certain Regard section at the 1980 Cannes Film Festival.   

==Cast==
* Michael Emil - Simon
* Zack Norman - Sidney
* Patrice Townsend - Jenny
* Irene Forrest - Leona
* Richard Romanus - Moose
* Kelly Rogers
* John Teranova - Mr. Carmichael
* Eric Starr - Gas Station Attendant
* Stasia Grabowski - Collector #1
* Ellen Talmadge - Collector #2
* Madeline Silver - Secretary (as Madeline N. Silver)
* Yoram Kaniur - Collector #3
* Judith Bruce - Head Waitress

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 