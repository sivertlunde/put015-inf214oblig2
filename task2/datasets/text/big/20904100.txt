Jadu Tona
{{Infobox film
| name           = Jadu Tona
| image          = Jadu Tona.jpg
| image_size     =
| caption        = 
| director       =  Ravikant Nagaich
| producer       = 
| writer         =
| narrator       = 
| starring       =  Feroz Khan and Reena Roy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Hindi horror film directed by Ravikant Nagaich. The film stars Feroz Khan and Reena Roy.

==Plot==

Amirchand (Prem Chopra) is a successful businessman and lives a single parent life with his two daughters Harsha (Baby Pinky) and Varsha (Reena Roy). He has his family roots in his paternal village, where he goes to meet his parents (Kanhaiyya Lal and Leela Mishra) once in a blue moon. On one such vacation, Amrichand is stopped at the village outskirts by some villagers, who ask him to pay respect to a local peepal tree, which is considered to be holy. Guided by urban and logical ethics, Amirchand considers this as a village hoax and passes by. However, he and his family dont see the black cat sitting atop the holy tree, staring at them.

Varsha remains busy with her novels but Harsha is overwhelmed to be the part of village life and strolls away in the fields. Once a sage (Prem Nath) arrives at the grandparents household, but is scoffed at by Varsha. The angry sage goes back but promises that once shell realize and witness the power of supreme. One day Harsha runs behind a strange looking butterfly and is led to a dilapidated mansion. She meets an old man there, who asks her to give him the bottle of medicine. He asks her to open the bottle as he is unable to move anywhere. Harsha opens the bottle and is soon occupied by the vengeful spirit of the old man. Harsha is found lying on a muddy road and is taken to her family. The family comes to know that the old man was Hiralal, who died long back mysteriously and had been since wandering as a haunting soul.

Amirchand tries every medication on her daughter but to no avail. Finally the family finds solace at the hands of a famous psychologist Dr. Kailash Arya (Feroz Khan), who considers Harshas feats as a mental condition. In the meantime some of the big wheels of the town are being murdered in the most shocking ways and inspector Jolly Goodman (Ashok Kumar) begins to look for the trails of the murderer. His search ends when he meets Amirchands family.

==Cast==
*Feroz Khan ...  Dr. Kailash 
*Reena Roy ...  Varsha 
*Prem Chopra   
*Aruna Irani
*Ashok Kumar ...  Inspector Jolly Goodman 
*Prem Nath ... Tantrik  
*Baby Pinky ...  Harsha

==External links==
*  

 
 
 
 


 
 