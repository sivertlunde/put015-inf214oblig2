Breakup at a Wedding
{{Infobox film
| name           = Breakup at a Wedding
| image          = Breakup at a Wedding.jpg
| director       = Victor Quinaz
| producer       = Anna Martemucci   Victor Quinaz   Zachary Quinto   Neal Dodson   Corey Moosa  Sean Akers   Steve Golin Richard Brown
| writer         = Anna Martemucci  Victor Quinaz  Philip Quinaz
| starring       = Alison Fyhrie  Philip Quinaz Victor Quinaz Mary Grill Chris Manley Damian Lanigan Anna Martemucci Brian Shoaf Caitlin Fitzgerald Hugh Scully
| cinematography  = Giovanni P. Autran
| editing        = Charlie Porter  Evan Leed Giovanni P. Autran
| studio = Before the Door Pictures  Anonymous Content
| distributor    = Oscilloscope Laboratories
| released       =  
| runtime        = 85 minutes
| country        = United States
}}

Breakup at a Wedding is the first film from comedy collective PERIODS. Directed by Victor Quinaz and written by Anna Martemucci, Victor Quinaz, and Philip Quinaz, the movie was produced by Before the Door Pictures (All Is Lost, Margin Call) and Anonymous Content  (Eternal Sunshine of the Spotless Mind). It was released on June 21, 2013 through Oscilloscope Laboratories.

==Plot==

The night before they are to be married, Phil’s (Philip Quinaz) fiancé Alison (Alison Fyhrie) gets cold feet and decides to call off their wedding. But, after breaking his heart, she manages to convince him to go through a sham ceremony, in order to save face in front of their friends and loved ones. Phil readily agrees, secretly hoping that the surprise wedding gift he has lined up for her will help her to reconsider her decision. Neither of them could have prepared themselves for the multitude of random complications that follow once the guests arrived to witness their breakup at a wedding.

==Cast==
*Alison Fyhrie
*Philip Quinaz
*Victor Quinaz
*Mary Grill
*Chris Manley
*Damian Lanigan
*Anna Martemucci
*Brian Shoaf
*Caitlin Fitzgerald
*Hugh Scully

==Critical reception==
The movie opened on August 8 in New York City and was well reviewed by the The Village Voice,  The New York Times,  and New York Post which gave the film three stars and said: "Breakup at a Wedding works, because Quinaz has come up with a concept that lets him skewer directorial pretension alongside wedding hysteria. He achieves a lot with harsh light, wavering focus and awful framing that occasionally beheads the person on-screen. It takes genuine skill to make a movie look this amusingly cruddy." 

==References==
 

==External links==
*  
*  
*  
*   on Netflix
*   on iTunes

 
 
 