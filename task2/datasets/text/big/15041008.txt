Aag Aandhi Aur Toofan
 

{{Infobox film
| name           = Aag Aandhi Aur Toofan 
| image          = 
| caption        =
| director       = Kanti Shah
| producer       = Mahendra Dhariwal
| writer         = 
| starring       = Mukesh Khanna, Mukesh Goyal, Sadhana Singh    
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  1994
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}}

Aag Aandhi Aur Toofan is a 1994 Hindi language movie directed by Kanti Shah and starring Mukesh Khanna, Mukesh Goyal and Sadhana Singh.

==Cast==
* Mukesh Khanna - Sikandar Khan
* Mukesh Goyal - 
* Sadhana Singh -  
* Karan Kumar - Inspector Arjun Singh
* Joginder Singh - dacoid Chandi
* Upasana Singh - Durga
* Meena Singh - Geeta
* Ashok Raj - Suraj
* Shabnam - Seema
* Sameer - Chhalia

==Plot==
The story revolves around attempts by several people to bring down Chandi, a vicious dacoit. Inspector Arjun Singh wants to destroy him to preserve the law. Durga, Seema, Geeta and Sikandar Khan, who all have reasons to seek revenge on Chandi, agree to help. During an encounter with Chandi, Arjun Singh is injured but he saves everybodys life.

Another element of the plot involves Durga falling in love and marrying Chhalia. But when Durga learns that Chhalia is a traitor against their country, Durga kills him.

==References==
* 

== External links ==
*  

 
 
 


 