Sombrero (film)
{{Infobox film
| name = Sombrero Norman Foster
| image	= Sombrero (film).jpg
| producer =
| writer =
| starring = Ricardo Montalbán Pier Angeli Vittorio Gassman Yvonne De Carlo
| music = Leo Arnaud Geronimo Villavino
| cinematography = Ray June
| editing = Cotton Warburton
| distributor = MGM
| released =  
| runtime =
| language = English
| budget = $1,821,000  . 
| gross = $2,460,000  
}}
Sombrero is a 1953 film starring Ricardo Montalbán, Pier Angeli, Vittorio Gassman and Cyd Charisse.

==Plot==
Three couples involved in budding romances are caught in the middle of a feud between two Mexico villages.

==Cast==
*Ricardo Montalban: Pepe Gonzales 
*Pier Angeli: Eufemia Calderon 
*Vittorio Gassman: Alejandro Castillo 
*Yvonne De Carlo: Marìa of the River Road (Maria del lungofiume)
*Cyd Charisse: Lola de Torrano 
*Rick Jason: Ruben 
*Nina Foch: Elena Cantù 
*Kurt Kasznar: Padre Zacaya 
*Walter Hampden: Don Carlos Castillo 
*Thomas Gomez: Don Homero Calderon 
*José Greco: Gitanillo de Torrano  John Abbott: Don Daniel 
*Andrés Soler: Little Doctor 
*Fanny Schiller: Doña Fela 
*Luz Alba: Rosaura 
*Rosaura Revueltas: Zia Magdalena 
*Alfonso Bedoya: Don Inocente

==Reception==
According to MGM records the film earned $1,071,000 in the US and Canada, and $1,389,000 elsewhere, resulting in a profit of $592,000. 

==References==
 

==External links==
* 

 
 
 