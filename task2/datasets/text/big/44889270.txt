Ninne Preethisuve
{{Infobox film|
| name = Ninne Preethisuve
| image = 
| caption =
| director = Om Prakash Rao Rajakumaran
| Raasi  Sharan
| producer = K Meharunnisa Rehaman   K Musthafa
| music = Rajesh Ramanath
| cinematography = Anaji Nagaraj
| editing = S. Manohar
| studio = Oscar Films
| released =  
| runtime = 152 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada romantic Raasi in lead roles, with Shivarajkumar in an extended guest appearance.  
 Devayani and Ajith Kumar in the lead roles. The film also had its Telugu version released in 2000 as Ninne Premistha starring Meka Srikanth|Srikanth, Soundarya and Akkineni Nagarjuna. 

== Cast ==
* Ramesh Aravind Raasi
* Shivarajkumar 
* Lokesh Sharan
* Mukhyamantri Chandru
* Sihi Kahi Chandru
* Chitra Shenoy
* Sadhu Kokila
* Dingri Nagaraj
* Srilalitha

== Soundtrack ==
The soundtrack of the film was composed by Rajesh Ramanath with the lyrics by K. Kalyan. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Prema Patra
| extra1 = Rajesh Krishnan & K. S. Chithra
| lyrics1 = K. Kalyan
| length1 = 
| title2 = Kogileya Haadu
| extra2 = Rajesh Krishnan & K. S. Chithra
| lyrics2 = K. Kalyan
| length2 = 
| title3 = Gudi Ganteyu
| extra3 = Rajesh Krishnan & K. S. Chithra
| lyrics3 = K. Kalyan
| length3 = 
| title4 = Olave Nanna Olave
| extra4 = Rajesh Krishnan & K. S. Chithra 
| lyrics4 = K. Kalyan
| length4 = 
| title5 = Ninna Preethiya
| extra5 = Rajesh Krishnan & K. S. Chithra
| lyrics5 = K. Kalyan
| length5 = 
| title6 = Nanna Preethiya Devathe
| extra6 = Rajesh Krishnan & K. S. Chithra
| lyrics6 = K. Kalyan
| length6 =
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 

 