Baahubali
 
 

 
 
{{Infobox film
| name           = Baahubali
| image          = Baahubali poster.jpg
| caption        = Film poster
| director       = S.S.Rajamouli
| producer       = Kovelamudi Raghavendra Rao|K. Raghavendra Rao  (Presenter)  Shobu Yarlagadda Prasad Devineni
| writer         = Ajay - Vijay   (Telugu Dialogue)  Madhan Karky  (Tamil Dialogue) 
| screenplay     = S. S. Rajamouli
| story          = K. V. Vijayendra Prasad
| starring       = Prabhas Rana Daggubati Tamannaah Anushka Shetty Sudeep Sathyaraj
| music          = M. M. Keeravani
| cinematography = K. K. Senthil Kumar
| editing        = Kotagiri Venkateswara Rao
| studio         = Arka Media Works
| distributor    = Arka Media Works
| released       = Scheduled for  
| runtime        =
| country        = India Telugu Tamil Tamil               
| website        =  
| budget         = 195 cr 
| gross          =
}} Indian epic Telugu and Tamil languages. Malayalam and in several foreign languages. Baahubali is directed by S. S. Rajamouli and features Prabhas, Rana Daggubati, Anushka Shetty and Tamannaah in the lead roles.    Apart from them, the film features an ensemble cast of Ramya Krishnan, Sathyaraj, Nassar, Adivi Sesh, Tanikella Bharani and Sudeep in crucial roles.
 National Award National Award winner V. Srinivas Mohan is working as the VFX supervisor. 
 Arri Alexa Rock Gardens in Kurnool on 6 July 2013.  
Before the film proceed to its audio release, a theatrical trailer will be unveiled on May 31, 2015 and
the first part of film is scheduled to be released on 10 July, 2015.  

Film producer Karan Johar is presenting the movie in Hindi.  

== Cast ==
 
* Prabhas as Baahubali and Shivudu
* Anushka Shetty as Devasena
* Rana Daggubati as Bhallala Deva
* Tamannaah as Avantika
* Sudeep
* Sathyaraj 
* Nassar
* Ramya Krishnan
* Adivi Sesh
* Tanikella Bharani
* Charandeep
* Baba Sehgal
 

== Production == 
=== Home Media=== Telugu movie satellite rights were ever sold for. 
 

=== Development ===
P. M. Satheesh stated that much effort was taken to keep the recording free of anachronistic sounds of modern machinery.    sword fighting, horse riding. historical movies Nadodi Mannan (1958) and that they would be in chaste Tamil language|Tamil.  The films action sequences were choreographed by Peter Hein who revealed that each and every shot was dealt carefully and there are huge requirements which are not seen in regular movies as it is a complete period film. For a particular action sequence, Peter Hein had to handle around 2000 stuntmen & elephants.  K. K. Senthil Kumar was selected to handle the films cinematography. 

P. M. Satheesh was the sound designer of the film. Regarding his experience with the film, he said "Baahubali is one of the very few films in South where a lot of importance is being given to sound recording. We dropped the idea of shooting with sync sound since the dialogue delivery has to be modified accordingly. The sound design team embeds various types of micro-phones throughout the set to record the ambient sound, which will lend a natural feel to the film. Its necessary, because some of these sounds arent available in the market. Its quite a challenge for everyone".  Sabu Cyril was the art director of the film. In an interview to The Times of India, he said "Every hour is a challenge on the sets of Baahubali. Period films are a huge responsibility as there is no room for mistakes. Everything was created from scratch : chairs, thrones, palaces, swords, armor and costumes."  Foley Artiste Philipe Van Leer started working with the films crew from 5 November 2014 till 14 November 2014 at Dame Blanche complex in Belgium.  Rana revealed that the film is about a war between two brothers - Baahubali played by Prabhas and Bhallala Deva played by himself for a kingdom. 

=== Casting === Tamil actor Sathyaraj signed the film.    There were reports towards the end of April 2013 that either Sridevi or Sushmita Sen would be selected to play Prabhas mother in the film which were confirmed as rumors by Rajamouli.   In the end of May 2013, Rajamouli denied the casting of Sonakshi Sinha as a replacement to Anushka in the Hindi version of the film.  There was news that actress Shruti Haasan would be portraying the female lead, but Rajamouli denied the news as a rumor and clarified that she was not approached for any role. 
 Sunil as baseless rumors. 

=== Characters and looks ===
Anushkas characters name was revealed as Devasena.    Rana Daggubati was said to be playing the role of Prabhas brother and He too underwent a lot of physical transformation for the role he was assigned to play in the film.  He also underwent training in Martial arts under the supervision of a Vietnamese trainer Tuan.  Sathyaraj sported a Tonsured look for his role in the film.  Sudeep said that he would play the role of a weapons trader in this film.  In the end of October 2013, Rana appeared at a fashion show with a beefed up body which, according to him, was a part of his look in the film.  In mid-May 2014, reports emerged that Anushka would be seen as a pregnant woman for a few sequences in the second part of the film. 

At the same time, Prabhas posted in his Facebook page that he underwent a minor shoulder surgery a month ago and would join the films sets in a span of a month which was also confirmed by Rajamouli.  In the end of May 2014, Prabhas attended a movie launch ceremony which dispelled the rumors which stated that he was in ICU battling several health issues and injuries.  On 1 June 2014, Prabhas and Ranas trainer Lakshman Reddy, who was the winner of 13 titles in India and abroad in bodybuilding, spoke to the media about the secrets behind the looks of Prabhas and Rana. According to him, Prabhas started his training 8 months before the commencement of shooting and after two years, both of them weighed nearly 100 kilos each. He also added that Prabhas would be seen in two attires with a heavy, bulky body for Baahubalis role and a lean physique for the second role. About the food intake, he said "Their daily diet consisted of six to eight meals – all non-vegetarian. No rice was given at all. Meals were eaten every two hours. The total calorie intake would range between 2000 to 4000 calories a day." For reduction of their weight, Reddy said "The process will be the same but you have to limit the food intake, depending on the person’s requirements. It takes four to five months to reduce".  For his look, Prabhas met WWE superstars in February 2014 and interacted with them about their daily regimen and workouts. 

Impressed with the infrastructure there, Prabhas got the equipment costing  1.5 crore shipped to his home, where he built a personal gym. His breakfast included 40 half boiled egg whites blended and added with protein powder everyday.    In mid-June 2014, regarding her role in the film, Tamannaah said that she would be playing the role of a warrior princess named Avanthika and her appearance in the film is completely different when compared to her past films.     Before joining the films shoot, Tamannaah did costume trials for the film which she confirmed in her micro-blogging page stating "I am very excited to get on to the set of Baahubali. I did some dress trials today and my look in this movie will be totally new. I have never been seen in such sort of a look till now. It will be a new role for me."  Rajamouli called Tamannaah and her characterization as a "value addition" to the movie.  She later revealed "I play Avanthika, a character from an unspecified era. A lot of thought went into the costume and jewelry to get the look right. Director Rajamouli asked me to lose 5-6 kilos to look the part, so I followed a stringent workout and diet regimen to get the look right" in an interview to Hemanth Kumar of The Times of India. 

=== Filming ===
  in Kurnool where principal photography began.]] Rock Gardens in Kurnool from 6 July 2013 exactly after a year from the release of Rajamoulis Eega (2012).        In the end of August 2013, the films shoot continued at Ramoji Film City in Hyderabad where key scenes on the lead cast were shot.  The second schedule of the film ended on 29 August 2013.  A new schedule started at Hyderabad on 17 October 2013.  In the end of October 2013, the maize field specifically cultivated at Ramoji Film City for filming of few crucial sequences was destroyed by rains just a week before the start of the planned shoot there.  The films shoot again continued in Kurnool in November 2013 but the schedule ended abruptly due to incessant rains. Despite taking required measures, the films crew could not control the people and around 30 thousand people reached the spot. After they were pacified by Prabhas and Rana, Rajamouli stood on the center stage and asked all of them to scream Jai Baahubali in sync. The entire incident was captured by the sound department so that it can be used in the films final cut to create the right ambient sound in some crucial scenes.  After that, the films unit traveled to Kerala for their next schedule.  The Kerala schedule started on 14 November 2013. 

  in Thrissur district of Kerala where few crucial sequences were shot as a part of the schedule.]]
In the end of November 2013, the films shoot was disrupted by incessant rains and since the portion of shoot was outdoor, the shoot was temporarily stalled. {{cite web|url=
http://www.123telugu.com/mnews/rain-disrupts-baahubalis-shoot-in-kerala.html|title=Rain disrupts Baahubali’s shoot in Kerala|publisher=123telugu.com|date=27 November 2013|accessdate=19 July 2014}}   The shoot at Kerala completed on 4 December 2013 in many exotic locales of Kerala including the famous Athirappilly Falls and Rajamouli informed to the media that a huge war sequence would be shot soon at Hyderabad.  A set was erected in Ramoji film city to shoot war sequences involving around 2000 junior artists and almost all the principal cast from 23 December 2013 for which groundwork began in October 2013.   There were reports that the farmers at Anajpur village close to Ramoji Film City tried to disrupt the films shooting citing that they did not have the required permissions to shoot there which were denied by Rajamouli.   The films unit took a two-day break on the eve of New Year and the shoot of the sequence resumed from 3 January 2014.   In mid-January 2014, a massive set was constructed there at Ramoji Film City resembling the city center of the kingdom in which the story unfolds.  The films unit took a break on account of Makar Sankranti and the shoot of war sequences resumed from 16 January 2014.  On 18 January 2014, the film completed 100 working days of the films shoot. 

  where the films major parts including a huge war sequence was shot in specially erected sets.]]
From 28 March 2014, key scenes of the film were shot during the nights at Ramoji Film City.  On 5 April 2014, Rajamouli informed that the war schedule came to an end.  The films next schedule started on 20 April 2014 after a brief break.     The films unit took a small break in the end of May 2014 after shooting some scenes on Rana Daggubati and Anushka at Ramoji Film City.   Later Rana took a break from Baahubalis shooting for a couple of months.  In the end of May 2014, it was reported that Tamannaah would join the sets in June 2014 and would participate till December 2014.  Sudeep returned to the films sets on 7 June 2014 and participated in the shoot along with Sathyaraj at  

  where the film was shot in extreme climatic conditions including fog, rain and cold weather.]]
A romantic song featuring Prabhas and Tamannaah was shot in a specially erected set in Ramoji Film City in the third week of July 2014 which was choreographed by K. Sivasankar.  The song was shot using ropes and trusses which are generally used in the action scenes and the films director of photography K. K. Senthil Kumar confirmed the same and added that the song is shot with an innovative concept with rich visuals.  After its completion, an action sequence was shot at Ramoji Film City under the supervision of Peter Hein.  On 10 August 2014 the film was declared as the first Telugu film to have been shot for 200 days.   A fresh schedule started at Mahabaleshwar on 26 August 2014.  The cast and crew had to brave bad weather, including rain, fog and cold weather to complete the scenes.  After nearly two weeks, the team wrapped up the schedule and the next schedule began at Ramoji Film City in Hyderabad from 12 September 2014.  Sabu Cyril designed a 100 feet statue which was erected by Peter Hein in late September 2014.  Filming continued at Hyderabad though the rest of the films shoots were halted because of the ongoing strike by Telugu Film Federation Employees as they employed the staff not belonging to the Federation. 

Few action sequences were shot at Ramoji Film City till 30 November on Prabhas and Rana.  During the films shoot for a particular sequence, Tamannaah stood under an artificial tree designed by Sabu Cyril as a part of the set to which she was tied to make sure that she did not fly away because of the strong winds.  In early December 2014, the films unit shifted to Bulgaria for a 25-day schedule. It was reported to shift to Bulgaria from Hyderabad because of the ongoing Telugu Film Federation Employees strike though the makers refuted those reports adding that the schedule was planned long ago.  After three weeks of shoot, the films unit returned to Hyderabad on 23 December 2014.  The films shoot later continued at Ramoji Film City and from 22 December 2014, thousand horses brought from Rajasthan were planned to be used for shooting. 

== Music ==
Rajamoulis cousin and his norm composer M. M. Keeravani composed the music for this film. After the break taken by the films team after completion of war schedule on 5 March 2014, an official statement from the films team stated that Keeravani is recording two songs from the film right now.  Madhan Karky was selected as the lyricist for the Tamil version of the soundtrack.  On 2 July 2014, the films official website published that Keeravani is recording a song at Prasad Studios in Hyderabad which is sung by Deepu.  On 8 October 2014, Keeravani told Deccan Chronicle that the writers are working on the lyrics of the songs and he would start working on them in a weeks time.  During the films shoot in Bulgaria, the films soundtrack was expected to be unveiled in February 2015. 

== Release ==
The film was scheduled to be released in July 2015.

=== Marketing ===
A number of short promotional "making of" videos has been released.                  For releasing all those videos, Arka Media Works had its own channel on YouTube and the team unveiled first look posters and videos featuring the films lead stars on the occasions of their birthdays.  The film used an augmented reality application to play the trailer on smart phones and tablets.  The crown used by the character of Baahubali in the film was exhibited at Comic Con, Hyderabad as a part of the films promotion. An event named Cos Play was held in which chosen winners were given a chance to visit the sets of the film.  The films unit also launched a WhatsApp messenger to give regular updates about the film to the subscribers. 

=== Distribution === Ceded (Rayalaseema) Ceded region, which included Kadapa, Kurnool, Anantapur district|Anantapur, Chittoor and Bellary areas, the rights alone fetched     At the same time, the films Nizam region theatrical distribution rights were purchased by Dil Raju for an amount of  . only for the first part.   Though he did not confirm the price, Dil Raju said in an interview to Deccan Chronicle that he purchased the first parts Nizam region rights and added that he would acquire the rights of the second part also for this region.  BlueSky Cinemas, Inc. acquired the theatrical screening and distribution rights in overseas.  

=== Title issues === Sodhe Matha a revered Arihant in Jainism as they were afraid that the film would portray his story in a violent manner.    Days later, Sobhu Yarlagadda defended the story of the film saying "The film has nothing to do with Gomatheswara or the Jain religion. The story is completely fiction written by Vijayendra Prasad and will remain so. Baahubali refers to the amount of power the protagonist possesses." He also clarified that they havent received the legal notice yet. 

=== Accusations of plagiarism and clarification === Adobe After Effects CS5 having a customizable template, depicts how film makers can use the video template to market their products. Regarding the issue, Shobhu Yarlagadda clarified "We have commercially purchased the template used in that video. We have licensed it legally. Anyone can license that template and it is not copying. We felt that the template is ideal for our video and hence we paid for it." 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 