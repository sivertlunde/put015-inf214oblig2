A Flash of Light
 
{{Infobox film
| name           = A Flash of Light
| image          = 
| alt            =  
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = Stanner E. V. Taylor Charles West
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| studio         = 
| distributor    = Biograph Company
| released       =  
| runtime        = 17 minutes
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          = 
}}
 short silent silent drama Charles West and featuring Mary Pickford and Blanche Sweet.   

==Cast== Charles West - John Rogers
* Vivian Prescott - Belle
* Stephanie Longfellow - The Older Sister
* Verner Clarges - The Father
* Joseph Graybill - Horace Dooley
* Dorothy Bernard
* William J. Butler - A Doctor Charles Craig - Wedding Guest Edward Dillon - At First Party John T. Dillon - At First Party / At Second Party (as Jack Dillon)
* Ruth Hart - At Second Party
* Guy Hedlund - At First Party / At Second Party
* Grace Henderson - Visitor
* Henry Lehrman - At Second Party (unconfirmed)
* Jeanie Macpherson
* Claire McDowell - At First Party George Nichols - A Doctor
* Anthony OSullivan - A Servant
* Alfred Paget - Wedding Guest
* Mary Pickford
* Gertrude Robinson - Wedding Guest
* W. C. Robinson - A Servant
* Mack Sennett - Wedding Guest
* George Siegmann - Wedding Guest
* Blanche Sweet
* Kate Toncray - A Servant Dorothy West - At First Party

==See also==
* List of American films of 1910
* D. W. Griffith filmography
* Mary Pickford filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 