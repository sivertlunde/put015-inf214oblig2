The Electric Grandmother
 
{{Infobox film
| name = The Electric Grandmother
| screenplay = {{Plainlist|
* Ray Bradbury
* Jeff Kindley
}}
| based on =  
| starring = {{Plainlist|
* Maureen Stapleton
* Edward Herrmann
* Paul Benedict
* Tara Kennedy
* Robert MacNaughton
}}
| director = Noel Black John Morris
| cinematography = Mike Fash
| editing = Dennis M. OConnor
| country = United States
| released =  
| distributor = Coronet Video
| runtime = 49 min.    English
}}
 I Sing I Sing the Body Electric", an episode of The Twilight Zone. The film was distributed on VHS by Coronet Video, but the video tape has now become hard to obtain, though the video is currently on YouTube.

==Awards== nominated for a Young Artist Award in the category Best Young Actress in a Movie Made for Television. 

==Reviews==
"Devotees of science fiction in school classes or public library programs will find this a fascinating interpretation of Ray Bradburys fiction." &mdash; School Library Journal 

==References in Pop Culture==
Noted electro-pop/comedy musical duo The Electric Grandmother took their name from the film. 

==References==
 

==External links==
*  
*  

 
 
 
 
 

 
 
 
 
 
 


 
 