Three Smart Girls
{{Infobox film
| name           = Three Smart Girls
| image          = Three_Smart_Girls_Poster.jpg
| border         = yes
| caption        = Lobby card
| director       = Henry Koster
| producer       = Joe Pasternak
| writer         = 
| screenplay     = {{Plainlist|
* Adele Comandini
* Austin Parker
}}
| starring       = {{Plainlist|
* Barbara Read
* Nan Grey
* Deanna Durbin
* Ray Milland
}}
| music          = 
| cinematography = Joseph A. Valentine
| editing        = Ted J. Kent
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} musical comedy film directed by Henry Koster and starring Barbara Read, Nan Grey, Deanna Durbin, and Ray Milland.    The film, Durbins feature film debut, had a screenplay by Adele Comandini and Austin Parker, and is about three sisters who travel to New York City to prevent their father from remarrying. The three plot to bring their divorced parents back together again.

It began an eight-year era of successful Deanna Durbin musicals and spawned two sequels, Three Smart Girls Grow Up and Hers to Hold. 

==Cast==
* Deanna Durbin as Penny Craig
* Nan Grey as Joan Craig
* Barbara Read as Kay Craig
* Binnie Barnes as Donna Lyons
* Charles Winninger as Judson Craig
* Alice Brady as Mrs. Lyons
* Ray Milland as Lord Michael Stuart
* Mischa Auer as Count Arisztid
* Ernest Cossart as Binns
* Lucile Watson as Martha Trudel
* John Dusty King as Bill Evans (as John King)
* Nella Walker as Dorothy Craig
* Hobart Cavanaugh as Wilbur Lamb

==Production==
Ray Milland was a last minute replacement from Louis Hayward who was originally cast but fell ill shortly before filming began. SUPER STYLE PAGEANT PROMISED IN IRENE DUNNE FEATURE: Sparkle of Alice Faye to Lend Zip to Temple Film
Schallert, Edwin. Los Angeles Times (1923-Current File)   21 Sep 1936: 13. 

==Awards==
Three Smart Girls received an Academy Award nomination for Best Picture, Best Sound (Homer G. Tasker), and Best Original Story.   

==Cultural References== On The Town, there is a reference to Three Smart Girls in the title song, sung by Betty Garrett, Ann Miller and Vera-Ellen.


==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 