Movement (R)evolution Africa
 

{{Infobox film
| name           = Movement (R)evolution Africa
| image          = 
| caption        = 
| director       = Joan Frosch, Alla Kovgan
| producer       = Joan Frosch
| writer         = 
| starring       = Company Kongo Ba Téria (Burkina Faso), Faustin Linyekula and Studios Kabako (Congo), Company Rary (Madagascar), Sello Pesa (South Africa), Company TchéTché (Côte dIvoire), Company Raiz di Polon (Cape Verde), Company Jant Bi (Senegal), Kota Yamazaki (Japan), Nora Chipaumire (Zimbabwe), Jawole Willa Jo Zollar (USA), Urban Bush Women (USA)
| distributor    = 
| released       = 2007
| runtime        = 65 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
| screenplay     =  Jeff Silva
| editing        = Alla Kovgan
| music          = 
}}

Movement (R)evolution Africa is a 2007 documentary film.

== Synopsis ==
In an astonishing exposition of choreographic fomentation, nine African choreographers tell stories of an emergent art form and their diverse and deeply contemporary expressions of self. Stunning choreography and riveting critiques challenge stale stereotypes of “traditional Africa” to unveil soul-shaking responses to the beauty and tragedy of 21st century Africa.

== Awards ==
* Cyprus 2007

== References ==
 

 
 
 
 
 
 


 