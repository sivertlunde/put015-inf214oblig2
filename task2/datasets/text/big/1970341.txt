The Wild Ride
{{Infobox film
| name           = The Wild Ride
| image          = Original film poster for The Wild Ride.jpg
| image_size     = 200px
| caption        =
| director       = Harvey Berman
| producer       = Harvey Berman (producer) Kinta Zertuche (executive producer)
| screenplay     = Ann Porter Marion Rothman
| story          = Burt Topper
| starring        = Jack Nicholson  Georgianna Carter  Robert Bean
| music          =
| cinematography = Taylor Sloan
| editing        = Monte Hellman William Mayer
| distributor    = Filmgroup
| released       =  
| runtime        = 59 minutes 88 minutes (producers cut)
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 dirt track cult classic.

== Plot==
 

=== Re-cut version === Jack Nicholson impersonator Joe Richards playing an older version of the character originally played by Nicholson, as well as performances by Jorge Garcia, Jason Sudeikis, and Dick Miller.

== Cast ==
*Jack Nicholson as Johnny Varron
*Georgianna Carter as Nancy
*Robert Bean as Dave
*Carol Bigby as Joyce
*John Bologni as Barny
*Gary Espinosa as Cliff
*Judith Tresize as Ann
*Wesley Marie Tackitt
*Sydene Wallace
*Donna Dabney
*Garry Korpi
*Leonard Williams
*John Holden
*Raymond ODay
*Carl Vicknair as Police Officer #1

==Production==
The executive producer on the film was Roger Corman. Harvey Berman was a high school drama teacher in northern California who had gone to the UCLA drama school with some friends of Corman. He decided to make a film during the summer using some of his high school drama class students in the cast and crew and sending a few Hollywood professionals to work with them. One of these was Jack Nicholson. Corman later wrote "this is one of the little pictures I remember with pleasure; it turned out very well." Ed. J. Philip di Franco, The Movie World of Roger Corman, Chelsea House Publishers, 1979 p 138 

==References==
 

== External links ==
* 
*  
* 
* Like Dreamsvilles synopsis of the film, with photos and commentary  

 

 
 
 
 
 
 
 
 


 