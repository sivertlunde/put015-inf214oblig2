Snowbound (1948 film)
Snowbound British thriller David MacDonald and starring Dennis Price, Stanley Holloway, Mila Parély, and Herbert Lom.  A group of people search for treasure hidden by the Nazis in the Alps following the Second World War. It was based on the novel The Lonely Skier by Hammond Innes.

==Cast==
* Dennis Price - Neil Blair 
* Mila Parély - Carla Rometta, alias Comtessa Forelli 
* Stanley Holloway - Joe Wesson 
* Marcel Dalio - Stefano Valdini 
* Guy Middleton - Gilbert Mayne 
* Herbert Lom - Von Kellerman, alias Keramikos 
* Robert Newton - Derek Engles 
* Willy Fueter - Aldo, innkeeper 
* Catherina Ferraz - Emilia, innkeeper 
* Richard Molinas - Mancini, rental agent 
* Rositer Shepherd - Italian Lawyer Buying Inn 
* Zena Marshall - Italian Girl Flirting with Joe 
* William Price - Capt. Heinrich Stelben 
* Lionel Grose - Cpl. Holtz 
* Gilbert Davis - Commissionaire

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 