Marvelous (film)
{{multiple issues|
 
 
}}
 

{{Infobox film name           = Marvelous image          = caption        = director       = Síofra Campbell producer       = Síofra Campbell Ian Moore writer         = Síofra Campbell starring       = Martha Plimpton music          = cinematography = editing        = distributor    = released       = 2006 runtime        = 90 min. country        = United States language       = English budget         = admissions     = gross          = website        =
}}
 2006 American drama/comedy written and directed by Síofra Campbell.

==Plot==
Martha Plimptons character, Gwen, is living with her sister Queenie after getting divorced. Soon after the movie begins, it is revealed that Gwen has the ability to magically fix broken machinery and heal sick and injured people. The film explores the effect that celebrity has on Gwen and her family and friends.

==Cast==
{| class="wikitable"
|-
! Actor / Actress
! Character
|-
| Martha Plimpton
| Gwen
|-
| Ewen Bremner
| Lars
|-
| Amy Ryan
| Queenie
|-
| Annabella Sciorra
| Lara
|-
|}

==Box office==
 

==Critical reception==
As an independent film, Marvelous did not have a great deal of critical focus. What there was, was mixed. Ronnie Scheib of Variety praised the "marvelously peculiar dialogue" and the good work of the actors. The reviewer at Moviefone.com calls it "a memorable satire." However, New York Magazine found the script "uneven" and praised only the work of Ewen Bremner and referred to the work of the other actors as "overacting." MSN movies gives it three stars out of five.

==External links==
*http://movies.yahoo.com/movie/1809426122/details
*http://www.imdb.com/title/tt0800347/maindetails
*http://www.variety.com/review/VE1117930624.html?categoryid=31&cs=1&p=0
*http://nymag.com/movies/reviews/16813/index4.html
*http://www.truveo.com/Marvelous-Trailer/id/4063822694# clip from the movie
*http://movies.msn.com/movies/movie-synopsis/%27marvelous%27/

 
 
 


 