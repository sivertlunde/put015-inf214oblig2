Atlantic City (1944 film)
{{Infobox film
| name           = Atlantic City 
| image          = Atlantic City 1944 poster.jpg
| border         = yes
| caption        = Original theatrical poster
| producer       = Albert J. Cohen
| director       = Ray McCarey
| story         = Arthur Caesar
| screenplay = Doris Gilbert Frank Gill Jr. George Carleton Brown
| starring       = Constance Moore Stanley Brown Charley Grapewin
| cinematography = John Alton
| editing         = Richard L. Van Enger
| studio = Republic Pictures
| distributor    = Republic Pictures   British Lion Films  
| released   =  
| runtime        = 87 min.
| language = English
}} musical Romance romance directed by Ray McCarey. The film concerns the formative years of Atlantic City, New Jersey. Vaudeville acts are re-created in the story of how Atlantic City became a famous resort.  The supporting cast features Louis Armstrong and Dorothy Dandridge. The film was reissued in 1950 under the title Atlantic City Honeymoon. 

==Plot==
In 1915, Atlantic City is a sleepy seaside resort, but Brad Taylor, son of a small hotel and vaudeville house proprietor, has big plans: he thinks it can be "the playground of the world." Brads wheeling and dealing proves remarkably successful in attracting big enterprises and big shows, but brings him little success in personal relationships. Full of nostalgic songs and acts, some with the original artists.

==Cast==
*Constance Moore as Marilyn Whitaker
*Stanley Brown as Brad Taylor
*Charley Grapewin as Jake Taylor
*Paul Whiteman as Himself 
*Louis Armstrong as Himself
*Robert B. Castaine as Carter Graham
*Dorothy Dandridge as Singer
*Adele Mara as Barmaid
*Ford L. Buck as Himself
*John W. Bubbles as Himself
*Gus Van as Himself

==References==
  

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 
 


 