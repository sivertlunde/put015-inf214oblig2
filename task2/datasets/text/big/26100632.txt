Hesher (film)
{{Infobox film
| name           = Hesher
| image          = Hesher Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Spencer Susser
| producer       = Natalie Portman Spencer Susser Morgan Susser Lucy Cooper Johnny Lin Scott Prisand Win Sheridan
| screenplay     = Spencer Susser David Michôd
| story          = Brian Charles Frank
| starring       = Joseph Gordon-Levitt Rainn Wilson Natalie Portman Devin Brochu
| music          = Francois Tetaz Metallica Motörhead
| cinematography = Morgan Susser
| editing        = Michael McCusker Spencer Susser
| studio         = The Last Picture Company CatchPlay Corner Store Entertainment Newmarket Films
| distributor    = Wrekin Hill Entertainment
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $7 million   
| gross          = $449,702 
}} heavy metal bands Metallica and Motörhead.

==Plot== heavy metal-loving lout.

After school, Dustin, a bully from the towing service, finds his car vandalized and blames T.J., who is saved and taken home by Nicole (Natalie Portman), a grocery store clerk. Hesher witnesses Dustin attacking T.J., but does nothing. Later, Hesher sets Dustins car on fire, leading to police questioning T.J., but lack of evidence forces them to let him go. T.J. proceeds to spy on Nicole at the grocery store when Hesher appears and follows her home with T.J. She causes a fender-bender and is verbally assaulted by the other driver, when Hesher comes to her rescue by threatening him. He then takes Nicole and T.J. to a random house that is for sale. There, Hesher trashes the swimming pool and lights the diving board on fire before he shocks Nicole and T.J. by leaving them there alone. When the pair get back to Nicoles broken down car, it has a ticket in the window, at which point Nicole breaks down and cries about how bad her life is.

Later that night, T.J. and Paul get into an argument over dinner. Madeleine is saddened that there is nothing she can do, and goes to her room. Hesher says hell walk with her in the morning, but the next morning, he finds her dead. T.J. steals his dads credit card and takes money to buy the car back, but is told its been removed. He decides to give the money to Nicole, but when he gets there she is having sex with Hesher, so he damages his van while telling both of them he doesnt want to see them again and leaves.

He threatens Dustin at his house and finds out the car was taken to be crushed. Dustin begins attacking T.J. until Hesher, who has been following him, appears and drags Dustin off him. T.J. tells him he still doesnt want to see him again and then goes to the junkyard. He climbs in the wrecked car and sleeps, dreaming of his mothers death.
He wakes up when the car is going to be crushed and falls out. He returns home after seeing it destroyed. While getting ready for his grandmothers funeral, Nicole comes and asks for forgiveness and leaves him. At the funeral, T.J. is asked to say words for her, but has nothing to say. 

Hesher walks in drunk and demands that the mourners listen to him. He tells a story about how when he was younger he blew up a car and shrapnel destroyed one of his testicles, and he was upset about it until he realized he still had one and a penis that works, and that Paul and T.J. both lost a loved one but still have each other, and that he promised to walk with Madeleine, so he takes the casket. He is followed by T.J. and Paul, who join him in walking her to the cemetery.

The next day, Paul shaves for the first time in weeks and shows T.J. the compacted remains of his mothers car, which Hesher has procured from the junkyard and dumped in the driveway. On their roof, he has painted in huge white letters "Hesher was here."

==Cast==
* Joseph Gordon-Levitt as Hesher
* Rainn Wilson as Paul Forney
* Natalie Portman as Nicole
* Devin Brochu as T. J. Forney
* Piper Laurie as Madeleine Forney
* John Carroll Lynch as Larry
* Audrey Wasilewski as Coleen Bolder
* Brendan Hill as Dustin
* Monica Staggs as Mom
* Frank Collison as Funeral Director

==Reception==
Hesher received mixed reviews upon its release. Review aggregator Rotten Tomatoes reports that 54% of 68 film critics have given the film a positive review, with a rating average of 5.7 out of 10 and a consensus stating "It has a dark sense of humor and a refreshing lack of sentimentality, but like its title character, Hesher isnt really interested in going anywhere."  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 45 based on 26 reviews. 
 Hallmark sentiment, you cant take your eyes off him."  However, other critics, such as Roger Ebert of the Chicago Sun-Times gave the film more lukewarm reviews. Ebert said, "Hesher assembles a group of characters who arent sure why theyre in the same movie together. One by one, they have an attraction, but brought together, theyre all elbows and angles." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 