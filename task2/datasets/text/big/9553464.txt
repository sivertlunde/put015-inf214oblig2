Press Start
{{Infobox film
| name           = Press Start
| image          = Pressstartposter.png
| image size     = 
| alt            = 
| caption        = Theatrical film poster
| director       = Ed Glaser
| producer       = Ed Glaser
| writer         = Kevin Folliard
| screenplay     = 
| story          =
| based on       =  
| narrator       =  Peter Davis Daniel Pesina Carlos Pesina
| music          = Jake Kaufman
| cinematography = Ed Glaser
| editing        = Ed Glaser
| studio         = Dark Maze Studios
| distributor    = Dark Maze Studios
| released       =  
| runtime        = 100 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 2007 independent Peter Davis, Daniel Pesina, and Carlos Pesina. {{cite news
|url=http://www.gamespot.com/news/2006/03/10/news_6145780.html
|title=GameSpot: Mortal Kombat actors get film work
|last=Surette
|first=Tim
|date=2006-03-10
|publisher=GameSpot}}   The film is a video game parody adventure comedy. It was released on DVD on September 25, 2007.

==Production== Champaign and American video game music composer Jake Kaufman.
 Scorpion and Raiden (Mortal Kombat)|Raiden.  A sequel, Press Start 2 Continue, was released in 2011.

==Focus== Donkey Kong to Halo (video game series)|Halo. The film also satirizes all aspects of gaming,  including collectible card games, Saturday morning cartoons, comic books, and trends of turning games into breakfast cereals. {{cite news
|url=http://videogames.yahoo.com/ongoingfeature?eid=453008&page=0
|title=Yahoo! Games: The Hollywood Byte #15: The Comedy of Gaming
|last=Gaudiosi
|first=John
|date=2006-04-21
|publisher=Yahoo! Games}} 

==Synopsis==
Press Start tells the story of a suburban youth in a videogame world who discovers his adventurous, if berserk, destiny when hes recruited by an ill-tempered ninja and a tough-as-nails space soldier to save the world from a tyrannical, but comically insecure, sorcerer. {{cite web
|url=http://www.pressstartmovie.com/about.html|title=Press Start Website: Synopsis
|accessdate=2007-02-16  archiveurl = http://web.archive.org/web/20070205135005/http://www.pressstartmovie.com/about.html    archivedate = 2007-02-05}} 

==Cast==
* Joshua Stafford as Zack Nimbus Peter A. Davis as Count Nefarious Vile
* Daniel Pesina as Sasori
* Carlos Pesina as Lei Gong
* Lauren Chambers as Sam
* Al Morrison as Lin-Ku
* J.W. Morrissette as Johnson
* Andy Dallas as Shopkeeper
* Michael Kleppin as G. Fourman
* Ben McDuffee as Telegram Man
* J.R. Thomas as Uncle Lou
* Meagan Benz as Zippy (voice)
* Jane F. Cox as Life Lady Arin Hanson as Forest Guardian (voice)
* Jennifer Zahn as Villager

===Bonus Levels/Adventures===
 

While in production and post-production, a series of web shorts called Press Start: Bonus Levels (known as Press Start Adventures following the movies release) were released on the films website and Newgrounds. The series was created and animated by screenwriter Kevin Folliard and produced by Press Start director Ed Glaser. The pilot episode was directed by Folliard, while the following episodes were directed by Glaser. Original music is composed by Jake Kaufman.

They premiered on April 28, 2006. Since, theres been a new episode on the last Friday of every following month. The episodes feature numerous visual gags and video game parodies.

The cartoons were created as a way to generate hype for the feature film by introducing people to the world, characters, and humor of Press Start. They are also noted by their lack of budget constraints allowing more artistic freedom to be taken with the action and effects than that seen in the feature film. Back story, inside jokes, and running gags are further flushed out in the animations than what could be used in the film.
 Mortal Kombats John Turk (who ironically took over the ninja roles portrayed by Pesina), and David Humphrey, the original voice of Shadow the Hedgehog.

The animations take place before the events of the movie, similar to two made-for-DVD animated prequels:   and  . The animations started out being nonlinear and do not progress after one another. Notably missing is the character of Zack Nimbus who, in terms of the film, has yet to be made aware of his destiny. However, on November 24, 2006 the shorts began a five-part miniseries which would lead directly into the feature film.

The shorts have since been hosted on the comedy media website That Guy with the Glasses, continuing new episodes now taking place after the films conclusion, now known as Press Start Adventures. Count Vile is now depicted as being in Hell, and the character Zack Nimbus appears, as well.

==Critical reception==
PC Gamer wrote that the film was "a surprisingly well-written parody of video game conventions". {{cite news
|url=http://www.darkmaze.com/pressstart/press/pcg0208p018.pdf|title=review: Press Start
|date=February 2008
|work=PC Gamer
|accessdate=23 November 2010}}   Movie Cynics makes note that if a filmmaker were to do everything possible to make a bad movie, Ed Glaser has succeeded and that Glaser has not only made a bad movie, but he has made an "awesomely bad movie ... that pokes fun at video games."  And in its being so bad, the "end result was a film that is a quintessential awesomely bad movie that is full of videogame references and is fun from beginning to end." {{cite web
|url=http://www.moviecynics.com/press-start-2007-independent-film-review/
|title=Press Start (2007) – Independent Film Review
|last=Bless
|first=Bobby
|date=July 17, 2010
|work=Movie Cynics
|accessdate=23 November 2010}} 

==Sequel==
In 2011, a direct-to-video sequel was made entitled Press Start 2 Continue, also produced and directed by Ed Glaser.

==References==
 

==External links==
* 
* 
* 
* 
* 
*  

 

 
 
 
 
 
 