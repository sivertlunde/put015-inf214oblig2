Blood from the Mummy's Tomb
 
 
{{Infobox film
| name           = Blood from the Mummys Tomb
| image          = Bloodmummytomb.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Seth Holt
| producer       = Howard Brandy
| writer         = Christopher Wicking 
| starring       = Valerie Leon Andrew Keir Mark Edwards James Villiers Hugh Burden Aubrey Morris
| music          = Tristram Cary Arthur Grant
| editing        = Peter Weatherly
| studio         = EMI Films|EMI-Elstree Hammer Film Productions
| distributor    = Anglo-EMI Film Distributors Ltd. EMI Films#MGM-EMI|MGM-EMI (UK) American International Pictures (USA)
| released       = 14 October 1971 
| runtime        = 94 min
| country        = United Kingdom  English
| budget         = £200,000
}}
 British film starring Andrew Keir, Valerie Leon, and James Villiers. This was director Seth Holts final film, and was loosely adapted from Bram Stokers novel The Jewel of Seven Stars. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 28  The film was released as the support feature to Dr. Jekyll and Sister Hyde.

==Plot==
An expedition led by British archeologist Professor Fuchs (Keir) attempts to locate the cursed tomb of an evil Egyptian princess. Her body is found perfectly preserved. Fuchs becomes obsessed by Princess Teras beauty and takes the body and sarcophagus back to London, building a secret shrine in his basement. Not long after, his daughter Margaret (Leon), who bears an uncanny resemblance to the princess, starts to experience strange dreams. A sinister man, whom only she seems to notice, watches her from the derelict house across the road. Wondering if shes going mad, Margaret starts experiencing wild personality changes.

Soon the evil will of Tera, reaching up through the house, begins to periodically take over Margarets mind and body. The strange man is revealed as Corbeck (Villiers), one of the expedition members, who is working to restore Tera to life and create a new reign of terror, and with his help Margaret kills the other desecrators of Teras tomb, and everyone that stands in her way. Corbeck, Margaret and her increasingly unhinged father start the ritual to awaken the princess. Professor Fuchs finally realizes the truth and convinces her to stop the ritual; together they overpower and kill Corbeck. Tera awakens, and in an attempt to stop Tera, the Professor is killed.  After a struggle Margaret stabs Tera in the heart, but Tera continues to struggle. Both appear to be unconscious when the tomb-like shrine collapses, as if by earthquake.

Later, in the hospital, we see a woman wrapped in bandages. We are told that the woman is the only survivor, with all of the others crushed beyond recognition. In the end, we see the woman open her eyes, and appear to struggle to speak. We are left unsure whether the woman is Margaret or Tera.

==Production== R1 DVD America by Anchor Bay Entertainment contains still photographs of Cushings day on the production. Director Seth Holt died of a heart attack five weeks into the six week shoot, collapsing into cast member Aubrey Morriss arms and dying on set.    Michael Carreras directed the final weeks filming.

According to the book Hammer, House of Horror: Behind the Screams by Howard Maxford, the budget for the film was £200,000. 

The film was shot at Elstree Studios in Hertfordshire.

==Cast==
* Andrew Keir as Julian Fuchs
* Valerie Leon as Margaret Fuchs/Queen Tera
* James Villiers as Corbeck
* Hugh Burden as Geoffrey Dandridge
* George Coulouris as Berigan
* Mark Edwards as Tod Browning
* Rosalie Crutchley as Helen Dickerson
* Aubrey Morris as Doctor Putnam
* David Markham as Doctor Burgess
* Joan Young as Mrs. Caporal
* James Cossins as Older Male Nurse David Jackson as Young Male Nurse

== Critical reception ==

AllMovies review of the film was favourable, commending its "glamorous style" and "creepy atmosphere".  Blood from the Mummys Tomb currently holds an average three star rating (5.8/10) on IMDb.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 