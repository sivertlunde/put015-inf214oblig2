City of Beautiful Nonsense (1935 film)
{{Infobox film
| name           = City of Beautiful Nonsense
| image          =
| image_size     =
| caption        =
| director       = Adrian Brunel
| producer       = Wilfred Noy
| writer         = Donovan Pedelty E. Temple Thurston (novel)
| starring       = Emlyn Williams Sophie Stewart Eve Lister George Carney
| music          = Eric Spear
| cinematography = Desmond Dickinson
| editing        =
| studio         = Butchers Film Service
| distributor    = Butchers Film Service
| released       = May 1935
| runtime        = 88 minutes
| country        = United Kingdom
| language       = English
}} novel of silent by Henry Edwards in 1919.  The plot deals with a young woman who is in love with a penniless composer, but believes she must marry a wealthy man to please her father and only realises after various tribulations that she should follow her heart rather than her head.  

==Cast==
* Emlyn Williams as Jack Grey
* Sophie Stewart as Jill Dealtry
* Eve Lister as Amber
* George Carney as Chesterton
* Marie Wright as Dorothy Grey
* Eric Maturin as Robert Downing
* J. Fisher White as Thomas Grey
* Daisy Dormer as Mrs. Deakin
* Hubert Harben as Mr. Dealtry
* Margaret Damer as Mrs. Dealtry Dorothy Vernon as Mrs. Rowse

==References==
 
== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 

 