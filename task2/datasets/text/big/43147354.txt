The Master Strikes
 
 
{{Infobox film
| name = The Master Strikes
| film name =  
| director = Go Bo-Shu
| starring = Casanova Wong  Meng Yuen-Man Ching Siu-Tung Meg Lam Kin-Ming Yen Shi-Kwan
| released =  
| runtime = 85 minutes 
| distributor = Park Films Ltd
| language = Mandarin Chinese
}}
The Master Strikes (Also known as Fist of Tiger or Crazy Tiger Fist) is 1980 comedy martial art movie directed by Go Bo-Shu and starring Casanova Wong, Meng Yuen-Man and Ching Siu-Tung.

==Plot==

Chen, a bodyguard, is entrusted by Lung Tung Chien to protect a rare treasure. After having a hard time to protecting the treasure (sleeping on the top to make sure the treasure is safe),  he finds out it has been stolen. As a result, he becomes violently insane. The two con men Lung and Li find out about Chen and the treasures connection, and they decide to help Chen to find the treasure in an attempt to get rich.

==Cast==

*Casanova Wong as Chen
*Meng Yuen-Man as Li
*Ching Siu-Tung as Lung (action director)
*Yen Shi Kwan as Lung Tung Chien
*Meg Lam Kin-Ming 
*Max Lee Chiu Jun as Beggar Su
*Chap Lap-Ban as old prostitute 
*Hon Kwok Choi as waiter at the restaurant (cameo, uncredited)
*Wong Mei Mei
*Tony Leung Siu Hung as thug (assistant action director)
*Eddy Co Hung
*Fong Ping as prostitute 
*Mama Hung as prostitute
*Yuen Bo
*Lee Fat Yuen as thug (extra, uncredited)

==Reception==

Carl Davis of DVD Talk rated it 2.5/5 stars and compared it negatively to the contemporaneous films of Jackie Chan and Sammo Hung.   J. Doyle Wallis, also writing for DVD Talk, rated it 2.5/5 stars and called it "a bit of low budget, martial comedy silliness." 

==References==
 

==External links==

* 

 
 
 
 


 