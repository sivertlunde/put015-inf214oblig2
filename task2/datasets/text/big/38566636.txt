The Skyrocket
{{infobox film
| name           = The Skyrocket
| image          =
| caption        =
| director       = Marshall Neilan
| producer       = 
| writer         = Benjamin Glazer (adaptation and scenario)
| based on       =  
| starring       = Peggy Hopkins Joyce
| music          = 
| cinematography = David Kesson
| editing        =
| studio         = Celebrity Pictures
| distributor    = Associated Exhibitors
| released       = February 14, 1926
| runtime        = 80 min. (8 reels, 7350 ft.)
| country        = United States Silent English intertitles
}}
 silent romantic drama film directed by Marshall Neilan. 

==Production background==
The film was based on the 1925 novel of the same name by Adela Rogers St. Johns and scripted by Benjamin Glazer.  The movie stars Peggy Hopkins Joyce, a one-time Ziegfeld Follies showgirl who became a media figure in the late 1910s and early 1920s for dating, marrying and divorcing wealthy men, acquiring a sizable collection of expensive jewelery and furs and wearing fashionable clothes.   

The film was Joyces first full length feature and was intended as a vehicle to launch her acting career as she was largely known only for her colorful personal life. The films distributor, Associated Exhibitors, launched a massive publicity campaign to promote the film.  While Joyce earned mainly positive reviews for her performance, the film barely earned back its budget in box office returns.  
The film caused the Wisconsin state legislature to introduce a bill that would allow for the censorship of all movies entering the state.
 International House (1933), before fading into obscurity.  

==Cast==
* Gladys Brockwell - Rose Kimm (prologue) Charles West - Edward Kimm (prologue)
* Muriel McCormac - Sharon Kimm as a girl (prologue) Junior Coghlan - Mickey as a boy (prologue)
* Peggy Hopkins Joyce - Sharon Kimm
* Owen Moore - Mickey Reid
* Gladys Hulette - Lucia Morgan
* Paulette Duval - Mildred Rideout
* Lilyan Tashman - Ruby Wright
* Earle Williams - William Dvorak
* Bernard Randall - Sam Hertzfelt
* Sammy Cohen - Morris Pincus
* Bull Montana - Film Comedian
* Arnold Gray - Stanley Craig Ben Hall - Peter Stanton
* Nick Dandau - Vladmir Strogin
* Hank Mann - Comedy Producer
* Joan Standing - Sharons Secretary
* Eugenie Besserer - Wardrobe Mistress Edward Dillon - the Comedy Director
* Hank Mann - the Comedy Producer

==Preservation status==
No prints of The Skyrocket are known to exist and the film is now presumed a lost film. 

==References==
 

==See also==
*List of lost films

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 