The Disappearance
 
{{Infobox film
| name           =  The Disappearance
| image_size     = 
| image	=	The Disappearance FilmPoster.jpeg
| caption        =  
| director       =  Stuart Cooper
| producer       =  David Hemmings
| writer         =  novel Derek Marlowe screenplay Paul Mayersberg
| narrator       =   
| starring       =  Donald Sutherland Francine Racette David Hemmings
| music          =  Robert Farnon
| cinematography =  John Alcott
| editing        =  Eric Boyd-Perkins
| studio         =   
| distributor    =   
| released       =  December 1977 (U.K.)   10 May 1981 (Canada) 
| runtime        =  100 min Canada: 88 min
| country        =  Canada UK
| language       =  English
| budget         =  $1,900,000 CDN  
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Disappearance is a 1977 British-Canadian thriller film directed by Stuart Cooper and starring Donald Sutherland, Francine Racette and David Hemmings.  It is based on the novel Echoes of Celandine by Derek Marlowe.

==Plot==
Jay Mallory is a contract killer living in Montréal who works for an unknown international criminal organization. He returns home to his downtown apartment one cold winter day to find that his wife, Celandine, is gone without a trace. Mallory initially thinks that Celandine has left him on her own volition since their marriage was a sometimes stormy, albeit passionate, relationship. However, words from Mallorys main point of contact at the Organization, Burbank, indicate that Celandines disappearance may be associated with Mallorys last hit. Shortly after their discussion, Burbank himself disappears.

The Organization assigns Mallory another job in Suffolk, England. Mallory has a feeling that there is something unusual about this job - he is given little initial information including not knowing who the target is - and that it too is associated with Celandines disappearance. Despite feeling that he may be being set up, Mallory decides to take the job anyway to see how it plays out and if it leads him back to Celandine.

Mallory flies to London as instructed where he meets his new contact, Atkinson, who gives him the weapon to be used for the "shy" (a code-word the Organization uses to describe an assassination job) and the location and when it will take place. After renting a car and driving to rural Suffolk, Mallory begins to suspect the Organization plans to betray him since Burbank informed him earlier that the Organization often "retires" (kills) fellow members who are deemed no longer trustworthy or if they are felt to be no longer useful. After breaking into a large country house of his target, Mallory finds that his "shy" happens to be Deverell, the head of the Organization whom Mallory has never met. Deverell and his fellow aide Edward inform Mallory that they have been expecting him to show up and reveal that Celandine has been having an affair with Deverell behind Mallorys back the entire time and that she just left England the day before that Mellory arrived and that she may have orchestrated the entire "shy". When Deverell attempts to kill Mallory for knowing too much, Mallory suceedes in killing him and Edward instead and flee to London where he takes the first flight back to Canada.

After arriving back at his apartment in Montréal, Mallory finds Celandine back where after confronting her, she admits to him that she indeed was involved with Deverell out of frustration to their failing marriage and she also, through the Organizations various channels and middle men, planned for Mallory to assassinate Deverell so she could be free from him and that Mallory could be free from the Organization. Mallory seems to accept this and he and Celandine make love.

The next morning, Mallory wakes up confident and decides to cook Celandine breakfast, but after seeing that he does not have enough food in his refrigerator he leaves the apartment alone to go food shopping. A little later, as Mallory returns to his apartment, carrying a large brown paper bag filled with groceries, he stumbles along the balcony outside his apartment to look for his apartment keys when an unseen sniper shoots at him but misses. When a surprised Mallory looks around trying to locate the sniper, the unseen marksman opens fire again and hits Mallory again in his chest with the second bullet, killing him instantly. In the final images, as Mallory lies dead outside the door to his apartment, Celandine sits alone inside the apartment with a calm look on her face, and ending the film with many more questions then answers (i.e.; Who killed Mallory?; The new leaders of the Organization? Deverells family? An unknown third party? Did Celandine have anything to do with Mallorys murder?, etc.).

==Cast==
* Donald Sutherland - Jay Mallory 
* Francine Racette - Celandine 
* David Hemmings - Edward 
* John Hurt - Atkinson  David Warner - Burbank 
* Peter Bowles - Jefferies 
* Virginia McKenna - Catherine 
* Christopher Plummer - Deverell 
* Michèle Magny - Melanie 
* Dan Howard - James
* Robin Sachs - Young Man 
* Christina Greatrex - Secretary 
* Robert Korne - Dominic 
* Michael Eric Kramer - Peter
* Maureen Beck - Maid

==Production, release and reception==

Shot in 1977, this was film producer Garth Drabinskys first production. According to film critic Jay Scott, Eric Boyd-Perkins originally edited the film; but the version released in Canadian cinemas in 1983 had been re-cut by "film doctor" Fima Noveck who had "saved any number of other ailing" movies.   

Jay Scott dismissed the film as "irredeemably nasty, supremely glossy trash." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 