The Quiet Woman
{{Infobox film
| name           = The Quiet Woman
| image          = "The_Quiet_Woman"_(1951).jpg
| director       = John Gilling
| producer       = Robert S. Baker Monty Berman
| writer         = Ruth Adam   John Gilling
| starring       = Derek Bond   Jane Hylton   Campbell Singer   Dora Bryan
| music          = John Lanchbery
| cinematography = Monty Berman
| editing        =  Jack Slade
| distributor    = Tempean Films
| released       = March 1951
| runtime        = 71 min
| country        = United Kingdom
| language       = English
}} British crime bar known as The Quiet Woman. She becomes outraged when she discovers the previous owner had allowed local smugglers to use it as a base. She soon has become romantically involved with one of the smugglers, which causes enormous problems when a customs officer turns up, followed closely by her former husband.  It was based on a story by Ruth Adam.

==Cast==
*Duncan -	Derek Bond
*Jane -	Jane Hylton
*Elsie -	Dora Bryan Michael Balfour
*Helen -	Dianne Foster
*Cranshaw -	Harry Towb John Horsley

==References==
 

== External links ==
*  

 

 
 
 
 
 
 


 
 