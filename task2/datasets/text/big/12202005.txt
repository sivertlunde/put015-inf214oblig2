Beyond Evil
{{Infobox film
  | name = Beyond Evil
  | image = TromaEvil.jpg
  | caption = Poster for Beyond Evil
  | director = Herb Freed
  | producer = Rooven Akiba David Baughn Herb Freed Jules Winter
  | writer = David Baughn Herb Freed Paul Ross John Saxon Lynda Day George Michael Dante Janice Lynde David Opatoshu
  | music = Pino Donaggio
  | cinematography = Ken Plotin
  | editing = Rick Westover
  | distributor = Troma Entertainment
  | released = 1980
  | runtime = 94 minutes
  | language = English
  | budget =
  | preceded by =
  | followed by =
  }}
 John Saxon.

==Plot==
When an architect and his bride move into their dream home—a colonial mansion—they find that their new digs have a rich history of bloody violence. Whats more, the manor is already occupied by a 100 year-old ghost with a thirst for vengeance and a yearning to oust the newlyweds from his haunt.

==Release==
The film is currently distributed on DVD by Troma Entertainment. 

==References==
 

==External links==
* 
*  – at the Troma Entertainment movie database

 
 
 
 
 

 