See You in Montevideo
 
{{Infobox film
| name           = See You in Montevideo
| image          = See You in Montevideo.jpg
| alt            = 
| caption        = Film poster
| director       = Dragan Bjelogrlić
| producer       = Dejan Petrovic
| writer         = Dragan Bjelogrlić
| starring       = {{Plainlist|
* Milos Bikovic
* Petar Strugar
* Predrag Vasic}}
| music          = Roberto Magnifico
| cinematography = Goran Volarevic
| editing        = {{Plainlist|
* Petar Markovic
* Dejan Urosevic
* Svetolik Zajc}}
| distributor    = Intermedia Network
| released       =  
| runtime        = 142 minutes
| country        = Serbia
| language       = {{Plainlist|
* Serbian
* English
* French
* Spanish}}
| budget         = $5.4 million
}} Best Foreign Language Film at the 87th Academy Awards, but was not nominated.      

==Cast== Aleksandar "Tirke" Tirnanić Blagoje "Moša" Marjanović Milutin "Milutinac" Ivković
* Armand Assante as Hotchkins
* Elena Martínez as Dolores
* Branko Đurić as Paco
* Predrag Vasić as Little Stanoje
* Nebojsa Ilic as Boško Simonović|Boško "Dunster" Simonović
* Vojin Ćetković as Mihailo Andrejević
* Srdjan Timarov as Kosta Hadži

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Serbian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 
 