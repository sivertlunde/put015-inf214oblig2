The Bloody Hands of the Law
{{Infobox film
| name           = The Bloody Hands of the Law
| image	=	The Bloody Hands of the Law FilmPoster.jpeg
| caption        = Film poster
| director       = Mario Gariazzo
| producer       = Giuseppe Rispoli
| writer         = Mario Gariazzo
| starring       = Klaus Kinski
| music          = Stelvio Cipriani
| cinematography = Enrico Cortese
| editing        = Alberto Gallitti
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Bloody Hands of the Law ( ) is a 1973 Italian crime film directed by Mario Gariazzo and starring Klaus Kinski.   

==Cast== Philippe Leroy - Commissario Gianni De Carmine
* Tony Norton - DAmico
* Silvia Monti - Linda De Carmine
* Klaus Kinski - Vito Quattroni
* Fausto Tozzi - Nicolò Patrovita
* Pia Giancaro - Lilly Antonelli
* Cyril Cusack - The Judge
* Guido Alberti - Prof. Palmieri
* Lincoln Tate - Joe Gambino
* Marino Masé - Giuseppe di Leo
* Luciano Rossi
* Sergio Fantoni - Musante
* Rosario Borelli - Salvatore Perrone
* Tom Felleghy
* Valentino Macchi
* Lorenzo Fineschi
* Denise OHara - Elsa Lutzer
* Lorenzo Magnolia
* Stelio Candelli
* Giuseppe Mattei
* Giulio Baraghini
* Cosimo Cinieri
* Ignazio Bevilacqua
* Cesare Di Vito
* Fausto Di Bella
* Lino Murolo
* Attilio Severini
* Sergio Serafini

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 