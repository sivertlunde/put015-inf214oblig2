Quiet Chaos (film)
{{Infobox Film
| name           = Quiet Chaos
| image          = Caos calmo poster.jpg
| image_size     = 
| caption        = 
| director       = Antonello Grimaldi
| producer       =  Caos calmo by Sandro Veronesi
| starring       = Nanni Moretti Valeria Golino Alessandro Gassman
| music          = Paolo Buonvino Ivano Fossati
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}} novel of the same name by Sandro Veronesi.

== Plot ==
Pietro Paladini and his brother Carlo, a fashion designer, rescue two women from drowning. At the same time Pietros wife dies unexpectedly at home. After the funeral, Pietro falls into a state of Quiet Chaos, which is marked by spending a lot of time with his daughter Claudia. The manager is absent from his work and spends his days waiting in the park, which is opposite the school of his daughter. All the while, the widower stays very calm on the outside and is a focal point for his wifes sister Marta (Valeria Golino), his brother and co-workers who are affected by the merger of his group. It is the rebirth of a man who was once a tough manager.

== Cast ==
 
* Nanni Moretti: Pietro Paladini
* Valeria Golino: Marta Siciliano
* Isabella Ferrari: Eleonora Simoncini
* Hippolyte Girardot: Jean Claude
* Alessandro Gassman: Carlo Paladini
* Silvio Orlando: Samuele
* Blu Yoshimi: Claudia Paladini
* Antonella Attili: maestra Gloria
* Manuela Morabito: Maria Grazia
* Beatrice Bruschi: Benedetta
* Alba Rohrwacher: Annalisa
* Kasia Smutniak: Jolanda
* Roberto Nobile: Taramanni
* Sara DAmario: Francesca
* Charles Berling: Boesson
* Roman Polanski: Steiner
* Denis Podalydes: Thierry
* Babak Karimi: Mario
* Stefano Guglielmi: Matteo
* Tatiana Lepore: Mamma Matteo
* Cloris Brosca: Psicoterapeuta
* Valentina Gaia: Ragazza Conferenza
* Valentina Carnelutti: Mamma 1
* Anna Gigante: Mamma 2
* Nestor Saied: Marito Simoncini Roberta Bruni: Insegnante Gimnastica
* Ugo De Cesare: Ragazzo Cena Gala
* Dina Braschi: Dona Anziana Cena Gala
* Claudia Alfonso: Ragazza Cena Gala
* Cinzia Bernardini: Altra Donna Cena Gala
* Lamberto Antinori: Uomo Cena Gala
* Ester Cavallari: Lara
* Il Cane Riccardo: Nebbia
* La Scimmia Di Peluche: Pupa
 

== External links ==
*  

 
 
 
 
 

 