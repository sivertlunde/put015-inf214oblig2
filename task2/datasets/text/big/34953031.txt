Hasaki Ya Suda
 
{{Infobox film
| name           = Hasaki Ya Suda
| image          = 
| alt            =  
| caption        = 
| director       = Cédric Ido
| producer       = DACP Films I Do Films 
| writer         = 
| screenplay     = Cédric Ido 
| story          = 
| starring       = Kylian Amable Jacky Ido Cédric Ido Minman Ma Ralph Amoussou
| music          = Nicola Tescari David Chalmin 
| cinematography = Thomas Garret 
| editing        = Samuel Danesi 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 24 minutes
| country        = Burkina Faso / France
| language       = Lingala 
| budget         = 
| gross          = 
}}

Hasaki Ya Suda  is a 2011 film.

== Synopsis ==
The year 2100. The global warming has caused massive droughts that have led to conflicts and famines. The first victims of the global warming are the Southern populations, forced to leave their lands to immigrate to the North. A massive exodus that makes chaos out of the known world order. Now, the earth is reduced to one giant no man’s land. Lost and defenseless, the survivors have no choice but to return to ancestral rites. All over the world, clans form and fight for the last natural resources and fertile lands.

== Awards ==
* Festival International du Cinéma et de lAudiovisuel du Burundi - Festicab 2011 
* Prix qualité CNC 2012

== References ==
 
 
Fespaco 2011

== External links ==
*  

 
 
 
 
 