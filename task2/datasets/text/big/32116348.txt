Lion, London Zoological Gardens
{{Infobox film
| name           = Lion, London Zoological Gardens
| image          = LionLondonZoologicalGardens.jpg
| image_size     = 
| caption        = Screenshot from the film
| director       = Alexandre Promio
| producer       = Auguste and Louis Lumière
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        =
| country        = France
| language       = Silent film
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
 1896 France|French short black-and-white silent actuality film, produced by Auguste and Louis Lumière and directed by Alexandre Promio, featuring a male lion reaching through the bars of its enclosure at London Zoological Gardens to get at the meat thrown by its keeper. The film was part of a series, including Tigers, London Zoological Gardens|Tigers and Pelicans, London Zoological Gardens|Pelicans, which were one of the earliest examples of animal life on film. 

==Current status==
Given its age, this short film is available to freely download from the Internet.

==References==
 

== External links ==
*  

 
 
 
 

 