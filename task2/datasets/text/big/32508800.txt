The Dressmaker (1988 film)
{{Infobox film
| name           = The Dressmaker
| image          = The Dressmaker.jpg
| caption        =  Jim OBrien John McGrath Ronald Shedlo John McGrath The Dressmaker&nbsp;by  
| starring       = Joan Plowright Billie Whitelaw Jane Horrocks
| music          = George Fenton
| cinematography = Michael Coulter
| editing        = William Diver
| studio         = British Screen Productions Channel Four Films
| distributor    = Euro American Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States United Kingdom
| language       = English
}} Jim OBrien The Dressmaker by Beryl Bainbridge.

==Cast==
* Joan Plowright - Nellie 
* Billie Whitelaw - Margo 
* Pete Postlethwaite - Jack 
* Jane Horrocks - Rita 
* Tim Ransom - Wesley 
* Pippa Hinchley - Val 
* Rosemary Martin - Mrs. Manders 
* Tony Haygarth - Mr. Manders 
* Michael James Reed - Chuck  
* Sam Douglas - Corporal Zawadski 
* Bert Parnaby - Mr. Barnes 
* Lorraine Ashbourne - Factory Girl 
* Mandy Walsh - Factory Girl 
* Margi Clarke - Shopwoman 
* Andrew Moorcroft - Butchers Boy 
* Marie Jelliman - Mrs. OToole

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 