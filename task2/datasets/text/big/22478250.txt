The Death of a Lumberjack
{{Infobox film
| name           = The Death of a Lumberjack
| image          = The Death of a Lumberjack Poster.jpg
| caption        = 
| director       = Gilles Carle
| producer       = Pierre David Pierre Lamy
| writer         = Gilles Carle Arthur Lamothe
| narrator       = 
| starring       = Carole Laure
| music          = 
| cinematography = René Verzier
| editing        = 
| distributor    = 
| released       = 25 January 1973
| runtime        = 115 minutes
| country        = Canada
| language       = French
| budget         = 
}}

The Death of a Lumberjack ( ) is a 1973 Canadian drama film directed by Gilles Carle. It was entered into the 1973 Cannes Film Festival.   

==Cast==
* Carole Laure - Marie Chapdeleine
* Willie Lamothe - Armand St. Amour
* Daniel Pilon - François Paradis
* Pauline Julien - Charlotte Juillet
* Marcel Sabourin - Ti-Noir LEsperance
* J. Léo Gagnon
* Roger Lebel
* Ernest Guimond
* Jacques Gagnon
* Gil Laroche
* Jacques Bouchard
* Yvonne Diabo
* Eugene Lahache
* Marcel Fournier
* Denise Filiatrault - Blanche Bellefeuille

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 