Men of the Sea
{{Infobox film
| name           = Men of the Sea
| image          =
| image_size     =
| caption        =
| director       = Aleksandr Faintsimmer
| producer       =
| writer         = Frederick Marryat (novel Midshipman Easy) Aleksandr Shtein (writer) Aleksandr Zenovin (writer)
| starring       = See below
| music          = V. Sherbachev
| cinematography = Svyatoslav Belyayev
| editing        =
| distributor    =
| released       = 2 January 1938
| runtime        = 83 minutes
| country        = Soviet Union
| language       = Russian
| budget         =
| gross          =
}}

Men of the Sea (Baltiytsy) is a 1938 Soviet film directed by Aleksandr Faintsimmer.

== Plot summary ==
 

=== Differences from novel ===
 

== Cast ==
*P. Gofman as Pilot Bezenchuk
*Galina Inyutina as Glafira
*Pyotr Kirillov as Dietrich
*Vladimir Kryuger as Gunner Zheslov
*Boris Livanov as Commissar Vikhoriev
*K. Matrossov as Lavretski
*V. Safranov as Vaviloo
*Leonid Smit as Signal Man Kolessov
*Konstantin Sorokin as Orderly
*Vladimir Uralsky as Machinist Khoritonich
*L. Viven as Commander Rostovtsev

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 
 