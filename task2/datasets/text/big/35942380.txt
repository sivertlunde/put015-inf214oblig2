Mystery (2012 film)
 
{{Infobox film
| name           = Mystery
| image          = 
| caption        = 
| director       = Lou Ye Kristina Larsen
| writer         = 
| starring       = Hao Lei Qin Hao
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = China
| language       = Mandarin
| budget         = 
}}

Mystery (浮城謎事) is a 2012 Chinese drama film directed by Lou Ye. This is Lou Yes seventh film but only the second (with Purple Butterfly in 2003) to have been released in his own country.  The story is based on a series of posts under the title of "This Is How I Punish A Cheating Man And His Mistress" (《看我如何收拾贱男与小三》), which has over one million hits. "Mystery is beautiful and violent, both in the emotions it deals with and the scenes that display them. It echoes some of contemporary Chinas own problems, such as corruption, money, ambiguity and morality," says Brice Pedroletti in his review on The Guardian 

The film competed in the Un Certain Regard section at the 2012 Cannes Film Festival.       At the 7th Asian Film Awards the film won the Asian Film Award for Best Film.   

==Plot==
Lu Jie has no idea her husband Yongzhao is leading a double life, until the day she sees him entering a hotel with a young woman. Her world crumbles – and it’s just the beginning.

==Cast==
* Hao Lei as Lu Jie - Qiao Yongzhaos wife, and they have one daughter.
* Qin Hao as Qiao Yongzhao - The male protagonist. Qi Xi as Sang Qi/Sang Hailan - Qiao Yongzhaos mistress, and they have a son.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 


 
 