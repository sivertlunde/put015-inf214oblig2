Eleftherios Venizelos (film)
{{Infobox film
| name           = Eleftherios Venizelos Ελευθέριος Βενιζέλος 
| image          = 
| image size     =
| caption        =
| director       = Pantelis Voulgaris
| producer       = Giorgos Iakovidis Michalis Lambrinos
| writer         = Pantelis Voulgaris
| starring       = Minas Christidis Giannis Voldis Dimitris Murat Manos Katrakis Olga Karlatou Anna Kalouta
| cinematography =
| photographer   =
| music          = Loukianos Kilandonis
| editing        =
| distributor    = Mer Film
| released       = 1980
| runtime        = 175 minutes
| country        = Greece
| language       = Greek
| imdb           = 164021
| cine.gr_id     = 600610
}}
 Greek biographical film of one of the most famous leaders of the Greek political scene of the 20th century Eleftherios Venizelos.  The movie stars Dimitris Murat, Manos Katrakis and Anna Kalouta.

==Plot==
The story was set in August 1909 when the movie presented in sound and Venizelos ideas and Venizelianism for a larger Greece.  It describes the capture of Thessaloniki and Ioannina and later the National Schism and up to the impending war in Europe, World War I.  Venizelos resigned from the political life after the tie in the elections that followed the Asia Minor Catastrophe and returned to his relatives in Crete.

==Cast==
*Minas Christidis
*Giannis Voglis
*Dimitris Murat
*Manos Katrakis
*Olga Karlatou
*Anna Kalouta

==External links==
* 
*   

 
 
 
 
 
 
 

 
 