Don't Go in the House
 
{{Infobox film
| name           = Dont Go in the House
| image          = Dont Go in the House FilmPoster.jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = 
| director       = Joseph Ellison
| producer       = Ellen Hammill
| writer         = 
| screenplay     = {{plainlist|
* Ellen Hammill
* Joe Masefield
* Joseph Ellison
}}
| story          = Joe Masefield
| based on       = 
| narrator       = 
| starring       = Dan Grimaldi
| music          = Richard Einhorn Oliver Wood
| editing        = Jane Kurson
| studio         = Turbine Films Inc.
| distributor    = Film Ventures International
| released       =   
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $250,000
| gross          = 
}}
Dont Go in the House is a 1980 horror film written and directed by Joseph Ellison, and co-written by Ellen Hammill and Joe Masefield. It gained notoriety as a video nasty and remains banned in some countries.

== Plot ==
 abuse he suffered at the hands of his mother, who would hold his bare arms over a gas stove in an effort to "burn the evil out of him". When his mother dies, he sets out to avenge himself on every woman who bears a resemblance to her with the aid of steel chains, a flamethrower, and a steel-paneled bedroom crematorium.

Donnys first victim is florist Kathy Jordan (Johanna Brushay). Befriending the harmless-looking man, Kathy escorts Donny back to his mothers house, where he knocks her unconscious, strips her naked, and chains her arms and legs to the ceiling and floor of the steel room.  Ignoring Kathys screaming pleas for mercy, he burns her to death with his flamethrower. Over the next few days, Donny murders two other women by immolating them. Donny also burns his mothers corpse, and dresses it up in her bedroom along with the other three dead women.

During his killing spree, Donny hears voices in his mind which call him "the master of the flame" and urge him to punish "evil". Donnys only friend is Bobby Tuttle (Robert Osth) a co-worker who phones the house one day asking why Donny has been absent from work for nearly a week. Donny lies and claims that his mother is sick and needs attention. When Donny attempts to pick up another victim, he cannot go through with it, and begins to feel remorse for his actions. Donny goes to a church, where he tells Father Gerritty (Ralph D. Bowman) about the abuse his mother inflicted upon him, and about his urges to kill. Father Gerritty persuades Donny to try to move on with his life, and put the past to rest.

In an attempt to stop killing, Donny decides to accept an invitation by Bobby to accompany him on a double-date to a disco club, despite Donnys lack of social skills. After traveling to a mens clothing store and purchasing a new outfit, Donny shows up at the disco as expected, but is shy and awkward with his date. When the woman attempts to take Donny to the dance floor and inadvertently holds his arm over a tables lighted candle, the memories of the childhood abuse come back, and Donny sets her hair on fire.

Fleeing from the disco, Donny runs into two drunken girls on the street, and he invites them back to his house. Bobby attempts to find Donny, and runs into Father Gerritty on his way to Donnys house. When no one answers the front door, Bobby and Father Gerrity break it down and rescue the two women. Donny dons his incinerator outfit and sets Father Gerrity ablaze with his flamethrower, but Bobby manages to smother the flames and rescue the priest.

Donny takes refuge inside his mothers bedroom with the burned corpses. The voices in his head express their disappointment with him. Donny hallucinates  the charred corpses coming to life. He tries to fight them with the flamethrower but sets the house on fire. He dies in the fire.

The final scene portrays a young boy being beaten by his mother. The boy hears the same whispering voices that Donny did, and they tell him that they are here to "help" him.

== Cast==

* Dan Grimaldi as Donald Donny Kohler
* Colin Mclnness as Young Donald Kohler
* Charlie Bonet as Ben
* Bill Ricci as Vito
* Robert Osth as Bobby Tuttle
* Ruth Dardick as Mrs. Kohler
* Ralph D. Bowman as Father Gerritty

== DVD release == region 1 DVD presented a remastered widescreen print of the film with an audio commentary from star Dan Grimaldi, as well as a filmed interview with Grimaldi, theatrical trailers, TV spots, and short "unmatted" extracts from the full-screen VHS version.

The film was also released uncut on DVD in the UK for the first time in 2012 by Arrow Films. This release was a remastered print with a reversible cover and special collectors booklet.

== Reception ==
 nihilistic much cheese that was just round the corner), but Dont Go in the House is a chilly reminder of times when practically anything went".  

DVD Verdict also had a positive response, writing "Dont Go in the House is a well-acted, disturbing film" and "this movie is a mental and visual screw job. Writer/director Joseph Ellison has put together a brutal little psycho flick—part excursion into the tortured mind of an abuse victim, and part shock horror".  Retro Slashers found Dont Go in the House a "very dull" piece that was brought down by bad acting, and a lack of suspense and originality.  

=== Controversy ===
 
Despite some respectable critical notices, Dont Go in the House attracted controversy almost immediately because of its graphic depiction of the death of Kohlers first victim, and the central theme of childhood abuse.  The film was cut by almost three minutes when it was released in Britain in the winter of 1980, but an uncut version was released on video by the Arcade label in 1982 – knowingly or not, they advertised the release as "a true nasty from Arcade", and it quickly wound up on the Director of Public Prosecutions (England and Wales)|DPPs list of banned titles.  The pre-cut British cinema version was released on video by the Apex label in April 1987, though the film was finally passed uncut in 2011. 

== References ==

 

== External links ==

*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 