Zindaggi Rocks
{{Infobox film
| name           = Zindaggi Rocks
| image          = Zindaggirocks.jpg
| caption        = Zindaggi Rocks Poster, showing Sushmita Sen
| imdb_id        =
| writer         = Mudassar Aziz
| starring       = Sushmita Sen Shiney Ahuja
| director       = Tanuja Chandra
| producer       = Anurradha Prasad
| distributor    = B.A.G. Film Ltd.
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| music          = Anu Malik
| awards         =
| budget         =
| gross          = Rs. 4.20 crores
}}

Zindaggi Rocks (translates as Life Rocks) is a 2006 Bollywood film, directed by Tanuja Chandra and produced by Anurradha Prasad. It is about the love story between a famous singer, Kriya, played by Sushmita Sen and a doctor, played by Shiney Ahuja.

==Summary==
Dr. Suraj Rihan (Shiney Ahuja) meets Kriya (Sushmita Sen), a popular singer in his hospital. While he is a quiet, responsible man, she is loud, kiddish and a little crazy. Kriya invites Suraj to meet her family, which includes her mother, who is very strict and her mothers twin sister, who is fun and giggly. Kriya has an adopted son, who is very mature for his age.

Kriya and Suraj fall in love with each other and their new feelings are tested by a crisis that enters both their lives. Kriyas son is in a critical state as he has a hole in his heart and they are unable to find a donor till the date of the operation. This makes Kriya commit a medicine induced suicide and her heart is then used to save her son.

==Narration==
This story is narrated by Dr. Suraj after 2 years of Kriyas death. He is in the interrogation room of a police station. He is on the verge of being arrested for performing the surgery after Kriyas suicide.

==Cast==
{| class="wikitable"
|- bgcolor="#d1e4fd"
! Actor/Actress !! Role !! Other notes
|- Sushmita Sen || Kriya ||
|- Julian Burkhardt || Dhruv ||
|- Shiney Ahuja || Dr. Suraj Rihan ||
|- Moushumi Chatterjee || Kriyas mother / Mausi || Double role
|- Kim Sharma || Joy ||
|- Ravi Gosain || Sam ||
|- Seema Biswas || DCP Gazala Qadri ||
|}

==Soundtrack==
{{Infobox album
| Name = Zindaggi Rocks
| Type = Soundtrack
| Artist = Anu Malik
| Cover =
| Released = 2006
| Recorded = Feature film soundtrack
| Length =
| Label =
| Producer =
| Reviews = Umrao Jaan (2006)
| This album = Zindaggi Rocks (2006)
| Next album = Humko Deewana Kar Gaye (2006)
}}

The album features 6 original songs and 4 remixed. All tracks composed by Anu Malik and the lyrics penned by Sayeed Quadri and Mudassar Aziz.

===Track lists===

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s)  !! Lyrics !! Length 
|-
| 1 || "Meri Dhoop Hai Tu" || Javed Ali, Tulsi Kumar || Sayeed Quadri || 4:37
|-
| 2 || "Rabbi" || Zubeen Garg, Sunidhi Chauhan, Krishna || Mudassar Aziz || 5:42
|-
| 3 || "Zindagi Rocks" || Anushka Manchanda || Mudassar Aziz || 5:32
|-
| 4 || "Ek Din Fursat" || Sunidhi Chauhan || Mudassar Aziz || 5:58
|-
| 5 || "Humko Chhoone Paas Aaiye" || Sunidhi Chauhan || Sayeed Quadri || 6:58
|-
| 6 || "Hadh Ko Aadab Ko" || Sunidhi Chauhan || Sayeed Quadri || 5:38
|-
| 7 || "Meri Dhoop Hai Tu (Remix)" || Javed Ali, Tulsi Kumar || Sayeed Quadri || 4:21
|-
| 8 || "Rabbi (Remix)" || Zubeen Garg, Sunidhi Chauhan, Krishna || Mudassar Aziz || 4:59
|-
| 9 || "Ek Din Fursat (Remix)" || Sunidhi Chauhan || Mudassar Aziz || 5:21
|-
| 10 || "Hadh Ko Aadab Ko (Remix)" || Sunidhi Chauhan || Sayeed Quadri || 4:10
|}

==Trivia==
* Was previously titled Rockin.
* UK full title - Zindaggi Rocks - Celebration of life.

==External links==
*  
 

 
 
 