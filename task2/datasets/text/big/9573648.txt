Machakaaran
{{Infobox film
| name = Machakaaran
| image =
| director = Tamilvannan
| producer = Madras Entertainment
| writer = Tamilvannan Malavika M. S. Baskar
| music = Yuvan Shankar Raja
| cinematography = A. Venkatesh (cinematographer)|A. Venkatesh
| editing = Rajamohamed
| studio = Madras Entertainment
| released =  
| language = Tamil
| country = India
}} Indian Tamil Tamil film Kalvanin Kadhali Malavika appears Telugu later and released as Dheera in 2009.

==Plot==
Vicky (Jeevan) is the eldest son of a Railway officer, is a perpetual loser in whatever he does and is looked down by other family members. One day he meets Shivani (Kamna Jethmalani), daughter of a rich textile tycoon, who has brought all the luck to her father, with her Midas touch!

Soon the ‘unlucky’ Vicky is drawn towards the ‘lucky’ Shivani and they fall madly in love! Due to circumstances, they are forced to elope, with Shivani’s cop brother in hot pursuit. The lovers decide to go to Shivani’s father’s village in Theni, where he is considered and treated as “Mr Nice Guy”, a philanthropist and savior of the locals, and is given the honour of conducting the temple Thiruvizha, every year. How the runaway couple against all odds rips his good guy image and exposes him in front of the entire village, forms the rest of this predictable yarn.

==Cast==
{| class="wikitable"
|- bgcolor="#123abc"
! Actor !! Role
|-
| Jeevan || Vicky
|-
| Kamna Jethmalani || Shivani Rajangam
|- Santhanam ||
|- Malavika || Item number
|-
| Vinod Rajan ||
|-
| G. M. Kumar || Rajangam
|-
| Shobraj ||
|-
| M. S. Baskar ||
|-
| Santhana Bharathi ||
|-
| Kalairani ||
|-
| Vaiyapuri ||
|-
| Mayilsamy ||
|}

==Soundtrack==
{{Infobox album
| Name = Machakaaran
| Type = soundtrack 
| Artist = Yuvan Shankar Raja
| Cover = Machakaaran small.jpg
| Released =  
| Recorded = 2007
| Genre = Film soundtrack
| Length = 
| Label = Big Music
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Vel (film)|Vel (2005)
| This album  = Machakaaran (2005) 
| Next album  = Billa (2007 film)|Billa (2005)
}}
 Kalvanin Kadhali (2005). The soundtrack, which features 5 tracks overall, was released on 24 October 2007 and garnered generally positive reviews.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Lyricist!! Notes 
|-  1 || "Nellayila Mannedutha" ||   ||
|- 2 || "Vaanathayum Megathayum" || Haricharan || 4:49 || Pa. Vijay ||
|- 3 || "Vayasu Ponnukku" || Mahalakshmi Iyer || 3:47 || Pa. Vijay ||
|- 4 || "Jigiruthaanda" || Shankar Mahadevan, Snekha Bandh || 4:21 || Pa. Vijay ||
|- 5 || "Nee Nee Nee" || Hariharan (singer)|Hariharan, Madhushree || 4:33 || Devandharan ||
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 