Uzhaikkum Karangal
{{Infobox film
| name           = UZHAIKKUM KARANGAL
| image          = Uzhaikkum Karangal.png
| image_size     =
| caption        =
| director       = K. Shankar
| producer       = Kovai Chezhian
| writer         = Nansil Ki.Manogaran
| narrator       = Latha
| music          = M. S. Viswanathan
| cinematography = P.L.Rav
| editing        = K.Shankar-E.A.Dhandhabany
| studio         = 
| released       = 23 May 1976 
| runtime        =
| country        = India Tamil
| budget         =
| gross          =
| website        =
}}
 Indian Tamil Tamil film Latha and Pandari Bai among others.

==Plot==

Yesterday as gentle as a lamb, today, like a lion, the brave Rangan (MGR) provides with its ferula (stick), makes the justice reign in the parts of the country of its (countryside).

From now on, nasty well have only to be held !

==Cast==
*M. G. Ramachandran as Rangan Latha as Muthamma, Rangan s wife
*Bhavani (introducing) as Kumari Punkajam, the sacred dancer
*Kumari Padmini as Gowri, the young widow
*Pandari Bai as Annam, Nagalingham s wife
*C. K. Saraswathi as Akkilandham, the strong woman of character
*Baby Rajkumari as Ramu, Nagalingham s son
*K. A. Thangavelu as Nagalingham,the chairman
*Thengai Srinivasan as Kabali
*Nagesh as Saba / Kitha
*Kannan as Karnaya
*V. S. Raghavan as Sendhil Nadhan
*V. Gopalakrishnan as Thanga Durai

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

In the movie, on the knob of the stick of MGR, we can observe the symbol of two-leaves of his party, All India Anna Dravida Munnetra Kazhagam.

Uzhaikkum Karangal was the 3rd and final movie where Nagesh acted in an MGRs movie without any contact with MGR. Because in 1974 when Nagesh in an interview to a famous magazine stated that MGR was a womanizer and very weak around women MGR stop casting Nagesh in his movies. But only in 3 movies MGR let him act with the condition that he shouldnt have any contact or any scene with him. MGR let Nagesh act in the last 3 movies because of Nageshs financial problems during at that time.

==Songs==
{{tracklist	
| headline     = Tracklist
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 
| title1       = Intha Nattu nadappu
| extra1       = 
| lyrics1      =
| length1      = 
| title2       = Naalai uzhagai
| extra2       = K. J. Yesudas, M. S. Viswanathan
| lyrics2      = Pulamaipithan
| length2      = 4.29
| title3       = Aadiya paadhangal
| extra3       = P. Susheela
| lyrics3      = Pulamaipithan
| length3      = 
| title4       =
| extra4       =
| lyrics4      = 
| length4      = 
| title5       = 
| extra5       =
| lyrics5      = 
| length5      = 
| title6       = 
| extra6       =
| lyrics6      = 
| length6      =
| title7       =
| extra7       =
| lyrics7      =
| length7      =
| title8       =
| extra8       =
| lyrics8      =
| length8      = 
}}

==References==
 

==External links==
 

 
 
 
 
 
 


 