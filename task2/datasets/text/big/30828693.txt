Rent Free
{{infobox film
| name           = Rent Free
| image          = Rent Free Poster.jpg
| imagesize      = 190px
| caption        = Theatrical poster
| director       = Howard Higgin
| producer       = Adolph Zukor Jesse Lasky Thomas Buchanan
| writer         = Izola Forrester (story) Mann Page (story) Elmer Rice (scenario)
| starring       = Wallace Reid Lila Lee
| music          = Charles E. Schoenbaum
| editing        =
| distributor    = Paramount Pictures
| released       = January 1, 1922
| runtime        = 5 reels; (4,661 feet)
| country        = United States Silent (English intertitles)

}} 1922 silent silent film feature comedy produced by Famous Players-Lasky and distributed through Paramount Pictures. The film starred Wallace Reid and his current regular co-star Lila Lee. It was directed by Howard Higgin and adapted by Elmer Rice from a story written directly for the screen by Izola Forrester(the granddaughter of John Wilkes Booth) and Mann Page. Currently this film is lost film|lost.   

Reid plays the part of a young painter who moves into an abandoned house. In real life he was a talented painter.

Higgins first film as a director.

==Cast==
*Wallace Reid - Buell Arnister Jr.
*Lila Lee - Barbara Teller
*Henry A. Barrows - Buell Arnister Sr.
*Gertrude Short - Justine Tate
*Lucien Littlefield - Batty Briggs
*Lillian Leighton - Maria Tebbs
*Claire McDowell - Countess de Mourney
*Clarence Geldart -Count de Mourney

==References==
 

==External links==
* 
* 
* (moviegoods)

 
 
 
 
 
 
 
 


 