The Girl Irene
{{Infobox film
| name =  The Girl Irene
| image =
| image_size =
| caption =
| director = Reinhold Schünzel
| producer =  Erich von Neusser  Philip Stuart (play)   Eva Leidmann   Reinhold Schünzel  
| narrator =
| starring = Lil Dagover   Sabine Peters   Geraldine Katt   Hedwig Bleibtreu
| music = Alois Melichar   
| cinematography = Robert Baberske   
| editing =  Arnfried Heyne       UFA
| distributor = UFA
| released =  9 October 1936 
| runtime = 95 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Girl Irene (German:Das Mädchen Irene) is a 1936 German drama film directed by Reinhold Schünzel and starring Lil Dagover, Sabine Peters and Geraldine Katt.  The widowed mother of a middle class family falls in love, provoking the jealousy of her daughter.

==Cast==
* Lil Dagover as Jennifer Lawrence 
* Sabine Peters as Irene Lawrence, ihre Tochter  
* Geraldine Katt as Baba Lawrence, ihre Tochter  
* Hedwig Bleibtreu as Großmutter  
* Elsa Wagner as Frau König  
* Karl Schönböck as Sir John Corbett  Hans Richter as Philip  
* Roma Bahn as Die Baronin 
* Alice Treff as Lady Taylor  
* Erich Fiedler as Bobby Cut  
* Olga Limburg as Die Herzogin 
* Gertrud Wolle as Die Lehrerin   Georges Boulanger as Der Geiger  
* Hilde Scheppan as Die Sängerin

== References ==
 

== Bibliography ==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999. 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 