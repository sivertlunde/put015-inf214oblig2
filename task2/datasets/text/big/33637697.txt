Tired Theodore (1957 film)
{{Infobox film
| name           = Tired Theodore
| image          = 
| image_size     = 
| caption        = 
| director       = Géza von Cziffra
| producer       = 
| writer         = Max Neal (play) and Max Ferner (play) Franz Gribitz (screenplay) and Gustav Kampendonk (screenplay)
| narrator       = 
| starring       = See below
| music          = Heino Gaze
| cinematography = Willy Winterstein
| editing        = Martha Dübber
| studio         = 
| distributor    = 
| released       = 1957
| runtime        = 95 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Tired Theodore (originally Der müde Theodor) is a 1957 West German film directed by Géza von Cziffra.

== Plot summary ==
 

=== Differences from play ===
 

== Cast ==
*Heinz Erhardt as Theodor Hagemann
*Renate Ewert as Lilo Haase
*Peter Weck as Felix
*Loni Heuser as Rosa Hagemann
*Karin Baal as Jenny Hagemann - Tochter
*Albert Rueprecht as Harald Steinbberg
*Werner Finck as Dr. Karl Findeisen
*Kurt Großkurth as Walter Steinberg - Fabrikant
*Wolfgang Neuss as Direktor Noll
*Hubert von Meyerinck as Wilhelm Schulze
*Wolfgang Müller as Anton - Chefkoch
*Franz-Otto Krüger as Professor Erwin Link
*Joseph Offenbach as Rafael Cacozzo
*Ralf Wolter as Storch - Gerichtsvollzieher
*Wolfgang Heyer as Fritz Kümmel, Piccolo
*Reinhard Kolldehoff as Hotelgast
*Herbert Weissbach as Pfandleiher
*Balduin Baas as Anton - Hoteldiener
*Horst Beck as Regierungsrat Stammfest
*Lonny Kellner as Sängerin

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 


 
 