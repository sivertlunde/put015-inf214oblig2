Shadow Dancer (film)
 
{{Infobox film
| name           = Shadow Dancer
| image          = Shadow dancer film.jpg
| caption        = Film poster James Marsh
| producer       = Chris Coen Ed Guiney Andrew Lowe
| writer         = Tom Bradby
| starring       = Clive Owen Andrea Riseborough Gillian Anderson
| music          = Dickon Hinchliffe
| cinematography = Rob Hardy
| editing        = Jinx Godfrey
| studio         = Irish Film Board BBC Films UKFS Element Pictures Unanimous Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 101 minutes
| country        = United Kingdom Ireland
| language       = English
| budget         = 
| gross          =  $2.2 million   
}}
 James Marsh and based on the novel of the same name by Tom Bradby who also wrote the films script. The film premiered at the 2012 Sundance Film Festival    and was screened out of competition at the 62nd Berlin International Film Festival in February 2012.   

==Plot==
In 1993   for MI5, spying on her own family. Colette agrees to do so. An MI5 officer, Mac, is assigned as her handler.

==Cast==

* Andrea Riseborough as Colette McVeigh
* Clive Owen as Mac
* Gillian Anderson as Kate Fletcher
* Aidan Gillen as Gerry McVeigh
* Domhnall Gleeson as Connor McVeigh
* Brid Brennan as Ma David Wilmot as Kevin Mulville
* Michael McElhatton as Liam Hughes Stuart Graham as Ian Gilmour Martin McCann as Brendan OShea

==Reception==
The film currently holds a "Fresh" rating of 82%, based on 69 reviews, at Rotten Tomatoes.  British film magazine Empire (film magazine)|Empire giving it a score of 4 out of 5 stars, calling it "an intelligent and emotionally charged spy drama".  The Guardian called it "a slow-burning but brilliant thriller about an IRA sympathiser forced to become an informant by MI5". 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 