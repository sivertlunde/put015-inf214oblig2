New Battles Without Honor and Humanity
 
{{Infobox film
| name           = New Battles Without Honor and Humanity
| image          = Newbattles2000.jpg
| caption        = 
| director       = Junji Sakamoto
| producer       = Yukiko Shii Izumi Toyoshima Toshio Zushi
| writer         = Kōji Takada Kōichi Iiboshi (story)
| starring       = Etsushi Toyokawa Tomoyasu Hotei Show Aikawa|Shō Aikawa Ittoku Kishibe Kōichi Satō
| music          = Tomoyasu Hotei
| cinematography = Norimichi Kasamatsu
| editing        = Takeo Araki Toei
| released       = November 25, 2000
| runtime        = 109 minutes
| country        = Japan Japanese
| budget         = 
}}

  also known as Another Battle, is a 2000 yakuza film directed by Junji Sakamoto. It is a remake of Kinji Fukasakus Battles Without Honor and Humanity series from the 1970s, which were adapted from a series of newspaper articles by journalist Kōichi Iiboshi, {{cite book last = D. first = Chris authorlink = Chris D. title = Outlaw Masters of Japanese Film publisher = I.B. Tauris year = 2005 isbn = 1-84511-086-2 url = http://www.ibtauris.com/ibtauris/display.asp?K=9781845110901 pages = 9–10, 23 archiveurl = http://web.archive.org/web/20080119205527/http://www.ibtauris.com/ibtauris/display.asp?K=9781845110901 archivedate = 2012-05-26
}}  that were rewrites of a manuscript originally written by real-life yakuza Kōzō Minō while he was in prison.

Rock musician Tomoyasu Hotei, who plays Tochino Masatatsu, wrote the soundtrack to the film. Its title piece would be reworked and retitled "Battle Without Honor or Humanity" and go on to become a hit both in Japan and internationally. The film was followed by New Battles Without Honor and Humanity/Murder, or Another Battle/Conspiracy, directed by Hajime Hashimoto in 2003.

==Synopsis==
While the film is a remake of the original Battles Without Honor and Humanity series, it has no direct similarities. Set in Osaka, it focuses on former childhood friends Kadoya Kaneo (Etsushi Toyokawa) and Tochino Masatatsu (Tomoyasu Hotei) as their lives cross paths again. Kaneo is now a yakuza member, while Masatatsu is a nightclub owner with a distaste for crime gangs. When a yakuza boss dies, a struggle for his position takes place between Kaneos boss Awano (Ittoku Kishibe) and the young Nakahira (Kōichi Satō). Nakahiras men try to extort money from Masatatsu, bringing him in between a yakuza battle. {{cite web last = Schilling first = Mark date = 2000-11-21 title = SHIN JINGI NAKI TATAKAI: Cant keep a good hood down  url = http://www.japantimes.co.jp/text/ff20001121a1.html publisher = The Japan Times accessdate = 2012-05-26
}} 

==Cast==
* Etsushi Toyokawa as Kadoya Kaneo
* Tomoyasu Hotei as Tochino Masatatsu
* Show Aikawa|Shō Aikawa
* Ittoku Kishibe as Awano
* Kōichi Satō as Nakahira
* Seizō Fukumoto
* Shigeki Terao

==Awards==
*2000 Nikkan Sports Film Awards for Best Director - Junji Sakamoto Japan Academy Award for Newcomer of the Year - Tomoyasu Hotei
*2001 Yokohama Film Festival Award for Best Supporting Actor - Jun Murakami
*2001 Kinema Junpo Award for Best Director - Junji Sakamoto {{cite web title = Awards for Another Battle (2000) url = http://www.imdb.com/title/tt0269859/awards publisher = IMDB accessdate = 2012-05-26
}} 

==Soundtrack==
{{Infobox album  
| Name        = 
| Type        = Soundtrack
| Longtype    =
| Artist      = Tomoyasu Hotei
| Cover       = 
| Alt         = 
| Released    = November 29, 2000
| Recorded    =
| Genre       = Soundtrack
| Length      = 34:42
| Label       = EMI Music Japan
| Producer    = Tomoyasu Hotei
| Last album  =
| This album  =
| Next album  =
}}

The soundtrack to the film was composed and performed by Tomoyasu Hotei, who plays Tochino Masatatsu, with the London Session Orchestra also performing on several tracks. It was released on November 29, 2000 as  . {{cite web
|script-title=ja:新・仁義なき戦い そしてその映画音楽 url = http://www.amazon.co.jp/新・仁義なき戦い-そしてその映画音楽-サントラ/dp/B00005HQLN
|language=Japanese publisher = Amazon.com|Amazon.co.jp accessdate = 2012-07-13
}}  The first track is the theme of the original 1970s series, composed by Toshiaki Tsushima, while the second is Hoteis version of the same song, which would later be re-titled "Battle Without Honor or Humanity" and become internationally known. The soundtrack peaked at number 41 on the Oricon chart,  while "Born to Be Free", which was released as a single earlier in the year, reached number 14. 

===Track listing===
{{tracklist
| all_writing  = Tomoyasu Hotei, except where noted
| total_length = 34:42
| title1   = Battles Without Honor and Humanity Theme
| note1    =  , music by Toshiaki Tsushima
| length1  = 1:57 New Battles Without Honor and Humanity Theme
| note2    =  
| length2  = 2:28
| title3   = 
| note3    =  
| length3  = 0:46
| title4   = 
| note4    =  
| length4  = 1:10
| title5   = 
| note5    =  
| length5  = 2:15
| title6   = 
| note6    =  
| length6  = 1:22
| title7   = Conspiracy
| note7    =  , music by Simon Hale
| length7  = 3:25
| title8   = 
| note8    =  
| length8  = 2:34
| title9   = Confusion
| note9    =  
| length9  = 0:52
| title10  = 
| note10   =  
| length10 = 1:39
| title11  = Crisis
| note11   =  
| length11 = 1:58
| title12  = 
| note12   =  
| length12 = 1:34
| title13  = 
| note13   =  
| length13 = 3:46
| title14  = 
| note14   =  
| length14 = 0:53
| title15  = 
| note15   =  
| length15 = 1:33
| title16  = Born to Be Free (Movie)
| note16   =  
| length16 = 3:41
| title17  = New Battles Without Honor and Humanity Theme/Reprise
| note17   =  
| length17 = 2:25
| title18  = Born to Be Free
| note18   =  
| length18 = 5:24
}}

==References==
 

==External links==
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 