The Spell (2009 film)
{{Infobox film
| name           = The Spell
| image          = File:The Spell 2009 Carey Jones Film.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Owen Carey Jones
| producer       = 
| writer         = Owen Carey Jones
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Rebecca Pitkin, Luke Harris, Amber Hodgkiss
| music          = Alan Moore
| cinematography = Stephen J. Nelson
| editing        = Owen Carey Jones
| studio         = Carey Films
| distributor    = Kaleidoscope Home Entertainment,  Showbox Media Group
| released       =  
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
The Spell is a 2009 British horror film directed by Owen Carey Jones. It was first released in the United Kingdom on September 18, 2009, and stars Rebecca Pitkin as a young woman that begins to dabble in witchcraft but soon gets in over her head. The film is loosely based on the true story of Emma Whale,     who contacted Carey Jones with the intent to turn her experience into a film.  The Spell was shot entirely in Leeds. 

==Synopsis==
A former drug user, Jenny (Rebecca Pitkin) finds herself constantly struggling to find acceptance and love from her divorced parents, neither of whom truly want her around. Her mothers boyfriend even goes so far as to use Jennys room as storage, which makes Jenny have to live in the basement after her father sends her back to her mothers house. Jenny tries to become more independent by getting a job and moving in with her boyfriend Rick (Luke Harris). This arrangement is short lived, as the two end up breaking up over Kate (Laura ODonoughue), a young girl that is obsessed with Rick. As a result Jenny is pushed further into the arms of Ed, a co-worker that she had become extremely close to. However what the viewer soon learns is that Ed and Kate are both individually involved in the occult and that Kate has cast several spells with the intent to harm others. Eventually Kate manages to convince Rick to assist her in casting a spell that would make Jennys life miserable.

==Cast==
*Rebecca Pitkin as Jenny Luke Harris as Rick (as Pietro Herrera)
*Amber Hodgkiss as Vicky
*Julia Curle as Clare
*Laura ODonoughue as Kate Steve Murphy as Jennys Dad Steve Smith as Ed
*Luke Dickson as Psychiatrist
*Kristy Bruce as Laura
*Thomas Frere as Priest
*Deborah Brian as Jennys Mum
*Ian Targett as City Centre Priest
*Paul Hurstfield as Ian
*Lou Birks as High Priestess
*Claire Ferdinando as Sandra

==Reception==
Critical reception for The Spell has been overwhelmingly negative and the film holds a rating of 0% on Rotten Tomatoes, based on 9 reviews.  The Guardian criticized the film as "bewilderingly awful" and noted that the movie had "characters whose lips dont quite move in sync with the audio track."  The Screen Daily also heavily panned the film, remarking that "The intention behind The Spell may have been to ground this young woman’s nightmare in a mundane sense of everyday life but the result is a film that feels underwhelming throughout."  In contrast, HorrorNews.net gave a more positive review and commented that the movie would have little appeal to gore hounds, but that fans of atmospheric films would likely enjoy it. 

==References==
 

==External links==
*  

 
 
 
 
 