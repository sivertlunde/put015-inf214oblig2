Les secrets professionnels du Dr Apfelglück
{{Infobox film
| name           = Les secrets professionnels du Dr Apfelglück
| image          = 
| caption        = 
| director       = Alessandro Capone Stéphane Clavier Mathias Ledoux Thierry Lhermitte Hervé Palud
| producer       = Louis Becker Claudio Bonivento Alain Clert Thierry Lhermitte Giuseppe Pedersoli
| writer         = Philippe Bruneau Thierry Lhermitte
| based on       = 
| starring       = Thierry Lhermitte Jacques Villeret Véronique Genest Alain Chabat Dominique Lavanant Roland Giraud Zabou Breitman Daniel Gélin Valérie Mairesse Gérard Jugnot Jean Yanne Ginette Garcin Josiane Balasko Christian Clavier Michel Blanc Charlotte de Turckheim Martin Lamotte Ticky Holgado
| cinematography = Claude Agostini Roberto Girometti Gérard Sterin Jean-Jacques Tarbès	
| editing        = Sophie Schmit
| distributor    = AMLF
| studio         = Ice Films Films A2
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $3,679,192 
}}
Les secrets professionnels du Dr Apfelglück (The Professional Secrets of Dr. Apfelgluck)  is a 1991 French comedy film directed by Alessandro Capone, Stéphane Clavier, Mathias Ledoux, Thierry Lhermitte and Hervé Palud. 

==Plot==
Dr. Apfelglück, a prominent psychiatrist, recounts some of the more serious cases that came to him.

==Cast==
 
* Thierry Lhermitte as Doctor Apfelglück
* Jacques Villeret as Martineau
* Alessandro Haber as Jean-Luc
* Véronique Genest as Micheline
* Ennio Fantastichini as Alain
* Alain Chabat as Gérard Martinez
* Dominique Lavanant as Jacqueline Vidart
* Roland Giraud as Émile Leberck
* Zabou Breitman as Carole Ribéra
* Daniel Gélin as Roland Grumaud
* Renato Scarpa as Michel Martinelli
* Pascal Sevran as Alain Laurent
* Valérie Mairesse as Astrée
* Gérard Jugnot as Martini
* Jean Yanne as Germain
* Laurence Ashley as Anne Métayer
** Louba Guertchikoff as Old Anne Métayer
* Micha Bayard as Mother Tonnerre
* Philippe Bruneau as Jean-Paul Tarade
* Luis Rego as M. Gomez
* Laurent Gamelon as Maurice
* Doudou Babet as Georges Bellerive
* Jacqueline Rouillard as Madame Garaud
* Carole Jacquinot as Marinette
** Ginette Garcin as Old Marinette
* Josiane Balasko as The scientist
* Christian Clavier as The lawyer
* Michel Blanc as The Hindu
* Charlotte de Turckheim as The Spanish
* Martin Lamotte as The Spanish
* Alexandra Vandernoot as The Belgian
* Francis Lemaire as The Belgian
* Ticky Holgado as The hotelier
* Claire Nadeau as The hotelier
* Jean-Marie Bigard as The Café owner
* Bruno Moynot as The consumers bistro
* Dominique Farrugia as The sellor
* Mouss Diouf as The nurse
 

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 