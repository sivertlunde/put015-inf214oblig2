Westfront 1918
{{Infobox film
| name           = Westfront 1918
| image          = Westfront 1918.jpg
| image_size     = 
| caption        = Swedish film poster for Westfront 1918
| director       = Georg Wilhelm Pabst
| producer       = Seymour Nebenzal
| writer         = Ernst Johannsen
| narrator       =  Claus Clausen
| music          = Alexander Laszlo 
| cinematography = 
| editing        = Wolfgang Loe-Bagier 
| distributor    = Nero Films 1930
| runtime        = 97 mins
| country        = Weimar Republic
| language       = German
| budget         = 
}} trenches of Western Front 1930 by Georg Wilhelm Pabst, from the novel Vier von der Infanterie by Ernst Johannsen, and deals with the impact of the war on a group of infantrymen. It featured an ensemble cast led by screen veterans Fritz Kampers and Gustav Diessl; Diessl had been a prisoner of war for a year during the war.
 All Quiet American production, New Objectivity work through the late 1920s.  It was particularly pioneering in its early use of sound—it was Pabsts first "Sound film|talkie"—in that Pabst managed to record live audio during complex tracking shots through the trenches.

Westfront 1918 was a critical success when it was released, although it was often shown in truncated form. With the rise of National Socialism, the film quickly became considered by the German authorities as unsuitable for the people, notably for its obvious pacifism, and for its clear denunciation of war. This was an attitude that propaganda minister Joseph Goebbels would soon label as "cowardly defeatism".

Some shots from the film were used for scene-setting purposes in a 1937 BBC Television adaptation of the play Journeys End.
 

==Plot==
France 1918. In the last months of World War I, four infantrymen—Bayer, a young man known as the student (Der Student), Karl and the lieutenant—spend a few rest-days behind the Front. Here, the student falls in love with the French peasant girl Yvette. Back at the Front, the four suffer again the everyday hardships of war, dirt trenches and danger of death. Bayer, Karl and the lieutenant become trapped when part of the trench collapses in, and the student digs them out. Later they are mistakenly fired upon by their own artillery due to a misjudgement of distance, and again they are saved by the student, who as a messenger risked his life to relay instructions to the soldiers setting the firing range of the artillery.
 
Karl receives leave, returning to his starving home town and promptly catches his wife in bed with a butcher. Embittered and unreconciled, he returns to the front. In his absence, the student was stabbed in the melee. In the mud of a shell-hole is his body, only one hand sticking out. An enemy offensive is announced. Finally, supported by tanks, a large French infantry attack breaks through the thin German lines. During the defence against this onslaught, Karl and Bayer are seriously wounded, covering the remaining members of the group. The lieutenant has a nervous breakdown and falls into insanity. Shouting "Hurrah" non-stop, he salutes a pile of corpses. He is admitted to the field hospital, as well as Karl and Bayer. While the lieutenant is being carried though the hospital, many injured soldiers can be seen. In a fever Karl sees his wife again and dies with the words "We are all to blame!". He is covered up, but his hand is hanging out the side. A wounded Frenchman lying beside him takes the hand in his and says "comrades, not enemies". The final message "End" is displayed with a question mark.

==Background==

 
Stylistically, the film achieves a surprisingly high degree of realism, especially in the trench and fight scenes. The monotony of dying reinforced the oppressive impression of authenticity. Addition, there are "small" silent scenes, such as when the student almost incidentally observed as grave markers are made in a field of carpentry on the assembly line, or as Karls mother does not want to leave their place in the food queue when they again see her son.
 
But Pabst wanted more than just "Realism" : "I a realist? From my very first movie I have chosen realistic themes, but with the intention resolutely to be a stylist. ... Realism must be a trampoline from which you jump higher and higher, and in itself it has no value. The point is to overcome the reality. Realism is a means, not an end." (Quoted in : Tape Man / Hembus, p. 21). So not the fight scenes, but the individual stories of four soldiers Pabst illustrate actual pacifist statement: the belief in the power of international solidarity of ordinary people.
 
After the Nazis had seized power in 1933, the film was banned because it was considered "a very one-sided and therefore false representation of war" show and that would jeopardize "vital interest of the state to preserve the military will of the people maintain and strengthen" (text of the prohibition application at the German Film Institute).

==Cast==

*Fritz Kampers as The Bavarian
*Gustav Diessl as Karl
*Hans-Joachim Moebis as The Student Claus Clausen as The Lieutenant
*Jackie Monnier as Yvette
*Hanna Hoessrich as Karls Wife
*Else Heller as Karls Mother Carl Ballhaus as Journeyman butcher

*Wladimir Sokoloff as Purser

==Reviews==

* Apart from anything, everything I saw in the winter, a sound film these days was my most deeply: because he exposes the face of war for non-participants in the rudest. The impression drowned weeks, months. One should perform every New Years Day, once each year beginning; in every village, in every school; ex officio by law. What are plays " (Alfred Kerr in Berliner Tageblatt in 1930, quoted in: vol. Man / Hembus 19)?
* The urge to truthful reproduction of horror that prevails here outgrown two scenes, already almost exceed the limit of the expressible. One: a single battle ends with an infantryman is nipped in the swamp in front of everyone. (The fact that you can still see protrude from the bubbling mud later a dead hand, is unnecessary sensationalism.) The other is the front military hospital in the church with the maimed, nurses and doctors who can barely operate their craft farther from exhaustion. It is as if medieval torture pictures come to life " (Siegfried Kracauer in Frankfurter Zeitung in 1930, quoted in: vol. Man / Hembus, p 21).
*  Western Front 1918 "is the only war denunciatory film, which denied any complacency of the army - in this respect it is a cleaner work than that of Lewis Milestone | Milestone (not to speak of the discouraging complicity with the ideal soldiers that are found repeatedly in the French films this time) " (Roger Boussinot: . LEncyclopédie du Cinema  Paris 1967).
* The War »Westfront 1918" ... refuses ... nor the most secret glorification of war for the establishment "of human probation." He appears as the perfection of horror that he is. Breaks gradually the sense context in which it also for the war was initially driven for four infantrymen. They appear initially as actors, so then wins the anonymous power of the war, more and more power over them until their identity completely disintegrates: in madness or in an absurd death. ... In artless, slow and uneven rides the camera scans the battlefield and reveals the epic succession the horror. Whose political cause is of course out of sight of the camera  (Ulrich Gregor, Enno Patalas. History of Film 1895-1939 Vol 1. Reinbek 1976, p... 141)
* Above all, the hulking staging and presentation of the native family dramas and the sentimental evocation of a universal brotherhood disturb in a film ... impressed by his hard realism with which he the monotony and the terrors of the grave war from the German perspective . portrays " (Liz-Anne Bawden (Ed.): . rororo Filmlexikon  Vol 3. Reinbek 1978, p 761f.).
* With Western Front in 1918, "we have the first of the three sound films in front of us, with whom GW Pabst ... his glittering career began in silent film ... crowned before he fell into the mediocrity and opportunism. "" (Vol man / Hembus 19)

==Literature==

* Christa band man, Joe Hembus:  Western Front 1918 . In this .:  Classics of German sound film . Goldmann, Munich, 1980, pp 19-21, ISBN 3-442-10207-3.
* Christiane Mückenberger  Western Front 1918 . In Günther Dahlke, Günther Karl (ed.):  German feature films from the beginnings to 1933. A film leader . Henschel Verlag, 2nd edition, Berlin 1993, p 221 ff. ISBN 3-89487-009-5.
* Marc Vanden Berghe:  La mémoire impossible. Westfront 1918 de G. W. Pabst. Grande Guerre, soldats, automates. Le film et sa problématique vus par la "Petite Illustration" (1931),  Brussel 2,001th
* Four of the infantry. Their last days on the Western Front (1918 Ernst Johannsen) (Audiobook) (Ed .: Andre Kagelmann & Reinhold None), ISBN 978-3-939988-03-8.
* Christian Hissnauer:  Western Front 1918 - Four of the infantry . In:  film genres: war film . Ed. By Thomas Klein, Marcus Stiglegger and Bodo trotter. Stuttgart: Reclam 2006, pp 57-60  . ISBN 978-3-15-018411-0.
* Andre Kagelmann u Reinhold None. "Casually starts death, humans and animals to harvest." Considerations Ernst Johannsens novel  Four of the infantry  and GW Pabsts film WEST FRONT 1918. In: Ernst Johannsen: Four of the Infantry , Their last days on the Western Front in 1918. Ed.. Dens. Kassel:  Media Usage Edition , 2014. S. 80-113. ISBN 978-3-939988-23-6.

== See also ==
*List of German films 1919-1933

== Bibliography ==
* BANDMANN, Christa & HEMBUS, Joe: Westfront 1918. In: Dies.: Klassiker des deutschen Tonfilms. Munich: Goldmann 1980, pp.&nbsp;19–21 ISBN 3-442-10207-3
* VANDEN BERGHE, Marc, La mémoire impossible. Westfront 1918 de G.W. Pabst. Grande Guerre, soldats, automates. Le film et sa problématique vus par la Petite Illustration (1931), Brussels, 2001 – text online in www.art-chitecture.net/publications.php  
* Andre Kagelmann u. Reinhold Keiner: „Lässig beginnt der Tod, Mensch und Tier zu ernten.“ Überlegungen zu Ernst Johannsens Roman Vier von der Infanterie und G. W. Pabsts Film WESTFRONT 1918. In: Ernst Johannsen: Vier von der Infanterie. Ihre letzen Tage an der Westfront 1918. Hrsg. v. dens. Kassel: Media Net-Edition 2014. S. 80-113. ISBN 978-3-939988-23-6.

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 