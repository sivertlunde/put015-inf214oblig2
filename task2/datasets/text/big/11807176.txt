Duck (film)
{{Infobox Film
| name           = Duck
| image          = Duck film poster.jpg
| caption        = Film poster for Duck
| director       = Nic Bettauer
| producer       = {{plainlist|
* Nic Bettauer
* Domini Hofmann
}}
| writer         = Nic Bettauer
| starring       = {{plainlist|
* Philip Baker Hall
* Bill Brochtrup
* Amy Hill
}}
| distributor    = Right Brained Releasing
| released       =  
| runtime        = 98 minutes
| language       = English
| music          = 
| awards         =  
| budget         = 
}}
Duck is a 2005 American drama film by director-writer-producer Nic Bettauer.  It stars Philip Baker Hall.  The film is a dystopian view of the near then-future of 2009.

==Plot==
 
It is 2009 and Frances is deceased after a long-battling illness.  Her widower, Arthur Pratt (Philip Baker Hall), takes the urn of her ashes to the park where the ashes of his son, Daniel, are located.  Just as Daniels parents planted a tree and scattered his ashes on it three decades from before, Arthur plants a second tree near that of Daniels and scatters Francess ashes on it.  Arthur plans to kill himself before a duckling crosses his path.

Arthur notices the duckling is alone, so he spends the rest of the day helping it find its family.  Unfortunately for the duckling, its mother, along with the rest of her pack, is found later that night as roadkill.  Feeling sorry for the duckling, Arthur decides to take it home to his apartment.  There, he takes care of the duckling as if he were its mother, bathing and feeding it.  Arthur then decides to call the duckling "Joe."  As he bonds with his new companion, Arthur starts to forget his loneliness over his late wife and son.
 nursing homes also do not allow pets, Arthur is brave enough to be homeless just to be protective over Joes safety.  Arthur decides to return to the park, where he and Joe end up living in.  Joe eventually becomes a fully-grown duck.

During his stay at the park one day, Arthur picks up what appears to be litter off the ground.  He then offers it to a waste picker by the name of JC (Noel Gugliemi), who informs Arthur that the park is now a part-landfill and part-construction site for a shopping mall.

Later, while Joe is swimming in a pond in the park, workers from a sewage and septic service come to drain the pond.  The workers attempt to shoo Joe away from the pond, but Joe cannot get himself out as quickly as he should since he never learned how to fly like any other duck.  When they realize Joes inability, the workers start throwing objects at him.  Arthur immediately comes to Joes rescue.  From observing Arthurs actions, the workers assume Arthur is insane.  As a result, Arthur ends up getting interrogated by members of the Psychological Evaluation Team, while Animal Control are sent to capture Joe.

Arthur explains to the P.E.T. members his situation with Joe, but they find him a public nuisance and get the police to pick him up.  However, the officers that arrived are unsure if they should arrest Arthur since no one has pressed any charges.  In a calm and polite manner, Arthur convinces the cops to let him be, confirming that he is sane enough to take care of himself.  Arthur then leaves the park for good and starts searching for Joe before Animal Control does.  Miraculously, Arthur ends up finding Joe in an alley later that night.

Arthur and Joe then meet Norman (Bill Cobbs), a blind man accompanied by his guide dog Trisha at a bus stop. Norman tells Arthur that he and Trisha are on their way to the beach of Los Angeles and invites him and Joe to join them.  However, when the bus arrives, the bus driver (Nikki Crawford) refuses to let Arthur in because of Joe.  The duo next cross paths with an Chinese take out delivery man (Kelvin Yu) who fell from his bike because of what almost had been a car crash.  They briefly help him in order for him to carry on with his delivery.

By sunrise, Arthur and Joe make it to a homeless shelter, where they befriend Leopold (Bill Brochtrup), whom Arthur gives a pair of socks to.  Later on at the shelter, Leopold and Arthur join a seminar where the homeless people in the city get to know each other.  When it was Arthurs turn to introduce himself, Arthur talks about how overprotective he is to Joe.  However the other group members heckle at Arthur.  Angrily, Arthur takes Joe and leaves the shelter.  Leopold follows him outside and apologizes to Arthur.  Arthur tells Leopold that he is heading west and asks if he would like to come with him and Joe.  Leopold politely refuses his invitation but tells Arthur that he will miss him.

Later that night, Arthur and Joe sleep on the sidewalk in front of the property owned by Jeffrey (French Stewart).  Jeffrey, who is on a balcony, notices Arthur on the sidewalk and orders him to get out of the way so that he could fall down and kill himself.  Arthur realizes Jeffreys contemplation and suggests that they talk about it before Jeffrey makes a decision.  Jeffrey agrees but will meet Arthur halfway.  Arthur then meets face to face with Jeffrey on mid-level while Joe keeps his eyes on Jeffrey.  Jeffrey implies to Arthur that he is contemplating suicide simply because he knows his girlfriend is having an affair with his best friend.  Arthur successfully persuades Jeffrey to change his mind by advising him to live "a happy life without them, all out for them to see."  When Jeffrey asks Arthur how to thank him, Arthur responds by asking if he and Joe were allowed to use his bathroom.  However, Jeffrey does not permit them out of respect for his girlfriend, who is apparently living with him.

The next day, a Vietnamese-American pedicurist (Amy Hill) allows the duo in her beauty salon before its opening time.  She kindly gives Joe a bath.  As she is drying him, the pedicurist openly confesses to Arthur that her husband was killed in Vietnam and that she and her daughter moved to America for a better life.  Later that day, at a golf course, the duo encounter the presence of a little girl by the name of Samantha (Quinby Kasch) who got herself separated from her nanny.  That night, it is revealed that it is Halloween, and the duo are invited to teenagers house where a costume party has occurred; some of the guests are drunk.  Arthur loses sight of Joe for a moment but finds him and they leave.

The next morning, Joe and Arthur make it to a bridge in which the latter decides it may be best that they part ways.  Joe is stubborn at first, so Arthur just walks away.  Joe then jumps off the bridge, and quacks in danger upon landing on the creek.  Arthur once again rescues Joe, noticing the creek is full of toxic waste.  Arthur apologizes to Joe for abandoning him as he cleans him up, telling him "Id die without you, Joe." 

Arthur and Joe finally make it to the beach, where the former allows his bare feet to touch the sand.  There, they reunite with Norman and Trisha.  The film fades to black as the four of them happily continue walking on the beach in horizon.

==Cast==
* Philip Baker Hall as Arthur Pratt
* Bill Brochtrup as Leopold
* Amy Hill as Pedicurist
* Noel Gugliemi as JC
* French Stewart as Jeffery
* Bill Cobbs as Norman

==Reception==
Rotten Tomatoes, a review aggregator, reports that 50% of 18 surveyed critics gave the film a positive review; the average rating is 5.3/10.   Metacritic rated it 53/100 based on seven reviews.   Lisa Nesselson of Variety (magazine)|Variety called it "a small, affecting road movie peopled with sharp vignettes".   Michael Rechtshaffen of The Hollywood Reporter wrote, "Bettauer has a lot of serious things to put across about survival in the big, unfeeling metropolis, and while her modern-day fable obviously has Capra-esque intentions, the maudlin results cry out for a better focused, more sharply executed plan of attack."   Gary Goldstein at reel.com rated it 1.5 stars, saying "Duck is a turkey" and "Bettauers made a tedious, groan-worthy picture notable only for the bigger issues it attempts—and fails—to successfully explore than for any real entertainment value."  The New York Times said "it tries too hard" and "ducks arent all that endearing".  Mark Feeney in the Boston Globe said that Bettauer "strikes a very uneasy balance" between playing for tears or laughs. 

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 
 