Tokyo: The Last War
{{Infobox film
| name         = Tokyo: The Last War
| image        = Teitotaisenposter.jpg
| caption      = Theatrical poster
| writer       = Kaizo Hayashi Yoshiharu Ueoka
| based on     =   Tadao Nakamura
| director     = Takashige Ichise
| producer     = Takashige Ichise Satoshi Kanno
| cinematography = Shohei Ando
| music        = Koji Ueno
| editing      = Keiichi Itagaki
| special effects = Screaming Mad George
| production studio = Exe/Imagica
| distributor  = Toho Studios
| released     =  
| country      = Japan Japanese
}}

Tokyo: The Last War (帝都大戦) is a  .

==Plot== Allied forces using magic.  Unfortunately, the spirits of those civilians who were horribly killed during the fire bombings culminate together and reincarnate Yasunori Kato (Kyusaku Shimada), the demonic Onmyoji who was suppressed 20 years ago.  Once again, Kato wants Tokyo to suffer for its crimes and sets off to stop Kouou’s plan from succeeding so that the war will continue and Tokyo will be destroyed.  To challenge Kato, Kouou has hired a young man, Yuko Nakamura (Masaya Kato), who has incredible psychic abilities.  During the course of the story, Nakamura meets and falls in love with Yukiko Tatsumiya (Kaho Minami), who is now working as a nurse in a War Victims Hospital and still suffering from traumatic memories of abuse as a child by Kato.  The young psychic is unable to contend with Kato’s mighty powers and after several battles (the final involving his power being enhanced through artificial means), he is nearly killed.  Despite nearing death, Yukiko’s love gives Nakamura enough strength to perform one last trick which successfully destroys Kato’s physical body.  Meanwhile Kouou realizes that Japan is doomed to lose the war.  Thus he changes targets, sparing the Allied leaders, and launching a psychic attack against Adolf Hitler which drives Hitler to commit suicide in his bunker.  Yukiko prays to Masakado to seal away Kato’s soul forever.

==Production== Dark Water as well as their respective Hollywood remakes. 

==Differences from the novel== Allied efforts in the war.  In the book, Kato is already alive and well, waiting in China for the right time to strike back.  When he hears of the project, he returns to Japan to make sure it goes through to completion and systematically kills any political figure that opposes it.  The leader of the protesters is Tomasso, an Italian mystic who controls the Japanese freemasonry lodges.  At the end of the book, Kato kills Tomasso and the project goes to completion.  Roosevelt is cursed, suffers from polio and dies.  This clears the way for Harry Truman to step into office, who authorizes the Hydrogen Bomb to be used against Japan.   Also the mystic hired to curse Roosevelt is Ōtani Kōzui, a famous Buddhist missionary, instead of Kan’nami Kouou (a completely fictional character).

==Name==
Because the film has not been released in the US, it is commonly referred to in North America by the Romaji title Teito Taisen (roughly translated as Great War in the Capital)  even though the official international title is Tokyo: The Last War. 

==References==
;Notes
 

;Bibliography
*  
* 

==External links==
* 
*  at Toho Kingdom

 
 
 
 
 
 
 
 
 
 
 
 