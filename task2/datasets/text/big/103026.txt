Kramer vs. Kramer
 
{{Infobox film 
|  name           = Kramer vs. Kramer 
|  image          = Oscar posters 79.jpg
| alt = 
|  image_size     =
|  caption        = Original film poster
|  director       = Robert Benton
|  producer       = Richard Fischoff Stanley R. Jaffe
| screenplay      = Robert Benton
|  based on       =  
|  narrator       =
|  starring       = Dustin Hoffman Meryl Streep Justin Henry Jane Alexander
|  music          = Paul Gemignani Herb Harris John Kander Erma E. Levin Roy B. Yokelson Antonio Vivaldi
| cinematography  = Néstor Almendros   
|  editing        = Gerald B. Greenberg Ray Hubley Bill Pankow
|  distributor    = Columbia Pictures
|  released       =  
|  runtime        = 105 minutes
|  country        = United States
|  language       = English
|  budget         = $8 million 
|  gross          = $106,260,000  
}} Best Picture, Best Director, Best Adapted Best Actor, Best Supporting Actress.

==Plot==
Ted Kramer (Dustin Hoffman) is a workaholic advertising executive who has just been assigned a new and very important account. Ted arrives home and shares the good news with his wife Joanna (Meryl Streep) only to find that she is leaving him. Saying that she needs to find herself, she leaves Ted to raise their son Billy (Justin Henry) by himself. Ted and Billy initially resent one another as Ted no longer has time to carry his increased workload and Billy misses his mothers love and attention. After months of unrest, Ted and Billy learn to cope and gradually bond as father and son. 

Ted befriends his neighbor Margaret (Jane Alexander), who had initially counseled Joanna to leave Ted if she was that unhappy. Margaret is a fellow single parent, and she and Ted become kindred spirits. One day, as the two sit in the park watching their children play, Billy falls off the jungle gym, severely cutting his face. Ted sprints several blocks through oncoming traffic carrying Billy to the hospital, where he comforts his son during treatment.
 custody battle ensues. During the custody hearing, both Ted and Joanna are unprepared for the brutal character assassinations that their lawyers unleash on the other. Margaret is forced to testify that she had advised an unhappy Joanna to leave Ted, though she also attempts to tell Joanna on the stand that her husband has profoundly changed. Eventually, the damaging facts that Ted was fired because of his conflicting parental responsibilities, forcing him to take a lower-paid job, come out in court, as do the details of Billys accident.  

The court awards custody to Joanna, a decision mostly based on the assumption that a child is best raised by his mother. Ted discusses appealing the case, but his lawyer warns that Billy himself would have to take the stand in the resulting trial. Ted cannot bear the thought of submitting his child to such an ordeal and decides not to contest custody.

On the morning that Billy is to move in with Joanna, Ted and Billy make breakfast together, mirroring the meal that Ted tried to cook the first morning after Joanna left. They share a tender hug knowing that this is their last daily breakfast together. Joanna calls on the intercom, asking Ted to come down to the lobby. She tells Ted how much she loves and wants Billy, but she knows his true home is with Ted. She will therefore not take him. As she enters the elevator to go and talk to Billy, she asks her ex-husband "How do I look?" The movie ends with the elevator doors closing on the emotional Joanna, right after Ted answers, "Terrific."

==Cast==
* Dustin Hoffman as Ted Kramer
* Meryl Streep as Joanna Kramer
* Justin Henry as Billy Kramer
* Jane Alexander as Margaret Phelps
* Petra King as Petie Phelps
* Melissa Morell as Kim Phelps
* Howard Duff as John Shaunessy
* George Coe as Jim OConnor
* JoBeth Williams as Phyllis Bernard
* Howland Chamberlain as Judge Atkins
* Dan Tyra as Court Clerk

==Production==
Kate Jackson was originally offered the role played by Meryl Streep but was forced to turn it down. At the time, Jackson was appearing in the TV series Charlies Angels, and producer Aaron Spelling told her that they were unable to rearrange the shooting schedule to give her time off to do the film.   
At the time, Streep was cast as Phyllis (the one-night stand Ted has); this role was eventually given to JoBeth Williams when Streep was cast as Joanna.
The producers initially asked François Truffaut to direct; in fact, cinematographer Néstor Almendros, a collaborator on numerous Truffaut films, had already been hired with the expectation that Truffaut would helm the film. Truffaut seriously considered it, but in the end, too busy with his own projects, turned it down and suggested screenwriter Robert Benton  direct it himself.

==Reception==
The film received positive reviews from critics. It holds an 88% approval rating on review aggregator   of the Chicago Sun-Times gave the film four stars, giving praise to Bentons screenplay: "His characters arent just talking to each other, theyre revealing things about themselves and can sometimes be seen in the act of learning about their own motives. Thats what makes Kramer vs. Kramer such a touching film: We get the feeling at times that personalities are changing and decisions are being made even as we watch them." 

==Cultural impact==
Kramer vs. Kramer reflected a cultural shift which occurred during the 1970s, when ideas about motherhood and fatherhood were changing.  The film was widely praised for the way in which it gave equal weight and importance to both Joanna and Teds points of view. 

==Awards and nominations==
The film won 5 Oscars, another 31 wins and 15 nominations.

;American Film Institute Lists:
*AFIs 100 Years...100 Movies - Nominated 
*AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated 
*AFIs 10 Top 10 - #3 Courtroom Drama

{| class="wikitable"
|-
!  Award !! Category !! Recipients and nominees !! Result
|- 52nd Academy Awards
| Academy Award for Best Picture
| Stanley R. Jaffe
|  
|-
| Academy Award for Best Director
| Robert Benton
|  
|-
| Academy Award for Best Actor
| Dustin Hoffman
|  
|- Academy Award for Best Adapted Screenplay
| Robert Benton
|  
|-
| Academy Award for Best Supporting Actor
| Justin Henry
|  
|- Academy Award for Best Supporting Actress
| Jane Alexander
|  
|-
| Meryl Streep
|  
|-
| Academy Award for Best Cinematography
| Nestor Almendros
|  
|-
| Academy Award for Best Film Editing Jerry Greenberg
|  
|- 34th British Academy Film Awards
| BAFTA Award for Best Film
| Stanley R. Jaffe
|  
|-
| BAFTA Award for Best Direction
| Robert Benton
|  
|-
| BAFTA Award for Best Actor in a Leading Role
| Dustin Hoffman
|  
|-
| BAFTA Award for Best Actress in a Leading Role
| Meryl Streep
|  
|-
| BAFTA Award for Best Screenplay
| Robert Benton
|  
|-
| BAFTA Award for Best Editing Jerry Greenberg
|  
|-
| César Awards 1981
| César Award for Best Foreign Film
| Robert Benton
|  
|- David di David di Donatello Awards
| David di Donatello for Best Foreign Film
| Robert Benton
|  
|- David di Donatello for Best Foreign Actor
| Dustin Hoffman
|  
|- Special David
| Justin Henry
|  
|- 37th Golden Globe Awards
| Golden Globe Award for Best Motion Picture – Drama
| Stanley R. Jaffe
|  
|-
| Golden Globe Award for Best Director
| Robert Benton
|  
|-
| Golden Globe Award for Best Motion Picture Actor - Drama
| Dustin Hoffman
|  
|-
| Golden Globe Award for Best Screenplay
| Robert Benton
|  
|- Golden Globe Award for Best Supporting Actor
| Justin Henry
|  
|- Golden Globe Golden Globe Award for Best Supporting Actress
| Jane Alexander
|  
|-
| Meryl Streep
|  
|- Golden Globe Award for New Star of the Year in a Motion Picture – Male
| Justin Henry
|  
|- Japan Academy Prize
| Japan Academy Prize for Outstanding Foreign Language Film
| Robert Benton
|  
|-
| Blue Ribbon Awards Best Foreign Language Film
| Robert Benton
|  
|-
| Directors Guild of America
| Directors Guild of America Award for Outstanding Directing – Feature Film
| Robert Benton
|  
|-
| Hochi Film Award Best International Picture
| Robert Benton
|  
|- Kansas City Film Critics Circle Awards 1979
| Kansas City Film Critics Circle Award for Best Film
| Robert Benton
|  
|-
| Kansas City Film Critics Circle Award for Best Director
| Robert Benton
|  
|-
| Kansas City Film Critics Circle Award for Best Actor
| Dustin Hoffman
|  
|-
| Kansas City Film Critics Circle Award for Best Supporting Actress
| Meryl Streep
|  
|- Los Angeles Film Critics Association Awards 1979
| Los Angeles Film Critics Association Award for Best Film
| Robert Benton
|  
|-
| Los Angeles Film Critics Association Award for Best Director
| Robert Benton
|  
|-
| Los Angeles Film Critics Association Award for Best Actor
| Dustin Hoffman
|  
|-
| Los Angeles Film Critics Association Award for Best Supporting Actress
| Meryl Streep
|  
|- National Board of Review Awards 1979
|  
| Robert Benton
|  
|-
| National Board of Review Award for Best Supporting Actress
| Meryl Streep
|  
|- National Society of Film Critics Awards 1979
| National Society of Film Critics Award for Best Film 
| Robert Benton
|  
|-
| National Society of Film Critics Award for Best Director
| Robert Benton
|  
|-
| National Society of Film Critics Award for Best Actor
| Dustin Hoffman
|  
|- National Society of Film Critics Award for Best Supporting Actress
| Jane Alexander
|  
|-
| Meryl Streep
|  
|- 1979 New York Film Critics Circle Awards
| New York Film Critics Circle Award for Best Film
| Robert Benton
|  
|-
| New York Film Critics Circle Award for Best Director
| Robert Benton
|  
|-
| New York Film Critics Circle Award for Best Actor
| Dustin Hoffman
|  
|- New York Film Critics Circle Award for Best Supporting Actress
| Jane Alexander
|  
|-
| Meryl Streep
|  
|-
| Writers Guild of America Award
| Writers Guild of America Award for Best Adapted Screenplay
| Robert Benton
|  
|-
| 2nd Youth in Film Awards
| Young Artist Award for Best Leading Young Actor in a Feature Film
| Justin Henry
|  
|-
|}

==Adaptation==
In 1995, Kramer vs. Kramer was remade in India as Akele Hum Akele Tum, starring Aamir Khan and Manisha Koirala.

==See also==
* " "
* Trial film

==References==
 

==External links==
 
*  
*  
*  
*  
 
 
 
{{S-ttl|title=Academy Award winner for   Best Actor and  Best Supporting Actress}}
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 