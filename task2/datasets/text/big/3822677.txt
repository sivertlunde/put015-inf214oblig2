City of Women
{{Infobox film
| name           = City of Women
| image          = CityOfWomen.jpg
| image_size     = 
| caption        = 
| director       = Federico Fellini
| producer       = Franco Rossellini Renzo Rossellini Daniel Toscan du Plantier
| writer         = Story and Screenplay: Federico Fellini Bernardino Zapponi Brunello Rondi
| narrator       = 
| starring       = Marcello Mastroianni
| music          = Luis Enríquez Bacalov
| cinematography = Giuseppe Rotunno
| editing        = Ruggero Mastroianni
| distributor    = New Yorker Films Artificial Eye
| released       = March 28, 1980
| runtime        = 149 mins
| country        = Italy France
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

City of Women ( ) is a 1980 film written and directed by Federico Fellini.    Amid Fellinis characteristic combination of dreamlike, outrageous, and artistic imagery, Marcello Mastroianni plays Snàporaz, a man who voyages through male and female spaces toward a confrontation with his own attitudes toward women and his wife.

== Plot ==
Snàporaz (Marcello Mastroianni) wakes up during a train ride and has a brief fling with a woman in the bathroom, but its cut short when the train suddenly stops and the woman gets off. Snàporaz follows her into the woods, through the wilderness and into a Grand Hotel overrun with women in attendance for a surrealistic feminist convention. He winds up in a conference about polyandry, where his presence is rejected. A frightened Snàporaz retreats to the hotel lobby, but the exit is blocked; instead he seeks refuge with a girl who offers her assistance, Donatella (Donatella Damiani), inside an elevator.
 high on drugs and listen to techno. A frustrated Snàporaz ditches them and is harassed by them and two more cars until he finds shelter in the off-limits private property of one Dr. Xavier Katzone (Ettore Manni), who hails gunfire on his persecutors.
 phalic sculptures. He is also fascinated by a collection of photographs hanging from the manor walls that become alight by pressing a button and whisper out arousing dialogue. Katzone takes pride on his many inventions and sets on to celebrate his 10,000th photograph with an eccentric party that involves the blowing of 10,000 candles and a performance by his wife, in which she lures objects such as coins and pearls into her vagina by means of telekinesis. It is in this party that Snàporaz comes across his ex-wife, Elena (Anna Prucnal), who has a drunken argument with him, and meets Donatella again.

The police (composed solely of women) arrive, interrupting Katzone mid-song, and announce the imminent demolition of his house. They also inform him that theyve shot one of his dogs, Italo, his most beloved. A grieved Katzone buries him. Meanwhile, Snàporaz dances to Fred Astaire with Donatella and a friend of hers (who are both dressed in scanty clothing), but fails to sleep with either of them, instead getting stuck with his ex in bed. Unable to sleep and hearing strange noises, he crawls under the bed, entering a dream-like world in which he slides down a toboggan, along the way revisiting his childhood crushes (a sitter, a nurse, a prostitute). He is caged at the end of the slide and transported before a strange court, where he is judged for his masculinity. Although he is dismissed to go free, he decides to confront his tentative punishment, and escalates a towering ring before a feminine crowd. At the top of the ring he climbs up a ladder and into a hot air balloon in the shape of Donatella. Donatella herself fires at him from below with a machine-gun, bursting the balloon and sending Snàporaz plummeting to apparent death.

Snàporaz then wakes up on the very same train from the beginning of the film, showing the story to have been a mere nightmare. Just as he comes to this conclusion, he realizes his glasses are broken (as in his dream) and that the wagon is filled by the women that crowded his dream. The train then races into a tunnel and credits roll.

==Cast==
* Marcello Mastroianni - Snàporaz
* Anna Prucnal - Elena
* Bernice Stegers - Woman on train
* Donatella Damiani - Donatella (Woman on roller skates)
* Jole Silvani - Motorcyclist (as Iole Silvani)
* Ettore Manni - Dr. Xavier Katzone
* Fiammetta Baralla - Onlio
* Hélène Calzarelli - Feminist (as Hélène G. Calzarelli)
* Isabelle Canto da Maya - (as Isabelle Canto de Maya)
* Catherine Carrel - Commandant
* Stéphane Emilfork - Feminist
* Marcello Di Falco - Slave
* Silvana Fusacchia - Skater
* Gabriella Giorgelli - Fishwoman of San Leo
*   - Feminist
* Dominique Labourier - Feminist
* Marina Confalone - Feminist 
* Marina Hedman - Girl of Giro della Morte

== Critical reception ==

=== Italy and France ===
City of Women opened in eighty Italian theaters in March 1980 Kezich, 345  and received generally favorable reviews bordering "on respect rather than praise".  Corriere della Sera critic Giovanni Grazzini interpreted the film as "a catalogue of emotions, sometimes grotesque, sometimes farcical, which provides a few caustic jibes against the destruction of femininity by aggressive feminism... From a stylistic point of view, its less homogeneous than usual but other parts of the film are delightful. For instance, when fantasy is used to create types of people rather than caricatures. In this sense Fellini, having abandoned his gallery of monsters, becomes more prosaic. Or when the ambiguity of certain characters - an excellent example is the soubrette played by the charming Donatella Damiani - provides a touch of grace and bitchiness; or when the film becomes almost a musical; or when paradox becomes surrealist, such as the party and the hurricane at the villa of Katzone whos in despair because his favourite dog has died".  

"Fellini appears as the Madame Bovary of his adolescence", wrote Claudia Fava for Corriere Mercantile. "He revels in the enjoyment he feels at working with an experienced crew, side by side with faithful technicians who simulate trains on the move or the sea washing the shores of the inevitable Romagnol beaches as though they were working of the set of George Méliès. But then, again and again, Fellini has shown us that he is the greatest and most ingenious of Méliès heirs. Only the magic does not always work, especially in the attempt to create a kind of astonished confession of amused impotence when faced with the new woman of today, together with a feeling of nostalgia for the old woman of the past... Despite Fellinis extraordinary virtuosity, the film rarely achieves harmony of inspiration, of order, of strip-cartoon fantasy, or of irony."  Francesco Bolzoni of LAvvenire insisted that Fellini was "only playing games. But then we would hardly expect from Fellini a deep analysis of the nature of women... It is a game with occasional gaps and, more often, inventions that rejuvenate an all too familiar, all too hackneyed subject. A surprising serenity predominates... It is a film with a tragic vein that in the end proves to be light-hearted and occasionally amusing".  La Notte magazines Giorgio Carbone felt the maestro had "finally reached a splendid maturity that permits him to lavish his treasures upon us for the simple pleasure of doing so. Behind the festival of images and colours we can feel his delight in making this film, a delight which, from the very first scene, becomes ours too, and its something we havent felt in a long time... If the film lacks suspense in its story (we care little what happens to Snàporaz or Katzone because we know that sooner or later Rimini and those bosomy extras will appear on the scene), theres suspense in the images and in the scenic inventions". 
 33rd Cannes Film Festival,    the film was panned by the majority of French critics, some of whom offered review titles such as "Zero for Fellini", "A Tiring Deception", "A Disaster", as well as "A Mountain of Tedious Pretension".  Russian film director Andrei Tarkovsky, in Rome that year for the pre-production of Nostalghia, noted in his diary that City of Women was a fiasco: "At the Cannes Festival the papers said that Fellinis last film was a total disaster, and that he himself had ceased to exist. Its terrible, but its true, his film is worthless." 

=== USA ===
Released by   or Fellini could have expected with that kind of personal film. He had lost most of his audience here by then. Which isnt to say that I dont think him one of the great filmmakers of the world."  For Vincent Canby of The New York Times, however, the film was a success: "Though the film is overlong, even for a Fellini aficionado, it is spell-binding, a dazzling visual display that is part burlesque, part satire, part Folies-Bergère, and all cinema. As Snàporaz is haunted by the phantoms of all the women he has known, or wanted to know, from childhood on, Mr Fellini in City of Women is obsessed by his own feelings toward women, by his need for them, his treatment (mostly poor) of them, his continued fascination by them and his awareness that (thank heavens) theyll always be different... Though City of Women is about a libertine, its anything but licentious. Mr Fellinis licentiousness suggests a profound longing for some kind of protective discipline, if not complete chastity. As such discipline would destroy Snàporaz, it would make impossible the conception and production of a film as wonderfully uninhibited as City of Women." 

John Gould Boyum of the Wall Street Journal observed that "the films entire thrust has little or nothing to do with the striking of attitudes, the analyzing of ideas. What Fellini seems after here is the recording and communicating of a set of feelings: those complex, contradictory ones experienced by a middle-aged Italian male suddenly faced with a cataclysmic upheaval in social and sexual mores... We do not go to Fellini to immerse ourselves in story and character or to encounter ideas. What we want from the maestro and what he gives us are fabulous adventures in feeling - a decidedly original mixture of nostalgia, poignancy, and joy that is unmistakably Fellinis own." 

==Other uses of the title==
* City of Women is also the title of a history written by Christine Stansell about sex and class in New York from 1789-1860.
* The Book of the City of Ladies (1405), a book defending women, by Christine de Pizan, is sometimes referred to as City of Women.
* The film is classified as  &nbsp;R18&nbsp;  in New Zealand for sex scenes.
* City of Women is also a controversial ethnography by Ruth Landes.

==References==

===Notes===
 

===Bibliography===
* Alpert, Hollis (1988). Fellini: A Life. New York: Paragon House.  ISBN 1-55778-000-5 
* Fava, Claudio and Aldo Vigano (1990). The Films of Federico Fellini. New York: Citadel.  ISBN 0-8065-0928-7 
* Kezich, Tullio (2006). Fellini: His Life and Work. New York: Faber and Faber.  ISBN 978-0-571-21168-5 
* Tarkovsky, Andrei (1994).  . London: Faber and Faber.  ISBN 0-571-16717-9 

===Further reading===
* Lederman, Marie Jean . "Dreams and vision in Fellinis City of Women." Journal of Popular Film and Television, Volume 9, n° 3 Fall 1981, p.&nbsp;114-22.
*   Cini, Roberta. Nella città delle donne : femminile e sogno nel cinema di Federico Fellini, Tirrenia (Pisa) : Edizioni del Cerro, 2008.
*   Monti, Raffaele. Bottega Fellini: la Città delle donne: progetto, lavorazione, film, with photographs by Gabriele Stocchi. Roma: De Luca,  .

==External links==
* 
*  
* , The New York Times, April 1981

 

 
 
 
 
 
 
 
 
 