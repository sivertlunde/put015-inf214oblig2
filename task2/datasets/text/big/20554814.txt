Mettukudi
{{Infobox film
| name = Mettukudi
| image = 
| caption = Official DVD Box Cover
| director = Sundar C.   
| producer = N. Prabhavathy N. Jyothi Lakshmi N. Vishnuram N. Raghuram
| writer = K. Selva Bharathy (dialogues)
| screenplay = Sundar C.
| story = Sundar C.
| narrator = Karthik   Gemini Ganesan   Goundamani   Nagma   Manivannan   
| music = Sirpy 
| cinematography = U. K. Senthil Kumar
| editing = P. Sai Suresh
| studio = Ganga Gowri Productions
| distributor = Ganga Gowri Productions
| released = 29 August 1996
| runtime =
| country = India Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 1996 Tamil language Drama film directed by Sundar C. starring Karthik (actor) |Karthik, Gemini Ganesan, Goundamani, Nagma and Manivannan. The film was screened for 75 days in many centres and was declared a commercial success. 

__TOC__

==Plot==
The story starts in the 17th century. The King Raja Cholan presents his valuable sword to his soldier for playing a vital role in winning a war. It was preserved by his family descendants. This information was known by an archeologist who plans to steal 
it due to its monetary value. Presently Gemini Ganesan is charged with the care of the sword.

The villain plans to send Karthik to do this job. He enters into the palace as the son (who ran of from the family in his childhood)of Manivannan, who is the son in law of Gemini Ganesan. Every thing went smooth initially. However Nagma, grand daughter of Gemini, fell in love with him. The situation called for Karthik to marry her. Later Nagma came to know the truth and challenges him about stealing the sword.

In the mean time Gemini Ganesan not knowing Karthik for what he came there for, gave the sword to him and pressed him to look after it. This made Karthik realize his mistake and promises him to protect it. The climax occurs when he manages to protect the sword from the villain, and the family accepted Karthik after learning of the truth.

==Cast== Karthik
* Nagma
* Gemini Ganesan
* Goundamani
* Manivannan
* Thilakan
* Rajiv
* Kazan Khan
* Udhay Prakash
* Viswanathan
* Alwa Vasu
* Kavitha
* Kalaranjani
* Rageena

==Soundtrack==
{{Infobox album  
| Name        = Mettukudi
| Type        = soundtrack
| Artist      = Sirpi
| Cover       =
| Caption     =
| Released    =    
| Recorded    =
| Genre       =
| Length      = Tamil
| Label       = Bayshore
| Producer    =
| Reviews     =
| Compiler    =
| Misc        =
}}

{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singers
| total_length    =
| all_writing     =
| all_lyrics      = Pazhani Bharathi
| all_music       =
| writing_credits =
| lyrics_credits  =
| music_credits   =
| title1          = Anbulla Mannavane
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Mano (singer)|Mano, Swarnalatha
| length1         = 5:15
| title2          = Velvetta Velvetta
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Mano (singer)|Mano, K. S. Chithra
| length2         = 4:59
| title3          = Indha Poonthendral
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Mano (singer)|Mano, Sirpy, Israth
| length3         = 4:59
| title4          = Adi Yaaradhu Yaaradhu
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Mano (singer)|Mano, K. S. Chithra
| length4         = 5:00
| title5          = Mana Madurai Gundu Malliye
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Krishnachander, Swarnalatha
| length5         = 4:55
| title6          = Saravanabava
| note6           =
| writer6         =
| lyrics6         =
| music6          = Mano
| length6         = 4:48

}}

==References==
 

==External links==
*  

 

 
 
 
 
 

 