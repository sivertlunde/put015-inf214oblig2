The Notebook (2013 film)
 
 
{{Infobox film
| name           = The Notebook
| image          = 
| caption        = 
| director       = János Szász
| producer       = 
| writer         = Agota Kristof András Szekér János Szász
| starring       = András Gyémánt László Gyémánt
| music          = 
| cinematography = Christian Berger
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = $85,621   
}} Best Foreign Language Film at the 86th Academy Awards,    making the January shortlist.   

==Cast==
*András Gyémánt as One
*László Gyémánt as Other
*Gyöngyvér Bognár as Mother
*Piroska Molnár as Grandmother
*András Réthelyi as Orderly
*Ulrich Thomsen as Officer
*Orsolya Tóth as Harelip
*János Derzsi as Sutor
*Péter Andorai as Deacon
*Miklós Székely B. as Old homeless
*Krisztián Kovács as Deserter soldier
*Ákos Köszegi as Hungarian officer
*Ulrich Matthes as Father

==See also==
*List of submissions to the 86th Academy Awards for Best Foreign Language Film
*List of Hungarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 