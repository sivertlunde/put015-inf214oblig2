The Three from the Filling Station (1955 film)
{{Infobox film
| name           = The Three from the Filling Station 
| image          = 
| image_size     = 
| caption        =  Hans Wolff
| producer       = Willi Forst   Kurt Ulrich
| writer         = Gustav Kampendonk    
| narrator       =  Walter Müller  Walter Giller   Germaine Damar
| music          = Werner R. Heymann
| editing        = Hermann Leitner
| cinematography =  Willi Sohm
| studio         = Berolina Film
| distributor    = Herzog-Filmverleih
| released       = 22 December 1955
| runtime        = 93 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 West German Hans Wolff Walter Müller and Walter Giller. After losing their money, three young men go to work at a filling station where they all fall in love with the same woman. 
 UFA film The Three from the Filling Station. 
It was one of a series of remakes made during the 1950s of major hits during the Weimar and Nazi eras.

==Cast==
* Adrian Hoven: Peter Walter Müller: Robert
* Walter Giller: Fritz
* Germaine Damar: Gaby Kossmann
* Willy Fritsch: Konsul Willy Kossmann
* Claude Farell: Irene von Turoff
* Oskar Sima: Dr. Calmus
* Wolfgang Neuss: Prokurist Bügel Hans Moser: Autofahrer
* Rudolf Vogel: Gerichtsvollzieher
* Hilde Hildebrand: Handleserin
* Fritz Imhoff: Chef der Kaufhaus A.G.
* Alexa von Porembsky: Sekretärin
* Oscar Sabo: Zigarettenhändler
* Carl Hinrichs: Bäcker Rolf Olsen: Bauer Hans Schwarz: Automechaniker
* Friedl Hardt: Frau bei der Autoreparatur

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 