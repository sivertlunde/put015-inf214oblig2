Teenage Paparazzo
{{Infobox film
| name           = Teenage Paparazzo
| image          = Teenagepapposter.jpg
| border         = yes
| caption        = Promotional poster
| director       = Adrian Grenier
| producer       = {{plainlist|
* Adrian Grenier
* Matthew Cooke
* Bert Marcus
* John Loar 
* Robin Garvick
* Lynda Pribyl
}}
| writer         = {{plainlist|
* Adrian Grenier
* Thomas de Zengotita
}}
| narrator       = Thomas de Zengotita
| starring       = {{plainlist|
* Adrian Grenier
* Austin Visschedyk
* Lindsay Lohan
* Paris Hilton
* Alec Baldwin Kevin Connolly
* Matt Damon
* Whoopi Goldberg
}}
| music          = Janice Ginsberg
| cinematography =  David Serafin
| editing        = Jim Curtis Mol
| distributor    = Reckless Productions 
| released       =  
| runtime        = 91 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Paparazzi photographer named Austin Visschedyk. It was directed by actor Adrian Grenier. Produced by Bert Marcus, Adrian Grenier and Matthew Cooke.

==Film premise== 
Teenage Paparazzo chronicles the relationship of a 14-year-old paparazzo  Austin Visschedyk and actor Adrian Grenier. Grenier encountered Visschedyk one night and decided to follow him while searching for celebrities.  Grenier had set himself a mission in getting to understand the world of the paparazzi. Austin has to be tutored and stays up late at night taking pictures and surfing the internet. During the day he is often called away to photograph celebrities, which he is successful at doing due to his young age and appearance. As the film progresses Grenier realizes his rather negative influence on Visschedyks life. A year after initial production of the film ended, Austins attitude and behavior has changed for the better. Grenier offers Austin a relationship stronger than the pap-celebrity one they have had. The film concludes with Austin telling Adrian to turn the camera off, which he subsequently does.

== Release ==
Teenage Paparazzo premiered at the 2010 Sundance Film Festival.   It aired on HBO on September 27, 2010,    and was released on DVD on March 29, 2011. 

== Reception == 
Rotten Tomatoes, a review aggregator, reports that 100% of five surveyed critics gave the film a positive review; the average rating was 6.7/10.   Jeniffer Merin of About.com gave the film four out of five stars, saying "Grenier not only does a very good job of considering the many nuances of the relationship between paparazzi and celebrities, his presentation is thoroughly entertaining."   David Chen of SlashFilm wrote, "Teenage Paparazzo Austin Visschedyk actually proves to be an incredibly fascinating individual and documentary subject. But Grenier also manages to weave in some cultural analysis and some decent drama to create a film that’s entertaining from beginning to end."   Dennis Harvey of Variety (magazine)|Variety wrote, "Adrian Greniers excellent feature offers sophisticated musings on Americas obsession with fame."     Neil Genzlinger of The New York Times called it "a surprisingly engaging, well-thought-out film."   John DeFore of The Hollywood Reporter described it as "a lively   whose familiar subject matter is countered by a novel spin and its makers privileged vantage point". 

==References==
 

==External links==
*  
*  

 
 
 
 
 