Patient Porky
 
{{Infobox Hollywood cartoon
|series=Looney Tunes (Porky Pig) Rob Clampett
|story_artist=Warren Foster
|animator=Norman McCabe
|voice_actor=Mel Blanc
|musician=Carl Stalling
|producer=Leon Schlesinger
|distributor=Warner Bros.
|release_date=August 24, 1940 (USA)
|color_process=Black and White
|run_time=6 minutes
|movie_language=English
}}

Patient Porky is a Warner Bros. Looney Tunes theatrical cartoon, starring Porky Pig. It was directed by Bob Clampett, written by Warren Foster, and scored by Carl W. Stalling. Patient Porky was released on August 24, 1940.

A title card at the beginning claims that the cartoon is adapted from a book called The Pains Came, but this is merely a joke, a pun off the novel and film The Rains Came.

==Plot==
The cartoon begins with a tour of a hospital where we see many patients resting in their beds. Porky soon checks in with a stomachache, caused by overeating at his birthday party. Instead of a real doctor, he encounters a crazy cat patient who as soon as he hears Porkys plea for a doctor rushes over and introduces himself as "Young Dr. Chilled-Air" (a reference to Dr. Kildare). After Porky tells him whats wrong with him, the cat decides to take an X-ray of Porkys stomach. Inside we see three-quarters of a birthday cake with the candles still lit. The cat then sympathetically escorts Porky over to a bed where he throws him into it and he bounces up off of the bed, his jacket comes off and the gown goes on. The cat then brags to the other patients, "Look fellas! I got a patient! I got a patient!" The cat then grabs the bed, pushes it and they go speeding through the hospital where he sings "Ive got a terrific urgin to be a famous surgeon so Im going to start out to carve my new career." He rushes Porky into Surgery where we see him sharpening knives and cleaning a saw with a rag. The cat then walks up to Porky who is lying in bed unsure of what the cats plans are, takes the covers down and lifts his gown to operate. As soon as Porky sees what the cat is up to and yells, "Hey! W-W-Whats the big idea?" and wiggles around to try to get away from the cat and the saw. He tries to escape the cat by slipping through the covers then running down the hall. He runs out of the hospital and back home where the cat is hot on his trail. Porky runs into his bedroom and slams the door and the cat follows him where he finds Porky lying in bed. Thinking he has the upper hand, the cat lifts his gown to operate on him again. Suddenly he sees a sticker pasted on Porkys stomach that reads "Do Not Open till Xmas." The cat looks confused at the camera and says "Christmas?", then jumps in bed next to Porky and states "Ill wait" much to Porkys horrific dismay and the cats satisfaction.

==Edited versions== The Jack Benny Program as an elevator operator are cut out of the cartoon.  However, the cartoon has still been aired unedited (as recently as 2015) on the Canadian cable channel Teletoon Retro.

==Availability==
* Patient Porky is available, uncut and restored, on  , Disc 3.

==See also==
* Looney Tunes and Merrie Melodies filmography (1940–1949)

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 