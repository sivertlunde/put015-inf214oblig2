Children of the Open Road
{{Infobox television film
| name           = Kinder der Landstrasse
| image          = Kinderdlandstrasse plakat.jpg
| alt            =
| caption        = 
| director       = Urs Egger
| producer       = Johannes Bösiger Helga Bähr Veit Heiduschka Alfred Nathan Martin Schmassmann Peter Spörri
| story          = Johannes Bösiger
| starring       = Jasmin Tabatabai Andrea Eckert Mathias Gnädinger Nina Petri Noemi Steuer
| screenplay     = 
| music          = Detlef Petersen
| cinematography = Lukas Strebel
| editing        = Barbara Hennings
| studio         = 
| distributor    = Metropolis Film World Sales 
| released       =  
| runtime        = 117 minutes
| country        = Switzerland Austria Germany German
| budget         = 
| gross          = 
}} Yeniche culture in Switzerland by the Kinder der Landstrasse foundation between 1926 and 1973.

== Plot (excerpt) ==
The  , Jara Weiss as child, and Jasmin Tabatabai as adult Jana), in 1939 five years old, has been snatched from her parents, and the following years shes consigned to a life of orphanages and foster homes, in order to sever her ties with her culture and to assimilate Jana to a better way of life. Jana becomes the ward of Dr. Schönefeld, the director of the agency. But the system is not able to break the young women, and instead of to preempt a new generations caravans from following their nomadic traditions along Switzerlands country lanes. Though grown sad-eyed, tough and wary after years as a ward of the state, inprisoned and stigmatized as crazy and unteachable and even declared insane for the same claimed reasons by officials, Jana struggles to unloose the bonds of the system and starts to find her mother and father. Experienced with foster families and homes, Jana is convinced, that she will always in the eyes of others be a Gipsy. As a young adult falls Jana in love with the farmers son Franz, and the plan for the future reunion with her parents, to the beginning ignoring that der family was finally destroyed by Schönefeld. At the request of their guardian, Jana is arrested again and prisoned by the so-called administrative care, but Franz hels her to escape. But the luck of the young pair is soon overshadowed by Janas pregnancy ...

== Cast ==
* Jasmin Tabatabai as Jana Kessel 
* Andrea Eckert as Theresa Kessel 
* Martina Strässler as 5 year old Jana 
* Herbert Leiser as Paul Kessel 
* Jara Weiss as 9 year old Jana 
* Hans-Peter Korff as Dr. Schoenefeld 
* Nina Petri as Ms Roth 
* Mathias Gnädinger as Roger Kessel 
* Noemi Steuer as Andrina Kessel 
* Andreas Schindelholz as Django 
* Andreas Löffel as Nino 
* Paul Schirmer as Yeniche men 
* Arnold Barri as Yeniche men 
* Felix Amsler as Yeniche men 
* Johanna Karl-Lory as fortune teller

== Background ==
From 1926 to 1973, the Swiss government had, according to the final report Unabhängige Expertenkommission Schweiz – Zweiter Weltkrieg (Volume 23) of the Swiss parliamentary commission of that name, a semi-official policy of institutionalizing Yeniche parents and having their children adopted by more "normal" Swiss citizens, in an effort to eliminate Yeniche culture.    The name of this program, provided by the Swiss children-oriented Pro Juventute foundation, was Kinder der Landstrasse (literally: children of the country road). In all, about 590 children were taken from their parents and institutionalized in orphanages, mental institutions and even prisons.

== Production ==
The film was produced by Lichtblick Film- und Fernsehproduktion, Panorama Films, Schweizer Fernsehen (SRF), Wega Film and Zweites Deutsches Fernsehen (ZDF) in 1992 on locations in Austria, Germany and Switzerland. As of 1992, Kinder der Landstrasse was the hitherto most expensive Swiss film production. It was also the first official co-production of the three German-speaking countries Switzerland, Germany and Austria.

== Cinema and television ==
Premierred at a public audition in May 1992 in Zürich-Wollishofen,  The dramas international premiere was in August 1992 on occasion of the Locarno Film Festival. In television, the dram was first aired on 24 February 1994. The film was presented repeatedly at international film festivals, among them the San Francisco Film Festival in 2006 at its 50th anniversary.   

 

== Critical response ==
Lexikon des Internationalen Films (LIF) claims on the fate of a vagrant family and her daughter in the period 1939–1972, the youth and social welfare of a Swiss charity is denounced, the exercise ideological abuse of power of the demon National Socialist ideas ... postponed action that encourages social and social conscience and provides fundamental issues of our Western social system.   

The San Francisco Film Festival claims with refreshing clarity, director Urs Eggers straightforward storytelling serves the film well as cinematic drama, as do fine, naturalistic performances, especially by Jasmin Tabatabi who plays the teenage Jana with determined if bewildered candor ... The familys near escape from the Nazis in the beginning casts an ironic light on the film. Whether it comes at the hands of the executioner or by the edicts of the self-righteous bureaucrat, cultural annihilation is the ultimate goal of racism. 

== Awards ==
* 1992: Amiens International Filmfestival, won OCIC Award to Urs Egger 
* 1992: Fort Lauderdale International Filmfestival, won International Film Guide Award as Best Foreign Picture and Spirit of the Independant Award to Johannes Bösiger

== Home media ==
The film was released on DVD in German language (FSK 12) 

== See also ==
* Kinder der Landstrasse foundation (1926–1973)
* Yeniche people#Switzerland

== External links==
*  

== References ==
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 