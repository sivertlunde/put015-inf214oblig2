Prathikaram
{{Infobox film 
| name           = Prathikaram
| image          =
| caption        =
| director       = Kumar
| producer       =
| writer         = T Rao Kedamangalam Sadanandan (dialogues)
| screenplay     =
| starring       = Jayabharathi Thikkurissi Sukumaran Nair Alummoodan Anandan
| music          = MB Sreenivasan
| cinematography =
| editing        =
| studio         = Sreekumar Productions
| distributor    = Sreekumar Productions
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by Kumar . The film stars Jayabharathi, Thikkurissi Sukumaran Nair, Alummoodan and Anandan in lead roles. The film had musical score by MB Sreenivasan.   

==Cast==
*Jayabharathi as Shobha
*Thikkurissi Sukumaran Nair as Pankajakshan Nair
*Alummoodan as Peethambharan
*Anandan as Raju, Soman
*Aranmula Ponnamma as Rajus mother Kanchana
*S. P. Pillai as Shobhas father
*Jose Prakash as Sreedharan Thampi
*Adoor Pankajam as Kamalam
*K. V. Shanthi as Vimala
*Baby Sumathi as Leela

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by Sreekumaran Thampi and . 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chirichappol || K. J. Yesudas, Aruna || Sreekumaran Thampi || 
|-
| 2 || Madhuram Madhuram || LR Eeswari || Sreekumaran Thampi || 
|-
| 3 || Suve Wazire waaliyaan || S Janaki, PB Sreenivas ||  || 
|-
| 4 || Swapnam Kaanukayo || S Janaki || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 