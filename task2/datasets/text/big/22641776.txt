Samson and Delilah (2009 film)
 
{{Infobox film
| name           = Samson and Delilah
| image          = Samson and Delilah poster.jpg
| caption        = Theatrical release poster
| director       = Warwick Thornton
| producer       = Kath Shelper
| writer         = Warwick Thornton
| starring       = Rowan McNamara Marissa Gibson
| music          = 
| cinematography = Warwick Thornton
| editing        = Roland Gallois
| distributor    = Madman Entertainment
| released       =  
| runtime        = 97 minutes
| country        = Australia Warlpiri
| budget         =
| gross          =
}}
 
 best foreign language film category. 

==Plot== sniffs petrol every morning. He lives in a run-down shelter with his brothers band playing reggae music, all day, right outside his bedroom. Samson is interested in Delilah, who lives with her grandmother. He throws a rock at her outside the local convenience store. In spite of mocking encouragement from her grandmother Delilah is not interested in him. Samson spends a day following Delilah around. He attempts to move in with Delilah. Delilahs grandmother dies and the old women in the community blame her neglect for her grandmothers death and thrash her with sticks. Samson in a fit of rage beats his brother with a stick to shut him and his band up. His brother beats him up. Samson steals a car and takes Delilah with him to Alice Springs where they live rough under a bridge over the dry bed of the Todd River. Gonzo, a deranged homeless man living there helps them. Samson continues to sniff petrol. At one point, he gets so high from the fumes that he does not notice when Delilah is taken by a group of white teenagers in a car. She is raped and bashed, but she eventually comes back to Samson, who is unconscious with petrol intoxication. She begins sniffing petrol too. Under the same circumstances as her abduction, they are both walking along the street and Delilah is hit by a car. When Samson eventually comes to and realises she has been hit he believes she is dead, and cuts off his hair as a sign of respect. He spends weeks sitting in the same position under the bridge sniffing petrol as a means of getting over Delilahs death. Delilah comes back and rescues Samson, and they are both brought back to their old village. As they arrive one of the communitys old women begins to beat Samson with a stick for stealing the communitys only car. Delilah decides to take Samson away to a secluded area, as a way of rehabilitating, and getting over his petrol sniffing habit. Eventually Samson stops sniffing petrol, and over time Delilah is able to coax him back to his original state.

==Cast==
* Rowan McNamara as Samson
* Marissa Gibson as Delilah
* Mitjili Napanangka Gibson as Nana Scott Thornton as Gonzo
* Matthew Gibson as Samsons Brother Steven Brown as Drummer
* Gregwyn Gibson as Bass Player
* Noreen Robertson Nampijinpa as Fighting Woman
* Kenrick Martin as Wheelchair Boy Peter Bartlett as Storekeeper

==Reception==
Based on 48 reviews, the film holds a 94% Fresh rating on the film review aggregator Rotten Tomatoes, with an average rating of 7.8/10. The critical consensus states that "Alternately beautiful and heartrending, Samson and Delilah is terrifically acted and shot, and presents a complex portrait of what it means to be Australian."   At The Movies, and was the only film to receive such a rating from the hosts in 2009.  
 SBS awarded the directors debut feature film four stars out of five, commenting that "the picture has an intrinsic sweetness, a genuine belief in the power of an individual’s love, but it is offset by a brutal worldview. " 

==Awards==
{| class="wikitable"
|-
! Award !! Category !! Nominee !! Result
|- Art Film Fest  Best Director Warwick Thornton
| 
|- Asia Pacific Screen Awards  Best Film Kath Shelper
| 
|- Australian Film Institute  AACTA Award Best Direction Warwick Thornton
| 
|- AACTA Award Best Film Kath Shelper
| 
|- AACTA Award Best Original Screenplay Warwick Thornton
| 
|- AACTA Award Best Cinematography
| 
|- AACTA Award Best Sound
|
| 
|- AACTA Award Best Editing Roland Gallois
| 
|- AACTA Award Best Lead Actor Rowan McNamara
| 
|- AACTA Award Best Lead Actress Marissa Gibson
| 
|- AACTA Award Best Supporting Actress Mitjili Napanangka Gibson 
| 
|- Australian Screen Sound Guild Best Sound Design
|
| 
|- AWGIE Award|Australian Writers Guild  Feature Film Warwick Thornton
| 
|- Major Award Warwick Thornton
| 
|- Cannes Film Festival  Golden Camera Award Warwick Thornton
| 
|- Dublin International Film Festival  Best Film Warwick Thornton
| 
|- Film Critics Circle of Australia Awards  Best Cinematography
|
| 
|- Best Director Warwick Thornton
| 
|- Best Film Kath Shelper
| 
|- Best Actress Marissa Gibson
| 
|- Best Editing
|
| 
|- Best Screenplay
|
| 
|- IF Awards  Best Sound
|
| 
|- Palm Springs International Film Festival  John Schlesinger Award – Honorable Mention Warwick Thornton
| 
|}

==Box office==
Samson and Delilah grossed $3,188,931 at the box office in Australia. 

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 