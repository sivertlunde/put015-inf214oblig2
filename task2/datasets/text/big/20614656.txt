Dushman (1990 film)
{{Infobox film
| name           = Dushman 
| image          = Dushmanoldmithun.jpg
| director       = Shakti Samanta
| producer       = Shakti Samanta
| writer         = 
| narrator       =  Mandakini Alok Nath Ranjeet Deepa Sahi Sadashiv Amrapurkar
| music          = R.D. Burman 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Mandakini as the lead protagonists. The film is directed by Shakti Samanta. 

==Plot==
Drama history of the father and the son separated because of frauds of the leader of a local gang those years when the son was the child. The young man by name of  Rakesh (a nickname "Bullet"), appeared the tool in hands of experienced gangsters, rises, not knowing that, on a way of the father which has decided to take justice in own hands and to revenge for family tragedy.

==Cast==
* Alok Nath as Rakesh father
* Mithun Chakraborty as Rakesh 
* Sadashiv Amrapurkar as Kalicharan Mandakini
* Deepa Sahi as Geeta
* Ranjeet
* Bob Christo

== Music ==
Music: R.D. Burman
Lyrics: Indivar

*Hoton Pe Tumne Pyar Likha Hai - Amit Kumar, Sadhna Sargam 
*Aao Huzoor Khao Huzoor- Amit Kumar
*Mera Naam Sweet Sixteen - Asha Bhosle, Amit Kumar
*Bhoola Nahin Maa - Amit Kumar
*Yeh Jhilmil Qateel Raten - Asha Bhosle, Amit Kumar
*Toone Jab Jeevan Diya - Amit Kumar

==External links==
*  
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Dushman+%281990%29

 
 
 
 

 