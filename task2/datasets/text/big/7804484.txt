Paradesi (2007 film)
{{Infobox film
| name = Paradesi
| image = Lal_in_Paradesi.JPG
| caption= Theatrical Poster
| writer = P. T. Kunju Muhammed
| starring = Mohanlal Lakshmi Gopalaswamy Padmapriya Shweta Menon
| director = P. T. Kunju Muhammed
| studio = Aashirvad Cinemas
| producer = Antony Perumbavoor
| music = Ramesh Narayan Shahabaz Aman
| cinematography = K. G. Jayan
| editing = Don Max
| distributor = Aashirvad Cinemas
| released =  
| runtime = 
| country = India
| language = Malayalam
| budget = 
|}}

Paradesi ( , meaning: foreigner) is a 2007 Indian Malayalam film written and directed by P. T. Kunju Muhammed and starring Mohanlal.

The film talks the issue of Indian citizens who left the country before independence in search of job, mostly to Arabia, and happened to reach Pakistan, and then when returned to the homeland, happened to be holding a Pakistani passport, and now is forced by the Indian authorities to leave the nation to Pakistan, which even the Pakistani authorities wont allow.

==Synopsis==
 Malabar region of Kerala, India to Karachi, Pakistan during the British Raj in search of a job. Post-Partition of India|partition, he returns from Pakistan and settles down in Malappuram, Kerala. This man, who lives as a true Indian, doesnt get his Indian passport and thus an Indian identity in the official sense, even after 50 years of Independence. The police harass him and his neighbours in a similar situation as Pakistani spies. The film has Mohanlal playing the character in three stages of his life, between the ages of 35 and 80. Jagathy Sreekumar and Sidhique play his friends in the film. Swetha Menon plays his wife and Padmapriya acts as a journalist.

==Production==

The film was produced by Antony Perumbavoor under the banner of Aashirvad Cinemas. The locations of the film included Ottappalam in Kerala and Rajastan.

==Cast==
* Mohanlal as Valiyakath Moosa
* Jagathy Sreekumar as Moosas friend
* Swetha Menon as Moosas Wife
* Lakshmi Gopalaswami as Moosas lover
* Padmapriya as Moosas journalist Siddique as Moosas friend, Hamsa
* T. G. Ravi as Moosas Friend	
* Madhupal as Journalist Vijayaraghavan
* Cochin Haneefa as Police
* Bharath Gopi
* Nilambur Ayesha Suresh Krishna
* Sona Nair
* Sreenath Irshad
* Ambika Mohan as Valiyakath Moosas mother
* Zeenath
* Manka Mahesh
* Rema Devi

==Music==
The film features music composed by Ramesh Narayan and Shahabaz Aman with lyrics penned by Rafeeq Ahmed. M. G. Sreekumar, Vineeth Srinivasan, K. S. Chithra and Sujatha Mohan are the vocalists.

==Awards== National Film Awards
* National Film Award for Best Make-up Artist - Pattanam Rasheed

;Kerala State Film Awards Best Actor - Mohanlal Best Story - P. T. Kunju Muhammed Best Makeup Artist - Pattanam Rasheed Special Jury Award - Jagathy Sreekumar Best Dubbing Artist - Hafsath, Zeenath
 Filmfare Awards Best Actor - Mohanlal Best Supporting Actress - Lakshmi Gopalaswami

;Kerala Film Critics Awards
* Best Actor - Mohanlal
* Best Actress - Lakshmi Gopalaswami

;Kerala Film Audience Council Awards
* Best Actor - Mohanlal

;Jaihind TV Awards
* Best Film
* Best Actor - Mohanlal
* Best Actress - Lakshmi Gopalaswami

==External links==
*   The Hindu
*  
*  

 

 
 