Pandaemonium (film)
 
{{Infobox film
| name           = Pandaemonium
| image          = Pandaemonium2000 poster.jpg
| caption        =
| alt            =
| director       = Julien Temple
| producer       = Nick OHagan
| writer         = Frank Cottrell Boyce John Hannah
| music          = Dario Marianelli
| cinematography = John Lynch
| editing        = Niven Howie
| distributor    = Optimum Releasing
| studio         =
| released       =  
| runtime        = 125 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 English poets Samuel Taylor Coleridge and William Wordsworth, in particular their collaboration on the "Lyrical Ballads", and Coleridges writing of "Kubla Khan".

The film was shot on the Quantock Hills near Taunton in Somerset.

==Cast==

* Linus Roache as Samuel Taylor Coleridge John Hannah as William Wordsworth
* Samantha Morton as Sara Coleridge
* Emily Woof as Dorothy Wordsworth
* Samuel West as Robert Southey

==Reception==
===Release Dates===

{| class="wikitable" Country
!align="left"|Date
|- Canada
|align="left"|
September 15, 2000 (Toronto International Film Festival) (Premiere)
|- Sweden
|align="left"|
January 31, 2001 (Gothenburg Film Festival)
|- France
|align="left"|
April 18, 2001
|- United States
|align="left"|
June 29, 2001
|- United Kingdom
|align="left"|
September 14, 2001
|- Norway
|align="left"|
October 23, 2001 (Bergen International Film Festival)
|- Australia
|align="left"|
July 14, 2005
|}

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Award
! Category
! Recipients and nominees
! Outcome
|-
| British Independent Film Award
| BIFA_Award_for_Best_Performance_by_an_Actress_in_a_British_Independent_Film | Best Actress
| Samantha Morton
|  
|-
| International_Filmfest_Emden | Emden International Film Festival
| Emden Film Award
| Julien Temple
|  
|-
| Evening Standard British Film Awards
| Best Actor
| Linus Roache
|  
|}

==External links==
 

*  
*   at Box Office Mojo BBFC
* 
*  
*  
*  

 

 
 
 
 
 

 