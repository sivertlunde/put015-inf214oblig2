X: The Unheard Music
 
X: The Unheard Music is a 1986 rockumentary film directed by W.T. Morgan about the Los Angeles punk band X (American band)|X. 
 John Doe, Exene Cervenka, Billy Zoom, and D.J. Bonebrake.
 Whisky and the Starwood, X set a new standard for driving, forceful songs that both critics and the public felt revolutionized the California sound.  X: The Unheard Music takes long, detailed, and often funny look at this scene but focuses on the group that critics have singled out as the leader of the underground pack. 

X: The Unheard Music was filmed by Angel City Productions between 1980 and 1985 in around Los Angeles. Post-production was completed almost five years to the month after shooting began.

The film was released on DVD and Blu-ray through MVD on December 7, 2011. Special features include footage of John Doe and Exene Cervenka in discussion as well as an interview with Angel City, the company behind the film. A live outtake and a trailer for the feature are also included on the disc.

==Songs in order of performance==
*"Los Angeles"
*"Year One"
*"Were Desperate"
*"Because I Do"
*"Beyond & Back"
*"Come Back to Me"
*"Soul Kitchen"
*"White Girl"
*"The Once Over Twice"
*"Motel Room in My Bed"
*"The Unheard Music"
*"Real Child of Hell"
*"Johny Hit & Run Paulene"
*"I Must Not Think Bad Thoughts"
*"The Worlds a Mess; Its in My Kiss"
*"The Have Nots"

==External links==
* 
* 
* 
* 

 

 
 
 
 

 