Truly, Madly, Deeply
 
 
 
{{Infobox film
| name           = Truly, Madly, Deeply
| image          = TrulyMadlyDeeplyMoviePoster.png
| image_size     = 215px
| alt            = 
| caption        = Reproduction poster
| director       = Anthony Minghella
| producer       = Robert Cooper
| writer         = Anthony Minghella
| starring       = Juliet Stevenson Alan Rickman
| music          = Barrington Pheloung
| cinematography = Remi Adefarasin
| editing        = John Stothart
| studio         = BBC Films Lionheart Winston
| distributor    = The Samuel Goldwyn Company
| released       =  
| runtime        = 106 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $1,554,742 
}} fantasy Musical music drama film made for the BBCs Screen Two series. The film, written and directed by Anthony Minghella, stars Juliet Stevenson and Alan Rickman.

==Plot==
Nina, an Interpreter (communication)|interpreter, is beside herself with grief at the recent death of her boyfriend, Jamie, a cellist. When she is on the verge of despair, Jamie reappears as a "ghost" and the couple are reconciled. The screenplay never clarifies whether this occurs in reality, or merely in Ninas imagination. Nina is ecstatic, but Jamies behaviour – turning up the central heating to stifling levels, moving furniture around and inviting back "ghost friends" to watch videos – gradually infuriates her, and their relationship deteriorates. She meets Mark, a psychologist, to whom she is attracted, but she is unwilling to become involved with him because of Jamies continued presence. Nina continues to love Jamie but is conflicted by his self-centered behaviour and ultimately wonders out loud, "Was it always like this?" Over Nina’s objections, Jamie decides to leave to allow her to move on. At the end of the film, Jamie watches Nina leave with Mark and one of his fellow ghosts asks, "Well?" and Jamie responds, "I think so... Yes." At this point the central conceit of the movie has become clear: Jamie came back specifically to help Nina get over him by tarnishing her idealised memory of him. 

==Cast==
* Juliet Stevenson as Nina
* Alan Rickman as Jamie
* Jenny Howe as Burge
* Carolyn Choa as Translator Bill Paterson as Sandy
* Christopher Rozycki as Titus
* Keith Bartlett as Plumber
* David Ryall as George
* Stella Maris as Maura
* Ian Hawkes as Harry
* Deborah Findlay as Claire
* Vania Vilers as Frenchman
* Arturo Venegas as Roberto
* Richard Syms as Symonds
* Michael Maloney as Mark

==Production== Italian word cielo for heaven. The film was made-for-TV, and produced in a 28-day shooting schedule for just $650,000. Anthony Minghella, Commentary with the MGM DVD release  

It was shot on location in London and Bristol, with external shots in Highgate and on the South Bank. Some of the film was also shot in Juliet Stevensons Highgate flat.  , Daily Telegraph 2 Mar 2012     Stevenson has stated in 2012 that it was the favourite role of her career, commenting that the shoot was like a party.  

==Reception== BAFTA for best original screenplay. (Alan Rickman and Juliet Stevenson received Best Actor and Best Actress, and Anthony Minghella Most Promising Newcomer, from the 1991 Evening Standard British Film Awards. )  It became a hit in the American arthouse circuit and Minghella subsequently was offered work by every major studio in Hollywood.  The movies combination of serious themes with comic scenes, music and strong performances from the actors was noted by critics. Roger Ebert called it "a "Ghost (1990 film)|Ghost" for grownups" (a common comparison because of the shared theme of lovers returning as ghosts and the concurrent releases of the movies) and considered the movie to reveal "some truths that are, the more you think about them, really pretty profound."  Review aggregator Rotten Tomatoes reports that 23 of 32 professional critics have given the film a positive review while 82% of the audience liked it.  At Metacritic the film has weighted mean score of 72 out of 100 based on 16 reviews. 

==Awards==
;Won
* Australian Film Institutes Best Foreign Film Award
* British Academy of Film and Television Arts|BAFTAs award for best original screenplay
* Rickman and Stevenson won Best Actor and Best Actress awards from the Evening Standard British Film Awards
* Minghella won Most Promising Newcomer from the Evening Standard British Film Awards
* Won Critics Award and Best Actress (Juliet Stevenson) at the 1992 Avoriaz International Fantastic Film Festival. 

;Nominated BAFTA

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 