Histoire vraie
{{Infobox Film
| name           = Histoire vraie
| image          = 
| image size     = 
| caption        = 
| director       = Claude Santelli
| producer       = 
| writer         = 
| narrator       = 
| starring       = Pierre Mondy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 5 May 1973
| runtime        = 65 minutes
| country        = France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}

Histoire vraie is a 1973 French film directed by Claude Santelli.   

==Cast==
* Pierre Mondy - Varnetot
* Marie-Christine Barrault - Rose
* Denise Gence - Mère Paumelle
* Claude Brosset - Le fils Paumelle
* Isabelle Huppert - Adelaïde
* Danielle Chinsky - Félicité (as Danièle Chinsky)
* Lucien Hubert - Déboultot
* Henri de Livry - Loncle
* Sylvie Herbert - La servante de loncle
* Fred Personne - Un convive
* Jean Puyberneau - Un convive
* Marcel Rouzé - Un convive

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 
 
 
 