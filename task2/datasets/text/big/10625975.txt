The Avenging Eagle
 
 
{{Infobox film name = The Avenging Eagle image = Avenging Eagle.jpg alt =  caption = Film poster traditional = 冷血十三鷹 simplified = 冷血十三鹰 pinyin = Léngxuě Shísān Yīng}} director = Sun Chung producer = Run Run Shaw writer = Ni Kuang starring = Ti Lung Alexander Fu Ku Feng Wang Lung-wei Eddy Ko Austin Wai music = Frankie Chan cinematography = Lam Ngai Kai editing = Chiang Hsing-lung Yu Siu-fung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime =  country = Hong Kong language = Mandarin budget =  gross =
}}
The Avenging Eagle is a 1978 Hong Kong wuxia film produced by the Shaw Brothers Studio, starring Ti Lung, Alexander Fu and Ku Feng. A remake of the film, titled The 13 Cold-Blooded Eagles, was released in 1993. 

This film is released in DVD by Dragon Dynasty in the United States. It was announced in July 2013 that there have been plans for a remake to start shooting in 2014 with collaboration between Celestial Pictures and The Weinstein Company.

== Plot ==
Eagle Chief Yoh Xi-Hung (Ku Feng) recruits and raises orphans to be his personal army of ruthless killers. One such orphan, indoctrinated by Yoh Xi-Hung, is Chik Ming-sing (Ti Lung). Chik soon becomes one of Chief Yohs top killers as he relishes engaging opponents in combat whenever the bandits go on a raid. However, after one particularly rough heist, Chik begins to have second thoughts about his criminal lifestyle. This is further complicated when he is taken in and nursed back to health from wounds received in battle by a kind family. Chik realizes that there is an alternative to his violent, murderous lifestyle. He learns that he can live a peaceful, nonviolent way of life with a wife and family if he so chooses.

Chik meets up with a stranger while on the run. The stranger, Cheuk Yi-fan (Alexander Fu), secretly carries hidden wrist knives and is obsessed with hunting down his enemies for revenge for the murder of his wife and family. Chik Ming-sing is suspicious of the mysterious Cheuk, even when Cheuk does not hesitate to help him kill the other Eagle Clan members who are chasing him for leaving the bandit clan.

Not knowing that Cheuk Yi-fan is the son of the family Chik and the other Eagles robbed and murdered, Chik Ming-sing confesses his evil doings to him one evening. Chik reveals that he has vowed to change his ways after that incident by killing Eagle Chief Yoh and the entire bandit clan. He also reveals that he is searching for the husband of the woman he killed so that he can die at his hands to atone for Chiks crimes.

Cheuk decides to accompany Chik to Eagle headquarters, but Eagle Chief Yoh recognizes Cheuk and reveals his true intentions to Chik. When Chik asks Cheuk why he did not take his revenge on Chik, Cheuk reveals that what he wanted was to know the whole story, and that he and Chik should join forces to defeat their common enemy Chief Yoh.

After attempting to manipulate both fighters to turn on one another, the two heroes decide to put aside their differences to finally end the murderous reign of the Eagle Clan. Once Chief Yoh is defeated, Chik tells Cheuk that he is ready to accept his punishment. Cheuk tells Chik that all his enemies are now dead and begins to walk away. Chik attacks Cheuk to force him to kill Chik. He tells Cheuk that if he doesnt get his revenge now, then the spirit of his pregnant wife and unborn child would never know peace.

The final scene of the film show Cheuk looking over the graves where his family is laid to rest. A new headstone can be seen where Chiks remains have also been placed at the burial site.

==Cast==
*Ti Lung as Black Eagle Chik Ming-sing
*Alexander Fu as Double Sleeve Knives Cheuk Yi-fan
*Ku Feng as Yoh Xi-hung
*Wang Lung-wei as Vulture Eagle Yien Lin
*Eddy Ko as Blue Eagle Wan Da
*Austin Wai as Owl Eagle Cao Gao-shing
*Bruce Tong as Eagle
*Lam Fai-wong as Eagle
*Dick Wei as Eagle
*Wong Pau-gei as Eagle
*Peter Chan as Soaring Eagle Wang Tao-sang
*Yuen Bun as Eagle Yau Koon-hung
*Chui Fat as Eagle
*Jamie Luk as Eagle Lin Gin-ming
*Cheung Gwok-wa as Eagle Fan Lun
*Shih Szu as Siu Fung
*Yue Wing as Devils Plight Wang An / Jiang Shun-kwai
*Tong Gai as Golden Spear Tao De-biu
*Yeung Chi-hing as Se-ma Sun
*Ou-yang Sha-fei as Se-ma Suns wife
*Jenny Tseng as Se-ma Yu-chin
*Yau Chui-ling as Se-ma Suns second daughter
*Kwok Wai-bing	 	 
*Au Ga-lai
*Lui Hung as Wang Ans mother
*Hung Ling-ling as Wang Ans wife
*Lee Hang
*Wong Chi-keung
*Law Keung as Hungs thug
*Wong Chi-ming as Hungs thug
*Man Man as inn waiter
*Gam Tin-chue as innkeeper

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 