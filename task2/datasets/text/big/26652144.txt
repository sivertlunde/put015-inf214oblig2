Amrapali (film)
 
{{Infobox film	
| name           = Amrapali
| image          = Amrapali.jpg
| alt            = 
| caption        = 
| director       = Lekh Tandon
| producer       = F.C. Mehra
| writer         = Story & screenplay: Omkar Sahib Dialogue: Arjun Dev Rashk Balbir Singh (Additional dialogue)
| starring       = Vyjayanthimala Sunil Dutt Prem Nath
| music          = Shankar-Jaikishan
| cinematography = Dwarka Divecha	 
| editing        = Pran Mehra
| studio         = Eagle Films  
| distributor    = 
| released       = 1966 
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 historical Hindi film, directed by Lekh Tandon, starring Vyjayanthimala and Sunil Dutt as leads. 
 Vaishali in Licchavi republic Magadha empire, Pali texts and Buddhist traditions.   
 Best Foreign Language Film at the 39th Academy Awards, but was not accepted as a nominee.  Though the film wasnt a commercial success, in time it started being seen as classic and is remembered not just for its dramatic cinematography of war scenes by Dwarka Divecha,  and Bhanu Athaiyas costumes for which she travelled to the Ajanta Caves, to seek references in Buddhist frescoes of the era, to create period costumes that subsequently became a template for costumes of that era, but also for the strong Anti-war sentiment, the film reveals in the end.   

==Cast==
* Vyjayanthimala  - Amrapali
* Sunil Dutt - Magadh Samrat Ajatashatru
* Prem Nath - Senpati Veer of Magadh
* Bipin Gupta - Vaishali (ancient city)|Vaishalis Samrat
* Gajanan Jagirdar - Kulpati Mahanam
* K.N. Singh - Balbadra SIngh Madhavi - Raj Nartaki
* Mridula Rani	- Raj Mata (Ajaat Shatrus Mother)
* Ruby Mayer - Vaishalis Empress
* Narendra Nath	- Lord Buddha
* Baburao Pendharkar - {   } Not known - he is there in the song Neel Gagan Ki - A Bald Person

==Crew==
* Art Direction: M.R. Acharekar Gopi Krishna 
* Costume Design: Bhanu Athaiya

==Music==
 
Another highlight of the film was its music by the duo Shankar Jaikishan, then at the peak of their career, who gave a highly restrained yet fully Indian classical music-based score in the four songs, another rarity in the period film of the era to have so few songs. All the song were sung by Lata Mangeshkar who also has some of her careers finest among them, including,  "Tumhen Yaad Karte Karte", "Neel Gagan Ki Chhaon Mein"  and  "Jao Re Jogi". 
===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jao Re"
| Lata Mangeshkar
|-
| 2
| "Tumhen Yaad Karte Karte"
| Lata Mangeshkar
|-
| 3
| "Neel Gagan Ki Chhaon Mein"
| Lata Mangeshkar
|-
| 4
| "Tadap Yeh Din Raat Ki"
| Lata Mangeshkar
|-
| 5
| "Nacho Gao Nacho Dhoom Machao"
| Lata Mangeshkar
|}

==In popular culture==
A scene clipping from the film, was used in the Dhoom tana song dance sequence in film Om Shanti Om (2007), wherein Deepika Padukone dances as Vyjayantimala, who was digitally removed from the frames, as Deepika was playing the role of an actor of the 1970s. 

==See also==
* List of historical drama films of Asia
* List of submissions to the 39th Academy Awards for Best Foreign Language Film
* List of Indian submissions for the Academy Award for Best Foreign Language Film
* Chitralekha (1964 film)|Chitralekha (1964)

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 