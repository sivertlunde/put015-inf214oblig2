The Middle Watch (1940 film)
{{Infobox film
| name           = The Middle Watch
| image          =
| image_size     =
| caption        =
| director       = Thomas Bentley 
| producer       = Walter C. Mycroft
| writer         = Ian Hay (play)   Stephen King-Hall (play)   Clifford Grey   J. Lee Thompson
| narrator       =
| starring       = Jack Buchanan   Greta Gynt   Fred Emney   Kay Walsh
| music          = 
| cinematography = Claude Friese-Greene
| editing        = Monica Kimick ABPC
| distributor    = Pathé Pictures International
| released       = 11 May 1940
| runtime        = 87 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1940 Cinema British comedy produced by adapted as a film in 1930.

==Synopsis== bon voyage party. He later discovers two stowaways, attractive young women, but much too late to turn back. Despite the captains efforts to hide them, they are soon discovered, with predictable consequences. 

==Cast==
* Jack Buchanan - Captain Maitland
* Greta Gynt Mary Carlton
* Fred Emney - Admiral Sir Reginald Hewett
* Kay Walsh - Fay Featon
* David Hutcheson - Commander Baddeley
* Leslie Fuller - Marine Ogg
* Bruce Seton - Captain Randall
* Martita Hunt - Lady Elizabeth Hewett
* Louise Hampton - Charlotte Hopkinson
* Romney Brent - Ah Fong
* Jean Gillie - Betty Hewettt
* Ronald Shiner - the ships mechanic or engineer
* Reginald Purdell - Corporal Duckett

==References==
 

==Bibliography==
* Mackenzie, S. P. British War Films, 1939-1945. Continuum, 2003.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 