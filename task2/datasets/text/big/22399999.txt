Catfish in Black Bean Sauce
{{Infobox Film
| name           = Catfish in Black Bean Sauce
| image          = Catfish in Black Bean Sauce DVD.jpg
| image_size     = 
| caption        = DVD cover art
| director       = Chi Muoi Lo
| producer       = Chi Muoi Lo
| writer         = Chi Muoi Lo
| starring       = Chi Muoi Lo Paul Winfield Sanaa Lathan Mary Alice
| music          = Stanley A. Smith	
| cinematography = Rudy M. Fenenga, Jr. Dean Lent
| editing        = Dawn Hoggatt
| distributor    = Black Hawk Entertainment
| released       = June 9, 2000
| runtime        = 119 minutes
| country        = United States
| language       = English
}}

Catfish in Black Bean Sauce is a 1999 comedy-drama film about a Vietnamese brother and sister raised by an African American couple. The film stars Chi Muoi Lo, Paul Winfield, Sanaa Lathan, and Mary Alice.

==Plot==
  Tyler Christopher), and his new transgender girlfriend (Wing Chen) waiting there. He quickly puts the ring away as he and Nina head to Dwayne’s parents’ house for a barbecue.

On the way over, Dwayne fails to stop for a stop sign and is pulled over by a cop who asks him to step out of the car. He spots the ring and tells Dwayne that Nina is way out of his league and to enjoy it while it lasts. This makes Dwayne question himself and his relationship with Nina. 
At the barbecue, the family has a debate over the difference between saying “this Saturday” and “next Saturday”. Dwayne, having had enough of the debate, proposes to Nina offhandedly. She says yes just as Mai (Lauren Tom) walks in. Mai lets the family in on a some news: shes located their birth mother, Thanh, and shes flying her to LA. Dolores takes the news hard and worries she’ll be replaced, running off to the bathroom as Nina follows behind. Harold is more optimistic, and Dwayne pretends to be indifferent.

There is a brief flashback to when Mai was young and she yells at Dolores saying “you’re not my mother” and runs to hug Harold. This particular scene makes it obvious that Mai and Dolores have never quite learned to get along. 
At the airport, Mai cries when she sees her mother, while Dwayne has no reaction. As they all sit down for a family dinner it is clear Dolores feels like she is being replaced. As everyone goes to leave, Thanh demands to go with Dwayne/Sap despite the extensive preparations Mai has made for her at her house. 

Thanh worries about Dwayne and doesn’t think Nina is the right girl for him (its unclear if this is because she’s black or because she’s gone a lot) and tells Mai. Nina goes to Mai’s for lunch one day and tells her that she ought to be more affectionate with Dwayne. Nina feels like she has been affectionate enough but Mai tells her she should try harder.
Harold’s birthday arrives and no one shows up for the party. Dolores has a fit and blames it on Thanh’s arrival Harold gets frustrated and has a heart attack.Mai rushes to the hospital to meet Dolores. The two have a fight. Dolores blames Mai saying “we’ve served our purpose you don’t need us anymore”. The doctor tells the two that Harold will be ok. Dolores tries to apologize but Mai leaves telling Dolores to spend time with Harold. 

The next day Dwayne calls to tell Dolores that he’s heard the news. Dolores is clearly upset that he wasn’t there and so is Dwayne. Wanting to be more affectionate, Nina prepares a big dinner for the two of them. Dwayne arrives and suggests the two break up. Finally, there is an all out war with everyone: Thanh and Dolores, Nina and Dwayne, Samantha and Michael ending when Michael’s girlfriend literally lifts Dolores and Thanh in the air to separate them. 

The next day Mai comes over to help with Harold’s food. Dolores says she doesn’t need help but Mai goes in to hug her; this is the first time there is a sense of understanding between the two. Mai realizes that Dolores has been more of a mother and treated her far better than Thanh. 
Over a game of monopoly Dwayne, Mai, Dolores, and Harold discuss the fact that Dwayne has broken up with Nina. Everyone thinks he’s stupid for doing it because they all know she loves him. Mai tells Dwayne that Nina has accepted a job in NY and is going to take it. Dwayne realizes he’s made a huge mistake and runs to go find her. Eventually he does and everyone finally gets along. The movie ends with a big family dinner over which Dolores and Thanh nod in understanding and play a game of Monopoly (game)|Monopoly together.

==Cast==
* Chi Muoi Lo as Dwayne Williams/Sap
* Sanaa Lathan as Nina
* Paul Winfield as Harold Williams
* Mary Alice as Dolores Williams
* Lauren Tom as Mai
* Kieu Chinh as Thanh Tyler Christopher as Michael
* Tzi Ma as Vinh George Wallace as James
* Wing Chen as Samantha
* Amy Tran as Young Mai
* Kevin Lo as Young Dwayne/Sap
* Kevin D’Arcy as Guard 1
* Andre Rosey Brown: Guard 2
* Ron Galbraith as Doctor
* Calvin Nguyen as Teacher
* Richard F. Whiten as Motorcycle Cop
* Lalanya Master as Bank Teller
* Mark Daniel Cade as Assistant Bank Manager
* Jedda Jones as Agnes
* Roxanne Reese as Nadine
* Saachiko Magwili as Mother
* William Thomas as Douglas
* April Tran as Interpreter
* Thomas Ryan as Lt. Davis
* Carol Kiernan as Nurse
* Thu Hong: Opera Singer
* Ho Lo as Man at Airport
* Vien Hong as Transvestite

==Reviews==
The film has received mixed reviews. A Los Angeles Times review noted that as the film’s writer/director/actor Chi Muoi Lo "spread himself too thin, resulting in an uneven picture but one that has plenty of substance and emotion".  Roger Ebert wrote that the film was "a first draft for a movie that could have been extraordinary".   The San Francisco Chronicle noted that the film was "a comedy of interracial wariness and misunderstanding marked by a refreshing lack of sappiness". 

==Awards and nominations==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#B0C4DE" align="center" Year
! Award
! Result
! Category
! Recipient
|-style="background-color: #EAEAEA;" First Americans in the Arts Awards || Won || Outstanding Performance by an Actor in a Supporting Role (Film) || Tyler Christopher 
|-style="background-color: #EAEAEA;" 1999 || Florida Film Festival || rowspan=2|Won || Best Narrative Feature || Chi Muoi Lo 
|-style="background-color: #EAEAEA;"
| Best Narrative Feature || Chi Muoi Lo 
|- Political Film Society || Nominated || Exposé ||  - 
|-style="background-color: #EAEAEA;"
| 1999 || WorldFest-Houston International Film Festival || Won || Theatrical Feature Films - 004 || Chi Muoi Lo 
|-
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 