Ten Little Indians (1965 film)
 
 
{{Infobox film
| name           = Ten Little Indians
| image          = TenLittleIndians1965Poster.jpg
| caption        = UK release poster George Pollock
| producer       = Harry Alan Towers
| writer         = Agatha Christie
| based on       = And Then There Were None 1939 Peter Welbeck Peter Yeldham Erich Kröhnke Enrique Llovet Fabian Leo Genn Stanley Holloway Wilfrid Hyde-White Daliah Lavi Dennis Price Marianne Hoppe Mario Adorf Christopher Lee  
| music          = Malcolm Lockyer
| cinematography = Ernest Steward
| editing        = Peter Boita
| studio         = Tenlit Films Seven Arts  
| released       = June 1965
| runtime        = 91 mins.
| country        = United Kingdom
| language       = English
}}
 detective novel of the same name. 
 And Then There Were None) with ten people invited to a remote location by a mysterious stranger, this one takes place on an isolated snowy mountain. The house used in the film was Kenure House in Rush, Dublin|Rush, County Dublin, Ireland. Looking at Hollywood: Fabian Heads for Dublin to Make Mystery Film
Hopper, Hedda. Chicago Tribune (1963-Current file)   24 Mar 1965: b5.  

This version is also the first adaptation of the novel to show the murders on screen. An uncredited Christopher Lee provides the pre-recorded voice of "Mr. U. N. Owen." 

==Alterations==
This adaptation has been retooled to fit the attitude of the "swinging sixties," such as changing the character of the repressed spinster into a glamorous movie star, adding a lot more action to complement the mystery, a fight scene and even a sex scene. Other changes include William Blore not faking his identity as Mr. Davis and essentially changing the backstory of most of the characters.

The ending was changed to a less pessimistic one, heavily borrowing from the upbeat finale Christie wrote for the stage version of the story, which was and remains completely at odds with the very downbeat ending of her original mystery thriller.

==Cast==
{| class="wikitable"
|-
|-
! Actor !! Character !! Occupation !! Killed !! Book character
|-
| Shirley Eaton || Ann Clyde || Secretary || Richard Barclay (sisters fiancee) || Vera Elizabeth Claythorne
|-
| Hugh OBrian || Hugh Lombard/Charles Morley || Engineer || Jennifer Hayes (lover)  || Philip Lombard
|-
| Stanley Holloway || William Blore || Detective || James Landor (perjured testimony) || William Henry Blore
|-
| Dennis Price || Dr. Edward Armstrong || Doctor || Ivy Benson (patient) || Dr. Edward George Armstrong
|-
| Wilfrid Hyde-White || Arthur Cannon || Judge || Edward Seton (defendant in a trial) || Lawrence John Wargrave
|-
| Daliah Lavi || Ilona Bergen || Actress || Mr. Bergen (husband) || Emily Caroline Brent
|-
| Leo Genn || Sir John Mandrake V.C. || General || Five soldiers (subordinates) || John Gordon Macarthur
|- Fabian || Michael "Mike" Raven || Entertainer || William and Liza Stern (car accident) || Anthony James "Tony" Marston
|-
| Marianne Hoppe || Elsa Grohmann || Cook || Countess Valenstein (employer) || Ethel Rogers
|-
| Mario Adorf || Joseph Grohmann || Butler || Countess Valenstein (employer) || Thomas Rogers
|-
| Christopher Lee || Voice of "Mr. U.N. Owen" (uncredited) || N/A || N/A || Ulick Norman Owen
|}

==Later film versions== And Then There Were None (1974 version)
* Desyat Negrityat (1987 Soviet adaptation) Ten Little Indians (1989 version)

==References==
 	

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 