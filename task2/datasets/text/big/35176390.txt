The Whisperer in Darkness (film)
{{Infobox film
| name = The Whisperer in Darkness
| image = Whisperer-poster.jpg
| caption = Film poster
| director = Sean Branney David Robertson
| writer = Sean Branney Andrew Leman
| music = Troy Sterling Nies
| editing = David Robertson
| distributor = H. P. Lovecraft Historical Society
| country = United States
| language = English
| runtime = 104 minutes
| released =  
}} of the King Kong".   

==Plot== two acts, the plot essentially follows the lines of the short story. However, in the third act of the film, the protagonist, Wilmarth, uncovers an attempt by collaborators (or cultists) to open a gateway between Yuggoth and Earth. He foils the plot with the help of an added character, Hannah, the child of one of the collaborators. His escape, however, is unsuccessful and at the end of the film the audience discovers that Wilmarth has been narrating from a machine attached to the cylinder in which his brain now resides. This differs from the original story in which Wilmarth flees in the middle of the night and safely returns to Arkham.
 Call of Cthulhu role-playing characters created years before by Branney, Leman, and a friend. Regarding the introduction of a biplane, Leman commented "If you have monsters that fly, you have to have a dogfight with a biplane." 

==Cast==
In order of appearance: 

* Paul Ita as Farmer
* Matt Foyer as Albert Wilmarth 
* Matt Lagan as Nathaniel Ward
* Lance J. Holt as Davis Bradbury
* Andrew Leman as Charles Fort
* Stephen Blackehart as Charlie Tower
* David Pavao as Jordan Lowell
* Don Martin as Dean Hayes
* Joe Sofranko as George Akeley
* Barry Lynch as Henry Akeley
* Martin Wately as Walter Brown
* Daniel Kaemon as P.F. Noyes
* Caspar Marsh as Will Masterson
* Autumn Wendel as Hannah Masterson
* Sean Branney as B-67
* Annie Abrams as Aviatrix
* Zack Gold as Astronomer
* John Jabaley as Superintendent

==Release==
The Whisperer in Darkness did not have a theatrical release but appeared at dozens of film festivals in over a dozen countries.  It was then released on DVD and Blu-ray in early 2012.

==Reception==
The Whisperer in Darkness received highly positive reviews. It holds 80% approval rating on Rotten Tomatoes. On the Internet Movie Database it has 6.7 out of 10 stars. John J. Puccio of Movie Metropolis said "The atmospherics are in place, and the filmmakers catch the essence of Lovecrafts expansive horror with efficiency. The film is entertaining without attaining greatness." Andrew OHehir of Salon (website)|Salon.com said "Whisperer in Darkness has a chiller-diller conclusion and some moments of real terror."

==Awards==

The Whisperer in Darkness was nominated at Oaxaca Film Fest.

* Gold Hugo—Nominated
* Free Spirit Award—Nominated

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 