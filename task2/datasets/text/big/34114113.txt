Une chance sur deux
{{Infobox film
| name           = Une chance sur deux
| image_size     =
| image	         =	
| caption        =
| director       = Patrice Leconte
| producer       = Christian Fechner
| writer         = Patrick Dewolf, Serge Frydman and Patrice Leconte
| narrator       =
| starring       = Jean-Paul Belmondo, Alain Delon, Vanessa Paradis
| music          = Alexandre Desplat
| cinematography = Steven Poster
| editing        = Joëlle Hache
| distributor    = UGC Fox Distribution
| released       = 1998
| runtime        = 110 min
| country        = France
| language       = French
| budget         =
| gross          =1,056,810 admissions (France) 
| preceded_by    =
| followed_by    =
| website        =
}}

Une chance sur deux is a French film directed by Patrice Leconte, released in 1998 in film|1998, and starring Jean-Paul Belmondo, Alain Delon and Vanessa Paradis.

== Synopsis ==
Alice (Vanessa Paradis) leaves prison after having served an eight month sentence for car theft. Her mother, who has just died, leaves her a cassette on which she admits to the mystery of her birth. Alice has never known her father. Twenty years before, her mother had loved two men (Belmondo, Delon). One of them is, unknowingly, her father. Alice goes off to find the two, but before discovering which is her father she gets them involved in an adventure.

==Production==
* Director : Patrice Leconte
* Screenplay : Patrick Dewolf, Serge Frydman and Patrice Leconte
* From a story by Bruno Tardon
* Production : Christian Fechner
**  
* Company : Les Films Christian Fechner, TF1 Films Production
* Original music : Alexandre Desplat
* Photography : Steven Poster
* Editing : Joëlle Hache
* Art direction : Ivan Maussion
* Costumes : Annie Périer
* Country : France Action
* Length : 110 minutes
* Distributor : UGC Fox Distribution
* Release dates : 
** France : 25 March 1998
** Belgium : 1 April 1998

== Cast ==
* Jean-Paul Belmondo : Léo Brassac
* Alain Delon : Julien Vignal
* Vanessa Paradis : Alice Tomaso
* Michel Aumont : Ledoyen
* Eric Defosse : Carella Aleksandr Yakovlev : Trenchcoat killer
* Valeri Gatayev : Anatoli Sharkov
* Sandrine Caron : Maître dhôtel
* Véronique Hubert
* Luis Jaime-Cortez : Ángel Vargas
* Mbembo : Maryline
* Philippe Magnan : Juge prison
* Daniel Millot : Brigadier
* Marie Neplaz 
* Bobby Pacha
* Olivier Parenty : Adjoint Carella
* Pascal Perbost
* Paul Poggi : Monsieur Perolaz
* Guillaume Rannou
* Vincent Roger : Le photographe
* Jacques Roman : Maitre Varinot
* Paola Sapone
* Vincent Skimenti : Paco
* Philippe Vieux

== References ==
 
== External links ==
*  

 
 

 
 
 
 
 
 
 

 