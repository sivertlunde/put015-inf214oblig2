A Birder's Guide to Everything
 
{{Infobox film
| name = A Birders Guide to Everything
| director = Rob Meyer
| writer = Rob Meyer Luke Matheny
| starring = Kodi Smit-McPhee Alex Wolff Katie Chang Focus World Screen Media Films
| released =  
}}

A Birders Guide to Everything is an independent film starring Kodi Smit-McPhee, Alex Wolff, Michael Chen, Katie Chang, James Le Gros, Daniela Lavender and Sir Ben Kingsley.  It was written by Rob Meyer and Luke Matheny and directed by Rob Meyer. 

==Plot==
The film follows the story of teenage birders who go on a road trip to find the (possibly) extinct Labrador Duck. It was based on Rob Meyers short film "Aquarium" which won an Honorable Mention at Sundance in 2008. 

==Release== Screen Media.The film was released onto video on demand and select theaters on March 21, 2014.

==Reception==
The film opened to very positive reviews.  The New York Times described it as a "smart, likeable, coming of age film   an eye opener for anyone who takes the everyday natural world for granted."  USA Today wrote that "not since Rob Reiners Stand by Me has such a compelling rite-of-passage film emerged."  and The Guardian wrote that "you dont have to be a birder to enjoy it. The movie shows that seeking the rare and elusive is often more than just a physical quest; it also is a spiritual journey that changes the seeker." 

The film also won Best American Independent Feature Film at the Cleveland Film Festival and Best Feature at the San Luis Obispo International Film Festival. 

==References==
 

==External links==
*  
*  

 
 