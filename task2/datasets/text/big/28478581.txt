Burning Flowers
 

{{Infobox film
| name           = Burning Flowers
| image          = 
| image size     =
| caption        = 
| director       = Eva Dahr   Eva Isaksen
| producer       = 
| writer         = Lars Saabye Christensen
| narrator       =
| starring       = Torstein Hølmebakk   Lise Fjeldstad 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1985
| runtime        = 84 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama film directed by Eva Dahr and Eva Isaksen, based on a story by Lars Saabye Christensen, and starring Torstein Hølmebakk and Lise Fjeldstad. The film is about the teenager Hermann (Hølmebakk) who develops a relationship with the middle-aged Rosa (Fjeldstad).

==External links==
*  
*  

 
 
 


 