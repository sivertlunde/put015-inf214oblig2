The NeverEnding Story III
{{Infobox film
| name           = The NeverEnding Story III: Escape from Fantasia
| image          = Tnes3.jpg
| image_size     = 
| caption        = Canadian video release poster Peter MacDonald
| producer       = Heinz Bibo Dieter Geissler Tim Hampton Klaus Kaehler Harry Nap Harold Lee Tichenor
| based on       =  
| story          = Karin Howard
| screenplay     = Jeff Lieberman Jack Black Freddie Jones Julie Cox Tony Robinson Peter Wolf
| cinematography = Robin Vidgeon
| editing        = Michael Bradsell Jim Roddan
| distributor    = Miramax Films   Warner Bros. Family Entertainment  
| released       =  
| runtime        = 95 minutes
| country        = Germany United States
| language       = English
| budget         = $17 million 
| gross          = 
}}
 Jack Black in one of his early roles as the protagonists tormentor, the school bully Slip. The special creature effects were provided by Jim Hensons Creature Shop. The movie is considered apocryphal and has no connection to the source material of the book beyond the usage of characters from it.

==Plot==
In a prologue, the Old Man of Wandering Mountain (Freddie Jones) reads from a large book; begins to record a prophecy of a day when The Nasty will arrive in Fantasia; and describes the savior of "Extraordinary Courage".
 Childlike Empress (Julie Cox), who asks Bastian to find the Neverending Story with AURYN. Falkor, Barky, the gnomes, and the Rock Biters son, Junior, help him; but a "wish overload" scatters across Earth, where Barky ends up in a conifer forest; Falkor saves Junior from falling to his death near Mount Rushmore; and the gnomes arrive in Nome, Alaska. Bastian locates Falkor and Junior, and Falkor flies off to find the others while Junior stays at Bastians house. Rock Biter sadly informs his wife that Junior is gone, and the Nasties provoke them to quarrel. 

Nicole takes AURYN from Bastians room, discovers its wishing abilities, and takes it for a shopping trip to the local mall. Bark Troll arrives at Bastians house disguised as a garden plant, while the Gnomes are mailed to him in a box. The reunited group go in search of Nicole; but the Nasties find AURYN first, whereupon giant crustacean creatures appear in Fantasia to kill the Empress and her advisors. Everyone in the mall turns to evil, including Mr. Koreander and Bastians parents. Bastian is struck by lightning, and begins to succumb to the wills of the Nasties; but Nicole saves him, and Bastian recovers AURYN and the book in a fight. The Fantasians return to Fantasia, which is restored to its former magnificence; Bastian and Nicole manage to keep their parents from divorcing; and Junior is reunited with his parents. Nicole and Bastian return to school the next day and find that Bastian has changed Slip and the Nasties into friendly classmates, and Bastian returns the Neverending Story to Mr. Koreander.

==Cast==
* Jason James Richter as Bastian Balthazar Bux
* Melody Kay as Nicole Baxter, Bastians stepsister.
* Jack Black as Slip ("The Nasty"), the leader of the Nasties
* Freddie Jones as Koreander, a former local librarian 
* Julie Cox as The Childlike Empress
* Moya Brady as Urgl
* Tony Robinson as Engywook
* Tracey Ellis as Jane Bux, Bastians stepmother  Kevin McNulty as Barney Bux, Bastians father
* Frederick Warder as Rock Biter
* William Todd-Jones as Mrs. Rock Biter
* Dave Forman as Rock Biter Jr.
* Gordon Robertson as Falkor
* Kaefan Shaw as Bark Troll
* Mark Acheson as the Janitor

===Voices===
* William Hootkins as Bark Troll, Falkor
* Mac McDonald as Mrs. Rock Biter
* Gary Martin as Rock Biter, Rock Biter Jr.

==Reception==
The film received overwhelmingly negative reviews by critics and is considered one of the worst sequels in cinema history. Critics and viewers have criticized the poorly-written story, the absence of Atreyu (character)|Atreyu, the dumbing-down of beloved characters Falkor and the Rock Biter, and the large number of continuity errors. Variety (magazine)|Variety stated: "The NeverEnding Story lives up to its title in the worst way possible with this third outing, a charmless, desperate reworking of the franchise that might just as well be subtitled Bastian Goes to High School."   

==Box Office==
In the United States, The NeverEnding Story III was released theatrically as a trial run in a few markets to test its potential nationwide. Its grosses werent released to the public, but according to Box Office Mojo, its possible it didnt reach the five-digit mark. 

By late December 1994, the film grossed $5 million in Germany. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 