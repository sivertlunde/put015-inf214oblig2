Calling the Ghosts
__NOTOC__
{{Infobox film
| name           = Calling the Ghosts
| image          =
| image_size     =
| caption        =
| director       = {{Plainlist|
* Mandy Jacobson
* Karmen Jelincic
}}
| producer       = {{Plainlist|
*  
* Mandy Jacobson
}}
| writer         = Christopher Grimm
| narrator       =
| starring       = {{Plainlist|
* Nusreta Sivac
* Jadranka Cigelj
}}
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       =
| budget         =
| gross          =
}}
Calling the Ghosts: A Story about Rape, War and Women is a 1997 documentary film that details the experience of Nusreta Sivac and Jadranka Cigelj at the Bosnian Serb-run Omarska camp in Bosnia and Herzegovina during the Bosnian War.  The films premiere was sponsored by Amnesty International, the Coalition for International Justice, the Center for Human Rights and Humanitarian law, and the Bosnian branch of Women for Women International. 

==See also==
* Rape in the Bosnian War

==Notes==
 

==References==
* {{cite news
  | last = Goodman
  | first = Walter
  | date = 3 March 1997
  | title = Women as Victims of the Bosnian War
  | publisher = New York Times
  | url = http://www.nytimes.com/1997/03/03/arts/women-as-victims-of-the-bosnian-war.html
  | ref =  
  }}
* {{cite journal
  | last = Purohit
  | first = Rajeev
  | title = Film Chronicles Suffering at Omarska Concentration Camp
  | journal = Human Rights Brief
  | year = 1997
  | volume = 4
  | number = 2
  | pages = 4–5
  | url = http://digitalcommons.wcl.american.edu/cgi/viewcontent.cgi?article=1651&context=hrbrief
  | ref = harv
  }}
* {{cite news
  | last = Ratner
  | first = Hannah 
  | date = 20 November 2011
  | title = "Calling the Ghosts" and the Continued Fight to End Violence Against Women
  | publisher = United Nations Entity for Gender Equality and the Empowerment of Women
  | url = http://unwomen-metrony.org/programs-events/past-events/2011-events/vent-summary-calling-the-ghosts-and-the-continued-fight-to-end-violence-against-women/
  | ref =  
  }}

==External links==
*  
* {{cite web
  | title = Calling the Ghosts: A Story About Rape, War and Women
  | publisher = Women Make Movies
  | url = http://www.wmm.com/filmcatalog/pages/c171.shtml
  }}

 
 
 
 
 
 
 
 
 


 