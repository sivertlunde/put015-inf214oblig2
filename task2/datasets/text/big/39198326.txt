The Look of Love (film)
 
{{Infobox film
| name = The Look of Love
| image = The Look of Love movie poster.jpg
| director = Michael Winterbottom
| writer = Matt Greenhalgh
| starring = Steve Coogan Imogen Poots Anna Friel Tamsin Egerton
| music = Antony Genn Martin Slattery
| cinematography = Hubert Taczanowski
| editing = Mags Arnold Film4 Baby Cow Productions Revolution Films
| distributor = StudioCanal UK
| released =  
| runtime = 101 minutes
| country = United Kingdom
| language = English
}} Paul Raymond, directed by Michael Winterbottom. It stars comedian Steve Coogan as Raymond.

==Plot==

The story opens in London in 1992. Paul Raymond (Steve Coogan) returns to his flat after attending the funeral of his daughter Debbie (Imogen Poots). Raymond plays a videotape of a TV programme he and Debbie took part in and reflects on their lives.

In a flashback to the late 1950s, Raymond is an impresario on the seaside variety show circuit, where he is making a name for himself by adding semi-nude women to his stage acts. After a lion attacks the shows dancers, his wife Jean (Anna Friel) joins the show. When the Daily Sketch claims that Jean performed nude, Raymond sues the newspaper unsuccessfully but appreciates the ensuing publicity, after which Raymond launches his London strip club, the Raymond Revue Bar. Its success allows him to expand his property empire and also indulge in a playboy lifestyle, which his wife tolerates.

In the early 1970s, Raymond moves into theatrical revues and casts aspiring actress Amber St. George (Tamsin Egerton) in a nude revue. Raymond moves in with her and his marriage to Jean ends. Raymond also agrees to meet a grown son, Derry, he sired out of wedlock, but after an awkward dinner together, he gives Derry no more of his time.

Raymond is approached by Tony Power to fund a mens magazine, Men Only. The magazine is a huge success, in part thanks to roving sex reporter Fiona Richmond, St. Georges pseudonym. Raymond continues to enjoy a hedonistic, Cocaine|coke-fuelled lifestyle. This becomes too much for St. George and their relationship ends.

Into this mix, his daughter Debbie is introduced. Initially, Raymond tries to make her a star in his theatrical ventures, but she lacks talent and the show is an unprecedented failure for him. Tony Power introduces her to cocaine, which leads to a downward spiral of substance abuse, in part aided by her father.

Debbie marries musician Jonathan Hodge. Jean returns for the wedding and volunteers to pose nude for Raymonds magazine. In the delivery room, Debbie gives birth to a girl after sniffing a line of coke that her father provides. She dies in 1992 of a heroin overdose. 

After the funeral, Raymond returns home with his granddaughter, pointing out the property he owns that will someday belong to her. An epilogue reveals that in December 1992, he was the richest man in Britain.

==Cast==
  Paul Raymond
*Imogen Poots as Debbie Raymond
*Anna Friel as Jean Raymond
*Tamsin Egerton as Amber St. George, aka Fiona Richmond
*David Walliams as Vicar Edwyn Young
*Chris Addison as Tony Power
*Shirley Henderson as Rusty Humphries
*James Lance as Carl Snitcher
*Paul Popplewell as Journalist
*Sarah Solemani as Anna
*Vera Filatova as Monika Matthew Beard as Howard Raymond
*Simon Bird as Jonathan Hodge
*Kieran OBrien as Jimmy Humphries
*Matt Lucas as Divine
*Stephen Fry as Barrister
*Miles Jupp as Interviewer Peter Wight as Police Inspector Liam Boyle as Derry
 

==Production history==
The Look of Love was originally called The King of Soho until that title had to be dropped due to a legal dispute. 

==Critical reception==
The film received mixed reviews from critics. It holds a 55 % approval rating on aggregate review site   score of 57 out of 100, based on 20 sampled reviews, indicating "mixed to average reviews."  Imogen Poots was featured on the inaugural Guardian Film Awards longlist, nominated for Best Actor 

==References and notes==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 