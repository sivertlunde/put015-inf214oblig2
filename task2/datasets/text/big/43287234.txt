My Little Pony: Equestria Girls – Rainbow Rocks
{{Infobox film
| name           = My Little Pony: Equestria Girls – Rainbow Rocks
| image          = Rainbow Rocks Poster 2.jpg
| caption        = Theatrical release poster
| director       = Jayson Thiessen
| producer       = Sarah Wall Devon Cody
| writer         = Meghan McCarthy
| based on       =   My Little Pony toyline by Bonnie Zacherle
| starring       = {{Plainlist|
* Tara Strong Ashleigh Ball
* Andrea Libman
* Tabitha St. Germain
* Rebecca Shoichet
* Cathy Weseluck
*  
* Marÿke Hendrikse
* Diana Kaarina
}}
| music          = William Anderson  (BGM)  Daniel Ingram  (songs)  
| editing        = Rachel Kenzie DHX Media/Vancouver Shout Factory (DVD) Discovery Family (TV premiere)
| released       =  
| runtime        = 75 minutes  
| country        = Canada United States
| language       = English
}}
My Little Pony: Equestria Girls – Rainbow Rocks is a  , and a   animated television series. Written by Meghan McCarthy and directed by Jayson Thiessen, the film premiered in select movie theaters across the United States and Canada on September 27, 2014, which was followed by broadcast on Discovery Family, a joint venture between Discovery Communications and Hasbro, on October 17, 2014.

The film re-envisions the main characters of the series, normally ponies, as human characters in a high school setting. The films plot involves Twilight Sparkles friends and Sunset Shimmer from Canterlot High in need of pony Twilights help.

Critical reviews have been positive, with the movie being described as "far superior" to the first installment.      

==Plot==
In the human world, the students of Canterlot High School are preparing for an upcoming musical showcase, with  s human friends to perform in their own band, the Rainbooms, at the event. While practicing, they find that they assume half-pony forms whenever they play, caused by the Equestrian magic left over from Twilights crown. Meanwhile, former school bully   is ostracized by the rest of the student body despite her attempts to redeem herself, though Twilights friends still value their newfound friendship with her. Sunset volunteers to show three new students—Adagio Dazzle, Aria Blaze, and Sonata Dusk—around the school. The trio, calling themselves  , perplex Sunset and the Rainbooms when they sing a song that creates dissent amongst the rest of the student body and convinces them to turn the showcase into a   and Vice Principal   under their spell, Sunset writes to Twilight for help through a magic book given to her by Princess Celestia.

Twilight receives Sunsets message while settling into her new castle in Equestria (after the events of "  and reunites with her human friends. She suggests that they use the power of friendship to counter the Dazzlings spell, but they are unable to evoke their magic. With help from Sunset Shimmer, Twilight determines that they must perform a song together as to cast their counterspell;   puts Twilight as the bands lead singer and Twilight begins to work out lyrics for the counterspell, finding it to be far more difficult than she originally thought. The Dazzlings are alerted to the magic protecting the Rainbooms and set the rest of the student body against the band, creating strife within the group and increasing their vulnerability to the trios spell.
 tone deaf. The Dazzlings run off in defeat while the rest of the school celebrates. Rainbow Dash offers Sunset a place in the band as a backing vocalist to replace Twilight. In response Sunset reveals that she can also play guitar. Twilight and Spike return to Equestria with the ability to return at any time, keeping in touch with Sunset via her magic book.
 montage that plays during the closing credits shows Sunset being accepted by the student body and performing as the Rainbooms newest singer and guitarist. In a post-credits scene, the human worlds version of Twilight Sparkle is shown investigating the strange activity around Canterlot High. 

==Cast==
  alicorn and one of the crowned princesses of Equestria. She returns to the human world and serves as the temporary lead vocalist of her human friends band, the Rainbooms. The human worlds version of Twilight makes a cameo appearance in a post-credits scene.
* Tabitha St. Germain as Rarity, a dressmaker unicorn. Her human counterpart is the Rainbooms keytarist.
** St. Germain also voices Vice Principal Luna, the vice principal of Canterlot High and human counterpart of Princess Luna. Ashleigh Ball as Rainbow Dash, a tomboyish and competitive Pegasus pony. Her human counterpart is the Rainbooms band Guitar|guitarist.
** Ball also voices Applejack, a hard-working farm pony. Her human counterpart is the Rainbooms Bass guitar|bassist.
* Andrea Libman as Pinkie Pie, a hyper, fun-loving party pony. Her human counterpart serves as the Rainbooms Drum kit|drummer.
** Libman also voices Fluttershy, a timid yet kindhearted Pegasus with a love for animals. Her human counterpart plays the tambourine on the Rainbooms.
* Cathy Weseluck as Spike, Twilights young dragon assistant, who journeys with her into the human world as a dog. His ordinary dog counterpart in the human world makes a cameo appearance in the films post-credits scene.
*   and befriends the Rainbooms.
* Vincent Tong as Flash Sentry, a guitar player and Sunset Shimmers ex-boyfriend who is infatuated with Twilight Sparkle.
* Nicole Oliver as Principal Celestia, the principal of Canterlot High and human counterpart of Princess Celestia. sirens that aims to  control Canterlot High through their singing.
* Maryke Hendrikse|Marÿke Hendrikse as Sonata Dusk,  |publisher=Twitter|work=(Self-published)|last=Rudell|first=Ishi|date=2014-09-08|accessdate=2014-09-08}}  the air-headed member of the Dazzlings.
* Diana Kaarina as Aria Blaze,  the abrasive member of the Dazzlings. magician who is the guitarist of her own band, the Illusions.

The film features minor appearances from Michelle Creber as the human version of Apple Bloom; Ingrid Nilson as the human version of Maud Pie, Pinkie Pies sister; Peter New as the human version of Big McIntosh; Lee Tockar and Richard Ian Cox as the human versions of Snips and Snails; Brian Drummond as an unnamed delivery pony in Equestria; and St. Germain as the human version of Photo Finish. Shoichet, Evans, and Shannon Chan-Kent also provide the singing voices of Twilight Sparkle, Rarity, and Pinkie Pie, respectively, while Sonata Dusk and Aria Blazes singing voices are performed by Madeline Merlo and Shylo Sharity. Evans also provides one line of dialogue as Octavia Melody. 

==Songs==
Like in Equestria Girls, the songs were composed by Daniel Ingram with lyric writing shared between Ingram and screenwriter Meghan McCarthy; with the exception of "Rainbow Rocks" and "Shine Like Rainbows" which had lyrics by Ingram; "Bad Counter Spell" and "Under Our Spell" by McCarthy; and "Shake Your Tail" having a first draft by writer Amy Keating Rogers, in which Ingram later revised.  Trevor Hoffman provided the arrangements for the songs, and musician Caleb Chan produced the songs as well as played guitar and bass.   
* Rainbow Rocks – The Rainbooms
* Better Than Ever – The Rainbooms sans Twilight Sparkle
* Battle  – The Dazzlings and students
* Bad Counter Spell – The Rainbooms
* Shake Your Tail – The Rainbooms
* Under Our Spell – The Dazzlings
* Tricks Up My Sleeve – Trixie and the Illusions
* Awesome as I Wanna Be – Rainbow Dash and the Rainbooms
* Welcome to the Show – The Dazzlings, the Rainbooms, Sunset Shimmer, and students
* Rainbooms Battle – Instrumental
* Shine Like Rainbows – The cast

==Production==
The sequel was announced by Hasbro as part of a press kit at the 2014 International Toy Fair.  On February 13, 2014, Meghan McCarthy tweeted that she had worked on the film during the summer of 2013.  That same day, songwriter Daniel Ingram tweeted that there will be a total of 12 songs in the film, the greatest number of songs featured in a Friendship Is Magic episode or Equestria Girls film thus far;  however, only 11 songs made it in the film. The films opening credits were storyboarded by Tony Cliff.  The storybook illustration depictions of the sirens in Equestria were done by Rebecca Dart.    The illustrations shown during the ending credits were drawn by Katrina Hadley. 

In the audio commentary of the DVD, Equestria Girls, at first, had no intention of becoming a franchise and the thought of a sequel did not cross Meghan McCarthys mind. Additionally, Sunset did not become a main character until the second draft of the script.  The DVD commentary also points out that the midnight snack scene between Twilight and Sunset was actually added late into the films production, Raritys line "past...ahem...booboos" was Ad libitum|ad-libbed by Germain, and the freestyle rap by Snips and Snails human counterparts was genuine freestyling courtesy of Lee Tockar and Richard Cox. 

==Release== Academy Award nominee Quvenzhané Wallis.  On July 24, 2014, a movie clip and a teaser trailer were released on Yahoo!|Yahoo! TV.  On September 10, 2014, through Equestria Daily, Shout! Factory revealed a 30-second trailer.  Two days later on September 12, a 50-second trailer was released on YouTube.  Six days later on September 18, a full theatrical trailer was released also via Yahoo! TV;  the trailer was then uploaded to Hasbros YouTube channel four days later on September 22. 

===International===
My Little Pony: Equestria Girls – Rainbow Rocks was theatrically released on October 25 and 26, 2014 in the United Kingdom.  The film was theatrically released on November 15, 2014, in Australia. 

===Home media=== Region 1. Target edition includes a bracelet, a $4 coupon for one MLP toy, and the original Equestria Girls DVD. The Walmart|Wal-Mart edition includes a music CD, a digital copy of the film, and the same $4 coupon. The Kmart|K-Mart edition includes a "backstage pass" and the coupon.  Upon initial release, the DVD version of the film had an error in which chapters 5 and 6 play in reverse when the DVD is played from the main menu "Play" listing. 

A Region 2 Rainbow Rocks DVD from distributor Primal Screen Entertainment was released on March 23, 2015, for various countries, including France, Germany, Italy, the Middle East region, Netherlands, and United Kingdom. 

===Television=== POP on December 24, 2014.

==Promotion==
===Live-action music videos===
On February 20, 2014, Hasbro released a live-action music video on its official website depicting the Equestria Girls in a rock band. Like the Magic of Friendship music video released to tie-in with the first movie, the Rainbow Rocks music video uses a rock version of the "Equestria Girls" song and portrays the main characters human counterparts performing the "EG Stomp".  Through the Equestria Girls YouTube channel, another music video was released on August 4, 2014. It depicts four more teenage girls, each one dressed as the characters DJ Pon-3, Adagio Dazzle, Aria Blaze, and Sonata Dusk respectively. 

===Mini-game===
A Rainbow Rocks missile command-type mini-game was added to the Hasbro Arcade mobile app on April 8, 2014. On October 29, 2014, the mini-game was updated to include the Dazzlings with two songs from the movie and one song from one of the live-action music videos. 

===Novels===
Perdita Finn wrote an adaptation of the Rainbow Rocks animated shorts under the same name; it was released on April 8, 2014.  A follow-up to the novelization, titled The Mane Event and also penned by Finn, was released on October 7, 2014. 

===Online===
====Prequel shorts====
  Hasbro Studios YouTube channel in 2014 from March 27 to June 19 to promote the film.  |publisher=Twitter|work=(Self-published)|date=2014-03-27|accessdate=2014-05-05}}  The shorts were created by the television series crew to tie into the film, and are considered prequels to the films events, detailing how each of them discovered that they could awaken Equestrian magic within them by playing their respective instruments. 

The shorts will have their television debut on Discovery Family on May 30, 2015. 

{| class="wikitable plainrowheaders" style="width:100%; margin:auto;"
|- style="color:white"
! style="background:#992699; color:white;" | No.
! style="background:#992699; color:white;" | Title
! style="background:#992699; color:white;" | Directed by
! style="background:#992699; color:white;" | Written by
! style="background:#992699; color:white;" | Original airdate List of My Little Pony: Friendship Is Magic episodes
| RowColor = Y
| Title = Music to My Ears
| DirectedBy = Ishi Rudell
| WrittenBy = Cindy Morrow
| OriginalAirDate = March 27, 2014
| EpisodeNumber = 1
| ShortSummary = DJ Pon-3 struts to her own beat on her way to Canterlot High, only to have her headphones confiscated by Principal Celestia when she gets there.
| LineColor = 992699
}} List of My Little Pony: Friendship Is Magic episodes
| RowColor = N
| Title = Guitar Centered
| DirectedBy = Ishi Rudell
| WrittenBy = Amy Keating Rogers
| OriginalAirDate = April 4, 2014
| EpisodeNumber = 2
| ShortSummary = Rainbow Dash and Trixie get into an argument over an electric guitar in a music store, and face each other in a "shred-off". Though Rainbow wins, she decides to keep the guitar that won her the shred-off and leaves Trixie with an unaffordable guitar as her prize.
| LineColor = 992699
}} List of My Little Pony: Friendship Is Magic episodes
| RowColor = Y
| Title = Hamstocalypse Now
| DirectedBy = Ishi Rudell
| WrittenBy = Josh Haber
| OriginalAirDate = April 11, 2014
| EpisodeNumber = 3
| ShortSummary = Fluttershy and Rarity get into trouble when hamsters run loose while they try to clean their home at the animal shelter. Fluttershy manages to corral the hamsters by hypnotizing them with her tambourine music.
| LineColor = 992699
}} List of My Little Pony: Friendship Is Magic episodes
| RowColor = N
| Title = Pinkie on the One
| DirectedBy = Ishi Rudell
| WrittenBy = Josh Haber
| OriginalAirDate = April 25, 2014
| EpisodeNumber = 4
| ShortSummary = Annoyed by Pinkie Pies constant drumming antics, Rainbow Dash finds an outlet for Pinkies energy by making her their bands drummer.
| LineColor = 992699
}} List of My Little Pony: Friendship Is Magic episodes
| RowColor = Y
| Title = Player Piano
| DirectedBy = Ishi Rudell
| WrittenBy = Amy Keating Rogers
| OriginalAirDate = May 9, 2014
| EpisodeNumber = 5
| ShortSummary = Rarity turns to the Diamond Dog boys for help moving her grand piano across campus in time for band practice with her friends. After she arrives late, she agrees to take up playing the more portable keytar instead.
| LineColor = 992699
}} List of My Little Pony: Friendship Is Magic episodes
| RowColor = N
| Title = A Case for the Bass
| DirectedBy = Ishi Rudell
| WrittenBy = Natasha Levinger
| OriginalAirDate = May 23, 2014
| EpisodeNumber = 6
| ShortSummary = Applejack tries to buy back her bass guitar after Granny Smith accidentally sells it to the Flim Flam brothers. When the brothers doubt her ownership and refuse to return it, Applejack proves herself by expertly playing the bass.
| LineColor = 992699
}} List of My Little Pony: Friendship Is Magic episodes
| RowColor = Y
| Title = Shake Your Tail!
| DirectedBy = Ishi Rudell
| WrittenBy = Amy Keating Rogers
| OriginalAirDate = June 6, 2014
| EpisodeNumber = 7
| ShortSummary = Twilight Sparkle and her friends try to come up with a theme for an upcoming party at Canterlot High.
| LineColor = 992699
}} List of My Little Pony: Friendship Is Magic episodes
| RowColor = N
| Title = Perfect Day for Fun!
| DirectedBy = Ishi Rudell
| WrittenBy = Daniel Ingram & Amy Keating Rogers
| OriginalAirDate = June 19, 2014
| EpisodeNumber = 8
| ShortSummary = The six friends hang out at Canterlot Highs outdoor carnival together.
| LineColor = 992699
}}
|}

====Encore shorts====
With lyric collaboration between Katrina Hadley, Daniel Ingram, Brian Lenard, Jayson Thiessen and Michael Vogel,  and songs composed by Ingram,  Hasbro released three Rainbow Rocks music videos on April 2, 2015: "Friendship Through the Ages",  "Life is a Runway",  and "My Past is Not Today".  These shorts are separate initiatives that lead up to the third Equestria Girls film installment, Friendship Games.  On April 6, 2015, Ingram replied in a comment on Facebook that these will likely be on the next Equestria Girls album later this year. 

{| class="wikitable plainrowheaders" style="width:100%; margin:auto;"
|- style="color:white"
! style="background:#992699; color:white;" | No.
! style="background:#992699; color:white;" | Title
! style="background:#992699; color:white;" | Directed by
! style="background:#992699; color:white;" | Written by
! style="background:#992699; color:white;" | Original airdate List of My Little Pony: Friendship Is Magic episodes
| RowColor = Y
| Title = Friendship Through the Ages
| DirectedBy = Ishi Rudell
| WrittenBy = Katrina Hadley, Daniel Ingram, Brian Lenard, Jayson Thiessen, and Michael Vogel
| OriginalAirDate = April 2, 2015
| EpisodeNumber =  
| ShortSummary = The Rainbooms sing about their everlasting friendship to Sunset Shimmer.
| LineColor = 992699
}} List of My Little Pony: Friendship Is Magic episodes
| RowColor = N
| Title = Life is a Runway
| DirectedBy = Ishi Rudell
| WrittenBy = Katrina Hadley, Daniel Ingram, Brian Lenard, Jayson Thiessen, and Michael Vogel
| OriginalAirDate = April 2, 2015
| EpisodeNumber =  
| ShortSummary = Rarity sings about her fashion philosophy.
| LineColor = 992699
}} List of My Little Pony: Friendship Is Magic episodes
| RowColor = S
| Title = My Past is Not Today
| DirectedBy = Ishi Rudell
| WrittenBy = Katrina Hadley, Daniel Ingram, Brian Lenard, Jayson Thiessen, and Michael Vogel
| OriginalAirDate = April 2, 2015
| EpisodeNumber =  
| ShortSummary = Sunset Shimmer sings about leaving her past mistakes behind her.
| LineColor = 992699
}}
|}

====Games====
On June 7, 2014, a Rainbow Rocks game was released on Hasbros Equestria Girls website titled "Repeat the Beat";  almost two months later, two more games have been released on July 31, 2014. One of them being "Equestria Girls: Battle of the Bands"  and the other being "Equestria Girls: V.I.F. (Very Important Friend)". 

===Soundtrack===
The films soundtrack was released on September 10, 2014 via the iTunes Music Store.  Announced on Ingrams Twitter eight days later on September 18, the soundtrack placed #15 on the Kid Albums Billboard chart;  two weeks later on October 2, the soundtrack placed #12;  a week later on October 9, it placed #10. 

{{tracklist
| extra_column    = Performer(s)
| writing_credits = yes
| total_length    = 22:16
| title1          = Rainbow Rocks
| writer1         = Daniel Ingram
| extra1          = The Rainbooms
| length1         = 1:40
| title2          = Better Than Ever
| writer2         = Daniel Ingram & Meghan McCarthy
| extra2          = The Rainbooms
| length2         = 1:35
| title3          = Under Our Spell
| writer3         = Daniel Ingram & Meghan McCarthy
| extra3          = The Dazzlings
| length3         = 1:51
| title4          = Tricks Up My Sleeve
| writer4         = Daniel Ingram & Meghan McCarthy
| extra4          = Trixie and the Illusions
| length4         = 2:06
| title5          = Shake Your Tail
| writer5         = Amy Keating Rogers & Daniel Ingram
| extra5          = The Rainbooms
| length5         = 1:59
| title6          = Welcome to the Show
| writer6         = Daniel Ingram & Meghan McCarthy
| extra6          = The Dazzlings, the Rainbooms, Sunset Shimmer, and Ensemble
| length6         = 4:37
| title7          = Awesome as I Want to Be
| writer7         = Daniel Ingram & Meghan McCarthy
| extra7          = Rainbow Dash and the Rainbooms
| length7         = 1:16
| title8          = Lets Have a Battle (of the Bands)
| writer8         = Daniel Ingram & Meghan McCarthy
| extra8          = The Dazzlings and Ensemble
| length8         = 2:52
| title9          = Shine Like Rainbows
| writer9         = Daniel Ingram
| extra9          = The Rainbooms, Sunset Shimmer, and the Equestria Girls cast
| length9         = 2:35
| title10         = Music to My Ears  (Bonus Track) 
| writer10        = Daniel Ingram
| extra10         = DJ Pon-3
| length10        = 1:45}}

==Reception==
===Box office===
As the Hasbro Earnings Report, it was revealed that the box office returns from this film exceeded that of the first film in the first weekend by 37%. On a per theater basis, sales for tickets grew by 19% in the first three weeks of its limited theatrical run. 

===Critical reception===
My Little Pony: Equestria Girls - Rainbow Rocks received generally positive reviews from critics and fans alike. Sherilyn Connelly for The Village Voice called Rainbow Rocks "far superior" to the first Equestria Girls film, and that while "the picture is continuity-heavy and not particularly accessible to newcomers", that the film was "up there with the show at its most thoughtful".   Sheri Linden of The Hollywood Reporter complimented the film, saying "Though its strictly for the faithful, the tween-friendly mix of cute and earnest has a forthright sharpness and is never cloying."  Shane OHare of Geekscape gave the Blu-ray of the film a score of 4 out of 5, praising the soundtrack stating "The shows composer, Daniel Ingram,...killed it. 10 all new original recordings from Daniel really stole the show."  Ed Liu of Toon Zone, who gave the first film a negative review, also found this film to be better.  He wrote the film "is almost as sweet and charming as the best episodes of the series, finding new and interesting ways to expand on the show’s themes of friendship." 

"Gerry O", 12-year-old film critic for The Huffington Posts Kids First! column called the film "a combination of adventure with a crème of comedy and a sprinkle of excitement all   up in a friendship sandwich." 

===Ratings===
My Little Pony: Equestria Girls – Rainbow Rocks premiered on Discovery Family on October 17, 2014. It was viewed by approximately 610,000 viewers. 

==Sequel==
  Beyond Home New York Toy Fair, it was announced that the sequel, titled Equestria Girls: Friendship Games, will be released sometime in late 2015. 

==References==
 

==External links==
*  
*  
*  
*  

  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 