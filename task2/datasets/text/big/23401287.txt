Yeh Dil
 
{{Infobox film
| name           = Yeh Dil
| image          = Yeh_Dil_poster.jpg
| image_size     =
| border         =
| alt            =
| caption        = Movie Poster Teja
| producer       = Kiran
| writer         =
| screenplay     = Teja
| story          = Teja
| based on       = Telugu film Nuvvu Nenu (2001 ) Anita Hassanandani
| music          = Nadeem-Shravan
| cinematography = Ravi Varman
| editing        =
| studio         =
| distributor    =
| released       = 4 April 2003
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
 Teja and Anita Hassanandani. Anita Hassanandani reprised her role from Nuvvu Nenu. This movie was later remade in bengali as "Dujone"with Dev and Srabanti in the lead roles.

== Synopsis ==
Ravi Pratap Singh (Tusshar Kapoor) is a college student and the son of a wealthy businessman Raghuraj Pratap Singh (Akhilendra Mishra). His best friend Kabir always helps him. Ravi lives with his father, as his mother died. Vasundhara Yadav (Anita Hassanandani), is the daughter of a middle class wrestler Mithua Yadav (Vineet Kumar) studying in the same college. She lives in a joint family with many of her relatives. Ravi excels in sports, but is terrible in academics. Vasu doesnt pay attention to sports and focuses on academics.

Ravi is neglected by Raghuraj who focuses on business and wealth. Raghuraj expects him to excel in academics, so he can send him to Harvard Business School. Similarly, Mithua, who wanted a boy, neglects Vasu and his wife (Chetana Das).

Ravi and Vasu start studying together and gradually become friends, but Raghuraj disapproves of her due to her social status. Mithua sends some goons to the college who beat up Ravi. Raghuraj orders the police to retaliate, and Mithua is taken to the police station and beaten up by the police. Vasu tells Ravi, after which he calls his father and threatens him.

Raghuraj arranges a "swaymvar" for his son, to which all the business tycoons and rich people send their daughters. Ravi invites Vasu and publicly proclaims his love for her. Mithua discovers she went to Ravis house and confronts Raghuraj, after which the police commissioner tells them they should trick Ravi and Vasu into keeping their love on hold until the completion of their studies.

After this agreement, Ravi is tricked into going to Mumbai and Vasu is tricked into going to her uncles house. Both escape, Ravi goes to Hyderabad, and Vasu goes to Mumbai. Ravi thinks Vasu is in Hyderabad and goes to her house, only to be beaten up by goons. Vasu goes to Mumbai, and is captured and taken to Ravis house where Raghuraj arranges for a killer to kill Vasu. Both are saved and they return to Hyderabad to get married with the help of Kabir and college. Mithua eventually accepts the marriage and stops her aunt Chandi (Pratima Kazmi) from interfering. Raghuraj is also stopped from interfering. Ravi and Vasundhara marry and the last scene shows Kabir and the whole college congratulating them.

==Cast==
* Tusshar Kapoor... Ravi Pratap Singh Anita Hassanandani...Vasundhara Yadav
* Pratima Kazmi...Chandi (Mithuas sister)
* Vineet Kumar...Mithua Yadav
* Akhilendra Mishra...Raghuraj Pratap Singh
* Chetana Das...Vasundharas mother
* Mushtaq Khan...College Director
* Sharat Saxena...Police Commissioner
* Saurabh Shukla... Economics Teacher
* Achyut Potdar...Mansukh
* Supriya Karnik...Mrs. Chaudhary
* Anupam Shyam...Inspector
* Sunila...Dance teacher

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "College Mein"
| Sonu Nigam
|-
| 2
| "Ek Mein Ek Tu"
| Abhijeet Bhattacharya|Abhijeet, Nirja Pandit
|-
| 3
| "Hey Kya Ladki"
| Abhijeet, Kavita Krishnamurthy
|-
| 4
| "Kyon Dil Bichde"
| Tauseef Akhtar
|-
| 5
| "Jaaneman O Jaaneman"
| Abhijeet, Alka Yagnik
|-
| 6
| "Tera Dilbar"
| Sonu Nigam, Alka Yagnik
|-
| 7
| "Yeh Dil" Nadeem Saifi
|}

==External links==
*  

 
 
 
 
 
 
 