Pavitra (film)
{{Infobox film
| name = Pavitra
| image = Pavitra Poster.jpg
| writer = Janardhan Maharshi Saikumar Kaushik Babu
| director = Janardhan Maharshi
| cinematography = V. N. Suresh Kumar
| producer = K. Sadhak Kumar G. Maheshwara Reddy
| editing = Ramesh
| distributor = Aadesh Films
| country = India
| released =   
| runtime = 155 mins
| language = Telugu
| music = M.M.Srilekha
| awards =
| budget =  
| gross = 
}}
 Prostitute in this film. Several wallpapers and working stills were released on 5 Jan13.   Pavitra will also be released in Tamil and Malayalam.

The film’s Tamil version has been titled ‘En Peyar Pavitra’. Which will release on the eve of Sarans birthday. Also the makers have retained the Telugu title for the Malayalam version as well.

According to the director, the film is inspired from a real incident where a prostitute, who was humiliated by a politician, goes on to defeat him in an election. Pavitra’s audio is already making waves amongst music buffs, meanwhile the film’s platinum disc function was held on May 19 to celebrate the audio success.  The film was released on 7 June 2013 to good reviews and emerged as a commercial success.

==Cast==
*Shriya Saran as Pavitra
*Roja Selvamani
*Tanikella Bharani Saikumar
*Ravi Babu
*Kaushik Babu AVS
*Sivaji (Telugu actor)

==Audio Release== function held Visakhapatnam on April 6, 2013.  Shriya Saran, Kaushik Babu, MM Srilekha, Saikumar, Janardhan Maharshi, Bellamkonda Suresh, K.Sadhak Kumar, G.Maheshwara Reddy graced the event. 

==References==
 

 
 
 

 