Satan, Hold My Hand
{{Infobox film
| name           = Satan, Hold My Hand
| image          = Official_Cover_Art_for_DVD_release_of_"Satan,_Hold_My_Hand".jpg
| caption        ="God Loves Sinners!"
| director       = Courtney Fathom Sell
| producer       = Jonathan Ames
| writer         = Reverend Jen Miller
| idea        = Reverend Jen Miller & Courtney Fathom Sell
| starring       = Janeane Garofalo   Reverend Jen Miller    Robert Prichard   Francis Hall as "Faceboy"   Reina Terror   John King   Paige Flash   Matthew Hammond   Moonshine Shorey   Scooter Pie   George Cutler   Don Eng   Bruce Ronn
| music          = Rachel Trachtenburg   Moby   Courtney Fathom Sell   Dusty Santamaria   Brer Brian
| cinematography = Courtney Fathom Sell
| editing        = Courtney Fathom Sell
| studio         = ASS Studios aka Art Star Scene Studios
| distributor    = MVD Entertainment 
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         = $27.00
| gross      = $603.00 
}}
 The Toxic Avenger & Class of Nuke em High. Following a loose narrative, the film concerns a group of Satanic worshippers, who, after kidnapping two Catholic schoolgirls, soon realize that Satan isnt as evil as they hoped for.     

==Production==
According to Sell, the estimated shooting budget on the film was only $27.00, which he claimed was mostly spent on alcohol and pizza for the cast. To further these claims, Sell, who is known for his no-budget film work announced publicly that the film was shot in "Drunk-O-Vision". In the same interview, the group jokingly claimed that Janeane Garofalo had been kidnapped in order to obtain a cameo, and only released upon giving a performance.  

Sell and Miller have mentioned that the idea and screenplay had been completed almost immediately during the time their now-defunct company ASS Studios was launched. However, due to financial issues, they began shooting short films in order to gain momentum and attention for the project, which at the time was entitled "Satans Bitches" then "Half-ASSed Satanists" and possibly written as a soft-core feature. The title change was decided when the two felt that, though with no intentions of putting the film in the Festival circuit, would be a bit more publicly accepted. Sell helped obtain funding for pre-production working as a cook at various Lower East Side restaurants. Regarding the involvement of Jonathan Ames in the film, Sell wrote: When our friend Jonathan Ames, writer and creator of HBO’s series Bored to Death decided to sign on as a producer, it saved us a lot of further hardship. People become interested. Sincerely interested. They were now curious. Jonathan had seen and enjoyed a bunch of our earlier work, and actually had been paid a not-so-subtle homage in our piece The Sinful Bitches. Rev had sent him over the screenplay and he was enthusiastic about playing the role of producer, though we are still trying to figure out what it all means to this day. It was sort of a Paul Morrissey-Warhol affair if you understand what I mean. 

==Release==

Satan, Hold My Hand premiered at Midnight in September in 2010 at Anthology Film Archives in New York City. Writer Trav S.D. recalls on his official website, 
 
The movie was dedicated to the late Taylor Mead, and that was fitting, for the entire proceedings from soup to nuts seemed infused with his gonzo spirit, from the underground rawness of the movie’s assembly, to the absinthian cocktail of humor and anarchistic free-for-all, to the neo-Warholian constellation of bona fide "characters" who not only populated the film but the screening and the before and after festivities that book-ended it.  

The film was released in May 2013 by MVD Entertainment. 

==References==
 

==External links==
*  

 
 
 
 
 
 