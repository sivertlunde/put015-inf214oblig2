Elmer, the Great
{{Infobox film
| name           = Elmer, the Great
| image          = 
| caption        = 
| director       = Mervyn LeRoy
| producer       = Raymond Griffith
| writer         = Ring Lardner George M. Cohan
| narrator       =  Joe E. Brown Patricia Ellis
| music          = Leo F. Forbstein
| cinematography = Arthur L. Todd Thomas Pratt
| distributor    = First National Pictures
| released       = April 29, 1933 
| runtime        = 72 min
| country        = USA
| language       = English
| budget         = $200,000 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Joe E. Brown and Patricia Ellis. Joe E. Brown plays Elmer Kane, a rookie ballplayer with the Chicago Cubs whose ego is matched only by his appetite. Because he is not only vain but naive, Elmers teammates take great delight in pulling practical jokes on him. {{Citation
  | url=http://www.allmovie.com/work/elmer-the-great-90384
  | accessdate =  }} 

==Plot== Joe E. Brown), is a rookie ballplayer with the Chicago Cubs whose ego is matched only by his appetite. Because he is not only vain but naive, Elmers teammates take great delight in pulling practical jokes on him. Still, he is so valuable a player that the Cubs management hides the letters from his hometown sweetheart Nellie (Patricia Ellis), so that Elmer wont bolt the team and head for home. When Nellie comes to visit Elmer, she finds him in an innocent but compromising situation with a glamorous actress (Claire Dodd). She turns her back on him, and disconsolate Elmer tries to forget his troubles at a crooked gambling house. Elmer incurs an enormous gambling debt, which the casinos owner is willing to forget if Elmer will only throw the deciding World Series game (which he refers to as the World Serious). Elmer brawls with the gambler and lands in jail, where he learns of a particularly cruel practical joke that had previously been played on him. Out of spite, he refuses to play in the Big Game, and thanks to a jailhouse visit by the gamblers, it looks as though Elmer has taken a bribe, but when he shows up to play (after patching things up with Nellie), Elmer proves that hes been true-blue all along. Based on the Broadway play by Ring Lardner and George M. Cohan, Elmer the Great betrays its stage origins in its static early scenes, but builds confidently to a terrific climax during a rain-soaked ball game.

==Cast== Joe E. Brown as Elmer Kane 
* Patricia Ellis as Nellie Poole 
* Frank McHugh as Healy High-Hips 
* Claire Dodd as Evelyn Corey 
* Preston Foster as Dave Walker (as Preston S. Foster) 
* Russell Hopton as Whitey 
* Sterling Holloway as Nick Kane (as Sterling Halloway) 
* Emma Dunn as Mrs. Kane  Charles C. Wilson as Mr. Wade (as Charles Wilson) 
* Charles Delaney as Johnny Abbott 
* Berton Churchill as Colonel Moffitt 
* J. Carrol Naish as Jerry (as J. Carroll Naish) 
* Gene Morgan as Noonan

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 

 