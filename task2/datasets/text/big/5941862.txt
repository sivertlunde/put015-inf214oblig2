What Is It?
 
 
 
{{Infobox film
| name = What Is It?
| image = Whatisitposter.jpg
| caption =
| director = Crispin Glover
| producer = Crispin Glover Matt Devlen Ryan Page Mike Pallagi
| writer = Crispin Glover
| starring = Crispin Glover Fairuza Balk Michael Blevis Rikky Wittman John Insinna Kelly Swiderski Lisa Fusco
| music =
| cinematography = Wyatt Troll
| editing = Crispin Glover
| di = Volcanic Eruptions
| released =  
| runtime = 72 minutes
| country = United States
| language = English
| budget =
}} surrealist film funded and directed by Crispin Glover. It is described by IMDb as "The adventures of a young man whose principal interests are snails, salt, a smoking pipe (tobacco)|pipe, and how to get home. As tormented by an hubristic, racist inner Psyche (psychology)|psyche." As of 2008, the film has only been shown at independent theaters, typically accompanied by a question-and-answer session, a one-hour dramatic narration of eight different profusely illustrated books as a slideshow, and a meet-and-greet/book signing with Glover.


What Is It? is the first in a planned trilogy, to be followed by It Is Fine. Everything Is Fine! and It Is Mine.   
==Plot==
 

==Cast==
 

==Release==
 

==Reception==
 
==References==
 

== External links ==
*  
*  
 

 
 
 
 
 
 


 