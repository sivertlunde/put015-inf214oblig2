The Wonderful Galaxy of Oz
  1990 futuristic adaptation of the classic story The Wonderful Wizard of Oz. It was originally released in the 1990s in Japan as a television series, Space Oz no Bōken (スペースオズの冒険, Adventures of Space Oz), and consists of 26 episodes. The plot parallels the original but imposes a science fiction theme.  The U.S. version is a highly truncated (76 minutes) and dubbed version of the original.

==Plot==
In the year 2060, eight-year-old Dorothy and her dog are mysteriously swept off their planet into the wonderful, magical Galaxy of Oz. An evil witch, Gloomhilda, once ruled the Galaxy through fear and terror but was driven out by the good Dr. Oz. Now, Gloomhilda has amassed an army on the outskirts of the galaxy and is preparing to win back her empire. Her sneak attack fails due to Dorothys unexpected arrival.

However, Dr. Oz knows Gloomhilda will return. He has a plan which will free the Galaxy of the wickedness of Gloomhilda forever. Legend tells of three crystals, the Crystal of Love, the Crystal of Wisdom, and the Crystal of Courage. These crystals were spread throughout the Galaxy and lost hundreds of years ago.

According to the legend, whoever possesses all three crystals will rule the Galaxy of Oz for all Eternity. Under the guidance of Dr. Oz, Dorothy and an assortment of "heroes" set out to scour the Galaxy in search of the three magical crystals.

==Characters==

===Main characters===
*Dorothy
*Toto
*Dr. Oz
*Plantman
*Chopper
*Lionman
===Villains===
*Gloomhilda (Glumilda)

==Music==
Japan
* Opening Theme:  
* Ending Theme:   by Yukari Morikawa

==Episodes==
# Dorothy Runs into the World of Oz 
# King of Ozs Surprising Secret 
# Crystal Empire Secret
# Worlds Cowardly Hero 
# Sleeping Beauty of Mangabu  
# Choppers Heart 
# Entrust Fashion to Me!
# Babby-Sitter Crisis 
# Promise with Rockman Yabor 
# Scrapia of Scrapper 
# Grand Prix of Planet Car 
# Dorothys Santa Claus
# Chopper lover
# Escape from Planet Game 
# Horror Birthday 
# Planet Water Rescue Mission 
# Toy Country War 
# Monster Panic 
# Crowded Amusement Park 
# Planet that Father and I Defended 
# Legend of Captain Garo 
# Return to the Alherihit Hometown
# Film Star Glumilda
# Reunion with Doctor Oz 
# Return of the Witch From the West 
# Miracle of the Rainbow Crystal

==Cast==

===Japanese cast===
*Mariko Kouda - Dorothy
*Hiroshi Takemura - Chopper
*Noriko Kamimura - Glumilda
*Ai Satou - Emily Obasan, Hagi
*Eken Mine - Minister, Virtual President
*Hidetoshi Nakamura - Yabor
*Hirohiko Kakegawa - Bisuti
*Ichiro Murakoshi - Dr. Oz, Mojis Father
*Katsumi Suzuki - General of Oz, Toto
*Kazuhiko Kishino - Santa
*Kazuo Oka - Henry Ojisan, Pauls Father
*Kenichi Ono - Jill
*Kozo Shioya - Plante
*Kumiko Nishihara - Azuma
*Kumiko Takizawa - Queen
*Mahito Tsujimura - Rakudo
*Mami Matsui - Moji
*Mari Adachi - Koras Girl, Maid
*Mariko Ikegami - Nicholas
*Miki Narahashi - Mishas Mother
*Minoru Inaba - Alherihit
*Mitsuo Chida - Robot A
*Miyuki Matsushita - Rabbit, Sister
*Narumi Hidaka - Teddy Bear
*Natsumi Sakuma - Witch of the West
*Reiko Suzuki - Mother
*Rica Matsumoto - Baby Dinosaur
*Ryuuzou Ishino - Hunter, Radio Voice
*Satomi Koorogi - Misha
*Shinobu Adachi - Toma
*Shinpachi Tsuji - Baresuku, Captain, Hunter
*Shōzō Iizuka - Oz
*Takumi Yamazaki - Adjutant, Soldier A, Stag
*Toshiyuki Morikawa - Athletic
*Yasuo Muramatsu - King
*Yuri Amano - Princess Shera
*Yuri Shiratori - Lily
*Yuuichi Nagashima - Hunter

==See also==
*The Wizard of Oz (adaptations) — other adaptations of The Wonderful Wizard of Oz

==External links==
* 
* 

 

 
 
 
 
 