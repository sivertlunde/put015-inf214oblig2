Beyond Belief (2007 film)
 
{{Infobox film
| name           = Beyond Belief
| image          = BB_digital_film_poster.jpg
| caption        = Theatrical poster for Beyond Belief.
| director       = Beth Murphy
| producer       = Beth Murphy
| music          = Evren Celimli Sean Flynn
| editing        = Kevin Belli Beth Murphy
| distributor    = Principle Pictures
| released       = April 26, 2007
| runtime        = 97 min.
| country        = USA Afghanistan English Dari Dari
| budget         =
| gross          =
}}

Beyond Belief is a feature documentary directed by Beth Murphy. The film follows Susan Retik and Patti Quigley, two women who lost their husbands on September 11, 2001, as they set up humanitarian programs for war widows in Afghanistan. It premiered at the 2007 Tribeca Film Festival.   

==Film credits==
*Director: Beth Murphy
*Producer: Beth Murphy
*Associate Producer: Sean Flynn
*Editors: Kevin Belli, Beth Murphy
*Camera: Kevin Belli, Sean Flynn
*Music: Evren Celimli
*Translator: Luna Asrar

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 

 