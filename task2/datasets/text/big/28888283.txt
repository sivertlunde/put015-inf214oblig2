Der Jäger von Fall (1936 film)
{{Infobox film
| name           = Der Jäger von Fall
| image          =
| caption        =
| director       = Hans Deppe
| producer       = 
| writer         = Ludwig Ganghofer (novel), Joseph Dalman
| starring       = Paul Richter, Marie Sera, Franz Loskarn
| music          = 
| cinematography = 
| editing        = 
| studio         =  Tonlicht-Film Ostermayr, Universum Film (UFA)
| distributor    = 
| released       =  
| runtime        = 
| country        = Germany
| awards         =
| language       = 
| budget         = 
}}
 Der Jäger von Fall by Ludwig Ganghofer.

==Cast==
* Paul Richter  ...  Friedl, Jagdgehilfe
*  Marie Sera  ...  Friedls Mutter
*  Franz Loskarn  ...  Hias, Jagdgehilfe
*  Georgia Holl  ...  Burgl, Sennerin
*  Rolf Pinegger  ...  Lenz, Senner
*  Hans Adalbert Schlettow  ...  Huisen Blasi
*  Willy Rösner  ...  Birkhofbauer
*  Betty Sedlmayr  ...  Loni
*  Thea Aichbichler  ...  Buchnerin
*  Hans Hanauer  ...  Förster Donhart
*  Hélène Robert (actress)|Hélène Robert  ...  Therese, Frau des Försters
*  Gustl Gstettenbaur  ...  Toni Donhart
*  Philipp Veit  ...  Dr. Rauch
*  Josef Eichheim ...  Brandtner Michl
*  Hans Henninger  ...  Leichtl Sepp

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 