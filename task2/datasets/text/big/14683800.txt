Guns a Poppin
{{Infobox Film |
  | name           = Guns a Poppin! |
  | image          =Gunspoppin.JPEG|
  | caption        = |
  | director       = Jules White Jack White (story and screenplay) |
  | starring       = Moe Howard Larry Fine Joe Besser Vernon Dent Frank Sully Joe Palma |
  | cinematography = Henry Freulich | 
  | editing        = Saul A. Goodkind |
  | art director  = Cary Odell |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 16 27"
  | country        = United States
  | language       = English
}}

Guns a Poppin! is the 179th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
 
Moe is on trial for assaulting Larry and Joe. Moe appeals to the judge (Vernon Dent), claiming he is a sick man who was instructed by his doctor to maintain peace and quiet. This peace is broken by Larry and Joe who are loudly rehearsing their "The Original Two-Man Quartet" routine to serenade Moe. Moe cracks, and wraps Larrys trombone slides around the quartets neck. Realizing Moe is in bad shape, Larry and Joe decide to take their ailing leader on a hunting trip to relieve his stress. Moe takes to the idea like ducks to bread, and the Stooges start packing.
 eggs and potatoes while Moe has his back turned. His nerves double frayed, Moe asks Larry and Joe to pursue the bear. One thing leads to another, and the bear ends up behind the wheel of the Stooges car, driving away with it. Then, when Moe thinks he is at the end of his rope, the Stooges get involved with a sheriff (Frank Sully) in hot pursuit of outlaw Mad Bill Hookup (Joe Palma).

Back in the courtroom, Moe ends his story by concluding that he must go back to bed for six additional months. The judge takes pity on the poor Stooge, and finds him not guilty. Joe and Larry are disgusted by the ruling and are about to get theirs. But thanks to Larrys tough skull the axe is now broken and Moe begins to lose his nerves again.

==Production notes==
Guns a Poppin! is a remake of 1945s Idiots Deluxe, using a surprisingly minimal amount of stock footage from the original. All new scenes were filmed on November 28, 1956. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 