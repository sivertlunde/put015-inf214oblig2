Hellish Flesh
{{Infobox film
| image          = Inferno carnal.jpg
| caption        = Original Brazilian film poster
| director       = Jose Mojica Marins 
| producer       = Jose Mojica Marins Alfredo Cohen
| writer         = Jose Mojica Marins Rubens Francisco Luchetti 
| starring       = Jose Mojica Marins
| music          = Solon Curvelo
| cinematography = Giorgio Attili
| editing        = Nilcemar Leyart	 	 
| distributor    = Brasil Internacional Cinematográfica
| studio         = Produções Cinematográficas Zé do Caixão Brasil Internacional Cinematográfica
| released       = March 24, 1977
| runtime        = 85 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}} 1977 Cinema Brazilian horror film directed by José Mojica Marins. Marins is also known by his alter ego Zé do Caixão (in English, Coffin Joe).    

==Plot==
Dr. George Medeiros is a brilliant scientist who does not find time for beautiful wife Rachel. She falls in love with Oliver, the best friend of her husband, and soon after the two plan to kill George and inherit his fortune. Benefiting from the distraction of her husband in the laboratory, Raquel throws acid on Georges face, disfiguring it. As he recovers in the hospital, Raquel and Oliver spend all his money. After months in the hospital, Dr. George comes home with a plan for revenge in mind.    
 
== Cast ==
* Cristina Andréia
* Lirio Bertelli
* Virgínia Camargo
* Michel Cohen
* Oswaldo De Souza
* Luely Figueiró
* José Mojica Marins
* Marisol Marins
* France Mary
* Jorge Peres
* João Paulo Ramalho (voice)
* Helena Ramos
* Mauro Russo

== References ==
 

== External links ==
*    
*  
*   on    

 

 
 
 
 
 
 

 
 