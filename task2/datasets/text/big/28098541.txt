Mars and April
 
{{Infobox film
| name           = Mars et Avril
| image          = Mars_et_Avril_Official_poster.jpg
| alt            =  
| caption        = Official poster of the movie Mars et Avril
| director       = Martin Villeneuve
| producer       = Martin Villeneuve Anne-Marie Gélinas Benoît Beaulieu
| writer         = Martin Villeneuve
| based on       =  
| starring       = Jacques Languirand Caroline Dhavernas Paul Ahmarani Robert Lepage
| music          = Benoît Charest
| cinematography = Benoît Beaulieu
| editing        = Mathieu Demers
| studio         = Mars et Avril Inc. EMAfilms Les Productions du 8e Art Item 7 Alliance Vivafilm (Canada) Gaiam (United States)
| released       =  
| runtime        = 90 minutes
| country        = Canada
| language       = French (English subtitled)
| budget         = $2,300,000
| gross          = 
}}
Mars et Avril (aka Mars & Avril or Mars and April,  — "Mars" here referring to  , July 16, 2012  is a Canadian  , October 26, 2011 
 Belgian comic ILM Senior Star Trek) Alliance Vivafilm and in the United States by Gaiam. The   was released in Quebec theaters and on Alliance Vivafilms YouTube channel on December 21, 2011. The World premiere took place on July 2, 2012, at the 47th Karlovy Vary International Film Festival, Czech Republic (the film was selected in the section “Another View” for its unique artistic approach in both form and content).  , article from Quiet Earth, June 16, 2012  Since then, the film has been screened in many other major cinema events around the globe (see the FESTIVALS section below).

==Releases==

Mars et Avril got its theatrical release in Quebec on October 12, 2012,  , news release from Alliance Vivafilm, August 23, 2012   , article from Le Devoir, August 25, 2012  and reviews were generally positive.  , review from eFilmCritic.com, February 11, 2013   , review from Le Devoir, October 13, 2012   , review from  : achievement in music, achievement in overall sound, best adapted screenplay and achievement in visual effects,  and five nominations at the 2013 Jutra Awards. 
 Alliance Vivafilm on March 19, 2013, to critical acclaim,   along with Benoît Charests original soundtrack  and the  trailer music composed by Ramachandra Borcar.  On October 22, 2013, Benoît Charest won the Félix Award|Félix in the category Album of the year – original soundtrack at the ADISQ Gala.  For the occasion, a limited edition of 300 vinyl records of the soundtrack was released,  as a nod to the retro-futuristic look of the film.

In September 2013, Mars et Avril was sold to the U.S. and is now available on U.S. iTunes and on Gaiam|GaiamTV.   The film is also available on iTunes in the UK, and all across Francophone Europe. Other major digital platforms will follow, starting in January 2014. 

==Martin Villeneuves TED Talk==

On February 27, 2013,  .  Prior to his talk, the opening sequence of the film was shown, as well as a three minutes overview of the steps leading from the green screen to the final images.   Martin Villeneuves talk, “How I made an impossible film,” was released on TED.com on June 7, 2013, and a month later was added to TEDs movie magic list, featuring famed directors such as James Cameron and J.J. Abrams.  Since then, Mars et Avril is being referred to as the “Impossible Film”. 

==Plot==
Adapted from two acclaimed graphic novels, Mars et Avril is set in a futuristic Montreal, where humanity is about to set foot on Mars. Jacob Obus (Jacques Languirand), a charismatic musician, takes pride in slowing down time by playing instruments inspired by women’s bodies, designed by his friend, Arthur (Paul Ahmarani). A love triangle develops when Jacob and Arthur both fall in love with Avril (Caroline Dhavernas), a young photographer. Enter Eugène Spaak (Robert Lepage), inventor, cosmologist and Arthur’s father, who unveils a new theory about man’s desire to reach Mars and helps Jacob find the true meaning of life and love. 

==Cast==
* Jacques Languirand ... Jacob Obus
* Caroline Dhavernas ... Avril
* Paul Ahmarani ... Arthur
* Robert Lepage ... Eugène Spaak (head)
* Jean Asselin ... Eugène Spaak (body) / Serveur automate
* Stéphane Demers ... Bernard Brel
* Jean Marchand ... Pneumatologue
* Kathleen Fortin ... Modèle dArthur
* Marcel Sabourin ... Capucin
* André Montmorency ... Pierrot
* Gabriel Gascon ... Arlequin
* Emanuel Hoss-Desmarais ... Marsonaute 1
* Pierre Leblanc ... Marsonaute 2
* Richard Robitaille ... Marsonaute 3
* Khanh Hua ... Serveur automate
* Michèle Deslauriers ... Standardiste (voice)
* Denis Gravereaux ... Cosmologue 1
* Charles Papasoff ... Cosmologue 2
* Émilie Blake ... Secrétaire du Pneumatologue

==Production design==
Mars et Avril benefits from the collaboration of  , December 8, 2011 

 . 

One of the main brainstorming sessions between Villeneuve and Schuiten is available as the audio commentary on the  .

==Production== The Far Side of the Moon in 2003), with the intent of adapting them into a science fiction feature film. While the author of the books was to write the script, Lepage was attached to the project as actor and producer. 

A year later, Lepage shut down Films Ex æquo, deciding at the time that he would not direct any more films in Canada.  Nevertheless, he strongly encouraged Mars et Avrils writer to take the directors chair.  Martin Villeneuve took over the project, while Lepage remained involved as actor and creative producer. 
 The Golden Compass and Mr. Nobody (film)|Mr. Nobody.  The young filmmaker was also able to convince Guy Laliberté, founder of Cirque du Soleil, to finance the creation of the imaginary musical instruments to be used by the actors on set. In 2008, SODEC, Telefilm Canada, The Harold Greenberg Fund and Alliance Vivafilm decided to finance the production. Due to the films significant amount of visual effects, an extensive year of pre-production was necessary. Also, in order to work around Robert Lepages extremely tight schedule, principal photography took place in Montreal in two segments; the first was in September 2008, and the second was in April 2009.  Since Lepage only had a few days available for filming, Villeneuve turned his character into a hologram and had another actor wearing a green hood stand in for his scenes during principal photography.  The film was almost entirely shot on green screen, in 25 days, using the RED digital camera. 

Most of the actors who appeared in the graphic novels reprised their roles in the film, with the exception of Marie-Josée Croze (who portrayed the lead female character in the books), due to a schedule conflict. Caroline Dhavernas (Passchendaele (film)|Passchendaele, Wonderfalls) was then hired to play the part of AVRIL, opposite Jacques Languirand (JACOB OBUS), Robert Lepage (EUGÈNE SPAAK) and Paul Ahmarani (ARTHUR SPAAK). From the original cast of the books, Stéphane Demers also reprised the role of BERNARD BREL. 
 virtual technologies extends to bold experiments on himself. His head is actually a hologram, with all of his ideas, memories and thoughts stored electronically. Six cameras were trained on Lepages head while another actor portrayed the cosmologists body.   

By the end of 2009, the editing of a first cut was assembled. In 2010, Martin Villeneuve searched for new investments in order to complete the 550 VFX shots involved in the film.  , article from Le Nouvelliste, January 21, 2012  In early 2011, Anne-Marie Gélinas and Benoît Beaulieu joined Villeneuve as producers.  Telefilm Canada and Alliance Vivafilm both accepted to raise their initial investment, and so did Robert Lepage and Lynda Beaulieu though their new Quebec city-based motion picture company, Les Productions du 8e Art. At this point, Pierre Even (C.R.A.Z.Y.) and Marie-Claude Poulin from Item 7 also joined the team as executive producers. 
 ILM Senior Compositor Carlos Monzon) then started the visual effects and sound design. Also, Oscar-nominated Benoît Charest (Les Triplettes de Belleville) was tasked with scoring the music. 

The official trailer was released in Quebec theaters and on Alliance Vivafilms YouTube channel on December 21, 2011.  The official poster, designed by  , Czech Republic (the film was selected in the section “Another View” for its unique artistic approach in both form and content).  The movie was well received by the public  and the reviews were positive.   The   unveiling the films creation has been released online on August 16, 2012, along with the official website created by the Sid Lee agency.  A 22-minute “Making Of” was aired on ARTV in October 2012 in order to promote the release of the film, and is now available   and with English subtitles on YouTube. 

Mars et Avril was released in Quebec on October 12, 2012   and reviews were generally positive.    

===Other facts about the film===
*   in the movie adaptation. Both Croze and Dhavernas were Villeneuve’s roommates during his college years. 

* Jacques Languirand, who was nearly 80 years old when the film was shot, wore an ear-piece so that his wife, Nicole Dumais, could feed him his lines off set. It was the radio host’s first leading role in a feature film. 

* The opening of the film is based on German astronomer Johannes Kepler’s cosmological model from the 17th century, Harmonices Mundi, in which the harmony of the universe is determined by the motion of celestial bodies. Benoît Charest also composed the score according to this theory. 

* Martin Villeneuve couldn’t afford to have the imaginary musical instruments built, so he went to Cirque du Soleil CEO Guy Laliberté and convinced him to buy them before they were even made.  When Laliberté saw Villeneuve’s TED Talk on June 7, 2013, he offered the young filmmaker the “Gravophone” which can be seen in the talk.  , BULB, December 2, 2013 

* Upon his performance on the stage of the Liquid Pub, the band of old musicians – formed by actors Jacques Languirand, Marcel Sabourin, André Montmorency and Gabriel Gascon, genuine cultural icons of Quebec – received a standing ovation that lasted several minutes from the 50 extras present during the shooting. The first assistant director even had to stop the applause so as not to be behind schedule. 

* A setting of Fever in Urbicand (Schuiten & Peeters, Casterman, 1985), cult album in Les Cités Obscures series, appears in the movie. As a matter of fact, François Schuiten agreed to have a 3D model made out of his futuristic auditorium, for a scene taking place inside the Temple of Cosmologists. Martin Villeneuve had this image in mind when writing his books, a few years before Schuiten joined the team. Before the shooting, even the extras were chosen to look like the characters in the comic book. 

* The relation between the musical instrument and the Martian topography, explained during Eugène Spaak’s conference, is a real discovery that Martin Villeneuve made while writing his books.   

* Since Robert Lepage only had a few days available for filming, an avant-garde 3D capturing technique was used to integrate him virtually into his scenes as a hologram. Six cameras were trained on Lepage’s head while a mime, Jean Asselin, portrayed the body.  

* Jean Asselin, the mime who performs Eugène Spaak’s body, had to wear a green hood for the whole duration of the shoot. He also plays the robot waiter of the Liquid Pub and of The Greenhouse Effect nightclub, where you get to see his face. 

* Aboard the Orient Express, Jacob Obus and Eugène Spaak are respectively ordering an “electric eel” and a “flying fish”. This scene was shot on April 1, 2009 and, in volume 2 of the graphic novel, Eugène’s order was actually the “April’s fool special”. 

* The “electric eel” ordered by Jacob Obus aboard the Orient Express was a false latex eel. However, believing that it was real, the property master placed it in a cooler prior to the shooting of the scene. 

* The space module that enters the Martian atmosphere was modeled after Jacob Obus’ microphone that can be seen on the stage of the Liquid Pub. 

* The Martian backgrounds were taken by photographer Denis McCready in the Mojave Desert using a panoramic film camera. The exact location is near Trona Pinnacles where several sci-fi movies and TV series were filmed including Battlestar Galactica, Star Trek V: The Final Frontier, The Gate II, Lost in Space and Planet of the Apes. 

* The scene where Jacob and Arthur are leaving for Mars is inspired by one of Martin Villeneuve’s childhood memories: when he was 4 or 5 years old, his older brothers put him in a box and made him believe that he made a space travel on his way to Mars. 

* In March 2011, exactly two years after principal photography, a re-shoot involving Jacques Languirand and Caroline Dhavernas took place at Robert Lepage’s request. The scene in which Jacob and Avril are making love was shot in Languirand’s actual bedroom. There’s a 48 years age gap in between the two lovers. 

* Because some extras were missing in a few shots, VFX artists at Vision Globale dressed in the futuristic fashion of the film to celebrate Halloween in 2011, and were shot on green screen. 

* Michèle Deslauriers, who’s the actual voice of the Montreal Transportation Service which can be heard in the metro, provided the Montreal Teleportation Service’s voice in the film. Michèle Deslauriers is also Caroline Dhavernas’ mother. 
 Greek for "breath", which metaphorically describes a non-material being or influence. 

==Festivals==
* September 27, 2014 (Closing gala): Festival du Film Canadien de Dieppe, France. 

* March 22, 2014: FICG Festival Internacional de Cine en Guadalajara, Mexico. 

* September 29, 2013: Lund Fantastisk Film Festival, Sweden. 

* September 22, 2013: UTOPIA — Tel-Aviv International Festival of Fantastic Film, Israel. 
 Ville de Québec &  Vitesse Lumière, Canada. 

* July 9, 2013: Neuchâtel International Fantastic Film Festival, Switzerland (international competition) – Honorable mention for incredible post-production work. 

* May 5, 2013: Sci-Fi London Film Festival, United Kingdom.  

* April 14, 2013: Imagine Film Festival in Amsterdam, Netherlands. 

* April 3, 2013: Brussels International Fantastic Film Festival, Belgium (“7th Orbit” 2013 international competition).    

* February 23, 2013: Rendez-vous du cinéma québécois, Montreal, Canada (in competition for the Gilles-Carle Award). 

* February 11, 2013 (Opening film): Boston Sci-Fi Film Festival, United States – Honorable mention. 

* December 6, 2012 (Opening film): Monsters and Martians International Film Festival, Toronto, Canada. 

* December 1, 2012: Whistler Film Festival, Canada (Section / Film Type: “Discoveries”). 

* October 21, 2012: Mumbai Film Festival, India. 

* October 11, 2012 (Opening film of the FOCUS section): Festival du Nouveau Cinéma, Montreal, Canada. 

* October 5, 2012 (U.S. premiere): Mill Valley Film Festival, California, United States. 

* September 29, 2012 (Closing gala): Calgary International Film Festival, Canada. 

* September 17, 2012: Cinéfest Sudbury International Film Festival, Canada. 

* September 15, 2012 (Canadian premiere): Atlantic Film Festival in Metropolitan Halifax|Halifax, Canada. 

* July 2, 2012 (World premiere): Karlovy Vary International Film Festival, Czech Republic (the film was selected in the section “Another View” for its unique artistic approach in both form and content). 

==References==
 

==External links==
*  
*  
*  , article from the official TED Blog, June 7, 2013
*  
*  
*  
*  
*  
*  
*  
*  
*   The Gazette, October 5, 2012
*  , Martin Villeneuves first podcast appearance
*  

 
 
 
 
 