A Cinderella Story: Once Upon a Song
{{Infobox film
| name           = A Cinderella Story: Once Upon a Song
| image          = A Cinderella Story Once Upon a Song poster.jpg
| caption        = DVD cover
| director       = Damon Santostefano
| producer       = Dylan Sellers
| writer         = Erik Patterson Jessica Scott
| starring       = Lucy Hale Freddie Stroma Megan Park Missi Pyle
| music          = Braden Kimbell
| cinematography = John Peters
| editing        = Tony Lombardo
| studio         = Dylan Sellers Productions
| distributor    = Warner Premiere
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
}} teen Romantic romantic comedy musical film, directed by Damon Santostefano who directed the previous film Another Cinderella Story and starring Lucy Hale, Freddie Stroma, Megan Park and Missi Pyle. The film was released direct-to-DVD on September 6, 2011. The film premiered on ABC Family on January 22, 2012.  

It is the third installment in A Cinderella Story series, following the 2004 film A Cinderella Story and the 2008 stand-alone sequel Another Cinderella Story again reprising the same themes and situations but not containing any characters from the original films.

==Plot==
 
Over-worked, bullied and terrified of being put in foster care, seventeen-year-old Katie Gibbs (Lucy Hale) is a singer who does her stepmother and stepsiblings bidding with no complaints. Shes constantly harassed by Gail, her stepmother. The film starts in a dream where she performs a music video for her hit single ("Run This Town"). Her stepbrother, Victor (Matthew Lintz) then wakes her up demanding breakfast. Soon after, it becomes obvious how badly shes treated by Gail (Missi Pyle) and her stepsister Beverly, also known as Bev (Megan Park). At the Wellesley Academy of the Arts, Guy Morgan (Dikran Tulaine), the president of the Massive Records Company enrolls his son into the school where Gail is headmistress. Katie, wanting to make a change in her life, sneaks her demo into Guys briefcase. Guy wants his son, Luke (Freddie Stroma), to produce the showcase and start following in his footsteps. However, Luke is less than excited about that idea.

Back at home, Bev, Katies stepsister, starts to practice singing so she can win a recording contract with Guy, but she is terrible. Gail gets a call from Guy and is shocked to hear that he loves Katies demo CD. Gail lies and says that it was Bevs demo CD and that Katie stole it. Meanwhile, Victor, Katies stepbrother, steals her clothes while she showers and takes her towel, locking her outside of the house naked. Katie then tries all the ways she know to go back in, but fails. She ends up wearing the welcome mat while waiting outside, meeting Luke for the second time. He seems amused by the situation, but gives her his jacket. Bev opens the door and invites Luke in, while Gail goes to Katies room and forces her to sing. Gail drives Luke out of the house with a funny excuse and starts planning to use Katies voice to get Bev a record deal. Bev, at first reluctant, gives in to Gails urging and agrees in order to win Lukes affection.

At school, Katie and her best friend Angela (Jessalyn Wanlim) hear Luke singing his song and Katie is entranced. Katie writes a song in response to his song and admits she kind of likes him. Angela tells her to sing it to him at the Bollywood Ball that night and gives Katie a costume to wear. They hear Gail shouting outside and Angela hides just as Gail opens the door. Seeing the costume Katie has, Gail makes her stay home and babysit Victor for the night. After Victor reveals to Katie by hidden cameras, that Bev wants Luke and is going to try to win him over at the ball, she asks Ravi (Manu Narayan), Gails guru to watch him. Ravi reveals hes not actually a guru but a method actor from New Jersey, named Tony, who couldnt refuse Gails money and is only actually half Indian. At the party, Katie leads Luke outside and sings ("Extra Ordinary") and he becomes entranced by her voice. Meanwhile, Angela distracts Gail by challenging her to a Bollywood dance off ("Oh Mere Dilruba"). After the dance battle, Gail leaves. Katie escapes and arrives home, only to find Gail already waiting for her. Gail threatens to expel Angela so she wont get into Juilliard if Katie does not sing for Bev, Katie agrees to save Angela.

Luke tries to find the girl who sang to him but instead walks to the music room, where Luke sees Bev who lip syncing using Katies voice singing ("Make You Believe") in the music room and mistakes her for the girl. Luke falls for Bev and invites her to write a song with him. The two go out on a date that same night and Luke is amazed by Bevs love for music, unaware that Katie is there and is texting to Bev, telling her what to say. The next day at the house, Luke is greeted by Victor and gives him a little guitar lesson. Katie reconnects with Luke and heads out to throw away Gails ruined painted portrait after Victor cut off the face with a special knife. Luke tells Katie he doesnt like her disappearing. Bev sweeps him away and diverts his attention from Katie to her instead. She somehow gets Victor to help her trick Luke. He gives her and Katie devices that allow Bev to hear Katie, so that she can help her during the date. Katie creates a song on the spot and while Luke sings her song ("Possibilities") to Bev, she falls harder for him. Bev and Luke kiss, breaking Katies heart. Victor tries to cheer Katie up, seeing her distress. Luke believes he is in love with Bev because of her voice and talent for song writing, unaware that Bev is pretending to possess Katies talents. Later, Gail admits to Katie that she can have access to her dads savings account, forcing Katie to submit under her stepfamilys will. Overhearing this, Victor has enough of the way his family had mistreated Katie over the years. Feeling guilty and realizing that Katie had been the only family member who cares for him, Victor is determined to expose his mother and Bev for their fraud. He goes to Katie to tell her this and apologizes for his mistreatment towards her. Victor admits that she was the only family who cares for him and he sort of loves her. Katie is touched by this and tells Victor that she sort of loves him back. 

At the showcase, Victor sabotages Bev who performs ("Make You Believe") in the middle of the song with Tonys help by crashing the iPhone of Katies recorded singing voice, forcing Gail to make Katie sing live backstage. Luke finally realizes Katie is the girl with a beautiful voice all along and Bev has been faking the whole thing. Luke steals the camera and films Katie, exposing Bev as a fraud. Gail tries to shut the camera but is stopped by Tony, who shocks her with the cables. Katie finishes her song ("Bless Myself") with Victors assistance. Katie and Luke admit their feelings for each other and they kiss. Luke asks Katie wants to make her album after Guy tells Luke about Katies performance. The film ends as Katie, Luke and their friends Victor, Tony, Angela and Mickey O Malley (Titus Makin Jr.) celebrate and a happily ever after.

In a post-credits scene, Gail survives (at the end of the film) and later escapes as she tries to make Guy see reason, but fails. He says theyll see what the school board thinks when he "gives them a ring". Bev tells Gail to give it up, as she too has had enough of her mothers manipulation and is possibly feeling guilty for hurting Katie as well. Still determined to make something of herself, Gail is then seen singing Katies song ("Make You Believe") at a ranch, but she is booed and laughed offstage by the audience.

==Cast==
*Lucy Hale as Katie Gibbs, the Cinderella character in the film.
*Freddie Stroma as Luke Morgan, the Prince Charming character in the film.
*Missi Pyle as Gail Van Ravensway, the Stepmother character in the film.
*Megan Park as Beverly "Bev" Van Ravensway, the Stepsister character in the film. Manipulative, vindictive and cruel, Beverly is more favored by Gail than Victor.
*Jessalyn Wanlim as Angela, Katies best and only friend since both her mother and father died, the Fairy Godmother in the film.
*Matthew Lintz as Victor Van Ravensway, the Stepbrother character in the film, and the first Stepbrother. He is kinder to Katie, but acts mean around her out of fear of how Gail and Beverly would treat him. In the end, Victor realizes what they were doing to Katie was wrong and exposes them for the fraud with Ravis help.
*Manu Narayan as Ravi/Tony Gupta.
*Titus Makin Jr. as Mickey O Malley, the Duke character in the film.
*Dikran Tulaine as Guy Morgan, the Kingly character in the film.
*Onira Tares as Crazy Girl, the Obsessed Fan.

==Production==
The film was shot in Wilmington, North Carolina, at notable locations such as Airlie Gardens, downtown Wilmington, St James Parish, the Graystone Inn and the campus of University of North Carolina Wilmington|UNCW.

==Soundtrack==
{{Infobox album  
| Name       = A Cinderella Story: Once Upon a Song
| Cover      =
| Artist     = Various artists
| Type       = Soundtrack
| Released   =   Digital download
| Recorded   = 2011
| Genre      = Pop music|Pop, pop rock
| Length     =
| Label      = WaterTower Music
| Producer   =
| Chronology = A Cinderella Story soundtracks Another Cinderella Story (2008)
| This album = A Cinderella Story: Once Upon a Song (2011)
| Next album =
}}

{{Album ratings rev1 = AllMusic rev1Score =  
}}

A Cinderella Story: Once Upon a Song is a soundtrack album by the film of the same name, released on September 6, 2011 by WaterTower Music (The same day as the films release).

;Track listing
{{Track listing
| extra_column = Performer(s)
| title1 = Run This Town 
| extra1 = Lucy Hale
| title2 = Bless Myself
| extra2 = Lucy Hale
| title3 = Make You Believe
| extra3 = Lucy Hale
| title4 = Knockin
| extra4 = Freddie Stroma
| title5 = Extra Ordinary
| extra5 = Lucy Hale
| title6 = Oh Mere Dilruba
| extra6 = Manu Narayan
| title7 = Possibilities
| extra7 = Freddie Stroma   
| title8 = Twisted Serenade
| extra8 = Big Pain Ticket  	 
| title9 = Knockin
| extra9 = Oral Majority
| title10 = Crazy Girl
| extra10 = The Co-Writes
}}

 

==Critical reception==

The film received mixed reviews although it did not get criticized by   or Victorious or any of those other tween girl shows that combine broad physical humor, mild expressions of pubescent sexuality and fantasies of becoming famous".  The DVD Sleuth gave the film 1 out of 2. 

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 