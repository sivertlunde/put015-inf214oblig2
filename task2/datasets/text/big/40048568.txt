The Broken Circle Breakdown
 
{{Infobox film
| name           = The Broken Circle Breakdown
| image          = The Broken Circle Breakdown.jpg
| caption        = Film poster
| director       = Felix Van Groeningen
| producer       = 
| writer         = Felix Van Groeningen
| starring       = Veerle Baetens Johan Heldenbergh
| music          = 
| cinematography = Ruben Impens
| editing        = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Belgium
| language       = Flemish
| budget         = 
}} Best Foreign Language Film at the 86th Academy Awards,       and was nominated.   

==Cast==
* Veerle Baetens as Elise Vandevelde/Alabama
* Johan Heldenbergh as Didier Bontinck
* Nell Cattrysse as Maybelle
* Geert Van Rampelberg as William
* Nils De Caster as Jock
* Robbie Cleiren as Jimmy
* Bert Huysentruyt as Jef
* Jan Bijvoet as Koen
* Blanka Heirman as Denise

==Plot==
The film is set in Ghent, Belgium, and explores the lives of Didier and Elise over seven years as they fall in love through their passion for bluegrass music. They have a daughter, Maybelle, who develops cancer after her sixth birthday, and succumbs to it within a year. The outcome of Maybelles illness has a devastating effect on their relationship and their lives.

==Production==
The film was shot from 18 July to 8 September 2011 in Belgium. 

==Soundtrack==
# Will the Circle Be Unbroken? See also Can the Circle Be Unbroken (By and By)	 
# The Boy Who Wouldnt Hoe Corn	by Alison Krauss, Pat Brayer, Jerry Douglas, Dan Tyminski, Barry Bales, and Ron Block 
# Dusty Mixed Feelings	 
# Wayfaring Stranger	 
# Ruebens Train
# Country In My Genes	 
# Further On Up The Road
# Where Are You Heading, Tumbleweed?	
# Over In The Gloryland	 
# Cowboy Man	 
# If I Needed You	
# Carved Tree Inn	 
# Sandmountain	 
# Sister Rosetta Goes Before Us	 
# Blackberry Blossom

==Accolades ==
{| class="wikitable" width=
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Recipient
! Outcome
|- 86th Academy Academy Awards Academy Award Best Foreign Language Film
|The Broken Circle Breakdown
| 
|- 39th César Awards|César Awards       Best Foreign Film
|The Broken Circle Breakdown
| 
|- 2013 Denver Denver Film Critics Society  Best Foreign Language Film
|The Broken Circle Breakdown
| 
|- 26th European European Film Awards   European Film European Film
|The Broken Circle Breakdown
| 
|- European Film European Director Felix Van Groeningen
| 
|- European Film European Actress Veerle Baetens
| 
|- European Film European Actor Johan Heldenbergh
| 
|- European Film European Screenwriter Carl Joos and Felix van Groeningen
| 
|-
|Peoples Choice Award  
|The Broken Circle Breakdown
| 
|- San Diego Film Critics Society San Diego Best Foreign Language Film
|The Broken Circle Breakdown
| 
|- 18th Satellite Satellite Awards Satellite Award Best Foreign Language Film
|The Broken Circle Breakdown
| 
|- Tribeca Film Festival Best Actress in a Narrative Feature Film Veerle Baetens
| 
|- Best Screenplay for a Narrative Feature Film Carl Joos, Felix Van Groeningen
| 
|- Washington D.C. Area Film Critics Association  Best Foreign Language Film
| The Broken Circle Breakdown
|  
|}

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Belgian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 