Defying Gravity (1997 film)
 
{{Infobox Film
| name           = Defying Gravity
| image          = Defying_Gravity_(film).jpg
| caption        = 
| director       = John Keitel
| producer       = Jack Koll David Clayton Miller  
| writer         = John Keitel
| starring       = Daniel Chilson Niklaus Lange Don Handfield Linna Carter 
| music          = 
| cinematography = Tom Harting
| editing        = Matthew Yagle
| distributor    = Wolfe Video
| released       = 1997
| runtime        = 92 minutes
| country        =  
| awards         =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} independent gay-themed romantic drama film|drama. Filmed in just 13 days using a cast largely of first-time actors, the film played the gay and lesbian film festival circuit in 1997 and 1998. It is John Keitels first film as a writer-director. 

==Plot== fraternity and frat house. He has a bunk bed in the room he shares with his best friend Todd Bently, Doogie and his pledge Stewy. Another of his fraternity brothers, Pete Bradley, has moved out of the frat house and into a house he shares with other students. Griff and Pete have a secret sexual relationship, but Griffs close-knit fraternity life puts a strain on it. Griff is satisfied with the arrangement, but Pete is not. Pete wants them to date as a romantic couple, and one night he gets Griff to agree to go on a date with him. 

Griff is annoyed to discover that Pete has tricked him into meeting at a gay coffeehouse. He runs into Sam, an out, loud, and proud activist, who is passing out flyers for a "community action patrol" to help prevent gay bashing. The juxtaposition of "closeted" and "out" gay people heightens the drama and serves as comic relief at the same time. The "date" ends with Griff telling Pete that he wants no part of the lifestyle displayed by the coffeehouses clientele. They both leave and separate in anger with Pete walking up a dark alley and Griff getting into his Jeep. Griff then notices a black sport utility vehicle|truck, going up the alley after Pete.
 attacked and is comatose in the local hospital. Griff is obviously shocked and disoriented, but the others are concerned about the negative impact on their upcoming rush week of having a gay member of their fraternity. At a special "house meeting" Buchanan, the head of the house, tells the others that there is a criminal investigation of Petes attack. The response of some of the fraternity brothers is anything but sympathetic to Pete.

When Griff and Todd go to the hospital to see about Pete, they are questioned by Detective Horne, who is investigating the attack, but Griff is silent about being with Pete that night because he would be outing himself at the same time. His deep love for Pete is apparent moments before when he breaks down silently in a stall in the mens room.

The tragic situation completely changes Griff: he drifts along in a daze, ignoring his friends, classwork, and fraternity responsibilities. He goes to the coffeehouse, the hospital, the place where Pete was attacked, and Petes home. At the coffeehouse he sees Denetra, an African-American fellow student, and his need to talk with a sympathetic listener motivates him to become friends with her.

Griffs continued preoccupation over Pete causes him to forget what he needs to do for the rush party: make sure that the house is well-stocked with alcohol and contact a sorority to invite them to the party. An emotional confrontation between the head of the house and Griff has Todd decide to take a time out with his troubled friend.
 truck going up the alley after him.

When Griff and Todd get back to the frat house they see Doogies friend Smitty there with his black truck. Griff realizes that that was the truck that followed Pete, and Todd remembers that Doogie and Stewy were with Smitty the night Pete was attacked. They were Petes attackers. Stewy admits to the attack when Griff confronts Doogie in the game room. A surprised Denetra walks by the frat house while the police escort Doogie and Stewy out in handcuffs.

There is nothing left for Griff to do but move out of the fraternity house and into Petes place. Griff is called when Pete comes out of his coma. When he is alone with Pete Griff promises him that what happened will never happen again, and he tells Pete he needs his help in figuring things out as they make their life together.

When Pete has fully recovered he and Griff double-date with Todd and Heather at a football game. As couples they appear detached from the fraternity group that is barbecuing near the stadium. Denetra drives up with her date Loretta, whom Heather knows from her English class, and they all go to the game together. In the final scene, which follows during the course of the credits, Pete is shown reading in bed with Griff playfully joining him: Petes dream becomes a reality.

==Cast==
* Daniel Chilson as John Griff Griffith
* Niklaus Lange as Todd Bentley
* Don Handfield as Pete Bradley
* Linna Carter as Denetra Washington
* Seabass Diamond as Matthew Doogie McDougal
* Lesley Tesh as Heather
* Ryan Tucker as Gary Buchanan
* Erika Cohen as Detective Horne
* Laura Fox as Mrs. Bradley
* Kevin P. Wright as Mr. Bradley
* Matt Steveley as Stewart Stewy Hanson
* Steven Burrill as Smitty
* Renee Kelly (billed Renee Eloise) as Loretta

==Awards==
Wins
* Austin Gay & Lesbian International Film Festival aGLIFF Award Best Narrative 1997
* Austin Gay & Lesbian International Film Festival aGLIFF Award Best Narrative Feature 1997 
* Cinequest San Jose Film Festival Audience Favorite Choice Award 1997
* Cinequest San Jose Film Festival Audience Favorite Choice Award 1998

Nominations
* Verzaubert - International Gay & Lesbian Film Festival Rosebud Award Best Film 1998

==DVD release==
Defying Gravity was released on Region 1 DVD on April 11, 2000.

==Notes==
 

==External links==
*  

 
 
 
 
 
 
 