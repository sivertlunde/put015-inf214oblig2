Sikandar (2009 film)
{{Infobox Film
| name           = Sikandar
| image          = Sikandar, 2009 film poster.jpg  
| image_size     = 
| caption        =  
| director       = Piyush Jha
| producer       = Sudhir Mishra
| writer         = Piyush Jha
| starring       = Parzan Dastur Ayesha Kapoor R. Madhavan Sanjay Suri
| music          = Shankar-Ehsaan-Loy Sandesh Shandilya Justin-Uday
| cinematography = Somak Mukherjee
| editing        = Dev Jadhav
| line producer  = Alan McAlex
| distributor    = BIG Pictures
| released       = August 21, 2009
| runtime        = 110 minutes
| country        = India Hindustani
| budget         = 
| gross          = 
}} terrorism in the Indian state of Jammu & Kashmir as its backdrop.  It was known in its production stages as Foot Soldier.  The film was released on 21 August 2009.

==Plot==
Sikandar Raza (Parzan Dastur) is a 14-year-old schoolboy in the Kashmir valley. Ever since his parents were killed by jihadis (from Azad Kashmir) (militants) 10 years ago, he has lived with his aunt and uncle, in a small town called Kupwara in Kashmir valley. All Sikandar’s desires revolve around the happiness of his foster parents and getting the ball into the goal on the football field. 

One day, on his way to a school football match, Sikandar finds a gun lying on the path. Despite admonishments by his newly made school friend, 14-year-old Nasreen (Ayesha Kapoor), Sikandar picks up the gun and begins a journey into the darker side of his nature. The quiet-yet-strong Nasreen becomes Sikandar’s conscience keeper. She tries to dissuade him from giving in to the lure of the gun. 

Sikandar gets embroiled further and further in situations beyond his control, and people get killed. At first it seems that the happenings occurring alongside Sikandar’s predicament are not connected. But, as layer upon layer is revealed, it becomes clear that Sikandar is the innocent victim in a game being played out between the militants, the army, the peace-bartering politicians and the religious heads of the little Kashmiri town. The pieces of the puzzle come together at the very end, leading to a shocking revelation.  The movie is a portrayal of how child psychologies can be moulded, how terrorists are made. "Bholi bhaali rahne do" by Prasoon Joshi wonderfully sums up the movie. 

==Cast==
{| class="wikitable"
|-
| Parzan Dastur || Sikandar
|-
| Ayesha Kapoor || Nasreen
|-
| R. Madhavan || Rajesh Rao
|-
| Sanjay Suri || Mukhtar Masoodi
|-
| Arunoday Singh || Zahageer Qadir
|- Rahul Singh || Captain Yashvardhan Singh
|}

==Soundtrack==
{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Duration !! Singer(s) !! Composer
|- Allah Hoo||4:57||Hrishikesh Kamerkar, Yash Narvekar|| Justin-Uday
|- Arzoo - Naat||2:00||Mehrajuddin|| Shankar-Ehsaan-Loy
|- Chaal Apni||3:29||Hamsika Iyer, Hrishikesh Kamerkar|| Justin-Uday
|- Dhoop Ke Shankar Mahadevan, Anousha Mani||Shankar-Ehsaan-Loy
|- Gulon Mein||4:58||Mohit Chauhan||Justin-Uday
|- Gulon Mein KK (singer)|K.K.||Sandesh Shandilya
|- Shilpa Rao||Justin-Uday
|-
|}

==Reception==
Nikhat Kazmi, film critic of the Times of India, gave it a 3.5 star rating. 
The movie is loosely based on Apt Pupil, a Stephen King novel.

==References==
 

==External links==
*  , official site
*  
*  

 
 
 
 
 
 