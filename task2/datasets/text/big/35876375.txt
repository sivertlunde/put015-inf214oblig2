Lentera Merah
{{Infobox film
| name           = Lentera Merah
| image          = Lentera Merah DVD cover.jpg
| image_size     = 
| caption        = Malaysian DVD cover
| director       = Hanung Bramantyo
| producer       = Subagio S. Gope T. Samtani Sunil Samtani
| writer         = Hanung Bramantyo
| starring       = Laudya Cynthia Bella Dimas Beck Firrina Sinatrya Teuku Wisnu Fikri Ramadhan Kartika Indah Pelapory Saputra Tesadesrada Ryza Beauty Oehmke Auxilia Paramitha Zainal Arifin
| music          = Wiwiex Soedarno
| cinematography =
| editing        = Andi Pulung
| distributor    = Rapi Films
| released       =  
| runtime        = 100 minutes
| country        = Indonesia
| awards         = Indonesian
| budget         =
| gross          =
}}
Lentera Merah (English translation: Red Lantern) is a 2006 Indonesian horror film directed by Hanung Bramantyo.

==Plot==
An employee of Lentera Merah, the campus newspaper of the University of Indonesia, is found dead in the office with the number 65 written in blood by his body. In the meantime, five students compete for a position at the publication: Risa Priliyanti (Laudya Cynthia Bella), Riki (Tesadesrada Ryza), Lia (Beauty Oehmke), Muti (Auxilia Paramitha) and Yoga (Zainal Arifin).
 hanged in the library. The rector forbids the test after these murders are known, but chief editor Iqbal (Dimas Beck) insists that the five students go through the initiation rites to show their mettle as journalists. Lia and Risa go with staff photographer Bayu (Saputra).

After several more staff members are killed, it is discovered that Risa is in fact the ghost of a former Lentera Merah staff member who was killed in 1965 after being accused of being a communist sympathiser; her body was later buried under the floor of one of the buildings. They realise that Risa is killing them because they are related to the staff members who killed her. Iqbals father, a Lentera Merah staff member in 1965, comes to the campus to warn his son. As the remaining students leave to find Risas body and give it a proper burial, Risa appears and strangles Iqbals father. Having found closure, she vanishes.

==Production==
Lentera Merah had a screenplay by Gina S. Noer.  According to Evi Mariani of The Jakarta Post, the title is reminiscent of Soe Hok Gies history of the Indonesian Communist Party, Di Bawah Lentera Merah (Beneath the Red Lantern). 
 Musashi who political right, Lentera Merah was to the Left-wing politics|left. 

The film had a low budget  but spent Indonesian Rupiah|Rp. 15-20 million (US$ 2,000-2,500) for the make-up of one actor. 

==Release and reception==
Lentera Merah was released in mid-May 2006,  a period where Indonesian theatres were filled with local horror films. It was viewed by 300,000 persons. 

A write-up in Tempo (Indonesian magazine)|Tempo magazine called the films historical research weak, and found that as a whole the combination of history and horror did not work well. 

==Awards==
Lentera Merah was nominated for two Citra Awards at the 2006 Indonesian Film Festival but did not win any. 
{| class="wikitable" style="font-size: 95%;"
|-
! scope="col" | Award Year
! scope="col" | Category
! scope="col" | Recipient
! scope="col" | Result
|-
! scope="row" rowspan="2" | Indonesian Film Festival
| rowspan="2" | 2006 Best Artistic Arrangement Allan Sebastian
| 
|-
| Best Music Arrangement
| Wiwiex Sudarno
|  
|}

 
==References==
;Footnotes
 

;Bibliography
 
* {{cite news
  | last1 = Agusta
  | first1 = Paula
  | date=18 August 2011
  | title = Gina S. Noer: Making better filmmakers
  |work= The Jakarta Post
  | url = http://www.thejakartapost.com/news/2011/08/18/gina-s-noer-making-better-filmmakers.html
  | accessdate =20 May 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/67n14N0DC
  |archivedate=20 May 2012
  }}
* {{cite news
  | last1 = Mariani
  | first1 = Evi
  | date=23 March 2008
  | title = Hanung Bramantyo: Hitting the right marks
  |work= The Jakarta Post
  | url = http://www.thejakartapost.com/news/2008/03/23/hanung-bramantyo-hitting-right-marks.html
  | accessdate =28 November 2011
  | ref =  
  |archiveurl=http://www.webcitation.org/63WexgCqZ
  |archivedate=28 November 2011
  }}
* {{cite news
  | last1 = Mariani
  | first1 = Evi
  | date=2 June 2006
  | title = Young director Hanung to film 1965 victims
  |work= The Jakarta Post
  | url = http://www.thejakartapost.com/news/2006/06/02/young-director-hanung-film-1965-victims.html
  | accessdate =28 November 2011
  | ref =  
  |archiveurl=http://www.webcitation.org/63Wztehxa
  |archivedate=28 November 2011
  }}
*{{cite web
 |title=Penghargaan Lentera Merah
 |trans_title=Awards for Lentera Merah
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-l035-06-178534_lentera-merah-kebenaran-harus-terungkap/award#.T7hFe1KVzMw
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=20 May 2012
 |archiveurl=http://www.webcitation.org/67n2BcRPY
 |archivedate=20 May 2012
 |ref= 
}}
* {{cite news
  | last1 = Rizal S.
  | first1 = Yos
  | last2 = Fadjar
  | first2 = Evieta
  | last3 = Dewanto
  | first3 = Andi
  | last4 = Evi
  | first4 = Yandi M.R.
  | date=29 January 2007
  | title = Musim Panen Film Horor
  |trans_title=Harvest Season for Horror Films
  |language=Indonesian
  |work= Tempo
  | url = http://majalah.tempointeraktif.com/id/arsip/2007/01/29/LYR/mbm.20070129.LYR122990.id.html
  | accessdate =20 May 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/67n1Py47k
  |archivedate=20 May 2012
  }}
 

==External links==
* 

 

 
 
 
 
 