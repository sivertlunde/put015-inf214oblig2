State Rowdy
{{Infobox film
| name           = State Rowdy
| image          = State Rowdy poster.jpg
| caption        = Movie poster
| director       = B. Gopal
| producer       =T. Subbarami Reddy
| writer         = Radha Bhanupriya Sharada Thiagarajan
| music          = Bappi Lahiri 
| cinematography = S. Gopal Reddy
| editing        =
| distributor    =
| released       = March 23, 1989
| runtime        =
| country        = India Telugu
| budget         =
}} Telugu film directed by B. Gopal. The film stars Chiranjeevi in the lead, while Radha (actress)|Radha, Bhanupriya, Sharada (actress)|Sharada, Ragdfgo Gopal Rao, Thiagarajan and Nutan Prasad are among the other cast.

==Plot==
Kaali (Chiranjeevi) is a Rowdy who kidnaps all the leading rowdies from various parts of the state working under two rivals Rao Gopal Rao and Nutan prasad, and makes them do various good jobs.
Bhanupriya has interest in him and keeps helping him in his deeds. To get rid of this ‘State Rowdy’, the villains come to know that he has a mother and cousin (Radha) and inform them on his whereabouts. It is known that Kaali is actually Prudhvi and was an aspiring candidate for becoming a police officer but does not get job due to Sharada though he performs well in interview and all the tests. He becomes anti-law due to this and turns as state rowdy to get rid of all the rowdies himself by taking law into his own hands. When his mother and Radha see him, he is forced to reveal the secret that he is actually a police informer working for Sharada to bring criminals to justice. Sarada has lost her husband and daughter in fighting the villains. Later it is learnt that Bhanupriya is Sarada’s daughter and she gets accused of murdering Rao Gopal rao’s younger brother, Thiagarajan. Rest of the story forms on how she is acquitted with help of chiru and how Kaali and sarada get rid of their enemies by bringing them to justice.

==Cast==

*Megastar Chiranjeevi Radha
*Bhanupriya
*Rao Gopal Rao Sharada
*Thiagarajan

==External links==
* 

 
 
 

 
This movie is a 300 days block buster in Shimoga, Karnataka