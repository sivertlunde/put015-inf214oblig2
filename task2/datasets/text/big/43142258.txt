Cotton Candy (film)
{{Infobox film
| name           = Cotton Candy
| director       = Ron Howard
| music       = Joe Renzetti
| writer         = {{plainlist|
* Ron Howard
* Clint Howard}}
| starring       = {{plainlist|
* Charles Martin Smith
* Clint Howard
* Leslie King
* Kevin Lee Miller}}

| released       = October 28, 1978  (US) 
| country        = United States
| language       = English
}}

Cotton Candy is a 1978 American TV movie directed by Ron Howard; the film was made for TV and syndicated on NBC.  }}  Also known as Ron Howards Cotton Candy.   

== Plot ==
A high school football player is cut from the team and begins looking for direction in life. A songwriter in his spare time, he recruits other local teens to form a rock band and ultimately perform in the towns Battle of the Bands competition.

== Cast ==
* Charles Martin Smith as George Smalley
* Clint Howard as Corky Macpherson
* Leslie King as Brenda Matthews
* Manual Padilla Jr. as Julio Sanchez
* Kevin Lee Miller as Barry Bates
* Dean Scofield as Bart Bates
* Rance Howard as Mr. Bremmercamp
* Mark Wheeler as Torbin Bequette
* Alvy Moore as Mr. Smalley
* Joan Crosby as Mrs. Smalley

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 

 