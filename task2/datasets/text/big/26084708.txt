Straight into Darkness
{{Infobox film
| name           = Straight into Darkness
| image          = Straightintodarkness.jpg
| image_size     =
| caption        =
| director       = Jeff Burr
| writer         =
| narrator       =
| starring       = Ryan Francis
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 95 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 2004 war Scott MacDonald.

==Plot==
When two American GIs desert their platoon in the final days of World War II, they find themselves struggling against all odds to stay alive. Their journey brings them together with a band of orphans who are expertly trained killing machines to try to defeat a Nazi battalion. Produced by Mark Hanna and Chuck Williams.  Directed by Jeff Burr.

==Cast==
*Ryan Francis as Losey
*Scott MacDonald (actor)| Scott MacDonald as Demming
*Linda Thorson as Maria 
*James LeGros as Soldier
*Daniel Roebuck as Soldier David Warner as Deacon
*Liliana Perepelicinic as Anna
*Gabriel Spahiu as The Lunatic Priest
*Nelu Dinu as Nelu
*Mihai Verbintschi as Buchler
*Ion Bechet as Sergeant

==Reception==
The film received mixed reviews, including a 60% rating on Rotten Tomatoes. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 

 