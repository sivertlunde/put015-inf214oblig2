Hassan and Marcus
{{Infobox Film
| name           = Hassan and Marcus
| image          = Hasan wa Murqos Poster (2008).jpg
| caption        = Promotional Poster
| director       = Ramy Imam
| eproducer      =
| aproducer      =
| starring       = Adel Emam Omar Sharif Lebleba Mohamed Imam Omar Sharif, Jr.
| music          = 
| distributor    = Good News Group
| released       =  
| country        = Egypt
| language       = Arabic
}}
Hassan and Marcus ( ) is an Egyptian film released in 2008.

==Background== conservatives and religious extremists who consider it blasphemy|blasphemous.

==Plot== Christian Priest religious extremists on both sides, the Egyptian government inducts them into a witness protection program that requires them to disguise themselves as the Christian Marcus and a Muslim Sheikh, Hassan el-Attar, respectively.

When, unwittingly, they move into the same building, a friendship blossoms that must, along with a romance between the protagonists children, withstand the difficulties of prejudice and social persecution.

Hassan and Morcos doesnt attempt to name the reasons for the tension between Christians and Muslims. But according to the political writer and Coptic Christian Sameh Fawzi, the conflicts have nothing to do with religion.

==Cast==
* Adel Emam as Boulos \ Hassan el Attar
* Omar Sharif as Mahmoud \ Marcus
* Lebleba as Matilda \ Zeenat
* Hana El Shorbagy as khairia
* Mohamed Imam as Gerges \ Emad
* Shery Adel as Mariam \ Fatima

==Themes==
The film addresses issues of religious extremism, intolerance and sectarian violence, and emphasises the possibility of friendship and love between members of different religions. 

Imam said of the film:   
:"I have declared war using art against the extremists - against those who foment differences between us. I hope Christians and Muslims will leave the cinema and embrace one another." 

== Criticism ==
Muslim movie star Adel Imam was accused of apostasy by Facebook activists over his role as a Coptic Christian priest in the film. The group accused him of promoting Christianity and discouraged Muslims from seeing the film. The groups mission statement read “This man is promoting conversion to Christianity and I am calling upon you to boycott him." Another group was also created for the same purpose under the title “Boycott Imam’s new movie.” The criticism of Imam occurred during a tense period marked by violent clashes between Muslims and Coptic Christians in Egypt.

==References==
 

==External links==
*  
* 
* 
* 

 
 
 
 
 
 