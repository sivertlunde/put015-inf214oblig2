Out for Justice
{{Infobox film
| name           = Out for Justice
| image          = OutforJustice91.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster John Flynn
| producer       = {{Plainlist|
* Steven Seagal 
* Arnold Kopelson
}}
| writer         = David Lee Henry
| starring       = {{Plainlist|
* Steven Seagal William Forsythe
* Jerry Orbach
* Jo Champa
}}
| music          = David Michael Frank
| cinematography = Ric Waite
| editing        = {{Plainlist|
* Don Brochu
* Robert A. Ferretti
}}
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $14,000,000
| gross          = $39,673,161
}}
 John Flynn, mafioso who murdered his partner. Out for Justice marks Julianna Margulies film debut.

== Plot == NYPD detective from Brooklyn who has strong ties within his neighborhood.
 William Forsythe) murders Bobby in broad daylight in front of his wife, Laurie, and his two children.
 crack addict who grew up with Gino and Bobby. He has become psychotic and homicidal due to rage and drug use, and seems not to care about the consequences of his actions. Richie then murders a woman at a traffic stop because she tells him to move the car. He heads off into Brooklyn alongside his goons, who are horrified by what he does but continue to work alongside him.

Gino knows Richie is not going to leave the neighborhood and he tells his captain, Ronnie Donziger (Jerry Orbach) to give him the clearance for a manhunt. Donziger gives him the clearance, and provides him with an Ithaca 37 pump action 12 gauge shotgun, and an unmarked 1988 Chevrolet Caprice tactical unit. Gino visits his mob connection Frankie and his boss Don Vittorio to tell them that he is going to find Richie, and he tells them he will not get out of the way of their own plans to take out Richie, whom they view as a loose cannon. As a subplot, Gino is driving when the driver of a station wagon throws a black garbage bag out the window in front of Ginos car. Seeing that the bag is moving, Gino stops, gathers the bag and is surprised to find a very scared german shepherd puppy inside of it. He decides to keep it as a pet.

Gino starts the hunt for Richie at a bar run by Richies brother Vinnie Madano. Vinnie and his friends all refuse to provide information, so Gino beats up all of them. He still does not know where Richie is, but his concern about getting an attitude problem has been taken care of. Richie later comes back to the bar and beats up Vinnie for not killing Gino when it was one cop against a bar full of armed men. He also has info leaked to the mob that he is at the bar, then emerges from hiding and ambushes the mobs hitmen in a shoot-out.

After visiting a number of local hangouts and establishments trying to find information, Gino discovers Richie killed Bobby because Bobby was having an affair with two women - Richies girlfriend, Roxanne Ford (Julie Strain), and a waitress named Terry Malloy (Shannon Whirry). When Gino goes to Roxannes home, he finds she is dead. Gino believes that Richie killed Roxanne before he killed Bobby. Gino goes to Lauries house and tells the widow what is going on. In Lauries purse, Gino finds the picture that Richie dropped on Bobbys body after killing Bobby. It turns out that Bobby was a corrupt cop who had wanted a money-making lifestyle like Richies, and Laurie knew Bobby was corrupt. Laurie had found a picture of Bobby and Roxanne naked. She had given Richie the picture out of jealousy, never expecting Richie to kill Bobby for sleeping with Roxanne. Laurie took the picture away from where Richie dropped it on Bobby because she wanted to protect Bobby.

Gino attempts to get Richie out of hiding by arresting his sister Pattie (Gina Gershon) and by talking to his elderly father, a hard-working and honest man who knows his son is worthless. Following a tip from his local snitch Picolino, Gino eventually finds Richie in a house in the old neighborhood having a party. Gino kills or wounds all of Richies men. Gino then finds Richie and beats him senseless, finally killing him by stabbing him in the forehead with a corkscrew. The mobsters arrive soon after, also intent on killing Richie. Gino uses the lead mobsters gun to shoot the already-dead Richie several times, then tells him to return to his boss and take the credit for Richies death.

Shortly after, Gino, his wife, and the puppy are walking along the pier when they encounter the "tough guy" who threw the bag into traffic. Gino accosts him, and the man attacks him in response. Gino kicks him in the groin, causing him to collapse on the ground in agony. Gino and Vicki laugh as the puppy runs over to him and urinates on him as he lays on the ground.

==Cast==
* Steven Seagal as Detective Gino Felino William Forsythe as Richie Madano
* Anthony DeSando as Vinnie Madano
* Dan Inosanto as Sticks
* Jerry Orbach as Captain Ronnie Donziger
* Jo Champa as Vicky Felino
* Shareen Mitchell as Laurie Lupo
* Ronald Maccone as Don Vittorio
* Sal Richards as Frankie
* Gina Gershon as Pattie Madano
* Jay Acovone as Bobby Arms
* Nick Corello as Joey Dogs
* Kent McCord as Jack
* Robert LaSardo as Bochi
* John Toles-Bey as King
* Joe Spataro as Bobby Lupo
* Julianna Margulies as Rica
* Dominic Chianese as Mr. Madano
* Vera Lockwood as Mrs. Madano
* Dennis Karika as The Trainer
* Ed Deacy as Detective Deacy
* John Leguizamo as Boy in Alley
* Jorge Gil as Chas the chair

==Soundtrack==
{{Infobox album  
| Name = Out for Justice
| Type = soundtrack
| Artist = various artists
| Cover = 
| Released = 1991 CD
| Recorded = 
| Genre = Southern Rock, Rap
| Length = 
| Label = Varèse Sarabande
| Producer = 
}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =    
}}
{{track listing
| extra_column    = Artist

| title1          = Dont Stand In My Way
| extra1          = Gregg Allman
| length1         = 4:10

| title2          = Shake the Firm
| extra2          = Cool J.T.
| length2         = 3:28

| title3          = Bad Side of the Town
| extra3          = Sherwood Ball
| length3         = 3:56

| title4          = When The Night Comes Down
| extra4          = Todd Smallwood
| length4         = 2:26

| title5          = Puerto Riqueño
| extra5          = Michael Jiminez
| length5         = 2:36

| title6          = Dime Corazon
| extra6          = Ali Olmo
| length6         = 3:54

| title7          = Temptation
| extra7          = Teresa James
| length7         = 4:02

| title8          = Long Way Home
| extra8          = Louis Price
| length8         = 4:10

| title9          = The Bigger They Are (The Harder They Fall)
| extra9          = Cool J.T.
| length9         = 2:46

| title10         = One Good Man
| extra10         = Kimberli Armstrong
| length10        = 3:30

| title11         = No Sleep Till Brooklyn
| extra11         = Beastie Boys
| length11        = 4:09

| title12         = Main Title 
| extra12         = David Michael Frank
| length12        = 4:02

| title13         = One Night in Brooklyn
| extra13         = David Michael Frank
| length13        = 3:39

| title14         = Final Encounter
| extra14         = David Michael Frank
| length14        = 4:25
}}

==Production==
John Flynn later claimed the original title was The Price of Our Blood, "meaning Mafia blood. That was the title that Steven and I wanted, but Warner Bros. said no. It had to be a three-word title like the other Steven Seagal films (Above the Law and Marked for Death)."   accessed 16 February 2015 

The movie was originally much longer and included more plot and characters. Steven Seagal cut some of William Forsythes scenes because he felt that Forsythe was upstaging him. Also, Warner Bros. brought in editor Michael Eliot to re-edit the original cut of the movie so that it could be shorter and more profitable on box office. Eliot did the same job on couple other Warner Bros. movies; Wes Cravens sci-fi horror Deadly Friend (1986) and Mark L. Lesters action movie Showdown in Little Tokyo (1991). Some scenes were deleted and some others were cut for pacing, this is why there are two montage scenes with no dialogue in the movie. Re-editing also caused some minor continuity mistakes. Theatrical trailer shows two deleted scenes; Richie shooting inside clothing store from which he took new shirt (in his first few scenes he wears one shirt then all of a sudden he wears different shirt in rest of the movie) and scene where police captain tells Gino that body count is going up. Some TV versions of the movie included two deleted scenes; Richie stealing the new shirt from store because his got blood on it (also seen in trailer), and Richie and his guys breaking into the house where Ginos wife is and trying to find her but they leave when some neighbours show up.

Flynn later recalled:
  I really liked working with Bill Forsythe and Jerry Orbach and all those guys in the car who played the killers. But I didn’t get along with Steven. He was always about an hour late for work and caused a lot of delays. We shot until October 31, 1990, because an IATSE strike was threatened. (IATSE stands for International Alliance of Theatrical Stage Employes, Moving Picture Technicians, Artists and Allied Crafts. - Ed.) Warner Bros. told us we had to be on a plane by November 1. So we shot for about a month in Brooklyn. The rest of Out for Justice was shot in and around south Los Angeles. We filmed those scenes on Lacy Street, in a slummy area of old wooden buildings that could pass for Brooklyn.  

Whilst on the production set, Seagal claimed that due to his Aikido training, he was immune to being choked unconscious. It has been alleged that at some point Gene LeBell (who was a stunt coordinator for the movie) heard about the claim, and gave Seagal the opportunity to prove it. LeBell is said to have placed his arms around Seagals neck, and once Seagal said "go", proceeded to choke him unconscious.  After refusing to comment for many years, LeBell confirmed the story in 2012 and said that after Seagal fell unconscious, he proceeded to defecate and urinate himself.  Whenever Seagal has been asked about the incident, he has constantly denied the allegations. 

==Reception==

The movie received generally negative along with some mixed reviews.  It was originally rated NC-17 for its brutal and graphic violence . Several cuts were made for the films release overseas. In the United Kingdom in particular, several of the gruesome action scenes were trimmed for the video release, cutting the duration by 54 seconds. It was later released uncut for DVD.

On Rotten Tomatoes, Out for Justice is currently rated at 19% on the Tomatometer, based on 21 reviews. 

===Box office===

The movie debuted at Number 1 at the box office.  This was the third straight Seagal movie to make number 1 at the U.S. box office on its opening weekend. In the United States the film grossed $40 million, falling short of the box office receipts of his last release, Marked for Death. 

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 