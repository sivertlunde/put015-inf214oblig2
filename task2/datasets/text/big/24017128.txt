Broken Highway
 
 
{{Infobox film
| name           = Broken Highway
| image          =
| caption        =
| director       = Laurie McInnes Richard Mason
| writer         = Laurie McInnes
| starring       = Aden Young
| music          =
| cinematography = Steve Mason
| editing        = Gary Hillberg
| distributor    =
| released       =  
| runtime        = 98 minutes
| country        = Australia
| language       = English
| budget         = A$1.35 million "Production Survey", Cinema Papers, August 1992 p73  gross = A$18,300 (Australia)
}}

Broken Highway is a 1993 Australian drama film directed by Laurie McInnes.    It was entered into the 1993 Cannes Film Festival.   

==Cast==
* Aden Young as Angel David Field as Tatts Bill Hunter as Wilson
* Claudia Karvan as Catherine
* Norman Kaye as Elias Kidd
* William McInnes as Roger Stephen Davis as Jack Dennis Miller as Max ODonnell
* Kris McQuade as Woman
* Peter Settle as Night Manager

==Production==
Laurie McInnes made the short film Palisade which won the Palme dOr at Cannes. She got development money from the Australian Film Commission to write a script.  The film was shot from 25 May to 10 July 1992. 

==Box office==
Broken Highway grossed $18,300 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 
 