Not a Feather, but a Dot
{{Infobox film
| name = Not a Feather, but a Dot
| image = Not a Feather, But a Dot Cover File.jpg
| caption = Theatrical release poster
| director = Teju Prasad
| producer = Julie Almendral
| music = Axel Belohoubek
| cinematography = Geoffrey Hug
| studio = Not a Feather, LLC
| runtime = 60 minutes
| country = United States
| language = English
}}
Not a Feather, but a Dot  is an hour-long documentary film on the history, perceptions, stereotypes and changes in the Indian American community.  The film discusses topics such as the growth of Hinduism in the United States, the origin of the stereotypes surrounding the Indian-American community, the early Indian migrants and events that have shaped the Indian-American community, and members of the Indian-American community that are changing "traditional" perceptions. It is narrated by filmmaker Teju Prasad who infuses his personal experience, historical analysis from academics, and experiences of other Indian Americans breaking "traditional ground." The film has been screened in New York, Washington DC,  New Brunswick, NJ, Durham, NC,    and San Francisco,  as well as the 2012 Jersey City Film Festival. 

==Cast==
*Vijay Prashad as himself
*Priya Anjali Rai as herself
*Sonia Dara as herself
*Kevin Negandhi as himself
*Sheetal Shah as herself
*Teju Prasad as himself

==References==
 

== External links ==
*  
*  

 
 
 

 