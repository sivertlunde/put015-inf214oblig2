The Flesh and Blood Show
{{Infobox film
| name           = The Flesh and Blood Show
| image          = The Flesh and Blood Show.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = American film poster
| film name      =  Pete Walker
| producer       = Pete Walker
| writer         = 
| screenplay     = Alfred Shaughnessy 
| story          = 
| based on       = 
| narrator       =  Ray Brooks Jenny Hanley Luan Peters Patrick Barr
| music          = Cyril Ornadel
| cinematography =  Peter Jessop
| editing        =
| studio         = Peter Walker (Heritage) Ltd.
| distributor    = 
| released       = 1972
| runtime        = 96 min.
| country        = England
| language       = English
| budget         = 
| gross          = 
}} Pete Walker. Ray Brooks, Jenny Hanley and Luan Peters.  

==Plot==
A anonymous producer assembles a group of unemployed actors and actresses to be in a play, rehearsing in an abandoned theatre beside the sea. A murderer, who wears black gloves, kills all of the actors in various ways. The murderer is later revealed to have previously been an actor, who trapped his wife and her new lover in the wall, re-emerging 30 years later to commit murder again.

==Production== Pavilion Theatre in Cromer, England. The films ending was originally filmed and shown in cinemas in 3-D. 

The films plot of young actors and actresses being murdered became a theme in the directors later work. His later horror films focused on youth and their culture being attacked by people who are older.    Walker mixed elements of sexploitation films and slasher films to direct this movie. 

The three films that the director is most known for came after this one - House of Whipcord, Frightmare (film)|Frightmare, and House of Mortal Sin. 

==Reception==
Bill Gribron, writing for DVD Verdict, said, "This film could use all the inventive help it can get. Otherwise, its as stiff as a British businessmans long-lamented upper lip".    Ian Jane, of DVD Talk, wrote, "While The Flesh & Blood Show has definitely got its fair share of obvious flaws, its nevertheless a fun and enjoyable horror movie with enough mildly sleazy thrills and odd characters to provide for some solid entertainment" 

==Home media==
The film was released on DVD by Shriek Show, a division of Media Blasters, in 2006. The special features are a picture gallery, a trailer, and an interview with the director. 

==Cast== Ray Brooks ...  Mike 
* Jenny Hanley ...  Julia Dawson 
* Luan Peters ...  Carol Edwards 
* Patrick Barr ...  Major Bell / Sir Arnold Gates
* Robin Askwith ...  Simon 
* Candace Glendenning ...  Sarah 
* Tristan Rogers ...  Tony Weller 
* Judy Matheson ...  Jane 
* David Howey ...  John 
* Elizabeth Bradley ...  Mrs. Saunders 
* Jess Conrad ...  Young Actor
* Rodney Diak ...  Warner 
* Penny Meredith ...  Angela 
* Sally Lahee ...  Iris Vokins 
* Raymond Young ...  Insp. Walsh Alan Curtis ...  Jack Phipps  Brian Tully ...  Willesden 
* Jane Cardew ...  Lady Pamela 
* Tom Mennard ...  Fred 
* Stewart Bevan ...  Harry Mulligan Michael Knowles ...  Curran

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 