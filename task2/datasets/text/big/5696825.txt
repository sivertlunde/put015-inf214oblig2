The Dark Half (film)
{{Infobox film
| name = The Dark Half
| image = DarkHalfPoster.jpg
| caption = Original 1993 theatrical poster
| director = George A. Romero
| producer = Declan Baldwin Christine Forrest George A. Romero
| screenplay = George A. Romero Paul Hunt Nick McCarthy
| based on = The Dark Half by Stephen King
| starring = Timothy Hutton Amy Madigan Michael Rooker Julie Harris Rutanya Alda
| music = Christopher Young
| cinematography = Tony Pierce-Roberts
| editing = Pasquale Buba
| studio = 
| distributor = Orion Pictures 
| released =   
| runtime = 122 minutes English
| budget = $15 million
| gross =  $10.6 million 
}} novel of the same name. The film was directed by George A. Romero and stars Timothy Hutton as Thad Beaumont and George Stark, Amy Madigan as Liz Beaumont, Michael Rooker as Sheriff Alan Pangborn and Royal Dano in his final film. 

==Synopsis==
The author of highbrow literary novels under his own name, Thad Beaumont (Timothy Hutton) is better known for the bestselling suspense-thrillers he writes under the pen name "George Stark". Beaumont wishes to retire the Stark name and symbolically buries Stark in a mock grave. 

However, Stark has become a physical entity (also portrayed by Hutton) and is terrorizing Beaumonts family and friends after he emerges from the grave. Stark then kills local photographer Homer Gamache and steals his truck. He also kills Thads editor, agent, and his agents ex-wife, and kills a man named Fred Clawson, who was trying to blackmail Thad for being a con artist that should not have written books under a false name.
 Castle Rock, Maine he had nothing to do with it. After putting an all-points bulletin on Clawson, who was accused of the death of Homer, the New York police find him castrated and his throat slit. They find a message on the wall, written in Clawsons blood, "The sparrows are flying again." Thad starts to think that he may have a psychic connection to the killer.

While in his office, Thad begins to receive messages from Stark, and begins to worry about the next victim. He and his family start to receive threatening phone calls from Stark. Pangborn initially suspects the phone calls are a prank by Thad until Stark begins to describe how he is going to kill Thads family, disturbing Pangborn.
 twin brother who died at "child birth."

His mother never told him about the twin, until a local doctor tells him that Stark is a fraternal twin that was living inside Thads brain. (A scene in the films start shows a developing fetus inside Beaumonts brain). Stark arrives, kills the doctor, and blames Thad for the crime. A local friend realizes that Stark is an entity controlled by the books that Thad wrote, and that he will do anything he can to stop him. Stark kidnaps Thads wife Liz and his children, and makes a deal with Thad: Finishing a book that has Stark living in the real world, or he will kill his family.

While writing the book, Thad notices Stark is healing himself with his own writings. Thad and Stark get into a fight, which ends with Thad stabbing Stark in the neck with a pencil. Thinking that it is over, Stark decides to kill Thads children instead. Sheriff Pangborn arrives and unties Liz, who says that Thad and Stark are upstairs. However, a huge flock of sparrows comes and tears Stark apart, and take him back to hell where he belongs. The sparrows are agents of Satan that come and collect evil souls that were not allowed to live. Thad and Liz are spared and they and Pangborn watch as the sparrows disappear into the night.

==Cast==

*Timothy Hutton as Thad Beaumont and George Stark
*Amy Madigan as Liz Beaumont
*Julie Harris as Reggie Delesseps
*Robert Joy as Fred Clawson
*Chelsea Field as Annie Pangborn
*Royal Dano as Digger Holt
*Rutanya Alda as Miriam Cowley
*Beth Grant as Shayla Beaumont
*Kent Broadhurst as Mike Donaldson
*Tom Mardirosian as Rick Crowley
*Glenn Colerider as Homer Gamache
*Michael Rooker as Sheriff Alan Pangborn

==Production== Old Main, seen at the beginning of the film as Beaumonts classroom, and the office of the college chaplain, used as Beaumonts office.     Members of the faculty and student body served as extras in the film.   

The film was Romeros second foray into filming with the support of a major film production company, posing issues for the notoriously low-budget director.  His first being Monkey Shines.

The film was shot from October 1990 until March 1991 and was in release limbo for two years due to Orion Pictures bleak financial situation. The film eventually saw release in April 1993, taking in just over $10 million domestically. 

==Reception==
In its opening week The Dark Half ranked in the box office charts at number 6, gathering a total of $3,250,883 from 1,563 theatres.  against type performance as Stark that "definitively shed his nice-guy image". However, Ebert faulted The Dark Half for failing to "develop its preternatural opening theme" and never offering a satisfactory explanation for Starks existence. 

==Awards==
*Academy of Science Fiction, Fantasy & Horror Films
**Saturn Award Best Director - George A. Romero - Nominated
**Saturn Award Best Horror Film - Nominated
**Saturn Award Best Makeup - John Vulich, Everett Burrell - Nominated
**Saturn Award Best Supporting Actress - Julie Harris - Nominated

*Fantafestival
**Best Actor - Timothy Hutton - Won
**Best Film - George A. Romero - Won
**Best Screenplay - Paul Hunt, Nick McCarthy - Won  

==References==
 

== External links ==
*  
*  
*  
*  official site at MGM.

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 