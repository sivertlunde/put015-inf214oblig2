Sathyabhama (film)
{{Infobox film
| name = Sathyabhaama
| image =
| caption =
| director = MS Mani
| producer = TE Vasudevan
| writer = Mythology Ponkunnam Varkey (dialogues)
| screenplay = Ponkunnam Varkey
| starring = Prem Nazir Adoor Bhasi Thikkurissi Sukumaran Nair CRK Nair
| music = V. Dakshinamoorthy
| cinematography = Melli Irani
| editing = MS Mani
| studio = Associated Producers
| distributor = Associated Producers
| released =   
| country = India Malayalam
}}
 1963 Cinema Indian Malayalam Malayalam film, directed by MS Mani and produced by TE Vasudevan. The film stars Prem Nazir, Adoor Bhasi, Thikkurissi Sukumaran Nair and CRK Nair in lead roles. The film had musical score by V. Dakshinamoorthy.    

==Cast==
*Prem Nazir 
*Adoor Bhasi 
*Thikkurissi Sukumaran Nair 
*CRK Nair Ambika  Baby Padmini 
*Kottarakkara Sreedharan Nair 
*L. Vijayalakshmi

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Abhayadev. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Araamathin Sundariyalle || S Janaki || Abhayadev || 
|- 
| 2 || Gokulathil Pandu || P. Leela || Abhayadev || 
|- 
| 3 || Idathu Kannilakunnathenthinaano || S Janaki || Abhayadev || 
|- 
| 4 || Jaya Jaya Naarayana || Kamukara || Abhayadev || 
|- 
| 5 || Kaadinte Karaluthudichu || P. Leela, Chorus || Abhayadev || 
|- 
| 6 || Maathe Jaganmaathe || P. Leela || Abhayadev || 
|- 
| 7 || Mannavanaayaalum || PB Sreenivas || Abhayadev || 
|- 
| 8 || Mathi Mathi Maayaaleelakal || P. Leela || Abhayadev || 
|- 
| 9 || Oru vazhi cholken || P Susheela || Abhayadev || 
|- 
| 10 || Prabhaathakaale Brahmaavaayee || K. J. Yesudas || Abhayadev || 
|- 
| 11 || Prakaasha Roopa Sooryadeva || K. J. Yesudas || Abhayadev || 
|- 
| 12 || Vaadaruthee Malarini || P. Leela, KP Udayabhanu || Abhayadev || 
|}

==References==
 

==External links==
*  

 
 
 


 