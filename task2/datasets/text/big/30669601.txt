The World Is Ours (film)
{{Infobox film
| name           = The World Is Ours
| image          = 
| caption        = 
| director       = Martin Frič
| producer       = Josef Stein
| writer         = Martin Frič George Voskovec Jan Werich
| starring       = Jiří Voskovec
| music          = 
| cinematography = Otto Heller
| editing        = Jan Kohout
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

The World Is Ours ( ) is a 1937 Czech comedy film directed by Martin Frič.   

==Cast== George Voskovec as Kamelot (as Jiří Voskovec)
* Jan Werich as Kamelot
* Bohus Záhorský as Dexler
* Vladimír Smeral as Josef Forman
* Ladislav H. Struna as Holister
* Jaroslav Prucha as Antonín Hart
* Adina Mandlová as Markétka Miroslav Svoboda as Berger
* Frantisek Cerný as Bidon
* Frantisek Filipovský as Pinker
* Zdenek Stepánek as Guvernér
* Karel Dostal as Tajemník Noelovy firmy
* Emanuel Kovarík as Zamestnanec u Noelu
* Jirí Hron as Zamestnanec u Noelu
* Jarmila Svabíková as Workwoman at the Noels
* Mirko Eliás as Delnik u Noelu

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 