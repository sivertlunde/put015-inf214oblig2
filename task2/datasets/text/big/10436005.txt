Mr. Medhavi
{{Infobox film
| name           = Mr. Medhavi
| image          =
| caption        =
| director       = G. Neelakanta Reddy
| producer       = RamaRao Bodduluri, Gopichand Lagadapati
| Studio         = Life Style Art Pictures
| writer         = G. Neelakanta Reddy Suman
| Chakri
| country        = India
| cinematography = Suneel L Reddy
| released       =  
| runtime        =
| language       = Telugu
| budget         =
}}
 Telugu film, directed by G. Neelakanta Reddy. The film stars Raja Abel, Genelia DSouza and Sonu Sood.

==Plot==
A young Vishwak (Raja) meets Swetha (Genelia) who comes from Canada on a vacation to her grandmom’s place. Incidentally, she goes to school for the time being at the local school, where she meets Vishwak. That’s when love blossoms. At least for Vishwak and he grows up loving Swetha, heart and soul.

However, given his humble background, life teaches him to be calculated in whatever he does. He soon masters the art of making the best for himself out of any given situation. Destiny brings him to the company that is owned by Swetha’s father. Here, the old friends meet once again. But this time around, Swetha is the boss’s daughter and dreams of setting up a pharmaceutical company. That’s when Swetha and Vishwak get to work and spend time together.

While Vishwak works his way towards making Swetha love him, she springs a surprise on him by announcing that she is head over heels in love with Sidarth (Sonu Sood), a millionaire-turned-HR guru. Seeing his game plan boomerang, Vishwak tries his best to take Swetha’s mind off Sidarth. But the more he tries, the more she is convinced she has found her ideal man.

Will Vishwak gain or loose his love?

==Cast==
*Genelia DSouza as Swetha Raja as Vishwak
*Sonu Sood as Rohit

== External links ==
*  

 

 
 
 


 