Hum Aapke Dil Mein Rehte Hain
{{Infobox film
| name           = Hum Aapke Dil Mein Rehte Hain
| image          = Hum Aapke Dil Mein Rehte Hain.JPG
| image_size     =
| caption        =
| director       = Satish Kaushik
| producer       = D. Rama Naidu Jainendra Jain Satish Kaushik
| story          = Bhoopathi Raja
| narrator       =
| starring       = Anil Kapoor Kajol Anupam Kher Shakti Kapoor Parmeet Sethi Satish Kaushik Johnny Lever Gracy Singh
| music          = Anu Malik M. M. Sree Lekha
| cinematography = Kabir Lal
| editing        = E.M. Madhavan Chaitanya Tanna Marthand K. Venkatesh
| studio         = Suresh Productions
| distributor    =
| released       = 22 January 1999
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Venkatesh and Soundarya.

==Plot==
Vishwanath is a multi-millionaire industrialist, living in a palatial house with his only child, a son named Vijay. The pampered Vijay has completed his education and now indulges in lifes temptations to the extreme. Vishwanath would like his son to get married and become responsible. Megha is Vishwanaths personal assistant in his office. She is a hardworking girl struggling to support her family. Vishwanath asks Megha to quit her job and marry his son, but she refuses when she hears Vijays strange condition. Vijay wants the marriage to be on a contract basis for a year and, if he does not fall in love with his wife in that duration, the marriage will be annulled. However, Meghas family is in dire financial straits and, so, she has to reconsider this offer. In return for marrying Vijay, she asks for financial support for her family, which Vishwanath readily provides.

Vijay and Megha are married. After the marriage, they become friendly with each other, and Megha goes out of her way to look after Vijay when he meets with an accident. At the end of the year, however, Vijay decides to annul the marriage, as had been agreed upon. Megha leaves Vijay and returns home. After the separation, Vijay seems to be enjoying himself; but slowly and eventually, he starts to feel a longing for the presence of his devoted wife. Complications arise when Megha finds out that she is pregnant with Vijays child. People in her neighbourhood start to question her stay at her mothers house and the identity of the childs father.

In order to support herself, Megha gets a job in a new company; to her surprise, when the companys managing director arrives, he turns out to be Vijay. He later confesses to her that hes a changed person and wants her back. But, even after repeated persuasion, she disagrees because her faith in him has been shattered. Vijay continues to pursue her and leaves no stone unturned to show her that he cares for her. Later, Megha and her family hold a ceremony for the well-being of her to-be-born child. Vishwanath and Vijay attend the ceremony as well and give her presents. Megha reveals Vijay to be her husband and tells all the guests about the marriage-contract. An argument follows; Vijay and his father walk out, followed by all the guests.

Near the completion of her pregnancy, Megha learns that trouble-makers Khairati Lal and Yeshwant Kumar, who had once attempted to kill Vijay, have escaped from prison. They are out looking for Vijay, who had fired them from his fathers company for cheating and fraud. Megha gets anxious and tries to reach Vijay as soon as possible. On the way, she learns that the whole thing was a set-up by Vijays friends to lure her back to her husband. Enraged, Megha goes to confront Vijay. As soon as she meets him, she accuses him of this shameless act. Vijay then staggers towards her, with his stomach pierced by a piece of glass and blood pouring from the wound. Khairati and Yeshwant have attacked him in reality. Megha runs toward him, slips and goes into labour. Vijay, summoning up all his strength, takes Megha to the hospital. There, he is treated for his injuries and she delivers a healthy baby boy. Megha and Vijay recover and get reconciled.

==Cast==
*Anil Kapoor as  Vijay
*Kajol as  Megha
*Anupam Kher as  Mr. Vishwanath, Vijays father
*Shakti Kapoor as  Khairati Lal
*Parmeet Sethi as Yeshwant Kumar
*Mink Singh as Anita
*Satish Kaushik as German—Vijays sidekick
*Rakesh Bedi as Salim the driver
*Johnny Lever as Sunny Goyal
*Smita Jaykar as  Meghas mother
*Sudha Chandran as  Manju
*Gracy Singh as Maya
*Sadhu Meher as  Badri Prasad
*Raju Shrestha as  Sudhakar
*Adi Irani as  Manjus husband
*Kumar Sanu as himself (Guest Appearance in the song Kasam Se)
*Anuradha Paudwal as herself (Guest Appearance in the song Kasam Se)
*Anu Malik as himself (Guest Appearance in the song Kasam Se)

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Hum Aapke Dil Mein Rehte Hain"
| Kumar Sanu, Anuradha Paudwal
|-
| 2
| "Dhingtara Dhingtara"
| Sonu Nigam, Hema Sardesai, Rahul Seth
|-
| 3
| "Chhup Gaya"
| Udit Narayan, Alka Yagnik
|-
| 4
| "Patni Pati Ke Liye"
| Shankar Mahadevan
|-
| 5
| "Zara Aankhon Mein"
| Kumar Sanu, Anuradha Paudwal
|-
| 6
| "Kasam Se Kasam Se"
| Kumar Sanu, Anuradha Paudwal
|-
| 7
| "Papa Main Papa Bangaya"
| Abhijeet Bhattacharya|Abhijeet, Anuradha Sriram, Shankar Mahadevan
|-
| 8
| "Jaata Hai Tu Kahan"
| Anu Malik, Hema Sardesai
|}

== References ==
 

==External links==
*  

 
 
 
 