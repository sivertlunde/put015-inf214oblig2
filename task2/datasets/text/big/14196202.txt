Moonzund (film)
 
{{Infobox film
| name           = Moonzund
| image          =
| image_size     =
| caption        =
| director       = Aleksandr Muratov
| producer       =
| writer         = Valentin Pikul (novel), Galina Muratova and Eduard Volodarsky (screenplay)
| narrator       =
| starring       = Oleg Menshikov Nikolai Karachentsov
| music          =
| cinematography =
| editing        =
| studio         = Lenfilm
| distributor    =
| released       = 1987
| runtime        = 142 minutes
| country        = Soviet Union Russian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1987 Soviet Soviet war novel with the same name by Valentin Pikul. The films name is derived from the old name of West Estonian archipelago where the Battle of Moon Sound took place during World War I.

== Cast ==
* Oleg Menshikov as Sergey Artenev
* Vladimir Gostyukhin as Semenchuk
* Lyudmila Nilskaya as Anna "Klara Georgievna" Revelskaya
* Nikolai Karachentsov as Von Knupfer
* Yuri Belyayev as Aleksandr Kolchak|Kolchak
* Boris Klyuyev as Von Grapf
* Vija Artmane
* Aleksei Buldakov
* Vladimir Baranov
* Yevgeniya Dobrovolskaya
* Yevgeniy Yevstigneyev
* Sergei Garmash Vladimir Yeryomin
* Vladimir Golovin
* Petr Shelokhonov as Andreev

==External links==
* 

 
 
 
 
 
 
 
 


 
 
 