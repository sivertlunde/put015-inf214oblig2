The Sharkfighters
{{Infobox film
| name           = The Sharkfighters
| image          = 
| producer       = Samuel Goldwyn Jr.
| director       = Jerry Hopper
| screenplay     = Jonathan Robinson Lawrence Roman 
| story          = Art Napoleon Jo Napoleon James Olson Philip Coolidge
| music          = Jerome Moross
| cinematography = Lee Garmes
| editing        = Daniel Mandell
| distributor    = United Artists
| released       =  
| country        = United States
| runtime        = 73 min. English
| gross          = 
}}
 American adventure James Olson, and Claude Akins. The fictional storyline is based on the invention of "Shark Chaser," an historical shark repellent developed by researchers during World War II.  , pp. 4-9      

==Plot== Isle of Caymanero fishermen Scripps Institution, Ensign "Dunk" Chief Petty copper acetate as a repellent. Evans advises that the project has already tested over 200 methods, including poisons, repulsive odors, color clouds, and ultrasound|ultrasonics, none of which has a lasting effect in driving away sharks. The test is initially successful until the acetate cloud dissipates after a few minutes.
 classified Color color film processed. Bens wife Martha (Steele) is staying in Havana, and during a night out dancing on their next visit, Evans tells Martha that while he admires Bens determination, he is worried that Ben never lets down his guard. She replies that he and Ben are a lot alike in their integrity, and that Ben and the scientist make a perfect team to achieve success. 
 octopod ink water soluble wax to keep dissipation from occurring. He volunteers as the human bait but Ben is noncommittal. 
 embassy in naval attaché expert riflemen to the project. The suspicious attaché maneuvers Ben into revealing his plan to run a test on himself and advises him to seek Navy approval first, but agrees to send the riflemen after Ben insists that hes only setting up preliminary plans. After more positive results than theyve ever achieved before, Ben abruptly orders the final test for the next day. Evans argues but begrudgingly acquiesces, admitting to Duncan that Ben has always been right. The repellent works effectively. As numerous sharks begin circling Ben, a marksman nervously shoots one, causing a feeding frenzy. Ben makes for the boat as Evans frantically showers him with extra repellent, which deters the frenzied sharks from attacking. Back aboard, Ben celebrates their success with a beer.

==Cast==
*Victor Mature ... Lt. Commander Ben Staves
*Karen Steele ... Martha Staves James Olson ... Ensign Harold Duncan
*Philip Coolidge ... Lt. Commander Leonard Evans
*Claude Akins ... Chief "Gordy" Gordon
*Rafael Campos ... Carlos
*George Neise ... Commander George Zimmer

==Production==
Samuel Goldwyn Jr. produced the film for Formosa Productions. Lawrence Roman and John Robinson wrote the screenplay from an original story by Art and Jo Napoleon based on actual events involving the creation of the Navys shark repellent, "Shark Chaser," a cake combining copper acetate to mimic putrefied shark tissue, black dye as a camouflage agent, and a water soluble wax binder as described in the script, which some sources attribute to efforts of Julia Child while working for the Office of Strategic Services during World War II.      
 Marine Studios oceanarium. 

The Sharkfighters was filmed in   

==Film score== scoring western (genre)|Westerns, and described as "lively and unique." While it is not known if he traveled to Cuba with the company, the distinctly ethnic themes of the music appear to be inspired by the filming on location, using syncopation and percussion instruments highly suggestive of his orchestral composition Biguine. 

Thematically the score is characterized by an ostinato that stresses the second half of the second beat but nothing at all on the third beat.  This rhythm is employed throughout the varied scene melodies using maracas, xylophones, guitars, claves, and bongos to produce a Caribbean motif. In the Havana night club scene he integrates a rumba into the score, then soothes it to a soft melody underscoring the dialogue between Martha and Len. Also notable is a "unique cue" to announce the presence of sharks.  The score of The Sharkfighters displays the fully developed elements of style now associated with Moross in Westerns such as The Big Country. 

==Adaptations== penciled and inked by John Buscema. 

==References==
 

*{{cite book| first = Mariana | last = Whitmer | chapter =| title = Jerome Morosss The Big Country: A film Score Guide| editor = 
| year = 2012 | publisher = Scarecrow Press | place = Lanham, Maryland | isbn = 0-81088-501-8| olcl = | url = | pages =}}

==External links==
*  at IMDb
*  
*  at Rotten Tomatoes

 

 
 
 
 
 
 
 