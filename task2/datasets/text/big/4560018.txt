The Oblong Box (film)
 
 
{{Infobox film
| name           = The Oblong Box
| image          = Theoblongbox.jpg
| caption        =
| director       = Gordon Hessler
| producer       = Gordon Hessler
| writer         = {{plainlist|
* Lawrence Huntington
* Christopher Wicking
}}
| starring       = {{plainlist|
* Vincent Price
* Christopher Lee
* Rupert Davies
* Uta Levka
* Sally Geeson
* Alister Williamson
* Peter Arne
* Hilary Dwyer
* Maxwell Shaw
* Carl Rigg Harry Baird
}} Harry Robinson
| cinematography = John Coquillon
| editing        = Max Benedick
| distributor    = American International Pictures
| released       =  
| runtime        = 91 min
| country        = United Kingdom
| language       = English
| budget         = ₤70,000   accessed 11 March 2014  or $175,000 
| gross          = $1.02 million (US/ Canada rentals) 
}}
The Oblong Box is a 1969 British horror film directed by Gordon Hessler, starring Vincent Price, Christopher Lee and Alister Williamson. This was the first film to star both Price and Lee.

==Plot==
 Harry Baird) to concoct a drug to put Sir Edward into a deathlike trance. Before Trench has time to act, Julian finds his "dead" brother and puts him in a coffin (the titles "oblong box"). Embarrassed by his brothers appearance, Julian asks Trench to find a proxy body for Sir Edwards lying in state. Trench and NGalo murder landlord Tom Hacket (Maxwell Shaw) and offer his corpse to Julian. After the wake, Trench and his young companion Norton (Carl Rigg), dispose of Hackets body in a nearby river, while Julian has Sir Edward buried. Now free of his brother, Julian marries his young fiance, Elizabeth (Hilary Dwyer), while Trench, Norton and NGalo go their separate ways.

Sir Edward is left buried alive until he is dug up by graverobbers and delivered to Dr. Neuhartt (Christopher Lee). Neuhartt opens the coffin and is confronted by the resurrected Sir Edward. With his first-hand knowledge of Neuhartts illegal activities, Sir Edward blackmails the doctor into sheltering him. Sir Edward then conceals his face behind a crimson hood and embarks on a vengeful killing spree.

Norton is first on Sir Edwards list and has his throat slit. In between killings, Sir Edward finds time to romance Neuhartts maid Sally (Sally Geeson), but when Neuhartt finds out about their affair, he discharges Sally and she goes to work for Julian. While searching for Trench, he is sidetracked by a couple of drunks who drag him into a nearby tavern. Here he ends up with prostitute Heidi (Uta Levka), who tries to roll him for his money, only to be killed by Sir Edwards knife. The police get involved, and the hunt is on for a killer in a crimson hood.

Meanwhile, Julian has become suspicious about the body which Trench supplied to him, after his friend Kemp (Rupert Davies) finds it washed up on a riverbank. Julian confronts Trench, who tells him the truth about Sir Edwards "death". Soon after, Trench is dispatched by Sir Edward, but not before he tells him the whereabouts of NGalo. Hoping he will cure him of his disfigurement, Sir Edward asks NGalo for his help. Here Sir Edward learns the truth about his time in Africa: in a case of mistaken identity he was punished for his brothers crime of killing an African child. NGalo fails to cure Sir Edward, and they fight; NGalo stabs Sir Edward and is rewarded with a face full of hot tar. Sir Edward returns to Neuhartts home, where Neuhartt tends to his wound. Mistrusting Neuhartts medical treatment, Sir Edward fatally wounds him and sets off to confront his brother.

Back at the Markham ancestral home, Julian learns the whereabouts of his brother from Sally and leaves for Dr. Neuhartts home, only to find him near death. Meanwhile, Sir Edward arrives back home to find Sally, who is repulsed by her former lovers killing. Sir Edward drags her out onto the grounds of the house, pleading for her love. Julian arrives back home and gives chase with a shotgun. Out on the grounds, Sally snatches Sir Edwards hood from him and his deformed face is revealed for the first time. Julian catches up and Sir Edward confronts him about his crime. As Sir Edward lurches forward, Julian shoots him. Cradling the dying Sir Edward in his arms, Julian is bitten by him.

Once again in his "oblong box", Sir Edward is resurrected by a vengeful NGalo, but this time he is six feet under with no hope of escape. While back at the Markham mansion, Julian is occupying his brothers old room, where he is beginning to show the first signs of the voodoo curse. His wife Elizabeth learns this when she tells him about his brothers room: Julian then says, "It is now my room". She is flushed with fear of his disfigurment and the film (with credits) closes with a close up of her widened eyes.

==Production==
 
Price, Davies and Dwyer had recently appeared in   as shooting began.

With the help of Christopher Wicking, he reworked the screenplay to incorporate the theme of imperial exploitation of native peoples in Africa. This theme gave the film a "pro-black" appearance that would later cause it to be banned in Texas.

The leading role of the film was given to character actor Alister Williamson, his first. Although he has the largest amount of screen time, more than either Price or Lee, his real voice is never heard (it was redubbed by another actor) and his face is covered for the majority of the film. His makeup was done by Jimmy Evans, whose other credits include Hesslers Scream and Scream Again (1970) and Captain Kronos – Vampire Hunter (1972).

Hessler says AIP insisted he use Hilary Dwyer.:
 I dont know what the situation was, but they liked her and they kept pushing you to use certain actors. I guess the management must have thought she was star material or something like that.  

==Reception==

===Box Office===
According to Hessler, the film was very successful and ushered in a series of follow up horror movies for AIP including Scream and Scream Again and Cry of the Banshee. 

===Critical===
Rotten Tomatoes, a review aggregator, reports that 57% of seven surveyed critics gave the film a positive review; the average rating was 4.6/10.   A. H. Weiler of The New York Times wrote, "The British and American producers, who have been mining Edgar Allan Poes seemingly inexhaustible literary lode like mad, now have unearthed The Oblong Box to illustrate once again that horror can be made to be quaint, laughable and unconvincing at modest prices."   Variety (magazine)|Variety wrote, "Price as usual overacts, but it is an art here to fit the mood and piece and as usual Price is good in his part." 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 