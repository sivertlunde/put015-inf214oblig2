Pooh's Heffalump Halloween Movie
 
 
{{Infobox film
| name = Poohs Heffalump Halloween Movie
| image = Poohs_Heffalump_Halloween_Movie.jpg
| caption = DVD cover
| writer = Evan Spiliotopoulos Brian Hohlfeld
| director = Elliot M. Bour Saul Andrew Blinkoff
| producer = John A. Smith Buena Vista Home Entertainment
| released =  
| music = Mark Watters
| runtime = 65 minutes
| language = English
}} Winnie the Pooh franchise. This is the first Pooh movie since the death of Paul Winchell and the last in which John Fiedler provides Piglet (Winnie-the-Pooh)|Piglets voice, as Fielder died in 2005.

== Plot ==
It is List of Winnie-the-Pooh characters#Lumpy|Lumpys first Halloween with Winnie the Pooh, Roo, and their friends in the Hundred Acre Wood. The group discusses their plans for Halloween and for their first night of trick-or-treating. Tigger, alarmed by a mysterious figure he spotted earlier in the woods, warns his friends about the dreaded Gobloon, a monster that comes out every Halloween to catch residents of the Hundred Acre Wood and turn them into Jack-o-lanterns. But Tigger tells them that if the Gobloon is captured first, it will grant its captors one wish. After Pooh eats all of Rabbits trick-or-treat candy, Roo and Lumpy set out to capture the Gobloon to wish for more candy.

When Roo and Lumpy reach the supposed Gobloons lair, Lumpy loses his courage to catch the Gobloon. So Roo tells him the story from Boo to You Too! Winnie the Pooh, when Piglet was afraid to go trick-or-treating, but gained his courage. Roo believes that if Piglet can conquer his fears so can Lumpy. An inspired Lumpy helps Roo set a trap for the Gobloon, but the two end up fleeing once they think the Gobloon is returning. Lumpy gets separated from Roo and ends up stuck in the trap theyd set for the Gobloon. Lumpy is heart-broken to be alone, as he and Roo had promised to stay together during the adventure. Roo finds a Jack-o-lantern resembling Lumpy, which makes him think his friend has been caught by the Gobloon.

Once back with the others, Roo recruits Pooh, Piglet, Tigger, Eeyore and Rabbit to help him capture the Gobloon and save Lumpy. The group arrives at the trap and believes the Gobloon is trapped. Roo wishes to have his friend Lumpy back. Hearing Roos voice, Lumpy is inspired to break his way out of the trap.

The group finally goes trick-or-treating, and Kanga, who was in reality the mysterious figure Tigger mistook for the Gobloon, throws a Halloween party for the friends, complete with Jack-o-lanterns shed carved in everyones likenesses. She explains that she must have accidentally dropped Lumpys Jack-o-lantern—a mistake that sparked the entire adventure. With Lumpys first Halloween being successful, everyone from the Hundred Acre Wood enjoys the Halloween party.

== Cast ==
* David Ogden Stiers as The Narrator
* Jimmy Bennett as Roo
* Kyle Stanger as Lumpy the Heffalump
* Peter Cullen as Eeyore
* Jim Cummings as Winnie-the-Pooh and Tigger
* John Fiedler as Piglet
* Ken Sansom as Rabbit
* Kath Soucie as Kanga

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 