Raavanan
 
 
 

{{Infobox film
| name           = Raavanan
| image          = Raavanposter.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Mani Ratnam
| producer       = Mani Ratnam   Sharada Trilok Suhasini  Kambar   (Story)   Vikram  Prithviraj
| music          = A. R. Rahman
| cinematography = Santosh Sivan   V. Manikandan
| editing        = Sreekar Prasad BIG Pictures Sony Pictures   Ayngaran International
| studio         = Madras Talkies
| released       =  
| runtime        = 134 minutes
| country        = India
| language       = Tamil
| budget         =   
| gross          =   
}}
 Indian Tamil Tamil thriller Prithviraj in Prabhu and Hindi as Telugu as Villain, with all three versions releasing simultaneously on 18 June 2010.

The film follows the crux of the epic Ramayana, with a ruthless police officer on the chase to find a tribal leader and lawbreaker, who has kidnapped his wife. Veeraiya, the kidnapper and his motive for the kidnap has been led on by the death of his sister, as a result of police custodial torture and brutal rape. The film explores the search by the officer, whilst exploring the changing emotions that the three protagonists experience with each other, leading to a riveting climax, situated in the jungle.

Raavanan was announced in February 2008, amidst much hype of the celebrated director, Mani Ratnams return to Tamil films, whilst Vikrams and Aishwarya Rais selection in the lead roles, creating more anticipation. Shooting began soon after, and took place in various locations with a record number of extras in areas including Chalakudy, Kerala and Ooty, Tamil Nadu amongst other regions throughout India.It declared as "Hit" at box office.

== Plot ==
The film opens with shots showing Veeraiya (Vikram (actor)|Vikram) jumping off a cliff into water. His gang is busy distracting the police. Police vehicles are set ablaze and women seduce policemen into a trap whereby Veeras henchmen attack. The police are ruthlessly murdered. Raagini (Aishwarya Rai Bachchan), whilst on a boating trip, is kidnapped by Veera. Dev Prakash (Prithviraj Sukumaran), her husband and a senior police officer, is informed of the incident.
 montage of sorts showcases the story of Veeraiya. He is seen as a local hero, who runs a parallel govt in areas near Tirunelveli, with his brothers – Singarasu (Prabhu Ganesan) and Sakkarai (Munna (actor)|Munna). Though considered a terrorist by the local police, Veeraiyya is respected by the villagers. He kidnaps Raagini hoping to avenge the death of his sister Vennila (Priyamani). He brings her to the edge of a cliff to shoot her to death. She refuses to die at his hands and jumps off the cliff into the water far below, hoping to kill herself, but fails to do so. This causes Veera to hold off the murder, being unable to kill someone who has no fear of death.

Dev and his team enter the forests with the aid of Gnanaprakasam (Karthik Muthuraman), a local forest guard. Despite searching deep in the forests, Dev is unable to hunt down Veera.

Meanwhile, Raagini develops feelings for Veera after she hears the story of his sisters death. Dev had led an encounter against Veera during Vennilas wedding. Devs shot grazes Veera in the neck. Veera, fighting for his life, is unable to protect his sister, and he is led out of the fiasco by his gang. The police pressurise Vennila into revealing Veeras hideout. When she refuses, she is subjected to physical and sexual assault. Veera returns home to find Vennila distraught and traumatised. The next day, she commits suicide by drowning in a nearby well.

Unhappy with the way his brother is leading a war causing distress to his gang, Sakkarai offers a truce to Dev. Dev initially seems to agree but, when Sakkarai comes out in the open, Dev shoots him to death – revealing that he considers the destruction of Veera as more important than saving his wife.

Veera and Singarasu are enraged and attack Devs camp; they wipe it out completely. A final confrontation between Veera and Dev takes place on a rickety bridge – where Veera triumphs over Dev – when he decides not to let Dev die. He tells Dev he is letting him live because of his wife. Dev manages to extricate himself out and finds Raagini bound and tied up – with Veera leaving her.

Dev is not entirely happy and, while returning to their hometown of Mettukudi, he accuses Raagini of infidelity and informs her that it was Veera who told him so. Furious, Raagini leaves Dev to meet Veera through Singarasu. She manages to meet him and asks him why he had accused her. Veera tells her he said that he had protected Raagini safely for the fourteen days and nothing else. He quickly realises that Dev lied, hoping Raagini would lead him to his hideout.

Dev appears with a police team and confront the duo. Raagini tries to save Veera, but he pushes her out of the line of fire. He is shot multiple times, whereby he falls off the cliff. The touching climax of the movie shows Raaginis true feelings coming to surface: She is seen trying with all her might to save Veera. Although Veera tries to grab her hand amidst the gun fire, he fails to do so, dying without touching Raagini throughout the time he knew her. (In the last song, "Naan Varuvaen," it says he, Veera, will be back) Veera, content that Raagini has feelings for him, falls to his death with a smile.

== Cast ==
  Vikram as Veeraiya
* Aishwarya Rai as Ragini 
* Prithviraj Sukumaran as Dev Prakash Karthik as Gnanaprakasam Prabhu as Singarasu
* Priyamani as Vennila
* John Vijay as Hemanth Shankar Munna as Sakkara 
* Vaiyapuri as Raasathi
* Ranjitha as Annam
* Varsha as Poonkodi
* Ashwanth Thilak as Velan Stanley as Padakotti
* Azhagam Perumal as photographer 
* Lakshmy Ramakrishnan as Velans mother 
* Saravana Subbiah as Ranjith

== Production ==

=== Development ===
During the making of his 2007 biopic Guru (2007 film)|Guru starring Abhishek Bachchan and Aishwarya Rai, Mani Ratnam had finalised a script for his next directorial venture titled Lajjo.  Based on a short story by Ismat Chughtai,  it was a musical period film set in the desert and was to star Aamir Khan and Kareena Kapoor in the lead.  Though the film was slated to go on floors after the release of the former,  there were reports of a fall-out between Ratnam and Khan due to creative differences. While cinematographer P. C. Sreeram denied the reports,  the films would-be lyricist Gulzar said there were actually problems with acquiring the copyright of the story,  and composer A. R. Rahman even confirmed to having completed 80% of the film score.  Yet, the project was put on the back burner for reasons unknown.  

Following the critical and commercial success of Guru, Ratnam announced his next film in February 2008.  A modern day retelling of the mythological epic Ramayana, the film again features the real life couple in the lead.  The film was initially planned to be made only in Hindi and the idea for the Tamil version came later.    In January 2009, while the film was in the making, it was further decided to dub the Tamil version to Telugu making it a tri-lingual.   While the film was yet to be titled,  it was widely reported in the media that the Tamil version was titled Ashokavanam in reference to the place where Sita was held captive by Ravana. Further reports emerged stating that since director Kasthuri Raja has already registered the title for his project, Ratnam has requested him for using the title.  Later, Vikram (actor)|Vikram, the lead actor, clarified in an interview that the film was tentatively titled Ravana but was wrongly reported as Ashokavanam.  Subsequently, the film was titled Raavan in Hindi, Raavanan in Tamil and Villain in Telugu. 

While the plot is inspired by Ramayana, the story is narrated from Ravanas perspective making him the protagonist.  The film is centered on the Ashokavanam episode where Ravana kidnaps Sita and keeps her in Asokavanam. Later Rama ventures to save his wife and bring her back.

{|class="wikitable" align="right" style="width: 20%; height: 200px"
|+ Character Map
|-
! Actors
! Roles
|-
| Vikram || Ravana
|-
| Rai || Sita
|-
| Prithviraj || Rama
|-
| Karthik || Hanuman
|-
| Prabhu || Kumbhakarna
|-
| Munna || Vibhishana
|-
| Priyamani || Surpanakha
|}

=== Casting === Prithviraj was Karthik made Prabhu was Munna was signed up to play a role synonymous with Vibhishana.  Bipasha Basu was to play the role of Mandodari,    which was later scrapped to keep the film short.      Comedian Vaiyapuri plays a transgender. 

While the cinematography was handled by V. Manikandan  and was later taken over by Santosh Sivan when the former left the project,  editing was done by Sreekar Prasad.  Rais costumes were exclusively designed by fashion designer Sabyasachi Mukherjee.  Choreography was by Ganesh Acharya, Brinda, Shobana,  and Astad Deboo. Peter Hein and Shyam Koushal choreographed the action sequences and Samir Chanda took care of production design.

=== Filming ===
The film was predominantly shot outdoors in various hitherto unseen locations in and around India.  Shooting took place at Tumkur (Karnataka), Orchha near Jhansi and the forests of Madhya Pradesh,  Mahabaleshwar in Maharashtra.  It was also reported that Ratnam had planned to shoot at Sri Lanka   but decided against it owing to insurgency by the rebel group LTTE.  But Ratnam dismissed the reports as rumours. 
 Hooghly at Agarpara.     Later, as the shooting resumed and progressed at Ooty, Ratnam fell ill in April 2009 and was hospitalised at Apollo Hospitals,    causing a further delay of 47 days until filming resumed in June 2009 following his recovery.  As the numerous delays affected his other projects, DOP Manikandan walked out in May and was replaced by Santosh Sivan.   By July, the crew moved back to Kerala,  to reshoot a few scenes at Chalakudy as Ratnam was reportedly unsatisfied after seeing the rushes.     This time around, heavy rains played spoilsport leading to another delay in filming.  Moreover, when an elephant brought for the shoot ran amok killing the mahout,  the Animal Welfare Board served a show cause notice to the production company (Madras Talkies) for not taking permission to use elephants.  

The film began its last schedule in August 2009 at the Malshej Ghats in Maharashtra where the climax sequence was shot,  the final encounter taking place on a wooden bridge.  Production designer Samir Chanda built three identical bridges to facilitate the scene to be captured from different angles.   Though initially planned to be built either in Sri Lanka, Australia or South Africa, the bridge was constructed in Mumbai to reduce costs.   While bad weather and heavy rains disrupted shoot for a few days,  the forest department filed cases against some crew members for trespassing.   The film went into post-production by the end of 2009. 

Numerous action sequences were performed by the actors.  The actors suffered from real cuts and bruises that they dint need make up.   For his introduction scene, the protagonist has to jump from a 90-foot high cliff near the Hogenakkal Falls into the river below.  This risky dive was performed by a body double, Balram, a Bangalore based former national diving champion.  Kalarippayattu, a martial art form origaniting from Kerala, was also featured in the film. Sunil Kumar, a Kalari gym trainer from Kozhikode, trained the actors.  Contemporary dancer Astad Deboo choreographed a stunt scene for the film.    

Vikram got his hair cropped short for his look and it was kept under wraps until the release.  Supporting actor Munna tonsured his head and went bald for a scene.  Rai was training in Tamil to voice her lines. While Ratnam was impressed with her Tamil and had planned to let her dub,     actress Rohini (actress)|Rohini, who had earlier dubbed for Rai in Iruvar and Guru, ended up lending her voice.  

== Release ==

=== Marketing ===
A 10 min teaser was released for a promo event.   The film look was unveiled at Cannes Film Festival.  Villain promotion in Andhra. 

=== Film festival screenings ===
The film screened at 67th Venice Film Festival.   The film was well received at Venice where the audience gave it a thunderous applause after it was screened.  The master filmmaker was honoured with the Jaeger-LeCoultre Glory to the Filmmaker Award, an award shared by the likes of legends like Takeshi Kitano, Abbas Kiarostami and Sylvester Stallone.    Later, the film has been screened at the 15th Busan International Film Festival.    Indian Panorama Film Festival.  The film was premiered at the 10th Annual Mahindra Indo-American Arts Council Film Festival in New York City.   Jim Luce praised mani ratnams work and mentioned Raavanan is a must see international film of 2010.  29th San Francisco Asian American Film Festival.  

=== Theatrical release ===
Prior to its release, the film was given a "U" (Universal) certificate by the Central Board of Film Certification.  Raavanan released in 375 screens worldwide. The overseas distribution rights of Raavanan was sold for a record price of $1.5 million to Ayngaran International. Raavanan was previewed at the Devi-Sri Devi Cinema Complex in Chennai, where it received a standing ovation by film personalities including Rajinikanth.     The film was also previewed at Inox and was attended by celebrities from tinsel town.  It was released worldwide on 18 June 2010  in 375 Screens (225 Screens in India and 150 Screens Overseas).  The Telugu version Villain released with 215 screens in Andhra and 25 screens overseas. In the US, it was distributed by Big Cinemas.    Raj TV bought the satellite rights for $1.1 million. 

== Controversy ==
It was agreed to screen the film in Bangalore in across 21 screens.  But both the versions were screened in more centres, the Karnataka Film Chamber of Commerce (KFCC) approached the court.  The film chamber banned the exhibition completely.  The Competition Commission of India (CCI), in an interim order, stayed the chamber ban and permitted Reliance Big Entertainment Limited (RBEL) to exhibit the film in 36 cinemas.  This order was to be in effect till 22 June  

The film also created a furore in Sri Lanka. Since Amitabh Bachchan, Abhishek and Aishwarya boycotted the IIFA awards that was held in Sri Lanka, the film release was protested and theatres were torched.      Films of those who attended IIFA in Sri Lanka were banned in Tamil Nadu. 

== Reception ==

=== Critical response === Aishwarya Rai Prithviraj is the ideal foil for Vikram, and is good. The movie lacks the Mani Ratnam touch in the story and screenplay department, and has a wobbly first half, where the story just does not move. The last 10 minutes are the best part of this 2 hours 7 minutes film".    Film critic Sudhish Kamath review.  On Rotten Tomatoes the film has a rating of 62% with 8 fresh and 5 rotten reviews. 

The Tamil version was regarded as the better version of the film,  with critics particularly applauding the lead performances and the technical work of the film.  

=== Box office ===
Unlike its Hindi counterpart, which tanked at the box office, the Tamil version tasted success in the South.   During its opening weekend on 15 screens in the Chennai, it was the number one film and netted  , an opening weekend record then.   Though the film opened to packed houses, it slumped a little due to mixed reviews but later picked up following a local holiday.  The film collected $8 million at the box office in the first month of release  including $400,000 from Kerala.   It went on to make over   600&nbsp;million at the worldwide box office and remained one of the top Tamil grossers of the year.    Uk opening weekend.  UK boxoffice second week.  New York boxoffice.  

=== Accolades ===
Raavanan was one among the films shortlisted for the Academy Awards.   V. Manikandan was nominated for Best Cinematography award in the Asia Pacific Screen Awards (APSA).     Karunanidhi praises.  Added to Austrian Film Museum.     Peter Hein world stunts award.    

; 58th Filmfare Awards South 

* Won – Filmfare Best Actor Award – Vikram 
* Won – Filmfare Best Male Playback Singer – Karthik
* Nominated – Filmfare Best Actress Award – Aishwarya Rai
* Nominated – Filmfare Best Supporting Actor Award – Prithviraj Sukumaran

; 5th Vijay Awards

* Won – Best Actor Award – Vikram
* Nominated – Best Actress Award – Aishwarya Rai
* Nominated – Best Cinematographer – Santhosh Sivan
* Nominated – Best Male Playback Singer – Karthik
* Nominated – Best Female Playback Singer – Anuradha Sriram
* Nominated – Favorite Director – Mani Ratnam
* Nominated – Favorite Actress – Aishwarya Rai

== Music ==
 

== References ==
 

== External links ==
*  
*  
*  
*   at Box Office Mojo

 
 

 
 
 
 
 
 
 
 