Professional Soldier (film)
{{Infobox film
| name           = Professional Soldier
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Tay Garnett
| producer       = Darryl F. Zanuck
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Victor McLaglen Freddie Bartholomew
| music          = 
| cinematography = Rudolph Maté
| editing        = 
| studio         = Twentieth Century Fox
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = U.S.
| language       = 
| budget         = 
| gross          =
}}
Professional Soldier is a 1935 adventure film based on a 1931 story by Damon Runyon, "Gentlemen, the King!"  It stars Victor McLaglen and Freddie Bartholomew. The film was directed by Tay Garnett, and produced by Twentieth Century Fox.

==Plot==
Michael Donovan (McLaglen), a soldier of fortune and former  ) and Ledgard (Walter Kingsford) want to hire him to kidnap King Peter II, ruler of a country somewhere in the Balkans. When a drunken Foster wakes up and interrupts their meeting, Donovan calls him his aide.

Donovan and Foster reconnoiter at a masquerade ball held at the kings palace, but Peter does not make an appearance. Foster quickly falls in love with a woman there named Sonia (Gloria Stuart).

When the two men sneak back into the palace later that night, they are surprised to discover that Peter (Freddie Bartholomew) is just a boy. Donovan is too disgusted to want to abduct him, though Peter is thrilled at the idea of an adventure. However, when Countess Sonia stumbles upon the scene and raises the alarm, Donovan has no choice. Peter helpfully shows him a secret escape passage; Sonia reluctantly goes with them to take care of the lad. 

As prearranged, the kidnappers take Peter to Lady Augusta (Constance Collier), who turns out to be Peters former nurse. Donovans employers succeed in overthrowing Gino (C. Henry Gordon) and installing their own reform government under the leadership of Stefan Bernaldo (Pedro de Cordoba).

As time goes on, Donovan becomes very fond of Peter and vice versa, while Foster convinces Sonia he really does love her. However, that does not sway her from what she sees as her duty; she manages to send a message to Gino revealing where the king is being held. Peter and Donovan initially evade Ginos men, but are recaptured within sight of the palace. 

Peter orders Gino to release Donovan unharmed, but Gino secretly has him imprisoned. Gino tells supporter Prince Edric (Lester Matthews) to start rumors that the new regime has killed the very popular king. Edric is aghast, grasping the implication that Gino intends to murder Peter. Sonia also realizes her mistake, and frees Donovan and Foster. With Fosters help, Donovan kills or captures all 250 of Ginos men just in time to save Peter from a firing squad. When Gino resists, Donovan shoots him. Later, a grateful Peter bestows a decoration on Donovan before they tearfully part.

==Cast==
*Victor McLaglen as Michael Donovan
*Freddie Bartholomew as King Peter
*Gloria Stuart as Countess Sonia
*Constance Collier as Lady Augusta
*Michael Whalen as George Foster
*C. Henry Gordon as Gino
*Pedro de Cordoba as Stefan Bernaldo
*Lumsden Hare as Paul Valdis
*Walter Kingsford as Christian Ledgard
*Lester Matthews as Prince Edric
*Rita Hayworth as Gypsy dancer (uncredited) 

==Notes==
  

==External links==
*  at the Internet Movie Database
*  at AllMovie TCM
* 
* 

 

 
 
 
 
 
 
 
 
 