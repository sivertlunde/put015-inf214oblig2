Rimsky-Korsakov (film)
{{Infobox film
| name = Rimsky-Korsakov 
| image =
| image_size =
| caption =
| director = Gennadi Kazansky   Grigori Roshal
| producer =
 | writer = Anna Abramova    Grigori Roshal
| narrator = Aleksandr Borisov   Liliya Gritsenko
| music = Georgi Sviridov
| cinematography = Mikhail Magid   Lev Sokolsky
| editing = 
| studio =  Lenfilm Studio 
| distributor =  Sovexport
| released =  1952 
| runtime = 101 minutes
| country = Soviet Union Russian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical musical Aleksandr Borisov. The film portrays the life of the Russian composer Nikolai Rimsky-Korsakov.   The film was shot in Sovcolor.

==Cast==
*  Grigori Belov as Nikolai Rimsky-Korsakov  
* Nikolai Cherkasov as Stasov   Aleksandr Borisov as Mamontov 
* Liliya Gritsenko as Zabela-Vrubel 
* Viktor Khokhryakov as Glazunov  
* Anatoliy Kuznetsov as Lyadov  
* Lidiya Sukharevskaya as Rimskaya-Korsakova  
* Aleksandr Ognivtsev as Shalyapin  
* Boris Kokovkin as Serov  
* Sergei Kurilov as Vrubel  
* Lidiya Dranovskaya as Almazova 
* Anatoli Verbitsky as Mikhailov 
* Tatyana Lennikova as Lebedeva  
* Agasi Babayan as Daryan 
* Bruno Frejndlikh as Ramensky  
* Vladimir Balashov as Dyagilev  
* Fyodor Nikitin as The Grand Duke 
* Yevgeni Lebedev as Kashchei Bessmertny in opera

==References==
 

==Bibliography==
* Mitchell, Charles P.  The Great Composers Portrayed on Film, 1913 Through 2002. McFarland & Company, 2004.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 