Captain Jack (film)
  British comedy Peter McDonald, Maureen Lipman and Michele Dotrice. It was made by Goldschmidts company Viva Films Ltd. The film was released on video by Koch Vision in 1999.

==Plot==
Captain Jack stars Bob Hoskins as a rebellious captain of a small Whitby boat who is determined to flout petty maritime bureaucracy. Officials declare his boat unsafe for a planned voyage to the Arctic, but Jack is determined to set sail and to place a plaque there in commemoration of his seafaring hero. With his motley crew, Captain Jack succeeds in making his voyage despite an international search for his boat by maritime authorities.

==Inspiration==
The film is based on a true life incident involving a Whitby man, Jack Lammiman, who declared that his ship was totally seaworthy but was being hampered from sailing by maritime rules. As in the film, he slipped out of the harbour unseen in 1992. His crew included a vicar, a lady pensioner and 62-year-old Royal Navy veteran named Hugh Taff Roberts. Lammiman successfully sailed his ship, Helga Maria, to the Arctic and fulfilled his wish to place a memorial plaque on Jan Mayen Island to honour Whitby whaling Captain William Scoresby.  During the voyage he succeeded in evading an international search by the naval authorities using a number of techniques which including painting his boat a different colour. Lammiman arrived back at his home port of Whitby to a hero’s welcome.

==External links==
*  
* 
* 

 

 
 
 

 