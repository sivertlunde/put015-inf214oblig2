Rajoo Dada
{{Infobox film
 | name = Rajoo Dada
 | image = RajooDada.JPG
 | caption = DVD Cover
 | director = D S Azad
 | producer = Balwant Singh Suri
 | writer = Suroor Subhash (Story, Screenplay)
 | dialogue = Suroor Subhash
 | starring = Mithun Chakraborty Vijayendra Ghatge Kajal Kiran Neeta Metha Kader Khan Sriram Lagoo Raza Murad
 | music = Usha Khanna
 | lyrics = 
 | associate director = 
 | art director = 
 | cinematography = V.Ratra
 | editing        = Sudhakar N. A.F.E
 | released =  February 28, 1992
 | runtime = 135 min.
 | language = Hindi Rs 1.8 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1992 Hindi Indian feature directed by D S Azad, starring Mithun Chakraborty, Vijayendra Ghatge, Kajal Kiran and Neeta Metha. Eventhough the film was censored in 1990, it had a theatrical release only in 1992. 

==Plot==

Rajoo Dada is the story of a 2 brothers and a sister and their revenge on their parents killer. One of the children grew up to became a Dada while the other, a police officer.

==Cast==
*Mithun Chakraborty
*Kajal Kiran
*Vijayandra
*Neeta Metha
*Kader Khan
*Sriram Lagoo
*Raza Murad
*Huma Khan
*Rajinder Nath
*Iqbal Khan
*Chand Usmani
*B.S.Suri
*Mohan Choti
*Yunus Parvez
*Late Maruti
*Sarita Devi
*Chandrasekhar
*Madhu Patel
*Keshav Rana
*Mushtaq Khan
*Zubeda
*Mona
*Prithvi Soni
*Ramesh Dixit
*Gurtar Gill
*Darshan Khanna
*Master Sandeep
*Master Ladoo
*Sumithra ( Dancer Girl )

==Reception==

The film was a late release after lying on cans for several years.

==References==
 
* http://www.imdb.com/title/tt00/
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Rajoo+Dada

==External links==

 
 
 


 