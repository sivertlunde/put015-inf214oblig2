Stolen Heaven
 
 
{{Infobox film
| name           = Stolen Heaven
| image          =
| caption        =
| director       = George Abbott
| producer       =
| writer         = George W. Hill (screenwriter) George Abbott (screenwrite) Dana Burnet (screen story)
| narrator       =
| starring       = Nancy Carroll Phillips Holmes Louis Calhern
| music          =
| cinematography = George Folsey
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Stolen Heaven is a 1931 American drama film, directed by George Abbott. It was released on February 21, 1931, by Paramount Pictures.

==Plot==
Mary, a girl of the streets, and Joe, a young thief, rob twenty thousand dollars and decide to spend all the money and then commit suicide. But Joes conscience speaks louder and he confesses the crime. He goes to prison knowing that Mary will await for him.

==Cast==
*Nancy Carroll as Mary
*Phillips Holmes as Joe
*Louis Calhern as Steve Perry Edward Keane as Detective Morgan
*Joan Carr as Mrs. Woodbridge-Wood
*Guy Kibbee as Police Commissioner

==See also==
* 1931 in film
* List of American films of 1931
* List of drama films
* List of Paramount Pictures films

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 