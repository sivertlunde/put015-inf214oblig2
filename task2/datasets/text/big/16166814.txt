Ezhumalai
 
{{Infobox film|
| name = Ezhumalai
| image =
| caption =
| director = Arjun Sarja
| producer = Arjun Sarja
| writer   = K. C. Thangam  (dialogues) 
| screenplay    = Arjun Sarja
| story   = Chinni Krishna Vijayakumar Ashish Vidhyarthi
| music = Mani Sharma
| cinematography = K. S. Selvaraj
| editing        = P. Sai Suresh
| released = 21 June 2002
| runtime =
| language = Tamil
| country = India
}}
Ezhumalai is an Indian Tamil film released in 2002. The Film is the remake of Telugu Blockbuster Narasimha Naidu starring Nandamuri Balakrishna|Balakrishna, Mukesh Rishi.  This movie was later dubbed in Telugu again as "Simhabaludu". The movie became a blockbuster hit.

==Plot==

There are two neighboring villages in Rayalaseema. Goons head one village and Raghupati Naidu (Vijayakumar) is heading another village. As Raghupati Naidu is peace loving, his village is dogged by the misdeeds of goons from the neighboring village. In order to save the future of his village, Raghupati Naidu asks one male kid from each family to be given for the purpose of saving the village. Raghupati Naidu selected his fourth son Ezhumalai(Arjun Sarja) as his familys contribution towards the security of the village. All these kids will be trained and prepared to protect the village. 15 years later.

Anjali (Gajala) is the niece of Jaya Prakash, who happens to be one of the goons from the goons village who later settled in another village. Ezhumalai sets up a naatya ashram to teach classical dance to the students. Anjali falls in love with Ezhumalai at the first sight. Anjali joins Ezhumalai as the student and tries to make Ezhumalai fall in love with her.

Later on, she comes to know that Ezhumalai is already married and he has a kid too who is growing in the ashram. Ezhumalai is a widower as his wife Lakshmi (Simran) is no more. Anjali tries to get closer to the kid so that she can then marry Ezhumalai.

The parents of Anjali fix up the marriage of Anjali with one of their relatives. When the uncle of Anjali comes to know that she is in love with a widowed dance master; he attacks him and threatens him that he would kill the kid. When they find Anjali missing, the entire battalion of goons invades the ashram of dance master only to find that he flew away.

Ezhumalai, after coming to know that goons were coming to attack him, vacates the ashram and boards the train. At the same time Anjali too boards the train, though Ezhumalai rejects it. Then a fleet 16 Whites Sumos containing goons chases the train using a parallel road. And they over take train and stop it forcibly at a station. Then we have around 100-armed goons surrounding the train and vying for the blood of Ezhumalai. After a couple of minutes Ezhumalai gets down the train showing his back to them. And then turns around. The moment, the goons realize that the dance master is none but Ezhumalai; they leave all their arms and run away to save their dear lives.This Movie is Blockbuster hit
Then Ezhumalai explains Anjali his flash back.

Raghupati Naidu realizes that no parents in the village is willing to marry off their daughter to Ezhumalai since he is meant for sacrificing for the safety of village. At that point of time Raghupati Naidu selects a beautiful girl called Lakshmi for his son. But Lakshmi is too sensitive and hates anything to do with violence. Hence Raghupati Naidu asks Ezhumalai to act as a soft guy who has liking for classical dance. After marriage Lakshmi realizes the fact and decides to live with it.

Ezhumalais three elder brothers arrive along with their families. Ezhumalai has lot of love and adulation for his brothers whereas they treat him very bad which Ezhumalai takes in good sense. After a few days Lakshmi lashes out at those guys who are making mockery of Ezhumalai. Then they complain about Lakshmi to Ezhumalai. Being upset about this, Ezhumalai sends Lakshmi to her parents place. At her parents place, Lakshmi gives birth to a son.

The brothers of Ezhumalai are still upset with the insult that is made to them by Lakshmi and decides to leave back to the states. They arrange for the police security and refuse for Ezhumalai to accompany them. During this period, the goons plan to kill the brothers of Ezhumalai and the son and wife of Ezhumalai .Ezhumalai  manages to save his brothers who realize their mistake albeit late but lakshmi is killed when she tries to save her baby.

After a fight scene Ashish vidhyarthi asks Ezhumalai to marry Anjali as his redemption for his killing Lakshmi.
Finally Ezhumalai marries Anjali.

==References==
 

 
 
 
 
 