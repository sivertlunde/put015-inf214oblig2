Between Two Women (1937 film)
{{Infobox film
| name           = Between Two Women
| image          = Between Two Women (1937 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = George B. Seitz
| producer       =
| screenplay     = Frederick Stephani Marion Parsonnet 
| story          = Erich von Stroheim
| starring       = Franchot Tone Maureen OSullivan Virginia Bruce Leonard Penn Cliff Edwards
| music          = William Axt
| cinematography = John F. Seitz
| editing        = W. Donn Hayes 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Between Two Women is a 1937 American drama film directed by George B. Seitz and written by Frederick Stephani and Marion Parsonnet. The film stars Franchot Tone, Maureen OSullivan, Virginia Bruce, Leonard Penn and Cliff Edwards. The film was released on July 9, 1937, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Franchot Tone as Allan Meighan
*Maureen OSullivan as Claire Donahue
*Virginia Bruce as Patricia Sloan
*Leonard Penn as Tony Woolcott
*Cliff Edwards as Snoopy
*Janet Beecher as Miss Pringle
*Charley Grapewin as Dr. Webster
*Helen Troy as Sally
*Grace Ford as Nurse Howley
*June Clayworth as Eleanor
*Edward Norris as Dr. Barili
*Anthony Nace as Tom Donahue
*Hugh Marlowe as Priest 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 