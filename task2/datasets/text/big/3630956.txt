Beer: The Movie
 
Beer: The Movie is a 2006 film.

== Sketchlist ==

# Main Titles  
# Burger P.I.  
# The Flashback  
# Captain Bobby  
# Das Wundertime  
# Only in Dreams  
# 40 Hands  
# Tom Sizemore  
# Good Livin  
# Jesus Walks  
# El Luchador  
# Happy Hour  
# Sudatrex  
# Butt Smoking  
# The Headhunter  
# Its My Birthday  
# Special Interview  
# Das Wundertime 2  
# The Good News Is...  
# Robb Ackermann  
# Ass Pirate  
# Say Anything  
# End Credits  

== Soundtrack ==
The soundtrack to Beer: The Movie is a compilation album released by Triple Crown Records. It was produced by Peter Hoare and distributed by BMG. It is an Enhanced CD, which contains both regular audio tracks and multimedia computer files.

=== Track listing ===
# Stand Up, State Your Name - Orange Island
# To My Better Angel - Northstar (previously unreleased)
# Liquorland - Mighty Casey Brand New (previously unreleased)
# Passing Of America, The - Moneen
# 05.04.68 - Outsmarting Simon
# Probably - Kevin Devine
# Your Own Disaster - Taking Back Sunday (previously unreleased)
# God Loves Ugly - Atmosphere Safety In Numbers
# Lightweight - J-Zone/Louis Logic (previously unreleased)
# Irish Car Bomb - Hot Rod Circuit
# Face Or Kneecaps - The Movielife
# Heres To The Things Gone Wrong - Crime In Stereo
# None Of My Friends Are Punks - Allister
# Destroyer - Fight Paris

In May 2006 the boys released a follow up DVD, Beer: The Movie 2, which was produced by Halo 8 Media.  The DVD was distributed nationwide by Sony BMGs RED Distribution.

Currently Chris Lilli, Biff Corrado, Tommy Gutter, and Peter Hoare of Beer: The Movie are working on a weekly web based series entitled Pederson 2008.  The website that hosts this weekly sketch series is www.pederson2008.com.  In this series, Chris Lilli stars as Dan Pederson, an up-and-coming politician.

==References==
 
 

==External links==
* 
*  Synopsis and reviews of the movie at beerthemovie.net

 
 
 
 
 
 


 