Take This Waltz (film)
 
 
{{Infobox film
| name           = Take This Waltz
| image          = Take This Waltz (film) poster art.jpg
| image_size     = 220px
| border         = yes
| alt            =  
| caption        = Promotional poster
| director       = Sarah Polley
| producer       = Susan Cavan Sarah Polley
| writer         = Sarah Polley Michelle Williams Seth Rogen Sarah Silverman Luke Kirby Jonathan Goldsmith
| cinematography = Luc Montpellier
| editing        = Christopher Donaldson The Harold Greenberg Fund Joes Daughter
| distributor    = Mongrel Media   Alta Classics   Broadmedia Studios   Magnolia Pictures  
| released       =  
| runtime        = 116 minutes    
| country        = Canada Spain Japan
| language       = English
| budget         = 
| gross          = $1,239,692   
}} Little Portugal rickshaw driver who lives across the street.
 Michelle Williams, Seth Rogen, Sarah Silverman, and Luke Kirby.

==Plot==
Margot, a freelance writer, meets Daniel, an artist and rickshaw operator, while on a business trip, and although they immediately share some chemistry, she reveals to him that she is married. However, it turns out that Daniel is living across the street from Margot and her husband Lou in Toronto. Although Lou and Margot appear happy together, it becomes clear that Margot is not completely satisfied with her marriage, possibly aggravated by encountering Daniel. As the film progresses Margot and Daniel interact more and more until she ultimately leaves her husband to be with him. Lou is saddened, yet understanding. The audience is then shown a montage of Margots new life with Daniel, including several brief sex scenes, though it is clear that she begins to regret leaving her husband. Geraldine, Lous sister and a recovering alcoholic, confronts Margot (while drunk) and tells her that she should have just accepted that life has gaps and that changing relationships was not the answer.

==Cast== Michelle Williams as Margot
* Seth Rogen as Lou Rubin
* Sarah Silverman as Geraldine
* Luke Kirby as Daniel
* Aaron Abrams as Aaron Rubin
* Jennifer Podemski as Karen

==Release== world premiere on September 11, 2011 at the Toronto International Film Festival.  The film then played at the 31st annual Atlantic Film Festival and the 59th annual San Sebastián International Film Festival.   At the end of September, Take This Waltz was shown at the 25th Edmonton International Film Festival and the Vancouver International Film Festivals.   The film closed the Calgary International Film Festival and the Cinéfest Sudbury International Film Festival.   In April 2012, Take This Waltz was shown at the Tribeca Film Festival. A month later it played at the Seattle International Film Festival.  
 VOD program on May 25, 2012, before releasing it to theatres on June 29, 2012.      The film also opened to Canadian theatres on June 29.  Take This Waltz was released on June 14, 2012 in Australia.  It was released by StudioCanal on August 17, 2012 in the UK.  
===Home media===
Take This Waltz was released on Blu-ray and DVD on October 23, 2012.

==Reception==

===Box office===
Take This Waltz earned Canadian dollar|$203,127 upon its opening weekend in Canada.    The film opened to 27 theatres and landed at number one in the box office top five.  In the US, Take This Waltz earned $137,019 during its opening weekend across 30 theatres.  As of September 28, 2012, the film has grossed $1,239,692 in the US.   

===Critical response=== review aggregation rating average Time Out New York chose Take This Waltz as one of the publications "Top Ten Tribeca Film Festival 2012 picks".    Rothkopf stated "her equally ambitious latest marks Polley as a serious explorer of broken relationships. Michelle Williams and Seth Rogen (more exposed than hes ever allowed himself to be) are married Torontonians who have settled into a too-comfortable domesticity. The simmering friction, caused in part by charming neighbour Luke Kirby, takes the film in surprising directions."  Stephen Holden of The New York Times commented "The temptations and perils of the grass is always greener syndrome arent as gripping a subject as Alzheimers, the topic of Ms. Polleys first film, Away From Her, but the movie radiates a melancholy glow." 

Stella Papamichael, writing for Digital Spy, gave the film three out of five stars. She praised Polleys approach to the film calling it "different, fresh and exciting", but not as "well-balanced" as Away From Her.    Papamichael added "Margot is an emotionally gritty role for Williams and she plays it brilliantly close to the edge, but she can seem at odds with a scenario that has more in common with a Mills & Boon fantasy than the real world."  The Guardians John Patterson proclaimed "Take This Waltzs practical wisdom about entropy in relationships and sense of resigned acceptance are leavened by an uncharacteristically active and talkative – and often very witty – performance from Williams."  Justin Chang from Variety (magazine)|Variety said "Given how quickly movie characters tend to fall into bed with one another, its especially rewarding to see writer-director Sarah Polley wring maximum tension, humor and emotional complexity from a young wifes crisis of conscience in Take This Waltz. Despite a few tonal and structural missteps, this intelligent, perceptive drama proves as intimately and gratifyingly femme-focused as Polleys 2006 debut, Away From Her."    Chang believed the film was "flat-out sexy enough" to appeal to audiences of either gender and praised Williams and Rogens performances. 
 New York Film Critics Circle Awards. 

===Accolades===
{| class="wikitable plainrowheaders" style="font-size: 95%;"
|- Award
! Date of ceremony Category
! Recipients and nominees Result
|-
! scope="row" rowspan="2" | Alliance of Women Film Journalists 
| rowspan="2" | January 7, 2013
| Best Woman Director
| Sarah Polley
|  
|-
| Best Woman Screenwriter
| Sarah Polley
|  
|-
! scope="row" rowspan="4" | Detroit Film Critics Society 
| rowspan="4" | December 14, 2012
| Best Actress
| Michelle Williams
|  
|-
| Best Director
| Sarah Polley
|  
|-
| Best Picture
| Take This Waltz
|  
|-
| Best Screenplay
| Sarah Polley
|  
|-
! scope="row" rowspan="4" | Directors Guild of Canada   
| rowspan="4" | October 20, 2012
| Best Direction in a Feature Film
| Sarah Polley
|  
|-
| Best Sound Editing
| Take This Waltz
|  
|-
| Best Production Design
| Matthew Davies
|  
|-
| Best Picture Editing
| Christopher Donaldson
|  
|-
! scope="row" rowspan="2" | Genie Awards  March 8, 2012 Best Actress in a Leading Role
| Michelle Williams
|  
|-
| Best Achievement in Make-Up
| Leslie Ann Sebert and David R. Beecroft
|  
|-
! scope="row" | Filmfest Hamburg 
| October 7, 2012
| Art Cinema Award
| Sarah Polley
|  
|-
! scope="row" | Hollywood Film Festival 
| October 24, 2011
| Best Actress
| Michelle Williams
|  
|-
! scope="row" rowspan="2" | San Diego Film Critics Society  December 11, 2012
| Best Actress
| Michelle Williams
|  
|-
| Best Original Screenplay
| Sarah Polley
|  
|-
! scope="row" | San Sebastian Film Festival 
| September 24, 2012
| Golden Seashell
| Take This Waltz
|  
|-
! scope="row" | St. Louis Film Critics  December 17, 2012
| Best Arthouse or Festival Film
| Take This Waltz
|  
|-
! scope="row" rowspan="2" | Vancouver Film Critics Circle  
| rowspan="2" | January 9, 2012 Best Actress in a Canadian Film
| Michelle Williams
|  
|-
| Best Supporting Actor in a Canadian Film.
| Seth Rogen
|  
|-
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 