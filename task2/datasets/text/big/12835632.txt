Kill the Umpire
{{Infobox film
| name           = Kill the Umpire
| image          = Kill the Umpire lobby card.jpg
| producer       = John Beck
| director       = Lloyd Bacon
| writer         = Frank Tashlin Ray Collins Richard Taylor Connie Marshall
| music          = Heinz Roemheld
| cinematography = Charles Lawton, Jr.
| editing        = Charles Nelson
| distributor    = Columbia Pictures
| released       =  
| runtime        = 78 min.
| country        = United States English
}}

Kill the Umpire is a 1950 baseball comedy film starring William Bendix and Una Merkel, directed by Lloyd Bacon and written by Frank Tashlin.

==Plot== Ray Collins) is a retired umpire.

During a period of unemployment, needing a job to support his loyal wife Betty (Una Merkel), Johnson is forced by his father-in-law to matriculate in an umpire school.  Johnson initially tries to get himself expelled by school director Jimmy OBrien (William Frawley), but eventually comes to enjoy his new job. He becomes an ump in the minor leagues, where blurred vision, caused by using the wrong eyedrops, causes him to see everything twice, earning him a nickname as "Two-Call" Johnson.

When he calls a popular player out at home plate, the crowd accuses him of dishonesty, leading to a near-riot during which the involved player is knocked out cold.  Johnson must disguise himself as a woman, and engage in several madcap subterfuges, to get to an important game on time, but his reputation is restored when the player he had originally called against publicly praises him for his honesty as an umpire.  The crowd accepts this, although quickly reversing its opinion again after Johnson, inevitably, makes another call they do not like.

The films climax is a manic chase scene, scripted by animator and future Jerry Lewis director Frank Tashlin.

== Cast ==
* William Bendix as Bill Johnson
* Una Merkel as Betty Johnson Ray Collins as Jonah Evans
* Gloria Henry as Lucy Johnson Richard Taylor as Bob Landon
* Connie Marshall as Suzie Johnson
* William Frawley as Jimmy OBrien
* Tom DAndrea as Roscoe Snooker

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 
 