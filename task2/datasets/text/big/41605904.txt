Zatoichi's Flashing Sword
{{Infobox film
| name = Zatoichis Flashing Sword
| image =
| director = Kazuo Ikehiro
| producer = Masaichi Nagata
| writer = Shozaburo Asai Minoru Inuzuka Kan Shimozawa (story)
| starring = Shintaro Katsu Tatsuo Endô Takashi Etajima
| music = Sei Ikeno
| cinematography = Yasukazu Takemura
| released = 11 July 1964
| runtime = 82 minutes
| country = Japan
| language = Japanese
}} Daiei Motion Picture Company (later acquired by Kadokawa Pictures).

Zatoichis Flashing Sword is the seventh episode in the 26-part film series devoted to the character of Zatoichi.

==Plot==
 
A young Yakuza, who is looking to make a name for himself, shoots Zatoichi in the back with a musket. Zatoichi is wounded, but is aided by a stranger: Miss Kuni. After recovering, Zatoichi travels to her home to thank her and repay her kindness by assisting in what household chores he can do.

Kunis father, Bunkichi, is a gang leader who controls a local river crossing. Yasugoro is a rival boss who wants to wrest control of the crossing so he can profit from raising the tolls.

==Cast==
* Shintaro Katsu as Zatoichi
* Naoko Kubo as Okuni
* Mayumi Nagisa as Oshizu
* Takashi Edajima as Seiroku Tatsuo Endo as Boss Yasugoro
* Yutaka Nakamura as Mekichi
* Bokuzen Hidari as Kyubei
* Ikuko Mori as Yasuhoro’s sister 

==References==
 

==External links==
*  
*  review by Brian McKay for eFilmCritic.com (22 February 2004)
*  , review by J. Doyle Wallis for Home Vision Entertainment (29 April 2003)

 

 
 
 
 
 
 
 
 

 