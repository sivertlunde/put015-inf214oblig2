Actor's and Sin
{{Infobox film
| name           = Actors and Sin
| image          = Actors and Sin theatrical poster.jpg
| image size     = 
| alt            = theatrical poster
| caption        = 
| director       = Ben Hecht Lee Garmes (co-director)
| producer       = Ben Hecht
| writer         = Ben Hecht
| narrator       = Dan OHerlihy Ben Hecht Marsha Hunt
| music          = George Antheil
| cinematography = Lee Garmes Otto Ludwig Sid Kuller Productions
| distributor    = United Artists  (1952 theatrical) 
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 American black-and-white comedy drama film written, produced and directed by Ben Hecht. {{cite book 
| title=Debut of a Sinner 
| publisher=Life (magazine)|Life magazine
| year=Jun 23, 1952 
| location=Vol. 32, No. 25 
| pages=121–122, 124 
| issn=0024-3019  Time  Marsha Hunt. {{cite book 
| title=The Edward G. Robinson encyclopedia 
| publisher=McFarland 
| author=Robert Beck 
| year=2002 
| pages=21 
| isbn=0-7864-1230-5 
| url=http://books.google.com/?id=nIBZAAAAMAAJ&q=%22Actors+and+Sin%22&dq=%22Actors+and+Sin%22}}  {{cite book 
| title=Theatre arts, Volume 36|publisher=Theatre Arts, Inc.|author=Society of Arts and Crafts (Detroit, Mich.) 
| year=1952 
| pages=42, 85}}  {{cite book 
| title=Creative careers in Hollywood|publisher=Allworth Communications, Inc. 
| author=Laurie Scheer 
| year=2002 
| pages=46–47 
| isbn=1-58115-243-4 
| url=http://books.google.com/?id=PEThCwOUFhAC&pg=PA47&dq=%22Actors+and+Sin%22#v=onepage&q=%22Actors%20and%20Sin%22&f=false}}   Also known by its section names of Actors Blood and Woman of Sin, the film debuted in New York City on May 29, 1952. Lee Garmes was co-director and cinematographer, as he was on most of the films Hecht directed.

==Plot==
The film   about legitimate theater.  The second section is Woman of Sin, a send-up of Hollywood greed.
 Broadway star Marsha Hunt) has been found shot dead in her apartment.  Her father Maurice (Edward G. Robinson) is himself an actor, and had watched her theater career rise as his own declined.  She had let success overcome her, and had thus alienated critics, fans, producers, and her playwright husband (Dan OHerlihy).  She had a few recent stage flops before being murdered. 

Woman of Sin takes place in Hollywood.  Dishonest agent writers agent Orlando Higgens (Eddie Albert) has been receiving frantic calls from Daisy Marcher (Jenny Hecht) about a screenplay she had written called Woman of Sin.  Thinking they are crank calls, Higgens tells her to never call his office again.  He then learns that through a mixup of the mails, her screenplay had been received by film mogul J.B. Cobb (Alan Reed), a man who once passed on Gone With the Wind based on Higgins advice. Cobb thinks that Higgins sent the script and offers Higgins a lucrative sum for the rights.  The problem is that Higgins has no idea where Daisy is, or that she is actually a nine-year-old child.

==Cast==
 
Actors Blood sequence:
*Dan OHerlihy as Alfred OShea, Narrator
*Edward G. Robinson as Maurice Tillayou Marsha Hunt as Marcia Tillayou
*Rudolph Anders as Otto Lachsley
*Peter Brocco as Frederick Herbert
*Robert Carson as Thomas Hayne
*Herb Bernard as Emile
*Alice Key as Miss Thompson, Tommy
*Irene Martin as Mrs. Murray
*Joseph Mell as George Murray
*Ric Roman as Clyde Veering
*Elizabeth Root as Mrs. Herbert
 
Woman of Sin sequence:
*Ben Hecht as Narrator
*Eddie Albert as Orlando Higgens
*Alan Reed as Jerome (J.B.) Cobb
*Jenny Hecht as Millicent Egelhofer, Daisy Marcher George Baxter as Vincent Brown John Crawford as Gilbert, Movie Actor Douglas Evans as Mr. Devlin Paul Guilfoyle as Mr. Blue Sam Rosen as Danello
*Tracey Roberts as Miss Flanagan
*Toni Carroll as Millicent, Movie Star
*Jody Gilbert as Mrs. Egelhofer
*George Keymas as Bill Sweitzer, Producer
*Alan Mendez as Captain Moriarity
*Kathleen Mulqueen as Miss Wright
*Cameo appearances by:
*Betty Field, Louis B. Mayer, and Jack L. Warner
 

==Reception==
In speaking toward the films two sections, DVD Talk writes "Both are light, breezy, and inconsequential, though admittedly written with an experts ear for dialogue and a knack for clever story twists."  They write that both sections "move at an efficient pace", and praise Ben Hecht for the dialog and rhythm of his scripts.  They also note that the actors were well chosen, finding flaw only in the child actors used in the Woman of Sin segment.  They did have critique about the material itself, noting that while Hecht knew his way around both Hollywood and Broadway, the subject matter comes off as a little "too inside".  They were also disappointed in the two stories, finding the plotlines "fairly hokey and predictable".  However, and despite the "hackneyed narrative", they found the film overall to be "very watchable", in that Hechts sense of timing kept the project from being boring. {{cite web 
| url=http://www.dvdtalk.com/reviews/40199/actors-and-sin-actors-blood-woman-of-sin/ 
| title=review: Actors And Sin: Actors Blood, Woman of Sin  
| publisher=DVD Talk 
| date=October 17, 2009 
| accessdate=May 22, 2011 
| author=Jamie S. Rich}} 

DVD Verdict wrote that "the most intriguing element" of the film, and not properly promoted by the films trailer, is that "it is actually two brief films combined in one package."  In analyzing Actors Blood, they wrote that there was "an opportunity for insight and depth in this story, but it would seem that Mr. Hecht wrote the screenplay while in a blind rage."  They offered that the material might even have been comedic but for it being "preposterously heavy-handed".  They felt that the actors generally spoke each line over-dramatically and floundered, with only Edward G. Robinson "able to make this work within the context of his character". In their analysis of Woman of Sin, they found it to be "reasonably engaging early on as a breezy satire", despite the concept of a story written by a nine-year-old "earning words of praise and adoration from the likes of Jack Warner and Louis B. Mayer". They noted that the cameos by the studio heads were amusing, but that the story was derailed by the use of Ben Hechts daughter Jenny in the role of child screenwriter Daisy Marcher.  They felt that she was "fingernails-on-a-blackboard grating" in this role, in that she "dials up every aspect of precociousness that can afflict a child actor as high as it can possibly go, and her presence effectively destroys any sense of comic momentum that the film had built up to that point," making her use a clear example of the problems inherent in nepotism.  They concluded that the film would stand as "an interesting curiosity for Hollywood history buffs, but fails as a cinematic experience." {{cite web 
| url=http://www.dvdverdict.com/reviews/actorsandsin.php 
| title=review: Actors And Sin 
| publisher=DVD Verdict 
| date=September 25, 2009 
| accessdate=May 22, 2011 
| author=Clark Douglas| archiveurl= http://web.archive.org/web/20110518142524/http://www.dvdverdict.com/reviews/actorsandsin.php| archivedate= 18 May 2011  | deadurl= no}} 

===Controversy===
Upon original release, several theater chains refused to screen the film due to its lampooning of stage and screen. This resulted in a lawsuit by United Artists and Sid Kuller Productions against the A. B. C. Theatres Company. {{cite news 
| url=http://select.nytimes.com/gst/abstract.html?res=F70F12FD3A58107A93C4A8178CD85F468585F9 
| title=HECHTS NEW FILM IN A LEGAL BATTLE; Coast Theatre Refuses to Show Actors and Sin – Producer Seeks Injunction in Court 
| work=The New York Times 
| date=July 16, 1952 
| accessdate=May 22, 2011}} 

==References==
 

==External links==
* 
*  at Allmovie
* 

 

 
 
 
 
 
 
 
 
 