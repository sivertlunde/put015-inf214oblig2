Frankenstein 80
{{Infobox film
| name           = Frankenstein 80
| image_size     =
| image	=	Frankenstein 80 FilmPoster.jpeg
| caption        =
| director       = Mario Mancini
| producer       = Benedetto Graziani (producer)
| writer         = Ferdinando De Leone (writer) Mario Mancini (writer)
| narrator       =
| starring       = See below
| music          = Daniele Patucchi
| cinematography = Emilio Varriano
| editing        = Enzo Micarelli
| distributor    =
| released       =
| runtime        = 85 minutes (USA) 85 minutes (Argentina)
| country        = Italy
| language       = Italian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Frankenstein 80 is a 1972 Italian film directed by Mario Mancini.

The film is also known as Frankenstein 80 in Italy (original title).

== Plot summary ==
By day, Dr. Frankenstein (Gordon Mitchell) works innocuously in his lab.  But at night, he works to perfect Mosaico (Xiro Papas), a monstrosity pieced together from dead bodies.  Once completed, the behemoth escapes from the lab and embarks on a killing spree.  Local beauties begin popping up dead, murdered in a variety of gruesome ways, as authorities attempt to stop Mosaicos rampage.

== Cast == John Richardson as Karl Schein
*Gordon Mitchell as Dr. Otto Frankenstein
*Renato Romano as Inspector Schneider the Frankenstein Monster
*Dalila Di Lazzaro as Sonia
*Roberto Fizz as Professor Schwarz
*Dada Gallotti as Butcher
*Marisa Traversi as Second Prostitute
*Lemmy Carson as Head Nurse Marco Mariani as Track Spectator
*Luigi Bonos as Hobo
*Enrico Rossi as First Investigator
*Fulvio Mingozzi as Second Investigator
*Umberto Amambrini as Vice Straus
*Luigi Antonio Guerra as Agent

== Critical reception ==
 Allmovie called the film "stupid, sickening, and obscene", but "seekers of psychotronic cinema will have a field day with this ridiculous Italian exploitation product." 

== References ==

 

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 
 