Thottilla
{{Infobox film 
| name           = Thottilla
| image          =
| caption        =
| director       = Karmachandran
| producer       = P Karmachandran
| writer         = P Karamchandran
| screenplay     = P Karamchandran
| starring       = Jayabharathi Kottarakkara Sreedharan Nair
| music          = R. K. Shekhar
| cinematography = ENC Nair
| editing        = N Gopalakrishnan
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by Karmachandran and produced by P Karmachandran. The film stars Jayabharathi and Kottarakkara Sreedharan Nair in lead roles. The film had musical score by R. K. Shekhar.   

==Cast==
*Jayabharathi
*Kottarakkara Sreedharan Nair

==Soundtrack==
The music was composed by R. K. Shekhar and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakaashathottilil ||  || Sreekumaran Thampi || 
|-
| 2 || Nin nadayil Annanada Kandu ||  || Sreekumaran Thampi || 
|-
| 3 || Omar Khayyaaminte ||  || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 