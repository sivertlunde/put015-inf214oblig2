Someone's Knocking at the Door
{{Infobox film
| name = Someones Knocking at the Door
| image = 
| image_size = 
| alt = 
| caption = 
| director = Chad Ferrin
| producer = {{plainlist|
* Sean Cain
* Chad Ferrin
* Noah Segan
}}
| screenplay = {{plainlist|
* Chad Ferrin
* Roham Ghodsi
* Rosie Roberts
}}
| starring = {{plainlist|
* Noah Segan
* Andrea Rueda
* Ezra Buzzington
* Elina Madison
* Jon Budinoff
* Ricardo Gray
* Lew Temple Vernon Wells
}}
| music = Brad Joseph Breeck
| cinematography = Nikklas Larsson
| editing = {{plainlist|
* Sean Cain
* Jahad Ferif
}}
| studio = Crappy World Films
| distributor = Vicious Circle Films
| released =  
| runtime = 80 minutes
| country = United States
| language = English
}} Vernon Wells.

==Plot== sexually sadistic serial killer couple from the 1970s who rape their victims to death.

Ray, a med student, sits in his dorm, watching stag films and shooting what at first appears to be heroin but is later revealed to be an experimental drug called "Taldon." He hears a knock on his door and answers it. He is greeted with the sight of a fully nude woman who begins to make sexual advances towards him. He reciprocates only for the woman to metamorphose into a grotesque man with a freakishly large penis. He is attacked by the man and it is strongly implied (and later confirmed) that the man rapes and kills him.

Rays friends Meg, Joe and Annie learn of Rays death. Another consort of Rays, Sebastian, suspects Joe of killing Ray in a bizarre sex game, as Joe was the last person to be seen with Ray. The protagonist, Justin, joins the four of them at Rays funeral. Meanwhile, police are baffled to learn that Ray died after a phallus, fifteen inches in length and four inches in diameter, pulverized his colon. The five remaining coeds come under suspicion and are interrogated. However, no evidence implicating them in the crime exists and they are allowed to leave. A flashback shows the six friends using Taldon, the drug found at the scene of Rays murder, and reading the case files of John and Wilma Hopper, a serial killer couple that was the subject of an intensive study at the school when it was still a mental hospital. The viewer is led to believe that the Hoppers, long thought to be dead, have returned via astral projection.

After a date, Sebastian and Annie park at a secluded place in the woods for some Making out|necking. Annie grows annoyed by Sebastians aggressive sexual behavior and is dropped off. Shortly thereafter, she comes face to face with a demonic Wilma Hopper who uses her genitalia to asphyxiate Annie. Back at the hospital, a detective reading information about the Hoppers is attacked by the reanimated John Hopper, who forces the detective to perform fellatio on him. Meg, after sharing a romantic evening with Justin, does more research on Taldon. It is revealed that the drug is a hallucinogen that stimulates the central nervous system to dangerous proportions. Her research is cut short when she is attacked by the now-undead detective from earlier, who chases her down the halls of the hospital before being gunned down. John Hopper, taking the form of Annie, seduces and kills Sebastian by anally raping him to death.

Meg, who had earlier passed out after her encounter with the possessed detective, lays on a hospital bed, recovering. She is joined in the same room by Justin. From this point onward, the plot descends into sheer madness. Justin, believing himself to be trapped in an artificial reality, guns down Joe and another detective before committing suicide. In a shocking turn of events that was hinted at earlier in the film, everything that has happened is revealed to be a hallucination in the mind of a dying Justin, who with his friends had overdosed on Taldon the night they first read about the Hopper spree. Justin is at first believed to have died with his friends (with the exception of Meg, who had sworn off drugs) but miraculously survives.

==Cast==
* Noah Segan as Justin William Carter II
* Andrea Rueda as Megan Laura "Meg" Wagner
* Ezra Buzzington as John Hopper
* Elina Madison as Wilma Hopper
* Jon Budinoff as Sebastian
* Ricardo Gray as Joe
* Lew Temple as Coroner Tom Collins Vernon Wells as Dr. Trotsky
* Jordan Lawson as Ray Harris
* Trent Haaga as Officer Tyler
* Sean Cain as Officer OMalley

==Production==
This was the first film that Ferrin directed based on someone elses screenplay.  Ferrin contacted Seagan for input on the script, and Segan eventually came on-board as a producer.  He also took a role that had been created during rewrites.   The film was shot in eleven days. 

==Release==
Someones Knocking at the Door premiered at Another Hole in the Head film festival on June 13, 2009.   It did not receive a wide release.  In the US, it played in Los Angeles on May 7, 2010, California,  and Las Vegas on July 24, 2010.   The DVD was released in the US on May 25, 2010,  and in the UK on March 7, 2011. 

==Reception==
Bloody Disgusting rated it 2.5/5 stars and wrote that "there is a meaning behind it all", but "the third act feels a little cheap and on-the-nose, at odds with the deceptively off-the-cuff invention that brings us to that point".   Steve Barton of Dread Central rated it 4/5 stars and called it "a love letter" to transgressive 1970s grindhouse cinema.   Shock Till You Drop wrote, "It was too coherent at the start to be able to claim the moniker of 70s drug movie."   Andrew Mack of Twitch Film called the ending "too tidy, too after school special and preachy".   Stephanie Scaife of Brutal as Hell wrote, "Its not absolutely dreadful and it is pretty much no-holds-barred in its approach to the rather outlandish subject matter."   Tyler Foster of DVD Talk rated it 3/5 stars and wrote, "Its not great, but its an interesting and memorable misfire". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 