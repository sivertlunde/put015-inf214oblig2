The Spider (1945 film)
 
{{Infobox film
| name           = The Spider
| image          = Spider noir poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Robert D. Webb
| producer       = Ben Silvey
| screenplay     = Scott Darling Jo Eisinger Irving Cummings Jr.
| story          = Fulton Oursler Lowell Brentano
| narrator       = 
| starring       = Richard Conte Faye Marlowe Kurt Kreuger
| music          = David Buttolph
| cinematography = Glen MacWilliams
| editing        = Norman Colbert	
| studio         = 
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Spider is a 1945 American crime film noir directed by Robert D. Webb. The drama features Richard Conte, Faye Marlowe, and Kurt Kreuger. 

==Plot==
A private detective is pursued by both police and a mysterious killer.

==Cast==
* Richard Conte as Chris Conlon
* Faye Marlowe as Delilah Lila Neilsen, alias Judith Smith
* Kurt Kreuger as Ernest, alias Garonne John Harvey as Burns
* Martin Kosleck as Mikail Barak
* Mantan Moreland as Henry
* Walter Sande as Det. Lt. Walter Castle
* Cara Williams as Wanda Vann, neighbor
* Charles Tannen as Det. Tonti
* Margaret Brayton as Jean, police records clerk Ann Savage as Florence Cain
* Harry Seymour as Ed, the Bartender
* Jean Del Val as Henri Dutrelle, hotel manager
* Odette Vigne as Mrs. Dutrelle
* James Flavin as Officer Johnny Tracy

==Critical reception==
Film critic Dennis Schwartz gave the film a mixed review, writing, "Robert D. Webb directs a lackluster B-film noir from the play by Charles Fulton...The Spider was a poor remake of the 1931 film of the same title. It held very little suspense, and the plot was filled with gaping holes. But Richard Conte is a fine action actor, and gives this slight film noir story a little boost just by his presence." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 