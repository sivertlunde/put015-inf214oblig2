Lemon Popsicle
{{Infobox film
| name           = Lemon Popsicle
| image          = Lemon Popsicle (film poster).jpg
| caption        = Theatrical release poster
| director       = Boaz Davidson
| producer       = Yoram Globus Menahem Golan
| writer         = Boaz Davidson Eli Tavor
| narrator       =
| starring       = Yftach Katzur Zachi Noy Jonathan Sagall
| music          =
| cinematography = Adam Greenberg
| editing        = Alain Jakubowicz
| studio    = Noah Films
| distributor    = Noah Films (Israel)  SCO (West Germany)
| released       =  
| runtime        = 95 minutes
| country        = Israel
| language       = Hebrew
| budget         = Israeli lira|I£3,000,000
| gross   = Israeli lira|I£12,500,000 (Israel; 1978)
}}

Lemon Popsicle (Israeli: Eskimo Limon,  ) is a 1978 Israeli cult film directed by Boaz Davidson which led to a series of sequels.  The movie follows a group of three teenagers in their quest to lose their virginity. 


==Synopsis==
Nili (Niki in English-language release, played by Anat Atzmon), a beautiful new girl comes to the school of the friend trio. Benzi, the typical "nice guy" of the three immediately falls in love with Nili. However, Nili prefers more pushing and experienced Momo who is keen on deflowering her. Later, it is revealed that Momo had impregnated and dumped Nili. Benzi, hoping to start a relationship with her, helps Nili get an abortion and emotionally consoles her, only to see that she soon returns to the arms of Momo.  
 olah named Stella (Ophelia Shtruhl) enticing the three kids into having sex with her in sequence, and then earning the nickname "Stella HaMegameret" ("A-cumming Stella") for she screams "Im a-cumming! Im a-cumming!" (instead of "cumming") during sex because of her poor Hebrew language|Hebrew.

==Release and reception==
===Budget===
The picture was produced at a budget of 3 million Israeli lira, of which a million was paid in royalties to the musicians (mostly American) whose songs were used in the soundtrack. Almog, Oz. Peridah mi-Śeruliḳ: shinui ʻarakhim ba-eliṭah ha-Yiśreʼelit. Zemorah-Bitan (2004). ISBN 9789653110519. p. 1156.  

===Box office=== Best Foreign Language Film at the 51st Academy Awards, but was not accepted as a nominee. 

==Legacy==
===Sequels===
The series became a success in Germany under the name Eis am Stiel. Most of the films were also dubbed into English and were released in both the United States and United Kingdom. Since the release of Lemon Popsicle, eight sequels have been made (beginning in 1979). 

===Remake===
In 1982, Davidson wrote and directed an American remake, The Last American Virgin, which was not met with the same success as the original. 

==See also==
* List of submissions to the 51st Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
 
*  
*  
*  
*  
*  
*  
* Spin-off  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 