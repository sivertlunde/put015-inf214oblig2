La locandiera (film)
{{Infobox film
 | name = La locandiera 
 | image = La locandiera (film).jpg
 | caption =
 | director =Paolo Cavara
 | writer = Carlo Goldoni (play), Leo Benvenuti, Piero De Bernardi, Lucia Dembry
 | starring = Adriano Celentano, Paolo Villaggio, Claudia Mori, Marco Messeri 
 | music = Detto Mariano
 | cinematography =  Mario Vulpiani
 | editing =  Angelo Curi
 | language = Italian 
 | country = Italy
 | runtime = 109 min
 | released = 1980
 }}

La locandiera (also known as Mirandolina) is a 1980 Italian comedy film directed by Paolo Cavara, based on the Carlo Goldonis three-act comedy The Mistress of the Inn.    

== Plot ==
The innkeeper Mirandolina is a beautiful girl, and for her beauty the Count of Albafiorita (Conte di Albafiorita) and the Marquis of Forlimpopoli (Marchese di Forlimpopoli) fall in love. The two noble customers bestow rich gifts to her, trying to ingratiate and to marry her, but the crafty Mirandolina only accepts the money and jewels, leaving them with an inch of them nose. A third man arrives on the scene: the Knight of Ripafratta (Cavaliere di Ripafratta) extremely  misogynist, who thinks that the woman bears only man trouble. Mirandolina, who has never seen a man like that, tries to do it on purpose and the knight manages to fall in love with her.

== Cast ==

* Claudia Mori as Mirandolina 
* Adriano Celentano as Cavaliere di Ripafratta
* Paolo Villaggio as Marchese di Forlimpopoli
* Marco Messeri as Conte di Albafiorita
* Lorenza Guerrieri as  Ortensia
* Gianni Cavina as Ortenzio
* Milena Vukotic as Dejanira
* Camillo Milli as Carlo Goldoni

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 