Jeevanadhi
{{Infobox film|
| name = Jeevanadhi
| image = 
| caption =
| director = D. Rajendra Babu
| screenplay = D. Rajendra Babu
| writer = Sainath (dialogues) Vishnuvardhan  Kushboo  Urvashi
| producer = P. Dhanraj
| music =  
| cinematography = Prasad Babu
| editing = Shyam
| studio = Sri Dhanalakshmi Creations
| released = 1996
| runtime = 148 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada drama drama film Kushboo in Koti upon release.

== Cast == Vishnuvardhan 
* Ananth Nag Kushboo  Urvashi
* Tara 
* Umashree
* Srinivasa Murthy
* R. N. Jayagopal
* Doddanna
* Shivaram
* Bank Janardhan
* Kishori Ballal

==Soundtrack==
{{Infobox album
| Name        = Jeevanadhi
| Type        = Soundtrack Koti
| Cover       = Jeevanadhi album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 
| Recorded    =  Feature film soundtrack
| Length      = 40:14
| Label       = Jhankar Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}
 Koti and lyrics written by R. N. Jayagopal.  The album consists of eight soundtracks.  The song "Kannada Naadina Jeevanadhi" was received extremely well and became very popular. Another soundtrack "Yello Yaaro Hego" from the film was Sonu Nigams first song in Kannada as a playback singer. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| total_length = 40:14
| title1 = Daivada Karuneyo
| extra1 = Rajesh Krishnan, Manjula Gururaj
| lyrics1 = R. N. Jayagopal
| length1 = 5:22
| title2 = Ee Andada Chendada
| extra2 = Rajesh Krishnan
| lyrics2 = R. N. Jayagopal
| length2 = 4:19
| title3 = Ee Andada
| extra3 = S. P. Balasubrahmanyam
| lyrics3 = R. N. Jayagopal
| length3 = 4:19
| title4 = Kannada Nadina Jeevanadi
| extra4 = S. P. Balasubrahmanyam
| lyrics4 = R. N. Jayagopal
| length4 = 5:50
| title5 = Kannada Nadina Jeevanadi - Duet
| extra5 = S. P. Balasubrahmanyam, Anuradha Paudwal
| lyrics5 = R. N. Jayagopal
| length5 = 5:49
| title6 = Navamasa Ninnanu Horalilla
| extra6 = Anuradha Paudwal, Rajesh Krishnan
| lyrics6 = R. N. Jayagopal
| length6 = 4:55
| title7 = Sheshadrivasa Sri Tirumalesha
| extra7 = Rajesh Krishnan, Manjula Gururaj
| lyrics7 = R. N. Jayagopal
| length7 = 4:45
| title8 = Eello Yaaro Hego
| extra8 = Sonu Nigam
| lyrics8 = R. N. Jayagopal
| length8 = 4:55
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 


 