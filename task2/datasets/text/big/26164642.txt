Vice Raid
{{Infobox film
| name           = Vice Raid
| image          = Viceraid1.jpg
| caption        =
| director       = Edward L. Cahn
| producer       = Robert E. Kent Edward Small (uncredited)
| writer         = Charles Ellis
| starring       = Mamie Van Doren Richard Coogan
| studio = Imperial
| distributor    = United Artists
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
}} 1960 B-movie crime drama starring Mamie Van Doren and Richard Coogan. It was issued on a double bill with Inside the Mafia. Mafia, Vice Raid Routine New Efforts
Stinson, Charles. Los Angeles Times (1923-Current File)   11 Dec 1959: B14.  

==Plot==

Police Sgt. Whitey Brandon works for the Vice Squad and is determined to beat corruption in the city. He encounters Carol Hudson who is working as a model. She is sent to frame him and succeeds. Carols sister comes to visit and is raped and bashed by a thug who knows Carol. Carol, desperate for revenge, enlists the help of Brandon to fight the thugs who attacked her sister.

==Cast==

*Mamie Van Doren as Carol Hudson
*Richard Coogan as Police Sergeant Whitey Brandon
*Brad Dexter as Vince Malone
*Barry Atwater as Phil Evans
*Carol Nugent as Louise Hudson
*Frank Gerstle as Captain William Brennan Joseph Sullivan as Police Sergeant Ben Dunton
==Production==
Van Doren signed a three picture deal with Small but this was the only movie she made for him. Dwayne Hickman Signs for TV Deal That Will Net Half Million
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   19 Dec 1958: a13.  
==References==
 

==External links==
*  

 
 

 
 