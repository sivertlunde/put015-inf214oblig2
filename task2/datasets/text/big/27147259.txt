Kakashi
 
{{Infobox animanga/Header
| name            = Kakashi
| image           =  
| caption         = Japanese movie poster
| ja_kanji        = 案山子
| ja_romaji       =  Horror
}}
{{Infobox animanga/Print
| type            = manga
| author          = Junji Ito
| publisher       = 
| demographic     = 
| magazine        = 
| published       = 
| first           = 
| last            = 
| volumes         = 
| volume_list     = 
}}

{{Infobox animanga/Video
| type            = live film
| director        = Tsuruta Norio
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = 2001
| runtime         = 86 minutes
}}
 

 , is a 2001 horror film based on the manga of the same name, the film chronicles a tale of Kaoru Yoshikawa whose search for her missing brother Tsuyoshi leads her to Kozukata, an isolated village that seems to harbor dark secrets.


== Plot ==

The opening text narrates the tradition of "Kakashi", where humans would burn animal and human hairs to prevent evil spirits from entering Earth. They eventually began to burn human effigies made of straws as it also attract the spirits of the dead so they can interact with them. Little they know that it may bring consequences far greater than they thought.

Opening in medias res, lead protagonist, Kaoru Yoshikawa (Maho Nonami) is shown screaming in front of flames, asking why it had to be done to her. In the beginning, Kaoru is a young woman who has close relationship with her brother, Tsuyoshi; their parents had been dead a long time ago. Unable to contact him for a week, she goes to his apartment and finds an envelope near his telephone. Inside it are bits of straws and a letter from Izumi Miyamori, a former friend of Kaoru who wants Tsuyoshi to meet her. The letters address comes from Kozukata Village, located in the hills of a mountain. While going there, Kaoru sees a missing person poster of a Chinese woman named Sally Chen, nearby which she finds a tunnel leading to the village. Going inside the tunnel, her car breaks down, forcing her to walk on foot. On the way, she hears a womans laugh. Kaoru does arrive at Kozukata, but all of the villagers act cold to strangers, only informing her about the upcoming "Kakashi Matsuri" (Scarecrow Festival). Arriving at a red-draped windmill, Kaoru meets Sally (Grace IP), who is playing with a little girl, Ayumi Noji (Mizuho Igarashi), whose father, Shusaku (Yoshiki Arizono) hastily arrives to take her, warning her not to talk to strangers. Finding Sally quickly leaving the scene, Kaoru heads to the Miyamori residence and meets with Izumis parents. While Izumis father (Kenzo Kawarasaki) greets her warmly, his wife (Lily (entertainer)|Lily), acts cold like the other villagers. He offers to let Kaoru stay for several days and tells her that Izumi is in the hospital.

On her first night, Kaoru dreams of meeting Izumi (Kou Shibasaki) who seemingly dislikes her appearance, as well as an Izumi-shaped scarecrow. Heading to the local police station to fix her car, she meets Sally again who antagonizes her when she discovers her father sitting in the desk. The next night, Kaoru dreams of seeing Tsuyoshi making an Izumi-shaped scarecrow. Going back to the station the next day, she is shocked to learn that Sallys father is in fact a living scarecrow, and she is promptly attacked by other citizens of the village who are also living scarecrows. Sally instructs her to leave while she takes care of them. In the Miyamori residence, Kaoru heads upstairs and finds Izumis diary, where she learns that Izumi had fallen in love with Tsuyoshi, but Kaorus overprotective attitude prevents her from reaching him, making her cursing Kaoru for her entire life. She meets Izumis ghost who says that Kaoru is jealous of her brother being taken away from herand that she will go alive that night. Running away, Kaoru confronts Izumis father who admits that Izumi had actually been dead several years ago, and that his appearance in the village is merely trying to celebrate the festival as he believed it will give chance for him and his wife to reunite with her. This act is not only done by them but also all other villagers, including Sally who travels from far away to reunite with her dead father. However, he realizes that the scarecrows will turn into mindless monsters once they are made, which are further fueled by Izumis malevolent spirit who haunts the village since she died grieving.

Izumis father takes Kaoru to the clinic where her brother (Shunsuke Matsuoka) is, who had been in a trance since his arrival. Kaoru manages to bring him out by slapping him. After convincing and picking up Sally, the trio escape from the scarecrows while making their way to the tunnel before the festival starts. Meanwhile, Izumis father is killed by Ayumi, who is revealed to be a scarecrow. As the festival starts, Izumis scarecrow is the first to come to life and she subsequently kills her mother by snapping her in the neck. The trio stumbles to the windmill where Izumi waits. Tsuyoshi decides to sacrifice himself to kill Izumi by burning them alive, although Izumis laugh reveals that this is done as an act of revenge to Kaoru, who watches in shock. Kaoru and Sally hastily go to the tunnel while other scarecrows come to life. Just as they are about to reach outside, Tsuyoshis spirit appears and calls Kaoru. Realizing that she has no place to go back and Tsuyoshi being the only one who cares her, she goes after Tsuyoshi, despite Sallys protests.

== Cast ==

* Maho Nonami as Kaoru Yoshikawa
* Kou Shibasaki as Izumi Miyamori  
* Grace IP as Sally Chen  
* Shunsuke Matsuoka as Tsuyoshi Yoshikawa Lily as Yukie Miyamori
* Kenzo Kawarasaki as Kozo Miyamori
* Yoshiki Arizono as Shusaku Noji
* Mizuho Igarashi as Ayumi Noji
* Yōji Tanaka as Policeman

== Production ==

In 2001, Kakashi was adapted into a live-action Japanese horror film, directed by Norio Tsuruta, starring Maho Nonami, Ko Shibasaki and Grace Yip.

== Reception ==

Derek Elley reviewed the film for Variety (magazine)|Variety and concluded that "Tsurutas deliberately restrained direction and a gentle ostinato score by Shinichiro Ogata are very effective in the opening reels at building a sense of foreboding from natural surroundings. Ultimately, however, film is let down by Nonamis rather bland performance as Kaoru and a lack of dramatic clout at the end." 

Beyond Hollywoods review said that "whilst “Uzumaki (film)|Uzumaki” was a wacky affair, featuring wild special effects and odd transformations, “Kakashi” takes a very different route, focusing instead on atmosphere and melodrama, recalling more adult films such as “Inugami (film)|Inugami”" and concludes that "those who enjoy deliberately paced, mournful films that focus on mood and character are likely to be entertained and satisfied, as this is one of the better examples of the last few years." 

== Manga ==
The story appeared in   as  , of which the fourth volume was Kakashi (2002, ISBN 9784257721697, 2007 ISBN 9784022670083).  Dark Horse Comics started translating this series but they only released the first three volumes - the first two were given over to Tomie and the third was The Long Hair in the Attic. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 