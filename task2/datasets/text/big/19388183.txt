A Victim of the Mormons
{{Infobox film
| name           = A Victim of the Mormons
| image          = A Victim Of The Mormons 1.jpg
| caption        = Nordisk Films marketing pamphlet for the films release in England
| director       = August Blom Ole Olsen
| writer         = Alfred Kjerulf Clara Wieth
| music          =
| cinematography = Axel Graatkjær
| editing        =
| distributor    = Nordisk Film
| released       =  
| runtime        =
| country        = Denmark
| language       = silent film
| budget         =
}}

A Victim of the Mormons ( ) is a 1911 Danish silent film directed by August Blom. The film was controversial for demonizing the Mormon religion, and its box-office success is cited for initiating a decade of anti-Mormon propaganda films in America.   It tells the story of an attractive young woman who is seduced and kidnapped by a Mormon missionary, then forced to accompany him to Utah to become one of his wives. The film became a hit, partly due to the popularity of its star, Valdemar Psilander, and partly due to the publicity arising from objections by The Church of Jesus Christ of Latter-day Saints (LDS church) and its failed campaign to ban the film. 

== Plot ==
Florence Grange (Clara Pontoppidan), a pretty young Danish woman, is vacationing with her father, her brother, George, and her fiance, Leslie, at a luxurious seaside hotel in Denmark. One evening, while they are sitting in the restaurant, George introduces them to a young American named Andrew Larson (Valdemar Psilander). Andrew, who is a Mormon priest, is quickly attracted to Florence and gives her an "admission card" to a Mormon meeting. Although Florence cares greatly for Leslie (Carlo Wieth), he often neglects her while pursuing other interests. Florence spends much time with the strangely fascinating American, enabling Andrew to indoctrinate her and convince her to go to Utah with him. Partly in passion and partly in hypnotic trance, Florence runs away and meets Andrew at the railway station.

After her disappearance, her father searches her room and finds the note from Andrew asking her to come to the train. Leslie and George (Henry Seemann) inform the police of Florences disappearance and the police notify the harbor patrol. With help from a Mormon friend, Andrew drugs Florence, then exchanges her hat and coat with another couple, enabling Andrew to sneak Florence aboard a steam ship heading for America. The harbor patrol detains the couple wearing Florences clothes. When George and Leslie arrive to get Florence, the ruse is revealed, but the ship has already sailed. While on board, Florence has a change of heart and wants to return home. Andrew assures her that they will return home. A telegraph is sent to the ship about the kidnapping, but Andrew overpowers the telegraph operator before his plot can be revealed. The ship arrives in America and Andrew escapes with Florence to Salt Lake City where he locks her in a bedroom. George and Leslie leave aboard the next ship for America.

Florence gains the sympathy of Andrews first wife (Emilie Sannom), but an attempt to set Florence free is unsuccessful when Andrew discovers it. One day, Andrew leaves home to perform a baptism at the Mormon temple. George and Leslie, having arrived in Salt Lake City, follow Andrew to his house. Andrew convinces them that Florence is elsewhere in the city. While the two race off on a wild goose chase, Andrew conceals Florence in a dark, dank cellar with a secret trapdoor entrance. Realizing that they were tricked, Leslie and George burst back into Andrews house and search desperately for Florence, but cannot find her. Andrew continues to plead his innocence. By luck, Florence discovers the hidden button which opens the secret trap door to her cell. Leslie frees her and promises he will never again neglect her. Andrew pulls a pistol and tries to kill Florence. Leslie prevents him and during the ensuing struggle Andrew shoots himself and dies.

== Cast ==
*Valdemar Psilander as Andrew Larson the Mormon Priest. Having been hired by Nordisk Film earlier that year, A Victim of the Mormons was only Psilanders third film. However, because of his outstanding performances in the other two films, Psilander was already Nordisks highest paid actor. During the next 6 years, Psilander made 83 films and remained the highest paid Danish actor until 1917 when he committed suicide at the age of 32. 
*Clara Pontoppidan as Florence Grange. Her character was named Nina Gram in the original Danish version. Pontoppidan is credited as Clara Wieth in the film, being married at that time to Carlo Wieth (who played the role of her fiance).
*Henry Seemann as George Grange, Florences brother. His character was named Olaf Gram in the original Danish version.
*Carlo Wieth as Leslie Berg, Florences fiance. Wieths character was named Sven Berg in the original Danish version. At the time of filming, he was married to Clara Pontoppidan. They later divorced and he married the actress Agnes Thorberg and became the father of the actor, Mogens Wieth.
*Carl Schenstrøm as Andrews Mormon friend. Schenstrøm would later go on to fame with his partner Harald Madsen as the silent film comedy duo of Fyrtårnet og Bivognen (literally translated as Lighthouse and Sidecar), the precursors to Laurel and Hardy.
Other cast: Franz Skondrup as the Police Detective; Emilie Sannom as Nancy, the First Wife; Otto Lagoni as Andrews Mormon friend; Frederik Jacobsen as Florences Father; Nicolai Brechling as the Telegraph Operator; Carl Petersen as the Police Officer; Axel Boesen; H.C. Nielsen; and Doris Langkilde.

== Production ==
A Victim of the Mormons followed a series of films by the Nordisk Film studio which portrayed young naive women who are kidnapped, held captive and in imminent danger of sexual assault. Early titles included The White Slave Girl (Den hvide slavinde, 1907), The White Slave Trade (Den hvide slavehandel, 1910) and The White Slave Trades Last Victim (Den hvide slavehandels sidste offer, 1910).
 white slavery, but audiences enjoyed the sensationalized melodrama and the exploitation of young women placed in compromising positions. Because of the popularity of the earlier "white slave" films, Victim of the Mormons became a prestige project for Nordisk. August Blom, the studios bright new director and later head of production, had a flair for melodrama and he was chosen to direct. The studios new star, Valdemar Psilander, who had earlier that year achieved international recognition in At the Prison Gates (Ved Fængslets Port), was cast as the villain.

In 1911, Nordisk Film had begun to concentrate on longer feature-length movies,  and, as such, Blom used three reels for A Victim of the Mormons. With a length of 3200 feet, it was one of the longest Danish films at that time. 

== Release and reaction ==
A Victim of the Mormons premiered at the Panoptikon Theater in Copenhagen, Denmark on 2 October 1911. One week later, on 10 October  1911, it was released in London. Before the release of the film, the Nordisk Film company ran a heavy advertising campaign. They distributed sensational programs which declared "Extraordinary Exposure of a Terrible Doctrine!" and "This exciting and effective modern drama, which reveals the Latter Day Saints ruthless propaganda, is one of films great international successes." 

  (February 10, 1912).]]

Advertising in film trade papers declared the movie would achieve record-breaking ticket sales. The trade papers followed suit with sensational headlines including "This Great Winner Creates a Record Booking," in Bioscope magazine and  the film "HAS NO EQUAL AS A MONEY-MAKER" in the American trade Motion Picture News. 

The movie was publicly condemned by leaders of the LDS Church in Europe and America. The church presidency complained to the American National Board of Censorship, demanding that all references to Mormonism in the title and content of the film be removed.  However, the LDS campaign to censor the film failed when it was released without changes. Utah governor William Spry said the Danish filmmakers were "only exceeded in their perversion of the truth by their absurdity,"  and banned the film in the state of Utah.  That effort also failed and the film was shown without alterations in Utah theaters that year.  The movie proved to be enormously successful internationally, not only because of an effective promotional campaign which emphasized the attempted ban, but also due to the popularity of its star, Valdemar Psilander.

By the time A Victim of the Mormons was released in the United States in February 1912, several other films featuring Mormon antagonists were already being readied for release. The new titles included Marriage or Death and the Mountain Meadows Massacre by the French studio Pathé Frères, The Mormon by the American Film Manufacturing Company, and The Danites by the Selig Polyscope Company of Chicago. During the following decade, the output of anti-Mormon propaganda films flourished with Mormon villains portrayed variously as white slavers, mesmerizing Svengalis and ruthless terrorists. 

Only approximately thirty minutes of A Victim of the Mormons has been recovered and preserved from the original 35mm film stock. The remaining footage has been transferred to 16mm and video tape. A copy is on file in the LDS Church archives in Salt Lake City. 

== See also ==
* List of incomplete or partially lost films

== References ==
 
; Citations
 
*  <!--
-->
*  <!--
-->
*  <!--
-->
*  <!--
-->
*  <!--
-->
*  
 

== External links ==
 
*   Den Danske Film Database (in Danish)
*   Det Danske Filminstitut (in Danish)
*  
*  

 
 
 
 
 
 
 
 
 
 
 