Luxury Liner (1933 film)
{{Infobox film
| name = Luxury Liner 
| image =
| image_size =
| caption =
| director = Lothar Mendes
| producer = B.P. Schulberg 
 | writer = Gina Kaus (novel)   Gene Markey   Kathryn Scola
| narrator =
| starring =  George Brent   Zita Johann   Vivienne Osborne   Alice White
| music = 
| cinematography = Victor Milner
| editing = Eda Warren     
| studio = Paramount Pictures
| distributor = Paramount Pictures
| released = February 3, 1933
| runtime = 70 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Jew Süss. 

==Main cast==
*   George Brent as Dr. Thomas Bernard 
* Zita Johann as Miss Morgan  
* Vivienne Osborne as Sybil Brenhard  
* Alice White as Milli Stern  
* Verree Teasdale as Luise Marheim 
* Frank Morgan as Alex Stevenson 
* C. Aubrey Smith as Edward Thorndyke 
* Wallis Clark as Dr. Veith  Henry Wadsworth as Fritz 
* Billy Bevan as Schultz  
* Theodore von Eltz as Exl 
* Barry Norton as Prince Vladimir Gleboff  
* Henry Victor as Baron von Luden  
* Edith Yorke as Mrs. Webber - Sick Passenger 
* Christian Rub as Peasant Father

==References==
 

==Bibliography==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise Cinegraph: Encyclopaedia of German Cinema. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 