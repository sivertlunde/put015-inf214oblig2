When the Last Sword Is Drawn
{{Infobox film
| name = When the Last Sword Is Drawn
| image = When the Last Sword Is Drawn.jpg
| image_size = 
| caption = Theatrical poster for When the Last Sword Is Drawn (2003)
| director = Yōjirō Takita 
| producer = Nozomu Enoki Hideji Miyajima
| writer = Jiro Asada (story) Takehiro Nakajima starring       = Kiichi Nakai Kōichi Satō Yui Natsukawa Takehiro Murata music          = Joe Hisaishi cinematography = Takeshi Hamada editing        = Nobuko Tomita distributor    = Shochiku released  2003
|runtime        = 137 min. country        = Japan language       = Japanese budget         = $4,000,000  gross          = $2,487,338 
}} 2003 Japanese 2004 Japanese Academy Awards, as well as the prizes for Best Actor (Kiichi Nakai) and Best Supporting Actor (Kōichi Satō). It received a further eight nominations. 

==Synopsis== Nambu Morioka. fall of flashbacks as two characters reminisce. The themes include conflicting loyalty to the clan, lord, and family.

More than just a story of swordplay, it is the story of a man willing to do anything for the good of his family, even if it means never being able to see them.

 

==Cast==
*Kiichi Nakai — Yoshimura Kanichiro
*Kōichi Satō — Saitō Hajime
*Yui Natsukawa — Shizu/Mitsu
*Takehiro Murata — Ono Chiaki
*Miki Nakatani — Nui
*Yuji Miyake — Ohno Jiroemon
*Eugene Nomura —  Hijikata Toshizo
*Masato Sakai — Okita Soji Atsushi Itō — Young Chiaki Ono
 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 


 