Theremin: An Electronic Odyssey
{{Infobox film
| name           = Theremin: An Electronic Odyssey
| image_size     = 
| image	=	Theremin- An Electronic Odyssey FilmPoster.jpeg
| alt            = 
| caption        =  Steven M. Martin
| producer       = Steven M. Martin
| writer         = Steven M. Martin
| narrator       = 
| starring       = Léon Theremin Clara Rockmore Robert Moog Lydia Kavina Nicolas Slonimsky Brian Wilson Todd Rundgren
| music          = Hal Willner Robert Stone Ed Lachman 
| editing        = 
| studio         = Kaga Bay Channel Four Films Orion Classics Fox Video (2003, DVD)
| released       =   August 29, 1993 (Channel 4) 
| runtime        = Varies (79 -84  minutes)
| country        = 
| language       = 
| budget         = 
| gross          = 
}} Steven M. Martin about the life of Leon Theremin and his invention, the theremin, a pioneering electronic musical instrument. It follows his life, including being imprisoned in a Soviet gulag, and the influence of his instrument, which came to define the sound of eerie in 20th Century movies, and influenced popular music as it searched for and celebrated electronic music in the 1960s.

==Reception== Documentary Filmmakers Trophy at the 1994 Sundance Film Festival.  It was also nominated for an International Emmy as well as a BAFTA, the Huw Wheldon Award for the Best Arts Programme, one of the British Academy Television Awards.  Theremin was named to the Top Ten Films of the Year lists in Los Angeles, Boston, and Washington DC, and was invited to almost every important film festival in the world, including The New York Film Festival, set a record for the longest question and answer period at the National Gallery in Washington, and was shown by invitation of the Russian Ministry of Culture to top scientists in St. Petersburg.

  

In a December 1995 review, Roger Ebert wrote: 
:Watching Theremin: An Electronic Odyssey is a curious experience. You begin with interest, and then you pass through the stages of curiosity, fascination and disbelief, until in the last 20 minutes, you arrive at a state of dumbfounded wonder.  It is the kind of movie that requires a musical score only the Theremin possibly could supply.

==Home Media==
Theremin: An Electronic Odyssey was released on DVD by MGM Home Video on April 1, 2003.

==See also==
*Moog (film)|Moog

==References==
 

==External links==
* 
*  from the Movie Review Query Engine

 
 
 
 
 
 
 