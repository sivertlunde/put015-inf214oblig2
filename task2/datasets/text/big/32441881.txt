I Am a Camera (film)
{{Infobox film
| name           = I Am a Camera
| image          = IAACDVD.jpg
| image size     = 225px
| alt            =
| caption        = Region 2 DVD cover
| director       = Henry Cornelius John Woolf   Jack Clayton (associate producer) John Collier
| based on       =   (book)     (play)
| starring       = Julie Harris   Laurence Harvey   Shelley Winters   Ron Randell   Lea Seidl   Anton Diffring
| music          = Malcolm Arnold Guy Green
| editing        = Clive Donner
| studio         = Remus Films
| distributor    = Independent Film Distributors (UK)  Distributors Corporation of America (US)
| released       = 21 July 1955 (UK and LA) Mayer, p. 200 
| runtime        = 98 minutes
| country        = UK
| language       = English
| budget         =
| gross          =£144,666 (UK) 
}}
 John Collier, I Am a Camera stars Laurence Harvey as Isherwood and Julie Harris recreating her Tony Award winning performance as Sally Bowles.
 1966 stage 1972 film adaptation of the same source material, contemporary critics have noted the historic interest of this earlier presentation.

==Plot== flashes back to Berlin, New Years Eve 1931. Broke and frustrated with his writing, Christopher plans to spend the night in but his would be gigolo friend Fritz insists they go to a night club to see Fritzs new inamorata, Sally Bowles, perform. Fritz hopes to live off Sallys earnings as a film star but his ardor quickly cools at the sight of her fiancé Pierre, with whom she plans to leave for Paris that night. Instead, Pierre absconds with her money. Chris, taking pity on her, invites her to stay at his boarding house. They arrange for Chris to move to a smaller room and for Sally to take his old room. Over the course of a long and unproductive winter in which Chris cannot write and Sally finds no work, Chris attempts to initiate a sexual relationship with Sally. She rejects him, saying it would spoil their friendship.
 wires that his plans have changed. Chris and Sally have a terrible fight, resulting in a rift in their friendship and Sallys planned departure.

Feeling as though he has reconnected with real life, the formerly apolitical Christopher starts a street altercation with a group of Nazis. Returning home he discovers that Sally has not left because she is pregnant. Christopher proposes marriage but Sally refuses him.

Writing up an account of his Nazi altercation, Chris sells his "Portrait of Berlin" to an American magazine to raise money for Sally to have an abortion. The magazine editor hires Chris to write a series of Portraits of European cities, expecting him to leave the following day. When he returns home Sally has changed her mind; she plans to keep the baby and marry Chris. The next morning, Sally tells Chris that she has mis-figured the dates and was never actually pregnant. She is also leaving Berlin for Paris, in pursuit of a film executive with whom Clive has connected her.

Back in present-day London, Christopher and Sally reunite. Upon learning that Sally is again penniless and homeless, Chris invites her to stay in his spare room.

In a subplot, Fritz tries to secure the affections of Natalia Landauer, a wealthy Jewish department store heiress and Christophers student of English. When Natalia fails to respond to his charms, Sally suggest that he "pounce", make a sexual advance. He reports that this tactic is unsuccessful and Natalia refuses to see him. Fritz confesses to Christopher that he is Jewish and has been concealing it for years, but vows to stop lying about his heritage. Their story concludes with their announcement to Chris and Sally that they plan to marry and emigrate to Switzerland.

==Cast==
* Julie Harris as Sally Bowles
* Laurence Harvey as Christopher Isherwood
* Shelley Winters as Natalia Landauer
* Ron Randell as Clive Mortimer
* Lea Seidl as Fräulein Schneider
* Anton Diffring as Fritz Wendel
* Jean Gargoetas Pierre

==Production== West End X certificate (no one under 16 admitted). The Boards initial report offered suggestions for how the play could be adapted to secure an A certificate (suitable for children if accompanied by an adult), including shifting the plays focus away from Sally Bowles, but recognized that such changes were unlikely because of how markedly they would depart from the original play. 

Director Henry Cornelius asked Isherwood to write the screenplay. He was forced to decline, as he was engaged working on the screenplay for Diane (film)|Diane, a biopic of Diane de Poitiers, for Metro-Goldwyn-Mayer in Hollywood. 

The first draft of John Colliers screenplay was submitted to the BBFC in October 1954. While the screenplay was being prepared, the Board sent four different examiners to see the play. Each agreed that the play as written was unsuitable for filming, although one held out hope that modifications could be made to allow for the A rating.  The BBFC demanded changes to the script, including insisting that Sally Bowles be left poor and unsuccessful at films end because of her sexual promiscuity in the Berlin flashbacks.  Negotiations between filmmakers and the Board continued through November. Finally on 29 November, a resolution was reached which left filmmakers prepared for the likelihood that the film would be certified X regardless. Aldgate and Robertson, p. 73 
 draft board to obtain a passport. 

==Ratings and certification== Room at Look Back Saturday Night and Sunday Morning, the BBFC found that the abortion subject matter prevented re-certification without re-editing. 

Upon its American release I Am a Camera was denied the Production Code Administration seal of approval. {{Cite news
  | title = Wildest Movie Binge
  | newspaper=LIFE magazine
  | pages = 57–8
  | date = 8 August 1955 }}  Joseph Breen of the PCA had reviewed a copy of the Van Druten play as early as May 1953 and deemed it to have multiple Code violations. His recommendations were sent to John Van Druten, who disregarded them.  The PCAs Geoffrey M. Shurlock, in denying PCA approval to the finished film, cited "gross sexual promiscuity on the part of the leading lady without the proper compensating moral values required by the Code".  This included the films treatment of the subject of abortion. Many theaters would not run the film without the seal.
 test case against the Production Code. However, the film did not rally the critical support it would have needed to defy the code in the way that the 1953 film The Moon Is Blue had. In February 1956, Schwartz wrote to Shurlock offering to include an additional scene in which Harvey as Isherwood condemned Sallys promiscuity but would not address the subject of abortion. The Production Code section on abortion was revised in December 1956 and Schwartz once again appealed to Shurlock. Shurlock responded later that month, re-affirming the denial on the basis of the light treatment of the subject matter. Following this denial Schwartz dropped his pursuit of the seal. 

On 25 August 1955, the National Legion of Decency condemned I Am a Camera and at least one theater pulled the film in response to attacks on the film by Catholic priests. 

==Critical response==
British critics were nearly uniform in their disappointment with I Am a Camera, with negative reviews appearing in the Evening News, the Daily Telegraph, the Financial Times, the Daily Mirror, the News Chronicle and Tribune. Each believed that Laurence Harvey had been miscast as Isherwood. For the most part they agreed that Harriss performance was a bright spot, although the Daily Sketch expressed a preference for Dorothy Tutin,  who had played Sally on stage in 1954.

Bosley Crowther of The New York Times gave I Am a Camera a bad review, finding it "meretricious, insensitive, superficial and just plain cheap". Crowther was particularly appalled by John Colliers script, blasting it for largely abandoning both the Van Druten and Isherwood source material. He also sharply criticized the abortion material, deeming it a "capstone of cheap contrivance and tasteless indelicacy". Julie Harris he labeled a "show-off" while Laurence Harvey is "an anxious straight man for her jokes", with all parties directed by Cornelius with no eye to any subtlety of character. {{Cite news
  | last = Crowther
  | first = Bosley
  | title = Screen: I Am a Camera
  | newspaper=The New York Times
  | date = 9 August 1955
  | url = http://movies.nytimes.com/movie/review?res=9E01E4D8103AE53BBC4153DFBE66838E649EDE&partner=Rotten%20Tomatoes
  | accessdate =18 July 2011}} 

In an August 1955 pictorial, Life (magazine)|LIFE magazine called the films party sequence "violently funny". LIFE praised Harriss acting while at the same time finding the film spends too much time on Harriss character. Still, LIFE felt confident in predicting the films success. 
 BAFTA Award for Best Foreign Actress. 

Isherwood recorded his distaste for the film in his diary, noting his attendance at a 22 June 1955 preview. He found the film "a truly shocking and disgraceful mess. I must admit that John Collier is largely to blame – for a sloppy, confused script. But everything is awful-except for Julie, who was misdirected."  In a letter to friend John Lehmann Isherwood called the film "disgusting ooh-la-la, near pornographic trash – a shameful exhibition". 

On the occasion of its video release in 1985, Lawrence Van Gleder for The New York Times found that this film, while suffering in comparison with the more lavish Cabaret (1972 film)|Cabaret, is still charming in its way, mostly because of Harriss performance as Sally. 

Phil Hall reviewed I Am a Camera for Film Threat in 2005. He questioned the casting of Harvey as Isherwood, saying that the role called for a light comedic touch that was never Harveys forte. Harveys underplaying of the part, he wrote, clashes with Harriss unrestrained stage-style performance of hers. Still, he found that the film is "an intriguing curio" that garners interest for its exploration of the anti-Semitism that gave rise to the Nazis and for its handling of "touchier aspects" of the original sources, including Isherwoods homosexuality Isherwood lived as more or less openly gay in Berlin but concealed his homosexuality in the source material; the film would state only that the character Isherwood was a "confirmed bachelor".  and Sallys abortion, which became a false pregnancy scare for the film. 

==Notes==
 
 

==References==
* Aldgate, Anthony and James Chrighton Robertson (2005). Censorship in Theatre and Cinema. Edinburgh University Press. ISBN 0-7486-1961-5.
* Fryer, Jonathan (1977). Isherwood: A Biography. Garden City, NY, Doubleday & Company. ISBN 0-385-12608-5.
* Harper, Sue and Vincent Porter (2003). British Cinema of the 1950s: The Decline of Deference. Oxford University Press US. ISBN 0-19-815934-X.
* Isherwood, Christopher, and Katherine Bucknell (ed.) (1996). Diaries Volume One: 1939—1960. Michael di Capua Books, a division of HarperCollins Publishers. ISBN 0-06-118000-9.
* Lehmann, John (1987). Christopher Isherwood: A Personal Memoir. New York, Henry Holt and Company. ISBN 0-8050-1029-7.
* Mayer, Geoff (2003). Guide to British Cinema. Greenwood Publishing Group. ISBN 0-313-30307-X.
* Slide, Anthony (1998).   Banned in the USA: British Films in the United States and Their Censorship, 1933–1960. I.B. Tauris. ISBN 1-86064-254-3.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 