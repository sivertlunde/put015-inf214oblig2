Romantic Island
{{Infobox film
| name           = Romantic Island
| image          = File:Romantic_Island_poster.jpg
| film name =  
 | rr             =  Romaentik Aillaendeu
 | mr             = Romaent‘ik Aillaendŭ }}
| director       = Kang Chul-woo
| producer       = Lee Jung-sub   Shin Hyung-chul   Lee Nam-ki   Kevin Kim
| writer         = Lee Jung-sub  Eugene
| music          = Tearliner 
| cinematography = Choi Yoon-man  
| editing        = Choi Jae-keun
| distributor    = SBSi   Mirovision
| released       =  
| runtime        = 107 minutes
| country        = South Korea
| language       = Korean   English   Filipino
| budget         = 
| gross          = 
}}
Romantic Island ( ) is a 2008 South Korean romantic comedy film set in Boracay, Philippines.

==Plot==
Six Koreans from Seoul travel separately to the Philippines: a middle-aged couple on their first trip overseas with the terminally ill husband plotting to kill himself while on holiday so his unsuspecting wife can claim an insurance payment; a convenience store clerk who meets a runaway pop star; and a happy-go-lucky office worker who gets hired as a tour guide by a rich businessman who came to pay his respects to his estranged dead father.     

==Cast==
*Lee Sun-kyun - Kang Jae-hyuk
*Lee Soo-kyung - Choi Soo-jin
*Lee Min-ki - Jung-hwan  Eugene - Yoo Ga-young 
*Lee Moon-sik - Joong-sik 
*Lee Il-hwa - Yoon-suk
*Lee Hyun-sook - Soo-jins mom
*Ki Eun-se - Yoo Hye-ra
*Song Min-ji - Kim Hyun-joo
*Ha Yun - Sandra
*Choi Dae-sung - manager
*Kim Jong-soo - mover
*Choi Jin-ho - interviewer

==References==
 

==External links==
* http://www.romantic2008.kr/
*  
*  


 
 
 
 
 
 


 
 