Kid (film)
{{Infobox film
| name           = Kid
| image          = Kid (film).jpg
| caption        = 
| director       = Fien Troch
| producer       = Antonino Lombardo
| writer         = Fien Troch
| starring       = 
| music          = Senjan Jansen
| cinematography = Frank van den Eeden
| editing        = Nico Leunen
| studio         = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Belgium Netherlands Germany
| language       = Dutch
| budget         = 
| gross          = 
}}
Kid is a 2012 drama film. It was written and directed by Fien Troch, produced by Antonino Lombardo, and starred Bent Simons, Gabriela Carrizo and Maarten Meeusen.

The film tells the story of Kid, a seven-year-old boy who lives with his mother and his older brother Billy on a farm outside a small town. Abandoned by their father, they have had to fend for themselves. Their finances are in ruins and the two boys have to move with their uncle and aunt.
 Best Flemish Film in Coproduction. 

==Notes==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 