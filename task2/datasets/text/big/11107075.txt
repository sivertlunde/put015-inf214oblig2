Bats (film)
 

 
{{Infobox film
| name           = Bats
| image          = Bats poster.jpg
| caption        = Theatrical release poster
| director       = Louis Morneau
| producer       = Bradley Jenkel Louise Rosner John Logan
| starring       = Lou Diamond Phillips Dina Meyer Bob Gunton
| music          = Graeme Revell
| cinematography = George Mooradian
| editing        = Glenn Garland
| distributor    = Destination Films
| released       =  
| runtime        = 91 minutes 92 minutes (extended cut)
| country        = United States
| language       = English
| budget         = $5.25 million
| gross          = $10,155,690 
}} science fiction horror Thriller thriller film, directed by Louis Morneau and produced by Bradley Jenkel and Louise Rosner. The film stars Lou Diamond Phillips, Dina Meyer, Bob Gunton and Leon Robinson|Leon. It was the first film released by Destination Films.

The plot concerns a hostile swarm of genetically mutated bats who terrorize a local Texas town and it is up to zoologist Sheila Casper, who teams up with town Sheriff Emmett Kimsey, to exterminate the creatures before they take more lives. The film was panned by several critics for its campiness, sub-par special effects, underdeveloped characters, and failing to be scary or creepy in any way. The films positive reviews have proclaimed that the film is so bad that it is good and should be viewed as an unintentional comedy rather than a horror film. As a result of these rare positive reviews, the film has developed somewhat of a cult following.

Despite all the negative acclaim, the film was a moderate box office success, recouping nearly half its budget.

==Plot==
After people start to die in the small Texas town of Gallup, the prime suspects are bats. The CDC calls in Dr. Sheila Casper (Dina Meyer) and her assistant, Jimmy Sands (Leon Robinson), who are zoologists who specialize in the study of bats, to investigate the situation. Dr. Alexander McCabe (Bob Gunton) is secretive about the situation, but admits that the bats were genetically modified by him to become more intelligent and also omnivorous so they wouldnt be in danger of extinction any longer. Sheila is disgusted by this but it seems that McCabe had the best intentions.

Aided by Sheriff Emmett Kimsey (Lou Diamond Phillips) and CDC specialist Dr Tobe Hodge (Carlos Jacott), Sheila and Jimmy begin to search for the nest. The first night they are attacked and manage to capture one and plant a tracking device on it. However as soon as they let it go, it is killed by the other bats, who knew of the feint. Emmett decides that Gallup needs to be evacuated, and the mayor is told to broadcast a warning to all residents to stay indoors and secure their houses. Unfortunately, no one listens. The bats invade the town and, within minutes, chaos ensues and several people are killed, including Dr. Hodge, who sacrificed himself to save Sheila.
 National Guard arrives and begins evacuating the town. They give Sheila and her bat hunters 48 hours before they destroy the town in hopes of killing the bats. Sheila sets up headquarters in the school. The military has promised to center the infrared cameras of a Chromo-B340 satellite on the area around Gallop to help locate the bat roost. Even when the roost is located, however, how are they going to annihilate every last bat? From the numbers they saw the other night, there are thousands of them. Bombing the roost will only scatter them, leading to the creation of other roosts and potentiating the problem. Poisoning them is not an option, as the most popular bat poison is only marginally effective against bats but highly lethal to humans. Sheila thinks the best solution is to put them to sleep by lowering the temperature in the cave. At 40 degrees, bats begin to hibernate. At 32 degrees, they freeze to death.

Jimmy arranges for an NGIC Industrial Coolant that uses freon, carbon dioxide, and oxygen to be dropped off as soon as the roost is located. When the spy satellite begins sending pictures of the bats, Emmett recognizes the area as that of the old Black Rock mine. Everything is set to begin at 0600 hours—except that the government has other ideas, which dont include waiting until dawn. During the night, they wire the mine with explosives set to trap the bats inside and freeze them. Unfortunately, the bats kill all the soldiers at the location and then attack the school. They use electrified fences and blowtorches to keep the creatures at bay, but it is revealed that McCabe, actually insane, has created the bats to be the ultimate predator, creating them to kill people. McCabe flees, only to be killed by his own creation after believing that they would listen to his wishes.

When Sheila, Emmett and Jimmy arrive at the mine the next morning, they are informed by the military that they werent able to turn on the coolant unit, so the plan now is to bomb and gas the mine starting in one hour. Emmett calls the military and tells them that they will be able to activate the unit but they are unable to get the air strike called off. Sheila decides that one hour is enough time for them to get the coolant unit started, so she and Emmett suit up and enter the mine. Jimmy stays outside to monitor their progress and to blow up the mine entrance should it become necessary. Although they find themselves up to their waists in bat guano, they are successful at starting the coolant. As they make for the exit, the bats are flying in hot pursuit. The moment they exit, Jimmy detonates the explosives, collapsing the mine entrance and trapping the bats inside, freezing them. All of a sudden, one final bat burrows out of the ground, having survived the freezing temperatures. It prepares to escape, only to be crushed by the car as the group drives away.

==Cast==
* Lou Diamond Phillips as Emmett Kimsey
* Dina Meyer as Sheila Casper
* Bob Gunton as Alexander McCabe Leon as Jimmy Sands
* Carlos Jacott as Tobe Hodge
* David McConnell as Deputy Wesley Munn
* Marcia Dangerfield as Mayor Amanda Branson
* Oscar Rowland as Dr. Swanbeck
* Tim Whitaker as Quint
* Juliana Johnson as Emma
* James Sie as Sergeant James
* Ned Bellamy as Major Reid
* George Gerdes as Chaswick

==Production== American Fork, Park City, despite the films plot being set in Texas. The bats in the film were a combination of computer-generated imagery (CGI), Animatronics, and actual bats which were brought from Indonesia.

==Release==
The film received its theatrical premiere in the U.S. on October 22, 1999. The film was not released theatrically in Norway, Japan, Hungary, or Finland. The film was released direct-to-video in Norway, Japan, and Hungary and as a TV movie in Finland.

===Home media===
The film was released on VHS and DVD on February 22, 2000. A Blu-ray version of the film has yet to be released. The film performed quite well in the home media market, grossing over $30 million in DVD and VHS sales (including theatrical and television sales), although it was considered a failure at the time of its release the film managed to recoup its budget during its first seven days of release.

An extended edition which was rated R was released on the same date (which is unusual considering most extended versions of a film are unrated) which extended certain scenes in the film by a few seconds and featured more graphic violence, blood, and gore than the theatrical cut.

==Reception==
===Box office===
In its opening weekend, Bats grossed about $4,717,902, nearly equaling its production budget. The film managed to surpass its budget with a domestic gross of $10,155,690.

===Critical reception===
Bats received mainly negative reviews from critics, where it currently holds a 17% rating on Rotten Tomatoes based on 42 reviews with the critical consensus simply stating "Neither scary nor creepy". Metacritic, which assigns a rating from 0 to 100, gave the film a 23, indicating generally unfavorable reviews. Despite the negative reviews the film is now considered  a cult film.

==Sequel== Sci Fi Channel in 2007.

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 