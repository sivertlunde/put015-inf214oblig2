Horses of God
 
{{Infobox film
| name           = Horses of God
| image          = 
| caption        = 
| director       = Nabil Ayouch
| producer       = Frantz Richard
| writer         = 
| starring       = Abdelhakim Rachid
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Morocco Belgium
| language       = Moroccan Arabic (dubbed in French)
| budget         = 
}}

Horses of God ( ,  , Transliteration|translit.&nbsp; Yakheel Allah) is 2012 Moroccan drama film directed by Nabil Ayouch, about the 2003 Casablanca bombings and based on the novel of the same name by Moroccan writer Mahi Binebine. The film competed in the Un Certain Regard section at the 2012 Cannes Film Festival.      
 Best Foreign Language Film at the 86th Academy Awards,    but it was not nominated. It received two nominations at the 4th Magritte Awards, winning Best Cinematography for Hichame Alaouié. 

==Cast==
* Abdelhakim Rachid as Yachine
* Abdelilah Rachid as Hamid
* Hamza Souidek as Nabil
* Ahmed El Idrissi El Amrani

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Moroccan submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 