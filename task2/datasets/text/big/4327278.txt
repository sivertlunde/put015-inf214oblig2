Dead of Winter
 
{{Infobox film
| name           = Dead of Winter
| image          = Dead of winter poster.jpg
| caption        = Theatrical release poster
| director       = Arthur Penn
| producer       = John Bloomgarden Marc Shmuger
| writer         = Marc Shmuger Mark Malone Jan Rubeš
| music          = Richard Einhorn
| cinematography = Jan Weincke
| editing        = Rick Shaine
| studio         = Metro-Goldwyn-Mayer MGM Entertainment Co.
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2,413,427 (USA)
| preceded_by    = 
| followed_by    = 
}}

Dead of Winter is a thriller film made in 1987. It was directed by Arthur Penn and is a loose remake of the 1945 film My Name Is Julia Ross. It stars Mary Steenburgen, who plays three roles.

== Plot ==
A woman drives to a train station on New Years Eve to retrieve a satchel full of cash. Later that night, she is strangled, and her left ring finger is removed.

In New York, struggling actress Katie McGovern (Mary Steenburgen) lives with her husband Rob Sweeney (William Russ) and her brother Roland (Mark Malone), who is visiting the couple. At an audition, Mr. Murray (Roddy McDowall) hires her immediately.
 Jan Rubeš), he graciously greets Katie from his wheelchair. He gives her a tour of the house, which features a number of trophies from his hunting days, including two massive stuffed polar bears. Katie asks to use the phone to call Rob but finds it has no dial tone. Dr. Lewis explains that the storm must have downed the lines and that Mr. Murray would drive her into town in the morning to make her call if necessary.

Dr. Lewis explains that Katie has been hired to replace Julie Rose, an actress who had a nervous breakdown during a film shoot. Katie is stunned to see photos of Julie, exclaiming, "I could be her sister!" Dr. Lewis says a test reel on videotape will be shot tomorrow, which the director will view when he arrives.

The following day, Mr. Murray tries to drive Katie into town to make her phone call, but his car will not start. Mr. Murray cuts and dyes Katies hair to match the photos of Julie. Asked how they met, Mr. Murray explains that Dr. Lewis was his psychiatrist. The shoot goes well. Katies lines are all about the attack seen at the beginning of the story.  

Mr. Murray sneaks out and reconnects the cars ignition. He drives to a house nearby where a woman, whose face is never shown, watches from her window. He slips the videotape into her mail slot, and she watches the video of Katie. After the test reel footage, Dr. Lewis appears on the video to tell the viewer that they need to meet. 
 Polaroids of Julies corpse. Horrified, she confronts Dr. Lewis, who explains that Julies breakdown ended with her suicide. In the parlor, Katie is startled by a pop in the fireplace, where she sees her drivers license burning. Unable to retrieve it, she rushes to her room and sees that all of her ID is missing from her wallet. Katie flees without a coat. The weather is so ferocious, she ends up crawling to the top of a hill where, to her horror,  Mr. Murray is waiting for her. 

Back at the house, Dr. Lewis pretends her imagination is running wild. Katie realizes that Mr. Murray has drugged her hot chocolate. In her room, she barricades the door with furniture before she passes out. As she sleeps, Mr. Murray enters her room from behind a full-length mirror. Katie wakes up in a fresh sleeping gown with a bandaged hand. She peels off the bandages to find that her left ring finger has been removed.

Her barricade undisturbed, Katie quickly finds the secret door and a staircase to the attic, which has a working phone. She calls Rob and explains that they are going to kill her. Rob asks where she is, but Katie can only remember vague landmarks about the drive upstate. Rob orders her to call the police, which she does. As she collapses, she sees the body of Julie Rose. Mr. Murray arrives and rips the handset out of the phone, taking her back downstairs to Dr. Lewis.

As Dr. Lewis redresses her finger, he explains that Julie was involved in a vicious family feud with her sister, Evelyn. As a radical therapy, Dr. Lewis had convinced her to blackmail her sister, theorizing that it would help her achieve a catharsis. He was pleased with Julies progress, but did not expect Evelyn to kill her, ordering the hit man to take her finger as proof.

During his explanation, the police arrive. Katie is confused from a sedative and Dr. Lewis claims that she is his patient, so the police to leave without much of an investigation. Meanwhile, Rob and Roland have begun to drive upstate, using the handful of clues they have to try to locate Katie.

Katie wakes to find Evelyn standing over her. Dr. Lewis offers her as proof that Julie is still alive, to continue the blackmail. Katie fakes an escape attempt, luring Dr. Lewis and Mr. Murray out of the house. She pleads with Evelyn to help her escape, but Evelyn is convinced she is really Julie and attacks her. Katie kills Evelyn and poses as her to try to escape.

Mr. Murray realizes the ruse, but Katie stabs him in the neck. Dr. Lewis is also not fooled by the disguise and lunges at Katie from his wheelchair. Using a fire poker as a crutch, he follows her upstairs and eventually into the attic, where Katie manages to kill him. Rob and Roland arrive with the police, having convinced them to revisit the house.

== Cast ==
* Mary Steenburgen as Julie Rose / Katie McGovern / Evelyn
* Roddy McDowall as Mr. Murray Jan Rubeš as Dr. Joseph Lewis
* William Russ as Rob Sweeney
* Ken Pogue as Officer Mullavy
* Wayne Robson as Officer Huntley
* Mark Malone as Roland McGovern
* Michael Copeman as Highway Patrolman
* Sam Malkin as Gas Jock
* Pamela Moller as Woman at audition
* Dwayne McLean as Killer
* Paul Welsh as New Years Eve reveler

== Production == Matthew -- began directing but soon ran into difficulties. Producer John Bloomgarden took over directing in the interim. Studio executive Alan Ladd, Jr. asked Penn -- who had brought the project to the studios attention -- to direct. Penn reluctantly agreed. 

The movie was filmed on location in Ontario, Canada.

== Reception ==

=== Critical response ===
Dead of Winter has an 82% freshness rating on Rotten Tomatoes.  In Janet Maslins review for The New York Times, she wrote, "When a director approaches Gothic horror with this much enthusiasm, the results are bound to be as merry as they are frightening. So audiences for Arthur Penns Dead of Winter are in for a hair-raising treat."  Roger Ebert concluded that, "The movie itself is finally just an exercise in silliness – great effort to little avail – but the actors have fun with it, the sets work and there are one or two moments with perfect surprises." 

Writing for The Washington Post, Paul Attanasio asserts that Steenburgen "manages with élan an assignment that has her playing three parts". He faulted the lengthy build up to the final confrontation, "An hours worth of exposition is a long wait, and if the payoff isnt quite worth it, it is fun. After nine yards of soggy oatmeal, youre reintroduced to the pleasures of an old-fashioned haunted house."  The staff review of the film in Variety (magazine)|Variety found Rubeš to be lacking as the villain, "Steenburgen and McDowall are the adversaries to follow, even though it would seem more likely that the wheel-chair bound doctor (Jan Rubes) should be the one to watch. Rubes is simply not sinister enough to be the mastermind behind this scheme." 

== References ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 