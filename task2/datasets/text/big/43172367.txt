Harichandra (1944 film)
 
{{Infobox film
| name = Harichandra
| image = 
| director = Naga Bushwanam 
| writer = T.C.Vadhivelu Nayakkar
| starring = P.U. Chinnappa P.Kannamba M. G. Ramachandran R. Balasubramaniam
| producer = Naga Bushwanam
| music = S.V.Vengatharaman
| distributor = Sri Raja Rajeshwari Film Company 
| released =  
| runtime = 
| country = India
| language = Tamil
| budget = 
}}

Harichandra ( ) is a Tamil language film starring P.U.Chinnappa, M. G. Ramachandran and M. R. Santhanalakshmi in the lead roles. The film was released in 1944.

==Cast==
* P. U. Chinnappa
* P. Kannamba
* M. G. Ramachandran
*  R. Balasubramaniam

==Crew==
* Producer: Naga Bushwanam
* Production Company: Sri Raja Rajeshwari Film Company 
* Director: Naga Bushwanam
* Music: S.V.Vengatharaman
* Lyrics: C.A.Lakshoumanadass
* Story:
* Screenplay:
* Dialogues: T.C.Vadhivelu Nayakkar
* Art Direction:
* Editing: N.K.Gopal
* Choreography: Meenatchi Sundharam Pilai
* Cinematography: Marcus Bartley & Mohammed Masthan 
* Stunt:
* Dance: Meenatchi Sundharam Pilai

 
 
 
 


 