Ladies' Day
{{Infobox film
| name           = Ladies Day
| image          = Ladies Day poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Leslie Goodwins
| producer       = Bert Gilroy
| screenplay     = Dane Lussier Charles E. Roberts 
| based on       =  
| starring       = Lupe Vélez Eddie Albert Patsy Kelly Max Baer Jerome Cowan
| music          = Roy Webb
| cinematography = Jack MacKenzie
| editing        = Harry Marker 
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Ladies Day is a 1943 American comedy film directed by Leslie Goodwins and written by Charles E. Roberts and Dane Lussier. The film stars Lupe Vélez, Eddie Albert, Patsy Kelly, Max Baer and Jerome Cowan. The film was released on April 9, 1943, by RKO Pictures.  {{cite web|url=http://www.nytimes.com/movie/review?res=9C05E2DD1F39E33BBC4E51DFB5668388659EDE|title=Movie Review -
  Ladies Day - At the Palace - NYTimes.com|publisher=|accessdate=17 September 2014}}  

==Plot==
 

== Cast ==
*Lupe Vélez	as Pepita Zorita
*Eddie Albert as Wacky Waters
*Patsy Kelly as Hazel Jones
*Max Baer as Hippo Jones
*Jerome Cowan as Updyke
*Iris Adrian as Kitty McClouen
*Joan Barclay as Joan Samuels
*Cliff Clark as Dan Hannigan
*Carmen Morales as Marianna DAngelo 
*George Cleveland as Doc
*Jack Briggs as Marty Samuels
*Russ Clark as Smokey Lee
*Nedrick Young as Tony DAngelo
*Eddie Dew as Spike McClouen Tom Kennedy as Dugan
*Ralph Sanford as First Umpire 

== References ==
 

== External links ==
*  

 
 
 
 
 
 