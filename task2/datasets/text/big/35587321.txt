Alice in Murderland
 
  American horror film written and directed by Dennis Devine. The film, produced in 2010, stars Malerie Grady, Marlene McCohen, Kelly Kula and Christopher Senger.
  critics for several reasons. However it has received a large fanbase on its Facebook page.

==Plot== hacked to death by a mask killer 20 years before. The girls set a rule that no cell phones and no boys will be allowed. 
 mayhem to the girls night as he starts murdering them one by one while the party is taking place. 

==Cast==
*Malerie Grady as Alice Lewis
*Marlene McCohen as Malory White
*Kelly Kula as Kat Glass
*Christopher Senger as Rene White
*Katie Loche OBrien as Tiffany
*Heath Butler as Donna
*Kim Argetsinger as Samantha Glass
*Gabrielle Abitol as Charlene Glass
*Elizabeth Lam Nguyen as Pima
*Jennifer Field as Dee
*Jennifer Kamstock as Aunt Lena
*Katie Hotchkiss as Ann Lewis
*Montre Bible as Andrew
*John Buco II as Matt

==Release==
Alice in Murderland was produced in 2010 by Tom Cat Films  and was released on February 8, 2011 by Brain Damage Films. 

==Reception== film critics titular character, a perception of characters as insincere and fake, numerous shots in which film crew and equipment are clearly visible, The identity of the Jabberwocky killer, The characters of Matt and Andrew as just being throw away characters and not even having 5 minutes of screentime each, a repetitive soundtrack and the misrepresentation of the film on the DVD cover.  

However, despite these criticisms the Alice in Murderland concept has developed a cult following.  

==References==
 

 
 
 