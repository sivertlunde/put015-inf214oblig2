Get Married 2
 
{{Infobox film
| name           = Get Married 2
| image          = Get Married 2.jpg
| director       = Hanung Bramantyo
| producer       = Chand Parwez
| writer         = Cassandra Massardi
| starring       = Nirina Zubir Nino Fernandez Aming Ringgo Agus Rahman Deddy Mahendra Desta
| music          = Slank
| cinematography =
| editing        =
| distributor    = Starvision
| released       =  
| runtime        =
| country        = Indonesia
| awards         = Indonesia
| budget         =
| gross          =
}} Get Married, it details the efforts of Mae and Rendy to have children.
 treatment by Cassandra Massardi. The film, in which most of the original cast returned, was released on 18 September and viewed by 1.2 million persons. Critical reception was mixed, although the film did receive an award at the 2010 Bandung Film Festival. Another sequel, Get Married 3, was released in 2011.

==Plot==
After four years of marriage, Mae (Nirina Zubir) and Rendy (Nino Fernandez) are childless. Meanwhile, her best friends Eman (Aming), Guntoro (Deddy Mahendra Desta) and Beni (Ringgo Agus Rahman) have already married and had children. This makes Mae feel pressured to quickly have a child and stresses her greatly. When Rendy forgets to come to a dinner celebrating their wedding anniversary, Mae is fed up and moves in with her parents, who try and convince her to leave Rendy.

Mae decides to try and work out her issues with Rendy, and the two begin working incessantly towards having a child. When that is unsuccessful, Mae decides that her tomboyish appearance is causing Rendy to be infertile. As such, she decides to surprise him by visiting his office after putting on make-up. However, she sees Rendy together with another woman, Vivi (Marissa Nasution) and walks out, saying that she wants a divorce. Rendy asks his mother (Ira Wibowo) to bring Mae back. However, both families fight, leaving Mae and Rendy separated.

Mae feels distraught without Rendy and often cries when she thinks nobody is watching. Eman, Guntoro, and Beni unsuccessfully try to cheer her up. Mae, trying to live independently, takes a job as a valet but faints at work; she discovers that she is pregnant. Rendy, not allowed to come near her, tasks the trio with watching over her. As Mae is very demanding and watching over her is very time consuming, Eman, Guntoro, and Beni are almost divorced by their wives. Mae furtively begins meeting with Rendy, as her parents still want her to divorce him. She discovers that he has not been having an affair, and they reunite. Mae later gives birth to triplets, as both families look on happily.

==Production== President to a pedicab driver. 
 Get Married, Indo actor Nino Fernandez replaced Richard Kevin as Rendy. 
 Generasi Biru (The Blue Generation; 2009), they agreed to work on Get Married 2 as they were not required to act. They recorded a total of twelve songs for the film, including two new ones. 

==Release and reception==
Get Married 2 was released on 18 September 2009,  four days after Bramantyos marriage with Mecca  and towards the Eid ul-Fitr holidays. It was one of four Indonesian films released before the holidays, the others being Ketika Cinta Bertasbih 2  (When Love Prays 2), Preman in Love and the animated Meraih Mimpi (Chasing Dreams).  Get Married 2 was a commercial success, being viewed by 1.2 million persons; it was the fourth best-selling Indonesian film of 2009, behind Ketika Cinta Bertasbih, Ketika Cinta Bertasbih 2, and Garuda di Dadaku (Garuda on My Chest).  The film won Best Poster at the 2010 Bandung Film Festival. 

The reviewer for the Bali Post found the film enjoyable and praised the casts performances.  The review in Suara Pembaruan found the film very similar to the first, and complained that the comedy trio of Rahman, Aming, and Desta&nbsp;– major characters in the first film&nbsp;– were underdeveloped in the sequel. 
 Pribumi actor, Fedi Nuril, played Rendy. 

 

==References==
;Footnotes
 

;Bibliography
 
* {{cite news
  | date=18 September 2009
  | title = Ayo Hamil
  |trans_title=Come On, Get Pregnant
  |language=Indonesian
  |work= Suara Pembaruan
  |page=16
  | ref =  
  }}
*{{cite news
 |url=http://www.balipost.co.id/mediadetail.php?module=detailberita&kid=23&id=20225
 |date=17 September 2009
 |work=Bali Post
 |title=Empat Film Nasional Memeriahkan Lebaran
 |trans_title=Four National Films Brighten the Holidays
 |language=Indonesian
 |accessdate=26 May 2012
 |archivedate=26 May 2012
 |archiveurl=http://www.webcitation.org/67wzdTsx1
 |ref= 
}}
* {{cite web
  | last1 = 
  | first1 = 
  | date=
  | title = Hanung Bramantyo
  |work=KapanLagi.com
  |language=Indonesian
  | url = http://selebriti.kapanlagi.com/indonesia/h/hanung_bramantyo/
  | accessdate =28 November 2011
  | ref =  
  |archiveurl=http://www.webcitation.org/63X0o57Zj
  |archivedate=28 November 2011
  }}
*{{cite news
 |url=http://www.thejakartapost.com/news/2009/12/27/cinema-comes-out-dark-ages.html
 |title=Cinema comes out of the dark ages
 |first=Triwik
 |last=Kurniasari
 |ref= 
 |date=27 December 2009
 |work=The Jakarta Post
 |accessdate= 26 May 2012
 |archiveurl=http://www.webcitation.org/67wyOW7Bq
 |archivedate=26 May 2012
}}
*{{cite news
 |url=http://www.thejakartapost.com/news/2010/08/22/zaskia-adya-mecca-juggling-family-and-movies.html
 |date=22 August 2010
 |last=Kurniasari
 |work=The Jakarta Post
 |first=Triwik
 |title=Zaskia Adya Mecca: Juggling family and movies
 |accessdate=15 January 2012
 |archivedate=15 January 2012
 |archiveurl=http://www.webcitation.org/64hOtnuSk
 |ref= 
}}
* {{cite news
  | last1 = Mariani
  | first1 = Evi
  | date=23 March 2008
  | title = Hanung Bramantyo: Hitting the right marks
  |work= The Jakarta Post
  | url = http://www.thejakartapost.com/news/2008/03/23/hanung-bramantyo-hitting-right-marks.html
  | accessdate =28 November 2011
  | ref =  
  |archiveurl=http://www.webcitation.org/63WexgCqZ
  |archivedate=28 November 2011
  }}
* {{cite news
  | last1 = Mubarak
  | first1 = Makbul
  | date=11 September 2011
  | title = A moving yet funny comedy
  |work= The Jakarta Post
  | url = http://www.thejakartapost.com/news/2011/09/11/a-moving-yet-funny-comedy.html
  | accessdate =21 April 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/6751uIhqU
  |archivedate=21 April 2012
  }}
*{{cite web
 |title=Penghargaan Get Married 2
 |trans_title=Awards for Get Married 2
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-g011-09-876128_get-married-2/award#.T8DdkFKVzMw
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=26 May 2012
 |archiveurl=http://www.webcitation.org/67wwPPIwk
 |archivedate=26 May 2012
 |ref= 
}}
* {{cite news
  | date=23 September 2009
  | title = Slank calls cut on movie making
  |work= The Jakarta Post
  | url = http://www.thejakartapost.com/news/2009/09/23/slank-calls-cut-movie-making.html
  | accessdate =26 May 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/67wxNhmJy
  |archivedate=26 May 2012
  }}
* {{cite news
  | last1 = Yazid
  | first1 = Nauval
  | date=27 September 2009
  | title = A variety of local flicks for the Lebaran holiday
  |work= The Jakarta Post
  | url = http://www.thejakartapost.com/news/2009/09/27/a-variety-local-flicks-lebaran-holiday.html
  | accessdate =26 May 2012
  | ref =  
  |archiveurl=http://www.webcitation.org/67wwzvqAE
  |archivedate=26 May 2012
  }}
 

==External links==
* 
 

 
 
 
 