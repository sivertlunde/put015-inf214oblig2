Tie a Yellow Ribbon
 
{{Infobox film
| name           = Tie a Yellow Ribbon
| image          = Tie a Yellow Ribbon film poster.jpg
| alt            =  
| caption        = Official Tie a Yellow Ribbon poster
| director       = Joy Dietrich
| producer       =  
| screenplay     = Joy Dietrich
| starring       = Kim Jiang Jane Kim Patrick Heusinger Ian Wen Theresa Ngo Gregory Waller Vincent Piazza
| cinematography = Lars Bonde
| editing        = Rasmus Høgdall Mølgaard  Stephen Maing
| Production company = Tie a Yellow Ribbon, LLC Independent Television Service (ITVS) Shouting Cow Productions Gigantic Pictures
| runtime        = 86 minutes 
| country        = United States
| language       = English
}}

Tie a Yellow Ribbon is a 2007 award winning film by Korean-American director and writer, Joy Dietrich.

{{Quote box|width=30%|quote="I wanted to make a film that gave nuanced portraits of young Asian-American women whose stories are seldom told in mainstream media. The dirty little secret is that Asian-American women have one of the highest rates of depression in the United States. While this film doesnt attempt to explain the reasons why, it does expose the isolating, alienating factors that make the young women feel the way they do -- the greatest among them the lack of acceptance and belonging. TIE A YELLOW RIBBON is ultimately about three young womens search for love and belonging." | source=Directors statement by Joy Dietrich. 
}}

It portrays the complex emotions for young adult Asian American women through its main character, Jenny, as a Korean adoptee in America struggling thorough life and difficult relationships. It was filmed and takes place in New York City.
 PBS television, ITVS series, in May 2008.

== Awards == CineVegas International Film Festival 2007, Special Jury Prize, Joy Dietrich, Best Director.
*Urbanworld Film Festival 2007, Grand Jury Prize, Best Narrative Feature, Joy Dietrich
*San Francisco Womens Film Festival 2008, Best Feature Indy
*Delray Beach Film Festival, May 2008, Directors Choice for Best Feature

== Festival showings ==
* Filmmor Womens Film Festival, Turkey, 2010 
* Asian Womens Film Festival, Germany, 2009
* Women Make Waves Film Festival, Taiwan, 2008
* Womens Film Festival, Turkey, 2008
* Women Make Waves Film and Video Festival, USA, 2008
*Silk Screen Asian American Film Festival, Pittsburgh, Pennsylvania, USA, 15 and 17 May 2008  
* UC Davis Asian American Film Festival, USA, 2008 
* Delray Beach Film Festival, USA, 2008
* International Womens Film Festival, South Korea, 2008
* Wisconsin Film Festival, USA, 2008
* Créteil International Womens Film Festival, France, 2008
* Mannheim-Heidelberg International Filmfestival, Germany, 2007
* Cal Poly Asian American Film Festival, USA, 2007 
* San Diego Asian Film Festival, USA, 2007
* DC Asian Pacific American Film Festival, USA, 2007
* Urbanworld Film Festival, USA, 2007
* CineVegas International Film Festival, USA, 2007
* VC Film Festival, USA, 6 May 2007
* San Francisco International Asian American Film Festival, USA, 18 March 2007

== Notes ==
 

== External links ==
* 
* 
* 
*  - Variety (magazine)|Variety magazine

 
 
 
 
 