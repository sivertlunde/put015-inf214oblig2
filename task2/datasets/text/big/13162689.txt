Deewana Mastana
{{Infobox film
| name           = Deewana 
| image          = 
| image_size     = 
| caption        = Movie Poster
| director       = David Dhawan
| producer       = Ketan Desai
| writer         = Anees Bazmee Prayag Raj
| narrator       =  Govinda Johnny Lever Ram Sethi
| music          = Laxmikant-Pyarelal
| cinematography = Ravi K. Chandran
| editing        = A. Muthu
| distributor    = M.K.D. Films Combine
| released       = 23 September 1997
| runtime        = 160 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1997 Bollywood Govinda in pivotal roles. Johnny Lever, Anupam Kher, Reema Lagoo, Shakti Kapoor, Saeed Jaffrey, Kader Khan have supporting roles, while Salman Khan makes a special appearance. This film borrows some elements from the 1991 Hollywood comedy, What About Bob?. Johnny Lever received the Filmfare award for Best Comedian for his performance on this film. The film was a success upon its release.The story thread was from the sathyan Anthikad Malayalam movie "Kinnaram" released in 1983.

The film is also noted for bringing together two superstars Salman Khan and Juhi Chawla together in a scene, the only time they were paired together. The film also reunites Anil Kapoor and Govinda after Awaargi (1990). All four actors also appeared in Salaam-E-Ishq in 2007.

==Synopsis==

Raja is a minor league crook who sells railway tickets on the black market at Amirpur Station. Tired of his job he looks for new ways to make a quick buck. One day, along with his friend Ghafoor (Johny Lever) and a police inspector (Avtar Gill), he robs Rs 2.5 million from the railway treasury. Later, Raja and Ghafoor bump off the inspector and run away with the loot to Bombay.

At Bombay airport, Raja sets eyes on psychiatrist Dr Neha (Juhi Chawla) and promptly falls in love with her. Raja and Ghafoor quickly find out where she lives. Ghafoor pretends to be a psychiatric patient while Raja takes on the name Raj Kumar and befriends her, telling her he has just returned from the U.S. However, Ghafoor cautions Raja not to hurry and be patient in matters of love.

The trouble begins when Bunnu (Govinda), the son of a wealthy businessman (Anupam Kher), is sent to Neha for treatment. He is crazy, and terrified of fire, heights, running, and water. Soon, he too falls for Neha and discovers he has a rival who is Raja. Suddenly, Neha has to leave for Ooty with her father (Saeed Jaffrey) to attend her uncles (Shakti Kapoor) wedding. She does not leave behind a forwarding address. Both Raja and Bunnu impersonate policemen and intimidate her secretary into revealing where she is. Dr Neha is thrilled to see them in Ooty but is drawn closer to the ill Bunnu rather than Raja.

Things take an ugly turn when Ghafoor tries to kill Bunnu, who escapes. Bunnu contacts contract killer Pappu Pager (Satish Kaushik) to bump off Raja. However, that plan too comes a cropper. Armed with a gold ring and garland, Neha calls Bunnu and Raja to court, ostensibly with the purpose of marriage. Both are surprised to see the other there.

Then they find out, Neha is getting married to someone else, Prem (Salman Khan) and Raja and Bunno end up being witnesses to her marriage. Raveena Tandon makes a special appearance in the end as both Raj and Bunnu walk off together.

==Cast==

*Juhi Chawla ...  Dr. Neha Sharma
*Anil Kapoor ...  Raj Kumar (Raja) Govinda ...  Bunnu
*Johnny Lever ...  Gaffoor 
*Anupam Kher ...  Birju (Bunnus dad) 
*Reema Lagoo ...  Bunnus mother 
*Saeed Jaffrey ...  Chander Nandkishore Kapoor Sharma (Neha Dad)
*Upasna Singh ...  Nehas Aunt 
*Himani Shivpuri ...  Rajs mom 
*Avtar Gill ...  Inspector. Mhatre 
*Shiva Rindani ...  Man teased Neha in the disco
*Guddi Maruti ...  Suzie 
*Shashi Kiran ...  Bus Driver 
*Pratibha Sinha ...  Tina 
*Satish Kaushik ...  Pappu Pager 
*Shakti Kapoor ...  Nehas Uncle 
*Kader Khan ...  Marriage Registrar

==Other Cast==

*David Dhawan ...  Himself 
*Salman Khan ...  Prem Kumar (uncredited) 
*Salim Khan ...  Man blasting the liquor stills (uncredited) 
*Ram P. Sethi ...  Man asking for discount for train tickets (uncredited) 
*Raveena Tandon ...  Sexy girl (Special appearance) (uncredited) 
*Babbanlal Yadav ...  House servant (uncredited)

==Soundtrack==

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! #!!Title !! Singer(s)

|-
| 1
| "Yeh Gaya Woh Gaya"
| Vinod Rathod, Alka Yagnik
|-
| 2
| "Hungama Ho Gaya" Poornima
|-
| 3
| "Tere Bina Dil Lagta Nahin"
| Udit Narayan, Vinod Rathod, Alka Yagnik
|-
| 4
| "Dil Chaahe Kisi Se"
| Alka Yagnik
|-
| 5
| "Head Ya Tail"
| Udit Narayan, Vinod Rathod, Kavita Krishnamoorthy
|-
| 6
| "O Mummy Mummy"
| Udit Narayan
|}

==Award== Filmfare Best Comedian - Johnny Lever

==External links==
* 

 

 
 
 
 
 