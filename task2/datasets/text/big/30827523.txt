The Glorious Resolve
{{Infobox film
| name           = The Glorious Resolve
| image          = GloriousResolve PakistanArmy.JPG
| alt            = 
| caption        = 
| director       = Sarosh Kayani Colonel Syed Mujtaba Tirmizi
| producer       = Lieutenant Colonel Irfan Aziz Brigadier Syed Azmat Ali
| writer         = Lieutenant Colonel Irfan Aziz
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Hamza Ali Abbasi Hassan Waqas Rana Bilal Lashari
| music          = 
| editing        = 
| studio         = Inter-Services Public Relations
| distributor    = 
| released       =  
| runtime        = 
| country        = Pakistan
| language       = Urdu
| budget         = 
| gross          =
}} International Film Festival.

Glorious Resolve won Jury special award in the recently held International film festival "Eserciti-e-Popoli" held at Bracciano, Italy. The festival saw the participation of NATO and 24 other countries with 60 films produced by renowned film makers which were evaluated by qualified and reputed jury. Glorious Resolve received the medal from the Chairman of the Italian Senate with the citation "A technically outstanding and emotionally powerful dramatization of the story of courageous soldier under fire in combat situation".

==Synopsis== Punjab infantryman|jawans were killed in the battle, and their deaths are reenacted in the film, as well as the actions of two "Ghazi (warrior)|Ghazis"—Sepoy Mashooq and Sepoy Muslim—who held their positions until reinforcements came. The film aims to depict the sacrifices and achievements of the Pakistan Army in the Global War on Terrorism as well as the Pakistan Armys "glorious resolve" to uproot the menace of terrorism from the "land of the pure". 

==Cast==
* Hamza Ali Abbasi
* Hassan Waqas Rana
* Bilal Lashari

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 


 
 