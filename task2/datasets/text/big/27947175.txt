Hordubalové
 
{{Infobox film
| name           = Hordubalové
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Martin Frič
| producer       = Antonín Vlach
| writer         = Karel Čapek Karel Hasler
| starring       = Jaroslav Vojta
| music          = 
| cinematography = Jaroslav Blazek
| editing        = Zdenek Gina Hasler
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}}
 Czech drama film directed by Martin Frič.    It was released in 1937. It was based on the novel Hordubal by Karel Čapek.

==Cast==
* Jaroslav Vojta - Juraj Hordubal
* Suzanne Marwille - Polana Hordubalová
* Palo Bielik - Michal Hordubal
* Mirko Eliás - Stepán Manya
* Vlasta Soucková - Maryna Hordubalová
* Eliska Kucharova - Hafie Hordubalová
* Frantisek Kovárík - Míso - Chief shepherd
* Filip Davidik - Filípek
* Gustav Hilmar - Gelnaj
* Vilém Pfeiffer - Karel Biegel
* Vladimír Majer - Gejza Fedeles
* Alois Dvorský - MUDr. Václav Klenka

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 