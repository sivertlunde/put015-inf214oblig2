Making Of (film)
{{Infobox film
| name           = Making of
| image          =
| caption        =
| director       = Nouri Bouzid
| producer       = C.T.V. Services
| writer         =
| starring       = Lotfi Abdelli, Fatima Saïdane, Afef Ben Mahmoud, Lotfi Dziri, Foued Litaiem
| distributor    =
| released       =  
| runtime        = 115 minutes
| country        = Tunisia
| language       =
| budget         =
| gross          =
| screenplay     = Nouri Bouzid
| cinematography = Michel Baudour
| sound          = Michel Ben Saïd, Hechmi Joulak
| editing        = Karim Hamouda
| music          = Néjib Charradi
}} 2006 film.

==Synopsis==
After failing sentimentally, with his family and at school, Bahta, a 25 years old breakdancer, feels down and, due to the Iraq war, reconsiders his clandestine escape. A rebel and disobedient by nature, the leader of a little breakdancer band, accomplishes many fearless deeds provoking the police’s anger. Wanted, he falls in with fundamentalists. The brainwashing process will not take place without mishaps.

==Awards==
* Cartago 2006
* FESPACO 2007
* Tetuán 2007
* TRIBECA 2007
* Taormina 2007
* Festival de New Delhi
* Festival de Orán
* FCAT 2008

==References==
 

 
 


 