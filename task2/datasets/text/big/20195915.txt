But Not for Me (film)
{{Infobox film
| name           = But Not for Me
| image          = ButNotforMe.jpg
| caption        = Theatrical release poster
| writer         = John Michael Hayes Samson Raphaelson (play)
| starring       = Clark Gable Carroll Baker Lilli Palmer Lee J. Cobb
| director       = Walter Lang
| producer       = William Perlberg George Seaton
| music          =Leith Stevens
| cinematography =
| editing        = studio = Pearlsea
| distributor    = Paramount Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| gross          = $2.5 million (est. US/ Canada rentals) 
}}
But Not for Me is a 1959 Paramount Pictures comedy film starring Clark Gable and Carroll Baker.  It is based on the play Accent on Youth  written by Samson Raphaelson.

==Plot==
Russ Ward is a Broadway producer with a 30-year record of success who has been out of town. On returning to New York, everybody wants a piece of him: ex-wife Kathryn Ward, hard-drinking playwright Jeremiah "Mac" MacDonald, magazine reporter Roy Morton, business manager Miles Atwood and lawyer Charles Montgomery, one after another.

The main topic of discussion is "Give Me Your Hand," the new play Russ is producing. The reporter hears its in trouble, but Russ says thats untrue. It will be ready for its Boston tryout right on schedule, he vows.

Kathryn keeps reminding him of his age, which Russ likes to lie about. Russ tells loyal young secretary and student actress Ellie Brown it is likely time to retire because the new show is a mess. He and writer Mac have a story about a middle-aged man romancing a 22-year-old woman and just cant seem to make it work.

Ellie is in love with Russ, so much so she proposes marriage to him. That gives him an idea. What if the play had the young woman pursuing the man? That way he wouldnt seem such a lecher. A delighted Mac rewrites it and everyone involved works on it at the Long Island mansion where the former actress Kathryn lives, partly thanks to her alimony from Russ.

A rich backer named Bacos wants in, but Atwood says his money isnt needed because an anonymous angel is financing the whole show. Ellie reads the womans part and strikes everybody as perfect for it. Gordon Reynolds, an up-and-coming young actor in Ellies acting class, gets the male lead and promptly falls for Ellie, but shes being led on by Russ, who doesnt discourage her love for him.

The shows so-so in Boston and a few of them panic, but Russ insists itll be a hit on Broadway and, sure enough, hes right. Now he needs to let down Ellie gently, and next thing he knows, she and Gordon have gotten married. Ellie returns exasperated because Gordon wants to give up theater and move to Montana. She strips and leaps into Russs bed so Gordon can catch her there and demand an annulment.

Everybody gets every misunderstanding sorted out. The newlyweds decide to compromise, and Russ, who finally has figured out that Kathryn was the anonymous angel who financed the show, is ready to give their relationship a second act.

==Cast==
*Clark Gable ... Russell "Russ" Ward
*Carroll Baker ... Ellie Brown / Borden
*Lilli Palmer ... Kathryn Ward
*Lee J. Cobb ... Jeremiah MacDonald
*Barry Coe ... Gordon Reynolds
*Thomas Gomez ... Demetrios Bacos Charles Lane ... Atwood
*Wendell Holmes ... Montgomery
*Tom Duggan ... Roy Morton

==Previous versions== 1935 movie Accent on Youth starred Herbert Marshall and Sylvia Sydney.  The 1950 film version was a musical entitled Mr. Music, starring Bing Crosby and Nancy Olson

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 