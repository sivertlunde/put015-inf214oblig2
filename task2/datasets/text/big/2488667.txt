Dear Wendy
{{Infobox film
| name        = Dear Wendy
| image       = Dear Wendy film.jpg
| director    = Thomas Vinterberg
| producer    = Sisse Graum Olsen
| writer      = Lars von Trier
| starring    = Jamie Bell Bill Pullman Michael Angarano Alison Pill
| distributor = Wellspring Media (US)
| released    =  
| runtime     = 105 minutes
| language    = English
| music       = Benjamin Wallfisch DKK 50,000,000 (est.)
}} Mark Webber and Alison Pill.

It is a co-production between Denmark, Germany, France, and the United Kingdom. The script was written by Lars von Trier.

The film performed poorly at the box office and received very poor reviews, frequently being compared unfavourably to von Triers award-winning Dogville, released the previous year.  Despite the poor critical response, Vinterberg won the Silver St. George for Best Director at the 27th Moscow International Film Festival.   

==Plot==
  The Dandies. Their club is assembled from the young misfits in a fictional small American mining town, Electric Park. It is started after the main character, Dick Dandelion (Jamie Bell), buys what he thinks is a toy gun as a gift. His co-worker tells him the gun is real, and the two start shooting and studying in their spare time. They later recruit other outcasts, young men (and one young woman) who do not, or cannot, work in the mine, including one boy in leg braces and his younger brother Freddie.

The film is framed by Dicks voiceover, resembling a love letter to his gun Wendy. He is the leader of the group (each member and their gun gets a vote), and tells them that their group is a "social experiment" and will reveal their true nature.

The script shares characteristics with earlier von Trier scripts, depicting violence and race relations in the United States in a highly stylized, exaggerated manner and mixing realistic and fantastic elements.

Though more substantial than the bare sound stage of Dogville, Electric Park is reminiscent of a western set on the back lot of a movie studio. The Dandies spend most of their time on this one block or in an abandoned mining shaft that they decorate and call the Temple.

The Dandies have several quirks and idiosyncratic rules. A Dandy may never brandish his weapon in public, but instead gains self-confidence simply knowing he is carrying a concealed weapon. As a badge of membership, they cultivate a Brideshead Stutter (a reference to the character Anthony Blanche in Brideshead Revisited, who also adopts a deliberate stammer). They refuse to say the word killing and instead refer to it as loving. They live up to their name, dandy|Dandies, by dressing in colorful, outdated clothing, including vests, long jackets and hats. Though they regularly shoot targets (bulls eyes are oddly common), they spend just as much time playing gun-related games, watching instructional videos and studying diagrams. They use their own personal guns, all antiques with names and back stories, more as props than weapons. Even when they do load and shoot their weapons, they favor style over function (for example, Dick decides to shoot from the hip, Susan uses two guns and works on indirect hits using ricocheted bullets).

Most of the characters in the film are white, save for two: Clarabelle, Dicks loving childhood nanny, and her grandson Sebastian. Sebastian shows up at Dicks house one day with the town sheriff (Bill Pullman). He has been put on probation for a weapons-related crime (he says he "blew a guy away") and has to regularly check in with Dick, whom the sheriff judges to be a good role model. The irony being that Dick and the Dandies spend all of their time using guns. Dick allows Sebastian to break probation and asks him to join the Dandies (Dick sees it as a bit of a Pygmalion (play)|Pygmalion), but only if he does everything on their terms.

One day, Sebastian gives them a suspicious box full of guns, and soon breaks a club rule by firing another members gun. Dick complains that Sebastian is "ruining it for everyone." The groups sole female member, Susan (Alison Pill), takes a shine to Sebastian and this threatens Dick. Freddie suggests that she likes "big schlongs". Like Dick, she lost a parent early on in the film though it doesnt seem to disturb her. Most of the film plays out like a childs game of cops and robbers or cowboys and Indians, and most of the non-Dandies portrayed are police officers and faceless miners.

Sebastians natural manner is in stark contrast to the affectations of the Dandies, especially the awkward poetry of Dicks voiceover. He speaks like a regular teenager and seems out of place in the one-block world of Electric Park. He points out their oddities, including their strange clothing. He is an outsider among outsiders, but still enlisted in the group.

Sebastian tells Dick that he and his friends carry guns because theyre scared, that everyone is scared. He tells them that his grandmother, Dicks former nanny, is too scared of "the gangs" to even leave her home (these vague, mysterious "gangs" had already been mentioned by Dicks boss at Salomons grocery store, who was terrified of them to the point of a nervous breakdown). Like a good Boy Scout, Dick devises a sort of war plan for assisting Clarabelle on her yearly visit to her cousins. He believes it is the "decent American" thing to do.

The Dandies accompany Clarabelle on her walk, but she becomes panicked when they encounter a deputy sheriff. There is a scuffle, he tries to help out and the old lady ends up shooting the deputy.

The sheriff asks the Dandies to hand over Clarabelle, even telling them they can keep their guns if they do so. He tells the boys that they are what the country is made of. The Dandies notice his automatic gun, which Dick calls "treacherous," and sense that they are being set up just as several other police officers appear. The Dandies flee to the Temple to hide.

Now outlaws, the Dandies decide to take Clarabelle to her cousins once and for all. Their decision is based more on principle than practicality, and it is clear that they are willing to martyr themselves. They treat it as a suicide mission, cutting themselves ceremonially and donning their fanciest clothes. Sebastian discovers Dicks now-finished letter to Wendy, which ends with the coded threat: "And now, its the time of the season for loving" ("loving" is Dandy code for killing).

They head outside one by one, armed, to face the team of shotgun-toting police officers assembled by a legion of squad cars. The first to go, Huey (Chris Owen), tells them "Were not interested in shooting anybody, so dont make us." The Marshall arrives and tells Huey to "Drop the pathetic gun right this minute." He is promptly shot by Huey, who smiles and announces "Officer d-d-down Im afraid!" before hobbling into gunfire. Huey discovers that he can walk fine without his crutches just as hes gunned down. Meanwhile, a bullet ricochets and hits Clarabelle in the leg as the Dandies continue attempting to escort her to her cousins house (like all the films location, it is located within a one-block radius).

Dick realizes that there is a sniper in their midst and sets on shooting the offender down. Susan is the next to shoot, using both of her guns and her carefully honed ricochet method. All the while, white lines and numbers on the screen graphically depicting the trajectories of the Dandies shots. Susan is shot in the head. Stevie and his gun Badsteel come to her defense and he is shot in the heart.

Sebastian asks Dick "What happened?" and a series of morgue photographs flash across the screen. Only Sebastian, Dick and Freddie remain. The three attempt to drag Clarabelle to safety. Freddie is the next to go. He has tied a cord around his testicles, a tactic with roots in Native American history that he championed earlier on, and grabs his crotch before getting up and firing. He is quickly shot down, rises, then is shot several more times.

Clarabelle stirs and Dick is hit as he comes to her aid, though he manages to get her all the way to her cousins house. While he is inside, police officers are scanning the windows and lining him up in their guns sights.

Sebastian remains outside, unharmed and hidden by the door of a police car. He sees Dicks gun, the one he called Wendy, lying in the street and recalls a line of Dicks letter: "Dear Wendy, I always dreamed that if someone were to make that final exit wound in me, it should be you. My saviour." He grabs the gun and then runs inside the home of his grandmothers cousin. He goes upstairs and shoots Dick in the back, mouthing "Dick,  ?"

Dick resembles a pilgrim as he turns around in his buckle hat. His life, at least that which was contained by the film, flashes before his eyes. He examines the exit wound and whispers "Wendy". The police on the roof across the street shoot up the windows and, mostly likely, Sebastian.

All the while, the "Battle Hymn of the Republic" plays in the background.

The film features many tracks by the 1960s pop-rock band The Zombies, including "Shes Not There", "Time of the Season". Dicks final words to Wendy in his letter, "its the time of the season for loving" is a quote from the latter song.

==Production==
The film was shot on a custom-built studio lot in Copenhagen, but represents a small mining town in West Virginia.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 