The Great Silence
 

{{Infobox film| name = The Great Silence
  | image = Great_silence_dvdcover.jpg
  | caption = DVD cover
  | director = Sergio Corbucci
  | producer = Alberto Marras
  | writer = Mario Amendola Bruno Corbucci Sergio Corbucci Vittoriano Petrilli Frank Wolff Luigi Pistilli Vonetta McGee
  | music = Ennio Morricone
  | cinematography = Silvano Ippoliti
  | editing = Amedeo Salfa
  | studio = Adelphia Compagnia Cinematografica Les Films Corona 20th Century Fox Italia
  | released =  
  | runtime = 105 minutes Italian
  | country = Italy France 
  | budget =
  }}
The Great Silence (Il grande silenzio, 1968), or The Big Silence, is an Italian revisionist western. 

The movie features a score by Ennio Morricone and stars Jean-Louis Trintignant as Silence, a mute gunfighter with a grudge against bounty hunters, assisting a group of outlawed Mormons and a woman trying to avenge her husband (a murdered outlaw). They are set against a group of ruthless bounty hunters, led by Loco (Klaus Kinski).

It is regarded as one of the best westerns ever made, and is acknowledged as Corbuccis masterpiece. Unlike most conventional spaghetti Westerns, The Great Silence takes place in the snow-filled landscapes of Utah during the Great Blizzard of 1899. 

It is often considered to be an anti-western.

==Plot== bounty rewarded on them. While people are suffering, the village becomes a paradise for bounty hunters, who can hardly be opposed by the poor, who are labelled as outlaws.

When Paulines husband falls prey to the unscrupulous bounty hunter Loco (Klaus Kinski), she hires a mute gunfighter, Silence (Jean-Louis Trintignant), to kill Loco. Since Silence as a child had to watch his parents being killed by bounty hunters, he tramps through the country chasing those who are killing people for money under the cloak of the law. In order to not violate the law and be added to the blacklist of the bounty hunters, he provokes them to pull out their weapon first. Then he has a reason to act in "self-defense" and shoot them.

But Loco does not let himself be provoked. Not until after he lures the new sheriff – who had been given the impossible task by the governor to re-establish order in the region and to grant amnesty to those starving in the mountains – to his death, does Loco face up to the final fight with Silence.

===Ending===
The film is famous for its bleak ending, a bloody scene in which the sympathetic characters are gunned down by the greedy bounty hunters, "all according to the law," as Loco comments. The director was forced to shoot an alternate ending for the North African and Asian markets.
 Frank Wolff returns from the "dead" (after having been trapped in a frozen lake by Loco) to save the day. 

==Cast==
*Jean-Louis Trintignant as Silence Italian version)
*Luigi Pistilli as Pollicutt Frank Wolff as Sheriff
*Vonetta McGee as Pauline
*Mario Brega as Martin
*Carlo DAngelo as Governor
*Marisa Merlini as Regina

==Background==
Jean-Louis Trintignant only agreed to play in a spaghetti western under the condition that he did not have to learn any lines for the role. Thats why the main character conveniently became a mute in the story. 

==Production== Italian Dolomites, San Cassiano in Badia (South Tyrol). It was also shot at Bracciano Lake, near Manziana in Lazio and the Elios town set in Rome was used for several of the Snow Hill scenes (including two nights sequences and the build-up to the final duel).

The scenes were shot at night so that the fake "snow" looked more convincing; shaving foam was used to give the street a snowbound look.  For the daylight scenes, the Elios set was swathed in fog, to disguise the fact that the surrounding countryside had no snow.

==Notes==

  and Michael Hanekes Amour (2012 film)|Amour.

Silences distinctive rapid-firing pistol is a Mauser C96, which started being manufactured in 1896. That Mauser pistol reappeared in Clint Eastwoods Joe Kidd (1972), while the snowbound setting was used in Pale Rider (1985) and briefly in Unforgiven (1992); in the early Seventies there was even a rumor that Eastwood was going to remake The Great Silence.

The only words Silence utters are as a boy, played in flashback by child actor Loris Loddi (from The Hills Run Red, 1966).  As his mother is shot, he cries out, "Mamma!  Mamma!", though the English dubbed voice is reused from the final scene of Corbuccis earlier film Johnny Oro (1966).

==Impact== Finnish Progressive Yesterdays wrote a 20-minute long epic called Suite Pauline based on the main characters story (this song is also featured on the Spaghetti Epic 3 CD). Anima Morte also recorded a version of the main theme for the Cani Arrabbiati - Opening themes tribute compilation.

The music by Ennio Morricone was later sampled by Thievery Corporation. The grindcore band Cripple Bastards released an album with the same title.

The film has been cited as the main inspiration for upcoming Quentin Tarantino film The Hateful Eight.

==Bibliography==
* Parts of this article were translated from the  , especially from  .
*  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 