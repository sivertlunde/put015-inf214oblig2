For Better, for Worse (1954 film)
 
 
{{Infobox film
| name           = For Better, for Worse 
| image          = "For_Better_for_Worse"_(1954).jpg
| image_size     = 
| caption        = British theatrical poster
| director       = J. Lee Thompson
| producer       = Kenneth Harper
| writer         = J. Lee Thompson Peter Myers
| based on       = play by Arthur Watkyn 
| narrator       = 
| starring       = Dirk Bogarde
| music          = Angela Morley (as Wally Stott) Guy Green Peter Taylor
| studio = Kenwood Productions
| distributor    =  Associated British-Pathe
| released       =  
| runtime        = 85 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = £206,736 (UK) 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
For Better, for Worse is a 1954 British comedy film directed by J. Lee Thompson. It was based on Arthur Watkyns play which had run for over 500 performances in the West End starring Leslie Phillips and Geraldine McEwan.  

It was also known as Cocktails in the Kitchen.

==Plot==
A young couple (Dirk Bogarde and Susan Stephen) decide to get married.

==Cast==
* Dirk Bogarde as Tony Howard 
* Susan Stephen as Anne Purves 
* Cecil Parker as Annes Father 
* Eileen Herlie as Annes Mother 
* Athene Seyler as Miss Mainbrace 
* Dennis Price as Debenham 
* Pia Terri as Mrs. Debenham  James Hayter as The Plumber 
* Thora Hird as Mrs. Doyle  George Woodbridge as Alf 
* Charles Victor as Fred 
* Sid James as The Foreman  Peter Jones as Car Salesman 
* Edwin Styles as Annes Boss

==Critical reception==
Sky Movies noted "Arthur Watkyns famous stage success has proved successful material for drama societies up and down the land - but still comes up like new in this bright little film version...Warm, human and charmingly funny domestic comedy, dressed up as fresh as paint by the colour camerawork."  

==References==
 

==External links==
* 

 

 
 
 
 