Marysia i Napoleon
{{Infobox film
| name           =Marysia i Napoleon
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Leonard Buczkowski
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = Wojciech Kilar
| cinematography = Wiesław Zdort
| editing        = 
| studio         = Studio Filmowe Kadr
| distributor    = 
| released       =1966
| runtime        = 
| country        = Poland
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Polish historical film. It was released in 1966.  The film is set in two time periods: modern and historical. 

==Plot==
A young French historian, Napoleon Beranger, comes to Warsaw on a foreign scholarship. While driving his car along a detour he runs into an old manor in Walewice village. On the wall of the manor he finds portraits of Napoleon Bonaparte and his great love Maria Walewska, a Polish aristocrat who used her charms to convince the emperor to stand up for her country. Beranger meets there by accident a beautiful woman, a student of art history by the same name of Maria Walewska (Marysia). The pair of modern heroes both notice the striking resemblance of each other to the historical figures, and as if by magic, move back into the Napoleonic period, where they play the roles of Napoleon and his Polish consort. They fall in love. 

==Main characters==
*Beata Tyszkiewicz - Maria Walewska/Marysia
*Gustaw Holoubek - Napoleon Bonaparte/Napoleon Beranger
*Juliusz Łuszczewski - Chamberlain Anastazy Walewski, uncle
*Halina Kossobudzka - Princess Jabłonowska, Walewski sister
*Ewa Krasnodębska - Anetka Potocka
*Ignacy Machowski- Geraud Duroc
*Kazimierz Rudzki - Charles-Maurice de Talleyrand-Périgord
*Anna Ciepielewska - maid to Mrs Walewska Walewskiej / Marta
*Bogumił Kobiela - valet Michael
*Wieńczysław Gliński - Paweł Łączyński, Mrs. Walewska brother

==References==
 

==External links==
*  

 
 
 
 


 