IOWA
{{Infobox film
| name           = IOWA
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Matt Farnsworth Diane Foster
| writer         = Matt Farnsworth Diane Foster John Savage
| music          = Elia Cmiral
| cinematography = John Houghton Andrew Parke Robert Brown
| studio         = Full Fathom 5
| distributor    = Koch Vision (U.S.)
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
IOWA is an independent neo-noir film directed, written and starring Matt Farnsworth. The film follows two young Iowan lovers who decide to cook their own methamphetamine.  The film was met with highly negative reviews.

==Plot==
After his father dies, Esper Harte learns that he may collect on his fathers insurance.  However, his mother and a crooked cop want to get rid of him so that they can take the money.
Desperate to escape their problems, Esper and his girlfriend, Donna Huffman, decide to cook their own methamphetamine.  

==Cast==
* Matt Farnsworth as Esper Harte Diane Foster as Donna Huffman
* Michael T. Weiss as Larry Clarkson
* Rosanna Arquette as Effie Harte John Savage as Irv Huffman

==Release==
IOWA premiered at the Tribeca Film Festival on April 22, 2005.  

==Reception==
The film received negative reviews.  Rotten Tomatoes reports 15% of surveyed critics liked it, with a 3.9/10 rating average out of 13 reviews.   Metacritic gave it a score of 35/100, indicating "generally unfavorable reviews".  

Slant Magazine called the film "stupendously slipshod" and "meandering, amateurish sleaze",  while The New York Times criticized the lack of subtlety.  In a more positive review, The Village Voice praised the films energy and acting.   Monsters and Critics said "This story and the characters in it are as real as the day is long and faithful to all that is good and bad about growing up with ever shrinking horizons." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 