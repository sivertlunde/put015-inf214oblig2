Runaway Train (film)
 
{{Infobox film
| name = Runaway Train
| image = Runaway trainposter.jpg
| alt = 
| caption = Promotional poster
| director = Andrei Konchalovsky
| producer = Menahem Golan Yoram Globus
| screenplay = Djordje Milicevic Paul Zindel Edward Bunker
| story = Akira Kurosawa
| starring = {{Plainlist|
* Jon Voight
* Eric Roberts
* Rebecca De Mornay
* Kyle T. Heffner John P. Ryan
* T. K. Carter Kenneth McMillan
}} Trevor Jones
| cinematography = Alan Hume
| editing = Henry Richardson
| studio = Northbrook Films Golan-Globus Productions The Cannon Group Inc.
| released =  
| runtime = 110 minutes  
| country = United States
| language = English
| budget = $9 million Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p189 
| gross = $8 million (US) 
}} thriller film John P. Tommy "Tiny" Lister, who both proceeded to successful careers as "tough guy" character actors.

The films story concerns two escaped convicts and a female railroad worker who are stuck on a runaway train as it barrels through snowy desolate Alaska. Voight and Roberts were both nominated for Academy Awards for their respective roles.

==Plot==
  warden to recapture them. Oscar "Manny" Manheim is a ruthless bank robber and hero to the convicts of Stonehaven Maximum Security Prison. After two previous escape attempts the doors to Mannys cell have been welded shut for three years.  A court order compels Mannys nemesis, the vindictive and sadistic Associate Warden Ranken, to release him back into the general prison population. Manny intends to break out a third time with his older brother Jonah Manheim, but is forced to set his escape plan into action in the middle of winter. Ranken employs a serial killer to give Manny the incentive, by stabbing him grievously through his left hand. Jonah fatally stabs the serial killer to death in retaliation, and in turn is severely beaten by the prison guard, leaving him in a high-security hospital wing.

Manny wants to wait for Jonah, but his older brother affirms hes not fit to join him, and warns him not to wind up in solitary confinement again as itll crush the convicts spirits inside. He joins Buck McGeehy, another convict (convicted of statutory rape), who due to his position in the prisons laundry room is recruited to smuggle him out in a trolley. Buck idolizes Manny. Naive and unintelligent, Buck decides to escape with Manny, who does not care for company. The prison staffs discovery of the convicts break out sets a riot in motion. After a freezing cross-country hike (involving a 300&nbsp;ft drop into a river and subsequent swim) the two hop on board a train consisting of four locomotives at a remote Alaskan rail yard.
 engineer suffers a heart attack. In attempting to stop the train and get off, Al does not set the throttle to Idle, instead engaging the brakes, before collapsing off the still-moving train. This overrides the engines automatic train stop. Consequently, although the brakes apply, the locomotives overpower them, and the brake shoes burn off, making it impossible to stop the train. Neither of the two convicts is aware of their situation, though Manny has suspicions the engines havent hitched up to any freight cars. Buck dismisses his concerns and is convinced of their freedom to which the bank-robber scoffs. Manny becomes increasingly irate with Buck, who he begins to regard as a clown with a cocky attitude, and advises him to get the kind of job an ex-convict can get, like cleaning toilets. Buck angrily asks Manny if he can do something that demeaning, to which Manny confesses I wish I could, leaving Buck speechless.
 railroad trestle ahead, Barstows superior Eddie McDonald arrives to order him to derail it, believing that no one alive is on board.

At this point the signal maintainer hears the trains Train horn|horn, as do Manny and Buck relieved into thinking theres still an engineer on board, not knowing it is the second units horn sounding. Realizing that someone is indeed alive on the train, Barstow orders a reversal of the switch. The speeding train continues on-wards towards the aged Seneca trestle, where emergency workers are gathering in expectation of a disaster. Ranken concludes that his two escaped convicts are escaping by rail and makes his way to the dispatchers office. Meanwhile, the two fugitives in the rear locomotive are alarmed when, a locomotive hostler named Sara, the only railway worker left on the train has clambered back there. Sara eventually manages to convince the men of their immediate crisis, explaining she had been asleep in the second unit and climbed back to the fourth engine in the belief she will be safer towards the rear of the train in another possible collision. Buck is skeptical of her, but Sara opens the window and shows them how dangerously fast the train is moving, confirming its out of control.
 MU cables connected to the two rear locomotives, shutting them down and slowing the train enough for it to cross the Seneca trestle, despite going much faster than the bridges speed rating. They made their way to the second engine, but both convicts are unable to pry open the jammed door.

The dispatchers divert the runaway onto a branch after determining it is only five minutes away from a head-on collision with a passenger train. This is only a brief respite, as further ahead the branch negotiates a tight curve adjacent to a chemical plant. Even at its reduced speed, the runaway is likely to derail on this curve and trigger a major chemical spill. His hand forced, Barstow agrees that they must switch the runaway onto a stub-ended siding and crash it, thus condemning the three people on the train to almost certain death, rather than risk a catastrophic chemical explosion. Warden Ranken, tired of waiting, confronts Barstow, who tells him to get lost. Ranken follows Barstow into the mens room and forces Barstows head into a toilet, thus winning Barstows cooperation in locating the train so Ranken can intercept it by helicopter.

Manny shows an increasingly violent streak, repeatedly asserting his dominance over Buck. Buck is desperate to please his partner in crime. Manny tries to force Buck to attempt a suicidal scramble around the outside of the second engines frozen nose (Buck already having tried once and failed). Saras intervention on Bucks behalf forces an armed face-off between the two convicts who threaten to kill one another. Manny ceases upon seeing Bucks disgust and sadness at his savageness. Buck informs him hes worse than Ranken, who is at least forthcoming with his brutality, even though Manny was adored by the inmates and seen as their hero. Emotionally broken, all three slump into a fatalistic depression in the F-units cab, only to be shattered, literally, when Rankens accomplice crashes through the second engines window and is killed after unsuccessfully trying to board the lead engine via helicopter. Ranken has now caught up with the train.
 
Spurred on by the appearance of his arch-foe, and resolved not to return to prison, even if it means his own death, Manny makes a perilous leap from the F-units broken windshield to the lead engine. Though he is successful, his left hand is crushed in the coupling. The warden successfully boards by helicopter also. Seeking refuge in the damaged cab, Manny manages to disarm and subdue Ranken and handcuffs him inside the cab. With Ranken his prisoner now, Manny is in control of the runaway engine. The locomotives are approaching the end of the abandoned spur, crashing through a disused tunnel. Ranken taunts him and orders Manny to shut down the engine, which he refuses, as he has decided to die and take Ranken with him. When Ranken inquires about Buck and Sara, the exhausted Manny heads back outside. To the music of the second movement of Antonio Vivaldis Gloria (Vivaldi)#RV 589|"Gloria" in D ("Et In Terra Pax"), Manny uncouples the lead engine from the rest of the train, shutting down the second engine, leaving Buck and Sara safely behind.
 Richard III:

 

==Cast==
 
* Jon Voight as Oscar "Manny" Manheim
* Eric Roberts as Buck
* Rebecca De Mornay as Sara
* Kyle T. Heffner as Frank Barstow John P. Ryan as Ranken
* T. K. Carter as Dave Prince Kenneth McMillan as Eddie MacDonald
* Stacey Pickren as Ruby
* Walter Wyatt as Conlan
* Edward Bunker as Jonah
* Reid Cruikshanks as Al Turner
* Dan Wray as Fat Con
* Michael Lee Gogin as Short Con John Bloom as Tall Con Norton E. "Hank" Warden as Old Con
* Danny Trejo as Boxer
* Tiny Lister as Jackson, security guard
* Dennis Franz (uncredited) as Cop
 

==Production==
Akira Kurosawa wrote the original screenplay intending it to be his first color film following Red Beard, but difficulties with the American financial backers led to it being shelved. 

The Alaska Railroad decided that their name and logo would not be shown. Several scenes referred to the railroad as "A&E Northern." The filming took place near Portage Glacier, Whittier, Alaska|Whittier, and Grandview.

The prison scenes at the beginning of the movie were filmed in Deer Lodge, Montana, and some railroad yard scenes were filmed in Anaconda, Montana.
 F7 #1500, and #1801 and #1810, both EMD GP7|GP7s. The latter two locomotives had previously been rebuilt by ARR with low short hoods as opposed to a GP7s original high short hood, but were fitted with mock-up high hoods made of plywood for the film, branded with fictional numbers 531 and 812, respectively. Because #1801s cab had been reconstructed prior to filming, the 531 prosthetic hood stood slightly higher than the normal hood height of a GP7 in order to fit over the locomotives number-board.

The locomotives used in the film have gone their separate ways:
* ARR GP40-2 #3010 is still active on the Alaska Railroad, painted in the new corporate scheme.
* ARR F7 #1500 was retired from service in 1992, is now at the Alaska Transport Museum in Anchorage, AK.
* ARR GP7 #1810 was sold to the Oregon Pacific Railroad and operated as OP #1810. In 2008, the unit was sold to the Cimarron Valley Railroad and is now permanently coupled to former OP Slug #1010.
* ARR GP7 #1801 was sold to a locomotive leasing company in Kansas City, MO, then sold to the Missouri Central Railroad and operated as MOC #1800. The locomotive subsequently appeared in another motion picture,  , in 1995.   MOC became the Central Midland Railroad in 2002.  As Central Midland had their own leased power, MOC 1800 was returned to Midwest Locomotive In Kansas City.  Shortly after, it was then sold the Respondek Rail Corp of Granite City, IL and is now used on Respondeks Port Harbor Railroad subsidiary.  The units identification is RRC #1800.
* The train that was hit by the runaway was led by ALCO MRS-1|MRS-1 #1605. This unit had been retired in 1984, one year before filming started. The unit has since been cut up for scrap.
* Sequences set at the rail yard, shot on the Butte, Anaconda and Pacific Railway in Anaconda, Montana, used local locomotives from the BA&P fleet along with former Northern Pacific EMD F9 #7012A, leased from the Mount Rainier Scenic Railroad. The two GP7s and the F9 were fitted with plywood boxes to duplicate the distinctive winterization hatches carried on their Alaskan counterparts.
* BA&P EMD GP38-2 #109, the BA&P locomotive used in the yard scenes as the lead-engine in place of ARR #3010, was subsequently sold to the Alaska Railroad and remains in service there as #2002, along with sister unit #2001 (ex-BA&P #108).

Richard (Rick) Holley was killed during filming when the helicopter he was piloting hit power lines on the way to a location shoot in Alaska. The film is dedicated to him during the closing credits.

==Release==
===Critical reception=== Michael Phillips At the Movies that it was the most under-rated movie of the 1980s. Roger Ebert awarded the film four out of four stars. 
 Time Out polled several film critics, directors, actors and stunt actors to list their top action films.  Runaway Train was listed at 64th place on this list. 

===Box office===
The film was a box office success. 

===Accolades===
The film was entered into the 1986 Cannes Film Festival.   
 Best Supporting Best Film Editing (Henry Richardson).

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 