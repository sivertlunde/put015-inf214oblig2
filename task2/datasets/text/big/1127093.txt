Mean Creek
 

{{Infobox film
| name           = Mean Creek
| image          = Mean_Creek_movie.jpg
| image_size     =
| caption        = Publicity poster
| director       = Jacob Aaron Estes Susan Johnson Rick Rosenthal Hagai Shaham
| writer         = Jacob Aaron Estes
| narrator       =  Trevor Morgan Josh Peck Carly Schroeder
| music          = tomandandy
| cinematography = Sharone Meir
| editing        = Madeleine Gavin
| casting         = Matthew Lessall
| distributor    = Paramount Classics
| released       = January 15, 2004 (Sundance Film Festival|Sundance) May 14, 2004 (Cannes Film Festival|Cannes) August 20, 2004 (limited release|limited)
| runtime        = 89 minutes 
| country        = United States
| language       = English
| budget         = $500,000
| gross          = $802,948  
| preceded_by    = 
| followed_by    = 
}}
 independent drama Trevor Morgan, Susan Johnson, Rick Rosenthal, and Hagai Shaham. The movie was filmed and set in a small town in Oregon.
 Lewis River  in southwest Washington.

The film premiered at the Sundance Film Festival in January 2004, and was later screened at the Cannes Film Festival that spring. The film was then given a limited release in major cities on August 20, 2004, mostly playing at art house theaters.

==Plot== Trevor Morgan) that the school bully, a dyslexic boy named George (Josh Peck), has hurt him because Sam moved Georges video camera while George was filming himself playing basketball, Rocky devises a plan to exact revenge on George.

Rocky recruits his friends, Clyde (Ryan Kelley) and Marty (Scott Mechlowicz), to assist him with his plan. Part of the prank entails taking George on a boating trip to celebrate Sams birthday party. The ultimate joke, in their opinion, will be when they get him to strip in a game of truth or dare, then make him run home naked.

Sam invites his girlfriend Millie (Carly Schroeder) along. He does not tell her the plan until they arrive near the river. Millie refuses to continue until Sam promises that he will call the plan off, which Sam agrees to do. Sam tells his brother to stop, and Rocky tells his friends what Sam has conveyed to him. Although Clyde has no problem with it, Marty is very reluctant to not go through with the plan. Throughout the trip, George attempts clumsily to fit in with the others by telling rude jokes, which the other members of the group do not find amusing. The group soon realizes that although George is annoying, he is very lonely and just wants to be accepted.

On the boat, Marty deviates from the others plan and initiates a game of truth or dare, though the rest decide to go along. After George shoots Marty with a water gun in good fun, George makes a funny quip about Martys father, not remembering that it is a sore subject as Martys father killed himself. This sets Marty off, who tells George the whole plan and starts to ridicule him.

Angered and humiliated, George launches into a vulgar tirade against everyone else on the boat, ending by crudely mocking Martys dead father. Marty snaps and Rocky, in an attempt to stop the fight, accidentally pushes George off the boat. Unable to swim, George struggles to remain afloat in the water. As the others regard the scene in horror, George accidentally hits his head with his video camera and does not come to the surface. Rocky dives into the water but is unable to find George. Minutes later, George appears face down in the shallow water close to the shore. Rocky exhorts the others to help him bring George to shore, where Millie gives him CPR. The effort is in vain as George is dead and it is apparent that he cannot be revived.

The group is traumatized and in fear of being charged with murder. They dig a hole and bury George. Clydes plan is to explain that it was an accident but Marty threatens them, gaining the complicity of both Clyde and the rest of the group. As they had already tricked George into not telling his mother where he was going, she would not know of their involvement. Marty speaks to the only witnesses of George with the group, his brother and his brothers friend, and they agree to keep quiet.

Marty goes to tell the news to his friends, who have all gathered at Sam and Rockys house. They are willing to accept the consequences as opposed to having the guilt of Georges death hanging over their heads. Marty refuses to turn himself in and feels betrayed by all of them. He storms out and convinces his brother to give him his gun and car. The brother again agrees to the favor, albeit reluctantly. Marty robs a gas station with the gun and drives off, becoming a fugitive. Meanwhile, the others go to Georges house and confess to his mother.

Sam is later seen in a confession room telling the story to the police, who later find and view the tape from Georges video camera. In a final scene, audio of George explaining his dream of becoming a filmmaker and documenting his life in hopes that those who see it will finally understand him plays in the background while Sam watches the sheriff exhume Georges body.

==Cast==
*Rory Culkin as Sam
*Josh Peck as George
*Scott Mechlowicz as Marty Trevor Morgan as Rocky 
*Carly Schroeder as Millie
*Ryan Kelley as Clyde

==Reception==
Mean Creek received highly positive reviews from critics and has a rating of 90% on Rotten Tomatoes based on 119 reviews with an average rating of 7.3 out of 10. The consensus states "Mean Creek is an uncomfortably riveting glimpse into the casual cruelty of youth."  The film also has a score of 74 based on 31 reviews on Metacritic. 

Roger Ebert praised the acting and concept of teenagers making conscious moral decisions and wrote "Mean Creek joins a small group of films including "The Rivers Edge" and "Bully," which deal accurately and painfully with the consequences of peer-driven behavior. Kids who would not possibly act by themselves form groups that cannot stop themselves. This movie would be an invaluable tool for moral education in schools, for discussions of situational ethics and refusing to go along with the crowd." 

The film received a limited release in North America in 4 theaters and grossed $29,170 with an average of $7,292 per theater. The film earned $603,951 domestically and $198,997 internationally for a total of $802,948.  

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Deauville American Film Festival Grand Special Prize Jacob Aaron Estes
| 
|- Flanders International Film Festival Ghent Grand Prix
| 
|- Humanitas Prize Sundance Film Category
| 
|- Film Independent Independent Spirit Awards Independent Spirit John Cassavetes Award
| 
|- Susan Johnson Susan Johnson
| 
|- Rick Rosenthal
| 
|- Hagai Shaham
| 
|- Special Distinction Award Rory Culkin
| 
|- Ryan Kelley
| 
|- Scott Mechlowicz
| 
|- Trevor Morgan Trevor Morgan
| 
|- Josh Peck
| 
|- Carly Schroeder
| 
|- Stockholm International Film Festival Best Directorial Debut Jacob Aaron Estes
| 
|- Young Artist Awards Young Artist Best Leading Young Actor in a Feature Film Rory Culkin
| 
|- Young Artist Best Leading Young Actress in a Feature Film Carly Schroeder
| 
|-
|}

==See also==
*List of artistic depictions of dyslexia

==References==
 

== External links ==
 
*  
*  
*  
*  
*  
*   at Drews Script-o-rama

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 