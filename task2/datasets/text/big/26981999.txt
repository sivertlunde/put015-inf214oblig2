The Rift (1989 film)
{{Infobox film
| name           = The Rift (Endless Descent)
| image          = Endless-descent.jpg
| title          = Theatrical Release Poster
| director      = Juan Piquer Simón Francesca De Jose Escriva Juan Piquer Simón Mark Klein David Coleman
| narrator       =
| starring       = Jack Scalia R. Lee Ermey Ray Wise Deborah Adair John Toles-Bey
| music          = Joel Goldsmith Juan Mariné Christopher Holmes
| studio         =
| distributor    =
| released       = Spain:  March, 9th 1990 West Germany: July 26, 1990 Australia: May 30, 1991
| runtime        = 79 minutes
| country        = United States Spain
| language       = English
| gross          =
}}

The Rift (Endless Descent) is a 1990 film directed by Juan Piquer Simón and starring R. Lee Ermey involving a submarine rescue that goes awry. It is one of many underwater-themed movies released around 1990, including The Abyss, Leviathan (1989 film)|Leviathan, DeepStar Six, The Evil Below and Lords of the Deep.

==Cast==
*Jack Scalia as Wick Hayes
*R. Lee Ermey as Capt. Phillips
*Ray Wise as Robbins
*Deborah Adair as Lt. Nina Crowley
*John Toles-Bey as Joe Kane (as John Toles Bey)
*Ely Pouget as Ana Rivera
*Emilio Linder as Philippe
*Tony Isbert as Fleming
*Álvaro Labra as Carlo Luis Lorenzo as Francisco
*Frank Braña as Muller
*Pocholo Martínez-Bordiú as Sven (as J. Martinez Bordiu)
*Edmund Purdom as CEO Steensland
*Garrick Hagon as Barton (as Garick Hagon) James Aubrey as Contek 1
*Derrick Vopelka as Contek 2
*Jed Downey as Man on Tapes

==Plot==
An experimental submarine, the "Siren II", with an experienced crew is sent to find out what happened to the missing "Siren I".

They trace the Siren I’s black box to an underwater rift where they discover that the company they are working for has been engaged in illegal genetic engineering experiments that have produced a number of different of mutant creatures.

==Reception==
 

==References==
 
 

== External links ==
*  - IMDB
*  - Rotten Tomatoes

 
 
 
 
 
 
 
 


 