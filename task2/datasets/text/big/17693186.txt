Slave Widow
 
{{Infobox film
| name           = Slave Widow
| image          = Slave Widow DVD.jpg
| image_size     =
| caption        = Cover to the DVD region code|region-1 DVD release
| director       = Mamoru Watanabe (as Yuzuru Watanabe)
| producer       =
| writer         = Mamoru Watanabe
| starring       = Noriko Tatsumi
| music          =
| cinematography =
| editing        =
| distributor    = Mutsukuni Eiga / Chuo Eiga
| released       = July 11, 1967
| runtime        = 73 minutes
| country        = Japan
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
}}

  is a 1967 Japanese Pink film directed by Mamoru Watanabe and starring Noriko Tatsumi. Along with the first "Queen" of Japanese soft-core pornography, the films cast includes Mari Imai and an early appearance by Naomi Tani, two other major stars of pink film.

==Synopsis==
When Mitsukos husband dies leaving her with a large debt, she becomes the sexual slave of his largest creditor, Kitō. Kitōs son Kazuhiko, who is expected to marry the daughter of a wealthy businessman, falls in love with Mitsuko. To escape the situation, Mitsuko drowns herself in the lake near her husbands country villa.    

==Cast==
* Noriko Tatsumi... Mitsuko Fuji 
* Masayoshi/Seigi Nogami... Kazuhiko, son of Kitō
* Mari Iwai... Maya, the maid
* Tadashi Oizumi... Kitō
* Naomi Tani... Mariko

==Style and themes==
In his book Behind the Pink Curtain (2008), Jasper Sharp writes that Mamoru Watanabes pink films are usually "lyrical...   full of poetic symbolism and striking pictorial compositions..."  Watanabe opens Slave Widow with close-ups of Noriko Tatsumis body rising and falling to the sound of heavy breathing, juxtaposed with images of the lake near her country villa. Watanabes skillful use of sound is also evident in the contrast between Mitsuko and Kazuhikos two love scenes. In the first, a lyrical scene which takes place near the lake in which Mitsuko will drown herself, the visuals are accompanied by natural sounds such as crickets, birds and water. In the second, which takes place in Tokyo, the desperation of Mitsukos situation is emphasized with harsh city noises such as non-stop traffic. 

Thematically, the film criticizes the nouveau riche of Japans post-war era in the person of Kitō. The conflict between romantic life and filial piety is also explored in the storyline involving Kitōs son Kazuhikos affection for Mitsuko. This puts the son in conflict with his father both because Kitō desires to make Mitsuko his own mistress, and because Kazuhiko is expected to marry the daughter of Kitōs business partner. It is Mitsuko who pays for the breach of social protocol through her suicide. 

==Availability==
Harry Novak released Slave Widow theatrically in the United States through his Boxoffice International Pictures distribution company.    Jasper Sharp notes that had it not been for Novaks release of the film, and subsequent preservation, Slave Widow would likely not be available today, as many early pink films were not saved in Japan.  The film was again available to U.S. audiences in the late 1990s in a videotape release by Something Weird Video.  The film was released on DVD region code|region-1 DVD by Gregory Hatanakas Cinema Epoch label on April 1, 2008. Though the DVD release contains no extras significant to the film, the trailer to Slave Widow is available on Something Weird Videos release of Kōji Wakamatsus The Notorious Concubines (1969). 

Following the DVD release, on May 9 and May 11, 2008, Portland, Oregons Clinton Street Theater played Slave Widow on a double-bill with Hiroshi Mukais The Bite (1966), previously considered a lost film. 

==Notes==
 

==Sources==
* Barrett, Michael.   (English).  , May 8, 2008. Retrieved on May 24, 2008.
* Buchanan, Jason.  . Allmovie.
* Cowie, Peter (editor). "Dorei Mibôjin" in "Japan" in World Filmography 1967 (1977). Tantivy Press, London. ISBN 0-498-01565-3. p.&nbsp;351.
*   (English).  . Retrieved on May 24, 2008.
*   (Dorei mibojin) at Japanese Movie Database. Retrieved on May 24, 2008.
* Loomis, Daryl.   (English).  , April 10, 2008. Retrieved on May 24, 2008.
* Wallis, John.   (English).  , March 28, 2008. Retrieved on May 24, 2008.
* Weisser, Thomas and Yuko Mihara Weisser. (1998). Japanese Cinema Encyclopedia: The Sex Films. Vital Books : Asian Cult Cinema Publications, Miami. ISBN 1-889288-52-7. pp.&nbsp;394–395.

== External links ==
*  
*  

 
 
 