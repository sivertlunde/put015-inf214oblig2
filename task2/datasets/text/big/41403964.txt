Chances (film)
{{infobox film
| title          = Chances
| image          =
| imagesize      =
| caption        =
| director       = Allan Dwan
| producer       = First National
| writer         = A. Hamilton Gibbs (novel) Waldemar Young (adaptation)
| starring       = Douglas Fairbanks, Jr. Rose Hobart
| music          = David Mendoza Oscar Potoker
| cinematography = Ernest Haller
| editing        = Ray Curtiss
| distributor    = First National and Warner Brothers
| released       = July 18, 1931
| runtime        = 72 minutes
| country        = USA
| language       = English

}}
Chances is a 1931 Pre-Code Hollywood|Pre-code war drama directed by Allan Dwan, starring Douglas Fairbanks, Jr., and is based on a novel by A. Hamilton Gibbs. 

==Cast==
*Douglas Fairbanks, Jr. - Jack Ingleside
*Rose Hobart - Molly Prescott
*Anthony Bushell - Tom Ingleside
*Holmes Herbert - Major Bradford
*Mary Forbes - Mrs. Ingleside
*Edmund Breon - The General
*Harry Allen - Private Jones

uncredited
*Robert Bennett
*Billy Bevan - Cuthbert, Pub Waiter
*Florence Britton - Sylvia
*David Cavendish - Bit
*Tyrell Davis - Archie
*Jean Fenwick - 
*Ethel Griffies - Drunk Flower Vendor Ruth Hall - Girl at Party
*Forrester Harvey - Joe, News Vendor
*Mae Madison - Ruth
*Edward Morgan - Lt. Wickham
*Jameson Thomas - Lt. Taylor

==Preservation status==
Preserved at the Library of Congress. 

==References==
 

==External links==
* 
* 

 
 
 
 
 

 