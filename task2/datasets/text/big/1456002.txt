Road to Rio
 
{{Infobox film
| name           = Road to Rio
| image          = Road_to_Rio_1947_Poster.jpg
| border         = yes
| caption        = Theatrical release poster Norman McLeod
| producer       = Daniel Dare
| writer         = {{Plainlist|
* Edmund Beloin Jack Rose
}}
| screenplay     = Barney Dean
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Bing Crosby
* Bob Hope
* Dorothy Lamour
}}
| music          = Robert Emmett Dolan
| cinematography = Ernest Laszlo
| editing        = Ellsworth Hoagland
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $4.5 million (US/ Canada rentals)  
}}
 Jack Rose, the film is about two inept vaudevillians who stow away on a Brazilian-bound ocean liner and foil a plot by a sinister hypnotist to marry off her niece to a greedy fortune hunter. Road to Rio was the fifth of the "Road to …" series.

==Plot==
Scat Sweeney and Hot Lips Barton, two out-of-work musicians, travel the United States trying to find work and stay away from girls.  After running from state to state, each time running because of a girl, they try their luck in Louisiana.

They stow away on board a Rio de Janeiro|Rio-bound ship, after accidentally starting some fires at a circus. They then get mixed up with the distraught Lucia, who first thanks them, then unexpectedly turns them over to the ships captain. Unbeknownst to both of them, Lucia is being hypnotized by her crooked guardian, Catherine Vail.  Vail plans to marry Lucia to her brother so she can control her and a set of "papers."

After a series of misadventures. including sneaking off the boat, recruiting a few local musicians, and the boys trying to escape with Lucia only to have Vail hypnotize her again and slap them both, Vail decides to do away with the boys permanently.  She hypnotizes both of them and tries to get them to kill each other in a duel, but it fails.  Scat and Hot Lips finally figure things out and the boys head for the ceremony in order to stop the wedding and to help catch the crooks. Upon finding the "papers", which Scat reads, when Hot Lips asks what they are about, Scat tears them up and looks into the camera, saying, "The world must never know."

Later on, Scat is dismayed to see that Lucia loves Hot Lips and not him, but upon peaking through a keyhole, he sees Hot Lips hypnotizing her.
 Jerry Colonna has a cameo as the leader of a cavalry charging to the rescue of Bing and Bob, as the film cuts away to the galloping horses periodically. All is resolved before he can arrive, leading Colonna to point out: 

 

==Cast==
* Bing Crosby as Scat Sweeney
* Bob Hope as Hot Lips Barton
* Dorothy Lamour as Lucia Maria de Andrade
* Gale Sondergaard as Catherine Vail
* Frank Faylen as Trigger
* Joseph Vitale as Tony
* George Meeker as Sherman Mallory
* Frank Puglia as Rodrigues
* Nestor Paiva as Cardoso
* Robert Barrat as Johnson
* Stanley Andrews as Capt. Harmon Harry Woods as Ships Purser
* The Wiere Brothers as Three Musicians
* The Andrews Sisters as Themselves Jerry Colonna as Himself   
* Marquita Rivera as Nightclub Performer

==Production==

===Soundtrack=== Johnny Burke (lyrics) and Jimmy Van Heusen (music).
* "You Dont Have to Know the Language", performed by Bing Crosby and The Andrews Sisters
* "Experience", by Dorothy Lamour But Beautiful", Bing Crosby
* "Apalachicola, Fla.", Bing Crosby and Bob Hope
* "For What?", Bing Crosby and Bob Hope
* "Diz Que Tem", performed by Marquita Rivera   

For the commercial recordings, Bing Crosby recorded the song "Experience" with big band vocalist Nan Wynn. Bing Crosby also recorded the songs "You Dont Have to Know The Language" and "Apalachicola, Fla" with the Andrews Sisters. Bing did not record any of the songs from the film with Bob Hope or Dorothy Lamour.

==Copyright==
 
 
The films copyright was renewed in a timely manner by an associate of the films owner. Originally registered for copyright as LP1171 with a declared publication date of August 25, 1947, the continuation of copyright was contingent upon renewal between the 27th and 28th anniversaries of that date. Renewal occurred July 25, 1975, number R610335. The film opened February 19, 1948, so the renewal is still timely even if that later date were considered publication date.

The copyright is now scheduled to run until 95 years after the publication date (2042). The renewal form names as authors Hope Enterprises and Bing Crosby Enterprises, which were partners when the film was produced. As it turned out, the copyright (which never expired, and thus the film never entered the public domain) was reassigned to Columbia Pictures Television—Columbia and LBS Communications (which formed the joint venture Colex Enterprises) assumed the rights to this film from Hope Enterprises, and re-released the film to television in the 1980s.

Today, CPT and what is now FremantleMedia hold ancillary rights, with FremantleMedia holding the video license. Any DVD release from a company not licensed by FremantleMedia violates the copyright, which continues to credit CPT as the copyright holder, as evidenced by recent video releases by BCI Eclipse, and now, as of the present, Shout! Factory (under Fremantles license).

==References==
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 