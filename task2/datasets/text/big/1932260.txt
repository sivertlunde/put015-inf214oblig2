Superstar: The Karen Carpenter Story
 
 
{{Infobox film
| name           = Superstar: The Karen Carpenter Story
| image          = Superstar The Karen Carpenter Story cover.jpg
| alt            = 
| caption        = Cover
| director       = Todd Haynes
| producer       = Todd Haynes
| writer         = Todd Haynes Cynthia Schneider Michael Edwards
| narrator       = Gwen Kraus Bruce Tuthill
| music          = The Carpenters
| cinematography = Barry Ellsworth
| editing        = Todd Haynes
| studio         = Iced Tea Productions
| distributor    = American International Video Search, Inc.
| released       =  
| runtime        = 43 minutes
| country        = United States
| language       = English
| budget         =
}} short biographical Richard Carpenter.    The films title is derived from The Carpenters 1971 hit song, "Superstar (Delaney and Bonnie song)#Carpenters version|Superstar".

Over the years, it has developed into a cult film and is included in Entertainment Weekly s 2003 list of top 50 cult movies. 

==Plot== first person flashback to 1966. The story touches on major points in Karens life from 1966 on:
* The duos signing with record label A&M Records|A&M
* Their initial success and subsequent decline
* Karens development of anorexia nervosa (spurred by an infamous review which described the well-proportioned Karen as "chubby")  Las Vegas
* Her search for treatment for her anorexia nervosa
* Her attempt to restart her career
* A claim that she gradually developed a reliance on syrup of ipecac (a product which, unbeknownst to her, destroyed her heart and led to her cardiac arrest).
 parodies of the documentary genre. The underlying soundtrack included many popular hits of the day, including songs by Gilbert OSullivan, Elton John, Leon Russell, and the Carpenters themselves.
 perfectionist who gay – which, if it had been reported to the public in the 1970s, would have destroyed the Carpenters "clean-cut" image and the groups career.

Haynes treatment of the film was quite dark; his choice of black captions often blended in with the scene, rendering them unreadable. Additionally, Haynes worked spanking (a common theme in his works) into the film with a repeated segment featuring a black-and-white overhead view of someone, possibly Harold, administering an over-the-knee spanking to a bare-bottomed adult Karen. The meaning of this segment is never discussed, leaving it to the viewers imagination – it may be an actual event, a representation of Karens self-loathing regarding her inability to be the "perfect" child, or a representation of the self-discipline involved with her anorexia.

==Cast==
 
* Gwen Kraus and Bruce Tuthill as Narrators
* Merrill Gruver as Karen Michael Edwards as Richard
* Melissa Brown as Mother
* Rob LaBelle as Father / Mr. A&M
* Nannie Doyle as Cherry Boone
* Cynthia Schneider as Dionne Warwick
* Larry Kole as Announcer
* Joanne Barrett
* Todd Haynes
* Michelle Manenti
* Moira McCarty
* Richard Nixon (archive footage) as himself
* Ronald Reagan (archive footage) as himself
 

==Songs==
* "Superstar (Delaney and Bonnie song)#Carpenters version|Superstar" – The Carpenters (Beginning credits)
* "Ill Never Fall in Love Again" – Dionne Warwick (which Karen sings along to)
* "Ill Never Fall in Love Again" – The Carpenters 
* "Weve Only Just Begun" – The Carpenters
* "(They Long to Be) Close to You" – The Carpenters Top of the World" – The Carpenters 
* "Sing (The Carpenters song)|Sing" – The Carpenters (at the White House)
* "Alone Again (Naturally)" – Gilbert OSullivan (while Karen is talking on the phone) Let Me Be the One" – The Carpenters (played straight after "Alone Again") Native New Odyssey (restaurant scene)
* "Loves Theme" – The Love Unlimited Orchestra Philadelphia Freedom" – Elton John (played briefly as Richard discovers Karen unconscious)
* "Rainy Days and Mondays" – The Carpenters (at the end of the song, Karen collapses)
* "Love Will Keep Us Together" – Captain & Tennille (scene when naked body parts are shown)
* "Dont Go Breaking My Heart" – Elton John with Kiki Dee (Karens housewarming party)
* "This Masquerade" – The Carpenters (Karen meeting Tom Burris) For All We Know" – The Carpenters (New York/Recovery montage)
* "(They Long to Be) Close to You" – The Carpenters (ending)

==Response==
Upon its release, the film was a minor art hit, and was shown at several film festivals. However, shortly thereafter, Richard Carpenter viewed the film and became irate with the films portrayal of his family, in particular because the film insinuated Richard was gay. It later emerged that Haynes never obtained  .  The Museum of Modern Art retains a copy of this film but has agreed with the Carpenter estate not to exhibit it. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 