Diamond Head (film)
{{Infobox film
| name           = Diamond Head
| image          = Diamond Head 1963 poster.jpg
| image_size     = 220
| caption        = 1963 theatrical poster Guy Green Jerry Bresler
| based on       =  
| writer         = Marguerite Roberts Elizabeth Allen
| music          = John Williams
| cinematography = Sam Leavitt
| editing        = William A. Lyon
| distributor    = Columbia Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| gross          = $4.5 million (US/ Canada rentals)  
}}
 Guy Green, and released by Columbia Pictures. The original music score was composed by John Williams, Hugo Winterhalter composed the theme, and Darren sang the title song. The soundtrack album was released by Colpix Records (CP 440).

Silent film star Billie Dove makes her last film appearance in a brief cameo.

==Plot summary==
Richard "King" Howland is a swaggering bigoted land baron living on the Big Island of Hawaii. He objects when his sister, Sloan Howland, announces she plans to marry Paul Kahana, a native Hawaiian, even though Richard is having a torrid affair with an Asian woman, Mai Chen. During Sloan and Pauls engagement party, Mai Chens brother attacks Richard with a knife. Paul tries to break up the fight and is killed. Bitter at her brother for Pauls death, Sloan runs off to Honolulu where she is taken in by Pauls brother, Dean, and his family.

Meanwhile, Mai Chen gives birth to Richards child, but dies during childbirth. Ever the rabid racist, Richard refuses to accept the child and Sloan takes it upon herself to care for the baby. After an angry fight with Sloan and Dean, Richard is confronted with a personal dilemma &mdash; whether to continue on with his close-minded ways or to welcome his newborn son into his family.

Although the story is based on the novel by Peter Gilman, the screenplay by Marguerite Roberts makes several significant changes in Gilmans story. Several characters are eliminated, including Richards father, Richards wife, and his hapa haole (half-Hawaiian/half Caucasian) half-brother. Roberts also changed the ending of the story.

==Main cast==
{| class="wikitable"
|- style="background:#CCCCCC;"
! Actor !! Role
|-
| Charlton Heston || Richard "King" Howland
|-
| Yvette Mimieux || Sloan Howland
|-
| George Chakiris || Dr. Dean Kahana
|-
| France Nuyen || Mai Chen
|-
| James Darren || Paul Kahana
|-
| Aline MacMahon || Kappa Lani Kahana
|- Elizabeth Allen || Laura Beckett
|- Vaughn Taylor || Judge James Blanding
|-
| Philip Ahn || Mr. Immacona
|-
| Edward Mallory || Robert Parsons
|}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 