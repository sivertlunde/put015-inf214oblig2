Let the Love Begin
{{Infobox film
| name = Let the Love Begin
| image =
| caption = Their romantic journey starts here
| director = Mac Alejandre
| producer = Annette Gozon-Abrogar
| writer = Suzette Doctolero RJ Nuevas
| starring =Richard Gutierrez Angel Locsin Mark Herras Jennylyn Mercado
| music =Universal Records Theme song "Let the Love Begin" by Kyla and Jerome John Hughes
| distributor = GMA Films
| released =  
| runtime = 135 minutes
| gross = P120,000,000
| country = Philippines Tagalog English English
}}
 romantic film released by GMA Films in the Philippines.  It is by far and has set the record to be one of the highest Valentine Movie of all time. The film was directed by Mac Alejandre. It starred Richard Gutierrez, Angel Locsin, Jennylyn Mercado and Mark Herras.

== Plot ==
The story is set in high school.  Pia (Angel Locsin) is beautiful and wealthy. She is the campus sweetheart that every guy is dreaming of. However, she is unhappy because her father (Tonton Gutierrez) constantly blames her for her brothers death, and makes all the decisions with regards to her future. Eric (Richard Gutierrez), Pias high school classmate, is intelligent and has a good heart, but lives in poverty. Both of his parents have already died. He is left with his kind and loving grandmother who continues to encourage him to pursue his dreams and his love for Pia, no matter what obstacles get in his way. Eric works as the schools janitor during the day to support his studies, and to provide for him and his grandmothers needs. He has always admired Pia but never really had the courage or the chance to even talk to her. Although they belong to the extremes of social classes, Pia and Erics paths cross when they share the same seat in class - Eric during the night (as part of his scholarship program) and Pia during the day. Eric becomes Pias anonymous savior as he answered Pias school-related questions, and promised to always be there whenever she needed him. Are they destined to be with each other because of this sign? Or is fate playing a trick on them?
Meanwhile, in the background, one of Erics best friends, Luigi(Mark Herras) is the typical playboy, and often changes his girlfriend. This has gone on since they were in highschool. Luigi best friend is Eric, but on the sidelines, Alex(Jennelyn Mercado). Alex is a tomboy who has a crush on Luigi. After five years, she changed and blossomed into a beautiful girl, with Luigi falling in love with her.

== Cast ==
* Richard Gutierrez as Eric Crisanto
* Angel Locsin  as Pia
* Mark Herras  as Luigi
* Jennylyn Mercado as Alex Gloria Romero as Lola Maring
* Paolo Contis as Uno
* Bearwin Meily as Boy Palito
* Tuesday Vargas as Wendy
* Tonton Gutierrez as Jake
* Dino Guevarra as Wency
* Mon Confiado as Baldo
* Polo Ravales as Brix
* Nanette Inventor as Lucing
rest of cast listed alphabetically: 
* Nicole Anderson as Nicole Cabrera
* Jerome John Hughes as Jaime Gaston
* Julianne Lee as Lucille Balboa
* Geoff Rodriguez as Dave Shamrock as Themselves
* Tessbomb as Tess
* Maggie Wilson as Bridget Dantes

== Theme song ==
The title of the movie is based on the song, Let the Love Begin. The song was written by Gloria Sklerov and Lenny Macaluso and performed by Jimmy Demers and Carol Sue Hill for the love theme of the American movie Thrashin.   

In the Philippines, the song was originally popularized by singer Gino Padilla in 1987 and it became popular.  In the Let the Love Begin movie, Kyla and Jerome John Hughes renditioned the song for its soundtrack.

== Trivia ==
* Epson and McDonalds, major sponsors of the film, are promoted in several scenes.
* Most of the classroom scenes were filmed at the Lyceum of the Philippines University.
* The airport scene was shot at Clark International Airport.
* The soccer field scene was shot at the Claret School of Quezon City.
* The movie starred Richards older half-brother, Tonton Gutierrez.

==Media release==
The series was released onto DVD-format and VCD-format by GMA Records. The DVD contained the movie plus bonus features like the music video of "Let the Love Begin" plus the full-length trailer of the movie and Karaoke features. The DVD/VCD was released in 2005.

==External links==
* 

 
 
 
 
 