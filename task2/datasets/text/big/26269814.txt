My Friend Ivan Lapshin
{{Infobox film
| name = My Friend Ivan Lapshin Aleksei German
| producer = Lenfilm
| based on =  
| screenplay = Eduard Volodarsky Andrei Mironov
| music = Arkadi Gagulashvili
| cinematography = Valeri Fedosov
| editing = Leda Semyonova
| released = 1984
| runtime = 100 minutes
| country =  
| language = Russian
}} Aleksei German and produced by Lenfilm. Based on a novel by Yuri German adapted by Eduard Volodarsky. Music composed by Arkadi Gagulashvili, sound by Nikolai Astakhov. Cinematography by Valeri Fedosov, film editing by Leda Semyonova. Narrated by Valeri Kuzin. Runtime - 100 min.

Set in 1935 in the fictional provincial town of Unchansk, the film is presented as the recollections of a man who at the time was a nine-year-old boy living with his father in a communal flat shared with criminal police investigator Ivan Lapshin and a number of other characters. There are several plot strands: a provincial troupe of actors arrive and put on a play without much success; a friend of Lapshins, the journalist Khanin, shows up, depressed after his wifes death; and Lapshin investigates the Solovyov gang of criminals. Lapshin falls in love with the actress Natasha Adashova, but she is in love with Khanin. It is "a film about people building socialism on a bleak frozen plain, their towns one street a long straggle of low wooden buildings beneath a huge white sky, leading from the elegant stucco square by the rivers quayside out into wilderness": 
These are people whose faith in the future remains intact, but whose betrayal is imminent. German has said that his main aim was to convey a sense of the period, to depict as faithfully as possible the material conditions and human preoccupations of Soviet Russia on the eve of the Great Purge. It is for this world, for these people that the narrator struggles to declare his love—unconditional, knowing how flawed that world was, and how tainted the future would be. German compared the film to the work of Chekhov, and one can see in it a similar tenderness for the suffering and absurdity of its characters.  
 

==Cast==
* Andrei Boltnev as Ivan Lapshin
* Nina Ruslanova as Natasha Adashova Andrei Mironov as Khanin
* Aleksei Zharkov as Okoshkin
* Zinaida Adamovich as Patrikeyevna
* Aleksandr Filippenko as Zanadvorov Yuriy Kuznetsov as Superintendent
* Valeriy Filonov as Pobuzhinskiy
* Anatoli Slivnikov as Bychkov
* Andrei Dudarenko as Kashin
* Semyon Farada
* Nina Usatova
* Lidiya Volkova
* Yuri Aroyan
* Natalya Laburtseva
* Anna Nikolayeva
* Anatoli Shvedersky
* Vladimir Tochilin
* Boris Vojtsekhovsky
* S. Kushakov, B. Meleshkin, Yu. Pomogayev, V. Sirotenko

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 