Sassy Pants
{{Infobox film
| name           = Sassy Pants
| image          = Sassy Pants poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Coley Sohn 
| producer       = Pavlina Hatoupis Adam Wilkins 
| writer         = Coley Sohn 
| starring       = Anna Gunn Ashley Rickards Haley Joel Osment Diedrich Bader Jenny OHara
| music          = Angela Correa
| cinematography = Denis Maloney 
| editing        = Robin Katz Kindra Marra 
| studio         = Ministry of Content Sassy Pictures Spruce Street Films Urbanite
| distributor    = Phase 4 Films
| released       =   
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Sassy Pants is a 2012 American comedy film written and directed by Coley Sohn. The film stars Anna Gunn, Ashley Rickards, Haley Joel Osment, Diedrich Bader and Jenny OHara. The film was released on October 19, 2012, by Phase 4 Films.

== Cast ==
*Anna Gunn as June Pruitt
*Ashley Rickards as Bethany Pruitt
*Haley Joel Osment as Chip Hardy
*Diedrich Bader as Dale Pinto
*Jenny OHara as Grandma Pruitt
*Martin Spanjers as Shayne Pruitt
*Shanna Collins as Brianna
*Aaron Perilo as Cory
*Rene Rosado as Hector
*Drew Droege as Michael Paul 

==Reception==
Sassy Pants received mixed reviews from critics. On Rotten Tomatoes, the film has a rating of 50%, based on 6 reviews, with an average rating of 5.5/10.  On Metacritic, the film has a rating of 51 out of 100, based on 6 critics, indicating "mixed or average reviews". 

== References ==
 

== External links ==
*  

 
 
 
 
 