A Single Spark
{{Infobox film
| name           = A Single Spark
| image          = A Single Spark.jpg
| caption        = Poster for A Single Spark (1995)
| director       = Park Kwang-su 
| producer       = Yoo In-taek
| writer         = Lee Chang-dong Kim Jeong-hwan Yi Hyo-in Hur Jin-ho Park Kwang-su
| starring       = Moon Sung-keun Hong Kyung-in
| music          = Song Hong-seop
| cinematography = You Young-gil
| editing        = Kim Yang-il
| distributor    = Age of Planning
| released       =  
| runtime        = 
| country        = South Korea
| language       = Korean
| film name = {{Film name hangul         =       hanja          = 아름다운   전태일 rr             = Areumdaun cheongnyeon Jeon Tae-il mr             = Arŭmdaun ch‘ŏngnyŏn Chŏn T‘ae-il }}
| budget         = 
| gross          = 
}}
A Single Spark ( ; lit. "A Beautiful Youth, Jeon Tae-il") is a 1995 South Korean film.

==Plot==
A biographical film about Jeon Tae-il, a worker who protested labor conditions through self-immolation. 

==Awards==
* Blue Dragon Film Awards (1995) Won Best Film Award 
* 46th Berlin International Film Festival (1996) Nominated for Golden Bear (Park Kwang-su)   

==References==
 

==Bibliography==
*  
*  
*  
*  

  
 
  
 

 
  
 
 

 
 
 
 
 
 


 