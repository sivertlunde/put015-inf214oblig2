Dattha
 
{{Infobox film
| name = Datta
| image = 
| caption =
| director = Chi Guru Dutt
| writer = Janardhan Maharshi Darshan  Ramya   Keerthi Chawla
| producer = Chi Guru Dutt
| music = R. P. Patnaik
| cinematography = Ramesh Babu
| editing = Deepu S Kumar
| studio = Udaya Ravi Cinemas
| released =  
| runtime = 146 minutes
| language = Kannada
| country = India
| budget =
}} action film Darshan and Ramya in the lead roles whilst Keerthi Chawla, Srinath and Vinaya Prasad play other pivotal roles. 

The film featured original score and soundtrack composed by R. P. Patnaik. The film, upon release received average response from both critics and audience.

== Cast == Darshan as Dattha
* Ramya
* Keerthi Chawla
* Komal Kumar as Tingu
* Srinath
* Avinash
* Vinaya Prakash
* Chitra Shenoy
* Bullet Prakash
* Mohan Juneja
* Shanthamma

== Plot ==

Datta (Darshan) & his friend Tingu (Komal) are small time thieves. They get in the wrong books of drug peddler Basha. During one of  their thefts, datta sees ramya and falls for her. In order to woo her, Datta returns what has been stolen in her house. Also pretends to be a well educated person, polyglot and also acts as a Police Inspector. However, eventually she finds out and breaks up with him, also insulting him in the process.

Later Datta is persuaded by a rich man (Srinath) who requests him to act like his son, so that his wife would recover from a bedridden sickness. This will be a plot by srinath to eliminate datta. Once his rivals finish datta who is enacting his son, he can bring back his original son and be assured of his safety.

Datta, not knowing srinaths ploy goes out along with Srinath to his village, but is surprised that he is being constantly followed and attacked by rowdies; who he feel are henchmen of Basha.

Ramya the daughter of Srinaths rival also tries to eliminate datta whom she feels is Srinaths son.

How Datta unites the warring families forms the crux of the story.

== Soundtrack ==
The music was composed by R. P. Patnaik. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Escape Mamu Karthik
| lyrics1 = V. Nagendra Prasad
| length1 = 
| title2 = Manasa Manasa
| extra2 = Kunal Ganjawala, Shreya Ghoshal
| lyrics2 = K. Kalyan
| length2 = 
| title3 = Ee Soundaryakke Nanditha
| lyrics3 = K. Kalyan
| length3 = 
| title4 = Kannige Kaanisuva
| extra4 = Madhu Balakrishnan
| lyrics4 = K. Kalyan
| length4 = 
| title5 = Baare Baare
| extra5 = Shankar Mahadevan, Malathi
| lyrics5 = V. Nagendra Prasad
| length5 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 


 

 