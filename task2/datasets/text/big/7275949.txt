Las Noches del Hombre Lobo
{{Infobox Film
| name           = Las Noches del Hombre Lobo
| image          = 
| caption        = 
| director       = René Govar
| producer       = 
| writer         = C. Bellard, René Govar, Jacinto Molina Peter Beaumont, Monique Brainville
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Spain France Spanish
| budget         = 
}} 1968 Spanish horror film that is the second in a long series about the werewolf Count Waldemar Daninsky, played by Paul Naschy. It is considered by some to be a lost film.    It has come to be referred to as the second of the 12 Waldemar Daninsky werewolf movies. However, there are serious doubts as to whether the film was actually made; no one (including Naschy himself) has ever seen it.  It apparently was never theatrically released, nor has it ever turned up on video.

==Real or a hoax?==
Naschy was the only person who ever insisted that his script was actually filmed. He insisted in interviews that he made the film in Paris. The story goes that the director, Rene Govar, was killed in a car accident  in Paris a week after the film was sent off to the lab for processing, and that since no one ever paid the negative costs, the lab held onto the film for collateral and later misplaced or discarded it.  To make matters even stranger, the lead actors whom Naschy attributed to this film (Peter Beaumont and Monique Brainville) apparently never existed. The "French director" has no other credits on IMDb, nor is there a record of a director named Rene Govar ever working in the French film industry.  This is why most Naschy fans regard this movie listing as a hoax or error, some thinking that Naschy may even have made it up to boost his resume at a time when he was just starting out in the industry.

==Plot==
Seeing how this is a lost film, little is known about its plot. All that is known of it, as mentioned by Naschy himself, is that the story deals with a professor who learns that a student of his suffers from lycanthropy, and under the guise of helping him, uses him as an instrument of revenge by controlling him by means of sound waves when he transforms. It is possible this film later somehow became the 1970 "FURY OF THE WOLF MAN" as the plots of the two films are very similar, and that would explain why "NIGHTS" no longer exists. Someone on a fan site claimed they saw a still photo from this film, but it could have actually been a still from "Fury of the Wolf Man" with a variant title printed on it by the distributor, especially since "Fury" was only theatrically released five years after it was made!

==References==
 

==The rest of the series==
Another Naschy film, El Retorno del Hombre Lobo, was recently released on DVD as Night of the Werewolf, causing confusion between the two films. Nights was followed by the third film in the series in 1969, Los Monstruos del Terror.

==External links==
* 

 

 
 
 
 
 
 
 