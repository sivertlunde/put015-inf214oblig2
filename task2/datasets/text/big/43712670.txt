Hello, Hello Brazil!
{{Infobox film
| name =  Hello, Hello Brazil!
| image = Alô, Alô Brasil, Carmen Miranda 1935.jpg
| caption = Carmen Miranda during filming.
| director = João de Barro Wallace Downey Alberto Ribeiro
| producer = Wallace Downey Adhemar Gonzaga
| writer = Alberto Ribeiro João de Barro	
| starring =
| music = 
| cinematography = Edgar Brasil A.P. Castro Luiz de Barros Ramon García Antonio Medeiros Fausto Muniz	
| editing =   
| studio = Cinédia 
| distributor = 
| released = February 4, 1935
| runtime = 
| country = Brazil Portuguese
| budget =
| gross =
| website =
}}

Hello, Hello Brazil! (Portuguese: Allô, Allô, Brasil!) is a Brazilian musical film written by Alberto Ribeiro and João de Barro, with directed by Wallace Downey and Adhemar Gonzaga, and released in 1935. The film tells the participation of Carmen Miranda. 

== Production ==
Wallace Downey began producing copies of successful musical films-Americans with Brazilian artists, many of whom were already famous artists from radio, such as Carmen Miranda, who starred this movie of 1935. A co-production between Waldown Filmes and Cinédia, Allô, Allô, Brasil! presented a multitude of singers, comedians and radio presenters, like the singers Francisco Alves and Mário Reis.

The close ties with the world of radio also manifested in the films storyline, written by popular composers duo João de Barros and Alberto Ribeiro, showing the adventures of a "radiomaníaco" who falls for a singer radio nonexistent.

The two genres of music synonymous with the carnival, including the  , had a prominent place in the first Brazilian musical movies and popular movies. 

== Cast ==
 
*Almirante		
*Ary Barroso	
*Aurora Miranda		
*Carmen Miranda		
*Adhemar Gonzaga		
*César Ladeira		
*Virgínia Lane	
*Francisco Alves
*Mário Reis			
*Ivo Astolphi ... as Bando da Lua
*Dircinha Batista		
*Simão Boutman	
*Sílvio Caldas		
*Chico	Chico		
*Apolo Correia	
*Elisa Coelho de Almeida	
 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 