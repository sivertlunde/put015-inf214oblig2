Round Eyes in the Middle Kingdom
 
Round Eyes in the Middle Kingdom is a 1995 documentary directed by Ronald Levaco, an American filmmaker who journeyed back to China, the nation of his boyhood days, to discover what became of an old friend of his family, Israel Epstein.

==Summary==
Ronald Levaco seeks the reasoning behind the answer to this question: why would a Jew from Poland fight for Communist rule in China under the direction of Mao Zedong?

Round Eyes in the Middle Kingdom allows viewers to meet a man who experienced life directly under Mao’s rule and lived through the  , put their offspring up for sale and perish due to severe lack of food. These catastrophes would trouble him throughout his life and help him to define his choice of politics.

Most Westerners fled China before the revolution reached their area of residence. The Levacoes family decided to leave during the upheaval. But Epstein plans did not include immigrating. Israel Epstein had a deep bond to China and its residents, and stayed behind to help contribute his talents to a government that pledged to look after its people. Mao is portrayed sympathetically in the film, explaining the controversial leader’s charismatic appeal. Epstein sees the historical figure of Mao as a Robin Hood type, whose group always had sufficient provisions for each and every one of its members. It is understandable how a young idealist would make a connection to such a leader. And despite being imprisoned in solitary confinement for sixty months under suspicion of plotting against Zhou Enlai; Epstein did not deter from his continued belief in Mao’s China.

The imprisonment was an ironic occurrence for Epstein, because China couldn’t have had a more loyal citizen. Locked away in an extremely small cell with little openings for windows, Epstein was not only put away from the other prisoners, into total isolation but also had no private space. Israel Epstein proves to be an individual of rare patience and endurance. He describes how he’s come to accept the ill treatment he endured behind bars. The resentment that someone could do this awful thing to you then fades away, he explains.

Round Eyes derives its title from the Chinese nickname for Westerners. While the name refers directly to the sunglasses foreigners wear and the lack of the epicanthic fold in many Europeans, it is also an innuendo, metaphorically, at the naiveté of the outsiders who only through a  haze saw the nation they were living in. Most westerners lived lavishly, interacted only with one another and fled a when lack of provisions and political uprising began. Epstein was one of few Europeans that remained.

When Levaco finally catches up with Israel Epstein, he is 73 years old and his political adventures have slowed down slightly. However he still has a strong sense of nationalism and is healthy. He is a Chinese citizen, the father of two adopted children of Chinese heritage and he’s spending the remainder of his years with his Chinese wife. “Why not?” Epstein asks. “There are Chinese Americans. Why not a Caucasian-Chinese?”

==References==

*{{cite web
  | title = Round Eyes In The Middle Kingdom 
  | publisher = Icarus Films 
  | date = May 2008
  | url =http://www.frif.com/new97/round_eye.html
  | accessdate = August 1  }}

*{{cite web
  | title = UNAFF 99: Program Schedule 
  | publisher = United Nations Associations Film Festival 
  | date = August 1999
  | url =http://www.unaff.org/1999/Fround.htm
  | accessdate = August 1 }}

==External links==
* 
* 

 
 
 
 