The Mist (film)
{{Infobox film
| name           = The Mist
| image          = The Mist poster.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Frank Darabont
| producer       = {{Plainlist |
* Frank Darabont
* Martin Shafer
* Liz Glotzer
}}
| screenplay     = Frank Darabont
| based on       =  
| starring       = {{Plainlist |
* Thomas Jane
* Marcia Gay Harden
* Laurie Holden
* Jeffrey DeMunn
* Andre Braugher
* Sam Witwer
* Toby Jones
}}
| music          = Mark Isham
| cinematography = Rohn Schmidt Hunter M. Via
| studio         = {{Plainlist |
* Dimension Films
* Darkwoods Productions
}}
| distributor    = {{Plainlist |
* Metro-Goldwyn-Mayer
* Dimension Films
}}
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         = $18 million 
| gross          = $57,293,715 
}} novella of The Green William Sadler, The Walking Dead actors Jeffrey DeMunn, Laurie Holden, and Melissa McBride.

Darabont began filming The Mist in Shreveport, Louisiana in February 2007. The director revised the ending of the film to be darker than the novellas ending, a change to which King was amenable. He also sought unique creature designs to differentiate them from his creatures in past films. The Mist was commercially released in the United States and Canada on November 21, 2007; it performed well at the box office and received generally positive reviews.

Although a monster movie, the central theme explores what ordinary people will be driven to do under extraordinary circumstances. The plot revolves around members of the small town of Bridgton, Maine who, after a severe thunderstorm causes the power to go out the night before, meet in a supermarket to pick up supplies. While they struggle to survive an unnatural mist which envelops the town and conceals vicious, otherworldly monsters, extreme tensions rise among the survivors.

==Plot==
The morning after a violent thunderstorm, David Drayton (Thomas Jane), a graphic artist, and his wife Stephanie (Kelly Collins Lintz) check the damage. They find a large tree planted by Davids grandfather has fallen on the house and another tree belonging to his next door neighbor Brent Norton (Andre Braugher) has demolished their boat-house and landing pier. David goes to ask the neighbor for his insurance details to pay for the damage to the boat-house. As he is leaving the lakeside, he and Stephanie notice a strange mist floating across the surface of the lake towards their property.

David decides to go to the local grocery store to buy supplies, bringing his eight-year-old son Billy (Nathan Gamble) and Norton along. On the way, they see a convoy of military trucks. When they arrive at the store, they find it crowded with people who are also recovering from the storm. As the towns tornado sirens go off, a panicked man with a bloody nose, Dan Miller (Jeffrey DeMunn), runs into the store warning of something dangerous in the oncoming mist. Shortly after, the mist envelops the store, making it impossible to see outside, and a violent, earthquake-like tremor hits.
 Chris Owen) volunteers to go outside and unplug the vent, but as he steps outside, he is snatched and devoured by an unseen monster, despite the efforts of David and assistant store manager Ollie Weeks (Toby Jones) to save him.

Upon returning to the main store, Norton doesnt believe their claims and is certain that David is playing a joke on him and the other men are backing him up, as pay-back for the lawsuit Norton filed against David last year. Later, Norton and several others head outside for help, only to be killed by an unseen creature, prompting the rest of the survivors barricade the shop-front windows. At night, enormous flying scorpion-like insects land on the windows and pterodactyl-like animals devour them, eventually causing the glass to break and allowing the insects to enter the store, killing two and badly injuring one. One of the insects lands on Mrs. Carmody, but she is spared when she prays. As a result, she starts preaching and quickly gains followers among the distraught survivors.
 other dimensions, and the scientists responsible for the experiment may have inadvertently opened a doorway into a dimension containing the creatures that are now invading the town. Mrs. Carmody convinces her followers that it is Jessups fault and he is repeatedly stabbed until Mrs. Carmody tells them to feed him to the creatures. He is then thrown out of the store, grabbed and devoured by a large praying mantis-like creature.

David and a handful of other survivors secretly gather supplies to flee. The next morning, however, they are intercepted by Mrs. Carmody, who destroys the supplies and attempts to have David and his group sacrificed, but Ollie shoots her twice with Amanda Dumfries (Laurie Holden) gun, killing her and forcing her horrified followers to stand down and allow Davids group to leave. As the group runs to Davids car, Ollie and two others are killed by the creatures and one runs back to the market in a panic. The remaining members of Davids group, consisting of David, Billy, Dan, Amanda, and Irene (Frances Sternhagen), make it to the car and retrieve Amandas gun.

Driving through the mist, David returns home to find his house destroyed and his wife dead. Heartbroken, he drives the group south, seeing destruction and a gigantic multi-legged, tentacled beast. When they run out of gas hours later, the group decides there is no point in going on. With four bullets left in Amandas gun and five people in the car, David shoots the others rather than have them suffer from the beasts. Distraught and determined to die, David gets out of the car to sacrifice himself to the monsters. However, the mist recedes, revealing that the U.S. Army has killed the monsters and rescued whatever survivors of the disaster. Among the survivors is the woman who left the store at the phenomenons onset, accompanied by her two children who she left to save. Realizing that they were only moments from being rescued and had been driving away from help the entire time, David falls to his knees, screaming in anguish, while two soldiers look on in confusion.

==Cast==
*Thomas Jane as David Drayton, a commercial painter and film-poster artist who is trapped in the market with his young son Billy.
*Marcia Gay Harden as Mrs. Carmody, a fanatically religious local woman.
*Laurie Holden as Amanda Dumfries, a young, married teacher whose husband is away.
*Jeffrey DeMunn as Dan Miller, a normal civilian who is the first to see the signs of danger from the mist.
*Andre Braugher as Brent Norton, Davids neighbor and a successful New York attorney who filed a lawsuit against David in the past year and lost.
*Toby Jones as Ollie Weeks, the assistant manager of the supermarket. William Sadler as Jim Grondin, a weak minded local mechanic.
*Frances Sternhagen as Irene Reppler, a third-grade elementary school teacher. Despite being elderly, she is very tough, competent and is always calm and collected.
*Nathan Gamble as Billy Drayton, Davids eight-year-old son.
*Alexa Davalos as Sally, a cashier at the market who is Billys babysitter. Chris Owen as Norm, a bag-boy.
*Samuel Witwer as Private Wayne Jessup, a local soldier trapped in the market.
*Robert Treveiler as Bud Brown, the supermarket manager.
*David Jensen as Myron LaFleur, a local mechanic.
*Melissa McBride as Woman With Kids at Home
*Buck Taylor as Ambrose Cornell, an elderly man who sides with Davids group.
*Brian Libby as Biker
*Juan Gabriel Pareja as Morales
*Kelly Collins Lintz as Stephanie "Steff" Drayton, Davids wife.
*Ron Clinton Smith as Mr. Mackey, the store butcher.

==Production==

===Development=== Dark Forces 1999 film adaptation of Stephen Kings The Green Mile (novel)|The Green Mile.   Darabont eventually set up a first look deal for The Mist with Paramount Pictures, having been entrusted feature film rights by Stephen King.   By December 2004, Darabont said that he had begun writing an adapted screenplay for The Mist,   and by October 2006, the project moved from Paramount to Dimension Films, with Darabont attached to direct and actor Thomas Jane in negotiations to join the cast. 

===Writing===
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "The story is less about the monsters outside than about the monsters inside, the people youre stuck with, your friends and neighbors breaking under the strain."
|-
| style="text-align: right;" | — Darabont on The Mist 
|}
Director Darabont chose to film The Mist after filming the "straighter dramas" The Shawshank Redemption and The Green Mile because he "wanted to make a very direct, muscular kind of film." Darabont conceived of a new ending in translating the novella for the big screen. Author King praised Darabonts new ending, describing it as one that would be unsettling for studios.  King said, "The ending is such a jolt—wham! Its frightening. But people who go to see a horror movie dont necessarily want to be sent out with a Pollyanna ending." 
 The Twilight Zone episode "The Monsters Are Due on Maple Street" and the 1944 film Lifeboat (film)|Lifeboat. 
 what refugees experienced at the Louisiana Superdome during Hurricane Katrina. 

While the origin of the mist is never explained in great details in the movie, Frank Darabont did write an opening scene in a draft dated 5 August 2005, in which the thunderstorm causes a malfunction at the Arrowhead Projects lab that allows the portal to another dimension to stay open too long. The scene was never filmed.

===Filming===
In December 2006, Jane finalized negotiations with the studio to join the cast.   In January 2007, actors Andre Braugher and Laurie Holden joined Jane for the cast of The Mist.     Production began the following February at StageWorks of Louisiana, a sound stage and movie production facility in Shreveport, Louisiana.   Marcia Gay Harden and Toby Jones joined the cast later in the month. 
 William Sadler, The Green Mile, were cast in supporting roles. Sadler had previously played Janes role, David Drayton, in a 1986 audiobook version of The Mist. Darabont wanted to cast King in the supporting role that eventually went to Brian Libby, an offer that King turned down because he did not want to travel to film the part. 
 documentary kind digitally but grainy effect. 
 Dark Tower The Green The Thing, and Guillermo Del Toros Pans Labyrinth, paying a tribute to him.

Darabont collaborated with the production designer to create a mix of eras to avoid appearing as a period piece but also not looking so contemporary. Cell phones were used by characters in The Mist, but the military police in the film did not dress in modern attire. While an MP also drove an old Jeep instead of a Humvee, other cars seen in the film are modern models.  The city police cars in the beginning of the film are a 1987 Chevrolet Caprice and a 1988 Ford LTD Crown Victoria, cars that were standard police vehicles in the late 1980s but have not been used in force since the late 1990s.
 extras from Shreveport, Louisiana were included in The Mist. Unlike conventional application of extras in the background of a film, sixty of the hundred extras were interwoven with the films ensemble cast.  Additional elements giving the film a local flavor include the prominence of local Louisiana brands such as Zapps potato chips. Exterior shots of the house at the beginning were in Shreveport.  Exterior shots of the supermarket were in Vivian, Louisiana. Also, if looked closely at, the shields on the side of the passing firetrucks early in the film identify them as part of the Caddo Parish fire department. This is possibly a mistake as the film is allegedly set in Maine.

===Music===
Darabont chose to use music to minimal effect in The Mist in order to capture the "heavier feel" of the darker ending  he had written to replace the one from the novella. The director explained, "Sometimes movie music feels false. Ive always felt that silent can be scarier than loud, a whisper more frightening than a bang, and we wanted to create a balance. We kept music to a minimum to keep that vérité, documentary feel."  Darabont chose to overlay the song "The Host of Seraphim" by the band Dead Can Dance, a spiritual piece characterized by wailing and chanting. As a fan of Dead Can Dance, Darabont thought that the song played "as a requiem mass for the human race."  The original score was composed by Academy Award-nominated composer Mark Isham.

===Effects===
Darabont hired artists Jordu Schell  and Bernie Wrightson to assist in designing the creatures for the film.   Greg Nicotero worked on the films creature design and make-up effects, while Everett Burrell served as the visual effects supervisor. Nicotero initially sketched out ideas for creature design when Darabont originally expressed interest in filming The Mist in the 1980s. When the project was greenlit, Nicotero, Burrell, and Darabont collaborated about the creature design at round-table meetings at CaféFX.  The studio for visual effects had been recommended to Darabont by Guillermo del Toro after Darabont asked the director who created the visual effects for Pans Labyrinth.
 motion capture dots during filming.   

==Release== The Green Mile. 

===Critical reception===
The film has received positive reviews from critics. On the film review aggregate website Rotten Tomatoes, The Mist received a 73% approval rating, based on 141 reviews, with an average rating of 6.6/10, indicating generally favourable reviews.  On the website Metacritic, the film has received a metascore of 58 out of 100 based on 29 reviews. 
 Green Mile track record, you will be sadly mistaken." 

Bloody Disgusting ranked the film #4 on their list of the Top 20 Horror Films of the Decade, with the article saying "The scary stuff works extremely well, but what really drives this one home is Darabont’s focus on the divide that forms between two factions of the townspeople – the paranoid, Bible-thumping types and the more rational-minded, decidedly left-wing members of the populace. This allegorical microcosm of Bush Jr.-era America is spot on, and elevates an already-excellent film to even greater heights." 

===Box office===
The film was commercially released in the United States and Canada on November 21, 2007.   Over the opening weekend in the United States and Canada, The Mist grossed $8,931,973.  As of August 9, 2009, the film grossed $25,593,755 in the United States and Canada and $27,560,960 in other territories for a worldwide total of $57,289,103.   

===Home media===
The Mist was released on DVD and Blu-ray Disc|Blu-ray on March 25, 2008. The single-disc includes an audio commentary by writer/director Frank Darabont, eight deleted scenes with optional commentary, and "A Conversation With Stephen King and Frank Darabont" featurette.

The two-disc edition includes an exclusive black-and-white presentation of the film, as well as the color version, and five featurettes ("When Darkness Came: The Making of The Mist", "Taming the Beast: Shooting Scene 35", "Monsters Among Us: A Look at the Creature FX", "The Horror of It All: The Visual FX of The Mist", and "Drew Struzan: Appreciation of an Artist").

==TV series==
In November 2013, Bob Weinstein revealed that he and Darabont were developing a 10-part television series based on the film. 

==See also==
* The Fog (novel), a 1975 novel by James Herbert

==References==
 

==External links==
 
* 
* 
* 
* 
* 
* 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 