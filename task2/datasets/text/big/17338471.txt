Art of the Devil
{{Infobox film
| name           = Art of the Devil
| image          = Artofthedevilposter.jpg
| caption        = Theatrical poster.
| director       = Tanit Jitnukul
| producer       = Charoen Iamphungporn
| writer         = Ghost Gypsy
| starring       = Supakson Chaimongkol Tin Settachoke
| music          =
| cinematography = Tanai Nimchareonpong Thaya Nimcharoenpong
| editing        = Sunij Asavinikul
| distributor    = Five Star Production
| released       = June 17, 2004
| runtime        = 96 minutes
| rating         = 18PL  (Malaysia)  NC-16  (Singapore) 
| country        = Thailand
| awards         =
| language       = Thai
| budget         =
}} 2004 Cinema Thai horror film directed by Tanit Jitnukul. It has two titular sequels, Art of the Devil 2 (2005) and Art of the Devil 3 (2008), but these films feature a different story with new characters.

==Plot==
Art of the Devil tells the story of Boom (Supaksorn Chaimongkol), a young Thai girl who meets  a married man named Prathan (Tin Settachoke) at a country club.  The two soon begin an affair, and Boom finds herself pregnant.  When she breaks the news to Prathan, he appears to settle for giving her a sum of money in exchange for her silence, reassuring her that he will not leave her. However, he then wakes her up in the middle of the night, informing her that for that large an amount of money, he had the right to share her.  While Prathan wields a video camera, his friends chase a terrified and screaming Boom out of the room and onto the beach, where they apparently gang-rape her.

After getting an ultrasound at the hospital, Boom shows up at the restaurant where Prathans daughter is celebrating her birthday and informs him that the sum of money he had given her was not enough.  He pulls her outside and hits her, tossing a wad of cash at her and warning her not to come near his family again.

Furious, Boom enlists the aid of a black magic user to exact revenge on her ex-lover and his entire family, notably causing the eldest son to shoot his girlfriend and his little sister before turning the gun on himself.

After their deaths, Boom visits a temple and finds that if she donates coffins for the spirits, they will not bother her.  She makes some offerings.  While leaving the temple, she sees the ghosts of her victims in the back of a car and steps off of the sidewalk to get a better look, whereupon she is hit by a car.  The accident causes her to lose her baby.

Prathans first wife inherits his fortune. She and her four children move in to the house. Boom again uses black magic to kill this new family off.  However, her motive this time is not for revenge, but in order to claim the inheritance. A young newspaper reporter becomes suspicious, so Boom arranges for his death, as well. Throughout this, the ghost of Booms dead daughter is seen around the house.

The story ends with only the youngest son and eldest daughter surviving the massacre. Boom voluntarily falls to her death from the roof of the hospital after seeing her daughters ghost.

==Cast==
* Arisa Wills as Nan
* Supakson Chaimongkol as Boom
* Krongthong Rachatawan as Kamala
* Tin Settachoke as Prathan
* Somchai Satuthum as Danai
* Isara Ochakul as Ruj
* Nirut Sutchart as Neng
* Krittayod Thimnate as Bon

==Reception== Around the Harry Potter The Punisher. It stayed in the No. 4 spot for two more weeks before moving to No. 5 in its fourth week at the box office. 

==See also==
*List of ghost films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 