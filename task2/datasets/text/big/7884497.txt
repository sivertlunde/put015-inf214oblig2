Dr. Jekyll and Mr. Hyde (1913 film)
{{Infobox film 
| name           = Dr. Jekyll and Mr. Hyde 
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Herbert Brenon 
| producer       = 
| writer         = 
| screenplay     = Herbert Brenon
| based on       =  
| narrator       = 
| starring       = King Baggot Jane Gail Matt B. Snyder Howard Crampton
| music          = 
| cinematography = 
| editing        =  Independent Moving Pictures Co. of America
| distributor    = The Universal Film Manufacturing Company, Incorporated
| released       = 1913
| runtime        = Two reels (26 minutes)
| country        = 
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = }}

Dr. Jekyll and Mr. Hyde is a 1913 horror film, directed by Herbert Brenon and Carl Laemmle, written by Brenon and produced by Laemmle. The picture is based on the Robert Louis Stevenson story The Strange Case of Dr. Jekyll and Mr. Hyde and stars King Baggot in the dual role of Jekyll and Hyde. The film was re-released in the US in August 1927.  

Like so many other performers of this period, it was standard practice for the actors to apply their own make-up, and while assuming the dual role of Jekyll and Hyde, King Baggot employed a variety of different greasepaints and a tangled mass of crepe hair. Through a series of camera dissolves Baggot was able to achieve the transformation. This is the only version in which Jekyll almost discovers an antidote.

== Plot ==
Dr. Henry Jekyll (King Baggot) sends a note to his fiancée, Alice (Jane Gail), and her father (Matt B. Snyder) to say that instead of accompanying them to the opera, he must give more time to his charity patients. At Jekyll’s practice, his friends Dr. Lanyon (Howard Crampton) and Utterson (William Sorrel), a lawyer, ridicule him for what they consider his dangerous research. Alice and her father also visit Jekyll’s rooms, but although apologetic, the doctor insists on devoting his time to his patients. That night, however, Jekyll undertakes a dangerous experiment, swallowing a drug intended to releases his evil self. His body convulses, and he transforms into a hunched, twisted figure.

The strange creature emerges from Jekyll’s room, bearing a note in Jekyll’s handwriting that orders the household staff to treat the stranger – “Mr Hyde” – as himself. Hyde then slips out into the night, terrorising the patrons of a nearby tavern before finding himself lodgings. From these rooms he begins a career or evil, until one night he attacks and injures a crippled child. Outraged witnesses corner Hyde and force him to agree to compensate the boy. Hyde reluctantly leads one man back to Jekyll’s house and gives him money. During this passage of events, a worried Dr. Utterson sees Hyde entering Jekyll’s house. Inside, Hyde takes a potion that transforms him back to Jekyll.

The doctor swears that he will abandon his experiments and never tempt fate again; but that night, without taking the drug, he turns spontaneously into Hyde.

==Cast==
* King Baggot as Dr. Henry Jekyll/Mr. Hyde
* Jane Gail as Alice
* Matt B. Snyder as Alices father
* Howard Crampton as Dr. Lanyon
* William Sorelle as Utterson, the attorney

==References==
 

==External links==
* 
* 


 
 

 
 
 
 
 
 
 
 
 
 