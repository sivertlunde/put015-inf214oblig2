Lights Out (film)
{{Infobox film
| name           = Lights Out
| image          = LLB-posterdef.jpg
| image_size     =
| caption        = Official US teaser poster
| director       = Fabrice Gobert
| producer       =  
| writer         = Fabrice Gobert
| narrator       = 
| starring       =  
| music          = Sonic Youth
| cinematography = Agnès Godard
| editing        = Fabrice Gobert
| studio         = 2.4.7. Films
| distributor    = TF1 International
| released       =  
| runtime        = 
| country        = France
| language       = French
| budget         = 
}}
Lights Out ( ) is a French thriller film directed by Fabrice Gobert and starring Jules Pelissier, Ana Girardot, Arthur Mazet, Laurent Delbecque, Serge Riaboukine and Laurent Capelluto. 

== Cast ==
* Jules Pelissier as Jérémie Legrand
* Ana Girardot as Alice Cartier
* Arthur Mazet as Jean-Baptiste Rabier
* Laurent Delbecque as Simon Werner
* Yan Tassin as Frédéric 
* Esteban Carvajal Alegria as Luc
* Audrey Bastien as Clara
* Laurent Capelluto as Yves
* Serge Riaboukine as Jean-Baptiste Rabiers Father

== Soundtrack ==
{{Infobox album  
| Name        = SYR9: Simon Werner a disparu
| Type        = Soundtrack
| Artist      = Sonic Youth
| Cover       = SYR9_Simon_Werner_a_Disparu.jpg
| Released    = February 15, 2011
| Recorded    =
| Genre       = Soundtrack
| Length      = 58:08
| Label       = Sonic Youth Recordings
| Producer    =
| Chronology  = SYR series
| Last album  =   (2008)
| This album  = SYR9: Simon Werner a Disparu (2011)
}}

{{Album ratings
| MC = (76/100)   
| rev1 = Allmusic
| rev1Score =   
| rev2 = BBC Music
| rev2Score = (favorable) 
| rev3 = Drowned in Sound
| rev3Score = (6/10) 
| rev4 = Dusted Magazine
| rev4Score = (favorable) 
| rev5 = Mojo (magazine)|Mojo
| rev5Score =   
| rev6 = No Ripcord
| rev6Score =   
| rev7 = Pitchfork Media
| rev7Score = (6.0/10) 
| rev8 = PopMatters
| rev8Score =   
| rev9 = Record Collector
| rev9Score =   
| rev10 = Sputnikmusic
| rev10Score = (3/5) 
}}

The soundtrack was written and performed by the US noise rock band Sonic Youth.  It was released as part of the Sonic Youth Recordings series, titled as SYR9: Simon Werner a Disparu. It is notable for being the last release of new music from the band prior to their indefinite hiatus which was announced in 2012. 

=== Track listing ===
{{Track listing
| all_writing = Sonic Youth
| title1 = Thème De Jérémie
| length2 = 4:30
| title2 = Alice Et Simon
| length2 = 2:38
| title3 = Les Anges au Piano
| length3 = 3:31
| title4 = Chez Yves (Alice et Clara)
| length4 = 3:33
| title5 = Jean-Baptiste à La Fenêtre
| length5 = 3:04
| title6 = Thème de Laetitia
| length6 = 6:01
| title7 = Escapades
| length7 = 3:04
| title8 = La Cabane Au Zodiac
| length8 = 2:08
| title9 = Dans Les Bois/M. Rabier
| length9 = 5:50
| title10 = Jean-Baptiste et Laetitia
| length10 = 1:17
| title11 = Thème De Simon
| length11 = 3:52
| title12 = Au Café
| length12 = 5:33
| title13 = Theme DAlice
| length13 = 13:08
}}

== Release ==
The film was entered into the Un Certain Regard section of the 2010 Cannes Film Festival.   

== Reception ==
Lee Marshall of Screen Daily wrote, "This genre-bending high-school thriller-drama about three students who go missing at a French lycee opens like a teen B-movie, but soon moves into more intriguing territory."   Jordan Mintzer of Variety (magazine)|Variety called it "a deftly realized teen thriller" that is too similar to Elephant (2003 film)|Elephant. 

== References ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 