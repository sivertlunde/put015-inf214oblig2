Drew: The Man Behind the Poster
{{Infobox film
| name           = Drew: The Man Behind The Poster
| image          = Drew-Struzan-the-man-behind-the-poster.jpg
| caption        = 
| director       = Erik Sharkey
| producer       = Greg Boas Rick Law Charles Ricciardi Erik Sharkey
| writer         = 
| starring       = Drew Struzan Dylan Struzan Harrison Ford Guillermo del Toro George Lucas Michael J. Fox
| music          = Ryan Shore
| cinematography =  Kino Lorber
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
}}
Drew: The Man Behind The Poster is a 2013 documentary film directed by Erik Sharkey about the career of American film poster artist Drew Struzan. It debuted July 19, 2013 at the San Diego Comic-Con International.

==Synopsis== Star Wars, The Thing, Indiana Jones Harry Potter franchise. 

The film delves into Struzans art for  , and Pans Labyrinth, though greatly admired by del Toro, was rejected by the studio in favour of more photographic posters.  This leads the documentary in a direction about how film studios are interested more in profit rather than quality, how film posters are currently more often photographs of the actors rather than paintings of them.

The documentary concludes with footage of Struzan at the San Diego Comic-Con International in 2010, wherein he receives the Inkpot Award for his illustrations.  This is then followed by footage of Struzan talking about and showing his personal paintings not related to film, and how he spends his time painting not for someone else, but now for himself, and how he often plays outside with grandson, Nico.

==Critical reception==
The documentary has received mixed to positive reviews with a rating of 50% on review aggregator website Rotten Tomatoes, based on 8 reviews. 

==Awards==
Best Feature Film at the 2014 Dragon Con Independent Film Festival  

Best Documentary at the 2014 Ridgewood Guild Film Festival  

Nominated for Best Documentary Feature at the 2013 Hollywood Film Festival  

==References==
 

==External links==
*  
*   at Rotten Tomatoes
*   at Metacritic

 
 
 
 