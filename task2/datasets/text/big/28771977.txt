This Ain't Avatar XXX
{{Infobox film
| name           = This Aint Avatar XXX
| image          = This Aint Avatar.jpg  
| image_size     = 215px
| alt            =  
| caption        = Cover
| director       = Axel Braun
| producer       = Larry Flynt
| writer         = Marc Star
| starring       = Evan Stone Chris Johnson Nicki Hunter Danica Dillan Eric Swiss Misty Stone Lexington Steele Chanel Preston Juelz Ventura
| cinematography = Axel Braun
| editing        = Axel Braun Claudia Ross
| distributor    = Hustler Video
| released       =  
| runtime        = 129 minutes
| country        = United States
| language       = English
| budget         = Most expensive film Hustler produced. 
}} 3D pornographic film that parodies James Camerons Avatar (2009 film)|Avatar. The film was shot, edited, and directed by Axel Braun, and stars an ensemble cast headed by Chris Johnson as Jake, the main human character. It was produced by Hustler Video.    Industry reviewers noted that the release used old-atyle rather than modern 3D technology, and faulted it for its poor production quality.

It is the most expensive film Hustler Video has ever produced.         

==Plot==
After the original events of Avatar, Jake reveals in a video log that the Nabi has a darker side. After sexual flashbacks which show moments in the film which purport to show what actually happened, the film shows what occurs after the human corporation leaves Pandhora. The Nabi turn out to be "fetish-fueled sex fiends", who have a massive orgy after the corporation leaves, and then reveal their true purpose for keeping humans in a twist ending. 

==Cast==
* Evan Stone as Quaritch
* Chris Johnson as Jake
* Nicki Hunter as Grace
* Danica Dillan as Trudy
* Eric Swiss as Norm
* Misty Stone as Neytiri
* Lexington Steele as Tsutey
* Chanel Preston as Moat
* Juelz Ventura as Human slave
* Alec Knight as Eytukan

Chris Johnson was chosen as "Jake", which was based on the main character from the original Avatar film. Nicki Hunter played "Grace", Jakes human guide to the Nabi. Eric Swiss was cast as "Norm", another human explorer of Panwhora. Misty Stone was slated to play Jakes Nabi love interest, and Lexington Steele and Chanel Preston were both cast as leaders of the Nabi tribe. 

==Production== pornography parody films were originally proposed as a joke by Axel Braun, the films eventual director.  Hustler then published a press release with a series of fake parody films (including Glee (TV series)|Glee and FOX News); one of the films listed was an Avatar parody, and the producers at Hustler and Braun began serious discussions about the creation of an Avatar porn film.  In March 2010, Hustler announced that Braun would be officially directing the film.   

Marc Star was tapped by Hustler to write the screenplay, where he came up with the idea of having the movie take place after the end of the original Avatar, and to have the sex scenes shown through video logs before and during the original film.  In order to avoid confusion with it actually being part of James Camerons Avatar universe, the names of many objects in the universe are subtly changed to parody the original Avatar.  The Pandoran_biosphere#Naʼvi|Navi are referred to as Nabi, the planet Pandora was renamed to Panwhora, and instead of being after unobtainium, the humans are after viagratanium. 

In order to film the 3D sequences, Hustler contacted an outside company with 3D expertise to train their personnel and to lease their equipment.  The director struggled to maneuver the 3D cameras during some of the sequences, but Braun was pleased with the end result.  In order to achieve a look similar to that of the blue Navi from Avatar, makeup artists used gallons of blue paint on the Nabi actresses in order to achieve a close look to that of the film.  Prosthetics were attached to the performers faces to simulate their alien nature.  The film finished production in late June 2010. 
 trailer for this films unique texture and 3D effects in September 2010.   

==Reception==
X-Critic reviewer Don Houston noted that the video "used the old fashioned red & blue anaglyph system", falling "far short of what fans should expect". He faulted the standard version as "so dark that even trying to discern what was happening was sketchy" and the 3D version for going "haywire", with the red- and blue-tinted components falling out of synchronization partway through the video, "killing the effect". He gave This Aint Avatar XXX a highly negative review, describing it as "was cheaply made and written on the back of a matchbook cover", and its "technical aspects" as "poorly handled". 

Reviewer "JLB", a writer/editor for the Internet Adult Film Database|IAFD, declared himself "disappointed" in the release, saying it "feels like a lazy, truncated version of James Cameron’s hugely popular film." He also was highly negative about the videos technical quality, saying "the 3D version is just the old fashioned red and blue anaglyph system, and it comes unglued at the midway point – probably a by-product of Braun’s inability to properly shoot using 3D equipment" and that the standard version "is so dark that it almost completely obfuscates the action". 

==Home media==
The film was released on DVD and Blu-ray Disc|Blu-ray on September 28, 2010  and is believed to be "the first ever adult movie made specifically for 3-D televisions." 

==Awards==
* 2010 Venus Award - Special Jury Award Movie and Film     2011 AVN Award - Best 3D Release   
* 2011 XBIZ Awards - Best Art Direction   
* 2011 XBIZ Awards - Marketing Campaign of the Year 

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 