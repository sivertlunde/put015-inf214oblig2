Nerariyum Nerathu
{{Infobox film
| name           = Nerariyum Nerathu
| image          = NerariyumNerathu.png
| caption        =
| director       = Salaam Chembazhanthy
| producer       = CG Bhaskaran for Sahrudaya Chithra
| writer         = Ezhacheri Ramachandran (Story) Pappanamkodu Lakshmanan (Screenplay) Shankar Ratheesh Rohini
| Johnson
| cinematography = VC Sasi S.I.C.A.
| editing        = K Sankunni
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         = 35 Lakhs
| gross          =
}}
 1985 Cinema Indian feature directed by Shankar and Rohini playing other important roles.                                                                                                                                                  

== Cast ==

*Prem Nazir Shankar
*Ratheesh
*T. G. Ravi
*Unnimary Rohini
*Jagathy Sreekumar
*Kuthiravattam Pappu
*Bindhu Ghosh
*Lalithasree Anuradha

==Soundtrack== Johnson and lyrics was written by Ezhacheri Ramachandran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Orupaadu Swapnangal || K. J. Yesudas, S Janaki || Ezhacheri Ramachandran || 
|-
| 2 || Panchaara Panchaayathil || CO Anto, Krishnachandran || Ezhacheri Ramachandran || 
|-
| 3 || Premakala Devathamarude || S Janaki || Ezhacheri Ramachandran || 
|}

==References==
 

==External links==
*   

 
 
 
 


 