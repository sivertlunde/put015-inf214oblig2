Little Buddha
{{Infobox film
| name = Little Buddha
| image = Little_buddha_imp.jpg
| caption = Theatrical release poster
| director = Bernardo Bertolucci
| screenplay = Rudy Wurlitzer Mark Peploe
| story = Bernardo Bertolucci
| starring = Keanu Reeves Bridget Fonda Chris Isaak Ruocheng Ying Rudraprasad Sengupta
| producer = Jeremy Thomas
| music = Ryuichi Sakamoto
| cinematography = Vittorio Storaro
| editing = Pietro Scalia
| studio = Recorded Picture Company Ciby 2000
| distributor = Miramax Films
| released =  
| country = Italy France United Kingdom
| runtime = 140 min.
| language = English
| budget =
| gross = $4,858,139 (USA)
}} British drama East after The Last Emperor.

== Plot ==
Tibetan Buddhist monks from a monastery in  . The monks come to Seattle in order to meet the boy.

Jesse is fascinated with the monks and their way of life, but his parents, Dean (Chris Isaak) and Lisa (Bridget Fonda), are wary, and that wariness turns into near-hostility when Norbu announces that he wants to take Jesse back with him to Bhutan to be tested. Dean changes his mind however, when one of his close friends and colleagues commits suicide because he went broke. Dean then decides to travel to Bhutan with Jesse. In Nepal, two children who are also candidates for the rebirth are encountered, Raju (Rajuh Lal) and Gita (Greishma Makar Singh).

Gradually, over the course of the movie, first Jesses mother and then Lama Norbu tell the life story of Prince Siddhartha, reading from a book that Lama Norbu has given to Jesse. 
 Siddhartha (Keanu Mara (a enlightenment and becomes the Buddha.

In the final scenes of the movie, it is found that all three children are rebirths of Lama Dorje, separate manifestations of his body (Raju), speech (Gita), and mind (Jesse). A ceremony is held and Jesses father also learns some of the essential truths of Buddhism. His work finished, Lama Norbu enters a deep state of meditation and dies. As the funeral ceremony begins, Lama Norbu speaks to the children, seemingly from a higher plane, telling them to have compassion; and just before the credits roll the children are seen distributing his ashes.

At the very end of the film credits, the sand mandala that was seen being constructed during the movie is destroyed, "with one swift stroke."

== Cast ==
* Bridget Fonda ... Lisa Conrad
* Keanu Reeves ... Siddhartha
* Chris Isaak ... Dean Conrad
* Ruocheng Ying ... Lama Norbu
* Alex Wiesendanger ... Jesse Conrad
* Raju Lal ... Raju
* Greishma Makar Singh ... Gita
* Sogyal Rinpoche ... Kenpo Tenzin
* Ven. Khyongla Rato Rinpoche ... Abbot
* Ven. Geshe Tsultim Gyelsen ... Lama Dorje
* Jo Champa ... Maria
* Jigme Kunsang ... Champa
* Thubtem Jampa ... Punzo
* Surekha Sikri ... Sonali (as Surehka Sikri)
* T.K. Lama ... Sangay
* Doma ... Ani la

== Production == 65 mm anamorphic Technovision.

Jeremy Thomas later remembered making the film:

  }}
 The Cup (1999) and Travelers and Magicians (2003). 

In addition to Kathmandu, another prominent Nepalese location used in the film was the city of Bhaktapur.   

=== Casting of Tibetan lamas ===
Three Tibetan incarnate lamas have roles in the film. Sogyal Rinpoche appears in the earlier segments in the role of Khenpo Tenzin. The Venerable Khyongla Rato Rinpoche plays the part of the Abbot of the monastery in Bhutan. Dzongsar Jamyang Khyentse Rinpoche appears near the end of the film, when Lama Norbu is shown meditating overnight. Khyentse Rinpoche also served as a consultant to Bertolucci. In a later documentary about Khyentse Rinpoche entitled Words of my Perfect Teacher, his role in the film is discussed along with a short interview with Bertolucci.

== Themes ==
The color schemes used in the movie are red-orange for Eastern locations, and blue-gray for Western locations. Jesse and his father are first presented in the red-orange scheme during their plane flight to Bhutan.

An unusual plot technique is later used through the final stages of the flashback sequences where the past gets merged with the present as the three children, Jesse, Raju and Gita find themselves actually in the scene with Prince Siddhartha, watching him as he is tempted by and overcomes egoic Mara (demon)|Mara.

== Soundtrack ==
{{Infobox album  
| Name        = Little Buddha
| Type        = soundtrack
| Artist      = Ryuichi Sakamoto
| Cover       = LittleBuddha.jpg
| Length      =
| Recorded    =
| Released    = 14 June 1994 Classical
| Label       = Milan Records
| Producer    =
}}
The soundtrack for the film was entirely composed by Japanese pianist/composer Ryuichi Sakamoto.

=== Track listing ===
All compositions are conducted by Sakamoto.

# "Main Theme" 2:50
# "Opening Titles" 1:47
# "The First Meeting" 1:50
# "Raga Kirvani" 1:28
# "Nepalese Caravan" 3:01
# "Victory" 1:45
# "Faraway Song" 3:18
# "Red Dust" 4:38
# "River Ashes" 2:25
# "Exodus" 2:33
# "Evans Funeral" 4:28
# "The Middle Way" 1:50
# "Shruti Sadolikar - Raga Naiki Kanhra - The Trial" 5:25
# "Enlightenment" 4:28
# "The Reincarnation" 1:52
# "Gompa - Heart Sutra"	2:38
# "Acceptance - End Credits" 8:57

== Reception ==
The film received mixed to positive reviews, as it currently holds a 65% rating on Rotten Tomatoes.     It was nominated for one Razzie Award, Worst New Star for Chris Isaak. The film however was very successful in France, where it was the 19th highest grossing film of the year, with 1,359,483 admissions sold. 

== Quotes ==
* "O, trickster; phantom of my own ego, you are pure illusion. You, self, do not exist. The earth is my witness to this Supreme Enlightenment." - Siddharta to Mara, the demon of the illusion of an existence of self.

==See also==
*Depictions of Gautama Buddha in film

== References ==
 

== External links ==
*  
*  
*  

 
 


 
 
 
 
 
 
 
 
 
 
 
 
 
 
 