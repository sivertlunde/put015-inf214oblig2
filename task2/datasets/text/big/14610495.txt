2 Girls 1 Cup
 
 
{{Infobox film
| name = Hungry Bitches
| image = Hungry_bitches.jpg
| image_size = 
| caption = DVD cover
| director = Marco Antônio Fiorito (under the pseudonym Marco Villanova) 
| producer = MFX Media
| starring = Karla & Latifa (stage names)
| music = Hervé Roy 
| cinematography = Roger Wu
| editing =
| studio = MFX Video
| distributor = Marco Fiorito
| released = January 5, 2007
| runtime = 62&nbsp;minutes (film) 1&nbsp;minute (trailer)
| country = Brazil  	 Portuguese
}}
 trailer for consuming the vomiting into Delusions of Grandeur, plays throughouts the video. 
 viral and became one of the best known shock videos in itself and for the reactions its graphic content elicited from viewers who had not seen such content before.       Around mid-October 2007, video-sharing sites including YouTube were flooded with videos depicting others reactions to watching the video for the first time.    Many other reaction videos have now appeared for similarly shocking and extreme videos.

==Production==
It is unknown exactly how the graphic scenes in the video were produced. Many people believe that the apparent feces is composed of a mixture of food substances such as chocolate, coffee cream, peanut butter. One theory states that the woman cleaned her bowels before introducing the substance. Her rectum then instantly rejected it, creating the appearance of defecation. Some speculate that the vomit is real, but was regurgitated before reaching the stomach and does not contain any gastric acids. In the video, the majority of the vomit does not enter the mouths. However, some believe that all of these scenes were created with advanced computer graphics. 

==Background==
The video originated from a Brazilian distributor and pornographer Marco Antônio Fiorito (born July 1, 1971 in Sao Paulo),    who describes himself as a "compulsive Sexual fetishism|fetishist".  Fiorito started having interest producing films in 1994, and in 1996, with his wife, Joelma Brito, using her artistic name Letícia Miller, he began a fetish film business  and soon moved on to coprophagia. The film was produced by MFX Video, one of several companies owned by Fiorito. 

Authorities in the United States have branded some of Fioritos films as obscene and filed charges against Danilo Croce, a Brazilian lawyer living in Florida, listed as an officer of a company distributing Fioritos films in the United States. Fiorito explained that had he known that selling his films in the U.S. was illegal, he would have stopped. In his declaration he quotes:

"I would have stopped because the money is not the main reason that I make these films." He then added, "I have already made fetish movies with scat/feces using chocolate instead of feces. Many actors make scat films but they dont agree to eat feces." 
 Department of leaked in the process.      

==Reaction videos==
The spread of 2 Girls 1 Cup has been facilitated by a series of videos depicting people reacting to watching it.     Many videos exist on YouTube of users showing the original video (off-camera) to their friends and filming their reactions, although some may be staged.    Even Joe Rogan, host of Fear Factor, a show notorious for the disgusting things its contestants are dared to eat, had to turn away in a reaction video posted to his blog.  A reaction video starring a Kermit the Frog puppet proved very popular on the community-based website Digg.   
 Violet Blue, an author, described this website as becoming "the new tubgirl and goatse all in one disgusting moment of choco-poo-love" in a San Francisco Chronicle article. 
 The Playhouse. the road."  "Genuine Nerd" Toby Radloff was so disgusted by the clip that he had to immediately watch it again. 

==Impact==
The video has led to many parodies and other shock videos with similar content. One of the most popular so-called "sequels" is "4 Girls Fingerpaint," which consists of four women conducting themselves in similar scat-fetishistic activities.    The title has also contributed to the titles and nicknames of other shock videos, including "2 Girls 1 Finger," "8 Girls No Cup," "1 Guy 1 Jar," "1 Guy 1 Screwdriver," "3 Orangutans 1 Blender," and "1 Girl 1 Cake," the nickname for the 2008 viral shock video "Cake Farts."  A short film by guitarist   and Christian Le Guilloux made a five-minute series called "2 Girls, 1 Cup: The Show" for the short film competition site, Channel 101. It debuted in first place on January 27, 2008.  Brandon Hardesty posted a video called 1 guy 1 lunchable, in which he eats the "taco filling" from a Mini-Taco Lunchable.  Canadian comedian Jon Lajoie also made a song named "2 Girls 1 Cup song", which described the activities in the video as if the two women were expressing their love for each other. The music video was instantly popular, gaining over 10&nbsp;million views on YouTube. 

In 2013 German company MediaMarkt marketed a cupcake maker dubbed "2 Girls 1 Cup-Cake Maker." The products slogan was that it made cupcakes "So good that its impossible to film." 

==Media recognition==
In the media the video has been used as an example of the poor content quality of YouTube and similar video-sharing websites, and their tendency towards deliberately shocking content.   
* The video was featured on VH1s Best Week Ever, where the videos existence and propagation was declared to cause "Moral Bankruptcy" to have the "Best Week Ever!"  Esquire (magazine)|Esquire magazine showed the video to actor George Clooney during an interview, prompting him to compare it to a rodeo, saying the point of the video was to see "how long you can last." 
* In Robot Chicken episode "The Ramblings of Maurice" in the "Chocolate Grain" segment, a cereal company uses a viral video to increase sales. When their Chocolate Grain video turns out a success, they turn to the man who came up with first video to produce a second, which he dubs "2 Berries 1 Cup" and shows the company executives a rough demo, which causes all the executives (save for the man who came up with the viral video, who shows no reaction to it at all) to vomit uncontrollably. In the episode "Eviscerated Post-Coital By A Six Foot Mantis", John Rambos flashbacks to Colonel Troutman at the end of First Blood includes Vietnam flashbacks with him being shown "Two Girls, One Cup".  Back to Brian shows Stewie and films his reaction with a camera. Despite his initial revulsion, Stewie decides to search for a possible male alternative. {{cite web|title=Stewie,s Reaction to 2 Girls 1 Cup (flv video) free file download at fliiby.com 
|url=http://fliiby.com/file/760304/sj0da5kauu.html|archiveurl=http://www.webcitation.org/5kLt5lhpU|archivedate=October 7, 2009|deadurl=no|accessdate=October 3, 2009}} 
*In Lil Waynes song "Gonorrhea" he mentions 2 Girls 1 Cup. 
* In an episode of Tosh.0, the entire audience is filmed reacting to this video. Host Daniel Tosh called it the Worlds Largest Reaction Video in terms of number of people being filmed. 
* The Toronto Star s blog covering the 2010 FIFA World Cup was titled "Two Guys, One Cup." 
* The site was featured in a Dr Pepper Facebook promotion open to minors,  which resulted in the Coca-Cola Company terminating its relationship with the digital marketing agency responsible.  Jim Edwards of BNET said that Coca Cola has full responsibility for allowing the situation to occur, arguing that Coca-Cola selected an advertising agency that openly advertised "profane" advertising campaigns and that the Coca-Cola executive who approved the 2 Girls 1 Cup line failed to do research on what the name meant. 
* In the British sitcom The Inbetweeners, the very beginning of season 2s episode "Night Out in London" depicted the shows main characters watching 2 Girls 1 Cup, with Will McKenzie stating "Thats gotta be chocolate!". 
* The cast of Avenue Q, responding to The Muppets version of "Bohemian Rhapsody", made a video of "We Will Rock You"/"We Are the Champions" (known as "We Will Rock Q"), ending with Nicky surfing the internet, finding 2 Girls 1 Cup (indicated by the soundtrack), and vomiting as the screen fades to black. {{cite web
|url= http://www.playbill.com/playblog/2010/05/avenue-q-rocks-muppetsqueen-video-parody/
|title= Avenue Q Rocks Muppets/Queen Video Parody
|date= May 5, 2010
|publisher=Playbill
|accessdate=September 24, 2010}}  Amy is shown getting the part, with the producer explaining exactly what she will be doing in the video while the other actress sits silently on the couch next to her eating an unidentified brownish food from a dish, which Amy declines when offered.   
*In Borgores song "Nympho" he mentions 2 Girls 1 Cup.

==References==
 

==External links==
*  
*  
 

 

 
 
 
 
 
 
 
 
 