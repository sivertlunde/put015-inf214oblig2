Kottayam Kunjachan
 
{{Infobox film
| name           = Kottayam Kunjachan
| image          = Kottayam Kunjachan.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = T. S. Suresh Babu
| producer       = M. Mani
| writer         = Dennis Joseph (screenplay) Muttathu Varkey (story)
| narrator       =  Ranjini  Innocent  K.P.A.C. Lalitha Sukumaran Prathapachandran Babu Antony Shyam
| cinematography = Anandakuttan 
| editing        = G. Venkataraman 
| studio         = Sunitha Productions
| distributor    = Aroma
| released       = 15 March 1990
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Kottayam Kunjachan (  directed by T. S. Suresh Babu, starring Mammootty in the title role.

==Plot==
Mikhael (Innocent (actor)|Innocent) is a family man and good Christian who lives in the mostly tranquil village of Odaangara, with his wife Aleyamma (KPAC Lalitha). They have two daughters, Mollykutty (Ranjini (actress)|Ranjini) and Susie and a son, Kuttappan. They have been involved in a long-standing dispute with their neighbour, Kanjirappalli Paappan (Prathapachandran), who wants to buy their land so that his bungalow would have a better front view and a road for his car. Paappans ruthless son, Jimmy (Babu Antony) is all for ejecting them by force, while Paappan is wary of what Aleyammas siblings, the Uppukandam Brothers, who are infamous thugs, might do in retaliation. When negotiations fail, Paappan resorts to underhanded tactics and scuttles Mollykuttys marriage proposal to Kuzhiyil Joy (Ravi Vallathol) and beats up Mikhael who questions it. Aleyaamma get her brothers involved, and Paappan and his son are beaten and humiliated by the Uppukandam brothers, led by Korah (Sukumaran) the eldest. Later, Joy visits Mollykutty and expresses his interest in her.

Meanwhile, Kottayam Kunjachan (Mammotty), an erstwhile thug, has just been released from jail. He has served a sentence of 7 years, for killing a man in a fight gone bad. He makes a dramatic entrance to Kottayam town and goes to collect his promised compensation. After an altercation with his previous employers, he is arrested and later bailed out by his guardian, a Christian priest (Jose Prakash) who found him as an infant in a garbage heap, and raised him as his own. Following his fathers advice, he leaves Kottayam and its bad influences for Odaangara. He starts a technical institute, accompanied by Bosco (Baiju (actor)|Baiju), his assistant. He generally endears himself to the village populace. It is there that he meets Mikhael and his wife, and gets in their good books by rescuing Mollykutty from a kidnap attempt by Jimmy and gang. Aleyamma convinces Mikhael to rent out their outhouse to Kunjachan, as a deterrent from further attacks by Paappan or Jimmy. Mikhael then convinces Kunjachan to go to confession, and start living as a good Christian.

Kunjachan takes a liking to Molly, who despises him for his past. However, when her parents attempt her to coerce her into another marriage, Molly has Susie enlist Kunjachans help to scuttle it, which he does masterfully. Kunjachan assumes that she returns his affections. His attempts to talk to Molly are mistaken for a rape attempt by Aleyamma. Mikhael throws him out and Kunjachan is too shocked to react. He is later beaten up by the Uppukandam brothers. Molly doesnt intervene, and she is reprimanded by Susie who realizes that Molly has used Kunjachan. Shortly afterwards, Mikhael and Korah are ambushed and killed by Jimmy and Kunjachan is the prime suspect. Kunjachan goes into hiding, and angrily confronts Molly and Susie when he gets an opportunity. Molly is aghast that Joy never showed up in her time of need and throws herself at Kunjachans mercy. He learns that Aleyamma is missing, and Kuttappan, her son has gone in search. He convinces them that he is innocent, and goes off to find Aleyamma. He manages to rescue her and her son from Jimmys thugs, but returns to find Molly and Susie missing and the rest of the Uppukandam brothers on the scene. They learn after beating up Paappan that the women have been taken to their plantation by Jimmy, and mount a rescue mission.

Molly and Susie are rescued in the nick of time by Kunjachan, who consoles Molly after her near-rape by Jimmy. The next morning, Kunjachan is visited at his institute by the Brothers. He fears another confrontation, but they reveal that they are there to propose Mollys marriage to Kunjachan.

==Cast==
*Mammootty as Kottayam Kunjachan Ranjini as Mollykutty Innocent as Mikhael
*KPAC Lalitha as Aliyamma
*Sukumaran as Korah
*Prathapachandran as Kanjirappalli Paappan
*Babu Antony as Jimmy Paappan
*K. B. Ganesh Kumar as Mathan Baiju
*Mala Aravindan as Anthru Jagannathan
*Zainuddin Zainuddin
*Adoor Bhavani as Maramkeri Mariyamma
*Ravi Vallathol as Kuzhiyil Joey
*Jagathy Sreekumar as Konayil Kochappi
*Kuthiravattom Pappu as Kuzhiyil Kochu
*Kollam Thulasi as Antrayose Kunchan as Kuttyappan Usha

==References==
 

==External links==
*  
* http://popcorn.oneindia.in/title/5783/kottayam-kunjachan.html
*  

 
 
 
 
 