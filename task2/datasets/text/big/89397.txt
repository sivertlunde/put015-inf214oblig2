A Bridge Too Far (film)
{{Infobox film
| name           = A Bridge Too Far
| image          = Bridge_too_far_movieposter.jpg
| caption        = original film poster
| screenplay     = William Goldman
| based on       =   James Caan Edward Fox Anthony Hopkins Gene Hackman Hardy Krüger Laurence Olivier Robert Redford Maximilian Schell
| director       = Richard Attenborough
| producer       = Joseph E. Levine Richard P. Levine
| cinematography = Geoffrey Unsworth, BSC
| music          = John Addison
| editing        = Antony Gibbs
| studio         = Joseph E. Levine Productions
| distributor    = United Artists
| released       =  June 15, 1977
| runtime        = 176 minutes
| country        = United States 
| language       = English
| budget         = $25 million 
| gross          = $50,750,000 
}}
 epic war book of the same name by Cornelius Ryan, adapted by William Goldman. It was produced by Joseph E. Levine and Richard P. Levine and directed by Richard Attenborough.   
 Allied attempt to break through German lines and seize several bridges in the occupied Netherlands, including one at Arnhem, with the main objective of outflanking German defences in order to end the war by Christmas of 1944.

The name for the film comes from an unconfirmed comment attributed to British Lieutenant-General Frederick Browning, deputy commander of the First Allied Airborne Army, who told Field Marshal Bernard Montgomery, the operations architect, before the operation: "I think we may be going a bridge too far." 
 Edward Fox, XXX Corps during Market Garden.

==Plot==

===Introduction and planning===
The film begins with a montage of archival film footage narrated by a Dutch woman, Kate ter Horst, describing the state of affairs in September 1944. The Allied advance is being slowed by overextended supply lines.
A Dutch family, part of the Dutch resistance underground, observes the German withdrawal toward Germany. The Germans in the Netherlands have few resources in men or equipment and morale is very poor.

U.S. General George S. Patton and British Field Marshal Bernard Montgomery have competing plans for ending the war quickly, and being the first to get to Berlin. Under political pressure, Supreme Allied Commander Dwight D. Eisenhower chose Montgomerys Operation Market Garden.

Operation Market Garden envisions 35,000 men being flown 300 miles from air bases in England and being dropped as much as 64 miles behind enemy lines in the Netherlands. The largest airborne assault ever attempted, with Lieutenant-General Frederick Browning saying, "Were going to lay a carpet, as it were, of airborne troops"  over which armored divisions of XXX Corps can pass and confidently suggests that "We shall seize the bridges - its all a question of bridges - with thunderclap surprise, and hold them until they can be secured". 
 Urquhart is Polish paratroopers under General Stanisław Sosabowski|Sosabowski. XXX Corps are to push up the road to Arnhem, as quickly as possible, over the bridges captured by the paratroopers, and reach Arnhem two days after the drop.
 Gavin of the 82nd worries about parachuting in daylight.

British commanders brief that they are badly short of transport aircraft and the area near Arnhem is ill-suited for a landing. They will have to land in an open area eight miles (13&nbsp;km) from the bridge. The British officers present at that briefing do not question the orders, but Sosabowski walks up to check the RAF briefing officers uniform insignia and says "Just making sure whose side youre on."  Later, when General Urquhart briefs his officers, some of them are surprised they are going to attempt a landing so far from the bridge, but they have to make the best of it. General Urquhart tells them that the key for the eight mile distance from the drop zone to the bridge, is the use of gliders to bring in reconnaissance Jeeps.  Browning lays out that if any one group fails, the entire operation fails.
 reconnaissance photos to General Browning showing German tanks at Arnhem. Browning dismisses the photos, and also ignores reports from the Dutch underground. Browning does not want to be the one to tell Montgomery of any doubts because many previous airborne operations have been cancelled. Major Fullers concerns are brushed off and he is removed from duty, sent on sick leave.

British officers note that the portable radios are not likely to work for the long distance from the drop zone to the Arnhem Bridge amid the water and trees of the Netherlands. They choose not to rock the boat and do not convey their concerns up the chain of command.

At the XXX Corps briefing, the overall plan is outlined by Lt. Gen. Brian Horrocks, laying out the bridges that will be taken by the paratroopers, held and then secured by ground forces. Speed is the vital factor, as Arnhem must be reached within 2–3 days. It is the crucial bridge, the last means of escape for the German forces in the Netherlands and an excellent route to Germany for Allied forces. The road to Arnhem, however, is only a single highway linking the various key bridges - trucks and tanks have to squeeze to the shoulder to pass. The road is also elevated causing anything moving on the road to stand out. The XXX Corps column would be led by the Irish Guards, under Col. Joe Vandeleur.

===Operation begins=== Son bridge is blown up by the Germans, just before the 101st Airborne secures it. German Field Marshal Walter Model|Model, thinking that the Allies are trying to capture him, panics and retreats from Arnhem. However, soon after landing, troubles beset Urquharts division. Many of the Jeeps either dont arrive by gliders at all or are shot up in an ambush. Their radio sets are also useless, meaning no contact can be made with either paratroopers moving into Arnhem under Lt. Col. John Frost or XXX Corps. Meanwhile, German forces reinforce Nijmegen and Arnhem.
Meanwhile, US Sergeant Eddie Dohun is driving his jeep searching for his commanding officer, Captain Glass. He finds the captain with a bullet in his head and thinking he is alive decides to take him to medical care. He encounters German troops but manages to avoid them. Arriving at the hospital, Dohun takes the captain to a medic who refuses to look at the captain until Dohun threatens to shoot him. The medic manages to get the bullet out of the captains skull and says hell possibly live. He places Dohun under arrest for 10 seconds as punishment for pointing a gun at him.

XXX Corps progress is slowed by German resistance, the narrowness of the highway and the need to construct a Bailey bridge to replace the destroyed bridge at Son. XXX Corps is able to move onto the Grave bridge without much resistance, but is halted at Nijmegen. There, soldiers of the 82nd Airborne Division perform a dangerous daylight river crossing in flimsy canvas-and-wood assault boats. Ultimately, despite heavy casualties the river crossing is successful, and the Nijmegen bridge is captured. The Germans close in on the isolated British paratroopers occupying part of Arnhem at the bridge. Urquhart is separated from his men, and the supply drop zones are overrun by the Germans. German attacks on the paratroopers at the bridge are repelled. British armour continues to fight its way up the corridor, but is delayed by strong German resistance.

===Operation Ends===
After securing Nijmegen Bridge, XXX Corps waits several hours for its infantry to secure the town. Finally, Sosabowskis troops enter the battle. Yet they are unable to effectively reinforce the British at Arnhem. The Germans, now on full alert, intercept and gun down numerous Poles during their drop; only a handful survive to reinforce the British. After days of house-to-house fighting at Arnhem, pitted against crack SS infantry backed by panzers, the outgunned paratroops are captured or forced to withdraw. Arnhem itself is indiscriminately razed.
 
Although Operation Market Garden is determined by Montgomery and his High Command to be 90% successful, most of those who actually carried it out feel quite differently. Urquhart escapes Arnhem with fewer than a fifth of his original 10 thousand crack troops; those who were too badly injured to flee stay behind and cover the withdraw, then give themselves up. Urquhart confronts Browning about his personal sentiments regarding the operation; did it go as well as Montgomery estimates? Brownings reply (and the films last line of dialogue, not counting the Allied prisoners who sing Abide With Me en route to the German POW camp) contradicts his earlier optimism for Market Garden: "Well, as you know, I always felt we tried to go a bridge too far."

In the films final scene, Kate Ter Horst (Liv Ullman) and her children are forced to abandon their bombed-out residence. Placing their belongings in a cart which is drawn by their husband/father (Lawrence Olivier), they pass through their front yard - which has been converted to a cemetery for fallen Allied troops - and trek across the countryside to an uncertain future. One of their sons brings up the rear, marching with a rifle-shaped branch he has found.

==Cast and roles==

===Allies===
{| class="wikitable"
|- " Actor
! Role
! Notes
|- Frederick "Boy" GOC I I British HQ First deputy commander, British Army at Nijmegen
|-
| James Caan || Staff Sergeant Eddie Dohun (based on Charles Dohun) || runner for Captain LeGrand King "Legs" Johnson, CO, Company F, 502nd Parachute Infantry Regiment, 101st Airborne Division U.S. Army (attacking Best)
|- The Irish The Guards XXX Corps, British Army 
|- Michael Byrne acting CO, 2nd Battalion (Armoured), The Irish Guards, British Guards Armoured Division. Cousin to Joe.
|- 1st British Airborne Division, Arnhem
|- Edward Fox Second Army
|-
| Elliott Gould || Colonel|Col. Robert Stout (based on Robert Sink) || CO, 506th Parachute Infantry Regiment, 101st Airborne Division 
|-
| Gene Hackman || Maj. Gen. Stanisław Sosabowski, || CO, Polish 1st Independent Parachute Brigade, Polish Armed Forces 
|- John Frost 1st Parachute Brigade, 1st British Airborne Division at Arnhem road bridge
|- James Gavin||CO, US 82nd Airborne Division, U.S. Army at the bridge across the Maas river in Grave, Netherlands|Grave, later at the Maas-Waal canal and the bridge across the Waal river in Nijmegen
|- 504th PIR, 82nd Airborne, U.S. Army seizing key bridges over the Maas-Waal Canal and the river assault crossing of the Waal river.
|-
| Denholm Elliott || RAF meteorology officer || fictional 
|- Peter Faber Royal Dutch Army  
|-
| Christopher Good || Maj. Carlyle (based on Maj. Allison Digby Tatham-Warter) ||CO, A Company, 2nd Parachute Battalion, 1st Parachute Brigade, Arnhem,  British Army
|- Moor Park Golf Club, Hertfordshire, England
|- Moor Park Golf Club, Hertfordshire, England
|-
| Nicholas Campbell || Capt. Glass (based on Captain LeGrand King "Legs" Johnson)|| CO, F Company, 2nd Battalion, 502PIR, He was initially wounded by a rifle bullet in the right shoulder. The following is taken from War Stories website   History vs Hollywood - Captain Legs Johnson "Medics made him lie down and set up an IV with plasma flowing into him. Medical jeeps bearing stretchers were evacuating wounded two at a time, to a field hospital in Zon. Since many of the wounded were hit more seriously than himself, Legs kept delaying his own evacuation, telling the medics to convey the others first. Even when Legs was finally loaded, he was still telling them to delay and take others. Against his objections, he was placed across the hood of the Jeep on a stretcher and then the Jeep scratched-off, headed for Zon.
At that time, a German MG42 machine-gun fired at the Jeep from over 500 yards distance. One round entered Legs helmet and tore into his head. He lost consciousness and would not wake -up until weeks later. 
At the hospital in Zon, Legs was briefly examined and since he was unconscious and his brains were exposed, he was relegated to the dead pile of troopers who were wounded so seriously that they had no chance to survive. 
Later that afternoon, Sgt Charles Dohun (Hollywood changed his first name to EDDIE), who was Legs runner   wandered over to the hospital for a specific purpose. He knew that the captain had a substantial amount of cash in his billfold and he didnt want a stranger from another unit to get it.  Luger at him and threatened to shoot him (he did not use a .45 as shown in A Bridge Too Far, but a M1911 pistol|.45 looks more impressive). 
The operation was successful. "Legs" regained consciousness six weeks later in a hospital, "deaf, dumb, blind, and with a steel plate in my head." As of this writing (October, 2005), Legs is still alive in Florida. Charles Dohun survived World War II and lived in N.C. until his death about 15 years ago. 
Regarding the Hollywood Depiction 
When I interviewed Legs Johnson in the late 1990s, he commented on how he and Sgt Dohun were portrayed in A Bridge Too Far, the 1977 Hollywood version of Cornelius Ryans book about Operation Market Garden. 
"Legs" said :"In the movie, I was a little, scared guy, and Dohun was a great big guy. Hell, in real life I wouldve made TWO of Dohun."  LTC Steve Chappuis (later Brigadier General (Ret.) Steve A. Chappuis), the 2/502 C.O. and "Silent Steve" placed him under arrest for one minute. As Dohun stood at attention before his desk, the LTC looked at his watch for sixty seconds, then told Dohun he could go. 
Captain Hugh Duke Roberts, the second battalion S-1 of the 502 PIR, was among the few individuals who knew the story of how Dohun had ordered the doctor to perform the operation, at gunpoint. Duke wrote a letter to Mrs Johnson,(Legs wife), explaining how Sgt Dohun had been responsible for saving his life. When Cornelius Ryan was researching A Bridge Too Far, Mrs Johnson sent that letter to Ryan, which is how the author became aware of the story. 
|- Pvt Wicks Batman to Lt. Col. Frost, CO, 2nd Parachute Battalion, British Army 
|- Donald Douglas || Brigadier Gerald Lathbury || CO, 1st Parachute Brigade, British Army in Arnhem. Wounded and briefly paralysed, Lathbury made a complete recovery and escaped captivity during Operation Pegasus.
|-
| Keith Drinkel || Lieutenant Cornish (based on Captain Eric Mackay, 9th Parachute Sqdn R.E.)|| 1st Airborne Division
|-
| Colin Farrell || Corporal Hancock || 1st British Airborne Division, Urquarts batman
|-
| Richard Kane || Col. Weaver (based on Graeme Warrack) || Senior Medical Officer, Headquarters RAMC, 1st British Airborne Division, at the Main Dressing Station in the Schoonoord Hotel of the Oosterbeek Perimeter
|-
| Paul Maxwell || Maj. Gen. Maxwell Taylor || CO, 101st Airborne Division, U.S. Army at the Zon bridge and later St-Oedenrode
|- Stephen Moore Major Anthony "Tony" John Deane–Drummond)  || Second–in–Command, 1st Airborne Divisional Signals  British Army, Arnhem
|-
| Donald Pickering || Lt. Col. C.B. Mackenzie || Principal General Staff Officer (Chief of Staff), Headquarters, 1st Airborne Division, British Army, Divisional HQ at the Hartenstein Hotel 
|-
| Gerald Sim || Col. Sims (based on (acting Colonel) Lt. Col. Arthur Austin Eagger)  || Senior Medical Officer, 1st Airborne Corps, R.A.M.C., British Army
|- Captain Lord Carrington) || British Grenadier Guards Commander who argues with Major Cook after 82nd capture Nijmegen Bridge
|- Alun Armstrong 2nd Battalion, 1st Parachute Brigade, 1st British Airborne Division
|-
| David Auker || Taffy Brace ||Medic, 1st British Airborne Division
|-
| Michael Bangerter || British staff colonel || British XXX Corps staff officer at General Brownings HQ
|-
| Philip Raymond || Grenadier Guards Colonel (based on Lt. Colonel Edward H. Goulburn) || C.O. 2nd Armoured Grenadier Guards Battalion
|-
| Michael Graham Cox || Capt. Jimmy Cleminson || T/Capt.,   James Arnold Stacey "Jimmy" Cleminson Officer Commanding, 5 Platoon (B Company), 3rd Parachute Battalion, British Army, Arnhem 
|-
| Garrick Hagon || Lieutenant Rafferty || Lieutenant, 101st Military Police Platoon, 101st Airborne Division, Division Field Hospital, U.S. Army 
|-
| John Ratzenberger || Lt James Megellas (based on Lt James Megellas) || Lieutenant, Company H, 504th PIR, 82nd Airborne Division, U.S. Army, at Waal River crossing 
|- Arthur Hill || U.S. Army surgeon (colonel) || Chief Division Surgeon Lt Col. David Gold, 101st Airborne Division Clearing Station
|-
| Ben Cross || Trooper Bins
|-
| Mark Sheridan || Sergeant Tomblin || 2nd Battalion, 1st Parachute Brigade, 1st  British Airborne Division
|-
| George Innes || Sergeant MacDonald || British 1st Airborne Division radio operator at the Hartenstein Hotel
|}

===Germans===
{| class="wikitable"
|- " Actor
! Role
! Notes
|- Generalmajor der Waffen-SS Karl Ludwig|| Based on Heinz Harmel, as he did not want his name to be mentioned in the film
|- General der Waffen-SS Wilhelm Bittrich|| CO of II SS Panzer Corps
|- OB West (commander of the German forces on the Western Front)
|-
| Walter Kohut || Generalfeldmarschall Walter Model|| CO of Army Group B
|- German Army sentry||
|- General der Infanterie Günther Blumentritt||
|- Lex van Delden || Oberscharführer Matthias|| Wilhelm Bittrich|Bittrichs aide.
|-
| Fred Williams || Hauptsturmführer Viktor Eberhard Gräbner|| Commander of the reconnaissance battle group of 9th SS Panzer Division Hohenstaufen
|}

 Source:   

===Dutch civilians===
{| style="width:500px;" class="wikitable"
|- style="background:#ccc;"
! Actor
! Role
|-
| Laurence Olivier || Dr. Jan Spaander
|-
| Liv Ullmann || Kate ter Horst
|- Underground leader
|-
| Erik van t Wout|| Underground leaders son
|-
| Marlies van Alcmaer || Underground leaders wife
|-
| Mary Smithuysen || Old Dutch lady
|-
| Hans Croiset || Old Dutch ladys son
|-
| Josephine Peeper || Cafe waitress
|-
| Erik Chitty || Organist
|-
| Richard Attenborough || Lunatic wearing glasses (uncredited cameo) 
|-
| Albert van der Harst || Medic
|}

 Source:   

===Military consultants=== John Waddy John Dutton Frost
* General James M. Gavin
* Lieutenant General Brian Horrocks
* Major General Roy Urquhart
* Brigadier Joe Vandeleur

 Source: Goldman, William Goldmans Story of a Bridge Too Far 

==Production== Dakota aircraft. Dakotas were procured. Two Portugal|Portuguese, ex-Portuguese Air Force, 6153, and 6171, (N9984Q and N9983Q), and two Air International Dakotas, operating from Djibouti in French Somaliland, F-OCKU and F-OCKX, (N9985Q and N9986Q) were purchased by Joseph E. Levine. Three Danish Air Force, K-685, K-687, and K-688, and four Finnish Air Force C-47 Skytrain|C-47s, DO-4, DO-7, DO-10 and DO-12, were loaned for the duration of the parachute filming.
 Horsa glider Horsa replicas were towed at high speed, though none went airborne. A two-seat LET L-13 Blaník|Blaník sail-plane, provided by a member of the London Gliding Club, Dunstable, was towed aloft for the interior take-off shots.
 Harvards portrayed Spitfire Mk. Neil Williams. 

  Nazi command centre) and the main church can be seen.
 XXX Corps reaching the Arnhem bridge (leading to the failure of the attack) differ considerably from those given in Cornelius Ryans text.

An episode of the Dutch TV history programme Andere Tijden  (English: Different Times) about the making of this movie stated that producer Joseph E. Levine told the Deventer town government that their town would host the world premiere for A Bridge Too Far, on June 14, 1977. This never came to be, though, and Deventer even missed out on the Dutch premiere, which was held in Amsterdam.

=== Interesting facts ===
* Joseph E. Levine financed the $22 million budget himself. During the production, he would show footage from the film to distributors who would then pay him for distribution rights. By the time the film was finished, Levine had raised $26 million, putting the film $4 million in the black before it had even opened. 
*United Artists agreed to pay $6 million for US and Canada distribution rights.   
* All the star-name actors agreed to participate on a favoured-nation basis (i.e. they would all receive the same weekly fee), which in this case was $250,000. per week (the 2012 equivalent of $1,008,250. or £642,000). "Entirely Up To You, Darling"; page 152-3; paperback; Arrow Books; published 2009. isbn 978-0-099-50304-0 
*According to Levine, the foreign star that distributors most wanted in the film was Robert Redford, followed by Sean Connery. 
*Richard Attenborough wanted to use Steve McQueen for a role and approached him. McQueens manager demanded $6 million for three weeks on A Bridge Too Far and three weeks on Apocalypse Now, plus Levine would buy McQueens house which McQueen was having trouble selling. Levine refused. 
* Shooting of the American-led assault on the Bridge at Nijmegen was dubbed the “Million-Dollar Hour”. Because of the heavy traffic, the crew had permission to film on the bridge between eight and nine oclock on October 3, 1976. Failure to complete the scene, would have necessitated rescheduling at a cost — including Redfords overtime — of at least a million dollars. For this reason, Attenborough insisted that all actors playing corpses keep their eyes closed. 
* Michael Caines scripted line to order the column of tanks and armoured cars into battle, was "Forward, go, charge". Luckily for Caine, Lieutenant Colonel Joe Vandeleur was on the set, so he could ask him what the actual line was. Vandeleur told him, "I just said quietly into the microphone, Well, get a move on, then", which is what Caine says in the film as released.
* Edward Fox had known General Horrocks before working on the film, and considered him a friend; thus, Fox took great care to portray him accurately. Years later, he would cite his portrayal of Horrocks as his favorite film role. 
* Dirk Bogarde had known General Browning from his time on Field Marshal Montgomerys staff during the war and took issue with the films largely negative portrayal of the general. General "Boy" Brownings widow, the author Daphne du Maurier, ferociously attacked his characterisation and "the resultant establishment fallout, much of it homophobic, wrongly convinced   that the newly ennobled Sir Richard had deliberately contrived to scupper his own chance of a knighthood." 
* Sean Connery initially turned down his role, fearing that the film would glamorize a military disaster, but changed his mind after reading the finished screenplay.
* Connery and Caine worked together on the 1975 film The Man Who Would Be King, although they had no scenes together in this movie.
* Even though Maximilian Schell spoke fluent English, he remained true to his character as General Bittrich and spoke no English in the movie. The Spy Who Loved Me prevented him from taking the part. Steve McQueen was originally offered the role of Major Cook but declined.
*Redford was paid $2 million for five weeks work. 
* A Bridge Too Far was the first war film in which actors were put through boot camp prior to filming. Attenborough put many of the extras/soldiers through a mini-boot camp and had them housed in a barrack accommodation during filming.

==Reception==

===Critical===
The film received mixed-to-positive reviews from critics. According to a "making-of" documentary included in a special edition DVD of A Bridge Too Far, at the time of its release, "the film was shunned by American critics and completely ignored at Oscar time for daring to expose the fatal inadequacies of the Allied campaign."  Review aggregator Rotten Tomatoes reports that 73% of 11 critics have given the film a positive review, with a rating average of 6.8 out of 10.   Rotten Tomatoes. Retrieved Sept. 5, 2010. 
While critics agreed that the film was impressively staged Canby, Vincent.  
New York Times (June 16, 1977).  and historically accurate, many found the film too long and too repetitive.    were cited by many critics for the excellence of their performances in a film filled with hundreds of speaking roles and cameos by many of the periods top actors.

===Box Office===
The film was a box office disappointment in the US but performed well in Europe. European filmgoers are holding up Bridge
Beck, Marilyn. Chicago Tribune (1963-Current file)   20 Oct 1977: a8. 

==William Goldmans Story of A Bridge Too Far==
{{Infobox book |  
| name          = Story of A Bridge Too Far
| title_orig    =
| translator    =
| image         = 
| caption       = 
| author        = William Goldman
| illustrator   = 
| cover_artist  =
| country       = United States English
| series        =
| genre         = non-fiction
| publisher     = 
| release_date  = 1977
| media_type    = 
| pages         = 
| isbn          = 
| dewey= 
| congress= 
| oclc= 
| followed_by  = 
}}
To promote the film, scriptwriter William Goldman wrote a book titled Story of A Bridge Too Far, published in December 1977.
It falls into three sections:
#"Reflections on Filmmaking in General and A Bridge Too Far". This section features some essays that would be reprinted in Goldmans famous Adventures in the Screen Trade. Egan p 145 
#"A Bridge Too Far: The Story in Pictures" - 150 sequential photographs from the film with captions from Goldman.
#"Stars and Heroes" - some of the movies actors and the men they play tell Goldman their thoughts on the film and the battle.
Goldman explains he wrote the books as a favour:
 Joseph E. Levine was very kind to me and I had a great experience on A Bridge Too Far. It was my first movie with Richard Attenborough and hes a marvelous human being. A lot of movies are shit, the experience is just terrible, and Bridge was wonderful and Mr Levine wanted something to publicize the movie so I wrote that for him. It was just something like Ive never done, but it was a as a favour for Mr Levine.  

==See also==
* Theirs is the Glory (1946 British film about the Battle of Arnhem)

==Notes==
 

==References==
*  Arthur, Max, Forgotten Voices of the Second World War: A new history of world war two in the words of the men and women who were there, Ebury Press, 2004 ISBN 0091897351  
*    
*  
*  
* Ambrose, Stephen E. & Immerman, Richard H., Ikes spies: Eisenhower and the espionage establishment, University Press of Mississippi, 1999. ISBN 0-385-14493-8  

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 