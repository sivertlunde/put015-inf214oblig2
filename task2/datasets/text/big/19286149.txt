Raja Desingu
 
{{Infobox film
| name = Raja Desingu இராஜா தேசிங்கு
| image = 
| director = T. R. Raghunath
| writer = Kannadasan Makkalanban
| screenplay = Kannadasan Makkalanban Bhanumathi Padmini Padmini T. S. Balaiah
| producer =  Letchumanan Chettiar
| music = G. Ramanathan
| cinematography = M. A. Rehman
| editing = V. B. Natarajan
| studio = Krishna Pictures
| distributor = Krishna Pictures
| released = 2 September 1960 
| runtime = 161 mins
| country = India Tamil
| budget = 
}}
 Bhanumathi and Padmini in the lead roles. The film, directed by T. R. Raghunath, had musical score by G. Ramanathan and was released on 2 September 1960.  The movie was miserably flopped at box office and ran for 70 days. 

==Production==
Gingee or Senji has a long history since 200 BC and over the years had been invaded and ruled by many dominant kingdoms. However, the one name that invariably crops up at any mention of Senji is that of Raja Desingu, who ruled the kingdom, albeit for a short period, with sagacity and valour in the 18th century. Over the years, the saga of Raja Desingu has been become an enduring and endearing part of Tamil folklore. Countless ballads, stories, puppet shows, dance, stage and street shows have been inspired by the tragic tale of this brave hero of Senji.

Adapted as a movie in 1936 by Rajeswari Talkies and was directed by Raja Chandrasekhar. The lead actors were T. K. Sundarappa, V. S. Mani, M. Lakshmi and K. R. Saradambal. The lyrics by Madurai Baskaradoss and music by M. Baluswami.

==Plot==
Swaroop Singh (O. A. K. Thevar) won the respect and loyalty of the people of Senji during his rule where Hindus and Muslims lived in peace and prosperity. A son was born to Swaroop Singh and his wife Rambai (Rushyendramani), whom they named Tej Singh pronounced as  Desingu (MGR) in Tamil. Swaroop Singh has a secret wife called Jaan Bibi (Lakshmiprabha), and they have a son called Dawood Khan (MGR) who was a little older than Desingu. Fearing that Dawood might pose a threat to the succession of the throne in years to come, on the advice of his commander and close confidant Yusuf Khan (T. K. Ramachandran), Swaroop Singh persuades Jaan Bibi to leave Senji, taking little Dawood with her. Desingu who has integrity and courage grows up with his childhood friend, another brave young man called Mohammed Khan (S. S. Rajendran), and two are always inseparable.

At this juncture, the Sultan of Delhi (M. G. Chakrapani) offers to free any state coming under his dominion whose ruler manages to tame a wild horse in his stable. Hearing this in the intention to have a free kingdom, Swaroop Singh leaves to Delhi without letting know Rambai and Desingu. Despite trying very hard, he failed to tame the wild horse and imprisoned by the Sultan of Delhi. Upon hearing this, the minister (Karikkol Raju) informs Desingu who rushes to Delhi where he meets his uncle Bheem Singh’s (M. R. Santhanam) for advice. The dauntless Desingu manages to tame the stallion and ride it, to the loud cheers of the huge audience. Filled with admiration, the Sultan frees Swaroop Singh who had earlier failed in this endeavour and gives them a written proclamation of Senji’s independence. Desingu marries Ranibai (Bhanumathi Ramakrishna |Bhanumathi) who is Bheem Singh’s  daughter. Pandiyan (K. A. Thangavelu) who works in the horse stable comes along to Senji and works as assistant to Mohamad Khan’s father and falls in love and marries Sengkamalam (M. N. Rajam) who is also Mohamad Khan’s adopted sister.

Meanwhile, growing up as an illegitimate child, Dawood vows to rule Senji some day. But in her deathbed, Jaan Bibi extracts a promise from Dawood that he would not cause any harm to Desingu. Dawood goes to Arcot and wins the confidence of the Nawab when he rescues the Nawab’s infant son from being crushed under an elephant’s feet. The grateful Nawab appoints him as his general. When Dawood expresses his determination to subjugate Senji, the Nawab expresses his helplessness, stating that nothing could be done as long as Desingu has the written proclamation of Senji’s independence in his custody. It is around the same time that Desingu dismisses his general Yusuf Khan from service when he is caught trying to commit adultery molesting Sengkamalam. Abetted by the humiliated Yusuf Khan who has now turned traitor, Dawood makes use of his striking resemblance to Desingu and extracts the written parchment from the unsuspecting wife of Desingu.

When Mohammed Khan falls in love with the pretty Ayisha (Padmini (actress)|Padmini), he anticipates no opposition from his parents for the match. However, he finds that his mother (T. A. Madhuram) and father (N. S. Krishnan) have each identified a girl of their own choice to be his wife. When he informs them that he has already chosen his life partner, his father proposes that they go the queen for an appropriate decision. Ranibai asks each of them to write down the name of the girl they have in mind, and then discovers that all 3 of them have written Ayisha’s name.

When the royal astrologer (P. G. Vengkadasalam) suggests that Desingu and Ranibai be separated for 3 years as an antidote for the inauspicious placement of their stars, Desingu pours scorn upon such beliefs. However, considering the welfare of Senji and heeding to his mother’s plea, Desingu is forced to consent to such separation. He carries on stolidly with his responsibilities as the ruler of Senji during the day.

The nawab od Arcot and Dawood Khan sents in (T. S. Balaiah) to meet Desingu to request Senji kingdom to pay tax. Desingu announces in agony that Senji would not bow to the Sulytanate of Delhi. Now that Senji had no proof of its independence, Arcot declares war on Senji for non-payment of taxes. Under the resolute leadership of Mohammed Khan, Senji emerges triumphant in warding off the Arcot forces. But even as Mohammed Khan fetches water to quench the thirst of a dying soldier of Arcot, Yusuf Khan shoots him from the back. And not satisfied with dastardly act, Yusuf Khan kills Mohammed Khan’s fiancée Ayisha who had come to the battlefield in search of her beloved. Wanting the brothers to fight against each other now, Yusuf Khan carries false reports to Desingu that it was Dawood who killed Mohammed Khan. Athirst with revenge for the death of his friend, Desingu rushes to the warfront and soon challenges Dawood to a duel. Dawood is hesitant to fight with his brother, and even advises Desingu that they could leave the fighting to their troops. But Desingu is no mood to listen and soon the brothers are engaged in a bitter duel. Handicapped by his oath not to harm Desingu, Dawood is soon vanquished and lies mortally wounded. Desingu’s delight is short-lived, for Yusuf Khan now informs him that Dawood is none other than his brother. Adding to his grief at the death of his friend, Desingu is horrified at having killed his brother, and in a moment of abject remorse, kills himself.

==Cast==
*M. G. Ramachandran as King Desingu and Dawood Khan
*S. S. Rajendran as General Mohammed Khan Bhanumathi as Ranibai Padmini as Ayisha
*T. S. Balaiah
*K. A. Thangavelu as Pandiyan
*M. G. Chakrapani as Nawab of Arcot
*M. N. Rajam as Sengkamalam
*N. S. Krishnan
*T. A. Madhuram
*T. K. Ramachandran as General Yusuf Khan
*O. A. K. Thevar as King Swaroop Singh
*M. R. Santhanam as Bheem Singh
*Lakshmiprabha as Jaan Bibi
*M. Saroja
*Rushyendramani as Queen Rambai
*P. G. Vengakadasalam as Royal Astrologer
*Karikkol Raju as Minister
*Sachu as Mohammed Khans younger sister

==Crew==
*Producer: Letchumanan Chettiar
*Production Company: Krishna Pictures
*Director: T. R. Raghunath 
*Music: G. Ramanathan
*Lyrics: Udumalai Narayana Kavi, Kannadasan & Thanjai N. Ramiah Doss
*Screenplay: Kannadasan & Makkalanban
*Dialogue: Kannadasan & Makkalanban
*Art Director: P. Angamuthu 
*Editing: V. B. Natarajan
*Choreograph: Vazhuvoor B. Ramaiah Pillai, P. S. Gopalakrishnan, T. C. Thangaraj & Chinni - Sampath 
*Cinematograph: M. A. Rehman 
*Stunt: R. N. Nambiar Kamala Lakshman, Ragini and Kuchalakumari

==Soundtrack==
The music composed by G. Ramanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Lyrics || Length (m:ss)
|-
| 1 || Aadhi Kadavul Ondruthan || Seerkazhi Govindarajan ||  || 02:52
|-
| 2 || Iyalodu Isaipole || C. S. Jayaraman & P. Bhanumathi ||  || 01:24
|-
| 3 || Kaadhalin Bimbam || P. Suseela ||  ||
|-
| 4 || Kanangkuruvi Kaattuppuraa || Seerkazhi Govindarajan &  P. Leela ||  || 04:07
|-
| 5 || Mannavane Senji || Seerkazhi Govindarajan & P. Leela ||  ||
|-
| 6 || Paarkadal Alaimele || M. L. Vasanthakumari ||  || 07:25
|-
| 7 || Vanamevum Raajakumaraa || Seerkazhi Govindarajan & Jikki ||  || 04:19
|-
| 8 || Podapporaaru || P. Leela ||  ||
|-
| 9 || Sarasaraani Kalyani || C. S. Jayaraman & P. Bhanumathi ||  || 02:54
|-
| 10 || Vaazhga Engal || P. Leela & Jikki ||  ||
|-
| 11 || Pazhanimalai || P. Leela ||  ||
|-
| 12 || Vandhaan Paaru || Seerkazhi Govindarajan & P. Leela ||  ||
|}

==References==
 

==External links==
 
* 
* 
* 
* 

 
 
 
 