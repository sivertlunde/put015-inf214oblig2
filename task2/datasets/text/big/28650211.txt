Laughing Boy (film)
{{Infobox Film
| name           = Laughing Boy
| image          = Ramon Novarro and Lupe Velez in Laughing Boy trailer.jpg
| image_size     = 
| caption        = Ramon Novarro and Lupe Velez
| director       = W. S. Van Dyke
| producer       = Hunt Stromberg W.S. Van Dyke John Colton John Lee Mahin Laughing Boy by Oliver La Farge
| starring       = Ramón Novarro Lupe Vélez
| music          = Herbert Stothart
| cinematography = Lester White
| editing        = Blanche Sewell
| storyboard     = 
| distributor    = Metro-Goldwyn-Mayer
| released       = April 13, 1934
| runtime        = 79 min.
| country        = United States
| language       = English}}
 of the same name by Oliver La Farge

==Cast==
{| class="wikitable"
|- align="center"
| Actor || Role 
|-
| Ramon Navarro|Ramón Navarro|| Laughing Boy
|-
| Lupe Vélez  || Slim Girl
|}

==External links==
 
*  
*  

 
 
 
 
 
 

 