Devour (film)
{{Infobox film
| name           = Devour
| image          = Devourdvd.jpg
| caption        = Devour DVD cover
| director       = David Winkler
| producer       = Daniel Bigel
| writer         = Adam Gross Seth Gross William Sadler Teach Grant
| music          = Joseph LoDuca
| cinematography = Brian Pearson
| editing        = Todd C. Ramsay
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Devour is a 2005 horror film directed by David Winkler.

==Plot== live roleplay-like The Game).

Following the deaths of his friends Conrad (Teach Grant) and Dakota (Dominique Swain), who introduced him to the game, Jake soon learns that "The Pathway" is actually being run by a man named Aiden Kater (Martin Cummins) and his band of Demonolatry|Devil-worshippers. Theyve been using it to look for a specific person, even as they manipulate others into killing. As their final acts, the victims of "The Pathway" commit suicide in various gruesome ways.
 William Sadler) that his wife, Anne Kilton, and their unborn child were taken by Kater and sacrificed to the devil. Soon after, he tracks down Kater and learns that Anne was not in fact sacrificed to the devil, that she gave birth, and that her child was stolen by mortals, and raised as a human. He is that child, the person whom "The Pathway" was created to find, and Anne is really Satan (devil) herself.

Ultimately, Jake confronts his birth mother (who has killed his adoptive parents) in the very place where he was stolen from her, he then learns that Marisol was, in fact, Satan/Anne. Following his rejection and attempted murder of her, Jake is shown a vision of the night he was born. He awakens covered with blood on the ground the next day, only to be arrested for the murder of his parents. The movie ends with Jake wondering if everything (including Pathway itself) really was not created by his imagination and if he had committed all those murders.

==Cast==
*Jensen Ackles as Jake Gray
*Shannyn Sossamon as Marisol
*Dominique Swain as Dakota William Sadler as Ivan Reisz
*Teach Grant as Conrad Dean
*Martin Cummins as Aiden Kater Rob Stewart as Ross North
*R. Nelson Brown as Walt
*Wanda Cannon as Kathy Gray
*Jenn Griffin as Older Anne Kilton
*Alan Ackles as Paul Kilton
*Rob Allen as Young Paul
*Tami DeSchutter as Young Kathy John Innes as Father Moore
*Reg Tupper as Hartney

==Reception==
Critical reception for Devour has been overwhelmingly negative. It currently holds a rating of 20% on Rotten Tomatoes, based on 5 reviews. 

==References==
 

==External links==
*  

 
 
 
 
 


 