Sennentuntschi
{{Infobox film
| name = Sennentuntschi
| image = 
| director = Michael Steiner
| producer = {{Plainlist|
* Simone Häberling
* Bruno Seemann}}
| screenplay = {{Plainlist|
* Michael Steiner
* Michael Sauter
* Stefanie Japp}}
| based on = Sukkubus - den Teufel im Leib (1989 film)
| starring = {{Plainlist|
* Roxane Mesquida
* Andrea Zogg
* Carlos Leal
* Nicholas Ofczarek
}}
| score = Adrian Frutiger
| cinematography = Pascal Walder
| editing = Ueli Christen
| country = Switzerland, Austria
| language = Swiss German
| released = 2010
| length = 115 min
}}

Sennentuntschi is a 2010 Swiss film written and directed by Michael Steiner. It is based on a eponymous Alpine fable.

==Plot==
1975, Grison Alps, Switzerland. A young priest is found hanged in a church tower. Shortly after the event, a mysterious girl appears in the village. Due to hostility she flees up into the mountains.
 herdsmen Erwin and Albert are joined by a third man, Martin. During a long night while being drunk and high on absinthe, Albert makes a doll out of a broom and hay, while Erwin tells Martin the story about Sennentuntschi. In the story, three men make such a doll, while the devil takes pity upon them and makes it come to life. On the next day, an actual girl appears in their lodge. Erwin warns Martin about the ending of the story, in which Sennentuntschi kills all three men, but they both rape her anyway. The girl takes revenge by slaughtering all the goats and killing Erwin, Albert dies in a fire by accident and Martin due to blood poisoning, as he was bitten by the girl during rape.

Meanwhile in the village, local (older) priest instigates parishioners against the unknown girl, calling her a witch, bearer of evil and responsible for the recent death of a new-born. He supports his theory by a photograph from 1950, showing young woman that resembles the girl. Police officer Reusch is the only one who keeps reason, therefore clashing with the villagers. He attempts to take the girl away, but is ambushed by the villagers. The girl however escapes. Reusch keeps investigating and finds out that the woman from the photograph the girls mother, who was impregnated by the priest who killed her and kept their daughter in captivity since birth. It is revealed that the young priest did not commit suicide but was killed in a fight during girls escape. Reusch arrests and imprisons the priest and goes into the mountains, searching for the girl.

Reusch finds the girl alive and well in the lodge, however days after the deadly events. The girl made dolls out of mens skins, just like also told in the fable, which made Reusch sick and angry with her. She runs away with Reusch following her, only to pursue her so far that she falls in a ravine. He goes after her and finds skinned corpses of herdsmen, as well as her body. Feeling responsible he takes his own life.

==Cast==
* Roxane Mesquida as Sennentuntschi
* Andrea Zogg as Erwin
* Carlos Leal as Martin
* Joel Basman as Albert
* Nicholas Ofczarek as Sebastian Reusch
* Hanspeter Müller-Drossaart as Notter
* Ueli Jäggi as Pfarrer Salis
* Peter Jecklin as Dr. Zingg
* Daniel Rohr as Bauer Stähli
* Rebecca Indermaur as Theres

==External links==
*  
*  

 
 