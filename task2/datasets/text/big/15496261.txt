Shaktiman (1993 film)
{{Infobox film
| name           = Shaktiman 
| image          = Shaktiman (1993 film).jpg
| caption        =
| director       = K.C. Bokadia|
| producer       = Suresh Bokadia
| writer         = K.C. Bokadia
| starring       = Ajay Devgn Karishma Kapoor Mukesh Khanna Kulbhushan Kharbanda
| music          = Channi Singh
| cinematography = 
| editing        = 
| distributor    = B.M.B Combines
| released       = 9 July 1993
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = }}

Shaktiman is 1993 Hindi Movie directed by K.C. Bokadia and starring Ajay Devgn,  Karishma Kapoor, Mukesh Khanna, Kulbhushan Kharbanda. 
Other cast members include Gulshan Grover, Ajit Khan, Parikshat Sahni, Tiku Talsania, Anjana Mumtaz, Beena, and Mahavir Shah.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kaise Teri Main Yaade Bhulaoon" 
| Aparna Mayekar
|-
| 2
| "Meri Haath Ki Choodi Bole"
| Asha Bhosle
|-
| 3
| "Mausam Haye Ye Kaisa Mausam"
| Amit Kumar
|-
| 4
| "Haule Haule Dil Doongi"
| Asha Bhosle
|-
| 5
| "Jeena Na Lage Bin Tere Yaara"
| Udit Narayan
|-
| 6
| "Sun Goriye"
| Asha Bhosle,Channi Singh
|}

== External links ==
*  

 
 
 

 