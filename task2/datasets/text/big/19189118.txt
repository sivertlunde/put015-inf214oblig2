Anne no Nikki
{{Infobox film
| name           = The Diary of Anne Frank
| image          = File:Anne no Nikki.jpg
| image size     = 222px
| border         = 
| alt            = 
| caption        = Poster of the film
| director       = Akinori Nagaoka
| producer       = 
| writer         = Hachirō Konno, Roger Parbes
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Michael Nyman
| cinematography = 
| editing        =  Madhouse
| distributor    = 
| released       =   
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

 , also known as The Diary of Anne Frank, is a 1995 Japanese anime film based on Anne Franks The Diary of a Young Girl. It is a feature film by Madhouse (company)|Madhouse, was directed by Akinori Nagaoka and released August 19.  

==Production== The Libertine.  The singer is contralto Hilary Summers.  "Candlefire" as well as piano-only versions of "If" and Why" appear on Nymans 2005 release, The Piano Sings.

==Release== French (as Le Journal dAnne Frank) with English subtitles.

== Soundtrack ==
{{Infobox album  
| Name        = The Diary of Anne Frank
| Type        = Soundtrack
| Artist      = Michael Nyman
| Cover       = Anne no Niki - soundtrack.jpg
| Released    = 1995
| Recorded    = 
| Genre       = Soundtrack
| Length      =  Virgin Venture
| Producer    = 
| Reviews     =  Carrington 1995
| This album  = The Diary of Anne Frank 1995 After Extra Time 1996
}}

=== Track listing ===
#Amsterdam Dawn
#Annes Birthday
#The Schoolroom
#Letter From Germany
#Goodbye Moortje
#Candlefire
#Renewal
#Light Of Love
#Chatterbox Waltz
#Hanukah
#Spring Freedom
#Concentration Camp
#If
#First Kiss
#D-Day
#The Diary Of Hope
#Silent Separation
#Lament For Lost Youth
#Why

==Reception==
Anime News Networks Justin Sevakis said that "even a story as powerful as Anne Franks cannot overcome truly odious filmmaking and weird directorial choices that just dont work" and that he couldnt "think of a worse way to experience the story than watching this film". 

==References==
 
* The Anime Encyclopedia by Jonathan Clements and Helen McCarthy (revised and expanded edition), "Diary of Anne Frank," p.&nbsp;144

==External links==
*  (Madhouse)  
* 
* 
*  

 
 
 

 
 
 
 
 
 
 


 

 