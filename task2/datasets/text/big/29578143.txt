Waste Land (film)
{{Infobox film
| name        = Waste Land
| image       = Waste Land-poster.jpg
| caption     = Promotional film poster
| alt         = A poster with the film title showing a portrait of a man reclining in a bathtub inspired by Jacques-Louis Davids 1793 painting Death of Marat, composed entirely of recyclable material
| director    =   Karen Harley
| producer    = Angus Aynsley Hank Levine Peter Martin Fernando Meirelles
| editing     = Pedro Kos
| starring    = Vik Muniz
| studio      = O2 Filmes Intelligent Media Almega Projects
| distributor =  
| released    =  
| runtime     = 100 minutes
| country     = UK, Brazil English
| music       = Moby
}} 2010 United Lucy Walker. catadores of recyclable materials, who find a way to the most prestigious auction house in London via the surprising transformation of refuse into contemporary art. The catadores work in a co-operative founded and led by Sebastião Carlos Dos Santos, the ACAMJG, or Association of Pickers of Jardim Gramacho, who dreamed of improving life for his community. The money created by the selling of the artworks was given back to the catadores and the ACAMJG, as well as the prize money from the film awards, in order to help the catadores and their community.

The film premiered at the 2010 Sundance Film Festival and went on to be nominated for the Academy Award for Best Documentary Feature, as well as win over 50 other film awards including the International Documentary Associations Best Documentary Award, which was handed to director Lucy Walker inside a garbage bag.  

The documentary was produced by Angus Aynsley and Hank Levine, executive produced by   and Karen Harley, co-produced by Peter Martin, and photographed by Dudu Miranda. The music was created by Moby, who is a friend and frequent collaborator of Walker. 

The film was distributed by   in its native Portugal.

==Reception==
Waste Land received unanimously positive reviews as "an uplifting portrait of the power of art and the dignity of the human spirit."   

On the Rotten Tomatoes aggregator site, 100% of critics gave the film positive ("certified fresh") reviews, based on 68 reviews, earning the film a Golden Tomato Award for the best-reviewed film of the year, and the only documentary to be 100% positively rated for that year. 
 SoHo House in Manhattan, NY and moderated a Q&A with Lucy Walker and Moby. 

==Awards==
* Audience Award for Best World Cinema Documentary, Sundance Film Festival 2010
* Audience Award Panorama Publikumspreis for Best Film, Berlin International Film Festival 2010
* Amnesty International Award, Berlin International Film Festival 2010
* Audience Award for Best Film, Full Frame Film Festival 2010
* HBO Audience Choice Award for Best Documentary Feature, Provincetown International Film Festival 2010
* Audience Award for Best World Cinema Documentary, Maui International Film Festival 2010
* Audience Award for Best Film, Paulinia Film Festival 2010
* Jury Award for Best Film, Paulinia Film Festival 2010
* Best Documentary, Dallas International Film Festival 2010
* $25,000 Target Filmmaker Award, Dallas International Film Festival 2010
* Best Documentary Golden Space Needle Award, Seattle International Film Festival 2010
* Best Documentary, Durban International Film Festival 2010
* Audience Choice Best Film, Durban International Film Festival 2010
* Amnesty International Durban Human Rights Award, Durban International Film Festival 2010
* Crystal Heart Best Documentary Award, Heartland Film Festival 2010
* Human Spirit Award, EcoFocus Film Fest 2010
* Audience Award for Best Feature Film, EcoFocus Film Fest 2010
* Best Documentary, Trinidad & Tobago Film Festival 2010
* Jury Award, Flagstaff Mountain Film Festival 2010
* Rogers Peoples Choice Award, Vancouver International Film Festival 2010
* Prêmio Itamaraty for Best Documentary Feature, São Paulo International Film Festival 2010
* Special Jury Prize for Best Feature, Amazonas Film Festival 2010
* Audience Award, IDFA International Documentary Film Festival Amsterdam 2010
* Audience Award, Stockholm International Film Festival 2010
* IDA Pare Lorentz Award, International Documentary Association Awards 2010
* Best Documentary, International Documentary Association Awards 2010
* Grand Prix du Festival, International Environmental Film Festival 2010
* Jury Award, Frozen River Film Festival 2011
* Best of Festival, Wild & Scenic Film Festival 2011
* Best in Festival, Princeton Environmental Film Festival 2011
* Best Documentary, Movies For Grownups Awards 2011
* Best Documentary Feature Film Audience Choice Award, Sedona International Film Festival 2011
* Keen Hybrid Life Filmmaker Grant, Boulder International Film Festival 2011
* Audience Award (tie), Environmental Film Festival at Yale 2011
* Best Film, Investigation section, Levante International Film Festival 2011
* Best Editing, Investigation section, Levante International Film Festival 2011
* KEEN Hybrid Life Filmmaker Grant, Boulder International Film Festival 2011
* Best Documentary, Sedona International Film Festival 2011
* Audience Award, Freezone Belgrade Film Festival 2011
* Green Fire Award, American Conservation Film Festival 2011
* Audience Award, Addis Ababa International Film Festival 2012
* Best Documentary Feature, Grande Premio do Cinema Brasileiro 2012
* Best Documentary Editing - Pedro Kos, Grande Premio do Cinema Brasileiro 2012
* Best Documentary nomination, British Independent Film Awards 2006
* Grand Jury Prize for Best World Cinema Documentary nomination, Sundance Film Festival 2010
* Peace Film Award - Unabhaengige, Filmfest Osnabrueck 2010
* Humanitas Award nomination for Best Documentary, Humanitas Awards 2010
* Best Documentary nomination, Grierson Award for Best Documentary, London Film Festival 2010
* Best Documentary nomination, British Independent Film Awards 2010
* Cinema for Peace Award for Most Valuable Documentary of the Year, Cinema For Peace Awards 2011
* Winner, London Film Awards 2012
* Best Documentary Feature nomination, The 83rd Annual Academy Awards 2011
* Golden Tomato Award, Rotten Tomatoes Golden Tomato Awards 2010

==International titles==

The Portuguese-language title is Lixo Extraordinário literally meaning "extraordinary garbage", which is a pun referring to the labels on Brazilian sanitation trucks containing special waste. 
The Spanish-language title is El Reino de la Basura, literally "the kingdom of garbage". 
The French distributors Eurozoom chose to keep the English title "Waste Land" in France with the subtitle "de la Poubelle au Musée" (From the Trash to the Museum). 

==See also==
*ACAMJG, the association of recyclables pickers of Jardim Gramacho.
*Jacques-Louis Davids 1793 painting Death of Marat
*Agnes Vardas 2000 documentary The Gleaners and I

==References==
 

==External links==
*  
*  
*   PBS
*  
*   at Metacritic
*  

 
 
 
 
 
 
 
 
 
 