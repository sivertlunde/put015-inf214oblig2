Gift of Gab (film)
 
{{Infobox film
| name           = Gift of Gab
| image          =
| caption        =
| director       = Karl Freund
| producer       = Rian James Carl Laemmle Jr.
| writer         = Lou Breslow Philip G. Epstein Rian James Jerry Wald
| narrator       =
| starring       = Edmund Lowe Gloria Stuart
| music          =
| cinematography = George Robinson Harold Wenstrom
| editing        = Ray Curtiss Universal Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
}}
Gift of Gab is a 1934 black-and-white film released by Universal Pictures. Edmund Lowe stars as a man with the "Gift of Gab" &mdash; he can sell anyone anything. The film costars Ruth Etting, Ethel Waters, Victor Moore, and Gloria Stuart, and features Boris Karloff and Béla Lugosi.

Ruth Etting sings "Talking to Myself" and "Tomorrow, Who Cares?". Originally the Three Stooges were signed to appear in the film. However, the Stooges had just signed with Columbia Pictures for Woman Haters, the first of their short subjects, so three look-alike actors replaced them for Gift of Gab.

==Cast==
* Edmund Lowe - Phillip "Gift of Gab" Gabney
* Gloria Stuart - Barbara Kelton
* Ruth Etting - Ruth Phil Baker - Absent-minded Doctor
* Ethel Waters - Ethel
* Alice White - Margot
* Alexander Woollcott - Cameo Appearance
* Victor Moore - Colonel Trivers
* Hugh OConnell - Patsy
* Helen Vinson - Nurse
* Gene Austin - Radio Artist
* Tom Hanlon - Radio Announcer
* Henry Armetta - Janitor
* Andy Devine - McDougal the waiter
* Wini Shaw - Cabaret Singer
* Boris Karloff - Cameo Appearance (as Karloff)
* Béla Lugosi - Cameo Appearance

==See also==
* List of American films of 1934
* Boris Karloff filmography
* Béla Lugosi filmography

==External links==
* 

 

 
 
 
 
 
 
 
 


 