Success (1991 film)
 
{{Infobox film
| name           = Success
| image          = 
| image size     = 
| caption        = 
| director       = Franz Seitz, Jr.
| producer       = Ferdinand Althoff Franz Seitz, Jr.
| writer         = Lion Feuchtwanger
| starring       = Bruno Ganz
| music          = 
| cinematography = Rudolf Blahacek
| editing        = Gisela Haller
| distributor    = 
| released       =  
| runtime        = 265 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Success ( ) is a 1991 German drama film directed by Franz Seitz, Jr. It was entered into the 41st Berlin International Film Festival.   

==Cast==
* Bruno Ganz as Jacques Tüverlin
* Franziska Walser as Johanna Krain
* Peter Simonischek as Dr. Martin Krüger
* Mathieu Carrière as Erich Bornhaak
* Gudrun Gabriel as Anna Elisabeth Haider
* Manfred Zapatka as Kaspar Pröckl
* Thomas Holtzmann as Dr. Franz Flaucher
* Jutta Speidel as Katharina von Radolny
* Gerd Anthoff as Paul Hessreither
* Günter Mack as Förtsch
* Dietrich Mattausch as Dr. Hartl - Amtsgerichtspräsident
* Bernhard Wicki as Bichler
* Ernst Jacobi as Dr. Siegbert Geier
* Martin Benrath as Dr. Otto Klenk 
* Gustl Bayrhammer as Vorsitzender Disziplinarausschuß

==References==
 

==External links==
* 

 
 
 
 
 


 
 