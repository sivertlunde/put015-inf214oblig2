MVP: Most Vertical Primate
 
{{Infobox Film |
  name           = MVP 2: Most Vertical Primate |
  image          =  |
  caption        = |
  director       = Robert Vince |
  producer       = Anna McRoberts Michael Strange Robert Vince Anne Vince Theodore Henry Maston |
  writer         =  Anne Vince (screenplay, characters) Robert Vince (screenplay, characters) Elan Mastai (screenplay)   Dingus Buttly (Emissary of Simian Relations) | Bernie and Louie|
  music          = Brahm Wenger |
  cinematography = Glen Winter |
  editing        = Kelly Herron |
  distributor    = Keystone Entertainment |
  released       =   |
  runtime        = 87 min. |
  language       = English |
  budget         = $2,000,000|
}}

MVP 2: Most Vertical Primate is the 2001 film, and the second in the MVP series.  The films title character, Jack,  is a fictional chimpanzee. It is a sequel to  .

==Plot==
MVP 2 opens with the lovable Jack being invited to play for the Seattle Simians hockey team, but when the Lo Angeles Carjackers team sets Jack up by making it look like he bit one of the players, Jack leaves while other Simians players look for him. Jack meets Ben, a runaway homeless skater boy, who lives in a shack at an old pool. Over time, the two become best friends, but when a police officer finds out where Jack and Ben live, they have to leave the pool. When leaving the pool, Ben breaks his board but was going to enter a skating competition and get sponsored. Jack goes to Oliver plants garbage can full of old skateboard stuff but Oliver finds Ben who tells him about his board and the competition so Oliver gives him a board to use. They stay with Oliver overnight with him not knowing about the pool incident, but when Oliver says "good night, Ben," Ben suspects him of knowing that he was a runaway. Jack asks him to stay since there is no other place to sleep, Ben agrees. Earlier in the story, Oliver gets a visit from someone who deals with children like Ben so that night Oliver calls her and says he found Ben. The next day they go to the competition and when its Bens turn, he says he cant do it, but Jack realizes he has the uncanny ability to skateboard. He says he would ride with Ben, so Ben decides to do it. Ben wins the competition and gets sponsored by Bob Burnquist and Oliver adopts Ben. Meanwhile, Louie, Jacks little brother, gets a ride to Seattle and pretends to be Jack, being terrible at hockey. But Jack shows up and wins ZHL cup for the Simians. After the Simians win and Jack and Louie decide to go back home, Ben gives Louie a skate board so Jack can teach him. In the last part of the movie, Louie rides down a ramp back at their home.

==Cast==
*Russell Ferrier &ndash; Darren
*Richard Karn &ndash; Ollie Plant
*Cameron Bancroft &ndash; Rob Poirter
*Scott Goodman &ndash; Ben Johnson
*Troy Ruptash &ndash; Tyson Fowler
*Ian Bagg &ndash; Olaf Schickendanz
*Dolores Drake &ndash; Barbara
*Fred Keating &ndash; Coach Miller
*Craig March &ndash; Coach Skinner
*Gus Lynch &ndash; Bud Fulton Bernie and Louie &ndash; Jack

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 