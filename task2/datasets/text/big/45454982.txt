Subah Ka Tara
{{Infobox film
| name           = Subah Ka Tara
| image          = Subah_Ka_Tara.jpg
| image size     =
| caption        = 
| director       = V. Shantaram
| producer       = Rajkamal Kalamandir
| writer         = Shams Lakhnavi
| narrator       =
| starring       = Pradeep Kumar Jayshree Baby Rajshree Amirbai Karnataki
| music          = C. Ramchandra 
| cinematography = G. Balakrishna
| editing        =
| studio         = Rajkamal Kalamandir
| distributor    =
| released       =  
| runtime        = 107 minutes
| country        = India
| language       = Hindi
| budget         =
| preceded by    =
| followed by    =
}} 1952 Hindi social romantic film directed by V. Shantaram.    The film was produced by   Rajkamal Kalamandir and had story and dialogues by Shams Lucknavi. The director of photography was G. Balkrishna. It had music composed by C. Ramchandra and the lyricists were Noor Lakhnavi and Diwan Sharar.    The title song "Chamka Chamka Subah Ka Tara" was one of the notable songs from this film. Rajshree appeared in her debut role as a child star.    The cast included Pradeep Kumar, Jayshree, Amirbai Karnataki, Rajshree, Shakuntala Paranjpye and Naaz.   

Jayshree plays the role of a young widow pursued by a young man (Pradeep Kumar) who falls in love with her but is unable to marry her due to societal norms and subsequently becomes insane.   

==Plot==
The film told in flash-back, starts with a few people troubling a disheveled, insane man holding a bedding. The mad man is Mohan (Pradeep Kumar) who finally sits down with a sympathiser and starts narrating the story. Mohan meets a young widow, Padmini (Jayshree) taking medicine for her dying mother. Padminis mother, from her death-bed, warns her about befriending young men as shes a widow and society demands that she live alone with just a white sheet as a companion for thats her life as a widow. Following her mothers death Padmini goes to her paternal uncles house to live. Her uncle has gone to Africa and her Aunt treats her badly. Mohan falls ill thinking about Padminis situation and her rebuff when he pursues her. He is looked after by his mother and young sister (Baby Rajshree). Padmini is made to work as a maid in her Uncles house but has a kind cousin sister, Bharti (Chandrakanta). Mohan arrives there on some work and the Aunt has chosen him as a suitable boy for Bhartis marriage. Mohan sees Padmini and tells her how he feels, which is overheard by the Aunt. The Aunt throws Padmini out of the house to fend for herself. The story continues with the problems Padmini faces, Mohans search for Padmini and her death when she sets the house left by her mother, on fire. Mohan finds the bedding  in the aftermath of the fire and goes insane.

==Cast==
* V. Shantaram as the drunken man
* Jayashree as Padmini
* Pradeep Kumar as Mohan
* Neelambai as Mohans mother
* Baby Rajshree as Sogi, Mohans sister
* Shakuntala Paranjpye as Chachi
* Naaz as Heera
* Amirbai Karnataki as neighbour
* Nimbalkar as Paanwala
* Chandrakanta as Bharti
* Jogendra as Milkman
* Aminabai as Padminis mother

==Crew==
* Producer Rajkamal Kalamandir
* Director: V. Shantaram
* Story: Shams Lucknavi
* Dialogue: Shams Lucknavi
* Music: C. Ramchandra
* Lyrics: Noor Lucknavi, Diwan Sharar
* Cinematographer: G. Balkrishna
* Editor: Chintamani Borkar
* Art Director: Baburao Jadhav
* Sound: A. K. Parmar
* Make-up: Baba Vardam
* Still Photography: Kirtiwan

==Review==
Shantaram played a small part as a drunk man, whose house Jayshree goes to looking for a job. His role was appreciated as the "engaging drunk" in the film.    Jayshrees role as Padmini, the young widow was commended but her "speaking in whispers" was censured.    Pradeep Kumars role as the "socially conscious" youth was termed "memorable".   

==Soundtrack==
Shantaram chose C. Ramchandra as opposed to Vasant Desai for two of his films, Parchhain (1952) and Subah Ka Tara. However, Desai scored the background music for both the films.    The films title song "Gaya Andhera Hua Ujala Chamka Chamka Subah Ka Tara" in Raga Pahadi sung by Talat Mehmood and Lata Mangeshkar.     The other notable songs from the film were Talats "Apni Nakami Se Mujhko Kaam Hai", and "Badi Dhoom Dham Se Meri Bhabhi Aayi" sung by Usha Mangeshkar, which was also her debut song.   

The music director was C. Ramchandra with lyrics by Noor Lucknavi and the song "Do Haklon Ka Suno Fasana" written by Diwan Sharar. The singers were Lata Mangeshkar, Talat Mahmood, C. Ramchandra, Chandrakanta and Usha Mangeshkar.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-
| 1 Gaya Andhera Hua Ujala
| Talat Mehmood, Lata Mangeshkar
|-
| 2
| Bhaabhi Aayi Badi Dhoom Dhaam Se Meri Bhabhi Aayi
| Usha Mangeshkar
|-
| 3
| Sune Zamane Ke Taane 
| Lata Mangeshkar
|-
| 4
| Khushi Achhi Hai, Qismat Ne Hamako Zulm Uthana Sikha Diya 
| Lata Mangeshkar
|-
| 5
| Kahun Kaase Main Man Ki Baat
| Lata Mangeshkar
|-
| 6
| Chali Ban Ke Dulhan Unse Laagi Lagan
| Lata Mangeshkar
|-
| 7
| Apni Naakaami Se Mujhko Kaam Hai
| Talat Mehmood
|-
| 8
| Do Haklon Ka Suno Fasana
| Lata Mangeshkar, Chandrakanta
|-
| 9
| Zara O Jane Wale Rukh Se Aanchal Ko Hata Dena
| C. Ramchandra
|}

==References==
 

==External links==
* 

 

 
 
 
 
 