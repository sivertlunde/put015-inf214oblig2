Kiss of a Siren
{{multiple issues|
 
 
}}
 
Kiss of a Siren is a 2014 short fashion film written and directed by Miguel Gauthier and Viktorija Pashuta. The film, produced by hair care company Nume, premiered on July 26, 2014, at the La Jolla International Fashion Film Festival. The story centers around a mermaid, portrayed by Cassi Colvin, who was outfitted with a realistic 25&nbsp;lb mermaid tail for the role, and Rick Taldykin as her love interest.

==Summary==
Legends say that the pure gold blood of mermaids holds the secret to eternal beauty. The Evil Queen hunted down their kind to extinction in her effort to stay young forever. Many have fallen in their attempts to hunt the few that remained, for the kiss of a siren is poison to all she doesnt love.

==Costumes==
The directors vision was to create a fashion film using clothing as an integral part of each characters development and the overall arc of the film. As such, the directors shied away from working within the constraints of using only one designer, allowing the fashion in the film to take on a life of its own. Each scene of the film contains a metaphorical classical element: earth, water, air and fire, and the costumes tie into that.

Stylist Tiffani Chynel was brought on to design custom pieces specifically for the film. Costumes were designed and picked by the Suehiro Kimono Agency, Caley Johnson, and Erin Brown. Some labels appearing in the film include Vivienne Westwood, Giuseppe Zanotti, Calvin Klein, G-Star Raw, Marianna Harutunian, and Ashton Michael.  Custom headpieces were designed by Miss G Designs  while the mermaid tail worn in the movie was created by Mertailor Eric Ducharme. Anastasia Durasova served as key makeup artist with Atma Hari leading as key hair stylist.

==Awards==
The film won awards for Best Costume Design and Best Film at the 2014 La Jolla Fashion Film Festival,  beating out high-profile brands such as Gucci, Fendi, and Prada. The film was nominated for 8 categories total, including Best Hair and Best Makeup.

==References==
 

==External links==
*  

 