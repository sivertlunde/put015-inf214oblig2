Lucky 13
 
 
{{Infobox film
|  name           = Lucky 13
| image          = 
| image size     = Chris Hall Chris Hall Richard Cooper Sasha Alexander (co-producer) Joseph DePompeii (executive producer) Peter Jay Klauser (executive producer) Chris Hall Eric Swelstad (story) Ira Heffler (story) | John Doe Kaley Cuoco Taryn Manning
| music          = Hal Lindes Herman Beeftink Michael Barrett
| distributor    = MGM Home Entertainment
| released       = 2005
| runtime        = 95 min.
| language       = English
}} Chris Hall John Doe, Kaley Cuoco and Taryn Manning.  

==Plot==

This film is about Zach Baker (Hunt) and his quest to go back through his past experiences with women so he will have the perfect date with his lifelong friend, Abbey (Graham).  Abbey would be the thirteenth women he has gone out with and he hopes she will be "Lucky 13".  The story revolves around Zach asking each woman what he did wrong in their relationship, so as to not make the same mistakes with Abbey.  A recurring gag involves Zach throwing objects, representing his past affairs, into a lake. During the course of the film, Zach makes changes to his appearance and demeanor, trying to emulate the advice he gets from his past girlfriends—most of which is contradictory.  After much soul-searching, Zach decides to ask Abbey to marry him—a proposal that she turns down in order to move to New York City and pursue her dream of being an artist. Zach eventually comes to realize that his life in the Mid-West is not so bad and he gains a new appreciation for his family and friends.

== External links ==
* 
* 

 
 
 
 

 