The Stendhal Syndrome
{{Infobox film
| name            = The Stendhal Syndrome
| image           = Sindrome di Stendhal.jpg
| caption         = Italian theatrical release poster
| director        = Dario Argento Luigi Cozzi
| producer        = Dario Argento Giuseppe Colombo Walter Massi
| screenplay      = Dario Argento
| story           = Dario Argento Franco Ferrini
| based on        = the novel "La Sindrome di Stendhal" by Graziella Magherini
| starring        = Asia Argento Thomas Kretschmann Marco Leonardi
| music           = Ennio Morricone
| cinematography  = Giuseppe Rotunno
| editing         = Angelo Nicolini
| studio          = Cine 2000 Medusa Produzione
| released        = 26 January 1996
| runtime         = 120 min.
| country         = Italy
| language        = Italian
| budget          = $3,800,000  (estimated) 
| gross           = Italian lira|₤5,443,000,000  (Italy) 
}}
 horror film written and directed by Dario Argento and starring his daughter Asia Argento. It was the first Italian film to use computer-generated imagery (CGI). 

Stendhal syndrome is a real syndrome, first diagnosed in Florence, Italy in 1982. Argento said he experienced Stendhal syndrome as a child. While touring Athens with his parents young Dario was climbing the steps of the Parthenon when he was overcome by a trance that caused him to become lost from his parents for hours. The experience was so strong that Argento never forgot it; he immediately thought of it when he came across Graziella Magherinis book about the syndrome, which would become the basis of the film.
 US $3,809,977), making it Argentos highest grossing film in his native country.

== Plot ==

Detective Anna Manni (Argento) travels to Florence on the trail of a serial killer Alfredo (Thomas Kretschmann). While at a museum, Anna is struck by Stendhal syndrome, which causes people to become overwhelmed by great works of art. Alfredo uses this disorder against Anna, kidnapping and raping her. She escapes and is deeply psychological trauma|traumatized. Alfredo tracks her movements and is able to capture her again. This time, Anna manages to break free, badly wounding her captor, and knocking him into a river.

While the search for the body is underway, Anna meets and falls in love with Marie, a young French art student. Anna also takes sessions with a psychologist to try and overcome her trauma. Anna begins to receive phone calls from Alfredo. Marie is found dead, and Annas psychologist visits her at home. Her police friend Marco calls to notify her that Alfredos body has been found. This leads to the psychologist confronting Anna with the reality that she is Maries murderer. Marco travels to Annas apartment, only to find the dead psychologists body. He attempts to take Annas gun, but she kills him after confessing that Alfredo is now inside her and ordering her to do terrible things. The police arrive on the scene and arrest her.

== Cast ==

* Asia Argento as Det. Anna Manni
* Thomas Kretschmann as Alfredo Grossi
* Marco Leonardi as Marco Longhi
* Luigi Diberti as Insp. Manetti
* Paolo Bonacelli as Dr. Cavanna
* Julien Lambroschini as Marie
* John Quentin as Annas father
* Franco Diogene as Victims husband
* Lucia Stara as the Shop assistant
* Sonia Topazio as Victim in Florence
* Lorenzo Crespi as Giulio
* Vera Gemma as the Policewoman
* John Pedeferri as Hydraulic engineer
* Veronica Lazar as Maries mother
* Mario Diano as the Coroner
*Cinzia Monreale as Alfredos Grossis wife

== Production ==
 La Reine Margot (1994). Argento was impressed enough by Kretschmann that he would later think of him for the role. 

The opening scene was shot in Florence at Italys famed Uffizi Gallery. Argento is the only director ever granted permission to shoot there. 
 The Night Watch. The painting that causes Anna to faint in the museum is by Pieter Bruegel the Elder|Bruegel, called Landscape with the Fall of Icarus.

The footage of Anna underwater after fainting in the gallery was shot in the sea.  The huge grouper fish that Anna kisses was a remote model that was being pulled through the waters by cables attached to a small float on the oceans surface.  Mere moments after wrapping the underwater shoot, the fish stopped working. 

This would be the last fiction feature film for acclaimed director of photography Giuseppe Rotunno. The following year he shot a documentary on Marcello Mastroianni before retiring.

Graffiti artists were brought in to cover the underground lair of Alfredo with graffiti. In one night the group created over a hundred square feet of graffiti-covered walls on the location. 
 The Phantom The Church, both of which Dario Argento produced.

Argento planned on making a sequel which would follow detective Anna Manni on another case. However, Asia was unavailable so the characters name was changed (to Anna Mari) and Stefania Rocca was cast. The resulting film is 2004s The Card Player.

== Release ==

=== Critical reception ===
 AllMovie called the film "a sadistic and disturbing psychological exploration driven by the horrifying concept of a rape victim who begins to take on her attackers dark persona", but one that is "ultimately a victim of its own excess and the directors tendency to overcomplicate a fairly simple storyline." 

=== Home video ===

In the U.S., The Stendhal Syndrome is distributed by B movie company Troma Entertainment. A new special edition DVD of the film was released by Blue Underground on 30 August 2007.
 BBFC for a video certificate. These cuts are to rape scenes, violence and some dialogue. The 2005 UK DVD release, by Arrow Pictures, has had all previous cuts waived and represents the full-length English version, although like all English releases it omits the two scenes exclusive to the Italian version. Since the uncut version has never been submitted to the British Board of Film Classification, this version was withdrawn and re-released in a cut form.

Blue Underground released The Stendahl Syndrome on Blu-ray in 2009, which contains the entire film uncut, including the additional Italian-only scenes (still in Italian, with English subtitles).

== Versions ==

* The US DVD release by Troma is the complete version of the English language edition but, like all English releases, is still missing around two minutes of material exclusive to the Italian print.
* The Italian release is around two minutes longer than the English export version, including an additional scene where Anna calls the husband of one of Alfredos victims and another where she meets Maries mother, played by Veronica Lazar (whose name is included in the credits of all versions, even those in which she does not appear).

==Further reading==
* Julian Hoxter. "Anna with the Devil Inside: Klein, Argento and The Stendhal Syndrome" in Andy Black (ed), Necronomicon: The Journal of Horror and Erotic Cinema: Book Two, London: Creation Books, 1998, pp.&nbsp;99–109.

== References ==

 

== External links ==

*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 