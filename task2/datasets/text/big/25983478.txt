Un mari à prix fixe
{{Infobox film
| name           = Un mari à prix fixe
| image          = 
| caption        = 
| director       = Claude de Givray
| producer       = Christine Gouze-Rénal
| writer         = Roger Hanin
| starring       = Anna Karina
| music          = 
| cinematography = Raymond Lemoigne
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = France
| language       = French
| budget         = 
}}

Un mari à prix fixe is a 1965 French film directed by Claude de Givray and starring Anna Karina.   

==Cast==
* Anna Karina - Béatrice Reinhoff
* Roger Hanin - Romain de Brétigny
* Gabrielle Dorziat - Mme Reinhoff, the mother
* Hubert Noël - Norbert Besson
* Gregor von Rezzori - Konrad Reinhoff
* Marcel Charvey - Me Luxeuil
* Guy dAvout
* Christian de Tillière
* Henry Gicquel
* Max Montavon
* Michel Peyrelon
* Marcelle Tassencourt - Gertrude Luxeuil
* Colette Teissèdre - Jeanne Pierre Vernier - the beekeeper

==References==
 

==External links==
* 

 
 
 
 
 
 
 