Never Trouble Trouble
{{Infobox film
| name =  Never Trouble Trouble
| image =
| image_size =
| caption =
| director = Lupino Lane
| producer = Lupino Lane
| writer =  George Dewhurst   Lauri Wylie 
| narrator = Jack Hobbs   Wallace Lupino
| music = 
| cinematography = 
| editing = 
| studio = Lupino Lane Productions 
| distributor = Producers Distributing Corporation 
| released = 1931
| runtime = 75 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Jack Hobbs. It was made at Cricklewood Studios. 

==Cast==
*  Lupino Lane as Olvier Crawford  
* Renee Clama as Pam Tweet   Jack Hobbs as Jimmie Dawson  
* Wallace Lupino as Mr. Tweet  
* Iris Ashley as Gloria Baxter  
* Dennis Hoey as Stranger  
* Wally Patch as Bill Hainton 
* Lola Hunt as Mrs. Hainton 
* Barry Lupino as Tompkins  
* George Dewhurst as Inspector Stevens 
* Syd Crossley as Minor role
* Merle Oberon as Minor role
* Tom Shale as Minor role

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
 

 

 
 
 
 
 
 
 


 