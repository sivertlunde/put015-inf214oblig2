Love at First Fight (film)
 
{{Infobox film
| name           = Love at First Fight
| image          = Les Combattants poster.jpg
| caption        = French theatrical poster
| director       = Thomas Cailley
| producer       = Pierre Guyard 
| writer         = Thomas Cailley Claude Le Pape
| starring       = Adèle Haenel Kévin Azaïs
| music          = Lionel Flairs Benoît Rault Philippe Deshaies 
| cinematography = David Cailley 
| editing        = Lilian Corbeille 
| distributor    = Haut et Court
| released       =  
| runtime        = 98 minutes
| country        = France
| language       = French
| budget         = $3 million   
}}
 Best Actress, Most Promising Best First Feature Film.      

==Cast==
* Adèle Haenel as Madeleine
* Kévin Azaïs as Arnaud Labrède
* Antoine Laurent as Manu Labrède
* Brigitte Roüan as Hélène Labrède 
* William Lebghil as Xavier  
* Thibault Berducat as Victor  
* Nicolas Wanczycki as Lieutenant Schliefer 
* Frederic Pellegeay as Le recruteur 
* Steve Tientcheu as Adjudant Ruiz
* Franc Bruneau as Le conseiller funéraire

==Accolades==

{| class="wikitable plainrowheaders sortable"
|- style="background:#ccc; text-align:center;" Award / Film Festival Category
! Recipients and nominees Result
|- Cairo International 2014 Cairo International Film Festival  Best Actress
|Adèle Haenel
|  
|- 2014 Cannes Film Festival   FIPRESCI Prize (Directors Fortnight) Thomas Cailley
|  
|- International Confederation Art Cinema Award Thomas Cailley
|  
|- SACD Prize (Directors Fortnight) Thomas Cailley
|  
|-
| Europa Cinemas Label Award Thomas Cailley
|  
|-
| Caméra dOr
| Thomas Cailley
|  
|- 40th César Awards  Best Film
|Love at First Fight
|  
|- Best Director Thomas Cailley
|  
|- Best Actress
|Adèle Haenel
|  
|- Most Promising Actor
|Kévin Azaïs
|  
|- Best Original Screenplay Thomas Cailley and Claude Le Pape
|  
|- Best First Feature Film
|Love at First Fight
|  
|- Best Editing Lilian Corbeille
|  
|- Best Sound
|Jean-Luc Audy, Guillaume Bouchateau and Niels Barletta
|  
|- Best Music Written for a Film Lionel Flairs, Benoît Rault and Philippe Deshaies
|  
|- Globes de Cristal Award Best Film
|Love at First Fight
|  
|- Best Actress
|Adèle Haenel
|  
|- Louis Delluc 2014 Louis Delluc Prize  Best First Film Thomas Cailley
|  
|- 20th Lumières Awards  Best Actress
|Adèle Haenel 
|  
|- Most Promising Actor
|Kévin Azaïs
|  
|- Best First Film
|Love at First Fight
|  
|- French Syndicate Prix du Syndicat Français de la Critique de Cinéma   French Syndicate Best First Film
|Love at First Fight
|  
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 
 