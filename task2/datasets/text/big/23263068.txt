Chandramukhi Pranasakhi
{{Infobox film
| name           = Chandramukhi Pranasakhi
| image          = Chandramuki Pranasakhi poster.jpg
| caption        = Film poster
| director       = Seetharama Karanth
| producer       = N. K. Prakash Babu
| writer         = Seetharama Karanth Prema
| music          = K. Kalyan
| cinematography = R. Giri
| editing        = P. R. Sounder Raj
| studio         = Sri Matha Pictures
| released       =  
| country        = India
| language       = Kannada
| budget         =
}}
 Prema and Third Best Best Music Director (K. Kalyan).  It also turned out be a commercial success.  The film was later remade in Telugu language as Naalo Unna Prema, starring Laya and Jagapathi Babu.

== Plot ==
Susheel (Ramesh Aravind) falls in love with a singer, Sahana (Prema) on seeing her sing in a music programme. He then finds her among the guests at his friends wedding and writes her a love letter. However, the letter is read by Sahanas sister, Bhavana and replies to it not taking it seriously, but later falls in love with Susheel.

==Cast==
* Ramesh Aravind as Susheel Prema as Sahana Bhavana
* Srinivasa Murthy
* Vaishali Kasaravalli

==Soundtrack==
{{Infobox album
| Name        = Chandramukhi Pranasakhi
| Type        = Soundtrack
| Artist      = K. Kalyan
| Cover       = Chandramukhi Pranasakhi album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 6 May 1999
| Recorded    = Feature film soundtrack
| Length      = 41:19
| Label       = Lahari Music
| Producer    =
| Last album  =
| This album  =
| Next album  =
}}

K. Kalyan composed the background score for the film and the soundtracks, also writing the lyrics for all the soundtracks. The album has eight soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 41:19
| lyrics_credits = no
| title1 = Nenapugala Maathu Madhura
| lyrics1 = 
| extra1 = S. P. Balasubramanyam, K. S. Chithra
| length1 = 5:02
| title2 = Manase O Manase
| lyrics2 =
| extra2 = K. S. Chithra, Badri Prasad
| length2 = 5:08
| title3 = Aralo Hunnime
| lyrics3 = 
| extra3 = S. P. Balasubramanyam
| length3 = 4:49
| title4 = El El Mallige
| lyrics4 = 
| extra4 = S. P. Balasubramanyam, K. S. Chithra, Bhutto
| length4 = 4:56
| title5 = Modala Prema Patrave
| lyrics5 = 
| extra5 = K. S. Chithra, S. P. Balasubramanyam
| length5 = 6:46
| title6 = Chiguru Bombeye
| lyrics6 = 
| extra6 = K. S. Chithra
| length6 = 4:49
| title7 = Chandramukhi Hoy
| lyrics7 = 
| extra7 = Chorus
| length7 = 4:45
| title8 = Ondu Prema Pallakkiya Mele
| lyrics8 = 
| extra8 = K. S. Chithra, S. P. Balasubramanyam
| length8 = 5:04
}}

==Awards==
;1999–2000 Karnataka State Film Awards Third Best Film Best Music Director – K. Kalyan

==References==
 

==External links==
*  

 
 
 
 

 