Camp Slaughter
{{Infobox film
| name           = Camp Slaughter
| image          = Campslaughter.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by Screamkings Productions
| film name      = Camp Daze
| director       = Alex Pucci
| producer       = Alex Pucci   Pete Jacelone
| writer         = 
| screenplay     = Draven Gonzales
| story          = Alex Pucci
| based on       = 
| narrator       = 
| starring       = Kyle Lupo   Matt Dallas   Miles Davis   Jon Fleming   Eric McIntire   Joanna Suhl   Bethany Taylor   Anika C. McFall
| music          = Brad Fowler
| cinematography = Pete Jacelone   Jonathan Williams
| editing        = James Conant   Jonathan Williams
| studio         = Screamkings Productions
| distributor    = Screamkings Productions   Lightning Home Entertainment
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $100,000
| gross          = 
}}
 science fiction horror film written and directed by Alex Pucci, and co-written by Draven Gonzales.

== Plot ==
 SUV breaks down after night comes unusually early. All the electronics fail to work, and the group is thrown into hysterics when screams emanate from the surrounding forest, and the vehicle is pelted with debris, prompting them to spend the night in it.
 anachronistic the place is) someone murders any campers who go off on their own. That night, the killer rampages through the facilities, butchering everyone except the quartet. At dawn, the travelers awaken to discover that nothing appears to be wrong, and that everyone is alive again.

Daniel and Ivan, a pair of counselors who are aware of what is going on, find the four, and explain that the camp is in a time loop, stuck repeating the day of the killing spree. To prove they are telling the truth, the counselors take the others to witness the first murder, a strangulation in the woods, which they have never been able to stop, despite their best efforts. The time-displaced four try to leave on their own, but the SUV will not appear until nightfall, and walking away just brings them back to Hiawatha. Daniel and Ivan state that they are limited in what they can do, but with the help of outsiders, they may be able to break the loop, and move on to whatever fate awaits them.

The night of the massacre, it is revealed that Daniel and Ivan are the murderers, and that they manipulated Michelle and Ruben, a pair of outcasts, into helping them with their thrill killing. The psychopaths intend to have the travelers take their place in the cycle, which they believe they can achieve by murdering them, so they can get out. Mario and Vade die, but Angela and Jen manage to kill Daniel and Ivan. The girls go to the SUV, where Lou, the Groundskeeping|groundskeeper, is attacking Michelle and Ruben. Lou snaps Michelles neck, and exposits that he took out the perpetrators of the original massacre minutes after it occurred. A wounded Ruben then shoots an arrow into Angelas chest, and is stomped to death by Lou as Jen escapes in the SUV.

Three years later, Jen has become a successful writer, and while in her office one day, she receives an email. It is from Daniel and Ivan, who have written that they cannot wait to meet their "favorite author" soon.

== Cast ==
 
 
* Kyle Lupo as Daniel
* Anika McFall as Jen
* Joanna Suhl as Angela
* Jon Fleming as Ivan
* Eric McIntire as Vade
* Matt Dallas as Mario
* Bethany Taylor as Michelle
* Miles Davis as Ruben
* Ashley Gomes as Nicole
 
* Philip Jess as Jay
* Ikaika Kahoano as Patrick
* Jessica Sonneborn as Elizabeth
* Troy Andersen as Tommy
* Brendan Bradley as Paul Marq
* Kyle Langan as Wesley
* Jesse Gurtis as Mark
* Amanda Gallagher as Linda
* Jim Marlowe as Lou
 

== Reception ==
 slasher movies from that time  ". 

Sticky Red gave Camp Slaughter a one and half out of five, and said that while the film had a lot to offer, it was brought down by budgetary constraints, poor quality sound, gratuitous scenes, obnoxious characters, unoriginal and sloppy kills, bad acting, confusing editing and cinematography, nonsensical plot twists, and a weak ending. 
 DTV slasher film that claimed it was recapturing the old school methods, but failed miserably."  

== References ==

 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 