The King's Daughters
 
 
{{Infobox film
| name           = The Kings Daughters
| image          = Saint-Cyr 2000 film.jpg
| caption        = Film poster
| director       = Patricia Mazuy
| producer       = Helga Bähr Diana Elbaum Denis Freyd
| writer         = Patricia Mazuy Yves Dangerfield Yves Thomas
| starring       = Isabelle Huppert
| music          = John Cale 
| cinematography = Thomas Mauch
| editing        = Ludo Troch
| distributor    = 
| released       =  
| runtime        = 119 minutes
| country        = France
| language       = French
| budget         = 
}}
 period drama film directed by Patricia Mazuy. It was screened in the Un Certain Regard section at the 2000 Cannes Film Festival.    It was adapted from the novel La maison d’Esther by Yves Dangerfield.

==Plot==
In March 1685, Louis XIV’s final wife Madame de Maintenon wishes to set up a boarding school for young daughters of noble families that have fallen on hard times, the Maison royale de Saint-Louis, a school where girls receive a pious but liberal education. The first difficulty is that the students from the provinces all speak different regional languages and dialects and the first task is to teach them all to speak a standardised Parisian French. 

After a few years of indifference, the school’s first aims prove impossible to attain. An important crisis arises from a performance by the students of an extract from Iphigenie by Jean Racine|Racine. This provokes too much passion among the actors and so Madame de Maintenon asks Racine to write her a play for her students that praises virtue – this proves to be Esther (drama)|Esther. The students put on the new play and, when the king and his court attend the production, Madame de Maintenon realises that this had made the nobles of the court view her protégées as targets for seduction and marriage. Marriage proposals mount up and one nobleman even manages to break into the school.

Madame de Maintenon decides to impose stricter rules and plunges into religion in an attempt to expiate her past. She asks an abbot to help her keep students on the right Christian moral path and keep them safe from the world. Instead of turning its students into an elite for the world outside, the school falls prey to realities, cuts itself off from reality and falls apart – the film ends with its final failure and closure.

==Cast==
* Isabelle Huppert as Madame de Maintenon
* Jean-Pierre Kalfon as Louis XIV
* Simon Reggiani as The Abbot
* Jean-François Balmer as Racine
* Anne Marev as Madame de Brinon
* Ingrid Heiderscheidt as Sylvine de la Maisonfort
* Nina Meurisse as Lucie de Fontenelle
* Morgane Moré as Anne de Grandcamp
* Bernard Waver as Gobelin
* Jérémie Renier as François de Réans
* Jeanne Le Bigot as Lucie (child)
* Mathilde Lechasles as Anne (child)
* Alain Hinard as First soldier

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
*  

 
 
 
 
 
 