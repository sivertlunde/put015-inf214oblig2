Colonel March Investigates
{{Infobox film
| name           = Colonel March Investigates
| image          = Film_"Colonel_March_Investigates"_(1953).jpg
| image_size     =
| caption        =
| director       = Cy Endfield
| producer       = Donald Ginsberg
| screenplay     = Leo Davis
| narrator       =
| starring       = Boris Karloff
| music          = John Lanchbery
| cinematography =
| editing        = Stanley Willis
| distributor    =
| released       = 1955
| runtime        = 70 minutes
| country        = UK
| language       = English
| budget         =
}}
Colonel March Investigates is a 1955 British film consisting of three episodes of the TV series Colonel March of Scotland Yard.  Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 362-365   These episodes being Hot Money, Death In The Dressing Room and The New Invisible Man.  

==Plot==
Karloff, in black eye patch and cloak, is Colonel March: head of the Department of Queer Complaints at Scotland Yard. He is an investigator of unusual criminal cases and activities. The film sees him solve a bank robbery (for which an innocent man was framed) and two murders involving complex tricks and disguises.   

==Cast==
*Colonel March -	Boris Karloff
*Ames -	Ewan Roberts
*Cabot -	Richard Wattis
*John Parrish -	John Hewer
*Joan Forsythe -	Sheila Burrell
*Jim Hartley -	Anthony Forwood
*Betty Hartley -	Patricia Owens
*Mr. Bowlder -	Ronald Leigh-Hunt
*Marjorie Dawson -	Joan Sims

==Critical reception==
TV Guide wrote, "the scripts are nothing special, but Karloff is a joy to watch, as usual."  
==References==
 
==External links==
*  at IMDB

 
 
 