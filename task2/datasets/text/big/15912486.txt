Solino
{{Infobox film
| name = Solino
| image = Solino.jpg
| caption = German DVD cover
| director = Fatih Akin
| writer = Ruth Toma
| starring = Barnaby Metschurat Moritz Bleibtreu Gigi Savoia Antonella Attili Gianluca Milano
| country = Italy Germany
| runtime = 124 minutes German Italian Italian
}}  German movie directed by Fatih Akın and starring Moritz Bleibtreu, Barnaby Metschurat, Gigi Savoia and Antonella Attili.

==Plot==
The movie portrays the story of an Italian family emigrated in Germany in the 1970s. Romano (Gigi Savoia), the father, decides to open a pizzeria  which, by mutual decision with the wife Rosa (Antonella Attili), will call Solino, leaving his sons Gigi and Giancarlo to work there. A hostile relationship comes to life between the father and his sons, which will end up in the escape of the boys from family.

==Cast==
{| class="wikitable"
|- bgcolor="#efefef"
! Actor !! Role
|-
| Moritz Bleibtreu || Giancarlo Amato
|-
| Barnaby Metschurat || Gigi Amato
|-
| Gigi Savoia || Romano Amato   
|-
| Antonella Attili || Rosa Amat
|-
| Gianluca Milano || 
|}

==Notes==
*(1)http://movies.nytimes.com/movie/284376/Solino/overview
*(2)http://www.timeout.com/film/reviews/74931/Solino.html
*(3)http://www.br-online.de/kultur-szene/film/kino/0308/00414/

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 

 