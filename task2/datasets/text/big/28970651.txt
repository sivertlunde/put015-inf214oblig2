Mumbhai Connection
{{ infobox film
| name           = Mumbhai Connection
| image          = Mumbhai_Connection_Poster_1.jpg
| caption        = Theatrical release poster
| director       = Atlanta Nagendra
| producer       = Abbas Moloo, Nawal Parwal, Sathya Narayanan Rafiq Batcha and Amber Sharma
| co producer    = Amber Sharma 
| screenplay     = Atlanta Nagendra
| Presenter      = Mowgli Productions Pvt Ltd
| starring       = Rafiq Batcha  Srinivas Dick Mays Alieesa P. Badresia 
| music          = 
| cinematography =
| editing        =
| studio         = Mowgli Productions Pvt Ltd Rafiq Media Productions De Arte Studios
| distributor    = Mowgli Productions Pvt Ltd
| released       =  
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Mumbhai Connection is a 2011 Hindi-language film shot entirely in Atlanta, USA. The film is set to release theatrically in August 2014  and is presented by Mowgli Productions Pvt Ltd.           It is directed by Atlanta Nagendra, who also wrote the screenplay. 
   
It is produced by Abbas Moloo, Nawal Parwal, Sathya Narayanan and Rafiq Batcha. The music director is Praveen Duth Stephen.  
    

== Synopsis ==
An Indian salesman, Faisal, joins an Atlanta IT company only to learn that it is run by the Mumbai mafia. Worse, he is forced to sell IT services to the Atlanta mafia. Even worse, they are not buying! Now Faisals life is on the line and the only way he can get out alive is by going boldly where no salesman has gone before.    

== Cast ==
*   ... Faisal Khan
*   ... Kal
*   ... Gambino
*   ... Tara Chrissy Chambers  ... Angela
*   ... Igor 
*   ... Vinny

==Release==
Mumbhai connection has been official selection for 8 film festivals.             
*  
*  
*  
*  
*  
*  
*  
*  
*  

==Accolades==
*  
*  
* Winner - Best Key Art Design 10th Urban Mediamakers Film Festival 2011
* Winner - 3rd Best Film in Feature Film Category 10th Urban Mediamakers Film Festival 2011

== References ==
 

==External links==
*  
*  
*  

 