So Little Time (film)
{{Infobox film
| name           = So Little Time
| image          = solittletime1952.jpg
| image_size     =
| caption        = German DVD cover
| director       = Compton Bennett
| producer       = Aubrey Baring Maxwell Seton
| writer         = John Cresswell
| starring       = Marius Goring Maria Schell
| music          = Louis Levy
| cinematography = Oswald Morris
| editing        = Vladimir Sagovsky ABPC
| distributor    = Associated British-Pathé
| released       = 1952
| runtime        = 88 min.
| country        = UK
| language       = English
| gross          = £91,096 (UK) 
}}
 1952 British World War II romance drama, directed by Compton Bennett and starring Marius Goring and Maria Schell.  The film is based on the novel Je ne suis pas une héroïne by French author Noelle Henry.  So Little Time is unusual for its time in portraying its German characters in a mainly sympathetic manner, while the Belgian Resistance characters are depicted in an aggressive, almost gangster-type light.  So soon after the war, this was not a narrative viewpoint British audiences and critics expected in a British film and there was considerable protest about the films content. 

==Plot==
In occupied Belgium during World War II, the chateau where Nicole de Malvines (Schell) lives with her mother (Gabrielle Dorziat) is partially requisitioned for use by German forces.  Among those billeted there is Colonel Hohensee (Goring), a ruthlessly efficient officer.  Having lost several male members of her family in the war, the proud and outspoken Nicole holds the Germans in contempt and has no hesitation in making her feelings clear.

Nicole and Hohensee discover a mutual love of music, particularly the piano.  This gradually brings them together and despite their differences and the inherent danger of the situation to both, they fall in love; they begin travelling to Brussels to attend concerts and recitals, acutely aware of the need to be discreet and the risks involved in being seen socialising with one another.  Matters become more complicated when members of the Belgian Resistance target Nicole to steal documentation from Hohensee to pass over to them, making clear that non-cooperation is not an option.

The couple realise that in one way or another the relationship is doomed, and are told by a sympathetic observer who has noticed their love to make the most of it while they can because there is "so little time".  Inevitably they are betrayed and have to face being parted forever.  She is shot and unable to reconcile himself to the situation,  Hohensee shoots himself.

==Cast==
* Marius Goring as Commander Hohensee
* Maria Schell as Nicole de Malvines
* Lucie Mannheim as Lotte Schönberg
* Gabrielle Dorziat as Mme. de Malvines
* Barbara Mullen as Anna John Bailey as Phillipe de Malvines
* David Hurst as Baumann
* Oscar Quitak as Gerard
* Andrée Melly as Paulette
* Stanley van Beers as Professor Perronet

==Home media==
So Little Time has never been released commercially in its original form on VHS or DVD. However, a dubbed German-language version under the title Wenn das Herz spricht (When the Heart Speaks) was released to the German market in 2005.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 