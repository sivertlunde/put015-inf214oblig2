Fados
{{Infobox Film
| name           = Fados
| image          =
| caption        = Theatrical release poster
| director       = Carlos Saura
| producer       = Ivan Dias  Luis Galvao Teles  Antonio Saura
| writer         = Carlos Saura Ivan Dias
| starring       = Mariza  Camane  Carlos Do Carmo  Cuca Roseta  Catarina Moura Lila Downs
| cinematography = Jose Luis Lopez-Linares Eduardo Serra
| editing        = Julia Juaniz
| music          = Carlos do Carmo
| studio         = Fado Filmes Duvideo Zebra Producciones Ventas Internacionales Latido Films
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 88 minutes
| country        = Portugal
| language       = Portuguese
}} Spanish film directed by  Carlos Saura. The film, a fusion of cinema, song, dance and instrumental numbers, explores Portugals most emblematic musical genre, fado, and its spirit of saudade (melancholy).

Using Lisbon as its iconic backdrop, the film explores the intricate relationship between the music and the city, and Fados evolution over the years from its African and Brazilian origins up to the new wave of modern Fadistas.

Under the musical supervision of Carlos do Carmo, Fados completed Sauras musical trilogy form with Flamenco (1995 film)|Flamenco (1995) and Tango (1998 film)|Tango (1998). Saura deploys mirrors, back projections, lighting effects, and lush colors to frame each song.

Fados contains homages to  Maria Severa, Alfredo Marceneiro, and Amália Rodrigues, as well as turns by modern stars like Mariza and Camané. Saura expands the songs (which traditionally involve just a singer and a guitarist) with dance and encompasses other nationalities of Portugal’s former colonies and idioms (such as hip hop, flamenco and reggae).

==External links==
*  
*  
*  

 

 
 
 
 
 


 
 