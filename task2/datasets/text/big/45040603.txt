The Homebreaker
{{Infobox film
| name           = The Homebreaker
| image          = 
| alt            = 
| caption        =
| director       = Victor Schertzinger
| producer       = Thomas H. Ince
| screenplay     = John Lynch R. Cecil Smith 
| starring       = Dorothy Dalton Douglas MacLean Edwin Stevens Frank Leigh Beverly Travis Nora Johnson
| music          = 
| cinematography = John Stumar
| editor         = W. Duncan Mansfield
| studio         = Thomas H. Ince Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Comedy comedy film directed by Victor Schertzinger and written by John Lynch and R. Cecil Smith. The film stars Dorothy Dalton, Douglas MacLean, Edwin Stevens, Frank Leigh, Beverly Travis and Nora Johnson. The film was released on April 20, 1919, by Paramount Pictures.   It is presumed to be a lost film. 

==Plot==
 

==Cast==
*Dorothy Dalton as Mary Marbury
*Douglas MacLean as Raymond Abbott
*Edwin Stevens as Jonas Abbott
*Frank Leigh as Fernando Poyntier
*Beverly Travis as Marcia Poyntier
*Nora Johnson as Lois Abbott
*Mollie McConnell as Mrs. White

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 
 
 

 