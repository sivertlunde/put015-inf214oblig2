Nargess (film)
 
{{Infobox film
| name = Nargess
| director =Rakhshan Bani-Etemad
| writer =Rakhshan Bani-Etemad Fereydoun Jeyrani
| starring = Atefeh Razavi Farimah Farjami Abolfazl Pour-Arab
| producer =Rakhshan Bani Etemad Arman Film
| released =Iran 1992
| language =Persian
| runtime =
| distributor =
}}
 1992 drama film, the fourth feature of Iranian director Rakhshan Bani-Etemad. Nargess is the first film in what has become known as the City Trilogy. The other two films are The May Lady and Under the Skin of the City. The three films are considered by many to be Bani-Etemads best films to date. All three were shot by the acclaimed Iranian cinematographer Hossein Jafarian who is known for his realistic lighting style.

==Plot summary==
The movie tells the story of a love triangle, in which a young man is loved by two women. Adel, a thief, is loved by aging Afaq also a thief. The story of the gang of thieves changes direction when Adel meets Nargess, a beautiful young girl from an impoverished family.

==External links==
*  

 
 
 
 
 
 
 