S&Man
 
{{Infobox film
| name           = S&Man
| image          = S&Man.jpg
| caption        =
| director       = J. T. Petty
| producer       = Jason Kliot Lawrence Mattis Joan Vicente
| screenplay     = J. T. Petty
| starring       = Carol J. Clover Debbie D Michelle Glick Erik Marcisak
| music          = On Filmore Darin Gray Glenn Kotche
| cinematography = Patrick McGraw
| editing        = Andy Grieve
| studio         = HDNet Films
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 84 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
}} indie horror filmmakers and other horror experts, including Erik Marcisak, Bill Zebub, Fred Vogel, Carol J. Clover, and Debbie D., as well as a scripted plot that comes into focus in the films second half.

The film discusses why some people enjoy underground horror films involving fetishes. Although the film is classified as a documentary, there is a fictional subplot which helps advance the films theme by showing what some people really think about the genre.

S&Man was shown at the reRun Gastropub Theater in New York City, film festivals, and released on home video. The film has received praise and criticism; although some reviews are strongly against the nature of the film, most reviews were positive.

==Summary==
S&Man is about voyeurism as it relates to underground films. Interviews with horror film directors and Carol J. Clover, a writer on the horror genre, are weaved between clips from horror films. In the beginning of the film, clips show the directors and Clover commenting on a horror film that is not shown onscreen and with the title not mentioned. The film then moves on to Pettys meetings with Eric Rost (Erik Marcisak), Bill Zebub, and Fred Vogel. He interviews these men and actress Debbie D., while clips from horror films and facts and opinions provided by Clover about the genre are interspersed.

The film has a fictional subplot, which questions whether or not Rosts works are actual snuff films. Pettys real life friendship with Marcisak  takes a fictional turn in the film; Petty gets close to discovering Rosts secret. At this point, Petty is a performer in his own film, and tension arises between him and Rost. During filming, Petty begins to suspect that Rosts work, in which women are "bound, gagged, tortured, and eventually butchered"    shows actual snuff.

As Petty gets further into his investigation, Rosts answers become more evasive. Petty suspects that Eric actually kills women for his snuff films, a suspicion further exacerbated when Rost says that he will relay Pettys contact information to the women instead of giving their contact information to Petty. It is not clear at the films end whether Rosts films are actual snuff in the fictional subplots context. However, it is known that he spies on them for a long period of time before asking them to participate.

==Analysis== sadistic and masochistic while discussing paraphilias; they state that most voyeurs may seem creepy, but are not dangerous. 

==Cast==
*J. T. Petty is a film director, producer, editor, and screenwriter.  His films include Soft for Digging and he wrote the books Clemency Pogue: Fairy Killer and The Squampkin Patch. 

*Bill Zebub, one of the interviewees in the film, is the director of low-budget fetish exploitation films such as Jesus Christ: Serial Rapist.  He has directed twelve films and acted in two.  Bill Zebub is shown saying, "I dont shoot movies to make art; I shoot movies so perverts will give me money." 

*Fred Vogel, another interviewee, is the head of Toetag Pictures. He began his career as a special effects artist and later directed August Underground and August Undergrounds Mordum. 

*Erik Marcisak plays the part of Eric Rost, one of the main characters and interviewees. In the film, Rost is the director of the fictional S&MAN video series. The actual Marcisak is an actor, comedian, game designer, writer, and producer. 

*Debbie D. is an actress who stars in low-budget fetish exploitation films; she is interviewed along with a sexologist and her husband, a forensic psychologist. 

* , is featured providing facts about horror filmmaking.    Clover is a professor of rhetoric, film, and Scandinavian history. Her books include The Peoples Plot: Film, Narrative, and the Adversarial Imagination, Old Norse-Icelandic Literature: A Critical Guide, and The Medieval Saga. 

==Production==
{{Quote box quote  = We have to react to real violence and real snuff footage all the time, news channels show decapitations and bodies dragged in the streets. source = J. T. Petty,   width  = 50% align  = right
}}
S&Man  director J. T. Petty originally intended the film to focus on a man who lived near his childhood home; the man often spied on, and filmed, his neighbors. The footage led to an indictment towards "the peeper", who recorded 191 videotapes of Pettys childhood neighborhood. The indictment implied the footage should be viewed in court, but the people in the neighborhood opposed this owing to privacy concerns.   Petty said of the 191 videotapes, "I admired the peeping tom; he had made movies that were frightening and titillating and real."  The idea of using a camera to record his neighbors house for hours fascinated him so much that he decided to direct a film about it.       Although he had already secured funding, Petty was left without a subject since "the peeper" wanted nothing to do with the film.  Petty decided to focus on three directors, who did not direct films aimed at the general public; they directed simulated snuff films, involving murder and sexual assault.     Petty found the directors&nbsp;— Zebub, Vogel, and Marcisak, Pettys friend who played the fictional director Rost&nbsp;— at the Chiller Convention, an underground horror event.   

The films title comes from the fictional S&Man video series directed by Rost. In the videos, Rost stalks women for a long time and, after learning about them, he asks them if they want to be a part of his films. All of the videos are first person with no dialogue.  The film references other films, including Peeping Tom,  , Kill the Scream Queen, and The Crucifier. 

==Release==

===Film festivals and theater===
S&Man premiered in the Toronto International Film Festivals "Midnight Madness",  a section which features a variety of films from new directors,  in 2006. Although Twitch Film reported that the films release at the Toronto International Film Festival caused controversy both offline and online,  TMZ.com|TMZ reported that the film was popular at the film festival.  It was shown again at South by Southwest in Austin, Texas, in 2006, and UnionDocs in Brooklyn, New York, in 2009.      S&Man had a theatrical release at the reRun Gastropub Theater in New York City in 2010. 

===Home video===
The DVD and Blu-ray of the film were released on October 12, 2010.  The DVD  and Blu-ray Disc|Blu-ray release each include two commentary tracks, one with a conversation between J. T. Petty and Erik Marcisak and the other with Bill Zebub. There are also deleted scenes, extended scenes, The Complete S&Man– Episode 11&nbsp;– a fictional film partially incorporated in S&Man&nbsp;– in its entirety, a clip from the underground horror film August Undergrounds Mordum, and film trailers. 

===Reception===
Jeannette Catsoulis of The New York Times called the film "a queasy glimpse into bargain-basement sleaze."  DVD Talk critic Adam Tyner was indifferent about the film while watching it, but later concluded that the film is "pretty brilliant".  Gordon Sullivan of DVD Verdict thought that the film is ambitious and offers "interesting insights into the modern consumption of horror."  Joe Leydon of Variety (magazine)|Variety described the film as an "uncomfortably close look at underground horror", and speculated that it may gain a cult following.  David Harley of Bloody Disgusting called S&Man a "must-watch" for anyone that has been scared while watching a horror film without being able to describe why.  In response to the film, Richard Corliss of Time (magazine)|Time questioned whether we can always believe what we see. 

Allan Darts review of S&Man for Fangoria was mixed. Although he described the film as "thought-provoking", he did not like the focus on whether Eric Rosts films are actual snuff.  Casey Broadwater of Blu-ray.com also disliked the fictional portion, but said that "S&Man delivers an occasionally insightful dialectic about the psychological nature of horror. 

Meg Hewings from Hour Community described the film as "a doc that flails in a dark, nihilistic pool" and deemed its fans "geeky", "twisted", "unstable", or "delusional".  A TMZ.com|TMZ article stated that S&Man "impolitely asked horror genre aficionados to question just why they so enjoy watching human suffering so much."   

==References==
 

==External links==
* 

 

 
 
 
 
 