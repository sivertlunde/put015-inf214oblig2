The Milpitas Monster
{{Infobox Film
| name           = The Milpitas Monster
| director       = Robert L. Burrill
| producer       = 
| writer         = David E. Boston
| starring       = Paul Frees Doug Hagdahl Krazy George Henderson Bill Guest Priscilla House 
| released       = 1976 in film|1976(1978)
| runtime        = 80 mins
| country        = USA English
}}
 thriller film created in 1973-1976, directed by Robert L. Burrill. The monster was created by environmental pollution.

== Plot ==
When a landfill is overfull, and pollution reaches its maximum, a monster is born. Made from garbage, and bearing a resemblance to a giant fly, the Milpitas Monster eats garbage to grow bigger. Some high school students find out about the monster and attempt to destroy it.

==Trivia==
*It was released on DVD with the alternate title "The Mutant Beast",
*The movie was filmed, stars, and was written by high school students,
*The scene where the monster takes a girl hostage, and climbs the electric tower, is an homage to the 1933 film King Kong,
*The monster makes the same screech as Rodan from the Godzilla franchise. It also has an origin similar to Hedorah, from the same franchise.
*Six years before Milpitas Monster was released, The Monster That Everybody Created, produced by Mike Carroll and written by Doug Brusig from Peterson High School, was the first Bay Area high school smog monster movie from a garbage dump, i.e., the Sunnyvale dump.

== External links ==
*  

 

 
 
 

http://www.milpitasmonster.com/
https://www.youtube.com/watch?v=ABSDzlE1He4
 