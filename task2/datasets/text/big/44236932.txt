Ex Machina (film)
 Ex Machina}}
 
 
{{Infobox film
| name           = Ex Machina 
| image          = Ex-machina-uk-poster.jpg
| alt            =
| caption        = British release poster
| director       = Alex Garland
| producer       = {{Plainlist| Andrew Macdonald
* Allon Reich}}
| writer         = Alex Garland
| starring       = {{Plainlist|
* Domhnall Gleeson
* Alicia Vikander
* Sonoya Mizuno
* Oscar Isaac}}
| music          = {{Plainlist|
* Ben Salisbury
* Geoff Barrow
}}
| cinematography = Rob Hardy Mark Day
| studio         = {{Plainlist|
* DNA Films Film4
* Scott Rudin Productions
}}
| distributor    = A24 Films
| released       =   
| runtime        = 108 minutes  
| country        = United Kingdom
| language       = English
| budget         = Euro|€11 million (United States dollar|$16.4 million)
| gross          = $19.1 million 
}}
 science fiction thriller film written and directed by author and screenwriter Alex Garland, making his directorial debut. The film stars Domhnall Gleeson, Alicia Vikander and Oscar Isaac.      

== Plot ==
Caleb is a computer programmer working for Bluebook, the worlds most popular search engine. He is chosen in what is stated to be a company lottery to meet and visit the companys CEO, Nathan, at his secluded house in the mountains. Nathan is an eccentric genius whose residence, it transpires, is also a research facility. The only other person at the compound is Kyoko, a Japanese woman who is Nathans assistant. 

Nathan wants Caleb to spend the week performing a test on a humanoid robot named Ava. Nathan explains that in the classical Turing test of artificial intelligence, the tester can interrogate the computer and is only shown its responses without knowing whether they come from an artificial intelligence or a human. If its replies cannot be distinguished from the replies a real human would give, the test is passed, and the computer must be considered intelligent. But in this new test, Nathan intentionally makes it obvious to Caleb from the beginning that Ava is an android - if Caleb still relates to Ava as "human", then passing the test will be truly meaningful. 

Nathan admits that he surreptitiously used the clout of his company to collect audiovisual data from billions of people, accumulating their most intimate experiences, and that he loaded these data to Avas brain. Nathan also claims that Caleb was deliberately selected to test Ava due to his exceptional abilities and knowledge of artificial intelligence.

Caleb gradually develops an attachment to Ava, who appears to have real human emotions. But Ava privately tells him that Nathan cannot be trusted. Caleb also learns that Nathans androids are treated as prisoners, with an automated security system keeping them confined to the research facility. 

During one of the meetings with Ava, she anxiously asks Caleb what will happen if she fails Nathans Turing test, which he cannot answer. But later, Nathan tells Caleb that Ava’s memories will be erased when developing the next AI version, even if she passes the test, which would be tantamount to killing her. Nathan also predicts that one day artificial intelligence will surpass humans and consider them inferior primitives. Caleb also notices behaviour from Nathan that he interprets as emotional abuse towards his androids. In addition, Caleb learns that even Kyoko, whom he thought was human, is also one of the androids. Caleb has sympathy for Ava and decides to help her escape from the compound. 
 hacks into the security system of the building and rigs it so that all doors will be open when Ava triggers an electrical blackout at 10pm the following night. Nathan, who becomes aware of Caleb’s plans and his emotional relationship with Ava, warns Caleb that Ava does not love him and that she is merely using him for the purpose of escaping from the facility. On this occasion, Nathan informs Caleb that the fact that Ava was able to emotionally manipulate Caleb into helping her escape, indicates that Ava passed the real test, and that his research is now a historic breakthrough. But Caleb has managed to stay one step ahead of Nathan and when the power blackout is triggered, the security system is disabled. When the automatic doors are unlocked, Ava gets out of her room and she attempts to escape the facility. Nathan tries to capture Ava but she kills him with the help of Kyoko, even though he breaks her arm. Ava then borrows components from other android prototypes to repair her arm and to make improvements to her body to acquire the appearance of a perfect human. 

After putting on a dress, she silently exits the building, leaving the confused and mortified Caleb locked inside Nathans office to die, giving him no importance, just as Nathan had warned. Ava is picked up the next morning by the helicopter meant for Caleb, and she enters human society with no one knowing she is a robot, with a profoundly happy facial expression.

== Cast ==

* Domhnall Gleeson as Caleb Smith
* Alicia Vikander as Ava the A.I.
* Oscar Isaac as Nathan Bateman
* Sonoya Mizuno as Kyoko
* Symara A. Templeman as Jasmine
* Elina Alminas as Amber

== Production ==
Garland wanted the movie to be made on a smallest possible budget for creative reasons:

"The reason to keep this small was simply because this story and the way I wanted to execute the story required pretty much total creative freedom, and the way to get creative freedom at the level of film I work in is to make it as cheaply as you can. So, it’s to do with protecting the movie basically. I mean, you can imagine if somebody said -- and by the way, people have said this kind of thing -- if we inject a car chase or something like that, you know, or a big fight in a helicopter and then the helicopter crashes and people jump out and carry on the fight on the ground and all that kind of stuff, it would interfere hugely tonally and actually thematically on every level with this film. So, the budget and the contained quality were about having the freedom to do it properly." 

=== Filming ===

The film was shot at Juvet Landscape Hotel  in Valldalen, Norway.

=== Music ===
 musical score for Ex Machina was composed by Ben Salisbury and Geoff Barrow, who previously worked with Alex Garland on Dredd (2012). 
 LP and Compact Disc UK release in February 2015 by Invada Records. 

== Release ==
 Universal Pictures.  The film screened on 14 March 2015 at South by Southwest prior to a theatrical release in the United States on 10 April 2015 by A24 Films.  

=== Critical reception ===

 
Ex Machina has received very positive reviews.  On Rotten Tomatoes, the film has a rating of 91%, based on 174 reviews, with an average rating of 8.1/10. The sites critical consensus reads, "Ex Machina leans heavier on ideas than effects, but its still a visually polished piece of work — and an uncommonly engaging sci-fi feature."  On Metacritic, the film has a score of 78 out of 100, based on 42 critics, indicating "generally favorable reviews". 

The magazine New Scientist in a multi-page review said, "It is a rare thing to see a movie about science that takes no prisoners intellectually...   is a stylish, spare and cerebral psycho-techno thriller, which gives a much needed shot in the arm for smart science fiction."  IGN reviewer Chris Tilly gave the movie a 9.0 out of 10 Amazing score, saying "Anchored by three dazzling central performances, it’s a stunning directorial debut from Alex Garland that’s essential viewing for anyone with even a passing interest in where technology is taking us." 

== See also ==

* AI box
* Artificial general intelligence
* Knowledge argument
* Sentience

== References ==

 

== External links ==

*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 