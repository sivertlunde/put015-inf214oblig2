Geronimo: An American Legend
{{Infobox film 
| name           = Geronimo: An American Legend
| image          = Geronimo film.jpg
| caption        = Theatrical release poster Walter Hill Walter Hill
| writer         = John Milius
| starring       = Wes Studi Jason Patric Robert Duvall Gene Hackman Matt Damon Pato Hoffmann
| narrator       = Matt Damon    
| music          = Ry Cooder
| cinematography = Lloyd Ahern
| editing        = Donn Aron Carmel Davies
| distributor    = Columbia Pictures
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $35 million
| gross          = $18,635,620     228,707 admissions (France) 
}}
 Walter Hill from a screenplay by John Milius and starring Wes Studi, Jason Patric, Gene Hackman, Robert Duvall  and Matt Damon. It was released on December 10, 1993 by Columbia Pictures.

==Plot==
The film follows the events leading up to the capture of Geronimo in 1886. The Apache Indians have reluctantly agreed to settle on a U.S. government approved Indian reservation|reservation. Not all the Apaches are able to adapt to the life of corn farmers, and one in particular, Geronimo, is restless. Pushed over the edge by broken promises and unnecessary actions by the government, Geronimo and 30 other warriors form an attack team which humiliates the government by evading capture, while reclaiming what is rightfully theirs. The plot centers upon Charles Gatewood (Jason Patric), the U.S. cavalry lieutenant charged with capturing the elusive Apache leader with the assistance of a scout leader Al Sieber (Robert Duvall) and a young graduate Britton Davis (Matt Damon). Gatewood is torn by a grudging respect for Geronimo and his people, and his duty to his country. But then all the white men in the film have respect for Geronimo, even as they are trying to hunt him down and kill him. Brigadier General George Crook (Gene Hackman), charged with overseeing the forced settlement of the Apaches on reservations has nothing but admiration for Geronimo.

==Cast==
* Wes Studi as Geronimo   
* Jason Patric as 1st Lt. Charles B. Gatewood  
* Gene Hackman as Brig. Gen. George Crook 
* Robert Duvall as Chief of Scouts Al Sieber 
* Matt Damon as 2nd Lt. Britton Davis   
* Pato Hoffmann as The Dreamer
* Rodney A. Grant as Mangas 
* Kevin Tighe as Brig. Gen. Nelson A. Miles 
* Steve Reevis as Chato 
* Carlos Palomino as Sgt. Turkey 
* Victor Aaron as Ulzana 
* Stuart Proud Eagle Grant as Sgt. Dutchy  Scott Wilson as Redondo
* Stephen McHattie as Schoonover 
* John Finn as Capt. Hentig 
* Lee de Broux as City Marshal Joe Hawkins 
* Rino Thunder as Old Nana

==Production==
Walter Hill had a development deal at Carolco. They approached him wanting to make a Western that focused on an Indian and Hill was enthusiastic. He initially considering doing a movie on   hired to write a draft. "I like Geronimo just as he was, a human predator," said Milius. Geronimo, Still With a Few Rough Edges: Its Still Geronimo, But With Edges
By ERIK ECKHOLM. New York Times (1923-Current file)   05 Dec 1993: H19.  

Hill said the title of the film should have been The Geronimo War. "The conception was you make the film from the last time he came in and broke off and was sent away," he said. "The last time he broke off the reservations. This had been a recurring pattern. I thought that would be more accurate."  

According to Hill, Milius screenplay was more inclusive of Geronimos early years and Milius was reluctant to revise it so he had it rewritten by Hill and Larry Gross.  "This movie certainly presents a heroic view of Geronimo," said Hill. 

The movie was eventually transferred from Carolco to Columbia. The View From the Top: Why Lethal Weapon 3 Should Outlast Alien3
Pond, Steve. The Washington Post (1974-Current file)   29 May 1992: D6. 

The part of Al Sieber was expanded when Robert Duvall was cast.  The character of Sieber was meant to ride off into the sunset at the end of the movie but during filming Hill felt that the running time was going to be too long and so decided to kill off the character. "If Id known I was going to die I might not have done the movie," said Duvall. "Ive died nine times in films." However part of Duvalls deal to make the film did mean his company, Butchers Run Films, signed a deal with Columbia. Robert Duvall Strikes Sweet Deal: Geronimo lands him lead role in his own production company INTERVIEW
Bonnie Churchill Special to The Christian Science Monitor. The Christian Science Monitor (1908-Current file)   11 Jan 1994: 15.  

The film was shot in Utah, Tucson, Arizona, and Culver City, California. 

Another film on Geronimo came out around the same time, a made for TV movie.

==Reception== Chris Carpenter, Doug Hemphill, Bill W. Benton and Lee Orloff).   

The film had a mixed reception from critics    but was praised by Native American groups.  Philip French of Londons Observer called it one of the greatest Westerns of all time. CINEMA Hills warrior charge
The Observer (1901- 2003)   16 Oct 1994: C9 

Walter Hill later expressed dissatisfaction with the title:
 It’s not about Geronimo. It should have been called The Geronimo War... It’s as much about the Army as it is Geronimo. That came out of my reading of historical accounts, and realizing that so much of what we think we know about the Indian campaigns is wrong. The Army is generally depicted as the enemy of the Apache, but in many cases, the people who were most sympathetic to their plight were those soldiers.  
===Box office===
The film was  a box office bomb. Earning only $18 million on a $35 million budget .  The movie dropped to number 7 the following week. 

==See also==
* Geronimo (TV film)|Geronimo (1993 TV film)

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 