Dragon Ball Z: Bojack Unbound
 
{{Infobox film
| name            = Dragon Ball Z: Bojack Unbound
| image           = DBZ THE MOVIE NO. 9 (wiki).jpg
| caption         = Japanese DVD cover art
| director        = Yoshihiro Ueda
| producer        = Chiaki Imada (Executive Producer) Yoshio Anzai (Shueisha)
| writer          = Takao Koyama (screenplay) Akira Toriyama (story)
| starring        = See  
| music           = Shunsuke Kikuchi
| cinematography  = 
| editing         = 
| distributor     = 
| released        =   (Japan) August 17, 2004 (North America and Europe)
| runtime         = 50 minutes gross     = ¥2.05 billion ($18.7 million)
|}}
Dragon Ball Z: Bojack Unbound, known in Japan as  , is the ninth   movie. The English version was dubbed by   and Dragon Ball Z Season 8 on February 9, 2009. It was re-released again to DVD on December 9, 2011 in a movie 4-pack with the previous three films.

==Plot== King Kai, Goku teleported Cell to King Kais planet, killing him, the seal has broken, allowing Bojack to reap destruction upon the Earth.

The movie starts with a big martial arts tournament in which 200 elite fighters will compete, including Yamcha, Tien, Piccolo, Gohan, Future Trunks, and Krillin. The winner will get to face the famous champion "Mr. Hercule" (also known as "Mr. Satan" in the Japanese version). Goku is shown in Other World, watching the tournament with King Kai. The first round consists of 8 contests - 25 fighters are put on each fighting "stage", and the last one standing on each stage will advance to the semi-finals. Tien, Piccolo, Gohan, Krillin, and Future Trunks are among the 8 who advance. As the advancing fighters names are announced, Mr. Hercule is shown getting increasingly nervous, as he is aware of the prowess of these fighters. He begins looking for a way to back out of the fighting, eventually claiming to have a "stomach ache". In the meantime, the semis pit Tien against Future Trunks and Krillin against Piccolo. Future Trunks beats Tien. Piccolo is disgusted by the lack of a challenge in this supposedly "elite" contest and forfeits against Krillin. Gohan, Krillin, and Future Trunks are among the 4 who advance to the finals, in which each of them will compete against a fighter from elsewhere in the galaxy. However, as the alien fighters are revealed, the fight promoter realizes that these are not the fighters that he had recruited for the contest.

Everyone soon begins to realize that there is something wrong as Future Trunks is challenged by Kogu, Krillin faces Zangya, and Gohan confronts Bujin. During this time, Mr. Hercule is in the rest room, thinking about how he got himself in this mess, but after looking at his belt he realizes that the people need him and, in an uncharacteristically brave mood he goes out and gets slammed into a space pod and sent to the fighting. During his fight against Kogu, Trunks transforms into a Super Saiyan, shatters Kogus sword, and shoves his fist right through Kogus abdomen, thus killing him. As he turns to leave, he is struck down in one blow by an unknown foe. Far away, Vegeta feels this power and knows something is wrong. Gohan continues his fight with Bujin until he is led out to where Krillin and Trunks are. After inspecting his fallen friend, Bojack introduces himself and tells Gohan his plans to rule the universe as his revenge. Yamcha and Tien turn up to fight but are defeated by Bujin, Bido and Zangya. Seeing his friends fallen, Gohan transforms into a Super Saiyan and fights Bojacks henchmen while Bojack watches on in amusement. Gohan is beaten down and Bojack powers up an attack to finish him off, although it is deflected by Piccolo, who has come to Gohans aid. Trunks comes back into action and they both challenge Bojack. After a period of fighting Trunkss sword appears; Trunks catches it as Vegeta enters the fight and transforms into a Super Saiyan, quickly setting on Bojack. During this, Trunks thinks that Vegeta would only ever fight when it meant him surpassing Gokus strength, and Trunks realizes that Vegeta has truly changed. While Vegeta fights Bojack, the rest fight against his surviving henchmen. During his fight against Vegeta, Bojack beats Vegeta down and transforms. Gohan rushes to Piccolos aid, but Piccolo tells him to not worry about him and fight Bojack. Bojack challenges Gohan to a fight and beats him thanks to the aid of Bido and Zangyas Untrapped ropes, which drains Gohans energy away, but Gohan is freed when Mr. Hercules incoming pod distracts them all. Annoyed at this, Bojack powers up an attack and blasts Mr. Hercules pod, causing him to fall to the ground. Trying to save him, Gohan is struck down by Bojacks henchmen.
 Bido and Bujin each in one-hit, breaking them both in half, and Zangya is killed when Bojack uses her as a shield from Gohan. The angry young Saiyan then slams his fist into Bojacks stomach, blowing a hole in his stomach and out through his back. As a last resort, Bojack powers up his Galactic Buster and Gohan prepares a Super Kamehameha. The energies clash together and in the blinding light Bojack is killed. Gohan, exhausted, falls backwards laughing as Goku praises him from Other World, and King Kai jokes that Goku broke the rules of going back to Earth while dead. After the events, everyone is in hospital watching the news that Mr. Hercule is a "true hero" who defeated the alien intruders; this results in several jokes as they laugh. As in Super Android 13!, the movie closes with Piccolo and Vegeta on the roof, arms folded and backs turned to each other, isolating themselves from the celebration. During the closing credits, "Ginga o Koete Raijingu High" plays with some pictures showing any of the Dragon Ball Z characters doing something being displayed on the left.

Finally, this graph is surrounded as though in a frame by several of the main characters, including Krillin, Trunks, Roshi, and Yamcha, all dressed in different colored suits, similar to the Toriyama drawn picture before.

==New characters==

===Bojack===
  is the main antagonist of the movie Bojack Unbound. His name is derived from the Japanese word bōjakubujin, which means "arrogance" or "audacity." Bojack is the leader of the Galaxy Soldiers, consisting of his henchmen Bido, Kogu, Zangya and Bujin. Bojack could only be impeded by being locked inside of a star by the Kais of the universe, where he would remain for thousands of years. However, with King Kais death following the destruction of his planet during the Cell Games, the link holding Bojack trapped is shattered. He infiltrates a World Martial Arts Tournament being held for a rich man sons birthday, hoping to kill all of  Earths fighters. Vegeta battles him as a Super Saiyan, only to lose and have Gohan transform into a Super Saiyan 2, which kills Bido, Kogu and Bujin. Bojack kills Zangya in order to stay alive, but Gohan launches a Super Kamehameha that ends Bojacks existence.

==Cast==
{| class="wikitable"
|-
! Character Name
! Voice Actor (Japanese)
! Voice Actor (English)
|-
| Gohan (Dragon Ball)|Gohan|| Masako Nozawa || Stephanie Nadolny
|-
| Goku || Masako Nozawa || Sean Schemmel
|- Piccolo || Toshio Furukawa || Christopher Sabat
|-
| Krillin || Mayumi Tanaka || Sonny Strait
|- Tien || Hirotaka Suzuoki || John Burgmeier
|-
| Yamcha || Toru Furuya || Christopher Sabat
|-
| Future Trunks|| Takeshi Kusao || Eric Vale
|-
| Vegeta|| Ryo Horikawa || Christopher Sabat
|-
| Bulma|| Hiromi Tsuru || Tiffany Vollmer
|-
| Master Roshi || Kôhei Miyauchi || Mike McFarland
|- Naoko Watanabe|| Cynthia Cranz
|- Oolong || Naoki Tatsuta|| Bradford Jackson
|-
| Chiaotzu || Hiroko Emori|| Monika Antonelli
|-
| Mr. Satan || Daisuke Ghori|| Chris Rager
|- Bujin || Hiroko Emori|| Christopher Bevins
|- Bido || Hisao Egawa|| Robert McCollum
|-
| Zangya || Tomoko Maruo|| Colleen Clinkenbeard
|-
| Kogu || Toshiyuki Morikawa|| Iman Nademzadeh
|-
| Bojack || Tesshō Genda|| Bob Carter
|-
| Gyosan Money || Naoki Tatsuta|| Grant James
|-
| Okkane Money || || Jamie Marchi
|-
| Dollar Money || || Colleen Clinkenbeard
|-
| Dosukoi || || Bradford Jackson
|-
| Udo || || Brice Armstrong
|- King Kai || Joji Yanami|| Sean Schemmel
|-
| Narrator || Joji Yanami || Kyle Hebert
|}

==Music==
*OP (Opening Theme)
*# "CHA-LA HEAD-CHA-LA" (Chara Hetchara) (OP animation 1)
*#* Lyrics:  , Vocals: Hironobu Kageyama

*ED (Ending Theme)
*#  
*#* Lyrics: Dai Satou, Music: Chiho Kiyoka, Arrangement: Kenji Yamamoto, Vocals: Hironobu Kageyama

The score for the English-language version was composed by Mark Menza; however the remastered release contains an alternate audio track containing the English dialogue and Japanese background music.

==External links==
 
*   of Toei Animation
*  
*  
*  
*  

 

 
 
 
 
 
 
 