Dushman Devta
 
{{Infobox film
| name           =Dushman Devta
| image          = DushmanDevtafilm.jpg
| image_size     = 
| caption        = 
| director       =Anil Ganguly
| producer       =Anil Ganguly
| writer         =Anwar Khan
|story=
| narrator       = 
| starring       =Dharmendra Dimple Kapadia Sonam Anjaan  (lyrics) 
| cinematography = Pratap Singh
| editing        = 
| distributor    = 
| released       = 1991
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Dushman Devta  is a 1991 Bollywood film directed by Anil Ganguly, and starring Dharmendra, Dimple Kapadia and Sonam.

==Plot==

A small town in rural India is being terrorized by wild animals, and bandits. No one is able to stop them from killing and looting the residents. The only one who was able to stand against the bandits was master Dina Nath, but he was arrested and is imprisoned. When Dina Nath returns home, he befriends a young man by the name of Shiva, and asks him to protect this town. Subsequently, Dina Nath is killed. Shiva is blamed for Dina Naths death by the townspeople, severely beaten, and left for dead. But some compassionate townspeople rescue him as they think he is Lord Shiva reincarnated to save their town. They do not know that Shiva is an escaped convict, and by playing God to the simple-minded townspeople, he is merely whiling for time, so that he can carry out his very own secret agenda.

==Cast==
* Dharmendra as Shiva
* Dimple Kapadia as Gauri
* Aditya Pancholi as Suraj
* Sonam as Ganga
* Sadashiv Amrapurkar as Raja
* Gulshan Grover as Durjan-Garjan Singh
* Jankidas as Pandit
* Shreeram Lagoo as Master Dina Nath
* Ajit Vachani as Inspector Basermal
* Sulbha Deshpande as Mrs. Dina Nath
==External links==
*  
*  

 
 
 


 