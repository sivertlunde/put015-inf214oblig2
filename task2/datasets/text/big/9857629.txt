Angel (1984 film)
{{Infobox Film |
name = Angel|
image = Angelmovie1984.png |
director = Robert Vincent ONeill|
producer = Donald P. Borchers|
writer = Joseph Michael Cala Robert Vincent ONeill|
| starring = {{Plainlist|
* Cliff Gorman
* Susan Tyrrell
* Dick Shawn
* Rory Calhoun
* Donna Wilkes
}} cinematography = Andrew Davis|
music = Craig Safan|
editing = Charles Bornstein |
distributor = New World Pictures|
released= January 13, 1984 |
runtime = 94 minutes |
country = United States |
language = English |
budget = $3 million |
gross = $17,488,564
}}

Angel is a 1984 film directed by Robert Vincent ONeill, and written by ONeill with Joseph Michael Cala.   

It was released by New World Pictures. 

==Plot==
Fifteen-year-old honor student Molly Stewart ( . Some members of her street family are the aging movie cowboy Kit Carson (Rory Calhoun), street performer Yoyo Charlie (Steven M. Porter), transvestite Mae (Dick Shawn), and fellow hookers Crystal (Donna McDaniel) and Lana (Graem McGavin).

All is not well as hookers are being killed by a psycho-necrophiliac (John Diehl). Los Angeles Police Lt. Andrews (Cliff Gorman) has been assigned to the case, but he has no leads. Tragedy strikes Angels group of friends when Crystal becomes a victim.

The next day at school, Molly sees teacher Patricia Allen (Elaine Giftos), who is concerned about Mollys lack of extra-curricular activities. Molly explains that her mother has been paralyzed by a stroke and she needs to go home immediately after school every day to take care of her.

Lt. Andrews advises the girls to work in pairs. Angel is working with her partner, Lana, when Lana takes a potential client to a motel room she and Angel share. A couple of hours later, when Angel shows up at the room with a client of her own, she finds the body of Lana in the shower where the killer left it. Based on a description Angel gives the police, a sketch is made and the killer is brought in for a lineup. Angel recognizes him, but he escapes, shooting his way out of the police station.

Andrews takes Molly/Angel home to speak with her parents, but he finds out that Mollys father had left nine years ago and that her mother had abandoned Molly three years ago. Molly keeps up the fiction of her mother being around so that she will not be sent to a foster home and believes someday her father will return. Thus, since the age of 12, she has paid the rent by prostitution.

Despite Andrewss warnings to stay off the street, Angel/Molly purchases a pistol and goes back to work. But that night, one of her schoolmates, Ric Sawyer, and some of his friends see Angel, and before long, the whole school commences to realize that Molly spends her evenings as a Hollywood hooker.

The next day, Ms. Allen visits Mollys apartment, insisting on seeing Mollys mother. Mae pretends to be Mollys mother, but Allen sees through this subterfuge. Later, Mae is still at the apartment when the killer shows up and stabs Mae to death.

Andrews and Molly return to her apartment to find Mae dead. Molly heads out on the streets with a huge, long-barreled magnum to avenge Mae, and Andrews goes after her. After a fight and chase, Carson, whom Andrews has enlisted to help, shoots the killer; Molly, Andrews, and a wounded Carson walk off together as the scene fades to black.

==Release==
Angel was released theatrically in the United States by New World Pictures on January 13, 1984.  Despite not opening in the top 5 at the box office, Angel grossed $2.2 million its opening weekend.  The film managed to stay in the box office top ten for several months, making it a sleeper hit and eventually earning $17,488,564.  It was New Worlds highest grossing picture that year. 

==Sequels==
The film was popular enough that three  , and Angel 4: Undercover (1993), starring Darlene Vogel. All were commercial failures at the box office.

==Production==
Lead actress Donna Wilkes was actually twenty-four years old, when she played the role of fifteen-year-old Molly Stuart in this film. She prepared for the role by talking to real-life street prostitutes on Hollywood Boulevard, and spent time with the Los Angeles Police Department, and in various halfway houses for underage children living on the streets of Los Angeles.
 score to this film in less than a week. The film premiered at the Hollywood Pacific Theatre on Hollywood Boulevard. A fact sheet inside the theatre, prior to its closure in 1994, confirmed this. The theatre also features in the climax of the film, where a gun-toting Angel opens fire on the killer, terrifying patrons outside. 

The motel in the film is the El Royale Motel at 11117 Ventura Boulevard, Studio City.  Most of the film was shot at real locations on and around Hollywood Boulevard.

==DVD release==
In 2003, Anchor Bay Entertainment released the Region 1 DVD box set of the first three Angel films entitled The Angel Collection. 

==References==
 

==External links==
*  
*  
*  
*   at  

 
 
 
 
 
 
 