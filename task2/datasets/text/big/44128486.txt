Ente Sathrukkal
{{Infobox film 
| name           = Ente Sathrukkal
| image          =
| caption        =
| director       = S Babu
| producer       =
| writer         =
| screenplay     =
| starring       = Jayan
| music          = M. K. Arjunan
| cinematography =
| editing        =
| studio         = Amardeep Films
| distributor    = Amardeep Films
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by S Babu. The film stars  and Jayan in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Jayan

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anuraagam oru daham || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Baale edi baale || K. J. Yesudas, Ambili || Poovachal Khader || 
|-
| 3 || Neelakkaadil peelikkaadil || K. J. Yesudas, Ambili, Chorus || Poovachal Khader || 
|-
| 4 || Paavaka jwaalakal uyarunnu || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 