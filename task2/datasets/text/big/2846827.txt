Doraemon: Nobita Drifts in the Universe
{{Infobox film
| name           = Doraemon: Nobita Drifts in the Universe
| image          = Nobita Drifts in the Universe.jpg
| caption        =
| director       = Tsutomu Shibayama
| producer       =
| writer         =
| narrator       =
| starring       = Nobuyo Ōyama Noriko Ohara Michiko Nomura Kaneta Kimotsuki Kazuya Tatekabe
| music          = Senri Ohe
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 93 mins.
| country        = Japan
| language       = Japanese
| budget         =
| gross          = ¥2.00 billion  ($ 19,900,000)
}}

  is the second   (1988) and  (1998). This movie commemorates the 20th anniversary of the Doraemon film series. It was released on March 6, 1999, in a bill with     and  Funny candy of Okashinana!? .

== Plot == Galactic Empire) is trying to take over Earth. Of course, our heroes joins another army (based on the Rebel Alliance) to stop the antagonists.

== Cast ==
{| class="wikitable"
|-
! Character
! Voice
|-
| Doraemon
| Nobuyo Ōyama
|-
| Nobita Nobi
| Noriko Ohara
|-
| Shizuka Minamoto
| Michiko Nomura
|-
| Takeshi "Gian" Goda
| Kazuya Tatekabe
|-
| Suneo Honekawa
| Kaneta Kimotsuki
|-
| Lian
| Fuyumi Shiraishi
|-
| Log
| Masako Nozawa
|-
| Freya
| Mayumi Shō
|-
| Gorogoro
| Tesshō Genda
|-
| Commander Liebert
| Kinryū Arimoto
|-
| Angolmois
| Kenji Utsumi
|-
| Rebel soldier
| Tōru Ōkawa
|-
| Lians Mother
| Miki Itō
|-
| Mazura
| Takeshi Watabe
|-
| ATC
| Toshihiko Nakajima
|-
| Cadre
| Masashi Hirose
|-
| Councilors
| Masashi Sugawara Chafurin Keisuke
|-
| Boys
| Yasuhiro Takato Kōichi Tōchika
|}

== Music == Speed

==References==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 


 