Da Thadiya
{{Infobox film
| name              = Da Thadiya
| image             = Da Thadiya Poster.jpg
| caption           = Theatrical Poster
| director          = Aashiq Abu
| producer          = Anto Joseph
| writer            = Syam Pushkaran Abhilash S Kumar Dileesh Nair
| starring          = Sekhar Menon Sreenath Bhasi Ann Augustine Nivin Pauly Arundathi Nag Maniyanpilla Raju Edavela Babu
| music             = Bijibal

| studio            = 
| distributor       = 
| released          =   
| runtime           = 
| country           = India
| language          = Malayalam
| budget            =
| gross             =
 }}
Da Thadiya ( , Hey Fatso) is a 2012 Malayalam romantic comedy film directed by Aashiq Abu, and starring Sekhar Menon, Sreenath Bhasi and Ann Augustine in the lead roles.  The cast also includes Nivin Pauly, Arundathi Nag, Maniyanpilla Raju and Edavela Babu. The film was produced by Anto Joseph in association with OPM cinema.  DJ Sekhar Menon who had worked as the same in around 500 stages makes his film debut as an actor.

The film released on 21 December 2012 to mixed to positive reviews from critics and audience. As part of the movies promotional activities, free tickets were offered toil appeared in the posters of this film.

==Plot== 
The film tells the love story of an obese youth named Luke John Prakash. Luke comes from a wealthy family. He is in love with a girl named Ann Mary Thadikkaran , his childhood friend whose parents are working in an insurance company.One day,Ann Marys parents get a transfer to Ernakulam and she along with her parents shift to Ernakulam.Many years later, Ann Mary comes back to meet Luke in his house.One day, ann Mary takes Luke to a restaurant.He fall and breaks the chair.Realizing she is not so fond of his fatness, Luke embarks on an Ayurvedic treatment run by Rahul Vaidyar to loose his weight.One day,Luke escapes from the Ashram to meet Ann Mary on a Christmas Eve. There he is shocked to realize that Ann Mary is the agent of the Ashram and she does not love him anymore.Luke joins his fathers political party Prakasham Parakkatte and finally becomes a mayor of his place.Ann marys father comes to meet Luke in the Mayors office.He is shocked to realize that Rahul Vaidyar is a corrupt person and he is making money by  decreasing the self esteem of many people through advertisements and posters.Luke beats rahul badly and warns him to stop his buisness.The story comes back to the present as Luke is waiting for Ann Mary in the beach . Ann Mary says sorry to Luke and proposes him. But to everyones surprise Luke rejects her proposal telling he is not her dream boy.

==Cast==
* Sekhar Menon as Luke John Prakash
* Sreenath Bhasi as Sunny Jose Prakash
* Ann Augustine as Ann Mary Thadikkaran
* Nivin Pauly as Rahul Vaidyar
* Arundathi Nag as Knight Rider
* Maniyanpilla Raju as John Prakash
* Edavela Babu as Jose Prakash
* Vinay Forrt as Shanthanoo
* Jins Varghese as Anish Naduvilethu (main TV Reporter)
* Jayaraj Warrier as Dasan, former Mayor of Kochi
* Basil Kothamangalam as Lukes friend 
* Jonathan Matthew as Little Luke
* Thesni Khan as Mrs Thadikkaran Kunchan as Mr. Thadikkaran
* Gayathri as Lukes mother

==Soundtrack==
The film features score and soundtrack composed by Bijibal.  The song "Enthanu Bhai" which was officially released in YouTube in November was a tremendous success and registered 4 lakh hits in the Internet within a month of its release.  There has been allegations that the song was inspired by Sneha Khanwalkars songs "Yere..." and "tung tung". Aashiq Abu is not perturbed by these allegations. "I prefer not to respond to such allegations. I want the listeners to decide for themselves. Our song has been electronically created for the film, while Yere... has original sounds," he says. 

==Critical reception==
Rediff gave the movie 3/5 stars, stating  the film shows "If your goal is the common good of the people then you can achieve unprecedented success" and that "The message that slimming clinics are mostly run by quacks who wish to cash in on the vulnerability of fat people does not get diluted in the fun and frolics."  

Veeyen at nowrunning.com gave the movie 3/5 stars 
 

== References ==
 

==External links==
 
 
 
 
 
 