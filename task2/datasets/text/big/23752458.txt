Personal Affair
{{Infobox film
| name           =  Personal Affair
| image          =  "Personal_Affair".jpg
| image_size     =
| caption        = 
| director       = Anthony Pelissier 
| producer       = Anthony Darnsborough
| writer         =  Lesley Storm from her play "A Days Mischief"
| narrator       = 
| starring       = Gene Tierney Leo Genn Glynis Johns
| music          = William Alwyn
| cinematography    = Reginald H. Wyer
| editing        =  Frederick Wilson
| studio         = Two Cities Films
| distributor    = United Artists
| released       = 21 December 1953 (UK) 15 October 1954 (US)
| runtime        = 82 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Personal Affair is a 1953 British drama film directed by Anthony Pelissier and starring Gene Tierney, Leo Genn, and Glynis Johns.    It was made at Pinewood Studios by Two Cities Films.

==Plot summary== crush on her Latin teacher, Stephen Barlow (Genn).  When Barlows wife Kay (Tierney) finds out about this, she confronts Barbara, who is humiliated and runs off.  Stephen chases after her to try to calm her down near a local river.  

Barbara does not return home to her parents for three days, during which time Stephen is accused by the community of causing her death without any evidence, causing him to lose his job and nearly his marriage.   , makes the situation worse.

==Cast==
* Gene Tierney as Kay Barlow
* Leo Genn as Stephen Barlow
* Glynis Johns as Barbara Vining
* Walter Fitzgerald as Henry Vinings
* Megs Jenkins as Vi Vining Pamela Brown as Aunt Evelyn
* Nanette Newman as Sally 
* Michael Hordern  as Headmaster Griffith  
* Thora Hird  as Mrs. Usher  
* Norah Gorsen as Phoebe

==Critical reaction==

The film was reviewed by Bosley Crowther of the New York Times in the October 23, 1954 edition.  Crowther called the film "a decent, eventually tedious film".   

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 