Vikatakavi
 
{{Infobox film
| name           = Vikatakavi
| image          = Vikatakavi (1984).jpg
| image size     = 
| alt            = 
| caption        =  Hariharan
| producer       = Dr. Balakrishnan George
| writer         = Hariharan Madhavi Mammootty
| music          = Devarajan
| cinematography = Vipin Das
| editing        = M. S. Mani
| studio         = Murali Films Aishwaryadhara
| distributor    = Murali & Benny Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Madhavi and Mammootty in lead roles.  The films music has been composed by Devarajan with lyrics have been penned by P. Bhaskaran.  

==Cast==
* Prem Nazir as Sankunni Nair Madhavi as Santhi
* Mammootty as Usman (driver)
* T. G. Ravi as Sankunnis Uncle
* Oduvil Unnikrishnan  
* Sukumari
* Kuthiravattam Pappu
* Bahadoor
* Vanitha Krishnachandran as Nabeesa

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kadicha chundu || K. J. Yesudas, P. Madhuri || P. Bhaskaran ||
|-
| 2 || Mankappenne || K. J. Yesudas || P. Bhaskaran ||
|-
| 3 || Oru kannil || K. J. Yesudas, P. Madhuri || P. Bhaskaran ||
|-
| 4 || Sankalpa Nandana || K. J. Yesudas, P Susheela || P. Bhaskaran ||
|}

==References==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 
 
 


 