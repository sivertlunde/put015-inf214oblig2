Year of the Dragon (film)
{{Infobox film name = Year of the Dragon image = Year of the dragon poster.jpg caption = Theatrical poster  director = Michael Cimino producer = Dino De Laurentiis based on =   writer = Oliver Stone Michael Cimino
| starring = {{Plainlist|
* Mickey Rourke
* John Lone Ariane
}} music = David Mansfield cinematography = Alex Thomson editing = Françoise Bonnot studio = Dino De Laurentiis Company distributor = Metro-Goldwyn-Mayer released =   runtime = 134 minutes country = United States language = Mandarin Cantonese Polish
|budget =  . Retrieved 2010-04-25.  gross = $18,707,466     . The Numbers. Retrieved 2010-06-02.  
}}

Year of the Dragon is a  . ISBN 0-87951-479-5. action film directed by Michael Cimino, starring Mickey Rourke, Ariane Koizumi and John Lone. The screenplay was written by Cimino and Oliver Stone and adapted from the novel by Robert Daley.

This was Ciminos first film after the infamous failure of Heavens Gate (film)|Heavens Gate (1980). Year of the Dragon is a New York crime drama and an exploration of gangs, the illegal drug trade, ethnic group|ethnicity, racism, and stereotypes.

==Plot Summary==
 
Stanley White (Rourke) is a decorated police captain and Vietnam War veteran assigned to New York Citys Chinatown, Manhattan|Chinatown, where he makes it his personal mission to come down hard on Chinese organized crime.
 triad societies, and as a result of his ambition, creates a high profile for himself and the triads activities. Together, they end the uneasy truce that has existed between the triads and the police precinct, even as they conduct a personal war between one another.

The married captain also becomes romantically involved with Tracy Tzu (Ariane), a television reporter, who comes under brutal attack from the criminals, as does Whites long-suffering wife. This makes him even more determined to destroy the triads, and especially Joey Tai.

White also hires an up-and-coming Chinese rookie cop, Herbert to go undercover as one of Tais restaurant workers. Herbert 
manages to get inside information on a drug shipment but loses his life in doing so when Tai finds out hes a cop. Now that Tai raped Tracy, killed Hebert and his wife, White wastes no time confronting Tai right as the shipment comes in. White is shot
at by one of Tais men in the hand but still kills them while Tai runs away on a train bridge. That leads to a showdown where
White and Tai run at one another while shooting bullets recklessly. White shoots Tai leaving him wounded. Rather than suffer,
Tai asks for Whites gun to commit suicide in front of him.

==Cast==
*Mickey Rourke as Capt. Stanley White
*John Lone as Joey Tai
*Ariane Koizumi as Tracy Tzu (as Ariane)
*Dennis Dun as Herbert Kwong
*Raymond J. Barry as Bukowski (as Ray Barry)
*Caroline Kava as Connie White
*Eddie Jones as McKenna Victor Wong as Harry Yung
*Roza Ng as "the daughter"

==Production==

===Pre-production===

Michael Cimino was approached many times to helm an adaptation of Robert Daleys novel, but consistently turned the opportunity down. When he finally agreed, Cimino realized he was unable to write and direct in the time allotted; The producers already had an approximate start date for the film. He brought in Oliver Stone, whom Cimino met through his producer and friend Joann Carelli, to help him write the script. DVD commentary by director Michael Cimino. Located on the Year of the Dragon 2005 Region 1 DVD.  

Cimino was prompted to seek out Stone after reading, and being impressed by, Stones (at the time) unproduced Platoon (film)|Platoon screenplay. Cimino asked Stone to work on Year of the Dragon for a lower-than-normal wage as part of a quid pro quo deal, namely, that Year of the Dragon producer Dino De Laurentiis would provide the funding for Stone to make Platoon. Stone agreed to this deal, although De Laurentiis later reneged on it, forcing Stone to obtain funding for Platoon elsewhere.

"With Michael, its a 24-hour day," said Stone. "He doesnt really sleep … hes truly an obsessive personality. Hes the most Napoleonic director I ever worked with." Griffin, Nancy (February 10, 2002).  . The New York Observer 16 (6): pp. 1+15+17. Retrieved 2010-10-13.  Cimino did a year and a half of research on the project. 
 final cut The Sicilian sued Cimino over the length of that film. 

===Casting===
Because the production was moving so fast, casting began before the script was completed.  Originally, Stone and Cimino had either  . Retrieved 2010-04-25.  According to Rourke, the difficulty with playing White was making himself appear 15 years older to suit the character.   prowess of Rourke. At first, Rourke did not take his physical training seriously, so Cimino hired a Hells Angel to be Rourkes instructor. Rourke has often quoted in many interviews that he loved working with Cimino despite the disapproved reputation he has earned himself over the years since his previous box office failures, quoting, He was a ball of fire. I hadnt seen anyone quite like him. 

===Shooting===
As with Streets of Fire, most of the film was shot not on location but on soundstages in Wilmington, North Carolina, after meticulous research of various locales which could be passed off as Little China and/or the Orient.   The sets proved realistic enough to fool even Stanley Kubrick, who attended the movies premiere. Cimino actually had to convince the Bronx-born Kubrick that the films exteriors were shot on a sound-stage and not on location. 

Other cities used in filming included New York City, Toronto, Vancouver, Victoria, British Columbia|Victoria, Thailand, Bangkok and Shangirey. Cimino has said he often likes to shoot in different cities, with interiors in one city and exteriors in another. In one scene, Joey Tai and his lawyer walk through a Chinese textile mill, past a guard-rail and into a shoddy apartment building to meet up with two of his assassins. The textile mill was in Bangkok, the guard-rail was in New York and the apartment building was in Wilmington. When one of the script girls commented that the scene "wouldnt cut" (edit seamlessly together), Cimino bet the script girl $1,000 that it would. On showing her the cut, the script girl conceded and Cimino won the bet but refused to take the $1,000. 

Unlike Heavens Gate, Cimino was able to bring the film in on time and on budget. 

===Post production=== politically incorrect. 

==Release== see below). box office flop, costing $24 million but grossing only $18 million through its run. 

{| class="wikitable sortable" 
|+ Box office charts 
|-
! Date
! Rank
! Weekend Gross
! Theaters
! Gross-to-date
|- August 16–18 5
|$4,093,079 982
|$4,093,079
|- August 23–25 5
|$2,864,487 970
|$8,938,692
|- August 30–September 2 7
|$2,597,573 834
|$12,881,875
|- September 6–8 7
|$1,461,768 796
|$14,898,009
|- September 13–15 7
|$958,830 711
|$16,385,510
|}

==Reception==
Year of the Dragon received polarizing reviews upon its release in 1985.  , in contrast, also writing for The New York Times, deplored a lack of "feeling, reason and narrative continuity", under which the actors fared "particularly badly", especially Ariane Koizumi whose role in the movie was "ineffectual". 

 , Roger Ebert of the Chicago Sun-Times wrote that Year of the Dragon was "strongly plotted and moved along with power and efficiency."  Leonard Maltin gave the film two and a half stars, calling it a "Highly charged, arresting melodrama... but nearly drowns in a sea of excess and self-importance."  Pauline Kael of The New Yorker dismissed the film as "hysterical, rabble rousing pulp, the kind that goes over well with subliterate audiences." 

The film has a 60% fresh rating on Rotten Tomatoes. 

===Top Ten lists===
3rd  (in 1985)  - Cahiers du cinéma 

===Legacy===
Quentin Tarantino has praised this film as one of his favorites,  naming its climactic train tracks shoot-out as one of his favorite "Killer Movie Moments" in 2004, remarking, "You forget to breathe during it!". 
 Too Much" singing in a Chinatown, dressed in a red cheongsam; this scene is based upon the film.

==Controversy==
Members of the Chinese American and Asian American communities protested the film,   criticizing the film for its racial stereotyping, widespread xenophobia, (especially the use of the derogatory terms "chinks", "slant-eyed", and "yellow niggers"), and sexism.  Some groups worried that the film would make Chinatown unsafe and cause an economic downturn in the community.  As a result of the controversy, a disclaimer was attached to its opening credits,  which read:

 "This film does not intend to demean or to ignore the many positive features of Asian Americans and specifically Chinese American communities. Any similarity between the depiction in this film and any association, organization, individual or Chinatown that exists in real life is accidental." Dirks, Tim.  . Greatest Films. Retrieved 10-29-10.   

Mariko Tse of the   by calling the film part documentary. Year of the Dragon is about as much a documentary as is a soft drink commercial." Tse, Mariko (September 1, 1985).  . Los Angeles Times. Retrieved 2010-06-10.  In her negative review, Pauline Kael added, "Year of the Dragon isnt much more xenophobic than The Deer Hunter was, but its a lot flabbier; the scenes have no tautness, no definition, and so youre more likely to be conscious of the bigotry." 

Director Cimino responded to the controversy in an interview in Jeune cinéma: "The film was accused of racism, but they didn’t pay attention to what people say in the film. It’s a film which deals with racism, but it’s not a racist film. To deal with this sort of subject, you must inevitably reveal its tendencies. It’s the first time that we deal with the marginalization which the Chinese were subject to. On that subject, people know far too little. Americans discover with surprise that the Chinese were excluded from American citizenship up until 1943. They couldn’t bring their wives to America. Kwong’s speech to Stanley is applauded. For all these reasons, the Chinese loves the film. And the journalists’ negative reactions are perhaps a shield to conceal these unpleasant facts." Camy, Gérard; Viviani, Christian (December/January 1985-1986). "Entretien avec Cimino" (in French). Jeune cinéma (n171). 

==Awards==
The film was nominated for five  . Retrieved 2010-06-13. 

==References==

===Annotations===
 

===Footnotes===
 

===Bibliography===
*Pauline Kael|Kael, Pauline (1989). "The Great White Hope". Hooked. New York, NY: E.P Dutton. pp.&nbsp;31–38. ISBN 0-525-48429-9.

==Further reading==
*Chevrie, Marc (November 1985). "Le point de mire" (in French). Cahiers du cinéma (n377).
*Marchetti, Gina (1993). "Conclusion: The Postmodern Spectacle of Race and Romance in Year of the Dragon". Romance and the "Yellow Peril": Race, Sex, and Discursive Strategies in Hollywood Fiction. Berkeley, CA: University of California Press.
*Marchetti, Gina (1991). "Ethnicity, the Cinema and Cultural Studies". Unspeakable Images: Ethnicity and the American Cinema. Urbana, IL: University of Illinois Press.
*Masson, Alain (November 1985). "L’année du dragon" (in French). Positif (n297).
*Pym, John (December/January 1985-86). "After the Deluge". Sight and Sound (55).
*Toubiana, Serge (December 1985). “Il ny a pas daffaire Cimino” (in French). Cahiers du cinéma (n378).
*Wood, Robin (Summer/Fall 1986). “Hero/Anti-Hero: The Dilemma of ‘Year of the Dragon’”. CineAction! (n6).

==External links==
* 
* 
*  at michaelcimino.fr (unofficial French website)
*  of Year of the Dragon on YouTube
*  from Year of the Dragon on YouTube
*  by John J. Puccio at DVD Town
*  at eFilmCritic.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 