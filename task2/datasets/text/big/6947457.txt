The Deep End (film)
 
{{Infobox Film
| name           = The Deep End
| image          = The-Deep-End-Poster.jpg
| caption        = Theatrical release poster David Siegel
| producer       =  David Siegel   Elizabeth Sanxay Holding (novel)
| starring       = Tilda Swinton   Goran Visnjic   Jonathan Tucker   Josh Lucas 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Fox Searchlight Pictures
| released       = August 8, 2001 (US)   December 14, 2001 (UK)   URL last accessed September 11, 2006. 
| runtime        = 101 minutes 
| country        = United States
| language       = English
| budget         = $3 million
| gross          = $10,031,529
}} David Siegel and Scott McGehee.  It stars Tilda Swinton, Goran Visnjic, Jonathan Tucker and Josh Lucas and was released by Fox Searchlight Pictures. The film was very loosely adapted from the novel The Blank Wall by Elizabeth Sanxay Holding (filmed before by Max Ophüls as The Reckless Moment).  The film premiered in competition at the Sundance Film Festival where English cinematographer Giles Nuttgens won the Best Cinematography award.

==Plot== USS Constellation. She is startled to discover that her son Beau (Tucker), a high school senior, has been having a sexual affair with 30 year-old Reno|Reno, Nevada night club owner Darby Reese (Lucas). Margaret visits Reeses nightclub, The Deep End, to demand that he stay away from her son. That night, Reese secretly visits Beau and the two meet in the boathouse. Beau confronts him about asking his mother for money. The two argue, eventually coming to blows. As Beau returns to the house, Reese leans on a railing, causing it to collapse, and falls into the water, impaling himself on an anchor.

The next morning, Margaret discovers Reeses body on the beach. Margaret removes the body and dumps it in a cove but it is soon discovered and the police investigate it as a homicide.  Soon after, a man named Alek Spera (Visnjic) confronts Margaret with a tape of Darby and Beau having sex. Alek demands $50,000 in 24 hours or he will turn the tape over to the police, which would implicate Beau in Reeses "murder".

Margaret struggles unsuccessfully to get the money. Alek calls Margaret the next day and tells her that she needs to get only $25,000 but Aleks partner, Nagle is convinced she is lying about not being able to raise the money. Nagle corners and beats Margaret but Alek arrives and the two men scuffle, and Alek strangles Nagle. Margaret attempts to take responsibility for Nagles death, but Alek takes the body away in Nagles car. As Margaret and her son drive looking for Nagles car, they see it overturned in a ditch. Margaret attempts to free Alek, who is critically injured. Alek pleads with her to leave before the police arrive. Margaret stays until Alek dies. Back at home, Margaret, in a state of distress, is comforted by Beau. The camera then pulls back, as the scene shifts to the exterior of the house, the audience hears another phone call coming in from the absent husband, which Beaus sister answers. The Halls normal life resumes.

==Cast==
*Tilda Swinton as Margaret Hall
*Goran Visnjic as Alek Spera
*Jonathan Tucker as Beau Hall
*Peter Donat as Jack Hall
*Josh Lucas as Darby Reese
*Raymond J. Barry as Carlie Nagel
*Tamara Hope as Paige Hall
*Jordon Dorrance as Dylan Hall

==Reception==
The Deep End received positive reviews from critics, as it holds an 82% rating on Rotten Tomatoes based on 116 reviews.

==References==
 

==External links==
*  
* 
*  
*  

 
 
 
 
 
 
 
 
 