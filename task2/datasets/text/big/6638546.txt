Deadly Outbreak
{{Infobox Film
| name           = Deadly Outbreak
| image_size     = 
| image	=	Deadly Outbreak FilmPoster.jpeg
| caption        = The fear is spreading
| director       = Rick Avery
| producer       = 
| writer         = Harel Goldstein and Charles Morris Jr. 
| narrator       = 
| starring       = Jeff Speakman, Ron Silver
| music          = 
| cinematography = 
| editing        = Alain Jakubowicz
| distributor    = Nu Image Films
| released       = 1996
| runtime        = 
| country        = 
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Deadly Outbreak (released in certain markets as Deadly Takeover) is an action movie released by US-Israeli company Nu Image in 1996, and directed by former stuntman Rick Avery.

It is set in Israel and follows the formula set by Die Hard rather closely.  Additionally, the last act of the film also borrows heavily from the U.S. blockbuster, Speed (1994 film)|Speed, released just two years earlier.

Dutton Hatfield, played by Jeff Speakman, is working for the American embassy. He finds himself inside a biochemical weapon laboratory when terrorists headed by Colonel Baron (played by TimeCop villain Ron Silver) take over the place. Baron is looking for a sample of a deadly virus that is being developed inside the facility.

== Cast ==
* Jeff Speakman — Sgt. Dutton Hatfield
* Ron Silver — Colonel Baron
* Rochelle Swanson — Dr  Allie Levin
* Jack Adalist — Ramos
* Jonathan Sagall — Gallo

== External links ==
*  
*  


 
 
 
 
 

 