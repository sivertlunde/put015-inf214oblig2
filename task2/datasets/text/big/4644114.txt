The Fanimatrix
 
 

{{Infobox Film
| name = The Fanimatrix 
| image =  
| director = Rajneel Singh
| producer = Steven A. Davis Rajneel Singh 
| writer = Steven A. Davis Rajneel Singh
| starring = Steven A. Davis Farrah Lipsham Fasitua Amosa Vaughan Beckley| Don Davis
| cinematography = Rajneel Singh
| editing = Rajneel Singh
| distributor = Plutonian Shore Productions, Absolute O Productions 
| released = 27 September 2003 
| runtime = 16 min.  English 
| awards = 
| budget = $500US
| followed_by = 
}} action fan film based on The Matrix, released on the Internet on 27 September 2003, written and directed by Steven A. Davis and Rajneel Singh. It stars Steven A. Davis, Farrah Lipsham, Fasitua Amosa, and Vaughan Beckley.  Its name is a deliberate pun using the title The Animatrix and the term fan film.  Hence it is a live-action, fan-made version of The Animatrix.

==Plot==
The short film is set within the Matrix (fictional universe)|Matrix universe, shortly before the discovery of "The One" (in The Matrix).  It tells a story of two rebels, Dante and Medusa, operating out of a ship called the Descartes, and of their fateful mission onto the virtual world of the Matrix.

The film opens with the insertion of Dante and Medusa into the Matrix.  They materialize inside a machining|machine-shop and quickly move across the city while talking to their Operator, who is guiding them on their mission.  The nights objectives are simple: Medusa is to break into a high-security building in order to steal important data.  Dante is to provide a distraction so the Matrix will not discover Medusas presence.
 cyberpunks and gothic subculture|goths.  After dispatching the two Bouncer (doorman)|bouncers, he quickly picks a fight with two goths who mock Dantes "normal" appearance.  The Operator - who is in constant contact with Dante via his cellphone - helps coordinates the fight so it coincides with Medusa breaking into the high-security building and taking out a team of security officers.
 Agent to take care of the situation and it begins chasing Dante.  He leads the Agent on a dangerous wild goose chase cross the city, keeping Medusa free to carry out her work.

Unfortunately things suddenly go wrong.  A security officer, missed by Medusa during her entry, discovers her presence and hits the alarm, meaning she has to flee.  Dantes goose chase now becomes a frantic dash to get to an exit point - the nearest being the machine shop they first appeared in, but he is unable to shake the Agent from his tail.

Dante is trapped and Medusa is out of time.  He realizes he must fight the Agent - even though it means certain death - in order to buy time for Medusa to escape with the information shes hacked.  He tells the Operator to get Medusa out and then, chanting the mantra "free my mind" to himself, he throws himself at the Agent. During an epic kung-fu fight he is able to hold off the Agent until he sees an opportunity to escape.  He decides, though, to not race to freedom, but continues the fight until he is eventually thrown against the machinery and his chest is pierced by a steel pipe, killing him.

As Dante dies, Medusa makes it back to the car and to safety - unaware of Dantes immense sacrifice for her own life.

==Production==
The Fanimatrix was filmed in New Zealand. With an estimated budget of $800NZ ($500US), {{cite web
  | last = Zhou
  | first = Lucy
  | title = Blockbusters On A Budget: Guerrilla Filmmaking On The Fanimatrix
  | date = 23 April 2007
  | url=http://www.usu.co.nz/inunison/features/blockbusters-on-a-budget-guerrilla-filmmaking-on-the-fanimatrix/ CCD Sony Wushu martial arts system used in the feature films. {{cite news 
  | last = Pamatatau 
  | first = Richard
  | title = Viewers rush for net flick 
  | newspaper = The New Zealand Herald
  | date = 3 October 2003
  | url = http://www.nzherald.co.nz/ict-news/news/article.cfm?c_id=55&objectid=3526734
  | accessdate =  }}  Several members of the local stunt industry assisted in the fight sequences and wire-work.  The final product was edited on Adobe Premiere Pro non-linear offline video editing software and premiered online on 27 September 2003, freely hosted by local internet service provider IHUG.

==Cast==
*Dante - Steven A. Davis
*Medusa - Farrah Lipsham
*The Operator - Fasitua Amosa
*The Agent - Vaughan Beckley
*Bald Goth - Mike Edward
*Contact Juggling Goth - Chris Rigby
*Bouncer #1 - Ben Butler-Hogg
*Bouncer #2 - Louis MacAllister
*Security Guard #1 - Andrew Salisbury
*Security Guard #2 - David Fraser
*Security Guard #3 - Dominic Skinner
*Crow - Matt Bennett 
*Pedestrian - Eden Phillips

==References==
 

==External links==
*  The Fanimatrix: Run Program Vimeo Host
* 
*  (defunct)
*  (defunct)
*  on fanfilms.net

 
 
 
 
 
 
 
 
 