Paisa (film)
 
 
{{Infobox film
| name           = Paisa 
| image          = Paisa poster.jpg
| director       = Krishna Vamsi
| producer       = Ramesh Puppala
| writer         = Padmasri K. K. Benoji Patrikeya Nani Catherine Tresa
| music          = Sai Karthik
| cinematography = Santosh Rai
| editing        = M.Thiyagarajan
| studio         = Yellow Flowers
| runtime        = 144 mins
| country        = India
| budget         = 
| gross          = 
| release date   =  
| language       = Telugu

}} Telugu film Krishna Vamsi, Nani and Catherine Tresa in the lead. It was written by Padmasri, K. K. Binojee and Patrikeya.  It deals with the power of money and how the whole world revolves around it.  The film was scheduled to release on 20 December 2013 with an A certificate from censor board for its excessive violence.  However, the film released worldwide on 7 February 2014.  Upon release, the film received mixed reviews from critics but turned an Hit at the Box office. 

==Plot==
A minister of Andhra Pradesh Cabinet named Kalamoorthy is removed from his post after the Central Bureau of Investigation proves his involvement in Liquor Mafia and the Election Commission of India declares the first by election after the main elections of that term in that constituency. While a senior ruling party leader Saradhi (Bharath Reddy) is willing to spend  25 crore for the election, other senior members ask Sanyasi Naidu (Charan Raj) to shell out  50 crores and if he wins in the by election, he would become the Chief Minister of Andhra Pradesh. Since it is his ambition to rule the state, Sanyasi Naidu accepts to spend  50 crores. With the help of his trusted aid, the Assistant Commissioner of Police (ACP), Sanyasi Naidu brings  50 crores to India through Hawala transaction into Old City in Hyderabad. The ACP is tasked with transporting the money to the constituency. On completion of the task and if Sanyasi Naidu becomes the Chief Minister, then the ACP would be promoted as Inspector General (IG) of Police. The ACP goes to the Old city to collect the amount and the money is wrapped in big black plastic bags and is placed in the Deck lid of a Black Innova.

Another story runs parallel to the above plot. Prakash (Nani (actor)|Nani) is a model of an Old city sherwani firm who dreams to become rich. He has such an obsession for money that he pronounces his name as Pracash often. He has two friends, Tabar (Akbar Bin Tabar), a cab driver and Gokul. Noor (Catherine Tresa), who also works in the same firm, is in love with him. However as Prakash is in love with Sweety (Siddhika Sharma), the daughter of Sanyasi Naidu, Noor decides to leave him and marry an old sheikh in order to solve her familys financial problems on the pressure of their creditor and the local don Yadav (R.K.). But Prakash does not know that Sweety is Sanyasi Naidus daughter and Sweety is not aware of Prakashs intentions and takes his advances lightly. When Prakash comes to know about Noors marriage, he beats the sheikh, Yadav and his men in disguise and takes away Noor along with his friends. The two plots converge here as Prakash escapes in that Black Innova along with Noor and his friends while the ACP is left injured severely. Neither Prakash nor his companions are aware of the money in the car.

To save Noor, Prakash arranges her stay in Sweetys house to which Sweety accepts. In the meantime, Sanyasi Naidu calls his henchmen to Hyderabad to kill the ACP and find the money. Another gang of Sanyasi Naidu is approached by Saradhi who tells them to find the  50 crores and distribute it among themselves and this stint makes the other gang turning into Saradhis henchmen. The ACP makes a deal with Encounter specialist Daniel (Raja Ravindra) to find the money and share it between them equally. Prakash comes to know that his phone is missing and is in the Innova and goes to the parking lot along with Tabar in the latters cab. There Prakash finds the money in the deck lid and shifts the bags into the deck lid of the cab. Tabar and Prakash go to the place where abandoned vehicles are kept and there Prakash and Tabar see the money. The keys are with Prakash and both go away from there. Meanwhile Daniel and his sub-ordinate go to the parking lot and find only a part of the money in the Innovas deck lid. While they are informing about it to the ACP, Sanyasi Naidus henchmen kill him and see the photo of Daniel in the caller image when he dials again.

Daniel arrests all the Old city youngsters along with Prakash and Tabar and Daniels sub-ordinate (Duvvasi Mohan) asks all of them about the money. Meanwhile the Local MLA intervenes and all of them are released. Just as Prakash and Tabar leave the station, Saradhis henchmen attack Prakash about the money and Prakash manages to beat them and escapes with Tabar. After an inquiry with the local Qazi, Daniel comes to know about Yadavs presence in the only marriage that happened on the day of missing of Innova. Yadavs men steal the cab from the new area and while Prakash and Tabar blame each other of stealing the money, Yadav makes a call to Prakash and threatens to sell the cab if he does not bring Noor today. But Yadav is unaware of the money in the cab. When Prakash, Tabar and Gokul see Yadavs men with the cab, they try to attack them but Daniel arrests Yadavs men and Sanyasi Naidus henchmen rob the car. While Prakash, Tabar and Gokul chase them on a bike, they hit the car of Daniel and since Daniel knows that Prakash is also involved in the robbery of Innova after beating Yadavs men at the spot, Prakash and Tabar are arrested. They are tortured and when Daniel tries to shoot Prakash, Tabar tells that the money is in his cab. Tabar is shot by Daniel and Sanyasi Naidus men kill Daniel and his sub-ordinate and kidnap Prakash. Gokul reaches the spot and takes Tabar along with him whom Prakash believes to be dead.

Sanyasi Naidu sees Prakash and threatens to kill Noor who is present in his house if he doesnt tell the place where the money is. Then Sanyasi Naidus henchmen inform him that the cab is at the place where they robbed it as there was no petrol in it to drive. Prakash goes there along with Sanyasi Naidus henchmen and is informed by Sweety that he is going to be murdered after they reach the spot. Prakash beats them up and refuels the cab. He goes to Yadavs place where Tabar and Gokul are being held. He calls Sanyasi Naidu to come there along with Noor if he wants him. Meanwhile Prakash leaves petrol cans open inside the cab and plans to explode the cab once the petrol leaks. The two parties meet each other and while everything is going smooth, Saradhis henchmen arrive along with Sweety and threaten Sanyasi Naidu to kill her if Prakash and the  50 crores are not handed to them. Prakash and Sanyasi Naidu are not at all happy as Yadav is now aware of the moneys presence and Prakash displays the money in the deck lid. He locks it and throws away the key. In the chaos that ensues, Sweetys life is endangered which worries Sanyasi Naidu.

Once Sweety and Noor are saved, the petrol starts leaking profusely and Prakash throws a burning stick on it which blasts the car and the entire money is burnt into ashes. Sanyasi Naidu is a reformed man. Prakash marries Noor and goes to Dubai along with his friends. There when his friends blame Prakash for burning the money, Prakash replies that before reaching Yadavs place, he took  50 lakhs from the lot and replaced it with white papers. Out of the  49.5 crores, he donates  45 crores to Saheli, an organisation established for the welfare of poor women in old city to ensure that there is no financial problem to the old city families among which Noors family is also one. The rest  4.5 cores are retained by Prakash with which he starts a very successful Sherwani business in Dubai with his wife and friends as employees.

==Cast== Nani as Prakash
* Catherine Tresa as Noor
* Siddhika Sharma as Sweety
* Bharath Reddy (actor)
* Tabar
* Charan Raj
* Raja Ravinder
* Duvvasi Mohan
* Raju Srivasthav
* Subramanyam Karra as Subbu Yadav
* Hari krishna

== Production ==

=== Development ===
The film was officially launched on 29 June 2011 at Hyderabad in the production office in the morning. Krishna Vamsi declared that a New Music Director would work for this film and Prasad Murella would handle the camera.  In the end of April 2012, while shooting was continuing, the films title was declared as Paisa.  In January 2013, a press note was issued stating that Nani would sport a Swarovski Crystal Suit which costed  0.45 million. It was said to be an inspiration of the outfits used by Michael Jackson. 

=== Casting === Nani was Chammak Challo and Iddarammayilatho released before this film.  Charan Raj was selected to play the main antagonist of the film.  In this film, he played the role of a corrupt politician.  Hyderabadi Cinema Comedian Akbar Bin Tabar, who is well known for his unique dialogue delivery, was selected to play the role of a Taxi Driver in this film. 

=== Marketing ===
The first look posters and teaser were released on 24 February 2013 on the occasion of Nanis Birthday.  The logo of the film was officially launched on the same day at Hyderabad on the eve of Nanis Birthday.  On 27 March 2013 two posters were released by the production team on the eve of Holi festival which carried the teams wishes to the people.  On 18 May 2013 the Audio release poster featuring Catherine Tresa was released along with few other posters on the Internet.  The films Trailer was released on 19 May 2013 along with the soundtrack Album. 

== Reception ==
The Hindu gave a review stating "A cocktail of crime, comedy and politics requires a solid plot, believable characters and racy storytelling. With Paisa, Krishna Vamsi tries to deliver a dizzying fun outing but tries to make do without a strong plot. Paisa has many things going for it by way of standing out from routine potboilers. There are witty lines, many well-conceived and well-executed scenes but on the whole, this isn’t Krishna Vamsi’s best."  Deccan Chronicle gave a review stating "Krishna Vamsi, unlike his earlier films, does not give attention to the emotional details making the storyline appear routine. His direction is lacking in many aspects making this one not one of his bests. The predictability of the plot is further marred by logical loopholes. The film might disappoint, but you can watch it for Nani" and rated the film 2.5/5.  Desimartini.com gave a review stating "Not as talked about as RGV’s but Krishna Vamsis descent into the restless madness is an equally interesting story. A super exciting auteur until a point for whatever reasons had turned into this insecure and loudly eager man that would try anything to distract the audience than letting the film have a shot longer than 12 frames. For the head to handle about 893 cuts in less than a minute and of something that wasn’t shot for such a pacey cut, the audience should be real brain dead to not feel irritated" and rated the film 2/5. 

Oneindia Entertainment gave a review stating "Krishna Vamsi, who wants to deal with Hawala and money laundering issues in a different way, has chosen wonderful story for Paisa, but he has failed to execute his ideas on screen. Overall, Paisa is a good commercial entertainer, but it is not up to the mark of Krishna Vamsis previous ventures. Nanis performance is the saving grace in the film. You can watch the film, If you are a hardcore fan of Nani or Krishna" and rated the film 3/5.  123telugu.com gave a review stating "Paisa is a movie that will not give you a paisa vasool experience at the Box Office. Despite some good efforts from Nani, this movie will face huge challenges at the Box Office. Poor technical values, lack of an engaging screenplay and low entertainment value will hurt the film" and rated the film 2.5/5.  IndiaGlitz gave a review stating "The script is full of lacunae. Nanis character is inappropriately happy-go-lucky. Moreover, his expression should have been intense in the second part of the film but that is not so. The screenplay was terribly written and executed, as the result, the audience feel that, barring Raja Ravindar, the others were less than perturbed by the mess. It is a Run-of-the-mill film from a director who has seemingly run out of ideas" and rated the film 2.25/5. 

greatandhra.com gave a review stating "The opening few minutes of the film doesn’t waste any time and gets the audience into the story. It is not until the interval caption that one realizes that first half is over. But trouble begins in second half. It could not sustain the same speed and had a dip which eventually played the spoilsport. Krishna Vamsi has tapped the element of human greed for money so while it appeals to the masses it doesn’t have enough for other genre of audience to like it" and rated the film 2/5.  gulte.com gave a review stating "Apart from Nanis performance there is nothing much to talk about Paisa. Krishna Vamsi failed execution let the film down. There are few impressive moments in the film, but those are not enough to hold the film together. Krishna Vamsis eccentric style of filmmaking will not impress many. Even his diehard fans will be disappointed to see him making a mess out of this interesting plot" and rated the film 2.75/5.  Apherald.com gave a review stating "These are hard times for Telugu Cinegoers as good movies are hard to find. The poor viewer is not demanding trailblazing or superb art at the movies. All they want is paisa vasool entertainers. Krishna Vamsis Paisa could have been a great entertainer, unfortunately it isnt!" and rated the film 2/5. 

== Soundtrack ==
{{Infobox album
| Name       = Paisa
| Type      = Soundtrack
| Artist    = Sai Karthik 
| Cover     = Paisa Audio.jpg
| Released   =  
| Recorded   = 2013 Feature film soundtrack
| Length     = 22:46 Telugu
| Label      = Aditya Music
| Producer   = Sai Karthik
| This album = Paisa (2013)
}}

The Soundtrack of the film was composed by Sai Karthik. The Album was launched through Aditya Music label on 19 May 2013 in a promotional event held at Shilpakala Vedika in Hyderabad. Nani and Allari Naresh anchored the event. The films crew and many well known celebrities attended the event. Veteran director Dasari Narayana Rao launched the Trailer in the same event. 

The Soundtrack Album received positive response. 123telugu.com gave a review stating "Paisa has some pretty decent music. The film’s theme song, ‘Paisa Theme’, is highly entertaining and music director Sai Karthik has done a very good job with this number. Among the other tracks, ‘Govindaa Govindaa’ ‘Neetho Edho’ and ‘Mayya Mayya’ are good. Sai Karthik has played a safe game, by coming up with pleasant but slightly routine music. Krishna Vamsi is known for coming up with very good visuals for his songs, so let us see what magic he can do this time."  IndiaGlitz gave a review stating "It is time for yet another Krishna Vamsi audio – something where each song is driven by the needs of the  script rather than any other extraneous consideration. Sirivennela Seetharama Sastry, the directors favourite, doesnt disappoint. When the theme is paisa, his talent comes to the fore like best. The singers, right from Tippu and Vijay Prakash to Shweta Mohan and others give their best. Sai Karthik gives one of his career bests."  Apherald.com gave a review stating "All together Paisa audio is impressive and songs will get more popular once film comes before audiences." 

{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    = 22:46
| lyrics_credits  = yes
| title1          = Eppudaithe Puttindo
| lyrics1         = Sirivennela Sitaramasastri
| extra1          = Krishna Vamsi, Vitthal, Venu, Dhanraj, Thagubothu Ramesh, Chandra
| length1         = 
| title2          = Govindaa Govindaa
| lyrics2         = Ananta Sriram
| extra2          = Tippu (singer)|Tippu, Baby Thillu
| length2         = 
| title3          = Mayya Mayya
| lyrics3         = Sirivennela Sitaramasastri
| extra3          = Vijay Prakash
| length3         = 
| title4          = Neetho Edo
| lyrics4         = Sirivennela Sitaramasastri
| extra4          = Shweta Mohan, Sai Karthik
| length4         = 
| title5          = Paisa Paisa
| lyrics5         = Sirivennela Sitaramasastri
| extra5          = Ranjith (singer)|Ranjith, Rahul Nambiar, Karthik Kumar
| length5         = 
| title6          = Paisa Theme
| lyrics6         = Instrumental
| extra6          = Sai Karthik
| length6         = 
}}

==External links==
*  

==References==
 

 

 
 
 
 
 
 