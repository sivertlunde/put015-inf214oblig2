Heaven Knows, Mr. Allison
 
{{Infobox film
| name           = Heaven Knows, Mr. Allison
| image          = Heavnknowsmrallison DVDcover.jpg
| caption        = Region 1 DVD cover
| director       = John Huston Charles Shaw
| based on       = Heaven Knows, Mr. Allison (novel)
| screenplay     = John Huston John Lee Mahin
| starring       = Deborah Kerr Robert Mitchum
| producer       = Buddy Adler Eugene Frenke
| music          = Georges Auric
| cinematography = Oswald Morris Russell Lloyd
| distributor    = Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 106 min
| country        = United States Japanese
| budget         = $2,905,000 
| gross          = $4.2 million (US) 
}}

Heaven Knows, Mr. Allison is a 1957 CinemaScope film which tells the story of two people stranded on a Japanese-occupied island in the Pacific Ocean during World War II.
 Charles Shaw Best Actress Best Writing, Screenplay Based on Material from Another Medium.

The movie was filmed on the islands of Trinidad and Tobago.  Producer Eugene Frenke later filmed a low-budget variation on the story, The Nun and the Sergeant (1962), starring Frenkes wife Anna Sten. 

==Plot==
In the  ), a novice nun who has not yet taken her final vows. She herself has been on the island for only four days; she came with an elderly priest to evacuate another clergyman, only to find the Japanese had arrived first. The frightened natives who had brought them to the island left the pair without warning, and the priest died soon after.

  with Deborah Kerr in a scene from the film]]

For a while, they have the island to themselves, but then a detachment of Japanese troops arrives to set up a meteorological camp, forcing them to hide in a cave. When Sister Angela is unable to stomach the fish Allison has caught, he sneaks into the Japanese camp for supplies, narrowly avoiding detection. That night, they see flashes from naval guns being fired in a sea battle over the horizon.

The Japanese unexpectedly leave the island, and in both celebration and frustration, Allison gets drunk on some sake left behind. He blurts out that he loves Sister Angela and that he considers her devotion to her vows to be pointless, since they are stuck on the island "like Adam and Eve." She runs out into a tropical rain and falls ill as a result; Allison, now sober and contrite, finds her shivering with chills. He carries her back, but sees that the Japanese have returned, forcing them to retreat to the cave again. Allison sneaks into the Japanese camp to get some blankets for her, but has to kill a soldier who discovers him in the act, which alerts the enemy to his presence. In an effort to force him out into the open, the Japanese set fire to the vegetation.
 pieces whose positions are well-concealed.

In what he attributes to a message from God, Allison comes up with the idea to disable the artillery during the barrage that will precede the American assault, while the Japanese are still in their bunkers. He is wounded, but manages to sabotage all the guns by removing their breechblocks, saving many American lives. After the landing, the Marine officers are puzzled by the lack of breechblocks in the guns.

Allison and Sister Angela say their goodbyes. He has reconciled himself to her dedication to Christ and she reassures him that they will always be close "companions". As Allison is carried down the hill on a stretcher, the film ends without him revealing what he had done to the guns, the implication being that virtue is its own reward.

==Production==
 

The movie was filmed in Trinidad and Tobago allowing Huston and Fox to use blocked funds in the UK, receive British film finance and qualify for the Eady Levy. The film was set later in the war than the novel that had Allison escaping from the Battle of Corregidor. In the film, the Allies are on the offensive and U.S. Marines capture the island.

The screenplay compares the rituals and commitment of the Roman Catholic Church and the United States Marine Corps. The National Legion of Decency monitored the production of the film closely, sending a representative to watch the filming; knowing this, Kerr and Mitchum deliberately ad-libbed a scene (not included in the final print), in which their characters wildly kissed and grabbed at each other. Server, Lee Baby, I Dont Care 2002 St Martins Griffin  
 Chinese from the laundries and restaurants of Trinidad and Tobago filled out the rest of the Japanese soldiers. 

Screen Archives Entertainment released Heaven Knows, Mr. Allison on Blu-ray on June 10, 2014.

==References==
 

==External links==
*  
*  
*  

; DVD reviews
*   by Glenn Erickson at   a part of  
*   by Barrie Maxwell at  
*   by Mark Zimmer at  
*   by Randy at  
*   by Steven D. Greydanus at  

; Australia (Region 4)
*   by Sarah Goodman at  

; France (Region 2)
*   at  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 