Leisurely Pedestrians, Open Topped Buses and Hansom Cabs with Trotting Horses
{{Infobox film
| name           = Leisurely Pedestrians, Open Topped Buses and Hansom Cabs with Trotting Horses
| image          = 
| caption        = 
| director       = William Friese-Greene
| producer       = 
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1889
| runtime        = 
| country        = United Kingdom
| language       = Silent film
| budget         = 
| gross          = 
}} 1889 British silent black-and-white short film, shot by inventor and film pioneer William Friese-Greene on celuloid film using his chronophotographic camera, which takes its name from a description of the content. The 20 feet of film, which was shot in January 1889 at Apsley Gate, Hyde Park, London, was claimed to be the first motion picture until pre-dating footage shot by Louis Le Prince was discovered. It was never publicly shown and is now considered a lost film with no known surviving prints or stills. 

==See also==
* The Magic Box
* Silent films

==Notes==
 

==External links==
*  

 
 
 
 
 
 


 
 
 