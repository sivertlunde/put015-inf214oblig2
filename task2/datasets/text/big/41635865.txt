The Hardys Ride High
{{Infobox film
| name           = The Hardys Ride High
| image_size     =
| image	         = File:Ann Rutherford Mickey Rooney Virginia Grey The Hardys Ride High 1939.jpg
| caption        = Scene from the film.
| director       = George B. Seitz
| producer       = Lou L. Ostrow Carey Wilson
| writer         = Aurania Rouverol
| based on       = 
| starring       = Mickey Rooney Lewis Stone David Snell
| cinematography = Lester White John F. Seitz
| editing        = Ben Lewis
| distributor    = Metro-Goldwyn-Mayer
| released       = 21 April 1939
| runtime        = 81 min.
| country        = United States English
| budget         = 
| gross          = 
}}

The Hardys Ride High is the fifth film of MGMs Andy Hardy series.

==Plot summary==
 
Judge Hardy is told he has inherited $2 million. He and his family move to Detroit.

==Cast==
 
* Lewis Stone as Judge James K. Hardy
* Mickey Rooney as Andy Hardy
* Cecilia Parker as Marian Hardy
* Fay Holden as Mrs. Emily Hardy
* Ann Rutherford as Polly Benedict
* Sara Haden as Mildred Aunt Milly Forrest
* Virginia Grey as Consuela MacNish
* Minor Watson as Mr. Terry B. Archer
* John Dusty King as Philip Phil Westcott
* John T. Murray as Don Davis, the Druggist
* Halliwell Hobbes as Dobbs, the Butler
* George Irving as Mr. Jonas Bronell
* Aileen Pringle as Miss Booth, Dress Saleslady
* Marsha Hunt as Susan Bowen
* Donald Briggs as Caleb Bowen
 

==References==
 

==External links==
*   at IMDB
*   at TCMDB
*   at Andy Hardy Films
*  
*  

 
 

 
 
 
 
 
 
 
 
 

 