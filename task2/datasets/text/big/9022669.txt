Aa Ab Laut Chalen
 
 
 
{{Infobox film 
| name     =       Aa Ab Laut Chalen
| image          = AaAbLautChalen.jpg
| caption        = DVD cover
| director       = Rishi Kapoor
| producer       = Rajiv Kapoor Randhir Kapoor Rishi Kapoor
| story         = Sachin Bhowmick Rumi Jaffrey Raju Saigal
| starring       = Rajesh Khanna Akshaye Khanna Aishwarya Rai Suman Ranganathan
| music          = Nadeem-Shravan
| cinematography = Sameer Arya
| editing        = Rajiv Kapoor
| distributor    = R.K. Films
| released       = 22 January 1999
| runtime        = 179 minutes
| country        = India
| language       = Hindi
| budget         = 
| preceded by    =
| followed by    =
}}
 romantic drama musical directed by Rishi Kapoor and written by Sachin Bhowmick. The film premiered in India and the United States on 22 January 1999. It starred Rajesh Khanna, Akshaye Khanna and Aishwarya Rai and was the last production of R. K. Films. Box Office India declared it a below-average performer at the box office.

==Plot==
Rohan Khanna (Akshaye Khanna) is a jobless graduate living with his grandfather (Alok Nath) and his mother (Moushumi Chatterjee). Rohan has been tirelessly looking for a decent job without success. One day, Rohans neighbour, Ranjit arrives. He lives in America, owns a motel, a perfect living and is married to an American woman. He wishes to keep his parents in America to live in peace. After listening to Rohans desire for a job, he suggests that Rohan should also move to America. Looking at Ranjits overnight success, Rohan is immediately persuaded. He tells his mother and grandfather of his wish and they are abruptly reminded of Rohans late father, Balraj Khanna (Rajesh Khanna) who left his wife and son for America after a bitter argument with his father. In a bus accident, Balraj had died leaving Rohan orphaned and his mother widowed. To avoid the same fate for Rohan as his fathers, Rohans grandfather prepares Rohans visa for America.

Rohan arrives in America and immediately meets Sardar Khan (Kader Khan), a Pakistani man who drives a taxi. Rohans good manners and friendly personality wins Sardar over as he drives Rohan to Ranjits motel. Rohan did not have any spare cash to pay Sardar so he asked Sardar to wait while he asked Ranjit for the taxi fare. He is shocked when Ranjit does not pay his fare or give him a place to stay saying that Rohan should help himself. Rohan finds out that Ranjits parents are working at the motel and facing verbal abuse from his wife. Sardar offers Rohan a place to stay much to the dismay of his roommate, Iqbal Singh (Jaspal Bhatti), a Punjabi man from India. Rohan is allowed to stay with Iqbal and Sardar with the persuasion of Chaurasia (Satish Kaushik. Chaurasia runs an Indian restaurant and betel leaf shop. Rohan is given a job as a taxi driver. One day, while making routes near the airport, he picks up a young Indian woman named Pooja (Aishwarya Rai). Pooja has just arrived in America to live with her older brother and his wife. To her shock, her brother only brought her because his boss had seen a picture her and had fallen for her. Poojas brother insists that she should marry his boss. In anger, she leaves his house without money and nowhere to go. Rohan sees Pooja crying and offers her a place to stay with Iqbal and Sardar. 
 Green Card NRI girl. Rohan and Pooja go to an Indian dance to search for an NRI girl disapproved by Pooja. There, he met the seductive Loveleen (Suman Ranganathan) and he falls for her immediately. Pooja, Iqbal, and Sardar do everything to keep Loveleen away from Rohan until he gets aggravated and tells the three to stay away from him. Iqbal advises Pooja to follow Rohan and Loveleen to Seaside Heights and declare her love for him. She is rejected by Rohan and he later tells her to stay away from him. Iqbal and Sardar accuse him of being consumed by money. Rohan tells them that they are envious of his progress. Iqbal tells Rohan that one day he will regret his decision and come back to Pooja. Rohan leaves the house for Loveleen in rage.

To forget Rohan, Pooja finds a job as a caretaker for a rich Indian man who is ill. He is Balraj Khanna, Rohans estranged father who has not died. Balraj has been living in New York with his son, Karan. Karans mother left Balraj when Karan was very young. Karan has grown to be a spoiled and arrogant young man who is friends with Loveleen. He consistently asks his father for money to spend for his enjoyment. He even took a loan from Marko, a dangerous drug dealer. To pay back his debts, he is assigned to transport drugs around the country. Balraj takes a liking to Pooja and considers her as a daughter. He wants to keep her in his home so he asks her to consider marrying Karan. Karan and Pooja are against the idea. Meanwhile, Rohan begins to realise that Loveleens American culture deeply goes against his. He gets fed up with her ways and realises he is really in love with Pooja. He leaves Loveleen with no where to go because of his poor decisions. Jack Patel forces Iqbal, Sardar, and Rohan to reconcile and they forgive Rohan for his mistakes. 

Rohan sets out to ask Pooja for forgiveness. She is in Springfield with Balraj, and Rohan arrives there to meet her. After a confrontation between Rohan and Pooja, Rohan tells her that they will go back home to India together: "Aa Ab Laut Chalen" (Come, Lets Go Back). Pooja forgives Rohan and Rohan gives her a locket with a picture of his mother inside saying that it is a promise that he will marry her. Pooja tells Balraj that she has fallen in love with a man named Rohan and he encourages her love for him. Meanwhile, Karan becomes desperate for the money so he decides to marry Pooja. After violently urging her to marry him, her locket falls on the ground and Balraj picks it up. When handing it to her, he sees Rohans mother and his wife. He realises that Rohan is his son but did not tell Pooja. After hearing that he is unemployed, he tells Pooja that he will give Rohan a job in his company. But he fears that his son will not accept him as a father because he left him and his mother back in India. 

Rohan accepts the job and is deeply grateful to Balraj. He wishes for a chance to meet him. Balraj continues his generosity from afar by giving Rohan a $5000 bonus. Rohan returns to Ranjits motel and frees Ranjits parents by paying for their tickets to India. Ranjit is arrested by Jack Patel for keeping his parents passports. His mother is happy to know that Rohan has found the love of his life and a decent job. One day, Rohan is invited to a party which Balraj would attend. When Rohan meets Balraj, he is stunned to realise that Balraj is his father and very much alive. He reacts in anger. Balraj openly confesses to everyone that he has left his wife and son in India and does not deserve everyones respect. Balraj reveals that after he arrived in America, he went back to India to find his family but he could not. Hence, he remarried. Rohan cannot bring himself to forgive his father because he has caused his mothers suffering. Balraj insists to speak to his wife to ask for forgiveness. She immediately forgives him and asks them to come back to India. Karan emerges saying that if Balraj leaves for India, he has to leave his fortunes to Karan in America After a tough debate and on Rohans consent Balraj agrees but on the condition that he will disown him as a son. 

Balraj, Rohan, and Pooja all leave for India. Suddenly, PC Jack Patel arrives with Karan. Turns out that Karan has turned in an important information about Marko to the officials. Karan tells Balraj that he does not want the wealth but a family he never had. They accept Karan and the family leaves for India. We later see Rohans mother welcoming them home and they finally live happily together.

==Cast==
*Rajesh Khanna as Balraj Khanna
*Akshaye Khanna as Rohan Khanna
*Aishwarya Rai as Pooja Walia
*Moushumi Chatterjee as Rohans mother
*Navin Nischol as Dr. Ashwin Khurana
*Suman Ranganathan as Loveleen
*Kader Khan as Sardar Khan
*Paresh Rawal as PC Jack Patel
*Satish Kaushik as Chaurasia
*Jaspal Bhatti as Iqbal Singh
*Vivek Vaswani as Vaswani
*Himani Shivpuri as Chaurasiaa wife
*Alok Nath as Rohans grandfather
*Aditya Hitkari as Karan Khanna
*Sulabha Arya as Ranjits mother
*Achyut Potdar as Ranjits father
*Vishwajeet Pradhan as Drug Dealer

==Music==

{| class="wikitable"
|-  style="background:#ccc; text-align:centr;"
! Title !! Singer(s) 
|-
| "Aa Ab Laut Chalen" 
| Udit Narayan, Alka Yagnik
|-
| "O Yaaro Maaf Karna"
| Kumar Sanu, Abhijeet Bhattacharya|Abhijeet, Shabbir Kumar, Alka Yagnik, Sonu Nigam, Saud Khan & Vijeta
|-
| "Yeh Kaisi Mulaqat Hai"
| Kumar Sanu, Alka Yagnik
|-
| "Mera Dil Tera Deewana"
| Alka Yagnik
|-
| "Yehi Hai Pyar"
| Udit Narayan, Alka Yagnik, Jaspinder Narula
|-
| "Tere Bin Ek Pal"
| Udit Narayan, Jaspinder Narula
|-
| "Pyar Hua Pyar Hua"
| Jaspinder Narula
|-
| "O Yaaro Marf Karna (part II)"
| Kumar Sanu, Alka Yagnik
|}

==Reception==
Mohammad Ali Ikram of PlanetBollywood gave the film 7.0/10 and wrote "Aa Ab Laut Chalen may be filmi in conveying the theme, but the point is presented with applaudable conviction in the second half.  Rediff noted "Maybe Rishi Kapoor was bowing to RK tradition, maybe he was trying to follow his heart, but he just has to get back to the drawing board and seriously plan that next venture".  

The film performed reasonably well at the box office.  It is R.K. Films last movie to date.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 