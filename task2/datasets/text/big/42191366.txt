Cheating the Piper
{{Infobox Hollywood cartoon|
| cartoon_name = Cheating the Piper
| series = Jerry on the Job
| image = 
| caption = 
| director = Vernon Stallings
| story_artist = Walter Hoban
| animator = Walter Lantz
| voice_actor = 
| musician = 
| producer = 
| studio = Bray Productions, Inc.
| distributor = Goldwyn-Bray Pictographs
| release_date = June 12, 1920
| color_process = Black and white
| runtime = 4:07 English
| preceded_by = The Mysterious Vamp
| followed_by = A Quick Change
}}

Cheating the Piper is a silent short animated film by Bray Productions, and is one of the cartoons based on the comic strip Jerry on the Job. The film itself is loosely based on the legend of the Pied Piper of Hamelin.

==Plot==
At the train station, Mr. Givney is doing some paper work when a rat pops out of a hole. After the rat eats his sock and belt, Mr. Givney pulls out a revolver and guns down the rodent. He then comes outside and orders Jerry to deal with the stations rat problem.

In no time Jerry comes up with using his saxophone. When Jerry plays on his instrument, the rats are mesmerized by the melody as they come out from various openings in the station. Jerry, while still playing, walks further from the station, and the rats follow him. Mr. Givney also comes along to see Jerrys handling.

Jerry lures the rats into a lake, hoping they would drown. Thinking the plan worked, Mr. Givney picks up Jerry like a beloved son, gives complimentary kisses, and carries him back to the station.

Back at the lake, a train stops to refill its boiler. The train engineer inserts a hose into the lake to take up water. It appears the rats are not completely drown as they pass through the hose.

Minutes later, the train arrives at the station. Jerry is excited, and Mr. Givney shows up. To their surprise, what comes out are the rats Jerry removed previously. Jerry and Mr. Givney then cling onto the edges of the door, and watch the rats run back into the openings inside.

==External links==
*  at the Big Cartoon Database
*  

 

 
 
 
 
 
 
 
 
 
 
 

 