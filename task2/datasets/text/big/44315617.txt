Riders of the Dark
{{Infobox film
| name           = Riders of the Dark
| image          = 
| alt            = 
| caption        = 
| director       = Nick Grinde
| producer       = 
| screenplay     = W. S. Van Dyke Madeleine Ruthven 
| story          = W. S. Van Dyke
| starring       = Tim McCoy Dorothy Dwan Rex Lease Roy DArcy Frank Currier
| music          = 
| cinematography = George Gordon Nogle 	
| editing        = Dan Sharits 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western silent film directed by Nick Grinde and written by W. S. Van Dyke and Madeleine Ruthven. The film stars Tim McCoy, Dorothy Dwan, Rex Lease, Roy DArcy and Frank Currier. The film was released on April 21, 1928, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Tim McCoy as Lt. Crane
*Dorothy Dwan as Molly Graham
*Rex Lease as Jim Graham
*Roy DArcy as Eagan
*Frank Currier as Old Man Redding
*Bert Roach as Sheriff Snodgrass
*Dick Sutherland as Rogers

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 