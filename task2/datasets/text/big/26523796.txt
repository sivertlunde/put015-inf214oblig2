Piano Girl
{{Infobox film
| name           = Piano Girl
| image          = PianoGirlFilmPoster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Murat Saraçoğlu
| producer       = Tolga Aydın
| writer         = Sevim Hazer Ünsal
| starring       = Tarık Akan   Şerif Sezer   Zuhal Topal
| music          = Özgün Akgül   Mehmet Erdem
| cinematography = Mustafa Kuşcu
| editing        = Mustafa Preşeva
| studio         = Aydin Film
| distributor    = Pinema
| released       =  
| runtime        = 95 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $614,899
}}
Piano Girl ( ) is a 2009 Turkish comedy-drama film, directed by Murat Saraçoğlu, starring Tarık Akan and Şerif Sezer as two elderly people forced to question their histories and reveal their big secrets. The film, which went on nationwide general release across Turkey on   , was the opening film at the Sinema Burada Film Festival in İzmir, Turkey,     and has since been screened in competition at a number of other film festivals, including the 46th Antalya Golden Orange Film Festival, where, according to Terry Richardson, writing for Todays Zaman, the rapt audience gave it a standing ovation.   

== Plot == Russian nation, are forced to migrate to Kars in eastern Anatolia. Among the migrating families is Mişkas (,) family. Mişka grows old in Kars and now operates the only mill in the village. However, he has to struggle with financial difficulties after modern machines start replacing traditional methods.

In the meantime, the cranky old woman of the village, Popuç (Şerif Sezer), hates Mişka and does not want him in the village. Popuç lives with her son Semistan (Levent Tülek), daughter-in-law Figan (Zuhal Topal) and three grandchildren. However, the smallest and most wayward of her granddaughters, Alma, befriends the old Mişka. Alma will help two elderly people question their histories and reveal their big secrets.

== Cast ==
* Tarık Akan as Mişka
* Şerif Sezer as Popuç
* Zuhal Topal as Figan
* Levent Tülek as Şemsitan
* Korel Cezayirli as Metin Öğretmen
* Murat Aydın as Fezo
* Ozan Erdoğan as Tavşan
* Muhammet Cangören as Allahyar
* Levent Uzunbilek as Mirza
* İsrafil Parlak as Adıbeş
* Havin Funda Saç as Esme

== Release ==
=== General release ===
The film opened in 133 screens across Turkey on   at number seven in the Turkish box office chart with an opening weekend gross of $99,761.   

=== Festival screenings ===
* 2009
**Sinema Burada Film Festival 
**46th Antalya Golden Orange Film Festival   
**4th Bursa International Silk Road Film Festival      
* 2010
**21st Ankara International Film Festival   

==Reception==
===Box Office===
The film reached number five in the Turkish box office chart and has made a total gross of $614,899. 

===Reviews===
Terry Richardson, writing for Todays Zaman, describes the comic drama as, Entertaining if melodramatic. 

== See also ==
* 2009 in film
* Turkish films of 2009

==References==
 
==External links==
*  
*  
*  

 
 
 
 