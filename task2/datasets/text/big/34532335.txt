A Woman in Berlin (film)
{{Infobox film
| name           = A Woman in Berlin
| image          = 
| alt            =  
| caption        = 
| director       = Max Färberböck
| producer       = Gunter Rohrbach
| writer         = Max Färberböck
| starring       = Nina Hoss Eugeny Sidikhin
| music          = Zbigniew Preisner
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 126 minutes
| country        = Germany, Poland
| language       = German, Russian
| budget         = 
| gross          = 
}} Eine Frau in Berlin, published anonymously in 1959 in German, with a new edition in 2003. (It was also published in English in 1954 and 2005, and in seven other languages.)

The film premiered at the 2009 Berlin Film Festival and was praised for its portrayal of a morally complex and brutal period.

== Plot ==
In the waning days of World War II, an assortment of women, children and elderly men struggle to survive in Berlin, cast out of their formerly middle-class lives. 

The Red Army arrives, defeating the last German defense. Its soldiers rape women of any age as they occupy the city. After being raped by a number of Soviet soldiers, the films anonymous woman, a German journalist (played by actress Nina Hoss), petitions the battalions commanding officer for an alliance and protection. After initially rejecting her, the married officer Andrei Rybkin (Eugeny Sidikhin) is gradually seduced by the beautiful but battered German woman. She has a cool, practical approach to her life and has been part of an informal community that developed among survivors in her apartment building.

The officer subsequently protects, feeds and parties with her and her neighbors. Other women in the flats also take particular officers or soldiers for protection against being raped by soldiers at large. Rybkin comes under suspicion and he is reassigned.

== Cast ==
* Nina Hoss - Anonyma
* Eugeny Sidikhin - Andrej Rybkin
* Irm Hermann - Witwe
* Rüdiger Vogler - Eckhart
* Ulrike Krumbiegel - Ilse Hoch
* Rolf Kanies - Friedrich Hoch
* Jördis Triebel - Bärbel Malthaus
* Roman Gribkov - Anatol
* Juliane Köhler - Elke
* Samvel Muzhikyan - Andropov
* August Diehl - Gerd
* Aleksandra Kulikova - Masha
* Viktor Zhalsanov - asiatischer Rotarmist
* Oleg Chernov - Erster Vergewaltiger
* Eva Löbau - Frau Wendt
* Anne Kanis - Flüchtlingsmädchen
* Sebastian Urzendowsky - Junger Soldat

==Reception==
It received strong reviews for its brutal truthfulness. The Washington Post described it as "A clear-eyed portrait of a highly charged chapter in Germanys history, a history that once again proves rewarding fodder for an alert artistic imagination."   The reviewer wrote that after the film portrays the initial rapes and assaults against German women by Soviet soldiers, it takes a "much more somber and morally complex turn."  The protagonist and her mostly women neighbors must "navigate a city thats become a physical and psychic no-mans land."  , Washington Post, 6 November 2009, accessed 7 September 2014 

Roger Ebert noted that "Yes, she profits from their liaison, and yes, he eventually takes up her offer. But for each there is the illusion that this is something they choose to do....The woman and man   make the best accommodation they can with the reality that confronts them."  , Roger Ebert website, 23 September 2009, accessed 8 September 2014 

The Austin Chronicle praised Hoss "in a supremely complex and modulated performance." It described the film as "that rarest of wartime dramas: an intimate, sorrowful glimpse into the heart and loins of the hellish aftermath of war." 

==See also==
* List of German films

==References==
 

==External links==
*    
*   at Strand Releasing
*  
*  
*   at Metacritic
*  
*  

 
 
 
 
 
 
 
 


 