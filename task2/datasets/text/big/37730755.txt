Crayon Shin-chan: Very Tasty! B-class Gourmet Survival!!
{{Infobox film
| name           = Crayon Shin-chan: Very Tasty! B-class Gourmet Survival!!
| image          = Poster_for_the_21st_movie_of_crayon_shinchan_in_2013.jpg
| alt            =Poster
| caption        = 
| director       = Masakazu Hashimoto
| producer       = 
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Shin-Ei Animation
| distributor    = Toho
| released       =  
| runtime        = 96 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = US$12.7 million (Japan)
}}

  is a 2013 Japanese anime film. It is the 21st film based on the popular comedy manga and anime series Crayon Shin-chan. It is directed by Masakazu Hashimoto.  The film was released to theaters on April 20, 2013 in Japan. The film is produced by Shin-Ei Animation, the studio behind the TV anime.   This movie was also released in India on Hungama TV on 19 July 2014 as Shin Chan in Very Very Tasty Tasty.

Masakazu Hashimoto is directing his first Crayon Shin-chan movie after storyboarding 2008s Crayon Shin-chan: Chō Arashi o Yobu Kinpoko no Yūsha and 2011s Crayon Shin-chan: Arashi o Yobu Ōgon no Spy Daisakusen. Yoshio Urasawa and Kimiko Ueno wrote the screenplay.

==Overview==
Directed by Masakazu Hashimoto, this movie is the work as a series that was used in the theme of "food" for the first time. In addition, it will be the first time in four years, after the 17th movie  , that the phrase "The Storm Called! (arashi o yobu!)" is not used in the movie title. 
On 15 January 2013, it was decided that Korokke (actor), comedian Naomi Watanabe, and chef Tatsuya Kawagoe will appear as special guests in their role themselves. 

It was released in 324 screens across the country on 21 April 2013, and by 22 April the number of viewers increased up to 199,993 people. Its total box office revenue was over 200,000,000 yen in the first two days of release, and was ranked second by a narrow margin. In four weeks of release, the revenue increased to 1 billion yen. The movie earned a total revenue of 1.3 billion yen.
 

==Plot==
 
In the story, Shin-chan and the Kasukabe defence group members were supposed to go to the B-class Gourmet Carnival. But, just then, a mysterious person asked Shin-chan to deliver a sauce to the carnival. But, that sauce was the only legendary sauce which could save B-class gourmet, which would otherwise get eradicated if fallen into the evil hands, and only A-class gourmet would remain. Will the mischievous kindergartener Shin-chan and his friends really be able to deliver the sauce safely to the carnival and save the worlds B-grade cuisine? 
(NOTE: In relation to gourmet, the Japanese word "bakauma バカうまっ" is a slang term for "very tasty". It does not mean "stupid horse" in this context.)

The theme of the movie is slightly similar to the 2003 movie:  .

==Story==
One morning, it was said in the TV news that a B-class gourmet carnival is being held in Kasukabe city.
Shinnosuke does want to go there and eat yakisoba (roasted soba, one of the Japanese noodles, with meat and vegetables), which Action Kamen ate cooked by master of sauce Ken (sauces ken) in the TV news. But Hiroshi had to go to work and Misae had to take Himawari to the hospital to get her the vaccination. Shinnosuke was disappointed.

In the Kindergarten, Nene-chan was excitedly talking about her visit to the carnival with her family last weekend.
On hearing this, Shinnosuke became more eager to go there. So, he suggested that only they (Shinchan, Nene, Masao, Bo, Kazama) will go alone secretly, without their parents.

But then, the B-class gourmet carnival was being taken over by the mysterious organization called A-class gourmet organization.
Its chief executive, Gourmet-poy (グルメッポーイ) declared that
"A-class food is the only food , B-class food ends from today."

About 20 years ago, Gourmet-poy was taught table manners by his father Papa-poy. His mother Mama-poy always kept an eye on her son.
He pointed his knife and fork upwards while eating, and not parallel. So Papa-poy was angry about Gourmet-poys bad manner.
The next day, his family went to downtown. His parents were chatting with someone. Gourmet-poy found Kens yakisoba stand, and went close to it.
Ken noticed that Gourmet-poy is watching him cooking yakisoba. Ken said, "Will you try to taste yakisoba? Just now I cooked."
Gourmet-poy was hesitant about what to do, because it didnt look like A-class food but was so delicious. Meanwhile, his parents came. His father said, "Its such a dirty food. I dont think I will eat one. Please dont disappoint me anymore!"
Gourmet-poy doesnt eat yakisoba and ran away.

The carnival had shops selling different types of food. Bunta sold okonomiyaki, Minami sold takoyaki, Okyou sold motsu-curry, Masa sold kushikatsu, etc.
But just then, the Masked Sushi Couple and steak Rider defeated the B-class gourmet members, Downtowns Korokke-don, Chicken Motsu-currys Okyou and others. They demolished their shops too.

In this troublesome situation, Ken talked with the members of the B-class party, that they we could make it if they could get the legendary sauce. He called the lady Shougas Benkiko (shouganobeniko). In Japanese, 生姜 (shouga) means ginger, 紅しょうが (benishouga) is red ginger and "しょうがの紅子" (shouganobeniko) is the anagram of Beni shoga|benishouga. (Japanese frequently cook dish with shouga as the spice. like udon, gyudon, takoyaki, etc.)
He trusted her truly, and asked her to bring the sauce.

Meanwhile, the Kasukabe Defense Group, Toru Kazama, Masao, Bo-chan and Nene-chan had gathered at the park. Shinnosuke came wearing a samba costume with Shiro also wearing that. When they departed for the carnival, Shinnnosuke took off that.
They said together "For eating Yakisoba, lets depart!! (焼きそば食べに、出発おしんこー!!)".

During their journey, they meet Beniko accidentally. She had taken the sauce pot from the bank and was caught by A-class organization. So she had run away from them. She gave the pot to Kasukabe Defense Group. She requested them to safely deliver the legendary sauce pot to Ken, who was in the carnival.

Ken and his party defended their yakisoba house desperately. The other B-class gourmet houses were destroyed and changed to A-class houses by the A-class organization.

The Kasukabe Defense Group had taken the wrong bus. They thought the bus was going to the carnival, but they were taken deep in the village. They were lost.

Meanwhile Beniko was captured.

The kids were bored because the next bus for Kasukabe would come at 8 p.m. Just then a car with a strange lady driving came. She said, "If you are going to the carnival , come with me!." Excited, the kids get on the car. She was Caviar, a 4 starred elite member of the A-class organization. She let them eat some caviar. But they said it tasted bad and only ate the cookie below the caviar. But Shinnosuke sprinkled some mayonnaise, which he took out from his pocket, on the caviar. When Caviar saw it, she became very angry (because she hated mayonnaise) and started to speak Russian. As she lost her control, the car in an instant crashed head-on with a truck coming from the opposite lane. Shinnosuke moved the handle, and the car dropped downward to the valley. The car was crashed and Kasukabe Defense Group was able to escape from Caviar.
They went deep into mountain.

Shinnosuke said confidently, " I understand! We got completely lost in the mountain!"

Kasukabe Defense Group examined what they had.
Kazama had a map, but it was a world map. In that situation it had no worth.
Nene had a water bottle with rabbit head, and a stuffed rabbit in her bag.
Bo had only a water bottle.
Shinnosuke had an arm of Kantam Robot, the butt of The Bare-ass Godzilla, and Chocobi. They planned to divide it when they were truly
hungry.
Masao had a towel and a biscuit, but he concealed the biscuit.

They were starting to the direction of the Carnival they guess. They carried the legendary sauce pot alternately. When it was Masaos turn they reached an old suspension bridge, which appeared as if it was about to break.
In the halfway of the bridge, the pot was so heavy that Masao had very nearly dropped it into the river. He cried. Then a kind of earthquake happened, birds were flying from trees. "What happened?", Toru exclaimed. An extremely big sumo wrestler came. He is Foie gras Brocade, a member of the Four Star A class gourmet Lovers. He was crossing the bridge. The bridge was badly shaking. Kasukabe Group ran away to the opposite side. Foie gras broke the bridge, and fell into the bottom of the river. Shinnosuke, Masao, Bo and Nene were able to reach the land safely, but Kazama falls just before reaching the land. Immediately, Shinnosuke took hold Kazamas hand and held Kazamas thumb by his own thumb counting up from one to ten. "Yes! I win!", Shinnosuke exclaimed. (This was the Thumb war or Thumb Wrestling
game, a popular kids game.)
Then they found a hot spring where monkeys were bathing (sometimes monkeys in Japan bathe in the hot spring). Shinnosuke bathed among the monkeys.
They were hungry, so Masao appeared like a real onigiri (rice ball) to them. Then they smelled a really good aroma. They searched for the origin of the aroma, and found that there was an omelet with truffle and a cluster of parsley on a table. They try to have it all by themselves. They fought with each other and tried to snatch it. When Toru ran towards the omelet, Masao interfered him. Nene took it by surprise, but Shinnosuke dived out from the hot spring like ninja to attack her. The omelet fly away in the air, and Bo opened
his mouth to catch it. But the omelet dropped into the hot spring.
They were very disappointed.

Then a pig speaking human language came. They were surprised. But actually a man was talking. He was Truffle,
a member of the Four Star Lovers. He has some pigs whom he calls "piggy". He ordered, "go my sweet piggy!", and the pigs stood on two legs. Shinnosuke showed his little "elephant" and
passed gas. Truffle said, "how shameful!!" and dived into the hot spring. Kasukabe Defense Group cold escape again.

Meanwhile, Misae and Kazamas mother looked for the kids. They both learned that kids had gone to the carnival, from Kazamas letter left behind. So they went to the carnival.

The kids had an awkward atmosphere, because they were tired and hungry. They blamed each other for their problem. Just then, Kazama emotionally said, "Kasukabe Defense Group is dismissed now!". Others were shocked. Suddenly, a table came from the air which had a meal specially for kids. Excited, they approach to the meal. But the sheet below the table covered them over, and they were lifted to the airship driven by Gourmet-poy.
But Shiro was left behind. When the sheet unrolled, they saw many people eating their meal sitting on chair, who masked and were in black suits with two or three stars. They were the underlings of A-class gourmet organization.
Gourmet-poy came closer to Kasukabe Defense Group, saying "Hi there, you have something you have to give me, right?". Shinnosuke sighs, "It cant be helped. Here you are.", and give his choco-bi. Gourmet-poy got angry and exclaimed, "whats this!?". The kids had a meeting, "Who has the pot?". No one had it, because Shinnosuke had given it to Shiro. They turned towards Gourmet-poy and said "It is with the beautiful lady over there wearing purple lingerie." Gourmet-poy and his underlings searched for the lady who didnt exist at all. The kids run away from there and Gourmet-poy ordered to capture them. They reached at the bottom of the airship and found some escape pods. They got in one pod. They randomly started pushing buttons, and the pod started to take off to the sky, and the roter came out from the bottom of pod. They had to wear the parachute bag to try to escape from the pod. The canopy was blown off, along with the seat and the kids (Masao was sitting on the blown-off seat). Masao had worn the parachute bag, and Shinnosuke (who was held by others) managed to reach Masao with his gas shoot and held him. They could return safely to the ground. They could also meet Shiro again.

Meanwhile, Misae met Hiroshi in front of the carnival entrance.
Kazamas mother had already entered.

At night, the kids decided to stay in the forest and make a simple house made of wood. They reconciled each other and eat the biscuit which Masao had. They slept looking up the stars.

But then, Misae, Hiroshi, Kazamas mother and others were captured as soon as they arrived at the carnival.

Next morning the kids stood upon the hill, being irritated. They put their hands in their pockets and exclaimed "lets go!".

When they arrived at a crossroad in the rice field,  Truffle with piggies, Foie gras, and Caviar having a fishing pole in her hand, came from all three sides. They ordered, "hey kids, give us the sauce pot!". They were unable to make out who had the pot., because every kid had a bag filled with something. They fought bravely with the A-class members. Nene used her bunny punch, while Masao fought fearlessly with the sumo wrestler.
Finally Kasukabe defense group defeated them,
and they continued going to the carnival.

When they arrived at the carnival, Beniko was leading the B-class group in front, and fighting against the A-class group. Hiroshi, Misae and others were also fighting with her. Hiroshi used his stinky socks technique to defeat the enemies.

The kids successfully passed through the battle field. At last they met Ken.
But suddenly Gourmet-poy came and　hit Ken.
He grabbed the pot and said "I will myself throw away the legendary sauce!" He overturned the pot. However, the sauce didnt spill out. He was confused and mumbled, "wh,wh,whats going on? Where is the sauce? where are the kids?"
The kids exclaimed "the sauce is here!", and they climbed secretly up the tower of Kens yakisoba house and opened the water bottle. They poured the sauce into each water bottle.
Shinnosuke said, "Im very hungry. I will try to cook yakisoba with this sauce".  Masao was surprised and asked, "But can we cook it?". Kazama said, "we have to only fry the noodle , meat and vegetables, and mix them. Then add the sauce.". They started cooking. Gourmet-poy shouted, "stoooooop!" and climbed the tower of Kens yakisoba house.
The kids finished cooking yakisoba as soon as Gourmet-poy arrived.
They were about to eat it, but Gourmet-poy seemed he wanted to eat it. (It reminded him of his childhood incident.)
So Shinnosuke let him eat the yakisoba.
He was delighted and exclaimed, "so delicious!"

At last, all the people, including the A-class group and the B-class group, together ate yakisoba with the legendary sauce.
In the movie ending credits, it was seen that Ken went forth pulling his yakisoba stand, with Gourmet-poy pushing it. This suggested an happy ending.
   

==Cast==
*Akiko Yajima - Shin-chan
*Keiji Fujiwara - Hiroshi Nohara
*Miki Narahashi - Misae Nohara
*Satomi Kōrogi - Himawari Nohara
*Mie Suzuki - Masao Sato
*Mari Mashiba - Toru Kazama / Shiro
*Tamao Hayashi - Nene Sakurada
*Chie Sato - Bo-chan

==Guest Cast==

===B class gourmet carnival===
; Sauces Ken
: 
: Fired-noodles worker. Has a clumsiness for women. Names origin and model is Ken Takakura.
; Beniko Shougano Beni shouga, which is a Japanese pickled ginger used mainly in yakisoba and okonomiyaki. Voiced by Japanese comedian Naomi Watanabe.
; Chef Kawagoe
: A real Japanese chef and cook. Voiced by Tatsuya Kawagoe himself. 

===B class gourmet carnivals allies===
; Shitamachi Korokke-Don
: He looks like a korokke. He is a mascot and a friendly existence. Voiced by Japanese actor KOROKKE.
; Okonomiyakis Bunta
: 
: Appearance like Japanese okonomiyaki. Although he is usually friendly; but when angry, he puts out "Hiroshima Style Okonomiyaki".
; Takoyakis Minami
: 
: Appears like Japanese takoyaki. She is the Idol of Takoyaki world.
; Hataka Motsu-Currys Okyou
: 
: Speciality is Motsu-curry. Although she is fast and strong while fighting, but she also has a shy side. 
; Osaka Kushikatsus masa
: 
: Appears like Osakas kushikatsu. Although he says rough words, but he is friendly towards women. 

===A Class Gourmet Organization===
; Gourmet-Poy
: 
: The Chief executive of A class gourmet organization. He is plotting the destruction of B class gourmet and wants only A class gourmet to thrive.
; Caviar
: 
: 5 star A class gourmet lovers only Russian person. She likes caviar, and hates those people who like mayonnaise.
; Truffle
: 
: A person of the 5 star A class gourmet lovers. Likes truffle and elegance, and deeply hates vulgarity. She owns a pig named "piggy" and frreely manipulates it.
; Piggy
: 
: It is Truffles pet pig. With its sensitive nose like police dogs, it is able to track the sauce.
; Nishiki Foie-gras Yokozuna
: 
: A person of the 5 star A class gourmet lovers. He likes the dish foie gras. He is characterised by a wrestlers figure, so he hates himself.
; Masked Sushi Couple
: 
: 4 star member. They dance brilliantly in combination while holding the sushi, and they feed the attack. They both appear to be having a training experience in Ginza.
; Steak-Rider
: 
: 4 star member. Hes a lone rider. With his iron plated motorbike, he attacks with a grilled steak. 

==Slogan==
The Fury! The Fried-noodles! The Focus! The Friendship!

==Theme Song==

===Opening Theme Song===
Title: Kimi ni 100 Percent (Warner Music Japan)  
Singer: Kyary Pamyu Pamyu

===Insert Song===
Title: Song of Yakisoba ~sauce is love~ (やきそばの歌 〜ソース is love〜)  
Singer: Toshiyuki Arakawa

===Ending Theme Song===
Title:   (Toys Factory Japan)   
Singer/Band: SEKAI NO OWARI
 

==DVD Release==
The release date of DVD and Blu-ray of the movie in Japan is 8 November 2013. 

==Reception==
The film grossed US$12.7 million in Japan. 

==See also==
* Crayon Shin-chan
* Yoshito Usui

==References==
 

 
 

 
 
 
 