Life During Wartime (film)
 
{{Infobox film
| name           = Life During Wartime
| image          = Life-during-wartime-poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Todd Solondz
| producer       = Derrick Tseng Christine K. Walker Elizabeth Redleaf
| writer         = Todd Solondz
| starring       = Ally Sheedy Allison Janney Ciarán Hinds Chris Marquette Paul Reubens Dylan Riley Snyder Ed Lachman
| editing        = Kevin Messman
| studio         = Werc Werk Works
| distributor    = IFC Films
| released       =  
| runtime        = 97 minutes  
| country        = United States
| language       = English
| budget         = $4.5 million   
| gross          = $744,816 
}} 2009 Venice Film Festival.  It is a direct, but loose sequel to his 1998 film Happiness (1998 film)|Happiness, with new actors playing the same characters. It stars Allison Janney, Shirley Henderson, and Ciarán Hinds, among others.

Solondz said Life During Wartime was "a little more politically overt" than previous works." 

Life During Wartime won the Golden Osella award for Best Screenplay at the 66th Venice International Film Festival.   

==Plot==
The plot revolves around the Jordan sisters featured in Happiness: Trish, Joy, and Helen.

Since the events of Happiness, Joy has married Helens former neighbor Allen Mellencamp, who continues to struggle with his compulsion to make obscene phone calls.

Trish has been raising her three children, Billy (now off at college), Timmy and Chloe. She has begun dating recently divorced Harvey Weiner, who she hopes is normal. Trishs ex-husband, Bill has been released from prison after serving a sentence for child molestation, and heads to Florida to find out how his family, particularly his eldest son, are doing. He finds brief solace in a one-night stand with Jacqueline, a self-described "monster" as filled with loneliness and self-hatred as he is; however, she kicks him out the next morning when she catches him taking money from her purse.

Trishs middle child, Timmy, is preparing for his bar mitzvah and trying to determine what it means to become a man. Trish has for years told Timmy and Chloe that Bill had died, to avoid telling them that he is a Pedophilia|pedophile. Timmy finds out, however, and is angry at her that she lied to him. When he asks her about the mechanics of rape, Trish urges him to scream as soon as any man touches him.

Meanwhile, Joy takes a break from Allen, and heads to Florida to spend time with Trish. She begins having visions of Andy, a former co-worker who had committed suicide shortly after dating her. She briefly goes to California to visit Helen, who has become a successful screenwriter, and calls her husband to leave a message that she is coming home. The audience sees that Allen has committed suicide. She returns to Florida to attend Timmys bar mitzvah.

In the meantime, Bill sneaks into Trishs house to find Billys college address. He pays Billy an unexpected visit at Northern Oregon University, where they discuss their past and the time that passed while Bill was in prison; in particular, Bill asks Billy a few very blunt questions about his sex life. Bill asks for forgiveness, but Billy refuses, saying his actions are unforgivable. Bill then disappears again, reassured that Billy will not turn out to be like him.

Harvey brings his adult son Mark to dinner at Trishs, where he and Trish introduce their children to each other. At Timmys request, Harvey comes to Timmys room to have a talk. Timmy asks Harvey whether he is gay or a pedophile. Harvey denies being either, and, suspecting Timmy has been molested, tries to comfort him, touching his shoulder and hugging him. Terrified, Timmy starts to scream as per his mothers earlier instructions. Trish believes that Harvey tried to molest Timmy, and dumps him.

Timmy has his bar mitzvah, during which Joy experiences visions of Andy and then Allen, who implores her to commit suicide as he has, but Joy refuses and banishes him from her life. Timmy leaves the reception to find Mark, Harveys son. He begs Mark (who, in Palindromes (film)|Palindromes, revealed he had dealt with allegations of pedophilia) for forgiveness, as he made his mistakes before his bar mitzvah (an event marking the beginning of moral accountability). Mark grants him forgiveness, but notes that such gestures are meaningless. Timmy then says that all he wants is his father. In the background, Bill materializes in the same manner that Andy did earlier in the film, but walks off screen as the film ends.

==Cast==
{| class="wikitable"
|-
! Actor !! Character !! Actor who portrayed the character in Happiness (1998 film)|Happiness
|-
| Ally Sheedy || Helen Jordan || Lara Flynn Boyle
|-
| Allison Janney || Trish Jordan/Maplewood || Cynthia Stevenson
|-
| Ciarán Hinds || Bill Maplewood || Dylan Baker
|-
| Chris Marquette || Billy Maplewood || Rufus Read
|-
| Paul Reubens || Andy || Jon Lovitz
|-
| Michael Kenneth Williams || Allen Mellencamp || Philip Seymour Hoffman
|- Jane Adams
|-
| Charlotte Rampling || Jacqueline || N/A
|-
| Renée Taylor || Mona Jordan || Louise Lasser
|- Michael Lerner || Harvey Wiener || Bill Buell (in Welcome to the Dollhouse and Palindromes (film)|Palindromes)
|-
| Dylan Riley Snyder || Timmy Maplewood || Justin Elvin
|-
| Rich Pecci || Mark Wiener ||  Matthew Faber (in Welcome to the Dollhouse and Palindromes (film)|Palindromes)
|-
| Gaby Hoffmann || Wanda || N/A
|}

==Release==

===Release dates===
 : April 16, 2010, distributed by  . 

==Critical reception==
Life During Wartime received mixed to positive reviews; it currently holds a 69% fresh rating, the consensus states "With Life During Wartime, Todd Solondz delivers an unexpected semi-sequel to Happiness in typically uncompromising fashion." 

==Soundtrack==
The title song was performed by Devendra Banhart. The song was written by Solondz, with music by Marc Shaiman; it was produced by Beck and Banhart, with background vocals by Beck.

==Home media==
The film was released by The Criterion Collection in July 2011. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  , September 3, 2009
*  
*   by David Sterritt

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 