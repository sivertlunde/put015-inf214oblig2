Stagecoach War
{{Infobox film
| name           = Stagecoach War 
| image          = Stagecoach War poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lesley Selander
| producer       = Harry Sherman 
| screenplay     = Norman Houston Harry F. Olmsted  William Boyd Russell Hayden Julie Carter Harvey Stephens J. Farrell MacDonald Britt Wood Rad Robinson
| music          = John Leipold
| cinematography = Russell Harlan
| editing        = Sherman A. Rose 	
| studio         = Harry Sherman Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, Russell Hayden, Julie Carter, Harvey Stephens, J. Farrell MacDonald, Britt Wood and Rad Robinson. The film was released on July 12, 1940, by Paramount Pictures.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*Russell Hayden as Lucky Jenkins
*Julie Carter as Shirley Chapman
*Harvey Stephens as Neal Holt
*J. Farrell MacDonald as Jeff Chapman
*Britt Wood as Speedy
*Rad Robinson as Gang Leader Smiley
*Eddy Waller as Wells Fargo Agent Quince Cobalt
*Frank Lackteen as Twister Maxwell
*Jack Rockwell as Matt Gunther  Eddie Dean as Henchman Tom
*The Kings Men as Singing Outlaws 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 