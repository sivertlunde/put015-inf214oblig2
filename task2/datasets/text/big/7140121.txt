Fist of Fury II
 
 
{{Infobox Film
| name = Fist of Fury II
| image = Chinese_connection_2.jpg
| image size = 
| caption = Region 1 DVD Cover
| director = Iksan Lahardi Tso-nam Lee
| producer =Jimmy Shaw
| writer = Hsin-yi Chang
| starring = Bruce Li Lo Lieh Chan Wai-lau Ti Fung
| music = Chow Fu-liang
| editing = Leong Wing-chan
| cinematography = Yeh Ching-piao
| distributor = Hong Kong Alpha Motion Picture Co.
| released =  
| runtime = 104 min.
| country = Hong Kong
| language = Cantonese
}} 
Fist of Fury II ( , aka Chinese Connection 2 and Fist of Fury Part II), is a 1977 Hong Kong kung fu film directed by Iksan Lahardi and Tso-nam Lee, and starring Bruce Li and Lo Lieh. It is the sequel to the Bruce Lee’s 1972s Fist of Fury.

The lead role of Chen Shan, played by Bruce Li, who goes to Shanghai to mourn his brothers death who was killed at the hands of the Japanese people|Japanese. Chen Shan then avenges his brother by killing the Japanese.

The final fight between Chen Shan and Miyamoto, played by Lo Lieh, is generally thought of as disappointing compared to other fights in the film as it is slow and long. This film is generally regarded as one of Bruce Lis other better films.  It wasnt as well received as its predecessor but was thought to be much better than Jackie Chan’s New Fist of Fury. Another sequel exists, called Fist of Fury III (aka Chinese Connection 3).

==Plot== Chen Zhens execution in Shanghai, the Japanese feared that his death would unite all Chinese kung fu schools against them. Fearing this, the Japanese gave orders to the head of the Hong Ku School, Miyamoto (Lo Lieh) to surpass all Chinese schools including the Ching Wu School. Miyamoto sends the Japanese along with their interpreter to the Ching Wu School ordering the leader & students to leave the School. When they refuse, the Japanese beat up the students and destroy the school. Meanwhile, one Chinese man learns about the destruction of the Ching Wu School when he goes to Shanghai to visit Chen Zhens grave. This Chinese man is the only one who has the guts to fight the Japanese, this Chinese is known as Chen Shan (Bruce Li) who is the brother of Chen Zhen and he vows to avenge his brothers death and end the terror of the Japanese once and for all.

==Cast==
*Bruce Li – Chen Shan
*Lo Lieh – Miyamoto
*Chan Wai-lau – Wang Bar
*Ti Fung – Tin Man Kwai
*Kun Li – Lee Shun
*Yasuyoshi Shikamura – Yanagi Saburo
*James Nam – Souto Jyo
*Chao Kin – Inspector Chiu
*Shin Nam – Policeman #1
*Shiu Yu – Policeman #2
*Mui-shao Sui – Sister
*Kam To – Kam Fuk
*Kin-ming Lee – Cheung S’mg Hung
*Cheng-hai Ching – Shun Chui
*Shun-chiu Bo – Ching Wu’s Brother #1
*Fa-yuan Li – Ching Wu’s Brother #2
*Chiang Li – Ching Wu’s Brother #3
*Ze-tin Ku – Japanese Knight #1
*You-pin Liu – Japanese Knight #2
*Hau-bao Wai – Japanese Knight #3
*Chiu-hong Seng – Japanese Knight #4
*Tai-kin Yin – Japanese Knight #5
*Chan-sum Lam – Japanese Knight #6
*Wai-hung Ho – Japanese Knight #7
*To Wai-wo

==Archive footage== Chen Zhen

==Action choreographer==
*Lee Gam-ming

==Reaction==
In 2001, Bruce Li fanzine Exit the Dragon, Enter the Tiger, Carl Jones spoke favourably of this film: "Lis martial skills are in top form here and his fights are well choreographed by veteran Tommy Lee. The final duel with Miyamoto is well staged and takes place at the Ching Wu school All in all a worthy sequel to a great Kung Fu classic."  

Also admiring is Dean Medows, who wrote in his three-part  ."  

==Trivia==
*The adventures of Chen Shen were continued in 1980 film Fist of Fury III.

*Bruce Li has said that out of all the movies he made, this was one of only three that he liked (the other two were Dynamo and The Chinese Stuntman. 

*There is some debate as to when this film was released.  IMDb lists the release year as 1977, but the Hong Kong release date on 5 April 1979 (which is also the year that the Hong Kong Movie Database lists).  Some websites say the film was from as early as 1976. 

*IMDb credits the directors as Iksan Lahardi and Tso-nam Lee, while the Hong Kong Movie Database credits Siu Fung, and other websites credit Jimmy Shaw.

*Nora Miaos part in Fist of Fury is played in this movie by a different woman (she is the only woman in the film) whose face is concealed by a hood, she killed herself near the beginning of the film.

*While veteran Fist of Fury actors Nora Miao and Han Ying-chieh acted in the "official" sequel in 1976 film New Fist of Fury (with the former playing her same character while Han played a new character), fellow veteran Fist of Fury actors Tien Feng and Li Kun reprised their roles for this film.

*The set of the Ching-Wu school, seen in Fist of Fury, was recreated for this film, and appears very similar to the original film.

*There is an infamous moment in some prints in where Chen Shan takes a pair of nunchaku out of a Ching-Wu students hands and the nunchaku suddenly disappear. This is due to a ban on nunchaku scenes in the United Kingdom. The UK video version has apparently been the source of the US DVDs but some UK tapes were released uncut. Ironically, one of the most widely seen publicity photos from this film involves Bruce Li holding said weapon.

*Due to Fist of Fury III being released on video in the US by Video Gems under the title Fist of Fury II, there has been frequent confusion as to which film is really the first sequel to the original Fist of Fury. However, the Chinese title of this film translates as "Jing Wu Men sequel".

==See also==
*List of Hong Kong films
*Lo Lieh filmography

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 