Manjhdhar (1947 film)
{{Infobox film
| name           = Manjhdhar
| image          = 
| image_size     = 
| caption        = 
| director       = Sohrab Modi
| producer       = Sohrab Modi
| writer         = Pandit Sudarshan
| narrator       =  Khursheed Surendra Surendra Sadiq Ali Ghulam Haider Anil Biswas
| cinematography = Dara Mistry
| editing        = 
| distributor    = 
| studio         = Minerva Movietone
| released       = 1947
| runtime        = 121 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1947 Hindi/Urdu Ghulam Haider, Anil Biswas, the lyricist was Shams Lucknowi.  The film starred Sohrab Modi, Khursheed Bano|Khursheed, Surendra (actor)|Surendra, Eruch Tarapore and Rafiq Ghaznavi. 

==Cast==
* Sohrab Modi Khursheed
* Surendra
* Sadiq Ali Tabassum
* Eruch Tarapore
* Rafiq Ghaznavi
* Surekha

==Soundtrack== Ghulam Haider, Anil Biswas Surendra and Khursheed Some of the notable songs of this film were "Jiske Milne Ki Tamanna Thi Wo Pyar Mil Gaya" composed by Ghulam Haider and sung by Khursheed, "Prem Nagar Ki Oar Chale Hain" composed by Gyan Dutt and sung by Khursheed and Surendra, and "Mera Chand Aa Gaya Mere Dware" composed by Anil Biswas and sung by Khursheed and Surendra. 
===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Kya Hai Nari Ki Shaan 
| Surendra
|-
| 2
| Kyun Tu Mujhse Rooth Gayi 
| Surendra
|-
| 3
| Aaj Mohe Sajan Ghar Jana 
| Khursheed
|-
| 4
| Jiske Milne Ki Tamanna Thi Wo Pyar Mil Gaya 
| Khursheed
|-
| 5
| Thi Mujhse Aaj Tak Ye Haqiqat Chhupi Hui 
| Khursheed
|-
| 6
| Nache Hai Man Mauj Magan Mein
| Khursheed
|-
| 7
| Mera Chand Aa Gaya Mere Dware 
| Surendra, Khursheed
|-
| 8
| Prem Nagar Ki Oar Chale Hain 
| Surendra,Khursheed
|-
| 9
| Mohabbat Karegi Asar Chupke Chupke 
|
|}

==References==
 

==External links==
* 

 

 
 
 
 
 

 