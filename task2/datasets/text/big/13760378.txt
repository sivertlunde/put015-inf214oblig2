Strange Affair (film)
{{Infobox film
| name           = Strange Affair
| image          =
| caption        = 
| director       = Pierre Granier-Deferre
| producer       = Alain Sarde
| writer         = Christopher Frank Pierre Granier-Deferre Jean-Marc Roberts
| starring       = Michel Piccoli Gérard Lanvin Nathalie Baye
| music          = Philippe Sarde
| cinematography = Étienne Becker
| editing        = Isabel García de Herreros
| distributor    = Parafrance Films
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Strange Affair ( ) is a 1981 French drama film directed by Pierre Granier-Deferre, and starring Michel Piccoli, Gérard Lanvin and Nathalie Baye.

== Plot ==
Louis Coline (Gérard Lanvin) is an executive assistant in a declining Paris department store. While he is not ambitious and has little to do, he is content with his wife Nina (Nathalie Baye), visits to his mother and grandmother and nights out with his friends playing poker. However, the manipulative Bertrand Malair (Michel Piccoli) becomes the stores new manager, and arrives to shake up the company. Louis fears for his job, but Bertrand draws him into his inner circle of confidants. Bertrand starts to control Louis life, while Louis is afraid of losing his privileged position. To maintain it Louis works overtime, accepts Bertrands invitations to nightclubs and dinners with an androgynous woman, and places his house at Bertrands disposal. Nina sees through her husbands behaviour and objects, but Louis cannot refuse Bertrands demands, and she is unable to make him see how much his personality has changed. Desperate, she leaves her husband, which only draws him further under Bertrands control. Bertrand soon disappears as mysteriously as he came, and Louis finds himself unable to revert to his previous self.

==Cast==
* Michel Piccoli as Bertrand Malair
* Gérard Lanvin as Louis Coline
* Nathalie Baye as Nina Coline
* Jean-Pierre Kalfon as François Lingre
* Jean-François Balmer as Paul Belais
* Pierre Michaël as Gérard Doutre
* Madeleine Cheminat as Yvette, la grand-mère de Louis
* Victor Garrivier as Le père de Nina
* Dominique Blanchar as La mère de Louis
* André Chaumeau
* Jacques Boudet as M. Blain, le chef du personnel
* Ariane Lartéguy as Salomé
* Nicolas Vogel as René
* Dominique Zardi as Gruault, chef des contentieux

==Awards==
The film won the Louis Delluc Prize and the César Award for Best Actress in a Supporting Role, and it received César nominations for best actor, best supporting actor, best director and best writing. Michel Piccoli won the Silver Bear for Best Actor at the 32nd Berlin International Film Festival.   

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 