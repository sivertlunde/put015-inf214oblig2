Astro Boy (film)
{{Infobox film
| name           = Astro Boy
| image          = Astro boy ver7.jpg
| alt            = 
| caption        = Theatrical release poster David Bowers
| producer       = Maryann Garger Kuzuka Yayoki Timothy Harris David Bowers
| story          = David Bowers
| based on       =  
| starring       = {{plainlist |  
*Freddie Highmore
*Kristen Bell
*Nathan Lane
*Eugene Levy
*Matt Lucas
*Bill Nighy
*Donald Sutherland
*Charlize Theron
*Nicolas Cage}}
| music          = John Ottman
| cinematography = Pepe Valencia
| editing        = Robert Anich Cole
| studio         = Imagi Animation Studios Tezuka Productions
| distributor    = Summit Entertainment
| released       =  
| runtime        = 95 minutes
| country        = United States Hong Kong China
| language       = English
| budget         = $65 million 
| gross          = $44,093,014 
}} manga and David Bowers.  Freddie Highmore provides the voice of Astro Boy in the film.    The film also features the voices of Kristen Bell, Nathan Lane, Eugene Levy, Matt Lucas, Bill Nighy, Donald Sutherland, Charlize Theron and Nicolas Cage. The film was released first in Hong Kong on October 8, 2009, Japan on October 10, 2009 and in the United States on October 23, 2009.

==Plot== Toby Tenma is a teenager who lives in the futuristic city-state Metro City, which floats above the polluted "Surface". Tobys father, Dr. Tenma, is a famous roboticist and head of the Ministry of Science, but has a distant relationship with his son. Dr. Tenma meets the citys obstructive leader President Stone to demonstrate a new defensive robot called the Peacekeeper. To power it, Tenmas friend Dr. Elefun unveils the Blue and Red Core, two energy spheres which have opposing positive and negative energy. Stone has the destructive Red Core placed into the Peacekeeper, causing it to go berserk, leading to Toby being disintegrated after sneaking into the room.
 robotic replica of Toby, complete with his memories, but has in-built defences to protect him and is powered by the Blue Core. Tobys replica is awakened, believing nothing has happened. While he has Tobys mind, his personality is different in Tenmas eyes, when in actuality it is quite similar. Toby discovers his robotic capabilities including rocket-powered flight and the ability to understand other robots. Toby flies home but learns from Tenma of his origins and is rejected by him, flying away much to the sadness of Dr. Elefun. Stone, desperate to win a re-election, has Toby pursued by his guards but the battle leads to Toby tumbling off the city edge onto the Surface. Tenma and Elefun are arrested.

Toby awakens in an enormous junkyard, created from the redundant robots dumped by Metro City. He is found by a group of human children, illiterate but smart Zane, twins Sludge and Widget, and the oldest Cora who has a grudge against Metro City. They accompanied by a dog-like waste dispodal robot named Trashcan. Toby also meets the members of the Robot Revolutionary Front (RRF), Sparx, Robotsky, and Mike the Fridge, who wish to free robots from mankinds control but are very inept and bound by the Laws of Robotics. However, they give Toby a new name, calling him "Astro". Astro departs with the kids, finding people still live on the Surface. He is taken in by robot repairman Hamegg, who also runs a robot fighting ring. The next day, Astro comes across an offline construction robot Zog, who he revives with the Blue Cores energy. Hamegg accidentally scans Astro, realising he is a robot, and paralyzes him the next day to participate in the fighting ring.

Astro defeats Hameggs fighters until Zog is deployed, but the two robots refuse to fight one another. Hamegg assaults them with an electrical blaster, only for Zog to attempt to harm him, immune from the Laws of Robotics. Astro saves Hamegg. President Stones flagship arrives and Astro is taken back to Metro City. Reuniting with Tenma and Elefun, Astro agrees to be shut down, apologising to his father for not being Toby. Realising Astro is still his son, Dr. Tenma reactivates him and lets him escape. Stone loads the Red Core into the Peacekeeper, only for it to absorb him and take on his personality. The Peacekeeper goes on a rampage across Metro City, prompting Astro to battle it. During the fight, Metro Citys systems shut down, causing it to tumble to the ground. Astro uses his superhuman strength to help it land safely.

The Peacekeeper grabs Astro, but the connection of their cores causes them both pain. Dr. Tenma finds Astro and informs him that if the two cores reunite, they will be destroyed. When Astros friends are captured, he flies into the Red Core, destroying himself and the Peacekeeper. Stone survives but is arrested for his crimes. Dr. Elefun and the children find Astros body, discovering the Blue Core is dead. Zog appears and revives Astro using the Blue Core energy given to him. Astro reunites with his loved ones, making peace with his father. A cycloptic extraterrestrial attacks the city, Astro leaping into action as the film ends.

==Cast== Toby / Astro Boy (character)|Astro, Dr. Tenmas son who is tragically killed in the introduction. Astro is a robotic replica of Toby built in his image and with his memories. Rejected by his father, Astro is trying to find his place in the world. He runs on Blue Core energy in the film.
* Nicolas Cage as Doctor Tenma|Dr. Tenma, Tobys father, creator of Astro, and head of Metro Citys Ministry of Science
* Kristen Bell as Cora,  a tomboy kid who lives on the Surface and is Astros best friend.
* Bill Nighy as Professor Ochanomizu|Dr. Elefun, Dr. Tenmas friend & associate and as Robotsky of the Robot Revolutionary Front.
* Donald Sutherland as President Stone, the former ruthless President of Metro City who is running for re-election who serves as the films main antagonist.
* Samuel L. Jackson as ZOG, a 100-year-old construction robot brought back to life by Astros blue-core energy
* Nathan Lane as Hamegg, a surface-dweller who repairs machines and then uses them in his fighting tournament
* Matt Lucas as Sparx, the leader of the Robot Revolutionary Front. David Bowers as Mike the Fridge, a talking refrigerator and third member of the Robot Revolutionary Front
* Charlize Theron as "Our Friends" Narrator, of an educational video seen at the films beginning
* Eugene Levy as Orrin, Tenmas robotic and household servant
* Moisés Arias as Zane, a surface-dwelling child
* Alan Tudyk as Mr. Squeegee, a cleaning robot
* David Alan Grier as Mr. Squirt, a cleaning robot
* Madeline Carroll as Widget, Sludges twin
* Sterling Beaumon as Sludge, Widgets twin
* Dee Bradley Baker as Trashcan, a dog-like robot who serves as a literal bin
* Elle Fanning as Grace, a girl from Hameggs house who kicks President Stone in the leg
* Ryan Stiles as Mustachio (Shunsaku Ban)|Mr. Mustachio, Tobys teacher.
* Newell Alexander as General Heckler, President Stones head of military.
* Victor Bonavida as Sam, a teenage boy from Hameggs house
* Tony Matthews as Coras father
* Bob Logan as Stinger One, President Stones pilot minion who leads a group of aircraft with suction tubes and wants to capture Astro
* Ryan Ochoa as Rick, another teenage boy from Hameggs house

==Production==
In 1997, Sony Pictures Entertainment purchased the film rights to Astro Boy from Tezuka Productions, intending to produce a  live-action feature film. Todd Alcott was set to write the screenplay, but the film halted in 2000 when Steven Spielberg began A.I. Artificial Intelligence|A.I., another film with a robot boy who replaces a dead child.  In December 2001, Sony hired Eric Leighton to direct an all-CGI film, with Angry Films and Jim Henson Productions producing it for a 2004 release.  A screenplay draft was written, but the film did not go into production, and Leighton left in early 2003 to pursue other film projects.
 The Power of the Dark Crystal, another co-production with Jim Henson Productions.  
 David Bowers.  

===Design===
When adapting the film for a western audience and making the leap from 2D to 3D, some changes to Astro had to be made. The more challenging was his kawaii portrayal, part of which were his large eyes and curly eyelashes, which made him too feminine. Imagi had several discussions on how round and curvy Astros body proportions should be and in the end they were made to be more lean.  Also there were issues on Astros rear end being too small, and that too was altered. 
The by-product of these changes was Astros Caucasian look.   

In early development Astros design was younger, resembling his iconic design of a 6-year-old boy. The design team changed that and made him look like a 12-year-old to appeal to a larger audience.  They also gave him a shirt, and a jacket since they thought it would be strange to have a normal boy running around without one.  They also replaced his heart-shaped energy core with a glowing blue one. 

===Music===
The score to Astro Boy was composed by John Ottman, who recorded his score with a 95-piece orchestra and choir at Abbey Road Studios.     A soundtrack album was released on October 20, 2009 by Varèse Sarabande Records.

==Release==

===Marketing===

Beginning in May 2009 and continuing through September 2009, IDW Publishing published a "prequel" and comic book adaptation of the film as both mini-series and in graphic novel format to coincide with the North American release of the film in October 2009.

A model of a motionless Astro Boy waiting to be powered up was set up at Peak Tower, Hong Kong, outside Madame Tussauds Hong Kong in September 2009.

A panel of the film was held at the San Diego Comic-Con on July 23, 2009. 

===Home media===
Astro Boy was released in the US on DVD and Blu-ray March 16, 2010, by Summit Entertainment. Both releases include two new animated sequences; a featurette with the voice cast including Nicolas Cage, Kristen Bell, Freddie Highmore and Donald Sutherland, among others; three other featurettes about drawing Astro Boy, making an animated movie and getting the Astro Boy look; and an image gallery. 

In Japan a special Limited Edition Astro Boy Premium Box Set was released. It featured the same content from the US release with the exception of it spanning on two DVDs (One the film, the other special features with two bonus features exclusive to Japanese) and has both English and Japanese dub (along with English and Japanese subtitles.) The box set also comes with a DVD (containing a single story on Astros first flight and an image gallery), Dr Tenmas Project Notes (featuring 80 pages of CGI models, character art and set designs from the film), a Micro SD (featuring the motion manga Atomu Tanjo (Birth of Astro Boy) originally written by Osamu Tezuka), a postcard of 1980 Astro Boy flying, a small bookmark (that is actually a reel from the film inside a plastic cover) and Astros blueprints from the film.  

==Reception==

===Critical response=== rating average of 5.6 out of 10. The sites general consensus is that "While it isnt terribly original, and it seems to have a political agenda that may rankle some viewers, Astro Boy boasts enough visual thrills to please its target demographic."  On Metacritic, which assigns an average rating out of 100 to reviews from film critics, has a rating score of 53 based on 58 reviews. 

Owen Gleiberman of Entertainment Weekly gave the film a B and wrote of the film having "little too much lost-boys-and-girls mopiness", but "Astro Boy is a marvelously designed piece of cartoon kinetics..."  Glenn Whipp of the Los Angeles Times gave the mixed review claiming "The kids wont get it but will enjoy the big, climactic robot rumpuses, which owe a heavy debt to Brad Birds The Iron Giant". 

===Box office=== Speed Racer, other American-produced films based on Japanese sources that were not big hits in the land of their origin but were very successful in China.  The film also was not a box office success in the U.S., opening at #6, grossing $6.7 million,  where it remained in the Top 10 for three weeks. When it closed in January 2010, it had a total gross of $20 million. 

==Video game==
  PSP versions were developed by High Voltage Software, and the Nintendo DS version by Art Co., Ltd. 

== See also ==
 

==References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 