Papam Pasivadu
{{Infobox film
| name           = Papam Pasivadu
| image          = Papam Pasivadu.jpg
| image_size     =
| caption        =
| director       = V. Ramachandra Rao
| producer       = Atluri Seshagiri Rao
| writer         = Gollapudi Maruthi Rao
| narrator       = Nagesh Kaikala Rajababu Chittor V. Nagaiah Chaya Devi Prabhakar Reddy Suryakantham
| music          = Chellapilla Satyam
| cinematography = M. Kannappa
| editing        = Balu
| studio         = Vauhini Studios
| distributor    =
| released       = 1972
| runtime        =
| country        = India Telugu
| budget         =
}}

Papam Pasivadu is a 1972 Telugu film directed by V. Ramachandra Rao. It is remake of South African film Lost in the Desert (Dirkie) (1969) directed by Jamie Uys.

==Plot==
Venugopal (Ranga Rao) and Janaki (Devika) are loving couple. They get a baby boy with lot of happiness in their heart. During the childhood, he is attacked by Tuberculosis. He was planned to get the best treatment. According to the advice of their family doctor (Nagaiah) and send him to Europe with pilot (Nagesh). The plane crashes in a desert due to sudden heart attack to the pilot. The boy has to face several obstacles and finally survives to meet the parents happily.

==Credits==

===Cast===
* child artist  Master Ramuraghuvaran
* S. V. Ranga Rao	...    Venugopal
* Devika	...    Janaki Nagesh   ...     Pilot
* Kaikala Satyanarayana   ...   Narasimham
* Suryakantham  ...   Durgamma
* Chaya Devi  ...   Subbamma
* Director: V. Ramachandra Rao
* Producer: Atluri Seshagiri Rao
* Story, Dialogues, Screenplay : Gollapudi Maruthi Rao
* Cinematography: M. Kannappa
* Choreography: K. S. Reddy
* Editor: Balu
* Lyrics: C. Narayana Reddy, Aathreya, Kosaraju Raghavaiah
* Playback singers: Ghantasala Venkateswara Rao, S.P. Balasubrahmanyam, P. Susheela, L. R. Eswari
* Music Director: Chellapilla Satyam

==Songs==
* Amma Choodali Ninnu Naannani Choodali (Lyrics:  )
* Are Manchi Annadhey Kaanaraadu (Lyrics:  )
* Ayyo Pasivada Papam Pasivada (Singer: Ghantasala (singer)|Ghantasala)
* O Baboo Maa Baboo Neekanna Maaku Pennidhi Evaru (Singer: P. Susheela)

==External links==
*  
*  

 
 
 


 