Shankar Hussain
{{Infobox film
| name           = Shankar Hussain
| image          = 
| image_size     =
| caption        = 
| director       = Yusuf Naqvi
| producer       = Tajdar Kamal and Hari Singh Aney
| writer         =Kamal Amrohi (Dialogue)
| narrator       =  Kanwaljit Singh 
| lyrics.          =Jan Nisar Akhtar,  Kaif Bhopali, Kaifi Azmi and Kamal Amrohi Khayyam Jan Nisar Akhtar, Kaif Bhopali, Kaifi Azmi and Kamal Amrohi (lyrics)
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Bollywood film directed by Yusuf Naqvi. It has memorable songs by  Rafi sing Kahin ek Masum Nazuk si Lardki sung by Rafi and Aaap yun Faaslon Se sung by Lata, both classics Khayyams repertoire. 
Short Story : Dr. Uday Shankar, rescues a Muslim child, Husain from a devastating flood and raises him along with his own son Ajay. He makes Husain follow his Muslim roots against the backdrop of his Hindu household. Years later, Husain is studying in college while Ajay returns home after his medical studies. Soon, Dr. Uday Shankar passes away and Ajay continues to help the poor of the village to maintain his fathers legacy. Husain falls in love with a Muslim girl Gulsum while Ajay too falls in love with a girl named Kusum. But things get complicated when they find out that Gulsum and Kusum are the same girl. Soon a series of unfortunate events puts Dr. Uday Shankars family in turmoil and Husain rises up to save his familys name. What is the mystery behind the girl that both brothers have fallen in love with? How will Husain save the honor and dignity of his family?

==Cast==
*Pradeep Kumar    Kanwaljit Singh  
*Madhu Chanda   
*Suhail   
*Gajanan Jagirdar   
*Shreeram Lagoo   
*Jalal Agha   
*Dina Pathak

==Songs== Khayyam 
#  - Lata Mangeshkar - Jan Nisar Akhtar (Lyrics)
#  - Lata Mangeshkar
#  - Mohammed Rafi

==External links==
*  
*   at Bollywood Hungama

 
 
 


 