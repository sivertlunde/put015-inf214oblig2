Kalyani (1971 film)
{{Infobox film
| name           = Kalyani
| image          =
| caption        = 
| director       = Geethapriya
| producer       = M V Chandre Gowda
| writer         = 
| screenplay     =  Jayanthi Gangadhar Gangadhar Ranga Vijayakala
| music          = Vijaya Bhaskar
| cinematography = V Manohar
| editing        = 
| studio         = 
| distrubutor    = 
| released       =  
| country        = India Kannada
}}
 1971 Cinema Indian Kannada Kannada film, directed by Geethapriya and produced by M V Chandre Gowda. The film stars Jayanthi (actress)|Jayanthi, Gangadhar (Kannada actor)|Gangadhar, Ranga and Vijayakala in lead roles. The film had musical score by Vijaya Bhaskar.   

==Cast== Jayanthi
*Gangadhar Gangadhar
*Ranga
*Vijayakala
*K. S. Ashwath
*M Jayashree

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ondhe Ondhe || Sudhakar, Anjali || Geetha Priya || 03.20
|-
| 2 || Veena Ninagyeko || BK. Sumithra || Geetha Priya || 03.26
|}

==References==
 

==External links==
*  

 

 
 
 
 


 