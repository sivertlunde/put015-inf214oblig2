Kaadu (1952 film)
 Kaadu}}
 
{{Infobox film
| name     = Kaadu (The Jungle) 
| image    = Kaadu the jungle.jpg
| alt      =
| caption  = American poster
| director = William Berke
| producer = William Berke  Ellis Dungan  Robert L. Lippert
| story    =  Rod Cameron Cesar Romero Marie Windsor M.N. Nambiar
| music    = G. Ramanathan
| cinematography = Clyde De Vinna
| editing  = L. Balu
| studio   =
| distributor =
| released =  
| runtime = 73 min
| country = India USA Tamil English
| budget = 
| gross =
}} American Sci-Fi Rod Cameron, Cesar Romero, Marie Windsor and M.N. Nambiar in lead roles. The film was the first Sci-Fi movie in India. Kaadu released on 1 August 1952.  

==Plot==
A great white hunter and an Indian princess trek into the Indian jungle to investigate a number of wild animal stampedes which have resulted in the deaths of many people. On their journey, they discover a herd of prehistoric wooly mammoths are responsible for the terror.

==See also==
*Science fiction films in India

==References==
 

 
 


 