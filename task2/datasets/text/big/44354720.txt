The Deadly Breaking Sword
{{Infobox film
| name           = The Deadly Breaking Sword
| image          = TheDeadlyBreakingSword.jpg
| alt            =
| caption        = Film poster
| film name      = {{Film name
| traditional    = 風流斷劍小小刀
| simplified     = 风流断剑小小刀
| pinyin         = Feng Liú Duàn Jiàn Xiao Xiao Dao
| jyutping       = Fung1 Lau4 Dyun3 Gim3 Siu2 Siu2 Dou1 }}
| director       = Sun Chung
| producer       = Run Run Shaw
| writer         = 
| screenplay     = Ni Kuang
| story          = 
| based on       =  Michael Chan Lily Li
| narrator       = 
| music          = Frankie Chan Joseph Koo
| cinematography = Cho On-sun Lam Ngai Kai
| editing        = Chiang Hsing-lung Yu Siu-fung
| studio         = Shaw Brothers Studio
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        = 101 minutes
| country        = Hong Kong Mandarin
| budget         = 
| gross          = HK$2,716,494
}}

The Deadly Breaking Sword is a 1979 Hong Kong wuxia film directed by Sun Chung and starring Ti Lung and Alexander Fu Sheng.

==Plot==
Warrior Tuan Changqing (Ti Lung) meets courtesan Liu Yinxi (Shih Szu) in a small town. Liu pleads Tuan to kill underworld master Guo Tiansheng (Ku Feng), known as the "Killer Doctor", in oder to avenge the death of her fiance. Tuan and casino security guard Xiao Dao (Alexander Fu Sheng) breaks into Guos household and trigeering the battle of good against evil.

==Cast==
*Ti Lung as Tuan Changqing, the Deadly Breaking Sword
*Alexander Fu Sheng as Xiao Dao
*Shih Szu as Liu Yinxu
*Ku Feng as Dr. Guo Tiansheng, the Killer Doctor Michael Chan as Lian San, the Throat Piercing Halberd
*Lily Li as Luo Jinhua
*Shum Lo as Boss Luo
*Chan Shen as Officer Fan Fei
*Ngai Fei as Chen Yinggang
*Teresa Ha as Madam Li Xing
*Kara Hui as Xiaoqin
*Keung Hon as Dr. Guos hanger-on
*Eddy Ko as Dr. Guos hanger-on
*Ng Hong-sang as Dr. Guos hanger-on
*Yuen Wah as Dr. Guos hanger-on
*Chow Kin-ping as Dr. Guos hanger-on
*Austin Wai as Sword Spirits Duo (red)
*Yuen Bun as Sword Spirits Duo (gold)
*Cheung Kwok-wa as Diao Qi
*Lam Fai-wong aas Troublemaker at the casino
*Jamie Luk as Troublemaker at the casino
*Wong Ching-ho as Conman
*Ting Tung as Prison Guard
*Tang Wai-ho as Prison guard
*Cheung Hei as Innkeeper
*Man Man as Inn waiter
*Wong Kung-miu as Inn waiter
*Fong Yue as Prostitute
*Sai Gwa-pau as Brothel worker
*Kam Tin-chue as Brothel guest
*Tony Lee as Gambler
*Tam Wai-man as Casino bouncer
*Lam Chi-tai as Prison guard
*Robert Mak as Prison guard
*Wong Chi-ming as Prison guard
*Hung Ling-ling as Prostitute
*Lui Hung as Prostitute
*Alan Chan

==Theme song==
*True Colors of a Beautiful Hero (美雄本色)   / Beauty Renowned to the World (五湖四海美名揚)  
**Composer: Joseph Koo
**Lyricist: Sun Yi
**Singer: Jenny Tseng

==Reception==
===Critical===
Ian Jane of DVD Talk gave the film a positive review and writes "The film might seem a little dated considering that it came out in the mid-eighties, past the genres prime by a bit of a margin, but it stands as a good example of how fun and exciting a well made Shaw Brothers swordplay could be when delivered by a dedicated director and a talented cast." 

===Box office===
The film grossed HK$2,716,494 at the Hong Kong box office during its theatrical run from 12 to 26 April 1979 in Hong Kong.

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 