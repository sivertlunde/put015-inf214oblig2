Chantha
{{Infobox film 
| name           = Chantha
| image          = CHANTHA-.gif 
| caption        = The first film of M.Jayachandran as independent Music Director 
| director       = Sunil
| producer       = 
| writer         = UA Khader
| screenplay     = 
| starring       = Thilakan Sathaar Augustine Babu Antony
| music          = M Jayachandran
| cinematography = Venu
| editing        = 
| studio         = Fax Productions & Release
| distributor    = Fax Productions & Release
| released       =  
| country        = India Malayalam
}}
 1995 Cinema Indian Malayalam Malayalam film, directed by Sunil. The film stars Thilakan, Sathaar, Augustine and Babu Antony in lead roles. The film had musical score by M Jayachandran.  

==Cast==
*Thilakan
*Sathaar
*Augustine
*Babu Antony Devan
*Lalu Alex Mohini
*Narendra Prasad
*Sadiq

==Soundtrack==
The music was composed by M Jayachandran and lyrics was written by Gireesh Puthenchery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kodi Ketti || MG Sreekumar || Gireesh Puthenchery || 
|-
| 2 || Yatheemin Sulthaan Vanne || MG Sreekumar || Gireesh Puthenchery || 
|}

==References==
 

==External links==

 
 
 

 