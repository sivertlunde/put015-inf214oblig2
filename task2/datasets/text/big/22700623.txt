Home (2009 film)
 
 
{{Infobox film
| name           = Home
| image          = HOME-SHOT.jpg
| caption        = The films title card
| director       = Yann Arthus-Bertrand
| producer       = Denis Carot Luc Besson
| writer         = Isabelle Delannoy Yann Arthus-Bertrand Denis Carot Yen le Van
| narrator       = Glenn Close   Jacques Gamblin   Salma Hayek   Mahmood Said   Zhou Xun   Stephen Chan Chi Wan  
| music          = Armand Amar
| cinematography = Michel Benjamin Dominique Gentil
| editing        = Yen le Van
| studio         = Europa Corp.
| distributor    = Europa Corp., with sponsorship from Kering
| released       =  
| runtime        = 120 minutes
| country        = France
| budget         = $12 million
| gross          = 
}}
Home is a 2009 documentary by Yann Arthus-Bertrand. The film is almost entirely composed of aerial shots of various places on Earth. It shows the diversity of life on Earth and how humanity is threatening the ecological balance of the planet. 
The English version was read by Glenn Close. The Spanish version was read by Salma Hayek. The Arabic version was read by Mahmood Said. The film had its world festival premiere at the Dawn Breakers International Film Festival in 2012. Before the festival premier, it was released simultaneously on 5 June 2009, in cinemas across the globe, on DVD, Blu-ray, television, and on YouTube, opening in 181 countries.   The film was financed by Kering, a French multinational holding company specializing in retail shops and luxury brands, as part of their public relation strategy.  

==Production==
Home was filmed in various stages due to the expanse of the areas portrayed. Taking over eighteen months to complete the film, director Yann Arthus-Bertrand and a camera man, a camera engineer and a pilot flew in a small helicopter through various regions in over fifty countries. The filming was done using High-definition video|high-definition "Cineflex" cameras which were suspended from a gyro-stabilized sphere from rails on the base of the helicopter. These cameras, originally manufactured for army firing equipment, reduce vibrations helping to capture smooth images, which appear as if they had been filmed from crane arms or Camera dolly|dollies. After almost every flight, recordings were immediately checked to ensure they were usable.  After filming was complete, Besson and his crew had over 488 hours of footage to edit. 

==Distribution and promotion ==
 
To promote the documentary online, a YouTube channel known as "HomeProject" was created. Uploaded to this were various short clips of filming which took place in different parts of the world including the Arctic Circle, Africa and the large metropolises featured. 

On 9 March 2009, a press-conference was held in Paris, France, where Yann Arthus-Bertrand and various producers talked to the media about the issues raised in the film, as well as confirming that Home would be the first film ever to be simultaneously released in theaters, on television, on DVD and on the Internet in five continents. 
 PPR was going to sponsor the film in order to facilitate unavoidable costs. 

The film, which was available for free release until 14 June, has been broadcast in 14 languages.      The Blu-ray edition was released by 20th Century Fox and features both the English and French versions.  It is expected to sell in excess of 100,000 copies. When production costs are met, all proceeds sale takings will go to the Good Planet Company.  

==Copyright and redistribution rights== TED talk   that the movie has no copyright:
"This film have  no copyright. On the fifth of June, the environmental day, everyone can download the movie on Internet. The film is given for free by the distributor to TV and theater to show it on June 5th." Nevertheless, a copyright notice appears in the final credits.

Several high resolution editions of the movie are available for download.   format, and   and   also offer high resolution editions.

==Public response==
The film received a large response upon release, receiving over 400,000 combined views within the first 24 hours on YouTube.   As of June 2012, the French, English, German, Spanish, Russian and Arabic versions on Youtube logged a total of more than 32 million views. It was shown to high ratings on channels around the world including the international network National Geographic Channel. France2 débuted the film to over 8.3 million viewers in France alone.  In India, Home was shown exclusively via the STAR World cable network. 

==Critical response==
Generally, the movie was praised for its visuals but received criticism regarding the attitude of the narration and the contradiction between its message and the sponsors legacy.

Jeannette Catsoulis of the New York Times criticizes the films narration and Glenn Close, narrator in the English version, both regarding content and style: "We’ve heard it all before, if not in the schoolmarmish tones of Glenn Close, whose patronizing narration   makes the film feel almost as long as the life of its subject." Furthermore, she denounces the films accusations towards the modern "lifestyle that destroys the essential to produce the superfluous — an accusation that the film’s bankrollers, led by the corporation behind luxury brands like Balenciaga and Gucci, are probably familiar with..." 

Jean-Michel Frodon, a French movie critic, expressed the opinion that "‘Home’ had many viewers but didn’t have much echo" because Arthus-Bertrand’s personality, activities and his innovative no-cost concept have captured more attention than the movie itself. 

==See also==
  Planet Ocean - a 2012 movie by Yann Arthus-Bertrand and Michael Pitiot. Planet Earth – a BBC TV series
*Qatsi trilogy – a series of three films by Godfrey Reggio
*Chronos (film)|Chronos – a film by Ron Fricke that explores the passage of time
*Baraka (film)|Baraka
*An Inconvenient Truth

==References==
 

==External links==
*   (requires Adobe Flash)
*  
*  
*  , a press release from the United Nations Environment Programme
*  
*   YouTube channel, where the film can be watched for free in its entirety TED

 

 
 
 
 
 
 
 
 