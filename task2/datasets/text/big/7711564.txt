Aaltoska orkaniseeraa
{{Infobox film
| name           = Aaltoska orkaniseeraa
| image          = Aaltoskaorkaniseeraa.jpg
| image_size     =
| caption        = 
| director       = Edvin Laine
| producer       = 
| writer         = Toivo Kauppinen
| narrator       = 
| starring       = Joel Asikainen  Matti Aulos
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 15 July, 1949
| runtime        = 
| country        = Finland
| language       = Finnish
| budget         = 
}} 1949 Finland|Finnish film directed by Edvin Laine and written by Toivo Kauppinen. A comedy the film starred Joel Asikainen and Matti Aulos.

The film was released on 15 July, 1949.

==Other cast==   
*Sasu Haapanen 
*Hannes Häyrinen ....  Lipponen, crime reporter 
*Elna Hellman ....  Mrs. Aaltonen 
*Eija Inkeri   
*Pentti Irjala .... Constable (uncredited)
*Mauri Jaakkola   
*Armas Jokio ....  Room applicant 
*Veli-Matti Kaitala ....  Jukka (as Veli-Matti) 
*Birger Kortman   
*Irja Kuusla

==External links==
*  

 
 
 
 
 
 
 