Sautela Bhai
{{Multiple issues|
  }}

{{Infobox film
| name           = Sautela Bhai 
| image          = SautelaBhaifilm.png
| caption        = Promotional Poster
| director       = B. R. Ishara
| producer       = Satish Khanna
| writer         = B. R. Ishara
| starring       = Rajesh Khanna Raj Babbar Kumar Gaurav Farha Naaz Deepti Naval
| music          = Rahul Dev Burman
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = }}
 1996 Bollywood|Hindi-language Indian feature directed by B. R. Ishara, starring Rajesh Khanna and Raj Babbar in lead roles. 
It was released on 21 June 1996.
==Cast==
* Rajesh Khanna...Master Tulsiram 
* Deepti Naval...Saraswati (wife of Master Tulsiram)
* Raj Babbar...Advocate Rajaram 
* Moon Moon Sen...Sheena 
* Kumar Gaurav...Shankar
* Farha Naaz...Bindiya (as Farha)
* Amjad Khan...Thakur Narayandas
* Ranjeet...Rehman
* Raja Bundela...Gopal
* A.K. Hangal...Bindiyas maternal grandfather
* Satyendra Kapoor...Advocate Surendranath
* Deepak Qazir...Harkishan
* Raza Murad...

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Na Na Na Mai Nahi Aati"
| Alka Yagnik
|-
| 2
| "Chahe Puchh Lo Kangana Se"
| Babla Mehta, Asha Bhosle
|-
| 3
| "O Jiyo Jiyo Yaaro"
| Amit Kumar
|-
| 4
| "Sajan Sajan Kahe Gir Gir Jaye"
| Asha Bhosle
|}

==References==

 

== External links ==
*  

 
 
 
 
 