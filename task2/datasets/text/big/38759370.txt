Masala (2013 film)
{{Infobox film
| name            = Masala
| image           = Masala telugu film poster.jpg
| writer          = Anil Ravipudi  
| story           = Rohit Shetty
| screenplay      = K. Vijaya Bhaskar
| director        = K. Vijaya Bhaskar  
| producer       = Daggubati Suresh Babu|D.Suresh Sravanthi Ravi Kishore Venkatesh Ram Ram Anjali Anjali Shazahn Padamsee
| music          = S. Thaman
| cinematography = Andrew
| editing        = M.R. Varma
| studio         = Suresh Productions  Sravanthi Art Movies
| country        = India
| language       = Telugu
| released       =  }}
| runtime        = 140 mins
| budget         =  
| gross          =      
}}
Masala ( ) is a 2013 Telugu film produced by Daggubati Suresh Babu|D.Suresh and Sravanthi Ravi Kishore jointly on Suresh Productions & Sravanthi Art Movies banner, directed by K. Vijaya Bhaskar. Starring Daggubati Venkatesh|Venkatesh, Ram Pothineni|Ram, Anjali (actress born 1986)|Anjali, Shazahn Padamsee in lead roles and music composed by S. Thaman.  It is a remake of the 2012 Hindi film Bol Bachchan   The film released worldwide on November 14, 2013.   Upon release, the film received positive reviews from critics albeit the lead actors and the comedians performances were appreciated. 

==Plot==
Balaram (Venkatesh) is a good hearted strongman of the village of Bheemarajapuram. He treats everyone with fairness and takes care of their needs. But there is one thing that Balaram cannot tolerate and that is lying. If he catches someone lying, Balaram metes out a severe punishment to the offender. Into this scenario comes in Rahman (Ram). Along with his sister Sania (Anjali), Rahman comes to the village in search of a job. With a brave deed, he falls into the good books of Balaram but circumstances force him to change his name to Ram. He soon becomes a trusted aide of Balaram and all is well for a while.

However, Balaram catches Rahman while he is offering Namaz and asks him for an explanation. Out of desperation, Rahman reveals that he has a younger brother named Rahman. (He says the brother’s name is Rahman, since Balaram knows the original guy as Ram). This Rahman character is portrayed as a gay and Balaram tasks him with teaching his sister (Shazahn Padamsee) the art of dance. Rahman struggles to manage the roles of Ram and Rahman and a confusion drama ensues. As can be expected, the truth comes out in the end and Balaram is outraged. Will he forgive Rahman? That forms the story of ‘Masala’   

==Cast== Venkatesh as Balaram Ram as Ram / Rehaman Anjali as Sania / Sarita
* Shazahn Padamsee as Meenakshi
* Jaya Prakash Reddy as Eddulodu
* M. S. Narayana as Musalodu
* Kovai Sarala as Anjali Devi / Chintamani Ali as Suri
* Posani Krishna Murali as Nagaraju

==Soundtrack==
{{Infobox album
| Name = Masala
| Longtype = To Masala
| Type = Soundtrack
| Artist = S. Thaman
| Cover = 
| Released = October 13, 2013
| Recorded = 2013 Feature film soundtrack
| Length = 19:12 Telugu
| Label = Aditya Music
| Producer = S. Thaman
| Reviews =
| Last album = Ramayya Vasthavayya  (2013)
| This album = Masala   (2013)
| Next album = Race Gurram  (2014)
}}
 Krishna Chaitanya.  Music released on ADITYA Music Compamy.  The Music received positive response from both critics and audience alike. 

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 19:12
| all_lyrics      = 
| lyrics_credits  = no
| music_credits   = 
| title1          = Ninu Choodani
| extra1          = Ranjith (singer)|Ranjith, Shreya Ghoshal
| length1         = 04:56
| title2          = Kotlallo Okkaday
| extra2          = Shankar Mahadevan
| length2         = 04:04
| title3          = Meenakshi Meenakshi
| extra3          = Suchith Suresan, M. M. Manasi
| length3         = 03:42
| title4          = Acharey Acharey
| extra4          = Ranjith, Bindu
| length4         = 04:02
| title5          = Masala (Remix)
| extra5          = Rahul Nambiar, Naveen Madhav, Bindu
| length5         = 02:26
}}
 

==Production==

===Development===
The film had its formal pooja ceremony on March 13, 2013 at Ramanaidu Studios in Hyderabad. The event had the crew of the film in full attendance and Ram clapped for the muhurat shot. Since Venkatesh was out of town and he couldnt attend the films launch.  Though Initially it was named "Garam Masala", it was officially renamed as "Gol Maal", which was confirmed by the Producers.  Later on the titles "Ram - Balaram" and "Masala" were considered.   Finally, the title was confirmed as "Masala" after the first look poster was released in September 2013. 

===Casting=== Venkatesh and Ram were Anjali and Shazahn Padamsee were roped to play the siblings of Ram and Venkatesh in the film respectively.  This film marked the second film of Anjali with Venkatesh after Seethamma Vakitlo Sirimalle Chettu, which was also a multistarrer film.  It also marked the second film of Bollywood Actress Shazahn Padamsee after the 2010 film Orange (2010 Telugu film)|Orange.  During the Thai Schedule of the film, Ram learnt Muay Thai, a famous boxing technique in Thailand.  Jaya Prakash Reddy, Ali (actor)|Ali, M. S. Narayana and Kovai Sarala were roped in to play important roles in the film. 

===Filming===
The film’s first schedule was held in Hyderabad and few scenes were shot on Ram and Anjali. Late it was reported in the end of March that After a major schedule in Bangalore, the film will also be shot in Panchgani, near Pune.  Later, Anjali, who couldn’t join the film unit in Bangalore after she went missing, had promised to join the film’s shooting in Panchgani.  During the shoot, Rohit Shetty, the director of Bol Bachchan visited the sets and wished the team for the movies success.  In June, a fight sequence was canned on Venkatesh and Ram at Ramoji Film City in Hyderabad amidst the principal star crews presence.  In July, the complete talkie portion was shot and it was reported that only two songs were left to shot.  Later a song on Ram and Shahzan Padamsee was shot at Thailand.  A song on Venkatesh and Anjali was planned to be shot in Japan from September 3, 2013.  The song was shot at Hokkaido, the second largest island in Japan.  The title song was shot in October in which Venkatesh, Ram, Jayaprakash Reddy, Kovai Sarala and Ali took part in the shoot thus wrapping the films shoot. 

==Release==
AustraliaTelugu won the overseas rights for screening in Australia and New Zealand. The film was awarded a clean U certificate by The Central Board of Film Certification on November 7, 2013.  The film is slated for a worldwide theatrical release on November 14, 2013 to 1000 screens. 

===Reception===
The film received positive reviews on an average. 123telugu.com gave a review stating "Masala has a few good moments. Venkatesh and Ram have tried their best to carry the film with their comedy timing. But a stale plot and outdated screenplay hamper their efforts. Any Masala gets a perfect flavour and taste only when the ingredients are blended in the right proportions. Sadly, that is not the case with this Masala" and rated the film 2.75/5.  TeluguOne.com gave a review stating "The movie is a worth watching for its comedy. cheerfully cheesy jokes made audience glued to their seats. Vijay Bhaskar has inserted funny and buttler English in what ever the areas possible. The movie is a must watch for comedy lovers. Masala is a worth watching for all the comedy lovers" and rated the film 3.25/5.  Times of AP gave a review stating "Overall, an okay film. Nice enjoyable movie. As comedy plays a major role especially for Telugu audience, this film will get pass marks. It will run with good openings and may continue to certain extent until another big movie comes for competition. Masala is a worth watching for all the comedy lovers" and rated the film 3.25/5.  way2movies.com gave a review stating "Masala had several clichés with confusion dramas that were seen in recent comedy releases, yet the screenplay packed with entertainment and silly gags works for the movie. Venkatesh, Ram’s performances, Anil Ravipudi’s dialogues and entertaining scenes are assets for the film while actions scenes, poor VFX works and weak climax are the negative points. Masala is decent enough to entertain comedy film lovers, provided you don’t look for logic."  gulte.com gave a review stating "Masala is a complete commercial formulaic film that sticks to the genre. It is more of a comedy entertainer with a touch of action. In a nutshell, Masala is for comedy movie lovers that dont care about the logics and other nuances of cinema. Content wise it is an average film and can be watched once for Venkatesh and Ram. It has enough to stay afloat at the box office and we have to wait and see if it can swim across to a safe zone or not" and rated the film 3/5 terming it a "Time pass Masala fare!".  Zopcorn gave a qualitative rating to the film "I have free time". 

==References==
 

== External links ==
*  

 

 
 
 
 