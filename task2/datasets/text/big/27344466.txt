Emperor Tomato Ketchup (film)
{{Infobox film
| name           = Emperor Tomato Ketchup
| film name =  
| producer       = Art Theatre Guild
| caption        =
| director       = Shūji Terayama
| writer         = Shūji Terayama
| starring       = Keiko Niitaka Salvador Tari
| music          =
| cinematography =
| editing        =
| distributor    = Daguerreo Press
| released       =   
| runtime        = 27 min (1971) 75 min (1996)
| country        = Japan
| language       = Japanese
| budget         = 
}}

  is a Japanese short experimental film made by Shūji Terayama and released in 1971 at 27 minutes. The original "Directors Cut" version of the film as created in 1970 was re-edited from repaired original prints and released as a 75-minute feature in 1996, 13 years after Terayamas death.  

==Plot==
A young boy is the emperor of a country in which children have overthrown the adults.

==Cast==
* Keiko Niitaka
* Salvador Tari
* Tarō Apollo
* Mitsufumi Hashimoto

==References==
 

==External links==
*  
*  

 

 
 
 
 

 