Battle Taxi
{{Infobox film
| name           = Battle Taxi
| image          = Battle Taxi poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Herbert L. Strock
| producer       = Art Arthur Ivan Tors
| screenplay     = Malvin Wald
| story          = Art Arthur Malvin Wald
| starring       = Sterling Hayden Arthur Franz Marshall Thompson Leo Needham Jay Barney
| music          = Harry Sukman
| cinematography = Lothrop B. Worth 	
| editing        = Jodie Copelan 
| studio         = Ivan Tors Productions
| distributor    = United Artists
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Battle Taxi is a 1955 American drama film directed by Herbert L. Strock and written by Malvin Wald. The film stars Sterling Hayden, Arthur Franz, Marshall Thompson, Leo Needham and Jay Barney. The film was released on January 26, 1955, by United Artists.  

==Plot==

In the Korean war, the commander of an Air Rescue helicopter team must show a hot-shot former jet pilot how important helicopter rescue work is and turn him into a team player.

== Cast ==
*Sterling Hayden as	Capt. Russ Edwards
*Arthur Franz as Lt. Pete Stacy
*Marshall Thompson as 2nd Lt. Tim Vernon
*Leo Needham as Sgt. Slats Klein
*Jay Barney as Lt. Col. Stoneham
*John Dennis as MSgt. Joe Murdock Michael Colgan as Medic Capt. Larsen
*Andy Andrews as Lazy Joker Two
*Dale Hutchinson as Blue Boy Three-Gene 

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 