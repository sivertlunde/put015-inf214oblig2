Lotte (film)
{{Infobox film
| name           = Lotte
| image          = 
| image_size     = 
| caption        = 
| director       = Carl Froelich 
| producer       = Carl Froelich   Henny Porten   Wilhelm von Kaufmann
| writer         = Fred Hildenbrand   Walter Supper
| narrator       = 
| starring       = Sig Arno   Kurt Gerron   Ernst Karchow   Elsa Wagner
| music          = 
| editing        =
| cinematography = Gustave Preiss
| studio         = Carl Froelich-Film   Henny Porten Film
| distributor    = Parufamet 
| released       = 18 April 1928
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent film directed by Carl Froelich and starring Henny Porten, Walter Jankuhn and Hermann Vallentin. Lotte, a young woman from an aristocratic background, masquerades as a poor person.  Art direction was by Franz Schroedter.

==Cast==
* Henny Porten as Lotte - Tochter Süßkinds 
* Walter Jankuhn as Harald von Lindenberg 
* Hermann Vallentin as Burgkastellan Süßkind 
* Elsa Wagner as Anita Negrelli - Hofschauspielerin a.D. 
* Lotte Werkmeister as Amalie Süßkind - seine zweite Frau 
* Alexandra Schmitt as Frau Wehmut - Hebamme a.D. 
* Ralph Arthur Roberts as Möricke - Schlosser a.D. 
* Paul Passarge   
* Adele Sandrock

==References==
 

==Bibliography==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
* 

 

 
 
 
 
 


 