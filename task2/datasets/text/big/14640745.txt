Of Cash and Hash
{{Infobox Film |
  | name           = Of Cash and Hash
  | image          = OfCashandHashTITLE.jpg
  | caption        = 
  | director       = Jules White  Jack White Kenneth MacDonald Frank Lackteen Duke York Stanley Blystone Cy Schindell 
  | cinematography = Ray Cory 
  | editing        = Robert B. Hoover
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 15 46"
  | country        = United States
  | language       = English
}}
Of Cash and Hash is the 160th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are mistaken for three armored car thieves after getting caught in crossfire. Captain Mullin (Vernon Dent) gives the boys a lie detector test, but finds no reason to hold them. He releases them, and they return to work at their restaurant, the Elite Café. Friend Gladys Harmon (Christine McIntyre) stops by the restaurant just as one of the real armored car bandits flees the scene. They then chase the bandit to a spooky old mansion where they and their hideous hatchet man, Angel (Duke York), are hiding. When the mob abducts Gladys, the Stooges come to her rescue and retrieve the stolen money.
  keeps the Stooges in line in Of Cash and Hash]]

==Production notes==
Of Cash and Hash is a remake of 1948s Shivering Sherlocks, using ample recycled footage from the original.    New footage was filmed on April 26, 1954.   

This was the final film featuring new footage of long-time Stooge character actress Christine McIntyre.  She  would appear in six additional Stooge films via stock footage. It also was the final Three Stooge film of long time stooge role player Stanley Blystone.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 

 