Musical Chairs (film)
 
{{Infobox film 
| name = Musical Chairs
| image = 
| caption =
| director = Susan Seidelman
| producer = Janet Carrus and Joey Dedio
| writer = Marty Madden
| starring = Leah Pipes E. J. Bonilla Priscilla Lopez Laverne Cox Auti Angel 
| music = 
| cinematography = 
| editing = 
| distributor = 
| released =  
| runtime = 
| country = United States
| language = English
| budget = 
}} platform release. It premiered on cable TV (HBO) in December 2013.

==Plot==
The story takes place entirely in New York. Armando (E. J. Bonilla) works part-time at his parents restaurant and is also a custodian at a dance studio, where he secretly practices dance moves. He befriends the beautiful Mia Franklin (Leah Pipes), a dancer who is having a relationship with the studios owner Daniel (Philip Willingham). She catches Armando dancing alone, likes what she sees, gives him a few tips, and they dance together briefly, but are discovered by Daniel. Trying to avoid an awkward situation, Mia leaves. When Armando realizes that Mia has left her scarf behind, he calls out to her from the studios second floor window.

On the sidewalk below, Mia turns to cross the street, when she struck by a taxi and rendered a paraplegic, paralyzed from the waist down. Upon learning of this, Daniel jilts her. Armando tries to boost her confidence and persuades her and other disabled people in the local rehabilitation center, including a "punky" Latina, Nikki (Auti Angel) and a wounded Iraq-war veteran, Kenny, (Morgan Spector) to enter a wheelchair ballroom dancing competition. Despite the opposition of his mother (Priscilla Lopez), Armando and Mia gradually fall in love and enter into a relationship, while Armandos uncle Wilfredo (Nelson Landrieu) falls in love with Chantelle (Laverne Cox), a disabled trans woman at the rehab center. Before the competition, Armandos mother, who has been maneuvering to get Armando hooked up with the beautiful Rosa (Angelic Zambrana), does her best to undermine (even to the point of "casting spells") the relationship between him and Mia, which has become sexually intimate by now. But Rosa understands, and generously breaks off with Armando.

On the night of the competition, Mia panics when she sees Daniel in the audience watching her, and instead of dancing, rushes to her dressing room and begins to pack her things. Armandos mother sees what is happening, rushes to Mias dressing room, admits that she was wrong, tells Mia that Armando loves her and encourages her to reunite with Armando and win the competition.

Mia goes to Armando, tells him she loves him, and the two return to the competition and begin to dance. But just as the dance is at its height, Mia falls out of her wheelchair. As the stunned audience watches, Armando triumphantly picks her up and the two complete their dance with Armando holding Mia in his arms. The film ends without our knowing if they have won the competition.

==Media coverage and reception==
 Hunger Games film, which came to theatres on the same day as Musical Chairs and trounced every other film which opened that week. Musical Chairs did not do well financially, partly perhaps it played at only twelve theatres, but also perhaps because the buzz over The Hunger Games was so strong.

However, in an interview on Deadline Hollywood, director Susan Seidelman said that she hoped the film would gain attention through "word of mouth", and that showings of the film were targeted at specific target groups (Latinos in its Florida showings, for instance; several Latino actors appear in the film).  There was little if any national television coverage of it, and only a few major publications reviewed it, among them the New York Times, the New York Daily News, the Village Voice, and the Chicago Tribune. Neither Entertainment Weekly nor Rolling Stone magazine reviewed the film. It was left to online reviewers such as Harvey Karten to review it, though James Berardinelli did not. Roger Ebert also reviewed it. 

So far, 170 people have voted on the film at the Internet Movie Database as of March 2013, and its score there is currently low (the highest score possible on the Internet Movie Database is a 10). However, it has received considerably higher scores from the general public on sites such as Rotten Tomatoes, receiving an 85% favorable audience review.

There has been some controversy over the fact that some of the disabled characters in the film  were played by able-bodied actors. Mia, the leading female character, starts out as able-bodied, so an actress who is not disabled in real life had to be found for the role. However, whenever possible the producers and director hired disabled performers and dancers in key acting roles. This includes Auti Angel, professional wheelchair dancer and reality TV star of Push Girls.

==Awards== Fred & The Artist.

On January 16, 2013, it was announced that Musical Chairs had been nominated for a GLAAD Award for Best Film in Limited Release. 

On August 13, 2013  the film received seven award nominations at the Massachusetts Independent Film Festival, including Best Film, Best Actress (Leah Pipes), Best Supporting Actress (Laverne Cox), and Best Director (Susan Seidelman) On August 19, 2013, it was announced that the film won six of those awards.

==Budget and gross== Westport Cinema Initiative,  and at Access Chicago on July 19, 2012. A scheduled August 22, 2012 showing at the Chatham Orpheum Theatre in Cape Cod sold out in a matter of hours. By popular demand it was brought back to Chatham, Massachusetts on September 15, 2012. It opened in Europe on October 3, 2012, where it was shown at the Hamburg International Film Festival in Germany. On December 10, 2012, it was shown in Havana, Cuba. On Valentines Day 2013, it played at the Reel Abilities Film Festival in Fairfax, Virginia,  and on March 22, 2013 it was shown in Hawaii for the first time. It was also shown on the island of Saint Martin on March 12, 2013 as part of Disability Awareness Week.

==Online==
On December 17, 2012, the film became available for streaming free online at its website for a two-week period only. The magazine The Advocate proclaimed the event as one of the Top 10 Highlights of the year.

==Cable television==
On November 15, 2013, Musical Chairs was shown uncut and without commercials, on cable television for the first time, by HBO Latino. It also finally became available on HBO video on demand. On HBO Latino, the film was not dubbed; instead, Spanish subtitles were provided for viewers who could not speak the language. Publicity for the showing was scant and limited only to the films Facebook page. It was eventually shown on other HBO channels.

==References==
 

== External links ==
* 

 

 
 
 
 
 
 