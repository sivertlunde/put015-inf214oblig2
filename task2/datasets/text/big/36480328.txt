Woodland Café
{{Infobox Hollywood cartoon
| cartoon name      =Woodland Café
| series            = Silly Symphonies
| image             = 
| image size        = 
| alt               = 
| caption           = 
| director          = Wilfred Jackson
| producer          = 
| story artist      = 
| narrator          = 
| voice actor       = 
| musician          = 
| animator          = 
| layout artist     = 
| background artist =  Walt Disney Productions
| distributor       = United Artists
| release date      = 1937
| color process     = Technicolor
| runtime           = 
| country           = United States
| language          = English
| preceded by       = 
| followed by       = 
}}
 animated Disney short film. It was filmed in Technicolor and released in 1937. While it contained no on-screen credits, Wilfred Jackson was the director and Leigh Harline was the musical director.

==Plot==
The setting is a nightclub staffed and frequented entirely by insects. The cartoon is broken into three sections. In the first, the patrons are shown arriving, getting to their tables and being served; the lighting is provided by fireflies, and a card game is dealt by a centipede. The second features a performance of a French Apache dance as a good-girl fly resists the advances of a bad-boy spider (eventually getting the better of him). Finally, everyone jams the dance floor, even the snails, as the orchestra plays (and sings) "Everybodys Truckin" on their instruments made of flowers.

==External links==
*  
*  

 

 
 
 
 
 
 
 


 