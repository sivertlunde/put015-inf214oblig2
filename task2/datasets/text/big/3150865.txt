Yankee Doodle Daffy
 
{{Infobox Hollywood cartoon
|cartoon_name=Yankee Doodle Daffy
|series=Looney Tunes (Daffy Duck, Porky Pig)
|image=Yankee Doodle Daffy title card.png 
|caption=Yankee Doodle Daffy Title Card
|director=Friz Freleng
|story_artist=Tedd Pierce
|animator=Richard Bickenbach Gerry Chiniquy Manuel Perez Phil Monroe
|layout_artist=Owen Fitzgerald Paul Julian
|voice_actor=Mel Blanc Billy Bletcher (uncredited) Carl W. Stalling
|producer=Leon Schlesinger
|studio=Leon Schlesinger Productions
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|release_date=July 3, 1943 (USA)
|color_process=Technicolor
|runtime=6:43 (one reel)
|movie_language=English
}}
  in Yankee Doodle Daffy, 1943]]
Yankee Doodle Daffy is a Warner Bros. Looney Tunes theatrical cartoon short released in 1943, directed by Friz Freleng and written by Tedd Pierce. The short was the second Technicolor Looney Tunes entry to feature Porky Pig and Daffy Duck (after My Favorite Duck).

The title and introductory music are inspired by the 1942 film Yankee Doodle Dandy, a major hit and a Warner release. Other than the fact of both films being about show business, they have no plot elements in common. This is the first cartoon in the collection that came from public domain.

==Plot==
Porky Pig, a producer, loaded down with luggage and a golf bag, leaves his office in a hurry to board an airplane. Daffy Duck, a talent agent, prevents him from leaving and attempts to secure an audition for his client, a lethargic child performer named Sleepy Lagoon|"Sleepy" Lagoon. The pitch, intended to demonstrate Sleepys allegedly wide and varied repertoire, consists of Daffy himself performing an array of musical and stage acts. Sleepy meanwhile stays seated, nonchalantly licking an enormous lollipop and silently commenting on Daffys ludicrous behavior using signs bearing rebuses.

Porky, with mounting frustration, repeatedly tries to escape from the pitch. Daffy handily foils each attempt in increasingly improbable ways, including by turning out to be the pilot of Porkys plane and then turning out to be the parachute Porky uses to escape said plane. Admitting defeat, Porky allows Sleepy to audition.

Sleepy calmly leaves his seat and begins to sing in a strong, operatic baritone that is not only surprising given his small stature but also substantially more dramatic than any of the acts Daffy used in the pitch. However, during a high note near the end, he erupts into a long coughing fit before weakly croaking the rest of the line.

==Analysis==
Michael S. Shull and David E. Wilt consider it ambiguous if this cartoon contain a World War II-related reference. When Daffy is revealed as the pilot of the plane, he is wearing an aviators goggles and helmet. In this guise, Daffy sings "We watch the skyways oer the land and the sea, ready to fly anywhere the duty calls, ready to fight to be free". This could be a reference to military aviation. Shull, Wilt (2004), p. 216 

==Sources==
*  

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 