Gratefulness
 
{{Infobox film
| name           = Gratefulness
| image          = Npvmov001f.jpg
| caption        = Khmer Theatrical poster
| director       = Heng Tola
| producer       = 
| writer         = Rith San
| narrator       = 
| starring       = Ly Chan Siha
| music          = 
| cinematography = 
| editing        = 	 	
| distributor    = Campro Production
| released       = 2003
| runtime        = 120 minutes
| country        = Cambodia
| language       = Khmer
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Gratefulness also Katanho is a  , Thai 1999s horror film. This film was released at the Kirirom cinema in Phnom Penh with English subtitles. 

== Tag lines ==
She ran to school because she wanted to study, She ran back home because of her gratefulness

== Plot ==
This movie was based on a true story. Lyka was born in a poor family where she was living with her parent and a blind grandma but one day, her father was killed in a car accident when he on the way back home from his work in the Battam bong province. After his death, the mother worked every day and night to feed the family until one day, she was sick. So everything fell to Lyka; she took care of her sick mother and blind grand mother. She ran many kilometers to school and went home after the break and she tried to find another job to buy the medicines and food for her mom and grandmother.

One day, Lyka wrote an Essay which telling a misery life and gratefulness of herself to the class as well as the writing made every classmate including the teacher knew the true life of Lyka. Her essay about her life was published in a newspaper and generous donations flooded in both from inside Cambodia and from foreign countries to Lykas family which fed her family and paid the hospital bills for Lykas mother. Lykas mother life was like the sunset as Lyka was too late to cure her mother from the illness.

At the end, we see Lyka was sitting on railway for a moment and then walking along it when thinking of her life at tomorrow while the sunset.

== Awards ==
the film received many awards in Khmer film festival:
* Best Actress
* Best Shooting
* Best Acting
* Best Script
* Best Direction
* Third prize for bronze award of Best Movie

== Success of the actress ==
After her role in Gratefulness, Ly Chan Siha become a famous actress.  She had roles in several other films including Moronak Meada and won the award for best actress.

== References ==
 

==External links==
*  
*  
*  

 
 
 