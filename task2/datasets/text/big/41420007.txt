Sing, Baby, Sing
{{Infobox film
| name           = Sing, Baby, Sing
| image          =
| caption        =
| director       = Sidney Lanfield
| producer       = Darryl F. Zanuck
| writer         = Milton Sperling Harry Tugend Jack Yellen Michael Whalen Tony Martin
| music          = Richard A. Whiting Walter Bullock Cyril J. Mockridge
| cinematography = J. Peverell Marley
| editing        = Barbara McLean
| distributor    = Twentieth Century-Fox Film Corporation
| released       = August 21, 1936
| runtime        = 90 min
| country        = United States Best Original Song English
| budget         =
}}
 Best Original Song at the 9th Academy Awards for their song "When Did You Leave Heaven".

==Plot==
After Joan Warren (Alice Faye) is fired from her singing job at the Ritz Club, where she performs with the Ritz Brothers, she seeks help from theatrical agent, Nicky Alexander (Gregory Ratoff). Nicky, however, is in the process of being evicted from his office suite, so he tells her to find another agent. When she insists that he represent her, he takes her to Mr. Brewster (Paul Stanton), president of the Federal Broadcasting Company, and Joan auditions, but Brewster refuses to hire her because she is not of the upper class. 
 Michael Whalen) and Joe (Cully Richards), a photographer, climb onto the fire escape and photograph them. 

Later, Al accompanies Farraday home, and the puzzled Farraday wonders why he cant remember hiring a personal physician. The newspapers print the story, and Brewster decides that he wants to hire Joan on the condition that Farraday will perform as well. At their new home at the Madison Towers, the group learns that Farraday is about to return to Hollywood at the behest of his cousin and business manager, Robert Wilson, who is furious over the publicity. Nicky goes to Farraday and suggests that he show his cousin that he has a head for business by getting the lucrative radio contract. Robert arrives, tells the newspapers that Joan is a gold digger and escorts Farraday onto the train leaving for California. As a result of the new story, Brewster no longer wants to sign Joan. Ted explains that to be the first to print a retraction of Roberts statement, his newspaper will fly Joan out to Farraday. They fly to Kansas City to meet Farradays train and trick Robert into leaving without Farraday. Then, they arrange with Brewster to broadcast that evening from Kansas City. They round up some performers for the show, including the Ritz Brothers, who happen to be in town. As Farraday prepares to go on the air, Robert returns and locks himself in the hotel room with Farraday, but Farraday escapes. He arrives at the station in the nick of time and exonerates Joan, securing for her the radio contract with Brewster.

== Cast ==
*Alice Faye as Joan Warren
*Adolphe Menjou as Bruce Farraday
*Gregory Ratoff as Nicholas K. Alexander
*Ted Healy as Al Craven
*Patsy Kelly as Fitz Craven Michael Whalen as Ted Blake
*The Ritz Brothers as Themselves
*Montagu Love as Robert Wilson
*Dixie Dunbar as Telephone operator
*Douglas Fowley as Mac Paul Stanton as Brewster Tony Martin as Tony Renaldo
*Cully Richards as Joe

== External links ==
*  

 

 
 
 
 
 
 
 
 