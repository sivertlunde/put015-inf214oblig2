Chantrapas
 
{{Infobox film
| name           = Chantrapas
| image          = Chantrapas2010Poster.jpg
| caption        = French film poster
| director       = Otar Iosseliani
| producer       = 
| writer         = Otar Iosseliani
| starring       = Dato Tarielachvili
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 122 minutes
| country        = Georgia
| language       = Georgian French Russian
| budget         = 
}}
 Best Foreign Language Film at the 84th Academy Awards,   Also verified with Georgian National Film Centre     but it did not make the final shortlist.   

==Cast==
* Dato Tarielachvili as Nicolas (as Dato Tarielashvili)
* Tamuna Karumidze as Barbara
* Fanny Gonin as Fanny
* Givi Sarchimelidze as Le grand-père
* Pierre Étaix as Le producteur français
* Bulle Ogier as Catherine
* Bogdan Stupka as Lambassadeur
* Lasha Shevardnadze as Le mari de Barbara
* Nino Tchkheidze as La grand-mère

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Georgian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 