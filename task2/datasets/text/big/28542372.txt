Magyar vándor
{{Infobox film
| name           = Hungarian Strayer
| image          = 
| image_size     = 
| caption        = 
| director       = Gabor Herendi
| producer       = 
| writer         = Gabor Harmat
| narrator       = 
| starring       = Karoly Gesztesi  Janos Gyuriska  Gyula Bodrogi
| music          = Robert Hrutka
| distributor    = Budapest Film
| released       = 5 February 2004
| runtime        = 110 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Hungarian action action comedy film directed by Gabor Herendi and starring Karoly Gesztesi, Janos Gyuriska and Gyula Bodrogi.

==The main story==
The seven leaders of the Hungarians wake up after a very hard party in Etelköz. Then they realise that their beloved Hungarians are gone without them to conquest... In this exciting movie the leaders have to find the new home, and their people as well. During their migration they live through the history of Hungary, both the comic and tragicomic episodes, but instead of forests and castles, they occupy inns and hotels, and they are fighting with harem girls instead of Mongols or Tatars. And the big battle is not with weapons and guns, but on a soccer field, with a football and two teams...

==Main cast==
* Károly Gesztesi ... Álmos
* János Gyuriska ... Előd
* János Greifenstein ... Ond
* Zoltán Seress ... Kond
* Győző Szabó ... Tas
* Tibor Szervét ... Huba
* István Hajdú ... Töhötöm
* Gyula Bodrogi ... General Wienerschnitzz
* János Gálvölgyi ... Turkish Pasha
* László Fekete ... Toldi Miklos
* András Hajós ... Singer

==External links==
* 
*  

 
 
 
 
 
 


 