Exiled
 
 
 
{{Infobox film name = Exiled image = Exiledmovieposter.jpg caption = American promotional poster traditional = 放‧逐 simplified = 放‧逐}} director = Johnnie To producer = Johnnie To writer = Milkyway Creative Team starring = Anthony Wong Francis Ng Simon Yam Nick Cheung Roy Cheung Lam Suet music = Dave Klotz Guy Zerafa cinematography = Cheng Siu-Keung editing = David M. Richardson studio = Media Asia Films Milkyway Image distributor = Media Asia Distribution released =   runtime = 100 minutes country = Hong Kong language = Cantonese Mandarin budget = 
}} Anthony Wong, Francis Ng, Simon Yam, Nick Cheung and Roy Cheung. The action takes place in contemporary Macau. The film made its premiere at the 63rd Venice International Film Festival, and was in competition for the Golden Lion.

==Plot==
In 1998 Macau, former mobster Wo (Nick Cheung) lives quietly with his wife, Jin (Josie Ho), and his newborn child in a nondescript apartment, having turned over a new leaf. But vengeful mob boss Fay (Simon Yam)—whom Wo once tried to assassinate—has dispatched a pair of ageing hitmen to cut that peaceful existence short. Once arrived, killers Blaze (Anthony Wong) and Fat (Lam Suet) find a second pair of hitmen, Tai (Francis Ng) and Cat (Roy Cheung), who are determined to protect Wo. After a brief showdown, the whole group comes to an uneasy truce, lay their weapons down and bond over dinner— after all, these men grew up together in the same gang. Reunited and hungry for another score, they visit a fixer called Jeff . Jeff gives the gang the job of killing a rival boss, Boss Keung, as well as telling them about the location of a large quantity of gold being transported for a corrupt official. Wo makes the gang promise that if anything happens to him, his wife and son will be looked after.

Later that night the friends find Boss Keung in a restaurant; however Boss Fay wanting to take over the other bosss territory, interrupts the meeting. Boss Fay recognising Blaze sitting in the restaurant, openly chastises and humiliates him for not killing Wo, culminating in Fay shooting Blaze. However unbeknownst to Fay, Blaze is wearing a bulletproof vest and survives. Wo seeing this opens fire before Fay can finish Blaze off. A gunfight erupts in the restaurant with Fay being shot in the leg and Keung in the arm. The two bosses come to an agreement to share territory and profits, further agreeing to kill the gang of friends. Having narrowly escaped the restaurant shootout the friends decide to take a severely shot Wo to an underground clinic for medical assistance. After negotiating a price, the doctor operates removing the bullets from Wo. However as he is sewing up Wos wound, there is a loud banging at the door. Having heard this the remainder of the waiting friends hide in the doctors flat. The door is answered and both Fay and Keung burst in seeking help for their injuries sustained in the restaurant shootout. Fay pushes a still unconscious Wo out of the way and orders the doctor to tend to his wound first. Meanwhile Keung takes a look around the flat and comes across a hiding Fat. Realizing that they have been found the gang begin to dispatch the bosses henchmen. Meanwhile Wo wakes up and slowly gets to his feet to escape before collapsing. The rest of the friends not knowing where Wo has got to, make an exit down the back of the apartment. However whilst escaping across the back courtyard, Boss Fay throws Wo from a high window and pins down Wos friends preventing any rescue attempt. The gang desperately try to retrieve their critically injured but still alive friend but Fay still shoots at them and even manages to shoot Wo. Quick thinking Fat seeing that his friend has come to rest on some tarpaulin pulls Wo to safety and the gang escape. Now in the car Wo knowing he is near death, asks to be taken back to his wife and son. Wo dies shortly after.

Handing Wos body over to his wife, Jin, she demands to know what has happened and in her grief opens fire on Blaze and Tai who run away. Jin contemplates killing herself and her son but thinks better of it. She instead smashes up the furniture in the house and makes a funeral pyre for Wo. She then sets fire to Wo and the flat and leaves with her son. The reduced gang leave the city in search of the gold. After coming across the heavily guarded convoy carrying the gold, they flip a coin to decide whether to hijack it or not. The coin comes up tails meaning they will not proceed with the robbery. After carrying on down the road however the come across the convoy being ambushed by another gang. They witness all the police officers bar one crack-shot being killed. The friends decide to help the officer by dispatching the rest of the gang. The friends appreciating the policemans sharp shooting decide to split the gold with him and drive off to a hidden dock to transport the gold to the mainland and a new life. Meanwhile back in the city, Jin still furious about the death of her husband goes looking for the friends, asking many people until she is recognised by the fixer Jeff who in turn contacts his boss, Boss Fay.

Fay with a captured Jin calls a gloating Blaze, who is then informed of the situation. He is told to meet Fay at midnight otherwise Jin and her son will be killed. Determined to protect Jin after Wos death, the friends agree and leave the officer at the dock with the gold telling him they will return by dawn. Once at the meeting place the four friends are confronted by Jin, whom Fay allows to shoot Blaze in revenge. However Blaze is again hit in the chest, surviving due to his bullet proof vest. Tai steps in, throwing a bag of gold at Fays feet telling him that he can have it all if Fay lets them all go. Fay agrees, but tells them Blaze must stay to face the consequences of not following orders. Blaze agrees to this deal and the remainder of the friends leave with Jin. However as they leave, Tai informs Jin of the boat and the policeman and tells her to drive there. With Jin safe the greatly outnumbered friends open fire. In the resulting gunfight all are killed including Boss Fay and Boss Keung. As the friends lie dying they all smile knowing they have kept their promise to Wo.

==Cast==
* Anthony Wong Chau-sang – Blaze
* Francis Ng – Tai
* Simon Yam – Boss Fay
* Nick Cheung – Wo
* Roy Cheung – Cat
* Lam Suet – Fat
* Josie Ho – Jin, Wos wife
* Richie Ren – Sergeant Chen
* Ellen Chan – Hooker
* Cheung Siu-Fai – Jeff
* Benz Hui – Sergeant Shan
* Gordon Lam – Boss Keung
* Tam Ping-Man – Uncle Fortune
* Wong Chi-Wai – Darkie

==Reception==

===Festivals===
Exiled was shown in Competition at the 2006 Venice Film Festival. It was also shown at the 2006 Toronto International Film Festival, the 2007 Bangkok International Film Festival, 2007 8th Jeonju International Film Festival, 2008 Midnight Sun Film Festival and the 2007 Seattle International Film Festival.

===Category III Rating=== Category III triads agreement handshake. The scene is present on the Mega-Star uncut Limited Edition DVD. However, only the Category IIB cut version was released in Hong Kong theatrically.

===Box office===
Exiled was released in Hong Kong on 19 October. The film, on opening weekend, grossed a total of HK$47,533. The films total HK box-office take was $687,434.
 MPAA "for strong violence and some sexual content." Following its limited released in the United States, Exiled grossed US$20,351 on opening weekend. The films total gross was $49,413.

===Distribution===
Magnolia Pictures acquired the North American rights of Exiled and the film was given a limited release as of 31 August 2007. The North American DVD version of the film was released on 11 January 2008.

===Critical reception=== Newsweek Magazine even awarded the film at number 4 as one of 2007s "Best Action Movie Scenes" 
 

Some critics, however, were less enthusiastic by the film. Film critic Roger Ebert of the Chicago Sun-Times, awarded the film a 2.5 star rating, writing: 

 

==Accolades==
{| class="wikitable" style="font-size:90%"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards
|- style="text-align:center;"
! style="background:#ccc;"| Award
! style="background:#ccc;"| Category
! style="background:#ccc;"| Recipient(s)
! style="background:#ccc;"| Outcome
|- Hong Kong Film Awards
|- Best Director Johnnie To
| 
|- Best Film Editing David M. Richardson
| 
|- Best Cinematography Cheng Siu-Keung
| 
|- Best Picture Johnnie To
| 
|- Venice Film Festival
|- Golden Lion Johnnie To
| 
|- Golden Bauhinia Awards
|- Best Director Johnnie To
| 
|- Best Picture Johnnie To
| 
|- Golden Horse Film Festival
|- Best Action Choreography Chun Pong Ling, Chi Wai Wong
| 
|- Hong Kong Film Critics Society Awards
|- Best Director Johnnie To
| 
|- Catalonian International Film Festival Best Director Johnnie To
| 
|}

===Remake=== Media Asia Group has announced a remake of Exiled. The company brokered a deal with film producer Samuel Hadida, who is attached as a producer. 
 Bachelor Party released on 15 June 2012 is an uncredited remake of the film Exiled.  

==See also==
* List of Hong Kong films
* List of films set in Macau

==References==
 

==External links==
*  
*  
*  

 
 

 
   
 
   
 
 
   
 
 
 
 
 
 