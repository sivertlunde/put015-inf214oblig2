If Looks Could Kill (film)
{{multiple issues|
 
 
}}

{{Infobox film
  | name = If Looks Could Kill
  | image = If Looks Could Kill movie poster.jpg
  | caption =
  | director = William Dear
  | story = Fred Dekker
  | screenplay=  Darren Star
  | producer = Neil Meron Craig Zadan
  | starring = Richard Grieco Linda Hunt Roger Rees Gabrielle Anwar Roger Daltrey
  | music = David Foster
  | cinematography = Douglas Milsome
  | editing = John F. Link
  | distributor = Warner Bros.
  | released =  
  | awards = 
  | runtime = 88 min.
  | country = United States
  | language = English
  | budget = $12 million
  | gross = $7,788,597
  }}
 action comedy spy film directed by William Dear and stars Richard Grieco.

==Plot==
Eighteen-year-old Michael Corben ( ), and the French Club are headed to France for summer school, and Michael must accompany them and participate if he wants to graduate next summer.
 CIA agent common currency. first class British Intelligence.

The late Agent Corbens mission had been to protect Augustus Steranko, who (being not suspected to be evil) has been murdering European finance ministers as part of his plan. After some efforts to explain that he isnt the Corben they think he is, Michael agrees to play along once it becomes apparent that he will be allowed to utilize high-tech gadgets, including X-ray glasses, exploding chewing gum and L.A. Gear sneakers with suction cups, as well as a Lotus Esprit. At first, he enjoys the thrilling adventures the life of a spy provides, but begins to rethink his decision once his life begins being endangered by Sterankos deadly assassins, including Zigesfeld (Tom Rack), a henchman with a prosthetic gold hand, and Areola Canasta (Carole Davis), who kills her victims using her poisonous pet scorpion.

Steranko however captures Michaels teacher and classmates and holds them all hostage at his remote castle stronghold, and now Michael teams up with a girl his own age named Mariska (Gabrielle Anwar), the daughter of Agent Blade (Roger Daltrey) whom Steranko and his gang murdered, to bring the villains down and save his friends as well as all of Europes gold. Despite being briefly captured and imprisoned by Sterankos men, Michael escapes, rescues Mrs. Grober and his friends, and battles and defeats Zigesfeld in the films climax.
 Eurocopter Ecureuil helicopter. Michael manages to rescue Mariska, and Steranko is subsequently killed when he falls out of the helicopter, and it and the onboard gold supply both drop on him. Afterwards, Mrs. Grober agrees to give Michael the French credit.

==Cast==
*Richard Grieco as Michael Corben
*Linda Hunt as Ilsa Grunt
*Roger Rees as Augustus Steranko, a leader of European Economic Community.
*Robin Bartlett as Patricia Grober
*Gabrielle Anwar as Mariska Blade
*Geraldine James as Vendetta Galante
*Michael Siberry as Derek Richardson
*Tom Rack as Zigesfeld
*Carole Davis as Areola Canasta
*Frederick Coffin as Lieutenant Colonel Larabee
*Roger Daltrey as Blade
*Oliver Dear as Kent
*Cynthia Preston as Melissa Tyler
*Michael Sinelnikoff as Haywood
*Travis Swords as Kelly
*Gerry Mendicino as Herb Corben
*Fiona Reid as Marge Corben
*Michael Vinokur as Brad Corben
*David McIlwraith as Agent Michael Corben
*Gene Mack as Agent Kramer
*Jacques Tourangeau as Jacques Lefevre

==Soundtrack==
{{Infobox album  
| Name        = If Looks Could Kill
| Type        = Soundtrack
| Artist      = various artists
| Cover       = 
| Released    = March 15, 1991
| Recorded    = 1990
| Genre       = 
| Length      = 
| Label       = 
| Producer    = 
| Reviews     = 
}}

#"If Looks Could Kill (No Turning Back)" - Glenn Medeiros
#"One Hot Country" - The Outfield Contraband
#"One Mo Time" - Trixter
#"Better the Devil You Know" - Kylie Minogue
#"Teach Me How to Dream" - Robin McAuley
#"All Is Fair" - The Fixx The Stabilizers
#"My Saltine" - Bang Tango Bill Ross

==Box office and reception== worldwide box office dissappointment, having earned only $7,788,597. 

The film had received dissappointing to negative reviews from movie critics, although some enjoyed it, such as Roger Ebert of The Chicago Sun-Times, who gave the film 3 out of 4 stars.    The film had scored a 33% on rotten tomatoes.

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 