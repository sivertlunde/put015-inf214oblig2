Autumn Leaves (film)
{{Infobox film
| name           = Autumn Leaves
| image          = Autumn_Leaves_One_Sheet.jpg
| image size     = poster
| director       = Robert Aldrich
| producer       = William Goetz
| writer         = Jean Rouverol Hugo Butler Lewis Meltzer Robert Blees
| starring       = Joan Crawford Cliff Robertson Vera Miles
| music          = Hans J. Salter
| cinematography = Charles Lang
| editing        = Michael Luciano
| distributor    = Columbia Pictures
| released       =  
| time           = 107 minutes
| country        = United States
| language       = English
| budhet         = $765,000 Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 244 
| gross          = $1.1 million (US)  11,216 admissions (France)   at Box Office Story 
}}

Autumn Leaves is a 1956 Columbia Pictures drama film starring Joan Crawford and Cliff Robertson in an older woman/younger man tale of mental illness. The screenplay was written by Jean Rouverol and Hugo Butler, though it was credited to Jack Jevne, Rouverol and Butler being blacklisted at the time of the films release.
 Berlin International Film Festival.

==Plot== Army veteran named Burt Hanson (Cliff Robertson). They share a romantic date at the beach, kissing amidst the crashing waves, but Millie tells Burt to date someone his own age.  A month later, Burt is waiting for the still-lonely Millie at her home and the two celebrate his new job at a department store. He proposes to her in a movie theater, and while she initially rejects the proposal, she reconsiders when she sees him walking away.

The next day, the couple gets married in Mexico.  However, on the marriage license, he lists his place of birth as Chicago, though he had earlier told her he was born in Wisconsin.  Once home, Burts Divorce|ex-wife, Virginia (Vera Miles), appears, which shocks Millie because Burt told her that he had never been married. Virginia gives her a property settlement that she wants Burt to sign and tells her that Burt is a habitual liar about his life and his past. Millie also learns that Burts father (Lorne Greene) is in Los Angeles to find him.

Burt is haunted by the day when he discovered his wife and father making love; he begins displaying signs of mental instability with their sudden, unwelcomed presence in his life. When he becomes violent, Millie sends him to a mental hospital. Burts condition improves with treatment, and he severs connections with his past. Millie happily discovers he still loves her and they look forward to a brighter future.

==Cast==
* Joan Crawford as Millie
* Cliff Robertson as Burt
* Vera Miles as Virginia
* Lorne Greene as Hanson
* Ruth Donnelly as Liz
* Marjorie Bennett as Waitress
* Frank Gerstle as Ramsey

==Song== Autumn Leaves" as sung by Nat King Cole. Coles rendition is used over the films title sequence.

The songs original title is "Les feuilles mortes" with music by Joseph Kosma and lyrics by Jacques Prévert. English lyrics were written by the American songwriter Johnny Mercer (1949). The song was introduced by Yves Montand in the French feature film Les Portes de la Nuit (1946).

==Reception==
Although Bosley Crowther panned the film in the New York Times of August 2, 1956, (calling it a "dismal tale")  Lawrence Quirk in Motion Picture Herald and William Zinsser in the New York Herald Tribune commented favorably upon the film.   Autumn Leaves was a modest box-office success, chiefly among Crawfords female fans.  The actress thought highly of the film, deeming it the "best older woman/younger man movie ever made," and added, "Everything clicked on Autumn Leaves. The cast was perfect, the script was good, and I think Bob   handled everything well. I really think Cliff did a stupendous job; another actor might have been spitting out his lines and chewing the scenery, but he avoided that trap. I think the movie on a whole was a lot better than some of the romantic movies I did in the past...but somehow it just never became better known. It was eclipsed by the picture I did with Bette Davis." 

The film has grown in stature among Aldrich fans since its 1956 premiere and is now regarded as one of the directors best films. Dan Callahan of Slant Magazine (June 16, 2004) wrote, "All of Aldrichs early work is intriguing, but Autumn Leaves is his secret gem. Its been passed over as camp because of its star, Joan Crawford, but Aldrich brings all his hard edges to this womans picture. The collision of his tough style with the soapy material makes for a film that never loses its queasy tension." 

==Awards== Berlin International Film Festival 1956.   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 