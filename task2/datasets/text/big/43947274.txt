Pappu (film)
{{Infobox film
| name = Pappu
| image =
| caption =
| director = Baby
| producer = Raghu Kumar
| writer = K. Balachander
| screenplay = Baby Seema Sukumari Jagathy Sreekumar
| music = K. J. Joy
| cinematography = K. B. Dayalan
| editing = K. Sankunni
| studio = Dhanya
| distributor = Dhanya
| released =  
| country = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, directed by Baby and produced by Raghu Kumar. The film stars Prathap Pothen, Seema (actress)|Seema, Sukumari and Jagathy Sreekumar in lead roles. The film had musical score by K. J. Joy.  
 

==Cast==
  
*Prathap Pothen  Seema 
*Sukumari 
*Jagathy Sreekumar 
*Prathapachandran 
*MG Soman 
*Kaviyoor Ponnamma 
*Adoor Bhasi 
*Sreelatha Namboothiri  Ravikumar 
 

==Soundtrack==
The music was composed by K. J. Joy.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kurumozhi Koonthalil vidarumo || K. J. Yesudas, S Janaki || Bichu Thirumala || 
|- 
| 2 || Madhumalar thaalamenthum || K. J. Yesudas || Bichu Thirumala || 
|- 
| 3 || Poo Poo Uthaapoo Kaayaampoo || Vani Jairam, Chorus || Bichu Thirumala || 
|- 
| 4 || Pushyaraagam Nrithamadum || P Jayachandran, Vani Jairam || Bichu Thirumala || 
|- 
| 5 || Thathappenne Thanchathil vaa || P. Susheela|Susheela, Pattom Sadan || Bichu Thirumala || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 