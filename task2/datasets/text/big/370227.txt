Young Törless
{{Infobox film
| name           = Young Törless
| image          = Young Torless poster.jpg
| caption        = 
| director       = Volker Schlöndorff
| producer       = Franz Seitz
| writer         = Volker Schlöndorff Herbert Asmodi Robert Musil (novel)
| starring       = Mathieu Carrière Marian Seidowsky Bernd Tischer Fred Dietz
| music          = Hans Werner Henze
| cinematography = Franz Rath
| editing        = Claus von Boro
| distributor    = Cine-International
| released       =  
| runtime        = 87 minutes
| country        = West Germany / France
| language       = German
| budget         = 
}} sadistic and homoerotic tendencies of a group of boys at an Austrian military academy at the beginning of the 20th century.

==Plot==
The story is set at the beginning of the 20th century. When Thomas Törless (Mathieu Carrière) arrives at the academy, he learns how Anselm von Basini (Marian Seidowsky) has been caught stealing by fellow student Reiting (Fred Dietz), and is obliged to become Reitings "slave," bowing to Reitings sadistic rituals. Törless follows their relationship with intellectual interest but without emotional involvement.

Also partaking in these sessions is Beineberg (Bernd Tischer), with whom Törless visits Bozena (Barbara Steele), the local prostitute. Again, Törless is aloof and more intrigued than excited by the woman.

He is however very eager to understand imaginary numbers, which are mentioned in his maths lesson.  The maths teacher is unwilling or unable to explain what these are, stating that in life, emotion is what rules everything - even mathematics.  

After Basini is humiliated and suspended upside down in the school gym because of one of Reitings intrigues, Törless realises intellectually that the other boys are simply cruel. He seems no more or less emotionally moved by this than by the revelation that he cannot understand imaginary numbers. He decides that he does not want to partake in cruelty, so decides to leave the academy. His teachers think that he is too "highly strung" for his own good, and do not want him to stay anyway - they are part of the system which can allow such terrible things to be done to the weak and vulnerable. 

At the end of the film Törless is dismissed from the school and leaves with his mother, smiling.

==Cast==
* Mathieu Carrière - Thomas Törless
* Marian Seidowsky - Anselm von Basini
* Bernd Tischer - Beineberg
* Fred Dietz - Reiting
* Lotte Ledl - Gastwirtin / Innkeeper
* Jean Launay - Mathematiklehrer / Maths Teacher
* Barbara Steele - Bozena

==Music== Fantasia for Strings.

==Awards== FIPRESCI Prize Best Foreign Language Film at the 39th Academy Awards, but was not accepted as a nominee. 

==See also==
* List of submissions to the 39th Academy Awards for Best Foreign Language Film
* List of German submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 