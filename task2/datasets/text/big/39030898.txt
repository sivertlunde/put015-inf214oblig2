Thaalikaatha Kaaliamman
{{Infobox film
| name           = Thaalikaatha Kaaliamman
| image          = 
| image_size     =
| caption        = 
| director       = R. Somasunder
| producer       = P. Kannappan Yadav
| writer         = R. Somasunder P. Kalaimani  (dialogues) 
| starring       =  
| music          = Sirpy
| cinematography = Babu
| editing        = K. R. Ramalingam
| distributor    =
| studio         = M. G. Pictures
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 2001 Tamil Tamil devotional Kausalya and Sanghavi in lead roles. The film, was produced by P. Kannappan Yadav, had musical score by Sirpy and was released on 16 February 2001.    

==Plot==

Bose (Prabhu (actor)|Prabhu) and his cousin Pandi (Pandiarajan) come to a village for discovering the miracles of the villages Goddess.

In the past, Abirami (Indhu), a young woman drowned in the village pond. The whole village then thinks that she had done some wrong for which Goddess had punished her. The villages Nattamai (Alex) ordered Abiramis family to leave the village immediately, her husband refused to perform her last rites and abandoned his little daughter. The child was brought up by Kamachi (Manorama (Tamil actress)|Manorama), the Nattamais wife. Thereafter, the village became unpopular, arid and the villagers left the village one by one.

Now, Indhus daughter, Karpagam (Kausalya (actress)|Kausalya) has grew up. Karpagams uncle, a rowdy, wants to steal her things but Bose saves her. The villagers force Karpagam to do a cruel thing. To protect her, Bose decides to marry her but the villagers refuses. The villages Nattamai tells what happened really : Abirami was in fact killed by Maruthu, Nattamais son, and Nattamai concealed it. Later, Goddess punished them : Maruthu died while Nattamai became physically ill. Bose marries Karpagam and the villagers apologise to Karpagam.

Bose returns to his house with his wife. Even though Bose married her secretly, his father Dharmalingam (Manivannan) blesses them and he prepares their wedding reception. At his first night, Bose finds Ramya (Sanghavi) instead of Karpagam in his room and she declares herself to be the woman he married. So Bose asks to his father who is Ramya and he tells the past.

Raghavan (Rajan P. Dev), a police officer, was transferred to their city and he became Dharmalingams family friend. Ramya, Raghavans daughter, met Bose in numerous places and she developed a soft corner for him. Raghavan arrested Bose in a misunderstanding and he released him immediately. Raghavan was subsequently suspended and wanted to take revenge on Bose and Dharmalingam. Bose and Ramya finally fell in love with each other. They got married secretly with Dharmalingam and Pandis help. Raghavan shot Bose with his gun and kidnapped his daughter. By some miracle, Bose survived and forgot everything. Since this incident, Dharmalingam had no informations about Ramya.

So Bose leaves his house with Karpagam and Pandi. They move in their guest house, there he sees again Ramya who wants to live with him. Later, Bose meets Ramyas mother who is now widow and she tells what happened to Ramya and Raghavan. After kidnapping Ramya, Raghavan tried to take of her thaali but she committed suicide. Few days later, Raghavan died in shock.

Ramyas spirit now wants to fulfil her wish of living with Bose by taking over Karpagams body. The rest of story is what happen to Bose, Karpagam and Ramya.

==Cast==
 Prabhu as Bose Kausalya as Karpagam
*Sanghavi as Ramya
*Pandiarajan as Pandi
*Manivannan as Dharmalingam, Boses father
*Bhanupriya as Kaliamman (guest appearance) Manorama as Kamachi, Nattamais wife
*Venniradai Moorthy Mansoor Ali Khan
*Rajan P. Dev as Raghavan
*Venniradai Nirmala as Ramyas mother Alex as Nattamai
*Jyothi Lakshmi
*Indhu as Abirami
*Thalapathy Dinesh as Ranjith
*Suryakanth
*Baby Aishwarya as Karpagam (child)

==Production==
The film is directed by R. Somasunder who has also penned the story and the screenplay. Somasunder has earlier wielded the megaphone for films like Engal Kula Deivam, Uyir, and Amman Koil Thiruvizha. The dialogue is written by Kalaimani. Sirpi sets the tune to lyrics written by Kalidasan, Pazhnibharathi, Kalaikumar, Vijay and Ravishanker. Editing is by K.R. Ramalingam, work by Kalai, and stunt choreography by Dalapathy Dinesh. A shooting stint took place at Rekha House, Saidapet, where a scene was picturised on Prabhu, Kousalya, Sanghavi and Manivannan. The film is produced by Sriyadav of Sunder Theatres, producers of such films as Ulavali, and Pass Mark. 

==References==
 

 
 
 
 