Spanish Masala
{{Infobox film
| name           = Spanish Masala
| image          = Spanish-masala_film.jpg
| caption        = Theatrical poster
| alt            =
| director       = Lal Jose
| producer       = Noushad
| writer         = Benny P Nayarambalam Dileep Kunchacko Boban Daniela Zacherl Biju Menon Vinaya Prasad, Nelson(comedy actor) Vidyasagar
| cinematography = L. Lokanathan
| editing        = Kevin 
| studio         = Big Screen Productions
| distributor    = PJ Entertainments Europe
| released       =   
| runtime        = 153 minutes
| country        = India
| language       =MALAYALAM| budget         = 
| gross          = 
}}

Spanish Masala is a 2012 Malayalam romantic comedy film directed by Lal Jose, starring Dileep (actor)|Dileep, Kunchacko Boban, Daniela Zacherl, Biju Menon and Chrys Hobbs. The film was a flop at the Box office.

==Plot==
The location of the story is Spain, and hence the name of the movie. The story revolves around Charlie (Dileep (actor)|Dileep), an illegal immigrant stranded in Madrid. To make things worse, he is conversant only in Malayalam. He gets a job as a cook in an Indian restaurant,as he has some experience in running a roadside eatery in his homeland, run by an expatriate, Majid. In the restaurant he is assigned with the task of serving up various types of dosas and, he comes up with a local variant named Spanish Masala. This turns out to be his passport to employment in the home of an ex-diplomat, who had earlier served in India. Here he meets the diplomats daughter Camilla (Daniela Zacherl), who is visually impaired. She becomes a big time fan of Charlies Spanish Masala. She is not on talking terms with her father, who is believed to have killed her Indian lover, Rahul (Kunchacko Boban), the son of her Malayali nanny, from whom she has learnt to speak Malayalam. Later Camilla starts to get closer to Charlie. Since he can imitate anyones voice, one servant Pappan (Nelson) leads him to imitate Rahuls voice and keep Rahuls memory alive. After sometime, she recovers and starts seeing everything, but to see her, her dad is no more. A strange incident occurs when Rahul arrives for his funeral. One day, as per Menons (Biju Menon) instruction, Charlie cleans Señor (Camillas Dad)s room. He finds a CD which Señor had seen before he died, and from that he finds out that Rahul was sent to Portugal to avoid the relationship between Camilla and Rahul, by paying him money by Señor. Again Rahul is spared by Charlie. As it goes, one day Menon enters a bar where he sees Rahul and another girl dancing together. He went to interrogate about it by following him, when he saw Rahul sitting with some goons and the girl. Later he researched on it, when he came to know that it wasnt a good life for Rahul in Portugal. He sold the factory which Señor offered him and he had taken loan from some goons for gambling and could not repay them. Camilla heard about it and she felt sorrow that she was keeping a crooked lover in her mind. Later Camilla and Menon go back to Kerala, India to fix the alliance between Charlie and Camilla.
 Hooli, Shekhar. (2011-07-23)  . Entertainment.oneindia.in. Retrieved on 2012-12-23. 

==Cast== Dileep as Charlie
*  Kunchacko Boban as Rahul
* Daniela Zacherl as Camilla
* Biju Menon as Menon
* Vinaya Prasad as Rahuls mother
* Kalaranjini as Theresa; Charlies mother
* Nelson as Pappan
* Archana Kavi as Lily (Charlies sister) - Cameo
* Nivin Pauly as Mathews (Lilys husband) - Cameo

==Production==
Most of the film was shot in Madrid, the capital of Spain. One song was shot in Vienna, the capital of Austria, and the remaining portions were shot in Cochin and Alappuzha districts of Kerala.  The film portrays several Spanish arts, festivals and sports such as bullfighting, flamenco dance and La Tomatina which was the real la tomatina festival shot for the first time in Indian cinema.  Daniela Zacherl, the female lead, is an Austrian model.    She replaced Amy Jackson who had to drop out due to conflicts with Ekk Deewana Tha. 

==Reception==
Spanish Masala received mixed reviews.

==Soundtrack==
Yet another combination of Lal Jose and composer Vidyasagar (music director)|Vidyasagar, the lyrics are penned by R. Venugopal. The soundtrack was rated 6/10 by Music Aloud. 

{|class="wikitable" its 
|-
!# !! Song Title !! Singers
|-
| 1 || "Akkare Ninnoru" || Vineeth Sreenivasan, Sujatha Mohan
|-
| 2 || "Aareyuthiyatho" || Karthik (singer)|Karthik, Shreya Ghoshal
|-
| 3 || "Hayyo Hayyo" || Yazin nizar , franco
|- Vidyasagar
|- Karthik
|}

==References==
 

==External links==
*  

 

 
 
 
 
 