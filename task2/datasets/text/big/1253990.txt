Venger
 
 
{{Infobox film
| name           = Venger
| image          =
| director       = Marc Furmie
| producer       = Matt Inglis Simon Ritch Hadi Tieu-Vinh
| writer         = Marc Furmie Shiyan Zheng Vincent Andriano
| starring       = Christian Clark Henry Nixon Dan Mor Bren Foster Jai Koutrae Amanda McConnell James Caitlin
| music          = Samantha Fonti
| cinematography = Marc Windon
| editing        = Gabriel Dowrick
| studio         = The Filmmakers Factory
| distributor    = The Filmmakers Factory
| released       =  
| runtime        = 15 minutes
| country        =
| language       = English
}} Mercedes GP Petronas Formula One Team. Filming took 7 days at iconic locations across Sydney, majority of which were on the Northern Beaches peninsular. A 4-minute teaser was presented to a crowd of 500 people on the final day of shooting at a celebration for the Australian Film Industry.

== Plot ==
Following the murder of his fiancé, racecar driver Anthony McCullough becomes hellbent on revenge. Set loose by a ruthless detective, McCullough begins to follow a trail of clues. As he gets closer to finding the men responsible, he is drawn into a web of secrecy and betrayal that threatens to destroy his entire family.

== Production ==
The movie was created as a branded content film for the G Brothers Mercedes-Benz dealership and Petronas Lubricants International by executive producers Jesse Press & Marc Windon of The Filmmakers Factory and writer/director Marc Furmie of FG Entertainment. The project was designed to merge strong storytelling and brand awareness with a concept that resonates with online audiences – eventually becoming the first episode in a web series. The screenplay was written by Marc Furmie, Shiyan Zheng and Vincent Andriano. Influenced by the classic action-revenge thrillers of the 70s such as Bullitt, Death Wish and Dirty Harry, Venger showcases several current model Mercedes-Benz in an edgy and stylish framework.

The 2 planned sequels will feature the AMG SLS Class, SL class, GLK class and many more of the best Mercedes-Benz cars in the world.

Venger is one of the first productions to use the Arriflex Alexa digital cinema camera.

== Release ==
The film is due for release on February 17, 2011.

==References==
* http://www.benzinsider.com/2011/01/video-“venger”-trailer—sydney-mercedes-benz-dealership-co-presents-upcoming-film/ "BenzInsider" preview of Venger by Keiichi January 28, 2011
* http://www.emercedesbenz.com/autos/mercedes-benz/corporate-news/venger-film-trailer-featuring-mercedes-benz-vehicles-makes-its-debut/ eMercedesBenz preview by JClark 31 January 2011
* http://marcwindon.blogspot.com/2011/02/venger-web-film-series-by-g-brothers.html?spref=tw "Blog article" by Marc Windon on February 3, 2011

==External links==
*  
*  
*  
*  

 
 
 
 
 
 