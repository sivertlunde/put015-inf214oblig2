Into the Sun (2005 film)
 
{{Infobox film
| name           = Into the Sun
| image          = intothesundvd.jpg
| caption        = 
| director       = Christopher Morrison
| producer       = Frank Hildebrand Tracee Stanley-Newell
| writer         = Steven Seagal Joe Halpin Trevor Miller
| starring       = Steven Seagal Matthew Davis Takao Osawa Eddie George with William Atherton and Juliette Marquis
| music          = Stanley Clarke
| cinematography = Don E. FauntLeRoy
| editing        = Michael J. Duthie
| distributor    = Destination Films   Sony Pictures Home Entertainment
| released       =  
| runtime        = 97 minutes
| country        = United States Japan
| language       = English Japanese
| budget         = $15,000,000 
| gross          = $175,563 
}}  American yakuza Black Rain.  It stars martial artist/actor Steven Seagal. Originally conceived as a remake of Sydney Pollacks The Yakuza, Warner Bros. would not release the rights to the story and the film was consequently reworked into Into the Sun.  The film was directed by Christopher Morrison, also known as "mink".

==Plot==
The assassination of Tokyo governor Takayama (Mac Yasuda) causes a stir of public outrage in Tokyo, Japan. Upon hearing the news of the incident, the FBI asks the CIAs Tokyo office to investigate the killing, believing it to be linked to the Yakuza, a dangerous Japanese mafia. Retired CIA agent Travis Hunter (Steven Seagal) and rookie FBI agent Sean Mac (Matthew Davis) are assigned to work on the case and to track down the perpetrators. The Japanese branch of the CIA starts sniffing around under the auspices of Homeland Security. During work, Mac proves to be more of a distraction, not really knowledgeable about procedures or Japanese customs. Hunter was raised in Japan, and has a strong understanding of the Yakuza and their mysterious, eccentric, and sinister ways.

Hunter and Mac discover a plan by Kuroda (Takao Osawa), the new boss of a new Yakuza outfit, to build an enormous drug-dealing network with a Chinese Tong outfit leader named Chen (Ken Lo), and Kuroda is killing everyone who gets in his way. It is revaled that Kuroda masterminded the governors assassination. Hunter turns for help to Kojima (Masato Ibu), the second-in-command of an old school Yakuza outfit run by elderly Oyabun Ishikawa (Shoji Oki). Hunter has been told that Kojima is the only Yakuza player who is capable of defeating Kuroda. Hunter visits Kojima, who says that now that the new Yakuza are joining forces with the Tongs, becoming too powerful. Kojima tells Hunter that he would love nothing more than to permanently get rid of Kuroda. 

As Hunter gets closer to Kuroda, he begins to endanger everyone associated with him. First, Kuroda has Ishikawa killed, making Kojima the new leader of Ishikawas Yakuza outfit. When Kuroda has Hunters fiance Nayako (Kanako Yamaguchi) brutally murdered, Hunters search for Kuroda becomes devastatingly personal. Teaming up with CIA spook Jewel (Juliette Marquis) and tattoo artist Kawamura (Daisuke Honda), whose wife and young child were killed by Kuroda, Hunter sets out to take down Kuroda in his hiding place in a temple.

They arrive at the hideout and brutally kill all the members with katana swords. Akayo arrives to slaughter some members and teams up with the two men. Kawamura is shot by Kuroda. Hunter then battles Kuroda and kills him by slashing his chest. They leave the crime scene after that. 

The three offer their respect to Nayako during a worship service and a ceremony is held to make a replacement of a leader. The hiding place of Kuroda is investigated and cleaned up with blue spray even his and all of the dead bodies and weapons. Hunter then walks to the park where he and Nayako used to stay.

==Cast==
* Steven Seagal	 ....	CIA Agent Travis Hunter, a law enforcement agent sent to Tokyo to track down a crime syndicate responsible for murdering the governor
* Matthew Davis	 ....	Sean Mac
* Takao Osawa	 ....	Kuroda, the leader of the crime syndicate responsible for assassinating Tokyos Governor and the main antagonist.
* Eddie George	 ....	Jones
* William Atherton	 ....	Agent Block
* Juliette Marquis	 ....	Jewel
* Ken Lo	         ....	Chen
* Kosuke Toyohara	 ....	Fudomyo-o
* Akira Terao	 ....	Matsuda
* Dale Payne	 ....	Zen Custodian
* Eve Masatoh	 ....	Kojima
* Pace Wu	         ....	Mai Ling
* Chiaki Kuriyama	 ....	Ayako, the younger sister of Nayako
* Kanako Yamaguchi	 ....	Nayako, Travis Japanese fiance who is murdered by one of the Yakuza mafia members
* Namihiko Ohmura	 ....	Takeshi
* Daisuke Honda	 ....	Kawamura
* Roy Oguri	         ....	Kenji
* Sokyu Fujita	 ....	Investigator Maeda
* Vikrom Suebsaeng	 ....	Chang Choudong
* Tim Yan	         ....	General
* Shôji Oki	         ....	Ishikawa
* Takashi Fujii      ....   Stand up comedian in bar scene
* Mac Yasuda	 ....	Governor of Tokyo
* Kairi Narita	 ....	Young Yakuza
* Ginji Yoshikawa	 ....	Yakuza Ceremony Master
* Joe Sagal	         ....	Tech #1
* Hatake	         ....	Sword Buyer
* Korokke	         ....	Club MC

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 