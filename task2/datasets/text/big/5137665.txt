Five Finger Exercise (film)
 
{{Infobox film
| name           = Five Finger Exercise
| image          = Fotobusta 141.jpeg
| caption        = Italian film poster
| director       = Daniel Mann
| producer       = Frederick Brisson
| writer         = Frances Goodrich  Albert Hackett Peter Shaffer (play)
| starring       = Rosalind Russell   Jack Hawkins   Richard Beymer  Maximilian Schell   Annette Gorman.
| music          = Jerome Moross
| cinematography = Harry Stradling Sr.
| editing        = William A. Lyon   
| distributor    = Columbia Pictures
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Five Finger Exercise (1962 in film|1962) is a drama film made by Columbia Pictures, directed by Daniel Mann and produced by Frederick Brisson from a screenplay by Frances Goodrich and Albert Hackett, based on the play by Peter Shaffer.

The film stars Rosalind Russell, Jack Hawkins, Richard Beymer, Maximilian Schell, and Annette Gorman, with an early screen appearance from Lana Wood, the sister of Natalie Wood.
 West End Broadway on December 2, 1959 and closed October 1, 1960 after 337 performances. The young Juliet Mills, a teenager at the time, played the role of Pamela Harrington, and was nominated for a Tony Award for her performance.

==Plot==
It follows a few days in the lives of the Harringtons, who are at war. While the husband and wife fight each other, the son and daughter are on the same path. Then, when a music teacher comes in, things begin to change, until other things start to threaten the peace.

==Cast==
* Rosalind Russell as Louise Harrington
* Jack Hawkins as Stanley Harrington
* Richard Beymer as Philip Harrington
* Annette Gorman as Pamela Harrington
* Maximilian Schell as Walter
* Lana Wood as Mary

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 