The Crucible (1957 film)
 
{{Infobox film
| name           = The Crucible
| image          = Lessorciersdesalem.jpg
| image_size     =
| caption        = Original film poster.
| director       = Raymond Rouleau
| producer       = Raymond Borderie
| writer         = Jean-Paul Sartre Arthur Miller (play) 
| starring       = Simone Signoret Yves Montand Mylène Demongeot Jean Debucourt Pierre Larquey
| music          = Georges Auric Hanns Eisler
| cinematography = Claude Renoir
| editing        = Marguerite Renoir
| studio = Compagnie Industrielle Commerciale Cinématographique Films Borderie DEFA
| distributor    = Pathé|Pathé Consortium Cinéma Kingsley-International Pictures
| released       =  
| runtime        = 145 minutes
| country        = France East Germany
| language       = French
| budget         = 
}}
 East German film production directed by Raymond Rouleau with a screenplay adapted by Sartre|Jean-Paul Sartre from the 1953 play The Crucible, by Arthur Miller. The film was only briefly released on home video, and is extremely difficult to find.

==Plot==
1692, Salem, Massachusetts. John Proctor is the only member in the towns assembly who resists the attempts of the rich to gain more wealth on the expense of the poor farmers, thus incurring the wrath of deputy governor Danforth. Proctors sternly puritanical wife, Elizabeth, is sick and has not shared his bed for months, and he was seduced by his maid, Abigail. When he ends his affair with her, Abigail and several other local girls turn to slave Tituba. Reverend Parris catches the girls in the forest as they partake in what appears to be witchcraft. Abigail and the rest deny it, saying that they have been bewitched. A wave of hysteria engulfs the town, and Danforth uses the girls accusations to instigate a series of trials, during which his political enemies are accused of heresy and executed. When Abigail blames Elizabeth Proctor, the latter rejects Johns pleas to defraud Abigail as an adulteress. Eventually, both Proctors are put on trial and refuse to sign a confession. The townspeople rebel, but not before John is hanged with other defendants; his pregnant wife has been spared. Elizabeth tells the angry crowd to let Abigail live.

==Cast==
* Simone Signoret as Elizabeth Proctor John Proctor
* Chantal Gozzi as Francy Proctor
* Mylène Demongeot as Abigail Williams
* Alfred Adam as Thomas Putnam
* Françoise Lugagne as Jane Putnam
* Raymond Rouleau as Thomas Danforth
* Pierre Larquey as Francis Nurse
* Marguerite Coutan-Lambert as Rebecca Nurse
* Jean Debucourt as Samuel Parris
* Darling Legitimus as Tituba
* Michel Piccoli as James Putnam
* Gerd Michael Henneberg as Joseph Herrick John Hale
*   as Mary Warren
* Véronique Nordey as Mercy Lewis
* Jeanne Fusier-Gir as Martha Corey
* Jean Gaven as Peter Corey
* Aribert Grimmer as Giles Corey
* Alexandre Rignault as Samuel Willard
* Pâquerette (Marguerite Jeanne Martine Puech) as Sarah Good
* Gérard Darrieu as Ezekiel Cheever
* François Joux as Judge
* Sabine Thalbach as Kitty
* Ursula Körbs as Wollit
* Hans Klering as Field

==Production== Sarah Bernhardt Theater, starring Simone Signoret as Elizabeth Proctor. Sartre later said he was moved to write his adaptation because "the play showed John Proctor persecuted, but no one knows why... His death seems like a purely ethical act, rather than one of freedom, that is undertaken in order to resist the situation effectively. In Millers play... Each of us can see what he wants, each public will find in it confirmation of its own attitude... Because the real political and social implications of the witch-hunt dont appear clearly." The screenplay was 300 pages long.  Sartres version was different from the original play in many ways; Elizabeth saves Abigail from lynching and the townspeople rise up against Thomas Danforth, who becomes the chief antagonist.  
 Till Ulenspiegels Les Misérables and Les Arrivistes. The Democratic Republics government authorized the DEFA studio to collaborate with companies outside the Eastern Bloc in order to gain access to Western audiences, thus bypassing the limitations imposed by West Germanys Hallstein Doctrine; eventually, they intended their films to reach also the public in the Federal Republic. The French, on their part, were interested in reducing costs by filming in East Germany.    Principal photography took place in DEFAs Babelsberg Studios from August to mid-October 1956, with additional shooting in Paris during early November. Susan Hayward. French Costume Drama of the 1950s: Fashioning Politics in Film. ISBN 978-1-84150-318-9. pp. 83-85. 

==Reception==      1957 BAFTA Award for Best Foreign Actress and Mylène Demongeot was nominated for the BAFTA Award for Best Newcomer in the same year.  In the 1957 Karlovy Vary International Film Festival, the Best Actor Award was given to "all actors of Les Sorcières de Salem in collective, and especially to Yves Montand."  

The New York Times critic Bosley Crowther wrote that "out of The Crucible... Jean-Paul Sartre and Raymond Rouleau have got a powerful and compelling film... For now Mr. Millers somewhat cramped and peculiarly parochial account... comes forth as a sort of timeless drama... This is a persistently absorbing film."  The Time (magazine)|Time magazines reviewer commented that "Witches of Salem is a foredoomed but fascinating attempt... But it hardly helps the scriptwriters case... When he sums the whole story up as an early American instance of class warfare." 

Michel Contat and Michel Rybalka, who edited and annotated Sartres writings, noted that he introduced a strong element of communist class struggle into his adaptation of Millers play, especially by turning Danforth from merely sanctimonious to a calculated villain who pulls the strings behind the trial, while making the character of Abigail more complex and consequently, almost sympathetic.  In the introduction to the 2010 edition of The Crucible, editor Susan C. W. Abbotson described the films plot as a "conflict between capitalists and heroic Marxists", writing that "Miller felt the Marxist references were too heavy-handed. Most critics agreed."  Abbotson also commented that "Sartre changed the plays theme... His version becomes despiritualized... As it desires to present us the heroic representatives of Communism."  In another occasion, Miller told that he disliked the film because it "reduced man to a digit in the socialist dialectic."  
 French War in Algeria but supported the Kremlin. Hayward, however, viewed it as standing in favor of the right to exercise free speech in general. 

==See also==
*Salem witch trials

==References==
 

==External links==
*  
*  
* 
*  on filmportal.de.
*  on cinema-leipzig.de.
*  on defa.de. 
*  on umass.edu.
*   on 2001.de.

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 