Kanchana 2
{{Infobox film
| name           = Kanchana 2
| image          = Muni 3 kanchana 2.jpg
| caption        = First look
| director       = Raghava Lawrence 
| producer       = Raghava Lawrence Bellamkonda Suresh
| writer         = Raghava Lawrence
| editor         = Kishore te
| choreographer  = Raghava Lawrence
| music          = Leon James C. Sathya S. Thaman Ashwamithra
| cinematography = R.B.Gurudhev Rajavel Olhiveeran
| starring       = Raghava Lawrence Taapsee Pannu Nithya Menen
}}

Kanchana 2 is a 2015  . The film was initially titled Muni 3 before it was changed to Kanchana 2.    The film, featuring Taapsee, Nithya Menen, Kovai Sarala and Jayaprakash in other pivotal roles, released worldwide on 17 April 2015. The Telugu version Ganga was released on 1 May 2015. 
==Plot==
The film starts with a husband and wife going to a friends home for dinner.To their surprise their friends are in hospital and the place is haunted. In the present, Raghavas mother introduces Raghavas room to her friend, which to her surprise contains 9 bedsheets of gods photo in it, and childrens diaper in it. Raghava works in Green TV channel as a cameraman where his crush also works, Nandhini.
When Green TV goes to the second position, Nandhini advises to take an haunted programme and decide places. In the haunted house, near the beach Nandhini discovers a Thali which she is haunted with it.
As insrtucted by a saint, she prepares a coffin and later haunted by the ghost. Raghava and Nandhini move into an another house where to their surprise Raghavas mother and Nandhinis sister in law also stays in. Several haunted happenings start and everyone came to know that Nandhini is haunted and also she repeats the word"Shiva". By Nandhini(Now haunted as Ganga), she influences Shivas ghost to be sent in Raghava. Several spirits gets into Raghava(Motta Shiva, Nandhinis friend, Shankar,an Old lady, a child and Nandhinis friends Husband). Raghava take revenge on Marudhu when knowing that Shiva and Ganga loved but Marudhu instead killed and buried them. The film ends with Raghava in Muni Part 4.

==Cast==
* Raghava Lawrence as Raghava/Motta Shiva
* Nithya Menon as Ganga
* Tapsee Pannu as Nandini
* Kovai Sarala as Raghavas mother
* Jayaprakash as Marudhu
* Rajendran as Marudhus brother
* Manobala as Arnold
* Mayilsamy as Mayi
* Sriman as Dr. Prasad Renuka as Nandhinis sister in law
* Pooja Ramachandran as Pooja
* Jangiri Madhumitha as Aishwarya
* Suhasini Maniratnam
* SriPuram Sai sateshKumar

==Production==
Raghava Lawrence began work on the third installment in the Muni franchise in 2012, then titled Muni 3: Ganga. He said that he had two different storylines for the sequel.    Amy Jackson was roped in to play the female lead but due to date problems, she was replaced by Pranitha Subhash. Later, Pranitha was replaced by Taapsee.  Anjali was signed up for a cameo role,  but she was later replaced by Nithya Menen.  It was reported that Lawrence would introduce his brother Elvin in the film, who would dance in one song.  He was reported to be appearing in six different looks in the film.  Pooja Ramachandran was roped in who stated that she plays the role of an anchor, who is a drama queen.  In October 2013, Lawrence injured himself while filming and the making of the film was stopped for over three months.  The film was in production for over two years. 

==Release==
The film recieved U/A certificate. The film was released on 17 April 2015 in around 750 screens worldwide alongside Mani Ratnams O Kadhal Kanmani.  Telugu version released on May 1, 2015, Got good reviews. 

===Critical reception===
Kanchana 2 opened to mostly positive reviews from critics.

The Times of India gave 3 stars out of 5 and wrote, "The comedy is broad and low-brow, banking on gay humour and the reactions of the characters when they are spooked by the presence of the ghost. It becomes too childish at times but you laugh nevertheless...the film does what it promises — we chuckle, cower, cry and at times, cringe".  Behindwoods gave the film 2.75 out of 5 stars and said, "Looks like Lawrence has mastered the genre, so well that he knows how to keep the audience at the edge of their seats. His creative imagination has made this film different from the usual horror comedy films".  Rediff rated the film 2.5 on 5, calling it "an enjoyable film" and wrote, "The plot may be out of date and the comedy absurd, but the antics of the familiar characters and their enthusiasm make you laugh despite yourself".  Sify wrote, "Kanchana 2 is no different from Lawrences earlier horror comedies...The formula is the same - mix crass comedy with lots of horror, dead persons spirit getting into hero/heroine body, slapstick humour, glamour, melodrama, ladies sentiments, fight scenes...Add five songs, haunted house, scream and wailing sounds, it works". 

Indo-Asian News Service gave 2.5 stars out of 5 and wrote, "The movie, which follows a very cliched and dated format of horror template, suffers heavily due to the lack of a good story" and called it "undoubtedly a weak film in the franchise". 
The Hindu wrote, "The acting isn’t the problem; the predictable story is...Kanchana-3, which is hinted at before the end credits, needs to do better if the franchise has to survive". 

===Box office===
Kanchana 2 was declared a Super Hit in the first weekend. 

====Domestic====
On its first day, the film grossed   nett in Tamil Nadu alone  and around   worldwide.    The film had a "phenomenal" opening, earning   nett in Tamil Nadu in two days.  Kanchana 2 earned   nett in Tamil Nadu in its opening weekend. The film netted   in Karnataka,   in Kerala and   from the rest of India.    In Chennai alone, the film made   in three days. 

The film did exceptional business in Tamil Nadu, earning   nett in ten days, prompting the trade to call it a Blockbuster. 

====Overseas====
The film opened with   in the USA. In UK and Ireland, the film made   in the opening weekend. In Malaysia, the film had a very good opening, grossing   in its first 3 days. 

==Soundtrack==
The soundtrack features five songs composed by C. Sathya, S. Thaman, Ashwamithra and debutant Leon James, with the latter contributing two songs to the album. 

{{tracklist	
| collapsed     = 	
| headline      = 	
| extra_column  = Singers
| music_credits = yes
| lyrics_credits = yes

| title1        = Sandimuni
| extra1        = Haricharan
| music1        = Leon James
| lyrics1        = Viveka
| title2        = Vaaya Veera
| extra2        = Shakthisree Gopalan
| music2        = Leon James
| lyrics2        = Ko Sesha
| title3        = Silatta Palatta
| extra3        = Jagadeesh Kumar
| music3        = C. Sathya
| lyrics3        = Ko Sesha 
| title4        = Motta Paiyaa
| extra4        = K. S. Chithra, Sooraj Santhosh
| music4        = S. Thaman
| lyrics4        = Viveka
| title5        = Moda Moda
| extra5        = Master Sriram Roshan
| music5        = Ashwamithra 
| lyrics5        = Viveka
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 