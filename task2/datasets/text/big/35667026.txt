The Tower (film)
{{Infobox film
| name           = The Tower  
| image          = The Tower2012-poster.jpg
| caption        = Promotional poster for The Tower
| film name      = {{Film name
| hangul         =  
| rr             = Tawo
| mr             = T‘awŏ}} Kim Ji-hoon
| producer       = Lee Han-seung   Lee Su-man
| writer         = Kim Sang-don   Heo Jun-seok
| adaptation     = Kim Ji-hoon   Yoo Young-ah    Lee Min-jae
| starring       = Sol Kyung-gu   Kim Sang-kyung   Son Ye-jin 
| music          = Kim Tae-seong
| cinematography = Kim Young-ho
| editing        = Kim Sang-beom   Kim Jae-beom
| studio         = The Tower Pictures
| distributor    = CJ Entertainment
| released       =  
| runtime        = 121 minutes
| country        = South Korea
| language       = Korean
| budget         =    
| gross          =   
}} disaster thriller thriller film Kim Ji-hoon,    and stars Sol Kyung-gu, Kim Sang-kyung and Son Ye-jin in the lead roles.   It was released in theaters on Christmas Day, December 25, 2012. 

==Synopsis==
Lee Dae-ho (Kim Sang-kyung) is a single father and manager of the 120 story luxurious landmark building complex, Tower Sky in Yeouido, Seoul. He is an earnest employee liked by his colleagues and is secretly in love and secretly even wanted to marry with Seo Yoon-hee (Son Ye-jin), a restaurant manager. The owner of the complex, Mr. Jo decides to hold a "White Christmas" party for tenants and VIPs on Christmas Eve, with helicopters circling above with huge lights attached below them, sprinkling snow onto the party. Dae-ho has promised to spend the day at an amusement park with his daughter Ha-na, but had to cancel when he is needed at the party. However, the building has faulty water sprinklers due to frozen pipes, but Mr. Cha, the Tower Skys safety section head, is more concerned for the party than any possible architectural errors within the building, despite warnings from his fellow employers about these errors. (One example of their warnings is when cook Young-cheol (Jeon Bae-soo) accidentally leaves a stove on for too long and causes a minor fire.) Young-cheo is in love with a receptionist within the building named Min-jung, and even sneaks out of the kitchen while on duty to make her an ice cream. Meanwhile, Lee Seon-woo (Do Ji-han) is a rookie fireman entering the Yeouido Fire Station. When he gets accepted, he learns that many of the firefighters get more breaks than actual firefighting. In a prank to fool Seon-woo, the other firefighters ring the fire alarm for the squad to assemble while he takes a shower, causing him to appear in front of his entire team naked, much to the other firefighters and even his own pleasure as they put his helmet on him for the first time.

While the party is in full swing, and while Young-cheo proposes a rose to Min-jung in an elevator, huge gusts of wind cause one of the helicopters to lose control and the attached lights crash into the glass bridge connecting the two Tower Sky buildings and into a side of one of the buildings. Another one of the helicopters then crashes into the building, causing the helicopter to leak fuel and the building catches fire. Shards of glass fall onto the partygoers below, which include Mr. Yoon (Song Jae-ho) and his friend Mrs. Jung (Lee Joo-shil), while another one of the huge lights falls and crashes onto a bus. At one point, while evacuating, some people, including a security guard, board an elevator despite warnings not to use it, as the security guard rejects Yoon-hee and Ha-na from entering just as they are about to board. While traveling down, an explosion occurs below the elevator, which stops it in its path. This literally fries the people inside while melting their shoes until the elevator explodes, killing everyone in it. Dae-ho moves quickly to save Ha-na, Yoon-hee and his colleagues. At the same time, firefighters Kang Young-ki (Sol Kyung-gu) and Seon-woo fight to bring the blaze under control, helping Dae-ho in the process. Together they fight to save the lives of everyone. While Yoon-hee, Ha-na, Young-cheol, Ming-jung and the others take refuge in the buildings Chinese restaurant, the firefighters decide to contain the blaze at its origin, the 63rd floor, where the helicopter crashed. Despite containing the fire on that floor, it spreads further within the building. In the end, Young-ki sacrifices his own life to save not only Seon-woo, but for all people in the future, as he detonates a bomb to demolish the building, in order to prevent it from being damaged any further.   

==Cast==
 
* Sol Kyung-gu – Captain Kang Young-ki
* Kim Sang-kyung – Lee Dae-ho
* Son Ye-jin – Seo Yoon-hee      
* Kim In-kwon – Sergeant Oh Byung-man
* Ahn Sung-ki – Yeouido Fire Station chief
* Song Jae-ho – Mr. Yoon, the old man 
* Lee Joo-shil – Mrs. Jung, Mr. Yoons friend
* Lee Han-wi – Mr. Kim, church elder 
* Kwon Tae-won – Jang, the Fire Commissioner
* Jeon Guk-hyang – Ae-ja
* Jung In-gi – Cha, the safety section head
* Cha In-pyo – President Jo
* Jeon Bae-soo – Young-cheol, the cook
* Kim Sung-oh – In-geon
* Min Young – Nam-ok, the pregnant woman
* Park Jun-seo – aide
* Lee Joo-ha – Min-jung, the receptionist
* Do Ji-han – Lee Seon-woo, the rookie fireman  
* Jo Min-ah – Lee Ha-na, Dae-hos daughter
* Lee Sang-hong – Yeouido fireman
* Jin Mo – Yeouido fireman
* Chu Min-ki – Yeouido fireman
* Kang Poong – Yeouido fireman
* Kwon Hyun-sang – Young-hoon
* Lee Chang-yong – command HQ specialist
* Lee Chang-joo – Jos private secretary
* Park Chul-min – head cook Kim Eung-soo – Jin
* Park Jeong-hak – Jung
* Park Yong-su – Park
* Nam Sang-seok – reporter in front of Tower Sky
* Kwon Young-hee – reporter in front of Tower Sky
* Lee Min-woo – reporter in front of Tower Sky
 

==Production== Kim Ji-hoon Sector 7 May 18) was inspired by the 1974 Hollywood film The Towering Inferno, and his personal experience seeing the 63 Building in Seoul for the first time as a middle school student and imagining how it would feel to be trapped inside. 

The crew built 26 different sets to create various spaces in the fictional 108-story Tower Sky such as a Chinese restaurant, elevators and a pedestrian overpass between the two blocks. For the scenes involving water on the 80th floor, actors Sol Kyung-gu and Kim Sang-kyung filmed in a water container set in Goyang City, Gyeonggi Province, without using stuntmen. 
 cuts out CGI and miniature in motion control camera for the ending scene. 

==Box office==
On its theater release on December 25, 2012, The Tower drew 431,759 admissions, the second highest opening day ticket sales in the history of Korean cinema (after the The Thieves 436,628).    It sold two million tickets in its first week,  3.54 million by its second week,  and 4.45 million by its third week.  On January 22, 2013, it became the first Korean film in 2013 to reach the five million mark.    

==International==
The film was pre-sold by CJ Entertainment to Entertainment One in UK, Splendid in Germany, Benelux, Zylo for French-speaking territories, Horizon International in Turkey, Rainbow Entertainment in Singapore, Indonesia and Malaysia; and Jonon Source in Mongolia. 

The Tower earned   at the Hong Kong box office. 

==Awards and nominations== 2013 Baeksang Arts Awards
*Nomination - Best New Actor - Do Ji-han

;2013 Mnet 20s Choice Awards
*Nomination - 20s Booming Star, Male - Do Ji-han

;2013 Buil Film Awards
*Nomination - Best Cinematography - Kim Young-ho

;2013 Grand Bell Awards
*Technical Award - Digital Idea

==References==
 

== External links ==
*    
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 