Fighter in the Wind
 
{{Infobox film
| name           = Fighter in the Wind
| image          = Fighter in the Wind movie poster.jpg
| caption        = Poster for Fighter in the Wind
| film name      = {{Film name
 | hangul         =  
 | rr             = Baramui paiteo
 | mr             = Paramŭi p‘aiut‘ŏ}}
| director       = Yang Yun-ho
| producer       = Jeon Ho-jin
| writer         = Yang Yun-ho
| starring       = Yang Dong-geun Masaya Kato Aya Hirayama
| music          = Choi Man-shik
| cinematography = Shin Ok-hyeon
| editing        = Park Sun-deok
| distributor    = Big Blue Film
| released       =  
| runtime        = 120 minutes
| language       = Korean Japanese
| country        = South Korea
| budget         =  
}}
Fighter in the Wind ( ) is a 2004 South Korean film. It is based on the Japanese book Karate Baka Ichidai which is a fictionalized account of karate competitor Choi Yeung-Eui (최영의, 崔永宜) who went to Japan after World War II to become a fighter pilot but found a very different path instead. He changed his name to Masutatsu Oyama (大山倍達) and went across the country, defeating martial artists one after another. This film concentrates on the period when he is still young, and developing his famous karate style, Kyokushin.  The film was the 7th highest grossing Korean film of 2004 with 2,346,446 admissions sold nationwide. 

==Plot summary== Choi Bae-dal is a young Korean man who longs to be able to fly fighter planes. Stowing away to Japan in order to join their air force, Bae-dals first experience of the country is when a confidence trickster|con-man tries to steal his money. Bae-dal discovers that the man is a fellow Korean called Chun-bae (Jung Tae-woo), who has survived the harsh treatment of Koreans in Japan by turning to petty crime. With their different motives: Bae-dal driven by desire for action and Chun-bae needing to escape from some gangsters, the two Koreans stow away in a truck to the air force training camp.
 fighting style, American attack on the airforce base allows Bae-dal and Chun-bae to escape.
 gangsters try to take protection money from Chun-bae, Bae-dal tries to defend him but is beaten up and humiliated by the gangsters. His ordeal is ended by the intervantion of Bum-soo (Jung Doo-hong), a martial arts expert from his home town who had also emigrated to Japan. Bum-soo invites Bae-dal back to the circus where he, and many fellow Korean immigrants, work and where he is attempting to build a decent standard of living for his countrymen. After some persuasion, he agrees to teach Bae-dal some of his more sophisticated fighting style.

Meanwhile, Bae-dal has taken to working as a rickshaw driver, honing his fighting skills by defending Japanese women from the rapacious advances of American servicemen. His success at protecting the women makes him something of a local hero, although his real identity is not known. One of the women he protects is the beautiful Yoko (Aya Hirayama), with whom he strikes up a romantic relationship.

When Bum-soo is killed by local gangsters, the Koreans from the compound vow revenge and attack the Japanese gangs. The fight ends abruptly for Bae-dal when he is knocked unconscious by a blow to the head. Bae-dal, vowing to never again lose a fight, retreats to the mountains where, living in his karate gi, he trains day and night; running in the mountains, lifting tree trunks and using makeshift training equipment to harden his body and fighting spirit through austerity.

Returning from the mountains, Bae-dal takes a Japanese name: Masutatsu Oyama, and sets about challenging the best fighters Japan has to offer. Wearing his ragged karate gi and looking like a cave-man with his unkempt appearance, Oyama challenges the first dojo he passes. He defeats every fighter in the dojo - often with only a single strike.

As word of his notoriety spreads, Oyamas actions come to the attention of the head of the Japan Karate Association - the former Air Force camp commander Kato. Kato is hugely offended that a foreigner would not only try to learn Japanese martial arts, but would consider himself worthy to beat Japanese fighters. Nevertheless, Oyama continues to defeat every fighter that Japan has to offer, including competitors in karate, judo, ninjutsu and Okinawan kobudō|kobudo, becoming a sensation in the Japanese media. Oyama explains to Yoko that, although he is scared of dying, he is more scared of living as a cripple, and this is why he is willing to sacrifice anything to win.

When the organization sends one of his followers to challenge and kill Oyama, the agent is instead killed by Oyama. Learning that the man he killed had a wife and son, Oyama feels a great deal of guilt for his actions and tracks down the family to apologise and offer to work for them to make up for killing the father of the household. Oyama surrenders his uniform to the wife, vowing to never again fight in martial art duels. Although initially angry and unaccepting of Oyamas offer, after fulfilling the sons wish of being carried to the top of the nearby mountain to view the sunrise, he eventually convinces them that he is a man of honour and not a violent thug. The wife asks Oyama to take back his uniform and become the best fighter in Japan.

Returning to the city, Oyama finds that Katos martial arts association has threatened his own family (Kato is not involved) and demanded a challenge between Kato and Oyama. Dressing in his weathered gi once again, Oyama treks out to the countryside location where Kato is waiting for him. Easily defeating Katos henchmen, Oyama then faces a final showdown with Kato himself. Although it is clear that Kato would like to see Oyama dead, Katos ankle is broken after receiving a kick in the fight sequence. When Kato stands up, he falls to a one-knee-down position, Oyama shows mercy to Kato, by stopping 2&nbsp;cm short of punching him squarely between the eyes, defeating him in combat but not killing him.

At the end of the movie, Oyama is shown fighting with a bull, grasping the horns and digging into the ground to stop him, and finally delivering a bone-shattering chop to the center of the top of the head.

==Cast==
* Yang Dong-geun as Choi Bae-dal, the main character who becomes a master of karate (based on the real life karateka Masutatsu Oyama).
* Masaya Kato as Kato, the head of the Japanese Karate Association and, due to his xenophobia, enemy of Bae-dal.
* Aya Hirayama as Yoko, a young Japanese woman who is rescued by Bae-dal and falls for him.
* Jung Tae-woo as Chun-bae, Bae-dals friend, something of a scoundrel but a loyal friend.
* Jung Doo-hong as Bum-soo, a martial arts master from Bae-dals home town who becomes a mentor to him.

==DVD releases==
  :
Cinema Epoch (America)
* Aspect Ratio: Widescreen (1:78:1) anamorphic
* Sound: Korean (Dolby Digital 2.0 Stereo)
* Subtitles: English
* Supplements: "Action Diary of a Martial Arts Director" featurette, Director & cast interviews, Behind the scenes featurette, Music video, Trailer, Stills gallery
* Region 1, NTSC

  France : DVD dubbed in French (2005).

==See also==
* Choi Bae-Dal

==References==
 

==External links==
*  
*  

 
 
 
 
 
 