Two Idiots in Hollywood
{{Infobox film
| name           = Two Idiots in Hollywood
| image          = Twoidiots.jpg
| image_size     =
| alt            =
| caption        = VHS cover
| director       = Stephen Tobolowsky
| producer       = David Lancaster
| writer         = Stephen Tobolowsky	 
| narrator       =
| starring       = Jim McGrath Jeff Doucette
| music          = Stephen Tobolowsky	
| cinematography = Robert Brinkmann
| editing        = Andy Blumenthal
| studio         = New World Pictures
| distributor    = 
| released       = 1 October 1988 		
| runtime        = 85 minutes
| country        =  
| language       = English
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    =
}}
Two Idiots in Hollywood is a 1988 comedy film written and directed by actor Stephen Tobolowsky, based on his stage play of the same name. 

==Plot==
Taylor and Murphy leave their small town existence in Ohio for the glamour of Hollywood. Taylor, completely without talent, becomes a successful television producer, while Murphy is tried for a murder he didnt commit. 

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Jim McGrath || Murphy Wegg
|-
| Jeff Doucette || Taylor Dup 
|-
| Cheryl Anderson || Marianne Plambo 
|-
| Lisa Robins || NBA Casting Secretary 
|-
| Kurtwood Smith || Defense Attorney 
|-
| M.C. Gainey || Sgt. Albert  
|}

==Critical reception==
The film was critically panned. From Chris Willman of The Los Angeles Times:
 

==References==
 

== External links ==
* 

 
 
 
 
 
 

 