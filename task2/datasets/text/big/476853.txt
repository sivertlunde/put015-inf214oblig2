At the End of the Day: The Sue Rodriguez Story
{{Infobox film
| name           = At the End of the Day: The Sue Rodriguez Story
| image          =
| image_size     =
| caption        =
| director       = Sheldon Larry
| producer       = Laszlo Barna
| writer         = novel Lisa Hobbs Birnie Sue Rodriguez screenplay Linda Svendsen
| narrator       =
| starring       = Wendy Crewson Carl Marotte
| music          = Mychael Danna Andrew Lockington
| cinematography = Albert J. Dunk
| editing        = Stephen Lawrence
| studio         = Atlantic Mediaworks
| distributor    = Alliance Atlantis Communications
| released       = 23 February 2007 (UK)
| runtime        =
| country        = Canada
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}		
 Canadian docudrama film about the life of Canadian right to die advocate Sue Rodriguez.

The film was written by Linda Svendsen based on the book by Lisa Hobbs Birnie, and directed by Sheldon Larry.

==Cast==
* Wendy Crewson    as Sue Rodriguez
* Carl Marotte     as David Rodriguez
* Patrick Galligan as Svend Robinson
* Walter Learning  as Justice Allen Melvin
* Al Waxman        as John Hofsess
* Miko Hughes      as Jesse Rodriguez
* Fiona Reid       as Sandra Burtch
* Jacob Whitney    as Whos that guy?

Politician Svend Robinson had a minor role in the film. He was the third reporter at Sue Rodriguezs first press conference

==Awards==
*In 1999 Wendy Crewson won the Best Performance by an Actress in a Leading Role in a Dramatic Program or Mini-Series at the Gemini Awards
*In 1999 Linda Svendsen won the WGC Award from the Writers Guild of Canada

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 
 