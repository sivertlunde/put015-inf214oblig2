The Bad Pack
{{Infobox film
| name           = The Bad Pack
| image          = TheBadPack.1997.dvdcover.png
| caption        = DVD cover
| director       = Brent Huff
| producer       = {{plainlist|
* David A. Jackson
* Terence M. OKeefe
* John Orland
* Shauna Shapiro Jackson
* Rustam Branaman
* Rod Wasserman
}}
| writer         = {{plainlist|
* Brent Huff
* Douglas L. Walton
}}
| starring       = {{plainlist|
* Robert Davi
* Roddy Piper
* Ralf Möller
* Larry B. Scott
* Shawn Huff
* Patrick Dollaghan
}}
| music          = Bob Christianson
| cinematography = Richard A. Jones
| editing        = Frank Muto
| studio         = Sandwedge Films
| distributor    = Lionsgate Home Entertainment Showcase Entertainment Ascot Films Audio Visual Entertainment (Greece) Avalanche Home Entertainment Broadway (Japan)
| released       =   (Germany)   (USA)   (Japan)
| runtime        = 81 minutes 
| country        = {{plainlist|
* United States
}}
| language       = English
| budget         = 
| gross          = 
}}
 1997 independent independent action Michael Cole, Jeep Swenson and Sven-Ole Thorsen.   The Bad Pack was Swensons last film appearance, as he died of heart failure prior to the films release.

==Plot==
A town of Mexican immigrants (on the Texas border) hire a team of mercenaries to protect them against an underground militia group, who try to claim the town as their own.

==Cast==
* Robert Davi as McQue
* Roddy Piper as Dash Simms
* Ralf Möller as Kurt Mayer (credited as Ralf Moeller)
* Larry B. Scott as Jeremy Britt
* Shawn Huff as Remi Sykes
* Patrick Dollaghan as Latrell Hoffman
* Brent Huff  as Callin
* Marshall R. Teague as Lamont Sperry
* Daniel Zacapa as Hector Chavez
* Bert Rosario as Jose Chavez Michael Cole as Fredrickson
* Jeep Swenson as Missouri Mule
* Sven-Ole Thorsen as Sven Vernon Wells as Biker
* Cristan Crocker-Reilly as Carmen
* Clifton Collins, Jr. as Townsman 1
* Joe Unger as Fight Promoter

==Reception==
The movie was not a big hit and got very poor reviews.

==References==
 

 
 
 


 