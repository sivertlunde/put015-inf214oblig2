Ride Along (film)
 
{{Infobox film
| name           = Ride Along
| image          = Ride Along poster.jpg
| image_size     = 220px
| border         = yes
| alt            =  
| caption        = Theatrical release poster
| director       = Tim Story
| screenplay     = {{Plain list |
* Greg Coolidge
* Jason Mantzoukas Phil Hay
* Matt Manfredi
}}
| story          = Greg Coolidge
| producer       = {{Plain list | 
* Ice Cube
* Matt Alvarez
* Will Packer
* Larry Brezner
}}
| starring       = {{Plain list | 
* Ice Cube
* Kevin Hart
* John Leguizamo
* Bruce McGill
* Tika Sumpter
* Laurence Fishburne
}}
| music          = Christopher Lennertz
| cinematography = Larry Blanford
| editing        = Craig Alpert
| studio         = Relativity Media Cube Vision Productions Rainforest Films Universal Pictures
| released       =  
| runtime        = 100 minutes  
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $154 million   
}}
 action comedy Phil Hay, and Matt Manfredi wrote the screenplay based on a story originally from Coolidge.

The film follows Ben (Hart), a high school security guard who must prove himself to his girlfriends brother James (Ice Cube) that he is worthy to marry her. James, currently undercover to catch a Serbian smugglers boss Omar (Fishburne), takes Ben on a ride along to prove himself.
 Universal Pictures. American Sniper sequel is scheduled to be released on January 15, 2016.

==Plot==
James Payton is a detective, on an undercover operation where fake passports are being smuggled into Atlanta by a man named "Omar". After a shootout with the smugglers, and a car chase that leaves one injured, the lieutenant leading the case ask James to drop the case on "Omar", something James doesnt do.

Ben Barber, a fast-talking, jumpy high school security guard, applies for the Atlanta City Police Academy. When he gets in, he decides to ask James for his blessings to marry his sister Angela. James then says Ben has to be worthy of Angela, making him go on a "ride along" to prove it.

After picking up Ben and taking him to the police station, James tells dispatch to give him all the 1-26s that are called into the station. James makes Ben take care of all the 1-26s. James then arrives at the park to question an informant Runflat, about a connection of Serbia and "Omar". He finds out that a shipment will come in later that day. After leaving the park, James takes Ben to the local shooting range where he finds out that Zastava M92s have been given to the store, Ben realizes he has a clue for James. When a 1-26 is called in, Ben and James are called to a market where a drunk man named Crazy Cody is being disorderly. Ben tries to subdue him but is unable to, so James arrests him.

Ben asks to be taken home before he receives a call from Angela. He tells her about his stressful day, before Angela says that James plays poker with a guy named Crazy Cody, along with telling him that 1-26s are a code for annoying situations for new-comers as a joke.. He then goes into the police station and sees Cody laughing with James and his partners Santiago and Miggs.

Upset at James, Ben refuses to go home and instead takes a 1-26 call to respond to a disturbance at a strip club, where two men get into a mexican standoff with James and Ben, which Ben thinks its another joke call, so he fools around. James subdues the men, and the two are then tipped off about a gun deal involving Omars men. Ben then confronts James about the 1-26s. James receives a call from Santiago in the car, when he finds out that Runflat had turned himself in, Ben says how Runflats brother in the park said that Runflat had just got out of prison. James then contradicts, saying that Runflat hadnt been in prison for over two years, leading to another clue.

The men go to talk Runflats other brother, J, trying to find out the location of the gun deal, during the confrontation, Ben accidentally shoots J, and finds out that the deal will take place at an old abandoned warehouse at 9 PM.

James decides to infiltrate the warehouse with Santiago and Miggs, after leaving Ben behind in the car. James is then betrayed by Santiago and Miggs, who turn out to be crooked cops working for Omar. James is then tied up, just when Ben witnesses. Santiago not only mocks James, but also criticizes him for his unsociable, egocentric personality. As the deal begins, Ben enters pretending to be Omar (since no one has ever actually seen the real Omar). Ben then wreaks havoc at the deal, and right as Ben is about to leave with James, the real Omar appears. A shootout ensues, and many of Omars men are killed. James and Ben take the money meant for the deal and escape right before the warehouse blows up because of Bens Grenade and destroys Jamess car. Unbeknownst to them, Santiago, Miggs and Omar all escape.

Santiago and Miggs arrive at Angelas apartment and tie her up. As she was playing one of Bens video games, Bens fellow players hear the confrontation ensuing between Angela, Miggs and Santiago from Bens headset. At the hospital after being shot, Ben receives a call from the fellow player, and tells James that theres something going on at the apartment. After seeing some dirty cops arrive at the hospital, James takes Ben and leaves to go to the apartment, along with Omars money. James then injures Miggs, before getting into a fight with Santiago. As Santiago is about to shoot James, Angela knocks him out with a frying pan and Ben get knocked out by Omar, before Omar takes the bag of money and Angela, and leaves the apartment.

James follows Omar and Angela and confronts them. Right as Omar is about to shoot James, Ben slides over a car and kicks him, and James shoots Omar twice, injuring him. Police arrest Omar, Miggs and Santiago. James gives Ben his blessing.

In a mid-credits scene Ben and Angela are engaged, and Ben is weeks away from graduating from the police academy. At a barbecue at James house Ben blows up the barbecue grill and is sent flying back into the bushes, killing the neighbors dog.

==Cast==
{{multiple image
| footer    = Ice Cube stars as James Payton and Kevin Hart stars as Ben Barber in the film.
| image1    = IceCube Toronto2006.jpg
| alt1      = Ice Cube
| width1    =  
| image2    = Kevin Hart 2014.jpg
| alt2      = Kevin Hart
| width2    =  
}}

* Ice Cube as James Payton,  a detective on an undercover operation in Atlanta to catch a smuggler boss named Omar. Cube joined the film in November 2009, to both star and produce.

* Kevin Hart as Ben Barber,  a fast-talking high school security guard who applies for the Atlanta City Police Academy. He goes on a ride along with Payton to prove himself worthy of Paytons sister, Angela. Hart joined the cast in July 2012.

* Tika Sumpter as Angela Payton,  Jamess sister and Bens girlfriend, for whom Ben has to prove himself worthy. Sumpter joined the film in October 2012.

* John Leguizamo as Santiago,  a detective and Miggs partner. Leguizamo joined the film in October 2012.

* Bryan Callen as Miggs,  a detective and Santiagos partner. Callen joined the film in October 2012.

* Laurence Fishburne as Omar,  a boss of Serbian smugglers, who has never been seen by anyone, considers as a ghost. Fishburne was cast in December 2012.

* Bruce McGill as Lt. Brooks,  a lieutenant in Atlanta Police department, and boss of Payton, Santiago, and Miggs.
 Gary Owen Benjamin Flores, Jr. as Morris, Runflats brother.

==Production== Universal Pictures Phil Hay, produced by Will Packer and Larry Brezner.    On October 31, the studio announced the film would be released on January 17, 2014.   

===Casting===
Ice Cube joined the cast on November 29, 2009 to play the lead role as Detective James, a rogue cop who tries to break off his sisters engagement to an upper-crust white psychiatrist by inviting his future brother-in-law on a ride-along.  Kevin Hart joined the cast on July 11, 2012 to play Ben, a high school security guard.  On October 16, John Leguizamo joined the cast of the film to play an undercover cop.    On October 30, Tika Sumpter, Bryan Callen, and Jay Pharoah joined the cast of the film, Sumpter plays Angela, Bens fiancee and James sister while Callen plays an undercover cop and Pharaoh plays a street informant.   
 Gary Owen also joined the cast to co-star with Cube and Hart.    Laurence Fishburne also joined the cast.   

===Filming===
The principal photography on the film began on October 31, 2012 in Atlanta, the crews were filming some scenes at Underground Atlanta on October 31 and November 1.  On October 31, CBS Atlanta posted the warning news that the Atlanta police are warning residents that there will be a simulated gun battle inside the mall area during the filming on Thursday, November 1.  It was a 35-day shoot which wrapped-up filming on December 19, in Atlanta. 

===Music===
On April 29, 2013, Christopher Lennertz was hired to score the film. Lennertz previously collaborated with Story on 2012 comedy Think Like A Man.  The soundtrack was released digitally on January 14, 2014 by Back Lot Music, while a CD version was released on January 28 by Varèse Sarabande. 

{{Infobox album 
| Name        = Ride Along (Original Motion Picture Soundtrack)
| Type        = Film score
| Artist      = Christopher Lennertz
| Cover       = 
| Released    = January 28, 2014
| Recorded    = 2014 Score
| Length      = 43:39
| Producer    = 
| Label       = Back Lot Music
| Chronology  = Christopher Lennertz film scores
| Last album  = Thanks for Sharing (2013)
| This album  = Ride Along (2014)
| Next album  = Think Like a Man Too (2014)
}}
{{Album ratings
}} 
{{Track listing
| total_length = 43:39
| all_music    = Christopher Lennertz (Tracks 1–21) 
| music_credits= 
| writing_credits = yes
| headline     = Ride Along (Original Motion Picture Soundtrack)
| extra_column = Performers
| title1       = Ride Along
| length1      = 0:32
| title2       = Serbian Negotiations
| length2      = 1:18
| title3       = Car Chase
| length3      = 2:08
| title4       = Bens First Ride Along
| length4      = 0:55
| title5       = Police Academy Acceptance
| length5      = 0:46
| title6       = Stranger Danger
| length6      = 3:11
| title7       = Bens Goodbyes
| length7      = 1:27
| title8       = Crazy Cody
| length8      = 2:51
| title9       = Ben Overhears The Prank
| length9      = 2:11
| title10      = Strip Club Drama
| length10     = 3:06
| title11      = Interrogating Jay
| length11     = 1:35
| title12      = Drive To Warehouse
| length12     = 0:51
| title13      = Warehouse Pt. 1
| length13     = 4:24
| title14      = Warehouse Pt. 2
| length14     = 4:05
| title15      = Warehouse Pt. 3
| length15     = 1:52
| title16      = Ben To Hospital
| length16     = 1:34
| title17      = Shootout
| length17     = 3:19
| title18      = Omar At Angelas / James Was Wrong
| length18     = 1:49
| title19      = Angela Held Hostage
| length19     = 1:47
| title20      = Apartment Fight
| length20     = 1:44
| title21      = Omar Shot
| length21     = 2:14
}}

==Promotion and release==
  teaser trailer and an image were released on July 1, 2013.  On September 26, studio revealed the first teaser poster featuring Cube and Hart.  On November 5, eight new images from the set and the poster were revealed.  A second trailer of the film was revealed from studio on November 7.  On December 19, Universal released a full length trailer for the film. 

On the night of January 6, 2014, the films first premiere was held at Atlantic Station in Atlanta.  On January 13, the Los Angeles premiere was held at TCL Chinese Theatre in Hollywood, California.  Following two premieres, the film was released worldwide on January 17, 2014. 

===Box office=== January ahead MLK weekend, the film grossed $48,626,380.   The film held the number one spot at the US box office for three weeks, grossing $21 million and $12 million in its second and third weeks.  The North American domestic gross was $134,202,565, with the international gross $19,059,619, bringing the worldwide total to $153,262,184. 

===Home media===
Ride Along was released on DVD and Blu-ray Disc|Blu-ray on April 15, 2014.  This release included an alternate ending, a gag reel, deleted scenes, a behind-the-scenes documentary and a feature commentary of the film by Story.  In the United States, the film has grossed $13.5 million from DVD sales and $8.7 million from Blu-ray sales, making a total of $22.2 million. 

==Reception==

===Critical response===
Ride Along received generally negative reviews from critics.   gives the film a score of 41 out of 100, based on 34 critics, indicating "mixed or average reviews".  Audiences surveyed by CinemaScore on the opening weekend gave the film an "A grade." 

A negative review came from Steve Pulaski of Influx Magazine, who gave the film a "D grade", stating, "Ride Along is a cut-and-paste, buddy cop actioneer that seems to have assembled its screenplay by taking sentences from the screenplays of other films of the same genre and throwing them in this particular project unaltered. It combines the zealous energy of Kevin Hart with Ice Cube’s trademark meanness, a trait he can exercise when he sleeps, and throws them both into a dead-on-arrival storyline about the usual circumstances of street crime and mobsters with a romantic twist in an attempt to offer narrative leverage."  British critic Mark Kermode describes the film as "An action-comedy short on both action and comedy" and gives it 1 star out of 5. 

Scott Foundas of Variety (magazine)|Variety called the film "a lazy and listless buddy-cop action-comedy that fades from memory as quickly as its generic title."  Entertainment Weeklys film critic Chris Nashawaty gave the film a "C+ grade." 

===Awards and nominations===
  BET Award Best On-Screen Best Comedic Performance for Hart.  On April 13, 2014, after the best-screen-duo award went to Vin Diesel and late Paul Walker for Fast & Furious 6, Ice Cube joked to USA Today that they were robbed. 

{| class="wikitable sortable" width="95%"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards and nominations
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Nominees
! Result
|-
| BET Awards June 29, 2014 Best Actor
| Kevin Hart
|  
|-
| rowspan="2"| MTV Movie Awards April 13, 2014 Best On-Screen Duo
| Ice Cube, Kevin Hart
|  
|- Best Comedic Performance
| Kevin Hart
|  
|}

==Sequel==
 
 On April 23, 2013,  the studio announced that there would be a sequel to the film.  On February 18, 2014, it was announced that after the success of the first Ride Along film, Universal is moving forward with its sequel which Tim Story will direct again.  Ice Cube and Kevin Hart reprise their roles with Will Packer producing and Phil Hay and Matt Manfredi on board again as screenwriters. Ride Along 2 started filming on July 7, 2014 and filming locations include Miami, Florida and Atlanta, Georgia.  Universal has set the film for a release date of January 15, 2016. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 