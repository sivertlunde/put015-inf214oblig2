Athena (film)
{{Infobox film
| name           = Athena
| image          = Athena-film-vhs.jpg
| caption        = VHS cover
| director       = Richard Thorpe
| producer       = Joe Pasternak
| writer         = William Ludwig Leonard Spigelgass
| narrator       = 
| starring       = Jane Powell Edmund Purdom Debbie Reynolds Vic Damone Louis Calhern
| music          = Hugh Martin Ralph Blane
| cinematography = 
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $1,443,000  .  gross = $1,880,000 
| preceded by    = 
| followed by    = 
}}
Athena (1954) is a romantic musical comedy, starring Jane Powell, Edmund Purdom, Debbie Reynolds, Vic Damone, Louis Calhern, and Norma Varden, and released by Metro-Goldwyn-Mayer.

The film tells the story of an old-fashioned conservative lawyer who falls in love with a daughter from a family of fitness fanatics.

==Plot==
Conservative lawyer Adam Calhorn Shaw (Edmund Purdom) hopes to be elected to office, like his father, and his fathers father. He is engaged to a sophistocated society lady, Beth Hallson (Linda Christian).
 teetotallers who follow astrology and numerology.

Athena offers to give him advice on how to mulch the peach trees, however, Adam is uneasy, and leaves. Later, at a party, Athena arrives, mulches Adams peach trees, kisses him, and announces her intention to marry him. She also decides, after a numerological calculation, that Adams friend Johnny Nyle (Vic Damone) would be perfect for her sister, Minerva (Debbie Reynolds).

Athena returns to Adams house the next morning to the shock of Adams fiancee. Adam promises to tell Athena that he has no romantic interest in her, but finds she has left. He asks his legal secretary Miss Seely to search for her but to no avail. Eventually Johnny returns and tells Adam that Athenas family owns a health food store, and that he can find her there.

That night when Adam goes to the house, he meets the meditating Grandma Salome, Minerva and Athenas 5 other beautiful, singing and dancing sisters, Niobe, Aphrodite, Calliope, Medea and Ceres. He also meets the bodybuilders that the girls grandfather Louis Calhern) has been training for the Universe Championships|Mr. Universe competition, Ed Perkins and Bill Nichols.

Despite the bizarre ways of the family, and although Adam initially tries to resist Athena, he eventually succumbs to her charms, and breaks up with Beth. Just when all is looking rosy, Grandma foresees difficult times ahead. Athenas sisters advise Athena to break up with Adam, however Athena chooses to push ahead with the relationship, knowing that "love can change the stars".

The sisters visit Adams house while he is out and perform a makeover, removing rugs and screens and installing large urns and fresh flowers. Adams influential family friend, Mr. Grenville, Adams law firm partner, Mr. Griswalde, and Adams campaign manager for election to the United States Congress, Mr. Tremaine, phone Adams house and reach Athena on the phone. Curious, they visit Adams house only to find Grandma there in place of the girls.

Adam invites Athena to a formal reception at Mr. Grenvilles home. Athena at first charms the party with her pleasant nature and an off-the-cuff rendition of an aria from a Donizetti Opera. However she loses her temper when Beth presents Athena with a buffet dinner where all of the vegetables are stuffed with meat.
 jiu jitsu throw with both events appearing on nationwide television.  Adam is told by his minders that his political career is over by his embarrassing the belief system of Athenas people that would alienate voters with those sympathies whilst those opposed to their beliefs would associate Adam with holding their beliefs by merely being with them.

Despite requisite further conflict, harmony is restored and all of the main players gather around for a Mulvain-style feast.

==Songs==
The film features several songs by Hugh Martin and Ralph Blane including 

*"Athena" (Chorus) The boy next door" from Meet Me in St. Louis|"Meet me in St Louis"
*"Vocalize" (Jane Powell) which is reprised later in a choral arrangement as "Harmonize" (Jane Powell, Louis Calhern, Chorus)
*"Imagine" (Debbie Reynolds, Vic Damone)
*"Love can change the Stars" (Jane Powell, Sisters, Vic Damone)
*"Never felt better" (Debbie Reynolds, Jane Powell, Sisters)
*"Venezia" (Vic Damone) La fille du régiment (Daughter of the Regiment) (Jane Powell)

==Cast==
*Jane Powell as Athena Mulvain
*Edmund Purdom as Adam Calhorn Shaw
*Debbie Reynolds as Minerva Mulvain
*Vic Damone as Johnny Nyle
*Louis Calhern as Ulysses Mulvain
*Linda Christian as Beth Hallson
*Evelyn Varden as Salome Mulvain Ray Collins as Mr Tremaine
*Carl Benton Reid as Mr Griswalde
*Howard Wendell as Mr Grenville
*Virginia Gibson as Niobe
*Henry Nakamura as Roy
*Nancy Kilgas as Aphrodite
*Dolores Starr as Calliope
*Jane Fischer as Medea
*Cecile Rogers as Ceres
*Kathleen Freeman as Miss Seely
*Steve Reeves as Ed Perkins
*Richard Sabre as Bill Nichols

==Production==
Director Dick Thorpe was less than enthusiastic about the picture. After a scene was finished, he would toss the pages of the script over his shoulder and walk away. In her autobiography, Jane Powell said that it really discouraged the cast. Powell also said that the film would have been better received had it been made twenty years later.   

===Casting===
In 1953, MGM swimming star Esther Williams left for maternity leave. Before her departure, she had assumed she would appear in Athena, when she returned, as she had helped create the premise with writers Leo Pogostin and Chuck Walters.     However, the studio changed the main character from a swimmer to a singer, and Powell was cast instead. Janet Leigh and Vera-Ellen were also cast in the film, but dropped out. 

==Reception==
According to MGM records the film made $1,222,000 in the US and Canada and $658,000 elsewhere resulting in a loss of $511,000. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 