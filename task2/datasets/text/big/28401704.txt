Hindle Wakes (1927 film)
 
 
{{Infobox film
| name           = Hindle Wakes
| image          = Hindlewakes1927.jpg
| caption        = Region 1 DVD cover
| director       = Maurice Elvey
| producer       = Victor Saville
| writer         = Victor Saville Stanley Houghton John Stuart
| cinematography = Jack E. Cox
| editing        = Gareth Gundry
| distributor    = Gaumont British
| released       =  
| runtime        = 115 minutes
| country        = United Kingdom
}}
 John Stuart. Mademoiselle from Armentieres.

In its time, Houghtons play was considered extremely controversial and provocative in its message. It is seen as proto-feminist in tone, with its assumption that women as well as men could enjoy a brief sexual fling for what it was, without any sense of obligation on either side, and further that a woman was capable of making her own decisions, ignoring familial and societal strictures if necessary.  Hindle Wakes was filmed four times, twice as a silent (the first version, also directed by Elvey, made in 1918) and twice in sound (in 1931 and 1952). The 1927 production was well-budgeted, made extensive use of location filming in Manchester and Blackpool, and is generally held by film historians to be the best of the four.

==Plot==
In the Lancashire mill town of Hindle, preparations are being made for the annual summer wakes week holiday. Fanny Hawthorn (Brody) is seen packing her suitcase in preparation for her trip to Blackpool with her friend Mary Hollins (Peggy Carlisle). Meanwhile Allan Jeffcote (Stuart), son of the owner of the mill in which Fanny works, and employed in the offices, has had his own holiday plans disrupted due to his fiancée having to cancel their arrangements at the last minute. After a final days work, the factory hooter sounds and Fanny and Mary board the excursion train to Blackpool, while Allan and a friend decide to travel there by car.

In the bustle and throng of Blackpool in peak season, Fanny and Mary meet up with Allan and his friend and enjoy the excitement of the resort as a foursome. Allan and Fanny are attracted to each other, and Allan persuades Fanny to leave Blackpool and instead accompany him for a stay in the more upmarket resort of Llandudno in North Wales. Knowing what this entails, Fanny agrees and writes a postcard to her parents, which Mary promises to post from Blackpool later in the week.

Soon after, Mary is tragically killed in a boating accident. When Fannys father hears the news he travels to Blackpool, only to find Fanny not there, and the unmailed postcard in Marys luggage. At the end of the week, Fanny and Allan return separately to Hindle, where Fanny, previously unaware of Marys death and shocked by the news, is interrogated by her parents and reveals that she has spent the week with Allan in Llandudno. In indignation, the Hawthorns go to the Jeffcote home and confront Allans parents with his caddish behaviour. To their surprise, they find Allans father equally appalled by the situation. Mr. Jeffcote determines that Allan must marry Fanny to prevent a scandal.

Allan initially opposes his fathers demand but explains the situation to his fiancée, who insists that in the circumstances the only decent thing for him to do is to comply with the insistence that he marry Fanny after having compromised her reputation. That evening the Hawthorns visit the Jeffcotes to make arrangements for the marriage. Fanny registers her defiance by refusing to dress up and insisting on wearing her working clothes. Allan makes a formal offer of marriage, and to everyones amazement Fanny turns him down flat, saying that she is just as entitled to enjoy a "little fancy" as any man.

Allan and his fiancée resume their engagement, while Fanny moves out of the family home to get away from the wrath of her mother. She strikes up a friendship with a fellow mill worker, and agrees to a date with him.

==Cast==
* Estelle Brody as Fanny Hawthorn John Stuart as Allan Jeffcote
* Norman McKinnel as Nathaniel Jeffcote
* Irene Rooke as Mrs. Jeffcote
* Marie Ault as Mrs. Hawthorn
* Humberston Wright as Chris Hawthorn
* Arthur Chesney as Sir Timothy Farrar
* Gladys Jennings as Beatrice Farrar
* Alf Goddard as Nobby
* Cyril McLaglen as Alf
* Peggy Carlisle as Mary Hollins

==Reputation==
Hindle Wakes proved successful with audiences and critics on its release, and latterly has become much admired as belonging in the top rank of British silent films. Its skilful use of location is considered to give the film a documentary realism feel very unusual in British films of the period, in many ways decades ahead of its time in foreshadowing the kitchen sink realism of the 1950s and 1960s. In an essay on Elveys career, Lawrence Napper described it as "a particularly successful example of Elveys blend of realism, melodrama and sense of location."  Brodys performance is also admired for its naturalness and spontaneity, and is strikingly modern in execution. An analysis of the film by the British Film Institute comments: "Fanny makes an enduring heroine. She is a modern miss and, eighty years on, her attitudes and behaviour stand up well." 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 