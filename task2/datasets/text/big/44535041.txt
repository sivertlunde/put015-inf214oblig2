Rama Parushurama
{{Infobox film 
| name           = Rama Parashurama
| image          =  
| caption        = 
| director       = Vijay
| producer       = Smt Gowramma Somashekar Smt Dhanalakshmi Vijay
| story          = Pasumani
| writer         = Chi. Udaya Shankar (dialogues)
| screenplay     = M. D. Sundar Vishnuvardhan Srinath Manjula Thoogudeepa Srinivas
| music          = Rajan-Nagendra
| cinematography = P. Devaraj
| editing        = P. Bhakthavathsalam
| studio         = Vijay Shekar Movies
| distributor    = Vijay Shekar Movies
| released       =  
| runtime        = 140 min
| country        = India Kannada
}}
 1980 Cinema Indian Kannada Kannada film, Manjula and Thoogudeepa Srinivas in lead roles. The film had musical score by Rajan-Nagendra.  

==Cast==
  Vishnuvardhan
*Srinath Manjula
*Thoogudeepa Srinivas
*Dinesh
*Shakthi Prasad
*Prasad
*Rajanand
*Ramakrishna
*Comedian Guggu
*Adavani Lakshmidevi
*Lakshmishree
*Lalithamma
*Shanthamma
*Sripramila
*Roopashree
*Master Rajesh
*Master Arun
*Baby Rekha
*Mallesh
*Bhatti Mahadev
*Shivaprakash
*Sharapanjara Iyengar
*Thipatur Siddaramaiah
*Police Mahadev
*Kunigal Ramanath
*Tiger Prabhakar
 

==Soundtrack==
The music was composed by Rajan-Nagendra. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Chi. Udaya Shankar || 04.32
|- Janaki || Chi. Udaya Shankar || 04.34
|-
| 3 || Yelli Neeno Alli Naanu || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.13
|- Janaki || Chi. Udaya Shankar || 04.13
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 