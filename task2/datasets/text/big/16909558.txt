The Princess from Hoboken
 
{{Infobox film
| name           = The Princess from Hoboken
| image          =
| caption        =
| director       = Allen Dale
| producer       =
| writer         = Sonya Levien
| starring       = Edmund Burns Blanche Mehaffey
| music          = Robert Martin
| editing        = James C. McKay
| distributor    = Tiffany Pictures
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 silent comedy film directed by Allen Dale and featuring Boris Karloff.  

==Cast==
* Edmund Burns as Terence OBrien
* Blanche Mehaffey as Sheila OToole
* Ethel Clayton as Mrs. OBrien
* Lou Tellegen as Prince Anton Balakrieff
* Babe London as Princess Sonia Alexandernova Karpoff
* Will Walling as Mr. OBrien (as Will R. Walling)
* Charles McHugh as Pa OToole
* Aggie Herring as Ma OToole
* Charles Crockett as Whiskers
* Robert Homans as McCoy
* Harry A. Bailey as Cohen (as Harry Bailey)
* Sidney DAlbrook as Tony
* Broderick OFarrell as Immigration Officer
* Boris Karloff as Pavel

==Preservation status==
This is now considered a lost film. 

==See also==
* Boris Karloff filmography
* List of lost films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 