Seeta (1960 film)
{{Infobox film
| name = Seetha
| image =Seetha 1960.jpg
| caption =
| director = Kunchacko
| producer = M Kunchacko
| writer = J. Sasikumar (dialogues)
| screenplay = J. Sasikumar Hari
| music = V. Dakshinamoorthy
| cinematography =
| editing =
| studio = Udaya
| distributor = Udaya
| released =   
| country = India Malayalam
}}
 1960 Cinema Indian Malayalam Malayalam film, Hari in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
*Prem Nazir as Sreeraman 
*Kushalakumari as Seetha 
*Thikkurissi Sukumaran Nair as Gurudevan Vaalmeeki Hari as Lavan
*T. R. Omana as Malini
*Boban Kunchacko as Lakshmanan
*J. Sasikumar as Vasishtar
*Kanchana as Kausalya
*S. P. Pillai as Mooshakan

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Abhayadev. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kaanmu Njan || PB Sreenivas || Abhayadev || 
|- 
| 2 || Kanne Nukaru Swargasukham || ML Vasanthakumari || Abhayadev || 
|- 
| 3 || Lankayil Vaana || PB Sreenivas || Abhayadev || 
|- 
| 4 || Mangalam Neruka || S Janaki, Chorus || Abhayadev || 
|- 
| 5 || Nerampoyi Nadanada || V. Dakshinamoorthy, Jikki || Abhayadev || 
|- 
| 6 || Paattupaadiyurakkaam || P Susheela || Abhayadev || 
|- 
| 7 || Paavana Bharatha || PB Sreenivas, A. M. Rajah || Abhayadev || 
|- 
| 8 || Prajakalundo Prajakalundo || PB Sreenivas, A. M. Rajah, Jikki, Punitha || Abhayadev || 
|- 
| 9 || Raamaraajyathinte || A. M. Rajah, Chorus || Abhayadev || 
|- 
| 10 || Rama Rama || A. M. Rajah, Chorus || Abhayadev || 
|- 
| 11 || Seethe Lokamaathe || PB Sreenivas || Abhayadev || 
|- 
| 12 || Unni Pirannu || A. M. Rajah, Chorus || Abhayadev || 
|- 
| 13 || Veene Paaduka || P Susheela || Abhayadev || 
|}

==References==
 

==External links==
*  

 
 
 


 