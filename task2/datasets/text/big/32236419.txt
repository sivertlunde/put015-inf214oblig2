Mera Faisla
 
{{Infobox film
| name = Mera Faisla
| image = 
| writer = Kader Khan
| starring = Sanjay Dutt Rati Agnihotri Jayapradha Kader Khan
| director = Rajendra Singh Babu
| producer = Ranjit Virk
| music = Laxmikant-Pyarelal
| lyricist = Anand Bakshi
| released = 6 April 1984
| language = Hindi
}}

Mera Faisla is an Indian film directed by Rajendra Singh Babu and released in 1984. The movie stars Sanjay Dutt, Rati Agnihotri, Jayapradha, Kader Khan. 

==Characters==
* Sanjay Dutt ... Raj Saxena
* Rati Agnihotri ... Rati Verma
* Jayapradha ... Nisha Dhawan
* Kader Khan... Jacob
* Shakti Kapoor... Tony
* Nirupa Roy... Mrs. Saxena (Rajs dadimaa)
* Pran (actor)|Pran... Commissioner Rana
* Pinchoo Kapoor ... Major Verma
* Satyen Kappu ... Dhawan
* Shobha Khote ... Hostel Warden
* G. Asrani

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Allah Meri Laaj Bachana"
| Mahesh Kumar
|-
| 2
| "Idhar Ho Raha Hai"
| Shabbir Kumar, Asha Bhosle
|-
| 3
| "Mera Ek Diwana Mujhe"
| Suresh Wadkar, Asha Bhosle
|-
| 4
| "Mere Yaron Mere Aane Ki"
| Shailender Singh
|-
| 5
| "Photographer Jaldi Kar"
| Asha Bhosle
|-
| 6
| "Music (Mera Faisla)"
|
|}
==External links==
*  

 

 
 
 
 

 