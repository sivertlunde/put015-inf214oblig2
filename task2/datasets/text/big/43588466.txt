Beyond the Reef (film)
{{Infobox film
| name           = Beyond the Reef 
| image          = Beyond the Reef poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Frank C. Clarke 
| producer       = Raffaella De Laurentiis
| screenplay     = Louis LaRusso II James Carabatsos 
| based on       =  
| starring       = Dayton Kane Maren Jensen Kathleen Swan Keahi Farden Oliverio Maciel Diaz George Tapare David Nakuna Robert Atamu Bob Spiegel 
| music          = Francis Lai
| cinematography = Sam Martin 
| editing        = Ian Crafford 
| studio         = Dino De Laurentiis Company
| distributor    = Universal Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $547,525 
}}

Beyond the Reef  is a 1980 American adventure film directed by Frank C. Clarke and written by Louis LaRusso II and James Carabatsos. The film stars Dayton Kane, Maren Jensen, Kathleen Swan, Keahi Farden, Oliverio Maciel Diaz, George Tapare, David Nakuna, Robert Atamu and Bob Spiegel. 

==Plot==
Tikoyo lives by the Bora Bora lagoon in the South Pacific, where he found a man-eating tiger shark when it was an orphaned baby, just a foot long. He named it Manidu, and it grew to be sixteen feet long. Tikoyo believes that his companion houses the soul of a wise old man, and helps him guard the sacred black pearls in the lagoon. In fact, Manidu does protect Tikoyo and his girlfriend Diana, only eating those who are their enemies, or who seek to destroy their environment, or steal the pearls.

== Cast ==		
*Dayton Kane as Tikoyo
*Maren Jensen as Diana
*Kathleen Swan as Milly
*Keahi Farden as Jeff
*Oliverio Maciel Diaz as Manidu
*George Tapare as The Hawaiian
*David Nakuna as Mischima
*Robert Atamu as Maku
*Bob Spiegel as Turpin
*Maui Temaui as Millys Boyfriend
*Teriitehu Star as Grandfather
*Joseph Kane as Tikoyo as a child
*Titaua Castel as Diana as a child
*Andre Garnier as Jeff as a child 

== References ==
 

== External links ==
*  

 
 
 
 
 