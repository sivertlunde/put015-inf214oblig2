Renegade Riders
{{Infobox film
| name = Renegade Riders Sette winchester per un massacro
| image = Renegade Riders.jpg
| director = Enzo G. Castellari
| writer =
| starring =
| music = Francesco De Masi
| cinematography = Aldo Pinelli
| editing =
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language =
| budget =
}} 1967 Cinema Italian spaghetti western film. It represents the official film debut for director Enzo G. Castellari (here credited E.G. Rowland), after his uncredited co-direction of Few Dollars for Django.   

==Plot==
Following the American Civil War a bunch of deserted Confederate soldiers dwell in Mexico and make a living on terrorising the villages around. Bounty hunter Stuart and his assistant Manuela shall put a crackdown on these misdeeds.

== Cast ==
* Edd Byrnes – Stuart
* Guy Madison – Colonnello Thomas Blake
* Ennio Girolami – Chamaco Gonzales (as Thomas Moore)
* Luisa Baratto – Manuela (as Louise Barrett)
* Federico Boido – Fred (as Ryk Boyd)

==Releases==
Wild East released this alongside Red Blood, Yellow Gold in an out-of-print limited edition R0 NTSC DVD in 2008. 

==References==
 

==External links==
* 

 

 
 
 
 
 


 
 