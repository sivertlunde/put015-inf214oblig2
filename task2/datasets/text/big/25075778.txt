The Passage (2007 film)
 

{{Infobox film
| name           = The Passage
| image          = The Passage (2007 film).jpg
| caption        = DVD cover
| director       = Mark Heller
| producer       = Jerry Mackintosh Stephen Dorff Mark Heller
| writer         = Neil Jackson
| starring       = Neil Jackson Stephen Dorff Sarai Givaty Abdel Ghani Benizza Khalid Benchagra Nick Dunning  Joaquín Cosío  Neil Fluellen
| music          = Timothy Williams (composer)
| cinematography = Belle Rose
| editing        = Jerry Mackintosh
| studio         = Mark Heller Films
| distributor    = Universal Studios
| released       =  
| runtime        = 109 minutes
| country        = United States, Israel  English and Riffian
| budget         = 
| gross          = 
}}

The Passage is a 2007 thriller-horror film directed by Mark Heller and starring a large ensemble cast that includes Stephen Dorff, Neil Jackson, Abdel Ghani Benizza, Khalid Benchagra, Nick Dunning, and Neil Fluellen. The film revolves around an American man falling for a Moroccan woman. They find out that their romance can put them in danger, so they seek out the help of people they know. Filming started in Morocco on October 1, 2006. It was released in the United States on March 18, 2007 and was released on DVD on September 5, 2007.

==Plot==
Two friends, Luke (Stephen Dorff) and Adam (Neil Jackson) travel to Morocco with the intention to relax and have a party. Adam thinks its the best way for Luke to get over the death of Lukes girlfriend. While Adam prefers night life and parties, Luke writes a diary and takes numerous photographs of the Morocco and Moroccan way of life.
During one of the walks across the city, Luke meets a Moroccan girl Zahra (Sarai Givaty) who offers him the help as a tourist guide. They are immediately attracted to each other although they need to be careful not to violate local customs which forbid local women to have any close contacts with the foreigners.

Zahra recognizes Lukes interest in exploring Morocco and invites him and Adam to a two-day trip to a remote village in the Atlas Mountains to show Luke some sights which are unseen by most tourists. However, Adam decides to join them the next day. 
When Luke and Zahra arrive to the village (previously a skiing center), they go to the only hotel left but are turned away by the angry receptionist who does not allow unmarried couples to stay overnight. They are forced to find another location to spend the night and they find a local man who offers them to spend the night in one of his cabins. They gladly accept his offer and he takes them to the cabin.

During the night Luke discovers a hidden tunnel which spreads into the labyrinth of similar tunnels. Curious to find out the purpose of those tunnels he finds out that these tunnels connect all cabins in the village. Later Zahra awakes and follows Luke and they explore the tunnel network further. They follow a strange noise and discover a room with the operating table and a chest freezer full of stainless steel boxes. Luke opens one of the boxes and becomes frightened (although the content of the box is not revealed). He and Zahra try to reach their cabin but find out that somebody has picked up all candles Luke used to navigate the dark tunnels. Finally they enter another cabin whose door is locked. While Luke tries to open the door, somebody grabs Zahra and drags her to the tunnels. Trying to find her, Luke is ambushed by a man who knocks him down unconscious.

The next morning Adam takes a bus trip to the village to join Luke and Zahra. When he arrives there he tries to find his friends but without success. He is approached by a man who offered the cabin to Luke and Zahra. Adam recognizes him as Hossef, an acquaintance of Adam who invited him to the village before, although Adam refused to go that time. He takes Adam to the cabin where Luke and Zahra spent the night. Once there, Adam finds Zahra who is frightened but Luke has disappeared. Suddenly Hossef grabs a lever and tries to hit Adam but Adam manages to avoid the strike and subdues Hossef. Together with Zahra they enter the tunnels in an attempt to find Luke. They again enter the room that Luke and Zahra found before and terrified Adam sees Lukes corpse lying on the operating table. It is revealed that the room is used for the illegal human organ harvesting and Zahras role is to lure naive men into the village where they can be kidnapped and their organs taken. Adam is knocked down and regains consciousness tied up to the operating table with the team of surgeons (including Zahra) preparing the surgical tools. Adam asks why he and Luke were kidnapped and a flashback reveals the past events which were staged to lure them into a trap - including a young girl who cuts Luke and a woman who scratches Adam during sex with the purpose of getting their blood samples.

The film ends with Zahra approaching an English tourist, Chris, whom she offers some help like she did to Luke before. One of the stainless steel boxes with human organs is packed into the car and sent to the private hospital somewhere in the west.

==References==

 

==External links==
*  

 
 
 