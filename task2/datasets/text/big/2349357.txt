If Footmen Tire You, What Will Horses Do?
{{Infobox name = If Footmen Tire You, What Will Horses Do?
| image          = If footmen.jpg
| caption        = DVD cover art
| director       = Ron Ormond
| producer       = Estus Pirkle Monnie Stanfield
| writer         = Screenplay: Ron Ormond Book: Estus Pirkle
| starring       = Estus Pirkle Cecil Scaife Judy Creech
| music          =
| cinematography =
| editing        = Ron Ormond Tim Ormond
| distributor    = The Ormond Organization
| released       = 1972
| runtime        = 52 minutes
| country        = USA
| language       = English
| budget         =
}}
If Footmen Tire You, What Will Horses Do? is a 1971 Christian film directed by Ron Ormond.

The film is based on the teachings of Estus Pirkle and warns of the dangers facing the United States from Communist infiltrators. The film suggests that the only way to avoid such a fate is to turn to Christianity. It has attracted something of a cult following among secular fans because of its explicit depictions of torture and the heavy-handed nature of its evangelical message. The title paraphrases  :
:"If you have run with footmen and they have tired you out, Then how can you compete with horses? If you fall down in a land of peace, How will you do in the thicket of the Jordan?"
 sampled by Jail Bait. Pirkles narrative includes a (real or speculative) experience in a communist totalitarian country where public loudspeakers issued proclamations such as "Christianity is stupid -- Communism is good" ad infinitum. Negativland lifted the phrases and played them repeatedly backed by industrial music and various other sound effects. A more complete version of Pirkles narrative can be heard on Negativlands Helter Stupid.

==See also==
*The Burning Hell

==External links==
* 
* 
*  of the film
*  on which the film was based
* 

 
 
 
 
 
 

 
 