Atlas Shrugged: Part I
 
{{Infobox film
| name           = Atlas Shrugged: Part I
| image          = Atlas Shrugged film poster.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Paul Johansson
| producer       = John Aglialoro Harmon Kaslow
| screenplay     = John Aglialoro Brian Patrick OToole 
| based on       =  
| starring       = Taylor Schilling Grant Bowler
| music          = Elia Cmiral
| cinematography = Ross Berryman
| editing        = Jim Flynn Sherril Schlesinger
| studio         = The Strike Productions
| distributor    = Rocky Mountain Pictures
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $20 million      
| gross          = $4,627,375 
}} first film of a trilogy encompassing the entire book. After various treatments and proposals floundered for nearly 40 years,  investor John Aglialoro initiated production in June 2010. The film was directed by Paul Johansson and stars Taylor Schilling as Dagny Taggart and Grant Bowler as Hank Rearden.

The film begins the story of Atlas Shrugged, set in a dystopian United States where John Galt leads innovators, from industrialists to artists, in a capital strike, "stopping the motor of the world" to reassert the importance of the free use of ones mind and of laissez-faire capitalism.   

A sequel film,   was released on October 12, 2012.  The third part in the series,   was released on September 12, 2014. 

==Plot== James Taggart shirks responsibility. His sister Dagny Taggart, Vice-President in Charge of Operation, defies him by replacing the aging track with new rails made of Rearden Metal, which is claimed to be lighter yet stronger than steel.  Dagny meets with its inventor, Hank Rearden, and they negotiate a deal they both admit serves their respective self-interests.
 playboy grown bored with the pursuit of money. He reveals that a series of copper mines he built are worthless, costing his investors (including the Taggart railroad) millions.

Rearden lives in a magnificent home with a wife and a brother who are happy to live off his effort, though they overtly disrespect it. Reardens anniversary gift to his wife Lillian is a bracelet made from the first batch of Rearden Metal, but she considers it a garish symbol of Hanks egotism. At a dinner party, Dagny dares Lillian to exchange it for Dagnys diamond necklace, which she does.

As Dagny and Rearden rebuild the Rio Norte line, talented people quit their jobs and refuse all inducements to stay. Meanwhile, Dr. Robert Stadler of the State Science Institute puts out a report implying that Rearden Metal is dangerous. Taggart Transcontinental stock plummets because of its use of Rearden Metal, and Dagny leaves Taggart Transcontinental temporarily and forms her own company to finish the Rio Norte line. She renames it the John Galt Line, in defiance of the phrase "Who is John Galt?"—which has come to stand for any question to which it is pointless to seek an answer.

A new law forces Rearden to sell most of his businesses, but he retains Rearden Steel for the sake of his metal and to finish the John Galt Line. Despite strong government and union opposition to Rearden Metal, Dagny and Rearden complete the line ahead of schedule and successfully test it on a record-setting run to Wyatts oil fields in Colorado. At the home of Wyatt, now a close friend, Dagny and Rearden celebrate the success of the line. As Dagny and Rearden continue their celebration into the night by fulfilling their growing sexual attraction, the shadowy figure responsible for the disappearances of prominent people visits Wyatt with an offer for a better society based on personal achievement.

The next morning, Dagny and Rearden begin investigating an abandoned prototype of an advanced motor that could revolutionize the world. They realize the genius of the motors creator and try to track him down. Dagny finds Dr. Hugh Akston, working as a cook at a diner, but he is not willing to reveal the identity of the inventor; Akston knows whom Dagny is seeking and says she will never find him, though he may find her.

Another new law limits rail freight and levies a special tax on Colorado. It is the final straw for Ellis Wyatt. When Dagny hears that Wyatts oil fields are on fire, she rushes to his home but finds a handwritten sign that reads, "I am leaving it as I found it. Take over. Its yours."

Wyatt declares in an answering machine message that he is "on strike".

==Cast==
 
* Taylor Schilling as Dagny Taggart
* Grant Bowler as Henry "Hank" Rearden
* Matthew Marsden as James Taggart
* Graham Beckel as Ellis Wyatt
* Edi Gathegi as Edwin "Eddie" Willers
* Jsu Garcia as Francisco Domingo Carlos Andres Sebastian dAnconia Michael Lerner as Wesley Mouch
* Jack Milo as Richard McNamara
* Ethan Cohn as Owen Kellogg
* Rebecca Wisocky as Lillian Rearden
* Christina Pickles as Mother Rearden
* Neill Barry as Philip Rearden
* Patrick Fischler as Paul Larkin
* Sylva Kelegian as Ivy Starnes
* Jon Polito as Orren Boyle
* Michael OKeefe as Hugh Akston
* Geoff Pierson as Midas Mulligan
* Armin Shimerman as Dr. Potter
* Paul Johansson as John Galt (only in Part I as silhouetted figure wearing a trenchcoat and fedora)    

==Production==

===Development=== Susan Black, Bill Collins.   Rand insisted on having final script approval, which Ruddy refused to give her, thus preventing a deal. In 1978, Henry and Michael Jaffe negotiated a deal for an eight-hour Atlas Shrugged television miniseries on NBC. Jaffe hired screenwriter Stirling Silliphant to adapt the novel and he obtained approval from Rand on the final script. However, in 1979, with Fred Silvermans rise as president of NBC, the project was scrapped.   
 option to Michael Jaffe and Ed Snider. Peikoff would not approve the script they wrote and the deal fell through. In 1992, investor John Aglialoro bought an option to produce the film, paying Peikoff over $1 million for full creative control. 
 Howard and Crusader Entertainment, Michael Burns of Lions Gate Entertainment approached the Baldwins to fund and distribute Atlas Shrugged.  A two-part draft screenplay written by James V. Hart    was re-written into a 127–page screenplay by Randall Wallace, with Vadim Perelman expected to direct.    Potential cast members for this production had included Angelina Jolie,  Charlize Theron,  Julia Roberts,    and Anne Hathaway.  Between 2009 and 2010, however, these deals came apart, including studio backing from Lions Gate, and therefore none of the stars mentioned above appear in the final film. Also, Wallace did not do the screenplay, and Perelman did not direct.   Aglialoro says producers have spent "something in the $20 million range" on the project over the last 18 years. 

===Writing===
In May 2010, Brian Patrick OToole and Aglialoro wrote a screenplay, intent on filming in June 2010. While initial rumors claimed that the films would have a "timeless" setting—the producers say Rand envisioned the story as occurring "the day after tomorrow" Atlas Shrugged DVD, Directors commentary audio channel —the released film is set in late 2016.  The writers were mindful of the desire of some fans for fidelity to the novel,  but gave some characters, such as Eddie Willers, short shrift and omitted others, such as the composer Richard Halley.  The film is styled as a mystery, with black-and-white freeze frames as each innovator goes "missing".  However, Galt appears and speaks in the film, solving the mystery more clearly than in the first third of the novel.

===Casting===
Though director Johansson had been reported as playing the pivotal role of John Galt, he made it clear in an interview that with regard to who is John Galt in the film, the answer was, "Not me."  He explained that his portrayal of the character would be limited to the first film as a silhouetted figure wearing a trenchcoat and fedora,    suggesting that another actor will be cast as Galt for the subsequent parts of the trilogy.

===Filming===
Though Stephen Polk was initially set to direct,  he was replaced by Paul Johansson nine days before filming was scheduled to begin. With the 18-year-long option to the films rights set to expire on June 15, 2010, producers Harmon Kaslow and Aglialoro began principal photography on June 13, 2010, thus allowing Aglialoro to retain the motion picture rights. Shooting took five weeks, and he says that the total production cost of the movie came in on a budget around US$10 million,  though Box Office Mojo lists the production cost as $20 million. 

===Score===
Elia Cmiral composed the score for the film.  Peter Debruge wrote in Variety that "More ambitious sound design and score, rather than the low-key filler from composer Elia Cmiral and music supervisor Steve Weisberg, might have significantly boosted the pics limited scale." 

===Marketing===
{{Quote box Tea Party. Republicans and Democrats who felt rejected by the establishment, and the same process is going to happen with Atlas Shrugged: Were going to build a constituency of people who believe in limited government and individual liberty.
| source = — , March 23, 2011 
 | width = 32%
 | align = right
}}

The film had a very low marketing budget and was not marketed in conventional methods. Persall, Steve.  , The St. Petersburg Times.  Prior to the films release on the politically symbolic date of Tax Day, the project was promoted throughout the Tea Party movement and affiliated organizations such as FreedomWorks.  The National Journal reported that FreedomWorks, the Tea Party-allied group headed by former House Majority Leader Dick Armey, (R-Texas), had been trying to get the movie opened in more theaters.  FreedomWorks also helped unveil the Atlas Shrugged movie trailer at the February 2011 Conservative Political Action Conference.  Additionally, it was reported that Tea Party groups across the country were plugging the movie trailer on their websites and Facebook pages.  Release of the movie was also covered and promoted by Fox News TV personalities John Stossel and Sean Hannity.  

==Release and reception==

===Box office===
The U.S. release of Atlas Shrugged: Part I was regarded as a "flop".    It opened on 300 screens on April 15, 2011, and made US$1,676,917 in its opening weekend, finishing in 14th place overall.  Producers announced expansion to 423 theaters several days after release and promised 1,000 theaters by the end of April,  , Atlas Productions Press Release  but the release peaked at 465 screens. Ticket sales dropped off significantly in its second week of release, despite the addition of 165 screens; after six weeks, the film was showing on only 32 screens and total ticket sales had not crossed the $5 million mark, recouping less than a quarter of the production budget.   

===Home media===
Atlas Shrugged: Part I was released on DVD and Blu-ray Disc on November 8, 2011 by 20th Century Fox Home Entertainment.  More than 100,000 DVD inserts were recalled within days due to the jackets philosophically incorrect description of "Ayn Rands timeless novel of courage and self-sacrifice".  As of April, 2013, 247,044 DVDs had been sold, grossing $3,433,445. 

===Critical response===
The film received overwhelmingly negative reviews.   gives the film a "generally unfavorable" rating of 28%, as determined by averaging 19 professional reviews.  Some commentators noted differences in film critics reactions from audience members reactions; from the latter group, the film received high scores even before the film was released.   

{{Quote box
| quote  = Lets say you know the novel, you agree with Ayn Rand, youre an objectivist or a libertarian, and youve been waiting eagerly for this movie. Man, are you going to get a letdown. Its not enough that a movie agree with you, in however an incoherent and murky fashion. It would help if it were like, you know, entertaining?
| source = —Roger Ebert, Chicago Sun-Times, April 14, 2011 
 | width = 32%
 | align = right
}}
 Geraldo Rivera broke into Al Capones vault."    Columnist Cathy Young of The Boston Globe gave the film a negative review.  Chicago Tribune published a predominantly negative review, arguing that the film lacks Rands philosophical theme, while at the same time saying "the actors, none of them big names, are well-suited to the roles. The story has drive, color and mystery. It looks good on the screen."  In the New York Post, Kyle Smith gave the film a mostly negative review, grading it at 2.5/4 stars, criticizing its "stilted dialogue and stern, unironic hectoring" and calling it "stiff in the joints", but also adding that it "nevertheless contains a fire and a fury that makes it more compelling than the average mass-produced studio item." 
 Fred Barnes Jack Hunter, contributing editor to The American Conservative, wrote, "If you ask the average film critic about the new movie adaptation of Ayn Rands Atlas Shrugged they will tell you it is a horrible movie. If you ask the average conservative or libertarian they will tell you it is a great movie. Objectively, it is a mediocre movie at best. Subjectively, it is one of the best mediocre movies youll ever see."  In the National Post, Peter Foster credited the movie for the daunting job of fidelity to the novel, wryly suggested a plot rewrite along the lines of comparable current events, and concluded, "if it sinks without trace, its backers should at least be proud that they lost their own money." 

==Sequels==
 
The poor critical reception of Atlas Shrugged: Part I initially made Aglialoro reconsider his plans for the rest of the trilogy.    In an interview with The Hollywood Reporter, he said he was continuing with plans to produce Part II and Part III for release on April 15 in 2012 and 2013, respectively.  In a later interview with The Boston Globe, Aglialoro was ambivalent: "I learned something long ago playing poker. If you think youre beat , dont go all in. If Part 1 makes   a return to support Part 2, Ill do it. Other than that, Ill throw the hand in." 
 2012 U.S. elections.   In October 2011, producer Harmon Kaslow stated that he hoped filming for Part II would begin in early 2012, "with hopes of previewing it around the time of the nominating conventions". Kaslow anticipated that the film, which would encompass the second third of Atlas Shrugged, would "probably be 30 to 40 minutes longer than the first movie."  Kaslow also stated his intent that Part II would have a bigger production budget, as well as a larger advertising budget. 

On February 2, 2012, Kaslow and Aglialoro, the producers of Atlas Shrugged: Part II, announced a start date for principal photography in April 2012 with a release date of October 12, 2012.  Joining the production team was Duncan Scott, who, in 1986, was responsible for creating a new, re-edited version with English subtitles of the 1942 Italian film adaptation of We the Living. The first films entire cast was replaced for the sequel.

The sequel film,  , was released on October 12, 2012.  Critics gave the film a 5% rating on Rotten Tomatoes based on 22 reviews.    One reviewer gave the film a "D" rating,    while another reviewer gave the film a "1" rating (of 4).    In naming Part II to its list of 2012s worst films, The A.V. Club claimed "The irony of Part II s mere existence is rich enough: The free market is a religion for Rand acolytes, and it emphatically rejected Part I."  , The A.V. Club, December 20, 2012, accessed December 20, 2012. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 