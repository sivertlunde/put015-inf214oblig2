Alpine Antics
 

{{Infobox Hollywood cartoon|
| cartoon_name = Alpine Antics
| series = Oswald the Lucky Rabbit
| image = Oswald in Alpine Antics.jpg
| caption = Theatrical poster Tom Palmer
| story_artist =
| animator =
| voice_actor =
| musician = Bert Fiske
| producer = George Winkler
| distributor = Universal Pictures
| release_date = April 1, 1929
| color_process = Black-and-white
| runtime = 7:51 English
| preceded_by = The Suicide Sheik
| followed_by = The Lumberjack
}}

Alpine Antics is an animated cartoon by Margaret J. Winkler|M. J. Winkler Productions, and features Oswald the Lucky Rabbit.

==Plot summary==
One day at the Swiss mountains, Oswald is milking a goat which would then runaway upon being called by another one passing by. As Oswalds goat jumps out of the scene, the bucket used is kicked off and was all over the rabbits head. Oswald struggles to remove the pail but is able to get it off on time when he stumbles. Just then, his faithful St. Bernard (dog)|St. Bernard dog comes to him, carrying a message. The message is a distress note from the girl cat seeking Oswalds help.

Oswald and the dog move forth and head upland. Obstacles on the way include large rocks and a canyon, both of which they get through with little trouble. After a few more paces, they find the girl cat up a cliff and hanging onto a branch. To reach her, they stuck a ladder on top of a boulder. Oswald climbs up and collects the feline. It turns out momentarilly that the boulder is in fact a wolverine which wakes up and isnt happy to see them. Frightened by this, the dog runs off, carrying the ladder with Oswald and the girl cat still on it.

Keeping away from the fierce predator, the three friends run into a cave. When they reach the opened end, they find themselves on an edge thousands of feet above water. They then move further from the exit and around the mount to hide themselves. The wolverine also enters the cave but is unaware of what lies ahead and therefore picks up speed. As a consequence, the wolverine overshoots the edge and plunges into the sea. Oswald and the girl cat ride the dog on their way back.

==See also==
* Oswald the Lucky Rabbit filmography

==External links==
*  
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 


 