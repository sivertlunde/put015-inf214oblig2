Yeh Raat
 
{{Infobox film name           = Yeh Raat image          = director       = Chintu writer         = Chintu starring       = Krishna, Monica, Mehandi,Upma Srivastava,Anis Khan producer       = Alok Rajput cinematography = S. Mansoor Editor         = I.M. Kunnu distributor    =  released       =   runtime        = 111 minutes country        = India language       = Hindi music          = Kunwar Bharti gross          = 
}}
Yeh Raat is a Hindi horror film by Divey Arts. It is written and directed by Chintu and it was released in 2000.
Most of the film was shot in Goel Bunglow (Delhi), Wimpy (Delhi), Film City, Bhullar Bunglow, Sun Villa & Essel Studio.

==Plot==
The film opens with a young couple Rahul & Rani on their way to Kandala for their honeymoon are mislead to a wrongway, their car got stop on midway & they decides to spent the night in an unknown bunglow they find. Rani is possessed by an unknown force & kills Rahul by crushing his bones who tries to have sex with her.

Police tries to find out the mystery behind the disappearance of couples & decides to fasten the enquiry on the disappearance of young couples who were on their way to Kandala.

Manish Malhotra & Shivani are two journalist couples. Manish decides to go to Kandala to find out the mystery behind this. They are also mislead by the same arrow pointing right side & got stuck in the jungle. They find a deserted house were Rahul & Rani stay earlier. There they find a young girl Rasmalai. She informs them that the house belongs to Dcosta & he is not available in the house.She warns them not to stay in that house as the couples stay there will disappear & asks them to go to some other place. Manish declines saying that he is a journalist & he needs to find out the reason behind it.

They decide to go inside the house & after sometime Manish hugs Shivani. Shivani who is now possessed by an unknown force starts crushing the bones of Manish. Their fellow journalists launch a complaint the disappearance about Manish & Shivani.

Mumbai Police Commissioner assigns this case to Inspector Vikram, who, in the company of the Commissioners daughter, Neelu; their servant, Khiladi; and Professor L.K. Chawla, a Parapsychologist, travel there to try and unravel the mystery. Her mom confronts the police commissioner for sending Neelu for a dangerous mission.

They find Rasmalai who take them to the bungalow & also warns them about the bungalow. They also find that the couples arrived at the bungalow only because their cars got stuck on the midway.

Vikram finds Dcosta going somewhere in the midnight & follows him. He overhears his conversation with someone as he was yelling at someone that he/she should not kill anymore innocent people. As Prof. Chawal & Vikram enquires him he says that it was his daughter Ruby Dcosta who is behind the disappearance of all couples.

Dcosta used to work for Yograj & Naagraj. One day Ruby takes food for them. But she is raped by Yograj & Naagraj, her lover Johnny comes to rescue her. But Yograj stabs him with a knife & as Johnny fights him Naagraj stabs him from behind. They decides to bury the dead bodies. Naagraj goes crazy and run outside who is then killed by Rubys spirit. Yograj who is scared also drives away but his car gets stuck in the midst of the jungle. He is then killed by Ruby.

Ruby then starts killing all the couples whoever stays there. Dcosta explains that when Rubys ghost possess the girls they kill their boyfriend by crushing their bones. Later when Ruby leaves their body they will also die. Therefore Dcosta requests Ruby not to kill anymore people. Also warns Vikram that she will kill tonight as well as she did not respond to his request.

Neelu is possessed by Ruby & tries to kill Vikram. Prof.Chawal comes there & tries to hypnotise Neelu. She replies that she was killed though she was innocent & she finds happiness in others death. Chawla convinces her saying that she should not kill anyone if her love was true for Johnny. She finally agrees to leave forever.

Also Vikram finds that all the dead bodies are buried by Dcosta outside the bungalow. Khiladi marries Rasmalai & they lives happily ever after.

==Cast==
* Krishna
* Monica
* Mehandi -Shivani
* Upma Srivastav -Dolly
* Anis Khan -Manish Malhotra/Journalist
* Preeti Arjun -Yograj
* Shiva Rindani -Naagraj
* Ramesh Goyal -Dcosta
* Shabnam Kapoor -Ruby Dcosta
* Ajay Bali
* Tarana -Rasmalai
* Banwarilal Jhol -Kiladi
* Sadashiv Amrapurkar -Police Commissioner
* Raja Hasan - Rahul/Guest Appearance
* Sonu Batra - Rani/Guest Appearance
* Ram Chandra - Guest Appearance

==External links==
*  

 
 
 
 