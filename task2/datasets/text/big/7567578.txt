Best of the Best 3: No Turning Back
 
{{Infobox Film
| name           = Best of the Best 3: No Turning Back
| image          = Best of the Best 3.jpg
| caption        = DVD cover
| director       = Phillip Rhee
| producer       = Phillip Rhee Peter Strauss
| eproducer      = Frank Giustra Marlon Staggs
| aproducer      = Stacy Cohen
| writer         = Barry Gray Deborah Scott
| starring       = Phillip Rhee Gina Gershon
| music          = Barry Goldberg
| cinematography = Jerry Watson
| editing        = Bert Lovitt
| studio         = The Movie Group Picture Securities Ltd. Buena Vista International
| released       =  
| runtime        = 90 min.
| country        = United States
| language       = English
| budget         = 
}}
 martial arts action film directed by the films star Phillip Rhee. It is the second sequel in the Best of the Best film series. The film co-stars Christopher McDonald, Gina Gershon, Dee Wallace and an uncredited R. Lee Ermey.

==Plot==

The film opens in the small town of "Liberty", where a vicious group of neo-Nazis have been terrorizing the populace, most recently having murdered an African-American pastor and set fire to his church. While visiting his sister and brother-in-law in Liberty, Tommy Lee (Phillip Rhee) crosses paths with the groups leader Donnie Hansen (Mark Rolston), and is drawn into the conflict when his sister is attacked in their car. Later, the group attempts to harass a schoolteacher named Margo (Gina Gershon) at the local 4-H fair, but Tommy intervenes and fends them off. Ungrateful at first, she eventually warms up to Tommy when they are set up on a blind date, and they start a relationship. Meanwhile, the neo-Nazis launch an assault on Tommys family. After saving Margo from an attempted rape, Tommy returns home to find his sister badly beaten. He and his brother-in-law, the local sheriff Jack Banning (Christopher McDonald), decide to take matters into their own hands and invade the groups heavily guarded compound, where Jacks children have been taken hostage. After a long, climactic fight, the children are rescued and Tommy defeats Hansen in single combat, but refuses to kill him, knowing that it would only further his message of hatred.  As Tommy turns away, Hansen takes aim at him with a rifle, prompting a local teenager named Owen Tucker (Peter Simmons) to shoot and kill Hansen himself, thus brokering a new peace in the town.  The ending scene shows the pastors child reading from the Bible and the church being rebuilt.

==Cast==
*Tommy Lee - Phillip Rhee
*Jack Banning - Christopher McDonald
*Margo Preston - Gina Gershon
*Donnie Hansen - Mark Rolston
*Owen Tucker - Peter Simmons
*Karen Banning - Cristina Lawson
*Georgia - Dee Wallace
*Tiny - Michael Bailey Smith
*Luther Phelps - Justin Brentley
*Rev. Phelps - Andra R. Ward
*Preacher Brian - R. Lee Ermey (uncredited)
*Random Neo Nazi - Mark Kreuzman
*Girl leaving Ice Cream Shop - Michele L. Bartlett (Bennett)
*Girl in choir - Jerra Wisecup (Thompson)
*Trucker/Neo Nazi - John E. Blazier

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 