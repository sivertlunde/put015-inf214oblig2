Uncle Bun
{{Infobox film
| name           = Uncle Bun
| image          = Uncle Bun.jpg
| image_size     = 
| alt            = 
| caption        =  Bhadran
| producer       = Ajitha Hari Pothan Bhadran P. Balachandran (Dialogues) Bhadran
| narrator       =  Kushboo  Nedumudi Venu  Charmila
| music          = Background score:  
| cinematography = K. P. Nambiathiri Jayanan Vincent A. Vincent
| editing        = M. S. Mani
| studio         = Supriya International
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Bhadran and starring Mohanlal in the title role. It was based on  the 1989 American film Uncle Buck. Special costumes for Mohanlal was designed by art director Sabu Cyril.

==Plot==
Charlie Chacko, an overweight man who is struggling with the death of his first love, is forced to take care of his brothers three children and then falls in love with Geetha Krishnan.

==Cast==
*Mohanlal as Charlie Chacko Kushboo as Geetha Krishnan
*Nedumudi Venu as Jameskutty Chacko
*Reshma as Asha James
*Monica (actress)|Monica- Child artist
*Charmila as Rosie
*Philomina as Gloria Therathi Santhakumari as Sister 1
*Sukumari as Sister 2
*Mala Aravindan as Mathai
*Sankaradi as Ittiyachan Usha
*T.S.Krishnan

==Box office==

The film was released along with Mammootty s Anaswaram, while Mohanlal had another release Kilukkam directed by Priyadarshan. While Kilukkam became a blockbuster and highest grosser of Malayalam film industry, Anaswaram flopped. Uncle Bun couldnt perform as expected eventhough its technical aspect was appreciated; finally it settled as an average earner, but still completed 100 days in some centres. 

==Music==
The songs were composed by maestro Raveendran and lyrics were penned by Pazhavila Rameshan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Artist(s)
|-
| 1 || Ambilikkalayoru || K. J. Yesudas|Dr. K. J. Yesudas
|-
| 2 || Dont Drive Me Mad || K. J. Yesudas|Dr. K. J. Yesudas, Malgudi Subha
|-
| 3 || Idayaraaga || K. J. Yesudas|Dr. K. J. Yesudas, K. S. Chithra
|-
| 4 || Kurukuthikannulla || K. J. Yesudas|Dr. K. J. Yesudas
|}

==References==
 

==External links==
*  

 
 
 
 


 