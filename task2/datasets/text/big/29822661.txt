The Countess of Castiglione
{{Infobox film
| name           = The Countess of Castiglione
| image          =
| image_size     =
| caption        =
| director       = Flavio Calzavara
| producer       = Giovanni Addessi
| writer         = Pino Accame   Mario Beltrami    Flavio Calzavara
| narrator       =
| starring       = Doris Duranti   Andrea Checchi   Renato Cialente   Enzo Biliotti 
| music          = Virgilio Doplicher
| cinematography = Carlo Montuori   Gábor Pogány
| editing          = Ignazio Ferronetti
| studio         = Nazionalcine 
| distributor    = Manenti Film 
| released       = 14 October 1942
| runtime        = 85 minutes
| country        = Italy Italian
| budget         =
}} 1942 Italy|Italian historical film directed by Flavio Calzavara and starring Doris Duranti, Andrea Checchi and Renato Cialente.  The film portrays the life of the nineteenth century Italian aristocrat Virginia Oldoini, Countess of Castiglione, best known as the lover of Napoleon III of France.

==Cast==
*Doris Duranti as Virginia Oldoini, contessa di Castiglione
*Andrea Checchi as Baldo Princivalli
*Renato Cialente as Costantino Nigra
*Enzo Biliotti as Napoleone III
*Lamberto Picasso as marchese Oldoini
*Clara Auteri Pepe as Martina
* Annibale Betrone as Lurbanista barone Haussmann 
* Giacomo Moschini as Mocquart  
* Cesare Barbetti as Il piccolo Peruzzi
* Marisa Dianora as Marianna 
* Claudio Ermelli as Il nobile sulla sedia a rotelle  
* Nino Marchesini as Connot 
* Alfredo Martinelli as Un amico degli Oldoini 
* Armando Migliari as Un amico degli Oldoini 
* Nico Pepe as Un amico degli Oldoini 
* Emilio Petacci as Un amico degli Oldoini  
* Vinicio Sofia as Un amico degli Oldoini  
* Maria Pia Spini as Limperatrice Eugenia  
*Gabriele Ferzetti
* Fulvio Ranieri 
* Ennio Sammartino
* Ambretta Glori  
* Edoardo Grandi
* Alda Lauri
* Alessandra Adari
* Luigi Allodoli

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 