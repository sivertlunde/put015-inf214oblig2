Spiral (1978 film)
 
{{Infobox film
| name           = Spiral
| image          = 
| caption        = 
| director       = Krzysztof Zanussi
| producer       = 
| writer         = Krzysztof Zanussi
| starring       = Jan Nowicki
| music          = 
| cinematography = Edward Kłosiński
| editing        = Urszula Śliwińska
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}

Spiral ( ) is a 1978 Polish drama film directed by Krzysztof Zanussi which tells the story of a stranger who turns up at a resort hotel in midwinter, behaves rudely towards other guests and disappears the next day. Found half-frozen in the snow he is taken to hospital where his story is gradually revealed.

It was entered into the 1978 Cannes Film Festival.   

==Cast==
* Jan Nowicki - Tomasz Piatek
* Maja Komorowska - Teresa
* Zofia Kucówna - Maria
* Aleksander Bardini - Doctor
* Jan Świderski - Henryk
* Piotr Garlicki - Henryks Son
* Marian Glinka - Physician
* Ewa Ziętek - Cleaning Woman
* Seweryna Broniszówna - Old Woman
* Andrzej Hudziak - Augustyn
* Marta Ławińska - Psychologist
* Cezary Morawski - Czarek
* Andrzej Szenajch - Guest House Manager
* Stefan Szmidt - Rescuer
* Daria Trafankowska - Nurse

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 