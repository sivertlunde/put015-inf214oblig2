Thank You, Mr. Moto (film)
{{Infobox film
| name           = Thank You, Mr. Moto
| image          = Poster of Thank You, Mr. Moto.jpg
| image_size     =
| caption        = Norman Foster
| producer       = Sol M. Wurtzel
| based on       =  
| writer         = Wyllis Cooper Norman Foster Thomas Beck Pauline Frederick Jayne Regan
| music          = Samuel Kaylin
| cinematography = Virgil Miller
| editing        = Irene Morra Bernard Herzbrun
| distributor    = Twentieth Century Fox
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 novel of the same name by the detectives creator, John P. Marquand. Mr. Moto battles murderous treasure hunters for priceless ancient scrolls which reveal the location of the long-lost tomb of Genghis Khan.

==Plot==
A caravan settles for the night in the Gobi Desert. A man sneaks into a tent to steal a scroll, but adventurer and soldier of fortune Kentaro Moto (Peter Lorre) is only pretending to be asleep and kills him. When the caravan reaches Beijing|Peiping, Moto is searched by the police. The scroll is found, but Moto grabs it and escapes.

He changes clothes and accepts an invitation to a party hosted by Colonel Tchernov (Sig Ruman) in honor of American Eleanor Joyce (Jayne Regan). At the soirée, Moto observes a guest, Chinese Prince Chung (Philip Ahn) leaving his mother to speak privately with Tchernov in another room. Tchernov offers to buy certain family heirloom scrolls from Chung. When Chung refuses to part with them, Tchernov draws a pistol, then is killed (off-screen) by Moto. Joyce stumbles upon the scene and watches in disbelief as Moto calmly stages it to look like a suicide. Moto politely advises her to say nothing to avoid an international incident.

Later, as a favor to his rescuer, Chung grants Motos request to see the scrolls. Chung informs him that the seven scrolls give directions to the lost grave of Genghis Khan. The Chungs are determined to see that it and its fabulous treasures remain undisturbed. However, one scroll was lent to an exhibition and was stolen.
 Thomas Beck), they see Moto entering Pereiras shop. Moto gets Pereira to confess that he stole the authentic scroll, but before he can obtain more information, Pereira is shot and killed by a gunman in a car which speeds away.

Moto returns to his apartment to find it ransacked. Sensing that the would-be thief is present, Moto leaves his gun lying around. Schneider (Wilhelm von Brincken) holds him at gunpoint and forces Moto to give him the scroll. When Moto tries to flee, Schneider shoots several times with Motos gun. However, the gun was filled with Blank (cartridge)|blanks; Moto trails Schneider to Madame Tchernov (Nedda Harrigan). When they leave to rendezvous with their gang, Moto starts to follow, but is knocked out by the butler, Ivan (John Bleifer), another crook. Joyce, who had been comforting the widow, is taken hostage.

The arch-villain (and Madame Tchernovs lover), Herr Koerger (Sidney Blackmer), forces Prince Chung to reveal the location of the scrolls by striking his mother. As they are leaving, Madame Chung attacks Koerger with a knife and is killed. Meanwhile, Nelson finds and revives Moto. They rush to the Chungs, but arrive too late. The dishonored prince commits suicide after they untie him; Moto comforts him before he dies by promising to avenge the Chung family and safeguard the tomb.

The two men track the criminals to a Junk (ship)|junk. After another attempt to kill him, Moto informs Koerger that the scroll he gave Schneider is a fake. He offers to split Genghis Khans treasure. Then he sows dissent by telling Madame Tchernov that Koerger is dumping her for Joyce, which the quick-thinking American "confirms". This provides a distraction for Moto to kill Koerger. Later, to the dismay of Joyce and Nelson, Mr. Moto burns the scrolls to fulfill his promise to Chung.

==Cast==
* Peter Lorre as Mr. Kentaro Moto Thomas Beck as Tom Nelson
* Pauline Frederick as Madame Chung
* Jayne Regan as Eleanor Joyce
* Sidney Blackmer as Herr Koerger
* Sig Ruman as Colonel Tchernov
* John Carradine as Pereira
* Wilhelm von Brincken as Schneider (as William Von Brincken)
* Nedda Harrigan as Madame Tchernov
* Philip Ahn as Prince Chung
* John Bleifer as Ivan
==Production==
Thank You, Mr Moto was the second Mr Moto novel following No Exit and was published in 1936 after having been serialised first. The New York Times praised the books "vitality and vividness". Plotters in Peking: THANK YOU, MR. MOTO. By John P. Marquand. 278 pp. Boston: Little, Brown & Co. $2.
E.C.B.. New York Times (1923-Current file)   17 May 1936: BR22.  

When it was decided to make a movie series from the Moto books, the first three films were to be Think Fast, Mr Moto, then Thank You, Mr Moto and Mr Motos Gamble. FOX LISTS FILMS FOR NEXT SEASON: 66 Features Are Included in Companys Most Ambitious Production Schedule 204 SHORTS TO BE MADE Zanuck Will Supervise 52 Long Pictures at 20th Century Studios in Beverly Hills
New York Times (1923-Current file)   02 June 1937: 20.  

Jayne Regan was given her first lead when cast in the film. HOLLYWOOD ENGINEERS COLOR INVASION OF LONDON AND PARIS: Charles Coburn, Art Theater Apostle, Signed
Schallert, Edwin. Los Angeles Times (1923-Current File)   11 Oct 1937: A10  Filming started October 1937. NEWS OF THE SCREEN: Three Stunt Fliers Signed for Parts in Paramounts Men With Wings--New Films Open Here Today
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   12 Oct 1937: 31.  

The film marked the last screen appearance of Pauline Frederick. A GREAT FILM ACTRESS: PAULINE FREDERICK
The Observer (1901- 2003)   25 Sep 1938: 12.  
==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 