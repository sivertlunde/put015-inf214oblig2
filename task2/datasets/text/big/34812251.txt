Au nom du Christ aka In the name of Christ
 

{{Infobox film
| name           = Au nom du Christ aka In the name of Christ
| image          = Au_Nom_Du_Christ.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Roger Gnoam MBala
| producer       =   (Suisse)
| writer         = 
| narrator       = 
| starring       = 
| music          = 

| distributor    =  
| released       = 1992 
| runtime        = 89 minutes
| country        = Ivory Coast 
| language       = French with or without English or Portuguese subtitles
| Sourced        = Mars / Lux Films 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Au nom du Christ aka In the name of Christ is a parable exploring the proliferation of pseudo-Christian sects in Africa. A pig herder, despised by the whole of his village, sees a vision in which he is informed that he has been chosen by God to save his people. He starts a new sect. After performing several miracles, he soon boasts a large army of disciples yearning for spiritual fulfillment. The pig herder claims to be the cousin of Christ and the villagers believe him after he performs further trumped-up miracles, like the curing of a ‘mad woman’. His power grows, but he soon begins abusing it, claiming to be incapable of sin. 

==Cast==
*Pierre Gondo 

==References==
 
Winner of the Grand Prize for Best Film  at the FESPACO Film Festival and Nominated for the Golden Leopard Award  at the  Locarno International Film Festival in 1993.  {{Cite web | title = History of Cinema in Côte dIvoire
  | work = 
  | publisher = Film Birth
  | year = 
  | url = http://www.filmbirth.com/cote_d_ivoire.html
  | doi = 
  | accessdate =20 February 2011 }}
 
 

==External links==
* 
* 
 
 

 