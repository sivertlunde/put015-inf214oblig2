Khwab
{{Infobox film
 | name = Khwab
 | image = Khwab-1980.jpg
 | caption = DVD Cover
 | director = Shakti Samanta
 | producer = Shakti Films
 | writer = 
 | dialogue = 
 | starring = Ashok Kumar Mithun Chakraborty Ranjeeta Kaur Naseeruddin Shah Yogeeta Bali Birbal
 | music = Ravindra Jain
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = February 22, 1980 
 | runtime = 115 min.
 | country = India
 | language = Hindi Rs 1.3 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1980 Hindi Indian feature directed by Shakti Samanta, starring  Ashok Kumar, Mithun Chakraborty, Ranjeeta Kaur, Naseeruddin Shah and  Yogeeta Bali

==Plot==
Khwab is a Family love story which turns as a suspense thriller in the end. The courtroom scenes in the Climax was well appreciated.

==Cast==
*Ashok Kumar
*Mithun Chakraborty
*Ranjeeta Kaur
*Naseeruddin Shah
*Yogeeta Bali
*Birbal

==References==
*http://www.imdb.com/title/tt0268407/
*http://www.ibosnetwork.com/asp/filmbodetails.asp?id=Khwab

==External links==
*  

 
 
 

 