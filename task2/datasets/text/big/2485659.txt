Rush (1991 film)
 
{{Infobox film
| name           = Rush
| image          = Rush (1991 film) cover.jpg
| caption        = Theatrical Poster
| director       = Lili Fini Zanuck
| producer       = Gary Daigler Richard D. Zanuck
| writer         = Kim Wozencraft (book) Pete Dexter (screenplay)
| starring       = Jason Patric Jennifer Jason Leigh Sam Elliott
| music          = Eric Clapton
| cinematography = Kenneth MacMillan Mark Warner
| studio         =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = $12 million
| gross          = $7,241,350
}} drug addicts themselves and, failing to get the evidence they need, use falsified evidence.

== Plot ==
In Texas, seasoned undercover narcotics police officer, Jim Raynor (Jason Patric), is told by his superior Lt. Dodd (Sam Elliott) to choose a partner from a group of recent police academy graduates for his undercover investigation, which has been ongoing for two years.  His choice of Kristen Cates (Jennifer Jason Leigh) surprises Dodd, but Raynor is confident she is the right fit for what he needs.

Raynor explains to Cates that all they will have is each other in this assignment.  They are caught between the legal world and the illegal underworld, belonging to neither and only able to trust one another.  This is a difficult and demanding job, one that will take them both to the brink.  Though initially startled by Raynors intensity, Cates insists she is capable of doing whatever it takes to get the job done.  Later at Raynors apartment, he teaches Cates how to properly shoot up heroin. Cates informs Raynor that she was instructed how to expertly fake drug use at the police academy, which prompts a strong rebuke from Raynor.  He informs her in no uncertain terms that she will be put in situations where she will have to take the drugs they will be buying.  The drug dealers they will be doing business with are not stupid, and if she tries to fake drug use in front of them, she will get both of them killed.

Raynor and now Catess main objective in their operation is to take down the cunning, mysterious, and powerful drug lord Will Gaines (Gregg Allman).  While Raynor (and the police department) is certain Gaines is the main drug boss in the town, Gaines is an expert at avoiding detection, and deeply mistrustful of Raynor.  Unable to secure access to him, Cates and Raynor buy large quantities of drugs from minor dealers in the town.  These drugs are cataloged and given to Dodd, along with information detailing from whom they bought the drugs.  Raynor and Cates quickly go from pretending to be lovers to the real thing.

As Raynor predicted, Cates is soon put into a position where she is forced to inject drugs in front of a drug dealer.  Raynor tries to intervene and tell the dealer that "his lady doesnt fix", but the dealer insists at gunpoint.  Cates first tries nervously to talk her way out of the situation, then yanks off her jacket and with shaking hands begins to prepare a heroin shot as Raynor had taught her.  Seeing her high level of anxiety, Raynor takes the prepared needle from her and injects it into her arm.  While the dealer watches approvingly and laughs, Cates quietly vomits off screen.
 addicted to withdrawal while appearing largely unaffected by his own drug use.  However, over the course of the film he becomes even more addicted than she.  Eventually, both are able to get clean, but remain traumatized and scarred by their drug experiences.

Though they have successfully infiltrated the towns drug underworld, Raynor and Cates are no closer to obtaining evidence against Gaines.  Under pressure from the department, they falsify evidence against Gaines in order to secure his arrest and indictment.  They move into a small trailer awaiting the beginning of the trial.  Cates is startled awake one evening by the barrel of a shotgun caressing her face.  A gunfight ensues in which Raynor is shot in the thigh, striking his femoral artery.  Cates frantically goes for help, then returns to the trailer to find Raynor barely conscious.  He dies in her arms.

At Gainess trial, Cates is on the witness stand.  During testimony she mentions that she has resigned from the police force.  Her testimony sticks to the fabricated story Raynor and she concocted, and appears to earn the support of the jury and courtroom spectators.  When she looks directly at Gaines, however, he slides two fingers down the bridge of his nose, mimicking the movement of the shotgun that eventually killed Raynor.  Stunned, Cates retracts her statements about Gainess involvement in the drug trade, and testifies that they were ordered to fabricate evidence by the Chief of Police.  This of course secures Gainess acquittal.

Freed from police custody, Gaines enters the drivers side of his car.  He notices someone hiding in the backseat and is killed with a shotgun blast to the face.  The killer is never shown, but the impression is that it is Cates.

==Factual Basis==
This film is based on the real-life 1977 Tyler, Texas drug scandal, in which Kim Wozencraft and her partner Creig Matthews (who was later her husband) were sent in undercover to try to arrest, and if necessary set up, a Tyler club owner by the name of Kenneth Bora (the Will Gaines character is based on him). Wozencraft and Matthews set up over 100 casual drug users that they bought or were given small amounts of drugs from, most of which they used themselves, and also eventually filed a false case against Bora as well, who spent almost 5 years of a 20-year sentence in prison before being exonerated. Wozencraft and Matthews finally admitted to the U.S. Attorneys office that they had falsely created most of the cases they had filed in Tyler, at the behest of the then Tyler police chief Willie Hardy, including the case that led to Boras conviction and imprisonment. While awaiting trial Wozencraft and Matthews married, and shortly thereafter pled guilty to federal deprivation of civil rights charges. Wozencraft served 3 years in federal prison and Matthews served almost 5. The lives of those that they falsely accused were also largely destroyed. The Tyler Police Department was cleaned up by court order in the early 1980s. Wozencraft is now an author and lecturer, but also remains a convicted felon. The Tyler Drug Scandal remains one of the earliest and highest profile police corruption scandals of its kind, and the scandal led to reforms in evidentiary procedures and other significant law changes.  

== Cast ==
* Jason Patric as Jim Raynor
* Jennifer Jason Leigh as Kristen Cates
* Sam Elliott as Dodd
* Max Perlich as Walker
* Gregg Allman as Will Gaines
* Tony Frank as Nettle William Sadler as Monroe
* Dennis Letts as Senior District Attorney
* Dennis Burkley as Motorcycle Guy
* Merrill Connally as Defense Attorney
* Michael Kirkland as Assistant DA

== Soundtrack == soundtrack includes Claptons guitar and vocals on "Tears in Heaven" and "Help Me Up"; Clapton and Buddy Guy perform "Dont Know Which Way to Go" as well.

Other songs featured in the film (but not on the soundtrack album) are Jimi Hendrixs "All Along the Watchtower" (composed by Bob Dylan), Lynyrd Skynyrds "Free Bird", Freddy Fenders "Before the Next Teardrop Falls", Robin Trowers "Bridge of Sighs", The Ohio Players "Love Rollercoaster", and Rick Derringers "Rock and Roll Hoochie Koo".

== Influence ==
The main character of the game Starcraft is reportedly based on the Jim Raynor character in this film. 

== Notes ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 