Taking Wing
{{Infobox film
| name           = Taking Wing
| image          = 
| caption        = 
| director       = Steve Suissa
| producer       = Thierry de Navacelle
| writer         = Marc Esposito Steve Suissa
| starring       = Clément Sibony
| music          = 
| cinematography = Dominique Chapuis
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}}

Taking Wing ( ) is a 2000 French drama film directed by Steve Suissa. It was entered into the 22nd Moscow International Film Festival where Suissa won the award for Best Director and Clément Sibony won the award for Best Actor.   

==Cast==
* Clément Sibony as Stan
* Isabelle Carré as Julie
* Christine Citti as Stans mother
* Marc Samuel as Stans father
* Léopoldine Serre as Lulu
* Steve Suissa as Joseph
* Corinne Dacla as Marthe
* Bernard Fresson as Victor
* Attica Guedj as Sarah
* Denis Bénoliel as Léon

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 