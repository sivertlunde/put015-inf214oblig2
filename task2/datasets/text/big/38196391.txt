Chanduki
{{Infobox film
| name           = Chanduki
| image          =
| image_size     =
| caption        =
| director       = A. Q. Pirzada
| producer       = Sardar ul-Lateef
| writer         = Agha Saleem
| lyrics         = Rashid Lashari
| starring       = Meh Parah, Mushtaq Changezi, Khurshid Kanul, Qurban Jilani
| music          = Ghulam Nabi Abdul Lateef
| cinematography = M. Hussain
| editing        =
| released       = 1969
| runtime        =
| country        = Pakistan Sindhi
| budget         =
| domestic gross =
| preceded_by    =
| followed_by    =
}}

Chanduki is a Pakistani film released in 1969. It was directed by A. Q. Pirzada, produced by Sardar ul-Lateef and starring Mushtaq Changezi.

==See also==
* Sindhi cinema
* List of Sindhi-language films

==Further reading==
* Gazdar, Mushtaq. 1997. Pakistan Cinema, 1947-1997. Karachi: Oxford University Press.

==External links==
*  

 
 
 

 