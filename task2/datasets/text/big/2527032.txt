The Anderson Platoon
{{Infobox film
| name           = The Anderson Platoon  La Section Anderson 
| image          = The Anderson Platoon VHS cover.png
| image_size     = 
| caption        = VHS cover image
| director       = Pierre Schoendoerffer
| producer       = Pierre Schoendoerffer
| writer         = Pierre Schoendoerffer
| narrator       = Pierre Schoendoerffer Stuart Whitman  English version 
| starring       = Joseph B. Anderson
| music          = 
| cinematography = Dominique Merlin
| editing        = 	
| distributor    = French Broadcasting System Pathé Contemporary Films
| released       = February 3, 1967 (France) 1967 (Italy) April 10, 1968 (U.S.) July 17, 1968 (Germany) 1968 (UK)
| runtime        = 60 min./65 min. (uncut)
| country        = France  French English English
| budget         = 
| preceded by    = 
| followed by    = 
}}
The Anderson Platoon ( , released in 1966 in Europe, 1967 in the US) is a documentary feature by Pierre Schoendoerffer about the Vietnam War, named after the leader of the platoon - Lieutenant Joseph B. Anderson - with which Schoendeorffer was Embedded journalism|embedded. Two decades later, a sequel was released as Reminiscence.

==Background==
 
In summer 1966, France Soir news magazine director and French public channel ORTF producer Pierre Lazareff proposed that war reporter and director Pierre Schoendoerffer complete the "unachieved" war documentary Schoendoerffer began in 1954.

Back in May 1954, Schoendoerffer was covering the First Indochina War for the French armys cinematographic service SCA. At the siege of Dien Bien Phu, he filmed the battle between the French Union forces and the Viet Minh, but his reels were captured when he surrendered to the enemy.

After the departure of the French forces from Vietnam in 1956, the U.S. Army replaced it several years later and fighting soon flared again, at the beginning of the Vietnam War.

Arguing that "the war was the same, the French only switching with the Americans", Lazareff convinced the French veteran to return to Vietnam as a kind of second chance to complete his war documentary.

==Plot== French war cameraman and First Indochina War veteran Schoendoerffer (38), already famous for his celebrated masterpiece The 317th Platoon (1965), returns to Vietnam.
 GIs led by Black West Pointer Lieutenant Joseph B. Anderson (24) until October 1966.

==Reception==
  Oscar on April 10, 1968 at the Santa Monica Civic Auditorium.  Prix Italia for Original Dramatic Program  Emmy Award for the Best Documentary Film of 1967 
*1967: Academy Award for Documentary Feature   
*1968: Merit Award (BBC) 

===Andersons comments===
Some time after the film was released, Captain Joseph B. Anderson, Jr., the leader of the 1st Cavalry Division, U.S. Army after whom the film is named, commented on the film (and on his other experiences in Vietnam and in the military, in general) in Wallace Terrys book Bloods: An Oral History of the Vietnam War by Black Veterans (1984). 

He observed, on page 227: 
 "After visiting different operations around the country, both army and Marine, he settled on the 1st Cav because of the new approach of our air mobility, or helicopter orientation. And he wound up with my platoon because of its racial mix - we had American Indians and Mexican-Americans, to - our success in finding the lost platoon, my West Point background and ability to speak French. He and the film crew stayed with us day and night for six weeks, filming everything we did. They spoke very good English, and I didnt speak good enough French. And Schoendoerffer had as much knowledge and experience about the war as any of us. 

 The film would be called The Anderson Platoon. And it would make us famous." 

On page 230, Captain Anderson discuses the death from friendly fire of a white soldier named Shannon, from California, who is introduced in the film at approximately 650". Anderson states: "I did write a letter to his folks, telling them he did an exceptionally good job. I did not describe the circumstances under which he was killed, because we were directed not to put those kinds of details in letters whatever the case may be."  He continues: "The film describes the grenade as an enemy grenade. Which is not the real circumstances."

On page 233, Captain Anderson notes: "I spent my last months in the base camp at An Khe, an aide to the commanding general. Being featured in The Anderson Platoon had obviously helped my career."

==Releases== machine gun. (09.1966)]]
  medical chopper. (09.1966)]]

===Television===
The Anderson Platoon was broadcast on the French public channel ORTFs monthly show Cinq colonnes à la une on February 3, 1967. 

CBS premiered the English dub versionon television in the United States on July 4, 1967.  Shortly after its 1968 Academy Award, it was broadcast a second time in Frances ORTF. 

In West Germany, the 62 min. version was broadcast on July 17, 1968 on Norddeutscher Rundfunk|NDR, Sender Freies Berlin|SFB, and Bremen III. After the Berlin walls fall it was broadcast in Germany, on Westdeutscher Rundfunk|WDR, on January 15, 1995. 

===Theater===
This documentary was originally made for a French TV show and was released in theaters in the United States only. 

==Alternate titles==
 
*La Section Anderson: original title
*The Anderson Platoon: English re-edited uncensored version title. This is the only accurate account for this title.
*2. Kompanie, 1. Zug, Vietnam 1966 ("2nd Company, 1st Platoon, Vietnam 1966"): West German title. The "Anderson Platoon"s actual name is "1st Platoon". The platoon belongs to the "B" for "Bravo" Company (the second letter in the English alphabet), and the documentary was shot in South Vietnam in 1966, hence the German title.
*Abteilung Anderson ("Detachment Anderson"): German title after the reunification. This is a translation of the original title; "Detachment (military)|Detachment" is a synonym for "Platoon" in French.

===Home video===
The Anderson Platoon was made available on VHS tapes in the United States only. 

A 60 min. VHS re-edited uncensored video edition was released in December 1987 by Hollywood Select Video. It was re-released by Timeless Video in May 1990. Timeless released a second print in June 1999. 

By June 2000, Homevision released the original 65 min. French version subtitled in English. 

===Video on demand=== VOD pay-per-view National Audiovisual Institutes website hosting the ORTF archives. 

==Reminiscence==
A sequel to The Anderson Platoon, entitled Reminiscence, was released in 1989.  It depicts Schoendoerffers meeting the platoon survivors 20 years after the events. 

==See also==
*1st Cavalry Division (United States)
*Vietnam war
*The 317th Platoon (1965)
*Platoon (film)|Platoon (1986)

==Media links==
*     (National Audiovisual Institute)

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 