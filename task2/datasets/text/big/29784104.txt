Adventures of Tarzan
hindi full
 | name = Adventures of Tarzan - Hindi
 | image = tarzanhindifilm.jpg
 | caption = DVD Cover
 | director = Babbar Subhash
 | producer = Mushir Riaz
 | writer = 
 | dialogue = 
 | starring = Hemant Birje Dalip Tahil Kimi Katkar Om Shivpuri
 | music = Bappi Lahiri
 | lyrics = 
 | associate director = 
 | art director = 
 | cinematography = Radhu Karmakar
 | released = 1985
 | runtime = 135 min.
 | language = Hindi Rs 3 Crores
 | preceded_by = 
 | followed_by = 
 }}
 Indian feature film directed by Babbar Subhash, starring Hemant Birje, Dalip Tahil, Kimi Katkar and Om Shivpuri. The film was much talked about in its time, mainly due to the hot scenes between lead pairs and super hit songs.

==Plot==
Adventures of Tarzan is the popular story of Tarzan, made in Hindi language.Ruby Shetty and her widowed dad live a wealthy lifestyle. Rubys father often travels to deep jungles of India in search of a fabled tribe in the Shakabhoomi region. The people who have tried to trace the tribe have never returned. This time Ruby also decides to accompany her dad. She is introduced to a man named D.K. by her dad and he would like her to get married to D.K. Later days, Ruby does a number of misadventures and is rescued by an ape-like man called Tarzan and both falls in love. Tarzan does not have experience with outside world and cannot speak any language as well, but he is intrigued by Ruby annoying D.K.. Rubys dad and D.K plans to capture Tarzan and take him to work for Apollo Circus owned by Krishnakant Verma. Tarzan is captured before Tarzan and Rubys romance could take wing. Tarzan is chained and taken to the circus and made to spend the rest of his days performing various acts, thus leaving D.K. to marry Ruby. Would Tarzan escape from Circus ? Would Ruby agrees to marry D.K. ? All these questions are answered in Climax.

==Cast==

*Hemant Birje  ...  Tarzan
*Kimi Katkar  ...  Ruby Shetty
*Dalip Tahil  ...  D.K.
*Narendra Nath  ...  Krishnakant Verma
*Om Shivpuri  ...  Shetty
*Sudhir  ...  Press Photographer
*Sunita
*Raja Duggal

==References==
* http://www.webmallindia.com/buy_dvd_online-movie-ADVENTURES+OF+TARZAN-p-5249.html
* http://www.imdb.com/title/tt0364049/

==External links==
*  

 

 
 
 
 
 

 
 