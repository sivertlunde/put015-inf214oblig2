The Return of Navajo Boy
{{Infobox film
| name = The Return of Navajo Boy
| image = The_Return_of_Navajo_Boy.jpg
| caption = 
| director = Jeff Spitz
| starring = Cly Family
| released =  
| runtime = 52 min.
| country = United States English Navajo Navajo
}}

The Return of Navajo Boy (released in 2000) is a documentary film produced by Jeff Spitz and Bennie Klain about the Cly family,  , white supremacy, media and political representation, off-reservation adoption, and denial of reparations for environmental illnesses due to uranium mining in Monument Valley, Utah, which was unregulated for decades.    Bill Kennedy served as the films executive producer; his late father had produced and directed the earlier silent film The Navajo Boy (1950s), which featured the Cly family.

In 2000, the film was an official selection of the Sundance Film Festival. It has won numerous awards.      

== The Cly family ==
The producers wanted to tell the full story of the Cly family, who were residents of the Navajo Nation in Monument Valley, Utah. They had earlier been the subjects in the silent film The Navajo Boy. Through their story, the director and family intended to explore many of the issues with which the Navajo Nation has had to struggle since the early 20th century: land use and environmental contamination, off-reservation adoptions, health education, enforcement of treaty rights, relations with the United States government.

Much of the story in the 2000 film is told by the chief subject, Elsie Mae Cly Begay, the oldest of the children shown in The Navajo Boy.  She is the oldest living Cly featured in the 2000 film. Her mother Happy Cly died of lung cancer, which the family believed was caused by environmental contamination from unregulated uranium mining on the reservation. Elise Mae Begay has lost two sons, one to lung cancer and the other to a tumor, whose deaths she attributes to uranium contamination near the house where they lived when her sons were children. (Constructed in part of contaminated rock, the structure was torn down in 2001.) )

As uranium was mined for four decades in six regions on the reservation, the result has been numerous sites of abandoned mine residues and tailings, including near Begays former home. In some areas, families used contaminated rock to build their homes. The air and water have also been contaminated. "By the late 1970s when the mines began closing, some miners were dying of lung cancer, emphysema or other radiation-related ailments." 

At the time of Happy Clys death, her youngest son John Wayne Cly was an infant. Christian missionaries adopted the boy. Elsie Mae Begay insists that the family agreed only to have her brother John cared for, but that he was to be returned to the family when he was six years old. They lost track of him, but through the making of the film, the Cly family was reunited with their long-estranged brother John Wayne Cly. 

Elsie Maes late grandparents, Happy and Willie Cly, were the main subjects of the earlier film. The "Navajo Boy" for whom the original film was named was Jimmy Cly, Elsie Maes cousin. 

== Films reception ==
The Return of Navajo Boy was aired on PBS November 13, 2000 List of Independent Lens films#Season 2 (2000). It has won awards at film festivals and is regularly screened at activist events, in public libraries and colleges, where it used for education related to the issues covered in the film. 

Elsie Mae Begay has become a public activist, telling her family and the Navajo Nations story on college campuses and to Congress, to try to have practices changed and such health hazards controlled.  Her daughter-in-law, Mary Helen Begay, has been filming webisodes of the EPA clean-up effort, with a camera supplied by Groundswell Educational Films, which produced the documentary.

With an updated epilogue in 2008, the film was shown on Capitol Hill in Washington, DC to Congressional and EPA staff.  In 2008 Congress authorized a five-year, five-agency clean-up plan to mitigate environmental contamination on the Navajo reservation. 

Since the film was made, Bernie Cly, one of the Navajo family featured, has been awarded $100,000 in compensation from the US government under the 1990 Radiation Exposure Compensation Act. The legislation was passed to compensate mine workers and residents for environmental damages due to uranium mining, especially from the 1950s through the 1970s, as the US government was the sole purchaser of the product.  , Press Release, 22 August 2011, accessed 5 October 2011 

==Changes in law and practice==
The Navajo Nation had long been concerned about the effects of uranium mining on their members at the reservation, due to longterm effects from direct and indirect exposure to contaminants. Their EPA has identified numerous sites that need hazardous waste remediation. In 2005, the Nation was the first indigenous nation to prohibit such mining on its reservation. 

Following identification of contaminated water and structures, the United States Environmental Protection Agency (EPA) and the Navajo Nation have developed a five-year, multi-agency plan to clean up contamination from the sites.  In 2011, it was completing the first major project, to remove 20,000 cubic yards of materials from the abandoned Skyline Mine, near Begays former home. Progress on reservation uranium clean-up by the government is documented with video webisodes online.  , Associated Press, carried in Houston Chronicle, 5 September 2011, accessed 5 October 2011 

In early 2010, the Indian Health Service began using the documentary as an outreach tool to raise awareness about uranium contamination and health issues on the Navajo Nation reservation.  In 2014, Amy Goodman used segments from The Return of Navajo Boy to illustrate the effects of uranium mining on Navajo land as part of the Democracy Now! program, “A Slow-Motion Genocide.” 

==Honors==
*Official selection, 2000 Sundance Film Festival
*Best Documentary, Indian Summer Festival
*Programmers Choice Award, Planet in Focus Festival
*Audience Award, Durango International Film Festival

==See also==
*The Navajo People and Uranium Mining
*Uranium mining and the Navajo people
*Church Rock uranium mill spill

== References ==
 

== External links ==
*  , includes links to Webisodes of EPA clean-up of reservation

 
 
 
 
 
 
 
 
 
 
 
 