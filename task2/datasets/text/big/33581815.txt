Deadly Love
{{Infobox television film
| name         = Deadly Love
| image        = Susan-dey deadly-love cover.jpg
| caption      = VHS cover
| runtime      = 104 minutes
| genre        = Action Horror Thriller
| writer       = Rob Gilmer Sherry Gottlieb
| director     = Jorge Montessi
| producer     = Les Alexander Don Enright Clara George David N. Gottlieb Julian Marks Jean LeClerc ABC Network
| editing      = Pia Di Ciaula
| music        = Micky Erbe Maribeth Solomon
| country      = United States
| language     = English Lifetime Television Network
| released     = October 16, 1995
}}
 Lifetime Original Movie starring Susan Dey as a lonely vampire photographer. The film was based on the book Love Bites by Sherry Gottlieb and co-stars Stephen McHattie. 

==Plot==
Rebecca Barnes (Susan Dey) is a successful photographer who has it all—including the curse of vampiric immortality.  Longing for companionship, Barnes leaves a disastrous trail of blood-less bodies in her wake.  Shockingly, photographs that she snapped of one of the victims brings Rebecca into the police investigation and into the arms of Detective Sean OConnor (Stephen McHattie). As the passion between Sean and Rebecca mounts, so does the evidence against her. 

==Cast==
*Susan Dey ...  Rebecca Barnes
*Stephen McHattie ...  Sean OConnor
*Eric Peterson ...  Elliott
*Julie Khaner ...  Poole
*Robert S. Woods ...  Jim King Jean LeClerc ... Trombitas Dracu
*David Ferry ...  Sal Consentino
*Roman Podhora ...  Steve Merritt
*Henry Alessandroni ...  Forman
*Kelly Fiddick ...  Griffith
*Suzanne Coy ...  Rita Berwald
*Jim Codrington ...  Derek Green
*Bernard Browne ...  Cab Driver
*Kevin Le Roy ...  Firebreather

==Reviews==
"Deadly Love" is an outstanding piece of reverse casting. Susan Dey, whose middle name is Nice, plays the vampire, and Stephen McHattie, Mr. Menace, is the cop trying to stop a series of murders. Because neither of these actors is playing to type, we dont know until the end which of the captivated lovers is the bigger threat to the other. When McHattie is on the screen, he is the one you look at. Dey had to give a strong performance to live up to him, and succeeded."  

==References==
 

==External links==
 
*  

 
 
 
 
 