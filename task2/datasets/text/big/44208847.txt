Waris (1969 film)
{{Infobox film
| name           = Waris
| image          =
| image_size     = Poster of Waris
| caption        = 
| director       = Ramanna
| producer       = Vasu Menon
| writer         = Rajendra Krishan
| narrator       = 
| starring       = Jeetendra   Hema Malini   Prem Chopra   Kamini Kaushal   Aruna Irani   Mehmood
| music          = Rahul Dev Burman
| cinematography = M. A. Rehman
| editing        = M. S. Money
| distributor    = 
| studio         = Vasu Studios
| released       = 1969
| runtime        = 158 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1969 Hindi family comedy drama  film directed by Ramanna.    A Vasu Films production it was produced by Vasu Menon.  The story writer was N. S. Bedi with dialogues and lyrics by Rajendra Krishan.  The music was composed by R. D. Burman.  Starring Jeetendra and Hema Malini in the lead it had Prem Chopra, Aruna Irani, Kamini Kaushal, Mehmood, S. A. Asokan in supporting roles.  

The story revolved around a young prince with socialist ideas who runs away from the palace.Twenty years later, three pretenders arrive each claiming to be the real prince.


==Plot==
This is a film based on a story of kings. Due to disagreements in the Kings household, his wife and son (Jeetendra) decide  to move away from his kingdom. Years later, the King passes away and  the palace decides to locate the Prince, so that he can claim his rightful place on the throne. They locate three young men, all claiming  to be the prince. The Palace officials  put them to the test, and one by one, all of them pass various tests  imposed on them, leaving the officials in a quandary, as to who the real prince is between these three young men. 

==Cast==
* Jeetendra as Ram Kumar #2
* Hema Malini as Geeta
* Prem Chopra as Ram Kumar #1 Mehmood as Ram Kumar #3
* Sudesh Kumar as  Ram Kumar #4 David as Diwanji
* Baby Sonia Neetu Singh
* Daisy Irani
* Chaman Puri as James Sunder as Birbal Sachin as Young Ram Kumar
* Suraj as Prem Kumar
* Ashokan 
* Nazima as Komal
* Kamini Kaushal as Rukmani  Manorama as Thakurain

==Reception==
The film was well-received by the audience. Hema Malinis performance was appreciated in her her first important and successful role. It was a commercial success and turned out to be the 9th highest grossing Bollywood film of 1969.  

Mehmood and Aruna Irani had made a successful comedy team in Aulad (1968 film)|Aulad (1968) produced a year earlier. Their next pairing was Waris and the audience appreciation helped in their working together in several films.   

==Music== Brahmachari (1968) and An Evening In Paris (1967).    
===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! #!!Title !! Singer(s) !! Duration
|-
| 1
| "Chahe Koi Mujhe Bhoot Kaho" 
| Mohammed Rafi, Asha Bhosle || 4:38 
|-
| 2
| "Parody Song" 
| Mohammed Rafi, Asha Bhosle || 4:37 
|-
| 3`
| "Title Music"
| - || 1:41 
|-
| 4
| "Lehra Ke Aya Hain Jhoka Bahar Ka"
| Lata Mangeshkar, Mohammed Rafi || 4:22 
|-
| 5
| "Kabhi Kabhi Aisa Bhi Hota Hain"
| Lata Mangeshkar, Mohammed Rafi || 3:31
|-
| 6
| "Dil Ki Lagi Ko Chhupaoon Kaise" 
| Lata Mangeshkar || 3:14 
|-
| 7
| "Ek Bechara Pyaar Ka Mara" 
| Mohammed Rafi || 3:37 
|-
|}

==References==
 

==External links==
* 


 
 
 