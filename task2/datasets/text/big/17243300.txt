Hello, I'm Your Aunt!
{{Infobox Film name = Hello, Im Your Aunt! image = Ja vasha tyotya.jpg image_size =  caption = DVD cover director = Viktor Titov producer =  writer =  narrator =  starring =  music =  cinematography =  editing =  studio = Studio Ekran distributor =  released = 1975 runtime = 98 min. country = Soviet Union language = Russian budget =  gross =  preceded_by =  followed_by = 
}} musical comedy film directed by Viktor Titov and is loosely based on the play Charleys Aunt by Brandon Thomas. Runtime - 98 min. Produced by Studio Ekran|T/O Ekran. The film was an immense hit; many lines of dialogue subsequently became catch phrases themselves.

==Plot summary==
The action takes place at the beginning of 20th century.

Unemployed and homeless Babbs Baberley (Alexander Kalyagin) is being chased by the police who attempt to arrest him for vagrancy. Babbs finds himself in a rich house, where he encounters Charlie and Jackie. Babbs unsuccessful attempt to disguise himself as a woman gives Charlie and Jackie an idea. By threatening to surrender Babbs to the (successfully bribed) police, they force their unexpected visitor to dress once again as a woman and pass himself for Donna Rosa dAlvadorez, Charlies millionaire aunt who is expected to arrive with a visit from Brazil. Charlie and Jackie want Babbs to seduce Judge Criggs (Armen Dzhigarkhanyan) with the irresistible charms of a millionaire widow and to trick the Judge into giving his nieces, Annie and Betty, a permission to marry Charlie and Jackie.

Complications to the scheme ensue. First, Charlies father Colonel Chesney (Mikhail Kozakov) decides to help his shattered finances by marrying the rich widow and joins Judge Criggs in courting the fake Aunt Rosa. Next, the real Donna Rosa arrives with her ward Ela. Upon encountering the impostor, Donna Rosa decides to remain incognito that gives her a great opportunity to observe and understand all the participants of the scheme. Babbs falls in love with Ela and is tortured by impossibility to reveal himself.

In the end, the fake Donna Rosa refuses to marry Colonel Chesney, and acquiesces to the courtings of Judge Criggs. The marriage permission for Annie and Betty is secured, and Babbs reveals himself to the company dressed as a man. Judge Criggs is incensed, but Donna Rosa reveals herself as the real aunt. The Judge and the Colonel rush to court her anew. Everyone exits the stage; Ela, despite being enchanted by the transformed "aunt", reluctantly follows. Babbs tries to follow the crowd only to see the door shut in his face. He starts desperately knocking on the door—only to wake up on a park bench pounded by a constables club.

Interesting facts: Dzhigarhanyan originally auditioned for the part of Colonel Chesney, whom he played in the theater adaptation. However, the film director Viktor Titov saw Dzhigarkhanyan in the role of Judge Criggs.

== Famous quotes ==

Babbs: "I am Charlies aunt! I arrived from Brazil, the country of a lot of wild monkeys!"

Colonel Chesney: "I am an old warrior! and I know not the language of love!"

Babbs (after refusing the Сolonels proposal): "No, no, no! He is not in love with me! He is in love with my millions!"

Charlie, after Babbs turning down of the Colonels proposal: "Why? why did you brute have to make fun of him?" — Babbs, coquettishly: "I had to torture him a little bit, hadnt I?"

Babbs (to Judge Criggs, after securing the marriage permission): "I will kiss you later. If you still want it."

==Cast==
* Alexander Kalyagin as Babbs Babberley
* Tamara Nosova as Donna Rosa dAlvadorez (note - Donna Lucia in the play)
* Tatyana Vedeneyeva as Ela Delahay
* Valentin Gaft as Brasset the footman
* Mikhail Lyubeznov as Charley Wyckham
* Oleg Shklovsky as Jackie Chesney
* Mikhail Kozakov as Colonel Sir Francis Chesney
* Armen Dzhigarkhanyan as Judge Criggs
* Galina Orlova as Bettie
* Tatyana Vasilyeva as Annie
* Viktor Gajnov
* Vladimir Korovkin
* Nikolai Tagin
* Anton Makarov
* Anatoli Malashkin Yuri Prokhorov
* Rogvold Sukhoverko

==Interesting facts==
* Original Donna Lucia in the film became Donna Rosa and the phrase "where nuts come from" became "where there are a lot of wild monkeys".
* The films title became a Russian figure of speech, exclaimed when somebody receives some shocking news that he or she can hardly believe (akin to the English phrase, "Well Ill be a monkeys uncle!").
* Famous soundtrack  Love and poverty ( ) was based on Poortith Cauld And Restless Love, the poem by Robert Burns translated by Samuil Marshak and features music by Vladislav Kazenin performed by Alexander Kalyagin.
* "Colonel Bogey March" used as Colonel Sir Francis Chesneys theme.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 