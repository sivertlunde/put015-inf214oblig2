Subapradam
 
{{Infobox film
| name           = Subhapradam
| image          = Shubhapradam.jpg
| alt            =  
| caption        = 
| director       = K. Viswanath
| producer       = Hari Gopalakrishna Pheela Neela Thilak 
| writer         = 
| screenplay     = K. Viswanath
| story          = K. Viswanath
| starring       = Allari Naresh Manjari Phadnis Ananth
| music          = Mani Sharma
| cinematography = 
| editing        = 
| studio         = Sri Rajeswari Combines
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}}

Subhapradam ( ) is a film directed by veteran director K. Viswanath, starring Allari Naresh and Manjari Phadnis.

==Plot== Ashok Kumar and Gundu Sudharshan) whose wives are from Chennai and Kolkata respectively. Thus entire India lives in her family. She is good at speaking Telugu, Malayalam, Bengali and Tamil. She happens to see Chakri (Allari Naresh), accidentally who is a good singer but not a professional one. Chakri falls in her love and the response from other side also comes good. But Chakri has a peculiar profession that gets revealed amidst the movie. That is carrying the old and disabled on his back for climbing the hill which is the abode of a goddess.
 
But eventually, Indu happens to encounter a rich man (Sharath Babu) that turns her life. The major twist in the story appears there. Who is that rich man? The story goes like this. He had a grand daughter by name Sindhu who looks perfectly like Indu. Sindhu dies in an acid attack followed by ragging and hence when he sees the look-alike of his grand daughter in Indu, he follows her. What happens later becomes rest of the movie in this confused story line.

==Cast==
* Allari Naresh
* Manjari Phadnis
* Sharath Babu
* Vizag Prasad
* Giribabu
* Raghu Babu Rallapalli
* Jenny
* Ashok Kumar
* Gundu Sudharshan

==Soundtrack==
{{Infobox album
| Name = Subhapradam
| Longtype = to Subhapradam
| Type = Soundtrack
| Artist = Mani Sharma
| Cover =
| Released = 20 June 2010
| Recorded = 2010 Feature film soundtrack
| Length =  Telugu
| Label = Sa Re Ga Ma
| Producer = Mani Sharma
| Reviews =
| Last album = Maanja Velu (2010)
| This album = Subhapradam (2010)
| Next album = Don Seenu (2010)
}}

The audio of Subhapradam was released on June 20, 2010. Chief Minister K Rosaiah unveiled the CDs and handed over to SP Balasubramanyam. The film has music scored by Mani Sharma and lyrics are penned by Seetarama Sastry, Rama Jogayya Sastry, Ram Bhotla and Anantha Sriram.

{| class="wikitable"
|-
! # !! Song title !! Lyrics !! Singers 
|- Chitra
|- 2 || "Mouname Chebutondi  " || Anantha Sreeram  ||S. P. Balasubrahmanyam, Pranavi
|-
| 3||  "Yelelo Yelelo"  || Anantha Sreeram || S. P. Balasubrahmanyam,  Shankar Mahadevan 
|- Sunitha
|- Malavika
|-
| 6 || "Orimi Chalamma O Bhumatoi" || Sirivennela Sitaramasastri || Rita
|-
| 7 || "Ambaparaaku Deviparaaku" || Traditional Hymn || D.S.V. Sastry
|}

==References==
*http://www.hindu.com/2010/07/28/stories/2010072863550300.htm
*http://www.cinegoer.com/telugu-cinema/sunitas-reviews/subhapradam-movie-review-160710.html
*http://telugu.16reels.com/news/movie/1278_k-viswanath-directed-subhapradam-audio-released.aspx

 
 
 
 