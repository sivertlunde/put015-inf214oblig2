Star Reporter
{{Infobox film
| name           = Star Reporter
| image          = Star Reporter (1939).jpg
| image_size     = 150px Marsha Hunt, Warren Hull,   Virginia Howell in   Star Reporter (1939)
| director       = Howard Bretherton
| producer       = E.B. Derr (producer) Frank Melford (associate producer)
| writer         = John T. Neville
| narrator       =
| starring       = See below
| music          =
| cinematography = Arthur Martinelli
| editing        = Russell F. Schoengarth
| distributor    = Monogram Pictures
| released       = 22 February 1939
| runtime        = 62 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Star Reporter is a 1939 American film directed by Howard Bretherton.

==Plot== Marsha Hunt), has faith in her Father, D.A. William Burnette (Wallis Clark), and throws the full weight of his newspaper behind him, in hopes of tracking down his own Father’s killer.

John is convinced that his Father was murdered to stop him from revealing the organized crime bosses, in the city. Now, all he needs is proof.

Just as he’s about to get the goods on the criminal kingpin, lawyer Whittaker (Clay Clement), there’s another murder.

Little does John suspect that the confessed killer, Joe Draper (Morgan Wallace) and his own Mother, Mrs. Julia Randolph (Virginia Howell) have their own deep, dark secret, from the past: the true identity of her long, lost, already declared dead, husband; and, Johns real Father.

Whittaker and his mobsters will do anything to close the case. Theyre willing to shut anyone up permanently, who they cant buy off.

John will stop at nothing, to see justice done, even when his own fiancée and Mother warn him that he might not be ready to handle the truth!

== Cast ==

*Warren Hull as John Randolph, aka John Charles Benton Marsha Hunt as Barbara Burnette
*Wallis Clark as District Attorney William Burnette
*Clay Clement as Whittaker
*Morgan Wallace as Joe Draper, aka Charles Benton
*Virginia Howell as Mrs. Julia Randolph
*Paul Fix as Clipper
*Joseph Crehan as Gordon, Newspaper Editor
*Eddie Kane as Sam Grey
*William Ruhl as Police Investigator Lane
*Effie Anderson as Molly, D.A.s Secretary
*Lester Dorr as Reporter Wilkins
*Monte Collins as Reporter Hogan
*Denis Tankard as Mason, Burnettes Butler

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 