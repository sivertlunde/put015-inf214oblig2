All Good Things (film)
{{Infobox film
| name           = All Good Things
| image          = All Good Things poster.jpg
| border         = yes
| caption        = Theatrical Poster
| director       = Andrew Jarecki
| producer       = Andrew Jarecki Michael London Bruna Papandrea Marc Smerling
| writer         = Marcus Hinchey Marc Smerling
| starring       = Ryan Gosling Kirsten Dunst Frank Langella Philip Baker Hall
| music          = Rob Simonsen
| cinematography = Michael Seresin
| editing        = David Rosenbloom Shelby Siegel
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $644,535 	
}} crime romantic drama film directed by Andrew Jarecki starring Ryan Gosling and Kirsten Dunst. Inspired by the life of accused murderer Robert Durst, the film chronicles the life of the wealthy son of a New York real estate tycoon, and a series of murders linked to him, as well as his volatile relationship with his wife and her subsequent unsolved disappearance.

All Good Things was filmed between April and July 2008 in Connecticut and New York. The film was originally scheduled for a July 24, 2009, release, but was further delayed with a limited release of December 3, 2010.      
 American cable first degree murder charges the day before its final episode—in which he appeared to unintentionally confess to three murders—aired on March 15. 

==Plot==
In 1970s New York City, David Marks (Gosling), the son of a powerful real estate tycoon, marries a beautiful working-class student, Katie McCarthy (Dunst). Together they flee the city for country life in Vermont—only to be lured back by Davids father. Upon their return, they buy a beautiful apartment where Katie brings up the idea of having children which David implies he cant have any. They eventually buy a lake house out of town and Katie tells their new pregnant neighbor that she is expecting as well. Katie tells David to which he responds by throwing a chair and breaking a shelf. David makes Katie have an abortion, which he misses while doing work for his dad. Katie goes back to college and eventually applies and gets into a medical school. During a celebration party at her parents house, David drags Katie out by her hair when he wants to go home and she asks him to wait. Katie wants a separation but her funds, which she needs in order to graduate, are cut off when she attempts this. David gets violent and Katie begins to show signs of abuse. Family secrets are slowly revealed, and then Katie disappears without a trace.  Years later, when Davids best friend Deborah Lehrman is found dead, the 20-year-old case is re-opened, with David as the main suspect.

==Cast==
* Ryan Gosling as David Marks    (Robert Durst) Kathleen McCormack)
* Frank Langella as Sanford Marks    (Seymour Durst)
* Lily Rabe as Deborah Lehrman   (Susan Berman) Morris Black)
* Michael Esper as Daniel Marks (Douglas Durst)
* Diane Venora as Janet Rizzo  (Jeanine F. Pirro)
* Nick Offerman as Jim McCarthy   (Jim McCormack)
* Kristen Wiig as Lauren Fleck  
* Stephen Kunken as Todd Fleck
* John Cullum as Richard Panatierre (Dick DeGuerin)
* Maggie Kiley as Mary McCarthy
* Liz Stauber as Sharon McCarthy
* Marion McCorry as Ann McCarthy (Ann McCormack)

==Production==
The All Good Things screenplay was written by Marcus Hinchey and Marc Smerling as a narrative loosely based on the real life experiences of Robert Durst, a real estate heir whose first wife, Kathleen McCormack, disappeared in 1982.     The films title is a reference to a health store of the same name set up by Durst and McCormack in the 1970s.  After the script was completed and Andrew Jarecki had agreed to direct the film, Ryan Gosling was attached to star and Kirsten Dunst in negotiations by late January 2008.  By early April, Frank Langella was in final negotiations with the films producers to join.  Soon after, The Weinstein Company closed a deal to distribute All Good Things, and the films budget was set at United States dollar|US$20 million. 

Filming began in April in New York City and various locations in Connecticut,  which were chosen for "the tax incentive, scenic and period locations" provided by the state.    Shooting on Lillinonah Drive in Brookfield, Connecticut commenced in early May, at a lakefront house.    Five locations at the Fairfield University campus were used for several scenes over a week of filming.    The set moved to Carl Schurz Park, New York City, briefly before switching back to Connecticut.  Three scenes were shot at Canal Street, Shelton, Connecticut, on May 30–31 after the production teams filming license for the scenes was completed less than a week beforehand. Much of the Canal Street filming focused on the "heavy, industrial features" of the area, while other touch-ups such as graffiti removal were made. 

A single minute-long scene was shot on a bridge over the Housatonic River.  Scenes were shot on Route 7 in Gaylordsville, Connecticut, on June 3, where a shop opposite the local fire department was used as a health store.  The following day, filming commenced in Waterbury, Connecticut.  The Hospital of Saint Raphael was used as a filming location on June 6. The film set at the hospital was built on a vacant floor scheduled to be renovated, and took a week for set designers to prepare.  Filming later returned to Brookfield, Connecticut,  and shot for two days at the Ridgefield Community Center—standing in for New Yorks Gracie Mansion—in Ridgefield, Connecticut.  Manhattan, New York|Manhattan, New York Citys West 38th Street, between 7th and 8th Avenues, stood in for the old 42nd Street on June 25–26 where shops were converted into 1970s Times Square sex shops and strip shows.  

Jarecki, who previously produced and directed the 2003 documentary Capturing the Friedmans, said that making All Good Things "was less about wanting to do a narrative feature vs. a documentary and more about the merits of this particular project".    He shot "hundreds of hours of footage" of real people associated with the true story of Robert Durst, saying that "It was part of the process. Maybe it will end up on the DVD some day." 

The footage eventually became the 2015  . 

==Release==
The film was originally set for release on July 24, 2009.  In spring 2009, the film was delayed. An insider from The Weinstein Company stated that "the movie is really strong. We just needed more time to complete it."  Soon after, the film was set to release on December 11, 2009, only to be delayed again.  The Weinstein Company released their upcoming film slate, with All Good Things listed for a March 2010 release. This never materialized. 

In March 2010, director Andrew Jarecki bought back the US distribution rights and was searching for a new distributor for the film. The Weinstein Company still holds the international rights, as well as basic cable television rights.  On August 24, 2010, Magnolia Pictures acquired the American rights to the film and gave the film a theatrical release on December 3, 2010.  

All Good Things was released on DVD and Blu-ray on March 29, 2011,  with commentary by Jarecki and Robert Durst.   

===Critical reception===
All Good Things met with mixed reviews from critics. On review aggregate   and Ryan Gosling have been praised for their performances. Roger Ebert awarded the film three and a half out of four stars, applauded Dunsts performance, and said, "I dont understand David Marks after seeing this film, and I dont know if Andrew Jarecki does." 

===Box office===
All Good Things earned $582,024 at the domestic box office and another $62,511 at the foreign box office for a worldwide total of $644,535. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 