Cowboy from Brooklyn
{{Infobox film name           = Cowboy from Brooklyn image          = Cowboy from Brooklyn.JPG caption        = Warners promotional poster director       = Lloyd Bacon producer       = writer         = Robert Sloane Louis Pelletier starring  Pat OBrien Dick Powell Priscilla Lane Ann Sheridan Ronald Reagan Dick Foran Johnnie Davis music          = Leo F. Forbstein Adolph Deutsch Adolph Deutsch distributor    = Warner Bros. released       =   runtime        = 77 min. country        = United States language       = English budget         = gross          =
}}
 , in the trailer for Cowboy from Brooklyn (1938).]] Pat OBrien, Dick Powell, Priscilla Lane, Ann Sheridan and the future US President Ronald Reagan. 

==Plot==
Singer Elly Jordan, a Brooklyn man who is terrified of animals, ends up broke along with his two musical partners at Hardys Dude Ranch in Two Bits, Wyoming. The Hardys, Ma and Pop, daughter Jane and son Jeff, hire the men to play for the dudes. Sam Thorne, Janes self-appointed boyfriend, ranch cowhand and amateur crooner, is jealous of Janes interest in Elly. Elly is so successful as a cowboy singer, that when theatrical agent Ray Chadwick arrives at the ranch on a vacation and hears him, he signs Elly immediately. Chadwick thinks that Elly is a real cowboy and Jane coaches him to talk like one. In spite of his fear of animals, he gets away with the deception. He makes a successful screen test as a cowboy, using the name Wyoming Steve Gibson, but he and Chadwick, who now knows the truth, fear that the deception will be revealed when the movie people arrive in New York from Hollywood with Ellys contract.
 
Meanwhile, Jane and some of the ranch people are traveling East as well so Sam can sing on Captain Roses Amateur Hour in New York. Jane tells Sam that she is in love with Elly and Sam is so angry that when he isnt a big success on the show, he blurts out the truth about Ellys background. To prove that Elly is on the level, Chadwick and his assistant Pat Dunn suggest that he compete in a rodeo. They take Elly to Professor Landis, who hypnotizes him. Under hypnosis, Elly leaps on a horse, rides to Madison Square Garden, enters the bulldogging contest and sets a new record. He sneezes and wakes from the hypnosis, but the movie people are convinced that he is a real cowboy. He signs the contract and kisses Jane to seal the deal.

==List of songs==
* I Got a Heartful of Sunshine - Candy Candido and Dick Powell
* Ride, Tenderfoot, Ride - Priscilla Lane and Dick Powell
* Git Along Little Doggie - Johnnie Davis
* Ill Dream Tonight - Dick Powe
* Howdy, Stranger - Dick Powell
* Ride, Tenderfoot, Ride - Priscilla Lane and Dick Powell

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 