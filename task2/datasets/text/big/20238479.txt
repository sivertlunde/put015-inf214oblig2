The Battle at Apache Pass
{{Infobox film
| name           = The Battle at Apache Pass
| image          = BattleApachePass.jpg
| image_size     =
| caption        = Theatrical poster
| director       = George Sherman
| producer       = Leonard Goldstein
| writer         = Gerald Drayson Adams
| narrator       = Jeff Chandler John Lund
| music          = Hans J. Salter
| cinematography = Charles P. Boyle
| editing        = Ted J. Kent
| distributor    = Universal-International
| released       =  
| runtime        =
| country        = United States English
| budget         = $681,000
| gross          = $2 million (US rentals) 
}} John Lund Jeff Chandler Broken Arrow. 
 The Bascom Affair" of 1861 and the "Battle of Apache Pass" of 1862, the first time that the Indians meet modern (for the age) artillery, in the Confederate Territory of Arizona.

==Plot== Civil War Southeastern states, Southwest is Fort Buchanans John Lund) Jeff Chandler) Mogollon Apaches Tommy Cook). Richard Egan) informs him of the situation, causing Colton to shut down Fort Buchanan and prepare to transfer everyone, including Mary and Baylor, to Fort Sheridan, a more secure redoubt, some distance away. From the hills along the trail, Cochise watches the procession, as does Geronimo and, as shooting begins, the wounded Baylor goes towards the Indians positions, shouting that he is their friend, but Geronimo kills him. Colton and Sergeant Bernard use the expeditions cannon to rout the warriors, as Cochise finds Nona, who has been hurt, and takes her to the wagons so that Army doctor Carter (Regis Toomey) can treat her. Geronimo calls Cochise a weak leader, but in a one-to-one battle, Cochise wins and, instead of killing Geronimo, banishes him. Nonas son is born and Nona gives her friend Mary a precious Apache bracelet. Colton and Mary look at each other with affection and Cochise tells them that time has come for peace, as he rides away with Nona.

==Cast==
===Starring=== John Lund {Major Colton} Jeff Chandler {Cochise}
 
 

===With===
*Susan Cabot {Nona}
*Bruce Cowling {Neil Baylor}
*Beverly Tyler {Mary Kearney} Richard Egan {Sergeant Bernard}
*Jay Silverheels {Geronimo} John Hudson {Lieutenant Bascom}
*Jack Elam {Mescal Jack}
 
*Regis Toomey {Doctor Carter} Tommy Cook {Little Elk}
*Hugh OBrian {Lieutenant Harley}
*James Best {Corporal Hassett}
*Richard Garland {George Culver}
*Palmer Lee {Joe Bent} William Reynolds {Lem Bent} Paul Smith {Trumpeter Ross} Jack Ingram {Johnny Ward}
 

==Evaluation in film guides==
Leonard Maltins Movie Guide gives The Battle at Apache Pass 2½ stars (out of 4) indicating that "Chandler reprises his BROKEN ARROW role as Cochise". The Motion Picture Guide assigns it 2 stars (out of 5), describing it as " ot the greatest western ever made, but interesting for its portrayal of Indians as rational human beings able to cooperate with the white man".

==References==
 

==External links==
* 
* 
* 
* 
*  at TV Guide (1987 write-up was originally published in The Motion Picture Guide)

 

 
 
 
 
 
 
 