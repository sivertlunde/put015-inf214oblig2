Winter Soldier (film)
 

{{Infobox Film
| name           = Winter Soldier
| image          = Winter soldier video.jpg
| image_size     = 
| caption        = 
| director       = 
| producer       =Vietnam Veterans Against the War   Winterfilm Collective 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Milliarium Zero
| released       = 27 January 1972
| runtime        = 96 min.
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1972 documentary film chronicling the Winter Soldier Investigation which took place in Detroit, Michigan, from January 31 to February 2, 1971. The film documents the accounts of United States soldiers who returned from Vietnam, and participated in this war crimes hearing.

The Winterfilm Collective (listed as Winterfilm, Inc. in the credits to the film Winter Soldier) consisted of: Rusty Sachs, Barbara Kopple, Fred Aranow, Nancy Baker, Joe Bangert, Rhetta Barron, Robert Fiore, David Gillis, David Grubin, Jeff Holstein, Barbara Jarvis, Al Kaupas, Mark Lenix, Michael Lesser, Nancy Miller, Lee Osborne, Lucy Massie Phenix, Roger Phenix, Benay Rubenstein, and Michael Weil. 

== Participants ==

Soldiers who appeared in the film included (in order of appearance):
*Rusty Sachs, 1st Marine Air Wing
*Joseph Bangert, 1st Marine Air Wing
*Scott Shimabukuro, 3rd Marine Division
*Kenneth Campbell, 1st Marine Division
*Scott Camil, 1st Marine Division
*John Kerry, Coastal Divisions 11 & 13, USN
*Steve Pitkin, 9th Infantry Division
*Jonathan Birch, 3rd Marine Division
*Charles Stevens, 101st Airborne Division
*Fred Nienke, 1st Marine Division
*David Bishop, 1st Marine Division
*Nathan Hale, Americal Division
*Michael Hunter, 1st Infantry Division
*Murphy Lloyd, 173rd Airborne Brigade
*Carl Rippberger, 9th Infantry Division
*Evan Haney, US Naval Support Activity
*Robert Clark, 3rd Marine Division
*Gordon Stewart, 3rd Marine Division
*Curtis Windgrodsky, Americal Division
*Gary Keyes, Americal Division
*Allan Akers, 3rd Marine Division
*William Hatton, 3rd Marine Division
*Joseph Galbally, Americal Division
*Edmund Murphy, Americal Division
*James Duffy, 1st Air Cavalry Division
*Scott Moore, 9th Infantry Division
*Mark Lenix, 9th Infantry Division
*Thomas Heidtman, 1st Marine Division
*Dennis Caldwell, 1st Aviation Brigade
*James Henry Winter Soldier movie and production credits 

The collective produced the 1971 film documentary about the Winter Soldier Hearings in Detroit, as well as associated anti-war protests and marches.

== War crimes allegations ==

The film, shot largely in black and white, features testimony by soldiers who participated in or witnessed atrocities in Vietnam, including the killing of civilians, including children; mutilation of bodies; indiscriminate razing of villages; throwing prisoners out of helicopters; and other acts of cruelty towards Vietnamese civilians and combatants. Some participants also claimed that these acts reflected orders from higher-up officers. A number of soldiers are quoted stating that their military training failed to include instruction in the terms of the Geneva Convention, while others state that the dangers they faced as soldiers created an environment in which they regarded all Vietnamese as hostile "gooks" and stopped seeing them as human beings.

In testimony by Joseph Bangert, he describes traveling in a "truckload of grunt Marines" when "there were some Vietnamese children at the gateway of the village and they gave the old finger gesture at us. It was understandable that they picked this up from GIs there. They stopped the trucks -- they didnt stop the truck, they slowed down a little bit, and it was just like response, the guys got up, including the lieutenants, and just blew all the kids away. There were about five or six kids blown away, and then the truck just continued down the hill."

In addition to soldiers testimony, the film includes color footage and photographic evidence to support some of the testimony. 

== Reception ==

At the time of its original release in 1972, Winter Soldier was greeted with skepticism and largely ignored by the mainstream media. "Only the local Detroit Free Press bothered to confirm the veracity of accounts and the credentials of participants," according to Johnny Ray Huston in a 2005 review of the film and its impact. "Television primarily turned a blind eye, and conservative publications like the Detroit News cast doubt on the allegations made without offering any specific proof of deception."  The ABC Television|ABC, NBC, CBS and PBS television networks were offered opportunities to broadcast the film but declined.  For the first 30 years after its release, the film was shown sporadically in arthouse settings. 

In 2005, the film was re-released in theaters, this time attracting mostly favorable reviews. Writing in the Washington Post, Ann Hornaday called it "a riveting example of pure filmic storytelling. ... Winter Soldier is an important historical document, an eerily prescient antiwar plea and a dazzling example of filmmaking at its most iconographically potent. But at its best, it is the eloquent, unforgettable tale of profound moral reckoning." Ann Hornaday, " ," Washington Post, December 9, 2005; p. C5. 

The movie review Web site Rotten Tomatoes, where established film critics reviews are collected and an aggregate "Tomatometer" rating given to each film, lists "Winter Soldier" as 100% on the Tomatometer, with unanimous positive reviews of the film. 

== References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 