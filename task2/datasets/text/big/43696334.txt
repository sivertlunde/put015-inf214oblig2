The Fun, the Luck & the Tycoon
 
 
{{Infobox film
| name           = The Fun, the Luck & the Tycoon
| image          = TheFuntheLuck&theTycoon.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 吉星拱照
| simplified     = 吉星拱照
| pinyin         = Jí Xīng Gǒng Zhào
| jyutping       = Gat1 Sing1 Gung6 Ziu3 }}
| director       = Johnnie To
| producer       = Catherine Hun
| writer         = 
| screenplay     = Hoi Tik
| story          =
| based on       =   Nina Li Chi Lawrence Cheng
| narrator       = 
| music          = Lo Tayu Fabio Carli
| cinematography = Horace Wong
| editing        = Wong Ming Gong Cinema City Golden Princess Amusement
| released       =  
| runtime        = 86 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$20,292,057
}}
 Nina Li Chi and Lawrence Cheng. The film is a remake of the 1988 American film Coming to America.      

==Plot== Nina Li Chi) because all his property would belong to her. He just wants to live a happy life like an ordinary civilian. While his grandaunt was not home, Sang disguises as a civilian and runs away from him.
 Ha Yu). Sang was attracted by Yuks wisdom and beauty and he falls in love with her at first sight. Then, Sang becomes an employee at their fast food restaurant and he was able to get close to Yuk with the help of his loyal butler Fatso (Wong San). This causes Tungs plan to match-make her sister with wealthy man Jimmy Chiu (Lawrence Cheng) to fail.

With his new life, Sang has become very energetic. While working at the restaurant, he gets to know the smart and lovely little kid Rocky (Wong Kwan Yuen) and four strong-willed brothers (Beyond (band)|Beyond) whom are his colleagues. Sang has won Yuks appreciation and love for him. However, Jimmy, Cindy and Tung would not let things go smoothly. After many understandings, Sang was finally able to be together with Yuk and live a happy and simple life just like any other ordinary civilian.

==Cast==
*Chow Yun-fat as Lam Bo Sang / Stink (2 roles)
*Sylvia Chang as Hung Leung Yuk Nina Li Chi as Cindy Chan
*Lawrence Cheng as Jimmy Chiu
*Wong Kwan Yuen as Rocky Ma Ha Yu as Hung Tung Tung Beyond as Restaurant workers
*Wong San as Fatso
*Wong Man as Sangs mother
*Ouyang Shafei as Sangs grandaunt
*Bowie Wu as Barrister Cheung Raymond Wong as Priest (cameo)
*James Lai as Jeweller (cameo)
*Fung King Man as Doctor
*Ho Ling Ling as Rockys girlfriend
*Yue Ming as Sangs office manager
*Lo Hoi-pang as Senior Policeman
*Yeung Yau Cheung as Junior Policeman
*Albert Lo as Customer at restaurant (brief cameo)
*Leung Oi as Customer at restaurant (brief cameo)
*Cheng Siu Ping as Woman at office
*English Tang
*Alan Chui as Robber
*Ho Wan as Party guest
*Leung Hak Shun as Party guest
*Ho Chi Moon as Party guest
*Lee Wah Kon

==Reception==

===Critical===
Andrew Saroch   rated the film 3 out of 5 stars and writes "The Fun, The Luck And The Tycoon is not hilarious or particularly side-splitting, but the humour is such that it hits the mark in a subtle manner. Therefore this is an agreeable Chow Yun Fat comedy that is mostly successful in its modest ambitions." 

===Box office===
The film grossed HK$20,292,057 at the Hong Kong box office during its theatrical run from 18 January to 15 February 1990 in Hong Kong.

==See also==
*Chow Yun-fat filmography
*Johnnie To filmography

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 
 