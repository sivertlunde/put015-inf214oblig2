Gates of the Night
{{Infobox film name   = Gates of the Night image  = Les-portes-de-la-nuit-original.jpg caption = Theatrical release poster director = Marcel Carné screenplay  = Jacques Prévert runtime  = 120 minutes music  = Joseph Kosma studio  = Pathé|Pathé Consortium Cinema
|cinematography=  Philippe Agostini released  = 1946    starring   = Yves Montand, Serge Reggiani

}} Autumn Leaves" (French: Les feuilles mortes).

==Plot== French underground during World War II, meets Raymond, one of his comrades in arms who was believed to have succumbed in battle. On the night of that meeting, Jean encounters a homeless man named "Destiny", whose predictions about him finding the woman of his life will not be too far from reality. Jean soon starts a liaison with Malou (Nathalie Nattier), a young woman who is married to a rich man. The next hours of his and Malous lives are underscored by extreme, dramatic events; however, as the clochard (homeless person) predicted, they find their way out of the struggle and are able to move on, leaving behind wartime and its dangers.     

==Reception==
Les Portes de la nuit was released in the United States four years after it was first shown in France, where this psychological urban drama, especially due to its depiction of post-war Paris and close-to-dejected characters did not break the box office.  It has been said that this is not Yves Montands best performance, probably due to the fact that this was only his second film.  Overall, Les Portes de la nuit is considered hollow by many with regard to the plot; however, the settings are considered to be fascinating.    

== References ==
 
 

== External links ==
* 

 

 
 
 
 
 
 