Fermo con le mani
{{Infobox Film
| name           = Fermo con le mani - "Hands Off Me!"
| image          = Fermo con le mani !.png
| image_size     = 
| caption        = 
| director       = Gero Zambuto
| producer       = Gustavo Lombardo
| writer         = Guglielmo Giannini, Gero Zambuto
| narrator       = 
| starring       = Totò, Tina Pica
| music          = Umberto Mancini
| cinematography = Otello Martelli
| editing        = Giacinto Solito
| distributor    = Titanus
| released       = 1937
| runtime        = 77 minutes
| country        = Italy
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = }}

Fermo con le mani (Hand Off Me!) is an Italian 1937 film directed by Gero Zambuto.
 Cops and Robbers and Toto in Paris) and also the scene in which he holds a broom, using it as a gun (shooting in "Figaro here, Figaro there").  The scene involving the conductor was also repeated in many subsequent films.

== Plot ==
In his first story, Totò plays the poor wanderer who meets up with a gentleman in poor economic conditions. The two try to earn a living as best they can, but because of their ineptitude, always end up incurring misfortune and hatred of all. After being kicked out of a beauty salon because he was disguised as a nurse, Totò makes a bet between nobles and later wins a competition and a lot of money after replacing a famous conductor who was sick. Here Totò will show his theatrical flair performing in the famous gag of "Uncoordinated muppet". After having won the satisfactory sum, Totò will also discover to be of a noble family.

== Cast ==
*Totò: Count Totò di Torretota
*Tina Pica: Giulia
*Franco Coop: Vincenzino
*Oreste Bilancia: Cavalier Gerolamo Battaglia
*Erzsi Paál: Eva Frasorni
*Cesare Polacco: Capomastro  
*Guglielmo Sinaz: il capo cameriere
*Alfredo Martinelli: accompagnatore di Eva

==External links==
*  

 
 
 
 

 