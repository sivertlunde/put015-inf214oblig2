Goonj Uthi Shehnai
{{Infobox film
| name           = Goonj Uthi Shehnai
| image          = Goonj Uthi Shahnai, 1959 Hindi film poster.jpg
| image_size     = 190px
| alt            = 
| caption        = 
| director       = Vijay Bhatt
| producer       = Vijay Bhatt
| writer         = 
| narrator       = 
| starring       = Rajendra Kumar Ameeta Anita Guha I. S. Johar
| music          = Vasant Desai Bharat Vyas (Lyrics)
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1959
| runtime        = 174 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = Rs. 1,80,00,000  
}}
Goonj Uthi Shehnai ( ) (meaning The Wedding Bells Have Started Ringing) is 1959 Hindi film directed by Vijay Bhatt, with Rajendra Kumar, Ameeta, Anita Guha and I. S. Johar as leads. The film has music by Vasant Desai and lyrics by Bharat Vyas, and the duo created numerous hits like Jivan Men Piya Tera Sath Rahe sung by Lata Mangeshkar, Mohd. Rafi, and Latas Tere Sur Aur Mere Geet and Dil Ka Khilona Hai Toot Gaya.

The film narrates the story of a Shehnai player, and features a shehnai recitals by maestro, Ustad Bismillah Khan throughout the film.  There is also Jugalbandi between him and Sitar player, Abdul Halim Jaffar Khan  

The film was the fifth highest grosser of the year and declared a Hit at the Indian  , who was soon giving dates four years from the day of signing. 

==Cast==
* Rajendra Kumar - Kishan
* Ameeta - Gopi
* Anita Guha - Ramkali (Rami)
* I. S. Johar - Kanhaiya
* Ulhas	... 	Raghunath Maharaj
* Manmohan Krishna - Gangaram
* Leela Mishra	- Jamuna
* Pratap Kumar - Shekhar
* Prem Dhawan - Banjara
* Ram Moorti - Uncle

==Awards and nominations==
* 1960: Filmfare Award
**  : Nominated

== Soundtrack == Amir Khan, a Hindustani classical music vocalist   
{{Infobox album  
| Name        = Goonj Uthi Shehnai
| Type        = Soundtrack
| Artist      = Vasant Desai
| Cover       = 
| Released    = 1995 (India)
| Recorded    =  
| Genre       = Film soundtrack|
| Length      = 
| Label       = EMI 
| Producer    = Vasant Desai
| Reviews     = 
| Last album  = Samarat Prithviraj Chauvan    (1959)
| This album  = Goonj Uthi Shehnai  (1959)
| Next album  = Ardhangini     (1959)       
|}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer (s)
|-
| Teri Shehnai Bole  Lata Mangeshkar, Mohd. Rafi 
|-
|Tere Sur Aur Mere Geet
| Lata Mangeshkar
|-
|Maine Peena Seekh Liya
|Mohd. Rafi 
|-
| Jivan Men Piya Tera Sath Rahe Lata Mangeshkar & Pushpak, Mohd. Rafi 
|-
|Haule Haule Ghoonghat Pat Khole Lata Mangeshkar, Mohd. Rafi 
|-
|Akhiyan Bhool Gain Hain Sona Geeta Dutt, Lata Mangeshkar
|-
|Mujhko Agar Bhool Jaoge Tum Lata Mangeshkar
|-
| Kah Do Koi Na Kare Yahan Pyaar 
|Mohd. Rafi
|-
| Dil Ka Khilona Hai Toot Gaya  Lata Mangeshkar
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 