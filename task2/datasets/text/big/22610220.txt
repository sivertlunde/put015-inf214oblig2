Patriotic Martyr An Jung-gun
{{Infobox film
| name           = Patriotic Martyr An Jung-gun
| image          = Patriotic Martyr An Jung-gun.jpg
| caption        = Theatrical poster for Patriotic Martyr An Jung-gun (1972)
| director       = Joo Dong-jin 
| producer       = Ju Dong-jin
| writer         = Lee Jeong-seon Kim Jin-kyu
| music          =
| cinematography = Choi Keum-dong
| editing        =
| distributor    = Yeonbang Movies
| released       =  
| runtime        =
| country        = South Korea
| language       = Korean
| budget         =
| gross          =
| film name      = {{Film name
 | hangul         =  
 | hanja          =    
 | rr             = Uisa Ahn Jung-geun
 | mr             = ŭisa An Chung-kŭn 
 | context        =}}
}} 1972 South Korean film directed by Joo Dong-jin. It was chosen as Best Film at the Grand Bell Awards.     

==Synopsis==
A film biography of An Jung-geun, a Korean independence activist who assassinated Itō Hirobumi, the first Prime Minister of Japan. 

==Cast== Kim Jin-kyu 
* Park Nou-sik
* Moon Jung-suk
* Han Mun-jeong
* Choi Nam-Hyun
* Choi Bool-am
* Lee Dae-yub
* Hah Myung-joong
* Choe Seong-ho
* Park Am

==Notes==
 

==Bibliography==
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 


 