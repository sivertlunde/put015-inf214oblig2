The Little Kidnappers (1953 film)
{{Infobox film
| name           = The Little Kidnappers
| caption        = 
| image	=	The Little Kidnappers FilmPoster.jpeg
| director       = Philip Leacock
| producer       = Sergei Nolbandov Leslie Parkyn Neil Paterson Duncan Macrae Jon Whiteley Vincent Winter
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Neil Paterson. remade as a TV movie in 1990.

== Plot ==

In the 1900s, two young boys are sent to live in a Scottish settlement in Nova Scotia, Canada, with their stern Grandaddy (Macrae) and Grandma (Anderson) after their fathers death in the Boer War. The boys would love to have a dog but are not allowed, Grandaddy holding that "ye canna eat a dog". Then they find an abandoned baby. Living in fear of Grandaddy (he beats Harry, the older boy, for disobeying him), they conceal it from the adults.  They see it as a kind of substitute for the dog they have been denied (Davy, the younger boy, asks his brother, "Shall we call the baby Rover, Harry?").

Grandaddy is having problems with the Dutch settlers who have arrived at the settlement in increasing numbers after leaving South Africa at the end of the Boer War. He has had a long-running dispute with Afrikaaner Jan Hooft over ownership of a hill and refuses to accept a legal ruling that the land, in fact, belongs to Hooft. He also keeps a close rein on his grown-up daughter Kirsty (Corri) and is reluctant for her to make a life for herself. She is in love with the local doctor Willem Bloem, who left Holland for Canada for reasons he will not disclose. He does not return her affections.

To make matters worse, it turns out that the "kidnapped" baby is Hoofts younger daughter. When found out, Harry is tried at a court set up in the local trading store. He is suspected of taking her as a result of the tensions between the two families but states that he did not know her identity Surprisingly, Hooft speaks up in his defense, stating that no harm had come to her and his older daughter should have been looking after her. The court official suggests that Harry be sent to a corrective school, and is immediately threatened with shooting by Grandaddy. The clerk climbs down, merely suggesting an investigation into the location of these schools in case a further kidnapping should occur. Afterwards, Grandaddy thanks Hooft for speaking up for Harry.

The film ends with Grandaddy (who had never learned to read or write) instructing Harry to write to a mail order company to order the red setter they had set their hearts on. He had found the flyer for the dog in one of his best boots, where the boys had hidden it. They had noticed that he sometimes walked without these boots, slinging them over his shoulder, to save wear and tear. To pay for the dog, Grandaddy had sold them – a prized item among his few possessions. Davy is now able to say, "I think well call him Rover, Harry."

One of the films most memorable moments comes with the horror on Duncan Macraes face at what his grandson must have thought of him when he implores "Dont eat the babbie".

== Cast ==
 Duncan Macrae as Jim MacKenzie
* Jean Anderson as Grandma MacKenzie
* Adrienne Corri as Kirsty
* Theodore Bikel as Dr. Willem Bloem
* Jon Whiteley as Harry, Jims grandson
* Vincent Winter as Davy, another grandson
* Francis de Wolff as Jan Hooft Sr.
* James Sutherland as Arron McNab
* John Rae as Andrew McCleod   Jack Stewart as Dominie  
* Jameson Clark as Tom Cameron  
* Eric Woodburn as Sam Howie  
* Christopher Beeny as Jan Hooft Jr.

== Reception ==

The film was the eighth most popular movie at the British box office in 1954. 

== Awards ==
 Honorary Juvenile Acting Oscars for their performances.  In addition, the film was nominated for three BAFTA Film Awards and was entered into the 1954 Cannes Film Festival.   

== References ==

 

== External links ==

*  
*  

 
 
 
 
 
 
 
 
 
 