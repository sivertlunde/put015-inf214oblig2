The Blood Trail
{{Infobox film
| name           = The Blood Trail
| image          = 
| alt            =  
| caption        = 
| director       = Jerrold Tarog
| producer       = John Silva  Franco Alido
| writer         = Jerrold Tarog (credited as Ramon Ukit) 
| starring       = Che Ramos Neil Ryan Sese Irma Adlawan Publio Briones III Pen Medina
| music          = Jerrold Tarog
| cinematography = Mackie Galvez
| editing        = Jerrold Tarog (credited as Pats R. Ranyo) 
| studio         = 
| distributor    = Videoflick Productions
| released       =   
| runtime        = 
| country        = Philippines
| language       = Filipino
| budget         = 
| gross          = 
}} Filipino independent film directed by Jerrold Tarog. The film was a finalist in the full-length feature category of the 2009 Cinemalaya Philippine Independent Film Festival. {{Cite news 
  | last = Mateo
  | first = Ibarra C.
  | coauthors = 
  | title = Mangatyanan: Forgiving the sins of our parents
  | newspaper = gmanews.tv
  | location =
  | pages = 
  | publisher = GMA News and Public Affairs
  | date = 2009-08-18
  | url = http://www.gmanews.tv/story/170076/mangatyanan-forgiving-the-sins-of-our-parents
  | accessdate = }} 

The title refers to a Mangatyanan "blood trail" ritual of the Labwanan tribe, which plays an integral role in the plot. Both ritual and tribe were invented for the film. {{Cite video
  | people = Jerrold Tarog (Director), Mackie Galvez (Director of Photography), Che Ramos (Actress, "Laya Marquez")
  | title = Mangatyanan Commentary Track 1
  | medium = DVD Commentary Track
  | publisher = Cinemalaya Foundation, Videoflick Productions
  | location =
  | date = 2009 }} 

==Plot==
The film tells the story of Himalaya "Laya" Marquez (Che Ramos), a 27 year old travel photographer who projects a cold, aloof persona to her family and workmates. The beginning of the film reveals that she is estranged from her mother Luzviminda (Irma Adlawan) and her father, the famous Photographer Danilo Marquez (Pen Medina). The film also reveals that she hasnt had a complete dream or a full nights sleep since age 12.

Things come to a head for Laya when her father suffers from a heart attack. Although she pays for his hospital bills, she refuses to see him at the hospital, which leads to even further strain between her and her mother. The film reveals that her father used to visit Laya in her bedroom in the small hours of the night in order to abuse her - a fact which leads Layas mother to leave, thinking that Danilo was seeing another woman, but not knowing that "the other woman" was in fact her daughter.

Not long after, Laya and new co-worker Eric (Neil Ryan Sese) are assigned by their boss (Mailes Kanapi) to document the Mangatyanan blood trail ritual of the Labwanan (a fictional Filipino tribe created for the story) before the practice dies out forever.

When the pair meet the Labwanan, they discover a severely dwindled group whose cultural identity is now asserted only by their tribal leader Mang Renato (Publio Briones III). While documenting the ritual, Laya feels a connection between the tribe’s predicament and her own.

When the disagreements and moral failures among the Labwanan lead to the disruption of the Mangatyanan ritual, Laya and Eric are asked to leave. Before finally leaving the tribe however, Laya runs away and takes it upon herself to perform the steps Mangatyanan ritual, one of the final steps of which is drinking a potentially fatal hallucigenic concoction.

While Eric and the tribesmen attempt to resuscitate Laya, the effects of the drink give her a vision of her father, who asks for her forgiveness. When she refuses, he instead convinces her to forgive her mother, which finally allows Laya to experience dignity and personal freedom.

==Production==
The Blood Trail was shot over the course of ten days, partly in Manila, and partly in Zambales. Manila locales include EDSA, the Metro Rail Transit, and various establishments near the Elisabeth Seton school. Zambales locales included Anawangin Cove PAndaquit Casa San Miguel and Capones Island. {{Cite video
  | people = 
  | title = Mangatyanan: Behind the Scenes
  | medium = DVD Bonus Feature
  | publisher = Cinemalaya Foundation, Videoflick Productions
  | location =
  | date = 2009 }} 

As Director, writer, composer, editor, and sound designer (credited under different names), Jerrold Tarog had a great deal of control over many aspects of the film, but recognized that the other people who made the film were as much its creators as he was, saying "Theres no such thing as an Auteur theory|Auteur." {{Cite video
  | people = Jerrold Tarog (director, writer, composer, editor, and sound designer)
  | title = Mangatyanan Commentary Track 2
  | medium = DVD Commentary Track
  | publisher = Cinemalaya Foundation, Videoflick Productions
  | location =
  | date = 2009 }} 

===Fictional Tribe, Language, and Ritual=== Kapampangan and Visayan languages|Bisaya, and then modified slightly.  

==Cast==
*Himalaya "Laya" Marquez:Che Ramos
*Eric:Neil Ryan Sese
*Luzviminda Marquez:Irma Adlawan
*Mang Ramon:Publio Briones III
*Danilo Marquez:Pen Medina
*Queen:Mailes Kanapi
*Junel:Bor Ocampo
*Manang Nida:Madeleine Nicolas
*Mang Igo: Ronald Legaspi
*Chits: Julia Enriquez
*Young Laya: Danielle Afuang

==Accolades==

===Awards=== Cinemalaya Cinco 2009 Cinemalaya Cinco 2009 - “for effectively creating the physical and oppositional terrains for the urban and rural, and the real and mystical, in a story of a young girl’s coming to terms with her own troubled past." 
*Best Editing - Jerrold Tarog (credited as Pats R. Ranyo) - 8th Gawad Tagapuring mga Akademik ng Aninong Gumagalaw (2009 Gawad Tanglaw) 

===Nominations===
*Best Actress - Che Ramos - 33rd Gawad Urian Awards (2010)  
*Digital Movie Cinematographer of the Year - Mackie Galvez - 26th Philippine Movie Press Club Star Awards (2010)  

==See also==
* Jerrold Tarog
* Confessional (film)
* Senior Year (film)
* Cinema of the Philippines

==References==
 

==External links==
*  
*  

 
 
 
 
 
 