Zombibi
 
{{Infobox film
| name           = Zombibi
| director       = Martijn Smits Erwin van den Eshof
| producer       = Paul Ruven
| writer         = Tijs van Marle
| starring       = Yahya Gaier Gigi Ravelli Mimoun Ouled Radi Sergio Hasselbaink Uriah Arnhem Noel Deelen
| music          = Matthijs Kieboom
| cinematography = Joost van Herwijnen
| editing        = Joost van de Wetering
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
| gross          = $302,777 (Netherlands) 
}}
Zombibi (also released as Zombie Kill!) is a 2012 Dutch splatter comedy film directed by Martijn Smits and Erwin van den Eshof, written by Tijs van Marle, and starring Yahya Gaier, Gigi Ravelli, Mimoun Ouled Radi, Sergio Hasselbaink, Uriah Arnhem, and Noel Deelen as survivors of a zombie attack in Amsterdam-West.

== Plot ==
Aziz works in an Amsterdam office building.  He dislikes his job and his boss, but he has just begun dating his dream girl, Tess.  Jealous of his success with Tess, his boss uses the constant phone calls by Azizs brother Mo as an excuse to fire him.  Annoyed but having nowhere else to go, Aziz joins Mo at the pool party to which Mo has invited him, only to become an unwilling participant in a fight instigated by Mos carelessness.  Mo and Aziz end up jailed with Jeffrey and Nolan, incompetent bouncers at the party, and Joris, a thief.  All but Aziz hit on Kim, a tough cop, and she tasers both Mo and Jeffrey after they talk back to her.  Later, alarms go off and the building loses power.  Mo initially dismisses this as theatrics intended to scare them, but when the doors to their cells open, the group discovers the police station in ruins.  Curious, they leave the station, and zombies attack them; Kim saves them, and they return to the station. 
 Ben Saunders.  The group retreats to a chop shop, where the mechanics demonstrate that the green slime is an energy source.  When the mechanics turn into zombies, the group splits up: Aziz and Kim set off to rescue Tess, and Joris convinces the others to rob a bank.

Joris doublecrosses the others and leaves with the money, and a zombie bites Nolan.  When Nolan begins vomiting green slime, he requests they kill him; after several unsuccessful attempts to bludgeon Nolan to death, they abandon him.  Both Mo and Tess call Aziz and ask for help.  Caught between his brother and his girlfriend, Aziz decides to first help his brother.  Kim and Aziz rescue Jeffrey and Mo, but they end up stranded at a playground.  They are rescued by the Barachi brothers, who, in a video game sequence, defeat the zombies and then leave.  When the group arrives at the shelter, soldiers refuse to assist Aziz in rescuing Tess, and he steals a truck.  The others join him, and they set off for the office building.  Jeffrey, now armed with a minigun, holds off the zombie hordes while Aziz, Mo, and Kim work their way up the building, only to find out that Tess was already rescued by a former lover. 

Furious at the pointlessness, Kim leaves on her own and randomly encounters Tess, who has now transformed into a zombie.  At the same time, Aziz and Mo reach the buildings roof, where a Russian commando explains his plan to blow up the building.  Before he can, he is bitten, turns into a zombie, and bites Mo, who decides to stay behind and use the detonator.  Aziz finds Kim just as she beheads Tess, and Aziz hurredly explains about the explosives.  Jeffrey, who has climbed the entire building while toting his minigun, follows them back down the staircase.  As zombies swarm Mo, he triggers the explosives, and the building collapses; Aziz, Kim, and Jeffrey escape with the help of the Barachi brothers.  When they triumphantly return to the shelter, they see the soldiers suck the blood from survivors.  Aziz says that he hates vampires, and they prepare for another fight.

== Release ==
Zombibi was released on 16 February 2012 in the Netherlands, where it grossed $302,777.     It was released on DVD in the UK on 12 September 2012,    and in the United States on 10 June 2014.   

== Reception ==
Paul Mount of Starburst (magazine)|Starburst rated it 7/10 stars and called it "delightfully daffy fun".   Adam Tyner of DVD Talk rated it 2.5/5 stars and wrote, "Kill Zombie! tries to mimic Edgar Wright|Wrights motions as best it can but never approaches anything close to those same heights."   Mac McEntire of DVD Verdict wrote that it "has some decent laughs" but "retreads what other filmmakers have done".   Keri OShea of Brutal As Hell wrote that film "goes the extra mile in terms of splatter" but is not groundbreaking. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 