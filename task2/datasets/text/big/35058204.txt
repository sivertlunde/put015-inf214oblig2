Andhar Bahar
 
 
{{Infobox film
| name           = Andar Baahar
| director       = Phaneesh S Ramanathapura
| producer       = Rajanish  Prasad Ambarish   Avinash Bhaskar Srinivas
| starring       = Shivrajkumar Parvathi Menon Shashikumar Arundathi Nag
| music          = Vijay Prakash
| background = Vijay Prakash Pritesh Mehta
| cinematography = Shekar Chandru
| editing        = Jo Ni Harsha
| studio         = Legends International
| released       =  
| country        = India
| language       = Kannada
}}

Andar Baahar ( ) is a 2013 Kannada action film starring Shivrajkumar and Parvathi Menon in the lead. The film is directed by Phaneesh Ramanathapura and produced by a host of USA Kannadiga friends under the Legends International Group banner.  Popular playback singer Vijay Prakash made his debut as a music composer for this film 

The film tells the story of the relationship between a newly married criminal and his wife.

==Cast==
* Shivrajkumar as Surya
* Parvathi Menon as Suhasini
* Shashikumar Srinath
* Arundathi Nag
* Raghuram
* Srujan Lokesh
* Spoorthi Suresh

==Release==
The film was released on 5 April 2013 all over Karnataka.

==Reviews==
The Oneindia entertainment website gave the film four stars,  and it received positive reviews by audience and press media. characterising the film as "a family entertainer that is a good watch"., 

==Soundtrack==
The soundtrack of the album was released on 10 February 2013.
{{Infobox album
| Name = Andar Bahar
| Type = soundtrack
| Artist = Vijay Prakash Kannada
| Label = Saregama Audio
| Producer = 
| Cover = 
| Released    =  10 February 2013 Feature film soundtrack
| Last album = 
| This album = 
| Next album = 
}} 

{{Track listing
| total_length   = 
| lyrics_credits = 
| extra_column	 = Singer(s)
| title1	= Andar Bahar
| lyrics1 	= 
| extra1        = Vishal Dadlani, Suzanne DMello
| length1       = 
| title2        = Koneye Irada
| lyrics2 	= 
| extra2        = Shankar Mahadevan
| length2       = 
| title3        = Aase Aase
| extra3        = Karthik (singer)|Karthik, Anuradha Bhat
| lyrics3 	= 
| length3       = 
| title4        = Maleyali Minda
| extra4        = Vijay Prakash, Shreya Ghoshal
| lyrics4       =
| length4       =
| title5        = Neenu Nanna
| extra5        = Chetan Sosca, Shamita Malnad
| lyrics5       =
| length5       =
}}

==BackgroundScore==
The background score was composed by Vijay Prakash and Pritesh Mehta

==References==
 

==External links==
*  
*   at  

 
 
 
 
 


 