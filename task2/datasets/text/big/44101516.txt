Avidathe Poele Ivideyum
{{Infobox film 
| name           = Avidathe Poele Ivideyum
| image          =
| caption        =
| director       = KS Sethumadhavan
| producer       = Raju Mathew John Paul (dialogues) John Paul
| starring       = Sukumari Mohanlal Mammootty Shobhana
| music          = M. K. Arjunan
| cinematography = Vasanth Kumar
| editing        = MS Mani
| studio         = Century Films 
| distributor    = Century Films 
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film,  directed by KS Sethumadhavan and produced by Raju Mathew. The film stars Sukumari, Mohanlal, Mammootty and Shobhana in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Sukumari
*Mohanlal
*Mammootty
*Shobhana
*Adoor Bhasi
*Sankaradi
*Adoor Bhavani
*Jagannatha Varma
*Karamana Janardanan Nair
*Kavitha Thakkur
*Lalu Alex
*MG Soman
*Paravoor Bharathan
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Deepam || S Janaki || P. Bhaskaran || 
|-
| 2 || Manassum Manassum || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Tak Tak Tak || Krishnachandran, Lathika || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 