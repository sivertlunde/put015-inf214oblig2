Kill Theory
 
{{Infobox film
| name           = Kill Theory
| image          = Kill-theory-2009 53833161.jpg
| caption        = Official theatrical poster Chris Moore Chris Bender Amanda White Adam Rosenfelt
| writer         = Kelly C. Palmer
| starring       = Teddy Dunn Agnes Bruckner Daniel Franzese
| music          = Michael Suby
| cinematography = David A. Armstrong
| editing        = Maureen Meulen
| studio         = BenderSpink Cross River Pictures Element Films Last Resort Productions Lift Films
| distributor    = After Dark Films
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
}} Chris Moore and written by Kelly C. Palmer. 

==Plot==
An unnamed man is sitting in a psychiatrists office, being released from an insane asylum. Three years ago, he and three friends were mountain climbing, when an accident left them all suspended on one rope that was fraying. Being the closest to the top, the man chose to cut the rope to save himself and dropped the other three to their death. He says that he knows his actions were wrong, but that he had to make the choice he did if he wanted to survive. The doctor reminds him that while his actions are almost understandable, they resulted in death, something that is never acceptable.  This causes the patient to become upset and more insistent that other people in the same situation would make the same choices as he did.

Meanwhile, seven teenagers are driving to their friend Brents house to celebrate their upcoming college graduation. While the group gets along for the most part, there are obvious moments of tension between people, most notably Amber trying to Michael despite his relationship with Jennifer.  Amber briefly refers to unspecified events that imply she and Michael were intimate at one point.  As the night progresses, Brents stepsister Alex unexpectedly shows up.  She and Brent argue, her calling Brent a spoiled rich kid and him calling her mother a gold-digger who only married his father for his money.  The group convinces Brent to let Alex stay, and in the spirit of maintaining the cheery atmosphere, several people try to fix her up with another boy Freddy. 

They all party and later that night when everyone else is asleep, Nicole is grabbed by a strange man as she goes for a beer. Her body is then thrown through the window onto a sleeping Freddy, who screams and alerts the rest of the group. The letters TV carved into her stomach indicates for them to watch a video taken earlier, where Nicole is given a gun and told to shoot the sleeping Carlos to save her own life.  Nicole refuses and is killed. The unnamed man tells the seven on the video that they will have to kill each other until only one person remains. If more than one person is still alive at 6:00 am, he will kill everyone left. 

The group panics and Brent and Carlos go to try and find a way to escape. The boat Brent showed Carlos earlier has been sunk, but the gun on board is in the keybox, so Brent takes it. Carlos seeks an axe and goes to get it, but is caught in a giant bear trap. Brent tries to help, but then leaves him, saying to the others that he died. Unbeknownst to him, the killer bandages Carlos and leaves him at the house. When he tells Jennifer that Brent left him, she loses all trust in Brent. 

Now that everyones back, Michael leads them to the van to try and escape, but the van is caught by something and cant move. Over the radio, the unnamed man says that they must now throw one person out to die or they will all die. Brent decides to throw the mortally injured Carlos out and he is shot through the head with the rifle stolen from the basement. Everyone splits up and runs back to the house. Brent and Amber separate and Brent is caught by the killer, but is let go when he promises to kill someone. 

Alex ran off on her own to get her motorcycle, but the killer fires at her and shes forced to hide.  Brent discovers her and drowns her in the lake. He sneaks back to the house to talk to Freddy alone, saying that the boat is OK and they can escape if he can get the gun from Michael. Freddy fakes a panic attack to get the gun and Brent comes in, but Amber reveals that the boat is inoperable. Freddy realizes hes been tricked and shoots Brent, and tries to force everyone else out of the house. Michael convinces Freddy to drop the gun, just as Brent skewers him through the head with a poker, killing him. 

Brent grabs the gun and chases Michael and Jennifer down to the basement, but as theyre cornered, Amber attacks Brent with a shovel and beats him to death. Jennifer grabs the gun and takes Michael upstairs. Amber finds a gun in her pill bag and draws on Jennifer, arguing with her over Michael, saying she loves him. It is revealed that Michael and Amber once slept together and that she has been in love with him ever since.  Jennifer is not surprised by this news, saying that she forgave Michael when he confessed to the infidelity the day after it happened.  Outraged, Jennifer shoots Amber in the stomach, but Michael takes them down in the basement as the unnamed man approaches. They hide under the staircase and shoot at the approaching figure, but it turns out to be Alex who survived Brents first attack. She dies from the gunshot wounds.

Jennifer then turns traitor and stabs Michael in the stomach, saying that she has to in order to survive. She tries to kill him, but Amber attacks her and strangles her to death. Amber crawls next to Michael, intent on staying with him until the end.  As the clock chimes six and the man approaches, Michael kills himself in order to save Amber. As the man passes, Amber says shell never be like him, and though he expresses skepticism, he leaves her alive and departs from the house.

The killer leaves a voice mail in the psychiatrists office, saying hes proved his theory that desperate people would resort to murder and the camera pans along a photograph to reveal Brent was the doctors son. The killer laughs and says hes now found closure.

==Cast==
* Don McManus as Dr. Karl Truftin
* Ryanne Duzich	as Amber
* Teddy Dunn as Brent 
* Daniel Franzese as Freddy 
* Agnes Bruckner as Jennifer 
* Patrick Flueger as Michael 
* Steffi Wickens as Nicole
* Theo Rossi as Carlos
* Taryn Manning as Alex  Kevin Gage as Killer
* Edwin Hodge as Paul
* Carly Pope as Riley

==Production==
The film was shot in the Cheyenne Studios (now operating as Fantastic Lane) - 27567 Fantastic Lane in Castaic, California and in New Orleans, Louisiana. 

==Release==
The film premiered on 14 May 2008 in France as part of the Cannes Film Market.  It was on 23 January part of the After Dark Horrorfest IV   and the DVD was released on 23 March 2010. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 