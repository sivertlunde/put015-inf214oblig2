Silver City (1951 film)
{{Infobox film
| name           = Silver City
| image          = 
| image_size     = 
| caption        = 
| director       = Byron Haskin
| writer         = 
| narrator       = 
| starring       = Edmond OBrien
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = December 1 1951
| runtime        = 90 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $1 million (US rentals) 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Silver City is a 1951 film directed by Byron Haskin. It stars Edmond OBrien and Yvonne De Carlo.  

==Cast==
*Edmond OBrien as Larkin Moffatt
*Yvonne De Carlo as Candace Surrency
*Richard Arlen as Charles Storrs
*Barry Fitzgerald as RR Jarboe
*Gladys George as Mrs. Barber

==References==
 

==External links==
* 

 

 
 