The Verdict of Lake Balaton
{{Infobox film
| name           = The Verdict of Lake Balaton
| image          = 
| image_size     = 
| caption        = 
| director       = Pál Fejös
| producer       = 
| writer         = Serge Plaute   
| narrator       =  Antal Páger Ernõ Elekes Gyula Csortos   Mária Medgyesi
| music          = Viktor Vaszy
| editing        = 
| cinematography = István Eiben 
| studio         = Osso Film
| distributor    = 
| released       = 1932
| runtime        = 
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    =  Hungarian drama Antal Páger, Ernõ Elekes and Gyula Csortos. A German-language version Balaton Condemned was also released.

==Cast== Antal Páger - János 
* Ernõ Elekes - Mihály 
* Gyula Csortos - Kovács 
* Mária Medgyesi - Mari
* Mór Ditrói - Szabó
* Erzsi Palotai - Síró asszony
* Elemér Baló - Toronyőr

==Bibliography==
* Buranbaeva, Oksana & Mladineo, Vanja. Culture and Customs of Hungary. ABC-CLIO, 2011.
* Burns, Bryan. World Cinema: Hungary. Fairleigh Dickinson University Press, 1996.
* Cunningham, John. Hungarian Cinema: From Coffee House to Multiplex. Wallflower Press, 2004.

==External links==
* 

 
 

 
 
 
 
 
 