Capricorn One
 
{{Infobox film
| name           = Capricorn One
| image          = Capricorn one.jpg
| caption        = Theatrical release poster
| director       = Peter Hyams
| writer         = Peter Hyams David Doyle James Karen
| producer       = Paul N. Lazarus III Bill Butler James Mitchell
| studio         = ITC Entertainment
| distributor    = Warner Bros.
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| music          = Jerry Goldsmith
| awards         =
| budget         = US$5,000,000 (estimated)
| certificate_no = 25000
}} thriller film about a Mars landing hoax. It was written and directed by Peter Hyams and produced by Lew Grades ITC Entertainment. It stars Elliott Gould with James Brolin, Sam Waterston and O. J. Simpson as the astronauts.

==Plot==
At an unspecified time,  Capricorn One&mdash;the first manned mission to Mars&mdash;is on the launch pad. Such NASA authorities as Dr. James Kelloway (Hal Holbrook) realize, too late, that a faulty life-support system supplied by a lowest-bid NASA contractor will kill the astronauts during the flight. As the manned space program needs a success to continue, they find themselves forced to falsify the landing rather than cancel the mission.

Minutes before launch, the bewildered crew of Air Force Colonel Charles Brubaker (James Brolin), Air Force Lieutenant Colonel Peter Willis (Sam Waterston), and Navy Commander John Walker (O. J. Simpson) are removed from Capricorn One and flown to an abandoned United States Army Air Corps desert base. The launch proceeds on schedule, but the public is unaware that the spacecraft is empty. At the base, the astronauts are informed they will help counterfeit the television footage during the flight to and from Mars, and that it is their patriotic duty to participate for the sake of national morale and prestige. Initially they refuse, but Kelloway, himself under extreme duress (from whom, what, or where is never clearly specified) to go through with the hoax, threatens their families if they do not cooperate, claiming a bomb will explode on a plane carrying the family members.

The astronauts remain in captivity during the flight and are filmed landing on Mars within a studio located at the base. The conspiracy is known to only a few officials, until alert technician Elliot Whitter (Robert Walden) notices that ground control receives the crews television transmissions before the spacecraft telemetry arrives. Whitter mysteriously disappears before he can finish sharing his concerns with journalist friend Robert Caulfield (Elliott Gould). Caulfield discovers that all evidence of his friends life has been erased and begins investigating the mission, surviving several attacks on himself and his reputation.

Upon returning to Earth, the empty spacecraft burns up due to a faulty heat shield during atmospheric reentry|re-entry. The captive astronauts board a plane to be placed in the spacecraft but the plane unexpectedly turns around and returns to the airfield. They realize that something has gone badly wrong with the re-entry process and that officials can never release them because doing so would automatically expose the hoax. Knowing that the only logical solution for their captors is to kill them during the cover-up process, they break out of their confinement and escape in a Learjet, which runs out of fuel soon after take-off. Forced to crash-land and stranded in the desert, they attempt to return to civilization while being pursued by "black helicopters" (actually, olive drab in the film). They start walking in three directions and Brubaker is the only one to avoid capture.

Caulfields investigation leads him to the desert, where he finds the military base and the hangar abandoned. Looking around, he finds a necklace given to Brubaker by his wife and concludes that the astronauts were in the hangar. With the help of crop-dusting pilot Albain (Telly Savalas), he searches the desert and rescues Brubaker before the men in the helicopters can capture or kill him. The film ends with Caulfield and Brubaker arriving at the astronauts memorial service, exposing the conspiracy in front of television cameras and scores of witnesses who are astonished at his arrival.

==Cast==
 
 
*Elliott Gould as Robert Caulfield USAF
*Sam Waterston as Lieutenant Colonel Peter Willis, USAF USN
*Hal Holbrook as Dr. James Kelloway
*Brenda Vaccaro as Kay Brubaker
*Karen Black as Judy Drinkwater David Doyle as Walter Loughlin
*Robert Walden as Elliot Whitter
*Telly Savalas as Albain
*David Huddleston as Congressman Hollis Peaker
*Lee Bryant as Sharon Willis
*Denise Nicholas as Betty Walker
*James Sikking as Control Room Man (as Jim Sikking)
 
*Alan Fudge as Capsule Communicator
*James Karen as Vice President Price
*Virginia Kaiser as Phyllis Price
*Nancy Malone as Emily Peaker
*Hank Stohl as General Enders
*Norman Bartold as the President
*Darrell Zwerling as Dr. Bergen
*Milton Selzer as Dr. Burroughs
*Lou Frizzell as Horace Gruning
*Chris Hyams as Charles Brubaker, Jr.
*Seanna Marre as Sandy Brubaker
*Paul Picerni as Jerry
*Barbara Bosson as Alva Leacock
*Jon Cedar as FBI Man Number 1
 

==Production==
Peter Hyams began thinking about a film of a space hoax while working on broadcasts of the Apollo missions for CBS.  He later reflected regarding the Apollo 11 moon landing, “There was one event of really enormous importance that had almost no witnesses.  And the only verification we have . . . came from a TV camera.”  , Nicholas de Monchaux, MIT Press, 2011.  This book sites the New York Times as stating “Watergate may not have inspired ‘Capricorn One,’ but it made its thesis more acceptable, its plot more credible and some of its content strangely prophetic.” 

He later elaborated:
 Whenever there was something on the news about a space shuttle, they would cut to a studio in St. Louis where there was a simulation of what was going on. I grew up in the generation where my parents basically believed if it was in the newspaper it was true. That turned out to be bullshit. My generation was brought up to believe television was true, and that was bullshit too. So I was watching these simulations and I wondered what would happen if someone faked a whole story.   accessed 30 July 2014  
Hyams went on to become a successful television writer and director and began writing the script for Capricorn One in the mid-1970s. The failure of Peeper (film)|Peeper jeopardized his career, but Hyams and his friend, producer Paul Lazarus, were able to obtain the support of Lew Grade, head of production company ITC Entertainment, who agreed to a $4.8 million budget. Szebin, 2000  Lew Grade, Still Dancing: My Story, William Collins & Sons 1987 p 247   The Watergate scandal from the early and mid-1970s also made the premise more plausible. 

To stay within the budget, NASAs co-operation was needed. Lazarus had a good relationship with the space agency from Futureworld. The filmmakers were thus able to obtain government equipment as props despite the negative portrayal of the space agency, including a prototype lunar module.
 Robert Blake was in (Hyams first feature) Busting. I’ve said many times: some people have AFI Lifetime Achievement awards; some people have multiple Oscars; my bit of trivia is that I’ve made films with two leading men who were subsequently tried for the first degree murder of their wives." 

==Release==
The film was originally scheduled to debut in February 1978, but good preview screenings and delays in Superman (1978 film)|Superman caused it to move to June. Capricorn One became the years most-successful independent film. 

Hyams later said:
 Audiences just stood up and cheered at one point in the film. It wasnt because it was such a great movie, its just that certain movies strike certain chords with people. In a successful movie, the audience, almost before they see it, know theyre going to like it. I remember standing in the back of the theater and crying because I knew that something had changed in my life. Sitting on the film cans outside the screening room, I felt my cheeks were wet with tears. A bright man,   David Picker came over to me and said, Youre going to have a lot of new best friends tomorrow. You better know how to handle it.   accessed 27 July 2014  

==Other media==
{{Gallery
|width=150
|lines=3
|align=right UK version, by Ken Follett (as Bernard L. Ross) US novelization of Capricorn One, by Ron Goulart
}}
Two novelizations of the film were written and published by separate authors. The first was written by Ken Follett (under the pseudonym Bernard L. Ross) and published in the United Kingdom, the other written by Ron Goulart and published in the United States. Allison, 2007. 

The Follett novel is notable for giving Robert Caulfield more development than the movie does, including giving him something of a relationship with CBS reporter Judy Drinkwater (who has more time here than in the movie) and ending the book with him and Judy (the story saves his career and results in his being employed by CBS).

Clips from the faked Mars landing scenes have been used for illustration purposes in various moon landing hoax conspiracy documentaries. Notably the Fox TV show Conspiracy Theory: Did We Land On The Moon and Bart Sibrels A Funny Thing Happened On The Way To The Moon, the latter of which also features a still shot from the hoax scene on the DVDs front cover.

==See also==
* Apollo Moon Landing hoax conspiracy theories

==References==

===Footnotes===
 

===Bibliography===
*  
*  
*  

==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 