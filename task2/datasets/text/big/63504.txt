Touch of Evil
 
{{Infobox film
| name           = Touch of Evil
| image          = Touchofevil.jpg
| caption        = Original theatrical release poster
| director       = Orson Welles
| producer       = Albert Zugsmith Rick Schmidlin (1998 restoration & directors cut)
| writer         =   Screenplay: Orson Welles Uncredited on screenplay: Paul Monash Franklin Coen
| starring       = Orson Welles Charlton Heston Janet Leigh Joseph Calleia
| music          = Henry Mancini
| cinematography = Russell Metty, ASC
| editing        = Aaron Stell Virgil Vogel Walter Murch
| distributor    = Universal-International
| released       =  
| runtime        = 1958 theatrical cut: 95 minutes 1976 alternate version: 108 minutes Restored cut: 112 minutes
| country        = United States
| language       = English
| budget         = $829,000
| gross          = $2,237,659  1,232,534 admissions (France)   at Box Office Story 
}}
 crime thriller thriller  film, written, directed by, and co-starring Orson Welles. The screenplay was loosely based on the novel Badge of Evil by Whit Masterson. Along with Welles, the cast includes Charlton Heston, Janet Leigh, Joseph Calleia, Akim Tamiroff, and Marlene Dietrich.

Touch of Evil is one of the last examples of film noir in the genres classic era (from the early 1940s until the late 1950s).  Since its release, the films reputation has grown in stature, and it is now widely regarded as one of Welless strongest films.

==Plot==
On the U.S.-Mexico border, a man plants a time bomb in a car. A man and woman enter the vehicle and make a slow journey through town to the U.S. border, the woman (Joi Lansing) insisting that she hears something ticking. Newlyweds Miguel "Mike" Vargas (Charlton Heston), a drug enforcement official in the Mexican government, and Susie (Janet Leigh) pass the car several times on foot. The car crosses the border, then explodes, killing the occupants.
 
 Harry Shannon) Ray Collins) arrive on the scene, followed by the one-legged police Captain Hank Quinlan (Orson Welles) and Quinlans longtime partner, Pete Menzies (Joseph Calleia). Quinlan nostalgically visits a brothel run by Tanya (Marlene Dietrich), who barely recognizes him.

Quinlans and Menzies prime suspect is Sanchez, a young Mexican secretly married to the victims daughter (Joanna Moore). They interrogate Sanchez in his apartment with Vargas present. Vargas visits the bathroom and accidentally knocks over an empty shoebox. Moments later, Menzies announces that two sticks of dynamite were found in the same shoebox. Vargas suspects Quinlan may have been planting evidence for years to help win convictions. Quinlan dismisses Vargas claim, saying he is just biased in favor of fellow Mexicans. With assistance from District Attorneys Assistant Al Schwartz (Mort Mills), Vargas studies the public records on Quinlans previous cases, revealing his findings to Gould and Adair. Quinlan arrives in time to overhear the discussion and angrily threatens to resign.

Susie moves from her Mexican hotel to a remote American motel to escape the unwanted attention of Grandi (Akim Tamiroff), brother of a man Vargas has been investigating. The motel, which Menzies recommended to her, has no other guests and is staffed only by a peculiar night manager (Dennis Weaver), and, unknown to Susie, is owned by Grandi himself. Grandi family members take over the motel. Vargas becomes concerned when his attempts to telephone Susie at the motel are blocked. Quinlan conspires with Grandi, arranging for Susie to be kidnapped, injected with drugs and taken to Grandi’s hotel in town. Quinlan then double-crosses Grandi, strangling him, then framing Susie for the murder in order to discredit Vargas.  However, exhausted and shaken from killing Grandi, Quinlan carelessly leaves his cane at the scene of the murder, implicating himself.

Vargas confronts Menzies about the history of evidence "discovered" by Quinlan. When he goes to Susies motel but cant find her, Vargas learns the motel is owned by Grandi. He confronts the gang members who attacked his wife. When they refuse to answer him, Vargas violently beats them down. Schwarz then informs a shocked Vargas that Susie has been arrested for murder. At the lockup Vargas finds her barely conscious. Menzies reveals to Vargas he discovered Quinlans cane at the murder scene. Vargas fits Menzies with a Covert listening device|wire. Near an oil field, Menzies meets Quinlan while being tracked on foot by Vargas recording the conversation.

Quinlan admits to Menzies that he planted evidence on people, but insists they were nevertheless guilty. Quinlan hears the secret microphones feedback and says his "game leg" informs him of Menzies betrayal. Quinlan demands Vargas show himself. Quinlan then shoots Menzies with Vargas gun. Quinlan prepares to shoot Vargas, but is shot by the dying Menzies. Schwartz arrives at the scene, and tells Vargas that Sanchez confessed to the crime. As Quinlan dies, Schwartz asks Tanya what she has to say for him; Tanya replies, "He was some kind of a man.  What does it matter what you say about people?"

==Cast==
* Charlton Heston as Ramon Miguel Vargas
* Janet Leigh as Susan Vargas
* Orson Welles as Hank Quinlan
* Joseph Calleia as Pete Menzies
* Akim Tamiroff as Uncle Joe Grandi
* Joanna Cook Moore as Marcia Linnekar Ray Collins as District Attorney Adair
* Dennis Weaver as the Night Manager Val de Vargas as Pancho
* Mort Mills as Al Schwartz
* Victor Millan as Manolo Sanchez
* Lalo Rios as Risto
* Phil Harvey as Blaine
* Joi Lansing as Blonde Harry Shannon as Gould
* Rusty Wescoatt as Casey
* Wayne Taylor as a gang member
* Ken Miller as a gang member
* Raymond Rodriguez as a gang member
* Arlene McQuade as Ginnie
* Dan White (actor) as the Border Guard
* Zsa Zsa Gabor as the Strip-club owner
* Marlene Dietrich as Tanya
* Mercedes McCambridge as a hoodlum
* Keenan Wynn as a man
* Joseph Cotten as a Detective

==History==
 
There are two stories as to how Welles ended up directing Touch of Evil. Charlton Heston recalled that Welles was originally hired to act in the film only, not to direct or write. Universal was keen to secure Heston for the lead, but he wanted the studio to confirm the director before he signed on. After learning that Welles was in the cast, Heston expressed his greater interest in starring if Welles were directing. The other story is that Welles had recently worked with producer Albert Zugsmith, known as the "King of the Bs", on a film called Man in the Shadow and was interested in directing something for him. Zugsmith offered him a pile of scripts, of which Welles asked for the worst to prove he could make a great film out of a bad script. At the time, the script was called Badge of Evil, after a Whit Masterson novel on which it was based. Welles did a rewrite and took it into production. After a decade in Europe during which he completed only a few films, Welles was eager to direct for Hollywood again, so he agreed to take only an acting fee for the role of Quinlan.  
 Marlene Dietrichs role was a surprise to the producers and they raised her fee so they could advertise her involvement. Welless friend and Mercury Theatre colleague Joseph Cotten appears uncredited as a police officer.

Janet Leigh recalled how Welles asked for input from the actors in the cast:

 It started with rehearsals. We rehearsed two weeks prior to shooting, which was unusual. We rewrote most of the dialogue, all of us, which was also unusual, and Mr. Welles always wanted our input. It was a collective effort, and there was such a surge of participation, of creativity, of energy. You could feel the pulse growing as we rehearsed.

You felt you were inventing something as you went along. Mr. Welles wanted to seize every moment. He didnt want one bland moment. He made you feel you were involved in a wonderful event that was happening before your eyes.  

Welles wrapped production on time, delivered a rough cut to Universal, and was convinced that his Hollywood career was back on the rails. However, the film was then re-edited (and in part re-shot) by Universal International pictures. The editing process was protracted and disputed, and the version eventually released was not the film Universal or Welles had hoped for. It was released as a B-movie, the lower half of a double feature. The A-movie was The Female Animal, starring Hedy Lamarr, produced by Albert Zugsmith and directed by Harry Keller, whom the studio had hired to direct the re-shot material in Touch of Evil. The two films even had the same cameraman, Russell Metty. Welless film was given little publicity despite the many stars in the cast. Though it had little commercial success in the US (Welles himself claimed that the movie turned a good profit but other records disputed his claim), it was well received in Europe, particularly by critics like future filmmaker François Truffaut.

==Differing versions==
 
 
Three versions of the film have been released:

# The original 1958 release version
# A longer version, released in 1976
# A 1998 restored version that attempted to fulfill Welless wishes in his 1958 memo.

Welless rough cut as submitted to Universal no longer exists. That cut was worked on and trimmed down by Universal staff, and in late 1957 Universal decided to perform some reshoots. Welles claimed these were done without his knowledge, but Universal claimed that Welles ignored the studios requests to return and undertake further work. It was at this point that Keller came aboard: some of his material was entirely new, others replaced Welles scenes. Welles screened the new cut and wrote a 58-page memo to Universals head of production, Edward Muhl, detailing what he thought needed to be done to make the film work. However, many of his suggestions went unheeded and Touch of Evil was eventually released in a version running 93 minutes.

In the mid-1970s, Universal discovered that it held a 108-minute print of Touch of Evil in its archives. Aware that there was a growing audience of cineastes with a strong interest in Welless work, the studio released this version to cinemas in 1976 and later issued it on video, billing it as "complete, uncut and restored". In fact, this print was not a restoration at all, but a preview version which post-dated the Welles memo but pre-dated the release version. While it did feature some vital Welles scenes that Universal cut from the release version, the preview version also featured more of Kellers material than the release version.

 

In 1998, Walter Murch, working from all available material, re-edited the film based on the Welles memo, with Bob ONeil, Universals director of film restoration, and Bill Varney, Universals Vice President of Sound Operations, participating in the restoration.     As Welless rough cut no longer exists, no true "directors cut" is possible, but Murch was able to assemble a version incorporating most of the existing material, omitting some of the Keller scenes (though some were retained, either because they had replaced Welles scenes which no longer existed and were necessary to the plot, or because Welles had approved of their inclusion). In addition, some of Welless complaints concerned subtle sound and editing choices, and Murch re-edited the material accordingly.  Notable changes include the removal of the credits and Henry Mancinis music from the three-minute opening shot, crosscutting between the main story and Janet Leighs subplot, and the removal of Harry Kellers hotel lobby scene. Rick Schmidlin produced the 1998 edit, which had a limited but successful theatrical release (again by Universal) and was subsequently made available on DVD. The DVD includes a reproduction of the 58-page memo.

Originally scheduled to be premiered at the 1998 Cannes Film Festival with Janet Leigh, Walter Murch, and Rick Schmidlin attending, the screening was canceled at the eleventh hour after threats of litigation from Welless daughter, Beatrice Welles,  who has issued similar threats against some parties who try to show or alter her fathers work (such as the completion of Welless film The Other Side of the Wind). The reason given for the litigation was that Beatrice Welles was not consulted for the restoration.

==Accolades==
The film opens with a three-minute, twenty-second tracking shot widely considered by critics as one of the greatest long takes in cinema history.  

In 1993, Touch of Evil was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 
 100 Years, 100 Thrills" list. 

==Cultural references==
  Paul Williamss Swan character and the Phantom alternately observing the histrionics from the balcony and proscenium, respectively.
 Ed Wood. In a scene near the end of the film, Ed Wood (Johnny Depp) is complaining to Orson Welles (Vincent DOnofrio) about how producers always want the wrong actors to play certain parts in their movies. Welles says, "Tell me about it. Im supposed to do a thriller with Universal, but they want Charlton Heston to play a Mexican!"
 Get Shorty, where movie fan Chili Palmer (John Travolta) invites another character to see a screening of Touch of Evil, saying, "You wanna go check it out? Watch Charlton Heston play a Mexican?" We later see Palmer watching the final scene of the movie, mouthing the words together with the characters on screen. Part of Mancinis score was used as the love theme between Chili and aging starlet Karen Flores (Rene Russo).

In James Robert Bakers novel, Boy Wonder, fictional movie producer Shark Trager makes it his goal to surpass Touch of Evils three minute opening tracking shot when filming a movie of his own. Tanyas line, "He was some kind of a man. What does it matter what you say about people?" was also quoted extensively in the book.
 The Player, The Office episode entitled "Performance Anxiety".
 Thalia Theatre Broadway after their yearly viewing of the film. Before heading to a nearby bar, both declare "I dont drink" in Hanks voice.

The opening scene of Boogie Nights (1997), written and directed by Paul Thomas Anderson, is a three-minute length shot through a bar, presenting all the main characters of the film. Anderson has admitted this is an homage to Touch of Evil.

In the 1999 Steve Martin/Eddie Murphy film Bowfinger, the original movie poster is shown on the wall several times in Martins characters home.

Singer-songwriter Tom Russell has a song titled "Touch Of Evil" on his 2001 album Borderland that makes several references to the movie, including the long opening shot and the dialogue between Dietrich and Welles about his future.

In the 2008 film In Bruges, the opening shots of Touch of Evil can be seen playing in the background during the scene when Harry (Ralph Fiennes) instructs Ken (Brendan Gleeson) to kill Ray (Colin Farrell)—in a six minute continuous take.

On the TV show House (TV series)|House, Dr. Wilson has a Touch of Evil poster on the wall behind his desk.
 Cabaret Voltaire have a song called "A Touch of Evil" on their album Red Mecca.

The film Apartment Zero opens with a shot of cinema owner Adrian LeDuc (Colin Firth) wiping away a tear as he watches the last scene of Touch of Evil.

==References==
 

==Further reading==
*Comito, Terry (editor). Touch of Evil. Orson Welles, director. New Brunswick, NJ: Rutgers University Press, 1985. ISBN 0813510961.
*Nericcio, William Anthony. Hallucinations of Miscegenation and Murder: Dancing along the Mestiza/o Borders of Proto-Chicana/o Cinema with Orson Welless Touch of Evil. 

==External links==

 
 
*  
*  
*  
*  
*  , Charlton Hestons account of the production of Touch of Evil.
*  , a new book from the University of Texas Press, features an article on Orson Welles and Touch of Evil entitled: Hallucinations of Miscegenation and Murder: Dancing along the Mestiza/o Borders of Proto-Chicana/o Cinema with Orson Welless Touch of Evil.
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 