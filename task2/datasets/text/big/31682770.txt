List of Kannada films of 1994
 
  Kannada film industry in India in 1994 in film|1994, presented in alphabetical order.

{| class="wikitable"
|-
! Title
! Director
! Cast
! Music
|-
| Adhipathi || Om Prakash Rao || Devaraj, Rockline Venkatesh, Prakash Rai, Girija, Avinash || Vijay Anand
|-
| Alexander || Srinivasa Reddy || Shashikumar, Sindhuja, Tara (Kannada actress)|Tara, Mukhyamantri Chandru || Vijay Anand
|-
| Apoorva Samsara || B Ramamurthy || Harshavardhan, Shruti (actress)|Shruti, Tara (Kannada actress)|Tara, Doddanna || Upendra Kumar
|-
| Appa Nanjappa Maga Gunjappa || Shivaram || Tennis Krishna, Anjali Sudhakar, Abhinaya, Dinesh || R. Damodar 
|- Srinath || V. Manohar
|-
| Bhairava || Rajkishore || Jaggesh, Nandini Singh, Vajramuni, Doddanna || M. M. Keeravani
|-
| Bhootayiya Makkalu || Siddalingaiah || Shashikumar, Abhijeeth, Madhuri, Dhanuja, Aravind || Rajan-Nagendra
|- Baby Shamili, Tara || C Ganesh
|-
| Chamatkara || Suresh Heblikar || Suresh Heblikar, Chinmayi, Ramesh Bhat, Vanitha Vasu || Manoranjan Prabhakar
|-
|  Chinna (1994 film)|Chinna || V. Ravichandran || V. Ravichandran, Yamuna, Puneet Issar, Silk Smitha || Hamsalekha
|- Baby Shamili, Geetha (actress)|Geetha, Tennis Krishna || Hamsalekha
|-
| Curfew || H S Rajashekar || Devaraj, Sudharani, B. C. Patil, Doddanna || Sadhu Kokila
|-
| Gandhada Gudi Part 2 || Vijay || Shivarajkumar, Rajkumar (actor)|Rajkumar, Rajeshwari, Bharathi, Tara (Kannada actress)|Tara, Charan Raj, Tiger Prabhakar || Rajan-Nagendra
|-
| Gandugali || Balaji Singh Babu || Shivarajkumar, Nirosha, Pournami, Ravi Kiran || Sadhu Kokila
|-
| Gold Medal || Om Sai Prakash || Devaraj, Ambarish, Thriller Manju, Sudharani, Vanitha Vasu, Shilpasri || Vijay Anand
|-
| Gopi Kalyana || B Ramamurthy || Tiger Prabhakar, Suresh (actor)|Suresh, Anjana || Hamsalekha
|-
| Halunda Tavaru || Dinesh Baboo || Vishnuvardhan (actor)|Vishnuvardhan, Sithara (actress)|Sithara, Pandari Bai, Srinivasa Murthy || Hamsalekha
|-
| Hantaka || K V Prasad || Ramesh Aravind, Panchami, Sathyajith, Sihi Kahi Chandru || V. Manohar
|- Sai Kumar, Rajesh || Upendra Kumar
|-
| Hongirana || K. P. Bhavani Shankar || Raghuveer, Keerthana, Bharath Raj, Vajramuni|| Hamsalekha
|-
| Indian || N K Keerthiraj || Devaraj, Sithara (actress)|Sithara, K. S. Ashwath, Srinivasa Murthy || Madan Mallu
|-
| Indrana Gedda Narendra || Om Sai Prakash || Jaggesh, Srishanthi, Srinath (actor)|Srinath, Jai Jagadish, Sindhuja || V. Manohar
|-
| Jaana (film)|Jaana || Shivamani || V. Ravichandran, Kasthuri (actress)|Kasthuri, Shruti (actress)|Shruti, Anjana, B. C. Patil  || Hamsalekha
|-
| Karulina Koogu || D. Rajendra Babu || Tiger Prabhakar, Vinaya Prasad, Srinath (actor)|Srinath, Umashree, Doddanna || Hamsalekha
|-
| Kaveri Theeradalli || S. Narayan || Raghuveer, Usha, Sangeetha, K. S. Ashwath || Vijay Anand
|-
| Keralida Sarpa || H N Shankar || Kumar Bangarappa, Yamuna, Ramesh Bhat || Sangeetha Raja
|-
| Kiladigalu || Dwarakish || Vishnuvardhan (actor)|Vishnuvardhan, Dwarakish, Suvarna Mathew|Swarna, Avinash || Raj-Koti
|-
| Kotreshi Kanasu || Nagathihalli Chandrashekar || Vijay Raghavendra, Karibasavaiah, Umashri || C. Ashwath 
|-
| Kunthi Puthra || Vijay || Vishnuvardhan (actor)|Vishnuvardhan, Shashikumar, Sonakshi || Vijayanand
|- Sai Kumar, Prakash Rai, Vajramuni, Silk Smitha || Hamsalekha
|-
| Looti Gang || B Ramamurthy || Devaraj, Anjana, Mukhyamantri Chandru, Komal Kumar || V. Manohar
|- Lakshmi || V. Manohar
|-
| Mahakshathriya || Rajendra Singh Babu || Vishnuvardhan (actor)|Vishnuvardhan, Sonu Walia, Sudha Rani, Ramkumar, Shankar Ashwath || Hamsalekha
|-
| Mahashakti Maye || Om Shakti Jagadishan || Kalyan Kumar, B. Sarojadevi, K. R. Vijaya, Vinaya Prasad, Lokesh || Hemanth Kumar
|- Baby Shamili, Master Anand, Srinivasa Murthy, Silk Smitha, Dolly || Hamsalekha
|-
| Mandyada Gandu || A. T. Raghu || Ambarish, Sreeshanti, Meghana, Vajramuni|| Upendra Kumar
|-
| Megha Maale || S. Narayan || Sunaad Raj, Master Madhan, Vijayashree || Hamsalekha
|- Ramakrishna || Hamsalekha
|-
| Murder || Mandya Nagraj || Suresh Heblikar, Anjali Sudhakar, Prakash Rai, Nagesh Kashyap || Guna Singh
|-
| Musuku || P H Vishwanath || Ambarish, Dolly, Ramesh Aravind, K. S. Ashwath, Vajramuni || Hamsalekha
|-
| Mutthanna || M.S.Rajshekar || Shivarajkumar, Shashikumar, Supriya, Bhavyasree Rai, Doddanna || Hamsalekha
|-
| Nishkarsha || Sunil Kumar Desai || Ananth Nag, Vishnuvardhan (actor)|Vishnuvardhan, Devaraj, B. C. Patil, Suman Nagarkar, Anjana || Guna Singh
|- Tara || Manoranjan Prabhakar
|-
| Odahuttidavaru || Dorai Bhagwan || Rajkumar (actor)|Rajkumar, Ambarish, Madhavi (actress)|Madhavi, Sreeshanti, Umashree, Vajramuni || Upendra Kumar
|- Jayanthi || V. Manohar
|-
| Rashmi || K. V. Jayaram || Abhijeeth, Shruti (actress)|Shruti, Sindhu Menon || Agasthya
|-
| Rasika (film)|Rasika || Dwarakish || V. Ravichandran, Bhanupriya, Shruti (actress)|Shruti, Dwarakish, Rockline Venkatesh, Jayanthi (actress)|Jayanthi, Puneet Issar || Hamsalekha
|- Lakshmi || V. Manohar
|- Srinath || Upendra Kumar
|-
| Sammilana || H. R. Bhargava || Shashikumar, Shruti (actress)|Shruti, Tara (Kannada actress)|Tara, K. S. Ashwath || Hamsalekha
|-
| Samrat || Naganna || Vishnuvardhan (actor)|Vishnuvardhan, Vinaya Prasad, Soumya Kulkarni, Ramesh Bhat, Vajramuni, Puneet Issar || Hamsalekha
|-
| Sididedda Pandavaru || Om Sai Prakash || Shashikumar, Mohini (actress)|Mohini, Vivek, Siddharth, Balaji || Hamsalekha
|-
| Sididedda Shiva || K. Suresh Reddy || Thiagarajan, Poornima, Ramesh Bhat, Anjali Sudhakar, Prathap || V. Manohar
|-
| Swathi || Shivamani|| Shashikumar, Sudharani, Vajramuni, Umashri || M. M. Keeravani
|-
| Thanike || Niranjan || Gulzar Khan, Sujatha, Disco Shanti || R. Damodar
|- Srinath || Manoranjan Prabhakar
|-
| Time Bomb || Joe Simon || Vishnuvardhan (actor)|Vishnuvardhan, Tiger Prabhakar, Shruti (actress)|Shruti, Puneet Issar, Devaraj, Soumya Kulkarni || Hamsalekha
|- Shruti || Amara Priya
|-
| Yarigu Helbedi || Kodlu Ramakrishna || Ananth Nag, Vinaya Prasad, Tara (Kannada actress)|Tara, Vanitha Vasu, Lokesh || Rajan-Nagendra
|-
|}

==References==
 
 

== See also ==

* Kannada films of 1993
* Kannada films of 1995

 
 

 
 
 
 