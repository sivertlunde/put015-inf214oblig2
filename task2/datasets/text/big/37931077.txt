Eliot & Me
{{Infobox film
| name           = Eliot & Me
| image          = Eliot & Me theatrical poster.jpg
| image size     = 200px
| alt            = 
| caption        = Theatrical poster
| director       = Fintan Connolly
| producer     = Fiona Bergin
| writer         = Fintan Connolly Fiona Bergin
| narrator       =  David Wilmot Joshua Brown Des Fitzgerald Lauren Stone Gerard Mannix Flynn Vincent McCabe Eliot the dog
| music          = Stephen Rennicks
| cinematography = Owen McPolin
| editing        = Guy Montgomery Mairead McIvor
| studio         = Fubar Films with the participation of Bord Scannán na hÉireann / the Irish Film Board in association with RTÉ
| distributor    = 
| released       =  
| runtime        = 60 minutes
| country        = Ireland
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Eliot & Me is a 2012 Irish drama film. The plot follows the adventures of a young girl trying to find her dog in Dublin. The film is directed by Fintan Connolly and features his then 10 year old daughter Ella Connolly in the lead role.

== Plot ==
Dublin. The present. Ten-year-old Lucy struggles to come to terms with her parents’ separation. She adopts a dog, Eliot, from the local dog shelter. It is love at first sight and her life starts to improve. But tragedy strikes when Eliot goes missing and Lucy undertakes a dangerous journey to find him. 

== Tagline ==

Sometimes it’s hard to find what you’re looking for

== Cast ==
*	Lucy – Ella Connolly
*	Mother – Renée Weldon David Wilmot
*	Ben – Joshua Brown
*	Window Cleaner – Des FitzGerald
*	Cathy – Lauren Stone
*	Garda – Gerard Mannix Flynn
*	DSPCA Officer – Vincent McCabe
*	Eliot the dog

== Production ==
Connolly developed and co-wrote the script for Eliot & Me with producer Fiona Bergin. “It was a bit of a family affair’, says Connolly “we shot at weekends and school holidays, whenever Ella was free. We cast actors we’d worked with before, like Renée Weldon, David Wilmot and Mannix Flynn. It was a real labour of love. We filmed all over the city, but mainly in Kilmainham.”

Describing working with his daughter Ella he said: “After playing a spooky kid in Hammer horror Wake Wood, she wanted to be in a film she could watch. Having worked on that film which was shot in winter and involved prosthetics and special effects, this was relatively straightforward, She dug deep to play this little girl and to give her a vulnerability and strength in keeping with the story. She was a joy to work with. She did everything in a couple of takes. And I’m not just saying that because I’m her dad. It was fun.”

Twelve-year-old Ella said about the experience “There is an awful lot of waiting around making films. It’s not nearly as glamorous as people think. One of the good things about Eliot & Me was working with my dog. He’s completely untrained, he hasn’t got a clue, he didn’t do anything he was asked. I thought that was hilarious. But he is the best thing in the film.” 

== Reception ==
Richard Propes described the film as “a warm and gentle family film, featuring a wonderful performance from Ella Connolly and a heart and spirit that will warm dog lovers and family film lovers alike. The film bears an easy comparison to the British made version of Lassie, though that comparison is a tad unfair as Eliot & Me is a delight all its own. Its warm heart and winning spirit will likely breeze through your mind for days after watching the young Connolly and this adorable Yorkshire Terrier.”  
 

== Accolades ==
The film had its world premiere as the opening film at the 52nd Zlín Film Festival in the Czech Republic in May 2012. Subsequently Eliot & Me was an official selection at the SCHLINGEL International Film Festival in Germany, Cinekid Festival in The Netherlands, Oulu International Childrens and Youth Film Festival in Finland and the Olympia International Film Festival for Children and Young People in Greece.  In June 2013 Ella Connolly won the Best Actor award at the Dream Fest International Film Festival for Children and Youth in the Romanian town of Slatina. Jury members described Connolly’s performance as “very mature and convincing”. 

==Awards==
In September 2013 Ella Connolly won two awards for ‘Eliot & Me’ at the 12th China International Children’s Film Festival: the International Jury Award for Best Performance by a Child Actress and the Award for the Children’s Favorite Child Performer. Famous Chinese actress Jiang Wenli chaired the international jury and singled out Ms. Connolly’s performance as “compelling and authentic, with a naturalness rare in child acting. Her character was very believable”. The second award was voted by the children’s jury, which was made up of 200 children from local primary and middle schools.
 

== Music ==
The musical score for Eliot & Me was written by Stephen Rennicks.

== References ==
 

== External links ==
* Eliot & Me   at the Internet Movie Database
* Eliot & Me Facebook  
* Eliot & Me trailer  
* Eliot & Me stills  
* Fintan Connolly interview  
* Fintan Connolly interview YouTube  
*  

 
 
 
 
 
 