Sami swoi
{{Infobox film
| name = Sami swoi
| image = Sami swoi DVD cover.jpg
| caption = DVD cover of color version
| director = Sylwester Checinski 
| producer = 
| writer = Andrzej Mularczyk 
| starring = Waclaw Kowalski  Wladyslaw Hancza  Zdzislaw Karczewski 
| music = Wojciech Kilar 
| cinematography = Stefan Matyjaszkiewicz 
| editing = Janina Niedzwiecka 
| distributor =  1967
| runtime = 81 min.
| country = Poland Polish
| budget = 
| preceded_by = 
| followed_by = 
}} comedic trilogy of movies by Sylwester Chęciński. Its two follow-ups are Nie ma mocnych (a Polish idiom meaning "no can do") (1974) and Kochaj albo rzuć ("Love It or Leave It") (1977).

== The movie ==
The film was black and white but was colorized in 2000 by Dynacs Digital Studios for Polish television station Polsat. The score was composed by Wojciech Kilar.

The movie was filmed mostly in Dobrzykowice near Wrocław, with some scenes at Lubomierz and surrounding areas.

It was one of the most popular Polish comedies of its times and still remains an old favorite. Lubomierz has a museum dedicated to the movies, and Toruń has a statue of the two main heroes, Kargul and Pawlak.

== The story ==
  were resettled plowed a few inches (3-fingers-width) into the Pawlaks territory, for which one of the Pawlaks hit him with a scythe and then, fearing retribution, emigrated to the United States. Years later, he comes back, and finds that both families live peacefully. His brother, Kazimierz, tells him the story of how the families came to terms, in a form of Romeo and Juliet-like marriage between Pawlaks son, Witia, and Karguls daughter, Jadźka.

== The actors ==
*Pawlak family
**Wacław Kowalski - Kazimierz (Kaźmirz)
**Zdzisław Karczewski - Jan John
**Jerzy Janeczek - Witia
**Maria Zbyszewska - Mania
**Natalia Szymańska - Leonia
**Zygmunt Bielawski - Paweł (Pawełek)
*Kargul family
**Władysław Hańcza - Władysław (Władyś)
**Ilona Kuśmierska - Jadwiga (Jadźka)
**Halina Buyno-Łoza - Aniela (Anielcia)
*Others
**Eliasz Oparek-Kuziemski - Kokeszko
**Aleksander Fogiel - sołtys (village headman)
**Witold Pyrkosz - the guy from Warsaw
**Kazimierz Talarczyk - Wieczorek
**Ryszard Kotys - cats seller
**Andrzej Mrożek - Russian soldier
**Jan Łopuszniak - German priest

== References ==

 

== External links ==
 
* 

 
 
 
 