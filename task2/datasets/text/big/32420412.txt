Aaltra
{{Infobox film
| name           = Aaltra
| image          = 
| caption        = 
| director       = Gustave de Kervern Benoît Delépine
| producer       = Guillaume Malandrin Vincent Tavier
| writer         = Gustave de Kervern Benoît Delépine
| starring       = Benoît Delépine Gustave de Kervern Michel de Gavre Gérard Condejean
| music          = Les Wampas
| cinematography = Hugues Poulain
| editing        = Anne-Laure Guégan
| studio         = 
| distributor    = Ad Vitam Distribution
| released       =  
| runtime        = 92 minutes
| country        = Belgium France
| language       = French English Finnish German Dutch
| budget         = 
| gross          = 
}}
 Belgian French language|French-language deadpan black comedy film directed and written by Gustave de Kervern and Benoît Delépine. The film won four awards and was nominated for three others.

==Plot==
 

==Cast==
*Benoît Delépine as The Employee
*Gustave de Kervern as The Farm Worker
*Michel de Gavre as Farmer
*Benoît Poelvoorde as Motocross Rider
*Gérard Condejean as The Chinese
*Isabelle Delépine as Wife

== Accolades ==

===Won===
*London Film Festival 2004
**FIPRESCI Prize - Benoît Delépine and Gustave de Kervern
*Puchon International Fantastic Film Festival 2004
**Best Actor - Benoît Delépine and Gustave de Kervern
*Transilvania International Film Festival 2004
**Audience Award - Benoît Delépine and Gustave de Kervern
*Joseph Plateau Awards 2005
**Best Belgian Actor - Benoît Poelvoorde

===Nominations===
*Copenhagen International Film Festival 2004
**Golden Swan Award - Benoît Delépine
*Rotterdam International Film Festival 2004
**Tiger Award - Benoît Delépine and Gustave de Kervern
*Joseph Plateau Awards 2005
**Best Belgian Film

== External links ==
* 

 
 
 
 
 
 
 
 
 


 
 