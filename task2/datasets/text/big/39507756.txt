Gone Girl (film)
 
 
{{Infobox film
| name           = Gone Girl
| image          = Gone Girl Poster.jpg
| alt            = A man in a blue shirt standing by a body of water, wispy clouds in the blue sky above. A womans eyes are superimposed on the sky. Near the bottom of the image there are horizontal distortion error lines. 
| caption        = Theatrical release poster
| director       = David Fincher
| producer       = {{Plain list | 
* Leslie Dixon 
* Bruna Papandrea
* Reese Witherspoon 
* Ceán Chaffin
}}
| screenplay     = Gillian Flynn
| based on       =  
| starring       = {{Plain list | 
* Ben Affleck 
* Rosamund Pike 
* Neil Patrick Harris 
* Tyler Perry
 
}}
| music          = {{Plain list |
* Trent Reznor
* Atticus Ross
}}
| cinematography = Jeff Cronenweth
| editing        = Kirk Baxter
| studio         = {{Plain list |
* Regency Enterprises Pacific Standard
}}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 149 minutes  
| country        = United States
| language       = English
| budget         = $61 million 
| gross          = $368.1 million  
}} adapted by 2012 novel of the same name. It stars Ben Affleck and Rosamund Pike. Set in the Midwestern United States, the films story begins as a mystery which follows the events surrounding Nick Dunne (Affleck), who becomes the primary suspect in the sudden disappearance of his wife, Amy (Pike).
 world premiere nationwide theatrical Academy Award, BAFTA Award, Golden Globe Screen Actors Golden Globe BAFTA Award and 20th Critics Choice Awards|Critics Choice Award nominations for Flynns adapted screenplay, receiving the award for the latter. 

==Plot==
The day of his fifth wedding anniversary, Nick Dunne returns home to find that his wife Amy is missing. Her disappearance receives heavy press coverage, as Amy was the inspiration for her parents popular Amazing Amy childrens books. Suspicions arise that Nick murdered her, and his awkward behavior is interpreted by the media as characteristic of a sociopath.
 recession and moved from New York City to Missouri. Nick has become lazy, distant, uninterested and unfaithful. Detective Rhonda Boney uncovers evidence of financial troubles and domestic disputes, a report indicating that Amy wanted to purchase a gun, and poorly concealed evidence of a struggle. She also finds a medical report indicating that Amy is pregnant, of which Nick denies knowledge.

Amy, after discovering his affair, planned to frame Nick for her murder by ingratiating herself into local life, faking a pregnancy, and fabricating a diary describing her fear of him. By using the clues in a "treasure hunt" game she and Nick play on their anniversary, she ensures he visits places where she has planted evidence against him for the police to find. Amy has changed her appearance and name and is hiding in a distant campground, believing Nick will be convicted and executed for her murder, and contemplating committing suicide after his conviction.

Nick hires Tanner Bolt, a lawyer who specializes in defending men accused of killing their wives. Nick meets Amys ex-boyfriend Tommy OHara, who claims Amy framed him for rape. He also approaches another ex-boyfriend, the wealthy Desi Collings—against whom Amy previously filed a restraining order—but Desi refuses to share any details.

When Amys neighbors rob her at the campground she is hiding in, she is left without money and calls Desi: she convinces him that she ran away from Nick because she feared for her life. He agrees to hide her in his lake house, which is equipped with surveillance cameras. 

Nick convinces his twin sister, Margo, of his innocence. After Nicks mistress, his student, reveals their affair at a press conference, Nick appears on a talk show to profess his innocence and apologize for his failures as a husband in the hope of luring Amy. His performance rekindles Amys feelings for him, even as Boney formally charges him with murder. Meanwhile, Amy inflicts injuries on herself to make it appear Desi kidnapped and abused her, then seduces Desi and kills him during sex by slitting his throat with a box cutter. She returns home covered in blood, naming Desi as her captor and rapist and clearing Nick of suspicion. 
 FBI sides with Amy, forcing Boney to back down.

Amy tells Nick the truth, saying that the man she watched pleading for her return on TV is the man she wants him to become again. Nick shares this with Boney, Bolt, and Margo, but they have no way to prove Amys guilt. Nick intends to leave Amy and expose her lies, but Amy reveals she is pregnant, having artificially inseminated herself with Nicks sperm stored at a fertility clinic. Nick doubts the child is his and says he will undertake a paternity test.

Nick reacts violently to Amys insistence that they remain married, but feels responsible for the child. Despite Margos objections, he reluctantly decides to stay with Amy. The "happy" couple announces on television that they are expecting a baby.

==Cast==
 , Gillian Flynn, Ben Affleck, Rosamund Pike, Neil Patrick Harris and Tyler Perry at the 52nd New York Film Festival.]]
 
* Ben Affleck as Nick Dunne 
* Rosamund Pike as Amy Elliott-Dunne, Nicks missing wife
* Neil Patrick Harris as Desi Collings, Amys ex-boyfriend
* Tyler Perry as Tanner Bolt, Nicks attorney
* Carrie Coon as Margo "Go" Dunne, Nicks twin sister 
* Kim Dickens as Detective Rhonda Boney, the lead investigator on Amys disappearance
* Patrick Fugit as Officer James Gilpin, Boneys partner
* Missi Pyle as Ellen Abbott, a cable TV host based on Nancy Grace        
* Emily Ratajkowski as Andie Fitzgerald, Nicks mistress and student
* Casey Wilson as Noelle Hawthorne, Nick and Amys neighbor
* Lola Kirke as Greta, a young woman Amy meets at a motel
* Boyd Holbrook as Jeff, a flirtatious man Amy meets at a motel                            
* Sela Ward as Sharon Schieber, a network TV host
* Scoot McNairy as Tommy OHara, a former boyfriend of Amys
 

==Production==

===Development=== novel of the same name. One of the films producers, Leslie Dixon, read the manuscript of the novel in 2011 and brought it to the attention of Reese Witherspoon in December of that year. Witherspoon and Dixon then collaborated with Bruna Papandrea to further develop the manuscript—with Flynns film agent, Shari Smiley, they met with film studios in early 2012. 
 optioned the book in a northern autumn deal with Flynn, in which the author negotiated that she would be responsible for the first draft of the screenplay. By around October 2012, Flynn was engaged in the production of the first draft while she was also involved in the promotional tour for her novel. A first-time screenwriter at the time, Flynn later admitted: "I certainly felt at sea a lot of times, kind of finding my way through."   

Flynn submitted her first draft screenplay to the Fox studio in December 2012, before Fincher was selected as the director for the project.    Fincher had already expressed interest in the project, and after he completed Flynns first draft, a meeting was scheduled between the director and author within days. Typically, authors are removed from film adaptations following the first draft and an experienced screenwriter takes over; but, on this occasion, Fincher agreed to work with Flynn for the entire project. Flynn later explained: "... he   responded to the first draft and we have kind of similar sensibilities. We liked the same things about the book, and we wanted the same thing out of the movie."   

As further preparation, Flynn studied screenplay books and also met with Steve Kloves, who wrote the screenplays for the Harry Potter series.  Fincher also provided guidance and advised the author: "We dont have the ability to gift the audience with the characters thoughts, so tell me how theyre behaving."    During the production of the final screenplay, Fincher and Flynn engaged in an intensive back-and-forth working relationship: Flynn sent Fincher "big swaths" of writing, which he then reviewed, and Fincher would then discuss the swaths with Flynn by telephone. Eventually, some scenes were rewritten "a dozen times", while other scenes were unaltered. 

Following the release of the film, Flynn spoke of an overwhelming adaptation process, in which she tackled a 500-page book with an intricate plot; she explained that her experience working for a magazine meant that she "wasnt ever precious about cutting." As a consequence of the distillation process, most of the parental storylines were lost, so the mother of the character of Desi Collings does not appear in the film, and it was not possible to include flashbacks of Nick Dunnes dead mother.   

In terms of the films ending, Flynn revealed that she experimented with a "lot of iterations". One of the aspects that she was certain of was the presence of the media, which she described as the "third player", alongside Nick and Amy. In Flynns words: "Once we got to the ending, I wanted it to wrap up quickly. I didnt want 8 million more loop-de-loops… I had no problem tossing stuff out and trying to figure out the best way to get there." 

Flynn enjoyed the experience of making the film, and she expressed appreciation for Finchers involvement, as he "really liked the book and didnt want to turn it into something other than what it already was", and he also reassured her, even when she second-guessed herself.  Fincher described Flynns screenwriting work as "very smart", "crafty", and "extremely articulate".  

===Filming=== Cape Girardeau, Missouri, U.S., and was scheduled to last about five weeks.     Some scenes were also filmed in Los Angeles, U.S. 

According to producer Ceán Chaffin, Fincher took, on average, as many as 50 takes for each scene,  while Flynn has said that, although Fincher is a visual director, he is meticulous about  —Fincher changed a scene in which Amy collects her own blood, as he thought it was unbelievable. 

Fincher later called Affleck "extremely bright" in regard to the manner in which he drew on his own experience with the media for the character of Nick Dunne. Fincher explained that Affleck "has a great sense of humor and great wit about what this situation is and how frustrating it is". Fincher described the behavior of the media in the film as "tragedy vampirism", but clarified that "The New York Times and NPR are not in the flowerbeds of the Dunne house". 

==Music==
  The Girl with the Dragon Tattoo. Fincher was inspired by music he heard while at an appointment with a chiropractor and tasked Reznor with creating the musical equivalent of an insincere facade. Reznor explained Finchers request in an interview:

 

The overall score is a combination of soothing sounds with staccato electronic noises, resulting in an unnerving, anxiety invoking quality.  NPR writer Andy Beta concludes: "Reznor and Ross relish being at their most beauteous, knowing that itll make the brutal moments of Gone Girl all the more harrowing."   
 Richard Butler teaser trailer.   The soundtrack album was released on the Columbia label on September 30, 2014. 

The soundtrack was nominated for the 2015 Grammy Award for Best Compilation Soundtrack for Visual Media,    and also for the 2014 Golden Globe Award for Best Original Score.   

==Release==
 .]]
Gone Girl opened the 52nd New York Film Festival, receiving high profile press coverage and early positive reviews. It saw a nationwide release in North America in 3,014 theatres on October 3, 2014. Coinciding with the North America release, Gone Girl released at 5,270 screens in 39 international markets like United Kingdom, Germany, on its opening weekend. 

===Box office===
 
Gone Girl grossed $167.8 million in the U.S. and Canada and $200.3 million in other territories for a worldwide total of $368 million, against a budget of $61 million.  Calculating in all expenses, Deadline.com estimated that the film made a profit of $129.99 million. {{cite web|url=http://deadline.com/2015/03/gone-girl-profit-box-office-2014-1201390479/|title=No. 12 ‘Gone Girl’ – 2014 Most Valuable Blockbuster Movie Tournament
|author=Mike Fleming Jr|publisher=Deadline.com|date=March 10, 2015|accessdate=March 21, 2015}} 
 Pearl Harbor which opened with $59.1 million and Daredevil (film)|Daredevil  which opened with $40.3 million) and Rosamund Pike’s second biggest opening (behind Die Another Day which opened with $47 million). The film is the tenth biggest October debut overall. The film played 60% female and 75% over-25 years old.  The film topped the box office for two consecutive weekend despite facing competition with Dracula Untold in its second weekend  before being overtaken by Fury (2014 film)|Fury in its third weekend. 

Outside North America, it earned $24.6 million from 5,270 screens in 39 international markets on its opening weekend, higher than expected.    High openings were witnessed in the United Kingdom ($6.7 million),  Australia ($4.6 million),  France ($3.65 million)    Russia ($3.4 million),  and Germany ($2.6 million).   

===Critical response===
Gone Girl received very positive reviews  from critics, with Pikes performance in particular earning praise.   while bringing the best out of stars Ben Affleck, Rosamund Pike and Tyler Perry."  Metacritic gave the film a score of 79 out of 100, based on 49 critics, indicating "generally favorable reviews".  Audiences surveyed by CinemaScore gave the film a B grade.    

The   called the film a "brilliantly glacial adaptation… This may not be the perfect film—but it is a perfect adaptation". 

Joshua Rothman wrote in The New Yorker on October 8, 2014 that he enjoyed the film "in all its abstract, intellectual, postmodern glory" and that, similar to other post-modern narratives, the film adaptation is "decisively unreal ...   heroes and villains in Finchers Gone Girl arent people but stories". Rothman, who draws parallels between Gone Girl and Finchers 1999 adaptation Fight Club, decides that the film is ultimately a farce and has resonated with filmgoers because it expresses "a creepy, confused, and troubling part of us". 

Anthony Lane of The New Yorker wrote: "At first blush, Gone Girl is natural Fincherland ... so why doesnt the movie claw us as The Social Network did? Who could have predicted that a film about murder, betrayal, and deception would be less exciting than a film about a website?"  Slant s Ed Gonzalez awarded the film two out of five stars, concluding: "Fincher and Flynn should have gone further and truly grappled with the real horror that, by giving his relationship with Amy another chance, Nick is indulging in one of the great myths of feminism: that it emasculates men. Rather than undermine that noxiousness, Fincher enshrouds it in funereal brushstrokes that cast his Gone Girl as a fashionable tumbling into an abyss of willful denial." 

===Gender issues===
In a 2013 interview with Time Out writer Novid Parsi, who described the ending of the novel as "polarizing", Flynn explained that she wanted the Gone Girl novel to counter the notion that women are "naturally good" and to show that women are "just as violently minded as men are".  In a November 2014 interview, Flynn admitted that the critical gender-related response did affect her: "I had about 24 hours where I hovered under my covers and was like: I killed feminism. Why did I do that? Rats. I did not mean to do that. And then I very quickly kind of felt comfortable with what I had written." 
 social privilege—whereby misogynist myths and fears about female behavior."  Alyssa Rosenberg wrote in the Washington Post on October 3, 2014, that, although she was initially "unconvinced" by the book, her fascination with the novel and film was partly due to her conclusion that "Amy Elliot Dunne is the only fictional character I can think of who might be accurately described as simultaneously misogynist and misandrist." 

In an October 6, 2014, article titled "Gone Girl  s Biggest Villain Is Marriage Itself", Jezebel  s Jessica Coen wrote: "Movie Amy pales in comparison to the vivid character we meet in the book. Strip away Book Amys complexities and youre left with little more than crazy fucking bitch. That makes her no less captivating, but it does make the film feel a lot more misogynistic than the novel."    Coen concedes that this did not negate her enjoyment of the film, "as we ladies are well accustomed to these injustices."  Time s Eliana Dockterman wrote on the same date that Gone Girl is both "a sexist portrayal of a crazy woman" and a "feminist manifesto," and that this duality makes the film interesting.  Zoë Heller of The New York Review of Books wrote: "The problem with Amy is not that she acts in vicious and reprehensible ways, or even that her behavior lends credence to certain misogynist fantasies. The problem is that she isnt really a character, but rather an animation of a not very interesting idea about the female capacity for nastiness", concluding that "The film is a piece of silliness, not powerful enough in the end to engender proper disapproval: only wonder at its coarseness and perhaps mild dismay at its critical success." 

Writing in The Guardian on October 6, 2014, Joan Smith criticized what she saw as the films "recycling of rape myths", citing research released in 2013 which found that false allegations of rape in the UK were extremely rare.    She wrote: "The characters live in a parallel universe where the immediate reaction to a woman who says shes been assaulted is one of chivalrous concern. Tell that to all the victims, here and in the US, who have had their claims dismissed by  sceptical  police officers."  Writing for the Guardian on the following day, Emine Saner wrote that Smiths argument "wouldnt carry as much weight were this film set against a vastly wider range of womens stories, and characters in mainstream culture", but concluded with Docktermans plea for the portrayal of "all sorts of women in our novels". 

Tim Kroenert, of the Australian Eureka Street website, wrote on October 8, 2014, that the films predominant focus upon Nicks perspective "serves to obfuscate Amys motives (though it is possible that she is simply a sociopath), and to amplify her personification of ... anti-women myths"; however, Kroenert concludes that Gone Girl is "a compelling rumination on the impossibility of knowing the mind of another, even within that ostensibly most intimate of relationships, marriage." 

===Accolades===
 

===Home media===
Gone Girl was released on DVD and Blu-ray Disc on January 13, 2015.  The Blu-ray release comes with a 36-page Amazing Amy book called Tattle Tale. 

===Sequel===
In an interview in October 2014, Rosamund Pike stated she would return for a sequel on the condition that Gillian Flynn again wrote the script. 
In January 2015, author Gillian Flynn was open to the idea of a sequel, but said it would be "a few years down the road" when the original cast and crew would be available again. 

==References==
{{Reflist|colwidth=30em|refs=
   

   

   

   

   

   

   
}}

==External links==
 
* 
* 
* 
* 
* 
* 
*  promotional website

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 