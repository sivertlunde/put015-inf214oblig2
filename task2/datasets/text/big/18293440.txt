Honeymoon in Bali
{{Infobox film
| name           = Honeymoon In Bali
| image          = Honeymoon in Bali film poster.jpg
| image_size     = 
| caption        = 
| director       = Edward H. Griffith
| producer       = 
| writer         = Virginia Van Upp (screenplay)  Katharine Brush Grace Sartwell Mason
| narrator       = 
| starring       = Fred MacMurray Madeleine Carroll Akim Tamiroff
| music          = 
| costumes       = Edith Head
| cinematography = 
| editing        = Eda Warren
| distributor    = Paramount Pictures
| released       = September 29, 1939
| runtime        = 95 min
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} romantic comedy film. It is also known by the alternative title Husbands or Lovers and My Love For Yours. Virginia Van Upps screenplay was based on the short stories Our Miss Keane by Grace Sartwell Mason in The Saturday Evening Post of 24 May 1923 and Free Woman by Katharine Brush in Redbook magazine from Nov-Dec 1936. In 1936 Paramount announced a film of Our Miss Keane to star Merle Oberon to be produced. 

==Plot==
On a rainy New York City autumn afternoon, the head of a major Department Store, Gail Allen, meets her second cousin and best friend Lorna for afternoon tea. Her cousin, an author of love stories set in the South Seas, invites a resident fortune teller to predict Gails future. At first the reading sounds like a hundred others, until she foresees her having a child and meeting a man whose arm was cut by a natives rice knife.  

The fortune teller predicts as Neptune is in her sign at the moment she could find herself walking down a street and taking an unexpected turn where things would change.  Thinking that her career will come first, Gail does not like her predicted future but finds herself taking an unexpected turn that takes her into a shop that sells sailboats. There she meets Bill Burnett who lives in Bali and is holidaying in New York.  Beginning with Bills injury from a natives rice knife, all of the predictions eventually come to pass.

==Cast==
Fred MacMurray  ...  Bill Willie Burnett   
Madeleine Carroll  ...  Gail Allen    Allan Jones  ...  Eric Sinclair    
Akim Tamiroff  ...  Tony, the Window Washer   
Helen Broderick  ...  Lorna Smitty Smith    
Osa Massen  ...  Noel Van Ness   
Carolyn Lee  ...  Rosie   
Astrid Allwyn  ...  Fortune Teller   
Georgia Caine  ...  Miss Stone, Gails Secretary   
Benny Bartlett  ...  Jack the Singing Telegram Boy   
Monty Woolley ... Lornas Publisher   Charles Lane ... Photographer  
Janet Waldo & Luana Walters ... Fortune Tellers companions

==Reception==
A review from The Washington Post, on October 5, 1939, says "Honeymoon in Bali Is Delightfully Easy To Take!"  The Los Angeles Times review from October 13, 1939, says "Honeymoon in Bali Light, Romantic Comedy."  Hollywood In Bali got 6.8 out of 10 on IMDB.com.

==Alternate names==
* Are Husbands Necessary? (working title; later used by Paramount for an unrelated 1942 comedy with Ray Milland and Betty Field)
* Husbands Or Lovers (UK)
* My Love For Yours (video title)

==Quotes==
"Theres not a wall between freedom and loneliness, you can fall into it without warning" - Lorna

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 