A Date with a Dream
 
 
{{Infobox film
| name           = A Date with a Dream
| image          = "A_Date_with_a_Dream"_(1948).jpg
| caption        = 
| director       = Dicky Leeman
| producer       = Robert S. Baker Monty Berman
| writer         = Robert S. Baker Monty Berman Dicky Leeman Carl Nystrom
| screenplay     = 
| story          = 
| based on       =  
| starring       = Terry-Thomas Jeannie Carson Len Lowe Bill Lowe
| music          = 
| cinematography = Monty Berman
| editing        = Anne Barker
| studio         = Tempean Films
| distributor    = Grand National Pictures
| released       =   
| runtime        = 55 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
A Date with a Dream is a 1948 British musical comedy film directed by Dicky Leeman and starring Terry-Thomas, Jeannie Carson and Wally Patch.  Its plot concerns a wartime group of musical entertainers who meet up a year after being demobbed and decide to reform their act. It was the first production of Tempean Films.This was one of Terry-Thomass first films and is reputedly partly based on his own experiences.  

==Plot==
When an Army concert party is disbanded after the war, they plan to meet up in a years time for a reunion. When they do they discover that all the various members arent coping too well with civilian life. Jean, a singer who is staying in the same house as two of the ex-concert party members, suggests that the various members get back together to perform. 

==Cast==
* Terry-Thomas – Terry
* Jeannie Carson – Jean
* Len Lowe – Len
* Bill Lowe – Bill
* Wally Patch – Uncle
* Vic Lewis – Vic
* Ida Patlanski – Bedelia
* Joey Porter – Max Imshy
* Alf Dean – Joe
* Julia Lang – Madam Docherty
* Harry Green – Syd Marlish
* Norman Wisdom – Shadow Boxer
* Sydney Bromley – Stranger in Maxs office

==Critical reception==
Sky Movies wrote, "a sparkling performance from the young Terry-Thomas and a bright, sassy attitude saves this dated, rather choppy little filler item from the scrapheap of movie history. Worth a look for nostalgia buffs and curio collectors."  

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 