Aadu (film)
 
 
 
{{Infobox film
| name           = Aadu  250px 
| caption        = Film poster
| director       = Midhun Manuel Thomas 
| producer       =  Vijay Babu Sandra Thomas 
| writer         = Midhun Manuel Thomas
| starring       = Jayasurya Sunny Wayne Vinayakan Renji Panicker Vijay Babu Vineeth Mohan
| music          = Shaan Rahman
| lyrics         = Manu Manjith
| cinematography = Vishnu Narayan
| editing        = Lijo Paul
| production company = Friday Film House  
| distributor    = Friday Tickets
| country        = India
| released       =  
| language       = Malayalam budget          = gross           =
}}
Aadu - Oru Bheegara Jeevi Aanu (  romantic comedy film directed by Midhun Manuel Thomas. The movie has Jayasurya  and Srinda Ashab in the lead roles.

The movie is produced by Vijay Babu and Sandra Thomas under the banner Friday Film House. The film released on February 6th and received mostly negative reviews from critics and audience. 

It is a road trip movie of of 7 village idiots with a she-goat.

==Cast==
* Jayasurya as Shaji Pappan
* Nanny Goat as Pinky
* Swathi Reddy as Pinky
* Srinda Ashab as Mary/Shaji Pappans wife
* Sunny Wayne as Sathan Xavior
* Aju Varghese as Ponnappan
* Vinayakan as Dude
* Renji Panicker as Thomas Paappan
* Vijay Babu as Sarbath Shemeer
* Sandra Thomas as Menaka Kanthan
* Vineeth Mohan as Moonga kuttan
* Bhagath Manuel as Krishnan
* Chemban Vinod Jose as Highrange Hakkem
* Indrans as PP Sasi
* Dharmajan Bolgatty as Caption Cleetus (Sachin Cleetus)
* Bijukuttan as Battery Simon
* Saiju Kurup as Arakkal Abu
*  Hari Krishnan as Lolan
*Chembil Ashokan as Vaidyar
*Anjali Aneesh Upasana as Usha
*Sudhi Kopa as Kanjavu Soman
*Nelson as Dragon Paili
*Pradeep Kottayam as Head Constable

==Soundtrack==
{{Infobox album
| Name = Aadu Oru Bheegara Jeeviyanu
| Artist = Shaan Rahman
| Type = Soundtrack
| caption =
| Cover =
| Released = 
| Recorded = 2014
| Genre = Film soundtrack
| Length = 
| Language = Malayalam
| Label = 
| Producer = Shaan Rahman
| Last album = Ormayundo Ee Mukham (2014)
| This album = Aadu Oru Bheegara Jeevi Aanu (2015)
| Next album = 
}}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 