Abandoned (1949 film)
{{Infobox film
| name           = Abandoned
| image          = Abandoned 1949 film.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Joseph M. Newman Jerry Bresler
| screenplay     =  Irwin Gielgud Jeff Chandler
| music          = Walter Scharf
| cinematography = William H. Daniels
| editing        = Edward Curtiss
| distributor    = Universal Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English}}

Abandoned is a 1949 American crime film noir directed by Joseph M. Newman.  The drama features Dennis OKeefe, Gale Storm, and others. 

==Plot==
A womans sister goes missing in Los Angeles.  She goes to city hall to see if she can get any information about her disappearance.  Although the police at first cant assist she does get support from a local crime reporter.  The two investigate the disappearance which leads them to a shady detective and a black-market baby ring.

==Cast==
* Dennis OKeefe as Mark Sitko
* Gale Storm as Paula Considine
* Marjorie Rambeau as Mrs. Leona Donner
* Raymond Burr as Kerric
* Will Kuluva as Little Guy Decola Jeff Chandler as Chief MacRae
* Meg Randall as Dottie Jensen
* Jeanette Nolan as Major Ross David Clarke as Harry, Mrs. Donners Chauffeur
* Mike Mazurki as Hoppe
* Perc Launders as Detective Dowd
* Steve Darrell as Detective Brenn

==Reception==

===Critical response===
A.W., writing for The New York Times called the film a "briskly-paced thriller" but said the movie "proceeds along conventional melodramatic lines". 

This was Chandlers first film for Universal under a long term contract, and the positive response to his performance began his graduation into leading roles. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 