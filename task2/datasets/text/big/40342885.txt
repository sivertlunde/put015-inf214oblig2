Stay (2013 film)
 
 
{{Infobox film
| name           = Stay
| image          = 
| caption        = 
| director       = Wiebke von Carolsfeld
| producer       = Andrew Boutilier David Colins Martina Nilan Martin Paul-Hus Mark Slone
| writer         = Wiebke von Carolsfeld
| based on       =  
| starring       = Taylor Schilling Aidan Quinn Michael Ironside
| music          = Robert-Marcel Lepage
| cinematography = 
| editing        = Yvann Thibaudeau
| distributor    = Entertainment One
| released       =     
| runtime        = 99 minutes
| country        = Canada Ireland
| language       = English
| budget         = 
}}

Stay is an upcoming Canadian-Irish drama film co-production. It is directed by Wiebke von Carolsfeld who adapted the story from the Aislinn Hunter novel.

==Production==
The film was produced by Canadian production companies Amérique Film and 422 Films and by Irish company Samson Films.   

==Cast==
* Taylor Schilling as Abbey
* Aidan Quinn as Dermot
* Michael Ironside Brian Gleeson
* Barry Keoghan
* Rayleen Kenny
* Nika McGuigan
* Chris McHallem
* Pascale Montpetit

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 