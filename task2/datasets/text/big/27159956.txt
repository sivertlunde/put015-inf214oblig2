A Chapter in Her Life
{{Infobox film
| name           = A Chapter in Her Life thumb
| caption        = Advertisement for film
| director       = Lois Weber
| producer       =
| writer         = Doris Schroeder Lois Weber (adaptation)
| based on       =  
| starring       = Claude Gillingwater Jane Mercer Jacqueline Gadsden Robert Frazer
| music          =
| cinematography = Benjamin H. Kline
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
| gross          =
}}
A Chapter in Her Life (1923) is an American film based on the novel Jewel: A Chapter in Her Life by Clara Louise Burnham. The film was directed by Lois Weber. She had previously adapted the same novel as the 1915 film Jewel, which she co-directed (uncredited) with her then-husband and collaborator Phillips Smalley. Weber made this film shortly after her divorce from Smalley.

==Plot==
Jewel (Mercer) is a young granddaughter who stays with her grizzled, angry grandfather while her parents are overseas on business. Anger and squabbling amongst the family are brought to heel through love, understanding and the teachings of Christian Science through Jewels pure sweet love for others and trust in Divine Love. 

==Cast==
* Claude Gillingwater as Mr. Everingham
* Jane Mercer as Jewel
* Jacqueline Gadsden as Eloise Everingham
* Frances Raymond as Madge Everingham
* Robert Frazer as  Dr. Ballard
* Eva Thatcher as Mrs. Forbes
* Ralph Yearsley as Zeke Forbes Fred Thomson as Nat Bonnell
* Beth Rayon as Susan

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 