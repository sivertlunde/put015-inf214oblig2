Sway (film)
{{Infobox film
| name           = Sway
| image          = Yureru.jpg
| caption        =
| imdb_rating    =
| director       = Miwa Nishikawa
| producer       = Kiichi Kumagai	
| writer         = Miwa Nishikawa
| starring       = Jo Odagiri, Teruyuki Kagawa, Masatō Ibu, Hirofumi Arai, Yōko Maki (actress)|Yōko Maki
| music          = 
| cinematography = Hiroshi Takase   
| editing        = Ryuji Miyajima
| distributor    = 2006
| runtime        = 119 min.
| country        = Japan Japanese
| budget         =
| preceded by    =
| followed by    =
| mpaa_rating    =
| tv_rating      =
}}
 2006 mystery-drama Japanese film director Miwa Nishikawa, which features sibling rivalry and a possible murder.

==Plot summary==
Tokyo fashion photographer Takeru Hayakawa (Jo Odagiri) returns to his hometown for his mothers memorial service. His late arrival at the memorial irritates his father (Masatô Ibu), who accuses him of disrespecting his late mother and shaming their family name. Takerus older brother Minoru (Teruyuki Kagawa) quickly intervenes and soothes their father with drinks and sympathetic comments.

Takeru and Minoru later have a conversation, which reveals Takerus thinly veiled contempt towards Minoru for not having the strength to leave their dead-end hometown, but Minoru tells him he doesnt understand how things are and besides, hes fine with the way things are. Takeru later learns that his brother has taken over their family business, a run-down garage, and that his ex-girlfriend, Chieko (Yôko Maki), now works there as a gas attendant.

Chieko and Takeru later meet for drinks, and proceed to have a one-night stand. In the morning after, she hesitantly suggests she would be happy to follow him to Tokyo when he leaves, but he pretends he doesnt hear her.

Minoru realises something has happened between his younger brother and Chieko, and suggests they all should go for a walk along the Hasami River in the mountains where Takeru could take photographs. Chieko, believing this would buy more time with Takeru, immediately agrees. Wanting to get away from their towns mundaneness, Takeru agrees.

During the trip, Takeru distances himself from Chieko by walking ahead of her and Minoru. Whilst on a forested mountain, he takes photographs and through his viewfinder, sees Minoru and Chieko talking on a suspension bridge over a gorge. He looks away to take photographs of flowers.

On the bridge, Minoru tries to catch Chieko when she stumbles, but she recoils from his touch as she shouts at him to get away from her. They stare at each other for a few tense seconds. Minoru asks if shes leaving him for Takeru. Chieko replies that she has no interest in him, and never will.

Takeru glances back to the bridge and sees Minoru alone on the bridge, looking down to the gorge below, and realises Chieko has fallen from the bridge. The police locates Chiekos body in the gorge and arrests Minoru for murder. His father rejects the charge, insisting Minoru isnt the type to kill, but Takeru isnt so sure.

As the case proceeds during a courtroom trial, Takeru begins to doubt his testimony when he realises hidden resentment and deeply buried anger between Minoru and himself, which may have affected what he witnessed on the bridge that afternoon.

==Awards== Best Film award at the 2006 Mainichi Film Award.

==Cast==
* Jo Odagiri - Takeru Hayakawa 
* Teruyuki Kagawa - Minoru Hayakawa
* Masatô Ibu - Isamu Hayakawa (father)
* Yōko Maki (actress)|Yôko Maki - Chieko Kawabata 
* Hirofumi Arai - Yohei Okajima
* Keizô Kanie - Osamu Hayakawa (mother)
* Yûichi Kimura - Akihito Maruo
* Tomorowo Taguchi - Judge
* Pierre Taki - Funaki

==References==
 

==External links==
*  

 
 

 
 
 
 