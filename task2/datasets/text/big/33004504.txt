The Ninety and Nine
{{Infobox film
| name           = The Ninety and Nine
| image          = Ninety and nine.jpg
| caption        = Theatrical poster from the Colleen Moore drama The Ninety and Nine. David Smith
| writer         = {{Plain list|
*C. Graham Baker
*Ramsay Morris (play)}}
| starring       = {{Plain list|
*Colleen Moore
*Warner Baxter
*Lloyd Whitlock
*Gertrude Astor}}
| cinematography = W. Steve Smith Jr. (as Steve Smith Jr.) 
| studio         = American Vitagraph Company
| released       =  
| country        = United States Silent (English intertitles)
}}
The Ninety and Nine was a 1922 silent drama starring Colleen Moore made shortly before she gained fame as a film flapper.

==Story==
Tom Silverton is a wrongly accused man whose real name is Phil Bradbury. He was engaged to society girl Kate Van Dyke, but she falls in love with another man. When there is a murder in the Van Dyke home, Bradbury is the primary suspect. He departs before the police can apprehend him and goes to the out-of-the way town of Marlow where he takes the name Tom Silverton. He appears to be a simple drunkard, but Ruth Blake tries to sober him up. She is ostracized by the rest of the town as a result. When Kate and Mark come to town, they spot Tom (Phil) but before he can escape a forest fire surrounds the town. Tom is the only one who kows how to operate the locomotive, but in operating it he will reveal his true identity. He does so, saving the townsfolk. He is exonerated of the crimes and marries Ruth. 

==Cast==
* Warner Baxter as Tom Silverton / Phil Bradbury
* Colleen Moore as Ruth Blake
* Lloyd Whitlock as Mark Leveridge
* Gertrude Astor as Kate Van Dyke Robert Dudley as Abner Blake
* Mary Young as Rachel
* Arthur Jasmine as Bud Bryson
* Ernest Butterworth as Reddy
* Aggie Herring as Mrs. Dougherty
* Dorothea Wolbert as Mrs. Markham
* Rex Hammel as Eric Van Dyke
* Charles R. Moore as Sam Grant

==Background==
Previously shot as a Vitagraph Blue Ribbon Feature in 1916, the original production of The Ninety and Nine had been adapted from a 1902 stage play by Ramsay Morris. The play had been based on the hymn The Ninety and Nine,  which had been, in its own turn, inspired by Luke 15:7. The play concerned the redemption of a fallen man (the single sheep, "Far off from the gates of gold") while the rest of the town stood idly by (the other ninety-nine "that safely lay/In the shelter of the fold"). The new version was intended to be a big production. "Miss Moore will have as her leading man Warner Baxter. Vitagraph is planning to make Ninety and Nine a big special and extra attraction." 

==References==
;Notes
 

;Bibliography
* 

==External links==
* 
* 

 
 
 
 
 
 
 