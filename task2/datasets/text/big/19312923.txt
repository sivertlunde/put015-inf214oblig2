Playin' Hookey
 
{{Infobox film
| name           = Playin Hookey
| image          = 
| caption        = 
| director       = Robert A. McGowan
| producer       = Hal Roach
| writer         = H. M. Walker
| starring       = 
| cinematography = 
| editing        = Richard C. Currier
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 short silent silent comedy film directed by Robert A. McGowan.       It was the 69th Our Gang short subject released.

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Allen Hoskins as Farina
* Jannie Hoskins as Zuccini
* Jay R. Smith as Jay
* Harry Spear as Harry
* Pete the Pup as Pansy

===Additional cast===
* Jean Darling as Jean, Joes sister
* Bobby Hutchins as Wheezer, Joes brother
* Mildred Kornman as Baby
* Harry Arras as Movie-maker
* Chet Brandenburg as Keystone-ish cop
* Ed Brandenburg as Keystone-ish cop
* Cameron Chase as Artie-Fro
* Edgar Dearing as Herr Dun der Blitzen
* Budd Fine as Joes father William Gillespie as Moving picture star Charlie Hall Chaplin
* Jack Hill as Keystone-ish cop
* Sam Lufkin as Keystone-ish cop
* Charles Meakin as Movie-maker
* Arthur Millett as Keystone-ish cop
* Charles A. Millsfield as Movie actor playing a surgeon
* Charley Oelze as Property man
* Lincoln Plumer as Movie-maker
* Tiny Sandford as Mike, studio guard
* Lyle Tayo as Joes mother
* Dorothy Coburn as Woman hit with pie

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 