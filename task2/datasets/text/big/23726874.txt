Spy School
 
{{Infobox film
| name = Spy School
| image = Spy School.jpg
| caption = Theatrical poster
| director = Mark Blutman
| producer = Robert Abramoff
| writer = Mark Blutman David DuBos
| starring = Forrest Landis AnnaSophia Robb Taylor Momsen Rider Strong
| music = Robert Bayless Paul Elliott
| editing = Richard Halsey
| distributor = Screen Media
| released =  
| runtime = 83 minutes
| country = United States
| language = English
| budget = $3 million
| gross =
}}

Spy School (also known as Doubting Thomas) is a 2008 American comedy-drama film, released outside the United States as Doubting Thomas or Lies and Spies. The films stars Forrest Landis and AnnaSophia Robb as the lead characters. The movie focuses on the adventures of Thomas Miller, in his efforts to save the Presidents daughter from being kidnapped. This is the last film for Taylor Momsen, though she now fronts the rock band The Pretty Reckless.

==Plot==
A twelve-year-old boy known for telling tall tales, overhears a plot to kidnap the Presidents daughter. When he goes public with his story, no one believes him, and he is forced to save her on his own.

==Cast==
* Forrest Landis as Thomas Miller
* AnnaSophia Robb as Jackie Hoffman
* Rider Strong as Mr. Randall
* Lea Thompson as Claire Miller
* D.L. Hughley as Albert
* Roger Bart as Principal Hampton 
* Taylor Momsen as Madison Kramer

==References==

 

==External links==
*  

 
 
 
 
 
 


 