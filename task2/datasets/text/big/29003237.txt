Mission in Tangier
 
{{Infobox film
| name           = Mission in Tangier    Mission à Tanger 
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = André Hunebelle
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Raymond Rouleau Gaby Sylvia Mila Parély
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = France
| language       = French
| budget         = 
| gross          = 
}} French drama film directed by André Hunebelle and starring Raymond Rouleau, Gaby Sylvia and Mila Parély.  It was the first in the trilogy of films featuring dashing reporter Georges Masse, it was followed in 1950 by Méfiez-vous des blondes.

==Plot==
During the Second World War, Georges Masse undergoes a dangerous mission by taking secret documents from Tangiers to London.

==Cast==
* Raymond Rouleau: Georges Masse
* Gaby Sylvia: Lily
* Mila Parély: Barbara
* Henri Nassiet: Alexandre Segard
*   general officer
* Christian Bertola: Henri Pelletier
* Pierre Destailles: Maurin
* Jo Dest: Herr von Kloster
* Max Révol: the barkeeper
* Madeleine Barbulée: the typist in the editorial department
* Billy Bourbon: the cabaret spectator
* Gregori Chmara: the Russian singer
* Monique Darbaud: von Klosters female escort

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 