Étoile (film)
{{Infobox film
| name           = Étoile
| image          = Étoile poster.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Peter Del Monte
| producer       = Claudio Mancini, Achille Manzotti	
| writer         = Peter Del Monte
| story          = Sandro Petraglia
| narrator       = 
| starring       = Jennifer Connelly
| music          = Jürgen Knieper
| cinematography = Acácio de Almeida
| editing        = 
| studio         = Gruppo Bema, Reteitalia
| distributor    = 
| released       =  
| runtime        = 
| country        = Italy
| language       = Italian, English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Étoile, also known as Ballet or Star, is a 1989 film starring Jennifer Connelly and Gary McCleery.

==Plot==
The American ballerina Claire Hamilton travels to Hungary to join a prestigious ballet school. The school is haunted by the spirit of a ballerina that died in a carriage accident, that possesses Claire. 

==Cast==
* Jennifer Connelly as Claire Hamilton / Natalie Horvath
* Gary McCleery 	as Jason Forrest
* Laurent Terzieff 	as Marius Balakin
* Olimpia Carlisi 	as Madam
* Charles Durning 	as Uncle Joshua
* Donald Hodson     as Karl
* Mario Marozzi

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 