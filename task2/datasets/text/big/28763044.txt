Murder Will Out (1939 film)
{{Infobox film
| name           = Murder Will Out
| image          = "Murder_Will_Out"_(1939).jpg
| image_size     = 
| caption        = Frederick Burtwell, Crystal Parzik and Jack Hawkins
| director       = Roy William Neill
| producer       = Roy William Neill Brock Williams Derek Twist
| story          = Roy William Neill
| based on       = 
| narrator       =  John Loder Jane Baxter Jack Hawkins
| music          = 
| cinematography = 
| editing        = 
| studio         = Warner Bros. First National Productions 
| distributor    = Warner Bros.
| released       =   
| runtime        = 66 minutes 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British crime John Loder, Jane Baxter and Jack Hawkins, and released by Warner Brothers.  A jade collector is given a piece by a friend, but it soon brings trouble on his shoulders.
 75 Most Wanted" list of missing British feature films.   

==Cast==
* John Loder as Dr. Paul Raymond
* Jane Baxter as  Pamela Raymond
* Jack Hawkins as Stamp
* Hartley Power as Campbell
* Peter Croft as Nigel
* Frederick Burtwell as Morgan
* William Hartnell as Dick
* Ian McLean as Inspector

==Critical reception==
TV Guide wrote, "could have been a good film had more effort been placed in developing the plot; as it is, its almost impossible to figure out what is going on."  

==References==
 

==External links==
* , with extensive notes
* 

 

 
 
 
 
 
 
 

 
 