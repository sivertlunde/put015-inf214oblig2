Pony Express (film)
{{Infobox film
| name           = Pony Express
| caption        = Film poster
| image          = Pony Express FilmPoster.jpeg
| director       = Jerry Hopper
| producer       = Nat Holt Frank Gruber (story) Charles Marquis Warren (writer)
| starring       = Charlton Heston Rhonda Fleming
| cinematography = Ray Rennahan
| editing        = Eda Warren
| distributor    = Paramount Pictures
| released       =  
| runtime        = 101 mins
| country        = United States English
| gross          = $1.4 million (US) 
}} western film directed by Jerry Hopper and starring Charlton Heston as Buffalo Bill, Forrest Tucker as Wild Bill Hickok, Jan Sterling as a Calamity Jane type character  The Pony Express and tells a highly fictionalized account of the formation of the Pony Express rapid transcontinental mail delivery pioneers in 1860’s United States.

==Plot== California secessionists intent on shutting the operation down to encourage California to secede from the Union.

==Cast==
*Charlton Heston as William Frederick Buffalo Bill Cody
*Rhonda Fleming as Evelyn Hastings
*Forrest Tucker as Wild Bill Hickok
*Jan Sterling as Denny Russell
* Michael Moore as Rance Hastings
*Porter Hall as Jim Bridger
* Richard Shannon as Red Barrett Henry Brandon as Joe Cooper Stuart Randall as Pemberton Lewis Martin as Sergeant Russell
*Pat Hogan as Chief Yellow Hand
* Eric Alden as Miller
* Howard Joslin as Harvey

==Production==
Charlton Heston did a film tie-in advertisement for Camel cigarettes. 

==See also==
* Pony Express (TV series)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 

 