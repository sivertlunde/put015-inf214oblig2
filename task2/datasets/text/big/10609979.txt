Follow Thru
 
{{Infobox film
| name = Follow Thru
| image = FollowThru.jpg
| caption = Zelma ONeal sings "I Want to Be Bad"
| producer = Frank Mandel and Laurence Schwab.
| director = Lloyd Corrigan and Laurence Schwab.
| writer = Lloyd Corrigan   based on the play by Frank Mandel and Laurence Schwab Claude King George Olsen
| music = Lew Brown Buddy G. DeSylva Ray Henderson George Marion Jr.  Richard A. Whiting Richard Rodgers Lorenz Hart Ed Eliscu Manning Sherwin Vernon Duke Irving Berlin.
| cinematography = Charles P. Boyle   Henry W. Gerrard   (Technicolor)
| editing = Alyson Shaffer
| distributor = Paramount Pictures
| released =  
| runtime = 92 minutes
| language = English
| country = United States
}}

Follow Thru is a 1930 musical comedy film photographed entirely in Technicolor. It was the second all-color all-talking feature to be produced by Paramount Pictures. The film was based on the popular 1929 Broadway play of the same name by Frank Mandel and Laurence Schwab. The play ran from January 9, 1929 to December 21, 1929; running for 401 performances. Jack Haley and Zelma ONeal, who starred in the original musical play, reprised their roles for the film version. The film is one of dozens of musicals made in 1929 and 1930 following the advent of sound, and one of several to feature color cinematography. However, many of these films have been lost or destroyed by the original studios. Follow Thru survives in its entirety and in excellent condition. It has been preserved by the UCLA Film and Television Archive under the direction of Robert Gitt.

==Cast==
*Charles Buddy Rogers as Jerry Downes
*Nancy Carroll as Lora Moore
*Zelma ONeal as Angie Howard
*Jack Haley as Jack Martin
*Eugene Pallette as J.C. Effingham
*Thelma Todd as Mrs Van Horn Claude King as Mac Moore
*Kathryn Givney as Mrs Bascomb Margaret Lee as Babs Bascomb
*Don Tomkins as Dinty Moore
*Albert Gran as Martin Bascomb

==Production==
The film was shot in Los Angeles. The extras who appear in golf course scenes had to be coached with regards to golf etiquette (when to applaud a strike, etc). About two hundred extras were used for the climactic golf championship sequence. 

==Songs==
* "Button Up Your Overcoat"
* "You Wouldnt Fool Me, Would You?"
* "I Want to Be Bad"
* "Im Hard to Please"
* "A Peach of a Pair"
* "It Must Be You"

==Preservation==
For a long time, the film was believed to be lost, but a print was found in the 1990s and it was carefully restored and preserved by the UCLA Film and Television Archive.

==See also==
*List of early color feature films

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 