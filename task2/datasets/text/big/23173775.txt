Death on the Set
{{Infobox film
| name = Death on the Set
| image =
| image_size =
| caption =
| director = Leslie S. Hiscott
| producer = Julius Hagen 
| writer = Michael Barringer   Victor MacLure 
| narrator = Henry Kendall   Eve Gray   Jeanne Stuart    Garry Marsh
| music = W.L. Trytel Ernest Palmer
| editing = Ralph Kemplen
| studio = Twickenham Studios
| distributor = Universal Pictures
| released = 29 July 1935 
| runtime = 73 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} British mystery Henry Kendall, Eve Gray, Jeanne Stuart and Wally Patch. Its plot concerns a film director who murders a leading gangster and takes his place, later pinning the killing on a prominent actress.  It is also known by the alternative title Murder on the Set.
 distribution by Universal Pictures.

==Cast== Henry Kendall as Cayley Morden / Charlie Marsh
* Eve Gray as Laura Cane
* Jeanne Stuart as Lady Blanche 
* Garry Marsh as Inspector Burford 
* Wally Patch as Sergeant Crowther 
* Lewis Shaw as Jimmy Frayle
* Alfred Wellesley as Studio Manager 
* Ben Welden as Freshman 
* Rita Halsam as Constance Lyon 
* Robert Nainby as Lord Umbridge 
* Hal Walters as Albert
* Elizabeth Arkell as Mrs. Hipkin  

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.

==External links==
 

 

 
 
 
 
 
 
 
 
 