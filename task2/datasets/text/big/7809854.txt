The Legend of the North Wind (film)
 

{{Infobox film
| name           = The Legend of the North Wind
| director       = Carlos Varela Maite Ruiz de Austri (orig. credited dirs) Juan Bautista Berasategi (orig. uncred.)
| producer       = Iñigo Silva Mikel Arregi Santi Uriarte
| writer         = Gregorio Muro Carlos Varela Maite Ruiz de Austri Joxan Muñoz  Rick Jones Terrence Scammell
| music          = Alejandro Massó
| cinematography = Eduardo Elósegi
| editing        = José Salcedo
| released       =  
| runtime        = 69 min.
| country        = Spain Basque Spanish Spanish
}}
 Spanish animated fantasy film directed by Juan Bautista Berasategi (originally credited to Maite Ruiz de Austri and Carlos Varela). It was based upon a story by Gregorio Muro and Josean Muñoz, and produced by Episa and Euskal Pictures International.

It was produced in the Spain under the working title of Balleneros (Basque title: Balearenak), before being released in 1992 as La leyenda del viento del Norte (Spanish Language|Sp), La légende du vent du nord (French language|Fr), and Ipar aizearen erronka (Basque language|Basque).

Originally, the released film was credited to Maite Ruiz de Austri and Carlos Varela, but Berasategi sued the producers for plagiarism, charging that the majority of the film was developed under his direction, and that Ruiz de Austri and Varela had received undue credit for what was actually his work. Berasategi eventually won the case, and received legal recognition as the films director. This is reflected on more recent releases of the film, in which the original director credit is replaced with a new credit for Berasategi.
 dubbed version was not released until 1997 in North America as direct to video by Plaza Entertainment and Nelvana in 1997. 

The Spanish release was followed by a 13-episode TV series,  and a 1994 sequel called El regreso del Viento del Norte, or The Return of the North Wind.

==Synopsis== Basque sailors and Mikmaq people|Mikmaq Indians. Now, the descendant of those Indian, Watuna, and the descendants of those Basque sailors, Ane and Peiot, must defeat the evil Athanasius before he achieve his purpose.

==Voice cast==
  
Basque version (original version)
* Xabier Eguzkiza as the Narrator
* Xebe Atencia as Pello
* Asun Iturriagagoitia as Ane
* Luz Enparanza as Watuna
* Xabier Eguzkiza as Captain Galar
* Kepa Cueto as The North Wind/Athanasius
* Xabier Ponbo as Bakailu
* Aitor Larrañaga as Martin
* Tere Jaioas The Sea
 
Spanish version
* Damián Velasco
* Chelo Vivares
* Gonzalo Durán
* Vicente Gisbert
* Isabel Fernández
* María José Castro
* Ángela María Romero
* Amparo Climent
* Teófilo Martínez
* Pedro Sempson
* José Carabias
* Daniel Dicenta
* Eduardo Jover
* José María Martín
 
English version
* Sonja Ball as Ane
* Teddy Lee Dillon as Elliot
* Daniel Brochu as Watuna
* A.J. Henderson as Captain Galar Terrence Scammell as The North Wind/Athanasius Rick Jones as Barnaby
* Richard Dumont as Martin
* Kathleen Fee as The Sea
 

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 