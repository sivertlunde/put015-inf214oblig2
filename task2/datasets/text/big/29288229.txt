Everything Happens to Me (1938 film)
{{Infobox film
| name           = Everything Happens to Me
| image          =
| image_size     =
| caption        =
| director       = Roy William Neill Jerome Jackson
| writer         = John Dighton Austin Melford Max Miller Chili Bouchier H.F. Maltby
| music          =
| cinematography = Basil Emmott Leslie Norman Warner Brothers-First First National Productions
| released       = December 1938
| runtime        = 82 minutes
| country        = United Kingdom
| language       = English
}} Max Miller, Chili Bouchier and H.F. Maltby.

The film was a quota quickie production featuring Miller as a vacuum-cleaner salesman volunteering as an election agent to canvas on behalf of prospective candidate Arthur Gusty (Maltby).  However, when he learns from nurse Sally Green (Bouchier) that Gusty is a crook who has been systematically siphoning off funds from the local orphanage into his own pocket, he withdraws his support and throws himself whole-heartedly behind the campaign of Gustys honourable opponent Norman Prodder (Frederick Burtwell).

==Cast== Max Miller as Charles Cromwell
* Chili Bouchier as Sally Green
* H.F. Maltby as Arthur Gusty
* Frederick Burtwell as Norman Prodder
* Norma Varden as Mrs. Prodder
* Allan Jeayes as Bill Johnson
* Winifred Izard as Mrs. Gusty
* Hal Walters as Jack

==External links==
*  
*   at BFI Film & TV Database
*   at Max Miller information site

 

 
 
 
 
 
 
 


 