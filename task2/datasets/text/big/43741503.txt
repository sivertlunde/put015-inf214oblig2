Pluto (film)
{{Infobox film name           = Pluto image          = Pluto,_2013_film.jpg director       = Shin Su-won producer       = Shin Sang-han   Francis Lim writer         = Shin Su-won starring       = Lee David   Sung Joon   Kim Kkot-bi music          = Ryu Jae-ah cinematography = Yoon Ji-woon editing        = Lee Do-hyun studio         = SH Film distributor  Sidus FNH released       =    runtime        = 107 minutes country        = South Korea language       = Korean budget         = gross          =   film name      = {{Film name hangul         = 명왕성 rr             = Myeongwangseong mr             = }}
}}
Pluto ( ) is a 2012 South Korean film written and directed by Shin Su-won about the severity of competition among students at an elite high school, and how far one will go to be at the top.       

The film made its world premiere at the 17th Busan International Film Festival   and was also screened at the 63rd Berlin International Film Festival where director Shin Su-won received a special mention in the Generation 14plus Section.  

==Plot==
Kim Joon, a transfer student of a prestigious high school, is arrested for the murder of one of his classmates. Yoo jin, the top student of his class, was found dead in the woods behind the school and strong evidence and a group of students testimonies point to Joon, but after some questioning with the police, he is released. Upon returning to the school, he holds the group of students as prisoners in a hidden basement. The students are members of an elite group of students, composed of the top 1% of the school who have access to certain privileges that allows them to keep their ranking high. Dark secrets of the group begin to unfold with flashbacks of how Joon, an ambitious student from an ordinary high school who was desperate to become a member, was asked to perform a series of cruel tasks in order to get in.

==Cast==
*Lee David as Kim Joon
*Sung Joon as Yoo-jin Taylor
*Kim Kkot-bi as Jung Soo-jin
*Jo Sung-ha as Chief detective Park
*Kim Kwon as Han Myung-ho
*Sun Joo-ah as Kang Mi-ra
*Nam Tae-boo as Choi Bo-ram
*Ryu Kyung-soo as Park Jung-jae
*Kim Mi-jeong as Eun-joo
*Park Tae-sung as Literature teacher
*Kil Hae-yeon as Joons mother
*Oh Jung-woo as Kang Chang-min
*Hwang Jeong-min   as Myung-hos mother
*Kim Jae-rok as Principal

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Award !! Category !! Recipient !! Result
|-
| 2013 || 63rd Berlin International Film Festival || Special Mention (Youth Jury Generation 14plus) || Pluto ||  
|- 2014 || 1st Wildflower 1st Wildflower Film Awards   || Best Film || Pluto ||  
|-
| Best Director || Shin Su-won ||  
|-
| Best Actor || Lee David ||  
|-
| Best Cinematography || Yoon Ji-woon ||  
|- 50th Baeksang Arts Awards || Best New Actress || Sun Joo-ah ||  
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 