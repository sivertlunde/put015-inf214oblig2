Dreamplay
{{Infobox Film
| name           = Dreamplay
| image          = 
| caption        = 
| director       = Unni Straume Lars Jönsson Unni Straume
| screenplay     = Unni Straume
| based on       =  
| starring       = Ingvild Holm
| music          = 
| cinematography = Harald Gunnar Paalgard
| editing        = Mikael Leschilowsky
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}

Dreamplay ( ) is a 1994 Norwegian drama film directed by Unni Straume. It was screened in the Un Certain Regard section at the 1994 Cannes Film Festival.   

==Cast==
* Ingvild Holm - Agnes
* Bjørn Willberg Andersen - Lawyer
* Bjørn Sundquist - Poet
* Lars-Erik Berenett - Officer
* Liv Ullmann - Ticket Seller
* Bibi Andersson - Victoria
* Erland Josephson - Blind Man
* Joachim Calmeyer - Plakatklistreren
* Svein Scharffenberg - Bartenderen
* Mona Hofland - Moren
* Espen Skjønberg - Faren
* Merete Moen - Kristin
* Eindride Eidsvold - Brudgommen
* Camilla Strøm-Henriksen - Bruden
* Nils Ole Oftebro - Victorias elsker
* Arne Hestenes - Don Juan

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 