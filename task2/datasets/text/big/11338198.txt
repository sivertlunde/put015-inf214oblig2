The Magic Crystal
 
 
{{Infobox film
| name           = The Magic Crystal
| image          = MagicCrystal.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 魔翡翠
| simplified     = 魔翡翠
| pinyin         = Mó Fěi Cuì
| jyutping       = Mo1 Fei2 Ceoi3 }}
| director       = Wong Jing
| producer       = Wallace Chung
| writer         = 
| screenplay     = Wong Jing
| story          = 
| based on       = 
| narrator       = 
| starring       = Andy Lau Cynthia Rothrock Natalis Chan Siu Ban Ban Wong Jing Max Mok Sharla Cheung
| music          = Joseph Yip
| cinematography = Movie Impact Cinematographers Team
| editing        = Lau Siu Kwong
| studio         = Wins Entertainment Lung Cheung Films
| distributor    = Movie Impact
| released       =  
| runtime        = 95 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = HK$28,000,000
| gross          = HK$11,383,366
}} Hong Kong action film written and directed by Wong Jing. The film stars Andy Lau and Cynthia Rothrock.

It is about a mysterious crystal that holds powers to be able to do both nice and terrible things. The film is well known for being one of those so bad its good films. What saves the film from total mediocrity is its high-octane martial arts scenes. 

==Storyline== Richard Norton, Cynthia Rothrock and a very youthful Andy Lau. Norton also shows off his great skill in using weapons, much to the annoyance of both Rothrock and Lau.

==Cast==
* Andy Lau - Andy Lo
* Cynthia Rothrock - Cindy Morgan
* Natalis Chan - Lau Ta
* Siu Ban Ban - Pin-Pin
* Wong Jing - Pancho
* Max Mok - Interpol Agent
* Sharla Cheung - Winnie Shen (沈雲妮)
* Chung Fat - Triad Thug
* Hung San-Nam - Triad Thug 
* Phillip Ko - Shen
* Eddie Maher - Karovs Thug in Greece
* Tony Leung Siu-Hung -  Karovs thug in Greece  Richard Norton - Karov
* Shih Kien - Sergeant Shi
* Shing Fui-On - Prisoner
* Shum Wai - Triad Boss
* Jackson Ng - Karovs Red Head Band Thug
* Wong Wan-Si - Maureen Yu
* Albert Lai - Cop David Ho - Steves father

==See also==
*Andy Lau filmography
*Wong Jing filmography

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 

 
 