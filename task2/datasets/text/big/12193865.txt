Radio tekee murron
 
{{Infobox film
| name           = Radio tekee murron
| image          = 
| image_size     = 
| caption        = 
| director       = Matti Kassila
| producer       = Toivo Särkkä
| writer         = Matti Kassila Aarne Tarkas
| narrator       = 
| starring       = Hannes Häyrinen Ritva Arvelo Kullervo Kalske Kunto Karapää Kauko Käyhkö Kaarlo Halttunen
| music          = Ahti Sonninen
| cinematography = Osmo Harkimo Pentti Valkeala	
| editing        = Armas Vallasvuo
| distributor    =  	 
| released       =  
| runtime        = 
| country        = Finland
| language       = Finnish
| budget         = 
| gross          = 
}}
Radio tekee murron ("The Radio Burglary") (1951) is a Finnish crime comedy directed by Matti Kassila and starring Hannes Häyrinen. The idea for the movie came from an actual radio program done by sensationalist reporter Usko Santavuori, in which he committed a fake burglary of which local police forces had not been made aware, with the exception of the commander.

==Plot==
Toivo Teräsvuori (Häyrinen) is a young radio journalist whos looking for new, interesting topics on which to report. After covering a skydive on a live broadcast, he gets the idea to stage a fake burglary for his next daring stunt. He shares this idea with his friend and fellow radio employee Laakso, whom he persuades to go along with the plan without notifying either their superiors in the radio station, nor the police.

Unfortunately, Teräsvuori and Lahti happen to discuss the details for their fake crime, a burglary into the Helsinki Art Museum, in a cafe within earshot of some actual burglars, who plan their own heist to coincide with Teräsvuoris. Both crimes, real and fake, go forward as planned, and Teräsvuori is captured inside the museum, only to learn to his great surprise that an actual theft took place and that hes being blamed for it. Unable to convince the police of his story, he instead escapes and tries to prove his innocence.

==Cast==
*Hannes Häyrinen as Toivo Teräsvuori
*Ritva Arvelo as Eila Ritolampi
*Kullervo Kalske as Inspector Tammisalo
*Kunto Karapää, Mr. Knickerbocker a.k.a. Robins
*Kauko Käyhkö, Durando a.k.a. Lindström
*Kaarlo Halttunen as Leo Laakso, sound engineer
*Uljas Kandolin as Rytky

==Reception==
In the 1951 Jussi Awards, Radio tekee murron received three awards. Director Matti Kassila won the award for Best Director, and shared the award for Best Screenplay with co-writer Aarne Tarkas. In addition, Hannes Häyrinen won a Jussi for Best Lead Actor.   

==References==
 

==External links==
* 

 
 
 
 
 
 