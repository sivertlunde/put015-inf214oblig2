The Big Year
{{Infobox film
| name           = The Big Year
| image          = The Big Year Poster.jpg
| caption      = Teaser poster
| director       = David Frankel   
| producer       = Karen Rosenfelt Stuart Cornfeld Curtis Hanson
| writer         = Howard Franklin
| based on       =  
| starring       = Steve Martin Jack Black Owen Wilson Theodore Shapiro
| cinematography = Lawrence Sher
| editing        = Mark Livolsi Red Hour Deuce Three Ingenious Media Sunswept Entertainment Dune Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $41 million 
| gross          = $7,448,385 

}}

The Big Year is a 2011   - a competition among birders to see who can see and identify the greatest number of species of birds in North America (north of Mexico) in a calendar year. The film uses the same premise with fictional characters.

The film was released on October 14, 2011, in the United States.    Filming took place from May to July 2010.    It was released in the United Kingdom on November 14, 2011.   

==Plot==
 

The film follows three amateur birders who each set out to achieve a Big Year. They are Brad Harris (Jack Black), a 36-year-old computer programmer based in Baltimore; Stu Preissler (Steve Martin), founder and CEO of a New York company bearing his name; and a roofing contractor named Kenny Bostick (Owen Wilson), who holds the current Big Year record of 732 birds.

Bostick is obsessively possessive of his record, but his second wife Jessica (Rosamund Pike) is concerned; this was supposed to be the year they focused on conceiving a child. She also believes that Bosticks birding obsession is what destroyed his previous marriage.   

Brad is a skilled birder who can identify nearly any species solely by sound. He hates his job maintaining the operational software of a Pennsylvania nuclear power plant. Living with his parents after a failed marriage, an aborted career at Dell, and dropping out of grad school, he is a "typical Jack Black underachiever".  He hopes that doing a Big Year will give him a sense of purpose and possibly even make his father proud of him. 

Stu is the founder and CEO of an enormous Manhattan-based chemical conglomerate which he built from the ground up, starting in his garage. After decades of corporate success, he is ready to retire to Colorado with his architect wife. Fear of an empty schedule led him to come back from a previous retirement, but now he wants to leave his company in the hands of his two lieutenants (Kevin Pollak and Joel McHale). The company is in the middle of complicated negotiations to merge with a competitor, so his two anointed successors keep calling him back to New York for important meetings; to some extent he is a "prisoner of his own success".  A Big Year has been his lifelong dream and hes pursuing it with the full support of his wife.

At his parents house over dinner, Brad reveals to his father (Brian Dennehy) that he has begun his Big Year and the travel costs necessary to undertake his dream have already been budgeted. His father objects, but Brad is resolute, encouraged by his mother (Dianne Wiest).

Stu flies off to British Columbia after receiving notification of a Xantuss hummingbird sighting in a backyard there. His former company colleagues  try unsuccessfully to convince him to return to New York City. Unbeknownst to Stu, Bostick arrives at the same house in British Columbia and sees the hummingbird. Bostick again comes into contact with Stu when all three men are aboard a birding boat. Aware he may have a fellow birder on his tail, Bostick tries to worsen Stus seasickness. Brad befriends a nauseated Stu and also meets for the first time fellow birder Ellie (Rashida Jones).

Bosticks wife attempts to gain her husbands attention back on the home front, informing him she has decided to hire a rival contractor to begin work on remodeling their home. Brad is feeling the financial pressure of balancing the need for constant exotic travel with his work schedule and limited budget. Meanwhile, the merger of Stus company continues to hit snags, causing his former lieutenants to pressure Stu to take an active role in the increasingly acrimonious negotiations.

A freak storm in the Gulf Coast creates a rare "fallout" event, driving exotic birds away from their established migratory routes and forcing them to take refuge on the one patch of land available. Its such an invaluable opportunity to spot dozens of rare, non-native species that Stu, Brad and Bostick all immediately drop what theyre doing to bird the island, despite the effects this will have on the merger deal, his job, and his marriage, respectively. Hundreds of birders flock to the area, including Ellie, much to Brads delight. After spending the day birding together, Brad and Stu dine together and bond further, forming the seeds of a close friendship. Brad discloses he is currently doing a Big Year; Stu does not. Hes still smarting from Bosticks interference earlier in the year, and fears what would happen if too many people learned of his own pursuit and word got back to Bostick.

The negotiations for the merger of Stus company have collapsed completely and the only hope is a do-or-die session scheduled to take place just before Stus birding expedition to Attu Island. Stus former lieutenants persuade him to come to the meeting,

A friend of Brads (Jim Parsons) who writes a prominent birding blog happens to discover Stus growing list. He posts a picture of Stu, along with his Big Year total of 497 species to date. Feeling hurt that Stu did not tell him, Brad divulges Stus goal to Bostick while the two are aboard a plane on their way to Attu Island. Stu was booked on the same chartered flight but narrowly missed it, held up by his successful closing of the merger deal. Stus disappointment only worsens as he watches local news anchormen stating that a storm created prime birding conditions on Attu. Stu arrives days later and makes amends with Brad, wanting to remain friends. He encourages Brad in his pursuit of Ellie, who also made the trip.

When they all are back home, Ellie calls Brad to inform him a pink-footed goose has been spotted in Boston. Any thoughts he had of pursuing her are dashed, however, when she and her boyfriend pick him up from the train station.

Stu and Brad meet up again while awaiting a ferry to an island where a  s.

Upon landing, Brad is notified that his father has suffered a heart attack; he returns home to be with him. His father comes to affectionately view his son with a newfound respect after hearing Brad explain his love for his favorite bird, the American golden plover, and comes to understand the significance of his Big Year attempt. He accompanies Brad into the snowy woods and helps him locate a great grey owl.

All three birders are coming to understand the cost of their birding obsessions. Brad leaves his sick father behind on the trail when he became short of breath during their pursuit of the great grey owl; cursing himself, he rushes back to find that all is well. Stu finds that hes regretting the time hes spending away from his wife and his new grandson, born during the Big Year and named "Stu" in his honor.

Bostick races home from yet another birding trip to keep an important appointment with his wife at a fertility clinic. He is literally at the front door of the clinic when he receives a report of a sighting of a snowy owl, his most coveted and elusive bird. Despite the fact that his wife is waiting inside to have her eggs harvested, fertilized and implanted after undergoing months of hormone injections, he speeds back to the airport and phones in an obviously made-up excuse for his absence. His wife returns alone to their big house, and screams in frustration inside the empty nursery. When Bostick finally returns home after the (fruitless) search for the snowy owl, she tells him she still loves him but cant be his wife anymore.

As the year draws to a close, Stu enjoys his newborn grandson; Brad gets a phone call from Ellie saying she and her boyfriend have broken up; and Bostick dines alone in a Chinese restaurant on Christmas Eve, still seeking a snowy owl.

When Stu is offered the chairmanship of 3M, the parent company that bought Preissler Chemical, he realizes that his fears of retirement are gone and he easily turns down the opportunity to become one of the worlds most powerful CEOs. Brad and Stu close out their Big Year together near Stus home in Colorado by finally sighting a Norwegian species that theyd just missed spotting during the fallout. Now close friends, they congratulate each other on "a very Big Year indeed."

The Big Year results are published and Stu phones Brad with the news. Bostick is first with 755, a new record; Brad came in second; Stu was fourth. Brad opines that "he got more birds, but we got more everything," as he looks at Ellie, who has come for a weekend visit. Stu smiles, looking at his wife.

The film ends with Brad and Ellie cozily birding together on a rocky coastline, while Brad confesses that birding is no longer the biggest part of his life. Stu, contented in retirement, is hiking with his toddler grandson (already enamored by birds) in the Rocky Mountains|Rockies. And Bostick is on a birding adventure in China, alone and gazing wistfully at a happy couple walking with their newborn child.

==Cast==
 

*Steve Martin as Stu Preissler
*Jack Black as Brad Harris
*Owen Wilson as Kenny Bostick
*Rashida Jones as Ellie 
*Anjelica Huston as Annie Auklet
*Jim Parsons as Crane
*Rosamund Pike as Jessica Bostick
*JoBeth Williams as Edith Preissler
*Brian Dennehy as Raymond Harris
*Dianne Wiest as Brenda Harris
*Anthony Anderson as Bill Clemens
*Tim Blake Nelson as Phil
*Joel McHale as Barry Loomis
*Calum Worthy as Colin Debs
*Veena Sood as Nurse Katie
*Corbin Bernsen as Gil Gordon
*Stacey Scowley as Vicki
*Jesse Moss as Jack Lusas
*Kevin Pollak as Jim Gittelson
*Barry Shabaka Henley as Dr. Neil Kramer Andrew Wilson as Mike Shin
*Al Roker as New York Weatherman
*John Cleese as Historical Montage Narrator
*June Squibb as The Old Lady  Steven Weber as Rick McIntire
 

==Production==
Principal photography was done from May 3 to July 30, 2010 in Vancouver.  Jack Blacks fall on Attu Island was unscripted.  

==Reception==
The film received mixed reviews from critics. The   polls reported that the average grade moviegoers gave the film was a "B-minus" on an A+ to F scale. 

The film was a box office failure, despite the established stars like Martin, Black and Wilson as the leads. Based on a budget of $41 million, it took in just $7.4 million in ticket sales worldwide according to Box Office Mojo.

==Songs==
{| class="wikitable"
|-
! Song !!  Writer !! Performer
|- Minor Swing || Stéphane Grappelli and Jean Reinhardt || Django Reinhardt 
|-
| (If I Had) A Sandwich With You || Dan DiPrima and Alex Marlowe || Zombie Bank
|- Wheel of Fortune Underscore || courtesy of Sony Pictures ||
|-
| The Devil Never Sleeps || Sam Beam || Iron & Wine
|-
| Pitkin County Turnaround || Steve Martin || Steve Martin
|- Let It Shine || Jeremy Fisher || Jeremy Fisher
|-
| Ill Have the Halibut || Dan DiPrima and Alex Marlowe || Zombie Bank
|-
| Away With Pie || Dan DiPrima and Alex Marlowe || Zombie Bank
|-
| The Dogs Decree - Concerto in C Major || Antonio Vivaldi || Alexandre Desplat
|- William Champion, Christopher Martin, Guy Berryman, & Jonathan Buckland || Coldplay
|-
| Come Fly Away || Jeremy Fisher and Jack Livesey || Jeremy Fisher
|-
| Surfin Bird || Alfred Frazier, John Harris, Turner Wilson Jr., and Carl White || The Trashmen
|- Blackbird || John Lennon and Paul McCartney || Brad Mehldau
|- E || Eels
|-
| Adeste Fideles || Folk Music|Traditional, Arranged by Virginia S. Davidson ||  New York Treble Singers
|-
| Silent Night || Traditional || Bing Crosby
|-
| This Could All Be Yours || Ryan Miller, Adam Gardner, Brian Rosenworcel and Joe Pisapia || Guster
|-
| Auld Lang Syne || Traditional, Arranged by Guy Lombardo || Guy Lombardo and His Royal Canadians
|}

 Soundtrack references:  

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 