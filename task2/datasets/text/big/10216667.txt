I Want Candy (film)
{{Infobox film
| name = I Want Candy
| image = I want candy.jpg
| caption = Promotional poster
| director = Stephen Surjik
| producer = Barnaby Thompson Piers Thompson Fred Wolf Tom Riley Tom Burke Carmen Electra
| music = Melanie C Sugardaddy Art Brut
| cinematography = Crighton Bone
| editing = Alex Mackie
| studio = Ealing Studios
| distributor = Buena Vista International
| released =  
| runtime = 87 minutes
| country = United Kingdom
| language = English
| budget =$1,500,000
| gross = $2,415,513 
}}
I Want Candy is a 2007 British comedy film from Ealing Studios.

==Plot==
A group of film students at Leatherhead University in search of funding for their feature film "The Love Storm" end up having to rewrite and make it into a pornographic film|porno. This leads the boys head first into a world of erotica that they didnt even know existed and into the life of actress Candy Fiveways (Carmen Electra).

==Cast== Tom Riley as Joe Tom Burke as Baggy
* Carmen Electra as Candy Fiveways
* Eddie Marsan as Doug
* Michelle Ryan as Lila
* Mackenzie Crook as Dulberg
* Felicity Montagu as Mum Philip Jackson as Dad
* Jimmy Carr as Video Store Guy
* John Standing as Michael de Vere

==Background and reviews==
It was shot on a low budget in west London, but was picked up by Buena Vista International for wide national release.

"I Want Candy" is also the title of a song written and originally recorded by The Strangeloves in 1965, and covered by Melanie C for the film.

The film received mixed reviews, but praise from Channel 4,  Time Out  and Empire Magazine. 

==Box office performance==
The movie entered the UK Top 10 at #7,  and moved to No.11 the following week. 

==Locations==
Scenes in the fictional Leatherhead University were filmed on the campus of Brooklands College in Weybridge, Surrey. 

==DVD release==
I Want Candy was released as a DVD on August 20, 2007. It was rated 15. The special feature it included on the DVD were deleted scenes, bloopers, and The Making Of.... It also included scenes which were from the official website like "Joe and Baggys 10 tips to making a film" and "Whats your porn star name?."

==References==
 

==External links==
 
 
*  
*  
*  
 

 

 
 
 
 
 
 
 
 
 