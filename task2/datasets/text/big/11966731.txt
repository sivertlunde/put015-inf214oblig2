When Andrew Came Home
  
{{Infobox film
| name           = When Andrew Came Home
| image          = When Andrew Came Home cover.jpg
| image_size     =
| producer       = Michael Filerman
| director       = Artie Mandelberg
| writer         = Susan Rice
| distributor    = Lifetime Television
| starring       = Park Overall   Seth Adkins   Jason Beghe   Evan Laszlo
| released       =   30 October 2000
| runtime        = 88 minutes English
| Jay Ferguson
| cinematography = Malcolm Cross
| editing        = Lance Luckey
|}} 2000 American drama television film, based on a true story and directed by Artie Mandelberg.

== Plot == Fourth of maternal instinct, Gail gives her son to her ex-husband, who promises to return him on time as he always does.

When Andrew is late returning, Gail tells police that her ex-husband kidnapped her child. The police fail to file a report. Meanwhile, Ted is driving a sleeping Andrew away from his mothers house. In the morning, they are stopped by the police for speeding right before they cross the state border. Since no report is filed, Ted and his girlfriend are not stopped from kidnapping Andrew.
 phone booth and to not call the cops. After being given the runaround by Ted Gail yells at her ex-husband and asks him if Andrew is coming home. He tells her that Andrew will travel to the bus station.

Despite losing all hope of finding her son and thinking that Ted lied to her, Gail still goes to the bus station. After a bus arrives and Andrew does not show, Gail is about to leave when another bus arrives and Andrew gets off. He is dirty and has no luggage. Gail runs to greet her son, but gets no response from him. She places her son in the back seat of her SUV and sits beside him, but Andrew prefers to sit on the floor curled up. Gail buckles Andrew in, and the three go home.

Gail and Eddie show Andrew his room. Andrew pushes the presents off the bed and begins to jump it. He makes grunting noises of delight, but still has not said a word. His grandmother walks to the room with EJ and Andrew grunts and holds out his arms, wanting to hold the baby. His grandmother places the baby on the bed and Andrew cuddles with his Sibling|half-brother.
 saying grace, Andrew starts eating the spaghetti with his hands. Gail brushes this off, saying that Andrew must be hungry. Later that night, Andrew is unresponsive when she sings their nightly song. He turns away from Gail when she tries to kiss him and screams when she turns out the lights. After Gail turns on a nightlight and leaves the room, Andrew goes into a fetal position and goes to the corner of his room.
 special residential school to help him catch up in his academics. Gail refuses to part with her son once again, so Andrew goes public school so he can socialize with the other children.

Whilst at the school a bully named Carl Rudnick makes fun of Andrew because he only stands on the side during recess and does not talk to the other children. During a game of baseball when another child hits the baseball in Andrews direction, he just stands there and lets the ball fall to the ground and roll away. This makes Carl furious and he yells at Andrew, who responds by urinating on Carl.

The principal calls both Gail and Carls mother. Mrs. Rudnick calls Andrew a freak and says that he should not be in public school, even yelling at Andrew after he apologizes. After Carl and his mother leave, Gail has a discussion with the principal. He wants to hold Andrew back, but she refuses and takes him out of the school.

Andrew also acts strangely at home. While driving one day, Andrew screams after seeing a car that resembles his fathers car and still does not talk to his mother, not trusting her. On his tenth birthday, Andrew sticks his face into his birthday cake for no reason at all, and later he speaks for the first time, telling his mother that he doesnt know why he did it and that he thinks his mother is trying to trick him. One night before dinner, hes playing with a glass of milk and it accidentally falls off the table, almost injuring EJ on the floor. Andrews behavior angers Eddie and he begins to fear for his own son. After Andrew accidentally sets the house on fire, Eddie takes the baby and they go to live with his mother for a few months.

Gail begins to teach Andrew at home. Slowly but surely, Andrew makes progress, and things seem to get better for the family, with Eddie and EJ even moving back into the house. However, a woman with Social Services tells Gail that this is not good enough and if Andrew isnt on grade level by the time the summer ends, then she will be forced to send him to the special school. In order to help Andrew, Gail takes him to her brothers farm so he can learn in a new atmosphere where he has no history.

Andrew shows how intelligent he is by being able to learn how to milk a cow and how to care for the animals very quickly, but is unable to subtract 2 from 7. Gail cries to her brother saying that she loves Andrew and thanks God every day for his safe return, but misses her old son, adding that if she had a choice to pick her son, it wouldnt have been Andrew.

Having overheard this conversation, Andrew runs out of the house. Gail runs after him and eventually finds him in the stables. She says she didnt mean what she said, but Andrew doesnt believe her. His father told him that she did not want him anymore and that she didnt love him. Gail explains that that isnt true, that she looked for him every day and missed him every minute during the five years he was gone. She told him she loves him and he will always be her son. After crying, Andrew falls asleep in the stables and Gail watches over her son.

He wakes up startled and in fear when Gail tries to place a blanket over him for warmth. She tells him he doesnt have to be afraid, and then Andrew finally opens up and tells his mother about his experiences with his father.  They never stayed long at any place and he was not allowed outside during the day. The last place they stayed the longest was a rundown trailer.  He was usually locked in a room all day by himself, watching TV. Both adults were abusive; Pattie would hit him, and Ted would lock him in a dark closet. At night, they put him outside, tied to a long leash, where he would sleep in the trees in the dark with the frightening sounds of the nocturnal animals surrounding him. Andrew was not allowed to go to school or socialize with other children his age, or anyone other than Ted and Pattie for that matter. He told his mother how he would think about her and home, but then get angry that she didnt love or want him. Then he explains that now he knows she did love him and want him. He then tells his mother that he is home, and the two cry in each others arms and sing their bedtime song together.

In the end, it is revealed that Gail and Andrew returned home to Eddie and EJ to build a healthier life together and that Ted was tried for kidnapping and abusing Andrew, found guilty, and was imprisoned.

== Cast and crew ==
=== Cast ===
* Park Overall – Gail
* Jason Beghe – Eddie
* Seth Adkins – Andrew
* Lynne Deragon – Joanne
* Craig Eldridge – Jack Shannon Lawson – Deena Drake
* Carl Marotte – Ted
* Patrick Chilvers – Officer Reston Jeff Clarke – Mr. Kemper
* Stan Coles – Dr. Burton
* Eve Crawford – Janet Hilgarde
* Jean Daigle – Officer Pearl
* Shane Daly – Highway Cop
* Jake Goldsbie – Carl Rudnick
* Bruce Gray – Dr. Matthews
* Katie Lai – Little Girl
* Bill Lake – Desk Sergeant
* Evan Laszlo – Young Andrew
* Joanne Reece – Officer Warner
* K Roberts – Margaret Granger
* Rhona Shekter – Mrs. Rudnick
* Jared Wall – Kid in Playground
* Patricia Zentilli – Pattie
* Hugo Hardinge – EJ
* Oliver Hardinge – EJ

== Awards ==
* Seth Adkins was nominated for a Young Artist Award (2001) Best Performance in a TV Movie (Drama) – Leading Young Actor   (accessed June 26, 2007) 

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 