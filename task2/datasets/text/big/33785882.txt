Star Trek Into Darkness
  
{{Infobox film
| name = Star Trek Into Darkness
| image = StarTrekIntoDarkness FinalUSPoster.jpg
| size =
| alt = The poster shows a flaming starship falling toward Earth, with smoke coming out of it. The middle of the poster shows the title "Star Trek Into Darkness" in dark gray letters, while the production credits and the release date are shown at the bottom of the poster.
| caption = North American release poster with the original U.S. release date
| director = J. J. Abrams
| producer = {{Plainlist|
* J. J. Abrams
* Bryan Burk
* Damon Lindelof
* Alex Kurtzman
* Roberto Orci
}}
| writer   = {{Plainlist|
* Roberto Orci
* Alex Kurtzman
* Damon Lindelof
}}
| based on =  
| starring = John Cho Benedict Cumberbatch Alice Eve Bruce Greenwood Simon Pegg Chris Pine Zachary Quinto Zoe Saldana Karl Urban Peter Weller Anton Yelchin
| music = Michael Giacchino Dan Mindel
| editing = {{Plainlist|
* Maryann Brandon
* Mary Jo Markey
}}
| studio = {{Plainlist|
* Bad Robot Productions
* Skydance Productions
}}
| distributor = Paramount Pictures
| released =     
| runtime = 133 minutes  
| country = United States
| language = English
| budget = $185&nbsp;million   
| gross = $467.4 million    
}} science fiction Star Trek series of the same name created by Gene Roddenberry. Lindelof, Orci, Kurtzman, and Abrams are also producers, with Bryan Burk. Chris Pine reprises his role as Captain James T. Kirk, with Zachary Quinto, Simon Pegg, Zoe Saldana, Karl Urban, John Cho,  Leonard Nimoy, Anton Yelchin, and Bruce Greenwood reprising their roles from the previous film. Benedict Cumberbatch, Peter Weller, and Alice Eve round out the films principal cast. The film was the last time Nimoy would portray the character of Spock, and his final film appearance before his death in 2015.
 USS Enterprise homeworld seeking John Harrison. After the release of Star Trek, Abrams, Burk, Lindelof, Kurtzman, and Orci agreed to produce its sequel. Filming began in January 2012. Into Darkness s visual effects were primarily created by Industrial Light & Magic.
 3D in post-production. Star Trek Into Darkness premiered at Event Cinemas in Sydney, Australia, on April 23, 2013,  and was released on May 9 in Australia, New Zealand, the United Kingdom, Europe, and Peru,  with other countries following. The film was released on May 16 in the United States and Canada, opening at IMAX cinemas a day earlier.  

Into Darkness was a critical success, and its gross earnings of over $467 million worldwide made it the highest-grossing entry in the Star Trek franchise.

==Plot==
  USS Enterprise Christopher Pike is reinstated as its commanding officer with Kirk demoted to first officer and the rank of Commander. For his part in the incident, Spock is reassigned to another ship. An officers meeting is called to discuss the bombing of a Section 31 installation in London perpetrated by the renegade Starfleet operative John Harrison (Cumberbatch). Harrison attacks the meeting in a jumpship, killing Pike. Kirk disables the jumpship, but Harrison uses a portable transporter to escape to Qonos|Kronos, the homeworld of hostile aliens known as Klingons.

Admiral Alexander Marcus (Weller) returns the command of the ship to Kirk, promoting him again to the rank of Captain, and orders the Enterprise to kill Harrison, arming it with 72 prototype photon torpedoes, shielded and untraceable to Klingon sensors. Angry at the untested and experimental weapons being placed on board the ship, Montgomery Scott resigns his commission during an argument with Kirk and returns to Earth, Chekov is assigned to take his place. En route to Kronos, the Enterprise suffers an unexpected coolant leak, disabling the ships warp capabilities. Kirk leads a team with Spock and Uhura onto the planet, where they are ambushed by Klingon patrols. Harrison dispatches the Klingons, then surrenders after learning of the number of torpedoes locked on his location.
 suspended animation to develop advanced weapons for war against the Klingon Empire. Khan reveals that Marcus had sabotaged the Enterprise s warp drive, intending for the Klingons to destroy the ship after it fired on Kronos, giving him an apparent moral justification to go to war with the Klingon Empire. Khan also reveals coordinates for a new weapon being built near Jupiter. Kirk contacts Scotty and admits that Scottys instincts were right about the torpedoes and asks him to check out the information Khan provided.

The Enterprise is intercepted by a larger Federation warship, the USS Vengeance, commanded by Marcus. Marcus demands that Kirk deliver Khan, but the Enterprise, with a hastily repaired warp drive, flees to Earth to expose Marcus. After the Vengeance intercepts and disables his ship, Kirk reveals Carolss presence aboard the Enterprise and offers to exchange her, Khan and the cryogenic pods for the lives of his crew. Marcus forcibly transports Carol to the Vengeance and orders the Enterprise s destruction. The Vengeance suddenly loses power, having been sabotaged by Scotty, who infiltrated the ship after following the coordinates relayed by Khan through Kirk. With transporters down, Kirk and Khan, with the latters knowledge of the warships design, space-jump to the Vengeance. Spock contacts his older self on New Vulcan, who warns him that Khan is ruthless and untrustworthy, and that in another reality, Khan was only defeated at a terrible cost. Meanwhile, after capturing the bridge, Khan overpowers Kirk, Scott, and Carol, kills Marcus and seizes control of the Vengeance.

Khan demands that Spock return his crew in exchange for the Enterprise officers. Spock complies but surreptitiously removes Khans frozen crew and arms the warheads. Khan beams Kirk, Scott, and Carol back aboard the Enterprise, but then betrays their agreement by critically damaging the Enterprise; however, the Vengeance is disabled when the torpedoes detonate. With both starships caught in Earths gravity, they plummet toward the surface. Kirk enters the radioactive reactor chamber to realign the warp core, saving the ship and dies with Spock at his side.

Khan crashes the Vengeance into San Francisco in an attempt to destroy Starfleet headquarters as revenge against his former masters. Khan survives the crash and jumps out of the bridge as Spock transports down in pursuit. McCoy discovers that Khans blood has regenerative properties that may save Kirk. With Uhuras help, Spock subdues Khan, and Kirk is revived.

Nearly one year later, Kirk speaks at the re-dedication ceremony of the Enterprise and recalls the sacrifices made by the victims of Marcuss machinations where he recites the "where no man has gone before" monologue. Khan is sealed in his cryogenic pod and stored with his compatriots while Carol joins the crew of a recommissioned Enterprise as it departs on a five-year exploratory mission.

==Cast== starship Enterprise
* Zachary Quinto as Commander Spock, first officer and science officer
* Benedict Cumberbatch as Khan Noonien Singh|Khan, a genetically engineered superhuman given the alias of Commander John Harrison Montgomery "Scotty" Scott, second officer and chief engineer
* Karl Urban as Lieutenant Commander Leonard McCoy|Dr. Leonard "Bones" McCoy, chief medical officer Nyota Uhura, communications officer
* Alice Eve as Lieutenant Carol Marcus (Star Trek)|Dr. Carol Marcus,  a science officer who uses the pseudonym of "Carol Wallace" to board the Enterprise
* John Cho as Lieutenant Hikaru Sulu, third officer and helmsman
* Peter Weller as Fleet Admiral Alexander Marcus, Carols father and commander-in-chief of Starfleet
* Anton Yelchin as Ensign Pavel Chekov, navigator and Scotts temporary replacement as chief engineer Christopher Pike, Kirks mentor and predecessor as captain of the Enterprise 

===  Supporting cast and cameos ===
*   of Spock.  This was Nimoys final film appearance and portrayal as Spock before he died.
* Noel Clarke as Thomas Harewood, a Starfleet officer working in Section 31, who is extorted by Khan into bombing the facility
* Nazneen Contractor as Rima Harewood, Thomass wife
* Christopher Doohan (son of James Doohan) as Transporter officer Amanda Foreman as Ensign Brackett 
* Jay Scully as Lieutenant Chapin Jonathan Dixon as Ensign Froman
* Aisha Hinds as Navigation Officer Darwin 
* Joseph Gatt as Science Officer 0718
* Bill Hader as the computer of the USS Vengeance (voice)
* Deep Roy as Keenser 
* Sean Blakemore as a Klingon
* Anjini Taneja Azhar as Lucille Harewood, Thomass terminally ill daughter  
* Nolan North as a Vengeance helmsman  
* Heather Langenkamp   as Moto
*Chris Hemsworth and Jennifer Morrison as George Kirk, Sr. and Winona Kirk (voices)

==Production==

===Development===
In June 2008, it was reported that Paramount Pictures was interested in signing producers of the 2009 Star Trek J. J. Abrams, Bryan Burk, Damon Lindelof, Alex Kurtzman, and Roberto Orci for a sequel.  In March 2009, it was reported that these five producers had agreed to produce the film, with a script again written by Orci and Kurtzman (with the addition of Lindelof). A preliminary script was said to be completed by Christmas 2009 for a 2011 release.   Kurtzman and Orci began writing the script in June 2009, originally intending to split the film into two parts.    Leonard Nimoy, the original Spock who plays an older version of the character in the 2009 film, said he would not appear in the film.    Abrams was reportedly considering William Shatner for the sequel.   
 The Dark Knight.   
 Roddenberry created is so vast that its hard to say one particular thing stands out". They also discussed the possibility of Khan Noonien Singh and Klingons.    Kurtzman and Lindelof said they had "broken" the story (created an outline); instead of a sequel, it will be a stand-alone film.    Abrams admitted in December 2010 that there was still no script.   

 n premiere in April 2013 (left to right: Karl Urban, Zachary Quinto, director J. J. Abrams and Chris Pine)]] Jack Ryan film meant that Chris Pine would film the Star Trek sequel first.    By April, Orci said at WonderCon that the scripts first draft had been completed.    Abrams told MTV that when he finished his film, Super 8 (film)|Super&nbsp;8, he would turn his full attention to the Trek sequel.   

Although a script was completed, uncertainty regarding the extent of Abramss involvement led to the films being pushed back six months from its scheduled June 2012 release.    In June Abrams confirmed that his next project would be the sequel, noting that he would rather the film be good than ready by its scheduled release date.  Simon Pegg, who played Montgomery Scott|Scotty, said in an interview that he thought filming would begin during the latter part of the year.    Abrams stated he would prioritize the films story and characters over an early release date.    In September Abrams agreed to direct the film, with the cast from the previous film reprising their respective roles for a winter 2012 or summer 2013 release.    In October Orci said that location scouting was underway, and a comic book series (of which Orci would be creative director) would "foreshadow" the film.    Into Darkness was given a revised release date of 2013,    and Michael Giacchino confirmed that he would return to write the score.   

Lindelof said that Khan was considered a character they needed to use at some point, given that "he has such an intense gravity in the Trek universe, we likely would have expended more energy NOT putting him in this movie than the other way around." References to   were eventually added to the script, but Lindelof, Orci, and Kurtzman "were ever wary of the line between reimagined homage and direct ripoff."    Orci and Kurtzman said they wanted a film which would work on its own and as a sequel, not using ideas from previous Star Trek works simply "because you think people are going to love it". Orci noted that when trying to create the "gigantic imagery" required by a summer blockbuster, Kurtzman suggested a scene where the Enterprise rose from the ocean. With that as a starting point they (and Lindelof) came up with the cold open in Nibiru, which blended action and comedy and was isolated from the main story in an homage to Raiders of the Lost Ark. 

Actor Benicio del Toro had reportedly been sought as the villain, and had met with Abrams to discuss the role;    however, he later bowed out. In 2011, Alice Eve and Peter Weller agreed to roles.       Doctor Who actor Noel Clarke agreed to an unknown role, reported to be "a family man with a wife and young daughter".  Demián Bichir auditioned for the villain role, but as reported by Variety (magazine)|Variety on January 4, 2012, Benedict Cumberbatch was cast.   

===Filming=== Dan Mindel anamorphic 35mm film and 15 perforation IMAX cameras.     About 30 minutes of the film is shot in the IMAX format,  while some other scenes were also shot on 8 perforation 70mm|65mm.  Into Darkness was released in 3D. On February 24, 2012, images from the set surfaced of Benedict Cumberbatchs character in a fight with Spock.     Edgar Wright directed one shot in the film. 
Production ended in May 2012. 
 Garden Grove and the Greystone Mansion in Beverly Hills. Some shots were made in Iceland.         
 editing and so new dialogue was constructed and dubbed during post production. 

===Title===
On September 10, 2012, Paramount confirmed the films title as Star Trek Into Darkness.        J. J. Abrams had indicated that unlike some of the earlier films in the franchise, his second Star Trek would not include a number in its title.    This decision was made to avoid repeating the sequel numbering which began with  , or making a confusing jump from Star Trek  to Star Trek 12.  Lindelof addressed the teams struggle to agree on a title: "There have been more conversations about what were going to call it than went into actually shooting it... Theres no word that comes after the colon after Star Trek thats cool. Not that Star Trek: Insurrection or First Contact arent good titles, its just that everything that people are turned off about when it comes to Trek is represented by the colon".    Of the titles proposed, he joked that he preferred Star Trek: Transformers 4 best because the title is "technically available". 

===Music===
  incidental music. Star Trek. The film score was recorded at the Sony Scoring Stage in Culver City, California from March 5 to April 3, 2013.  Its soundtrack album was released digitally on May 14, 2013, and was made available on May 28 through Varèse Sarabande.  The score contains the original Theme from Star Trek|Star Trek theme by Alexander Courage.
 Robert Conley co-wrote a track with Penelope Austin, "The Dark Collide". 

An expanded soundtrack album was released on July 28, 2014, limited to 6,000 copies. 

==Themes==
On May 10, 2013, Cho, Pegg, and Eve were interviewed on  , and Pegg obliged: "I think its a very current film, and it reflects certain things that are going on in our own heads at the moment; this idea that our enemy might be walking among us, not necessarily on the other side of an ocean, you know. John Harrison, Benedict Cumberbatchs character, is ambiguous, you know? We   dont know who to support. Sometimes, Kirk, he seems to be acting in exactly the same way as him  . Theyre both motivated by revenge. And the Into Darkness in the title is less an idea of this new trend of po-faced, kind of, everythings-got-to-be-a-bit-dour treatments of essentially childish stories. Its more about Kirks indecision." Cho agreed about the characterization of Captain Kirk: "Its his crisis of leadership." 

Kurtzman and Orci defined the main theme of Into Darkness as "how far will we go to exact vengeance and justice on an enemy that scares us. How far should we go from our values?" They added that running from personal values is a personal struggle, where "the enemy’s blood is within us; we are the enemy. We must not succumb to it; we are the same."   

==Distribution== Andy Nelson Matthew Wood.   The film was released on May 9, 2013 in international markets and May 16, 2013 in the United States.

===Marketing=== Super 8 (2011), the prize for answering a series of questions would be walk-on roles for two people in Into Darkness.    He debuted three frames of the film on Conan (talk show)|Conan on October 4, 2012, showing what he described as Spock "in a volcano, in this crazy suit".    The official poster for the film was released two months later on December 3, 2012, showing a mysterious figure (thought to be Benedict Cumberbatchs villain) standing on a pile of burning rubble looking over what appears to be a damaged London;   he is standing in a hole in the shape of the Starfleet insignia, blown out of the side of a building. 

About nine minutes of the opening sequence was shown before  , which was released in the United States on December 14, 2012.    Alice Eve, Cumberbatch and Burk unveiled the IMAX prologue in London, England on December 14. A two-minute teaser was released in iTunes Movie Trailers on December 17. The teaser marked the beginning of a viral marketing campaign, with a hidden link directing fans to a movie-related website. A 30-second teaser premiered February 3, 2013 during the stadium blackout of Super Bowl XLVII.  The same day, Paramount released apps for Android, iPhone and Windows Phone which enabled users to unlock tickets for showtimes two days before the films release date.  

An international trailer was released on March 21, 2013, with an embedded URL revealing an online-only international poster. On April 8, Paramount released the final international one-sheet featuring solely Benedict Cumberbatchs character.    On March 24, 2013, at 9:30 pm a swarm of 30 mini-quadrotors equipped with LED lights drew the Star Trek logo over London.     This choreography marked the beginning of the Paramount UK marketing campaign for Star Trek Into Darkness. It was coordinated with the World Wildlife Fund (WWF) Earth Hour event and was performed and developed by Ars Electronica Futurelab from Linz (Austria) in cooperation with Ascending Technologies from Munich (Germany).   

On April 12, 2013, iTunes Movie Trailers revealed the final domestic One sheet#Cinema|one-sheet featuring the USS Enterprise, and announced that the final US domestic trailer would be released on April 16. In the days leading up to the trailer release, character posters featuring Kirk, Spock, Uhura, and Harrison were released on iTunes.   
Paramount attempted to broaden the films appeal to international audiences, an area where Star Trek and other science-fiction films had generally performed poorly.    Into Darkness was dedicated to post-9/11 veterans.    J.&nbsp;J. Abrams is connected with The Mission Continues, and a section of the films website is dedicated to that organization.

===Promotional tours===
The cast (except for  . 

On May 10, Cho, Pegg, and Eve had a radio interview on The Bob Rivers Show. They discussed approaching a body of work already mastered by an earlier generation of actors, agreeing that they would remain with the franchise as long as it lasted.  That night, Chris Pine appeared on the Late Show with David Letterman; Letterman showed a gag reel of robots in a black-and-white film before showing a clip from Into Darkness. Pine said that he had to gain weight for the part of Captain Kirk. 

One story told by cast members during the promotion concerned an on-set prank initially devised by Pegg, which he later noted grew out of proportion. While filming at the National Ignition Facility, Pegg and Pine (with the crews help) tricked the arriving actors into believing there was "ambient radiation" at the location and they had to wear "neutron cream" to avoid being burned by it.   Cumberbatch was tricked into signing a release (which was meant to give the joke away, but he signed it without reading it),  while Urban and Cho were tricked into recording a public service announcement about the necessity for neutron cream. 

On May 13, Abrams appeared on  .

==Reception==

===Box office=== Rob Moore said he was "extremely pleased" with the sequels performance. 

Several weeks after release, the film grossed $147 million at the foreign box office, surpassing the lifetime international earnings of its predecessor.  Into Darkness reached the top spot of Chinas box office with a $25.8&nbsp;million gross, tripling the overall earnings of the previous film during its opening weekend.  Star Trek Into Darkness ended its North American theatrical run on September 12, 2013, with a box office total of $228,778,661, which places it as the 11th highest-grossing film for 2013.  It earned $467,365,246 worldwide, ranking it in 14th place for 2013, and making it the highest-grossing film of the franchise. 

Scott Mendelson of Forbes contends that the films box office performance was the result of the Paramounts inability to sell the basic components of the film’s story, and inclusion of Khan "for little reason other than marketability and then spent the next year or so lying to everyone and claiming said villain wasn’t in the picture....With no added value elements to sell, Paramount was forced to craft a generic campaign based around Benedict Cumberbatch as “Generic Bad Guy”, so the excitement never took hold....This was adding to the idea that merely withholding basic story elements is tantamount to promising stunning plot twists....and it made fans and general moviegoers less excited about Star Trek 2 than they were four, three, or even two years ago."  Calculating in all expenses, Deadline.com estimated that the film made a profit of $29.9 million. 

===Critical reception===
The film has received positive reviews, with critics calling it a "rousing adventure"  and "a riveting action-adventure in space".  Into Darkness has an 87%  approval rating on Rotten Tomatoes based on 246 reviews, with an average score of 7.6 out of 10. The sites consensus reads, "Visually spectacular and suitably action packed, Star Trek Into Darkness is a rock-solid installment in the venerable sci-fi franchise, even if its not as fresh as its predecessor".    On Metacritic the film has a score of 72 out of 100 , indicating "generally favorable reviews", based on 43 collected reviews.    It received an average grade of "A" from market-research firm CinemaScore.   
 Daily News  wrote that Cumberbatch delivered "one of the best blockbuster villains in recent memory".  Jonathan Romney of The Independent noted Cumberbatchs voice, saying it was "so sepulchrally resonant that it could have been synthesised from the combined timbres of Ian McKellen, Patrick Stewart and Alan Rickman holding an elocution contest down a well".  The New York Times praised his screen presence: "He fuses Byronic charisma with an impatient, imperious intelligence that seems to raise the ambient I.Q. whenever he’s on screen".   

Not all of reviews were positive, however; The Independent said the film would "underwhelm even the Trekkies".  Lou Lumenick of the New York Post gave the film one-and-a-half stars (out of four), saying it had a "limp plot" and the "special effects are surprisingly cheesy for a big-budget event movie".  A. O. Scott dismissed the film in The New York Times: "Its uninspired hackwork, and the frequent appearance of blue lens flares does not make this movie any more of a personal statement". 

The film was criticized for a scene with actress  , Abrams addressed the matter by debuting a deleted scene of actor Benedict Cumberbatchs character Khan taking a shower.   Eve addressed the underwear controversy at a 2013  Las Vegas Star Trek Convention and said, "I didn’t know it would cause such a ruckus. I didn’t feel exploited." 

{{quote box|quote=The second one, where Benedict Cumberbatch played Khan, I thought was unfortunate. Benedict Cumberbatch is a wonderful actor. I love everything that he’s done, but if he was going to be playing that character, J.J. should have made him an original character thats singular to him. Because the Khan character first appeared in our TV series, "Space Seed" and Ricardo Montalban was sensational in our second movie – he was the title character, The Wrath of Khan, you know! The other thought that Gene Roddenberry always had in the back of his mind — and that was his philosophy — was to embrace the diversity of this planet.
|source=—George Takei, who originated the role of Hikaru Sulu and appeared in Star Trek films and TV episodes   
|width=35%|align=right}}

Despite an acclaimed performance from Cumberbatch, Christian Blauvelt of Hollywood.com criticized the casting of the actor as   actor Garrett Wang tweeting "The casting of Cumberbatch was a mistake on the part of the producers. I am not being critical of the actor or his talent, just the casting".  George Takei, the original Hikaru Sulu, was also disappointed with Cumberbatchs casting.  On Trekmovie.com, co-producer and co-screenwriter Bob Orci addressed Khans casting: "Basically, as we went through the casting process and we began honing in on the themes of the movie, it became uncomfortable for me to support demonizing anyone of color, particularly any one of Middle Eastern descent or anyone evoking that. One of the points of the movie is that we must be careful about the villain within US, not some other race". 

===Accolades===
 
  
{| class="wikitable sortable"
|+ List of awards and nominations
! Year !! Award !! Category !! Recipients !! Result
|-
| rowspan="8"| 2013
|- California on Location Awards
| Location Team of the Year - Features
| Becky Brake, Steve Woroniecki, Taylor Boyd, Leo Fialho, Peter Gluck, Kathy McCurdy, Rob Swenson, Scott Trimble, Shelly Armstrong, Christina Beaumont
|  
|-
|rowspan="2"| Golden Trailer Awards    Best Summer 2013 Blockbuster Poster
|
|rowspan="4"  
|-
| Best Summer Blockbuster 2013 TV Spot
| "Return (Super Bowl Trailer)
|- Teen Choice Awards
| Summer Movie Star: Male
| Chris Pine
|-
| Summer Movie Star: Female
| Zoe Saldana
|-
| Britannia Awards 
| British Artist of the Year
| Benedict Cumberbatch (Also for 12 Years a Slave, The Fifth Estate, August: Osage County and The Hobbit: The Desolation of Smaug)
|rowspan="2"   
|- Hollywood Film Awards 
| Best Film
|
|-
|rowspan=20| 2014 19th Critics Choice Awards|Critics Choice Movie Awards    Best Action Film
|
| rowspan="12"  
|- Best Sci-Fi/Horror Movie
|
|- Best Visual Effects
|
|-
||IGNs Best of 2013 Movie Awards   
| Best Sci-Fi Movie
|
|- 40th Peoples Choice Awards|Peoples Choice Awards 
| Favorite Movie
|
|-
| Favorite Action Movie 
| 
|- 
| Favorite Movie Duo
| Chris Pine and Zachary Quinto
|- Empire Awards Empire Award Best Sci-Fi/Fantasy	
| 
|- 41st Annie Annie Awards Outstanding Achievement in Animated Effects in a Live Action Production
| Ben O’Brien, Karin Cooper, Lee Uren, Chris Root 
|- 
| Dan Pearson, Jay Cooper, Jeff Grebe, Amelia Chenoweth 
|- 67th British British Academy Film Awards
| Best Special Visual Effects
| Roger Guyett, Patrick Tubach, Ben Grossmann, and Burt Dalton	
|-
| BAFTA Kids Vote - Feature Film
|
|- Satellite Awards
| Best Overall Blu-ray
|
|  
|- Academy Awards Best Visual Effects 
| Roger Guyett, Patrick Tubach, Ben Grossmann, and Burt Dalton	
| rowspan="7"  
|- 2014 MTV MTV Movie Awards   Best Villain
| Benedict Cumberbatch
|-
| Favorite Character
| Khan Noonien Singh (Benedict Cumberbatch)
|- 40th Saturn Saturn Awards Best Science Fiction Film
|
|- Best Director
| J.J. Abrams
|- Best Supporting Actor
| Benedict Cumberbatch
|- Best Costume
| Michael Kaplan
|}

==Home media==
Star Trek Into Darkness was released as a digital download on August 20, 2013.  It was first released on DVD, Blu-ray and Blu-ray 3D in the United Kingdom on September 2  and in the United States and Canada on September 10.   The retailer Sainsburys has an exclusive edition with a second bonus DVD disc containing 33 minutes of extra features.  There is also a Special Limited Edition Blu-ray set available with a model on a stand of the USS Vengeance as seen in this movie. 

In North America, the release is split into various retailer exclusives. Retailer Best Buy has an exclusive Blu-ray edition with 30 minutes of additional content, available on disc in Canada, and via streaming service CinemaNow in the United States.  Targets Blu-ray edition also has 30 minutes of additional content that is different from Best Buys. Online retailer iTunes version comes with audio commentary for the film not available in the retailer exclusives.  A collection of deleted scenes is available exclusively via the Xbox SmartGlass second-screen app paired with the Xbox Video release of the film. 

The split of the special features between various retailers has attracted criticism from fans. In particular, The Digital Bits editor Bill Hunt remarked that "taking fully half or more of the disc-based special features created for a major Blu-ray release and casting them to the winds as retailer exclusives, thus forcing your customers to go on an expensive scavenger hunt...is, I’m sorry, absolutely outrageous....Seriously, if Paramount is going to treat its Blu-ray customers like this, they should just get out of the business altogether. Or better yet, farm all their titles...out to third party licensees who will treat these films and Blu-ray customers in general with greater care and respect."  Weeks after posting his article, Hunt himself was invited by Paramount to discuss about the issue of giving away the films special features to different retail partners. He suggested "  together the true special edition that should have been delivered from the start, with all of the extras that got scattered around to different retailers, including the enhanced audio commentary, plus all-new content just for this release....  to consider offering the IMAX version of the film..."  This eventually resulted with the release of Star Trek: The Compendium, a box set that includes all of the previously retailer exclusive special features, plus additional features for such as a gag reel, alongside both the theatrical release of 2009s Star Trek and the IMAX 2D version of Star Trek Into Darkness.

==Sequel==
Paramount has announced that the next film in the franchise, provisionally referred to by entertainment media as Star Trek 3, is planned to be released in July 2016, in time for the franchises 50th anniversary. {{cite web
  |url=http://www.trekmovie.com/2014/09/18/exclusive-roberto-orci-set-to-star-shooting-next-star-trek-movie-febuary-2015-targeting-summer-2016/
  |title=Exclusive: Roberto Orci Set To Start Shooting Next Star Trek Movie Feb 2015 – Targeting Summer 2016 Release
  |last=Pascale
  |first=Anthony
  |work=TrekMovie.com
  |date=September 18, 2014
  |accessdate=September 19, 2014
}}  According to reports, the film will take place in deep space, with the Enterprise and the crew dealing with an unrevealed crisis. This film will reportedly be more similar to the original Star Trek series than its predecessors were. {{cite web
  |url=http://variety.com/2014/film/news/star-trek-3-set-in-deep-space-1201254002/
  |title=Star Trek 3 Will Stay True to Original Franchise, Go Into Deep Space
  |last=Khatchatourian
  |first=Manne
  |work=Variety
  |date=June 28, 2014
  |accessdate=September 19, 2014
}} 

Due to Abrams commitment directing  , he will be a producer but not the director. {{cite web
  |url=http://variety.com/2014/film/news/roberto-orci-to-direct-star-trek-3-1201180140/
  |title=Roberto Orci to Direct ‘Star Trek 3′ (EXCLUSIVE)
  |last=Kroll
  |first=Justin
  |work=Variety
  |date=May 13, 2014
  |accessdate=May 13, 2014
}}  Writer Alex Kurtzman was announced to no longer be involved, with his writing partner Roberto Orci remaining, joined by Patrick McKay and J. D. Payne. {{cite web
  |url=http://variety.com/2014/biz/news/alex-kurtzman-roberto-orci-splitting-up-on-big-screen-exclusive-1201160542/
  |title=Alex Kurtzman & Roberto Orci Splitting Up on Bigscreen (EXCLUSIVE)
  |publisher=Variety
  |date=April 22, 2014
  |accessdate=2014-04-22
}}  It was later announced that Orci would also direct the film, but on December 5, 2014, it was reported that he would no longer direct and that Edgar Wright was being looked at as a potential replacement. {{cite web
  |url=http://www.comingsoon.net/movies/news/389701-breaking-bob-orci-removed-from-star-trek-3#/slide/1|title=Breaking: Bob Orci Removed from Star Trek 3!
  |last=Lesnick
  |first=Silas
  |work=ComingSoon.net
  |date=December 5, 2014
  |accessdate=December 5, 2014
}}  {{cite web | url = http://deadline.com/2014/12/star-trek-roberto-orci-1201311255/ | title = Roberto Orci Beaming Off ‘Star Trek’ As Director
   |last=Fleming Jr
   |first=Mike
   |work=Deadline.com
   |date=December 5, 2014
   |accessdate=December 6, 2014
}}  On December 22, 2014, Justin Lin was confirmed as director of Star Trek 3. 

On January 22, 2015, it was announced that Simon Pegg will co-write the third film with Doug Jung. 

Filming will commence in June 2015,  with Bryan Cranston and Idris Elba in talks for the villain role.   On April 10, 2015, Deadline reported that French actress Sofia Boutella was cast in the third film.  On April 21, 2015, Trek Movies revealed the title of the film would be Star Trek Beyond. 

==See also==
 
* List of action films of the 2010s
* List of science fiction films of the 2010s

==References==
 
 

==External links==
 
*  
*  
*  
*  

{{Navboxes|list1=
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 