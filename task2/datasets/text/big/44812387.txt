Lovely Man
{{Infobox film
| name = Lovely Man
| director = Teddy Soeriaatmadja  
| producer = Indra Tamoron Musu
| executiveproducer = Millan Rushan Sendi Sugiharto
| writer = Teddy Soeriaatmadja
| starring = Donny Damara Raihaanun Yayu Unru Ari Syarif Lani Sonda
| music = Bobby Surjadi
| cinematography = Ical Tanjung
| studio = Karuna Pictures
| editor = Wahyu Ichwandiardono
| costume = Ve Verdinand
| released =   (Busan International Film Festival)    (Indonesia)
| runtime = 76 minutes
| country = Indonesia Indonesian
}}Lovely Man is an  .
 religious group    but saw overwhelming receptions abroad; screening for various international film festivals.

==Plot==
Lovely Man tells the story of Cahaya, a 19-year-old girl with strong Islamic values, who discovers that her long lost father is a transvestite working on the streets of Jakarta. The story unfolds over that one night as they walk the streets of Jakarta and explains how the encounter changes their lives, as they learn about love, loss and redemption. 

Cahaya arrives in Jakarta from what can be assumed is her small town home just as the sun is setting. Armed with a piece of notepaper and a few rupiah, she’s in the city on a search for the father she hasn’t seen since she was four. Asking neighbors and shopkeepers in the area he lives in for Syaiful gets her blank stares in return. When they finally figure out she means Ipuy, they point her in the right direction and say he’s “working” around Taman Lawang (Jakartas infamous spot for transgender sex workers). Cahaya, naturally, goes looking for an office building or store.

When she locates Ipuy (Damara), she finds a transvestite prostitute plying her trade on the streets. In the initial minutes after encountering each other, both are shocked at the turn of evens. The innocent Cahaya is crushed at her father’s choices; Ipuy is horrified to see the daughter he willfully left behind.   

==Cast==
* Donny Damara as Syaiful/Ipuy, Cahayas transgender father
* Raihaanun as Cahaya, a devout 19-year old Muslim girl who is 
*   
*   
*  

==Production==
The film is made with very low budget, which director Soeriaatmadja referred to as the "survival technique."   

== Release == 2011 Busan International Film Festival on the segment "A Window on Asian Cinema." The segment also screens Ari Sihasales Serdadu Kumbang and Salaman Aristos Jakarta Maghrib.

In October 2013, it was screened for the London Indonesian Film Screenings at the  s Parts of the Heart, Eugene Panjis  , Rahung Nasutions Mentawai Tattoo Revival, Yosep Anggi Noens  , and  ,  .
 religious group  but still saw limited release in several theaters throughout the country on 10 May 2012.

The film was screened in the following film festivals: 
* Official Selection of the 2011 Mumbai Film Festival, India
* Official Selection of the 2011 World Film Festival of Bangkok, Thailand
* Official Selection of the 2011 Hong Kong International Film Festival
* In Competition at the 2011 Asiatica Filmmediale, Rome, Italy
* In Competition at the 2011 Bangalore International Film Festival, India
* In Competition at the 2011 New Delhi Digital Film Festival, India
* In Competition at the 2011 Torino LGBT Film Festival, Italy
* In Competition at the 2012 Indonesian Film Festival, Yogyakarta, Indonesia Tel Aviv International LGBT Film Festival, Israel
* In Competition at the 2012 Osaka Asian Film Festival, Japan
* In Competition at the 2012 Tiburon International Film Festival, California, United States
* 2011 Jogja-Netpac Asia Film Festival, Yogyakarta, Indonesia
* 2011 Hua Hin Film Festival, Bangkok, Thailand
* 2011 Q! Film Festival, Jakarta, Indonesia
* 2011 CinemAsia Film Festival, Amsterdam, Netherlands
* 2012 Melbourne Indonesian Film Festival, Victoria, Australia
* 2012 Palm Springs International Film Festival, California, United States Balinale International Film Festival, Bali, Indonesia Washington D. C., United States

==Reception==
Film critics website Inspire Ground praises Soeriaatmadjas direction in regards to Cahaya and Syaifuls contradicting values for managing to make "both world combined and worked well   without having too much fantasy or too depressing."  The review singled out Damaras performance as Syaiful/Ipuy, noting that " it was pretty shocking to see   as a transgender."

In a positive review, Elizabeth Kerr of The Hollywood Reporter also single out Damaras performance; calling it "Damaras show" while adding that "with his square jaw and heavy brow, Damara jettisons excessive mannerisms for little details (playing with his eyelashes, fidgeting with his wig) and stays respectful of Ipuy. He uses words as weapons and comports himself in a way that makes clear the status transgender people hold in the world. When he finally relates to Cahaya as Saiful, his ruggedly handsome features carry a melancholy that speaks to what happens after the film is over. Ipuy is in trouble with some local gangsters and after he sends Cahaya home with a promise never to contact him again, it’s clear how the story truly ends." 

==Accolades== Maya Awards, Asian Film Citra Awards. The film has been credited for reviving Damaras career as a film actor. 
{| style="text-align: center" class="wikitable sortable"
!Year
!Award
!Category
!Nominee(s)
!Result
|- 2012
|6th Asian Film Awards Asian Film Best Director Teddy Soeriaatmadja
| 
|- 2012
|6th Asian Film Awards Asian Film Best Actor Donny Damara
| 
|- 2012
|6th Asian Film Awards
|Peoples Choice Award for Favorite Actor Donny Damara
| 
|- 2012
|TLVFest|Tel Aviv International LGBT Film Festival Best Film
|Lovely Man
| 
|- 2012
|Osaka Asian Film Festival Special Mention Award
|Lovely Man
| 
|- 2012
|Tiburon International Film Festival Best Film
|Lovely Man
| 
|- 2012
|Tiburon International Film Festival Best Director Teddy Soeriaatmadja
| 
|- 2012
|Indonesian Movie Awards
|Peoples Choice Award for Favorite Film
|Lovely Man
| 
|- 2012
|Indonesian Movie Awards Best Actor Donny Damara
| 
|- 2012
|Indonesian Movie Awards Best Actress Raihaanun
| 
|- 2012
|Indonesian Movie Awards Best On-Screen Duo Donny Damara
Raihaanun
| 
|- 2012
|Indonesian Movie Awards
|Peoples Choice Award for Favorite Actor Donny Damara
| 
|- 2012
|Indonesian Movie Awards
|Peoples Choice Award for Favorite Actress Raihaanun
| 
|- 2012
|Indonesian Citra Awards of Indonesian Film Festival Citra Award Best Film
|Lovely Man
| 
|- 2012
|Indonesian Citra Awards of Indonesian Film Festival Citra Award Best Director Teddy Soeriaatmadja
| 
|- 2012
|Indonesian Citra Awards of Indonesian Film Festival Citra Award Best Actor Donny Damara
| 
|- 2012
|Indonesian Citra Awards of Indonesian Film Festival Best Screenplay Teddy Soeriaatmadja
| 
|- 2012
|Indonesian Citra Awards of Indonesian Film Festival Best Editing Wahyu Ichandiardono
| 
|- 2012
|Indonesian Citra Awards of Indonesian Film Festival Best Art Direction Richard Sibuea
| 
|- 2012
|Indonesian Citra Awards of Indonesian Film Festival Best Original Story Teddy Soeriaatmadja
| 
|- 2012
|1st Maya Awards Best Feature Film
|Lovely Man
| 
|- 2012
|1st Maya Awards Best Director Teddy Soeriaatmadja
| 
|- 2012
|1st Maya Awards Best Actor in a Leading Role Donny Damara
| 
|- 2012
|1st Maya Awards Best Actress in a Leading Role Raihaanun
| 
|- 2012
|1st Maya Awards Best Screenplay Teddy Soeriaatmadja
| 
|- 2012
|1st Maya Awards Best Costume Design Ve Verdinand
| 
|- 2012
|1st Maya Awards Best Makeup & Hairstyling Ebba Syeba
| 
|- 2012
|1st Maya Awards Best Sound Design Khikmawan Santosa
| 
|}

== References ==
 

==External links==

 

 
 
 
 
 
 
 