Kado Kendo
 
{{Infobox Film name = Kado Kendo image = Kado_Kendo_Poster.jpg caption = Kado Kendo Screening Poster director   = Budi Tandiono cinematography = Wellson Lo editing        = Wellson Lo,  David Chandra production designer = Clarinta Subrata writer     = Yulian Suwandri,  Irene Atmadja music  Billy Simpson,  Teguh Pramana Putra producer   = Arman Sundjaja,  Johan Suriadihalim
|distributor= Reach Production Australia Inc. released = 6 September 2007 runtime    = 100 minutes country = Australia language = Indonesian
|}}

Kado Kendo (Kendos Gift) is a 2007 Indonesian film produced by Indonesian students studying in Melbourne, Australia.

== About ==
Kendo Wijaya (Johan Suriadihalim) is a moody, reckless, and short-tempered guy who walks through life rebelliously and alone. Like a typical offspring of a wealthy family, Kendo is emotionally abandoned by his busy father, who sees money as an end to solve problems and provide happiness for his son. However, with Kendo’s future appearing bleaker by the day – in the midst of failing school and frequent school brawls – his father sends him abroad to Melbourne.

They say when one door closes, another one opens – and this is especially true in Kendo’s case. In a surprising turn of events, Kendo then manages to befriend several people with a mutual passion and talent for music and together they form a band. Also, against his own expectations, Kendo finds himself incredibly gripped by Natalia (Christa Natawidjaja), a sweet and religious girl whose personality is an exact antithesis of himself.

Yet, due to Kendo’s headstrong nature and bitter upbringing, he undoubtedly clashes with his friends and band members. But when all hope feels lost, Natalia introduces him to a single force that will subsequently transform his life – God. Problems may never cease to appear, but with God he can face them more courageously than before. But will he be able to ever forgive his neglecting father? And will he be able to face the ultimate test of faith when he unearths a devastating secret about Natalia, which will ominously change both of their lives?

Kado Kendo is a story of friendship, forgiveness and faith that intensifies the importance of having someone beside you to walk through it all...

Movie trailer can be viewed at  

== Cast ==
*Johan Suriadihalim as Kendo

*Christa Natawijaya as Natalia
 Billy Simpson as Andre

*Teguh Pramana  as Duta

*Calista Subrata as Lita

*Sentosa Setiawan as Damar

== Screening ==
U-Channel, a satellite Christian channel, aired Kado Kendo on 17 August 2010  .

In Melbourne, it was screened at   on 6–7 September 2007. It was also screened at Monash University Clayton Theater on 8 September 2007.

In Sydney, it was screened at Ritchie Theatre and Guthrie Theatre on 14–15 September 2007.

In Perth, it was screened at   on 26–27 October 2007.

The official screening website is  .

SATU TV, a local TV network, has coverage on the Melbourne Screening which was aired in October. It can also be watched on  .

== Production House ==
Kado Kendo was produced by REACH Production, which is affiliated to  . Their official website is  .

They recently screened Pelangi (Rainbow) on 8 and 9 May 2009. The trailer can be viewed at http://www.youtube.com/watch?v=OXXK6fFlnlY

== Original Soundtrack (OST) == Billy Simpson, Teguh Pramana Putra and Johanes Chen. The following is a list of the song titles:
*Dekat Hati
*Green
*Janjimu Itu
*Mousetrap
*Cerita Mentari
*Learning to Love
*Orang Gila

== Trivia ==
*In real life, Christa (Natalia) and Johan (Kendo) were really next door neighbors at the time of filming.
*The director, Budi Tandiono, appeared for a while as Bob during the orientation scene.
*The cameraman, Wellson Lo, appeared as himself in the Friendster photo scene.
*All of the home scenes were shot at various CityPoint apartment units, except for the flashback scene, which was shot in a Mulgrave house.
*The film was shot in summer and carried on through winter. In order to keep the costume consistency, the characters had to wear summer clothing despite the cold weather.
*Kado Kendo trailer was first aired during KafeArt 2007.

 
 
 