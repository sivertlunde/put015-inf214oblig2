Black Sun (1964 film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Black Sun
| image          = 
| caption        = 
| director       = Koreyoshi Kurahara
| producer       =
| writer         = Nobuo Yamada (screenplay)
| starring       = Tamio Kawaji Chico Roland Tatsuya Fuji
| music          = 
| cinematography = Mitsuji Kanau
| editing        =
| studio         = Nikkatsu
| distributor    = 
| released       =  
| runtime        = 95 min
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
  is a 1964 Nikkatsu film directed by Koreyoshi Kurahara based on a story by Tensei Kono and starring Tamio Kawaji and Chico Roland.  

==Plot==
At the start of the film, Akira commits larceny and buys a jazz record, an upbeat recording of "Six Bits Blues." In town, he encounters civil insurrection following the killing of an American soldier, presumably by one of his colleagues who is now missing. When Akira returns to his home and his dog Monk in a half-destroyed church, the missing soldier appears from behind a curtain and points a gun at Akira. His leg is injured, supposedly by the real assailants firearm. Since the soldier (Gill) is African-American, Akira is convinced that he will appreciate the jazz record he bought and tries to use it as a way of communication. He plays a few songs on the record, but the G.I. responds badly and, in a fit of restlessness, attacks and kills Meis dog.

Eventually, the soldier asks Akira to take him to the sea, for unknown reasons, and on their way there, they form a bond and become close friends. However, eventually they encounter military police|MPs. In a moment of despair, the soldier sings "Six Bits Blues" as its original blues dirge, affecting Akira. Akira and Gill find their way to the top of a building, overlooking the sea, where Gill ties himself to a balloon. He asks Akira to cut the rope, hoping to float up in the air and from there, to see his mother one more time. Akira reluctantly fulfills his request and they all watch the soldier, buoyed on his airborne balloon, as he approaches the sea.

==Cast==
* Tamio Kawaji as Akira
* Chico Roland as Gill
* Tatsuya Fuji as Akiras friend
* Shogen Shinda as the Engineer
* Yuko Chishiro as Yuki
* Hideji Ōtaki as the Owner of the Junk Shop

==Music==
The film featured music by the American jazz drummer Max Roach. 

==References==
 

==External links==
*  
*  

 
 
 
 
 


 