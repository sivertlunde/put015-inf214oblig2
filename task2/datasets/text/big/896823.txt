X: The Man with the X-ray Eyes
 
{{Infobox film
| name = X: The Man with the X-ray Eyes
| image = X-RayEyes Rep.jpg
| caption = Theatrical release poster
| director = Roger Corman
| producer = Roger Corman
| writer = Robert Dillon Ray Russell
| starring = Ray Milland Diana Van der Vlis Harold J. Stone John Hoyt Don Rickles Cathie Merchant
| music = Les Baxter
| cinematography = Floyd Crosby
| editing = Anthony Carras
| distributor = American International Pictures
| released =   1971 (France)
| runtime = 79 min
| country = United States English
| budget = ~$250,000 Roger Corman & Jim Jerome, How I Made a Hundred Movies in Hollywood and Never lost a Dime, Muller, 1990 p 117  gross = 53,087 admissions (France) 
}}
 horror motion picture|film.  Directed by Roger Corman from a script by Ray Russell and Robert Dillon, the film stars Ray Milland as Dr. James Xavier. A world renowned scientist, Dr. Xavier experiments with X-ray vision and things go horribly wrong. While most of the cast are relatively unknown, Don Rickles is notable in an uncharacteristically dramatic role. Veteran character actor Morris Ankrum makes an uncredited appearance, his last in the movie industry. American International Pictures released the film as a double feature with Dementia 13.

Shot in a mere three weeks on an ultra-slim budget of $300,000, Corman described the films success as a miracle. The movie was notable for its use of visual effects to portray Dr. Xaviers point of view. While crude by later standards, the visuals are still effective in impressing upon the audience the bizarre viewpoint of the protagonist.

==Plot synopsis==
Dr. Xavier develops eyedrops intended to increase the range of human vision, allowing one to see beyond the visible spectrum|"visible" spectrum into the ultraviolet and x-ray wavelengths and beyond. Believing that testing on animals and volunteers will produce uselessly subjective observations, he begins testing the drops on himself.

Initially, Xavier discovers that he can see though peoples clothing, and he uses his vision to save a young girl whose medical problem was medical error|misdiagnosed. Over time and with continued use of the drops, Xaviers visual capacity increases and his ability to control it decreases. Eventually he can no longer see the world in human terms, but only in forms of lights and textures that his brain is unable to fully comprehend. Even closing his eyes brings no relieving darkness from his frightening world, as he can see through his eyelids.  

After accidentally killing a friend, Xavier goes on the run, using his x-ray vision first to work in a carnival, and then to win at gambling in a casino. Xaviers eyes are altered along with his vision: first they become black and silver, and then entirely black. To hide his startling appearance, he wears dark wrap-around sunglasses at all times.
 evangelist that he is beginning to see things at the edges of the universe, including an "eye that sees us all" in the center of the universe. The pastor replies that what he sees is "sin and the devil", and declares the biblical quote of "If thine eye offends thee... pluck it out!",  and Xavier chooses to blind himself rather than see anything more.

==Alternate ending==
There have long been rumors about an alternate ending for the movie, in which Dr. Xavier removes his eyes, and afterwards screams "I can still see!"
 Danse Macabre. This footage has never turned up, but in the DVD audio commentary for X in the 2001 "Midnite Movies" series from Metro-Goldwyn-Mayer|MGM, Corman claimed shooting the scene on a whim—the "I can still see!" line was not in the script. However, Corman says he was dissatisfied with the results and retained the original scripts ending.

However, in other instances, Corman has claimed that the scene was completely Stephen Kings creation, though he wishes that he had thought of it.

==Awards, analysis and adaptations==
The film won the 1963 Best Film Award, The Silver Spaceship, at the First International Festival of Science Fiction Films.
 Danse Macabre, Lovecraftian quality to X, based on Xaviers near-insanity when he cannot comprehend the god-like being he sees at the center of the universe.
 Nighthawk to look like Ray Milland from his portrayal in the film, for the comic book mini series Earth X. The character also has eyes with powers.

==References==
 

==External links==
*  
*  

 

 

 
 
 
 
 
 
 