Porky's Romance
 
{{Infobox Hollywood cartoon
|cartoon_name=Porkys Romance
|series=Looney Tunes (Porky Pig/Petunia Pig)
|image=
|caption=
|director=Frank Tashlin
|story_artist= Robert Bentley Joe DIgalo Nelson Demorest Don Williams
|background_artist=
|layout_artist=
|voice_actor=Joe Dougherty
|musician=Carl Stalling, Milt Franklyn
|producer=Leon Schlesinger
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|studio=Warner Bros. Cartoons
|release_date=1937
|color_process=B&W
|runtime=
|movie_language=English
}}
Porkys Romance is a 1937 Looney Tunes cartoon that stars Porky Pig and for the first time, Petunia Pig. The cartoon deals with Porky trying to propose to Petunia and him getting rejected by Petunia. He proceeds to try to commit suicide, but ends up in a bizarre dream showing what would happen if he married Petunia.
 stutter had made it extremely difficult for the producers to work with him; such frustrations were evident in the cartoons production, much of which has Porky either as a silent character or one who only speaks with his face turned away from the view of the audience. (When Blanc took over the Porky role, he maintained the characters stutter, but harnessed it to achieve a greater comedic effect.)

==Plot==
The cartoon opens with an introduction of Petunia Pig; Petunia is shown as nervous, tripping on her lines and being unable to pronounce them correctly while on stage, leading an off-screen announcer to quietly tell her not to get excited. This causes her to go into an explosive rant and then the curtain closes on her and the main part of the cartoon starts. Porky is shown buying some flowers, candy, and then eventually a diamond ring. He proceeds to go over to Petunias house and knock on the door, then Petunia goes to answer the door with her spoiled dog, Fluffnums, but when she sees Porky, she is disgusted to see him so she disdainfully tells him to go away. This causes him to leave crying out of sadness and he then walks away, but Fluffnums sees the candy Porky has and alerts her to this fact.

Petunia proceeds to run out after Porky and take him into her house, where she rips open the candy container and starts eating it. Porky tries to help himself to the candy several times but is constantly harassed by Fluffnums, who snarls and growls at him each time he tries to reach for the candy box. He eventually tries to propose to Petunia, but as he is starting to do so, Fluffnums pulls a mean-spirited trick on Porky by pulling the rug out from under him and the fickle and selfish Petunia laughs at him, causing Porky to leave the house and walk off in shame. He proceeds to write a suicide note and tries to hang himself from a tree, but the branch the rope is on snaps due to Porkys weight, knocking him out and causing him to go into a dreamlike state.

Porky dreams that he is at a church and getting married to Petunia, then after the ceremony, they head off on their honeymoon and the couple get together there and then a message Time... munches on! is displayed on screen, and sounds of Petunia eating candy can be heard. The screen then shows Porky having to do all the housework while Petunia is essentially a couch potato; she has become fat and lazy, and Fluffnums has done the same thing. Eventually, Porkys kids (all of whom are named "Porky Pig Jr.") are woken up when a stack of dishes accidentally falls over on him from the kitchen counter. He tries to put them back to sleep after Petunia yells at him to "shut those kids up", but even though he meekly tells her he is trying his best to do so, Petunia yells at him for back-talking her and she beats him over the head repeatedly with a rolling pin while the kids cheer her on.

Porky then wakes from the dream by Petunia gently brushing him and saying yes to his proposal. When he remembers Petunias horrid treatment of him in his dream and fears what his future could hold for him if he marries her, he proceeds to take his gifts and runs off, then he comes back once to punish Fluffnums for its ill behavior toward him by kicking it, making the dog yelp in pain while Porky runs off again, leaving a dazed and confused Petunia behind as the cartoon irises out.

==Censorship==
*On the defunct WB channel, the end where Porky kicks Petunias dog through the closing iris was cut so that it didnt encourage animal cruelty, making the cartoon end with Porky running away from Petunia and never looking back.  However, this short has aired unedited (as recently as 2015) on the Canadian cable channel Teletoon Retro.

==External links==
* 

 
 
 
  
 
 