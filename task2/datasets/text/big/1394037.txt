Guest House Paradiso
 
 
{{Infobox film
| name           = Guest House Paradiso
| image          = Guest House Paradiso.jpg Adrian Edmondson
| producer       = Phil McIntyre
| writer         = Rik Mayall Ade Edmondson
| based on       = Bottom (TV series)|Bottom written by and starring Ade Edmondon and Rik Mayall
| starring       = Rik Mayall Ade Edmondson Universal Pictures
| released       = 3 December 1999
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
| music          =
| awards         =
| rating         =
| budget         = £3,000,000
| followed_by    = 
}} slapstick dark Adrian Edmondson, and directed by Edmondson - his directorial debut for a feature film. 

The film is semi-officially based on their comedy television series  " in the TV show, is here referred to as "Richard Twat" (although he regularly and angrily insists on the pronunciation "Thwaite"). Edmondsons character changes from "Edward Elizabeth Hitler" in the TV/live show to "Edward Elizabeth Ndingombaba". The film was made at Ealing studios and on location on the Isle of Wight.

==Plot== Adrian Edmondson) run the worst guest house in the United Kingdom ("Youre not in any of the guidebooks. Nobody for miles around - an oasis of calm. Even the peasants in the village denied its existence."), neighbouring a poorly maintained nuclear power station. The chef is not only unable to cook, but is both an idiotic drunkard and an illegal immigrant and eventually leaves due to not being paid ("Have you seen Pascal? Oh, damn. Ill have to phone the psychiatric hospital, hes probably checked himself in again!"). The guests (one of them played by Bill Nighy) are thoroughly dissatisfied by the poor service, and all decide to leave, except for one "Mrs Foxfur" (Fenella Fielding) who lives there.
 Italian actress "Gina Carbonara" (Hélène Mahieu) comes to stay in the grotty house while seeking safety from her ill-tempered fiancé Gino Bolognese (Vincent Cassel). However Gino does eventually find her at the guest house as Eddie and Richard had put her name up in lights outside in order to attract more guests. Later, Richie finds some fish, which fell off a military lorry heading away from the nuclear power station. Richie and Eddie dont realise that the fish had been contaminated by a radiation leak until after theyve fed them to the guests.
 winks to the camera after commenting "How lucky he was the only fatality. Otherwise thered be a moral question mark hanging over our escape."

==Cast==
*Rik Mayall as Richard Twat Adrian Edmondson as Eddie Elizabeth Ndingombaba
*Vincent Cassel as Gino Bolognese
*Hélène Mahieu as Gina Carbonara
*Bill Nighy as Mr Johnson
*Kate Ashfield as Ms Hardy
*Simon Pegg as Mr Nice
*Fenella Fielding as Mrs Foxfur
*Lisa Palfrey as Mrs Nice Steve ODonnell as Chef

== Reception ==
Empire (film magazine)|Empire gave the film two stars out of five stating "The boys toil incredibly hard to make the whole thing work and, while there are some hilarious moments, it is far too patchy for a full feature film.". 

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 