The Lebanese Rocket Society (film)
 
The Lebanese Rocket Society is a 2012 Franco - Lebanese documentary film directed by Joana Hadjithomas and Khalil Joreige and released theatrically on 1 May 2013. 

==Synopsis==
In the 1960s, Lebanon was the first Arab country to start sending rockets into the sky.  Led by Manoug Manougian, their physics teacher, a small group of students from the Haigazian University (called Haigazian College at the time) began tests and launched their first rockets to conquer space under the name ″Lebanese Rocket Society″. Their work will take more and more important as to make the headlines, and the Lebanese space program was briefly a source of national pride.

Who were these young people? Why have they stopped? And most importantly, why was all this totally forgotten?

==Cast and crew==
* Director: Joana Hadjithomas and Khalil Joreige
* Production: Edouard Mauriat (Mille et une productions), and Georges Shoucair (Abbout Productions)
* France Distribution: Urban Distribution
* Photography: Jeanne Lapoirie and Khalil Joreige
* Animation: Ghassan Halawani
* Editing: Tina Baz Scrambled Eggs
* Genre: Documentary
* Country of origin: France, Lebanon DCP

==Selections==
* Official selection at the Doha Tribeca Film Festival 2012 
* Official selection at the Toronto International Film Festival 2012 
* Official selection at Cinéma du réel 

==References==
 

 
 
 
 
 
 
 


 
 