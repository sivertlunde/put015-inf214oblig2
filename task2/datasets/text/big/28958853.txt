The Vigilantes Return
{{Infobox film
| name           = The Vigilantes Return
| image          = 
| image size     = 
| alt            = 
| caption        =  Ray Taylor
| producer       = 
| writer         = 
| screenplay     = Roy Chanslor
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Universal Pictures
| distributor    = 
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

The Vigilantes Return is a 1947 western film produced by Universal Pictures in Cinecolor.  The film was shot in Iverson Ranch in Chatsworth, Los Angeles, California.

==Plot== stage holdup, Curtwrights henchman, Ben Borden, talks the sheriff and Judge Holden into suspecting Johnnie. Johnnie reveals himself to Judge Holden as a government marshal, and the judge voices his opinion that Curtwright is the leader of the road agents, but voices it in the presence of his granddaughter, Louise Holden. The Judge doesnt know that Louise is in love with Curtwright, and she tips him off as to Johnnies real identity. Curtwright frames Johnnie for a murder and arranges for the crooked sheriff to promote a lynching and Andy and Kitty help Johnnie escape jail. Johnnie rounds up vigilantes and heads for a showdown at the Bull Whip saloon. 

==Cast== Jon Hall - Marshal Johnnie Taggart/Ace Braddock
* Margaret Lindsay - Kitty
* Andy Devine - Andy
* Paula Drew - Louise Holden Jack Lambert - Henchman Ben Borden
* Jonathan Hale - Judge Holden Robert Wilcox - Clay Curtwright
* Arthur Hohl - Sheriff
* Joan Shawlee - Bens Girl (Joan Fulton)
* Lane Chandler - Messenger
* Wallace Scott - Bartender

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 