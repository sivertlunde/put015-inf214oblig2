Damaged Goods (film)
:For the 1919 UK film based on the same play, see Damaged Goods (1919 film). For the 1933 film based on the same play, see Damaged Lives.
{{Infobox film
| name           = Damaged Goods
| image          = Damaged Goods (1917 reedition).jpg
| image_size     =
| caption        = Advertisement for the 1917 reedition Richard Bennett
| producer       =
| writer         = Eugène Brieux (play Les Avariés) Harry A. Pollard (adaptation)
| starring       = Richard Bennett Adrienne Morrison
| cinematography = Thomas B. Middleton
| editing        = Mutual Film Corporation
| released       =  
| runtime        =
| country        = English intertitles
| budget         =
| gross          =
}} American silent silent short Richard Bennett, Richard Bennett. It is based on Eugène Brieuxs play Les Avariés (1901) about a young couple who contract syphilis. No print of the film is known to exist, and Damaged Goods is considered to be a lost film.  It is believed to have begun the sex hygiene/venereal disease film craze of the 1910s. 
 Damaged Goods in 1919. A sound film based on the Brieux play, also titled Damaged Goods (1937) was directed by Phil Goldstone, released by Grand National Pictures, and was closer to an exploitation film about premarital sex without mentioning venereal disease. 

==Cast== Richard Bennett as George Dupont
*Adrienne Morrison as a girl of the streets
*Maud Milton as Mrs. Dupont
*Olive Templeton as Henriette Locke
*Mrs. Tom Ricketts as Mrs. James Forsythe
*Jacqueline Moore as Seamstress
*Florence Short as Nurse
*Louis Bennison as Dr. Clifford
*John Steppling as Senator Locke William Bertram as a quack doctor George Ferguson as the quacks assistant
*Charlotte Burton as Mrs. Lester

==See also==
*List of lost films

==References==
 

==External links==
*  AFI Catalog of Feature Films

 
 
 
 
 
 
 
 
 
 
 
 


 