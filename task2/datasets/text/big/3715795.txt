Viva Knievel!
{{Infobox film
| name = Viva Knievel!
| image =Viva Knievel!.jpg
| caption = Theatrical poster Gordon Douglas Stanley Hough
| story = Antonio Santillán
| screenplay = Norman Katkov Cameron Mitchell Frank Gifford Dabney Coleman Marjoe Gortner Charles Bernstein
| cinematography = Fred Jackman Jr.
| editing = Harold F. Kress
| distributor = Warner Bros.
| released =  
| runtime = 106 minutes
| language = English
| country = United States
| budget =
| gross =
}}
Viva Knievel! is a 1977 action film starring Evel Knievel (as himself), Gene Kelly, and Lauren Hutton.

== Plot ==
 . One of the boys casts away his crutches, telling Knievel that hell walk after his accident just as Knievel had.

Knievel then prepares for another of his stunt jumps. We are introduced to his alcoholic mechanic Will Atkins (Gene Kelly), who was a former stunt rider himself before his wife died, driving him to drink. While signing autographs, Knievel is ambushed by feminist photojournalist Kate Morgan (Lauren Hutton), who has been sent to photograph the jump: if Knievel is killed, it will be a great story.

{{Quote box quote  = “Before I make the jump, there’s something I’d like to say to you, that’s been bothering me for a long time.
I go to Indianapolis every year to see the Indy 500.  I go there with friends to drive and race.  Every year when they go there to qualify, they usually have to go as fast as they possibly can to get a front row position.  They put nitro in their cars sometimes, instead of the fuel that is intended to be in the cars so that the cars will go faster…and they do, for five or ten laps.  And then they blow all to hell.
And you people, you kids, if you put nitro in your bodies in the form of narcotics, so that you can do better, or so that maybe you think that you can do better, you will for about five or ten years, and then you’ll blow all to hell.” source = Evel Knievel, speaking to the audience just before the first jump width  = 50% align  = left
}}

As it happens, Evel does crash while attempting the stunt, and though badly injured, survives. He berates Morgan, announces his retirement, and is taken to the hospital.

While rehabilitating, Knievel resists all attempts to get back on the horse, including those from Jessie (Marjoe Gortner), a former protégé with mysterious backers who want Evel to do a jump in Mexico. Eventually, though, Knievel relents and agrees.

A subplot develops when Wills estranged son Tommy shows up from boarding school, and asks to join the tour. Will, who is reminded of his dead wife, is cold to Tommy, leaving Knievel to show the boy kindness. Likewise, Kate reappears, apologetic for her previous motives, and now wishes that he will never stop jumping.

Meanwhile, Jessies benefactor is revealed: Drug lord Stanley Millard (Leslie Nielsen). Millard (without Jessies knowledge) plans to cause a fatal accident during the jump. He will then have Knievels body transported back to America in an exact duplicate of the tour trailer, but one that has a massive supply of drugs hidden in the walls.

Will, however, stumbles onto the plot, is drugged, and sent to a psychiatric ward under the control of the corrupt Ralph Thompson (Dabney Coleman) to prevent him from spilling the beans. Evel sneaks into the ward late at night when Will has dried out, but all Will can remember is that someone knocked him out. Knievel leaves him there to keep whoever is behind the plot in the dark.

As Knievel prepares for the jump (down a massive ramp and over a fire pit), Jessie&mdash;hopped up on drugs&mdash;confronts Evel, claiming that he will prove who the best jumper is. Jessie knocks Evel out and dresses in Knievels signature red, white, and blue outfit. Jessie then successfully makes the jump, however, the bike has been sabotaged and he is killed as he lands (footage from a real Knievel crash was used). While the body is taken away for the drug smuggling plot, Evel wakes up, gets on another bike, and goes to free Will. 

After breaking out of the psych ward, the two find the mockup trailer, in which, by an amazing coincidence, both Tommy and Kate have been taken hostage. Pursuing the truck, Will and Evel decide to split up: Will will disable the semi, Evel will lead off the gun-toting drug lords riding guard in another car.

At the end of several extended chase scenes, the drug lords are defeated, Will and his son are reunited, and Kate has fallen head over heels for Knievel. The film ends with Knievel performing a daredevil jump over a pit of fire, this time successfully.

The end jump is stopped in a freeze-frame shot and a color matte, similar to that of the one that appears in the opening credits, appears over Evel in mid-air. The song that plays over the opening credits also plays over the films end credits.

==Production==
The production was done under the Irwin Allen banner, with Allen serving as the uncredited Supervisor Producer.  Irwin Allens wife, Shelia Allen, has a credited role as Sister Charity.

For the more dangerous motorcycle stunts, the producers hired the professional stuntman Gary Charles Davis.  However, Davis role in the production was kept under wraps to avoid questions about Knievel himself performing his own motorcycle stunts.
 Wembley Stadium.

To allow for a love interest to occur with Lauren Huttons character, Evel is apparently single and there is no mention of Knievels then-wife, Linda, or his (at the time) three children.

== Popular culture reception== Mike Nelson, Kevin Murphy and Bill Corbett. 

==Cast==
*Evel Knievel as Himself
*Gene Kelly as Will Atkins
*Lauren Hutton as Kate Morgan
*Red Buttons as Ben Andrews
*Leslie Nielsen as Stanley Millard
*Marjoe Gortner as Jessie Cameron Mitchell as Barton
*Frank Gifford as Himself
*Eric Olson as Tommy Atkins
*Albert Salmi as Cortland
*Dabney Coleman as Ralph Thompson
*Sheila Allen as Sister Charity

== References == 
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 