Densha Otoko (film)
{{Infobox film
| name           = Train Man (Densha Otoko)
| director       = Shosuke Murakami
| producer       = Minami Ichikawa Hiroyoshi Koiwai
| writer         = Arisa Kaneko
| starring       = Takayuki Yamada Miki Nakatani
| music          = Takayuki Hattori
| cinematography = Shigeki Murano
| editing        = Junosuke Hogaki
| released       =  
| runtime        = 105 minutes
| country        = Japan
| language       = Japanese
}}
  is a 2005 Japanese film, starring Takayuki Yamada and Miki Nakatani. It is part of the Densha Otoko franchise. The film was a big success at the box office, making the story of Densha Otoko popular. It was directed by Shosuke Murakami. The screenplay was written by Arisa Kaneko, and was based on the original book by Hitori Nakano.

==Plot== the drama, with the lead protagonist saving a young woman from a drunk on the train. The young woman sends him a set of Hermès tea cups as a thank you.

==Cast==
*Takayuki Yamada - Train_Man/Densha Otoko
*Miki Nakatani - Hermes
*Eita - Hirofumi (hikikomori)
*Tae Kimura - Michiko (housewife)
*Ryoko Kuninaka - Rika (nurse)
*Kuranosuke Sasaki - Hisashi (business man)
*Yoshinori Okada - Yoshiga (geek 1)
*Hiroki Miyake - Tamura (geek 2)
*Makoto Sakamoto - Muto (geek 3)
*Naomi Nishida - Hermes friend
*Momoko Shimizu - schoolgirl
*Ren Osugi - drunkard

==Production==
The film was filmed in only 25 days and released in 35 days. 

Atsushi Itō and Misaki Ito who respectively played Densha Otoko and Hermes in the television series have a cameo appearance after the end credits in the movie.  Incidentally, Yamada also appears in a brief cameo in the first episode of the TV series.  Miho Shiraishi who had a brief minor role in the movie also appears in the TV series as a different character.

==Release==
VIZ Media, through its Viz Pictures arm, gave the film a limited release in the United States on September 22, 2006, and the DVD was released on February 6, 2007. An accompanying manga series followed. 

In the United States, the film premiered at the Imaginasian Theater in New York City, New York on September 22, 2006. It was also screened in Portland, Oregon, Chicago, Illinois and Seattle, Washington.

==Reception== movies in Japan in 2005, staying on in the box offices top 10 movies for 10 weeks, grossing Japanese yen|¥3,532,525,613 as of January 2006.  It grossed over $35 million since its release. 

Metacritic gave Densha Otoko an aggregated score of 60%.  Pop Culture Shocks Erin Finnegan commented on the movies ASCII art, which made the "strange transition from computer monitor to silver screen". She continues, "you might be familiar with emoticons in English internet-speak, but the Japanese use totally different emoticons. Thankfully, these symbols are clarified in the film. The bowing man art is usefully overlaid with an image of Densha Otoko bowing."    Manga Lifes Michael Aronson criticised the film, saying, "It’s understood that he lacks all social grace when he trips and falls in the train in the beginning, but he does so over and over throughout the film. Furthermore, the length could easily be cut by twenty minutes if Train Man could keep from stammering all the time and actually spit out a word or two. His inadequacies translate into a crippling experience for the audience who just wants to see him get the girl and call it a day. Lastly, the characterization of the girl is so bare-bones that it’s bewildering why she would be attracted to him at all." 

San Francisco Chronicles G. Allen Johnson commented that Train Man "fable ups the ante on depicting modern communication". He also commented on the chat-room messages, saying "chat-room messages are texted right onto the screen, sometimes narrated in voiceover, with such frequency that a real person saying something in his real voice to another real person can be startling".  The New York Times Jeannette Catsoulis commented on Train Man: Densha Otoko, saying, "though its appeal may be limited to the socially awkward, the movie is unusually perceptive about the attractions of online communities while gently insisting on the superiority of flesh over fantasy. “Train Man” wants us to get off our computers and get out of the house; in a country as technocentric as Japan, the suggestion that it may be time to replace the cyber with the real is not just subversive, it’s downright revolutionary." 

The Village Voices Drew Tillman criticises the film for its "annoying split screens" when "  turns to his chat room buddies for advice on how to win   over".  TV Guide′s Maitland McDonagh criticises the film for its ending that "drags on, and the fantasy sequences are bluntly obvious (though Train Mans nightmare vision of meeting Hermes parents is pretty funny)". However, she commends the fairy-tale romance   is grounded in authentic detail. 

==See also==
*Densha Otoko
*Densha Otoko (drama)

==References==
 

==External links==
*    
*  

 

 

 
 
 
 
 
 

 