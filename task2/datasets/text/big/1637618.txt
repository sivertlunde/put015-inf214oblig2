10 MPH
 
{{Infobox film
| name           = 10 MPH
| image          = 10 MPH cover.jpg
| director       = Hunter Weeks
| producer       = {{Plainlist | 
* Hunter Weeks 
* Josh Caldwell 
* Johnathan F. Keough
}}
| writer         = Hunter Weeks
| starring       = {{Plainlist | 
* Hunter Weeks 
* Josh Caldwell 
* Johnathan F. Keough
}}
| music          = Everett Griffiths
| editing        = Josh Caldwell
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = US $54,000
}}
10&nbsp;MPH is a documentary film directed by Hunter Weeks and starring Josh Caldwell with his Segway HT, the two-wheeled electronic scooter. This film, which takes its name from the Segways average speed, documents Caldwells 100-day, coast to coast journey across the United States riding the "Human Transporter". The trip started in Seattle|Seattle, Washington on August 8, 2004 and ended in Boston, Massachusetts on November 18, 2004. 10&nbsp;MPH has had a favorable reaction at screenings and film festivals and has won several awards.   

==Plot==
 
10&nbsp;MPH follows the progress of Caldwell as he rides a Segway scooter across the United States from Seattle to Boston, stopping at many places along the way to interact with people. The film focuses on showing the dynamic nature of the US countryside as well as documenting the stories of people Caldwell and Weeks encounter along the way.
 batteries to the production challenge of losing a producer part way through the filming process.

==Development==
Dissatisfied with their average corporate jobs,  filmmakers Weeks and Caldwell formed a production company, Spinning Blue, in Denver, Colorado prior to beginning production of 10&nbsp;MPH.  They have discussed their lack of training and experience in the film industry, lack of investment capital to make the film, and lack of film industry contacts as challenges at the beginning of the process.  Rather than pursuing traditional channels of film making, Weeks and Caldwell used the Web to raise funds and promote 10&nbsp;MPH.    They conceived the idea of making a film before knowing what it would be about.  Shortly thereafter, a friend gave them the idea of making a film involving a Segway. 

In 2004, Weeks and Caldwell purchased a Segway and decided to film a documentary about traveling across the United States at  .     Caldwell actually rode the scooter while Weeks directed and managed the film footage. Weeks and other members of the production crew followed Caldwell in a car with the film equipment.   

They used a Sony PD-150 to capture 180&nbsp;hours of footage.  Post-production occurred back in Denver after the Segway journey with involvement from other members of the Denver film community. 

==Distribution==
10&nbsp;MPH is available in several ways including on DVD, which premiered on May 29, 2007 in the United States and is available through Netflix and other outlets, and by direct download from the film Web site and from iTunes.  Additionally, Weeks and Caldwell spent months in 2007 traveling back over their path to show the finished documentary in theaters along the way. 

 

==Critical response== rating average of 7.4 out of 10. {{cite web |url= http://www.rottentomatoes.com/m/1176779-10_mph//
|title=10 MPH |publisher= Flixster |work= Rotten Tomatoes|accessdate=January 3, 2010| archiveurl= //web.archive.org/web/20091201014322/http://www.rottentomatoes.com/m/1176779-10_mph/| archivedate= December 1, 2009  }} 

In   compared it with other accounts of American   in Blue Highways.” 

The documentary also received praise in the West Seattle Herald, where Bruce Bulloch called Caldwell and Weeks “savvy storytellers” and applauded 10&nbsp;MPH for being a “witty counterpoint to the hyped-up rhythms of American life.” Bulloch further commented of 10&nbsp;MPH, that despite its slow tempo “  has a seductive vitality about it – the last fit of reckless abandon of a disappearing youth.” Noting that while the popularity of documentaries has risen in recent years; “Theyre generally politically charged forays into the troubling issues of our times. ‘10 MPH’ heads off in another direction, taking us, regardless of age, on a sentimental journey to the cusp of adulthood when a sense of adventure was still an important virtue.” 
 This land is your land, this land is my land.” 

In a favorable review in the   as they ever-so-slowly wound their way from the home of the Space Needle to the city of Fenway Park.” 

In an article on upcoming releases in 2007, the Boston Globe mentioned 10&nbsp;MPH, calling it an “..amusingly offbeat documentary.”  In the Idaho Statesman, Chad Dryden remarked; “10 MPH succeeds on many levels. For one, it’s a terrific American travelogue, taking the viewer through small-town charm, purple mountain majesty and the big city’s vibrating pulse. More importantly, it’s an inspiring story about cutting through the stagnancy of 21st-century life to follow your dreams.” 

Calling the documentary “Charming and maddening”, Jordan Harper raised concerns with the main concepts of 10&nbsp;MPH, writing in the  , be one of those jackasses from the auditions. The guys seem nice enough, and there are plenty of sweet slices of Americana, so you might find yourself torn. Its hard not to be drawn in by the films good-natured vibe, but theres no getting around the urge to smack these guys and tell them to make their next movie about anything except themselves.” 

==Artwork and soundtrack==
  Roman Candle and Daphne Loves Derby.

==References==
 

==External links==
*  
*  , full film freely available online  
*  
*   Everett Griffiths (other songs by)

 

 
 
 
 
 
 
 
 