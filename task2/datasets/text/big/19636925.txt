The Wedding Night
{{Infobox film
| name           = The Wedding Night
| image          = The Wedding Night poster.jpg
| alt            = 
| border         = yes
| caption        = Theatrical release poster
| director       = King Vidor
| producer       = Samuel Goldwyn
| screenplay     = Edith Fitzgerald
| story          = Edwin H. Knopf
| starring       = {{Plainlist|
* Gary Cooper
* Anna Sten
* Ralph Bellamy
}} Alfred Newman
| cinematography = Gregg Toland
| editing        = Stuart Heisler
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
}} romantic drama film directed by King Vidor and starring Gary Cooper, Anna Sten, and Ralph Bellamy.    Based on a story by Edwin H. Knopf, the film is about a financially strapped novelist who returns to his country home in Connecticut looking for inspiration for his next novel and becomes involved with a beautiful young Polish woman and her family.    King Vidor won the Volpi Cup for Best Director at the Venice Film Festival in 1935.  

==Plot==
New York novelist Tony Barrett (Gary Cooper) and his wife Dora (Helen Vinson) have accumulated serious debts as a result of their fast and affluent lifestyle in the big city. When he approaches his publisher expecting an advance on his newest novel, Tony is told that success has gone to his head and the novel is unpublishable. With few options available, Tony and Dora move to his familys run-down farm in Connecticut, where they meet his neighbors, a Polish farmer, Jan Novak (Sig Ruman), and his beautiful daughter, Manya (Anna Sten). Looking to expand his own property, Mr. Novak offers Tony $5,000 for a field bordering the Novak farm. With their finances replenished, Dora returns to New York, leaving Tony at the farm, where he intends to write a new novel inspired by the Novaks and their friends.

Sometime later, Tony and Manya are at his house discussing her betrothal to Fredrik (Ralph Bellamy), the young man chosen by her father to be her husband. A drunken Tony tells Manya that she is not in love with the young man, and makes suggestive remarks that anger her. The following day, Tony apologizes to Manya and the two begin a close friendship. After Tonys servant leaves to return to New York, Manya begins spending more time at Tonys farm and the two fall in love, just like the characters in his new novel, Stephen and Sonya. 

Fredrik soon learns that Manya has been seeing Tony at his farm, and he and her father forbid her from seeing Tony again. Ignoring their orders, Manya continues to spend time with the novelist at his house. One evening, a snow storm prevents Manya from returning home. The next morning, Mr. Novak angrily confronts Tony at his farm. Later, he demands that Manya marry Fredrik the following Monday. When Manya tells him that she will not spend her life being an unpaid servant like her mother, Novak slaps her.

Later that day, Dora arrives back at the farm, telling her husband that she missed him terribly during their separation. Dora hears stories about the previous night, but she hopes that they mean nothing, until she reads his new manuscript about the love affair between Stephen and Sonya. On the night before her wedding, Manya goes to see Tony, but finds Dora instead. The two speak about Tonys new novel and how it will end—both realizing that they are really speaking about their own lives. Dora tells Manya that she is sure that Stephens wife, Daphne, will not give up her husband, but that she would feel sorry for Sonya. Manya tells her about the upcoming wedding, and then leaves. 

Tony returns home and asks Dora for a divorce, but she refuses, telling him that his novel should end with Sonya marrying her Polish fiancé. The next day, after learning that Manya and Fredrik are getting married, Tony goes to the wedding party and dances with Manya, and then returns home. Later, a drunken Fredrik, angered by his fiancés lack of passion and interest, storms out of their bedroom and heads over to Tonys house to confront the man he suspects is to blame. Manya follows him to Tonys house, where she attempts to stop Fredrik from fighting with Tony on the stairs. During the struggle, Manya falls down the stairs and is seriously injured. Tony carries her to the parlor, where he tells her he loves her, just before she dies. Later, Dora tells Tony that he can now see Manya privately. Gazing out the window, Tony tells her about how full of life Manya was and imagines that she is waving to him.

==Cast==
* Gary Cooper as Tony Barrett
* Anna Sten as Manya Novak
* Ralph Bellamy as Fredrik Sobieski
* Helen Vinson as Dora Barrett
* Sig Ruman as Mr. Jan Novak
* Esther Dale as Mrs. Kaise Novak 
* Leonid Snegoff as Sobieski 
* Eleanor Wesselhoeft as Mrs. Sobieski 
* Milla Davenport as Grandmother 
* Agnes Anderson as Helena 
* Hilda Vaughn as Hezzie Jones 
* Walter Brennan as Bill Jenkins    

==Reception==
In his review in The New York Times, Andre Fennwald praised the film for its "uncommonly adult style" that is "both pictorially and dramatically striking." Fennwald writes:
  }}
Fennwald also praised the performances by Sten, the "highly talented Russian star", Cooper who "continues to reveal a refreshing sense of humor in his work", and Helen Vinson who is "excellently right as the wife, playing the part with such intelligence and sympathy that she contributes definitely to the power of the climax. 

==Awards and nominations==
* 1935 Venice Film Festival Award for Best Director (King Vidor)  Won 
* 1935 Venice Film Festival Nomination for the Mussolini Cup (King Vidor)    

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 