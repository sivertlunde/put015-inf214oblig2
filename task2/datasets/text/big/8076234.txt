Camille (1984 film)
{{Infobox television film
| name           = Camille
| image          = Camille_(1984_film).jpg
| caption        = Video cover
| director       = Desmond Davis
| producer       = Norman Rosemont
| writer         = Alexandre Dumas, fils (novel& play)  Blanche Hanalis
| starring       = Greta Scacchi Colin Firth John Gielgud Billie Whitelaw  Patrick Ryecart Denholm Elliott Ben Kingsley
| music          = Allyn Ferguson
| cinematography = Jean Tournier
| editing        = Alan Pattillo Hallmark Hall of Fame Productions
| released       =  
| runtime        = 100 minutes
| country        = United Kingdom United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} La Dame aux Camélias by Alexandre Dumas, fils. It was adapted by Blanche Hanalis and directed by Desmond Davis. It stars Greta Scacchi, Colin Firth, John Gielgud, Billie Whitelaw, Patrick Ryecart, Denholm Elliott and Ben Kingsley. 

==Plot==
Camille is a courtesan in 19th Century Paris: she falls deeply in love with a young man of promise, Armand Duval. When Armands father begs her not to ruin his hope of a career and position by marrying Armand, she acquiesces and leaves her lover, leading the two lovers to other relationships and to tragedy. 

==Cast==
*Greta Scacchi as Marguerite Gautier
*Colin Firth as Armand Duval
*John Geilgud as Duke de Charles
*Billie Whitelaw as Prudence Duvorney
*Patrick Rycart as Gaston
*Ben Kingsley as Duval
*Denholm Elliott as Count de Nolly
*Rachel Kempson as Hortense 
*Ronald Pickup as Jean
*Julie Dawn Cole as Julie
*Natalie Ogle as Blanche 
*Richard Beale as Farmer 
*Kathy Staff as Flower Lady 
*Maurice Teynac as Joseph  William Morgan Sheppard as Captain
*Shelagh McLeod as Nicole

==See also==
Camille (disambiguation)

== External links ==
*  

 
 

 
 
 
 
 

 
 