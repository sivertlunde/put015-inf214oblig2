The Round-Up (1920 film)
 
{{Infobox film
| name           = The Round-Up
| image          = Round Up Poster.jpg
| image_size     =
| caption        = Movie poster
| director       = George Melford
| producer       = 
| writer         =  
| narrator       = 
| starring       =  
| music          = 
| cinematography = Paul P. Perry
| editing        = 
| distributor    = Famous Players-Lasky Corporation
| released       =  
| runtime        = 
| country        = United States
| language       =  
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Western film Tom Forman, Julia Dean on the Broadway stage in 1907. It was Macklyn in the play who created the famous phrase used in advertisements of the film, nobody loves a fat man. 

Arbuckle was cast as a most unconventional-looking cowboy lead in The Round-Up because the studio didnt want to let their expensive star remain idle while his next comedy was being readied, and the film turned out to be one of Arbuckles biggest critical successes.

The movie was screened in April and May 2006 as part of a massive 56-film Arbuckle retrospective at the Museum of Modern Art in New York City.  The museum chose to take the unprecedented step of running the entire series twice in a row for additional emphasis, once in April and a second time in May. 

==Cast==
* Roscoe "Fatty" Arbuckle as Slim Hoover
* Mabel Julienne Scott as Echo Allen
* Irving Cummings as Dick Lane Tom Forman as Jack Payson
* Wallace Beery as Buck McKee
* Jean Acker as Polly Hope
* Guy Oliver as Uncle Jim
* Jane Wolfe as Josephine
* Fred Huntley as Sagebrush Charlie
* George Kuwa - Chinese boy
* Lucien Littlefield - Parenthesis
* Chief Red Fox Molly Malone
* Buster Keaton - Indian (uncredited)

==See also==
 
* List of American films of 1920
* Fatty Arbuckle filmography
* Buster Keaton filmography

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 
 