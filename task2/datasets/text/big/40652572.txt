Kaithi Kannayiram
{{Infobox film
| name           = Kaithi Kannayiram
| image          =
| caption        =
| director       = A. S. A. Sami
| producer       = T. R. Sundaram
| writer         = A. S. A. Sami
| based on       = Quaidi No. 911
| starring       = R. S. Manohar   Rajasulochana   P. S. Veerappa   Javar Seetharaman   K. A. Thangavelu
| music          = K. V. Mahadevan
| cinematography = C. A. S. Mani S. S. Lal
| editing        = 
| studio         = Modern Theatres
| distributor    = Modern Theatres
| country        = India Tamil
| released       = 1 December 1960
| runtime        = 
}}
 1960 Indian Indian Tamil Tamil film, written and directed by A. S. A. Sami. Produced by T. R. Sundaram of Modern Theatres, the film starred R. S. Manohar and Rajasulochana in the lead.

== Plot ==
A tough jailors (‘Javert Seetharaman) kid (‘Baby Savithiri) is kidnapped by a vengeful murderous prisoner (Veerappa). Another prisoner (Manohar) who has lost his child (‘Master Sridhar) escapes from the prison. He takes up the task of rescuing the jailors missing kid. The duty-conscious jailor is only keen on nabbing him. The child is taught music by a pretty teacher (Raja Sulochana) and the song, “Konji konji pesi, becomes the link for the rescue of the child kept prisoner in a building. Unable to locate the child who is held captive, the music teacher goes around the town singing the song. On hearing the song, the child emerges from her hideout and utters the lines outwitting the guards. The child is rescued after a long fight between the two prisoners.

== Cast ==
* R. S. Manohar as Kannayiram
* P. S. Veerappa
* Rajasulochana as music teacher
* K. A. Thangavelu
* E. V. Saroja Seetharaman as Jailor
* ‘Master Sridhar as Kannayirams kid
* ‘Baby Savithiri as Jailors kid
* ‘Kallapart Natarajan
* ‘Socrates Thangaraj

== Soundtrack ==
{{Infobox album
| Name     = Kaithi Kannayiram
| Longtype = to Kaithi Kannayiram
| Type     = Soundtrack
| Genre    = Film soundtrack
| Artist   = K. V. Mahadevan
| Producer = K. V. Mahadevan Tamil
| Length   = 29:23
}}

The soundtrack album was composed by K. V. Mahadevan. The lyrics were written by A. Maruthakasi.

; Tracklist 
{{track listing
| extra_column  = Singer(s)
| all_lyrics    = A. Maruthakasi
| total_length  = 29:23

| title1     = Konji Konji Pesi&nbsp;- Happy
| extra1     = P. Susheela
| length1    = 03:58

| title2     = Sundelikkum
| extra2     = M. S. Rajeshwari
| length2    = 02:58

| title3     = Maanam Nenjile
| extra3     = Jamuna Rani
| length3    = 03:05

| title4     = En Kannai Konjam
| extra4     = Jamuna Rani
| length4    = 05:17

| title5     = Kaadhalai Sodhichu
| extra5     = Sirkazhi Govindarajan, Jamuna Rani
| length5    = 03:58

| title6     = Saala Mishtri
| extra6     = Sirkazhi Govindarajan
| length6    = 03:18

| title7     = Sangadam
| extra7     = Sirkazhi Govindarajan
| length7    = 02:55

| title8     = Konji Konji Pesi&nbsp;- Pathos
| extra8     = P. Susheela and Chorus
| length8    = 03:54
}}

== Reception ==
Kaithi Kannayiram fared well at the box office. Film historian Randor Guy said that the film will be remembered for the melodious music and the fine performances by Manohar, Veerappa, Seetharaman, Raja Sulochana and the child artistes. 

== References ==
 

 
 
 
 
 