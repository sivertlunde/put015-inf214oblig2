Rail Payanangalil
{{Infobox film 
| name           = Rail Payanangalil 
| image          = Rail Payanangalil.jpg
| caption        = 
| director       = Vijaya T. Rajendar 
| producer       = Mayilai Gurupatham
| writer         = Vijaya T. Rajendar
| screenplay     = Vijaya T. Rajendar Jyothi Rajeev Rajeev Sivaranjani
| music          = Vijaya T. Rajendar
| cinematography = C. S. Ravibabu
| editing        = R. Devarajan
| studio         = G. R. P. Arts
| distributor    = G. R. P. Arts
| released       =  
| runtime        = 135min
| country        = India  Tamil
}}
 1981 Cinema Indian Tamil Tamil film, Rajeev and Sivaranjani in lead roles. The film had musical score by Vijaya T. Rajendar.   

==Cast==
 
*Sreenath (debut in Tamil) Jyothi (debut in Tamil) Rajeev (debut)
*Sivaranjani (debut)
*Master Ananth (debut)
*Idichapuli Selvaraj Dilip
*J. Lalitha
*Veeraraghavan
*Dhanalakshmi
*Johnson
*Vijaya T. Rajendar in Cameo appearance
 

==Soundtrack==
The music was composed by Vijaya T. Rajendar.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Vasantham Paadi Vara || S. P. Balasubrahmanyam || Vijaya T. Rajendar || 04.48 
|- 
| 2 || Vasantha Kaalangal || Jayachandran || Vijaya T. Rajendar || 04.45 
|- 
| 3 || Ada Yaaro || S. P. Balasubrahmanyam || Vijaya T. Rajendar || 04.36 
|- 
| 4 || Noolumillai || T. M. Soundararajan || Vijaya T. Rajendar || 05.27 
|-  Janaki || Vijaya T. Rajendar || 04.48 
|- 
| 6 || Amaithikku Peyarthaan || T. M. Soundararajan || Vijaya T. Rajendar || 05.17 
|}

==References==
 

==External links==
*   
*  

 

 
 
 
 


 