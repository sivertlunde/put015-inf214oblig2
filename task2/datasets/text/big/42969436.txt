Suspected Person
{{Infobox film
| name =  Suspected Person
| image =
| image_size =
| caption =
| director = Lawrence Huntington
| producer = Warwick Ward
| writer =  Lawrence Huntington
| narrator = David Farrar   Robert Beatty
| music = Guy Jones
| cinematography = Ronald Anscombe   Günther Krampf
| editing = Flora Newton Associated British
| distributor = Pathé Pictures International (UK)   Producers Releasing Corporation (US)
| released = June 1942
| runtime = 78 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} David Farrar. The film was made at Welwyn Studios by Associated British, one of the two leading British studios of the era. It  was released in the United States in 1944 by Producers Releasing Corporation.  After accidentally discovering the money from a bank raid, an innocent man is hunted by the police.

==Cast==
* Clifford Evans as Jim Raynor  
* Patricia Roc as Joan Raynor   David Farrar as Inspector Thompson  
* Anne Firth as Carol  
* Robert Beatty as Franklin  
* Eric Clavering as Dolan 
* Leslie Perrins as Tony Garrett  
* Eliot Makeham as David 
* John Salew as Jones  
* William Hartnell as Saunders   Martin Benson 
* Terry Conlin
* Anthony Shaw

==References==
 

==Bibliography==
* Slide, Anthony. Banned in the U.S.A.: British Films in the United States and Their Censorship, 1933-1966. I.B.Tauris, 1998.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 