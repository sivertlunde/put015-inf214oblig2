Undu Hoda Kondu Hoda
{{Infobox film name           = Undu Hoda Kondu Hoda image          = image_size     = caption        = director       = Nagathihalli Chandrashekar producer       = K B Swamyshankar G Kumaraswamy writer         = Nagathihalli Chandrashekar narrator       = starring  Tara Maanu Tennis Krishna music          = Vijaya Bhaskar cinematography = S. Ramachandra editing        = Suresh Urs studio         = Padmamba Combines released       =   runtime        = 136 minutes country        = India language  Kannada
|budget         =
}}
 Kannada satire|satirical Tara in the lead role whilst Umesh, Tennis Krishna, Anjali Sudhakar, Vijay Kashi and Maanu feature in other prominent roles.  The film was produced by Padmamba Combines banner.

The films plot deals with a "cow" inspector played by Ananth Nag who comes to a village from city and plays his tricks on the innocent minds of the villagers and wins over them by cheating. The sequence that unfolds later are shown in a comical way. Nagathihalli won the Best story writer award at the Karnataka State Film Awards. 

==Cast==
* Ananth Nag  Tara 
* Anjali Sudhakar
* Tennis Krishna
* Umesh
* Vijay Kashi
* Maanu
* Shimoga Venkatesh
* Girija Lokesh
* Karibasavaiah
* Dingri Nagaraj
* Nagathihalli Chandrashekar guest appearance

==Soundtrack==
The music of the film was composed by Vijaya Bhaskar. All the lyrics except one were written by Nagathihalli Chandrashekar. A poem of K. S. Narasimhaswamy was included in the soundtrack.

{{Infobox album  
| Name        = Undu Hoda Kondu Hoda
| Type        = Soundtrack
| Artist      = Vijaya Bhaskar
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Sangeetha
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Nakka Haage Natisabeda
| lyrics1 	= K. S. Narasimhaswamy
| extra1        = Vani Jayaram, Rajkumar Bharathi
| length1       = 
| title2        = Bandano Bandano Bhagyava Thandano
| lyrics2 	= Nagathihalli Chandrashekar
| extra2        = B. R. Chaya, Vishnu
| length2       = 
| title3        = Lolalotte Ee Baduku
| lyrics3       = Nagathihalli Chandrashekar
| extra3 	= S. P. Balasubrahmanyam
| length3       = 
| title4        = Undu Hoda Kondu Hoda
| extra4        = Vani Jayaram
| lyrics4  	= Nagathihalli Chandrashekar
| length4       = 
}}

==Awards== Best Story Writer - Nagathihalli Chandrashekar

==References==
 

==External source==
*  

 
 
 
 
 
 
 