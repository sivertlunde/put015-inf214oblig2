James Blunt: Return to Kosovo
 
 

{{Infobox film
| name           = James Blunt: Return to Kosovo
| image          = 
| caption        = 
| director       = Steven Cantor
| producer       = Mikaela Beardsley   Steven Cantor   Mark Cann (co-producer)
| starring       = James Blunt
| music          = 
| editing        = Trevor Ristow
| distributor    = 
| released       =  
| runtime        = 50 minutes
| language       = English
}}
James Blunt: Return to Kosovo is a 2007 documentary film recorded in September 2006, when musician and former British Army Captain James Blunt returned to Kosovo to perform a concert for serving NATO troops, and to visit places and people he had encountered whilst serving in Kosovo in 1999.  The documentary was directed by Steven Cantor.     

==Synopsis==
The documentary interlaces the footage filmed in 2006 with personal videos taken by Blunt in 1999, as well as news footage from the Kosovo conflict.  Blunt was reunited with the three interpreters with whom he had worked when his unit was first sent to Kosovo. Together, they had intended to seek out families with whom Blunt and others had interacted; instead, they found abandoned and destroyed homes where these families had once lived. The group also returned to a mass grave that Blunts squadron had been instrumental in identifying; it was now a traditional cemetery, where most of the dead have been identified and gravestones with their likenesses have been erected. Blunt performed songs from his Back to Bedlam album, including "Youre Beautiful". Also included was his performance of No Bravery, a song about the conflict that Blunt wrote during his tour of duty in Kosovo.   

==Release and distribution==
Return to Kosovo had its world premier at the South by Southwest Festival in Austin, Texas on 10 March 2007.  In August 2007 the documentary was shown at the Dokufest film festival in Kosovo. {{cite web
  | title = Dokufest screening listings
  | url =http://dokufest.com/2007/repository/docs/dokufest_2007_web.pdf
  |format=PDF| accessdate = 26 January 2010}}   

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 

 