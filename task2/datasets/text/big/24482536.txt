Haunting Sarah
{{Infobox television film
| name           = Haunting Sarah
| image          = Haunting Sarah poster.jpg
| caption        = film poster
| director       = Ralph Hemecker
| producer       = Peter Sadowski Randy Sutter
| writer         = Screenplay:   Rick Roberts 
| music          = Joel Goldsmith
| cinematography = Christian Sebaldt
| editing        = Quincy Z. Gunderson
| studio         = Edelstein Company Von Zerneck Sertner Films
| distributor    = Hallmark Channel
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
}}

Haunting Sarah is a 2005 American television horror film written by Tony Phelan and Joan Rater and directed by Ralph Hemecker, and based upon the novel New Year’s Eve by Lisa Grunwald.  The film is about a woman who has lost her son and finds out her niece is in contact with his ghost|spirit. {{cite web
|url=http://ftvdb.bfi.org.uk/sift/title/810978
|title=Haunting Sarah
|publisher=British Film Institute
|accessdate=11 December 2009}}  {{cite news
|url=http://www.movievine.com/movies/article00242.shtml
|title=Just in time for Halloween : Haunting Sarah - a ghost story
|last=Lifetime
|date=September 23, 2005
|work=Lifetime (TV network)|Lifetime|publisher=movievine
|accessdate=10 December 2009}}  {{cite news
|url=http://www.tvguide.com/news/raver-24-haunting-36735.aspx
|title=Dual Role Doesnt Scare 24 Star
|last=Mitovich
|first=Matt Webb
|date=3 October 2005 TV Guide
|accessdate=10 December 2009}} 

==Plot==
Erica and Heather Rose ( ) and Heathers son David (Ryland Thiessen) grew up sharing an equally close bond, but then David, during a horrible accident in which he is struck by a car, dies. Erica anguishes over how she might be able to explain to her daughter Sarah that her companion David is dead, but the bond between the two children seems to extend past the grave, as Sarah reveals that David has already told her that he is gone. The following day at school Sarah hurts a boy from her class who had just been made that weeks class leader and told Sarah that "David cant be class leader because hes dead." After her parents pick Sarah up from school because of her acting out, she tells them nervously that she doesnt want them to drive over Creek Bridge and Ericas husband gets upset but finally pulls over. Later that day they find out there was an accident on that bridge and Sarah had drawn a picture of the accident because David had told her to. That day, Sarahs nanny notices that the girl has a strange bruise on the back of her neck and wonders what that could be. 
Erica and her husband decide that Erica and Sarah, along with the nanny, should to go stay at the familys county house to help Sarah forget about David. Upon arriving, Erica is overwhelmed with the places memories and starts crying because she misses her mother and Sarah gives her a hug, saying "She misses you too". Meanwhile in the city, Heather tries to go back to work but still seems to be troubled, so she decides to drive out to the country house to join her sister and niece. Rosie, the nanny, notices the mark on Sarahs neck again when she is rubbing sunscreen on her back. In the evening, she puts her to bed and finds out that Sarah is still in contact with David and tells her that David really isnt her friend. She gives Sarah a friendship bracelet to protect her. Rosie and Erica discuss the situation and Rosie thinks the bruises may be from Davids spirit looking to cross back over, holding onto the neck of a living person - which she refers to as hanting - and that David is indeed in contact with Sarah. Erica puts it off and tries to rationalize it. During the night, Erica, Heather and Rosie come rushing into Sarahs room at her screams and see that the wrist where she wears the bracelet is swollen and bloody, as if someone was trying to rip it off. The window blows open and knocks the lamp off the dresser.The twins decide it would be best to have Rosie go back to the city because of her beliefs. Before leaving, she asks Heather to put the bracelet under Sarahs mattress as it will protect her and that Erica doesnt need to know. Rosie takes off and Heather throws the bracelet out of the window.
Heather and Sarah start building a doll house as a memorial for David, but they dont tell Erica that. Erica has a bad dream and when she wakes up, Sarah is standing next to her bed and when Erica asks "Wheres aunt Heather?" Sarah replies "Dont worry about aunt Heather, its the baby". Just then, Erica notices theres blood on her night gown. She goes to the hospital and finds out shes 12 weeks pregnant and is put on 3 weeks bed rest. She starts getting jealous because Sarah is spending a lot of time with her sister building the doll house, but eventually calms down when she sees the beautiful doll house they built. Erica goes to check up on the baby and everything is fine. When they arrive at the house, Edgar, Ericas husband, is waiting for her to show her a house he put an offer on. The strange thing is that its an exact replica of the doll house. They move in. 
3 months later, Sarah is still acting strange, she never has an appetite, is always tired and doesnt make any friends at the new school and Erica is concerned. She takes Sarah to the psychologist, who notices the bruises. She wants to call social services because she thinks that Erica might be hurting her own daughter, but initiates contact with Heather first, who talks her out of it. She says that she will examine Sarah, but when she talks to Erica about it, the latter freaks out and tells Heather to leave her house immediately. On the way to her car, Heather turns around and sees Sarah holding a sign to the window saying "HES MAD". 
Erica goes up to Sarahs room to talk to her. She asks her what is troubling her and where she got the bruises. Sarah says shes not sure where the bruises come from and that she knows what is wrong but that she cant tell her mother. She then shows her mother the toy figure in her shelf that belonged to David (which fell of his rucksack on the street, which was why he ran back and got struck by the car that killed him). Erica, confused how the toy got there, takes it and throws it in the bin. Sarah says "You shouldnt have done that". Erica then goes to the library and looks up Hanting, reading that "Their life-force now drained by the Hant, the host slips into a coma and will die soon". Erica rushes to the school, but Sarah is not there and the teacher informs Erica that she had picked up Sarah and shows her the signed paper. Erica realizes that Heather mustve acted as her, drives to her house and breaks in as no one is answering the door. She checks Heathers timetable on her laptop and finds out that she has an appointment with a certain Frank Grigsby and drives to the address. She goes in and finds Heather and a man sitting on chairs, while Sarah is sitting on the floor. They are talking to Sarah, but calling her David, who says "I want to be born NOW!". Erica runs to her daughter, who suddenly flies towards her and attacks her, saying "You are not my mother. You bitch!". Heather and the man have to pull Sarah away from her struggling mother and Heather starts explaining that "You wouldve done the same in my position, I wanted to talk to David", but Erica punches her in the face and takes her daughter with her, just after the man asks Sarah to come back to her body.
Concerned, Erica calls Rosie after seeing the strong bruising on Sarahs back. Rosie explains, that David is hanging on to Sarah, buying time while waiting for Ericas baby to be born so that he can take its body and be reborn. They get rid of everything that belonged to David or has something to do with him. Erica goes down the stairs to the cellar, where the lights dont work. She finds the doll house, illuminated with the fairy lights. Rosie comes down, scaring Erica accidentally, and says that it is no wonder David has been so strong; he had a place to live all along in the doll house. Erica destroys and burns down the doll house with Rosies and Sarahs help and they throw every bit and piece that reminds them of David into the flames. Sarah throws in the last picture she has of him, saying "Its true that youre dead, David, Im sorry!", finally accepting the death of her friend. That night, Erica has a strange dream, in which she realises they hadnt burned everything of Davids; the toy she had thrown away is back in Sarahs room and she suddenly wakes up, rushing to the attic with her husband when they find Sarahs bed empty. They find her sitting in the top window and try to make her come back inside, but she wont listen and jumps off the roof. They rush to hospital, where a doctor tells the parents that Sarah didnt fracture her skull but suffered from severe head trauma. Heather turns up and Erica asks her if she thinks that David really wont let Sarah go unless he gets what he wants. When she answers with Yes, Erica says that they will give him what he wants to save Sarah. Heather injects Erica with a medication that will induce the birth of the child. But when Erica gives birth to a baby girl, both sisters start to get scared. Suddenly, Erica starts having more contractions, and the nurse says that they have to do an emergency c-section as there is a second baby which seems to have the umbilical cord tied around its neck. The surgery goes well and Erica has a perfectly healthy second baby, this time a baby boy. At the same moment the boy starts to cry, Sarah opens her eyes, showing that she survived and that David let go of her. Heather takes the boy in her arms and looks at him lovingly.
One year later, at their halloween party, the family seems to be happy and Sarah is in good health again. We find out that Erica has finally published her new book about twins, and admits to Heather that she believed in Davids presence all along, just like her. Sarah goes up to the baby boys crib and says "Hello David. Now, Im the one in charge.". And the film ends with her putting her halloween costume mask back on.

==Partial cast==
* Kim Raver as Erica Rose Lewis / Heather Rose Lord
* Niamh Wilson as Sarah Lewis
* Ryland Thiessen as David Lord
* Alison Sealy-Smith as Rosie Rick Roberts as Edgar Lewis
* Gordon Tanner as Richard Lord
* Terri Cherniak as Dr. Rachel Koening
* Marina Stephenson Kerr as Dr. Samantha Graton 
* Blake Taylor as Dr. Christopher Myman

==Reception==
The Futon Critic reported that Haunting Sarah drew 4 million viewers on its October 3, 2005 debut, marking it as the 15th most-watched program that week. {{cite web
|url=http://www.thefutoncritic.com/news.aspx?id=7008
|title=Development Update: October 12-14
|last=Futon Critic Staff 
|publisher=Futon Critic
|accessdate=11 December 2009}}   Roger Catlin of Hartford Courant that it was enjoyable to watch actress Kim Raver in the twin lead roles of Erika and Heather Rose. {{cite news
|url=http://pqasb.pqarchiver.com/courant/access/906091831.html?dids=906091831:906091831&FMT=ABS&FMTS=ABS:FT&type=current&date=Oct+03%2C+2005&author=ROGER+CATLIN%3B+Courant+TV+Critic&pub=Hartford+Courant&desc=KIM+RAVER+AS+TWINS+IS+FUN+TO+WATCH+IN+%60HAUNTING+SARAH&pqatl=google
|title=Kim Raver as twins is fun to watch in Haunting Sarah
|last=Catlin
|first=Roger
|date=October 3, 2005
|publisher=Hartford Courant
|accessdate=11 December 2009}}   Fearscene wrote that as it was a Lifetime movie, they did not expect much from it, but offered that they were intrigued by the characters of Erica and Heather Rose and that although the film seemed to try to go in too many directions, it was mildly entertaining. 

==Recognition==

===Awards===
*2006, For her role as Sarah Lewis in the film, Niamh Wilson won a 2006 Young Artist Award for "Best Performance in a TV Movie, Miniseries or Special - Supporting Young Actress" {{cite web
|url=http://www.youngartistawards.org/noms27.htm
|title=27th Annual Young Artists Awards
|publisher=Young Artist Awards
|accessdate=10 December 2009}} 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 