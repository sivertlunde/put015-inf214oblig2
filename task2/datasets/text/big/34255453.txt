Chinnabbayi
{{Infobox film
| name           = Chinnabbayi
| image          =
| alt            =  
| caption        =
| director       = K. Vishwanath
| producer       = M.Narasimha Rao
| writer         = K. Vishwanath     Satyanand  
| screenplay     = K. Vishwanath Venkatesh  Ramyakrishna Ravali
| music          = Ilayaraja
| cinematography = S. Gopal Reddy
| editing        = G.G. Krishna Rao
| studio         = Raasi Movies
| distributor    =
| released       = 17th October 1996
| runtime        = 2:23:48
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Tollywood film directed by K. Vishwanath and produced by M.Narasimha Rao on Raasi Movies. Starring Daggubati Venkatesh|Venkatesh, Ramyakrishna and Ravali played the lead roles and music composed by Ilayaraja.   The film was flop at the box office. 

==Cast==
  Venkatesh
* Ramyakrishna
* Ravali Indraja
* Satyanarayana
* Kota Srinivasa Rao
* Subhalekha Sudhakar
* Satya Prakash AVS
* Sakshi Ranga Rao
* Suthivelu Chitti Babu
* Misro
* Rajasimha
* Jeet Mohan Mitra
* Srividya Srilakshmi
* Siva Parvathi
* Nagaraja Kumari 
* Meena Kumari
* Parvathi 
* Master Anilraj 
 

==Soundtrack==
{{Infobox album
| Name       = Chinnabbai
| Tagline    =
| Type       = Soundtrack
| Artist     = Ilayaraja
| Cover      =
| Released   = 1996
| Recorded   =
| Genre      = Soundtrack
| Length     = 36:44
| Label      = T-Series
| Producer   = 
| Reviews    =
| Last album = Srikaram   (1996)
| This album = Chinnabbai    (1997)
| Next album = Antahpuram   (1998)
}}

Music composed by Ilayaraja. Music released on T-Series Audio Company.
{{Track listing
| extra_column   = Singer(s)
| lyrics_credits = yes
| total_length = 36:44

| title1         = Telusuko Telusuko Sirivennela Sitarama Sastry SP Balu, K. S. Chithra|Chitra, Malgudi Subha
| length1        = 5:45

| title2         = Vinnapalu Vinamani
| lyrics2        = Sirivennela Sitarama Sastry
| extra2         = SP Balu, S. P. Sailaja
| length2        = 5:21

| title3         = Ninna Chusina Udayam
| lyrics3        = Sirivennela Sitarama Sastry Sujatha
| length3        = 4:30

| title4         = Adagakandi
| lyrics4        = Sirivennela Sitarama Sastry
| extra4         = SP Balu,Sujatha
| length4        = 5:02

| title5         = Antha Randoy
| lyrics5        = Sirivennela Sitarama Sastry 
| extra5         = SP Balu,Chitra
| length5        = 6:18

| title6         = Navvulo Puttanu
| lyrics6        = Sirivennela Sitarama Sastry
| extra6         = SP Balu
| length6        = 4:06

| title7         = Jajimalli Tella Cheera  
| lyrics7        = Bhuvanachandra  
| extra7         = SP Balu,Chitra
| length7        = 5:42
}}

==References==
 

==External links==
*  

 
 
 
 


 