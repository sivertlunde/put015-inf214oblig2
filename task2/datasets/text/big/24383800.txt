Let's Go Native
 
{{Infobox film
| name           = Lets Go Native
| image          =
| image_size     =
| alt            =
| caption        =
| director       = Leo McCarey
| producer       = Leo McCarey Percy Heath George Marion Jr. James Hall
| music          = George Marion Jr. Richard A. Whiting
| cinematography = Victor Milner
| editing        =
| studio         =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 musical comedy film, directed by Leo McCarey and released by Paramount Pictures.

A very memorable, witty quote is when Jerry comments on being the only man on an island populated by women. Jerry: "It was one of the Virgin Islands, but it drifted."

The tagline was: "Paramounts wild, merry, mad hilarious farce!"

==Cast==
*Jack Oakie - Voltaire McGinnis
*Jeanette MacDonald - Joan Wood
*Richard "Skeets" Gallagher - Jerry, King of the Island James Hall - Wally Wendell William Austin - Basil Pistol
*Kay Francis - Constance Cook
*David Newell - Chief Officer Williams
*Charles Sellon - Wallace Wendell Sr.
*Eugene Pallette - Deputy Sheriff Careful Cuthbert
*Iris Adrian - Chorus Girl
*Virginia Bruce - Chorus Girl

==Soundtrack==
* "It Seems To Be Spring"
:Lyrics by George Marion Jr.
:Music by Richard A. Whiting
:Copyright 1930 by Famous Music Corp.
* "Lets Go Native"
:Lyrics by George Marion Jr.
:Music by Richard A. Whiting
:Copyright 1930 by Famous Music Corp.
* "My Mad Moment"
:Lyrics by George Marion Jr.
:Music by Richard A. Whiting
:Copyright 1930 by Famous Music Corp.
* "Ive Gotta Yen For You"
:Lyrics by George Marion Jr.
:Music by Richard A. Whiting
:Copyright 1930 by Famous Music Corp.
:Sung by Jack Oakie
* "Joe Jazz"
:Lyrics by George Marion Jr.
:Music by Richard A. Whiting
:Copyright 1930 by Famous Music Corp.
:Sung by Jack Oakie
* "Pampa Rose"
:Lyrics by George Marion Jr.
:Music by Richard A. Whiting
:Copyright 1930 by Famous Music Corp.
* "Dont I Do?"
:Lyrics by George Marion Jr.
:Music by Richard A. Whiting
:Copyright 1930 by Famous Music Corp.

==External links==
*  
*  
*  
*  
*  at the Jeannette and Nelson website
*  at OV Guide
*  at the NNDB database
*  at NYTimes
*  at Answers.com
*  at Google Books

 
 
 
 
 
 