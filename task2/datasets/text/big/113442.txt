The Right Stuff (film)
 
{{Infobox film
| name           = The Right Stuff
| image          = Right_stuff_ver1.jpg
| border         = yes
| alt               =
| caption        = Theatrical release poster
| director        = Philip Kaufman
| producer      = Irwin Winkler Robert Chartoff
| screenplay    = Philip Kaufman The Right Stuff by Tom Wolfe
| narrator      = Levon Helm
| starring = {{Plainlist|
* Charles Frank
* Scott Glenn
* Ed Harris
* Lance Henriksen
* Scott Paulin
* Dennis Quaid
* Sam Shepard
* Fred Ward
* Kim Stanley
* Barbara Hershey
* Veronica Cartwright
* Pamela Reed
}}
| music          = Bill Conti
| cinematography = Caleb Deschanel Douglas Stewart Tom Rolf
| studio         = The Ladd Company
| distributor  = Warner Bros.
| released     =  
| runtime      = 192 minutes  
| country      = United States
| language    = English Russian
| budget       = $19 - 27 million
| gross          = $21,192,102 
}}
The Right Stuff is a 1983 American  ,  December 18, 2013.  Retrieved: December 18, 2013. 
 

==Plot== Jack Ridley. Ridley cuts off part of a broomstick and tells Yeager to use it as a lever to help seal the hatch to the X-1, and Yeager becomes the first man to fly at supersonic speed, defeating the "demon in the sky".
 Scott Crossfield Gordon "Gordo" Virgil "Gus" Donald "Deke" Slayton, captains of the United States Air Force, are among the "pudknockers" that hope to also prove that they have "the Right Stuff". The tests are no longer secret, as the military soon recognizes that it needs good publicity for funding, and with "no bucks, no Buck Rogers". Coopers wife, Trudy, and other wives are afraid of becoming widows, but cannot change their husbands ambitions and desire for success and fame.
 alarms the United States government. Politicians such as Senator Lyndon B. Johnson and military leaders demand that NASA help America defeat the Russians in the new Space Race. The search for the first Americans in space excludes Yeager, because he lacks a college degree. Grueling physical and mental tests select the Mercury Seven astronauts, including John Glenn of the United States Marine Corps, Alan Shepard, Walter Schirra and Scott Carpenter of the United States Navy, as well as Cooper, Grissom and Slayton; they immediately become national heroes. Although many early NASA rockets explode during launch, the ambitious astronauts all hope to be the first in space as part of Project Mercury. Although engineers see the men as passengers, the pilots insist that the Mercury spacecraft have a window, a hatch with explosive bolts, and pitch-yaw-roll controls. However, Russia beats them on April 12, 1961 with the launch of Vostok 1 carrying Yuri Gagarin into space. The seven astronauts then decide theyve been waiting long enough, and to "get the show on the road".

Shepard is the first American to reach space on the 15-minute sub-orbital flight of Mercury-Redstone 3 on May 5. After Grissoms similar flight of Mercury-Redstone 4 on July 21, the capsules hatch blows open and quickly fills with water. Grissom escapes, but the spacecraft, overweight with seawater, sinks. Many criticize Grissom for possibly panicking and opening the hatch prematurely. Glenn becomes the first American to orbit the Earth on Mercury-Atlas 6 on February 20, 1962, surviving a possibly loose heat shield, and receives a ticker-tape parade. He, his colleagues, and their families become celebrities, including a gigantic celebration in the Sam Houston Coliseum to announce the opening of the Manned Space Center in Houston, despite Glenns wife Annie Glenn|Annies fear of public speaking due to a stutter.

Although test pilots at Edwards mock the Mercury program for sending "spam in a can" into space, they recognize that they are no longer the fastest men on Earth, and Yeager states that "it takes a special kind of man to volunteer for a suicide mission, especially when its on national TV." While testing the new Lockheed NF-104A, Yeager attempts to set a new altitude record at the edge of space but is nearly killed in a high-speed ejection when his engine failed. Though badly burned, after reaching the ground Yeager gathers up his parachute and walks to the ambulance, proving that he still has the Right Stuff.

The film ends with Coopers successful launch on May 15, 1963 on Mercury-Atlas 9, ending the Mercury program. As the last American to fly into space alone, he "went higher, farther, and faster than any other American ... for a brief moment, Gordo Cooper became the greatest pilot anyone had ever seen."

==Cast==
  
* Fred Ward as Gus Grissom, USAF
* Dennis Quaid as Gordon Cooper, USAF USMC
* USN
* Sam Shepard as Chuck Yeager, Colonel, USAF
* Barbara Hershey as Glennis Yeager
* Lance Henriksen as Walter Schirra, USN
* Veronica Cartwright as Betty Grissom
* Jane Dornacker as Nurse Murch
* Harry Shearer and Jeff Goldblum as the NASA recruiters sent to find astronaut candidates
* Kim Stanley as Pancho Barnes
* Pamela Reed as Trudy Cooper
* Scott Paulin as Donald K. Slayton, USAF
* Charles Frank as Scott Carpenter, USN
* Donald Moffat as U.S. Senator and Vice President Lyndon B. Johnson Jack Ridley, USAF and the narrator
* Mary Jo Deschanel as Annie Glenn Scott Wilson North American Aviation Company
 
* Kathy Baker as Louise Shepard
* Mickey Crocker as Marge Slayton
* Susan Kase as Rene Carpenter
* Mittie Smith as Jo Schirra
* Royal Dano as a Minister
* David Clennon as a Liaison Man John P. Ryan as the Head of the Manned Space Program
* Eric Sevareid as himself
* William Russ as Slick Goodlin
* Robert Beer as President Dwight D. Eisenhower
* Peggy Davis as Sally Rand
* John Dehner as Henry Luce
* Royce Grones as the first X-1 pilot, who was Jack Woolams
* Brigadier General Chuck Yeager, USAF (Ret) as Fred, the bartender at Panchos saloon
* Anthony Munoz as Gonzales
  Bill Dana (playing his character José Jiménez (character)|José Jiménez). Yuri Gagarin and Nikita Khrushchev are seen embracing at a review, along with Georgi Malenkov, Nikolai Bulganin, Kliment Voroshilov, and Anastas Mikoyan in attendance. Lyndon B. Johnson and John F. Kennedy are also seen.

==Production==
In 1979, independent producers Robert Chartoff and Irwin Winkler outbid Universal Pictures for the movie rights to Tom Wolfes book, Ansen, David and Katrine Ames. "A Movie with All The Right Stuff." Newsweek, October 3, 1983, p. 38.  hiring William Goldman to write the screenplay. At Winklers suggestion, Goldmans adaptation focused on the astronauts, entirely ignoring Chuck Yeager.  Goldman was inspired to accept the job because he wanted to say something patriotic about America in the wake of the Iran hostage crisis.
 Michael Ritchie and John Avildsen were originally attached but both fell through.  They approached director Philip Kaufman who agreed to make the film but did not like Goldmans script, disliking the emphasis on patriotism and wanting Yeager put back in the film.  Eventually Goldman quit the project in August 1980 and United Artists pulled out.

When Wolfe showed no interest in adapting his own book, Kaufman wrote a draft in eight weeks.  His draft restored Yeager to the story because "if youre tracing how the future began, the future in space travel, it began really with Yeager and the world of the test pilots. The astronauts descended from them". Wilford, John Noble.   The New York Times, October 16, 1983. Retrieved: December 29, 2008. 
 turnaround and The Ladd Company stepped in with an estimated $17 million.

Actor Ed Harris auditioned twice in 1981 for the role of John Glenn. Originally, Kaufman wanted to use a troupe of contortionists to portray the press corps, but settled on the improvisational comedy troupe Fratelli Bologna, known for its sponsorship of "St. Stupids Day" in San Francisco.  The director created a snake-like hiss to accompany the press corps whenever they appear, which was achieved through a sound combination of (among other things) motorized Nikon cameras and clicking beetles. 

Shot between March and October 1982, with additional filming continuing into January 1983, most of the film was shot in and around San Francisco, where a waterfront warehouse was transformed into a studio.    Location shooting took place primarily at the abandoned Hamilton Air Force Base north of San Francisco which was converted into a sound stage for the numerous interior sets.  No location could substitute for the distinctive Edwards Air Force Base landscape which necessitated the entire production crew move to the Mojave Desert for the opening sequences that framed the story of the test pilots at Muroc Army Air Field, later Edwards AFB. 

Yeager was hired as a technical consultant on the film. He took the actors flying, studied the storyboards and special effects, and pointed out the errors. To prepare for their roles, Kaufman gave the actors playing the seven astronauts an extensive videotape collection to study. 

The efforts at making an authentic feature led to the use of many full size aircraft, scale models and special effects to replicate the scenes at Edwards Air Force Base and Cape Canaveral Air Force Station.  According to special visual effects supervisor Gary Gutierrez, the first special effects were too clean and they wanted a "dirty, funky, early NASA look."  K Gutierrez and his team started from scratch, employing unconventional techniques—like going up a hill with model airplanes on wires and fog machines to create clouds, or shooting model F-104s from a crossbow device and capturing their flight with up to four cameras.  Avant garde filmmaker Jordan Belson created the background of the Earth as seen from high-flying planes and from orbiting spacecraft. 

Kaufman gave his five editors a list of documentary images the film required and they searched the country for film from NASA, the Air Force, and Bell Aircraft vaults.  They also discovered Russian stock footage not viewed in 30 years. During the course of the production, Kaufman met with resistance from the Ladd Company and threatened to quit several times.  In December 1982, 8,000 feet of film portraying John Glenns trip in orbit and return to Earth disappeared or was stolen from Kaufmans editing facility in Berkeley, California. The missing footage was never found but the footage was reconstructed from copies. Williams, Christian. "A Story that Pledges Allegiance to Drama and Entertainment."  Washington Post, October 20, 1983, A18. 

===Historical accuracy===
Although The Right Stuff  was based on historical events and real people, as chronicled in Wolfes book, some substantial dramatic liberties were taken. Neither Yeagers flight in the X-1 to break the sound barrier early in the film or his later, nearly-fatal flight in the NF-104A were spur-of-moment, capriciously decided events, as the film seems to imply - they actually were part of the routine testing program for both aircraft. Yeager had already test-flown both aircraft a number of times previously and was very familiar with them.   Jack Ridley had actually died in 1957,  even though his character appears in several key scenes taking place after that, most notably including Yeagers 1963 flight of the NF-104A.

The Right Stuff depicts Cooper arriving at Edwards in 1953, reminiscing with Grissom there about the two of them having supposedly flown together at the  , Grissom had already left Edwards and returned to Wright-Patterson Air Force Base, where he had served previously and was happy with his new assignment there. Grissom did not even know he was under consideration for the astronaut program until he received mysterious orders "out of the blue" to report to Washington in civilian clothing for what turned out to be a recruitment session for  NASA.   

While the film took liberties with certain historical facts as part of "dramatic license", criticism focused on one: the portrayal of Gus Grissom panicking when his Liberty Bell 7 spacecraft sank following splashdown. Most historians, as well as engineers working for or with NASA and many of the related contractor agencies within the aerospace industry, are now convinced that the premature detonation of the spacecraft hatchs explosive bolts was caused by mechanical failure not associated with direct human error or deliberate detonation by Grissom.  This determination had been made long before the film was completed,  and both Schirra and Gordon Cooper were critical of The Right Stuff for its treatment of Grissom.    However, Kaufman was closely following Tom Wolfes book, which focused not on how or why the hatch actually blew, but how NASA engineers and some of Grissoms colleagues (and even his own wife) believed he caused the accident; much of the dialogue in this sequence was taken directly from Wolfes prose.  

There were other inaccuracies as well, notably about the engineers who built the Mercury craft. 

===Film models===
  in Hutchinson, Kansas.]]

A large number of film models were assembled for the production; for the more than 80 aircraft appearing in the film, static mock-ups and models were used as well as authentic aircraft of the period. Farmer 1983, p. 49.  Lieutenant Colonel Duncan Wilmore, USAF (Ret) acted as the United States Air Force liaison to the production, beginning his role as a technical consultant in 1980 when the pre-production planning had begun. The first draft of the script in 1980 had concentrated only on the Mercury 7 but as subsequent revisions developed the treatment into more of the original story that Wolfe had envisioned, the aircraft of late-1940s that would have been seen at Edwards AFB were required. Wilmore gathered World War II era "prop" aircraft including:
* Douglas A-26 Invader
* North American P-51 Mustang
* North American T-6 Texan and
* Boeing B-29 Superfortress

The first group were mainly "set dressing" on the ramp while the Confederate Air Force (now renamed the Commemorative Air Force) B-29 FIFI (aircraft)|"Fifi" was modified to act as the B-29 "mothership" to carry the Bell X-1 and Bell X-1#X-1A|X-1A rocket-powered record-breakers. Farmer 1983, pp. 50–51. 

Other "real" aircraft included the early jet fighters and trainers as well as current USAF and United States Navy examples. These flying aircraft and helicopters included:
* Douglas A-4 Skyhawk
* LTV A-7 Corsair II
* North American F-86 Sabre
* Convair F-106 Delta Dart
* McDonnell Douglas F-4 Phantom II Sikorsky H-34 Choctaw
* Sikorsky SH-3 Sea King
* Lockheed T-33 Shooting Star
* Northrop T-38 Talon 

A number of aircraft significant to the story had to be recreated. The first was an essentially static X-1 that had to at least roll and even realistically "belch flame" which was accomplished by a simulated rocket blast from the exhaust pipes.  A series of wooden mock-up X-1s were used to depict interior shots of the cockpit, the mating up of the X-1 to a modified B-29 fuselage and bomb bay and ultimately to recreate flight in a combination of model work and live-action photography. The "follow-up" X-1A was also an all-wooden model. 

The U.S. Navys Douglas D-558-2 Skyrocket that Crossfield duelled with Yeagers X-1 and X-1A was recreated from a modified Hawker Hunter jet fighter. The climactic flight of Yeager in a Lockheed NF-104A was originally to be made with a modified Lockheed F-104 Starfighter but ultimately, Wilmore decided that the production had to make do with a repainted Luftwaffe F-104G, which lacks the rocket engine of the NF-104. 

Wooden mock-ups of the Mercury space capsules also realistically depicted the NASA spacecraft and were built from the original mold. 

For many of the flying sequences, scale models were produced by USFX Studios and filmed outdoors in natural sunlight against the sky. Even off-the-shelf plastic scale models were utilized for aerial scenes. The X-1, F-104 and B-29 models were built in large numbers as a number of the more than 40 scale models were destroyed in the process of  filming.  The blending together of miniatures, full-scale mock-ups and actual aircraft was seamlessly integrated into the live-action footage. The addition of original newsreel footage was used sparingly but to effect to provide another layer of authenticity. 

===MPAA Rating===
The film was originally rated "R" (Restricted, which means no one under 17 admitted) by the Motion Picture Association of America because of some strong language (the word "fuck" is used 5 times, which meant a near-impossible chance of it receiving anything short of an "R" rating) a scene of implied masturbation and other hard content; but it was given a "PG" rating on appeal (the PG-13 rating did not exist then; it was created the year after this film was released). 

==Reception==

===Box office=== world premiere on October 16, 1983, at the Kennedy Center in Washington, D.C., to benefit the American Film Institute. Morganthau, Tom and Richard Manning. "Glenn Meets the Dream Machine." Newsweek, October 3, 1983, p. 36.  Arnold, Gary. "The Stuff of Dreams."  Washington Post, October 16, 1983, p. G1.  It was given a limited release on October 21, 1983, in 229 theaters, grossing $1.6 million on its opening weekend. It went into wide release on February 17, 1984, in 627 theaters where it grossed an additional $1.6 million on that weekend.

As part of the promotion for the film, Veronica Cartwright, Chuck Yeager, Gordon Cooper, Scott Glenn and Dennis Quaid appeared in 1983 at ConStellation, the 41st World Science Fiction Convention in Baltimore. 

===Reviews=== At the Movies, also named The Right Stuff the best film of 1983, and said "Its a great film, and I hope everyone sees it." Siskel also went on to include The Right Stuff at #3 on his list of the best films of the 1980s, behind Shoah (film)|Shoah and Raging Bull. 

In his review for  , wrote, "The movie is obviously so solid and appealing that its bound to go through the roof commercially and keep on soaring for the next year of so".  In his review for  , October 21, 1983. Retrieved: December 29, 2008  Pauline Kael wrote, "The movie has the happy, excited spirit of a fanfare, and its astonishingly entertaining, considering what a screw-up it is". Kael, Pauline. "The Sevens". The New Yorker, October 17, 1983. 

Yeager said of the film: "Sam   is not a real flamboyant actor, and Im not a real flamboyant-type individual ... he played his role the way I fly airplanes".  Deke Slayton said that none of the film "was all that accurate, but it was well done". Bumiller, Elisabeth and Phil McCombs. "The Premiere: A Weekend Full of American Heroes and American Hype." Washington Post, October 17, 1983,  p.  B1.  Slayton later described the film as being "as bad as the book was good, just a joke".  Walter Schirra said, "They insulted the lovely people who talked us through the program - the NASA engineers. They made them like bumbling Germans".  Scott Carpenter felt that it was a "great movie in all regards". 
 Beemans chewing gum from his best friend, Jack Ridley. 
 Senator John Democratic nomination for President of the United States.

==Awards and nominations== Best Film Best Original Best Sound Mark Berger, Tom Scott, David MacMillan).   oscars.org. Retrieved: October 10, 2011. 

The film was also nominated for  . Retrieved: January 1, 2009.  The movie was also nominated for the Hugo Award in 1984 for Best Dramatic Presentation. 

==Media==
On June 23, 2003, Warner Bros. released a two-disc   format. The extras are in standard DVD format.

In addition, the British Film Institute published a book on The Right Stuff by Tom Charity in October 1997 that offered a detailed analysis and behind-the-scenes anecdotes.

==Soundtrack==
The soundtrack to The Right Stuff was released on September 20, 2013.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 37:31 

| title1          = Breaking The Sound Barrier
| length1         = 4:46
| extra1          = Bill Conti

| title2          = Mach I
| length2         = 1:23
| extra2          = Bill Conti

| title3          = Training Hard / Russian Moon
| length3         = 2:17
| extra3          = Bill Conti

| title4          = Tango
| length4         = 2:20
| extra4          = Bill Conti

| title5          = Mach II
| length5         = 1:58
| extra5          = Bill Conti

| title6          = The Eyes Of Texas Are Upon You / The Yellow Rose Of Texas / Deep In The Heart Of Texas / Dixie
| length6         = 2:50
| extra6          = Bill Conti

| title7          = Yeager and the F104
| length7         = 2:26
| extra7          = Bill Conti

| title8          = Light This Candle
| length8         = 2:45
| extra8          = Bill Conti

| title9          = Glenns Flight
| length9         = 5:08
| extra9          = Bill Conti

| title10         = Daybreak in Space
| length10        = 2:48
| extra10         = Bill Conti

| title11         = Yeagers Triumph
| length11        = 5:39
| extra11         = Bill Conti

| title12         = The Right Stuff (Single)
| length12        = 3:11
| extra12         = Bill Conti

}}

==See also==
* Astronaut
* Flight airspeed record
* Test pilot

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Buckbee, Ed and Walter Schirra.   Burlington, Ontario: Apogee Books, 2005. ISBN 1-894959-21-3.
* Charity, Tom. The Right Stuff (BFI Modern Classics). London: British Film Institute, 1991. ISBN 0-85170-624-X.
* Bill Conti|Conti, Bill (with London Symphony Orchestra). The Right Stuff: Symphonic Suite; North and South: Symphonic Suite. North Hollywood, California: Varèse Sarabande, 1986  .
* Cooper, Gordon. Leap of Faith. New York: Harper Collins Publishers, 2000. ISBN 0-06-019416-2.
* Farmer, Jim. "Filming the Right Stuff." Air Classics, Part One: Vol. 19, No. 12, December 1983, Part Two: Vol. 20, No. 1, January 1984.
* Glenn, John. John Glenn: A Memoir. New York: Bantam, 1999.  ISBN 0-553-11074-8.
* Goldman, William. Which Lie Did I Tell?: More Adventures in the Screen Trade. New York: Vintage Books USA, 2001. ISBN 0-375-70319-5.
* Hansen, James R.  . New York: Simon & Schuster, 2005. ISBN 0-7432-5631-X.
* Wolfe, Tom. The Right Stuff. New York: Bantam, 2001. ISBN 0-553-38135-0.
* Slayton, Deke and Michael Cassutt. Deke!  U.S. Manned Space: From Mercury to the Shuttle. New York: Tom Doherty Associates, 1994. ISBN 0-312-85503-6.
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 