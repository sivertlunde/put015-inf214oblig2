Fleur bleue (The Apprentice)
{{Infobox film
| name           = The Apprentice
| image          = The Apprentice.jpg
| image_size     = 
| caption        = 
| director       = Larry Kent
| producer       = Donald Brittain
| writer         = Larry Kent Edward Stewart
| starring       = Susan Sarandon Steve Fiset Celine Bernier Jean-Pierre Cartier Carole Laure Gerard Parkes
| cinematography = Jean-Claude Labrecque
| editing        = John Broughton
| distributor    = Somerville House Distribution (DVD)
| released       = September 10, 1971 (Canada) July 1996 (USA) 
| runtime        = 81 min  
| country        = Canada
| language       = French English
| budget         = 
}}
 English as English and dubbed for the appropriate audience.

==Plot==
Jean-Pierre (Fiset) is a working class young Francophone with big dreams.  He comes under the tutelage of his older best friend, a con-man and bank robber named Dock (Jean-Pierre Cartier), who starts teaching him the secrets of his trades, and making him an accomplice in his crimes.  He is dating Docks virginal sister, who is also active in the Quebec separatist movement.
 commercial shoot, Anglophone model.  After he is fired from the shoot for explaining the English meaning of the French name of the product she is supposed to be selling, she starts going out with him out of a sense of guilt, and they soon become lovers.  However, she is a sexual libertine with no intention of being monogamous, and this soon strains their relationship.

Eventually, the strain of keeping up with his two girlfriends wears on him and he decides to break up with Elizabeth.  However, his clumsy attempts to break up in English (of which he does not have a fluent command) merely results in their spending the night together.  He then decides to break up with Docks sister, who realizing that he is going back to Elizabeth because of their sexual relationship, throws herself at him.

Eventually, Jean-Pierres criminal activities catch up with him, and everything ends tragically.

==Place in Canadian film history==
 
Fleur bleue is one of the very few films to deal with the strain between Anglophone and Francophone relationships in the city of Montreal in the late 1960s and early 1970s.  At the time the film was made, Montreal was largely segregated into French speaking areas in the east, and English speaking areas in the west, with the two groups rarely interacting with each other.  Jean-Pierre and Elizabeth both are from Montreal, but neither has a working command of the others language - Jean-Pierres English is non-fluent, and Elizabeths French is non-existent.  

In addition to the strain of his relationships and his criminal career, Jean-Pierre is also dealing with a society which, while marginally under French political control, was largely under English economic control.  This film was made just after department stores such as Eatons required their Francophone clerks to converse with customers in English only, even if the customer was a Francophone as well.  Jean-Pierre, rightly or wrongly, blames his economic circumstances on Anglophones, who are the only ones who can provide him with an honest, but low paying, job.  His girlfriend has come to the conclusion that separation from Canada is the only solution to the problems plaguing Francophones.  Dock is convinced that it doesnt matter who is in charge - people like him will suffer nevertheless, so its everyone for themselves.  Elizabeth, who is relatively well off, doesnt acknowledge the problems that exist for all the Francophones with whom she shares her city .

==External links==
* 
* 

 
 
 
 
 
 
 
 
 