For the Love of Mary
{{Infobox film
| name           = For the Love of Mary
| image          = For the Love of Mary Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Frederick de Cordova
| producer       = Robert Arthur
| screenplay     = Oscar Brodney
| starring       = {{Plainlist|
* Deanna Durbin
* Edmond OBrien Don Taylor
* Jeffrey Lynn
}}
| music          = Frank Skinner
| cinematography = William H. Daniels
| editing        = Ted J. Kent
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Don Taylor, and Jeffrey Lynn.       Written by Oscar Brodney, the film is about a young woman who takes a job at the White House as a switchboard operator and soon receives help with her love life from Supreme Court justices and the President of the United States. For the Love of Mary was the last film by Deanna Durbin, who withdrew from the entertainment business the following year to live a private life in France. 

==Plot== Don Taylor), a fishing expert who insists on speaking to the President about a political issue involving a small Pacific island. After hanging up on him twice, Mary spends the rest of her day fielding calls from various Supreme Court justices who attempt to reconcile her with her former fiancée, Phillip Manning (Jeffrey Lynn), a Justice Department attorney.
 Harry Davenport) at a restaurant to discuss her breakup with Phillip, who is also there. After resisting their efforts to reunite her with Phillip, Mary tells Phillip that she broke their engagement not because she saw him with another woman, but because she was not jealous about it. Their conversation about her hard day at the White House fending off calls from the "fish peddler" is overheard by David, who assures Mary that he will speak to the President, despite her interference.
 Ray Collins), down to check on her condition and offers her a paper bag to breath into. When Phillip calls expecting to drive her to Justice Peabodys party that night, she declines, not wanting to resume their relationship. Later, as she is leaving work, Mary must drive David off the White House grounds in order to prevent his arrest. Mary asks David to escort her to the party, offering to introduce him to the Presidents secretary in exchange for the favor.

Meanwhile, the President, having overheard Mary telling Phillip that she would rather stay home than attend the party with him, sends Lt. Tom Farrington (Edmond OBrien), a naval aide at the White House, to escort Mary. At the party, they cause a stir and not a little jealousy in Phillip. After a pleasant evening of singing around the piano, Tom escorts Mary home, where she notices theyre being watched. After she kisses Tom goodnight and he leaves, she is confronted by David, whos been waiting on the porch all night. David kisses a startled Mary, who starts to hiccup again.

The next day, when the President learns that Mary was upset about not keeping her date with David, he calls the fishing expert himself to express his regrets. Later over lunch, Mary tells David she will arrange a meeting for him with Phillip who can help him with his political issue. That afternoon at the Peppertree home, Tom arrives with presidential orders to take Mary to a White House movie screening. When Phillip learns that Mary is with Tom, he questions David about his relationship with Mary. The frustrated marine biologist announces he is leaving town and that everyone in Washington seems to have a "Mary Peppertree fixation".

Meanwhile, Toms friend, newspaper publisher Samuel Litchfield (Frank Conroy), complains to Elwood about Toms involvement with Mary, a mere switchboard operator. When Mary and Tom arrive together at the restaurant, the restaurant owner, Gustav Heindel (Hugo Haas), tells Elwood he saw Mary kissing David. Elwood decides to handle the matter with the Navy personally. Phillip offers to put the Justice Department on the case and promises to clear up the matter in two days. That night, at Elwoods request, David takes Mary out on a date in Toms place. After kissing David, Mary breaks out in hiccups, a sign that she is in love. When Phillip and Tom show up, a jealous David leaves in anger, believing she is also seeing them.

The next morning, Mary receives calls from Gustav and the Supreme Court justices congratulating her on her engagement to Phillip, and a call from the President congratulating her on her engagement to Tom! When Elwood learns that David is not technically a citizen of the United States, he has the young man arrested for illegally entering the country. Elwood soon discovers, however, that the Pacific island on which David was born and holds a deed is now home to a strategic military base, and if David is declared an alien, the Navy could be forced to move. Later that day, everyone arrives at Gustavs restaurant to try and resolve the issue. After meeting with the Presidents advisors, David fashions a Senate resolution for the American annexation of his island if both Phillip and Tom are given appointments far from Washington, and both he and Gustav are made United States citizens. The government readily agrees, and when Mary calls the President with the good news, David interrupts her conversation with a kiss, causing both of them to hiccup.

==Cast==
 
* Deanna Durbin as Mary Peppertree
* Edmond OBrien as Lt. Tom Farrington Don Taylor as David Paxton
* Jeffrey Lynn as Phillip Manning Ray Collins as Harvey Elwood
* Hugo Haas as Gustav Heindel Harry Davenport as Justice Peabody
* Griff Barnett as Timothy Peppertree
* Katharine Alexander as Miss Harkness James Todd as Justice Van Sloan
* Morris Ankrum as Adm. Walton
* Frank Conroy as Samuel Litchfield
* Leon Belasco as Igor
* Louise Beavers as Bertha
* Raymond Greenleaf as Justice Williams Charles Meredith as Justice Hastings
* Adele Rowland as Mrs. Peabody Mary Adams as Marge
* Adrienne Marden as Hilda
* Beatrice Roberts as Dorothy
* Harry Cheshire as Col. Hedley
* Donald Randolph as Asst. Attorney General William Gould as Sen. Benning    

==Production==
The working titles of this film were White House Girl and Washington Girl.    The music was written by Frank Skinner. The songs were conducted by Edgar Fairchild and staged by Nick Castle. 

==Reception==
In his review for Allmovie, Craig Butler called it "a moderately entertaining mini-musical simply because of the charming presence of Deanna Durbin."    Butler saw the film as an unsuccessful attempt at creating a romantic screwball comedy of the classic type, lacking the necessary keen eye on detail, organization, character, and wit. According to Butler, the screenplay lacks all of these. The main "gimmick", of the President of the United States getting personally involved in straightening out the love life of a switchboard operator, is "obnoxiously cute and simply too unbelievable."  Butler does applaud de Cordovas direction and the acting—Harry Davenport is especially good as a Supreme Court justice—but it is not enough to salvage a poor script and unbelievable story. Butler concludes:
 

In his 1948 review for The New York Times, Bosley Crowther called the film "a sly piece of propaganda against the administrators of our government in Washington."    Crowther explained:
 
According to Crowther, the filmmakers, whom he called "propagandists", manipulate the viewer into drawing negative conclusions about Washington and its leaders. The film also fails on an entertainment level, with "painful attempts at humor" and "flat jokes" about the "woefully lax and quixotic operations of White House officialdom." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 