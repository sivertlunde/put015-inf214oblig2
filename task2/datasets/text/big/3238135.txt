House of the Dead (film)
{{Infobox film
| name = House of the Dead
| image = Houseofthedeadposter.jpg
| writer = Mark A. Altman Dan Bates
| starring = Jonathan Cherry Tyron Leitso Clint Howard Ona Grauer Ellie Cornell Jürgen Prochnow
| director = Uwe Boll
| producer = Uwe Boll Wolfgang Herold
| distributor = Artisan Entertainment
| released =  
| runtime = 90 minutes
| country = Germany Canada United States
| language = English
| budget = $12 million   
| gross = $13,818,181 
|}}
 same name produced by Sega. The film was directed by Uwe Boll. This film featured Erica Durance in one of her earliest roles before she became known for playing Lois Lane on Smallville. 

==Plot ==
The film begins with two college kids named Simon ( ), Karma (Enuka Okuma) and Cynthia (Sonya Salomaa). Karma has a crush on Simon, Simon has a crush on Alicia, and Cynthia is Gregs girlfriend. When the five arrive at the dock, they find that they are late and the boat that is supposed to take them to the island has already left. They get a ride on a boat with a man named Victor Kirk (Jürgen Prochnow) and his first mate Salish (Clint Howard). A cop named Jordan Casper (Ellie Cornell) tries to stop them from leaving, being that Kirk is a smuggler, but they leave anyway.  

They soon arrive at the island, but the site of the rave is completely deserted. The place is a complete mess. Alicia, Karma and Simon leave the site to go find anybody around while Cynthia and Greg stay behind. Greg and Cynthia are about to make out in a tent, but Greg leaves the tent to urinate. Alone in the tent Cynthia is killed by a group of zombies. Meanwhile, Alicia, Karma and Simon find an old house. Inside the house they find Rudy (Jonathan Cherry), Liberty (Kira Clavell) and Hugh (Michael Eklund) who tell them that zombies attacked the rave, killing everyone. Alicia and Rudy used to date and Liberty was a dancer at the rave. The six leave the house to go get Greg and Cynthia. Meanwhile, the zombies kill Salish when he is alone in the forest. 

Alicia, Rudy, Karma, Simon, Liberty and Hugh return to the rave site and find Greg. Cynthia comes out from behind the tree, but she is now a zombie. She kills Hugh but is killed when Casper arrives and shoots her. They form a plan to return to Kirks boat and leave the island. When they return to the beach they find only zombies on Kirks boat. Casper and Greg leave the group to go find help, but Greg is killed in the forest. Kirk later takes the group to a spot in the forest where he has hidden a box full of guns. Once everyone is armed, they decide to head back to the house. The front of the house is filled with zombies. Liberty and Casper are killed in the ensuing fight and Alicia, Rudy, Kirk, Karma and Simon manage to take shelter inside the house. 

When Kirk is alone, he hears Salish whistling outside of the house. He goes outside and sees Salish as a zombie. Kirk commits suicide with a stick of dynamite, also blowing up the entrance to the house. The remaining four lock themselves in a lab in the house, but the zombies break in. Karma finds a hatch in the floor. She, Alicia and Rudy go down into the hatch and Simon shoots a barrel of gun powder blowing up the house, a lot of zombies, and himself. Alicia, Rudy and Karma find themselves in underground tunnels. They make their way through the tunnels, but Karma is killed by zombies as she attempts to hold them off as Rudy and Alicia flee. 

Alicia and Rudy are captured by an evil Spaniard named Castillo who injected himself with immortality serum many years ago and created the first zombie. Alicia and Rudy escape the tunnels, blowing them up in the process, but are followed by Castillo. Alicia gets into a sword fight with Castillo, and he stabs her in the heart. Rudy manages to decapitate Castillo and thinks the fight is over. Castillo is in fact still alive and his headless body begins to strangle Rudy. Alicia who is barely alive gets up and crushes the head under her foot which finally kills him. She then dies. Rudy and Alicia are rescued by a team of agents. The agents ask for Rudys last name, to which he responds with The House of the Dead (video game)|"Curien".  The ending narration reveals that Rudy gave Alicia the immortality serum and that is why she is alive. The film ends with the two survivors returning home.

==Characters==
*Rudy (Jonathan Cherry), the male protagonist who is on the island when the zombies attack. 
*Alicia (Ona Grauer), the female protagonist and Rudys ex-girlfriend. 
*Karma (Enuka Okuma), Alicias close friend, who has a crush on Simon. 
*Simon (Tyron Leitso), friend of Alicia.
*Greg (Will Sanderson), Cynthias boyfriend. 
*Salish (Clint Howard), is Kirks first mate who always dresses in a yellow fishermans coat and hat.
*Captain Victor Kirk (Jürgen Prochnow), ex-navy and a smuggler who helps Alicias friends to get to the island. 
*Jordan Casper (Ellie Cornell), Kirks nemesis. She dies when she loses her legs and bleeds to death. 
*Hugh (Michael Eklund), a nerd who records the zombie attack on the rave on his camcorder. 
*Liberty (Kira Clavell), an Asian-American rave-girl. 
*Castillo (David Palffy), the creator of the zombies and the main antagonist of the film.
*Cynthia (Sonya Salomaa), Gregs girlfriend
*Johanna (Erica Durance), a party girl (Credited as Erica Parker)

==Reception==
The film was panned by the critics and Rotten Tomatoes ranked the film 41st in the 100 worst reviewed films of the 2000s, with a rating of 4% based on 54 reviews. IGN Movies, however, gave it three out of five stars, citing it as "an unabashed B-movie that does an incredibly decent job with a limited budget, unknown cast, and routine storyline."  In 2009, Time (magazine)|Time listed the film on their list of top ten worst video games movies. 

==Directors cut==
A directors cut of the film was released on DVD on 9 September 2008.  The new version "features new dialogue, alternative takes, pop up commentary and animation from the original video game." 

==References==
 


==External links==
*  
*   at Internet Movie Database
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 










 