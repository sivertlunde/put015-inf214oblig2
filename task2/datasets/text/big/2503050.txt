Bush Christmas
 
 
 
{{Infobox film
| name           = Bush Christmas
| image          = 
| image size     =
| caption        = 
| director       = Henri Safran
| producer       =
| writer         = Ted Roberts
| narrator       = John Howard Mark Spain James Wingrove The Bushwackers
| cinematography =
| editing        =
| distributor    = Barron Films Umbrella Entertainment
| released       = 1983
| runtime        = 91 minutes
| country        = Australia
| language       = English
| budget         = A$950,000 
| gross = $122,035 (Australia)
| preceded by    =
| followed by    = the same The Bushwackers.

==Plot== John Howard) find out about the horse and steal it, escaping into the nearby mountain range.
 Mark Spain), and their English cousin Michael (James Wingrove), saddle up their own horses and go after the crooks on their own. They are assisted by Manalpuy, a local Aboriginal who works on the farm.

==Production==
At one stage Howard Rubie was announced as director and he was involved in casting. However for a time it seemed the film might not go ahead so Rubie accepted a chance to direct The Settlement instead.  David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p341-342 

The movie was shot in Queensland in Beaudesert. Funding came in part from the Australian Film Commission, the Film and TV Institute (WA) and Queensland Film Corporation. 

==Release==

===Box office===
Bush Christmas grossed $122,035 at the box office in Australia,  which is equivalent to $322,172
in 2009 dollars.

===Home Media===
Bush Christmas was released on DVD by Umbrella Entertainment in December 2012. The DVD is compatible with all region codes and includes special features such as the theatrical trailer, press kit stills and the script.   

==See also==
*List of Australian films
*Cinema of Australia

==References==
 

==External links==
*  
* 
*  at Oz Movies
 
 
 
 
 
 
 