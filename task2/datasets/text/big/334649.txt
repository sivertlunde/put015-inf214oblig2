Romeo and Juliet (1936 film)
{{Infobox film
| name           = Romeo and Juliet
| image          = Romeo_juliet_movieposter.jpg
| image_size     = 220
| caption        = 1936 US Theatrical Poster
| director       = George Cukor
| producer       = Irving Thalberg
| writer         =  
| starring       =  
| music          = Herbert Stothart
| cinematography = William H. Daniels   
| editing        = Margaret Booth
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = $2,066,000 Glancy, H. Mark "When Hollywood Loved Britain: The Hollywood British Film 1939-1945" (Manchester University Press, 1999)   . 
| gross          =    
}} 1936 American the play Leslie Howard Romeo and Norma Shearer as Juliet Capulet|Juliet.    

The New York Times selected the film as one of the "Best 1,000 Movies Ever Made," calling it "a lavish production" which "is extremely well-produced and acted." 

==Cast== Leslie Howard Romeo
*Norma Juliet
*John Barrymore as Mercutio Nurse
*Basil Rathbone as Tybalt
*C. Aubrey Smith as Lord Capulet
*Andy Devine as Peter, servant to Juliets Nurse
*Conway Tearle as Escalus - Prince of Verona Paris - Young Nobleman Kinsman to the Prince
*Henry Kolker as Friar Laurence
*Robert Warwick as Lord Montague
* Virginia Hammond as Lady Montague  Reginald Denny as Benvolio - Nephew to Montague and Friend to Romeo
*Violet Kemble-Cooper as Lady Capulet Fred Graham, Ronald Howard, Lon McCallister, Ian Wolfe 

==Production==
 
=== Development === the depression. A Midsummer Nights Dream that Mayer, not to be outdone, gave Thalberg the go-ahead. 

=== Pre-production === Carpaccio and Gozzoli were Harvard and William Strunk, Jr. of Cornell University|Cornell) were flown to the set, with instructions to criticise the production freely. Brode, p.44  

=== Production ===
The film includes two songs drawn from other plays by Shakespeare: "Come Away Death" from  , who was known as "the womens director". Thalbergs vision was that the performance of Norma Shearer, his wife, would dominate the picture. 
 Leslie Howard and Norma Shearer, and an elderly John Barrymore as a stagey Mercutio decades out of date." Orgel, p.91  Barrymore was in his late fifties, and played Mercutio as a flirtatious tease. Tatspaugh, p.138  Romeo wears gloves in the balcony scene, and Juliet has a pet fawn. Tatspaugh, p.136  Tybalt is usually portrayed as a hot-headed troublemaker, but Basil Rathbone played him as stuffy and pompous. 

Thalberg cast screen actors, rather than stage actors, but shipped-in East Coast drama coaches (such as Frances Robinson Duff) to coach Shearer. In consequence, actors previously adored for their naturalism gave stilted performances.  The shoot extended to six months, and the budget reached $2 million, MGMs most expensive sound film up to that time. 

Like most Shakespearean filmmakers, Cukor and his screenwriter  , including three sequences featuring Friar John in Mantua. In contrast, the role of Friar Laurence (an important character in the play) is much reduced.  A number of scenes are expanded as opportunities for visual spectacle, including the opening brawl (set against the backdrop of a religious procession), the wedding and Juliets funeral. The party scene, choreographed by Agnes de Mille, includes Rosaline (an unseen character in Shakespeares script) who rebuffs Romeo.  The role of Peter is enlarged, and played by Andy Devine as a faint-hearted bully. He speaks lines which Shakespeare gave to other Capulet servants, making him the instigator of the opening brawl.  
 arcadian scene, complete with a pipe-playing shepherd and his dog; the livelier Juliet is associated with Capulets formal garden, with its decorative fish pond. 

== Premiere== MGM producer Irving Thalberg, husband of Norma Shearer, died at age 37. The stars in attendance were so grief-stricken that publicist Frank Whitbeck, standing in front of the theater, abandoned his usual policy of interviewing them for a radio broadcast as they entered and simply announced each one as they arrived.   

== Reception==
===Box Office===
According to MGM records the film earned $2,075,000 world wide but because of its high production cost lost $922,000. 
===Critical Reaction=== Franco Zeffirelli version (with Olivia Hussey and Leonard Whiting) an equal rating of three-and-a-half stars. 

Today, the most noticeable element of the film, aside from the quality of the acting, is that nearly every actor in it is too old for his or her role, especially those who are playing teenagers. Leslie Howard, as Romeo, was forty-three when he made the film. John Barrymore, who plays Mercutio, was fifty-four when he made the film and, with his looks aged by years of drinking, could not avoid appearing old enough to be Romeos father. Even   and the Mediterranean into it." 

==Awards==
{| class="wikitable"
|-
! Academy Award nomination !! Nominee(s)
|- Best Picture || Irving Thalberg
|- Best Supporting Actor || Basil Rathbone
|- Best Actress || Norma Shearer
|- Best Art Direction || Cedric Gibbons, Fredric Hope, Edwin B. Willis
|}

==See also==
* Romeo and Juliet on screen

==References==
 

==External links==
*  
*  
*  
*   at Virtual History
* 

  
 

 
 
 
 
 
 
 
 
 
 