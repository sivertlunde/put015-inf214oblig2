Boomerang Bill
{{infobox film
| name           = Boomerang Bill
| image          = Boomerang Bill - newspaperad - 1922.jpg
| imagesize      =
| caption        = A newspaper advertisement
| director       = Tom Terriss
| producer       = William Randolph Hearst (*for Cosmopolitan Productions)
| based on       =  
| writer         = Tom Terriss (scenario)
| starring       = Lionel Barrymore Marguerite Marsh
| cinematography = Al Ligouri
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States Silent (English intertitles)
}}
Boomerang Bill is an extant 1922 American silent crime melodrama film produced by Cosmopolitan Productions and distributed through Paramount Pictures. It was directed by Tom Terriss and stars veteran actor Lionel Barrymore. It is preserved incomplete at the Library of Congress and George Eastman House.       

==Cast==
*Lionel Barrymore - Boomerang Bill
*Marguerite Marsh - Annie
*Margaret Seddon - Annies Mother
*Frank Shannon - Terrence OMalley
*Matthew Betz - Tony the Wop
*Charles Fang - Chinaman
*Harry Lee - Chinaman
*Miriam Battista - Chinese Girl
*Helen Kim - Chinese Girl

==References==
 

==External links==
 
* 
* 
* 
*  (University of Washington, Sayre Collection)

 
 
 
 
 
 
 
 
 


 
 