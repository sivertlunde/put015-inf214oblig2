U-Carmen eKhayelitsha
{{Infobox film
| name           = U-Carmen eKhayelitsha
| image          = U-Carmen e-Khayelitsha film.jpg
| writer         = Mark Dornford-May Andiswa Kedama
| starring       = Pauline Malefane
| director       = Mark Dornford-May
| music director = Charles Hazlewood
| producer       = Mark Dornford-May Camilla Driver Ross Garland
| distributor    = Koch-Lorber Films
| released       =      
| runtime        = 120 minutes
| country        = South Africa Xhosa
| music          = Georges Bizet
| budget         = South African rand|R5,279,092
}}
U-Carmen eKhayelitsha is a 2005 South African operatic film directed and produced by Mark Dornford-May.

==Production== township of Khayelitsha. U-Carmen was translated into Xhosa by Andiswa Kedama and Pauline Malefane, who also play Amanda and Carmen in the movie, respectively.

The cast rehearsed for four weeks before they began shooting the film. The films musical numbers were recorded live on the set without any additional dubbing.  The film was Dornford-Mays directorial debut. Also, none of the cast members had ever acted on film before. 

==Plot summary==
The film begins when Nomakhaya arrives at a Cape Town police station, looking for police sergeant Jongikhaya. He is out on patrol, so she decides to return later to avoid being harassed by the other officers. Meanwhile, Carmen and Amanda are going to work at the cigarette factory. They pass in front of Jongikhayas police vehicle and Carmen yells at him for parking there. Nomakhaya eventually finds Jongikhaya and gives him a ring that his dying mother sent him: she urges him to return to his village to see his mother before she dies. A flashback reveals that Jongikhaya has been disowned by his mother after he drowned his brother during an argument. Later, the bored police officers decide to go to the cigarette factory to see the girls. Carmen is piqued when Jongikhaya reads his Bible and ignores her. She flirts with him and throws a rose into his car. 
Later, the cigarette girls are watching TV when they see that a singing star, Lulamile Nkomo, from their township is returning to the area for a special concert. Pinki turns off the TV when Carmen is trying to watch the footage and a fight ensues. The police arrive to break up the fight; Jongikhaya takes Carmen into custody after she wounds Pinki with a knife. However, Carmen convinces Jongikhaya to let her go in exchange for her love and promises to meet him later at a local bar. Jongikhaya is demoted by Captain Gantana and confined to barracks for his role in the escape.

Several days later, Carmen, Amanda, the cigarette factory workers, and Carmens drug dealer friends eagerly await the arrival of Lulamile Nkomo at Bra Nkomos bar. The police arrive to search for Carmen, but she hides. Jongikhaya also arrives, although he is treated with hostility by the factory girls and the drug traffickers until Carmen vouches for him. He declares his undying love for Carmen and she warns him again that she only belongs to herself. He also gives her his mothers ring. At the urging of Carmen, Jongikhaya quits his job and becomes a drug trafficker. However, one night during a smuggling operation, he becomes jealous when Carmen is friendly towards another man and starts a fight. The other drug traffickers beat him up and a furious Carmen declares that their relationship is over. Carmen returns the ring that he gave her. Jongikhaya is determined that Carmen will not forget him. He declares he will kill her if she rejects him.
Carmen is scheduled to sing at Lulamile Nkomos homecoming concert. Her friends warn her that Jongikhaya is in the audience. When Carmen tries to tell Jongikhaya that their relationship is over, he chases her outside the music venue and threatens her with a knife. Despite his ominous threat, Carmen refuses to take him back. Jongikhaya stabs her and is seized by members of the concert audience as she dies.

==Awards==
The film won critical acclaim when it received the Golden Bear at the 2005 Berlin International Film Festival.

==Cast==
* Carmen - Pauline Malefane
* Nomakhaya - Lungelwa Blou
* Jongikhaya - Andile Tshoni
* Amanda - Andiswa Kedama
* Manelisa - Noluthando Boqwana

==See also==
*Dimpho di Kopane
*Xhosa music
 Black adaptations of Carmen: Carmen Jones (1954), set in 1950s USA and directed by Otto Preminger with lyrics by Oscar Hammerstein II
*  (2001), an MTV production from 2001 starring Beyoncé Knowles and set in Philadelphia and Los Angeles in modern times.
*Karmen Geï (2001), the first African Carmen and, arguably, the first African filmed "musical."

==References==
 

==External links==
*     
*    
* 

 
 

 
 
 
 
 
 
 
 
 
 