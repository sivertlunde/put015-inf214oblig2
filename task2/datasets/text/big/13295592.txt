Ramona (1910 film)
 
{{Infobox film
| name           = Ramona
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = Helen Hunt Jackson (novel) D. W. Griffith Stanner E. V. Taylor
| starring       = Mary Pickford Henry B. Walthall
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 17 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
  short drama film directed by D. W. Griffith, based on Helen Hunt Jacksons 1884 novel Ramona, and starring Mary Pickford and Henry B. Walthall. A copy of the print survives in the Library of Congress film archive.     The film was remade in 1928 with Delores del Rio and 1936 with Loretta Young.

==Cast==
* Mary Pickford as Ramona
* Henry B. Walthall as Alessandro
* Francis J. Grandon as Felipe
* Kate Bruce as The Mother
* W. Chrystie Miller as The Priest
* Dorothy Bernard
* Gertrude Claire as Woman in west
* Robert Harron
* Dell Henderson as Man at burial
* Mae Marsh Frank Opperman as Ranch hand
* Anthony OSullivan as Ranch hand
* Jack Pickford as A boy
* Mack Sennett as White exploiter

==See also==
* List of American films of 1910
* D. W. Griffith filmography
* Mary Pickford filmography
* Ramona (1928 film)
* Ramona (1936 film)

==References==
 

==External links==
* 
*   on YouTube

*  

 

 
 
 
 
 
 
 
 
 
 
 
 



 