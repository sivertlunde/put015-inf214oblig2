Hindustan Ki Kasam (1999 film)
 
{{Infobox Film
| name           = Hindustan Ki Kasam
| image          = Hindustan Ki Kasam 1999 DVD cover.jpg
| image_size     = 
| caption        = DVD cover
| director       = Veeru Devgan
| producer       = Veeru Devgan
| writer         = 
| narrator       = 
| starring       = Amitabh Bachchan Ajay Devgan Manisha Koirala Sushmita Sen
| music          = Sukhwinder Singh Norman Kent
| editing        = 
| studio         = Pavan Hans Airstrip
| distributor    = Devgan Films
| released       = 23 July 1999
| runtime        = 
| country        = India
| language       = Hindi
| budget         =4 cr
| gross          =58 cr
| preceded_by    = 
| followed_by    = 
}}
 Indian film directed by Veeru Devgan and starring his son Ajay Devgan (in a dual role), Amitabh Bachchan, Manisha Koirala and Sushmita Sen. The movie was a super hit grosser, despite taking a bumper opening. 

==Plot==
A mother gives birth to twins.The twins father was an Indian Army officer who died in a war. Unfortunately, they are separated at the end of the war when their father was celebrating their recent success in a battle with Pakistan. One of the twins ends up in neighbouring Pakistan and is brought up as a Muslim named Tauheed.He was brought up by a terrorist who told him that his mother had died in an attack of Indian army, while the other grows up as a Hindu named Ajay.Ajay is a novellist. With the ongoing rivalry and hatred between the two countries, both find themselves on the opposite side, and must battle each other. The only way they can unite is by saving the life of the Pakistani Prime Minister, who himself has become the target of terrorists.

==Cast==
* Ajay Devgan as Ajay/Tauheed
* Sushmita Sen as Priya
* Manisha Koirala as Roshanaara
* Amitabh Bachchan as Kabeera
* Farida Jalal as Mother of Ajay and Tauheed
* Prem Chopra as Brigadier B.S.Brar
* Navin Nischol as Chander Malhotra
* Shakti Kapoor as Major Verma
* Kader Khan as Dr.Dastoor
* Gulshan Grover as Jabbar Shahbaz Khan as I.S.I Chief
* Pramod Moutho as Pakistani Prime Minister

==Soundtracks==
{| class ="wikitable"
|-
!#
!Title
!Singer(s)
! Length
|-
| 1
| Jalwa Jalwa 	
| Udit Narayan, Sukhwinder Singh, Jaspinder Narula
| 05:52
|-
| 2
| Akhiyan Akhiyan
| Udit Narayan, Alka Yagnik
| 05:00
|-  
| 3
| Mera Dil Nai Lagda 
| Alka Yagnik
| 05:27
|- 
| 4
| Tere Dil Ke Paas 	
| Sonu Nigam, Asha Bhosle
| 06:25
|-
| 5
| Is Paar Sarhad Ke
| Anuradha Paudwal, Sukhwinder Singh
| 04:33
|- 
| 6
| Main Hindustan Hoon
| Sukhwinder Singh
| 07:02
|-
| 7
| Ranjhana Ve
| Alka Yagnik
| 05:20
|-
| 8
| Love Love  Poornima
| 04:31
|- 9
|Ishq Brandi Sukhwinder Singh
|03:38
|- 10
|Yaara Teri Ghoot Sadhana Sargam
|00:59
|- 11
|Tere Dil Mein Sadhana Sargam
|00:54
|}

== External links ==
*  

==References==
 

 
 
 


 