Four Corners (film)
 
 
 
{{Infobox film
| name           = Four Corners
| image          = 
| caption        = 
| director       = Ian Gabriel
| producer       = Cindy Gabriel Genevieve Hofmeyr
| writer         = Hofmeyr Scholtz
| story          = Hofmeyr Scholtz Ian Gabriel
| screenplay     = Hofmeyr Scholtz Terence Hammond
| starring       = Brendon Daniels Lindiwe Matshikiz
| cinematography = Vicci Turpin
| editing        = Ronelle Loots
| distributor    = Indigenous Films
| released       =  
| runtime        = 114 minutes
| country        = South Africa
| language       = Afrikaans, English
| budget         = 
}}
 Best Foreign Language Film at the 86th Academy Awards,       but it was not nominated. It won Best Narrative Feature at the Santa Fe Independent Film Festival, 2014.

==Cast==
* Brendon Daniels as Farakhan - a reformed prison general from the 28s gang.
* Lindiwe Matshikiza as Leila -  a doctor returning from London to her childhood home in the Cape Flats.
* Irshaad Ally as Gasant - the charismatic leader of the 26 gang (archrivals of the 28s).
* Abduragman Adams as Tito - a dedicated detective in the Cape Flats.
* Jezriel Skei as Ricardo - a 13 year old chess prodigy from the Cape Flats.
* Jerry Mofokeng as Manzy - a lodger in Leilas house.
* Israel Makoe as Joburg - Farakhans best friend and fellow 28 gang member.

==Release==
Four Corners was screened on a limited release for one week in 2013 at The Bioscope Cinema in Johannesburg, 23–29 September. The general South African theatrical release is scheduled for 28 March 2014.

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of South African submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 