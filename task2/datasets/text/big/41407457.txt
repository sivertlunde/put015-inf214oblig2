Anna Thangi
{{Infobox film
| name = Anna Thangi
| image =
| caption = 
| director = Om Sai Prakash
| writer = Om Sai Prakash
| producer = Prabhakar
| starring = Shivrajkumar, Radhika Kumaraswamy, Deepu
| music = Hamsalekha
| cinematography = R. Giri
| editing = K. Eshwar
| studio  = Vijay Films
| released =  November 4, 2005
| runtime = 150 min
| language = Kannada  
| country = India
}}
 Radhika in the lead roles. The film is directed and written by Om Sai Prakash and produced under Vijay films banner. The film, upon release, met with positive reviews and was remade into Telugu language with the title Gorintaku (2008 film)|Gorintaku.

==Cast==
*Shivrajkumar as Shivanna Radhika as Lakshmi
*Deepu
*Srinivasa Murthy
*Sudharani
*Doddanna
*Vishal Hegde
*Mukhyamantri Chandru
*Bank Janardhan
*Ramesh Bhat
*Sadhu Kokila
*Tennis Krishna

==Release==
The film was released on November 4, 2005 all over Karnataka. The film met with positive response at the box-office.

==Soundtrack==
All the songs are composed, written and scored by Hamsalekha. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) 
|-
| 1 || "Anna Thangiyara Anubandha" || S. P. Balasubramanyam, K. S. Chithra
|-
| 2 || "Dum Dum Dum" || Udit Narayan, K. S. Chithra 
|-
| 3 || "Hubbaliya Shehardaage" || KK (singer)|K.K, Anuradha Sriram 
|-
| 4 || "Thavarumane Eega" || K. S. Chithra, Anuradha Sriram, Madhu Balakrishnan
|-
| 5 || "Gandasu Horagade" || Sunidhi Chauhan 
|-
| 6 || "Anna Nammavanadaru" || K. S. Chithra 
|-
|}

==References==
 

== External links ==
* 
* 
* 

 
 
 
 
 
 
 


 