The Legend of Mor'du
{{Infobox film
| name           = The Legend of Mordu
| image          =
| image_size     =
| caption        =
| director       = Brian Larsen Mark Andrews (executive) John Lasseter (executive)
| writer         = Steve Purcell Brian Larsen Mark Andrews Brenda Chapman
| starring       = Julie Walters Steve Purcell Callum ONeill
| music          = Patrick Doyle
| cinematography = Andrew Jimenez
| editing        = Tim Fox
| studio         = Pixar Animation Studios Walt Disney Pictures Walt Disney Studios Home Entertainment Brave DVD)
| runtime        = 7 minutes
| country        = United States
| language       = English
}}
The Legend of Mordu is a 2012 Pixar short attached to the Blu-ray and DVD release of Brave (2012 film)|Brave.    It gives in-depth background about the films villain, an evil greedy prince as told by the eccentric witch who transformed him into the bear he is in the film. The film combines traditional animation and computer animation.

==Plot==
The short begins with the witch inviting a guest into her cottage. Her crow assumes the guest wants the bear spell, so she tells the story of a man who became the demon bear Mordu. The man had been the eldest of four sons of a king in an ancient kingdom, each of whom had his own gift. Of the younger three, one was wise, one was compassionate and one was just. The kings eldest was strong, yet he mistook strength for character. When the king died one autumn, rather than giving the eldest all the inheritance, he divided the rule among all of his sons equally, believing their gifts combined would make the kingdom even greater. Feeling disgraced and filled with greed, however, the eldest son refused to accept this, proving his point in front of his brothers by using one of his axes to break an image of himself off of a stone tablet that depicted himself and his brothers on it, shattering the bonds of their brotherhood. His words turned to war, changing the kingdoms fate forever.

Even though the princes army was powerful, the war remained a stalemate. In looking for a way to change his fate, the prince came across a menhir ring within the woods. From there, the will-o-the-wisps guided him to the edge of a loch, and the witchs cottage far from the shore. Hoping to turn the tide of the war to his favor, the prince persuaded the witch to make a spell to give him the strength of ten men by offering her his signet ring, and she gave him the spell in a drinking horn, but warns him of making a choice: either to fulfill his dark wish or heal the family bonds he had broken. When the prince brought his brothers before him by staging up a false truce, to their protests he drank the spell, which immediately gave him the strength tenfold but to his surprise, in the form of a massive black bear. While he could have broken the spell by choosing to "mend the bond torn by pride", instead the prince accepted his new form and brought his brothers down, along with their armies. He then tried to get his army to rule the kingdom, but they saw him as a wild creature and turned against him. Enraged, he attacked his former men, killing most of them. The remainder fled the kingdom in terror, leading to its collapse. Doomed to this bestial form by his desire for power over the bonds of family, the princes human consciousness and intelligence were, over time, consumed by animalistic bloodlust. Now known by the name Mordu, he wandered the land aimlessly, a savage beast mentally and physically, killing and instilling terror wherever he roamed.

The witch ends the story here, and she offers the spell in the form of a cake to the guest, who turns out to be Wee Dingwall. He panics, says he only stopped by for water, and runs out of the cottage.

==Cast==
* Julie Walters as The Witch
* Steve Purcell as The Crow
* Callum ONeill as Wee Dingwall

==References==
 

==External links==
*  

 
{{succession box
 | before = Partysaurus Rex Pixar Animation short films
 | years = 2012 The Blue Umbrella}}
 

 
 

 
 
 
 
 
 
 
 
 