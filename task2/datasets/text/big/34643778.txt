The Dark Place
{{Infobox film
| name = The Dark Place
| image = Teaser_poster_for_The_Dark_Place.jpg
| caption = The poster for The Dark Place
| director = Jody Wheeler
| producer = Carlos Pedraza Jody Wheeler J.T. Tepnapa
| writer = Jody Wheeler 
| cinematography = David Berry	 	
| editing = Steve Parker
| music = David John Joe Terrana
| starring = Sean Paul Lockhart Timo Descamps Blaise Embry
| studio = Blue Seraph Productions
| released =  
| distributor = Breaking Glass Pictures  PRO-FUN media Filmverleih 
| runtime = 87 minutes 
| country = United States
| language = English
| budget = 
| gross = 
}} 2014 mystery film|mystery-thriller film written and directed by Jody Wheeler. It is produced by  J.T. Tepnapa and Carlos Pedraza. It stars Blaise Embry, Timo Descamps, Sean Paul Lockhart and Eduardo Rioseco. The film is a twisted thrill ride of betrayal, hope, greed, love — and mommy issues, having a gay main character makes familiar worlds and genres become brand new landscapes. The film was shot in Hillsboro and Portland, Oregon, USA.  It was produced by Blue Seraph Productions.

==Plot==
Keegan Dark (Blaise Embry) returns to the heart of Californias winery valleys to make peace with his long-estranged family. Instead, he finds a harrowing mystery that endangers his familys lives and livelihood.

==Cast== Blaise Embry ... Keegan Dark 
* Timo Descamps ... Wil Roelen
* Sean Paul Lockhart ... Jake Bishop
* Eduardo Rioseco ... Ernie Reyes
* Shannon Day ... Celeste Dark
* Andy Copeland ... Adrian Bishop
* Shade Streeter  ... Young Keegan
* Genevieve Buechner ... Wendy Luckenbill
* Allison Lane ... Sherriff Timmer
* Joshua Stenseth ... Collin Dark
* Jessica Hendrickson ... Lucinda
* Denny McAuliffe ... Young Ernie
* Kendall Wells ... Calvin
* Harold Phillips ... Steven Dark
* Ron Boyd ... Deputy Nelson

==Reviews==
Ed Kennedy from The Backlot said, "Refreshing... a real thriller with a beautiful setting, georgeous actors with complicated relationships".  Timothy Junes of the Zizo Online commented, "The Dark Place is neither a masterpiece nor a monster of a film. Its refreshing to see where homosexuality is a given and no theme in itself. A film with gay men Entertaining". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 