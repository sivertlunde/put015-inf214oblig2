Bheegi Palkein
 
{{Infobox film
| name           = Bheegi Palkein
| image          = Bheegi Palkein.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sisir Mishra
| producer       = K. K. Arya
| screenplay     = Sisir Mishra, Kartik Rath
| story          = Sisir Mishra
| based on       =  
| starring       = Raj Babbar Smita Patil
| music          = Jugal Kishore  Tilak Raj
| cinematography = 
| editing        = Sudhakar
| studio         = Filmistan, Natraj Essel, Film City
| distributor    = 
| released       =  
| runtime        = 123 minutes
| country        = India Hindi
| budget         = 
| gross          = 
}} Indian Bollywood film directed by Sisir Mishra and produced by K. K. Arya. It stars Raj Babbar and Smita Patil in pivotal roles.

==Plot== Brahmin family, uncivilized casting systems at that time and about the blame between husband and wife in family.

The film starts with Ishwar Acharya (Raj Babbar) an I.E.S. officer who want to go for his inspection to a Roman Catholic Mission school of Odisha. There he met Shanti (Smita Patil) as a school teacher. He met her and ask to go with their past live as a husband and wife. Then, she say a home built with the understanding and feelings which we dont have more. Then, Ishwar go to his lifes flash back.

Ishwar (Raj Babbar) and Shanti (Smita Patil) both were childhood friends. They live, study and travel together. But, when they become young both were love each other. They want to marry each other. But at that time Ishwars sister-in-law (Sulabha Deshpande) choose a girl for him but he denied and ask to marry Shanti. Because of lower caste and poorness of her his whole family except his brother opposed him. At that time Ishwar got a job in a private company.  After this, Ishwar go for registry marriage with Shanti. Because of the opposition of family Ishwar and Shanti dont go his house. They rent a house and live there happily.

Some days later, when Shanti give monthly list for house wanting things then, Ishwar bought a costly pressure cooker. Here, Shanti worried about their future and give suggestion to him but he angry and tell that,"when I am happy you arent happy for me. So you are so jealous on me". In this way they give birth a child. But in any matter or suggestion Ishwar discarded her. In the following days she also work in a bank. But Ishwar dont like this. One day, Ishwar rid the bike with speed and accident with a truck. He blamed Shanti for this and every day he rude on her in hospital. Shanti thinks this is for the bad time of his sun shine. So, he dont argue with him. On the following days she become so busy in bank for her work. So, she late to go home and hospital. So, Ishwar thinks that there is any new relationship with anyone. At that time her son was so ill, she dont care him properly for her work. So, one rainy night she discovered her son was no more. After the discharge from hospital Ishwar know that his son is dead. Then, he dont have more trust on her from that and go to his family. To know this Shanti also left the house and go to a Roman Catholic Mission school. When Ishwar know the truth behind this he go to the meet Shanti. But, after the arrival he hear that Shanti also left the house after him.

From that day to present Ishwar want to looking for Shanti and want to say sorry. But not. After a long period Ishwar met her in a church as a teacher. After the flash back he go to her cottage and Shanti asked from that you decide yourself for any decision as my husband, today decide like my friend then Ishwar says husband had defeated, your friend wins. So, they decide to maintain their life as friendship. Then Ishwar go back by leaving Shanti in that church.

==Cast==
* Raj Babbar  as  Ishwar Acharya
* Smita Patil  as  Shanti
* Dina Pathak  as  Mrs. Acharya
* Sulabha Deshpande  as  Janki Kishan Acharya
* Suresh Chatwal  as  Prakash Acharya
* Laxmi Chhaya  as  Ishwars sister
* Jagdeep  as  Hussein Bhai Madadgari Asit Sen  as  Divisional Manager
* Leela Mishra  as  Chachi

==Filming==
The film locations included Jagannath Temple, Puri, Cuttack Chandi Temple (Cuttack), Udayagiri and Khandagiri Caves. The first song of this film was shot in Roman Catholic Church (Surada). Other scenes were shot in Cuttack, Bhubaneswar and Brahmapur, Odisha|Brahmapur. Basically the overall film was shot in Odisha and some studios in Mumbai.

==Crew==
*Director: Sisir Mishra
*Producer: K. K. Arya
*Music Director: Jugal Kishore, Tilak Raj
*Lyrics: M. G. Hashmat
*Playback Singers: Amit Kumar, Asha Bhosle, Kishore Kumar, Lata Mangeshkar, Mohammad Rafi, Sangita Mahapatra

==External links==
* 

 
 
 