The Sheep Thief
The Sheep Thief is a 1997 16&nbsp;mm short film by Asif Kapadia Writer /Director lasting 23mins. It is Kapadia’s graduation film from the Royal College of Art.

==Plot==
A young Street kid is branded for stealing and becomes an outcast.

==Awards==
*Festival de Cannes 1998 Cinefondation - Second Prize. Brest European Grand Prix}.
*St. Petersburg Film Festival 1998 - Best Short Film Award.
*Isfahan Iranian Children’s Film Festival 1999 - Best Short Film Award. Melbourne Film Festival 1998  - Best Short Film Award.
*Poitiers Film Festival 1998 - Best Direction Award.
*Tel Aviv Film Festival 1998 - Most Promising Director Award.
*Locarno Film Festival 1998 - International Prix de Aaton.
*New York Expo 1998 - Jury Prize.
*Montecatini Film Festival 1998  -Jury Prize.
*Cinema Texas Film Festival 1997 - Best Cinematography.

The film is included on CINEMA 16, the DVD of British Short films.

Screened at Clermont Ferrand, Toronto and London Film Festivals, televised in the UK by Channel 4 and across Europe by Canal +, ZDF and Arte.

 

 
 
 