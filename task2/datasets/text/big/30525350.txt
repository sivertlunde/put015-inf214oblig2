A Good Man (2011 film)
{{Infobox film
| name           = A Good Man
| image          = 
| caption        = 
| director       = Gordon Quinn 
Bob Hercules
| producer       = Joanna Rudnick
| writer         = 
| narrator       = 
| music          =
| cinematography = Keith Walker
| editing        = David E. Simpson
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} race and coming to grips with the legacy of the Lincoln Presidency and the American Civil War.  From the initial pre-production to the shows final performance, the documentary follows Jones as he attempts to connect with his dancers and convey the spirit of the civil rights movement that has inspired him as an artist.  

A Good Man was a co-production of American Masters, ITVS, Kartemquin Films, Media Process Group, The Ravinia Festival and was produced by Joanna Rudnick and directed by Gordon Quinn and Bob Herucles.  The documentary aired on Public Broadcasting Service|PBSs American Masters series in 2011. 

==References==
 

==External links==
*  
*   at Kartemquin Films

 

 
 
 
 
 
 
 
 

 