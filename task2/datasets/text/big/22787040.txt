Ambush Bay
{{Infobox film
| name           = Ambush Bay
| image          = Ambush_Bay_1966_film_poster.jpg
| caption        = 1966 film poster
| director       = Ron Winston
| producer       = Hal Klein Aubrey Schenck
| executive producer = Aubrey Schenk
| writer         = Ib Melchior Marve Feinberg starring        = Hugh OBrian Mickey Rooney James Mitchum Tisa Chang Peter Masterson Harry Lauter
| music          = Richard LaSalle
| cinematography = Emmanuel I. Rojas
| editing        = John F. Schreyer
| studio     = Courageous Films
| distributor    = United Artists
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Ambush Bay is a 1966 American war film, filmed on location in the Philippines and starring Hugh OBrian, Mickey Rooney and James Mitchum.

==Plot== invasion of scouts is PFC Grenier (James Mitchum).
 camouflage jacket to carry on his first ground combat mission. He serves as a narrator to the audience.
 Navy Cross  and who also acted as the films technical advisor) is killed while ambushing a small group of Japanese soldiers, and First Sergeant Corey (Hugh OBrian, recognized as one of the youngest Drill Instructors to have served in the USMC ) takes command.

Pvt. George George and Pfc. Henry Reynolds are killed while taking out a Japanese tank and patrol. And Cpl. Stanley Parrish is killed by a guerrilla trap soon after. As they walk on, Amado is shot by a Japanese officer while scaling a small hill. The marines let him die to keep their presence secret. Grenier is eventually told by Gunnery Sergeant Wartell (Mickey Rooney) that they were sent to recover some important information from a contact in a tea house whose radio was destroyed, thus explaining the radios importance to the mission.

Greniers inexperience and incompetence arouses anger amongst Corey and the other members of the patrol. His only friend is easy going but professional Gunnery Sergeant Wartell who acts as a mediator between the hard no nonsense 1st Sgt Corey and Grenier, explaining each one to the other and the audience. Rooney provides the only comedy relief in the film when his character is captured and interrogated by a group of careless Japanese soldiers.

The surviving squad members eventually arrive at the tea house but, unfortunately, Amado was the one who was supposed to meet the contact as he was the only Filipino in the group. Desperate, Corey decides to met the contact, Miyazaki a Japanese-American woman from Long Beach, California. While sneaking out of the camp with Miyazaki, Corey crashes into a waiter and the two run across a straw bridge then blow it up with a grenade, escaping from the soldiers. Meanwhile, a large skirmish with a Japanese patrol has killed Cpl. Alvin Ross and Platoon Sergeant William Maccone, shot the radio up beyond repair, and wounded Gunnery Sergeant Wartell. Wartell, knowing he will slow the survivors down, tells the reluctant Corey to leave him behind. When they leave, he plants grenades under himself and is captured by the Japanese soldiers. After toying with them a bit during his interrogation he sets off the grenades, taking them all out in the blast, and leaving Corey and Grenier the only surviving marines. The explosion is heard by the survivors and they sadly track on.

Corey and Grenier learn from Miyazaki that the Japanese are expecting the invasion fleet and have placed a mine field powerful enough to destroy the entire fleet in the water around the invasion sites. Arriving at a friendly Filipino village, Corey and Grenier are able to escape a Japanese patrol by boat. But Miyazaki is killed by an officer she seduced to buy the guys some time. At last discovering the principles of mission accomplishment, altruism, and self sacrifice through observation, Grenier becomes a squared away Marine. He and his First Sergeant infiltrate the enemy base to remotely detonate the minefield with the Japanese radio transmitter. As Corey provides a one man army diversion, Grenier is able to detonate the mines by radio control. Grenier then steals a radio and goes to tell Corey of their success, only to find Corey dead of blood loss from wounds he got while holding off the Japanese. Grenier escapes to the coast and radios for pick up. Leaving him the sole survivor of the mission. The movie ends with Grenier looking at the ocean while he listens to General Macarthur speech as he awaits pick up.

==Production== The Outsider, and The Lieutenant television series.

A novelisation of the films screenplay was written by Jack Pearl.

During filming, Mickey Rooney became ill with a fever. While he was hospitalized in Manila, his wife Barbara Ann Thomason was killed by her lover in a murder-suicide. 

==Reception==
The New York Times remarked that the film was one that everyone had seen at least once in some form but was "a trim, muscular, pint-sized package as sensible as it is modest that makes a little count for a lot". 

==Notes==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 