Detective 909 Keralathil
{{Infobox film 
| name           = Detective 909 Keralathil
| image          =
| caption        =
| director       = P. Venu
| producer       = TC Sankar
| writer         = P. Venu P. J. Antony (dialogues)
| screenplay     =
| starring       = Jayabharathi Hemalatha Sankaradi Sreelatha Namboothiri
| music          = M. K. Arjunan
| cinematography =
| editing        =
| studio         = Prince Productions
| distributor    = Prince Productions
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed by P. Venu and produced by TC Sankar. The film stars Jayabharathi, Hemalatha, Sankaradi and Sreelatha Namboothiri in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Jayabharathi
*Hemalatha
*Sankaradi
*Sreelatha Namboothiri
*T. S. Muthaiah
*Abbas
*Alummoodan
*Girish Kumar
*Junior Sheela
*K. P. Ummer
*Kottarakkara Sreedharan Nair
*Kshema
*Kuttan Pillai
*MS Namboothiri Meena
*Nambiar
*Panjabi
*Ramu Sadhana
*Shamsudeen
*Shyam Shankar
*Sreekumar Sudheer
*Vijayasree
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maanasa Theerathe || S Janaki || P. Bhaskaran || 
|-
| 2 || Manmadha Devante || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Paala Poothu || Chorus, KP Chandramohan, Latha Raju || P. Bhaskaran || 
|-
| 4 || Prema Saagarathin || P Jayachandran || P. Bhaskaran || 
|-
| 5 || Rangapooja Thudangi || Usha Ravi || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 