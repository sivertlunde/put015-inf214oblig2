Banjo on My Knee (film)
 
{{Infobox film
| name           = Banjo on My Knee
| caption        = Film poster
| image	         = Banjo on My Knee FilmPoster.jpeg John Cromwell
| producer       = Darryl F. Zanuck Nunnally Johnson Harry Hamilton (novel)
| based on       = Banjo on My Knee (1936 novel)
| screenplay     = Nunnally Johnson
| starring       = Barbara Stanwyck Joel McCrea Walter Brennan
| music          = Charles Maxwell
| cinematography = Ernest Palmer
| editing        = Hanson T. Fritch
| studio         = 
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
}}
 musical comedy-drama John Cromwell. Sound Recording (Edmund H. Hansen).   

==Plot==
Ernie Holley (Joel McCrea) runs away on his wedding night because he thinks he has killed a wedding guest. His father Newt (Walter Brennan) and bride Pearl (Barbara Stanwyck) find him in New Orleans and persuade him to come home.

==Cast==
* Barbara Stanwyck as Pearl Elliott Holley
* Joel McCrea as Ernie Holley
* Walter Brennan as Newt Holley
* Buddy Ebsen as Buddy
* Helen Westley as Grandma
* Walter Catlett as Warfield Scott Tony Martin as Chick Bean (as Anthony Martin)
* Katherine DeMille as Leota Long (as Katherine De Mille)
* Victor Kilian as Mr. Slade
* Minna Gombell as Ruby
* Spencer Charters as Judge Tope

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 