Casino Jack
 
{{Infobox film
| name = Casino Jack
| image = Casino Jack.jpg
| border = yes
| caption = 
| director = George Hickenlooper
| producer = Gary Howsam Bill Marks George Zakk
| writer = Norman Snider
| starring = Kevin Spacey Barry Pepper Rachelle Lefevre Kelly Preston Jon Lovitz Maury Chaykin Jonathan Goldsmith
| cinematography = Adam Swica
| editing = William Steinkamp
| studio = Hannibal Pictures Rollercoaster Entertainment
| distributor = Art Takes Over Films (ATO)
| released =  
| runtime = 108 minutes
| country = Canada 
| language = English
| budget = $12.5 million
| gross = $1.1 million 
}}
 action thriller thriller comedy corruption scandal conspiracy and tax evasion in 2006,  and of trading expensive gifts, meals and sports trips in exchange for political favors.    
Abramoff served three and a half years of a six-year sentence in federal prison, and was then assigned to a halfway house. He was released on December 3, 2010.
 Golden Globe Award for Best Actor for his portrayal of Abramoff, eventually losing to Paul Giamatti for his role in Barneys Version (film)|Barneys Version. 

==Plot==
A hot shot Washington DC lobbyist and his protégé go down hard as their schemes to peddle influence lead to corruption and murder.

==Cast==
* Kevin Spacey as Jack Abramoff
* Kelly Preston as Pam Abramoff
* Rachelle Lefevre as Emily J. Miller
* Barry Pepper as Michael Scanlon
* Jon Lovitz as Adam Kidan
* John David Whalen as Kevin A. Ring
* Yannick Bisson as Oscar Carillo Graham Greene as Bernie Sprague
* Eric Schweig as Chief Poncho
* Maury Chaykin as Big Tony Ralph Reed
* Spencer Garrett as Tom DeLay
* Joe Pingue as Anthony Ferrari David Fraser as Karl Rove
* Jeffrey R. Smith as Grover Norquist
* Daniel Kash as Konstantinos "Gus" Boulis
* Conrad Pla as Agent Hanley
* Hannah Endicott-Douglas as Sarah Abramoff
* Ruth Marshall as Susan Schmidt
* Reid Morgan as Brian Mann
* Duke Redbird as Senator Nighthorse

==Production== East Hampton, New York.]]
Filming took place in June 2009 in various locations across Hamilton, Ontario, Canada, including McMaster University and downtown Hamilton. The film was scheduled for release in December 2010 and premiered at the Toronto International Film Festival. {{Citation | title = Casino Jack premiere photos - 35th Toronto International Film Festival | url = http://www.digitalhit.com/galleries/38/570/ | year = 2010 | author = Evans, Ian | journal = DigitalHit.com | accessdate = 2012-04-10
}} 

This was Hickenloopers final film. He died on October 29, 2010, seven weeks before its scheduled December 17, 2010, national opening. 

==Reception==
Casino Jack received mixed reviews from critics. On Rotten Tomatoes, the film has a rating of 36%, based on 95 reviews, with an average rating of 5.4/10. The sites critical consensus reads, "Kevin Spacey turns in one of his stronger performances, but Casino Jack is a disappointingly uneven fictionalized account of a fascinating true story."  On Metacritic, the film has a score of 51 out of 100, based on 24 critics, indicating "mixed or average reviews". 

Roger Ebert gave the film three out of four stars, stating that "Casino Jack is so forthright, it is stunning." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*   production website at Hannibal Pictures

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 