El hermano José
{{Infobox film
| name           =El Hermano José
| image          = Pepe Arias - El Hermano Jose.JPG
| image_size     =
| caption        =
| director       = Antonio Momplet
| producer       =
| writer         = Nicolás Proserpio
| narrator       =
| starring       = Pepe Arias, Carlos Castro and Ada Cornaro
| music          =
| cinematography = Antonio Merayo
| editor       = Nicolás Proserpio
| distributor    =
| studio = Argentina Sono Film
| released       = 27 August 1941
| runtime        =
| country        = Argentina Spanish
| budget         =
}}
 1941 Argentina|Argentine comedy film.

==Production==

The 93-minute black and white film was made for Argentina Sono Film by director Antonio Momplet.
It was written by Nicolás Proserpio, and stars Pepe Arias, Carlos Castro and Ada Cornaro. 

==Synopsis==

The movie deals with the interaction in a small town between a healer, his daughter and a young doctor, between science and superstition.

==Reception==

La Nación called the film a popular and satirical comedy. Halki noted that it was a visual version of a successful radio show.
Manrupe and Portela and said it was a classic Pepe Arias work, with everything good and bad that implies, and had been filmed without much effort. 

==Full cast==
The full cast was:
 
* Pepe Arias
* Carlos Castro
* Ada Cornaro
* Dario Cossier
* María Duval
* Isabel Figlioli
* Ramón Garay
* Antonio Gianelli
* José Otal
* Raimundo Pastore
* Ernesto Raquén
* José Ruzzo
* Semillita
* Ernesto Villegas
 

==External links==
Citations
 
Sources
 
* |url=http://www.imdb.com/title/tt0192115/
 |title=Brother Joe|work=IMDb|accessdate=2014-06-06}}
*{{cite book|ref=harv
 |last1=Manrupe|first1=Raúl |last2=Portela|first2=María Alejandra|year=2001|title=Un diccionario de films argentinos (1930-1995) 
 |location=Buenos Aires|language=Spanish|publisher=Corregidor|ISBN=950-05-0896-6}}
 

 
 
 
 
 
 

 