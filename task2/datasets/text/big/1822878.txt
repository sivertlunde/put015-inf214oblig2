Stealing Beauty
{{Infobox film
| name           = Stealing Beauty
| image          = Stealing_Beauty_Poster.jpg
| caption        = Theatrical release poster
| director       = Bernardo Bertolucci
| producer       = Jeremy Thomas
| screenplay     = Susan Minot
| story          = Bernardo Bertolucci
| starring       = Liv Tyler Joseph Fiennes Jeremy Irons Sinéad Cusack Rachel Weisz
| music          = Richard Hartley
| cinematography = Darius Khondji
| editing        = Pietro Scalia UGC Images
| distributor    = 20th Century Fox
| released       =   
| runtime        = 113 minutes 119 minutes (DVD)
| country        = France, Italy, United Kingdom German
| budget         =
| gross          = $4.7 million 
}} 1996 drama film directed by Bernardo Bertolucci and starring Liv Tyler, Joseph Fiennes, Jeremy Irons, Sinéad Cusack, and Rachel Weisz. Written by Bertolucci and Susan Minot, the film is about an American teenage girl who travels to a lush Tuscan villa near Siena to stay with family friends of her poet mother who recently committed suicide.    The film was actress Liv Tylers first leading film role. Stealing Beauty premiered in Italy in March 1996, and was officially selected for the 1996 Cannes Film Festival in France in May.    It was released in the United States on June 14, 1996.

==Plot==
 
Lucy Harmon, an American teenager, arrives in the lush Tuscan countryside to be sculpted by a family friend who lives in a beautiful villa there. Lucy visited there four years earlier and exchanged a kiss with an Italian boy with whom she hopes to become reacquainted. Lucys mother, who lived in the villa, has committed suicide since then. Lucy also hopes to discover the identity of her father, who her mother hinted was a resident of the villa. Once there, Lucy meets and befriends a variety of eccentric locals who were companions of her mother, and begins to form relationships and connections with each of them, specifically with Alex Parrish.

Lucy yearns to lose her virginity and becomes an object of intense interest to the men of the household, but the  suitor she finally selects is not the initial object of her affection. After Alex is brought into hospital - presumably going there to die - Lucy loses her virginity in the farm fields outside the villa.

==Cast==
* Liv Tyler as Lucy Harmon
* Joseph Fiennes as Christopher Fox
* Jeremy Irons as Alex Parrish
* Sinéad Cusack as Diana Grayson
* Donal McCann as Ian Grayson
* Rebecca Valpy as Daisy Grayson
* Jean Marais as M. Guillaume
* Rachel Weisz as Miranda Fox
* D. W. Moffett as Richard Reed
* Carlo Cecchi as Carlo Lisca
* Jason Flemyng as Gregory
* Anna Maria Gherardi as Chiarella Donati
* Ignazio Oliva as Osvaldo Donati
* Stefania Sandrelli as Noemi
* Francesco Siciliano as Michele Lisca
* Mary Jo Sorgani as Maria
* Leonardo Treviglio as Lieutenant
* Alessandra Vanzi as Marta
* Roberto Zibetti as Niccoló Donati   

==Soundtrack== 2 Wicky" (Burt Bacharach) by Hooverphonic Portishead
# Axiom Funk 
# "Annie Mae" by John Lee Hooker 
# "Rocket Boy" by Liz Phair
# "Superstition (song)|Superstition" by Stevie Wonder
# "My Baby Just Cares For Me" (Walter Donaldson) by Nina Simone 
# "Ill Be Seeing You (song)|Ill Be Seeing You" (Sammy Fain) by Billie Holiday Rhymes Of An Hour" (Hope Sandoval) by Mazzy Star
# "Lullabies to Violaine|Alice" by Cocteau Twins
# "You Wont Fall" by Lori Carson Sam Phillips
# "Say It Aint So" by Roland Gift
# "Horn concerto in D K412, 2nd movement" by Wolfgang Amadeus Mozart
# "Clarinet concerto in A K622, 2nd movement" by Wolfgang Amadeus Mozart

;Additional songs Hole was also used in the film. Tyler is shown dancing and singing wildly along to the track, listening with her headphones and walkman.
*Björks song "Bachelorette (song)|Bachelorette" of her 1997 album Homogenic was originally written to be part of the soundtrack and its first working title was "Bertolucci". Björk later faxed Bertolucci to inform him the song would be used in her upcoming album instead.

==Reception==
 
The critical reception for the film was mixed, with some critics praising the Italian setting and the slow pace, while others criticised it for its apparent self-indulgence, and lack of character development and drama.

According to Roger Ebert of the Chicago Sun-Times, who gave it 2/4 stars, "The movie plays like the kind of line a rich older guy would lay on a teenage model, suppressing his own intelligence and irony in order to spread out before her the wonderful world he would like to give her as a gift....The problem here is that many 19-year-old women, especially the beautiful international model types, would rather stain their teeth with cigarettes and go to discos with cretins on motorcycles than have all Tuscany as their sandbox.." 

Critics such as  , June 28, 1996. retrieved on July 2, 2009.  Mick LaSalle of the San Francisco Chronicle,  and James Berardinelli of ReelViews    gave negative reviews, with Berardinelli in particular, calling the movie an atmosphere study, lacking characters,  and Thompson calling it inscrutable. 

Others, such as Jonathan Rosenbaum of Chicago Reader,  Peter Travers of Rolling Stone,  Janet Maslin of The New York Times,  and Jack Mathews of the Los Angeles Times  were more positive, with Rosenbaum in particular praising the movies mellowness and charm.

Review aggregation website Rotten Tomatoes gives the film a score of 51% based on 22 reviews. 

Despite the mixed reception to the film, Liv Tylers performance was met with critical acclaim, making the movie her breakthrough role.

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 