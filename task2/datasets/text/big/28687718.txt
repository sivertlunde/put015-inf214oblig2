Yuvan Yuvathi
{{Infobox film
| name = Yuvan Yuvathi
| image = Yuvan Yuvathi.jpg
| caption = 
| director = G. N. R. Kumaravelan
| producer = Baija
| writer = 
| starring =  
| music = Vijay Antony
| cinematography = Gopi Jagadeeswaran
| editing = G. K. Prabhakaran
| studio = Ram Pictures
| distributor = Reliance Entertainment
| released =  
| runtime = 
| country =  
| language = Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}} Indian Tamil Tamil romantic Telugu under the title Dear.

==Plot==
Kathirvel Murugan (Bharath) is a software engineer in Chennai. He shares his room with friend Sakkarai (Santhanam (actor)|Santhanam). Kathir hails from Usilampatti but wants to project him as city-bred and his ambition is to settle in USA. Kathirs father Sevaka Pandian (Sampath raj) is against his sons attitude. An influential local chieftain, he is against inter-caste marriage and love. Kathir meets Nisha (Rima Kaleengal), who too wants to go abroad. The later loses her passport and Kathir helps her find it.
Slowly Kathir falls for her. Meanwhile, Sevaka Pandian arranges for his sons wedding with a High Court judges daughter, Thangameena (Anuja Iyer). In the meantime, Kathir and Nisha get their visa and get ready to leave together for USA.

The former thinks that he has escaped from his marriage and when he wants to propose to Nisha, he gets a rude shock. She informs him that she is going to USA for her marriage. In the meantime Kathirs father comes to know about their relationship and kidnaps her. Afterwards Kathir also comes to know about it and saves Nisha but Nishas marriage gets stopped. Again after 10 months they both meet in the same country again Kathirs doesnt want to see her but his mind changes and tries to make Nisha love Kathir. At last they both fall in love with each other. Now, they return to their hometown for their marriage. Initially Kathirs father doesnt allow. Later Kathir convinces them and they both marry each other making it a very happy ending.

==Cast== Bharath as Kathirvel Murugan
* Rima Kallingal as Nisha Santhanam as Sakkarai
* Sampath Raj as Sevaka Pandian
* Anuja Iyer as Thangameena Sathyan as Chacha
* Devadarshini as Anu
* Five Star Krishna as Krishna
* Shobana as Sakkarais wife
* Swaminathan as Sakkarais father
* T. P. Gajendran as Thangameenas father
* Mayilsamy as auto driver
* Madan Bob as Alexpandian
* Sujatha as Sevakapandians wife
* Hemalatha as Deivanai
* Shakthi Vasudevan as Saravanan (Guest Appearance)

==Critical reception==
Rohit Ramachandran of nowrunning.com gave it 1/5 stars stating that "Yuvan Yuvathi feels like being stuffed with stale food. Youre likely to regurgitate".  Malathi Rangarajan from thehindu.com stated, "A predictable yet enjoyable finish to a love story that sparkles with the energy of youth!". 

== Soundtrack ==
{{Infobox album|  
| Name = Yuvan Yuvathi
| Type = Soundtrack
| Artist = Vijay Antony
| Cover = 
| Released = 1 July 2011
| Recorded = 2010 Feature film soundtrack
| Length = 
| Label = Think Music
| Producer = Vijay Antony
| Reviews =  
| Last album = Sattapadi Kutram (2011)
| This album = Yuvan Yuvathi (2011)
| Next album = Velayudham (2011)
}}
The soundtrack, composed by Vijay Antony, released on 1 July 2011. The album features 7 tracks with lyrics by Annamalai, Kalai Kumar and Priyan.

{{tracklist
| headline        = Tracklist 
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = yes
| title1          = Oh My Angel
| lyrics1         = Annamalai
| extra1          = Vijay Antony
| length1         = 04:40
| title2          = Mayakka Oosi
| lyrics2         = Kalai Kumar
| extra2          = Vijay Prakash, Madhumitha
| length2         = 05:20
| title3          = 24 Hours
| lyrics3         = Annamalai
| extra3          = Haricharan
| length3         = 05:32
| title4          = Kola Kuthu
| lyrics4         = Annamalai
| extra4          = Vijay Antony, Snehga Lock up(Malaysia)
| length4         = 04:03
| title5          = Un Kannai Partha Piragu
| lyrics5         = Priyan
| extra5          = Karthik (singer), Ramya
| length5         = 05:31
| title6          = Kadalai Poduran
| lyrics6         = Annamalai
| extra6          = Krishnan Mahesan
| length6         = 02:04
| title7          = Ullangai
| lyrics7         = Annamalai
| extra7          = Sangeetha Rajeshwaran
| length7         = 04:39
}}

==References==
 

==External links==

 
 
 
 
 
 