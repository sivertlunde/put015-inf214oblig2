Romantic Warriors - A Progressive Music Saga
 
 
Romantic Warriors - A Progressive Music Saga is a feature-length documentary film written and directed by José Zegarra Holder and Adele Schmidt, released in 2010 as the first part of a series dedicated to progressive rock as an artistic, cultural and social phenomenon. The project reflects the film makers love of a music genre that has made a strong comeback in the early 21st century - four decades after its Seventies heyday - although without regaining the commercial appeal of the original movement. Romantic Warriors was positively received by long-time followers of the genre, and sparked interest also outside the boundaries of the progressive rock fandom.

The second installment of the series - titled   - was released in the summer of 2012, while a third film, dedicated to the Canterbury scene and titled Romantic Warriors III: Canterbury Tales, was released April 2015.

{{Infobox film
| name           = Romantic Warriors - A Progressive Music Saga
| image          =  
| alt            =
| caption        =
| film name      =
| director       = Adele Schmidt
                   José Zegarra Holder
| producer       = Zeitgeist Media LLC
| writer         = Adele Schmidt
                   José Zegarra Holder
| screenplay     =
| story          =
| based on       =  
| starring       = {{plainlist|
* Debi Byrd
* Cabezas de Cera
* Cheer-Accident
* Deluge Grander
* D.F.A.
* Steve Feigenbaum Gary Green
* Gentle Giant
* Karmakanic
* Rob LaDuca
* Ray Loboda
* Rob Martino
* La Maschera di Cera
* Joyce Nalewajk
* Oblivion Sun
* Leonardo Pavkovic
* Mike Potter
* Phideaux
* Qui
* Jim Robinson
* George Roldan
* Deborah Sears
* Paul Sears
* Steve Sly
* Roine Stolt
}}
| narrator       =
| music          = {{plainlist|
* Cabezas de Cera
* Cheer-Accident
* Deluge Grander
* D.F.A.
* Gentle Giant
* Karmakanic
* Rob Martino
* La Maschera di Cera
* Oblivion Sun
* Phideaux
* Qui
}}
| cinematography = Adele Schmidt
                   José Zegarra Holder
| editing        = Adele Schmidt
| studio         =
| distributor    = Zeitgeist Media
| released       = 2010
| runtime        = 95 min.
| country        = United States
| language       = English, Spanish (with English subtitles)
| budget         =
| gross          =
}}

== Background ==

Adele Schmidt and José Zegarra Holder are the co-founders and managers of Zeitgeist Media LLC, a video production company based in the Washington, DC metropolitan area, where it has been active for over 15 years. Schmidts work as a film and video director, editor and producer has won numerous international awards; Zegarra Holder, who in the 1990s was the owner of Media Sur Films, a documentary production firm, also deals with the marketing aspect of the companys activity. 

Romantic Warriors was inspired by the film makers lifelong passion for progressive rock. In particular, Zegarra Holder has been contributing to the Spanish-language blog Autopoietican since 2004.  The documentary was partly funded by the Arts Council and the government of Montgomery County, Maryland. 

== Synopsis ==

Romantic Warriors - whose title references jazz-fusion outfit Return To Forevers seminal 1976 album, Romantic Warrior - introduces progressive rock (or prog, as it is widely known among its fans) in its social, cultural and artistic dimension through interviews and concert footage, captured for the most part in various venues of the US East Coast between 2007 and 2009. The genres  early beginnings in the late Sixties are identified with influential albums such as The Beatles Sergeant Peppers Lonely Hearts Club Band and The Mothers of Inventions Freak Out!, emphasizing its relationship with classical music and the unusual focus on long, multi-part compositions rather than on the conventional song form. The spread of the original movement from the UK to other European countries, and then other parts of the world such as the US, South America and Japan, is illustrated through the use of maps that trace the development of the genre over the past 40 years, including its many modern ramifications.
 Gary Green.

Though most of Romantic Warriors was filmed in the US, some of the bands and artists featured in the documentary hail from countries such as Italy (D.F.A. and  , avant-garde progressive rock and jazz-fusion.

The film also relies on the testimony of people who have contributed to keeping the scene alive: independent label owners (Steve Feigenbaum and Joyce Nalewajk of Cuneiform Records and Leonardo Pavkovic of Moonjune Records), concert/festival organizers (Mike Potter of Orion Studios, George Roldan of ROSfest, Steve Sly of ProgDay, Rob LaDuca, Ray Loboda and Jim Robinson of NEARfest), radio DJs (Deborah Sears of The Prog Rock Diner ), magazine publishers (John Collinge of Progression ), and dedicated fans who have been supporting the genre by buying CDs, attending live events, and setting up webzines and Internet forums (a phenomenon that has contributed enormously to progs 21st-century revival).

While celebrating the golden age of progressive rock, Romantic Warriors places a strong emphasis on those who have been carrying the torch in the past two decades, struggling against the odds of a music industry that has become less and less interested in quality, as well as the steadily decreasing attention span of the average music user. The interviews also touch upon the impact of modern technology on the genre, the use of unconventional instruments such as the Chapman Stick, and the compositional process - highlighting the largely do-it-yourself ethos of the current prog scene. Some of the financial implications of playing non-mainstream music are explored, including the thorny issue of illegal downloading, and the difficulty of standing out in an increasingly crowded scene. Even when decrying the lack of financial rewards for those who choose to play such a niche genre, all the musicians emphasize the passion that drives them to continue in spite of the difficulties.

== Reception ==

{{Album ratings rev1 = Babyblaue Seiten Prog-Reviews rev1score = 10.17/15   rev2 = Dutch Progressive Rock Pages rev2score = 6.5-7/10  rev3 = Educational Media Reviews Online rev3score = Recommended  rev4 = Sea of Tranquility rev4score =    rev5 = USAProgressiveMusic rev5score = 8.5/10 
}}

As the first documentary film of its kind, Romantic Warriors was generally well received by its intended audience, though Dave Baird of the Dutch Progressive Rock Pages pointed out that the film was "interesting and well-made, but for a limited hard-core audience really."  A more positive assessment came from Eric Pseja of USAProgressiveMusic, who stated that the film is "a pretty solid package that will educate, entertain and inspire both Progressive Rock veterans and neophytes alike."   On Educational  Media Reviews Online, Vincent J. Novara of the University of Maryland interpreted the film as a sociological study rather than a documentary about progressive rock.  Other reviewers, such as Pete Pardo of Sea of Tranquility, praised the films down-to-earth style and its emphasis on the community as much as on the music. 

A few weeks after its DVD release, the film was officially presented on September 10, 2010, at the Mexican Cultural Institute in Washington, DC, before an audience that included long-time followers of the genre as well as people who were not previously familiar with the music, but showed keen interest during the Q&A session that followed the showing. The event also featured live performances by two of the artists that appear in the documentary: Dan Britton of Maryland-based band Deluge Grander and Mauricio Sotelo of Mexican trio Cabezas de Cera. 

In 2011, just one year after its worldwide release, Romantic Warriors won the Bronze Peer Award for Best Documentary Over 30 Minutes. The competition is sponsored by the Television, Internet and Video Association of DC (TIVA-DC) to celebrate excellence in local media.  In the same year, the documentary was broadcast by numerous public TV stations across the US  

== References ==

                                   
}}

== External links ==
*  

 
 
 
 