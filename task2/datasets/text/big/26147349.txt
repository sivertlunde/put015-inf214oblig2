Saigon (1948 film)
{{Infobox_Film
| name           = Saigon
| image          = Saigonlakeladd.jpg
| caption        = Theatrical release poeter
| director       = Leslie Fenton
| producer       = P.J. Wolfson
| writer         = Julian Zimet Arthur Sheekman P.J. Wolfson
| starring       = Alan Ladd Veronica Lake
| music          = Robert Emmett Dolan
| cinematography = John F. Seitz William Shea	
| distributor    = Paramount Pictures
| released       = March 31, 1948 (United States)
| runtime        = 93 minutes
| country        = United States
| language       = English
}}
Saigon is a 1948 film starring Alan Ladd and Veronica Lake in their fourth and final film together. It was distributed by Paramount Pictures and was one of the last films Veronica Lake made under her contract with the studio.

==Plot==
World War II has ended and Major Larry Briggs finds out that his friend Mike has only a short time to live but doesnt know yet. Determined to show Mike a good time before he dies, Larry takes a flying job working for a profiteer, Maris. Everything is set until Maris secretary Susan Cleaver gets shoved on board. Mike falls for Susan and Larry convinces her to play along. Susan, however, has fallen for Larry.

==Cast==

*Alan Ladd as Major Larry Briggs
*Veronica Lake as Susan Cleaver
*Douglas Dick as Captain Mike Perry
*Wally Cassell as Sergeant Pete Rocco
*Luther Adler as Lieutenant Keon
*Morris Carnovsky as Zlec Maris

==Background== The Glass Key both in 1942, The Blue Dahlia in 1946 and Saigon. While Gun, Key and Dahlia all proved to be big box office successes, Saigon did not do as well financially. Ladd continued to remain one of Paramounts top male stars, while Lakes career was in decline. By the end of 1948 her contract with Paramount had expired and the studio chose not to renew it.

The film was one of a series of globe-trotting adventure tales Ladd made, starting with Two Years Before the Mast and Calcutta. Saigon New Adventure Subject for Alan Ladd
Schallert, Edwin. Los Angeles Times (1923-Current File)   24 Oct 1946: A2. 

Filming was meant to start in October 1946 but shooting was pushed back while Ladd had to complete Wild Harvest. PARAMOUNT NAMES LAKE, LADD TO FILM: Studio Will Co-Star Team in Saigon, Adventure Story-- Fenton to Be Director Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   29 Oct 1946: 42.  Nebenzal, Film Producer, Pays $150,000 For World Rights to Madame Butterfly: Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   24 Oct 1946: 44.  

Paramount had previously announced a film called Saigon to be made about the Japanese occupation of China but decided not to make it and used it for this story instead. James Henagan and John Leman worked on the script. ANNABELLA NAMED FOR FOX FILM LEAD: She Will Be Seen in Night the World Shook--Jeanne Crain to Star in Party Line
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   02 Sep 1946: 12. 
==Reception==
The Los Angeles Times called the film "long on atmosphere and short on logic." SAIGON MELODRAMATIC FARE
Scheuer, Philip K. Los Angeles Times (1923-Current File)   05 Mar 1948: 17.  
==See also==
Mess jacket (civil)

==References==
 

==External links==
*  

 
 
 
 
 
 
 