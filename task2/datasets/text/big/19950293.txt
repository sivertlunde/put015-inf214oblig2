Dirty Gertie from Harlem U.S.A.
{{Infobox film
| name           = Dirty Gertie from Harlem U.S.A.
| image          = DirtyGertie.jpg
| image_size     =
| caption        = Poster art for the theatrical release Spencer Williams
| producer       = Bert Goldberg
| writer         = True T. Thompson
| narrator       = Don Wilson Spencer Williams
| music          =
| release        =
| cinematography = John L. Herman
| editing        =
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| distributor    = Sack Amusement Enterprises
}} Spencer Williams and produced and distributed by Sack Amusement Enterprises.

==Plot==
Gertie LaRue (Francine Everett) is a nightclub entertainer from the Harlem neighborhood of New York City.  She arrives on the Caribbean island of "Rinidad" to perform as the headliner in a revue at the Paradise Hotel. Gertie has earned the nickname "Dirty Gertie" for the casual nature in which she entices and then humiliates men.  On the island she attracts the attention of two US military officers—a soldier and a sailor—whom she nicknames "Tight Pants" and "High Pockets," respectively.  The men enjoy sharing Gertie’s affections.  However, Diamond Joe, the owner of the hotel, finds himself falling for Gertie and begins to shower her with gifts.  Gertie also attracts the attention of two missionaries, Mr. Christian and Ezra Crumm, who keep watch on Gertie’s activities.  However, a former boyfriend from Harlem tracks Gertie to the island.  Unable to possess her, the ex-boyfriend kills her while insisting that he loves her.     

==Production==
Dirty Gertie from Harlem U.S.A. is an unauthorized adaptation of the   starring Gloria Swanson and the 1932 Rain (1932 film)|Rain starring Joan Crawford. This adaptation changed the names of the characters, switched the location from the Pacific Rim to the Caribbean, and gave the female lead a career in entertainment (the original concept had her as a prostitute). 
		
It was directed by Spencer Williams, an African-American actor and writer who directed a series of race films during the 1940s for the Dallas, Texas-based producer Alfred Sack, who distributed these all-black productions to cinemas catering to African-American audiences.  Williams appeared in Dirty Gertie from Harlem U.S.A. as a female fortune teller who predicts Gertie’s death. 
 Big Timers No Way Out (1950), before retiring from acting. 

Dirty Gertie from Harlem U.S.A. was a commercially successful title on the race film circuit, but it was not widely seen by white audiences until the 1990s.  The film is a public domain title. 

==DVD release== Region 0 DVD by Alpha Video, as part of a double feature with Sepia Cinderella, on July 31, 2007. 

==See also==
* List of films in the public domain

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 