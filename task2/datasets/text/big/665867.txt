Police Story (1985 film)
 
 
{{Infobox film
| name           = Police Story
| image          = Police-Story-poster.jpg
| caption        = Hong Kong film poster
| film name = {{Film name| traditional    = 警察故事
 | simplified     = 警察故事
 | pinyin         = Jǐngchá Gùshì
 | jyutping       = Ging2 Caat3 Gu3 Si6}}
| director       = Jackie Chan
| producer       = Raymond Chow Leonard Ho
| story          = Golden Way Creative Group
| screenplay     = Jackie Chan Edward Tang
| starring       = Jackie Chan Brigitte Lin Maggie Cheung Chor Yuen Charlie Cho
| music          = Michael Lai Tang Siu Lam
| cinematography = Cheung Yiu Cho
| editing        = Peter Cheung Golden Harvest Media Asia Group
| released       =  
| runtime        = 101 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         =  HK $26,626,760
}} Hong Kong Hong Kong police detective named "Kevin" Chan Ka-Kui.
 The Protector, American film market.
 Best Film 1986 Hong Kong Film Awards. According to Chans autobiography he considers Police Story his best action film.

==Plot==
The Royal Hong Kong Police Force is planning a major undercover sting to arrest crime lord Chu Tao (Chor Yuen). Inspector Chan Ka-Kui (or Kevin Chan in some versions) is part of the operation, along with undercover officers stationed in a shanty town. However, the criminals spot the police and the ensuing car chase cuts through the hillside shanty town. Ka-Kui persists in his chase, eventually following on foot as the drug lord attempts to escape in a double-decker bus. Ka Kui manages to get in front of the bus and bring it to a halt by threatening to shoot the driver with his service pistol|revolver.

Later, Ka-Kui is reprimanded by Superintendent Li for letting the operation get out of hand, but subsequently presented to the media as a model police officer. His next assignment is to protect Chu Taos secretary, Selina Fong (Brigitte Lin), who plans to testify in court about Chu Taos illegal activities. At first, Selina insists that she does not require protection, but after Ka Kui has a fellow policeman break into her apartment and pose as a knife-wielding assassin, she becomes more cooperative.

When Ka-Kui arrives at his apartment with Selina, he is surprised by his girlfriend, May (Maggie Cheung) and her friends, who are throwing a birthday party for him, but May becomes angry with Ka-Kui after seeing Selina only wearing lingerie and Ka-Kuis jacket. Ka-Kui later explains to May that Selina is a witness, but after much bumbling and embarrassment, causing her to leave the scene, though she does go to Chu Taos trial the next day.

Meanwhile, Selina discovers that the attack at her apartment was a sham, and decides to slyly record over her confession about working for Chu Tao that took place in the car ride there. She sneaks away while Ka-Kui is sleeping and is not present at the trial the next day, which ends with failure for the prosecution because of Selinas absence and tampering with the recording.

Though Chu Tao is released on bail, he wants revenge against Ka-Kui. He captures Selina and threatens to kill her to ensure her silence. Ka-Kui finds and frees her, but is attacked by several of Chu Taos men. When fellow Police Inspector Man arrives (Kam Hing Ying), he reveals that he had been working with Chu Tao and thus Selinas capture was merely a ruse to trap Ka-Kui. To Mans grim surprise, the plan is also to include Taos men killing him with Kuis gun to frame him for murder. Now a fugitive cop killer, Ka-Kui must try to catch Chu Tao and clear his name, taking his superindendent as hostage in order to escape custody, though he soon lets his co-operative superior go free.

Selina goes to Chu Taos office at a shopping mall to download incriminating data from Chu Taos computer system. Chu Tao notices this and he and his men rush to the shopping mall to intervene. Ka-Kui and May, who are monitoring Chu Taos activities, follow. In the ensuing carnage, Ka-Kui defeats all of Chu Taos henchmen (and destroys a good portion of the mall). The briefcase containing the computer data falls to the ground floor of the mall, but Chu Tao retrieves it after attacking May. Ka-Kui, at the top floor, slides down a pole wrapped in lightbulbs to the ground floor and catches Chu, but the rest of the police force quickly arrives and prevent him from further taking matters into his own hands. Selina attests to them that Danny Chu killed Inspector Man and evidence of his crimes is in the briefcase. Chus defence attorney shows up and accuses the police of misconduct, prompting a beating from an at-wits-end Ka-Kui, who goes on to extend the beating to Chu Tao before being stopped by his friends.

==Cast==
* Jackie Chan as Sergeant "Kevin" Chan Ka-Kui ("Jackie" on U.S, UK and Australian releases)
* Brigitte Lin as Selina Fong, Chu Taos secretary (as Brigette Lin)
* Maggie Cheung as May, Ka-Kuis Girlfriend
* Chor Yuen as Chu Tao, Crime Lord aka Tom Koo (New Line Cinema Dub)
* Charlie Cho as John Ko, gangster aka John Chow (New Line Cinema Dub)
* Fung Hak-on as Danny Chu Koo
* Kam Hing Yin Inspector Man
* Lam Kwok-Hung as Superintendent Raymond Li
* Bill Tung as "Uncle" Bill Wong Mars as Kim
* Lau Chi-wing as Counsellor Cheung
* Tai Po as Lee / Snake Eyes
* Kent Tong as Tak / Tom
* Wan Fat as Jacknife / Mad Wing
* Bowie Wu as Sha Tau Kok police officer
* Clarence Fok as Photographer
* Money Lo as TV reporter

===Jackie Chan stunt team===
* Chan Tat-kwong
* Johnny Cheung
* Danny Chow
* Fung Hak On
* Benny Lai
* Rocky Lai
* Sam Wong
* Ben Lam
* Chris Li Mars
* Pang Hiu-sang
* Paul Wong

==Production==
  revolver and a climactic fight scene in a shopping mall. This final scene earned the film the nickname "Glass Story" by the crew, due to the huge number of panes of sugar glass that were broken. During a stunt in this last scene, in which Chan slides down a pole from several stories up, the lights covering the pole had heated it considerably, resulting in Chan suffering Burn#Classification by degree|second-degree burns, particularly to his hands, as well as a back injury and dislocation of his pelvis upon landing.   

Edward Tang, the screenwriter for this film and many others, said that he did not write this film the way normal Hollywood screenwriters work. Chan instructed Tang to structure the  comedic film around a list of props and locations: e.g. a shopping mall, a village, a bus, etc. In contrast to this production, most Hollywood films rely on the creativity of the screenwriters to create the plot-elements of a film, which are then forwarded to the director for actual filming.

In an interview with Chan, he discusses the stunt of sliding down the pole covered with lights. As with the clock tower stunt from Project A (1983), Chan described his fear at the thought of performing the stunt. However, during the filming of Police Story, there was the added pressure of strict time constraints, as the shopping mall had to be cleaned up and ready for business the following morning. One of Chans stuntman gave him a hug and a Buddhist prayer paper, which he put in his trousers before finally performing the stunt.   

Stuntman Blackie Ko doubled for Chan during a motorcycle stunt in which his character drives through glass towards a hitman. In the double decker bus scene, Jackie used a metal umbrella because a wooden one kept slipping when he tried to hang onto the bus.

==Box office==
The film grossed HK $26,626,760 at the Hong Kong box office.

==Critical reception==
The English version of this film received a rating of 83% on review aggregator Rotten Tomatoes, based on 12 reviews.   Time Out Film Guide has stated that "In Jackie Chan-land vehicles are for trashing small buildings, while big buildings are for falling off or sliding down... The likeable and graceful Chan directs, sings and performs jaw-dropping stunts."  Film website   has given a favorable review, concluding that "Police Story is a decent action flick that features incredible martial arts action as well as Chans signature death defying stunts."  Film website Cinema Blend has panned it in a review, commenting that the "extreme, guerilla-style filmmaking is unnerving to say the least" and that "the movie falls a bit short" for those who are not fans of the genre. 

==Awards and nominations== 1986 Hong Kong Film Awards
** Won: Best Picture
** Won: Best Action Choreography (Jackie Chan Stunt Team)
** Nominated: Best Director (Jackie Chan)
** Nominated: Best Actor (Jackie Chan)
** Nominated: Best Actress (Brigitte Lin)
** Nominated: Best Cinematography (Cheung Yiu Cho)
** Nominated: Best Film Editing (Peter Cheung)

==Home media== anamorphic widescreen Police Story Trilogy boxed set in Region 0 NTSC format, featuring optional English subtitles and a choice of Chinese-language soundtracks. Hong Kong-based company Kam & Ronsom Enterprise released the first three Police Story films on Blu-ray Disc in June 2009. The first film was released on Blu-ray on 14 September 2009. 

New Line Cinema acquired the rights to the film from Golden Harvest, distributing it on VHS and Laserdisc on August 4th,1998, with 11 minutes of footage cut from the original Hong Kong version and a recycled soundtrack score from J. Peter Robinson. On 19 December 2006, The Weinstein Company released the film on Region 1 NTSC DVD (under their Dragon Dynasty label) with special features and deleted scenes; it was also released in Canada on 23 January 2007. Shout! Factory released Police Story and Police Story 2 as a double feature on DVD and Blu-ray Disc on 16 April 2013. 

==Influence== Born to Fight. Rapid Fire by filming a similar sequence from the mall fight scene, in which Jackies character rams a villain with a motorcycle, through multiple layers of glass.
* The scene where Chan stops a bus in Police Story, inspired a similar scene in the Sylvester Stallone and Kurt Russell film Tango & Cash. theme song sung by Jackie Chan, as its own theme. Since 2009, the same song is re-adopted as the theme song of Police Report, but sung by Hacken Lee. Televised job advertisements for the Hong Kong Police also adopted segments of the song.

==Sequels==
 

===Police Story 2===
 
Police Story 2 ( ), made in Hong Kong films of 1988|1988, features many of the same actors reprising their roles from the original. The story picks up with Chan Ka-Kui demoted to traffic cop for causing so much damage in his apprehension of Chu. Chu has been released from prison on the pretense that he is terminally ill, and Chu and his clan continue to harass Chan and his girlfriend May as Chan gets reinstated to the detective unit when criminal bombers begin extorting money from businessmen.

===Police Story 3: Super Cop===
  Project S (1993). Dimension Films released Police Story 3 in the US in 1996 under the name of Supercop with some edits to the film, the complete replacement of all music and sound effects, and English Dubbing (filmmaking)|dubbing.

===Police Story 4: First Strike===
 
Police Story 4 ( ), made in Hong Kong films of 1996|1996, is the only film in the Police Story series made partially in English language|English. The action shifts away from Hong Kong and East Asia, with a globe trekking espionage plot, lending the film the air of a James Bond adventure. New Line Cinemas US release contained several alterations. Filmed on location in Ukraine and Australia, the film also marks the last appearance of Bill Tung, who plays Chans superior in the series.

===New Police Story===
  reboot of the Police Story series. Chan portrays a disgraced detective named Wing, and acts alongside younger Hong Kong actors including Nicholas Tse, Charlene Choi and Daniel Wu. The story features a more dramatic focus, taking a darker and more serious tone.

===Police Story 2013===
  Hong Kong action crime film starring Jackie Chan in another reboot of the Police Story (film series)|Police Story film series. The film is directed by Ding Sheng, whom previously helmed Chans Little Big Soldier. According to Chan, unlike the previous Police Story films where he portrayed a Hong Kong cop, in the new film he will portray a mainland Chinese officer. Like New Police Story, 2013 is a stand-alone installment with a darker tone than the previous installments, which were comedies.

==See also==
* List of Hong Kong films
* Hong Kong action cinema
* Jackie Chan filmography
* List of Dragon Dynasty releases

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 