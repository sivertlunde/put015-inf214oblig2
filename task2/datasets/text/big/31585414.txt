Simba: King of the Beasts
r 
{{Infobox film
| name           = Simba: The King of the Beasts
| image          = SimbaTheKingoftheBeasts.jpg
| image_size     =
| caption        = Theatrical poster for the film Martin & Osa Johnson
| producer       =  
| writer         =  
| starring       = Martin & Osa Johnson
| music          =  
| cinematography = Martin & Osa Johnson
| editing        =  
| studio         = Martin Johnson African Expedition Corporation
| distributor    =
| released       =  
| runtime        = 87 minutes
| country        = United States silent
| budget         =
}}
 1928 USA|American silent documentary Kenyan veld to his lair. The film, which went on nationwide general release on  , was premiered at the Earl Caroll Theatre in New York on  .      

==Production==
Footage for the film was shot from 1924 to 1927, during the couples second and longest trip to Africa, when spent much of their time in northern Kenya by a lake they dubbed Paradise, at Mount Marsabit. The film Martins Safari (1928) was also made with footage of this trip and some was reused in later productions Congorilla (1932) and Osas Four Years in Paradise (1941).

==Reviews==
Allmovie reviewer Hal Erickson writes that, despite being made, "under the auspices of the American Museum of Natural History," and purporting, "to be an authentic filmed record of the Johnsons most recent foray into Africa," the "expedition" documentary, "seldom lets facts get in the way of a good story." For example, "The films highlight shows the intrepid Mrs. Osa Johnson bringing down a charging rhinoceros with one well-aimed shot. But the reusage of Simba footage in the Johnsons 1932 documentary Congorilla reveals that the rhino was merely scared away by the gunfire -- a classic example of how the truth can be adjusted with the help of a clever editor."   

The "groundbreaking travelogue," was, according to WildFilmHistory, "Filmed in an era when African wildlife was still relatively bountiful, the production shows how the landscape looked in the early twentieth century." "Evading stampeding elephants, employing scores of servants and gamefully shooting down a variety of species," "with the production including provoked behaviour, staged confrontations and animals shot to death on film. Relying heavily on cutting in kills from professional marksmen, numerous hunting scenes culminate in a heart stopping sequence where, with the use of clever editing, the adventurous Mrs Johnson appears to bring down a charging rhinoceros with one well-aimed shot," and. "the film provides an intriguing glimpse not only into 1920s Africa but also into the Johnsons themselves, part of a gung-ho breed that is, in itself, now largely extinct." "Responsible for introducing a whole generation of American movie-goers to the wonders of the African environment, Simba was a large-scale success, detailing wildlife and indigenous tribespeople that had seldom appeared on screen before." 

==References==
 

==External links==
* 

 
 
 
 
 
 