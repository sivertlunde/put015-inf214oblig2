Revenge of the Barbarians
{{Infobox film
| name           =  Revenge of the Barbarians
| image          = Revenge of the Barbarians.jpg
| caption        = 
| director       = Giuseppe Vari
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Daniela Rocca
| music          = Roberto Nicolosi 	
| cinematography = Sergio Pesce 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        =  
| language       = Italian
| budget         = 
| gross          =  sack of Rome in AD 410 by the Visigoths.

This film was written by Gastone Ramazzotti and directed by Giuseppe Vari.

==Cast==
*Daniela Rocca as Galla Placidia  Anthony Steel as Consul Olympus 
*Robert Alda as Ataulf 
*José Greci as Sabina  Honorius
*Evi Marandi as Ameria
*Arturo Dominici as Antemius
*Cesare Fantoni as Alaric I 
*Salvatore Borghese as Gothic Warrior

==See also==
*List of historical drama films

==External links==
*  

 

 
 
 
 
 

 