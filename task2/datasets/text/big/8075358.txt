On Ice (film)
 
{{Infobox Hollywood cartoon cartoon name=On Ice
|series=Mickey Mouse
|image= image size=
|alt=
|caption=
|director=Ben Sharpsteen
|producer=Walt Disney story artist=Webb Smith voice actor=Marcellite Garner Pinto Colvig Walt Disney Clarence Nash
|musician=Bert Lewis
|animator=Paul Allen Milt Kahl Fred Spencer Art Babbitt Johnny Cannon Norman Ferguson Eric Larson John McManus Webb Smith Don Towsley Marvin Woodward layout artist= background artist= Walt Disney Productions
|distributor=United Artists release date=  (USA) color process=Technicolor
|runtime=8 minutes
|country=United States
|language=English preceded by=Plutos Judgement Day followed by=Mickeys Polo Team
}}
On Ice is a Mickey Mouse cartoon released in 1935.

==Plot== Minnie learn Donald pulls Pluto by putting ice skates on his feet and luring him out onto the ice in the third one. The subplots come together when Donald skates around with a kite on his back. The wind kicks up and sends him flying over the waterfall. Mickey hears his cries for help and saves him by pulling on the yarns of his sweater. Donald ends landing right where Goofy is fishing.

==DVD release==
The cartoon is included on the Walt Disney Treasures DVD collections Mickey Mouse in Living Color and The Chronological Pluto - Volume 1 (although its inclusion on the latter is questionable).

==External links==
* 
* 
 

 
 
 
 
 
 

 