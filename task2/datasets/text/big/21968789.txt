The Accusation
{{Infobox film
| name           = The Accusation
| image          =
| caption        =
| director       = Giacomo Gentilomo
| producer       = Luigi Carpentieri Ermanno Donati
| writer         = Franco Brusati Gaspare Cataldo Giacomo Gentilomo
| starring       = Marcello Mastroianni
| music          =
| cinematography = Alvaro Mancori
| editing        = Otello Colangeli
| distributor    =
| released       =  
| runtime        = 99 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

The Accusation ( ) is a 1951 Italian drama film directed by Giacomo Gentilomo.   

==Cast==
* Lea Padovani - Irene
* Marcello Mastroianni - Renato La Torre
* Andrea Checchi - Inspector Constantini
* Marga Cella - Miss Inghirami
* Emma Baron
* Alda Mangini
* Karl Ludwig Diehl - Massimo Ruska
* Amilcare Pettinelli - Donate
* Gaetano Verna
* Mary Genni
* Silvana Muzi
* Alessio Ruggeri
* Maria Pia Spini

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 