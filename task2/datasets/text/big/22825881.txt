The Golden Touch (film)
{{Infobox_Film | name = The Golden Touch image = caption = Theatrical Poster director = Walt Disney producer = Walt Disney studio = Walt Disney Productions distributor = United Artists released  = March 22, 1935
}}

The Golden Touch is a Walt Disney Silly Symphony cartoon made in 1935. The story is based on the Greek myth of King Midas, albeit with a medieval setting.
== Story ==

The extremely rich King Midas never cares for women nor wine and never gets enough of his gold, wishing that everything he touched would turn to gold. One day an elf named Goldie appears in front of him and offers him the Golden Touch, demonstrating its magical power by turning his cat to gold, then claps his hands to change it back. Midas tries to offer up everything he owns in exchange ("My gold, my kingdom, everything for the Golden Touch!"), but is warned by Goldie that "To you, the Golden Touch would prove a golden curse." Midas however derides this -eventually exclaiming "Fiddlesticks! Give me gold, not advice!"- and Goldie gives him the Golden Touch ("I gave thee advice, now I give thee gold."). At first Midas is happy about his newfound power (he turns many things in his garden to gold, then talks to himself in his mirror about turning the Earth and then the Universe to gold), but then he finds out he cant eat and cant drink anymore (even his bite turns a roast chicken to gold). Deprived of his food and fearing starvation, he asks himself in his mirror "Is the richest king in all the world to starve to death?". He hallucinates himself as a golden skeleton form in his mirror which nods in reply to his question. Horrified, Midas tries to flee the castle, but as he approaches the castle gate, he sees his shadow morph into a golden Grim Reaper, after which a terrified Midas flees back to his counting room where the short began. He summons the elf who agrees to take back the Golden Touch in exchange for everything Midas possesses. In return, Midas is given a hamburger ("With onions!").

== History ==
 Walt Disneys Timeless Tales/Wave Two/Volume Three in 2006

== Sources ==
*   in IMDB
*  
*  

 
 
 
 
 
 
 
 
 

 