Kokoda (film)
 
{{Infobox Film |
  name     = Kokoda|
image          = Kokodaposter.jpg|
    writer         = Alister Grierson John Lonie | Christopher Baker Ben Barrack Shane Bourne Angus Sampson Travis McMahon Ewen Leslie |
  director       = Alister Grierson |
  producer       = Catriona Hughes, Leesa Kahn |
  distributor    = Palace Films |
  released   = 2006 |
  country = Australia |
  runtime        = 92 minutes |
  language = English |
  budget         = |
}}
Kokoda (also known as Kokoda - 39th Battalion) is a 2006 Australian film directed by Alister Grierson and is based on the experiences of Australian troops fighting Japanese forces during the 1942 Kokoda Track campaign.

Due to budgetary restrictions, Grierson and co-writer John Lonie were forced to scale down the story, concentrating primarily on the trials and tribulations of one lost patrol.

==Background== division captured. Australian Imperial Force (AIF) were still in the Middle East fighting the Axis. Australia then only had available conscripts who were considered unfit for combat duties. These were known as chocos – it was believed that they would melt away in the heat of battle. The chocos had been kept doing menial tasks such as working at unloading cargo.
 Kokoda Track till they came in contact with Imperial Japanese forces coming along the track from the other direction.

The story of Kokoda is of men from one of these units, under-trained, under-provisioned sent to face battle-hardened Japanese soldiers in a desperate effort to save Australia.

==Plot== 39th Battalion Japanese invasion. AIF arrives Christopher Baker) offers to stay and provide cover. However, the remaining men are cut off and surrounded in the dense jungle, with little hope of escaping. The Australians try to remain hidden until nightfall, when Darko (Travis McMahon) and Jack (Jack Finsterer) decide to go and find out where Blue is. They stumble across Blue, who is tied up and being tortured by Japanese soldiers. Darko and Jack look on helplessly as the Japanese soldiers bayonet him in the stomach and groin, and finally decapitate him with a sword. They return to their hiding place, shaken by what they have seen. The Japanese ambush the section and they run further into the jungle. Caught behind enemy lines in harsh terrain, Jack, to whom the others (including his brother, Max, have deferred) tries to maintain command of a small group of men. Suffering from malaria and dysentery, the remaining six men decide to make their way to Isurava, where the remainder of the 39th are fighting a desperate battle.

One of the soldiers, Sam (Steve Le Marquand), has been injured in the leg and orders the rest of the section to leave him behind. They refuse and he struggles along with a crutch. After a full day of walking, the men are exhausted. The next morning they awake to find Sam gone, having hidden himself in a hollow tree stump to avoid holding them up.

The men continue and are ambushed by a Japanese patrol. The Japanese are all killed, but Max is badly wounded by a gunshot to the stomach and is unable to walk. He is carried by all the men.
 Luke Ford), Darkos number two, head off to Isurava. The journey becomes treacherous and Burkes dysentery is getting worse.

Meanwhile at the village, a few Japanese arrive to search the village and in a desperate attempt to save the life of his mate, Johnno fires at the Japanese and runs into the jungle; however, he is tracked down and gunned down by the Japanese. A day or so later a New Guinea tribesman comes back to inspect the village and finds a badly wounded Max in the hut.

After a gut-wrenching climb, Jack, Darko and Burke are found by the AIF, who take them to Isurava, where the situation is in dire straits. The AIF has arrived but they too are weak from the trek to Isurava. The 39th is no longer a fighting unit and almost all of the men are too sick or wounded to fight. The three men check themselves into a makeshift field hospital for treatment. However an AIF officer comes in and asks for any available men from the 39th to help hold the line. Jack, Burke and Darko volunteer and they are assigned to a position held by men of the 2/16th Infantry.

That night the Japanese attack in waves against the Australian positions. The Australians, who are only equipped with rifles and machine guns, desperately hold off the Japanese. The Japanese are gunned down by the superior firepower of the Australians, but in the final seconds of the fight Burke is shot through the chest and dies in Darkos arms as the fight rages on. The Japanese end the assault and the battle is over.

The next day, the scant remainder of the 39th is paraded at Isurava village. The men are tired and haggard and receive news that they will be taken off the line and that they have just saved Australia from an imminent invasion. After the speech by the 39ths colonel (William McInnes), Jack and Darko withdraw with the rest of the soldiers. (The Australians withdrew from Isurava to take up positions at Brigade Hill.) While withdrawing, Jack and Darko spot Max being carried by Fuzzy wuzzy angels to an aid station. He has survived.

==Based on true story==
The movie was inspired by the true story of a forward patrol led by Lieutenant Sword that found itself cut off from supply at the beginning of the battle for Isurava. After many frightening days making their way back to Isurava with no food, carrying the wounded and suffering from the effects of tropical diseases, they emerged from the jungle near Alola. Upon hearing that the 39th Battalion was about to be overrun, they joined a party of severely wounded men and made their way back to the battle. 

==Style==
The film barely shows the Japanese themselves in any detail – reflecting the claustrophobic jungle warfare – when the enemy could be just in front of you but hidden from sight.

==Reception==
The film received generally positive reviews from critics  . Beyond Hollywood.com called the film a gem.

Paul Byrnes from The Sydney Morning Herald called the film "a glimpse of what this dirty, nasty, very personal corner of the war was like." In Australia the film made over $3 million in its first few weeks and became one of the highest-grossing Australian films of the year. Some critics went as far to say that Kokoda could be compared to Peter Weirs film Gallipoli (1981 film)|Gallipoli.  . Some critics felt that character development was slim except for Jack and his brother who are later found out to be of German-Australian descent. The performances by Jack Finsterer, Travis McMahon and Steve Le Marquand were highly praised by critics, considering most of them had never done a film on this large scale. The film was also praised for its realistic portrayal of the Australian 39th Battalion during the campaign as well as the jungle setting which critics found "haunting, scary and very realistic."  Many Kokoda veterans have also praised the film, many calling it "the closest thing you can get other than experiencing combat on the Kokoda Track yourself".

==Awards== AFI nominations for best costume design and best visual effects, one nomination from the Film Critics Circle of Australia for best cinematography and three from the IF Awards for best cinematography, best editing and best production design.

==Box office==
Kokoda grossed $3,138,501 at the box office in Australia. 

==See also==
*List of Australian films

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 