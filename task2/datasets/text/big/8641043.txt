Labou
{{Infobox film |
 name = Labou |
 image = LabouPoster.jpg |
  producer = Sheri Bryant|
 writer = Greg Aronowitz (story) Sheri Bryant and Greg Aronowitz (screenplay) |
 starring = Chris Violette  Monica May   Kelson Henderson   Barnie Duncan   Marissa Cuevas   Darnel Hamilton   Bryan James Kitto    Earl J Scioneaux Jr |
 director = Greg Aronowitz |
 distributor = MGM |
 runtime = 98 minutes |
 language = English |
 music = Nathan Wang, Christie Yih Chong |
 released =   |
 budget = $5 Million est. |
 }}

Labou is an independent film produced by Sheri Bryant and written and directed by   and directed many of the episodes.  Many of the same actors that appeared in that season of Power Rangers are also seen in Labou, including Chris Violette, Kelson Henderson, Barnie Duncan and Monica May. The film has received three prestigious awards including Best of Fest at the Chicago International Childrens Film Festival, Best Family Feature at WorldFest 2008 Houston, and Best Feature at Bam Kids Film Festival in NY; and has also been approved by the Dove Foundation, KidsFirst!, and NAPPA.

Production was interrupted by Hurricane Katrina, forcing the cast and crew to abandon production and return early 2006.   The film has a dedication at the end to the people of New Orleans.

New Orleans Mayor Ray Nagin stars in Labou as the Mayor of New Orleans. Local jazz legend Ellis Marsalis plays the wise "Jazz Man" in the picture.

Drew Struzan designed the films poster and the website was created by Ian J. Duncan. 

==Plot summary==
Three unlikely friends set out on a journey to find the dreaded Ghost of Captain LeRouge whose treasure laden ship was lost in the Louisiana bayou over two hundred years ago. What they find is an adventure beyond their wildest imagination and the magical swamp creature "Labou" whose whistles are rumored to be the original inspiration for jazz.

With the help of Labou, the kids race to stay one step ahead of two crazy oil tycoons and discover the long lost treasure in time to save the swamps from destruction.

==Cast==
*Bryan James Kitto as Toddster 
*Marissa Cuevas as Emily Ryan
*Darnell Hamilton as Gavin Thomson
*Chris Violette as Reggie
*Monica May as Librarian
*Kelson Henderson as Clayton
*Barnie Duncan as Captain Lerouge

==References==
 

==External links==
*  

 
 
 
 
 
 