Munchies (film)
{{Infobox film|
 name = Munchies |
 image = MunchiesCover.jpg|
 writer = Lance Smith |
 starring =Harvey Korman  Charlie Stratton  Nadine Van der Velde | Bettina Hirsch |
 producer = Roger Corman |
 distributor = New Concorde |
 released = 1987 |
 runtime = 83 minutes |
 language = English |
 music = Ernest Troost |
 theme song = Bruce Goldstein |
}} 1987 comedy horror film starring Harvey Korman, Charlie Stratton, and Nadine Van der Velde.  Clearly inspired by the success of Gremlins, and directed by that films editor, Tina Hirsch, the film features a remarkably similar plot line. It spawned two sequels, Munchie and Munchie Strikes Back, which possess no relation to the original save the title.  These two films dealt with an impish wish-granting creature named Munchie.

==Plot summary==

The protagonist, space archaeologist Simon Watterman, discovers a fossilized "Munchie" in a cave in Peru. Bringing the specimen back to the United States, Wattermans son and girlfriend accidentally reanimate it and name it Arnold. Cecil Watterman, Simons evil twin brother and snack food entrepreneur, kidnaps Arnold while Cindy and Paul are making out.

When Arnold is hurt by his kidnappers, it becomes aggressive and attacks Cecils adopted son. Attempting to kill Arnold, they chop him into quarters, but instead of dying, Arnold multiplies into four new munchies. The quartet of creatures develop a love of women, beer, attacking people, and junk food in the process.

==Cast==
*Harvey Korman as Cecil Watterman and Simon Watterman
*Hardy Rawls as Big Ed
*Robert Picardo as Bob Marvalle
*Wendy Schaal as Marge Marvalle
*Paul Bartel as Dr. Crowder
*Ellen Albertini Dow as Little Old Lady
*Steven Bernstein as Dean
*Frank Welker as the voice of Munchie Fred Newman as the voice of Munchie 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 


 