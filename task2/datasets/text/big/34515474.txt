The Willow Tree (1920 film)
{{infobox film
| name           = The Willow Tree
| image          = Viola Dana The Willow Tree Film Daily 1919.png
| imagesize      = 175px
| caption        = Ad for film
| director       = Henry Otto
| producer       = Screen Classics Incorporated Maxwell Karger
| based on       =  
| writer         = Harrison Rhoades (play) June Mathis (adaptation, scenario)
| starring       = Viola Dana
| music          =
| cinematography = John Arnold
| editing        =
| distributor    = Metro Pictures
| released       =   reels
| country        = United States Silent (English intertitles)
}} silent film directed by Henry Otto and distributed by Metro Pictures. The film is based on a Broadway play, The Willow Tree, by Joseph Henry McAlpin Benrimo|J. H. Benrimo and Harrison Rhodes. Fay Bainter starred in the Broadway play in 1917. The film stars Viola Dana and is preserved in the George Eastman House Motion Picture Collection.   

==Cast==
*Viola Dana - O-Riu
*Edward Connelly - Tomotada
*Pell Trenton - Ned Hamilton
*Harry Dunkinson - Jeoffrey Fuller
*Alice Wilson - Mary Fuller
*Frank Tokunaga - John Charles Goto
*Togo Yamamoto - Itomudo
*George Kuwa - Kimura
*Tom Ricketts - Priest

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 
 


 