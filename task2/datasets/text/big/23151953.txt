Ted & Venus
Ted David Robbins.

The film was initially released theatrically in the United States on December 20, 1991. http://www.imdb.com/title/tt0103057
/releaseinfo  and on home video in 1993. On March 1, 2005, the film was released on DVD in Canada by Legacy Entertainment, but in full frame format and without any bonus material. The DVD has since been discontinued, and as of 2010, there are currently no plans for a new DVD of the film.

==Cast==
*Bud Cort	 as 	Ted Whitley
*James Brolin	as 	Max Waters Kim Adams	as 	Linda Turner
*Carol Kane	as 	Colette
*Pamella DPella	as 	Gloria
*Brian Thompson	as 	Herb
*Rhea Perlman	as 	Grace
*Woody Harrelson	as 	Homeless Vietnam Veteran
*Martin Mull	as 	Teds Attorney
*Roberta Wallach	as 	District Attorney
*Timothy Leary	as 	Judge William H. Converse
*Tricia ONeil	as 	Judge Katherine Notch
*Tony Genaro	as 	Bailiff
*Vincent Schiavelli	as 	Publisher
*Andrea Martin	as 	Bag Lady
*Cassandra Peterson	as 	Lisa
*Tracy Reiner	as 	Shelly
*Arleen Sorkin	as 	Marcia Pat McCormick	as 	Marcias Elderly Boyfriend
*Gena Rowlands	as 	Mrs. Turner
*Lily Mariye	as 	Rose
*Bettye Ackerman	as 	Poetry Award Presenter
*John Blyth Barrymore	as 	Patient #1
*Rob Moran	as 	Patient #2
*Zoe R. Cassavetes	as 	Waitress with Attitude Chad Taylor	as 	Chainsaw Juggler
*Joe Paul	as 	Wino
*Tamara De Treaux	as 	Park Bench Lover
*Peter Koch	as 	Cop #1
*Norma Maldonado	as 	Cop #5

==Reviews==
* 
* 
* 

==References==
 

==External links==
*http://www.imdb.com/title/tt0103057/maindetails

 
 
 
 
 
 


 