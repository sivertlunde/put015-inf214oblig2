Stico
{{Infobox film
| name           = Stico
| image          = 
| image size     = 
| caption        = 
| director       = Jaime de Armiñán
| producer       = 
| writer         = Jaime de Armiñán Fernando Fernán Gómez
| starring       = Fernando Fernán Gómez
| music          = 
| cinematography = Teodoro Escamilla
| editing        = José Luis Matesanz
| distributor    = 
| released       = 4 March 1985
| runtime        = 105 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
 1985 cinema Spanish drama film directed by Jaime de Armiñán. It was entered into the 35th Berlin International Film Festival where Fernando Fernán Gómez won the Silver Bear for Best Actor.   

==Cast==
* Fernando Fernán Gómez as Don Leopoldo Contreras de Tejada
* Agustín González as Gonzalo Bárcena
* Carme Elias as María (as Carme Elías)
* Amparo Baró as Felisa (as Amparo Baro)
* Mercedes Lezcano as Margarita
* Manuel Zarzo as Claudio
* Beatriz Elorrieta
* Manuel Torremocha
* Toa Torán
* Sandra Milhaud
* Bárbara Escamilla
* Vanesa Escamilla
* Manuel Galiana as Luis Cuartero

==References==
 

==External links==
* 

 
 
 
 
 
 