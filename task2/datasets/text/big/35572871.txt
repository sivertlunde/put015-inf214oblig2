Confession of a Child of the Century
 
{{Infobox film
| name           = Confession of a Child of the Century
| image          = 
| caption        = 
| director       = Sylvie Verheyde
| producer       = Bruno Berthemy
| writer         = Sylvie Verheyde Alfred de Musset
| starring       = Charlotte Gainsbourg
| music          = 
| cinematography = Nicolas Gaurin
| editing        = 
| distributor    = 
| released       =  
| runtime        = 125 minutes
| country        = France
| language       = English
| budget         = $4.3 million
| gross          = $184,016 
}}

Confession of a Child of the Century ( ) is a 2012 French drama film directed by Sylvie Verheyde. The film competed in the Un Certain Regard section at the 2012 Cannes Film Festival.       The film is an adaptation of Alfred de Mussets 1836 autobiographical novel of the same name.

==Cast==
* Charlotte Gainsbourg as Brigitte
* Pete Doherty as Octave
* Lily Cole as Elise
* August Diehl as Desgenais
* Volker Bruch as Henri Smith
* Joséphine de La Baume as Desgenaiss Mistress
* Guillaume Gallienne as Mercanson
* Rebecca Jameson as (voice)
* Effi Rabsilber as Cantatrice
* Rhian Rees as Madame Levasseur
* Karole Rocher as Marco
* Julia Schmelzle as Maîtresse
* Kathrin Anna Stahl as Chanteuse
* Diana Stewart as Brigittes aunt

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 