The Hidden Eye
{{Infobox film
| name           = The Hidden Eye 
| image          = The Hidden Eye poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Richard Whorf
| producer       = Robert Sisk 
| screenplay     = George Harmon Coxe Harry Ruskin
| story          = George Harmon Coxe 	 Edward Arnold Ray Collins Paul Langton William Bill Phillips Thomas E. Jackson David Snell
| cinematography = Lester White 
| editing        = George Hively
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Edward Arnold, Ray Collins, Paul Langton, William Bill Phillips and Thomas E. Jackson. The film was released on August 31, 1945, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast == Edward Arnold as Capt. Duncan Maclain
*Frances Rafferty as Jean Hampton Ray Collins as Phillip Treadway
*Paul Langton as Barry Gifford
*William Bill Phillips as Marty Corbett
*Thomas E. Jackson as Insp. Delaney
*Morris Ankrum as Ferris Robert Lewis as Stormvig
*Francis Pierlot as Kossovsky
*Sondra Rodgers as Helen Roberts
*Theodore Newton as Gibbs  Jack Lambert as Louie
*Raymond Largay as Arthur Hampton 
*Leigh Whipper as Alistair
*Byron Foulger as Burton Lorrison
*Lee Phelps as Polasky
*Eddie Acuff as Whitey
*Bob Pepper as Sgt. Kramer
*Clyde Fillmore as Rodney Hampton
*Friday as Himself

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 