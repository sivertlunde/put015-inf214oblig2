Child of Divorce
{{Infobox film
| name           = Child of Divorce
| image_size     =
| image	         = Child of Divorce FilmPoster.jpeg
| caption        =
| director       = Richard Fleischer
| screenplay     = Lillie Hayward
| based on     =  
| narrator       =
| starring       = Sharyn Moffett Regis Toomey Madge Meredith
| music          = Leigh Harline
| cinematography = Jack MacKenzie
| editing        = Samuel E. Beetley
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 62 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Child of Divorce is a 1946 film directed by Richard Fleischer. It was the first film that he directed.

==Plot Summary==

Young Roberta "Bobby" Carter, only eight years old, catches her mother Joan as she kisses a man who isnt her father in a park. She is especially embarrassed, since her friends are present and recognize her mother.

Bobbys father Ray is away on a business trip, as he so often is, but comes home all of a sudden, bringing a small toy piano as a gift to Bobby. Joan tries to collect enough courage to tell her husband about her affair, but backs out in the last second.

Bobby is bullied for her mothers antics and romantics and ends up asking God to make her parents fall back in love. Unaware of her daughters discovery, Joan continues to see her lover, Michael Benton.

Soon Ray becomes suspicious because of Joans frequent absence from their home and asks her about it. Joan confesses that she is seeing another man and that she wants a divorce.

Bobby watches from a hidden position how her parents talk, and how her father slaps her mother in the face. Joan flees the house and is followed by the desperate Bobby. Joan tells her daughter that she is leaving the house and her father immediately and that she is taking Bobby with her. Bobby is crushed.

Months later, Bobby is asked to the stand in her parents divorce trial, as a witness of her mothers infidelity, but she refuses to leave any information. Her parents divorce and a judge grants Joan custody of Bobby for all year except summer. Later, Joan marries Michael but Bobby refuses to accept Michael as her stepfather.

Michael grows tired of Bobbys behavior and tells Joan that the girl is breaking their marriage apart. When Bobby returns to her father in the summer, she is introduced to his new fiancé, Louise Norman, and gets even more upset.

A psychiatrist tells Joan and Ray that bobby needs stability and continuity in her life to cope, and strongly suggests that only one of them should have sole custody over her. None of the parents feels up to this task, and instead Bobby is sent away to a boarding school.

Bobby is eventually visited by her parents, one at a time, and one of her schoolmates tell her that she will be used to being alone. To the sound of church bells playing the same tune as on her toy piano, Joan vows to herself that she will never leave her own children when she grows up, and tuck them to bed every night. 

==Cast==

*Sharyn Moffett as Roberta "Bobby" Carter
*Regis Toomey as Ray Carter
*Madge Meredith as Joan Carter Benton
*Walter Reed as Michael Benton Una OConnor as Nora
*Doris Merrick as Louise  
*Harry Cheshire as Judge
*Selmer Jackson as Dr. Sterling
*Lillian Randolph as Carrie
*Pat Prest as Linda
*Gregory Muradian Freddie

==External links==
* 

==References==
 

 

 
 
 
 
 
 
 
 
 
 