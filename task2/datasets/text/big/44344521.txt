A Lowland Cinderella
 
{{Infobox film
| name           = A Lowland Cinderella
| image          =
| caption        =
| director       = Sidney Morgan 
| producer       = 
| writer         = S.R. Crockett (novel) George Foley   Mavis Clair
| music          = 
| cinematography = Stanley Mumford 
| editing        = 
| studio         = Progress Films
| distributor    = Butchers Film Service   Second National Film Corporation (US)
| released       = December 1921  
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent romance George Foley.

==Cast==
* Joan Morgan as Hester Stirling  
* Ralph Forbes as Master of Darrock George Foley as David Stirling 
*  Mary Carnegie as Mrs. Torpichan  
* Mavis Clair as Ethel Torpichan  
* Nell Emerald as Megsy 
* Eileen Grace as Claudia Torpichan  
* Charles Levey as Dr. Silvanus Torpichan   Kate Phillips as Grandmother Stirling  
* Cecil Susands as Tom Torpichan   Frances Wetherall as Duchess of Niddisdale

==References==
 

==Bibliography==
* Low, Rachael. The History of the British Film 1918-1929. George Allen & Unwin, 1971.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 