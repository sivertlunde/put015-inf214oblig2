The Pact (2012 film)
{{Infobox film
| name           = The Pact
| image          = The-pact-poster.jpg Nicholas McCarthy
| producer       = Ross M. Dinerstein
| writer         = Nicholas McCarthy
| starring       = {{plainlist|
* Caity Lotz
* Casper Van Dien
* Agnes Bruckner
* Haley Hudson
* Kathleen Rose Perkins
* Samuel Ball
* Mark Steger
}}
| music          = Ronen Landa
| cinematography = Bridger Nielson
| editing        = Adriaan van Zyl
| distributor    = {{plainlist|
* Entertainment One
* IFC Midnight
* ContentFilm International
}}
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $400,000 
| gross          = $7.2 million 
}} Nicholas McCarthy and starring Caity Lotz and Casper Van Dien. The film was made following the success of McCarthys short film of the same name which premiered at the 2012 Sundance Film Festival. The film follows Annie, whose mother has recently died, as she tries to discover what caused her sister, Nicole, and her cousin, Liz, to disappear. The film premiered at the 2012 Sundance Film Festival on January 20, opening to generally favourable reviews. The film was released in the United States on June 6, 2012 in select cinemas and 8 May 2012 in the UK and Ireland. The film was released on DVD and Blu-ray Disc in the UK on October 1, 2012. 

== Plot ==
Nicole Barlow is finalizing preparations for her mothers funeral at her childhood home in San Pedro, California. Her sister, Annie does not want to attend, reminding Nicole of the way their mother used to treat them. Nicole tries to contact her cousin, Liz and her daughter Eva via video call. After losing the connection, Nicole sees an open door, leading into a dark room and walks inside.

Annie arrives, having been informed that Nicole is missing, and finds Nicoles phone along with a photo. That night, she awakens from a bad dream and finds a photo of two pregnant women—her mother and a woman in a floral dress. The next day, Annie attends her mothers funeral, and it is shown that Annie has one blue eye and one green eye, but her mother didnt.  After the funeral, Annie meets Liz and Eva, and the three return to Annies mothers house. Later, Annie dreams of a shirtless man crying, and, while she sleeps, her phone shows a map, pinpointing an address. When Annie awakens and sees a figure in the hall, she enters Lizs room and finds her bed empty. An unseen force tries to attack Annie, but she is able to escape the house with Eva.

The police dont believe Annies story, with one officer, Bill Creek, implying her guilt in the disappearances. In a motel, Annie notices the address on her phone, finding a blurred picture of a figure in a floral dress. That night, she dreams of the crying man again, as well as a decapitated woman in a floral dress; still in the dream, Annie tries to flee from the room but the door slams shut. In the morning she wakes up, extremely agitated. Annie, accompanied by Bill Creek, return to her mothers house where they discover a hidden room, one which Annie has absolutely no recollection of, and, when Bill and nobody else will take her seriously, recruits her psychic friend, Stevie. Stevie, Annie, and Stevies protective brother Giles go to the house. In the hidden room, Stevie has a hysterical fit, repeating the name "Judas". They then see the corpse of a woman in a floral dress floating above them, and Annie realizes that it is not her mother haunting the house. Outside, Giles blasts Annie for putting his sister Stevie in harms way. They have a brief altercation, and Giles angrily drives away with Stevie, leaving Annie all by herself. Researching Judas online, Annie finds a serial killer, dubbed the "Judas Killer", who decapitated a woman in a floral dress, Jennifer Glick. Annie also discovers that her mother has a brother, and that both had connections with Glick. 

Creek investigates the house after finding a mysterious photo pointing to a cupboard, where there is a secret door leading into the hidden room. Creek is then murdered by an unknown assailant. Desperate for answers, Annie uses a makeshift Ouija board to contact Glick, who confirms her suspicions that Glick was murdered by her uncle, the Judas killer. Annie sees Judas come up from a secret hatch, and hides, discovering the bodies of Creek and Nicole. While Judas cries, Annie takes Creeks gun but is overpowered by Judas and knocked unconscious. She awakens to find herself tied up and imprisoned in the closet, but manages to escape, stabbing Judas with a coat hanger. Before Judas can kill her, Glicks spirit pulls her away, to Creeks gun. Annie shoots Judas, killing him, noting that he has heterochromia as well. In the aftermath, Annie is awarded custody of Nicoles daughter, Eva. With Liz and Nicole dead, the house has been sold and is under renovations. In the final scene, which may or may not be a part of Annies dream, Judas eye opens, and he looks around.

==Cast==
*Caity Lotz as Annie Barlow
*Casper Van Dien as Bill Creek
*Agnes Bruckner as Nicole Barlow
*Mark Steger as Charles ("Judas")
*Haley Hudson as Stevie
*Kathleen Rose Perkins as Liz
*Samuel Ball as Giles
*Bo Barrett as Jesse
*Dakota Bright as Eva
*Jeffrey T Ferguson as Officer Benson
*Rachael Kahne as waitress
*Santiago Segura as dishwasher
*Sam Zuckerman as County Clerk

== Critical reception ==
Rotten Tomatoes, a review aggregator, reports that 67% of 33 surveyed critics gave the film a positive review; the average rating was 5.7/10.  Metacritic rated it 54/100 based on ten reviews.  Alan Bacchus of DailyFilm said that the film was "smart, well written and genuinely scary" and gave the film three out of four stars, a rating of good. 

== Sequel ==
  VOD in September 2014 and was given a limited theatrical release the following month. The film had Lotz, Steger, and Hudson returning to perform their same roles and True Blood actress Camilla Luddington was brought on to perform in the leading role of June Abbott. McCarthy did not return to direct the film, which was instead written and directed by Dallas Hallam and Patrick Horvath. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 