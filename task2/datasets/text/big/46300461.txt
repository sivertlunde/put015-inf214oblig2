My Italian Secret: The Forgotten Heroes
 
{{Infobox film
| name           = My Italian Secret: The Forgotten Heroes 
| image          = File:My Italian Secret cover.jpg
| alt            = 
| caption        = 
| film name      =  
| director       =  Oren Jacoby  
| producer       = Oren Jacoby  
| writer         =  Oren Jacoby  
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = Isabella Rossellini, Robert Loggia
| music          = 
| cinematography = Gerardo Gossi, Robert Richman  
| editing        = Deborah Peretz  
| production companies =  Storyville Films
| distributor    =  
| released       =  
| runtime        = 92 minutes  
| country        = U.S.A.
| language       = 
| budget         = 
| gross          =  
}}
My Italian Secret: The Forgotten Heroes is a 2014 documentary film, directed and written by Oren Jacoby, that tells the story of the rescue of thousands of Italian Jews during World War II by ordinary and prominent Italians, including the champion cyclist Gino Bartali. The film had its U.S. premiere at the Hamptons International Film Festival in October 2014,     and opened at theaters in Los Angeles and New York in March 2015.    

==Synopsis==

The film tells its story by relating the accounts of Jewish survivors "who return to Italy in their late adulthood to revisit the scenes of their worst nightmares: hidden in terror, fleeing in desperation, separated from loved ones, saying final goodbyes without knowing they were final."      
  used bicycle training as a cover for secret efforts to rescue Jews. ]]
The film, narrated by Isabella Rossellini, includes dramatic reenactments in addition to interviews with survivors and relatives of the rescuers. It describes how many Italians, including Roman Catholic priests, risked their lives to hide Jews from Nazi troops after the German occupation of Italy in 1943. Among them was Bartali, whose words are spoken in a voiceover by actor Robert Loggia. 
 Fascist values, who proved Italians were part of the "master race." But Bartali rejected Fascism and opposed its antisemitism|anti-Semitic policies. During the war,  he traveled throughout Italy on his bicycle while pretending to train for competitions, delivering documents for hidden Jews. He did so at the behest of the Archbishop of Florence, where he lived. Bartali never spoke of his wartime exploits after the war, not even to his family, and only did so late in life. Bartali risked his own life and the life of his family by his activities.     In 2013, Bartali was recognized as a "Righteous Among the Nations" by Yad Vashem for his efforts to aid Jews during World War II.   
 Fatebenefratelli hospital on Tiber Island in Rome, who established a special ward to hide Jews. He said that the patients in the ward were suffering from a fatal and contagious disease, to discourage entry by Nazi troops.    

Among the survivors profiled in the film is a woman named Charlotte Hauptman, now in her eighties, who as a child had been rescued from the Nazis along with her parents in Calabria, Venice, and then Marche, where an entire village conspired to harbor them despite the dangers posed by German troops. 

About 80 percent of the Jews in Italy survived during World War II because of Italian rescuers, and Bartali alone rescued hundreds if not thousands of Jews and anti-Fascist Italian resistance movement|partisans.  

==Critical response==

The Hollywood Reporter called the film "uplifting as it is moving" and a "valuable addition to the ever-growing canon of Holocaust-themed documentaries," but said that it was marred by its "de rigueur dramatic reenactments.".  The Los Angeles Times said it was "compelling if unsuitably titled" and that the "carefully crafted film deftly blends archival footage with dramatic re-creations and interviews with surviving family members to illuminating effect." The reviewer said that Loggias rendition of Bartalis words was "distractingly delivered" and that "it would have been more effective to simply let his heroic actions speak for themselves."   

The New York Times said that the film "unfolds in a somewhat standard testimonial documentary format," and called the soundtrack "heavy-handed." The reviewer said Jacoby "doesnt give My Italian Secret much structural or chronological organization," and said that its "anecdotal presentation sometimes seems more suited for museum browsing than for viewing in a theater."  A columnist for The Jewish Daily Forward said the film suffered from lack of focus as it alternated between Bartalis story and other stories of rescue and betrayal, but said the film "retrieves pieces of the fragile past, revives the honor of a great many brave Jewish and Gentile Italians, and restores to one athletic Italian hero his eternal moral achievement." 

==See also==

* Gino Bartali
* History of the Jews in Italy

==References==
 

==External links==
*  
*   
*  

 
 
 
 
 
 
 