Far Marfa
 
Far Marfa is a 2013 feature film written and directed by Cory Van Dyke in Marfa, Texas|Marfa, Texas, USA. http://www.imdb.com/title/tt1709657/ 

==Plot==
The story, about lost art, both literally and figuratively, follows the journey of Carter Frazier, a down on his luck music producer hiding out in the tiny West Texas town of Marfa when a strange man gives him a (famously lost) piece of modern art, then turns up dead later that day. Carter seeks the art after it goes missing, hoping to fund his dream of building a music studio and improve his foundering existence, but hes thwarted by cops, local characters and, ultimately, fate along the way.

==Cast and Crew==
The cast includes Johnny Sneed, Jolyn Janis, Jessie Bernstein, Julie Mintz and numerous local cast and crew from Marfa. Van Dyke and Jennie Lyn Hamilton produced the film. Carolyn Pfeiffer executive produced the film.  Iskra Valtcheva is the Cinematographer and Graham Reynolds is the Composer. The editor is named Hugo Friedman.

==Reception==
Far Marfa screened at numerous festivals across the US and Europe where it received unanimously positive, often rave reviews, and sold out a majority of its screenings. Louis Black in the Austin Chronicle called it ""a layered film - a human comedy and a metaphysical mystery as well as a consideration of art and romance. The landscape, texture, sensibility, and atmosphere of Marfa is what really makes this film so captivating and unique. All of this so sweetly comes together under the visionary and skilled hand of director, Cory Van Dyke. Highly recommended.". 

Gerald Peary, film critic, The Arts Fuse wrote: "If theres such a thing as an organic film feature than its surely Far Marfa, Cory Van Dykes delightful home grown movie with local actors who are screen naturals, splendid free-range locales, and a nifty little"neo-noir" Texas story dramatized by Van Dyke with humor and grace." -

Don Simpson at Smells Like Screen Spirit says "Oblique clues play like Oblique Strategies, sending Carter along on a surrealist string of events tied together by random coincidences and blind fate. Its sort of like Slacker-meets-The Long Goodbye in a small world surrounded by the sprawling landscapes of West Texas and populated with artists, musicians and bohemian philosophers — in other words, Marfa, Texas. 8/10."  

At Slackerwood, Debbie Cerda wrote that Far Marfa is "a hilarious journey full of encounters with small-town druggies and the local art scene snobs across the visual imagery of Marfa."  

Chad Nance of The Camel City Dispatch said: “Far Marfa is a small treasure of a film that entertains, amuses, and in the end offers up a measured spoonful of hope to go along with the rather grim realities of the early 21st Century … In the end Van Dyke’s wonderful gem of a movie comes down to one idea… work, specifically working with your hands to make or create something tangible. It is a rebooted American Dream from a generation who had many of their opportunities squandered by the generation before. The end of this new journey may not be the glories of financial riches, but the satisfaction of a job well done and the knowledge that even if you never sell much, that is still better than selling out.”  

In the Winston-Salem Journal, Susan Gilmore called it "a lot of fun -- surreal, artistic and existential."  

==Screenings==

Far Marfa screened at the Lone Star Film Festival   in   at the Stateside / Paramount Theater as the first narrative feature in their weekly independent film series. The screening was co-presented by the Austin Film Societys Texas Independent Film Network.  It screened at The Riverrun Film Festival in April, 2013.  and was voted by audiences as the best (narrative) American Indi. It played at the Manhattan Film Festival on June 23.  and had its international premiere at the 25th Galway Film Fleadh in Ireland on July 13.  On August 3rd it screened at the Plaza Classic Film Festival in El Paso, TX.

== References ==
 

 
 