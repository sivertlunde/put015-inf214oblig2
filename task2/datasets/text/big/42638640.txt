Shamitabh
{{Infobox film
| italic title   = no
| name           = Shamitabh
| image          = Shamitabh.jpg
| caption        = Promotional Poster
| director       = R. Balki
| producer       = Sunil Lulla R. Balki Rakesh Jhunjhunwala R. K. Damani Amitabh Bachchan Abhishek Bachchan Sunil Manchanda Shobha Kapoor Ekta Kapoor Dhanush
| writer         = R. Balki
| starring       = Amitabh Bachchan Dhanush Akshara Haasan
| music          = Ilaiyaraaja 
| cinematography = P. C. Sreeram
| editing        = Hemanti Sarkar 
| studio         = Hope Productions Amitabh Bachchan Corporation Wunderbar Films Balaji Motion Pictures MAD Entertainment Ltd 
| released       =      }}
| country        = India
| language       = Hindi
| runtime        = 
| budget         =   
| gross          =   
}}
Shamitabh is a 2015 Indian drama film written and directed by R. Balki.   The film features Amitabh Bachchan, Dhanush and Akshara Haasan, in her debut, in the lead roles.    The film is jointly produced by Sunil Lulla, Balki, Rakesh Jhunjhunwala, R. K. Damani, Amitabh Bachchan, Abhishek Bachchan, Sunil Manchanda and Dhanush under their respective production banners. Ilaiyaraaja composed the soundtrack album and background score, while cinematography was handled by P. C. Sreeram.    

==Plot==

 

Daanish (Dhanush (Actor)|Dhanush), a mute whose childhood dream was to become a Bollywood actor, reaches Mumbai to fulfill his dream. He tries to enter the film city there, but gets stopped by the guards there. He trespasses the gate and comes across an Assistant Director, Akshara Pandey (Akshara Haasan), who is amazed by his acting. She approaches a director for requesting a role for Daanish in a film, but the director refuses after he comes to know that Daanish cannot speak.

Daanish and Akshara go to a voice hospital to get his check-up, but doctors declare his vocal chords are fully paralyzed and are unable to be treated. But, they assure him that with their advanced technology, Danish could have a voice transfer. They fit a chip inside Daanishs larynx with which a person could transfer their voice into the chip, and when the person would speak, the words would also come from Danishs mouth. They set out to search for a person who could speak for Daanish every time, when they come across an old drunkard, Amitabh Sinha (Amitabh Bachchan), lying on the footpath. Amitabh also wanted to become an actor but was rejected because of his voice.

Daanish approaches the director again, who agrees to launch him this time, but wanted him to change his name. Daanish searches for a name and concludes on Shamitabh (a portmanteau of DaaniSH and AMITABH), giving credit to the person who provided him with voice. Daanish makes a couple of films and turns out to be a hit actor to which Amitabh realises was only because of him and an ego problem between the two starts to rise which comes to a head after Amitabh pulls a gun on a police and is jailed while Daanish is at London for a lecture at a university. They both separate after a confrontation at the airport and start making their own films, where Daanish plays the role of a mute person and Amitabh dubbing the voice for another actor. Both films fail commercially.

Akshara makes both of them realise they are nothing without each other and gets them together for a film titled Sorry, which she is directing. Meanwhile, Akshara and Daanish tell that they like each other. Amitabh and Daanish are on the way to the venue of the press meet for Sorry, and on the way become engrossed in a fun conversation when their car is hit by a trailer; the accident kills Daanish and damages Amitabhs larynx, rendering him mute.

Some time later, Amitabh paces around Daanishs grave with the script of Sorry, imagining that Daanish is still alive and practising his lines for the film.
 

==Cast==
* Amitabh Bachchan as Amitabh Sinha/Robert/Shamitabh
* Dhanush as Daanish/Shamitabh
* Akshara Haasan as Akshara Pandey/The Beauty
* Rekha as Herself  Abhinaya as Herself
* Rukmini Vijayakumar as Herself
* Rajeev Ravindranathan as Media Reporter
* Vandita Shrivastava

;Cameo appearances
 
* Rohit Shetty
* Karan Johar
* Mahesh Bhatt
* Anurag Basu
* Rakesh Omprakash Mehra
* Rajkumar Hirani
* Gauri Shinde
* Javed Akhtar
* Boney Kapoor
* Ekta Kapoor
* Krishika Lulla
 

==Production==

===Development===
In August 2013, reports announced that  . 16 November 2013. Retrieved 20 August 2014.  In addition to portraying one of the films central characters, Amitabh Bachchan had also bankrolled the venture.  In May 2014, Balki revealed that the film would be titled Shamitabh, which also sounded similar to the Bollywood stars name,  whilst also revealing that half the films shooting had been completed.  Amitabh Bachchan lent his voice to Dhanush, who plays a dumb aspiring actor in the film.  The films title derived from the duo’s names in which Sh from Dhanush was added as a prefix to Amitabh, therefore becoming Shamitabh. 

===Casting=== Abhinaya of Nadodigal (2009) fame was selected to play a crucial role in the film.   Actress Rekha was selected to play a supporting role.  Vandita Shrivastava, an MBA-turned-actress, stated that she had a role in the film. 

===Filming=== Lapland region of Finland and in Helsinki.   The crew shot some scenes at the Seven Hills Hospital, the same place where Amitabh Bachchan underwent two abdominal surgeries and saw the birth of Aaradhya, his granddaughter.  Some scenes were also shot in London.  The team took a break in mid-July 2014 to celebrate Dhanushs birthday and the success of his 25th film, Velaiyilla Pattathari (2014).  In mid-August 2014, Amitabh Bachchan stated that he had completed his portions for the film,  whilst also confirming that 90% percent of the films shooting had been completed.  Dhanush reportedly did a few dastardly stunts in the film.  The films cinematographer P. C. Sreeram was fascinated by Aksharas acting capability and lack of nervousness while shooting with Dhanush and Amitabh Bachchan and lauded her as a "woman of substance".  Akshara did her own choreography for her dance. 
 snow storm hit the area where shooting was going on for a dance sequence in the film and had to immediately walk back towards a safer environment. Hence, they walked for about 10 kilometers with their heavy equipment. Choreographers Bosco-Caesar decided to continue with it and shot the song the next day since a delay would have caused permission trouble. 

==Music==
 
{{Infobox album 
| Name = Shamitabh: Original Motion Picture Soundtrack
| Longtype = 
| Type = Soundtrack
| Artist = Ilaiyaraaja
| Cover = 
| Border =
| Alt =
| Caption = Digital soundtrack cover art
| Released = 16 January 2015 
| Recorded = 
| Genre = 
| Length = 25:40 Hindi
| Label = Eros Music
}}

The soundtrack album and background score were composed by Ilaiyaraaja.  Illaiyaraaja began work for the films songs on 6 January 2014 in Mumbai.  Amitabh Bachchan had lent his voice to a song in the film.  The album received positive response from  critics and topped the i-Tunes charts. 

{{tracklist
| headline        = Shamitabh (Original Motion Picture Soundtrack) 
| extra_column    = Singer(s)
| total_length    = 
| all_lyrics      = 
| lyrics_credits  = yes
| title1          = Ishq E Phillum
| lyrics1         = Swanand Kirkire
| extra1          = Suraj Jagan
| length1         = 04:30
| title2          = Sha Sha Sha Mi Mi Mi
| lyrics2         = Swanand Kirkire
| extra2          = Caralisa Monteiro
| length2         = 05:20
| title3          = Piddly Si Baatein
| lyrics3         = Swanand Kirkire
| extra3          = Amitabh Bachchan
| length3         = 05:09
| title4          = Stereophonic Sannata
| lyrics4         = Swanand Kirkire 
| extra4          = Shruti Haasan
| length4         = 04:59
| title5          = Thappad
| lyrics5         = Swanand Kirkire
| extra5          = Suraj Jagan & Earl DSouza
| length5         = 04:07
| title6          = Lifebuoy
| lyrics6         = Swanand Kirkire
| extra6          = Suraj Jagan
| length6         = 01:54
}}

==Release==
The film released worldwide on 6 February 2015.    
Shamitabh was made with a Budget of    from which  25 Crores is Production Cost  while EROS International acquired Worldwide Distribution Rights for  .

===Marketing===
The first look of the lead actors Amitabh Bachchan and Dhanush were released on 23 July 2014,  and 25 July 2014,  respectively. A video trailer of Amitabh sitting in a toilet and singing "piddly si baatein", a catchy, peppy song while holding the script, was released on 31 December 2014. An extended version of the trailer was released on 7 January 2015.  Amitabh Bachchan had been reportedly seen sporting a rugged look and wearing shabby clothes,  Bachchan found the look quite difficult to preserve throughout the shooting of the film.  Bachchan described every film he made with Balki as "novel, out of the ordinary and most challenging" and Shamitabh is "no different".  A still featuring the lead actors was released by Amitabh Bachchan on Twitter on 20 August 2014.  Eros International distributed the film worldwide. 

==Critical Reception==
Srijana Mitra Das of Times of India gave it 3.5 stars out of 5 and stated, "Shamitabh has a doubly dramatic act - but less emotion".  Indian Express gave the movie 2 stars.  Rajeev Masand of IBNLIVe gave the movie2.5 stars stating "The promising film comes undone by the curse of the second half".  The Guardian gave average reviews describing the movie "Amitabh Bachchan in affectionate but incoherent satire". 

==References==
 

==External links==
*  

 

 
 
 
 
 
 