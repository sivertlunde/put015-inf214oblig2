Black Box (2002 film)
{{Infobox film
| name           = Black Box
| image          = Cajanegraposter.jpg
| caption        = Theatrical release poster
| director       = Luis Ortega
| producer       = Miriam Bendjuia Mariano Fernández
| writer         = Luis Ortega
| narrator       =
| starring       = Dolores Fonzi Eugenia Bassi
| music          = Leandro Chiappe
| cinematography = Luis Ortega
| editing        = César Custodio
| distributor    = Bodega Films
| released       =  
| runtime        = 81 minutes
| country        = Argentina
| language       = Spanish
| budget         =
}} Argentine  film, written and directed by Luis Ortega.  The film stars Dolores Fonzi, Eugenia Bassi, and others.

==Plot==
The film tells of the relationships in a dysfunctional family.

Dorotea (Dolores Fonzi) is a young girl in her teens who works in a laundry and takes care of her grandmother (Eugenia Bassi) she lives with.

Her father, Eduardo (Eduardo Couget), is released from prison.

Eduardo is indigent and stricken with Parkinsons and lives in the Salvation Army shelter and panhandles from passing motorists as well.

Dorotea becomes Eduardos caregiver.

==Cast==
* Dolores Fonzi as Dorotea
* Eugenia Bassi as Abuela
* Eduardo Couget as Padre
* Silvio Bassi as Silvio

==Distribution==
The film was first presented at the Buenos Aires International Festival of Independent Cinema on April 27, 2002 in Buenos Aires.  It opened wide in Argentina on August 15, 2002.

The film was screened at many film festivals, including: the Donostia-San Sebastián International Film Festival, Spain; the International Filmfest Mannheim-Heidelberg, Germany; the Philadelphia International Film Festival, USA; the Karlovy Vary Film Festival, Czech Republic; and others.

==Awards==

===Wins===
* International Filmfestival Mannheim-Heidelberg|Mannheim-Heidelberg International Filmfestival: Special Mention, Luis Ortega; 2002.
* Mar del Plata Film Festival: SIGNIS Award - Special Mention, Luis Ortega; Special Jury Award, Luis Ortega; 2002.
* Fribourg International Film Festival, Switzerland: Don Quixote Award; Luis Ortega; E-Changer Award, Luis Ortega; Special Jury Award, Luis Ortega; 2003.

===Nominations===
* Viña del Mar Film Festival, Chile: Grand Paoa, Luis Ortega; 2002.
* Argentine Film Critics Association Awards: Silver Condor; Best Actress, Dolores Fonzi; Best First Film, Luis Ortega; Best New Actor, Eduardo Couget; 2003.

==References==
 

==External links==
*  
*   at the cinenacional.com  
*   film review at Fotograma.com by Pablo Silva  
*    

 
 
 
 
 
 
 