Have a Good Funeral, My Friend... Sartana Will Pay
{{Infobox film
| name           = Have a Good Funeral, My Friend... Sartana Will Pay
| image          = Have-a-good-funeral-my-friend-sartana-will-pay-movie-poster-1970.jpg
| caption        = 
| director       = Giuliano Carnimeo
| producer       = Aldo Addobbati Paolo Moffa
| writer         = Roberto Gianviti Giovanni Simonelli
| starring       = Gianni Garko Daniela Giordano Ivano Staccioli
| music          = Bruno Nicolai
| cinematography = Giovanni Bergamini
| editing        = Ornella Micheli
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Have a Good Funeral, My Friend... Sartana Will Pay ( ) is a 1970 Italian Spaghetti Western film directed by Giuliano Carnimeo, written by Roberto Gianviti and starring Gianni Garko.            

== Plot ==
After witnessing a massacre, Sartana is ready to do investigation. Almost everyone in the town of Indian Creek is under suspicion. 

== Cast ==
* Gianni Garko as Sartana
* Daniela Giordano as Abigail Benson
* Ivano Staccioli as Blackie
* Helga Liné as Saloon Girl Mary
* Luis Induni as The Sheriff
* Franco Ressel as Samuel Piggott George Wang as Lee Tse Tung 
* Franco Pesce as The Gravedigger  Rick Boyd as  Jim Piggott   
* Attilio Dottesio as Joe Benson

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 