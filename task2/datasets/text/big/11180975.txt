Abbot of Shaolin
 
 
{{Infobox film
| name           = Abbot of Shaolin
| image_size     = 
| image	=	Abbot of Shaolin FilmPoster.jpeg
| caption        = A poster bearing the films alternative title: Slice Of Death
| director       = Ho Meng Hua
| producer       = Sir Run Run Shaw
| writer         = 
| narrator       = 
| starring       = David Chiang   Lo Lieh   Lily Li
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1979
| runtime        = 
| country        = Hong Kong
| language       = Mandarin
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Abbot of Shaolin (少林英雄榜)  aka Shaolin Abbot aka Slice of Death is a Shaw Brothers film directed by Ho Meng Hua. It is one of the Shaolin Temple themed martial arts films and their rebellion against the Qing’s, featuring David Chiang and Lo Lieh in his famous role as Priest Pai Mei. 

== Plot ==
The Monk Chi San (David Chiang) is sent by his Shaolin masters to learn a special kung fu from a Wu-Tang priest and befriends his niece Wu Mei (Lily Li). The priests brother is Pie Mei (Lo Lieh), who disapproves of associating with Shaolin rebels who oppose the Qing leave’s as his nephew (Ku Kuan Chun) remains behind to cause trouble for Chi San. While Chi San is in training, Pai Mei is promoted by the court and arranges for the Qing soldiers and a northern Lama to attack and destroy Shaolin Temple. Chi San arrives too late as the attack has already run its course, his master orders him to go to the south to search for loyal Shaolin men and rebuild the temple.

Pai Mei returns to his brothers temple and arranges for his nephew to poison him, he then attacks the priest himself and kills him for teaching Chi San kung fu. Pai Mei orders the search for Chi San so that he may kill him. Chi San arrives in the south and assists a local businessman who is being robbed. The grateful man takes Chi San into his home. At this point Chi San gains his first student and goes through various integrity "tests" from local businessmen to prove if he is a true Shaolin Monk. Chi San doesn’t disappoint and the impressed businessmen offer to help Chi San rebuild the temple. Chi San also gains more students after their failed attempts to beat him at kung fu.

Pai Mei and his nephew then attempt to kill Wu Mei but she is rescued by Chi San and his students. Chi San orders his students to head south to wait on a ferry while he goes after Pai Mei and the Lama. Chi San meets the lama in the woods and defeats him but Pai Mei intercepts Wu Mei and the students. Chi San arrives before Pai Mei can kill them and defeats Pai Mei

== References ==
 

== External links ==
*  

 
 
 
 


 