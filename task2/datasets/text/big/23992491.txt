The Arrival of Averill
{{Infobox Film
| name           = The Arrival of Averill
| image          = 
| image_size     = 
| caption        = 
| director       = Michael Schottenberg
| producer       = Peter Pochlatko
| writer         = Michael Schottenberg Michael Juncker
| narrator       = 
| starring       = Claude Aufaure
| music          = 
| cinematography = Michael Riebl
| editing        = Ortrun Bauer
| distributor    = 
| released       = 1992
| runtime        = 95 minutes
| country        = Austria
| language       = German
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Arrival of Averill ( ) is a 1992 Austrian drama film directed by Michael Schottenberg. It was screened in the Un Certain Regard section at the 1992 Cannes Film Festival.   

==Cast==
* Claude Aufaure
* Maria Bill
* Fabio Carfora
* Umberto Conte
* Andras Jones - Averill
* Elisabeth Kaza
* Michael Kroecher

==References==
 

==External links==
* 

 
 
 
 
 
 
 