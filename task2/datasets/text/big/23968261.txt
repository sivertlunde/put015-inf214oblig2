Fair Wind to Java
{{Infobox film
| name           = Fair Wind to Java
| image          = Fair Wind to Java - 1953- Poster.png
| image_size     = 
| caption        = 1953 film poster
| director       = Joseph Kane
| producer       =  
| writer         = Richard Tregaskis (screenplay) Garland Roark (novel)
| narrator       = 
| starring       = Fred MacMurray Vera Ralston
| music          = Victor Young
| cinematography = Jack Marta
| editing        =  
| distributor    = Republic Pictures
| released       = April 28, 1953
| runtime        = 92 minutes
| country        = United States English
| budget         = $2 million Charles Tranberg, Fred MacMurray: A Biography, Bear Manor Media, 2014 
| gross          = $1.3 million (US) 
}} American adventure Robert Douglas 1883 eruption on the island of Krakatoa. 

==Cast==
* Fred MacMurray  ...  Captain Boll
* Vera Ralston  ...  Kim Kim Robert Douglas  ...  Saint Ebenezer / Pulo Besar
* Victor McLaglen  ...  OBrien John Russell  ...  Flint
* Buddy Baer  ...  King
* Claude Jarman Jr.  ...  Chess
* Grant Withers  ...  Jason Blue
* Howard Petrie  ...  Reeder
* Paul Fix  ...  Wilson William Murphy  ...  Ahab
* Sujata Rubener  ...  Dancer
* Philip Ahn  ...  Gusti
* Stephen Bekassy  ...  Lieutenant
* Keye Luke  ...  Pidada

==Plot==
 pirates and Indonesian whose divers Marine salvaged a Chinese Junk junk captain who has cargo that will lead Boll to the diamonds.   
 first mate, discovers Kim Kim aboard the ship and finds out that Boll had purchased her; he blackmails Boll, threatening to turn Boll in to the Dutch authorities if Boll does not give him half the diamonds when they are found. 
 Dutch citizen "Saint" Ebenezer, the pirate Pulo Besar also becomes aware that Kim Kim is aboard the Gerrymander. He tips off the authorities. They search the Gerrymander but do not find Kim Kim, who is hiding in a half-filled vat of water. Later, however, the Gerrymander s crew discovers her. Boll insists that they treat her well, but first he has to fight one of his sailors, Reeder, in order to protect her. 

Boll constantly questions Kim Kim about the diamonds. At first, this angers her, but when he confides in her, telling her that he is impoverished but dreams of one day owning his own ship, she has a change of heart and decides to help him. She tells him that the diamonds are on what she calls the island of the fire god, Vishnu, which she visited as a child. Meanwhile, Flint incites the Gerrymander s crew against Boll by claiming that Kim Kim s presence aboard the ship has made Boll mentally unbalanced, and he leads a mutiny against Boll. When the mutinous crew confronts Boll, Boll offers the crew Flints half of the fortune if they leave him in command of the Gerrymander. They agree, and the mutiny comes to an end. Flint is imprisoned. The Gerrymander sails on in search of Vishnus island. 
 whip Kim Kim and show her that her mother, Bintang, is a prisoner of Besar s and has been broken by torture and imprisonment. Loyal to Boll, Kim Kim refuses to tell them anything about the diamonds. Giving up on torturing Kim Kim, Besar instead threatens to kill Boll unless she helps him find the diamonds. She agrees in order to save Bolls life. Flint and two other sailors from the Gerrymander also offer to cooperate with Besar in finding the diamonds. 

Besar, his pirates, Kim Kim, Flint, and the two other sailors from the Gerrymander set sail for Vishnus island in Besars pirate ship. Meanwhile, the crewmen of the Gerrymander escape and set Boll free. They take back control of the Gerrymander from the pirates and set out in the Gerrymander in pursuit of Besar. To keep Besar from losing them during a moonless night, Boll and two of his men ride ahead of the  Gerrymander  in a longboat and send signals back to the Gerrymander to allow her to remain on Besar s tail. While they are doing this, one of the Gerrymander s sailors aboard Besar s ship, Wilson, sneaks off the ship and swims to Boll after overhearing where Besar is heading. Wilson s information allows Boll and his crew to identify Vishnu s island as Krakatoa. 
 the islands volcano erupting. Despite their fear of the volcano, both the Gerrymander s men and Besar s pirates go ashore and climb the mountain, with each party racing the other to be the first get to a temple at the mouth of the volcano where the diamonds supposedly have been hidden. During the climb, Boll spots Kim Kim on the shore below. As the eruption becomes more powerful and lava begins to flow down the mountainside, Boll and the crew of the Gerrymander decide that the situation has become too dangerous for them to continue their climb to the diamonds; instead, they rescue Kim Kim, return to the Gerrymander, and head out to sea. Besar and his men also soon give up on finding the diamonds and put to sea in their own ship to flee the volcano. Boll expects the eruption to generate a huge tsunami, so he orders his crew to set the sea anchor and turns the Gerrymander toward Krakatoa to ride out the wave, which she successfully does. Besar instead makes the mistake of trying to outrun the tsunami, which capsizes his ship. He and his crew drown. 
 guilder Bounty bounty on Besar, which they will earn by handing Besars island over to the Dutch authorities. In his capacity as captain of the ship, Boll then marries himself to Kim Kim on the deck of the Gerrymander as his crew looks on. 

==Production== Los Angeles, California. Vera Ralston claimed that shots of Java inserted in the film were made by John Ford,  though other sources claim location shots were done in Hilo, Hawaii|Hilo, Hawaii, by Bud Thackery. 
 Howard and Mono County, California, using models. A 26-foot (8-meter) model of the Gerrymander was built, as was a model of Besars Lateen|lateen-rigged pirate ship and a miniature volcano, constructed on one of the rocky outcrops along the lake s shoreline, to serve as Krakatoa. Miniature palm trees and huts were placed along the shore of the lake to simulate the Indonesian coast. For the final eruption, bags of cement were used to simulate Krakatoa s immense volcanic plume.  The remains of the movies model volcano at Lake Mono still existed as of 2014 and had been adapted for use as a field base for bird researchers.   

The tsunami scenes depicting the Gerrymander riding out the wave and the sinking of Besar s ship were created by filming the two model ships in the Pacific Ocean surf on a California beach. 

The plot of the 1969 disaster film Krakatoa, East of Java, which tells the story of an attempt to salvage a fortune in pearls from a submerged shipwreck perilously close to Krakatoa during its 1883 eruption, bears many similarities to that of Fair Wind to Java. 

==Restoration==
After the University of California, Los Angeles Film and Television Archive restored Fair Wind to Java with funding from The Film Foundation, Martin Scorsese presented a showing of the film in 2006, calling it "the epitome of a Saturday afternoon matinee picture." 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 