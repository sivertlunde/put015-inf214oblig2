Moving Violations
{{Infobox film
| name           = Moving Violations
| image          = moving_violations_movie_poster.jpg
| caption        = Moving Violations movie poster
| writer         = Neal Israel, Pat Proft (screenplay)  Paul Boorstin, Sharon Boorstin (story)
| starring = {{Plainlist| John Murray
* Jennifer Tilly
* James Keach
* Wendie Jo Sperber
* Sally Kellerman
}}
| director       = Neal Israel
| producer       = Joe Roth Harry J. Ufland
| cinematography = Robert Elswit
| editing        = Tom Walls
| studio         = SLM Production Group
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 minutes
| country        = United States English
| music          = Ralph Burns
| awards         =
| budget         =
| gross          = $10,600,000 (USA)
}}
 John Murray, Jennifer Tilly, Brian Backer, Sally Kellerman, Nedra Volz, Clara Peller, Wendie Jo Sperber and Fred Willard. It was directed by Neal Israel. It is notable for starring the lesser-known siblings of many famous actors, and being the film debut of Don Cheadle.

==Synopsis== Los Angeles, John Murray), very suspicious of their scheme and enlisting his fellow students to expose their plot to sell their impounded vehicles.

==Trailer==

The trailer contains several scenes and lines of dialogue that were not in the final film, including the traffic school classroom scene with the offending drivers, in which the classroom is much smaller than the one featured in the actual film.

==Production==
Writer and director Israel himself attended traffic school.   

==Reception==
The film was reviewed poorly by Janet Maslin at The New York Times, who described it as an "especially weak teen-age comedy even by todays none-too-high standards."  In a later appraisal, David Nusair of Reelfilm.com wrote that Moving Violations contains "enough laughs to be had here to warrant a mild recommendation." 

==Cast== John Murray as Dana Cannon
*Jennifer Tilly as Amy Hopkins 
*James Keach as Deputy Henry Hank Halik
*Brian Backer as Scott Greeber
*Sally Kellerman as Judge Nedra Henderson
*Ned Eisenberg as Wink Barnes
*Clara Peller as Emma Jean
*Wendie Jo Sperber as Joan Pudillo
*Nedra Volz as Mrs. Loretta Houk
*Fred Willard as Terrence Doc Williams
*Lisa Hart Carroll as Deputy Virginia Morris
*Nadine Van der Velde as Stephanie McCarty
*Ben Mittleman as Spencer Popodophalos
*Don Cheadle as Juicy Burgers Worker
*William Forward as Police Officer #1
*Robert Conrad as Chief Robert Fromm (uncredited)
*Willard E. Pugh as Jeff Roth
*Dedee Pfeiffer as Cissy Michael McManus as Farmer #1 (as Mike McManus)

==Notes==
 

==See also==
* List of American films of 1985

==External links==
* 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 