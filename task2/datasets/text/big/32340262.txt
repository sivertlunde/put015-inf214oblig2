Morte di un matematico napoletano
{{Infobox film| name = Death of a Neapolitan Mathematician
  | image =
  | caption =
  | director = Mario Martone
  | producer = Angelo Curti Antonietta de Lillo Giorgio Magliulo
  | writer = Mario Martone Fabrizia Ramondino
  | starring = Carlo Cecchi Anna Bonaiuto Toni Servillo
  | music = Michele Campanella
  | cinematography = Luca Bigazzi
  | editing = Jacopo Quadri
  | distributor = Radiotelevisione Italiana
  | released = 8 September 1992 - Italy   14 September 1992 - Canada   February 1992 - Portugal   14 September 1994 - France   20 April 2002 - Argentina
  | runtime = 108 minutes
  | country = Italy
  | language = Italian
  | budget =
}}
 Italian drama film, written and directed by Mario Martone.

The film earned nine awards and was nominated for two, with director and writer Mario Martone winning seven awards.

==Plot==
 

==Cast==
* Carlo Cecchi ... Renato Caccioppoli
* Anna Bonaiuto ... Anna
* Renato Carpentieri ... Luigi Caccioppoli
* Toni Servillo ... Pietro
* Licia Maglietta ... Emilia
* Antonio Neiwiller ... Don Simplicio

==Awards==

===Won===
*49th Venice International Film Festival 1992:
**Grand Special Jury Prize - Mario Martone
**Kodak Cinecritica Award - Mario Martone
**Pasinetti Award - Best Actor: Carlo Cecchi
*Angers European First Film Festival 1993:
**C.I.C.A.E. Award - Mario Martone
**European Jury Award - Feature Film: Mario Martone
**SACD Grand Prize - Mario Martone
*David di Donatello Awards 1993:
**David Award - Best New Director: Mario Martone
**Special David Award - Carlo Cecchi
*Italian National Syndicate of Film Journalists 1993:
**Silver Ribbon Award - Best New Director: Mario Martone

===Nominated===
*European Film Awards 1993:
**European Film Award - Best Actor: Carlo Cecchi
*Italian National Syndicate of Film Journalists 1993:
**Silver Ribbon - Best Actor: Carlo Cecchi

==External links==
 
 

 
 
 
 


 