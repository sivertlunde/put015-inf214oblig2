Devil May Call
{{Infobox film
| name           = Devil May Call
| image          = Official_Poster_for_Devil_May_Call.jpg
| caption        = Devil May Call Official Poster
| director       = Jason Cuadrado
| producer       = Camillia Sanes
| writer         = Jason Cuadrado Wyatt Doyle
| story          = Jason Cuadrado
| starring       = Corri English Tyler Mane Traci Lords Van Hansis Tracy Perez Camillia Monet Daniel Hugh Kelly
| music          = Nicholas Pike
| cinematography = A.J. Raitano
| editing        = Vicente Perez Marta Bonet
| runtime        = 90 minutes
| country        = United States
| language       = English
}}
Devil May Call is a 2013 horror film directed by American filmmaker Jason Cuadrado and written by Jason Cuadrado and Wyatt Doyle. The film stars Corri English as Samantha, a crisis hotline counselor.

== Plot ==
Blind telephone counselor Sam (Corri English) is leaving. Volunteering to train new employee Jess (Van Hansis), Sam joins skeleton crew Val (Traci Lords) and Jules (Tracy Perez) for one last night on the phones. They’re expecting a quiet night. But John (Tyler Mane), a serial killer and one of Sam’s regular callers, is upset at Sams leaving and feels betrayed. Seeking revenge, John knocks out the power to the building and begins tearing the office to pieces. Terror ensues.

== Cast ==
* Corri English – Sam Carvin
* Tyler Mane – John Reed Smith
* Traci Lords – Valerie Kramer
* Van Hansis – Jess Gibson
* Traci Perez – Jules Ibanez
* Camillia Monet – Emily Gretsch
* Daniel Hugh Kelly – Tony Taylor
* Carina Aviles – Ana Charvel

== Production ==
Devil May Call was financed by Urs Brunner, the CEO of Boncafe and Angel and Bear Productions. It is his first American production.  

Corri English studied with a blind coach to learn the mannerisms of the sight impaired. The production also consulted with crisis hotlines to authenticate the calling process for volunteers and how the centers are managed. 

The film was shot in Los Angeles over the course of 12 nights. The office location where most of the action takes place was in a practically abandoned building which helped sell the isolation and general creepiness of the story. A nighttime schedule also allowed the production to work without disturbing the few residents still left in the entire building. 

The entire project went from a story by Jason Cuadrado to a completed rough cut in six months.  The film was completed and had its domestic premiere at Paramount Studios three months after.

== Distribution ==
North American film rights were acquired by Grindstone Entertainment for Lionsgate Home Entertainment.

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 