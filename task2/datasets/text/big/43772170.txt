Chance at Heaven
{{Infobox film
| name           = Chance at Heaven
| image          = Chance at Heaven poster.jpg
| alt            = 
| caption        = Theatrical release poster James Anderson (assistant)
| producer       = Merian C. Cooper
| screenplay     = Julien Josephson Sarah Y. Mason
| story          = Viña Delmar
| starring       = Ginger Rogers Joel McCrea Marian Nixon Andy Devine Lucien Littlefield
| music          = Max Steiner Roy Webb
| cinematography = Nicholas Musuraca
| editing        = James B. Morley 
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Chance at Heaven is a 1933 American drama film directed by William A. Seiter and written by Julien Josephson and Sarah Y. Mason. The film stars Ginger Rogers, Joel McCrea, Marian Nixon, Andy Devine and Lucien Littlefield. The film was released on October 27, 1933, by RKO Pictures.   

==Plot==
 

== Cast ==
*Ginger Rogers as Marjorie Marje Harris
*Joel McCrea as Blackstone Blacky Gorman
*Marian Nixon as Glory Franklyn 
*Andy Devine as Al
*Lucien Littlefield as Mr. Fred Harris
*Virginia Hammond as Mrs. S.T. Franklyn
*George Meeker as Sid Larrick
*Ann Shoemaker as Mrs. Harris

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 