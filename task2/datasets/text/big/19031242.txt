Phantom Punch (film)
{{Infobox film
| name           = Phantom Punch
| image          = PhantomPunch(film).jpg
| image_size     = 
| caption        =  Robert Townsend
| producer       = Hassain Zaidi Marek Posival Ving Rhames 
| executive producer =  
| writer         = Ryan Combs
| narrator       = 
| starring       = Ving Rhames Stacey Dash Nicholas Turturro Alan van Sprang David Proval Bridgette Wilson
| music          = Stephen James Taylor
| cinematography = John Dyer Michael Doherty
| studio         = Access Motion Pictures
| distributor    = Screen Media Films
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Robert Townsend. The film is a biopic of Sonny Liston, with Ving Rhames in the lead role.  The film also stars Stacey Dash, Nicholas Turturro, Alan van Sprang, David Proval, and Bridgette Wilson. 

Music was composed by Stephen James Taylor.

==Cast==
* Ving Rhames as Sonny Liston
* Nicholas Turturro as Caesar Novak
* Stacey Dash as Geraldine Liston
* Alan van Sprang as Nico Orso
* David Proval as Savino
* Bridgette Wilson as Farrah Rick Roberts as Father Alios

==References==
 

==External links==
*  
*  

 

 
 
 


 