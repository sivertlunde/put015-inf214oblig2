Hide and Go Shriek
{{Infobox film
| name           = Hide and Go Shriek
| image_size     = 
| image	=	Hide and Go Shriek FilmPoster.jpeg
| caption        = 
| director       = Skip Schoolnik
| producer       = Dimitri Villard
| writer         = Michael Kelly
| starring       = Bunky Jones Brittain Frye George Thomas Donna Baltron
| music          = John Ross
| cinematography = Eugene Shlugleit
| editing        = Tim Alexander Mark Manos Amy Tompkins Adam Wolfe
| distributor    = New Star Entertainment
| released       =  
| runtime        = 90 minutes English
| country        = United States
| budget         = Unknown
| box office     = Unknown
}} horror Thriller suspense film. Halloween II.  Screenplay by Michael Kelly.  The film was given a limited theatrical release with an R rating; an alternate video version, containing only a little more violence, was released unrated.

==Plot==
The film begins with a unidentified man dressed in a suit and tie applying womens makeup in an old and dingy city apartment. The man picks up a prostitute from the street, and while having sex with her in an alleyway, stabs her to death.

Four couples, Judy (Donna Baltron) and David (George Thomas), John (Sean Kanan) and Bonnie (Bunky Jones), Randy (Brittain Frye) and Kim (Annette Sinclair), Shawn (Scott Fults) and Melissa (Ria Pavia) have just graduated from high school and are preparing to sneak into a furniture store owned by Johns father Phil (Ron Colby). Arriving before the store closes, the teenagers hide as Phil closes up. However, the group are unaware that an employee, Fred (Jeff Levine), is an ex-convict and living in the basement of the store. The teenagers begin to drink beer and party as John gives them a tour of them a tour of the large store. He advises them not to move anything and also to keep the lights switched off, so as not to get caught. After a while, Kim suggests playing hide-and-go-seek. The group agree, but force Kim to be it. John and Bonnie use the opportunity to have sex, not realising an unseen figure is watching them. Kim eventually finds the pair and declares them to be it. During the second round, Shawn and Melissa decide not to play but instead make out. Melissa goes into a bathroom to change into a negligee dress which was given to her by Kim. However Melissa is attacked by the killer who forces her head into the sink, drowning her. After some time Shawn becomes impatient of waiting and goes to find Melissa. A figure dressed in Melissas clothing runs by him in the dark. Shawn follows the figure, only to discover its the killer, who picks Shawn up and impales him on a piece of furniture.

While the others finish the game, they sit down to have dinner. After they have eaten, the group become worried about Melissa and Shawn disappearing. The group search for them, but John becomes angered when he finds multiple mannequins and furniture disarranged, concluding that the pair are playing a joke on them. Everyone then decides to go to bed. As John and Bonnie are having sex, a man dressed in Shawns clothes enters the room. After mooning them, an angered John runs after him. Upon entering the second floor of the building, John sees a man in a blonde wig applying makeup. Not recognizing the man, John confronts him only to spark a fight between the two, eventually leading to John being impaled through the stomach by a mannequins arm, killing him. Bonnie becomes anxious waiting and hides under the bed as she sees a figure entering and then leaving the room.

Meanwhile, Judy decides to lose her virginity to David. As Randy and Kim sleep, Kim awakens and decides to go to the bathroom. However she is quickly attacked by the killer. She attempts to escape in the store elevator, but the killer catches up with her and jumps into the elevator with her. Kims screams wake up Randy, who attempts to find her, only to bump into a distraught Bonnie who tells her John has gone missing. Randy and Bonnie wake up Judy and David, and they take the elevator down to the ground floor, not realising Kim has been bound and gagged on the top of the elevator. They discover a figure dressed in Kims clothes, but realize its not Kim. The group decide to phone the police, but find the line has been cut and all the exits have been chained shut. They begin to panic and attempt to get the attention of a passing cop car, but fail. Judy attempts to turn the lights on to attract attention, but the power suddenly is cut, sending the emergency lights on. The teenagers arm themselves with weapons, deciding to fight back. Bonnie notices a door and believes it to be a way out, only to discover the dead bodies of their friends. Fred appears, but the survivors believe him to be the killer, so knock him unconscious and tie him up.

Believing they have caught the killer, the group go into the elevator, only to hear Kim being beaten on the roof. Kim manages to get her arms and legs free before leaning down from the roof to call for help. The killer holds her head outside of the elevator as Judy tries to stop the elevator. However the button fails to work and Kim is eventually decapitated. Judy, David, Randy and Bonnie retreat to a bedroom. Outside, a man notices the teenagers in the store and calls the cops to report a break in. The killer quickly attacks the group and Randy is slashed across the chest. As the killer attempts to attack again, Fred appears and tackles the killer, who is revealed to be Zack (Scott Kubay), Freds lover from prison. Zack informs Fred that he killed the teenagers as he thought they were coming between him and Fred. Fred is repulsed and rejects Zack, angering him. Zack attacks Fred and stabs him in the neck with a knife. The teenagers fight with Zack, and Judy attempts to stab him with a knife. Zack leaps back and stumbles over Kims severed head before falling down the empty elevator shaft. After defeating Zack the police and Phil arrive.
 EMT has been murdered, with Zack driving the ambulance, having survived the fall. Zack looks directly at the camera and smiles sadistically.

==Cast==
* Bunky Jones as Bonnie Williams
* Brittain Frye as Randy Flint
* Annette Sinclair as Kim Downs
* George Thomas as David Hanson
* Donna Baltron as Judy Ramerize
* Scott Fults as Shawn Phillips
* Ria Pavia as Melissa Morgan
* Sean Kanan as John Robbins
* Scott Kubay as Zack
* Jeff Levine as Fred
* Michael Kelly as the alley Wino
* Ron Colby as Phil Robbins
* Donald Mark Spencer as Vince
* James Serrano and Lyons as the cops in the police car
* Robin Turk as the prostitute in the opening scene
* Joe White as the man behind the newsstand

==Production==
Hide and Go Shriek was filmed in an abandoned warehouse in downtown L.A. in the early summer of 1987.  Filming took about three weeks, and post production took about the same.  The film was ready for a brief theatrical release in the fall of that same year.  Annette Sinclair, who plays Kim Downs in the film, was asked to do the makeup on the plaster cast of her head used for the decapitation scene so that it would best match her own makeup.  The writer of the movie, Michael Kelly, makes a brief cameo in the film as the wino outside the store towards the end of the movie.  Screaming Mad George did all the special effects in the movie, some of which had to be cut for the theatrical release due to its graphic nature.

==References==
 

==External links==
*  

 
 
 
 