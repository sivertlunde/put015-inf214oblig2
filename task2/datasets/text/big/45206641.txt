Stockholm (film)
{{Infobox film
| name     = Stockholm
| image    =
| director = Rodrigo Sorogoyen
| producer = 
| writer   = Rodrigo Sorogoyen Isabel Peña Javier Pereira   Aura Garrido
| cinematography = Alejandro de Pablo
| music    = 
| production companies = Caballo Films Tourmalet Films Morituri
| country  = Spain
| language = Spanish
| runtime  = 90 min
| released =  
}}
Stockholm is a 2013 Spanish drama film directed by Rodrigo Sorogoyen. 

== Plot == Javier Pereira) tries to get a girl (Aura Garrido) he meets at a party to like him. She refuses, but he does not give up until he manages to change her mind. After they spend the night together, she discovers he is not like she thought.

== Cast == Javier Pereira - Él
*Aura Garrido - Ella
*Jess Caba - Amigo
*Susana Abaitua - Amiga 1
*Lorena Mateo - Amiga 2
*Miriam Marco - Amiga 3
*Helena Sanchis-Guarner - Chica
*Javier Santiago - Chico

==Awards and nominations==
{| class=wikitable
! Awards !! Category !! Nominated !! Result
|-
| rowspan=4 | I Premios Feroz
| colspan=2 | Best Drama
|  
|-
| Best Screenplay
| Isabel Peña and Rodrigo Sorogoyen
|  
|-
| Best Main Actress
| Aura Garrido
|  
|-
| colspan=2 | Best Film Poster
|  
|-
| rowspan=3 | 28th Goya Awards
| Best New Director
| Rodrigo Sorogoyen
|  
|-
| Best Actress
| Aura Garrido
|  
|-
| Best New Actor Javier Pereira
|  
|-
|}

==References==
 

== External links ==
* 

 
 