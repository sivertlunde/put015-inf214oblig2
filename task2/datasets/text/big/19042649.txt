The Little Drummer Girl (film)
{{Infobox film
| name           = The Little Drummer Girl
| image          =Poster of the movie The Little Drummer Girl.jpg
| caption        =
| director       = George Roy Hill Patrick Kelley Dieter Meyer
| writer         = John le Carré Loring Mandel
| narrator       =
| starring = {{plainlist|
* Diane Keaton
* Yorgo Voyagis
* Klaus Kinski
}}
| music          = Dave Grusin
| cinematography = Wolfgang Treu
| editing        = William Reynolds
| distributor    = Warner Bros.
| released       =  
| runtime        = 132 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $7,828,841
}}
The Little Drummer Girl is a 1984 American spy film directed by George Roy Hill and adapted from the 1983 novel The Little Drummer Girl by John le Carré. It starred Diane Keaton, Yorgo Voyagis, Klaus Kinski and Thorley Walters.    The film received divided reviews among critics.

==Plot==
Set in Europe and the Middle East, the plot follows the Mossads clandestine attempt to flush out a PLO bomber named Khalil. To capture him, they kidnap and later kill his brother, then recruit an anti-Zionist American actress named Charlie to impersonate the dead mans girlfriend, hoping Khalil will contact her. Charlie does everything the Mossad asks of her, but as she goes deeply undercover, she questions just how far she should go before reaching a point where she is unable to escape.

==Cast==
* Diane Keaton as Charlie
* Yorgo Voyagis as Joseph
* Klaus Kinski as Martin Kurtz
* Sami Frey as Khalil
* Michael Cristofer as Tayeh
* Eli Danker as Litvak
* Ben Levine as Dimitri
* Jonathan Sagall as Teddy
* Shlomit Hagoel as Rose
* Juliano Mer as Julio
* Sabi Dorr as Ben
* Doron Nesher as David
* Smadar Brener as Toby
* Shoshi Marciano as Rachel
* Philipp Moog as Aaron
* Bill Nighy as Al
* David Suchet as Mesterbein
* John le Carre as Commander

==Response==
The film opened to mixed reviews from critics.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 