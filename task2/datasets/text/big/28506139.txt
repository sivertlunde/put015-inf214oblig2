Déjà Vu (1997 film)
 
{{Infobox film
| name           = Déjà Vu 
| image          =DejaVuredgrave.jpg
| caption        = Promotional poster
| director       = Henry Jaglom
| producer       = John Goldstone Judith Wolinsky
| writer         = Victoria Foyt Henry Jaglom
| starring       = Stephen Dillane Victoria Foyt Vanessa Redgrave
| music          = Gaili Schoen
| cinematography = Hanania Baer
| editing        = Henry Jaglom 
| distributor    = Lions Gate Films
| released       =  
| runtime        = 117 min
| country        = United States English
| budget         = 
| gross          =  $1,086,181 
}} 1997 American American Film Institute Festival on 25 October 1997 and was released theatrically on 22 April 1998. 

==Plot==
Dana (Foyt), a young American woman, is told a story of lost love by a stranger while on vacation in Jerusalem. The stranger clearly pines for her former American lover but both subsequently married different partners. After a trip to Paris with her fiancée Dana has a chance encounter with a British man, Sean (Dillane), at the White Cliffs of Dover. Although both are in relationships, they develop passionate feelings for each other that threaten to destroy their established relationships. Dana begins to reflect that Sean is her true love and feels as if her situation will reflect that of the sad stranger in Jerusalem if she does not act.   Los Angeles Times, 21 April 1998 

==Cast==
*Stephen Dillane as Sean
*Victoria Foyt as Dana Howard
*Vanessa Redgrave as Skelly
*Glynis Barber as Claire
*Michael Brandon as Alex
*Vernon Dobtcheff as Konstantine
*Rachel Kempson as Skellys mother
*Noel Harrison as John Stoner
*Anna Massey as Fern Stoner
*Aviva Marks as Woman in Cafe

==Reception==
The film was generally well-received, it holds a 65% fresh rating on Rotten Tomatoes.  Empire (magazine)|Empire gave the film 4 out of 5 stars, remarking that it is; "An honest look at the concept of love, as previously sold to us by the movies. Its romantic, but its firmly grounded in reality. Overall, a true delight."  The Los Angeles Times warmly praised the film comparing it favourably with others, "it has the easy elegance and verve of an Astaire-Rogers musical" and remarked that it "represents a new level of accomplishment for Jaglom". The newspaper continued to praise the performances, cinematography and notably "the suspense it generates". 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 