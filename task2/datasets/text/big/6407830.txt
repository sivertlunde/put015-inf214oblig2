Bloodfist
{{Infobox film
| name           = Bloodfist
| image          = Bloodfist.jpg
| caption        = DVD cover for Bloodfist
| director       = Terence H. Winkless
| producer       = Roger Corman Sally Mattison Cirio H. Santiago Robert King Don Wilson Joe Mari Avellana Billy Blanks Rob Kaman Kris Aguilar Riley Bowman Michael Shaner
| music          = Sasha Matson
| cinematography = Ricardo Jacques Gale
| editing        = Karen Horn
| distributor    = Concorde Films 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| gross          = $1,770,082
}}
 Robert King, and starring Don "The Dragon" Wilson. Wilson plays a dojo sensei in California who travels to Manila to avenge his professional kickboxer brother, who was murdered after a fight.  It has become a cult film. 

==Plot==
A man gets beaten by his opponent, but after he finds out the fight was rigged, he decides to fight back. He kills his opponent and is announced as the winner. On his way back to his home, another man kills him.
 police department. He is told his half-brother Michael is dead, and he must pick up the body in Manila. Raye travels to Manila and collects the body, but he decides to stay there and find his brothers killer. Raye gets training help from a man named Kwong and stays with local kickboxer Baby Davies, upon whom local Filipino neighbor Angela has a crush, and his sister Nancy. Kwong tells Raye about a gladiator-like tournament known as the Red Fist Tournament where only one comes out alive, and his brothers killer will likely be there.

Kwong trains Raye for the tournament and enters him. He manages to win all the fights and proceeds to the final match, where he faces off with Chin Woo. Kwong tells him that Chin Woo is his brothers killer and also the fighter who put Baby Davis in a coma. Hal, who has come from California to watch Rayes final bout, informs Raye that Kwong is the killer after Kwong drugs Raye. Angela comes in with a gun, but dies at the hands of Chin Woo. Woo is defeated by Raye, who sets off after Michaels true killer. Kwong reveals that has a brother who died at the hands of Michael that night, and Kwong is the one who murdered him. Kwong fights Raye in the same alley where Michael died. Raye is badly wounded but impales Kwong on a fence. Nancy and Raye walk off into the night.

==Cast==
* Don The Dragon Wilson as Jake Raye
* Joe Mari Avellana as Kwong
* Rob Kaman as Raton
* Billy Blanks as Black Rose
* Cris Aguilar as Chin Woo (as kris Aguilar and Chris Aguilar)

==Releases==
Bloodfist received a limited release theatrically from Concorde Films, opening on September 22, 1989, and ended up grossing $1,770,082.  

The film was released on VHS by MGM/UA Home Entertainment and made over $11 million in video rental sales. 

 , and  .  The DVD is currently out-of-print.

==Reception== Black Belt wrote, " aybe it wasnt the best karate film in the world, but at least people could see what different martial artists look like doing their fight scenes." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 