Decalogue III
{{Infobox film
| name = Decalogue III
| image = Decalogue trzy.png
| caption =
| director = Krzysztof Kieślowski
| producer = Ryszard Chutkovski
| writer = Krzysztof Kieślowski Krzysztof Piesiewicz
| starring = Daniel Olbrychski Maria Pakulnis Joanna Szczepowska
| music = Zbigniew Preisner
| cinematography = Piotr Sobociński
| editing = Ewa Smal
| art director = Halina Dobrowolska
| sound = Nikodem Wolk-Laniewski
| distributor = Polish Television
| released = 1988
| runtime = 56 min.
| country = Poland Polish
| budget = $10,000
| gross =
}}
 The Decalogue by Polish director Krzysztof Kieślowski, possibly connected to the fourth and seventh imperative of the Ten Commandments: "Remember the Sabbath day, to keep it holy" and "Thou shalt not commit adultery".       

==Plot==
  mass in the city. There he spots Ewa (Maria Pakulnis), with whom he had an affair three years earlier. Ewa just happened upon the church after visiting her senile aunt in the retirement home (the aunt asks Ewa whether shes done her homework, but also enquires after her husband).

Ewa later comes to Januszs place looking for her ex-lover, asking him for help finding her husband, who she says has disappeared. Janusz leaves his house saying that his taxi got stolen, although his wife (Joanna Szczepowska) suspects something and suggests that he should leave it alone. Janusz answers that the taxi is their sole income and leaves. Janusz and Ewa spend the whole night driving around the city and discussing past and present. Janusz is eager to go home and be with his family on Christmas evening, but Ewa is desperate and manages to keep him with her by setting up clues along the way to track her husband down. They inquire in hospitals and at the train station, and eventually Janusz sees through her game but does not say anything.

When the clock strikes seven the next morning, Ewa reveals that she has been lying to Janusz. She is no longer with her husband, they divorced right after her tryst with Janusz and he has been living with his new family in Kraków for the past three years. She is now forced to face the holidays all alone while watching other families sharing the love and peace that she does not have. Ewa reveals that, in her mind, if she had succeeded in her game keeping Janusz away from his family till 7a.m., all would be well again, if not, she would commit suicide. They part at dawn, with Janusz returning to his family and Eva leaving with her loneliness. When Janusz gets home, his suspecting wife asks him whether hes been seeing Ewa. He promises never to see her again.  

==Cast==
* Daniel Olbrychski - Janusz
* Maria Pakulnis - Ewa
 
* Joanna Szczepowska - Januszs wife
* Artur Barciś - tram-driver
* Krystyna Drochocka - aunt
* Krzysztof Kumor - doctor
* Henryk Baranowski - Krzysztof
* Jacek Kalucki - policeman

* In other roles:
Zygmunt Fok, Jacek Kalucki, Barbara Kolodziejska,
Maria Krawczyk, Jerzy Zygmunt Nowak, Piotr Rzymszkiewicz,
Wlodzimierz Rzeczycki, Wlodzimierz Musial

==References==
 

 

 
 
 
 