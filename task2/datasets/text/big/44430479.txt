Dragon Ball Z: Resurrection 'F'
 
{{Infobox film
| name            = Dragon Ball Z: Resurrection F
| image           = DBZ THE MOVIE NO. 15.png
| caption         = Japanese release poster
| film name       =  
| director        = Tadayoshi Yamamuro
| producer        =
| screenplay      = Akira Toriyama
| based on        =  
| starring        = 
| music           = Norihito Sumitomo
| cinematography  = 
| editing         = 
| studio          = Toei Animation
| distributor     = Toei Company 20th Century Fox
| released        =   }}
| runtime         = 93 minutes 
| country         = Japan
| language        = Japanese
| budget          = US$5 million   
| gross           = US$21.7 million 
}} Japanese animated feature film based on the Dragon Ball series and the fifteenth to carry the Dragon Ball Z branding, released in theaters on April 18, 2015. It is the first ever Japanese film to be screened in IMAX 3D and also received screenings at 4DX theaters. 

Resurrection F is the second film personally supervised by series creator   in 2013. The movie depicts the return of the villain Frieza, as well as the God of Destruction Beerus and Whis from Battle of Gods. Funimation announced that their English language dub of the film will be released in North American theaters in summer 2015. Madman Entertainment will release the film in Australian theaters in winter 2015.

==Plot==
 
With their power dwindling due to the absence of their leader, the remnants of Friezas army are led by an alien named  , who decides to revive their master. He then sets off to Earth with one other minion,  , where they convince Pilaf, Mai, and Shu, who have collected the Dragon Balls to let them summon Shenlong instead. As Shenlong is unable to fully revive Frieza, he brings him back to life in pieces, and his minions put him together using their advanced technology. Once restored, Frieza kills Tagoma and learns that Goku got much stronger, to the point of defeating Majin Boo, and thus he decides he too needs to become more powerful. Frieza reveals that, since his power was inborn, he had never trained a day in his life, and so he trains himself for the first time, before returning to Earth with his army six months later. Jaco the Galactic Patrolman travels to earth to warn Bulma that Frieza is approaching, and she gathers the other warriors to fight him. With Goku and Vegeta training on Planet Beerus with Whis, unaware that Frieza has been revived, Gohan, Piccolo, Krillin, Master Roshi, Tenshinhan and Jaco fight off Friezas soldiers.

Bulmas message eventually reaches Whis, and Goku and Vegeta travel back to earth to fight Frieza. Whis and Beerus join them in order to eat a strawberry-flavored dessert Bulma has prepared for them. Goku fights Frieza first by transforming into the blue-haired  , and Frieza transforms into a new   form. Goku ultimately gains the upper hand, but he is shot in the chest by Sorbet. Frieza stands over the incapacitated Goku and offers Vegeta a chance to kill Goku for him in exchange for his own life. Vegeta refuses and transforms into the blue-haired Super Saiyan God Super Saiyan form as well, much to Friezas shock. Vegeta tells Krillin to revive Goku with a Sensu bean. As the latter moves to do so, Frieza attacks him, but Vegeta intercepts and deflects Friezas towards Sorbet, which kills him. Vegeta attacks Frieza, eventually causing him to revert back to his base form, who then realizes that he cannot win. Frieza decides to destroy the planet, killing Vegeta in the process.

Earth is destroyed, but Whis creates a bubble around a small piece of rock to protect himself, Beerus, Bulma, Krillin, Gohan, Goku, Master Roshi, Piccolo, Tenshinhan and Jaco the Galactic Patrolman. While Bulma despairs, Goku expresses his regret that he didnt kill Frieza when he had the chance. Whis tells him that he has the power to set back time but only three minutes earlier. He does so, and they arrive back right before Frieza destroys the Earth. Goku immediately attacks with a Kamehameha and kills Frieza.

==Voice cast==
{|border="1" style="border-collapse: collapse;" class="wikitable"
|-style="background-color: rgb(255,85,0);"
!Character
!Japanese voice actor
|- Son Goku Masako Nozawa
|- Vegeta
|Ryō Horikawa
|- Freeza
|Ryūsei Nakao
|- Gohan (Dragon Son Gohan Masako Nozawa
|- Kuririn
|Mayumi Tanaka
|- Piccolo (Dragon Piccolo
|Toshio Furukawa
|- Tien Shinhan|Tenshinhan Hikaru Midorikawa
|-
|Kame-Sennin Masaharu Satō
|- Bulma
|Hiromi Tsuru
|- Videl
|Yuko Minaguchi
|- Android 18 Miki Itō
|- Trunks (Dragon Trunks
|Takeshi Kusao
|- Shenlong (Dragon Shenlong
|Ryūzaburō Ōtomo
|- Jaco the Jaco
| Natsuki Hanae
|- List of Pilaf
|Shigeru Chiba
|- Mai
|Eiko Yamada
|- Shu
|Tesshō Genda
|- Beerus
|Kōichi Yamadera
|- Whis
|Masakazu Morita
|-
|Yogen-gyo Shoko Nakagawa
|- Tagoma
|Kazuya Nakai
|- Sorbet
|Shirō Saitō
|- Shisami
|Tetsu Inada
|- Angels
|Momoiro Clover Z
|}

== Development and production == Son Goku in Super Saiyan form using the Shunkan Idō technique confirmed Toriyamas credits for the original concept, screenplay and character designs and his mention that the film would be a continuation of his original manga. The author stated that the film will also be a sequel to Battle of Gods, that he strictly scrutinized all the dialogue, and promised more action scenes.    A flyer of the same image, with a backside showing Shenlong (Dragon Ball)|Shenlong, handed out at the Jump Victory Carnival event on July 19 revealed that Dragon Ball Z animation supervisor Tadayoshi Yamamuro is directing the new film.    Later that month, a short teaser trailer was released of Shenlong bringing someone back to life with text calling this  , followed by Goku transforming into a Super Saiyan.    

In November 2014, the films Japanese title of Dragon Ball Z: Fukkatsu no F and basic plot outline were unveiled in the January 2015 issue of V Jump. A promotional image released for the movie depicts Frieza, Goku, Vegeta, Piccolo, Son Gohan, Kuririn, as well as the God of Destruction Beerus and Whis from the previous film Battle of Gods. Also featured in the image were two new characters that are servants of Frieza, Sorbet and Tagoma. The magazine also disclosed that Toriyama came up with the title while listening to rock band Maximum the Hormones 2008 song "Tsume Tsume Tsume/F|F", which is about Frieza himself. 

On December 5, 2014, the first full trailer for the film was aired on Fuji TVs morning show Mezamashi TV.  The following day a slightly different trailer and a special audio message from Frieza (Ryūsei Nakao) were added to the films official website,    as well as some of the cast and crew. People who order advance tickets receive an earphone jack strap of either Goku or Frieza designed by Toriyama. 

Momoiro Clover Z appeared with Masako Nozawa at a press conference in Tokyo on February 3, 2015, where it was announced that all five members will have roles in the movie as "angels from hell." Costumed mascots of Son Goku and Frieza were also present at the event which was held on Setsubun, and accordingly beans were thrown at Frieza in the mamemaki tradition. 

On March 2, 2015, a second full trailer was released for Fukkatsu no F revealing Friezas new form, covering his skin in both gold and dark shades of purple. Other details included in the trailer were a defeated Gohan and Goku, Friezas henchmen fighting against Piccolo, Tenshinhan, Kame-Sennin and Kuririn. It also features Jaco from Toriyamas 2013  manga series Jaco the Galactic Patrolman, which is set before Dragon Ball.    A trailer featuring the series heroes fighting Frieza and his 1,000-man army was released on March 24 by Mainichi Shimbun. 

In April 2015, the 20th issue of Weekly Shōnen Jump revealed Gokus new form of the Super Saiyan God transformation he acquired in Battle of Gods.    Toei released a short advertisement depicting Goku and Freeza fighting in their new forms, days before the magazines official release. 

===Music===
Like the previous film, the music in Fukkatsu no F was composed by Norihito Sumitomo. Its theme song is   by the idol group Momoiro Clover Z,  with a Toei producer saying each member is a fan of the series. The song was released as a single on April 29, 2015 and includes a cover of Hironobu Kageyamas "Cha-La Head-Cha-La", the original opening theme of Dragon Ball Z.    An English-language version of the song has also been recorded for use in international versions of the film.  Maximum the Hormones 2008 song "Tsume Tsume Tsume/F|F" appears in the film during a fight scene.  The films original soundtrack, containing 32 tracks, will be released on May 8, 2015. 

==Promotions and other media==
  and Sean Schemmel, pose for the Los Angeles red carpet premiere of Resurrection F]]
In December 2014, Toho Cinemas collaborated with Fukkatsu no F for a television commercial promoting their Cinemileage Card. In it Frieza explains how the card program works and Gokus yelling of Friezas name appears as a pun on the word "free." 

A three-chapter manga adaptation of the film, drawn by Toyotarō, began in the April 2015 issue of V Jump. 

As part of a collaboration with the J. League Division 1 football team Yokohama F. Marinos, a special poster with players mimicking the films own was displayed in promotion of their matches with Vegalta Sendai on April 12 and Shonan Bellmare on April 25, 2015. 

In March 2015, a collaboration between Toei Animation and Kirin Company spawned two dance parody commercials tying Fukkatsu no F with Kirins Mets cola beverage, with the grape flavor advertisement featuring Frieza and his henchmen, and the orange flavor ad featuring Goku and the other heroes.  

A collaboration with Curry House CoCo Ichibanya will run from April 1 to May 31, 2015. Anyone with a receipt of over ¥1,000 from one of the restaurants can send it in to enter a merchandise lottery, where they can win exclusive items such as shot glasses and a platter featuring Goku eating curry. 

Friezas new form from the movie is a playable character in the video games Dragon Ball Zenkai Battle Royale, Dragon Ball Z Extreme Butōden and Dragon Ball Xenoverse.   Extreme Butōden also has Gokus new form from the movie as a playable character and Vegetas as an assist character.  

The first 1.5 million filmgoers received a book called  , which includes Toriyamas complete script for the movie as well as design materials. They also got either a Goku or Vegeta card for the Dragon Ball Heroes arcade game and a certificate that gives them access to exclusive content for five different Dragon Ball video games.   

==Release== 3D theaters across Japan on April 18, 2015.    It is the first Japanese film to be screened in IMAX 3D Digital theaters, shown in eighteen such establishments across the country. The previous movie Battle of Gods was the first ever Japanese film to be shown at IMAX Digital theaters, but was not in 3D.  It is also screened in ten 4DX theaters across Japan, which adds environmental effects such as seat motion, wind, rain and scents to the standard video and audio.  Prior to its nationwide release, preview screenings were held in seven different cities for 2,264 winners of a lottery held amongst 24 different Shueisha magazines. The earliest four taking place on March 30, two more the following day, and the final two on April 2. 

The film will be screened in 74 countries worldwide.  On March 15, 2015,   announced at Supanova Pop Culture Expo 2015 that they will release the film, both subtitled and dubbed into English, in Australian theaters in winter 2015. 

==Reception==
===Box office===
Dragon Ball Z: Fukkatsu no F opened at number one in the Japanese box office, earning approximately US$8.1 million with 716,000 tickets sold in its first two days. It made 40.3% more and sold 27.4% more tickets than Battle of Gods did in its opening weekend.    Deadline.com reported that it had the biggest opening in Japan so far for 2015, making Japan the only territory in the world where Furious 7 did not debut at number one.   The film earned around US$10.9 million by its sixth day and sold 1 million tickets by the end of its first week, making it the fastest film to reach that attendance number in Japan so far in 2015.   

== References ==
 

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 