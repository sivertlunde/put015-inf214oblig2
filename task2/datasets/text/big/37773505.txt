Mediterranean Holiday
{{Infobox film
| name           = Mediterranean Holiday
| image          = 
| caption        = 
| director       = Hermann Leitner Rudolf Nussgruber
| producer       = Rudolf Travnicek
| writer         = Karl Hartl
| narrator       = Hans Clarin
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 158 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Mediterranean Holiday ( ) is a 1962 West German documentary film directed by Hermann Leitner and Rudolf Nussgruber. It was entered into the 3rd Moscow International Film Festival.   

==Cast==
* Hans Clarin as Narrator (voice)
* Graham Hill as Himself
* Burl Ives as Narrator (US version)
* Grace Kelly as Herself (as Princess Grace of Monaco)
* Begum Aga Khan III as Herself (as Die Begum)
* King Constantine II as Himself (as Prince Constantine of Greece)
* Prince Rainier of Monaco as Himself

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 