Snatch (film)
 
 
{{Infobox film
| name           = Snatch
| image          = Snatch ver4.jpg
| border         = yes
| image_size     =
| caption        = Theatrical release poster
| director       = Guy Ritchie
| producer       = Matthew Vaughn
| writer         = Guy Ritchie
| starring       = {{Plain list |
*Benicio del Toro
*Dennis Farina
*Vinnie Jones
*Brad Pitt
*Rade Šerbedžija
*Jason Statham
}} John Murphy
| cinematography = Tim Maurice-Jones Jon Harris
| studio         = SKA Films
| distributor    = {{Plain list |
*Columbia Pictures
* 
*Screen Gems
* 
}}
| released       =  
| runtime        = 104 minutes
| country        = {{Plain list |
*United Kingdom
*United States 
}}
| language       = English
| budget         = $10 million   
| gross          = $83.6 million 
|}}
 Alan Ford) who is ready and willing to have his subordinates carry out severe and sadistic acts of violence.
 Irish Traveller Mickey ONeil (Brad Pitt), or in this case, referred to as "pikeys", arms-dealer Boris "the Blade" Yurinov (Rade Šerbedžija), professional thief and gambling addict Franky "Four-Fingers" (Benicio del Toro), American gangster-jeweller "Cousin Avi" (Dennis Farina), and bounty hunter Bullet-Tooth Tony (Vinnie Jones). It is also distinguished by a kinetic direction and editing style, a circular plot featuring numerous ironic twists of chance and causality, and a fast pace. 

The film shares themes, ideas and motifs with Ritchies first film, Lock, Stock and Two Smoking Barrels. It is also filmed in the same visual style and features many of the same actors, including Jones, Statham, and Ford.

==Plot synopsis==
After stealing an   diamond in a heist in Antwerp, Franky "Four-Fingers" goes to London to deliver it to diamond dealer Doug "The Head" on behalf of New York jeweller "Cousin Avi". One of the other robbers advises Franky to obtain a gun from ex-KGB agent Boris "The Blade". Unbeknownst to Franky, Boris and the robber are brothers and plan to steal the diamond from him before he can turn it over to Doug.
 caravan from throws the fight in the fourth round.

Boris gives Franky a .357 Smith & Wesson Model 681 revolver in exchange for a favour: Franky is to place a bet on Boris behalf at Brick Tops bookies. Avi, knowing Franky has a gambling problem, flies to London with his bodyguard "Rosebud" to claim the diamond personally. Boris hires Vinny and Sol, two small-time crooks, to rob Franky while he is at the bookies. The robbery goes awry and Sol, Vinny, and their driver Tyrone are caught on-camera, but manage to kidnap Franky.

Instead of throwing the fight, Mickey knocks his opponent out with a single punch. Infuriated, Brick Top robs Turkish of his savings and demands that Mickey fight again, and lose this time. Meanwhile, Boris retrieves the diamond and murders Franky with a .45 M1911A1 pistol. As Sol, Vinny, Tyrone, and their friend, Yardie "Bad Boy" Lincoln are trying to decide how to dispose of Frankys body, Brick Top arrives to kill them for robbing his bookies. Sol bargains for their lives by promising Brick Top the stolen diamond, and is given 48 hours to retrieve it.

Avi and Doug hire "Bullet-Tooth" Tony to help them find Franky. When the trail leads to Boris, they kidnap him and retrieve the diamond, closely pursued by Sol, Vinny, and Tyrone. Coincidentally Turkish and Tommy are driving on the same stretch of road at the time. When Tommy throws Turkishs carton of milk out of their car window; it splashes over Tonys windscreen, causing him to crash and killing Rosebud in the process. Boris escapes from the wreck only to be hit by Tyrones car. Tony and Avi are confronted by Sol, Vinny, and Tyrone at a pub where Tony realizes that their pistols, a 9mm Heckler & Koch P7 pistol & a 9mm Heckler & Koch P9S pistol, are replicas, which he contrasts with his very real .50 Desert Eagle pistol and intimidates them into leaving. The wounded Boris arrives with an AK-74 assault rifle with a grenade launcher looking for the diamond back but is killed by Tony with a .50 Desert Eagle pistol.  Sol and Vinny escape with the diamond, which Vinny hides in his pants. When Tony catches up to them, they tell him that the diamond is back at their pawn shop. Once there, they produce the diamond, but it is promptly swallowed by a dog that Vinny got from the pikeys. Avi fires at the fleeing dog and accidentally kills Tony with the Desert Eagle. He gives up and returns to New York.
 wake that Turkish fears he will not make it to the fourth round. If he fails to go down as agreed, Brick Tops men will execute Turkish, Tommy, Mickey, and the entire campsite of pikeys. Mickey makes it to the fourth round, when he suddenly knocks out his opponent. Outside the arena, Brick Top and his men are killed by the pikeys. Mickey has bet on himself to win, and waited until the fourth round to allow the pikeys time to ambush and kill Brick Tops men at the campsite.

The next morning, Turkish and Tommy find the pikey campsite deserted. When confronted by the police, they cannot explain why they are there, until Vinnys dog suddenly arrives and they claim to be walking it. Sol and Vinny are arrested when the police find Franky and Tonys bodies in their car. Turkish and Tommy take the dog to a veterinarian to extract a squeaky toy that it had swallowed, and discover the diamond in its stomach as well. They consult Doug about selling the diamond, and he calls Avi, who returns to London.

==Cast==
 
* Jason Statham as Turkish, promoter for unlicensed, no-gloves boxing Stephen Graham as Tommy, Turkishs business partner Alan Ford as Brick Top, sadistic crime boss
* Brad Pitt as Mickey ONeil, gypsy boxer and trader in vans Mike Reid as Doug The Head, jewel dealer running a diamond shop
* Dennis Farina as Abraham "Cousin Avi" Denovitz, American jewel dealer who arranges a diamond heist in Antwerp
* Vinnie Jones as Bullet Tooth Tony, debt collector and gun-for-hire
* Robbie Gee as Vinny, small-time pawn shop owner
* Lennie James as Sol, Vinnys partner Ade as Tyrone, Sol and Vinnys getaway driver
* Adam Fogerty as Gorgeous George, unlicensed boxer in Turkishs camp
* Rade Šerbedžija as Boris "The Blade" Yurinov aka Boris the Bullet Dodger, ex-KGB Uzbek  criminal
* Benicio del Toro as Franky "Four-Fingers", jewel robber and gambling addict
* Jason Flemyng as Darren, Mickeys gypsy friend
* Sam Douglas as Rosebud, Cousin Avis bodyguard
* Ewen Bremner as Mullet, spiv and informant to Tony
* Goldie as "Bad Boy" Lincoln, small-time gangster, Sol and Vinnys friend
* Dave Legeno as John, Brick Tops enforcer
* Andy Beckwith as Errol, Brick Tops enforcer
 

==Soundtrack==
{{Infobox album  
| Name        = Snatch: Stealin Stones and Breakin Bones
| Type        = soundtrack
| Artist      = various artists
| Cover       =
| Cover size  =
| Released    = 9 January 2001
| Recorded    =
| Genre       = Rock Pop Brit pop Reggae Jazz Rock
| Length      = Universal International TVT
| Producer    =
| Chronology  = Guy Ritchie film soundtracks
| Last album  = Lock, Stock and Two Smoking Barrels#Soundtrack|Lock, Stock and Two Smoking Barrels (1998)
| This album  = Snatch (2000) Swept Away (2002)
}}
{{Album ratings
| rev1 =Allmusic
| rev1Score =   
| noprose=yes
}}

Two versions of the soundtrack album were released, one on the Universal International label with 23 tracks and a TVT Records release with 20.

===Track listing=== Klint
#"Vere Iz da Storn?" – Benicio del Toro Overseer
#"Hernandos Hideaway" – The Johnston Brothers
#"Zee Germans" – Jason Statham
#"Golden Brown" – The Stranglers
#"Dreadlock Holiday" – 10cc John Murphy and Daniel L. Griffiths
#"Avi Arrives" – Dennis Farina Maceo & the Macks Mirwais
#"Nemesis" Alan Ford
#"Hot Pants (Im Coming, Coming, Im Coming)" – Bobby Byrd Lucky Star" Madonna
#"Come Alan Ford Ghost Town" – The Specials
#"Shrinking Balls" – Vinnie Jones
#"Sensual Woman" – The Herbaliser
#"Angel (Massive Attack song)|Angel" – Massive Attack
#"RRRR...Rumble" – Charles Cork Oasis
#"Avis Declaration" – Dennis Farina Huey "Piano" Smith & the Clowns

== Reception ==
Snatch was largely successful, both in critical response and financial gross, and has gone on to develop a devoted cult following. From a budget of $10 million,  the movie grossed a total of £12,137,698 in the United Kingdom and $30,093,107 in the United States. 
{{cite web
|url=http://www.imdb.com/title/tt0208092/business
|title=Snatch. (2000) – Box office / business
|publisher=IMDB
|accessdate=15 January 2008
|last=
|first=
}}
  Rotten Tomatoes lists Snatch as having 73% of the reviews (133 reviews listed in total) as being "fresh" (positive). 
{{cite web
|url=http://www.rottentomatoes.com/m/snatch/
|title=Snatch – Rotten Tomatoes
|publisher=Rotten Tomatoes
|accessdate=4 October 2008
|last=
|first=
}}
 

Snatch also appears on Empire magazines 2008 poll of the 500 greatest movies of all time at number 466. 

While the film received mostly positive reviews, several reviewers commented negatively on perceived similarities in plot, character, setting, theme and style between Snatch and Ritchies previous work, Lock, Stock and Two Smoking Barrels. In his review, Roger Ebert, who gave the film two out of four stars, raised the question of "What am I to say of Snatch, Ritchies new film, which follows the Lock, Stock formula so slavishly it could be like a new arrangement of the same song?", 
{{cite web
|url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20010119/REVIEWS/101190306/1023
|title=Reviews – Snatch
|publisher=Sun Times
|date=19 January 2001
|accessdate=15 January 2008
|last= Ebert
|first= Roger
}}
  and writing in the New York Times Elvis Mitchell commented that "Mr. Ritchie seems to be stepping backward when he should be moving ahead". 
{{cite news
|url=http://www.nytimes.com/2001/01/19/arts/19SNAT.html?ex=1200546000&en=005cfdd5f436065d&ei=5070
|title=Snatch: Man, All They Wanted Was to Go Buy a Trailer
|publisher=New York Times
|accessdate=15 January 2008
|last= Mitchell
|first= Elvis date = 19 January 2001
}} 
  Critics also argued that the movie was lacking in depth and substance; many reviewers appeared to agree with Eberts comment that "the movie is not boring, but it doesnt build and it doesnt arrive anywhere".  Movie Room Reviews gave the movie four stars and said "there is no movie quite like it". 

==Home media==
The film has been released in multiple incarnations on DVD.
 theatrical trailer TV spots, text/photo galleries, storyboard comparisons, and filmographies.
 Sony released a "Deluxe Collection" set in the companys superbit format. This release contained two discs, one being the special features disc of the original DVD release, and the other a superbit version of the feature. As is the case with superbit presentations, the disc was absent of the additional features included in the original standard DVD, such as the audio commentary. (The disc did still contain subtitles in eight different languages including a "pikey" track, which only showed subtitles for the character Mickey.)

Nine months later, on 3 June 2003, a single disc setup was released, with new cover art, containing the feature disc of the special edition set. This version was simply a repackaging, omitting the second disc.

===Deluxe edition error=== dealer button in the same theme. Also included was a supplemental booklet revealing extended filmography information about the cast as well as theatrical press kit production notes.

Soon after the set was released, it was discovered the feature disc that was supposed to contain the film in its original special edition incarnation (with audio commentary, etc.) was not included. Instead, the Superbit release, containing the higher quality version of the film, was in its place. 

==See also==
* Hyperlink cinema – the film style of using multiple inter-connected story lines
* Irish travellers
* Shelta

==Notes==
 

==External links==
 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 