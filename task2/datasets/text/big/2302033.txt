The Kelly Gang
 
{{Infobox film
  | name     = The Kelly Gang
  | image    = Godfrey Cass (The Kelly Gang 1920).jpg
  | caption  = Godfrey Cass as Ned Kelly
  | director = Harry Southwell		
  | producer = Harry Southwell
  | writer   = Harry Southwell
  | starring = Godfrey Cass V. Upton Brown Horace Crawford
  | music    = 
  | cinematography = Charles Herschell
  | editing  = 
| studio = Southwell Screen Plays
  | distributor = 
  | released = 21 February 1920
  | runtime  = 7,500 feet
  | language = Silent film English intertitles
  | country = Australia
  | budget   = ₤450   accessed 6 December 2011 
  | gross = ₤20,000 
  }}

The Kelly Gang is an Australian feature length film about the Australian bush ranger, Ned Kelly. The film was released in 1920, and is the second film to be based on the life of Ned Kelly, the first being The Story of the Kelly Gang, released in 1906. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 96. 

==Cast==
*Godfrey Cass as Ned Kelly
*V. Upton Brown as Dan Kelly
*Horace Crawford as Joe Byrne
*Jack McGowan as Steve Hart
*Robert Inman as Aron Sherritt
*Thomas Sinclair as Sergeant Kennedy
*Harry Southwell as Sergeant Steele
*Cyril Mackay as Constable McIntyre
*Adele Inman as Kate Kelly
*Maud Appleton as Ms Kelly
*Frank Tomlin as Constable Scanlon
*

==Production==
Filming took place in late 1919 in a temporary studio in the Melbourne suburb of Coburg with additional scenes shot on the outskirts of Melbourne at Croydon and Warburton. 

==Release== banned films on bushranging but this movie escaped it, most likely due to its opening warning against breaking the law. The movie was reasonably successful. 

==Trivia==
"The Kelly Gang" was also the name given to a small provisional unit of Australian troops during the Second World War who served in the British campaign in Syria.  The unit only existed for four weeks, but had a colourful history of mounted service.

== Other Ned Kelly films ==
* The Story of the Kelly Gang (1906)
* When the Kellys Were Out (1923)
* When the Kellys Rode (1934)
* A Message to Kelly (1947)
* The Glenrowan Affair (1951)
* Stringybark Massacre (1967) Ned Kelly (1970)
* Reckless Kelly (1993) Ned Kelly (2003)
* Ned (movie)|Ned (2003)

==References==
 

==External links==
*  at the National Film and Sound Archive
*  

 
 

 

 
 
 
 
 
 
 
 

 
 