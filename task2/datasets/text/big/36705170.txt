Attakathi
{{Infobox film
| name           = Attakathi
| image          = Attakathi.jpg
| caption        = Early promotional poster
| director       = Pa. Ranjith
| producer       = C. V. Kumar
| writer         = Pa. Ranjith Dinesh  Nandita   Iyshwarya Rajesh
| music          = Santhosh Narayanan
| cinematography = P. K. Varma
| editing        = Leo John Paul
| studio         = Thirukumaran Entertainment
| distributor    = Studio Green
| released       =  
| runtime        = 125 Minutes
| language       = Tamil
| country        = India
| budget         = 
}}
 Indian Tamil Tamil romantic Independence Day of India.  Attakathi fetched positive reviews and emerged successful at the box office. 

== Plot ==

Dinakaran (Attakathi Dinesh|Dinesh) referred as Atta among his friends is a regular semi-urban teenager growing up in a small village in the outskirts of Chennai. He did well in his school exams, but failed his English paper. Rather than studying for the second attempt, he wastes time looking for a girlfriend since he has made a pact with his best friends; to fall in love and get married.

Dinakaran first falls for a girl he meets at the bus stop named Poornima (Nandita). She appears to like him too as she smiles at him and accepts his gifts. But when he goes after her, she suddenly refers to him as big brother and tells him to stop following her around as she feels awkward. Dinakaran tries to feel sad, but he cannot since he was never really in love in the first place. It was just an infatuation. Dinakaran next starts wooing a distant relative who comes visiting at his house. Unfortunately, she turns out to have fallen in love with his elder brother. He then gets beaten up by the boys at a neighboring village for following two of their girls. He learns martial arts to protect himself and impress the girls, but nothing of that sort works out.

Dinakaran finally gives up on falling in love and concentrates on his studies. He finally passes his English test and enrolls into the local college. He is taken under the wing of an overprotective senior, who is the self-proclaimed underground student leader whom everyone refers to as route thala or designated don of a route/street. Dinakarans life takes a sudden turn as he mixes with the wrong group. He becomes less sociable and is always involved in fights and other problems. When his senior finally graduates, he makes Dinakaran the next route thala. Dinakaran takes his responsibility very seriously and is soon both respected and feared by all the students in his college.

One day, Dinakaran gets a new junior in the form of Poornima (Nandita), his childhood friend and one of the girls he tried to woo as a teenager. He tries to avoid her as it reminds him of the time he was a complete loser who was constantly humiliated by his own feeble attempts to impress girls. As time passes, he cannot avoid falling in love with Poornima, especially when she shows him an old bus ticket he bought for her. She only keeps things from people she really like as a kind of memorabilia. Dinakaran is convinced that she is in love with him too, but cannot bring himself to propose to her. He even changes back to his old haircut and the usual way he used to dress just because Poornima likes it more. His family and friends tease him, which embarrasses him deeply. But once Poornima complements him on his new look, he does not mind anymore.

Things take a turn for the worst when Poornimas family finds out she is in love with someone they disapprove of and they fix an arranged marriage for her. Dinakaran panics and recruits his old friends to help him out. His parents seemingly allow him to leave home and bless his actions. Most of his friend decide to arrange for Dinakaran to elope with Poornima with the help of their relatives, though one of them disagrees as he feels they should ask Poornima how she really feels and then decide what they should do. However, Dinakaran becomes impatient and goes after Poornima, only to face her elder brother in an ensuing fight. Dinakaran manage to escape and waits for Poornima at the place they had arranged to meet, but she never shows up.

Dinakaran then takes the public bus back home, where he meets Poornima. As it turns out, she was never in love with Dinakaran in the first place. Her lover was another boy named Dinakaran, which made her family assume he was the one she was in love with all the while. While her family was busy going after Dinakaran, Poornima had already married the other Dinakaran and was now on the way to watch a movie with her newly wedded husband. Dinakaran is heartbroken, but cannot stay sad forever, and bounces back quickly as he soon realizes he was never in love with Poornima at all, it was all just a phase of infatuation once again.

Post-credits it reveals that Dinakaran studied hard and managed to become a teacher, while taking care of his mother and has finally tied the knot.

== Cast ==
* Dinesh Ravi as Dinakaran
* Nandita as Poornima
* Iyshwarya Rajesh as Amudha
* Sophia as Divya
* Shalini as Nadhiya
* Mahendran as Mahendran
* Kalaiyarasan as Dinakaran
* Dhilsa
* Jangiri Madhumitha

== Soundtrack ==
{{Infobox album
| Name       = Attakathi
| Type       = Soundtrack
| Artist     = Santhosh Narayanan
| Cover      = 
| Released   = 9 January 2012 Feature film soundtrack
| Length     =  Tamil	
| Label      = Think Music
| Producer   = Santhosh Narayanan
| Reviews    =
| Last album = 
| This album = Attakathi   (2012)
| Next album = Pizza (2012 film)|Pizza   (2012)
}}

Attakathis soundtrack was composed by newcomer Santhosh Narayanan, who previously worked as an assistant to composer A. R. Rahman except Nadukadalula Kappala by the singer Gaana Bala himself.  The launch of the album took place on 9 January 2012 at Sathyam Cinemas. Celebrities including directors Venkat Prabhu, Sasi (director)|Sasi, Vetrimaran, composer Yuvan Shankar Raja, producers Abirami Ramanathan, S. Thanu, T. Siva and actors Shiva (actor)|Shiva, Vaibhav Reddy and S. P. B. Charan among others.  The album consists of seven tracks. Mastering of the songs was done at Studios 301 in Sydney, Australia. 

Tracklist
{{tracklist
| extra_column    = Singer(s)
| lyrics_credits  = yes
| total_length    =

| title1          = Aasai Oru Pulveli
| extra1          = Pradeep, Kalyani Nair Kabilan
| length1         = 3:52

| title2          = Aadi Pona Aavani
| extra2          = Gaana Bala
| length2         = 4:41 Kabilan

| title3          = Adi En Gana Mayil
| extra3          = Ayinjivaakkam Muthu
| length3         = 2:28
| lyrics3         = -

| title4          = Podi Vechi Pudippan
| extra4          = Sathyan (singer)|Sathyan, Palakkad Sreeram, Brinda
| length4         = 4:08
| lyrics4         = Muthamizh

| title5          = Band Musical Sequence
| extra5          = -
| length5         = 1:03
| lyrics5         = -

| title6          = Nadukadalula Kappala
| extra6          = Gaana Bala
| length6         = 1:39
| lyrics6         = Gaana Bala

| title7          = Vazhi Parthirundhen
| extra7          = Pradeep
| length7         = 2:56
| lyrics7         = Pradeep
}}

== Critical Reception ==
Attakathi received mostly positive reviews upon release. Rediffs Pavithra Srinivasan rated it 3.5 out of 5 and cited: "Though Attakathis second half drags a little, theres no doubt that Attakathi is well worth your time and patronage".  Sifys critic wrote: "We recommend that you make time for this charming little treat of a film, as it has an inherent sweetness and honesty that will stay with you".  Behindwoods rated the film 2.5 out of 5 and noted that it was "realistic; light hearted and turns out to be quite a ride, with its share of speed bumps", while calling it a "roller-coaster ride".  Similarly, Rohit Ramachandran of Nowrunning.com concluded that it was "simple, modest, and light", giving it 3 out of 5.  Vivek Ramz of in.com rated it 3.5/5 stating that it was a "small yet beautiful film with its heart at the right place. It definitely deserves a watch for its refreshing screenplay and unique treatment". 

== Awards and Nominations == SIIMA Awards 2013

* Won - Best Debut Producer - C. V. Kumar Dinesh
 7th Annual Vijay Awards
 Dinesh
* Nominated - Best Debut Actress - Nandita
* Nominated - Best Male Playback Singer - Gaana Bala
* Nominated - Best Lyricist - Gaana Bala Dinesh

; Jaya TV Awards - 2012   
 Dinesh
* Won - Best Director - Pa. Ranjith
* Won - Best Music Director - Santhosh Narayanan
* Won - Best Producer - C. V. Kumar
* Won - Sensational Debutant Actress - Nandita

; Vikatan Awards 2012
 Dinesh
* Won - Best Playback Singer (Male) - Gaana Bala
 6th Edison Awards 
 Dinesh

; Chennai Times Film Awards 2012   
 Dinesh
* Nominated - Promising Newcomer (Female) - Nandita
* Nominated - Best Music Director - Santhosh Narayanan
* Nominated - Best Lyrics - Kabilan
* Nominated - Best Singer (Male) - Gaana Bala
* Nominated - Best Youth Film
 Big Tamil Melody Awards 2013  

* Nominated - Best Singer (Male) - Pradeep

== References ==
 

== External links ==
*  
 

 
 
 
 
 