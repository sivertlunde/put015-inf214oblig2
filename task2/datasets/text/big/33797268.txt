1920: The Evil Returns
 
 

 
{{Infobox film
| name           = 1920: The Evil Returns
| image          = 1920 Evil Returns poster.jpg
| alt            =  
| caption        = Movie poster
| director       = Bhushan Patel
| producer       = Vikram Bhatt http://bollywood-movie-review.com/1920-evil-returns-reviews-cast-plot-summary-song.html 
| writer         = Vikram Bhatt
| screenplay     = Rensil DSilva
| starring       = Aftab Shivdasani Tia Bajpai Vidya Malvade Sharad Kelkar
| music          = Chirantan Bhatt
| cinematography = Naren Gedia
| casting        = 
| editing        =  Swapnil Raj
| studio         =  
| distributor    =ASA Productions and Enterprises Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         =   90&nbsp;million  
| gross          =   (3 weeks domestic)  
}}

1920: The Evil Returns is a Bollywood horror film written by Vikram Bhatt and directed by Bhushan Patel. The film is a quasi-sequel to a 2008 film 1920 (film)|1920,  and stars Aftab Shivdasani, Tia Bajpai, Vidya Malvade and Sagar Saikia in the lead roles.   The movie is third in a series of quasi-sequels released under the Bhatt Banner including Raaz – The Mystery Continues, Murder 2, Jism 2, Jannat 2 and Raaz 3D each of which had nothing to do with their respective prequels, but somehow fell in the same genre following a similar story. The trailer was released on 28 September 2012. The film released on 2 November 2012 to mixed reception and fared better at the opening Box Office weekend (122.7&nbsp;million Net.) as compared to any other releases that week except Skyfall (270&nbsp;million Net.).  Boxofficeindia declared the film as a hit after its two-week run.  Professor U Dinesh Kumar of IIM Bangalore along with his team worked with Ami Shah of IntelliAssist, the company that carried out the social media marketing for the film, and assessed Internet activities and campaigns; the team finally establish the connect between box office collections and promotions on social media. This case study is published by Harvard Business Publishing  and is being used by students in the US, Canada, Norway, Slovenia and Sri Lanka.  

==Plot==
In 1920, Jaidev Verma (Aftab Shivdasani) is a famous poet who lives the life of a loner as he is unable to meet the love of his life, Smruti (Tia Bajpai). They got to know each other through an exchange of letters and slowly fall in love. One day, Jaidev receives a letter informing him that Smruti had an accident and died. Now his sister Karuna (Vidya Malvade) is his only support system that keeps him motivated. One day Jaidev finds an unconscious girl near a lake and brings her home. After gaining consciousness, she is unable to remember anything from her past life except Jaidevs poems. Karuna becomes skeptical of her presence in the house and gets even more so when the keeper of the cemetery warns them of an evil spirit inside of her who is in love with her.

Jaidev is insistent on keeping her at home since he feels a connection with her. He even names her Sangeeta. Strange & spooky things start happening with Sangeeta like vomiting iron nails and seeing ghosts in her room. On their way to see a doctor, Sangeeta gets completely possessed by the ghost. To save her, the only person Jaidev can turn to is the cemetery keeper. Slowly Jaidev gets to know that Sangeeta is actually his lost love Smruti. He goes to Smrutis old address to find the truth. He discovers that Karuna had come there before, asking about Smruti. Jaidev returns home and finds Karunas body hanging in the forest with suicide notes around it. From Karunas letters, he comes to know that his best friend Amar (Sharad Kelkar), who was always jealous of Jaidevs success, exploited Karuna to get his revenge. When Amar discovered that Jaidev loved Smruti, he went to Smruti, posing as Jaidev, and took her to Shimla to exploit her, but in the process, Amar dies. It is his spirit possessing Smruti.

The cemetery keeper warns Jaidev that the spirit is very vengeful and has to be deceitfully taken to the same place it all happened--Amars residence in Shimla. Once Smruti touches Amars corpse, Amars spirit will have to leave Smrutis body and return to his own body; the corpse can be set on fire then, releasing Amars spirit from the karmic cycle of life and death. Whilst doing this, Smruti must not know where she is being taken, else the spirit will also know, so Smruti is made unconscious and completely enveloped in a sacred cloth. They reach the designated place, but the cemetery keeper trips and the sacred cloth moves away from Smrutis face, awakening the spirit. The possessed Smruti unleashes mayhem on all four persons, and in a matter of time, everyone except Jaidev is brutally killed by her. 

Jaidev is badly injured in battle of evil versus just, and the spirit in Smruti burns the corpse of Amar, thus forever remaining in her body. Jaidev helplessly pleads with the spirit to kill him since there is no meaning in letting him live if the spirit will take Smruti from him. Amars spirit refuses, saying that this is exactly what he wanted: for Jaidev to suffer. Jaidev cuts a rope attached to a loft in ceiling; a corpse falls from there, landing on Smruti, and making contact with her touch. It is revealed in a flashback that Jaidev and the group had hidden the real corpse of Amar in the ceiling as precaution.The corpse comes alive as Amar is forced to return to his original body. Enraged, Amars corpse tries to kill Smruti but Jaidev saves her, and they live happily with each other in the end.

==Cast==

* Aftab Shivdasani as Jaidev Verma
* Tia Bajpai as Smriti/Sangeeta
* Vidya Malvade as Karuna Verma
* Sharad Kelkar as Amar
* Vicky Ahuja as Bankimlal
* Sanjay Sharma as Bhola
* Naresh Sharma as Driver of Horse Carriage 

==Soundtrack==
The soundtrack is composed by Chirantan Bhatt and got positive reviews from critics.

{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| title1 = Apnaa Mujhe Tu Lagaa
| extra1 = Sonu Nigam
| length1= 06:06
| title2 = Uska Hi Banana
| extra2 = Arijit Singh   
| length2= 05:29
| title3 = Jaavedaan Hai 
| extra3 = Krishnakumar Kunnath|KK, Suzanne DMello
| length3= 05:48
| title4 = Khud Ko Tere
| extra4 = Mahalaxmi Iyer
| length4= 05:09
| title5 = Majboor Tu Bhi Kahin 
| extra5 = Amit Mishra
| length5= 04:53
}}

==Critical reception==
{|class="wikitable infobox" style="width:20em; font-size: 80%; text-align: center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0" cellspacing="0"
! colspan="2" style="font-size:120%;" | Professional Reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review scores
|-
! Source
! Rating
|-
|koimoi
| 
|-
|The Times of India
| 
|- Bollywood Hungama
| 
|- Rediff
| 
|-
|}

The movie received average to negative reviews. Renuka Vyavahare of Times of India gave it 3 stars. "1920 gives you the creeps...watch it." said ToI.  Rediff Movies said "1920 Evil Returns is yet another needless horror film. Its cold and bland." and gave it 1 star.  Roshni Devi of Koimoi gave it 2 stars. "Watch it only if youre desperate for some uninspiring horror. Give it a rest otherwise." wrote Roshni Devi.  Social Movie Rating site MOZVO gave it a rating of 2.3 putting it in Below Average category.  Taran Adarsh of Bollywood Hungama gave it 2.5 stars. 

==Box office==
1920 – Evil Returns had a decent opening weekend where it collected around   nett.    The film had a good first week and collected   nett. {{cite web|title=1920 – Evil Returns Week One Territorial Breakdown
 |url=http://boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=5071&nCat=|publisher=boxofficeindia|accessdate=12 November 2012}}  It had collected around   nett in its second week taking its total to   nett and is declared a Semi Hit.    It finished at   domestically and was elevated to hit status.

==Sequel==

1920– Evil Returns had high expectations riding on it. but while the movie wasnt able to meet the chills and thrills of its first part, it didnt face a rude rejection either. And thats reason enough for producer Vikram Bhatt to go ahead and chart out a plan for the third instalment – 1920 – Part III in this scary franchise, right? "I will be making another sequel to 1920. We are working on the script right now. It is too early to talk about it as we are developing the concept for it", Vikram said in an interview. Bhatt revealed that the third part wont be in 3D, like the first two. And that the star cast will be finalised only after the script is locked. The sequel to the movie is named 1920 London and is to be released on March - April 2015.

== References ==
 
the total was 22.83 its super hit

== External links ==
* 
*  

 
 
 
 
 
 
 