Sleepy Hollow (film)
{{Infobox film name = Sleepy Hollow image = Sleepy hollow ver2.jpg caption = Theatrical release poster director = Tim Burton producer = Scott Rudin Adam Schroeder screenplay = Andrew Kevin Walker Tom Stoppard (uncredited) story = Andrew Kevin Walker Kevin Yagher based on =   starring = Johnny Depp Christina Ricci music = Danny Elfman cinematography = Emmanuel Lubezki editing = Chris Lebenzon Joel Negron studio = Mandalay Pictures American Zoetrope distributor = Paramount Pictures released =   runtime = 105 minutes    country =   language = English
|budget = $70 million  gross = $206 million 
}} fantasy horror horror adventure Sleepy Hollow by a mysterious Headless Horseman.

It is the first film by Mandalay Pictures. Development began in 1993 at Paramount Pictures with Kevin Yagher originally set to direct Andrew Kevin Walkers script as a low-budget slasher film. Disagreements with Paramount resulted in Yagher being demoted to prosthetic makeup designer, and Burton was hired to direct in June 1998. Filming took place from November 1998 to May 1999, and Sleepy Hollow was released to generally favorable reviews from critics, and grossed approximately $207 million worldwide. Production designer Rick Heinrichs and set decorator Peter Young won the Academy Award for Best Art Direction.

==Plot== Hessian mercenary from the American Revolutionary War who rides at night on a massive black steed in search of his missing head.
 Headless Horseman himself, who kills the Magistrate Samuel Phillipse (Richard Griffiths) on sight. Boarding in a room at the home of the towns richest family and the Van Garretts next of kin, the Van Tassels, Crane develops an attraction to their daughter Katrina (Christina Ricci), while he is plagued by nightmares of his mothers horrific torture when he was a child. This attraction is deeply resented by Brom van Brunt (Casper Van Dien), a suitor to Katrina, who scares Crane in a prank by posing as the Headless Horseman. Riding into the Western Woods with the orphaned Young Masbath, son of the Horsemans fifth victim (before Magistrate Phillipse), Crane and Katrina come across the cave dwelling of a reclusive sorceress. She reveals the location of the gnarled Tree of the Dead, which marks the Horsemans grave, as well as his portal into the natural world from the supernatural.

Crane discovers that the ground is freshly disturbed and, digging through, discovers the Horsemans skeleton and that the skull is missing. He realizes that whoever dug up and stole the skull is the person controlling the Horseman. Just then, the Horsemans ghost bursts out of the tree and gallops towards Sleepy Hollow. Crane attempts to follow but winds up lost. The Killian family are taken by the Horseman and Brom is killed - cut in half - when trying to stop the Horseman.

Crane starts to believe that a conspiracy links all the deaths together, so he goes to the town Notary James Hardenbrook (Michael Gough) to look into Van Garretts Last Will. Hardenbrook confesses Van Garrett had made a new will just before he died, leaving all his possessions to his new bride, Emily Winship, who Crane had learned from the late Magistrate Phillipse was pregnant at the time of her death and thinks that the father of the child might have killed Emily to keep the secret hidden. Crane deduces that all who knew about the new will were the victims of Horseman and that Katrinas father Baltus Van Tassel (Michael Gambon), who would have inherited the fortune, is the person holding the skull. Katrina, finding out that Crane suspects her father, burns the evidence that Crane has accumulated and tells him that she doesnt love him anymore.

In fear of the Horseman, Hardenbrook hangs himself and a town council is held in the town church. The Horseman seemingly kills Katrinas stepmother, Lady Van Tassel, and heads off to the church to get Baltus, with the townspeople filing in just as the he arrives. With the men firing muskets as he circles the church, Crane realizes the Horseman cant enter the church grounds due to some protective supernatural force. A massive fight breaks out in the church when Dr. Thomas Lancaster (Ian McDiarmid) suggests confessing for forgiveness, and is killed by Reverend Steenwyck (Jeffery Jones), who is in turn shot by a frightened Baltus. The chaos ends only when the Horseman harpoons Baltus through a church window using a pointed church fence post attached to a rope, dragging him out and acquiring his head.

As Crane is leaving Sleepy Hollow, he becomes suspicious when the hand of the corpse of Lady Van Tassel has a wound which shows signs of having been caused post-mortem. His suspicions are confirmed to be right when the real Lady Van Tassel (Miranda Richardson) emerges, alive, from the dark and shocks her step-daughter Katrina into a faint. Katrina awakens and eventually uncovers a plot revolving around revenge on the Van Garretts and land rights with the Horseman controlled by Lady Van Tassel, who sends the supernatural killer after Katrina now to solidify her hold on what she considers her property, a piece of land unjustly claimed by Baltus. She also reveals that she had just murdered the mysterious witch in the Western Woods, her own sister, for her role in aiding Crane and Young Masbath.

Following a fight in the local windmill and a stagecoach chase through the woods, Crane eventually thwarts Lady Van Tassel by throwing the skull to the Horseman, breaking the curse. The Horseman, no longer under Lady Van Tassels control, hoists her up on his horse, then rides to Hell taking her with him, fulfilling her end of the deal with the Devil. With the Headless Horseman menace eradicated, Crane returns home to New York with Katrina and Young Masbath, just in time for the new century.

==Cast==
*Johnny Depp as Ichabod Crane
*Christina Ricci as Katrina Van Tassel Count Hessian Headless Horseman
*Marc Pickering as Young Masbath, whose father was a victim of the Horseman
*Casper Van Dien as Brom Van Brunt
*Michael Gambon as Baltus Van Tassel
*Miranda Richardson as Lady Mary Van Tassel / Crone Sister
*Jeffrey Jones as Reverend Steenwyck
*Richard Griffiths as Magistrate Samuel Philipse
*Ian McDiarmid as Dr. Thomas Lancaster
*Michael Gough as Notary James Hardenbrook
*Christopher Lee as the Burgomaster
*Steven Waddington as Killian, Beths husband
*Claire Skinner as Beth Killian, the Midwife Alun Armstrong as the High Constable
*Martin Landau as Peter Van Garrett Peter Guinness as Lord Crane, Ichabods father Lisa Marie Smith as Lady Crane, Ichabods mother

==Production==
===Development=== The Crucible. We started developing it before horror movies came back." 
 trailer and Black Sunday—particularly homage to Jason and CalArts in Disney version as one of the layout artists on the chase, and he brought in some layouts from it, so that was exciting. It was one of the things that maybe shaped what I like to do." Burton, Salisbury, pp. 161–169  Burton worked with Walker on rewrites, but Rudin suggested that Tom Stoppard rewrite the script  to add to the comical aspects of Ichabods bumbling mannerisms, and emphasize the characters romance with Katrina. His work went uncredited through the WGA screenwriting credit system. 
 prosthetic snipe Death on 1939 Sherlock The Addams Ed Wood) as Reverent Steenwyck, Christopher Walken (Max Schreck in Batman Returns) as the Hessian Horseman, Martin Landau (Ed Wood) in a cameo role, and Hammer veteran Michael Gough (Alfred in Burtons Batman films), whom Burton tempted out of retirement.  The Hammer influence was further confirmed by the casting of Christopher Lee in a small role as the Burgomaster who sends Crane to Sleepy Hollow. 

===Filming===
 
 Tarrytown    for an October 1998 start date.    The Historic Hudson Valley organization assisted in scouting locations, which included the Philipsburg Manor House and forests in the Rockefeller State Park Preserve.    "They had a wonderful quality to them," production designer Rick Heinrichs reflected on the locations, "but it wasnt quite lending itself to the sort of expressionism that we were going for, which wanted to express the feeling of foreboding."    Disappointed, the filmmakers scouted locations in Sturbridge, Massachusetts, and considered using Dutch colonial villages and period town recreations in the Northeastern United States. When no suitable existing location could be found, coupled with a lack of readily available studio space in the New York area needed to house the productions large number of sets, producer Scott Rudin suggested the UK. 

Rudin believed England offered the level of craftsmanship in period detail, painting and costuming that was suitable for the films design.    Having directed  .  The majority of filming took place at Leavesden, with studio other work at   scenes were shot using a sound stage in Yonkers, New York the following May.  

===Design===
  ]]
 production design Black Sunday—particularly the supernatural feel they evoked as a result of being filmed primarily on sound stages. Heinrichs was also influenced by American colonial architecture, German Expressionism, Dr. Seuss illustrations, and Hammers Dracula Has Risen from the Grave.    One sound stage at Leavesden was dedicated to the "Forest to Field" set, for the scene in which the Headless Horseman races out of the woods and into a field. This stage was then transformed into, variously, a graveyard, a corn field, a field of harvested wheat, a churchyard, and a snowy battlefield. In addition, a small backlot area was devoted to a New York City street and waterfront tank. 

====Cinematography==== Great Expectations, monochromatic effect contrast of the film stock was increased in post-production to add to the monochromatic feel.   

Leavesden Studios, a converted airplane factory, presented problems because of its relatively low ceilings. This was less of an issue for The Phantom Menace, in which set height was generally achieved by digital means. "Our visual choices get channeled," Heinrichs elaborated, "so you end up with liabilities that you tend to exploit as virtues. When youve got a certain ceiling height, and youre dealing with painted backings, you need to push atmosphere and diffusion."  This was particularly the case in several exteriors that were built on sound stages. "We would mitigate the disadvantages by hiding lights with teasers and smoke." 

====Visual effects==== The Mill handled motion control photography.  In part a reaction to the computer-generated imagery|computer-generated effects in Mars Attacks!, Burton opted to use as limited an amount of digital effects as possible.  Ray Park, who served as the Headless Horseman stunt double, wore a blue ski mask for the chroma key effect, digitally removed by ILM.    Burton and Heinrichs applied to Sleepy Hollow many of the techniques they had used in stop motion animation on Vincent (1982 film)|Vincent—such as forced perspective sets. Burton, Salisbury, pp. 170–176 

The windmill was a 60-foot-tall forced-perspective exterior (visible to highway travellers miles away), a base and rooftop set and a quarter-scale miniature. The interior of the mill, which was about 30-feet high and 25-feet wide, featured wooden gears equipped with mechanisms for grinding flour. A wider view of the windmill was rendered on a Leavesden soundstage set with a quarter-scale windmill, complete with rotating vanes, painted sky backdrop and special-effects fire. "It was scary for the actors who were having burning wood explode at them," Heinrichs recalled. "There were controls in place and people standing by with hoses, of course, but theres always a chance of something going wrong."  For the final shot of the burning mill exploding, the quarter-scale windmill and painted backdrop were erected against the outside wall of the "flight shed", a spacious hangar on the far side of Leavesden Studios. The hangars interior walls were knocked down to create a 450-foot run, with a 40-foot width still allowing for coach and cameras. Heinrichs tailored the sets so cinematographer Emmanuel Lubezki could shoot from above without seeing the end of the stage.   

Actor Ian McDiarmid, who portrayed Dr. Lancaster, had just finished another Leavesden production with  . He compared the aesthetics of the two films, stating that physical sets helped the actors get into a natural frame of mind. "Having come from the chroma key|blue-screen world of Star Wars it was wonderful to see gigantic, beautifully made perspective sets and wonderful clothes, and also people recreating a world. Its like the way movies used to be done." 

===Musical score===
{{Infobox album Name = Sleepy Hollow: Music from the Motion Picture Type = Soundtrack Artist = Danny Elfman Cover = Sleepy Hollow (soundtrack) cover art.jpg Background = soundtrack Released = November 16, 1999 Length = 67:52 Label = Hollywood Records
}}
 Golden Satellite Award and was also nominated by the Las Vegas Film Critics.

====Track listing ====
Tracks marked with ♦ are only available as a bonus track on disc 8 of the  .

The track numbers listed here do not therefore correspond to the original 1999 album.

#Introduction
#Main Titles
#Young Ichabod
#The Story...
#Masbaths Terrible Death
#Young Masbath ♦
#Phony Chase ♦
#Sweet Dreams
#A Gift
#Phillipses Death ♦
#Into the Woods / The Witch
#Mysterious Figure ♦
#More Dreams
#The Tree of Death
#Bad Dreams / Tender Moment
#Evil Eye
#The Church Battle
#Love Lost
#The Windmill
#The Chase
#The Final Confrontation
#A New Day!
#End Credits

==Release== online launch of a motion picture to date."  The site (sleepyhollowmovie.com) offered visitors live video chats with several of the filmmakers hosted by Yahoo! Movies and enabled them to send postcards, view photos, trailers and a six-minute behind-the-scenes featurette edited from a broadcast that aired on Entertainment Tonight. Extensive tours of 10 sets where offered, where visitors were able to roam around photographs, including the sets for the entire town of Sleepy Hollow, forest, church, graveyard and covered bridge. Arthur Cohen, president of worldwide marketing for Paramount, explained that the "Web-friendly" pre-release reports    from websites such as Aint It Cool News and Dark Horizons   encouraged the studio to create the site.  In the weeks pre-dating the release of Sleepy Hollow, a toy line was marketed by McFarlane Toys.  Simon & Schuster also published The Art of Sleepy Hollow (ISBN 0671036572), which included the films screenplay and an introduction by Tim Burton.  A novelization, also published by Simon & Schuster, was written by Peter Lerangis. 
 David Walsh of the National Institute on Media and the Family criticized the films financial success from the exaggeration of gore. "The real impact is not so much that violent images create violent behavior," Walsh explained, "but that they create an atmosphere of disrespect." Burton addressed the concerns as a matter of opinion. "Everyone has a different perception of things. When I was a kid," Burton continued, "I was probably more scared by seeing John Wayne or Barbra Streisand on the big screen than by seeing violence." 

Paramount Home Video first released Sleepy Hollow on DVD in the United States on May 23, 2000.  The HD DVD release came in July 2006,  while the film was released on Blu-ray Disc two years later, in June 2008. 

===Reception=== average score of 6.2/10, with the sites consensus stating, "Sleepy Hollow entertains with its stunning visuals and creepy atmosphere."   Metacritic, another review aggregator, assigned the film a weighted average score of 65 (out of 100) based on 35 reviews from mainstream critics, considered to be "generally favorable". 
 TIME Magazine, "Burtons richest, prettiest, weirdest   since Batman Returns. The simple story bends to his twists, freeing him for an exercise in high style." 

David Sterritt of The Christian Science Monitor highly praised Burtons filmmaking and the high-spirited acting of cast, but believed Andrew Kevin Walkers writing was too repetitious and formulaic for the third act.  "You go into a Tim Burton film wanting to be transported, but Sleepy Hollow is little more than a lavish, art-directed slasher movie."
 The Mummy, and said "it feels like every high-powered action climax of the last 10 years. Personally, Id rather see Burton so intoxicated by a movie that he lost his head." 
 Andrew Johnston of Time Out New York wrote: "Like the best of Burtons films, Sleepy Hollow takes place in a world so richly imagined that, despite its abundant terrors, you cant help wanting to step through the screen." 
 Doug Walker linked the film to the Hammer Films style of horror cinematography, considering it an homage to those movies, comparing the usage of dignified British actors, choices in color and movie sets and character relations. Walker gave it the merit of recreating the "very specific genre" of Hammer Films, citing the skill and "clever casting" Burton used to manage this. 
 Ichabod and Mr. Toad. When it comes to one of Americas great stories, Burton obviously couldnt care less." 

==Accolades==
{| class="wikitable sortable" width="95%"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|- Academy Awards  March 26, 2000 Best Production Design Peter Young
|  
|- Best Cinematography
| Emmanuel Lubezki
|  
|- Best Costume Design
| Colleen Atwood
|  
|-
| rowspan="3"| British Academy Film Awards  April 9, 2000 Best Production Design
| Rick Heinrichs
|  
|- Best Production Design
| Colleen Atwood 
|  
|- Best Visual Effects
| Jim Mitchell, Kevin Yagher, Joss Williams, Paddy Eason
|  
|-
| rowspan="11"| Academy of Science Fiction, Fantasy & Horror Films 
| rowspan="11"| June 6, 2000 Best Horror Film
| Scott Rudin, Adam Schroeder
|  
|- Best Director
| Tim Burton
|  
|- Best Writing
| Andrew Kevin Walker
|  
|- Best Actor
| Johnny Depp
|  
|- Best Actress
| Christina Ricci
|  
|- Best Supporting Actor
| Christopher Walken
|  
|- Best Supporting Actress
| Miranda Richardson
|  
|- Best Music
| Danny Elfman
|  
|- Best Costume
| Collen Atwood
|  
|- Best Make-up
| Kevin Yagher, Peter Owen
|  
|- Best Special Effects
| Jim Mitchell, Kevin Yagher, Joss Williams, Paddy Eason
|  
|-
 American Society of Cinematographers  February 20, 2000 Outstanding Achievement in Cinematography
| Emmanuel Lubezki
|  
|-
| rowspan="1"| Art Directors Guild  February 8, 2000 Excellence in Production Design for a Feature Film Andrew Nicholson, Bill Hoes, Julian Ashby, Gary Tompkins, Nick Navarro
|  
|- BMI Film & Television Awards 
| rowspan="1"| December 8, 2014 BMI Film Music Award
| Danny Elfman
|  
|- Blockbuster Entertainment Awards   
| rowspan="4"| May 9, 2000
| Favorite Actor - Horror
| Johnny Depp
|  
|-
| Favorite Actress - Horror
| Christina Ricci
|  
|-
| Favorite Supporting Actress - Horror
| Miranda Richardson 
|  
|-
| Favorite Supporting Actor - Horror
| Marc Pickering
|  
|- Boston Society of Film Critics Awards  December 12, 1999 Best Cinematography
| Emmanuel Lubezki
|  
|- Chicago Film Critics Association  March 13, 2000 Best Cinematography
| Emmanuel Lubezki
|  
|-
| rowspan="1"| Costume Designers Guild  February 25, 2000 Excellence in Period/Fantasy Costume Design for Film
| Colleen Atwood
|  
|- Hollywood Makeup Artist and Hair Stylist Guild Awards  March 19, 2000
| Best Character Makeup - Feature
| Kevin Yagher, Peter Owen, Liz Tagg, Paul Gooch
|  
|-
| rowspan="1"| International Film Music Critics Association  February 23, 2012 Best Archival Release of an Existing Score
| Danny Elfman  
|  
|-
| rowspan="1"| International Film Music Critics Association  February 4, 1999 Film Score of the Year
| Danny Elfman
|  
|-
| rowspan="1"| International Horror Guild  May 12, 2000
| Best Film
| 
|  
|-
| rowspan="1"| Italian National Syndicate of Film Journalists  May 12, 2000
| Best Foreign Director
| Tim Burton
|  
|-
| rowspan="4"| Las Vegas Film Critics Society Awards 
| rowspan="4"| January 18, 2000
| Best Score
| Danny Elfman
|  
|-
| Best Cinematography
| Emmanuel Lubezki
|  
|-
| Best Costume Design
| Colleen Atwood 
|  
|-
| Best Production Design
| Rick Heinrichs
|  
|-
| rowspan="1"| Los Angeles Film Critics Association Awards {{cite web|url=http://articles.latimes.com/1999/dec/12/local/me-43179|title=
The Insider Wins Top L.A. Film Critics Award|publisher=The LA Times|date=December 12, 1999|accessdate=December 12, 1999}} 
| rowspan="1"| December 12, 1999
| Best Production Design
| Rick Heinrichs
|  
|-
| rowspan="1"| MTV Movie Awards {{cite web|url=http://www.mtv.com/ontv/movieawards/1999/|title=
1999 MTV Movie Awards|publisher=MTV|date=June 5, 2000|accessdate=June 5, 2000}}  June 5, 2000 Best Villain
| Christopher Walken
|  
|-
| rowspan="1"| Motion Picture Sound Editors  December 15, 1999
| Best Sound Editing - Effects & Foley
| Skip Lievsay, Thomas W. Small, Sean Garnhart, Lewis Goldstein, Paul Urmson, Craig Berkey, Richard L. Anderson, John Pospisil, Michael Dressel, Scott Curtis, Matthew Harrison, Tammy Fearing
|  
|-
| rowspan="1"| National Society of Film Critics  January 8, 2000 Best Cinematography
| Emmanuel Lubezki 
|  
|-
| rowspan="1"| New York Film Critics Circle Awards  January 9, 2000 Best Cinematographer
| Emmanuel Lubezki 
|  
|- Online Film & Television Association 
| rowspan="9"| January 12, 2000
| Best Original Score
| Danny Elfman
|  
|-
| Best Cinematography
| Emmanuel Lubezki 
|  
|-
| Best Production Design
| Rick Heinrichs, Ken Court, John Dexter, Andy Nicholson, Kevin Phipps, Leslie Tomkins, Peter Young
|  
|-
| Best Costume Design
| Colleen Atwood
|  
|-
| Best Makeup and Hairstyling
| Kevin Yagher, Peter Owen, Liz Tagg, Paul Gooch, Susan Parkinson, Bernadette Mazur, Tamsin Dorling
|  
|-
| Best Sound Mixing
| Lee Dichter, Robert Fernandez, Skip Lievsay, Frank Morrone
|  
|-
| Best Sound Effects
| Skip Lievsay
|  
|-
| Best Visual Effects
| James Mitchell, Kevin Yagher, Joss Williams, Paddy Eason  
|  
|-
| Best Official Film Website
| www.sleepyhollow.com
|  
|-
| rowspan="1"| Online Film Critics Society  January 2, 2000
| Best Cinematography
| Emmanuel Lubezki 
|  
|- Santa Fe Film Critics Circle Awards  January 9, 2000
| Best Cinematography
| Emmanuel Lubezki 
|  
|-
| rowspan="8"| Satellite Awards  January 16, 2000 Best Actor - Musical or Comedy
| Johnny Depp
|  
|- Best Original Score
| Danny Elfman
|  
|- Best Cinematography
| Emmanuel Lubezki 
|  
|- Best Art Direction
| Ken Court, John Dexter, Rick Heinrichs and Andy Nicholson
|  
|- Best Costume Design
| Colleen Atwood 
|  
|- Best Editing
| Chris Lebenzon
|  
|- Best Sound
| Gary Alpers, Skip Lievsay, Frank Morrone
|  
|- Best Visual Effects
| Jim Mitchell, Joss Williams
|  
|-
| rowspan="1"| Teen Choice Awards  August 6, 2000
| Film - Choice Actress
| Christina Ricci
|  
|-
| rowspan="1"| Young Artist Awards  March 19, 2000
|  
| Christina Ricci
|  
|-
|}

American Film Institute recognition:
*AFIs 100 Years...100 Thrills – Nominated
*AFIs 100 Years...100 Heroes and Villains:
** The Headless Horseman – Nominated Villain 

==See also==
*List of ghost films

==References==
 

==Further reading==
* 

==External links==
 
* 
* 
* 
* 
* 
* 
*  by Charlie Rose
* 
* 
* 

 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 