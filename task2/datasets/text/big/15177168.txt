Asylum Days
{{Infobox film
| name = Asylum Days
| image = Asylum Days Movie Poster.jpg
| image_size =
| caption =
| director = Thomas Elliott
| producer = Joy Czerwonky Jennifer Daly Thomas Elliott
| writer = Llywelyn Jones
| narrator =
| starring = C. Thomas Howell
| music = Jann Castor
| cinematography = Brian Baugh
| editing =
| distributor =
| released =
| runtime = 100 mins.
| country = United States
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
}}

Asylum Days is a 2001 thriller film directed by Thomas Elliott.

==Plot==
After losing her parents in the age of five in a car accident, Laurie Cardell is sent to an orphanage where she stays for two years, until a fire burn down the place. Years later, she becomes a famous Hollywood actress, but traumatized and haunted by her past. When the small time criminal Nathan (C. Thomas Howell) is released after spending most of his life in prison, he goes to the house of his brother Daniel, a young man that works in a rental and worships Laurie. Daniel has researched the entire life of Laurie and written a screenplay with her biography. Nathan decides to please his younger brother and asks Laurie to read the script. She refuses and he kills her manager and friend and abducts her. Nathan locks Laurie in the trunk of his car, and calls his brother to go to Saint Claire orphanage to shoot the movie. Once in the destroyed spot, secrets from the past are disclosed.

==Cast==
* C. Thomas Howell
* Charlie Weirauch
* Jason Widener
* Deborah Zoe
* Roark Critchlow
* Michael Crider
* Kristina Malota
* Delaney Niehoff

==External links==
*  
*  

 
 
 
 


 