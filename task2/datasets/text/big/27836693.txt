The Blonde with Bare Breasts
 
{{Infobox film
| name           = The Blonde with Bare Breasts
| image          = La Blonde aux Seins Nus Poster.jpg
| alt            = 
| caption        = Film poster
| director       = Manuel Pradal
| producer       = Alain Goldman
| writer         = Manuel Pradal
| starring       = Nicolas Duvauchelle Vahina Giocante
| music          = Carlo Crivelli
| cinematography = Hugues Espinasse
| editing        = Beatrice Herminie
| studio         = Légende Films
| distributor    = Seven Films
| released       =  
| runtime        = 
| country        = France
| language       = French
| budget         =
| gross          = 
}} French film written and directed by Manuel Pradal.

==Plot==
Julien (Nicolas Duvauchelle), 25 years old, and Louis (Steve le Roi), 14 years old, are brothers. Their father often beat Julien, who defended Louis from also being beaten by him. He is terminally ill now. The brothers do not care much, but try to obtain ownership of his river transport ship, which they already use.

A gang offers money for stealing the Édouard Manet painting La blonde aux seins nus (The Blonde with Bare Breasts) from the museum.  They agree, and Louis actually does it, while the young guard Rosalie (Vahina Giocante) is distracted. Rosalie comes after him, onto the ship, but Louis manages to lock her up. Later she is free to move around on the ship. Although she is sometimes treated rudely, she likes the adventure. The police suspect her of being involved. She helps by hiding the painting and herself, when the ship is searched.

Julien and Rosalie have sex. Louis is jealous and pretends to have hung himself by hanging up a life-size doll dressed in his clothes. Julien plans to kill Rosalie, because she knows they stole the painting, Louis tries to protect her by advising her to escape. She returns the painting to her father, who arranges its restoration. The gang that wanted to buy it beats up Julien, out of revenge. Rosalie reunites with the brothers on the ship. In the meantime, the brothers father died. Louis gets the ship, apparently because only he is his real son.

== Cast ==
* Nicolas Duvauchelle as Julien
* Vahina Giocante as Rosalie
* Tomasz Kowalski as Polonais
* Steve Le Roi as Louis

 

== Production ==
Filming began on 29 July 2008,   for a 9 week period near Paris and the Seine,  and by spring 2009 was completed. 

== Release ==
The film was first released in France on 16 January 2010, and was screened at film festivals, including Le Festival du Film de Cabourg   and Cannes Film Festival|Cannes.   The film had its world premiere in June 2010 in the Netherlands under the French title. 

== Reception ==
Of the films world premiere in the Netherlands, André Waardenburg of   village that still breathes the atmosphere of the impressionists delivers a few nice scenes).  

Jordan Mintzer of Variety (magazine)|Variety wrote, "Despite what some may consider a promising title, theres unfortunately little to show for it in The Blonde With Bare Breasts", calling Manuel Pradals film an "aimless intrigue".  Referring to Pradals earlier films, he wrote the film was "clearly a case of style over substance", and offered that the substance of the film was "so ephemeral and misconstrued" that it detracted from Yorgos Arvanitis otherwise superb location cinematography. 

== References ==
 

   

   

   

   

   

   

   

   

   

 

==External links==
* 

 
 
 
 
 
 
 
 