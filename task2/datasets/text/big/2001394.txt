Midnight Lace
{{Infobox film
| name           = Midnight Lace
| image          = Midnight Lace poster.jpg
| image_size     = 225px
| alt            = 
| caption        = Original poster David Miller
| producer       = Ross Hunter Martin Melcher Ben Roberts Based on play Matilda Shouted Fire by Janet Green
| narrator       = 
| starring       = Doris Day Rex Harrison John Gavin Myrna Loy Frank Skinner
| cinematography = Russell Metty
| editing        = Leon Barsha Russell F. Schoengarth
| studio         = 
| distributor    = Universal-International
| released       = October 13, 1960
| runtime        =  United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 thriller film David Miller. Ben Roberts is based on the play Matilda Shouted Fire by Janet Green.
 Universal Television for NBC in 1981.

==Plot==
Newlywed American heiress Kit Preston is living with her financier husband Tony on Grosvenor Square in London. Returning home in a dense fog, she is startled by an eerie, electronic-like voice threatening to kill her within the month.  The voice calls her by name and torments her as she runs to escape. Tony tries to convince her she has been the victim of a practical joker and suggests they travel to Venice for the honeymoon they never had.
 contractor Brian Younger, who startles her when he addresses her by name. Inside she encounters Malcolm Stanley, her maid Noras shiftless son, whose unctuous behavior annoys her.

Just after he leaves, Kit receives a call from the voice in the park, repeating his intention to kill her. Neighbor Peggy Thompson urges Tony to take Kit to Scotland Yard to discuss the situation. After questioning her, Inspector Byrnes tells Tony he suspects that Kit is merely a lonely wife in need of attention.  Meanwhile, Kits Aunt Bea arrives.  When Kit tells her of the phone calls, they both put it down to "telephone talkers."

Tony must cancel the trip to Italy due to continuing problems at work. Kit receives another call, but before Tony can take the phone to hear the voice, she hysterically hangs up. That evening, Tony and Kit meet Aunt Bea and her former beau Charles Manning at a nightclub, where Aunt Bea questions Tony about Kits nervousness. When Tony repeats the inspectors suspicions, Bea wonders if he may be right: Kit received the last call after hearing that the Italy trip was cancelled.

The next day, Kit is trapped in an elevator when the power goes out. She panics when she hears footsteps ominously approaching in the dark, and is relieved to discover that its Brian, who was on his way to warn her that his crew has blown a circuit. Brian escorts the visibly shaken Kit to a pub for a brandy. He relates an experience he had during World War II, suffering "blackouts" and once losing a whole day.  Kit, disturbed by his intense manner, returns home.  The pub owner asks Brian if she should include last nights phone charges in his bill.
 embezzled from the firm. Daniel knows company treasurer Manning has large gambling debts and suggests he is responsible for the loss. Malcolm confronts Kit at the ballet and asks for money. When she hesitates, he vaguely threatens her.

Kit becomes increasingly paranoid. Her reports of more calls and a visit from a mysterious stranger nobody else sees are met with scepticism by everybody. On the street shes pushed in front of an approaching bus and is nearly killed. Kit frantically begs Peggy to lie that she heard the voice on the phone, but the plan backfires: Tony reveals that their phone has been out of order.

Now certain that Kit is delusional, Tony and Bea take her to a physician who suggests that she may be suffering from a split personality and should see a psychiatrist. Tony decides to take Kit to Venice immediately and asks Bea to help her pack while he attends a board meeting.

Having dinner nearby, Brian sees a man looking ill-at-ease. The pub owner tells him the man has recently been hanging around, staring at the building across—the one where Kit lives.

Before Tony leaves for the meeting, the phone rings and he hears the voice. He calls Inspector Byrnes and asks him to come to the apartment, then tells Kit he will pretend to leave the building and secretly return, hopefully to catch her stalker.

As soon as Tony leaves, the caller phones to announce he is coming to kill Kit.  Tony returns and they turn off all the lights. Inside the apartment, the voice calls to Kit, telling her hes there to kill her. A man with a gun is seen at the terrace doors. Tony tackles him; as they struggle throughout the apartment, the gun goes off and hits the intruder.

Wondering why Scotland Yard is taking so long, Kit starts to place a call. Tony stops her and confesses that he never contacted the police—but he soon will, after shes been thrown from the terrace and killed "fighting off the intruder." He explains his plan to kill her and make her death appear a suicide she was driven to by mental illness; then he would collect her inheritance and repay the money he stole from his business. But the intruder has changed the plan: Tony now has to come down the stairs to kill the intruder after the intruder has killed Kit.

Just then Peggy enters the apartment and Kit asks for her help. But Tony explains that Peggy has actually been helping him—with the phone calls and pushing her at the bus stop. When the gunman regains consciousness, he turns out to be Peggys husband Roy, who had planned to murder Tony and Peggy after learning of their affair.

As Tony and Peggy deal with Roy, Kit works her way out on the terrace into the construction site gliding across girders. Brian and a policeman see her and shine a light on her, stopping Tony from following and throwing her off. When Brian reaches her he helps her to the elevator and down to the street, where Aunt Bea waits.

When Inspector Byrnes arrives, he reveals that he had tapped Kits phone and knew she was in trouble when Tony pretended to call the police. Brian and Aunt Bea comfort Kit while the inspector arrests her husband and his mistress.

==Cast==
*Doris Day as Kit Preston 
*Rex Harrison as Tony Preston 
*John Gavin as Brian Younger 
*Myrna Loy as Bea Vorman
*Roddy McDowall as Malcolm Stanley 
*Herbert Marshall as Charles Manning
*Natasha Parry as Peggy Thompson John Williams as Inspector Byrnes
*Hermione Baddeley as Dora Hammer
*Richard Ney as Daniel Graham
*Anthony Dawson as Roy Ash Rhys Williams as Victor Elliot
*Doris Lloyd as Nora Stanley

==Critical reception== Lolita that many customers may find themselves less in sympathy with her plight than with the villains murderous intentions."  Variety (magazine)|Variety said In a Ross Hunter effort the emphasis is on visual satisfaction. The idea seems to be to keep the screen attractively filled. First and foremost, it is mandatory to have a lovely and popular star of Doris Days calibre. She is to be decked out in an elegant wardrobe and surrounded by expensive sets and tasteful furnishings. This is to be embellished by highly dramatic lighting effects and striking hues, principally in the warmer yellow-brown range of the spectrum. The camera is to be maneuvered, whenever possible, into striking, unusual positions. The effervescent Day sets some sort of record here for frightened gasps. Harrison is capable. Director David Miller adds a few pleasant little humorous touches and generally makes the most of an uninspired yarn.

==Awards and nominations== Academy Award for Best Color Costume Design but lost to Arlington Valles for Spartacus (film)|Spartacus.

==Home Media==
* VHS pan/scan home video released in 1996, it is also currently available on DVD and Blu-ray.

==References==
 

==External links==
 
*  

 
 

 
 
 
 
 
 
 
 
 
 