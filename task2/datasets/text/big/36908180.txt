Freakin' Beautiful World
{{Infobox film
| name           = Freakin Beautiful World
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jarmo Lampela
| producer       = Mika Ritalahti
| writer         = Jarmo Lampela
| starring       = Joonas Bragge Arttu Kapulainen Pihla Penttinen Ilkka Koivula Kati Outinen
| music          = Petri Nieminen
| cinematography = Harri Räty
| editing        = Kimmo Taavila
| studio         = Lasihelmi Filmi
| distributor    = Finnkino
| released       =  
| runtime        = 96 minutes
| country        = Finland Finnish
| budget         = 
| gross          = 
}} Finnish drama film written and directed by Jarmo Lampela. The film is about two teenage boys from Helsinki who owe money to a local drug dealer. In order to pay their debt, they have to travel to Stockholm to collect drugs for the drug dealer. 

The film won Jussi Awards for Best Film, Best Cinematography, and Best Editing. 

== Cast ==
* Joonas Bragge as Ismo, "Ippe"
* Arttu Kapulainen as Pauli, "Papu"
* Pihla Penttinen as Mia
* Ilkka Koivula as Kalevi "Kalle" Lahtinen
* Kati Outinen as Tarja, Ippes mother
* Jyri Ojansivu as Sami, Papus brother
* Pekka Valkeejärvi as Esa, Papus father
* Sakari Korhonen as Sakke, Ippes father
* Irja Matikainen as Anita, Mias mother
 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 