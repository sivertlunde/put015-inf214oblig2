Cruising (film)
{{Infobox film
| name           = Cruising
| image          = Cruisingposter.jpg
| caption        = Original film poster
| director       = William Friedkin
| producer       = Jerry Weintraub
| writer         = William Friedkin Richard Cox Don Scardino
| music          = Jack Nitzsche
| cinematography = James A. Contner
| editing        = Bud S. Smith
| distributor    = Lorimar Productions / United Artists (theatrical release) Warner Bros. (DVD release)
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| gross          = $19,798,718
}} novel of leather scene.
 play on words with a dual meaning, as "cruising" can describe police officers on patrol and also cruising for sex. The film is also notable for its open-ended finale, further complicated by the directors incoherent changes in the rough cut and synopsis, as well as due to other production issues. 

==Plot==
In New York City during the middle of a hot summer, body parts of men are showing up in the Hudson River. The police suspect it to be the work of a serial killer who is picking up homosexual men at West Village bars like the Eagles Nest, the Ramrod, and the Cock Pit, then taking them to cheap rooming houses or motels, tying them up and stabbing them to death. 
 Meatpacking District in order to track down the killer. He rents an apartment in the area and befriends a neighbor, Ted Bailey (Don Scardino), a struggling young gay playwright. Burnss undercover work takes a toll on his relationship with his girlfriend Nancy (Karen Allen), due to both his refusal to tell her the details of his current assignment and Burns building a close friendship with Ted, who himself is having relationship issues with his jealous and overbearing dancer boyfriend Gregory (James Remar).

Burns mistakenly compels the police to interrogate a waiter, Skip Lee (Jay Acovone), who is intimidated and beaten to coerce a confession before police discover Skips fingerprints dont match the killers. Burns is disturbed by this police brutality, and comes to believe that the police are motivated by homophobia. Outraged, and exhausted by his undercover assignment, he almost quits his job. However, Burns is convinced by his boss (Paul Sorvino) to continue with the investigation. 
 Richard Cox), schizophrenic disorder Morningside Park. Burns brings the man into custody, but shortly afterward Teds mutilated body is found. The police dismiss the murder as a lovers quarrel turned violent and put out an arrest warrant for Gregory, with whom Burns earlier had a fight over his relationship with Ted. 

With the police under the impression that the murders have been solved, Burns moves back in with Nancy. In an ambiguous finale, Burns begins shaving his beard in the bathroom while Nancy secretly inspects clothes that he left on a chair: a leather peaked cap, aviator frames, and a leather jacket that all look very similar to the outfit the killer wore. Burns, meanwhile, wipes off his shaving cream and looks directly at the camera.

==Cast==
 
* Al Pacino – Steve Burns
* Paul Sorvino – Captain Edelson
* Karen Allen – Nancy Gates Richard Cox – Stuart Richards
* Don Scardino – Ted Bailey
* Joe Spinell – Patrolman DiSimone
* Jay Acovone – Skip Lee
* Randy Jurgensen – Det. Lefransky
* Barton Heyman – Dr. Rifkin Gene Davis – DaVinci
* Arnaldo Santana – Loren Lukas
* Larry Atlas – Eric Rossman
* Allan Miller – Chief of Detectives
* Sonny Grosso – Detective Blasio Edward ONeil – Detective Schreiber
* Michael Aronin – Detective Davis
* James Remar – Gregory
* William Russ – Paul Gaines Mike Starr – Patrolman Desher
* Steve Inwood – Martino
* Keith Prentice – Joey
* Leland Starnes – Jack Richards
 

==Production== The French Arthur Bell. The Exorcist, who had confessed to some of those murders. All of these factors gave Friedkin the angle he wanted to pursue in making the film.  Jurgenson and Bateson served as film consultants, as did Sonny Grosso, who had earlier consulted with Friedkin on The French Connection. Jurgenson and Grosso appear in bit parts in the film.

In his research, Friedkin worked with members of the Mafia, who at the time owned many of the citys gay bars.    Al Pacino was not Friedkins first choice for the lead; Richard Gere had expressed a strong interest in the part, and Friedkin had opened negotiations with Geres agent. Gere was Friedkins choice because he believed that Gere would bring an androgynous quality to the role that Pacino could not. 
 R rating.    The deleted footage, according to Friedkin, consisted entirely of footage from the clubs in which portions of the film were shot and consisted of " bsolutely graphic sexuality....that material showed the most graphic homosexuality with Pacino watching, and with the intimation that he may have been participating."  In some discussions, Friedkin claims that the missing 40 minutes had no effect on the story or the characterizations,  but in others he states that the footage created "mysterious twists and turns (which   no longer takes)", that the suspicion that Pacinos character may have himself become a killer was made more clear and that the missing footage simultaneously made the film both more and less ambiguous. When Friedkin sought to restore the missing footage for the films DVD release, he discovered that United Artists no longer had it. He believes that UA destroyed the footage.  Some obscured sexual activity remains visible in the film as released, and Friedkin intercut a few frames of gay pornography into the first scene in which a murder is depicted.
 Rough Trade. 

Friedkin asked noted gay author John Rechy, some of whose works were set in the same milieu as the film, to screen Cruising just before its release. Rechy had written an essay defending Friedkins right to make the film, although not defending the film itself. At Rechys suggestion, Friedkin deleted a scene showing the Gay Liberation slogan "We Are Everywhere" as graffiti on a wall just before the first body part is pulled from the river, and added a disclaimer: 

"This film is not intended as an indictment of the homosexual world. It is set in one small segment of that world, which is not meant to be representative of the whole." 

Friedkin later claimed that it was the MPAA and United Artists that required the disclaimer, calling it "part of the dark bargain that was made to get the film released at all" and "a sop to organized gay rights groups".  Friedkin claimed that no one involved in making the film thought it would be considered as representative of the entire gay community, but gay film historian Vito Russo disputes that, citing the disclaimer as "an admission of guilt. What director would make such a statement if he truly believed that his film would not be taken to be representative of the whole?"   

===Protests=== East Village demanding the city withdraw support for the film. 

Al Pacino said that he understood the protests but insisted that upon reading the screenplay he never at any point felt that the film was Heterosexism|anti-gay. He said that the leather bars were "just a fragment of the gay community, the same way the Mafia is a fragment of Italian-American life", referring to The Godfather, and that he would "never want to do anything to harm the gay community".   

==Release==
Cruising was released February 15, 1980 in the United States and had a weak domestic box office take of United States dollar|$19,784,223. 

==Reception==
===Critical reception=== gay activists had public protests against the film. However, critical opinion of it has warmed somewhat over the years as the film has been reassessed. As of June 2014, the film holds a 51% "rotten" rating at Rotten Tomatoes based on 41 reviews.  Upon its original release, Roger Ebert gave Cruising two and a half out of four stars, describing it as well-filmed and suspenseful yet it "seems to make a conscious decision not to declare itself on its central subject." The "central subject" being the true feelings of Pacinos character about the S&M subculture, which are never explored to Eberts satisfaction.    

Critic Jack Sommersbys comments typified the contemporary criticism directed at non-political matters such as character development and the changes made when the film was transferred from a novel to a film: 
*  "The closest we get to a motivation comes from his imaginary conversations with his deceased, formerly-disapproving father, who tells his boy, You know what you have to do, which sets him off to kill, and, again, were baffled as to the connection Friedkins trying to make. Was the fathers disapproval pertaining to his son being gay, and is the son trying to win back his fathers approval by killing men of a sexual nature the father has a seething hatred for? If so, theres no indication of any of this. In fact, we dont even know if the father knew his son was gay before passing on."
*  "Gone is the back story of his having harassed gays at an off-base bar when he was in the Army; also gone is his racism, along with his seemingly asexual nature in the first half. Instead, hes been made a regular, happy-go-lucky guy with a steady girlfriend. One can easily surmise Friedkins motivation here: using someone identifiable to lead us into the underworld of black leather and kinky sex...  ere brought up short, and the cops emotional progression seems stunted, as if Friedkin simply didnt care. We see the cop engaging in some heavy vaginal intercourse with his girlfriend, but we dont know if hes normally this semi-rough, if hes doing so under the pretense that the rougher, the manlier he must be – fucking away any trace of gay, if you will. A week later, the girlfriend complains about his not wanting her any more, and he replies, What Im doing is affecting me. How? Turning him off sex with women, or off sex altogether in light of what hes seeing and experiencing every night? Again, we do not know."
 psychotic and begin to kill."  Ebert argued that "The validity of these arguments   is questionable."  Similarly, in Exorcising Cruising, a behind-the-scenes documentary on the Cruising DVD, Friedkin notes that the film was enthusiastically supported by much of New York Citys leather/S&M community, who appeared by the dozens as extras in the nightclub scenes. 

Raymond Murray, editor of Images in The Dark (an encyclopedia of gay and lesbian films) writes that "the film proves to be an entertaining and (for those born too late to enjoy the sexual excesses of pre-AIDS gay life) fascinating if ridiculous glimpse into gay life - albeit Hollywoods version of gay life." He goes on to say "the film is now part of queer history and a testament to how a frightened Hollywood treated a disenfranchised minority." 
 The Story of O or other European high porn of the 1960s." She also praised the soundtrack, and added, "The gay opposition to Cruising prefigured the dismayingly Stalinist gay and feminist picketing of Basic Instinct."

===DVD release===
A deluxe collectors edition DVD, distributed by Warner Home Video, was released in Region 1 on September 18, 2007 and Region 2 on 25 Feb 2008. This release is not in its original directors cut, but does include some extra scenes not seen in the original VHS release and additional visual effects added by Friedkin. Friedkin also added a commentary track to accompany the DVD. The only visible omission in this re-release, as compared to the theatrical and VHS releases, is the absence of the disclaimer at the beginning of the film stating that Cruising depicts a gay S&M subculture and is not representative of mainstream gay life. The DVD also includes two featurettes entitled The History of Cruising and Exorcising Cruising, the latter being about the controversy the film provoked.

===Interior. Leather Bar.===
In 2013, filmmakers  , January 19, 2013.  The film is not actually a recreation of the footage, however; instead, it uses a docufiction format to explore the creative and ethical issues arising from the process of trying to film such a project. 

==Awards and nominations==
* 1st Golden Raspberry Awards Worst Picture (lost to Cant Stop the Music) Worst Director – William Friedkin (lost to Robert Greenwald for Xanadu (film)|Xanadu) Worst Screenplay (lost to Cant Stop the Music)

== See also ==
 

==References==
;Notes
 

;Bibliography
*  
*  
*  
*  
*  

;Further reading
* Nystrom, Derek (2009). Hard Hats, Rednecks, and Macho Men: Class in 1970s American Cinema. Oxford Univ. Press. ISBN 9780195336764.
* Savran, David (1998). Taking It Like a Man: White Masculinity, Masochism, and Contemporary American Culture. Princeton University Press. ISBN 0-691-05876-8, pp. 213–217

==External links==
*  
*   by Bob Stephens (1995).
*  .
*   by Drew Fitzpatrick (Digital Destruction).
*   by Trenton Straube
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 