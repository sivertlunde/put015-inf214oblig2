The Double Man (1976 film)
{{Infobox film
| name           = The Double Man
| image          = 
| caption        = 
| director       = Franz Ernst
| producer       = Steen Herdel Ib Tardini Vibeke Windeløv
| writer         = Franz Ernst Kirsten Thorup
| starring       = Erik Wedersøe
| music          = 
| cinematography = Dirk Brüel
| editing        = Christian Hartkopp
| distributor    = 
| released       = 19 April 1976
| runtime        = 91 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

The Double Man ( ) is a 1976 Danish crime film directed by Franz Ernst and starring Erik Wedersøe.

==Cast==
* Erik Wedersøe - Christian Mortensen
* Peter Belli - Mikael Mortensen
* Poul Reichhardt - Mortensen
* Chili Turèll - Janne (as Inge Margrethe Svendsen)
* Lane Lind - Eva
* Lotte Hermann - Lily
* Claus Nissen - Verner
* Morten Grunwald - Hugo
* Martin Miehe-Renard - Kurt
* Frederik Frederiksen - Direktør Holm
* Susanne Heinrich - Gurli
* Preben Harris - Gorilla
* Michael Bastian - Gorilla
* Willy Rathnov - Bossen
* Masja Dessau - Luder

==External links==
* 

 
 
 
 
 
 
 


 
 