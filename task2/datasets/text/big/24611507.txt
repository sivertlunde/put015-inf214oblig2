I'll Take Sweden
{{Infobox film
| name           = Ill Take Sweden
| image          = Ill Take Sweden.jpg
| image_size     =
| alt            =
| caption        = Movie Poster
| director       = Frederick de Cordova
| producer       = Edward Small
| writer         = Nat Perrin Bob Fisher Arthur Marx
| narrator       =
| starring       = Bob Hope Tuesday Weld Frankie Avalon
| music          = William "By" Dunham Jimmie Haskell
| cinematography = Daniel L. Fapp
| editing        =
| studio         = Edward Small Productions
| distributor    = United Artists
| released       = June 18, 1965 (USA)
| runtime        =  97 min.
| country        = United States English
| budget         =
| gross          = $1,500,000 
| preceded_by    =
| followed_by    =
}}

Ill Take Sweden is a 1965 comedy film directed by Frederick de Cordova, and starring Bob Hope, Frankie Avalon, and Tuesday Weld.

==Plot==

Single father Bob Holcomb (Hope), a widower, is unhappy with the guitar-playing boy (Avalon) his daughter JoJo (Weld) chooses as a husband-to-be. An executive with an oil company, Bob accepts a transfer to the firms Stockholm branch and he takes JoJo along, hoping it will distract her.
 liberal sexually than the United States. Bob, having met an attractive interior designer, Karin (Dina Merrill), decides to take her away for a romantic weekend at a mountain resort.

JoJo, however, has accepted a similar offer from Erik (Jeremy Slate), who is Bobs new assistant. Originally seen as a respectable suitor, Erik turns out to be a playboy and a cad. A girl thought to be his cousin, Marti, is actually a former girlfriend.

Kenny turns up and brings Marti along to the resort, where the three couples continue to awkwardly encounter one another. Kenny finally has his fill of Erik, knocking him out with his guitar. On a voyage home, the ships captain performs a double wedding ceremony.

==Principal cast==
{| class="wikitable" border="1"
|-
! Actor
! Role
|-
| Bob Hope || Bob Holcomb
|-
| Tuesday Weld || JoJo Holcomb
|-
| Frankie Avalon|| Kenny Klinger
|-
| Dina Merrill || Karin Granstedt
|-
| Jeremy Slate || Erik Carlson
|-
| Rosemarie Frankland|| Marti
|-
| John Qualen || Olaf
|}

==Production notes==
The parts of the movie that were supposed to be in Sweden were shot at Big Bear Lake and Lake Arrowhead, California.  

The casting of Tuesday Weld and Frankie Avalon was seen as Bob Hope getting some box office insurance to attract younger audiences. BOX-OFFICE INSURANCE: Hope Takes Sweden, Teen-agers
Scott, John L. Los Angeles Times (1923-Current File)   02 July 1965: D11. 

The movie was advertised as being Hopes 50th but even he disputed that. Hopes Heard the One About the...
Alpert, Don. Los Angeles Times (1923-Current File)   20 June 1965: b7 

Director Frederick De Cordova saw Luci Baines Johnson, daughter of President Lyndon B. Johnson, dance the Watusi at a White House barbecue. He offered her a role in the film but she declined on the grounds she had to go to school. Luci Offered Film Role
By Winzola McLendon. The Washington Post, Times Herald (1959-1973)   15 Aug 1964: C11.  Billie Dove visited the set and Bob Hope offered her a role too but the former star declined. Former Silent Film Beauty Visits Hope Set
Los Angeles Times (1923-Current File)   03 Oct 1964: B3 

==Critical reception== Howard Thompson of The New York Times loathed the film:
 
Other reviews were mixed. Van Dyke Amusing in Faltering Comedy: Road to Sweden Leads Hope Astray
By Philip Kopper Washington Post Staff Writer. The Washington Post, Times Herald (1959-1973)   01 July 1965: D25 

Hope was so impressed with Avalons work, he signed Avalon to appear on his television show. Skelton Hailed as Pied Piper of Fun: London Paper Asks Why His Show Hasnt Played Britain
Hopper, Hedda. Los Angeles Times (1923-Current File)   10 Sep 1964: C12. 

==References==
 

==External links==
*  
*  
 

 
 

 
 
 
 
 
 
 