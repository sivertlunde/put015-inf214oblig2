Fanny (1932 film)
{{Infobox film
| name           = Fanny
| image          = Fanny-1932-poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Marc Allégret
| producer       = Pierre Braunberger Roger Richebé
| writer         = Marcel Pagnol
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Raimu Orane Demazis Pierre Fresnay
| music          = Vincent Scotto Georges Sellers
| cinematography = Nicolas Toporkoff Roger Hubert George Benedict André Dantan
| editing        = Jean Mamy
| studio         = 
| distributor    = Mediterranean Film Company
| released       =   
| runtime        = 104 minutes
| country        = France
| language       = 
| budget         = 
| gross          = 
}} Marius (1931) and concluded with César (film)|César (1936). Like "Marius" the film was a box office success in France and today is still considered to be a classic of French cinema.

==Plot==

The story takes place in Marseille, where Marius, the son of a barkeeper César, has a romance with Fanny, neighbourhood girl of the fish sales man in the harbor. Marius dreams of sailing away one day and travel the seven seas. Fanny soon discovers she is pregnant of Marius, a shameful position in their community since shes a single mother with a father unable to secure the future of her and her child. She agrees with her mother and fathers advice to marry a more prosperous salesman in the harbor, Honoré Panisse, who is 30 years older than she is. A few months after the marriage and the birth of the baby Marius returns and tries to win back Fanny...

==Cast==
* Fanny Cabanis  (Orane Demazis)
* César Olivier  (Raimu)
* Honorine Cabanis  (Alida Rouffe)
* Honoré Panisse  (Charpin)
* Albert Brun (Robert Vattier)
* Félix Escartefique (Auguste Mouries)
* Claudine Foulon (Milly Mathis)
* Mangiapan  (Marcel Maupi)
* Félicien Venelle  (Edouard Delmont)
* Elzéar Bonnegrâce (Louis Boulle)
* Fortunette (Odette Roger)
* Amélie  (Annie Toinon)
* André Gide and Pierre Prévert have cameos

==In popular culture==
* The famed restaurateur and founder of California cuisine, Alice Waters, was so taken by this film that she named her Berkeley restaurant "Chez Panisse". The café upstairs from the restaurant is decorated with posters from the films "Marius", "Fanny", and "César".  (http://akas.imdb.com/title/tt0022877/trivia)
* In 1984 Waters opened a small breakfast café in Berkeley. Café Fanny is named after the heroine of Marcel Pagnols 1930s Marseilles movies (as is Alice Waters daughter)  - a love story involving the whole community, centered around a little standup café. Waters wanted to evoke their spirit: an ideal reality where life and work were inseparable and the daily pace left time for the afternoon anisette or the restorative game of petanque, where eating together nourished the spirit as well as the body-since the food was raised, harvested, hunted, fished and gathered by people sustaining and sustained by each other and by the earth itself. The café closed in March 2012. (http://cafefanny.com/history.html)
* The main characters from the films Marius, Fanny and César make a cameo appearance in the Asterix comic book Asterix and the Banquet. The pétanque playing scene in the comic book is a reference to a similar scene in this film. (http://www.mage.fst.uha.fr/asterix/allusion/pagnol.html) (http://www.asterix.com/encyclopedia/characters/cesar-drinklikafix.html)

==See also==
* Fanny (1961 film), a remake of the 1932 version.

==Notes==
 
 
 
 
 
 
 
 
 
 