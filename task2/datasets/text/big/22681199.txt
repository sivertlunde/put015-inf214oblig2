More than Blue
{{Infobox film
| name           = More Than Blue
| image          = More Than Blue film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | rr             = Seulpeumboda deo seulpeun iyagi
 | mr             = Sŭlp‘ŭmpoda tŏ sŭlp‘ŭn iyagi}}
| director       = Won Tae-yeon
| producer       = Kim Jho Kwang-soo
| writer         = Won Tae-yeon
| starring       = Kwon Sang-woo Lee Bo-young Lee Beom-soo
| music          = 
| cinematography = 
| editing        = Moon In-dae
| studio         = Mnet Media
| distributor    = Showbox
| released       =  
| runtime        = 105 minutes
| country        = South Korea
| language       = Korean
| budget         = United States dollar|US$2 million (approx.)
| gross          = $3,541,108 
}} 2009 South Korean film. The directorial debut of poet Won Tae-yeon, it stars Kwon Sang-woo, Lee Bo-young and Lee Beom-soo in the lead roles. The films Korean title translates as "A Story Sadder Than Sadness". Lee Hyo-won. " ". The Korea Times, 5 March 2009. Retrieved on 4 May 2009. 

== Plot ==
K and Cream first meet each other in high school & both are orphans; K was abandoned by his mother after his father died of cancer who nevertheless left him a sizable sum of money, while Cream lost her entire family in a traffic accident. The two become soulmates and come to share a home, though K watches Cream switch from boyfriend to boyfriend as he keeps his own feelings for her to himself. Knowing that Creams biggest fear is to be left alone, K keeps the fact that he has terminal cancer a secret, and instead he urges her to marry a kind and healthy man. When Cream announces that she is in love with affluent doctor Joo-hwan, K is left heartbroken, but is satisfied that she has met her ideal partner.

Believing Cream has really found someone she loves, K asked Joo-hwans fiance to break up with him.  Joo-hwans fiance agrees under the condition that K lets her take photographs of him. It is later revealed that Cream first learned about Ks illness when she takes what she thought were Ks vitamins, but in fact turned out to be pain medication for terminal cancer patients. Earlier in the movie, Cream asked K what his wish was: for Cream to find a good and healthy man to spend her life with. With her knowledge of his illness, Cream thus faked falling in love with Joo-hwan, in order to appease K. This perspective of Creams is revealed toward the end of the movie. 

The night before Joo-hwan and Creams wedding, K found the courage to tell Cream that he loves her while Cream replies "me, too".  In the end, Cream believes that they are married because they walked down the aisle together when K was sending Cream off to Joo-hwan at their wedding. The movie closes with a scene of Joo-hwan at a burial site where he leaves a photograph of K and Cream as well as Creams recorder, which contains a recording Cream made for K telling him to wait for her on the other side so that they can be together with "no more tears". The grave is revealed to be Creams, thus implying that she committed suicide.

== Cast ==
* Kwon Sang-woo ... K
* Lee Bo-young ... Cream
* Lee Beom-soo ... Cha Joo-hwan
* Jeong Ae-yeon ... Jenna
* Lee Han-wi ... President Kim
* Sin Hyeon-tak ... Min-cheol
* Kim Jeong-seok ... Organization chief
* Seon Ho-jin ... Sa-pa-mi DJ
* Park Yeong-jin ... Handsome guy at guardhouse
* Ji Dae-han ... Singer As engineer
* Jung Joon-ho ... President Im
* Lee Seung-cheol ... Singer A
* Nam Gyu-ri ... Catgirl
* Kim Heung-gook ... Traffic broadcast DJ

== Production ==
More Than Blue marked the directorial debut of poet  2 million,  with all three of the main cast members investing a portion of their salaries into the production. Lead actor Kwon Sang-woo drew inspiration from his own then-recent marriage, saying, "I think being married enables me to think more deeply about playing melodramatic parts... Kay braves his circumstances for love, and I thought I might have done the same if I were in his shoes". 

== Release == topped the domestic box office on its opening weekend with 256,809 admissions.  As of 19 April the film had accumulated a total of 724,206 admissions,  and as of 12 April had grossed a total of United States dollar|US$3,577,302. 

== Critical response ==
Lee Hyo-won of The Korea Times commented that despite its familiar premise, More Than Blue "feels more classic than cliched and the undying fidelity of the love-struck characters nostalgically evokes old romances. The talented actors also bring freshness to their parts, making them very believable and worthy of every ounce of ones empathy." Kwon Sang-woo in particular was singled out for his performance, with Lee commenting, "Kwon wins the audiences heart by bringing a certain tenderness to his character, suggesting a maturation in his acting, a palpable break away from his previous roles as a romantic tough guy." 

== References ==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 