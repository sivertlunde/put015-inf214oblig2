The March (film)
 
{{Infobox film
| name           = The March
| image          =
| image_size     =
| caption        =
| director       =  
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = United States Information Agency|U.S. Information Agency
| released       = 1964 (non-US) 1990 (US)
| runtime        = 33 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 civil rights March on Washington.  It was made for the Motion Picture Service unit of the United States Information Agency for use outside the United States &ndash; the 1948 Smith-Mundt Act prevented USIA films from being shown domestically without a special act of Congress.  In 1990 Congress authorized these films to be shown in the U.S. twelve years after their initial release.

In 2008, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==See also==
* African-American Civil Rights Movement in popular culture

==Notes==
 

==External links==
*  ,  , and   on the US National Archives YouTube channel
*  

 

 

 

 
 
 
 
 
 
 
 
 
 


 
 