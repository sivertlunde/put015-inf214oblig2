The Charm School (film)
{{Infobox film
| name           = The Charm School
| image          = The Charm School - 1921.jpg
| imagesize      =
| caption        = 1921 lobby poster
| director       = James Cruze
| producer       = Adolph Zukor Jesse Lasky
| writer         = Thomas J. Geraghty (scenario)
| based on       =  
| starring       = Wallace Reid Lila Lee
| music          =
| cinematography = Charles Schoenbaum|C. Edgar Schoenbaum
| editing        =
| distributor   = Paramount Pictures
| released       =  
| runtime        = 5 reel#Motion picture terminology|reels; (4,743 feet)
| country        = United States Silent (English intertitles)

}} silent comedy film starring Wallace Reid. Produced by Famous Players-Lasky and distributed through Paramount Pictures this James Cruze directed film was based on a 1920 Broadway stage play and novel by Alice Duer Miller that starred veteran actress Minnie Dupree. It is currently a lost film.   It was filmed on the campus of Pomona College in Claremont, California.

==Plot== vamp him, but when Austin does not fall she tells him directly that she loves him. Elsies uncle is very interested in the young Austin. When Mrs. Rolles hears of how well he is getting along, she tries to patch things up between Austin and her daughter Susie, and tells Elsie that the two are engaged. While Elsie is brokenhearted, in the end all turns out well for she and Austin.

==Cast==
*Wallace Reid - Austin Bevans
*Lila Lee - Elsie
*Adele Farrington - Mrs. Rolles
*Beulah Bains - Susie Rolles
*Edwin Stevens - Homer Johns
*Grace Morse - Miss Hayes
*Patricia Magee - Sally Boyd
*Lincoln Stedman - George Boyd
*Kate Toncray - Miss Curtis
*Minna Redman - Miss Tevis
*Snitz Edwards - Mr. Boyd
*Helen Pillsbury - Mrs. Boyd
*Tina Marshall - Europia

==Remake==
The story was remade in 1936 as the film Collegiate.

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 


 