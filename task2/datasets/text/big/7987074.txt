Spooks (film)
 

{{Infobox film|
  | name           = Spooks!
  | image          = Stooge spooks53.jpg
  | director       = Jules White Felix Adler Tom Kennedy Frank Mitchell
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 15 44"
  | country        = United States
  | language       = English
}}

Spooks! is the 148th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Tom Kennedy). There is also a gorilla kept imprisoned in the house for experimental purposes. The Stooges arrive to rescue the kidnapped girl disguised as door-to door pie salesmen.

==Production notes==
Spooks! was the first of two shorts (the other being Pardon My Backfire) made by Columbia with the Stooges in Stereoscopy|3D, after the 3-D craze of 1953 began with Bwana Devil. It originally premiered on May 20, 1953 with the Columbia western Fort Ti (also in 3D). Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion; Comedy III Productions, Inc., ISBN 0-9711868-0-4 
 sepia toned in order to allow for more light to pass through the Polaroid filters necessary for the dual-strip 3D projection method of that time. The process did not work as expected and the idea was dropped after these two productions. 
 
 Academy aspect ratio and released in wide-screen during the confusion , Spooks! and future releases were composed at 1.85:1, Columbias house ratio.

==In popular culture== TBS 1995 Halloween special The Three Stooges Fright Night.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 