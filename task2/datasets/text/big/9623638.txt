El otro
 
{{Infobox film
| name           = El otro
| image          = ElOtro.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Ariel Rotter Enrique Piñeyro Christian Baute
| screenplay     = Ariel Rotter
| narrator       = 
| starring       = Julio Chávez Inés Molina María Ucedo Arturo Goetz
| music          = 
| cinematography = Marcelo Lavintman
| editing        = Eliane Katz
| studio         = Aqua Films
| distributor    = Distribution Company
| released       =  
| runtime        = 83 minutes
| country        = Argentina France Germany Spanish
| budget         = 
}} German drama film, written and directed by Ariel Rotter, his second feature. 
 Enrique Piñeyro and Christian Baute.

El otro was funded by the Instituto Nacional de Cine y Artes Audiovisuales (Argentina), the Vision Sudest Fund (Switzerland), the World Cinema Fund (Berlin International Film Festival), and the Hubert Bals Fund (Netherlands). 

The picture features Julio Chávez as Juan Desouza, who was awarded the Silver Bear as Best Actor at the 2007 Berlin International Film Festival.

==Plot==
The film tells of Juan Desouza (Julio Chávez), a lawyer in his late 40s, whos happily married and his wife is expecting a child.

On a one-day business trip to the country-side, Desouza embarks on an unintended journey.  When he reaches his destination Desouza discovers that the man traveling next to him is not sleeping but dead.

Secretly, he assumes the dead mans identity and invents a profession for himself.  He finds a place to stay in the village where the man used to live and contemplates not returning.

Juan Desouza undertakes an adventure into nature, into the rediscovery of his tastes and his basic instincts.  He tries to grasp the idea that the life dealt out for him, and which he chose to live, is not the only one possible.

He eventually goes back home, stronger from the spiritual experience.

==Cast==
* Julio Chávez as Juan Desouza
* María Onetto
* María Ucedo
* Inés Molina
* Arturo Goetz
* Osvaldo Bonet
* Raminta Kavaliūnaitė

==Distribution==
The film was first presented at the Berlin International Film Festival on February 13, 2007.

==Reception==

===Critical response===
The film was well received at the Berlin International Film Festival winning a Jury Grand Prix Silver Bear.

Film critic Annika Pham, who writes for CineEuropa, liked the film, and wrote, "The film subtly explores the apprehension of death as well as the palpable yet invisible world that lurks underneath the surface. Few words are needed. We feel with Juan and easily relate to him. The scenes where he cares for his father, becoming his father’s father, are particularly moving." 

===Awards===
Wins
* Berlin International Film Festival: Silver Bear, The Jury Grand Prix; Silver Bear, Best Actor, Julio Chávez; 2007.

==References==
 

==External links==
*  
*  
*   at cinenacional.com  
*  

 
 
 
 
 
 
 
 
 
 