Land of Silence and Darkness
 
{{Infobox film name            = Land of Silence and Darkness image           = Silence_and_Darkness_Cover.jpg caption         = director        = Werner Herzog producer        = Werner Herzog writer          = Werner Herzog starring        = Fini Straubinger music           = J.S. Bach, Vivaldi narrator     =   cinematography  = Jörg Schmidt-Reitwein editing         = Beate Mainka-Jellinghaus studio          = Referat für Filmgeschichte Werner Herzog Filmproduktion distributor     = Werner Herzog Filmproduktion released        =   runtime         = 85 minutes country         = West Germany language        = German language|German, German Sign Language budget          =
|}}
 German director Werner Herzog. Produced by Werner Herzog Filmproduktion.

==Summary==
 
In telling the story of Fini Straubinger a deaf-blind German woman, Herzog investigates the nature of human thought and communication. Herzog follows Fini Straubinger to numerous events as she visits with other people in the deaf-blind community, discussing their struggle to live in the modern world with their disabilities. He reveals their communication with each other through a sort of sign language of strokes and taps on the other persons palm. Three important scenes in the film, for example, involve: a home for people who, unlike Ms. Straubinger and her friends, were born deaf-blind; an airplane ride; and a man exploring a tree with his hands. 

At the home, a charitable group helps boys who were born deaf-blind, and have therefore experienced the world only through taste, smell, and touch. Herzog provokes the viewer to ponder what the world would be like, what, indeed, thought would be like for someone who had no concept of speech or sight. He shows how an act as simple as showering can be an alien and horrifying thing for someone who has no idea what a shower is and no way for it to be explained to him. This scene, perhaps, evokes best the profound sense of loneliness and isolation present in the film.

A more optimistic scene occurs when Ms. Straubinger and her friends take a flight in a plane. Many of them are experiencing flight for the first time, and, unable to see or hear; the viewer is led to ponder the wonders present even in the mundane. 

Herzog, himself, has stated that the whole film is a preparation for the final image. In this final shot, one of the deaf-blind approaches a tree and embraces it in a gesture which expresses a “fanatic materialism: it would be difficult to imagine a more stripped-down, economical illustration of what Heidegger calls Dasein, existential being in a ‘senseless’ world” (Hoberman, J.). 

==References==
*   (Chicago: Facets Multimedia Center, 1979) 22.
* J. Hoberman, Alien Landscapes, V. 26 (New York: “Village Voice”, 23/29 December 1981) 66.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 
 