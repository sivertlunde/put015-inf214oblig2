The Spider and the Fly (1931 film)
{{Infobox Hollywood cartoon
| cartoon name      = The Spider and the Fly
| series            = Silly Symphonies
| image             =
| image size        = 
| alt               = 
| caption           = 
| director          = Wilfred Jackson
| producer          = Walt Disney
| story artist      = 
| narrator          = 
| voice actor       =   
| musician          =  Charles Byrne Harry Reeves
| layout artist     = 
| background artist = 
| studio            = Walt Disney Productions
| distributor       = Columbia Pictures
| release date      = October 16, 1931
| color process     = Black and White
| runtime           = 7 min
| country           = United States
| language          = English
| preceded by       = The Clock Store The Fox Hunt
}}

The Spider and the Fly is a 1931 Silly Symphonies cartoon.

==Summary==
A kitchen is filled with houseflies. A spider wakes up and plays his web like a harp, attracting a pair of them; the female is trapped, and the male summons the cavalry, which arrives riding horseflies, riding dragonflies to drop pepper bombs, firing champagne bottles, and ultimately setting the web on fire and catching the spider on flypaper when he falls.

== External links ==
* 

 

 
 
 
 
 
 
 
 


 