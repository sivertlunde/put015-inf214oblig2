Boomerang (1992 film)
{{Infobox film 
| name           = Boomerang
| image          = Boomerang-Posters.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Reginald Hudlin
| producer       = Brian Grazer Warrington Hudlin
| screenplay     = Barry W. Blaustein David Sheffield
| story          = Eddie Murphy Tisha Campbell Lela Rochon John Canada Terrell
| music          = Marcus Miller
| cinematography = Woody Omens
| editing        = Earl Watson John Carter Michael Jablow Imagine Films Entertainment Eddie Murphy Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = $42 million 
| gross          = $131,052,444
}} womanizer and male Chauvinism|chauvinist. When he meets his new boss, Jacqueline Broyer (Robin Givens), Marcus discovers that she is essentially a female version of himself, and realizes he is receiving the same treatment that he delivers to others. The film features supporting performances by Halle Berry, David Alan Grier, and Martin Lawrence.
 House Party (1990).  Hudlin and the writers aimed to create a romantic comedy that differed strongly from Murphys previous comic efforts. Filming took place mainly in New York City, while other scenes were filmed in Washington, D.C.
 BMI Film soundtrack became a top-selling album.

==Plot==
The film begins as Marcus (Eddie Murphy) enters Chantress, the company that he works for as an advertisement executive.  It is quickly established that he is quite the ladies man, as he tells his assistant to send flowers to nine different women, with cards reading "only thinking of you."  When he enters his office, Nelson (Geoffrey Holder), one of the creative minds also in Marcus department, shows Marcus a re-cut hes done of one of the ads theyve been working on.  Marcus tells Nelson, whos very creative but also very risqué and overtly sexual with his style, that he must re-edit the ad, as some of the material may be offensive to women and may also cause problems with the U.S. Federal Communications Commission.  Nelson reluctantly agrees, then leaves.

The next day, Marcus meets Lady Eloise (Eartha Kitt), the head of the company which will soon be acquiring Chantress in a business merger.  She suggests that Marcus may be promoted to head of the marketing department, and invites him to her home for dinner that evening.  Marcus realizes she intends to have her way with him, and thinking of the promotion, goes through with it and spends the night with her.  The next day at work, he meets Jacqueline Broyer (Robin Givens), a beautiful woman who tells Marcus that Lady Eloise has had no position in the company for a number of years, but uses her image to her advantage. Marcus realizes that Jacqueline is going to be given the job he wanted, and will soon become his boss.  At the party being held for the companies merger, Marcus tells his friends Tyler (Martin Lawrence) and Gerard (David Alan Grier) hes thinking about resigning since hes not going to be promoted.  While there, Jacqueline introduces him to Angela Lewis (Halle Berry), who works in the art department, and tells him the two of them should be working together.  Also, everyone in the party is introduced to Strangé (Grace Jones), the wild fashion diva whos been chosen as the new face of Lady Eloise Cosmetics.  While looking for Jacqueline, Marcus bumps into Angela, and when he spots Jacqueline, introduces Angela to Gerard so that he can go and catch up with her.  When he attempts to woo Jacqueline, she tells him she doesnt date co-workers.  Marcus however still believes he will be made the exception because of her flirtatious nature towards him.

Some time later at the office, Jacqueline tells Marcus they should go over some of their work together, and Marcus suggests they do it at his place over dinner, to which Jacqueline agrees.  Marcus also finds out that Angela and Gerard will be going out that night.  Both couples meet that evening, and ironically, Gerard, whos not as successful with women as Marcus, ends up having the better evening with Angela, while Jacqueline and Marcus eat dinner in front of the TV, which doesnt leave much time for them to establish any sort of chemistry.  Later, the two are sent on a business trip in New Orleans, and they eventually have sex while there.

Marcus, who assumes that he and Jacqueline are now a couple, is rather shocked to find that Jacqueline feels they should take it slowly, and isnt as enthusiastic about their relationship as he is.  While he feels hes falling in love, he doesnt understand how she can be so careless and inconsiderate about his feelings.  After Marcus finds out that Jacqueline has told some of their personal business to Strangé, he tells her that the two of them should take a break for a while.  Jacqueline gets the last word however, when she bluntly tells Marcus "its over."  Distraught about their breakup, Marcus work ethic begins to lag, and after he ruins a major business proposal (by giving Nelson complete "creative" control of the advertising project), Jacqueline decides rather than fire him, shell give him a few weeks off work to get himself together.

During this time, Marcus begins to hang out with Angela, who tries to help bring him out of the funk hes in.  On Thanksgiving, Marcus, Angela, Tyler, Gerard and his parents get together for dinner.  While eating, Angela and Gerard tell his parents that they arent a couple, when his father assumes that the two are.  When everyone else leaves, Marcus and Angela clean up, and fall asleep on the couch together.  When they wake up, the two begin to make out, and assumedly sleep together.  While out with Gerard and Tyler, Marcus tells Gerard that he and Angela are going out together, which upsets Gerard, knowing Marcus past with women.

Marcus, now much more relaxed and confident, becomes very attractive to Jacqueline again, and as he hadnt put away his feelings for her, the two end up sleeping together again one night.  The next day, Angela confronts Marcus about his whereabouts the night before, knowing he was with Jacqueline.  Upset by how Marcus makes himself out to be the victim, Angela angrily leaves.  Marcus tries to go back to Jacqueline, then realizes his feelings for her arent the same as his love for Angela and he now sees Jacqueline for the man eating schemer she truly is.

Later, Marcus talks to Gerard, and Gerard tells him that Angela got a promotion at a new company shes working for.  Marcus apologizes to Gerard, and the two make up for the argument they had before.  He then goes to Angelas new job, and though at first she is very cold towards him, she finally forgives him, and the two get back together.

==Cast==
* Eddie Murphy as Marcus Graham
* Robin Givens as Jacqueline Broyer
* Halle Berry as Angela Lewis
* David Alan Grier as Gerard Jackson
* Grace Jones as Helen Strangé
* Martin Lawrence as Tyler Hawkins
* Geoffrey Holder as Nelson
* Eartha Kitt as Lady Eloise
* Chris Rock as Bony T Tisha Campbell as Yvonne
* Lela Rochon as Christie

==Production==

===Development=== House Party. For Boomerang, Hudlin said that, from a creative standpoint, he really wanted to take Murphy "somewhere where hes never been before," and that he and the writers goal was to "put Eddie through paces. To have him have an arc like most characters in movies do, where hes not just the Brer Rabbit character starting trouble," and to put him in a situation that "allows him to have a genuine obstacle."

Woody Omens, the cinematographer of the film, was insisted upon by Murphy, who had previously worked with him on Harlem Nights. Earl Watson worked as the films main Film editing|editor, and had previously worked with Hudlin on House Party. Francine Jamison-Tanchuck worked as the films costume designer.

The intention was to give Murphys character Marcus Graham a cool, yet sophisticated style. "So typically when it comes to black characters, either you have to be a successful, smart business person, or youre hip, but youre never both," said Hudlin. "And one of the reasons why the movie has had such enduring popularity is because the character is both. Hes much more in the Cary Grant mode of business person." While working on the characterization of the main character, Hudlin came up with the foot gag, which was used as a plot device to show the arc of the characters growth. It originated from a joke between the director and his friends about a man whos so picky about women that he even looks at their back teeth. "So its like, how do we define that Eddies a back teeth kind of guy? So I came upon the idea of showing feet. That no matter how beautiful this woman is, if she has messed up feet, hes out of there." Also, Hudlin said that "we know hes evolved into a higher level of consciousness when he doesnt even notice what Halle Berrys feet look like." The character Angelas art class was an idea the filmmakers had to put Murphy in a context where he could interact with kids, which he hadnt done prior to this film.

The cast rehearsed for two weeks before production started, which the director said "really created that sense of team, and everyone playing off each other and everyone having that comfort level." Murphy even told Hudlin that he hadnt rehearsed since his days at SNL, and that he was going to rehearse for all of his movies from then on.

The scene in which Marcus changes his mind while with Jacqueline was a major rewrite to the script. According to Hudlin, in the original draft, Eddies character Marcus "never decides anything, things are decided for him by the different women. So, we finally realized he had to make an affirmative choice. Going through the script with a buddy of mine, we talked about it, and my buddy Tre said look, hes got to make a decision. And I said, youre right. Hes got to reject her, and choose her. And it seems so obvious now, Im like whats wrong with me? What was wrong with all of us that it took us so long to figure out what we really needed to do to fix the movie? {{cite video
  | people = Hudlin, Reginald
  | title = Audio commentary for Boomerang.
  | medium = DVD
  | publisher = Paramount Pictures
  | date = 2002}} Retrieved on 2008-09-24. 

===Casting===
 
Hudlin knew immediately that he wanted Halle Berry for the role of Angela after she came in and did the reading, and was nervous that Murphy wouldnt like her. Berry had appeared in a couple of films prior to being cast in Boomerang. However, after Berry performed her screen test, Murphy told Hudlin "well, thats it. Theres no need to see the other two actresses because shes the part." Executives at Paramount Pictures were nervous about Robin Givens being cast in such a major role in the film, as she was disliked by many in the general public at the time because of her past relationship with Mike Tyson. Hudlin however, "thought that actually made her perfect for the role, that she was this formidable person, and a match for Eddie Murphy, who also had an increpid reputation as a Seducer|ladies man. So, I wanted the audience to feel like this would be a fair fight."

{{multiple image
|image1=MartinLawrenceHWOFJune2013.jpg
|width1=140
|caption1=Martin Lawrence (left) David Alan Grier (right)
|image2=David Alan Grier.jpg
|width2=120
|align=left
}} John Witherspoon Tisha Campbell also worked with Hudlin on House Party, and was brought on board to play Murphys obnoxious, and somewhat obsessive, neighbor. While on set, Hudlin would always rave about Campbell and Lawrence, and Murphy suggested Hudlin do a film with the two of them. Lawrence, who was developing his television show at the time, overheard the two actors talking, and decided to cast Campbell as his girlfriend and later wife, Gina, for his hit sitcom Martin (TV series)|Martin.

{{multiple image
|image1=Eartha Kitt 2007.jpg
|width1=110
|image2=Grace Jones @ Fremantle Park (17 4 2011) (5648773026).jpg
|width2=185
|caption2=Eartha Kitt (left) and Grace Jones
}} parody of herself. Of her work ethic, Hudlin said that "she was always 100% committed, and would do the absolute craziest thing at any given time. She was absolutely perfect for the role. It was written for her, and she came in very humble, very sweet."

Eartha Kitt was the hardest person to cast for the picture, as she was somewhat offended by her characters tone in the original script. Eventually she accepted the role, after some of the more tasteless jokes in the script were changed or removed. Hudlin however did say that "she was great to work with," and that "she really got into the spirit of it, and was a lot of fun."

Lela Rochon, who was known for her role in Murphys 1989 film Harlem Nights, was brought back for a small role in Boomerang. Chris Rock, who was a protege of Murphys, and had acted in small roles in a few other films at the time, was also given a bit part. The role of Lady Eloises butler was played by Jonathan Hicks, a friend of the directors, who replaced a sick actor at the last minute. Hicks was a reporter for The New York Times, not an actor, and took the role as a favor to Hudlin. {{cite video
  | people = Hudlin, Reginald
  | title = Audio commentary for Boomerang.
  | medium = DVD
  | publisher = Paramount Pictures
  | date = 2002}} 
 James Bond Baron Samedi Live and May Day from the film A View to a Kill), and Halle Berry (Giacinta "Jinx" Johnson from the film Die Another Day).

===Filming===
  Park Plaza Hotel, in a suite personally redesigned by Ivana Trump, which accounts for the unique styling.

Although director Hudlin was willing to let his actors improvise, he made sure to keep boundaries and rules to it, so that the jokes wouldnt lead to nowhere. He explained that once the actors "get a sense that someone will tell them if something doesnt work, or that were getting too far afield, then they feel comfortable doing what they do. So, thats the balance that we struck."

Production went very well, and as expected with so many comic actors and personalities around, was very lively. During production, Hudlin said that whenever David Alan Grier and Martin Lawrence worked, the films crew usually worked a little slower because wherever they were, there would be so much comedy being generated and they were so funny that everyone wanted to hang out near where they were throughout the day. In the scene where the new fragrance is being presented to Strangé, the director said that "what Grace was doing on the set while we were shooting was so funny that I remember Halle crying off camera, cause she was trying to keep a straight face but she couldnt. So whenever she was off-camera, she would just be literally crying because she was laughing so hard. And fortunately, for her reaction shots she was able to recover."
 Newark Airport. However, the filmmakers felt it to be too shocking, to the point where it threw the rhythm of the picture off, so they only used it in the small clip being edited for the perfume commercial.

The scene late in the film where Grier, Lawrence, and Murphy hug in front of the Empire State Building was shot around one or two oclock in the morning, when the lights are usually shut off. If the filmmakers had them shut off at their command, it would have cost them $40–$60,000, so instead Hudlin had the three hug, and the lights were eventually shut off as usual. When editing the film, they simply reversed the footage to make it appear as if the lights were being turned on. The filmmakers even thought about ending the movie at this point, deciding instead to go with Marcus attempting to win back Angelas affection. For this, an alternate ending was shot where Angela is teaching the kids at the school and Marcus comes, talks to her and eventually wins her back; the filmmakers thought it was slow and uninteresting, and eventually came up with the released version.
 New Orleans, and the scenes taking place there were shot in D.C. 

===Music===
  score for Antonio "L. A." Reid and Kenneth "Babyface" Edmonds worked on the soundtrack. With such a wealth of new material, director Hudlin said, "sometimes we used score, sometimes wed use a song by L.A. and Face, or sometimes Marcus would take some of the songs they had written and do score interpolations of it. For example, one of the songs, "Love Shoulda Brought You Home", is a great example. That came from a line of dialogue where Halle is furious with Eddie Murphy|Eddie, says love shouldve brought your ass home last night, slaps him. At that time, L.A. leans over to me and goes thats a great song title. I said, great idea. They write a song, "Love Shoulda Brought You Home". I said, well, who we going to get to sing it? And they said well you know, weve got this great young singer who sang the demo, Reggie. If you like the demo then we can use her. And I said great, lets use her. So that was Toni Braxtons debut. And I remember when I finally met Toni at Babyfaces wedding, L.A. said, you owe your career to this guy, he gave you your big break, which was really sweet." 
 Hound Dog".

===Design===
The production was very demanding, including an expensive wardrobe budget, which Hudlin felt was completely worth the price. "The clothing in the movie is incredible, for both the men and the women. Eddie looks great, Robin and Halle look great. Great hair, great makeup work for all concerned, cause we wanted it to look fantastic. We wanted to give people a level of production value they hadnt seen for Eddies first foray into true romantic comedy." 

===Influences=== Hudlin explained, "I remember when I saw Jules and Jim when I was a kid, and that awkward, awkward feeling of two guys, one has a better time with girls than the other, and they both fall in love with the same woman. And I remember the agony of watching the emotional stakes of that picture. And, this is just a fun movie, not trying to compare myself to Truffaut on any level, but, at least, thats what inspired the moment."

The pacing and rhythm of Howard Hawks His Girl Friday was a big influence on the pictures style. Woody Allens Annie Hall was also very influential while Hudlin worked on the project because, according to Hudlin, "if you talk about contemporary romantic comedy, youre really talking about Annie Hall." 

==Reception==

===Box office===
On its opening weekend, the film earned $13,640,706, and ranked #3 at the box office.  It also ranked #3 the two following weekends.  By the end of its theatrical run, the film had grossed over $70 million  domestically, and $61 million outside of the U.S., making a total of $131,052,444. It was the 18th highest grossing film in the U.S. in 1992. 

===Critical reception===
The film has received generally mixed reviews. At Allmovie, it was given 2½ stars, and called "imbalanced" and "uneven."  At Metacritic, it has averaged a 45% rating from critics.  Based on 29 reviews, the film has been given a 38% rating from critics at Rotten Tomatoes.  Film critic Roger Ebert gave the film and its stars positive reviews, calling the movie "predictable but enjoyable all the same."  In 2005, the film was ranked number 21 on Black Entertainment Television|BETs Top 25 Movies In the Last 25 Years list. 

=== Awards & nominations ===
 1993 BMI Film & TV Awards: Antonio "L.A." Kenneth "Babyface" Edmonds for End of the Road (winner)
; 1993 MTV Movie Awards:
* Best Breakthrough Performance: Halle Berry (nominated)
* Best Comedic Performance: Eddie Murphy (nominated)
* Best Movie Song: Boyz II Men, End of the Road (nominated)
* Most Desirable Female: Halle Berry (nominated)

==References==

 
  tags!-->

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 