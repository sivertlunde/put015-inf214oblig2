Three Against the World
 
 
{{Infobox film
| name           = Three Against the World
| image          = ThreeAgainsttheWorld.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 群龍奪寶
| simplified     = 群龙夺宝
| pinyin         = Qún Lóng Duó Bǎo
| jyutping       = Kwan4 Lung4 Dyut6 Bou2 }}
| director       = Brandy Yuen
| producer       = Anthony Chow
| writer         = 
| screenplay     = Sam Chim Leung Szeto Cheuk Hon
| story          = 
| based on       = 
| narrator       = 
| starring       = Andy Lau Teddy Robin Norman Chu Rosamund Kwan Charlene Tse Sandy Lam
| music          = Teddy Robin
| cinematography = Peter Ngo
| editing        = Keung Chuen Tak Peter Cheung Bo Ho Film Company Golden Harvest
| released       =  
| runtime        = 85 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          =  HK$8,926,502
}}
 Hong Kong action film directed by Brandy Yuen and starring Andy Lau and Rosamund Kwan.

==Summary==
When Charlie Chans (Andy Lau) friends fathers insurance company undertakes to look after a priceless copy of the Koran while it is on display, Charlie vows to help protect the scroll. When two sets of crooks looking to pinch the Koran turn up in town, Charlie has his hands full trying to outwit them and keep it safe.

==Cast==
*Andy Lau as Charlie Chan
*Teddy Robin as Cho Fei Fan
*Norman Chu as Sharp Shooter Ma Yun Lung
*Rosamund Kwan as Fans daughter
*Charlene Tse as Lungs girlfriend
*Sandy Lam as Fans daughter
*Chin Kar-lok as Siu Ming
*Yuen Woo-ping as Chia Yi Chen in disguise
*Wu Ma as Chia Yi Chen
*Cho Tat-wah as Detective looking for toilet
*Shing Fui-On as Sungs thug at bathhouse
*Chiu Chi-ling as Sungs thug at bathhouse Teddy Yip as Mings dad
*Chung Fat as Gambler on train
*Corey Yuen as Candy seller
*Yuen Shun Yi (cameo)
*Brandy Yuen (cameo)
*Pauline Wong Yuk-Wan as Secretary Fong

 {{Cite web |url=http://www.imdb.com/title/tt0095946/ |title=Three Against the World 
 |accessdate=23 July 2010 |publisher=imdb.com}} 
   

==Music==
*Theme song Enchanted (心醉)
**Composer: Wong Sing To
**Lyricist: Chan Ka Kwok
**Singer: Andy Lau

*Insert theme True and False (真真假假) Jonathan Lee
**Lyricist: Calvin Poon
**Singer: Andy Lau, Teddy Robin

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 