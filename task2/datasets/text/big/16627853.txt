Cadillac Records
 
{{Infobox film
| name = Cadillac Records
| image = Cadillac records poster.jpg
| caption = Theatrical release poster
| director = Darnell Martin
| producer = Sofia Sondervan Andrew Lack
| writer = Darnell Martin
| narrator = Cedric the Entertainer Jeffrey Wright  Steve Jordan
| cinematography = Anastas Michos
| editing = Peter C. Frank Sony Music Film Parkwood Entertainment
| distributor = TriStar Pictures  
| released =  
| runtime = 109 minutes
| country = United States
| language = English
| budget = $12 million
| gross = $8,880,045 
}} musical Biographical biopic written and directed by Darnell Martin. The film explores the musical era from the early 1940s to the late 1960s, chronicling the life of the influential Chicago, Illinois|Chicago-based record-company executive Leonard Chess, and a few of the musicians who recorded for Chess Records.
 Jeffrey Wright as Muddy Waters, Eamonn Walker as Howlin Wolf, and Beyoncé|Beyoncé Knowles as Etta James. The film was released in North America on December 5, 2008 by TriStar Pictures.

==Plot== Polish immigrant Jewish descent, starts the record label Chess Records in Chicago in 1950. It opens its doors for black musicians and attracts people such as Muddy Waters, Chuck Berry, Little Walter and Etta James.

==Main cast==
* Adrien Brody as Leonard Chess Jeffrey Wright as Muddy Waters
* Cedric the Entertainer as Willie Dixon
* Gabrielle Union as Geneva Wade
* Columbus Short as Little Walter
* Emmanuelle Chriqui as Revetta Chess
* Eamonn Walker as Howlin Wolf
* Mos Def as Chuck Berry
* Beyoncé|Beyoncé Knowles as Etta James
* Shiloh Fernandez as Phil Chess
* Jay O. Sanders as Mr. Feder
* Eric Bogosian as Alan Freed
* Kevin Mambo as Jimmy Rogers

==Background== soul legend Etta James    and guitarist singer-songwriters Chuck Berry and Willie Dixon.   

==Production==
 
The screenplay was written by director Darnell Martin.  The filming of Cadillac Records started in February 2008.  Filming locations included Louisiana, Mississippi, and New Jersey.  Martin directed the film,   financed by Sony BMG Film.  Cadillac Records was produced by Andrew Lack and Sofia Sondervan,  and co-executive produced by Beyoncé Knowles. 

===Casting=== Jeffrey Wright as Muddy Waters,   and multi-Grammy Award winner Beyoncé|Beyoncé Knowles as Etta James. According to director Martin,  the role of James was written with Knowles in mind. 

As production increased, the roster grew to include Canadian actress Emmanuelle Chriqui as Revetta Chess, Tammy Blanchard as Isabelle Allen, English actor Eamonn Walker as Howlin Wolf, and comedian Cedric the Entertainer as Willie Dixon.        Final line ups of the cast also grew to include rapper Mos Def as Chuck Berry, and Gabrielle Union in the role of Geneva Wade, Muddy Waters common law wife. 

===Music===
  Steve Jordan produced the soundtrack to the film. He also picked a group of blues musicians, including Billy Flynn (guitar), Larry Taylor (bass), Eddie Taylor Jr. (guitar), Barrelhouse Chuck (piano), Kim Wilson (harmonica), Danny Kortchmar (guitar), Hubert Sumlin (guitar), and Bill Sims (guitar) who, along with Jordan on drums, recorded all of the blues songs used in the film.   

Knowles recorded five songs for the soundtrack, including a cover version of Etta James "At Last" which was released on December 2, 2008 as its lead single.    Mos Def, Jeffrey Wright, Columbus Short, and Eamonn Walker recorded songs for the soundtrack, and Raphael Saadiq, Knowles sister Solange Knowles|Solange, Mary Mary, Nas, Buddy Guy, and Elvis Presley also appear on the album. The soundtrack was released in single and double-disc editions. 
 Michelle danced together for the first time as president and first lady. 

The soundtrack spent 48 weeks at number one of the Top Blues Albums.

The soundtrack was nominated for three 2010 Grammy Awards in the following categories: Best Compilation Soundtrack Album For Motion Picture, Television Or Other Visual Media, Beyoncés "Once in a Lifetime" for Best Song Written For Motion Picture, Television Or Other Visual Media and Beyoncés "At Last" for Best Traditional R&B Vocal Performance.

==Release and reception== world premiere on November 24, 2008 at the Egyptian Theatre in Los Angeles.  On December 5, 2008, it was released nationally in the United States of America. On its opening weekend, the film opened at Number 9, grossing $3.4 million in 686 cinemas with an $5,023 average.  When the film left cinemas in January 2009, it had yet to recoup its $12 million budget; it ended its run with a worldwide box office gross of  $8,880,045.   

===Critical reception===
The film received mostly positive reviews. Rotten Tomatoes gives a score of 68% based on reviews from 120 critics. Its consensus state that "What Cadillac Records may lack in originality, it more than makes up for in strong performances and soul-stirring music."  Another review aggretator, Metacritic, gave the film a 65% approval rating based on 30 reviews classifying that the film has "generally favorable reviews". 
 Daily News awarded the film with 3 stars and wrote in her review, "Writer-director Darnell Martin clearly respects the fact that the history of Chess Records is a worthy subject."  Most critics praised the film for its music, but complained about its script. Jim Harrington of the San Jose Mercury News praised Knowles vocal performance and wrote in his review that, "Beyoncé Knowles captivating voice and the films other pluses cant outweigh the glaring omissions from the story line for this critic" and "Chess Records deserves, and will hopefully someday get, a better spin than the one delivered by Cadillac Records." 

===Recognition and accolades===
David Edelstein of New York (magazine)|New York magazine named it the 4th best film of 2008, 
Stephanie Zacharek of Salon.com|Salon named it the 4th best film of 2008,  and
A. O. Scott of The New York Times named it the 10th best film of 2008.      During the 2009 award season, Knowles received a Satellite Award nomination for her portrayal of Etta James.  Knowles, Amanda Ghost, Scott McFarmon, Ian Dench, James Dring and Jody Street received a Golden Globe nomination, Best Original Song, for writing "Once in a Lifetime"; a song Knowles recorded for the films soundtrack.   Jeffrey Wright), Outstanding Supporting Actor in a Motion Picture (Cedric the Entertainer, Columbus Short and Mos Def), and Outstanding Supporting Actress in a Motion Picture (Beyoncé Knowles). 

===Home media===
The film was released on DVD and Blu-ray Disc|Blu-ray on March 10, 2009, and sold over 130,000 copies in its first week.    To date it has made an estimate of $11,916,737 in sales,  which coupled with its box office gross helped the film pay back its $12 million budget (total gross: $20,796,782).

==Awards and nominations==
{| class="wikitable" width="100%"
|-
! width="25%"| Ceremony
! width="45%"| Category
! width="20%"| Recipient
! width="10%"| Result
|-
| African-American Film Critics Association
| Best Supporting Actor Jeffrey Wright
|  
|-
| rowspan="8"| Black Reel Award
| colspan="2"|  
|  
|-
| colspan="2"|  
|  
|-
|  
| rowspan="2"| Darnell Martin
|  
|-
|  
|  
|-
| rowspan="3"|   Jeffrey Wright
|  
|-
| Eamonn Walker
|  
|-
| Mos Def
|  
|-
|  
| Columbus Short
|  
|-
| Golden Globe Award Best Original Song
| rowspan="2"| "Once In a Lifetime"
|  
|-
| rowspan="3"| Grammy Award Best Song Written for Motion Picture, Television or Other Visual Media
|  
|- Best Compilation Soundtrack Album for a Motion Picture, Television or Other Visual Media
|  
|- Best Traditional R&B Vocal Performance At Last"
|  
|-
| rowspan="7"| NAACP Image Award Outstanding Motion Picture
|  
|- Outstanding Actor in a Motion Picture Jeffrey Wright
|  
|- Outstanding Supporting Actress in a Motion Picture
| Beyoncé|Beyoncé Knowles
|  
|- Outstanding Supporting Actor in a Motion Picture
| Cedric the Entertainer
|  
|-
| Columbus Short
|  
|-
| Mos Def
|  
|- Outstanding Writing in a Motion Picture (Television or Film)
| Darnell Martin
|  
|-
| Satellite Award Best Supporting Actress in a Motion Picture
| Beyoncé|Beyoncé Knowles
|  
|}

==See also==
 
* List of American films of 2008
*  

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 