Bodyguards and Assassins
  
 
 
{{Infobox film
| name = Bodyguards and Assassins
| image = Bodyguards and Assassins poster.jpg
| film name = {{Film name| traditional = 十月圍城
| simplified = 十月围城
| pinyin = Shí Yuè Wéi Chéng
| jyutping = Sap6 Jyut6 Wai4 Sing4}}
| director = Teddy Chan
| producer = Peter Chan Huang Jianxin
| writer = Chun Tin-nam James Yuen Chan Wai Guo Junli Wu Bing Joyce Chan Zhou Yun Wang Po-chieh Cung Le
| music = Chan Kwong-wing Peter Kam
| cinematography = Arthur Wong Peter Ngor Lai Yiu-fai Liu Aidong Yeung Jan-yiu
| editing = Derek Hui Wong Hoi
| studio = We Pictures Cinema Popular Polybona Films China Film Group
| released =  
| runtime = 138 minutes
| country = Hong Kong China
| language = Cantonese Mandarin English
| budget = US$23 million 
| gross = US$5,837,674 (excluding China) 
}}
Bodyguards and Assassins is a 2009 Hong Kong historical action film directed by Teddy Chan, featuring an all-star cast, including Donnie Yen, Nicholas Tse, Tony Leung Ka-fai, Leon Lai, Wang Xueqi, Simon Yam, Hu Jun, Eric Tsang, Cung Le and Fan Bingbing.

==Plot== Sun Wen intends to go abroad to Hong Kong, then a British colony, to discuss his plans with fellow Tongmenghui members to overthrow the corrupt and crumbling Qing dynasty in China. Empress Dowager Cixi sends a group of assassins, led by Yan Xiaoguo, to kill Sun. Revolutionary Chen Shaobai arrives in Hong Kong a few days before Suns arrival, to meet Li Yutang, a businessman who provides financial aid for the revolutionaries. As Sun Wens arrival day draws near, trouble begins brewing in Hong Kong as Chen Shaobais acquaintances are murdered and Chen himself is kidnapped by the assassins during a raid. Li Yutang decides to officially declare his support for the revolutionaries after the newspaper agency is closed by the British authorities, who do not interfere in Chinas political situation. Li rallies a group of men, including rickshaw pullers, hawkers and a beggar, to serve as bodyguards for Sun Wen when he arrives. Lis son, Chongguang, is chosen to act as a decoy for Sun Wen to divert the assassins away while Sun attends the meeting and leaves Hong Kong safely.

==Cast== Sun Wen, Chen Shaobai and Yeung Ku-wan, are based on real historical figures.

===Main cast===
* Donnie Yen as Shen Chongyang (沈重陽), a policeman addicted to gambling. He is eager to do anything for money but decides to join the bodyguards after Yueru persuades him to do so. He engages Yan Xiaoguos henchman in a vicious fight and defeats his opponent eventually but is gravely wounded. He sacrifices himself to disable Yans horse. Sun Wen (孫文), leader of the revolutionaries. He arrives in Hong Kong for a secret meeting with fellow Tongmenghui members to discuss their plans for revolution. Chen Shaobai (陳少白), the chief editor of China Daily in Hong Kong. He is kidnapped by the assassins during a raid but manages to escape and rejoin his comrades. He pulls the rickshaw for the decoy Sun Wen in the final scene after Si dies.
* Wang Xueqi as Li Yutang (李玉堂), a businessman and old friend of Chen Shaobai. He initially provides only financial support for the revolutionaries only but decides to openly declare his support after being strongly influenced by them. He rallies a group of bodyguards to protect Sun Wen. fourth wife and Shen Chongyangs ex-wife. She has a daughter with Shen, and the child was raised as Lis.
* Wang Po-chieh as Li Chongguang (李重光), Li Yutangs 17-year-old only son. He goes against his fathers will to show his fervent support for the revolution. He sacrifices himself to protect Sun Wen by acting as a decoy to divert the assassins.
* Leon Lai as Liu Yubai (劉郁白), a beggar who comes from a rich family. He is highly skilled in martial arts and fights with an iron fan. He is killed by Yan Xiaoguo while holding off the assassins alone at the residence of Sun Wens mother.
* Nicholas Tse as Deng Sidi / "Si" (鄧四弟 / 阿四), a rickshaw puller who serves the Li family faithfully. He pulls the rickshaw carrying the decoy Sun Wen and brings his friends along to help. He sacrifices himself to buy time for the decoy to escape.
* Simon Yam as Fang Tian (方天), a former general living in exile in Hong Kong. He and his men disguise themselves as an opera troupe. He is killed when the assassins raided the theatre.
* Li Yuchun as Fang Hong (方紅), Fang Tians daughter. She knows martial arts and decides to join the bodyguards after her father is murdered. She is killed while preventing the assassins from using explosives to destroy the convoy.
* Mengke Bateer as Wang Fuming (王復明), an outcast monk from the Southern Shaolin Monastery who became a stinky tofu vendor. He seeks redemption in order to return to his former monastery. He has a gigantic build and possesses immense strength. He is apparently killed after being stabbed numerous times by the assassins but reappears one last time to destroy a structure to block the assassins path.
* Eric Tsang as Smith (史密夫), the police chief. He is initially unwilling to help the bodyguards due to orders from his British superiors but later eventually leads his men to protect the convoy, from assassin marksmen, for part of their journey.
* Hu Jun as Yan Xiaoguo (閻孝國), the assassin leader who was formerly a student of Chen Shaobai. He is fiercely loyal to the Qing government, and fears the chaos that would ensue following a revolution. He manages to kill the decoy Sun Wen before being fatally shot by Chen. Zhou Yun as Chun (阿純), Sis fiancée
* Lü Zhong as Madame Yang (楊氏), Sun Wens mother
* Jacky Cheung as Yeung Ku-wan (楊衢雲), a pro-democracy English teacher and President of the revolutionary Furen Literary Society . His death in the films prologue marks the first political assassination in Hong Kong.
* Michelle Reis as Liu Yubais lover
* John Shum as Zhang Dayou (張大友), a photographer. He is Chuns father.
* Cung Le
* Philip Ng
* Xing Yu
* Dennis To

 
 

==Accolades==
; 4th Asian Film Awards
* Nominated: Best Film
* Won: Best Actor (Wang Xueqi)
* Nominated: Best Newcomer (Li Yuchun)
* Won: Best Supporting Actor (Nicholas Tse)
* Nominated: Best Production Designer (Kenneth Mak)
* Nominated: Best Costume Designer (Dora Ng)
; 29th Hong Kong Film Awards
* Won: Best Film
* Won: Best Director (Teddy Chan)
* Nominated: Best Screenplay (Guo Junli, Qin Tiannan, Joyce Chan and Chan Tong-man)
* Nominated: Best Actor (Wang Xueqi)
* Nominated: Best Supporting Actor (Tony Leung Ka-fai)
* Won: Best Supporting Actor (Nicholas Tse)
* Nominated: Best Supporting Actress (Li Yuchun)
* Nominated: Best Supporting Actress (Fan Bingbing)
 
* Nominated: Best New Performer (Li Yuchun)
* Won: Best Cinematography (Arthur Wong)
* Nominated: Best Film Editing (Derek Hui and Wong Hoi)
* Won: Best Art Direction (Ken Mak)
* Won: Best Costume Make Up Design (Dora Ng Li-lo)
* Won: Best Action Choreography (Stephen Tung Wai and Lee Tat-chiu)
* Nominated: Best Sound Design (Kinson Tsang and George Lee Yiu-keung)
* Nominated: Best Visual Effects (Ng Yuen-fai, Chas Chau Chi-shing, Joe Tam Chi-wai and Yung Kwok-yin)
* Won: Best Original Film Score (Chan Kwong Wing and Peter Kam)
* Nominated: Best Original Film Song (Chan Kwong-wing, Chris Shum Wai-chung and Li Yuchun)
; 16th Hong Kong Film Critics Society Awards
* Won: Film of Merit
* Won: Best Actor (Wang Xueqi)
; 47th Golden Horse Awards
* Nominated: Best Film
* Nominated: Best Director (Teddy Chan)
* Nominated: Best Actor (Wang Xueqi)
* Nominated: Best Supporting Actor (Nicholas Tse)
* Nominated: Best Cinematography (Arthur Wong)
* Nominated: Best Visual Effects (Ng Yuen-fai, Chas Chau Chi-shing, Joe Tam Chi-wai and Yung Kwok-yin)
* Won: Best Costume Make Up Design (Dora Ng Li-lo)
* Nominated: Best Action Choreography (Stephen Tung Wai and Lee Tat-chiu)
* Nominated: Best Editing (Hui Wan Yu, Wong Hoi)

==See also==
* Hong Kong films of 2009

==References==
 

==External links==
*  
*  

   
{{succession box title = Hong Kong Film Award for Best Film years = 2010 Ip Man after = Gallants (film)|Gallants
}}
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 