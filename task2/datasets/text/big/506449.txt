Ferry Tales
{{Infobox film
| name           = Ferry Tales
| image          = 
| caption        =
| director       = Katja Esson
| producer       = Katja Esson, Sabine Schenk, Corinna Sager
| writer         = Katja Esson
| starring       =
| music          = Cassis, Robby Baier
| cinematography = Martina Radwan, Katja Esson
| editing        = Sabine Hoffman, Moira Demos
| distributor    = Women Make Movies
| released       = 2003
| runtime        = 40 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
| website        = http://www.ferrytales-documentary.com/
}} documentary film Staten Island to Manhattan. It was nominated for the Academy Award for Documentary Short Subject 

==Story==
Ferry Tales exposes a secret world that exists in the powder room of the Staten Island Ferry—a place that brings together suburban moms and urban dwellers, white-collar and blue-collar, sisters and socialites. For 30 minutes every day, they gather around mirrors to put on their makeup – talking not as wives, mothers, or professionals, but just as themselves. Sassy and honest, they dish on everything from sex scandals to stilettos, family problems to September 11, leaving stereotypes at the door and surprising viewers with their straight-shooting wisdom.

In broaching such topics as divorce, single motherhood and domestic violence, Ferry Tales goes beyond the surface to show us the realities of life for working women. A rare and honest look at the intersections of race and class, this heartwarming film is utterly charming and often outrageous, FERRY TALES gives these unlikely heroines their moment in the spotlight.

==Production==
In July 2001, Katja Esson decided to create a sort of ‘working-girl’ documentary about the women who occupy the ladies bathroom on the ferry each and every morning. Coming up with a concept was simple enough, but trying to gain the confidence of these women and filming them in their element was quite another matter.

Then, just as filming had started, the tragic events of September 11 happened. At first, Esson felt she would have to scrap the project altogether. Instead, she kept filming. The resulting footage shows just how strong a sense of community exists between the many different women in the powder room.

==Film Festivals & Awards==

Essons film gives a fantastic glimpse into one of the countless secret sub-cultures of New York. Her brilliant portrayal rethinks class and gender and turns our ideas of the suburban/urban divide on their head. (K.E. Flemming, New York University) 

	Academy Award Nominee for Best Short Doc

Womens Network Awards - Best Documentary Film   

	Underdog Film Festival - Best Doc

	Annapolis Film Festival - Honorable Mention

	Woodstock Film Festival - Honorable Mention

==References==
 

==External links==
*  
* 
*  

 
 
 
 
 
 
 