Winnie the Pooh and Tigger Too
{{Infobox film name = Winnie The Pooh and Tigger Too image = Winnie the Pooh and Tigger Too!.jpg caption = VHS Cover director = John Lounsbery producer = Wolfgang Reitherman Walt Disney writer = A. A. Milne   Larry Clemmons   Ted Berman   Eric Cleworth   Don Bluth   Gary Goldman   John Pomeroy narrator = Sebastian Cabot starring = Timothy Turner music = Buddy Baker   Sherman Brothers studio = Walt Disney Productions distributor = Buena Vista Distribution released =   runtime = 25 minutes country = United States language = English
}} Disney released Academy Award Best Animated 1840 United States presidential election, is based on the fourth and seventh chapters of The House at Pooh Corner, the second Winnie-the-Pooh book by A. A. Milne.

==Plot==
During the fall, Tigger has been bouncing on anyone he comes across for fun, especially Rabbit when he is gardening, which angers Rabbit, so he calls a meeting with Pooh and Piglet and formulates a plan to abandon Tigger in the mist|woods, and find him the next day so hopefully Tigger will stop bouncing on his friends unexpectedly. Initially the plan seems to work, but when Rabbit, Pooh, and Piglet cannot find their way home, Pooh makes a suggestion about following a sandpit in order to find their way out of the forest. In an attempt to prove Pooh wrong, Rabbit wanders away. Pooh and Piglet then fall asleep, but are woken by Poohs empty stomach. He explains to Piglet that his honeypots have been calling to his tummy from home and that he couldnt hear them over Rabbits voice. Pooh and Piglet find their way out of the forest, but are immediately bounced by Tigger. Piglet, realizing that the plan failed, mentions Rabbits plan, and Tigger goes into the forest to find him. Rabbit walks through the forest by himself, and is scared by numerous noises such as a caterpillar eating a leaf and frogs croaking. Rabbit tries to run away in a panic, only to be tackled by Tigger. Rabbit is humiliated that his plan to lose Tigger had failed. Tigger explains to him that "Tiggers never get lost", and takes Rabbit home.

Wintertime comes and Roo wants to go play. Kanga cannot be with him so she calls on Tigger to look after Roo as long as he comes back in time for Roos nap. Tigger gladly accepts. Along the way through the woods, Tigger and Roo see Rabbit skating on the ice. Tigger tries to teach Roo how to ice skate by doing it himself, but unfortunately, he loses his balance and collides with Rabbit while trying to regain it. In moments Tigger slides into a snowbank and Rabbit crashes into his house. Tigger then decides that he does not like ice skating. Later on, while bouncing around the woods with Roo on his back, Tigger accidentally jumps to the top of a very tall tree and is afraid to climb back down. He gets even more scared when Roo uses his tail as a swing, making Tigger think hes "rocking the forest".
 the narrator chimes in for help. Tigger begs him to "narrate" him down from the tree, and he tilts the book sideways, allowing Tigger to step onto the text of the page. Tigger starts to feel better that he made it this far but before he can do otherwise, the narrator tilts the book back the other way, causing Tigger to fall into the snow. 

Happy, Tigger attempts to bounce but Rabbit stops him reminding Tigger of the promise he made. Devastated, Tigger realizes he cannot bounce anymore and slowly walks away and Rabbit feels better that there will be peace, but everyone else does not and felt sad to see Tigger depressed and remind Rabbit of the joy Tigger brought when he was bouncing. Then Rabbit shows sympathy for Tigger and takes back the promise they had agreed on; he is then given a friendly tackle by an overly-excited Tigger. Tigger invites everyone to bounce with him and even teaches Rabbit how to do it. For the first time, Rabbit is happy to be bouncing, as is everyone else as Tigger sings his signature song once more before the short closes.

==Voice cast==
*Sterling Holloway – Winnie-the-Pooh
*Paul Winchell – Tigger Rabbit
*Dori Whitaker– Roo Piglet
*Timothy Timothy Turner – Christopher Robin Kanga
*Narrated Sebastian Cabot

==Awards==
In 1975, Winnie the Pooh and Tigger Too won the Grammy Award for Best Album for Children (this fact is mentioned during the pop-up facts during the film as a bonus feature for the 2002 DVD release of The Many Adventures of Winnie the Pooh). It was also nominated for the Academy Award for Best Animated Short.

==Winnie the Pooh featurettes==
*Winnie the Pooh and the Honey Tree (1966)
*Winnie the Pooh and the Blustery Day (1968)
*Winnie the Pooh and Tigger Too (1974)
*Winnie the Pooh and a Day for Eeyore (1983)

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 