Trans (film)
{{Infobox film
| name           = Trans
| image          = transfilm1999.jpg
| image_size     = 
| alt            = 
| caption        = Trans DVD cover
| director       = Julian Goldberger
| producer       = Michael A. Robinson Joe Monteleone Martin Garner
| writer         = Julian Goldberger Story: Michael A. Robinson Martin Garner 
| narrator       = 
| starring       = Ryan Daugherty Edge Edgerton Jon Daugherty Stephanie Davis
| music          = Fat Mama & Her Trans World Orchestra Jonathan Goldberger 
| cinematography = Jesse Rosen
| editing        = Affonso Gonçalves
| studio         = Down Home Productions Yid Panther
| distributor    = Cowboy Booking International 1998
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1998 United American independent film, written for the screen and directed by Julian Goldberger. The film is based on a story by Julian Goldberger, Michael Robinson, and Martin Garner. The film stars Ryan Daugherty as Ryan Kazinski and was filmed in Fort Myers, Florida. 
{{cite news
|url=http://www.miaminewtimes.com/2000-04-20/news/kulchur/
|title=Kulchur
|last=Sokul
|first=Brett
|date=April 20, 2000
|publisher=Miami New Times
|accessdate=2009-08-14}}  

==Background== James Herbert, particularly his collaborations with the band R.E.M. 
{{cite news
|url=http://www.indiewire.com/article/more_songs_of_the_south_with_julian_goldberger/
|title=More Songs of the South with Julian Goldberger
|last=Hernandez
|first=Eugene
|date=April 8, 1999
|publisher=indieWire
|accessdate=2009-08-14}} 

The rural country store depicted in the film is in actuality The Corkscrew Country Store, in Estero, Florida as evident by the listing in the credits and the address which is prominently displayed on the front of the store in the film. 
{{cite web
|url=http://www.manta.com/coms2/dnbcompany_w8s0g
|title=Corkscrew Country Store
|accessdate=2009-08-14}}   The Animal Clinic scenes were filmed at the Miracle Mile Animal Clinic in Fort Myers, Florida, again according to the films credits.

==Plot summary==
The film opens in the Southwest Florida Youth Detention Center, where we are introduced to the protagonist, Ryan Kazinski (Ryan Daugherty) in his cell, obviously having a difficult time with his confinement. We are also shown the harsh conditions and level of discipline in the facility through prisoners interactions with guards and Ryans own experience with the warden.

Later while on trash pickup detail, a fight breaks out between two inmates, and several other inmates (including Kazinski) escape in the confusion. Kazinski and two other inmates are seen running through an orange grove and a swamp, eventually coming to a farmhouse where they enter the house and steal civilian clothes.

The inmates leave the farmhouse and travel down a dirt road to a rural convenience store where Ryan steals an ice cream bar. While he is eating in the restroom, the other two escapees steal a truck and drive away, leaving Ryan behind.

Ryan emerges to find them gone and is questioned by some of the locals who take a liking to him and ask him about his plans and whether he intends to keep running. One of the locals gives Ryan a ride into town.

In the city, Ryan spends time listening to a street musician and talking with a parking meter.

Ryan then ends up in a supermarket where he inhales nitrous oxide from whipped cream cans and observes and interacts with customers. Upon leaving the supermarket, Ryan steps on the bottle cap of one of two men who are drinking beer on the hood of a car. The men become offended and one proceeds to beat Ryan unconscious.

While unconscious Ryan has a vision of a silhouetted woman against a blue sky.

Ryan is recognized and awakened by three other ex-inmates, with whom he attends a party. He leaves, assuring his friends that he has a place to stay.

Ryan visits his brother, who questions him as to why he ran when he only had one month left of his sentence to serve. Ryan cannot answer, and informs his brother of his plan to go to Colorado and seek his mother. Ryans brother then reluctantly tells him to leave before he becomes too attached to him again.

Ryan enters a bus station and asks for a one-way ticket to Denver. He discovers that he doesnt have the money for the ticket, and after attempting to bribe the bus station manager he is thrown out of the bus station.

Ryan then hitches a ride with a lady who takes him to a doughnut shop. While she is inside, he discovers a gun in her purse, steals it, and runs away.

Ryan is later approached by one of the other escapees who enlists his help in breaking into a veterinary clinic to steal drugs. The two break in, but while the other inmate steals the drugs, Ryan is distracted by the dogs in the kennels, one of which he frees and later places in the window of his brothers room.

Soon after, Ryan is spotted by police. He pulls out his gun, which the police notice, and then runs. The police fire two shots, but it is never made clear if Ryan is hit.

The following morning, Ryan is seen observing planes taking off and landing at a small airport. He approaches a man sitting at a table and asks if he could take him up. The man notices the gun in Ryans hand, and we then see the plane taking off. The film ends with Ryan looking out of the window smiling.

==Cast==
* Ryan Daugherty as Ryan Kazinski
* Jon Daugherty as Little Brother
* Edge Edgerton as Bus Station Manager
* Stephanie Davis as Boston Cream Girl
* Charles Walker as Inmate/Party Rapper
* Elijah Smith as Inmate/Party Rapper
* Jeremiah Robinson as Inmate/Party Rapper
* Marshall Williams III as Drill Guard

*The man at the desk as Ryan looks in a window during the street musician / parking meter scene is played by director, Julian Goldberger.

==Screenings==
Trans received screenings at the 1998 Toronto Film Festival, the 1999 Berlin Film Festival, the 1999 Sundance Film Festival, and the 1999 South By Southwest Film Festival, 
{{cite web
|url=http://www.ifc.com/movies/177590/Trans
|title=Trans IFC
|accessdate=2009-08-14}}  and was released in the United States on January 7, 2000. 
{{cite web
|url=http://www.film.com/movies/trans/6170671
|title=Trans (2000)
|publisher=Film.com
|accessdate=2009-08-14}}   The film has been aired in the U.S. on both The Independent Film Channel,  and the Sundance Channel. 
{{cite news
|url=http://www.indiewire.com/article/daily_news_sundance_and_trans_shooting_gallery_and_jezebel_and_new_site_for/
|title=Sundance and "Trans"; Shooting Gallery and "Jezebel" and New Site for Indie Films
|date=December 16, 1999
|publisher=Indiewire
|accessdate=2009-08-14}} 

==Distribution==

Despite praise from critics and recognition at film festivals, Trans had difficulty in securing distribution. A year after screening at the Sundance Film festival, the film still lacked widespread distribution in the United States. Robert Horton, of Film Comment magazine writes "Given the state of the arthouse/indie scene these days, it cant be too surprising that a film like Trans is left by the roadside...yet Trans is exactly the sort of smallish, idiosyncratically personal movie that belongs in the arthouse loop; for various reasons it will never draw the Happy, Texas-size crowd, but it will mesmerize the kind of audience that regularly takes a chance on something at a repertory house with an adventurous calendar" 

Of the film gaining distribution in 2000, Brett Sokul of Miami New Times wrote "Despite the reams of praise, conventional industry wisdom saw Trans as "difficult," i.e., not a reliable arthouse ticket-seller. Its an attitude that dramatizes the increasingly commercial pressures on the world of independent film, once a respite from the dictates of the box office but now often just as enslaved to it." 
{{cite news
|url=http://www.miaminewtimes.com/2000-04-20/news/kulchur/
|title=Kulchur
|last=Sokul
|first=Brett
|date=April 20, 2000
|publisher=Miami New Times
|accessdate=2009-08-14}} 

Trans was distributed by Cowboy Booking International.

===DVD releases===

Trans was first released on DVD by Fox Lorber on November 20th 2001. The DVD features the film, a trailer, the soundtrack, and web links. 
{{cite web
|url=http://www.amazon.com/Trans-Edge/dp/B00005MEVG/ref=sr_1_5?ie=UTF8&s=dvd&qid=1250103700&sr=1-5
|title= Trans 2001 release details
|publisher=Amazon.com
|accessdate=2009-08-14}}   Trans was released again on DVD by Wellspring Media on December 26th, 2006. 
{{cite web
|url=http://www.amazon.com/Trans-Jon-Daugherty/dp/B000JGWD3W/ref=sr_1_1?ie=UTF8&s=dvd&qid=1250106008&sr=1-1
|title= Trans 2006 release details
|publisher=Amazon.com
|accessdate=2009-08-14}} 

==Original Score & Soundtrack==

The score contains music by Fat Mama and her Trans World Orchestra, Jonathan Goldberger, and includes.

*"Peace" performed by Lonnie Liston Smith and The Cosmic Echoes
*"Peace" performed by Horace Silver Quintet
*"Theme" performed by Cibo Matto
*"In a Blink" performed by The Mondal Family 
*"Baby D" performed by The Mondal Family
*"Lions on the Loose" performed by The Mondal Family
*"Pimp Slap" performed by Fat Mama
*"Gifeltiluv" performed by Fat Mama
*"D.U.O" performed by Eulipion Journey Agents

==Reception==

Trans received mostly positive reviews from critics. Wesley Morris of San Francisco Chronicle wrote "Trans itches to be hyper-stylized but settles for occasional flights into coolness". 
{{cite news
|url=http://www.sfgate.com/cgi-bin/article.cgi?f=/e/a/2000/03/24/WEEKEND14490.dtl
|title=Trans mainly a detour into weird encounters
|last=Morris
|first=Wesley
|date=March 24, 2000
|publisher=San Francisco Chronicle
|accessdate=2009-08-14}}   Philadelphia City Paper wrote "Goldberger’s exceptionally weird debut invokes Jarmusch, Southern Gothic and THX 1138-style sci-fi". 
{{cite web
|url=http://www.citypaper.net/articles/011300/ae.mov.screenpicks.shtml
|title=Screen picks
|last=Adams
|first=Sam
|date=January 13–20, 2000
|publisher=Philadelphia City Paper
|accessdate=2009-08-14}}   Gavin Smith from   wrote "Trans remains a sensitive evocation of youthful turmoil". 
{{cite web
|url=http://www.nytimes.com/2000/01/07/movies/film-review-on-a-spree-to-nowhere-a-teenager-in-trouble.html
|title=On a Spree to Nowhere, A Teenager in Trouble
|last=Van Gelder
|first=Lawrence 
|date=January 7, 2000
|publisher=New York Times
|accessdate=2009-08-14}}   In a favorable review from the Village Voice, critic Amy Taubin writes: "Whats most remarkable about Trans is how faithful it is to Ryans consciousness and to the way it shifts between fantasy and a mesmerized response to details of the outside world." 
{{cite news
|url=http://www.villagevoice.com/2000-01-04/film/straight-out-of-the-everglades
|title=Straight Out of the Everglades
|last=Taubin
|first=Amy
|date=January 4, 2000
|publisher=Village Voice
|accessdate=2009-08-14}} 

The film also caught the attention of producer, Ted Hope, who offered to produce Goldbergers next feature, The Hawk is Dying. {{cite web
|url=http://www.ioncinema.com/news/id/1992/interview_julian_goldberger
|title=Interview: Julian Goldberger
|accessdate=2009-08-16}} 

===Awards & nominations===
* 1999, Readers Jury Prize for Best Film - Berlin International Film Festival. 
{{cite web
|url=http://www.iftn.ie/?act1=record&aid=73&rid=232&sr=1&only=1&hl=tamara&tpl=archnews
|title=The Berlin Film Festival Awards, Readers Prize of the Berliner Zeitung  
|date=February 25, 1999
|publisher=IFTN
|accessdate=2009-08-14}}  {{cite web
|url=http://www.wsfilms.com/talent-julian-goldberger.asp
|title=Julian Goldberger Bio
|publisher=Washington Square Arts and Film
|accessdate=2009-08-16}} 
* 2000, Someone to Watch Award at Independent Spirit Awards. 
{{cite web
|url=http://www.moviefone.com/celebrity/julian-goldberger/2006639/main|title=Julian Goldberger awards
|work=moviefone.com
|accessdate=2009-08-14}}  
{{cite news|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=LA&p_theme=la&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EAEE6B464725134&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM
|title=The Buzz
|date=January 16, 2000
|publisher=Daily News of Los Angeles
|accessdate=2009-08-14}} 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 