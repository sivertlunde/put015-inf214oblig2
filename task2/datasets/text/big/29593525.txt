Repeat Dive
{{Infobox film
| name           = Repeat Dive
| image          = 
| image size     = 
| caption        = 
| director       = Shimon Dotan
| producer       = Shimon Dotan Amos Mokadi
| writer         = Shimon Dotan Judith Hendel
| starring       = Doron Nesher
| music          = 
| cinematography = Daniel Schneor
| editing        = Danny Shick
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}

Repeat Dive ( , Tzlila Chozeret) is a 1982 Israeli drama film directed by Shimon Dotan. It was entered into the 32nd Berlin International Film Festival.   

==Cast==
* Mosko Alkalai
* Zaharira Harifai
* Dan Muggia
* Doron Nesher - Yoav
* Liron Nirgad
* Batia Rosenthal - Rachel
* Yair Rubin
* Dalia Shimko
* Zeev Shimshoni
* Ami Traub

==References==
 

==External links==
* 

 
 
 
 
 
 