Love Simple
 
{{Infobox film
| name           = Love Simple
| image          = LS posterCMYKlaurels.jpg
| alt            =  
| caption        = 
| director       = Mark von Sternberg
| producer       = 
| writer         = Mark von Sternberg
| starring       = Francisco Solorzano Patrizia Hernandez John Harlacher Caitlin Fitzgerald Israel Horovitz|
| music          = Danny Mordujovich
| cinematography = Lance Kaplan
| editing        = Dave Buchwald
| studio         = Cubicle Warrior
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Love Simple is a 2009 romantic comedy film written and directed by Mark von Sternberg and starring Francisco Solorzano, Patrizia Hernandez, John Harlacher, Caitlin Fitzgerald and Israel Horovitz.

The film is currently available on DVD through Synergetic Distribution and also for download and VOD on iTunes movie store. The producers of the film have partnered with the SLE Lupus Foundation, donating 10% of their proceeds from all sales to the organization.

==Plot==

Adam and Seta fall in love after meeting in a Brooklyn laundromat. She suffers from Lupus, while he is stuck at home caring for his chronically ill father. Both lie to avoid having to reveal they are anything but perfect. Eventually, their deceit unravels and they are faced with a choice: walk away or try to save the relationship. Deciding to give it one last chance, Adam and Seta reveal everything about who they really are despite the fact that they may not love one another once they know the truth.

==Production==

Principal photography was shot over a 16 day span (with two pick-up days) on location in Brooklyn, primarily in the Park Slope neighborhood with additional scenes shot in Williamsburg, Brooklyn|Williamsburg.

The film was shot with the Panasonic HVX200 with the SGPro 35mm lens adapter using Canon prime lenses.

==Release==
Love Simple premiered at the HBO International Latino Film Festival in NYC in 2009. The film made subsequent festival screenings at The Feel Good Film Festival (Los Angeles premiere), Orlando Hispanic Film Festival (special screening), NewFilmmakers New York, Victoria Film Festival (Canadian premiere), and was part of the 2009 Fall program at Filmmakers’ Symposium (co-sponsored by Kean University).

The film was released exclusively on iTunes June 16, 2010. For the week of June 16 – June 22, the producers donated 30% of sales to the SLE Lupus Foundation. The producers have continued their partnership with the SLE Lupus Foundation, donating 10% of all subsequent iTunes and DVD sale proceeds.

==External links==
* 
* 

 
 
 