Prathi Gnayiru 9.30 to 10.00
{{Infobox film|
| name = Prathi Gnayiru 9.30 to 10.00
| image =
| caption =
| director = M. Anbu
| writer =
| starring = Karunas Poornitha, Ramesh, Balaji, Ravi, Vaiyapuri, Delhi Ganesh, Vennira Aadai Murthy, Kuyili (actress)|Kuyili,
| producer = Salai Sahadevan
| music = John Peter
| editor =
| released =  
| runtime = 135 minutes
| language = Tamil
| country = India
| budget =
}}
Prathi Gnayiru 9.30 to 10.00 is a South Indian Tamil film released in 2006.

== Plot ==
Prathi Gnayiru 9.30 to 10.00 movie is about four college students Suresh (Ramesh), Remo (Karunaas), Murugan (Balaji) and Seetharaman (Ravi) who are carefree and have no responsibilities in their life. Mistaking Kalyani (Poornitha) to be a call girl, they forcibly gang rape her on a holiday. Coming to know of their mistake, they hurriedly return to Chennai.

Suresh is married off to a distant relative by his father (Delhi Ganesh). Only after the wedding, he comes to know that his wife resembles Kalyani. This sends a chill down the spines of Rameshs friends.

A sequence of events lead to Kalyani taking revenge on the friends. Whether they realized their mistake and were they taken to task forms the rest of the storyline. In the end Kalyani kills Suresh by making him inhale chili smoke.

 
 
 
 
 


 