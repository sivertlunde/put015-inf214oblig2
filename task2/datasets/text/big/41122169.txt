A Villa in Los Angeles
{{Infobox film
| name           = A Villa in Los Angeles
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Aliocha
| writer         = Aliocha
| starring       = Jean-Louis Coulloch Vincent Guédon Pascal Leduc Valère Leduc Pablo Saavedra
| cinematography = Pierre-Alain Giraud
| editing        = Aliocha Julie Duclaux
| distributor    =  
| runtime        = 80 minutes
| country        = France
| language       = French
}} French Comedy-drama. It is the first feature film written and directed by Aliocha.

==Plot==
A father, his son and some friends spend a weekend by the sea, but relationships worsen and tensions rise to the rhythm of the ever-changing tides.

==Cast==
* Jean-Louis Coulloch as Jean-Louis
* Vincent Guédon as Vincent
* Pascal Leduc as Pascal
* Valère Leduc as Valère
* Pablo Saavedra as Pablo
* Susan Jane Aufray as Sue, Jean-Louis friend

==External links==
*  
* 

 
 
 
 
 


 