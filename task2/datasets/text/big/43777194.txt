Sing and Like It
 

{{Infobox film
| name = Sing and Like It
| image = 
| alt = 
| caption = 
| film name = 
| director = William A. Seiter
| producer = Merian C. Cooper Howard J. Green (associate)
| writer = 
| screenplay = Marion Dix Laird Doyle
| story = 
| based on =     
| starring = Zasu Pitts Pert Kelton Edward Everett Horton
| narrator = 
| music = Max Steiner
| cinematography = Nicholas Musuraca
| editing = George Crone RKO Radio Pictures
| distributor = 
| released =      }} 
| runtime = 72 minutes
| country = United States
| language = English
| budget = 
| gross =        
}}

Sing and Like It is a 1934 American comedy film directed by William A. Seiter from a screenplay by Marion Dix and Laird Doyle, based on the unpublished short story So You Wont Sing, Eh? by Aben Kandel.  The film starred Zasu Pitts, Pert Kelton, and Edward Everett Horton.

==References==
 

 
 
 
 


 