Mannina Doni
{{Infobox film|
| name = Mannina Doni
| image = 
| caption =
| director = M. S. Rajashekar
| writer = T. K. Rama Rao
| starring = Ambarish  Sudharani  Vanitha Vasu  Vajramuni
| producer = Sandesh Nagaraj
| music = Hamsalekha
| cinematography = B. C. Gowrishankar
| editing = S. Manohar
| studio = Sandesh Combines
| released = 1992
| runtime = 133&nbsp;minutes
| language = Kannada
| country = India
| budget =
}} Kannada romantic drama film directed by M. S. Rajashekar and written by T. K. Rama Rao. The film features Ambareesh, Sudharani and Vanitha Vasu in the lead roles.  The film, produced by Sandesh Nagaraj, was widely appreciated for its songs tuned by Hamsalekha and lead actors performances upon release.

== Cast ==
* Ambareesh as Karthik
* Sudharani as Saubhagya
* Vanitha Vasu
* Umashree
* Jai Jagadish
* Mukhyamantri Chandru
* Vajramuni
* Ramesh Bhat
* Chi. Guru Dutt
* Avinash
* Pandari Bai
* K. S. Ashwath
* Thoogudeepa Srinivas
* Girija Lokesh
* Ashalatha

== Soundtrack ==
The music of the film was composed and lyrics written by Hamsalekha and the entire soundtrack was received extremely well.  Audio was released on Lahari Music.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Kogileye Kshemave
| extra1 = S. Janaki
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Rajanu Rani
| extra2 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Male Male Male Male
| extra3 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics3 =Hamsalekha
| length3 = 
| title4 = Nandu Nindu Indu
| extra4 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Megha Banthu Megha Rajkumar
| lyrics5 = Hamsalekha
| length5 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 