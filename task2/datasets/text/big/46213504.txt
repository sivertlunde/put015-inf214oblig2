Prachanda Putanigalu
{{Infobox film
| name           = Prachanda Putanigalu
| image          =
| alt            =
| caption        =
| film name      = ಪ್ರಚಂಡ ಪುಟಾಣಿಗಳು
| director       = Geethapriya
| producer       = M. K. Balaji Singh
| writer         = T. N. Narasimhan (dialogues)
| screenplay     = T. N. Narasimhan
| story          = M. K. Balaji Singh
| based on       =
| starring       = Master Ramakrishna Hegde Master Bhanuprakash Baby Indira Master Naveen
| narrator       =
| music          = Upendra Kumar
| cinematography = Kulashekar
| editing        = K. Balu
| studio         = Varuna Films Creations
| distributor    = Varuna Films Creations
| released       =  
| runtime        = 143 min
| country        = India Kannada
| budget         =
| gross          =
}}
 1981 Cinema Indian Kannada Kannada film, directed by Geethapriya and produced by M. K. Balaji Singh. The film stars Master Ramakrishna Hegde, Master Bhanuprakash, Baby Indira and Master Naveen in lead roles. The film had musical score by Upendra Kumar.  

==Cast==
 
* Master Ramakrishna Hegde
* Master Bhanuprakash
* Baby Indira
* Master Naveen
* Sundar Krishna Urs
* Srilalitha
* Sadashiva Brahmavar
* Dinesh
* Shivaram
* Musuri Krishnamurthy
* Sudheer
* Prabhakar
* Dwarakish
* Thoogudeepa Srinivas
* Jr Narasimharaju
* Dingri Nagaraj
* Guggu
 
==References==
 

==External links==
*  
*  

 

 
 
 

 