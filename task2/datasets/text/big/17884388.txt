Dil Kabaddi
 
{{Infobox film
| name           = Dil Kabaddi
| image          = Dil Kabsddi.jpg
| caption        = Film poster
| director       = Anil Sharma
| producer       = Shailesh Singh (Paramhans Creations)
| writer         = Vivek Anand Anil Senior Lezlie Sparx
| starring       = Irrfan Khan Rahul Bose Rahul Khanna Konkona Sen Sharma Soha Ali Khan Payal Rohatgi
| music          = Sachin Gupta
| cinematography = Anay Goswamy
| editing        =
| distributor    =
| released       =  
| runtime        =
| language       = Hindi English
| budget         =
| country        = India
}} Hindi film directed by debutante Anil Sharma (Senior). The film stars Irrfan Khan, Rahul Bose, Konkona Sen Sharma, Soha Ali Khan, Payal Rohatgi, Rahul Khanna and special appearance by Rahat Nusrat Fateh Ali Khan.   

==Synopsis==
The movie follows the same plot as Woody Allens Husbands and Wives.

Set in contemporary Mumbai, the movie takes a close look at the evolving equations among urban couples and paints the metamorphosis amongst the relationships with a comic stroke.

The film tracks the lives of two modern-day married couples — Samit (Irrfan Khan) and Mita (Soha Ali Khan); Rishi (Rahul Bose) and Simi (Konkona Sen Sharma) — caught in web of boredom, loss of love and temptation.

The film starts with an announcement by Samit and Mita of their separation and follows the moral muddles and emotional crises of the couples over the next year and a half — as friends fight, separate, take lovers and, in a way, reconcile.

== Cast ==
* Irrfan Khan as Samit
* Rahul Bose as Rishi
* Rahul Khanna as Rajveer
* Konkona Sen Sharma as Simi
* Soha Ali Khan as Mita
* Payal Rohatgi as Kaya
* Saba Azad as Raga
* Rahat Nusrat Fateh Ali Khan as special appearance in song "Zindagi Ye"
* Manu Malik as special appearance

== Controversy ==
Popular song from the movie titled Ehsaan was a copy from a Chicago based underground band, Ghom. Ghoms original track, titled Ehsaas, written by lead singer Azhar Mohammad and produced by Haaris Haroon, was uploaded to YouTube on November 21, 2007. Through inquiry it was noted that Sachin Gupta had listened to this track on YouTube, where the melody of the song was copied. The loyalty of this track was never honored to the original owners.

== References ==
 

== External links ==
*  
*  

 
 
 

 