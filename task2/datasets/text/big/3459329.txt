Lady Frankenstein
{{Infobox Film
| name           = Lady Frankenstein
| image          = L_Frankenstein.jpg
| image_size     = 
| caption        = 
| director       = Mel Welles Aureliano Luppi
| producer       = Umberto Borsato Hurbert Case Gioele Centanni Harry Cushing Egidio Gelso Jules Kenton Mel Welles
| writer         = Umberto Borsato Edward Di Lorenzo Egidio Gelso Aureliano Luppi Dick Randall Mary Shelley Mel Welles
| narrator       = 
| starring       = Joseph Cotten Rosalba Neri
| music          = Alessandro Alessandroni
| cinematography = Riccardo Pallottini
| editing        = Cleofe Conversi
| distributor    = New World Pictures
| released       = October 22, 1971
| runtime        = 99 mins
| country        =   Italy English
| budget         = under $200,000 Koetting, Christopher T. (2009). Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. p. 33. 
| preceded_by    = 
| followed_by    = 
}} Italian horror Paul Müller.  The script was written by cult writer Edward di Lorenzo.

==Plot==

The films opens with a trio of grave robbers (led by a man named Lynch) delivering a corpse to Baron Frankenstein (Cotten) and his assistant Dr. Marshall (Müller), for obvious reanimation purposes.

Baron Frankensteins daughter Tania (Neri/Bay) arrives from school, having completed her studies in medicine, and is greeted by her father and his servant, the handsome but mildly retarded Thomas.  Tania reveals to her father that she has always understood his work with "animal transplants" to be a cover for his work reanimating corpses, and that she intends to follow in his footsteps and help him in his work.

The next day, Frankenstein, Tania, and Marshall witness the execution of a criminal who is hanged down a Water well|well, and it is implied that his body will be harvested for their experimentation.   
Law enforcement agent Captain Harris (Hargitay) arrives to harass Lynch at the hanging.  Harris claims to be on to Lynchs grave robbing.

That evening, having harvested salient body parts, Frankenstein and Marshall successfully reanimate a corpse (The Monster) as Tania secretly watches.  The Monster, however, bear-hugs Frankenstein to death almost instantly, then walks out of the castle.  Tania and Marshall report the murder to Harris, but claim that it was a burglar.

Meanwhile, the narrative is interspersed with shots of the Monster roaming the countryside killing locals, including Lynch.

Tania then goads Marshall into admitting harboring romantic feelings for her.  She responds to his affections, but says that while Marshalls body is old, she finds the body of Thomas young and attractive.  The "solution" to this situation will be to transplant Marshalls brain into Thomass body.  To accomplish this, Tania seduces Thomas, and Marshall kills him with a pillow during their lovemaking.

Tania then transplants Marshalls brain into Thomass body.  The local villagers, however, have had enough and arrive to destroy the castle, replete with torches and pitchforks.  In the chaos, the Monster arrives and has a fight against Marshall/Thomas and Tania.  The Monster is defeated, but Tanias allegiance during the fight is highly suspect.

Harris arrives with Thomass sister to see Tania and Marshall/Thomas enjoying post-fight sexual intercourse.  However, during their lovemaking, Marshall/Thomas strangles Tania to death, and the film ends.

==Cast==
* Rosalba Neri as Tania Frankenstein (credited as Sara Bay)
* Joseph Cotten as Baron Frankenstein Paul Muller as Dr. Charles Marshall
* Peter Whiteman as The Creature
* Herbert Fux as Tom Lynch, the graverobber
* Mickey Hargitay as Captain Harris
* Lorenzo Terzon as Harris assistant (credited as Lawrence Tilden)
* Marino Masé as Thomas Stack (uncredited), the mildly retarded servant
* Renate Kasché, as Julia Stack (credited as Renata Cash), Thomas Sister

== Production ==
The film was largely financed through Harry Cushing, but just prior to the start of filming a letter of credit from a film company was not accepted by the Italian banks. The final last-minute $90,000 needed to make the film was obtained from Roger Cormans New World Pictures.    The suggestion of Rosalba Neri for the lead role was from one of the financers of the film. 

== The Frankenstein canon ==
The film is often compared with the Frankenstein cycle made by the Hammer Studios (1957–72), and may also have been an influence on Paul Morrisseys controversial Flesh for Frankenstein (1973).

== Feminism ==
Some have posited that Di Lorenzo intended that this film should present a feminist slant to the mad scientist genre. 

==See also==
* List of films featuring Frankensteins monster
* Women in science fiction
* List of films in the public domain

== References ==
 

==External links and resources==
* 
*  
*  
* 

 
 
 

 
 
 
 
 
 
 
 
 