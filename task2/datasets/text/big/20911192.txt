Demons and Wonders
{{Infobox film
| name           = Demons and Wonders
| image          = Demonios e maravilhas.jpg
| caption        = Original Brazilian film poster
| director       = José Mojica Marins
| producer       = José Mojica Marins
| writer         = José Mojica Marins Regina Andrion Crounel Marins
| starring       = José Mojica Marins
| music          =       
| cinematography = Giorgio Attili
| editing        = Nilcemar Leyart
| distributor    =  1987
| runtime        = 49 min  
| country        = Brazil Portuguese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1987 Brazilian autobiographical documentary film by and about Brazilian filmmaker, film director|director, screenwriter, film and television actor and media personality José Mojica Marins. Marins is also known by his alter ego Zé do Caixão (in English, Coffin Joe).    In the film Marins focuses on himself in scenes recounting life and experiences in filmmaking, with much focus on Marins many battles with Brazilian film censors.       

Marins began filming a second installment of the film called Alucinação Macabra (Macabre Hallucination), which consisted of scenes from his films mixed with video segments. Although all filming was completed, the film was never edited or released due to finances.   

==Cast==

*Carmem Marins
*Elza Leonetti do Amaral
*Francisco Cavalcanti
*Jofre Soares
*José Mojica Marins	(as himself)
*Lírio Bertelli
*Nilcemar Leyart
*Pelé
*Satã
*Sílvio Santos

==See also==
*Damned – The Strange World of José Mojica Marins

==References==
 

==External links==
* 
* 
*  at    
*   
* 

 

 
 
 
 
 
 
 
 

 
 