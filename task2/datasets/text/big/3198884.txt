The Devil's Playground (1976 film)
 Devils Playground}}
 
 
{{Infobox film
| name           = The Devils Playground
| image          = The Devils Playground 1976 cover.jpg
| caption        = Video cover
| director       = Fred Schepisi
| producer       = Fred Schepisi
| writer         = Fred Schepisi
| starring       = {{plainlist|
* Arthur Dignam
* Nick Tate
* Simon Burke
}}
| music          = Bruce Smeaton Ian Baker Brian Kavanagh
| studio         = The Film House
| distributor    = {{plainlist|
* Fred Schepisi
* Umbrella Entertainment
}}
| released       =  
| runtime        = 107 minutes
| country        = Australia
| language       = English
| budget         = A$306,000 Pike, Andrew; Cooper, Ross (1998). Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, p. 303. 
| gross          = A$334,000 (Australia)
}}
The Devils Playground is an Australian 1976 semi-autobiographical film by director Fred Schepisi. It tells the story of a boy growing up and going to school in a Catholic seminary. Its focus is on the trials of the flesh and the tensions that arise, for both priests and students, from the religious injunction to control ones sexuality.

==Plot==
In August 1953, the 13-year old Tom Allan attends a Catholic juniorate / (junior seminary)   in Melbourne, Australia. Students and brothers face individual challenges of faith and self-restraint.

==Cast==
* Arthur Dignam as Brother Francine
* Nick Tate as Brother Victor
* Simon Burke as Tom Allen
* Charles McCallum as Brother Sebastian John Frawley as Brother Celian
* Jonathan Hardy as Brother Arnold
* Gerry Duggan as Father Hanrahan Peter Cox as Brother James
* Thomas Keneally as Father Marshall
* John Diedrich as Fitz
* Alan Cinis as Waite Richard Morgan as Smith
* Jeremy Kewley as Thompson

==Production==
The screenplay was based on Schepisis own experience attending a Catholic seminary and took him five years to write.  The film financing took three years to arrange,  eventually coming from the Australian Film Commission ($100,000) and the Film House, Schepisis own company ($154,000), with the balance coming from private investment. Stratton, p.131-132 

It was shot in 1975 mostly at Werribee Park near Melbourne. 

==Recognition== Best Film, Best Direction, Best Lead Best Screenplay, Best Achievement Jury Prize. 

==Box Office==
The Devils Playground grossed $334,000 at the box office in Australia,  which is equivalent to $1,726,780 in 2009 dollars. According to Schepisi, the movie almost got its money back. 

==Home Media==
The Devils Playground was released on DVD with a new print by Umbrella Entertainment in November 2008. The DVD is compatible with all region codes and includes special features such as the theatrical trailer, an interview with Fred Schepisi, and audio commentary with Fred Schepisi.    This film was released on Blu-ray by Umbrella Entertainment in June 2014, with extras.

==See also==
* Cinema of Australia
* Devils Playground (TV series)

==References==
 

==External links==
*  
*  at Oz Movies
*  

 
 

 
 
 
 
 
 
 
 
 