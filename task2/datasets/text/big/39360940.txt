Blue (Da Ba Dee) (music video)
{{Infobox film
| italic title = no
| name = Blue (Da Ba Dee)
| image = Eiffel 65 Blue video.jpg
| caption = Eiffel 65 on television screens during the opening segment of the video.
| director = Celestino Gianotti 
| producer = 
| writer = 
| starring = Eiffel 65
| distributor = BlissMultiMedia
| released = 1999
| runtime = 3:40 English
| music = Eiffel 65
| budget = 
}}
The music video for Eiffel 65s Blue (Da Ba Dee) was released in 1999 by the BlissCoMedia, a computer graphics company of the Bliss Corporation, known at the time the video was produced and released as "BlissMultiMedia".  The video featured computer graphics done in 3ds Max, and features Eiffel 65 members Maurizio Lobina and Gabry Ponte trying to save Jeffrey Jey from the aliens Zorotl and Sayok6. The video was later uploaded to the BlissCoporations official YouTube channel in 2009, where, as of January 27, 2015, it has more than 55 million views. 

The video was listed in NMEs "50 Worst Music Videos". 

==Plot==
The video takes place on Tukon4, where lead singer Jeffrey Jey is abducted by blue   with him. Group members Maurizio Lobina and Gabry Ponte give chase in their own small spacecraft, and eventually both ships arrive on the blue aliens planet via a Portal (fiction)|portal.

It is then shown that the Jey was abducted to perform in a concert for the aliens, performing the same song he was about to sing when he was taken. He slowly realises that the crowd is composed of blue aliens, which are known as Tukonians,  and becomes increasingly concerned.

After fighting off a number of alien guards using martial arts and several energy-based, psionic abilities the pursuing members succeed in rescuing their lead singer. While leaving the planet, they are asked to return by the aliens. They oblige, and spend the remaining duration of the video by performing a concert for the blue-skinned extraterrestrials, creating a friendship between the band and the Blue Aliens, which possibly leads into Move Your Body. Zorotl also joins in the concert.

The events in the video share no relation to the songs lyrical content, apart from the aliens, which all have blue skin.

==Development==
Jey recalls of making the video and the song, "I started thinking about this character I invented called Zorotl and the lifestyle he led, from the way he would buy his house, pick his girlfriend, his job or the neighborhood he would live in. Then I came up with a color, a color I thought described the way he saw things." 

Like much of the Bliss Corporations music video, this one was done in a green screen garage studio at BlissCoMedia,   and it featured computer-generated graphics that were done in 3ds Max.  With very few resources, tutorials and books, and only one editing machine, the video was made between 1998 and 1999 in a garage in about 2 to 3 months, much like other videos made by BlissCo. Orfino, Mary (May 30, 2012).  . Eiffel65.com. Archived from March 5, 2013. Archive accessed from May 11, 2013. Originally accessed from April 16, 2013.  At the time the video was made and released, BlisscoMedia were originally known as BlissMultiMedia. 

Former BlissCo employee Davide La Sala has stated about coming up with the story for the video: "We had brainstorming sessions and we were a very imaginative team, huge fans of sci-fi movies and video games: Blade Runner, Star Wars, etc… we were master in doing our best and working with the few tools we had to create complete short stories in a very short period of time." 

Similar to other music videos by BlissCo, a total of five people worked on the music video, and, because, while the green-screen footage was done in a short amount of time, some if it would already be put into a computer generated 3D environment, while components of the band would also be shot. La Sala has said, "We were very flexible but every person in the team had his own special skill who was more towards motion graphics, design and editing, others more skilled in architectural design and me and the CEO experts in animation." 

==Legacy==
The video is BlissCoMedias most famous one. La Sala said to have been proud of the videos popularity. 

In 2009, when a YouTube upload of the video hit 10,000,000 views, BlissCoMedia.com celebrated the 10th anniversary of Zorotls debut in the video.   (In Italian). Blisscomedia.com. Accessed from May 11, 2013.  On September 2, 2009 the video was uploaded on BlissCos official YouTube channel. As of January 31, 2015, the video has 55,639,295 views, with 212,469 users liking the video and 5,547 dislikes.  

==References==
 

==External links==
* .

 
 
 