Home (2008 film)
 
{{Infobox film
| name           = Home
| image          = Home2008.jpg
| caption        = Film poster
| director       = Ursula Meier
| producer       = Denis Delcampe
| writer         = Ursula Meier Antoine Jaccoud
| starring       = Isabelle Huppert
| music          = 
| cinematography = Agnès Godard
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Switzerland
| language       = French
| budget         = 
}}
 official Swiss submission for Best Foreign Language Film at the 82nd Academy Awards.   

==Plot==
Marthe (Isabelle Huppert) and Michel (Olivier Gourmet) live with their three children in a house next to an uncompleted highway. They use the deserted road as an extension to their property. For example, they have an inflatable swimming pool and the son as well as his friends use the highway to ride their bicycles. They have been living for ten years close to the highway and believe that it will not be used. One day without warning, construction workers begin to upgrade the road and the highway becomes open to traffic. Instead of leaving the house, the family continue to live there, despite the increased noise from the passing traffic. It used to be the case that the father would simply walk across the highway in order to use his car to get to work. This becomes more complicated as the highway becomes increasingly used by motorists. He and his children eventually have to use a tunnel in order to gain access to the outside world.

Their younger daughter, Marion (Madeleine Budd), becomes obsessed about the quality and cleanliness of her surroundings. She monitors the grass as it exhibits evidence of carbon monoxide emissions and is convinced that the family will die prematurely, or may fall ill, as a consequence of living in such close proximity to the highway. The elder daughter, Judith (Adélaïde Leroux), continues to lead her life of sunbathing out on the front lawn in her bikini, despite attracting unwanted attention from passing motorists. One day she decides to leave the house and does not  return.

Meanwhile, the remaining family start to sound-proof their house by bricking themselves into the home. This includes blocking up all the windows and sealing all the ventilation points so no sound can get in. Confined in their own home, the pressure begins to take its toll on the family and they eventually leave their house.

==Cast==
* Isabelle Huppert as Marthe
* Olivier Gourmet as Michel
* Adélaïde Leroux as Judith
* Madeleine Budd as Marion
* Kacey Mottet Klein as Julien

==Production== Private Property.   

==Awards==
{| class="wikitable"
! Festival
! Category
! Winner/Nominee
! Won
|- International Film Bratislava International Film Festival  Grand Prix Ursula Meier No   
|-
|rowspan="3"|César Awards 2009|César Award  Best Cinematography
|Agnès Godard No   
|- Best First Film Ursula Meier No 
|- Best Production Design Ivan Niclass No 
|- 13th Flying Flying Broom Womens Film Festival International Federation FIPRESCI Award Ursula Meier Yes   
|- Lumiere Awards Best Technical Achievement
|Agnès Godard Yes   
|- Mar del Plata Film Festival  Best Actress Isabelle Huppert Yes   
|- ADF Cinematography Award
|Agnès Godard Yes 
|- Best Film Ursula Meier No 
|-
|Reykjavík International Film Festival FIPRESCI Award Ursula Meier Yes   
|- Swiss Film Prize  Best Emerging Actor or Actress Kacey Mottet Klein Yes   
|- Best Film Ursula Meier Yes 
|- Best Screenplay Ursula Meier and Antoine Jaccoud Yes 
|}

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 