Predicament
 
 
{{Infobox film
 | name = Predicament
 | image = Predicament.jpg
 | caption = Theatrical release poster
 | director = Jason Stutter
 | producer = Sue Rogers
 | based on =  
 | writer = Jason Stutter
 | starring = Jemaine Clement Hayden Frost Heath Franklin Tim Finn 
 | music = 
 | cinematography = Simon Raby 
 | editing = 
 | distributor =
 | released =  
 | runtime = 
 | country = New Zealand
 | language = English
 | budget =
 | gross = 
 }} Eltham in Taranaki Region|Taranaki, it was the last Morrieson novel to be adapted for cinema; his other three novels were filmed in the 1980s.

==Plot==
Predicament is a powerful and disturbing account of the psychological fantasy world of adolescence with the familiar small-town setting of Morriesons writing, so is a coming-of-age novel and a crime comedy. Naïve teenager Cedric Williamson is involved with two older criminally inclined misfits in photographing and blackmailing amorous couples, and ends up an accomplice to murder. It is set in a 1930s Taranaki town similar to Morriesons Hawera.

But while Morrieson’s first two novels were published in Australia, Predicament was declined by Angus & Robertson. It went through numerous drafts, many abandoned, before (like Pallet on the Floor) being published posthumously by Dunmore Press of Palmerston North in 1975. 

==Film==
The opening scene is of a hunched figure digging in the darkness, and demonstrates Simon Raby’s superb cinematography; as does the next (daytime) shot of a high rickety wooden tower built by Cedric’s mentally unbalanced father Martin. But when the characters start talking, what ought to be a darkly hilarious crime comedy dissolves into mush, according to reviewer David Larsen. The screenplay was written by the director Jason Stutter, who chopped up and rearranged Morrieson’s dialogue. 

==Cast==
* Jemaine Clement as Spook, misfit  
* Hayden Frost as Cedric Williamson, teenager 
* Tim Finn as his father, Martin Williamson 
* Heath Franklin as Mervyn Toebeck, misfit  
* Rose McIves as Maybelle 
* Edward Newborn as Detective Huggins
* David Van Horn as Officer Dawson
* Chad Mills as Blair Bramwell 
* Brooke Williams as Margot Bramwell
* Peter Mochrie as Vernon Bramwell
* Carmel McGlone as Rita Zombrini 
* Hadleigh Walker as Ernie Fox

==References==
 

==External links==
*   
*   
*   
 

 
 
 
 
 
 
 
 
 
 


 
 