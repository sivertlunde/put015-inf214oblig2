Chicken (film)
{{Infobox film
| name           = Chicken
| image          =
| image_size     =
| caption        = Mick and Kev look at each other while a train passes behind
| director       = Barry Dignam
| producer       = Barry Dignam
| writer         = Audrey OReilly
| narrator       =
| starring       = Darren Healy Niall OShea
| music          =
| cinematography = Peter Robertson
| editing        = James Finlan
| studio         = Hit and Run Productions Bord Scannán na hÉireann
| distributor    =
| released       =   
| runtime        = 3 minutes
| country        = Ireland
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} games of chicken.

==Plot==
Mick (Darren Healy) and Kev (Niall OShea) spend a late afternoon near railroad tracks by the seaside where Mick teaches Kev how to "Shotgunning|shotgun" beer for reasons then unknown to the viewer. He observes that Kev is "a bit of a wuss" after he fails to replicate the proper shotgunning technique and calls for Kev to come sit close to him for a test of courage, the knife game, which involves stabbing a knife between outstretched fingers at an ever-faster rate. The game is usually played with one persons hand at a time and as a gesture of what may be seen as self-sacrifice, Mick puts his hand over Kevs in order to shield Kevs hand from the brunt of an injury should it occur.

When a train speeds by them, Mick accidentally cuts Kev and himself very slightly with the knife. They clasp each others hands tightly and Mick, who suddenly seems very insecure and in need of affection is lovingly embraced by Kev, who perhaps has known all along why Mick brought him here.

A single screen of credits appears, then the film ends with a brief shot of the two in silhouette, standing apart, watching the sun set over the ocean.

==Production==
Chicken was shot over two days on 35 mm film with a Dolby Digital soundtrack.

The title of the short film is a double entendre, since "Chicken (gay slang)|chicken" is also a slang term for a young homosexual. 

==Awards and accolades==
{|class="wikitable" border="1" align="centre"
|-
! Film Festival !! Category !! Year !! Outcome
|- 2001 Cannes Short Film Palme dOr|| 2001 || 
|- Dublin Gay Best Short Film||2001|| 
|- Newport International Jury Award for Best Short Film||2001|| 
|- Barcelona Gay Special Jury Commendation||2001|| 
|- BFI International Jury Commendation Film Four TX Short Film Prize||2002|| 
|- Cabbagetown Film Best Short Film||2002|| 
|}

==References==
 

==External links==
*  — information page on Barry Dignams official website
* 
*  on Vimeo — the full short film in High-definition video|HD.
*  — the full short film in medium resolution.

 
 
 
 
 


 
 