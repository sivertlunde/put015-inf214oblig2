Until the Light Takes Us
{{Infobox film
| name           = Until the Light Takes Us
| image          = UTLTU poster.jpg
| alt            = film title, awards and critical commentary imposed over a stark black-and-white image of a man in black metal-wear – black clothing, spikes and corpse paint – holding an inverted cross
| caption        = Theatrical release poster
| director       = Aaron Aites Audrey Ewell
| producer       = Aaron Aites Tyler Brodie Audrey Ewell Gill Holland Frederick Howard  Gylve "Fenriz" Varg "Count Jan Axel Kristoffer "Garm" Rygg Faust (musician)|Bård "Faust" Eithun
| cinematography = Audrey Ewell Odd Reinhardt Nicolaysen
| editing        = Andrew Ford
| studio         = Artists Public Domain Field Pictures The Group Entertainment
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
}} documentary film about Norwegian black metal by the directors Aaron Aites and Audrey Ewell. It was released on December 4, 2009.

== Featured interviewees == Gylve "Fenriz" Nagell (Darkthrone) Varg "Count Grishnackh" Vikernes (Burzum) Jan Axel "Hellhammer" Blomberg (Mayhem (band)|Mayhem) Olve "Abbath" Eikemo (Immortal (band)|Immortal) Harald "Demonaz" Nævdal (Immortal)
* Bjarne Melgaard (visual artist) Kristoffer "Garm" Rygg (Ulver, Arcturus) Kjetil "Frost" Haraldstad (Satyricon (band)|Satyricon, 1349 (band)|1349, Keep of Kalessin)
* Faust (musician)|Bård "Faust" Eithun (Emperor (band)|Emperor) – Eithun chose to appear as a silhouette, with his voice distorted

== Release == Up in the Air). 

=== Reception ===
Until the Light Takes Us has received a 54% approval rating at Metacritic  and a 45% rating at Rotten Tomatoes.   

Andrew OHehir of Salon.com called the film "crafty and compelling".  Nick Pinkerton of The Village Voice said, "The filmmakers seem cowed into obeisance by their subjects. Vargs last onscreen appearance is accompanied by a montage fitting a schoolyard crush, and the films title is the translation of Burzums fourth album, Hvis lyset tar oss.     arrives a decade too late to add much." 

== References ==
 

== External links ==
*  
*  

 
 
 
 