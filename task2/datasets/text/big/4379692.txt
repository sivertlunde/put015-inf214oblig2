Children Shouldn't Play with Dead Things
 
 
{{Infobox film  name     = Children Shouldnt Play With Dead Things  image          = With dead things.jpg caption        = Theatrical release poster director       = Bob Clark  producer  Peter James  writer         = Bob Clark Alan Ormsby  starring  Jane Daly Roy Engleman Robert Philip Seth Sklarey music     = Carl Zittrer cinematography = Jack McGowan editing   = Gary Goch released   = 1972  runtime        = 87 minutes  country = United States language = English 
}}
 Black Christmas, A Christmas Story, and Porkys. 
The film was shot in 14 days on a budget of $70,000. Clark employed some of his college friends on it. Encyclopedia of Horror concludes that given the budget and the number of personnel involved, the special effects by Alan Ormsby are "surprisingly effective". 

==Plot==
 
The story focuses on a theatre troupe, led by Alan (Alan Ormsby). He is a mean-spirited director, who travels with the others by boat to a small island for buried criminals to have a night of fun and games. Once on the island Alan tells his group—he calls them his "children"—numerous stories relating to the islands history and buried inhabitants. At midnight using a grimoire, Alan begins a séance to raise the dead after digging up the body of a man named Orville Dunworth (Seth Sklarey). Though the original intent of the ritual may have been solely as a joke, Alan appears disappointed that nothing happens.

Afterwards the party continues and Alan goes to extremes to degrade the actors, using the corpse of Orville for his own sick jokes. Then, however, animated by the fell ritual, the dead return to life and force the troupe to take refuge in the old house. Unfortunately for the group, the dead get their revenge, and in the movies closing credits we see the group of corpses boarding Alans boat with the lights of Miami in the background.

==Cast==

* Alan Ormsby as Alan
* Valerie Mamches as Val
* Jeff Gillen as Jeff
* Anya Ormsby as Anya
* Paul Cronin as Paul
* Jane Daly as Terry
* Roy Engleman as Roy
* Robert Philip as Emerson
* Bruce Solomon as Winns
* Alecs Baird as Caretaker
* Seth Sklarey as Orville Dunworth

==Production==
 

==Reception==
 
  the film holds a score of 45% on Rotten Tomatoes, based on 11 reviews, with an average rating of  5.2/10. 

==DVD release==
 
Although previously available on VHS, a 35th anniversary special edition DVD was released in 2007 by VCI Entertainment. It features the uncut version of the film, a photo gallery, and a cast commentary. The same content was briefly made available on VHS through Anchor Bay Entertainment.

==Remake==
In November 2010, Gravesend Film Enterprises confirmed they would produce a remake, set to begin filming in Spring 2011. 

==References==
 

==External links==
* 
* 
* 
*  at Rotten Tomatoes

 

 
 
 
 
 
 
 
 