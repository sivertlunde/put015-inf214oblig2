Crazy Heart
{{Infobox film
| name           = Crazy Heart
| image          = Crazy heart poster.jpg
| caption        = Theatrical release poster Scott Cooper
| producer       = Robert Duvall Rob Carliner Judy Cairo T Bone Burnett Scott Cooper
| screenplay     = Scott Cooper
| based on       =  
| starring       = Jeff Bridges Maggie Gyllenhaal Robert Duvall 
| music          = Stephen Bruton T Bone Burnett
| cinematography = Barry Markowitz
| editing        = John Axelrad
| studio         = Informant Media Butchers Run Films
| distributor    = Fox Searchlight Pictures United States }}
| runtime        = 112 minutes 
| country        = United States
| language       = English
| budget         = $7 million   
| gross          = $47,405,566 
}} Scott Cooper Thomas Cobb. Jeff Bridges plays a down-and-out country music singer-songwriter who tries to turn his life around after beginning a relationship with a young journalist  portrayed by Maggie Gyllenhaal.  Other supporting roles are played by Colin Farrell, Robert Duvall,  and child actor Jack Nation. Bridges, Farrell, and Duvall also sing in the film.
 Hank Thompson.    Bridges earned the 2009 Academy Award for Best Actor for his performance in the film.
 Santa Fe),  in Houston, Texas,  and in Los Angeles, California. Original music for the film was composed by T Bone Burnett, Stephen Bruton, Ryan Bingham and others. Bingham and Burnett received the 2009 Academy Award for Best Original Song for co-writing "The Weary Kind", which Bingham also performed.

The film was produced for $7 million by Country Music Television, and was originally acquired by Paramount Vantage for a direct-to-video release,         but was later purchased for theatrical distribution by Fox Searchlight Pictures.  It opened in limited release in the U.S. on December 16, 2009. 

==Plot==
Otis "Bad" Blake (Jeff Bridges) is a 57-year-old alcoholic singer-songwriter who was once a country music star. He now earns a modest living by singing and playing his guitar at one-night stands, in small town bars, in the southwestern United States. Having a history of failed marriages (four that he admitted to, although a reference is made to a 5th he does not discuss) Blake is without a family. He has a son, aged 28, with whom he has not had contact in 24 years. He is mostly on the road performing, staying in cheap motels and traveling in his old 78 Suburban alone. The film opens with his arrival at a bowling alley for a show.

Enter Jean Craddock (Maggie Gyllenhaal), a young journalist after a story, divorced and with a four-year-old son, Buddy (Jack Nation). She interviews Blake, and the two enter into a relationship. Jean and her son become a catalyst for Blake beginning to get his life back on track. In doing so, he lets himself be pushed into renewing a professional relationship with Tommy Sweet (Colin Farrell), a popular and successful country music star he once mentored, and plays as the opening act at one of Tommys concerts, despite his initial balking and wounded pride at being the opening act to his former student. He asks Tommy to record an album with him, but Tommy says his record company insists on a couple more solo albums before a duet project can be recorded. He instead suggests that Blake concentrate on writing new songs that Tommy can record solo, telling him he writes better songs than anyone else.

Blakes drinking soon gets out of control, and he ends up running off the road while driving drunk. In the hospital, the doctor informs him that although he only sustained a broken ankle from the crash, he is slowly killing himself, and must stop drinking and smoking and lose 25 pounds if he wants to live more than a few more years.  Blakes relationship with Jean makes him start to rethink his life. He calls up his son to make amends, only to have his son tell him that his mother, Bads ex-wife, has died, and hangs up on him.  The relationship starts to look up, with Jean visiting him with her son Buddy.  After a situation where Blake loses Buddy briefly at a shopping mall while drinking at a bar, Jean breaks up with him.

After losing Jean and her son, who were becoming his only family, Blake resolves to quit drinking. After going through a treatment program at a rehab center, and with support from his Alcoholics Anonymous group and his old friend Wayne (Robert Duvall), Blake finally manages to get sober. Having cleaned up his act, he tries to reunite with Jean, but she tells him that the best thing he can do for her and Buddy is to leave them alone. After losing Jean, Blake finishes writing a song that he thinks is his best ever, "The Weary Kind", and sells it to Tommy.

Sixteen months later, Tommy plays "The Weary Kind" to an appreciative audience while Blake watches backstage, as his manager presents him with another of the large royalty checks for the song. As Blake is leaving, Jean approaches him, saying she has come to the show as writer for a large music publication. As they catch up, Blake sees an engagement ring on Jeans finger and tells her that she deserves a good man. He offers her the money from that royalty check for Buddy to have for his 18th birthday, which Jean initially refuses but eventually accepts after Blake says the song wouldnt exist without them. Jean asks if Blake would like to see Buddy again, but Blake declines saying it might be too unsettling for the young boy. The film ends with them walking away from the concert and chatting to each other.

==Cast==
*Jeff Bridges as Otis "Bad" Blake
*Maggie Gyllenhaal as Jean Craddock
*Colin Farrell as Tommy Sweet
*Robert Duvall as Wayne Kramer
*Paul Herman as Jack Greene
*Jack Nation as Buddy, Jeans son
*Ryan Bingham as Tony of Tony and the Renegades, backup group at bowling alley
*Rick Dial as Wesley Barnes, Jeans uncle, Santa Fe piano player

==Production==

===Development of original novel===
 . January 29, 2010. Retrieved on July 31, 2010.  Cobb based the character "Bad" Blake on  country music entertainer    . Thursday March 4, 2010. Retrieved on July 31, 2010.  The book, which was out of print since its original publication, went into print again when the film was released. 

===Pre-production===
The process of creating a film adaptation took many years because the concept was optioned, but was never produced into an actual adaptation until director Scott Cooper produced the film.  Cobb assumed that the film would use a more upbeat ending, because the Hollywood film industry often prefers "things that are generally positive".  According to Cobb, he had nothing to do with the making of the film.    The shooting of a sequence depicting the original book ending occurred; Cooper wanted to use it as the ending, but he did not get final authority to do so. A sequence of Bad Blake visiting his son in Los Angeles was also cut from the final film.  

Bridges initially passed on the role when he was first offered it. He explained to Vanity Fair that although he liked the script, he realized that the movie would make or break it and at the time the film had no musical attachments.    A year later he talked with T-Bone Burnett who was approached to work on the films soundtrack, together they both agreed to work on the film and Bridges joined the project. 

===Music===
  John Goodwin, Gary Nicholson, Greg Brown, Billy Joe Shaver, and Eddy Shaver

The songs are performed by various artists including actors Bridges, Farrell, and Duvall, as well as singers Bingham (who sings the theme song "The Weary Kind" and plays Tony in the film), Buck Owens, The Louvin Brothers, Lightnin Hopkins, Waylon Jennings, Townes Van Zandt, and Sam Philips.
 Best Song Best Compilation Soundtrack Album For Motion Picture, Television Or Other Visual Media.

==Reaction==

===Critical reception===
Review aggregator Rotten Tomatoes reports that 91% of critics have given the film a positive review based on 199 reviews, with an average score of 7.4/10.   

Critics mainly praised the performance of Jeff Bridges as Bad Blake, with many claiming he elevated the film above its seemingly conventional story and languid pace.  Tom Long from Detroit News writes, "Its a bit too easy, a bit too familiar, and maybe even a bit too much fun. But the easy magic Bridges brings to the screen makes it all work."   The Toronto Star s Linda Barnard attests that "some goodwill evaporates in the final reel, when a few false endings lead to a choice thats not the best one for Crazy Heart, but the generosity of Bridges performance puts us in a forgiving mood." 
 Golden Globe.   

===Accolades===
{| class="wikitable"
|-

! Year

! Association

! Category

! Nominated work

! Result
|-
| rowspan="5"| 2009
| rowspan="2"| Satellite Award
| Satellite Award for Best Actor - Motion Picture
| Jeff Bridges
|  
|-
| Satellite Award for Best Original Song
| "The Weary Kind"
|  
|-
| Los Angeles Film Critics Association
| Los Angeles Film Critics Association Award for Best Actor
| Jeff Bridges
|  
|-
| Las Vegas Film Critics Society
| Las Vegas Film Critics Society Award for Best Song
| "The Weary Kind"
|  
|-
| Chicago Film Critics Association
| Chicago Film Critics Association Award for Best Actor
| Jeff Bridges
|  
|-
| rowspan="11"| 2010
| rowspan="3"| Academy Award
| Academy Award for Best Actor
| Jeff Bridges
|  
|-
| Academy Award for Best Supporting Actress
| Maggie Gyllenhaal
|  
|-
| Academy Award for Best Original Song
| "The Weary Kind"
|  
|-
| rowspan="2"| British Academy Film Awards
| BAFTA Award for Best Actor in a Leading Role
| Jeff Bridges
|  
|-
| BAFTA Award for Best Film Music
| T Bone Burnett, Stephen Bruton
|  
|-
| rowspan="2"| Broadcast Film Critics Association Award
| Broadcast Film Critics Association Award for Best Actor
| Jeff Bridges
|  
|-
| Broadcast Film Critics Association Award for Best Song
| "The Weary Kind"
|  
|-
| rowspan="2"| Golden Globe Award
| Golden Globe Award for Best Actor - Motion Picture Drama
| Jeff Bridges
|  
|-
| Golden Globe Award for Best Original Song
| "The Weary Kind"
|  
|-
| Independent Spirit Awards
| Independent Spirit Award for Best Male Lead
| Jeff Bridges
|  
|-
| Screen Actors Guild Award
| Screen Actors Guild Award for Outstanding Performance by a Male Actor in a Leading Role
| Jeff Bridges
|  
|-
|}

==Home media==
The film was released on April 20, 2010, on DVD and Blu-ray. The single-disc DVDs special features included six deleted scenes, while the Blu-ray 2-disc set contained eight deleted scenes (including one in which Bad reunites with his son), plus two alternative music cuts and a short documentary in which the stars discuss "What Brought Them to Crazy Heart". 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   script

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 