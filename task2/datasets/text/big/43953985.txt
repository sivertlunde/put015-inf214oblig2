Aarattu
{{Infobox film 
| name           = ARAATTU (1979)
| image          =
| caption        =
| director       = IV Sasi
| producer       = Ramachandran for Murali Movies
| writer         = 
| screenplay     = T. Damodaran
| starring       = Jose, Seema, Shoma Anand, Vincent, Balan.K.Nair, Sanakaradi, Bahadur, Nellicodan, Pappu, Kaviyoor Ponnamma etc
| music          = A. T. Ummer
| cinematography = Jayanan Vincent
| editing        = K Narayanan
| studio         = 
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by IV Sasi and produced by Ramachandran under the banner Murali Movies. The film revolves around the life of fire workers  in Kerala.   The film stars Jose and Seema in the lead role along with Hindi actress Shoma Aanand, Balan.K.Nair, Kaviyoor Ponnamma  in various roles. The film had musical score by A. T. Ummer.   

==Cast==
 
*Kaviyoor Ponnamma Jose
*Sankaradi
*Prathapachandran
*Bahadoor
*Balan K Nair Kunchan
*Kuthiravattam Pappu Meena
*Nellikode Bhaskaran Seema
*Shoma Anand
*T. P. Madhavan Vincent
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Manjaveyilppoo || S Janaki || Bichu Thirumala || 
|-
| 2 || Romancham poothu || P Jayachandran, Ambili || Bichu Thirumala || 
|-
| 3 || Swapnagopurangal || K. J. Yesudas || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 