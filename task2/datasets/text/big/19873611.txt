Faceless (film)
{{Infobox Film
| name           = Faceless  
| image          = FacelessPoster1988.jpg
| image_size     = 
| caption        = Faceless poster under French title
| director       = Jesús Franco 
| producer       = René Chateau
| writer         = René Chateau Jesus Franco Michel Lebrun Jean Mazarin Pierre Ripert
| narrator       = 
| starring       = Helmut Berger Brigitte Lahaie Telly Savalas Christopher Mitchum Stéphane Audran
| music          = Romano Musumarra
| cinematography = Jean-Jacques Bouhon Maurice Fellous
| editing        = Christine Pansu
| distributor    = 
| released       = 22 June 1988
| runtime        = 98 minutes
| country        = France Spain
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Faceless is a 1988 French slasher film directed by Jesús Franco. The film is about Dr. Flamand (Helmut Berger) and his assistant Nathalie (Brigitte Lahaie) who lure unsuspecting victims to use their skin to perform plastic surgery on the doctors disfigured sister - a plot reminiscent of Francos first film, Gritos en la noche (1961). Hallen (Telly Savalas) is a New York businessman who hires private detective Sam Morgan (Chris Mitchum) to find his missing fashion model daughter Barbara (Caroline Munro). Other elements of the story include a Nazi doctor (Anton Diffring) and a chainsaw/power tool tormentor who are called in by Dr. Flamand. This was Savalas final acting appearance before his death in 1994.

==Plot==

A former patient of Dr. Frank Flamand (Helmut Berger), a disfigured Mrs. Francoisis (Tilda Thamar) seeks revenge for a botched operation by throwing acid at him but she misses and catches his sister, Ingrid (Christiane Jean), full in the face, resulting in severe burns.

At a photo shoot in Paris, the doctors assistant Nathalie (Brigitte Lahaie) drugs and kidnaps Barbara Hallen (Caroline Munro) and locks her in a room in the basement of Flamands clinic. Whilst checking on other kidnapped girls, a scuffle starts with Natalie and Gordon (Gerard Zalcberg), who lives in the basement chops off the girls arms. 

In New York City, Barbaras father Terry Hallen (Telly Savalas) is desperately awaiting a news of his daughter and hires a private detective, Sam Morgan (Chris Mitchum), to go and find her. Once in Paris, Morganvisits a morgue with Brian Wallace (Daniel Grimm) of the Paris police to see a decapitated body, but knows its not Barbara due to a missing mole. 

Flamand and Nathalie go to see a surgeon Dr. Orloff (Howard Vernon) about an operation to amputate Barbaras face and attach it to his sisters Ingrids face. Orloff tells them to track down Nazi doctor Karl Heinz Moser (Anton Diffring). They return to find Barbaras face has been badly cut by Gordon. 

Morgan interviews Barbaras photo director Maxence (Marcel Philippot) and gets some information through intimidation before Maxences bouncer, Doudo (Tony Awak), forces Morgan to flee. Meanwhile, Flamand has kidnapped another women, Melissa, to use for the face transplant. Morgan updates Terry with limited information on Barbara - that she was a prostitute and that she left with a gold watch. 

Moser arrives for the operation, but destroys Melissas donor face due to complications and Flamand and Nathalie seek a replacement. At a club they find an actress (Florence Guerin), trick her into going to the clinic, drug her and hide her body. Morgan traces a credit card belonging to Barbara Hallen to the Paris suburb of St. Cloud, and to Flamands clinic. 

At the clinic Morgan sees a watch Natalie is wearing and later sees this in pictures at his hotel as belonging to Barbara and decides to return to the clinic. A nurse at the clinic enters the basement and finds all of the girls locked up. She is caught and killed by Gordon. At this moment Moser, Flamand and Nathalie remove the actresss face and show it to Ingrid.

Morgan returns to the clinic and is attacked by Gordon but manages to impale him on some hooks. Morgan finds keys and locates the girls and Barbara but is locked in Barbaras cell with her by Natalie. Flamand, Moser and Nathalie then brick up the cell. Barbara and Sam find themselves trapped and gasping for air. 

Sam though has sent Terry a message saying Terry, I traced Barbara to this clinic in Paris. Im going in tonight to look for her. If I dont leave a message in 12 hours, send in the marines, Merry Christmas. Terry says to his office executive Jenny, get me on the first flight to Paris!, in hopes to rescue the two.

===Alternate ending===

The original ending of the film involved Sam successfully rescuing Barbara, and arresting Flamand, Nathalie, Moser, and Ingrid, with Terry going to Paris to pick them up. Jess Franco wanted a slightly different touch to make it different, so while switching the ending around, this time it is mentioned that Terry Hallen is going to Paris to the clinic, but it is left open, if he gets there in time to save them or not.

==Cast==
 
* Helmut Berger as Dr. Frank Flamand
* Brigitte Lahaie as Nathalie
* Chris Mitchum as Sam Morgan
* Telly Savalas as Terry Hallen
* Caroline Munro as Barbara Hallen
* Anton Diffring as Dr. Karl Heinz Moser
* Howard Vernon as Dr. Orloff
* Stéphane Audran as Mrs. Sherman
 
==Soundtrack==
The soundtrack to the film was released on a Maxi single with the theme song, Faceless, by Vincenzo Thoma although it has been out of print and hard to find for many years now. 

Two more of the songs used in the film by Thoma, Crystal Eyes and In the Heart of the City can be found on Romano Musumarras soundtrack to Les Nouveaux Tricheurs which is an LP. The last two songs used in the film, both by Carol Welsman were Just Imagination and Mais Que Bonita. Just Imagination had its own Maxi 45T single, and the song Mais Que Bonita can only be found on the Just Imagination LP album, which only printed 500 to 900 copies.

===Songs used===

*Faceless (Main Theme, Performed by Vincenzo Thoma)
*Crystal Eyes (Plays in the Florence Guerin Scene, Performed by Vincenzo Thoma) ("Un-credited")
*In the Heart Of The City (Plays While Ingrid Watches TV, Performed by Vincenzo Thoma)
*Mais Que Bonita (Plays During Model Photoshoot, Performed by Carol Welsman)
*Just Imagination (Plays in Club Scene with Morgan, Performed by Carol Welsman)

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 