Tiger Heart
{{Infobox film
| name           = Tiger Heart
| image          = 
| image_size     = 
| caption        = 
| director       = Georges Chamchoum
| producer       = Joseph Merhi (producer) Marta Merrifield (co-producer) Richard Pepin (producer) Bob Roberts (associate producer) Ted Jan Roberts (associate producer)
| writer         = William Applegate Jr. (screenplay)
| narrator       = 
| starring       = Ted Jan Roberts
| music          = John Gonzalez
| cinematography = Maurice K. McGuire
| editing        = Kevin Mock Ron Shaw
| studio         = 
| distributor    = 
| released       = 1996
| runtime        = 90 minutes
| country        = USA English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Tiger Heart is a 1996 American film directed by Georges Chamchoum. 

== Plot summary ==
A martial arts expert takes on a gang of criminals.

== Cast ==
*Ted Jan Roberts as Eric Carol Potter as Cynthia
*Jennifer Lyons as Stephanie
*Robert LaSardo as Paulo
*Rance Howard as Mr. Johnson Timothy Williams as Brad
*David Michael as Bobby
*Brian Gross as Steve
*Vincent DePalma as Manny
*Christopher Kriesa as Nat
*Gene Armor as Randolph
*Elena Sahagun as Chi-Chi
*Diane Klimaszewski as Amy
*Elaine Klimaszewski as Amanda
*Lorissa McComas as Jill
*Art Camacho as Sensei
*Frank Bruynbroek as Cyril
*George Calil as Jack
*Gary Bullock as Brads Father
*Albert Garcia as Ferret
*Jhoanna Trias as Billie
*Ron Yuan as Johnny
*Caroline Kim as Exchange Student
*Timothy D. Baker as Cop
*Denney Pierce as Drunk
*Chris S. Koga as Thug #1
*Randall Shiro Ideishi as Thug #2
*Chester E. Tripp III as Thug #3
*Tim Sitarz as Bouncer
*Cole S. McKay as Punk
*Rob King as Punk Richard Humphreys as Punk
*Paul Allen as Karate Student

== Soundtrack ==
*Derol Caraco - "Tiger Heart" (Written by John Gonzalez)
*Cynthia Manly - "We Like Crushin You"  (Written by John Gonzalez)
*Pattie Kelly - "Constant Conflict" (Written by John Gonzalez)

==References==
 

== External links ==
* 

 
 
 
 
 


 