Pandithurai
{{Infobox film
| name           = Pandithurai
| image          = Pandithurai DVD cover.svg
| image_size     = 
| caption        = DVD cover Manoj Kumar
| producer       = K. Balu
| writer         = Manoj Kumar
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Rajarajan
| editing        = P. Mohanraj
| distributor    = K. B. Films
| studio         = K. B. Films
| released       = 15 January 1992
| runtime        = 150 minutes
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1992 Tamil Manoj Kumar. Prabhu and Kushboo in lead roles. The film had musical score by Ilaiyaraaja and was released on 15 January 1992. The film was remade in Telugu as Bava Bavamaridi (1993) with Suman (actor)|Suman, Hindi as Bandhan (1998 film)|Bandhan (1998) with Salman Khan and in Kannada as Baava Baamaida (2001) with Shivrajkumar. The film completed a 100-day run. 

==Plot==

Pandithurai (Prabhu (actor)|Prabhu) left his mother (Manorama (Tamil actress)|Manorama) and his father to live with his newlywed sister (Sumithra (actress)|Sumithra) and his rich brother-in-law Malaisamy (Radha Ravi).

Few years later, Pandithurai becomes an uneducated angry youth but respects Malaisamy. Muthulakshmi (Kushboo), Malaisamys daughter comes back from the city. She falls in love with Pandithurai.

Malaisamy, who is respected among the villagers, has a secret relation with Sindamani (Silk Smitha), a stage dancer. Pandithurai tries to stop their relation but he fails and Malaisamy evicts him.

The rest story is how Pandithurai punished his brother-in-law and got married with Muthulakshmi.

==Cast== Prabhu as Pandithurai
*Kushboo as Muthulakshmi
*Radha Ravi as Malaisamy Manorama as Pandithurais mother Sumithra as Pandithurais sister
*Silk Smitha as Sindamani
*Goundamani as Mayilsamy Senthil as Chola Mansoor Ali Khan as Narasimma, Sindamanis brother
*Major Sundarrajan
*Ajay Rathnam as Rudramani
*Singamuthu

==Soundtrack==

{{Infobox album |  
  Name        = Pandithurai |
  Type        = soundtrack |
  Artist      = Ilaiyaraaja |
  Cover       = |
  Released    = 1992 |
  Recorded    = 1992 | Feature film soundtrack |
  Length      = 29:54 |
  Label       = |
  Producer    = Ilaiyaraaja |  
  Reviews     = |
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1992, features 6 tracks with lyrics written by Vaali (poet)|Vaali, Gangai Amaran and Piraisoodan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Mano || 4:59
|- 2 || Enna Marantha || K. S. Chithra, Mano || 5:03
|- 3 || Ennaipaarthu || K. S. Chithra || 5:12
|- 4 || Kaana Karunkuyil || S. P. Balasubramaniam, Swarnalatha || 4:48
|- 5 || Maalaiyitta Ponn || Malaysia Vasudevan || 4:38
|- 6 || Malliye Chinna Mullaiyae || Mano, Swarnalatha || 5:14
|}

==References==
 

 
 
 
 
 
 