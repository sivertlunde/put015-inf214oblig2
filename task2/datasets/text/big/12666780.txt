Marion's Triumph
{{Infobox Film
| name           = Marions Triumph
| image          =Marionstriumphdvdcover.jpg
| image_size     = 
| caption        = Marion
| director       = John Chua
| producer       = 
| writer         = 
| narrator       = Debra Messing
| starring       = 
| music          = 
| cinematography = 
| editing        = Tchavdar Gueorguiev
| distributor    =
| released       = 2003 (USA)
| runtime        = 50 min.
| country        =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 documentary that tells the  story of Marion Blumenthal Lazan, a child Holocaust survivor, who recounts her painful childhood memories in order to preserve history. The film combines rare historic footage, animated flashbacks, and family photographs to illustrate the horrors she experienced. It is narrated by Debra Messing.

==Summary==

“We often tripped and fell over the dead,” Marion says of life in the concentration camps, “death was everywhere.” From the age of four to ten Marion was in a concentration camp, where she says she picked lice out of her hair and urinated on herself to prevent frostbite.

At the onset of the war, the Blumenthal family left Germany to flee to United States|America. But, while they were in the Netherlands, Germany invaded and bombed the ships that would have taken them to safety. For the next six-and-a-half-years of her childhood, Marion struggled through the Holocaust, surrounded by death, starvation, filth and disease.

Marions stories suggest that self-discipline and a strong imagination helped her to survive. At one point, her mother had somehow scrapped up bits of wood and a potato and decided to cook up a meager soup on her bunk in the barracks. Of all nights, the Nazis had a surprise inspection that night, and, in the rush to hide the soup, the boiling water spilled on Marions leg. If she had cried out, the Nazis most likely would have discovered their attempt to cook illegal soup and killed both her and her mother. So Marion swallowed hard and pretended as if the severe burn on her leg wasnt there.

Whats interesting is that if Anne Frank had survived the war, her story would have been similar to Marions. Both Marion and Anne Franks families tried to escape the Holocaust, but were caught by Nazis. Both were  young girls during the war, and both traveled from Westerbork transit camp|Westerbork, a deportation camp, to concentration camps; Marion went directly to Bergen-Belsen concentration camp|Bergen-Belsen, but Anne Frank was sent first to Auschwitz and then to Bergen-Belsen. Its there that Anne Frank died of typhus, but somehow, despite her malnutrition and the disease that surrounded her, Marion survived.

After all that shes experienced Marions temperament is surprising. She smiles easily and stresses the importance of optimism. “Im determined not be bitter and angry,” she explains, “On the contrary, Im determined to be cheerful and positive as much as possible.” She believes that life owes her nothing, and its her job to make the best of what shes given. “None of us is spared hardships,” Marion explains, “Its not so much what happens to us, but how we deal with the situation that makes the difference.”
 churches and civil organizations, but her favorite audience is an auditorium full of students.

“You, the students, are the very last generation that will hear the story first hand,” Marion explains to her eager listeners, “I therefore ask you to please, please, share my story with your friends and with your family and someday with your children.”

==Production==
When Schindlers List was recently screened for high school students in Oakland, California, they were reported to have heckled and laughed. “The incident was a major catalyst to lead me to create this documentary,” explains filmmaker John Chua. It seemed to Chua that ignorance was at the root of the students irreverence. Chua wanted to make a Holocaust film that would be accessible for a middle-school-age audience. Marion, who has dedicated her life to educating the public about the Holocaust, especially students, was the ideal subject for his documentary. {{cite news
  | last =Schatz Rosenthal
  | first =Sharon
  | title =Survivor Film Aims to Educate Students
  | publisher = Jewish Journal.com
  | date =April 25, 2003
  | url =http://www.jewishjournal.com/home/print.php?id=10444
  | accessdate = August 7  }} 

==Reception==
Both Marion and her documentary have been highly praised. Her story has inspired people of all ages. One eighth-grader said of Marion, "Everything she said seemed to be for the greater good of the world--everything she does has an impact."  {{cite news
  | last =Rogowin
  | first =Larry
  | title =Jackson continues search for pebbles
  | publisher =Villa Park Review
  | date =December 3, 2003
  | url =http://members.aol.com/lombardian/vpnews0347a.html
  | accessdate = August 7  }}
  A middle school teacher comented on her "unwavering" generosity for sharing her story and time. 

==Notes==
 

==References==
*{{cite news
  | last =Schatz Rosenthal
  | first =Sharon
  | title =Survivor Film Aims to Educate Students
  | publisher = Jewish Journal.com
  | date =April 25, 2003
  | url =http://www.jewishjournal.com/home/print.php?id=10444
  | accessdate = August 7  }}

==External links==
*  
* 
* 

 
 
 
 
 
 