Love and Learn
{{Infobox film
| name           = Love and Learn
| image          =
| caption        =
| director       = Frank Tuttle
| producer       = Adolph Zukor Jesse Lasky
| writer         = Florence Ryerson(adaptation) Louise Long(scenario) Herman J. Mankiewicz(titles)
| based on       = story by Doris Anderson
| starring       = Esther Ralston
| music          =
| cinematography = Harry Fischbeck
| editing        = Verna Willis
| distributor    = Paramount Pictures
| released       = January 14, 1928
| runtime        = six reels
| country        = USA
| language       = Silent..(English titles)
}}
Love and Learn is a lost  1928 silent film comedy directed by Frank Tuttle and starring Esther Ralston. Famous Players-Lasky produced the picture with released through Paramount Pictures.  

==Cast==
*Esther Ralston - Nancy Blair
*Lane Chandler - Anthony Cowles
*Hedda Hopper - Mrs. Ann Blair Claude King - Robert Blair
*Jack J. Clark - Hansen (*as John J. Clark)
*Jack Trent - Jim Riley
*Hal Craig - Sgt. Flynn
*Helen Lynch - Rosie
*Catherine Parrish - Jail Matron
*Martha Franklin - Martha
*Jerry Mandy - Gardener
*Dorothea Wolbert - Maid Johnnie Morris - Bum (*as Johnny Morris)
*Guy Oliver - Detective

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 