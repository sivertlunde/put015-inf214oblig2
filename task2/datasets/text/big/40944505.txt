Fear of a Brown Planet Returns
{{Infobox film
| name           = Fear of a Brown Planet Returns
| image	         = Fear of a Brown Planet Returns film promotional poster.jpeg
| image size     = 
| caption        = Promotional poster
| director       = Danielle Karalus
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Aamer Rahman Nazeem Hussain
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Madman Entertainment
| released       =  
| runtime        = 75 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}

Fear of a Brown Planet Returns is a 2011 Australian concert film directed by Danielle Karalus featuring Australian comedy duo Fear of a Brown Planet (Aamer Rahman and Nazeem Hussain).

==Production and release==
The film features the "best of" material from Aamer Rahman and Nazeem Hussains 2010 sell-out festival show, Fear of a Brown Planet Returns, as well as content from their debut shows.       It was recorded at the Chapel Off Chapel in Melbourne    on 15 January 2011.   

On 31 August 2011, the film was released on DVD and Blu-ray.      

==References==
 

 
 
 
 
 
 
 
  
 


 
 