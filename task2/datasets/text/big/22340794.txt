Saint Sinner (film)
{{Infobox television film
| name = Saint Sinner 
| image = Saint Sinner.jpg
| caption =  Horror
| director = Joshua Butler
| producer = Oscar L. Costo
| writer = Clive Barker (story)  Doris Egan (screenplay) Hans Rodionoff (screenplay)
| starring = Greg Serano Gina Ravera Mary Mara Rebecca Harrell William B. Davis Antonio Cupo
| music = Christopher Lennertz
| cinematography = Barry Donlevy
| editing = Sean Albertson
| studio = Seraphim Films Universal Video
| released =  
| runtime = 90 min.
| country = USA Canada
| language = English
| budget = 
| gross = 
}} horror TV-movie Sci Fi Channel on October 26, 2002. 

==Plot==
In 1815 California, Father Michael, an emissary of Pope Pius VII, has traveled to meet with novice monk Brother Tomas. The young monks order serves as the secret repository for evil, supernatural objects collected by the Church, and kept there for safekeeping. Michael delivers an ancient statue that has trapped two beautiful female demons, Munkar and Nakir. Tomas and his friend Brother Gregory inadvertently release the murderous demons, who travel to the 21st century using the monasterys Wheel of Time. To redeem himself, Tomas pursues them to present-day Seattle, Washington, where he allies with police detective Rachel Dressler to recapture the homicidal terrors.

==Cast==
*Greg Serano	 . . . . . . 	 Brother Tomas
*Gina Ravera	 . . . . . . 	 Det. Rachel Dressler
*Mary Mara	 . . . . . . 	 Munkar
*Rebecca Harrell	 . . . . . . 	 Nakir
*William B. Davis	 . . . . . . 	 Father Michael
*Antonio Cupo	 . . . . . . 	 Brother Gregory
*Jay Brazeau	 . . . . . . 	 Abbot
*Simon Wong	 . . . . . . 	 Wade
*Boyan Vukelic	 . . . . . . 	 Playland Guard
*Brian Drummond	 . . . . . . 	 Officer #1
*Peter Bryant	 . . . . . . 	 Officer #2
*Lisa Dahling	 . . . . . . 	 Officer #3
*Kris Pope	 . . . . . . 	 Brother Rafael
*Robin Mossley	 . . . . . . 	 Clark
*Donna Yamamoto	 . . . . . . 	 Irate mother
*Justine Wong	 . . . . . . 	 Little girl
*David Thomson	 . . . . . . 	 Vince

==References==
 

==External links==
*  
* 
* 
* 

 

 
 
 
 
 
 
 
 
 