Mohabbat Ke Ansu
{{Infobox film
| name           = Mohabbat Ke Ansu
| image          = 
| image_size     = 
| caption        = 
| director       = Premankur Atorthy
| producer       = New Theatres
| writer         = 
| narrator       = 
| starring       = K. L. Saigal Akhtari Muradabadi Mahajabin 
| music          = R. C. Boral
| cinematography = Nitin Bose
| editing        = 
| distributor    =
| studio         = New Theatres
| released       = 1932
| runtime        = 128 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1932 social romantic Urdu film made in India.    It was directed by Premankur Atorthy for New Theatres Ltd. Calcutta..    The music for the film was directed by  R. C. Boral.    The film starred K. L. Saigal in his debut role with Akthari Muradabadi, Mahajabeen, Ansari and Sadiq.    According to reports the film was not successful    however Nevile claims that the "debut was successful beyond expectations" as it led to Saigal acting in several New Theatres films.   

==K. L. Saigal== Zinda Lash (1932) and Subah Ka Sitara (1932) as he didn’t want his relatives to find out about his profession.    Though the film did not do well he made an enormous impact with Chandidas (1934 film)|Chandidas (1934) and went on to become an idol and first superstar through his singing and acting.   

==Music==
Songlist.   
*Beqarar Itna To Kar De
*Ek Bimar Bhi Ghar Tujhse
*Buri Ghadi Thi
*Ham IzatrabeKalb Ka
*Koi Baanka Nukila Sajila Tumse Milega
*Nawaazish Chaahiye
*Piyarav Ko Seene Se Apne Lagao
*Sitam Ijaad Ho Koi
*Kya Aap Mere Marz Ko Achha Na Karenge

==References==
 

==External links==
* 

 
 
 
 
 