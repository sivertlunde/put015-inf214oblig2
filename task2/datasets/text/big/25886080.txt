Totally Spies! The Movie
{{Infobox film
| name           = Totally Spies! The Movie
| image          = TotallySpiesTheMoviePoster.jpg
| caption        = Official movie poster
| director       = Pascal Jardin II
| producer       = Michelle Lamoreaux    (executive producer)    Robert Lamoreaux    (executive producer)    David Michel   Vincent Chalvon-Demersay   Daniel Marquet
| writer         = Robert Lamoreaux   Michelle Lamoreaux
| based on       =  
| starring       = 
| music          = Maxime Barzel Paul-Étienne Côté
| editing        =
| studio         = Marathon Media Studio 37 TF1 Teletoon
| distributor    = Marathon Media Group
| released       =  
| runtime        = 75 minutes
| country        = Canada France French English English
| budget         =
| gross          = $1.3 million  
}}
 action comedy film. The film is an adaptation of the Totally Spies! series and reveals the origins of the three secret agents, how they met and their first mission as members of WOOHP.

In Canada and the United States, it was released on Cartoon Network and Teletoon a year later from the original release date.

==Plot==
The movie begins with Sam, Alex and Clover starting their new lives in Beverly Hills, CA. As each of them were about to cross paths outside a sushi restaurant, nearby WOOHP agents (including Jerry) purposely cause the giant sushi roll above the entrance to break off and chase after them, setting some nearby animals free in the process. The giant sushi roll, with the three girls log rolling on top of it, chases a baby pig to the street. The three girls are able to avoid it, save the life of the baby pig (which Alex later adopts and names "Oinky") and destroy the sushi roll before it causes any damage. After that, Alex, Sam and Clover introduce themselves and this starts their friendship when they later see each other again at their new school, meet the current principal Miss Skrich and their future rival Mandy and Dominique and Catlin. Clover offers her new friends a change of clothes after Mandy sprays them with a chai machine, but they find themselves sucked in through a locker and into one of the offices of WOOHP. Its there that they meet Jerry and fellow WOOHP agent Tad. Jerry reveals that WOOHP had been observing the three of them secretly since childhood, showing videos of each girl, and picking them as prime recruits for the organization. However, the girls are quick to reject the invite and refuse to join. However, they later are "forced" into training after each having traumatic experiences that seemingly relate to WOOHP. They agree to go through training and in 48 hours, complete the training.

After training, they are thrown into their first mission when famous celebrities, like rockstar Rob Hearthrob and animal psychologist Peppy Wolfman, have been mysteriously abducted. This also shows how the girls obtained their differently colored uniforms (thanks to a design suggestion by Clover). They first go to Wolfmans building where Alex has Oinky "go hog wild for mommy" as a distraction, and Oinky deliberately runs around the lobby with the other animals and the receptionist in pursuit. They later find that each went through a make over by a mysterious machine called the "Fabulizor", discovered thanks to security footage in Wolfmans office. They later see that everyone at school also had gone through the Fabulizor, having the same look the next day, and Oinky ends up going through the Fabulizor, getting the same make over as well. This is after nearly being blasted by one of Fabus minions in a fighter jet while being flown back to school and after nearly avoiding Miss Skritch as they sneak back into school. Tailing Mandy that night, they find that all of those who went through the Fabulizor became hypnotized by a special chip in their cheek bones prior to the make over and Alex spots Oinky in the crowd and grabs onto him, with Sam and Clover grabbing onto Alex as they were abducted into a strange space station out in space. They then meet the mastermind behind the entire affair, Fabu, a runway model who quickly lost fame in five minutes on the runway and was ashamed of not being a part of the crowd during his childhood. The spies accidentally expose themselves and are captured by Fabus strongest henchmen. He then relates his entire plan, to abduct everyone who went through the Fabulizor and place them inside a special space station which he calls Fabutopia to live out new lives in the posh surroundings, then use a missile to destroy all of Earth, before using his Fabulizor in reverse and give the girls each horrible make overs (Sam gets green skin, Clover grows a unibrow and Alex gets massive pimples). He then sets them to be blasted back to Earth in rockets. But just as he leaves, things get more difficult when Tad meets the girls again while they are still imprisoned and says he will let them fail the mission and stop Fabu himself, taking all the credit and regaining his "favorite agent" status with Jerry.
 Sphynx in his escape pod. After the mission, the girls admit that the mission was difficult at first, but it also made them friends, so they accept their position as spies. Alex is invited for a session with Wolfman and Clover is offered a date by Rob Hearthrob over the phone. But before that, they later return to school to face punishment from the principal for the "damage" they caused when trying to avoid her earlier on in the movie (thanks to Sam using a laser lip stick to cut an escape hole in the wall earlier). But fortunately, it seems that thanks to WOOHP, they have a new principal, whose name is not revealed, and seemingly does not know about the girls punishment and gives Sam high praise.

Miss Skritch had been transferred to another school in Antarctica in an igloo, Fabu, his henchman and Tad are later imprisoned and set for punishment by WOOHP and everyone who was rescued from aboard Fabus space station have their minds erased (including Mandy). But just after the girls celebrate getting even with Mandy for the last time, courtesy of one of WOOHPs gadgets, they are sucked away to another mission. The girls are quick to bring up personal appointments, but soon find themselves running from a WOOHP jet as it prepares to suck them aboard. But the girls are ready for their mission as they change into their spy uniforms and exclaim their friendship as the movie ends.

==Voice cast==
{| class="wikitable"
|-
! Character name (English, if different) 
! French voice actor  
! English voice actor 
|- Clover || Fily Keita || Andrea Baker
|- Sam || Claire Guyot || Jennifer Hale
|- Alex || Celine Mauge || Katie Griffin
|- Mandy || Celine Mauge || Jennifer Hale
|- Jerry || Jean-Claude Donda || Adrian Truss
|-
| Tad || Emmanuel Garijo || Jay Schramek
|-
| Madame Scritch (Mrs. Scritch) || &nbsp; || Barbara Budd
|-
| Peppy Garou (Peppy Wolfman) || &nbsp; || Walker Boon
|-
| Groin Groin (Oinky) || &nbsp; || &nbsp;
|-
| Rob Idole (Rob Hearthrob) || &nbsp; || Lyon Smith
|-
| Yuri || &nbsp; || Jason Gray
|-
| Fabu || Karl Lagerfeld || Joris Jarsky
|}

==Release==
===Theatrical===
The movie was released in France on 22 July 2009 in 272 theatres.

On 14 October 2009 the movie hit the Netherlands and stayed there for seven weeks. On 10 March 2010 the movie came out on DVD in the Netherlands.

===Television===
The film aired on Disney Channel Asia on 27 February 2010, a week later than the première of the season finale adaptation of its spin-off, The Amazing Spiez!. Prior to the film version, Disney Channel Asia has made two compilation movies of the TV series. The first one covers the 3rd season finale (Evil Promotion Much? Part 1-3) and the second one covers the 4th season finale (Totally Busted! Part 1-3).

The film was first shown in the US on Cartoon Network on April 25 at 7pm.

===DVD release===
A DVD of the movie in its original language, French, was released in France on the 3rd of February through Fox Pathe Europa. It topped charts in the French Amazon in the week of its release.

Vendetta Films, a new New Zealander film distributing company, has recently confirmed the release date of the Totally Spies: The Movie! DVD. It will be released on 1 April, nothing else has been announced. Vendetta can ship overseas. It was also announced that it will cost $24.99 NZD. Marathon currently has no plans to release it in any North American country other than Mexico.
On 30 March 2010, news was obtained that Cartoon Network have the rights to release the Totally Spies! The Movie in America including the spin-off, The Amazing Spiez!

==Soundtrack==
There was also an official soundtrack released in France. It includes the song "Walk Like an Egyptian" by The Bangles. The opening and closing for the movie is the song "Gold Guns Girls" by Canadian rock band Metric (band)|Metric. 

==Reception==
===Box office===
The film grossed $572,000 on the first weekend and ranked at number #9 at the French box office with approximately $2,100 per theater.    The gross for the second, third, and fourth weekends were $191,000, $89,106 and $29,083 respectively. The film grossed $1.3 million internationally.   

===Awards===
The film earned 2011 Kidscreen Awards under Best TV Movie category. 

==References==
 

== External links ==
*  
*  
*  
*  

{{Navbox
| name = Totally Spies!
| titlestyle = background:lightblue
| title = Totally Spies!
| groupstyle = background:lightblue
| abovestyle = background:lightblue
| group1 = Video games
| group2 = Other
| list1 =         | belowstyle = background:lightblue
| list2 =        
| below =    
}}

 
 
 
 
 
 
 
 
 
 
 