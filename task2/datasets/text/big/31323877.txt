SAS 181 Does Not Reply
{{Infobox film
| name           =SAS 181 antwortet nicht
| image          = 
| image size     =
| caption        =
| director       =Carl Balhaus
| producer       =Adolf Fischer
| writer         = Wolfgang Krüger
| narrator       =
| starring       =Ulrich Thein 
| music          = Helmut Nier
| cinematography = Horst E. Brandt
| editing        = Ilse Peters
| studio    = DEFA 
| distributor    = PROGRESS-Film Verleih
| released       = 3 July 1959
| runtime        = 90 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} East German black-and-white film, directed by Carl Balhaus. It was released in List of East German films|1959.

==Plot==
Kurt, a young apprentice in a fishing commune and a member of the Technology and Sport Association, enters a race to show his maritime skills. After he fails to win, he falls in love with a girl named Anke, but she loves the apprentice Hannes. Kurt and Hannes sail with skipper Laue on one of the collectives boats, SAS 181. Laue pretends to be Kurts friend and encourages a conflict between the two boys. However, they both realize that he intends to reach the Danish isle of Bornholm and defect to the West. The two settle their differences and, with the help of the old fisherman Jens, return the boat home.

==Cast==
*Ulrich Thein as  Kurt		
*Otmar Richter as Hannes 		
*Rita Gödikmeier as Anke
*Friedrich Wilhelm Junge as Linnet 		
*Erwin Geschonneck as Laue 	
*Wilhelm Koch-Hooge as Poppe		
*Gustav Püttjer as Jens		
*Erich Brauer as Laues deck hand		
*Hans Finohr as Kollmorgen		
*Klaus Gendries as Eisermann		
*Fritz Diez as collective director
*Harry Hindemith as Danish harbor manager	
*Hans-Joachim Glaser as Spuchti		
*Gerd Scheibel as Knautschke	
*Klaus Erforth as

==Production==
The film was the first in the history of East Germany to portray the pre-military youth training organization known as the   (GST). It was produced with the assistance of the association. 

==Reception==
Heinz Kersten noted that the theme of "fleeing from the republic" - a common feature in East German films, which showed a person leaving the GDR for the West, grow disillusioned and sometimes commit suicide - was present in the plot of the SAS 181, as well.  The Germa Film Lexicon cited it as having "an unimaginative plot". 

==References==
 

==External links==
*  

 

 
 
 
 
 