Love Story (2011 New Zealand film)
 
 

{{Infobox film
| name           = Love Story
| image          = 
| alt            =  
| caption        = 
| director       = Florian Habicht
| producer       = Pictures for Anna
| writer         = Florian Habicht Peter O’Donoghue
| starring       = Masha Yakovenko Florian Habicht Frank Habicht
| music          = 
| cinematography = Maria Ines Manchego
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = New Zealand
| language       = English
| budget         = 
| gross          = 
}}
Love Story is a 2011 New Zealand film directed by and starring Florian Habicht.  Set in New York City, the film combines real-life scenes where members of the public dictate the love story with those of Florian (as himself) and Masha Yakovenko as they act it out.
 New Zealand International Film Festival 2011. Pluk de Pulp saw the film during the London International Film Festival, and invited Habicht to make a film about his group.

==External links==
*  

 
 


 