Adventures of Red Ryder
 
 
{{Infobox Film
| name = The Adventures of Red Ryder
| image = Advredryder.jpg
| caption = Theatrical release poster John English
| producer =Hiram S. Brown Jr
| writer =Franklin Adreon Ronald Davidson Norman S. Hall Barney A. Sarecky Sol Shor Tommy Cook Harry Worth Hal Taliaferro William Nobles
| music = Cy Feuer William Lava Paul Sawtell William P. Thompson Edward Todd
| distributor =Republic Pictures
| released       =   {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 12–13
 | chapter =
 }} 
| runtime         = 12 chapters / 211 minutes 
| country = United States
| language = English
| budget = $144,852 (negative cost: $145,961) 
}}
 Republic Serial movie serial Western comic John English. This serial was the 18th of the 66 serials produced by Republic. Westerns made up a third of all serials produced by the studio.

==Characters and story== ranchers from their land to profit from a railroad. However, on one of these ranches, the Circle R, lives the Ryder family who resist the gang. After his father, Tom, is killed by One Eye Chapin, Red Ryder swears revenge and sets out to defeat the gang once and for all.

==Cast==
*Don "Red" Barry as Red Ryder. Donald Barry retained the nickname from this serial as Don "Red" Barry. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 2. In Search of Ammunition
 | page = 22
 }} 
*Noah Beery as Ace Hanlon Tommy Cook as Little Beaver Maude Pierce Allen as Duchess Ryder Vivian Coe as Beth Andrews Harry Worth as Calvin Drake
*Hal Taliaferro as Cherokee Sims
*William Farnum as Colonel Tom Ryder
*Bob Kortman as One-Eye Chapin
*Carleton Young as Sheriff Dade

==Production==
The Adventures of Red Ryder was based on Fred Harmans comic strip.  The serial was budgeted at $144,852 although the final negative cost was $145,961 (a $1,109, or 0.8%, overspend). 1940 was the first year in which Republics overall spending on serial production was less than in the previous year.  It was filmed between 27 March and 25 April 1940.  The serials production number was 997.  The special effects were created by the Lydecker brothers, Republics in-house effects team.

===Stunts=== David Sharpe as Red Ryder (doubling Don "Red" Barry)
*Duke Green
*Ted Mapes
*Post Park
*Ken Terrell
*Bill Yrigoyen
*Joe Yrigoyen

==Release==
===Theatrical===
The Adventures of Red Ryders official release date is 28 June 1940, although this is actually the date the sixth chapter was made available to film exchanges. 

==Chapter titles==
# Murder on the Santa Fe Trail (27min 48s)
# Horsemen of Death (16min 42s)
# Trails End (16min 41s)
# Water Rustlers (16min 39s)
# Avalanche (16min 44s)
# Hangmans Noose (16min 44s)
# Framed (16min 42s)
# Blazing Walls (16min 42s)
# Records of Doom (16min 42s)
# One Second to Live (16min 43s)
# The Devils Marksman (16min 41s)
# Frontier Justice (16min 44s)

 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 227
 }} 
 King of the Royal Mounted, also based on a comic strip. Republics standard pattern was two 12-chapter serials and two 15-chapter serials in each year.

==References==
 

==External links==
*  
*  


 
{{succession box Republic Serial Serial
| before=Drums of Fu Manchu (1940)
| years=Adventures of Red Ryder (1940) King of the Royal Mounted (1940)}}
{{succession box English Serial Serial
| before=Drums of Fu Manchu (1940)
| years=Adventures of Red Ryder (1940) King of the Royal Mounted (1940)}}
 


 
 

 
 
 
 
 
 
 
 
 