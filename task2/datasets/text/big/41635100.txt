Death Drives Through
{{Infobox film
| name = Death Drives Through
| image =
| image_size =
| caption =
| director = Edward L. Cahn
| producer = Clifford Taylor 
| writer = John Huston   Katherine Strueby   Gordon Wellesley Robert Douglas   Miles Mander   Percy Walsh
| music = Ernest Irving  Eric Cross James Wilson
| editing = Edward L. Cahn
| studio = Clifford Taylor Productions Associated British  
| released = March 1935
| runtime = 63 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} sports drama Robert Douglas independent producer Clifford Taylor at Ealing Studios. The racing scenes were shot at Brooklands. 

==Cast==
* Chili Bouchier as Kay Lord   Robert Douglas as Kit Woods 
* Miles Mander as Garry Ames  
* Percy Walsh as Mr. Lord  Frank Atkinson as John Larson 
* Lillian Gunns as Binnie

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The British of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 