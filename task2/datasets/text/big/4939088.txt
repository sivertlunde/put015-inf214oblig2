Away from Her
{{Infobox film
| name        = Away from Her
| image       = Away From Her.jpg
| director    = Sarah Polley Jennifer Weiss
| based on    =  
| writer     = Sarah Polley Michael Murphy Kristen Thomson Wendy Crewson Jonathan Goldsmith
| cinematography = Luc Montpellier
| editing = David Wharnsby
| studio      = Capri Releasing Echo Lake Productions Foundry Films Hanway Films The Film Farm
| distributor = Capri Releasing Pulling Focus Pictures Lionsgate Films (US)
| released    =  
| runtime     = 110 minutes 
| language    = English
| country     = Canada
| budget      = 
| gross       =$9,194,283 
}}
Away from Her is a 2006 Canadian film which debuted at the Toronto International Film Festival and also played in the Premier category at the 2007 Sundance Film Festival. The feature-length directorial debut of Canadian actress Sarah Polley, the film is based on Alice Munros short story "The Bear Came over the Mountain", from the 2001 collection Hateship, Friendship, Courtship, Loveship, Marriage. It was executive produced by Atom Egoyan (Polleys director in both Exotica (film)|Exotica and The Sweet Hereafter (film)|The Sweet Hereafter) and distributed by Lionsgate Films.
 Michael Murphy, Olympia Dukakis, Wendy Crewson, Alberta Watson, Lili Francks and Kristen Thomson. The film was shot primarily in Hamilton, Ontario|Hamilton,    with some location shooting in Brant, Ontario|Brant, Kitchener, Ontario|Kitchener, and Paris, Ontario.

==Plot==
Grant (Pinsent) and Fiona (Christie) are a retired married couple living in rural Brant County, Ontario.  Fiona begins to lose her memory, and it becomes apparent she suffers from Alzheimers disease. Throughout the film, Grants reflections on his marriage are woven with his reflections on his own infidelities, and influence his eventual decisions regarding Fionas happiness.

When she feels she is becoming a risk to herself, Fiona decides to check into a nursing home, where one of the rules is that a patient cannot have any visitors for the first 30 days, in order to "adjust". Wary of this policy, Grant agrees anyway, at the insistence of his wife whom he loves. During the drive to the home, Fiona acknowledges Grants past infidelity while he was a university professor. Despite the awkward situation, the couple makes love one last time before separating.
 coping partner" in the facility.

While seeing his wife grow closer to Aubrey, Grant becomes an unhappy voyeur when visiting his wife at the nursing home. As time goes by and Fiona still does not remember him, Grant even wonders whether Fionas dementia is an act, to punish him for his past indiscretions. After some time, Aubreys wife removes him from the home due to financial difficulties. This causes Fiona to sink into a deep depression (mood)|depression, with her physical wellbeing also appearing to deteriorate. Grant is touched by this, and visits Aubrey’s wife Marian (Dukakis) in an effort to allow Fiona to see Aubrey again. He would rather see his wife happy with another man than miserable and alone. Marian initially refuses, but the meeting leads to a tentative relationship between her and Grant.

As time passes, Grant continues to visit both Fiona and Marian. He eventually succeeds in taking Aubrey back to visit his wife. But in his "moment alone" before he brings Aubrey into Fionas room, Fiona temporarily remembers him and the love she has for him. The film closes on their embrace.

==Cast==
*Julie Christie as Fiona Anderson
*Gordon Pinsent as Grant Anderson Michael Murphy as Aubrey
*Nina Dobrev as Monica
*Olympia Dukakis as Marian
*Kristen Thomson as Kristy
*Wendy Crewson as Madeleine Montpellier
*Stacey LaBerge as Young Fiona
*Deanna Dezmari as Veronica
*Clare Coulter as Phoebe Hart
*Thomas Hauff as William Hart
*Alberta Watson as Dr. Fischer
*Grace Lynn Kung as Nurse Betty
*Lili Francks as Theresa

==Production== No Such Thing in Iceland when she read the Alice Munro short story "The Bear Came Over the Mountain" in The New Yorker. "I was so unbelievably moved by the story. I had just finished working with Julie Christie, and as I read, I kept seeing Julies face in the character of Fiona," said Polley. "I am certainly not one of those people who reflectively thinks about adapting stories; I just want to leave the things I love alone. But this fascinated me. I read the story and I saw the film and I knew what the film was."

At that point of Polleys career, she had been acting since the age of six, and had written and directed two short films, Dont Think Twice and The Best Day of my Life. "For two years, I couldnt get the story out of my head and finally asked producer Danny Iron to look into getting the rights. I threw myself into writing, but its daunting, taking on the work of somebody you respect so much. Alice Munro is one of my favorite writers because she looks right through things. The characters are all so flawed, so lovable in certain moments and so detestable in others. The adaptation didnt feel like a huge process because the film was embedded in that story."

Working alongside Polley were producers Jennifer Weiss, with whom she made her Genie-award winning short I Shout Love, and Simone Urdl, partners in the production company The Film Farm, and Daniel Iron of Foundry Films who produced Polleys first short Dont Think Twice. Atom Egoyan served as executive producer. Daniel Iron, having known Polley for a very long time, never doubted her ability to direct a feature. "I know how fiercely intelligent and diligent she is. Shes been on sets since she was young and knows the craft better than any first time director. She shot-listed her first draft of the script."

==Critical reception==
The film received universal acclaim from critics. As of January 6, 2008, the review aggregator Rotten Tomatoes reported that 95% of critics gave the film positive reviews, based on 128 reviews.  Metacritic reported the film had an average score of 88 out of 100 signifying universal acclaim based on 36 reviews. 

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2007. 

*1st - Best Drama, Rotten Tomatoes  Dana Stevens, Slate (magazine)|Slate
*2nd - Jack Mathews, New York Daily News
*3rd - Carrie Rickey, The Philadelphia Inquirer The Savages)
*4th - David Germain, Associated Press   
*4th - V.A. Musetto, New York Post
*5th - Christy Lemire, Associated Press 
*5th - Stephanie Zacharek, Salon.com|Salon
*6th - Marjorie Baumgarten, The Austin Chronicle
*6th - Roger Ebert, Chicago Sun-Times 
*7th - Kevin Crust, Los Angeles Times
*8th - David Ansen, Newsweek The Savages)
*9th - Liam Lacey and Rick Groen, The Globe and Mail

==Awards and nominations==

===Genie Awards=== Best Motion Best Director, Best Actor Best Actress Best Supporting Best Adapted Best Editing, which it lost to Eastern Promises.

Away From Her was the third film in Genie Award history, after Le Confessional and Atanarjuat, to win both the Claude Jutra Award and the Best Picture Genie in the same year.

===Academy Awards=== Best Actress Best Adapted Screenplay.   

===Others=== Best Performance Outstanding Performance by a Female Actor in a Leading Role at the 14th Screen Actors Guild Awards held in 2008,  and was nominated for the Best Leading Actress BAFTA at the 61st British Academy Film Awards. 

==Criticism on the adaptation==
* Agnès Berthin-Scaillet,  , in: Journal of the Short Story in English (JSSE)/Les cahiers de la nouvelle, ISSN 0294-0442, n° 55 (Autumn 2010).
* McGill, Robert, "No Nation but Adaptation: The Bear Came over the Mountain, Away from Her, and What It Means to Be Faithful", in: Canadian Literature/Littérature canadienne, 2008 Summer; 197: 98–111.

=== Other ===

* Demetrios Matheou, "Not Remembering to Forget", in: Sight and Sound, 2007 May; 17 (5): 12. (Interview)
* Danny Munso, "Away from Her", in: Creative Screenwriting, 2007 Mar–Apr; 14 (2): 30.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*   from the Washington Post (subscription required)
*   from CBC Arts
*   from Macleans magazine
*  

 
 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 