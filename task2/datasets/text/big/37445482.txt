The Hoodlum (1919 film)
{{infobox film
| name           = The Hoodlum
| image          =The Hoodlum 01.png
| imagesize      =180px
| caption        =Opening credits Sidney Franklin
| producer       = Mary Pickford
| starring       = Mary Pickford
| writer         = 
| based on       =  
| cinematography = Charles Rosher
| editing        = Edward McDermott
| distributor    = First National
| released       =   reels (6,462 feet) 
| country        = United States
| language       = Silent film English intertitles
}} Sidney A. Franklin and was based on the novel Burkeses Amy by Julie Matilde Lippman.    

==Plot== Ralph Lewis), in his Fifth Avenue, New York City mansion. She is initially delighted when he offers to take her with him on a trip to Europe. However, as the day approaches for their departure, she changes her mind and decides to go live with her newly returned father, "sociological writer" John Burke (T. D. Crittenden), at Craigen Street, wherever that is. Unused to having his plans thwarted, Guthrie becomes cold to his beloved granddaughter.
 Andrew Arbuckle) and Jew Abram Isaacs (Max Davidson) through good-natured trickery.

 
When a policeman is alerted by a sore loser to her game of craps in the street, she escapes by hiding under the cloak of newcomer Peter Cooper, who takes a room on the floor above the Burkes. Unbeknownst to Amy, the new resident is actually her grandfather in disguise, come to see how she is doing. He is initially disgusted with her behavior, noting on paper that she "has become a hoodlum". When Amy takes a sick mother and her children under her wing, she asks Cooper to look after a baby, only to be brusquely rebuffed. Cooper has a change of heart, however, and adopts a whole new, more benevolent attitude, much to Amys delight. He returns to his mansion a changed man (taking along Dish Lowry).

One night, Amy spots a thief in Turners room. The intruder flees. Turner informs Amy that it was no thief but an agent of Alexander Guthrie looking for his writings. Guthrie framed him to hide corrupt business practices, resulting in a year in the penitentiary. Amy and Turner break into her grandfathers mansion to try to steal evidence that would prove him innocent, but set off a burglar alarm and are caught. When Guthrie recognizes Amy, he has Turner freed and offers to exonerate him. Afterward, Amy and Turner are married.

==Cast==
*Mary Pickford as Amy Burke Ralph Lewis as Alexander Guthrie / "Peter Cooper"
*Kenneth Harlan as William Turner 
*T. D. Crittenden as John Burke
*Aggie Herring as Nora Andrew Arbuckle as Pat OShaughnessy
*Max Davidson as Abram Isaacs
*Paul Mullen as The Pugilist
*Buddy Messinger as Dish Lowry

==Public service announcement== war savings stamps. 

==Home media==
The film is in the public domain.  It has been released on DVD and Blu-ray. 

==See also==
*Mary Pickford filmography

==References==
 

==External links==
 
*  
*  
*  
*  
*   available for download at Internet Archive
*  

 

 
 
 
 
 
 
 
 