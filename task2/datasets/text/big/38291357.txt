Linked by Fate (film)
{{Infobox film
| name           = Linked by Fate
| image          =
| caption        = Albert Ward
| producer       = G.B. Samuelson
| writer         = Charles Garvice (novel)   Albert Ward Bernard Vaughan   Esme Hubbard
| music          = 
| cinematography = 
| editing        = 
| studio         = G.B. Samuelson Productions
| distributor    = General Film Distributors
| released       = August 1919
| runtime        = 6 reels 
| country        = United Kingdom
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama Albert Ward Bernard Vaughan. Linked by Fate by Charles Garvice.

==Cast==
* Isobel Elsom as Nina Vernon
* Malcolm Cherry as Vane Mannering Bernard Vaughan as Dr. Vernon
* Esme Hubbard as Polly Bamford Clayton Green as Julian
* Manning Haynes as Lord Sutcombe
* Elaine Inescourt as Juliet Orme
* Barbara Gott as Deborah
* Ernest A. Douglas as Reverend Fleming

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 

 
 
 
 
 
 
 
 
 


 