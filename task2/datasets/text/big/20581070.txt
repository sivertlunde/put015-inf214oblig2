Jaane-Anjaane
{{Infobox film
| name           = Jaane-Anjaane
| image          = Jaane-Anjaane.jpg
| image_size     = 
| caption        = 
| director       = Shakti Samanta 
| producer       = 
| writer         =
| narrator       = 
| starring       = Shammi Kapoor  Vinod Khanna   Leena Chandavarkar
| music          = Shankar Jaikishan
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1971
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1971 Bollywood drama film directed by Shakti Samanta .  The film stars Shammi Kapoor, Vinod Khanna and Leena Chandavarkar. 

==Cast==
*Shammi Kapoor ...  Ram Prasad Ramu 
*Leena Chandavarkar ...  Mala 
*Sandhya Roy ...  Koyli 
*Vinod Khanna ...  Police Inspector Hemant  Helen ...  Suzie 
*Sulochana Latkar ...  Shobha (as Sulochana) 
*Lalita Pawar ...  Laxmi 
*K.N. Singh ...  Poonamchand  Dhumal ...  Dhondu 
*Sajjan ...  Hemants Father 
*Gulshan Bawra ...  Munnu 
*Birbal ...  Chunnu 
*Murad (actor)|Murad...  Judge

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jane Anjane Log Mile"
| Kishore Kumar
|-
| 2
| "Chham Chham Baje Re Payaliya"
| Manna Dey
|-
| 3
| "Aaya Apni Nagariya Mein"
| Manna Dey, Suman Kalyanpur
|-
| 4
| "Jane Anjane Yahan Sabhi Hai" Sharda
|-
| 5
| "Mohe Jaal Mein Phansae Liyo"
| Lata Mangeshkar
|-
| 6
| "Ram Kasam Bura Na Manoongi"
| Asha Bhosle
|-
| 7
| "Teri Neeli Neeli Ankhon Ki"
| Mohammed Rafi, Lata Mangeshkar
|}
==External links==
*  

 
 

 
 
 
 
 