Interference (film)
{{Infobox film
| name           = Interference silent version) sound version)
| writer         = Roland Pertwee (play) Howard Dearden (play) Louise Long Hope Loring (adaptation) Ernest Pascal (dialogue) Julian Johnson (titles)
| narrator       =
| starring       = William Powell Evelyn Brent
| music          = W. Franke Harling J R. Hunt
| editing        = George Nichols Jr.
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 10 reels
| country        = United States
| language       = English Also silent version with English intertitles
| budget         =
| gross          =
}}
 sound film talking movie. It was also simultaneously filmed as a silent. The film was based on the play Interference, a Play in Three Acts by Roland Pertwee and Howard Dearden. When a first husband turns out not to be dead, blackmail leads to murder. 

The silentera.com website has stage actress Ruth Chatterton and theatrical impresario Daniel Frohman as appearing in this film.

==Cast==
*William Powell as Philip Voaze
*Evelyn Brent as Deborah Kane
*Clive Brook as Sir John Marlay
*Doris Kenyon as Faith Marlay
*Tom Ricketts as Charles Smith
*Brandon Hurst as Inspector Haynes
*Louis Payne as Childers
*Wilfred Noy as Dr. Gray Donald Stuart as Freddie Raymond Lawrence as Reporter
*Clyde Cook as Hearse Driver

uncredited
*Ruth Chatterton
*Daniel Frohman

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 