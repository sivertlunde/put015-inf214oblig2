Babumon (film)
{{Infobox film
| name = Babumon
| image =
| image_size =
| caption = Hariharan
| producer = GP Balan
| writer = S. L. Puram Sadanandan
| screenplay = S. L. Puram Sadanandan
| starring = Prem Nazir Jayabharathi Srividya Adoor Bhasi
| music = M. S. Viswanathan
| cinematography = T. N. Krishnankutty Nair
| editing = V. P. Krishnan
| studio = Chanthamani Films
| distributor = Chanthamani Films
| released =  
| country = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, Hariharan and produced by GP Balan. The film stars Prem Nazir, Jayabharathi, Srividya and Adoor Bhasi in lead roles. The film had musical score by M. S. Viswanathan.   The film was a remake of Hindi film Door Gagan Ki Chhaon Main.

==Cast==
  
*Prem Nazir 
*Jayabharathi 
*Srividya 
*Adoor Bhasi 
*Thikkurissi Sukumaran Nair 
*Jose Prakash 
*Pattom Sadan 
*Sankaradi 
*Sreelatha Namboothiri 
*Sunil
*T. S. Muthaiah 
*Prathapachandran 
*Azhikkode Balan
*Bahadoor 
*Benny the dog
*K. P. Ummer 
*Madamana Subramanyam Master Raghu 
*Vellur P Chandrasekharan
*Vinayaraj 
 

==Soundtrack==
The music was composed by M. S. Viswanathan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Indraneelam Choriyum || K. J. Yesudas || Mankombu Gopalakrishnan || 
|- 
| 2 || Naadan Paattinte || K. J. Yesudas || Mankombu Gopalakrishnan || 
|- 
| 3 || Padmatheerthakkarayil || Vani Jairam || Mankombu Gopalakrishnan || 
|- 
| 4 || Padmatheerthakkarayil || P. Susheela|Susheela, Jayachandran || Mankombu Gopalakrishnan || 
|- 
| 5 || Raksha Daivatham   || K. J. Yesudas, Jayachandran, Chorus || Mankombu Gopalakrishnan || 
|-  Susheela || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 
 
 

 