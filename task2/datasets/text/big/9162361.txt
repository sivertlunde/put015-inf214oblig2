The Good Old Soak
{{Infobox film
| name           = The Good Old Soak
| image          = File:Good Old Soak 1937.JPG
| image size     = 
| alt            = The Good Old Soak lobby card
| caption        = Lobby card
| director       = J. Walter Ruben
| producer       = Harry Rapf
| writer         = A.E. Thomas
| screenplay     = 
| story          =
| based on       =  
| narrator       = 
| starring       = Wallace Beery Una Merkel Eric Linden Judith Barrett
| music          = Edward Ward
| cinematography = Clyde De Vinna
| editing        = Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    = 
| released       =  
| runtime        = 67 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

The Good Old Soak is a 1937 drama film directed by J. Walter Ruben, with a screenplay by A. E. Thomas based upon the stage play of the same name by Don Marquis.  The film stars Wallace Beery with a supporting cast of Una Merkel, Eric Linden, Betty Furness, and Ted Healy.

Previously made as a silent film by Universal in 1926 called The Old Soak.

==Cast==
* Wallace Beery as Clem Hawley
* Una Merkel as Nellie
* Eric Linden as Clemmie Hawley
* Judith Barrett as Ina Heath
* Betty Furness as Lucy Hawley
* Ted Healy as Al Simmons
* Janet Beecher as Matilda Hawley
* George Sidney as Kennedy
* Robert McWade as Webster Parsons James Bush as Tom Ogden  Margaret Hamilton as  Minnie

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 