Alien from L.A.
{{Infobox Film
| name = Alien from L.A.
| image = Alien from LA.jpg
| image_size =
| caption = One-sheet for Alien from L.A.
| director = Albert Pyun
| producer = Yoram Globus
| writer = Regina Davis Albert Pyun Debra Ricci
| narrator =
| starring = Kathy Ireland William R. Moses Richard Haines Don Michael Paul Thom Mathews Janet Du Plessis Simon Poland
| music = Jim Andron Simon LeGassick Anthony Riparetti James Saad
| cinematography = Tom Fraser
| editing = Daniel Loewenthal MGM (current)
| released = February 26, 1988
| runtime = 87 min
| country = United States English
| budget =
| gross =
| preceded_by =
| followed_by = 
}}

Alien From L.A. is a 1988 science fiction film that stars Kathy Ireland as a young woman who visits the underground civilization of Atlantis. The film was featured on Mystery Science Theater 3000.

==Plot==
Wanda Saknussemm (Ireland) is a nerdy social misfit with large glasses and a squeaky voice who lives in Los Angeles and works at a diner. After being dumped by her boyfriend for "not having a sense of adventure", Wanda is informed by a letter that her father, an archaeologist, has died. She flies to northern Africa and while going through her fathers belongings, she finds his notes about Atlantis, apparently an alien ship that crashed millennia ago and sank into the center of the Earth. Wanda comes across a chamber beneath her fathers apartment and accidentally sets off a chain of events that ultimately cause her to fall into a deep hole.

An unharmed Wanda wakes up deep within the Earth to find Gus (William R. Moses), a miner whom she protects from being slain by two people. Gus, who has a very inconsistent Australian accent, agrees to help Wanda find her father, whom she believes is alive and trapped underground. Wanda soon discovers that both she and her father are believed to be spies planning an invasion of Atlantis. During her adventures, Wandas appearance changes from nerdy to attractive (by removing her glasses and using a steam vent to clean her skin). People from the surface world are referred to as "aliens" by Atlanteans, and when Wanda is overheard talking about Malibu Beach by a low-life informant (Janie Du Plessis), she soon becomes a hunted woman and must dodge efforts at capture, both from the mysterious "Government House" and from thugs in the pay of the crime lord Mambino (Deep Roy). Much mention of Wandas "big bones" are made during these sequences.

Wandas efforts at escape are aided by Charmin (Thom Matthews), a handsome rogue who (briefly) assists her flight and falls for Wanda. She is ultimately captured by the evil General Pykov (Du Plessis again), who wants to kill both Wanda and her incarcerated father. The Atlantean leader decides to free Wanda and her father, provided they remain quiet about Atlantis. Gus shows up and helps the duo escape while fighting off General Pykov and her soldiers. Wanda and her father board a ship that takes them back to the surface and the film ends with Wanda on the beach, wearing a bikini and a sarong. She refuses the advances of her ex-boyfriend and is soon reunited with Charmin, who inexplicably appears on a motorcycle.

==Cast==
*Kathy Ireland as Wanda Saknussemm
*William R. Moses as Gus
*Richard Haines as Arnold Saknussemm
*Don Michael Paul as Robbie
*Thom Matthews as Charmin
*Janet Du Plessis as General Rykov / Shank / Claims Officer
*Simon Poland as Consul Triton Crassus / The Mailman
*Linda Kerridge as  Roeyis Freki / Auntie Pearl
*Kristen Trucksess as  Stacy
*Lochner De Kock as  Professor Ovid Galba / Prof. Paddy Mahoney
*Deep Roy as Mambino

==Production==

The film was mostly shot in Johannesburg, at producer Avi Lerners studio, plus additional shooting in Durban, South Africa and Swakopmund, Namibia. Locations ranged from South Africas deep digging mines and gold fields both on the outskirts of Johannesburg. There was one additional day of shooting at a safari complex near Pretoria. While scouting and shooting, director Albert Pyun felt acutely uncomfortable as a non-white being the head of a largely white crew. The company flew to Namibia in a DC-30 cargo plane which landed on a dirt landing strip outside Windhoek, Namibia. The most of the Namibia shoot took place in and around the old German colonial town of Swakopmund, Namibia. Company also shot along the famed Skeleton coast of Namibia.

==Reception==
The film was featured in a 1993 episode of Mystery Science Theater 3000.  This episode was released in March 2013 by Shout! Factory.

== References ==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 