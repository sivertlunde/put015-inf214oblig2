Without Warning (1980 film)
{{Infobox film
| name           = Without Warning
| image          = Without warning 1980 movie poster.jpg
| image size     = 215px
| caption        = theatrical release poster
| director       = Greydon Clark
| producer       = Greydon Clark  Lyn Freeman  Daniel Grodnik
| writer         = Lyn Freeman  Daniel Grodnik  Ben Nett  Steve Mathis
| narrator       =
| starring       = Jack Palance  Martin Landau Tarah Nutter David Caruso Kevin Peter Hall
| cinematography = Dean Cundey
| music          = Dan Wyman
| editing        = Curt Burch
| distributor    = World Amusement Partnership Nr.108
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $150,000
| gross          =
}} Jurassic Park, Hook (film)|Hook and Titanic (1997 film)|Titanic, created the aliens for this low-budget film. The film released on September 26, 1980 and was released on home video for the first time on August 5, 2014 through Shout! Factorys Scream Factory label in a Blu-ray/DVD Combo Pack.

Without Warning is credited with being an inspiration for the 1987 film Predator (film)|Predator, both of which starred Kevin Peter Hall as a costumed alien hunter.   

==Plot==

A father and son go hunting in the mountains. Before they can begin hunting, which the son does not want to do anyway, they are killed by flying jellyfish-like creatures, which penetrate their skin with needle-tipped tentacles.

Some time later, four teenagers, Tom, Greg, Beth and Sandy, hike in the same area, ignoring the warnings of local truck stop owner Joe Taylor (Jack Palance). A group of cub scouts is also in the area; their leader (Larry Storch) is also killed by the alien creatures, while his troop runs into an unidentified humanoid and flee.

The teenagers set up camp at a lake, but after a few hours, Tom and Beth disappear. Sandy and Greg go looking for them and discover their bodies in an abandoned shack. They drive away in their van, while being attacked by one of the starfish which tries to get through the cars windshield. After they get rid of it, they arrive at the truck stop. Greg tries to get help from the locals, but they do not believe him, except for Fred Sarge Dobbs (Martin Landau), who is a mentally ill veteran. Meanwhile, Sandy encounters the humanoid and flees into the woods, where Joe Taylor finds and returns her to Greg.

While they discuss the situation, the sheriff arrives, but Sarge shoots him and begins to become more Paranoia|paranoid. Greg and Sandy leave with Taylor, who reveals he has been attacked by the humanoid before and secretly keeps the flying jellyfish as trophies. They search for the shack and once there, Taylor goes inside to only find the bodies of Tom, Beth and the cub scout leader. They discuss waiting for the creature when Taylor is attacked by another "jellyfish". The young people run once again, leaving him behind as ordered. They stop a police car and get into the back seat, but find Sarge driving. He abducts them, believing them to be Extraterrestrial life|aliens. Greg plays along, telling the deranged man that an invasion force is on the way, thus distracting him enough to toss him aside, run away with Sandy and jump from a bridge.

They make it to a house where they find new clothing and try to relax. In the night, Sandy wakes up and goes looking for Greg, only to discover that he has been killed by the alien, who is still in the room. She flees to the basement and the creature is about to get her when Taylor arrives and saves her. On the way to the shack, he tells her about the creature: it is a tall extraterrestrial (Kevin Peter Hall) who hunts humans for sport to keep as trophies, using the living creatures as living weapons against its prey.

They wait at the shack to ambush the hunter with dynamite when Sarge shows up, almost spoiling their plan. He and Taylor fight, and Sandy is about to hit Sarge from behind when the alien arrives and kills Sarge. Taylor then shoots the creature, with little to no effect. Realizing the last chance of success, he lures it to the shack, which is then blown up by Sandy. She alone survives the horrible night.

== Cast ==

* Tarah Nutter as Sandy
* Christopher S. Nelson as Greg
* Jack Palance as Joe Taylor
* Martin Landau as Fred Sarge Dobbs
* Neville Brand as Leo
* Ralph Meeker as Dave Cameron Mitchell as Hunter
* Darby Hinton as Randy
* David Caruso as Tom
* Lynn Thell as Beth
* Sue Ane Langdon as Aggie
* Larry Storch as Cub Scout Leader
* Kevin Peter Hall as The Alien

== Reception==
 
Dread Central gave the home release of Without Warning 3 1/2 out of 5 blades, singling out the Blu-rays special features out as a highlight and giving them 4 out of 5 blades.  DVD Talk recommended the movie, writing "Without Warning may not be a lost masterpiece but it is a really entertaining low budget horror picture that makes the most of its effects set pieces and a few notable cast members."  The AV Club praised the performances of Landau and Palance but stated that they found the overall film "dull". 

== References==
 

== External links ==

*  
*  

 

 
 
 
 
 
 
 
 