The Illusionist (1983 film)
{{Infobox film
| name           = The Illusionist
| image          = 
| image_size     = 
| caption        = 
| director       = Jos Stelling
| producer       = Jos Stelling
| writer         = Freek de Jonge Jos Stelling
| narrator       = 
| starring       = 
| music          = Willem Breuker
| cinematography = Theo van de Sande
| editing        = Rimko Haanstra
| studio         = Jos Stelling Filmprodukties
| distributor    = 
| released       = 6 October 1983
| runtime        = 90 minutes
| country        = Netherlands
| language       = 
| budget         = 
}} Dutch comedy film directed by Jos Stelling and starring Freek de Jonge. The film has no dialogue. It won the Golden Calf for Best Feature Film at the 1984 Netherlands Film Festival. 

==Cast==
* Freek de Jonge as the illusionist
* Jim van der Woude as brother
* Catrien Wolthuizen as mother
* Gerard Thoolen as father
* Carel Lapere as grandfather
* Craig Eubanks as magician
* Gerrie van der Klei as assistant

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 
 