The Sea Urchin (1926 film)
{{Infobox film
| name           = The Sea Urchin
| image          =
| caption        =
| director       = Graham Cutts
| producer       = Michael Balcon Charles Lapworth
| starring       = Betty Balfour George Hackathorne Haidee Wright
| music          = 
| cinematography = 
| editing        = 
| studio         = Gainsborough Pictures
| distributor    = Woolf & Freedman Film Service 
| released       = 31 January 1926
| runtime        = 86 minutes
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British drama film directed by Graham Cutts and starring Betty Balfour, George Hackathorne and W. Cronin Wilson.  It was made at Gainsborough Studios with Michael Balcon as producer.

==Cast==
* Betty Balfour - Fay Wynchbeck 
* George Hackathorne - Jack Trebarrow 
* W. Cronin Wilson - Rivoli 
* Haidee Wright - Minnie Wynchbeck 
* Marie Wright - Mary Wynchbeck 
* Cecil Morton York - Sir Trevor Trebarrow 
* Clifford Heatherley - Sullivan 
* A.G. Poulton - Janitor

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 


 