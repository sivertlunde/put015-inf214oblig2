Savitri (1937 film)
 
 
{{Infobox film
| name           = Savitri
| image          =  
| image_size     = 
| caption        = 
| director       = Franz Osten
| producer       = Bombay Talkies
| writer         = Niranjan Pal J. S. Casshyap  (dialogue) 
| narrator       = 
| starring       = Devika Rani Ashok Kumar Maya Devi Mumtaz Ali Saraswati Devi
| cinematography = Joseph Wirsching
| editing        = 
| distributor    = Ramniklal Mohanlal and Co., Bombay
| studio         = Bombay Talkies
| released       = 1937
| runtime        = 136 minutes
| country        = British India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1937 mythology Hindi film directed by Franz Osten.       Adapted from  a story in the Mahabharata the writers were Niranjan Pal with dialogues by J. S. Casshyap. The cinematographer was Joseph Wirsching. Saraswati Devi composed the music.    According to Garga,  Savitri was the "only mythological" produced by Himanshu Rai for Bombay Talkies, who were known for making "rurlist reform dramas".        
Ashok Kumar and Devika Rani played the lead roles supported by Maya Devi, Saroj Borkar, Mumtaz Ali, and Sunita Devi.   

The film is based on an incident from the epic Mahabharata and tells the story of Savitri and Satyavan, played by Devika Rani and Ashok Kumar. Savitri persists in getting the god of death, Yama, to revoke her husband Satyavans death.

==Plot==
Savitri is born as a boon from the Sun God, to Ashwapati, the King of Madra and his wife Malawi. Savitri meets and falls in love with Satyavan, the son of the blind King Dumatasena. Dumtasenas Minister has taken over his kingdom and exiled the king, who now lives in the forest. When Savitri decides to marry Satyavan, the sage Narada advices against it, as according to the planetary charts, Satyavan will die a year following their marriage. However, Savitri insists on going ahead, ready to take on Yama the God of death. A year later when Savitri sees Yama  carrying  Satyavans soul she follows and discourses with him, while cotinuing her pursuit. Finally, Yama tells her she can ask three boons of him except for Satyavans life. The boons Savitri asks for are: sight for her father-in-law and restoration of his kingdom, her father should be granted a hundred sons, and last, she be granted a hundred sons. Yama gives in to her request, thereby restoring Satyavans life. Her father-in-laws sight is restored and he learns of the death of his usurper.

==Cast==
* Devika Rani
* Ashok Kumar
* Kamta Prashad
* Chandraprabha
* P. F. Pithawala
* Mumtaz Ali
* Sunita Devi
* Vimala Devi
* Maya Devi
* Sushila
* Aloka
* Madhurika Devi


==Savitri In Indian Cinema==
There were about thity-four versions of the Savitri Satyavan films made. One of the earliest was the film Svitri produced in 1913 by  ].    The 1923 version, Savitri also called Satyavan Savitri, was an Italian co-production directed by Giorgio Mannini and J. J. Madan produced by Madan Theatres Ltd. and Cines.    Savitri (1933) was the first film produced by East India Film Company. Directed by C. Pullaiah, it received an Honorary Certificate at the Venice Film Festival.   


==Soundtrack==
Music was composed by Saraswati Devi with lyrics by J. S. Casshyap.The singers were Devika Rani, Ashok Kumar, Sushila Bhiwandkar, Chandraprabha, P. F. Pithawala,    

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|- 
| 1
| Uttho Uttho Begi Uttho Sajanwa 
| Devika Rani
|-
| 2
| Surya Wahi Chandra Wahi
| Ashok Kumar
|-
| 3
| Koi Aandhi Ko Rok Sake Rok Le 
| Sushila Bhiwandkar
|-
| 4 Surya Kiran Navjivan Laayi
| Chandraprabha
|-
| 5
| Saath Saath Jaayenge
| Chandraprabha
|-
| 6
| Wahan Der Sahi Andher Nahin
| P. F. Pithawala
|-
| 7
| Yo Suno Geet Sangeet Manohar
| Ashok Kumar
|-
| 8
| Devi Devi Sur Nar Muni
|
|-
| 9
| Dhanya Hua Dhany Hua Tav Kaaran Aaj
|
|-
| 10
| Jab Koyal Ku Ku Karti Kya Veena Bhi Sur Bharti
| Sushila Bhiwandkar
|-
| 11
| Jal Bharan Aayi Gujariya
| Ashok Kumar
|-
| 12
| Hari Jab Jab Bhagat Par Bheer Padi
| P. F. Pithawala
|}
 
==References==
 

==External links==
*  

 

 
 
 
 
 