Crime in the Streets
 
{{Infobox film
| name           = Crime in the Streets
| image          = Crime in the Streets poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Don Siegel
| writer         = Reginald Rose
| narrator       =
| starring       = James Whitmore John Cassavetes Sal Mineo
| music          = Franz Waxman
| cinematography = Sam Leavitt
| editing        = Allied Artists
| released       =  
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         =
| gross          = $1.2 million (US)  
| website        =
| amg_id         =
}}
Crime in the Streets is a 1956 film about juvenile delinquency, directed by Don Siegel and based on a television play written by Reginald Rose. The play first appeared on the Elgin Hour and was directed by Sidney Lumet. 

The film, starring James Whitmore and John Cassavetes, also featured actor Sal Mineo, who had previously appeared in Rebel Without A Cause.  From his role in Crime in the Streets, Mineo earned a Hollywood nickname, "The Switchblade Kid." Malcolm Atterbury, Virginia Gregg and future director Mark Rydell had prominent roles.    

Siegel adapted the play to a film by expanding some sequences but keeping much of the same cast.  His credited dialogue coach on the film was Sam Peckinpah.

==Plot==
After a rumble between New York City street gangs, the Hornets and Dukes, a youth is taken captive and threatened with a zip gun by Lenny Daniels, one of the Hornets. The act is witnessed by a neighbor, McAllister, who tells the cops.

Lenny is arrested and sentenced to a year in jail. Hornets leader Frankie Dane decides to get even. Seemingly incorrigible, 18-year-old Frankie resists all efforts to get through to him by social worker Ben Wagner or his worried mother, who was abandoned by Frankies father when he was eight.

Frankie threatens McAllister, who isnt afraid of Frankie. McAllister even slaps him, then walks away. An angry Frankie then enlists friends Lou Macklin and Angelo "Baby" Gioia to assist in killing McAllister, which frightens Frankies 10-year-old brother Richie, who overhears the plotting. 

Babys dad slaps Baby and orders, then pleads with him to stop hanging out with the no-good Frankie. An effort is made by Wagner to understand the boys rather than be angry with them, and Richie tells him of Frankies plans to commit a murder. Wagner talks to Frankie, seemingly to no avail. The three conspirators fake going to bed, (to later use as their alibi) to wait until the agreed upon time to act. McAllister is trapped in an alley at 1:30 in the morning by the three. Richie stops his brother just-in-time, but ends up with a knife held to his throat by angry Frankie, while McAllister and other two run off, as the intended victim yells for help.
Wagner appears due to the commotion, and watches as Frankie finally comes to his senses and lets his brother go. He is then accompanied by Wagner to the approaching police.

==Cast==
* James Whitmore as Ben Wagner
* John Cassavetes as Frankie Dane
* Sal Mineo as Angelo "Baby" Gioia
* Virginia Gregg as Mrs. Dane
* Malcolm Atterbury as McAllister
* Mark Rydell as Macklin

==DVD release==
Warner Bros. released the film on DVD on July 13, 2010, in its Film Noir Classic Collection, Vol. 5. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 