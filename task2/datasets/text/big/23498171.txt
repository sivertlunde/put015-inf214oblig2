Wife or Country
 
{{Infobox film
| name           = Wife or Country
| image          = Wife or Country (1918) - 1.jpg
| caption        = Film still
| director       = E. Mason Hopper
| producer       =
| writer         = Harry Mestayer Charles J. Wilson
| starring       = Gloria Swanson
| music          =
| cinematography = Clyde Cook
| editing        =
| distributor    = Triangle Film Corporation
| released       =  
| runtime        =
| country        = United States
| language       = Silent (English intertitles)
| budget         =
}}
 silent drama film directed by E. Mason Hopper and starring Gloria Swanson in her last role for Triangle Film Corporation.    Its survival status is classified as unknown, suggesting that it is a lost film. 

==Plot==
Based upon a review in a film magazine,  Gretchen (Lederer) has reclaimed husband Dale Barker (Mestayer) from a drinking habit, and he has become a lawyer working with the Department of Justice in hunting down German propagandists. Before their marriage, Gretchen had been involved with propagandists and cannot now extricate herself, and she fears discovery by her husband. She is jealous of Sylvia Hamilton (Swanson), Dales stenographer, and plans on bringing her under suspicion. When arrested as a spy, Sylvia turns over a mass of evidence, some of which incriminates Gretchen, placing Dale between love of country and loyalty to his wife.

==Cast==
* Harry Mestayer as Dale Barker
* Gretchen Lederer as Gretchen Barker
* Gloria Swanson as Sylvia Hamilton Jack Richardson as Dr. Meyer Stahl Charles West as Jack Holiday

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 