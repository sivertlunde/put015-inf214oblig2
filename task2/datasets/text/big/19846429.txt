The Secret of the Black Widow
{{Infobox film
| name           = The Secret of the Black Widow
| image          = 
| caption        = 
| director       = Franz Josef Gottlieb
| producer       = Alfons Carcasona
| writer         = Alexandra Becker Rolf Becker José Luis Guarner
| starring       = O. W. Fischer
| music          = 
| cinematography = Godofredo Pacheco
| editing        = Anni Lautenbacher
| distributor    = 
| released       = 28 November 1963
| runtime        = 110 minutes
| country        = Germany 
| language       = German
| budget         = 
}}

The Secret of the Black Widow ( ) is a 1963 German crime film directed by Franz Josef Gottlieb and starring O. W. Fischer.   

==Cast==
* O. W. Fischer - Wellby
* Karin Dor - Clarisse
* Klaus Kinski - Boyd
* Werner Peters - Mr. Shor
* Doris Kirchner - Mrs. Shor
* Eddi Arent - Fish
* Claude Farell - Mrs. Ayke
* Gabriel Llopart - Selwood
* Fernando Sancho - Slim
* José María Caffarel - Cartwright (as Josef Cafarell)
* Antonio Casas - Bronsfield
* Tomás Blanco (actor)|Tomás Blanco - (uncredited)
* Félix Dafauce - Inspector Terry (uncredited)
* Cris Huerta - Slim (uncredited)
* Ángel Menéndez - (uncredited)
* George Rigaud - (uncredited)

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 