Los Andes no creen en Dios
{{Infobox film
| name           = Los Andes no creen en Dios
| image          = Los Andes no creen en Dios cover.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Antonio Eguino
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Diego Bertie, Carla Ortiz and Milton Cortés
| music          = 
| cinematography = Ernesto Fernandez
| editing        = 
| studio         = Laboratorios Sonido Filmoso do Santiago de Chile, Imagen Technicolor Montreal, Canada
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Bolivia
| language       = Spanish
| budget         = 
| gross          = 
}}
Los Andes no creen en Dios (The Andes do not believe in God) is a 2007 Bolivian film directed by Antonio Eguino. 
It is a drama set in a mining town in the Andes in the 1920s.

==Synopsis==

The film is loosely based on the 1974 novel Los Andes no creen en Dios by Adolfo Costa du Rels and two short stories by the same author, La Miskisimi (Sweet Lips in Quechua languages|Quechua) and Plata del diablo (The Devils Silver). {{cite web |ref=harv
 |url=http://boliviasol.wordpress.com/2007/06/14/los-andes-no-creen-en-dios/
 |last=Montoya |first=Victor
 |title=Los Andes no creen en Dios
 |date=14 June 2007 
 |accessdate=2012-07-07}} 

The film is set in the late 1920s in the small and remote mining town of Uyuni in the highlands of Bolivia, lying between a vast salt desert and towering mountains.
The hero is Alfonso Claros, a young writer who has been educated in France and who arrives in Uyuni seeking to make his fortune in mining. 
He begins an affair with the sultry mestizo Claudina, but breaks it off due to local prejudice.
Claudina turns to Joaquín, a friend of Alfonso, destroying the friendship between the two men.
Alfonso befriends Clota, a former singer and now the local brothel keeper. 

Alfonso joins Genaro, a mystic and expert prospector, on a search for a gold vein in the mountains, and the quest turns out to be a turning point in both mens lives.
Returning to Uyuni, Alfonso finds that Claudina and Clota have aroused the intolerance of the priest and the local women, who have embarked on a campaign against them.
Sickened by events, Alfonso leaves Uyuni.
Twenty years later, Alfonso is on the way to take up the position of Bolivian ambassador to France, when his train stops over in Uyuni.
He meets Joaquín by accident, and as he remembers Claudina is confirmed in his feelings about Joaquín. 

==Production==

Planning for the film started in 1996, but was postponed for several years. 
The screenplay was finalized in 2003 and 2004, filming was done in 2005, and editing, sound, music and post production completed in 2006 and early 2007, 
with the film ready for release in March 2007.
Over $500,000 was spent on production, a large amount for Bolivia, caused in part by the number of locations and the historical sets with period cars and steam locomotives. 

==Reception==

Los Andes no creen en Dios was Bolivias submission to the 80th Academy Awards for the Academy Award for Best Foreign Language Film. {{cite web
 |url=http://www.cfi-icf.ca/index.php?option=com_cfi&task=showscreening&id=39
 |title=THE ANDES DONT BELIEVE IN GOD (LOS ANDES NO CREEN EN DIOS)
 |publisher=CANADIAN FILM INSTITUTE
 |accessdate=2012-07-07}}  
It was not accepted as a nominee.         
The film was nominated for the Best Film Award at the Cartagena Film Festival. 

==Cast==

*Diego Bertie - Alfonso Claros
*Carla Ortiz - Claudina Morales
*Milton Cortés - Joaquin Avila
*Jorge Ortiz Sánchez - Genaro
*Geraldine Chaplin - Clota

==See also==
* List of Bolivian submissions for the Academy Award for Best Foreign Language Film
* List of submissions to the 80th Academy Awards for Best Foreign Language Film

==References==
{{reflist |refs=
 {{cite web
 |url=http://dclatinfilm.org/2009/films/losandesnocreen.php
 |work=DC Latin America Showcase
 |title=THE ANDES DON’T BELIEVE IN GOD (Los Andes no Creen en Dios)Bolivia
 |accessdate=2012-07-07}} 
 {{cite web
 |url=http://www.uyuniweb.com/uyuni-ciudad/pelicula-filmada-en-uyuni.php
 |work=Uyuniweb
 |title=Los Andes no creen en Dios
 |accessdate=2012-07-07}} 
}}

==External links==
* 

 
 
 
 
 