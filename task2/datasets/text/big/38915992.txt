A Wedding Invitation
{{Infobox film
| name           = A Wedding Invitation
| image          = A Wedding Invitation poster.jpg
| alt            = 
| caption        = 
| director       = Ki Hwan Oh
| producer       = San Ping Han Tae Sung Jeong Jonathan Kim
| writer         = Qin Hai Yan
| starring       = Bai Baihe Eddie Peng
| music          = 
| cinematography = Yong Ho Kim
| editing        = Min Kyung Shin
| studio         = China Film Group CJ Entertainment C2M Media
| distributor    = 
| released       =   
| runtime        = 101 minutes
| country        = China South Korea
| language       = Mandarin
| budget         = 
| gross          = Renminbi|CN¥192 million (US$31.4 million) (China)  South Korean won|￦127 million (US$118,000) (South Korea)
}}
A Wedding Invitation (分手合约 fēnshǒu héyuē, literal translation: breakup agreement) is a 2013 Chinese romantic comedy film directed by Ki Hwan Oh and starring Bai Baihe and Eddie Peng. 

==Plot==
High school sweethearts Qiao Qiao and Li Xing are about to graduate from university when Li Xing surprises Qiao Qiao with a marriage proposal. Qiao Qiao, however, declines the proposal, offering rash arguments as to why shes not ready. Little does he know, Qiao Qiao has a devastating secret. Nonetheless, they write a breakup agreement; if theyre both single in five years, theyll get married. They part ways and five years pass. Believing Li Xing is still waiting for her, Qiao Qiao waits patiently for his call. But when Li Xing finally does contact her its to invite her to his wedding. Qiao Qiao is flabbergasted and is determined to win her former flame back at any cost, even though the secret that forced them apart lingers.

==Cast==
* Bai Baihe as Qiao Qiao
* Eddie Peng as Li Xing
* Jiang Jinfu as Mao Mao
* Pace Wu as Zhou Rei

==Release==

The movie was released in China on April 12, 2013, in the USA and Canada on May 24 and 31, 2013 for limited screenings, in Hong Kong on June 6, 2013, and in South Korean on June 20, 2013.

==Reception==
The film grossed Renminbi|CN¥192 million (US$31.4 million) in China and South Korean won|￦127 million (US$118,000) in South Korea. 

==References==
 

== External links==
* 
* 

 
 
 
 
 
 
 
 


 
 