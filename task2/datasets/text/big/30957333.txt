Shanghai (2012 film)
 
 
{{Infobox film
| name = Shanghai
| image = Shanghai Poster.jpg
| alt = Official poster for Shanghai 2012
| caption = Theatrical release poster
| director = Dibakar Banerjee
| producer = Ajay Bijli, Dibakar Banerjee, Sanjeev K Bijli, Priya Sreedharan
| writer = Urmi Juvekar Dibakar Banerjee
| starring = Abhay Deol Emraan Hashmi  Kalki Koechlin Prosenjit Chatterjee
| music = Songs:  
| background music = Mikey McCleary
| cinematography = Nikos Andritsakis
| Costumes = Rushi Sharma
| editing = Namrata Rao
| studio = studio green
| distributor = PVR Pictures
| released =  
| runtime = 120 minutes
| country = India Hindi
| budget = 
| gross = 
}}
 political thriller film directed by Dibakar Banerjee, starring Abhay Deol, Emraan Hashmi, Kalki Koechlin, Prosenjit Chatterjee, and based on the French film Z (1969 film)|Z and a novel by the same name by Vassilis Vassilikos. On 6 June 2012, the high court refused stay on the release of the film. It received critical acclaim upon its release on 8 June 2012 with 1200 prints.

==Plot==
The film is set in the fictional city of Bharat Nagar, which is hailed as an example of progress through infrastructure. State government is planning to build an International Business Park (IBP), making the city a Shanghai.

Bhaggu (Pitobash Tripathy) participates in the assault of a local bookstore owner who stocked the copies of Dr. Ahmadis (Prosenjit Chatterjee) latest book, which criticises the local political party Morcha for ignoring the plight of the poor in its quest for infrastructure. Ahmadi, a socialist academic, is scheduled to visit Bharat Nagar for a speech. Shalini (Kalki Koechlin), a former student of Ahmadi, is part of a small group that struggles to raise awareness about the underside of the local partys platform. Jogi (Emraan Hashmi) works in a video shop of questionable repute with the owner Vinod. Krishnan (Abhay Deol), an IAS officer and Vice Chairman of IBP, who is the favourite of the Chief Minister (Supriya Pathak), is assured by Principal Secretary Kaul (Farooq Sheikh) of a promotion and a trip to Stockholm.

Ahmadi arrives from New York and delivers a scathing speech against the establishment. A mob gathers and attacks his associates. Ahmadi is run over by a Tempo driven by Jaggu (Anant Jog), who is arrested. Ahmadi winds up comatose in a hospital. Shalini is confident that this was a planned attack. Vinod informs Jogi that he has incriminating evidence against the IBP and wants to sell Shalini the tape. Dr. Ahmadis wife Aruna (Tillotama Shome) agrees to lead a media campaign demanding the truth, though she seems uninterested. The campaign forces the CMs office to set up an inquiry commission headed by Krishnan. Krishnan finds that the police are hiding evidence, so he summons SSP Chavan, who is also uncooperative.

Shalini meets with Vinod but leaves when Vinod says that he wont give the video for free. Later, Vinod is found dead in what looks like an accident. At a later Morcha rally where Jogi is filming, Damle, head of the local party leader Deshnayaks (Kiran Karmarkar) men, informs him that he knows about the tape, subtly threatening him. Jogi is about to pack up and leave town, but he and Shalini discover the identity of the goons. Shalini and her group presents Krishnan with the CD of evidence linking IBP goons to the accident. After viewing it, Krishnan sends out a summons to the leader Deshnayak, who rejects it in a public rally, sparking up a riotous mob. Shalini visits Jogi, and Morcha thugs attack the studio. Jogi and Shalini manage to escape and hide out on the roof until morning. In the chaos of the riots, Bhaggu is found dead, ostensibly from falling off a moving vehicle.

Shalini receives a panicked call from her maid Gauri, who says that Gauris family is in danger. Shalini and Jogi sneak over to Gauris place, where it is revealed that Jaggu is Gauris husband. Jogi remembers that Vinod kept a backup of the incriminating video, and sneaks back to the studio to get it. Krishnan meets with the Chief Minister about the inquiry. The Chief Minister, buoyant because she has an opportunity to eventually become Prime Minister, presents Krishnan with the approval of his Stockholm trip, and a high promotion.

Jogi finds the CPU amid the studios wreckage and has a narrow escape from the thugs chasing him. They all show Krishnan the video, which establishes that Deshnayak conspired to have Ahmadi killed and the Chief Minister was complicit in this plan. Krishnan confronts Kaul with the truth, and Kaul threatens to ruin Krishnans career. Undeterred, Krishnan blackmails Kaul, until Kaul finally leaves to talk to the Home Ministers secretary. In the hospital, Ahmadis family decide to take him off life support.

An epilogue explains the fate of the major characters: Krishnan refused the chance to go to Stockholm to ensure that a national investigation is opened up as per his plan. Jogi escaped Bharat Nagar but as a pornographer wanted by the police. He is declared untraceable. Shalini wrote a book about the conspiracy, but it is banned in India. In the closing scene, Jaggu is operating a bulldozer demolishing old homes for IBP. A poster carries Arunas picture with the slogans, "Chief Minister for all, IBP for all."

==Cast==
* Abhay Deol as T. A. Krishnan, senior IAS official
* Emraan Hashmi as Joginder Parmar, an adult film maker
* Prosenjit Chatterjee as Dr. Ahmadi, a social activist
* Kalki Koechlin as Shalini Sahay
* Supriya Pathak as Chief Minister Madamji Pitobash Tripathy as Bhagu
* Farooq Sheikh as Principle Secretary Kaul
* Tillotama Shome as Mrs. Aruna Ahmedi
* Chinmay Mandlekar as SSP Chavan
* Anant Jog as Truck Driver Jaggu
* Scarlett Mellish Wilson (Item number)

==Crew==
* RamaRoa as Line Producer
* Arvind Goswami as Unit Production Manager
* Naveen Kasturia as Assistant Director
* Aman Dhillon as Assistant Director
* Risheeka Upadhyay as Assistant Director
* Atul Mongia as Casting Director
* Vinod Rawat as Casting Associate
* Vandana Kataria as Production Designer
* Shabbir Shaikh as Production Manager
* Aniket More as Local Coordinator, Locations, Action Cars & Transportation Coordinator

==Production==
Filming started in May 2011 in Latur, Maharashtra. The first look was released on 5 April 2012. Shanghai premiered on 7 June 2012 at the IIFA awards in Singapore. The film was released in Bengali in a few locations.

==Reception==

===Critical reception===
Upon its release Shanghai received positive reviews from all top critics of India. Ramesh Babu R of idlebrain.com gave 4 out of 5 stars and said "Shanghai is an intense political thriller. Plus points of the film are realistic set-up, gripping screenplay, strong story telling coupled with right casting and performances."  Madhureeta Mukherjee of ToI gave it 3.5 out 5 stars and said "Whether Shanghai is off-beat or mainstream is debatable, but if you thrive on rustic realistic cinema, however heavy-duty – this (Shanghai) is your pick".  Janhavi Patel of FilmiTadka gave it 4 out of 5 stars and wrote in her review "Shanghai is brilliant, to say the least. This is a movie that commands your attention and is an honest film that hits you hard. Watch it for sure!"  Suparna Sharma of The Asian Age rated the movie with 4.5 out of 5 stars: "Director Dibakar Banerjee loves this country dearly and Shanghai is his guttural, anguished wail. But being the super-smart, light-touch director that he is, he conducts the last rites of our beloved country to the loud, cheery strains of the song Bharat Mata ki, Bharat Mata ki... we are in mourning, but we are conducting Bharat Mata’s antim sanskar in a medieval fashion. We are dancing, screaming... And though the lyricist has added “jai bolo” at the end, that’s not the sentiment of this song, or our mourning, or the film. The correct rendition of this song would end with the words “le lo”. 
  of the The Statesman gave three and a half out of five stars and wrote "Dibakar Banerjee succeeds in cranking up the tension effortlessly..."  Rajeev Masand of CNN-IBN gave it 3.5 stars out of 5 saying "Shanghai is consistently watchable... It’s a good film from one of Hindi cinema’s most exciting filmmakers, just not great." 

===Controversy===
The song "Bharat Mata Ki Jai" (Victory for Mother India) had irked a group called Bhagat Singh Kranti Sena, whose president Tajinder Pal Singh Bagga tweeted: “We are giving open warning to Shanghai Directer to remove Bharat Mata Ki Jai song from the movie. Otherwise movie will be banned... We strongly condemn the lines Bharat Mata ki jai Sone ki Chidiya, Dengu, Maleria gud hai, Gobar hai Bharat Mata Ki jai (sic).”  , Hindustan Times, 4 May 2012.  The lines roughly translate to "The golden sparrow, (as well as) Dengue and malaria, We have the good as well as the shitty, hail Mother India."  , Bharat Mata ki Jai (Shanghai): Lyrics, Meaning, Translation. 

===Box office===

====India====
Shanghai had a poor first day as it collected around 9–102.5&nbsp;million net on its first day. The film has just managed average collections at some high end multiplexes of metros during the first weekend, but the box office sales increased the subsequent week.  Shanghai showed growth on Saturday of around 25%–30% as it collected in the 4-42.5&nbsp;million nett region, but it needed much bigger growth as the starting level was so low. Shanghai collected around 7.25–75.0&nbsp;million nett in two days which is not good.  It fell flat on Sunday as collections could not grow as they did on Saturday; it grossed around 120&nbsp;million nett over the weekend. The approximate breakdown on the weekend are 32.5&nbsp;million nett on Friday, 42.5&nbsp;million nett on Saturday and 45.0&nbsp;million nett on Sunday. {{cite web|url=http://boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=4537&nCat=|title=Shanghai Falls Flat on Sunday
 |date=10 June 2012}}  Shanghai had a low Monday as it collected around 17.5&nbsp;million nett. The drop from Friday is less than 50% but collections are too low for a film released on nearly 1000 screens. 

====Overseas====
Shanghai was dull overseas grossing around $325,000. The film did not release in UK. {{cite web |url=http://boxofficeindia.com/overdetail.php?page=shownews&articleid=4544&nCat=|title=
Shanghai Dull Rowdy Rathore At $2.75 Million|date=13 June 2012}} 

====Special screening====
After a gap of 20 years, this was the first Hindi, rather Indian film to be screened in Baghdad. It was critically acclaimed there, exceeding gross expectations.

====Awards====
*Filmfare Award for Best Costume Design (2013) – Manoshi Nath & Rushi Sharma
* IRDS Film Awards for Social concern, 2012: Best direction – Dibakar Banerjee 
*Stardust Award for Best Supporting Actor (2013) – Prosenjit Chatterjee

==Soundtrack==
 

{{Infobox album  
| Name       = Shanghai
| Type       = Soundtrack
| Artist = Vishal-Shekhar
| Type = soundtrack Feature film soundtrack
| Border = Yes
| Label = T-Series
| Producer = Vishal-Shekhar
| Last album = Kahaani (2012)
| This album = Shanghai (2012) Student of the Year
}}
 Vishal Dadlani Anvita Dutt.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = no
| collapsed = no
| title1 = Bharat Mata Ki Jai
| extra1 = Keerthi Sagathia, Vishal Dadlani, Mandar Apte, Chintamani Sohoni, R N Iyer, Bhupesh
| music1 = Vishal-Shekhar
| length1 = 4:06
| title2 = Imported Kamariya Richa Sharma, Vishal Dadlani, Shekhar Ravjiani
| music2 = Vishal-Shekhar
| length2 = 3:58
| title3 = Duaa
| extra3 = Nandini Srikar, Arijit Singh, Shekhar Ravjiani
| music3 = Vishal-Shekhar
| length3 = 4:20
| title4 = Khudaaya
| extra4 = Shekhar Ravjiani, DJ Kiran, Raja Hasan
| music4 = Vishal-Shekhar
| length4 = 2:57
| title5 = Morcha
| extra5 = Raja Hasan, Vishal Dadlani
| music5 = Vishal-Shekhar
| length5 = 3:32
| title6 = Bharat Mata Ki Jai (Remix)
| extra6 = Vishal Dadlani, DJ Kiran, Keerti Sagathia
| music6 = Vishal-Shekhar
| length6 = 3:15
| title7 = Khudaaya (Remix)
| extra7 = Shekhar Ravjiani, DJ Kiran
| music7 = Vishal-Shekhar
| length7 = 3:26
| title8 = Mantra: Vishnu Sahasranamam (The Thousand Names of Lord Vishnu)
| extra8 = Srivatsa Krishna
| music8 = Vishal-Shekhar
| length8 = 4:55
}}

==Satellite rights==
The satellite rights of Shanghai have been sold for   8 crore and the music rights for Rs 2.75 crore. Also, 20% of theatrical rights have been sold for   40&nbsp;million. 

==References==
 

==External links==
 
*  
*  
*  
*" " – a review of the film from the Hindi film site  

 
 
 
 
 
 
 
 
 