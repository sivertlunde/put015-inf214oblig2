Flight to Hong Kong
{{Infobox film
| name           = Flight to Hong Kong
| image          = Flight to Hong Kong poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Joseph M. Newman
| producer       = Joseph M. Newman 
| screenplay     = Edward G. OCallaghan Leo Townsend 
| story          = Gustave Field Edward G. OCallaghan Joseph M. Newman
| starring       = Rory Calhoun Barbara Rush Dolores Donlon Soo Yong Pat Conway Werner Klemperer
| music          = Albert Glasser
| cinematography = Ellis W. Carter 	
| editing        = William W. Moore George C. Shrader 
| studio         = Rorvic Productions Sabre Production
| distributor    = United Artists
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Flight to Hong Kong is a 1956 American crime film directed by Joseph M. Newman and written by Edward G. OCallaghan and Leo Townsend. The film stars Rory Calhoun, Barbara Rush, Dolores Donlon, Soo Yong, Pat Conway and Werner Klemperer. The film was released on October 8, 1956, by United Artists.  

==Plot==
 

== Cast == 
*Rory Calhoun as Tony Dumont
*Barbara Rush as Pamela Vincent
*Dolores Donlon as Jean Blake
*Soo Yong as Mama Lin
*Pat Conway as Nicco
*Werner Klemperer as Bendesh
*Mel Welles as Boris
*Paul Picerni as Michael Quisto
*Aram Katcher as Lobero
*Bob Hopkins as Cappy
*Booth Colman as Maxler
*Timothy Carey as Lagarto 
*Noel Cravat as Bob Gantz 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 