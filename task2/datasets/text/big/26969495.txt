Moomins and the Comet Chase
{{Infobox film
| name           = Moomins and the Comet Chase
| image          = Moomins and the Comet Chase poster.jpg
| caption        = Official poster
| director       = Maria Lindberg
| producer       = Tom Carpelan
| writer         =  
| starring       =  
| music          =  
| cinematography = 
| editing        = Maria Lindberg
| studio         = Stereoscape Ltd.
| distributor    = Oy Filmkompaniet Alpha Ab
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = €974,893
| gross          = 
}} compiled from The Moomins restored and voice actors stereoscopic 3-D. A similar revision of the remainder of the series for high-definition television of all 78 episodes will follow and is currently in production.
 Comet in Moominland.

International version of the film features the voices of known Swedish actors like Stellan Skarsgård, Peter Stormare, Alexander Skarsgård. Musician and Moomins fan Björk also wrote an original song for the film.   

The international debut was at the Cannes Film Festival in May 2010. In Finland, it arrived in movie theaters on 6 August 2010 and will later be broadcast on the MTV3 channel.    The film will also be produced in dozens of other languages and will subsequently receive global distribution.   

==Plot== Muskrat if he knows what is happening, who advises him that things tend to look like this before an awful fate coming from the sky hits the Earth. With the help of his father, Moominpappa, Moomintroll and his close friends Sniff and Snufkin build a raft and head out on a challenging journey to the observatory in the Lonely Mountains hoping to find out more from the wise professors there. The friends have to overcome several adversities in order to make it there. When they arrive, they find the professors deep in calculations. They reveal that a comet will reach the Earth in four days, four hours, four minutes and 44 seconds.

The group decides to get back home as fast as they can to share the news. On the journey home, they encounter more dangerous creatures and obstacles as well as some old and new friends, including Snork Maiden and her brother Characters in the Moomin series#Close friends|Snork, who join them on their adventure. The sky continues to become redder and the air hotter. When they get to the seashore, they are shocked to see that the sea has completely dried up. A walk along the dried out sea bottom brings more excitement and danger.

Eventually they make it back home to the Moominhouse and discover another shocking detail: the comet is supposed to land right in their own garden that very night. Ultimately, the big question is if they can get everybody to safety in time.   

== Cast ==
(International / Finnish)

* Max von Sydow / Tapani Perttu as narrator
* Stellan Skarsgård as Moominpappa and Hemulens / Tapani Perttu as Moominpappa
* Alexander Skarsgård / Jasper Pääkkönen as Moomin
* Peter Stormare / Taneli Mäkelä as Snufkin
* Mads Mikkelsen / Ilpo Mikkonen as Sniff
* Helena Mattsson as Snork Maiden
* Johanna Viksten as Moominmamma
* Jarmo Koski as Snork
* Jarmo Koski as Hemulen

== Music ==
Icelanders Björk and Sjón, both big fans of the Moomin franchise, composed "The Comet Song" for the films soundtrack.    Antonia Ringbom, a Finnish director, producer, animator and illustrator who specializes in childrens programming, is working on the projects title sequence, which combines her animation with the theme song performed by Björk.    Bjork  : " I read the books as a child and then I read them for my children. I guess I realised that I still like them just as much. They probably stand for something Nordic, I think theres something quite stark, a certain kind of simplicity. You could speculate and say, because there are fewer plants there, and fewer animals, fewer buildings, so its kind of more minimal. Ive sometimes compared it to Scandinavian furniture , compared to Italian furniture. Italian - hundreds of little details, curls and curves and decoration - and Scandinavian furniture is kind of naked. And its funny and sad at the same time. And the relationship to nature Tove Jansson has, thats very important to me. And everybodys allowed to be just as eccentric as they are, they dont have to conform. Reading to my children I actually noticed some kind of anti-authority aspect in it - theres no hierarchy between the characters, they are all equal. I like that a lot."  

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 