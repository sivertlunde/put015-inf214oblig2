Crossroads (2002 film)
{{Infobox film
| name = Crossroads
| image = Crossroads poster.JPG
| caption = Theatrical release poster
| director = Tamra Davis
| producer = David Gale| 
| writer = Shonda Rhimes
| starring = Britney Spears Anson Mount Zoe Saldana Taryn Manning Kim Cattrall Dan Aykroyd Trevor Jones
| cinematography = Eric Alan Edwards
| editing = Melissa Kent Zomba Films
| distributor = Paramount Pictures 
| released =  
| runtime = 94 minutes
| country = United States
| language = English
| budget = $12 million http://www.boxofficemojo.com/movies/?id=crossroads.htm 
| gross = $61.1 million 
}}
 road film set in Georgia (U.S. state)|Georgia. Directed by Tamra Davis and written by Shonda Rhimes, the film stars Britney Spears, Anson Mount, Zoe Saldana, Taryn Manning, Kim Cattrall and Dan Aykroyd. The film was produced by MTV Films and released on February 15, 2002, in North America by Paramount Pictures. The plot centers on three teenage girls as they take a cross-country road trip, finding themselves and their friendship in the process.

Development on the film began in 2001, when Spears created a concept that was later expanded by Rhimes. Principal filming began on March 2001, and encompassed over a period of six months. Critics gave negative reviews to Crossroads; however, they considered it a better effort when compared to Mariah Careys 2001 film Glitter (film)|Glitter. Despite the movies response from critics, it was a box office success, grossing over $61.1 million worldwide in three months.

==Plot==
Lucy (  and Kit to visit her fiancé in Los Angeles.

They set out on the road with little money in a yellow 1969 Buick Skylark convertible with a guy named Ben (Anson Mount). Lucy, however, leaves without the permission of her father Pete (Dan Aykroyd), who wakes up the next day to find his daughter is gone. Shortly into their journey, the car breaks down. They realize that they dont have enough money between them for the travel nor the repair costs. Mimi then suggests that she sing karaoke at a local bar, where good singers are tipped well by the customers. While at the bar, the girls dress up and go the stage to perform. However, when the song starts Mimi develops stage fright, being unable to sing. Seeing that they needed the money, Lucy takes her place, and quickly becomes a hit with the crowd. They make enough money to fix the car and continue on their way.

Shortly after the group checks into a motel, Kit tells the girls that she heard a rumor about Ben, commenting that he had recently been released from jail, after killing someone. The girls then spend most of the journey feeling uneasy around him, until they confront him. Ben reveals that he in fact was in jail - for driving his step-sister across state borders without parental consent, after his father was abusing her. Having established that Ben was not the assassin they imagined him to be, Lucy and Ben grow closer. The girls talk properly to each other for the first time since they were kids. Lucy reveals that her mother abandoned her and her father when she was three. Kit, who was overweight as a child, has an overbearing mother who sent her to "fat camp" but now cannot stand that her daughter is more beautiful than her. Mimi reveals that the babys father was not her boyfriend, but a guy who raped her after she got drunk at a party.

After they arrive in Arizona, Lucy finally meets her mother Caroline (Kim Cattrall) after eighteen years. However, Caroline reveals that she married again, and that she now has two sons. She reveals to Lucy that she never wanted to have her, and that she was a mistake. Lucy leaves the house and starts to cry in a motel bathroom, while Ben comforts her. The following day, Lucy rejoins the others and goes on to Los Angeles. When they arrive, Kit brings Mimi along to surprise her fiancé, Dylan (Richard Voll). While alone in the hotel, Lucy loses her virginity to Ben. When Kit and Mimi arrive to Dylans place, it is revealed that he is cheating on Kit after they see another girl in his apartment. Already upset, Kit suddenly realizes that it was Dylan who raped Mimi and got her pregnant, and she punches him. Scared, Mimi runs away. However, she falls down the stairs and, consequently, loses her baby.  Lucy and Ben fall in love, and against her fathers wishes, she stays in Los Angeles and goes to the audition instead of Mimi, receiving a standing ovation at the end. The film ends with Lucy, Kit, and Mimi burying a new time capsule on the beach. This time they make no promises for the future, but, instead promise to let go of their past.

==Cast==
* Britney Spears as Lucy Wagner
* Anson Mount as Ben
* Zoe Saldana as Kit
* Taryn Manning as Mimi
* Kim Cattrall as Caroline Wagner
* Dan Aykroyd as Pete Wagner
* Justin Long as Henry  
* Beverly Johnson as Kits Mother
* Kool Moe Dee as Bar Owner
* Richard Voll as Dylan
* Katherine Boecher as Dylans Other Girl David Gruber Allen as Bar Patron Kyle Davis as High School Burnout
* Bowling For Soup as Graduation Band
* Jesse Camp as Audition Applicant
* Jamie Lynn Spears as Young Lucy Wagner

==Production==
  in Cannes, France.]]
In early 2001, Spears said that she had plans to make her film debut.  She and her team then created a concept for it,    which was later developed by Greys Anatomy creator Shonda Rimes.  Spears commented that she "talked to   and told her what I wanted the movie to be about and she elaborated on it. It was my little project. When you do a movie, I think you have to be really passionate about it. I was having a lot of offers, but this is something my heart was into."    A press conference was held during the Midem|Marché International du Disque et de lEdition Musicale (MIDEM) in Cannes, France, on January 19, 2002, where Spears also premiered the film. 
 Baton Rouge and Hammond, Louisiana, near Spears hometown.  Due to the fact that Spears was also recording her third studio album along with the films production, filming only wrapped up after six months.    Additional scenes were filmed in Los Angeles, California.  Crossroads had a total budget of $10 million; a relatively low budget by industry standards.    According to the Louisiana Film and Video Commission, the film was originally titled What Friends are For.  Spears described it as a teen movie that deals with real issues that normal teenagers live on a daily basis.  She continued to explain the films content, saying that it "is about this journey that the three of us best friends take, finding ourselves and what we want out of life and getting our friendship back. Friends are all you have at the end of the day. When your boyfriend breaks up with you, who do you call? Your girlfriend. I just love that message."   

Justin Long, who plays one of Lucys best friends from high school, thought that Crossroads is "like a road trip buddy movie for girls."    Long also said that he was impressed by Spears work ethic, commenting that "she could not have been more down to earth. Shes the sweetest girl. After 10 minutes, I forgot she was a big pop star."  Anson Mount, who plays Ben, revealed that actor Robert De Niro ran a few of Spears lines with him while rehearsing for the film. Mount revealed that De Niro called the singer "a sweet girl" and convinced him to do Crossroads. 

==Release and reception==

===Box office===
Crossroads was released in the United States on February 15, 2002. On its opening day, the film grossed an estimated $5.2 million in 2,380 theaters, becoming the second highest grossing film of the day.    On the first weekend of its release, Crossroards placed second, grossing an estimate of $14,527,187.  By the second week, the film dropped a 52% on tickets sales, ranking at number 5 on the Box Office.  Crossroads was a moderate financial success, grossing a total $37,191,304 in the United States.  Worldwide, the film grossed a total of $61,141,030 until its close day, on May 9, 2002. 

===Critical response=== weighted average score, gave the film a 27 out of 100 based on 31 reviews from critics.   

Robert K. Elder of   commented that "the films mealy-mouthed messages about feminine empowerment will almost certainly fall on deaf ears, since even 11-year-olds know Spearss power resides largely in her taut torso".  Claudia Puig of USA Today considered it "less a movie than a mind-numbingly dull road trip", while The Washington Post reporter Ann Hornaday said, "not a music video, not yet a movie, but more like an extended-play advertisement for the Product that is Britney".  Jane Dark of Village Voice compared Crossroads to Mariah Careys Glitter (film)|Glitter, saying, "you spend a lot of time wondering, Better or worse than Glitter? You think if the projectionist cranked the volume a little you could actually sort of get into this". 

John Anderson of Los Angeles Times commented "Spears acquits herself as well as anyone might, in a movie as contrived and lazy as this one".  Chris Kaltenbach of Baltimore Sun said, "go see Crossroads if you want to hear Britney sing or see her wear next-to-nothing. But otherwise, avoid this train wreck at all costs".  Lisa Schwarzbaum of Entertainment Weekly, however, gave the movie a positive review, commenting Crossroads "not only makes excellent use of the singers sweetly coltish acting abilities, but it also promotes a standardized set of sturdy values with none of Mariah Careys desperate Glitter, or any of Mandy Moores gummy pap in A Walk to Remember".  Bret Fetzer of Amazon.com also gave a positive review, noting that the movie "could have been trite schmaltz, but the script has some grit and the direction is fresh and relaxed--and, most significantly, Spears is far more sympathetic and engaging than you might expect".    Jane Crowther of BBC applauded Cattrall and Aykroyds interactions with the characters, and said that "Spears manages to come across on film as natural, endearing, and extremely likable".  Time (magazine)|Time named it one of the top 10 worst chick flicks. 

===Accolades===
{| class="wikitable"
! Group !! Category !! Recipient !! Result
|- MTV Movie 2002 MTV Movie Awards  Best Breakthrough Performance
| Britney Spears
|  
|- Best Dressed
| Britney Spears
|  
|- 2002 Teen Choice Awards 
| Choice Actress, Drama/Action Adventure
| Britney Spears
|  
|-
| Choice Breakout Performance, Actress
| Britney Spears
|  
|-
| Choice Chemistry
| Britney Spears and Anson Mount
|  
|-
| rowspan="8" | 23rd Golden Raspberry Awards 
| Most Flatulent Teen-Targeted Movie
| Crossroads
|  
|-
|  Worst Actress
| Britney Spears
|  
|-
| Worst Director
| Tamra Davis
|  
|-
| Worst Original Song
| "Im Not a Girl, Not Yet a Woman"
|  
|-
| Worst Original Song
| "Overprotected"
|  
|-
| Worst Picture
| Paramount
|  
|-
| Worst Screen Couple
| Britney Spears and Anson Mount
|  
|-
| Worst Screenplay
| Shonda Rhimes
|  
|-
| rowspan="4" | 25th Stinkers Bad Movie Awards 
| Worst Actress
| Britney Spears
|  
|-
| Worst Original Song
| "Im Not a Girl, Not Yet a Woman"
|  
|-
| Worst On Screen Couple
| Britney Spears and Anson Mount
|  
|-
| Worst Fake Accent - Male
| Dan Aykroyd
|  
|-
|}

==Soundtrack==
{{Infobox album |  
| Name        = Music from the Major Motion Picture Crossroads 
| Type        = Soundtrack 
| Artist      = Various artists 
| Cover       = Crossroads Soundtrack.JPG
| Released    = February 15, 2002
| Recorded    = 1990–2002  Pop
| Length      =  Zomba 
| Producer    = Rodney Jerkins, The Neptunes, Fred Maher, Matthew Sweet, Dennis Herring, Jaret Reddick, Max Martin, Rami Yacoub, JS16
}}

===Background=== Zomba Records on February 2, 2002,  and was produced by Rodney Jerkins, The Neptunes, Fred Maher, Matthew Sweet, Dennis Herring, Jaret Reddick, Max Martin, and Rami Yacoub. "Overprotected" was remixed by JS16 for the soundtrack album. 

===Track listing===
{{Track listing
| extra_column    = Performer(s)
| writing_credits = yes I Love Rock n Roll
| note1           = karaoke sing-along version
| length1         = 3:06
| writer1         = Alan Merrill, Jake Hooker
| extra1          = Britney Spears Shake It Fast
| length2         = 4:15
| writer2         = Michael Tyler, Pharrell Williams, Chad Hugo
| extra2          = Mystikal Girlfriend
| length3         = 3:40
| writer3         = Matthew Sweet
| extra3          = Matthew Sweet
| title4          = Unforgetful You
| writer4         = Jars of Clay
| extra4          = Jars of Clay
| length4         = 3:20
| title5          = Greatest Day
| writer5         = Jaret Reddick
| extra5          = Bowling for Soup
| length5         = 3:14
| title6          = Overprotected
| note6           = JS16 Remix
| writer6         = Max Martin, Rami Yacoub
| extra6          = Britney Spears
| length6         = 6:07
}}

===Credits and personnel===
*Performers – Britney Spears, Mystikal, Matthew Sweet, Jars of Clay, Bowling for Soup
*Songwriters – Alan Merrill, Jake Hooker, Michael Tyler, Pharrell Williams, Chad Hugo, Matthew Sweet, Jars of Clay, Jaret Reddick, Max Martin, Rami Yacoub
*Producers – Rodney Jerkins, The Neptunes, Fred Maher, Matthew Sweet, Dennis Herring, Jaret Reddick, Max Martin, Rami Yacoub
*Remixer – JS16
*Audio mastering – Tom Coyne

Source: 

==References==
 

==External links==
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 