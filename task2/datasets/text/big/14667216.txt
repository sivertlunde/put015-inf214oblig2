The Hands of Orlac (1924 film)
{{Infobox film
 | name = Orlacs Hände The Hands of Orlac Les Mains dOrlac
 | image_size = 
 | caption = 
 | director = Robert Wiene
 | producer = Pan-Film
 | writer = Ludwig Nertz (play), Maurice Renard (book)
 | narrator = 
 | starring = Conrad Veidt, Alexandra Sorina, Fritz Kortner, Carmen Cartellieri, Fritz Strassny, Paul Askonas
 | music = Pierre Oser
 | cinematography = Günther Krampf, Hans Androschin
 | editing = 
 | distributor = Pan-Film Berolina-Film Aywon Film Corporation
 | released =  
 | runtime = 90 minutes
 | country = Austria French
 | budget = 
 | gross = 
 | image= The Hands of Orlac VideoCover.png
}} Austrian silent silent horror Expressionist films motifs with naturalistic visuals. The film has been remade twice.

== Plot ==
Concert pianist Paul Orlac (Conrad Veidt) loses his hands in a horrible railway accident. His wife Yvonne (Alexandra Sorina) pleads with a surgeon to try and save Orlac’s hands.  The surgeon decides to try and transplant new hands onto Orlac, but the hands he uses are those of a recently executed murderer named Vasseur. From that point forward, the pianist is tortured by the presence of a knife he finds at his house, just like that used by Vasseur, and the desire to kill. He believes that along with the murderer’s hands, he has also gained the murderers predisposition to violence. He confronts the surgeon, telling him to remove the hands, but the surgeon tries to convince him that a person’s acts are not governed by hands, but by the head and heart.  

Orlac’s new hands are unable to play the piano, and in time he and his wife run out of money.  Creditors give them one more day to pay their bills.  Yvonne goes to Paul’s father for money, but is refused.  Orlac himself then goes to see his father, but finds he has been stabbed to death with the same knife like Vasseurs. He starts to think he himself committed the murder, and goes to a café for a drink.  There he meets a man who claims he is Vasseur, who tells Orlac the same surgeon who did the hand transplant also transplanted a new head onto Vasseur’s body.   He then tells Orlac he wants money to keep quiet about the murder.

In the meantime, police find Vasseur’s finger prints at the scene of the crime, causing confusion.  Paul and Yvonne Orlac decide to go to the police and try to explain about Vasseur’s hands being on Paul’s arms, but that he had no recollection of killing his father.  He also tells the police about the man claiming to be the executed murderer, and the blackmail money.  It turns out that the man is actually a con man, well known to police.  The Orlacs maid tells the police that he was a friend of Vasseur, and that he had made a set of rubber gloves with Vasseur’s finger prints on them.  The gloves were used during the murder.

== Characters ==
* Conrad Veidt as Paul Orlac
* Alexandra Sorina as Yvonne Orlac
* Fritz Kortner as Nera
* Carmen Cartellieri as Regine
* Fritz Strassny as Pauls father
* Paul Askonas as Servant

== Background ==
Orlacs Hände was based on the book Les Mains dOrlac by Maurice Renard. It was one of the first films to feature the motif, often recurring in later films, of hands with a will of their own, whether or not attached to a body, as well as popular fears, around the subject of surgical transplants, in the days before such procedures were possible. It was shot at the studios of Listo-Film in Vienna by the Pan-Film production company.
 German Berolina-Film French version English version The Hands of Orlac. The film was first shown in the United States in 1928, where its promotion and distribution were undertaken by the Aywon Film Corporation.

The sets were constructed by the film architects and set builders Hans Rouc, Karl Exner and Stefan Wessely.

== Critics ==
* "Paimanns Filmlisten", Nr. 441, 1924, p. 181: "... the presentation of the subject is extremely gripping and tension is maintained right up to the last scene: an extraordinarily well-chosen ensemble headed by Konrad Veidt makes the very most of the possibilities. The direction is taut and careful, especially in the very realistic scenes of the railway accident, the decor tasteful, the events of the action effectively emphasised. The photography is of the highest quality in every respect. An Austrian film that is the equal of the best foreign products..."
 

== Versions ==
The original version was 2,507 metres long, the equivalent of 92 minutes. The reconstructed version is 2,357 metres long, which corresponds to about 90 minutes. On 11 January 2001 ARTE broadcast a version reconstructed in 1995 by the German Federal Archives | Bundesarchiv-Filmarchiv in Berlin, Friedrich Wilhelm Murnau|Friedrich-Wilhelm-Murnau-Stiftung and the Deutsches Filminstitut with the cooperation of Yugoslav Film Archive | Jugoslovenska Kinoteka in Belgrad. This version had new music by Henning Lohner and was also given background noises and the sound effects of an interrogation scene off, which was not universally approved. Since then the film has been shown at many film festivals worldwide. Another version was released on DVD by Kino International (company) | Kino Lorber in 2008, based on the 1995 reconstruction and restored by Bret Wood with a score by composer Paul Mercer and additional footage courtesy of The Raymond Rohauer Collection in Columbus, Ohio. The Filmarchiv Austria has restored the film with material from its archives. It will premiere at Konzerthaus Wien on June 13, 2013 as part of the Vienna Music Festival, with a new score by American composer Donald Sosin, performed by the composer at the piano and Dennis James at the Rieger organ.

== Censors decisions ==
The film was approved for German release on 25 September 1924, but for adults only. An application was made by the Ministry of the Interior of Saxony dated 10 January 1925, urging that the film should be censored, because it "...is likely to endanger public safety and order... Based on an assessment by the Provincial Criminal Office at Dresden the Government of Saxony does not think it appropriate to make publicly known the internal arrangements and tools of the criminal police, particularly in connection with the taking of finger prints, as this would make the fighting of crime more difficult. Further, the representation of means which enable the criminal to obliterate his prints and deceive the police, is highly unsuitable."

The application for censorship was refused by the Higher Inspectorate, as an expert from the police headquarters in Berlin when questioned by them about it described the specialist content as unrealistic. There were so far no experiences across the whole of Europe of the falsification of finger prints by the use of wax impressions or similar techniques, leaving aside any other methods of falsifying prints. The Inspectorate did concede that if the film had shown a realistic method of forging fingerprints, if any existed, then it would have raised issues of public security, but concluded that what was shown in the film was pure fantasy. 

In 1996 the film was re-evaluated and released, still with an age limit. 

== Remakes ==
There were two direct remakes: Mad Love (USA 1935) with Colin Clive & Peter Lorre, directed by Karl Freund. This version is repeatedly evoked in Malcolm Lowrys Under the Volcano (1947), under its Spanish title : "Las Manos de Orlac. Con Peter Lorre". The Hands of Orlac / Les mains dOrlac (GB/FRA 1960) with Mel Ferrer and Christopher Lee, directed by Edmond T. Gréville.
 The Hand (1981) directed by Oliver Stone, and Les Mains de Roxana (2012), directed by Philippe Setbon and starring Sylvie Testud as Roxana Orlac, a violinist who receives the hands of a criminal who committed suicide.  

The 1960 film also inspired several sequels.

== Notes ==

 

== Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.
* Roland M. Hahn und Rolf Giesen: Das neue Lexikon des Horrorfilms. Berlin: Lexikon Interprint Verlag, 2002. ISBN 3-89602-507-4
* Matthias Bickenbach, Annina Klappert, Hedwig Pompe: Manus Loquens. Medium der Geste - Geste der Medien. Dumont Literatur und Kunst Verlag, Cologne 2003, pp. 243–305: Monströse Moderne. Zur Funktionsstelle der manus loquens in Robert Wienes ORLACS HÄNDE (Österreich 1924) ISBN 3-8321-7830-9

== External links ==
*  
*  
*  
*     
*     
*     
*  
 

 
 
 
 
 
 
 
 
 
 
 
 