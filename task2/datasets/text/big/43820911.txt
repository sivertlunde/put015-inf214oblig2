Hatim Tai (1956 film)
{{Infobox film
| name           = Hatim Tai
| image          = Hatim Tai 1956.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Basant Pictures
| writer         = JBH Wadia
| screenplay     = JBH Wadia
| story          = JBH Wadia
| based on       = 
| narrator       =  Shakila Meenaxi Sheikh
| music          = S. N. Tripathi
| cinematography = Anant Wadadeker
| editing        = Kamlaker
| studio         = Basant Studios
| distributor    = 
| released       = 1956
| runtime        = 142 min
| country        = India Hindi
| budget         = 
| gross          = 
}} 1956 fantasy film directed by Homi Wadia for Basant Pictures.    The story, script and scenario were by JBH Wadia, with dialogues by Hakim Latta and Chand Pandit. The film had several past favorites of Wadia Brothers credited in the film title roll, like Sardar Mansoor, Mithoo Miyan, Dalpat as actors and Boman Shroff as production manager. The cast included Shakila (actress)|Shakila, P. Jairaj, Meenaxi, Naina, Krishna Kumari, S. N. Tripathi, B.M. Vyas and Sheikh.    
 Hatim Tai (Hatim of Tayy), a merchant and poet who was known for his charity and kindness.    In this Arabian Nights style fantasy Hatim Tai referred to as Shehzada (Prince) from Yemen, undertakes a perilous journey in order to save a young fairy turned to stone. He can do this by answering seven questions posed to him on the way.

==Plot==
While dispensing charity Hatim Tai (P. Jairaj), a kind-hearted Shehzada  (Prince) of Yemen meets a young beggar who is the rich Shezada Munir. The girl Munir loves, Husn Bano,  has been cursed by a fairy, Gulnar Pari (Shakila (actress)|Shakila), that if she gets married she will turn to stone due to her father Saudagar (merchant) Bardoz’s unwelcome advances towards Gulnar. On finishing the curse Gulnar herself turns into a stone statue as a human hand has touched her. The father is repentant and has tried to get wise men from all over to find a way out of this predicament. There are seven questions written on the wall and on finding the answers for all, the curse will be lifted and Gulnar too will return to her normal self. Hatim Tai agrees to undertake the mission and extracts a promise of marriage between Munir and Husn Bano. Bardoz agrees to the promise and keeps Munir with him. Hatim Tai sets out with his companion Nazroo Dhobi on the journey, coming across mermaids from an undersea kingdom, and are able to answer the first question. They go through several similar adventures meeting Gulnar pari’s twin sister Husna Pari (Shakila), who helps them. Hatim Tai and Husna Pari fall in love but the marriage can take place if Husna gives up her wings. However, they have to wait as Hatim has to complete his task and Husnas father has to agree. Finally all the questions are answered along with several other adventures and Gulnar resumes normal form. Husna pari’s father gives permission for Hatim to marry Husna and Munir and Husan Bano also get married. 

==Cast== Shakila
* P. Jairaj
* Meenaxi
* Naina
* Krishna Kumari
* S. N. Tripathi
* B. M. Vyas
* Sheikh
* Vasantrao Pahlwan
* Dalpat
* W. M. Khan
* Sardar Mansoor
* Habib
* Mithoo Miya
* Aga Shapoor

==Production==
The film has been remade several times since 1929, but Homi Wadia’s Hatim Tai in Geva colour is cited to be the "most popular version".    The film, though produced at Basant Studios, was credited as a Wadia Brothers production. The special effects for this rare A-Grade film from Basant were by Babubhai Mistry and were regarded as the main draw for the large audiences.   

==Music==
The music direction was by S. N. Tripathi and according to the credit roll of the film he was assisted by JBH Wadia. The lyricists were Chand Pandit, B.D. Mishra, Akhtar Romani, Raja Mehdi Ali Khan. The songs were sung by Mohammed Rafi, Suman Kalyanpur, Shamshad Begum, Asha Bhosle, and Sheikh.    The song "Parwar Digaar-e-Alam" sung by Mohammed Rafi with lyrics by Akhtar Romani became popular. At the screening of the film in Hyderabad, the then Nizam of Hyderabad had the song repeatedly shown eleven times.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|-
| 1 Parwar Digaar-e-Alam
| Mohammed Rafi
| Akhtar Romani
|-
| 2
| O Jane Wale, Khuda Ki Rehmaton Ka Tujh Pe Saya 
| Suman Kalyanpur, Mohammed Rafi
| Raja Mehdi Ali Khan
|-
| 3
| Aa Gayi Bahar Haye, Teri Kasam Aaj Mera Dil Hua 
| Shamshad Begum
| Raja Mehdi Ali Khan
|-
| 4
| Jo Ishq Ki Aag Me Jalte Hain, Angaro Se Kab Darte Hai 
| Mohammed Rafi
| Chand Pandit
|-
| 5
| Jhoomti Hai Nazar Jhoomta Hai Pyar Ye Nazar Chhin Kar 
| Mohammed Rafi, Asha Bhosle
| Raja Mehdi Ali Khan
|-
| 6
| Nahin Tujhko Khabar Teri Pehli Nazar Mere Dil Me 
| Shamshad Begum and chorus
| Raja Mehdi Ali Khan
|-
| 7
| Hum Azad Ho Jahan Dil Mein Pyar Ho Jawan Ho Bahar 
| Shamshad Begum
| B. D. Mishra
|-
| 8
| Nakhre Karti Darti Nakhre Karti Darti Gayi Thi Main Bazar 
| Mister Sheikh
| Raja Mehdi Ali Khan
|-
| 9
| Dil Kis Pe Aa Gaya Hai Tumhara
| Mubarak Begum,Asha Bhosle
| Raja Mehdi Ali Khan 
|-
|}

==References==
 

==External links==
* 

 

 
 
 
 
 