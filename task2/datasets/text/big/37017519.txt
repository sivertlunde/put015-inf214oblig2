A Trip
{{Infobox film
| name           = A Trip
| image          = 
| caption        = 
| director       = Nejc Gazvoda
| producer       = 
| writer         = Nejc Gazvoda
| starring       = Luka Cimpric
| music          = 
| cinematography = Marko Brdar
| editing        = 
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = Slovenia
| language       = Slovene
| budget         = 
}}
 Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Cast==
* Luka Cimpric as Andrej
* Jure Henigman as Gregor
* Nina Rakovec as Ziva

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Slovenian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 