Phool Bane Angaray
 
{{Infobox film
| name           = Phool Bane Angaray
| image          = Phool bane angaray.jpg
| caption        = 200px
| director       = K.C. Bokadia
| producer       =
| writer         =
| starring       = Rajinikanth Rekha Prem Chopra
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = July 12, 1991
| runtime        =
| country        = India
| awards         =
| language       = Hindi
| budget         =
| gross          =
| website        =
}}

Phool Bane Angaray is a 1991 film by K.C. Bokadia which stars Rajinikanth and Rekha in the lead roles.It was basically Female oriented Film with Rekha as the centre of attraction but Rajinikanth made a great impact as Inspector Ranjit Singh in the special appearance.It was Box office Hit.

==Plot==
Dutta Babu (Parikshat Sahni) is standing for elections against a cunning, corrupt, & established gangster, Bishamber Prasad (Prem Chopra), who is also powerful and influential enough to swing the election his way, as well as have Dutta killed. Inspector Ranjit Singh (Rajinikanth) gets evidence about Bishambers involvement in Duttas death, but his superior officer, DSP Ravi Khanna, prevents him from taking any action. Ranjit then meets with beautiful Namrata (Rekha), and both get married. As Ranjit continues to be a thorn on Bishambers side, he is killed, leaving behind a sorrowing Namrata and two children. Namrata pledges to avenge his death.  

==Cast==
* Rajinikanth as Inspector Ranjit Singh SP Namrata Singh
* Prem Chopra as Bishambar Prasad
* Charanraj as Charanraj Prasad
* Shafi Inamdar as Murlidhar
* Beena Banerjee as Beena
* Parikshat Sahni as Dutta Babu
* Aparajita Mohanty as Sharda Devi Dutta
* Jagdish Raj as Commissioner Pandey
* Dalip Tahil as DSP Ravi Khanna Senthil as Mutthu Swamy Bindu as Veena Singh
* Alok Nath as Major Rajan Singh
* Harsheeta Mehta as student
* Manjeet Kullar

== Music ==
Music: Bappi Lahiri, The Song Gori Kabse Hui Jawaan, Jeena Agar Jaruri Hai Hai To and the duet on Rajinikanth and Rekha Naina Ho Gaye Baware was a Hit.

===Soundtrack===
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !
|-
| 1
| "Gori Kabse Hui Jawaan"
|-
| 2
| "Jeena Agar Jaruri Hai To"
|-
| 3
| "Dil Tera Jaan Teri"
|-
| 4
| "Naina Ho Gaye Baware"
|-
| 5
| "Phool Kabhi Jab"
|-
|}

==References==
 

==External links==
*  

 
 
 
 


 