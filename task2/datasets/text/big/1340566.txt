The Squeeze (1977 film)
 
 
{{Infobox film
| name           = The Squeeze
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Michael Apted
| producer       = Stanley OToole
| writer         = 
| screenplay     = Leon Griffiths
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Stacy Keach
* David Hemmings Edward Fox
}}
| music          = David Hentschel
| cinematography = Dennis C. Lewiston
| editing        = John Shirley
| studio         = Warner Bros. Pictures
| distributor    = Warner Bros. Pictures
| released       =   
| runtime        = 104 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
}}
 1977 United British gangster Bill James Minder creator Leon Griffiths.
 Edward Fox and David Hemmings. Irish actor Stephen Boyd was also featured in a major (and final) role as a gangster.

==Production==

To create realism in the film, the producers of The Squeeze enlisted an ex-gangster called Bob Ramsey to act as a contact between the film unit and the local underworld in order to cut down on harassment and keep everybody unharmed and happy.  This was due to location shooting taking place in rather undesirable areas where the criminals were.

Local people in the area were hired as extras.

==Reception==

The Squeeze is now considered a forgotten crime masterpiece that usually gets mentioned in books on British cinema of the 1970s but usually gets overlooked in the light of Get Carter (1971) & The Long Good Friday (1981).

On its release tabloid paper The News of The World called The Squeeze, "A nail-biting thriller...".

==Cast==
* Stacy Keach ....  Jim Naboth
* David Hemmings ....  Keith Edward Fox ....  Foreman
* Stephen Boyd ....  Vic
* Carol White ....  Jill
* Freddie Starr ....  Teddy
* Hilary Gasson ....  Barbara
* Rod Beacham ....  Dr. Jenkins
* Stewart Harwood ....  Des Alan Ford ....  Taff
* Roy Marsden ....  Barry
* Leon Greene ....  Commissionaire
* Lucinda Duckett ....  Sharon
* Alison Portes ....  Christine
* Marjie Lawrence ....  Beryl

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 