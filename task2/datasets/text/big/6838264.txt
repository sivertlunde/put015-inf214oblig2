Funny Bones
 
 
{{Infobox film
| name = Funny Bones
| image = funnybones.jpg
| caption = Theatrical release poster
| director = Peter Chelsom
| producer = Peter Chelsom Simon Fields
| writer = Peter Chelsom Peter Flannery Lee Evans Leslie Caron Richard Griffiths Sadie Corre Oliver Reed George Carl Freddie Davies Ian McNeice John Altman
| cinematography = Eduardo Serra Martin Walsh
| studio = Hollywood Pictures Buena Vista Pictures
| released =  
| runtime = 128 minutes
| country = United Kingdom United States
| language = English
| budget = 
| gross = $532,268 52,088 admissions (France)   at Box Office Story 
}} John Altman and the cinematography by Eduardo Serra.
 Las Vegas Lee Evans, Leslie Caron, Richard Griffiths, Sadie Corre, Oliver Reed, George Carl, Freddie Davies and Ian McNeice.

==Plot==
Tommy Fawkes is the son of comedy legend George Fawkes. After his own Las Vegas comedy act flops with his beloved father in the audience, Tommy returns to Blackpool, England, where he spent the summers of his childhood.

Disguised with a new identity, Tommy intends to seek out unique performers and purchase their acts. During this time, Tommy encounters his fathers old comedy partners, Bruno and Thomas Parker. Once great performers, they now work as ghouls on a ghost train at Blackpool Pleasure Beach Circus.

Brunos son Jack is a brilliant comic,  but psychologically troubled. He has also been manipulated by a corrupt policeman known as Sharkey into stealing valuable wax eggs from smugglers. Tommy meets Jacks mother Katie, and even though Tommy is in disguise, she suspects that he is somehow connected to the family.

Tommy eventually realises that his father stole his original act from the Parker brothers. He then reveals himself to be Tommy Fawkes and Katie tells him that Jack is his half-brother. Tommy phones his father about the revelation and George gets on the next plane to Blackpool.

As part of their reconciliation, George arranges for the Parkers to top the bill at a Blackpool Tower Circus event. However, Jack is still hounded by Sharkey and cannot perform. During an elaborate Egyptian act, Katie gets rid of Sharkey via a sarcophagus, which is then kidnapped by the smugglers. The wax eggs (Chinese inscription on egg is read "Eight Immortals") contained a mystical, ancient Chinese rejuvenating powder. Jack had previously placed the powder within a makeup tin, which Bruno and Thomas accidentally use, helping them to perform brilliantly.

Toward the end of the show, Jack is seen climbing a giant pole chased by a policeman. Jack smacks the policeman in the face with a glass bottle and the policeman begins to fall.  When the identity of the policeman is revealed to be Tommy, the slow motion filming prolongs the feeling of dread in seeing Tommys struggle to not fall.

In the agonising last moments, Jack clasps Tommys hand and saves him.  The circus audience clasps wildly with relief.  Jack then says to Tommy, "Theyre beginning to like you." Jack laughs and Tommy, suddenly no longer afraid, gazes at the audience and finds the feeling he was searching for all his life.

==Cast==
* Oliver Platt as Tommy Fawkes
* Jerry Lewis as George Fawkes Lee Evans as Jack Parker
* Leslie Caron as Katie Parker
* Richard Griffiths as Jim Minty
* Sadie Corre as Poodle Woman
* Oliver Reed as Dolly Hopkins
* George Carl as Thomas Parker
* Freddie Davies as Bruno Parker
* Ian McNeice as Stanley Sharkey
* Christopher Greet as Lawrence Berger
* Ruta Lee as Laura Fawkes Peter Gunn as Nicky

==Home media==
Funny Bones was released on DVD on September 2, 2003.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 