Lawyer Vera
{{Infobox film
| name           = Lawyer Vera
| image          = 
| caption        = 
| director       = Martin Frič
| producer       = 
| writer         = Václav Wasserman Vlasta Zemanová
| starring       = Truda Grosslichtová
| music          = 
| cinematography = Ferdinand Pecenka
| editing        = Jan Kohout
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

Lawyer Vera ( ) is a 1937 Czech comedy film directed by Martin Frič.   

==Cast==
* Truda Grosslichtová as JUDr. Vera Donátová
* Ruzena Slemrová as Ruzena Donátová Theodor Pištěk as Jindrich Donát
* Oldrich Nový as Petr Tygr Kucera
* Bedrich Veverka as Eman Pálený
* Ladislav H. Struna as Dlouhý Gusta Rudolf Deyl as Consul Raboch
* Stanislav Neumann as Richard Raboch
* Jaroslav Marvan as Grocery Store Proprietor
* Darja Hajská as Marie
* Karel Veverka as JUDr. Basus
* Václav Trégl as The Solicitor  
* Cenek Slégl as Karel Benda
* Jirí Hron as Jan Vrzal
* Václav Vydra as Emil Cipra (as Václav ml. Vydra)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 