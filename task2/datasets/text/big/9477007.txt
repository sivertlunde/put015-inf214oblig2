Veneno para las hadas
{{Infobox Film |
  name     = Veneno para las hadas|
  image          = Veneno Para Las Hadas.jpg|
  caption        = DVD cover|
  writer         = Carlos Enrique Taboada   |
  starring       = Ana Patricia Rojo   Elsa María Gutiérrez  Leonor Llausás  Carmen Stein   Anna Silvetti  |
  director       = Carlos Enrique Taboada   |
  producer       =  |
  distributor    = |
  released   = 1984 in film|1984|
  runtime        = 90 min.| Spanish |
  country = Mexico |
  music          = |
  budget         = |
}} 1984 Mexican horror film, written and directed by Carlos Enrique Taboada. Two little girls dabble in witchcraft, resulting in steadily increasing mayhem.

==Plot==
Veronica is a young orphan living alone with her invalid grandmother and her nanny.  The many witchcraft tales told by Veronicas nanny have taken hold in Veronicas imagination, and she often pretends to be a real witch to combat the girls who mock her at her parochial school.

Veronica befriends Flavia, a new student who comes from a wealthy family but is lonely and generally ignored at home. Veronica tries to convince her new friend that she is a witch.  When Flavia doesnt believe her, Veronica takes credit for a series of strange events by telling Flavia that she caused them with black magic. Flavia finds herself growing more and more terrified of the power Veronica seems to wield, to the point of giving Veronica her most cherished possessions and obeying her whenever she asks.

Veronicas demands culminate in a request to be taken along on Flavias family vacation to a remote ranch in the country. There, Veronica plans to make a poison for the fairies which are said to be the natural enemy of witches.  Flavia becomes even more terrified at the thought of Veronicas power once the fairies are destroyed.  When Veronica demands Flavia surrender her beloved pet dog, Flavia is finally compelled to stop Veronica in a horrifying climax.

==Cultural references==
"Veneno para las hadas" is the name of a song on Steven Wilsons first solo album, Insurgentes (album)|Insurgentes.

  is also the name of a Mexican darkwave/ambient band.

==External links==
* 
*  
* 
* 
* 

 
 
 
 
 
 
 
 
 


 
 