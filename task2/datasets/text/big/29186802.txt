Marcel the Shell with Shoes On
{{Infobox film
| name           = Marcel the Shell with Shoes On
| image          = 
| alt            = 
| caption        = 
| director       = Dean Fleischer-Camp
| producer       = Dean Fleischer-Camp
| writer         = Jenny Slate   Dean Fleischer-Camp
| starring       = Jenny Slate
| music          = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 3 minutes 20 seconds
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Marcel the Shell with Shoes On is a stop motion animated short film about Marcel, an anthropomorphic shell. It is a collaboration between writer/director Dean Fleischer-Camp and writer/actress Jenny Slate.
 AFI FEST 2010 where it was awarded Best Animated Short   and was an official selection of the 2011 Sundance Film Festival. It won the Grand Jury and Audience Awards at the New York International Childrens Film Festival.

==Format==
The concept of the film is that Marcel is being interviewed in his home by a documentary filmmaker. Responding to off-camera questions, Marcel speaks about his activities, hobbies, hopes, and disappointments.  The film is edited to resemble rough cut footage.

==Sequel==
A sequel, Marcel the Shell with Shoes On, Two, was posted to YouTube on 14 November 2011. The third installation, "Marcel the Shell with Shoes On, Three," was posted to YouTube on 20 October 2014.

==Characters==
* Marcel - a cute and friendly shell.
* Documentarian - An anonymous filmmaker, heard interjecting occasionally to prompt Marcel. According to Marcel, "the community" or "Marcel" has given him the nickname "Windy".

==References==
 

 
*2. http://www.interviewmagazine.com/blogs/culture/2010-11-11/jenny-slate-bored-to-death/
*3. http://content.usatoday.com/communities/popcandy/post/2010/08/exclusive-snl-star-jenny-slate-chats-about-marcel-the-shell/1
*4. http://www.deathandtaxesmag.com/32771/jenny-slate-on-bored-to-death-marcel-the-shell-and-life-after-snl/

 

==External links==
 
*  on Vimeo
* 

 
 
 