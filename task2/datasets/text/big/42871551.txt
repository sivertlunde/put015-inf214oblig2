D III 88
{{Infobox film
| name = D III 88
| image =
| image_size =
| caption =
| director = Herbert Maisch   Hans Bertram
| producer =  Fred Lyssa 
| writer =  Wolf Neumeister     Hans Bertram
| narrator =
| starring = Christian Kayßler   Otto Wernicke   Heinz Welzel   Hermann Braun
| music = Robert Küssel   
| cinematography = Georg Krause   Heinz von Jaworsky  
| editing =  Carl Otto Bartning      
| studio = Tobis Film
| distributor = Tobis Film
| released = 26 October 1939    
| runtime = 
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
D III 88 (sometimes written as DIII 88) is a 1939 German drama film directed by Herbert Maisch and Hans Bertram and starring Christian Kayßler, Otto Wernicke and Heinz Welzel. It was made as a propaganda film with the support of Luftwaffe chief Hermann Göring, and was the last of a series of Nazi aviation films to be made before the outbreak of the Second World War. It was one of the most commercially successful films released during the Nazi era.  It was praised by Joseph Goebbels as "an irreproachable film of national destiny".  The title, referring to the serial number of the Albatros D.III flown by one of the characters in the First world war, was an attempt to re-inforce the propaganda link between the modern Luftwaffe and that of the First World War. 

==Synopsis==
Two extraordinary young pilots engage in a competitive rivalry and also fight over the same girl. In an effort to show off both fly into a dangerous storm, damaging their planes and are suspended from duty. They are finally convinced by their commanding officer, a veteran of the First World War, to use their talents in a more disciplined way for their country.

==Cast==
*   Christian Kayßler as Oberleutnant Mithoff  
* Otto Wernicke as Oberwerkmeister Bonicke  
* Heinz Welzel as Obergefreiter Fritz Paulsen  
* Hermann Braun as Obergefreiter Robert Eckhard   Adolf Fischer as Gefreiter Zeissler  
* Horst Birr as Monteur Hasinger   Karl Martell as Lt. Ludwig Becker  
* Fritz Eberth as Funker Lindner  
* Carsta Löck as Bauernmagd Lina  
* Paul Otto as General 
* Paul Bildt as Stabsarzt der Flugstaffel  
* Hans Bernuth as Flieger  
* Ernst Dernburg as Adm. beim Manöver 
* Erich Dunskus as Bauer  
* Heinz Engelmann as Lt. Frank  
* Ilse Fürstenberg as Bäuerin  
* Malte Jäger as 1. Funker  
* Ferry Reich as 2. Funker  
* Josef Kamper as Bauernknecht  
* Hilde Land as Kantinenwirtin  
* Guenther Markert as Marineoffizier  
* Hans Meyer-Hanno as Kantinenwirt  
* Egon Vogel as Sanitäter  
* Eduard von Winterstein as Landarzt  
* Wolfgang Staudte as Marineoffizier  Walter Gross as Funker  
* Theo Brandt as Fliegeroffizier

== References ==
 

== Bibliography ==
* Paris, Michael. From the Wright Brothers to Top Gun: Aviation, Nationalism, and Popular Cinema. Manchester University Press, 1995. 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 