Mischievous Miss
{{Infobox film
| name           = Mischievous Miss
| image          = 
| image_size     = 
| caption        = 
| director       = Erich Schönfelder 
| producer       =  Karl Ritter
| narrator       = 
| starring       = Julius Falkenstein   Dina Gralla   Albert Paulig   Josefine Dora
| music          = 
| editing        =
| cinematography = Axel Graatkjær
| studio         = Olympia Film 
| distributor    = Süd-Film
| released       = 28 January 1930
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent comedy on location in Portugal. The films art direction was by Heinrich Richter.

==Cast==
* Julius Falkenstein as Baron Eggloffsburg 
* Dina Gralla as Daisy, seine Tochter 
* Albert Paulig as Onkel Egon 
* Josefine Dora as Tante Josefine 
* Wolfgang von Schwindt as Onkel Adolf 
* Emmy Wyda as Tante Agathe 
* Arthur Duarte as Rittmeister Frank Neuhaus 
* Robin Irvine as Harry Spring, dessen Freund 
* Siegfried Berisch as Stallmeister Arisch 
* Max Nosseck as Bob, ein Stalljunge 
* Ernst Behmer as Pächter Schreck 
* Gustl Herrmann as Seine Frau 
* Else Reval as Kathi Ferkl, Gutsmamsell 

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 