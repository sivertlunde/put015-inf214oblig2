Puran Bhagat (film)
{{Infobox film
| name           = Puran Bhagat
| image          = 
| image_size     = 
| caption        = 
| director       = Debaki Bose
| producer       = New Theatres
| writer         = 
| narrator       = 
| starring       = K. L. Saigal Uma Shashi K. C. Dey Nawab
| music          = R. C. Boral
| cinematography = Nitin Bose
| editing        = 
| distributor    =
| studio         = New Theatres
| released       = 1933
| runtime        = 159 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1933 Hindi devotional biopic film from New Theatres Ltd. Calcutta.    The film was Debaki Boses debut direction in Hindi.    The film starred K. L. Saigal, Uma Shashi, Kumar (actor)|Kumar, Molina Devi, K. C. Dey and Tarabai.    The film was based on a popular Punjabi devotional story which was a change for New Theatres from their regular films based on traditional Bengali stories, Saints and novels. It was a "great success all over India".   

==Synopsis==
After several years of penance and prayers a male child is born to King Silwan of Sialkot and his wife Queen Ichhara. However the Raj Guru predicts that they must not see their son till he’s 16 years old or the child will die. The child, Puran, is given to Raj Guru to be fostered. Mahipat the Senapati (King’s General), has looked forward to becoming the king for a long time as the king had no son. But the birth of Puran thwarts his desires and over the years he spreads rumours about Puran’s wild antics, which also reach the king. Time comes for Puran to return home. He finds his father has taken a much younger wife, Luna. 

Luna becomes infatuated with Puran and lets him know. Puran is at once repelled and horrified. He rejects her advances and leaves. The spurned Luna tells the king that Puran tried to molest her. The irate King has Puran’s arms cut off, and has him thrown in a well. A miracle occurs where the saint, Guru Gorakhnath, arrives and sews up Puran’s hands and keeps him as his disciple.  Gorakhnath tests Puran’s ascetism by sending him to an Amazonian style Kingdom. The Queen, Rani Sundara, falls in love with Puran. After Puran has left his father’s kingdom, the Senapati attacks and has King Silwan deposed. King Silwan sends for help from Rani Sundara. Goraknath insists that Puran along with the Sundara go to the help of his father. They succeed in restoring King Silwan to his throne. Puran after marrying Sundara remembers his vow of ascetism and leaves the kingdom renouncing his throne.

==Cast==
*K. L. Saigal
*Uma Shashi Kumar
*Molina Devi
*K. C. Dey
*Tarabai
*Bikram Kapoor
*Anwari Begum
*Ansari 

==Release== Bengali literature and Chandidas (Bengali) (1932) had been a big success. Now he decided to focus on the legend of the Punjabi Prince and saint Puran Bhagat. The film with its "enchanting music" "was a box-office hit". The punjab Cinema Art Society acclaimed it as a "masterpiece". In Lahore the film ran to packed houses.   

==Music==
The film is regarded as R. C.  Borals  first  hit  Hindi  movie.    Boral used new techniques in his music and combined elements from classical Indian Raagas with folk music starting a trend followed by other music directors.   

===Songs===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Min
|-
| 1
| "Din Neeke Beete Jaate Hain" 
| K. L. Saigal
| 3.13
|-
| 2
| "Avsar beeto jaat praani"
| K. L. Saigal 
| 3.01
|-
| 3
| "Raadhe Raani De Daaro Naa Bansari Mori Re" 
|  K. L. Saigal
| 3.31
|-
| 4
| "Bhaju Main To Bhaav Se Shiri Giridhari" 
| K. L. Saigal
| 2.58 
|-
| 5
| "Jaavo Jaavo E Mere Saadhu"
| K. C. Dey
| 3.09
|-
| 6
| "Kya Kaaran Hai Ab Rone Kaa"
| K. C. Dey
|
|-
|}

==References==
 

==External links==
* 

 
 
 