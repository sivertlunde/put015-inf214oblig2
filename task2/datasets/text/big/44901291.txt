United in Anger: A History of ACT UP
{{Infobox film
| name           = United in Anger: A History of ACT UP
| image          = File:Pink_triangle_up.svg
| alt            = Pink Triangle Symbol
| caption        = 
| producer       = Jim Hubbard, Sarah Schulman
| writer         = Ali Cotterill, Jim Hubbard
| editing        = Ali Cotterill
| music          = 
| cinematography = James Wentzy
| studio         =  
| distributor    =  
| released       = 6 June 2012 USA
| runtime        = 90 mins
| country        = USA
| language       = English
}}
United in Anger: A History of ACT UP is a 2012 documentary film about the beginning and progress of the AIDS activist movement from the perspective of the people fighting the epidemic. Archival footage with oral histories of members of ACT UP depicts the history of civil disobedience against corporate greed, social indifference, and government negligence in the face of AIDS. Producers Jim Hubbard and Sarah Schulman created a documentary film that captures the efforts of ACT UP to remove the stigma associated with AIDS, fast track experimental drug research and testing, and provide a context for the devastating effects of the epidemic. Film includes several actions by ACT UP:  Seize Control of the FDA,  Stop the Church,  and Day of Desperation. 

==Plot==
HIV arrives in the United States. People, mostly gay men, start dying. The US government ignores it.  The Church condemns homosexuals.  The Pharmaceutical industry produced expensive drugs.  People keep dying.    Love, grief and outrage lead to the formation of ACT UP in March 1987. United in Anger: A History of ACT UP documents ACT UPs use of direct activism, civil disobedience, inroads and outroads to raise awareness and affect change on a national level.

==Archival resources==
* ACT UP Oral History Project

==See also==
*How to Survive a Plague: Documentary on the history of ACT UP

==References==
 

==External links==
*  
*   
*   in Variety

 
 
 
 
 
 
 
 
 
 