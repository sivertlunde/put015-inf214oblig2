Our Relations
 
 
{{Infobox film
  | name = Our Relations
  | image = L&H_Our_Relations_1936.jpg
  | caption = Theatrical release poster 
  | director =  Harry Lachman
  | producer = Stan Laurel   Hal Roach Felix Adler   Richard Connell 
  | starring = Stan Laurel   Oliver Hardy 
  | music = Leroy Shield
  | cinematography = Rudolph Maté
  | editing = Bert Jordan
  | distributor = Metro-Goldwyn-Mayer
  | released =  
  | runtime = 72 58"
  | language = English
  | country = United States
  | budget = 
}}
Our Relations is a 1936 feature film starring Laurel and Hardy, produced by Stan Laurel for Hal Roach Studios.

==Plot== Ollie characters and as Stan and Ollies twin brothers Bert and Alf.

==Cast==
*Stan Laurel - Stan/Alf Laurel
*Oliver Hardy - Ollie/Bert Hardy Alan Hale - Joe Grogan
*Sidney Toler - Captain of SS Periwinkle
*Daphne Pollard - Mrs. Daphne Hardy
*Betty Healy - Mrs. Betty Laurel
*Jimmy Finlayson - Finn
*Iris Adrian - Alice
*Lona Andre - Lily
*Ralf Harolde - Gangster boss
*Noel Madison - Second gangster
*Arthur Housman - Drunk
*Harry Bernard - policeman

==Production==
In most of the Laurel and Hardy films, their usual Stan and Ollie characters are a pair of hopeless but likable dimwits, often just barely able to earn a living. In Our Relations, Stan and Ollie are respectable citizens with wives and steady employment. It is their seafaring twin brothers, Alf Laurel and Bert Hardy, who are dim-witted incompetents sailors aboard the S.S Periwinkle.

On board, Alf and Bert wear seafaring garb. Once ashore, they dress in "civilian" clothes—down to the traditional derbies—making them nearly indistinguishable from their brothers. Stan always wore a bow-tie, while Oliver wore the more conventional type. This is reversed for the brothers, with Alf wearing the usual style and Bert wearing the bowtie. Music cues also help differentiate between the twins; Laurel & Hardys theme song, "Dance of the Cuckoos", plays when Stan and Ollie appear; the Sailors Hornpipe plays when Alf and Bert are onscreen.
 Felix Adler and Richard Connell.

==Legacy==
In 2000, the Dutch revivalist orchestra The Beau Hunks collaborated with the Metropole Orchestra to re-create composer Leroy Shields soundtrack to Our Relations from original sheet music that had been discovered in a Los Angeles archive in 1994 and 1995.

==References==
 

== External links ==
* 
* 
* 
*  Text of the short story which was the basis for the film.

 
 

 
 
 
 
 
 
 
 

 