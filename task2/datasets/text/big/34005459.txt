Yevadu
 
 
 
{{Infobox film
| name           = Yevadu
| image          = Yevadu poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Vamsi Paidipally
| producer       = Dil Raju
| screenplay     = Vakkantham Vamsi Vamsi Paidipally
| story          = Vakkantham Vamsi
| starring       = Ram Charan Shruti Haasan Amy Jackson
| music          = Devi Sri Prasad
| cinematography = C. Ram Prasad
| editing        = Marthand K. Venkatesh
| studio         = Sri Venkateswara Creations
| released       =  
| runtime        = 165 minutes
| country        = India
| language       = Telugu
| budget         =  350 million 
| gross          =  471 million 
}}
 Telugu action Sai Kumar, Jayasudha and Rahul Dev play supporting roles. The film was produced by Dil Raju under the banner Sri Venkateswara Creations. Vakkantham Vamsi co-wrote the films script with Paidipally. Devi Sri Prasad composed the films music, while Marthand K. Venkatesh was the films editor.

The films story was partially inspired by John Woos 1997 film, Face/Off, and revolves around two strangers, Satya and Charan. The film was made on a budget of  350 million, and was officially launched on 9 December 2011. Principal photography began on 27 April 2012 and lasted until 22 July 2013. The majority of the film was shot in Vishakhapatnam and Hyderabad, India|Hyderabad, mostly in Ramoji Film City, while two songs were shot in Switzerland and Bangkok.

Released during the season of the Makar Sankranti festival, the film received decent feedback from critics. It grossed more than  600 million and collected a distributor share of  471 million, becoming one of the highest grossing Telugu films of 2014. The film was dubbed into Malayalam as Bhaiyya My Brother the same year.

== Plot ==
Satya and Deepthi are lovers living in Vishakhapatnam. A dreaded don, Veeru Bhai, lusts for her. When Deepthis parents are killed, she and Satya escape. They board a bus en route to Hyderabad, but the bus is stopped unexpectedly and boarded by three men: Veerus henchman Deva, Veerus brother Ajay, and a corrupt police inspector, Shravan. They kill Deepthi in front of Satya, and he suffers an almost-fatal stab wound. Before the three leave, they set fire to the bus, leaving Satya to his death. However, Dr. Shailaja saves Satya by giving him a new face and skin. Ten months later, Satya awakes from his coma.

Under the pseudonym of Ram, Satya returns to Vishakhapatnam where he meets a model named Shruti, whom Veeru likes. Befriending Shruti, he takes advantage of his new unknown face, and lures Deva to a half-constructed apartment, where Satya kills him. A photo of Deva is discovered by assistant commissioner Ashok Varma, which has been marked with a "1". Satya then files a report on Deepthi with the police, stating that she has been missing for ten days. Since Shravan knows that Deepthi was killed ten months earlier, he becomes suspicious of Satya in his new persona, and follows him to a mall, where Satya kills him. During the investigation on Shravans death, Satya presents himself to Varma as an eyewitness. When asked to provide a description of the killer, Satya describes his former face. Satya then manipulates Ajay to fall in love with Shruti and gets the two of them to pose in compromising positions for some pictures, under the pretense that they will be used as a promotion to assist Shruti in getting an opportunity to audition for the female lead in a film. Instead, Satya sends copies of the pictures to Veeru, in order to turn him against both Ajay and Shruti. Satya then convinces Ajay that the only way to save Shruti from Veerus ire is to kill Veeru, which would also allow Ajay to gain his brothers position. However, Veerus henchmen kill Ajay, but Veeru is himself killed by Satya, who reveals his true identity. He apologizes to Shruti for using her, before leaving.

His vengeance fulfilled, Satya departs from Vishakhapatnam. On the way, a stranger attacks Satya, but is killed. Suspecting that the attack might have something to do with his new face, Satya visits Dr. Shailaja. When they meet, Shailaja confesses that she has given Satya the face of her deceased son, Charan; she then goes on to explain the circumstances surrounding Charans death. Charan was a happy-go-lucky rich graduate who hung out with his friends and girlfriend, Manju. One of his friends, Shashank, questions the local don, Dharma, regarding his illegal acquisition of lands by exploiting the slum people. Threatened, Dharma kills Shashank, after which Charan begins to raise support among the local population to revolt against Dharma. Again threatened, Dharma approaches another one of Charans friends, Sharath, promising to establish his political career if he kills Charan. Charan and Sharath go to Vishakhapatnam to attend a wedding. On their return, they board the same bus as Satya and Deepthi. When the bus was stopped, Sharath pulls a knife on Charan and the two engage in a fight. Veerus men simultaneously enter the bus. Deepthi is killed inside the bus while Charan and Sharaths struggle has led them outside the bus, where Charan is stabbed to death. Charans body is sent to Shailaja, who after observing Satyas urge to live decides to transplant her sons face onto Satya, rather than simple plastic surgery. Before Shailaja could inform Satya about Charan, Satya had left the hospital.

Leaving Shailaja, Satya visits the slum, under the guise of Charan. After meeting the locals, he decides to avenge Charans death. First, he meets Sharath in a political meeting and, using the shock of Charan seemingly being alive, makes him kill Dharma. Satya makes the slum people follow Sharath to Dharmas house. Sharath stabs Dharma, who in turn kills Sharath. However, the mob, which has followed Satya, finally kills Dharma. Afterwards, Manju is shown living in a distant land, mourning Charans death. The film ends with Satya meeting her.

== Cast ==
* Ram Charan as Satya / Ram / Charan
* Allu Arjun as Satya (Cameo appearance)
* Shruti Haasan as Manju
* Amy Jackson as Shruthi
* Kajal Aggarwal as Deepthi (Cameo appearance) Sai Kumar as Dharma
* Rahul Dev as Veeru Bhai
* Jayasudha as Dr. Shailaja Ajay as Ajay
* Murali Sharma as The Assistant Commissioner of Police
* Kota Srinivasa Rao as Central Minister Shashank as Shashank (Cameo appearance)
* Raja as Sharath
* Brahmanandam as Satyas illegal tenant
* Vennela Kishore as Charans friend
* Subbaraju as Dharmas henchman
* Scarlett Mellish Wilson in the item number Ayyo Paapam

== Production ==

=== Development ===

Dil Raju wanted to produce a technically driven film and selected a script written by Vamsi Paidipally.  Raju announced the project officially in early October 2011, as well as announcing that Ram Charan would star as the films protagonist. The project would be produced under the banner Sri Venkateswara Creations, with the details, including its cast and crew, announced shortly thereafter.  Before sharing the script with Charan, Paidipally worked on it for nearly two months, choosing the action genre after having directed family dramas like Brindavanam (film)|Brindavanam (2010).  Devi Sri Prasad was confirmed as the films music director in early November 2011, marking his first collaboration with both Paidipally and Charan. The music sittings were held at Goa. 

The films production was officially launched on 9 December 2011, in Hyderabad;  the films title was announced as Yevadu and a first look poster was unveiled on the same day.  Paidipally based his screenplay from a story written by Vakkantham Vamsi.  Anand Sai was the films art director and completed his work by September 2013.  Abburi Ravi contributed to the dialogues.  C. Ram Prasad was the films cinematographer, while Marthand K. Venkatesh was the films editor. 

=== Casting ===
{{multiple image
 
| align     = left
| direction = vertical
| footer    =Samantha Ruth Prabhu (top) was selected as the main female lead but was subsequently replaced by Shruti Haasan (bottom) due to creative differences.
| width     = 
 
| image1    = Samantha Ruth Prabhu at 60th South Filmfare Awards 2013.jpg
| width1    = 200
| alt1      = 
| caption1  =

 
| image2    = ShrutiHaasan TeachAIDS Interview3.png
| width2    = 200
| alt2      = 
| caption2  =
}}
Ram Charan was signed as the protagonist in October 2011. Allu Arjun was cast for a crucial cameo appearance in a role which would bring a twist in the film; the producers wanted an energetic and spirited actor in the part.  Samantha Ruth Prabhu was selected to play the female lead to be paired with Charan, and was expected to join the production in October 2012.  For the other pairing with Charan, in his alter-ego role, Amy Jackson was selected to play the role of Shruti.  This marked her debut in Telugu cinema, and she participated in various workshops to prepare herself for the role before joining the shoot.  Jackson revealed that she was afraid to be seen as a glamorous young woman aiming to be an actress; she worked hard to get her diction right and called the workshop an orientation programme to get her used to the Telugu language. For her role, she practiced yoga, functional training, and went for jogs in and around Bandra bandstand in Mumbai, before rejoining the films sets in Hyderabad. 

Kajal Aggarwal was signed for a cameo appearance and was paired with Arjun.  Her inclusion was in doubt due to scheduling conflicts,  but she eventually accepted the role, calling it a short and sweet one.  Shweta Bhardwaj was approached to perform an item number in early May 2012, which she confirmed the news at the time, but added that although she was approached, she was not officially cast in the film.  Scarlett Mellish Wilson was selected for an item number, which was supposed to be her first in Telugu; However the film, Cameraman Gangatho Rambabu, was released earlier, making it her debut.  
 Sai Kumar Shashank and Prabhas Srinu were seen in cameo roles.  

=== Filming ===
{{multiple image
 
| align     = right
| direction = vertical
| footer    =Vishakhapatnam (Up) and Ramoji Film City (Down), where the film was significantly shot.
| width     = 
 
| image1    = Vizag-arealview.jpg
| width1    = 200
| alt1      = 
| caption1  =

 
| image2    = Ramoji Film City.jpg
| width2    = 200
| alt2      = 
| caption2  =
}}
Principal photography began on 27 April 2012.  Ram Charan was involved in two projects simultaneously, Yevadu and Zanjeer (2013 film)|Zanjeer (2013). After completing initial filming on Zanjeer, Charan joined the cast and crew of Yevadu on 8 May 2012, to shoot the item number,  as well as scenes featuring him, Amy Jackson, and others. After this first phase of production, Charan was scheduled to go back to Bangkok to complete the filming schedule of Zanjeer, before rejoining the Yevadu production on 7 August 2012.  Filming continued in Hyderabad until mid-September 2012 after which production moved to Vishakhapatnam on 14 September 2012.  By early October 2012, 40 percent of the film was completed, consisting mostly of the first half.  Shruti Haasan joined the films sets on 24 January 2013. 

After a short break, filming recommenced on 5 Mach 2013,  and by the end of the month, 90 percent of the films speaking sequences were completed.  Several action sequences were shot at Ramoji Film City in mid April 2013, under the supervision of Peter Hein.  By then, the portions containing Arjun and Aggarwal were almost complete.  The films team left for Switzerland on 20 May 2013, in order to film a song sequence.  Charan and Haasan were on location in Zurich, Switzerland to film one of the songs, which was completed by 27 May 2013. 

Primary filming recommenced on 29 May 2013, in Hyderabad, with Amy Jackson rejoining the production. The filming was expected to be wrapped up by 20 June 2013.  In mid-June 2013, a song featuring Charan was shot at Keesara Strech, Ramoji Film City.  Later, a song choreographed by Shekhar, and featuring Charan and Haasan was shot at Annapurna 7 Acres Studio.  Meanwhile, a song featuring Charan and Jackson was shot in Bangkok.  The song Freedom which was choreographed by Johnny, was shot in early July 2013 at Ramoji Film City. 

Principal photography came to an end on 22 July 2013 after the completion of Haasans scenes.  However, in late September 2013, some parts of the film were reportedly re-shot and new scenes were added, in an effort to speed up the pace of the film, after the completion of censor formalities. 

== Themes and influences ==
Despite the films writer, Vakkantham Vamsi|Vamsi, denying it in early June 2013,  many critics believed the film was heavily inspired from John Woos 1997 action film, Face/Off, as well as bearing similarities to the Telugu films Chhatrapati (film)|Chhatrapati (2005) and Vikramarkudu (2006), both directed by S. S. Rajamouli.     While critic Sangeetha Devi Dundoo called the film a combination of Face/Off and Vikramarkudu,  another critic, Karthik Pasupulate, wrote, "This movie this isnt a rip off of Face/Off. We dont have filmmakers who have the required creativity and technical know how to make half decent adaptation of that 1997 action thriller. The director just borrows the central idea to dish out another silly revenge drama." 

== Soundtrack ==
 
 Krishna Chaitanya and Sri Mani each wrote the lyrics for one song.  Aditya Music acquired the audio rights.  The soundtrack was released on 1 July 2013. 

== Release ==
The film was initially slated for a 14 July 2013 release, which was Charans first wedding anniversary.  After few delays, the films release date was announced as 31 July 2013 to coincide with the fourth anniversary of Ram Charans 2009 film Magadheera.  After several delays, the film was rescheduled to open the same day as Pawan Kalyans Attarintiki Daredi, but was postponed again to 21 August 2013.   The Central Board of Film Certification gave the film an A certificate because of its violent action sequences.  However, due to protests in Seemandhra over the Telangana State formation, the release of both the films were delayed. Raju scheduled the release on either Diwali or Christmas 2013, again postponing the release from 10 October to avoid another release conflict with Attarintiki Daredi.   Due to Piracy issues, Attarintiki Daredi released on 27 September 2013 which forced Yevadu to enter the theaters as a December release. 

Dil Raju confirmed that the films original and Malayalam dubbed version would release simultaneously on 19 December 2013.  Regarding the films release, Dil Raju said "Im planning to release the film on Dec 19 if the Telangana bill is not passed. I havent decided on postponing the film, but if the bill is passed and it is likely create some agitation, then I plan to release the film during Sankranti next year in January".  The film finally released on 12 January 2014 as a  , Veeram and Jilla at the worldwide box office.  Sathyam Cinemas distributed the film in Chennai, where it released in 16 screens.  A special paid premier show was held on 11 January 2014 at 9:00 PM and the money made from the premier show was donated to the construction of Lord Venkateswara Swamy temple in Mallepally. 

=== Marketing ===
The films posters were designed by Anil and Bhanu of AB Core Design.  The first-look poster was unveiled on 24 March 2013, featuring Charan with a rugged look.  The first-look teaser of 36 seconds was unveiled on 27 March 2013.   The theatrical trailer was launched on 1 July 2013, along with the films soundtrack, and received a positive response from the audience.  The promos for the songs "Nee Jathaga Nenundali", "Oye Oye" and "Pimple Dimple" were released on 1, 3 and 21 July 2013, respectively.    The 16 second promo for the song "Freedom" was unveiled on 31 December 2013, which received positive reviews. 

Dil Raju started the films promotional activities 12 days before the films release, and planned an advertisement campaign on television, newspapers, magazines, and movie portals.  Charan released the second trailer of 85 seconds on 3 January 2014, at the Sandhya 70MM theater in RTC X Roads.   Hindustan Unilever became associated with the film by paying  10 million for co-branding activity in terms of hoardings, television, radio, and print. MediaCorp did the marketing tie-ups for the film.  The films official app was launched on 5 January 2014.  While promoting the film, Shruti Haasan was admitted to the Apollo Hospital in Hyderabad, and underwent treatment for suspected food poisoning.  The promo of the song "Cheliya Cheliya" was unveiled on 7 January 2014. 
 Tirumala Temple on 17 January 2014, and made obeisance to Venkateswara, before interacting with the audience at Group Theatres.  A press meet addressing the audience was held at Vijayawada on the next day, with Vamsi, Dil Raju, Sai Kumar, Shashank, L. B. Sriram, Raja and others in attendance.  The promo of the song "Ayyo Paapam" was unveiled on 13 March 2014. 

=== Legal issues === FEFSI Vijayan and Stunt Silva, failed to comply with this mandate, Andhra Pradesh stuntmen obstructed the films production. 

K. Nagendra Prasad, a former councillor of Yemmiganur in Kurnool district, lodged a complaint with the police regarding a poster, alleging that it featured an image of a half-nude Amy Jackson. Suits were filed, naming Ram Charan, Amy Jackson, Dil Raju, the films presenter Anitha, co-producer Sirish, the photographer, and the owner of the Raghavendra theatre which screened the film, citing  section 292 of the IPC, as well as section 3 of the Indecent Representation of Women (Prohibition) Act of 1986.  Vamsi defended that Jacksons still used in the poster was from a song sequence which had no obscenity in it and called the charges baseless.  Justice K. G. Shankar of the High Court stayed the criminal cases on 28 July 2014. 
 FIR in CID began investigation in May 2014. They questioned administrators of the websites which uploaded the photos, and the photographers who reportedly supplied the photos to them. However, it was uncertain whether uploading the photographs, since they were taken in a public place, could be termed as illegal, and sought a legal opinion on how to go about the investigation. 

=== Home media ===
The films television rights were acquired by MAA TV for an undisclosed price.  The television rights of the dubbed Malayalam version, Bhaiyya My Brother, were sold to Mazhavil Manorama for  6.5 million, which was the highest price ever paid for a Telugu film dubbed in Malayalam.  The Indian DVD and Blu-ray were marketed by Aditya Videos, and were released in June 2014.  

== Reception ==

=== Critical response === International Business Times India, the film received "decent feedback" from critics.   

Y. Sunita Chowdary of The Hindu felt that the film was made only for the mass audience. She criticised the films revenge sequences, calling them "non-stop and prolonged" but praised Devi Sri Prasads music and C. Ram Prasads cinematography.    Sridhar Vivan of Bangalore Mirror gave the film 3 out of 5 stars and wrote, "For those watching Yevadu, the first half ends quite shockingly as the director tries to wrap up the movie in the first leg itself. It is a treat for Cherrys fans and for others, its strictly average stuff". He felt that the violence, lack of comedy and a predictable turn of events affected the films narration.   

IndiaGlitz rated the film 3 out of 5 and wrote, "Yevadu has not one but two revenge stories. The first revenge story has not much going for it - perhaps the director deemed it fit not to take it seriously. Followed by this no-brainer, there comes the films actual story, giving us respite and much-needed entertainment in good measure".    Sify also rated the film 3 out of 5 and wrote, "Yevadu provides novelty in the beginning episodes promising a different commercial film but ends up as a regular revenge drama. Has many cliched moments but it is also a film that works to some extent. A masala fare".   

Karthik Pasupulate of The Times of India was yet another to rate the film 3 out of 5 and he panned the film, criticising its blatant similarities to Face/Off. He added that the action choreography was "undecipherable" and called Sai Kumars character as "one of the most over the top villain characters ever" adding that he would certainly "break the outer limit of human hearing threshold".    Shekhar of Oneindia Entertainment rated the film 3 out of 5 as well, stating, "Yevadu is a good action entertainer with all commercial ingredients. Ram Charan Teja is the showman in the film. If you are looking for entertainment without any message, it is good treat for you this Sankrathi".   

=== Box office ===

==== India ====
The film became the biggest opener for Ram Charan by collecting  86.5 million on its first day, making the film with the fourth highest opening day grosses of all time, surpassing  .  While the films trade witnessed a drop on its second day, earning  40 million, it improved on its third day, earning  45 million, bringing its three day total to  173.8 million, overtaking the totals of   and Ramayya Vasthavayya (2013).    The film collected a distributor share of  200 million in four days at AP Box office.  The film collected  278.5 million in six days, surpassing the first week share of Seethamma Vakitlo Sirimalle Chettu, and was expected to surpass its first week worldwide share of  335 million.  The film also established a first week record in Nellore, and the east and west areas of Andhra Pradesh and Karnataka, by collecting  16.3 million,  26.7 million,  22.2 million and  40.6 million, respectively. However, it missed a first week record in Ceeded and Vishakhapatnam by a small margin. It also took the second spot in the list of the top 10 films with highest first week shares from AP. 

The film collected  337.5 million in 9 days, and more than  60 million from other areas, including Karnataka, the rest of India, and overseas territories, enabling it to cross the  400 million mark at the worldwide Box office, becoming Ram Charans fourth film to cross that mark.  The film grossed  350.5 million in 11 days.  The film collected  363.2 million by the end of its second week at the AP Box office and  440 million worldwide.  The film collected  376 million in 16 days at AP Box office, and was expected to reach the  500 million mark worldwide.  The film completed a 25 day run in early February 2014, collecting more than  380 million at the AP Box office.  The film collected  393.4 million at the AP Box office and  471 million worldwide during its lifetime, and was declared as one of the Ram Charans biggest hits. 

==== Overseas ====
According to Taran Adarsh, the film collected  14.6 million in its opening weekend in the United States.  In its first five days in the US, the film collected  22 million.  By the end of its second week, the films performance was disappointing; and it only collected a total of $330,000 as of 20 January 2014, while 1: Nenokkadine collected $1 million during that same span of time.  Overseas, the film grossed a total of  17 million during its lifetime. 

== Notes ==
 

== References ==
 

== External links ==
*  
 
 

 
 
 
 
 
 
 
 
 
 