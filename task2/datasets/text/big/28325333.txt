Brought by the Sea
{{Infobox film
| name           = Brought by the Sea
| image          = BroughtbytheSeaFilmPoster.jpg
| image size     =
| alt            = 
| caption        = Theatrical poster
| director       = Nesli Çölgeçen
| producer       = Ömer Can Berna Akpınar
| writer         = Ersin Kana
| narrator       = 
| starring       = Onur Saylak Jordan Deniz Boyner Ahu Türkpençe Sümer Tilmaç
| music          = Kemal Sahir Gürel İrşad Aydın Erdal Güney Ayşe Önder
| cinematography = Aydın Sarıoğlu
| editing        = Ahmet Can Çakırca
| studio         = 
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = US$38,949
| preceded by    = 
| followed by    = 
}}
Brought by the Sea ( ) is a 2010 Turkish drama film, directed by Nesli Çölgeçen, about an ex-cop who accidentally kills an African immigrant and retreats with a guilty conscience back to his hometown. The film, which went on nationwide general release across Turkey on  , has been shown at International film festivals in Istanbul, Moscow and Adana, where it won the Grand Jury "Yılmaz Güney" Best Picture prize.      

==Production== Aegean towns of Ortaca, Dalyan and Dalaman, Turkey. 

==Synopsis==
Halil (Onur Saylak) is a cop who accidentally kills an African illegal immigrant; he resigns and retires with a guilty conscience back to his hometown. Jordan (Jordan Deniz Boyner) is a five-year-old boy from Ghana whose mother is killed in an accident in Dalyan as they try to cross illegally to Greece. When Halil finds Jordan, their destinies intersect.

==Release==

===General release===
The film opened on general release in 34 screens across Turkey on   at number 25 in the Turkish box office chart with an opening weekend gross of US$8,790.   

===Festival screenings===
* 29th International Istanbul Film Festival (April 3–18, 2010)      
* 32nd International Moscow Film Festival (June 17–26, 2010)    
* 17th Adana "Golden Boll" International Film Festival   

==Reception==

===Box office===
The film was in the Turkish box office charts for ten weeks and has made a total gross of US$38,949. 

===Reviews===
Emine Yıdırım, writing in Todays Zaman, says this is, "a film which aims to say a lot", and, "indeed underlines the dire situation of the thousands of refugees that try to cross the borders of Turkey and Greece", but, "while achieving some of its ambitions, specifically those of the political and worldly context, Brought by the Sea unfortunately disappoints on some very crucial emotional points."   

===Awards===
* 17th Adana "Golden Boll" International Film Festival (September 20–26, 2010) Grand Jury "Yılmaz Güney" Best Picture (won)   

== See also ==
* 2010 in film
* Turkish films of 2010
* Cinema of Turkey

==References==
 

==External links==
*   for the film (Turkish)
*  

 
 
 
 
 