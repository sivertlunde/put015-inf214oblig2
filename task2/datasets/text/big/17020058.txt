Brothers Under the Skin
{{Infobox film
| name           = Brothers Under the Skin
| image          = Brothersundertheskin1922-lobbycard.jpg
| image size     =
| caption        = Lobby card
| director       = E. Mason Hopper
| producer       = Samuel Goldwyn
| writer         = Grant Carpenter Peter Bernard Kyne
| narrator       = Pat OMalley Helene Chadwick
| music          =
| cinematography = John J. Mescall
| editing        =
| distributor    = Goldwyn Pictures
| released       =  
| runtime        = 60 minutes; 5 reels
| country        = United States Silent (English intertitles)
| budget         =
}}
 silent comedy film directed by E. Mason Hopper. This picture survives in the Turner archives but is incomplete.  

==Plot==
As described in a review in a film publication,  Newton Craddock (OMalley), a shipping clerk who makes $30 per week, loves his wife Millie (Chadwick) even though she is running him into debt through her extravagance. Thomas Kirtland (Kerry) has the same problem with his wife Dorothy (Windsor) even though he makes $30,000 per year. After a fight with his wife, Newton decides to kill himself but is stopped by some wharf workers. Newton is given $5 to deliver a package to the Kirtlands apartment and, after finding the door open, lets himself in. He sees some of Dorothys expensive gowns hanging there and exchanges them for the cheap gowns that he had purchased as a peace offering to his wife. He also helps himself to some liquor and sits down, only to find that the Kirtlands have returned home. Dorothy is suspicious of her husband and during an exchange of strong words notes the gowns. Newton then comes forward to explain what happened. Thomas refuses to be submissive any longer and dictates the new policy of the household. Newton returns to his home with the same strategy and finds his wife eager to bend to his will. Happiness then reigns in both households.

==Cast== Pat OMalley - Newton Craddock
* Helene Chadwick - Millie Craddock
* Mae Busch - Flo Bulger
* Norman Kerry - Thomas Kirtland
* Claire Windsor - Dorothy Kirtland
* William Haines - Bit Part (uncredited)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 