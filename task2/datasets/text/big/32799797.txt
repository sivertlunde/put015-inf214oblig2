Inconceivable (2008 film)
{{Infobox film
| name           = Inconceivable
| image          =Inconceivablefilmposter.jpg
| caption        =Promotional poster
| director       = Mary McGuckian
| producer       = Mary McGuckian Jeff Abberley Martin Katz
| writer         = Mary McGuckian
| starring       = Jennifer Tilly Andie MacDowell Geraldine Chaplin Elizabeth McGovern Colm Feore Kerry Fox Amanda Plummer
| music          = Kevin Banks
| cinematography = Mark Wolf
| editing        = David Freemantle Ted Seaborn
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Canada UK
| language       = English
| budget         = 
| gross          =
}}
Inconceivable is a 2008 satirical drama about the test-tube baby industry. The film was written and directed by Mary McGuckian.

==Plot== Las Vegas Assisted Reproductive Technology clinic. Eight of the nine women are inseminated and become pregnant, except Salome (Tilly) who manages to conceive naturally.

After the births, investigative journalist Tallulah (McGovern) notices a striking resemblance between the toddlers. She comes to believe that Dr. Freeman swapped donor sperm for his own. A preliminary hearing is held and the process of the nine women is recalled. 

==Cast==
*Jennifer Tilly as Salome Sally Marsh 
*Andie MacDowell as Charlotte Lottie Louise Du Bose
*Geraldine Chaplin as Frances Church-Chappel 
*Elizabeth McGovern as Lesley Banks
*Colm Feore as Dr. Jackson Charles Jack Freeman
*Kerry Fox as Kay Stephenson
*Amanda Plummer as Tallulah Tutu Williams
*Michael Eklund as Marlon Bell
*John Sessions as Finbar Darrow
*Jordi Mollà as Victor - The Clinical Coordinator 
*David Sutcliffe as John Du Bose
*Lothaire Bluteau as Malcolm Blay
*David Alpay as Mark Henderson
*Donna DErrico as Elsa Roxanne Gold Oona Chaplin as Laura Chappel
*Owen Teale as Richard Newman
*Greta Scacchi (uncredited)

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 