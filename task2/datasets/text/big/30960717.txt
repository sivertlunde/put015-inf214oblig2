The Squeaker (1937 film)
 
{{Infobox film
| name           = The Squeaker
| image          = "The_Squeaker"_(1937).jpg
| image size     =
| caption        = U.S. theatrical poster
| director       = William K. Howard
| producer       = 
| screenplay     = Ted Berkman  (as Edward O. Berkman) Bryan Edgar Wallace  (scenario) (as Bryan Wallace)
| based on       = novel by Edgar Wallace Sebastian Shaw  Ann Todd
| music          = Miklós Rózsa
| cinematography = Georges Périnal Russell Lloyd
| studio         = London Film Productions
| distributor    = United Artists  (UK)
| released       = 6 November 1937  (UK)
| runtime        = 77 min.
| country        = United Kingdom English
| budget         =
}} British crime Sebastian Shaw and Ann Todd.  Edmund Lowe reprised his stage performance in the role of Inspector Barrabal.    It is based on the novel The Squeaker by Edgar Wallace. The Squeaker is underworld slang for Informant|informer.     The film is sometimes known by its U.S. alternative title Murder on Diamond Row. 

==Plot==
Londons thieves are at the mercy of a super Fence (criminal)|fence, who is in on every big jewellry robbery in the city. If the thieves wont split the loot with him, The Squeaker shops them to the Police. A disgraced ex-detective believes there may be an opportunity to clear his name if he can capture The Squeaker.

==Cast==
* Edmund Lowe - Inspector Barrabal Sebastian Shaw - Frank Sutton
* Ann Todd - Carol Stedman
* Tamara Desni - Tamara
* Robert Newton - Larry Graeme
* Allan Jeayes - Inspector Elford
* Alastair Sim - Joshua Collie
* Stewart Rome - Police Superintendent Marshall
* Mabel Terry-Lewis - Mrs. Stedman Gordon McLeod - Mr. Field
* Alf Goddard - Sergeant Hawkins Danny Green - Safecracker
* Michael Rennie - Medical Examiner

==Critical reception==
TV Guide wrote, "(it) has its moments, but is bogged down by the unnecessary characterizations, some occasionally inept lensing, and slow-paced direction" ;  while the Radio Times wrote, "Edgar Wallaces classic whodunnit has been reworked into an efficient crime story by producer Alexander Korda...Confined within starchy studio sets, William K Howard directs steadily, but the removal of that touch of mystery leaves him with precious little to play with, to the extent that he has to bolster the action with protracted love scenes between Lowe and Ann Todd. Robert Newton and Alastair Sim put in pleasing support appearances" ;  and Leonard Maltin wrote, "classy cast in first-rate Edgar Wallace mystery."  

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 