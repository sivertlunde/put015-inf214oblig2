Real Life (2004 film)
{{Infobox Film
| name           = Real Life
| image          = Real Life 2004 poster.jpg
| image_size     = 
| caption        = 
| director       = Panos H. Koutras
| producer       = 
| writer         = Panos Evagellidis, Panos H. Koutras
| narrator       = 
| starring       = Nikos Kouris Themis Bazaka Marina Kalogirou Anna Mouglalis Maria Panouria Odysseas Papaspiliopoulos Yiannis Diamantis 
| music          = Mikael Delta
| cinematography = Elias Kostandakopoulos
| editing        = 
| distributor    = Playtime (Greece) Wild Bunch (worldwide) 
| released       = November 5, 2004 (Greece)
| runtime        = 111 min.
| country        = Greece France French Greek Greek
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2004 Greece|Greek drama film written and directed by Panos H. Koutras. 

==Plot==

Aris, 27 years old, returns home after a long absence. His mother, a rich, eccentric and lonely woman lives with her secretary, Sylvia and her loyal gardener Christos, who is mute, in a house famous both for its view in Acropolis and for its swimming pool, supposed to be the deepest in Europe. As Aris falls in love with a poor girl, Alexandra, and confronts his past, a wild fire threatens Athens.    

==Cast==
*Nikos Kouris ..... Aris
*Themis Bazaka ..... Kalliga
*Marina Kalogirou ..... Alexandra
*Maria Panouria ..... Sylvia
*Anna Mouglalis ..... Joy
*Yiannis Diamantis ..... Hristos

==References==
  

==External links==
* 
* 

 

 
 
 
 
 

 