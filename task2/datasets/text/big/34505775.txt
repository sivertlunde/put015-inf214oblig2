Love's Old Sweet Song (1923 film)
 
{{Infobox film
| title          = Loves Old Sweet Song
| image          =
| caption        =
| director       = J. Searle Dawley
| producer       = Lee DeForest
| writer         = Augustus Bertilla Jacques Byrne
| narrator       =
| starring       = Louis Wolheim Donald Gallaher Una Merkel
| music          = James Lynam Molloy|J. L. Molloy (song "Loves Old Sweet Song")
| cinematography = Freeman Harrison Owens
| editing        =
| studio         = Norca Pictures DeForest Phonofilm
| released       =  
| runtime        = 20 minutes
| country        = United States English
| budget         =
| gross          =
| awards         =
}} DeForest Phonofilm Helen Weir, Helen Lowell, Ernest Hilliard, and Una Merkel in her film debut.

==Production background==
This was one of the few two-reel films produced by Lee DeForest in Phonofilm due to problems with changeovers when the film was projected in theaters. In June 1923, DeForest and the films cinematographer Freeman Harrison Owens became embroiled in a legal battle over the Phonofilm process and patents. 

==References==
 

==See also==
*Phonofilm
*Sound film
*Abraham Lincoln (1924 film) another  two-reel Phonofilm

==External links==
* 
* 


 
 
 
 
 