Tres Mujeres en la Hoguera
{{Infobox Film
| name           = Tres Mujeres en la Hoguera
| image          = Tremujers.JPG
| caption        = Scene from the film Abel Salazar
| producer       = 
| writer         = Luis Alcoriza Carlos Valdemar
| narrator       = 
| starring       = Maricruz Olivier Pilar Pellicer Daniela Romo Rogelio Guerra
| music          = 
| cinematography = 
| editing        = 
| distributor    = Conacite Uno 1976
| runtime        = 108 min
| country        = Mexico Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Mexican motion drama and Thriller (genre)|thriller. It was filmed in 1976.

== Synopsis  ==
Alex and Mane invite a couple (Gloria and Susy) to their house in the Pacific coast of Mexico (Puerto Vallarta) to spent a spring break there. In a game of constant lust, the women tangle in a plot of eroticism, betrayal, ambition, passion and death.   .

== Cast ==
* Maricruz Olivier ... Gloria
* Pilar Pellicer ... Mane
* Maritza Olivares ... Susy
* Rogelio Guerra ... Alex
* Enrique Muñoz
* Carlos Bravo y Fernández
* Daniela Romo ... Peggy (uncredited)

== External links ==
*  

 
 
 
 
 
 

 