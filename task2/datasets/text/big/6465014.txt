Three Fugitives
{{Infobox Film
| image          = Threefugitives.jpg
| caption        = Promotional film poster
| alt            = A man holding a smaller man in his arms, with a little girl sitting on the smaller man. 
| name           = Three Fugitives
| director       = Francis Veber
| producer       = Lauren Shuler Donner
| writer         = Francis Veber
| starring       = {{Plain list | 
* Nick Nolte
* Martin Short
* James Earl Jones
* Sarah Doroff
}} David McHugh
| cinematography = Haskell Wexler
| editing        = Bruce Green
| studio         = Touchstone Pictures  Silver Screen Partners IV Buena Vista Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $15 million 
| gross          = $40,586,886 
}}

Three Fugitives is a 1989 crime film|crime-comedy film written and directed by Francis Veber, starring Nick Nolte and Martin Short, and featuring Sarah Rowland Doroff, James Earl Jones and Alan Ruck in supporting roles. It is a remake of Les Fugitifs, a 1986 French comedy starring Gérard Depardieu and Pierre Richard also directed by Veber.

== Plot ==
Lucas (Nolte) has been in prison for armed robbery. On the day he is released, he gets taken hostage by Ned Perry (Short), an incompetent, novice criminal who robs a bank (to get money for treatment for his ill daughter, Meg) at the moment Lucas just happens to be there.

Detective Duggan (Jones) assumes they must be in it together and sets about tracking them down.  Several chases, an accidental shooting, treatment from a crazy vet who thinks hes a dog and other capers follow, all the while Lucas trying to ditch his idiotic companion and prove his own innocence.

Whilst avoiding the law, the two form an unlikely partnership to help cure the silent Meg and make good their escape. They rescue Meg from the care home shes in (with Perry nearly ruining the whole affair with his clumsiness) and flee for Canada, pretending to be a married couple with a son. 

All appears to end well. However, in the closing scene, Perry enters a Canadian bank to change some currency only to find himself taken hostage by a different bank robber in the same manner he originally kidnapped Lucas. Because of this unexpected development, Lucas does not need to say goodbye to Meg, with whom he has formed a bond.

==Reaction==
The film received negative reviews from critics. Rotten Tomatoes gives the film a score of 15% based on 13 reviews. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 