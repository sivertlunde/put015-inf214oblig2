Left Right and Centre
{{Infobox film
| name           = Left Right and Centre
| image size     = 
| image	=	Left Right and Centre FilmPoster.jpeg
| caption        = 
| director       = Sidney Gilliat
| producer       = Frank Launder   Sidney Gilliat   Leslie Gilliat
| writer         = Val Valentine   Sidney Gilliat
| narrator       = 
| music          = Humphrey Searle Gerald Gibbs
| editing        = Gerry Hambling
| starring       = Ian Carmichael Patricia Bredin Richard Wattis  Eric Barker
| distributor    = British Lion Film Corporation
| released       = 1959
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded by    = 
| followed by    = 
}} 1959 cinema British satirical comedy film directed by Sidney Gilliat and starring Ian Carmichael, Patricia Bredin, Richard Wattis, Eric Barker and Alastair Sim. It was produced by Frank Launder.   A political comedy, it follows the events of a by-election in a small English town.

==Plot== Conservative candidate Labour Party.

Travelling up on the train to Earndale, the two candidates meet and while she quickly works out who he is, he remains ignorant of her true identity. To try to show off he begins to tell her about his selection for the seat and how he expects to win. He describes his opponent as a bluestocking. He also inadvertently reveals embarrassing details to her such as the fact that he has scarcely been to Earndale in his life and that his family once controlled the seat as a rotten borough. Once they arrive at Earndale station, he is soon made aware of his mistake. The electoral agents of both candidates are furious to discover they have been fraternising on the train.

Wilcot goes to visit his uncle, and finds him to be an eccentric who has turned his country house into a money-making operation for visiting coach parties of tourists. It appears that he has engineered Robert Wilcots selection as a candidate in order to spark public interest in the election, boosting his own business. It is also clear that the political contest is added to by the enmity of the two electoral agents the Tory Harding-Pratt and Labours Bert Glimmer.
 stump the two candidates keep running into each other around Earndale, at one point during a factory visit leading to a shouting match. Both begin to become entranced by the other, and become convinced they are falling in love. This comes to a head during the hustings at Wilcot Hall where they are caught embracing in the maze by their respective agents. Burying the hatchet, the two agents try to foil the potential romance. Despite repeated attempts to break up the candidates they continue a covert relationship.

==Cast==
* Ian Carmichael - Robert Wilcot
* Patricia Bredin - Stella Stoker
* Richard Wattis - Harding-Pratt
* Eric Barker - Bert Glimmer
* Alastair Sim - Lord Wilcot
* Moyra Fraser - Annabel
* Jack Hedley - Bill Hemmingway
* Gordon Harker - Hardy William Kendall - Pottle
* Anthony Sharp - Peterson George Benson - Egerton
* Leslie Dwyer - Alf Stoker
* Moultrie Kelsall - Grimsby Armfield
* Jeremy Hawke - TV interviewer
* Russell Waters - Mr. Bray
* Olwen Brookes - Mrs. Samson
* John Salew - Mayor
* Bill Shine - Basingstoke
* Erik Chitty - Deputy returning officer
* Redmond Phillips - Mr. Smithson
* Irene Handl - Mrs. Maggs John Sharp - Mr. Reeves

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 