She (1925 film)
 
{{Infobox film
| name           = She
| image          = SHE (1925), Movie Poster.jpg
| caption        = She (1925), Movie Poster
| director       = Leander de Cordova   G. B. Samuelson
| producer       = G. B. Samuelson Arthur A. Lee
| writer         = H. Rider Haggard (novel She) Walter Summers (scenario) H. Rider Haggard (intertitles)
| starring       = Betty Blythe Carlyle Blackwell
| music          = Louis Levy
| art direction  = Heinrich Richter
| cinematography = Sydney Blythe
| editing        =
| distributor    =
| released       = 1926 (US release) May 3, 1926 (Finland)
| runtime        = 9 reels (8,250 feet)
| country        = Germany   United Kingdom Silent (German, Finnish and/or English intertitles)
}}
She is a 1925  . According to the opening credits, the intertitles were specially written for the film by Haggard himself (he died in 1925, the year the film was made).

The book has been a popular subject for filmmakers in the silent and sound eras, with at least five short film adaptations produced in 1908, 1911, 1916, 1917, and 1919 respectively.  The 1925 version was the first feature length adaptation, although it was trimmed from its original 98-minute running time down to 69 minutes for US release. It is the most faithful of the three feature-length adaptations to date and follows the action, characters and locations of the original novel closely.

==Cast==
*Betty Blythe - Ayesha
*Carlyle Blackwell - Leo Vincy/Kallikrates
*Mary Odette - Ustane Tom Reynolds - Job
*Heinrich George - Horace Holly
*Jerrold Robertshaw - Billali
*Dorothy Barclay 
*Marjorie Statler - Amenartes
*Alexander Butler - Mahomet

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 