Sprung (film)
{{Infobox film
| name        = Sprung
| image       = SprungMoviePoster.jpg
| caption     = Theatrical Release Poster
| writer      = Rusty Cundieff Darin Scott Tisha Campbell Rusty Cundieff Paula Jai Parker Joe Torry
| director    = Rusty Cundieff
| producer    = Mark Amin Darin Scott
| distributor = Trimark Pictures
| released    = May 14, 1997
| runtime     = 105 minutes
| language    = English
| music       =
| awards      =
| budget      = $7,500,000 (estimate)
}}
 1997 comedy directed by Tisha Campbell, Joe Torry, and Paula Jai Parker. It grossed $7,575,028 at the US box office.

== Plot ==
Set in 1990s Pittsburgh. Montel (  seeking a wealthy husband with robotic determination, and her shy law clerk friend Brandy (Tisha Campbell-Martin). The couples pair off in predictable combinations, but while the brief encounter between Clyde and Adina quickly implodes when she discovers that Clydes success is an act and his Porsche is borrowed, the relationship between Montel and Brandy blooms into a true romance. Eventually, they decide to move in together, motivating Clyde and Adina to reunite in a selfish scheme to break their best friends up using whatever treacherous means they can employ.

== Cast == Tisha Campbell as Brandy 
* Rusty Cundieff as Montel 
* Paula Jai Parker as Adina 
* Joe Torry as Clyde 
* Moon Jones as Godzilla Nuts Suspect 
* Bobby Mardis as Foreign Suspect  John Witherspoon as Detective  Jennifer Lee as Veronica 
* Clarence Williams III as Grand Daddy 
* Loretta Jean as Brides Mother 
* Ronnie Willis as Party Guard 
* John Ganun as Patrol Officer #1 
* David McKnight as Patrol Officer #2 
* Ron Brooks as Watch Commander

== Soundtrack ==
 

== Reception ==
The movie had a mostly negative response with a 22% rating on Rotten Tomatoes. 

==References==
 

== External links ==
*  

 

 
 
 
 


 