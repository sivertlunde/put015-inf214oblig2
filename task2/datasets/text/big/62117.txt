Mr. Smith Goes to Washington
{{Infobox film
| name           = Mr. Smith Goes to Washington
| image          = smith_goes.jpg
| caption        = theatrical poster
| director       = Frank Capra
| producer       = Frank Capra
| based on       =   
| screenplay     = Sidney Buchman
| narrator       = Colin James Mackey Edward Arnold
| music          = Dimitri Tiomkin Joseph Walker, A.S.C.
| editing        = Gene Havlick Al Clark
| distributor    = Columbia Pictures
| released       =  
| runtime        = 129 minutes
| country        = United States
| language       = English
| budget         =     $1.5 million
| gross          = $9,000,000 
| running time   = 127 minutes
}}
  Edward Arnold, Thomas Mitchell and Beulah Bondi.

Mr. Smith Goes to Washington was nominated for 11   added the movie to the United States National Film Registry, for being "culturally, historically, or aesthetically significant".
 

==Plot== Edward Arnold), pressures Hopper to choose his handpicked stooge, while popular committees want a reformer, Henry Hill. The governors children want him to select Jefferson Smith (James Stewart), the head of the Boy Rangers. Unable to make up his mind between Taylors stooge and the reformer, Hopper decides to flip a coin. When it lands on edge – and next to a newspaper story on one of Smiths accomplishments – he chooses Smith, calculating that his wholesome image will please the people while his naïveté will make him easy to manipulate.

Junior Senator Smith is taken under the wing of the publicly esteemed, but secretly crooked, Senator Joseph Paine (Claude Rains), who was Smiths late fathers friend. Smith develops an immediate attraction to the senators daughter, Susan (Astrid Allwyn). At Senator Paines home, Smith has a conversation with Susan, fidgeting and bumbling, entranced by the young socialite. Smiths naïve and honest nature allows the unforgiving Washington press to take advantage of him, quickly tarnishing Smiths reputation with ridiculous front page pictures and headlines branding him a yokel|bumpkin.
 graft scheme political machine" and supported by Senator Paine.

Unwilling to crucify the worshipful Smith so that their graft plan will go through, Paine tells Taylor he wants out, but Taylor reminds him that Paine is in power primarily through Taylors influence. Through Paine, the machine in his state accuses Smith of trying to profit from his bill by producing fraudulent evidence that Smith already owns the land in question. Smith is too shocked by Paines betrayal to defend himself, and runs away.

Saunders, who looked down on Smith at first, but has come to believe in him, talks him into launching a filibuster to postpone the appropriations bill and prove his innocence on the Senate floor just before the vote to expel him. In his last chance to prove his innocence, he talks non-stop for about 24 hours, reaffirming the American ideals of freedom and disclosing the true motives of the dam scheme. Yet none of the Senators are convinced.

The constituents try to rally around him, but the entrenched opposition is too powerful, and all attempts are crushed. Owing to the influence of Taylors machine, newspapers and radio stations in Smiths home state, on Taylors orders, refuse to report what Smith has to say and even distort the facts against the senator. An effort by the Boy Rangers to spread the news in support of Smith results in vicious attacks on the children by Taylors minions.
 Harry Carey). Smith vows to press on until people believe him, but immediately collapses in a faint. Overcome with guilt, Paine leaves the Senate chamber and attempts to commit suicide, but is stopped by other senators. When he is stopped, he bursts back into the Senate chamber, loudly confessing to the whole scheme; that he should be expelled from the Senate, and affirms Smiths innocence.

==Cast==
  and Jean Arthur in a taxicab]]
* James Stewart as Jefferson "Jeff" Smith
* Jean Arthur as Clarissa Saunders
* Claude Rains as Senator Joseph Harrison "Joe" Paine Edward Arnold as Jim Taylor
* Guy Kibbee as Governor Hubert "Happy" Hopper Thomas Mitchell as "Diz" Moore
* Eugene Pallette as Chick McGann
* Beulah Bondi as Ma Smith
* H. B. Warner as Senate Majority Leader Harry Carey President of the Senate
* Astrid Allwyn as Susan Paine
* Ruth Donnelly as Mrs. Hopper Grant Mitchell as Senator MacPherson
* Porter Hall as Senator Monroe
* Pierre Watkin as Senate Minority Leader Charles Lane as "Nosey"
* William Demarest as Bill Griffith
* Dick Elliott as Carl Cook
* The Hopper Boys: Billy Watson
** Delmar Watson
** John Russell Harry Watson
** Gary Watson
** Baby Dumpling (Larry Simms)
* H. V. Kaltenborn
* Dub Taylor
Among unbilled veteran character actors seen in the film are Guy Kibbees brother, Milton Kibbee, who has a bit as a reporter, Lafe McKee and Matt McHugh of the McHugh acting family.  A number of the cast members and distinctive plot motifs would reappear in Its a Wonderful Life.

==Production==
 ]]
Columbia Pictures originally purchased Lewis R. Fosters unpublished story, variously called The Gentleman from Montana and The Gentleman from Wyoming, as a vehicle for Ralph Bellamy, but once Frank Capra came on board as director&nbsp;– after Rouben Mamoulian had expressed interest&nbsp;– the film was to be a sequel to his Mr. Deeds Goes to Town, called Mr. Deeds Goes to Washington, with Gary Cooper reprising his role as Longfellow Deeds.  Because Cooper was unavailable, Capra then "saw it immediately as a vehicle for Jimmy Stewart and Jean Arthur",  and Stewart was borrowed from Metro-Goldwyn-Mayer|MGM.  Capra said of Stewart: "I knew he would make a hell of a Mr. Smith... He looked like the country kid, the idealist. It was very close to him." Tatara, Paul.   TCM article. Retrieved: June 26, 2009. 

Although a youth group is featured in the story, the Boy Scouts of America refused to allow their name to be used in the film and instead the fanciful "Boy Rangers" was used. 
 MGM had submitted Fosters story to the censors at the Hays Office, probably indicating that both studios had interest in the project before Columbia purchased it. Joseph Breen, the head of that office, warned the studios: " e would urge most earnestly that you take serious counsel before embarking on the production of any motion picture based on this story. It looks to us like one that might well be loaded with dynamite, both for the motion picture industry, and for the country at large." Breen specifically objected to "the generally unflattering portrayal of our system of Government, which might well lead to such a picture being considered, both here, and more particularly abroad, as a covert attack on the Democratic form of government". and warned that the film should make clear that "the Senate is made up of a group of fine, upstanding citizens, who labor long and tirelessly for the best interests of the nation..."

Later, after the screenplay had been written and submitted, Breen reversed course, saying of the film that "It is a grand yarn that will do a great deal of good for all those who see it and, in my judgment, it is particularly fortunate that this kind of story is to be made at this time. Out of all Senator Jeffs difficulties there has been evolved the importance of a democracy and there is splendidly emphasized the rich and glorious heritage which is ours and which comes when you have a government of the people, by the people, and for the people ".
 Union Station and at the United States Capitol, as well as other locations for background use.  
 Press Club of Washington was reproduced in minute detail,   but the major effort went into a faithful reproduction of the Senate Chamber on the Columbia lot. James D. Preston, a former superintendent of the Senate gallery, acted as technical director for the Senate set, as well as advising on political protocol. The production also utilized the "New York street set" on the Warner Bros. lot, using 1,000 extras when that scene was shot. 

The ending of the film was apparently changed at some point, as the original program describes Stewart and Arthur returning to Mr. Smiths hometown, where they are met by a big parade, with the implication that they are married and starting a family.  In addition, the Taylor political machine was shown being crushed, Smith, riding a motorcycle, visits Senator Paine and forgives him, and a visit to Smiths mother. Some of this footage can be seen in the films trailer. 

==Impact== National Press Communist for American government.  While Capra claims in his autobiography that some senators walked out of the premiere, contemporary press accounts are unclear about whether this occurred or not, or whether senators yelled back at the screen during the film. 

 
It is known that Alben W. Barkley, a Democrat and the Senate Majority Leader, called the film “silly and stupid”, and said it “makes the Senate look like a bunch of crooks”.  He also remarked that the film was “a grotesque distortion” of the Senate, “as grotesque as anything ever seen! Imagine the Vice President of the United States winking at a pretty girl in the gallery in order to encourage a filibuster!” Barkley thought the film “...showed the Senate as the biggest aggregation of nincompoops on record!” Capra 1971, p. 287. 
 Pete Harrison, Neely Anti-Block Booking Bill, which eventually led to the breakup of the studio-owned theater chains in the late 1940s. Columbia responded by distributing a program which put forward the film’s patriotism and support of democracy and publicized the film’s many positive reviews. 

Other objections were voiced as well. Joseph P. Kennedy, the American Ambassador to Great Britain, wrote to Capra and Columbia head Harry Cohn to say that he feared the film would damage “America’s prestige in Europe”, and because of this urged that it be withdrawn from European release. Capra and Cohn responded, citing the film’s review, which mollified Kennedy to the extent that he never followed up, although he privately still had doubts about the film. 
 dubbed in certain European countries to alter the message of the film so it conformed with official ideology.   
 German occupied France in 1942, some theaters chose to show Mr. Smith Goes to Washington as the last movie before the ban went into effect. One theater owner in Paris reportedly screened the film nonstop for 30 days after the ban was announced.    ReelClassics.com. Retrieved: June 26, 2009. 

The critical response to the film was more measured than the reaction by politicians, domestic and foreign. The critic for the New York Times, for instance, Frank S. Nugent, wrote that "  is operating, of course, under the protection of that unwritten clause in the Bill of Rights entitling every voting citizen to at least one free swing at the Senate. Mr. Capra’s swing is from the floor and in the best of humor; if it fails to rock the august body to its heels&nbsp;— from laughter as much as from injured dignity&nbsp;— it won’t be his fault but the Senate’s, and we should really begin to worry about the upper house." 

Mr. Smith Goes to Washington has been called one of the quintessential whistleblower films in American history. Dr. James Murtagh and Dr. Jeffrey Wigand cited this film as a seminal event in U.S. history at the first “Whistleblower Week in Washington” (May 13–19, 2007).  
 You Can’t Take It With You (1938) had trumpeted their belief in the decency of the common man. In Mr. Smith Goes to Washington, however, the decent common man is surrounded by a venal, petty and thuggish group of crooks. Everyone in the film — except for Jefferson Smith and his tiny cadre of believers — is either in the pay of the political machine run by Edward Arnold’s James Taylor or complicit in Taylor’s corruption through their silence, and they all sit by as innocent people, including children, are brutalized and intimidated, rights are violated, and the government is brought to a halt”. 

Nevertheless, Smith’s filibuster and the tacit encouragement of the Senate President are both emblematic of the directors belief in the difference that one individual can make. This theme would be expanded further in Capras Its a Wonderful Life (1946) and other films.

==Awards and nominations==

===Academy Awards===
Mr. Smith Goes to Washington was nominated for 11 Academy Awards but only won one.   oscars.org. Retrieved: October 16, 2011. 

{| class="wikitable" border="1"
|-
! Award !! Result !! Winner
|- Outstanding Production Gone with the Wind 
|- Best Director Gone with the Wind 
|- Best Actor  ||   || James Stewart    Winner was Robert Donat - Goodbye, Mr. Chips (1939 film)|Goodbye, Mr. Chips 
|- Best Writing, Gone with the Wind 
|- Best Writing, Original Story  ||   || Lewis R. Foster
|- Best Supporting Harry Carey Thomas Mitchell - Stagecoach (1939 film)|Stagecoach 
|- Best Supporting Thomas Mitchell - Stagecoach (1939 film)|Stagecoach 
|- Best Art Gone with the Wind 
|- Best Film Al Clark Gone with the Wind 
|- Best Music, The Wizard of Oz 
|- Best Sound When Tomorrow Comes 
|}

===Other honors===
* Mr. Smith Goes to Washington was named as one of the best films of 1939 by the New York Times and Film Daily, and was nominated for Best Film by the National Board of Review.
* Jimmy Stewart won the 1939 New York Film Critics Circle Award for Best Actor.
* In 1989, Mr. Smith Goes to Washington was added to the United States National Film Registry as being deemed "culturally, historically, or aesthetically significant." 
 
; American Film Institute recognition
* 1998 AFIs 100 Years... 100 Movies #29
* 2003 AFIs 100 Years... 100 Heroes and Villains:
** Jefferson Smith, Hero #11
**Senator Joseph Paine, Villain - Nominated
* 2006 AFIs 100 Years... 100 Cheers #5
* 2007 AFIs 100 Years... 100 Movies (10th Anniversary Edition) #26

==Remakes==
In 1949, Columbia planned, but never actually produced, a sequel to Mr. Smith Goes to Washington, called Mr. Smith Starts a Riot. They also considered doing a gender-reversed remake in 1952, with Jane Wyman playing the lead role.  TV series]] 
A television series of the same name,   and Evan Almighty. The short-lived NBC political drama Mister Sterling (2003) was described as "a Mr. Smith Goes to Washington for the 21st century", with the show centering on an idealistic young senator from California, coming to grips with Washington and appointed by a scheming, underhanded governor. 

The VHS release of Ernest Rides Again featured the opening Saturday Night Live-based short "Mr. Bill Goes to Washington", a spoof of Mr. Smith Goes to Washington.

The March 10, 1940 broadcast of Jack Bennys NBC radio show featured a parody entitled "Mr. Benny Goes to Washington." 

The Simpsons episode Beyond Blunderdome includes a parodistic, fake remake of Mr. Smith Goes to Washington, authored by a fictional Mel Gibson with Homer Simpsons help. The fictional remake follows the same plot of the original (save for being set in the 21st century) until the final iconic "filibuster scene", replaced with a stock action scene in which a nearly exhausted Mr. Smith suddenly stands up and viciously slaughters every single senator, impaling Senator Payne with an American flag, destroying the Senate and beheading the President of the United States, mockingly quoting Marilyn Monroes Happy Birthday, Mr. President.

The Simpsons episode Mr. Lisa goes to Washington is inspired by, and contains several references to Mr. Smith Goes to Washington. The episode deals with Lisa Simpsons disillusionment with Washington government, following her winning a trip to Washington as a prize in an essay contest.

== See also ==
* Filibuster
* Machine politics
* Patriotism
* Political corruption
* Politics of the United States

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Capra, Frank. Frank Capra, The Name Above the Title: An Autobiography. New York: The Macmillan Company, 1971. ISBN 0-306-80771-8.
* Jones, Ken D., Arthur F. McClure and Alfred E. Twomey. The Films of James Stewart. New York: Castle Books, 1970.
* McBride, Joseph. Frank Capra: The Catastrophe of Success. New York: Touchstone Books, 1992. ISBN 0-671-79788-3.
* Michael, Paul, ed. The Great Movie Book: A Comprehensive Illustrated Reference Guide to the Best-loved Films of the Sound Era. Englewood Cliffs, New Jersey: Prentice-Hall Inc., 1980. ISBN 0-13-363663-1.
* Rosales, Jean. DC Goes To The Movies: A Unique Guide To The Reel Washington. Bloomington, Indiana: IUniverse, 2003. ISBN 978-0-595-26797-2.
* Sennett, Ted. Hollywoods Golden Year, 1939: A Fiftieth Anniversary Celebration. New York: St. Martins Press, 1989. ISBN 0-312-03361-3.
 

==External links==
 
 
*  
*  
*  
*  
*   from AmericanRhetoric.com
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 