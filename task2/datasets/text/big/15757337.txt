Ready (2008 film)
{{Infobox film
| name = Ready
| image = Ready telugu 2008 film 1.jpg
| Tagline(caption)= Denikaiyna
| director = Srinu Vytla
| producer = Ravi Kishore
| writer = Gopimohan Ram Genelia Sunil Chandra Chandra Mohan Brahmanandam
| music = Devi Sri Prasad
| cinematography = Prasad Murella
| editing = M.R. Varma
| country = India
| language = Telugu
| released = 19 June 2008
| run time = 174 min
| budget =
| gross =
}}
 2008 Telugu telugu action action romantic romantic comedy Ram and Genelia in Chandra Mohan, Brahmanandam and Jaya Prakash Reddy play other prominent roles. Tamanna made Guest Appearance. The music of the film is composed by Devi Sri Prasad. 
 Uthama Puthiran same name with Salman Khan and Asin in the lead as Ready (2011 film)|Ready  and was a high-grossing blockbuster as well.  

==Plot== Ravi Varma) who is depressed because his girlfriend is getting married to someone else. Chandu decides to stop the marriage and marry off her to Gopi. However, they kidnap Pooja (Genelia DSouza) instead of Gopis girlfriend due to a mistaken identity. After being chased by some hooligans - who are after Pooja - they escape with the exception of Chandu and Pooja who end up in forest. Chandu falls for Pooja who also reciprocates his feelings.

After learning that her best friend is not in town, Pooja decides to stay in Chandus home disguising herself as disciple of Raghupathis spiritual guru. Chandu is also welcomed back in his family. When he was about to propose to Pooja, Chandu gets to know about the true identity of Pooja. She is an NRI who came to visit her warring maternal uncles Peddinaidu (Kota Srinivasa Rao) and Chittinaidu (Jaya Prakash Reddy). Both of them try to force her into marriage with their respective sons to get a hold on her property she inherits. Chandu decides to transform them before marrying Pooja. He joins their auditor "McDowell" Murthy (Brahmanandam) as his assistant. Murthy introduces Chandu as his nephew to both Peddinaidu and Chittinaidu. When asked about Chandus experience in auditing, Murthy lies to them that he used to be an auditor in America to imaginary brothers "Chicago" Subba Rao and "Dallas" Nageswara Rao.

Chandu manipulates Murthys funny idea and makes his uncle Raghupathi and his father pose as Chicago Subba Rao and Dallas Nageswara Rao respectively. Swarajyams husband and Rajaram pose themselves as the friends of Poojas parents and make Peddinaidu think twice about his sons marriage with Pooja after they deliberately lie to him that her parents took a loan of 100 crores from World Bank. Chittinaidu also decides to call off his sons marriage with Pooja after Chandu plays a prank on him that Chicago Subba Raos daughter has more property than Pooja. After some hilarious twists and turns, Chandu and his uncle Raghupathi bring about a positive change in both Chittinaidu and Peddinaidu and Pooja marries Chandu after her uncles give their consent.

==Cast== Ram as Chandu - Chandu is a happy-go-lucky guy who is always willing to help others. He is shown to be the only inheritor of the property of his father.
* Genelia DSouza as Pooja - Pooja is an NRI who comes to India to visit her maternal uncles. She lost her parents in an air accident and she is brought up by her uncles.
* Brahmanandam as "McDowell" Murthy - Murthy is the auditor of both Peddi Naidu and Chitti Naidu. Nasser as Raghupathi/"Chicago" Subba Rao - Raghupathi is the head of his family and business. He loves acting and is shown in the film to have received Best Actor award from N. T. Rama Rao for a play in theater. Chandra Mohan as husband of Swarajyam (Janakis Father)
* Tanikella Bharani as Raghava/"Dallas" Nageswara Rao - Raghava is the younger brother of Raghupathi and father of Chandu.
* Kota Srinivasa Rao as Peddi Naidu - Peddi Naidu is the elder of the two brothers. He tries to marry off his son to Pooja to grab her property, in the process, even breaking up with his brother.
* Jaya Prakash Reddy as Chitti Naidu - Chitti Naidu is the younger of the two brothers. He also tries to force Pooja to marry his son, eyeing her property. He even spars with his brother on whose son should marry Pooja.
* Supreet as Narasimha - Narasimha is the right-hand man of Peddinaidu and one of the antagonists.
* Shafi as Nagappa - Nagappa is the son of Chitti Naidu and one of the antagonists. Sudha as Lakshmi (Raghupathis Wife)
* Pragathi as Rajyam (Chandus Mother) Sunil as Janaki and cousin of Chandu
* Rajitha as Swarajya Lakshmi (Janakis Mother)
* Srinivasa Reddy as Raju Garu (Chandus Friend)
* M. S. Narayana as Chinna Sastry
* Vajja Venkata Giridhar as RajaRam
* Satya Krishnan as RajaRams wife
* Dharmavarapu Subramanyam as Santosh Reddy (Happy Reddy)
* Balayya as Peddha Sastry Ravi Varma as Google Gopi (Chandus Friend)
* Surekha Vani as Chinna Chitti Naidus Mother Tamanna as Swapna (Chandus Cousin) (Guest Appearance)
* Navdeep as Swapnas Boyfriend (Guest Appearance)
* Master Bharat as Chinna Chitti Naidu
* Master Uday Nair as Chinna Peddi Naidu

==Crew==
* Producer: Sravanthi Ravi Kishore
* Director:: Srinu Vytla
* Story: Gopimohan
* Screenplay: Srinu Vytla
* Co-Director:Saikishore Macha
* Dialogues: Kona Venkat
* Cinematography: Prasad Murella
* Stunts: Peter Hein
* Music: Devi Sri Prasad
* Choreography: Suchitra, Sankar, Prem Rakshit
* Editing: M.R. Varma
* Art: Prakash

==Songs==
{{Infobox album
| Name = Ready
| Type = Soundtrack
| Cover = Ready 2008 ACD Cover.jpg
| Artist = Devi Sri Prasad
| Released =  Feature film soundtrack
| Length = 28:04
| Label = Aditya Music
| Producer = Devi Sri Prasad
| Last album = 
| This album = 
| Next album = 
}}

The film has six songs composed by Devi Sri Prasad. The lyrics were written by Sirivennela Seetarama Sastry, Ramajogayya Sastry and Benny. The audio was released on May 19, 2008 and the reviews were mostly positive.  

{{Track listing
| extra_column = Singer(s) Karthik | length1 = 5:06
| title2 = Ayyo Ayyo Ayyo Danayya!| extra2 = Priya, Franco & Benny | length2 = 4:30
| title3 = Mere Sajnaa! | extra3 = Kunal Ganjawala & Shreya Ghosal | length3 = 4:16
| title4 = Ninne Pelladukoni Rajaipota | extra4 = Ranjith & Kalpana| length4 = 5:22
| title5 = Naa Pedavulu Nuvvaithe | extra5 = Sagar & Poornima | length5 = 3:59
| title6 = Om Namaste Bolo | extra6 = Neeraj Sridhar & Divya | length6 = 4:21
}}

==Reception==
Ready received extremely positive reviews. Jeevi of Idlebrain.com rated the film at 3.25 out of a scale of 5 saying that it is "Stuffed with ample commercial elements, mainly vibrant comedy".  Teluguone.com rated the film at 3.75 out of 5 saying that the film is a "Neat family entertainer with an appreciable dose of clean comedy". It also says that though "The story and treatment has shades of Gudumba Shankar to a large extent, but good-natured comedy makes the movie quite enjoyable for a light viewing".  Ram was fabulous and Brahmanandam was well appreciated for his performance by Sify.com saying that "As McDowell Murthy, Brahmanandam brings the house down". 

==Awards==
;Filmfare Awards South
;;Nominations Best Film - Ravi Kishore Best Director - Srinu Vaitla Best Actor Ram
* Best Supporting Actor - Brahmanandam
;Nandi Awards
* Best popular feature film providing wholesome entertainment Best Male comedian - Bramhanandam Best Child Actor - Master Bharath

==Remakes== Raam - Director: Madesha
* Tamil:  
* Hindi:  

{| class="wikitable" style="width:50%;"
|- style="background:#ccc; text-align:center;"
| Ready (Telugu language|Telugu) (2008) || Raam (2009 film)|Raam (Kannada language|Kannada) (2009) || Uthamaputhiran (2010 film)|Uthamaputhiran (Tamil language|Tamil) (2010) || Ready (2011 film)|Ready (Hindi language|Hindi) (2011)
|- Ram || Puneet Rajkumar || Dhanush || Salman Khan
|-
| Genelia DSouza || Priyamani || Genelia DSouza || Asin Thottumkal
|}

==References==
 

==External links==
*  
 

 

 
 
 
 
 
 
 
 