Wolf Guy
 

{{Infobox animanga/Header
| name            = Wolf Guy
| image           = 
| caption         = 
| ja_kanji        = ウルフガイ
| ja_romaji       = Urufu Gai
| genre           = 
}}
{{Infobox animanga/Print
| type            = manga Kazumasa Hirai
| illustrator     = Hisashi Sakaguchi  
| publisher       = Bunkasha
| demographic     = Seinen manga|Seinen
| magazine        = Shuukan Bokura Magazine
| first           = 1970
| last            = 1971
| volumes         = 11
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = Ōkami no Monshō
| director        = Shōji Matsumoto
| producer        = 
| writer          = Jun Fukuda
| music           = Riichiro Manabe
| studio          = 
| released        = 1973
| runtime         = 
}}
{{Infobox animanga/Video
| type            = ova
| director        = Naoyuki Yoshinaga
| music           = Kenji Kawai
| studio          = J.C.Staff
| first           = December 17, 1992
| last            = June 21, 1993
| runtime         = 
| episodes        = 6
| episode_list    = 
}}
{{Infobox animanga/Print
| type            = manga Kazumasa Hirai 
Yoshiaki Tabata 
| illustrator     = Izumitani Ayumi  
| publisher       = Akita Shoten
| demographic     = Seinen manga|Seinen
| magazine        = Young Champion
| first           = 2007
| last            = 2012
| volumes         = 12
| volume_list     = 
}}
 

  is a Japanese  .

A live-action film adaptation, titled Ōkami no Monshō was released in 1973.

== Plot ==
Akira Inugami is a new exchange student at Hakutoku Middle School, but there is another side of him that is secret and hidden. He is a werewolf.

The story begins with the Homeroom teacher Akiko Aoshika walking home drunk, suddenly trips and is saved by Akira Inugami who then proceeds to walk away. Ms. Aoshika sees that he is a student and pursues him only to be jumped by Inugamis old school rivals. The gang attacks Inugami without mercy and seems beat him to death and yet he keeps getting back up. They then hit him with a car and he appears dead, they began to ruffle through his pockets. Ms. Aoshika faints soon after witnessing it and then Inugami takes his true form, the form of a Werewolf and destroys the gang. When Ms. Aoshika awakes, she finds the gang dead around her and is escorted to her school by the police only to find that Inugami is her new exchange student.

(Note: This plot by Tabata and Yugo differs greatly from the original novel written by Kazumasa Hirai.)

== Characters ==

;Akira Inugami (Inugami Akira 犬神 明)
The main protagonist of the story. An exchange student enrolled where Akiko Aoshika works at where he is constantly bullied and abused by other students (whom he usually ends up killing). His parents were killed when he was young by hunters, mainly because of their werewolf lineage. He can transform into a werewolf in his own will. He is quite powerful during full moons and is weak only during the days of new moons. He shows to be quite intelligent in matters that concern him. It is said that the reason he is bullied so badly is that, as a werewolf, normal humans have a natural subconscious fear of him. In the mangas closing chapters, Akira decides to stop taking the abuse of humans, seeing them all (with the exception of Akiko) as demons and beasts, and gleefully slaughters Haguros men. He is taken by government researchers after killing Haguro and is experimented on with apparently no hope of escape, but it is implied that Akira does eventually escape and reunite with Akiko in Alaska.

;Akiko Aoshika (Aoshika Akiko 青鹿 晶子)
The female protagonist in the story. She is a homeroom teacher who met Inugami prior to his introduction in her class. She was saved from being gang-raped at the first part of the manga by Akira Inugami, much to her surprise, Inugami is transferred to her classroom. She was a victim of rape when she was young, and was divorced from her husband. She escapes to Alaska after the final battle with Haguro and begins to live with several wolves hiding in the wild, waiting for Akira to find her.

;Dou Haguro(Haguro Dou 羽黒 獰)
His name means "Ferocious looking Black Feather". 
He is the main antagonist in the manga. Son of Tōmei yakuza group, Haguro is also a genius with an extremely well built body and super human reflexes. He is not only feared by students and teachers, but also by pros and yakuza ! Though he initially comes on as emotionless and merciless, after a confrontation with Inugamis true form (and powers), Haguro loses his mind and becomes obsessed with Inugami and his complete destruction, to the point where he can only have sex with his girlfriend while thinking about Akira or carving Akiras name into his flesh. Indeed, in the short "fight" who opposed them, Haguro was helpless and felt very humiliated, he who was since birth considered a monster by everyone he met, finally came across a "real monster" with abusive super powers. He then proceed on committing several horrible acts throughout the series, including two rapes (despite he himself consider love and sexe as a instinctive natural drive like urinating) and one of the highest death toll of the cast (though he probably killed and injured less people than Kuro and Akira). Though he is clearly the antagonist of the manga and a very evil one in fact, Haguro, after having discovered the truth about his instinctive hatred for Akira, stop to persecute him for petty reasons, but sees as his own responsibility to kill the last monster threatening Mankind. He can also be seen as a very gifted individual, who despite his character and crimes, give a good representation of both the best and the worst of Humanity. He even stated that fighting the Werewolf with "materialistic weapons" was a stupid idea, and that what was needed was spiritual strength, henceforth a samurai sword, with which he happens to be very prominent. Haguro holds complex feelings for Inugami Akira, both an innate hatred and strange love tainted with jealousy. For him, slaying Inugami is a responsibility toward Humanity (since he may very well be the only human with strength and determination to oppose Inugami and the possible wave of disaster who would come from his blood), but also a way to return to being a monster. Haguro was a monster before meeting a monster truest to the name than him, even though he didnt saw himself as a monster before that, he regretted the feeling of being a monster and just a frail human being. That is why during their fight, Haguro devours several of Akiras fingers, gaining a fraction of his power in the process, he also realizes that he wants to eat Akira so that they may "become one". In the end, he find another way to realize this wish. After Akira transformed into a wolf with bright white fur (apparently created by his love for Akiko), Haguro instinctively hug Inugami but by doing that, he impales himself with his own katana, which had been rammed through Akiras abdomen. He makes a final plea for Akira to become one with him, but he dies before hearing Akiras answer and is dragged into hell by the souls of those he killed.

== References ==
 

== External links ==
* 
*  

 
 
 
 
 
 