Quiz Show (film)
 
{{Infobox film
 | name           = Quiz Show
 | image          = QuizShowPoster.jpg
 | caption        = Quiz Show theatrical poster
 | director       = Robert Redford
 | producer       = Robert Redford Michael Jacobs Julian Krainin Michael Nozik
 | screenplay     = Paul Attanasio
 | based on       =  
 | starring       = John Turturro Rob Morrow Ralph Fiennes 
 | music          = Mark Isham
 | cinematography = Michael Ballhaus
 | editing        = Stu Linder
 | studio         = Hollywood Pictures Buena Vista Pictures
 | released       =  
 | runtime        = 133 minutes
 | country        = United States
 | language       = English
 | budget         = $31 million  
 | gross          = $24,822,619   
 }}
Quiz Show is a 1994 American historical  , Rob Morrow, and Ralph Fiennes, with Paul Scofield, David Paymer, Hank Azaria, and Christopher McDonald appearing in supporting roles.  
 Twenty One box office Golden Globes.

==Plot== Twenty One, a popular television quiz show, are sent into a television studio as studio producers Dan Enright (David Paymer) and Albert Freedman (Hank Azaria) watch from the control booth. The evenings main attraction is Queens resident Herb Stempel (John Turturro), the reigning champion, who answers question after question. However, both the network, NBC, and the corporate sponsor of the program, a supplementary tonic called Geritol, find that Stempels approval ratings are beginning to level out, meaning the show would benefit from new talent.

Enright and Freedman find a new contestant in Columbia University instructor Charles Van Doren (Ralph Fiennes), son of the renowned poet and intellectual Mark Van Doren (Paul Scofield) and the novelist Dorothy Van Doren (Elizabeth Wilson). The producers subtly offer to rig the show for him but Van Doren uprightly refuses. Enright soon treats Stempel to dinner at an upscale restaurant, where he breaks the news that Stempel must lose in order to boost flagging ratings. Stempel begrudgingly agrees, only on the condition that he remains on television, threatening to reveal the true reason of his success: the answers had been provided for him.

Stempel and Van Doren face each other in Twenty One, where the match comes down to a predetermined question regarding Marty (film)|Marty, the 1955 winner of the Academy Award for Best Picture. Despite knowing the correct film, Stempel gives the wrong answer of On the Waterfront, allowing Van Doren to get a question he previously answered while in Enrights offices; he provides the winning response.

In the weeks that follow, Van Dorens winning streak makes him a national celebrity. Buckling under the new pressure, he begins to let the producers directly give him the answers instead of researching for them himself. Meanwhile, Stempel, having lost his financial prize winnings to a fleeting bookie, begins threatening legal action against the NBC network after weeks go by without his return to television.  He is shown going into the office of New York County District Attorney Frank Hogan.
 Richard N. Congressional lawyer from Harvard Law, becomes intrigued when he reads that the grand jurys findings from Hogans proceedings are sealed.  He travels to New York to investigate rumors of rigged quiz shows. Visiting a number of contestants, including Stempel and Van Doren, he begins to suspect Twenty One is indeed a fixed operation. However, Stempel is a volatile personality and nobody else seems to corroborate that the show is rigged. Goodwin enjoys the company of Van Doren, who invites him to social gatherings, and doubts a man of Van Dorens background and intellect would be involved in the hoax.

Stempel desperately confesses to being in on the fix himself, and further insists that if he got the answers in advance, Van Doren did as well. This wins Stempel an angry tell-off from his wife, who believed in him.  With the evidence mounting, Van Doren deliberately loses, but is rewarded with a sizable contract from NBC to appear as a special correspondent on the Today (NBC program)|Today show.

Meanwhile, Goodwin proceeds with the hearings before the House Committee for Legislative Oversight, with extended proof of the shows corruption. Goodwin strongly advises Van Doren to avoid making any public statements supporting the show. If he agrees to this, Goodwin promises not to call Van Doren to appear before the Congressional committee. However, at the prompting of the NBC network head, Van Doren issues a statement reaffirming his trust in the honesty of the quiz show.

Stempel testifies before Congress and, while doing so, implicates Van Doren, forcing Goodwin to call him in as a witness. Van Doren goes before Congress and publicly admits his role in the conspiracy. At first, Congress is very impressed with him, but one Congressman from New York is unimpressed and puts Van Doren in his place, turning the tide against him.  Afterward, he is informed by reporters of his firing from Today as well as the universitys decision to ask for his resignation.

Goodwin believes he is on the verge of a victory against Geritol and the network, but instead realizes that Enright and Freedman will not turn in their bosses and jeopardize their own futures in television; he silently watches the producers testimony, vindicating the sponsors and the network from any wrongdoing.

==Cast==
*John Turturro as Herb Stempel Richard N. "Dick" Goodwin
*Ralph Fiennes as Charles Van Doren
*David Paymer as Dan Enright
*Paul Scofield as Mark Van Doren
*Hank Azaria as  Albert Freedman Christopher McDonald Jack Barry
* Adam Kilgour as Thomas Merton
*Johann Carlo as Toby Stempel
*Elizabeth Wilson as Dorothy Van Doren
*Allan Rich as Robert Kintner
*Mira Sorvino as Sandra Goodwin George Martin as Chairman
*Paul Guilfoyle as Lishman
*Griffin Dunne as Geritol Account Executive
*Michael Mantell as Pennebaker
*Martin Scorsese as Martin Rittenhome
*Neil Ross as Twenty-One Announcer
*Barry Levinson as Dave Garroway

==Historical comparison== The American Experience. 

In a July 2008 edition of The New Yorker, Van Doren writes about the events depicted in the film, agreeing with many of the details but also saying that he had a regular girlfriend (who eventually became his wife) at the time he was on Twenty-One, who is not present in the film depiction. Van Doren also notes that he continued teaching, contrary to the films epilogue which states he never returned to doing so. 

==Release==
===Box office===
The film opened in limited release on September 14, 1994. After its initial run, the film grossed a domestic total of $24,822,619 and was a box office bomb. 

===Critical reception===
 , it had a 96% rating from Rotten Tomatoes.  Film critic Roger Ebert gave the film 3-and-a-half stars out of four, calling the screenplay "smart, subtle and ruthless".  Web critic James Berardinelli praised the "superb performances by Fiennes", and said "John Turturro is exceptional as the uncharismatic Herbie Stempel." 

===Accolades===
Robert Redford was nominated for the Academy Award for Best Director, the Academy Award for Best Picture (alongside Michael Jacobs, Julian Krainin, and Michael Nozik), the BAFTA Award for Best Film (alongside Jacobs, Krainin, and Nozik), the Golden Globe Award for Best Director, the Golden Globe Award for Best Motion Picture – Drama, and the Directors Guild of America Award for Outstanding Directing – Feature Film.

Paul Attanasio won the BAFTA Award for Best Adapted Screenplay and was nominated for the Academy Award for Best Adapted Screenplay,  and the Golden Globe Award for Best Screenplay.

Paul Scofield was nominated for the Academy Award for Best Supporting Actor, the BAFTA Award for Best Actor in a Supporting Role, the Dallas–Fort Worth Film Critics Association Award for Best Supporting Actor, the National Society of Film Critics Award for Best Supporting Actor, and the New York Film Critics Circle Award for Best Supporting Actor.

John Turturro was nominated for the Golden Globe Award for Best Supporting Actor – Motion Picture, the Screen Actors Guild Award for Outstanding Performance by a Male Actor in a Supporting Role, and the Chicago Film Critics Association Award for Best Supporting Actor.

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 