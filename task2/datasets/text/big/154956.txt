Ocean's 11
 
{{Infobox film
| name = Oceans 11
| image = OceansEleven(1960)Poster.jpeg
| caption = 
| alt =
| director = Lewis Milestone
| producer = Lewis Milestone Harry Brown Charles Lederer
| story = George Clayton Johnson Jack Golden Russell
| starring = Frank Sinatra Dean Martin Sammy Davis, Jr. Peter Lawford Joey Bishop Angie Dickinson
| music = Nelson Riddle
| cinematography = William H. Daniels Philip W. Anderson
| distributor = Warner Bros.
| released =  
| country = United States
| runtime = 127 minutes
| language = English
| budget = 
| gross = $5 million (US/ Canada rentals) 
}}
Oceans 11 is a 1960  , Frank Sinatra, Dean Martin, Sammy Davis, Jr. and Joey Bishop. Variety Film Reviews|Variety film review; August 10, 1960, page 6. 
 Las Vegas Harry Wilson and Buddy Lester, as well as cameo appearances by Shirley MacLaine, Red Skelton, and George Raft.

A Oceans Eleven|remake, directed by Steven Soderbergh, starring George Clooney, Brad Pitt, Matt Damon, Andy García, Julia Roberts, Bernie Mac, and Don Cheadle (among others) was released in 2001, followed by a pair of sequels.

==Plot== 82nd Airborne Wilbur Clarks Sands and the Flamingo Las Vegas|Flamingo) on a single night.

 
The gang plans the elaborate New Years Eve heist with the precision of a military operation. Josh Howard (Sammy Davis Jr.|Davis) takes a job driving a garbage truck while others work to scope out the various casinos. Sam Harmon (Dean Martin|Martin) entertains in one of the hotels lounges. Demolition charges are planted on an electrical transmission tower and the backup electrical systems are covertly rewired in each casino.

At exactly midnight, while everyone in every Vegas casino is singing "Auld Lang Syne" the tower is blown up and Vegas goes dark. The backup electrical systems open the cashier cages instead of powering the emergency lights. The inside men sneak into the cashier cages and collect the money. They dump the bags of loot into the hotels garbage bins, go back inside, and mingle with the crowds. As soon as the lights come back on, the thieves stroll out of the casinos. A garbage truck driven by Josh picks up the bags and passes through the police blockade. It appears to have gone off without a hitch.

Their ace electrician, Tony Bergdorf (Richard Conte|Conte), has a heart attack in the middle of the Las Vegas Strip and drops dead. This raises the suspicions of police, who wonder if there is any connection.

Reformed mobster Duke Santos (Cesar Romero|Romero) offers to recover the casino bosses money for a price. He learns of Ocean being in town and his connection to Foster, who is the son of Dukes fiancee (Ilka Chase|Chase). Santos pieces together the puzzle by the time Bergdorfs body arrives at the mortuary.

Santos confronts the thieves, demanding half of their take. In desperation, the money is hidden in Bergdorfs coffin, with $10,000 set aside for the widow (Jean Willes). The group plans to take back the rest of the money, making no payoff to Santos, after the coffin is shipped to San Francisco.

This plan backfires when the funeral director talks Bergdorfs widow into having the funeral in Las Vegas, where the body is cremated&nbsp;– along with all of the money.

==Production==
Peter Lawford was first told of the basic story of the film by director Gilbert Kay, who heard the idea from a gas station attendant.  Lawford eventually bought the rights in 1958, imagining William Holden in the lead. pp.117–121 Levy, Shawn Rat Pack Confidential 1998 Fourth Estate Ltd   Sinatra became interested in the idea, and a variety of different writers worked on the project. When Lawford first told Sinatra of the story, Sinatra joked, "Forget the movie, lets pull the job!" 

The opening animated title sequence was designed by Saul Bass. The closing shot shows the main cast walking away from the funeral home, with the Sands Hotel marquee behind them listing their names as headliners.

==Cast==

===Oceans 11===
#Frank Sinatra as Danny Ocean
#Dean Martin as Sam Harmon
#Sammy Davis, Jr. as Josh Howard
#Peter Lawford as Jimmy Foster
#Richard Conte as Tony Bergdorf
#Joey Bishop as Mushy OConnors
#Henry Silva as Roger Corneal
#Buddy Lester as Vince Massler
#Richard Benedict as Curly Steffans
#Norman Fell as Peter Rheimer
#Clem Harvey as Louis Jackson

===Others===
*Angie Dickinson as Beatrice Ocean
*Cesar Romero as Duke Santos
*Patrice Wymore as Adele Elkstrom
*Akim Tamiroff as Spyros Acebos
*Ilka Chase as Mrs. Restes
*Jean Willes as Gracie Bergdorf
*Hank Henry as Mr. Kelly
*Lew Gallo as Jealous Young Man
*Robert Foulk as Sheriff Wimmer

===Cameos===
*Shirley MacLaine as Tipsy Woman (Martins kisser)
*George Raft as Jack Strager (Casino Owner) Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 171 
*Red Skelton as Himself
*Richard Boone as The Minister (Voice)
*Red Norvo as Himself/Hotel Vibraphonist

==Blu-ray release==
Oceans 11 was released on Blu-ray on November 9, 2010 in a "50th Anniversary Edition". Bonus features include: 
*Special commentary by Frank Sinatra, Jr. and Angie Dickinson.
*"Vegas Map" – Mini-documentaries of the five casinos involved in the movie. November 14, 1977.
*"Tropicana Museum Vignette"

==References==
 

==External links==
* 
* 
* 
*TCM notes http://www.tcm.com/tcmdb/title.jsp?stid=18360&category=Notes

 
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 