Dil Pe Mat Le Yaar!!
 
 
{{Infobox film
| name           = Dil Pe Mat Le Yaar
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Hansal Mehta 	
| producer       = Ajay Tuli	
| writer         = Saurabh Shukla
| screenplay     = Saurabh Shukla	
| story          = Moin-ud-din	
| based on       =  
| narrator       =  Tabu Aditya Srivastava
| music          = Vishal Bhardwaj
| cinematography = Sanjay Kapoor
| editing        = Girish Madhu
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Tabu and Aditya Srivastava in pivotal roles.  The film met with positive critical response whereas ended up being an average grosser at the box-office. 

==Plot==

An innocent simpleton Ram Saran Pandey (Manoj Bajpai) leaves his village to Mumbai to make his earnings leaving behind his parents and promising them that he would call them to the city once he is stable. In the bustling city, he ends up getting a job as a car mechanic. Kamya Lal (Tabu), a journalist by profession, visits the car garage as a customer and is bowled over by his innocence and honesty. She makes up her mind to write articles on his life and make him popular among the social circles. Soon, the mutual admiration develops into a good friendship and Ram falls into love with her. However, she rebuffs him on his every innocent proposals. On one night, Ram accidentally spots Kamya in a compromising position with other person in her apartment and this breaks his heart. He decides to shed all his innocence and takes the rough path to propose her and eventually ends up being a don in town.

==Cast==

* Manoj Bajpai...Ram Saran Pandey
* Tabu (actress)|Tabu...Kamya Lal
* Aditya Srivastava...Tito
* Saurabh Shukla...Gaitounde
* Divya Jagdale...Gayatri
* Kishor Kadam...Bhaskar Shetty
* Vijay Raaz...Raju Bhai
* Harsh Chhaya...Ali
* Prithvi Zutshi...Suleman Patil
* Anupam Shyam Rakesh Kumar
* Kashmira Shah...Item song dancer

==Soundtrack==
The music is composed by Vishal Bhardwaj.  Lyrics were written by Abbas Tyrewala and Piyush Mishra.

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dil Pe Mat Le Yaar"
| Asha Bhonsle
|-
| 2
| "Lai Ja Re Badra"
| Sanjeev Abhyankar
|-
| 3
| "Jee Jee"
| Udit Narayan, Kavita Krishnamurthy
|-
| 4
| "Chal Padi"
| Suresh Wadkar, Roop Kumar Rathod
|-
| 5
| "Haule Haule" KK
|-
| 6
| "Paagal"
| Abhijeet Bhattacharya, Kavita Krishnamurthy
|-
| 7
| "Swagatham" Hariharan
|-
|}

==References==
 

==External links==
* 

 
 
 
 
 
 