The Man in White
 
{{Infobox film
| name           = The Man in White
| image          = TheManInWhite2003Film.jpg
| image_size     =
| caption        =
| director       = Takashi Miike
| producer       =
| writer         = Shigenori Takechi (screenplay and story)
| narrator       =
| starring       =
| music          = Kōji Endō
| cinematography = Kazunari Tanaka
| editing        = Yasushi Shimamura
| distributor    =
| released       = 2003
| runtime        = 150 minutes
| country        = Japan
| language       = Japanese
| budget         =
}} Japanese yakuza film directed by Takashi Miike.

==Cast==
*Masaya Kato as Azusa
*Shoko Aida|Shôko Aida
*Naomi Akimoto
*Narumi Arimori
*Kenichi Endo
*Tatsuya Fuji
*Mitsuru Hirata
*Renji Ishibashi
*Masaya Kato
*Hiroshi Katsuno
*Kazuki Kitamura
*Masaomi Kondo
*Shigeru Kôyama
*Hiroki Matsukata
*Ryôsuke Miki
*Yasukaze Motomiya
*Hiroyuki Nagato
*Chikage Natsuyama
*Jinpachi Nezu
*Masaki Nomura
*Masahiko Tsugawa

==Other credits==
*Produced by:
**Michinao Kai - producer
**Shigeji Maeda - associate planner
**Fujio Matsushima - planner
**Makiko Natsuyama - producer
**Shizuka Natsuyama - executive producer: Cinema Paradise/Shinema Paradaisu
**Shôichirô Natsuyama - associate planner
**Yasuko Natsuyama - executive producer: Cinema Paradise/Shinema Paradaisu
**Tsuneo Seto - planner
**Kôzô Tadokoro - producer
**Shigenori Takechi - planner
*Production Design: Tatsuo Ozeki
*Sound Department: Yoshiya Obara - sound

== External links ==
*  

 

 
 
 
 
 
 

 