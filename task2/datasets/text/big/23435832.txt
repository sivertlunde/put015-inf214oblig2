Mahakaal
 
{{Infobox film
| name           = Mahakaal
| image          =
| image size     =
| caption        =
| director       =  Ramsay   Tulsi Ramsay
| producer       = Gangu Ramsay   Chander Ramsay
| writer         =  Y.V. Tyagi Sayeed Sultan (Dialogue) Kafil Azar (Lyrics) Karan Shah Archana Puran Singh Reema Lagoo Johnny Lever Kulbhushan Kharbanda
| music          = Anand-Milind  K J Sing (background score)  Vishal (background score)
| cinematography = Gangu Ramsay
| editing        = Vikas Daripkar Shyam Gupte
| distributor    = Mondo Macabro
| released       =  
| runtime        = 143 minutes
| country        = India
| language       = Hindi
| budget         =
}}
Mahakaal (also known as The Monster and Time of Death) is a 1993 Indian horror film. It is directed by Shyam Ramsay and Tulsi Ramsay and is based on the American horror film franchise A Nightmare on Elm Street. It was released on DVD in the US by distributor Mondo Macabro in 2009. The film soundtrack was composed by Anand–Milind, and the background score was composed by K. J. Sing and Vishal.

==Plot== mysterious monster haunts the dreams of a young woman named Nancy Thompson (A Nightmare on Elm Street)|Anita. Anita becomes concerned when one of her friends is killed by the monster in a dream.

==Cast== Karan Shah
*Archana Puran Singh - Anita
*Reema Lagoo
*Johnny Lever
*Kulbhushan Kharbanda
* Baby Swetha
* Sunil Dhawan
* Asha Patel
* Dinesh Kaushik
* Mahavir Bhullar
* Kunika
* Karan Shah

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chal chal meri jaan chal zara"
| Udit Narayan
|}

==External links==
*  
;Reviews
*   DVD Review at 10K Bullets

 
 
 
 
 
 


 
 