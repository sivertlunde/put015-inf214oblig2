11:14
 
{{Infobox film
| name           = 11:14
| image          = 11-14 (2003 film) poster.jpg
| caption        = 11:14 DVD cover
| director       = Greg Marcks
| producer       = Mark Damon Stewart Hall Jeff Kwatinetz Sammy Lee David Rubin Hilary Swank Tripp Vinson Beau Flynn John Morrissey Raju Patel
| writer         = Greg Marcks
| starring       = Rachael Leigh Cook Henry Thomas Blake Heron Barbara Hershey Clark Gregg Patrick Swayze Hilary Swank
| music          = Clint Mansell
| cinematography = Shane Hurlbut
| editing        = Dan Lebental Richard Nord
| studio         = Media 8 Entertainment
| distributor    = New Line Cinema
| released       =  
| runtime        = 86 minutes 
| country        = Canada United States
| language       = English
| budget         = $6 million 
}}
 American independent indie thriller (genre)|thriller.

The film was edited in a way that the various scenes of the movie end exactly at 11:14 PM.

==Plot==
The film involves a series of interconnected events that converge up to the same time at 11:14&nbsp;p.m.  The connections between the events are not apparent at first, but are gradually revealed by a series of progressively receding flashback (literary technique)|flashbacks:

===Part 1===

11:13 - 11:33
::Jack ( ), assumes that he hit a deer. She offers to call the police over Jacks protests, insisting that it is no trouble because she has a new cell phone that she "never uses" and because she is friends with the Chief of Police. Norma offers Jack a ride to her house to wait for the police, but he declines.  When she drives off, Jack decides to hide the body in the trunk of his car. He gets back into the car to pull away, but a police officer pulls up behind him. Officer Hannagan (Clark Gregg) speaks with Jack and, noting his odd behavior, asks him if he would submit to a breathalyzer test. Jack requests a sobriety test instead, which Hannagan administers, getting him to count back in reverse from Z to A.  When the officer checks with dispatch, he finds that Jacks license has been revoked for driving under the influence. Hannagan tells Jack that he is under arrest and that his car will be impounded. When he finds the body in the trunk, Hannagan handcuffs Jack with a cable tie and tells the two people already in the back of the cruiser, Duffy (Shawn Hatosy) and Buzzy (Hilary Swank), to move over so that Jack will fit.  While Hannagan argues with Buzzy, who refuses to move over, Jack is able to pull clippers out of his pocket, cut the cable tie, and escape. Hannagan pursues, leaving the cruiser door open, and Duffy and Buzzy also escape. Jack runs across a property with security lights and a dog, causing the owner, Norma, to come out. She is obviously upset and is looking for her husband Frank (Patrick Swayze) because she just received a phone call that their daughter, Cheri (Rachael Leigh Cook), was killed in a hit-and-run accident. Hannagan catches up to Jack at this point, and Norma angrily strikes Jack with a flashlight, assuming that he is responsible for her daughters death. He flees again, into the cemetery where he trips over a bowling ball and is again taken into custody.

===Part 2===

11:09 - 11:29
::Teenagers Tim (Stark Sands), Mark (Colin Hanks) and Eddie (Ben Foster) are driving around causing trouble by throwing things out of the windows of Marks van, including a book they have set on fire. Mark, distracted by Eddie urinating out the vans window, runs into and kills Cheri, who was crossing the road while on her cell phone. They stop, but flee the scene when Duffy comes toward the van with a gun. As Duffy fires on the retreating van, Tim realizes that the accident also caused the vans window to snap shut, cutting off Eddies penis. Tim insists that Mark stop, and Tim goes back to find the penis. Tim is accosted by the paramedics, Leon (Jason Segel) and Kevin (Rick Gomez) at the scene, but manages to escape and get the severed penis back to Eddie.

===Part 3===

11:04 - 11:24
::Frank is walking his dog late at night and discovers his daughters (Cheris) car keys next to the dead body of Aaron (Blake Heron) in the cemetery. Thinking his daughter is responsible for the death, Frank packs the body in the trunk of Aarons car, accidentally locking the keys in with the body. He uses a rock to break a window and get into the car, then drives to a bridge. He has to hide from a car driven by Duffy that passes by, but then disposes of the body by dropping it over the side of the bridge, where it lands on Jacks car (as seen at the start of the film). His dog runs off with the blood-soaked jacket. He chases the dog, eventually catching it. He sees the burning book that the teenagers threw on the sidewalk, and uses it to set his jacket on fire. His wife, Norma, sees him and gives him a ride home, where she sends him out to look for the deer that Jack supposedly hit.

===Part 4===

10:59 - 11:19
::Buzzy is working at a convenience store late at night. Her friend and co-worker Duffy arrives and they begin discussing Cheris pregnancy and money needed for an abortion.  Mark and Eddie arrive after the store is closed but Duffy lets them come in.  They are there to buy items to throw out the van windows. After they leave, Duffy tells Buzzy his plan to steal from the store the $500 required to pay for the abortion. Cheri arrives and she and Duffy go into the cooler. Meanwhile Buzzy is playing around with Duffys revolver (the one he plans to use to rob the store) and she accidentally shoots a bullet through a glass door of a refrigerator, barely missing Duffy and Cheri. Cheri leaves and Duffy asks Buzzy to allow him to steal the money from the convenience stores cash register. Buzzy objects, fearful of losing her job, but relents, while insisting that Duffy shoot her in the arm to make it look like an authentic robbery. He shoots her in the arm and then dials 9-1-1 for her, leaving while she is on the phone. Duffy looks for his keys, barely escaping the police who are arriving more quickly than he thought they would. While driving away, he passes by Aarons car, where Frank has parked it preparing to dispose of Aarons body. Duffy then sees Cheri parked and tells her he got the money for her abortion. When she gets out of her car, he witnesses the teenagers van knocking down and killing Cheri, and he shoots at the teenagers. He is then arrested by Officer Hannagan for shooting at the van and for the store robbery, based on the description someone phoned in (later revealed to be Cheri). Buzzy is arrested as an accomplice when she refuses to identify Duffy and admits to conspiring with him.

===Part 5===

10:54 - 11:14
::Cheri leaves her house to have sex with Aaron at the cemetery. Aaron is reclining against a tombstone that has a stone angel on top. The angels neck is damaged and the heavy stone head falls onto Aarons face, killing him instantly and mutilating his face. Cheri runs away from the scene, not realizing that she has dropped the set of keys that Frank found in the earlier scene. Cheri borrows her fathers car and goes to the convenience store to get Duffys bowling ball, intending to replace the angel head with the bowling ball and implicate Duffy as killing Aaron. As she drives away from the store, she sees the shooting and reports a description of Duffy to the police. When Cheri arrives back at the cemetery, she drops the bowling ball when she sees that Aarons body is gone. She tries to leave, but her car is again having trouble starting. Her cell phone rings, and she begins talking to Jack. This is the phone conversation the movie begins with, continued to inform the viewer that Cheris "pregnancy" is actually a scam to get money from both Duffy and Aaron, so that Cheri and Jack can leave town together with the money. In the midst of the call, Duffy calls out Cheris name from across the street to tell her that he got the $500 she wanted him to get. Cheri hangs up quickly, and while she is crossing the street, the cell phone rings again, and, distracted, she stops in the middle of the road, where she is hit by the van containing Mark, Tim, and Eddie. The camera pans to Cheris cell phone, which reads 11:14 p.m.

==Cast==
* Henry Thomas as Jack 
* Barbara Hershey as Norma 
* Clark Gregg as Officer Hannagan 
* Shawn Hatosy as Duffy 
* Hilary Swank as Buzzy 
* Patrick Swayze as Frank 
* Rachael Leigh Cook as Cheri 
* Stark Sands as Tim 
* Colin Hanks as Mark 
* Ben Foster as Eddie 
* Jason Segel as Leon 
* Rick Gomez as Kevin 
* Blake Heron as Aaron

==Critical reception==
11:14 has been well received by critics. It currently holds a 92% "fresh" rating on Rotten Tomatoes, based on 11 reviews.  Mick LaSalle, of the San Francisco Chronicle, called it an "inventive, black comedy" and "a meticulous piece of plot construction, entertaining, full of incidents and infused throughout with a mischievous and bleak sense of humor." 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 