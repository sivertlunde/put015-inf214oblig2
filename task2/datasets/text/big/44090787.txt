Ambalavilakku
{{Infobox film 
| name           = Ambalavilakku
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       = S Kumar
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi Madhu Srividya Sukumari Jagathy Sreekumar
| music          = V. Dakshinamoorthy
| cinematography = Anandakkuttan
| editing        = K Narayanan
| studio         = Sastha Productions
| distributor    = Sastha Productions
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by Sreekumaran Thampi and produced by S Kumar. The film stars Madhu (actor)|Madhu, Srividya, Sukumari and Jagathy Sreekumar in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
  Madhu as Gopi
*Srividya as Sumathi Teacher
*Sukumari as Srimathi Rama Varma
*Jagathy Sreekumar as Vasukkutty
*Thikkurissi Sukumaran Nair as Dr. Rama Varma
*Sreelatha Namboothiri as Rajamma
*Vaikkam Mani
*Sivakumar
*Adoor Bhavani as Gopis Mother
*Aranmula Ponnamma as Savithris Mother-in-law
*Aroor Sathyan
*Kailasnath
*Kuthiravattam Pappu as Lonachan
*Lissy
*Peyad Vijayan
*Poojappura Ravi as Radhakrishnan Sir Roopa as Geetha
*Sasi
*Roja Ramani as Savithri
*Somasekharan Nair
*Unnikrishnan Chelembra
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Manjappattu Njorinju || Vani Jairam || Sreekumaran Thampi || 
|-
| 2 || Pakal Swapnathin pavanurukkum || K. J. Yesudas, Vani Jairam || Sreekumaran Thampi || 
|-
| 3 || Varumo veendum thrikkaarthikakal || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 