Maza Pati Karodpati
{{Infobox film
| name           = Maza Pati Karodpati - Marathi Movie
| image          = Maza Pati Karodpati.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Sachin Pilgaonkar
| producer       = Chelaram Bhatia Lalchand Bhatia
| screenplay     = 
| story          = 
| starring       = Sachin Pilgaonkar Ashok Saraf Kishori Shahane Supriya
| music          = Arun Paudwal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Maza Pati Karodpati is a Marathi movie released on 20 January 1988.  Produced by Chelaram Bhatia along with Lalchand Bhatia and directed by Sachin Pilgaonkar.  The movie is based on a young girl who values money more than love and how her own wrongdoings can reprimand her.

== Synopsis ==
Shanku, a sober but stubborn orphan, lives with her maternal uncle, a bachelor, in a slum. Her sole dream is to marry a millionaire. According to her, a girl gets only one chance to change her name in life, and that is after her marriage.

Shaku’s uncle Damodar works as a cook for Mr.Laxmikant Kuber, a renowned industrialist, whose son Naren has just returned from abroad after completing his studies. At a party thrown by Mr.Kuber for his son’s homecoming, Naren tells his friends his views on marriage. His only condition is that he will marry a young widow, which Shaku overhears.

With the help of Tatya Malwankar, Shaku poses as a young widow Saudamini and enters the Kuber residence. With the help of theatrical antics, she soon wins over the hearts of Mrs. Kuber and Naren who ask her to live in the house. As days go by, Naren falls in love with her and Mr.Kuber asks Saudamini for her late husbands photo. Again with the help of Tatya Malwankar, Shaku shows him a photo of a military Captain, and tells his name as Capt. Bajirao Rangade.

Lukutuke a clerk, who works for Deshmukh is in love with Hema, Mr. Deshmukh’s daughter. Mr. Kuber asks for Hema’s hand in marriage for Naren. One day when Kuber visits Deshmukh at his office, he bumps into Luktuke, who he recognises the Capt. in the photo, Bajirao Rangade. A plan begins to take shape in Kuber’s mind.

== Cast ==

The cast includes Sachin Pilgaonkar, Ashok Saraf, Supriya, Kishori Shahane, Niloo Phule, Machchhindra Kambli, Shubha Khote & Others.

==Producers==
The Bhatia duo: Chelaram Bhatia and Lalchand Bhatia. (Glamour Films)

==Soundtrack==
The music is provided by Arun Paudwal.

== References ==
 
 

== External links ==
*  
*  

 
 
 


 