Number One (1994 film)
{{Infobox film
| name = Number One
| image = Number-one.jpg
| alt =
| caption =
| director = S. V. Krishna Reddy
| producer =
| writer = Diwakar Babu   (Dialogues)  
| screenplay =
| story = S. V. Krishna Reddy Krishna Soundarya
| music = S. V. Krishna Reddy
| cinematography = Sarath
| editing = Ramgopal Reddy K.
| studio =
| distributor =
| released =  
| runtime =
| country = India
| language = Telugu
| budget =
| gross =
}}
 Krishna and Soundarya in lead roles.

==Cast== Krishna
* Soundarya
* Kota Srinivasa Rao Ali
* Babu Mohan
* Maharshi Raghava
* Gundu Hanumantha Rao
* Srilatha
* Sivaji Raja
* Raja Ravindra
* Subbaraya Sarma
* Y.Vijaya
* Mahesh Anand
* Brahmanandam

==Crew==
* Director- S. V. Krishna Reddy
* Story -S. V. Krishna Reddy
* Dialogues-Diwakar Babu
* Music -S. V. Krishna Reddy
* Cinematography-Sarath
* Editing -Ramgopal Reddy K.
* Co-director-Ranga Rao Kurra
* Art Direction by-Raju R.S.
* Second assistant director-G.Nageswara Reddy
* Lyricist-Jonnavithula, Bhuvana Chandra, Sirivennela Sitaramasastri Chitra

==Music==
{{Infobox album
| Name = Number One
| Tagline =
| Type = soundtrack
| Artist = S. V. Krishna Reddy
| Cover =
| Released = 1994
| Recorded =
| Genre =
| Length = 24:28
| Label =
| Producer =
| Reviews =
| Last album =
| This album =
| Next album =
}}
The music of the film was composed by S. V. Krishna Reddy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
! No. !! Song !! Lyrics!! Singers !! Length (m:ss)
|- Jonnavithula ||S. Chitra || 05:08
|- 2 || Bhuvana Chandra ||S. P. Balasubrahmanyam,Chitra || 05:10
|- Sirivennela Sitaramasastri ||S. P. Balasubrahmanyam,Chitra||04:51
|- Jonnavithula ||S. P. Balasubrahmanyam,Chitra ||05:04
|- Sirivennela Sitaramasastri ||S. P. Balasubrahmanyam,Chitra ||04:15
|}

==References==
 

==External links==
*  

 

 
 
 
 