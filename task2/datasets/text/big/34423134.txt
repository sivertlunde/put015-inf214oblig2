It's Always the Woman
{{Infobox film
| name           = Its Always the Woman
| image          = 
| image_size     = 
| caption        = 
| director       = Wilfred Noy
| producer       = Clarendon Film Company
| writer         = Bryant Adair (play)
| narrator       = 
| starring       = Hayden Coffin Daisy Burrell Barbara Hoffe
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    =  
| released       = 1916 
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} silent motion picture of 1916 directed by Wilfred Noy (1883–1948) and produced by the Clarendon Film Company. It stars Hayden Coffin and Daisy Burrell.

The story was adapted from a play by "Riada" (Bryant Adair). 

==Cast==
* Hayden Coffin — Major Sterrington 
* Daisy Burrell — Mrs Sterrington  
* Barbara Hoffe — Esmeralda Chetwynde

==References==
 

==External links==
* 
*  at British Film Institute database

 

 
 
 
 
 
 
 
 

 