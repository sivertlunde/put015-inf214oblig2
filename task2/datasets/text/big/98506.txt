The Russians Are Coming, the Russians Are Coming
 
{{Infobox film
| name           = The Russians Are Coming, the Russians Are Coming
| image          = Russians are coming.jpg Jack Davis
| image_size     = 225px
| director       = Norman Jewison
| producer       = Norman Jewison William Rose
| based on       =  
| starring       = Carl Reiner Eva Marie Saint Alan Arkin Brian Keith Jonathan Winters Theodore Bikel Paul Ford
| music          = Johnny Mandel Bonia Shur
| cinematography = Joseph F. Biroc
| editing        = Hal Ashby J. Terry Williams The Mirisch Corporation
| distributor    = United Artists
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         = $3.9 million Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 186 
| gross          = $21,693,114 
}}
 American comedy William Rose.

The film depicts the chaos following the grounding of the Soviet submarine Спрут (pronounced "sproot" and meaning "octopus") off a small New England island during the Cold War. The film stars Carl Reiner, Eva Marie Saint, Alan Arkin in his first major film role, Brian Keith, Theodore Bikel, Jonathan Winters, and Paul Ford.

==Plot==
A Russian submarine called Спрут ("Octopus") draws too close to the New England coast one morning when its captain (Theodore Bikel) wants to take a good look at America and runs aground on a sandbar near the fictional Gloucester Island, which, from other references in the movie, is located off the coast of Cape Ann or Cape Cod, Massachusetts. Rather than radio for help and risk an embarrassing international incident, the captain sends a nine-man landing party, headed by his zampolit (Political Officer) Lieutenant Yuri Rozanov (Alan Arkin), to find a motor launch to help free the submarine from the bar. The men arrive at the house of Walt Whittaker (Carl Reiner), a vacationing playwright from New York City. Whittaker is eager to get his wife Elspeth (Eva Marie Saint) and two children, obnoxious but precocious nine and half-year-old Pete (Sheldon Collins) and three-year-old Annie (Cindy Putnam), off the island now that summer is over.

Pete tells his dad that "Russians with machine guns" dressed in black uniforms are near the house, but Walt is met by the men who identify themselves as Norwegian fisherman. Walt buys this, and to teach Pete a lesson about  judging others, asks if they are "Russians with machine guns", which startles Rozanov into admitting that they are Russians and pulling a gun on Walt. Rozanov promises no harm to the Whittakers if they hand over their station wagon and provide information on the military and police forces of their island. Although Walt and Elspeth provides the keys, the sailors are perplexed as to why there are no military personnel on the island, and only a small police force. Before the Russians depart, Rozanov orders one of the sailors, Alexei Kolchin (John Phillip Law), to prevent the Whittakers from fleeing. An attractive 18-year-old neighbor, Alison Palmer (Andrea Dromm), who works as a babysitter for Annie, expected to work that day and finds herself captive as well. 

The Whittakers station wagon quickly runs out of gasoline, forcing the Russians to walk. They steal an old sedan from Muriel Everett (Doro Merande), the postmistress; she calls Alice Foss (Tessie OShea), the gossipy telephone switchboard operator, and before long, wild rumors throw the entire island into confusion. As level-headed Police Chief Link Mattocks (Brian Keith) and his bumbling assistant Norman Jonas (Jonathan Winters) try to squelch an inept citizens militia led by blustering Fendall Hawkins (Paul Ford), Walt, accompanied by Elspeth, manages to overpower Kolchin, because the Russian is reluctant to hurt anyone. During the commotion Kolchin flees, but when Walt and Elspeth leave to find help, he reappears to the house, where only Alison and Annie remain.  Alexei says that although he does not want any fighting, he must obey his superiors in guarding the residence. He promises he will not harm anyone and offers to surrender his submachine gun as proof. Alison tells him that she trusts him and does not need to hand over his firearm.  Alexei and Alison become attracted to each other, taking a walk along the beach with Annie, and finding commonality despite their different cultures and the Cold War hostility between their countries.

Trying to find the Russians on his own, Walt is re-captured by them in the telephone central office. After subduing Mrs. Foss and disabling the islands telephone switchboard, seven of the Russians appropriate civilian clothes from the dry cleaners, manage to steal a cabin cruiser, and head to the submarine, still aground on the sandbar. Back at the Whittaker house, Kolchin is by now falling in love with Alison. At the phone exchange, Walt manages to free himself. He and Elspeth return to the house and almost shoot Rozanov, who arrives there just before they do. With the misunderstandings cleared up, the Whittakers, Rozanov, and Kolchin decide to head into town together to explain to everyone just what is going on.

As the tide rises, the sub floats up off the sandbar and proceeds on the surface to the islands main harbor. Chief Mattocks arrives with the rest of the villagers and the militia force, the men armed with everything from a harpoon and a bow and arrows to .22s, Winchester lever actions, 12 gauge shotguns, and military surplus rifles. With Political Officer Rozanov acting as translator, the Russian captain threatens to open fire on the town with his deck gun and machine guns unless the seven missing sailors are returned to him, his crew facing upwards of a hundred armed, apprehensive, but determined townspeople. Chief Mattocks warns the Soviet officer, "You come in here, scaring people half to death, you steal cars and motorboats, and you cause damage to private property and you threaten the whole community with grievous bodily harm and maybe murder. Now, we aint going to take any more of that, see? We may be scared, but maybe we aint so scared as you think we are, see? Now you say youre going to blow up the town, huh? Well, I say, all right! You start shooting, and see what happens!" As the Captain and Chief Mattocks glare at each other, two small boys go up in the church steeple to see better.  With tension approaching the breaking point, one of the boys (Johnny Whitaker) slips and falls from the steeple, but his belt catches on a gutter, leaving him precariously hanging forty feet in the air. Immediately uniting to save the child, the American islanders and the Russian submariners form a human pyramid and Kolchin rescues him.
 Air Force by radio. In a joint decision, the submarine heads out of the harbor with a convoy of villagers in small boats protecting it. Kolchin says goodbye to Alison, the stolen boat with the missing Russian sailors aboard intercepts the group shortly thereafter, and the seven board the submarine, just before two Air Force McDonnell F-101 Voodoo|F-101B Voodoo jets arrive. They break off after seeing the escorting flotilla of small craft, and the Octopus is free to proceed to deep water and safety. 

==Cast==
  
* Alan Arkin as Lt. Yuri Rozanov
* Carl Reiner as Walt Whittaker
* Eva Marie Saint as Elspeth Whittaker
* Brian Keith as Police Chief Link Mattocks
* Jonathan Winters as Officer Norman Jonas
* Paul Ford as Fendall Hawkins
* Theodore Bikel as The Russian Captain
* Tessie OShea as Alice Foss (telephone operator)
* John Phillip Law as Alexei Kolchin
* Ben Blue as Luther Grilk (town drunk)
 
* Don Keefer as Irving Christiansen
* Andrea Dromm as Alison Palmer
* Sheldon Collins as Pete Whittaker 
* Guy Raymond as Lester Tilly
* Cliff Norton as Charlie Hinkson
* Michael J. Pollard as Stanley, the airplane mechanic
* Richard Schaal as Oscar Maxwell
* Milos Milos as Lysenko
* Cindy Putnam as Annie Whittaker
 

==Production==
 
  F-101B Voodoo]]

Although set on the fictional "Gloucester Island" off the coast of Massachusetts, the movie was filmed on the coast of Northern California, mainly in Mendocino, California|Mendocino. The harbor scenes were filmed in Noyo Harbor, a small town south of Fort Bragg, California. Because of the filming location on the West Coast, the dawn scene at the beginning of the film was actually filmed at dusk through a pink filter.

The submarine used was a fabrication. The United States Navy refused to loan one for the production and barred the studio from bringing a real Russian submarine, forcing the studio to create their own. It was segmented into four parts, each having its own motor to power it.

The planes used were actual F-101 Voodoo jets from the 84th Fighter-Interceptor Squadron, located at the nearby Hamilton Air Force Base. They were the only Air Force planes that were based near the location of the supposed island.
 Paul Reveres Paul Revere#The Midnight Ride of Paul Revere|Ride, as does the subplot in which the town drunk (Ben Blue) rides his horse to warn people of the "invasion".
 Washington and Moscow. It was one of the few films of the time to portray the Russians in a positive light.  Senator Ernest Gruening mentioned the film in a speech in Congress, and a copy of it was screened in the Moscow Kremlin|Kremlin. According to Jewison, when screened at the Soviet film writers union, Sergei Bondarchuk was moved to tears. 
 Soviet hammer and sickle as transitional elements, zooming into each to create a montage, which ultimately worked to establish the tone of the film. The music in the sequence alternates between the American "Yankee Doodle" march and a Russian marching song called "Polyushko Pole"  (Полюшко Поле, usually "Meadowlands" in English).

Much of the dialog was spoken by the Russian characters, and few American actors of the era were proficient in a Russian accent. Musician and character actor Leon Belasco, who was born in Russia, spoke fluent Russian and specialized in foreign accents during his 60-year career, was the dialog director. John Phillip Laws consistently, magnificently incorrect pronunciation of difficult English phonemes, most notably in his slow, careful, hopeless struggle to master Alison Palmers name (ah-LYEE-sown PAHL-myerr), was remarkably authentic by the standards of the day. Arkin, a Russian speaker, did so well as Rozanov that he would be sought for both American characters and a few ethnic ones.  Another star of the film, Theodore Bikel, was able to pronounce Russian so well, despite not actually being able to speak it, that he won the role of the submarine captain.

==Reception==
The film was a critical and commercial success. According to Rotten Tomatoes, the film received a rating of 81%, making it one of Jonathan Winters highest filmography reviews. 

==Awards and honors==
Wins:
* Golden Globe Award for Best Motion Picture - Musical or Comedy
* Golden Globe Award for Best Actor - Motion Picture Musical or Comedy (Alan Arkin) William Rose)

Nominations:
* Academy Award for Best Picture
* Academy Award for Best Actor (Alan Arkin)
* Academy Award for Film Editing (Hal Ashby & J. Terry Williams) William Rose) Golden Globe Award for Best Screenplay (William Rose)
* Directors Guild of America – Outstanding Directorial Achievement in Motion Pictures (Norman Jewison)

==See also==
* The Russians are coming
* Whiskey on the rocks

==References==
Notes
 

==External links==
*  
*  
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 