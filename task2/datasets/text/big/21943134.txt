Baksho Rahashya (film)
{{Infobox film
| name           = Baksho Rahashya
| image          =dvd_baksha_rahasya.jpg
| image_size     =
| caption        = Poster of Baksho Rahashya
| director       = Sandip Ray
| producer       = Chhayabani Pvt. Ltd
| writer         = Satyajit Ray
| based on       = Baksho-Rahashya by Satyajit Ray
| narrator       =
| starring       = Sabyasachi Chakrabarty  Saswata Chatterjee  Robi Ghosh
| music          = Satyajit Ray
| cinematography = Barun Raha
| editing        = Subrata Roy
| distributor    =
| released       = 1996 (TV), 2001 (Film)
| runtime        = 98 minutes
| country        = India Bengali
| budget         = 4.5 lakhs
| website        =
}}

Baksho Rahashya ( )(1996 in TV, 2001 as film/DVD) is a Bengali thriller Telefilm directed by Sandip Ray based on the story of the same name by Satyajit Ray.It is the first Telefilm made for Feluda 30 TV Series, which aired on DD Bangla. Later it was released in Nandan Complex and DVD & VCD has been also been released in 2001.  

==Plot== Lal Mohan babu in Shimla, and into a realm of deceit and mystery involving a long forgotten diamond and a priceless manuscript about A Bengalee in Lamaland. Besides The Trio we have Dinanath Lahiri, the rich kind-hearted businessman, his nephew, Prabeer Lahiri who is a striving actor but too much obsessed with his only weakness, his voice. The rest three characters in this story are the ones who were with Dinanath Lahiri in his first class compartment, Mr.Pakrashi who is a manuscript collector, Mr. Brijmohan and Mr. Dhameeja who got his suitcase exchanged with Mr. Lahiri. Feludas investigations finally put the wrong-doers behind bars.

==Cast==
* Sabyasachi Chakrabarty as Feluda
* Saswata Chatterjee as Topshe
* Robi Ghosh as Jatayu 
{{Cite web
|url=http://www.telegraphindia.com/1080418/jsp/entertainment/story_9155135.jsp
|title=Blood and brandy
|publisher=The Telegraph
|accessdate=2009-03-12
|last=
|first=
}}
 
* Haradhan Bandopadhyay as Dinanath Lahiri
* Ajit Bandopadhyay as Sidhu Jyatha
* Pradeep Mukhopadhyay as Prabir Lahiri
* Dibya Bhattacharya as Naresh Chandra Pakrashi
* Rajaram Yagnik as G.C.Dhameeja

==References==
 

==External links==
*  
*  
 
 
 
 

 
 
 
 
 
 
 
 
 


 