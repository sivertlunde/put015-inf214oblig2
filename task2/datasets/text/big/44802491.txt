The Laughing Lady (1929 film)
{{Infobox film
| name           = The Laughing Lady
| image          =
| caption        =
| director       = Victor Schertzinger
| producer       = Paramount Famous Lasky Corporation
| writer         = Bartlett Cormack Arthur Richman
| based on       = play, The Laughing Lady, by Alfred Sutro 
| starring       = Ruth Chatterton Clive Brook
| cinematography = George Folsey
| editing        =
| distributor    = Paramount Pictures
| released       = December 28, 1929
| runtime        = 80 minutes
| country        = USA
| language       = English
}}
The Laughing Lady is a 1929 sound film melodrama directed by Victor Schertzinger, starring Ruth Chatterton and produced and released by Paramount Famous Lasky Corporation.  It is based on a 1922 British play, The Laughing Lady, by Alfred Sutro. The play was brought to New York in 1923 and put on Broadway.

A 1924 Paramount silent film retitled A Society Scandal starred Gloria Swanson. It is now lost.

In 1930 an alternate sound version was produced by Paramount in Hungarian with a Hungarian director and cast.

Jeanne Eagels was to star in the film but died before production began.

==Cast==
*Ruth Chatterton - Marjorie Lee
*Clive Brook - Daniel Farr Dan Healy - Al Brown
*Nat Pendleton - James Dugan
*Raymond Walburn - Hector Lee
*Dorothy Hall - Flo
*Nedda Harrigan - Cynthia Bell (*as Hedda Harrigan)
*Lillian B. Tonge - Parker
*Marguerite St. John - Mrs. Playgate
*Hubert Druce - Hamilton Playgate
*Alice Hegeman - Mrs. Collop Joe King - City Editor
*Helen Hawley - Rose
*Betty Bartley - Barbara

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 