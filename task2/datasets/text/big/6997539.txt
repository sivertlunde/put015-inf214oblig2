Matir Moina
{{Infobox Film |
 name =Matir Moina  |
 image =Matir Moina.jpg|
 director =Tareque Masud |
 producer = Catherine Masud |
 writer =Tareque Masud,  Catherine Masud |
 starring =Nurul Islam Babu,  Russell Farazi,   Jayanta Chattopadhyay, Rokeya Prachy, Soaeb Islam, Lameesa Reemjheem |
 original_music =Moushumi Bhowmik | cinematography  = Sudheer Palsane|
 distributor = Audiovision   MK2 |
 released =2002 |
 runtime =98 minutes |
 country =Bangladesh | Bengali |
 budget = BDT 24,000,00
 awards = 2002 Cannes Film Festival, FIPRESCI International Critics Prize |
 }}
Matir Moina ( ; also spelled Matir Moyna and known in   for the Academy Award for Best Foreign Language Film.

Matir Moina deals with Masuds own experiences studying at a madrasah against the increasing tensions in East Pakistan culminating in the Bangladesh War of Liberation. Throughout the film there are references to historical occurrences in agitated times, and the film portrays these episodes through the human experiences of the young protagonist, his family, and his teachers and peers at the madrasah. Matir Moina won a number of awards internationally but was initially banned in Bangladesh on the grounds that it dealt with issues sensitive to the religious. The ban was repealed and the DVD version was released on April 16, 2005. 

==Credits==
According to the Masuds, the film was shot almost entirely with non-professionals in local settings often using local sounds. The cinematography attempted to capture the seasons in rustic appeal and the festivals and holidays of Bangladesh.

===Cast===
* Nurul Islam Bablu as Anu
* Russell Farazi as Rokon, his friend
* Jayanto Chattopadhyay as Kazi Shaheb, Anus father
* Rokeya Prachy as Ayesha Bibi, Anus mother
* Soaeb Islam as Milon, Anus uncle
* Lameesa R. Reemjheem as Asma, Anus sister
* Moin Ahmed as Ibrahim Shaheb, one of Anus teachers
* Mohammed Moslemuddin as the headmaster
* Abdul Karim as Halim Mia
* Manjila Begum as a female singer
* Momtaz as a female folk singer
* Mamunr Rahman(Sojib Khan)

===Crew===
* Tareque Masud, director, co-producer, and co-writer
* Catherine Masud, co-producer, editor and co-writer
* Sudheer Palsane, cinematography
* Indrajit Neogi, sound recording
* Moushumi Bhowmik, music direction
* Momtaz Begum, female playback singer for "Pakita bondi achhe deher khachai" (roughly translating as "the bird is trapped in the caged body")

==Synopsis==
 
The film is set against the backdrop of unrest in East Pakistan in the late 1960s leading up to the Bangladesh War of Liberation. In this setting, a small family must come to grips with its culture, its faith, and the brutal political changes entering its small-town world. Anu, a young boy, is sent off to a madrasah by his unbendingly devout father Kazi. Anus younger sister falls ill and dies because of Kazis refusal to use conventional medicine. While at the madrasah, Anu befriends Rokon, an eccentric misfit in the rigorous religious school, who is forced by the teachers to undergo an exorcism by ducking in the freezing river to cure himself.

Personal tragedies beset the family and tests its loyalty to the obdurate patriarch Kazi, who still believes in the religious unity of Pakistan, in the face of cruel, contradictory events.

A shattering political development then changes their town, their life, and the inner dynamics of the family, including the patriarchs role.

==Soundtrack==

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Translation !! Singer(s)
|-
| 1
| "Jodi Bheste Jaite Chao"
| If You Wish to Go to Heaven
|
|-
| 2
| "Pakhita Bondi Aachhe"
| The Bird Is Trapped In the Bodys Cage
|
|-
|}

==Awards==
Wins   
*2002 Cannes Film Festival, FIPRESCI Prize in section Directors Fortnight outside competition 
*2002 International Film Festival of Marrakech, Best Screenplay Award - Tareque Masud & Catherine Masud

Nominations 
*2002 International Film Festival of Marrakech, Golden Star - Tareque Masud
*2004 Nominated, Directors Guild of Great Britain, Outstanding Directorial Achievement in Foreign Language Film - Tareque Masud

==Controversy and censorship==
Matir Moina became the first full-length feature film from Bangladesh to be selected for the Cannes Film Festival and was the opening film of the Directors Fortnight section. At about the same time, the Bangladesh Film Censor Board felt the film was too sensitive to be screened in Bangladesh due to some religious overtones. The Masuds took their case to the Appeal Board and were able to get the decision of the government reversed. Thus, the film was finally shown in the country of its creation in late 2002.

==Sequel==
 

==Reception==
The film currently holds 89% of critics rating at Rotten tomatoes.

==DVD==
The DVD of Matir Moina was released on 16 April 2005 by Laser Vision. It includes a two-hour documentary that includes shootings, interviews, and opinion from the audience. It is thought to be the first interactive DVD in Bangladesh.

==Notes==
#    

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 