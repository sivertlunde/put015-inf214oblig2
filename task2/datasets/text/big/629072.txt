Hard Eight (film)
{{Infobox film
| name           = Hard Eight
| image          = Hardeight.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Paul Thomas Anderson
| producer       = Hans Brockmann François Duplat Keith Samples
| writer         = Paul Thomas Anderson
| starring       = Philip Baker Hall John C. Reilly Gwyneth Paltrow Samuel L. Jackson
| music          = Jon Brion Michael Penn
| cinematography = Robert Elswit
| editing        = Barbara Tulliver
| studio         = Rysher Entertainment
| distributor    = The Samuel Goldwyn Company 
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $222,559 
}} crime Thriller thriller film written and directed by Paul Thomas Anderson, starring Philip Baker Hall, John C. Reilly, Gwyneth Paltrow and Samuel L. Jackson. There are also brief appearances by Robert Ridgely, Philip Seymour Hoffman, and Melora Walters. 

The film, originally titled Sydney, was Andersons first feature; Hall, Reilly, Ridgely, Hoffman and Walters acted in Andersons subsequent films. It was screened in the Un Certain Regard section at the 1996 Cannes Film Festival.    The film was expanded from the principal idea of Andersons short film Cigarettes & Coffee (1993).      

==Plot==
Sydney (Philip Baker Hall), a gambler in his 60s, finds a young man, John (John C. Reilly), sitting forlornly outside a diner and offers to give him a cigarette and buy him a cup of coffee. Sydney learns that John is trying to raise enough money for his mothers burial. He offers to drive him to Las Vegas and teach him how to make some money and survive. Skeptical at first, John agrees.

Two years later, John, having gotten the money for the funeral, has become Sydneys protégé. John has a new friend named Jimmy (Samuel L. Jackson), who does security work, and is attracted to Clementine (Gwyneth Paltrow), a cocktail waitress in Reno. 

Sydney encounters Clementine at night, learning she moonlights as a prostitute. Although Clementine believes Sydney might want to sleep with her, Sydney actually wants to set her up with John.

Sydney gets a frantic late-night phone call. John summons him to a motel. He arrives to find John and Clementine holding a hostage. He turns out to be a customer who refuses to pay Clementine for sex. John also reveals that he and Clementine eloped. The situation is dangerous, because John and Clementine have called the hostages wife to demand $300. They do not have a plan, and they have beaten the hostage badly.

Sydney manages to smooth the situation over. He then advises John and Clementine to leave town and head to Niagara Falls for their honeymoon. After the two leave, Sydney is confronted by Jimmy, who threatens to tell John that Sydney killed Johns father unless Sydney gives him money. Sydney pays up, but later sneaks into Jimmys house and kills him. He then returns to the same diner where he met John. The film ends with Sydney covering up blood on his shirt cuff.

==Cast==
* Philip Baker Hall as Sydney Brown
* John C. Reilly as John Finnegan
* Gwyneth Paltrow as Clementine
* Samuel L. Jackson as Jimmy
===Cameos===
* Philip Seymour Hoffman as craps player
* Robert Ridgely as keno manager
* Melora Walters as Jimmys girl

==Reception==
Reviewing the film shortly after its release, Roger Ebert wrote, "Movies like "Hard Eight" remind me of what original, compelling characters the movies can sometimes give us."  Stephen Holden wrote, "Hard Eight is not a movie that wants to make a grand statement. It is really little more than a small resonant mood piece whose hard-bitten characters are difficult to like. But within its self-imposed limitations, it accomplishes most of what it sets out to do. And the acting is wonderfully understated, economical and unsentimental." 

Review aggregation website Rotten Tomatoes gives the film a score of 82% based on reviews from 34 critics. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 