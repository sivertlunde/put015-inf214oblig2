Trouble on the Corner
 
{{Infobox film

 | name = Trouble on the Corner
 | caption = 
 | director = Alan Madison
 | writer = Alan Madison
 | starring = Tony Goldwyn,  
                  Edie Falco
 | producer = Alan Madison,  Dan Stern,  Diane Kolyer,  Glenn Krevlin,  Henry Eisenberg
 | music = 
 | cinematography = Phil Abraham
 | distributor = Trouble On the Corner L.C.C.
 | released = 9 October 1997
 | runtime = 114 minutes English
 | followed_by = 
 | 
 | image = Trouble on the Corner VideoCover.png
}} 1997 Crime, Drama Film in which Tony Goldwyn plays Jeff Steward, a psychologist, takes good care of his patients mostly living in the same apartment. One day a piece of the bathroom ceiling collapse so he can watch the woman living in the upper apartment taking a bath. This causes total disorder of his normal life and he starts mixing the patients psychoses up with his own.

==Cast==
* Tony Goldwyn - Jeff Steward
* Edie Falco - Vivian Steward
* Debi Mazar - Ericca Ricce
* Joe Morton - Detective Bill
* Tammy Grimes - Mrs. K.
* Giancarlo Esposito - Daryl
* Roger Rees - McMurtry
* Bruce MacVittie - Sandy
* Mark Margolis - Mr. Borofsky
* Charles Busch- Ms. Ellen

==Production crew==
*Directed by Alan Madison

==External links==
*  

 
 
 
 
 
 
 
 


 

 