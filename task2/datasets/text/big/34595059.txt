Yellow Faced Tiger
 
 
 
{{Infobox film
| name           = Yellow Faced Tiger
| image          = Yellow Faced Tiger.jpg
| caption        = Theatrical release poster
| film name = {{Film name| traditional    = 黄面老虎
| pinyin         = Huang mian lao hu}}
| director       = Lo Wei
| producer       = Lo Wei   Raymond Chow   Leonard Ho
| writer         = Lo Wei   Chang Yung Hsiang
| starring       = Wong Tao   Sylvia Chang   Chuck Norris   Dan Ivan 
| music          = Joseph Koo
| cinematography = David Bailes
| editing        = Fred Cuming
| studio         = Lo Wei Motion Picture Company Golden Harvest
| released       =  
| runtime        = 100 minutes
| country        = Hong Kong
| language       = Mandarin
}}
Yellow Faced Tiger ( ; aka  Slaughter in San Francisco, and two undefined titles which includes Chuck Norris Vs. The Karate Cop or Karate Cop in the United States) is a 1974 Hong Kong martial arts action film starring Chuck Norris and Wong Tao. This was Norriss first starring role in a low budget Hong Kong picture.

== Summary ==
An early Chuck Norris kung-fu vehicle where he plays the biggest drug king in San Francisco who owns everyone, the police department included. He finds his match when a young police officer stands up to the drug lords power and must fight him and the entire system. A former cop in the San Francisco Bay Area goes after the goons who murdered his ex-partner.

== Plot == Robert Jones) and Wong (Wong Tao) arrest two men raping a girl named Sylvia (Sylvia Chang), utilizing their knowledge of martial arts to take down the assailants, but at the station Sylvia refuses to press charges claiming that she knew the men and that the altercation was all in fun, in clear contradiction of what had truly taken place. In retribution for their arrest, the rapists kidnap John in broad daylight and take him to the beach, where he is beaten by numerous assailants. Wong manages to come to his partners rescue, killing one of them. For this, he is kicked out of the force by his superior Captain Newman (Dan Ivan) and soon imprisoned, but John still keeps in touch with his old friend. After serving his sentence, Wong takes a job as a waiter.  While waiting tables he meets Chuck Slaughter (Chuck Norris), a criminal mastermind. Slaughter offers Wong a job in his organization but Wong refuses, even as Slaughter threatens to have him killed.

John witnesses some men running away from a bank robbery one morning. While John is chasing some bank robbers. Hes pursues the robbers, but there are too many for him and one of them pulls a gun. John runs but is caught in the backyard of the Chu family, where the robbers kill him. The next morning when the police find his body the captain, Newman, accuses Mr. and Mrs. Chu of being in on the murder and the bank robbery. Newman has the Chus arrested and locked up without bail despite Mr. Chus claims that he had nothing to do with the crime and didnt even see the men killing John in his backyard. Wong becomes involved because he wants to avenge Johns death and free the Chus. He begins shaking down Chinese criminals looking for clues, and they lead him to Captain Newman. Newman, who was working for Mr. Slaughter, intended to frame the Chus for the crime committed by Slaughters own men.

Wong fights Newman and kills him. After Wong kills Newman, he attempts to warn Chus defending lawyer, but the boss of the gang has already murdered the lawyer (Joy Sales). Wong is confronted by Slaughters men on the street, and fights them all off, eventually confronting Slaughter himself. Wong defeats Slaughter after a heated battle, and almost kills him when the police burst onto the scene. The new police captain stops Wong from delivering the lethal blow, informing him that he knows the whole story. Wong is reinstated to the police force, but refuses to carry a pistol saying "(He) has no need of guns".

== Cast ==
{|class="wikitable"
|-
! Actor !! Role
|-
| Wong Tao || Officer Don Wong (Yellow Faced Tiger)
|-
| Sylvia Chang || Sylvia Clu
|-
| Chuck Norris || Chuck Slaughter
|-
| Dan Ivan || Captain Newman
|}

== Reception ==
 
The movie gained a negative reception from critics. 

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 