Stage Left (film)
{{Infobox film
| name           = Stage Left
| image          =
| caption        =
| director       = Austin Forbord
| producer       = Jeremy Briggs Paul Festa Kenneth Rainin Foundation
| writer         = Austin Forbord
| narrator       = William Ball Marga Gomez (narrator)
| music          =
| cinematography = Austin Forbord
| editing        = Austin Forbord Paul Festa
| distributor    = Rapt Productions
| released       =  
| runtime        = 82 minutes (theatrical version) 56 minutes (TV version)
| country        = United States English
| budget         =
| gross          =
| awards         =
}}
Stage Left: A Story of Theater in San Francisco (2011 in film|2011) is a documentary film about the history of theater in the San Francisco Bay Area from about 1952 until 2010. The film was directed by Austin Forbord, and features Robin Williams, Peter Coyote, Herbert Blau, Tony Taccone, Oskar Eustis, Bill Irwin, Joan Holden, R. G. Davis, Richard "Scrumbly" Koldewyn, and is narrated by Marga Gomez. 

The film premiered at the Mill Valley Film Festival in October 2011 and aired on KQED-TV on November 11, 2012.

==External links==
* 
* 


 
 
 
 
 
 
 
 
 

 