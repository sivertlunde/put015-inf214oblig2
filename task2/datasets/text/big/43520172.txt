One Night in Istanbul
 
 
{{Infobox film
| name           = One Night in Istanbul
| image          = ONII Movie Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = James Marquand
| producer       = Nicky Allt, Matthew Whyte, Krystian Kozlowski
| writer         = Nicky Allt
| based on       =   Paul Barber Lucien Laviscount Samantha Womack
| music          = 
| cinematography =
| editing        =
| studio         = Big Ears Entertainment Karma Film Productions Stray Dog Films
| distributor    = Stray Dog Films Liverpool F.C.
| released       =  
| runtime        = 93 minutes 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Paul Barber, Lucien Laviscount, Samantha Womack, and Ingvar Eggert Sigurðsson. It is based on the play of the same name by Nicky Allt. 

The films trailer was revealed on 8 August 2014 and was released on 11 September 2014, exclusively at Odeon Cinemas in the United Kingdom and Republic of Ireland|Ireland. The film was released worldwide on 15 September. 

==Synopsis==
Two down on their luck Liverpool cabbies, Tommy and Gerry, strike an unusual deal with a local gangster to take their sons on a trip of a lifetime, to watch their beloved football team play in the European Cup Final in Istanbul. Hoping to use the trip as a chance to bond with their sons, big trouble awaits them in the form of a sexy hotel chambermaid, two ruthless crooks on a mission and a bag of counterfeit cash”. 

==Cast==
* Steven Waddington as Tommy Paul Barber as Gerry
* Lucien Laviscount as Joseph
* Samantha Womack as Carmella Jones
* Gamze Seber as Leyla
* Ingvar Eggert Sigurðsson as Altan Mark Womack as Tony Fitz
* Natasha Jones as Tia Edwards

==Release==
The premiere was held on 10 September 2014 at the Odeon Cinema in Liverpool One and was attended by Steven Gerrard and former Liverpool player Jamie Carragher along with many other former Liverpool players. The film went on general release in the UK on 11 September. 	
 
==References==
 

== External links ==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 