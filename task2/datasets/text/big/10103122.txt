Cursed (2004 film)
{{Infobox film
| name               = Cursed
| image              = Cursed_(2004_film).jpg
| screenplay         = Yoshihiro Hoshino
| based on           =  
| starring           = Kyôko Akiba Yumeaki Hirayama Takaaki Iwao Etsuyo Mitani Hiroko Sato Osamu Takahashi Susumu Terajima
| director           = Yoshihiro Hoshino
| producer           = Digital Frontier   TaKe Shobô
| movie_music        =  Kuniyuki Morohashi
| distributor        =
| released           =  
| runtime            = 81 minutes
| country            = Japan
| language           = Japanese
}}
 Japanese horror film. Based on the written work of Yumeaki Hirayama, it is the directorial debut of Yoshihiro Hoshino.   

The full Japanese title is Extremely Scary Story A: Dark Crow, and is the first theatrical release of the Cho Kowai Hanashi series.

==Summary==
The story centres on a haunted convenience store, the disturbing effect it has on the owners and other local residents, and the grisly deaths of the customers who shop there. 

==Plot==
 
Two students, Yuko and her friend, arrive at Mitsuya where Yuko proclaims she cannot enter the shop. She backs away too far and is killed by a truck.

Ryouko Kagami, a representative of the Cosmo Mart chain, is looking is to take Mitsuya into the chain on behalf of her boss, Tejima. She explains to the shop owners, Mr and Mrs Kitura, that Tejima could not come as he was in an accident. Asked for more details, Ryouko explains that that his feet were amputated, which the Kituras find amusing. Kitura tells her she must do the inventory like Tejima promised before they allow them to stock Cosmo Mart products.

Nao Niigaki, the young woman who works the morning part-time shift, becomes friends with Ryouko. Ryouko goes to do the inventory and a customer in a hooded winter coat comes in and starts reading a magazine. Nao tries to get a good look at his face but can only see darkness.

Later, a man comes in and buys a product for 666 yen. While walking home, that man encounters a white ball which has rolled from a dark alley. He picks up the ball and walks into the alley where a voice asks him to return the ball. He walks into the darkness, disappears, and the ball is bounced outside again.

While Nao sorts the drinks in the freezer, she sees a pair of eyes staring back at her. Ryouko, while doing inventory, finds a product that expired three years ago. She questions the Kituras, who blankly continue staring in the video camera of the store. Two crows crash into the window, killing themselves. Nao and Ryouko go to outside to investigate, where the Kituras are somehow already present with a hose. They hose away the crow remains, laughing madly.

The night-shift part-timer, Komori, takes Naos position. That night, a man and woman buy products for 699 yen and 999 yen respectively. On the way home, the woman is stalked by a man with a sledgehammer, who eventually appears inside her apartment and attacks her.

The next day, Ryouko receives a call from Tejima, who tells her "everyone has feet" before the signal becomes static. Tejima puts the phone down and murmurs "Ghosts dont have feet" and continues to watch people pass by in his wheelchair. That night, Komori serves the hooded man who was in the shop the previous day. The till registers 44.44444 yen, and Komori looks up and finds only blackness in the hood. The man then forces Komoris head inside the hood. Komori comes out in shock and panic. Ryouko arrives and sees that one of Komoris eyes has monstrously bulged out of his sockets. The Kituras smile blankly at Komori.

Ryouko takes his shift for him and sells products to a woman for 666 and a man whose total was originally 907 until he was hypnotized by steamed buns. He buys one and the price changes to 999.

That night, the woman (the one who bought products previously and whose total was 666) was cooking food for her boyfriend until a blackout occurred. She opens her fridge to find a long lit-up corridor. Confused, she closes it and the light returns to her house. She opens it again to find a pale girl coming out of the corridor. She climbs out and continues to get closer to the woman. A hand appears behind her and she turns around to find the girl. The woman takes the knife and stabs her. The camera reveals that the girl behind her was actually her boyfriend and that she only sees the ghost. The woman is then shown to have suffocated by a bag over her head, it is unknown if she committed suicide or the ghost finished killed her. The camera zooms onto a picture on the fridge of her and her boyfriend, with the ghost disfiguring half her face.

That same night, the man (the one who had been hypnotized by steamed buns and whose total came to 999) is at a Japan bathhouse. He pays the old man in front who is blankly staring at the screen and enters the bathhouse. While taking a bath, he find a weird bloodstain, moving shadows and somebodys hair. When he exits the bath, he slips on the hair and is knocked unconscious. When he wakes up, the old man is over him asking if he is okay. The old man then suddenly grabs the mans head and smashes his head on the corner of a slab, likely killing him. He grabs the mans steamed bun and eats it while watching the television.

That same night, Nao is walking home until she arrives at a railway with a train running through it. The hooded man appears and tries to pull her toward the train. She resists until the train passes and the hooded man disappears.

While on her way to work, Nao finds a homeless woman who tells her to come with her to learn more about the store. Nao examines the baby carriage the woman is pushing to find a china doll in there. Nao leaves with the woman, looking back to find Komori staring blankly into space like the Kituras. Nao and the woman talk at the park. Ryouko, passing by, joins them. The woman reveals that the man who owned the store was a very evil man who abused everyone. While building the store, the owner abused his contractor in many ways with money. The contractor, in anger, broke all the tombstones of people without families and used the fragments to build the foundation. This explains why the store is haunted. Ryouko tells Nao to stop working there.

Nao plans to save Komori before he becomes insane like the Kituras. Nao comes to the store to find Komori in the bathroom hypnotized by the eyes of a ghost. The ghost draws Komori toward it until Nao saves him. Komori, regaining his mind, runs out of the store where, in the sun, he baptizes himself in the river. He throws away his jacket, regains his composure, and leaves with Nao.

Ryouko, walking toward Mitsuya to work, finds a wheelchair rolling down the hill. Temija tells her every ghost does not have feet and he is happy. Ryouko thanks him and walks away from Mitsuya.

The movie goes back to the beginning, now shot from Yukos point of view, where she sees the tortured souls in the store.

==Critical response==
 

==External links==
*  
*  
*   at Cinema-Repose.com
*   at SaruDama.com

==References==
 

 
 
 
 
 
 
 
 
 
 