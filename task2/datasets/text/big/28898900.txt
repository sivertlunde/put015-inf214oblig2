Rajavazhcha
{{Infobox film
| name           = Rajavazhcha
| image          = 
| image_size     = 
| alt            = 
| caption        =  Sasikumar
| producer       = Sasikumar
| writer         = S. L. Puram Sadanandan
| narrator       =  Saikumar  Ranjini
| Johnson
| cinematography = J. Williams
| editing        =    
| studio         = 
| distributor    = 
| released       = 1990
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Sasikumar starring Ranjini and Chithra.   

==Cast==
*Suresh Gopi  ...  Govindankutty Saikumar  ...  Vijayaraghavan (Younger brother of Govindankuttty)
*Thilakan  ...  Madhava Panikkar (father of Mini) Chithra  ...  Amminikutty (Wife of Govindankutty) Ranjini ... Mini (Wife of Vijayaraghvan)
*M. G. Soman  ...  Kuttan Nair
*Kaviyoor Ponnamma  ...  Naniyamma (Mother of Govindankutty and Vijayaraghavan)
*Sukumari ...(mother of Mini)
*Sankaradi  ...  Kaimal Innocent  ...  Ittoopp
*Jagathy Sreekumar  ...  Mathai
*Mohan Raj  ...  Karadi Vasu
*Mammukoya  ...  Mohammadkutty
*Priya  ...  Aleykutty

==Soundtrack== Johnson and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Etho kaikal maaykkunnu || P Jayachandran || Poovachal Khader || 
|-
| 2 || Mele mekhangal || KS Chithra, MG Sreekumar || Poovachal Khader || 
|-
| 3 || Vanchippaattolam thullum || MG Sreekumar, Chorus || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 
 
 


 
 