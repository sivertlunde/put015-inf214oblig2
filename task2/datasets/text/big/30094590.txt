Jolene (film)
{{Infobox film
| name           = Jolene
| image          = Joleneposter1.jpg
| caption        = Promotional poster
| director       = Dan Ireland
| producer       = Zachary Matz Riva Yares
| writer         = E. L. Doctorow Dennis Yares
| starring       = Jessica Chastain Dermot Mulroney Chazz Palminteri Rupert Friend Denise Richards Michael Vartan Frances Fisher Theresa Russell Jose Rosete
| music          = Harry Gregson-Williams
| cinematography = Claudio Rocha
| editing        = Luis Colina
| distributor    = E1 Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Jolene is a 2008 American  , itself inspired by Dolly Partons song, Jolene (song)|Jolene.  It premiered on 13 June 2008 at the Seattle International Film Festival where Chastain won the Best Actress award. It was later released in the United States on 29 October 2010.

==Plot==
 

Jolene (Jessica Chastain) is a 15 year old foster child recently orphaned. At 15, she marries Mickey, a "green", nerdy, 20 year old, partially to break free of the foster care. The teen couple live with Mickeys Uncle Phil (Dermot Mulroney) and Aunt Kay (Theresa Russell). Jolene asks Mickey about his mother, but Mickey rebuffs her and starts crying. To make him feel better, she then takes off the nightgown and seduces him, but Mickeys inexperience leads to uncomfortable and joyless sex. The next morning, Mickey and Uncle Phil wish Jolene a "Happy Birthday" with Kay asking Jolene to clean the floor. Uncle Phil walks in as Jolene is cleaning the floors, is aroused and they have sex. Starting an affair, they have passionate sex, with Phil promising to build a house for "just the two of them", but asks Jolene to keep the affair clandestine. Eventually, Kay walks in on them having sex and violently throws Jolene out of the house, Mickey, Phil and Kay fight, then Mickey walks out and commits suicide by jumping off a bridge. Aunt Kay has Jolene placed in a juvenile mental institution, meeting Cindy, a psychiatric nurse. Jolene asks for paper and colored pencils to draw. Her request is denied, but she is allowed crayons. Uncle Phil, visibly gray-haired, haggard and old-looking, is later sentenced to 18 months in prison. Cindy, a lesbian, takes a liking to Jolene and this culminates in another clandestine relationship. Cindy breaks Jolene out of the mental hospital and hides her, but Jolene soon breaks free and boards a bus west out of South Carolina forever. Jolene ends up in Texas, but after a brief relationship with a firebrand (also Dermot Mulroney), Jolene resorts to hitchhiking further west, supporting herself by prostitution.

Jolene next ends up in a Southwestern state, and works as a waitress at an outdoor diner, where she meets Coco Leger (Rupert Friend), an aspiring musician and tattoo artist. A relationship blooms and Jolene becomes a tattoo artist at the tattoo parlor Coco works at. After passing out and had sex, Coco is revealed to have a big addiction to cocaine, and supports his habit and the parlor by dealing cocaine.  One evening, a young woman enters the parlor and declares herself to be his wife, Marin, and introduces his son, Coco Jr. Coco returns and is confronted by both women, before he walks out with Marin and the baby. In a rage, Jolene destroys the parlor, dumps the cocaine on a table, steals the money and dumps her ring in the center of the cocaine, before dialing 911 and fleeing the scene.

Next, Jolene ends up in Las Vegas, working as an erotic dancer when she meets Sal Fontaine, a mobster. She moves into his high rise condo and at his insistence, quits dancing. It is revealed that Jolene has continued her art, improving. One morning, Sal demands Jolene dress, go downstairs and wait for him. She falls asleep in the restaurant and quietly returns to find Sal dead and the mob still in the condo looking for something. Jolene escapes and ends up boarding a bus to Tulsa.

In Tulsa, Jolene finds work as a receptionist and has a second job as a banquet server for upscale parties. There she meets Brad Benton (Michael Vartan), the son of a wealthy family. Although put off by his eccentricism, Jolene eventually dates and marries Brad. Although refusing to discuss her past at first, when Jolene becomes pregnant, he inquires and when he realizes she is less than pristine, becomes physically and emotionally abusive. He punches her in the face.  Jolene meets his parents around the time of her pregnancy.  They do not approve of her.  After the baby is born, his parents investigate Jolenes past and inform Brad.  He then begins to punch her again and dumps paint on her. She tries to leave and take Brad Jr. While staying in a shelter for battered women, the police come and arrest Jolene for kidnapping and impeached by her checkered past, Brad is granted an annulment, Jolene is convicted of the kidnapping and allowed supervised visits for every second Sunday monthly for one hour. Realizing this was more harmful than good, Jolene exits her babys life while he is an infant, reasoning he can imagine his mother to be anybody.

Finally, Jolene ends up in Los Angeles, working as a comic illustrator for a graphic novel company. She also reveals that a film studio wants to use her as an actress. Seeing a woman walking her four-year-old up the street, Jolene quickly imagines herself a famous actress returning to Tulsa in a limousine to visit her son and happily walks out of frame.

 

==Cast==
* Jessica Chastain as Jolene
* Dermot Mulroney as Uncle Phil
* Chazz Palminteri as Sal
* Rupert Friend as Coco Leger
* Denise Richards as Marin Leger
* Michael Vartan as Brad
* Frances Fisher as Cindy
* Theresa Russell as Aunt Kay
* Jose Rosete as Sid

==Reception==
The film received a mixed critical reaction. Rotten Tomatoes gives it a score of 50% based on reviews from 20 critics. 

Jeanette Catsoulis of The New York Times praised Chastains performance, noting that she "digs deep. Surrendering to her character’s smoky voice-over and disastrous judgment, the actress finds pockets of soul in a role that’s part Jessica Rabbit, part Marilyn Monroe." {{cite web 
| date = October 28, 2010
| author = Jeanette Catsoulis
| url = http://movies.nytimes.com/2010/10/29/movies/29jolene.html 
| title = Searching for Stability 
| work = New York Times 
| publisher = 
| archiveurl = http://web.archive.org/web/20101101164627/http://movies.nytimes.com/2010/10/29/movies/29jolene.html
| archivedate = 2010-11-01
}}  
Rex Reed of The New York Observer rated the film 3 out of 4 stars. Reed praised the casting; "This movie boasts a terrific cast, and Ms. Chastain not only holds her own corner of every scene, shes the only thing you want to watch." {{cite web 
| url = http://www.observer.com/2010/culture/jolene-was-worth-wait-two-year-old-film-finally-gets-spotlight 
| title = Jolene Was Worth the Wait: A Two-Year-Old Film Finally Gets the Spotlight
| work =New York Observer
| publisher = 
| date = 27 October 2010
| author = Rex Reed 
}} 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 