Tenussian Vaccuvasco
 
Tenussian Vaccuvasco is an experimental popular film by Daryush Shokof with an original structure and setting. the whole film takes place in a four story building with 9 windows, as we view 9 different life styles of nine different crowd of people from completely different ranks of the society.

== Plot ==

Tenussian Vaccuvasco (the word does not have any meaning except perhaps a state of chaos, and is just another word play by Shokof) was made in Berlin, Germany in 2000. It was produced by Taies Farzan, and was shot in 3 days with digital cameras and had a cast of over 100 people who had to undergo amazing physical burdens for the film.

Each window in the building shows the life of a crowd and or residents in the apartment in the building block. Residents differ from Boxers to drug dealers to lovers and or a Maniac couple slapping each other in the face for fun. Their funny ritual of slapping each other in the face ends in a bloody finale. as the film continues the stories begin to become more intertwined, and complicated.Finally they start to directly relate to one another, and begin to even collide and connect. The hilarious situations peak as people start going into other peoples apartments through the open windows in the building block. The party people enter the room of the drug dealer and steal the drugs that actually belongs to bad group of organized crime people who are after the dealer anyway, and will later find him to clear their accounts. many different situations and absurd dramaturgy of each scene in each apartment keeps the tenants busy and engaged with humorous results.

Soon more twists and troubles prevail alongside much fun and comedy moments that give the whole film a layer of lightheartedness. Shokof explained that some of the actors had to run up and down the stairs in order to be ready to act in the next scene and in a different apartment more than 5 times in a single hour to keep the continuity of the scenes intact as they had to shoot without a "cut". The Boxers had to box for over half an hour in their room on the fourth floor before the crane and camera could have reached them after covering other stories from the first floor up and "uncut". the boxers had to remain boxing for almost an hour to keep the film in continuity when the camera finally reached their scene on the fourth floor.

The music contributions by Legendary Ravi Shankar and Mikis Theodorakis, Dissidenten, Geheimbund, and Taies Farzan make the whole film a popular one for regular movie viewers even though the film remains an experimental effort to the end.

==CAST==

* Taies Farzan
*Hasan ali Mete
*Daryush Shokof
*Drew Sarich
*Candice Farzan
*Farkhondeh Maal

==Awards==

the film won 3 awards at the NYIIFVF in 2000; 

* Best Experimental film
* Best International Film
* Best International Director

== External links ==
*  
*  
*  
*  
*  
*  

== References ==

 

 