Two of a Kind (1983 film)
{{Infobox film
| name           = Two of a Kind
| image          = Twoofakind1983Poster.jpg
| caption        = Theatrical release poster
| director       = John Herzfeld
| producer       = Roger M. Rothstein Joe Wizan
| writer         = John Herzfeld
| starring = {{Plainlist|
* John Travolta
* Olivia Newton-John
* Oliver Reed
* Beatrice Straight
* Scatman Crothers
* Charles Durning
}} Patrick Williams
| cinematography = Fred J. Koenekamp
| editing        = Jack Hofstra
| distributor    = 20th Century Fox
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| gross          = $23,646,952
}} Patrick Williams. Travolta plays a cash strapped inventor while Newton-John plays the bank teller whom he attempts to rob. These two unlikely individuals must come to show compassion for one another in order to delay  Gods judgment upon the Earth. This is Travolta and Newton-Johns second film together, after Grease (film)|Grease. The film was unsuccessful both critically and commercially, but did yield three popular singles for Newton-John and a platinum rating for the soundtrack. 

==Plot==
Four angels &mdash; Charlie, Earl, Gonzales, and Ruth &mdash; have been in charge of Heaven for the last 25 years. They are playing a golf match in Heaven when their game is interrupted by God, who has now returned to the office and does not like what he sees down on Earth. God wants to order up another flood and start all over again (despite his promise in the rainbow that he never would again), but the angels persuade God to reconsider, reasoning that if a typical Earth man can reform, it would prove that all mankind is capable of it.

God agrees to the scheme, and the typical Earth man selected by the angels is Zack Melon &mdash; a failed inventor who, threatened by loan sharks, decides to hold up a bank. Zack points his gun at bank teller Debbie Wylder who ostensibly gives him all the money. But when Zack peers into the sack after the robbery, he sees that Debbie has substituted bank deposit slips for the cash and has kept the money for herself. Zack tracks her down to reclaim his stolen money. While dodging the loan sharks and the evil interventions of the devil, the two come to develop a relationship, which is put to the test when the two are threatened by a masked thug.

==Cast==
*John Travolta as Zack Melon
*Olivia Newton-John as Debbie Wylder
*Charles Durning as Charlie
*Oliver Reed as Beasley
*Beatrice Straight as Ruth
*Scatman Crothers as Earl Richard Bright as Stuart
*Toni Kalem as Terri
*Ernie Hudson as Detective Skaggs
*Jack Kehoe as Mr. Chotiner
*Robert Costanzo as Captain Cinzari
*Castulo Guerra as Gonzales
*Gene Hackman as God (uncredited) (Hackman would later work with Travolta in Get Shorty)

==Reception==
Two of a Kind received heavily negative reviews during its release.   It currently holds a 20% rating on Rotten Tomatoes.
 Staying Alive), Worst Actress Worst Picture.
The movie was nominated for a Stinkers Bad Movie Awards for Worst Picture. 

==Soundtrack==
 
The film was salvaged by a platinum soundtrack which yielded three singles for Newton-John:
 Twist of Fate" - No. 5 Pop (her last of 15 Top 10 Pop hits)
* "Take a Chance" (duet with John Travolta)- No. 3 AC (B-side to "Twist of Fate")
* "Livin in Desperate Times" - No. 31 Pop
 Journey had initially intended for their multi-platinum 1983 album Frontiers (Journey album)|Frontiers but which was only available on the soundtrack album, and Patti Austins "Its Gonna Be Special", which wasnt a major pop hit but peaked at #15 on the R&B charts and #5 on the Dance charts in 1984.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 