Team America: World Police
 
{{Infobox film
| name           = Team America: World Police
| image          = Team america poster 300px.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Trey Parker
| producer       = Scott Rudin Trey Parker Matt Stone
| writer         = Trey Parker Matt Stone Pam Brady Masasa Daran Norris Phil Hendrie
| music          = Harry Gregson-Williams
| cinematography = Bill Pope
| editing        = Thomas M. Vogt Scott Rudin Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 98 minutes  
| country        = United States
| language       = English
| budget         = $32 million   
| gross          = $52.1 million 
}}
Team America: World Police is a 2004 American  .

The use of marionettes instead of actors in an action film is a reference to Thunderbirds (TV series)|Thunderbirds, a popular 1960s British television show, although Stone and Parker were not fans of that show. The duo worked on the script with former South Park writer Pam Brady for nearly two years. The film had a troubled time in production, with various problems regarding the marionettes, as well as the scheduling extremes of having the film come out in time. In addition, the filmmakers fought with the Motion Picture Association of America, who returned the film over nine times with an NC-17 rating. The film was recut by a few seconds and rated R.

The film was released in the United States on October 15, 2004 and received generally positive reviews. Team America grossed over $52 million worldwide. The film was released on DVD in the United States on May 17, 2005, available in both R-rated and unrated versions.

==Plot== jock who Broadway actor with college majors in Theater and World Languages. Unbeknownst to the team, North Korean dictator Kim Jong-il is supplying international terrorists with weapons of mass destruction.

Using his acting skills, Gary successfully infiltrates a terrorist group in  , a group of terrorists blow up the Panama Canal as retaliation for what had happened to Cairo.
 Third World country. During a depressed state as an alcoholic, Gary finds himself reminded of his responsibility by a speech from a drunken Drifter (person)|drifter. Upon returning to Mount Rushmore, he finds the area in ruins, though Spottswoode and I.N.T.E.L.L.I.G.E.N.C.E. have survived.
 blowjob and undergoing a one-day training course, Gary is sent to North Korea, where he uses his acting skills to free the other members. The team then engages in a violent battle with the Film Actors Guild in which most of the actors are brutally killed. After Gary uses his acting skills to save Chriss life, Chris finally confesses to Gary that he mistrusts actors because when he was 19 years old, he was raped by the cast of the musical Cats (musical)|Cats. The team then confronts Kim Jong-il. Gary goes on stage and convinces the worlds leaders to unite by using the drifters emotional speech. Kim Jong-il kills Alec Baldwin with an assault rifle, and then is kicked over a balcony by Lisa. He is impaled on a Pickelhaube and is revealed to be an alien cockroach from the planet Gyron. The cockroach then departs in a miniature spaceship, promising to return. As Gary and Lisa begin a relationship, the team reunites, preparing to combat all of the worlds remaining terrorists.

==Cast==
* Trey Parker as Gary Johnston / Joe Smith / Carson / Kim Jong-il / Hans Blix / Matt Damon / Tim Robbins / Sean Penn / Michael Moore / Helen Hunt / Susan Sarandon / Drunk in Bar / Additional voices
* Matt Stone as Chris / George Clooney / Danny Glover / Ethan Hawke / Additional voices
* Kristen Miller as Lisa Jones Masasa as Sarah Wong
* Daran Norris as Spottswoode
* Phil Hendrie as I.N.T.E.L.L.I.G.E.N.C.E. / Chechen terrorist
* Maurice LaMarche as Alec Baldwin
* Chelsea Marguerite as French mother
* Jeremy Shada as Jean Francois
* Fred Tatasciore as Samuel L. Jackson
* Scott Land, Tony Urbano, and Greg Ballora as Lead puppeteers

 man dressed as a giant statue of Kim Il-sung, two live cats, two nurse sharks, and a cockroach, with the difference in size with the marionettes played for humorous effect.

A poster of The Barbi Twins was featured on the billboard in Times Square, making the Twins the only non-marionettes in the film.

==Production==
===Development===
 
After the "hassle" of producing their last film,  , Parker and Stone vowed never to create another movie.  The films earliest origins involve Parker and Stone watching Gerry Andersons Thunderbirds (TV series)|Thunderbirds on television while bored. When the duo saw the series, they recalled seeing it on television but were not fans. Parker found that the series was unable to hold his interest as a child because "the dialogue was so expository and slow, and it took itself really seriously."  The duo inquired about the rights to the series and found Universal Studios was doing a Thunderbirds (film)|Thunderbirds film directed by Jonathan Frakes. "We said, What? Jonathan Frakes is directing puppets? and then we found out it was a live-action version, and we were disappointed," said Parker.   
 Fox due to a one-line pitch regarding global warming, which Parker and Stone found hilarious and "insane." Parker recalled Stone running up to him during work at South Park holding the paper, who sat down and read the synopsis regarding "sudden global warming attacking the earth." The two were in tears from laughing.    The two got a copy of the script, and soon realized that The Day After Tomorrow was the "greatest puppet script ever written".  Originally intending to do a shot-for-shot puppet parody of The Day After Tomorrow, Parker and Stone were advised by their lawyers that there could be possible legal repercussions.  The spoof would have been called The Day After the Day After Tomorrow, and been released a day later than The Day After Tomorrow.  News broke of the duo signing on to create the film on October 17, 2002, with Stone revealing that it would be a homage to Anderson.    The news was confirmed in June 2003, with Variety (magazine)|Variety quoting Stone as saying "What we wanted was to do a send-up of these super important huge action movies that Jerry Bruckheimer makes."   

===Writing=== Pearl Harbor to get the nuances of the puppets just right when they were staring at each other, and also used Ben Affleck as a model.  To help shape the films archetypal heroes (from the true believer to the reluctant hero to the guy who sells out his friends for greater glory), they read the books of Joseph Campbell. "On one level, its a big send-up," Brady said. "But on another, its about foreign policy."  The first draft of the script was turned in well before the Iraq War.    The film takes aim at various celebrities, many of whom came out in opposition to the Iraq War in 2003. Brady explained that the films treatment of celebrities was derived from her annoyance at the screen time given to celebrities in the beginning of the Iraq War, in lieu of foreign policy experts. 

===Filming===
The films central concept was easier to conceive than to execute.  Team America was produced using a crew of about 200 people, which sometimes required four people at a time to manipulate a marionette. The duo were forced to constantly rewrite the film during production due to the limited nature of the puppets. The 270 puppet characters were created by the Chiodo Brothers, who previously designed puppets for films such as Elf (film)|Elf and Dinosaur (film)|Dinosaur. The costumers of the crew were responsible for making sure the over 1,000 costumes remained in cohesive order and were realistic. Production began on May 23, 2004.  The project was interrupted multiple times early on in production.  As soon as filming began, Parker and Stone labored to find the right comic tone; the original script for the film contained many more jokes. After shooting the very first scene, the two realized the jokes were not working, and that the humor instead came from the marionettes.  "Puppets doing jokes is not funny," Stone found. "But when you see puppets doing melodrama, spitting up blood and talking about how they were raped as children, thats funny."  The film was filmed by three units shooting different parts of the film at the same time. At times, the producers would have up to five cameras set up to capture the scene filmed.   

The film was mainly based on the 1982 cult classic action film Megaforce, of which Parker and Stone had been fans. Many ideas had been copied such as the flying motorcycle sequence. The film was painstakingly made realistic, which led to various shots being re-done throughout the process due to Parker and Stones obsession with detail and craftsmanship. For example, a tiny Team America-scale Uzi cost $1,000 to construct, and Kim Jong-ils eyeglasses were made with hand-ground prescription lenses.  Although the filmmakers hired three dozen top-notch marionette operators, simple performances from the marionettes was nearly impossible, with a simple shot such as a character drinking taking a half-day to complete successfully.  Both Parker and Stone agreed during production of Team America that it was "the hardest thing   ever done." Rather than rely on computer-generated special effects added in post-production, the filmmakers vied to capture every stunt live on film.  Parker likened each shot to a complicated math problem. 

The late September 2004 deadline for the films completion   took a toll on both filmmakers, as did various difficulties in working with puppets, with Stone, who described the film as "the worst time of   life," resorting to coffee to work 20-hour days, and sleeping pills and coffee to go to bed.    The film was barely completed in time for its October 15 release date. At a press junket in Los Angeles on October 5, journalists were only shown a 20-minute reel of highlights because there was no finished print.    Many of the films producers, besides Parker and Stone, had not even seen the entire film with the sound mix until the premiere.   

===Editing===
 
 urinating and defecating on one another.    The entire joke was based on what children do humorously with dolls such as Ken and Barbie. At least nine edits of the puppet love scene were shown to the MPAA before the board accepted that it had been toned down enough to qualify for an R rating.    Parker contrasted the MPAAs reluctance for the sex scene to their acceptance of the violence: "Meanwhile, were taking other puppets and, you know, blowing their heads off, theyre covered with blood and stuff, and the MPAA didnt have a word to say about that."    In addition to the sex scene, the MPAA were also upset with a puppet being eaten alive by sharks.    The duo faced a similar conflict with their previous film, South Park: Bigger, Longer & Uncut, in 1999.   
===Music=== 
{{listen
|filename=Team america march.ogg
|title=Harry Gregson-Williams - "Team America March"
|description=Main theme of the 2004 film Team America: World Police.
}}

{{Infobox album 
| Name        = Team America: World Police
| Type        = soundtrack
| Artist      = Various artists
| Cover       =
| Released    = 2004
| Recorded    = Various
| Genre       = Soundtrack
| Length      = 47:00
| Label       = Atlantic
| Producer    = Trey Parker, Matt Stone, Scott Rudin (exec.) , Scott Aversano (exec.) , Anne Garefino  (exec.) 
}} compose the original score and help Parker compose the films songs. He helped compose "Everyone Has AIDS" and "Derka Derk (Terrorist Theme)". He submitted a score, but the studio rejected it and fired Shaiman, hiring Harry Gregson-Williams as a last-minute replacement (Parker had instructed Shaiman to score the film as if it were a typical action film, which they agreed would make it funnier, while the studio felt the score should play up the comedy). However, Shaiman still conducted the orchestra in the films scoring sessions and Gregson-Williams stuck to the traditional action film score concept.

The films songs include: jingoist form of patriotism. Have You Forgotten?" by Darryl Worley and "Where Were You (When the World Stopped Turning)" by Alan Jackson. 
* "Derka Derk (Terrorist Theme)", an instrumental parody of "Cantina Band" from Star Wars.  Pearl Harbor.) 
* "Im So Ronery": Sung by Kim Jong-il when he feels everyone else is incompetent. Push It to the Limit" by Paul Engemann.  and "Holding Out for a Hero" by Bonnie Tyler, songs famed for appearances in 80s films.  The song features the line "even Rocky (film)|Rocky had a Montage (filmmaking)|montage". A variation of the song was featured in the South Park (season 6)|6th-season South Park episode "Asspen".
* "North Korean Medley": Gibberish song used to distract the group of people in  : 都市處女 媤집와요). Pearl Harbor Berlin from Top Gun. Broadway musical Lease (a parody of Rent (musical)|Rent). Magic Carpet Ride".
* The song playing when the team walks through Kim Jong-ils palace is Tomoyasu Hoteis "Battle Without Honor or Humanity", which was also featured in Kill Bill and the film "Pillows".
* There is also a bonus song sung by Kim Jong-il named "You Are Worthress, Arec Barrwin" during the end credits of the film.

==Individuals parodied== Queen Elizabeth II, Peter Jennings and Hans Blix. With the exception of Jennings, Tony Blair and Queen Elizabeth (and Sheen, whose death is not shown despite being involved in the F.A.G. vs. Team America battle), all are killed in dramatic and extremely violent ways.
 n leader Kim Jong-il was parodied in the film, and the DPRK asked the Czech Republic to ban it. ]] his daughters classmates would recite Kim Jong-ils line to him, "You are useress to me, Arec Bardwin."  Sean Penn, who is portrayed making outlandish claims about how happy and utopian Iraq was before Team America showed up, sent Parker and Stone an angry letter inviting them to tour Iraq with him, ending with the words "fuck you".  Both George Clooney and Matt Damon are said to be friends with Stone and Parker, and Clooney has stated that he would have been insulted had he not been included in the film. {{cite web |url=
http://www.contactmusic.com/new/xmlfeed.nsf/mndwebpages/clooney%20supports%20team%20america%20duo_30_01_2006 | title= Clooney Supports Team America Duo | date= January 31, 2006 | quote=  ...the Hollywood big-hitters all insist they would have been offended to be left out of the film. }} 

  extra and jokingly suggested he sing "Im So Ronery".
 glutton who suicide bombing socialist weasel" by I.N.T.E.L.L.I.G.E.N.C.E. Stone explained the reason for this portrayal in an MSNBC interview:
 We have a very specific beef with Michael Moore. . .  . I did an interview, and he didnt mischaracterize me or anything I said in Bowling for Columbine. But what he did do was put this cartoon   right after me that made it look like we did that cartoon.    

A deleted scene also shows Meryl Streep and Ben Affleck (who is portrayed with a real-life hand replacing his head).

==Reception==
Reviews of the film were generally positive; it holds a 77% ("Certified Fresh") at the review aggregator website Rotten Tomatoes, out of 193 reviews with the consensus "Team America will either offend you or leave you in stitches. Itll probably do both."  The film also holds a rating of 64/100 at Metacritic ("generally favorable reviews"), based on reviews by 38 critics.   It is one of Quentin Tarantinos top 20 films since 1992. 

Thunderbirds (TV series)|Thunderbirds creator Gerry Anderson was supposed to have met Parker before production, but they cancelled the meeting, acknowledging he would not like the films expletives. Anderson did see the completed film and felt "there are good, fun parts   but the language wasnt to my liking." 

National Review Online has named the film #24 in its list of "The Best Conservative Movies". Brian C. Anderson wrote, "the films utter disgust with air-headed, left-wing celebrity activism remains unmatched in popular culture."  However, political and social commentator Andrew Sullivan considers the film brilliant in its skewering of both the left and rights approach on terrorism. Sullivan (a fan of Stone and Parkers other work, as well) popularized the term "South Park Republican|South Park Republican" to describe himself and other like-minded fiscal conservatives/social libertarians. Parker himself is a registered Libertarian Party (United States)|Libertarian. 

The film suffered oppositional criticism prior to release. In August, Internet news aggregator Matt Drudge blasted Paramount and the filmmakers for trying to "mock the terror war." A week later, the conservative group Move America Forward criticized the film, saying it was "inconceivable" that filmmakers would have spoofed the Nazis during World War II.    Before the film was released, statements were released by a "senior Bush administration official" condemning the film. Upon receiving the news, the duo called and found it was instead a "junior staffer," causing Stone to quip "What is it – junior or senior? What are we talking about here? Who knows? It might have been the janitor." The two eventually decided it was free publicity, with which they were fine.  Some media outlets interpreted the films release on October 15 to be in theaters before the November elections. In reality, the release date had nothing to do with the elections; in fact, the film was intended to be released earlier, but production fell behind. 

===Box office=== Friday Night Lights. The film eventually grossed a total of $52,107,422, with $32,886,074 in U.S. domestic receipts and $18,221,348 in international proceeds. 

===Filmmakers response===
In an interview with Matt Stone following the films release,  Anwar Brett of the BBC asked the following question. "For all the targets you choose to take pot-shots at," he asked, "George W. Bush isnt one of them. How come?" Matt Stone replied, "If you want to see Bush-bashing in America you only have to walk about 10 feet to find it. Trey and I are always attracted to what other people arent doing. Frankly that wasnt the movie we wanted to make."

In another interview, Parker and Stone further clarified the end of the film which seems to justify the role of the United States as the "World Police." 

 

==Legacy==
  Amazon skyrocketed until copies were sold out in December 2014.  

Snippets of the film mocking Kim Jong-il are reportedly set to be included, alongside copies of The Interview, in helium-filled balloons launched by North Koran defectors into their home country in an effort to inspire education on the Western worlds views on it.  {{cite news |url=http://www.hollywoodreporter.com/news/interview-sequel-inside-frightening-battle-789873|title=The Interview Sequel: Inside the Frightening Battle Raging on the North Korean Border
|author= Paul Bond|date= April 20, 2015 |publisher=The Hollywood Reporter}} 
==See also==
* List of films set in or about North Korea

==References==
 
*   - set report from AintItCool.com
*   - synopsis, clips and images from LatinoReview.com
* (October 2004).   Wired 12.10. Retrieved October 6, 2004.
*   with Matt Stone
*   by composer M.Regtien

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 