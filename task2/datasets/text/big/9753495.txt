Black Girl (film)
 
 
{{Infobox film
| name        = Black Girl
| image       = LaNoiredeDVD.jpg
| caption     = DVD Cover
| director    = Ousmane Sembène
| writer      = Ousmane Sembène
| starring    = Mbissine Thérèse Diop, Anne-Marie Jelinek and Robert Fontaine
| producer    = André Zwoboda
| cinematography =Christian Lacoste
| editing     = André Gaudier
| distributor = New Yorker Video
| released    =  
| country     = France Senegal|
| runtime     = 65 minutes
| language    = French
}}
Black Girl is a 1966 film by the Senegalese writer and director Ousmane Sembène, starring Mbissine Thérèse Diop. Its original French title is La Noire de..., which means "The black girl of...", as in "someones black girl". The film centers on a young Senegalese woman who moves from Senegal to France to work for a rich French couple. It was the directors first feature-length film. {{cite book
 | first =Geoffrey
 | last =Nowell-Smith
 | year = 1996
 | title = The Oxford History of World Cinema
 | chapter =
 | editor =
 | others =
 | edition =
 | pages =
 | publisher =
 | location =
 | id = ISBN 0-19-874242-8
}}  It is often considered the first Sub-Saharan African film by an African filmmaker to receive international attention. 

==Plot== cosmopolitan lifestyle. But from her arrival in Antibes, Diouana experiences harsh treatment from the couple, who force her to work as a full servant. She becomes increasingly aware of her constrained and alienated situation and starts to question her life in France.

==Cast==
* Mbissine Thérèse Diop as Diouana
* Anne-Marie Jelinek as Madame
* Robert Fontaine as Monsieur
* Momar Nar Sene as Diouanas Boyfriend 

==Themes==
This film addresses the effects of colonialism, racism and post-colonialism|post-colonial identity in Africa and Europe. These themes are highlighted through the recurring appearance of the African mask Diouana gives to her employers on her first day of work at the house. The mask is hung on the wall in the French couples Senegalese apartment, along with other pieces of African art. 

==Significance==
In his 1997 book Movies as Politics, Jonathan Rosenbaum makes a case for Black Girl as the symbolic genesis of sub-Saharan African filmmaking, at least to the extent that the authorship belonged to a born and bred African. {{cite book
 | first =Jonathan
 | last =Rosenbaum
 | year = 1997
 | title = Movies as Politics
 | chapter =
 | editor =
 | others =
 | edition =
 | pages = 284
 | publisher = University of California Press
 | location = Berkeley, Calif
 | id = ISBN 0-520-20615-0}} 

==Awards==
* 1966, Prix Jean Vigo for best feature film

==References==
 

==External links==
* 
*   
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 