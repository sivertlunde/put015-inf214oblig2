Kitchen Party (film)
{{Infobox Film
  | name = Kitchen Party
  | image = KitchenParty_poster.jpg
  | caption = Kitchen Party theatrical poster Gary Burns
  | producer = Christine Haebler John Hazlett Gary Burns
  | starring = Scott Speedman Laura Harris Gillian Barber
  | music = Schaun Tozer
  | editing = Reginald Harkema
  | cinematography = Robert Aschmann
  | casting = Paul Weber
  | released = September 8, 1997
  | runtime =  1:32 English
}} Gary Burns (The Suburbanators). The movie cast a number of then-unknown young Canadian actors, including Scott Speedman, Laura Harris, and Tygh Runyan, and was released on September 8, 1997 at the Toronto Film Festival.

== Plot summary ==

In the bored suburban atmosphere of a Canadian city, Scott (Scott Speedman) decides to throw a celebratory get-together with friends in his parents home. Unfortunately, theres a catch: Scotts parents happen to be particularly anal about the direction the carpet fibers lay and the distance from doily to table-edge. This means that the only part of the house that is safe, that is, the only part of the house with no carpeting and therefore no potential mess, is the tiled kitchen.

The festivities begin once the parents go off to a party of their own, leaving Scott and his buddy, Wayne (Tygh Runyan), with a house that would be entirely empty but for Scotts mysterious brother lurking in the basement.

Soon the girls are arriving, including Scotts girlfriend, Tammy (Laura Harris) — whom he plans on bedding before the night is over — and alcohol, drugs, music, more people, and everything else that characterizes a stereotypical house party follows. This includes calamity, as Scott quickly discovers just how much can go wrong in one night of kitchen partying.

== Cast ==
*Scott Speedman as Scott
*Laura Harris as Tammy
*Tygh Runyan as Wayne John Payne as Bill
*A.J. Bond as Tim
*James McBurney as Cal
*Jenafor Ryane as Marni (credited as Janafor Ryane)
*Joelle Thomas as Marie Kevin McNulty as Brent
*Gillian Barber as Barb
*Marie Stillin as Marge
*Sarah Strange as Cynthia Dave Cox as Lester Jr.
*Jason Wiles as Steve

==Awards and nominations==
*  nominated for Tiger Award.
*  won FIPRESCI Prize - Special Mention for the film’s "incisive and ironic portrayal of middle class family life in Western society."
*  won a Special Mention.
* .
*  won for Best New Western Canadian Director.

==External links==
*  

 
 
 
 