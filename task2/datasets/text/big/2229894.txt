Romance & Cigarettes
{{Infobox film
| name = Romance & Cigarettes
| image = Romancecigarettes.jpg
| image_size =
| alt = 
| caption = Theatrical release poster
| director = John Turturro
| producer = John Penotti John Turturro
| writer = John Turturro
| starring = James Gandolfini Susan Sarandon Kate Winslet Tom Stern
| editing = Ray Hubley
| studio = GreeneStreet Films
United Artists
| distributor = Icon Film Distribution (UK) Boroturro (US)
| released =  
| runtime = 105 minutes
| country = United States
| language = English
| budget = 
| gross = $2,935,242 
}} musical romantic comedy film written and directed by John Turturro. The film stars an ensemble cast which includes James Gandolfini, Susan Sarandon, Kate Winslet, Steve Buscemi, Bobby Cannavale, Mandy Moore, Mary-Louise Parker, Aida Turturro, Christopher Walken, Barbara Sukowa, Elaine Stritch, Eddie Izzard, and Amy Sedaris.

==Main cast==
  
* James Gandolfini as Nick Murder 
* Susan Sarandon as Kitty Kane Murder  
* Kate Winslet as Tula 
* Steve Buscemi as Angelo  
* Bobby Cannavale as Chetty Jr. / Fryburg
* Mandy Moore as Baby Murder
* Mary-Louise Parker as Constance Murder
* Aida Turturro as Rosebud / Rara
* Christopher Walken as Cousin Bo  
* Barbara Sukowa as Gracie
* Elaine Stritch as Grace Murder 
* Eddie Izzard as Father Gene Vincent 
* Amy Sedaris as Frances
* P.J. Brown  as Police Officer
* Adam LeFevre as Francess boyfriend
* Tonya Pinkins as Medic
* Cady Huffman as Roe 
* Kumar Pallana as Da Da Kumar
 

==Production and release==
Produced by upcoming New York production company GreeneStreet Films,  with financial backing from United Artists, the Coen brothers and Mel Gibsons company Icon Entertainment International, Romance & Cigarettes premiered at the Venice Film Festival on September 6, 2005, followed by a showing at the Toronto International Film Festival a week later. It was first released in the United Kingdom and Ireland on March 24, 2006, quickly followed by a number of other European countries in March and April 2006. In the United States the film got a limited release on September 7, 2007, distributed by director Turturro himself, although it was originally intended that United Artists should handle the US distribution.  UA still owns a financial stake in this film, but the main underlying rights are currently with Icon.

==Reception==
Romance & Cigarettes has received mixed reviews; on Rotten Tomatoes, as of October 2011, it has a score of 52% 

On April 27, 2008, the film was viewed at the 10th Annual Ebertfest, in Champaign, Illinois. Ebertfest is Roger Eberts film festival near his hometown of Urbana, Illinois. Aida Turturro and Tricia Brouk were scheduled to attend the event. Ebert gave the film 4 stars out of 4.

==Soundtrack==
 
  Tom Jones Engelbert Humperdinck
# "Piece of My Heart" - Dusty Springfield
# "Answer Me, My Love" - Gene Ammons
# "Red-Headed Woman" - Bruce Springsteen
# "Scapricciatiello (Do You Love Me Like You Kiss Me?)" - Connie Francis Hot Pants" - Bobby Cannavale
# "Quando minnamoro" - Anna Identici
# "Little Water Song" - Ute Lemper Prisoner of Love" - Cyndi Lauper
# "Trouble (Elvis Presley song)|Trouble" - Elvis Presley Samson and Delilah Theme" - Victor Young
 
# "El cuarto de Tula" - Buena Vista Social Club
# "Piece of My Heart" - Erma Franklin
# "I Want Candy" - Mandy Moore with Aida Turturro and Mary-Louise Parker
# "Its a Mans Mans Mans World" - James Brown It Must Be Him" - Vikki Carr
# "The Girl That I Marry" - James Gandolfini and Susan Sarandon
# "Ten Commandments of Love" - Harvey & The Moonglows
# "I Wonder Whos Kissing Her Now" - Aida Turturro
# "Banks of the Ohio" - David Patrick Kelly and Katherine Borowitz
# "Piece of My Heart" - Janis Joplin
# "When the Saviour Reached Down for Me" - The R&C Choir
 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 