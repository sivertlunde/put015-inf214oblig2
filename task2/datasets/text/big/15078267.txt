Vaisali (film)
 Vaishali}}
{{Infobox film
| name = Vaishali
| image = Vaisali VCD Cover.jpg
| image_size =
| caption = Promotional Poster designed by Bharathan
| director = Bharathan
| producer = Atlas Ramachandran
| writer = M. T. Vasudevan Nair
| narrator = Geetha Babu Antony Nedumudi Venu Ravi O N V Kurup (lyrics)
| cinematography = Madhu Ambat
| editing = Bharathan
| studio = Chandrakanth Films
| distributor = Chandrakanth Films
| released =   
| runtime =
| country = India
| language = Malayalam
| budget =
| gross =
}} Malayalam film Vedavyasa to King Yudhishtira in the epic Mahābhārata|Mahabharatha. It is the story of Vaisali, a devadasi girl who was assigned the mission of seducing Rishyashrungan, the son of Mahirshi Vibhandakan and bringing him to Chambapuri to perform a mahayagam to cause rain. Shot in a lavish setting, Vaisali was  a huge hit at box office. This film, often considered as one of the best works of Bharathan is also considered as a cult classic in Malayalam cinema world. The cinematography by Madhu Ambat was widely appreciated and had won several awards. 

==Plot==
Angarajyam, under King Lomapadan (Babu Antony) is suffering from severe drought. There had been no rain for past 12 continuous years as the result of a curse by a Brahmin. The film begins with king Lomapadans visit to Rajaguru (Nedumudi Venu) requesting him to return to Champapuri, the royal capital of Angarajya. Chitrangadan (Ashokan (actor)|Ashokan), the son of rajaguru was in love with Vaisali (Suparna Anand), daughter of a devadasi, which was against the social customs. Rajaguru had migrated away from capital with an aim of separating his rebellious son from his lover. Now, with the repeated requests from King, Rajaguru decides to shift back to Champapuri to perform a yagam to please Lord Indra, the god of rain. On the night of arrival at the capital, Rajaguru, gets a mystic vision, in which his guru advices him to bring Rishyashrungan (Sanjay Mitra), the only son of Mahirshi Vibhandakan to perform a mahayagam at Angarajyam. Only the son of Vibhandakan can now save the nation. Rishyashrungan is a teenager, living deep inside a dense forest, along with his father, who is famous for his spiritual powers. Vibhandakan had taken all care to keep his son away from the rest of the world, since the day he was born. Rishyashrungan had not met any human being in his whole life apart from his father, and is unaware of the world outside his ashram inside the deep forest. Lomapadan and his group of ministers decide to send a beautiful girl to seduce and bring Rishyashrungan to Champapuri. Rajaguru, who had a deep rooted anger against Vaisali (Suparna Anand) asks king to send her to the forest. Vaisali, daughter of Malini (Geetha (actress)|Geetha), a famous dancer in the palace, is also  the illegitimate daughter of King Lomapadan, which he is unaware of. Malini, on the demand of the king sets out with her daughter to the forest. While the rest of the team waits outside the deep forest, Vaisali sets in and meets Rishyashrungan and attempts to seduce him with her looks. He unaware of the existence of any human being on the earth, is easily carried away by her exotic looks. But by evening Vibhandakan smells out the trouble and warns his son about the danger. But the teenager is again lost by the charm of Vaisali. But soon realizing his mistake, Rishyashrungan decides to take up deep meditation to attain mental power to win over Vaisali, whom he mistakes to be a Mayavi, who had come to prevent him from gaining eternal happiness and salvation. Vaisali, who by now had started loving him seriously is completely broken by seeing him on meditation in midst of fire. She, in her final attempt starts dancing sensuously in front of Rishyashrungan, and succeeds in alluring and taking him to Angarajyam. But the boy hermit regains his sense at the boat and is about to curse her. Malini, by then begs for mercy. On seeing Malini, another woman, the teenager hermit is surprised. Vaisali explains her mission to Rishyashrungan and convinces him of his power to bring rain. They start back to the kingdom. They are welcomed royally at the capital. King Lomapadan welcomes Rishyashrungan personally and takes him to the stage where the yaga has to be performed. Malini, who by now is expecting the King to accept Vaisali as his child tries to move towards him, but is stopped by the Rajaguru. Rishyashrungan performs the yagam successfully, and it rains heavily by the time havan ends. The entire nation is now on joyous mood and they celebrate by singing and dancing. Vaisali and her mother Malini, are completely forgotten by the King now. The Rajaguru, by then advises the king to offer Princess Shanta (Parvathy Jayaram|Parvathi), his daughter to Rishyashrungan, which means that the offspring of Rishyashrungan will rule Angarajyam after the time of Lomapadan. The King obeys the order and announces the marriage of the young saint and the princess, which is welcomed by the crowd whole heartedly. Vaisali, who, with in this time was completely in love with the saint is heartbroken. Her attempts to reach the king ends in vain and she along with her mother falls down on the ground. The mother is trampled to death by crowd. The crowd is no more bothered about them and the heavy rain continues.

==Cast==
* Sanjay Mitra as Rishyashrungan
* Suparna Anand as Vaisali
* Parvathy Jayaram as Shantha
* Babu Antony as Lomapadhan (dubbed by Narendra Prasad Geetha as Malini
* Nedumudi Venu as Rajaguru
* V. K. Sreeraman as Vibhanthakan
* Valsala Menon Ashokan as Chandrangadan
* Narendra Prasad as voice of  Lomapadhan
* Jayalalita

==Soundtrack==
The music was composed by Bombay Ravi and lyrics was written by ONV Kurup.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dum dum dum dundubhinaadam || Chorus, Dinesh, Lathika || ONV Kurup ||
|-
| 2 || Indraneelimayolum || KS Chithra || ONV Kurup ||
|-
| 3 || Indupushpam || KS Chithra || ONV Kurup ||
|-
| 4 || Poomullakkaattil || KS Chithra || ONV Kurup ||
|-
| 5 || Theduvathethoru || KS Chithra || ONV Kurup ||
|}

==Pre production==
*Bharathan had been planning to film Vaisali since 1982. But several factors made him to wait till 1988.
*Bharathan was searching for an actor to play the role of King Lomapadan. The moment he came across Babu Antony, he decided to cast him. As Bharathan was then working on Chilambu, he decided to launch Babu Antony as the main villain in it.

==Trivia==
*This was the first script by M.T. Vasudevan Nair for Bharathan. Later they worked again in Thazhvaram in 1990.
*Narendra Prasad dubbed for Babu Antony.
*The editing of Vaishali was done by Bharathan himself. National Award for his work. Kerala State National Award for the song "Indupushpam Choodi"
*The art direction was done by Krishnamoorthy.
*This film was produced by Dr. M. M. Ramachandran, popularly known as Atlas Ramachandran. He later produced films including Dhanam, and Sukrutham. He also directed a movie titled Holidays (2010 film)|Holidays in 2010. 
* Bharathan assisted by Sasi Menon did the Poster designs and Paper ads for the film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 