The Meeting (film)
{{Infobox film
| name               = The Meeting
| image              = The_Meeting_2013_Film.jpg
| alt                = 
| caption            = Theatrical poster
| director           = Mildred Okwo
| producer           =  
| writer         = Tunde Babalola
| starring       =  
| music          = Truth
| cinematography = Jim Bishop
| editing        = Okey Benson
| studio         = Mord Pictures Production
| distributor   = The Audrey Silva Company (TASC) Silverbird Distribution
| released       =  
| runtime        = 132 minutes
| country        = Nigeria
| language       =  
| budget         =  
| gross          =
}}
The Meeting is a 2012 Nigerian romantic comedy drama film produced by Rita Dominic & Mildred Okwo and directed by Mildred Okwo. It stars Femi Jacobs, Rita Dominic, Linda Ejiofor, Kehinde Bankole and Jide Kosoko with Special Appearances from Nse Ikpe Etim, Kate Henshaw and Chinedu Ikedieze. It received 6 nominations at the 9th Africa Movie Academy Awards and won the award for the category Achievement In Make-Up. 

==Plot== Corp member who pleads for a Joual#English loanwords (Anglicisms)|Lift, as she is short on cash and there is a long queue at the ATM. Makinde refuses at first, but after much persuasion from Ejura, he reluctantly agrees. Even though Ejura had promised earlier to keep mute during the ride, her inquisitive nature gives Makinde facial expressions that prompt answers and eventually start a conversation.

Mr Makinde arrives at the Ministry of land and is met by a scenario involving Mr. Ugor (Chinedu Ikedieze), being forced out of the building by security operatives. Makinde is in awe but manages to help find his way to the reception desk to meet the ministers discourteous secretary, Clara Ikemba (Rita Dominic). He is unapologetically informed by Clara that his meeting, which was originally scheduled for 9:30 am, has been moved to 4:30 pm. Makinde retires his stance and joins the other appointees who are all seated to see the minister. While waiting, Clara informs the appointees that she sells recharge cards and cold drinks to cater for their needs as they wait to see the minister, Ejura also calls Mr Makinde to thank him for the ride he gave to her earlier. Several hours has gone past and Makinde is yet to get a word from the secretary about his re-scheduled meeting. He decides to inquire about it from her but to his surprise, she informs him rudely that the minister has already left the office. Makinde argues that she could have told him and others waiting, instead of making people waste their precious time. Clara replies him "OYO (meaning On Your Own) is their case", a slang which means everyman is responsible to himself and she has no business telling them to wait or go home. The meeting is eventually rescheduled for the following day. Makinde checks into a hotel. While trying to fight boredom, Ejura calls and eventually joins Makinde in the hotel in a bid to keep him company.
 Igbo kinsmen arrive at the reception. Hours pass on and the kinsmen conclude their meeting with the minister only for Clara to tell Makinde after a confrontation that the minister is having his lunch and can not see anyone at the moment. Bolarinwa (Nse Ikpe Etim) stylishly enters the reception room to the amazement of everyone in the room. From her brief chat with Clara It is quite clear that she is a close friend to the secretary and the ministers mistress. shortly after the conversation she is granted entry to see the minister. After a while, Makinde stands up to inquire from the secretary again only to be told she is closing. Makinde asks about his appointment and she tells him the minister left 30 minutes ago. He tells her that she should have informed him and other people waiting and give replies with her signature sentence "OYO is their case".

==Cast==
*Femi Jacobs as Makinde Esho
*Rita Dominic as Clara Ikemba
*Linda Ejiofor as Ejura
*Kehinde Bankole as Kikelomo
*Jide Kosoko as MD
*Nse Ikpe Etim as Bolarinwa
*Chika Chukwu as Mrs Kachukwu
*Collins Richard as Jolomi
*Kate Henshaw as Mrs Ikomi
*Basorge Tariah as Professor Akpan Udofia
*Amina Ndim as Hajia
*Chinedu Ikedieze as Mr Ugo

==Reception==

===Critical reception===
The Meeting received critical acclaim.   of DStv wrote "The Meeting is a beautiful movie driven by its story line and well chosen cast of characters. It is a movie that sets out with pretty high ambitions and it manages to achieve them all". 

===Awards===
The Meeting received six nominations at the 9th Africa Movie Academy Awards including awards for categories Achievement In Costume Design, Achievement In Makeup, Best Nigerian Film, Best Actress In A Supporting Role for Linda Ejiofor, Best Actor In A Leading Role for Femi Jacobs and Best Actress In A Leading Role for Rita Dominic. It eventually won just the award for the category Achievement In Make-Up.  It also received 11 nominations at the 2013 Nollywood Movies Awards,  Rita Dominic won award for Best actress in a film for her role in The Meeting at the 2013 Nigeria Entertainment Awards. 

==References==
 

==External links==
*  
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 