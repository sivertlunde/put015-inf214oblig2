Kitty and the Bagman
{{Infobox film name            = Kitty and the Bagman image           = writer         =  starring  John Stanton, Liddy Clark, Val Lehman, Colette Mann,  Gerard Maguire, Kylie Foster director       = Donald Crombie producer       = Anthony Buckley cinematrography = Dean Semler distributor    =  Umbrella Entertainment studio         = Adams Packer Film Productions|Adams-Packer released       = 1983 runtime        =  country        = Australia language       = English music          = awards         = budget         = AU$2.5 million (est) David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p37-38 
| gross = AU $58,407 (Australia) 
}}
Kitty and the Bagman is a 1983 Australian film about gangsters in the 1920s.

==Production==
During filming a visit was paid to the set by then-treasurer John Howard and then-Prime Minister Malcolm Fraser. The film was completed in 1982 but its release was held back a year. 

Director Donald Crombie looked back on the film with mixed emotion:
 That probably shouldnt have been made. It was a bit of an aberration. That only got made because we were flush with funds. That was when it became ridiculously easy to make films... There were better things we should have been doing with our time.  

==Home Media==
Kitty and the Bagman was released on DVD by Umbrella Entertainment in October 2011. The DVD is compatible with all region codes.   

==References==
 

==External links==
*  at IMDB
*  at Oz Movies
 

 
 
 
 
 
 


 
 