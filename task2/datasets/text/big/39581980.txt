Million Dollar Arm
 
{{Infobox film
| name = Million Dollar Arm 
| image = Million Dollar Arm poster.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Craig Gillespie
| producer = Joe Roth Mark Ciardi Gordon Gray Tom McCarthy
| starring = Jon Hamm Aasif Mandvi Bill Paxton Suraj Sharma Lake Bell Alan Arkin
| music = A.R. Rahman
| cinematography = Gyula Pados
| editing = Tatiana S. Riegel Roth Films Mayhem Pictures Walt Disney Studios Motion Pictures
| released =  
| runtime = 124 minutes  
| country = United States
| language = English Hindi
| budget = $25 million 
| gross = $38.3 million 
}} biographical Sports sports drama Tom McCarthy. The film is based on the true story of baseball pitchers Rinku Singh and Dinesh Patel who were discovered by sports agent J.B. Bernstein after winning a reality show competition. The film stars Jon Hamm as Bernstein, Bill Paxton as pitching coach Tom House, Suraj Sharma as Singh, Madhur Mittal as Patel, and Alan Arkin. The films music is composed by A.R. Rahman. Produced by Joe Roth, Mark Ciardi, and Gordon Gray, the film was released theatrically by Walt Disney Pictures on May 16, 2014.  

==Plot==

J. B. Bernstein (Jon Hamm) is a big time sports agent who, along with his partner Ash Vasudevan (Aasif Mandvi) recently formed their own company. Unfortunately, all of J.B.’s clients have retired, and he is unable to reel in star football player Popo Vanuatu (Rey Maualuga). Desperate to find new clients, J.B. realizes India, with over one billion people, has real potential for untapped baseball talent. He approaches investor Mr. Chang with his proposal - a talent contest staged in India called “Million Dollar Arm.” Contestants score points by demonstrating they can pitch a baseball with speed and accuracy. Along with the prize money, two winners will be flown to the U.S. and receive coaching to become legitimate baseball prospects within two years. Chang commits to providing the funding, on the condition the prospects are ready within only one year. With no alternative, J.B. reluctantly assures Chang the winners will be ready for a major league try-out within one year.

J.B. approaches veteran baseball pitching coach Tom House (Bill Paxton) who explains cricket, the main sport played in India, and baseball have entirely different motions for bowling and pitching, and getting a fresh recruit ready for a try-out within one year is extremely unlikely, if not impossible. J.B. points how House has nothing to lose and everything to gain by taking up the challenge, and House agrees.

J.B. flies to India and is devastated at the traffic, overcrowding, the lax way the Indians conduct business. He is joined by long time elderly major league scout, Ray Poitevint (Alan Arkin), and hires Amit Rohan (Pitobash Tripathy) as his interpreter. After lengthy try-outs in numerous cities, two youngsters emerge as the winners - Rinku Singh (Suraj Sharma) and Dinesh Patel (Madhur Mittal), and they are flown to the U.S. to begin their baseball training. The pair, who grew up in poverty in India, are quickly overwhelmed by the sights and sounds of America. After they accidentally set off a fire alarm at the hotel J.B. arranged for them, J.B. is forced to let them stay at his home. For their baseball training, J.B. dumps the pair by House and his staff while he runs off to close more deals. In so doing, the pair, who both barely speak English, feel abandoned. J.B.’s tenant Brenda Fenwick (Lake Bell) is the only person who genuinely seems interested in their well-being.

The boys and Amit get drunk at a party and J.B. has to drive them home, losing his second crack at signing Popo (Rey Maualuga). Brenda calms him down and makes him realize he is treating the two boys like a business deal. The next day, J.B. trades in his Porsche for a Dodge Caravan and joins the boys for their prayers. Ignoring J.B.’s pleas of the two boys lack of readiness, Chang insists his terms be fulfilled and the boys demonstrate their baseball skills one year from the time they arrived in the US. ESPN, Sports Illustrated, as well as local media are joined by numerous major league scouts to watch the boys pitch. The try-out is a complete disaster. The pair are both very nervous and pitch without speed or control, failing to impress anyone.

Brenda convinces J.B. that the boys be given another try-out. Chang refused to go along with it, and no scouts are interested in wasting their time on another fiasco. All hope is lost until Ray arranges for J.B. to meet the Pittsburgh Pirates head scout who was away in Puerto Rico for the first try-out, and agrees to come. This time, J.B. insists the boys relax and have fun. The scouts are quickly impressed as the pair consistently deliver 85 and 90&nbsp;mph fastballs thrown accurately, and both are offered a contract by the Pittsburgh Pirates.

==Cast==
* Jon Hamm as J. B. Bernstein 
* Aasif Mandvi as Ash Vasudevan, Bernsteins business partner   
* Suraj Sharma as Rinku Singh 
* Madhur Mittal as Dinesh Patel
* Bill Paxton as Tom House 
* Lake Bell as Brenda Fenwick 
* Alan Arkin as Ray Poitevint, a sports scout 
* Pitobash Tripathy as Amit Rohan, a baseball fanatic hired by Bernstein
* Allyn Rachel as Theresa, Bernsteins assistant 
* Darshan Jariwala as Vivek, a local Indian guide
* Tzi Ma as Will Chang, a sports business investor
* Bar Paly as Lisette
* Rey Maualuga as Popo

ESPN sports columnists Jayson Stark, Karl Ravech, and Steve Levy, as well as retired Major League Baseball players Barry Larkin and Curt Schilling, have cameo appearances in the film.    

==Production== Gordon Gray and Joe Roth. In early 2009, the screen rights to Singh and Patels life story were purchased by Sony Pictures Entertainment for development at Columbia Pictures, which hired Mitch Glazer to write a screenplay.   
 turnaround and Tom McCarthy to write a script.  In May 2012, Jon Hamm was hired to play J.B. Bernstein.     Alan Arkin    and Suraj Sharma    were hired in April 2013, with Allyn Rachel joining the cast the following month.    Principal photography began on May 30, 2013 with filming taking place in Mumbai, Atlanta, and Los Angeles.   

===Music===
 
A. R. Rahman composed the Film score|films score. The soundtrack album was digitally released by Walt Disney Records on May 13, 2014  whereas the CD release was released on May 19, 2014. 

==Release== trailer was CinemaCon in Walt Disney Alan Horn first Harry Potter film.  The film held its world premiere at the El Capitan Theatre on May 6, 2014. 

===Home media===
Million Dollar Arm was released by Walt Disney Studios Home Entertainment on Blu-ray Disc and DVD on October 7, 2014. 

==Reception==

===Box office===
Million Dollar Arm grossed $10.5 million its opening weekend, finishing in fourth place behind fellow new release Godzilla (2014 film)|Godzilla as well as Neighbors (2014 film)|Neighbors and The Amazing Spider-Man 2.  As of September 1, 2014, the film has grossed a total of $36.9 million.

===Critical response===
 
Million Dollar Arm received mixed to positive reviews from film critics. The film   gave the film a score of 56 out of 100, based on 38 critics, indicating "mixed or average reviews". 

Scott Foundas of Variety (magazine)|Variety described the film as a "sharp, slickly produced addition to the Disney sports movie canon   works as both a stirring underdog tale and as a revealing look at the expanding global footprint of the American sports-entertainment machine." Foundas also praised McCarthys screenplay and Gillespies direction, elaborating that they "put across the movies many cliches with a certain verve, and find room for unexpected detours and grace notes in an overall familiar trajectory."  Michael Rechtshaffen of The Hollywood Reporter wrote,  "Dutifully covering all the requisite inspirational sports movie/fish-out-of-water bases yet still managing to throw a few fresh curves into the mix, Disneys Million Dollar Arm assuredly hits a home run."  Chris Nashawaty of Entertainment Weekly gave the film a "B-" grade, comparing it to Jerry Maguire, and calling Hamms performance as one of films highlights. 

Alonso Duralde of The Wrap criticized the films predictability and excessive sentimentality. The film was also criticized for constant camera shakings. 
 Best Original Song (for "Million Dollar Dream", "Spreading the Word/Makhna", and "We Could Be Kings"), but ultimately was not nominated for the award.  
==See also==
 

==References==
 

==External links==
*   at Disney.com
*  
*  
*  
*  
*   at History vs. Hollywood
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 