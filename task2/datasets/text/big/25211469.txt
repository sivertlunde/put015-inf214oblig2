Road to Sangam
 
{{Infobox film
| name = Road to Sangam
| image = Road to Sangam Movie Poster.jpg
| caption = Theatrical poster
| director = Amit Rai
| producer = Amit Chheda
| writer = Amit Rai
| starring = Paresh Rawal Om Puri Pawan Malhotra Javed Shaikh Swati Chitnis
| music = Nitin kumar gupta and Prem Haria
| cinematography = Dharam Gulati
| editing = 
| distributor = Shethia Audio Video Productions
| released=  
| country = India
| language = Hindi

}}
Road to Sangam ( ,  }}) is a 2009 Bollywood film written and directed by Amit Rai. The film features Bollywood actors Paresh Rawal, Om Puri and Pawan Malhotra. 
 Ford V8 engine, not knowing the historic significance – that it once carried the ashes of Mahatma Gandhi which were immersed in the holy Triveni Sangam. 

The film was released worldwide on 29 January 2010.

==Plot== Ford V8 work strike is called by the prominent leaders of his community, played by Om Puri and Pawan Malhotra, to protest against the unjust treatment meted out to the youths arrested by the police. Will he support the protest and abandon the repair of the engine or go against the wishes of his community? Thus begins his journey. A journey of Gandhian values, principles and patriotism.

==Cast==
*Paresh Rawal - Hasmat Ullah
*Om Puri - Mohammad Ali Kasuri
*Pawan Malhotra - Maulana Qureshi
*Javed Shaikh - Dr. Banerjee
*Swati Chitnis - Aara
*Masood Akhtar - Zulfikar
*Yusuf Hussain - Gaffar

Awards and accolades
*9th Annual International Film Festival (South Africa), 2 – 8 November 2009 - (best first film director)
*5th Annual Macon Film Festival (US), 17 – 21 February 2010 - (best film)
*11th Annual MAMI (Mumbai Film Festival) (India), 29 October – 5 November 2009 - (audience choice)
*2nd Annual Radar Hamburg International Independent Film Festival (Germany), 2 – 7 November 2009 - (best film)
*1st annual Los Angeles Reel Film Festival (US), 30 December 2009 (3rd place for best film, best original score and production design)
*The Gollapudi Srinivas National Award for best debutant director (Chennai, India), 2009	
*6th annual White Sands International Film Festival (US), 15 – 18 April 2010 (feature length narrative)
*4th annual Mexico International Film Festival (Mexico), 21 – 23 May 2010 (Silver Palm Award)
*10th annual Nickel Independent Film Festival (Canada), 22 – 26 June (best drama)
*1st annual London Indian Film Festival (UK), 18 July 2010 (audience choice)
*7th edition Bollywood and Beyond Indian film festival (Stuttgart, Germany), 21 – 25 July 2010 (directors vision)
*1st annual Jagran Film Festival (North India), 5 June – 28 August 2010 (Paresh Rawal for best actor, best director and jury award)
*17th annual Star Screen Awards (Mumbai, India), 6 January 2011 (best story)
*17th annual Lions Golden Awards (Mumbai, India), 11 January 2011 (Lions Favourite International Acclaimed Film and Paresh Rawal for best actor)

Official selection and nomination in festivals worldwide:
*Third Eye Asian Film Festival (Mumbai, India)	8–15 October 2009
*International Film Festival Ahmedabad (Gujarat, India)
*11th annual Bare Bones International Film Festival (US) 15 – 25 April 2010
*20th annual Cinequest Film Festival (US) 23 February – 7 March 2010
*Ekwa (Reunion Island) 22 – 28 October 2009
*International Film Festival (Egypt) 4 – 8 April 2010
*International Forum of New Cinema (Kolkata, India) 13 – 19 November 2009
*Kerala Film Festival (Kerala, India)
*Memphis International (US) 22 – 25 April 2010
*Swansea Bay Film Festival (UK) 7 – 16 May 2010
*3rd annual International Festival of Independent Cinema Off Plus Camera (Poland) 16 – 26 April 2010
*15th annual Trivandrum International Film Festival (Kerala, India), 30 July – 5 August 2010
*3rd annual Indie Spirit Film Festival (US) 23 – 25 April 2010
*7th annual Salento International Film Festival (Italy), 3 – 12 September 2010
*Norway Bollywood Film Festival (Oslo, Norway) 10 – 17 September 2010
*Yes India Film Festival (Wellington, New Zealand) 7 – 10 October 2010
*Yes India Film Festival (Auckland, New Zealand) 28 – 31 October
*17th Star Screen Awards (India)
*Stardust Awards (India)
*56th Film Fare (India) 29 January 2010
*Balaji - The Global Indian Film and TV Honours (Mumbai, India) 12 February 2011
*4th annual Hidden Gems Film Festival (Alberta, Canada) 29 January 2012

==Production==
Amit Rai first formed the idea for the film when he read a story about the restoration of the truck that carried Mahatma Gandhis ashes to the Sangam by a Muslim mechanic. After he read about Gandhis ashes being unearthed in an Orissa bank vault, he decided to write a script based on the idea. 
To prepare for his role as an orthodox Maulvi, Pawan Malhotra watched Urdu language channels.  
The film was shot on location in Allahabad. Tushar Gandhi makes a cameo appearance in the film  and many of the secondary characters are non-actor residents of Allahabad.

==Marketing== Indian Post & Telegraphs Department issued special envelopes on the release of the film. They also put up posters and advertisements in all the post offices in the country. 

==Music== Vaisnav Jan", which was a favourite of Gandhi, has been rearranged for this film. The music received the award for Best Foreign Film Original Score at Los Angeles Reel Film Festival.

==Reviews==
The film has received acclamation all over the world. Pratyush Khaitan called it the best Gandhi film of all time.  
Another has termed it one of the six must see forthcoming films for 2010. 

==Awards== 2011 Star Screen Awards - Won  Best Story - Amit Rai

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 