Hanging Garden (film)
 
{{Infobox film |
  name           = Hanging Garden (空中庭園) |
  image          = Hanginggardendvd.jpg|
  writer         = Toshiaki Toyoda (screenplay) Mitsuyo Kakuta (novel)|
  starring       = Anne Suzuki Kyoko Koizumi Masahiro Hirota Itsuji Itao|
  director       = Toshiaki Toyoda|
  producer       = Miyoshi Kikuchi|
  distributor    = Asmik Ace Entertainment |
  released       = 12 May 2005 (Cannes) 8 October 2005 (Japan)|
  runtime        = 114 minutes |
  language       = Japanese |
  music          = Kazuhide Yamaji |}}

  is a 2005 Japanese film directed by Toshiaki Toyoda. The film is a family drama concerning the Kyobashis, whose house rule is to not keep secrets from each other when asked a question directly.

==Cast==
* Kyoko Koizumi — Eriko Kyobashi
* Anne Suzuki — Mana Kyobashi (as An Suzuki)
* Itsuji Itao — Takashi Kyobashi
* Masahiro Hirota — Ko Kyobashi
* Jun Kunimura — Erikos brother
* Eita — Tezuka
* Hiromi Nagasaku — Asako Iizuka
* Michiyo Ohkusu — Satoko Kinosaki

==Awards==
*Kyoko Koizumi - Best Actress, 2006 - Blue Ribbon Awards
*Kyoko Koizumi - Best Actress, 2006 - Nikkan Sports Film Awards

== External links ==
*  

 

 
 
 
 
 
 


 