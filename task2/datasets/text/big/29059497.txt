Nemuritorii
{{Infobox film
| name           = The Immortals
| image          =
| director       = Sergiu Nicolaescu   
| producer       =
| writer         = Sergiu Nicolaescu
| starring       = Ion Besoiu Ilarion Ciobanu Jean Constantin Gheorghe Dinică Sergiu Nicolaescu Costel Băloiu Gina Patrichi Amza Pellea
| original music = Tiberiu Olah
| cinematography = Alexandru David
| runtime        =
| released       =  
| country        = Romania
| language       = Romanian
}}

Nemuritorii ( ) is a 1974 Romanian historical-adventure drama film. This film was directed by Romanian director Sergiu Nicolaescu.    It was released on 22 January 1974 in Hungary.

==Cast==
* Ion Besoiu as Costea
* Ilarion Ciobanu as Vasile
* Sergiu Nicolaescu as Captain Andrei
* Gheorghe Dinică as The Noble Man Butnaru
* Costel Băloiu as Mihăiţă
* Gina Patrichi as Johanna
* Amza Pellea as Dumitru
* Colea Răutu as Iusuf Paşa
* Zephi Alsec as Earl Sarosi
* Dumitru Crăciun as Crăciun
* Alexandru Dobrescu as Mohor
* Mircea Paşcu as Paraschiv
* Vasile Popa as One Nemuritor (Immortal)
* Heidemarie Wenzel as Maria-Christina
* Jean Constantin

==See also==
* List of historical drama films

==References==
 

==External links==
*  

 

 
 
 
 
 