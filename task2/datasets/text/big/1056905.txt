Bombay (film)
 
 
 
{{Infobox film
| name           = Bombay
| image          = Bombayposterfilm.jpg
| image size     =
| caption        =
| director       = Mani Ratnam
| producer       = S. Sriram
| writer         = Mani Ratnam
| narrator       =
| starring       = Arvind Swamy  Manisha Koirala
| music          = A. R. Rahman
| cinematography = Rajiv Menon
| editing        = Suresh Urs
| studio         = Aalayam Productions
| distributor    = Aalayam Productions Ayngaran International
| released       = 10 March 1995
| runtime        = 138 minutes
| country        = India
| location       = Mumbai, India Tamil
| budget         =
| gross          = 
}} Tamil drama demolition on 6 December 1992 and increased religious tensions in the city of Bombay (now Mumbai) that led to the Bombay Riots. It is the second in Ratnams trilogy of films that depict human relationships against a background of Indian politics, including Roja (1992) and Dil Se.. (1998). 
 Chennai film industry, the film was well-received both critically and commercially, and it was screened at many international film festivals including the Philadelphia Film Festival in 1996 where it was an audience favourite. The films soundtrack sold 15 million units, becoming one of the best-selling film soundtracks of all time, and earning composer A. R. Rahman his fourth consecutive Filmfare Best Music Director Award (Tamil). However, the film caused considerable controversy upon release in India and abroad for its depiction of inter-religious relations and religious riots. The film was banned in Singapore and Malaysia upon release.
 BFI Modern Classics, looking at the films production, the several issues it covered, and its impact upon release in India and abroad.       The film was ranked among the top 20 Indian films in the British Film Institutes rankings.  The film was also dubbed in Hindi and Telugu language|Telugu.

==Plot==
  orthodox Hindu Narayana Pillai (Nassar) living in a coastal village in Tamil Nadu. A journalism student studying in Bombay, Shekhar visits home to be with his family. On one of his return trips, he notices Shaila Banu (Manisha Koirala), a Muslim schoolgirl in the village and loses his heart to her. Initially shy, Shaila seeks to distance herself from Shekhar, but after frequent run-ins, and days of pursuit, Shaila begins to like Shekhar. Eventually, they both fall in love.

Shekhar meets Shailas father Basheer Ahmed (Kitty (actor)|Kitty) and reveals he wants to marry her. Basheer throws him out, citing difference in religions. Shekhar reveals his interest to his father Pillai, who becomes angry, meets Basheer and gets into an abusive argument with him. Upset with rejection from both families, Shekhar leaves the village and returns to Bombay. Through Shailas friend, he sends her a letter and a ticket for her to travel to Bombay. However, she is undecided; Basheer comes to know of her regular letters from Shekhar and plans to get her married immediately to stop this relation growing further. Left with no choice, Shaila leaves the village with the ticket sent by Shekhar and reaches Bombay. They get married and lead a happy life. Shaila conceives and delivers twins who are named Kabir Narayan and Kamal Basheer. The twins are raised in both religions. Shekhar continues to work as a journalist, while Shaila takes care of home and children. After six years, Shekhar and Shaila settle down in their life and begin the process of re-establishing relationship with their respective families.
 demolished by riots break out in Bombay. Kabir and Kamal, who have gone to buy groceries, get caught in the riots; eventually, Shekhar and Shaila save them and reach home safely. Narayana Pillai, who receives the news of the riots, rushes to Bombay to meet his son and his family. Everyone is happy with his arrival, and he stays with them. Soon, Basheer also comes with his wife and all of them live together happily for a few days. Both Pillai and Basheer are happy with their grandchildren and wish to be with them.

On 5 January 1993, when two murders are interpreted as communal killings, another riot breaks up in Bombay, raising tensions between Hindu and Muslims and they clash in the streets. Hundreds of poor people belonging to both the religions die. The mansion where Shekhar stays with his family also gets burnt. When Shekhar evacuates everyone, Narayana Pillai, Basheer and his wife get caught in the fire accident and die. The children who run to save themselves get separated from their parents. Shekhar and Shaila begin to search for them and they go through several tense moments. Shekhar participates in the movement to stop the riots with other religious leaders (who realise the futility of the riots by then) and succeeds. When the riots stop, the children who were saved by people from different religions, also turn up and join their parents.

==Cast==
* Arvind Swamy as Shekhar Narayanan Misra
* Manisha Koirala as Shaila Banu
* Nassar as Narayana Pillai Kitty as Basheer
* M.V. Vasudeva Rao
* Prakash Raj Rallapalli as eunuch
* Sonali Bendre in the Item number "Hamma Hamma"
* Nagendra Prasad in the Item number "Hamma Hamma"

==Production== Vikram and Manisha Koirala, but eventually did not choose Vikram as he was unwilling to remove his beard and moustache that he had grown for the production of another film during the period, Vikramans Pudhiya Mannargal (1994). 

When Ratnam approached cinematographer Rajiv Menon to shoot Bombay, he described it as a film about the riots and said that he (Menon) needed to "make the riots as beautiful as possible". So, Menon suggested shooting in the rains to achieve the effect. They shot the interiors of homes in Pollachi in Tamil Nadu and the exteriors were shot in Kasargod in Kerala. Several scenes of the city of Mumbai during riots were recreated with the help of photographs. Menon also explained in his interview that "The camera moves a lot-there would be long takes followed by three-four small cuts. It made lighting continuity easier for me and I was able to move fluidly." He said that Mani and him, both have a fascination for how Guru Dutt shot his song sequences. They were also inspired by Satyajit Rays style.   

==Reception==

===Box office===
Bombay was a huge blockbuster and is regarded as one of the most acclaimed Tamil films of the 90s. The Hindi version of the film earned  , as reported by Box Office India which was phenomenal for a dubbed film.   

===Critical reception===
The Times of India rated it 3.5 out of 5, saying "Bombay might not be a masterpiece, but is certainly a bold attempt".  In 1996, James Berardinelli rated the film 3.5 out of 4 and said, "Largely because of their limited North American appeal and dubious quality, Indian movies are routinely ignored by distributors   Occasionally, however, a worthwhile picture causes enough people to take notice that it becomes a favorite on the international film festival circuit. One such movie is Bombay, the fourteenth feature from celebrated director Mani Rathnam." He concluded, "Director Rathnam has shown great courage in making this picture (bombs were thrown at his house after it opened in India), which speaks with a voice that many will not wish to hear. Bombay recalls how forceful a motion picture can be." 

==Awards==
The film has won the following awards since its release:

===National=== National Film Awards
* Nargis Dutt Award for Best Feature Film on National Integration – Mani Ratnam Best Editing – Suresh Urs

;1996 Filmfare Awards Best Film (Critics) – S. Sriram Best Performer (Critics) – Manisha Koirala

;1996 Filmfare Awards South Best Film – Tamil – S. Sriram Best Director – Tamil – Mani Ratnam Best Actress – Tamil – Manisha Koirala Best Music Director – Tamil – A. R. Rahman

;1996 Matri Shree Media Award Best Film – Mani Ratnam 1996 : 20th Matrishree Awards
   

;1995 Tamil Nadu State Film Awards Best Music Director – A. R. Rahman Best Lyricist – Vairamuthu Best Female Playback – K. S. Chitra

===International===

;1995 Edinburgh International Film Festival (Scotland)
* Gala Award – Bombay – Mani Ratnam

;1996 Political Film Society Awards (United States) 
* Special Award – Bombay – Mani Ratnam

;2003 Jerusalem Film Festival (Israel)
* Wim Van Leer in Spirit for Freedom Award – Best Feature – Bombay – Mani Ratnam

==Soundtrack==
 

==Further reading==
*  

==References==
 

==External links==
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 