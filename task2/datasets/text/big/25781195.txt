The Last Ottoman
{{Infobox film
| name           = The Last Ottoman
| image          = Son osmanli yandim ali.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Mustafa Şevki Doğan
| producer       = Mehmet Soyarslan
| writer         = Suat Yalaz Mehmet Soyarslan Baykut Badem
| narrator       =
| starring       = Kenan İmirzalıoğlu Cansu Dere Engin Şenkan
| cinematography = Zekeriya Kurtuluş
| editing        = 
| distributor    = Özen Film
| released       =  
| runtime        = 121 mins
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    =
}}
 Mustafa Kemal Turkish film of 2007.   

==Production==
Yandım Ali is a great hero and heroism is something which always appeals to us. Maybe we follow this path because we know the make-up of our people and know there is a majority that likes nationalist films, director Mustafa Şevki Doğan told Reuters.      

The film was shot on location in Istanbul and Bahçesehir, Turkey.   

==Plot== Mustafa Kemal, who has just set on a journey to save the Turkish land from the enemy. Ali recognizes that his real love is his country, and joins in the endeavors to save his country.

==Main cast==
*Kenan İmirzalıoğlu as Tahtacızade "Yandım" Ali
*Cansu Dere as Defne
*Engin Şenkan as Çukurçeşmeli Osman Bey
*Anna Babkova as Nadia Mustafa Kemal
*Baykut Badem as Kelle
*Öner Erkan as Gerard (Jerar)
*John Baker as Dimitri
*Emin Boztepe as Monsieur Gureau
*Kim Feenstra as Collette (French teacher)

==Release==

===Festival Screenings===
* 31st Cairo International Film Festival (Nov. 27-Dec. 7, 2007)   

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 
 