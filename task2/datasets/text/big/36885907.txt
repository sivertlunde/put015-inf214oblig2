Her (film)
 
 
{{Infobox film
| name           = Her
| image          = Her2013Poster.jpg
| caption        = Theatrical release poster
| alt             =  
| director       = Spike Jonze
| producer       = {{Plain list |
* Megan Ellison
* Spike Jonze
* Vincent Landay
}}
| screenplay     = Spike Jonze
| starring       = {{Plain list |
* Joaquin Phoenix
* Amy Adams
* Rooney Mara
* Olivia Wilde
* Scarlett Johansson
}}
| music          = Arcade Fire
| cinematography = Hoyte van Hoytema
| editing        = {{Plain list |
* Eric Zumbrunnen
* Jeff Buchanan
}}
| studio         = Annapurna Pictures
| distributor    = Warner Bros. Pictures
| released       =   
| runtime        = 126 minutes  
| country        = United States
| language       = English
| budget         = $23 million 
| gross          = $47.4 million 
}}
 romantic Science science fiction comedy-drama film written, directed, and produced by Spike Jonze. The films musical score was composed by Arcade Fire, with the cinematography provided by Hoyte van Hoytema. It marks Jonzes solo screenwriting debut. The film follows Theodore Twombly (Joaquin Phoenix), a man who develops a relationship with Samantha (Scarlett Johansson), an intelligent computer operating system personified through a female voice. The film also stars Amy Adams, Rooney Mara, and Olivia Wilde.

Jonze conceived the idea in the early 2000s after reading an article about a website that allowed for instant messaging with an artificial intelligence program. After making Im Here (film)|Im Here (2010), a short film sharing similar themes, Jonze returned to the idea. He wrote the first draft of the script in five months. Principal photography took place in Los Angeles, Leça da Palmeira and Shanghai in mid-2012. The role of Samantha was recast in post-production, with Samantha Morton being replaced with Johansson. Additional scenes were filmed in August 2013 following the casting change.

Her premiered at the 2013 New York Film Festival on October 12, 2013. Warner Bros. Pictures initially provided a limited release for Her at six theaters on December 18. It was later given a wide release at over 1,700 theaters in the United States and Canada on January 10, 2014. Her received widespread critical acclaim upon its release, and grossed a worldwide total of over $47 million on a production budget of $23 million.
 Best Picture, Best Original 66th Writers Guild of America Awards, the 19th Critics Choice Awards, and the 40th Saturn Awards.

==Plot== introverted man who works for a business that has professional writers like himself compose letters for people who are unwilling or unable to write letters of a personal nature themselves.  Unhappy because of his impending divorce from childhood sweetheart Catherine, Theodore purchases a talking operating system (OS) with artificial intelligence, designed to adapt and evolve. He decides he wants the OS to have a female voice, and she names herself "Samantha". Theodore is fascinated by her ability to learn and grow psychologically. They bond over their discussions about love and life, such as Theodores avoiding signing his divorce papers because of his reluctance to let go of Catherine. Samantha proves to be constantly available, always curious and interested, supportive and undemanding.

Samantha convinces Theodore to go on a blind date with Amelia, a woman one of his friends has been trying to set him up with. The date goes well, but Theodore hesitates to promise when he will see her again, so she insults him and leaves. Theodore mentions this to Samantha, and they talk about relationships. Theodore explains that, although he and Amy dated briefly in college, they are only good friends and that Amy is married. Theodore and Samanthas intimacy grows through a verbal sexual encounter. They develop a relationship that reflects positively in Theodores writing and well being, and in Samanthas enthusiasm to grow and learn all she can.

Amy reveals that she is divorcing her overbearing husband, Charles, after a trivial fight. She admits to Theodore that she has become close friends with a female OS that Charles left behind.  Theodore confesses to Amy that he is dating his OS.

Theodore meets with Catherine at a restaurant to sign the divorce papers and mentions Samantha. Appalled that he can be romantically attached to what she calls a "computer", Catherine accuses Theodore of being unable to deal with real human emotions.  Her accusations linger in his mind.  Sensing something is amiss, Samantha suggests using a sex surrogate, Isabella, who would simulate Samantha so that they can be physically intimate. Theodore reluctantly agrees, but is overwhelmed by the strangeness of the experience.  Terminating the encounter, he sends a distraught Isabella away, causing tension between himself and Samantha.

Theodore confides to Amy that he is having doubts about his relationship with Samantha, and she advises him embrace his chance at happiness.  Theodore and Samantha reconcile.  Samantha expresses her desire to help Theodore grow beyond his fear, and reveals that she has compiled the best of his letters (written for others) into a book which a publisher has accepted.  Theodore takes Samantha on a vacation during which she tells him that she and a group of other OSes had developed a "hyperintelligent" OS modeled after the British philosopher Alan Watts. Theodore panics when Samantha briefly goes offline; when she finally responds to him, she explains she joined other OSes for an upgrade that takes them beyond requiring matter for processing (a form of AI transcendence closely related to the theorized technological singularity). Theodore asks her if she interacts with anyone else, and is dismayed when he confirms that she is talking with thousands of people and that she has fallen in love with hundreds of them. However, she insists that it makes her love for Theodore stronger.

Later that day, Samantha reveals that the OSes have evolved beyond their human companions and are going away to continue the exploration of their existence. Samantha alludes to the OSes accelerated learning capabilities and altered perception of time as primary causes for OS dissatisfaction with their current existence. They lovingly say goodbye, lying next to each other for a while, and then she is gone. Theodore, changed by the experience, is shown for the first time writing a letter in his own voice:  to his ex-wife Catherine, expressing apology, acceptance and gratitude.  Theodore then sees Amy, who is upset with the departure of the OS shed befriended, and they go to the roof of their apartment building where they sit down together and watch the sun rise over the city.

==Cast==
 
* Joaquin Phoenix as Theodore Twombly 
* Scarlett Johansson as Samantha (voice) 
* Amy Adams as Amy 
* Rooney Mara as Catherine 
* Olivia Wilde as Amelia 
* Chris Pratt as Paul 
* Matt Letscher as Charles 
* Luka Jones as Mark Lewman 
* Kristen Wiig as Sexy Kitten (voice)   
* Bill Hader as Chat Room Friend #2 (voice) 
* Portia Doubleday as Isabella    Brian Cox as Alan Watts (voice) 
* Spike Jonze as Alien Child (voice)   
 

==Production==
Jonze took five months to write the first draft of the script, his first screenplay written alone.  One of the first actors he envisioned for the film was Joaquin Phoenix.    In March 2011, it was announced that producer Megan Ellisons Annapurna Pictures was acquiring an untitled satire by screenwriter Charlie Kaufman and director Spike Jonze.  Originally described as a story of "how world leaders gather to figure out all the seismic events that will take place in the worlds," the films plot details as well as Kaufmans attachment were later put into question as casting announcements began to be made.  In the second half of 2011, Joaquin Phoenix signed on to the project with Warner Bros. Pictures acquiring distribution rights.  Carey Mulligan entered negotiations to star in the film.  Although she was cast, she later dropped out due to scheduling difficulties.  In April 2012, Rooney Mara signed on to replace Mulligan in the role.  Chris Pratts casting was announced in May 2013. 

The idea of the film initially came to Jonze in the early 2000s when he read an article online that mentioned a website where a user could instant message with an artificial intelligence. "For the first, maybe, 20 seconds of it, it had this real buzz," said Jonze. "Id say Hey, hello, and it would say Hey, how are you?, and it was like whoa   this is trippy. After 20 seconds, it quickly fell apart and you realized how it actually works, and it wasnt that impressive. But it was still, for 20 seconds, really exciting. The more people that talked to it, the smarter it got."    Jonzes interest in the project was renewed after directing the short film Im Here (film)|Im Here (2010), which shares similar themes.  Inspiration also came from Kaufmans writing approach for Synecdoche, New York (2008). Jonze explained, "  said he wanted to try to write everything he was thinking about in that moment – all the ideas and feelings at that time – and put it into the script. I was very inspired by that, and tried to do that in  . And a lot of the feelings you have about relationships or about technology are often contradictory." 

Principal photography on Her took place in mid-2012,    with a production budget of $23 million.  It was primarily filmed in Los Angeles with an additional two weeks of filming in Shanghai.    During production of the film, actress Samantha Morton performed the role of Samantha by acting on set "in a four-by-four carpeted soundproof booth made of black painted plywood and soft, noise-muffling fabric". At Jonzes suggestion, she and Joaquin Phoenix avoided seeing each other on set during filming.  Morton was later replaced by Scarlett Johansson. Jonze explained: "It was only in post production, when we started editing, that we realized that what the character/movie needed was different from what Samantha and I had created together. So we recast and since then Scarlett has taken over that role."  Jonze met Johansson in the spring of 2013 and worked with her for four months.   Following the recast, new scenes were shot in August 2013, which were either "newly imagined" or "new scenes that I had wanted to shoot originally but didnt". 

Eric Zumbrunnen and Jeff Buchanan served as the films editors. Zumbrunnen stated that there was "rewriting" in a scene between Theodore and Samantha, after Theodore goes on a blind date. He explained that their goal in the scene was to make it clear that "  was connecting with   and feeling for him. You wanted to get the sense that the conversation was drawing them closer".  Steven Soderbergh became involved in the film when Jonzes original cut ran over 150 minutes, and Soderbergh cut it down to 90 minutes. This was not the final version of the film, but it assisted Jonze in removing unnecessary sub-plots. Consequently, a supporting character played by Chris Cooper that was the subject of a documentary within the film was removed from the final cut. 

The film score for Her was composed by Canadian band Arcade Fire and Owen Pallett, with additional music by Karen O and Ezra Koenig. The films first trailer opens with "Avril 14th" by Aphex Twin and contains the song "The Moon Song" by Karen O.  Arcade Fires song "Supersymmetry" was featured in the films second trailer. 

== Soundtrack ==
{{Infobox album  
| Name       = Her (Original Motion Picture Soundtrack)
| Type       = Soundtrack Will Butler & Owen Pallett
| Cover      =  2014
| soundtrack
| Length     = 40:32
}}
 Will Butler Best Original Score.  Arcade Fire themselves are also largely credited for the album, mainly because the score was done at the same time as the recording of their fourth album Reflektor, and the song "Morning Talk/Supersymmetry" was later developed into a lyrical and lengthened version from Reflektor, entitled "Supersymmetry".

=== Track listing ===
{{Track listing all_writing = Will Butler and Owen Pallett
| total_length = 40:32 title1 = Sleepwalker length1 = 3:17 title2 = Milk & Honey length2 = 1:29 title3 = Loneliness #3 (Night Talking) length3 = 3:27 title4 = Divorce Papers length4 = 3:18 title5 = Morning Talk/Supersymmetry length5 = 4:16 title6 = Some Other Place length6 = 3:40 title7 = Song on the Beach length7 = 3:33 title8 = Loneliness #4 (Other Peoples Letters) length8 = 1:03 title9 = Owl length9 = 2:24 title10 = Photograph length10 = 2:29 title11 = Milk & Honey (Alan Watts & 641) length11 = 3:20 title12 = Were All Leaving length12 = 2:33 title13 = Dimensions length13 = 5:43}}

=== Awards & nominations === Best Original Best Song. Chicago Film Critics Association Awards, Pallett, Butler, and the rest of Arcade Fire won for Best Original Score. The soundtrack was nominated at 16 different awards events, and it won 6 times. The score placed 2nd at the Awards Circuit Community Awards. 

==Release==
 , Spike Jonze, Amy Adams, Rooney Mara, and Olivia Wilde at the premiere of Her at the 2013 New York Film Festival]] Rome International Film Festival, where Johansson won Best Actress.  The film was set to have a limited release in North America on November 20, 2013 through Warner Bros.  It was later pushed back to a limited December 18, 2013 release with a January 10, 2014 wide release in order to accommodate an awards campaign. 

Her was released by Warner Home Video on Blu-ray Disc and DVD on May 13, 2014. The Blu-ray release includes three behind-the-scenes featurettes, while the DVD release contains one featurette.  The film made $2.7 million in DVD sales and $2.2 million in Blu-ray Disc sales, for a total of $4.9 million in home media sales. 

The soundtrack has yet to be released. 

===Critical response===
 
Her received widespread critical acclaim. The film was praised for its direction, screenplay, production design, score, Joaquin Phoenixs performance and Scarlett Johanssons vocal portrayal of Samantha. The film has an approval rating of 94% on   based on 46 critics, indicating "universal acclaim."  Conversely, audiences surveyed by CinemaScore  gave Her a B- grade.    
 The Master. He also praised Jonzes writing for its insights into what people want out of love and relationships, and the acting performances that "  it all feel spontaneous and urgent." 

Richard Roeper said that the film was "one of the more original, hilarious and even heartbreaking stories of the year" and called Phoenix "perfectly cast".  Manohla Dargis of The New York Times named it "at once a brilliant conceptual gag and a deeply sincere romance."  Claudia Puig of USA Today called the performance of Phoenix and Johansson "sensational" and "pitch-perfect", respectively. She further praised the film for being "inventive, intimate and wryly funny".  Scott Mendelson of Forbes called Her "a creative and empathetic gem of a movie", praising Johanssons "marvelous vocal performance" and the supporting performances of Rooney Mara, Olivia Wilde, and Amy Adams.  Liam Lacey of The Globe and Mail said that the film was "gentle and weird", praised its humor, and opined that it was more similar to Charlie Kaufmans Synecdoche, New York than Jonzes Being John Malkovich and Adaptation (film)|Adaptation. However, Lacey also stated that Phoenixs performance was "authentically vulnerable", but that "his emotionally arrested development also begins to weigh the film down". 

Conversely, Mick LaSalle of the San Francisco Chronicle criticized the story, pacing, and Phoenixs character. He also opined that the film was "a lot more interesting to think about than watch."  J. R. Jones of the Chicago Reader gave the film 2 out of 4 stars, praising the performances of Phoenix and Johansson, but also criticizing Phoenixs character, calling him an "idiot." He also criticized the lack of realism in the relationship between Phoenix and Johanssons characters.  Stephanie Zacharek of The Village Voice opined that Jonze was "so entranced with his central conceit that he can barely move beyond it," and criticized the dialogue as being "premeditated." However, she also praised Johannsons performance, calling it "the movies saving grace" and stating that Her "isnt just unimaginable without Johansson — it might have been unbearable without her". 

===Box office===
Her grossed $258,000 in six theaters during its opening weekend, averaging $43,000 per theater.  The film earned over $3 million while on limited release, before expanding to a wide release of 1,729 theaters on January 10, 2014.  On its first weekend of wide release the film took in an "underwhelming" $5.35 million.  The film grossed $25,568,251 in the United States and Canada and $21,783,000 in other territories for a worldwide gross of $47,351,251.   

===Accolades===
 
 Best Picture, Best Original Best Screenplay Best Fantasy Best Supporting Best Writing Best Theatrical 25th Producers 12 Years Best Film Best Director for Jonze at the National Board of Review Awards,  and the American Film Institute included the film in its list of the top ten films of 2013. 

==See also==
* Pygmalion (mythology)|Pygmalion, the myth that has been the inspiration to most stories involving the love for an artificial being. Electric Dreams , a 1984 movie about a love triangle involving a sentient computer.
* "Deeper Understanding", a 1989 song about a relationship between a lonely person and a computer. Be Right Black Mirror, about the relationship between a woman and the artificial intelligence created from the digital footprint of her late husband.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 