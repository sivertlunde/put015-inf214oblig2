Peace Officer (film)
{{Infobox film
| name           = Peace Officer
| image          = Peace Officer (film) POSTER.jpg
| caption        = 
| director       = Scott Christopherson and Brad Barber
| producer       = Scott Christopherson, Brad Barber, Dave Lawrence
| writer         = 
| narrator       =
| starring       = William J. "Dub" Lawrence, Liz Wood, Jerry Wood, Nancy Lawrence, Radley Balko, Kara Dansky, Sheriff Jim Winder, Sheriff Todd Richardson, Det. Jason Vanderwarf, Officer Derek Draper, Mike Stewart, Chris Shaw
| music          = Micah Dahl Anderson
| cinematography = Barber, Christopherson
| editing        = Renny McCauley
| distributor    =
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}} police militarization in the United States. It won the 2015 Documentary Feature Competition Grand Jury award at the South by Southwest Film Festival.   

==References==
 

==External links==
Official movie web site with film trailer: http://www.peaceofficerfilm.com/

 
 
 