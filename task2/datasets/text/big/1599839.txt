In Good Company (2004 film)
{{Infobox film
| name           = In Good Company
| image          = In Good Company movie.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster Paul Weitz
| producer       = {{Plainlist|
*Lawrence Pressman
*Paul Weitz
}}
| writer         = Paul Weitz
| starring       = {{Plainlist|
*Dennis Quaid
*Topher Grace
*Scarlett Johansson
}}
| music          = {{Plainlist|
*Damien Rice
*Stephen Trask
}}
| cinematography = Remi Adefarasin
| editing        = Myron I. Kerstein
| studio         = {{Plainlist| Universal Pictures
*Depth of Field
}}
| distributor    = Universal Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $61,315,215 
}} Paul Weitz, and starring Dennis Quaid, Topher Grace, and Scarlett Johansson.    The film is about a middle-aged advertising executive whose company is bought out by a large international corporation leaving him with a new boss who is nearly half his age. His life is further complicated when his boss takes a romantic interest in his daughter.

==Plot==
Dan Foreman (Dennis Quaid) is a 51-year-old advertising executive and head of sales for Sports America, a major sports magazine. Happily married with two daughters, Dan faces a life-changing event when his magazine is bought out by Globecom, an international corporation that promotes the corporate concept of "synergy". After he is forced to fire several of his longtime colleagues, Dan is demoted and becomes the "wingman" of his new boss, Carter Duryea (Topher Grace), a 26-year-old business school prodigy. While Dan develops clients through handshake deals and relationships, Carter champions the corporate creed of synergy, cross-promoting the magazine with the cell phone division and "Krispity Krunch", a snack food also owned by Globecom.

Dan and Carter are both facing challenges in their personal lives. Dan is supporting two daughters—16-year old Jana (Zena Grey) and 18-year-old Alex (Scarlett Johansson) who is preparing to enter college—and learns that his wife is pregnant with their third child. Meanwhile, Carter is dumped by his wife of seven months, and focuses all his energy on work. With Dan facing the financial realities of taking out a second mortgage, to cover his daughters college education costs, and a new child, and with Carter needing Dans practical, real-life experience in the field of advertising, the two form an uneasy friendship.

One day, Carter, who has been struggling with loneliness following the breakup of his marriage, invites himself to dinner at Dans house, where he meets Dans daughter Alex, and the two quickly form an attraction. Their initial friendship allows Carter to forget his loneliness, and Alex, who is now attending New York University, is able to escape her own loneliness and boredom. In the coming days, Carter and Alex spend time together and become romantically involved. Fearful of offending her father, they keep their relationship a secret for the time being.

Their friendship, however, takes a turn for the worse when Dan discovers that Carter and Alex have been seeing each other, approaches them in a restaurant, and punches his boss in the face. The confrontation with her father convinces Alex to break off the relationship with Carter who is heartbroken. Soon after, Globecom CEO Teddy K visits the sales office and during a grand speech to all the employees on synergy and other similar corporate business strategies, he is questioned by Dan and shrugs him off. However, Carters boss, Mark Steckle tells Carter to fire Dan, and he refuses and the pair buy time by telling Steckle that he will lose a major advertising contract.  He gives them 24 hours to come up with the contract or be fired. When faced with a potential deal with a longtime client who Dan knows and cares for, Carter gives way to Dans personal approach (after Dans insistence), which lends to the client doing business with the company.

Following another corporate shakeup, Sports America is sold off, Carter is let go, and Dan returns to his former position as head of sales. Having developed fatherly feelings toward Carter, he offers him a position in his new department as his "wingman", but Carter refuses, admitting he needs to take some time off and examine what he really wants to do in his life, and the two part ways. Dan comes to have a new respect for Carter and begins to think of him as something akin to an adopted son and part of the family. On his way down the elevator, Carter runs into Alex and they catch up and then soon part company. The next week, Dan is shown telling his daughters that they have a little sister and he soon calls Carter, who is jogging on the beach in Los Angeles, to tell him about the baby.

==Cast==
* Dennis Quaid as Dan Foreman
* Topher Grace as Carter Duryea
* Scarlett Johansson as Alex Foreman
* Marg Helgenberger as Ann Foreman
* Clark Gregg as Mark Steckle
* David Paymer as Morty Wexler
* Selma Blair as Kimberly
* Ty Burrell as Enrique Colon
* Frankie Faison as Corwin
* Philip Baker Hall as Eugene Kalb
* Amy Aquino as Alicia
* Lauren Tom as Obstetrician
* Colleen Camp as Receptionist
* Zena Grey as Jana Foreman
* John Cho as Petey
* Malcolm McDowell (uncredited) as Teddy K, Globecom CEO

==Reception==
In Good Company received mostly positive reviews and has a rating of 83% on Rotten Tomatoes based on 167 reviews with an average score of 7 out of 10. The consensus states "The witty and charming In Good Company offers laughs at the expense of corporate culture."    The film also has a score of 66 out of 100 on Metacritic based on 40 reviews.   

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 