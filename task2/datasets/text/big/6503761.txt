Boyfriends (film)
{{Infobox Film
| name           = Boyfriends
| caption        =
| image	=	Boyfriends FilmPoster.jpeg
| director       = Tom Hunsinger, Neil Hunter
| producer       =
| writer         = Tom Hunsinger, Neil Hunter
| narrator       =
| starring       = James Dreyfus
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =
| runtime        =
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British independent gay couples, The Thin Blue Line. The film also starred Mark Sands, Michael Urwin, Andrew Ableson, David Coffey, Darren Petrucci and Michael McGrath.

==Plot==

Paul, Matt and Will, three best friends decide to go on holiday together. Paul (James Dreyfus) brings his lover Ben, but their five-year relationship is unstable owing to Pauls continued moodiness over the death of his brother Mark; Matt brings Owen, with whom he wants a lifelong relationship but whose boisterous personality doesnt suit him; and Will brings Adam, a 20-year old one-night stand. Marks lover also comes along for some sense of closure.

==Cast==
Only the seven main characters and Mark appear throughout the entire film.

* James Dreyfus - Paul
* Mark Sands - Ben
* Michael Urwin - Matt
* Andrew Ableson - Owen
* David Coffey - Will
* Darren Petrucci - Adam
* Michael McGrath - James
* Russell Higgs - Mark

==Soundtrack==
The films closing music is Dinah Washingtons I wish I knew the name (of the boy in my dreams) .

==Critical reception==
Boyfriends won the 1996 Best Featured Film Award at the Torino International Gay & Lesbian Film Festival. General critical reviews were mixed, with one describing the film as "a biting, shrewd and scathingly funny dissection of gay relationships".  Another critic, however, wrote "Boyfriends suffers from too many soap-opera-like subplots that seem set up to create tension". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 