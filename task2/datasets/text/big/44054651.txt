Prarthana (1943 film)
{{Infobox film
| name           = Prarthana
| image          = 
| image_size     = 
| caption        = 
| director       = Sarvottam Badami
| producer       = Sohrab Modi
| writer         =  
| narrator       =  Motilal Jahanara Kajjan Sabita Devi K. N. Singh Saraswati Devi 
| cinematography = 
| editing        = 
| distributor    =
| studio         = Minerva Movietone
| released       = 1943
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1943 Hindi Saraswati Devi and the lyricist was Safdar Aah Sitapuri.  Having worked for Madan Theatres Ltd and Sagar Movietone in the 1930s, the famous singer and actress Jahanara Kajjan returned to work after a hiatus of four years working for studios like Minerva Movietone.   Prarthana is cited as "probably" her last film before her death in 1944.  The film starred Motilal (actor)|Motilal, Jahanara Kajjan, Sabita Devi, Sajjan, Nimabalkar, K. N. Singh, Sadat Ali, Mehboob and Abu Bakar. 

==Cast== Motilal
* Jahanara Kajjan
* Sabita Devi
* K. N. Singh Sajjan
* Nimbalkar
* Sajjan
* Sadat Ali
* Mehboob 
* Ghulam Hussain
* Abu Bakar
* Nimbalkar
* Menaka.
* Shorrey.


   Menaka & Shorrey added by Neeta Pate ,with the help of old book

==Music== Saraswati Devi with lyrics by Safdar Aah. The singers were Jahanara Kajjan, Vasant, Rehmat Bai and Moolchand.  The song "Aaya Sawan Aaja" sung by Kajjan with relative ease, was in the raga Brindabani Sarang.    Other popular songs of Kajjan from this film are, "Kahe Neha Lagaye Sajaniya" and "Ek Dhundhla Sa Mohabbat Ka Hai Nasha Baaki".   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-
| 1
| Aaya Sawan Aa Jaa Saajan -----Raga Bindravani Sarang. Tal Dadra.

     
| Jahanara Kajjan
|-
| 2
| Ek Dhundhla Sa Mohabbat Ka Hai Nasha Baaki  ----Raga Jaunpuri.
| Jahanara Kajjan
|-
| 3
| Kahe Neha Lagaye Sajaniya -----Raga Pilloo.
| Jahanara Kajjan
|-
| 4
| Mohabbat Ka Rasta Dikhaya Na Hota ----Raga Bhim Palasi.
| Jahanara Kajjan
|-
| 5
|  Tumhare Darshan Ka Naina Taras Gaye Ho ----Raga Gond Tal Kaharawa.
| Jahanara Kajjan
|-
| 6
| Gajare Wali Najariya Milaye Ja ----Raga Des Tal Kaharawa.
| Rehmat Bai,Moolchand
|-
| 7
| Khel Ye Sansar Hai Jaan Ki Hain Baaziyan----Raga Kafi. Tal Dadra.
| Vasant
|-
| 8
| Hari Aate Hai ----Raga Aman Kalyan. Tal Kaharawa.
| Vasant
|-
| 9
| Chhip Na Saka Ab Chhipane Wala -----Raga Mond. Tal Kaharawa.
|
|-
| 10
| Do Jeevan Mein Gaanth Lagi----Raga Bihag. Tal Dadra.
|
|}  Compiled by Neeta Pate.   From old book.

==References==
 

==External links==
* 

 

 
 
 

 