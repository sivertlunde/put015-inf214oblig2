Draupadi Vastrapaharanam (1934 film)
{{Infobox film
| name           = Draupadi Vastrapaharanam
| image          = Draupadi Vastrapaharanam.jpg
| image size     = 200px
| caption        = A scene from Draupadi Vastrapaharanam
| director       = R. Padmanaban
| producer       = 
| writer         = 
| narrator       =
| starring       =   
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1934
| runtime        = 
| country        = India
| language       = Tamil
| budget         =
| preceded by    =
| followed by    =
}}
Draupadi Vastrapaharanam was a 1934 Tamil-language film starring T. P. Rajalakshmi, V. A. Chellappa and Serukulathur Sama. The movie was directed by R. Padmanaban.

== Plot ==

The film is based on the episode of the dice game and the disrobing of Draupadi in the Indian epic Mahabharatha.

== Cast ==

* T. P. Rajalakshmi as Draupadi
* V. A. Chellappa as Duryodhana
* M. D. Parthasarathy as Yudhishtra
* Serukulathur Sama as Krishna

==References==
*  

==External links==
*  

 
 
 
 
 


 