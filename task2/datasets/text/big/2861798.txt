Vital (film)
{{Infobox film
| name = Vital
| image = Vital_DVD_cover.jpg
| caption = Vital DVD cover
| director = Shinya Tsukamoto
| producer = Shinya Tsukamoto
| writer = Shinya Tsukamoto
| starring = Tadanobu Asano Nami Tsukamoto
| music = Chu Ishikawa
| cinematography =
| editing =
| distributor = Gold View Company Ltd.
| released = 2004 (Japan)
| runtime = 86 min.
| language = Japanese
| budget =
}}
Vital is a Japanese film made in 2004. It was directed by Shinya Tsukamoto and stars Tadanobu Asano as Hiroshi Takagi, a man whose girlfriend dies and who loses his memory in a car accident.

The original concept that inspired Vital was the image of medical students making sketches during a dissection. Tsukamoto visited a medical school and observed a dissection while writing the screenplay, which was originally titled: Dissection Film Project. Leonardo da Vincis anatomical sketches were a direct inspiration. 

==Plot==

Hiroshi (Tadanobu Asano) wakes up in a hospital room and realizes that he was in a serious car accident that caused the death of his girlfriend, Ryoko (Nami Tsukamoto), as well as the loss of his memory. While trying to regain his memory, one of the first clues that Hiroshi finds are his old medical textbooks that he studied prior to his accident. This gives him a renewed purpose in life and he delves forward into his medical school studies. One of his medical school classmates, Ikumi (Kiki), soon becomes infatuated with Hiroshi, although he does not return her interests initially. During a 4-month period, in which his class dissects human cadavers, Hiroshi realizes that the body that he is dissecting is the body of his former girlfriend, Ryoko, which causes more of Hiroshi’s memory to return. During this time, Hiroshi engages in a relationship with his classmate Ikumi, that helps him recall further memories of his ex-girlfriend. Ikumi, meanwhile, feels jealous rage because of Hiroshi’s devotion in dissecting the cadaver of Ryoko, while Hiroshi is consumed in the quest to understand who that person was that died in his car and ultimately find out who he really is.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Tadanobu Asano || Hiroshi Takagi
|-
| Nami Tsukamoto || Ryoko Ooyama
|-
| Kiki || Ikumi
|-
| Kazuyoshi Kushida || Hiroshis father
|- Lily || Hiroshis mother
|-
| Jun Kunimura || Ryokos father 
|-
| Hana Kino || Ryokos mother
|-
| Ittoku Kishibe || Dr. Kashiwabuchi
|-
|}

==References==
 

==External links==
* 
*  

 

 
 
 
 

 