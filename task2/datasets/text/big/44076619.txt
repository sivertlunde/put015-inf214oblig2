Buckskin Frontier
{{Infobox film
| name           = Buckskin Frontier
| image          = Buckskin Frontier poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lesley Selander
| producer       = Harry Sherman 
| screenplay     = Norman Houston Bernard Schubert 
| story          = Harry Sinclair Drago 
| starring       = Richard Dix Jane Wyatt Albert Dekker Lee J. Cobb Victor Jory Lola Lane Max Baer Joe Sawyer
| music          = Victor Young
| cinematography = Russell Harlan
| editing        = Sherman A. Rose 
| studio         = Harry Sherman Productions
| distributor    = United Artists
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Buckskin Frontier is a 1943 American action film directed by Lesley Selander and written by Norman Houston and Bernard Schubert. The film stars Richard Dix, Jane Wyatt, Albert Dekker, Lee J. Cobb, Victor Jory, Lola Lane, Max Baer and Joe Sawyer. The film was released on May 14, 1943, by United Artists.   

==Plot==
 

== Cast == 
*Richard Dix as Stephen Bent
*Jane Wyatt as Vinnie Marr
*Albert Dekker as Gideon Skene
*Lee J. Cobb as Jeptha Marr
*Victor Jory as Champ Clanton
*Lola Lane as Rita Molyneaux
*Max Baer as Tiny
*Joe Sawyer as Brannigan Harry Allen as McWhinny
*Francis McDonald as Duval
*George Reeves as Surveyor
*Bill Nestell as Whiskers

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 
 
 
 