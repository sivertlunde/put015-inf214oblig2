Evita (2008 film)
{{Infobox film
| name           = Evita
| image          = Evitaposter.jpg
| caption        = 
| director       = Eduardo Montes-Bradley
| producer       = Soledad Liendo
| writer         = Eduardo Montes-Bradley
| narrator       = Taylor Johnson
| starring       = Eva Duarte 
| music          = Various
| cinematography = 
| editing        = Eduardo Montes-Bradley
| distributor    = Heritage Film Project, Alexander Street Press
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = $110.000
}}
Evita is a documentary film on the life of Eva Duarte created by Eduardo Montes-Bradley.  Conformed in its entirety with previously unseen historical footage and documents, the film reconstructs the life of the former Argentine first lady from an unbiased perspective. The film starts with meticulous examination of Evita Duartes origins, her relationship with her parents (particularly her father) and her siblings. Evita goes on to reveal intimate aspects of Duartes early childhood and adolescence, leading to her resolving to flee her home in the countryside for the capital city of Buenos Aires. The latter segments of the film are highlighted by interviews with former teachers from her elementary schooling in Junin, a small city in the province of Buenos Aires. Throughout the film, the director is steadily reminding the audience of the domestic and international context out of which Evita Duarte emerged, enhancing the viewers understanding the facts. The second act of the film concludes with her funeral in Buenos Aires. The third act exposes the macabre plot to have Evita Perons corpse disappear, and looks at the curious series of events leading to the exchange of cadavers between the so-called Peronist youth and the military regime. Evita premiered in the US on WCVE Richmond-PBS, and WHTJ Charlottesville-PBS on July 12, 2012.  Evita has been included in courses on Political Science and History.  Most recently, the documentary was listed 11 in the “Top 25 Political Documentaries That Shed Light On Latin America’s Reality" 

==Film festivals==
Evita was screened at the Virginia Film Festival, in Charlottesville, on November 4, 2011.  

==References==
 

==External links==
*  
*  

 


 
 
 
 
 


 