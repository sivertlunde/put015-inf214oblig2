The Fair Co-Ed
{{infobox film
| name           = The Fair Co-Ed
| image          =
| imagesize      = 
| caption        =
| director       = Sam Wood
| producer       = Irving Thalberg Sam Wood William Randolph Hearst
| writer         = Byron Morgan (scenario) Joseph Farnham (intertitles)
| based on       =  
| starring       = Marion Davies
| cinematography = John Seitz
| editing        = Conrad Nervig
| studio         = Cosmopolitan Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes (7 reel#Motion picture terminology|reels) 
| country        = United States
| language       = Silent film English intertitles
}} silent film comedy starring Marion Davies and released through Metro-Goldwyn-Mayer|MGM. The film was produced by William Randolph Hearst, through Cosmopolitan Productions and directed by Sam Wood.

The film is based on a 1909 play/musical comedy The Fair Co-Ed by George Ade which starred a young Elsie Janis, and opened on Broadway on February 1, 1909. 

The film survives today, supposedly in the MGM/UA archives, now controlled by Warner Brothers. 

==Cast==
*Marion Davies - Marion Bright
*Johnny Mack Brown - Bob
*Jane Winton - Betty
*Thelma Hill - Rose
*Lillian Leighton - Housekeeper
*Gene Stone - Herbert
*James Bradbury, Sr. - uncredited
*Lou Costello - Extra
*Joel McCrea - Student
*Jacques Tourneur -Extra

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 