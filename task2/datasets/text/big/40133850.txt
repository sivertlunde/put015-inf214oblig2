Farmer Al Falfa's Prize Package
{{Infobox Hollywood cartoon|
| cartoon_name = Farmer Al Falfas Prize Package
| series = Farmer Al Falfa
| image = 
| caption = 
| director = Mannie Davis George Gordon
| story_artist = 
| animator = 
| voice_actor = 
| musician = 
| producer = Paul Terry
| studio = Terrytoons
| distributor = 20th Century Fox
| release_date = July 31, 1936
| color_process = Black and white
| runtime = 5:48 English
| preceded_by = Puddy the Pup and the Gypsies
| followed_by = The Health Farm
}}

Farmer Al Falfas Prize Package is a short animated film. It is among the theatrical cartoons, featuring Farmer Al Falfa. When released for home viewing by Castle Films, the film wore the alternate title of The Prize Package. 

==Plot==
Farmer Al Falfa is napping in front of his countryside house until he receives a letter. The letter is from his brother Hank who is sending him a pet named Kiko the Kangaroo|Kiko. Al is excited by this at first. Momentarily the package with the pet arrives, and it appears Kiko is a kangaroo.

Al seems dismayed about Kiko being a kangaroo. Nevertheless he gives Kiko a decent welcome to the house as well as doing some shining on Kikos shoes. After bathing inside, Kiko comes out to play with Al but the old farmer doesnt find the marsupials antics enjoyable.

Moments afterward, a pack of cops confront Al and tell him its "illegal" to keep a kangaroo. When they attempt to arrest him, Kiko brawls with the cops. Kiko then forces the cops back into their vehicle which the kangaroo then pushes further into the horizon. Al is most thankful and begins to adore his pet.

==See also==
*Kiko the Kangaroo

==External links==
*  at the Big Cartoon Database

 
 
 
 
 
 

 