Sharaabi
{{Infobox film
| name           = Sharaabi
| image          = Sharaabi.jpg
| image_size     =
| caption        =
| director       = Prakash Mehra
| producer       = Satyendra Pal
| writer         =
| narrator       = Pran Om Prakash
| music          = Bappi Lahiri
| cinematography = N. Satyen
| editing        = J. Adhikari
| distributor    = Prakash Mehra Productions
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}}
 1984 Indian Hindi drama Steve Gordons Arthur starring Dudley Moore.
 Pran and Om Prakash along with Bharat Bhushan and Ranjeet. The music was composed by Bappi Lahiri. 

The film was remade in Kannada as Nee Thanda Kanike with Vishnuvardhan (actor)|Vishnuvardhan.

==Plot==

Vicky Kapoor (Amitabh Bachchan), the only son of multi-millionaire industrialist, Amarnath (Pran (actor)|Pran), grows up to be an alcoholic, a spoilt  brat but with a good heart. Amarnath has detached himself completely from Vickys life and provides him money in lieu of affection and care, leaving this to be taken care of by Munshi Phoolchand (Om Prakash). Vicky is very resentful of being ignored by his dad in this unceremonious manner and leaves no stone unturned to mock and belittle him at every available opportunity. Amarnath hopes to get his son married, but Vicky loves Meena (Jaya Prada), the daughter of a poor blind man, which is not acceptable to Amarnath. One day, Amarnath decides he has had enough and decides to turn Vicky out of his house. He has him sign waivers and asks him to get out, which Vicky happily does. While Vicky is relieved of being freed from his dads control, the question remains that with no skills at making a living, how is Vicky to survive in a cold-hearted world?
It celebrated platinum jubilee all over. The record sales touched 1 million marks and earned platinum disc for HMV.

==Cast==
*Amitabh Bachchan as Vicky Kapoor
*Jaya Prada as Meena
*Om Prakash as Munshi Phoolchand  Pran as Amarnath Kapoor 
*Bharat Bhushan as Masterji 
*Chandrashekhar as Advocate Saxena 
*C.S. Dubey as Father of prospective bride 
*Rajan Haksar   
*A.K. Hangal as Meenas Blind Father 
*Dinesh Hingoo as Neighbour calling police in the song "De De Pyar De" 
*Jankidas as Rustom Bandukwala 
*Pinchoo Kapoor as Father of prospective bride 
*Satyendra Kapoor as Govardhandas 
*Viju Khote as Mr. Nahata 
*Mayur as Bar waiter 
*Mukri as Natthulal 
*Suresh Oberoi as Abdul 
*Deepak Parashar as Inspector Anwar 
*Babbanlal Yadav as Drunker in song "Jahan Chaar Yaar"
*Smita Patil (Guest Appearance) in song "Jahan Chaar Yaar" 
*Ranjeet as Natwar 
*Gur Bachchan Singh as Ruffian #2  Sudhir as Ruffian #1

==Track listing==

{{Track listing
| extra_column = Singer(s)
| title1 = Mujhe Naulakha Mangaade | extra1 = Kishore Kumar, Asha Bhosle| length1 = 10:56
| title2 = Manzilein Apni Jagah | extra2 = Kishore Kumar | length2 = 05:55
| title3 = Jahaan Chaar Yaar | extra3 = Kishore Kumar, Amitabh Bachchan | length3 = 06:36
| title4 = De De Pyaar De - Female | extra4 = Asha Bhosle | length4 = 04:29
| title5 = De De Pyaar De - Male | extra5 = Kishore Kumar | length5 = 05:48
| title6 = Inteha Ho Gayi Intezaar Ki | extra6 = Kishore Kumar, Asha Bhosle | length6 = 08:53
| title7 = Log Kehte Hai Main Sharaabi Hoon | extra7 = Kishore Kumar, Asha Bhosle | length7 = 07:53 }}

==Awards and nominations==
*Filmfare Best Music Director Award-Bappi Lahiri
*Filmfare Best Male Playback Award-Kishore Kumar for the song "Manzilein Apni Jaga"
*Filmfare Nomination for Best Film
*Filmfare Nomination for Best Director-Prakash Mehra
*Filmfare Nomination for Best Actor-Amitabh Bachchan
*Filmfare Nomination for Best Actress-Jayaprada Anjaan for the song "Manzilein Apni Jaga" Anjaan and Prakash Mehra for the song "Inteha Ho Gayi" 

== References ==
 

==External links==
*  

 
 
 
 
 