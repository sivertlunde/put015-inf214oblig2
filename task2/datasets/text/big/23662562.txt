The Man Who Planted Trees (film)
 
{{Infobox film
| name           = The Man Who Planted Trees
| image          = The Man Who Planted Trees (film).jpg
| caption        = Film poster
| director       = Frédéric Back
| producer       = Frédéric Back Hubert Tison
| writer         = Jean Roberts
| based on       =  
| narrator       = Philippe Noiret Christopher Plummer
| starring       =
| music          =
| cinematography =
| editing        = Norbert Pickering CBC National NFB Societe Radio-Canada
| distributor    = 
| released       =  
| runtime        = 30 minutes
| country        = Canada
| language       = French/English
| budget         =
}}
 short animated film directed by Frédéric Back. It is based on Jean Gionos novel The Man Who Planted Trees. This 30-minute film was distributed in two versions, French and English, narrated respectively by actors Philippe Noiret and Christopher Plummer, and produced by Télévision de Radio-Canada|Radio-Canada.
 Region 1 DVD, either on its own or with other animated films directed by Frédéric Back.

==Awards==
The film won the Academy Award (1988) for Best Animated Short Film.    It also competed for the Short Film Palme dOr at the 1987 Cannes Film Festival.   

In 1994, it was voted number 44 of the 50 Greatest Cartoons of all time by members of the animation field.

==Further reading==
* Olivier Cotte (2007) Secrets of Oscar-winning animation: Behind the scenes of 13 classic short animations. (Making of "The Man Who Planted Trees") Focal Press. ISBN 978-0-240-52070-4

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 