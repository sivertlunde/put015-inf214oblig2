Ausente (film)
{{Infobox film
| name           = Ausente  (Absent)
| image          = Ausente-by-marco-berger.jpg
| caption        = Theatrical release poster
| director       = Marco Berger
| producer       = Marco Berger, Mariana Contreras & Pablo Ingercher Casas (executive producers) 
| writer         = Marco Berger
| starring       = Carlos Echevarría  Javier De Pietro 
| music          = Pedro Irusta
| cinematography = Tomas Perez Silva
| editing        = Marco Berger
| studio         = 
| distributor    = 
| released       =    
| runtime        = 90 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = 
}} Argentine director other way round (i.e. where an older individual in a position of authority or trust becomes infatuated with a minor and lures the minor into a sexual relationship).

==Synopsis==
The story is told by Sebastián (Carlos Echevarría), the sports coach who becomes the object of a students affection.  Martin (Javier De Pietro) is a 16-year old young student who is attracted to his coach, Sebastián.  Sebastián tries to keep Martin at a distance, but at the same time tries to be kind and nurturing. Martin goes to great lengths in his attempt to get close to his teacher. When Martin hurts his eye during his swimming class, Sebastián initially takes him to the hospital. After treatment, Sebastián offers Martin a ride home. However, Martin was supposed to spend the night at a friends house, so no one is expecting him to come home that night.  Martin spends the night at the Sebastiáns house.  Things come to a head when Sebastián realizes that he was being lied to and punches Martin in the face.  He is not angry from disgust for being the object of Martins desire, rather because Martins dishonesty could potentially cost the coach his job. Offended, Marin taunts Sebastián, telling him to call the police and suggesting it would cause greater problems for Sebastian. Later, Martin accidentally falls off a roof after retrieving a neighbors soccer ball, and Sebastian finds himself filled with remorse.

==Style==
The director is vague on certain plot points. In the last images, for example, there is a shot of Sebastián gently kissing Martin on the lips. It is not clear whether this actually happened or only occurred in Sebastiáns imagination. It is also unclear whether Martin accidentally fell to his death, or whether it was suicide, driven by Sebastiáns rejection. The viewer is kept contemplating if a romantic relationship had occurred, and if it did, if it is immoral in itself regardless of consent.

==Reception==
Read in newspaper reports as child abuse cases. They also praised Javier De Pietro for his candid role as Martín.

When the film won "Best feature film" during Berlin International Film Festival, the judging committee praised it as a film with "an original screenplay, an innovative aesthetic and a sophisticated approach, which creates dynamism. A unique combination of homoerotic desire, suspense and dramatic tension."     

==Cast==
*Carlos Echevarría as Sebastián
*Javier De Pietro as Martín
*Antonella Costa as Mariana
*Rocío Pavón as Analía
*Alejandro Barbero as Juan Pablo

==Awards and nominations==
*2011: The film won the Teddy Awards for "Best feature film" at the Berlin International Film Festival
*2011: The actor Javier De Pietro was nominated for "Best New Actor" for his role Martín in the film during the Argentine Academy of Cinematography Arts and Sciences Awards

==See also==
*Hawaii (2013 film)|Hawaii (2013 film)
*Plan B (2009 film)|Plan B (2009 film)

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 