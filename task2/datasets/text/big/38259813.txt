Kshanbhar Vishranti
{{Infobox film
| name           = Kshanbhar Vishranti
| image          = Kshanbhar Vishranti.jpg
| border         = yes
| alt            =  
| caption        = Theatrical release poster
| director       = Sachit Patil
| producer       = Sandeep S. Shinde Maulik Bhatt
| writer         = 
| starring       = Bharat Jadhav Sachit Patil Sonalee Kulkarni Sidharth Jadhav Manwa Naik Hemant Dhome Kadambari Kadam Maulik Bhatt Pooja Sawant Shubhangi Gokhale
| music          = Rishikesh Kamerkar
| cinematography = 
| editing        = Amit Pawar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = ₹ 2,25,00,000.00
| gross          = 
}}
Kshanbhar Vishranti is a Marathi film released on 9 April 2010.  The movie has been produced by Sandeep S. Shinde along with Maulik Bhatt and directed by Sachit Patil. The movie is a thought provoking film with a philosophy of its own and a sheer poetry on celluloid.

== Synopsis ==
This is the story of four young couples who share a strong bond of friendship, with different dreams and aspirations and find love amidst themselves. These four young couples represent todays youth that are hard working and will not leave any stone unturned to achieve their goals.

Ever ready to prove their mettle and their ability to understand and support each other, they manage to create a platform that helps them move ahead in life. 

== Cast ==

The cast includes Bharat Jadhav, Sachit Patil, Sonalee Kulkarni, Sidharth Jadhav, Manava Naik, Hemant Dhome, Kadambari Kadam, Maulik Bhatt, Pooja Sawant & Shubhangi Gokhale.

==Soundtracks==
The music is composed by Hrishikesh Kamerkar. Lyrics are penned by Guru Thakur.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s)!! Duration
|-
| "Holiday Holiday"
| Hrishikesh Kamerkar
| 05:38
|-
| "Tu Sanso Me Hai Tu"
| Hrishikesh Kamerkar, Shilpa Pai
| 05:13
|-
| "Bavra"
| Hrishikesh Kamerkar, Shilpa Pai, Avadhoot Gupte, Janavi Prabhu-Arora
| 04:39
|-
| "Sargam"
| Shilpa Pai
| 04:52
|-
| "Kshanbhar Vishranti"
| Hrishikesh Kamerkar, Shilpa Pai
| 03:58
|-
| "Holiday Holiday (Remix)"
| Hrishikesh Kamerkar
| 04:05
|-
| "Tu Sanso Me Hai Tu (Remix)"
| Hrishikesh Kamerkar, Shilpa Pai
| 04:01
|}
 

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 


 