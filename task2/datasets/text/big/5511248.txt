Holes (film)
{{Infobox film
| name           = Holes
| image          = Holesposter03.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster Andrew Davis
| producer       = {{Plain list |
* Andrew Davis 
* Lowell D. Blank 
* Mike Medavoy 
* Teresa Tucker-Davies
}}
| screenplay     = Louis Sachar
| based on       =  
| starring       = {{Plain list |  
* Sigourney Weaver 
* Jon Voight 
* Patricia Arquette  
* Tim Blake Nelson 
* Shia LaBeouf 
}}
| music          = Joel McNeely
| cinematography = Stephen St. John
| editing        = {{Plain list |
* Thomas J. Nordberg 
* Jeffrey Wolf
}}
| studio         = {{Plain list |
* Walt Disney Pictures 
* Walden Media 
* Phoenix Pictures 
* Chicago Pacific Entertainment
}}
| distributor    = Buena Vista Pictures Distribution  
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $71,406,573 
}} novel of Khleo Thomas, distribution company Buena Vista.

Holes was Scott Planks final film; he died on October 24, 2002.

==Plot==
 
Stanley Yelnats the 4th is a teenager born to a family who have been cursed with bad luck. One day, Stanley is falsely accused of stealing a pair of sneakers. Upon conviction, Stanley decides to attend Camp Green Lake, a juvenile detention camp, in lieu of serving his time in jail.

He arrives to find that the camp is a dried-up lake run by the Warden, Louise Walker, her assistant Mr. Sir, and camp counselor Dr. Pendanski. Prisoners spend each day digging holes in the desert to "build character." The inmates are told that they may earn a day off, if they find anything interesting or unusual. After finding a golden lipstick tube initialed K.B. and a fossil, Stanley is accepted into the group and is given the nickname "Caveman." After taking the blame for Magnets stealing of Mr. Sirs sunflower seeds, Stanley is taken to the wardens house where old wanted posters and newspapers lead him to suspect that "KB" stands for Kate Barlow.

In a series of flashbacks the history of Camp Green Lake is revealed. The town was a lake town, thriving with water and life until Katherine Barlow, a local teacher, was involved in a love triangle with the wealthy Trout Walker, whom Kate rejected, and an African American onion seller named Sam, whom Kate loved. One day she ended up kissing Sam. At that time, it was illegal for people of different races to show affection towards one another. After much turmoil, a group of men from the town, including Walker, kills Sam while he is rowing his boat. Kate kills the local sheriff in retaliation and becomes an outlaw. Sams death causes the Green Lake to turn into an arid wasteland. Years later the now bankrupt Walkers approach the outlaw Kate and demand she hand over her buried treasure, but Kate responds that they could "dig for a hundred years" and not find it. Kate allows a yellow spotted lizard to bite her killing her within minutes. Before she allows the lizard to bite her she says in a resentful tone, "Start digging, Trout". For the next generation the Walker family sets about digging for the treasure but never actually find anything. 
 
While digging one day, Pendanski insults Zero, who responds by hitting Pendanski with a shovel and running into the desert. Stanley goes searching for Zero. Stanley and Zero survive in the arid wasteland. Eventually Stanley carries the now ill Zero up the mountain, where they find a wild field of onions and a spring, helping them regain strength and at the same time unknowingly fulfilling his ancestors promise to the fortune teller, breaking the curse, and restoring his familys luck.

Suddenly feeling lucky, Stanley and Zero decide to return to the camp and investigate the hole where Stanley found the lipstick, where they dig deeper and uncover a chest just as they are discovered by the warden, Mr. Sir, and Pendanski. After escaping Walker, with the help of some lizards, its revealed that she is Trouts granddaughter and using the inmates to search for treasure. The next morning, the attorney general and Stanleys lawyer arrive, the chest Stanley found is revealed to have belonged to his great-grandfather before being stolen by "Kissin Kate". The warden, Mr. Sir (who turns out to be a paroled criminal named Marion Sevillo), and Pendanski (who, as it turned out, was impersonating a doctor) are arrested for perverting the laws of justice, Stanley and Zero are released from the now-under-investigation camp. With justice finally being served, rain returns to Green Lake. The Yelnats family claims ownership of the chest, which contains jewels, old money, deeds, and promissory notes, which they share with Zero, who reunites with his missing mother. Camp Green Lake was closed and Stanley and his friends heard it was supposed to reopen as a Girl Scout camp. The Yelnatses and the Zeronis move to new houses along with their friends, and Clyde Livingston apologizes to Stanley for accusing him of stealing the shoes.

==Cast== Stanley "Caveman" Yelnats IV Hector "Zero" Zeroni
* Sigourney Weaver as Warden Louise Walker
* Jon Voight as Marion Sevillo/Mr. Sir
* Tim Blake Nelson as Dr. "Mom" Pendanski
* Jake M. Smith as Alan "Squid"
* Byron Cotton as Theodore "Armpit" Brenden Jefferson Rex "X-Ray"
* Miguel Castro as José "Magnet"
* Max Kasch as Ricky "Zigzag"
* Noah Poletiek as Brian "Twitch"
* Zane Holtz as Louis "Barf Bag"
* Steve Koslowski as Lump
* Siobhan Fallon Hogan as Tiffany Yelnats
* Henry Winkler as Stanley Yelnats III Nathan Davis as Stanley Yelnats Jr.
* Shelley Malil as the Yelnats Landlord
* Rick Fox as Clyde "Sweet Feet" Livingston
* Eartha Kitt as Madame Zeroni
* Damien Luvara as Elya Yelnats
* Sanya Mateyas as Myra Menke
* Ravil Isyanov as Morris Menke
* Ken Davitian as Igor Barkov
* Patricia Arquette as Holes (novel)#Katherine "Kissin Kate" Barlow|Kissin Kate Barlow
* Scott Plank as Charles "Trout" Walker
* Dulé Hill as Sam the Onion Man
* Allan Kolman as Stanley Yelnats Sr.
* Louis Sachar as Mr. Collingwood
* Roma Maffia as Atty. Carla Morengo
* Gary Bullock as Prospector

==Music==
 
The films music which included the Grammy winning single "Just Like You" by Keb Mo, and "Dig It" by The D Tent Boys (the actors portraying the D Tent group inmates), which had a music video which played regularly on Disney Channel. The soundtrack also included contributions by Eels (band)|Eels, Devin Thompson, Dr. John, Eagle Eye Cherry, Fiction Plane, Little Axe, Moby, North Mississippi Allstars, Pepe Deluxé, Shaggy (artist)|Shaggy, Stephanie Bentley, and Teresa James and the Rhythm Tramps.

The score was written by Joel McNeely.

{{Infobox album
| Name     = Holes (Original Soundtrack)
| Type     = Soundtrack
| Artist   = Various
| Cover    = 
| Released = April 15, 2003
| Length   =
| Label    = Walt Disney Records
| Reviews
}}

 
# "Dig It" – D-Tent Boys Shaggy
# Eels
# "Honey (Moby song)|Honey" – Moby
# "Im Gonna Be A Wheel Someday" – Teresa James & The Rhythm Tramps
# "Just Like You" – Keb Mo
# "Everybody Pass Me By" – Pepe Deluxé
# "I Will Survive" – Stephanie Bentley
# "Shake Em On Down" – North Mississippi Allstars
# "Dont Give Up" – Eagle Eye Cherry
# "Happy Dayz" – Devin Thompson
# "Lets Make A Better World" – Dr. John
# "If Only" – Fiction Plane
# "Eyes Down" – Eels
# "Down To The Valley" – Little Axe

==Reception==

===Box office=== Anger Management s second weekend. 

The film would go on to gross a domestic total of $67,406,173 and an additional $4 million in international revenue, totaling $71,406,573 at the box office against a $20 million budget, making the film a moderate financial success.   

===Critical response===
The film received positive reviews.  , which uses an average of critics reviews, the film has a 71/100 rating based on 28 reviews, indicating "generally favorable reviews". 
 Anger Management." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 