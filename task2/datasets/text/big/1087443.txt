The Mark of Zorro (1920 film)
{{Infobox film
|name=The Mark of Zorro
|image=FairbanksMarkofZorro.jpg
|producer=Douglas Fairbanks
|director=Fred Niblo Theodore Reed (2nd unit)
|writer=Johnston McCulley (story) Eugene Miller & Douglas Fairbanks (scenario) Noah Beery Robert McKim
|music=Mortimer Wilson
|cinematography=William C. McGann  Harris Thorpe William Nolan
|studio=Douglas Fairbanks Pictures Corporation
|distributor=United Artists
|released= 
|country=United States
|runtime=90 minutes
|language=Silent film  English intertitles
}}
  adventure was the first movie version of The Mark of Zorro. Based on the 1919 story "The Curse of Capistrano" by Johnston McCulley, which introduced the masked hero, Zorro, the screenplay was adapted by Fairbanks (as "Elton Thomas") and Eugene Miller.
 Noah Beery) Disney TV Guy Williams as Diego/Zorro, who was renamed Don Diego de la Vega.
 once in again in 1974 (starring Frank Langella).

==Primary cast==
*Douglas Fairbanks as Don Diego Vega/Señor Zorro
*Marguerite De La Motte as Lolita Pulido
*Noah Beery, Sr. as Sergeant Pedro Gonzales
*Charles Hill Mailes as Don Carlos Pulido
*Claire McDowell as Doña Catalina Pulido Robert McKim as Captain Juan Ramon
*George Periolat as Governor Alvarado Walt Whitman as Father Felipe
*Sidney De Gray as Don Alejandro Vega
*Tote Du Crow as Bernardo, Don Diegos mute servant
*Noah Beery, Jr. as Boy Charles Stevens as Peon beaten by Sergeant Gonzales
*Milton Berle (uncredited child)

==Plot==
  Spanish California of the early 19th century. Seeing the mistreatment of the peons by rich landowners and the oppressive colonial government, Don Diego, who is not as effete as he pretends, has taken the identity of the masked Robin Hood-like rogue Señor Zorro ("Mr. Fox"), champion of the people, who appears out of nowhere to protect them from the corrupt administration of Governor Alvarado, his henchman the villainous Captain Juan Ramon and the brutish Sergeant Pedro Gonzales (Noah Beery, Wallace Beerys older half-brother). With his sword flashing and an athletic sense of humor, Zorro scars the faces of evildoers with his mark, "Z".

When not in the disguise of Zorro, dueling and rescuing peons, Don Diego courts the beautiful Lolita Pulido with bad magic tricks and worse manners. She cannot stand him. Lolita is also courted by Captain Ramon; and by the dashing Zorro, whom she likes.

In the end, when Lolitas family is jailed, Don Diego throws off his masquerade, whips out his sword, wins over the soldiers to his side, forces Governor Alvarado to abdicate, and wins the hand of Lolita, who is delighted to discover that her effeminate suitor, Diego, is actually the dashing hero.

==Reception and impact==
The New York Times gave the The Mark of Zorro a mixed review. 

Fairbanks biographer  .” 

==Further Reading==
*Vance, Jeffrey. Douglas Fairbanks. Berkeley, CA: University of California Press, 2008.  ISBN 978-0-520-25667-5. 

==References==
{{Reflist|refs=
 {{Citation
|date=November 29, 1920
|title=The Screen
|newspaper=The New York Times
|publisher=The New York Times Company
|place=New York, NY, U.S.A.
|issn=0362-4331
|oclc=1645522
|url=http://query.nytimes.com/mem/archive-free/pdf?res=9B03EEDB163CE533A2575AC2A9679D946195D6CF
|accessdate=July 5, 2013
|archiveurl=http://movies.nytimes.com/movie/review?res=9B03EEDB163CE533A2575AC2A9679D946195D6CF
|archivedate=an unspecified date
|quote=All of which may mean that "The Mark of Zorro" is more enjoyable than "The Curse of Capistrano" could ever hope to be.
}} 
}}

==External links==
* 
* 
* 
* 
* . at The Film Tribune
* 

 
 

 
 
 
 
 
 
 
 
 
 