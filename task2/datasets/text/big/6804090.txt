Charley's (Big-Hearted) Aunt
 
{{Infobox film
| name           = Charleys (Big-Hearted) Aunt
| image          = "Charleys_(Big-Hearted)_Aunt"_(1940).jpg
| image_size     =
| caption        =
| director       = Walter Forde Edward Black (producer)
| writer         = Brandon Thomas (farce) J.O.C. Orton (screenplay) & Marriott Edgar (screenplay) & Ralph Smart (screenplay)
| narrator       =
| starring       = See below
| music          = Louis Levy
| cinematography = Arthur Crabtree
| editing        = R.E. Dearing
| studio         = Gainsborough Pictures
| distributor    = General Film Distributors (UK)
| released       = 31 August 1940
| runtime        = 75 minutes
| country        = UK
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Charleys (Big-Hearted) Aunt is a 1940 British comedy film directed by Walter Forde starring Arthur Askey and Richard Murdoch as Oxford scholars. 

The film is one of many to be made based on the Victorian farce Charleys Aunt. Arthur Askeys professional nickname was "Big-Hearted Arthur", which was added to the title to distinguish it from Jack Bennys version, for its (limited) American release.

==Plot==
Oxford students Arthur (Arthur Askey), Stinker (Richard Murdoch), and Albert (Graham Moffatt) are in danger of being "sent down" (expelled) for bad behaviour.  Learning the Dean of Bowgate College is an amateur Egyptologist, Arthur—who had just played the lead in a stage version of "Charleys Aunt"—poses as Alberts wealthy Aunt Lucy, who might finance an archeological expedition if the Dean is lenient on her nephew and his friends. Unfortunately, the real Aunt Lucy picks this day to pay a visit to Oxford herself, with calamitous results.

==Differences from play==
Aside from the Oxford setting and the premise of a male student impersonating his wealthy aunt, the film bears little resemblance to the original play. In one brief sequence, the play "Charleys Aunt" is shown being performed by the Oxford students.

==Cast==
*Arthur Askey as Arthur Linden-Jones
*Richard Murdoch as "Stinker" Burton
*Graham Moffatt as Albert Brown
*Moore Marriott as Jerry
*J.H. Roberts as Dean of Bowgate
*Felix Aylmer as The Proctor
*Wally Patch as The Buller
*Leonard Sharp as Bullers assistant
*Phyllis Calvert as Betty Forsythe
*Jeanne de Casalis as Aunt Lucy
*Elliott Mason as Dame Luckton

==Soundtrack==
 

==Critical reception==
*TV Guide wrote "Brandon Thomass oft-filmed farce (at least seven times since 1925) has frequently been better, but Askey gives it a good shot."  
*Sky movies wrote "tailored to the talents of the two stars, on the strength of their great success in the hit radio series Band Waggon. Those stalwarts of the British comedy film, 1935-45, plump Graham Moffatt and doddery Moore Marriott, are on hand to add to the fun, and the heroine is Phyllis Calvert, who was appearing in her second film."  

==External links==
* 
* 

==References==
 

 
 
 

 
 
 
 
 
 
 
 
 


 