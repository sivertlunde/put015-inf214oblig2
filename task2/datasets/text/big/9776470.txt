Gente bien
{{Infobox film
| name           = Gente bien
| image          =
| image size     = 
| caption        = Hugo del Carril
| director       = Manuel Romero
| producer       = Manuel Romero
| writer         = Manuel Romero
| narrator       =
| starring       = Hugo del Carril Tito Lusiardo Delia Garcés
| music          = Francisco Canaro   Juan dArienzo Héctor Quesada Tito Ribero Alberto Soifer
| cinematography = Antonio Solano
| editing        = José Cardella
| distributor    = EFA
| released       =  
| runtime        =
| country        = Argentina
| language       = Spanish
| budget         =
| preceded by    =
| followed by    =
}} Argentine musical tango film premiered in Buenos Aires on June 28, 1939 and the United States on October 8, 1939 and starred Hugo del Carril, Tito Lusiardo and Delia Garcés.

The score was composed by Francisco Canaro and Tito Ribero.

==Cast==
*María Armand
*Amalia Bernabé
*María Esther Buschiazzo
*Miguel Caló
*Hugo del Carril
*Lucy Galián
*Delia Garcés
*Tito Lusiardo
*June Marlowe
*Ana May
*Nathán Pinzón
*Enrique Roldán
*Marcelo Ruggero

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 


 
 