The Rise of Catherine the Great
 
 
{{Infobox Film
| name           = Catherine the Great
| image_size     = 
| image	=	The Rise of Catherine the Great FilmPoster.jpeg
| caption        = 
| director       = Paul Czinner
| producer       = Alexander Korda Ludovico Toeplitz (uncredited)
| writer         = Marjorie Deans Arthur Wimperis
| narrator       = Alexander Kerensky 
| starring       = Elisabeth Bergner Douglas Fairbanks Jr. Flora Robson
| music          = Ernst Toch (uncredited) Irving Berlin
| cinematography = Georges Périnal Harold Young
| distributor    = London Films/United Artists
| released       = 9 February 1934
| runtime        = 95 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1934 UK|British Grand Duke Empress Elizabeth.

== Plot ==
This historical drama recounts the events that led to the accession of Catherine the Great, Empress of all the Russias. The film opens with the arrival of Princess Sophie Auguste Frederika – whose name would be changed to ‘Catherine’ – from her father’s court of Anhalt-Zerbst (in modern Germany) to the court of the Empress Elizabeth.  "Little Catherine" is to marry the Grand Duke Peter, nephew and heir presumptive of the unmarried and childless Empress Elizabeth.

Peter already displays signs of mental instability and a sharply misogynist streak.  He rejects Catherine on their wedding night, reacting to something innocently said by his French valet, claiming that she used feminine tricks to win him over. In time though, Peter accepts her and they have a happy marriage for a while.  Meanwhile, Catherine gains important experience of government from working as principal aide to the empress.

The empress dies and Peter becomes tsar, but his mental illness is starting to get the better of him, along with sheer boredom in the job.  Catherine still loves him despite beginning a very public love affair with one of her best friends – until one night when Peter goes one step too far in publicly humiliating his wife.  She ceases to love him, which enables her to be clear-headed in supporting a planned coup détat.  The following morning, he is arrested and Catherine is made Empress of All the Russias.

The elevation is marred by Peter’s murder that very morning, contrary to Catherine’s command. Grigory Orlov explains that everything has a price, and the crown has the highest price of all. The film ends, with Catherine in tears on her throne, while the cheers of the crowds are heard outside.

==Cast==
*Douglas Fairbanks Jr. as Grand Duke Peter
*Elisabeth Bergner as Catherine
*Flora Robson as Empress Elisabeth
*Gerald du Maurier as Lecocq
*Irene Vanbrugh as Princess Anhalt-Zerbst
*Joan Gardner as Katushienka
*Dorothy Hale as Countess Olga
*Diana Napier as Countess Vorontzova Griffith Jones as Grigory Orlov
*Gibb McLaughlin as Bestujhev (as Gibb MacLaughlin)
*Clifford Heatherley as Ogarev
*Laurence Hanray as Goudovitch
*Allan Jeayes as Col. Karnilov

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 