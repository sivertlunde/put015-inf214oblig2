Shake, Rattle & Roll XV
{{Infobox film
| name           = Shake, Rattle & Roll XV
| image          = 
| alt            = 
| caption        = 
| director       = Dondon Santos (Ahas) Jerrold Tarog (Ulam) Percival M. Intalan (Flight 666)
| producer       = Lily Monteverde Roselle Monteverde
| writer         = 
| screenplay     = 
| story          = 
| starring       = Erich Gonzales JC de Vera Carla Abellana Dennis Trillo Lovi Poe Matteo Guidicelli John Lapus Chanda Romero Ariel Rivera Kiray Celis IC Mendoza
| music          = Cesar Francis Concio (Ahas) Jerrold Tarog (Ulam) Von De Guzman (Flight 666)
| cinematography = Mackie Galvez (Ulam)   
| editing        = Benjamin Tolentino (Ulam) 
| studio         =  Regal Entertainment, Inc. Regal Multimedia, Inc.
| released       =  
| runtime        = 103 minutes
| country        = Philippines
| language       = Tagalog English
| budget         = 
| gross          = P 55.5 million
}} horror anthology film directed by Dondon Santos, Jerrold Tarog, and Perci Intalan. The film stars an ensemble cast including Erich Gonzales, JC de Vera, Carla Abellana, Dennis Trillo, Lovi Poe, and Matteo Guidicelli.
 40th Metro Manila Film Festival and was released on December 25, 2014 in Philippine cinemas nationwide. It is the most expensive installment in the Shake, Rattle & Roll film series,  although the films co-producer Lily Monteverde stated she doesnt want to reveal its exact budget yet. 

==Plot==
===Ahas===
Sandra/Sarah (Erich Gonzales) portray the mythical twin snake of Alegria Mall/Shopping Mall.

===Ulam===
In this segment of the horror anthology, a family is being fed by their maid with delicious dishes that soon turns them into horrible monsters. This episode starring Carla Abellana and Dennis Trillo.

===Flight 666===
A hijacker (Bernard Palanca) terrorizes the passengers of Flight 666. Amidst the chaos, a passenger gives birth to a "tiyanak" (a monstrous creature from Philippine mythology) that soon attacks the rest of the people inside the aircraft one by one. Flight stewardess Karen (Lovi Poe), who has a relationship with Dave (Matteo Guidicelli), together with the help of co-pilot Bryan (Daniel Matsunaga), must save them from this horrifying trip to Hell.

== Cast ==
=== Ahas ===
*Erich Gonzales as Sandra/Sarah
*JC De Vera as Troy
*John Lapus as Iggy Moda
*Ariel Rivera as Alberto
*Alice Dixson as Lourdes
*Lou Veloso as Mang Banjo Giselle Canlas as Jinny
*Jason Francisco as Jake
*Melai Cantiveros as Julie

=== Ulam ===
*Dennis Trillo as Henry
*Carla Abellana as Aimee
*Chanda Romero as Aling Lina
*John Lapus as Iggy
*Perla Bautista as Amah Choleng Kryshee Frencheska Grengria as Julie
*Richard Quan as Young Angkong Cesar
*Cris Villonco as Young Amah Choleng

=== Flight 666 ===
*Lovi Poe as Karen 
*Matteo Guidicelli as Dave
*Daniel Matsunaga as Bryan 
*John Lapus as Iggy Moda
*Kim Atienza as Kuya Tim 
*Bernard Palanca as Carlos
*Khalil Ramos as Gino
*Kiray Celis as Lovely Bentong as William
*Betong Sumaya as Macoy Nathalie Hart as Pamela
*IC Mendoza as Adam
*Yael Yuzon as Eli
*Ria Garcia as Jane
*John Spainhour as Brandon
*Ken Alfonso as Charlie
*Rocky Salumbides as Aswang the Father of Tiyanak
*Arlene Muhlach as Brandons Girlfriend
*Lui Manansala as Janes Grandmother
*Joy Viado as Stella
*Sue Pardo as Nina

== Production == All of Me" with Venus Raj on YouTube.  The film will also be the acting debut of Yael Yuzon who will be playing a rock star in the "Flight 666" segment. 

The snake-skin costume for the character of Erich measures 20 feet long and takes about five hours to fully apply the costumes prosthetics onto her.  Almost the whole segment of "Flight 666" was shot on board a retired Philippine Airlines Airbus A330 parked at Villamor Airbase.  Lovi Poe, the leading actress of the segment, stated that they filmed it while the plane was flying.  Filming of "Ulam" wrapped on November 1, 2014.   

== Ratings ==
Shake, Rattle & Roll XV is rated PG by the Movie and Television Review and Classification Board. 

== Release ==
=== Box office === 40th Metro Manila Film Festival. 

=== Critical response ===
 

==Accolades==
===Awards and nominations===
{| class="wikitable"
|- style="background:#ccc;"
| Award Giving Body || Award || Recipient(s) || Result
|- 40th Metro Manila Film Festival
|-
| Best Festival Float || Shake, Rattle & Roll XV ||  
|-
| Best Festival Sound Recording || Wild Sound (Shake, Rattle & Roll XV) (Ulam Episode) ||  
|-
| Best Festival Musical Score || Von De Guzman (Shake, Rattle & Roll XV) (Flight 666 Episode) ||  
|-
| Best Festival Child Performer || Kryshee Grengia (Shake, Rattle & Roll XV) (Ulam Episode) ||  
|-
| Best Festival Make Up Artist || (Shake, Rattle & Roll XV) (Ulam Episode) ||  
|-
| Best Festival Visual Effects || Imaginary Friends (Shake, Rattle & Roll XV) (Ahas Episode) ||  
|-
| Best Festival Editor || Benjamin Tolentino (Shake, Rattle & Roll XV) (Flight 666 Episode) ||  
|-
| Best Festival Supporting Actress || Chanda Romero (Shake, Rattle & Roll XV) (Ulam Episode) ||  
|-
| Best Festival Director || Jerrold Tarog (Shake, Rattle & Roll XV) (Ulam Episode) ||  
|-
| Best Festival Actress || Erich Gonzales (Ahas Episode) Carla Abellana (Ulam Episode) (Shake, Rattle & Roll XV) ||  
|}

==See also==
*Shake, Rattle & Roll (film series)

== References ==
 

 
 

 
 
 
 
 
 
 
 
 
 
 