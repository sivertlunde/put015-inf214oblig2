Love Trilogy (film)
{{Infobox Film
| name           = Love Trilogy
| image          = Lovetrilogy(film).jpg
| image_size     = 
| caption        = China version official poster
| director       = Derek Chiu Raymond Wong Peter Chan
| writer         = Aubrey Lam Michael Chow
| music          = 
| cinematography = 
| editing        = 
| distributor    = Mandarin Films China Film Group
| released       = February 5, 2004 (Hong Kong) February 5, 2004 (Peoples Republic of China) May 20, 2008 (South Korea)
| runtime        = 105 min.
| country        = Hong Kong China Mandarin Korean Korean
}} Michael Chow, Lu Yi and Han Xiao. It was directed by Derek Chiu. It has differing titles in China and Hong Kong. 
The China title is Falling in Love with Heaven & Earth (我爱天上人间 / Wo Ai Tian Shan Ren Jian)   . ent.163.com. 9 February 2004. 

==Plot==
Three pairs of lovers arrived at Yunnan at the same time, meets Shibo Yuan tourguide, Liu Hai.

===The Red===
The first pair arriving at Yunnan to tour are from Hong Kong, Mark & his wife Ah Cui. They had been married for seven years, both had been childhood friends, they should travel the road together for the latter half of their lives. Because of the market crash of a year ago, both are busy all day, their feelings for each other are starting to diminish.

===The White===
The second pair arriving at Yunnan to tour are from Shanghai, Xu Jing & Liu Bai-sheng just got married & are on their honeymoon. Bai-sheng is a workaholic, unknowingly neglecting Xu Jing. This new couple status tested by their continued companionship. Xu Jing gave herself a pregnancy test at the clinic in which she works in, found out that shes expecting, thus becoming very worried, the reason being Baisheng still doesnt want to become a father yet.

===The Blue===
The third pair traveling are from South Korea, Song Zi-ming and his girlfriend BoBo are a young couple dating for only about half a year. Zi-ming is romantic, very literary, BoBo is vain but in tune with reality, after detecting they are farther. Zi-ming & BoBo separated in the end, but Zi-ming had wanted to go to Shangrila, so he asks Liu Hai, whether she can continue being his tourguide. Liu Hai agreed to accompany him to Shangrila, to fulfill his wish. Zi-mings appreciation towards her, this wooly insect finally turn into a butterfly?

Seeing these three pair of lovers, miss tourguide, Liu Hai, has her own innate opinions, in the course 2-3 of introducing Shibo Yuan, she has become a "matchmaking|matchmaker" for these three pair of lovers, acting as an intermediary many times, helping them settle their disputes, had them all convinced, but also stirred up a lot of storm.

==Casts==
* Francis Ng - Mark Lao (about 30 years old)
* Anita Yuen - A Cui (28 years old)
* Ruby Lin - Liu Hai (20 years old) Lu Yi - Liu Bai Sheng (26 years old)  Michael Chow
* Han Xiao - Xu Jing (23 years old) Wu Ji Ho (Wu Chih Hao) - Song Zi Ming (20 years old)
* Yin Yi Li - Bobo (23 years old)

==Trivia==
* The film is set and was filmed in Yunnans Shangrila. 
* Director Derek Chiu disclosed this films three love stories divide into "White, Blue, Red" three sections.   . nen.com. 17 February 2004. 
* Cinema Line-Up: Tun Men, Fen Ling, Hua Mao, Ma An Shan

==References==
 

==External links==
* 
*  
* 

 
 
 
 
 
 
 
 
 