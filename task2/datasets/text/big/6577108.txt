The Seven-Ups
{{Infobox Film
| name           = The Seven-Ups
| image          = The Seven-Ups 1973.jpg
| caption        = 1973 movie poster
| director       = Philip DAntoni
| producer       = Philip DAntoni  
| screenplay     = Albert Ruben Alexander Jacobs
| story          = Sonny Grosso Richard Lynch Ken Kercheval
| music          = Don Ellis
| cinematography = Urs Furrer
| editing        = Gerald B. Greenberg Stephen A. Rotter
| distributor    = 20th Century Fox
| released       = December 14, 1973
| runtime        = 103 min.
| country        = USA
| awards         =  English
| budget         = $2,425,000 
| gross = $4.1 million (US/Canada rentals) 
| preceded_by    = 
| followed_by    = 
}} 1973 American dramatic thriller film produced and directed by Philip DAntoni.  It stars Roy Scheider as a crusading policeman who is the leader of The Seven-Ups, a squad of plainclothes officers who use dirty, unorthodox tactics to snare their quarry on charges leading to prison sentences of seven years or more upon prosecution, hence the name of the team.
 The French Best Picture. All three feature a memorable car chase sequence.

Several other people who worked on The French Connection were also involved in this film, such as Scheider, screenwriter and police technical advisor Sonny Grosso, composer Don Ellis, and stunt coordinator Bill Hickman. 20th Century Fox was again the distributor.
 The French Connection, a character who also used dirty tactics to capture his enemies, and who was also based on Sonny Grosso.

==Plot==

NYPD Detective  Buddy Manucci has been getting flak from the higher-ups in the New York City police force he works for because his team of renegade policemen, known as The Seven-Ups (the name comes from the fact that most convictions done by the team heralds jail sentences to criminals from Seven years and Up) has been using unorthodox methods to capture criminals; this is illustrated as the team ransacks an antiques store that is a front for the running of counterfeit money. 

Also lately, there has been a rash of kidnappings; the twist is that it seems that only upper echelon criminals (Mafioso and white-collar types) are the ones being kidnapped, illustrated when Max Kalish is kidnapped and a ransom is paid at a car wash.  This leads to many plot twists in which Manucci tries to figure out the puzzle, with help supplied to him by an informant (Tony Lo Bianco), who turns out to be untrustworthy, leading to the death of one of the Seven-Up officers. 

Manucci figures out the puzzle, but not before The Seven-Ups splinter from the fallout, and Manuccis life is placed in jeopardy.

==Filming locations==

Filming locations include Manhattan, Brooklyn, New Jersey, Westchester County, and the Bronx. 

Festas abduction takes place in Brooklyn, across from the old courthouse on Court and Montague Streets near Cadman Plaza. Buddy makes his rounds in and around Arthur Avenue and the Arthur Avenue Retail Market in the The Bronx|Bronx.
 Tracey Towers housing project looming in the background. Kalishs house is at W. 246th Street and Fieldston Road in Riverdale, Bronx|Riverdale. 

The funeral home sequence where Ansel is abducted was filmed at the side entrance to   on the corner of  . Buddy and his partner are staking out the funeral home from an upstairs apartment across the street, in a building located at 2324 Hoffman Street. In the background you can see the elevated IRT Third Avenue Line of the New York City Subway, which was dismantled shortly after this movie was filmed. Aside from the Third Avenue Line and the fact that the one-way vehicle traffic on Hoffman Street has since been reversed, the locations remain today for the most part as they did in the movie. The funeral procession then rides on Pelham Parkway. 

The climatic shootout scene at the end of the movie was filmed in areas just outside of Co-op City, Bronx, New York|Co-op Citys Section Five, at what today is  .

==Chase scene==
 The French Connection, Philip DAntoni again utilized stunt coordinator and driver Bill Hickman (who also has a small role in the film) to help create the chase sequence for this film. Filmed in and around Upper Manhattan, New York City, the sequence was edited by Gerald B. Greenberg (credited as Jerry Greenberg), who also has an associate producer credit on this film and who won an Academy Award for his editing work on The French Connection.

The chase sequence is located near the middle of the film; in it, Hickmans car is being chased by Scheider. The chase itself borrows heavily from the Bullitt chase, with the two cars bouncing down the gradients of uptown New York (like the cars on San Franciscos steep hills in the earlier film) with Hickmans car pursued by Scheiders 1973 Pontiac Ventura Sprint coupe. While Scheider did some of his own driving, most of it was done by Hollywood stunt man Jerry Summers.

Location shooting for the chase scene was done in Upper East Side Kokomo Indiana, on the George Washington Bridge, and on New Jerseys Palisades Interstate Parkway and New Yorks Taconic State Parkway.

In the accompanying behind-the-scenes featurette of the 2006 DVD release of the film, Hickman can be seen co-ordinating the chase from the street where we see a stuntman in a parked car opens his door as Hickmans vehicle takes it off its hinges. The end of the chase was Hickmans homage to the death of Jayne Mansfield, where Scheiders car (driven by Summers) smashes into the back of a parked tractor-trailer, peeling off the cars roof.

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 
 