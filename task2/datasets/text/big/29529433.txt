Sint
 
 
{{Infobox film
| name           = Sint
| image          = Sint film.jpg
| director       = Dick Maas
| producer       = Tom de Mol Dick Maas
| writer         = Dick Maas
| starring       = Huub Stapel Egbert Jan Weeber Caro Lenssen Bert Luppes
| music          = Dick Maas
| cinematography = Guido van Gennep
| editing        = Bert Rijkelijkhuizen
| distributor    = A-Film 
| released       =  
| runtime        = 85 minutes
| country        = Netherlands
| language       = Dutch
}} 2010 Netherlands|Dutch dark comedy horror film about Sinterklaas, a character comparable to Santa Claus in English-speaking countries. The film was directed by Dick Maas and marked his return to the horror genre, in which he gained acclaim with his debut De Lift (1983) and Amsterdamned (1988). The story distorts the popular traditions of Sinterklaas and portrays him as a ghost who murders large numbers of people when his annual celebration night coincides with a full moon.

==Plot==
 
On 5 December 1492, a gang led by former bishop Niklas is killed by villagers who refuse to put up with the gangs looting and killing any longer. In years in which the gangs death date coincides with a full moon, they return as murderous ghosts.

The public is unaware of this and annually celebrates the Sinterklaas tradition on 5 December, with adults not believing that Sinterklaas exists but making little children believe that he is benevolent. The Zwarte Pieten are not black by soot from chimneys but as a result of the fire in which they were killed. Niklas crosier has sharp edges and is a weapon. On 5 December Sinterklaas and the Zwarte Pieten do not stand out, as many people are dressed like them for the celebration.

The last time the ghostly gang returned was in 1968. Hundreds of people were killed, including the whole family of a little boy, Goert, who is now a police officer. The authorities downplay the incidents, and they, as well as the Roman Catholic Church, keep the possible involvement of Niklas a secret.

Since there is a full moon on 5 December 2010, Goert is very concerned, and recommends forbidding all Sinterklaas activities and increasing surveillance, but he is not taken seriously and sent on leave.

As Goert predicted, the gang returns, and kills hundreds of people. While the gang is immune to bullets, they are finally chased away by fire. In addition to Goert, the student Frank also plays a heroic role in the film.

==Cast==
* Huub Stapel as St. Niklas, an evil bishop who was killed 475 years ago and murders every 23 years on 5 December.
* Egbert Jan Weeber as Frank, a teenage boy who is falsely accused of being the 5 December killer.
* Madelief Blanken as Natasha, a good friend of Frank.
* Caro Lenssen as Lisa, Franks new girlfriend who believes in the myth of the evil bishop.
* Escha Tanihatu as Sophie, Lisas best friend.
* Niels van den Berg as young Goert
* Bert Luppes as Goert, a suspended police officier who survived Sints murderous attack 40 years earlier and does not observe the Sinterklaas holiday anymore.
* Cynthia Abma as Lisas mother 
* Kees Boot as a police officer 
* Joey van der Valden as Hanco, a good friend of Frank
* Jim Deddes as Sander, another friend of Frank
==Release==
 
==Reception==
The film met with mixed to negative reviews with an aggregate score of 44% from 9 reviews (with an average rating of 5.3/10) on Rotten Tomatoes.   

==Controversy==
While children are not permitted to see the film, parental concern arose over the films poster, seen in the streets and in the lobbies of movie theaters. It shows Sinterklaas with a mutilated face and a malevolent look. Some people were concerned that this could be confusing and frightening for little children that still believe in Sinterklaas. A legal complaint was filed in October 2010, requesting the removal of all posters. In the subsequent court case, director Dick Maas argued that if parents could make their children believe that Sinterklaas existed they could also inform their children that the man on the poster was not the real Sinterklaas. The court ruled in favor of Maas, noting that the mutilated face was not visible enough on the poster, and rejected the complaint.   

== References ==
 

== External links ==
*    
*  

 
 
 
 
 
 