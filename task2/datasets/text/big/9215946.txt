Movie Movie
{{Infobox film
| name           = Movie Movie
| image          = Moviemovie.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Stanley Donen
| producer       = Stanley Donen
| writer         = Larry Gelbart Sheldon Keller Barbara Harris
| music          = Ralph Burns
| cinematography = Charles Rosher Jr. Bruce Surtees
| editing        = George Hively
| studio         = ITC Entertainment
| distributor    = Warner Bros.
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 double bill musical comedy film|comedy, both starring the husband-and-wife team of George C. Scott and Trish Van Devere. A fake trailer for a flying-ace movie set in World War I entitled Zero Hour (also starring Scott) is shown between the double feature.
 Barbara Harris and Ann Reinking featured in one each. The script was written by Larry Gelbart and Sheldon Keller.

==Plot summary==
At the start of the film, George Burns tells us that we are about to see an old-style double feature.  In the old days, he explains, movies were in Black and white|black-and-white, except sometimes "when they sang it came out in color."

===Dynamite Hands===
 boxer to raise the money to have her cured.  Along the way, he gets seduced by fame and fortune, and runs afoul of a crooked boxing manager.  In the end, his sister is cured and Joey, so that "poetic justice could be served," races through law school to become the prosecutor who puts the villain behind bars, spouting corny courtroom aphorisms such as "a man can move mountains with his bare heart."

===Baxters Beauties of 1933===
 Broadway smash. drunkenness and reckless spending of the shows money. In the end Kitty must go on in Isobels place. Kitty becomes a star, and learns that Baxter is her long-lost father.  As the curtain falls, a dying Baxter tells her, "One minute youre standing in the wings, the next minute youre wearing em."

==Cast==
* George C. Scott - Gloves Malloy / Spats Baxter
* Trish Van Devere - Betsy McGuire / Isobel Stuart
* Red Buttons - Peanuts / Jinks Murphy
* Eli Wallach - Vince Marlow / Pop Rebecca York - Kitty
* Harry Hamlin - Joey Popchik
* Ann Reinking - Troubles Moran
* Jocelyn Brando - Mama Popchik / Mrs. Updike
* Michael Kidd - Pop Popchik
* Kathleen Beller - Angie Popchik
* Barry Bostwick - Johnny Danko / Dick Cummings
* Art Carney - Doctor Blaine / Doctor Bowers
* Clay Hodges - Sailor Lawson
* George P. Wilbur - Tony Norton
* Peter Stader - Barney Keegle (as Peter T. Stader)
* Jimmy Lennon Sr. - The Announcer (as James Lennon) Barbara Harris-Trixie Lane Charles Lane-The Judge-Mr.Pennington

==Release==
In the theatrical release, as George Burns leads us to expect in the films prologue, Dynamite Hands and the mock film trailer were in black-and-white, while the musical Baxters Beauties of 1933 was in color.  Some home video editions featured the original color version of Dynamite Hands which was printed on black and white film stock during its theatrical release.

Lew Grade liked the movie so much that he commissioned Larry Gelbart to write a sequel. However the movie failed at the box office. Grade blamed poor distribution from Warner Bros. Lew Grade, Still Dancing: My Story, William Collins & Sons 1987 p 251 

==Awards==
Movie Movie earned three Golden Globes nominations, for Scott, for Hamlin and for Best Picture / Musical or Comedy.

Gelbart and Keller won the 1979 Writers Guild of America award for Best Comedy Written Directly for the Screen.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 