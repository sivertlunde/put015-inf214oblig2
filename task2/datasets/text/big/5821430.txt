Through the Looking Glass (film)
{{Infobox film
| name           = Through the Looking Glass 
| image          = File:ThroughTheLookingGlass1976eroticfilmposter.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Original theatrical release poster
| film name      = 
| director       = Jonas Middleton
| producer       = Jonas Middleton
| writer         =
| screenplay     = Ron Wertheim,  David Maryla,  Jonas Middleton
| story          = David Maryla,  Jonas Middleton
| based on       =  
| narrator       = 
| starring       = Catharine Burgess, Jamie Gillis, Laura Nicholson 
| music          = Harry Manfredini,  Arlon Ober
| cinematography = João Fernandes (as Harry Flecks)
| editing        = James Macreading,  Maurizio Zaubmann	
| studio         = Mastermind Productions
| distributor    = Mature Pictures Corp., VCX, Video-X-Pix
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 1976 adult film with avant-garde, experimental elements directed by Jonas Middleton.   The film stars Catherine Erhardt (billed as "Catharine Burgess") as a bored socialite that finds herself drawn to a mirror that ignites her sexual desires.  Through The Looking Glass has themes of incest and the traumatic effects it has on those who have experienced it.  The film also holds scenes of violence and sexual violence, as well as presenting the sexual content in an artistic as opposed to erotic manner.

==Synopsis==
Catherine (Catherine Erhardt) is a sexually unfulfilled socialite that longs for the memory of her father. She spends much of her time in the attic, masturbating in front of a gothic mirror that reminds her of her childhood and teen years with her father (Jamie Gillis). During one of her daily visits she encounters the ghost of her father in the mirror, who masturbates her and draws her into the mirror to witness several sexually charged scenarios. These pique her interest and eventually culminate in a scene where Catherine witnesses her teen self semi-reluctantly, then enthusiastically, take part in an incestuous encounter with her father. After her father is finished with the encounter, he remarks to Catherine that this was what she wanted (hinting that the sexual encounter and history of incest may or may not have been a false memory). Before returned to reality, Catherine realizes that this is not her father but a demonic figure. She tries to deny his invitation to come into the mirrors realm as a permanent resident but the demon tells her that he knows that she will return as she always have because she is bored with the world and with her husband. He tells her to return to the mirror at 1 am, but only after fulfilling a few conditions. She must not only throw all of her jewelry away, but she must also allow her daughter Jennifer to have unconditional access to the room so that the demon can watch her just as he watched Catherine grow. This upsets Catherine, as she had previously forbid her daughter from entering the room and did not want to allow the demon access to Jennifer. She tries to get out of the late night encounter by trying to leave the house and persuade her husband to take her somewhere, but is unsuccessful. Later that night Catherine returns to the room and in a dream-like state begins to masturbate. The demon exits the mirror and while initially languid upon his approach, Catherine begins to struggle against his advances. He then violently rapes Catherine, who screams, which awakens the rest of the house. She eventually passes out, awakening in a horrific world where people perform the most depraved degradation to themselves and one another. Catherine is horror struck to realize that rather than the lavish world that had been promised to her, the demon has tricked her and that she is there because she chose to focus on sexual illusions and fulfilling her deepest desires rather than try to interact more with the world around her and improve herself as a person. She discovers her father among one of the souls in the sexual hell, which further terrifies her. Dodging the many people trying to sexually assault her, Catherine tries to escape but is unable to and succumbs to the madness of the hell. The film ends with her daughter sitting in front of the mirror, becoming just as enraptured with it as her mother was.

==Cast==
*Catherine Erhardt as Catherine (Billed as Catharine Burgess) 
*Jamie Gillis as Demon / Father
*Laura Nicholson as Jennifer
*Kristen Steen as Young Catherine (as Marie Taylor)
*Douglas Wood as Richard
*Kim Pope as Ann
*Eve Every as Lilly
*UltraMax as Mrs. Manchester
*Roger Caine as Abel (as Mike Jefferson)
*Nancy Dare as Karen (as Suzan Swanson)
*Terri Hall as Lisa
*Jeffrey Hurst as Mr. Manchester
*Rocky Millstone as Eugene (as Jacob Pomerantz)
*Grover Griffith as Fat Man
*Victoria Karl as Beauty Queen

==Release history==
While filming Through the Looking Glass, Middleton shot three different versions of the film (two soft core, one hard core) in order to market the film to different audiences.    The film played in American and European art houses, and in its original run at New Yorks infamous World Theater, the theater screened a Warner Bros. cartoon before the feature film. It achieved some small notoriety in the New York papers at the time because it featured an underage soap opera actress in a small, non-sex role, as the original poster named Nicholson as a "14 year old Actress|starlet". The line was removed from all future posters.

Through the Looking Glass has since been shown at some film festivals, such as the 2013 In The Flesh film festival. 

==Book tie-in==
A novelization of the films script, written by Eileen Lottman under pseudomyn of Molly Flute, was published through Dell Publishing.  Largely similar to the film, the novel elaborates on the movies story line, elaborating on the mirrors history within Catherines family and heavily implying that her mother was spirited away through the mirror like Catherine was. The book also takes away the ambiguity of whether or not Catherine was molested, outright stating that the sexual scene between Catherine and her father occurred. The book also affirms at the end that Catherines daughter Jennifer has been caught up in the mirrors spell and will eventually end up in the same sexual hell and in many of the same situations that Catherine herself was in, perpetuating the cycle that has been in place throughout Catherines family. 

==See also==
*Golden Age of Porn

==References==
 

==External links==
* 

 
 
 
 
 