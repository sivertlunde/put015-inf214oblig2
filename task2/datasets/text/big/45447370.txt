The Night Angel
{{Infobox film
| name           = The Night Angel
| image          = The Night Angel poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Edmund Goulding
| producer       = 
| screenplay     = Edmund Goulding 	
| starring       = Nancy Carroll Fredric March Phoebe Foster Alison Skipworth Alan Hale, Sr. Hubert Druce Katherine Emmet
| music          = 
| cinematography = William O. Steiner
| editing        = Emma Hill
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

The Night Angel is a 1931 American drama film written and directed by Edmund Goulding. The film stars Nancy Carroll, Fredric March, Phoebe Foster, Alison Skipworth, Alan Hale, Sr., Hubert Druce and Katherine Emmet. The film was released on July 18, 1931, by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=9506E6D7153BE433A25752C1A9609C946094D6CF|title=Movie Review -
  The Night Angel - THE SCREEN - NYTimes.com|work=nytimes.com|accessdate=19 February 2015}}  
 
==Plot==
 

== Cast ==
*Nancy Carroll as Yula Martini
*Fredric March as Rudek Berken
*Phoebe Foster as Theresa Masar
*Alison Skipworth as Countess von Martini
*Alan Hale, Sr. as Biezel
*Hubert Druce as Vincent
*Katherine Emmet as Mrs. Berken
*Otis Sheridan as Schmidt
*E. Lewis Waller as Kafka
*Clarence Derwent as Rosenbach
*Charles Howard as Clown
*Doris Rankin as Matron
*Francine Dowd as Mitzi
*Cora Witherspoon as Head Nurse
*Francis Pierlot as Jan 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 