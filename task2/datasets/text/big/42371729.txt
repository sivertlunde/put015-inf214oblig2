Puthir
{{multiple issues|
 
 
 
}}
{{Infobox film
| name = Ajeya
| image =
| caption =
| director = Siddalingaiah
| writer = Kanmani Subbu (dialogues)
| story = B. L. Venu
| producer = Siddalingiah,   SD Murali Murali   Sandhya   Ashalatha
| music = Ilaiyaraaja
| cinematography = V. K. Kannan
| editing = R. Rajan   R. Rajashekar
| studio  = Kamadhenu Art Productions
| released = 1986
| runtime = 130 min
| language = Tamil 
| country = India
}}
 Murali in his first dual role. The film was a remake of directors own Kannada film Ajeya. Murali acted in Kannada version too.

==Plot==
Anuradha is a wealthy and spoiled teenage girl who is raised by her step-mother, while her father died under mysterious circumstances. Murali appears as a poor educated youth who happens to save her from a group of vagabonds. She, however, constantly gets into fights with Murali, which eventually leads to the two falling in love. Their love is met with many obstacles, as Anuradhas scheming uncle and cousin (Chinni Jayanth) try to gain all of Anuradhas wealth. Murali, on the other hand, has to deal with Chinni as rivals for Anuradhas love. Towards the end as everything seems to go right for the couple, a sudden plot twist occurs when a second Murali enters the picture and claims himself as the true love of Anuradha. Will Anuradha be able to sort out the identity of the real Murali? Watch the movie to find out.

==Cast== Murali
*Sandhya
*Ashalatha
*Kathiravan
*Chinni Jayanth
*Delhi Ganesh

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Hero Hero || Yesudas || Ponnaruvi || 04:17
|-
| 2 || Katta Nalla || Malaysia Vasudevan || Gangai Amaran || 04:24
|-
| 3 || Muthal Mutham || Yesudas, Janaki || M. Metha || 04:30
|-
| 4 || Unna Pol Manmathan || Janaki || Gangai Amaran || 04:27
|-
| 5 || Ellaam Theriyum || Yesudas || Vairamuthu || 04:26
|-
| 6 || Theme Music || Instrumental || Illayaraja || 04:27
|}

== References ==
 

==External links==
* 

 

 
 
 
 
 
 
 
 