Govindudu Andarivadele
 
 
{{Infobox film
| name = Govindudu Andarivadele
| image = Govindudu Andarivadele poster.jpg
| caption = Theatrical release poster Krishna Vamsi
| producer = Bandla Ganesh Krishna Vamsi, Paruchuri Brothers Srikanth Kajal Aggarwal Kamalinee Mukherjee
| cinematography = Sameer Reddy
| music = Yuvan Shankar Raja
| editing = Naveen Nuli
| studio = Parameswara Art Productions
| country = India
| released =  
| runtime = 149 minutes 
| language = Telugu
| budget =     
| gross =   
}}
 Telugu family Krishna Vamsi Rahman and soundtrack and score while Sameer Reddy worked as the cinematographer.

The film is partially inspired by the 1991 Telugu film Seetharamaiah Gari Manavaralu directed by Kranthi Kumar. Govindudu Andarivadele portrays a "non-residential Indian" named Abhiram who visits his grandfather Balarajus house as a student of agriculture. He actually came to reconcile the differences between his father, Chandrasekhar Rao, and Balaraju. The pair parted ways as Chadrasekhar went to the US while Balaraju stayed and built a charitable hospital for local people. Abhiram succeeds in winning over family members, and Balaraju understands the truth behind Abhiram and his attempts.
 Tamil as Ekalavya and Ram Leela respectively.

== Plot ==
Balaraju, the head of his village, lives with his wife Baby, his two sons; Chandrasekhar Rao and Bangari, and his two daughters. He helps Chandrasekhar become a doctor and builds a local hospital. On the day it opens, Chandrasekhar comes back home with his lover Kausalya, also a doctor. Both express their wish to marry and settle abroad, which upsets Balaraju, leading to their separation.

25 years later, Chandrasekhar, a successful doctor in London, tells the story to his son Abhiram and daughter Indu. Abhiram decides to go home to Balaraju and attempt a reconciliation. He meets his friend, Bunny, at the airport and goes to the village on Bunnys bike the next day. On the way, he witnesses a cockfight organised by Bangari and Baachi; he gets the help of Balaraju to pardon Bangari and get Baachi arrested. Abhiram then introduces himself as a student from London who came here to learn agricultural practices and martial arts. He particularly impresses Balarajus family when he saves a childs life and so is allowed to stay with them.

Balarajus granddaughter Satya comes back from Hyderabad. Abhiram is surprised to see Satyas cultured behavior and traditional attire, as Bunny and Abhiram previously met her in a pub in Hyderabad. She is equally surprised to see him; his cell phone contains photos showing how she spent her time at the pub. Abhiram blackmails Satya with the photos,but she then asks Bangari to get Abhirams cell phone by telling him that Abhiram is blackmailing her. Their collective efforts fail, and Baachi is also dragged into the affair.

A fight happens between Abhiram and Baachi, deepening their rivalry. To get rid of Bangari, Balaraju arranges Chitras marriage. Satya tells Bangari about this who then kidnaps Chitra. Abhiram chases him, saves Chitra and gets Bangari arrested. Satya finds Abhirams phone and finds out his true identity. Abhiram offers to help Balaraju renovate the hospital he built with his fathers asssistance; Balaraju accepts the offer. Abhiram finds out that Satya has his phone when he goes to call his father. She deletes the photos of her. After learning that she knows the truth, Abhiram does a deal with her. They fall in love with each other.
 Special Economic Zone manufacturing beer, release Bangari from jail. Abhirams father sends the advanced equipment to Hyderabad, which Bunny and Bangari receive. Bangari attacks Bunny and seizes the equipment. Abhiram stops Bangari and his men and reveals his identity. Bangari realises what he has done and reconciles with Balaraju. The equipment is unloaded at the hospital.

Indu too visits Balarajus house. Satya gets engaged to an American NRI doctor. However, she tells Abhiram that she would die if she doesnt marry him. He decides to break up with her and upon hearing this, Bangari reveals Abhiram and Indus identity to Balaraju, who orders Abhirams and Indu to leave the house.

Later, Abhiram gets a phone call from his father who plans to come back to the village. Suddenly, Baachi kidnaps Indu. An injured Abhiram manages to save her, but gets shot by Baachi. Bangari arrives and Abhiram prevents Baachi from getting harmed by saying that Baachi too is a family member. Chandrasekhar comes to the hospital and operates on Abhiram. When Abhiram regains consciousness, Balaraju welcomes him, Indu and Chandrasekhar back into the family. The film ends with Abhiram getting married to Satya and Bangari to Chitra, also coinciding with Balaraju and Babys anniversary.

== Cast ==
;Principal cast
*Ram Charan as Abhiram.  Srikanth as Bangari. 
*Kajal Aggarwal as Satya. 
*Kamalinee Mukherjee as Chitra. 
*Prakash Raj as Balaraju. 
*Jayasudha as Baby.  Rahman as Dr. Chandrasekhar Rao. 

;Supporting cast
 
*Adarsh Balakrishna as Baachi. 
*Vennela Kishore as Bunny. 
*Rao Ramesh as Rajendra.  Ravi Prakash as Baachis brother. 
*Kota Srinivasa Rao as Balarajus brother-in-law. 
*Harsha Vardhan as Chandrasekhars friend.  Paruchuri Venkateswara Rao as Bangaris uncle. 
*Posani Krishna Murali as a lawyer engaged by Balarajus brother-in-law.  Pragathi as Satyas mother. 
*Ayesha Kaduskar as Indu, Abhirams sister. 
*Kasi Vishwanath as Kasi, Satyas father. 
*Satya Krishnan as another aunt of Abhiram. 
*Sameer as the father of the child whose life Abhiram saves. 
*Bhanu Sri Mehra as Kausalya Chandrasekhar Rao (Cameo appearance). 
*Srinivas Avasarala as Dr. N. Raju (Cameo appearance). 
 

;Bangaris gang members
*M. S. Narayana. 
*Peela Gangadhar. 
*Harsha Chemudu. Govindudu Andarivadele. (DVD). Character introduced at 28:31 
*Giridhar. 
*Raghu Karumachi. 
*DV Artist. 

== Production ==

=== Development ===
  Krishna would act in a film directed by Vamsi and produced by Bandla Ganesh under the banner Parameswara Art Productions banner.  At that point, however, the script was not yet complete and so the project was still put on hold. 
 Sri Lakshmi Narasimha Swamy Temple in Antarvedi with the finished script.  The film was initially titled Vijetha after the 1985 Telugu film which featured Chiranjeevi and Bhanupriya in the lead roles.  In March 2014, the supposed story of the film was leaked onto the internet. The story was that Ram Charan played the role of an NRI who visits his joint family in order bridge a gap between his father and his fathers younger brother. 
 Tamil film. 

=== Casting ===
{{multiple image
 
| align     = right
| direction = vertical
| footer    = Tamannaah (top) was initially approached for the female lead role, but Kajal Aggarwal (bottom) ended up being engaged for the role due to the lack of the formers dates.
| width     =

 
| image1    = Tamannaah.jpg
| width1    = 150
| alt1      = 
| caption1  =

 
| image2    = Kajal Aggarwal 3.jpg
| width2    = 150
| alt2      = 
| caption2  =
}} Venkatesh and Krishna were initially set to play the lead roles along with Charan,  with Venkatesh playing the role of Charans paternal uncle.  In September 2013, Kajal Aggarwal was signed to play opposite Charan.  Actor Jagapati Babu turned down the role of Charans father as he wanted to concentrate on antagonistic character roles. 

In November 2013, Tamil actor Rajkiran was chosen to play the role of Charans grandfather, previously earmarked for Krishna.  The producers also began searching for a new actress in the lead role.  In December 2013, Venkatesh left the project and was subsequently replaced by Telugu actor Meka Srikanth|Srikanth, saying that the role did not suit his image rather than any other dissatisfaction.  Chandini Choudary was rumoured to have been signed up as the female lead, but this proved not to be the case.  Tamannaah was approached as well, but she had prior commitments in Aagadu and Baahubali. Kajal was finalised by the end of 2013 as Charans heroine in this film, marking her return to Telugu Cinema after a brief hiatus. 

Kamalini Mukherjee was chosen by Krishna Vamsi in late January 2014 by Krishna Vamsi, mainly because of her appearance and her acting talent.  A song from rock band "The Tapes" took seven to nine hours to film for a sequence in the film featuring Charan and Kajal. This was the bands first onscreen appearance.  Vennela Kishore confirmed his presence in the film twice through his Twitter account.  
 Rahman were signed for key roles.       M. S. Narayana was selected to play a supporting role.  Adarsh Balakrishna was selected to portray the films antagonist, recommended by Srikanth to Vamsi.  

=== Characterisation and costume designing ===
 
Ram Charans character was Abhiram, an NRI who goes from London to India in search of his roots.    He was depicted as the star player of a Rugby League player at Hemel Hempstead  Charan grew a ponytail  and appeared in traditional Telugu costume, wearing a dhoti for some scenes in the film, contrasting with the modern clothes he wore in previous films.  He also learnt stick fighting skills for the film. 

Vamsi showed Charan a lot of family dramas, drove him to villages to help him understand the atmosphere and mindset of the people. According to Vamsi, Charan "comes across as a shy and reserved guy but has a fun and family loving side to him", which he tried to show onscreen.  Srikanth played the role of Charans younger paternal uncle and wore his hair long for the role.  His costume was rustic in appearance. A still of him in costume was shown in the end of July 2014.  His "crucial role" in the film was aggressive and rebellious.   

Kajal Aggarwal revealed one of her outfits in the film on 27 July 2014 would be a black half-sari.  Jayasudha was selected to play the role of Prakash Rajs wife in the film while Rahman was signed in to play the role of Ram Charans father.   Kamalini Mukherjees character was named Chitra, a rural girl quite unlike the urbane roles she had played in the past.    Adarsh Balakrishna, cast as a flashy Indian Parliament|M. P.s son living in the same village, wore jewelry with Indian costume, again with long hair. 

=== Filming ===
{{multiple image
 
| align     = right
| direction = vertical
| footer    = (top to bottom) The film was shot in Rameswaram, Nagercoil and Pollachi, finishing in late May 2014.
| width     =

 
| image1    = Rameswaram temple (11).jpg
| width1    = 100
| alt1      = 
| caption1  =

 
| image2    = Green Is Nagercoil.jpg
| width2    = 100
| alt2      = 
| caption2  =

 
| image3    = Busstandpollachi.JPG
| width3    = 100
| alt3      = 
| caption3  =
}} Hyderabad and continued for 3 days there before a long pre-planned schedule started in Rameswaram, Nagercoil and Pollachi.  Kajal joined the sets of the film in Hyderabad on 9 February 2014 and some scenes between herself and Charan were shot there.  They also participated in the films shoot at Nagercoil near Kanyakumari in mid February 2014 after the completion of the schedule at Rameswaram.  A fight sequence with Charan, Srikanth and others was filmed at Kanyakumari before shifting to Pollachi.  A few action sequences focusing Charan were shot at Pollachi in late March 2014.  He also worked on some key action sequences for the film under the supervision of Ram Lakshman in mid May 2014, for which a special set was built.  Some song sequences were shot in Malaysia from 2 June 2014.  
 Ramanaidu Cine Village in Hyderabad.   30 scenes and 2 songs were shot in that house set in Hyderabad over a period of 45 days.  Prakash Raj allotted bulk dates and the reshoot took 8 days only and not 20 days as earlier reported.    After completing the shoot of few scenes and a montage song, the first part of the films Hyderabad schedule ended on 18 June 2014. The next part commenced on 21 June 2014.  At the end of June 2014, another song was shot on a special set featuring mirrors, in which the lead pair participated.  Due to incessant rain at Pollachi, the shooting continued at Hyderabad, delaying the planned schedule.  The Pollachi schedule therefore began on 23 July 2014,  lasting for 5 days.  A fresh schedule began in Karaikudi on whose completion, filming continued in Hyderabad from 4 August 2014.  The films shoot was temporarily halted on 19 August 2014 because of the statewide survey in Telangana. 
{{multiple image
 
| align     = left
| direction = horizontal
| footer    = (left to right) The film was shot on a specially-built set at Ramanaidu Studios, in London and in Petra, Jordan, finishing in late September 2014. The majority of the film was shot on the house set, while a quarter of the film, including two songs, were shot in the other two places.
| width     =

 
| image1    = GAV house set.jpg
| width1    = 125
| alt1      = 
| caption1  =

 
| image2    = London from a hot air balloon.jpg
| width2    = 110
| alt2      = 
| caption2  =

 
| image3    = Petra-4.JPG
| width3    = 100
| alt3      = 
| caption3  =
}}
The next schedule began in London on 22 August 2014.  A romantic song was shot on Charan and Kajal in early September 2014 in Jordan.  After its completion, a song was shot with the lead pair in London. The production unit returned to Hyderabad on 14 September 2014.  A quarter of the film was shot in London. Charans introduction scene was shot at the Pennine Way Stadium, and a rugby match was shot at the Rugby League club in Hemel Hempstead in Hertfordshire in south-eastern England. To add some authenticity, the local club captain B. J. Swindells was brought on board.    Two songs sequences were shot simultaneously in Hyderabad.   

   The principal photography ended on 22 September 2014 on completion of the songs shoot.    However, it was officially announced later that the film would be released without a song which would be shot on 2 and 3 October and then be added to the film. 

=== Post-production === dubbing activities commenced at Shabdalaya Studios in Hyderabad on 18 July 2014. The supporting cast then dubbed their respective roles.  Yuvan Shankar Raja started re-recording for the films first half on 26 August 2014 at his studio in Chennai.  Srikanth completed dubbing for his role on 10 September 2014  and Charan completed dubbing on 24 September 2014.  Chiranjeevi personally monitored the post-production works and promotion strategies.  In an interview with the Deccan Chronicle, Chiranjeevi mentioned that Charan also participated in the production and editing of the films trailer and took extra care of the trailers release and had asked him to check if everything was right.  While shooting two songs at Hyderabad, the post production works were completed. 

   A copy of the film was sent to Central Board of Film Certification on 26 September 2014.  The same day, the film was awarded an U/A certificate instead of a clean U due to some sections and scenes between the lead actors.  The Board asked the makers to obtain a No Objection Certificate from Animal Welfare Board of India in order to retain scenes featuring animals. They muted a few dialogues as well as "backless" views of the heroine. They also insisted on obscuring the brands on alcoholic drinks as well as the display of mandatory warnings "when Srikanth is smoking". 

== Themes and influences ==
Ram Charan said in an interview that the film was partially inspired by Seetharamaiah Gari Manavaralu (1991),  although there were also remarks about tracing inspiration from Vamsis previous works, Ninne Pelladata (1986) and Murari (film)|Murari (2001). Critics also compared the film to other family dramas like Brindavanam (film)|Brindavanam (2010) and Attarintiki Daredi (2013).    
 Telugu films.   

== Music ==
 
Yuvan Shankar Raja composed the music for the film, which marked his first collaboration with Vamsi; it was also the first time he scored for a film starring Srikanth and Ram Charan.  The soundtrack, consisting of six songs,  was released on 15 September 2014 in Hyderabad.  The audio rights were purchased by Aditya Music.  The soundtrack received positive reviews from critics. 

== Release == Tamil as Ekalavya and Ram Leela respectively.  

=== Distribution ===
In mid-July 2014, the distribution rights of the film in the Ceded region were sold for approximately   including prints & publicity costs which, as of March 2015, is the highest amount for a Ram Charan film.   The Nellore region rights were sold to Hari Pictures for a record price of  .  Asian Movies acquired the films overseas distribution rights and released it in collaboration with CineGalaxy, Inc.  Aakash Movies distributed the film in the UAE along with Ravi Tejas Power (2014 Telugu film)|Power.  Errabus distributed the film in the United Kingdom.  Bandla Ganesh himself distributed the film in the Nizam and Krishna regions. 

=== Marketing ===
 , where the films theatrical trailer was unveiled along with the soundtrack album and their video promos on 15 September 2014.]]
The first-look posters and stills of the film were unveiled on 26 March 2014 and 27 March 2014.  On 26 March 2014, 4 stills of Charan from the film were released into the Internet, coinciding with his birthday on 27 March 2014  and receiving a positive response.   The next day, posters designed by Working Title featuring the logo and 3 of those 4 stills were released.  The posters also received a positive response and were widely distributed on various social networking sites.  The films teaser was initially planned to be launched on 29 July 2014 at Ramanaidu Studios, but the release was postponed to 7 August 2014. A promotional event was planned at Ramanaidu Studios in Nanakramguda for the teasers launch.  

The first teaser of 40 seconds was launched with a press conference at the house set in Ramanaidu Studios where the film was shot.   The teaser received very good response from  the critics and the viewers.  Reviewing the teaser, Nivedita Mishra of Hindustan Times wrote "Make no mistake, this one is no poor regional cousin of Bollywood. Its got color, good-looking stars with baddies to boot and loads of song and dance executed with a finesse that only Indian film industry can. The teaser of Ram Charan Tejas new film Govindudu Andarivadele is out and proves just that. From lush paddy fields to festive Indian family lives, this one has it all. Watch out for a shot of Ram Charan Teja riding a bullock cart! Quite novel!".  The Times of India wrote "the teaser looked colorful and quite impressive reminding Krishna Vamsis style of family and love entertainers".  Subramaniam Harikumar of Bollywood Life wrote "The teaser sure has raised out hope. A lot of importance seems to be given to the rustic village backdrop with green fields and bullock carts and big ancestral bungalows. The cinematography is top notch with colours melting in your eyes in every frame. The teaser gives equal importance to family, tradition and romance, with all the important characters getting a decent exposure in the promo."  The films teaser received more than 300,000 views within 24 hours of its online release on YouTube.  A set of stills were released in the second week of September 2014. 

The films official trailer was launched on 15 September 2014 at the Shilpakala Vedika along with the films soundtrack. The trailer too received positive response.  As a part of the films promotion, Bandla Ganesh booked 860 spots on the Telugu news channels for airing the films teaser and trailer until 2 October 2014. Out of them 70 spots each were booked on TV5, TV9, and Sakshi, while 50 spots each were booked on 14 other Telugu news channels.  Post release, Ram Charan and Kajal participated in a special promotional program title Radhe Govinda hosted by Anasuya where they spoke about the film as a part of the films promotion.  As a marketing strategy, the makers planned to add the song Kokkokkodi to the film being screened in theaters to boost the takings.   

=== Home Media ===
Gemini TV acquired the films satellite rights for an amount of  .  The film had its worldwide Television premier on 21 March 2015 on the eve of Ugadi.  The films DVDs and VCDs were released and marketed by Sri Balaji Movies on 31 March 2015. 

== Reception ==

=== Critical reception ===
{{multiple image
 
| align     = right
| direction = vertical
| footer    = Both Charan (top) and Prakash Raj (bottom) won critical acclaim for their performances in their respective roles of a London based NRI and his grand father.
| width     =

 
| image1    = Ram Charan at Promotion of Zanjeer on Jhalak Dikhhla Jaa.jpg
| width1    = 150
| alt1      = 
| caption1  =

 
| image2    = Prakashraj Bhung.jpg
| width2    = 150
| alt2      = 
| caption2  =
}}
The film received generally positive reviews from critics.   Sangeetha Devi Dundoo of The Hindu wrote "Filmmakers hope the family audience will troop in, connect with a thought, a dialogue, a character or a bond between the on-screen family members, have a good laugh, shed a few tears and get their money’s worth. An old-fashioned story told reasonably well is enough to put a smile on many faces. Krishnavamsi walks this predictable path in Govindudu Andarivadele" and added "This oft-repeated tale, has shades of several family dramas told earlier in Telugu, Tamil and Hindi cinema. Yet, Krishna Vamsi makes it his own in the way he narrates it".    Sandhya Rao of Sify wrote, "Overall Govindhudu Andhari Vadele is a neat family drama with the right dash of emotions. The first half is okay, but the second half surely does elevate the film to a different level. Watch GAV this festive season."   
 Oneindia Entertainment wrote "Govindudu Andarivadele is a must watch family entertainer with emotion, action, family bonding and love. The movie has included everthing which should be in a family drama. GAV will be a grand treat this festival season" and rated the film 3.5 out of 5.      IndiaGlitz wrote "A nice blend of sentiment and comedy apart, Govindudu Andarivadele packs the punch by presenting a different Ram Charan. Krishna Vamsi shows yet again that a director with a knack for varied themes can always spring up surprises by coming back in form - that too with a bang. His screenplay lives up to his own set standards, and goes beyond to unleash an actor Charan as against the star Charan" and rated the film 3.5 out of 5.     

Karthik Pasupulate of The Times of India gave the film 3 out of 5 and said, "The film has some moments that stand out like Ram Charans introduction on the rugby field, a couple of well picturised songs, a few smart one-liners and a couple of profound musings on the importance of family and overly color graded village imagery", before concluding that "the movie does offer   some feel good melodrama and production values that are more rich than original."      Suresh Kaviyarani of the Deccan Chronicle rated the film 3 out of 5 and said, "You can watch it once for Charan’s performance and it is a clean family drama. This is a holiday time and it may work out for this film."      Behindwoods wrote "This family entertainer does bring a glow in our faces and take us on a nostalgic trip of vacations we had with our joint families. Even though the proceedings of the film and the song placements hinder the progress at times, overall, the screenplay takes the film to the finish line. The story might lack an equally powerful antagonist, but there is nothing obvious about the movie that goes wrong" and rated the film 2.75 out of 5, summarising the film as an age old story that still works.     

In contrast, Subramanian Harikumar of Bollywood Life gave a mixed review saying, "Govindudu Andarivadele is an earnest attempt to dish out a clean family entertainer. But alas it’s cliche ridden plot pulls it down." He felt that the film was pleasing to the eyes but lacked novelty in its novelty in story and treatment, giving the film a rating of 2.5 out of 5.  Haricharan Pudipeddi, writing for Indo-Asian News Service|IANS, gave a mixed review saying, "Govindudu Andarivadele displays Telugu filmmakers reluctance to dig deep within a genre. Theres enough material readily available from our own lives for an engaging family tale, but rarely do we come across anything realistic." and gave the film a rating of 2 out of 5.     

=== Box office ===

==== India ====
Govindudu Andarivadele took a record opening at the box office. The film grossed approximately   in both  , which grossed   in the region.    According to trade reports, the film amassed approximately   in Nizam while the Ceded region registered around  . Collections in the East Godavari and Guntur regions were around   and   respectively.  According to trade analyst Trinath, the film collected a total of   in India alone on its release day.  The film collected a share of   in five days at the AP/Nizam region.  The film managed to gross a total of   at AP/Nizam region by the end of its first week run.  This took the first week worldwide collections to approximately   share. The film had the third-highest AP/Nizam share as well as having the top worldwide week one share, surpassing Seethamma Vakitlo Sirimalle Chettu (2013) and Race Gurram. 

The films collections slowed down on its eighth day, collecting   at Nizam box office alone. This took its nine-day worldwide collections to  .  On the next day, there was a drop of nearly 50% in the films collections when compared to the release day because of new releases in many theatres. On that day, it collected approximately   at the worldwide box office.  It witnessed subsequent growth on the second Saturday and collected   share at AP/Nizam Box office.  The film grossed a total of   at AP/Nizam region in 12 days and with revenue from Karnataka, Rest of India and Overseas, the 12 day global total was   with which this film surpassed the final collections of Businessman (film)|Businessman (2012), Pokiri (2006), Arundhati (2009 film)|Arundhati (2009), Aagadu and Iddarammayilatho (2013).  The film was declared one of the biggest hits of 2014 at the Box office. 

The film crossed the   mark in two weeks with the film collecting   at AP/Nizam Box office and the rest from Karnataka, Rest of India and Overseas. It was Ram Charans fifth film to gross in excess of  .    The films final collections in AP/Nizam region stood at  ,   in Karnataka and   from rest of India. It surpassed the final collections of Legend (2014 film)|Legend but failed to enter the list of all-time top 10 Telugu films with highest worldwide share, ending in thirteenth place behind Eega (2012).    The film completed a 50-day run on 20 November 2014. 

==== Overseas ====
The film had the biggest opening for a Ram Charan picture in the US, where it grossed   on its first day, according to trade analyst Jeevi of Idlebrain.com.  The film grossed   in 2 days in the US.  The film grossed around   on its third day in the US including reported and non-reported screens, making the film Ram Charans highest-grossing US-released film.  By the end of the first weekend, the film grossed around   at US Box office including rentrak and non reporting screens. It earned around   rentrak only from 121 locations in 5 days including Tuesday premier shows in the US.  The film collected an amount of   in its lifetime at Overseas Box office. 

== Remake == Hindi after watching a special screening by Bandla Ganesh. Ganesh confirmed that Prabhu watched the film and liked it. Prakash Raj was also expected to be a part of the remake as Prabhu was impressed with his performance. 

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 