Rita (film)
{{multiple issues|
 
 
}}

 
{{Infobox film
| name           = Rita
| image          =
| caption        =
| director       = Fabio Grassadonia, Antonio Piazza
| producer       = Massimo Cristaldi
| co-producer    = Fabrizio Mosca
| writer         = Fabio Grassadonia, Antonio Piazza
| narrator       =
| starring       = Marta Palermo
| music          =
| cinematography = Olaf Hirschberg
| editing        = Desideria Rayner
| set design     = Cesare Inzerillo
| distributor    =
| released       =  
| runtime        = 19 minutes
| country        = Italy
| language       = Italian
| budget         =
}}
Rita is an Italian short drama film released in 2009 and directed by Fabio Grassadonia and Antonio Piazza. It is a film realised from the point of view of a blind person. It has been presented in more than 100 international film festivals receiving several awards.

==Plot==
Rita is ten years old and blind since birth, lives in a seaside neighbourhood of Palermo in Sicily. She is stubborn, curious and feels thwarted by an overprotective dictatorial mother. The claustrophobic world of her home is suddenly breached by a mysterious presence. Its a boy on the run, wounded, terrified. The meeting between the two of them is highly enigmatic. Thanks to her mysterious new friend, or perhaps only thanks to her imagination and courage, Rita escapes from home and goes to the beach. She learns to swim and experiences a brief moment of freedom which ends when the miraculous encounter vanishes and she finds herself alone in the sea.

==Film structure==
During this short film the camera is focused on Rita, without reverse angle shots. Rita is under everyone’s gaze, the gaze of others which acts also as a controlling element, a form of surveillance. We can’t see what Rita has before her because she can’t either. For example, we see her mother’s hands flit in and out of the frame, when her mother is fussing around her, but we never see her mother. We feel the desire to see what stands before her but, for the most part of the short film, we are not granted any gratification of this desire. For Rita hearing is fundamental, and that is why the story, from her “point of view” is told through the sounds, the noises. The sounds represent what we can’t see, they evoke it, and to some extent create it. Only through an unexpected and enigmatic encounter does the staging change, because this meeting suddenly enables her to "see" and we, for the first time, are gifted with her perspective and share her vision. The film explores this brief moment of grace. Then everything goes back to normal for Rita and for us.

==Cast==
*Marta Palermo as Rita
*Marco Correnti as the young boy

==Awards==
Best Short Film
11th International Film Festival Bratislava | 2009
27 Nov – 4 Dec | Slovakia

Arte Prize for European Short Film
22nd Premiers Plans Festival d’Angers | 2010
Angers First Film Festival
22 Jan – 31 Jan | France

Diploma of Merit
40th Tampereen kansainväliset elokuvajuhlat | 2010
Tampere Film Festival
10 Mar – 14 Mar | Finland

Best Short Film
11th Festival Internacional de Cine Las Palmas de Gran Canaria | 2010
Las Palmas de Gran Canaria International Film Festival
12 Mar – 20 Mar | Spain

Award for Directing
19th Aspen Shortsfest | 2010
6 Apr – 11 Apr | United States

Best Use of Sound Design
37th Athens International Film and Video Festival | 2010
23 Apr – 29 Apr | United States

Special Jury Prize - National
18th Arcipelago Festival Internazionale di Cortometraggi e Nuove Immagini | 2010
Arcipelago International Festival of Short Films and New Images
18 Jun – 24 Jun | Italy

Best International Short Film
64th Edinburgh International Film Festival | 2010
16 Jun – 27 Jun | United Kingdom

Best Italian Short Film
8th MoliseCinema Film Festival | 2010
3 Aug – 8 Aug | Italy

Special Mention - Artistic Direction
9th Concorto Film Festival | 2010
22 Aug – 29 Aug | Italy

Special Jury Mention
8th Imaginaria Film Festival | 2010
24 Aug – 29 Aug | Italy

Jury Mention
16th Aye Aye Film Festival Nancy-Lorraine | 2010
3 Sep – 11 Sep | France

Best Sound
3rd Festival Internacional Curtametraxes Bueu | 2010
Bueu International Shortfilm Festival
6 Sep – 11 Sep | Spain

Best Short Fiction Film
17th Altin Koza Film Festival | 2010
Golden Boll Film Festival
20 Sep – 26 Sep | Turkey

Special Jury Mention
7th Filmfest Eberswalde | 2010
2 Oct – 9 Oct | Germany

Best Short Film
8th Zagreb Film Festival | 2010
17 Oct – 23 Oct | Croatia

Special Jury Mention - Narrative Short Film
Abu Dhabi Film Festival | 2010
14 Oct – 23 Oct | United Arab Emirates

Artistic Direction Award
10th Cortopotere ShortFilmFestival | 2010
25 Oct – 31 Oct | Italy

Special Jury Mention - European Short Films
40th Alcine: Festival de Cine de Alcalá de Henares | 2010
5 Nov – 13 Nov | Spain

Special Jury Mention
25th Festival Europeen du Film Court de Brest | 2010
Brest European Short Film Festival
8 Nov – 14 Nov | France

Best Short Feature Film
5th Breaking Down Barriers Disability Film Festival | 2010
18 Nov – 21 Nov | Russia

Best European Film
4th Unlimited: Europäisches Kurzfilmfestival | 2010
Unlimited: European Short Film Festival
23 Nov – 28 Nov | Germany

Best Short Film on Social Themes
7th Corto Dorico: Festival del Cortometraggio Italiano | 2010
12 Dec – 19 Dec | Italy

Special Mention
Nastri DArgento | 2011
25 Mar – 28 Mar | Italy

Runner-up - 16 to 30 minutes
Imago Film Festival | 2011
28 Mar – 1 Apr | United States

Special Jury Prize
27th Festival du Cinema Européen de Lille | 2011
European Film Festival of Lille
1 Apr – 8 Apr | France

Future Shorts Prize
27th Festival du Cinema Européen de Lille | 2011
European Film Festival of Lille
1 Apr – 8 Apr | France

Best Short Film
6th Sicilian Film Festival | 2011
6 Apr – 13 Apr | United States

Best Film - Youth Jury
23rd Filmfest Dresden: Internationales Kurzfilmfestival | 2011
Filmfest Dresden: International Short Film Festival
12 Apr – 17 Apr | Germany

Special Jury Mention
23rd Filmfest Dresden: Internationales Kurzfilmfestival | 2011
Filmfest Dresden: International Short Film Festival
12 Apr – 17 Apr | Germany

Highly Commended - International
Magma Short Film Festival | 2011
28 Apr – 2 May | New Zealand

Outstanding Achievement in Short Filmmaking
12th Newport Beach Film Festival | 2011
28 Apr – 5 May | United States

Best Narrative Short Film
14th Brooklyn Film Festival | 2011
3 Jun – 12 Jun | United States

Best International Short Film
Lume International Film Festival | 2011
14 Jul – 23 Jul | Brazil

Best Drama Short Film
10th Route 66 Film Festival | 2011
16 Sep – 18 Sep | United States

Directors Choice
30th Black Maria Film and Video Festival | 2011
6 Feb – 31 Mar | United States

Special Mention
8th Sedicicorto: Festival del Cortometraggio Forlì | 2011
Sedicicorto: International Film Festival Forlì
3 Oct – 9 Oct | Italy

Best Short Film
4th Festival de Cine Italiano de Madrid | 2011
24 Nov – 1 Dec | Spain

Award of Excellence - Disability Issues
The Accolade Competition | 2011
1 Jan – 31 Dec | United States

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 