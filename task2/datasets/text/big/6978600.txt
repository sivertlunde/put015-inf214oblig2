Androcles and the Lion (film)
{{Infobox film
| name           = Androcles and the Lion
| image          = Androcles lion.jpg
| caption        = DVD cover
| director       = Chester Erskine Nicholas Ray (uncredited)
| writer         = George Bernard Shaw Ken Englund Chester Erskine
| starring       = Jean Simmons Victor Mature Alan Young
| producer       = Gabriel Pascal
| music          = Friedrich Hollaender
| cinematography = Harry Stradling Sr.
| editing        = Roland Gross
| distributor    = RKO
| released       =    |ref2= }}
| runtime        = 98 minutes
| country        = United Kingdom
| language       = English
}} play of the same name. It was Pascals last film, made two years after the death of Shaw, his long-standing friend and mentor, and two years before Pascals own death.                  

==Plot==
The plot is a more or less careful rendition of George Bernard Shaws script. Androcles, a fugitive Christian tailor, accompanied by his nagging wife, is on the run from his Roman persecutors. While hiding in the forest he comes upon a wild lion who approaches him with a wounded paw. Androcles sees that the cause is a large thorn embedded in its paw, which he draws out while talking baby language to the lion. His wife had fled, and Androcles is next seen in a procession of Christian prisoners on their way to the Colosseum in Rome. They are joined by the fierce convert Ferrovius who subsequently provides much of the comic entertainment in his struggle to keep his nature in check. Love interest is also introduced by the growing attraction of the Captain to the noble-born convert Lavinia. Eventually the party is sent into the arena to be slaughtered by gladiators, but Ferrovius kills all of them and accepts a commission offered him in the Praetorian Guards. To appease the crowd, one Christian is needed to be savaged by the lions and Androcles volunteers in order to uphold the honour of the tailors. It turns out that the lion is the one that Androcles has helped, and the two waltz round the arena to the acclaim of the people. The Emperor dashes behind the scenes to get a closer look and has to be rescued from the lion by Androcles. He then orders an end to the persecution of Christians and allows Androcles and his new pet to depart in peace.

==Cast==
* Jean Simmons as Lavinia
* Victor Mature as the Captain
* Alan Young as Androcles
* Robert Newton as Ferrovius Maurice Evans Caesar
* Elsa Lanchester as Megaera
* Reginald Gardiner as Lentulus
* Gene Lockhart the Menagerie Keeper
* Alan Mowbray as the Editor of Gladiators
* Noel Willman as Spintho
* John Hoyt as Cato
* Jim Backus as the Centurion
* Lowell Gilmore as Metellus 
* Woody Strode as the Lion
* Sylvia Lewis as the Chief of the Vestal Virgins

Note that the opening sequence of the film places it during the time of Emperor Antoninus Pius, but the character is only addressed as "Caesar" during the film.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 