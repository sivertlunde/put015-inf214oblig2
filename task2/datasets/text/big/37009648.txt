Simple Souls
{{infobox film
| name           = Simple Souls
| image          = Simple Souls (1920) - Ad 1.jpg
| imagesize      = 210px
| caption        = Ad for film
| director       = Robert Thornby
| producer       = Jesse D. Hampton
| writer         = Fred Myton (scenario)
| based on       =  
| starring       = Blanche Sweet
| cinematography = Charles E. Kaufman
| editing        =
| distributor    = Pathé Exchange
| released       =  
| runtime        = 6 reels; 5,264 feet 
| country        = United States
| language       = Silent (English intertitles)
}}

Simple Souls is a 1920 American  . 

==Plot==
Based upon a description in a film publication,  Molly Shine (Sweet) is a simple girl who likes her books, and lives with a drinking father (Grimwood) and weeping mother (Kelso). One day she meets the young Duke (Meredith) who falls in love with her and marries her, astonishing her parents who do not believe it until they see the marriage certificate. After the marriage she faces the problems of all who marry outside of their class. The Dukes sister Lady Octavia (Lester) tries to snub her at every turn, resulting in Mollys dejection and which to leave. When an opportunity presents itself, she attempts to escape but fate returns her to her husband in a peculiar but pleasing way.

==Cast==
*Blanche Sweet - Molly Shine Charles Meredith - Duke of Wynningham
*Kate Lester - Lady Octavia
*Herbert Standing - Peter Craine
*Mayme Kelso - Mrs. Shine
*Herbert Grimwood - Samuel Shine

==See also==
*Blanche Sweet filmography

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 