The Party's Over (1965 film)
{{Infobox film
| name           = The Partys Over
| image          = The Partys Over film poster.jpg
| image_size     =
| caption        = Theatrical release poster
| writer         = Marc Behm Katherine Woodville
| director       = Guy Hamilton
| producer       = Anthony Perry
| cinematography = 
| editing        = 
| country        = United Kingdom
| distributor    = Monarch Film Corporation (UK) Allied Artists Pictures (US)
| released       =  
| runtime        = 94 minutes John Barry
| language       = English
}} John Barry. Guy Hamilton asked for his name to be removed from the credits in protest at the censorship of the film. 

==Plot== Chelsea beatniks, catching the attention of the gangs defiant leader Moise (Oliver Reed) but inviting scorn and jealousy from the groups other members, including Moises lover Libby (Ann Lynn). Wild and drunken partying has terrible consequences and when Melinas fiancee Carson (Clifford David) begins investigating, the shocking truth is revealed.

==Cast==
 
* Oliver Reed as Moise
* Clifford David as Carson
* Ann Lynn as Libby Katherine Woodville as Nina
* Louise Sorel as Melina Mike Pratt as Geronimo
* Maurice Browning as Tutzi
* Jonathan Burn as Phillip
* Roddy Maude-Roxby as Hector
* Annette Robertson as Fran
* Alison Seebohm as Ada
* Barbara Lott as Almoner
* Eddie Albert as Ben
 

==Censorship== John Trevelyan the Secretary of the Board of the BBFC, called the film unpleasant, tasteless and rather offensive. The BBFC requested three rounds of cuts, before granting an X certificate and allowing the film to finally reach cinemas in the UK in 1965. Two big changes were incorporated, a voiceover by Oliver Reed and a happier ending focusing on Melinda and Carson.

Director Guy Hamilton, along with the producer and executive producer, removed their names from the credits in protest. 

==DVD & Blu-ray Release== Flipside series. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 