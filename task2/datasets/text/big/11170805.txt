Congo River, Beyond Darkness
{{Infobox film
| name           = Congo River, beyond darkness
| image          = Congo river petit 2.jpg
| image_size     = 
| caption        = 
| director       = Thierry Michel
| producer       = 
| writer         = Thierry Michel
| narrator       = 
| starring       = 
| music          = Lokua Kanza
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Belgium
| language       = French English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Congo River, beyond darkness is a 2005 film by Thierry Michel examining the Congo River in Africa.

==Synopsis== Livingstone and Henry Morton Stanley|Stanley, the colonial kings Léopold II and Baudouin I and leaders such as Patrice Lumumba|Lumumba, Mobutu and Laurent-Désiré Kabila|Kabila.

==Technical data==
The collectors edition DVD of "Congo River" (2 DVD) with:
* The making of (52 min.)
* Interview of Thierry Michel
* Trailer
* Versions: French and English with subtitles in French, Dutch, German and English
The DVD of "Congo River" with:
* Versions: French and English with subtitles in French, Dutch, English and German
The serial:
* Number: 3 episodes
* Length: 2h30
* Versions: French and English

==External links==
*  
* 
* 
* 

 
 
 
 
 
 


 
 