Anumodhanam
{{Infobox film
| name = Anumodhanam
| image = Anumodmalfilm.JPG
| image_size =
| caption = LP Vinyl Records Cover
| director = IV Sasi
| producer = Thayyil Kunjikandan
| writer = Thoppil Bhasi
| screenplay = Thoppil Bhasi Raghavan Bahadoor
| music = A. T. Ummer
| cinematography = C Ramachandra Menon
| editing = K Narayanan
| studio = Chelavoor Pictures
| distributor = Chelavoor Pictures
| released =  
| country = India Malayalam
}}
  1978 Cinema Indian Malayalam Malayalam film, Raghavan and Bahadoor in lead roles. The film had musical score by A. T. Ummer.    

==Cast==
*Kamalahasan 
*Sankaradi  Raghavan 
*Bahadoor 
*MG Soman  Seema 
*Vidhubala

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Bharanikkavu Sivakumar. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Chiri Kondu || K. J. Yesudas || Bharanikkavu Sivakumar || 
|- 
| 2 || Kaappikal Pookkunna || P Jayachandran, Ambili, KP Brahmanandan, B Vasantha || Bharanikkavu Sivakumar || 
|- 
| 3 || Kizhakku Mazhavil || K. J. Yesudas, Ambili || Bharanikkavu Sivakumar || 
|- 
| 4 || Mullappoo || Ambili, Chorus || Bharanikkavu Sivakumar || 
|}

==References==
 

==External links==
*  

 
 
 


 