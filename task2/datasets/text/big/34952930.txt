Gabbla
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Gabbla
| image          =
| caption        =
| director       = Tariq Teguia
| producer       = Neffa Films, Ciné@, Captures, Le Fresnoy
| writer         =
| starring       = Abdelkader Affak, Ines Rose Djakou, Ahmed Benaïssa, Fethi Gharès, Djalila Kadj-Hanifi
| distributor    =
| released       =  
| runtime        = 138 minutes
| country        = Algeria France
| language       =
| budget         =
| gross          =
| screenplay     = Tariq Teguia, Yacine Teguia
| cinematography = Nasser Medjkane, Hacène Aït Kaci
| sound          = Matthieu Perrot, Kamel Fergani
| editing        = Rodolphe Molla, Andrée Davanture
| music          = Ina Rose Djakou
}}

Gabbla is a 2009 film.

== Synopsis ==
Despite practically living as a recluse far from the maddening world, Malek, a forty-year-old topographer, accepts a job to the West of Algeria. A company in Oran entrusts him with the layout of the new electrical line that will bring power to the hamlets in the Ouarsenis Massif, an area that lived under the whip of radical Islamism until barely ten years ago. After several hours on the road, Malek reaches the base camp. While he starts putting things in order, he discovers a young woman hidden in a corner.

== Awards ==
* Venice Film Festival 2008

== References ==
 
*  

 
 
 


 