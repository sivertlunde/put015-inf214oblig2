Women at a Secret Meeting: From Wives to Coeds
{{Infobox film
| name = Women at a Secret Meeting: From Wives to Coeds
| image = Women at a Secret Meeting - From Wives to Coeds.jpg
| image_size = 
| caption = Theatrical poster for Women at a Secret Meeting: From Wives to Coeds (1996)
| director = Sachio Kitazawa 
| producer = 
| writer = Sachio Kitazawa
| narrator = 
| starring = Yūki Morishita Shunichi Yajima Kyōsuke Sasaki
| music = Taoka
| cinematography = Kazuhiro Suzuki
| editing = Sachio Kitazawa
| studio = Izumi Production
| distributor = Xces Film
| released = October 10, 1996
| runtime = 60 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1996 Japanese pink film directed by Sachio Kitazawa. It won the award for Ninth Best Film at the Pink Grand Prix ceremony.  Kitazawa filmed Women at a Secret Meeting: From Wives to Coeds for Izumi Production and it was released theatrically in Japan by Xces Film on October 10, 1996.  On February 25, 2011, Exces re-released the film, under the title  . 

==Synopsis==
The film depicts the lives of several women working at a paid dating shop. 

==Cast==
* Yūki Morishita ( )   
* Shunichi Yajima ( )
* Kyōsuke Sasaki ( )
* Azumi Yūki ( )
* Kaori Nishiyama ( )
* Tetsuo Itadaki ( )

==Bibliography==
*  
*  

==Notes==
 

 
 
 
 
 

 

 
 
 
 


 
 