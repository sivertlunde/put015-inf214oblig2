Los martes, orquídeas
{{Infobox film
| name           = Los martes, orquídeas
| image          = Los martes, orquideas.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Francisco Múgica
| producer       = 
| writer         = Francisco Oyarzábal, Carlos A. Olivari, Sixto Pondal Ríos
| screenplay     = 
| story          = 
| based on       =  
| starring       = Enrique Serrano, Juan Carlos Thorry, Nuri Montsé
| narrator       = 
| music          = Enrique Delfino
| cinematography = Alfredo Traverso
| editing        = 
| studio         = Lumiton
| distributor    = 
| released       =   
| runtime        = 84 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = 
}} 1941 Argentina|Argentine black and white comedy film which won the Argentine Best Picture award for 1941.

==Production==

Los martes, orquídeas was produced by Lumiton, directed by Francisco Múgica.
The script was written by Francisco Oyarzábal based on a simple yet effective story by Carlos A. Olivari and Sixto Pondal Ríos. 
The stars were Enrique Serrano, Juan Carlos Thorry and Nuri Montsé.
The film introduced Mirtha Legrand as Elenita, in her first leading role. 

==Synopsis==

The plot revolves around Elenita, the youngest of four sisters, shy, romantic and lonely.
To try to bring her to life, her father sends her a bouquet of orchids each week, so she will think she has an admirer. 
A young, unemployed actor is hired to pose as the wealthy suitor. There are comic scenes as he dresses in fancy clothes, rides a horse and attends a party. By the time the ruse comes to light, the young couple have fallen in love and decided to marry. The father expects that his daughter will reject the young man when she learns he is poor and Italian in origin, but to his dismay she accepts him as he is. 

==Reception==

Los martes, orquídeas was considered the best comedy that had been made in Argentina to date. 
It launched the career of the young Mirtha Legrand. 
Columbia Pictures used the film as the basis for You Were Never Lovelier (1942) with Fred Astaire and Rita Hayworth.  Argentine Academy of Cinematography Arts and Sciences gave a number of awards for the film: 
*Best Film: Lumiton
*Best original story: Pondal Sixto Ríos and Carlos Olivari
*Special mention: Mirtha Legrand

==Full cast==
The full cast was: 
 
*Enrique Serrano		
*Juan Carlos Thorry	
*Nuri Montsé	
*Felisa Mary
*Mirtha Legrand (as Elenita)
*Ana Arneodo
*Silvana Roth
*Zully Moreno
*Juan Mangiante
*Eva Guerrero
*René Pocoví
*Horacio Priani
*Domingo Márquez
*Alfredo Jordan
*José Herrero]
*Jorge Salcedo
 

==References==
Citations
 
Sources
 
* |url=http://www.academiadecine.org.ar/premios-anuales.php |language=Spanish
 |title=ARCHIVO · Premios Anuales 1941 - 1953|publisher=Academia de Artes y Ciencias Cinematográficas|accessdate=2014-05-30}}
*{{cite book|ref=harv
 |last=Karush|first=Matthew B.|title=Culture of Class: Radio and Cinema in the Making of a Divided Argentina, 1920–1946
 |url=http://books.google.com/books?id=6-1DK1CwGeAC&pg=PA131|accessdate=2014-06-01
 |date=2012-05-15|publisher=Duke University Press|isbn=0-8223-5264-8}}
* |url=http://www.todo-argentina.net/historia/videos/decadainfame/orquideas.html
 |title=Los martes orquideas|work=Todo Argentina|accessdate=2014-06-01}}
* |url=http://www.imdb.com/title/tt0175892/
 |title=On Tuesdays, Orchids|work=IMDb|accessdate=2014-06-01}}
*{{cite book|ref=harv
 |last=Parkinson|first=David|title=The Rough Guide to Film Musicals|url=http://books.google.com/books?id=UGDTBhDNJhkC&pg=PA44|accessdate=2014-06-01
 |date=2007-08-01|publisher=Rough Guides|isbn=978-0-7566-4712-4}}
*{{cite book|ref=harv
 |last=Plazaola|first=Luis Trelles|title=South American Cinema/ Cine De America Del Sur: Dictionary of Film Makers/ Diccionario De Los Productores De Peliculas
 |url=http://books.google.com/books?id=VansCHXxnZYC&pg=PA142|accessdate=2014-06-01
 |date=1989-01-01|publisher=La Editorial, UPR|isbn=978-0-8477-2011-8}}
 
 
 
 
 
 
 
 
 
 