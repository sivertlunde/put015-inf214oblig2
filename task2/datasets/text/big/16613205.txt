The Tunnel (1935 film)
 
 
 
{{Infobox film
| name           = The Tunnel
| image          = The Tunnel US poster.jpg
| caption        = poster for the American release
| director       = Maurice Elvey
| producer       = Michael Balcon
| writer         = Bernhard Kellermann (Der Tunnel (novel)|novel) Curt Siodmak L. du Garde Peach Clemence Dane (additional dialogue)
| starring       = Richard Dix Leslie Banks Madge Evans Helen Vinson C. Aubrey Smith Basil Sydney
| music          = Hubert Bath (uncredited)
| cinematography = Günther Krampf
| editing        = Charles Frend
| distributor    = Gaumont British
| released       =  
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} British science Der Tunnel by Bernhard Kellermann, about the building of a transatlantic tunnel. It was directed by Maurice Elvey and stars Richard Dix, Leslie Banks, Madge Evans, Helen Vinson, C. Aubrey Smith and Basil Sydney. The script was written by Curt Siodmak, L. du Garde Peach and Clemence Dane.

==Plot== undersea tunnel linking England with the United States. Though the group is initially sceptical, the backing of Lloyd and his associate Mostyn convinces the group to buy shares in the project.

Three years into construction of the tunnel, McAllan is a worldwide celebrity, but his work keeps him from his devoted wife Ruth and their young son Geoffrey. Called away to New York, he is informed that the people are losing faith in the project. Lloyd needs to have him use his fame to shore up support. Lloyds attractive daughter Varlia, who is secretly in love with McAllan, keeps him company to further intensify the attention of the press.

The photos of the couple add to Ruths sense of isolation, and she decides to work in the tunnel as a nurse. There she is affected by an unknown gas afflicting the workers and loses her eyesight. Worried that her husband no longer loves her and not wanting him to stay with her out of pity, Ruth leaves McAllan, taking their son with her. Heartbroken at her unexplained departure, McAllan throws himself into the project, alienating Robbins in the process.

Years pass. Though the cost of the tunnel in lives and money continues to mount, the British Prime Minister and American President eagerly anticipate its completion and the unity and peace they promise it will bring. Ruth lives in the countryside with her now-grown son, who lobbies Robbins to find him a job working in the tunnel. The tunnel itself is nearing completion, but the workers encounter a submarine volcano that will necessitate a detour. McAllan needs more money to establish a detour, but is opposed by Grellier, an arms manufacturer, and Mostyn. The two men had earlier manipulated the stock market to become the controlling shareholders in the company. Lloyd suspects that Grellier and Mostyn plan to use the delay to depress stock prices again, and this time gain total ownership of the tunnel, but Varlia convinces Mostyn to fund further construction by promising him the one thing he has always wanted, but never gotten—her hand in marriage. Though the project goes forward, Grellier has Mostyn killed for backing out of their deal.

Despite the renewed effort, samples indicate the volcano may be too large to drill around. The drill breaks through to volcanic gases that kill hundreds of workers, including Geoffrey. The project seems on the verge of collapse. Determined to see the project through and fortified by the reappearance of Ruth (who came to the tunnel site to discover Geoffreys fate), McAllan vows to press on. With three volunteers, McAllan and Robbins man the radium drill and, despite near-fatal temperatures, break through to the American side of the tunnel.

==Cast==

*Richard Dix as Richard "Mack" McAllan
*Leslie Banks as Frederick "Robbie" Robbins
*Madge Evans as Ruth McAllan
*Helen Vinson as Varlia Lloyd
*C. Aubrey Smith as Lloyd
*Basil Sydney as Mostyn
*Henry Oscar as Grellier
*Hilda Trevelyan as Mary
*Walter Huston as President of the United States
*Cyril Raymond as Harriman
*George Arliss as Prime Minister of Great Britain
*Jimmy Hanley as Geoffrey McAllan

==Production== Der Tunnel had been filmed three times before, once as a German silent, Der Tunnel (1915),  and then as two sound films Der Tunnel (German)  and Le Tunnel (French),  both released in 1933, and both directed by Curtis Bernhardt, who went on to become a Hollywood director. 
 Perfect Strangers (1945).

The New York City opening of The Tunnel took place on 27 October 1935. 

The filmmakers were sued by a Lady Fairhaven who complained about a character with that name in the film being confused for her. The case was settled out of court. 

==Poster==
An original advertising poster for the film was catalogued with an estimated value of between $2000 – $3000 by Heritage Auction Galleries in Dallas in the summer of 2006.

==See also==
*Transatlantic tunnel
*Tatra 77, an advanced Czech automobile used in the film by director Maurice Elvey, who was taken with its then-futuristic look

==References==
 

==External links==
*  
*  
*  
*  
*   at RichardDix.org

 
 

 
 
 
 
 
 
 
 
 
 
 
 