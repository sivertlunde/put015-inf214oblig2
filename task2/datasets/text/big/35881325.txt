Gunfight at Comanche Creek
{{Infobox film
| name           = Gunfight at Comanche Creek
| image          =
| caption        = Frank McDonald
| producer       = Ben Schwalb
| writer         = Edward Bernds
| narrator       =
| starring       = Audie Murphy Colleen Miller
| music          =
| cinematography = Joseph F. Biroc
| editing        = Allied Artists
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}} Western film starring Audie Murphy. 

==Plot==
A gang of bank robbers led by Amos Troop (DeForest Kelley) uses a technique where they break prisoners out of jail, use them to commit crimes, then later kill them to collect the reward. A detective, Gifford (Audie Murphy), goes undercover with the gang to bring them to justice.

==Cast==
*Audie Murphy as Bob Gifford
*Ben Cooper as Kid Carter
*Colleen Miller as Abbie Stevens
*DeForest Kelley as Amos Troop
*Jan Merlin as Nielson John Hubbard as Marshal Shearer
*Damian OFlynn as Winton
*Susan Seaforth as Janie Adam Williams as Mort Mills
*John Milford
*Michael T. Mikler

==References==
 

==External links==
*  
*  

 
 
 
 
 
 