Breath of Hate
{{Infobox film
 | name = Breath of Hate
| starring = Jason Mewes Ezra Buzzington Monique Parent
 | image = Breath of Hate FilmPoster.jpeg
| director = Sean Cain
 | producer = Sean Cain, Wes Laurie
 | writer = Wes Laurie
 | music = Mario Salvucci
 | country = United States
 | language = English
 | released =  
 }}
Breath of Hate is an unreleased 2011 horror film which stars Jason Mewes,  Ezra Buzzington  and Monique Parent. It is the second collaboration between Velvet Hammer Films and ArsonCuff Entertainment who previously teamed up on Silent Night, Zombie Night.

==Plot== psychotic cultists whove just escaped from a mental hospital and are trawling for victims.

==Cast==
*Ezra Buzzington as Hate
*Jason Mewes as Ned
*Lauren Walsh as Love
*Jack Forcinito as Sonny
*Monique Parent as Selma
*Timothy Muskatell as Poot
*Ricardo Gray as Cleb
*Felissa Rose as Realtor
*Alexis Zibolis as Jenna
*Jeanine Daniels as Hailey
*Trista Robinson as Tabbi Ted Prior as Danton
*Aramis Sartorio as Mike
*Joanna Angel as Candy
*Regan Reece as Lead Dancer

==Production==
Production began in August 2010 for fourteen days in the Los Angeles area. The Malibu mansion that Hate and his cohorts take over has been used in many other productions, mostly notably David DeCoteaus 1313 films and sex comedies from The Asylum.

==Release==
The film had a sneak preview at the Gorezone magazine Weekend of Horrors on October 2, 2010 in London where it played to a packed house.  It had its official world premiere as part of 2011s Another Hole in the Head film festival in San Francisco  and as part of the Fangoria film festival in Indiana. 
 four wall theatrical debut and DVD/Blu-ray release. The $15,000 fundraiser raised $5,033 after one month, and the project subsequently failed to get funded.  Soon after, the film was picked up for a Los Angeles premiere in March by the CineMayhem film festival  in conjunction with Dread Central as part of their 2013 Indie Horror month where it will play alongside another Jason Mewes film, K-11 (film)|K-11 and the world premiere of The ABCs of Death.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 