We'll Live Till Monday
 
 
{{Infobox film
| name           = Well Live Till Monday
| image          = Well_Live_Till_Monday.jpg
| caption        = Film poster
| director       = Stanislav Rostotsky
| producer       = Grigoriy Rimalis
| writer         = Georgi Polonsky
| starring       = Vyacheslav Tikhonov
| music          = Kirill Molchanov
| cinematography = Vyacheslav Shumsky
| editing        = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

Well Live Till Monday ( , Transliteration|translit.&nbsp;Dozhivyom do ponedelnika) is a 1968 Soviet drama film directed by Stanislav Rostotsky. It was entered into the 6th Moscow International Film Festival where it won the Golden Prize.   

The film is about the life of an ordinary Moscow school with all its joys, problems and difficult choices in their lives of students and teachers.

==Cast==
* Vyacheslav Tikhonov as Ilya Semyonovich Melnikov - History Teacher
* Irina Pechernikova as Natalya Sergeevna Gorelova - English Language Teacher, former Melnikovs student
* Nina Menshikova as Svetlana Mikhailovna - Russian Language and Literature Teacher
* Mikhail Zimin as Nikolai Borisovich - School Principal
* Nadir Malishevsky as TV Show Host
* Dalvin Shcherbakov as Borya Rudnitsky, former Melnikovs student
* Olga Zhiznyeva as Melnikovs Mother
* Lyudmila Arkharova as Nadya Ogarysheva, pupil
* Valeriy Zubarev as Genka Shestopal, pupil
* Olga Ostroumova as Rita Cherkasova, pupil
* Igor Starygin as Kostya Batishchev, pupil
* Roza Grigoryeva as Sveta Demidova, pupil
* Yuri Chernov as Syromyatnikov, pupil
* Lyubov Sokolova as Levikova
* Arkadi Listarov as Vova Levikov, pupil

== Awards ==
* Sovetskiy Ekran Magazine Best 1968 film
* Golden Prize of 6th Moscow International Film Festival, 1969
* USSR State Prize, 1970

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 