True North (film)
{{Infobox Film

 | name = True North
 | image_size = 
 | caption = 
 | director = Steve Hudson
 | producer = David Collins Eddie Dick Sonja Ewers
 | writer = Steve Hudson
 | narrator =  Gary Lewis Steven Robertson Angel Li
 | music = Edmund Butt
 | cinematography = Peter Robertson
 | editing = Andrea Mertens
 | distributor = Ariel Films
 | released = September 11, 2006 (Canada) January 18, 2007 (Germany) September 14, 2007 (UK) December 18, 2007 (Ireland)
 | runtime = 96 min.
 | country = United Kingdom Germany Republic of Ireland English
 | budget = 
 | preceded_by = 
 | followed_by = 
 | image = True North VideoCover.png
}}
 Scottish drama Gary Lewis Chinese into the United Kingdom.

The film premiered at the Toronto Film Festival in Canada on September 11, 2006 and was also shown at the Max Ophüls Festival in Germany, the Karlovy Vary Film Festival in the Czech Republic, the Copenhagen International Film Festival in Denmark and the Ourense International Film Festival in Spain.

==Plot== Snakehead (a smuggler of people), telling him the false stories they will use for sympathy in the West.
 Chinese immigrants into the United Kingdom. Seán reluctantly agrees as he and his father are heavily indebted after failing to catch enough fish to pay the mortgage. Riley returns to find the Chinese aboard the ship, but he and Seán agree not to inform the Skipper or the cook.

The Chinese are kept below deck in poor conditions but one, a 12-year-old girl named Su Li (Angel Li), sneaks off and lives in another part of the boat. She begins to steal food from the kitchen but the slow-witted cook eventually notices that some of his food is missing. Seán insists they cant go home without a good catch as an empty boat would look suspicious and the cargo hold may be searched and the Chinese discovered. The nets come up near empty almost every time, however, and the conditions in the hold are beginning to deteriot rapidly. When one of the Chinese dies, Riley goes to the Skipper and confesses all. The Skipper confronts Seán but soon realises that he was simply doing what was in the ships, and their, best interests. For the first time, the Skipper is forced to acknowledge that hes only making a loss by staying at sea. Broken, he gives the wheel to Seán, who turns for home. Meanwhile, the cook discovers Su Li and comforts her, but does not inform anyone else of her presence on board the ship. The Skipper believes that if they return to Scotland, they will face certain arrest; he will lose his license, and the bank will foreclose on the boat. He drowns the Chinese without telling anyone. Seán and Riley then see the nets are full and pull them up. The crew is horrified to find the corpses of the Chinese. The cook attacks the Skipper with a chain and kills him, as he would have no choice but to kill Su Li also.

The film then ends with Riley and Su Li sitting at a bus stop in Peterhead, Scotland. Su Li gets on a coach and it drives away. Inside her bag is the large sum of money from the ship.

==Cast==
* Peter Mullan as Riley
* Martin Compston as Seán Gary Lewis as The Skipper
* Steven Robertson as The Cook
* Angel Li as Su Li
* Hark Bohm as Pol
* Pat Kiernan as Henri
* Shi Ming as The Snakehead

==External links==
*  
* 

 
 
 
 
 
 
 
 
 
 