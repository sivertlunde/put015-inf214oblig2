The Great Rupert
{{Infobox film
| name           = The Great Rupert
| image          = The Great Rupert.jpg
| caption        = DVD cover
| director       = Irving Pichel
| producer       = George Pal Ted Allen (story) László Vadnay Terry Moore Tom Drake
| music          = Leith Stevens
| cinematography = Lionel Lindon
| editing        = Duke Goldstone
| distributor    = Eagle-Lion Films|Eagle-Lion
| budget         =
| released       =  
| runtime        = 87 minutes
| language       = English
}}
 comedy family Terry Moore. It is based on a story, written by Ted Allan, which has also been published as a childrens book under the title "Willie the Squowse". 

The story revolves around a little animated squirrel who, with lots of charm, accidentally helps two economically distressed families overcome their obstacles.

==Plot==

Rosalinda Amendola, the daughter of happy but impoverished former acrobats is in love with the boy next door, aspiring composer Pete Dingle.  Though Petes parents are wealthy, his miserly father Frank insists on hiding his money from his investments in the wall of their family home.

The siutation changes when Joe Mahoney, a vaudeville performer has fallen on hard times and has to leave his best friend and stage companion, Rupert a dancing squirrel in Frank and Rosalindas town where he will have to fend for himself with the other squirrels and live in a tree.  Unsatisfied with tree life, Rupert gains access to the Dingle home and unbeknownst to Frank, has his bed in Franks hidden cache of money.  Rupert decides to clear room in his domicile by throwing Franks money through a hole that floats down into the Amendola household who think the money has come from Heaven in answer to Mrs Amendolas prayers.

Attracted by Louie Amendola not only paying his debts, but helping all the needy businesses of the town, the FBI, IRS and local police coverge on the House of Amendola to discover the source of the familys wealth.

==Cast==
 
* Jimmy Durante as Mr. Louie Amendola Terry Moore as Rosalinda Amendola
* Tom Drake as Peter Pete Dingle
* Frank Orth as Mr. Frank Dingle
* Sara Haden as Mrs.Katie Dingle
* Queenie Smith as Mrs. Amendola
* Chick Chandler as Phil Davis
* Jimmy Conlin as Joe Mahoney
* Rupert, an animated squirrel
* Hugh Sanders as Mulligan
* Don Beddoe as Mr. Haggerty
* Candy Candido as Molineri - Florist
* Clancy Cooper as Police Lt. Saunders Harold Goodwin as Callahan - F.B.I. Man
* Frank Cady as Mr. Taney - Tax Investigator
 

==Notes==
George Pals stop-motion animation used in creating the illusion of a dancing squirrel (Rupert) was so realistic that he received many inquiries as to where he located a trained  squirrel.

In 1999, Arnold Leibovit Entertainment re-released the film on DVD.
 colorized special edition of the film, under the title A Christmas Wish. For that release, Terry Moore provided an audio commentary track.

==See also==
* List of films in the public domain

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 