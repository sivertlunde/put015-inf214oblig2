Return of the Living Dead: Rave from the Grave
 
{{Infobox television
| name           = Return of the Living Dead: Rave to the Grave
| image          =  
| caption        = Early advertisement for the film
| director       = Ellory Elkayem
| producer       = Anatoly Fradis Steve Scarduzio William Butler Aaron Strongoni John Keefe Jenny Mollen Peter Coyote Robert Duncan Ralph Rieckermann
| cinematography = Gabriel Kosuth
| studio         = Denholm Trading Inc. Aurora Entertainment Castel Film Romania
| distributor    = Denholm Trading Inc.
| released       = TV premiere (edited):   DVD premiere:  
| runtime        = 86 minutes
| country        = United States
| language       = English
| preceded_by    =  
}} John Keefe, Jenny Mollen, and Peter Coyote.

It is the fifth installment of the   films is the substance Trioxin, being a direct sequel to  , and a zombie cameo.

==Plot==
 
The film opens a year after the previous film with Charles Garrison (Peter Coyote) arriving at a mortuary with one of the canisters of Trioxin that he salvaged from the previous movie. He is greeted by a team of Russian government officials whose goal is to destroy the last of the canisters to avoid another incident like that of what happened in Necropolis.  Nevertheless, one of them sprays three corpses with the gas, and revives three corpses. Charles, is killed during the incident along with the mortuary owner and one of the government officials.
 John Keefe), Cody (Cory Hardrict) and Becky (Aimee-Lynn Chadwick), surviving characters from the previous film except Jenny are now in college as of this movie, receive notice of the “murder” of Charles. Julian and Jenny go to search for and possibly sell what belonged to Charles, and they find the last two barrels of Trioxin. One of them is taken to Cody, who tests the chemical inside it. Jeremy, Jenny’s brother, tastes the chemical when he thinks that it is a drug similar to Ecstasy, and he goes into a spasm, in which he foams at the mouth, and then describing what it was like. The chemical is named “Z” for its “zombie-like” effect on the living.

When Jenny and Julian leave the lab, Cody, Jeremy and Shelby extract the chemical from the canister and they put the liquid extract into pills which the sell to Skeet (Catalin Paraschiv), so he can sell the drug around the school. While Skeet informs everyone to only take one pill at a time for health reasons, most that go to the school take more than one pill at a time (which speeds up the process that causes humans to reanimate as zombies).

Gino (Sorin Cocis) and Aldo Serra (Claudiu Bleonț), the only survivors from the beginning of the movie, recognize what is going on when they are shown the severed head of one of the zombies, and they go to question Julian, knowing that he is familiar with Trioxin due to the events of the previous movie, but he doesn’t tell them anything of the canisters that he and Jenny discovered. Sometime later, people are turning into zombies, and the drug is being passed around a rave, and getting out of control. Seeing no other option, Aldo calls in military assistance, but he is told that an American bomber plane is already on the way to the rave location.

==Cast==
*Cory Hardrict as Cody John Keefe as Julian Garrison
*Jenny Mollen as Jenny
*Peter Coyote as Uncle Charles
*Claudiu Bleonț as Aldo Serra
*Sorin Cocis as Gino
*Cain Mihnea Manoliu as Jeremy
*George Dumitrescu as Artie
*Maria Dinulescu as Shelby
*Catalin Paraschiv as Skeet
*Aimee-Lynn Chadwick as Becky
*Radu Romaniuc as Brett
*Sebastian Marina as Dartagnan
*Violeta Aldea as Rainbow
*Ricky Dandel as Coach Savini
*Allan Trautman (unconfirmed) as Tarman (cameo)

==Production==
The movie was filmed immediately after   using the same locations in Romania and Ukraine. Peter Coyote, Aimee-Lynn Chadwick, Cory Hardrict, and John Keefe return from the previous installment.

An edited version of the film aired along with Return of the Living Dead: Necropolis on the Syfy|Sci-Fi Channel on  . The R-rated version of the film was released on DVD on   by Lions Gate Entertainment|Lionsgate.

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 