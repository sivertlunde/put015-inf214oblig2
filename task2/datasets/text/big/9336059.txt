Wives Under Suspicion
{{Infobox film
| name           = Wives Under Suspicion
| image          =Wives Under Suspicion poster3.jpg
| image size     =
| caption        = Theatrical release poster
| director       = James Whale
| producer       = Edmund Grainger Tom Barry Myles Connolly
| narrator       =
| starring       = Warren William Gail Patrick
| music          = Charles Previn
| cinematography = George Robinson
| editing        = Charles Maynard
| distributor    = Universal Pictures
| released       =  
| runtime        = 69 min.
| country        = United States English
| budget         =
}} public domain (in the USA) due to the Universal Pictures failure to renew its copyright registration in the 28th year after publication. 

==Plot==
A district attorney (William) realizes that his own wife (Patrick) might be having an affair while he is prosecuting a cuckolded murderer.

==Earlier version==
Wives Under Suspicion is a remake of a film also directed by Whale, The Kiss Before the Mirror (1933). Ralph Morgan, brother of Frank Morgan, who plays the prosecutor in The Kiss Before the Mirror, appears in the remake.

==Cast==
*Warren William as District Attorney Jim Stowell
*Gail Patrick as Lucy Stowell
*Constance Moore as Elizabeth
*William Lundigan as Phil
*Ralph Morgan as Professor Shaw MacAllen
*Cecil Cunningham as "Sharpy"
*Samuel S. Hinds as David Marrow
*Milburn Stone as Eddie Kirk
*Lillian Yarbo as Creola
*Jonathan Hale as Dan Allison

==References==
 

==External links==
* 
* 
* 
*   complete film on YouTube

 

 
 
 
 
 
 
 
 
 


 