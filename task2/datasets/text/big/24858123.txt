Earthly Love
{{Infobox film
| name           = Earthly Love
| image          =      

| image_size     =  Movie poster Earthly Love 
| alt            =
| caption        = 
| director       = Yevgeny Matveyev
| producer       = 
| writer         = Yevgeny Matveyev Valentin Chernykh Pyotr Proskurin
| narrator       = 
| starring       = Yevgeny Matveyev Olga Ostroumova  Yury Yakovlev  Valeriya Zaklunnaya Zinaida Kiriyenko
| music          = Yevgeny Ptichkin Victor Yakushev, Genadiy Tsekaviy
| editing        = 
| studio         = Mosfilm
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}}
 State Prize of the RSFSR for Eathy Love and Destiny (1977 film)|Destiny. 

==Plot==
A story about love, married the late chairman of the collective farm Zakhar Derugin (Yevgeny Matveev) to the young woman Maria Polivanova (Olga Ostroumova) at the height of the harvest. Parallel developing romance between Catherine and her sister Derugin secretary of the District Party Committee Bryukhanov. The film shows the life of the Soviet countryside 1930s.

==Cast==
*Yevgeny Matveyev   -     Zakhar Deriugin
* Zinaida Kiriyenko 	-     Efrosinya
*Yury Yakovlev	-     Tikhon
*Valeriya Zaklunnaya -    Katerina
*Olga Ostroumova	-     Manya 
*Irina Skobtseva	-     Elizaveta Polivanova
*Viktor Khokhryakov	-     Chubarov
* Vladimir Samojlov -	Rodion Anisimov
* Stanislav Chekan 	- Koshev	
* Aleksandr Potapov  -	Nikita Bobok
* Muza Krepkogorskaya -	Varya Chernaya
* Valentin Bryleyev -	Kudelin	
* Yuri Leonidov - 	Tikhomirov
*Vadim Spiridonov - Fedor Makashin
* Ivan Bondar - Proshka
* Alexander Degtyar -	supplier	
* Roman Filippov	 - Baturin	
*Zoya Isayeva	-	collective farmer	
*Valentina Klyagina - Nadya
*  Yura Kovalyov  
* Aleksandr Lebedev	- 	Semyon
*Aleksandr Lukyanov 	-	collective farmer
*Viktor Markin	 	
*Yuri Martynov 	- 	District Committee member	
*Ivan Matveyev - a short  collective farmer
*Grigory Mikhaylov 	- construction worker
*Vladimir Nosik	- Yurka	
*Lidiya Olshevskaya -	mother Zakhar
* Misha Orlov - 
*Dmitri Orlovsky - District Committee member
*V. Pikalova - 		member of the Bureau of the District Committee
*Vladimir Pitsek - 	called on the Office of the District Committee
*Vladimir Plotnikov		- Kiryan Polivanov
*Viktor Shulgin	-	District Committee member
*Nikolai Smorchkov -	Vasiliy	
*Lyubov Sokolova	-	District Committee member
*Viktor Uralsky	-	man at reception
*Valentina Ushakova	-	Nyurka Bobok
*Kirill Uspensky  		
*Aleksandr Vigdorov 	- Mitry Polivanov
*Nikolai Vikulin - 	Vanya Deriugin
*Valentina Vladimirova	- Nina Ivanovna, secretary Bryukhanov 	
*Nikolai Yudin - grandfather Makar
* Valentin Chernyak - Kulikov
*Zoya Vasilkova - Emelyanova
* Vladimir Protasenko - NKVD officer (uncredited) Yevgeny Nikitin - German soldier (uncredited)

==References==
 

==External links==
* 
*   
*   

 
 
 
 
 