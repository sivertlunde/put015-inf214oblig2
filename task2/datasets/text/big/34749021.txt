Disobedience (film)
{{Infobox film
| name        = Disobedience
| director    = Licínio Azevedo
| producer    = Ebano Multimedia
| starring    = Rosa Castigo Tomás Sodzai
| music       = João Carlos Schwalbach
| editing     = Orlando Mesquita
| distributor = Marfilmes
| released    =  
| runtime     = 92 minutes
| country     = Mozambique
| language    = Portuguese language|Portuguese, Ximanica
}}
Disobedience is a 2003 drama film directed by Licínio Azevedo.

==Synopsis==
Rosa, a Mozambican peasant, is accused by the family of her husband of having caused his suicide by refusing to obey him. She is suspected of having a "spirit-husband". In order to prove her innocence, Rosa submits herself to two trials: one by a traditional healer, the other in court. She is absolved twice. Nevertheless, the dead man relatives continue to persecute her and she is punished in a cruel way.
The film is interpreted by the real players of this story, the dead man by his twin brother.

During the shooting, the director decided to install a second camera to follow the never ending plot of revenge. Such a complex editing, a mix of feature film and documentary - has no parallel in African cinema. 

According to the director It was a difficult task to convince both families – the wife’s and the dead husband’s – to participate in the film, since the conflict was far from being over. On the other hand, they had never seen a film, and, as I found out as soon as we began shooting, they hadn’t quite realized the purpose of their participation. They simply wanted to seize the opportunity to relive the events in order to get even once and for all. Every time there was a pause in the shooting the conflict would resume on a parallel level, with unpredictable events on both sides. This made me bring a second camera into the scene in order to register such events and those situations in which the main characters would ignore the script and try to introduce other elements, or simply refuse to stick to it. Since it was so closely related to the story, this “making of” became a fundamental element in the dramatic structure of the film. 

==Festivals==
* Africa in the Picture, The Netherlands  
*   (2008)
* Kino Afrika, Norway
* Festival International du Film dAmiens, France
*   (2006)

==Awards==
*    (2003)
* Kuxa Kanema Award at FUNDAC - Fundo Para o Desenvolvimento Artístico Cultural, Mozambique
* Special Mention of the Jury for the interpretation of Rosa Castigo - International Film Festival of Zanzibar, Tanzania

==See also==
*Lobolo

==References==
 

==External links==
*  
*   Jornal O Globo
*   in Africine
*  

 
 
 
 