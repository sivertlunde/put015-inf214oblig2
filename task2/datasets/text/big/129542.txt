My Left Foot
 
{{Infobox film
| name = My Left Foot
| image = My_Left_Foot.jpg
| caption = Theatrical release poster
| director = Jim Sheridan
| producer = Noel Pearson My Left Foot by Christy Brown
| screenplay = Shane Connaughton Jim Sheridan
| starring = Daniel Day-Lewis Ray McAnally Brenda Fricker Fiona Shaw Hugh OConor
| music = Elmer Bernstein Jack Conroy
| editing = J. Patrick Duffner
| distributor = Granada Films   Miramax Films
| released =   
| country = Ireland
| runtime = 103 minutes
| language = English
| budget = £600,000
| gross = $14,743,391 
}} Irishman born book of the same name by Christy Brown. 
 Best Actress Best Adapted Best Director for Sheridan and the Academy Award for Best Picture.

==Plot==

The film opens with Christy Brown, who has cerebral palsy, being taken to a charity event, where he meets his handler, a nurse named Mary Carr. She begins reading his autobiography. Christy could not walk or talk, but still received love and support from his family, especially his mother. One day, while Christy was still a young boy, he is the only person home to see his mother fall down a flight of stairs while in labor. He is able to get the attention of some neighbors, who come to his mothers rescue. His father, who had never really believed in Christy, becomes a supporter when, one day, when he is about ten, Christy uses his left foot (the only part of his body he can fully control) to write the word "mother" on the floor with a piece of yellow chalk.

Consequently, Christy seeks a hobby in painting. He is included by the young people in his neighborhood in their activities, such as playing street football, etc. However, when he paints a picture and gives it to a girl he likes, she returns it to him. His father loses his job and the family faces exceptionally difficult hardships. Christy, to his mother’s dismay, devises a plan to help his brothers steal coal.  Christy’s elder sister, who was always very nice to him, gets pregnant and has to get married and leave home. Christys mother, who had been gradually gathering some savings in a tin in the fireplace, finally saves enough to buy him a wheelchair.

Christy meets Dr. Eileen Cole, who takes him to her school for cerebral palsy patients and persuades a friend of hers to hold an exhibition of his work. Christy falls in love with Dr. Cole, but in the subsequent dinner, he learns she is engaged to be married. As a result, Christy considers suicide. He and his mother build Christy his own private studio, but his father soon thereafter dies of a stroke. During the wake, Christy instigates a brawl. At this point, Christy starts writing his autobiography, My Left Foot. Dr. Cole returns and they resume their friendship. Meanwhile, at the fete, Christy asks Mary Carr to go out with him and they leave the fete together.

==Cast==
* Daniel Day-Lewis as Christy Brown
* Brenda Fricker as Bridget Brown
* Ray McAnally as Paddy Brown
* Fiona Shaw as Dr. Eileen Cole
* Hugh OConor as young Christy Brown
* Kirsten Sheridan as Sharon Brown
* Alison Whelan as Sheila Brown
* Eanna MacLiam as Benny Brown
* Declan Croghan as Tom Brown
* Marie Conremme as Sadie Brown
* Cyril Cusack as Lord Castlewelland
* Phelim Drew as Brian
* Eileen Colgan as Nan
* Ruth McCabe as Mary Carr

==Production==
Many scenes were filmed through a mirror, as Daniel Day-Lewis could only manipulate his right foot to perform the actions seen in the film. Day-Lewis spent some time preparing for the film at Christy Browns alma mater in Dublin. He later returned there for a visit, with his Academy Award|Oscar. 

==Reception==

===Critical response===
Upon its initial release, My Left Foot received positive reviews. Review aggregator Rotten Tomatoes reports that 97% of 34 film critics have given the film a positive review, with a rating average of 8.1 out of 10. 

===Accolades=== Best Actress Best Director, Best Picture Best Writing, NYFCC Best Picture Award for 1989.

{| class="wikitable"
! colspan="5" | List of awards and nominations
|- Award
! Date of ceremony Category
! Recipients and nominees Result
|- Academy Awards   
| rowspan="5" | 26 March 1990
| Best Actor
| Daniel Day-Lewis
|  
|-
| Best Supporting Actress
| Brenda Fricker
|  
|-
| Best Picture
| Noel Pearson, producer
|  
|-
| Best Director
| Jim Sheridan
|  
|-
| Best Adapted Screenplay
| Shane Connaughton & Jim Sheridan
|  
|- BAFTA Film Awards   
| rowspan="5" | 1990
| Best Actor
| Daniel Day-Lewis
|  
|-
| Best Supporting Actor
| Ray McAnally
|  
|-
| Best Film
| My Left Foot
|  
|-
| Best Adapted Screenplay
| Shane Connaughton & Jim Sheridan
|  
|-
| Best Makeup
| Ken Jennings
|  
|- European Film Awards   
| rowspan="3" | 25 November 1989
| Young European Film of the Year
| My Left Foot
|  
|-
| Best Director
| Jim Sheridan
|  
|-
| Best Actor
| Daniel Day-Lewis
|  
|- Golden Globe Awards   
| rowspan="2"| 20 January 1990
| Best Actor
| Daniel Day-Lewis
|  
|-
| Best Supporting Actress
| Brenda Fricker
|  
|- Independent Spirit Awards   
| 24 March 1990
| Best Foreign Film
| My Left Foot
|  
|- Los Angeles Film Critics   
| rowspan="2" | 16 January 1990
| Best Actor
| Daniel Day-Lewis
|  
|-
| Best Supporting Actress
| Brenda Fricker
|  
|- National Film Critics 
| 8 January 1990
| Best Actor
| Daniel Day-Lewis
|  
|- New York Film Critics   
| rowspan="2"| 14 January 1990
| Best Film
| My Left Foot
|  
|-
| Best Actor
| Daniel Day-Lewis
|  
|- Young Artist Awards   
| rowspan="2"| March 1990
| Best Young Actor Supporting Role in a Motion Picture
| Hugh O’Conor
|  
|-
| Best Motion Picture: Drama
| My Left Foot
|  
|}

==See also==
* BFI Top 100 British films

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 