I Am the Law (1938 film)
{{Infobox film
| name       = I Am the Law
| director   = Alexander Hall
| image      = I Am The Law 1938 poster.jpg
| image_size = 220
| caption    = 1938 Theatrical Poster
| writer     = Jo Swerling
| starring   = Edward G. Robinson
| music      = George Parrish
| cinematography = Henry Freulich
| editing    = Viola Lawrence
| distributor = Columbia Pictures
| released   =  
| runtime    = 83 minutes
| country    = United States
| language   = English
| budget     =
| gross      =
}}
I Am the Law (1938) is a crime drama directed by Alexander Hall.

==Plot==
New York City law professor John Lindsay (Robinson) is asked by Eugene Ferguson (Kruger), a member of the governors Civic Committee, to become a special prosecutor to fight racketeers and corruption in the city, but unknown to Lindsay, Ferguson is in association with the racketeers. 

==Cast==
* Edward G. Robinson as John Lindsay
* Barbara ONeil as Jerry Lindsay John Beal as Paul Ferguson
* Wendy Barrie as Frances Ballou
* Otto Kruger as Eugene Ferguson
* Arthur Loft as Tom Ross
* Marc Lawerence as Eddie Girard Douglas Wood as  D.A.  Bert Beery
* Robert Middlemass as Moss Kithell
* Ivan Miller as Inspector  Gleason
* Charles Halton as George Leander
* Louis Jean Heydt as J.W Butler
* Fay Helm as Mrs. Butler

==References==
 

==External links==
* 

 

 
 
 
 
 


 