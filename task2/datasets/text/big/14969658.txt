Angel (2007 film)
 
{{Infobox film
| name           = Angel
| image          = Angel poster.jpg
| caption        = Theatrical release poster
| director       = François Ozon
| producer       = Olivier Debosc Marc Missonnier
| writer         = François Ozon Dialogue: Martin Crimp François Ozon
| based on       = 
| starring       = Romola Garai Sam Neill Charlotte Rampling Lucy Russell Michael Fassbender
| music          = Philippe Rombi
| cinematography = Denis Lenoir
| editing        = Muriel Breton
| studio         = Fidélité Films Poisson Rouge Pictures Scope Pictures
| distributor    = Wild Bunch Distribution (France) Cinemien IFC Films (US)
| released       =  
| runtime        = 134 minutes
| country        = Belgium France United Kingdom
| language       = English 
| budget         = 
| gross          = $2,822,161 
}} novel of Elizabeth Taylor, about the life of a fiery and passionate young writer. The protagonist was portrayed by Romola Garai; other characters were played by Sam Neill, Michael Fassbender and Charlotte Rampling.

==Plot==
Angel Deverell (Romola Garai) is considered an outsider in the town of Norley. She has a fanciful imagination and prefers to be alone, writing. Her mother, a shopkeeper, and her aunt, who works for the family that lives in the grand house called Paradise, dont understand her.

She finishes writing her novel, "Lady Irinia," and sends it off. Theo Gilbright (Sam Neill) offers to publish her novel, but he requires that she make some changes. Angel refuses and leaves in tears. Theo comes after her and offers to publish the novel as it is. That evening, they dine at Theos house, where Theos wife, Hermione (Charlotte Rampling), takes an immediate dislike to Angel.
 Lucy Russell), who is a great admirer of Angel, and her brother, Esmé  (Michael Fassbender), an artist. Angel buys Paradise and hires Nora as her personal secretary. She marries Esmé. The Great War breaks out. Esmé enlists and Angel is heartbroken.

==Cast==
* Romola Garai as Angel Deverell.
* Sam Neill as Théo Lucy Russell as Nora Howe-Nevinson
* Michael Fassbender as Esmé
* Charlotte Rampling as Hermione
* Jacqueline Tong as Mother Deverell
* Janine Duvitski as Aunt Lottie
* Christopher Benjamin as Lord Norley
* Tom Georgeson as Marvell
* Simon Woods as Clive Fennelly
* Jemma Powell as Angelica
* Alison Pargeter as Edwina
* Seymour Matthews as Norley Doctor
* Una Stubbs as Miss Dawson
* Rosanna Lavelle as Lady Irania
* Geoffrey Streatfield as Sebastian
* Roger Morlidge as Journalist
* Cinsyla Key as Party guest
* Teresa Churcher as Governess
* Alexandre Garcia-Hidalgo as Party Guest
* Roland Javornik as Party guest
* David Vanholsbeeck as Wounded Soldier in Cafe
* Animal handling by Gill Raddings Stunt Dogs
* Seamus as Sultan

==Production==
The director gave an interview about the film, stating that "the character of Angel was inspired by   for the role of Esmé, Angels love interest,  and when talking about Lucy Russell, who played Nora, he stated: "I saw a lot of actresses for the role of Nora. During the screen tests, I realized that many of them actually wanted to be Angel. As soon as theyd finish reading, theyd say to me "I could play Angel too, I am Angel!" They had no desire to play a supporting role, whereas Lucy Russell didnt mind. She showed up for her screen test dressed like an old maid, with thick glasses and her hair in a strict bun. She was actually there to play Nora! Of course the role is far less glamorous than that of Angel, but Lucy was smart enough to know that it is often the person in the shadows who gets noticed, even if shes not the one wearing the beautiful dresses. And like Charlotte, Lucy speaks fluent French, so she was my second crutch on the set!" 

Reportedly, the outdoor scenes were shot in bitterly cold weather, and the camera would freeze after a few minutes of shooting, and had to be taken inside and warmed up with hot towels before taken outside again. The actors did not mind this as they too got the chance to warm up.

== Reception ==
Angel received mixed reviews, with a 50% Fresh rating on Rotten Tomatoes.  However, Matthew Turner of View London called it an "Enjoyable, impressively directed melodrama that stays just the right side of kitsch and features a superb performance by Romola Garai."  Whilst also stating that "The films biggest problem is that Angel is such a thoroughly obnoxious character that its impossible to engage with her on an emotional level", he ended saying that "Ozons films are always worth watching and Angel is no exception, thanks to great direction and a committed performance from Garai."

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 