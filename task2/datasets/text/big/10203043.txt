The Scarlet Horseman
{{Infobox Film
| name           = The Scarlet Horseman
| image_size     = 
| image	=	The Scarlet Horseman FilmPoster.jpeg
| caption        =  Ray Taylor
| producer       = Morgan Cox Joseph ODonnell
| narrator       =  Paul Guilfoyle Janet Shaw Virginia Christine
| music          = Milton Rosen
| cinematography = Jerome Ash George Robinson
| editing        = Irving Birnbaum Jack Dolan D. Pat Kelley Alvin Todd Edgar Zane
| distributor    = Universal Pictures 1946
| runtime        = 13 chapters (221 min)
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Universal Serial film serial.

==Plot== Texas Rangers investigate the kidnapping of wives and daughters of United States Senate|Senators.  In order to do so, one goes undercover as "The Scarlet Horseman", a legendary and respected Comanche figure.  The villainess, Matosca, intends to use the kidnappees to force a partition of Texas.

==Cast==
* Peter Cookson as Kirk Norris, a Texas Ranger Paul Guilfoyle as Jim Bannion, a Texas Ranger secretly using the identity of The Scarlet Horseman Janet Shaw as Elise Halliday, heroine
* Virginia Christine as Carla Marquette, daughter of a discredtied Senator and secretly the villainous Matosca
* Victoria Horne as Loma
* Cy Kendall as Amigo Mañana Edward Howard as Zero Quick
* Harold Goodman as Idaho Jones
* Danny Morton as Ballou, saloon owner  Helen Bennett as Mrs. Ruth Halliday  Jack Ingram as Tragg, one of Matoscas henchmen
* Edmund Cobb as Kyle, one of Matoscas henchmen
* Guy Wilkerson as Panhandle, one of Matoscas henchmen Al Woods as Senator Mark Halliday
* Fred Coby as Tioga Ralph Lewis as Saloon Henchman

==Chapter titles==
# Scarlet for a Champion
# Dry Grass Danger
# Railroad Rescue
# Staked Plains Stampede
# Death Shifts Passengers
# Stop that Stage
# Blunderbuss Broadside
# Scarlet Doublecross
# Doom Beyond the Door
# The Edge of Danger
# Comanche Avalanche
# Staked Plains Massacre
# Scarlet Showdown
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 242
 | chapter = Filmography
 }} 
The opening narration to each chapter was by Milburn Stone.

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{succession box  Universal Serial Serial 
| before=The Royal Mounted Rides Again (1945)
| years=The Scarlet Horseman (1946)
| after=Lost City of the Jungle (1946)}}
 

 

 
 
 
 
 
 
 
 
 
 


 