Kranti Kshetra
 
{{Infobox film
 | name = Kranti Kshetra
 | image = Krantikshetra.jpg
 | caption = DVD Cover
 | director = Rajeev Kumar
 | producer = Saptarishi Rajeev Kumar
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Pooja Bhatt Harish Kumar Shakti Kapoor Gulshan Grover
 | music = Nadeem-Shravan
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = August 26, 1994
 | runtime = 140 min.
 | language = Hindi Rs 5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1994 Hindi Indian feature directed by Rajeev Kumar, starring Mithun Chakraborty, Pooja Bhatt, Harish Kumar, Shakti Kapoor and Gulshan Grover

==Plot==

Kranti Kshetra is a Campus based Action thriller starring Mithun Chakraborty with Shakti Kapoor and Gulshan Grover

==Cast==

*Mithun Chakraborty    	
*Pooja Bhatt 	
*Harish
*Kanchan 			
*Adnan 			
*Siddhant 		
*Gulshan Grover 
*Shakti Kapoor 	 		
*Haiwan Singh
*Laxmikant Berde 	
*Shiva Rindani 			
*Shankara 
*Puneet Issar 			
*Shaitan Singh 
*Rakesh Bedi 		
*Avtar Gill 	 	
*Minister
*Shekhar Navre 			
*Prem Pardeshi
*Guddi Maruti 		 	
*Gurpreet

==Soundtracks==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Malan Thara Baag Mein
| Vinod Rathod, Babul Supriyo, Sapna Awasthi, Sapna Mukherjee, Suryakant 
|- 
| 2
| Jaaneman Yeh Geet Nahin 
| Kumar Sanu
|- 
| 3
| Thoda Whisky Thoda Rum
| Udit Narayan, Chandrashekhar, Yunus Parvez
|- 
| 4
| Tumhara Naam Kya Hai 	
| Vinod Rathod, Sadhana Sargam
|- 
| 5
| Shor Machaongi
| Babul Supriyo, Sapna Mukherjee
|}

==References==
*  http://ibosnetwork.com/asp/filmbodetails.asp?id=Kranti+Kshetra
*  http://www.imdb.com/title/tt0205189/fullcredits#cast

==External links==
*  

 
 
 
 
 
 


 