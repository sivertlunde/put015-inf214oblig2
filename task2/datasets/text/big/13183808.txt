Hälsoresan – En smal film av stor vikt
{{Infobox Film
| name           = Hälsoresan - En smal film av stor vikt
| image          = Hälsoresan.jpg
| caption        = DVD cover
| director       = Lasse Åberg Bo Jonsson
| writer         = Bo Jonsson, Lasse Åberg 
| starring       = Lasse Åberg, Jon Skolmen
| music          =  , Janne Schaffer
| cinematography = 
| editing        = 
| distributor    = Svensk Filmindustri
| released       =  
| runtime        = 105 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
Hälsoresan – En smal film av stor vikt ( ) is a Swedish comedy film, released in 1999 and directed by Lasse Åberg.

== Synopsis ==
The film is about the health resort Granhedsgården in Dalarna, Sweden, and they have a problem. They dont have many guests at all and something must be done. At the same time, Stig-Helmer is a little depressed after his girlfriend has left him, and so he lives on junk food. His best friend Ole invites him to come along to the health resort Granhedsgården. Now, the craziness begins.

== Cast ==
*Lasse Åberg - Stig-Helmer Olsson
*Jon Skolmen - Ole Bramserud
*Magnus Härenstam - Dr. Levander

== External links ==
* 
* 

 

 
 
 
 
 
 


 
 