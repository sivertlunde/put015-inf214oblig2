Nightworld: Lost Souls
{{Infobox Film 
| name           = Lost Souls
| image          = 
| caption        =  Scott Peters John Savage   Barbara Sukowa   Richard Lintern   Ted Rusoff
| director       = Jeff Woolnough 
| producer       = Ken Gord  Jonathan Goldsmith
| distributor    = 
| released       = November 12, 1998
| runtime        = 90 minutes
| country        = United States
| language       = English
| movie_series   = Nightworld
| awards         =
| budget         =
}}
 Fox Family 13 days of Halloween special in October.

On June 15, 2010, all the Nightworld films were released in the US as double feature DVDs. Lost Souls is being released as a double feature DVD with Marked, a 2007 film.

Lost Souls has also been released again in 2011 as a double feature DVD with Darkness, a 2002 film.

==Synopsis== Edison invention and begins to play it, he hears the sounds of children laughing and playing. This is followed by Victors autistic 12-year-old daughter, Meaghan, painting and singing; this is surrounded by strange occurrences around the house. When Victor discovers that two children were murdered in the area years before, he believes they are trying to contact him; he also believes that their neighbor is responsible for the murders.

== Cast ==
 John Savage: Victor Robinson
* Barbara Sukowa: Sheila Robinson
* Nicholas Diegman: Jesse Robinson
* Laura Harling: Meaghan Robinson
* Richard Lintern: Graham Scofield
* Robert Sherman : George Giffard
* Jean-François Wolff: Elson Garrett
* Ted Rusoff: Humphrey Garrett
* Christian Erickson: Jack Mennias
* Claudette Roche: Dr. Hollings
* Gary Beadle: Stuart Markle
* Mark OHagan: Young Elson

==References ==
 

==External links==
*  


 
 

 
 
 
 

 
 