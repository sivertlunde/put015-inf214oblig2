Fat Kid Rules the World (film)
{{Infobox film
| name           = Fat Kid Rules the World
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Matthew Lillard
| producer       = Jane Charles Matthew Lillard Jennifer Maas Nick Morton Rick Rosenthal Talan Torriero Evan Wasserstrom
| screenplay     = Michael M.B. Galvin Peter Speakman
| based on       =  
| starring       = Jacob Wysocki Matt OLeary Billy Campbell
| music          = Mike McCready
| cinematography = Noah Rosenthal
| editing        = Michelle M. Witten
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 book of the same name and stars Jacob Wysocki, Matt OLeary & Billy Campbell.

==Plot==

17 year old Troy Billings is overweight and suicidal. Hes saved by Marcus from jumping in front of a bus and begins an uneasy friendship with Troy. Marcus then enlists the musically challenged Troy to become the drummer in a new punk rock band. As Troys relationship with Marcus grows, Troys father becomes increasingly concerned about his sons new friendship.

==Cast==

*Jacob Wysocki as Troy
*Matt OLeary as Marcus
*Billy Campbell as Mr. Billings
*Dylan Arnold as Dayle
*Tyler Trerise as Manoj
*Russell Hodgkinson as Marcus Stepfather
*Julian Gavilanes as Matt
*Sean Donavan as Ollie
*Lili Simmons as Isabel
*Jeffrey Doornbos as Mr. Sherman
*Matthew Lillard as Guidance Counsellor

==Reception==

Fat Kid Rules the World received positive reviews from critics and audiences, earning an 83% approval rating on Rotten Tomatoes.

==External Links==

*  at Internet Movie Database
*  at Rotten Tomatoes

 