The Real Howard Spitz
{{Infobox Film 
| name  = The Real Howard Spitz
|  writer          = Jurgen Wolff 
|  starring        = Kelsey Grammer Amanda Donohoe Genevieve Tessier Joseph Rutten Patrick McKenna 
|  director        = Vadim Jean 
|  producer        = Paul Brooks 
| editing          = Pia Di Ciaula
|  distributor     = New Line Cinema 
|  released = August 28, 1998 (USA) 
|  runtime         = 102 min. 
|  language  = English 
|}} 1998 film starring Kelsey Grammer.

==Plot==

Howard Spitz (Kelsey Grammer) is the author behind a string of poorly selling detective novels. He discovers that in contrast childrens books enjoy strong sales. Believing it an easy way to make money, Spitz becomes a childrens author with his new book character, a bovine detective named "Crafty Cow", but finds writing for his new audience significantly difficult. Whilst doing research at the local library, he submits his drafts to a little girl called Samantha Kershaw (Genevieve Tessier). In return, she asks Spitz to find her father who left her mother (Amanda Donohoe) before she was born. Spitz also discovers that to become a successful childrens author he will need to do public appearances with his audience. Terrified at the prospect of having to spend time with children, Spitz hires a struggling actor to serve as his public face. But soon his doppelganger is having delusions of grandeur, as his book becomes more and more successful. Finally, at an awards ceremony, he confesses his identity, and finds that after spending so much time with Samantha, and after all the help shes given him, he feels more at ease around children.

==Cast==
*Kelsey Grammer — Howard Spitz
*Amanda Donohoe — Laura Kershaw
*Genevieve Tessier — Samantha Kershaw
*Joseph Rutten — Lou
*Patrick McKenna — Roger
*Kay Tremblay — Theodora Winkle
*David Christoffel — Bill Sellers

==External links==
* 

 
 
 
 


 