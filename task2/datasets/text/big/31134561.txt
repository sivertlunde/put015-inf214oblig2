Charley's Aunt (1930 film)
{{infobox film
| title          = Charleys Aunt
| image          = Lccharleysaunt.jpg
| caption        = advertisement
| director       = Al Christie
| producer       = Al Christie Charles Christie
| writer         =   (screenplay, dialogue)
| cinematography = Gus Peterson Harry Zech Leslie Rowson
| editing        =
| distributor    = Columbia Pictures
| released       = December 25, 1930
| runtime        = 88 minutes, 9 reels (7,890 feet)
| country        = USA
| language       = English
}}
Charleys Aunt is a 1930 American comedy film directed by Al Christie and starring Charles Ruggles, June Collyer and Hugh Williams.  It was an adaptation of the play Charlies Aunt by Brandon Thomas. It marked the film debut of Williams, who then returned to Britain and became a major star.  

==Cast==
* Charles Ruggles - Lord Fancourt Babberley
* June Collyer - Amy Spettigue
* Hugh Williams - Charlie Wykeham
* Doris Lloyd - Donna Lucia D Alvadorez
* Halliwell Hobbes - Stephen Spettigue
* Flora le Breton - Ela Delahay
* Rodney McLennan - Jack Chesney
* Phillips Smalley - Sir Francis Chesney
* Flora Sheffield - Kitty Verdun
* Wilson Benge - Brassett

==References==
 

==External links==
* 
* 

==Bibliography==
* Sweet, Matthew. Shepperton Babylon: The Lost Worlds of British Cinema. Faber and Faber, 2005.

 

 
 
 
 
 
 
 
 


 