Transcendence (2014 film)
 

{{Infobox film
| name           = Transcendence
| image          = Transcendence2014Poster.jpg
| image_size     = 220px
| alt            = 
| caption        = Theatrical release poster
| director       = Wally Pfister
| producer       = {{plainlist|
* Broderick Johnson Andrew A. Kosorve
* Kate Cohen
* Marisa Polvino
* Annie Marter
* David Valdes
* Aaron Ryder }} 
| writer         = Jack Paglen
| starring       = {{plainlist|
* Johnny Depp
* Rebecca Hall
* Paul Bettany
* Kate Mara
* Cillian Murphy
* Cole Hauser
* Morgan Freeman }}
| music          = Mychael Danna 
| cinematography = Jess Hall
| editing        = David Rosenbloom
| studio         = {{plainlist|
* Alcon Entertainment
* DMG Entertainment
* Straight Up Films }}
| distributor    = {{plainlist|
* Warner Bros. Pictures
*  
* Summit Entertainment
*   }}
| released       =  
| runtime        = 119 minutes  
| country        = United States China
| language       = English
| budget         = $100 million 
| gross          = $103 million    
}}

Transcendence is a 2014 science fiction film directed by cinematographer Wally Pfister in his directorial debut, and written by Jack Paglen. The English-language co-production stars Johnny Depp, Rebecca Hall, Paul Bettany, Kate Mara, Cillian Murphy, Cole Hauser and Morgan Freeman. Pfisters usual collaborator, Christopher Nolan, served as executive producer on the project.
 the Black List, a list of popular but unproduced screenplays in Hollywood. Transcendence was a disappointment at the box office, grossing only slightly more than its $100 million budget. The film received mainly negative reviews; it was criticized for its plot structure, characters and dialogue.

==Plot== sentient computer, he predicts that such a computer will create a technological singularity, or in his words "Transcendence". His wife Evelyn (Rebecca Hall), whom he loves, is his "partner in science as well as in life".

The terrorist group "Revolutionary Independence From Technology" (R.I.F.T.) has one of their members shoot Will with a polonium laced bullet and carry out a series of synchronized attacks on AI laboratories across the country. Will is given no more than a month to live.

In desperation, Evelyn comes up with a plan to upload Wills consciousness into the quantum computer that the project has developed. His best friend Max Waters (Paul Bettany), also a researcher, questions the wisdom of this choice. Wills consciousness survives his bodys death in this technological form and requests to be connected to the Internet to grow in capability and knowledge. Max panics, insisting that the computer intelligence is not Will. Evelyn forces Max to leave and connects the computer intelligence to the Internet via satellite.

Max is almost immediately confronted by Bree (Kate Mara), the leader of R.I.F.T. Max is captured by the terrorists and eventually persuaded to join them. The government is also deeply suspicious of what Wills uploaded persona will do and plans to use the terrorists to take the blame for the governments actions to stop him.

In his virtual form, and with Evelyns help, Will uses his new-found vast intelligence to build a technological utopia in a remote desert town called Brightwood, where he spearheads the development of groundbreaking new technologies in the fields of medicine, energy, biology and nanotechnology. But even Evelyn begins to grow fearful of Wills motives when he displays the ability to remotely connect to and control peoples minds after they have been subjected to his nano-particles.

FBI agent Donald Buchanan (Cillian Murphy), with the help of government scientist Joseph Tagger (Morgan Freeman), prepares to stop the technological sentient entity from spreading. As Will has spread his influence to all the networked computer technology in the world, Max and R.I.F.T. develop a computer virus with the purpose of deleting Wills source code, killing him. Evelyn plans to upload the virus by infecting herself, then having Will upload her consciousness. An unfortunate side effect of the virus would be the destruction of technological civilization. All the characters, Bree, Max, Tagger, Evelyn, and even Will Caster himself, are forced to choose between uploading the virus or risking assimilation into Wills Transcendence, which holds the promise of ending pollution, disease, and human mortality.

When Evelyn goes back to the research center, she is taken aback seeing Will in a newly created organic body identical to his old one. Will welcomes her but is instantly aware that she is carrying the virus and intends to destroy him. The FBI and the activists of R.I.F.T. attack the base with artillery, fatally wounding Evelyn. Will is given the choice between healing Evelyns body, or protecting her by uploading her mind as she did his, which will also infect him with the virus. After being threatened by Bree to upload the virus or see his friend Max die, Will hesitates. Evelyn tells Will that no one should die for their mistake. Thus, Will chooses to spare the people he loves instead of saving technological civilization. As Will is dying, he explains to Evelyn that he did what he did for her: saving the planet was her wish, to learn the secrets of the universe was his. Evelyn then realizes it was Will all along and whatever he did was actually all for her. Then, the virus kills both Will and Evelyn, and a global technology collapse and blackout ensues.

Five years later, in Will and Evelyns garden outside their old home in Berkeley, Max notices that their sunflowers are the only blooming plants within it. Upon closer examination, he notices that a drop of water falling from a sunflower petal instantly cleanses a puddle of oil — and realizes that the Faraday cage has also protected a sample of Wills nano-particles.

==Cast==
* Johnny Depp as Dr. Will Caster, an artificial-intelligence researcher.
* Rebecca Hall as Evelyn Caster, Wills wife and a fellow academic. 
* Paul Bettany as Max Waters, Wills best friend. 
* Kate Mara as Bree, the leader of Revolutionary Independence From Technology (R.I.F.T.) 
* Cillian Murphy as Donald Buchanan, an FBI agent. 
* Cole Hauser as Colonel Stevens, a military officer. 
* Morgan Freeman as Joseph Tagger, an FBI agent and old friend of Evelyn. 
* Clifton Collins, Jr. as Martin 
* Cory Hardrict as Joel Edmund, a member of the R.I.F.T. Unit.   
* Josh Stewart as Paul

==Production==

===Development=== directorial debut. Jack Paglen wrote the initial screenplay for Pfister to direct,  and producer Annie Marter pitched the film to Straight Up Films.    The pitch was sold to Straight Up. By March 2012, Alcon Entertainment acquired the project.  Alcon financed and produced the film; producers from Straight Up and Alcon joined together for the film. In the following June, director Christopher Nolan, for whom Pfister has worked as cinematographer, and Nolans producing partner Emma Thomas joined the film as executive producers. 

===Financing===
The Chinese company DMG Entertainment entered a partnership with Alcon Entertainment to finance and produce the film. While DMG contributed Chinese elements to Looper (film)|Looper and Iron Man 3, it did not do so for Transcendence.   

===Casting===
By October 2012, actor Johnny Depp entered negotiations to star in Transcendence.  The Hollywood Reporter said Depp would have "a mammoth payday" with a salary of   versus 15 percent of the films gross. Pfister met with Noomi Rapace for the films female lead role and also met with James McAvoy and Tobey Maguire for the other male lead role. The director offered a supporting role to Christoph Waltz.  In March 2013, Rebecca Hall was cast as the female lead.    By the following April, actors Paul Bettany, Kate Mara, and Morgan Freeman joined the main cast.   

===Filming=== photochemical finish instead of a digital intermediate.    In addition to film, a digital master was completed in 4K resolution, and the film was additionally released in IMAX film format. Transcendence was also scheduled for a 3D release in China.  Filming officially began in June 2013,  and took place over a period of 62 days.  The majority of the movie was filmed in a variety of locations throughout Albuquerque, New Mexico.

===Music===
The musical score for the film composed by Mychael Danna was released on April 15, through WaterTower Music. A CD format of the score was released through Amazon.com.

==Release==
Transcendence was released in theaters on April 18, 2014. It was originally scheduled for April 25, 2014. 

Warner Bros. distributed the film in the United States and Canada. Summit Entertainment (through Lionsgate) is distributing it in other territories, except for China, Italy, Hong Kong, Austria, United Kingdom, Australia, Ireland, New Zealand and Germany.  DMG Entertainment, who collaborated with Alcon Entertainment to finance and develop Transcendence, distributed the film in China.  The Chinese version includes a 3D and IMAX 3D release, funded by DMG, which is done in post-production. 
===Home media===
Transcendence was released on Blu-ray and DVD on July 22, 2014. 

==Reception==
===Box office===
Transcendence grossed a $23 million in North America and $80 million in other territories for a worldwide total of $103 million, just above its $100 million budget. 

On the films opening weekend, the film grossed $4,813,369 on Friday, $3,820,074 on Saturday and $2,252,943 in North America, for a weekend grossed of $10,886,386 playing in 3,455 theaters with an average of $3,151 and ranking #4.  In CinemaScore polls conducted during the opening weekend, cinema audiences gave the film an average grade of "C+" on an A+ to F scale. 

The biggest market in other territories being China, France, South Korea where the film grossed $20.2 million, $6.45 million, $5.3 million respectively. 

===Critical reception===
Publications such as The Guardian, Forbes and International Business Times considered the film to be largely a critical failure,      with The Guardian stating that the reviews were "almost universally damning" and referring to the film as "one of 2014s bigger critical turkeys".  Film critic Mark Kermode said that the film got a "critical kicking" in the UK, much as it did in the US.  However, a number of sources considered the film to have received a mixed rather than a primarily negative reception.    

Film review aggregator   surveyed 45 critics and gave the film an aggregate score of 42 out of 100, which indicates "mixed or average" reviews. 
 Michael Atkinson of Sight & Sound identified "vast plot holes and superhuman leaps of logic", writing that "for all its gloss, hipster pretensions, plot craters and sometimes risible attempts at action, Transcendence traffics in large, troublesome ideas about Right Now and What’s Ahead, even if the film itself is far too timid and compromised to do those hairy questions justice."  William Thomas of Empire Magazine|Empire believed that the cast and director had been "wasted on a B-Movie script with pretensions of prescience", dismissing the film as a "banal sci-fi slog"; he awarded it a 2 out of 5 star "poor" rating.  David Denby of The New Yorker considered the film to be "rhythmless, shapeless, and, with the exception of a few shots, cheesy-looking".  Nigel Floyd of Film4 concluded that it was "an ambitious, old fashioned, ideas-driven science fiction film that is never as mind-expanding as its futuristic images and topical, dystopian ideas seem to promise". 

Contrary to the many less than enthusiastic reviews that the film received, several top film critics such as  Richard Roeper of the Chicago Sun-Times lauded the film and awarded it four stars, calling it a "bold, beautiful flight of futuristic speculation" and " a stunning piece of work".  Kenneth Turan of The Los Angeles Times was also positive towards the film, praising the quality of the cast and intelligence of the script.   Stuart Russell claims that despite particular technical aspects of the movie being "a non-starter", the basic premise of superhuman AI is quite possible, and that "AI researchers must, like nuclear physicists and genetic engineers before them, take seriously the possibility that their research might actually succeed and do their utmost to ensure that their work benefits rather than endangers their own species." 

===Accolades===
Despite the negative reviews, Transcendence was nominated for the following awards...

{| class="wikitable"
|-
!Award
!Category
!Subject
!Result
!Notes
|- Golden Trailer Awards Most Original Trailer Buddha Jones, Warner Bros.
| 
|rowspan=2| For the first theatrical trailer entitled "Its Me" 
|- Best Motion/Title Graphics
| 
|-
| 
| For the second theatrical trailer entitled "Singularity" 
|- Most Innovative Advertising for a Feature Film
| 
| "Spraypaint" 
|- Village Voice Film Poll Award Worst Film
| 
| Tied with Interstellar (film)|Interstellar 
|-
|}

==See also==
* The Lawnmower Man (film)

==Notes==
 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 