Drylanders
 
{{Infobox film
| name           = Drylanders
| image          = Drylanders (movie).jpg
| image_size     =
| alt            =
| caption        = Drylanders Movie Poster
| director       = Don Haldane Peter Jones-producer
| writer         = M. Charles Cohen
| narrator       = William Weintraub
| starring       =
| music          = Eldon Rathburn
| cinematography = Reginald H. Morris John Kemeny 
| studio         = National Film Board of Canada
| distributor    = Columbia Pictures
| released       = April 1963
| runtime        = 69 Minutes 
| country        = Canada
| language       =
| budget         = $218,000
| gross          =
| preceded_by    =
| followed_by    =
}}

Drylanders is a 1963 Canadian film directed by Don Haldane from a screenplay by M. Charles Cohen. It was the National Film Board of Canada’s first English-language feature film. 

==Synopsis==
Drylanders is set in Canada at the turn of the century. Daniel Greer (James Douglas) returns home after the Boer War to find city life not to his liking. Instead,he opts for the life of a wheat farmer. At first, his farm is prosperous, but he becomes victim to a nationwide drought. He struggles to keep his farm afloat, but dies before he could see the end of the drought. His wife(Frances Hyland) continues her husbands work on the farm. 

==Production==
Drylanders was a fictionalized documentary similar to earlier French-language productions from the NFBs Panoramique series and dramas in the English-language Perspective series. Heavily promoted during its release, the film was modestly successful at the box office.   

Drylanders came about after a documentary on farming and irrigation in Saskatchewan, suggested by writer Charles Cohen, had been rejected by the Canadian Broadcasting Corporation. Director Donald Haldane then suggested making a fiction film. Cohen wrote a personal story, concentrating on the trials of the "Greer" family, who had come from Montreal to try their luck at farming. 

The film was filmed in black and white, in  . It was the first time SuperScope had been used at the NFB. Filming began in the summer of 1961, shooting in and around Swift Current, Saskatchewan. Several stage actors from Toronto were brought in for the principal roles, including Hyland and Douglas. Local actors were signed for some of the smaller supporting roles. Filming on the Prairies took several weeks. A key blizzard scene was shot in winter of 1962 at the NFB studio in Montreal. The production budget rose to about $218,000, roughly twice what was planned. 

==Release==
Columbia Pictures distributed the film, premiering Drylanders in Swift Current, then releasing across the Prairies, a couple of towns at a time. The film was also released in British Columbia and Eastern Canada, eventually playing in more than 500 cinemas nationwide.   

The film was offered as part of a double feature or accompanied by the NFB documentary Fields of Sacrifice. A French-language version, Un autre pays, played across Quebec. The film was released theatrically in the United Kingdom, United States and Central and South America. It would also play on television in Switzerland, Yugoslavia, China and Malaysia, among other countries. 

It played theatrically throughout 1963 and 1964 before making its way to the non-theatrical circuit, where it was shown in schools and community centres on 16&nbsp;mm. It later enjoyed a second career on television and home video.   

==Cast==
Frances Hyland as  Liza  
James Douglas as  Daniel Greer  
Lester Nixon as  Bob McPherson  
Mary Savage as  Ada McPherson  
William Fruet as  Colin (as William Fruete)  
Don Francks as  Russel  
Iréna Mayeska as  Thora  
William Weintraub as  Narrator (voice)

==References==
 

==External links==
* 
* 
*  New York Times review]

 
 
 
 
 
 
 
 
 
 