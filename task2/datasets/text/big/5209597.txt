The Terminal Man (film)
 
{{Infobox film
| name           = The Terminal Man
| image          = Terminalmanposter.jpg
| caption        = Theatrical release poster
| director       = Mike Hodges
| producer       = Mike Hodges
| based on         =  
| writer = Mike Hodges William Hansen Jill Clayburgh
| music          = 
| cinematography = Richard H. Kline
| editing        = Robert L. Wolfe
| distributor    = Warner Bros.
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1974 film novel of the same name by Michael Crichton. It stars George Segal. The story centers on the immediate dangers of mind control and the power of computers.

== Plot ==
 IQ 144) computer programmer in his 30s, suffers from epilepsy. He often has seizures which induce a blackout, after which he awakens to unfamiliar surroundings with no knowledge of what he has done. He also suffers from delusions that computers will rise up against humans.

Benson suffers from Acute Disinhibitory Lesion (ADL) syndrome, and is a prime candidate for an operation known as "Stage Three". Stage Three requires surgeons to implant electrodes in his brain and connect them to a miniature computer in his chest which is meant to control the seizures. The operation is presented with no musical score; the only sounds are from the surgeons, from the medical procedure itself, and from medical students viewing from above. The surgery is a success.

Bensons psychiatry|psychiatrist, Janet Ross, is concerned that once the operation is complete, Benson will suffer further psychosis as a result of his person merging with that of a computer, something he has come to distrust and disdain. Shortly before he can fully recover, Harry suffers a relapse and his electrode malfunctions while his brain has more severe seizures, making him more violent and dangerous.

==Production==
Crichton was originally hired to adapt the novel himself, but Warner Bros. felt he had departed from the source material too much and had another writer adapt it. Director Michael Crichton Films a Favorite Novelist, by Michael Owen. New York Times (1923-Current file)   28 January 1979: D17.  "I dont think they   gave it a chance," said Crichton later. An author of pleasurable fear: Michael Crichton takes fiction where you wouldnt want to go
Gorner, Peter. Chicago Tribune (1963-Current file)   24 June 1987: D1. 

=== Directors cut ===
The directors cut omits the scene in which the doctors explain the cause of Harrys seizures. Hodges did not like the scene, believing it spoon-fed the audience. Warner Brothers felt that the story needed it, and that the audience would not understand or like Harry Benson without the scene. The directors cut was shown at the 2003 Edinburgh Film Festival and at the ICA cinema in London in 2008. It was also screened in 2011 in Newcastle-Upon-Tyne at the Mike Hodges retrospective to celebrate the 40th anniversary of Get Carter.   

== Release ==
 Warner Archive series. Viewers were offered a choice between on-demand streaming or a DVD-R created on a per-sale basis. Regarding the quality, Warners website states it was "manufactured from the best-quality video master currently available and has not been remastered or restored specifically for this DVD and On Demand release." 

Terrence Malick, the director of Badlands (film)|Badlands, wrote to Hodges expressing how much he loved watching The Terminal Man, saying "Your images make me understand what an image is."   

==References==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 