Meendum Kokila
 
{{Infobox film
| name           = Meendum Kokila
| image          = Meendum Kokila.jpg
| image_size     =
| caption        =
| director       = G. N. Rangarajan
| producer       = T. R. Srinivasan
| writer         = Ananthu
| screenplay     = Ananthu
| story          = Hassan Brothers
| narrator       =
| starring       =  
| music          = Ilaiyaraaja
| cinematography = N. K. Viswanathan
| editing        = K. R. Ramalingam
| studio         = Charuchitra Films
| distributor    = Anadha
| released       = 14 January 1981
| runtime        =
| country        = India Tamil
| budget         =
| gross          = 
| preceded_by    =
| followed_by    =
| website        =
}}
 Tamil film Deepa in lead roles. The story is about a married lawyers infatuation towards an actress and the trouble he faces with his wife. Through the film Sridevi established herself as one of the top actresses and fetched her a Filmfare Award. The film was initially launched with J. Mahendran as the director and Rekha playing a supporting role. However, both pulled out of the project for unknown reasons following which they were replaced by Rangarajan and Deepa respectively. The soundtrack and background score were composed by Ilaiyaraaja. The film was box-office success.

==Plot==
Subramaniam, a lawyer is married to Kokila and has a daughter. Things go well until he meets Kamini, a movie star in a party. He gets attracted towards Kamini and becomes ready to sacrifice his own family for her. Kokilas efforts in bringing back her husband forms the rest of the story.

==Cast==
* Kamal Hassan as Subramaniam
* Sridevi as Kokila
* Deepa as Kamini Anju
* Thengai Srinivasan
* Omakuchi Narasimhan

==Production==
The film was originally directed by J. Mahendran with Kamal Haasan, Sridevi and Rekha—in her Tamil cinema debut—playing prominent roles.     Rekha was the original choice for actress role, and scenes featuring her were shot till 3000 feet.  Initially Mahendran opted out of the film after a song sequence was shot. Later as the film progressed Rekha pulled out citing no reason.  It was Kamal Haasan who requested G. Rangarajan to take over the film as director.   Rekha was replaced by Deepa. 

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chinna Chiru Vayathil || K. J. Yesudas, S. P. Sailaja || Kannadasan || 04:32
|- Panju Arunachalam || 03:55
|-
| 3 || Ponnana Meni || K. J. Yesudas, S. Janaki || 04:30
|-
| 4 || Radha Radha Nee || S. P. Balasubrahmanyam, S. Janaki || Kannadasan || 04:27
|}

==Reception and accolades== Filmfare Award in the Best Actress category;  the first of her four Filmfare Awards.  The film established Sridevi as one of the top actress in South India. 

==References==
 

==External links==
* 
*  

 
 
 
 