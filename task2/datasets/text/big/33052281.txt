The Face at the Window (1919 film)
 
{{Infobox film
| name           = The Face at the Window
| image          = 
| image_size     =
| caption        =  Charles Villiers
| producer       = David B. OConnor
| writer         = Gertrude Lockwood play by F. Brooke Warren
| narrator       =
| starring       = David B. OConnor
| music          =
| cinematography = Lacey Percival
| editing        = 
| studio = D.B. OConnor Feature Films
| distributor    = 
| released       = 8 November 1919
| runtime        = 5 reels 
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1897 play. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 92. 

==Plot==
In Paris, a thief and murderer known as Le Loup (actually Lucio Delgrade) hides his identity behind a mask and howls before he kills his victims. He has killed 36 people in all. He kills a caretaker while rifling a safe. Then he stabs a banker, M. de Brison, whose daughter Marie has spurned his advances. Detective Paul Gouffet investigates but Le Loup kills him. However the detective is revived from the dead through a device invented from a mad doctor and his hand writes the name of Le Loups real identity. The police go after him and Le Loup is shot while trying to escape. 

==Cast==
*D.B. OConnor as Lucio Delgrade
*Agnes Dobson as Marie de Brison
*Claude Turton as Paul Gouffet
*Gerald Harcourt as Lucien Cortier
*Collet Dobson as M. de Brison
*Charles Villiers as Barbelon
*Percy Walshe as Dr Le Blanc
*Lulu Vincent as Mother Pinau
*Syd Everett as Barlet
*Millie Carlton as maid
*Charles Beetham as Prefect of Police
*D.L. Dalziel was Detective Drummond
*Gilbert Emery as caretaker of bank

==Production==
The movie was one of several based on a popular stage play. It was shot in the Rushcutters Bay study in March and April 1918. Censors requested the deletion of a scene where a policemen is stabbed by Le Loup. 

It was the film debut of popular stage actor Agnes Dobson.  She later reprised the role on stage. 

==Reception==
The film was a popular success and was widely seen 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 
 


 