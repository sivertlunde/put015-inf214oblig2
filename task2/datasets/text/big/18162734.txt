The Culpepper Cattle Co.
 
{{Infobox film
| name           = The Culpepper Cattle Co.
| image          = Culpepper-cattle-co.jpg
| caption        = Promotional poster for The Culpepper Cattle Co.
| director       = Dick Richards
| producer       = Paul Helmick Jerry Bruckheimer (associate producer)
| writer         = Dick Richards
| starring       = Gary Grimes Billy Green Bush Tom Scott
| cinematography = Lawrence Edward Williams Ralph Woolsey
| editing        = John Burnett
| distributor    = 20th Century Fox
| released       =  
| runtime        = 92 min.
| country        = United States
| language       = English
| budget         = $1 million 
| gross          = $1.25 million (US/ Canada) 
}}

The Culpepper Cattle Co.  or Dust, Sweat and Gunpowder (Australian title) is a 1972 Revisionist Western film produced by Twentieth Century Fox. It was directed by Dick Richards and starred Billy Green Bush as Frank Culpepper and Gary Grimes as Ben Mockridge. This was the first credited film for Jerry Bruckheimer, for which he received an associate producer credit.  Its tagline is "How many men do you have to kill before you become the great American cowboy?" and also "The boy from Summer of 42 becomes a man on the cattle drive of 1866", which references a similar coming of age film starting Gary Grimes. The film is typical of the "hyper-realism" of many early 1970s revisionist westerns. It is particularly noted for its grainy photography and use of sepia toning in some scenes. 

==Synopsis==

Ben Mockridge (Gary Grimes) is a young man proud of his $4 handgun and enamored of “cowboyin.” He asks Frank Culpepper (Billy Green Bush) if he can join his cattle drive to Fort Lewis, Colorado. Culpepper (a reformed gunslinger) reluctantly agrees and sends Ben to the cook (Raymond Guth) to be his “little Mary.” 

Ben quickly discovers that the adults have little interest in young’ns, and no interest in “showing him the ropes”. Culpepper nevertheless assigns Ben tasks the greenhorn handles poorly — or simply fails at — repeatedly causing serious trouble.

After rustlers stampede the herd, Culpepper tracks them to a box canyon. When the rustlers’ leader (Royal Dano) demands 50 cents a head for having rounded up and taken care of the cattle, Culpepper will have none of it. He and his hands kill the rustlers, not hesitating to gun down disarmed men, or repeatedly shoot anyone still moving. They lose four of their own in the fight.
 Geoffrey Lewis), he and three of his buddies agree to join the drive. When they cross the trappers’ path, there’s no parlaying — they immediately kill the trappers and take their possessions.

When Ben stands night watch, hes unprepared for a one-eyed man (Gregory Sierra) trying to steal the horses. Instead of immediately shooting him, Ben lets the man distract him with his talk, and is overcome by another thief. Culpepper is outraged at Bens stupidity.

The horse theft “tears it,” and Culpepper decides to toss Ben on a stage, regardless of where its headed. When Culpepper & Co. enter a town where they hope to buy horses and send off the greenhorn, they stop at a saloon, where Ben recognizes one of the patrons as the one-eyed horse thief. Another shootout ensues, with Ben "redeeming" himself by killing the bartender as the latter reaches for his shotgun. As before, Culpepper’s adversaries wind up dead, an unlikely survivor directing Culpepper to the horses.

When Ben handles Caldwell’s gun without his permission, the touchy Caldwell goes into a snit, and knocks Ben to the ground. When one of the hands calls Caldwell an SOB for striking Ben, Caldwell demands a gun fight to reclaim his “honor". The hand decides it isn’t worth the trouble and leaves the drive. “You cost me a good man, boy.”

When they reach an area with grass and water, Culpepper leaves the cattle to graze, looking for the landowner to pay him. The owner, Thornton Pierce (John McLiam), tells Culpepper he should have asked first, and demands $200 as down-payment simply for having trespassed. This time, Culpepper & Co. are outgunned, and forced to surrender their sidearms, which they view as a symbolic castration.
 Anthony James), who invites them to stay and water their cattle. He says God has led his party here, and they intend to settle.

Not surprisingly, Pierce and his thugs show up, claiming “this land is mine”, and gives everyone — Green and his people included — an hour to get off. Green is convinced Culpepper was sent by God to help. Culpepper responds that Green need only leave to be safe, which is what he intends to do, as it’s less than two weeks to Fort Lewis, and selling his cattle is all he cares about.

Ben decides to stay, feeling he can help in some unspecified manner. As Culpepper & Co. ride off, their consciences (and lust for revenge) get the better of them, and they return to defend Green & party from Pierce. In the ensuing shootout, everyone in the Culpepper and Pierce parties — except Ben — is killed.

Revealing his hypocrisy and ingratitude, Green tells Ben that they aren’t going to stay after all, as the ground has been stained with blood. "God never intended us to stay — he was only testing us." An angry Ben forces them to bury the bodies, then discards his gun and rides off to parts unknown.

==Cast==

* Gary Grimes as Ben Mockridge
* Billy "Green" Bush as Frank Culpepper
* Luke Askew as Luke
* Bo Hopkins as Dixie Brick Geoffrey Lewis as Russ
* Raymond Guth as Cook
* Wayne Sutherlin as Missoula Matt Clark as Pete Anthony James as Nathaniel

==Historic accuracy==
 
The Culpepper Cattle Co. has been praised for its attention to detail and period atmosphere. A subtle example is seen when Frank Culpepper leans against a water barrel and his arm above the wrist is exposed — it’s white, untanned. People rarely took off any clothing in public (there’s a comic moment when the cook is embarrassed to be seen with his shirt off), and the idea of an “all-over” tan would have been absurd, if not incomprehensible. (Only working folk had a tan.) Cowboys were “fish-belly white” over most of their bodies.

The opening title sequence mixes genuine period photographs with sepia-tinted posed images of the cast members.

The story is almost casually violent, but this has to be seen in the context of the offenses against Frank Culpepper and his party. These included horse and cattle theft, which were usually punishable by hanging. As there was no practical way to haul the thieves into court, Culpepper was justified in dispensing immediate “justice”, however brutal. Culpepper’s final act of justice is to wipe out the evil agro-Capitalist (the villain in scores of Westerns) and his horde.
 Goodnight and Loving made their first long cattle drive.

The film’s principal anachronism is showing most of the cowhands with beards. Contemporary photos indicate that, while cowboys often had moustaches (sometimes quite fancy), beards were not common — one out of twenty cowboys, perhaps. This was unusual in an era (extending to the end of the 19th century) where a high percentage of men took pride in having full beards.  

The firearms used are almost all anachronisms in an 1866 setting. With the exception of Bed Mockridges 1850s-era percussion-cap pistol, the other pistols and rifles date from the early 1870s to the mid-1890s. 

Another anachronism is a pressure lantern (invented in the late 1890s) visible in one bar scene.

==References==
 

==External links==
* 
* 

 
 

 

 
 
 
 
 
 
 
 
 
 