Professor Layton and the Eternal Diva
{{Infobox Film
| name = Professor Layton and the Eternal Diva
| image = Professor Layton and the Eternal Diva Poster.jpg 
| caption = Japanese release poster
| director = Masakazu Hashimoto
| producer = Kenji Horikawa Arimasa Okada Toshiaki Okuno Shin Omura Ichiro Takase
| story = Akihiro Hino
| screenplay = Aya Matsui
| starring = Yo Oizumi Maki Horikita Nana Mizuki Atsurô Watabe Saki Aibu
| music = Tomohito Nishiura Tsuneyoshi Saito
| cinematography =
| editing = 
| studio = P.A.Works OLM, Inc. Robot Communications
| distributor = Toho Manga UK
| released =   
| runtime = 99 minutes
| country = Japan
| language = Japanese
}}
 mystery comedy-drama  film directed by Masakazu Hashimoto and produced by P.A.Works, OLM, Inc., and Robot Communications. The film is based on the Professor Layton video game series by Level-5 (video game company)|Level-5, taking place between the events of the video games Professor Layton and the Last Specter and Professor Layton and the Miracle Mask.

According to Level-5, the film stays true to the games, with music, puzzles and characters. An English-language version was released by Manga Entertainment in the United Kingdom on October 18, 2010 at the same time Professor Layton and the Unwound Future was. The film was released in the United States on November 8, 2011 by Viz Media.

==Plot==
 

In the present day, archaeologist and puzzle master Professor Layton and his young apprentice Luke Triton solve the mystery of the theft of the sound of Big Ben, deducing the culprit to be Laytons perennial adversary, Don Paolo. Following this resolution, the pair return to Laytons office, where they listen to "The Eternal Diva", a record by an old student of the professors named Janice Quatlane, prompting them to recall one of their earliest adventures together from three years ago...

Layton is invited by Janice to attend an opera in which she is performing, centered around the legendary lost land of Ambrosia and the secret of eternal life it claims to hold, which it is claimed will be rediscovered when its queen returns. Janice believes recent strange occurrences are somehow connected to the opera: in addition to girls disappearing from London, the operas composer Oswald Whistler has recently adopted a young girl who has claimed to be Whistlers deceased daughter, and Janices friend, Melina. Layton and Luke attend the opera at the Crown Petone opera house, built on the White Cliffs of Dover, and witness Whistler play the entire piece on an elaborate one-man orchestra machine known as the Detragon. Once the opera is complete, however, a mystery man (speaking through a marionette), informs the audience that they are to play a game, the winner of which will receive the secret of eternal life.

The Crown Petone is revealed to be a ship, which breaks off from the cliffs and sets sail to isolate the "game", after which the mystery man unveils a series of timed puzzles intended to progressively eliminate the players until one winner remains. The first two puzzles challenge the players to find "the oldest thing they can see", then go where they can "see the largest crown", but Laytons prolific puzzle-solving prowess allows him and a group of eleven others to quickly deduce the answers (the sky and the Crown Petone itself, respectively). To "see the largest crown", the group leaves the ship in two lifeboats, rowing out to a distance from which they can see the whole ship, at which point the lifeboats start moving on their own, taking the successful players to their next destination.

The next morning, the group finds that the boats have brought them to a mysterious island. Discovering a sculpted stone seal, party member and amateur historian Marco Brock realizes that they have discovered Ambrosia. Evading mentally-controlled wolves as they make their way towards the bizarre castle at the centre of the island, Layton, Luke and Janice become separated from the rest of the group and cobble together a makeshift helicopter that allows them to fly to the castle in short order. There, they solve the fourth and final puzzle that directs them to the final room of the contest, but Layton leaves Luke, Janice and two other contestants (Brock and child prodigy Amelia Ruth) to enter it while he explores the rest of the castle.  Hes followed by two others, who are quickly claimed by a trap.

Finding Melina in the castle and witnessing her apparently talking to herself, Layton realizes the truth of what is going on, and races to rejoin the others: Oswald Whistler is behind everything, having conspired with Laytons old foe, scientist Jean Descole, to abduct girls from London and use the Detragon to "program" them with his dead daughters memories in order to keep her alive. This is the fate that befell "Melina" — actually named Nina — and it is the fate that Whistler has planned for another contestant called Amelia... until Layton explains that Janice was also a victim to Whistlers experiments, and that unbeknownst to Whistler, he had succeeded. Janice was given Melinas memories, and had invited Layton to stop her father from hurting any more girls, at which point Descole reveals himself to be the true villain of the piece, having always intended to use the Detragon in concert with Melinas voice to combine the melodies hidden in the seal of Ambrosia to raise the island itself. Melina complies, but when Ambrosia does not rise after two attempts (Both involving Descole playing the melody of the stars and Melina singing that of the sea), Descole loses patience and thinking music is not enough, resorts to drastic measures. The Detragon destroys the castle, unveiling and becoming the controls of a gigantic excavation robot called the Detragiganto, which Descole commandeers and begins rampaging across the island in a desperate attempt to uncover Ambrosia by force.

During the fracas, Melina tries to stop Descole, who (believing he has no further use for her) knocks her away, causing her to fall over the side of the robot, leaving her holding on for dear life. Upon seeing this, Luke pleads to save her from falling. While Luke rescues Melina, Layton duels with Descole on top of the Detragiganto, and reveals that Descole had overlooked a third melody hidden in the seal (that of the sun, revealed by turning the seal upside-down). Again, Melina sings as Layton takes the Detragons controls, and this time, the ruins of Ambrosia do indeed rise, infuriating Descole even further. He lunges at Layton, believing that the ruins only belong to him, but merely damages the control panel instead, throwing the Detragiganto out of control and causing it to heavily damage itself and him to fall off it and disappear. In the aftermath of the islands rise, Melinas thoughts and memories leave Janices body (as Melina decided she could not take over Janices life).

In a post-credits scene, back in the present day, Layton and Luke finish listening to the record, just as Janice arrives at Laytons office.

==Characters==
 

*Professor Hershel Layton  : A tea-loving professor of archaeology from Londons Gressenheller University, and an avid puzzle solver. He is no detective, but there are no mysteries he cannot solve. His adventures around the world solving cases brought him much fame, and a very good reputation.
*Luke Triton  : The self-proclaimed "apprentice number one" of Professor Layton. Luke follows his teacher everywhere, and is always trying to learn from him. Luke has a special talent that allows him to speak to animals.
*Emmy Altava  : The appointed assistant of Professor Layton. She is quite knowledgeable, and is described as a heroine and jokingly refers Luke as Laytons "second assistant". She leaves Layton and Luke to investigate about kidnapped children from London with Dr. Schrader.
*Janice Quatlane  : A famed opera singer who requests Professor Laytons aid. She is known to have a beautiful voice. It is interesting to note that her Japanese voice actress also serves as her singing voice, regardless of language
*Jean Descole  : The true villain of the film. He is an intelligent scientist, who draws people to his side with his empty promises.
*Inspector Clamp Grosky  : Head of Scotland Yard, he investigates the Crown Petone to find out about the abductions.
*Oswald Whistler  : The 54-year-old pianist who runs the music in the opera house. He built the Crown Petone opera house with the help of Dr. Andrew Schrader, but built the Detragon with Descoles help.
*Melina Whistler  : Oswalds daughter and Janice best friend who died from an illness one year prior to the start of the film at the early age of 22.
*Nina  : Seven-year-old girl who claims to have been granted the gift of Eternal Life. She also claims to be Janice friend, Melina brought back to life.
*Amelia Ruth  : The 16-year-old, talented, and beautiful young lady. She is a champion chess player, known throughout all Britain. She is more determined than the other contestants to gain Eternal Life, not for herself, but rather wants to give it to her sick grandfather (who has a month to live) to keep him alive.
*Curtis ODonnel  : 65-year-old adventurous sea-captain. He wants to win Eternal Life in order to navigate the seas forever.
*Marco Brock  : 45-year-old amateur historian who has studied the legend of Ambrosia extensively. He wants to win Eternal Life in order to solve all the mysteries of the ancient world.
*Frederick Bargland  : Wealthy CEO of the World Fleet Corporation, who lives in London. His doctor gave him six months to live, and he wants to win Eternal Life so his companies will remain successful.
*Annie Dretche  : A famous British author, who wrote famous mystery novel, Murder on the Thames. She wants Eternal Life in order to write novels forever.
*Celia Raidley  : The beautiful 32-year-old widow of a former millionaire, who often appears in gossip magazines. She wants Eternal Life so she will retain her beauty forever.
*Pierre Starbuck  : The young 25-year-old talented football player nicknamed "The Man with the Golden Left Leg", who had to give up playing due to a leg injury. He wants Eternal Life in order to "get his strength back".
 Professor Layton series including Flora Reinhold, Don Paolo, Inspector Chelme, Constable Barton, Spring and Cogg, Stachenscarfen, Sammy Thunder, Mr. Beluga, Granny Riddleton, Babette, Claudia the Cat, Dorothea and Dr. Andrew Schrader. The Elysian Box (Pandoras Box) also makes an appearance on Dr. Schraders office.

==Home media==
The film was released in Singapore on March 18, 2010, showing in Japanese with English and Chinese subtitles. Manga Entertainment UK has licensed the film for DVD and Blu-ray Disc release in the United Kingdom in October 2010.  The Manga Entertainment release is dubbed by the voice actors used in the UK releases of the game, including several new voice actors, like Sarah Hadland and Wayne Forester.  Several versions of the film have been released: a standard DVD release, a standard Blu-ray release, a three-disc DVD and Blu-ray combo pack, and a three-disc collectors edition that includes a 630-page book containing the complete storyboard.

When asked about a North American release, director Akihiro Hino said, "We dont have any plans to release the movies in America currently, but well make sure to let you know if that changes."  Viz Media announced they had licensed the movie at the 2011 Anime Expo and released it on DVD in North America on November 8, 2011.  The DVD was a direct port of the UK version and was not redubbed with the North American voice actors. The film was also released in German as Professor Layton und die ewige Diva and in Holland as Professor Layton en de Eeuwige Diva.

==Soundtracks==
 
Two albums were released in Japan containing the music of the film. One titled The Eternal Diva: Janice Quatlane, containing all the vocal songs, and the other titled Layton Kyouju To Eien no Utahime Original Soundtrack, containing the main music from the film (most of which is reorchestrated versions of music from the first four games). Unlike the games, an actual orchestra was used for most of the music. In addition, the films ending theme, The Eternal Diva, is included with its lyrics on both albums.
 

{{Track listing
| collapsed = yes
| headline = The Eternal Diva: Janice Quatlane
| total_length = 19:13
| title1 = Record of Memories
| length1 = 1:19
| title2 = Let this Happiness be Eternal
| length2 = 1:46
| title3 = A Transient Lifes Departure
| length3 = 2:06
| title4 = Janices Tears
| length4 = 1:38
| title5 = The Eternal Diva
| length5 = 7:03
| title6 = A Song of the Stars
| length6 = 0:37
| title7 = A Song of the Sea
| length7 = 0:49
| title8 = A Song of the Sun
| length8 = 2:16
| title9 = Indigo Memories
| length9 = 1:39
}}
{{Track listing
| collapsed = yes
| headline = Professor Layton and the Eternal Diva Original Soundtrack
| total_length = 62:36
| title1 = Cold Open ~Professor Laytons Theme
| length1 = 2:38
| title2 = Prologue to the Adventure ~Puzzles
| length2 = 1:27
| title3 = Travel Guide ~Descoles Theme (unused)
| length3 = 1:40
| title4 = Compensation 1 ~Detragigantos Theme
| length4 = 0:33
| title5 = Departure to the Voyage ~Descoles Theme
| length5 = 2:05
| title6 = Detragans Echoes ~Whistlers Theme
| length6 = 1:06
| title7 = Rules for the Survivors ~An Uneasy Atmosphere
| length7 = 0:46
| title8 = Puzzle Number 001 ~Puzzles Reinvented (from Unwound Future)
| length8 = 1:56
| title9 = Compensation 2 ~Detragigantos Theme
| length9 = 0:21
| title10 = Puzzle Number 002 ~Puzzles 5 (from Last Specter)
| length10 = 4:11
| title11 = Melinas Tenacity ~An Uneasy Atmosphere (from Diabolical Box)
| length11 = 0:27
| title12 = People of the Past ~The Looming Tower (from Curious Village)
| length12 = 2:13
| title13 = The True Crown ~Descoles Theme
| length13 = 1:54
| title14 = About London ~About Town (from Curious Village)
| length14 = 0:43
| title15 = The Passionate Whistler ~Whistlers Theme
| length15 = 1:26
| title16 = The Legendary Kingdom ~Theme of Ambrosia
| length16 = 0:56
| title17 = Rest ~Time for a Break
| length17 = 0:53
| title18 = Approaching Pursuer 1 ~Approaching Pursuer
| length18 = 1:18
| title19 = Puzzle Number 003 ~Revolutionary Idea
| length19 = 0:54
| title20 = Adjusting the Pace ~Pursuit in the Night (from Curious Village)
| length20 = 1:11
| title21 = Compensation 3 ~Detragigantos Theme
| length21 = 0:12
| title22 = Escape! ~Professor Laytons Theme
| length22 = 2:24
| title23 = Puzzle Number 004 ~The Plot Thickens (from Curious Village)
| length23 = 2:07
| title24 = Descole Appears ~Descoles Theme
| length24 = 1:01
| title25 = Professor Laytons Piano ~A Song of the Sea
| length25 = 0:32
| title26 = Approaching Pursuer 2 ~Approaching Pursuer
| length26 = 1:04
| title27 = Emmys Efforts ~Emmys Theme
| length27 = 0:31
| title28 = Whistlers Experiment ~Dangerous Experiment
| length28 = 2:10
| title29 = The Mystery Explained! ~Professor Laytons Theme
| length29 = 2:34
| title30 = Great Conspiracy ~Descole, Ambrosias Theme
| length30 = 2:36
| title31 = Prelude to Destruction ~Descoles Theme
| length31 = 1:07
| title32 = Detragiganto Appears ~Detragigantos Theme
| length32 = 1:43
| title33 = Janices Crisis ~Tense Decision
| length33 = 0:21
| title34 = Future British Gentleman ~Lukes Theme
| length34 = 1:53
| title35 = The Final Battle ~Time of Conclusion
| length35 = 1:49
| title36 = The Dream Collapses ~Theme of Ambrosia
| length36 = 1:38
| title37 = Fathers Memories ~Whistlers Theme
| length37 = 0:35
| title38 = The Feelings Will Always Be Close ~Whistlers Theme
| length38 = 2:46
| title39 = The Eternal Diva / Janice Quatlane (CV Nana Mizuki)
| length39 = 6:55
}}

==Reception== USD during its opening weekend. By the weekend of March 24 to the 26th, the film had grossed $6,140,049 in both Japanese and overseas (Taiwan and Singapore) totals.    It was the ninth most watched anime film of the first of half of 2010 in Japan,  and the 14th of the entire year. 

The film has been given generally good reviews. GameSync.net gave it a positive review, calling it "wholeheartedly entertaining and heartwarming, with a dash of British wit and eccentricity."  IGN UK gave the film 3.5 out of 5 stars, praising its plot, animation, and how the puzzles were incorporated into the film, but criticized its "overreliance of CG." It also regarded the film one of the first "good" video game films. 

==Possible sequel==

The game series developer and publisher Level-5 (video game company)|Level-5 has stated that they wished to release a Professor Layton film every winter, and that they are already producing the next film. Besides the animated films, a British/Japanese live-action film was in the works as well.  While not committing to a second film, director Akihiro Hino said that the "second season" of the Layton series (which begins with Professor Layton and the Last Specter) was originally imagined with another film. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 