Time Teens

{{Infobox film
| name               = Time Teens: The Beginning
| image              = File:Time Teens Cinema Poster.png
| caption            = Theatrical release poster
| director           = Ryan Alexander Dewar
| writer             = Ian Grieve
| screenplay    =
| starring           = {{plainlist|
* Andy Gray
* Ian Grieve
* Emily Winter
* Ralph Riach}}
| released           = 
| runtime            = 114 minutes
| country            = Scotland (UK)
| language           = English
| budget         =
| gross          = 
}}

Time Teens: The Beginning is a 2015 Scottish feature film directed by Ryan Alexander Dewar. It was originally written as a TV Series by writer/actor Ian Grieve around ten years earlier. When Ryan Alexander Dewar and Ian met in Perth Theatre where Ian was resident director, they pursued his TV scripts as a short film.   

==Cast==
Appearing in the film are:
* Andy Gray  as Black Ruthven
* Ralph Riach  as The Meridian
* Ian Grieve
* Annie Louise Ross  as The Victorian Woman
* Anne Kidd  as Ghost Madelaine Lockhart
* Liam Brennan  as Edwin
* Tom McGovern  as The harlequin
* Helen Mackay 
* Irene MacDougall  as Andrea Novotny
* Gareth Morrison 
* Amanda Beveridge  as Miranda
* Emily Winter  as Emma
* Richard Addison  as Lawrence Toureq
* Michele Gallagher  as Jenny Buchan
* Sharon Young  as Buccaneer
* Jo Freer  as Buccaneer

==Plot==
Time travel Exists. William is a Tourasaiche, Pilgrim Time traveller, policing time crime. When he receives a letter from the future, he has to decide if his future will happen how it is written, or if things can be changed. 

==Production==
After filming for four days in 2013, the film was too long to be a short film and too short to be a feature. The short film soon turned into a full feature film with a plethora of Scottish cast coming on board and waived their fee on the production. Production took place in under three weeks over 2013 and 2014.    The release date was put back as Dewar and Grieve appealed for assistance to get the project finished. 

==Release==
The film premiered in February 2015 at the Perth Playhouse Cinema on an IMAX screen and was then shown there for the following 3 weeks. 

It is scheduled for a May 2015 screening at the North By Midwest Micro-budget film festival in Michigan and the Fife Film Expo in Scotland  . 

==Reception==
Some complained that the film did not include enough teenagers and was perhaps difficult in its concepts for younger audiences. Suggesting that the name could have been changed, though understood it was a pilot for a longer series.

===Awards and nominations===
Time Teens picked up an award at the monthly San Francisco Film awards  and an award from the Accolade Film Award Competition.

The film opened with generally positive reviews with average weighted ratings between 6.6 and 8 out of 10.

==TV script==
Alongside the film, a TV script was also written.  The end of the film is The Beginning of the series. 

==References==
 

==External links==
*   Cinema release trailer on YouTube
*  
*  
*   The List
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 