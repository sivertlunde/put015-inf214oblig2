Miss Jerry
{{Infobox film
| name           = Miss Jerry
| image          = 
| caption        = 
| director       = 
| producer       = Alexander Black
| writer         = Alexander Black William Courtenay Chauncey Depew
| music          = 
| cinematography = 
| editing        = 
| distributor    = Alexander Black Photoplays
| released       =  
| runtime        = 
| country        = United States Silent
| budget         = 
| gross          =
}}
 silent pre-film "Picture Play" written and produced by Alexander Black and starring Blanche Bayliss. Miss Jerry was not a film, but a series of posed magic lantern slides projected onto a screen with a dissolving stereopticon, accompanied by narration and music, making it the first example of a feature-length dramatic fiction on screen.

Miss Jerry debuted on October 9, 1894 at the Carbon Studio in New York City. It has been described as "the first picture play"   and while other early film and peep-show animations produced at this time were short documentaries, Miss Jerry sought to develop what is arguably the first feature of moving pictures. This   attempts to create an impression of movement with the slides changing once every 15 seconds.

==The Picture Play==
"In Miss Jerry my purpose has been to test experimentally, in a quiet story, certain possibilities of illusion, with this aim always before me, that the illusion should not, because it need not and could not safely, be that of photographs from an acted play, nor of artistic illustration, but the illusion of reality."
 {{cite journal
 | author = Alexander Black
 |date=00 Volume 189618 Issue 3, September
 | title = Photography in Fiction
 | journal = Scribners
 | url = http://digital.library.cornell.edu/cgi/t/text/pageviewer-idx?c=scri;cc=scri;rgn=full%20text;idno=scri0018-3;didno=scri0018-3;view=image;seq=0356;node=scri0018-3%3A9
 | accessdate = 2010-02-25
 }}  

Aware of the progress made by Eadweard Muybridge and other photographers towards the illusion of motion, Black instead set out to present a convincing narrative story in front of an audience, using photography to present fiction in a convincing way, rather than a perfect illusion of motion. 

In his 1926 history of the movies, A Million and One Nights the author Terry Ramsaye says, "While the motion picture was progressing with mincing steps in the peep show Edison Kinetoscope the sheer force of the evolution of expression presented the world with an interesting paradox – the birth of the photoplay upon the screen. . . Black arrived at a rate of four slides a minute for his presentation. The plan was to make the pictures successively blend into one another in the dissolving stereopticon, avoiding an optical ‘jar’ as much as possible. Each picture represented a step forward in the action. The pictures were carefully registered always to present every still object in the view in precisely the same position, while only the moving actors were shown in altered attitudes. There could, of course, be no hope of depicting rapid motion. Black chose, in such instances, to picture the moments before and after the swift action involved. For example, the villain might stand menacing the hero with an upraised dagger, while the next slide would show the victim of the stab in a heap on the floor. The spoken obligation from the stage had to carry across the imagined stroke of the stabbing."

Unfortunately, no intact set of slides for Miss Jerry is known to exist. 

==Plot==
After finding out that her father is suffering financial problems, Jerry Holbrook decides to start a career in journalism in the heart of New York. While working she falls in love with the editor of her paper, Mr. Hamilton. After being offered a job in London the couple initially have problems but Jerry accepts a proposal of marriage and they leave for London together.

==Cast==
* Blanche Bayliss (under the name "Constance Arthur") as Miss Geraldine Holbrook (Miss Jerry) William Courtenay as Walter Hamilton
* Chauncey Depew as Himself (Director of the New York Central Railroad)

==References==
 

==External links==
*  
*  
*   with some of the stills at Google Books
*  

 
 
 
 
 