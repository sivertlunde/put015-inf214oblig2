The God Within
 
{{Infobox film
| name           = The God Within
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = F. P. Bayer
| starring       = Blanche Sweet Henry B. Walthall
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = Biograph Company
| released       =  
| runtime        = 17 minutes (16 Frame rate|frame/s)
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short drama film directed by  D. W. Griffith and starring Blanche Sweet. A print of the film survives.   

==Cast==
* Henry B. Walthall - The Woodsman
* Claire McDowell - The Woodsmans Wife
* Blanche Sweet - The Woman of the Camp
* Lionel Barrymore - The Woman of the Camps Lover
* Charles Hill Mailes - The Doctor
* Gertrude Bambrick - In Bar
* Clara T. Bracy - The Madam
* William J. Butler - In Other Town
* Christy Cabanne - On Street (as W. Christy Cabanne) Harry Carey John T. Dillon - On Street
* Frank Evans - In Bar Charles Gorman - In Bar/In Other Town
* Joseph Graybill - In Bar
* J. Jiquel Lanoe - In Other Town
* Adolph Lestina - In Other Town
* W. C. Robinson - In Bar Charles West - In Other Town (as Charles H. West)

==See also==
* Harry Carey filmography
* D. W. Griffith filmography
* Blanche Sweet filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 