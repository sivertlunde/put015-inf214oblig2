Memories of Murder (1990 film)
 
{{Infobox television film
| name         = Memories of Murder
| image        = Memories of a Murder.jpg
| caption      = VHS cover
| runtime      = 104 minutes
| genre        = Action Crime Thriller John Harrison Nevin Schreiner
| director     = Robert Lewis
| producer     = Robert Michael Lewis Nancy Allen Vanity Robin Thomas
| distributor  = Viacom Productions
| editing      = Brian Q. Kelley
| music        = Joseph Conlan
| country      = United States
| language     = English Lifetime Television Network
| released     = July 31, 1990
}}
 Nancy Allen, John Harrison.

Memories of Murder is the first of many original films that would be produced for the  .

==Plot== Nancy Allen) is a woman who is suffering from amnesia to the extent that she does not even recognize her husband and daughter. Extremely confused and tormented, she desperately seeks to piece together her life and, in doing so, stumbles upon some startling secrets from her shadowy past. She discovers that a female killer connected to her from her earlier life is intent on stalking her and seeking revenge by killing her and her family.

==Cast== Nancy Allen as Jennifer Gordon/Corey
*Nick Benedict as Mr. Bates
*Olivia Brown as Brenda
*Linda Darlow
*Don S. Davis
*Jerome Eden as Store Manager
*Robyn Simons as Amy
*Robin Thomas as Michael Vanity as Carmen

==Critical reception==
Chris Willman of the Los Angeles Times called the film "amazingly pedestrian" in terms of its mystery.  Daniel Ruth of the Chicago Sun-Times wrote that he "couldnt keep track" of what was happening in the plot.  Ken Tucker of the Entertainment Weekly gave it a "D" grade, saying, "Memories of Murder is full of romance-novel dialogue". 

==References==
 

==External links==
* 

 
 
 
 
 
 
 