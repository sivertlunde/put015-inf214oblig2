Black Hand (1950 film)
{{Infobox film
| name           = Black Hand
| image          = Black Hand (1950 film).jpg
| alt            =
| caption        = Theatrical release poster
| director       = Richard Thorpe
| producer       = William H. Wright
| screenplay     = Luther Davis 
| story          = Leo Townsend
| starring       = Gene Kelly J. Carrol Naish Teresa Celli
| music          = Alberto Colombo
| cinematography = Paul Vogel
| editing        = Cotton Warburton
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =   
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $774,000  . 
| gross          = $1,210,000 
}} Black Hand extortion racket in New York City in the first decade of the 20th century. 

==Plot==
The film opens in 1900 as Roberto Colombo, an Italian-American attorney living in the Little Italy section of New York City, is killed by gangsters as he meets with a police officer to give information about an attempt to extort money from him. His widow and son return to Italy where his widow dies. In 1908, his son Giovanni Colombo (Gene Kelly) returns to New York City, determined to conduct a vendetta against the men who killed his father. He meets up with childhood friend Isabella Gomboli (Teresa Celli) and police detective Louis Lorelli (J. Carrol Naish), both of whom try to dissuade him.

After the man that Giovanni Colombo had hoped would tell him about his fathers killers is murdered, Giovanni and Isabella work to rally people in Little Italy against the Black Hand racket, but that movement is dealt a set-back when Giovanni is attacked and his leg is broken.  Recovering from his injury, Giovanni decides to study law as his father had, with the help of Isabella, with whom he has fallen in love. But when Lorelli shows them evidence uncovered after a bombing of a local store, Giovanni puts his studies on hold to help track down the perpetrators. The trial that results comes to nothing when a key witness is intimidated and refuses to testify, but the defendant is ultimately deported when police in Naples identify him as a fugitive from justice there.

Following the deportation, Lorelli travels to Italy to examine photographs of Italian criminals at large, in an attempt to identify other New York City gangsters that could be deported to Italy. He is attacked and killed in Italy, but only after he has mailed a list with the results of his research back to Giovanni in New York City. In an attempt to prevent this list from getting to authorities, gangsters kidnap Isabellas young brother. Giovanni is captured trying to save the boy, and reveals to the gangsters where they can find the list after they threaten to cripple the boy. After the gangsters get the list and the boy is released, Giovanni escapes and saves the list by igniting a bomb that he finds in the gangsters hideout.

==Cast==
 
 
* Gene Kelly as Giovanni E. "Johnny" Columbo
* J. Carrol Naish as Louis Lorelli
* Teresa Celli as Isabella Gomboli
* Marc Lawrence as Caesar Xavier Serpi
* Frank Puglia as Carlo Sabballera
* Barry Kelley as Police Captain Thompson
* Mario Siletti as Benny Danetta / Nino
* Carl Milletaire as George Allani / Tomasino
 
* Peter Brocco as Roberto Columbo
* Eleonora von Mendelssohn as Maria Columbo
* Grazia Narciso as Mrs. Danetta
* Maurice Samuels as Moriani
* Burk Symon as Judge
* Bert Freed as Prosecutor
* Mimi Aguglia as Mrs. Sabballera
 

==Reception==
According to MGM records the film earned $772,000 in the US and Canada and $438,000 elsewhere; it recorded a loss of $55,000. 
===Critical response===
Film critic Bosley Crowther praised Kellys and Naishs work, while questioning the screenplay, "In his first straight role in a picture—away from dancing and singing, that is—Mr. Kelly is eminently forceful as a young Italian-American who aspires to help his neighbors rid themselves of the bands of terrorists and extortionists which are fearfully known as the Black Hand. And Mr. Naish is equally impressive as an Italian detective on the New York police force who joins in the youthful zealots campaign to wipe out this terrifying scourge. One might tactfully question the simplicity of the plot, prepared by Luther Davis, as a wee bit theatrical." 
 Night Must Fall/Malaya (film)|Malaya/Ivanhoe (1952 film)|Ivanhoe) sets a dark mood, while Irish-American hoofer Gene Kelly plays a brave Italian-American immigrant out to avenge his fathers death by the Mafia, at the turn of the 20th century. Its a rare dramatic role for Kelly, who seems to be at home in this genre." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 