Run, Angel, Run!
{{Infobox film
| name           = Run, Angel, Run!
| image          = File:Film_Poster_for_Run,_Angel,_Run!.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Jack Starrett
| producer       = Joe Solomon
| writer         = Jerome Wish V.A. Furlong
| narrator       =  William Smith Valerie Starrett Dan Kemp Stu Phillips
| cinematography = John M. Stephens
| editing        = Renn Reynolds
| studio         = 
| distributor    = Fanfare Films
| released       = May 22, 1970
| runtime        = 95 mins.
| country        = United States
| language       = English
| budget         = $96,000
| gross          = $13 million 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} William Smith 15th highest grossing film of 1969.

==Cast== William Smith as Angel
*Valerie Starrett as Laurie
*Dan Kemp as Dan Felton
*Gene Shane as Ron
*Lee de Broux as Pappy

==References==
 

==External links==
* 
* Valerie Starrett interview http://sixtiescinema.com/2012/07/10/valerie-starrett-this-angel-can-act-and-write/

 

 
 
 