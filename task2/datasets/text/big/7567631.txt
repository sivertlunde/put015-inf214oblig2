Jatt Punjab Daa
{{Infobox Film
| name           = Jatt Punjab Daa
| image          = Jattpunjabda 1992 film poster.jpg
| caption        = Film poster
| director       = Yograj Singh
| producer       = Yograj Singh
| writer         = 
| starring       = Yograj Singh Deep Dhillon
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1992
| runtime        = 
| country        = India
| awards         =  Punjabi
| budget         = 
| gross          = 
}}

Jatt Punjab Da is a Punjwood film, which released in 1992.

==Synopsis==
Balwant (Yograj Singh) is a young man who has ongoing disputes with one of his neighbors Jang Singh (Deep Dhillon). The court gives a decision in Balwants favor. Jang Singh is furious. He kills Balwants father and frames Balwant for it. After 7 years imprisonment, Balwant returns home to find out that Jang Singh has also killed his brother. He takes a revenge by killing each person who was involved in his fathers and brothers murder. And at the end, he kills Jang Singh. But he also dies multiple shots from the police.

==Cast==
* Yograj Singh .... Balwant
* Daljeet Kaur .... Balwants sister-in-law
* Deep Dhillon .... Jang Singh
* Neena Sidhu

Songs: Faqir Mauliwala, Dev Tharikewala & Gill Surjit

== External links ==
*  

 
 
 


 