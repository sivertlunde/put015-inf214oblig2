Queens of the Ring
{{Infobox film
| name     = Queens of the Ring
| image    = 
| director = Jean-Marc Rudnicki
| producer = 
| writer = 
| starring = Marilou Berry   Nathalie Baye   Audrey Fleurot   Corinne Masiero
| cinematography = 
| music = 
| country = France
| language = French
| runtime = 97 minutes
| released =  	
}}
Queens of the Ring or Les reines du ring  is a 2013 French comedy film directed by Jean-Marc Rudnicki. The film was supported by WWE Studios. 

==Plot==
A single mother with a criminal past is upset by being ignored by her child who is growing into adolescence and preferring the company of his school friends to hers. She decides to appeal to the only thing that her son is interested in by training to become a female wrestler, together with her workmates from the supermarket.
	
== Cast ==
* Marilou Berry - Rose / Rosa Croft
* Nathalie Baye - Colette / Wonder Colette
* Audrey Fleurot - Jessica / Calamity Jess
* Corinne Masiero - Viviane / Kill Biloute
* André Dussollier - Richard Cœur de Lion
* Jacques Frantz - Tonio
* CM Punk
* The Miz
* Eve Torres

==References==
 

== External links ==
*  

 
 
 
 


 
 