Bleach: The DiamondDust Rebellion
{{Infobox film
| name           = Bleach: The DiamondDust Rebellion
| image          = Bleach - The DiamondDust Rebellion.jpg
| caption        = The North American promotional poster for the film.
| film name = {{Film name| kanji          = 劇場版BLEACH The DiamondDust Rebellion もう一つの氷輪丸
| romaji         = Gekijōban Burīchi Za Daiyamondo Dasuto Reberion Mō Hitotsu no Hyōrinmaru}}
| director       = Noriyuki Abe
| producer       = 
| writer         = Masahiro Ōkubo Michiko Yokote Tite Kubo (original manga)
| starring       = Masakazu Morita Fumiko Orikasa Romi Park Akira Ishida
| music          = Shiro Sagisu
| cinematography = Toshiyuki Fukushima
| artdirector    = Tomoyuki Shimizu
| editing        = Hidetoshi Okuda Junichi Uematsu
| studio         = Aniplex   Bandai Co., Ltd.   Dentsu   Studio Pierrot   Sega   Shueisha   Sony Computer Entertainment Japan   TV Tokyo
| distributor    = Toho (Japan)   Viz Media (North America)
| released       =  
| runtime        = 95 minutes
| country        = Japan
| language       = Japanese English
| budget         = 
| gross          = USD|$6,915,595  
}}
  is the second animated film adaptation of the anime and manga series Bleach (manga)|Bleach. The film is directed by Noriyuki Abe and co-written by Michiko Yokote and Masahiro Ōkubo, and the theatrical release was on December 22, 2007. The theme music for the film is   by Sambomaster.  The DVD of the film was released on September 6, 2008. 

To promote the film, the opening and closing credits of the Bleach anime from episode 151-154 use footage from the film. Kubo also published a special manga chapter focusing on Hitsugayas past to further promote the film. The English release of the DVD was on September 8, 2009,  and it was aired on Adult Swim on December 5, 2009.  The official European release of the film was on September 6, 2010  and in the United Kingdom on Blu-ray on May 7, 2012.

==Plot== Rangiku Matsumoto is sent to escort an artifact known as the "Kings Seal" or "Oin", which is stolen during transport from the Soul Society by an unidentified and masked Soul Reaper and two mysterious Arrancar resembling girls who control fire and lightning. During the attack, Hitsugaya corners and seems to recognize the Soul Reaper, who wounds him and leaves. In the middle of battle, Hitsugaya abandons his post to pursue him and leaves his squad behind to fend for themselves. After the battle, the Soul Society suspects Hitsugaya of treason and orders his immediate capture. The whole 10th Squad is then put under house arrest at which time they can surmise whether or not the division should be abolished.
 Soi Fon, Izuru Kira and List of Soul Reapers in Bleach#Shūhei Hisagi|Shūhei Hisagi, and things only look worse when the deviated Soul Reaper thief named Sōjirō Kusaka uses Hyōrinmaru, a zanpakutō with identical powers to Hitsugayas, to injure 8th Division Captain Shunsui Kyōraku. The 6th Division is sent in to investigate this attack, with Byakuya Kuchiki confirming it to be done by Hyōrinmaru. Tōshirōs capture is then made top priority and his execution is approved.
 Rukia meets Ichigo. The group, along with Ishida, Orihime Inoue and Yasutora Sado exchange information, and Rukia reveals rather surprising, in-depth knowledge of Hitsugayas personality, as well as some rather accurate guesses as to what Hitsugaya plans to do next. During the talk, Ichigo realizes what might be troubling Hitsugaya and leaves to find him. Through his wandering to avoid capture, Tōshirō eventually manages to track Sōjirō down, where it is revealed that the two managed to manifest the same zanpakutō. Because it is impossible for the same zanpakuto to be wielded in Soul Society, the two were forced to fight to the death by the Central 46 Chambers of Soul Society. Tōshirō did not wish to fight, but was left with no other option as Kusaka attacked, claiming that he was the only one worthy of Hyōrinmaru. In the fight, Kusaka is defeated by Tōshirō and the authorities concluded that Tōshirō was the true owner of Hyōrinmaru, with the Military Corps carrying out his execution. Cursing Soul Society and its authorities, Kusaka died and was reborn in Hueco Mundo, where he became a hollow and started his plans of revenge against the Soul Society. Once he learned of the Kings Seal and its powers, which allows the user to freely manipulate time, space and matter within a small space, he leapt at the opportunity to settle the score with both Hitsugaya and the Soul Society.
 Yumichika Ayasegawa. As all Soul Reapers led by Captain-Commander List of Soul Reapers in Bleach#Genryūsai Shigekuni Yamamoto|Genryūsai Shigekuni Yamamoto appear on top of the hill. They attack Tōshirō and Kusaka but are thrown back when Ichigo and Rukia intervene, having just arrived in Soul Society. In front of everyone, Tōshirō refuses and Sōjirō breaks the seal himself, having gained enough strength to do so himself, after which he transforms into a giant dragon-like creature made of ice. Kenpachi Zaraki attacks and is defeated by the Dragon Kusaka, who then proceeds to mould the Soul Societys landscape at will. However, because he lacks the control which Tōshirō possesses, the power goes berserk and threatens to destroy the Soul Society. The Soul Reapers try to attack Kusaka all at once, but are repelled by the immense powers of the Kings Seal. Encountering Rangiku who had recovered his Captains Robe from Renji given from Ichigo, he accepts it. Ichigo and Rukia meet up with them and are joined by Renji, Ikkaku and Yumichika. A hoard of hollows appears out of Kusakas castle which he had generated out of the Kings Seal, and Yoruichi Shihoin and Soi Fon appear to defeat them. The group then splits up into two, and they both scale there. Battling with various Hollows, Menos and Adjuchas, they each formulate a strategy to attack Kusaka who is at the centre of the castle. Ichigo uses his Visored form while Tōshirō activates Bankai and they both storm up to the central tower, destroying Sōjirōs dragon-like bankai form. In one last clash, Tōshirō impales Sōjirō who asks an incomplete question and finally understands that he really was not worthy of Hyōrinmaru. Tōshirō clears all charges and the Kings Seal is restored, allowing him to finally put his past to rest.

After the credits, he and Rangiku visit Kusakas grave, where Rangikus lazy nature infers that the 10th Division is reformed.

==Production==
Tite Kubo, author from the Bleach manga, authored a one-shot based on Hitsugayas characters prior to the films premier in order to promote it. He was also allowed to participate in the making of the movie, to design the character of Kusaka. However, Kubo could not add Kusaka to his one-shot due to the fact the original concept came from the manga.  In order to promote the second film of Bleach, the trailer had the line "Execute Hitsugaya!". Kubo admitted that it was his own idea to make everybody be surprised, but he and Masakazu Morita, the voice actor of Ichigo Kurosaki, received a lot of letters from worried fans, causing Kubo to apologize in response.   

==New characters==
* Sōjirō Kusaka
:  
:: The primary antagonist of the film. Sōjirō appears in the  ,   and   as a playable character.

* Ying and Yang
: Ying  
: Yang  
:: Two twin Arrancar girls who are the henchman of Sōjirō Kusaka. Ying has long blue hair in pig tails while Yang has short red hair.

==Cast==
{| class="wikitable"
|-
! Character
! Japanese voice
! English voice 
|-
| Ichigo Kurosaki
| Masakazu Morita Yuki Matsuoka (child)
| Johnny Yong Bosch Mona Marshall (child)
|-
| Tōshirō Hitsugaya
| Romi Park
| Steve Staley
|-
| Rukia Kuchiki
| Fumiko Orikasa
| Michelle Ruff
|-
| Renji Abarai
| Kentarō Itō
| Wally Wingert
|- Rangiku Matsumoto
| Kaya Matsutani
| Megan Hollingshead
|- Ikkaku Madarame
| Nobuyuki Hiyama
| Vic Mignogna
|- Yumichika Ayasegawa
| Jun Fukuyama
| Brian Beacock
|-
| Byakuya Kuchiki
| Ryōtarō Okiayu
| Dan Woren
|-
| Kenpachi Zaraki
| Fumihiko Tachiki David Lodge
|-
| List of Soul Reapers in Bleach#Genryūsai Shigekuni Yamamoto|Genryūsai Shigekuni Yamamoto
| Masaaki Tsukada Bob Johnson
|-
| List of Soul Reapers in Bleach#Suì-Fēng|Suì-Fēng
| Tomoko Kawakami
| Karen Strassman
|- Retsu Unohana
| Aya Hisakawa
| Kate Higgins
|- Sajin Komamura
| Tetsu Inada
| J.B. Blanc
|- Shunsui Kyōraku
| Akio Ōtsuka Steve Kramer
|-
| Mayuri Kurotsuchi
| Ryūsei Nakao
| Terrence Stone
|-
| List of Soul Reapers in Bleach#Jūshirō Ukitake|Jūshirō Ukitake
| Hideo Ishikawa
| Liam OBrien
|-
| List of Soul Reapers in Bleach#Chōjirō Sasakibe|Chōjirō Sasakibe
| Taro Yamaguchi
| Dan Woren
|- Marechiyo Ōmaeda
| Shōto Kashii
| Lex Lang
|- Izuru Kira
| Takahiro Sakurai
| Grant George
|- Isane Kotetsu
| Yukana
| Stephanie Sheh
|- Nanao Ise
| Hitomi Nabatame
| Kate Higgins
|-
| List of Soul Reapers in Bleach#Shūhei Hisagi|Shūhei Hisagi
| Katsuyuki Konishi
| Steve Staley
|- Kiyone Kotetsu
| Chinami Nishimura
| Cindy Robinson
|- Yachiru Kusajishi
| Hisayo Mochizuki
| Stevie Bloch
|- Tetsuzaemon Iba
| Rintarou Nishi
| Steve Cassling
|- Yasochika Iemura
| Yutaka Aoyama
| Cam Clarke
|- Nemu Kurotsuchi
| Rie Kugimiya
| Megan Hollingshead
|-
| List of Soul Reapers in Bleach#Hanatarō Yamada|Hanatarō Yamada
| Kōki Miyata
| Spike Spencer
|-
| Orihime Inoue
| Yuki Matsuoka
| Stephanie Sheh
|-
| Yasutora Sado
| Hiroki Yasumoto
| Jamieson Price
|-
| Uryū Ishida
| Noriaki Sugiyama
| Derek Stephen Prince
|- Kisuke Urahara
| Shin-ichiro Miki
| Michael Lindsay
|- Yoruichi Shihōin
| Satsuki Yukino
| Wendee Lee
|- Tessai Tsukabishi
| Kiyoyuki Yanada
| Michael Sorich
|- Isshin Kurosaki
| Toshiyuki Morikawa
| Patrick Seitz
|- Yuzu Kurosaki
| Ayumi Sena
| Janice Kawaye
|- Karin Kurosaki
| Rie Kugimiya
| Kate Higgins
|- Masaki Kurosaki
| Sayaka Ohara
| Ellyn Stern
|- Jinta Hanakiri
| Takako Honda
| Cindy Robinson
|- Ururu Tsugumiya
| Noriko Shitaya
| Wendee Lee
|- Kon
| Mitsuaki Madono
| Quinton Flynn
|- Zangetsu
| Takayuki Sugō
| Richard Epcar
|-
| List of Bleach characters#Hyōrinmaru|Hyōrinmaru
| Daisuke Matsuoka
| Travis Willingham
|-
| Sōjirō Kusaka
| Akira Ishida
| Keith Silverstein
|-
| Ying
| Aya Hisakawa
| Kate Higgins
|-
| Yang
| Yukana
| Tara Platt
|-
| Soul Reapers
|
| Kyle Hebert Yuri Lowenthal
|-
| Central 46 Member
|
| Tara Platt
|}

==Reception==
 
The film opened in 4th place at the Japanese box office,  and held a top ten location until its 5th week. 

The DVD release of the film was the first best selling anime DVD released that week, and is now released in many different languages. 

==References==
 

==External links==
*    
*  
*  

 
 
 

 
 
 
 
 
 