Dombivali Fast
{{Infobox Film
| name           = Dombivli Fast
| image          = Dombivli Fast.jpg
| image_size     = 
| caption        = Poster
| director       = Nishikanth Kamath
| producer       = Sameer Gaikwad
| writer         = Nishikanth Kamath (screenplay) Sanjay Pawar (screenplay and dialogues)
| narrator       = 
| starring       = Sandeep Kulkarni  Shilpa Tulaskar  Sandesh Jadhav
| music          = Sanjay Mourya  Alwyn Rego Sameer Phatarpekar (background)
| cinematography = Sanjay Jadhav
| editing        = Amit Pawar
| distributor    = 
| released       = 2005
| runtime        = 112 mins
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}} breaking point, and his rampage as a vigilante across Mumbai to set things right. The film stars Sandeep Kulkarni in the lead with Shilpa Tulaskar and Sandesh Jadhav.

The film bears resemblance to the 1993 Hollywood film Falling Down, starring Michael Douglas.
 Tamil as Evano Oruvan with R. Madhavan playing the lead.

==Plot==
Madhav Apte is a common man with strong principles. He has strong values and does not believe in bending any rules for anybody, even when the future of his children is involved. He fights with people around when he sees injustice and corruption, which include his colleagues, his boss, shop keepers, school principal and even his wife. His principles and his behavior is a cause of fights between his wife and him. She is tired of him only preaching of changing the world but not doing anything about it.

He is pushed to a corner by everybody who finds his path of righteousness too difficult to handle, and one day he snaps. He goes on a rampage trying to do right, everything that goes against his principles and then starts a mayhem on the streets of Mumbai, ultimately ending in a tragic climax.

==Cast==
* Sandeep Kulkarni as Madhav Apte 
* Shilpa Tulaskar  as Madhavs wife
* Sandesh Jadhav as Insp. Subhash Anaspure
* Srushti Bhokse as Prachi Madhav Apte, Madhav Aptes daughter

==Awards==
* 2006 Star Screen Awards - Best Actor Male (Marathi) - Sandeep Kulkarni

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 


 