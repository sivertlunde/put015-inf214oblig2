Corner Store (film)
{{Infobox film
| name           = Corner Store
| image          = Corner Store.jpg
| caption        = 
| director       = Katherine Bruens
| writer         = Katherine Bruens Sean Gillane
| starring       = 
| music          = 
| cinematography = Sean Gillane
| editing        =
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Corner Store (2010) is a documentary film about Yousef, an owner of a corner grocery store in San Francisco, produced and directed by Katherine Bruens and Sean Gillane, acting as both cinematographer and co-writer. Filming began in December 2009.

Yousef Elhaj, an owner of a corner grocery store in San Francisco is the focus of Corner Store. The film is structured as a first-person narrative with an informal and intimate tone, and shot largely as cinema vérité. The camera introduces the audience to Yousef, who left Palestine ten years ago during the Second Intifada, and was forced to leave his wife and children behind as an economic refugee. During the interceding decade, Yousef has not only worked alone in the store but has also lived alone in the stores small back room, which also serves as the office and stock room.

As his children have grown into young adults, he has only been to visit them once, and then for only ten days. Yet he has put everything into ensuring the success of his business working from eight in the morning until midnight or two in the morning seven days a week and 365 days a year – for ten years. The film then follows the family as they embark on new challenges and adjustments as Palestinian Americans in the United States.

The filmmakers traveled to the West Bank to capture Yousefs return and the culturally beautiful but politically unstable condition of the West Bank today. The filmmakers documented their journey with a daily blog so supporters at home could track the production.    

==References==
 

==External links==
*  
*  
*  
*   
*  
*  

 
 
 
 
 
 
 
 
 

 