Straight (2009 film)
{{Infobox Film 
| name         = Straight
| image = Straight_(film).jpg
| starring     = Vinay Pathak Gul Panag
| director     = Parvati Balagopalan
| Music        = Sagar Desai
| producer     = Idream productions 
| country      = India English Hindi
| released     =  
}} Siddharth Makkar, Rasik Dave, Ketki Dave and Damandeep Singh in supporting roles.

== Plot == Siddharth Makkar), who is quite a contrast to his own personality. A casual air about him, he is the lead singer of a rock band. Rajat is very fond of Pinu, though is often a source of annoyance to him as he finds Pinu really funny. 

One day, a young Indian fellow called Kamlesh comes to his restaurant and asks for a job as a stand-up comedian. Quite thrown off, Pinu initially refuses but finally lets him join as a cook, who also does a stand-up act in the evenings. On the same day, he hires a new cashier - Renu (Gul Panag), a young art student from India, who has a passion for caricatures. Life changes dramatically for Pinu as Gaylord begins to transform... Renu works on the look of the place makes cheerful caricatures for customers while Kamles is a fabulous cook with a great talent for making people laugh in his acts and soon the restaurant is more happening and the business is much better. But the greatest difference they bring to his life is friendship. There is a visible difference in pinus personality now as he lets himself hang out a bit with them. 

One fine day, Pinu is thrown in to a daze as he walks away from the restaurant ...he has discovered a totally new fear that he might be gay 

On a mission now, Pinu goes on a rampage seeking available women for a sexual rendezvous. However, it leads to him suffering another string of tragicomic situations, ending up feeling humiliated.

== Cast ==
{| class="wikitable"
|-
! Actor || Role
|-
| Vinay Pathak || Pinu
|-
| Gul Panag || Renu
|- Siddharth Makkar || Rajat
|-
| Anuj Chaudari || Kamlesh
|-
| Rasik Dave || ?
|-
| Ketki Dave || ?
|-
| Damandeep Singh || ?
|-
| Parth Kanani || Waiter 1
|-
| Mian || Waiter 2
|-
| Leo Playa || Rock band member
|-
| Amaan Irees || Rock band member
|-
| Aneta Bo Piotrowska || Rock band member
|-
| Sam Lewis || Rock band member
|-
| Lachlan Robison || Doctor 1
|-
| Richard Clifton-Smith || Doctor 2
|-
| Esther Hamill || Lady in restaurant
|- Hannah Hamill || Lady in restaurant / Wedding guest
|}

== Crew ==
* Director: Parvati Balagopalan
* Production house: Idream Productions
* Music Director:Sagar Desai

== References ==
 
http://www.gulpanag.net/films/straight.php

http://www.glamsham.com/movies/previews/31-straight-movie-preview-050801.asp

==External links==

* 

 

 
 
 
 