Battle for Haditha
 
 
 
{{Infobox film
| name           = Battle for Haditha
| image          = BattleForHaditha2007Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Nick Broomfield
| producer       = Nick Broomfield
| writer         = Nick Broomfield Marc Hoeferlin Anna Telford
| starring       = Elliot Ruiz Yasmine Hanani Matthew Knoll
| music          = Nick Laird-Clowes
| cinematography = Mark Wolf
| editing        = Stuart Gazzard Ash Jenkins
| studio         =
| distributor    = HanWay Films
| released       = Spain: 14 December 2007 United Kingdom: 4 February 2008
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English Arabic
| budget         =
| gross          =
}}

Battle for Haditha is a 2007 drama film directed by British director Nick Broomfield based on the Haditha killings. Dramatising real events using a documentary style, Battle for Haditha is Broomfields follow up to Ghosts (2006 film)|Ghosts. The film was aired on Channel 4 in the UK on 17 March 2008.

==Plot== United States Al Anbar. Since the release of the film, however, the US military justice controversially  {{cite web
  | first = Mark
  | last = Walker
  | title = Immunity grants may signal problems with Haditha prosecution
  | publisher = North County Times
  | date = 20 April 2007
  | url = http://www.nctimes.com/articles/2007/04/21/news/top_stories/1_02_204_20_07.txt
  | accessdate = 26 August 2007 }}  dropped all charges to all Marines involved.  The names of the involved parties have been changed in the film. 

==Production==
Shot in Jerash, Jordan, the film uses former US Military personnel and Iraqi refugees to play many of the roles.  However, the film was shot in an unconventional way – it was shot sequentially enabling the cast to build their characters as the story progressed. It also used real locations, and a very small documentary style film crew. This greatly added to the feeling of reality. Actors, while working from a detailed script, and the final form of the film reflects that structure, were also able to improvise and add to the dialogue, making it their own.

==Cast==
The film features Elliot Ruiz as Cpl Ramirez, a Marine who loses his composure after watching a friend die, Jase Willette as PFC Cuthbert, the young Marine whose death sets off the chain of events, Yasmine Hanani as Hiba, a young Iraqi woman stuck in the middle of the chaos, Eric Mehalacopoulos as the no-nonsense Sgt Ross, Falah Flayla as a former Iraqi Army officer turned insurgent, and Thomas Hennessy Jr. as a Navy corpsman assigned to Kilo company.

==Film festivals== Toronto Film San Sebastian Film Festival on 29 September 2007.  It was also presented at the London Film Festival on 30 October 2007. 

==Critical reception==
As of May 2008, the review aggregator Rotten Tomatoes reported that 67% of critics gave the film positive reviews, based on 30 reviews.  Metacritic reported the film had an average score of 65 out of 100, based on 12 reviews. 

==See also==
*Cinema of Jordan

==References==
 

==External links==
* 
* 
* 
* 
* 
*  interview with Elliot Ruiz

 

 
 
 
 
 
 
 
 
 
 
 
 