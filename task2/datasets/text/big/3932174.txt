Wackiki Wabbit
{{Infobox Hollywood cartoon
| cartoon_name      = Wackiki Wabbit
| series            = Merrie Melodies (Bugs Bunny)
| image             = Wackiki wabbit title.JPG Charles M. Jones
| story_artist      = Tedd Pierce
| animator          = Ken Harris
| voice_actor       = Mel Blanc (uncredited) Michael Maltese (uncredited) Tedd Pierce (uncredited)
| layout_artist     = Bernyce Polifka
| background_artist = Gene Fleury
| musician          = Carl Stalling
| producer          = Leon Schlesinger Leon Schlesinger Productions
| distributor       = Warner Bros. Pictures
| release_date      = July 3, 1943
| color_process     = Technicolor
| runtime           = 7:00
| movie_language    = English
}}
Wackiki Wabbit is a 1943 Warner Bros. Merrie Melodies cartoon, starring Bugs Bunny. It was written by Tedd Pierce and directed by Chuck Jones. Voices were provided by Mel Blanc (Bugs), Tedd Pierce (the tall, thin man), and Michael Maltese (the short, fat man - the two mens appearances are rough caricatures of the actual men). The musical score was conducted by Carl Stalling.

Wackiki Wabbit is notable for its experimental use of strongly graphic, nearly abstract backgrounds. The title is a double play on words, with "Wackiki" suggesting both the island setting (as in "Waikiki") as well as suggesting "wacky" (crazy) along with the usual Elmer Fudd speech pronunciation of "rabbit", although Elmer does not appear in this picture.

This cartoon has fallen to the  .

==Plot==
 Asleep in the Deep". Delirious from hunger, they start imagining each other (or even their own limbs) as food. They spot an island in the distance and rush ashore, underscored by "Down Where the Trade Winds Play", a song used several times in the cartoon (and in others, such as Gorilla My Dreams), where they meet Bugs Bunny, who is munching on his carrot as usual. To his friendly, "Whats the good word, strangers?" they answer "FOOD!" and start after Bugs, who leaps away on a vine with a Tarzan yell.

Chasing Bugs through the jungle, they spy him, semi-disguised as one of the "natives", dancing. Bugs welcomes them with, "Ah! White Men! Welcome to Reef triggerfish|Humuhumunukunukuapuaaaa island." He then proceeds to speak in Polynesian-accented nonsense, a long stretch of which is subtitled simply "Whats up Doc?" and a very short segment is subtitled, "Now is the time for every good man to come to the aid of his party." The tall and skinny man says, "Well, thanks!" and the short, fat man, actually seeing the peculiar subtitle, "Ofa eno maua te ofe popaa", says, "Gee, did you say that?" The skinny man shrugs.
Bugs and the two men prepare the feast as they sing "Were gonna have roast rabbit" and Bugs realizes hes the roast rabbit and climbs back up his tree and then possessed by a Ghost, until the strings become tangled and he has to make a quick escape.
 Hays Office irises out.

==External links==
*  

 
{{succession box |
before= Jack-Wabbit and the Beanstalk | Bugs Bunny Cartoons |
years=1943 |
after= A Corny Concerto (not part of Bugs Bunny cartoons, but it is a one-shot)|}}
 

 
 
 
 
 
 
 