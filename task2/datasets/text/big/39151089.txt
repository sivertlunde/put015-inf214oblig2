Young & Beautiful
 
{{Infobox film
| name           = Young & Beautiful
| image          = Jeune et Jolie Young and Beautiful poster.jpg
| caption        = Theatrical release poster 
| director       = François Ozon
| producer       = Eric Altmayer Nicolas Altmayer
| writer         = François Ozon
| starring       = {{Plain list | 
* Marine Vacth 
* Charlotte Rampling
* Johan Leysen 
* Géraldine Pailhas 
* Frédéric Pierrot
}}
| music          = Philippe Rombi
| cinematography = Pascal Marti
| editing        = Laure Gardette Wild Bunch StudioCanal Arthaus Lionsgate Films
| released       =  
| runtime        = 93 minutes
| country        = France
| language       = French
| budget         = 
}}
 teenage prostitute, and features supporting performances by Charlotte Rampling, Johan Leysen, Géraldine Pailhas, and Frédéric Pierrot. The film was nominated for the Palme dOr at the 2013 Cannes Film Festival, and received praise from the film critics.    

It was shown at the 2013 Toronto International Film Festival.   

==Plot==
Isabelle is on summer holiday with her family in the south of France. The film opens with a scene of voyeurism when Victor uses binoculars from a cliff to watch his sister Isabelle sunning topless on a sun drenched French beach. She then decides to have sex for the first time with a German boy named Felix, but the experience leaves her cold. 
 cowgirl position when he dies of a heart attack. Isabelle vainly tries to save him by administering mouth-to-mouth resuscitation and CPR, and then she trips, injuring her forehead. She then leaves the room and quits prostitution. When police detectives investigate her clients death, they track down Isabelle and reveal her secret life to her mother, Sylvie, by informing her that her daughter is leading a double life as Lea and Isabelle. After learning that her daughter is a prostitute, Sylvie flies into a rage and repeatedly slaps Isabelle as she lies on a sofa. Sylvia then drags Isabelle to see a doctor (Serge Hefez).

After Isabelle quits prostitution she returns to living a normal teenage life. She goes to a party where she talks to a boy named Alex, who she eventually starts dating. Once they start having sex, she is once again left cold in which she ends their relationship. Her sex drive leads her back into prostitution. Georges widow Alice (Charlotte Rampling) gets Isabelles phone number from her deceased husbands address book, and learns that Isabelle was with him when he died. She poses as a client and arranges to meet with Isabelle in the lounge of the hotel. When they meet up, Alice tells Isabelle that she is Georges widow. She has a very pleasant demeanor as they talk about him and return to Room 6095, where he and Isabelle had their encounters. Alice says that dying while making love is a beautiful death and asks Isabelle to lay down beside her on the bed, and begins to caress Isabelles face. The film ends with Isabelle waking up to find that Alice has left.

==Cast==
* Marine Vacth as Isabelle
* Charlotte Rampling as Alice, Georges widow
* Johan Leysen as Georges Ferriere, Isabelles elderly client
* Frédéric Pierrot as Patrick, Isabelles stepfather
* Géraldine Pailhas as Sylvie, Isabelles mother
* Nathalie Richard as Véro
* Akéla Sari as Mouna
* Lucas Prisor as Felix, a German tourist
* Fantin Ravat as Victor, Isabelles brother
* Laurent Delbecque as Alex

==Reception==
 . Frédéric Pierrot can be seen behind Ravat.]] In the House Rooney wrote, " nlike that playful Hitchcockian quasi-thriller, Young & Beautiful is both more carnal and more sober, suggesting the danger and fragility inherent in the central character’s experimentation while keeping the dramatic intensity subdued."  Leslie Felperin of Variety (magazine)|Variety noted that the film was "a nuanced, emotionally temperate study of a precocious youth" and added that "its elegant execution will win warm regard   subject matter should lure audiences at art houses worldwide." 

Derek Malcolm of London Evening Standard wrote that Ozon was successful in "directing the slim and striking Vacth through a series of sex scenes, and also showing how the girl doesn’t really know what she is doing even when pretty experienced in the art of seduction."  While being appreciative of the film as a whole, Peter Bradshaw of The Guardian noted that the film was a "luxurious fantasy of a young girls flowering: a very French and very male fantasy, like the pilot episode of the worlds classiest soap opera." 

Rotten Tomatoes gives the film a score of 77% based on reviews from 35 critics. 

==Accolades==
{| class="wikitable" width="90%"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award / Film Festival
! Category
! Recipients and nominees
! Result
|- 2013 Cannes Cannes Film Festival Palme dOr
|François Ozon
|  
|- San Sebastián International Film Festival  TVE Otra Mirada Award
|François Ozon
|  
|-
|}

==Music==
The film tells the story of takes place over the course of a year and is divided into four segments, each separated by a song by Françoise Hardy. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 