Ballast (film)
 
 
{{Infobox film
| name           = Ballast
| image          = Ballast onesheet midsm.jpg
| caption        = 
| director       = Lance Hammer Mark Johnson
| writer         = Lance Hammer
| starring       = Michael J. Smith Sr. Jim Myron Ross Tarra Riggs Johnny McPhail
| music          = 
| cinematography = Lol Crawley
| editing        = Lance Hammer
| distributor    = Alluvial Film Company 
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Ballast is a 2008 film directed by Lance Hammer. It competed in the Dramatic category at the 2008 Sundance Film Festival, where it won the awards for Best Director and Best Cinematography.      The film received six nominations in the 2009 Film Independent Spirit Awards. 

==Plot==

The film opens with a local man driving to the home of Lawrence and Darius, twin brothers who operate a local store. Upon entering the home, the man discovers Darius dead in his bedroom with Lawrence sitting on the couch unable to speak. Lawrence then walks to a neighboring property and shoots himself in the chest, which he survives. In the hospital, it is revealed that Darius committed suicide. Depressed and unable to return to the store he owns, Lawrence spends his days at the property he shared with his brother. Meanwhile, James, Darius estranged pubescent son, steals Lawrences gun and holds him at gunpoint for money to buy crack. After a failing to repay debts to his drug dealer, James and his mother, Marlee, are targeted in a drive-by assault. Unable to return home, Marlee confronts Lawrence about Darius leaving her and James many years prior and moves into Darius vacant apartment. After she is fired from her job as a cleaner, James convinces Lawrence to buy them food, which he does. Bound by these numerous tragedies, the trio form a defacto family, determined to move forward, starting with Marlee convincing Lawrence to let her run the store. The pair loosely decides to raise James together, beginning with a shared homeschooling schedule to keep James away from the negative influences that led him to drugs in the first place. The film ends when Lawrence discovers that ammunition is missing. He confronts James, fearing he has found himself another gun, only to find that James had thrown the bullets into a stream, to prevent Lawrence from attempting to harm himself again.

==Cast==
* Michael J. Smith Sr.
* Jim Myron Ross
* Tarra Riggs
* Johnny McPhail

==Reception==
Ballast received critical acclaim. The review tallying website   of the Chicago Sun-Times gave the film four stars out of four, saying the film "inexorably grows and deepens and gathers power and absorbs us."   He later named it as one of the 20 best films of 2008. 

It was nominated for a 2008 Independent Spirit Award for Best Film.

===Top ten lists===
The film appeared on several critics top ten lists of the best films of 2008.     

*2nd - Sheri Linden, The Hollywood Reporter
*3rd - Wesley Morris, The Boston Globe
*4th - Kenneth Turan, Los Angeles Times (tied with Frozen River)

==References==
 

==External links==
 
* 
* 
* 

 
 
 
 
 
 