Lara Croft Tomb Raider: The Cradle of Life
{{Infobox film
| name           = Lara Croft Tomb Raider: The Cradle of Life
| image          = Lara Croft Tomb Raider - The Cradle of Life Poster.png
| caption        = Theatrical release poster
| screenplay     = Dean Georgaris
| story          = Steven E. de Souza   James V. Hart
| based on       =   Christopher Barrie
| director       = Jan de Bont Lawrence Gordon
| music          = Alan Silvestri Michael Kahn
| cinematography = David Tattersall
| studio         = Mutual Film Company Eidos Interactive  Lawrence Gordon Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 117 minutes
| country        = United States Germany Japan United Kingdom 
| language       = English Mandarin
| budget         = $95 million 
| gross          = $156,505,388 
}}

Lara Croft Tomb Raider: The Cradle of Life (or simply Tomb Raider: The Cradle of Life or Tomb Raider 2) is a 2003  . 

Like the first film, the film received mostly negative reviews, though critics noted an improvement on its predecessor particularly in the action sequences and continued to praise Jolies performance as Lara Croft. Despite the films critical improvement over its predecessor, it did not repeat its financial success, grossing $156 million compared to the previous installments $274 million.

==Plot== MI6 to plague (the companion to the origin of life itself), before Nobel Prize-winning scientist turned bio-terrorist Jonathan Reiss (Ciarán Hinds) can get his hands on it and unleash it on the world. The key to finding the box, which is hidden in the mysterious Cradle of Life, is a magical luminous sphere that serves as a map, the one stolen by Chen Lo in Santorini (Simon Yam), who plans to sell the sphere to Reiss.

MI6 offer Lara the help of two of their best operatives, but she flat-out refuses them, insisting she needs to work with an old flame of hers who has knowledge of Chen Los criminal operation. They are reluctant but having no other choice relent and release Terry from a maximum security prison. Much to Terrys delight, who is delighted to have the opportunity to work with Lara again, (after their previous heated five months). They infiltrate Chen Los lair, where hes excavating/smuggling Terracotta Soldiers, where Lara defeats him in a fight, winning back her swiped medallion and forcing the location of the stolen orb/map off of him. Lara and Terry then meet up with Kosa (Djimon Hounsou), an African friend who serves as her translator, as they obtain information from a local tribe about the Cradle of Life. Kosa translates for the tribes Chief, stating that the Cradle of Life is in a crater protected by the "Shadow Guardians". As the expedition sets out, Lara, Kosa, and the tribesmen with them are ambushed by Reiss soldiers. More tribesmen are killed by Reiss soldiers with some of the soldiers being killed by Lara in the fight. The battle ends with Lara surrendering to overwhelming odds, as Reiss helicopter started to land. Reiss and Sean (Til Schweiger) threaten to kill Bryce, Hillary, and Kosa unless Lara leads him and his Tribeman to the Cradle of Life. Upon arrival at the crater, they encounter the Shadow Guardians, humanoid creatures which kill immediately when they sense a movement and vanish into wet patches on dead trees. Sean and most of Reiss soldiers are killed by the creatures. When Lara drops the Orb into the hole that opens the entrance to the Cradle of Life, the Guardians fall to pieces and both Lara and Reiss are drawn into the Cradle of Life, a labyrinthine cavern made of some strange crystalline substance, racked by bolts of energy. Inside, there is a pool of highly corrosive black acid (linking back to one of the myths about Pandoras Box), which holds the box and where the laws of physics do not apply, as Lara and Reiss are able to walk (upside down) along the ceiling of the cave. Terry arrives, frees Reiss captives, and catches up to Lara. 

Following a climatic fistfight between Lara and Reiss, Reiss is knocked into the acid pool by Lara after he is distracted by Terry, killing and dissolving him. When the couple tries to leave, Terry attempts to take the box as compensation for finding it; but she staunchly refuses to let him, knowing the danger if the box were ever open. When he challenges her ability to stop him, she shoots him dead and leaves him lying by the pool where the box still floats. 

==Cast==
 
* Angelina Jolie as Lara Croft
* Gerard Butler as Terry Sheridan
* Ciarán Hinds as Jonathan Reiss
* Chris Barrie as Hillary
* Noah Taylor as Bryce
* Djimon Hounsou as Kosa
* Til Schweiger as Sean
* Simon Yam as Chen Lo
* Terence Yin as Xien
* Daniel Caltagirone as Nicholas Petraki
* Fabiano Martell as Jimmy Petraki
* Ronan Vibert as MI6 Agent Calloway

==Production==
The budget for Cradle of Life was $100 million (less than the first films $115 million budget), and like the first film, it was financed through Tele-München Gruppe. The picture was also distributed internationally by Japanese company Toho-Towa.   (English) 

Filming lasted for three and a half months, which included six-day shoots on location in Hong Kong, Santorini,  ) after the government complained that it portrayed their country as lawless and "overrun with secret societies".   - Internet Movie Database  One scene in the movie was set in Shanghai, but it was shot on a set and not on location. 
 Wrangler Rubicon, first seen when Lara parachutes into the moving vehicle in Africa and takes over the wheel from Kosa. As part of Jeeps advertising campaign, it was specially customised for the film by Jeeps design team along with Cradle of Life production designers, with three copies constructed for filming.   - Motor Trend, 4/29/03  1,001 limited-run Tomb Raider models were produced—available only in silver like the film version and minus its special customisations—and put on the market to coincide with the release of the film. Jeep vice president Jeff Bell explained, "  is more than just a product placement   the Jeep Wrangler Rubicon is the most capable Jeep ever built, so the heroic and extreme environment in which Lara Croft uses her custom Wrangler Rubicon in Tomb Raider is accurate."   - Difflock.com  In the end, Laras Rubicon had less than two total minutes of screen time in the finished film.

==Release==
===Critical reception===
Cradle of Life holds a 24% rating out of 166 reviews on  .   - Metacritic    gave the film 3 out of 4 stars, stating that the film was "better than the first one, more assured, more entertaining   it uses imagination and exciting locations to give the movie the same kind of pulp adventure feeling we get from the   praised Jolie for being "hotter, faster and more commanding than last time around as the fearless heiress/adventuress, plus a little more human."   - Variety.com, 7/25/03 

Cradle of Life was nonetheless heavily panned. Rene Rodriguez of the   wrote, "Its a bullet-riddled   nomination for Worst Actress for her performance in the film.

===Box office performance===
Despite the slightly more favorable critical response, Cradle of Life suffered a disappointing opening weekend, as it debuted in fourth place with a take of $21.8 million,   - Box Office Mojo  a 54% drop from the originals opening gross of $47.7 million. In the United Kingdom|UK, the film opened up at number three, earning £1.5 million in its first three days.  The film finished with a domestic gross of only $65 million.

Overall, 2003 was not a good year for the Tomb Raider franchise. Paramount blamed the failure of Cradle of Life on the poor performance of the then-latest installment of the video game series,  .   - Entertainment Weekly, 7/29/03  After numerous delays, Angel of Darkness was rushed to shelves just over a month before the release of the movie, despite the final product being unfinished and loaded with glitches. It spawned mediocre sales while garnering mixed reviews from critics,   - Metacritic  and former Eidos Interactive senior executive Jeremy Heath-Smith, who was also credited as an executive producer in the film, resigned days after the game was released. 

In March 2004, producer Lloyd Levin said that Cradle of Life had earned enough internationally for Paramount to bankroll a second sequel, but any hopes of it going into production were soon quelled by Jolies announcement that she had no desire to play Lara Croft a third time. "I just dont feel like I need to do another one. I felt really happy with the last one. It was one we really wanted to do."   - IGN.com, 3/16/04 

==Music== score for the movie.

===Soundtrack===
{| class="wikitable"
|-
!  # !! Song title !! Artist
|-
| 1 || "Heart Go Faster" || Davey Brothers
|- Filter
|-
| 3 || "Bad Girl" || Alexandra Slate
|-
| 4 || "Satellite (P.O.D. song)|Satellite" (Oakenfold Remix) || P.O.D.
|- The Last High" || The Dandy Warhols
|- Saliva
|-
| 7 || "Leave You Far Behind" || Lunatic Calm
|-
| 8 || "Jam for the Ladies" (Jason Nevins Remix) || Moby
|-
| 9 || "Starting Over" || The Crystal Method
|- Sloth
|- Nadirah "Nadz" Seid
|-
| 12 || "Reason Is Treason" || Kasabian
|-
| 13 || "Into Hell Again" || 3rd Strike
|-
| 14 || "Tears from the Moon" (Chillout Mix) || Conjure One, Sinéad OConnor
|-
| 15 || "Flight to Freedom" || David A. Stewart
|-
| 16 || "Pandoras Box" || Alan Silvestri
|}
The track "Did My Time" by Korn was supposed to appear on the soundtrack, but due to problems with Korns record company, this did not happen. The song still appears during the films end credits.

===Score===
*Composed by Alan Silvestri
*Performed by London Symphony Orchestra
*Conducted by Alan Silvestri

{| class="wikitable"
|-
!  # !! Song title
|-
| 1 || "Opening"
|-
| 2 || "The Luna Temple"
|-
| 3 || "Shark Attack"
|-
| 4 || "I Need Terry Sheridan"
|-
| 5 || "Arrival in China"
|-
| 6 || "Captured by the Shay Ling" 
|-
| 7 || "Escape from Chen"
|-
| 8 || "Flower Pagoda Battle"
|-
| 9 || "Skydive Getaway"
|-
| 10 || "Orb Transmission"
|-
| 11 || "Journey to the Cradle of Life"
|-
| 12 || "The Cradle of Life"
|-
| 13 || "Pandoras Box"
|-
| 14 || "Not Meant to Be Found"
|-
| 15 || "Lara Croft – Tomb Raider"
|}

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 

 
   
   
   
 
 
 
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 