Down Our Street
{{Infobox film
| name           = Down Our Street
| image          = 
| caption        = 
| director       = Harry Lachman
| producer       = S.E. Fitzgibbon
| writer         = Ernest George (play) Harry Lachman
| starring       = Hugh Williams Nancy Price Elizabeth Allan
| music          = 
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| filmed         = 1932
| released       = 
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Down Our Street is a 1932 black and white film directed by Harry Lachman.

==Plot==
This drama, set during the Depression, sees the character Charlie Stubbs trying to escape his poverty by becoming a criminal. When this course of action fails he cleans up his act and becomes a cab driver for the woman he loves, Annie Collins. Trouble follows Charlie however when Annies uncle discovers his past.

==Cast==
*Hugh Williams as Charlie Stubbs
*Nancy Price as Annie Collins
*Elizabeth Allan as Maisie Collins
*Binnie Barnes as Tessie Bemstein
*Frederick Burtwell as Fred Anning
*Sydney Fairbrother as Maggie Anning
*Alexander Field as Sam
*Morris Harvey as Bill Collins
*Merle Tottenham as Rose

==External links==
*  
*  

 
 
 
 
 
 
 
 

 