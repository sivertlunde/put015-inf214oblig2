Lonesome Luke, Mechanic
{{Infobox film
| name           = Lonesome Luke, Mechanic
| image          =
| image size     =
| caption        =
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| narrator       =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent film English intertitles
| budget         =
| preceded by    =
| followed by    =
}}
 1917 short short comedy film featuring Harold Lloyd.

==Cast==
* Harold Lloyd - Lonesome Luke
* Snub Pollard
* Bebe Daniels
* Arthur Mumas
* Sammy Brooks
* W.L. Adams
* Bud Jamison
* Sidney De Gray
* Lottie Case
* May Ballard
* Gus Leonard
* Harvey L. Kinney
* Elmer Ballard
* Estelle Harrison
* Dorothea Wolbert
* Marie Mosquini

==See also==
* Harold Lloyd filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 