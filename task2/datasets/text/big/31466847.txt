Debtocracy
{{Infobox film
| name = Debtocracy
| image = Debtocracy poster.png
| director = Katerina Kitidi Aris Chatzistefanou
| writer = Katerina Kitidi Aris Chatzistefanou
| producer = Kostas Efimeros
| music = Giannis Aggelakas
| editing = Leonidas Vatikiotis
| released =  
| runtime = 74 minutes
| language = Greek
| country = Greece
}} Greek debt crisis in 2010 and advocates for the default of "odious debt".
 Creative Commons Greek and English and is subtitled in French, Spanish, Italian and Portuguese.    According to The Nation, half a million people watched the film in the first five days of its release. 

The film was followed by a Greek-language book with the same title. 

==Name==
The production team has coined   the word "debtocracy" (Greek "Χρεοκρατία"), defining it as the condition by which Greece found itself trapped in its debt.  The term is derived from the words "χρέος" (debt) and the  ). The title is meant to imply that the Greek government has been functioning mainly under the interests of the financial debt (at points superseding or even replacing the principles of Democracy and the Constitution of Greece), and therefore the debt has been elevated as a de facto form of government by itself.

==Content summary==
 .]] global economic crisis and its origins in the 1970s. Interviews with prominent  figures of the global philosophical and economic scene argue that the euro is non-viable and contributed to the worsening of the finances of Greece due to a systematic loss of competitiveness in the markets by the PIGS (economics)|PIGS.
 revolution of odious the people should not have to pay for it and therefore it should be erased.

==Production and participation==
The production team of Debtocracy have said that the producers are all those individuals that donated money in order to finance the project.    Interviewees include: David Harvey, geographer and social theorist
*Hugo Arias, president of the debt analysis committee of Ecuador
*Samir Amin, economist
*Gerard Dumenil
*Costas Lapavitsas, economist
*Alain Badiou, philosopher
*Manolis Glezos, member of the Greek Resistance and left-wing politician
*Avi Lewis, journalist and film director
* Sahra Wagenknecht

==Critical response==
The documentary has received mixed reviews, both for its use of economics and its political intention as a film, both by Greek and international media.
 Aganaktismenoi movement who surrounded the Greek Parliament in 2011.

The critics against the film rely on four points:
; Accusation of propaganda 
: The film is suspected to be only a part of a marxist propaganda
; False comparison of Greece with Argentina and Ecuador
: The  purpose of the film is to say that Argentina and Ecuador where right to refuse to pay but that comparison has been found incorrect.
; Lack of opposing point of views
: Only people in favor of a Greek default have been questioned
; No alternative propositions
: Only a cancellation of the greek debt is invoked. Nothing about competitivity of the greek economy.

Writing for the British The Guardian|Guardian, Aditya Chakrabortty said the movies criticism of the euro was "compelling" and, despite its low budget, called the documentary "the best film of Marxian economic analysis yet produced".   

Major Greek newspapers such as To Vima and Kathimerini have criticized the documentary as a work of political propaganda.       To Vima argues that Greeces and Ecuadors economies have little in common, as Ecuador is a major oil producer for its size and population, contrary to Greece.  

A similar critical review was published by Kathimerini, stating that Debtocracy is aimed at promoting political propaganda rather than objectively presenting a proposed solution to the Greek crisis.  Kathimerinis review also fell in line with that of To Vima, saying that both the examples of Ecuador and Argentina are unfortunate as they bear no resemblance to Greece and come in full contrast with the documentarys line of argument. 

Other Greek media that have criticized the film include Skai TV.    

=== In universities ===
Professor of Economics at the University of Athens wrote a letter to Debtocracy explaining the reasons for which he refused to be included in the documentary production team, and said among other things that he does not believe that Greece and Argentina have anything in common and that a default would be an easy  solution.  

Former Aristotle University of Thessaloniki professor, and London School of Economics Doctor of Philosophy, Eleftheria Karnavou wrote a letter to the newspaper Agelioforos arguing that being completely against the documentary is unjustified, even though it is politically and scientifically unfounded, as it provides food for thought.   

=== Response to criticisms ===
When asked about the one-sidedness of the film by the Greek newspaper Eleftherotypia, the producers replied that they had made it clear from the start who was funding the project,    that they used abstracts from documentaries produced by reporters of the BBC,  and that those people that speak out against Debtocracy are the same people that find the television news broadcasts to be objective and credible. 

==See also==
* Odious debt
* 2010 Greek economic crisis
* Economy of Greece
* Propaganda model

==References==
 

==External links==
*    
*   
*  
*  , Owni.eu, May 6, 2010

 
 
 
 
 
 
 
 
 
 