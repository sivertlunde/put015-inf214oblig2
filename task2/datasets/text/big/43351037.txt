Katha Ithuvare
{{Infobox film 
| name           = Katha Ithuvare
| image          =
| caption        =
| director       = Joshiy
| producer       = Joy Thomas
| writer         = AR Mukesh Kaloor Dennis (dialogues)
| screenplay     = Kaloor Dennis Madhu Mammootty Rohini
| Johnson
| cinematography = CE Babu
| editing        = K Sankunni
| studio         = Jubilee Productions
| distributor    = Jubilee Productions
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Rohini in lead roles. The film had musical score by Johnson (composer)|Johnson.    

==Cast== Madhu
*Mammootty
*Thilakan Rohini
*Shalini|Baby Shalini
*Lalu Alex
*Rahman
*Suhasini

==Soundtrack== Johnson and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Cherunnu njangalonnaay || P Jayachandran, CO Anto, Krishnachandran || Poovachal Khader || 
|-
| 2 || Mazhavillin Malarthedi || K. J. Yesudas, KS Chithra || Poovachal Khader || 
|-
| 3 || Ragini Ragaroopini || K. J. Yesudas, KS Chithra || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 