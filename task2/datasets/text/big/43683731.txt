The Wolf at the Door
{{Infobox film
| name = The Wolf at the Door
| image = Oviri - The Wolf at the Door.jpg
| caption =
| director = Henning Carlsen
| writer =
| starring =
| music = Roger Bourland Ole Schmidt
| cinematography = Mikael Salomon
| editing =
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country =
| language = French English
| budget =
}} biographical drama film written and directed by Henning Carlsen. It is based on real life events of French artist Paul Gauguin.  It was entered into the main competition at the 43rd Venice International Film Festival. 

== Cast ==
*Donald Sutherland as Paul Gauguin 
*Max von Sydow as  August Strindberg
*Sofie Gråbøl as  Judith Molard
*Jean Yanne as  William Molard
*Valeri Glandut as  Annah
*Ghita Nørby as  Ida Molard
*Merete Voldstedlund as  Mette Gad
*Fanny Bastien as  Juliette Huet
*Jørgen Reenberg as  Eduard Brandes Henrik Larsen Julien Leclercq
*Yves Barsacq as  Edgar Degas

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 