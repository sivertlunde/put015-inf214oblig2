Coming Out (2000 film)
{{Infobox film
| name           = Coming Out
| image          = Coming Out poster.jpg
| caption        = Promotional poster
| film name      = {{Film name
| hangul         =    
| rr             = Keoming aut
| mr             = K‘ŏming aut }}
| director       = Kim Jee-woon
| producer       = Kim Sung-je
| writer         = Kim Jee-woon   Lee Hae-young   Lee Hae-jun
| starring       = Gu Hye-ju   Shin Ha-kyun   Jang I-ji 
| cinematography = Choi Young-hwan
| editing        = Lee Jae-woong
| music          = Lee Young-ho
| distributor    = Media 4M
| released       =  
| runtime        = 45 minutes
| country        = South Korea
| language       = Korean
}}
Coming Out is a 2000 South Korean short film directed by Kim Jee-woon.

== Plot ==
The film opens with a man being interviewed about his sister, who has recorded a video diary in which she makes a shocking confession to her friends and family. Purporting to be a true story, the bulk of the film is presented as a reconstruction of actual events.

Hyun-joo announces to her younger brother Jae-min and his girlfriend Ji-eun that she has an important confession to make, and asks them to record it on video. After issuing an apology to her parents, she reveals that she has been hiding a painful secret and is not like normal people; she is in fact a vampire. Jae-min and Ji-eun initially think she is playing a prank, but to prove her sincerity Hyun-joo bites into her wrist and starts to suck her own blood. She goes on to disclose further details of her life as a vampire, and dispels many of the common myths associated with the legend. Having heard of others like her overseas, she has decided to join a community of vampires living in England.

Going out at night to attract less attention, Jae-min films his sister as she feeds on a young woman in a telephone box. He and Ji-eun later talk to the woman, who remains unharmed, and she tells them that the experience was not painful, likening the sensation to an electric shock. The events prove to them that Hyun-joo is telling the truth.

Some time later, Ji-eun visits Hyun-joo who is now living in England. Curious to know how it feels being bitten by a vampire, she asks Hyun-joo to suck her blood. Choosing the inside of her thigh, Hyun-joo begins to feed on her friend.

== Cast ==
* Gu Hye-ju ... Hyun-joo
* Shin Ha-kyun ... Jae-min
* Jang I-ji ... Ji-eun
* No Eul ... Girl in telephone booth
* Kim Il-woong ... Interviewer
* Im Won-hee ... Tutor

== Production ==
Kim Jee-woon wrote and directed Coming Out as part of a project to distribute three digital short films online. Commissioned by venture group Media 4M, the project also included shorts by   in South Korea was still in its infancy, and went on to inspire many other digital productions. 

== Release and critical response ==
Coming Out was first broadcast online on 7 August 2000.  In 2001 it was shown at the Fantasia Festival, and a report for Aint It Cool News described the film as a "sexy and funny little short" with "a series of truly hilarious and unnerving scenes", also noting the "unmistakable overtones to the imagery" with regard to the final scene where Hyun-joo draws blood from her friends thigh.  Other festival screenings include the Puchon International Fantastic Film Festival in 2001,  and the Thessaloniki International Film Festival in 2005.  Coming Out was also included as a special feature on the UK DVD release of The Quiet Family, and a review at DVDActive praised it as "delicate, cerebral and contemporary cinema at its most profound." 

==See also==
*Vampire film

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 