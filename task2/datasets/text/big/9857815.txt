Buenos Aires Sings
{{Infobox film
| name           =Buenos Aires Sings
| image          =
| image_size     =
| caption        =
| director       =Antonio Solano
| producer       =
| writer         = Santos Rodolfo Stella 
| narrator       =
| starring       = Niní Marshall   Hugo del Carril   Azucena Maizani   Homero Cárpena
| music          =
| cinematography =
| editor       =
| studio         = Sucesos Argentinos 
| distributor    =
| released       = 3 June 1947 
| runtime        =
| country        = Argentina Spanish
| budget         =
}}
Buenos Aires Sings (Spanish:Buenos Aires canta) is a 1947 Argentine musical film directed by Antonio Solano and starring Niní Marshall, Hugo del Carril and Azucena Maizani.  The film is part of the tradition of tango films.

==Cast==
* Niní Marshall 
* Hugo del Carril
* Azucena Maizani
* Homero Cárpena
* Oscar Alemán
* Oscar Alonso
* Los Lecuona Cuban Boys
* Lilia Bedrune
* Los Mills Brothers
* Chola Luna
* Ernesto Famá
* Francisco Amor

==References==
 

==Bibliography==
* Etchelet, Raúl. Niní Marshall: (la biografía).  La Crujía Ediciones, 2005.

==External links==
*  

 
 
 
 
 
 
 
 
 

 