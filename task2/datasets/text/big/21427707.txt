One Is a Lonely Number
{{Infobox film
| name           = One Is a Lonely Number
| caption        =
| image	         = One Is a Lonely Number FilmPoster.jpeg
| director       = Mel Stuart
| producer       = Stan Margulies
| writer         = Rebecca Morris David Seltzer
| starring       = Trish Van Devere Monte Markham Janet Leigh Melvyn Douglas Jane Elliot
| music          = Michel Legrand
| cinematography = Michel Hugo
| editing        = David Saxon MGM
| released       =  
| runtime        = 97 min.
| country        = United States
| awards         = English
| budget         =
}}

One Is a Lonely Number (also known as Two Is a Happy Number) is a 1972 drama film directed by Mel Stuart and starring Trish Van Devere, Janet Leigh and Melvyn Douglas. The story was written by Rebecca Morris and David Seltzer.

==Plot==
The story follows Aimee Brower (Van Devere), who wakes and finds her husband has left her. After learning everything about why he did it, she then proceeds to put her life back together.

==Cast==
*Trish Van Devere as Amy Brower
*Monte Markham as Howard Carpenter
*Janet Leigh as Gert Meredith
*Melvyn Douglas as Joseph Provo
*Jane Elliot as Madge Frazier Jonathan Lippe as Sherman Cooke
*Mark Bramhall as Morgue Attendant Paul Jenkins as James Brower
*Scott Beach|A. Scott Beach as Frawley King
*Henry Leff as Arnold Holzgang
*Dudley Knight as King Lear
*Maurice Argent as Pool Manager
*Thomas McNallan as Hardware Clerk Joseph Spano as Earl of Kent

==Awards and nominations==
Golden Globe Awards
*1973: Nominated, "Best Motion Picture Actress in a Dramatic Film" - Trish Van Devere

==External links==
*  

 

 
 
 
 
 
 
 
 
 