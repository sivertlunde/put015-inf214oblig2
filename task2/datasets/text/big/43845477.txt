Nanda Gokula
{{Infobox film
| name           = Nanda Gokula
| image          = 
| caption        = 
| director       = Y. R. Swamy
| producer       = Shyamprasad Enterprises
| writer         = 
| screenplay     = Y. R. Swamy   Chi. Udaya Shankar
| story          = Chi. Udaya Shankar
| based on       =  
| narrator       =  Rajkumar  Bharathi   Ramesh   K. S. Ashwath
| music          = Vijaya Bhaskar
| cinematography = R. Chittibabu
| editing        = P. Bhaktavatsalam
| studio         = Shyamprasad Enterprises
| distributor    = 
| released       = 1972 
| runtime        = 147 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Jayanthi in the lead roles.  Vijaya Bhaskar scored the music and the story was written by Chi. Udaya Shankar.

== Cast == Rajkumar  Jayanthi 
* Ramesh Balakrishna
* K. S. Ashwath
* Dwarakish
* B. V. Radha
* M.N Lakshmi Devi
* Sampath
* Dinesh
* Advani Lakshmi Devi
* Shakti Prasad
* Srinath in a guest appearance

== Soundtrack ==
The music of the film was composed by Vijaya Bhaskar. 

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Lyrics
|- 
| 1
| "Nanjana Goodinda"
| P. B. Sreenivas, S. P. Balasubrahmanyam
| Doddarangegowda
|-
| 2
| "Nee Janisida Dinavu"
| P. B. Sreenivas
| Chi. Udaya Shankar
|-
| 3
| "Ee Dina Januma Dina"
| P. B. Sreenivas
| Chi. Udaya Shankar
|-
| 4
| "Manada Maathige"
| P. Susheela
| Vijaya Narasimha
|-
|}

==See also==
* Kannada films of 1972

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 


 

 