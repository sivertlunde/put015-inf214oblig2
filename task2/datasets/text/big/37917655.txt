The Christmas Tree (1969 film)
{{Infobox film
| name = The Christmas Tree
| image =  The Christmas Tree (1969 film).jpg Terence Young
| writer =
| starring = William Holden
| music = Georges Auric
| cinematography = Henri Alekan
| editing = Monique Bonnot
| producer =
| distributor =
| released =
| runtime =
| awards =
| country =
| language =
| budget =
}}    Terence Young.  It was defined as "the most tearful film of sixties".  The film was co-produced by Italy where it was released as Lalbero di Natale.

==Plot==
The story follows a Frenchman named Laurent and his son Pascal, who live somewhere in France. Along the way, the widower Laurent meets and falls for the beautiful Catherine, but also learns that his son is dying after witnessing the explosion of a plane with a nuclear device inside. Finding this out, Laurent and Pascal have a string of adventures with Catherine along.

== Cast ==
* William Holden: Laurent Ségur
* Virna Lisi: Catherine Graziani
* Bourvil: Verdun 
* Brook Fuller : Pascal Ségur
* Mario Feliciani:  Paris doctor
* Madeleine Damien: Marinette
* Friedrich von Ledebur: Vernet
* Georges Douking: Pet owner
* Jean-Pierre Castaldi: The motorcycle policeman 
* Yves Barsacq: Charlie Lebreton

==References==
 

==External links==
* 

 

 
 
 
 
 

 