The Hero (2004 film)
{{Infobox film
| name          = The Hero
| image         = The Hero poster.jpg
| caption       = French theatrical poster.
| director      = Zézé Gamboa
| producer      = Fernando Vendrell
| writer        = Carla Baptista Pierre-Marie Goulet Fernando Vendrell
| starring      = Oumar Makena Diop Milton Santo Coelho Maria Ceiça
| music         = David Linx Diederik Wissels
| cinematography= Mario Masini
| editing       = Anna Ruiz
| distributor   = 
| released      =  
| runtime       = 97 minutes
| country       = Angola France Portugal
| language      = Portuguese
| budget        = 
}}
The Hero ( ) is a film about the life of average Angolans after the Angolan Civil War. The film follows the lives of four individuals; Vitório, a war veteran crippled by a landmine who returns to Luanda, Manu, a young boy searching for his soldier father, Joana, a teacher who mentors the boy, and Judite (later known as Maria Barbara), a prostitute who begins a romantic relationship with Vitório. Directed by Zézé Gamboa, The Hero won the 2005 Sundance World Dramatic Cinema Jury Grand Prize. It is a joint Angolan, Portuguese, French production, but was filmed entirely in Angola.   California Newsreel 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 