Trastevere (film)
{{Infobox film
| name = Trastevere
| image = Trastevere (film).jpg
| caption =
| director = Fausto Tozzi
| writer =Fausto Tozzi 
| starring = Nino Manfredi Rosanna Schiaffino Vittorio De Sica
| music = Maurizio De Angelis Guido De Angelis
| cinematography = Arturo Zavattini
| editing =
| producer = Alberto Grimaldi
| distributor =
| released =  
| runtime =
| awards =
| country = Italy Romanesco
| budget =
}} Italian comedy Riccardo Garrone. 

== Cast ==
*Nino Manfredi: Carmelo Mazzullo
*Rosanna Schiaffino: Caterina Peretti aka Rama
*Vittorio Caprioli: Don Ernesto
*Ottavia Piccolo: Nanda
*Vittorio De Sica: Enrico Formichi
*Leopoldo Trieste: The professor
*Mickey Fox (as Mikey Fox): Sora Regina
*Milena Vukotic: Delia, wife of professor
*Gigi Ballista: The count
*Ronald K. Pennington (as Ronald Kerry Pennington): Kerry
*Luigi Uzzo: Cesare
*Lino Coletta: Alvaro Diotallevi
*Don Powell: John
*Rossella Como: Teresa, the prostitute
*Fiammetta Baralla (as Fiammetta): Gigliola, other prostitute
*Enzo Cannavale: Straccale
*Nino Musco: The sergeant
*Luigi Valanzano: The Barman
*Lino Murolo: The policeman
*Luciano Pigozzi: Angry man
*Franca Scagnetti: Woman near other angry man
*Vittorio Fanfoni: Pierre
*Stefano Oppedisano: Gaston
*Leonardo Benvenuti: Officier of GdF (uncredited)
*Nerina Montagnani: Sora Rosa (uncredited)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 