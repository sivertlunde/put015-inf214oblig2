The Evil Thereof (1913 film)
:For the 1916 film released by Paramount Pictures, see The Evil Thereof (1916 film).
{{infobox film
| name           = The Evil Thereof (1913 film)
| image          =
| imagesize      =
| caption        =
| director       = 
| producer       = Thomas Edison
| writer         = Ashton Crawford
| based on       = 
| starring       = May Abbey Bliss Milford
| cinematography = 
| editing        =
| studio          = Edison Studios
| distributor    = General Film Company
| released       =  
| runtime        = 10 minutes (1 reel)
| country        = United States
| language       = Silent film (English intertitles)
}}
The Evil Thereof (1913 in film|1913) is a short silent film, starring May Abbey and Bliss Milford, produced by the Edison Studios, and released by General Film Company. 

==Cast==
*Bliss Milford as Mary Randolph, shopgirl
*May Abbey as Kathryn Dolby, daughter of department store owner

==Plot==
Mary Randolph (Milford) has a hard time supporting herself and her little sister with the money she earns in a department store. Driven to desperation on rent day she summons all her courage and goes to the proprietor of the store and asks for a substantial wage. Her pleas meet with a stern refusal. Haunted by the stricken look in the girls eyes, Kathryn Dolby (Abbey), daughter of the proprietor, who has been listening, determines to investigate conditions by becoming one of the salesgirls. Unknown to her father she obtains such a position in his store.

On the afternoon following the unsuccessful interview with the proprietor, Mary, exhausted by the grind, faints as she is leaving the store. She is caught by a nice young man who, after accompanying her home, pays the insistent rent collector what is due him. Several days later, owing to the increasing rush of customers, Mary again faints from exhaustion, and after a stormy scene with her father, Kathryn takes the weakened woman to her squalid home.

There she learns of the discovery of a note from her brother, Jack, urging the woman to accept the money. Misunderstanding the note Kathryn takes it to her father, upon whom dawns the true state of things. Through parsimony he has made himself a virtual instigator of his sons evil intentions. Whereupon an immediate raise of wages is given all employees.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 