James Brown: Man to Man
James syndicated television Watts accompanied by Browns reflections on the situation of blacks in America. Writing in The New York Times, Albert Goldman described Man to Man as "fascinating in detail and overwhelming in total impact" and hailed it as a breakthrough for "offering such a long and relatively unobstructed look at a great black entertainer on his home ground." 

Man to Man was released on DVD by Shout! Factory in 2008 under the title James Brown Live at the Apollo 68 as part of the box set I Got the Feelin: James Brown in the 60s.

==Songs==
# "If I Ruled the World"
# "Thats Life (song)|Thats Life" Kansas City"
# Medley: "Its a Mans Mans Mans World"/"Lost Someone"/"Bewildered" Get It Together"
# "There Was a Time"
# "I Got the Feelin" Try Me"
# Medley: "Cold Sweat"/"Maybe the Last Time"/"I Got You (I Feel Good)"/"Please, Please, Please"/"I Cant Stand Myself (When You Touch Me)"/"Cold Sweat" (reprise)

==See also==
*  

==References==
 

 

 
 
 
 
 
 


 
 