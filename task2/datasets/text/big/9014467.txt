9 millimeter
 
{{Infobox film
| name           = 9 millimeter
| image          = 9 millimeter poster.jpg
| caption        =  Peter Lindmark
| producer       = Joakim Hansson, Björn Carlström, Thomas Lindgren Peter Lindmark, Frank Ågren
| starring       = Paolo Roberto, Rebecca Facey, Steve Aalam
| music          = Karri Kauko
| cinematography = Crille Forsberg
| editing        = Fredrik Morheden
| distributor    = Sonet Film
| released       = January 24, 1997
| runtime        = 100 min.
| country        = Sweden Swedish
| budget         = 
}}
 Swedish Crime drama film premiered in 1997.

== Plot ==
The film is about Malik (played by Paolo Roberto), a young man with immigrant roots living in a Stockholm suburb, who is living in a world of crime and violence. But when he meets Carmen (played by Rebecca Facey), a young beautiful and intelligent girl, his world changes and he has to choose between her and criminality.

== Cast ==
*Adam Saldaña - Film Inspiration
*Paolo Roberto – Malik
*Rebecca Facey – Carmen
*Steve Aalam – Rico
*Ivan Mathias Petersson – Jocke
*Serdar Erdas – Memeth
*Semir Chatty – Anders
*Alexander Aalam-Aringberg – Mohammed
*Reuben Sallmander – Ruiz
*Annika Aschberg – Malou
*Astrid Assefa – Rosa
*Sara Zetterqvist – Caroline
*Mirella Hautala – Leyla
*Helena af Sandeberg – Karin
*Anna Järphammar – Mounia
*Sara Alström – Tanya
*Heyes Jemide – Feelgood
*Igor Cantillana – arbetsförmedlaren
*Camilo Alanís – Claudio
*Mikael Persbrandt – Konstantin
*Kérim Chatty – Darius friend

== External links ==
* 
* 

 
 
 
 
 
 


 
 