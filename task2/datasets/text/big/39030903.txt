A Long Weekend in Pest and Buda
{{Infobox film
| name           = A Long Weekend in Pest and Buda
| image          = A Long Weekend in Pest and Buda.jpg
| caption        = Film poster
| director       = Károly Makk
| producer       = András Böhm
| writer         = Marc Vlessing Károly Makk
| starring       = Mari Töröcsik
| music          = 
| cinematography = Elemér Ragályi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

A Long Weekend in Pest and Buda ( ) is a 2003 Hungarian drama film directed by Károly Makk. It was entered into the 25th Moscow International Film Festival.   

==Cast==
* Mari Törőcsik as Török Mari
* Iván Darvas as Drégely Iván
* Eszter Nagy-Kálózy as Török Anna
* Dezső Garas as Pozsár Pál
* Eileen Atkins as Amanda
* Attila Kaszás as Robi
* Emese Vasvári as Nõvér
* Zoltán Seress as Orvos
* Zsuzsa Nyertes as Zsuzsika
* Géza Pártos as Professzor
* Zoltán Gera (actor)|Zoltán Gera as Szállodaportás
* Tamás Andor as Csapos
* Imre Csuja as Falusi ember
* János Derzsi as Taxisofõr

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 