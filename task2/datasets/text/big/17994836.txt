The Red Menace (film)
 
{{Infobox film
| name           = The Red Menace
| caption        =
| image	=	The Red Menace FilmPoster.jpeg
| director       = R. G. Springsteen
| producer       = Herbert J. Yates
| writer         = Albert DeMond Gerald Geraghty
| narrator       = Lloyd G. Davies
| starring       = Robert Rockwell Hannelore Axman Betty Lou Gerson Barbra Fuller
| music          = Nathan Scott
| cinematography = John MacBurnie
| editing        = Harry Keller
| distributor    = Republic Pictures
| released       = 1949
| runtime        = 81 mins.
| country        = United States
| language       = English
| budget         =
| gross          =
| awards         =
}}
The Red Menace (reissue title Underground Spy) is a 1949 anti-communist and an anti-Soviet film released by Republic Pictures.

An ex-GI named Bill Jones (Robert Rockwell) becomes involved with the Communist Party USA. While in training, Jones falls in love with one of his instructors. At first true followers of Communism, they realize their mistake when they witness Party leaders murder a member who questions the partys principles. When they try to leave the party, the two are marked for murder and hunted by the partys assassins. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 