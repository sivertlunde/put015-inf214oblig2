Firestorm (2013 film)
 
 
{{Infobox film
| name           = Firestorm
| image          = Firestorm2013.jpg
| caption        = Film poster
| film name = {{Film name| traditional    = 風暴
| simplified     = 风暴
| pinyin         = Fēng Bào
| jyutping       = Fung1 Bou6}}
| director       = Alan Yuen
| producer       = Andy Lau   William Kong
| writer         = Alan Yuen
| starring       = Andy Lau Yao Chen Gordon Lam Hu Jun Ray Lui
| music          = Peter Kam
| cinematography = Chan Chi-ying
| editing        = Kwong Chi-leung Ron Chan Focus Films Youku Tudou Inc
| distributor    = Edko Films
| released       =  
| runtime        = 109 minutes
| country        = Hong Kong
| language       = Cantonese   Mandarin
| budget         = US$20,000,000 
| gross          = US$56,382,533    
}}
 2013 Cinema Hong Kong action film 3D during post-production, making it the first 3D Hong Kong police action film.  Firestorm was chosen to be the opening film at Screen Singapore held on 4 December 2013 where Lau and co-star Gordon Lam walked the red carpet for the films premiere.  The film also opened the 56th Asia Pacific Film Festival on 13 December 2013 in Macau.  In addition, Firestorm also had its North American premier at the 57th San Francisco International Film Festival on 3 May 2014. 

==Plot==
A storm is heading to the city of Hong Kong, and with it comes another occurrence so destructive, it vows to bring down everything it touches.

A crew of seasoned criminals led by the notorious Cao Nam (Hu Jun), armed with high-powered weapons, pulls off another smooth and violent armored car heist in broad daylight in a crowded street. Whoever tries to get in their way, they will show no mercy. This puts the police force to shame and humiliation.

A hardboiled senior police inspector Lui Ming-chit (Andy Lau), hot on the trails of Nam and his tight crew, determines to put an end to this madness that causes the lives of innocent people. But he soon comes face with the cruel reality that the usual police tactics are too futile to send these armed thieves behind bars. Extreme crime requires extreme justice, even if it means crossing his moral line. To Shing-bong (Gordon Lam), an ex-con desperate to leave his criminal past behind, volunteers to be Lui’s informant in exchange for a fresh start with his girlfriend Yin Bing (Yao Chen). But little does he know what hellish situation he is getting himself into.

While Nam is putting together his next big score, the two colliding forces from both sides of the law, each at the top of their games, will soon face their archenemies. As Lui’s hunt heats up, their ultimate confrontation is about to take place on the streets.

As the storm passes through and the dust settles, whoever survives can hardly cope with its horrifying aftermath.

==Cast==
*Andy Lau as Senior Inspector Lui Ming-chit (呂明哲)
*Yao Chen as Law Yin-bing (羅燕冰)
*Gordon Lam as To Shing-bong (陶成邦)
*Hu Jun as Cao Nam (曹南)
*Ray Lui as Paco (啪哥)
*Philip Keung as Tong Keung (唐強)
*Kenny Wong as Sergeant Chiu Kin-kwok (趙建國)
*Oscar Leung as Kit (杰)
*Michael Tong as Jackal (傻豹)
*Vincent Sze as Sergeant Szeto Yat-ming (司徒一鳴)
*Terence Yin as Goofy (高飛)
*Sammy Hung as Dicky Michael Wong as Chief Superintendent Choi (蔡警司)
*Wong Cho-lam as Correctional Service Superintendent 
*Alex Tsui as Police negotiator Eddie Cheung as truck driver
*Ben Wong as S.D.U. Team Leader
*Bob Lam as Attorney
*Mandy Wong as Yin Bins colleague
*Lo Hoi-pang as Uncle Chi 
*Grace Wong as female victim
*Bonnie Sin as hostage
*Lavinia Smith as Nipple
*Cheung Kwok-keung as CIB Sergeant Chow (周Sir)
*Hayama Go as Daffy (大飛)
*SIR JBS@24HERBS as Au Yeung
*Phat@24HERBS as Phat
*William Chan

==Production== Government House Central and Sheung Wan areas. 

==Reception==
===Critical=== Time Out Hong Kong gave the film three stars out of five praising the action sequences, strong performances and visual effects that were never seen in Hong Kong cinema. 

On the other hand, James Marsh of Twitch Film gave the film a mixed review, praising it for having "a vibrant, kinetic aesthetic that attempts to keep its audience in a state of breathless anticipation throughout" and also criticizing "the over-reliance on computer-generated effects and the almost total absence of plot or characterisation make Firestorm an incredibly loud, yet hollow experience."  Clarence Tsui of The Hollywood Reporter also gave a mixed review praising Lams performance and action choreography by Chin Kar-lok, but criticizing the  action sequences as "bombastic" and the film "could not read as anything more than just an action thriller." 

===Box office===
Firestorm premiered in China on 12 December 2013 and grossed Renminbi|¥165,308,501 during its first three days and opening at No.1 during its debut weekend. {{cite web|url=http://boxofficemojo.com/intl/china/?yr=2013&wk=50&p=new|title=China Box Office
December 9–15, 2013}}  During its second week, the film grossed ¥100,045,163 and was the second highest grossing film of the week.  The film remained at top 10 for the rest of its theatrical run in China and eventually grossed ¥309,878,757. 

In Hong Kong, Firestorm premiered on 19 December and grossed Hong Kong dollar|HK$8,024,961 during its first three days and was also No. 1 during its opening weekend grossing a total HK$11,056,920 including its preview-screening gross.  During its second weekend, the remained at No. 1 spot and grossed HK$11,186,358 
The film grossed a total of HK$24,336,182 at the Hong Kong box office. 

As of April 2014, Firestorm grossed a total of US$56,382,533 (HK$437,172,118.47) worldwide, combining its box office gross from Hong Kong, China, Malaysia, Singapore, Thailand, Australia and New Zealand. 

==Awards and nominations==
*33rd Hong Kong Film Awards Best Action Choreography (Chin Kar-lok)
**Nominated: Best Editing (Kwong Chi Leung, Ron Chan)
**Nominated: Best Visual Effects (Yu Kwok Leung, Lai Man Chun, Ho Kwan Cheung, Lam Ka Lok)
**Nominated: Best New Director (Alan Yuen)

*12th Huading Awards
**Won: Top Ten Chinese Films

*14th Chinese Film Media Awards
**Won: Outstanding Actress (Yao Chen)
**Nominated: Outstanding Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 