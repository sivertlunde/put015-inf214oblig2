Superman/Batman: Public Enemies
{{Infobox film
| name        = Superman/Batman: Public Enemies
| image       = supbatpub.jpg
| caption     = DVD cover art
| alt         = 
| director    = Sam Liu
| producer    = Bruce Timm Alan Burnett Michael Goguen Bobbie Page Sam Register Benjamin Melniker Michael Uslan
| writer      = Stan Berkowitz
| starring    = Kevin Conroy Tim Daly Clancy Brown Xander Berkeley Corey Burton Ricardo Chavira Allison Mack John C. McGinley C. C. H. Pounder
| music       = Christopher Drake
| studio      = Warner Bros. Animation Warner Premiere DC Comics
| distributor = Warner Home Video
| released    =  
| runtime     = 67 minutes
| country     =  
| language    = English
| budget      =
| gross       = $10,328,363    
}} animated superhero DCAU reprised their roles, although it is not a DCAU production and is said not to be connected with that universe beyond sharing of voice actors.

==Plot== economic depression. Under his leadership, the economy begins to thrive, and he assembles a force of government-employed superheroes consisting of Captain Atom, Katana (comics)|Katana, Black Lightning, Power Girl, Starfire (comics)|Starfire, and Major Force. Meanwhile, Superman and Batman maintain their distrust toward Luthor.

The United States government discovers that a massive Kryptonite meteor is hurtling toward Earth. Instead of asking superheroes for aid and wanting to take credit for himself, Luthor decides to destroy it with nuclear missiles. Luthor arranges a meeting with Superman in Gotham City under the pretense of forming a pact. This results in a battle between the hired Metallo and Superman and Batman. Following the heroes escape, an unknown assailant kills Metallo.

On national television later that night, Luthor pins Metallos murder on Superman, using footage of their battle to implicate him. Luthor claims that the radiation being emitted by the meteor can affect Supermans judgment, and he places a one-billion-dollar bounty on the Man of Steel.
 Solomon Grundy, and Shrike (comics)|Shrike. After some effort, most of the villains are defeated. Captain Atom, who has arrived with Luthor’s superhero team to arrest Superman, defeats the remaining villains.  All but Power Girl, whose loyalties are divided, attempt to capture the heroic duo. Superman creates a twister using his superspeed, and the two heroes escape with Power Girl.

In Metropolis (comics)|Metropolis, Power Girl admits that she feels unnerved by Luthor and does not believe Superman killed Metallo. Luthors superheroes catch up and the fight begins anew, this time with Power Girl aiding Superman and Batman. The Dark Knight realizes that Major Force killed Metallo under Luthors orders and goads him into admitting it in front of everyone. In anger, Power Girl punches him in the stomach with so much force that it ruptures his containment suit. Captain Atom, ashamed at his complicity in Luthors misdeeds, absorbs the energy, disintegrating Major Force and injuring himself in the process.
 Captain Marvel and Hawkman (Carter Hall)|Hawkman, eventually emerging victorious with Power Girls (off-screen) aid. However, Luthor refuses to relinquish the data, going so far as to erase it from the lab computers. Waller gives them a copy, being disgusted with Luthors plans. Batman and Superman fly off to Tokyo to deliver the data on the meteor to the Japanese Toyman, who has already built a giant rocket-propelled spacecraft, intending to use it as a large missile to stop the meteor. Waller and the military then attempt to arrest Luthor, but he injects himself with more kryptonite steroid  and dons a power suit. After escaping Waller and the military, Luthor follows Superman and Batman overseas.
 composite version of Superman and Batman. With the data, Toyman is able to calculate the necessary reinforcements needed for his own rocket so it will not explode before impact. Unfortunately, the arriving Luthor neutralizes Power Girl, Superman and Batman, and then disables the rockets remote guidance systems so that it will not take off by itself. Having no other choice, Batman decides to fly the rocket himself, despite Supermans protests. Though initially faring poorly against Luthor and his kryptonite power suit, the rage over losing his best friend allows Superman to gain the upper hand, and after an aerial chase leading them all the way back to Metropolis, he defeats him in the streets there. Batman succeeds in destroying the meteor, and Superman finds him alive and well in an escape pod.

With the truth of Metallos death now public knowledge, Superman is cleared of the murder charge and Luthor is arrested and taken away to face trial and impeachment for his crimes. Batman then returns to Gotham while the Daily Planets star journalist, Lois Lane, arrives and happily embraces the Man of Steel.

==Cast==
* Clancy Brown as Lex Luthor
* Kevin Conroy as Batman|Batman/Bruce Wayne
* Tim Daly as Superman 
* Xander Berkeley as Captain Atom Captain Marvel Solomon Grundy (uncredited)
* Ricardo Chavira as Major Force
* Allison Mack as Power Girl
* John C. McGinley as Metallo
* C. C. H. Pounder|C.C.H. Pounder as Amanda Waller
* LeVar Burton as Black Lightning Japanese Toyman / Hiro Okamura Mark Jonathan Davis as Newscaster
* Brian George as Gorilla Grodd Starfire , Killer Frost (uncredited) Billy Batson (uncredited)
* Alan Oppenheimer as Alfred Pennyworth
* Andrea Romano as Giganta, Computer (uncredited)
* Bruce Timm as Mongul Michael Gough Hawkman (uncredited), Captain Cold (uncredited) Jonathan Adams as General (uncredited)
 Justice League and Justice League Unlimited. 

In addition to Jennifer Hales uncredited reprisal of Killer Frost, actors that reprise their roles from the DC Animated Universe include Clancy Brown, Kevin Conroy, Tim Daly, and CCH Pounder, who reprise their roles as Lex Luthor, Batman, Superman, and Amanda Waller, respectively.

==Soundtrack==
{{Infobox album
| Name = Superman/Batman: Public Enemies (Soundtrack from the DC Universe Animated Original Movie)
| Type = Film score
| Artist = Christopher Drake Digital download)
| Cover = Superman Batman Public Enemies (soundtrack).jpg
| Released = September 29, 2009
| Length = 56:57
| Label = New Line Records
| Reviews =
}}
{{Track listing
| collapsed       = no
| headline        = Superman/Batman: Public Enemies (Soundtrack from the DC Universe Animated Original Movie)
| extra_column    = 
| total_length    = 56:57

| all_writing     = 
| all_lyrics      = 
| all_music       =

| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1          = Markets Crash
| note1           = 
| writer1         = 
| lyrics1         = 
| music1          = 
| extra1          = 
| length1         = 1:32

| title2          = Main Title
| note2           =  
| writer2         = 
| lyrics2         = 
| music2          = 
| extra2          = 
| length2         = 2:27

| title3          = Freeway Chase
| note3           =  
| writer3         = 
| lyrics3         = 
| music3          = 
| extra3          = 
| length3         = 1:15

| title4          = Admit Something
| note4           = 
| writer4         = 
| lyrics4         = 
| music4          = 
| extra4          = 
| length4         = 1:40

| title5          = Metallo
| note5           = 
| writer5         = 
| lyrics5         = 
| music5          = 
| extra5          = 
| length5         = 4:51

| title6          = High Voltage
| note6           = 
| writer6         = 
| lyrics6         = 
| music6          = 
| extra6          = 
| length6         = 0:51

| title7          = Framed
| note7           = 
| writer7         = 
| lyrics7         = 
| music7          = 
| extra7          = 
| length7         = 1:27

| title8          = Luthor talks to Power Girl
| note8           = 
| writer8         = 
| lyrics8         = 
| music8          = 
| extra8          = 
| length8         = 0:55

| title9          = S.T.A.R. Labs/ Banshee & The Cold Crew / Mongul, Grundy, Grodd
| note9           = 
| writer9         = 
| lyrics9         = 
| music9          = 
| extra9          = 
| length9         = 5:57

| title10         = Bounty Hunters
| note10          =  
| writer10        = 
| lyrics10        = 
| music10         = 
| extra10         = 
| length10        = 1:58

| title11         = No Surrender
| note11          =   
| writer11        = 
| lyrics11        = 
| music11         = 
| extra11         = 
| length11        = 2:02

| title12         = Tornado Recovery
| note12          = 
| writer12        = 
| lyrics12        = 
| music12         = 
| extra12         = 
| length12        = 0:16

| title13         = Trust Your Instints
| note13          = 
| writer13        = 
| lyrics13        = 
| music13         = 
| extra13         = 
| length13        = 4:05

| title14         = The Corps Fights Sinestro  
| note14          = 
| writer14        = 
| lyrics14        = 
| music14         = 
| extra14         = 
| length14        = 2:48

| title15         = Missile Launch
| note15          = 
| writer15        = 
| lyrics15        = 
| music15         = 
| extra15         = 
| length15        = 1:45

| title16         = Luthor’s Fix
| note16          = 
| writer16        = 
| lyrics16        = 
| music16         = 
| extra16         = 
| length16        = 0:23

| title17         = Shazam!
| note17          = 
| writer17        = 
| lyrics17        = 
| music17         = 
| extra17         = 
| length17        = 3:05

| title18         = Luthor Shoots Up
| note18          = 
| writer18        = 
| lyrics18        = 
| music18         = 
| extra18         = 
| length18        = 1:17

| title19         = Heroes In Disguise 
| note19          = 
| writer19        = 
| lyrics19        = 
| music19         = 
| extra19         = 
| length19        = 3:11

| title20        = Toyman
| note20         = 
| writer20       = 
| lyrics20       = 
| music20        = 
| extra20        = 
| length20       = 2:13

| title21        = Blast Off
| note21         =  
| writer21       = 
| lyrics21       = 
| music21        = 
| extra21        = 
| length21       = 4:02

| title22        = Ultimate Sacrifice
| note22         = 
| writer22       = 
| lyrics22       = 
| music22        = 
| extra22        = 
| length22       = 3:06

| title23        = A Hero’s Return
| note23         =  
| writer23       = 
| lyrics23       = 
| music23        = 
| extra23        = 
| length23       = 2:56

| title24        = End Credits
| note24         = 
| writer24       = 
| lyrics24       = 
| music24        = 
| extra24        = 
| length24       = 4:00
}}

==Reception==
 
 ,   ,  and  .  The review score matched that of  .

==Home media==
 
Superman/Batman: Public Enemies was released on standard  ,  , and  , DC Comics 2009 crossover event  , trailers of Green Lantern: First Flight,  , digital copy download, and two episodes of   picked by Bruce Timm. The Blu-ray edition has all the features of the double disc standard definition release including three additional Justice League episodes selected by Timm.

 
At the DVD sales chart, Superman/Batman: Public Enemies opened at #5, selling 197,626 in the first week for revenue of $3,222,460. As of today, 527,482 units have been sold translating to $7,911,279 in revenue (This does not include rentals/Blu-ray sales). This makes Public Enemies the second highest selling DVD only behind   and the third most profitable of the ten movies in the DC Universe Animated Original Movies line-up. 

 
The good performance of the Superman/Batman: Public Enemies release has led Warner Premiere and DC Universe to release a sequel,  , based on the Superman/Batman comic storyline "The Supergirl from Krypton". It was released on September 28, 2010. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 