Rage (2014 film)
{{Infobox film
| name           = Rage
| image          = Rage2014film.jpg
| alt            = 
| caption        = 
| director       = Paco Cabezas
| producer       = Michael Mendelsohn   Richard Rionda Del Castro
| writer         = Jim Agnew and Sean Keller
| starring       = Nicolas Cage
| music          = Laurent Eyquem
| cinematography = Andrzej Sekuła
| editing        = Robert A. Ferretti
| studio         = Hannibal Pictures
| distributor    = Image Entertainment
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $25 million
| gross          = 
}}
 action crime Rachel Nichols, Peter Stormare, Danny Glover, Max Ryan, Judd Lormand and Pasha D. Lychnikoff.

==Plot==
Paul Maguire (Nicolas Cage) and his buddies Kane (Max Ryan) and Danny (Michael McGrady) live a life of crime. One night, they ambush a Russian mobster, intercepting him when he’s on his way to drop off money to his boss. The take is much larger than they imagined; along with a Russian gun called a TT pistol|Tokarev, they get away with a briefcase full of cash. It’s enough for Paul to leave the life of crime and go legit, but their crime sets off a bloody protracted war between their Irish crime family and the Russian mob. Paul instructs his friends to hide the money until things calm down between their outfit and the Russians. In spite of several casualties on both sides, their plan works well; five years after stealing the money, Paul, Kane and Danny meet to split up the take.

Over the next twenty  years, Paul uses his share to slowly build a legitimate construction empire in Mobile, Alabama. He marries a beautiful, ambitious woman named Vanessa (Rachel Nichols) and is a doting father to his teenage daughter Caitlin (Aubrey Peeples). His former mob boss, O’Connell (Peter Stormare) has allowed him to go legit and stays on speaking terms, but he remains friends his buddies Kane and Danny, who haven’t had his luck when it comes to reforming. One night Paul and Vanessa are heading to a charity dinner, leaving Caitlin at home to hang out with her friends Evan (Jack Falahee) and Mike (Max Fowler). At the dinner, a detective (Danny Glover) named St. John approaches Paul, who is used to being hassled for his former life of crime. But the Detective isnt hassling him; something has happened to Caitlin.

Evan and Mike tell the police and Paul that they were watching TV with Caitlin, when several masked men barged in and took her. They fought – but the men overtook Mike and Evan. Convinced the kidnapping is long-overdue retaliation for the crimes of his youth – specifically his run-in with Ivan, Paul ignores the police and mounts his own investigation.

Pauls patience with the police ends completely when Caitlins body is found in a storm drain. After her funeral, the wheelchair-bound OConnell strongly suggests that Paul allow the police to handle the investigation, reasoning that Paul was allowed to walk out of the mob, and therefore he should stay away from mob methods.

Paul and his buddies shake down and rough up anyone they can find who might be connected to Chernov (Pasha D. Lychnikoff), the legendary mob boss whose money they stole seventeen years earlier and whom they think was involved in Caitlins murder. OConnell pleads with Paul to stop his rampage, but he refuses. Vanessa, who initially backed her husbands search, now fears hes becoming the killer he once was. But there exists another possibility: perhaps he has always been a violent man and can never truly leave that life behind.

After executing some of Chernovs men and wrecking their businesses, Pauls buddies are targeted by Chernov. When Kane is captured and tortured, he tries to get Chernov to admit to killing Caitlin as revenge for the heist decades ago.  Chernov is perplexed, but also angry that Paul did the long-ago crime, as the Russian mobster they killed turns out to have been Chernovs brother. Meanwhile, Paul thinks Danny had to have talked because the friend is directly working for OConnell once again, and Danny uses drugs and parties and may have talked and not remembered. He confronts Danny, accuses him of ratting, and knifes him in a moment of wrath. 

Chernov calls a meeting with OConnell, and tells OConnell that the long-ago heist, which has started the gang war between the two, was done by Paul and his buddies, and demands OConnell exact discipline. OConnell refuses to listen to Chernov on general principles, and the resulting shootout leaves Chernov as the only survivor.

In a flashback it is learned that Mike killed Caitlin using the Tokarev, which was stolen from the Russian mobster by Paul during the heist. Caitlin, pressured by Evan and Mike to show something from Pauls violent past, shows them several pistols of Pauls. Mike is pointing the Tokarev with an outstretched arm when Caitlin comes up behind him and startles him, and as he spins around to her he accidentally pulls the trigger.

Paul gets the story from Evan and, after an inner struggle, lets Mike live and walks away. He goes home, sits down, and calls his wife. He apologizes and says things will be over soon, as Chernov and some of his surviving men enter his house. He then tells her the story about the first time he killed, when someone gave him a knife in a barfight. He admits he has nightmares about it, where his older self tries to stop the violence and instead can only watch helplessly. He hangs up just as an execution squad walks in.

== Cast ==
* Nicolas Cage : Paul Maguire Rachel Nichols : Vanessa
* Peter Stormare : OConnell
* Danny Glover : Detective St. John
* Max Ryan : Kane
* Michael McGrady : Danny Doherty
* Judd Lormand : Mr. White
* Max Fowler : Mike
* Pasha D. Lychnikoff : Chernov
* Ron Goleman : Detective Hanson
* Aubrey Peeples : Caitlin Maguire
* Elena Sanchez : Lisa
* Weston Cage : young Paul Maguire
* Jon Dannelley : young Kane
* Dawn Hamil : Amber
* Jack Falahee : Evan
* Kelly Tippens : Eleanor
* Kevin Lavell Young : Oliver
* Kara Riann Brown : a druggie
* Patrice Cols : Anton
* Steven Vickers Jr. : Jack
* Sarah Ann Schultz : Miss Russell
* Paul Sampson : Sasha
* Dretta Love : a stripper
*Jessica Fiesta George : a stripper
* Jaanika Kaarpalu : a stripper
* Regis Harrington : stunt double (guy on black motorcycle)

== Production ==
Filming began in June 2013 in Mobile, Alabama|Mobile, Alabama. 

== Reception ==
Rage received negative reviews from critics. On Rotten Tomatoes, the film holds a rating of 14%, based on 37 reviews, with an average rating of 3/10. The sites consensus reads, "Depressingly dull and all-around poorly made, Rage is the rare Nicolas Cage action thriller lacking enough energy to reach "so bad its good" territory."  On Metacritic, the film has a score of 28 out of 100, based on 17 critics, indicating "generally unfavorable reviews". 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 