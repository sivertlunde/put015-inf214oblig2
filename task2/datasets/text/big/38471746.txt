Shoe Palace Pinkus
{{Infobox film
| name = Shoe Palace Pinkus
| image =
| image_size =
| caption =
| director = Ernst Lubitsch
| producer =
| writer = Hanns Kräly   Erich Schönfelder
| narrator =
| starring = Ernst Lubitsch   Else Kentner   Guido Herzfeld   Ossi Oswalda
| music =
| editing =
| cinematography =
| studio = PAGU Union Film
| released = March 1916
| runtime = 60 minutes
| country = Germany Silent  German intertitles
| budget =
| gross =
| preceded_by =
| followed_by =
}} silent comedy Sally series of films featuring Lubitsch as a sharp young Berliner of Jewish heritage. After leaving school, a self-confident young man goes to work in a shoe shop. Before long he has become a shoe tycoon. 

==Cast==
* Ernst Lubitsch as Sally Pinkus
* Else Kentner as Melitta Herve
* Guido Herzfeld as Mr. Meiersohn
* Ossi Oswalda as Apprentice
* Hanns Kräly as Teacher
* Erich Schönfelder as Schuhmacher
* Fritz Rasp

==References==
 

==Bibliography==
* Eyman, Scott. Ernst Lubitsch: Laughter in Paradise. Johns Hopkins University Press, 2000.
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 