Galileo (1974 film)
{{Infobox film
| name           = Galileo
| image          = GalileoDVD.jpg
| image_size     =
| caption        = DVD box art
| director       = Joseph Losey
| producer       = Ben Baker Ely A. Landau
| writer         = Joseph Losey Barbara Bray
| narrator       = Topol Edward Edward Fox John Gielgud Tom Conti Margaret Leighton
| music          = Hanns Eisler
| release        = Michael Reed
| editing        = Reginald Beck
| released       = 1974 (UK)
| runtime        = 145 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| distributor    = 
}}

Galileo is a 1974 film version of the Bertolt Brecht play The Life of Galileo. The film was produced and released as part of the American Film Theatre, which adapted theatrical works for a subscription-driven cinema series.

==Plot== Venetian republic. Part of his work involves the use of a telescope, a relatively new scientific instrument brought from the Netherlands. Using the telescope, Galileo seeks to test the theories put forth by Nicolaus Copernicus that place the sun – and not the Earth – at the center of universe. As his research progress, Galileo accepts a more prestigious academic position in Florence, Italy. But his new position does not come with the government protection he enjoyed in Venice, and his friends in the higher echelons of the Roman Catholic Church refuse to come to his aid when he is summoned before the Inquisition.   

==Production==
Joseph Losey, the director of the film, also directed the first U.S. theatrical production of Galileo in 1947.    In his cinematic adaptation, Losey maintained several theatrical concepts that appeared in the theatrical version, including the use of a chorus of young boys who advance parts of the plot and the staging of Galileo’s recantation against a shadow-filled white screen. Losey’s opening shot, an overhead view of the film sound stage, also calls attention to the theatricality of the production.  

==Release== Edward Fox, Patrick Magee, John McEnery.”  

The film was shown at the 1975 Cannes Film Festival, but wasnt entered into the main competition.   

==Home video==
A region 1 DVD of the film was released in 2003. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 