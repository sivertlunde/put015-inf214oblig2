The Stoker (1932 film)
{{Infobox film
| name           = The Stoker
| image_size     =
| image	=	The Stoker FilmPoster.jpeg
| caption        =
| director       = Chester M. Franklin
| producer       = M.H. Hoffman Jr. (associate producer) M.H. Hoffman (producer)
| writer         = F. Hugh Herbert (writer) Peter B. Kyne (story)
| narrator       =
| starring       =
| music          = Tom Galligan Harry Neumann
| editing        = Mildred Johnston
| distributor    =
| released       = 15 June 1932
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

The Stoker is a 1932 American film directed by Chester M. Franklin.

== Plot summary ==
 
A man whose wife has deserted him winds up saving a beautiful girl from the clutches of a murderous bandit on a Nicaraguan coffee plantation. 

== Cast ==
*Monte Blue as Dick Martin
*Dorothy Burgess as Margarita Valdez
*Noah Beery as Santini
*Natalie Moorhead as Vera Martin Richard Tucker as Alan Ballard
*Clarence Geldart as Senor Valdez Charles Stevens as Ernesto
*Harry J. Vejar as Jailer
*Chris-Pin Martin as Chief of Police

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 