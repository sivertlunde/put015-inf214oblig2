The Burning Court (film)
  name = The burning court
| image = La_chambre_ardente_poster.jpg
| director = Julien Duvivier
| starring = Jean-Claude Brialy    Édith Scob    Nadja Tiller   Claude Rich
| music = Georges Auric
| runime = 110 minutes
| released   = 1962
}}
The burning court ( ) is a French-Italian-German film directed by Julien Duvivier, released in 1962. The script was written by Charles Spaak and Duvivier, from the novel by John Dickson Carr.

==Plot==
Historian Michel Boissard (Walter Giller), is invited with his wife Marie (Édith Scob), a descendant of the Marchioness of Brinvilliers, the notorious poisoner, to the château of Mathias Desgrez (Frédéric Duvallès). Mathias Desgrez is a descendant of the  last lover of the Marchioness,  who denounced her.  Mathias is  adept in the occult arts, which he practises with his friend Dr. Hermann (Antoine Balpêtré). To the château come Marc Desgrez (Jean-Claude Brialy), and Stephane Desgrez (Claude Rich), nephews of Mathias, who are waiting impatiently for their inheritance. The wife of Marc, Lucy (Perrette Pradier) also has hopes. And Marcs mistress Myra (Nadja Tiller) who is Mathias personal nurse, would like to accelerate Marcs uncles death. Mathias himself is fascinated by Marie Boissard.  One night, a little time later, Mathias dies having received a visit from a mysterious woman bringing him medications, and is seen by a servant. Mathias is buried and then his body disappears just when the analysis proves that he has undoubtedly been poisoned.  By whom? Lucy - eager to inherit? Myra - to please her lover? Marie - to avenge her betrayed ancestor, by the ancestor of Mathias? Inspector Krauss (Claude Piéplu) enquires into the matter.

==External links==
*  

 

 
 
 
 
 
 
 
 

 