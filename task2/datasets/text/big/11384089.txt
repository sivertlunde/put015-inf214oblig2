Private Valentine: Blonde & Dangerous
{{Infobox film
| name           = Private Valentine: Blonde & Dangerous
| image          = Normal 01.jpg
| director       = Steve Miner
| producer       = Bill Gerber Denise Di Novi
| writer         = April Blair Kelly Bowe
| starring       = Jessica Simpson Vivica A. Fox Olesya Rulin Steve Guttenberg Aimee Garcia
| studio         = Emmett/Furla/Oasis Films|Emmett/Furla Films
| distributor    = Sony Pictures/Screen Gems
| released       =  
| runtime        = 98 minutes
| music          = Dennis Smith
| cinematography = Patrick Cady
| editing        = Nathan Easterling
| country        = United States
| language       = English
| budget         = 
| gross          = $103,971 
}}
Private Valentine: Blonde & Dangerous or Major Movie Star is a 2008 comedy film starring Jessica Simpson. Simpson plays the title role of Megan Valentine in the comedy, about a down-on-her-luck actress who enlists in the United States Army.   

==Plot==
When fluffy bubble-gum movie star Megan Valentine (Jessica Simpson) suddenly finds herself broke and humiliated in the public eye, she wanders from the wreckage of a car accident and witlessly enlists in the U.S. Army, hoping in vain that it will change her life. The spoiled actress immediately finds herself at odds with her tough drill sergeants and the harsh discipline of Army life, leading to many humorous situations. In the end, however, Valentine wins the respect of her fellow trainees, and they all graduate together. She also gets her house back and the money that she lost. In the end she changes and becomes a better person.

==Cast==
* Jessica Simpson as Private Megan Valentine
* Vivica A. Fox as 1SG Louisa Morley
* Olesya Rulin as Petrovich
* Jill Marie Jones as Johnson
* Keiko Agena as Private Hailey Hamamori
* Steve Guttenberg as Sidney Green
* Aimee Garcia as Vicky Castillo
* Ryan Sypek as SSG Mills Evans
* Cheri Oteri as PVT Jeter
* Bryce Johnson as Derek OGrady
* Michael Hitchcock as Nigel
* Michael Vlcej as SPC Hooper (not credited)
* Travis Schuldt as SFC Harrison (as Trevor Schuldt)

==Production notes== Fort Jackson, which is a real Army training base. The movie was filmed, however, at Camp Minden, a Louisiana National Guard Camp located in Minden, Louisiana|Minden, Louisiana, United States.

==Releases==
The film was released in Russia on 9 October 2008 and in Bulgaria on 7 November 2008. IMDB (2010). Major Movie Star. Retrieved from http://www.imdb.com/title/tt1034320/. 

===DVD release===
The film was released on DVD on 3 February 2009 in the United States under the new name Private Valentine: Blonde & Dangerous. Us Magazine (2008-10-31). Jessica Simpsons Major Movie Star Goes Straight to DVD in America. Us Magazine, 31 October 2008. Retrieved from http://omg.yahoo.com/news/jessica-simpsons-major-movie-star-goes-straight-to-dvd-in-america/14847?nc/.  The film was released in the United Kingdom under the old name of Major Movie Star on 18 May 2009. 

==Late Night with Conan OBrien== English is bootleg copy before having recurring guest star James Lipton make a passionate plea to Simpson to reconsider her decision to withhold the film, saying the "bad defines the good" and the apparent "85 minute stinkbomb" of Major Movie Star would only help to highlight how good Schindlers List really was. 

==References==
 

==External links==
*  
*  
*  
* http://www.sonypictures.com/homevideo/catalog/catalogDetail_DVD043396274310.html

 

 
 
 
 
 
 
 
 
 