The Game of Liberty (film)
{{Infobox film
| name           = The Game of Liberty
| image          =
| caption        =
| director       = George Loane Tucker 
| producer       = 
| writer         = E. Phillips Oppenheim Douglas Munro   Laura Cowie 
| cinematography = 
| editing        = 
| studio         = London Films
| distributor    = Jury Films
| released       = January 1916 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent comedy comedy crime Douglas Munro novel of the same title by E. Phillips Oppenheim. 

==Cast==
* Gerald Ames as Hon. Paul Walmsley   Douglas Munro as Joseph H. Parker 
* Laura Cowie as Eva Parker  
* Bert Wynne as Inspector Cullen  
* Sydney Fairbrother as Mrs. Bundercombe 
* Hugh Croise as Bert Johnson

==References==
 

==Bibliography==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 