Jab Pyar Kisi Se Hota Hai
 
{{Infobox film
| name           = Jab Pyar Kisi Se Hota Hai
| image          = Jab_Pyar_Kisi_Se_Hota_Hai_Poster_1961.jpg
| caption        = Poster of Jab Pyar Kisi Se Hota Hai
| director       = Nasir Hussain
| producer       = Nasir Hussain
| writer         = Nasir Hussain
| narrator       =  Pran
| music          = Shankar Jaikishan
| cinematography = Dilip Gupta
| editing        = Babu Rao
| distributor    = 
| released       =  1961
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          =   1,80,00,000
}}
 Pran played the villain in the film and Rajindernath starred in a supporting role.

Hussain would rework some of the elements from this film into a later film, Teesri Manzil (1966).  This was also the only time that he used Shankar Jaikishan as music directors and Dev Anand in a leading role. The playback singers were Mohammed Rafi and Lata Mangeshkar.

==Plot==
Nisha lives a very wealthy lifestyle in Neelgaon, India, with her businessman dad, Sardar Roop Singh. She is now of marriageable age, and her dad wants her to marry his friends son, Sohan, but Nisha dislikes him. While traveling to Darjeeling with a dance troupe, she meets with her dads business associates son, Popat Lal, and after a few misadventures, both fall in love with each other. She takes him to meet her dad where he can also finalize his business transaction, but when Roop comes inside, he finds that Popat has disappeared, and in his place is another man claiming to be the real Popat. Nishas heart is broken and she starts to hate Popat. She does meet with Popat, who tells her that his real name is Sunder and both had been promised to each other by their respective parents, but Roop had subsequently changed his mind. Nisha believes him and agrees to marry him without her fathers blessings. When the marriage is to take place, a man named Khanna comes over and tells Nisha that Sunder is already married to woman named Shanti, who he subsequently killed, and had been the primary accused in this case by the police. Watch what impact this news has on the marriage, and see what excuses Sunder now comes up with.

==Cast==
* Dev Anand... Sunder / Popat Lal / Monto
* Asha Parekh ... Nisha R. Singh
* Sulochana Latkar ... Malti
* Mubarak ... Sardar Roop Singh
* Raj Mehra ... Khanna
* Wasti ... Shanti
* Rajendra Nath ... Popat Lal / Charlie
* Tahir Hussain (as Tahir Husain) Dulari  ... Roop Singhs sister
* Bishan Khanna
* Ram Avtar Pran ... Sohan Mofat Lal

==Soundtrack==
The music was by Shankar Jaikishan, and interestingly, the whole soundtrack was sung only by Mohammed Rafi and Lata Mangeshkar. Many of the songs went on to become classics and are still remembered even now, particularly, "Jiya O Jiya", "Teri Zulphon Se Judai" sang by Mohammad Rafi were big hits and "Sau Saal Pehle" by Mohammad Rafi and Lata Mangeshkar.
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s)!! Lyrics!! Picturized on
|-
| 01.   "Jiya O Jiya"
| Mohammed Rafi
| Hasrat Jaipuri
| Picturized on Dev Anand and Asha Parekh
|-
| 02.   "Yeh Aankhen Uff Yun Maa"
| Mohammed Rafi and Lata Mangeshkar
| Hasrat Jaipuri
| Picturized on Dev Anand and Asha Parekh
|-
| 03.   "Sau Saal Pehle"
| Mohammed Rafi and Lata Mangeshkar
| Hasrat Jaipuri Picturized on Dev Anand and Asha Parekh
|-
| 04.   "Tum Jaise Bigade Babu Se"
| Lata Mangeshkar
| Hasrat Jaipuri
| Picturized on Asha Parekh
|-
| 05.   "Teri Zulfon Se Judai"
| Mohammed Rafi
| Hasrat Jaipuri
| Picturized on Dev Anand and Asha Parekh
|-
| 06.   "Bin Dekhe Aur Bin Pehchane"
| Mohammad Rafi
| Shailendra
| Picturized on Dev Ananad
|-
| 07.   "Nazar Mere Dil Pe"
| Lata Mangeshkar
| Shailendra
| Picturized on Asha Parekh
|-
| 08.  "Jiya O Jiya"
| Lata Mangeshkar
| Hasrat Jaipuri
| Picturized on Dev Anand and Asha Parekh
|}

==References==
 

== External links ==
*  

 
 
 
 
 
 