In the Name of Love: A Texas Tragedy
{{Infobox film
| name           = In the Name of Love: A Texas Tragedy
| image          = In the Name of Love a Texas Tragedy.jpg
| alt            = 
| caption        = 
| director       = Bill DElia
| producer       = 
| writer         = Danielle Hill  Michael Hayden Richard Crenna Bonnie Bartlett
| music          = 
| cinematography = James R. Bagdonas
| editing        = 
| studio         = 
| distributor    = Fox Broadcasting Company
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
In the Name of Love: A Texas Tragedy is a 1995 American film loosely based on the tragic story of Mississippis "Delta Prince," Ralph Hand III, and his ex-wife Olivia Browning.

==Plot== Michael Hayden), the grandson of a rich farmer who has been crippled in an automobile accident.  Against the wishes of his grandfather (Richard Crenna), who sees the girl as a gold-digger, young Luke and Laurette get married.  The senior Mr. Constable, however, goes out of his way to make his new granddaughter-in-law feel unwelcome, which quickly causes a rift in her marriage.  Following a brief breakup, Laurette encourages Luke to break away from his grandfather and relinquish his rights to the family fortune.  Struggling in poverty, Luke contemplates murdering Mr. Constable, but he cant bring himself to do it. The couple divorce, reconcile, then separate again. Laurette finds Luke drunk and brandishing a gun; she goads him into shooting her - he kills her with one shot. He takes his wifes dead body into the woods and burns it. He is arrested and imprisoned.

==Production==
 
 Pilot Point, Sanger, Texas|Sanger, Bolivar, Texas|Bolivar, Lewisville, Texas|Lewisville, and Dallas, Texas.   The film was shot in 19 days,    during which time the visiting cast commuted around the area from a hotel in Denton. 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 