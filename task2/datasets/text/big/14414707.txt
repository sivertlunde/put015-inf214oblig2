The Red Awn
{{Infobox film
| name           = The Red Awn
| image          = Red Awn.jpg
| caption        = 
| film name = {{Film name| jianti         = 红色康拜因
| fanti          = 紅色康拜因
| pinyin         = Hóngsè kāngbàiyīn}}
| director       = Cai Shangjun
| producer       = Li Xudong
| writer         = Cai Shangjun Gu Xiaobai Feng Rui Wang Hong
| music          = Huang Zhenyu Dong Wei Li Chengyu Chen Hao
| editing        = Zhou Ying
| distributor    = Xiudong Hao Ye/Wan Ji
| released       =  
| runtime        = 105 minutes
| country        = China
| language       = Mandarin
}} Chinese film directed Cai Shangjun. The film premiered at the 2007 Pusan International Film Festival where it won the FIPRESCI Prize.  The film tells the story of a father and son in Chinas interior Gansu province. It also won the Golden Alexander, the top award at the International Thessaloniki Film Festival. 

== Cast ==
*Yao Anlian as Song, a man who returns to his hometown after five years only to discover that his family has declared him dead.
*Lu Yulai as Yongtao, Songs 17-year old son.
*Shi Junhui as Yongshan, Songs friend and the owner of the titular (in the original Chinese) red combine harvester.
*Huang Lu

== Reception ==
The Red Awn found success on the international film festival circuit early on. Besides winning the FIPRESCI Prize at Pusan, The Red Awn also was an official selection of the 2007 International Thessaloniki Film Festival where it premiered on November 24, 2007.  It went on to win the Golden Alexander, the Festivals top prize along with a €37,000 monetary award. 

On November 11, 2008, the film won the Jury Grand Prize in the 2008 Asia Pacific Screen Awards.   

===Awards and nominations===
* 2007 International Thessaloniki Film Festival
** Golden Alexander
* 2007 Pusan International Film Festival
** FIPRESCI Prize
* 2008 Asia Pacific Screen Awards The Prisoner)

== Notes ==
 

== External links ==
*  
*  
*   at the Chinese Movie Database
*  , a review from FIPRESCI

 
 
 
 
 
 
 

 