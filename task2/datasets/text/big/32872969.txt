Diggers in Blighty
 
 
{{Infobox film
| name           = Diggers in Blighty
| image          =
| image_size     =
| caption        =
| director       = Pat Hanna Raymond Longford (associate director) "Raymond Longford", Cinema Papers, January 1974 p51 
| producer       = Pat Hanna
| writer         = Pat Hanna Wilfred King Edmund Warrington Bert Reid
| based on = stage show by Pat Hanna
| narrator       =
| starring       = Pat Hanna Joe Valli
| music          = 
| cinematography = Arthur Higgins
| editing        =
| studio = Pat Hanna Productions
| distributor    =
| released       = 11 February 1933
| runtime        = 72 mins
| country        = Australia
| language       = English
| budget         =
| gross          =£18,000 
| preceded_by    =
| followed_by    =
| website        =
}}

Diggers in Blighty is a 1933 Australian film starring and directed by Pat Hanna. Hanna decided to direct this film himself after being unhappy with how F.W. Thring had handled Diggers (1931 film)|Diggers (1931). Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 160. 

==Plot==
While serving in the Australian Army in France in 1918, soldiers Chic and Joe steal some rum from the quartermasters store. They later help British intelligence pass on some false battle plans to a German spy and are rewarded with ten days leave in England. They go to a country house in Essex and have trouble with their uncouth manners but help some upper class friends have a romance.

==Cast==
*Pat Hanna as Chic Williams 
*Joe Valli as Joe McTavish
*George Moon as Joe Mulga
*Norman French as Sir Guy Gough
*John DArcy as Captain Jack Fisher
*Prudence Irving as Alison Dennett
*Thelma Scott as Judy Fisher
*Edwin Brett as the Colonel
*Nellie Mortyne as Aunt Martha
*Isa Crossley as the sister
*Raymond Longford as Von Schieling
*Guy Hastings as quartermaster sergeant
*Field Fisher as Muddles
*George Randall as Colonel Mason
*Alfred Frith as a Tommie
*Reg Wykeham as WO Pay Corps
*Sylvia Sterling as French adventuress

==Production==
The script was based on material Hanna had performed on stage for years. Although Hanna did not make the film under the Efftee umbrella, he hired Efftee Studios facilities and technical staff. 

The film was shot over six weeks commencing in October 1932. Many of the cast had appeared on stage, including Hanna, Valli and Moon. They were joined by comedian Alfred Firth in his film debut. 

Old Melbourne Gaol stood in for a medieval castle. 

==Reception==
The movie was released on a double bill with an Effee film, Harmony Row (1932). Hanna later claimed that he film was a big success at the box office but due to the amounts taken by cinema owners and distributors it took him over a year for his production costs to be reimbursed.  

Contemporary reviews were poor, the critic from the Sydney Morning Herald claiming that:
 Everyone in the play seems to be talking at the top of his or her voice; and talking so fast that the listener often grows quite desperate trying to keep up with them. Any microscopic respites from speech are zealously filled up with bursts of lively music... The directors must realise that actors need directing when they are before the camera. Merely to turn the players (however clever) loose in a drove across the studio floor is fatal.... The acting... is often much too violent for the screen; and, in the case of the women, the energetic "registering" of emotion recalls the early days of the silent screen... Mr. Hanna would be wise to consult well-informed opinion concerning his story and his continuity. Both are exceedingly weak.  

The film was released in England. 

Diggers in Blighty proved to have a long life and Hanna re-released it regularly over the next 20 years. It was appearing in cinemas as late as 1952.   

== References ==
 

== External links ==
* 
*  at Oz Movies

 
 
 