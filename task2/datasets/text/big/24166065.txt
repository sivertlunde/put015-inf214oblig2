Money Movers
{{Infobox film
| name           = Money Movers
| image          = MoneyMoversPoster2.jpg
| image_size     =
| caption        = DVD Cover
| director       = Bruce Beresford
| producer       = Matt Carroll
| writer         = Bruce Beresford
| based on       = novel by Devon Minchin Terence Donovan Tony Bonner Ed Devereaux Charles Bud Tingwell Candy Raymond Jeanie Drynan Bryan Brown Lucky Grills
| music          =
| cinematography = Don McAlpine
| editing        = William M. Anderson
| studio         = South Australian Film Corporation
| distributor    = Roadshow Entertainment
| released       = 1978
| runtime        = 92 minutes
| country        = Australia
| language       = English
| budget         = AU$536,861  
| gross          = AU$330,000 (Australia)
}}

Money Movers is a 1978 Australian crime action drama  film directed by Bruce Beresford. The film was based on the book Money Movers by Devon Minchin, founder of Metropolitan Security Services.  The story deals loosely with two real-life events, the 1970 Sydney Armoured Car Robbery where A$500,000 was stolen from a Mayne Nickless armoured van, and a 1970 incident where A$280,000 was stolen from Metropolitan Security Services offices by bandits impersonating policemen.   

Money Movers is "one of the few films of the 1970s that deal with crime and police corruption as an entrenched state of being, and one of the earliest to embrace extremely violent action." 

==Plot==
An armoured payroll truck owned by Darcys Security Services is robbed and the driver, ex-policeman Dick Martin, is removed from armoured cars and put onto night patrols. The robbers are double crossed by crime boss Jack Henderson whos henchman Dino kills all the robbers.
 speedway driver and a Senior Supervisor with Darcys, and his brother Brian Jackson who also works as a guard for Darcys as an armoured truck driver. When Eric Jackson breaks into Bassetts apartment, Hendersons men kidnap him and cut off the little toe on his left foot with a pair of bolt cutters in their attempt to force him to for him.

The film builds to a bloody climax as both Dick Martin and Leo Bassett foil the planned robbery.

==Cast== Terence Donovan as Eric Jackson
*Tony Bonner as Leo Bassett
*Ed Devereaux as Dick Martin
*Charles Bud Tingwell as Jack Henderson
*Candy Raymond as Mindel Seagers
*Jeanie Drynan as Dawn Jackson
*Bryan Brown as Brian Jackson
*Alan Cassell as Detective-Sergent Sammy Rose
*Gary Files as Ernest Sainsbury
*Ray Marshall as Ed Gallagher
*Hu Pryce as David Griffiths
*Lucky Grills as Robert Conway Frank Wilson as Lionel Darcy
*Terry Camilleri as Dino
*Stuart Littlemore as Himself (television reporter)

==Production==
After making The Getting of Wisdom Bruce Bersford signed a contract with the South Australian Film Corporation (SAFC) to make two films in two years. He wanted to make a movie that was in complete contrast with his last movie, and had written a script called The Ferryman. However the SAFC did not want to make it and they offered him a number of other projects instead.   accessed 17 October 2012  Beresford decided to adapt a novel by Devon Minchin, who founded Metropolitan Security Services in 1954. Beresford worked with MSS for two months doing research. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p53  

Shooting took six weeks in February and March 1978. Although the film was based in Sydney, it was shot mostly in the studios of the SAFC and at various locations in Adelaide, notably the Rowley Park Speedway, with some scenes also filmed in Sydney.  This is seen with vehicles regularly jumping between South Australias black on a white background license plates and the NSW version of black on yellow.

Although fake money was used in the film, where there was calls for large amounts of cash (approximately Australian dollar|A$1 million was used), real armed guards from Metropolitan Security Services (MSS) were on hand.

== Release ==
The film, when it was released in 1979, failed badly at the box office.  Beresford:
 Nobody went to see it. I went on the opening night in Melbourne and there were three people there and me. I was sitting up the back wondering what time the session started and then the film came on. I thought, this is going to be a disaster. And it was.  

20/20 Filmsight said the film is "often let down by stagy performances, uneven editing and a poor script", but is "worth checking out."  

Movie News said Money Movers "delivers an intriguing plot and hair-raising suspense with incredible pace and ferocity." 

Australian Screen said that "Money Movers was ahead of its time, and may have suffered because of that. The film opened early in 1979, and failed badly, but it was not alone – 1979 was the worst year for Australian films, in box-office terms, since the new wave of Australian cinema had begun." 

===Box Office===
Money Movers grossed $330,000 at the box office in Australia,  which is equivalent to $1,290,300 in 2009 dollars.

==See also==
* Cinema of Australia
* South Australian Film Corporation

==References==
 

==Further reading==
* McFarlane, Brian. Australian cinema New York: Columbia University Press, 1988. ISBN 0-231-06728-3
* Minchin, Devon George. The money movers London: Hutchinson of Australia, 1978. ISBN 0-09-130830-5
* Moran, Albert and Errol Vieth. Film in Australia: an introduction London: Cambridge University Press, 2006. ISBN 0-521-61327-2
* Murray, Scott. Australian film, 1978-1992: a survey of theatrical features : Vol. 2 London: Oxford University Press, 1993. ISBN 0-19-553584-7

==External links==
* 
*  at Oz Movies
 

 
 
 
 
 