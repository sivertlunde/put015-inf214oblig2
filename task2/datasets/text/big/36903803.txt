The Final Judgment
 
{{infobox film
| name           = The Final Judgment
| image          =
| imagesize      =
| caption        =
| director       = Edwin Carewe
| producer       = Rolfe Photoplays Incorporateed (B. A. Rolfe)
| writer         = George Scarborough (original screen story)
| starring       = Ethel Barrymore Mahlon Hamilton Percy Standing
| music          =
| cinematography =
| editing        =
| distributor    = Metro Pictures
| released       =   reels
}}
The Final Judgment is a 1915 silent film produced by B. A. Rolfe and distributed by Metro Pictures. Actor Edwin Carewe directed. It stars Ethel Barrymore in her second silent film and first as a player for then new Metro Pictures, later to become a part of MGM in 1924.  

A complete print of this film was preserved by MGM. 

==Cast==
*Ethel Barrymore - Jane Carleson, Mrs. Murray Campbell
*Beatrice Maude - Hortense Carleson
*Mahlon Hamilton - Murray Campbell
*H. Cooper Cliffe - Hamilton Ross
*Percy Standing - Henry Strong(*as Percy G. Standing)
*Paul Lawrence - Doctor Perry
*M. W. Rale - Kato, valet to Ross

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 