Someone Behind You
 
{{Infobox film
| name           = Someone Behind You
| image          = Someone Behind You film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         = 두 사람이다
 | rr             = Du saramida
 | mr             = Tu saramida}}
| director       = Nayato Fio Nuala Oh Ki-hwan
| producer       = Moo Ryung Kim
| writer         = Kang Kyung-ok (comic)
| starring       = Yoon Jin-seo Park Ki-woong Lee Ki-woo
| music          = Kim Jun-seok
| cinematography = Kim Yong-heung
| editing        = Kim Sun-min
| distributor    = M&FC Chungeorahm
| released       =  
| runtime        = 84 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = $1,760,707 
}}
Someone Behind You is a 2007 South Korean psychological horror film, based on the manhwa Its Two People by Kang Kyung-ok. It was released in America as Voices under the 2009 After Dark Horrorfest film festival. In this movie, a young woman tries to escape what seems to be a curse that is killing members of her family one by one.

==Plot==
Ga-In (Yoon Jin-seo) is a student in Korea. She and her family come to her aunt Jee-Suns wedding but before the wedding, Jee-Sun is pushed off the balcony and rushed to the hospital. Ga-In waits with her boyfriend, Hyun-Joong, while her aunt recovers. They then witness Jee-Suns younger sister, Kim Jung-Sun, repeatedly stabbing her.

Kim Jung-Sun is arrested for Jee-Suns murder, and held for questioning. It is revealed that the family believes they are cursed and at least one member dies in incomprehensible ways. In this case, Jung-Sun was possessed and killed Jee-Sun. At school, Eun-Kyung, the top student, attempts to kill Ga-In with scissors. During the struggle, Eun-Kyung is stabbed instead and transfers to another school. That same day, Ga-In is confronted by her teacher, who blames her for Eun-Kyung leaving and tries to kill her. Ga-In is saved by a classmate.

Hong Suk-Min, an eccentric student rumored to have killed his own father, advises Ga-In to trust no one, including herself. She visits Kim Jung-Sun to ask her about the murder. She tells her that Jee-Suns husband is Jung-Suns ex-boyfriend, and she had required revenge on Jee-Sun. She says that an unknown force possessed her and coerced her to kill. That evening, the classmate that had saved her comes to have his try at killing her. She runs inside only to find her mother throwing knives at her. Frightened and no longer feeling safe inside her own home, Ga-In tells her father she is leaving. Her father tells her that there is a forgotten family member in a different village, named Hwang Dae-Yong. On the bus, Ga-In meets Hong again, and together they visit Dae-Yong.

Dae-Yong tells them how he killed his wife in a fit of jealousy when he learned of his wifes alleged affair, then tried to find the force that controlled him after being released from jail. After Ga-In and Hong Suk-Min return home, Dae-Yong commits suicide to stay away from the curse. At school, Ga-In has an apparition of her best friend killing her and realizes that she really cannot trust anyone. One night she wakes up and finds her parents killed. It is divulged that Hyun-Joong is the one who killed her parents. Hyun Joong stabs her as she tries to save her younger sister, and wants to set the house on fire to turn it into hell. Both sisters stab him and leave him dead as the house begins to burn.

In the hospital, Ga-In and Ga-Yun are placed in the same room. Ga-In is having nightmares and is woken by her sister. Ga-In has a hallucination of Ga-Yun taking a knife from her pocket; they struggle and the knife ends up stabbing Ga-yun. Ga-Yun cries and asks Ga-In how she could do this to her own sister. Hong Suk-Min appears and exposes his true individuality: the curse. He can only be seen by Ga-In and the people he controls. Ga-In looks down at Ga-Yun and realizes that it wasnt a knife in her pocket, but instead a burnt family photo. Hong Suk-Min then tries to kill Ga-In. Ga-In stabs Hong Suk-Min, but in reality she stabs herself because he is controlling her from the inside.

In the epilogue, a boy is being verbally assaulted by his teacher. After the teacher leaves, Hong Suk-Min appears in a new body and asks the boy if he wants help to get back at the teacher.

==Reception==
The film currently holds a 43% "rotten" rating on Rotten Tomatoes audience review section.  Dread Central reviewed the movie, writing "These Voices need to shut up. Do not bother. Youve been warned."  

==Cast==
* Yoon Jin-seo as Ga-in
* Park Ki-woong as Suk-min
* Lee Ki-woo ... Heon-joong
* Oh Yeon-seo as Chung Eun-gyeong
* Kim So-eun .As Ga-hyun
* Ahn Nae-sang

==References==
 

==External links==
*    
*  
*   at HanCinema

 

 

 
 
 
 
 
 
 
 
 