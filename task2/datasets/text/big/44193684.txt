Three Bad Sisters
{{Infobox film
| name           = Three Bad Sisters
| image          = Three Bad Sisters poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Gilbert Kay 
| producer       = Howard W. Koch
| screenplay     = Gerald Drayson Adams
| story          = Devery Freeman
| starring       = Marla English Kathleen Hughes Sara Shane John Bromfield Jess Barker
| music          = Paul Dunlap
| cinematography = Lester Shorr 
| editing        = John F. Schreyer 	
| studio         = Bel-Air Productions
| distributor    = United Artists
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Three Bad Sisters is a 1956 American drama film directed by Gilbert Kay and written by Gerald Drayson Adams. The film stars Marla English, Kathleen Hughes, Sara Shane, John Bromfield and Jess Barker. The film was released in January 1956, by United Artists.  

==Plot==
 

== Cast ==
*Marla English as Vicki Craig
*Kathleen Hughes as Valerie Craig
*Sara Shane as Lorna Craig
*John Bromfield as Jim Norton
*Jess Barker as George Gurney
*Madge Kennedy as Martha Craig
*Anthony George as Tony Cadiz 
*Marlene Felton as Nadine 

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 