Reel Zombies
{{Infobox film
| name           = Reel Zombies
| image          = Reel Zombies.jpg
| caption        = DVD Cover 
| director       = {{plainlist|
* David J. Francis
* Mike Masters
}}
| producer       = {{plainlist|
* David J. Francis
* Mike Masters
* Stephen Papadimitriou
}}
| writer         = Mike Masters
| starring       = {{plainlist|
* Lloyd Kaufman
* Andrew Fruman
* Jordan Richards
}}
| cinematography = {{plainlist|
* Chris Bellio
* Chris Matthews
}}
| editing        = David J. Francis
| released       =   }}
| runtime        = 89 minutes
| country        = Canada
| language       = English
}}
Reel Zombies is a  2008 Canadian  .  Shot in documentary style, it depicts a film crew that attempts to follow up on their low budget zombie films during an outbreak of a real zombie apocalypse.

== Synopsis ==
When a zombie outbreak erupts, zombie films are made illegal.  Undeterred, a low budget film crew set out to create the sequel to their two previous, unsuccessful zombie films.  Reasoning that once zombie plague ends, there will once again be a market.  The filmmakers attempt to film around the hostile zombies, while also using them as extras in their film.

== Cast ==
* Dan Rooney as Basil
* Mukesh Asopa as Pascal
* Stephannie Hawkins as Rebecca Shelley
* Steve Curtis as Keith
* Mike Masters
* David J. Francis
* Stephen Papadimitriou
* Lloyd Kaufman
* Tony Watt

== Release ==
Reel Zombies premiered at the 2008 Lisbon International Horror Film Festival.   Synapse Films released it on DVD on 11 February 2014.   

== Reception ==
Michael Gingold of Fangoria wrote, "Reel Zombies is a mix of big laughs and smiles of recognition, and will likely be most appreciated by anyone who’s spent time in the indie filmmaking trenches themselves."   Dave Murray of JoBlo.com rated it 3/4 stars and wrote that it "couldnt be funnier".   HorrorNews.Net wrote that the film is very funny and offers many surprises.   Geoff Bough of Revenant Magazine wrote that the film "is honestly one of my favorite films of the year and provides an         explosive dose of hilarity to the zombie subgenre."   Olie Coen of DVD Talk rated it 3.5/5 stars and wrote, "If you like the genre check this out; its a refreshing take on an old storyline thats surprisingly entertaining and smart."   Gordon Sullivan of DVD Verdict wrote, "Though it has some good ideas—having actors play themselves, tying the film into the real world, making zombies slightly less threatening, and making a zombie film with "real" zombies—these additions to the formula arent utilized effectively." 

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 