The Mighty Quinn (film)
{{Infobox film
| name= The Mighty Quinn
| image= The Mighty Quinn.jpg
| caption= Film poster
| director = Carl Schenkel
| producer = Sandy Lieberson Marion Hunt Ed Elbert
| based on =  
| screenplay = Hampton Fancher Robert Townsend as Maubee
| music = Anne Dudley
| editing = John Jympson
| cinematography = Jacques Steyn
| studio = A&M Records|A&M Films
| distributor = Metro-Goldwyn-Mayer
| released =February 16, 1989
| runtime =98 min.
| language =English
| gross =$4,557,214
| budge t= }}
 1989 thriller thriller film Robert Townsend, James Fox, Mimi Rogers, M. Emmet Walsh, and Sheryl Lee Ralph.  The screenplay by Hampton Fancher is based on A. H. Z. Carrs 1982 novel Finding Maubee.  In the film, Washington plays Xavier Quinn, a police chief who tries to help his childhood friend Maubee (Townsend) after he becomes a murder suspect. 
 song of the same name, a Reggae cover version of which appears on the soundtrack. It was notable for film critic Roger Ebert to give the film an overwhelmingly positive review, calling it one of the best films of 1989.   

==Plot==
Xavier Quinn ( ), an arrogant political fixer, and the islands underqualified governor (Norman Beaton).  Quinns worries over the murder exacerbate his troubles at home; he is estranged from his wife, Lola (Sheryl Lee Ralph), and rarely has time to see his son.

Maubee eludes the police at every turn, even appearing personally to Xavier now and again, before running off. On one of these occasions, Quinn questions a witness afterward, who says that Maubee was carrying a "Large denominations of United States currency#.2410.2C000 bill|$10,000 bill," despite there being no such thing. Trying to track down Maubee, Quinn questions Ubu Pearl (Esther Rolle), the local witch and aunt of Maubees current girlfriend, Isola; and Hadley Elgin (Mimi Rogers), Thomass wife, who feels a powerful attraction to Quinn.  The governor also introduces Xavier to Fred Miller (M. Emmet Walsh), an affable American said to represent the murdered mans company.

The cause of Paters death seems obvious: he was found floating in a Jacuzzi tub, decapitated.  Against the governors instructions, Quinn has the body autopsied by the elderly Dr. Raj (Keye Luke), who reports that Pater died of a venomous snake bite and was already dead when his head was cut off.  Quinn notices a Latin man following him and stops to apprehend him.  The man, Jose Patina (Alex Colon), claims to be on vacation, but Xavier finds he has also been questioning people around the island trying to find Maubee. Quinn questions Hadley about her encounter with Patina. She tries to seduce him, but he resists, and wanders to a bar where he entertains the crowd with a piano performance. Half drunk, Quinn is picked up by Maubee in a stolen car, and they spend a night on the beach when they remininsce.  In the morning, Quinn wakes up to find Maubee gone, despite the handcuffs that he put on him.
 Congress doesnt agree. The President is using discontinued currency that is still good but will not be missed from its storage in the U.S. Treasury.  The murder messed up the plan, so the Central Intelligence Agency|C.I.A. has sent Miller to retrieve the money and "plug up the holes."

Quinn tracks Maubee down at their childhood playground in an ancient ruin, where Maubee explains the rest of the mystery: Pater impregnated Isola when she was a maid at the hotel.  Ubu Pearl demanded that Pater support the child. Pater laughed this off, and Isola was fired.  Ubu Pearl instructed Isola to go to the hotel and leave a snake in Paters room.  Maubee snuck into the house to see Isola and, when he found out where she had gone, sped to the hotel but was too late.  He arrived just as Pater was dying from the snakebite and saw the money in the suitcase.  Maubee cut Paters head off, put his body into the tub to try to conceal the real cause of death, and grabbed the money.

Miller arrives, having also found the hiding place, and holds the pair at gunpoint.  Maubee hands over the money, and Miller departs in a helicopter.  Insanely, Maubee runs out and grabs onto the helicopter as it lifts off over the ocean.  Seeing him, Miller sticks his gun out the window and fires, and Quinn watches helplessly as his friends body falls into the ocean. A snake hidden in the money sack slithers out and fatally bites the helicopter pilot.  Miller struggles to regain control, but the chopper crashes into the old ruins and explodes, killing them both.

Grieved at the loss of his friend, Quinn returns home and reconciles with his wife.  As he walks on the beach with his son, the camera pans down to show a line of bare footprints emerging from the water, leading to a rock with a $10,000 bill sitting on it.

==Cast==
* Denzel Washington as Xavier Quinn Robert Townsend as Maubee
* James Fox as Thomas Elgin
* Mimi Rogers as Hadley Elgin
* M. Emmet Walsh as Fred Miller
* Sheryl Lee Ralph as Lola Quinn
* Esther Rolle as Ubu Pearl
* Art Evans as Jump Jones
* Norman Beaton as Governor Chalk
* Alex Colon as Jose Patina
* Tyra Ferrell as Isola
* Keye Luke as Doctor Raj Carl Bradshaw as Cocodick (The Prisoner)
* Oliver Samuels Officer Rupert

==Production==
The Mighty Quinn was filmed at various locations throughout Jamaica, with the principal outdoor scenes shot in Port Antonio.  Interior scenes of Donald Paters mansion were filmed at Golden Clouds Villa in Oracabessa. 

==Reception==
The Mighty Quinn gained mostly positive reviews from critics, as it holds an 88% rating on Rotten Tomatoes based on 16 reviews.

Roger Ebert gave the film four stars.  The high point, he said, was Washingtons performance: 

  Oscar nomination Bond pictures, he is able to be tough and gentle at the same time, able to play a hero and yet not take himself too seriously. 
 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 