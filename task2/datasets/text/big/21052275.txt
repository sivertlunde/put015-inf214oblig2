The Boondock Saints II: All Saints Day
{{Infobox film
| name           = The Boondock Saints II: All Saints Day
| image          = Boondock 2 poster.JPG
| caption        = Theatrical release poster
| director       = Troy Duffy
| producer       = Chris Brinker Don Carmody
| writer         = Troy Duffy
| starring       = Sean Patrick Flanery Norman Reedus Clifton Collins Jr. Julie Benz Billy Connolly Judd Nelson Peter Fonda David Della Rocco
| music          = Jeff Danna
| cinematography = Miroslaw Baszak
| editing        = Bill DeRonde  Paul Kumpata
| studio         = Stage 6 Films Apparition
| released       =  
| runtime        = 117 minutes  
| country        = United States
| language       = English
| budget         = $8 million   
| gross          = $10,273,188 
}}
The Boondock Saints II: All Saints Day is a 2009 American vigilante film written and directed by Troy Duffy. The film serves as a sequel to the 1999 film The Boondock Saints. The film stars Sean Patrick Flanery and Norman Reedus, who return to their roles, as well as several of the other actors from the first film.

The film takes place eight years after the events of the original Boondock Saints, as fraternal twin sibling vigilantes Connor (Flanery) and Murphy (Reedus) are living a quiet life with their father, the former assassin known as "Il Duce". However, they are drawn back into action after someone attempts to frame the brothers for the murder of a priest in Boston, Massachusetts|Boston. The duo travel back to the United States, where they meet some old friends and are pursued by Eunice Bloom (Julie Benz), an FBI agent and former protégé of Agent Smecker.

==Plot==
 
After the McManus brothers, Connor and Murphy (Sean Patrick Flanery and Norman Reedus), and their father, Noah (a.k.a. "Il Duce") (Billy Connolly), assassinated Joe Yakavetta, they fled to Ireland. Eight years later, their uncle Sibeal (Mairtin OCarrigan), a priest, arrives to inform them that a renowned Boston priest was murdered by a mysterious assassin who attempted to frame the Saints by using their ritual assassination style. In response, the brothers dig up their old gear and weapons and depart for the United States.

En route to Boston aboard a container ship, the brothers meet a Mexican underground fighter named Romeo (Clifton Collins Jr.), who recognizes them as the Saints. Romeo convinces them to let him join them as their new partner. Hearing a radio broadcast regarding Joes son, Concezio Yakavetta (Judd Nelson), they deduce that he must have hired the hit-man who killed the priest in order to draw them out of hiding.
 Bob Marley, David Ferry and Brian Mahoney) are at the scene of the priests murder. They are greeted by Special Agent Eunice Bloom (Julie Benz), the protégé of Paul Smecker (who has died) who has been assigned to investigate the murder and determine whether or not the Saints are responsible. She comes to the conclusion that it was not the Saints who murdered the cleric and begins an investigation to find the real assassin. She and the other officers find out the assassin is Ottilio Panza (Daniel DeSanto), a man who appears to be working for a mysterious man known only as "The Old Man" (Peter Fonda).

Connor, Murphy and Romeo hit a warehouse that is being used by an Asian gang to process heroin for Yakavetta. After killing everyone at the warehouse, Connor and Murphy reunite with their old bartender friend, Doc (Gerard Parkes). They learn that the assassin was an independent contractor and that Yakavetta himself is hiding in the Prudential Tower. Later at the warehouse, now a crime scene, Bloom confirms that the Saints have returned. Bloom interrupts a massage in progress and hits a mob boss with a paddle, displaying her identity.
 Bob Rubin) set up a meeting with a group of mobsters at a bar, where they kill them. Panza arrives shortly after and attempts to ambush the brothers, but Bloom arrives in time to save them by wounding Panza who flees. Bloom introduces herself, revealing her intentions to help the Saints in Smeckers place. The group then cleans up the crime scene to make it look as if the mobsters had turned on each other. Later, Bloom reunites the other detectives with the Saints, thus bringing them in on their plans.

Yakavetta calls a meeting with his crew, during which the Saints arrive and kill everyone including Yakavetta. Bloom interrogates Yakavettas Consigliere Jimmy (Robb Wells) and learns of the Old Mans involvement with Panza. The crime scene is visited by FBI Special Agent Kuntsler (Paul Johansson) who takes over the gang murder case upon suspending Bloom. Later at the bar, Greenly arrives to celebrate the boys victory, but is shot and killed by Panza. Noah, earlier having decided to help his sons, unexpectedly arrives to demand Panza tell him the Old Mans location. They engage in a type of "Russian Roulette" stand-off and after Panza still refuses to answer, Noah kills him.

Noah reveals to the group that in 1958 New York, he watched a trio of mobsters brutally murder his father in front of his eyes. Consumed with anger and wanting revenge, Noah killed the mobsters with the help of his best friend Louie, who is revealed to be the Old Man. Noah still felt unsatisfied, so Louie helped him pick out mobsters to kill. They continued this until 1975, when Louie gave Noah up to the police.

Bloom illegally obtains a file regarding Louies location and gives it to Noah. Louie, anticipating the Saints arrival at his mansion, has several hit-men stationed on the grounds. When the McManus family arrives, Louie reveals that he had only used Noah to eliminate the competition in the Mafia, afterwards giving him up to the police when he was no longer useful. After this however, the Mafia cast Louie himself out for the very same reason. He then helped rebuild the Yakavetta family after Joes demise and let the Saints take out the rest of the organization so Louie could take control. Louie signals the hit-men waiting to take out the Saints to make their move, but the Saints kill them all. Noah suffers a fatal gunshot wound, but kills Louie before he dies. The Police arrive and arrest the wounded Connor, Murphy and Romeo.

Bloom meets with Father Sibeal who has arranged to take her to a safe place out of the country to flee FBI prosecution. She is shocked to discover that Sibeal has been working with Smecker (Willem Dafoe) who faked his own death and developed a network of support for the Saints and their work. Smecker tells Bloom his plans to break the Saints out of prison.

As protesters outside of the prison shout for the freedom of the Saints, Connor and Murphy stare out of their window at the sea of prisoners in the yard, finding that theyll have plenty of work while they wait to be freed.

==Cast==
 
* Sean Patrick Flanery as Connor McManus, one-half of the McManus brothers. Aside from the "Veritas" tattoo on his left hand, he now has a tattoo of Christs upper torso and face on his back.
* Norman Reedus as Murphy McManus, the other half of the McManus brothers. In addition to the "Aequitas" tattoo on his right hand, he now has a tattoo of Christs nailed feet on his back.
* Clifton Collins Jr. as Romeo, a Mexican fighter who becomes the brothers new sidekick.
* Billy Connolly as Noah McManus a.k.a. "Il Duce" ("The Duke"), father of the McManus brothers. FBI Special Agent Eunice Bloom, the agent assigned to the gang murders connected to the Saints. She is a former apprentice of Paul Smecker. FBI Special Agent Paul Smecker, ally to the McManus brothers in the first film. Fakes his own death and reveals at the end that he has started an agency funded by the Catholic Church which will help the Saints escape from jail and continue their work as vigilantes.
* Judd Nelson as Concezio Yakavetta, son of Don "Papa" Joe Yakavetta, who was executed by the Saints in court eight years ago. He hires a hitman to execute a priest to call out the Saints. Bob Marley, David Ferry and Brian Mahoney as Detectives Greenly, Dolly and Duffy, Boston Police Department detectives and secret allies of the Saints.
* Daniel DeSanto as Ottilio Panza a.k.a. "The Assassin", a hitman of short stature hired by Yakavetta to kill a priest.
* Bob Rubin as Gorgeous George, Yakavettas underboss from Brooklyn. He is forced by Special Agent Bloom and the brothers to give information on the Yakavetta clan.
* David Della Rocco as Rocco "The Funny Man", the brothers former sidekick, who was killed by Joe Yakavetta in the first film. He appears in flashbacks and dream sequences.
* Paul Johansson as FBI Special Agent Kuntsler, an agent who suspends Special Agent Bloom and takes over the gang murder case.
* Gerard Parkes as Doc, the owner of an Irish pub and a friend of the brothers. He has a stuttering problem, as well as Tourette syndrome.
* Peter Fonda as Louie a.k.a. "The Roman" or "The Old Man", Noahs former friend who had him sent to prison in 1975.
* Robb Wells as Jimmy the Gofer, Concezio Yakavettas Consigliere

==Production==
After years in development hell, {{cite web | url = http://dvd.ign.com/articles/708/708836p1.html
| title = Interview: Troy Duffy
| accessdate = 2006-12-02
| work = IGN}}  the success of the first films DVD release spurred 20th Century Fox to finance a sequel. {{cite web
| url = http://www.empireonline.com/news/story.asp?NID=18961
| title = Boondock Boom
| accessdate = 2006-11-28
| work = Empire (magazine)|Empire}}  In March 2008, Troy Duffy confirmed the film was greenlit. {{cite web
| url = http://www.movieweb.com/news/06/27406.php
| title = Boondock Saints 2: All Saints Day Will Happen
| accessdate = 2008-03-17
| work = Movieweb.com}} 
Pre-production on the film started in early September 2008. {{cite web
| url = http://www.youtube.com/watch?v=P-B9MI9p3IY
| title = Boondock II: All Saints Day (Day 35)
| accessdate = 2008-12-09
| work = YouTube}}  Principal photography took place in Ontario from October 20, 2008 to December 10, 2008. {{cite web
|url=http://www.omdc.on.ca/AssetFactory.aspx?did=6478
|title=Production in Ontario 2008
|publisher=Ontario Media Development Corporation
|format=PDF
|accessdate=September 4, 2010 }} 

Duffy kept a video diary of the films progress on YouTube, with some of the shooting sequences, and "question and answer" segments from fans answered by the films stars. {{cite web
| url = http://www.youtube.com/watch?v=d7F-Xx3fbrM
| title = Boondock II: All Saints Day (Day 7 of shooting, Female Lead Revealed.)
| accessdate = 2008-10-29
| work = YouTube}} 

==Marketing and release==
The films first trailer was officially released online on September 2, 2009 via IGN.     The film was initially released in 67 theaters in the Northeastern and Western areas of the continental US,  and was gradually released into more theaters in the following weeks.
  {{cite web 
|url=http://www.movieweb.com/news/the-boondock-saints-ii-all-saints-day-expands-nationwide |title=The Boondock Saints II: All Saints Day Expands Nationwide |accessdate=2011-01-12}} 

===Critical reception===
The film has received generally negative reviews from critics. On Rotten Tomatoes the film only received a 23%, which was still higher than the original films rating of 19%.  The film received better audience reviews, but not to the extent of the first film (62% compared to 93%).

==Home media==
On March 9, 2010 the film was released on single disc DVD and 2-Disc Steelbook Special Edition DVD as well as Blu-ray Disc|Blu-ray with special features including deleted scenes, audio commentary, and a behind the scenes featurette. 

On July 16, 2013 the film was released in a new Directors Cut as a Best Buy Exclusive release.

==Soundtrack==
The soundtrack became available for pre-order in late March on the official Boondock Saints Store website and became available for purchase and digital download at other retailers April 7, 2010.
{{tracklist
| extra_column      = Artist
| title1          = Ireland Intro
| extra1          = Jeff Danna
| length1         = 0:42
| title2          = Line of Blood
| extra2          = Ty Stone
| length2         = 3:49
| title3          = B.O.S.M
| extra3          = The Dirges
| length3         = 2:08
| title4          = Plastic Jesus
| extra4          = Taylor Duffy
| length4         = 4:27
| title5          = Eclipse
| extra5          = Radiant-X
| length5         = 0:57
| title6          = Balls Deep
| extra6          = Sean C
| length6         = 3:42
| title7          = Better Days
| extra7          = The Dirges
| length7         = 4:05
| title8          = Requiem Massive
| extra8          = Radiant-X
| length8         = 2:02
| title9          = Real Thang
| extra9          = Ty Stone
| length9         = 3:10
| title10         = Holiday
| extra10         = The Dirges
| length10        = 3:03
| title11         = The Wreckoning
| extra11         = Radiant-X
| length11        = 2:14
| title12         = Holy Fool
| extra12         = The Boondock Saints
| length12        = 4:16
| title13         = The Saints Are Coming
| extra13         = The Skids
| length13        = 2:51
| title14         = Saints From The Streets
| extra14         = Jeff Danna
| length14        = 2:56
| title15         = Crew Cut vs. Poppa M
| extra15         = Jeff Danna
| length15        = 1:48
| title16         = Young Noah
| extra16         = Jeff Danna
| length16       = 1:55
| title17         = Ireland
| extra17         = Jeff Danna
| length17        = 2:22
| title18        = Noahs Vendetta
| extra18        = Jeff Danna
| length18       = 1:36
| title19        = Fake-bake Shake
| extra19        = Jeff Danna
| length19       = 1:40
| title20        = Skyscraper Assault
| extra20        = Jeff Danna
| length20       = 3:02
| title21        = The Last Gun Battle
| extra21        = Jeff Danna
| length21       = 1:55
| title22        = Get Them Out
| extra22        = Jeff Danna
| length22       = 2:04
| title23        = Blood Of Cu Chulainn 2010
| extra23        = Jeff Danna & Mychael Danna
| length23       = 4:06
}}

==Books==
A six-issue comic book series, made up of 3 2-issue chapters written by Troy Duffy and published by 12 Gauge Comics, was released in May 2010 as a companion to the movie.  The story is a more in-depth version of Il Duces backstory together with the story of a hit the brothers performed that does not appear in the film.

There was also a mini-book available from the official Boondock Saints website which told a story that took place prior to the strip club scene from the first film. Both will eventually be released in a single graphic novel. 

==Sequel==
Director Troy Duffy spoke about a potential sequel in an interview on June 13, 2011. He said that "We’ve been approached to do a possible Boondock Saints TV series. So the fans may be getting a part 3 as a television show. We might be able to pull that off….I actually called both Sean and Norman and they both said “hell yeah, we’ll drop everything.” On March 21, 2012 it was indicated by Sean Patrick Flanery and Norman Reedus that Boondock Saints 3 is being written, tentatively titled "The Boondock Saints III: Saints Preserve Us."     However, on September 14, Norman Reedus stated that there would be no third movie. 

Again, on February 26, 2013, Troy Duffy stated that he was getting together with Norman Reedus and Sean Patrick Flanery to resume talks about Boondock Saints 3, in hopes that they could make the film a reality for fans. Later in 2013 at the Calgary Comic & Entertainment Expo, Sean Patrick Flanery confirmed that The Boondock Saints 3 is being worked on by Troy Duffy by saying "After the recent tragedies hit Boston I texted (director) Troy Duffy to ask him about when he would work on the third one and he replied ‘on it’".  On July 16, 2013, Troy Duffy stated in an interview with CraveOnline that he was halfway finished with the script for Boondock Saints III. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 