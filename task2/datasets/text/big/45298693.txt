Humo en los ojos
{{Infobox film
| name           = Humo en los ojos
| image          = 
| caption        =
| director       = Alberto Gout
| producer       = Alfonso Rosas Priego Enrique Rosas Priego
| writer         = Alberto Gout
| starring       = Meche Barba David Silva María Luisa Zea
| music          = Rosalío Ramírez
| cinematography = Ezequeil Carrasco
| editing        = 
| distributor    = Producciones Rosas Priego
| released       = November 13, 1946 (México) 
| runtime        = 77 min 
| country        = Mexico
| language       = Spanish
| budget         =
| gross          =
}}
Humo en los ojos (Smoke in the Eyes) is a Mexican drama film directed by Alberto Gout. It was released in 1946 and starring Meche Barba and David Silva. 


==Plot==
Maria Esther (Meche Barba) is a young girl that works as a dancer in a cabaret. María Esther falls in love with Carlos (David Silva), a sailor who had a previous relationship with a cabaret woman named Sofia (María Luisa Zea). A rivalry erupts between the two women.  Neither of the two realizes that, in reality, they are mother and daughter.

==Cast==
* Meche Barba ... María Esther
* David Silva ... Carlos
* María Luisa Zea ... Sofía
* Rubén Rojo ... Juan Manuel
* Fernando Soto "Mantequilla" ... Chon
* Toña la Negra ... Toña



==Reviews==
The plot was inspired by the same named song of Agustín Lara, who brought other tropical themes along with Juan Bruno Tarraza and Rafael Hernández Marín. One of the classic films of the Rumberas film is Humo en los ojos. In his essential and delicious Documentary History of Mexican cinema, the researcher Emilio García Riera writes the following comment: Its funny the outcome of this tropical melodrama. David Silva feels incestuous by simple solidarity with her former lover Maria Luisa Zea and that makes him give up of a Meche Barba more "femme fatal" than ever. In the microcosm of the tropical cabaret, the director Alberto Gout discovered how the passions tragically collide with the revelations of kinship. At the same time, Gout was finding, for him and for the genre of Rumberas, the style that would give prosperity in the coming years. 
 Rumbera films.   

==References==
 

==External links==
*  
*  

 
 
 
 
 

 