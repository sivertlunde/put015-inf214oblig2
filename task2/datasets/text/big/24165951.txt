Getting Even (1986 film)
{{Infobox film
| name           = Getting Even
| image          = Getting Even 1986.jpg
| image_size     =
| caption        =
| director       = Dwight H. Little
| writer         = Eddie Desmond Michael J. Liddle Dwight H. Little M. Phil Senini
| starring       = Edward Albert Audrey Landers Joe Don Baker Caroline Williams
| music          = Christopher Young
| cinematography = Peter Lyons Collister
| editing        = Charles Bornstein
| computer       = Ken Winters
| studio         = AGH Productions The Samuel Goldwyn Company The Movie Store ADG Avid Video Vestron Video
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Getting Even (also known as Hostage: Dallas) is a 1986 film directed by Dwight H. Little.

==Plot==

A soldier-of-fortune, Tag Taggert, played by Edward Albert, steals some Russian nerve gas from Afghanistan, and brings it to the U.S. to be analyzed. A greedy millionaire rancher, played by Joe Don Baker, finds out about it, steals it and uses it in an extortion scheme.  Audrey Landers plays the lead FBI agent-in-charge tasked with thwarting the extortion scheme.

==External links==
* 

 

 
 
 
 


 