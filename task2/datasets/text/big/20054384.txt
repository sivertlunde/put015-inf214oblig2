No, or the Vain Glory of Command
{{Infobox Film
| name           = No, or the Vain Glory of Command
| image          = 
| caption        = 
| director       = Manoel de Oliveira
| producer       = Paulo Branco
| writer         = Manoel de Oliveira P. João Marques
| starring       = Luís Miguel Cintra
| music          = 
| cinematography = Elso Roque
| editing        = Manoel de Oliveira
| distributor    = 
| released       = 26 September 1990
| runtime        = 110 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
}} Afonso of Isabella of Lusiads The episode of the Island of love, which are told through flashbacks as a professorish Portuguese soldier recounts them while marching through a Portuguese African overseas territory in 1973, during the Portuguese Colonial War (1961-1974). He easily draws his comrades into philosophical musings, while the little contingent suffers surprise attacks by groups of independentist guerrillas.

It was screened out of competition at the 1990 Cannes Film Festival.   

==Cast==
* Luís Miguel Cintra - Ens. Cabrita, Viriathus|Viriato, Dom João of Portugal Lusitanian Lusitanian War|warrior, Dom Joãos cousin
* Miguel Guilherme - Soldier Salvador, Lusitanian warrior, Battle of Alcazar|Alcácer warrior
* Luís Lucas - Cpl. Brito, Lusitanian warrior, Alcácer nobleman Carlos Gomes - Soldier Pedro, Alcácer warrior
* António S. Lopes - Soldier, Lusitanian warrior, Alcácer warrior Dom Sebastião Dona Isabel Dom Afonso
* Ruy de Carvalho - Preacher at funeral, Suicidal warrior (as Rui de Carvalho) Venus (as Teresa Meneses) Tethys
* Paulo Matos - Radio operator, Vasco da Gama, Luís Vaz de Camões|Camões Prince Dom João Dom Afonso V
* Duarte de Almeida - Baron of Alvito

==References==
 

==External links==
*  

 

 
 
 
 
 
 