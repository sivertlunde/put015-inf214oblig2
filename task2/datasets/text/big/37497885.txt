Satyagraha (film)
 
 

{{Infobox film
| name           = Satyagraha
| image          = Satyagraha poster.jpg
| caption        = Theatrical release poster
| alt            =  
| director       = Prakash Jha 
| producer       = Prakash Jha Ronnie Screwvala Siddharth Roy Kapur
| writer         = Anjum Rajabali Prakash Jha
| starring       = Amitabh Bachchan Ajay Devgn Kareena Kapoor Arjun Rampal Manoj Bajpai Amrita Rao Indian Ocean Meet Bros Anjjan Aadesh Shrivastava
| cinematography = Sachin Krishna
| editing        = Santosh Mandal
| studio         = Prakash Jha Productions UTV Motion Pictures
| distributor    = UTV Motion Pictures
| released       =   }}
| runtime        = 153 minutes   
| country        = India
| language       = Hindi
| budget         =   
| gross          =  
}}
 Hindi political political drama film  directed by Prakash Jha starring Amitabh Bachchan, Ajay Devgn, Kareena Kapoor, Arjun Rampal, Manoj Bajpai, Mitalee Jagtap Varadkar, Amrita Rao , jagat Singh and Vipin Sharma in the lead roles.    The first look of the film was released on 10 September 2012.   Satyagraha was released on 30 August 2013, although it released in the UAE one day before on 29 August.   The teaser was released online on 30 May 2013 and theatrically attached with Yeh Jawaani Hai Deewani.    

==Plot== DM and is imprisoned. Manav starts a campaign to free him, using social media, roping in Arjun Singh (Arjun Rampal) and journalist Yasmin (Kareena Kapoor). As hopeful students, hungry laborers and angry middle-class citizens join in the agitation, politicians start panicking. Eventually, Dwarka Anand gets freedom after the DM takes his complaint back upon pressure from Balram Singh.

Dwarka Anand gives a notice of 30 days to the government to clear all pending claims in the entire district. After a series of dramatic events, he sits on hunger strike and asks the government to bring ordinance in the district. Meanwhile, Lal Bahadur, a youth, commits suicide to support the agitation. During his cortege, four policemen brutally get killed by the mob. Soon after this, riots break out, forcing Balram Singh to send paramilitary force. His henchman shoot Dwarka Anand who dies in Manavs lap, requesting the public to stop the riots. Balram Singh is then caught by the police. Manav and Arjun decide to construct a regional party to eliminate corruption and reconstruct the system for the common welfare.

==Cast==
* Amitabh Bachchan 
: He plays the role of Dwarka Anand, a man who is a firm believer of truth and an inherent believer of society who wants his son to give back to society and the nation all that they have given him.
* Ajay Devgn 
:He plays the role of Manav Raghvendra. He represents shining India and is a brilliant telecommunication czar who uses the ways of the world to get what he wants. 
* Kareena Kapoor 
:She plays the role of Yasmin Ahmed. She is a hard headed TV reporter out in the field. She digs deep and has an opinion. She has a very strong relationship with Manav but when they clash on ideology they break up. Later they reunite when he comes back to his principles.
* Arjun Rampal 
:Plays the role of Arjun, a strong socially committed to become a politician. He studied in the same school of which Dwarka Anand was the principal.
* Manoj Bajpai  
:Plays the role of Balram Singh, wily young politician who uses every means to break the system. He is the real threat to democracy.
* Amrita Rao   plays the role of Sumitra as daughter-in-law of Dwarka.
* Vipin Sharma as Gauri Shankar, the Leader of the Opposition.
* Natasa Stankovic, in a special appearance in an item song.   
* Mitalee Jagtap Varadkar
* Mugdha Godse as Malini Mishra, a sultry, opportunistic corporate lady.  
* Indraneil Sengupta as Akhilesh, Dwarka Anands son (cameo).
* Prakash Jha in a special appearance.

==Filming==
Satyagraha acquired CNN chief international correspondent and journalist Christiane Amanpour in a role mirroring her real-life occupation as an international television correspondent.  Kareena Kapoor traveled to Dubai with her stylist to choose offbeat and chic attire that completed her look of an international journalist. Jha visualised Kareenas role as that of a reporter who keeps a tab on an entire movement and reports at the international level. The director was reportedly influenced by the huge fan following that Amanpour, famous for her reportage from war zones, enjoys.  

The story starring Amitabh Bachchan, based on social activist Anna Hazare, was shot mainly in Bhopal and New Delhi. The state-of-the-art news studio set was built in Bhopal as it offered more space. Ralegan Siddhi (village of Anna Hazare), a small village in Maharashtra with a population of roughly 2,500 people, served as one of the main shooting locations.  Bachchan plays a man who is a firm believer of truth, akin to a new age Gandhi, while Ajay plays an ambitious entrepreneur who represents the philosophy of modern India. Manoj Bajpai plays the role of a wily politician who uses every means to break the system.  

Shooting of Satyagraha began in February 2013 in Bhopal.         After that, the production unit headed to Ralegan Siddhi. Amitabh Bachchan stayed at the heritage Noor-Us-Sabah Palace Hotel, whose Gauhar Taj suite was being renovated.   Satyagraha was also shot at the IES College campus, Bhopal.  It was speculated that the film more than just touches upon the 2G spectrum scam that rocked the nation.  Prakash Jha had a small presence in the film.  

== Location ==
# Bhopal

===Post-production===
Amitabh and Ajay Devgn started dubbing of Satyagraha on 27 May 2013.  

==Soundtrack== Indian Ocean and Meet Bros Anjan, while the lyrics penned by Prasoon Joshi. It was released on 30 July 2013.

{{Infobox album  
| Name       = Satyagraha - The Revolution Has Begun!
| Type       = Soundtrack
| Artist     =  
| Cover      =
| Alt        =
| Released   =  
| Recorded   =
| Genre      = Film soundtrack
| Length     =  
| Label      =  
| Producer   =  
| Chronology = Salim-Sulaiman
| Last album = Rabba Main Kya Karoon (2013)
| This album = Satyagraha - The Revolution Has Begun! (2013)
| Next album = Kaanchi (2013)
| Misc           = {{Extra chronology
 | Artist        = Meet Bros Anjjan
 | Type          = Soundtrack
 | Last album    = Policegiri (2013)
 | This album    = Satyagraha - The Revolution Has Begun! (2013)
 | Next album    = Zanjeer (2013 film)|Zanjeer (2013)
 }}
 {{Extra chronology Indian Ocean
 | Type          = Soundtrack
 | Last album    = Peepli Live (2010)
 | This album    = Satyagraha - The Revolution Has Begun! (2013)
 | Next album    =
 }}
 {{Extra chronology
 | Artist        = Aadesh Shrivastava
 | Type          = Soundtrack
 | Last album    = Chakravyuh (2012)
 | This album    = Satyagraha - The Revolution Has Begun! (2013)
 | Next album    = Janta V/S Janardan - Bechara Aam Aadmi (Filming)
 }}
}}

{{Track listing
| extra_column = Artist(s)
| lyrics_credits = Prasoon Joshi
| total_length = 36:49
| music_credits = yes

| title1   = Satyagraha
| extra1   = Rajiv Sundaresan,Shivam Pathak & Shweta Pandit
| music1   = Salim-Sulaiman
| lyrics1  =
| length1  = 5:12

| title2   = Aiyo Ji
| extra2   = Salim Merchant & Shraddha Pandit
| music2   = Salim-Sulaiman
| lyrics2  =
| length2  = 3:55

| title3   = Raske Bhare Tore Naina
| extra3   = Shafqat Amanat Ali & Arpita
| music3   = Aadesh Shrivastava
| lyrics3  =
| length3  = 4:39

| title4   = Janta Rocks
| extra4   = Meet Bros Anjjan, Keerthi Sagathia, Shibani Kashyap, Shalmali Kholgade, Papon
| music4   = Meet Bros Anjjan
| lyrics4  =
| length4  = 7:34

| title5   = Hume Bole The
| extra5   = Rahul Ram, Amit Kilam & Himanshu Joshi
| music5   = Indian Ocean
| lyrics5  =
| length5  = 6:39
| title6   = Raske Bhare Tore Naina (House Mix)

| extra6   = Aadesh Srivastava & Arpita
| music6   = Aadesh Srivastava
| lyrics6  =
| length6  = 4:23

| title7   = Aiyo Ji (Remix)
| extra7   = Salim Merchant & Sharddha Pandit
| music7   = Salim-Sulaiman
| lyrics7  =
| length7  = 4:37
}}

==Release==
Satyagraha released on 30 August 2013 on more than 2,700 screens in India and occupied many multiplex screens in place of Madras Cafe.  The film was submitted for Censor certification on 8 August 2013.  The film released in the UAE on 29 August 2013. Satyagraha received a 12A certificate by British censors on 23 August 2013; it was awarded an M certificate by Australian censors on 27 August.

===Marketing=== Kareena Kapoor. The book, penned by writer Pooja Verma and published by Om Books International and priced at Rs. 995, was released on 1 September 2013.  The book tells the story of the development of the film.    Ajay Devgn and Prakash Jha promoted Satyagraha on Comedy Nights with Kapil.  The star cast also promototed the show on various news channels like Aaj Tak, Zee News, ABP News, Times Now, CNN IBN, etc.   The film was promoted in Mumbai and Delhi.    

===Controversy=== Gandhi Medical College (GMC, Bhopal). The palace itself, not a protected monument, is held by the public works department (PWD). On 28 August 2013, Bombay High Court refused to stay the films exhibition while hearing a suit filed by a producer claiming he was the original copyright owner of the movies title. The plea was to stop release of the film Satyagraha and sue for damages worth Rs 250&nbsp;million.   

==Critical reception==

===India===
The film received mixed reviews upon release.

Taran Adarsh of Bollywood Hungama rated it 4 out of 5 stars and stated that the film is an all-engrossing, compelling drama that mirrors the reality around us.  Ankit Ojha of Planet Bollywood gave 4 stars (out of 10), stating that, "Satyagraha is suitable only for those who can ignore the films many faults for its message. That in itself is going to be a major task for the audience to live up to."  Srijana Mitra Das of The Times of India gave it 3.5 stars out of 5 and said that "Satyagraha deserves an extra half-star for capturing corruption from root to branch."  India Today gave it 4 out of 5 stars and summarised that Satyagraha is "a timely wakeup call for a wounded nation".  Subhash K. Jha also gave it 4 out 5.   Joginder Tuteja gave it 4 out of 5 stars.  Neha Gupta of the Deccan Herald gave the film 4 stars and stated that Satyagraha conveys the uncontrollable anger and energy of a nation on the brink.  

Koimoi gave it 2.5 stars and judged that the film is a well-intentioned social drama.  Prathamesh Jadhav of DNA gave it 2.5 stars and concluded that it is a "predictable story spoon-fed with sincerity."  Sarita Tanwar of DNA India gave 2.5 stars and stated that Satyagraha is a bad script with good actors.  Saurabh Dwivedi of India Today gave it 2 stars and remarked that "despite choosing a hard-hitting topic other directors have managed to side step, Prakash Jhas Satyagraha was lacking and managed to land right on top of a pile of weak stories."  Rajeev Masand of CNN-IBN gave it 2 out 5 and opined that in Satyagraha, "Jha effectively meshes the urban angst witnessed on social media platforms like Twitter and Facebook with the ground realities of Indias heartland, but the plot subsequently loses its way."   Anupama Chopra gave it 2 stars and felt that the impact is diluted by a plot that "lurches from one event to the next without giving us anything new".  Resham Sengar of Zee News gave it 2 out of 5 stars and wrote that Satyagraha is a mission left unaccomplished.  Saibal Chatterjee of NDTV gave it 2.5 out 5, stating that parts of Satyagraha make perfect sense but, on the whole, "it never comes close to clicking into top gear. It leaves you more disappointed than angry."  

Khalid Mohamed of the Deccan Chronicle awarded it 2 stars and said that it offers nothing new either by way of content or style. 
Paloma Sharma of Rediff.com she gave the film 3 stars and stated that "Prakash Jhas much-awaited political thriller, Satyagraha has a heart of gold." She adds the story "attempts to speak about important issues but ends up over-simplifying them. Nevertheless, it is a good attempt."  Prasanna D Zore of Rediff.com gave it 3 star and stated that Prakash Jhas Satyagraha is a terrible hodgepodge of Arakshan, Rajneeti and Gangaajal. 
Shubhra Gupta of The Indian Express rated the film just 1.5 stars. 

===International===
 Dawn Pakistan gave it an average review stating that "Satyagraha starts out swell, even originally, for at least until the intermission, asking the right questions and building up a rational path to the post-break conflict. And then it crumbles bad, piling up drama-upon-drama that borders on being frivolous." 

==Box office==

===India=== East Punjab and Bangalore on the second day of its release. It collected   crore nett across its first two days of release. The films collections dropped by 26 percent but still grossed 140&nbsp;million nett on Sunday. The first weekend total of Satyagraha amounted to 40 cr crore nett. Its collection dropped by 20 percent within four days of its release. The film collected 1.08&nbsp;billion nett approx in three weeks. Ultimately, it was given the verdict of a semi hit.

===Overseas===
The 2 weeks overseas gross of film was 550&nbsp;million. 

==References==
 

==External links==
*  
*  
* Bollywood films of 2013

 

 
 
 
 
 
 
 
 