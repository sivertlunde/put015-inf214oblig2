Eenam
{{Infobox film
| name           = Eenam
| image          =
| caption        =
| director       = Bharathan
| producer       = MO Joseph
| writer         = P Padmarajan
| screenplay     = P Padmarajan
| starring       = Venu Nagavally Shanthi Krishna Adoor Bhasi Bharath Gopi
| music          = Bharathan
| cinematography = Madhu Ambatt
| editing        = MS Mani
| studio         = Manjilas
| distributor    = Manjilas
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by Bharathan and produced by MO Joseph. The film stars Venu Nagavally, Shanthi Krishna, Adoor Bhasi and Bharath Gopi in lead roles. The film had musical score by Bharathan.   

==Cast==
*Venu Nagavally
*Shanthi Krishna
*Adoor Bhasi
*Bharath Gopi
*Unnimary
*Sreenath

==Soundtrack==
The music was composed by Bharathan and lyrics was written by Venu Nagavally and Bharathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ambaadikkutta || Vani Jairam || Venu Nagavally || 
|-
| 2 || Maaleya lepanam || Vani Jairam, KP Brahmanandan || Bharathan || 
|}

==References==
 

==External links==
*  

 
 
 

 