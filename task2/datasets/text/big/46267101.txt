Kanoon (1943 film)
 
 
{{Infobox film
| name           = Kanoon
| image          = Kanoon_(1943).jpg
| image_size     =
| caption        = Song synopsis booklet cover
| director       = A. R. Kardar
| producer       = A. R. Kardar
| writer         = Qabli Amritsari 
| narrator       = Mehtab Gajanan Jagirdar Nirmala Devi 
| music          = Naushad 
| cinematography = Dwarkadas Diwecha 
| editing        = A. R. Kardar
| studio         = Kardar Productions
| distributor    = Mudnaney Film Service
| released       = 1943
| runtime        = 
| country        = British India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1943 Hindi/Urdu social film directed by A. R. Kardar.  It was produced by Kardar for "Kardar Productions".    The music director was Naushad who once again made a young Suraiya give playback for Mehtab (actress)|Mehtab.    The story was by Qabli Amritsari with screenplay and lyrics by D. N. Madhok. The film starred Shahu Modak, Mehtab, Gajanan Jagirdar, Nirmala Devi, Ulhas and Badri Prasad.     

The film was based on a social issue involving an arranged marriage between an older man to a girl who is younger than his son. 

==Plot==
Ashok and Usha are friends in college who fall in love with each other. They have a common issue which unites them; the social evil of young girls forcibly married off to older men. Ashok has strong reasons as he is ashamed of his father who is married a girl younger than Ashok. Lala Dinanath, Ushas father is heavily in debt to Seth Hirachand. Hirachands son, Ramesh, who studies in college with Usha has fallen in love with her. He does not know that his father intends marrying Usha in lieu of the debt her father owes Hirachand. His attraction for Usha causes a source of anguish for Hansa, the family Munims (accountant) daughter, and hence are in service to Hirachand. Hansa and Ramesh have been childhood friends with a vague understanding of marriage between them. 

When Dinanath is told by Hirachand that he would like to welcome Usha in his house, it is misunderstood as an offer of marriage for Ramesh. Dinanath agrees initially, but refuses when Hirachand lets him know his intentions of marrying Usha. Ramesh does not know about his fathers proposal to Dinanath, as his father has sent him off on a family business with the Munim. Usha agrees to marry Hirachand when he threatens to take Dinanath to court over the borrowed money. A drama staged by Ashok and Usha in college forms the final part of the story. The stage play called "Kanoon" (Law) is about the social injustices against women, which force a woman to take her life. The play is watched by Hirachand who starts becoming uncomfortable as the play unfolds and rushes home, just as the protagonist in the stage play puts a gun to her head. He reaches home and dies following a heart attack.	

==Cast==
* Shahu Modak Mehtab
* Gajanan Jagirdar
* Nirmala Devi Ulhas
* Badri Prasad
* A.Shah
* Jetha
* Amirali
* Amir Bano
* Padma

==Crew==
* Producer: A. R. Kardar
* Director: Kardar
* Production Company: Kardar Productions
* Story: Qabli (Amritsari)
* Cinematographer: Dwarka Diwecha
* Editing: A. R. Kardar
* Music: Naushad
* Lyrics: D. N. Madhok
* Art Director: D. S. Malvankar, Jagnath Mistry
* Make-up: Jetha N. Lakhani
* Sound Recordist: Minoo Katrak
* Distribuors: Mudnaney Film Service

==Soundtrack==
One of the popular songs in the film was "Ek Tu Ho Ek Main Hoon" sung by Suraiya and picturised on Mehtab.    Naushad, the composer, made "notable" use of trumpet and piano in the song.    Suraiya had earlier given playback singing for Mehtab in Sharda (film)|Sharda (1942), when Naushad had opted for the then thirteen-year-old Suraiya.    Mehtab, who was at first reluctant to have the "baby-voiced" Suraiya sing for her, was now "impressed" enough with her voice to have Suraiya playback for her.   

The lyrics were written by D. N. Madhok. The singers were Suraiya, Shyam and Nirmala Devi.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Aaye Jawani Jaaye Jawani
| Suraiya, Shyam
|-
| 2
| Aa More Saiyan Jiya Kalpaye
| Nirmala Devi
|-
| 3
| Bolo Bolo Re Sajanwa
| Nirmala
|-
| 4
| Saiyaan Khade More Dwaar Mein
| Nirmala
|-
| 5
| Ek Dil Tera Ek Dil Mera
| Suraiya, Shyam
|-
| 6
| Toot Gaya Ek Tara Mann Ka
| Suraiya, Shyam
|-
| 7
| Ek Tu Ho Ek Main Hoon
| Suraiya
|-
| 8
| Suno Fariyad Meri
| Nirmala
|}

==References==
 

==External links==
*  

 

 
 
 
 