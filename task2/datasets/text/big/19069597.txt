Paris Calling
{{Infobox film
| name            = Paris Calling
| image           = Paris Calling 1941 poster.jpg
| caption         = Theatrical release poster
| director        = Edwin L. Marin
| producer        = Benjamin J. Glazer Charles Kaufman
| starring        = Elisabeth Bergner Randolph Scott Basil Rathbone
| cinematographer = Milton R. Krasner
| music           = Hans J. Salter
| editing         = Edward Curtiss
| distributor     = Universal Pictures
| released        =  
| runtime         = 95 minutes
| country         = United States
| language        = English
}}
Paris Calling is a 1942 American war film directed by Edwin L. Marin and starring Basil Rathbone, Randolph Scott, Elisabeth Bergner.

==Synopsis==
Marianne Jannetier, a well-to-do Parisian, engaged to Andre Benoit, a high-ranking government official, flees the city when the goose-stepping Nazi storm-troopers arrive.

==Cast==
* Elisabeth Bergner as Marianne Jannetier
* Randolph Scott as Lt. Nicholas Nick Jordan
* Basil Rathbone as Andre Benoit
* Gale Sondergaard as Colette
* Lee J. Cobb as Captain Schwabe
* J. Pat OMalley as Sgt. Bruce McAvoy

==Production notes==
* Production dates: July 22 to Mid-September 1941
* This was the first film made in America by noted European stage and screen actress Elisabeth Bergner, whose name is spelled "Elizabeth" in the onscreen credits.

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 


 