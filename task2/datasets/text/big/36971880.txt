New Year's Eve (1924 film)
{{Infobox film
| name           = New Years Eve
| image          = 
| alt            = 
| caption        = 
| director       = Lupu Pick
| producer       = Lupu Pick
| writer         = Carl Mayer
| starring       = Eugen Klöpfer Edith Posca
| music          = Klaus Pringsheim, Sr.
| cinematography = Karl Hasselmann Guido Seeber
| editing        = Luise Heilborn-Körbitz
| studio         = Rex-Film GmbH
| distributor    = Universum Film AG
| released       =  
| runtime        = 66 minutes
| country        = German
| language       = Silent film
}}
 1924 Cinema German silent silent Kammerspielfilm directed by Lupu Pick and written by Carl Mayer. It was filmed in 1923 and premiered in Berlin on January 4, 1924.   The film is known to be one of the earliest examples of a kammerspielfilm  and was innovative in its extensive use of unchained camera technique|"entfesslte Kamera", using tracking and gliding techniques as opposed to keeping the camera stationary.  Like Picks previous films, New Years Eve does not use intertitles.

== Plot ==
A man is celebrating New Years Eve with his wife and his mother, who are at odds with one another. As the evening progresses, the rivalry between the two women increases to an open hatred that eventually escalates into a big fight. The man assumes no position in favor of either woman and instead chooses to flee from the conflict.

== Cast ==
* Eugen Klöpfer as Der Mann
* Edith Posca as Die Frau
* Frida Richard as Die Mutter
* Karl Harbacher
* Julius E. Herrmann
* Rudolf Blümner

==External links==
*  
*  
*  

== References ==
 

 
 
 
 
 
 
 
 
 


 