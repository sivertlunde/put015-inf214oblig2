La Fonte des neiges
{{Infobox film
| name           = La Fonte des neiges
| image          = La fonte des neiges film.jpg
| alt            = 
| caption        = 
| director       = Jean-Julien Chervier
| producer       = Sylvie Brenet 
| writer         = Jean-Julien Chevrier
| starring       = Marc Beffa, Géraldine Martineau
| music          = Steffen Breum 
| cinematography = Pierre Stoeber
| editing        = Julie Dupré 
| studio         = Vonvon Films Associés
| distributor    = Les Films du Requin
| released       =  
| runtime        = 26 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
La Fonte des neiges (English title: Thawing out; literally The melting of the snows) is a 2009 short comedy drama film directed by Jean-Julien Chervier.

==Synopsis==
La Fonte des neiges is about a few days of a holiday when a twelve-year-old child is forced to follow his mother to a nudist camp. At first deeply shy, he responds by wearing extra clothes. After meeting a girl he slowly becomes more relaxed and is revealed as a responsible and gentle person.  The discovery of his body and the first feelings of love take on the appearance of a fairy tale with a hallucinogenic quality. {{cite web
 |url=http://www.jeunecineaste.net/Entretien-avec-Jean-Julien.html
 |title=Jean-Julien Chervier
 |work=Jeune Cineaste
 |accessdate=30 May 2012}} 

==Reception==
The film, which deals carefully with a subject that could be controversial, was broadcast on the French-German national television channel Arte. It has been called "A very fine short film". 
The film was shown in 2009 at the Brooklyn Film Festival. {{cite web
 |url=http://www.brooklynfilmfestival.org/films/detail.asp?fid=964
 |title=THAWING OUT
 |work=Brooklyn Film Festival
 |accessdate=30 May 2012}}  
It was shown at the  Lisbon International Independent Film Festival in 2010. {{cite web
 |url=http://www.indielisboa.com/movie_detail.php?lang=2&movie=0759_01
 |title=La fonte des neiges
 |publisher=Lisbon International Independent Film Festival
 |accessdate=30 May 2012}} 
It was shown at the 12th Festival du Film Court Francophone at Vaulx-en-Velin in January 2012. {{cite web
 |url=http://www.vaulxfilmcourt.com/court-metrage-la-fonte-des-neiges.php
 |title=Court métrage : La fonte des neiges
 |work=vaulxfilmcourt
 |accessdate=30 May 2012}} 
It was also shown at international festivals such as Ebensee and Clermont-Ferrand.

==Cast==
*Marc Beffa : Léo
*Géraldine Martineau: Antoinette
*Zazon Castro
*Laurent Roth
*Natanaël Sylard
*Philippe Caulier
*Laura Luna

==References==
{{reflist |refs=
 {{cite web
 |url=http://www.cineclassik.com/2011/01/la-fonte-des-neiges-2009-jean-julien.html
 |title=La fonte des neiges (2009) Jean-Julien Chervier
 |work=Cineclassik
 |accessdate=30 May 2012}} 
}}

==External links==
*  

 
 