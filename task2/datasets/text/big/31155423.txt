Mayavi (1965 film)
{{Infobox film 
| name           = Mayavi
| image          =
| caption        =
| director       = GK Ramu
| producer       = P Subramaniam
| writer         = Neela Sree (dialogues)
| screenplay     = Sree Madhu Sheela Adoor Bhasi
| music          = MS Baburaj
| cinematography = GK Ramu
| editing        = N Gopalakrishnan
| studio         = Neela
| distributor    = Neela
| released       =  
| country        = India Malayalam
}}
 1965 Cinema Indian Malayalam Malayalam film,  directed by GK Ramu and produced by P Subramaniam. The film stars Prem Nazir, Madhu (actor)|Madhu, Sheela and Adoor Bhasi in lead roles. The film had musical score by MS Baburaj.   

==Cast==
 
*Prem Nazir as Raghu Madhu as Madhu
*Sheela as Vasanthy
*Adoor Bhasi as Bhasi
*Thikkurissi Sukumaran Nair as Krishna Menon
*Sreekantan Nair
*Soman
*Anandavally
*Aranmula Ponnamma as Raghus mother
*Kottarakkara Sreedharan Nair as Prathapan
*Kundara Bhasi
*Muttathara Soman
*Paravoor Bharathan as Police officer
*S. P. Pillai as Kaimani
*K. V. Shanthi as Malathi/Jayanthi
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran and .

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Jeevitham Innoru || Kamukara, KP Udayabhanu || P. Bhaskaran || 
|-
| 2 || Harinaakshi   ||  ||  || 
|-
| 3 || Kalivaakku Chollumbol || LR Eeswari, Chorus || P. Bhaskaran || 
|-
| 4 || Kannaaram Pothi || P. Leela, Kamukara || P. Bhaskaran || 
|-
| 5 || Pandorikkal || P. Leela || P. Bhaskaran || 
|-
| 6 || Pavizhakkunnil || S Janaki || P. Bhaskaran || 
|-
| 7 || Valakilukkum Vaanampaadi || S Janaki, KP Udayabhanu || P. Bhaskaran || 
|-
| 8 || Vandaaranikkuzhali || LR Eeswari, Kamukara || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 