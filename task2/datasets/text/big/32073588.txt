The Crimson Candle
{{Infobox film
| name           = The Crimson Candle
| image          = 
| image size     = 
| caption        = 
| director       = Bernard Mainwaring
| producer       = Bernard Mainwaring
| writer         = Bernard Mainwaring
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Eve Gray Eliot Makeham Kenneth Kove
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       = 
| runtime        = 48 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
}}


The Crimson Candle is a 1934 British crime film directed by Bernard Mainwaring and starring Eve Gray, Eliot Makeham and Kenneth Kove. 

==Plot==
A doctor attempts to prove that a maid is a murderer.

==Cast==
* Eve Gray - Mavis
* Eliot Makeham - Doctor Gaunt
* Kenneth Kove - Honorable Horatius Chillingsbotham
* Derek Williams  - Leonard Duberley
* Kynaston Reeves - Inspector Blunt
* Eugene Leahy - Detective
*  Audrey Cameron - Maid
*  Arthur Goullet

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 