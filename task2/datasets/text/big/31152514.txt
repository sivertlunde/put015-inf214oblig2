Almost a Honeymoon (1938 film)
{{Infobox film
| name = Almost a Honeymoon
| image =
| alt =  
| caption =
| director = Norman Lee
| producer = Warwick Ward Kenneth Horne Ralph Neale
| starring = Tommy Trinder Linden Travers Edmund Breon Frederick Burtwell
| music =
| cinematography = Bryan Langley
| editing = Ted Richards
| studio =
| distributor =
| released =  
| runtime = 80 minutes
| country = United Kingdom
| language = English
| budget =
| gross =
}} Almost a Honeymoon by Walter Ellis. Its plot follows a young man who urgently needs to find a wife so that he can get a lucrative job in the colonial service, and sets out to persuade a woman to marry him.

==Cast==
* Tommy Trinder - Peter Dibley
* Linden Travers - Patricia Quilter
* Edmund Breon - Aubrey Lovitt
* Frederick Burtwell - Charles
* Vivienne Bennett - Rita Brent
* Arthur Hambling - Adolphus
* Aubrey Mallalieu - Clutterbuck Ian Fleming - Sir James Hooper
* Betty Jardine - Lavinia Pepper
* Wally Patch - Bailiff

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 