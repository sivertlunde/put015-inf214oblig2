'Gator Bait II: Cajun Justice
{{Infobox Film
| name           = Gator Bait II: Cajun Justice
| image          = Gatorbait2.jpg 
| image_size     = 
| caption        = VHS Cover
| director       = Beverly Sebastion Ferd Sebastion
| producer       = Beverly Sebastion Ferd Sebastion
| writer         = Beverly Sebastion
| narrator       = 
| starring       = Jan Mackenzie Jocelyn Boudreaux Rocky Dugas Keith Gros
| music          = Julius Adams Vernon Rodrique George H. Hamilton
| cinematography =  
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
}} thriller film Gator Bait, written, produced and directed by Beverly Sebastion and Ferd Sebastion. Largely ignored upon release, the film received a second life on cable television and on home video.

==Plot==
When a sweet city girl is initiated into the rugged ways of the Louisiana swamp by her good-natured Cajun husband "Big T", she ends up putting her newly acquired survival skills into good use when she is kidnapped by Big Ts chief rival Leroy and his swarthy, brutish family as part of an ongoing feud.

==Cast==
* Melissa Alleman as Bridesmaid
* Joycelyn Boudreaux as Bridesmaid
* Rocky Dugas as Emile
* Keith Gros as Abert
* Reyn Hubbard as Geke
* Sid Larrwiere as Drunk
* Jan MacKenzie as Angelique
* Randolph Parro as Cajun Gentleman
* Susan Serigny as Cajun Cook
* Jerry Armstrong as Joe Boy
* Betty Flemming as Cajun Woman
* Levita Gros as Bartender
* Brad Kepnick as Luke
* Tray Loren as Big T.
* Paul Muzzcat as Leroy
* Ben Sebastion as Elick
* Peter Torrito as Bestman

==External links==
*  

 
 
 
 
 
 
 


 