Chip Off the Old Block
{{Infobox film
| name           = Chip Off the Old Block
| image          =
| caption        =
| director       = Charles Lamont
| producer       = Bernard W. Burton
| writer         = 
| starring       = 
| music          =
| cinematography = 
| editing        = 
| distributor    = Universal Pictures
| released       = 1 February 1944    
| runtime        = 71 min.
| country        = United States
| language       = English 
}}
 1944 United American film starring Donald OConnor, Peggy Ryan, and Ann Blyth.

The son of a strict Navy man, OConnor falls in love with a girl from a performing family (Ann Blyth). In the end, the families learn to accept their differences and they are allowed to be in love freely.

== Cast ==

* Donald OConnor as Donald Corrigan
* Peggy Ryan as Peggy Flaherty
* Ann Blyth as Glory Marlow III
* Helen Vinson as Glory Marlow Jr.
* Helen Broderick as Glory Marlow Sr.
* Arthur Treacher as Quentin
* Patric Knowles as Commander Judd Corrigan
* J. Edward Bromberg as Blaney Wright
* Ernest Truex as Henry McHugh
* Minna Gombell as Milly
* Samuel S. Hinds as Dean Manning
* Irving Bacon as Prof. Frost
* Joel Kupperman as Quiz Kid
* Mantan Moreland as Porter
* unbilled players include Leon Belasco, Vernon Dent and Dorothy Granger

== External links ==

*  

 

 
 
 
 
 
 
 


 