Lady L
{{Infobox film
| name           = Lady L
| image          = Ladylpos.jpg
| caption        = Original film poster by Robert McGinnis
| image size     = 
| director       = Peter Ustinov
| producer       = Carlo Ponti
| screenplay     = Peter Ustinov
| Based on       =  
| starring       = Sophia Loren Paul Newman David Niven 
| music          = Jean Françaix
| cinematography = Henri Alekan
| editing        = Roger Dwyre
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  (World Premiere, London)
| runtime        = 117 minutes
| country        = France Italy United Kingdom
| language       = English
| budget         = 
| gross          = $2.7 million (est. US/ Canada rentals) 
}} the novel Corsican lady recalls the loves of her life, including a Parisian aristocrat and an anarchist.

The film had its World Premiere at the Empire, Leicester Square in the West End of London on 25 November 1965. 

==Plot==
As she approaches her 80th birthday, the sophisticated and still attractive Lady Lendale recounts to her biographer, Sir Percy, the story of her life.

Fleeing her humble origins in Corsica, she traveled to Paris, where she found work in a brothel. There she falls in love with a thief and anarchist, Armand, and becomes pregnant by him. But before he can use a bomb to assassinate a Bavarian prince, she meets Lord Lendale, who is so enchanted by the young woman that he offers to overlook Armands activities if she will agree to marry him.

Lady L becomes a woman of means, royalty and high society, and together she and Lord Lendale raise a large family. In the end, however, Lendale reveals their secret, that Lady L has continued to be the lover of Armand, who has fathered all their children while posing as the familys chauffeur. 
 
==Production==

The film was a co-production between France, Italy and the United Kingdom. Castle Howard in Yorkshire was used for the shooting of some scenes.

==Cast==

*Sophia Loren as Lady Louise Lendale / Lady L
*Paul Newman as Armand Denis
*David Niven as Dicky, Lord Lendale
*Marcel Dalio as Sapper
*Cecil Parker as Sir Percy
*Philippe Noiret as Ambroise Gérôme
*Jacques Dufilho Dufilho as Bealu
*Eugene Deckers as Koenigstein
*Daniel Emilfork as Kobeleff
*Hella Petri as Madam
*Jean Wiener as Krajewski
*Roger Trapp as Linspecteur de police Dubaron
*Jean Rupert
*Joe Dassin as Un inspecteur de police
*Jacques Legras as Un inspecteur de police
*Mario Feliciani as Lanarchiste italien
*Sacha Pitoëff as Bomb-throwing revolutionary
*Arthur Howard as Butler
*Dorothy Reynolds
*Jacques Ciron	
*Hazel Hughes
*Michel Piccolii as Lecoeur Claude Dauphin as Inspector Mercier
*Catherine Allégret as Pantoufle
*France Arnel as Brunette
*Dorothée Blank as Blonde girl
*Jean-Paul Cauvin as The Little Orphan
*Lo Ann Chan as Chinese girl
*Sylvain Levignac
*Laurence Lignières as High society girl
*Tanya Lopert as Agneau
*Moustache as Delcour John Wood (uncredited) as the Photographer
*Jenny Orléans as Blonde girl
*Peter Ustinov as Prince Otto of Bavaria

==References==
 

 

 

 
 
 
 
 
 
 
 
 