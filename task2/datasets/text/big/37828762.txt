Azooma
{{Infobox film name           = Azooma  image          = File:Azooma_poster.jpg director       = Lee Ji-seung producer       = Lee Ji-seung   Ryu Sung-jin  writer         = Lee Ji-seung   Kim Hyeong-guk starring       = Jang Young-nam   Ma Dong-seok  music          = Jung Jin-ho  cinematography = Hwang Ki-seok   Yoon Joo-hwan  editing        = Shin Mi-kyung company        = Cinema Factory distributor    =  released       =   runtime        = 74 minutes country        = South Korea language       = Korean budget         =  gross          =  hangul         = 공정사회  rr             = Gongjeongsahoe mr             = Kongjŏngsahoe
}}
Azooma ( ) is a 2013 South Korean film starring Jang Young-nam in her first leading role as a mother seeking justice for the rape of her ten-year-old daughter.   It made its world premiere at the 2012 Busan International Film Festival, and was released in theaters on April 18, 2013. The film has since received recognition in the international film festival circuit.  

Written and directed by Lee Ji-seung,   the movies English title is a transliteration of the Korean term "ajumma" (아줌마), a form of address used for married (or simply older) women that has complex connotations and by which Jangs character is always called. The films Korean title ironically means A Fair Society. 

==Plot==
Seoul, the present day. Ten-year-old Yeon-joo (Lee Jae-hee) is picked up outside school by a man (Hwang Tae-kwang) who says he knows her mother and is then driven to a flat where she is sexually abused. Six hours later, her mother, Yoon Young-nam (Jang Young-nam), reports her disappearance to the police, who say it is too soon to launch a proper investigation. After being found dumped on the street in a suitcase, Yeon-joo is taken to a hospital by Yoon and recovers; however, Yoons ex-husband, TV celebrity dentist Dr. Lee (Bae Sung-woo), is not happy at the adverse publicity Yoons action has generated. Yoon eventually persuades a busy detective, Ma (Ma Dong-seok), to take an interest in the case; he questions Yeon-joo in hospital but the child reveals little. Later, a female police officer questions her, with more success. Angry at the apparent slowness with which the police are treating the case, Yoon tracks down the child molester herself and confronts him at his flat. After a struggle and chase, the police arrive and take both of them in, though en route the child molester escapes. Yoon decides to take more radical action.     

==Cast==
*Jang Young-nam - Yoon Young-nam
*Ma Dong-seok - Detective Ma
*Hwang Tae-kwang - the rapist
*Bae Sung-woo - Dr. Lee
*Lee Jae-hee - Lee Yeon-joo

==Awards== Athens International Film and Video Festival: First Prize in the Feature Narrative section  
*2013 Beloit International Film Festival: Best Feature Film  
*2013 Irvine International Film Festival: Best Actress - Jang Young-nam    
*2012 Costa Rica International Film Festival: Best Feature Film
*2012 Nevada Film Festival: Platinum Award
*2012 Directors Guild of Korea: Best Actress - Jang Young-nam 

==References==
 

==External links==
*   at M-Line Distribution
*  
*  

 
 
 
 