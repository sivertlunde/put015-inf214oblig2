Adam (2009 film)
{{Infobox Film
| name           = Adam
| image          = Adamposter09.jpg
| caption        = Promotional film poster
| director       = Max Mayer
| producer       = Miranda De Pencier Leslie Urdang Dean Vanech
| writer         = Max Mayer
| starring       = Hugh Dancy Rose Byrne Frankie Faison Mark Linn-Baker Amy Irving Peter Gallagher
| music          = Christopher Lennertz
| cinematography = Seamus Tierney
| editing        = Grant Myers
| studio         = Olympus Pictures
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $3,200,000
| gross          = $2,549,605 
}}
Adam is an American romantic drama film written and directed by Max Mayer, starring Hugh Dancy and Rose Byrne. The film follows the relationship between a young man named Adam (Dancy) with Asperger syndrome, and Beth (Byrne). Mayer was inspired to write the films script when he heard a radio interview with a man who had Aspergers.

Filming took place in New York in December 2005. The film premiered at the 2009 Sundance Film Festival, where it won the Alfred P. Sloan Prize, and was released in the United States on July 29, 2009. The release date in Canada and the UK was 7 August 2009 in Australia, and everywhere else after Labor Day.

==Plot==
Adam Raki (Dancy) is a young man, who has Asperger syndrome, living alone in Manhattan after his fathers recent death. He has a friend, Harlan Keyes (Frankie Faison), an old army friend of his fathers, who is always there for him. Because of his condition, Adam has difficulty communicating with others and likes to escape into his love of space exploration. His fixation on detail, repetitive behaviors, and mind-blindness cost him his job at a toy manufacturing company and nearly get him arrested after he is mistaken for a pedophile. He has been living in the apartment he and his father shared, and does not want to leave it, but the loss of his job leaves him with an uncertain future, including the problem of continuing to pay the mortgage. Although he has Aspergers Syndrome and likes to stick to his own routine, avoiding socializing with others, Adam is lonely and wishes things could be different.
  
Beth Buchwald (Byrne), a school teacher and aspiring childrens book writer, moves into the apartment above his and they strike up an awkward friendship. One night, Beth is shocked to find him trying to clean her apartment windows suspended from the roof of the building in a spacesuit.

Beth takes a liking to Adam despite his oddities. Adam hopes for a relationship, but the first attempts are uneasy due to his fear of social interaction. Eventually he breaks out of his beloved routines enough to be able to date Beth.  
  
When he tries to ask Beth if she was aroused as he was by their spotting of raccoons during an outing to Central Park, she is taken aback. Adam admits his Aspergers to her, explaining his inability to interpret her emotions. Surprised by his innocence, Beth understands the reason for his question. The next day she asks a fellow teacher some questions about Aspergers Syndrome and concludes with the assertion that surely Adam is not "dating material".

However Beth is drawn to Adam and in fact the eccentricities of Aspergers make him more attractive to her, his innocence, honesty and unique ways endear him to her and she falls in love. Eventually their relationship becomes sexually intimate and Beth also helps him understand how to better connect with her, how to introduce himself to her family, and how to interview for a new job. As time goes on there are moments in which Beth and Adam dont understand each other. One of those moments involved Adams suggestion to Beth that her childrens book on raccoons, simply be true to life and avoid anthropomorphizing the animals.
  
Things go downhill as Beths father, Marty (Peter Gallagher), gets into legal trouble. Adam naively asks him questions about the details of Martys alleged crimes, to make conversation, not understanding how to make small talk. This creates a rift with Beth who doesnt understand Adams mistake as innocent, although she later apologizes to him.

Marty pleads guilty and is about to be sentenced. Beths mother (Amy Irving) calls Adam trying to find Beth. She asks him to check Beths calendar for any way to reach her. He finds that Beth had put down a reminder to meet her parents on the night they "unexpectedly" bumped into each other. Beth knew that Adam would be uncomfortable, so she and her father engineered an "accidental" meeting. When Beth confirms this, Adam angrily accuses her of being a liar and her father a criminal. Beth storms out and goes to her parents home. 
   guidance and navigation systems. He is uncertain about leaving the home he has known for most of his life. Feeling guilty for the things he said to Beth, he sets out by any means of transportation to the Buchwalds home in upstate New York, even on foot, to apologize to Beth. He asks her to go to California with him. Marty tries to convince Beth not to go with someone "not of this world." This leads to a heated argument with Beth, who tells her father that he has lied to her and the family about his crimes.
  
Beth returns with Adam to New York City to prepare him to go to California, but she has second thoughts when she asks Adam exactly why he wants her to come to California with him. Not realizing this is meant to be an opportunity for him to express his feelings for her, he lists practical ways in which she helps him, which Beth interprets as meaning that Adam doesnt feel love for her in the same way that she feels it for him. Realizing that the ways in which they express themselves are extremely different, she tells Adam she cant go to California with him. Beth therefore essentially submits to her fathers view that they are from different worlds. To navigate the continuation of learning to be with each other is too much for Beth while she struggles with the strong emotions connected to her fathers imprisonment.
  
A year later Adam is working at the observatory where his keen interest in space telescopes and his eidetic memory have made him successful and fulfilled in his job. He has also seemingly learned much more about picking up on social cues and making an effort to deliberately put himself in social situations. This time when a female colleague is struggling with heavy boxes, he remembers to ask her if she needs help. Adam receives a package from Beth, containing her first published childrens book inspired by Adam and Aspergers Syndrome. He reads the first page in which Beth has anthropomorphized raccoons, used to represent Adam and his family and that Adam didnt feel he had a place in society in New York. Adam looks deep in thought and has a moment of realization, he understands why Beth wanted to anthropomorphize the raccoons. Even after a year apart, this moment of clarity seems to bring him closer to Beth and he smiles in happiness.

==Cast==
* Hugh Dancy as Adam Raki
* Rose Byrne as Beth Buchwald
* Peter Gallagher as Marty Buchwald
* Amy Irving as Rebecca Buchwald
* Frankie Faison as Harlan Keyes
* Mark Linn-Baker as Sam Klieber
* Karina Arroyave as Anna Maria
* Maddie Corman as Robin
* Adam LeFevre as Wardlow
* Mark Margolis as Older Man
* Steffany Huckaby as Carol

==Production==
Max Mayer formed the idea for Adam when he heard a man with Asperger syndrome being interviewed on National Public Radio about how he viewed the world. He was intrigued and, after doing further research on Aspergers, felt that the syndrome was a metaphor for "the tension between all of us human beings   the desire to connect to one another".         As he worked on the screenplay, Mayer drew the character of Adam from the man he had heard talking on the radio in addition to his research, while Beths character was based on numerous people Mayer knew.  Hugh Dancy and Mayer worked together for a month before filming began to develop Adams character,  and in preparation for his role, Dancy spoke with individuals affected by Aspergers about their feelings, sensory issues and interests. 

Principal photography took place in New York City in December 2005.    Specific filming locations included Greenwich Village and Central Park.  

==Release==
Adam had its world premiere at the 2009 Sundance Film Festival on January 20, 2009 where it won the Alfred P. Sloan Prize for "an outstanding feature film focusing on science or technology as a theme, or depicting a scientist, engineer or mathematician as a major character."  Although producer Leslie Urdang was initially doubtful of the films chances of finding a distributor at Sundance,  Fox Searchlight Pictures bought the distribution rights.  In March 2009, Adam was screened at the Method Fest Independent Film Festival, winning the Jury Award for Best Picture.  It was shown at the Seattle International Film Festival,  Edinburgh Film Festival,  CineVegas Film Festival  and San Francisco Jewish Film Festival  before it had a U.S. theatrical release on July 29, 2009. It was released on DVD January 5, 2010. 

== Reception ==

The movie received mixed reviews. Roger Ebert gave it 2.5/4 stars, saying that the movie "wraps up their story in too tidy a package, insisting on finding the upbeat in the murky, and missing the chance to be more thoughtful about this challenging situation."
The movie holds a 64% rating at Rotten Tomatoes and 56% at Metacritic. Nina Caplan of Time Out London accused the movie of dishonesty: "But it’s not clear why a pretty daddy’s girl would fall for a talkative and lonesome engineer with Asperger’s and a space fixation."

The movie was also somewhat of a box office flop|flop, making only around 80% of the spent budget at the box office and losing millions of dollars to the producers.

==Soundtrack==
# "A Friendly Face"- Flipper Dalton
# "Gone Away"- Lucy Schwartz
# "Someone Elses Life"- Joshua Radin
# "Plastic Flowers"- The Hiders
# "Into The Light"- The Alexandria Quartet
# "Beautiful Day"- Miranda Lee Richards
# "When You Find Me"- Joshua Radin feat. Maria Taylor
# "Cant Go Back Now"- The Weepies
# "Somebody Loved"- The Weepies
# "Girl With the Light Brown Hair"- Charlotte Politte
# "Jingle Bells"
# "O Christmas Tree"

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
{{succession box Alfred P. Sloan Prize Winner
| years=2008
| before=Sleep Dealer
| after=Obselidia}}
 

 
 

 
 
 
 
 
 
 
 
 
 
 