Woodstock (film)
{{Infobox film
| name           = Woodstock
| image          = WoodstockFilmPoster.jpg
| caption        = Film poster by Richard Amsel
| director       = Michael Wadleigh
| producer       = Bob Maurice
| writer         =
| starring       =
| music          =
| cinematography =
| editing        = Michael Wadleigh Martin Scorsese Stan Warnow Yeu-Bun Yee Jere Huggins  Thelma Schoonmaker
| distributor    = Warner Bros.
| released       =  
| runtime        = 184 minutes 225 minutes (1994)
| country        = United States
| language       = English
| budget         = $600,000
| gross          = $50 million   
}}
 counterculture Woodstock Bethel in New York. Entertainment Weekly called this film the benchmark of concert movies and one of the most entertaining documentaries ever made. {{cite book
|title= The Entertainment Weekly Guide to the Greatest Movies Ever Made
|year=1996
|publisher=Warner Books
|location=New York
|isbn=
|page=130}}  

The film was directed by Michael Wadleigh. Seven editors are credited, including Thelma Schoonmaker, Martin Scorsese, and Wadleigh. Woodstock was a great commercial and critical success. It received the Academy Award for Best Documentary Feature. Thelma Schoonmaker was nominated for the Academy Award for Film Editing, which is a quite rare distinction for a documentary film.  Dan Wallin and Larry Johnson (film producer)|L. A. Johnson were nominated for the Academy Award for Best Sound.       The film was screened at the 1970 Cannes Film Festival, but wasnt entered into the main competition.   

The 1970 theatrical release of the film ran 184 minutes. A directors cut spanning 225 minutes was released in 1994. Both cuts take liberties with the timeline of the festival. However, the opening and closing acts are the same in the film as in real life; Richie Havens opens the show and Jimi Hendrix closes it.
 Jimi Hendrix at Woodstock was also released separately on DVD and Blu-ray.

In 1996, Woodstock was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". An expanded 40th Anniversary Edition of Woodstock, released on June 9, 2009 in Blu-ray and DVD formats, features additional performances not before seen in the film, and also includes lengthened versions of existing performances featuring Creedence Clearwater Revival and others. 

==Artists==

===Artists by appearance===
{|cellpadding="2" cellspacing="1" border="0" style="background:#FFDEAD;"
|---- style="background:#FFF"
| bgcolor="#ffdead" | No.
| bgcolor="#ffdead" | Group / Singers
| bgcolor="#ffdead" | Title
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 1.*
| bgcolor="#FFFFFF" | Crosby, Stills, Nash & Young|Crosby, Stills & Nash
| bgcolor="#FFFFFF" | "Long Time Gone"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 2.*
| bgcolor="#FFFFFF" | Canned Heat
| bgcolor="#FFFFFF" | "Going Up the Country"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 3.*
| bgcolor="#FFFFFF" | Crosby, Stills, Nash & Young|Crosby, Stills & Nash
| bgcolor="#FFFFFF" | "Wooden Ships"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 4.
| bgcolor="#FFFFFF" rowspan="2" valign="top" | Richie Havens
| bgcolor="#FFFFFF" | "Handsome Johnny"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 5.
| bgcolor="#FFFFFF" | "Sometimes I Feel Like a Motherless Child|Freedom" / "Sometimes I Feel Like a Motherless Child"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 6.
| bgcolor="#FFFFFF" | Canned Heat
| bgcolor="#FFFFFF" | "A Change Is Gonna Come" **
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 7.
| bgcolor="#FFFFFF" rowspan="2" valign="top" | Joan Baez
| bgcolor="#FFFFFF" | "Joe Hill"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 8.
| bgcolor="#FFFFFF" | "Swing Low Sweet Chariot"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 9.
| bgcolor="#FFFFFF" rowspan="2" valign="top" | The Who
| bgcolor="#FFFFFF" | "Were Not Gonna Take It (The Who song)|Were Not Gonna Take It" / "See Me, Feel Me"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 10.
| bgcolor="#FFFFFF" | "Summertime Blues"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 11.
| bgcolor="#FFFFFF" | Sha-Na-Na
| bgcolor="#FFFFFF" | "At the Hop"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 12.
| bgcolor="#FFFFFF" | Joe Cocker and the Grease Band
| bgcolor="#FFFFFF" | "With a Little Help from My Friends"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 13.
| bgcolor="#FFFFFF" | Audience
| bgcolor="#FFFFFF" | "Crowd Rain Chant"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 14.
| bgcolor="#FFFFFF" | Country Joe and the Fish
| bgcolor="#FFFFFF" | "Rock and Soul Music"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 15.
| bgcolor="#FFFFFF" | Arlo Guthrie
| bgcolor="#FFFFFF" | "Coming Into Los Angeles"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 16.
| bgcolor="#FFFFFF" | Crosby, Stills, Nash & Young|Crosby, Stills & Nash
| bgcolor="#FFFFFF" | " "
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 17.
| bgcolor="#FFFFFF" | Ten Years After
| bgcolor="#FFFFFF" | "Im Going Home"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 18.
| bgcolor="#FFFFFF" rowspan="2" valign="top"| Jefferson Airplane
| bgcolor="#FFFFFF" | "Saturday Afternoon" / "Wont You Try" **
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 19.
| bgcolor="#FFFFFF" | "Uncle Sams Blues" **
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 20.
| bgcolor="#FFFFFF" | John Sebastian
| bgcolor="#FFFFFF" | "Younger Generation"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 21.
| bgcolor="#FFFFFF" | Country Joe McDonald FISH Cheer / Feel-Like-Im-Fixing-to-Die-Rag"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 22. Santana
| bgcolor="#FFFFFF" | "Soul Sacrifice"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 23.
| bgcolor="#FFFFFF" | Sly and the Family Stone Dance to the Music" / "I Want to Take You Higher"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 24.
| bgcolor="#FFFFFF" | Janis Joplin
| bgcolor="#FFFFFF" | "Work Me, Lord" **
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 25.
| bgcolor="#FFFFFF" rowspan="5" valign="top" | Jimi Hendrix
| bgcolor="#FFFFFF" | "Voodoo Child (Slight Return)" (credited as "Voodoo Chile" in the film) **
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 26. The Star-Spangled Banner"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 27.
| bgcolor="#FFFFFF" | "Purple Haze"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 28.
| bgcolor="#FFFFFF" | "Woodstock Improvisation" **
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 29.
| bgcolor="#FFFFFF" | "Villanova Junction"
|----- style="background:#FFF"
| bgcolor="#FFFFFF" | 30.*
| bgcolor="#FFFFFF" | Crosby, Stills, Nash & Young
| bgcolor="#FFFFFF" | "Woodstock (song)|Woodstock" / "Find the Cost of Freedom" **
|----- style="background:#FFF"
|}

* studio recording from an album by the artist 
** directors cut only, not in the original theatrical release

===Artists omitted===
 
  Sweetwater
*Incredible String Band
*Bert Sommer
*Tim Hardin
*Ravi Shankar
  Melanie
*Quill Quill
*Keef Keef Hartley Mountain
*Grateful Dead (a Jerry Garcia interview is included)
 
*Creedence Clearwater Revival
*The Band
*Blood, Sweat & Tears Johnny and Edgar Winter Paul Butterfield
 

==Reception==
Woodstock received universal acclaim from newspaper and magazine critics in 1970.  It was also an enormous box office smash.  The May 20, 1970 edition of   but one of the most profitable movies of that year as well.
 review aggregate website Rotten Tomatoes. 

==Subsequent editions==

===25th Anniversary "Directors Cut" (1994)===
 
Upon the festivals 25th anniversary, in 1994, a directors cut of the film — subtitled 3 Days of Peace & Music — was released. It added over 40 minutes and included additional performances by Canned Heat, Jefferson Airplane and Janis Joplin. Jimi Hendrixs set at the end of the film was also extended with two additional numbers. Some of the crowd scenes in the original film were replaced by previously unseen footage.
 Mama Cass Elliot, Jim Morrison, John Lennon, Max Yasgur, Roy Orbison, Abbie Hoffman, Paul Butterfield, Keith Moon, Bob Hite, Richard Manuel, Janis Joplin and Jimi Hendrix. It ends with the epitaph to the right:

===40th Anniversary edition (2009)===
On June 9, 2009 a remastered 40th Anniversary edition was released on both Blu-ray and DVD.  The 40th Anniversary edition is available as both a two-disc "Special Edition" and a three-disc "Ultimate Collector’s Edition". The film was newly remastered and provided a new 5.1 audio mix. Among the Special Features two extra hours of rare performance footage feature 18 performances never before seen (from 13 groups including Joan Baez, Country Joe McDonald, Santana, The Who, Jefferson Airplane, Canned Heat, Joe Cocker) and five (Paul Butterfield, Creedence Clearwater Revival, The Grateful Dead, Johnny Winter and Mountain) who played at Woodstock but never appeared in any film version. 

===Woodstock: 3 Days of Peace & Music - The Directors Cut, 40th Anniversary Revisited (2014)===
Same version of the main movie, but some of the bonus items now in HD on Blu-ray. Also contains exclusive bonus tracks only available from special retailer versions from the last edition.    

== Cultural references ==
In the science fiction thriller The Omega Man (1971), Colonel Robert Neville (played by Charlton Heston) is seen traveling to a movie theatre in Los Angeles to screen the film for himself alone. Woodstock had been the most recent film debuting prior to the onslaught of biological warfare, and Neville darkly remarks the film is so popular it was "held over for the third straight year". As he repeats some of the dialogue verbatim, it is clear that Neville has repeated the ritual many times during the two years that he has believed himself to be the last man alive on Earth.

== References ==
 

== Further reading ==
*{{cite book
  | last = Kato
  | first =  M. T.
  | authorlink =
  | title = From Kung Fu to Hip Hop: Globalization, Revolution, and Popular Culture
  | publisher = SUNY Press
  | year = 2007
  | location =
  | url = http://books.google.com/books?id=JvXM9YJnmfcC&printsec=frontcover
  | isbn = 0-7914-6991-3
}} Cf.  
*{{cite book
  | last = Saunders
  | first = Dave
  | authorlink =
  | title = Direct Cinema: Observational Documentary and the Politics of the Sixties
  | publisher = Wallflower Press
  | year = 2007
  | location = London
  | isbn = 1-905674-16-3
}}
*{{cite book
  | last = Bell
  | first = Dale
  | authorlink =
  | coauthors = (edited by)
  | title = Woodstock An Inside Look at the Movie that Shook Up the World and Defined a Generation
  | publisher = Michael Wiese Productions 
  | year = 1999
  | location = Studio City
  | isbn = 0-941188-71-X
}}

==External links==
*  
*  
*  . Chicago Sun-Times.
*  . DVD Times.
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 