Chorus (2015 film)
 
{{Infobox film
| name           = Chorus
| image          = 
| caption        = 
| director       = François Delisle
| producer       = 
| writer         = François Delisle
| starring       = Sébastien Ricard
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Canada
| language       = French
| budget         = 
}}

Chorus is a 2015 Canadian drama film written and directed by  . It was screened in the Panorama section of the 65th Berlin International Film Festival.   

The film centres on Christophe (Sébastien Ricard) and Irène (Fanny Mallette), a former married couple still struggling to cope with the murder of their son eight years earlier. 

==Cast==
*   as Christophe
* Fanny Mallette as Irene
* Geneviève Bujold as Gabrielle
* Pierre Curzi
* Antoine LÉcuyer
*  
*  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 