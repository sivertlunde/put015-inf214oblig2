Taxi!
 
{{Infobox film
| name           = Taxi!
| image          =Taxi_lobby_card_2.jpg
| image_size     =260px
| caption        =Lobby card
| director       = Roy Del Ruth
| producer       = Robert Lord John Bright 
| narrator       =
| starring       = James Cagney Loretta Young
| music          = 
| cinematography = James Van Trees
| editing        = James Gibbon
| distributor    = Warner Bros.
| released       = January 23, 1932
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Taxi! is a 1932 film starring James Cagney and Loretta Young.  The movie was directed by Roy Del Ruth.

==Plot==

When a veteran cab driver, Pop Riley (Guy Kibbee), refuses to be pressured into surrendering his prime soliciting location outside a cafe, wherein his daughter works, the old mans cab is intentionally wrecked by a ruthless mob seeking to dominate the cab industry. Upon learning of the "accidental" destruction of his cab (and along with it his livelihood), the old man retrieves his handgun and shoots the bullying man known to be responsible, which lands him in prison, where he dies of poor health in fairly short order.

Pops waitressing daughter, Sue (Loretta Young), is asked by a scrappy young cab driver, Matt (James Cagney), to lend moral support to a resistance movement populated by other drivers, who are also experiencing similar strong-arm tactics by the same aggressive group of thugs. However, after enduring the crushing loss of her father, Sue undergoes a complete ethical reversal about the notion of fighting back, feels thoroughly sickened by the violence and bloodshed, and she angrily tells the drivers as much.

Her unpredictably willful but passionate rant instantly lands her on Matts bad side, although he eventually has a redemptive change of heart, then seeks to charm Sue into becoming his girlfriend. Other complications arise shortly thereafter, putting their loving relationship in jeopardy, which all too often tends to involve Matts inability to control his own quick and fiery temper.

==About the film==
 
The film includes two famous Cagney dialogues, one of which features Cagney conducting a conversation with a passenger in Yiddish, and the other when Cagney is speaking to his brothers killer through a locked closet, "Come out and take it,  you dirty yellow-bellied rat , or Ill give it to you through the door!." The provenance of this sequence led to Cagney being famously misquoted as saying, "You dirty rat, you killed my brother."

Also, in a lengthy and memorable sequence, an unbilled George Raft and his partner win a ballroom dance contest against Cagney and Young, after which Cagney slugs Raft and knocks him down. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 20 

In the film they see a movie at the cinema called Her Hour of Love starring John Barrymore, about whom Cagney cracks a joke saying, "his ears are too big". The film was fictional, but also advertised in the cinema in the film is The Mad Genius, which was an actual film starring Barrymore which was released the previous year by Warner Brothers and is a plug by them.   

==Cast (in credits order)==
*James Cagney as Matt Nolan
*Loretta Young as Sue Riley Nolan
*George E. Stone as Skeets
*Guy Kibbee as Pop Riley
*Leila Bennett as Ruby
*Dorothy Burgess as Marie Costa David Landau as Buck Gerard
*Ray Cooke as Danny Nolan

== References ==
 

== External links ==
 
*  
* 

 

 
 
 
 
 
 
 
 
 
 