Zombie Beach
  }}
{{Infobox film
| name          = Zombie Beach
| image         = Zombie Beach.jpg
| caption       = 
| director      = Mukesh Asopa
| producer      = Asopa Films Incorporation   Twinkle Productions   Asopa Films International
| writer        = Mukesh Asopa   Twinkle Asopa   Rajesh Asopa   Carey Lewis
| starring      = Mukesh Asopa   Kamal Nandi   Katherine Stefanski   Kevin Tracy   Kristi Woods   Rick Cordeiro
| released      =  
| runtime       = 71 minutes
| language      = English
| country       = Canada
}}
Zombie Beach is the third film from Canadian based Asopa Films Incorporation, Twinkle Productions and Asopa Films International after Aisha and Rahul and The Taste of Relation. {{cite web
  | last = Don of the Dead
  | title = Zombie Beach, 2010 Canada (Zombie Zone News)
  | url= http://www.zombiezonenews.com/archives/zombie-beach-2010-canada/
  | accessdate = 2012-12-08 }} 

==Plot==
Centuries ago, rather than have a full out war of the revelation, a deal was struck between the heroes of the good and the evil. As a result of the deal a beach was sanctioned to the evil. There, the undead could walk like living beings among us, as Zombie (fictional)|zombies. {{cite web
  | last = Cordeiro | first = Rick
  | title = Zombie Beach, Plot Summary
  | url= http://www.imdb.com/title/tt1604064/plotsummary
  | accessdate = 2012-12-08 }} 

==Cast==
* Mukesh Asopa as Roshan
* Katherine Stefanski as Vanessa
* Kamal Nandi as The Holy Man
* Kevin Tracy as Crazy Nicky
* Kristi Woods as Sharon
* Rick Cordeiro as The Detective

==Festivals and Awards==
* Award of Excellence - Feature Competition at the Canada International Film Festival -  
* Golden Palm Award - Feature Competition at the Mexico International Film Festival -  
* Silver Ace Award - Feature at the Las Vegas Film Festival -  
* Nominated for  Red Rose award at the 2011 Jaipur International Film Festival (JIFF)  Port Townsend Film Festival, Washington, USA 
* Appeared at Third World Indie Film Festival 

== References ==
 

==External links==
*  

 
 
 

 