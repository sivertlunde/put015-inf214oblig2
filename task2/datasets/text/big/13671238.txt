Legally Blondes
{{multiple issues|
 
 
}}

{{Infobox film
| name =  Legally Blondes
| image = legallyblondes.jpg
| caption = 
| director = Savage Steve Holland Sean McNamara Marc Platt David Grace
| writer = Chad Creasey Dara Resnik
| based on =   Milly Rosso Becky Rosso Christopher Cousins Brittany Curran Bobby Campo
| music = John Coda
| cinematography = William D. Barber
| editing = Cindy Parisotto Savage Studios Brookwell McNamara Marc Platt Productions
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 86 min
| country = United States English
| website = 
}}

Legally Blondes (also known as Legally Blonde 3) is a 2009 film of the Legally Blonde series from Metro-Goldwyn-Mayer|MGM.  Elle Woods, previously played by Reese Witherspoon, does not appear in the film, but is mentioned several times. Witherspoon produced the film. The film aired on ABC Family and Disney Channel, and is directed towards a younger audience than the first two films. The film stars Camilla and Rebecca Rosso as Elle Woods British twin cousins.

==Plot== chihuahua dogs, their giddiness is cut short when they find out they are going to attend Pacific Preparatory, a private school requiring uniforms.

Upon their first day, Izzy and Annie are registering themselves and they sign for ID cards. They also start off on the wrong foot with Tiffany Donohugh (Brittany Curran), the spoiled daughter of a primary funder of "Pac Prep." And they also meet Chris, who is almost immediately smitten with Annie.

Tiffany later apologizes for her rude behavior and befriends the twins, although she is doing it merely to "keep her enemies close." Annie and Izzy believe Tiffany to be a sweet person, though she dislikes classmates on scholarship. However, her true colors come out, when she embarrasses Annie and Izzy at a formal dance, revealing that they are on partial scholarship at the school.

Izzy and Annie rekindle their friendships with the other scholarship students, including Chris. Izzy wants to help Chris get closer to Annie, but in several instances, Annie believes Chris to like Izzy. The twins and their friends believe the uniforms are stifling their creativity and they redo their clothing while still cleverly abiding by the schools many rules.

Chris and Izzy are later accused of cheating on a big history test and they all set out to prove the school wrong. Izzy and Annie suspect Tiffany and Justin are behind this and find that Chris and Tiffany have the same backpack that comes with a lock and key. In Chriss locked zipper he keeps a master-key, that opens all doors in the school, for his work-studies to help pay off his scholarship. They discover that all the keys and locks for the backpacks are the same, any key will open any lock. On the floor of the teachers private office, where the answer key is kept, they discover red markings that match the marks that Justins expensive shoes create. Tiffany told Justin the uber code to access the answers to the test.

With this new piece of information, Annie and Izzy set to prove Izzys innocence. In student court, Annie poses as Izzy when she is locked in the bathroom by Justin, and defeats her fear of public speaking. Even as Izzy escapes and returns to the court, Annie points out that Justin owns a new cell phone, one that has not yet been released to the public, and could only have gotten from one person, the daughter of the creator, Tiffany Donohugh. Justin, who is irritated by noises such as pencils being sharpened and pen clicks, struggles to hold himself together as the entire courtroom click their pens. He finally admits to framing Chris and Izzy and Tiffany admits to being in on it too, though she protests that she cannot be punished since her father is a major funder of the school. Despite this, Headmistress Elsa Higgins expels them both for violating the Pac Prep honor code and for attempting to frame their fellow classmates for a crime they didnt commit.

At the end of the movie, Annie and Chris dance together at a school dance and Izzy does the same with Brad (a secret scholarship student who assisted the twins in student court by ringing Justins phone). As for Tiffany and Justin, they now face the consequences for their actions as they are apparently cut off from their wealth (as punishment from their respective parents for trying to frame Izzy and Chris and for Tiffany shaming her family who funded her previous school) and are seen boarding a rowdy student-filled bus and whisked off to public school, much to their horror.

==Principal cast== Camilla Rosso as Annabelle "Annie" Woods  Rebecca Rosso as Isabelle "Izzy" Woods 
*Lisa Banes as Headmistress Elsa Higgins
*Christopher Cousins as Mr. Richard Woods
*Brittany Curran as Tiffany Donohugh
*Curtis Armstrong as Mr. Gary Golden
*Rose Abdoo as Sylvia
*Bobby Campo as Christopher Lopez
*Chad Broskey as Justin Whitley
*Chloe Bridges as Ashley Meadows
*Amy Hill as Ms. Chang
*Kunal Sharma as Vivek
*John Michael McPhail-Doherty as Angry Bike Messenger
*Christoph Sanders as Brad Wellington
*Tanya Chisholm as Marcie
*Teo Olivares as Rainbow

==Soundtrack==
*"This Is Me" performed by Danielle McKee 
*"Its Up To Me" performed by Cathy Heller
*"Gaoti Raga" performed by The New Indians
*"Just a Girl" performed by Bexy
*"Welcome to the Party" performed by Molly M
*"Super Secret Agent Girl" performed by The Mona Lisas
*"Swing Hard" performed by Steve Multer and Karen A. Muller
*"Come On Come On" performed by Joey Sykes
*"Hey Now" performed by Buddah Belly
*"Ordinary Superstar" performed by Danielle McKee
*"Without You" performed by Lewis LaMedica
*"Gave It Away" performed by Sleestack
*"Rigamarole" performed by BTK
*"Who I Am" performed by Danielle McKee
*"This Time - Its Mine" performed by Britney Christian
*"Lucky Girl" performed by Rebecca "Becky" and Camilla "Milly" Rosso
*"Its a Girls World" performed by Tabitha Fair
*"Dance All Nite" performed by The Raymies

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 