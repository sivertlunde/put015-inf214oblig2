The Marriage of Figaro (film)
{{Infobox film
| name =  The Marriage of Figaro 
| image =
| image_size =
| caption =
| director = Georg Wildhagen Walter Lehmann
| writer =  Pierre Beaumarchais  (play)   Lorenzo da Ponte  (libretto)   Georg Wildhagen
| narrator =
| starring = Angelika Hauff   Willi Domgraf-Fassbaender   Sabine Peters   Elsa Wagner
| music =   
| cinematography = Eugen Klagemann   Karl Plintzner  
| editing =  Hildegard Tegener       
| studio = DEFA
| distributor = Progress Film
| released = 25 November 1949  
| runtime = 107 minutes
| country = East Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} The Marriage of Figaro by Pierre Beaumarchais. The film was made by DEFA, the state studio of East Germany. It sold 5,479,427 tickets. 

==Cast==
* Angelika Hauff as Susanna 
* Willi Domgraf-Fassbaender as Figaro
* Sabine Peters as Gräfin Rosine 
* Mathieu Ahlersmeyer as Graf Almaviva 
* Elsa Wagner as Marcellina 
* Victor Janson as Dr. Bartolo 
* Alfred Balthoff as Basilio  Franz Weber as Don Curzio 
* Ernst Legal as Antonio 
* Willi Puhlmann as Cherubino 
* Katharina Mayberg as Barbarina
* Theodor Vogeler as Schreiber

== References ==
 

== Bibliography ==
* Davidson, John E. & Hake, Sabine. Framing the Fifties: Cinema in a Divided Germany. Berghahn Books, 2007. 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 