Flu (film)
{{Infobox film
| name           = Flu
| image          = The_Flu_poster.jpg
| film name      = {{Film name
 | hangul         =  
 | hanja          = 
 | rr             = Gamgi 
 | mr             = Kamgi}} Kim Sung-su 
| producer       = Kim Sung-jin   Seo Jong-hae   Jeong Hoon-tak   Im Young-ju
| writer         = Lee Yeong-jong   Kim Sung-su 
| starring       = Jang Hyuk Soo Ae 
| music          = Kim Tae-seong
| cinematography = Lee Mo-gae
| editing        = Nam Na-yeong iFilm Corp. iLoveCinema CJ Entertainment (international)
| released       =  
| runtime        = 121 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| admissions     = 
| gross          =    
}} Kim Sung-su about an outbreak of a deadly disease which throws a city into chaos. It stars Jang Hyuk and Soo Ae.    

==Plot== Bundang by H5N1 (avian influenza, or bird flu). As the man escapes, he quickly spreads the virus to nearby residents. Mir (Park Min-ha), daughter of a single mother Kim In-hae (Soo Ae), meets the man, named Mossai, and gives him some food and tells him to wait as she calls rescue worker and paramedic Kang Ji-goo (Jang Hyuk). However as he arrives, Mossai is nowhere to be found.
 Code Blue" was declared over the hospitals intercom, indicating that his brother has entered cardiac arrest. As he runs into the isolation room of his brother, he had already expired.

A meeting of doctors and politicians is held. A doctor insists on the lockdown of Bundang, which is met with sharp criticism by a Congressman, which is worried about his approval ratings. Meanwhile, the virus spreads at an alarming rate, with it being airborne. 2,000 new cases were being reported at every hour, with the hospitals being overloaded with people vomiting blood and expiring.
 APB is sent out to find him, as he could be a source for a vaccine.

In the quarantine camp, infected persons are separated and placed in cages. It is later revealed that they are placed in plastic bags, with some of them still alive and thrown into a pit in a stadium and incinerated. Mir is found to be infected by her mother, but manages to sneak through the head to toe examination. Her condition deteriorates, just as the antibody carrier is found. In desperation, Mirs mother takes her to the room containing Mossai, the person with the antibody, and does a blood transfusion, just before the door is kicked down and she is tranquilized by a soldier.

Mir is brought to the pit of bodies to be incinerated, but is rescued by Kang Ji-goo. The uninfected people see the pit and a riot ensues, forcing the evacuation of all medical and military personnel from the camp. Kim In-hae and the antibody carrier are the last to be evacuated, however, he is stabbed multiple times before he is evacuated by the first victims brother, and dies on the way to the control center.

The mob of people from the camp reach their way to the city limit of Bundang, where a checkpoint of soldiers are waiting. The South Korean defense minister, citing the Mutual Defense Treaty between the United States and South Korea, clears the soldiers to engage the unarmed civilians, against the orders of the President, who is enraged. Dozens of people are shot and killed, and the civilians withdraw.

Mir, who is slowly recovering due to the development of antibodies in her system, is brought by Ji-goo to the city limit. They are separated and Mir runs with the crowd, which is again now slowly moving towards the border. An order is given by the defense minister to shoot anyone who crosses the border, which is indicated by an Orange Line. Mirs mother runs from the other side from the border, through the soldiers who try in vain to stop her.

As Mir sees her mother on the other side, she starts running towards the Orange Line. A general orders a soldier to fire. The soldier hesitates and the general puts a gun to his head, reinstating his order to fire. The soldier is forced to fire, whose bullet hits the mother. The wound however, is not critical. Mir uses herself as a human shield and pleads with the soldiers not to fire. The soldiers do not fire any further shots, moved by the young girls tears.
 fighter jets. The South Korean prseident, who is informed of Mir as being an antibody carrier, states that he has supreme command of Capital Defence, and puts out a call to ready surface-to-air missiles, and to shoot down the approaching jets.
 SAMs lock on the jets, but they are called off by an American general at the last second. An ambulance later brings Mir to a hospital where a vaccine and a cure is developed.

Ji-goo, Mir and In-hae are later seen on their way to a holiday, with South Korea returning back to normal.

==Cast==
* Jang Hyuk as Kang Ji-goo   

* Soo Ae as Kim In-hae    

* Park Min-ha as Kim Mi-reu 

* Yoo Hae-jin as Bae Kyung-ub
* Ma Dong-seok as Jeon Gook-hwan
* Lee Hee-joon as Byung-ki
* Kim Ki-hyeon as the South Korean prime minister
* Cha In-pyo as the South Korean president
* Lee Sang-yeob as Byeong-woo
* Park Hyo-joo as Teacher Jung
* Park Jung-min as Chul-gyo
* Boris Stout as Leo Snyder
* Kim Moon-soo as Dr. Yang
* Choi Byung-mo as Choi Dong-chi
* Lester Avan Andrada as Mong Sai
* Jo Hwi-joon
* Noh Gi-hong as head of contagion center
* Lee Dong-jin as rescue swimmer 1
* Son So-yeong
* Lee Young-soo as citizen at boundary line citizen
* Choi Jung-hyun as Prevention Special Forces
* Lee Sang-hyung as Defense Special Forces 
* Kim Hyung-seok as ER resident 2
* Ham Jin-seong as ambulance worker
* Lee Seung-joon as police officer at accident scene 
* Yang Myung-heon as captain at boundary line
* Andrew William Brand as Dr. Bill Backman
* Wayne W. Clark as American general

===English voice-over cast===
*Matthew Mercer as Kang Ji-goo
*Julie Ann Taylor as Kim In-hae
*Christine Marie Cabanos as Kim Mi-reu (or Mir)
*Derek Stephen Prince as Bae Kyung-ub
*Keith Silverstein as Jeon Gook-hwan
*Grant George as Byung-ki
*Stephen Mann as South Korean prime minister Kevin Delaney as South Korean president
*Erik Scott Kimerer as Byeong-woo

==References==
 

==External links==
* 
* 
* 
*  at SidusHQ  

 
 
 
 
 
 