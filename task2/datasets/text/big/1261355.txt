The King of Kings (1927 film)
{{Infobox film
| name = The King of Kings
| image = Kingofkings poster.gif
| image_size = 225px
| caption =
| imdb_rating =
| director = Cecil B. DeMille
| producer = Cecil B. DeMille
| writers = Jeannie Macpherson
| starring = H.B. Warner Dorothy Cumming Ernest Torrence Joseph Schildkraut James Neill
| music = Hugo Riesenfeld Josiah Zuro
| cinematography = J. Peverell Marley F.J. Westerberg
| editing = Anne Bauchens Harold McLernon
| distributor = Pathé|Pathé Exchange
| released =  
| runtime = 155 minutes
| country = United States
| language = Silent film English language|English/Aramaic intertitles
| budget = gross = $1.5 million   accessed 19 April 2014 
}}
 silent epic crucifixion and stars H. B. Warner in the lead role. 
 resurrection scenes The Ten The Sign of the Cross (1932).

==Plot== Judas is Peter is Mark as a child who is healed by Jesus. Mary (mother of Jesus)|Mary, the mother of Jesus, is shown as a beautiful and saintly woman who is a mother to all her sons followers. Our first sight of Jesus is through the eyesight of a little girl, whom He heals. He is surrounded by a halo. Mary Magdelene arrives afterwards and talks to Judas, who reveals that he is only staying with Jesus in hopes of being made a king after Jesus becomes the king of kings. Jesus casts the Seven Deadly Sins out of Mary Magdalene in a multiple exposure sequence.
 Lazarus and healing the little children. Some humor is derived when one girl asks if He can heal broken legs and He says yes, she gives him a legless doll. Jesus smiles and repairs the doll. The crucifixion is foreshadowed when Jesus, having helped a poor family, wanders through the fathers carpentry shop and, himself a carpenters son, briefly helps carve a piece of wood. When a sheet covering the object is removed, it is revealed to be a cross towering over Jesus.
 High Priest is also angry at Judas for having led people to a man whom he sees as a false prophet. Meanwhile Jesus drives away Satan who offers Him an Earthly kingdom, and he protects a woman caught in adultery. The words he draws in the sand are revealed to be the sins the accusers themselves committed.

Judas, desperate to save himself from Caiaphas, agrees to turn over Jesus. Noticeably at the Last Supper, when Jesus distributes the bread and wine saying that they are His body and blood, Judas refuses to eat. He puts the cup to his lips but refuses to drink; he tears off a piece of bread but lets it drop to the ground. Towards the end, Mary confronts her son and tells Him to flee the danger that is coming. Jesus replies that it must be done for the salvation of all peoples. They leave the room but the camera focuses on the table upon which a dove alights for a moment.

Jesus goes to the Garden of Gethsemane where He is soon captured by the Roman soldiers and betrayed by Judas. Judas life is saved, but on seeing that Jesus is going to be killed he is horrified. He takes a rope that the Romans had used to bind Jesus wrists and runs off. Jesus is beaten and then presented by Pontius Pilate to the crowd. Mary pleads for the life of her son and Mary Magdalene speaks for Him but Caiaphas bribes the crowd to shout against Jesus.
 mocked Jesus run in terror, and the veil covering the Holy of Holies in the Jerusalem Temple is torn in two.

The tumult ends when Mary looks up at heaven and asks God to forgive the world for the death of their son. The chaos ends and the sun shines. Jesus is taken down from the cross and is buried. On the third day, He rises from the dead as promised. To emphasize the importance of the resurrection, this scene from an otherwise black and white film is shot in color. Jesus goes to the Apostles and tells them to spread His message to the world. He tells them "I am with you always" as the scene shifts to a modern city to show that Jesus still watches over His followers.

Many of the films intertitles are quotes (or paraphrases) from Scripture, often with chapter and verse accompanying.

==Cast==
*H. B. Warner as Jesus
*Dorothy Cumming as Mary (mother of Jesus)|Mary, the mother of Jesus Peter
*Joseph Schildkraut as Judas Iscariot
*Jacqueline Logan as Mary Magdalene

Cast notes
*Sally Rand was an extra in the film, years before becoming notorious for her "fan dance" at the 1933 Worlds Fair. Frank OConnor on set.

==Production details==
  King Kong, Gone with the Wind (1939). Other sets and costumes were re-used for the 1965 Elvis Presley film, Harum Scarum.

The King of Kings was the first movie that premiered at the noted Graumans Chinese Theater in Los Angeles, California on May 18, 1927. 

The movie has two Technicolor sequences, the beginning and the resurrection scene, which use the two-strip process invented by Herbert Kalmus.

== Lawsuit ==
In 1928 actress Valeska Surratt and scholar Mirza Ahmad Sohrab sued DeMille for stealing the scenario for The King of Kings from them.  The case went to trial in February 1930 but eventually settled without publicity. http://www.tribstar.com/history/local_story_073225216.html  Surratt who had left films to return to the stage in 1917 appeared to be unofficially blacklisted after the suit. 

==See also== King of Kings (1961 film)
* List of early color feature films

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 