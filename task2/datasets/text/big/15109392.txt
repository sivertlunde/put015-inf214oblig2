A Terrible Beauty (film)
{{Infobox film
| name           = A Terrible Beauty (The Night Fighters)
| image          = ATerribleBeauty1960Poster.jpg
| caption        = US Poster
| director       = Tay Garnett
| producer       = Raymond Stross Robert Mitchum (uncredited) Arthur Roth (novel)
| screenplay     = 
| story          = 
| based on       =  
| starring       = Robert Mitchum Richard Harris Dan OHerlihy Anne Heywood Cyril Cusack
| music          = Cedric Thorpe Davie
| cinematography = Stephen Dade
| editing        = Peter Tanner
| studio         = 
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = UK / USA
| language       = English
| budget         = 
| gross          = 
}} Arthur Roth.  It was an international co-production between the United Kingdom and Robert Mitchums production company DRM.

==Plot== Second World War. Reaction to the news is mixed. His mother is strongly against it, while his father (Harry Brogan) is proud. His brother Ned (Niall MacGinnis) and sister Bella are ambivalent. Dermots girlfriend, Neeve Donnelly (Anne Heywood), breaks up with him, telling him the IRA will turn him into a murderer.

Dermot and his friend Sean Reilly (Harris) are chosen from their unit to participate in a raid on a British armoury to steal weapons and ammunition. Don McGinnis (Dan OHerlihy) is frustrated because, as commandant of the unit, he is too important to risk. The theft goes off without a hitch. 

However, their next attack, to destroy a guarded power plant in concert with a planned German invasion, results in bloodshed. To get away, Dermot shoots a soldier blocking the way out. Sean is wounded in the foot and Johnny Corrigan is killed. Dermot and Sean evade their pursuers and manage to cross the border to safety in Ireland. Dermot returns home, leaving his friend to recuperate.

Despite Dermots advice to stay away, Sean tries to sneak back across the border and is captured by the police. Dermot wants to stage a rescue, but McGinnis turns him down. Sean is sentenced to ten years imprisonment. 

McGinnis decides to get revenge by attacking a police barracks. Dermot opposes this plan, as a policemans wife and children are living there, and warns that he will tell the authorities if McGinnis does not change his mind. When the commandant refuses to back down, Dermot tells McGinnis he is quitting the IRA. He is beaten up, but fortunately, a police patrol comes upon the scene before the IRA members can do anything more drastic. Dermot carries through on his threat, telling Sergeant Crawley, though without naming names. He is abducted to stand trial as an informant.

Bella becomes concerned when her brother does not come home. She goes to Neeve. The two then consult Dermots good friend, cobbler Jimmy Hannafin (Cyril Cusack). Jimmy has a pretty good idea what has happened. He gets Ned to help in the rescue. Neeve refuses to be left behind, but Bella is sent home to reassure her parents.
Once they find and free Dermot, guarded only by youngster Quinn, Jimmy arranges for a friend to give Dermot a ride to Belfast, where he can leave the country. Neeve goes with him. 

Meanwhile, the IRA men start searching for him. McGinnis stations himself at the ONeill home. In the darkness and driving rain, he mistakes the returning Bella (wearing Dermots coat) for the fugitive and shoots her dead. He is horrified to discover that he has killed the woman he loves.

==Cast==
* Robert Mitchum as Dermot ONeill
* Richard Harris as Sean Reilly
* Anne Heywood as Neeve Donnelly
* Dan OHerlihy as Don McGinnis
* Cyril Cusack as Jimmy Hannafin
* Niall MacGinnis as Ned ONeill
* Marianne Benet as Bella ONeill
* Christopher Rhodes as Tim Malone
* Harry Brogan as Patrick ONeill
* Eileen Crowe as Mrs. Kathleen ONeill Joe Lynch as Seamus
* Marie Kean as Mrs. Matia Devlin
* Geoffrey Golden as Sergeant Crawley
* Edward Golden as Johnny Corrigan
* Wilfred Downing as Quinn

==References==
 

==External links==
*  at The New York Times
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 