Eklavya: The Royal Guard
 
 
 
 
{{Infobox film
| name             = Eklavya: The Royal Guard
| image            = Eklavya.jpg
| caption          = Theatrical release poster
| director         = Vidhu Vinod Chopra
| producer         = Vidhu Vinod Chopra Abhijat Joshi
| screenplay       = Abhijat Joshi Vidhu Vinod Chopra
| starring         = Amitabh Bachchan Sharmila Tagore Sanjay Dutt Saif Ali Khan Vidya Balan Raima Sen Jackie Shroff Jimmy Shergill Boman Irani
| music            = Shantanu Moitra
| cinematography   = Natarajan Subramaniam
| editing          = Rabiranjan Maitra
| associate editor = Rachel Reuben
| distributor      = Vinod Chopra Productions
| released         = 16 February 2007
| runtime          = 138 min
| country          = India
| language         = Hindi
| budget           =     
| gross            =  
}}
Eklavya: The Royal Guard is a 2007 Bollywood drama film directed by Vidhu Vinod Chopra which was released in India, Netherlands, the United States, and the United Kingdom on 16 February 2007.    It was an average at Box office   

It stars Amitabh Bachchan, Saif Ali Khan, Sharmila Tagore, Sanjay Dutt, Vidya Balan, Raima Sen, Jackie Shroff, Jimmy Shergill and Boman Irani. The film marks Vidhu Vinod Chopras return to directing after seven years.
 Best Foreign Film category for the year 2007.   

==Plot==
Contemporary India. A majestic fort. A royal dynasty that no longer rules. A king without a kingdom. Yet Eklavya (Amitabh Bachchan), their royal guard, lives in a time warp. He lives only to protect the fort, the dynasty, and the king. For nine generations Eklavyas family has protected Devigarh, a centuries old citadel in Rajasthan. His marksmanship is the stuff of legends. His unflinching loyalty inspires ballads. Eklavya has spent his entire life serving the royals and closely guarding their secrets but now hes getting old and increasingly blind. Unable to cope with the suffocating customs of his land, the heir, Prince Harshwardhan (Saif Ali Khan), has stayed away in London. But the sudden demise of the queen, Rani Suhasinidevi (Sharmila Tagore), forces the Prince back to the kingdom he had left behind. The queen leaves a letter for her son in which she tells him that his biological father is actually Eklavya.

The Princes return brings a rush of joy into the moribund fort. His mentally challenged twin sister, Princess Nandini (Raima Sen), and his childhood love, Rajjo (Vidya Balan), are delighted to see him. But the joy of reunion is short-lived.

There is unrest in the kingdom: Farmers are being stripped of their lands. The king, Rana Jaywardhan (Boman Irani), influenced by his brother, Rana Jyotiwardhan (Jackie Shroff), supports the atrocities being forced upon the helpless peasants. The king receives a death threat over the phone. An irreverent police officer, Pannalal Chohar (Sanjay Dutt), is called in to investigate. But he might be too late. The fragile peace of the land is suddenly shattered by a barrage of bullets. Jaywardhan instructs his brother to kill Eklavya in a fit of rage but his brother betrays him and kills him and his driver (Rajjos father). And amidst the mayhem, the safely guarded secrets of the fort are revealed.

Eklavya suspects Jyotiwardhan and his son, Udaywardhan (Jimmy Shergill), are responsible for Jaywardhans death. He kills Udaywardhan and leads Jyotiwardhan to Udaywardhans body, intending to kill him and fulfill his oath, whereupon Jyotiwardhan reveals to Eklavya that it was the prince who ordered the murder of the king. Shouting in denial, Eklavya kills Jyotiwardhan, knowing he must face the young Prince, his own son, in order to finally fulfill his dharma.

Harshwardhan, overcome with guilt, reveals his own hand in the murder of Jaywardhan to Rajjo, who leaves him because his actions also caused the death of her father. When Eklavya comes to the palace to kill the Prince, he explains why he killed the king. The King had murdered the Queen when she, in her semi-conscious state, kept saying Eklavyas name. Eklavya finally fulfills his dharma, by sparing his sons life and declaring the original Eklavya wrong. Rajjo eventually forgives Harshwardhan because she believes that he is truly sorry for his actions and Pannalal finds a suicide note saying that Udaywardhan and Jyotiwardhan, afraid that they were about to be caught, jumped in front of a train.

==Cast==
* Amitabh Bachchan as Eklavya
* Sanjay Dutt as Inspector Pannalal Chohar
* Jackie Shroff as Rana Jyotiwardhan
* Saif Ali Khan as Prince Harshvardhan
* Vidya Balan as Rajjo
* Sharmila Tagore as Rani Suhasinidevi
* Boman Irani as Rana Jayvardhan
* Jimmy Shergill as Prince Udaywardhan
* Raima Sen as Princess Nandini
* Parikshat Sahni as Rajjos father (Omkar) 
* Pankaj Jha as a peasant villager

== Production ==
Eklavya: The Royal Guard was originally titled Yagna.    Production began in early 2005. The principal shooting of the film was completed in November 2005.    The primary location for shooting was in Rajasthan, at Devigarh and in Udaipur. According to the official website, shooting took place inside the living quarters of the Jaipur Royal Family. Prince Raghavendra Rathore and Subarna Rai Chaudhari designed the costumes for the film.      Eklavyas cottage was built from scratch.

The action sequences were to take place in Egypt but were shot near Bikaner. 600 camels were used in the shoot.     

Shantanu Moitra, who was the music director of Parineeta (2005 film)|Parineeta (2005), composed the music for the film.    The global distributor for the film is Eros International.   

== Music ==
{{Infobox album |
 Name = Eklavya: The Royal Guard |
 Type = soundtrack |
 Artist = Shantanu Moitra |
 Cover = Eklavya Royal Guard soundtrack.jpg|
 Background = gainsboro |
 Released =  15 February 2007 (India) |
 Recorded = Unknown |

 Genre = Film soundtrack |
 Length = 26:26 |
 Label =   |
 Producer = Vinod Chopra Productions | 
 Reviews = |

 Last album = Parineeta (2005 film)|Parineeta  (2005)|
 This album = Eklavya: The Royal Guard  (2007)|
 Next album = Laaga Chunari Mein Daag (2007) |
}}
The soundtrack to Eklavya: The Royal Guard was released in 15 February 2007. 
{| class="wikitable"
|- bgcolor="#d1e4fd"sdfsdfsd
! # !! Title !! Singer !! Time
|-
| 1 || The Revelation || Pranab Biswas, Ravindra Sathe || 3:25
|-
| 2 || Chanda Re (The Moon Song)  || Hamsika Iyer || 4:36
|-
| 3 || The Gayatri Mantra Theme|| Ravindra Sathe || 2:27
|-
| 4 || Jaanu Na || Sonu Nigam, Swanand Kirkire || 4:29
|-
| 5 || The Killing || Sunidhi Chauhan, Pranab Biswas || 4:33
|- Pranab Biswas|| 4:28
|-
| 7 || The Love Theme|| Hamsika Iyer || 2:26
|-
| 8 || Suno Kahani (The Legend of Eklavya) || Swanand Kirkire || 3:22
|-
|}

== References ==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 