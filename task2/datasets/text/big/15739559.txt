The Third Eye (serial)
{{Infobox film
| name           = The Third Eye
| image          = Thethirdeye-1920-newspaperad.jpg
| caption        = Newspaper advertisement.
| director       = James W. Horne
| producer       = 
| writer         = H. H. Van Loan
| starring       = Warner Oland Eileen Percy
| music          = 
| cinematography = 
| editing        =  Astra Films
| released       =  
| runtime        = 15 episodes 
| country        = United States  Silent (English intertitles)
| budget         = 
}}

The Third Eye is a 1920 American film serial directed by James W. Horne. The film is considered to be lost film|lost.   

==Plot==
As described in a film publication,  Curtis Steele (Oland), a society man at a film studio, has been pursuing actress Rita Moreland (Percy) and confronts her at the studio with the intention of making love to her. She repulses him and during the struggle shoots him. Steele staggers forward and collapses. She is terrified as she thought that the revolver had been loaded with blanks. As she bends over him, he leaps to his feet and with a sneering remark leaves. Later that night Rita is informed that Steele was found at the studio shot through the heart, and that there is a film showing Steel chasing her and then her shooting him. The serial then develops around Rita, her sweetheart, a villain, and the mystery of who killed Steele, who made the film, and attempts to obtain the film.

==Cast==
* Warner Oland - Curtis Steele / Malcolm Graw
* Eileen Percy - Rita Moreland
* Jack Mower - Dick Keene
* Olga Grey - Zaida Savoy
* Mark Strong - Detective Gale

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 