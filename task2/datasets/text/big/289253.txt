Grease 2
{{Infobox film
| name = Grease 2
| image = Grease 2.jpg
| caption = Theatrical release poster
| director = Patricia Birch
| producer = Robert Stigwood Allan Carr
| writer = Ken Finkleman
| starring = {{plainlist|
* Maxwell Caulfield
* Michelle Pfeiffer
* Adrian Zmed
* Lorna Luft
* Didi Conn
* Eve Arden
* Sid Caesar
* Dody Goodman
* Tab Hunter
* Connie Stevens
}} 
| music = Louis St. Louis Frank Stanley
| editing = John F. Burnett
| studio = Paramount Pictures
| distributor = Paramount Pictures
| released =  
| runtime = 115 minutes
| country = United States
| language = English
| budget = $11.2 million
| gross = $15,171,476
}} musical of the same name by Jim Jacobs and Warren Casey. Grease&nbsp;2 was produced by Allan Carr and Robert Stigwood, and directed and choreographed by Patricia Birch, who also choreographed the first film. It takes place two years after the original film at Rydell High School, with an almost entirely new cast, led by actors Maxwell Caulfield and Michelle Pfeiffer.

==Plot==
It is 1961, two years after the original Grease film. The first day of school has arrived and the T-Birds and the Pink Ladies dance and sing as they enter the school. ("Back To School Again") The Pink Ladies are now led by Stephanie Zinone (Michelle Pfeiffer), who feels she has "outgrown" her relationship with the arrogant and rather immature leader of the T-Birds, Johnny Nogerelli (Adrian Zmed).
 the previous film), who quickly becomes smitten with Stephanie. At the local bowling alley, a game ("Score Tonight") turns sour due to the animosity between Johnny and Stephanie. Stephanie retaliates by kissing the next man who walks in the door, which happens to be Michael. Bemused by this unexpected kiss, Michael asks her out but learns that she has a very specific vision of her ideal man ("Cool Rider"). As he realizes that he will only win her affection if he turns himself into a cool rider, Michael accepts payment from the T-Birds to write papers for them, and uses the cash to buy a motorcycle.

Following an unusual biology lesson ("Reproduction") given by Mr. Stuart (Tab Hunter), a gang of rival motorcyclists called the Cycle Lords and led by Balmudo (Dennis C. Stewart) surprise the T-Birds at the bowling alley. Before the fight starts, a lone anonymous biker appears, who is Michael in disguise, defeats the enemy gang and disappears into the night ("Whos That Guy?"). Stephanie is fascinated with the stranger. The next evening, Stephanie is surprised again by the Cool Rider and they enjoy a romantic twilight motorcycle ride. Just as Michael is about to reveal his identity, they are interrupted by the arrival of the T-Birds and Pink Ladies; before Michael leaves, he tells Stephanie that he will see her at the talent show, in which the Pink Ladies and T-Birds are performing. Johnny, enraged by Stephanies new romance, threatens to fight the Cool Rider if he sees him with her again. The Pink Ladies walk away haughtily but this has little effect on the T-Birds self confidence ("Prowlin") .

At school, Stephanies poor grades in English lead her to accept Michaels offer of help. Johnny, upon seeing them together in a discussion, demands that Stephanie quit the Pink Ladies for his "rep". Although still head over heels for the Cool Rider, interactions with Michael reveal that she has become smitten with him as Michael ponders over his continuing charade he puts on for Stephanie ("Charades").

At the talent show, Stephanie and the Cool Rider meet but are ambushed by the T-Birds who pursue Michael with Stephanie and the Pink Ladies following in a car. They chase him to a construction site which conceals a deadly drop, and the bikers absence suggests that he has perished below, leaving Stephanie inconsolable. Johnny and his T-Birds remove the competing Preptones preppie boys by tying them to a shower pole in the boys locker room and drenching them. During the Pink Ladies number at the talent show ("A Girl For All Seasons"), Stephanie enters a dreamlike fantasy world where she is reunited with her mystery biker ("(Love Will) Turn Back The Hands Of Time"). She is named winner of the contest and crowned the queen of the upcoming graduation luau, with Johnny hailed as king for his performance of "Prowlin" with his fellow T-Birds.

The school year ends with the luau ("Rock-A-Hula Luau"), during which the Cycle Lords suddenly reappear and begin to destroy the celebration.  After the Cool Rider reappears and defeats the Cycle Lords again, he reveals himself to be Michael. After his initial shock, Johnny offers him a T-Bird jacket and welcomes him into the gang, and Stephanie accepts that she can be with him. All the couples pair off happily at the seniors graduation as the graduating class sings ("Well Be Together"). The credits start rolling in yearbook-style, as in the original film.

==Cast==
*   or John Travolta. However, when Grease 2 flopped, nobody would touch me. It felt like a bucket of cold water had been thrown in my face. It took me 10 years to get over Grease 2."   at movietome.com 

* Michelle Pfeiffer as Stephanie Zinone, the leader of the Pink Ladies. With only a few television roles and small film appearances, the 23-year-old Pfeiffer was an unknown actress when she attended the casting call audition for the role of Stephanie. Other better-known actresses up for the part included Lisa Hartman, Kristy McNichol, Andrea McArdle and singer Pat Benatar.  Pfeiffer was a wild card choice, but according to Patricia Birch, she won the part because she "has a quirky quality you dont expect."  Despite the disappointing reception of the film, Pfeiffers meteoric rise to the Hollywood A-list began the following year when she played Elvira Hancock in Scarface (1983 film)|Scarface.

;The T-Birds
* Adrian Zmed as Johnny Nogerelli
* Peter Frechette as Louis DiMucci
* Christopher McDonald as Goose McKenzie
* Leif Green as Davey Jaworski

;The Pink Ladies
* Lorna Luft as Paulette Rebchuck
* Maureen Teefy as Sharon Cooper
* Alison Price as Rhonda Ritter
* Pamela Segall as Dolores Rebchuck

;Returning from the original Grease (film)|Grease
* Didi Conn as Frenchy
* Eddie Deezen as Eugene Felsnic
* Eve Arden as Principal McGee (this was Ardens final film appearance before retiring from a five-decade career on stage and screen.  She made a few more television appearances before her death in 1990)
* Sid Caesar as Coach Calhoun
* Dody Goodman as Blanche Hodel
* Dennis C. Stewart as Balmudo (appeared in the first film as Leo)
* Dick Patterson as Mr. Spears (appeared in the first film as Mr. Rudie)

;Supporting cast
* Tab Hunter as Mr. Stuart
* Connie Stevens as Miss Yvette Mason Jean and Liz Sagal as Twin Cheerleaders who perform the song "Brad"
* Matt Lattanzi as Brad
* Lucinda Dickey as Girl Greaser

==Production==

===Development===
Grease co-producer Allan Carr had a deal with Paramount Pictures to be paid $5 million to produce a Grease sequel, with production beginning within three years of the original film.  Carr decided to hire Patricia Birch as director for the sequel, as she had previously served as the choreographer for the stage and film versions of Grease.  Birch was initially hesitant to accept after learning that neither composers Jim Jacobs and Warren Casey nor John Travolta and Olivia Newton-John would be involved in film.   The total budget for the production was $11.2 million, almost double the budget of the original. 

Grease&nbsp;2 was intended to be the second film (and first sequel) in a proposed Grease franchise of four films and a   was unhappy with the films "drab" title, and unsuccessfully lobbied to change it to Son of Grease. 

===Casting===
Birch proposed an idea to feature Travolta and Newton-John reprising their characters as a now married couple running a gas station near the end of the film, which did not come to fruition.  Paramount tried to get Jeff Conaway and Stockard Channing from the first film to do cameos but this did not happen. 

Timothy Hutton was announced as a male star  but Maxwell Caulfield was signed after impressing producers on Broadway in Entertaining Mister Sloane. Lorna Luft was the last star cast.  The part played by Connie Stevens was originally meant for Annette Funicello but she was unable to appear as she was filming a peanut butter advertisement that week. 

===Shooting=== Excelsior High School, an abandoned school in Norwalk, California.  Filming took place throughout a 58-day shooting schedule.  According to director Patricia Birch, the script was still incomplete when filming commenced.  Sequences that were filmed but cut during post-production include scenes in which Frenchy helps Michael become a motorcycle rider, and a sequence at the end of the film showing Michael and Stephanie flying off into the sky on a motorcycle. 

In the film, after Stephanie wins the contest, it goes on to show the stakeout in the final scene. Originally, there were a few minutes dedicated to a scene in which Michael (believed to be dead in his alter ego, by Stephanie) comes out on stage as Stephanie is exiting the stage, unbeknownst to her that he is the cool rider and he is alive. He attempts to ask her whats wrong and she storms past him and runs off crying, then it cuts to the stakeout. There was a scene in the film within the "Whos that Guy?" number in which Goose (Christopher McDonald) accidentally smashes Rhondas (Alison Price) nose at the Bowl-A-Rama door. None of these scenes have been shown since the films release.

==Reception==
The sequel took in just over $15 million after coming at #5 on opening weekend behind  ,  , Rocky III and Poltergeist (1982 film)|Poltergeist.  Barry Diller of Paramount said that the film "on no level is as good as the first. The quality isnt there." Hollywood Sequels Are Just the Ticket: Superman VII? Maybe. But studios risk going to the well once too often. Hollywood Sequels: Just the Ticket
By LESLIE WAYNE. New York Times (1923-Current file)   18 July 1982: F1  Jim Jacobs described it as "awful... the pits." Playwright a hit at Taft High: Grease-er revisits scene of his teens Grease-er visits scene of his teens
Clifford, Terry. Chicago Tribune (1963-Current file)   12 Apr 1983: d1 
 the original Grease, but at half-speed." 
 Miss Newton-Johns sunniness was."  Variety (magazine)|Variety wrote that she was "all anyone could ask for in the looks department, and she fills Olivia Newton-Johns shoes and tight pants very well."  She was nominated for a 1983 Young Artist Award in the category of Best Young Motion Picture Actress. The movie was nominated for a Stinkers Bad Movie Awards for Worst Picture. 

==Remakes== Ravichandran and Juhi Chawla, released in 1987 which went on to become a blockbuster.

==Plans for a third film==

Paramount had intended to turn Grease into a multi-picture franchise with three sequels planned and a TV series down the road. When Grease 2 flopped at the box office, all the plans were scrapped. 

In 2008, it was reported that Paramount was planning a new sequel to Grease that would debut straight to DVD. 

==Soundtrack==
 
{{Infobox album 
| Name        = Grease 2
| Type        = Soundtrack |
| Artist      =
| Cover       =
| Released    = 1982
| Recorded    = 1981
| Genre       =
| Length      = 32:28 RSO
| Producer    =
| Chronology  =
| Last album  =   (1978) |
| This album  =
| Next album  =
}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =    
| noprose = yes
}}
# "Back to School Again" – Cast, The Four Tops
# "Score Tonight" – Cast
# "Brad" – Sorority Girls
# "Cool Rider" – Stephanie
# "Reproduction" – Mr. Stuart, Cast
# "Whos That Guy?" – Michael, T-Birds, Pink Ladies, Cycle Lords, Cast
# "Do It for Our Country" – Louis, Sharon (Sharons part is absent from the soundtrack)
# "Prowlin" – Johnny, T-Birds
# "Charades" – Michael
# "Girl for All Seasons" – Sharon, Paulette, Rhonda, Stephanie
# "(Love Will) Turn Back the Hands of Time" – Stephanie, Michael
# "Rock-a-Hula Luau (Summer Is Coming)" – Cast
# "Well Be Together" – Michael, Stephanie, Johnny, Paulette, Cast

Featured as background music at Rydell Sport Field:
# "Moon River" (Military Band)

Featured as background music at the bowling alley:
# "Our Day Will Come" – Ruby & The Romantics (Grease 2 takes place in 1961 and "Our Day Will Come" did not come out until 1963) Because Theyre Young")

Featured at the beginning:
# "Alma Mater" – Instrumental (this song was played at the beginning when Principal McGee and Blanche put up the 1961 Rydell flag)

==References==
;Footnotes
 

;Bibliography
 
* 
* 
 

==External links==
 
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 