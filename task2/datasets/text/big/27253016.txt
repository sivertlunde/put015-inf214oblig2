Admirals All
 {{Infobox film
| name = Admirals All
| image =
| caption =
| director = Victor Hanbury
| producer = John Stafford
| writer = Ian Hay (play) Stephen King-Hall
| screenplay =
| story =
| based on =   George Curzon
| music = Jack Beaver
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| runtime = 75 minutes
| country = United Kingdom
| language = English
| budget =
| gross =
}}
 George Curzon. It was based on a play by Ian Hay. 

==Synopsis==
A temperamental female film star (Gibson) arrives in China to film her next movie, but becomes inadvertently involved with local bandits.

==Cast==
* Wynne Gibson - Gloria Gunn
* Gordon Harker - Petty Officer Dingle
* Anthony Bushell - Flag Lieutenant Steve Langham George Curzon - Pang Hi
* Joan White - Prudence Stallybrass
* Henry Hewitt - Flag Captain Knox
* Percy Walsh - Admiral Westerham
* Wilfrid Hyde-White - Mr Stallybrass
* Gwyneth Lloyd - Jean Stallybrass
* Ben Welden - Adolph Klotz

==See also==
*O.H.M.S. (film)|O.H.M.S.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 