The Wedding Date
{{Infobox film
| name           = The Wedding Date
| image          = The_Wedding_Date_poster.JPG
| border         = yes
| caption        = Theatrical release poster
| director       = Clare Kilner
| writer         = Dana Fox
| based on       =  
| producer       = {{Plainlist|
* Jessica Bendinger
* Paul Brooks
* Michelle Chydzik
* Nathalie Marciano
}}
| starring       = {{Plainlist|
* Debra Messing
* Dermot Mulroney Amy Adams
}}
| music          = Blake Neely
| cinematography = Oliver Curtis
| editing        = Mary Finlay
| studio         = Gold Circle Films
| distributor    = Universal Studios
| released       =  
| runtime        = 90 minutes
| language       = English
| country        = United States
| budget         = $15 million
| gross          = $47,175,038
}} Amy Adams. Elizabeth Young, Aap Ki Khatir which also performed well at box office.

==Plot==
Kat Ellis (Debra Messing) is a single New Yorker who returns to her parents house in London to be the maid of honor at her younger half sisters (Amy Adams) wedding. The best man is none other than her former fiancé, who unexpectedly dumped her two years ago.  Anxious about confronting him and eager to impress him, she hires suave escort Nick Mercer (Dermot Mulroney) to pose as her boyfriend.

Kat intends to make her former flame, Jeffrey (Jeremy Sheffield), jealous, but her plan backfires when Nick convinces everyone, including her, that they are madly in love. Kat then feels herself, too, falling for Nick as he slowly falls for her. The night before the wedding, Kat discovers Amy slept with Jeffrey when they were still together, and that Jeffrey dumped Kat because he believed he was in love with Amy. Nick had discovered this fact a day earlier, and when Kat finds that out, she feels betrayed from all sides, and puts Nick off. He decides to return to America, and leaves Kat the money she had paid him.

On the wedding day, seeing Kat distressed, her step-father (Peter Egan) asks Kat if Nick is the guy for you, and Kat realizes he is, so she sets off to find him. Meanwhile, just before the wedding, Amy confesses her betrayal to her fiancé, Ed (Jack Davenport), but professes her love for him. Ed, upset, chases Jeffrey out of the church and down the road. Jeffery in distress of the chase, said he gave up on Amy and believes hes done nothing wrong. To which Ed, calls him a "back-stabbing weasel", though Jeffery believes hes still done nothing wrong because he slept with Amy before they dated. Ed shouts out that he was engaged to Kat, proving he was still in the wrong for what he did to Kat. Nick, driving away, picks up Ed as Jeffrey disappears into the woods.

Nick and Ed talk about love, and Ed decides he loves Amy more than he is angry. To make it more clear that he should go back, Nick tells Ed if he went back the couple would end up having great make-up sex. To which, helps urge him more to return to the church, so they end up getting married, with Nick as new best man. Just before the ceremony, Nick tells Kat he realized hed "... rather fight with you than make love with anyone else", and they kiss passionately. Kat and Nick begin a real relationship together. Amy and Kat now reconcile and Kat lets go of her anger and forgives Amy since she confessed the truth to Ed. TJ, Kats cousin also apparently enjoys a moment with Woody after the wedding.  Jeffrey, the main cause of all the trouble, learns absolutely nothing. At the end he is seen trying to get the attentions of a female neighbor.

==Cast==
* Debra Messing as Kat Ellis
* Dermot Mulroney as Nick Mercer
* Amy Adams as Amy Ellis
* Jeremy Sheffield as Jeffrey
* Jack Davenport as Edward Fletcher-Wooten
* Sarah Parish as TJ, Kats cousin
* Peter Egan as Victor Ellis, Kats step-father
* Holland Taylor as Bunny, Kats mother
* Jolyon James as Woody
* C. Gerod Harris as Bike Messenger
* Martin Barrett as Teenager Jay Simon as Male Flight Attendant
* Danielle Lewis as Pretty Woman
* Ivana Horvat as Smitten Girl
* Linda Dobell as Sonja

==Soundtrack==
* "Serenade for Lovers" by John Arkell and Ray Charles 
* "Moonlight Waltz" by John Arkell
* "Grosvenor House" by Terry Day Forever Young" Alphaville
* "Kings Road" by Michael Melvoin
* "The Lavender Room" by Dick Walter
* "Lovedance" by Dave Rogers and Paul Shaw
* "Breathless (Corrs song)|Breathless" by The Corrs
* "When We Are Together" by Texas
* "All Out of Love" by Air Supply One Fine Day" by The Chiffons
* "Boogie Shoes" by KC & The Sunshine Band
* "I Want You to Dance with Me" by Amy Ward
* "I Got the Feeling" by James Brown
* "Secret" by Maroon 5
* "Sway (song)|Sway" by Michael Bublé
* "Home (Michael Bublé song)|Home" by Michael Bublé
* "Save the Last Dance for Me" by Michael Bublé
* "Friends & Family" by Trik Turner
* "Moon River" by Andy Williams
* "Lets Go to Bed (The Cure song)|Lets Go to Bed" by The Cure Super Duper Love" by Joss Stone

==Locations==
Some outdoor scenes where they are playing rounders were filmed on location in Parliament Hill Fields, overlooking west London. Another part of the film is set in Shere, Surrey.

==Reception==
The film garnered almost universally negative reviews, receiving only 10% positive reviews on Rotten Tomatoes and a score of 32 on Metacritic.  

The film was reasonably a financial success, grossing about three times its budget.

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 