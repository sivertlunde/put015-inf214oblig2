Count Your Change
 
{{Infobox film
| name           = Count Your Change
| image          = 
| caption        = 
| director       = Alfred J. Goulding
| producer       = Hal Roach
| writer         = H.M. Walker
| starring       = Harold Lloyd
| music          = 
| cinematography = Walter Lundin
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  Silent English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd.

==Cast==
* Harold Lloyd - the boy
* Snub Pollard - Billy Bullion (as Harry Pollard)
* Bebe Daniels - Miss Flighty
* Sammy Brooks
* Lige Conley - (as Lige Cromley)
* Wallace Howe
* Bud Jamison
* Dee Lampton
* Marie Mosquini
* Fred C. Newmeyer - (as Fred Newmeyer)
* James Parrott

==See also==
* List of American films of 1919
* Harold Lloyd filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 