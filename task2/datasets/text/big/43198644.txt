Miss, Please Be Patient
{{multiple issues|
 
 
}}
{{Infobox film
| name = Miss, Please Be Patient
| film name =  
| director = Lee Hyung Pyo
| starring = Kim Tai-chung  Jeong Yun-hui
| released =  
| runtime = 105 minutes 
| distributor = Dong A Exports Ltd
| language = Korean
}}

Miss, Please Be Patient is a 1981 South Korean romantic comedy action film starring Kim Tai-chung and Jeong Yun-hui. The movie is also known as Super Lady Hopper as an alternate title. The movie is apparently Kim Tai-chungs rarest movie appearance in South Korea. It was directed by Lee Hyung Pyo and released domestically in 1981, but the international release was cancelled.

==Background==
The movie was only screened in South Korea in 1981 and after that the movie was considered to be lost except for a few VHS copies. The movie was not revealed to an international audience until 2011, when the movie was rediscovered by Houndslow Team, a group of martial arts fans who are tracking down rare kung fu action movies and releasing them on DVD.

==Plot==

Mr. Jun, the head of a Taiwanese shipbuilding company, was murdered by Pang, who is vice-president of the company. Pang decided to take over the company, but his plan did not progress smoothly because Juns daughter Li-Hwa (Jeong Yun-hui) inherits his fortune.
 triads so that they can force Li-Hwa to sign the inherited contract.

When Li-Hwa is kidnapped, Hyun-Hee decided to rescue her and get help from the mysterious man Kim Min-wuk (Kim Tai-chung) who had fell in love to Li-Hwa. Hyun-Hee and Min-wuk rescue Li-Hwa safely at the end.

==Cast==
*Kim Tai-chung as Kim Min-wuk
*Jeong Yun-hui as Li-Hwa
*Seo Yeong-ran as Hyun-Hee
*Kwon Yeong-mun as Pang

==Media release==
 stomach hemorrhage. The movie was released only few copy of VHS tapes in 1981. In 2011, Houndslow Team released the movie on DVD in a fully remastered version with English subtitles.

==External links==
*http://www.clonesofbrucelee.co.uk/pleasemiss.html

 
 
 


 