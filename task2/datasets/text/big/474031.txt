Laurel Canyon (film)
{{Infobox film
| name = Laurel Canyon
| image = Laurel_canyon_poster.jpg
| image_size = 215px
| alt =
| caption = Theatrical release poster
| director = Lisa Cholodenko
| producer = Jeff Levy-Hinte Susan A. Stover
| writer = Lisa Cholodenko
| starring = Frances McDormand Christian Bale Kate Beckinsale Natascha McElhone Alessandro Nivola
| music =  , music by Henri Betti (1947)
| cinematography = Wally Pfister
| editing = Amy E. Duddleston
| studio = Antidote Films
| distributor = Sony Pictures Classics
| released =  
| runtime = 101 minutes
| country = United States
| language = English
| budget =
| gross = $4,412,203
}}
Laurel Canyon is a 2002 American drama film written and directed by Lisa Cholodenko. The film stars Frances McDormand, Christian Bale, Kate Beckinsale, Natascha McElhone, and Alessandro Nivola.

==Plot== Los Angeles Laurel Canyon section of the City of Los Angeles. 

In a change of plans, however, Jane is still around, recording an album with her boyfriend, Ian McKnight (Alessandro Nivola), and his band. The film focuses in some depth on the challenge of trying to create successful pop music, showing work on two tracks (both actually written, and previously released, by the band Sparklehorse). Ians bandmates are played by noted indie rockers, bassist Lou Barlow and guitarist Imaad Wasif. 

Jane and Ian are in the midst of a fiery romantic love|romance, and both the producer and the band seem more interested in partying than finishing the record. Janes presence is a source of consternation for Sam, as he and his mother have a somewhat strained relationship, due to their very different mindsets. 

Alex, however, is intrigued by the new lifestyle options presented by her soon-to-be mother-in-law. Normally hardworking, Alex begins spending more time with the band and less time working on her dissertation. Her growing fascination with Jane and Ian leads to a scene where the three of them kiss one another while naked in the swimming pool.

Meanwhile, Sam finds himself attracted to fellow resident Sara (Natascha McElhone), who is unapologetically interested in him as well. They share one first kiss while returning from an informal interns meeting, around the same time Alex has her first tryst with Jane and Ian in the pool. Some time later, while Alex attends Jane and Ians party held in a crowded hotel suite to celebrate the bands new album release, Sam and Sara meet in a parking lot and, in a conversation filled with sexual tension, they declare their attraction for one another. 

The situation strains Sam and Alexs relationship almost to the point of breaking by the end of the film. After the party has finished and the three of them are left alone in the suite, Ian tries to "finish" (in his words) his encounter with Alex and Jane, but the latter decides against it and the threesome does not take place. Upon returning home after his conversation with Sara, Sam decides to go to the hotel and discovers Jane, Ian, and Alex scantily-clad in the bedroom. In a fit of rage he repeatedly punches Ian, hits his mother with the elbow as she tries to split the fight, and leaves the hotel, but Alex chases him down the street and professes her love for him. 

The next morning, the situation seems back to normal again. But Sara phones Sam and tells him she cant control her heart, as opposed to what he told her the day before. Sam watches his surroundings, postpones any further conversation, and takes a moment of reflection. Credits run.

==Cast==
* Frances McDormand as Jane
* Christian Bale as Sam
* Kate Beckinsale as Alex
* Natascha McElhone as Sara
* Alessandro Nivola as Ian McKnight
* Lou Barlow as Frip
* Imaad Wasif as Dean
* Russell Pollard as Rowan
* Mickey Petralia as Mickey
* Melissa De Sousa as Claudia

==Background==
Lisa Cholodenko stated that the film was inspired by the Joni Mitchell album Ladies of the Canyon.   

  }}

The script was workshopped at Sundance Institutes lab.   

==Music==
The music of Laurel Canyon is intrinsically woven into the film. Analogous to the still photographs in Cholodenkos prior film, High Art, it was critically important that the “behind the scenes” music being created by Jane, Ian and the Band was credible to viewers as the work of major label artists. 

Ians backing band in the film is portrayed by  , the creative force behind the band Sparklehorse.  Linkous also appears in the film in an impromptu jam session at the Chateau Marmont, playing an improvised song with producer Daniel Lanois (whose credits include U2 and Bob Dylan) and Becks bass player Justin Meldal Johnsen. To maximize authenticity, it was decided to use working musicians to play Ians band on camera. Lou Barlow and his Folk Implosion bandmates portrayed the band: singer/songwriter Barlow (playing Fripp on bass), Russ Pollard (Rowan on drums) and Imaad Wasif (Dean on guitar.)  

Nivola sings on all of the on-camera tracks seen in the film, with backing both on-camera and in the studio pre-recordings by Folk Implosion. The songs were recorded over a four-day period at Sunset Sound in Hollywood, produced by Mickey Petralia, a veteran LA record producer. Karyn Rachtman was Music Supervisor. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 