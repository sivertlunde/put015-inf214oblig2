The Tricky Game of Love
{{Infobox film
| name           = The Tricky Game of Love
| image          = Hry lasky salive.JPG
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| film name      = {{Infobox name module
| original       = Hry lásky šálivé}}
| director       = Jiří Krejčík
| producer       = Ladislav Hanuš
| based on = {{plain list|
*The Decameron 
*Náušnice 
}}
| writer         = {{plain list|
*Giovanni Boccaccio
*Marguerite de Navarre|Markéta Navarrská
}}
| screenplay = Jiří Krejčík
| narrator       = 
| starring       = {{plain list|
*"Arabský kůň" 
*Božidara Turzonovová
*"Náušnice" 
*Jiří Sovák
}}
| music          = Zdeněk Liška
| cinematography = Jaromír Šofr
| editing        = Josef Dobřichovský
| studio         = 
| distributor    = Ústřední půjčovna filmů
| released       =  
| runtime        = 108 minutes Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}} Czech comedy film directed by Jiří Krejčík. Consisted of two sequences, the work was released on October 22, 1971 in Czechoslovak Socialist Republic|Czechoslovakia.

==Cast==
;Giovanni Boccaccio - "Arabský kůň"
* Božidara Turzonovová - Sandra Vergellesi 
* Miloš Kopecký - Francesco Vergellesi
* Jozef Adamovič - Ricciardo Minutolo 
* Josef Chvalina - nobleman 
* Jan Přeučil - messenger
* Oldřich Velen - nobleman 
* Jaroslav Blažek - nobleman  
* Radko Chromek - jeweler
* Ilona Jirotková - Bianca, chambermaid 
* Gustav Opočenský - fashioner
* Jan Trávníček - servant
* Jiří Vašků - nobleman  
* Luděk Munzar - narrator  

;Markéta Navarrská - "Náušnice"
* Jiří Sovák - The Count
* Slávka Budínová - The Countess Magda Vašáryová - chambermaid 
* Pavel Landovský - servant

==References==
;General
* 
* 
 

==External links==
*  

 
 
 
 
 
 

 
 