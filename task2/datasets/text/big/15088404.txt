Kingdom of Crooked Mirrors
 
{{Infobox film
| name           = Kingdom of Crooked Mirrors
| image          = Korolevstvo Krivykh Zerkal poster.jpg
| caption        = Soviet billboard theatrical poster of the film Nushrok, Abazh and Anidag trio (top) Olya and Yalo (bottom)
| director       = Aleksandr Rou
| producer       = 
| writer         = Lev Arkadyev Vitali Gubarev
| starring       = Olga Yukina Tatyana Yukina Andrei Fajt Arkadi Tsinman Lidiya Vertinskaya
| music          = Arkady Filippenko
| cinematography = 
| editing        = 
| distributor    = Gorky Film Studio
| released       = 1963
| runtime        = 80 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}} Kingdom of Crooked Mirrors by Vitali Gubarev.
 musical remake with the same name, featuring stars of Russian scene Nikolay Baskov, Alla Pugacheva, and The Tolmachevy Sisters.  The original film contains introduction music and a fairytale style of the early 1960s.

The film was also behind the inspiration for a restaurant in Moscow to only hire twins as waiting staff. 

== Plot summary ==
 
Similar in subject to and perhaps inspired by the novel  ) produces crooked mirrors that brainwash its people through subtle changes in reality.  When Yalos friend, a man named Gurd, is suddenly imprisoned for refusing to make crooked mirrors by the evil leaders "Anidag" (reverse of Gadina, meaning snake), "Nushrok" (reverse of Korshun, meaning Kite (bird)) and "Abazh" (reverse of Zhaba, meaning toad), Olya decides to accompany Yalo to rescue him.
 happily ever after with her grandmother.
 Soviet propaganda machine during the cold war.

== Cast ==
* Olga Yukina as Olya
* Tatyana Yukina as Yalo
* Tatyana Barysheva as Grandmother
* Anatoli Kubatsky as Jagupop 77
* Andrei Fajt as Nushrok
* Lidiya Vertinskaya as Anidag
* Arkadi Tsinman as Abag
* Andrei Stapran as Gurd Ivan Kuznetsov
* Georgi Millyar
* Pavel Pavlenko as Minister
* Tamara Nosova as Aunt Aksal
* Vera Altayskaya as Asirk
* Alexander Khvylya
* Valentin Bryleyev

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 