Emmanuelle 6
{{Infobox film
|image=Emmanuelle-6-poster.jpg 
|caption=Original film poster
|director=Bruno Zincone Jean Rollin   (uncredited)     
|writer=Jean Rollin Emmanuelle Arsan (novel)     
|producer=Roger Corman  Alain Siritzky George Korda (associate producer)  
|starring=Natalie Uher
|music=Olivier Day    
|cinematography=Serge Godet Max Monteillet    
|editing=Michel Crivellaro 
|country=France
|runtime=80 minutes
|released=July 6, 1988 

}}

Emmanuelle 6 is a  . The filming location was Venezuela. Although hardcore scenes were filmed to make this remake of the original 1974 version more edgy, they were not used in any known available cuts except for the French VHS version, which is 10 minutes longer. 

==Plot==
Emmanuelle lives in Bangkok with her husband Jean, who is several years older. She likes him because hes taught her much sexually, and he likes her because she is very eager to learn. Although both have had extramarital affairs, they tolerate it as they are content with their arrangement.

A young woman named Marie-Ange comes to the house to visit with Emmanuelle, and she wants more than talk from her, but Emmanuelle herself is magnetically drawn to an older man named Bee, and one day he invites her to join him "in the jungle," to which she does.

==Release==
It was released in July 1988 in France, but due to its dwindling popularity (despite the name) and the rise of more conventional Hardcore pornography|hard-core pornography available for home viewing, the film made very little money and was never released worldwide except briefly in Spain and Germany.

==Cast==
*Natalie Uher as Emmanuelle
*Jean-René Gossart as  Professor Simon
*Thomas Obermuller as  Benton
*Gustavo Rodríguez as  Tony Harrison
*Hassan Guerrar as  Carlos
*Luis Carlos Mendes as  Morales
*Tamira as  Uma

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 


 
 