Udaya Geetham
{{Infobox film
| name           = Udhaya Geetham
| image          = Udhaya geetham.jpg
| caption        = Official VCD Cover
| director       = K. Rangaraj
| writer         = M. G. Vallabhan
| screenplay     = K. Rangaraj
| story          = R. Selvaraj
| starring       =  
| producer       = Kovai Thambi
| music          = Ilaiyaraaja
| cinematography = Dinesh Babu
| editing        = R. Bhaskaran
| studio         = Motherland Pictures
| distributor    = Motherland Pictures
| released       =   
| country        = India
| language       = Tamil
| gross          = 
}}
 Lakshmi and Revathi in the lead, while Goundamani and Senthil appear in the comic subplot. The films widely acclaimed soundtrack was composed by Ilaiyaraaja. Udhaya Geetham tells the story of a successful stage singer who is in jail for a murder he did not commit, and a young girl who believes his story, and promises to free him from prison. The film was critically acclaimed and commercially successful upon release.

==Plot==
Arun (Mohan (actor)|Mohan), a prominent singer is in jail with a death sentence looming over his head. He meets Shanthi (Revathi) during one of his concerts, and is followed by her on many occasions into the jail and outside too. He shows nothing but animosity towards her behavior, and keeps pushing her out of sight. But Shanthi is persistent and does not give up either. An inmate named Maari attempts to escape from jail, but he is stopped by Arun. Maari later tells Arun about his sick daughter (Shekhina Shawn), and his inability to pay for her operation. Arun sympathises with him, and promises to raise funds for the sick child through his next concert, which he also plans shall be his final one.

Leela Chowdhry (Lakshmi (actress)|Lakshmi) is the newly appointed Superintendent of Police for the Madras Central Jail and is merciless. However, Shanthi with her smartness talks Leela into getting her married to Arun. Arun also agrees to it after a lot of thought and deliberation. Leela arranges for the wedding to take place in her home. During their first night, Shanthi stabs Aruns stomach and explains that she is the sister of Anand (Anand Babu), whom Arun was accused of killing. Arun and Anand sang and danced for a music troupe before Anands murder. Arun explains his side of the story in pain and proves his innocence to her. Shanthi, realising her mistake, now starts to set things right.

Leela allows Shanthi and Arun to go to a nearby temple, but sends policemen along with them to prevent Arun from escaping. Shanthi however outsmarts them and takes Arun to her home where she traps him, much to his dislike. She makes it impossible for him to escape, until she is able to prove his innocence. Leela is later punished for having let Arun "escape", and simultaneously realizes that Arun is her long lost son, becoming heartbroken. However, she remains bound to her duty of capturing Arun, and a manhunt for him begins.

Shanthi later comes across Shankar (Prathapachandran), who is the real killer of Anand. He accepts the truth but states that he killed Anand because he witnessed the latter sleeping with his wife (J. Lalitha). Shanthi, knowing that Shankars wife is a women who has affair with multiple men (it was she who seduced the reluctant Anand), takes him to her and Shankar is angered on seeing his wife with another man in bed. He kills her, but is fatally stabbed during the fight. Arun, now having escaped from custody, is confronted by Leela who he realises is his mother. She allows him to perform his final concert which he had earlier planned to raise funds for Maaris sick daughter, but has to have him hanged to death him after that. Right after he finishes his concert he is arrested, but Shanthi and Shankar finally arrive and tell the truth about Anands death, after which Shankar succumbs to his pain. Aruns death sentence is cancelled, and he returns to his normal life.

==Cast==
* Mohan (actor)|Mohan as Arun
* Lakshmi (actress)|Lakshmi as Leela Chowdhry
* Revathi as Shanthi
* Goundamani
* Senthil (actor)|Senthil
* Anand Babu as Anand
* Prathapachandran as Shankar
* J. Lalitha as Shankars wife
* Shekhina Shawn as the sick child 

==Soundtrack==
The films original soundtrack was composed by Ilaiyaraaja.  

{{Track listing
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    =
| all_lyrics      = 
| lyrics_credits  = yes
| title1 = Sangeetha Megam
| extra1 = S. P. Balasubrahmanyam
| length1 = 4:28
| lyrics1 = Muthulingam
| title2 = Paadu Nilavae
| extra2 = S.P. Balasubrahmanyam, S. Janaki
| length2 = 5:02
| lyrics2 = Mu. Metha
| title3  = Ennodu Paattu Paadungal
| extra3  = S.P. Balasubrahmanyam
| length3 = 5:58
| lyrics3 = M. G. Vallabhan
| title4 = Thene Thenpaandi
| extra4 = S.P. Balasubrahmanyam
| length4 = 4:12
| lyrics4 = Vaali
| title5 = Uthaya Geetham
| extra5 = S.P. Balasubrahmanyam
| length5 = 4:49
| lyrics5 = Vairamuthu
| title6 = Thene Thenpandi (female)
| extra6 = S. Janaki
| length6 = 3:22
| lyrics6 = Vaali
| title7 = Maane Thene
| extra7 = S.P. Balasubrahmanyam, S. Janaki
| length7 = 4:19
| lyrics7 = N. Kama Rajan
}}

==Release==
===Reception===

===Box office===
The film ran for over 200 days in theatres, and was officially declared a hit. It was labelled a "Silver Jubilee" film and was a major breakthrough for Mohan, who later earned the nickname "silver jubilee hero", after acting in more "silver jubilee" films.  

==References==
 

==External links==
* 

 
 
 
 
 
 
 