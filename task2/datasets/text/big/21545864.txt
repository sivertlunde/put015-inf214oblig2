A Home for Tanya
 
{{Infobox film
| name           = A Home for Tanya (Отчий дом)
| image          = 
| alt            =  
| caption        = 
| director       = Lev Kulidzhanov
| producer       = 
| writer         = Budimir Metalnikov
| starring       = Vera Kuznetsova
| music          = 
| cinematography = Pyotr Katayev
| editing        = Lidiya Zhuchkova
| studio         = Gorky Film Studio
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}}

A Home for Tanya ( , Transliteration|translit.&nbsp;Otchiy dom) is a 1959 Soviet drama film directed by Lev Kulidzhanov. It competed for the Palme dOr at the 1959 Cannes Film Festival.   

==Cast==
* Vera Kuznetsova as Natalya Avdeyevna
* Lyudmila Marchenko as Tanya
* Valentin Zubkov as Sergei Ivanovich
* Nikolai Novlyansky as Grandfather Avdey (as N. Novlyansky)
* Nonna Mordyukova as Stepanida
* Lyusyena Ovchinnikova as Nyurka
* Pyotr Kiryutkin as Mokeich
* Pyotr Alejnikov as Fyodor
* Yelena Maksimova as Markarikha
* Yuri Arkhiptsev

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 