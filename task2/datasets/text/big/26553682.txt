Whity (film)
 
{{Infobox film
| name           = Whity
| image          = Whity (film).jpg
| caption        = DVD cover
| director       = Rainer Werner Fassbinder
| producer       = Peter Berling Peer Raben Ulli Lommel
| writer         = Rainer Werner Fassbinder
| starring       = Günther Kaufmann, Hanna Schygulla
| music          = Peer Raben
| cinematography = Michael Ballhaus
| editing        = Rainer Werner Fassbinder Thea Eymèsz
| distributor    =
| released       =  
| runtime        = 95 minutes
| country        = Germany
| language       = German English
| budget         =
}}
 Western film directed by Rainer Werner Fassbinder. Shot in Spain, it was entered into the 21st Berlin International Film Festival.    It was never distributed theatrically, but was eventually released on DVD.

==Cast==
* Günther Kaufmann as Whity 
* Hanna Schygulla as Hanna
* Katrin Schaake as Katherine Nicholson
* Harry Baer as Davy Nicholson
* Ulli Lommel as Frank Nicholson
* Tomas Blanco as Fake Mexican physician
* Stefano Capriati as Judge
* Elaine Baker as Marpessa, Whitys mother
* Mark Salvage as Sheriff
* Helga Ballhaus as Judges wife
* Ron Randell as Benjamin Nicholson

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 
 