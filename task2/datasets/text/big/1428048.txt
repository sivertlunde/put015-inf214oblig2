The Last of the Mohicans (1992 film)
{{Infobox film
| name = The Last of the Mohicans
| image = Mohicansposter.jpg
| caption = Theatrical Poster Michael Mann
| producer = Michael Mann Hunt Lowry
| screenplay = Michael Mann Christopher Crowe
| based on =    
| story = John Balderston Paul Perez Daniel Moore 
| starring = Daniel Day-Lewis Madeleine Stowe Jodhi May  Trevor Jones Randy Edelman
| cinematography = Dante Spinotti Arthur Schmidt
| studio = Morgan Creek Productions
| distributor = 20th Century Fox   Warner Bros.  
| released =  
| runtime  = 112 minutes   117 minutes   114 minutes   English French French Mohawk Mohawk Wyandot Huron
| country = United States
| budget = $40 million
| gross = $75,505,856 (United States) 
}} historical epic Michael Mann novel of 1936 film adaptation, owing more to the latter than the novel. The film stars Daniel Day-Lewis, Madeleine Stowe, and Jodhi May, with Russell Means, Wes Studi, Eric Schweig, and Steven Waddington in supporting roles.
 The soundtrack Trevor Jones Scottish singer-songwriter Dougie MacLean. Released on September 25, 1992, in the United States, The Last of the Mohicans was met with nearly universal praise from critics as well as commercial success during its box-office run.

==Plot==
 Nathaniel Hawkeye,.” 
 Colonel Edmund Lake George, an important point of the defense of New York against the French in Canada. Heyward has also been given the assignment of escorting the colonel’s two daughters, Cora and Alice, to the fort to join their father. He is a family friend, and in love with Cora, and proposes to her to before they leave. She does not give him an answer.
 Mohawk tribe. Unexpectedly, Magua leads the party into an ambush, where all are killed except Heyward and the women. The fight, however, is interrupted by the arrival of Chingachgook and his sons, who kill the enemy warriors, but allow Magua to escape. The major and the women are now stranded and the Mohicans and Hawkeye agree to accompany them the rest of the way. During this trek, Cora begins to form a bond with Hawkeye, and Heyward notices. 
 under siege by the French. They enter the fort during the bombardment, and are greeted by Colonel Munro, asking Major Heyward about reinforcements. The colonel admits to Heyward and the others that the fort is about to fall. While there, Cora and Hawkeye share a passionate kiss. Heyward begins to suspect Cora’s attraction to Hawkeye, and erupts in jealousy. In response, Cora finally tells him she will not marry him.
 Huron army are with the French. In a secret meeting, Magua reveals his hatred for Colonel Munro, and his desire for revenge for the murder of his family.

The next day, Colonel Munro and everyone else march with the British garrison from the fort. In the countryside, Magua and his Huron warriors ambush the British, and Magua kills Colonel Munro. Hawkeye and the Mohicans fight their way through, leading Cora, Alice, and Heyward from the battle, though Magua later captures the major and the women, and takes them prisoner.
 burned alive. In a final gesture of affection, Heyward arranges to be executed in Cora’s place. Later, along steep mountain trails, Chingachgook, Uncas, and Hawkeye follow and attack Magua’s war party. Uncas is killed along with Alice, but in a single combat with Chingachgook, Magua is quickly defeated and killed.

In the end, during a funeral ritual with Hawkeye and Cora, Chingachgook prays to the Great Spirit on behalf of Uncas, remarking that he is now the last of the Mohicans.

==Cast==
 
 
* Daniel Day-Lewis as Hawkeye/Nathaniel Poe
* Madeleine Stowe as Cora Munro
* Russell Means as Chingachgook
* Eric Schweig as Uncas
* Jodhi May as Alice Munro
* Steven Waddington as Maj. Duncan Heyward
* Wes Studi as Magua
* Maurice Roëves as Col. Edmund Munro
* Patrice Chéreau as Gen. Louis-Joseph de Montcalm
* Edward Blatchford as Jack Winthrop
 
* Tracey Ellis as Alexandra Cameron
* Terry Kinney as John Cameron
* Sebastian Roché as Martin
* Justin M. Rice as James Cameron
* Dennis Banks as Ongewasgone
* Pete Postlethwaite as Capt. Beams
* Colm Meaney as Maj. Ambrose Daniel Webb
* Benton Jennings as Scottish Officer
* Jared Harris as British Lieutenant
 

==Soundtrack==
 

==Props== tomahawks used in the film and knifemaker Randall King made the knives.    Wayne Watson is the maker of Hawkeyes "Killdeer" rifle used in the film. The gunstock war club made for Chingachgook was created by Jim Yellow Eagle. Maguas tomahawk was made by Fred A. Mitchell of Odin Forge & Fabrication.

==Locations==
  The Biltmore Triple Falls, Bridal Veil High Falls, DuPont State Recreational Forest. Another of these falls was Linville Falls, in the mountains of North Carolina.

==Reception and honors==
The Last of the Mohicans opened to general acclaim, with critics praising the film for its cinematography and music. Critic Roger Ebert of the Chicago Sun-Times called the film "...quite an improvement on Coopers all but unreadable book, and a worthy successor to the Randolph Scott version," going on to say that "The Last of the Mohicans is not as authentic and uncompromised as it claims to be — more of a matinee fantasy than it wants to admit — but it is probably more entertaining as a result." {{cite news | author= Roger Ebert | date= September 25, 1992| title= The Last of The Mohicans | format= | work= Chicago Sun-Times | url= http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19920925/REVIEWS/209250302/1023
| accessdate=2007-03-18}} 

Desson Howe of The Washington Post classified the film as "glam-opera" and "the MTV version of gothic romance". {{cite news | author= Desson Howe | date= September 25, 1992| title= The Last of The Mohicans | work= The Washington Post | url= http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/thelastofthemohicansrhowe_a0af0a.htm
| accessdate=2007-03-18}}  Rita Kempley of the Post recognized the "heavy drama," writing that the film "sets new standards when it comes to pent-up passion", but commented positively on the "spectacular scenery". {{cite news | author= Rita Kempley | date= September 25, 1992| title= The Last of The Mohicans | work= The Washington Post | url= http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/thelastofthemohicansrkempley_a0a32a.htm
| accessdate=2007-03-18}} 

The Last of the Mohicans is certified "Fresh" at the film site Rotten Tomatoes, with a positive rating of 94% (34 reviews out of 35 counted fresh). 
 Chris Jenkins, Mark Smith, Simon Kaye).   

American Film Institute recognition:
* AFIs 100 Years...100 Heroes and Villains:
** Hawkeye - Nominated Hero 

===Box office===
The film opened in the United States on September 25, 1992, in 1,856 theaters. It was the number 1 movie on its opening weekend.   By the end of its first weekend, The Last of the Mohicans had generated $10,976,661, and by the end of its domestic run, the film had made $75,505,856. {{cite web | date= March 18, 2007| title= The Last of The Mohicans | work= Box Office Mojo | url= http://www.boxofficemojo.com/movies/?id=lastofthemohicans.htm
| accessdate=2007-03-18}}  It was ranked the 17th highest-grossing film of 1992 in the United States. 

==Alternate versions==
The film was released theatrically in 1992 at a length of 112 minutes. It was released at this length on VHS in the U.S. It was re-edited to a length of 117 minutes which was billed as the "Directors Expanded Edition". It was again re-edited for its U.S. Blu-ray release on October 5, 2010,  this time billed as the "Directors Definitive Cut", with a length of 114 mins.

== References ==
 

==External links==
 
*Kristopher Tapley:   at hitfix.com
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 