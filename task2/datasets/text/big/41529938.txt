Alarm (2008 film)
{{Infobox film name            = Alarm image           =  caption         =  director        = Gerard Stembridge producer        = Anna J. Devlin,   Marina Hughes writer          = Gerard Stembridge based on       =  starring       = {{Plainlist|
* Ruth Bradley
* Aidan Turner}} music            = Kieran Lynch cinematography   = Bruno de Keyzer editing          = Mary Finlay studio           = {{Plainlist|
*Film i Väst
*Irish Film Board
*Radio Telefís Éireann}} distributor      = released         =   runtime          = 105 minutes country         = Ireland language        = English budget          =  gross           =
}} Irish Thriller thriller film written and directed by Gerard Stembridge.       

== Overview == The Hobbit) and Ruth Bradley (known for playing Garda Lisa Nolan in Grabbers). 

== Plot ==
After witnessing the murder of her father by burglars, Molly is living with friends and seeing a psychiatrist (Emmet Bergin) to deal with her panic attacks.  She dreams of finding a house where she can live alone.  Upon buying her dream house in a Dublin suburb, she gives a house-warming party and one of her friends brings along an old classmate of Mollys, Mal.  Molly had fancied Mal during school, and now they become a couple.  With a new house and a new boyfriend, things seem perfect.

However, soon her house is burglarized again and again, although her neighours are spared.  Molly suspects that someone she knows might be involved.

== Cast == 
* Ruth Bradley as Molly
* Aidan Turner as Mal
* Owen Roe as Joe and Mossie Tom Hickey as Frank
* Anita Reeves as Jessie
* Emmet Bergin as Psychiatrist
* Alan Howley as Peter 
* Fionnuala Murphy as Receptionist 
* Alan Martin Walsh

== Release ==
Alarm was shown at the Irish cinema in 2008.  On November 30, 2010, an American DVD was published by the IFC Film studios. 

== Critical reception ==
When director and writer Gerard Stembridge introduced Alarm to the audience he said that he had wanted to do a "swan song for the Celtic tiger" (i.e. Ireland’s economic boom in the 1990s). Furthermore, he had wanted to create a good old-fashioned thriller.  However, critics doubt that he succeeded with this, noting that the plot is predictable. Many scenes were filmed at night and the action during these scenes is hard to see. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 