Sthreehridayam
{{Infobox film
| name = Sthreehridayam
| image =Sthreehridayam.png
| caption =
| director = JD Thottan
| producer =
| writer = JD Thottan Jagathy NK Achari (dialogues)
| screenplay = Jagathy NK Achari Ambika
| music = LPR Varma
| cinematography = ENC Nair
| editing = TR Sreenivasalu
| studio = T&T Productions
| distributor = T&T Productions
| released =  
| country = India Malayalam
}}
 1960 Cinema Indian Malayalam Malayalam film, Ambika in lead roles. The film had musical score by LPR Varma.   

==Cast==
*Pappukutty Bhagavathar 
*T. S. Muthaiah 
*Prem Nawas  Ambika 
*Chandni
*Kochappan
*Kottarakkara Sreedharan Nair 
*Sree Narayana Pillai

==Soundtrack==
The music was composed by LPR Varma and lyrics was written by Jayadevar, P. Bhaskaran and Irayimman Thampi. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Chandana Charchitha || LPR Varma || Jayadevar || 
|- 
| 2 || Iniyurangoo || || P. Bhaskaran || 
|- 
| 3 || Kadha parayamo kaatte || A. M. Rajah, Jikki || P. Bhaskaran || 
|- 
| 4 || Maanathulloru Muthassi || || P. Bhaskaran || 
|- 
| 5 || Madhuvidhuvin || Jikki || P. Bhaskaran || 
|- 
| 6 || Praanavallabhamaare || LPR Varma, Shanta, Thripura Sundari || Irayimman Thampi || 
|- 
| 7 || Thaamarakkannaalaare || || P. Bhaskaran || 
|- 
| 8 || Thottaal Mookkinnu || A. M. Rajah || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 