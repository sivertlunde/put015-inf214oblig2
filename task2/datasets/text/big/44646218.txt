Tathagatha Buddha
{{Infobox film
| name           = Tathagat Buddha: The Life & Times of Gautama Buddha
| image          = Tathagatha Buddha film DVD cover.jpg
| caption        = DVD cover
| director       = Allani Sridhar
| concept        = Sadguru Sivananda Murty
| producer       = K. Raja Sekhar Radhakrishna
| story          =
| writer         =  
| based on       =  
| starring       =  
| music          = Shashi Preetam
| cinematography = Madhu Mahankali
| studio         = Dharmapatha Creations
| released       =  
| runtime        = 135 minutes
| language       =  
}}

Tathagat Buddha: The Life & Times of Gautama Buddha  ( ), also known as Gautama Buddha and as The Path Finder,  is a multilingual feature film on the life and times of the Buddha directed by Allani Sridhar and is based upon the story by Sadguru Sivananda Murty.    The film was released in Telugu in 2007  and in other languages in 2008.

==Production== Chief Minister Dr. Rajasekhar Reddy in November 2007, at which event he praised the film and director by saying that "the film and its message were the need of the hour as people were getting tired of all the violence around them"     The film was produced and released in three different languages; as Tadgatha Buddha in Hindi, as Gautama Buddha in Telugu language|Telugu, and as The Path Finder in Indian English|English.   

== Plot ==
Initially released in 2007 by Dharmapatha Creations, the film tells the story of Siddhartha Gautama, Prince of Kapilavastu, situated in Nepal, who lived during 6th century B.C. He was born on Vaisakha Poornima. The history of his family - The Ikshvaku dynasty is traceable to pre-Ramayana times. Renouncing the life and responsibility of a king, Siddhartha Gautama sought a solution to human misery. Ever in the midst of the great Indian religious and spiritual traditions, he noticed what was most needed by all people - Dharma, and Non-violence. He sought a direct path to salvation. He was a lone pathfinder who inspired the religions that eventually spread to China, Japan, and to the United States and Europe as late as the 20th Century.

== Cast ==
* Sunil Sharma as Buddha
* Kausha Rach as Yashodhara Suman as Bimbisara
* Puneet Issar as Anguli Mala
* Parvati Melton
* Surender Pal as Suddhodhana

==Soundtrack==
The Soundtrack is composed by Shashi Pritam. Singers Kumar Sanu, Alka Yagnik, Kavita Krishnamurthy, Sadhana Sargam.

==Reception==
The Hindu wrote that the director  breaks new ground in revealing the fascinating story of Prince Siddhartha and his spiritual transformation into the Buddha, the great teacher who changed the entire world. 

== See also ==
* Depictions of Gautama Buddha in film
*List of historical drama films of Asia Tathagatha Buddha

==References==
 

==External links==
*  
*  

 
 
 
 
 
 