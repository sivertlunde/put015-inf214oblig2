Courage of Lassie
{{Infobox Film
| name           = Courage of Lassie
| image          = 01 Courage of Lassie.jpg
| image_size     = 185px
| caption        = VHS cover
| director       = Fred M. Wilcox
| producer       = Robert Sisk
| writer         = Lionel Houser
| narrator       =  Pal (credited Harry Davenport Selena Royle Tom Drake George Cleveland
| music          = Scott Bradley Bronislau Kaper Leonard Smith	
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       = November 8, 1946
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $1,530,000  . 
| gross          = $4,100,000 
}}
 Pal in a story about a collie named Bill and his young companion, Kathie Merrick. When Bill is separated from Kathie following a vehicular accident, he is trained as a war dog, performs heroically, and, after many tribulations, is eventually reunited with his beloved Kathie. Though the film is called Courage of Lassie, Eric Knights fictional canine character Lassie does not appear in the film.

Courage of Lassie is the third of seven MGM films featuring a canine star called Lassie, which, in actuality, was a male collie named Pal (dog actor)|Pal. Using the stage name Lassie, Pal appeared as the titular character in the first film, Lassie Come Home and as Laddie in its sequel, Son of Lassie. Collins, Ace. Lassie: a Dogs Life. Penguin Books, 1993.  Courage of Lassie has been released to VHS and DVD.

==Plot==
A collie pup is separated from his mother and grows to young adulthood in the forest. After being swept away in a torrent and then shot by a young hunter, he is found by Kathie Merrick (Elizabeth Taylor) and carried to her home. With the help of a kindly shepherd, Mr. MacBain (Frank Morgan), she tends him back to health, names him Bill, and teaches him to herd sheep.  

One day, unknown to Kathie, Bill is hit by a truck and taken to an animal hospital. Kathie risks her life futilely searching for him on the island where they first met. Bill remains unclaimed in the hospital for two months and is sent to a War Dog Training Center, where he is referred to as "Duke". After training, he is shipped out with the troops to the Aleutian Islands Campaign. Duke performs heroically on the battlefield, but the stress and a wound cause him to become aggressive. Sent back to the War Dog Training Center to recover, he escapes, attacking livestock and threatening people as he finds his way back to Kathie.  

Merricks neighbors insist he be put down because of his attacks, and Bill is impounded. A hearing is held and Mr. MacBain acts as Bills lawyer.  He discovers an Army tattoo in Bills ear; a quick investigation reveals Bill is a war hero. All then realize that the dog who served on the battlefield was not himself after his war experiences, and he will need time to adjust to civilian life. Bill is freed and joyfully reunites with Kathie.

==Main cast==
  Pal (credited as Lassie) as Bill, a rough collie
* Elizabeth Taylor as Katherine Eleanor Merrick, a young girl living on a sheep ranch
* Selena Royle as Mrs. Merrick, her mother 
* Catherine McLeod as Alice Merrick, her sister  David Holt as Pete Merrick, her brother
* Frank Morgan as Harry MacBain, a shepherd 
* Tom Drake as Sergeant Smitty, Bills trainer in the Army 
* Bill Wallace as Sergeant Mac, Smittys friend  Harry Davenport as Judge Payson 
* George Cleveland as Old Man, Bills original owner 
* Morris Ankrum as Farmer Crews, a Merrick neighbor 
* Mitchell Lewis as Gil Elson, a Merrick neighbor 
* Jane Green as Mrs. Elson, Gil Elsons wife 
* Minor Watson as Sheriff Ed Grayson 
* Donald Curtis as Charlie, a truck driver
* Clancy Cooper as Casey, a truck driver
* Carl Switzer as First Youth, a hunter
* Conrad Binyon as Second Youth, a hunter
 

==Production==
The film was shot on location in Railroad Creek by Lake Chelan near Holden Village, Washington|Holden.  

Courage of Lassie was fourteen-year-old Elizabeth Taylors second "Lassie" film as she had appeared in Lassie Come Home in the minor role of the Duke of Rudlings granddaughter, Priscilla.   Taylor received the first top billing of her career with Courage of Lassie.   George Cleveland, the "Old Man" in the opening scenes of Courage of Lassie would become the star of the 1954 television series Lassie (1954 TV series)|Lassie. 
==Reception==
The film was popular and earned $2,505,000 in the US and Canada and $1,595,000 elsewhere, making MGM a profit of $968,000. 
==References==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 