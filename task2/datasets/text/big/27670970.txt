Loaded (2008 film)
{{Infobox film
| name           = Loaded
| image          = Loaded 2008.jpg
| caption        = DVD Cover
| director       = Alan Pao
| producer       = Corey Large Jack Luu Alan Pao Ian Carrington Aaron Rattner
| writer         = Kyle Kramer Corey Large S.A. Lucerne Alan Pao  Johnny Messner Jimmy Jean-Louis Vinnie Jones Mitchell Baker Mary Christina Brown
| music          = Ralph Rieckermann   
| cinematography = Roger Chingirian
| editing        = Chris Levitus
| distributor    = GoDigital Media Group
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Loaded is a 2008 crime thriller film directed by Alan Pao.

==Synopsis==
Tristan Price is a wealthy and privileged teenager who seemingly has everything he could ever want, money, loving parents and a beautiful girlfriend. However, his perfect life is turned upside down by the arrival of Sebastian, a handsome, charismatic, ruthless drug dealer. As Tristan is sucked into a seedy underworld of drugs, sex and violence he begins to realise his new best friend is, in fact, his worst enemy.

==Cast==
*Jesse Metcalfe as Tristan Price 
*Corey Large as Sebastian 
*Monica Keena as Brooke 
*Nathalie Kelley as April 
*Chace Crawford as Hayden Price  Johnny Messner as Javon 
*Jimmy Jean-Louis as Antonio 
*Vinnie Jones as Mr.Black 
*Mitchell Baker as Damon 
*Mary Christina Brown as Lin

==External links==
* 

 
 
 
 
 
 
 
 
 
 