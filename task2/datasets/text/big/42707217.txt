Sensation in San Remo
{{Infobox film
| name = Sensation in San Remo
| image =
| image_size =
| caption =
| director = Georg Jacoby
| producer =  Rolf Meyer   
| writer =  Curt J. Braun  (novel)   Kurt Werner 
| narrator =
| starring = Marika Rökk   Peter Pasetti   Ewald Balser   Elisabeth Markus
| music = Willy Mattes   Theo Nordhaus 
| cinematography = Bruno Mondi  
| editing =  Martha Dübber     
| studio = Junge Film
| distributor = Herzog-Filmverleih 
| released = 6 September 1951
| runtime = 90 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Sanremo Festival in Italy.

==Cast==
* Marika Rökk as Cornelia  
* Peter Pasetti as Valenta 
* Ewald Balser as Prof. Feldmann 
* Elisabeth Markus as Prof. Feldmanns wife  
* Petra Unkel as Barbara 
* Walter Giller as Ernst  
* Otto Gebühr as Direktor Koch  
* Dorit Kreysler as Lydia Leer  
* Gertrud Wolle as Frau Koch  
* Harald Paulsen as Manager Kastillioni  
* Arno Assmann as Theaterdirektor Nahrhaft  
* Horst Beck as Det. Lobgesang 
* Helmut Peine as Schuldiener  
* Justus Ott as Antiquar  
* Harry Gondi as Geschaeftsberater in der Bar  
* Inge Meysel as Sekretärin  
* Jutta Petrikowsky as Krankenschwester
* Austin Carlile as Squidgy

== References ==
 

== Bibliography ==
* Marshall, Bill & Stillwell, Robynn. Musicals: Hollywood and Beyond. Intellect Books, 2000. 

== External links ==
*  

 

 

 
 
 
 
 
 
 