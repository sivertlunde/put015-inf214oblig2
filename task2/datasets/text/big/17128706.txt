The Midnight Express (film)
:For the 1978 Alan Parker film, see Midnight Express (film).
{{Infobox film
| name           = The Midnight Express
| image          =
| caption        =
| director       = George W. Hill
| producer       =
| writer         = George W. Hill
| narrator       =
| starring       = Elaine Hammerstein William Haines
| cinematography =
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 56 minutes
| country        = United States Silent English English intertitles
| budget         =
}}
The Midnight Express (1924 in film|1924) is a romantic film directed by George W. Hill.

==Plot==
Jack (Haines) is a young man who has a wild lifestyle. This displeases his father. Jack wants to prove to him he can be a hardworking man as well and decides to find work at a railroad yard as a laborer. Silent Bill Brachley is a convict who has escaped jail and steals Jacks car. Jack chases him and eventually meets Mary (Hammerstein). Brachley is led back to jail and swears revenge. He escapes jail yet again and confronts Jack. After Jack wins the fight, he receives the respect from his father and has Mary as his sweetheart. 

==Reception==
After femme fatale Peggy Hopkins Joyce viewed the film, she recalled the kiss between Haines and Hammerstein as the best shed ever seen on screen. The studio tried to present Hammerstein and Haines as a real-life couple. This was without success, as Haines was homosexual. 

==Cast==
* Elaine Hammerstein - Mary Travers
* William Haines - Jack Oakes George Nichols - John Oakes
* Lloyd Whitlock - Joseph Davies
* Edwin B. Tilton - James Travers
* Pat Harmon - Silent Bill Brachely
* Bertram Grassby - Arthur Bleydon
* Phyllis Haver - Jessie Sybil
* Roscoe Karns - Switch Hogan Jack Richardson - Detective Collins
* Noble Johnson - Deputy Sheriff

==External links==
* 

==References==
 

 
 
 
 
 
 
 
 


 
 