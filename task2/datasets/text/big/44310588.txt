Nativity 3: Dude, Where's My Donkey?
{{Infobox film
| title = Nativity 3: Dude, Wheres My Donkey?
| director       = Debbie Isitt  Nick Jones
| writer         = Debbie Isitt Jason Watkins
| music          = Nicky Ager   Debbie Isitt
| cinematography = Sean Van Hales
| editing        = Nicky Ager
| studio         = Mirrorball Films
| distributor    = Entertainment One
| released       =  
| runtime        = 110 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =£5.7 million 
}}
 Jason Watkins. Once again, Entertainment One is the distributor of Nativity 3.

==Plot==
Disaster strikes again: This time Mr Poppy has lost his beloved donkey and Mr Shepherd has lost his memory because of it! Does the class have enough time to help him find them and reunite him with his fiancèe too?

==Cast==
*Martin Clunes as Jeremy Shepherd
*Marc Wootton as Desmond Poppy
*Catherine Tate as Sophie ODonnell
*Celia Imrie as Clara Keen Jason Watkins as Gordon Shakespeare
*Stewart Wright as Uncle Henry
*Adam Garcia as Bradley Finch
*Duncan Preston as Mr. ODonnell
*Susie Blake as Mrs. ODonnell

==Reception==
===Critical response===
The film was mostly panned by critics. Rotten Tomatoes gives it a 10% rating based on 20 reviews (18 negative reviews and 2 positive reviews) with an average score of 3/10. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 