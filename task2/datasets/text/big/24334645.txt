Get Low (film)
{{Infobox film
| name           = Get Low
| image          = Get Low Poster.jpg
| caption        = Theatrical release poster
| director       = Aaron Schneider
| producer       =  
| screenplay     =  
| story          =  
| starring       =  
| music          = Jan A. P. Kaczmarek David Boyd
| editing        = Aaron Schneider
| studio         =  
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $7 million
| gross          = $9.7 million 
}}  Rebecca Grant, Scott Cooper and Chandler Riggs.  The motion picture was filmed entirely on location in Georgia (U.S. state)|Georgia, and support for the production was provided by the Georgia Department of Economic Development.  

The film premiered at the Toronto International Film Festival and is distributed by Sony Pictures Classics. For his performance, Robert Duvall was awarded the Hollywood Film Festival Award for Best Actor in October 2010. The film was released on July 30, 2010, in the United States. It received positive reviews from critics.

==Plot== in league funeral party" for himself. Frank Quinn (Bill Murray), the owner of the local funeral parlor in financial trouble, coveting Bushs wad of cash, agrees to advertise a funeral party at which the townsfolk will be invited to tell Felix Bush the stories theyve heard about him.  To ensure a good turnout, a lottery is organized, with Bushs property as the prize. Many people buy tickets.

Things get more complicated when an old mystery is remembered, involving a local widow named Mattie Darrow (Sissy Spacek), who was Bushs girlfriend in their youth, and her deceased sister, Mary Lee Stroup (Arin Logan ). With the help of a preacher who insists on Bush telling her the truth, Bush tells those gathered at his funeral party, particularly Mattie, what happened forty years earlier. He reveals his affair with Matties married sister, Mary Lee. He confesses to Mattie that it was Mary Lee who was his true love, his only love. They made plans to run away together, and when she didnt arrive at the agreed place, he went to her home to search for her. He discovered that her husband had attacked her with a hammer, knocking her out. The husband threw a kerosene lamp against a wall to set the house on fire and kill himself, the unconscious Mary Lee, and Bush. Bush freed himself from the attacking husband, but as his clothes caught fire, he also saw Mary Lee catch fire. As he went to put the fire out, he felt himself flying through the window, possibly pushed by the husband, and he was unable to re-enter the house to save Mary Lee.

Mattie returns, seeming to forgive Bush. He sees what appears to be the ghost of Mary Lee down the lane. He dies shortly after his funeral party.

==Cast==
* Robert Duvall as Felix Bush
* Sissy Spacek as Mattie Darrow
* Bill Murray as Frank Quinn
* Lucas Black as Buddy Robinson
* Gerald McRaney as Rev. Gus Horton
* Bill Cobbs as Charlie Jackson
* Arin Logan as Mary Lee Stroup
* Lori Beth Edgeman as Kathryn Robinson
* Andrea Powell as Bonnie Rebecca Grant as Joan Scott Cooper as Carl
* Blerim Destani as Gary
* Chandler Riggs as Tom
* Danny Vinson as Grier
* Tomasz Karolak as Orville
* Andy Stahl as Photographer

==Production==
The film is loosely based on a true story that happened in Roane County, Tennessee, in 1938. Duvalls character, Felix Bush, was based on a real person named Felix Bushaloo "Uncle Bush" Breazeale.  

==Critical response== Oscar nomination, writing, "...Mr. Duvall, whos probably looking at another Oscar nomination next year, gives it a heart."  A "New York Times" review by A. O. Scott highlights Robert Duvalls superb performance. 
 
Casey Burchby at DVD Talk noted that Get Low contains "a wonderful group of performances by a dream cast. Surprisingly, none of the leads were in the running for any of 2010s major awards." 

==Box office==
The film opened to four cinemas on July 30, 2010, taking in a weekend gross of $90,900, averaging $22,725 per cinema. This placed the film at twenty-third overall for the weekend of July 30 to August 1, 2010.    As of January 2011, the film had grossed $9,100,230 in North America and $401,361 in other territories, totaling $9,513,225 worldwide. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  , editorial essay and documentary on Get Low

 
 
 
 
 
 
 
 
 
 