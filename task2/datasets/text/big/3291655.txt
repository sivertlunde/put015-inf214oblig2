Top of the World (1997 film)
{{Infobox film
| name           = Top of the World
| image_size     =
| image	         = Top_of_the_World_(1997_film).jpeg
| caption        =
| director       = Sidney J. Furie
| writer         = Bart Madison
| narrator       =
| starring       = Peter Weller Dennis Hopper Tia Carrere
| music          = Robert O. Ragland
| cinematography = Alan Caso
| editing        = Alain Jakubowicz
| distributor    =
| released       = 1997
| runtime        = 99 min.
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}}
Top of the World is a 1997 film directed by Sidney J. Furie and starring Peter Weller, Las Vegas.

==Plot==
In Las Vegas for a quicky divorce, a just-paroled ex-cop and his wife wander into the Cowboy Country Casino, run by the shady Charles Atlas. They win big, right as the casino is being robbed. The police believe their big win was a staged diversion, and the two of them become suspects. Over the course of the evening and next morning, the two attempt to escape to surrounded casino, and to prove their innocence, as well as to save their marriage.

==Cast==
* Peter Weller as Ray Mercer
* Dennis Hopper as Charles Atlas
* Tia Carrere as Rebecca Mercer
* David Alan Grier as Detective Augustus
* Joe Pantoliano as Vince Castor

== External links ==
*  

 

 
 
 
 
 
 
 
 

 