Marie Galante (film)
{{Infobox film
| name           = Marie Galante
| image_size     =
| image	=	Marie Galante FilmPoster.jpeg
| caption        = Theatrical release poster Henry King
| producer       = Winfield R. Sheehan (producer) Reginald Berkeley (screenplay) Dudley Nichols  (uncredited)
| narrator       =
| starring       = See below
| music          = Arthur Lange
| cinematography = John F. Seitz
| editing        = Harold D. Schuster
| distributor    = Fox Film Corporation
| released       = 1934
| runtime        = 88 minutes
| country        = USA
| language       = French, English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Marie Galante is a 1934 American film directed by  .

== Plot summary ==
 

=== Differences from novel ===
 

== Cast ==
*Spencer Tracy as Dr. Crawbett
*Ketti Gallian as Marie Galante
*Ned Sparks as Plosser
*Helen Morgan as MissTapia
*Sig Ruman as Brogard
*Leslie Fenton as General Saki Tenoki
*Arthur Byron as General Gerald Phillips
*Robert Loraine as Ratcliff
*Frank Darien as Ellsworth

== Soundtrack ==
* Helen Morgan - "Serves Me Right for Treating You Wrong" (Written by Maurice Sigler, Al Goodhart and Al Hoffman)
* "Song of a Dreamer" (Music by Jay Gorney, lyrics by Don Hartman)
* "Un Peu Beaucoup" (Music by Arthur Lange, lyrics by Marcel Silver)
* "Shim Shammy" (Music and lyrics by Stepin Fetchit)
* "Its Home" (Music by Jay Gorney, lyrics by Jack Yellen)
* "On a Little Side Street" (Music by Harry Akst, lyrics by Bernie Grossman)
* "Je tadore" (Music by Harry Akst, lyrics by Bernie Grossman)

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 