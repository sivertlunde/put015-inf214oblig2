Invisible Invaders
{{Infobox film
| name           = Invisible Invaders
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Edward L. Cahn
| producer       = Robert E. Kent
| writer         = Samuel Newman
| starring       = {{Plainlist |
* John Agar
* John Carradine
* Philip Tonge
}}
| music          = Paul Dunlap
| cinematography = Maury Gertsman
| editing        = 
| studio         = Premium Pictures
| distributor    = United Artists
| released       =  
| runtime        = 67 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Invisible Invaders is a 1959 science fiction film starring John Agar and John Carradine.  It was produced by Robert E. Kent, directed by Edward L. Cahn and written by Samuel Newman.

== Plot ==
Dr. Karol Noymann, an atomic scientist, is killed in a laboratory explosion. His colleague, Dr. Adam Penner, is disturbed by the accident and resigns his position and calls for changes.

At Dr. Noymanns funeral, an invisible alien takes over Noymanns dead body. The alien, in Noymanns body, visits Dr. Penner and tells him the earth must surrender or an alien force will invade and take over the earth by inhabiting the dead and causing chaos. The alien demonstrates to Penner that they are able to make things invisible. Penner tells his daughter Phyllis and Dr. John Lamont about the experience and asks Dr. Lamont to relay the message to the government in Washington, D.C.. The government ignores the warning and Dr. Penner is labeled a crank by the media.

Dr. Penner takes his daughter and Dr. Lamont to Dr. Noymanns grave, where they are visited by an invisible alien. Later, at the site of a plane crash, another alien takes over a dead pilots body, goes to a hockey game, chokes the announcer and issues an ultimatum for the earth to surrender. Another alien take over a dead body from a car crash and issues the same ultimatum at a different sporting event. The media announces the threat and the governments of the world decide to resist the invasion. Aliens takes over more dead bodies and blow up dams, cause fires, flooding and destroy buildings.

Maj. Bruce Jay arrives to take Dr. Penner, Phyllis and Dr. Lamont to a secret bunker. On the way, they are confronted by a scared farmer who tries to take their vehicle. Maj. Jay kills the farmer and they proceed to the bunker while an alien takes over the dead farmers body.
 acrylic to seal it in plastic, but this fails. They fill a hole with the acrylic liquid and lure an alien into it. Once captured, the alien is taken back to the bunker.

Back at the bunker, they confine the alien in a room and break the acrylic to set it free. They try several experiments, but nothing effects the alien. Frustrated and hopeless, Dr. Lamont wants to surrender, but Maj. Jay does not. The two men fight and damage some equipment that sets off a loud alarm. They notice that the alien reacted violently to the noise.

They make a sound gun and test it on the alien, causing it to become visible and killing it in the process. They try to inform the government, but their broadcast is jammed by the aliens. They follow the jamming signal to the alien ship, killing several aliens along the way. Maj. Jay walks through the woods to get to the ship and is confronted by several aliens. He kills them with the sound gun but is shot. He finds the alien ship, shoots it with the sound gun and destroys it. Dr. Penner is then able to contact the government and tell them how to stop the aliens while Phyllis tends to Maj. Jays wound.

Later, at the United Nations, Dr. Penner, Dr. Lamont, Phyllis and Maj. Jay receive thanks for saving the world from the alien invasion.

== Cast ==
* John Agar as Maj. Bruce Jay
* Jean Byron as Phyllis Penner
* Philip Tonge as Dr. Adam Penner Robert Hutton as Dr. John Lamont
* John Carradine as Dr. Karol Noymann
* Hal Torey as The Farmer
* Paul Langton as Lt. Gen. Stone
* Eden Hartford as WAAF Secretary
* Don Kennedy as Pilot
* Chuck Niles as Hockey Game Announcer

== Production ==
 
Production of Invisible Invaders began in December 1958.  The film was made as part of a package deal with The Four Skulls of Jonathan Drake. It was the fourth science fiction film made by Premium Pictures. 

== Release ==
Invisible Invaders was released to theaters in June 1959. 

== Reception ==
The movie only had a short run but became a minor cult film on television. 

=== Critical response ===
 
Film critic Emanuel Levy rated the film 3 out of 5 stars.   Writing in The Zombie Movie Encyclopedia, academic Peter Dendle said, "Though clearly a product of its own time and a low budget, Invisible Invaders is engaging and fast-paced, riddled with genuinely inspired twists alongside breathtaking implausibilities." 

=== Home media ===
Invisible Invaders was first released to the home video market on VHS videotape in 1996 by MGM/UA Home Video.  The film was released on DVD by MGM Home Entertainment in 2003, packaged with Journey to the Seventh Planet as part of their Midnite Movies series. 

== Soundtrack ==
  later in 1959, The Three Stooges in Orbit in 1962 and Destination Inner Space in 1966.

== References ==
{{Reflist |refs= 
 
{{cite web
 | url = http://www.tcm.com/tcmdb/title/5012/Invisible-Invaders/articles.html
 | title = Invisible Invaders (1959)
 | first1 = Scott
 | last1 = McGee
 | first2 = Jeff
 | last2 = Stafford
 | year = 2013
 | work = Turner Classic Movies
 | publisher = Turner Broadcasting System
 | accessdate = October 13, 2013
}}
 

 
{{cite news
 | title = FILMLAND EVENTS: MGM Purchases New Novel of Chamales
 | author = LA Times staff
 | newspaper = Los Angeles Times
 | publisher = Eddy Hartenstein
 | location = Los Angeles, California, US
 | issn = 0458-3035
 | oclc = 3638237
 | date = December 30, 1958
 | page = B9
}}
 

 
{{cite journal
 | author = AFI staff
 | year = 2013
 | title = Invisible Invaders
 | journal = AFI Catalog of Feature Films
 | location = Los Angeles, California, USA
 | publisher = American Film Institute
 | oclc = 772904208
 | accessdate = October 13, 2013
 | url = http://www.afi.com/members/catalog/DetailView.aspx?s=&Movie=52929
}}
 

 
{{cite AV media
 | year = 1996
 | title = Invisible Invaders
 | medium = VHS
 | publisher = MGM/UA Home Video
 | location = Santa Monica, California, US
 | isbn = 9780792829843
 | oclc = 35071276
}}
 

 
{{cite AV media
 | year = 2003
 | title = Invisible Invaders / 
 | medium = DVD
 | publisher = MGM Home Entertainment
 | location = Santa Monica, California, US
 | isbn = 9780792855286
 | oclc = 52900002
}}
 

 
{{cite web
 | url = http://www.rottentomatoes.com/m/invisible_invaders/reviews/
 | title = Invisible Invaders Reviews
 | first = Emanuel
 | last = Levy
 | date = August 30, 2005
 | work = Rotten Tomatoes
 | publisher = Flixster
 | accessdate = October 13, 2013
}}
 

   
}}

=== Bibliography ===
*{{cite book
 | last1 = Parla
 | first1 = Paul
 | last2 = Mitchell
 | first2 = Charles P.
 | title = Screen Sirens Scream!: Interviews with 20 Actresses from Science Fiction, Horror, Film Noir and Mystery Movies, 1930s to 1960s
 | url = http://books.google.com/?id=VVNpvvU6A0EC&pg=PA31&dq=%22Invisible+Invaders%22+1959+Agar+Carradine
 | accessdate = October 13, 2013
 | edition = illustrated
 | date = October 1, 2009
 | publisher = McFarland & Company
 | location = Jefferson, North Carolina, US
 | isbn = 9780786445875
 | oclc = 318421123
 | page = 31
}}
*{{cite book
 | last1 = Weaver
 | first1 = Tom
 | last2 = Mank
 | first2 = Gregory W.
 | title = John Carradine: the films
 | url = http://books.google.com/?id=ZHpZAAAAMAAJ&q=%22Invisible+Invaders%22+1959+Agar+Carradine&dq=%22Invisible+Invaders%22+1959+Agar+Carradine
 | accessdate = October 13, 2013
 | edition = illustrated
 | year = 1999
 | publisher = McFarland & Company
 | location =  Jefferson, North Carolina, US
 | isbn = 9780786406074
 | oclc = 40723696
}}
*{{cite book
 | author = Variety
 | authorlink = Variety (magazine)
 | title = Varietys Film Reviews: 1959-1963
 | url = http://books.google.com/?id=y2pZAAAAMAAJ&q=%22Invisible+Invaders%22+1959+Agar+Carradine&dq=%22Invisible+Invaders%22+1959+Agar+Carradine
 | accessdate = October 13, 2013
 | date = May 1, 1989
 | publisher = R.R. Bowker
 | location = New York City, New York, US
 | isbn = 9780835227896
 | oclc = 489584871
}}

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 