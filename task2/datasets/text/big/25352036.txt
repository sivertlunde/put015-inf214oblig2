Up a Tree
 
{{Infobox Hollywood cartoon cartoon name = Up a Tree series = Donald Duck image = Up a Tree.png caption = Title card director = Jack Hannah producer = Walt Disney story artist = Dick Kinney   Milt Schaffer voice actor Jimmy MacDonald   Clarence Nash musician = Oliver Wallace animator = Bob Carlson   Al Coe   Volus Jones   Bill Justice   Dan MacManus (effects) layout artist = Yale Gracey background artist = Claude Coats studio = Walt Disney Productions distributor = RKO Radio Pictures release date =   (USA) {{cite encyclopedia last = first = authorlink = Dave Smith (archivist) encyclopedia =   title = edition = year = publisher = Disney Editions location = New York isbn = pages = }}  color process = Technicolor runtime = 6 minutes country = United States language = English preceded by = Beezy Bear followed by Chips Ahoy
}} Walt Disney RKO Radio Pictures. The film stars Donald Duck and Chip n Dale, with Donald working as a lumberjack trying to work on a tree Chip and Dale are living in. It was directed by Jack Hannah and features original music by Oliver Wallace.

==Plot== top a tall tree on a hill. The tree turns out to be the home of chipmunks Chip and Dale, who repeatedly foil Donalds attempts to his frustration as Donald has no idea what is going on at first. Eventually he discovers the chipmunks are deliberately sabotaging his work, and finally decides to chop the entire tree down in retaliation. The tree falls and flips end over end landing on a log flume. Using a pike pole, Donald catches a ride on the log and heads for the sawmill.

Chip and Dale catch a ride in a toolbox on a zip line overhead. They ride ahead of Donald, jump from the tool box with a hammer, and dismantle a side of the flume with it. Donald goes careening off of the flume and the out of control log follows him down a hill. He tries to outrun it with his car but Chip and Dale cause it to fall on the car and crush it.

The log runs through a mine, picking up dynamite as it did, and heads straight for Donalds house. Panicking, Donald moves everything he can out of the path of the log and breathes a sigh of relief as it passes through the house. However, Dale points out to Donald that the log has struck power lines behind the house. All Donald can do is watch as the lines shoot the log back toward the house, sending it flying into the distance where it explodes. A dazed Donald can only watch in disbelief while the chipmunks mock him.

==Up a Tree in House of Mouse==
The short Up a Tree also appears in House of Mouse under the episode name Chip n Dale.

==Availability==
*Laserdisc - Chip N Dale with Donald Duck
*VHS - Chip N Dale with Donald Duck
*VHS - The Adventures Of Chip N Dale
*DVD - Chip n Dale : Volume 1 : Here Comes Trouble
*DVD - The Chronological Donald: Volume 4, Disc 2  

==References==
 

==External links==
*  at the Encyclopedia of Animated Disney Shorts
* 
* 
 
 
 
 
 