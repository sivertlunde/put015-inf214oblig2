Oru Kudakeezhil
{{Infobox film 
| name           = Oru Kudakeezhil
| image          = Orukudakeezhilfilm.png
| caption        = Poster designed by Gayathri Ashokan
| director       = Joshiy
| producer       = Sajan
| writer         = AR Mukesh Kaloor Dennis (dialogues)
| screenplay     = Kaloor Dennis Madhavi Nedumudi Shankar
| Johnson
| cinematography = Anandakkuttan
| editing        = K Sankunni
| studio         = Saj Movies
| distributor    = Saj Movies
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Madhavi and Rohini plays supporting roles. The film had musical score by Johnson (composer)|Johnson.   

==Cast==
  Madhavi
*Nedumudi Venu
*Sukumari
*Thilakan
*Venu Nagavally Rohini
*Prathapachandran Baby Shalini
*KPAC Sunny
*Lalu Alex
*Mala Aravindan
*Paravoor Bharathan
*Thodupuzha Vasanthi Shankar
 

==Soundtrack== Johnson and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anuraagini || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Bhoomippennin Poomey || K. J. Yesudas, Vani Jairam || Poovachal Khader || 
|-
| 3 || Pinakkamenthe || Vani Jairam || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 