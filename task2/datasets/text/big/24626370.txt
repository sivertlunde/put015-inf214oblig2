Teenage Dirtbag (film)
{{Infobox film
| name = Teenage Dirtbag
| caption = 
| image	=	Teenage Dirtbag FilmPoster.jpeg
| producer = Chris Aagaard Seth Caplan
| director = Regina Crosby
| writer = Regina Crosby
| starring = Scott Michael Foster Noa Hegesh Regina Crosby
| music =
| cinematography = Armando Ballesteros
| editing = Andrea Trillo
| distributor = Vivendi Entertainment Lightyear Entertainment
| released =  
| runtime = 106 minutes
| country = United States
| language = English
| budget =
}}

Teenage Dirtbag is a 2009 drama film starring    and Lightyear Entertainment.

==Plot==
At a local IGA store, a pregnant girl is shocked to learn from an old classmate that a boy she once knew named Thayer died in a river recently. When going back to her car, the girl has a flashback of her time in high school.

The girl is known to be Amber Lange, once a popular cheerleader in high school. In class, they are dissecting a fetal pig. When its time to clean up, Thayer Mangeres, a classmate who always sat beside Amber because of their last names, asks people around their table how much they will bet him if he drinks the juice from the fetus. As everyone is putting their money on the table, Amber looks disgusted. Thayer then tells everybody to keep their money, implying that he wont drink it, but he then says he will for free. After the bet, Thayer asks what Amber thought. She tells him that she thinks nothing of him. Ever since that day, hes harassed her.

The next year they are put in Creative Writing and Study Hall together, which Thayer skips the classes to get high with his friend. For the next couple of days, its hard for Amber and Thayer to get along. At a party that night, he eats fish from a bowl that she caught earlier that day. Very angry, Amber continually pushes Thayer and asks him why he is such a freak.
Thayer continues to harass Amber in class, as a new girl named Tabitha is assigned a seat by them. Eventually Tabitha and Thayer form a friendship, and knowing that he does not like Amber and bullies her, she does the same. By the soda machine, Thayer flirts with Tabitha,   unbeknownst to Tabitha that he is doing this to make Amber envious. When Amber walks away peeved, he stops, clearly wanting Ambers attention all along. After school, Thayer walks home. He walks in and his father shouts angrily that he got a call from the school about his actions,and does not want another call from them. Thayer is frightened by his dad, and his father calls in Thayers brother Dooley, who physically abuses him, to hold him down while his dad whips him. On the same night, Amber walks in to her home, but no one is there.

Back in Creative Writing, Thayer starts to show his emotions in his poems, and Amber realizes this because she feels exactly the same way.

Thayer begins the start of their friendship by asking Amber questions through writing notes to each other in his notebook. As their friendship progresses, so does Thayers feelings. Thayer begins to fall in love with Amber the more they talk. During class, Thayer sends Amber signals in his poems. As Amber hears these poems in class, she knows they are about her. Shes not sure if she feels the same about him because of their different social groups, although she has strange dreams of kissing him.
Thayer and Ambers lives and homes are different, even though they are both experiencing trouble. Thayer is struggling at home with his family. His dad beats him and drunk brother does the same, but his sister Jeannie is very compassionate toward him as he is the same to her. Amber is mostly alone when her parents are gone. At a sandbar one night, Amber has sex with a school friend as Thayer watches abroad. Feeling betrayed, Thayer writes another poem about Amber, but this time, its about the night before. He starts becoming rude again and harasses her verbally. When the teacher realizes that hes insulting one of the students, he sends Thayer outside with him. Tabitha tells Amber that Amber is jealous of her and Thayers "relationship". Amber, ticked off, takes out hers and Thayers notebook and throws it onto Tabithas desk. After looking at a few pages, Tabitha runs out of the classroom. Everyone rushes to see the notebook, all while Amber sits there. Her life turns back to normal because she doesnt associate with him anymore. One day in the hallway, Thayer and Amber accidentally meet up in the hallway. Thayer begins to yell at her, and questions why she gave away their notebook. After being nose to nose, literally, Thayer walks away without an answer from her. Amber finally screams "Because I hate you!"

Thayer still has feelings for Amber, after Tabitha mischievously attempts to spill purple nail polish in Ambers book bag, when he grabs her wrist. After eating 4 brown tablets which Thayer tells Amber theyre drugs, which she doesnt believe, Thayer convulses and has a seizure on the floor. The whole class has to leave the room and Amber watches through the window on the door as Thayer goes unconscious. After the overdose, Thayer never returned to school. Ambers world returned to normal. Years later, while shopping, Amber meets Thayer again and is surprised to learn that he had joined the Mormon faith.

The scene switches back to the present, Amber is at home calling up old schoolmates trying to find out exactly how Thayer died. No one is really sure how Thayer died, but Amber finds out that his father and brother died suspiciously, and no one seems to know where his sister is.

Scene changes to another old memory, Amber meets Thayer again at the beach. She questions him about his Mormon Mission and he informs her that he left early to which she replies, "you dont look very Mormon". Thayer evades her questions by changing the subject. Eventually, Amber informs him that she is getting married, Thayer remains silent for a second and then starts talking about how easy it would be to fake his own death. Amber debates with him a little bit telling him that she would know that he wasnt dead, to which he smirks and says "No, you wouldnt" and walks off.

The scene briefly switches back to the present and then to another memory when Thayer follows Amber into the bathroom of a movie theatre the same day of the beach meeting. He argues with her trying to get her to admit her feelings, but she denies it and pretends that she doesnt know what hes talking about.

Amber eventually finds Thayers sister and visits her, she then visits the school and finishes writing in the notebook which she pulled out from her closet. She admits defeat stating that shell never know whether he was alive or dead. The movie ends with a scene of Thayers sister looking in on Amber in a hospital room having just given birth to a baby boy, named Thayer.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 