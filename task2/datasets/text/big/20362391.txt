A Connecticut Yankee in King Arthur's Court (1921 film)
{{Infobox film
| name           = A Connecticut Yankee in King Arthurs Court
| image          = Connecticut Yankee 1921.jpg
| image_size     = 190px
| caption        = Theatrical poster featuring star Harry Myers with the spirit of Mark Twain peering over his shoulder
| director       = Emmett J. Flynn William Fox
| writer         = Berhard McConville
| based on       =  
| starring       = Harry Myers Pauline Starke Rosemary Theby George Siegmann
| cinematography = Lucien Andriot
| editing        = C.R. Wallace
| distributor    = Fox Film
| released       =   reels (8,291 feet)
| country        = United States
| language       = Silent film English Intertitle
}}

A Connecticut Yankee in King Arthurs Court is a 1921   to the past. 
 Battle of A Connecticut Tom Hepburn, brother of Katharine Hepburn, to commit suicide in 1921. 

According to silentera.com, only reels 2, 4 and 7 survive.  Likewise the Library of Congress silent film database has the film incomplete.   

==Cast==
*Harry Myers - Martin Cavendish (as Harry C. Myers)
*Pauline Starke - Sandy
*Rosemary Theby  - Queen Morgan le Fay
*Charles Clary - King Arthur
*William V. Mong - Merlin the Magician
*George Siegmann - Sir Sagramore
*Charles Gordon - Clarence, the Page
*Karl Formes - Mark Twain
*Herbert Fortier - Mr. Cavendish
*Adele Farrington - Mrs. Cavendish
*Wilfred McDonald - Sir Lancelot

==Reception==
The film was the seventh biggest hit of 1922 in the US and Canada. 
==References==
;Notes
 

;Bibliography
*Norris J. Lacy|Lacy, Norris J. (1991). The New Arthurian Encyclopedia. New York: Garland. ISBN 0-8240-4377-4.

==External links==
 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 