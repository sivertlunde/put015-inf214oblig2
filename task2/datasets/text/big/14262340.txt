Jane and the Lost City
 
 
{{Infobox film
| name           = Jane and the Lost City 
| image          = 
| image_size     = 
| caption        = 
| director       = Terry Marcel Harry Robertson
| writer         = Mervyn Haisman
| narrator       =  Sam Jones Kirsten Hughes Jasper Carrott Robin Bailey Harry Robertson
| cinematography = Paul Beeson
| editing        = 
| distributor    = Blue Dolphin Film Distribution (UK) New World Pictures (US)
| released       =   (USA)
| runtime        = 94 min
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} newspaper strip Jane (comic strip)|Jane by Norman Pett.  An adventure comedy set during World War II, the film was directed by Terry Marcel, and stars Kirsten Hughes in the title role, Sam Jones, Maud Adams, Jasper Carrott and Robin Bailey.

== Synopsis == Colonel (Bailey) on a mission to prevent the diamonds of the fabled Lost City from falling into enemy hands.  Journeying to Africa where they are joined by Jungle Jack Buck (Jones), their quest is dogged by Nazi agents Lola Pagola and Heinrich (Adams and Carrott).
 PG (Parental Guidance) in the USA. {{cite news 
  | last = 
  | first =
  | coauthors =
  | title = Jane and the Lost City movie review
  | work = Time Out New York
  | pages =
  | language =
  | publisher = 
  | date = 
  | url = http://www.timeout.com/film/newyork/reviews/77107/Jane_and_the_Lost_City.html
  | accessdate = 2007-11-15}}  {{cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title = Jane and the Lost City
  | work = MonsterHunter
  | publisher =
  | date =
  | url = http://monsterhunter.coldfusionvideo.com/JaneLostCity.html
  | doi = archiveurl = archivedate = 2007-07-03}} 

== Cast ==
*Sam J. Jones – Jungle Jack
*Maud Adams – Lola Pagola
*Jasper Carrott – Heinrich / Herman / Hans Kirsten Hughes – Jane
*Graham Stark – Tombs
*Robin Bailey – The Colonel Ian Roberts – Carl
*Elsa OToole – The Leopard Queen
*John Rapley – Dr. Schell
*Charles Comyn – Paddy
*Ian Steadman – Capt. Fawcett
*Graham Armitage – Gen. Smythe-Paget
*Richard Huggett – Churchill
*Andrew Buckland – Grenville
*Albert Raphael – Rashleigh
*James White – Scott
*Victor Gallucci – Muller
*Patrick Hugnin – German Pilot
*John Alton – Freddy
*Magnum – Fritz the dog
*Dharmarajen Sabapathee - Indian chief

== Production notes ==
Location filming took place in Mauritius. {{cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title = Jane and the Lost City
  | work = EOFFTV
  | publisher =
  | date = 
  | url = http://www.eofftv.com/eofftv/index.php/Jane_and_the_Lost_City_(1987)
  | format =
  | doi =
  | accessdate = 2007-11-15
}} 

== Releases == Anchor Bay. 

== References ==
 

== External links ==
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 
 