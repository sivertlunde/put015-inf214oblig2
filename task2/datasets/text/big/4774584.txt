La Commune (Paris, 1871)
 
{{Infobox film
| name           = La commune (Paris, 1871)
| image          = 
| image_size     = 
| caption        = 
| director       = Peter Watkins
| producer       = 
| writer         = Agathe Bluysen Peter Watkins
| narrator       = 
| starring       = 
| music          = 
| cinematography = Odd-Geir Sæther
| editing        = Agathe Bluysen Patrick Watkins Peter Watkins
| studio         = 
| distributor    = 
| released       = Germany: 26 May 2000 (TV premiere) United States: 3 July 2003 France: 7 November 2007
| runtime        = 345 min. 220 min. (theatrical cut) France
| French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

La Commune (Paris, 1871) is a 2000 historical drama film directed by Peter Watkins about the Paris Commune. A historical re-enactment in the style of a documentary film|documentary, the film received much acclaim from critics for Watkins direction and political themes.

==Production==
La Commune (Paris, 1871) has been noted for its very large cast. It is mainly non-professional, including many immigrants from North Africa. Members did much of their own research for the project. Watkins once said of the film, "The Paris Commune has always been severely marginalized by the French education system, despite - or perhaps because - it is a key event in the history of the European working class, and when we first met, most of the cast admitted that they knew little or nothing about the subject. It was very important that the people become directly involved in our research on the Paris Commune, thereby gaining an experiential process in analyzing those aspects of the current French system which are failing in their responsibility to provide citizens with a truly democratic and participatory process." 

===Filming===
La Commune (Paris, 1871) was shot in just 13 days in an abandoned factory on the outskirts of Paris.

Like many of Watkins later films, it is quite lengthy - a long cut runs 5 hours and 45 minutes, though the more common version is 3 and a half hours long. The long version is available on DVD. The making of La Commune (Paris, 1871) was documented in the 2001  , directed by Geoff Bowie.      

==Critical reception== mean score of 90/100, indicating "universal acclaim." 

J. Hoberman of Sight & Sound magazine wrote, "Watkins restages history in its own ruins, uses the media as a frame, and even so, manages to imbue his narrative with amazing presence. No less than the event it chronicles, La Commune is a triumph of spontaneous action."  Jonathan Rosenbaum called it Watkins "latest magnum opus."  Dave Kehr, writing for The New York Times, called it "essential viewing for anyone interested in taking an exploratory step outside the Hollywood norms." 

== See also ==
*List of longest films by running time

==References==
 

==Bibliography==
* Montero, José Francisco & Paredes, Israel. Imágenes de la Revolución. La inglesa y el duque/La commune (París, 1871). 2011. Shangrila Ediciones. http://shangrilaedicionesblog.blogspot.com/2011/10/imagenes-de-la-revolucion-intertextos.html

==External links==
*   from Peter Watkins website
*   NFB
*   commentary at  .

 

 
 
 
 
 
 
 
 