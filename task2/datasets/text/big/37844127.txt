Horns (film)
{{Infobox film
| name           = Horns
| image          = File:Horns_Official_Movie_Poster.jpg
| alt            = 
| caption        = Official teaser poster
| director       = Alexandre Aja Joe Hill
| screenplay    = Keith Bunin
| based on       =   }} Joe Anderson David Morse 
| music          = Robin Coudert
| cinematography = Frederick Elmes
| editing        = Baxter
| studio         = Red Granite Pictures Mandalay Pictures
| distributor    = Dimension Films The Weinstein Company|RADiUS-TWC
| released       =  
| runtime        = 120 minutes  
| country        = United States Canada
| language      = English
| budget         = 
| gross          = $3,347,106 
}} comedy Thriller thriller film Joe Hills novel of the same name. Daniel Radcliffe stars as a man who is accused of raping and murdering his girlfriend (Juno Temple) and uses his newly discovered paranormal abilities to uncover the real killer.
 world premiere at the 2013 Toronto International Film Festival, and was released theatrically in the United States on October 31, 2014.

Upon its release, the film received mixed reviews from critics, praising the originality and Daniel Radcliffes performance but criticizing the script.

==Plot==
Ignatius "Ig" Perrish is the prime suspect when his girlfriend Merrin is raped and murdered. Despite his declarations of innocence, he is shunned by the community, and only his childhood friend Lee seems to completely believe him. Even his parents and his brother Terry seem pessimistic about the situation. After a nighttime vigil led by Merrins father, who believes Ig to be guilty, Ig drinks heavily and is taken home by another childhood friend of his — Glenna — only to wake up the next morning with a pair of horns protruding from his head. He soon learns that the horns force people to reveal their darkest inner-desires to him and they seek his permission to enact them. While at his doctor to try and remove his horns, Ig, under anaesthesia, dreams of his childhood where he first met Merrin after the death of her mother from cancer. After Merrin used a cross necklace in church to get his attention, Ig found it broken in a pew and had his friend Lee repair it so he could return it to her. In exchange, Lee got a cherry bomb Ig had received earlier for a dangerous stunt he performed and from which Lee saved him from drowning. While Ig returned Merrins necklace, Lee accidentally blew off two of his own fingers with the cherry bomb.

Ig decides to test his horns on his parents, learning the disheartening truth from them that they are disappointed in him for not being as talented as Terry, who is a professional musician. However, he finds Lee seemingly both unaffected by the horns and unable to see them. Learning that a new witness for the prosecution is the waitress who saw Merrin break up with him on the night she was killed, Ig uses his horns to learn she had been embellishing her stories in order to get on television and become famous. He approaches Terry to reveal this to him, and learns from him that he drove her home that night, something he hadnt told police. Touching his skin, Ig sees what happened that night: Merrin had left his car en route and run into the forest; Terry passed out, awakening the next morning with a bloodied rock in his hand and discovering her dead body, though he did not remember killing her. This enrages Ig, who brutally beats Terry up until another friend from their childhood — Eric, now a police officer — arrests him.

Ig discovers snakes follow him wherever he goes, and embraces his abilities, using them to exact vengeance against the waitress, wounding her so that her vanity is no longer a reason for her to continue to lie about what she saw that night. He forces Terry to go on a drug binge to relive the night of Merrins death, for not admitting his side of the story in order to save his own skin. Ig has Lee meet him at the docks, and notices that he is wearing Merrins cross. After he removes it, Lee suddenly sees the horns that it had protected him from and falls under their influence, admitting he killed Merrin. Ig uses his abilities to see that Lee followed Merrin into the forest and was enraged that she still loved Ig; Lee thought she broke up with Ig in order to be with him. Lee then raped Merrin, fatally struck her in the head with a rock — which he planted on Terry — and stole Merrins necklace. Weakened by his anger, Ig is overpowered by Lee, who brutally beats him, then douses him with gasoline and lights him on fire in his car. Ig drives into the bay to put out the fire and apparently drowns.  Lee reports to the news that Ig confessed to him before committing suicide, but Ig survives his injuries due to his powers.

After meeting with Merrins father, who no longer believes him to be responsible for her death, he gives Ig a key to a lock box in their treehouse, and allows Ig to keep her necklace when Ig offers it to him. When he puts it on, his body is restored and his horns disappear. He goes to the treehouse and finds a note from her that explains that she knew he was going to propose, and that she wanted to be his wife, which makes Ig happy. However, she was suffering from the cancer that had killed her mother and she couldnt put him through that, so she made a pretense of leaving to be with someone else in order to push him away. After visiting Terry in the hospital to tell him that he is alive, Ig confronts Lee - who does not remember their earlier confrontation due to the horns influence - and leads him into the woods to the treehouse. When Terry arrives with Eric, who points a shotgun at Lee, he confesses again to the murder. At first appearing cooperative, Lee kills Eric and severely injures Terry. As he prepares to kill Ig, Ig tears off the necklace, sprouting a pair of angelic wings that cause him to take flight. The wings are soon engulfed by a fire that also consumes Igs body, turning him into a demonic creature who uses his preternatural strength to shrug off Lees attacks before impaling Lee on his horns. With Merrins killer dead, Ig dies from his injuries, claiming that his vengeance was all-consuming. It is implied, though, that Ig is reunited with Merrin in the afterlife.

==Cast==
* Daniel Radcliffe as Ignatius "Ig" Perrish, a 26-year-old man who wakes up after a drunken night to find two protrusions growing from his forehead, which give him the power to compel people to unravel their deepest secrets.    Radcliffe stated, "It’s a very, very different type of part than anything Ive done before",       adding that the role was "deeply emotional and also incredibly outrageous in some ways".   
** Mitchell Kummen as young Ig
* Max Minghella as Lee Tourneau, Igs best friend from childhood, who lost two of his fingers from a cherrybomb explosion. He is apparently the only one who completely believes in Igs innocence and tries to defend him.
** Dylan Schmid as young Lee Joe Anderson as Terry Perrish, Igs alcoholic, drug-addicted older brother. A talented musician who believed he was the last one to see Merrin alive, but he didnt come forward as that would have ruined his reputation and made him a suspect in her murder.
** Jared Ager-Foster as young Terry
* Juno Temple as Merrin Williams, Igs late girlfriend whose death transpired one year prior.   
** Sabrina Carpenter as young Merrin
* Kelli Garner as Glenna
** Laine MacNeil as young Glenna
* James Remar as Derrick Perrish
* Kathleen Quinlan as Lydia Perrish
* Heather Graham as The Waitress David Morse as Dale Williams
* Alex Zahara as Dr. Renald
* Kendra Anderson as Delilah
* Michael Adamthwaite as Eric Hannity
** Erik McNamee as young Eric
* Desiree Zurowski as Radio reporter

Shia LaBeouf was originally set to play the lead, but was later replaced by Radcliffe.      
 Joe Andersons character on lead trumpet is The Brass Action from Vancouver, British Columbia. The scene features the bands song, "The Devil Down Below". 

==Production==
  in July 2014]] Joe Hills cult book, I couldnt resist temptation to dive into the devilish underworld and reinvent a universal myth".   
 Mission and Squamish, British Columbia|Squamish,          completing shooting in December 2012.    

==Release== world premiere was held at the 2013 Toronto International Film Festival,   

The film was released in North America and the United Kingdom on October 31, 2014,       however the movie was also made available via digital download on iTunes as of October 6, 2014. 

Horns grossed a total of $3,347,106 worldwide in 31 days of release.  
 distribution rights were acquired by Dimension Films and The Weinstein Company|RADiUS-TWC.    Anchor Bay Entertainment released the film on January 13, 2015 on DVD and Blu Ray.  

===Critical response===
 
Horns received mixed reviews from critics. Review aggregator Rotten Tomatoes gives a 43% approval rating, based on 102, with an average rating of 5.2/10. The consensus says "Horns is a bit of a tonal jumble, but it offers enough thoughtful horror-comedy -- and strong work from Daniel Radcliffe -- to hook genre enthusiasts."   

John DeFore of The Hollywood Reporter gave a positive review of the film, remarking, "While this all begins as a kind of supernatural black comedy ... the tone grows darker with each revelation".   
 Joe Hill, who wrote the novel the film is based on, praised Radcliffes performance, calling it a "wrenching, vulnerable, emotionally naked performance that isn’t like anything he’s ever done on screen before. He is such a wonderful Ig Perrish".   

The Guardian scored the film two out of five stars, calling it "a Dogma (film)|Dogma-style mash-up of grim comedy and religious satire".    Eric Kohn of IndieWire wrote, "Predominantly a failure of tone, "Horns" has plenty of admirable traits and yet dooms itself from the outset. Its an admirable conceit stuffed into far less subtle material".    Peter Debruge of Variety (magazine)|Variety sensed that the film "benefits from the helmers twisted sensibility, but suffers from a case of overall silliness".    Jonathan Weichsel of MoreHorror.com stated that "The cast is all around terrific, especially Daniel Radcliffe who is nothing short of phenomenal, and the set pieces are entertaining in that wild, over the top way that only horror can pull off effectively.".   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 