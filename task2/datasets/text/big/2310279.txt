Mad Love (1935 film)
{{Infobox film | name =Mad Love
  | image =File:Mad Love lobby card.JPG
  | caption =Lobby card
  | director = Karl Freund	
  | producer = John W. Considine Jr.
  | writer = Screenwriters:   Frances Drake Colin Clive Ted Healy Sara Haden Dimitri Z. Tiomkin
  | cinematography = Chester A. Lyons Gregg Toland
  | editing = Hugh Wynn
  | distributor = Metro-Goldwyn-Mayer
  | released =  : August 2, 1935
  | runtime = 68 min English
  | budget = $403,000 
    }}
 Mad Love}} American horror Frances Drake as Yvonne Orlac and Colin Clive as Stephen Orlac. The plot revolves around Doctor Gogols obsession with actress Yvonne Orlac. When Stephen Orlacs hands are destroyed in a train accident, Yvonne brings him to Gogol, who claims to be able to repair them. As Gogol becomes obsessed to the point that he will do anything to have Yvonne, Stephen finds that his new hands have made him into an expert knife thrower.

Mad Love was Freunds final directorial assignment and Lorres American film debut. Critics praised Lorres acting, but the film was unsuccessful at the box office. Film critic Pauline Kael found the film unsatisfactory, but argued that it had influenced Citizen Kane. Cinematographer Gregg Toland was involved in the production of both films. Mad Loves reputation has grown over the years, and it is viewed in a more positive light by modern film critics.

==Plot== Frances Drake) rests after her final performance at the Théâtre des Horreurs (syled after the Grand Guignol) in Paris, France. As she listens to her husband Stephen Orlac (Colin Clive) play the piano on the radio, she is greeted by Dr. Gogol (Peter Lorre), who has seen every show featuring Yvonne, and unaware of her marriage, is aghast to learn that she is moving to England with her husband. Gogol leaves the theater heartbroken, buys the wax figure of Yvonnes character, refers to it as Galatea (from the Greek myth), and arranges that it be delivered to his home the following day.

Stephen Orlac is on a train journey from Fontainebleau to Paris, where he sees murderer Rollo the Knife thrower (Edward Brophy), who is on the way to his execution by guillotine. (Gogol later witnesses the execution, along with the American reporter Reagan (Ted Healy)). Orlacs train crashes later that night, and Yvonne finds her husband with mutilated hands. She takes Stephen to Gogol in an attempt to reconstruct his hands, and Gogol agrees to do so. Gogol uses Rollos hands for the transplant, and the operation is a success.

The Orlac couple are forced to sell many of their possessions to pay for the surgery, while Stephen finds he is unable to play the piano with his new hands. When a creditor comes to claim the Orlacs piano, Stephen throws a fountain pen that barely misses his head. Stephen seeks help from his stepfather, Henry Orlac (Ian Wolfe). Henry denies the request, upset that Stephen did not follow in his line of business as a jeweler. A knife thrown in anger by Stephen misses Henry, but breaks the shop fronts window. Gogol meanwhile asks Yvonne for her love, but she refuses. Stephen goes to Gogols home and demands to know about his hands, and why they throw knives. Gogol suggests that Stephens problem comes from childhood trauma, but later confirms to his assistant Dr. Wong (Keye Luke) that Stephens hands had been Rollos.

Gogol then suggests to Yvonne that she get away from Stephen, as the shock has affected his mind and she may be in danger. She angrily rejects Gogol, whose obsession grows. Henry Orlac is murdered, and Stephen receives a note that promises that he will learn the truth about his hands if he goes to a specific address that night. There, a man with metallic hands and dark glasses claims to be Rollo, brought back to life by Gogol. Rollo explains that Stephens hands were his, and that Stephen used them to murder Henry. Stephen returns to Yvonne and explains that his hands are those of Rollo, and that he must turn himself in to the police. A panic-stricken Yvonne goes to Gogols home, and finds him completely mad. Gogol assumes that his statue has come to life, embraces her, and begins to strangle her. Reagan, Stephen and the police arrive, but are only able to open the observation window. Stephen produces a knife and throws it at Gogol, then finds his way in. Gogol dies as Stephen and Yvonne embrace.

==Production== MGM with an original translation/adaptation of Renards story "Les Mains DOrlac". Mank, 2001. p.125  Writer Guy Endore, who worked with director Karl Freund on early drafts.  Producer John W. Considine Jr. assigned the continuity and dialogue to P.J. Wolfson,  and John L. Balderston began to write a "polish-up" of the draft on April 24, 1935.  Balderstone went over the dialogue with Lorre in mind, and at points called for the actor to deploy his "M (1931 film)|M look".  Balderston continued his re-write three weeks past the start of filming. Mank, 2001. p.130 
 Chester Lyons Frances Drake recalled difficulty between Freund, Toland and Considine. Drake said that "Freund wanted to be the cinematographer at the same time", and that "You never knew who was directing. The producer was dying to, to tell you the truth, and of course he had no idea of directing." Mank, 2001. p.140  Several titles for the film were announced, and on May 22, 1935, MGM announced that the title would be The Hands of Orlac. Mank, 2001. p.147  The Mad Doctor of Paris was also suggested, but the studio eventually settled on the original title of Mad Love.  Shooting finished on June 8, 1935, one week over schedule.  After the initial release, MGM cut about fifteen minutes of scenes from the film.  Cut scenes included the surgery to get Rollos hands, a pre-credit warning scene similar to the one in Frankenstein (1931 film)|Frankenstein, and Isabel Jewells entire portrayal of the character Marianne. 

==Cast== Crime and MGM announced American film debut. Mank, 2001. p.129  Mank, 2001. p.122 
*Frances Drake as Yvonne Orlac. The role was originally intended for Virginia Bruce. Mank, 2001. p.126  Drake also played Yvonnes wax figure in closeup shots. Mank, 2001. p.132 
*Colin Clive as Stephen Orlac. Clive was on loan from Warner Bros. at the time. 
*Ted Healy as Reagan, an American reporter
*Sara Haden as Marie, Yvonnes maid
*Edward Brophy as Rollo the Knife Thrower
*Henry Kolker as Prefect Rosset
*Keye Luke as Dr. Wong
*May Beatty as Françoise, Gogols drunken housekeeper

==Release and reception== Karloff melodrama, an interesting but pretty trivial adventure in Grand Guignol horror." {{cite web |url=http://movies.nytimes.com/movie/review?res=9403E4DC113DE53ABC4D53DFBE66838E629EDE
 |title=THE SCREEN; Peter Lorre in His First American Photoplay, "Mad Love," on View at the Roxy Theatre. |author=Sennwald, Andre|date=1935-08-05|accessdate=2008-08-04 |work=The New York Times}}  Mad Love was not a hit at the box office, and had a small domestic gross of $170,000. Mank, 2001. p.149  Its foreign gross was larger, at $194,000. 

Critic Pauline Kaels essay "Raising Kane", published in The New Yorker, accused director Orson Welles of copying the visual style of Mad Love for Citizen Kane. Mank, 2001. p.154  Mank, 2001. p.155  Kael noted that both Gogol and Kane are bald, Gogols house and Kanes "Xanadu" are similar, and that Gogol and Kane both have a pet cockatoo.  She also wrote that Toland had "passed Freunds technique onto Welles".  Peter Bogdanovich wrote a rebuttal to Kaels statements in Esquire (magazine)|Esquire in 1972. Both writers had negative views on Mad Love. Kael called it a "dismal static horror movie", and Bogdanovich said that it was "one of the worst movies Ive ever seen." 

Recent reviews of Mad Love have been much more positive. At the online film review database Rotten Tomatoes, the film has a 100% approval rating, and a 7.5 critical average. {{cite web |url=http://www.rottentomatoes.com/m/1038641-mad_love/
 |title=Mad Love |accessdate=2008-08-04 |publisher=Rotten Tomatoes}} 

==Home video== Doctor X, The Devil-Doll, Mark of the Vampire, The Mask of Fu Manchu and The Return of Doctor X, in the three-disc Hollywood Legends of Horror Collection.    DVD bonus material included film commentary by Steve Haberman, and the films theatrical trailer. 

== Remakes == remade once, silent film The Hands British co-production magician character named Nero.   
 Alfred Hitchcocks unproduced projects was The Blind Man, a variant of the theme in which a blind pianist receives implanted eyes whose retinas retain the image of their previous owners murder.

==See also==
*The Beast with Five Fingers (1946) - a variant on the theme, also starring Peter Lorre. The Hand (1981) - remake of the 1946 film.
* Blink (film)|Blink (1994) - a variant on Hitchcocks The Blind Man theme
*Under the Volcano, the novel published by Malcolm Lowry in 1947, includes many references to the 1935 film, under its Spanish title, "Las Manos de Orlac. Con Peter Lorre". Mad Love, is supposed to be broadcast at the local theatre in Quaunahuac (Cuernavaca, Mexico) when the characters, the ex-Consul Geoffrey Firmin, his former wife Yvonne and Geoffreys brother come to be reunited. The film and the novel have many themes in common : a strong feeling of alienation, impossible and desperate love, jealousy, the guilty conscience of the creator compromised with evil.
* , a music video by Tim Burton inspired by the film, starring Winona Ryder.

==Notes==
 

==References==
*{{cite book
 | last= Youngkin
 | first= Stephen D.
 |author2=James Bigwood |author3=Raymond Cabana
  | title= The Films of Peter Lorre
 |publisher= Citadel Press
 |year= 1982
 |isbn= 0-8065-0789-6
}}
*{{cite book
 | last= Mank
 | first= Gregory William
 | title= Hollywood Cauldron: Thirteen Horror Films from the Genres Golden Age McFarland
 |year= 2001
 |isbn= 0-7864-1112-0
}}
*{{cite book
 | last= Johnson
 | first= Tom
 | title= Censored Screams: The British Ban on Hollywood Horror in the Thirties McFarland
 |year= 2006
 |isbn= 0-7864-2731-0
}}

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 