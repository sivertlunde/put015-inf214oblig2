Battle in Heaven
{{Infobox film image              =Batallaenelcieloposter.jpg
 name               =Battle in Heaven director           =Carlos Reygadas producer           =Philippe Bober Susanne Marian Carlos Reygadas Jaime Romandia Joseph Rouschop writer             =Carlos Reygadas starring           =Marcos Hernández Anapola Mushkadiz Berta Ruiz music              =John Tavener cinematography     =Diego Martínez Vignatti editing            =Adoración G. Elipe Benjamin Mirguet Carlos Reygadas Nicolas Schmerkin executive producer =Hamish McAlpine Studio               ==Nodream Cinema Mantarraya Producciones runtime            =98 minutes country            =Mexico language           =Spanish
|}}

Battle in Heaven ( ) is a 2005 Mexican-French-German film. It is the second feature film by director Carlos Reygadas who previously directed the Mexican film Japón (film)|Japón. It was entered into the 2005 Cannes Film Festival.    Reygadas has said about this film: "it’s my problem child, and therefore the film of mine I love the most." 

==Plot== Marcos Hernández) is a working class man in Mexico, employed by "the general." Marcos learns that the baby that he and his wife kidnapped for ransom had accidentally died. The remainder of the film follows a despondent Marcos, seemingly haunted by the moral and/or legal implications of his actions.
 sex trade. While driving, Marcos is very distracted, and at one point stalls the car.  Ana recognises that something is wrong, but Marcos claims hes distracted only because of his wifes supposed ill health.

Ana invites Marcos into the "boutique" so that he can have sex with one of her "friends." Marcos is apparently not aroused by the "friend." The friend tells Ana that Marcos would prefer her instead. Ana goes to talk to Marcos, and reminds him that they have known each other since her childhood. Marcos then reveals that he and his wife kidnapped a baby but the baby died before they could collect any ransom. Ana seems to remain composed at hearing this news.

Back home, Marcos has sex with his wife, Berta. They seem united in their sorrow regarding the dead baby. Marcos tells Berta that he told Ana about the kidnapped baby, indicating that the confession brought him relief. Berta, upset, demands that he make sure that "the princess" does not tell anyone. The next day, Marcos visits Ana. She seems annoyed by his visit, but drives him to her place where they have sex. Ana advises Marcos to turn himself in to the police.

Marcos, Berta, their son, and a few friends (including the mother of the dead baby, who does not know who took her child, nor that it has died) go out to the countryside. Marcos tells Berta that he is going to turn himself in.  She asks him to wait until after the pilgrimage (which is in honor of the Lady of Guadalupe), an event that Marcos had earlier shown disdain for. Marcos seems to agree with his wife. Marcos mental state seems to worsen. Instead of driving back with his party, he treks through the countryside. He reaches a peak with Christian crosses, overlooking a valley. Marcos buries his face in his hands.

Marcos visits Ana at her home. He tells her that he will turn himself into the police that day. She gives him a goodbye kiss. Marcos leaves the apartment. He pees his pants, goes back to the apartment and fatally stabs Ana.

The police become aware of both the attack on Ana and the death of the baby, and are in search for Marcos. Marcos seems to have joined the pilgrimage to the Basilica, at first on foot, and then on his knees. Someone places a hood over his face, but Marcos continues to hobble forward. The hood becomes increasingly stained with blood as he makes his way into the Basilica during the church service.

Eventually the pilgrims are gone, and the Basilica is vacant. The police allow Berta to go in to see her husband. She touches him on the head and he collapses.

==Cast== Marcos Hernández - Marcos
* Anapola Mushkadiz - Ana
* Bertha Ruiz - Marcos Wife David Bornstein - Jaime
* Rosalinda Ramirez - Viky
* El Abuelo - Chief of Police
* Brenda Angulo - Madame
* El Mago - Preacher
* Francisco El Gato Martínez - Gas Station Attendant
* Diego Martínez Vignatti - Soccer Player
* Alejandro Mayar - Police Inspector
* Chavo Nava - Neurotic Conductor
* Estela Tamariz - Ines

==Production==
Like Robert Bresson, Reygadas prefers to use non-professional actors,  while occasionally recycling one (Hernández had a small part as a chauffeur in Japón). Unlike Bresson, Reygadas puts explicit sex scenes in his films. The sex scene between Hernández and Ruiz was simulated at Ruizs husbands request, according to Reygadas on an interview included on the DVD.
 Primera División de México Apertura 2003, which ended with a draw & no goals scored.

==Critical reception==
Jonathan Romney says that "To a degree, Battle in Heaven might seem like another warmed-over example of a familiar movie myth: a fairly repellent no-hoper redeemed by hot sex with a quasi-virginal prostitute," but that "its finally hard to know whether Reygadas takes his transcendental, religious theme seriously, or is deriding it outright - or even deriding us for taking it seriously." 

Lisa Schwarzbaum gives the film a grade of D+. "Between those two attention grabbers on a theme of flagpoles, languorously performed and indifferently observed, Mexican filmmaker/provocateur Carlos Reygadas pitches his own fight for the aesthetic tolerances of viewers, goading us to react to images about which he himself studiously offers no opinion." Schwarzbaum finds that "for all the shock of the movies clinical carnality, this battle is lost." 

==Awards==

*Rio de Janeiro International Film Festival, FIPRESCI Prize

==See also==
Unsimulated sex in film

==References==
 

==External links==
* 
* 
* 
*  by Peter Fraser, Close-Up Film
*  by Chris Neumer, Stumped Magazine

 

 
 
 
 
 
 