Traffic Signal (film)
 
 
 
 
{{Infobox film
| name           = Traffic Signal
| image          = TrafficsignalMovie Poster.jpg
| caption        = Theatrical release poster
| director       = Madhur Bhandarkar
| producer       = Percept Pictures Company
| writer         = Sachin Yardi Madhur Bhandarkar
| starring       = Kunal Khemu Neetu Chandra Konkona Sen Sharma Ranvir Shorey
| music          = Shamir Tandon
| cinematography = Mahesh Limaye
| editing        =
| distributor    = Percept Picture Company Bhandarkar Entertainment
| released       =  
| runtime        =
| language       = Hindi
| country        = India
| budget         =  
| gross          =  
}}

Traffic Signal is a  , 11 June 2008. 

== Plot ==
This film is about the lives and travails of people living around a fictitious traffic signal in Mumbai. Anybody who drives in Mumbai has experienced the 2-odd minute wait at a traffic signal.
 eunuchs and others who sell clothes, flowers and trinkets. They speak quickly, act fast and operate somewhat honourably (with each other) to eke a meager living out of the harsh Mumbai street life. They owe allegiance and hafta (weekly "protection" fee) to the signal manager, Silsila (Kunal Khemu).

Silsila grew up at the signal. He ran various trades there before he became the manager. He is an ideal manager: sensitive and caring of his workers yet ruthless when it comes to delivery. Silsila reports into a mid-level don, Jaffar, who in turn reports to the big boss Haji bhaijaan.

Life is well at the signal. A gentle girl, Rani (Neetu Chandra), arrives at the signal to sell ethnic clothes. The initial fireworks blossom into deeper companionship with Silsila. Everything proceeds normally.

Unbeknownst to Silsila, Haji bhaijaan is part of a larger nexus of evil that comprises politicians and the larger Mafia. Haji must play a dangerous game where he is forced to invoke the unwitting pawn, Silsila, to start a series of events that could ultimately destroy the traffic signal. Silsila is unaware of the larger consequences as he carries out his orders without question. But reality dawns on him when he is apprehended in a case of murder, extortion and bribery where he was hardly aware of things.

The signal faces destruction. Silsilas world, and all the people who grew up with it, face extinction. Silsila is forced to make a choice between his life and his world. As his life moves from Green to Red, he may only hope that the signal moves from Red to Green.

== Cast ==
* Kunal Khemu as Silsila
* Neetu Chandra as Rani
* Ranvir Shorey as Dominic
* Konkona Sen Sharma as Noorie
* Upendra Limaye as Mangaya
* Sudhir Mishra as Haji Bhaijaan
* Gopal K. Singh as Samari
* Madhu Sharma as Businessmans Wife
* D. Santosh (Actor)|D. Santosh as Jaffar Bhaai Manoj Joshi as Chief Engineer Sailesh Jha
* Vinay Apte as Inspector Sheikh

== Awards ==
* 2007 : National Film Award for Best Direction for Madhur Bhandarkar Best Make-up Artist for Anil Moti Ram Palande

==Reception== Page 3 (2005) and Corporate (film)|Corporate (2006).  Konkona Sen Sharmas acting is critically acclaimed and she got very good reviews for her role. Reviews state that Konkona Sen Sharma is able to bring a shred of credibility to her part. 

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 