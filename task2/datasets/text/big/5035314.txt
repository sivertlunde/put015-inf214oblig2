Cleopatra (1917 film)
 
{{Infobox film
| name           = Cleopatra
| image          = Cleopatra1917.jpg
| caption        = Original release poster
| director       = J. Gordon Edwards William Fox Adrian Johnson William Shakespeare (plays Anthony and Cleopatra and Julius Caesar) Victorien Sardou Émile Moreau (writer)|Émile Moreau (play Cléopâtre)
| starring       = Theda Bara Fritz Leiber, Sr. Thurston Hall
| music          = José Martínez
| cinematography = John W. Boyle Rial Schellinger George Schneiderman
| editing        = Edward M. McDermott
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 125 mins. (11 reels)
| country        = United States Silent English intertitles
| budget         = $500,000
}}
 silent historical drama film based on H. Rider Haggards 1889 novel Cleopatra (1889 novel)|Cleopatra and the plays Cleopatre by Émile Moreau (writer)|Émile Moreau and Victorien Sardou and William Shakespeares Antony and Cleopatra.  The film starred Theda Bara in the title role, Fritz Leiber, Sr. as Julius Caesar and Thurston Hall played Mark Antony.

==Production notes==
 

Cleopatra was one of the most elaborate Hollywood films ever produced up to that time, with particularly lavish sets and costumes.  According to the studio, the film cost $500,000 (approximately $8.3 million in 2009) to make and employed 2,000 people behind the scenes. Theda Bara appeared in a variety of costumes, some quite risqué. The film was a great success at the time.
 Leon Schlesinger Productions, the production company behind the Looney Tunes and Merrie Melodies cartoons; its disposition after the acquisition of that company by Warner Bros. is unknown. 

==Cast==
*Theda Bara as Cleopatra Fritz Leiber as Caesar
*Thurston Hall as Antony Albert Roscoe as Pharon 
*Herschel Mayall as Ventidius
*Dorothy Drake as Charmian
*Delle Duncan as Iras
*Henri De Vries as Octavius Caesar
*Art Acord as Kephren Hector V. Sarno as Messenger 
*Genevieve Blinn as Octavia

==Preservation status==
 
The majority of the film is now considered a lost film,   as no known complete negatives or prints of it survive. Brief fragments of footage from the film are known to exist today.    
 obscene to Fox studios in 1937 (along with the majority of Baras other films for Fox) and at the Museum of Modern Art in New York.    Only a few fragments survive today. 

==See also==
* List of American films of 1917
* Cleopatra VII
* List of incomplete or partially lost films

==Notes==
 

== External links ==
 
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 