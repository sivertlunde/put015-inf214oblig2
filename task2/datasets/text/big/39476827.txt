Ape (2012 film)
 
{{Infobox film
| name           = Ape
| image          =
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Joel Potrykus
| producer       = Joel Potrykus Ashley Young Michael Saunders Kevin Clancy
| writer         = 
| screenplay     = Joel Potrykus
| story          = 
| based on       =  
| narrator       = 
| starring       = Joshua Burge
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Factory 25
| released       =  
| runtime        = 86 min
| country        = United States
| language       = 
| budget         = 
| gross          = 
}}
Ape is a 2012 American independent film written and directed by Joel Potrykus, starring Joshua Burge as Trevor Newandyke.  The film is notable for reigniting the American indie slacker niche of the mid 90s, both in aesthetic and voice. 

==Plot==
A black comedy and rage fantasy, the film follows failing stand-up comic Trevor as he suffers one humiliation after another, both on stage and off. His only outlet is a secret pyromania on display at home and in public.  His anger hits the streets after making a deal with a man dressed as the Devil. 

==Cast==
*Joshua Burge as Trevor Newandyke

==Release==
Ape made its world premiere at the 2012 Locarno Film Festival, where it won Best New Director and Best First Feature Special Mention at the festival.  It went on to make its North American premiere at the Vancouver International Film Festival,  and US premiere at AFI Fest in Hollywood.  The film received a theatrical release through Factory 25. 

==Reception==
Ape has received generally favorable reviews from critics. Rotten Tomatoes gave the film a rating of 100%, based on 5 reviews. 

==References==
 

==External links==
* 
* 

 
 
 
 
 

 