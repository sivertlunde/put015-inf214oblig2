Bridge of Dragons
{{Infobox film
| name           = Bridge of Dragons
| image          = Bridge of Dragons.jpg
| caption        =
| director       = Isaac Florentine
| producer       =
| writer         = Carlton Holder
| starring       = Dolph Lundgren
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 92 minutes.
| country        = United States English
| Budget         =
| awards         =
| Gross          =
}} American Romance romantic Science fiction film|Sci-fi action film directed by Isaac Florentine, and starring Dolph Lundgren as a cold and tough mercenary. It co-stars Cary-Hiroyuki Tagawa, who Lundgren had previously worked with in the 1991 film Showdown in Little Tokyo. It was the first Nu Image film to be shot in Bulgaria.

==Plot==
The tough and cold mercenary Warchild, is working for the man who took care of his war training and upbringing, the greedy General Ruechang. Ruechang is planning to take over the country by marrying Princess Halo. But Halo discovers that Ruechang killed her father to gain more power than he had working for the King, so she decides to run away. Warchild is the one who has to bring her back to Ruechang, but the one thing no one counted on happens... 

==Cast==
*Dolph Lundgren ...  Warchild
*Cary-Hiroyuki Tagawa ...  General Ruechang
*Valerie Chow ...  Princess Halo (as Rachel Shane) Gary Hudson...  Emmerich John Bennett ...  The Registrar
*Scott L. Schwartz ...  Belmont
*Jo Kendall ...  Lily (The Maid)
*Dave Nichols ...  York
*Bashar Rahal ...  Robert
*Velizar Binev ...  The Tailor
*Ivan Istatkov ...  Arena Ring Master
*Nikolay Binev ...  The Doctor
*Boyan Milushev ...  Jake
*Sevdelin Ivanov ...  Scott
*Brian Fitkin ...  Renegade Leader

==External links==
*  

 

 
 
 
 
 
 
 

 