Asterix in Britain (film)
{{Infobox film name = Asterix in Britain caption = French theatrical release poster image = Asterix in Britain (Astérix chez les Bretons) poster.jpg director = Pino Van Lamsweerde released =   runtime = 79 min. writer = Pierre Tchernia adapted from René Goscinny Albert Uderzo producer = Philippe Grimond Yannik Piel starring = French
*Roger Carel (Asterix)
*Pierre Tornade (Obelix)
English
*Jack Beaber (Asterix)
*Billy Kearns (Obelix) music = Vladimir Cosma country = France, Italy language = French
}}
 book of the same name. The theme song The Lookout is Out was performed by Cook da Books based on the previous films theme but with a slower tempo, played with acoustic guitars and brand new English lyrics.

==Plot==
Asterix and Obelix must travel to Britain with a barrel of Magic potion, to help a rebel village fight against the Roman Empire, which has conquered the whole country.

==Changes from the book==
The movie contains many changes from the original story:
*Dogmatix comes along with the heroes for the journey and gets a subplot involving him following the barrel thief to his house.
*Caesars role in the film is larger, and he has more screen time. (He had only a cameo appearance in the book)
*Some of the characters (such as the Roman centurions and the British pubkeeper) have different names and designs.
*Asterix and Obelix meet the pirates on their way to Britain.
*Asterix doesnt get tea from Getafix, but from some Phoenicians who Obelix saved from the pirates.
*The search for the stolen barrel of potion is shorter than in the book.
*Stonehenge makes an appearance in the movie.
*In the book, Asterix found out that the barrel was sold to the rugby team by the Pubkeeper, whilst in the film the rugby team were simply the first on the list.
*During the meeting with the pirates: On the way back, the captain sank his own ship, much like in the book Asterix and Cleopatra.
*In the English dub, Fulliautomatix and Impedimenta were renamed to Blacksmix and Instantmix respectively.
*In the book, the uniform of the rugby team that had the magic potion, Camulodunum, was white and blue, identical to Colchester FCs current colours.

==See also==
*List of animated feature-length films

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 
 