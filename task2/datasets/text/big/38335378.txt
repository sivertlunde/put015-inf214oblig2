The Witness Chair
{{Infobox film
| name           = The Witness Chair
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = George Nichols Jr.
| producer       = Samuel J. Briskin
| writer         = Rita Weiman
| based on       = 
| screenplay     = Rian James Gertrude Purcell
| narrator       = 
| starring       = Ann Harding Walter Abel
| music          = Roy Webb
| cinematography = Robert De Grasse
| editing        = William Morgan
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 64 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

The Witness Chair is a 1936 courtroom drama film directed by George Nichols Jr., starring Ann Harding and Walter Abel. After this movie, Harding briefly retired from films, and the following year she married symphony conductor Werner Janssen.

==Plot ==
Late one night, secretary Paula Young (Ann Harding) leaves the office of her boss, Stanley Whittaker (Douglas Dumbrille, locking the door and taking the stairs to avoid being seen by the elevator operator (Frank Jenks). The next morning, the cleaning lady finds Whittakers dead body, an apparent suicide. Police Lieutenant Poole (Moroni Olsen) finds a letter signed by Whittaker in which the deceased states he embezzled $75,000. Soon, however, he suspects otherwise and, after investigating, arrests widower James "Jim" Trent (Walter Abel), the vice president of Whittaker Textile Corporation. The gun that fired the fatal shot belongs to Trent, and the typewritten suicide note, though signed by Whittaker, specifically states that Trent is not involved in the embezzlement. 

The trial goes badly for the defendant. The elevator operator recalls seeing only  Whittaker and Trent in the office building that night, and Martin (Paul Harvey), the prosecuting attorney, produces a possible strong motive: Trents daughter Connie intended to run away with Whittaker that night. However, Paula interrupts the proceedings to claim responsibility for the crime. She had guessed that Whittaker intended to flee the country with Connie (she being unaware of his embezzlement) when two ship tickets were delivered to the office. With strong, concealed feelings for Trent, Paula forced Whittaker at gunpoint to sign the confession she had typed. However, Whittaker then tried to grab the gun, only to be fatally shot in the struggle. Trent asks Paula to marry him.

==Cast==
* Ann Harding as Paula Young  
* Walter Abel as James "Jim" Trent  
* Douglass Dumbrille as Stanley Whittaker  
* Frances Sage as Constance "Connie" Trent  
* Moroni Olsen as Police Lieutenant Poole   Margaret Hamilton as Grace Franklin, the bookkeeper  
* Maxine Jennings as Tillie Jones, Trents secretary  
* William Billy Benedict as Benny Ryan, the office boy  
* Paul Harvey as Prosecuting Attorney Martin  
* Murray Kinnell as Defense Attorney Conrick  
* Charles Arnt as Mr. Henshaw, the auditor  
* Frank Jenks as Roy Levino, the elevator operator  

==References==
 	

== External links ==
*  
*  
*  

 
 
 
 
 
   
 