Yaaruku Yaaro
 
 

 
{{Infobox film
| name = Yaaruku Yaaro
| image =
| caption =
| producer = Erode Jayakumari
| director = Joe Stanley
| writer = Joe Stanley
| cinematography = Joe Stanley
| editing = Joe Stanley
| music = Joe Stanley
| starring = Sam Anderson Varnika Jothi Venniradai Moorthy
| studio = Universal Thavamani Cine Arts
| country = India
| language = Tamil
| runtime = 2 Hrs
}}
Yaaruku Yaaro (  Jayakumari under the banner of Universal Thavamani Cine Arts. The story, screenplay, dialogues, lyrics, music and direction were by Joe Stanley. The movie stars Sam Anderson, alongside newcomers Jothi and Varnika.

==Synopsis==
The film is about an aspiring automobile engineer who owns a motor workshop. He plans to manufacture a family car which can be sold for 70000 Indian Rupees. How the two heroines help him in this quest forms the crux of the story.The catch phrase for the film is "Step Nee".

==Plot==
The story opens as Deepa (Varnika), a medical college student, sings a song during the cultural festival. She is being surrounded by lush green fields and mountains but the college, or even the stage for that matter, is nowhere in sight. The next scene shifts to three robbers, ogling at Deepa. She is on her way to sit for her college examinations. But while wheeling her punctured scooter, the robbers snatch her chain and escape on a bike. In comes hero David (Sam Anderson) who does a chase after them. He returns empty-handed and recognizes Deepa from the college function earlier. He praises her amazing vocal skills and gives his gold crucifix chain as a gift. Deepa refuses, but David then gives a sermon about Hindu-Christian difference. Yet, she still does not accept it. David also gives his stepnee (spare) tyre to Deepa. This incident is one of the most important scenes in the movie.

The next day, David goes to Deepas house. He overhears Deepa singing a devotional song and stands mesmerized by her voice. He asks Deepa to return his tyre. Deepa informs him that her scooter has been sent for service. David also impresses her mother with his gentlemanly manners which clearly failed to impress the audiences. The scene then shifts to Davids confrontation with the robbers who also happen to be the accomplices of his fathers best friend (Venniradai Moorthy). David starts a sermon about the greatness of the crucifix and gives it to the robbers for cleansing their sins. The robbers have a dramatic change of heart and praise David as their savior. They also give back the chain they stole from Deepa during the beginning of the film.  Deepa learns that David is an automobile engineer and visits his workshop, and sees "high-end models" specifically designed by David. David then returns Deepas chain that he had got from the robbers. He also presents the crucifix along with her chain. David and Deepa then fall in love. Deepas mother is concerned that her daughter might be tricked into love by David. Deepa consoles her and
praises Davids manners.

Meanwhile, the robbers turn over a new leaf and begin to work in Davids workshop. The mechanics then prod David to tell them about his affair with Deepa. David blushes.
As luck would have it, Deepas father is a car showroom owner. Deepa takes David to her father the next day. David then proposes his idea, but Deepas father scoffs at it, leaving David heart-broken. David then requests for a car poster (which is of a Ferrari F50) that is posted on the office wall. Deepas father tells him it is the new customer car that is to be sold in the (Hyundai?)showroom and refuses. David then leaves the office, dejected. Deepa gets at angry at her father for his treatment of David and rushes out to console him.

The next scene shows David back in his workshop with his mechanics. They console him and make a plan to ask for loan from the moneylender (Venniradai Moorthy). Meanwhile, Deepas father expresses his fear to his wife about Deepa falling in love with David. The next day, David goes to the moneylenders house along with his faithful mechanics to ask for loan. Moorthy also promises to help him out by agreeing to fund for two of his model cars. David and Deepa then meet at a beach and break into a song (Raasathi)where Davids luscious shoulder movements really captivated the audience. Deepas looks were equally as good as Davids dancing.

The next day, Moorthy visits Davids workshop but upon seeing the reality, changes his mind and refuses to lend money to David. David is heart-broken but Deepa visits him and consoles him. She presents the Ferrari poster to him and asks him to marry her. She also plans to use her 500,000 rupees (that happens to be her inheritance) to help David out. Soon, Davids birthday comes up and Deepa plans a big surprise for him. But everyone is in for a huge surprise as Manju (Jothi), comes from Canada to visit David. She sees David cutting the cake alone and joins him. David is happy to see her and says, "My God, how beautiful you are!". We learn that Manju was Davids sweetheart during their college days but had a fall-out because Manju went to Canada to pursue her career. She also says that she came back to India to take David with her to Canada. David then has a duet sequence with Manju for some unexplained reasons. Deepa suddenly enters the room and gets angry at David for flirting with another woman. She leaves the room fuming. David tries to stop her but she doesnt listen. At this point of the movie, most of the audience wouldve concluded that both girls wouldve been wooed by Davids talent in dancing.

Manju takes David out for lunch and learns about his life after college and his new love Deepa. She gets angry at him for not treating Deepa properly. But she also feels sorry for him and soon falls prey to his (charms?). The Raasathi song is shown again but this time with Manju instead of Deepa. Manju asks David to marry her and plans their migration to Canada. She then asks David to forget Deepa and give back her 500,000 rupees. David then asks for advice from his faithful sidekicks. He then visits Deepa surrounded by a stunning landscape and asks for her forgiveness. Deepa gets angry with his behavior and scorns at him. Wounded, David comes back to his sidekicks who advise him to jump for Manju instead. David then goes to Manju seeking refuge. Manju initially gets angry that David had not returned the check but then suddenly declares her love for him and starts yet another duet sequence. David and Manju stop their car in No-Mans Land and plan to go to the airport from there. Out of nowhere, Deepa suddenly appears there on her Scooty. This is the films climax. Deepa accuses David that he used her just like the stepnee tyre that was shown in the beginning. David then explains that he never used her but always thought of her as his stepping stone to success (Vazhkaiyil Ovvoru Steppum Nee). Deepa refuses to be consoled and gives back the gold crucifix chain to David. She then leaves the place with sorrow. Manju gets angry with David again. She asks David to give the chain to whomever he likes. David then puts the chain around Manjus neck. Manju takes it as the holy mangalsutra, and says that she is now all his. David and Manju circle the car by which they came, three times. This scene symbolically indicates that the marriage of David and Manju has been made in heaven, marked by the circling of the holy fire thrice in actual marriages. The next scene shows a cargo airplane taking off, presumably with David and Manju inside it, for Canada.

==Cast==
 
* Sam Anderson as David. David is an aspiring automobile engineer whose dream is to manufacture a family car that is affordable by all. 
* Jothi as Manju. Manju is a successful businesswoman who comes to India from Canada in search of David, her college-mate.
* Varnika as Deepa. Deepa is a medical student who falls in love with David and looks to help him achieve his dream.
* Pandu as Davids friend who works in his workshop.
* Bayilvan Rangasamy as the robber who has a change of heart after hearing Davids sermon.
* Venniradai Moorthy as the moneylender, who is also a friend of Davids.

==Critical Reception==

   Sam Anderson proved himself nothing more than an untrained actor. 

==Soundtrack==
* Anbaana Iraivan.
* Nenjam Magizhum.
* Raasathi (The song occurs twice in the movie)
* Vaazhga Vaazhgave.
* Neela Vaanil.

==Reception==

The film screened for three days. It got highly negative reviews from the audience who watched the movie in the theaters. The film released in only a few theaters and people who watched the movie without knowing about the film and sam cursed everyone including the theatre owners and distributors.Yarukku Yaaro went relatively unnoticed until leaked versions of the movie appeared on YouTube. After 2 to 3 years of its release its fans,viewers,followers increased gradually.

==References==
 

 
 
 
 