Alive (1993 film)
{{Infobox film
| name     = Alive|
| image          = Alive92poster.jpg|
| caption        = Theatrical release poster|
| screenplay     = John Patrick Shanley
| based on       =  : Piers Paul Read
| narrator       = John Malkovich Josh Hamilton Frank Marshall| Kathleen Kennedy Robert Watts Peter James Michael Kahn William Goldenberg
| music          = James Newton Howard
| studio         = Touchstone Pictures Paramount Pictures The Kennedy/Marshall Company
| distributor    = USA/Canada Buena Vista Pictures International United International Pictures
| released       = January 15, 1993
| runtime        = 127 minutes
| country        = United States
| language       = English
| budget         = $32 million 
| gross          = $36,733,909 (USA) 
}}
 Uruguayan Air Force Flight 571, which crashed into the Andes mountains on October 13, 1972.
 Frank Marshall Josh Hamilton.

==Plot== Stella Maris Colleges Old Christians Rugby Team. Carlitos Páez explains that the pictures were taken by his father and points out several members of the team, including himself as a young man, Alex Morales, Felipe Restano, Nando Parrado and the teams Captain Antonio Balbi. Carlitos then reflects on the accident in a brief monologue, speaking of heroism, the gravity of the situation and of solitude and faith.

The story moves to October 13, 1972 as Uruguayan Air Force Flight 571 flies over the Andes. The raucous rugby players and a few of their relatives and friends are eagerly looking forward to the upcoming match in Chile. Nandos sister, Susana, praises the beauty of the mountains and happily observes that the plane will be landing in 20 minutes.

However, after emerging from clouds, the plane encounters turbulence and collides with an uncharted mountain peak. During the collision, the wings and tail are separated from the fuselage of the plane, and the remnants of the fuselage slide down a mountain slope before coming to a stop. In the process, 7 passengers (Six passengers and 1 Flight Attendant) are ejected out of the plane and die. Antonio, the team captain, takes charge of the situation, coordinating efforts to help his injured teammates. Roberto Canessa and Gustavo Zerbino, both medical students, are the first to address the injured. Shortly afterwards, another six die shortly afterward including both pilots, Alex, Nandos mother Eugenia, and an older couple. Nando, who sustained a head injury, falls into a coma and Susana suffers harsh internal injuries.

As the sun sets, the survivors begin to make preparations for the night. Canessa discovers that the seat covers can be unzipped and used as blankets. The survivors go inside the fuselage and curl up beside one another to stay warm. Antonio, Roy Harley and Rafael Cano plug the gaping hole at the end of the fuselage with luggage to keep out the wind. Two passengers die during the night from their injuries, including Mrs. Alfonsín, causing Carlitos to feel ashamed after earlier yelling at her as she moaned about the pain she had been experiencing. With nothing to hunt or gather on the mountain, Antonio declares they will use rationing when the survivors find a tin of chocolates and a case of wine. After seeing a plane dip its wing, the survivors celebrate. Expecting to be rescued the next day, everyone except Javier, his wife Liliana, and Antonio eat the remaining chocolates.

The survivors listen to a radio for word of their rescue but are devastated to hear the search is called off after day nine. This causes a quarrel between Antonio and several others for eating the chocolate. Meanwhile, Nando regains consciousness through the care of Carlitos and Hugo Diaz. After learning of his mothers death, Nando watches over Susana vigilantly. Knowing that she will die of her injuries within a few days, he vows to set off on foot and find a way out of the mountains. When Carlitos reminds him that he will need food, Nando suggests consuming flesh from the corpses of the deceased pilots to survive his journey to find help. Susana dies from her injuries.
 Antonio "Tintin" Vizintin, finds the tail of the plane. Unable to bring the batteries to the fuselage, they return to the fuselage to get Roy, who is rumoured to have experience setting up electrical equipment. They bring him to the tail of the plane (where the batteries are located) to see if he can fix the radio. When Roy is unsuccessful, the team returns to the fuselage once more. 

Federico Aranda and Alberto Antuna die from their injuries soon after as does Rafael, leading Nando to convince a reluctant Canessa to search for a way out of the mountains, taking Tintin with them. Two days into the journey, they send Tintin back to the fuselage so they can appropriate his rations and continue on their own. After a 12-day trek, the two escape the mountains and alert the authorities of their companions location. As helicopters land on the glacier, the other 14 survivors celebrate.

The film then shifts to the present as Carlitos explains the survivors later returned to the site of the crash and buried the bodies of the dead under a pile of stones, and marked with a cross. The memorial is then displayed with the films dedication to both the 29 deceased and 16 survivors.

==Cast==
 
 
;Survivors
* Ethan Hawke as Nando Parrado Josh Hamilton as Roberto Canessa Antonio "Tintin" Vizintin
* Bruce Ramsay as Carlitos Páez
* David Kriegel as Gustavo Zerbino Roberto "Bobby" Francois
* Kevin Breznahan as Roy Harley Adolfo "Fito" Strauch
* Gian DiDonna as Eduardo Strauch
* John Cassini as Daniel Fernandez Ramon "Moncho" Sabella
* Nuno Antunes as Alvaro Mangino Jose Luis "Coche" Inciarte
* Sam Behrens as Javier Methol Alfredo "Pancho" Delgado
* Steven Shayler as Jose Pedro Algorta
* John Malkovich as older Carlitos Páez / Narrator (uncredited)
 
;Deceased
* Michael Sicoly as Uruguayan Air Force Flight 571|Col. Julio Ferrádas, Pilot
* Jerry Wasserman as Uruguayan Air Force Flight 571|Lt. Colonel Dante Lagurara, Co-Pilot
* Tony Morelli as Uruguayan Air Force Flight 571|Lt. Ramón Martínez, Navigator
* José Zúñiga as Uruguayan Air Force Flight 571|Sgt. Carlos "Fraga" Roque, Mechanic
* Frank Pellegrino as Uruguayan Air Force Flight 571|Sgt. Ovidio Joaquín Ramírez, Steward Liliana Methol Susana Parrado Eugenia Parrado Diego Storm) Marcelo Pérez) Numa Turcatti) Francisco Abal) Enrique Platero) Daniel Maspons) Graciela Mariani) Rafael Echavarren) Arturo Nogueira)
* Silvio Pollio as Álex Morales (based on Uruguayan Air Force Flight 571|Fernándo Vázquez) Gustavo Nicolich) Carlos Valeta)
* Aurelio Dinunzio as Dr. Solana (based on Uruguayan Air Force Flight 571|Dr. Francisco Nicola) Esther Nicola)
 

==Soundtrack==
James Newton Howards end credit music was reused in a number of film trailers, most commonly during the mid-1990s.

==Reception== Spanish and demographic composition of Uruguay), instead of Northern European. However, it should be noted that several of the survivors actually were Uruguayans of Northern and Central European descent (British, Irish, French, German, Croatian, etc.) such as Roy Harley, Bobby François and the Strauchs. In this regard, the passengers were representative of the generally privileged clientele of their school, rather than of Uruguayan society as a whole. 

David Ansen of Newsweek said that, while, "Piers Paul Reads acclaimed book ... paid special attention to the social structure that evolved among the group ... Marshall ... downplays the fascinating sociological details—and the ambiguities of character—in favor of action, heroism and a vague religiosity thats sprinkled over the story like powdered sugar." 

Others, such as Ray Green, praised the tactful nature of the film stating that, "despite the potential for lurid sensationalism, Marshall manages to keep his and the films dignity by steering an effectively downbeat course through some grim goings on thanks in no small manner to the almost allegorical ring of Shanleys stylized dialogue."   Green continues by describing the film as, "thrilling and engrossing as it is at times, Alive is more than an action film—in its own way it is also a drama of ideas, and of the human spirit as well."

Roger Ebert wrote "There are some stories you simply cant tell. The story of the Andes survivors may be one of them."    He also questioned the realism of how normal the actors bodies looked after portraying two months of near starvation. 

==Documentary==
A companion documentary,  , was released at the same time as the film. It includes interviews with the survivors, as well as documentary footage of the rescue. The 30th Anniversary Edition of Alive: The Miracle of the Andes (on DVD) includes this documentary in the Extras section.

==See also==
* Survival film, about the film genre, with a list of related films
* Survive! (film)|Survive!
*  
*  
*  
* Rugby union in Uruguay

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 