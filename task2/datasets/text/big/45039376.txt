Lagna Pathrike
{{Infobox film 
| name           = Lagna Pathrike
| image          =  
| caption        = 
| director       = K. S. L. Swamy
| producer       = A M Sameevulla
| writer         = Chi Udayashankar
| screenplay     = Chi Udayashankar Rajkumar Narasimharaju Narasimharaju Dwarakish Hanumanthachar
| music          = Vijaya Bhaskar
| cinematography = K Janakiram
| editing        = Bal G Yadav
| studio         = Bawa Movietone
| distributor    = Bawa Movietone
| released       =  
| country        = India Kannada
}}
 1967 Cinema Indian Kannada Kannada film, directed by K. S. L. Swamy and produced by A M Sameevulla. The film stars Rajkumar (actor)|Rajkumar, Narasimharaju (Kannada actor)|Narasimharaju, Dwarakish and Hanumanthachar in lead roles. The film had musical score by Vijaya Bhaskar.

==Cast==
  Rajkumar
*Narasimharaju Narasimharaju
*Dwarakish
*Hanumanthachar
*Chi. Udaya Shankar
*Shivaram
*M. Srinivas
*Srirangamurthy
*Thipatur Raghu
*Lakshmayya
*Puttaswamy
*P. R. Srikantayya Jayanthi
*B. V. Radha
*B. Jayashree
*Ameer Ahmed in Guest Appearance
*Javadbai in Guest Appearance
*A. R. Raju in Guest Appearance
*Ashwath Narayan in Guest Appearance
*Gangadhar in Guest Appearance
*Narayana Swamy in Guest Appearance
*Kumari Gayathri in Guest Appearance
*Humayun in Guest Appearance
*Master Babu in Guest Appearance
*S. Rama Rao in Guest Appearance
 

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Brahmachari Sharanade || PB. Srinivas, AL. Ragavan || Chi. Udaya Shankar || 04.01
|-
| 2 || Gandu Muththina Chendu || PB. Srinivas, L. R. Eswari || Chi. Udaya Shankar || 06.19
|-
| 3 || Illi Yaru Illa || PB. Srinivas, L. R. Eswari, Latha || Chi. Udaya Shankar || 03.39
|- Janaki || K. Prabhakara Shastry || 03.14
|- Janaki ||  || 03.41
|-
| 6 || Kanneradoo || PB. Srinivas ||  || 03.58
|- Janaki ||  || 05.05
|-
| 8 || Nijavo Sullo || S. Janaki|Janaki, PB. Srinivas || K. Prabhakara Shastry || 04.05
|- Janaki ||  || 02.54
|-
| 10 || Title Songs || Instrumental ||  || 02.10
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 


 