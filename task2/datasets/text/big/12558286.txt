20,000 Years in Sing Sing
{{Infobox film
| name           = 20,000 Years in Sing Sing
| image          = 20,000 Years in Sing Sing.jpg
| caption        = Theatrical release poster
| director       = Michael Curtiz
| producer       = Darryl F. Zanuck (uncredited) Raymond Griffith (uncredited supervising producer) Robert Lord (uncredited associate producer)
| writer         = Lewis E. Lawes (book) Courtney Terrett (adaptation) Robert Lord (adaptation) Wilson Mizner Brown Holmes
| narrator       =
| starring       = Spencer Tracy Bette Davis
| music          =
| cinematography = Barney McGill
| editing        =
| studio         = First National Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States English
| budget         =
| gross          =
}} Sing Sing maximum security prison in New York State. This movie was directed by Michael Curtiz and was based upon the nonfiction book Twenty Thousand Years in Sing Sing, which was  written by Lewis E. Lawes, the warden of Sing Sing from 1920 to 1941.  Spencer Tracy portrays an inmate and Bette Davis plays his girlfriend.

==Plot==
Cocky Tommy Connors (Spencer Tracy) is sentenced from 5 to 30 years in Sing Sing for robbery and assault with an automatic weapon. His associate, Joe Finn (Louis Calhern), promises to use his contacts and influence to get him freed long before that, but his attempt to bribe the warden to provide special treatment is met with disdain and failure.

Connors makes trouble immediately, but several months confined to his cell changes his attitude somewhat. As the warden had predicted, Connors is only too glad to do some honest work on the rockpile after his enforced inactivity.  

Nonetheless, his determination to break out is unshaken. Bud Saunders (Lyle Talbot), a highly educated fellow prisoner, recruits him and Hype (Warren Hymer) for a complicated escape attempt. By chance, however, it is scheduled for a Saturday, which Connors superstitiously regards as always unlucky for him. He backs out, forcing Saunders to take another volunteer. The warden is tipped off and, though two guards are killed, the escape is foiled. Trapped, Saunders jumps to his death. His two accomplices are captured and returned to his cells.

Meanwhile, Connors girlfriend, Fay Wilson (Bette Davis), visits him regularly in prison since his trial. On one visit, she admits she has become friendly and close to Finn in order to encourage him to help Connors, but Connors tells her that she is only giving Finn a reason to keep him locked up in jail.

The warden shows Connors a telegram which says that Wilson was injured in a car accident; there is no hope for her. Then, he gives Connors a 24-hour leave to see her; Connors promises to return, no matter what. When he sees Wilson, he learns that Finn was responsible for her injuries. He takes out a gun from a drawer, but Wilson persuades him to give her the pistol. Finn shows up, however, expecting her to sign a statement exonerating him in exchange for $5000 she intended to give to Connors. Connors attacks him. When it seems that Finn is about to kill her boyfriend, Wilson shoots him. Connors flees, taking the gun with him; Wilson secretly slips the money into his pocket. Before he dies, Finn names Connors as his killer.

The warden is lambasted in the newspapers for letting Connors go. Just when he is about to sign a letter of resignation, Connors walks in. He is found guilty of first-degree murder and sentenced to death in the electric chair, despite a recovered Wilsons testimony that she killed Finn. Connors comforts her before being taken to death row.

==Production==
Bette Davis liked working with Spencer Tracy, and had actually idolized him. The two wanted to do another movie together but never got the chance, but did work with each other again on a radio version of Dark Victory. 

Tracy, then under contract to Fox, was lent out to Warner Brothers for the film.  It was originally intended for James Cagney, but at the time Cagney was having one of many misunderstandings with Jack Warner. 

==Cast==
*Spencer Tracy as Tommy Connors
*Bette Davis as Fay Wilson
*Arthur Byron as the Warden, Paul Long
*Lyle Talbot as Bud Saunders
*Warren Hymer as Hype
*Louis Calhern as Joe Finn

==See also==
* List of American films of 1932

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 