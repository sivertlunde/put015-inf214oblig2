Zombiez
{{Infobox film
| image          =
| alt            =
| caption        =
| director       = John Bacchus
| producer       =
| writer         = John Baccus
| starring       = Jenicia Garcia Jackeem Sellers Randy Clarke
| music          =
| cinematography = Paul Swan
| editing        =
| studio         = Purgatory Blues
| distributor    = Lionsgate Films
| released       =   }}
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Zombiez is a 2005 American horror film written and directed by John Bacchus (as ZWS).  It stars Jenicia Garcia, Jackeem Sellers, and Randy Clarke.  Garcia plays a woman who fights to free her husband from a mad scientist played by Sellers.

== Plot ==
After zombies kill her coworkers at a construction company, Josephine flees to her house, where she and her husband attempt to make a stand.  The zombies capture Josephine and her husband, but she escapes.  After a series of adventures throughout the city, she returns to rescue her husband. She discovers he is held by The Dr., who claims to control the zombies.  Josephine fights The Dr. and defeats him.

== Cast ==
* Jenicia Garcia as Josephine
* Jackeem Sellers as The Dr.
* Randy Clarke as Steve
* Raymond Spencer as Terry

== Release ==
Lionsgate Films released Zombiez direct-to-video on May 17, 2005. 

== Reception ==
Jon Condit of Dread Central rated it 0/5 stars and called it "the absolute worst of the direct-to-video horror genre".   David Walker of DVD Talk rated it 0/5 stars and wrote, "There is nothing good to say about this garbage."   Peter Dendle called it "pointless and uninteresting, with no discernible sense of love for the topic." 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 