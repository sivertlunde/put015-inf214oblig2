Mr. Rajesh
 

{{Infobox film
| title =Mr. Rajesh
| image =File:Mr. Rajesh Poster.jpg
| caption =Poster
| director =Jai Akash
| producer =Rahmath Productions
| writer =Jai Akash
| starring =Jai Akash
| music =Sai Karthik
| released =26 April 2013
| country =India
| language =Telugu
}} action romance romance film, directed by Jai Akash. The film was produced by Khader Valli and released under the banner of Rahmath Productions. The film featured Jai Akash alongside Sony Charista, Simran and various others. The background score and soundtrack were composed by Sai Karthik. Akash played seven different characters in the movie.   

== Plot ==

A family is killed in a cold-blooded murder. The man who wants them dead is Akshay (Aakash), whose brother and entire family was wiped out because of a property dispute. The clash in the family, however, is over marriage issues rather than property. The murderers motive is directly linked to his business ventures. 

== Cast ==

* Jai Akash as Akshay
* Sony Charista
* Simran
* Kahdervalli

== Music ==

The background score and soundtrack were composed by Sai Karthik, with lyrics by SS. Athreya & Siva Ganesh. The audio was released on September 18, 2012, and the music rights were acquired by Legend Music in Telugu.   

=== Songs ===

{| class="wikitable sortable"
|-
! Sr. No. !! Song !! Singers !! Lyrics
|-
| 1 || Deewana... || Pranavi || SS. Athreya
|-
| 2 || I Love You... || Tada Raja || SS. Athreya
|-
| 3 || Naa Style Pataash... || Bindu || SS. Athreya
|-
| 4 || Kallallona... || Jayadev || Siva Ganesh
|}

== Release ==

The film was released on April 26, 2013, in India. 

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  
*  

 
 
 


 