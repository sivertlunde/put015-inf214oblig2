Woman Basketball Player No. 5
{{Infobox film
| name           = Woman Basketball Player No. 5
| image          = Woman Basketball Player No. 5.jpg
| caption        = 
| director       = Xie Jin
| producer       = 
| writer         = Xie Jin
| starring       = Qin Yi
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 女篮5号
| fanti          = 女籃5號
| pinyin         = Nǚ lán wǔ hào}}
}}
Woman Basketball Player No. 5 ( ) is a 1957 Chinese film presented by Tianma Film Studio and directed by Xie Jin, starring Qin Yi, Liu Qiong, Cao Qiwei and Wang Qi. It is the first colored sports movie filmed after the formation of the Peoples Republic of China, and also the first film directed by renowned director Xie Jin.

==Plot==
Before the establishment of PRC, Lin Jie, the daughter of the boss of East China Basketball Team in Shanghai, falls in love with the leading player of the team, Tian Zhenhua. During a game against foreign marine soldiers, the boss takes bribery and orders the team to lose the game. Due to his nationalist dignity, however, Tian leads the team to victory, against the will of the boss. As a result, the boss hires thugs to beat Tian, and also forces his daughter to marry a rich man. 18 years later, Tian is now the coach of Shanghai Woman Basketball Team. Xiao Jie, the daughter of Lin, is a girl with basketball talent but also has prejudice towards sports career. Tian educates and helps her with patience. Xiao Jie was injured in a game and hospitalized. Tian and Lin accidentally reunite when both visiting Xiao Jie and their love is reborn. Xiao Jie is later elected into national team and will participate in international games.

== External links==
 
* 
* 
*  at the Chinese Movie Database

 
 
 
 
 
 


 
 