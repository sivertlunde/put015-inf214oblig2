The Pied Piper (1942 film)
 
{{Infobox film
| name = The Pied Piper
| image = Pied_piper_2.jpg
| image_size =
| caption = original movie poster
| director = Irving Pichel
| producer = Nunnally Johnson
| writer = Nunnally Johnson
| starring = Monty Woolley Roddy McDowall Anne Baxter Alfred Newman
| cinematography = Edward Cronjager
| editing =
| distributor = Twentieth Century-Fox
| released =  
| runtime = 87 min.
| country = United States
| language = English
}} German invasion novel of the same name by Nevil Shute.  It was directed by Irving Pichel.
 Best Actor Best Cinematography, Best Picture.

==Cast==
*Monty Woolley as Howard
*Roddy McDowall as Ronnie Cavanaugh
*Anne Baxter as Nicole Rougeron
*Otto Preminger as Major Diessen
*J. Carrol Naish as Aristide Rougeron
*Lester Matthews as Mr. Cavanaugh
*Jill Esmond as Mrs. Cavanaugh
*Ferike Boros as Madame
*Peggy Ann Garner as Sheila Cavanaugh
*Merrill Rodin as Willem
*Maurice Tauzin as Pierre
*Fleurette Zama as Rose

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 