The Story of Esther Costello
{{Infobox film
| name           = The Story of Esther Costello
| image          = TheStoryofEstherCostello.jpg
| image_size     =  poster
| David Miller David Miller  Charles Kaufman
| based on       =  
| narrator       = 
| starring       = Joan Crawford Rossano Brazzi Heather Sears
| music          = Lambert Williamson
| cinematography = Robert Krasker
| editing        = Ralph Kemplen Romulus Films
| distributor    = Columbia Pictures
| released       =  
| runtime        = 127 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $1,075,000 (US rentals) 
}}
 British drama Bafta as David Miller Charles Kaufman was based on a novel by Nicholas Monsarrat. It was distributed by Columbia Pictures.

==Plot==
With her marriage to womaniser Carlo Landi (Rossano Brazzi) in ashes, wealthy and childless Margaret Landi (Joan Crawford) finds an emotional outlet in patronizing a 15-year-old deaf, dumb, and blind Irish girl named Esther Costello (Heather Sears). Esthers disabilities are the result of a childhood trauma and are psychosomatic rather than physical. As Costello makes progress with Braille and sign language, she is seen as an example of triumph over adversity. Carlo gets wind of Margarets new life and re-enters the scene. He views Esther as a source of cheap financial gain and arranges a series of exploitative tours for her under a mercenary manager Frank Wenzel (Ron Randell). One day when Margaret is absent from the Landi apartment, Carlo seduces and rapes the now 16-year-old Esther. The shock restores the girls sight and hearing. When Margaret learns of her husbands business duplicities and the rape, she consigns Esther to the care of a priest and a young reporter who loves her (Lee Patterson). Margaret then kills Carlo and herself.

==Cast==
* Joan Crawford as Margaret Landi
* Rossano Brazzi as Carlo Landi
* Heather Sears as Esther Costello
* Lee Patterson as Harry Grant
* Ron Randell as Frank Wenzel
* Fay Compton as Mother Superior John Loder as Paul Marchant
* Denis ODea as Father Devlin Sidney James as Ryan
* Bessie Love as Matron in Art Gallery Robert Ayres as Mr. Wilson
* Maureen Delaney as Jennie Costello
* Harry Hutchinson as Irish publican
* Tony Quinn as Irish pub customer
* Janina Faye as Esther Costello, as a child

==Production notes==
The film is based on a book by Nicholas Monsarrat that nearly had Helen Kellers co-workers suing for libel due to perceived parallels between Helens story and Esthers.  In particular, the book seemed to slur the character of Anne Sullivans husband, writer-publicist John Macy, who was close to Kellers age. A relationship between John and Keller has long been a subject of speculation.  Esthers reporter friend was reminiscent of Kellers highly publicised attempt to elope with reporter-secretary Peter Fagan. 

The novel focuses mainly on the falsehoods behind large-scale charities and the self-serving natures of those involved. When Esther recovers her sight and hearing, her promoters worry that this will put an end to the Costello Fund. They convince her that the rape was her fault, and that to reveal that she is healed would bring shame and ruin to Margaret. She is actually coached to continue acting as deaf-blind, and does so for about a year; however, in that time she makes several slips (some deliberate). Esthers reporter friend discerns the truth and meets Esther privately, whereupon she tells him everything. He plans to write up the story, then take Esther away and marry her. But as he returns to the newspaper office, word comes to him that Esther has suddenly died. When he confronts Margaret she reveals nothing, and continues the charade. The story can never be printed because it will smear Esthers name and disillusion the people who believe in the charity and its message of hope. The novel closes with Margaret giving an impassioned speech at a convention, as millions of attendees open their wallets.

==Reception==
The film was the 11th most popular movie at the British box office in 1957. 

The New York Times noted, "Miss Crawford, Mr. Brazzi, and Mr. Patterson and all the minor players are professional throughout." William K. Zinsser in the New York Herald Tribune wrote, "It wouldnt be a Joan Crawford picture without plenty of anguish...And her fans will have their usual good time...this plot enables Miss Crawford to run a full-course dinner of dramatic moods, from loneliness to mother love, from pride in the girl to passion with her husband, and finally to smouldering rage...Somehow she pulls it off. This may not be your kind of movie but it is many womens kind of movie and our Joan is queen of the art form." 

==DVD==
The Story of Esther Costello was released on DVD by Turner Classics (under license from Sony Pictures Home Entertainment) on November 25, 2014, as part of the "Joan Crawford in the 1950s" collection.

==References==
;Notes
 

;Bibliography
* 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 