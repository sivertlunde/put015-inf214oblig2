Murder 2
 
 
{{Infobox film
| name           = Murder 2
| image          = Murder 2 Hindi Movie Poster.jpg
| caption        = Theatrical release poster
| director       = Mohit Suri
| producer       = Mahesh Bhatt  (Presenter)  Mukesh Bhatt
| writer         = Shagufta Rafiq  (Screenplay & Dialogue) 
| story          = Mahesh Bhatt
| based on       =  
| starring       = Emraan Hashmi Jacqueline Fernandez   Sudhanshu Pandey  
| music          = Harshit Saxena Mithoon Sangeet Haldipur Siddharth Haldipur
| cinematography = Ravi Walia
| editing        = Devendra Murdeshwar
| studio         = Vishesh Films
| distributor    = Vishesh Films T series
| released       =  
| runtime        = 127 minutes
| country        = India
| language       = Hindi 
| budget         =   
| gross          =   
}} The Chaser (2008).   The film did well in the box office and was declared a blockbuster by Box Office India.  It is one of the highest grossing Bollywood films of 2011.    

==Plot==
Arjun Bhagwat (Emraan Hashmi) is an ex-police officer who is hungry for money and earns his bread by doing wrong things. He is an atheist (Nastika|Nastik) but regularly visits church to donate money to orphans. Priya (Jacqueline Fernandez), a model, is in a passionate but confused relationship with Arjun. Whilst Priya is in love with Arjun, Arjun merely lusts for her and he forces upon her in the beginning of  movie but she too enjoys his company.

Arjun meets a gangster, Sameer, who has been going through a slump in his business due to an unexplained disappearance of his hookers. Sameer offers him a huge amount of money to locate the missing girls, which Arjun accepts. During his investigation, Arjun finds a phone number linked to the missing girls. He tells Sameer to send a prostitute to the number. Sameer decides to send Reshma (Sulagna Panigrahi), a 17-year-old college newcomer in the business, who has entered prostitution to feed her family, though keeps this a secret from them. Reshma is sent to the house of Dheeraj Pandey (Prashant Narayanan), who is actually a psychopathed murderer responsible for torturing and killing the missing hookers. He decides to do the same with Reshma and throws her in a dark well, with the intention of torturing her till she dies.

Arjun finds out that Dheeraj is the murderer and tells the police. While Dheeraj is held in jail, the commissioner calls a psychiatrist to extract his confession. Dheeraj tells the doctor that he kills women because he thinks they take advantage of men. He also reveals that he castrated himself and became a eunuch to get rid of his sex addiction, with the help of a fellow eunuch and big-shot politician, Nirmala Pandit. Dheeraj is eventually let out under Nirmala Pandits influence. Meanwhile, Reshma escapes from the well and tries to find her way out through the forest.

Arjun meets Dheerajs family, who reveal that Dheeraj used to beat his wife. He next meets a private dancer, Sonia, who had also been tortured by Dheeraj, but managed to escape. Arjun then meets an idol-maker who used to work with Dheeraj. The maker tells him that Dheeraj used to make idols of devils instead of deities and killed the factory-owner who tried to stop him.
Inspector Sadaa (Sudhanshu Pandey) informs Arjun that Dheeraj is free, and the police try to track him down as quickly as possible. Nirmala and Dheeraj enter the same temple where Reshma is hiding. Nirmala and the priest, who had both been unaware of Dheerajs true nature, are killed by Dheeraj, but not before the priest reveals that Reshma is there. Dheeraj finds the terrified Reshma and brutally murders her, escaping just before Arjun and the cops arrive. Arjun finds Reshmas body and breaks down, feeling guilty and responsible for her death.

Dheeraj targets Priya next whom he calls for a photoshoot and tries to torture, but Arjun saves her, engaging Dheeraj in a fight as police officers show up. They request Arjun not kill Dheeraj. Dheeraj then plays the tape he recorded when he was torturing Reshma. Hearing Reshmas pleading cries, Arjun, tormented by her death and blaming himself for it, angrily kills Dheeraj, ending his reign of terror once and for all.

The film ends with Arjun visiting a church with Priya, implying that Priyas near death has made Arjun realize his love for her.

==Cast==
* Emraan Hashmi as Arjun Bhagawat
* Jacqueline Fernandez as Priya
* Prashant Narayanan as Dheeraj Pandey
* Sulagna Panigrahi as Reshma
* Sudhanshu Pandey as Inspector Sadaa
* Sandeep Sikand as Nirmala Pandit
* Bikramjeet Kanwarpal as Commissioner Ahmed Khan
* Shweta Kawatra as Psychiatrist Dr. Sania (Special Appearance)
* Yana Gupta as Jyoti, Special Appearance in song "Aa Zara"
* Amardeep Jha as Reshmas mother
* Abhijit Lahiri as Dheeraj Pandeys father
* Jhuma Biswas as Hostel Warden
* Deep Jyoti Das as Criminal Lawyer
* Ravi Kiran as Randi

==Production and filming==
The film was shot in Mumbai and Goa. Earlier, Bipasha Basu was offered the part of the leading lady, but she refused. Then Mohit Suri offered the leading role to Asin Thottumkal but, she thought the role of leading lady is not so powerful so she also refused to do this role. The part then went to Jacqueline Fernandez, after also considering actress Sonal Chauhan.  Basu was also offered the item number "Aa Zara". When she refused, the song was given to Yana Gupta. 

The song "Haal-E-Dil" had to be re-shot to make it more suitable for television promos. 

Yana Guptas item number "Aa Zara" was not aired on television promos as it was too explicit and suggestive for audiences under 18. Instead, the number was shot with Jacqueline Fernandez and Emraan Hashmi, which was aired on television. The original number with Yana was retained in the film. 

The DVD of the film was released on 1 August 2011.

==Reception==

===Critical reception===
Murder 2 received mixed to positive reviews from various critics of India. Taran Adarsh of Bollywood Hungama gave it 4/5 stars and stated "Murder 2 is one of the finest crime stories to come out of the Hindi film industry. Also, as a film, it lives up to the expectations that you may associate from a sequel of a smash hit."  Nikhat Kazmi of The Times Of India gave it 3.5/5 stars coomenting,Murder 2 has enough to give the masses a mast time.IANS gave it 3/5 stars. Komal Nahta of Koimoi rated Murder 2 with 3/5 stars and said – " It doesn’t have too much to offer in terms of entertainment as it is a dark film but its plus points are the abundant sex scenes and the good music. Its reasonable budget on the one hand, and wonderful recovery from sale of its satellite, music and worldwide theatrical rights on the other have ensured that the producers have made a handsome profit before release."  Pankaj Sabnani of Glamsham gave it 3/5 stars while writng thatAn intriguing plot supported by superb performances, make MURDER 2 a killer film. Daily Bhaskar also gave it 3/5 stars, stating that  If Emraan plus Jacqueline under the name Murder 2 dont arise your interest enough, then the story will surely do the trick.

Saibal Chatterjee of NDTV gave the movie 2.5/5 stars.  Nikita Kapoor of FilmiTadka rated the film with 2.5/5 stars and said – "Overall Murder 2 is an adult, violent, and gruesome movie, which is a good thing, however after a time you feel as if the film was written and directed only for Emraan Hashmi (excessive presence in every frame) which doesn’t go down well with me."  Raja Sen of Rediff gave it 1.5/5 stars and wrote in his review – "Murder 2 is flat, boring and not worth talking about. Even Emraan, sporting less stubble than usual, seems baby-faced as he goes through the motions. It might be inspired by some obscure film, but I dont even care enough to look for its name. By now, Ive come to accept that the Bhatts have a bigger DVD collection than me. I do wish theyd stop flaunting it, though." 

===Box office===
The film opened with full houses across India with occupancy ranging from 70–100%, earning  .  The film grossed   in its opening weekend.   After the weekend, the film grossed   on Monday,   on Tuesday and   on Wednesday.  The film went on to gross   in its first week, although collections were affected on 13–14 July due to the bomb blasts in Mumbai on 13 July 2011.  As of February 2012, Box Office India claimed that it was the 28th biggest opening week of all time.  The movie dominated the single screens despite new releases   and grossed   in the second week despite limited multiplex release. Murder 2 grossed approximately   at the Indian box office. The all India distributor share stood at  .  It was elevated to Blockbuster status all over India.

==Soundtrack==
{{Infobox album
| Name       = Murder 2 
| Type       = Album 
| Artist     = Harshit Saxena, Mithoon, Sangeet Haldipur, Siddharth Haldipur 
| Cover      = Murder 2 CD Cover.jpg
| Released   =  
| Recorded   =  Feature film soundtrack 
| Length     = 
| Label      = T-Series
| Producer   =
| Reviews    = 
}}

===Tracklist===
The films soundtrack is composed by Mithoon, Harshit Saxena & Sangeet-Sidharath. Lyrics are penned by Mithoon, Kumaar & Syeed Quadri. Songs are as follows : -

{| class="wikitable"
|-
! Track No !! Song !! Singer(s) !! Composer
|-
| 1 || Haal-E-Dil || Harshit Saxena || Harshit Saxena
|-
| 2 || Aa Zara || Sunidhi Chauhan || Sangeet Haldipur, Siddharth Haldipur
|-
| 3 || Aye Khuda || Mithoon, Kshitij Tarey & Saim Bhat || Mithoon
|-
| 4 || Phir Mohabbat || Mohammad Irfan Ali, Arijit Singh & Saim Bhat || Mithoon
|-
| 5 || Tujhko Bhulana || Sangeet Haldipur & Roshni Baptist || Sangeet Haldipur, Siddharth Haldipur
|-
| 6 || Aa Zara (Reloaded) || Sunidhi Chauhan || Sangeet Haldipur, Siddharth Haldipur
|-
| 7 || Haal-E-Dil (Acoustic) || Harshit Saxena || Harshit Saxena
|-
| 8 || Aye Khuda (Remix) || Mithoon, Kshitij Tarey & Saim Bhat || Mithoon
|}

===Reception===
The album received positive reviews from critics. Joginder Tuteja from Bollywood Hungama gave the album a 3/5 stars saying that "Murder 2 turns out to be a good deal overall." Musicaloud gave the album 3.5/5 stars.   

==Awards and Nominations==
{| class="wikitable"
|-
! Award
! Category
! Result
! Recipient
! Source
|- Screen Awards Best Actor in Negative Role
|   Prashant Narayanan
|
|- Stardust Awards Best Actor (Thriller/Action)
|   Emraan Hashmi
|   
|- Stardust Awards Best Actress (Thriller/Action)
|   Jacqueline Fernandez
| 
|- Stardust Awards Stardust Award for  Breakthrough Supporting Performance – Male
|   Prashant Narayanan
| 
|- Stardust Awards Stardust Award for Standout Performance by a Music Director
|   Sangeet & Siddharth Haldipur – Tujhko Bhulaana And Aa Zaraa Murder 2
| 
|- Apsara Film & Television Producers Guild Awards Best Actor
|   Emraan Hashmi
|
|- Apsara Film & Television Producers Guild Awards Best Performance in Negative Role
|   Prashant Narayanan
|
|- Apsara Film & Television Producers Guild Awards Best Actress in Supporting Role
|   Sulagna Panigrahi
|
|- Apsara Film & Television Producers Guild Awards Best Playback Singer – Male
|   Mohammed Irfan
|
|- Apsara Film & Television Producers Guild Awards Best Playback Singer- Female
|   Sunidhi Chauhan
|
|}

==Sequel== The Hidden Face,  starred Randeep Hooda, Aditi Rao Hydari and Sara Loren in lead roles. However, unlike the earlier parts, it only performed moderately well at the box office 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 