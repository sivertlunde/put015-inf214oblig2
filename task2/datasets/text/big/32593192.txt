Ee Adutha Kaalathu
{{Infobox film
| name = Ee Adutha Kaalathu
| image = E_adutha_kalathu.jpg
| caption = The familiar becomes strange and the strange familiar...
| writer = Murali Gopy Indrajith Murali Nishan Mythili Tanusree Ghosh Jagathy Sreekumar
| director = Arun Kumar Aravind
| producer = Raju Malliath
| cinematography = Shehnad Jalaal Arun Kumar Aravind
| music = Gopi Sunder Lyrics: Rafeeq Ahamed
| Distributor = PJ Entertainments 
| studio = Ragam Movies
| released =    
| runtime = 160 minutes
| country = India
| language = Malayalam
| budget = 
| gross = 
}} Kerala State Award winning cinematographer Shehnad Jalal handled the camera. 

The narrative is patterned like a Rubiks Cube; its a brainy entertainer that mixes various genres. It features the lives of six different persons from different strata of the social life of the city, interconnected due to unexpected events beyond their control. Bengali model and theater artiste Tanushree Ghosh makes her debut in Malayalam cinema with this film.  The cast includes Indrajith Sukumaran|Indrajith, Murali Gopy, Anoop Menon, Nishan K. P. Nanaiah|Nishan, Jagathy Sreekumar and Mythili.  The music is composed by Gopi Sundar and the lyrics are by Rafeeq Ahmed.  
 17th International Film Festival of Kerala,  where it won the NETPAC Award for Best Malayalam Film.    Discussions are underway for the Hindi remake of the movie. 

MalluDeals.com gave Ee Adutha Kalathu the ranking of No 2 among the Top 10 Malayalam Movies of 2012. 

== Plot ==
Vishnu is an unemployed man who makes toys from garbages for living. He has a wife - Ramani, two daughters and mother. Ajay Kurien is a rich businessman who tries to avoid his wife, Madhuri. Madhuri is a former Bollywood actress.

==Production==
 
Ee Adutha Kaalathu started its shooting on 12 September 2011 at Thiruvanathapuram.  The official teaser was released on 5 September, even before the commencement of shooting.  The teaser has been highly appreciated for its originality, the use of Malayalam words and for the background score. 
The second schedule of shooting started on 28 October.  Almost the whole movie was shot at Thiruvananthapuram except for few scenes which were shot at Nagercoil. The theatrical trailer was released on 27 January which became a hit in YouTube and social networking sites.

==Promotional campaign==
The theme song Naatil Veetil and the trailer have generated tremendous buzz for the movie online. 

The promotional campaigns carry the caption - "Reality is a Movie... that stars You And only You. In this movie, there are no Good People, no Bad People, only Unfortunate People.. Who hunt for a Fortune.. This Season.. Get Ready For an Adventure.. Into Your Own World!" 

The movie has done away with the concept of Good vs Evil. It explores the way people sway to situations, drawing their energy from things that are socially considered both good and bad.

==Cast==
*Indrajith Sukumaran as   Vishnu
*  Murali Gopy as   Ajay Kurien
* Anoop Menon as   Tom Cherian Nishan as   Rustam
* Tanu Roy as   Madhuri Kurien
* Mythili as   Ramani Lena as   Roopa
* Jagathy Sreekumar as   Bonacaud Ramachandran Baiju as Watson
* Manikandan Pattambi as Sundaram Gimi George as   Shyleja
* Indrans as   House broker
* Krishna Prabha as Bindu
* Baby Eva as Minnu, Vishnus daughter
* Baby Vaishnavi as Chinnu, Vishnus daughter
* Master Ramzan as Ayur
* KPAC Leelamani as Paatti
* Anitha Nair as Ratnam
* Manga Mahesh as Ramachandran’s wife  Santhakumari as Vishnus mother
* Kalabhavan Haneef as Mammootty
* Sajitha Madathil as   Madhuris mother Riza Bawa as   Madhuris father
* Dinesh Panicker as Minister
* Prem Prakash as Psychiatrist
* Gopalji as S.I. Somashekaran 
* Felix JK as Suleiman Bhai
* Biju Varkey as Vicar
* Shine Tom Chacko as Serial killer 
* Dinesh as Trichur goon

==Soundtrack==
{{Infobox album
| Name = Ee Adutha Kaalathu
| Type = Soundtrack
| Artist = Gopi Sundar
| Cover = Ee Adutha Kaalathu audio.jpg
| Caption = Album cover for Ee Adutha Kaalathu
| Released =
| Recorded = Sunsa Digital Work Station, Chennai Feature film soundtrack
| Length =
| Online Promotion =
| Label = Manorama Music  
| Producer = Raju Malliath
}}

The soundtrack features three songs composed by Gopi Sundar, with lyrics by Rafeeq Ahamed. The background score of the film has been composed by Gopi Sundar. The sound track was released by Manorama Music. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Artist(s) !! Composer !! Lyricist
|- Vijay Yesudas, Nayana Nair || Gopi Sundar || Rafeeq Ahamed
|- Sithara (Singer)|Sithara, Rahul Nambiar || Gopi Sundar || Rafeeq Ahamed
|-
| 3 || Naatil Veetil || Gopi Sundar, Anna Katharina Valayil || Gopi Sundar || Rafeeq Ahamed
|}

==Reception==
The film was released on February 24 in 63 centers inside Kerala. The film received positive reviews from the audiences and critics alike and has become a hit. It was mostly welcomed by the youth. The positive critical response from the viewers shows the thirst of viewers for varied films, rather than star, action oriented masala films. 

===Critical response===
Malayala Manorama called the film "A must watch movie" and commented that "By the loud, standing ovation at the end, when the lights are on, Arun Kumar can rest assured that he has established himself as a director who has once again successfully brought about the phenomenal change Malayalam cinema has ever been thirsting for". 

Indiaglitz.com rated the film (3.5/5) and said that "Ee Adutha Kalathu is one movie that is a definite prescription for a demanding viewer. A well-made film that works for its powerful plot and structuring, engaging script, and super performances.”  Metromatinee.com rated the movie as "Excellent" and said "Ee Adutha Kalathu is a remarkably refreshing and sensitive movie making a candid statement of life in the cities".  

Veeyen of Nowrunning.com rated the film (3/5) and said, "Arun Kumar Aravinds Ee Adutha Kaalathu is an emotionally bruising film that is oxymoronic to the core; its the kind of film that is pleasingly depressing, casually engrossing and placidly terrorizing. A must-watch in short!"   

Paresh C. Palicha of Rediff.com gave the movie a (3/5) rating and said that "Ee Adutha Kaalathu shows the life of six different people living a city, in a very entertaining manner". 

===Accolades===
;Mohan Raghavan Foundation Awards 
* Best Script - Murali Gopy

;Asiavision Movie Awards 
* Outstanding Movie
* Best Editor – Arun Kumar Aravind
* New Sensation in Script – Murali Gopy
* Best Anti-hero – Murali Gopy
* Outstanding Performance – Indrajith
* New Promise in Singing – Anna Katharina

; International Film Festival of Kerala (International Film Festival of Kerala#IFFK 2012|2012)
* NETPAC Award for Best Malayalam Film 

; Nana Film Awards
* Second Best Movie
* Best Script Writer - Murali Gopy
* Second Best Actor - Indrajith Lena

; P. Bhaskaran Foundation Awards 
* Best Story - Murali Gopy

; Sathyan Award 
* The Best Actor - Indrajith Sukumaran

; Asianet Film Awards 
* The Best Editor - Arun Kumar Aravind
* The Best Villain - Murali Gopy

; Vanitha Film Awards 
* The Best Anti-Hero - Murali Gopy

;South Indian International Movie Awards  Best Actress Lena
* Best Actor in a Negative Role - Murali Gopy

==References==
 

==External links==
*  

 
 
 
 
 