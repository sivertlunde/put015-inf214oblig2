Let's Be Famous
{{Infobox film
| name = Lets Be Famous
| image =Let s Be Famous poster.jpg
| image_size =
| caption =
| director = Walter Forde
| producer = Michael Balcon
| writer = Roger MacDougall   Allan MacKinnon
| narrator =
| starring = Jimmy ODea 
| music = Ernest Irving
| cinematography = Gordon Dines   Ronald Neame
| editing = Ray Pitt
| studio = Associated Talking Pictures Associated British 
| released = March 1939
| runtime = 83 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Lets Be Famous is a 1939 British comedy film directed by Walter Forde and starring Jimmy ODea, Betty Driver and Sonnie Hale. It was made by Ealing Studios, with shooting beginning in November 1938.  The films art direction was by the Austrian Oscar Werndorff, in his final production.

==Cast==
* Jimmy ODea as Jimmy Houlihan 
* Betty Driver as Betty Pinbright 
* Sonnie Hale as Finch 
* Patrick Barr as Johnny Blake 
* Basil Radford as Watson 
* Milton Rosmer as Albert Pinbright 
* Garry Marsh as B.B.C. Official 
* Alf Goddard as Battling Bulger 
* Henry Hallett as Grenville 
* Hay Plumb as Announcer

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.
* Sutton, David R. A Chorus of Raspberries: British Film Comedy 1929-1939. University of Exeter Press, 2000.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 