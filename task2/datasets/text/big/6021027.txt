Blissfully Yours
{{Infobox film
| name           = Blissfully Yours
| image          = blissfullyyours01.jpg
| caption        = The Thai film poster.
| director       = Apichatpong Weerasethakul
| producer       = Eric Chan Charles de Meaux
| writer         = Apichatpong Weerasethakul
| starring       = Kanokporn Thong-aram Min Oo Jenjira Jansuda
| music          =
| cinematography = Sayombhu Mukdeeprom
| editing        = Lee Chatametikool
| distributor    = Kick the Machine Anna Sanders Films
| released       = May 17, 2002 (France) August 13, 2002 (Thailand)
| runtime        = 125 minutes
| country        = Thailand
| language       = Thai
| budget         =
}}
Blissfully Yours ( ) is a 2002 Thai romance film directed by Apichatpong Weerasethakul. It won the Un Certain Regard prize at the 2002 Cannes Film Festival.    The film features male full-frontal nudity. 

==Plot== Burmese immigrant living in Thailand who has contracted a mysterious rash that is painful and covers the upper part of his body. His girlfriend, Roong, and an older woman, Orn, take him to see a doctor. Min pretends that he cannot speak because he is not fluent in Thai and speaking would reveal him to be an illegal immigrant. The doctor treats him, however because he does not have proper ID documents, the doctor refuses to supply him with the medical papers necessary to get a work permit. Orns prescribed medication is also mentioned, and it is unclear whether she is taking pain killers or anti-depressants.

Orn brings Min to her husbands work and instead of using the prescription she got for Mins rash, Orn prepares a homemade concoction, a mix of finely chopped vegetables and store-bought skin creams. Orn also mentions to her husband that shed like to have another child. Her husband is sympathetic, but is afraid to try again because their first-born child died. While waiting in silence, Min is approached by a man who places his hand on Mins lap suggestively.

Roong works in a factory, painting ceramic figurines. She does not want to work after having worked overtime the day before. Min and Orn drive over to the factory, and Roong feigns an illness and takes off with Min. She drives into the countryside, while Min directs her to a secret location she does not know. They come upon a cliff-side plateau over looking the mountainous jungle. Roong is surprised by the beautiful view and they have a romantic picnic on the cliff and later by a river in the jungle. It is mentioned briefly by Roong that she dislikes Orn, and also claims that no one likes the older woman.

Orn, meanwhile has an erotic affair with one of her husbands co-workers. They are suddenly interrupted when the motorcycle is stolen. The man chases after the thief and disappears, leaving Orn in anxious solitude. She ends up lost in the woods, and eventually stumbles upon the young lovers by the river while Roong is giving oral fellatio to Min.

Orn approaches the young couple after they finish. Roong leads Orn into a stream where they both rub lotion on Min. They then dry off and lie down by the river bank, Roong with Min and Orn by herself. Orn looks over at the couple and becomes emotional. She discovers ants on all the picnic articles and throws the majority of the garbage into the river. She brushes off the picnic blanket, lies down on it and begins to cry. Meanwhile, Roong brings Mins penis out of his pants and rubs it with her hand. She slowly falls asleep as she whispers Mins name. The final shot is of Roong turning over and gazing directly into the camera.

The film ends with text stating: December 2001. Min is in Bangkok while waiting for work at a casino on the Thai-Cambodian border. Roong got back together with her boyfriend and they sell noodles in a town not far from Bangkok. Orn continues working as an extra in Thai movies.

==Cast==
* Kanokporn Tongaram as Roong
* Min Oo as Min
* Jenjira Jansuda as Orn

==Title sequence and soundtrack== Summer Samba (So Nice)", composed by Marcos Valle and sung by the Thai artist, Nadia.

==Alternate versions== censored in Thailand, with about 10 minutes cut from the Thai DVD release.

==Festivals and accolades== Cannes Film Festival – Un Certain Regard Prize 
* 2002 Thessaloniki Film Festival – Golden Alexander
* 2002 Tokyo FilmEx – Grand Prize
* 2003 International Film Festival Rotterdam – Circle of Dutch Film Critics (KNF) Award
* 2003 Buenos Aires International Festival of Independent Cinema – Best director and FIPRESCI Prize
* 2003 Singapore International Film Festival – Young Cinema Award

==See also==
* List of Thai films Nudity in film (East Asian cinema since 1929)

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 