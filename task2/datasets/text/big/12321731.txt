Dreams on Spec
{{Infobox Film
| name = Dreams on Spec
| image = DREAMS poster.jpg
| image size = 190px Scott Alexander Larry Karaszewski
| director  = Daniel J. Snyder
| producer =Daniel J. Snyder Gary Edgren
| cinematography= Harry Frith
| music =Deane Ogden
| distributor = Mercury Productions
| runtime = 86 min.
| country        = United States
| language       = English
}}

Dreams on Spec is a 2007 American documentary film that profiles the struggles and triumphs of emerging Hollywood screenwriters.  It was written and directed by Daniel J. Snyder, who learned first-hand about the screenwriters travails in the late 1980s when he was a teenager working alongside aspiring writer/directors Quentin Tarantino and Roger Avary in the famed Video Archives video store in Manhattan Beach, California.  

==Synopsis==
The film follows three aspiring screenwriters as they struggle to turn their scripts into movies.    David is a hip talent agents assistant with three scripts circulating around town. Hes plugged into "young Hollywood" - and when hes not working or writing, hes usually hanging out at the beach. Joe is a middle-aged family man who has split time over the last three years between caring for his autistic daughter and writing what he believes could be the great American screenplay.  And Deborah is trying to become one of the few African-American women to ever write and direct a feature film, though shes struggling just to pay her bills while she searches for money to produce her script.  Between these stories, the film intercuts critical insight from such Hollywood screenwriters as James L. Brooks, Nora Ephron, Carrie Fisher, Gary Ross, Steven E. de Souza, Ed Solomon, Paul Guay, Scott Alexander and Larry Karaszewski. 

==Selected quotations==
The established filmmakers interviewed for the documentary offer anecdotes about working in Hollywood.

Writer-director Nora Ephron (When Harry Met Sally and Youve Got Mail) describes why she believes there are so few women making films: "Its a very male business, and it has in vast portions of it&mdash;the whole action movie part of it might as well be the United States Army in 1943, in that the ethics of it are boot camp and action movies and guns and explosions and all the rest of it, and that&mdash;so that means that&mdash;that about 50% of the business is not only pretty much closed off to women, but women don’t even wanna be in it."
 Terms of Endearment) relates his love for screenwriting: "I never knew anybody who ever got a Writers Guild card who didn’t have a hard time when somebody said, What do you do for a living? saying, Im a writer.  Your&mdash;your voice always catches on a writer.  I think it takes about 14 years to not have the catch in your voice if you’re very aggressive.  It takes longer if youre not.  Because ... so many of us have dreamt about it forever as a dream that could not be realized."

  that asked me to rewrite Hook (film)|Hook&mdash;just to rewrite Tinker Bell. But that makes no sense because you cant just write one character. There is another character that they speak to. Although, you know, it was Robin’s character mostly, so I would improvise with Robin Williams.  Well, he and I do that anyway.  So now you have two people that desperately need medication, but its fine if theyre off and you’re taking notes.  And we had a very good time."
 The Little Rascals) says, “The thing that separates more successful writers from less successful writers, the most important thing, is the perseverance.”  The writer-director of Seabiscuit (film)|Seabiscuit, Gary Ross, adds that the term “success” is rather elusive.  “There’s a great line in J.D. Salinger when he talks about writing, he says, “The ultimate question is not ‘Were you successful or weren’t you successful?’ ...  The real question at the end of your days when you’re judged as a writer is, ‘Were all your stars out? ...  Did you live up to your potential?”

==Production notes==
Dreams on Spec was filmed in 2004 and 2005 in Los Angeles, California, and Portland, Oregon.  The documentary was shot in the widescreen, 16 x 9 aspect ratio.

Cinematographer Harry Frith put a Panasonic DVX100 camera in a splash bag and took it into the water to film the scenes where aspiring writer David Stieve goes surfing on Venice Beach.  Frith himself is an avid surfer. 
 
For almost the entire production, director Daniel J. Snyder worked at his day-job as a producer/director of non-fiction television.  He only worked on Dreams at night, on weekends, and during his hiatus periods.

== References ==
 

==External links==
* 
*  

 
 
 
 
 
 