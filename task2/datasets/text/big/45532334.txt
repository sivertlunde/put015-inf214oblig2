Sjätte skottet
{{Infobox film
| name           = Sjätte skottet
| image          =
| caption        =
| director       = Hasse Ekman
| producer       = Lorens Marmstedt
| writer         = Gösta Stevens
| narrator       =
| starring       = Edvin Adolphson Karin Ekelund Gunn Wållgren Nils Lundell
| music          = Kai Gullmar, Sune Waldimir
| cinematography =
| editing        =
| distributor    = Terrafilm 
| released       =  
| runtime        = 84 minutes
| country        = Sweden Swedish
| budget         =
| gross          =
}}
 Swedish drama drama film directed by Hasse Ekman.

==Plot==
Two people who feel unsuccessful meet one evening in Monte Carlo. Georg Winkler even plans to commit suicide, but he is hindered by Marguerite Hoffman and they start discussing their lives over a drink. They agree to start a new life together. 

Georgs talent is pistol shooting and Marguerite has been an actress, but not a successful one. They now start touring under the name "Hard & Hardy", where Georg uses his pistol in a lethally dangerous number with Marguerite as a target. They are now touring with a hit number, happy and in love. But even the best of plans can go terribly wrong...

== Cast ==
*Edvin Adolphson as Georg Winkler, "Mr. Hard"
*Karin Ekelund as Marguerite Hoffman, "Miss Hardy" 
*Gunn Wållgren as Lulu 
*Nils Lundell as Toni, the clown 
*Olof Widgren as Björn Hoffman
*Tore Lindwall as Chief Superintendent
*David Erikson as stablekeeper
*Sten Hedlund as  police commissary  
*Nils Johannisson as Director Möller  Wiktor "Kulörten" Andersson as Carnival Barker
*Otto Moskovitz as Kiki, dwarf at Cirkus Zoo 
*Willi Wells as danish konferencier

== External links ==
*  

 

 
 
 
 
 

 