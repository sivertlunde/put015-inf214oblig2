Princess Kaiulani (film)
{{Infobox film
| name           = Princess Kaiulani
| image          = Princess film.jpg
| caption        = Poster for US theatrical release
| director       = Marc Forby
| producer       =
| writer         =  
| starring       =  
| music          = Stephen Warbeck
| cinematography =
| editing        =
| studio         =  
| distributor    = Roadside Attractions
| runtime        =
| released       =  
| country        = United Kingdom United States
| language       = English
| budget         = $9 million 
| gross          = $883,887 
}} Princess Ka iulani (1875–1899) of the Kingdom of Hawaii.

==Description== Lorrin Thurston new constitution Royal Guards. Amidst the chaos of the moment Kaiulani is taken away for her own safety by her Scottish father, Archie Cleghorn, and sent to England for both protection and education. 
 failed native rebellion against the new constitution Thurston had enough support to arrest and depose Queen Liliuokalani, overthrowing the monarchy. After discovering the Davies family knew of her familys overthrow but hid the news from her Kaiulani decides to leave England and call off her engagement. The Princess instead decides to travel to the United States, where she gathers media attention and denounces the overthrow as well as the U.S.s involvement. Her cultured and regal appearance overcomes the racist views against her and many note that she is not at all the "Barbarian Princess" she was depicted as. Her campaign against the overthrow climaxes with her meeting President Cleveland. At a lunch with the President Kaiulani charms him and convinces Cleveland to actively oppose the overthrow, despite Cleveland having only a few weeks left in office. 
 annexation proposal, annexing Hawaii as the 50th state in the United States of America. She attends a small private funeral for the Kingdom of Hawaii hosted by her aunt, Queen Liliuokalani. Shortly after her return she is visited by Sanford B. Dole, who explains that three U.S. legal commissioners are arriving and that he and Thurston would like the Princess to host a small dinner for them. At the dinner Kaiulani charms her visitors before surprising Thurston by publicly petitioning for an amendment to the annexation treaty guaranteeing universal suffrage to all Native Hawaiians. She is disregarded by Thurston, who points out she is not a recognized diplomat, before Dole declares he will petition the amendment for her. As the amendment gathers support amongst the dinner guests Thurston, embarrassed and angered, leaves.

After the dinner Kaiulani is surprised to learn that Clive has come to Hawaii and she goes to see him. He tells her that his father passed and he is now in charge of his assets, including those in Hawaii. The two make up and Clive asks Kaiulani to return to England and marry him. Kaiulani however refuses, stating that her future is in Hawaii. The two share a last farewell kiss before Clive leaves for England. The film ends with Kaiulani returning her treasured seashells, which she had kept throughout her travels to remind her of Hawaii, back to the ocean as she wades in the waves, with a voiceover saying that the bright flame of Kaiulani is kept alive by the love of her people. A post credits card shows that Kaiulani died less than one year after annexation, some say of a broken heart for the loss of her kingdom. Another card mentions that in 1993 President Clinton signed the Apology Resolution, apologizing to Hawaii for any role the U.S. played in the overthrow.

== Cast == Princess Ka iulani
* Barry Pepper as Lorrin A. Thurston
* Shaun Evans as Clive Davies
* Will Patton as Sanford B. Dole
* Jimmy Yuill as Archibald Scott Cleghorn Queen Liliuokalani King Kalākaua Prince David Kawānanakoa "Koa"
* Kimo Kalilikane as Kalehua
* Kamuela Kalilikane as Mamane
* Peter Banks as President Grover Cleveland
* Rosamund Stephen as Frances Folsom Cleveland Preston|Mrs. Cleveland
* Julian Glover as Theophilus Harris Davies
* Barbara Wilshere as Mrs. Davies
* Tamzin Merchant as Alice Davies
* Catherine Steadman as Miss Barnes
* Kainoa Kilcher as Kaleo
* Laura Soller as Mrs. Anna Dole
* Christian Brassington as Duke of Winchester
* Olivia Mardon as Duchess of Winchester Premier Gibson
* Kaalakaiopono Faurot as Keiki (Baby) Ka iulani

== Controversy ==
The films working title "Barbarian Princess" provoked controversy in Hawaii, with individuals stating that it brings up painful memories of past discrimination.   In response, the title was briefly changed to The Last Princess,  changed to Princess Kaiulani later in 2008,  then shown as Barbarian Princess for the 2009 festival.  The producers stated that the title was meant to be ironic and is meant to draw audiences who may not be familiar with the history of Hawaii.  The film was finally released for wider distribution as Princess Kaiulani.

Many native Hawaiians were disappointed that the film used a non-Hawaiian for the title role. 

==Critical reception==
Rotten Tomatoes reported that only 23% of critics gave the film a positive review. Roger Ebert called it an "interesting but creaky biopic."
Hailed by the Hollywood Reporter and panned by the New York press, Princess Kaiulani was either praised or left audiences cold. 
The film won the Audience Award for "Best Feature" at the 2009 Honolulu International Film Festival in a tie with  .

== References ==
 

== External links ==
*  
*  
* 
* http://www.youtube.com/watch?v=gubVK_Si-Y4#t=334 Comments by Palani Vaughan

 
 
 
 
 
 
 
 