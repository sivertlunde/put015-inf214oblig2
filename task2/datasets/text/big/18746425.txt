A Yank in Rome
 
{{Infobox film
| name           = A Yank in Rome
| image          =
| caption        =
| director       = Luigi Zampa
| producer       = Gino Castrignano Carlo Ponti
| writer         = Gino Castrignano Aldo De Benedetti Luigi Zampa
| starring       = Valentina Cortese
| music          = Nino Rota
| cinematography = Václav Vích
| editing        = Eraldo Da Roma
| distributor    =
| released       =  
| runtime        = 110 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

A Yank in Rome ( ) is a 1946 Italian comedy film directed by Luigi Zampa and starring Valentina Cortese.   

==Cast==
* Valentina Cortese - Maria, La maestrina
* Andrea Checchi - Roberto
* Leo Dale - Dick
* Adolfo Celi - Tom
* Paolo Stoppa - Sor Augusto
* Elli Parvo - Elena
* Giovanni Dolfini - Don Giuseppe
* Felice Minotti - Il padre di Roberto
* Gino Baghetti - Monsignor Caligaris
* Anna Maria Padoan - La signorina Paolina
* Arturo Bragaglia - Il sacrestano
* Oreste Fares - Il portiere di casa Caligaris
* Luciano Salce - Lufficiale americano
* Elettra Druscovich - La contessina Arcieri
* Achille Ponzi - Pietro, il cameriere

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 