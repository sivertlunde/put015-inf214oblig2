Fall Guy
 
{{Infobox film
| name = Fall Guy
| image = Fall Guy.jpg
| image_size = 
| caption = DVD cover to Fall Guy (1982)
| director = Kinji Fukasaku 
| producer = Haruki Kadokawa 
| writer = Kouhei Tsuka
| narrator = 
| starring = Morio Kazama Keiko Matsuzaka
| music = Masato Kai
| cinematography = Kiyoshi Kitasaka
| editing = Isamu Ichida
| distributor = Shochiku
| released = October 9, 1982
| runtime = 109 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1982 Japanese Japan Academy Prize ceremony. 

"Kamata koshin-kyoku" is originally a title of the tune that covered song "Song of the Vagabonds" of operetta " " that was the stage of the movie. Originally, Yutaka Kawasaki and Naoko Soga sang this tune. In the movie, Keiko Ishizaka, Morio Kazama, and Mitsuru Hirata are singing.

==Cast==
*Keiko Matsuzaka as Konatsu
*Morio Kazama as Ginshiro
*Mitsuru Hirata as Yasu
*Chika Takami as Tomoko
*Daijiro Harada as Tachibana
*Keizo Kanie as Film Director
*Rei Okamoto as Toku-san
*Hyoei Enoki as Tome
*Nagare Hagiwara as Yuji
*Toshiya Sakai as Makoto
*Akihiro Shimizu
*Nijiko Kiyokawa as Yasus mother
*Sonny Chiba as Actor
*Hiroyuki Sanada as Actor
*Etsuko Shihomi as Actress
*Seizo Fukumoto
*Akira Shioji as Yamada

==Awards==
4th Yokohama Film Festival   
*Won: Best Supporting Actor - Mitsuru Hirata
*2nd Best Film

==Bibliography==
*  
*  
*  
*  
*  
*    
*  

==Notes==
 

 
{{Navboxes  title = Awards  list =
 
 
 
 
 
}}

 
 
 
 
 
 
 


 