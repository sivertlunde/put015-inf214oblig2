Wild Things: Foursome
 
{{Infobox film
| name           = Wild Things: Foursome
| image          = Wild Things - Foursome.png
| caption        = DVD cover
| director       = Andy Hurst
| producer       = Marc Bienstock
| writer         = Howard Zemski Monty Featherstone John Schneider Ethan S. Smith
| music          = Steven M. Stern
| cinematography = Jeffrey D. Smith
| editing        = Anthony Adler
| studio         = Mandalay Entertainment RCA Media Group
| distributor    = Stage 6 Films
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Wild Things: Foursome is the fourth installment in the Wild Things series. The film was released as a direct-to-DVD video on June 1, 2010.

==Plot== John Schneider) investigates Teds death which happened under questionable circumstances. Teds lawyer, George Stuben (Ethan S. Smith), suddenly announces during the reading of the will that Carson cannot inherit Teds money and estate until he marries. Carson quickly gets married to Rachel, and inherits. Carson and Rachel decide to kill the two other women and keep all the money for themselves. However, Rachel, along with Brandi and Linda, are also plotting to kill Carson so that Rachel can inherit as a grieving widow and split the money three ways. Rachel does murder Carson and inherits his money. Rather than split the money, Rachel plots to murder Brandi, as well as Linda, to keep all of the inheritance money for herself. Brandi, anticipating that Rachel might do just that, plots a scheme of her own to kill Rachel. However, Linda has an agenda of her own to steal the money for herself, along with her shadowy partner-in-crime who is the mastermind behind everything.

==Cast==
* Jillian Murray as Brandi Cox
* Marnette Patterson as Rachel Thomas
* Ashley Parker Angel as Carson Wheetly John Schneider as Detective Frank Walker
* Ethan Smith as George Stuben
* Jessie Nickson as Linda Dobson
* Cameron Daddo as Ted Wheetly
* Marc Macaulay as Captain Blanchard
* Josh Randall as Shane Hendricks

==External links==
*  

 

 
 
 
 
 
 
 


 