The Trail to Yesterday
{{Infobox film
| name           = The Trail to Yesterday
| image          =
| caption        =
| director       = Edwin Carewe
| producer       = Metro Pictures
| writer         =
| starring       = Bert Lytell Anna Q. Nilsson
| music          =
| cinematography = Robert Kurrle
| editing        =
| distributor    = Metro Pictures
| released       = May 6, 1918
| runtime        = 6 reels
| country        = USA
| language       = Silent..English
}}
The Trail to Yesterday is a 1918 silent film western directed by Edwin Carewe and starring Bert Lytell and Anna Q. Nilsson. It was produced by and distributed by Metro Pictures. It is based on a novel, The Trail to Yesterday c.1913, by Charles Alden Seltzer. 

Some filming took place at Arivaca, Arizona. 

Surviving print held by EYE Institut/Filmmuseum, Netherlands. 

==Cast==
*Bert Lytell - Ned "Dakota" Keegles
*Anna Q. Nilsson - Sheila Langford
*Harry S. Northrup - Jack Duncan
*Ernest Maupain - David Langford
*John A. Smiley - Ben Doubler
*Danny Hogan - "Texas" Blanco

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 

 