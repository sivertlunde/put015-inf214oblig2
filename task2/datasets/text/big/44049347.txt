Swarnappakshikal
{{Infobox film 
| name           = Swarnappakshikal
| image          =
| caption        =
| director       = PR Nair
| producer       = Vasanthi
| writer         = Mani Muhammed
| screenplay     = Mani Muhammed
| starring       = Sukumari Nedumudi Venu Jose Prakash Nagavally R. S. Kurup
| music          = Raveendran
| cinematography = Divakara Menon
| editing        = P Raman Nair
| studio         = Chinthu Films
| distributor    = Chinthu Films
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by PR Nair and produced by Vasanthi. The film stars Sukumari, Nedumudi Venu, Jose Prakash and Nagavally R. S. Kurup in lead roles. The film had musical score by Raveendran.   

==Cast==
 
*Sukumari
*Nedumudi Venu
*Jose Prakash
*Nagavally R. S. Kurup
*Sukumaran
*Alummoodan
*Jagannatha Varma
*Jalaja
*Leela Namboothiri
*Muralimohan
*Nellikode Bhaskaran Swapna
 

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Mullanezhi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Devaangane Neeyee || K. J. Yesudas || Mullanezhi || 
|-
| 2 || Kollam kandaal illam venda || P Jayachandran, Chorus || Mullanezhi || 
|-
| 3 || Smrithikal Nizhalukal || K. J. Yesudas || Mullanezhi || 
|-
| 4 || Thaamarappoovilayaalum || S Janaki || Mullanezhi || 
|}

==References==
 

==External links==
*  

 
 
 

 