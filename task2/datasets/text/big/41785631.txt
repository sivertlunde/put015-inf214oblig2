God Help the Girl (film)
 
 
{{Infobox film
| name           = God Help the Girl
| image          = GHTG poster.jpg
| alt            = 
| caption        = Film poster Stuart Murdoch
| producer       = Barry Mendel
| writer         = Stuart Murdoch
| starring       = Emily Browning Olly Alexander Hannah Murray Pierre Boulanger
| music          = 
| cinematography = Giles Nuttgens
| editing        = David Arthur
| studio         = Barry Mendel Productions British Film Company Zephyr Films Amplify
| released       =  
| runtime        = 111 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $102,757 
}} musical drama Stuart Murdoch of the band Belle and Sebastian.    The film premiered in-competition in the World Cinema Dramatic Competition at 2014 Sundance Film Festival on 18 January 2014.  

The film served as the opening night film of the Generations section at the 64th Berlin International Film Festival on 9 February 2014.         
 Amplify acquired the distribution rights of the film. It was released theatrically and Video on demand on 5 September 2014 in the United States.   

==Plot==
Eve escapes from the psychiatric hospital where she is being treated for anorexia nervosa and makes her way to Glasgow, hoping to become a musician. At a gig, she meets James, a lifeguard and aspiring songwriter. He introduces her to his guitar student Cassie, and the three become friends.

Eve meets Anton, the arrogant singer of Wobbly-Legged Rat, a Glasgow band attracting attention from a local radio station. She gives him a tape of her music to pass on and they begin seeing each other.

James convinces Eve she needs bass and drums to finish her songs. They and Cassie form a band, God Help the Girl, with some local musicians. Anton admits he never gave Eves tape to the radio producers, saying she needs better production and musicianship, and they argue.

James discovers Eves relationship with Anton and becomes distanced from her. Feeling alone, Eve takes drugs and returns to hospital. She tells James she plans to attend music college in London, and they reconcile. After God Help the Girl performs their final concert, the radio station play Eves tape. The next day, she leaves for London.

==Cast==
*Emily Browning as Eve
*Olly Alexander as James
*Hannah Murray as Cassie
*Pierre Boulanger as Anton

==Reception==
God Help the Girl received mixed reviews. Review aggregator Rotten Tomatoes reports that 68% of 40 reviews have given the film a positive review, with an average score of 5.7 out of 10. 

Dennis Harvey of Variety (magazine)|Variety wrote that God Help the Girl "is a slender exercise in self-conscious charm."  David Fear of Esquire (magazine)|Esquire praised it as "rife with the kind of giddy thrills and hormonal flushes you associate with being a teen."  Jonathan Romney of Film Comment Magazine said that "its easy to categorize Murdochs film as a vanity project, but if it is, its a very honest one."  David DArcy of Screen International said the film "has a soft whimsy that connects to a time before video clips put editing rhythms into overdrive."  Xan Brooks of the theguardian.com|Guardian gave the three out of five stars, writing: "Its warm and generous, verging on the sentimental; a film that crystallises the best and worst of Belle and Sebastians songwriting skills." 

Leslie Felperin of the Guardian gave the film two out of five stars and called it "disastrous, fatally flawed by a shoddy script and poor direction, like something made by the most ostensibly talented guy at art school ... Its not funny or clever, or even musically very interesting. Its just bad."  Rodrigo Perez of   called the film "an indie musical that feels like one long B-side."  Sarah Sahim, writing for Pitchfork Media|Pitchfork, called the film "an egregious mess that romanticizes a woman’s struggles with an eating disorder for the sake of Murdoch’s self-promotion", and criticised the films lack of racial diversity: "a microcosmic view of what is wrought by racial exclusivity that is omnipresent in indie rock." 

==Accolades==
 .]]
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="4" | 2014 Sundance Film Festival
| World Cinema Grand Jury Prize: Dramatic Stuart Murdoch
|  
|-
| World Cinema Dramatic Special Jury Award (Ensemble)
| Emily Browning Olly Alexander Hannah Murray Pierre Boulanger
|    
|-
| Berlin International Film Festival
| Crystal Bear Stuart Murdoch
|  
|}
 

==References==
 

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 