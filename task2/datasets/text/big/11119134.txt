Ara Soyza
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Ara Soyza
| image          =
| director       = Herbert Ranjith Peiris
| producer       = U.A.S. Hameed
| writer         = Herbert Ranjith Peiris
| starring       = Fredee Silva Wimal Kumara De Costa Don Sirisena Nihal Kumarapathirana B.S. Perera Liliyan Edirisinghe Rukmani Devi Sabeetha Perera Raju Kumarasinghe
| cinematography = Mercelin Perera Norbat Rathnasiri
| editing        = Stanley Alwis
| distributor    = Samagi Films
| released       = June 11, 1984
| runtime        = 127 minutes
| country        = Sri Lanka
| language       = Sinhalese language
}} 1984 Comedy film directed by Herbert Ranjith Peiris.The film centers on three middle aged men staying in a hostel. At a later stage, another tenant comes to stay in the hostel, where he falls in love with the eldest daughter of the hostel owner. The entire film was based on how the old three tenants trying their best to chase the new tenant and win the heart of owners daughter. The plot is based on the Tamil film Indru Poi Naalai Vaa.

==Plot==
The lead character Soyza (Fredee Silva), is currently a modest farmer who grows potatoes together with his housemates (Don Sirisena ) and Costa (Wimal Kumara De Costa). The villain or the other housemate, Wadigapatuna (Nihal Kumarapathirana) is a Mudalali businessman who owns a shop in the village. Unlike Soyza, Don and Costa, Wadigapatuna is a selfish and a bad person.

Soyza and Wadigapatuna fall in love with the eldest daughter of the owner of their rental home and fight against each other to win the heart of the girl Kanthi.

The parents of the daughter in question, Kanthi prefer Wandigapatuna and the plot revolves around the many tricks Soyza and his colleuges play to win the heart of Kanthi. This involves dressing up don Sirisena as a pregnant women to accuse Wandigapatuna of producing a child out of wadlock with her. 
Movie culminates with a fight between Wandigapatuna and a nearby "strongman" who joins Soyza and the clique.

==Characters==

*Soyza (Fredee Silva) is a modest farmer, working together with two other friends. Not so wealthy, yet rich in love. After series of battles with his only rival Wadigapatuna and the mistress of the house, he gets the chance to marry the owners eldest daughter Kanthi Kiridena].

*Costa (Wimal Kumara De Costa) is Soyzas best mate. He always help Soyza, in battles against Wadigapatuna.

*Don (Don Sirisena) is a friend of both the above, yet most of the times slightly selfish. He falls in love with the owners youngest daughter Shanthi Kiridena (Sabeetha Perera). But, similar to Costa, he always try to help Soyza in fights against Wadigapatuna.

*Wadigapatuna (Nihal Kumarapathirana) is a rich businessman, who rents a room in the same house where Soyza. Costa and Don resides. With a gigantic body and height, he always try achieve whatever he wants.

*Kanthi (Raju Kumarasinghe) The eldest daughter of the owner. With a face like a crushed tomato (as always referred to in the film), falls in love with Soyza. She somehow convince Soyza to fight with Wadigapatuna and chase him out of the house.

*Tarzan (Raju Kumarasinghe) With a body made out of matches, he comes to help Soyza to fight against Wadigapatuna. He even wins a boxing match against Wadigapatuna. He does so by cheating.

*Uncle (B.S.Perera) The owner of the rental property where the story occurs. A retired classical musician, always get abused by his dominative wife Haminey (Liliyan Edirisinghe). Most of the times he go to Zoysa, Costa and Don to ask really silly questions. At one time, he goes to Don and ask what has happened to his tie knot, in which Costa answers "hey uncle... I saw the knot in the garden... Please leave us now!"

*Haminey (Liliyan Edirisinghe) Being the wife of the owner, she domniates the entire house. She always blame her husband for been stupid aad wasting her time by talking non-sense. She hates Zoysa, Costa and Don and tries her best to marry her eldest daughter to Wadigapatuna.

*Shanthi (Sabeetha perera) Naturally beautiful, she is the youngest daughter of the owner. Until the end of the film, she strongly oppose Dons love.

==Songs==
The film has only a handful of songs, which seems really unusual when compared with the other Sri Lankan films produced in the 1980s .

One of the best songs in the film is a Wadha Baila musical by Soyza and Wadigapatuna. In the song, we can see Soyza dressed as an angel and Wadigapatuna dressed as a devil. They both sing to Kanthi, who is sitting in-between like a goddess.

 
 
 
 