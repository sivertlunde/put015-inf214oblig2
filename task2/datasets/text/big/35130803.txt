The Woman Racket
{{infobox film
| name           = The Woman Racket
| image          =
| imagesize      =
| caption        =
| director       = Robert Ober  Albert H. Kelley Paul Bern(uncredited)
| producer       = Metro Goldwyn Mayer
| writer         = Philip Dunning (play Night Hostess)
| starring       = Blanche Sweet
| music          = William Axt
| cinematography = J. Peverell Marley
| editing        = Anton Stevenson Basil Wrangell
| distributor    = Metro-Goldwyn-Mayer
| released       = January 24, 1930
| runtime        = 70 minutes
| country        = United States
| language       = English
}} Tom Moore and Blanche Sweet. It is based on a 1928 Broadway play, Night Hostess by Philip Dunning. In January 2012 the film became available on DVD from the Warner Archive collection home library.    

One of three Blanche Sweet talking films.

Example of Pre-Code dialogue in the film:
:Tom: Julia, see that star up there?
:Julia: Tom!, every man Ive known has tried to show me that star up there.

==Cast== Tom Moore - Tom Hayes
*Blanche Sweet - Julia Barnes Hayes Sally Starr - Buddy
*Robert Agnew - Rags Conway
*John Miljan - Chris Miller
*Tenen Holtz - Ben
*Lew Kelly - Tish
*Tom London - Hennessy
*Eugene Borden - Lefty
*Jack Byron - Duke(*as John Byron)
*Nita Martan - Rita(her scenes deleted)
*Richard Travers - Frank Wardell

unbilled
*Ann Dvorak - Chorus Girl
*Fred Kelsey - Police Captain
*Wilbur Mack - The Mayor
*Leo White - Oscar the Chef

==See also==
*Blanche Sweet filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 