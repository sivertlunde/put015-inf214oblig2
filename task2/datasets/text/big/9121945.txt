De Taali
{{Infobox film
| name           = De Taali
| image          = Detaali.jpg
| caption        = Theatrical poster
| director       = E. Nivas
| cinematography = Amitabha Singh
| producer       = Ravi Walia
| starring       = Aftab Shivdasani Ayesha Takia Ritesh Deshmukh Rimi Sen
| music          = Vishal-Shekhar
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
}}
De Taali ( : دے تالی,   film that stars Aftab Shivdasani, Ayesha Takia, Ritesh Deshmukh and Rimi Sen in the main roles. The movie is a remake of the 2001 film Saving Silverman.  It is directed by E. Nivas and produced by Ravi Walia. The film revolves around three childhood friends who have grown together and when Rimi Sen comes in their life, the whole dynamics of their friendship changes.  The film was initially titled Jalsa. De Taali was released on 20 June 2008

==Plot==
Paglu (Ritesh Deshmukh), Abhi (Aftab Shivdasani) and Amrita Amu (Ayesha Takia) are best friends since childhood. Abhis father (Anupam Kher) is a very rich businessman, and his only son has no interest in work. Abhi has had many girlfriends, but none of them loved him. Paglu and Abhis father tells Amu to marry Abhi because they know each other very well. One day Abhi comes and tells Amu that he loves someone that is his childhood friend; Amu thinks that she is the girl whom Abhi loves and she, too, falls in love with him. 

Abhi later reveals that Kartika (Rimi Sen) is whom he loves.. Karthika is actually after his money. Amu is heartbroken. Amu and Paglu start hating her and try to get them separated. Abhi gets angry with them and decides to marry her in another country. At the airport Amu hits Kartika and she falls unconscious. Amu and Paglu kidnap Kartika and force her to write a letter to Abhi saying that she doesnt love him. Paglu tells Abhi that Amu is the right girl for her, and Abhi falls in love with her. Paglu, with Karthika still kidnapped, finds out that her name is Anjali, not Karthika. She is not in love with Abhi but with his money, and she has a history of cheating people. Paglu frees her and she tells Abhi about the kidnapping. Abhi is unhappy hearing this and decides to marry Karthika anyway. On the wedding day, Paglu tries to stop them from getting them married by bringing all the people she cheated; Abhi gets angry and tells Paglu and Amu to get out. The next day, Abhi comes back, apologizes to them and says that he isnt married. He tells them that yesterday when they left Anjali told him that she has learned a lot from the time Paglu and Amu kept her in captivity. She apologizes that she cant marry him and that Amu is the right girl for him. Abhi proposes to Amu. Later, Paglu ends up with the changed Anjali.

== Cast ==
* Aftab Shivdasani ... Abhi/Abhishek
* Ayesha Takia ... Ammo/Amrita
* Ritesh Deshmukh ... Paglu/Paresh Gurudev
* Rimi Sen ... Anjali/Karthika
* Anupam Kher ... Abhis Father
* Neha Dhupia ... Sara (Special Appearance)
* Anjana Sukhani...Anjali(Special Appearance)
== Direction ==
* Mohd Rafiq Lasne ... Associate Director

==Music==
{{Infobox album
| Name        = De Taali
| Type        = soundtrack
| Artist      = Vishal-Shekhar
| Cover       = 
| Caption     = 
| Released    =  
| Recorded    = 
| Genre       = Film soundtrack
| Length      = 
| Label       = T-Series
| Producer    = Ravi Walia| 
| Last album  = Bhoothnath  (2008)
| This album  = De Taali (2008)
| Next album  = Bachna Ae Haseeno (2008)
}}

The music was conducted by the duo Vishal-Shekhar.

===Track listing===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s)!! Duration
|-
| 1
|"Everybody Put Your Hands Together"
| Shaan (singer)|Shaan, Sunidhi Chauhan & Anushka Manchanda
| 3:12
|-
| 2
|"De Taali"
| Shaan & Sunidhi Chauhan
| 4:09
|-
| 3
| "Aaj Mein Boond Hoon" Shekhar Ravjiani & Shreya Ghoshal
| 3:55
|-
| 4
| "Hone Lagi"
| Anushka Manchanda & Shekhar Ravjiani
| 4:05
|-
| 5
| "Maari Teetri Maari Teetri (The Butterfly Song)"
| Raja Hasan
| 4:46
|-
| 6
| "Tooti Phooti Dhadkone Ki"
| Krishnakumar Kunnath|KK, Shaan & Sunidhi Chauhan
| 4:45
|-
| 7
| "Hone Lagi (Jump Into Bed Mix)"
| Anushka Manchanda & Shekhar Ravjiani
| 4:20
|-
| 8
| "De Taali (The Clap Trap Mix)"
| Shaan & Sunidhi Chauhan
| 3:50
|}

== Reception ==
The film did not really take off at the box office. And to add to its woes the reviews were not too good either. However actor Riteish Deshmukh seems to have pleased everyone with his performance as he got some great reviews. Noyon Jyoti Parasara of AOL India said, "Truly this movie is Riteshs. Ritesh has already established himself as a good comic actor and De Taali seconds that." 

==References==
 

== External links ==
*  
*  
*   at IndiaFM

 
 
 