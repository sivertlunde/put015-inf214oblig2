Horrors of Malformed Men
{{Infobox Film
| name           = Horrors of Malformed Men
| image          = Horrors of Malformed Men poster.jpg
| image_size     = 
| caption        = Poster for Horrors of Malformed Men (1969)
| director       = Teruo Ishii
| producer       = 
| writer         = Edogawa Rampo (novel) Teruo Ishii Masahiro Kakefuda
| starring       = Teruo Yoshida Teruko Yumi
| music          = Masao Yagi
| cinematography = Shigeru Akatsuka
| editing        =  Toei
| released       = October 31, 1969
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1969 Japanese film in the ero guro (erotic-grotesque) subgenre of Toei Company|Toeis style of Pink film. Directed by Teruo Ishii, the film is considered a precursor to Toeis ventures into the "Pinky violent" style in the early 1970s. 

==Cast==
* Teruo Yoshida
* Teruko Yumi
* Tatsumi Hijikata

==Critical appraisal==
 Rampo to butoh, it taps into the countrys post-nuclear trauma so audaciously that people fled theaters in disgust upon its release and that it has been consistently barred from appearing on video or DVD since. Yet, it is beautiful, haunting and oneiric; it is the closest one can come to a dreamlike experience without closing ones eyes." 

==Availability==
In effect banned in Japan, Horrors of Malformed Men was rarely seen in the decades after its release. On August 28, 2007, Synapse Films and Panik House gave Horrors of Malformed Men a mass-market release on DVD region code|region-1 DVD. 

==Notes==
 

==Sources==
* {{cite web |url=http://wc03.allmovie.com/cg/avg.dll?p=avg&sql=1:405464|title=Horrors of Malformed Men
|accessdate=2007-10-14|last=Buchanan|first=Jason|publisher=Allmovie}}
*  
*  
*  
*  
*  

== External links ==
*  
*  

 

 
 
 
 
 
 


 
 