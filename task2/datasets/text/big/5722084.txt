Yogi (2007 film)
{{Infobox film
| name = Yogi
| image = Yogi.dvd.jpg Prem (story),   Rajendra Kumar (dialogues) Pradeep Rawat, Ali
| director = V.V. Vinayak
| producer = P. Ravidranath Reddy
| cinematography = Sameer Reddy
| editing = Gowtham Raju
| music = Ramana Gogula
| country = India
| released = 14 January 2007
| runtime = 155 minutes
| language = Telugu
| budget =  
| gross     =   
}}
 Telugu film directed by V.V. Vinayak, which has Prabhas and Nayantara paired up for the first time. Ramana Gogula, who composed music for V.V. Vinayaks earlier hit Lakshmi (2006 film)|Lakshmi, also composed music for this film. Songs were shot in Canada, Egypt, & Malaysia. The film was dubbed into Tamil as Murattu Thambi and into Malayalam as Yogi and in Hindi as Maa Kasam Badla Lunga by RKD Studios who exclusively own the Hindi rights of the film.

This is a remake of Kannada superhit Jogi (film)|Jogi. The film released on 14 January 2007 in 225 theatres. {{cite web| title=indiaglitz.com| work= Yogi to hit 225 theatres during release
|url= http://www.indiaglitz.com/channels/telugu/article/28395.html
|accessdate=11 January 2007}} 

== Plot == Pradeep Rawat). Chandra Mohan). He earns good money and purchases two gold bangles for his mother and plans to leave the city. At this juncture, Narsing humiliates Eeshwar for saving Kotaiah and stamps the gold bangles of his mother. Irked over this, Eeshwar kills Narsing in broad day light. As a result, he turns the biggest goon of the city. Meanwhile, Santamma reaches Hyderabad city to search for her son and takes shelter with Nandini (Nayantara), a journalism student. In order to save him from the police, Chandranna takes him out saying that his name is Yogi. Learning that Yogi killed Narsing, Kotaiah wants Yogi to join him, but to no avail. Chandranna encourages Eeshwar who turned Yogi to stay back in the city to save the poor, who gets harassed by the goons. Yogi remains a terror for the goons. So, Kotaiah and Saidulu join hands and hatch a plan to kill Yogi. While trying to carry it out, Yogi kills Saidulu. Nandini is also after Yogi for an interview to complete her practicals. Somehow, she falls in love with him. Though Yogi and Shantamma live in the same city, they could not meet each other. One day, Yogi meets Basha, who reveals that he saw Shantamma.

Though Basha and Santamma go to the address, they could not find him as Eeshar because he is known as Yogi for the people. Shantamma waits for her son in front of a temple, as Basha tells her that Eeshwar is in the habit of visiting the temple on every Monday. But Eeshwar did not turn up on Monday fearing attack by Saidulus people. Shantamma dies in front of the temple and Basha arranges funeral for her. At this time Yogi comes but fails to have a look at the covered face and helps the orphan body lifters with money for cremation and he even bring flowers and puts on the body of his mother. Yogi is unaware that the body is of his mothers body until it is moved to fire cabin. Basha finally arrives and explains the trauma but it is too late. He realizes the truth only after Nandini shows the belongings of his mother, who came to see him. The film ends with this emotional scene.

== Cast ==
* Prabhas ... Eeshwar Chandra Prasad alias Yogi
* Nayantara... Nandini
* Kota Srinivasa Rao ... Kotaiah, the main villain Pradeep Rawat ... Narsing Pahilwan, opposition of Kotaiah (guest appearance)
* Subbaraju ... Saidulu, younger brother of Narsing Ali ... Basha, Friend of Yogi Sharada ... Shantamma, mother of Yogi Venu Madhav
* Chalapathi Rao ... Ram Moorti Chandra Mohan ... Chandranna, supporter of Yogi
* Fish Venkat ... Henchman of Yogi
* Mumaith Khan in item number "Orori Yogi"

== Crew ==
* Director: V.V. Vinayak
* Screenplay: V.V. Vinayak
* Story: Prem
* Producer: P. Ravidranath Reddy
* Music: Ramana Gogula
* Background Music: Gurukiran
* Cinematography: Sameer Reddy
* Editing: Gowtham Raju
* Art Director: Chinna

== Music ==
The music of the film was launched on 15 December 2006. {{cite web| title=idlebrain.com| work= Audio launch-Yogi|url= http://www.idlebrain.com/news/functions/audio-yogi.html
|accessdate=3 January 2007}}  The film has six songs composed by Ramana Gogula:
* "Dolu Baja" - Shankar Mahadevan Karthik & Bangalore Sunitha
* "Ye Nomu Nochindo" - Suresh
* "Gilli Gichi" - Rajesh & Ganga Tippu & Sunitha
* "Gana Gana Gana" - Adnan Sami & Sudha

==Box office==
Despite getting negative to mixed reviews, {{cite web| title=cinegoer.com| work= A Feel That Never Even Starts
|url= http://www.cinegoer.com/reviews/yogi.htm
|accessdate=4 February 2007}}  {{cite web| title=filmchamber.com| work= Movie Review - Yogi
|url= http://www.filmchamber.com/tmpl.asp?it=rw1095 archiveurl = archivedate = 12 February 2007}}  Yogi managed to gross  13.12 crores in its opening week, {{cite web| title=sify.com| work= Yogi- A runaway hit!
|url= http://sify.com/movies/telugu/fullstory.php?id=14374337
|accessdate=4 February 2007}}  giving Prabhas a very big hit. As of March 2007, grossed a total of  25 crores and a share of   18 crores. {{cite web| title=indiaglitz.com| work= Yogi collected Rs 18 crore share
|url= http://www.indiaglitz.com/channels/telugu/article/29647.html
|accessdate=5 March 2007}} 

== Trivia ==
* This is the third film music director Ramana Gogula has not composed background music for. His two earlier films were Lakshmi (2006 film)|Lakshmi & Annavaram (film)|Annavaram.
* Director V.V. Vinayak had local girls as dancers and they are not supposed to Indecent exposure|expose. So, they wore body hugging slips under their specially designed costume. 2005 film Chhatrapati (film)|Chhatrapati. However in Chhatrapati, the protagonist searched for his mother. In this film, the protagonists mother searched for her son. Incidentally, both films star Prabhas in the lead role.

== See also ==
* Jogi (film)|Jogi
* Parattai Engira Azhagu Sundaram

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 