In Country
 
{{Infobox film
| name           = In Country
| image          = In country.jpg
| caption        = Theatrical release poster
| director       = Norman Jewison
| writer         = Frank Pierson Cynthia Cidre Bobbie Ann Mason (novel)
| starring       = Bruce Willis Emily Lloyd
| producer       = Norman Jewison Richard A. Roth
| music          = James Horner
| cinematography = Russell Boyd Lou Lombardo
| distributor    = Warner Bros.
| released       =  
| country        = United States
| language       = English
| runtime        = 120 minutes
| budget         = $18 million
| gross          = $3,532,000
}}
 Golden Globe nomination for his role.

==Plot== Vietnam veteran with post-traumatic stress disorder. Samanthas father, Dwayne, was killed in Vietnam at 21 after marrying and impregnating Samanthas mother, Irene. Samantha finds some old photographs, medals, and letters of her father, and becomes obsessed with finding out more about him.

Irene, who has moved to Lexington, Kentucky with her second husband, wants Samantha to move in with them and go to college. But Samantha would rather stay with Emmett and try to find out more about her father. Her mother is no help, as she tells Samantha, "Honey, I married him a month before he left for the war. He was 19. I hardly even remember him." Finally, Samantha, Emmett and her grandmother visit the Vietnam Veterans Memorial in Washington, D.C. Finding her fathers name in the memorial releases cathartic emotions in Samantha and her family.

==Cast==
* Bruce Willis as Emmett Smith
* Emily Lloyd as Samantha Hughes
* Joan Allen as Irene Kevin Anderson as Lonnie John Terry as Tom
* Peggy Rea as Mamaw
* Judith Ivey as Anita
* Daniel Jenkins as Dwayne
* Stephen Tobolowsky as Pete
* Jim Beaver as Earl Smith
* Richard Hamilton as Grandpaw
* Heidi Swedberg as Dawn
* Ken Jenkins as Jim Holly
* Jonathan Hogan as Larry
* Patricia Richardson as Cindy

==Production==

===Casting===
To prepare for her role, Emily Lloyd stayed with a lawyer and his family in Paducah, Kentucky. In order to get into the mindset of a girl whose father has died, the young actress thought of the death of her paternal grandfather, Charles Lloyd Pack, a British actor to whom she was very close.    Lloyd underwent training to speak with a Kentucky accent in the film.

The veterans in the dance sequence are all actual Vietnamas well nd their real family members accompany them.  Of the five major characters who are Vietnam veterans, only one, Earl, is played by an actual Vietnam veteran, Jim Beaver. Ken Jenkins, who plays Jim Holly (the organizer of the veterans dance), is the father of Daniel Jenkins, who plays Samanthas father Dwayne in the Vietnam flashbacks. Their casting in the film was purely coincidental. The commencement speaker was played by Don Young, the minister of a large Baptist church in Paducah, Kentucky. In an interview with The Paducah Sun, he said the speech had been written for him but joked that it was so good, he might "borrow" parts of it in future sermons.

===Filming=== Graves County, specifically its county seat of Mayfield, Kentucky|Mayfield, was the location for many scenes. The walk-in doctors office seen in the film is actually a dry cleaners which was renamed "Clothes Doctor" following its appearance in the film. Several other scenes were shot in the Purchases largest city of Paducah, particularly the scenes inside their home. In a brief scene shot outside the small home, located in Paducah, one can briefly observe a boxer and a black lab lying under a tree in the front yard. The boxer, Dempsey, belonged to the actual resident of the home, while the black lab was Dempseys lifelong pal from down the street, Scotty. Foot note: World War ll vets were also used in the film. Some scenes of the film were shot on location in Mayfeld, KY    ( Bobbie Ann Masons home town)and other scenes were shot in Paducah, KY, where Bruce Willis daughter Rumer was born.

==Release==
In Country had its world premiere on September 7, 1989 at the Toronto International Film Festival,    which Bruce Willis attended and dedicated to Canadian war veterans who fought in
Vietnam.   

===Box office performance===
The film was given a limited release on September 15, 1989 in four theaters grossing $36,505 on its opening weekend. It was given a wide release on September 29, 1989 in 606 theaters grossing $1.3 million on its opening weekend. It went on to make $3.5 million in North America.   

===Critical reception===
In Country was generally well received by critics. It has a 70% rating on  , is letter perfect &ndash; her accent impeccable and her energy immense".    USA Today gave the film three out of four stars and praised Bruce Willis "subsidiary performance as Lloyds reclusive guardian-uncle is admirably short on showboating".    In his review for The Guardian, Derek Malcolm praised Lloyd for her "portrait is of a lively waif who does not intend to be easily defeated by the comedy of life without adding a few jokes of her own, and it is the most complete thing she has so far done on the screen, good as she was in Wish You Were Here".    Time (magazine)|Time magazine felt that the script "perhaps pursues too many banal and inconsequential matters as it portrays teen life in a small town", but that "the film starts to gather force and direction when a dance, organized to honor the local Viet vets, works out awkwardly". Furthermore, its critic felt that the film was "a lovely, necessary little stitch in our torn time".   

In her review for The New York Times, Caryn James criticized the "cheap and easy touches ... that reduce it to the shallowness of a television movie", and found James Horners score, "offensive and distracting".    Newsweek magazines David Ansen wrote, "While one can respect its lofty intentions, the movie doesnt seem to have any better sense than its high-school heroine of just what its looking for. At once underdramatized and faintly stagy, it keeps promising revelations that never quite materialize".    In her review for the Washington Post, Rita Kempley wrote, "Whats meant to be a cohesive family portrait, a suffering American microcosm, is a shambles of threads dangling and characters adrift. Jewison leaves it to stymied viewers to figure out the gist of it".   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 