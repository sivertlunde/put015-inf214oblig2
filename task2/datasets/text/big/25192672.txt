Bomber (2009 film)
{{Infobox film
| name           = Bomber
| image          = Bomber film.jpg
| caption        = Theatrical release poster
| director       = Paul Cotter
| producer       = Maureen A. Ryan
| writer         = Paul Cotter
| starring       = Shane Taylor Benjamin Whitrow Eileen Nicholas
| music          = Stephen Coates
| cinematography = Rick Siegel
| editing        = Matt Maddox
| released       =  
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
}}
Bomber is a 2009 British comedy-drama film directed and written by Paul Cotter about an 83 year-old man returning to Germany for a long planned journey of atonement. 

==Plot==
Shane Taylor, Benjamin Whitrow, and Eileen Nicholas star in Paul Cotters bittersweet comedy about an old man who goes back to Germany to apologise to a village he accidentally bombed during the war.

Lovelorn art school graduate Ross is still down in the dumps when his eighty-three-year-old father announces plans for a family road trip to Germany. Back in the war, Rosss father accidentally bombed a small German town and hes regretted the mistake ever since. Hes determined to make amends but getting to Germany wont be easy, because its been years since father and son have exchanged a kind word. Along the way, father and son both learn some important lessons that will help them to be better, more compassionate people in the future.

==Awards and Reception== SXSW (South by Southwest) Film Festival in the Narrative Competition. The film subsequently appeared at film festivals across the world including Filmfest München, Torino, Raindance, Mill Valley and Gothenburg. Along the way, Bomber picked up the following awards:

* Winner, Best Director, Savannah Film Festival 
* Winner, Best Feature, Sonoma International Film Festival
* Winner, Best Feature, San Luis Obispo Film Festival
* Winner, Best Feature and Audience Award, Estes Park Film Festival
* Winner, Best Feature, 540 Film Festival
* Winner, Best UK Feature, Falstaff International Film Festival
* Winner, Best Actress, BendFilm
* Winner, Best Actress, Nashville Film Festival
* Winner, Best Actress, Kiev International Film Festival
* Honoree, New Directors Grand Jury Prize, Nashville Film Festival
* Runner-up, Best Feature, Anchorage International Film Festival
* Nominee, Best Micro-Budget Feature, Raindance

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 