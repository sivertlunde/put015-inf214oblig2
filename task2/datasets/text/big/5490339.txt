Waiting at the Royal
 
{{Infobox Film |
  name     = Waiting at the Royal|
  image          = |
  writer         = Glenda Hambly|
  starring       = Catherine McClements, Noni Hazlehurst, Josephine Byrnes |
  director       = Glenda Hambly |
  producer       = |
  distributor    = |
  released   = 2000|
  runtime        = |
  country = Australia |
  language = English |
  music          = |
  awards         = |
  budget         = |
}}

Waiting at the Royal is a 2000 Australian drama film starring Catherine McClements, Noni Hazlehurst, Josephine Byrnes and Jo Kennedy. It is about four women from very different backgrounds sharing a maternity ward.

==Characters==
* Dinny (played by Catherine McClements)
* Eloise (played by Noni Hazelhurst)
* Antoinette (played by Josephine Byrnes)
* Diana (played by Jo Kennedy)
* Saresh (played by Ramon Tikaram)
* Josie (played by Glynis Angell)
* Noel (played by Luke Elliot)

==External links==
* 
*  

==See also==
*List of Australian films

 
 
 
 
 


 
 