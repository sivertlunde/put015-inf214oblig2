The Year My Voice Broke
 
 
{{Infobox film
| name           = The Year My Voice Broke
| image          = Year my voice broke.jpg
| image size     =
| caption        = Video release artwork
| director       = John Duigan George Miller Doug Mitchell
| writer         = John Duigan
| narrator       =
| starring       = Noah Taylor Loene Carmen Ben Mendelsohn
| music          =
| cinematography =Geoff Burton	 	
| editing        = Neil Thumpston
| studio = Kennedy Miller
| distributor    = Avenue Pictures Productions
| released       =  
| runtime        = 103 min.
| country        = Australia
| language       = English
| budget         =
| gross = A$1,513,000 (Australia)
}} coming of age drama film written and directed by John Duigan and starring Noah Taylor, Loene Carmen, and Ben Mendelsohn.
 New South Wales, it was the first in a projected trilogy of films centred on the experiences of an awkward Australian boy, based on the childhood of writer/director John Duigan. Although the trilogy never came to fruition, it was followed by a sequel, Flirting (film)|Flirting. It was the recipient of the 1987 Australian Film Institute Award for Best Film.

==Plot== joyride and is arrested and sent to juvenile detention; it is while he is away that Freya reveals to Danny that she is pregnant. Danny offers to marry her and claim that the child is his, but Freya refuses, saying that she does not want to marry anyone. Meanwhile, intrigued by a locket left to Freya by an elderly friend of theirs who recently died -engraved "SEA"- Danny begins to investigate the towns past, and discovers a lone cross in the cemetery bearing those initials, belonging to a "Sara Elizabeth Amery," who died days after Freya was born. Through inquiries with his parents, Danny learns that Sara was something of the town prostitute years ago, and that she was Freyas biological mother, who died trying to give birth by herself at the abandoned house.

Meanwhile, Trevor breaks out of detention, steals another car, and severely wounds a store clerk during an armed robbery. Trevor returns to town long enough to reunite with Freya at the abandoned house, and learn that she is pregnant. The police arrive at Trevors hiding place, but Danny warns him, and Trevor is able to escape, but the police run his car off the road during the course of the pursuit, and Trevor dies the next day. Freya disappears, and later suffers a miscarriage and hypothermia until Danny finds her (at the abandoned house) and takes her to the hospital. Hesitantly, Danny reveals the identity of Freyas mother to her. Realising the stigma now hanging over her, Freya decides to leave on the night train for the city. At the station, Danny gives her his lifes savings to support herself and sees her off - promising their friendship to one another and to keep in touch. Later Danny travels to their favourite hangout spot and carves Freyas, Trevors, and his name into a rock, as his adult self informs the audience that he never saw Freya again.

The film is a series of interconnected segments narrated by Danny who recollects how he and Freya grew apart over the course of one year. 
Dannys history continues in the film Flirting (film)|Flirting (1991).

==Production==
John Duigan wrote a script based on his experiences going to a boarding school in the mid 60s called Flirting. He was unable to get the film funded so wore a prequel The Year My Voice Broke based on the leading character growing up in a country town. Duigan had worked with Kennedy Miller making the mini series Vietnam and they agreed to make the film as one of four tele movies they were making for the Channel Ten network. Duigan was allowed to make the film on 35mm. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p348-350 <ref name=
"duigan">Scott Murray, "John Duigan: Awakening the Dormant", Cinema Papers, November 1989 p31-35, 77 

===Location===
The film was shot, but not set, in Braidwood, New South Wales, Australia.

===Music===
Music credits: 
*"Apache (instrumental)|Apache" Performed by The Shadows Corinna Corinna" Performed by Ray Peterson
*"The Lark Ascending" Written by Ralph Vaughan Williams
*"Temptation (1933 song)|Temptation" Performed by The Everly Brothers Tower of Strength" and "A Hundred Pounds of Clay" Performed by Gene McDaniels
*"Diana (Paul Anka song)|Diana" Performed by Paul Anka
*"(The Man Who Shot) Liberty Valance" Performed by Gene Pitney
*"Get a little dirt on your hands" Performed by The Delltones I Remember You" Performed by Frank Ifield
*"Thats the Way Boys Are" Performed by Lesley Gore

The main theme used in the film is The Lark Ascending by English composer Ralph Vaughan Williams. At a 2005 special event screening in Sydney, director John Duigan stated that he chose the piece as he felt it complemented Dannys adolescent yearning.  All of the songs are to period, except "Thats the Way Boys Are" which was released in 1964.

==Release==
The movie was entered in the AFI Awards, despite protests that it was a tele movie. However it was allowed in because Duigan argued it was shot in 35mm and designed as a feature. The movie won five AFI Awards, which led to Hoyts picking it up and releasing it as a feature film. 

The Year My Voice Broke grossed $1,513,000 at the box office in Australia,  which is equivalent to $3,041,130 in 2009 dollars.  The US Box office was $213,901. 

==Awards== AFI Awards:    George Miller)
*Best Direction (John Duigan)
*Best Original Screenplay (John Duigan)
*Best Actor in a Supporting Role (Ben Mendelsohn)
*Members Prize
 AFI Awards:
*Best Actor in a lead role (Noah Taylor)
*Best Actress in a lead role (Loene Carmen)
*Best Achievement in Editing (Neil Thumpston)

In commenting about the film, the AFI website states: 

The most important awards   went to The Year My Voice Broke. It won Best Film for producer George Miller who had twice been named Best Director (for the two Mad Max films). John Duigan won the awards for direction and original screenplay. He had had two previous nominations for direction (Mouth To Mouth and Winter Of Our Dreams) and two for original screenplay (The Trespassers and Winter Of Our Dreams). Ben Mendelsohn was named Best Supporting Actor.

==DVD Release==
A 21st Anniversary Special Edition DVD was released in December 2008.

==References==
;Notes
 

:Bibliography
* 
* 
* 

==External links==
* 

 
 

 
 
 
 
 
 
 
 