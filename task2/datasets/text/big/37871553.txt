Henry: Portrait of a Serial Killer, Part II
{{Infobox film 
|name=Henry: Portrait of a Serial Killer, Part II
|image=
|caption=
|director=Chuck Parello
|producer=Waleed B. Ali Malik B. Ali Thomas J. Busch
|writer=Chuck Parello  Kate Walsh
|music=Robert F. McNaughton
|cinematography=Michael Kohnhorst
|editing=Tom Keefe
|studio=
|distributor=
|released=1996
|runtime= 84 minutes
|country=United States
|language=English
|budget=
|gross=
}} horror film written and directed by Chuck Parello. 

The film is the sequel to  .

==Plot==
Serial killer Henry continues his killing spree when he takes a job at a port-o-john company where he meets two employees, Kai and his wife Cricket. They take pity on Henry when they learn that he is a homeless drifter and offer him a room in their home. Also staying with them is Kai and Crickets emotionally fragile teenage niece, Louisa. Henry learns that Kai has a side job as an arsonist-for-hire, setting up phony insurance scams to make money for their boss, Rooter. Henry agrees to join Kai and on one of their first outings, they discover two squatters in a building thats been marked for fire. It is then that Henry introduces Kai to his lifes work of being a serial killer... and the murders begin. Kai has never killed before, but he turns into a willing accomplice. Initially, the two men work well together. But as the killing sprees increase in their depravity, its more than Kai can handle. He wants out, but hes in too deep with both his insurance scam with Rooter as well as being an murder accomplice with Henry.

In the meantime, Louisa develops a romantic crush on Henry, but he refuses to respond to her sexual advances. After Henry rejects Louisas advances, she becomes more dangerously disturbed and suicidal.

When Henry attends a gathering with Kai and Cricket at Rooters house, he refuses to take recreational drugs until Rooter slips Henry a roofie in his drink, Henry passes out and wakes up later after the other guests leave. Angry that Rooter drugged him, Henry murders Rooter on the spur of the moment as well as his friend, which is against Henrys "code" never to murder people that he knows personally. Kai helps Henry dispose of the two bodies with Cricket finally seeing the side work that Henry and Kai have been doing. When the three of them return at Kai and Crickets house, they find Louisa who kills herself in front of them. Again on the spur of the moment, Henry takes the gun that Louisa used to kill herself and shoots both Kai and Cricket. Cricket dies while Kai is critically wounded. Henry takes the bodies to the basement of the house, pours gasoline all over the place and sets fire to the house allowing Kai to burn to death. Henry gets into Kai and Crickets car and drives away into the night, alone once again to continue his killing spree as the house explodes and burns to the ground, again leaving no trace for the police to learn about Henrys existence.

==Cast==
* Neil Giuntoli as Henry
* Rich Komenich as Kai Kate Walsh as Cricket
* Carri Levinson as Louisa
* Daniel Allar as Rooter
* Penelope Milford as Henrys first victim

==References==
 

==External links==
* 
 
 
 
 
 
 
 