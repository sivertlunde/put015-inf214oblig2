Eternity (1943 film)
{{Infobox film
| name           = Eternity
| image          =
| image_size     =
| caption        =
| director       = Bu Wancang Ma-Xu Weibang Zhu Shilin
| producer       =
| writer         = Zhou Yibai
| narrator       =
| starring       = Chen Yunshang
| music          =
| cinematography =
| art direction  =
| editing        =
| distributor    =
| released       =  
| runtime        = 96 min.
| country        = China (occupied) Empire of Japan Mandarin Chinese
| budget         =
}}	
 1943 Cinema Chinese film made in Japanese-occupied Shanghai during the Second World War. The film was a collaborative effort between the Japanese-controlled Manchukuo Film Association and Chinese filmmakers that remained in Shanghai under the Japanese-controlled Zhonglian Productions ("United China") brand.

Telling the story of Lin Zexu and the First Opium War, the film was Japans attempt to make a crowd-pleasing anti-Western film as propaganda in Japanese-occupied areas of China. Ultimately the film (and the Shanghai filmmakers) were seen as tools of the enemy once the war was over, with many involved in the production (notably directors Bu Wancang, Ma-Xu Weibang, and Zhu Shilin eventually moving to Hong Kong due to the hostile environment.

Today the film is still seen as Japanese propaganda, or more generally as anti-foreign in general.

== Cast ==
* Chen Yunshang as Zhang Jingxian
* Li Xianglan (Japanese name Yoshiko Ōtaka)
* Gao Zhanfei as Lin Zexu
* Yuan Meiyun
* Wang Yin

== Production history ==
In 1939, the Japanese had formed the China Movie Company ("Zhongdian") to make Japanese propaganda shorts. By 1941, Zhongdian signed a deal with the head of the Xinhua Film Company, Zhang Shankun, followed quickly by two other deals with the Yihua Film Company and the Guohua Film Company. 

=== Casting ===
The film was cast primarily with Chinese actors out of (what remained) of the Shanghai studio system (now under the control of Zhonglian). One major star cast, however, was the Manchuria-born Japanese actress Yoshiko Ōtaka. Though she had already starred in several Chinese features under her Chinese name of Li Xianglan, Ōtaka was a seeming outlier in the cast of Eternity. She was, therefore, an indication of the control the Japanese exercised over the Chinese film industry in Shanghai. (ref "Her Traces are Found Everywhere" 227, Shelly Stevenson, in Cinema and Urban Culture in Shanghai 1922-1943) Helped by the massive pop hits, "Candy-Peddling Song" (賣糖歌) and "Quitting (opium) Song" (戒煙歌), the film would catapult Li into stardom, as her earlier works had been in films so blatantly pro-Japanese, as to turn off most of the Chinese audience (Shelly, 227).

== Notes ==
 

==References==
* Fu, Poshek "Resistance in Collaboration: Chinese Cinema in Occupied Shanghai" in  . David, Barrett P. ed.  (Stanford:  Stanford University Press, 2001)

== External links ==
*  
*   from the Chinese Movie Database

 
 
 
 