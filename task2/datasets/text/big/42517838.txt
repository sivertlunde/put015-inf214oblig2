The Palm Beach Girl
{{infobox film
| title          = The Palm Beach Girl
| image          =
| imagesize      =
| caption        =
| director       = Erle C. Kenton
| producer       = Adolph Zukor Jesse Lasky
| writer         =  
| starring       = Bebe Daniels
| music          =
| cinematography = Lee Garmes
| editing        =
| distributor    = Paramount Pictures
| released       = May 17, 1926
| runtime        = 70 minutes; 7 reels
| country        = USA
| language       = Silent film..(English intertitles)
}} lost   1926 silent film romantic comedy starring Bebe Daniels and directed by Erle C. Kenton.  

==Cast==
*Bebe Daniels - Emily Bennett
*Lawrence Gray - Jack Trotter
*Marguerite Clayton - Julia
*Josephine Drake - Aunt Jerry
*John Patrick - Herbert Moxon (as John G. Patrick)
*Armand Cortes - Tug Wilson
*Royal Byron - Sheriff (as Roy Byron)
*Maude Turner Gordon - Aunt Beatrice

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 