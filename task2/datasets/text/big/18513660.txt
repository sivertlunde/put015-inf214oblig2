Egon Schiele – Exzess und Bestrafung
{{Infobox film
| name           = Egon Schiele – Exzess und Bestrafung
| image          = 
| director       = Herbert Vesely Robert Russ  Dieter Geissler
| screenplay     =  Herbert Vesely
| narrator       = 
| starring       = Mathieu Carriere Jane Birkin Christine Kaufmann  
| music          = Brian Eno Felix Mendelssohn
| cinematography = 
| editing        = 
| distributor    = Cinevox
| released       =  
| runtime        = 123 minutes
| country        = West Germany, France, Austria
| language       = German
| gross          = 
}} Austrian artist Egon Schiele. It stars Mathieu Carriere as Schiele with Jane Birkin as his artist muse Walburga (Wally) Neuzil and Christine Kaufmann as his wife Edith and Kristina van Eyck as her sister. The film is essentially a depiction of obsession and its constituents of sex, alcohol and uncontrolled emotions. Set in Austria during the Great War, Schiele is depicted as the agent of social change leading to destruction of those he loves and ultimately of himself.
 Best Foreign Language Film at the 53rd Academy Awards, but was not accepted as a nominee. 

==Plot==
The final years of the short life of Egon Schiele, the Austrian expressionist painter are chronicled against the backdrop of the final years of the Habsburg rule. The story begins around 1912 as Schiele (Mathieu Carriere) and his mistress and artistic muse Wally (Jane Birkin) are befriended by an obsessed teenage girl (Karina Fallenstein) who has run away to be with Schiele. Subsequently Schiele is imprisoned on the grounds that he has behaved in a sexually improper way towards the young woman. The young woman falsely accuses Schiele and although he denies the charge he is imprisoned. The girl withdraws her accusations but Schiele is requested to leave the area as he has offended the social mores of the conservative society in which he was living. Those offended include his mother (Angelika Hauff) who rails against his lax morals.

On his release his excesses continue although he fights (literally) to conform even volunteering to serve in the Austrian army in World War I. As a soldier Schiele cuts a pathetic figure and he is quickly removed as unsuitable.  He disposes of his alcoholic mistress and has an affair with a society beauty who ultimately abandons him as she can not cope with his sexual obsessions. Schieles emotional cruelty is exposed as he shuns Wally as she nears death.Their parting scene at a Vienna social gathering reveals a corruption and cruelty that is at the heart of Schieles artistic soul. 

Schieles paintings however develop greater depths as he pushes his body and emotions to their limit. Whilst his paintings are gaining acceptance (and many now hang in the Leopold Museum in Vienna ) his own sanity suffers. He marries and appears to find a modicum of contentment until his wife Edith (Christine Kaufmann) dies in the 1918 Spanish Influenza pandemic. Schiele makes love to his dying wife in a scene that is tender yet shocking, evoking a central theme of Schieles work:  a link between sex and death. Shortly thereafter Schiele himself contracts influenza and dies. 

The locations for the film were Vienna and in the Croatian capital Zagreb. 

==Cast==
*Mathieu Carriere – Egon Schiele
*Jane Birkin – Wally Neuzil
*Christine Kaufmann – Edith Harms
*  –  Adele Harms
*  – Tatjana von Mossig
*Ramona Leiss –   Gerti
*Marcel Ophüls – Dr Stovel
*Robert Dietl –  Benesch
*  Danny Mann  – Mrs Stovel
*Guido Weiland – Herr von Mossig
*Maria Ebner – Frau Harms
*Angelika Hauff – Frau Schiele, Schieles mother
*Helmut Dohle – Gustav Klimt
*Wolfgang Leisowsky – Arthur Roessler
*Serge Gainsbourg – Unnamed

==Reviews==
* 

==See also==
* List of submissions to the 53rd Academy Awards for Best Foreign Language Film
* List of Austrian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 