Phish: Live in Vegas
 
 
{{Infobox film
| name           = Phish: Live in Vegas
| image          = PhishVegasVid.jpg
| image_size     =
| caption        =
| director       =
| producer       =
| writer         =
| narrator       =
| starring       = Phish
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 172 mins
| country        =
| language       =
| budget         =
| gross          =
}}
Phish: Live in Vegas (2000) is a video of a complete live performance by the rock band Phish recorded on September 30, 2000, bandleader Trey Anastasios 36th birthday. It includes many rarities and songs that were brought out of "retirement" after long periods of inactivity.

The performance was originally a live Internet webcast that came just days after Phish officially announced an indefinite break from recording and touring. Anastasio used this opportunity to officially announce the break to audiences across the globe via the Internet.

Also included are tracks from the following nights performance in Phoenix, Arizona. 

This was the first uncut concert DVD released by Phish, the second being  , released on July 11, 2006.

==Track listing==
* Bonus footage was recorded in Phoenix on October 1, 2000 and appears in the Extras section of the DVD.

===Set one===
#"Walfredo"  (Trey Anastasio|Anastasio, Jon Fishman|Fishman, Mike Gordon|Gordon, Page McConnell|McConnell)  - 7:14
#"The Curtain With"  (Anastasio, Marc Daubert|Daubert)  - 13:06
#"Maze"  (Anastasio, Tom Marshall (singer)|Marshall)  - 11:05
#"Roggae"  (Anastasio, Fishman, Gordon, Marshall, McConnell)  - 8:24
#"I Didnt Know"  (Wright)  - 4:16
#"Mikes Song"  (Gordon)  - 8:46
#"Simple"  (Gordon)  - 6:25
#"Saw It Again"  (Anastasio, Marshall)  - 6:27
#"Esther"  (Anastasio)  - 9:08
#"Weekapaug Groove"  (Anastasio, Fishman, Gordon, McConnell)  - 11:20

===Set two===
#"Timber"  (Josh White|White)  - 8:31
#"AC/DC Bag"  (Anastasio)  - 9:25
#"Colonel Forbins Ascent"  (Anastasio)  - 5:39
#"Fly Famous Mockingbird"  (Anastasio)  - 15:35
#"Twist (Phish song)|Twist"  (Anastasio, Marshall)  - 10:36
#"Sand"  (Anastasio, Russ Lawton|Lawton, Tony Markellis|Markellis, Marshall)  - 12:37
#"A Day in the Life"  (John Lennon|Lennon, Paul McCartney|McCartney)  - 7:14

===Encore=== Emotional Rescue"  (Mick Jagger|Jagger, Keith Richards|Richards)  - 14:43

===Bonus footage===
#"Piper"  (Anastasio, Marshall)  - 22:13 *
#"Camel Walk"  (Jeff Holdsworth|Holdsworth)  - 5:38 *

==Personnel==
Phish
:Trey Anastasio - guitars, vocals (piano on "Walfredo", drums on "I Didnt Know", keyboard on "Sand")
:Page McConnell - keyboards, vocals (drums on "Walfredo")
:Mike Gordon - bass guitar, vocals (guitar on "Walfredo")
:Jon Fishman - drums, vocals (bass guitar on "Walfredo", vacuum on "I Didnt Know")

== External links ==
*  

 

 
 
 
 
 