Remains to Be Seen (film)
{{Infobox film
| name           = Remains to Be Seen
| image_size     =
| image	         = 
| caption        =
| director       = Don Weis
| producer       = 
| writer         = Sidney Sheldon
| based on = 
| starring       = June Allyson Van Johnson Louis Calhern Angela Lansbury Dorothy Dandridge
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       = 1953
| runtime        = 
| country        = United States English
| budget         = $976,000  . 
| gross          = $939,000 
| preceded_by    =
| followed_by    =
}} crime musical musical comedy film directed by Don Weis.

==Plot==

A girl vocalist and her apartment manager get mixed up in a creepy Park Avenue murder and find themselves facing danger at every turn.

==Cast==
* June Allyson: Jody Revere
* Van Johnson: Waldo Williams
* Louis Calhern: Benjamin Goodman
* Angela Lansbury: Valeska Chauvel John Beal: Dr. Glenson 
* Dorothy Dandridge: Herself
* Barry Kelley: Lt. OFlair

==Reception==
According to MGM records the movie earned $697,000 in the US and Canada and $242,000 elsewhere, making a loss to the studio of $438,000. 

==Soundtracks==
*Toot, Toot, Tootsie (Goo Bye!) - Sung and Danced by June Allyson and Van Johnson 
*Taking a Chance on Love - Sung by Dorothy Dandridge 
*Too Marvelous for Words - Sung by Van Johnson, then sung by June Allyson, then reprised by Van Johnson and June Allyson

==References==
 

==External links==
*  at TCMDB
* 

 
 
 
 
 
 


 