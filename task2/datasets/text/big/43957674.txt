Avatharam (1981 film)
{{Infobox film 
| name           = Avatharam
| image          =
| caption        =
| director       = P Chandrakumar
| producer       = RS Prabhu
| writer         = Velliman Vijayan
| screenplay     =
| starring       = Kaviyoor Ponnamma Sankaradi Sathaar Sukumaran
| music          = A. T. Ummer
| cinematography = Anandakkuttan
| editing        = G Venkittaraman
| studio         = Sree Rajesh Films
| distributor    = Sree Rajesh Films
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by P Chandrakumar and produced by RS Prabhu. The film stars Kaviyoor Ponnamma, Sankaradi, Sathaar and Sukumaran in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
 
*Kaviyoor Ponnamma
*Sankaradi
*Sathaar
*Sukumaran GK Pillai Kunchan
*Mala Aravindan
*Paravoor Bharathan
*Santhakumari Seema
*Vincent Vincent
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Sathyan Anthikkad. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chingappennin || S Janaki, Chorus || Sathyan Anthikkad || 
|-
| 2 || Moham Moham Moham Chiraku Vidarthi || K. J. Yesudas || Sathyan Anthikkad || 
|-
| 3 || Nilaavinte Chumbanamettu || K. J. Yesudas || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 

 