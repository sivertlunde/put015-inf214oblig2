Love and Honor (2013 film)
 
{{Infobox film
| name           = Love and Honor
| image          = Love and Honor poster.jpg
| alt            = 
| caption        = Theaterical release poster
| director       = Danny Mooney
| producer       = Chip Diggins Patrick Olson Eddie Rubin Peter Pastorelli Jim Burnstein Garrett Schiff
| screenplay     = Jim Burnstein Garrett K. Schiff
| based on       = 
| starring       = Liam Hemsworth Aimee Teegarden Teresa Palmer Austin Stowell Chris Lowell
| music          = Alex Heffes
| cinematography = Theo van de Sande
| editing        = Glenn Garland
| studio         =  Lighting Entertainment IFC Films
| released       =   (Limited release) 
| runtime        = 
| country        = United States
| language       = English
| budget         =
| gross          = $16,769   
}}
 Ann Arbor and surrounding areas.  The story follows a soldier who, after being dumped by his girlfriend, decides to return home secretly from war with his best friend to win her back.

==Plot==
In 1969, at the time of the Apollo 11 mission, U.S. soldier Dalton Joiner, fighting in the Vietnam War, uses his time of R&R (military)|R&R supposed to be spent in Hong Kong, to fly back to the U.S. to re-capture the heart of his girlfriend Jane. Fellow soldier Mickey Wright accompanies him. Jane now calls herself Juniper, and is member of a group of anti-war activists. Joiner and Wright pretend they are AWOL, and are admired by the group for that, until it is revealed that they plan to return in time. Jane breaks up (again) with Joiner, which makes him decide to flee to Canada. Wright falls in love with Candace, but returns to Vietnam.

==Cast==
*Liam Hemsworth as Mickey Wright
*Aimee Teegarden as Juniper/Jane
*Teresa Palmer as Candace
*Austin Stowell as Dalton Joiner
*Chris Lowell as Peter Goose Max Adler as Burns
*Wyatt Russell as Topher Lincoln
*Delvon Roe as Isaac

==Production==
The film was shot in and around Ann Arbor, Michigan from July 11 to August 12, 2011.  A scene was also filmed in Ypsilanti, Michigan. 

The international sales rights were acquired by Lighting Entertainment in May 2012.    Lighting Entertainment intended to premiere the film for international buyers at the Cannes Film Market held in May 2012.  

==See also==
*"Jennifer Juniper"

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 