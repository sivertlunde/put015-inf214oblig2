Law and Order (1940 film)
{{Infobox film
| name           = Law and Order
| image          =
| image_size     =
| caption        = Ray Taylor
| producer       = Joseph Gershenson
| writer         = W. R. Burnett (novel) Sherman L. Lowe (screenplay) Victor McLeod (screenplay)
| narrator       =
| starring       = See below
| music          = 
| cinematography = Jerome Ash
| editing        = Edward Curtiss
| distributor    =
| released       = November 28, 1940
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Ray Taylor.

==Plot summary==
At the beginning of the film, Bill Ralston arrests a bad cowboy named Deadwood and takes his to Judge Williams. Ralston returns to his girl, Sally Dixon, before she is shot in the chest 8 times by Brant, a goon of Deadwoods, and Poe Daggett, an old enemy of Ralston. Ralston shoots Daggett in the knee, and the two run away into the saloon. He sees all of his friends including Sheriff Fin Elder at the Saloon, and they sing him a song. Then Daggett comes behind the Sheriff and tries to kill him, but Poes brother, Walt, stops him from doing it. Everyone gets into a big fight, and in the end, the only survivors are Bill Ralston and Sheriff Fin Elder.

==Cast==
*Johnny Mack Brown as Bill Ralston
*Fuzzy Knight as Deadwood
*Nell ODay as Sally Dixon James Craig as Brant
*Harry Cording as Poe Daggett
*Earle Hodgins as Sheriff Fin Elder Robert Fiske as Ed Deal
*Jimmie Dodd as Jimmy Dixon William Worthington as Judge Williams Ted Adams as Walt Daggett
*Ethan Laidlaw as Kurt Daggett
*George Plues as Stage Driver
*Harry Humphrey as Cal Dixon

==Soundtrack==

 

==External links==
* 

 
 
 
 
 
 
 


 