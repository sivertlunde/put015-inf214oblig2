Demonic Toys 2
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Demonic Toys 2: Personal Demons
| image          = Demonic Toys 2.jpg
| alt            =  
| caption        =  William Butler
| producer       = Charles Band
| writer         = William Butler
| starring       = Selene Luna Elizabeth Bell Leslie Jordan
| music          = Richard Band Kenny Meriedeth
| cinematography = Thomas L. Callaway
| editing        = Danny Draven
| studio         = 
| distributor    = Full Moon Features
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} William Butler and produced by Charles Band.  It is a slasher film and is a sequel to Demonic Toys, and Hideous!. 

==Plot==
 
Appeared to take place right after the events of Demonic Toys, an unknown stranger with a pair of gloved hands picks up the pieces of the destroyed toys and starts stitching them together. The only toys the perpetrator could fix correctly were Baby oopsie daisy and Jack Attack. The unidentified man then puts the toys in a crate, and is handed over a suitcase full of cash by another man, who then leaves with the toys. The film then switches to Italy, where college graduate student Caitlin  and Mr. Butterfield, of the Antiquates Foundation (who works in carbon dating antique toys), wait outside of a castle for someone to arrive. The someone finally arrives, and is revealed to be Dr. Emilio Lorca, who survived his attack at the end of Hideous!. Also with Dr. Lorca is his sweetheart Lauraline  and her stepson David, and a little woman named Lillith, who is a psychic of some sorts. Dr. Lorcas driver, Eric accidentally drops a crate that Dr. Lorca wants brought into the house, revealing the Demonic Toys inside it. It is revealed that Dr. Lorca is still collecting oddity toys. Hes arrived because Caitlin called and told him about an oddity toy she found within the castle thats able to move. The castles current owners seldom come there, meaning theyre superstitious of everything thats happened in there. The owners decided to empty and sell it to Italian government to make it a historical landmark and keep it open for the public. Caitlin takes them inside the castle and gives them its history.

The castle was built in the 13th century, believed to be built over the temple of Jupiter Optimus Maximus by the local villagers. It was once ruled by Fiora Borisoff, a Bulgarian empress who used to practice black magic and fled her own country when her people revolted her. Since then, the castle was home to Italian royalty, and a few murderous madmen. The paintings are believed to have demons hidden in the background. If any one of them does so, then they represent one of Fioras personal demons. Caitlin then takes them into the dining room, with a box on the table. The toy Caitlin found was buried in the dungeon inside a large stone container that was like an ornamental mosaic tile. The case container was made out of hand compound steel with some magnetic and fixtures. The doll itself was hand carved out of wood with a mixture of fabric elements. Caitlin opens the box and shows them the doll Divoletto. Mr. Butterfield examines the toy and claims it is the oldest toy hes ever seen, made possibly in the 14th century. Caitlin then shows them how it moves. Just tap a wand on the side of the box a couple of times and then it will come to life. After a while, the toy finally moves. Caitlin believes that there are magnets in the wand and when the box is tapped, it sets off the springs and mechanisms inside of Divoletto. However, Lillith thinks differently. Eric suddenly runs in the room and tells them that their cars are gone. Since everything is closed and have no transportation to get back to Rome, Caitlin suggests that they stay at the castle for the night. Meanwhile, Lillith examines Divoletto to catch a vision of some sorts, and sees a vision of the future where Divolettos killing them all.
 clay vessels in the room. Mr. Butterfield then finds a flight of stairs that leads into the dungeon. David and Caitlin goes down the stairs while Mr. Butterfield decides to go to bed.

Meanwhile, Dr. Lorca returns to his room and begins to make love to Lauraline, who in a disgusted manner, plays along. Meanwhile, Lillith goes to see another vision out of Divoletto, not knowing that shes holding the replica. After seeing the vision of the replica being made, the castle starts to shake like an earthquake. After this, the picture of Fiora Borisoff starts to move and one of Fioras personal demons drives into Lillith and she falls unconscious on the table. Meanwhile, David and Caitlin get attacked by bats in the dungeon and realize that theyre in a natural cavern. They then find a water well|well-like structure with the words "Prodigious Abyssus" carved onto it, which Caitlin claims is a portal to hell. They then find a book called "Prodigium Exorcisio", which Caitlin claims is an exorcist journal. Through the journal, Caitlin finds out that Fioras doctors were believed she was possessed by demons, so they brought her down to the cavern to perform exorcisms on her. The exorcist wouldve performed rituals on her to rid her from her personal demons. One of them turns out to be the portrait of Fiora herself, including the clay vessels. Her personal demons consist of jealousy, hatred, humility and revenge. Meanwhile, Mr. Butterfield finds some valuable items in his room and stuffs them into his clothes. As he tries to sleep, the toys walk into his room and Baby Whoopsie and Divoletto climb on the wall where two swords are hanging. Jack Attack forces Mr. Butterfield to jump out of bed and the swords come down on him, slicing his head off. Meanwhile, David, Caitlin and Dr. Lorca meet in the living room at midnight for the seance, and Lillith brings forth the spirit of Fiora and becomes possessed.

The spirit tells them to leave the castle in peace, but take nothing or there will be consequences. Meawnhile, Lauraline sneaks off to find Eric. She goes into the dining room and finds the doll missing from its box, but also finds some jewels and diamonds in the side table drawers. She then cranks Jack Attacks box up, popping him out and he bites her neck. Meanwhile, the spirit of Fiora disappears and Lillith returns to normal, and tells them that the Divoletto doll they have right now is a replica. Baby Whoopsie appears and stabs David in the arm. Dr. Lorca pulls out a small gun hidden in his coat and shoots at Baby Whoopsie. Dr. Lorca then continues to threatens to kill Caitlin if Divoletto is really a replica. David then punches Dr. Lorca in the face and steals his gun. Lillith, David and Caitlin runoff to escape and they run into Lauraline, who survived her attack by Jack Attack. Lauraline grabs the gun off of David and points it at Caitlin, since shes the one who brought them into the castle. David and Lauraline fight over the gun and Lauraline screams shes going to kill him the same way she killed Davids father. The gun then goes off and hits Lauraline in the chest, killing her. Dr. Lorca runs in on them and scares them away, and mourns over Lauralines body. The trio run into the physicians room and head down the staircase. Dr. Lorca grabs a hatchet lying in the hallway and runs into the room, when suddenly Baby Whoopsie jumps on his back and stabs him in the skull, fulfilling Lilliths vision. Meanwhile, David, Lillith and Caitlin run to the Prodigium Abyssus, where Divoletto jumps on Lillith, and the spirit of Fiora possesses her and throws him off her.

David then smashes Divolettos head with a shovel, but its revealed that Divoletto was one of Fioras personal demons. The demon then sucks Fioras spirit out of Lillith and brings her through the portal back to hell. The demonic toys then attack them and David cuts their heads off with the shovel, killing them. The next day, David, Caitlin and a spooked out Lillith leave the castle, with the demonic toys remains so no one else can find them, along with the clay vessels containing Fioras personal demons. The sound of glass shatter is heard, and the painting of Fiora at the castle starts whispering, implying that Fioras revenge personal demon has been released.

==Cast==
* Alli Kinzel as Caitlin
* Lane Compton as David
* Selene Luna as Lilith
* Michael Citriniti as Dr. Lorca
* Elizabeth Bell as Lauraline
* Billy Marquart as Eric
* Leslie Jordan as Butterfield
* Gage Hubbard as Personal Demon Baby Whoopsie

==Soundtrack==
The score was composed by American film composer Richard Band. 

==Release==
The film was released on DVD in January 2010. 

==Featured Toys== Baby Oopsy Daisy Jack Attack Divoletto

==See also==
* List of toys in the Demonic Toys films

==References==
 

* The painting of Fiora also appears in Full Moon movie Stuart Gordons Castle Freak

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 