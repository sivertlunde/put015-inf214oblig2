Cheeni Kum
 
 
{{Infobox film
| name           = Cheeni Kum
| image          = Wall800CheeniKum.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = R. Balki
| producer       = Sunil Manchanda
| writer         = R. Balki   Manoj Tapadia
| narrator       = Tabu Paresh Rawal
| music          = Ilaiyaraja
| cinematography = P. C. Sreeram
| editing        = Chandan Arora
| studio         = MAD Entertainment Ltd.
| distributor    = MAD Entertainment Ltd.
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          =  
}}
Cheeni Kum (English: Less Sugar) is a 2007 Bollywood romance film directed by R. Balki, starring Amitabh Bachchan, Tabu (actress)|Tabu, Paresh Rawal, Zohra Sehgal and Swini Khara.

==Plot==
Cheeni Kum focuses on Buddhadev Gupta (Amitabh Bachchan). Buddhadev is the 64-year-old chef and owner of Londons top Indian restaurant, Spice 6. He lives with his 85-year-old mother (Zohra Sehgal) and his only friend and confidante is his 9-year-old neighbour, Sexy (Swini Khara) who is diagnosed with cancer and been to the hospital seven times. Buddhadev Gupta is an arrogant, ego-centric, pompous man with a singular passion in life — cooking. He is a confirmed bachelor who has never been in love until 34-year-old Nina Verma (Tabu (actress)|Tabu) walks into his restaurant and his life. Nina is a beautiful and charming Indian woman. Cool, calm, quiet, always smiling but independent and strong willed. The two extreme in age, character and attitude, meet and against all odds, fall in love. They decide to get married and like any Indian man, Buddhadev respectfully comes to ask Ninas father, Omprakash Verma (Paresh Rawal), who is a true Gandhian living in Delhi, for her hand. The main problem here is the Buddhadev is older than Ninas father. Omprakash is horrified when Buddhadev asks his daughters hand and intentionally refuses, he attempts to commit suicide by starving himself to death. Buddha explained to him why he loved Nina and what kind of father is he keeping Nina single for 34 years. He left them with his mother and is horrified when Sexys father (Vinay Jain) calls him and said that Sexy just died. Omprakash finally realises his mistake and let Nina go after Buddhadev and with his in-laws family, he let them go to London to his restaurant. He bonds with Omprakash when he tells him he got tickets to the cricket match.

==Cast==
*Amitabh Bachchan as Buddhadev Gupta Tabu as Nina Verma
*Paresh Rawal as Omprakash Verma
*Zohra Sehgal as Buddhadevs mother
*Swini Khara as Sexy
*Alexx ONell as the English waiter

==Soundtrack==
{{Infobox album 
| Name        = Cheeni Kum
| Type        = Soundtrack
| Artist      = Ilayaraja
| Cover       =
| Released    = May 2007
| Recorded    = 2006–2007 Feature film soundtrack
| Length      =  Eros Music
| Producer    = Ilayaraja
}}
The score and soundtrack were composed by Maestro Ilaiyaraaja. The songs had their tunes re-used from the composers earlier songs in other languages, while the arrangements were fresh.

{| class="wikitable"
|-
! Track No !! Song !! Singer !! Adapted From
|- Mouna Raagam
|-
| 2 || Baatein Hawa Hai || Amitabh Bachchan & Shreya Ghoshal || Adapted tune from "Kuzhaloodhum kannanukku" from the Tamil film Mella Thirandhathu Kadhavu
|-
| 3 || Jaane Do Na || Shreya Ghoshal || Adapted from "Jotheyali Jothe Jotheyali" from Kannada film, Geetha (film)|Geetha. A later version of the same is "Vizhiyile Mani Vizhiyil" from Tamil film, Nooravathu Naal
|- Mouna Raagam
|-
| 5 || Baatein Hawa Hai || Shreya Ghoshal || Adapted tune from "Kuzhaloodhum Kannanukku" from the Tamil film Mella Thirandhathu Kadhavu
|-
| 6 || Theme Melody || Instrumental || 
|-
| 7 || Melody Saxophone ||  || 
|}

==Reception==

===Critical reception===
Cheeni Kum received positive reviews. Anurag Anand of Wogma gave the movie 4/5 stars, saying that "All in all, Cheeni Kum is a movie that should certainly be watched. The movie has one or two small sequences (though comical), which may warrant parental discretion, so decide for yourselves if you would like to take your kids along."  Martin DSouza of Glamsham gave the movie 4/5 stars, concluding that "Theres a dash of sweetness (Tabu), the right amount of spice (Amitabh), a proper dose of lime (Paresh Rawal), perfect quantity of salt (sexy) and tadka (provided by Zohra Sehgal). A clean entertainer which can be watched by the entire family."  Raja Sen of Rediff gave the movie 3.5/5 stars, stating that "This isnt a groundbreaking film, but it didnt set out to be. Its a maturely written film with great characters, tremendous performances and some fantastic moments. It could have been perfect, but the lesser said about that end the better. Watch it. A brilliant sequence involving the chef, a chemist, chhatris and chachas is absolute movie magic, and in itself well worth the price of admission. Bravo."  Naresh Kumar Deoshi of Apun Ka Choice gave the movie 3/5 stars, saying that "The movie, featuring Amitabh Bachchan in yet another brilliant performance, not just entertains you with its sarcastic humour, it also touches your heart with its emotional moments. Do not miss this sugarfree spread."  Taran Adarsh of Bollywood Hungama gave the movie 3/5 stars, concluding that "On the whole, CHEENI KUM is absorbing in parts. A lackluster first half gets a boost with a much energetic second half and that elevates the film to the watchable level. At the box-office, CHEENI KUM is targeted at the multiplexes mainly. Clever promos and feel-good vibes should ensure a positive run at the multiplexes."  On the contrary, Rajeev Masand of CNN-IBN gave the movie 2/5 stars, stating that "So Ill go with two out of five for R Balkis Cheeni Kum, its an average entertainer at best. If youre a die-hard Bachchan fan, do give it a shot because he doesnt disappoint. How you wish the film didnt either!" 

===Box office===
Cheeni Kum was a semi hit at the box office, grossing  , despite taking a poor opening. The movie collected   nett in its lifetime. 

==References==
 

==External links==
* 

 
 
 
 
 
 