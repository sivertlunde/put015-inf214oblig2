Winged Creatures (film)
 
{{Infobox film name = Winged Creatures image = Winged creatures ver2.jpg caption = Theatrical release poster director = Rowan Woods writer = Roy Freirich based on =   starring = Kate Beckinsale Dakota Fanning Guy Pearce Forest Whitaker Jennifer Hudson Jackie Earle Haley Josh Hutcherson producer = Robert Salerno music = Marcelo Zarvos editing = Meg Reticker cinematography = Eric Alan Edwards studio = Peace Arch Entertainment distributor = Icon Entertainment released =   country = United States runtime = 100 minutes language = English
|gross=$39,171 (Foreign)  	
}} Winged Creatures.  The film stars Kate Beckinsale, Dakota Fanning, Josh Hutcherson, Guy Pearce, Forest Whitaker, Jennifer Hudson, Jackie Earle Haley, Jeanne Tripplehorn and Embeth Davidtz. It was released on DVD by Peace Arch Entertainment in the United States on August 4, 2009, as Fragments. 

==Plot==
While in a restaurant, Carla Davenport, the restaurant cashier; Charlie Archenault, a driving-school teacher; Bruce Laraby, an emergency room physician; Annie Hagen, her father, and her best friend, Jimmy Jasperson suddenly hear gunshots. Annie, her father, and Jimmy retreat under a table as a suicidal gunman shoots several people (including Annies father) and then himself. The film shows the aftermath as these five traumatized people struggle to regain their trust in the ordinary world. 

==Cast==
* Dakota Fanning as Annie Hagen
* Josh Hutcherson as Jimmy Jasperson
* Forest Whitaker as Charlie Archenault
* Guy Pearce as Dr. Bruce Laraby
* Kate Beckinsale as Carla Davenport
* Jeanne Tripplehorn as Doris Hagen
* Jennifer Hudson as Kathy Archenault
* Jackie Earle Haley as Bob Jasperson
* Walton Goggins as Zack
* Embeth Davidtz as Jo Laraby
* Troy Garity as Ron Alber
* Robin Weigert as Lydia Jasperson
* Andrew Fiscella as Numbers Man
* Jaimz Woolvett as the Swedish Cook
* Hayley McFarland as Lori Carline
* Mae McGrath as the swag bystander
* Nick Farrell as swag bystanders wife and son

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 