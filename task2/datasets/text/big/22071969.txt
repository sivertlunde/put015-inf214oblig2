The Human Factor (1979 film)
 
{{Infobox film
| name           = The Human Factor
| image          = Humanfactorposter.jpg
| caption        = Theatrical release poster by Saul Bass
| director       = Otto Preminger
| writer         = Graham Greene   Tom Stoppard from the novel The Human Factor by Graham Greene
| starring       = Richard Attenborough   Derek Jacobi   John Gielgud   Nicol Williamson
| music          = Richard Logan   Gary Logan
| cinematography = Mike Molloy
| editing        = Richard Trevor
| distributor    = Metro Goldwyn Mayer United Artists
| released       = 1979
| runtime        = 115 mins
| country        = United Kingdom
| language       = English
| gross          =  $376,050 
}}
 British thriller novel of the same name by Graham Greene, with the screenplay written by Tom Stoppard.   It examined British espionage, and the Wests relationship with apartheid South Africa.

The film was directed by Otto Preminger, the 38th and final film he helmed in his nearly half-century career.

==Plot==
Maurice Castle (Nicol Williamson) is a mid-level bureaucrat in MI6 whose life seems completely without peculiarity, peccadillo, or any highlighting quality to suggest he’s anything but a dull bureaucrat, except for the interesting, casually introduced detail that he has an African wife, Sarah (Iman (model)|Iman), and son, Sam (Gary Forbes). Meanwhile, the company regime, represented by corpulent, bluffly cheery Dr. Percival (Robert Morley), who’s actually an expert in assassinations and biological toxins, and grey eminence Sir John Hargreaves (Richard Vernon), advise newly appointed security chieftain Daintry (Richard Attenborough) that, thanks to a source they have cultivated in their Moscow enemy headquarters, they believe they have a traitor at the MI6 African desk. The duo determine that the mole must be quietly killed, rather than be allowed publicity in a trial or a flight to Moscow. They determine quickly that the most likely candidate for the traitor is Arthur Davis (Derek Jacobi), Castle’s playboy office partner.  Actually, Castle is the mole (espionage)|mole, but the information he leaks is entirely unimportant financial documents. He became involved in leaking to the Soviets when he was an MI6 agent in South Africa, seven years earlier: he met and fell in love with Sarah, and when their affair was discovered by the authorities, Castle was all but thrown out of the country, and he entrusted Sarah’s smuggling out of the country to a mutual communist acquaintance. Ever since, he’s been repaying the favor by filtering insignificant data to the Soviets. Castle makes one last informational drop to his communist handlers and he is summarily whisked off to Moscow for protection. However, Castles primary problem is that he is not a communist, is not a communist sympathizer, and has absolutely no interest in politics, socialism, the Russian language, Slavic history or culture, geopolitical power plays, Moscow nor the Soviet Union. His only interest is in his wife and his son, who are left in London — where they will remain separated from him.  

==Cast==
 
* Richard Attenborough - Colonel Daintry
* John Gielgud - Brigadier Tomlinson
* Derek Jacobi - Arthur Davis
* Robert Morley - Doctor Percival
* Joop Doderer - Cornelius Muller
* Ann Todd - Castles Mother 
* Richard Vernon - Sir John Hargreaves 
* Angela Thorne - Lady Hargreaves
* Nicol Williamson - Maurice Castle Iman - Sarah
 

==Production==
The film was shot in Kenya and at Shepperton Studios near London. As with the book, much of the theme about alleged treason and suspicion are based on the defection of Kim Philby, a friend of Graham Greene, to the Soviet Union|Soviets. The movie also introduced Iman (model)|Iman, who was working as a model before she went into movies.

Preminger had trouble securing funding for the film and had to partially fund it with his own money. Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p 234-235  
==References==
 

 

 
 
 
 
 
 
 
 
 
 
 
 