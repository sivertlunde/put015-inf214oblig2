Short Circuit (1986 film)
{{Infobox film
| name           = Short Circuit
| image          = Short circuit.jpg
| caption        = Theatrical release poster
| director       = John Badham
| producer       = {{plainlist|
* David Foster
* Lawrence Turman
}}
| writer         = {{plainlist|
* S. S. Wilson
* Brent Maddock
}}
| starring       = {{plainlist|
* Ally Sheedy
* Steve Guttenberg
* Fisher Stevens
* Austin Pendleton
* G. W. Bailey
}}
| music          = David Shire
| cinematography = Nick McLean
| editing        = Frank Morriss
| studio         = {{plainlist|
* Producers Sales Organization
* The Lawrence Turman|Turman-Foster Company
}}
| distributor    = TriStar Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $9 million 
| gross          = $40.7 million (US)
}}

Short Circuit is a 1986 American comic science fiction film directed by John Badham, and written by S. S. Wilson and Brent Maddock. The films plot centers upon an experimental military robot that is struck by lightning and gains a more humanlike intelligence, with which it embarks to explore its new state. Short Circuit stars Ally Sheedy, Steve Guttenberg, Fisher Stevens, Austin Pendleton, and G. W. Bailey, with Tim Blaney as the voice of the robot, Johnny Five.

A sequel, Short Circuit 2, was released in 1988.

==Plot==
Number 5 is part of a series of prototype U.S. military robots built for the Cold War by NOVA Laboratories. The series inventors, Newton Graham Crosby and Ben Jabituya, are more interested in peaceful applications including music and social aid. After a demonstration of the robots capabilities, Number 5 is hit by a lightning-induced power surge, erasing its programming and giving it a sense of free will. Several incidents allow the robot to escape the facility accidentally, barely able to communicate and uncertain of its directive. In Astoria, Oregon, Stephanie Speck (who cares for animals and mistakes Number 5 for an extraterrestrial visitor) grants Number 5 access to books, television, and other stimuli, to satisfy his hunger for input; whereupon Number 5 develops a whimsical and curious childlike personality. When Stephanie realizes Number 5 is a military invention, she contacts NOVA who send out a team to recover him, bringing one of the other robots along to help. When Number 5 accidentally crushes a grasshopper and gains an understanding of mortality, he concludes that if NOVA disassembles him he too will cease to be alive. Horrified, Number 5 steals Stephanies van and flees; but the pair are cornered by NOVA, including Newton and Ben. Although Stephanie attempts to reveal his newly discovered sentience, Number 5 is deactivated and captured. Being taken on the way to NOVA, he manages to turn himself back on and escapes despite a tracker that had been placed on him. Returning to Stephanie for protection, Number 5s unusual actions catch the attention of his creators, but NOVAs CEO Dr. Howard Marner turns a deaf ear to their wild hypothesis.

From this follow several adventurous escapes from the soldiers, led by NOVAs security chief Captain Skroeder (G. W. Bailey). Having humiliated Stephanies suitor Frank and three of the remaining prototypes (that have been reprogrammed to imitate The Three Stooges by Number 5), Stephanie and the robot convince Newton of the robots sentience. Shortly after, the trio are cornered by Skroeder who has secretly taken over NOVAs security and the United States Army|Army, both of whom chases after Number 5, whose only concern is Newton and Stephanies safety. Number 5 flees across the fields until an US Army helicopter sweeps over the a nearby rise and its missiles blow the hapless robot to pieces. Newton and Stephanie are devastated over the loss, and watching Skroeders men trading scraps of their blasted quarry as trophies, Newton quits NOVA. Marner is also disappointed at the destruction of millions of dollars, as well as years of research, and seeing the security team celebrate, quickly dismisses Skroeder for insubordination. A tearful Stephanie leaves with Newton, who plans to head to his familys estate in Montana and start over. During their drive, Number 5 reveals himself to them, surprising his friends by revealing the warmongers actually blew up a duplicate he made out of spare parts. Elated in their reunion, Number 5 renames himself "Johnny Five" after a song "Whos Johnny" that he heard when he first met Stephanie. He accompanies her and Newton to their new home.

==Cast==
*Ally Sheedy as Stephanie Speck, who befriends Johnny Five
*Steve Guttenberg as Newton Crosby, Ph.D., the designer of the prototypes
*Fisher Stevens as Ben Jabituya, Newtons assistant
*Austin Pendleton as Dr. Howard Marner, President of Nova Robotics
*G. W. Bailey as Captain Skroeder
*Tim Blaney as Number 5 (voice)
*Brian McNamara as Frank, Stephanies abusive ex-boyfriend
*Marvin J. McIntyre as Duke, one of Novas security officers
*John Garber as Otis
*Penny Santon as Mrs. Cepeda, Stephanies housekeeper
*Vernon Weddle as General Washburne
*Barbara Tarbuck as Senator Mills
*John Badham as Cameraman (uncredited)

==Production==
 
 
 
This film was conceived after the producers distributed an educational video about a robot to various colleges. Studying other films with a prominent robot cast (like the Star Wars series) for inspiration, they decided to question human reactions to a living robot, on the premise that none would initially believe its sentience.

According to the commentary in the DVD, Number 5 was the most expensive part of the film, requiring several different versions to be made for different sequences. Almost everything else in the film was relatively inexpensive, allowing them to allocate as much money as they needed for the robot character. Number 5 was designed by Syd Mead, the "visual futurist" famous for his work on Blade Runner and Tron.

Meads design was greatly influenced by the sketches of Eric Allard, the Robotics Supervisor credited for "realizing" the robots. John Badham named Eric "the most valuable player" on the film.

Most of the arm movements of Number 5 were controlled by a "telemetry suit", carried on the puppeteers upper torso. Each joint in the suit had a separate sensor, allowing the puppeteers arm and hand movements to be transferred directly to the machine. He was also voiced in real-time by his puppeteer, the director believing that it provided for a more realistic interaction between the robot and the other actors than putting in his voice in post-production, although a few of his lines were re-dubbed later.  

Fisher Stevens said that when he was originally hired to play Ben Jabituya, the character was not intended to be Indian. Stevens was fired and replaced by Bronson Pinchot at one point, but then Pinchot was fired and Stevens was rehired.  To portray the role he had to grow a beard, dye his hair black, darken his skin with makeup, turn his blue eyes brown with contact lenses, speak with an East Indian accent and "walk hunched over like a cricket player." 

During Stephanies impromptu news interview, director John Badham makes a cameo appearance as the news cameraman.

==Soundtrack==
Although no soundtrack album was released at the time, El DeBarge had a chart hit with the single "Whos Johnny|Whos Johnny (Theme from Short Circuit)." 
 CD Club series of limited edition releases. The DeBarge song was not included or mentioned in the liner notes. The last three tracks are source music. 

The booklet claims the end title song is not used in the film. It is, however, on the soundtrack. The finale mix and end title are combined into one track, but used separately in the film.
{{Track listing
| headline        = Short Circuit   
| total_length    = 43:09
| title1          = 	Main Title
| length1         = 	2:13
| title2          = 	The Quickening/Off The Bridge 
| length2         = 	2:44
| title3          = 	Discovering Number 5/Sunrise 
| length3         = 	4:32
| title4          = 	Grasshopper/Joy(less) Ride 
| length4         = 	4:43
| title5          = 	The Attack/Coming To 
| length5         = 	3:47
| title6          = 	Road Block/Bathtub/Robot Battle
| length6         = 	2:42
| title7          = 	Getaway/Hello, Bozos 
| length7         = 	2:41
| title8          = 	Night Scene/Joke Triumph 
| length8         = 	4:17
| title9          = 	Danger, Nova/Escape Attempt/Aftermath 
| length9         = 	3:48 Marcy Levy
| length10         = 	5:04
| title11          = 	Source Music: Rock 
| length11         = 	3:39
| title12          = 	Source Music: Bar 
| length12         = 	1:51
| title13          = 	Source Music: The Three Stooges 
| length13         = 	1:08
}}

==Video game==
A video game developed by Ocean Software for ZX Spectrum,  Commodore 64  and Amstrad CPC   was also made based on the film. It featured two parts: one arcade adventure where Johnny 5 had to escape from the lab, and one action part where Johnny 5 escapes across the countryside, avoiding soldiers, other robots, and animals.

==Reception==
The film received mixed to positive reviews from critics. It currently holds a 57% rating on   wrote, "Short Circuit is a hip, sexless sci-fi sendup and praises the writers for some terrific dialog that would have been a lot less disarming if not for the winsome robot and Sheedys affection for it. Guttenberg plays his best goofy self."  In The New York Times,  Vincent Canby wrote, "The movie, which has the clean, well-scrubbed look of an old Disney comedy, is nicely acted".   The Sun-Sentinel gave a good review, saying, "Number Five is the real star of this energetic film. Sheedy, Guttenberg and company are just supporting players."   Roger Ebert of the Chicago Sun-Times rated it 1.5 out of 4 four stars and called it "too cute for its own good". 
 The Fly, Little Shop About Last Night.  

===Awards and nominations===
* Honored with the Winsor McCay Award  
{| class="wikitable"
|-
|+ Awards
|-
! Award
! Category
! Recipient(s)
! Outcome
|- Saturn Awards
|- Best Director
| John Badham
| 
|- Best Science Fiction Film
| 
| 
|- Best Special Effects
| Eric Allard, Syd Mead
| 
|-
| BMI Film Music Award
| 
| David Shire
|  
|}

==Sequel and remake==
The sequel, Short Circuit 2, premiered in 1988.  There was a script for a possible third film written in 1989 and rewritten in 1990, but it was found unsatisfactory by the producers, and the project was subsequently scrapped.

In April 2008, Variety (magazine)|Variety reported that Dimension Films had acquired the rights to remake the original film. Dan Milano had been hired to write the script, and David Foster to produce it. Foster said that the robots appearance would not change. 

On October 27, 2009, it was announced that Steve Carr would direct the remake and that the films plot would involve a boy from a broken family befriending the Number 5 robot.  
 Tim Hill would direct the reboot instead. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 