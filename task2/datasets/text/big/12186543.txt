So Evil, So Young
{{Infobox film
| name           = So Evil, So Young
| image          = "So_Evil,_So_Young"_(1961).jpg
| image_size     = 
| caption        = Original British poster
| director       = Godfrey Grayson
| producer       = Edward J. Danziger   Harry Lee Danziger 
| writer         = Mark Grantham
| narrator       =  Jocelyn Britton
| music          =  James Wilson
| editing        = Desmond Saunders
| studio         = Danziger Productions
| distributor    = United Artists
| released       = 1961
| runtime        = 77 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1961 British Technicolor Reform school Prison film from the Danzigers, starring Jill Ireland and Ellen Pollock. 

==Synopsis==
 hack into the safe behind a picture. Inside are several jewels. Claire warns Lucy to leave, but Lucy says that the home-owners are on vacation and the butler is the only one at home; however, he is at the back of house. She says that they have all the time in the world. However, a few seconds later the butler walks in on them. He sees Lucy and recognizes her, but does not see Claire; Claire whacks him on the head with something, knocking him out.
 
Lucy tells Claire to take the jewels and give her half when she gets out of prison, where she will undoubtedly go. She heads to a local club where she sees her ex-boyfriend Tom (John Charlesworth) eyeing his new girlfriend Anne (Jill Ireland). Jealous and angry, she takes a necklace she kept in her jacket pocket, which she stole from the house, and puts it in Annes jacket pocket, without anyone seeing.

The police come to Annes house and ask her if she knows of any stolen jewellery, for Lucy has told the police that she was her accomplice. Anne denies and but the police find the necklace in her coat pocket. She is sentenced to three years in Wilsham, a prison with no bars on the windows and they rely on your honour not to just walk out. They also do not like to call it a prison.

Everybody comes to hate the warden of the girls, Miss Smith. Anne, as she was a secretary before coming to Wilsham, is told to help the mistress with her secretary business, which Anne accepts willingly. Meanwhile, Lucy starts to pit all the girls against Anne, except for one, Marie, who has served four years in Wilsham and is getting out in one week.
 hanged herself.

When questioned about if anything she could have said indirectly have caused Marie to hang herself, the warden denies this. One of the girls talks to Anne and apologizes for their actions towards her and tells Anne that she knows shes innocent of the crime she was accused of committing. She tells her to talk to Dear Old Margen and leaves it at that.

The girls then start a riot led by Lucy and the girl that talked to Anne earlier. They hurt Miss Smiths arm and trash the kitchen. The only one that didnt participate in the riot is Anne and all the girls are sentenced to a punishment except Anne. Anne asks the mistress if she can have the same punishment as the rest of the girls and the mistress hesitantly complies.
 pawn shop, who reveals that Claire pawned all the jewels and told him a false story. He checks the police record for stolen jewellery and finds all the pieces there. He calls the police who find Claire and bring her into the station. They also bring Lucy into the station who denies that Claire is her partner; however, when she finds out that Claire pawned all the jewels, she becomes outrages and has a fight with her, and then tells the police that Claire really was her accomplice.

Anne is released from prison.

==Cast==
* Jill Ireland as Ann
* Ellen Pollock as Miss Smith
* John Charlesworth as Tom Jocelyn Britton as Lucy
* Joan Haythorne as Matron
* John Longden as Turner
* Sheila Whittingham as Mary
* Bernice Swanson as Claire
* Colin Tapley as Inspector
* Constance Fecher as Cell Wardress
* Annette Kerr as Workroom Wardress
* C. Denier Warren as Sam
* Gwendolyn Watts as Edna

==Critical reception== Sentenced for Life (1960) and Man Accused (1959) to create So Evil, So Young... derivative in concept, So Evil, So Young strived for novelty by being one of the Danzigers only films shot in Technicolor" ;  while Sky Movies called the film, "an offbeat movie drama which takes a grim look at a womens reformatory. Jill Ireland, now married to screen tough guy Charles Bronson, is both fetching and effective in the central role" ;  and TV Guide wrote, "the cruelties of reform school are realistically exposed," adding, "this film has never been shown theatrically in the US, but it should be."  

== External links ==
*  

==References==
 

 

 
 
 
 
 
 