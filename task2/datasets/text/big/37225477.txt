Sound City (film)
 
{{Infobox film
| name = Sound City
| image = Sound-City-poster.png
| caption = Theatrical release poster
| director = Dave Grohl
| producer = Dave Grohl James A. Rota John Ramsay
| writer = Mark Monroe
| genre = Documentary
| cinematography = Kenny Stoff Paul Crowder
| studio = Therapy Content Diamond Docs
| distributor = {{Plain list | 
* Variance Films
* Roswell Films
* Gravitas Ventures
}}
| released =    
| runtime = 107 minutes
| country = United States
| language = English
| budget =
| gross  =  $422,853    
}} directorial debut, about the history of recording studio Sound City Studios in Van Nuys, Los Angeles.   

==Background== Nirvana recorded Neve 8028 analog mixing console, when the studio closed in 2011. 

==Synopsis  ==
Sound City Studios was located in the  s One Hot Minute, Nirvana (band)|Nirvanas Incesticide and Nevermind, Rage Against The Machines Rage Against the Machine (album)|self-titled album and many others.

==Release==
The film was first exhibited in the 2013 Sundance Film Festival on January 18, 2013,  and released on video-on-demand and in theaters on February 1, 2013.  It was screened on January 31 in five Australian cities (Melbourne, Sydney, Brisbane, Adelaide and Perth).  The documentary was also screened in three cities in Canada (Toronto, Vancouver and Montreal) and 51 cities in the United States.  The film was screened for a one-off showing on February 18, 2013 in 23 theaters across the United Kingdom. 

After the   of the film. {{cite web | url=http://webcache.googleusercontent.com/search?q=cache:9CthFvZgO10J:www.imdb.com/title/tt2306745/fullcredits/+%22Brian+L.+Hauge%22%22key+grip%22&hl=en| title=
Sound City (2013) Full Cast & Crew - Camera and Electrical Department | publisher=Internet Movie Database | accessdate=December 3, 2014}} 

==Appearances  ==
The documentary features interviews conducted by Grohl of artists associated with the studio:  

 
* Vinny Appice Frank Black
* Lindsey Buckingham
* Johnny Cash (archival)
* Kurt Cobain (archival)
* Kevin Cronin
* Rivers Cuomo
* Warren DeMartini
* Mick Fleetwood
* John Fogerty
* Neil Giraldo
* Josh Homme
* Alain Johannes
* Jim Keltner
* Barry Manilow
* Paul McCartney
* Rupert Neve
* Stevie Nicks
* Rick Nielsen
* Krist Novoselic
* Stephen Pearcy
* Tom Petty
* Trent Reznor
* Ross Robinson
* Rick Rubin Jim Scott
* Pat Smear
* Rick Springfield
* Corey Taylor
* Benmont Tench
* Lars Ulrich
* Butch Vig
* Lee Ving
* Brad Wilk
* Neil Young
* Robert Levon Been

 
 Heaven & Rage Against The Machine appeared in the film.

The drummer of Foo Fighters, Taylor Hawkins, also appeared in the film.

==Reception==

===Critical response===
 
Sound City received positive reviews. Film   and documentary movies of the year on the website.   On Metacritic, which assigns a normalised rating out of 100 based on reviews from critics, the film has a score of 76 based on reviews from 15 critics, indicating "generally favorable reviews". 
 New York Daily News praised that "Grohls aim is to explore the aura of a place, but what he winds up proving is that people make the magic." 

Nevertheless Phil Gallo from Billboard (magazine)|Billboard stated "Grohls inexperience as a filmmaker only shows when the film makes a sharp turn out of history and into the more recent past: Theres a sense that instead of celebrating great rock n roll moments, a product is about to be pitched at the viewer." 

  18th edition Cinema Eye Honor 2014 Audience Choice Prize.

===Certifications===
 
 
 
 

==Soundtrack==
 
Sound City: Real to Reel is the official soundtrack of the documentary and was released on March 12, 2013. The songs "Cut Me Some Slack", "From Can to Cant", "You Cant Fix This" and "Mantra" were made available on Sound Citys official YouTube channel on December 14, 2012, January 15, 2013, February 15, 2013 and March 8, 2013, respectively.

== See also ==
*  , 2014 television series by Grohl inspired by Sound City

==References==
 

==External links==
*  
*  
*   on Facebook
*   on Twitter
*  
*  
*  
*  
*   by Mark Deming
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 