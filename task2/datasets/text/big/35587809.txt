Ruby Sparks
 
{{Infobox film
| name           = Ruby Sparks
| image          = Ruby Sparks poster.jpg
| image_size     = 215px
| alt            = A block of text with a blank space forming the outline of a woman. A man carrying a woman over his shoulder. 
| caption        =  Jonathan Dayton Valerie Faris
| writer         = Zoe Kazan Albert Berger Ron Yerxa
| starring       = {{Plain list | 
* Paul Dano
* Zoe Kazan
* Annette Bening
* Antonio Banderas
* Steve Coogan
* Elliott Gould
* Chris Messina
}}
| music          = Nick Urata
| cinematography = Matthew Libatique Pamela Martin
| studio         = Bona Fide Productions
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 104 minutes   
| country        = United States
| language       = English 
| budget         = 
| gross          = $9,128,263 
}} romantic comedy-drama film directed by Jonathan Dayton and Valerie Faris, and written by Zoe Kazan. It stars Paul Dano as an anxious novelist whose fictional character, Ruby Sparks, played by Kazan, comes to life.   

== Plot ==
Calvin Weir-Fields (Paul Dano) is a young novelist who is struggling to recreate the early success of his first novel but unable to commit to any of his ideas. With his introverted personality and idealistic view of what it means to be in love, Calvin also struggles in finding relationships, feeling most women are only interested in an idolized and preconceived notion of who they believe him to be.

His therapist, Dr. Rosenthal ( ).

Calvins brother Harry (Chris Messina) and sister-in-law Susie (Toni Trucks) come to visit and find several articles of womens clothing around the house. That night while writing before falling asleep at his typewriter, Calvin writes a passage with Ruby admitting, although he is not the type of guy she usually falls for, she is falling in love with him. 
The next day, Calvin is stunned to find Ruby in his kitchen, as an actual living person. Thinking he is going crazy, he calls Harry, who does not believe him and advises him to meet with someone else to take his mind off things. Ruby is confused by his behavior and insists on coming along, but he leaves her to shop while he meets Mabel (Alia Shawkat), a young fan of his book who had given him her number. Ruby finds them and believes he is cheating on her. In the ensuing confrontation, Calvin discovers that others can actually see Ruby, proving that she is a real person and not a figment of his imagination. Calvin explains that he feels overwhelmed, and they make up.

Calvin introduces Ruby to Harry, who is incredulous at first and suggests alternate explanations. However, Calvin soon proves that his writing directly affects Ruby. Saying he loves Ruby, Calvin asks Harry not to tell anyone of Rubys origins. Although Harry warns him that women are mysterious creatures and that things may change, Calvin insists that since he wrote her into existence, he knows her. He asserts that he will never write about Ruby again.

Months later, Calvin reluctantly takes Ruby to meet his free-spirited mother Gertrude (Annette Bening) and her boyfriend Mort (Antonio Banderas). While Ruby with her much more outgoing personality enjoys the time with Calvins family, introverted Calvin spends the weekend reading by himself, growing jealous of the time Ruby is spending with other people. Rubys happy spirit begins to dampen at Calvins increased gloominess.

After they return home to Calvins house, the relationship is tense. Calvin complains about Rubys singing while she cooks and he reads. Depressed, she and Calvin have a serious talk. Ruby tells Calvin how lonely she is and suggests they start spending less time together. Calvin feels miserable and fearful Ruby will soon break up with him. He begins to write Rubys story again out of desperation, and writes that Ruby is also miserable without him. Ruby returns to Calvins house full-time but becomes incredibly clingy toward him, afraid to leave his side for even a second. Calvin tires of this and writes that Ruby is instead "filled with effervescent joy," at which point she becomes constantly happy. This leaves Calvin unhappy, knowing her happiness is artificial.

After talking with Harry about what he has been doing, Calvin intends to write Ruby back to her normal self, but the wording he uses leaves Ruby confused. Ruby fights with Calvin once more. He attempts to cheer her up and takes her along to a party hosted by author Langdon Tharp (Steve Coogan). At the party, Calvin leaves Ruby and talks with people about his still-unfinished manuscript. He also runs into his ex-girlfriend Lila (Deborah Ann Woll), and they have a heated argument in which Lila accuses Calvin of being uninterested in anyone outside of himself. Langdon sees Ruby alone and flirts with her, convincing her to strip to her bra and panties and join him in the pool. Calvin finds Ruby just as she is stepping into the pool with Langdon. Furious and humiliated, Calvin drives Ruby home.

At home, Calvin and Ruby fight, with Ruby telling Calvin he can not stop her from doing what she wants. As she prepares to leave, Calvin reveals that she is a product of his imagination and that he can indeed control her, and is capable of making her do anything he writes. He demonstrates this to her by making her perform various ever more frenzied and humiliating actions. In his excitement, he jams the type bars of his typewriter. Ruby, exhausted, collapses to the floor. Calvin hangs his head in anguish, before gently attempting to approach Ruby. Just as Calvin reaches out for her, Ruby runs into his bedroom and locks the door.

Calvin writes a final page, which states that as soon as Ruby leaves the house she is no longer his creation, no longer subject to his will, and she is free. He leaves the manuscript outside her door, with a note telling her to read the last page and that he loves her. The next morning, Calvin finds that the note is gone along with Ruby, and he breaks down sobbing. 

Time passes, and Harry suggests he write a new book about his experiences with Ruby. The novel, The Girlfriend, is a success. While walking with Scotty in the park, Calvin sees a woman that appears to be Ruby, but she has no recollection of him. She is reading his book, which she says her friend described as pretentious. She also states Calvin seems very familiar to her, which Calvin deflects by showing her his photo on the book. Embarrassed when Calvin reveals himself as the author, she asks if they can "start over". They sit and talk, and she asks him not to spoil the ending for her. Calvin replies, "I promise."

== Cast ==
* Paul Dano as Calvin Weir-Fields, a young novelist who struggles with writers block having not written a full book since his first publication at 19.
* Zoe Kazan as Ruby Tiffany Sparks, a woman who initially is a dream and inspires Paul Danos character to write about her until she eventually manifests herself fully in his life. 
* Chris Messina as Harry Weir-Fields, Calvins older brother whom he confides in about the true nature of Ruby.
* Annette Bening as Gertrude, Calvins mother.
* Antonio Banderas as Mort, the carefree boyfriend of Calvins mother. Banderas took on the role for the opportunity to work with Bening. 
* Aasif Mandvi as Cyrus Modi, Calvins publicist.
* Steve Coogan as Langdon Tharp, a novelist friend of Calvin who he first met when his first book was published. 
* Toni Trucks as Susie Weir-Fields, Harrys wife.
* Deborah Ann Woll as Lila, Calvins ex-girlfriend who left him before the events of the film and has recently written her own book. 
* Elliott Gould as Dr. Rosenthal, Calvins therapist who encourages Calvin to write a page about anything not caring if it was bad.
* Alia Shawkat as Mabel, a fan of Calvins who gives him her number at a book signing.
* Wallace Langham as Warren
* Michael Berry Jr. as Silverlake Passerby

== Production ==

=== Writing ===
The film was written by Zoe Kazan who plays the eponymous character. Kazan was initially inspired by a discarded mannequin, and the myth of Pygmalion (mythology)|Pygmalion, quickly writing twenty pages, before putting the script aside for six months. She returned to the writing when she was clear on the central concept of comparing the idea of love to the actuality of it.     Groundhog Day, wanting to present a slanted version of our own reality. From early in the development she wrote the lead character Calvin with her boyfriend Paul Dano in mind. On the feminist aspects of the story Kazan explains she wanted to explore the idea of "being gazed at but never seen" where a woman is not properly understood but in a way that wasn’t unkind or alienating for men.  She rejects the description of Ruby Sparks as a Manic Pixie Dream Girl, calling it reductive and diminutive, whereas Ruby Sparks is about the danger of idealizing a person, of reducing a person down to an idea of a person. 

Kazan thanks Warren Beatty for his indirect encouragement of Paul Dano to develop their own material, and Dano in turn suggested she write a project. 

=== Development ===
Kazan shopped the script around and got the attention of Albert Berger and Ron Yerxa, the producers of Little Miss Sunshine, who sent it to directing couple Jonathan Dayton and Valerie Faris, who took it on as the first project since Little Miss Sunshine in 2006. Faris blamed their delay between films on their own need to be ready for the right project. 

=== Location ===
Kazan talked about the importance of Los Angeles as the location for the film, comparing it to a character as much as a setting. She sees Los Angeles as a place where it is easy to feel alone and isolated and that fits in with the isolation of Calvin in the story.  Directors Dayton and Faris said it was great to show Los Angeles itself, rather than pretending to be another place.
 Los Feliz Hollywood Cemetery.

The party hosted by Langdon Tharp is held in a Lloyd Wright-designed house. 

== Reception ==

===Critical Response===
Ruby Sparks received positive reviews from critics and has a "Certified Fresh" score of 79% on Rotten Tomatoes based on 168 reviews with an average rating of 7.1 out of 10. The critical consensus states "Cleverly written and wonderfully acted, Ruby Sparks overcomes its occasional lags in pace with an abundance of charm and wit".  The film also has a score of 67 out of 100 on Metacritic based on 40 critics indicating "Generally favorable reviews." 

Stephen Holden from the New York Times described it as- "Ruby Sparks doesn’t try to pretend to be more than it is: a sleek, beautifully written and acted romantic comedy that glides down to earth in a gently satisfying soft landing."  Online film critic Chris Pandolfi from At A Theater Near You called it "an intelligent commentary on the creative process, insecurity, controlling behavior, idealism, and the fragility of the male ego. It’s all rather ingeniously combined into one of the most likeable films I’ve seen all year – a fantasy, a character study, and a cautionary tale all rolled into one."   

===Box office===
Ruby Sparks opened in a limited release in 13 theaters and grossed $140,822, with an average of $10,832 per theater and ranking #28 at the box office. The films widest release in the U.S. was 261 theaters and it ultimately earned $2,540,106 domesitcally and $6,588,157 internationally for a worldwide total of $9,128,263. 

== Music ==
  score was composed by DeVotchKas Nick Urata.  

{{Track listing
| title1 = Creation
| title2 = Writers Block
| title3 = Inspiration!
| title4 = Ruby Sparks
| title5 = I Was Waiting for You
| title6 = Ill Go with You
| title7 = Shes Real
| title8 = Ça plane pour moi|Ça Plane Pour Moi
| note8  = Plastic Bertrand
| title9 = Une Fraction de Seconde
| note9  = Holden
| title10 = He Loved You
| title11 = Quand Tu Es La (The Game of Love (Wayne Fontana song))
| note11  = Sylvie Vartan
| title12 = Psychedelic Train
| note12  = Derrick Harriott
| title13 = Roll It Round
| note13  = The Lions
| title14 = Miserable
| title15 = Inseparable" (feat. TIMUR (Bekbosunov) on vocals)   
| title16 = Youre a Genius
| title17 = The Past Released Her
| title18 = She Came to Me
| title19 = Can We Start Over
| title20 = Ruby Was Just Ruby
}}

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 