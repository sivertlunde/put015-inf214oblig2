Pithamaha
{{Infobox film|
| name = Pithamaha
| image = 
| caption =
| director = K. S. L. Swamy
| writer = Subhash Ghai
| based on = Hindi film Vidhaata Rajesh  Udaykumar   V. Ravichandran   Jai Jagadish   Vijayalakshmi Singh
| producer = Sangram Singh   Jairam Singh
| music = M. Ranga Rao
| cinematography = D. V. Rajaram
| editing = V. P. Krishnan
| studio = Singh Brothers
| released =  
| runtime = 134 minutes
| language = Kannada
| country = India
| budget =
}}

Pithamaha ( ) is a 1985 Kannada film directed by K. S. L. Swamy (Ravi) and produced by Singh Brothers. The film stars Rajesh (Kannada actor)|Rajesh, Udaykumar, V. Ravichandran and Vijayalakshmi Singh in the lead roles[  While M. Ranga Rao composed the music, the lyrics and dialogues were written by Chi. Udaya Shankar.

The film was a remake of the 1982 blockbuster Hindi film Vidhaata directed by Subhash Ghai and starring Dilip Kumar, Shammi Kapoor, Sanjeev Kumar and Sanjay Dutt in the lead roles. 

== Cast == Rajesh
* Udaykumar
* V. Ravichandran
* Vijayalakshmi Singh
* Sundar Krishna Urs
* Jai Jagadish
* Dheerendra Gopal
* Thoogudeepa Srinivas
* Mysore Lokesh
* Bank Janardhan
* Keerthi Raj
* Ashwath Narayan

== Soundtrack ==
The music was composed by M. Ranga Rao with lyrics by Chi. Udaya Shankar.  All the songs composed for the film were received well, especially "Mareyadiru Aa Shaktiya" is considered an evergreen song.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Angailiruva Rekheye
| extra1 = S. P. Balasubramanyam, K. J. Yesudas
| lyrics1 = Chi. Udaya Shankar
| length1 = 
| title2 = Ellara Mane Doseyu
| extra2 = S. P. Balasubramanyam, S. Janaki
| lyrics2 = Chi. Udaya Shankar
| length2 = 
| title3 = Kudi Baa Kudi Baa
| extra3 = Ravi, S. Janaki
| lyrics3 = Chi. Udaya Shankar
| length3 = 
| title4 = Mareyadiru Aa Shaktiyaa
| extra4 = K. J. Yesudas
| lyrics4 = Chi. Udaya Shankar
| length4 = 
}}

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 