An Invisible Sign
{{Infobox film
| name           = An Invisible Sign
| image          = An Invisible Sign Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Marilyn Agrelo
| producer       = {{Plainlist|
* Jana Edelbaum
* Rachel Cohen
}}
| screenplay     = {{Plainlist|
* Michael Ellis
* Pamela Falk
}}
| based on       =  
| starring       = {{Plainlist|
* Jessica Alba
* J. K. Simmons Chris Messina
* Bailee Madison
* Sophie Nyweide
}}
| music          = Andrew Hollander
| cinematography = Lisa Rinzler
| editing        = Sabine Hoffmann
| studio         = {{Plainlist| Kimmel International
* Silverwood Films
}}
| distributor    = {{Plainlist|
* IFC Films  
* MPI Home Video  
* Pinema  
}}
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $49,859   
}} Chris Messina, Sophie Nyweide, and Bailee Madison. Based on the 2001 novel An Invisible Sign of My Own by Aimee Bender, the film is about a painfully withdrawn young woman who, as a child, turned to math for comfort after her father became ill, and now as an adult, teaches the subject and must help her students through their own crises.    For her performance in the film, Bailee Madison received a 2011 Young Artist Award nomination for Best Performance in a Feature Film.   

==Plot== Chris Messina) shows romantic interest in her, Mona reverts to some of her old self-destructive impulses. Eventually, Mona discovers her value in the love she shows her students, and Bens patience is finally rewarded as the two find love in each other.

==Cast==
* Jessica Alba as Mona Gray Chris Messina as Ben Smith
* John Shea as Dad
* Sônia Braga as Mom
* J. K. Simmons as Mr. Jones
* Bailee Madison as Young Mona Gray
* Sophie Nyweide as Lisa Venus
* Donovan Fowler as Levan Beeze
* Mackenzie Milone as Ann DiGanno
* Jake Richard Siciliano as Elmer Gravlaki
* Emerald-Angel Young as Rita Williams
* Ian Colletti as Danny OMazzi
* Marylouise Burke as Ms. Gelband
* Joanna P. Adler as Lisas mom
* Ashlie Atkinson as Lisas Aunt
* Crystal Bock as Panida Saleswoman
* Stephanie DeBolt as Ellen
* Sharon Washington as Levans Mom
* Daniel McDonald as Runny Nose Boy
* Ian Blackman as Anns Dad
* Jill Abramovitz as Anns Mom
* Daniel Pearce as Dannys Dad
* Lori Hammel as Dannys Mom
* Conor Carroll as 5th Grader
* Brandon Jeffers as Science Kid #1
* Daniel Dugan as Attorney
* Blythe Auffarth as Candy Striper
* Marin Gazzaniga as Hostess
* Bill Coelius as Movie Patron #1
* Tom Nonnon as Doctor
* Lilly Hartley as Female Runner   

==Production==
;Script
Aimee Benders 2001 novel An Invisible Sign of My Own was adapted for the screen by The Wedding Planner scribes Michael Ellis and Pamela Falk. 

;Filming locations
* Steiner Studios, 15 Washington Avenue, Brooklyn, New York City, New York, USA
* Tarrytown, New York, USA 

;Release
A test screening was shown at Comsewogue High School in Port Jefferson Station, New York. The film premiered at the 18th Hamptons International Film Festival  on October 7, 2010. It was released through video on demand on April 1, 2011, and premiered in theaters on May 6, 2011.   

==Reception==
The film received universally negative reviews upon release. Rotten Tomatoes reported 0 positive and 14 negative reviews, including mentions to the films "boredom" and singling out both Albas performance and Agrelos direction. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 