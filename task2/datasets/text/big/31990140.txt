A Hound for Trouble
 
{{Infobox Hollywood cartoon
| cartoon name      = A Hound For Trouble Charlie Dog)
| image             = 
| caption           =  Charles M. Jones Edward Selzer
| story artist      = Michael Maltese
| narrator          =
| voice actor       = Mel Blanc
| musician          = John Carey Phil Monroe
| layout artist     =
| background artist =
| studio            = Warner Bros. Cartoons
| distributor       = Warner Bros.
| release date      =  
| color process     = Technicolor
| runtime           =
| country           = United States
| language          = English
}}
 Charlie Dog.

==Plot== Charlie is found by the ships owner (who thought he kicked him off earlier) and is kicked off (again). Trying to find a new master, Charlie keeps asking people in English, but they keep responding no capice ("I dont understand"). Charlie eventually spots a restaurant owner opening his shop and makes himself at home (the shop owner actually speaking English) before being kicked out of the restaurant. As he goes back, he sees the owner has gone out for 15 minutes ("15 minootsa") and decides to run the restaurant himself. Charlie first enrages the one customer who comes in, then after feeding him spaghetti from a spool serves grape juice he presses with his feet in front of the customer, causing the customer to rush out. When the shop owner comes back, Charlie tries to convince the owner that they need a floor show (singing "Atsa Matta for You?"). The owner finally appears to relent, and starts walking Charlie home when he yells that the Tower of Pisa is "about to fall on that little house!" The owner then has Charlie hold up the tower while he calls for help (instead just going back to his restaurant). Charlie is left holding up the tower, calling out for help and asking "Doesnt anyone around here capice?"

==External links==
*  

 
 
 
 
 
 

 