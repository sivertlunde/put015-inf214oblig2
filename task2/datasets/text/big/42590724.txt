Chamada a Cobrar
{{Infobox film
| name           = Chamada a Cobrar
| image          = Chamada a Cobrar Film Poster.jpg
| caption        = Theatrical release poster
| director       = Anna Muylaert
| producer       = 
| writer         = Anna Muylaert
| starring       = Cida Almeida Maria Manoela Lourenço Mutarelli Pierre Santos Tatiana Thomé
| music          = Rica Amabis Tejo Damasceno
| cinematography = Marcelo Trotta
| editing        = André Finotti
| studio         = Africa Filmes
| distributor    = Gullane Entretenimento
| released       =  
| runtime        = 72 minutes
| country        = Brazil
| language       = Portuguese
| budget         = R$950,000 
| gross          = R$6,479 
}}
Chamada a Cobrar is a 2012 Brazilian thriller film written and directed by Anna Muylaert. The film is an offshoot of the television film Para Aceitá-la Continue na Linha produced for TV Cultura in 2010. It was selected as hours-concours for the Première Brasil of the 2012 Festival do Rio. 

==Plot==
The film tells the story of Clara, a lady of high society in São Paulo that falls in a fake kidnapping scam and is guided by the voice of the criminal in the following 12 hours, following by roads that lead up to the suburb of Rio de Janeiro. In this way, she will be extorted up to the limit of her credit card.   

==Cast==
*Cida Almeida as Dalva
*Maria Manoela as Cristina
*Lourenço Mutarelli as Delegado
*Pierre Santos as Vladmir
*Tatiana Thomé as Adriana

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 
 