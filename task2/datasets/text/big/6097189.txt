Johnny Oro
{{Infobox film
| name           = Johnny Oro
| image          =Johnny-oro-poster.jpg
| image_size     =
| caption        =
| director       = Sergio Corbucci
| producer       =
| writer         =
| narrator       =
| starring       = Mark Damon
| music          = Carlo Savina
| cinematography = Riccardo Pallottini
| editor       =
| distributor    =
| released       = 1966
| runtime        =
| country        = Italy Italian
| budget         =
 1966 spaghetti western directed by Sergio Corbucci and starring Mark Damon. 

==Synopsis==
Renamed to cash in on the success of Duccio Tessari’s Ringo movies, Ringo and His Golden Pistol focuses on bounty hunter Johnny Oro/Ringo who sees killing as purely business, in fact he won’t draw his solid gold pistol unless he can profit from it and this will get him into trouble. He lets Juanito Perez live because there is nothing to gain from killing a man without a price on his head unlike his brothers who "Ringo" does kill. Perez swears revenge and faces off with "Ringo". Juanito has formed an alliance with the local Indian chief and is now prepared for an all out war against the peaceful town and sheriff that are protecting Ringo.

==Cast==
* Mark Damon as Johnny Oro / Ringo
* Valeria Fabrizi as Margie
* Franco De Rosa as Juanito Perez 
* Giulia Rubini as Jane Norton
* Loris Loddi as Stan Norton
* Andrea Aureli as Gilmore
* Pippo Starnazza as Matt
* Ettore Manni as Sheriff Bill Norton
* John Bartha as Bernard 

==References==
 

==External links==
* 

 

 
 
 
 
 


 
 