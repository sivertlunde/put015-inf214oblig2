Andari Bandhuvaya
{{Infobox film
| name           = Andari Bandhuvaya
| image          =
| alt            =  
| director       = Chandra Siddhartha
| producer       = Chandra Siddhartha   V RamaKrishna
| writer         = Balabhadrapatruni Ramani
| lyricist       = Chaitanya Prasad
| Editing        = Nandamuri Hari Padma Priya Naresh  Jeeva
| music          = Anoop Rubens
| cinematography = Gummadi Jayakrishna
| banner         = Filmotsav, Uthopia Entertainers
| distributor    = Blue Sky
| released       =  
| Running Time   = 150 minutes
| country        = India
| language       = Telugu
}} Akkineni Award Chandra Siddarth. Andari Banduvaya. It is a low budget movie which did well overseas. At box office it couldnt get good collections initially but it met slow success. It was dubbed into Tamil as Gokulam.

==Plot== Padma Priya) belongs to a poor family and feeds her family with a normal job. She is optimist and straight forward. Accidentally she visits heros village. She watches values of village. Her behavior and conduct changes.

==Cast==
*Sharwanand as Nandu Padma Priya as Paddu Naresh as Nandus father
*Vijay Sai as Nandus friend
* V. Ramakrishna as Jangaiah  NRI
*M S Narayana
*Pragathi (actress)| Pragathi as Paddus mother
* Priyanka as Paddus younger sister 
* Aryan as NRI Surya
*Krishna Bhagawan Jeeva
*Anand

==Soundtrack==
The music was scored by Anoop Rubens
* "Mallee Malle Rammanee "  (Lyrics: Chaitanya Prasad ; Singer: Deepti Madhuri, Anoop Rubens)
* "Roju Roju Ninne"  (Lyrics: Chaitanya Prasad ;  Singers: Anoop Rubens, Deepti Madhuri,Sravani)
* "Jigi Bigi Chilakaa"  (Lyrics: Chaitanya Prasad ; Singers: Karunya, Pranavi, Dileep, Noel )
* "Soorydu Aevarayya"  (Lyrics: Chaitanya Prasad ; Singers: Amruthavarshini, Anoop Rubens, Sravani, Jai Srinivas, Aishwarya )
* "Jaama Chettuki Jaama Kaayalu"  (Lyrics: Chaitanya Prasad ; Singers: Sharwanand, Padmapriya )
* "Malle Mallee Rammanee (Male)"  (Lyrics: Chaitanya Prasad ; Singers: Anoop Rubens )
* "Nandaamaya"  (Lyrics: Chaitanya Prasad ;Singers: Sravana Bhargavi, Sreeram Chandra, Bhargavi )

==Awards==
Nandi Award for Akkineni Award for Best Home-viewing Feature Film - Chandra Siddhartha

==External links==
*  

 
 