Thiruvonam (film)
{{Infobox film
| name           = Thiruvonam
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       = KP Mohanan
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi Sharada Kaviyoor Ponnamma Kamalahasan
| music          = M. K. Arjunan
| cinematography = TN Krishnankutty Nair
| editing        = K Sankunni
| studio         = Vandana
| distributor    = Vandana
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed by  Sreekumaran Thampi and produced by KP Mohanan. The film stars Prem Nazir, Sharada (actress)|Sharada, Kaviyoor Ponnamma and Kamalahasan in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Prem Nazir Sharada
*Kaviyoor Ponnamma
*Kamalahasan
*Sreelatha Namboothiri
*T. S. Muthaiah
*Chandraji
*Jayasudha
*K. P. Ummer
*MG Soman Sujatha
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aa Thrisandhyathan || K. J. Yesudas, Chorus || Sreekumaran Thampi ||
|-
| 2 || Ethra Sundari || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 3 || Kaattinte Vanchiyilu || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 4 || Pachanellin Kathiru || P Jayachandran, P. Madhuri || Sreekumaran Thampi ||
|-
| 5 || Thaaram Thudichu || P Jayachandran || Sreekumaran Thampi ||
|-
| 6 || Thiruvonappularithan || Vani Jairam || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 