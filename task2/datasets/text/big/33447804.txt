Port Whines
{{Infobox Hollywood cartoon|
| cartoon_name = Port Whines
| series = Krazy Kat
| image =
| caption =
| director = Ben Harrison Manny Gould
| story_artist = Ben Harrison George Herriman
| animator = Allen Rose Harry Love
| voice_actor =
| musician = Joe de Nat
| producer = Charles Mintz
| distributor = Columbia Pictures
| release_date = October 10, 1929
| color_process = Black and white
| runtime = 6:31 English
| preceded_by = Canned Music
| followed_by = Soul Mates
}}

Port Whines is an animated short film by the Columbia Pictures Corporation. It is also the 136th Krazy Kat cartoon.

==Plot==
The story starts with a galleon sailing across the ocean. On it, Krazy is a seaman whose routines include mopping the main deck. After cleaning, he goes for a stroll around the vessel. As he continues moving, Krazy accidentally bumps into the captain who is quite irritable.

The captain starts chasing Krazy, only to slip on a soap bar and get airborne. Very quickly, the other seamen come in and cushion his fall. Remembering how unpleasantly ill-tempered their leader is, however, the seamen hurl the captain off the ships side where he plunges into the water. Following that, they and Krazy begin to celebrate the deed with some music and dancing.

In a kitchen levelled with the main deck, an Oriental chef was tries to fry fish on skillets but notices the place is short in supply. The chef comes outside and orders Krazy to do some fishing. Krazy takes a pole and casts a line. Moments later, something is caught, and Krazy starts to reel in. It turns out what is pulled in is the captain. The chef, who isnt very happy with the result, tries to punch Krazy but misses as the cat ducks. Instead the captain is the one struck. The captain then hits back in a similar fashion. For the rest of the film, the captain and the chef punch each other repeatedly, and Krazy is enjoying the sight.

==Availability==
*Columbia Cartoon Collection: Volume 1. {{cite web
|url=http://theshortsdepartment.webs.com/columbiacartoons.htm
|title=The Columbia Cartoons
|accessdate=2012-06-17
|publisher=the shorts development
}} 

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 