The Brave Ones
 
{{Infobox film
| name           = The Brave Ones
| image          = 
| caption        = 
| director       = Will Louis
| producer       = Louis Burstein
| writer         = 
| narrator       = 
| starring       = Oliver Hardy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  English intertitles
| budget         = 
}}
 silent comedy film featuring Oliver Hardy.

==Plot==
After they are caught stealing food, the sheriff (Billy Bletcher) sentences Plump (Oliver Hardy) and Runt (Billy Ruge) to spend the night in a haunted house.  They are not alone, though, as the house is actually a hideout for a gang of counterfeiters.

==Cast==
* Oliver Hardy - Plump (as Babe Hardy)
* Billy Ruge - Runt
* Billy Bletcher - The Sheriff
* Elsie MacLeod - The Sheriffs daughter (as Elsie McLeod)

==See also==
* List of American films of 1916
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 

 