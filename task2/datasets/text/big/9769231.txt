Le batteur du Boléro
 
{{Infobox film
| name           = Le Batteur du Boléro
| image          = 
| image_size     = 
| caption        = 
| director       = Patrice Leconte
| producer       = 
| writer         = Patrice Leconte
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1992
| runtime        = 8 minutes
| country        = France
| language       = None
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Le Batteur du Boléro is a short film by Patrice Leconte, released in 1992.  It features a drummer in an orchestra, played by Jacques Villeret, who plays a simple, repetitive rhythm on his single drum during a performance of Maurice Ravels Boléro.  

The focal point of the film is the drummer’s facial expressions.  He has a bland expression at first, but as the film develops, he starts twitching his eyes, mouth and eyebrows. This technique is used to show the performers frustration with the monotony of the piece. When the drummer’s mind is elsewhere, he starts to get out of rhythm. He shows his discomfort by twitching, indicating that his back hurts, and his continuous muttering.

When a female timpanist starts her part, he looks enviously at her and mutters every time he looks back at her. “Displeasure at being overshadowed by a bigger drum,” the director comments on the commentary.

The film was screened out of competition at the 1992 Cannes Film Festival.   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 