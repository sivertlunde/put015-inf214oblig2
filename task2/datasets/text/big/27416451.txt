Basta't Kasama Kita (film)
{{multiple issues|
 
 
}}

{{Infobox film|
name           = Bastat Kasama Kita |
image          =|
caption        =  |
director       = Rory Quintos |
producer       =  |
writer         =  |
starring       = Aga Muhlach Dayanara Torres |
music          =  |
cinematography =  |
editing        =  |
distributor    = Star Cinema |
released       =   |
runtime        = |
country        = Philippines | English  Tagalog |
budget         =  |}}
 Filipino film produced by Star Cinema, directed by Rory Quintos. The plot is similar to that of the 1953 film Roman Holiday, which starred Audrey Hepburn.

==Background==
Princess Isabella (Dayanara Torres), who comes from an unspecified country, is on a much-publicised royal visit to the Philippines. Feeling trapped in a world she can no longer stand, she escapes from her guards, and accidentally bumps into Alex (Aga Muhlach), a jeepney driver. While hiding her identity, they eventually fall in love. Can this fairy tale romance end "happily ever after"?

==Cast==
*Aga Muhlach as Alex
*Dayanara Torres as Ella
*Paolo Contis as Paolo
*Smokey Manaloto as Brix
*Ruby Rodriguez as Sara
*Heather Rasch as Ms. Thompson
*Megan Herrera as Aunt Belle
*Bill Campbell as Chief of Security
*James Slowey as King of Bavaria
*Tony Carreon as Minister of Foreign Affairs
*Pocholo Montes as Mr. Garcia
*Gamaliel Viray as Police Officer (as Gammy Viray)
*Vangie Labalan as Carinderia Owner
*Josie Tagle as Minda
*Joe Jardy as Neighbor (as Joe Hardy)
*Babalu as Sgt. Baba  (Uncredited) 
*Josh Fernandez as Boy at the Convention 1  (Uncredited) 
*Jhake Vargas as Boy at the Convention 2  (Uncredited) 
*Jojo Acosta as Boy at the Convention 3  (Uncredited) 
*Liza Tan as Girl at the Convention 1  (Uncredited) 
*Anna Rosal as Girl at the Convention 2  (Uncredited) 

==External links==
* 

 
 
 
 
 
 

 