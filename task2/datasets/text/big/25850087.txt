List of Malayalam films of 1993
 

The following is a list of Malayalam films released in the year 1993.

{| class="wikitable sortable"
!  width="25%" | Title
!  width="17%" | Director
!  width="15%" | Story
!  width="15%" | Screenplay
!  width="30%" | Cast
|- valign="top"
| Venkalam
| Bharathan
| A. K. Lohithadas
| A. K. Lohithadas
| Murali (Malayalam actor)|Murali, Urvashi (actress)|Urvashi, Manoj K. Jayan
|- valign="top"
| Sthalathe Pradhana Payyans
| Shaji Kailas
| Renji Panicker
| Renji Panicker Siddique
|- valign="top"
| Vakkeel Vasudev
| P. G. Viswambaran
| Rafeeq
| B Jayachandran Sunitha
|- valign="top"
| Ente Sreekuttikku
| Jose Thomas
| &nbsp;
| &nbsp;
| Mukesh (actor)|Mukesh, Thilakan, Maathu
|- valign="top"
| Dhruvam
| Joshy
| &nbsp;
| S. N. Swamy
| Mammootty, Gauthami, Jayaram, Rudra

|- valign="top"
| Aayirappara
| Venu Nagavally
| &nbsp;
| &nbsp;| Urvashi
|- valign="top"
| Ponnu Chami Ali Akbar
| &nbsp;
| &nbsp; Vijayaraghavan
|- valign="top"
| Aakasha Dhooth
| Sibi Malayil
| &nbsp;
| A. K. Lohithadas Madhavi
|- valign="top"
| Ghoshayaathra
| G. S. Vijayan
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Injakkadan Mathai & Sons
| Anil Babu
| &nbsp;
| &nbsp; Urvashi
|- valign="top"
| Cheppadividya
| G. S. Vijayan
| &nbsp;
| &nbsp;
| Sudeesh, Monisha
|- valign="top"
| Ottayadippathakal
| C. Radhakrishnan
| &nbsp;
| &nbsp;
| Revathy, Madhu

|- valign="top"
| Thiraseelakku Pinnil
| P. Chandrakumar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Agnishalabhangal
| P. Chandrakumar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Mithunam (1993 film)|Mithunam
| Priyadarsan
| &nbsp;
| &nbsp; Urvashi
|- valign="top"
| Aalavattam (film)|Aalavattam
| Raju Ambaran
| &nbsp;
| &nbsp;
| Nedumudi Venu, Innocent (actor)|Innocent, Shanthi Krishna, Sreenivasan, Jagathy Sreekumar
|- valign="top"
| Valtsalyam
| Cochin Haneefa
| &nbsp;
| A. K. Lohithadas
| Mammootty, Geetha (actress)|Geetha, Sunitha (actress)|Sunitha, Sidhik
|- valign="top"
| Devasuram
| I. V. Sasi
| &nbsp; Ranjith
| Napoleon
|- valign="top"
| Ammayane Sathyam
| Balachandra Menon
| &nbsp;
| &nbsp; Annie
|- valign="top"
| Paithrukam
| Jayaraj
| &nbsp;
| &nbsp; Geetha

|- valign="top"
| Kulapathy
| Nahas
| &nbsp;
| &nbsp;
| Vijayakumar (actor)|Vijayakumar, Anusha (actress)|Anusha, Ashokan (actor)|Ashokan, KPAC Lalitha, Prem Kumar
|- valign="top"
| Pravachakan
| P. G. Viswambharan
| &nbsp;
| &nbsp; Siddique
|- valign="top"
| Sthreedhanam
| Anil Babu
| &nbsp;
| &nbsp; Urvashi
|- valign="top"
| Ghazal (film)|Ghazal Kamal
| &nbsp;
| &nbsp;
| Vineeth, Mohini
|- valign="top"
| Samagamam
| George Kithu
| &nbsp;
| &nbsp; Rohini
|- valign="top"
| Jackpot (1993 film)|Jackpot
| Joemon
| &nbsp;
| &nbsp;
| Mammootty, Gouthami Tadimalla, Aishwarya (actress)|Aishwarya, Jagadish
|- valign="top"
| Ekalavyan (film)|Ekalavyan
| Shaji Kailas
| &nbsp;
| Renji Panicker Siddique
|- valign="top"
| Gandhari (film)|Gandhari Sunil
| &nbsp;
| &nbsp;
| Madhavi (actress)|Madhavi, Siddique (actor)|Siddique, Babu Antony, Jagathy Sreekumar, Rajan P. Dev, Charuhasan
|- valign="top"
| Journalist (1993 film)|Journalist
| Viji Thampi
| &nbsp;
| &nbsp; Siddique
|- valign="top" City Police
| Venu B. Nair
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Samooham
| Sathyan Anthikkad
| &nbsp;
| &nbsp;
| Sreenivasan, Suresh Gopi,  Manoj K Jayan,  Suhasini, Sunitha (actress)|Sunitha, Vineeth, Nedumudi Venu 
|- valign="top"
| Chamayam (1993 film)|Chamayam
| Bharathan
| &nbsp;
| &nbsp;
| Murali (Malayalam actor)|Murali, Manoj K Jayan, Meghanathan, Sithara (actress)|Sithara, Ranjitha
|- valign="top"]
| Kanyakumariyil Oru Kavitha
| Vinayan
| &nbsp;
| &nbsp;
| Vineeth, Rajan P. Dev, Suchithra
|- valign="top"
| Sarovaram (film)|Sarovaram
| Jeassy
| &nbsp;
| &nbsp;
| Mammootty, Thilakan, Narendra Prasad, V. K. Sreeraman, Ashokan (actor)|Ashokan, Mala Aravindan, Jayasudha
|- valign="top"
| Bandhukkal Sathrukkal
| Sreekumaran Thampi
| &nbsp;
| &nbsp;
| Mukesh (singer)|Mukesh, Jayaram, Narendra Prasad, Jagathy Sreekumar, Thilakan, Rohini (actress)|Rohini, Rupini (actress)|Rupini, KPAC Lalitha
|- valign="top"
| Butterflies (1993 film)|Butterflies
| Rajeev Anchal
| &nbsp;
| &nbsp;
| Mohanlal, Aishwarya (actress)|Aishwarya, Jagadish
|- valign="top"
| Maya Mayooram
| Sibi Malayil
| &nbsp;
| &nbsp;
| Mohanlal, Shobana, Revathi
|- valign="top"
| Gandharvam
| Sangeeth Sivan
| &nbsp;
| &nbsp; Kalpana
|- valign="top"
| Addeham Enna Iddeham
| Viji Thampi
| &nbsp;
| &nbsp;
| Jagadish, Siddique (actor)|Siddique, Sunitha (actress)|Sunitha, Maathu
|- valign="top"
| O Faby
| Sreekuttan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Customs Diary
| T. S. Suresh Babu
| &nbsp;
| &nbsp;
| Jayaram, Mukesh (actor)|Mukesh, Jagathy Sreekumar, Mohanraj, Ranjitha
|- valign="top"
| Oru Kadankatha Pole
| Joshy Mathew
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Aagneyam
| P. G. Viswambharan
| &nbsp;
| &nbsp;
| Jayaram, Gauthami, Sunitha (actress)|Sunitha, Maathu
|- valign="top"
| Narayam (film)|Narayam
| Sasi Sankar
| &nbsp;
| &nbsp; Urvashi
|- valign="top"
| Uppukandam Brothers
| T. S. Suresh Babu
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Mafia (1993 film)|Mafia
| Shaji Kailas
| &nbsp;
| &nbsp;
| Babu Antony, Suresh Gopi, Geetha (actress)|Geetha, Vijayaraghavan, Ganeshan, Soman, Prathapa Chandran, Ranjitha
|- valign="top"
| Meleparambil Aanveedu
| Rajasenan
| &nbsp;
| Regunath Paleri
| Jayaram, Shobana, Jagathy, Janardhanan, Vijayaraghavan (actor)|Vijayaraghavan, Narendra Prasad
|- valign="top"
| Padheyam
| Bharathan
| &nbsp;
| A. K. Lohithadas Sasikala
|- valign="top"
| Koushalam
| T. S. Mohan
| &nbsp;
| &nbsp;
| Siddique (actor)|Siddique, Urvashi, Swetha Menon
|- valign="top"
| Chenkol
| Sibi Malayil
| &nbsp;
| A. K. Lohithadas
| Mohanlal, Thilakan, Sreenath, Cochin Haneefa, Shanthi Krishna, Kaviyoor Ponnamma
|- valign="top"
| Golanthara Vartha
| Sathyan Anthikkad
| &nbsp;
| &nbsp; Kanaka
|- valign="top"
| Sakshal Sreeman Chathunni
| Anil Babu
| &nbsp;
| &nbsp;
| Innocent (actor)|Innocent, Jagadish, Baiju (actor)|Baiju, Maathu
|- valign="top"
| Sowbhagyam
| Sandhya Mohan
| &nbsp;
| &nbsp;
| Sunitha (actress)|Sunitha, Jagadish, Jagathy Sreekumar, Suchithra
|- valign="top"
| Yaadhavam
| Joemon
| &nbsp;
| &nbsp;
| Suresh Gopi, Kushboo,  Narendra Prasad
|- valign="top"
| Kalippattam
| Venu Nagavally
| &nbsp;
| &nbsp;
| Mohanlal, Urvashi (actress)|Urvashi, Vineeth
|- valign="top"
| Janam (film)|Janam
| Viji Thampi
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Vidheyan
| Adoor Gopalakrishnan
| &nbsp;
| &nbsp;
| Mammootty, Gopakumar M R, Soman P C, Sabitha Anand, Thanvi Azmi
|- valign="top"
| Thalamura
| K. Madhu
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Bhoomi Geetham Kamal
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Magrib (film)|Magrib
| P. T. Kunju Muhammed
| &nbsp;
| &nbsp;
| Murali (Malayalam actor)|Murali, Sreenivasan, V. K. Sreeraman, Saranya, Zeenath, Santha Devi
|- valign="top"
| Swaham
| Shaji N. Karun
| &nbsp;
| &nbsp; Annapoorna
|- valign="top"
| Aacharyan Asokan
| &nbsp;
| &nbsp;
| Thilakan, Sreenivasan
|- valign="top"
| Shudhamaddalam
| Thulasidas
| &nbsp;
| &nbsp;
| Mukesh (actor)|Mukesh, Madhurima
|- valign="top"
| Ethu Manju Kaalam
| Thulasidas
| &nbsp;
| &nbsp;
| Suresh Gopi, Urvashi
|- valign="top"
| Varaphalam
| Thaha
| &nbsp;
| &nbsp;
| Mukesh (actor)|Mukesh, Mathu
|- valign="top"
| Arthana
| I. V. Sasi
| &nbsp;
| &nbsp;
| Murali (Malayalam actor)|Murali, Radhika, Priya Raman
|- valign="top"
| Sopanam
| Jayaraj
| &nbsp;
| &nbsp;
| Manoj K. Jayan, Chippi
|- valign="top"
| Manichitrathazhu Fazil
| &nbsp;
| Madhu Muttom
| Mohanlal, Suresh Gopi, Shobana, Vinaya Prasad
|- valign="top"
| Ponthan Mada
| T. V. Chandran
| &nbsp;
| &nbsp;
| Mammootty, Naseeruddin Shah, V. K. Sreeraman, Laboni Sarkkar, Sreeja, Reshmi
|- valign="top"
| Padaliputhram
| Baiju Thomas
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Paalayam
| T. S. Suresh Babu
| &nbsp;
| &nbsp;
| Jagadish, Urvashi
|- valign="top"
| Thaali (film)|Thaali Sajan
| &nbsp;
| &nbsp;
| &nbsp;

| Mukesh (Malayalam actor)|Mukesh, Baiju (actor)|Baiju, Thilakan, Saranya
|- valign="top"
| Avan Ananthapadmanabhan
| Prakah Koleari
| &nbsp;
| &nbsp;
| Sudha Chandran
|- valign="top"
| Bhagyavan
| Suresh Unnithan
| &nbsp;
| &nbsp; Sithara
|- valign="top"
| Kabooliwala
| Siddique Lal
| &nbsp;
| &nbsp;
| Innocent (actor)|Innocent, Jagathy, Vineeth
|- valign="top"
| Nandini Oppol
| Mohan Kulperi
| &nbsp;
| &nbsp;
| Geetha (actress)|Geetha, Sunitha (actress)|Sunitha, Siddique (actor)|Siddique, Nedumudi Venu
|- valign="top"
| Johny (film)|Johny
| Sivan
| &nbsp;
| &nbsp;
| Master Tharun Kumar, Shanthi Krishna
|- valign="top"
| Kavadiyattam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Aparna (1993 film)|Aparna
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Porutham
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Eeswaramurthi In
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Theeram Thedunna Thirakal
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Meghasangeetham
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|}

==Dubbed films==
{| class="wikitable sortable"
!  width="25%" | Title
!  width="17%" | Director
!  width="15%" | Story
!  width="15%" | Screenplay
!  width="30%" | Cast
|- valign="top"
|- valign="top"
| Pranavam (film)|Pranavam
| K. Viswanath
| &nbsp;
| &nbsp;
| Mammootty, Radhika
|- valign="top"
| Vaasarashayya
| G. S. Panikker
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Vairam
| Uma Balan
| &nbsp;
| &nbsp; Geetha
|}

==References==
 
* 
* 
* 

 
 
 

 
 
 
 
 