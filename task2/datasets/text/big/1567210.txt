Swept Away (2002 film)
 
{{Infobox film
| name           = Swept Away
| image          = Swept away.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Guy Ritchie
| producer       = Matthew Vaughn
| writer         = Guy Ritchie Madonna Adriano Giannini Bruce Greenwood Jeanne Tripplehorn Elizabeth Banks
| music          = Michel Colombier
| cinematography = Alex Barber
| editing        = Eddie Hamilton
| distributor    = Medusa Distribuzione   Screen Gems  
| released       =  
| runtime        = 89 minutes
| country        = United States United Kingdom Italy
| language       = English Greek Italian
| budget         = $10 million 
| gross          = $1,036,520 
}} remake of 1974 film of the same name. It received negative reviews and was a box office bomb.

==Plot==
Amber Leighton is 40: beautiful, rich, spoiled, foul-mouthed and arrogant beyond measure. Nothing makes this woman happy, including her wealthy but passive husband, Tony, a pharmaceutical kingpin.

When Tony takes her on a private cruise from Greece to Italy with two other couples, Amber is unimpressed by this impromptu no-frills vacation and takes out her anger on the ships first mate, Giuseppe Esposito. When a storm leaves the two shipwrecked on a deserted island, however, the tables suddenly turn, with Giuseppe gaining the upper hand, followed by the two falling in love.

==Cast== Madonna as Amber Leighton
* Adriano Giannini as Giuseppe Esposito
* Bruce Greenwood as Tony Leighton
* Elizabeth Banks as Debi
* Jeanne Tripplehorn as Marina
* Michael Beattie as Todd David Thornton as Michael

==Production==
The films working title was Love, Sex, Drugs and Money  and was filmed in Sardinia and Malta from 1 October 2001 until 9 November 2001 with security increased due to the September 11 attacks|9/11 terrorist attacks.  Madonna had only finished her 2001 Drowned World Tour two weeks prior to filming. Giancarlo Gianninis son Adriano Giannini plays his original film role.

==Release==
===Critical reception===
Swept Away received negative reviews from critics; it currently holds a 5% rating on Rotten Tomatoes. It holds an 18 out of 100 on Metacritic.   Roger Ebert, who called the original Swept Away an "absorbing movie" that he bestowed with a 4-out-of-4 star rating,    gave the remake only 1 star.  According to Ebert, despite Ritchies relatively faithful adaptation, the original Swept Away was "incomparably superior," and the remakes fatal flaw was the "utterly missing" vitality or emotional resonance of the main characters. Additionally, wrote Ebert, Madonnas character "starts out so hateful that she can never really turn it around" and gain any redemption or believable change. Similarly, A.O. Scott  of the New York Times wrote, "In her concerts, music videos and recordings, Madonna has often been a mesmerizing performer, but she is still not much of an actress. Striking a pose is not the same as embodying a person, and a role like this one requires the surrender of emotional control, something Madonna seems constitutionally unable to achieve."
 his first two films. Thats how the media is: eventually they have to pull you down."  When the studio screened the film for Lina Wertmüller, it is alleged that Wertmüller left the theatre at the end crying out, "What did they do to my movie? Why   they do this?"

===Box office===
The film was a box office bomb; from a $10 million budget, it grossed $598,645 in the United States. And Foreign 437,875     It was shown only on 196 screens for two weeks, dropping down to 59 in the final third week of release. In Italy, it grossed €71,575 and in Spain €105,371 from 174 screens. 

===Accolades=== 2002 Golden Raspberry Awards: Worst Picture Worst Actress - Madonna Worst Screen Couple - Madonna and Giannini Worst Remake or Sequel Worst Director - Guy Ritchie
 Dangerous Game."  Madonna even won the Worst Supporting Actress award that same year (for Die Another Day).

==Soundtrack==
 
{{Infobox Album  
| Name        = Original Motion Picture Soundtrack: Swept Away
| Type        = soundtrack
| Artist      = Michel Colombier
| Cover       =
| Cover size  = 
| Released    =  
| Recorded    = Pop
| Length      = 43:28
| Label       = Varèse Sarabande
| Producer    =
| Chronology  = Guy Ritchie film soundtracks
| Last album  = Snatch (film)#Soundtrack|Snatch (2000)
| This album  = Swept Away (2002)
| Next album  = Revolver (2005 film)#Soundtrack|Revolver (2005)
}} score to Come On-a My House", sung by Della Reese, is the only one featured on the album.

Songs from the film not featured on the album include "Lovely Head" by Goldfrapp (played during the opening credits), "Aint Nobody Here but Us Chickens" by Louis Jordan (the charades scene), and "Fade into You" by Mazzy Star (as Amber and Pepe experience life on the island together). Arvo Pärts "Spiegel im Spiegel" plays during the closing moments and end credits of the film.

==Home media==
  theatrical trailers, and filmographies.

==See also==
* 2002 Golden Raspberry Awards

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 