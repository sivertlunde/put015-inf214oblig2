Honey (2003 film)
{{Infobox film
| name           = Honey
| image          = Honey 2003 film poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Bille Woodruff
| producer       = {{Plainlist| Marc Platt
* Andre Harrell
}}
| writer         = {{Plainlist|
* Alonzo Brown
* Kim Watson
}}
| starring       = {{Plainlist|
* Jessica Alba
* Mekhi Phifer
* Joy Bryant
* Romeo Miller|Lil Romeo 
}}
| music          = Mervyn Warren
| cinematography = John R. Leonetti
| editing        = {{Plainlist| Mark Helfrich
* Emma E. Hickox
}}
| studio         = NuAmerica Entertainment
| distributor    = Universal Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $18 million 
| gross          = $62,228,395 
}}

Honey is a 2003 dance film released by Universal Pictures. Featuring music produced by Rodney Jerkins, the film stars Jessica Alba, Mekhi Phifer, Lil Romeo (rapper)|Lil Romeo, Joy Bryant, David Moscow and features performances by Tweet (singer)|Tweet, Jadakiss and Ginuwine. It also features a cameo by Missy Elliott. Honey was followed by a sequel, Honey 2, released on June 10, 2011.

==Plot== Zachary Williams). Honey invites them to attend her classes at the community center, and they work together to inspire new dance moves. The young teacher soon catches the attention of music director Michael Ellis (David Moscow), who gives her a job as a backup dancer in Jadakiss new video. Unimpressed with his current choreographer, Michael decides to let Honey choreograph the video. Impressed, Honey gets promoted, and choreographs for Tweet, Sheek Louch, and Shawn Desman.
 Pied Piper. Ginas 25th birthday comes up, and Honey makes plans to take her to Atlantic City, but Michael convinces her to make an appearance at a black and white party instead, where he subsequently makes a drunken pass at her. She refuses his advances, slaps him and Michael begins a major outburst so she leaves the party.
 blackballs her from the business. Honeys friendship with Gina, although strained, improves, and she helps Honey realize she can still make her dreams come true.

Honey struggles to pay the down payment on the studio, and eventually comes up with an idea to raise revenue. She holds a dance benefit, using an abandoned church scheduled for demolition, for the event. She spends time getting to know Raymond and Benny, and is shocked to see Benny going down the wrong track  and visits him in prison. Benny refuses her help and insults her. Honey informs him that his mother knows and he will be getting bailed out. Honey tries to tell him how she feels about him being in jail, but he tells her to go. She does manage to ask him how often his friends visit and he is visibly saddened by her question, realizing none visit him at all and leaves him to thinking.

In the benefits  has a new video for Missy Elliott to choreograph, Michael suggests that she gives Katrina the chance to do so. She refuses and insists on having Honey choreograph it. Once done watching the impromptu choreograph from Katrina, Missy tells both her and Michael that shes unimpressed with their arrogance and sexy ideas. She makes it clear that she will postpone the filming of her video until Michael fires Katrina and brings Honey to choreograph it. Realizing his mistake, Michael goes crawling back to Honey, begging her to work for him and even offers to buy her the studio. She refuses, saying she will do it on her own. She also tells him that hes as arrogant and selfish as Katrina in the way he behaved from an earlier job that not only got her fired, but also made her break a lot of her students hearts.
 Tweet also attends, and joins Honey on stage to celebrate her victory. Missy Elliott arrives around the time the benefit finishes, cursing her driver, saying she will have him fired if hes the reason she cant meet Honey. The credits roll, and feature Honeys successful music video for Blaque with Missys approval.

==Cast==
* Jessica Alba as Honey Daniels
* Mekhi Phifer as Chaz
* Joy Bryant as Gina
* Romeo Miller|Lil Romeo as Benny
* David Moscow as Michael Ellis
* Zachary Isaiah Williams as Raymond
* Missy Elliott as Herself
* Laurieann Gibson as Katrina
* Ginuwine as Himself
 hip hop and Contemporary R&B|R&B musicians make cameos in the film, including producer Rodney Jerkins, Jadakiss and Sheek Louch of The Lox|D-Block, Tweet (singer)|Tweet, and Ginuwine. Canadian R&B artist Shawn Desman was also featured.

==Production==
The film is inspired by the life of choreographer  ). April 8, 2011.  she also appears in the film as the main characters rival, Katrina, and worked as the films choreographer. 
	 
Singer/actress Aaliyah was originally cast as Honey, though the role was later recast to Jessica Alba due to Aaliyahs death in August 2001. 

==Reception==

===Critical response===
 
Honey was released to mostly negative reviews. 
Rotten Tomatoes gives film a score of 20% of based on reviews from 112 critics, with an average rating of 4.1 out of 10. The critical consensus was "An attractive Jessica Alba and energetic dance numbers provide some lift to this corny and formulaic movie". {{cite web 
| url = http://www.rottentomatoes.com/m/honey/ 
| title = Honey 
| work = Rotten Tomatoes
| publisher = Flixster
}} 
 
Metacritic gives the film as score of 36% based on 30 reviews. {{cite web
| title = Honey
| url = http://www.metacritic.com/movie/honey
| work = Metacritic 
| publisher = CBS
}}
 

A. O. Scott of The New York Times gives the film a positive review, noting that it "brings out the wholesome, affirmative side of the hip-hop aesthetic without being overly preachy", although it will not impress anyone with its originality. 
{{cite news 
| title = Honey (2003)
| date = December 5, 2003
| title = FILM REVIEW; Shes Aiming for the Stars, With Feet Planted in the Bronx
| author = A. O. SCOTT
| url = http://movies.nytimes.com/movie/review?res=9C05EFD6173DF936A35751C1A9659C8B63
| work = New York Times
}}
 

===Box office===
The film opened at #2 at the U.S. box office, raking in United States dollar|US$12,856,040 in its first opening weekend, behind The Last Samurai. The film made $30,308,417 in the U.S. and Canada and $31,919,978 in other countries, for a total of $62,228,395 worldwide.   

==Music==
  
 
A soundtrack containing Hip Hop music, R&B, Funk and Disco  music was released on November 11, 2003 by Elektra Records. 
It peaked at #105 on the Billboard 200|Billboard 200 and #47 on the Top R&B/Hip-Hop Albums.

==Sequel==
 
A sequel starring Katerina Graham and Randy Wayne was released on 1 August 2011.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 