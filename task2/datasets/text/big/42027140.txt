Supercondriaque
{{Infobox film
| name           = Supercondriaque
| image          = Supercondriaque poster.jpg
| caption        = Theatrical release poster
| director       = Dany Boon
| producer       = {{plainlist|*Eric Hubert  
*Jérôme Seydoux}} 
| writer         = Dany Boon 
| starring       = {{plainlist|*Dany Boon
*Alice Pol
*Kad Merad
*Jean-Yves Berteloot
*Judith El Zein
*Marthe Villalonga
*Valérie Bonneton
*Bruno Lochet}}
| music          = Klaus Badelt
| cinematography = Romain Winding
| editing        = Monica Coleman
| studio         = Pathé Canal+
| distributor    = Pathé
| released       =  
| runtime        = 107 minutes
| country        = {{plainlist|*France
*Belgium}}
| language       = French
| budget         = $31 million  
| gross          =   (international)
}}

Supercondriaque (also known as Superchondriac)   is a 2014 French comedy film written and directed by Dany Boon.  

==Plot==
Romain Faubert is a mature man who can never hide his hypochondriasis. Romains fears are profitable for his doctor Dimitri Zvenka. Even so, Dimitri really wants to cure the patient who has no other friend than him. He feels that Romains actual problem is his loneliness rather than anything else. He subsequently helps Romain in seeking an appropriate female companion, but after a great many futile attempts, he loses hope that Romain could ever succeed. In need of an alternative, he decides to take Romain with him when he goes to an eastern European refugee camp (refugees speak, in fact, Ukrainian language|Ukrainian), where Dimitri sometimes works on behalf of a non-profit organisation. He believes the sight of people who are really suffering might bring Romain to his senses. Yet Romain finally finds the love of his life when he gets to know Dimitris sister who confuses him with Anton Miroslav, a certain freedom fighter. The real Anton Miroslav has stolen Romains ID and is hiding in the apartment of the hypochondriac.

==Cast==
* Dany Boon as Romain Faubert 
* Alice Pol as Anna Zvenka 
* Kad Merad as Dr. Dimitri Zvenka 
* Jean-Yves Berteloot as Anton Miroslav 
* Judith El Zein as Norah Zvenka 
* Marthe Villalonga as Dimitris mother
* Valérie Bonneton as Isabelle 
* Bruno Lochet as Policeman in the service of the immigration bureau
* Jérôme Commandeur as Guillaume Lempreur  Jonathan Cohen as Marc  
* Vanessa Guide as Manon   
* Marion Barby as Nina Zvenka

==Reception==
The film earned a total of   internationally. 

==See also==
* List of French films of 2014

==References==
 

==External links==
*   
*  

 

 
 
 
 
 
 
 
 
 
 

 
 