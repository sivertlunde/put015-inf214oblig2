Godavari (film)
 
 

{{Infobox film name           = Godavari image          = Godavari_poster.jpg caption        = Theatrical poster director       = Sekhar Kammula producer       = G. V. G. Raju writer         = Sekhar Kammula starring       = Sumanth Kamalinee Mukherjee Neetu Chandra music          = K. M. Radha Krishnan cinematography = Vijay C. Kumar editing        = Marthand K. Venkatesh distributor    = Amigos Creations(India)  KAD Entertainment(USA) released       = 19 May 2006  runtime        = 160 minutes country        = India language       = Telugu awards         = budget         = Indian rupee|Rs. 70&nbsp;million  (70&nbsp;million) (estimated)
}} Telugu Musical romantic film Nandi and Filmfare awards. Music of the film was composed by K.M. Radha Krishnan.

== Plot ==
Godavari is a film about Ram, Seetha and the river cruise  from Rajamundry to Bhadrachalam that brings them together. It is a semi-urban love story set against the scenic backdrop of the Godavari river aboard a houseboat also called "Godavari".

SriRam (Sumanth) is an idealistic man. He wishes to make a career in politics to serve people, even though hes a software engineer with a masters degree from the USA. He lives with his grandmother. Hes in love with his cousin, Raji, who he hopes to marry someday.Raji gets a marriage proposal from Ravi, an IPS officer.Rajis father says that he does not want Raji to marry Ram as he feels insecure about Rams professional and financial future.Even Raji says that shes not sure about her feelings for Ram.Ram says that he will prove himself and then marry Raji.He works on a project with a company and earns 1 lakh rupees.He is happy and excited, and rushes to share this news with Raji, but he realises that she has already agreed to marry Ravi and remains silent.A heartbroken but realistic Ram is forced to join her and his family on the river journey to Bhadrachalam, where Raji is to get married.

Seetha (Kamalinee) is a fashion designer who runs an unsuccessful clothing boutique. Her parents are not very keen about her business and want her to get married instead. Seetha, being an independent woman who wants to succeed in her life, is not enthusiastic about the idea; however, circumstances force her to reluctantly agree with her parents. Unfortunately, the marriage alliance doesnt work out for her and she subsequently chooses to take a break from everything by going on the same Godavari river journey.

Ram and Seetha meet aboard the cruise liner and from there starts a story where at first, they dont start off well, but develop a bond of friendship and get to understand each other.How they finally fall in love and accept each other forms the crux of the story.The story also reflects the characteristic culture of the Godavari region. There are also several sub stories involving the other characters on the house boat.

== Cast ==
* Sumanth ... Ram (Sree Ram)
* Kamalinee Mukherjee ... Seetha (Seetha Mahalakshmi)
* Neetu Chandra ... Raji (Rajeswari)
* Kamal Kamaraju ... Ravi (Ravindra)
* Tanikella Bharani ... Captain Chintamani
* Shiva ... Chinna
* Gangadhar Panday ... TDP Guy

==Crew==
* Director: Sekhar Kammula
* Screenplay: Sekhar Kammula
* Story: Sekhar Kammula
* Dialogue: Sekhar Kammula
* Producer: G.V.G Raju
* Music: K.M. Radha Krishnan
* Cinematography: Vijay C. Kumar
* Editor: Marthand K. Venkatesh
* Lyrics: Veturi Sundararama Murthy
* Art Direction:  Kishore Choksey
* Costume Designer: Aravind Joshua

==Soundtrack==
* Uppongele Godavari (Singer: S. P. Balasubramanyam)
* Andamgaa Lena Asalem Balena (Singer: Sunitha Upadrashta|Sunitha; Cast: Kamalini and Sumanth)
* Manasa Gelupu (Singers: Shankar Mahadevan and K. S. Chitra)
* Manasa Vacha (Singers: Unnikrishnan and K. S. Chitra)
* Rama Chakani Sita (Singer: Gayathri)
* Tippulu Tappulu (Singer: Shreya Ghoshal)

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 