Burglar Proof
{{Infobox film
| name           = Burglar Proof
| image          = Burglar Proof (1920) - 1.jpg
| alt            = 
| caption        = Film still
| director       = Maurice Campbell
| producer       = Jesse L. Lasky
| screenplay     = Thomas J. Geraghty Lois Wilson Grace Morse Emily Chichester Clarence Geldart Clarence Burton
| music          = 
| cinematography = Charles Edgar Schoenbaum
| editor         = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Comedy comedy film Lois Wilson, Grace Morse, Emily Chichester, Clarence Geldart, and Clarence Burton.     The film was released on November 21, 1920, by Paramount Pictures. Its survival status is classified as unknown,  which suggests that it is a lost film.

==Plot==
 

==Cast==
*Bryant Washburn as John Harlow Lois Wilson as Laura Lowell
*Grace Morse as Jenny Larkin
*Emily Chichester as Clara
*Clarence Geldart as Richard Crane
*Clarence Burton as Martin Green
*Tom Bates as Uncle Jim Harlow 
*Hayward Mack as George
*Blanche Gray as Mrs. Lowell 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 