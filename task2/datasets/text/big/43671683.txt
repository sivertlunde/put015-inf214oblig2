Bhoopathi Ranga
{{Infobox film
| name           = Bhoopathi Ranga
| image          = 
| caption        = 
| director       = Geethapriya
| producer       = Bhaskar Movies
| writer         = 
| screenplay     = Geethapriya
| story          = Bhaskar Movies
| based on       =  
| narrator       =  Rajkumar   Udaya Chandrika
| music          = Vijaya Bhaskar
| cinematography = Prakash
| editing        = Bal G. Yadav
| studio         = Bhaskar Movies
| distributor    = 
| released       = 1970 
| runtime        = 157 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}

Bhoopathi Ranga ( ) is a 1970 Indian Kannada language drama film written and directed by Geethapriya.  It stars Rajkumar (actor)|Rajkumar, Udaya Chandrika and Renuka in the lead roles.  The films story was written and produced by Bhaskar Movies. The future director H. R. Bhargava worked as an associate director in this film.

== Cast == Rajkumar 
* Udaya Chandrika
* Renuka Narasimharaju 
* Dinesh
* Ashwath Narayan
* Prem Dinakar
* C. R. Sampath Kumaran

== Soundtrack ==
The music of the film was composed by Vijaya Bhaskar and lyrics for the soundtrack written by Geethapriya.  The song "Hakkiyu Haaruthide" and the cabaret number "Rasika Rasika" are considered one of the most evergreen songs and popular songs in Kannada cinema.

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Nagareeka Manava"
| P. Susheela
|-
| 2
| "Hakkiyu Haaruthide"
| P. B. Sreenivas
|-
| 3
| "Oho Muddina Mallige"
| P. B. Sreenivas
|-
| 4
| "Rasika Rasika"
| L. R. Eswari
|-
| 5
| "Goodalli Thai Hakki"
| P. Susheela
|-
| 6
| "Hasuvina Veshada"
| P. B. Sreenivas
|-
|}

==See also==
* Kannada films of 1970

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 