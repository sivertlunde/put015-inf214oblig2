Running Scared (1980 film)
 Running Scared}}
{{Infobox Film |
  name           =Running Scared |
  image          = | 
  writer         =David Odell | John Saxon Bradford Dillman Pat Hingle Lonny Chapman Annie McEnroe  |
  director       =Paul Glickler |
  producer       =Paul Glickler |
 editing = Robert Q. Lovett |
  distributor    =Goodtimes Home Videos |
  released   = November 17, 1980 |
  runtime        = 90 min. |
  language =English|
  budget         =|}} 1980 action film directed by Paul Glickler and starring Ken Wahl, Judge Reinhold and introducing Annie McEnroe.

==Plot details== Infantry tour Army in John Saxon and Bradford Dillman) who are planning an invasion of Florida and the United States from the secret island base. The Communist leaders develop LeRoys negatives and now can visually ID Chaz.
 Ronnie Dawson ("Decided By The Angels") is playing and they encounter two young high school girls with a stationwagon. The boys go to a secluded spot with the girls and Sexual intercourse|score with the girls before they go off to school. However the boys are not off the plane too long before Saxons armed men, having identified them in the diner, are chasing and shooting at them in the night. LeRoy makes it out of the first ambush and gets back to his parents home. Chaz loses contact with him but survives the ambush. The next morning Chaz is searching for LeRoy when a fancy convertible driven by an attractive and rich young woman named Sallie Mae stops to give him a lift. Sallie Mae at first refuses to take Chaz to LeRoys house and he then threatens to cut up her car seats if she abandons him on the road. Sallie Mae then consents to take Chaz to LeRoys parents house where her car breaks down in the driveway. The commie soldiers track the men to LeRoys parent home, spot Sallies car, deep in the Everglades swamp where Mister Beecher (Lonny Chapman) runs an illegal Moonshine Still.

At this point a shootout occurs at the Beecher home, with Mister Beecher getting pissed off  that his Moonshine secret is out, and curses his son LeRoy. LeRoy angered, along with Chaz and Sallie Mae, steal one of Mister Beechers airboats and an exciting long chase continues through the swamp. Chaz later returns to his own fathers (Pat Hingle) house and encounters his old girlfriend. The girlfriend seeing Sallie Mae thinks that Chaz has impregnated Sallie Mae. Sallie Mae also returns with Chaz to her parents (who are vacationing in Europe) palatial estate and the two clean themselves up. After much adventure eluding Saxon, Dillman and their men, LeRoy and Chaz eventually dispose of Saxon and send Dillman on a one way trip to Cuba. A romance has developed between Chaz and the wealthy Sallie Mae.

==Notes==
This film is known under several titles for home video, cable TV and foreign market. Two are "Desperate Men" and "Back in the USA".

==Cast==
*Judge Reinhold - LeRoy Beecher
*Ken Wahl - Chaz McClain
*Annie McEnroe - Sally Mae Giddens
*Bradford Dillman - Arthur Jaeger John Saxon - Captain Munoz
*Pat Hingle - Sergeant McClain
*Lonny Chapman - Pa Beecher (*billed as Lonnie Chapman)
*Tom McFadden - Colonel Williams
*Tim Brantly - Kid in Jalopy
*Mary Lawson - Ponytail
*Lisa Felcoski - Sandy
*Francine Joyce - Robin Winston
*Malcolm Jones - Storekeeper
*Tom Tully - Pilot of C-47
*Roger Pretto - Commando Lieutenant

== External links ==
*  
* 

 
 
 
 
 
 