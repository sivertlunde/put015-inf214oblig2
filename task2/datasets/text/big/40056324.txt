Dysfunctional Friends
  }}

{{Infobox film
| name           = Dysfunctional Friends
| image          = Dysfunctional Friends movie poster.jpg
| caption        = Theatrical release poster
| director       = Corey Grant
| producer       = Datari Turner  Greg Carter  
| writer         = Corey Grant
| starring       = Stacey Dash Reagan Gomez-Preston Wesley Jonathan Datari Turner  Tatyana Ali Meagan Good Jason Weaver Persia White Terrell Owens Stacy Keibler Hosea Chanchez  Christian Keyes
| music          = Keith Robinson 
                   Ira Antelis
| cinematography = Richard J. Vialet
| editing        = Ralph Jean-Pierre
| studio         = Datari Turner Productions
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Dysfunctional Friends  is a 2012 American drama comedy film starring Stacey Dash, Reagan Gomez-Preston, Wesley Jonathan, Datari Turner, Tatyana Ali, Meagan Good, Jason Weaver, Persia White, Terrell Owens, Stacy Keibler, Hosea Chanchez and Christian Keyes. The film was released in theaters February 3, 2012.      

==Plot==

The unexpected death of a quirky and wealthy friend has forced nine former friends to reunite at the funeral of their friend; years after graduating college and going their separate ways. After the funeral they learn from Ms. Stevens (Good) that they are all eligible for a big inheritance if they can just spend five days together at his former estate in which she will oversee. The stipulations of their friends will is if anyone leaves before the five day period, everyone sacrifices their portion of the estate.  What first seems like a lighthearted reunion quickly turns for the worst as old wounds are reopened, and lingering grudges are resurrected. Many of the issues resurrected are created by Ebonys (Gomez-Preston) persistent eavesdropping. The unstable engagement between Lisa (Dash) and Jackson (Owens) is revealed as Jackson and Storm (Keibler) struggle to keep it a secret that they had a sexual relationship a few years ago when Jackson began to date Lisa. Storm didnt know they were in a relationship at the time and it increases the tension. Gary (Weaver) has become a porn director much to the disgust of all the women in the estate and is desperate to escape the porn industry.  However, he is reluctant to admit it and when he approaches Trenyce (White) with screenplay for a movie whom is a struggling actress she assumes its to do pornography which increases the tension in the household even more.  An attempt to have a peaceful dinner with everyone fails as tempers flare and dark secrets are revealed creating new wounds between them all. A few of the friends begin feeling trapped in a mansion with people who know their darkest secrets grows increasingly unbearable and forces them to reconcile their ways. Some of which truly finding their way realizing one of their greatest mistakes was abandoning their friends.  

==Reception==
Dysfunctional Friends is an award winning independent film produced by Datari Turner. The film premiered at South by Southwest (SXSW), as well as the American Black Film Festival (ABFF) and was nominated for nearly every major award category (with Persia White winning best actress at ABFF). The film was released theatrically in two markets (LA/NYC) and has become a cult hit, grossing over a million dollars on VOD & DVD. Additionally, the cast of Dysfunctional Friends has a combined digital fan base of over 10 million Twitter followers.

==References==
 

 
 

 

==External links==
*  

 
 
 
 