Living on Love
{{Infobox film
| name           = Living on Love
| image          =
| caption        =
| director       = Lew Landers
| producer       = Merian C. Cooper
| writer         = Franklin Coen John K. Wells James Dunn Whitney Bourne Joan Woodbury
| music          =
| cinematography =
| editing        =
| distributor    = RKO Radio Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $112,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p57 
| gross          = $135,000 
}}
Living on Love (1937 in film|1937) is a feature film released by RKO Radio Pictures. The film is a remake of the RKO film Rafter Romance (1933).

==Preservation status==
This is one of the "lost RKO films" owned by Merian C. Cooper and only re-released in April 2007 when Turner Classic Movies acquired the rights and showed all six films on TCM. 

Cooper accused RKO of not paying him all the money contractually due for the films he produced in the 1930s. A settlement was reached in 1946, giving Cooper complete ownership of six RKO titles: 
*Rafter Romance (1933) with Ginger Rogers
*Double Harness (1933) with Ann Harding and William Powell Robert Young
*One Mans Journey (1933) with Lionel Barrymore
*Living on Love (1937)
*A Man to Remember (1938) 

According to an interview with a retired RKO executive, shown as a promo on TCM, Cooper withdrew the films, only allowing them to be shown on television in 1955-1956 in New York City.  

In 2006, Turner Classic Movies, which had acquired the rights to the six films after extensive legal negotiations, broadcast them on TCM in April 2007, their first full public exhibition in over 70 years. TCM, in association with the Library of Congress and the Brigham Young University Motion Picture Archive, had searched many film archives throughout the world to find copies of the films in order to create new 35mm prints. Fristoe, Roger.     

==Reception==
The film recorded a loss of $28,000 in its original release. 

==References==
 

==External links==
*  at IMDB

 
 
 
 
 