A Stranger in Town
{{Infobox film
| name           = A Stranger in Town
| image          = A Stranger in Town (1943) 1.jpg
| caption        = Frank Morgan, Richard Carlson, and Jean Rogers in the film Roy Rowland John E. Burch (assistant)
| producer       = Robert Sisk 
| writer         = Isobel Lennart  William Kozlenko 
| narrator       =  Richard Carlson Jean Rogers
| music          = Daniele Amfitheatrof Nathaniel Shilkret Sidney Wagner
| editing        = Elmo Veron
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 
 Roy Rowland Sidney Wagner.

== Cast ==
*Frank Morgan as John Josephus Grant Richard Carlson as Bill Adams
*Jean Rogers as Lucy Gilbert
*Porter Hall as Judge Austin Harkley
*Robert Barrat as Mayor Connison
*Donald MacBride as Vinnie Z. Blaxton
*Walter Baldwin as Tom Cooney
*Andrew Tombes as Roscoe Swade
*Olin Howland as Homer Todds
*Chill Wills as Charles Craig
*Irving Bacon as Orrin Todds
*Eddie Dunn as Henry
*Gladys Blake as Birdie
*John Hodiak as Hart Ridges Edward Keane as Blaxtons Lawyer
*Robert Homans as Sergeant

== External links ==
* 
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 

 