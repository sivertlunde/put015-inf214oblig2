Thérèse Desqueyroux (1962 film)
{{Infobox film
| name           = Thérèse Desqueyroux
| image          = THERESE DESQUEYROUX by Franciszek Starowieyski.jpg
| image_size     = 200px
| caption        = A poster by Franciszek Starowieyski for the film Thérèse Desqueyroux published in Poland at the time of the films first release.
| director       = Georges Franju
| producer       = Eugène Lépicier
| writer         = François Mauriac  (novel /script)  Claude Mauriac Georges Franju
| narrator       =
| starring       = Emmanuelle Riva Philippe Noiret
| music          = Maurice Jarre Christian Matras
| editing        = Gilbert Natot
| distributor    = 20th Century Fox (Fr.) 1962
| runtime        = 109 minutes
| country        = France
| language       = French
| budget         =
| preceded_by    =
| followed_by    =
}} the novel Best Actress at the Venice Film Festival, the Étoile de Cristal award for Best Actress, and the Silver Goddess Award from the Mexican Cinema Journalists for her performance.

== Plot ==
Thérèse is living in a provincial town, unhappily married to Bernard, a dull, pompous man whose only interest is preserving his family name and property. They live in an isolated country mansion surrounded by servants. Early in her marriage her only comforts are her fondness for Bernards pine-tree forest, which was her primary reason for marrying him, and her love for her sister-in-law and Bernards half-sister, Anne. On Thérèses honeymoon, she slipped away from Bernards bed to throw away a letter from Anne in which Anne expressed her love for Jean, a Jewish student. Later, when Jean leaves Anne, Thérèse feels a sense of satisfaction and relief. However, Anne soon leaves. Desperately lonely and trapped, Thérèse accidentally learns that an increase in Bernards medication makes him ill. While Anne nurses Thérèses unwanted baby, Thérèse begins to experiment, taking advantage of his hypochondria and forgetfulness. Eventually she tries to poison him with arsenic, but the dose isnt fatal. Thérèses forged prescriptions are then discovered. Thérèse is arrested, but Bernard refuses to press charges. She is acquitted when Bernard perjures himself for her at the trial and her politically influential father bribes a court official. On the way back to the country estate, she tries to think of an explanation to offer to Bernard. Unable to give Bernard a proper explanation, she allows Bernard to place her in a prison of his own devising. He locks her in a bedroom and allows her only cigarettes and wine, as she slowly wastes away. Much later, he frees her for a party at which the family gathers to meet Annes new husband, and their friends are shocked at her sickly appearance and deterioration. Bernard then moves her to Paris. Still hoping to learn the motives for her crime, he listens to further explanations, but he cannot understand.The movie recounts in flashback the circumstances that led to her being charged with poisoning her husband.

==Production==
The film was shot at Studios Franstudio in Marseille, Bouches-du-Rhône, France. While the exterior part of the film was shot at Gironde, France.

==Cast==
*Emmanuelle Riva - Thérèse Desqueyroux
*Philippe Noiret - Bernard Desqueyroux
*Édith Scob - Anne de la Trave
*Sami Frey - Jean Azevedo
*Renée Devillers - Mme de la Trave
*Jeanne Pérez - Balionte
*Hélène Dieudonné - Aunt Clara
*Richard Saint-Bris - Hector de la Trave
*Jean-Jacques Rémy - Specialist

==Soundtrack==
{{Infobox album  
| Name        = Ma Periode Française
| Type        = Soundtrack
| Longtype    =
| Artist      = Maurice Jarre
| Cover       = Mauricejarresoundtrack.jpg
| Released    = February, 2005
| Recorded    =
| Genre       = Film music
| Length      = 71:11
| Label       = Play Time
| Producer    =
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}
Long after the films original release, in February 2005, the French soundtrack record label Play Time released the soundtrack on Compact Disc along with other soundtracks performed by Jarre. This also includes soundtracks from other Franju films including Head Against the Wall and Eyes Without a Face. {{cite web
| title = Anthologie 80ème Anniversaire
| publisher = FGL Productions
| year =
| month =
| url = http://www.fglmusic.com/produit?id=467
| language = French
| accessdate = 2008-08-26
}} 

===Tracklisting===
{{Track listing
| collapsed       = no
| headline        =
| extra_column    = Film
| total_length    =
| all_writing     =
| all_lyrics      =
| all_music       = Maurice Jarre 
| writing_credits =
| lyrics_credits  =
| music_credits   =
| title1          = Générique / Surprise-partie
| extra1          = La Tête contre les Murs
| length1         = 4:30
| title2          = Thème de Stéphanie
| extra2          = La Tête contre les Murs
| length2         = 4:30
| title3          = Enterrement à l’asile
| extra3          = La Tête contre les Murs
| length3         = 2:44
| title4          = Générique
| extra4          = Eyes Without a Face
| length4         = 2:05
| title5          = Thème romantique
| extra5          = Eyes Without a Face
| length5         = 2:50
| title6          = Filature
| extra6          = Eyes Without a Face
| length6         = 1:23
| title7          = Des phares dans la nuit
| extra7          = Eyes Without a Face
| length7         = 3:32
| title8          = Valse poursuite
| extra8          = Eyes Without a Face
| length8         = 1:45
| title9          = Final
| extra9          = Eyes Without a Face
| length9         = 1:01
| title10         = Générique
| extra10         = Thérèse Desqueyroux
| length10        = 1:54
| title11         = Non-lieu
| extra11         = Thérèse Desqueyroux
| length11        = 1:35
| title12         = Thérèse Desqueyroux
| extra12         = Thérèse Desqueyroux
| length12        = 2:50
| title13         = La femme idéale
| extra13         = Les Dragueurs
| length13        = 2:36
| title14         = La ballade des dragueurs
| extra14         = Les Dragueurs
| length14        = 2:47
| title15         = Surboum chez Ghislaine
| extra15         = Les Dragueurs
| length15        = 2:01
| title16         = Loiseau de paradis
| extra16         = LOiseau de Paradis
| length16        = 2:48
| title17         = Lunivers dUtrillo
| extra17         = Un court-métrage de Georges Régnier 
| length17        = 4:44
| title18         = Générique
| extra18         = Le Soleil dans l’œil
| length18        = 2:28
| title19         = Thème
| extra19         = Mort, où est ta Victoire ?
| length19        = 3:30
| title20         = Valse de Platonov
| extra20         = Recours en Grâce
| length20        = 3:50
| title21         = Les animaux (générique)
| extra21         = Les Animaux
| length21        = 1:20
| title22         = Pavane des flamands roses
| extra22         = Les Animaux
| length22        = 2:43
| title23         = La fête
| extra23         = Les Animaux
| length23        = 2:18
| title24         = Surf des loutres
| extra24         = Les Animaux
| length24        = 1:59
| title25         = Mourir à Madrid
| extra25         = Mourir à Madrid
| length25        = 4:21
| title26         = Générique
| extra26         = Week-End à Zuydcoote
| length26        = 2:28
| title27         = Sergent Maillat
| extra27         = Week-End à Zuydcoote
| length27        = 3:10
| title28         = Final
| extra28         = Week-End à Zuydcoote
| length28        = 1:29

}}

==References==
 

== External links ==
* 
*  

 

 
 
 
 
 
 