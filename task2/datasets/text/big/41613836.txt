Parawarthana
 
{{Infobox film
| name = Parawarthana
| image =
| caption = Film poster
| director = Jayanath Gunawardhana
| writer =
| starring =Somi Rathnayaka,Pubudu Chathuranga,Geetha Kanthi Jayakody,Dulani Anuradha,Nalin Pradeep Uduwella,Morine Charuni,Sarath Kothalawala,
| producer = Mohommad Mujahid,Jayanath Gunawardana,Chanika Ranganath Dias
| cinematography = Jayanath Gunawardhana
| editing = 
| distributor = 
| released =  
| runtime = 118 minutes
| language = Sinhala
| country = Srilanka
| music = Dinesh Subasinghe
| gross = 
}}
Parawarthana is a 2014 Sinhala language|Sinhala-language Sri Lankan film directed by cinematographer Jayanath Gunawardhana.  The plot was based on Buddhist teachings of universal justice, retributive justice|retribution. The score has been done by Dinesh Subasinghe. 

== Plot ==

The story starts calmly with simple things happening in a folk village near Anuradhapura.
Rathane aiya (Somi Rathnayaka) portrays a saintly person who lives in the neighborhood of a mother and two sons Jayasena (eldest) and another Siripala. The day before the poya, Siripala kills his brother. Rathne aiya is the prime suspect, and arrested. He is sentenced to the gallows.

While awaiting death, he confesses his bad behavior during his prime years. He thrived in terrorizing the village with ill-gotten money and power. He came there to evade punishment for a double murder.

He tries to live a good life, putting his past behind him. He becomes a Good Samaritan and is respected by all. Siripala gets shot and confesses that he killed Jayasena.


== Cast ==
{{columns-list|3|
*Somie Rathnayake as Rathne
*Pubudu Chathuranga as Siripala
*Dulani Anuradha as Kusum
*Nalin Pradeep as Jayasena
*Geeta Jayakody as Vimalavati
*Maureen Charuni as Kusums mother
*Sarasth Kothalaawala as Kusums father
*Sithuni Mallawarachie as Kusums daughter
*Sanjaya Leelarathna as Prison SP
*Janaka Ranasinghe as Thotiya
*Ariyasiri Gamage as Police Sagent
*Bimal Jayakody as The Police OIC
*Nilmini Kottege as Chandralatha
*Damitha Saluwadana as Chandares mother
}}
==References==
 

==External links==
* 
* 

 
 
 

 