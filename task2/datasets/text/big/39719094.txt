Mistake (film)
 
{{Infobox film
| name = Mistake
| image = 
| caption = 
| alt = 
| director = S.K.
| writer = 
| screenplay =  
| starring = Vikram Chatterjee Preeti Jhangiani
| producer = 
| distributor = 
| cinematography = 
| editing = 
| sound = 
| released =  
| runtime =  
| music = 
| country = India
| language = Bengali
}}
Mistake ( ) is a Bengali drama film directed by S.K.

==Story==

Mistake is a story which hovers around the life of five friends in the movie. The movie explores the problems faced by students during their college and university. It shows that if the right time for right work is not utilized, then it can lead to serious problems. Preeti Jhangiani, Vikram Chatterjee, Malabika, Sourav and Clio are seen in the shoes of five friends while actress Indrani Halder essays a key character. Other actors like Kunal Padhy, Biswajit Chakraborty, Dulal Lahiri and Barun Chatterjee are also in the movie. 

This S.K. directed Bengali film is exclusively dedicated to this controversial subject of Live-in-relationship. Although the film actually revolves around the lives of five friends, the central characters are the two college goer Preeti Jhangiani and Vikram Chatterjee, who fall for each other and end up in a live-in-relation and the complications around it. 

==Cast==
* Vikram Chatterjee
* Preeti Jhangiani

* Malabika

* Sourav

* Clio

* Indrani Halder

* Kunal Padhy

* Biswajit Chakraborty

* Dulal Lahiri

* Barun Chatterjee

==References==
 

==External links==
*  

 
 
 
 


 