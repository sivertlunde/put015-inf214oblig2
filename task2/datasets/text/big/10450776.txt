Lady Oscar (film)
 
 
{{Infobox film
| name = Lady Oscar
| image = LadyOscarPoster.jpg
| caption = International release poster
| director = Jacques Demy
| producer = Mataichiro Yamamoto
| screenplay = Jacques Demy Patricia Louisianna Knop
| based on =   Barry Stokes Christine Bohm Jonas Bergstrom
| music = Michel Legrand
| cinematography = Jean Penzer Paul Davies
| distributor = Toho
| released =  
| runtime = 124 minutes
| country = France Japan
| language = English
| budget =
| gross =

}}Lady Oscar is a 1979 English-language French-Japanese romantic drama film, based on the manga The Rose of Versailles by Riyoko Ikeda. The film was written and directed by Jacques Demy, with music composed by his regular collaborator Michel Legrand. Lady Oscar was filmed on location in France. 

==Plot== Barry Stokes), the son of the familys housekeeper. Years later, when the French Revolution begins, Oscar and Andres paths cross for the first time in years. With the assault on the Bastille, Oscar and Andre find themselves fighting on opposite sides of the revolution.

==Cast==
* Catriona MacColl as Oscar François de Jarjayes
* Patsy Kensit as young Oscar François de Jarjayes Barry Stokes as André Grandier Jonas Bergström Hans Axel von Fersen Marie Antoinette
* Terence Budd as Louis XVI
* Mark Kingston as General Jarjayes
* Georges Wilson as General Bouillé Martin Potter as Count de Gerodere Duchess de Polignac Jeanne Valois de la Motte Mike Marshall as Nicolas de la Motte Robespierre
* Constance Chapman as Nanny
* Gregory Floy as Cardinal de Rohan Rosalie Lamorlière Michael Osborne as Bernard Chatelet
* Angela Thorne as Mademoiselle Bertin
* Paul Spurrier as Prince Louis Joseph
* Rose Mary Dunham as Marquise de Boulainvilliers
* Lambert Wilson as the Cocky Soldier
* Caroline Loeb

==Production==
The major sponsor of the film was Shiseido, a cosmetics company, and Catriona McColl promoted a red lipstick for the spring cosmetic line that year.  Frederik L. Schodt translated the entire manga series into English as a reference for the producers of this film, but gave the only copy of the translation to them and it was lost. 

==Reception==
The film was not a commercial success,    and MacColls portrayal of Oscar, in particular, was criticised, it was felt by some critics that she was not androgynous enough to play Oscar. {{cite journal
 |last=Shamoon
 |first=Deborah
 |title=Revolutionary Romance: The Rose of Versailles and the Transformation of Shōjo Manga
 |journal=Mechademia
 |url=http://muse.jhu.edu/journals/mechademia/v002/2.shamoon.html
 |volume=2
 |year=2007
 |publisher=University of Minnesota Press
 |issn=2152-6648
 |pages=3–17
}} 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 


 
 