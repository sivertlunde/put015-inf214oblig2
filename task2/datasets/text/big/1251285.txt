Women on the Verge of a Nervous Breakdown
 
 
{{Infobox film
| name            = Women on the Verge of a Nervous Breakdown
| image           = Women on the Verge of a Nervous Breakdown`.jpg
| alt             =
| caption         = Theatrical release poster
| director        = Pedro Almodóvar
| producer        = Pedro Almodóvar
| writer          = Pedro Almodóvar
| starring        = {{Plainlist|
* Carmen Maura
* Antonio Banderas
* Julieta Serrano
* Rossy de Palma
* María Barranco}}
| music           = Bernardo Bonezzi
| cinematography  = José Luis Alcaine
| editing         = José Salcedo El Deseo, S.A.
| distributor     = Laurenfilm S.A.
| released        =  
| runtime         = 89 minutes  
| country         = Spain
| language        = Spanish
| budget          = $700,000   Internet Movie Database. Amazon.com. Retrieved 12 July 2013 
| gross           = $7.2 million    $16.9 million    
}} Academy Award Best Foreign Language Film, and won five Goya Awards including Best Film and Best Actress in a Leading Role for Maura.

The actual Spanish title refers to an  , which is not actually well translated as "nervous breakdown" ( ).   are culture-bound psychological phenomena during which the individual, most often female, displays dramatic outpouring of negative emotions, bodily gestures, occasional falling to the ground, and fainting, often in response to receiving disturbing news or witnessing or participating in an upsetting event. Historically, this condition has been associated with hysteria and more recently in the scientific literature with post-traumatic stress and panic attacks. 

==Plot== Fernando Guillén) has just left her. Both she and Iván work as Dubbing (filmmaking)|voice-over actors who dub foreign films, notably Johnny Guitar with Joan Crawford and Sterling Hayden. The voice he uses to sweet-talk her (and many other women) is the same one he uses in his work. He is about to leave on a trip and has asked Pepa to pack his things in a suitcase that he will pick up later.

Pepa returns home later to find her answering machine filled with frantic messages from her friend, Candela (María Barranco). In anger, she rips out the phone and throws it through the window onto the balcony. Candela arrives, still overwhelmed, but before she can explain her situation, Carlos (Antonio Banderas), Iváns son with previous lover Lucía (Julieta Serrano), arrives with his snobby fiancée, Marisa (Rossy de Palma). It turns out they are apartment-hunting, and by coincidence have chosen Pepas penthouse to look at. Carlos and Pepa figure out each others relationship to Iván; Pepa wants to know where Iván is because she has to tell him something, but Carlos doesnt know where his father is. Candela unsuccessfully attempts to kill herself by jumping off the balcony.

Meanwhile, Marisa has become bored and decides to drink some gazpacho she finds in the fridge, not realizing that it has been spiked with sleeping pills. Candela finally gets to explain her situation: a while back she had a love affair with an Arab who later came to visit her, bringing some friends with him. It turns out that they are a Shiite terrorist cell and Candela was unknowingly harboring them in her home. When the terrorists left, Candela fled to Pepas place for help. Candela fears that the police think she is involved and will come for her. Pepa sets out to see a lawyer Carlos has recommended to help Candela, and ends up catching the same cab with the same Mambo-loving driver.

However, Paulina, the lawyer she visits, is acting strangely. Pepa sees that Paulina has tickets to Stockholm. Iván calls the office at one point, and Paulina seems to know Pepa, and is very rude to her. Meanwhile, Candela reveals to Carlos that the Shiites plan to hijack a flight to Stockholm that evening and divert it to Beirut, where the Shiite terrorists have a friend who was captured by the authorities. After Carlos fixes the broken phone, he quickly calls the police, but hangs up before (he believes) they can trace the call, then surprisingly kisses Candela. Pepa returns and Lucía calls, announcing she is coming over to confront her about Iván. Carlos reveals that Lucía has been in a mental hospital since Iván left her and has only now been released. Pepa, now sick of Iván and no longer wanting to see him, heads back down with Iváns suitcase; she throws it out, just barely missing Iván, who has arrived with Paulina (Kiti Manver) on their way to the airport. He leaves Pepa a message.
 record and throws it out the window, which ends up hitting Paulina. Pepa then hears Iváns message and once again rips out the phone and throws the answering machine back out the window; it lands on Paulinas car. Back in the apartment, Lucía arrives, along with the phone repairman and the police, who have traced Carlos earlier call. Candela starts flipping out, but Carlos comes up with an idea: to serve everyone the spiked gazpacho. The cops and repairman are knocked out, Carlos and Candela make out on the sofa and also fall asleep, and crazy Lucía grabs the cops guns and aims them at Pepa. Pepa figures out that Paulina is the other woman Iván is going to Stockholm with, and that their flight is the one that the terrorists are planning to hijack. Lucía reveals that she is still insane and only faked sanity when she heard Iváns voice dubbed on a foreign film. She throws the gazpacho into Pepas face and rushes to the airport to kill Iván; she sees a motorcyclist and forces him to act as her driver.

Pepa chases her and is joined by Ana, the motorcyclists angry girlfriend. They quickly hail a cab (it turns out that it is the Mambo taxi again) and a mad chase ensues to the airport, with Lucía firing the gun at them. Lucía arrives at the airport, sees that Iván and Paulina are about to pass security, and aims her gun at them. Pepa arrives just in time and thwarts the murder attempt by rolling a luggage cart at Lucía. Iván runs over to Pepa, who is now mentally and physically exhausted after two days of trying to chase down her lover. Iván offers to finally speak with her about whatever she has been trying to speak to him about, and for a moment, it seems he might even leave Paulina to take her back. But Pepa refuses, saying, "There was still time last night, this morning, even today at noon. But now its too late." Having saved his life, she leaves the airport, and Iván, for good.

Pepa returns to her home, which is a mess with a burnt bedroom, broken windows, a telephone ripped off the wall, spilled gazpacho on the floor, her collection of animals running around loose, and several unconscious visitors all overdosed on sleeping pills. Pepa sits on her balcony where Marisa has just woken up. The two women share a moment of tranquility at the end of a crazy 48 hours, and Pepa finally reveals what her big news for Iván was: shes pregnant.

==Cast==
* Carmen Maura as Pepa Marcos
* Antonio Banderas as Carlos
* Julieta Serrano as Lucía
* Rossy de Palma as Marisa
* María Barranco as Candela Fernando Guillén as Iván
* Kiti Manver as Paulina Morales
* Ana Leza as Ana
* Chus Lampreave as Portera Testiga de Jehová

==Critical reception==
Women on the Verge of a Nervous Breakdown received generally positive reviews from critics. Review aggregator Rotten Tomatoes reports that 89% out of 27 professional critics gave the film a positive review, with a rating average of 7.7/10.  Based on 12 critics, it holds an 85/100 rating on Metacritic, signifying "universal acclaim". 

The film is ranked #78 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" list in 2010. 

==Awards and nominations==
;United States
* Academy Awards Best Foreign Language Film
* Golden Globe Awards Best Foreign Language Film
* National Board of Review Best Foreign Language Film New York Film Critics Best Foreign Language Film Best Actress (Carmen Maura)

;United Kingdom
* BAFTA Awards (UK)
** Nominated: Best Film not in the English Language

;Overseas
* David di Donatello Awards (Italy) Best Foreign Direction (Pedro Almodóvar)
* European Film Awards Best Actress – Leading Role (Carmen Maura) Best Young Film (Pedro Almodóvar)
** Nominated: Best Art Direction (Félix Murcia)
* Goya Awards (Spain) Best Actress – Leading Role (Carmen Maura) Best Actress – Supporting Role (María Barranco)
** Won: Best Editing (José Salcedo) Best Film Best Screenplay – Original (Pedro Almodóvar)
** Nominated: Best Actor – Supporting Role (Guillermo Montesinos)
** Nominated: Best Actress – Supporting Role (Julieta Serrano) Best Cinematography (José Luis Alcaine)
** Nominated: Best Costume Design (José María Cossío) Best Director (Pedro Almodóvar)
** Nominated: Best Makeup and Hairstyles (Jesús Moncusi and Gregorio Ros)
** Nominated: Best Original Score (Bernardo Bonezzi)
** Nominated: Best Production Design (Félix Murcia)
** Nominated: Best Production Supervision (Esther García)
** Nominated: Best Sound (Gilles Ortion)
** Nominated: Best Special Effects (Reyes Abades)
* Venice Film Festival (Italy)
** Won: Golden Osella – Best Screenplay (Pedro Almodóvar)

==Stage adaptation==
 
Women on the Verge of a Nervous Breakdown has been adapted into a musical by Jeffrey Lane (book) and David Yazbek (music and lyrics). The production opened on Broadway in previews on 5 October 2010, and officially on 4 November 2010, at the Belasco Theatre. The cast included Patti LuPone, Sherie Rene Scott, Laura Benanti, Brian Stokes Mitchell, Danny Burstein, Mary Beth Peil, Justin Guarini, deAdre Aziza, and Nikka Graff Lanzarone, with direction by Bartlett Sher. 

The production was a limited engagement that was scheduled to end 23 January 2011, but due to low grosses and ticket sales, closed early on 2 January 2011. At the time of closing, the show had played 30 previews and 69 regular performances. 

The show is currently running in Londons West End at the Playhouse Theatre, starring TV star Tamsin Greig in a leading role.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 