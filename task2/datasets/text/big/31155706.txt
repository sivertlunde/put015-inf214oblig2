Naadan Pennu
{{Infobox film 
| name           = Naadan Pennu
| image          =
| caption        =
| director       = KS Sethumadhavan
| producer       = MO Joseph
| writer         = Chempil John S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan Sathyan Sheela Jayabharathi
| music          = G. Devarajan
| cinematography = Melli Irani
| editing        = MS Mani
| studio         = Navajeevan Films
| distributor    = Navajeevan Films
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, directed by KS Sethumadhavanand produced by MO Joseph. The film stars Prem Nazir, Sathyan (actor)|Sathyan, Sheela and Jayabharathi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir as Babu Sathyan as Chacko
*Sheela as Achamma
*Jayabharathi as Sainaba
*Adoor Bhasi as Ummukka
*Thikkurissi Sukumaran Nair as Babus father
*T. S. Muthaiah as Mathai
*Bahadoor as Antappan Meena as Saramma
*N. Govindankutty as Priest
*S. P. Pillai as Aliyaar
*Paul Vengola as Appu
*P. R. Menon
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakashangalirikkum || P Susheela, Chorus || Vayalar Ramavarma || 
|-
| 2 || Bhoomiyil Mohangal || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Eeyide Penninoru || S Janaki || Vayalar Ramavarma || 
|-
| 4 || Himavaahini || P Susheela || Vayalar Ramavarma || 
|-
| 5 || Himavaahini   || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 6 || Himavaahini   || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 7 || Iniyathe Panchami || P Susheela || Vayalar Ramavarma || 
|-
| 8 || Naadan Premam || P Jayachandran, JM Raju || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 