Subway Sadie
 
{{Infobox film
| name      = Subway Sadie
| image     = File:Subway Sadie lobby card 1926.jpg
| image size   = 275
| border     = 
| alt      = 
| caption    = Lobby card
| director    =Alfred Santell
| producer    =Al Rockett
| based on       =  Paul Schofield
| starring    = Dorothy Mackaill Jack Mulhall
| cinematography = Arthur Edeson
| editing    = Hugh Bennett
| studio = Al Rockett Productions
| distributor  = First National Pictures
| released    =  
| runtime    = 70 minutes Silent
}} Charles Murray, Peggy Shaw, Gaston Glass, and Bernard Randall.

The film began production in May 1926. Arthur Edeson served as cinematographer, shooting around Central Park in areas like casinos and nightclubs. Distributed by First National Pictures, the film premiered in New York on September 12, 1926. Many publications wrote positively of the film, praising its acting and Santells direction. Today, it remains unclear if a print of Subway Sadie has survived.

==Plot==
Salesgirl Sadie Hermann (Dorothy Mackaill), employed in a New York fur store, has always dreamed of traveling to Paris. While riding the subway to work one morning, she meets Irish subway guard Herb McCarthy (Jack Mulhall), and the two strike up a conversation before Herb eventually arranges to have them meet at Cleopatras Needle that Sunday. 

Herb and Sadie are soon engaged to be married, but as Sadie has been promoted from saleslady to firm buyer she must cancel the wedding date to sail to Paris for the job, saddening Herb. Sadie prepares to leave, but then receives a message from Herb, which informs her that he is in the hospital as the result of an accident. Sadie chooses to visit him, and she decides to forgo her new job and marry Herb instead, Herb revealing that his father is the president of the subway company.    

==Production==
{{multiple image
| footer    = Dorothy Mackaill and Jack Mulhall, who both had lead roles in Subway Sadie
| image2    = Jack Mulhall - Jul 1920 MP.jpg
| alt2      = Jack Mulhall, American actor, who played Herb McCarthy in Subway Sadie
| width2    = 220

| image1    = Dorothy MacKaill Stars of the Photoplay.jpg
| alt1      = Dorothy Mackaill, actress who played Sadie Hermann in Subway Sadie
| width1= 195
}} Paul Schofield. The pair adapted a short story by Mildred Cram entitled Sadie of the Desert, which had first been published in an October 1925 issue of Redbook|The Red Book Magazine.   

On May 3, 1926, the film entered production.  Santell and Al Rockett, the films producer and production manager, selected the actors to appear in the film.   did when he directed A Woman of Paris&nbsp;- he and Lewis Milestone and Mal St. Clair all have that same touch, they all belong to the new school of directors, it seems to me. Theyre not so busy thinking about technique that they have actors turning into marionettes.    
 Charles Murray as a driver, Peggy Shaw as Ethel, Gaston Glass as Fred Perry, and Bernard Randall portraying Brown.   

Arthur Edeson served as the cinematographer for Subway Sadie, shooting the silent film in black-and-white. reels long, and comprised   of film,  running for about 70 minutes. 

==Release and reception==
 
First National Pictures handled distribution for Subway Sadie, with the film premiering in New York on September 12, 1926.  It received positive reviews; a journalist for The New York Times enjoyed the film, calling it "an amusing photoplay". Although the review branded the ending unsurprising, they described it as "nevertheless pleasing".  The Evening Independent praised the film, lauding it as "one of the cleverest and most interesting pictures that has been here this season". 

A review from Photoplay applauded Mackaills performance and described the film as "a true and human story".  The review in the Motion Picture Herald assessed it as "a nice little feature, nothing big, but will go over on bargain nights", with praise directed to Mulhalls performance.  A Berkeley Daily Gazette review wrote of the film by saying "sheer brilliance rarely has been equalled" and praised the story, direction, and acting.  

The Morning Telegraph s review said that Subway Sadie would "delight the majority of straphangers" and that "it is what the boys call excellent box-office". The New York American review was similarly positive, describing it as "a light but charming comedy". In the New York World, the review described the film as "a consistently decent affair" which featured good direction by Santell.    A review in The Daily Mirror wrote positively of Mackaills performance and complimented Santells directing abilities,  while Reading Eagle praised the performances of the leads, calling them "a stellar combination."  Not all reviews were positive; a negative review came from The Educational Screen, whose reviewer found the film to be "pretty trite stuff". 
 Lady Be Good (1928),  and Children of the Ritz (1929).  The 1933 drama Curtain at Eight marked the final film they appeared in together.  

Screenings of Subway Sadie occurred as late as January 12, 1928.  As of November 2007, it is unclear whether a print of the film exists; it has likely become a lost film.  A poster of the film can be seen at the New York Transit Museum. 

==References==
 

===Bibliography===
 
* 
* 
 

==External links==
 
* 

 

 
 
 
 
 
 