You're in the Army Now
 

 
{{Infobox film
| name           = Youre in the Army Now
| caption        = Promotional Movie Poster 1941
| image	=	Youre in the Army Now FilmPoster.jpeg
| director       = Lewis Seiler
| producer       = Benjamin Stoloff (associate producer) George Bentley and  Paul Girard Smith  (story and screenplay)
| starring       = Jimmy Durante  Phil Silvers  Jane Wyman  Regis Toomey Howard Jackson
| cinematography = Arthur L. Todd
| editing        = Frank Magee Warner Brothers
| distributor    = 
| released       = December 25, 1941 (United States)
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Youre in the Army Now is a 1941 comedy film starring Jimmy Durante, Phil Silvers, Jane Wyman, and Regis Toomey.

It featured the longest kiss in film, lasting three minutes and six seconds. 

== Cast ==
{| class="wikitable"
|-
! Characters !! Actors
|-
| Homer Jeeper Smith || Jimmy Durante
|-
| Breezy Jones || Phil Silvers 
|-
| Bliss Dobson || Jane Wyman
|-
| Capt. Joe Radcliffe || Regis Toomey 
|-
| Colonel Dobson || Donald MacBride 
|-
| Sgt. Madden || Joe Sawyer
|-
| Brig. Gen. Damon P. Winthrop || Clarence Kolb 
|- Paul Harvey
|-
| Capt. Austin || George Meeker
|- Paul Stanton
|- 
| Sgt. Thorpe || William Haade
|- John Maxwell
|-
| Della || Etta McDaniel
|-
|}

==See also==
*1941 in film

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 