Gudachari 116
 
{{Infobox film
| name           = Gudachari 116
| image          = Gudachari 116.jpg
| image_size     =
| caption        = DVD cover of Gudachari 116 film
| director       = M. Mallikharjuna Rao
| producer       = Doondi
| writer         = Arudra   (story) 
| narrator       = Krishna Jayalalitha Rajanala Mukkamala 
| music          = T. Chalapathi Rao
| cinematography = Ravikant Nagaich
| editing        = 
| studio         =
| distributor    =
| released       = 1967
| runtime        =
| country        = India Telugu
| budget         =
}}

Gudachari 116 or Goodachari 116 is a 1967 Telugu film in directed by M. Mallikharjuna Rao.

==Cast==
* Krishna Ghattamaneni	... 	Agent Gopi (116) Rajanala		
* Mukkamala		 Jayalalitha		
* Relangi Venkataramaiah		
* Ramana Reddy		
* Gitanjali		 Rajababu		
* B. Padmanabham		 Venu Madhava Rao Nerella		
* Vijayalalitha				
* T. Chalapathi Rao
* sobhanbabu
* Chalapathi Rao

==Soundtrack==
There are 6 songs in the film. 
* Chempameeda Chitikeste Sompulanni (Lyrics: Arudra; Singer: Ghantasala)
* Manasuteera Navvule Navvule Navvali (Lyrics: Arudra; Singers: Ghantasala, P. Susheela and group)
* Neetho Yedo Paniundi (Lyrics: Arudra; Singer: P. Susheela)
* Nuvvu Naa Mundunte Ninnalaa Choostunte (Lyrics: C. Narayana Reddy; Singers: Ghantasala and P. Susheela)
* O Vaalu Choopula Vanneladi (Lyrics: Arudra; Singer: Ghantasala)
* Padileche Keratam Choodu (Lyrics: Arudra; Singer: P. Susheela)
* Yerra Buggala Meeda Manasaite (Lyrics: C. Narayana Reddy; Singers: Ghantasala, P. Susheela and group)

==References==
 

==External links==
* 

 
 
 
 


 