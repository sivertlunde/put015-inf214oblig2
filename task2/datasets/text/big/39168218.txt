As I Lay Dying (film)
 
{{Infobox film
| name           = As I Lay Dying
| image          = As I Lay Dying 2013 film poster.jpg
| caption        = Theatrical release poster
| director       = James Franco
| producer       = Caroline Aragon Lee Caplin Vince Jolivette Avi Lerner Miles Levy Matthew OToole Robert Van Norden
| writer         = James Franco Matthew Rager
| based on       =  
| starring       = James Franco
| music          = 
| cinematography = Christina Voros
| editing        = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = 
}}
 novel of the same name published in 1930. The story is based on the loss of a mother and the struggles in which the family suffers by going the distance to her burial grounds in her home town.   

The film was screened in the Un Certain Regard Section at the 2013 Cannes Film Festival.      

==Cast==
* James Franco as Darl Bundren
* Logan Marshall-Green as Jewel Bundren
* Danny McBride as Vernon Tull
* Tim Blake Nelson as Anse Bundren
* Ahna OReilly as Dewey Dell Bundren
* Beth Grant as Addie Bundren
* Jim Parrack as Cash Bundren
* Jesse Heiman as Jody
* Scott Haze as Skeet MacGowan

==Production== As I Lay Dying was described as a story impossible to be transformed into a film due to the multi-narrative voices within it.    Franco saw this as a challenge and was able to depict the many voices, through choices of styling through camera edits. Faulkner told this story in a chorus of voices: 15 narrators in the 59 chapters.  To locate an equivalent for the novel’s polyphonal scheme, Franco in relation to this decision employs the use of narrative expressed through dialogue and voice overs. 

Filming took place in Mississippi from August 21, 2012 to October 5, 2012.

==Release==
The film was originally scheduled for a theatrical release on September 27, 2013 but Millenium Films scrapped the plans. It was released on October 22, 2013 to iTunes and November 5, 2013 to DVD/VOD platforms.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 