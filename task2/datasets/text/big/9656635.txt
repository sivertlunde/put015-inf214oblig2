Michel Vaillant (film)
{{Infobox film
| name           = Michel Vaillant
| image          =
| image_size     =
| caption        =
| director       =  
| producer       = Pierre-Ange Le Pogam
| writer         = Jean Graton    Philippe Graton     Luc Besson   Gilles Malençon   
| starring       = Sagamore Stévenin Peter Youngblood Hills Diane Kruger
| music          = Tim Abbott Archive
| cinematography = Michel Abramowicz
| editing        = Hervé Schneid
| distributor    =
| released       =  
| runtime        = 103 minutes
| country        = France
| language       = French
| budget         =
}}
 French movie starring Sagamore Stévenin and Diane Kruger. It depicts events around the 24 hours of Le Mans race, based largely on a comic about the Michel Vaillant character.
 2002 race.

==Cast==
*  
* Peter Youngblood Hills : Steve Warson
* Diane Kruger : Julie Wood
* Jean-Pierre Cassel : Henri Vaillant; Michels father
* Béatrice Agenin : Élisabeth Vaillant, Michels mother
* Philippe Bas : Jean-Pierre Vaillant, Michels brother
* Lisa Barbuscia : Ruth
* Stefano Cassetti : Giulio Cavallo
* Agathe de La Boulaye : Gabrièle Spangenberg

==Soundtrack album==
The soundtrack album for the film was performed by the band, Archive (band)|Archive, and released on November 4, 2003.

===Track listing===
CD1
#"Le Mans"  
#"Bridge Scene"  
#"Helicoptere"  
#"Come to Me, Pt. 1"  
#"Valliant Theme"  
#"Nothing"  
#"Friend"  
#"Nightmare Scene"  
#"Leader Theme"  
#"Nightmare Is Over"  
#"Valliant (Acoustic)"  
#"Night Time"  
#"Red"  
#"Come to Me, Pt. 2"  

CD2
#"Opening Credits (Includes Nightmare Scene)"  
#"Indian Theme"  
#"Calling"  
#"Brass Indian"  
#"Main Bridge Scene (Including Sound Design)"  
#"End of Bridge Scene/Keen for a Dead Child"  
#"Falaise"  
#"Break In"  
#"Chase Scene"  
#"Blue Room"  
#"Come to Me, Pt. 3"  
#"Crash Scene"  
#"Hélicoptère"  
#"Warm Up/Leader Theme (Strings Version)"  
#"Le Mans (End)"  
#"Nightmare Is Over (Acoustic Version)"  

== See also ==
* Michel Vaillant

== External links ==
* 
* 

 
 

 
 
 
 
 
 


 
 