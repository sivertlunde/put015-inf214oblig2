Sherlock Holmes (1932 film)
{{Infobox film
| name           = Sherlock Holmes
| image          = Poster of Sherlock Holmes (1932 film).jpg
| image_size     =
| caption        =
| director       = William K. Howard
| producer       = Fox Film Corporation
| writer         =
| narrator       =
| starring       = Clive Brook Reginald Owen
| music          =
| cinematography =
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 68 min.
| country        = United States English
| budget         =
}} eponymous London stage play Sherlock Holmes The Return of Sherlock Holmes.
 A Study in Scarlet. Owen is one of only four actors to play both Holmes and Watson — Jeremy Brett played Watson on stage in the United States and, most famously, Holmes on British television, Carleton Hobbs played both roles in British radio adaptations, while Patrick Macnee played both roles in US television movies.

==Cast==
* Clive Brook	 ... 	Sherlock Holmes
* Miriam Jordan	... 	Alice Faulkner Moriarty
* Herbert Mundin	... 	George Watson
* Howard Leeds	... 	Little Billy
* Alan Mowbray	... 	Colonel Gore-King
* C. Montague Shaw	... 	Judge
* Frank Atkinson	... 	Man in Bar
* Ivan F. Simpson	... 	Faulkner Stanley Fields	... 	Tony Ardetti

Uncredited:
*Ted Billings - Carnival Thug
*Roy DArcy - Manuel Lopez Edward Dillon - Al John George - Bird Shop Thug
*Robert Graves - Gaston Roux
*Lew Hicks - Prison Guard
*Brandon Hurst - Secretary to Erskine Claude King - Sir Albert Hastings
*Arnold Lucy - Chaplain
*Lucien Prival - Hans Dreiaugen

==External links==
* 
* 

 
 

 
 
 
 
 
 
 


 