Sound Thoma
 
{{Infobox film
| name = Sound Thoma
| image = Sound Thoma Theatrical Release Poster.jpg
| caption = Theatrical Release Poster
| alt =
| director = Vyshakh
| producer = Anoop
| writer = Benny P Nayarambalam Dileep Mukesh Mukesh  Saikumar Namitha Pramod Nedumudi Venu
| music = Gopi Sundar
          Rajeev Alunkal(lyrics)
| cinematography = Shaji Kumar
| editing = Mahesh Narayanan
| studio = Priyanjali Films
| released =  
| country = India
| language = Malayalam
| budget         = 5.75 cr 
| gross          = 10.50 cr 
| website =  
}} Dileep playing a character with a cleft lip and defective voice while Mukesh (actor)|Mukesh, Saikumar (Malayalam actor)|Saikumar, and Namitha Pramod portray supporting roles. The film, produced by Anoop, features background score and soundtrack composed by Gopi Sundar with cinematography handled by Shaji Kumar.

==Plot==
The film opens with the birth of Thoma (Dileep).  Thomas mother dies while giving birth to him, and he also suffers from a cleft lip and vocal cord deformity.  Thomas father is the very rich Plapparambil Paulo (Saikumar) a man known for loansharking, greed, stinginess, and evicting people for inability to pay back the loan and forcing foreclosure.  Paulo was reluctant to spend money for his sons surgery. Paulo has two elder sons—the eldest Plapparambil Mathai (Mukesh) and the younger son, Joykutty (Shiju).

Mathai falls in love with a Muslim girl and marries her, causing Paulo to kick him out of their house. Mathai moves to his wifes house, converts to Islam, and begins a new life as a fishmonger.

Paulo anticipating his son Joykutty to fetch 2 crore rupees as a dowry and is searching for a wife for him, but is unsuccessful because no one wants to send a girl into Paulos home because of his bad reputation as a heartless money lender.  In order to change his reputation, Thoma has an idea—Paulo should conduct a group wedding for five orphan girls and show that he is a charitable man. During the course of the wedding,  Paulos plans go astray.Joy marries an orphan girl when one of the grooms runs away and the only soulution the people found when Paulos servant says the truth they make Joykutty marry her which was actually Thomas plan. Paluo kicks Joey out of the house

==Cast==
* Namitha Pramod as Sreelakshmi Dileep as Plapparambil Thoma Mukesh as Plapparambil Mathai/Musthafa Saikumar as Plapparambil Paulo
* Nedumudi Venu as Manikunju
* Suraj Venjaramoodu as Uruppadi Vijayaraghavan as Bhagavathar
* Shiju as Plapparambil Joykutty
* Kalabhavan Shajon as Sabu
* Reshmi Boban as Mathai/Musthafas wifw
* Dharmajan 
* Ambika Mohan as SI Rajeshs aunt
* Soja Jolly
* Subbaraju as SI Rajesh
* Joju George
* Balachandran Chullikad as Dr. George Joseph
* Subbalakshmi

==Soundtrack==
{{Infobox album 
| Name = Sound Thoma
| Longtype =
| Type = Soundtrack
| Artist = Gopi Sundar
| Cover = 
| Border = yes
| Alt =
| Caption = Cover Art
| Released = 24 March 2013
| Recorded = 2012 - 2013 Feature film soundtrack
| Length = 27:58
| Language = Malayalam Eastcoast Audios
| Producer = Gopi Sundar
| Last album = Kammath & Kammath (2013)
| This album = Sound Thoma (2013)
| Next album =   (2013)
}}

The soundtrack was composed by Gopi Sundar, with lyrics by Murugan Kattakada, Nadirsha, Rajeev Alunkal.

{{Track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 27:58
| all_music =
| lyrics_credits = yes
| title1 = Kandal Njanoru (Thoma Style) Dileep
| lyrics1 = Nadirsha
| length1 = 03:47
| title2 = Kanni Penne
| extra2 = Shankar Mahadevan, Rimi Tomy
| length2 = 04:30
| lyrics2 = Murugan Kattakada
| title3 = Oru Kaaryam
| extra3 = Udit Narayan, Shreya Ghoshal
| lyrics3 = Rajeev Alunkal
| length3 = 06:02
| title4 = Ambili Maame (Title Song)
| extra4 = Resmi Sateesh, Chorus
| length4 = 02:35
| lyrics4 = Murukan Kattakada
| title5 = Kandal Njanoru (Thoma Style)
| extra5 = Karaoke
| length5 = 03:46
| lyrics5 = Instrumental
| title6 = Kanni Penne
| extra6 = Karaoke
| length6 = 04:47
| lyrics6 = Instrumental
| title7 = Oru Kaaryam
| extra7 = Karaoke
| length7 = 05:01
| lyrics7 = Instrumental
}}

==Reception==
With an aggregate review score of 3/5 at "Reviewbol.com", the film earned mixed reviews from critics while response from the audience was generally positive. The film ultimately went on to become a box-office success and ran for over a 100 days. 

Unni R. Nair of Kerala9.com rated the film   and said that the film was "OK for Dileep fans and the family audience".  Theaterbalcony gave it a total score of 57% on 100. 

==References==
 

== External links ==
*  
*  

 
 
 
 