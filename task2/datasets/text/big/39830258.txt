The Clown and Automobile
{{Infobox film
| name           = Automaboulisme et Autorité
| image          = Méliès Automaboulisme.jpg
| image_size     =
| border         =
| alt            =
| caption        = A frame from the film
| film name      =
| director       = Georges Méliès
| producer       =
| writer         =
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 40 meters/130 feet   
| country        = France
| language       = Silent
| budget         =
| gross          =
}}

The Clown and Automobile ( ), released in the United Kingdom as The Clown and Motor Car, is an 1899 French silent film directed by Georges Méliès. It was released by Mélièss Star Film Company and is numbered 194–195 in its catalogues. 
 lost until 2011, when a hand-colored fragment on nitrate film was found among a collection donated to the Cinémathèque Française.   

==Summary==
Though the print rediscovered in 2011 only comprises fragments of the original,  Mélièss film catalogues provide a summary of the complete film:

 

==Legacy==
When writing about his childhood, the filmmaker Jean Renoir described a short silent film he saw as a child in 1902, featuring a clown called "Automaboul." The film made a vivid impression on Renoir, who said in 1938 that he "would give almost anything to see that program again. That was real cinema, much more than the adaptation of a novel by Georges Ohnet or a play by Victorien Sardou can ever be."  The film scholar Alexander Sesonske has suggested that the film Renoir remembered was Mélièss Automaboulisme et Autorité. 

==References==
 

 

 
 
 

 