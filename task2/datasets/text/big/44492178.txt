Arsène Lupin Returns
{{Infobox film
| name           = Arsène Lupin Returns
| image          = Arsène Lupin Returns poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = George Fitzmaurice
| producer       = John W. Considine Jr. 	
| screenplay     = James Kevin McGuinness Howard Emmett Rogers George Harmon Coxe
| story          = James Kevin McGuinness Howard Emmett Rogers George Harmon Coxe John Halliday Nat Pendleton Monty Woolley
| music          = Franz Waxman
| cinematography = George J. Folsey
| editing        = Ben Lewis 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Halliday, Nat Pendleton and Monty Woolley. The film was released on February 25, 1938, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Melvyn Douglas as Rene Farrand
*Virginia Bruce as Lorraine de Grissac
*Warren William as Steve Emerson John Halliday as Count de Grissac
*Nat Pendleton as Joe Doyle
*Monty Woolley as Georges Bouchet
*E. E. Clive as Alf
*George Zucco as Prefect of Police
*Rollo Lloyd as Duval
*Vladimir Sokoloff as Ivan Pavloff
*Ian Wolfe as Le Marchand
*Tully Marshall as Monelle
*Jonathan Hale as F.B.I. Special Agent

== References ==
 

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 