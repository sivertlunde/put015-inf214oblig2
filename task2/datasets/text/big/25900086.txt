Lolita: Vibrator Torture
{{Infobox film
| name = Lolita: Vibrator Torture
| image = Lolita Vibrator Torture.jpg
| image_size = 
| caption = Theatrical poster for Lolita: Vibrator Torture (1987)
| director = Hisayasu Satō 
| producer = 
| writer = Shirō Yumeno
| narrator = 
| starring = Sayaka Kimura Kiyomi Itō
| music = Sō Hayakawa
| cinematography = Toshihiko Ryū
| editing = Shōji Sakai
| distributor = Shishi Productions Nikkatsu
| released = September 19, 1987
| runtime = 63 minutes
| country = Japan Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}

  is a  , and lead actress of Lolita: Vibrator Torture, Kiyomi Itō was awarded Best Actress at the same ceremony for Hisayasu Satōs Dirty Wife Getting Wet. 

==Synopsis==
A man captures schoolgirls and takes them to an abandoned freight container in Shinjuku which he has decorated with enlarged black & white images of the faces of his previous victims. He smears the captive girls with paint and shaving cream, rapes, tortures and brutally murders them.     

==Cast==
* Sayaka Kimura 
* Kiyomi Itō
* Rio Yanagawa
* Yūko Suwano
* Ayako Toyama
* Yutaka Ikejima

==Critical reception==
Allmovie, noting the films "strong graphic visuals" judges Lolita: Vibrator Torture to be " ne of Satos most repellent and excessive pinku-eiga films." In reference to genre, the review concludes that " he focus here is on sadism and gore rather than erotica."    In their Japanese Cinema Encyclopedia: The Sex Films the Weissers confirm that this "is usually cited as Satos most grotesque film." 

Jasper Sharp writes that Lolita: Vibrator Torture "does for the marital aid what Tobe Hooper did for the chainsaw." Sharp, p. 263.  He uses the film as an example of Satōs use of the distorting ability of the camera, pointing out the "stroboscopic intensity" that the flashing stills camera adds to the disturbing imagery of the film. 

Award-winning director Yūji Tajiri, one of the  , cites Lolita: Vibrator Torture as one of the inspirations for his own career. He remembers, "One day I saw Lolita Vibrator Torture by Hisayasu Satō, and was fascinated by this film, which was completely different from anything I had ever seen before." Satōs presence at Shishi Productions, where he directed Lolita: Vibrator Torture was influential in Tajiris decision to join the studio in 1990. 

==Availability==
Lolita: Vibrator Torture was released theatrically on September 19, 1987.  On March 22, 2002 Uplink released the film on DVD in Japan as  .   This was Satōs original title for the film. 

==Bibliography==

===English===
*  
*  
*  
*  
*  

===French===
*  

===Japanese===
*  
*  
*  
*  

==Notes==
 

 

 
 
 
 
 
 
 