It Was I!
{{Infobox film
| name = It Was I!
| image =
| image_size =
| caption =
| director = Raffaello Matarazzo 
| producer = Giuseppe Amato  
| writer =  Roberto Bracco (play)   Paola Riccora  (play)   Giuseppe Amato     Raffaello Matarazzo 
| narrator =
| starring = Eduardo De Filippo   Peppino De Filippo   Titina De Filippo   Isa Pola 
| music = Cesare A. Bixio 
| cinematography = Václav Vích
| editing = Eraldo Da Roma 
| studio =   Amato Film 
| distributor = E.I.A.
| released = 1937 
| runtime = 72 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} remade in 1973.

==Cast==
* Eduardo De Filippo as Giovannino Apicella  
* Peppino De Filippo as Carlino  
* Titina De Filippo as Donna Rosa  
* Isa Pola as Lisa  
* Alida Valli as Lauretta  
* Federico Collino as Matteo 
* Lina Gennari as Fiammetta 
* Tecla Scarano as Fiammettas mother
* Silvio Bagolini 
* Calisto Bertramo 
* Corrado De Cenzo
* Silvana Jachino 
* Armando Migliari
* Dina Perbellini    
* Albino Principe   
* Mirella Scriatto    
* Guglielmo Sinaz
* Vinicio Sofia    
* Marisa Vernati

== References ==
 

== Bibliography ==
* Moliterno, Gino. Historical Dictionary of Italian Cinema. Scarecrow Press, 2008.

== External links ==
* 

 
 
 
 
 
 

 

 