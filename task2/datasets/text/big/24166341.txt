Rapture (1965 film)
{{Infobox film
| name           = Rapture
| image          = Rapture1965.jpg
| image_size     = 
| caption        = 
| director       = John Guillermin
| screenplay     = Ennio Flaiano Stanley Mann
| based on       =  
| starring       = Melvyn Douglas Patricia Gozzi Dean Stockwell
| music          = Georges Delerue
| cinematography = Marcel Grignon
| editing        = Max Benedict Françoise Diot 
| distributor    =  
| released       =  
| runtime        = 104 minutes
| country        = France United States
| language       = French English
| budget         = 
| gross          = 
}}

Rapture ( ) is a 1965 French-American film directed by John Guillermin, and starring Melvyn Douglas, Patricia Gozzi, and Dean Stockwell.

==Plot==
Agnes, a lonely teenage girl, and her father befriend an escaped convict, named Joseph, who arrives at their farmhouse in Brittany, France. When Joseph develops an attraction to Agnes, her father threatens to break up their relationship. 

== Cast ==
* Melvyn Douglas as Frederick Larbaud 
* Patricia Gozzi as Agnes Larbaud 
* Dean Stockwell as Joseph 
* Gunnel Lindblom as Karen

==Reception==
Time (magazine)|Time magazine called it a "penumbral play of love against loneliness"
that "boost  the artistic stock of English director John Guillermin" and "clinch  the reputation of Frances 15-year-old Patricia Gozzi." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 