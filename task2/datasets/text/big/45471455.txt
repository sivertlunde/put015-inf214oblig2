Chandrasena (film)
{{Infobox film
| name           = Chandrasena
| image          = 
| image_size     = 
| caption        = 
| director       = V. Shantaram Keshavrao Dhaiber
| producer       = Prabhat Film Company
| writer         = 
| narrator       = 
| starring       = Lilavati Pendharkar Gulbbai G. R. Mane Kamla 
| music          = 
| cinematography = Keshavrao Dhaiber
| editing        = 
| distributor    =
| studio         = Prabhat Film Company
| released       = 1931
| runtime        = 
| country        = India
| language       = Silent film
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1931 Indian Indian silent film directed by V. Shantaram and Keshavrao Dhaiber.       The director of photography was Keshavrao Dhaiber.    Produced under the banner of  Prabhat Film Company, the film brought the company into the "frontline" of film makers.    The cast included Lila, "alias" Lilavati Pendharkar, who was making her debut with this film,       with Kamla, Gulabbai and G. R. Mane.    

The films story involved an episode from the epic Ramayana.    Ahiravans wife, Chandrasena helps Hanuman to rescue Rama and Lakshmana from Patala and in the process defeat Mahiravanas army. 

The use of the trolley was made for the first time in Chandrasena.    The Prabhat company films, were cited to be of "high quality" even though their content was similar to others produced around the same time.   

==Cast==
* Lila 
* Kamla
* Gulabbai
* G. R. Mane

==References==
 

==External links==
* 

 

 
 
 
 
 

 
 