Aattanayagann
{{Infobox film
| name           = Aattanayagann
| image          = Aatanayagan.jpg
| caption        =
| director       = Krishnaram
| producer       = K. Muralidharan Swaminathan T. S. Rangarajan
| writer         = Krishnaram Sakthi Adithya_(actor)|Adithya Santhanam Meera Vasudevan
| music          = Srikanth Deva
| cinematography = S. D. Vijay Milton
| editing        = Harsha
| studio         = Lakshmi Movie Makers
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
}} Adithya Menon and Remya Nambeeshan in lead and Ravi Kale, Santhanam (actor)|Santhanam, Meera Vasudevan and R. Sundarrajan in supporting roles.  It released on December 17, 2010 to negative reviews.  

==Plot==
Lingam (Sakthi), the younger son of Nasser, leads life in his own way. He along with his set of friends (which includes Santhanam and ‘Lollu Sabha’ Jeeva) goes around town enjoying. But Lingams father is against his ways. He often compares him with his elder brother Chandran (Adhitya Menon), who runs a software firm in Hyderabad and is caring towards the family. Lingam falls in love with Radhika (Ramya Nambeesan) and he decides to make her sister Indira (Meera Vasudevan) enter wedlock with his brother.

After their marriage, Chandran takes Indira to Hyderabad. But she comes across a startling truth that Chandran is none but a dreaded don in the capital city of Andhra Pradesh and that had hid this truth to his family.

The onus now falls on Lingam to set things right. He promises Indira and Radhika that he would bring his brother back to right ways. But a hindrance to his mission is Boppala Ram Babu (Ravi Kale), who has a score to settle with Chandran. Does Lingam succeeds in his mission or not forms the climax.

The film also gained popularity with the lead actress Remya Nambeeshan appearing glamourous in the songs for the 1st time in her career.

==Cast== Sakthi as Lingam
* Remya Nambeeshan as Radhika Adithya Menon as Chandran
* Ravi Kale as Pettinaidu Santhanam
* Meera Vasudevan as Indhira Chandran
* Nassar
* R. Sundarrajan as Lingam
* Lollu Sabha Jeeva Ashwini
* Sachu

==References==
 

==External links==
*  

 
 
 
 