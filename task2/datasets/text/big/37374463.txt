Earthbound (1920 film)
{{Infobox film
| name           = Earthbound
| image          = Earthbound 1920.JPG
| alt            = 
| caption        = Article from Photoplay Magazine, November, 1920
| director       = T. Hayes Hunter
| producer       = Basil King
| writer         = Edfrid A. Bingham
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Wyndham Standing, Mahlon Hamilton, Naomi Childers
| music          = 
| cinematography = André Barlatier - ( )
| editing        = 
| studio         = 
| distributor    = Goldwyn Pictures
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

Earthbound was a silent film from Goldwyn Pictures Corporation that was released on August 11, 1920. The film was written by Edfrid A. Bingham from a story by Basil King, and directed by T. Hayes Hunter. Earthbound was produced by Basil King with cinematography by André Barlatier, film editing by J.G. Hawks and art direction from Cedric Gibbons.  It is considered a lost film. 

==Plot==
 Since their college days, Desborough and Rittenshaw have pretended to live by the creed, "No God, no sin, no future life." To this Breck, an author, refuses to subscribe, though the three are intimate friends of long standing. A cloud on their relationship which rapidly assumes tragic proportions, is the intimacy between Desborough and the beautiful wife of Rittenshaw. The latter first has his eyes opened by the suffering wife of Desborough. He waits until he has definite proofs, then kills Desborough on sight at their club. 

 Rittenshaw offers no defense at his trial, refusing to incriminate his wife. The earthbound spirit of Desborough, tortured by full realization of his duplicity, finds it difficult to communicate with his own deceived wife, but he goes to Daisy, the wife of Rittenshaw, and haunts her conscience until she makes her way to the courtroom and testifies to her own fault. By the unwritten law, Rittenshaw is pronounced "not guilty," but his release brings no great happiness. 

 Desborough haunts those he has caused to suffer until he works through their finer sentiments to restore love and happiness. His last effort to secure the forgiveness of his wife is the more touching that she freely pardons him. Then his tortured soul is finally released from earthly trammels, and the spirit of kindness and consideration left behind by his efforts reunites Rittenshaw and his erring wife. Photoplay Plot Encyclopedia, 1922,  

==Cast of Characters==
 
*Nicholas Desborough .. Wyndham Standing
*Jim Rittenshaw .. Mahlon Hamilton
*Caroline Desborough .. Naomi Childers Flora Revalles 
*Doctor Galloway .. Alec B. Francis
*Connie Desborough .. Billie Cotton (child actor)
*Harvey Breck .. Lawson Butt
*Miss De Windt .. Kate Lester

==Reception==
 Earthbound, written by Basil King and produced by Goldwyn, has immortality as its theme—life after death. This is perhaps the most fascinating subject that ever engaged the attention of mankind—this greatest of all mysteries. And it will continue to be so until the mystery is solved. Out of the miseries of the late war arose a tremendous heart-hunger for more light—more definite knowledge concerning the hereafter. In response to this feeling such pictures as Earthbound were produced. Other photoplays dealing with the same subject but with less dramatic power followed in rapid succession. Palmer Plan Handbook, 1922  

==References==
 

==External links==
*  

 

 
 
 