My Giant
 
{{Infobox film
|name=My Giant
|image=My giant poster.jpg
|image_size= 225px
|caption=theatrical release poster
|director=Michael Lehmann
| producer       = Billy Crystal
|writer=Billy Crystal David Seltzer
|starring=Billy Crystal Gheorghe Mureșan Kathleen Quinlan Joanna Pacuła
| music          = Marc Shaiman
| cinematography = Michael Coulter
|editing=Stephen Semel
|studio=Castle Rock Entertainment
|distributor=Columbia Pictures
|released=April 10, 1998 (US) February 12, 1999 (UK)
|country=United States
|gross=$8,072,007
}}
 NBA player The Princess Bride.

==Plot==
A huckster named Sammy (Billy Crystal) travels to Romania on business after splitting up with his wife. After a disappointment with his client, he crashes his car and is rescued while unconscious by an enormous Romanian man named Max (Gheorghe Mureșan) who is close to 8 feet tall.

Sammy thinks the rescuer is God, as he can only see Maxs giant hands. When Sammy wakes up, he thinks he is in heaven. But, he is confused to find a statue of Jesus next to his bed, as he was raised Jewish. He then realizes Max has brought him to a monastery, where he was raised after being given up for adoption by his parents because of his height.

Once he wakes up and interacts with Max, he sees potential stardom in him. Sammy attempts to broker his introduction into the movies. In doing so he exploits Maxs desire to visit a long-lost paramour, Lilliana (Joanna Pacuła), in Gallup, New Mexico. First, Max obtains the role of a villain in a movie, but he is so drunk that he vomits on the protagonist. However, the scene is included in the movie.

One day, Sammy talks to Steven Seagal about including Max as a villain in one of his movies, convincing him that he needs a different kind of villain. At first Seagal rejects him because there was another actor who would take that role, but he changes his opinion after listening to an extract of a Shakespearean play done by Max.

Suddenly, after some medical exams, Max is diagnosed with heart disease which cannot be treated with a transplant because Maxs heart is so big. Sammy decides to find Lilliana, and tries to convince her to meet Max again, but she rejects the invitation. Sammy then convinces his wife to take the role of Lilliana and after some words, Max asks her for a kiss.

Afterwards, Sammy and his wife decide to get back together again, after realizing that they were truly in love and that their son needed them.

Sammy eventually decides to return Max home to Romania. Max refuses to go back, but finally he enters his old house, and meet his parents again, who abandoned him. Sammy ends up watching Maxs first filmed scene in a cinema with his family. Max dies shortly after, because of his heart, but changed many peoples lives forever.

==Cast==
* Billy Crystal as Sammy Kamin
* Gheorghe Mureșan as Maximus Zamfirescu
* Kathleen Quinlan as Serena Kamin
* Joanna Pacuła as Liliana Rotaru
* Zane Carney as Nick Kamin
* Dan Castellaneta as Partlow
* Steven Seagal as himself

==Reception==

===Box office===
The movie was not a box office success, grossing a little over $8 million domestically, far less than its $20 million budget.  

===Critical===
The movie gained mostly negative reviews,     and has a 19% rotten rating on review aggregate Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 