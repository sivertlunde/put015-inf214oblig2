Roar of the Iron Horse
{{Infobox film
| name           = Roar of the Iron Horse
| image          = Roar of the Iron Horse.jpg
| image_size     =
| caption        = Thomas Carr
| producer       = Sam Katzman
| writer         = George H. Plympton Sherman L. Lowe Royal K. Cole (story/screenplay)
| narrator       = Knox Manning Jock OMahoney William Fawcett Hal Landon
| music          =  Mischa Bakaleinikoff (musical director) John Leipold (original music) Fayte Brown (B&W) Earl Turner
| distributor    = Columbia Pictures (U.S.)
| released       =  
| runtime        = 15 episodes 260 minutes
| country        = United States English
| budget         =
| gross          =
}} serial released by Columbia Pictures.

==Plot==
Roar of the Iron Horse had hero Jock Mahoney in his first leading role, playing Jim Grant, a railroad agent, opposing a typical hardfaced German, George Eldred, called The Baron. Head of a strong ring in America, the infamous Baron was thwarted time and time again as he tried to sabotage the building of the transcontinental railroad with all the means to his scope, strategically bribing the local Indians into doing his dirty work. Mahoney was an ideal choice to play the lead character, and the role marked the beginning of a long and rewarding association with Columbia Pictures.

==Cast== Jock OMahoney as Jim Grant
* Virginia Herrick as Carol Lane William Fawcett as Rocky Hal Landon as Tom Lane Jack Ingram as Homer Lathrop
* Mickey Simpson as Cal
* George Eldred as Karl Ulrich/The Baron 
* Myron Healey as Ace
* Rusty Wescoatt as Scully Frank Ellis as Beefy
* Pierce Lyden as Erv Hopkins
* Dick Curtis as Campo
* Hugh Prosser as Lefty
* Tommy Farrell as Del
* Bud Osborne as Outlaw
* Rick Vallin as White Eagle

==Critical reception==
Cline writes that Roar of the Iron Horse was an outstanding release of 1951. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 40
 | chapter = 3. The Six Faces of Adventure
 }} 

==Chapter titles==
# Indian Attack
# Captured by Redskins
# Trapped by Outlaws
# In the Barons Stronghold
# A Ride for Life
# White Indians
# Fumes of Fate
# Midnight Marauders
# Raid of the Pay Train
# Trapped on a Trestle
# Redskins Revenge
# Plunge of Peril
# The Law Takes Over
# When Killers Meet
# The End of the Trail
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 251–252
 | chapter = Filmography
 }} 

==See also== List of film serials by year
*List of film serials by studio

==References==
 

==External links==
* 
* 
* 

 
{{succession box  Columbia Serial Serial 
| before=Pirates of the High Seas (1950)
| years=Roar of the Iron Horse (1951) Mysterious Island (1951)}}
 

 

 
 
 
 
 
 
 