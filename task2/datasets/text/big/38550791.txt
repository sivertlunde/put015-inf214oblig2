Save Your Legs!
{{Infobox film
| name           = Save Your Legs!
| image          = 
| caption        = 
| director       = Boyd Hicklin Robyn Kershaw
| writer         = Brendan Cowell
| story          =  David Lyons Stephen Curry
| music          = Cornek Wilczek
| cinematography = Mark Wareham
| editing        = Leanne Cole
| studio         = Indian Take One Productions Nick Batzias Productions
| distributor    = Madman Entertainment
| released       =  
| runtime        = 92 minutes
| country        = Australia India
| language       = English
| budget         = 
| gross          = 
}} Stephen Curry, Damon Gameau and Brendan Cowell. The film is inspired by a 2005 documentary of the same name. It follows the story of Edward "Teddy" Brown and his two mates of a Melbourne cricket team, who travel to India for a tour.    The film premiered at the Melbourne International Film Festival on August 19, 2012. 

==Plot==
Edward Teddy Brown lives for his suburban cricket club and his two best friends, Rick and Stavros. But when he realizes that his beloved team mates are moving on and growing up, hes forced to take matters into his own hands and remind them of just how good cricket can be.
 
Rallying a boyhood dream and his own teenage obsession, 35 year old Teddy leads his very ordinary cricket team into the extraordinary heart of India, on an audacious three match tour and a mission to meet cricketing legend, Sachin Tendulkar. While Teds best-laid plans are brought undone by the chaos of India, stumps fly, friendships fray and a life-changing comic adventure unfolds.
 
The dream tour becomes a nightmare and the men are forced to face the realities of their friendship, confront their fears and Ted has to learn to move with the changing times.

==Cast==
*Damon Gameau .... Stavros Stephen Curry ... Edward "Teddy" Brown
*Brendan Cowell ... Rick
*Brenton Thwaites ... Mark David Lyons ... Prince
*Pallavi Sharda ... Anjali
*Darshan Jariwala ... Sanjeet Thambuswanny
*Sid Makkar ... Rai

==Production== Victorian government supported the film as a way to promote trade between India and Australia.  Filming began on December 12, with extensive shoots in India in early 2012.

==Critical Reception==
Ed Gibbs of the Sydney Morning Herald gave it 4 out of 5 and stated that "this feel-good Australian comedy knocks it into the stands - even for viewers with no interest in cricket". 

==Box office==
Save Your Legs! opened across 176 screens for distributor Madman, taking $165,000, for a screen average of just $936 per screen. 

The film is due for a UK theatrical and DVD release in June 2014 under the revised title of Knocked For Six.

==References==
 

==External links==
 
* 
* 
* 

 
 


 
 