Funes, a Great Love
{{Infobox film
| name = Funes, un gran amor
| image = Funes,ungranamor.jpg
| image_size =
| caption =
| director = Raúl de la Torre
| producer = Raúl de la Torre
| writer = Humberto Constantini (novel) Ugo Pirro
| narrator =
| starring = Graciela Borges Moria Casán Andrea Del Boca Gian Maria Volonté
| music = Charly García Raúl de la Torre
| cinematography = Juan Carlos Desanzo
| editing = Norberto Rapado Marcela Sáenz
| distributor = Raúl de la Torre Producciones
| released = April 1, 1993
| runtime = 110 minutes
| country = Argentina
| language = Spanish
| budget = 
| gross = $48,245,593
}}
 1993 cinema Argentine film directed by Raúl de la Torre and based on a novel by Humberto Constantini. The film starred Graciela Borges, Moria Casán and Gian Maria Volonté.

== Other cast ==
* Graciela Borges: Azucena Funes/Tena
* Gian Maria Volonté: Bergama
* Pepe Soriano: Herminio
* Moria Casán
* Andrea Del Boca: Beatriz Núñez
* Rodolfo Ranni: Tito
* Nacha Guevara: Amanda
* Jairo
* Dora Baret
* Beba Bidart
* Alfredo Zemma: Master Paladino
* Juan Cruz Bordeu: Mario
* Matías Gandolfo
* Antonio Tarragó Ros
* Susana Rinaldi: La Tana
* Aníbal Vinelli
* Virgilio Expósito
* Daniel Binelli
* Roberto Amerise
* Carlos Buono
* Juan Carlos Copes
* María Nieves
* Teresa Brandon
* Carol Ilujas
* María José Gabín
* Argentinita Vélez
* José Andrada
* Carlos Broggi
* Constantino Cosma
* Raúl de la Torre

== External links ==
* 

 
 
 
 
 
 
 
 
 

 
 