Junglee
 
 
{{Infobox film
| name           = Junglee
| image          = Junglee 1961 film poster.jpg
| image_size     =
| caption        = Film poster
| director       = Subodh Mukherjee
| producer       = Subodh Mukherjee Agha Jani (dialogue) Subodh Mukherjee (screenplay/story)
| narrator       =
| starring       = Shammi Kapoor Saira Banu Shashikala
| music          = Shankar-Jaikishan
| cinematography = N. V. Srinivas
| editing        = V. K. Naik
| distributor    =
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Hindi
| budget         =
| gross          =
}} Asit Sen. Saira Banu, in her debut film, earned a Filmfare nomination as Best Actress. 
 Telugu as "Saradaa Ramudu" (1980) starring N.T. Rama Rao and Jayasudha. 

==Plot==
Chandrashekhar (Shammi Kapoor), or Shekhar as he is called now, belongs to an aristocratic family, who believe laughter is a sign of belonging to the lower class, and as a result laughter is not encouraged in the household, run by Shekhars domineering mother (Lalita Pawar). In this household the only one who dares to laugh and enjoy life is Shekhars sister Mala (Shashikala), who also dares to love Jeevan (Anoop Kumar). Things change when their mother finds out about Malas indiscretions, and asks Shekhar to take her away from their home, to distant and scenic Kashmir, for a while. She instructs Shekhar to get married to a girl from a princely family. When Shekhar and Mala go to Kashmir, they meet with beautiful Rajkumari (Saira Banu). And after being stranded with her for a few nights during a snowstorm, he realizes what he has been missing, and becomes a care-free man in love. This change is welcomed by Mala, but not by their mother. To make matters worse, Rajkumari does not belong to a princely family, and Shekhar knows that his mother will never approve of his marrying her.

==Cast==
*Shammi Kapoor ...  Chandra Shekhar kapoor
*Saira Banu ...  Rajkumari (as Saira Banoo)
*Shashikala ...  Mala (as ShashiKala)
*Anoop Kumar ...  Jeevan
*Lalita Pawar ...  Shekhars Mother
*Azra ...  Rajkumari the Princess
*Moni Chatterjee ...  Doctor (Rajs Father) (as Moni Chatterji) Asit Sen ...  Doctor Uncle
*Rajan Haksar ...  The Princess Brother (as Rajen Haksar)
*Shivraj ...  Manager
*Helen ...  Miss Suku

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kashmir Ki Kali Hoon Main"
| Lata Mangeshkar
|-
| 2
| "Nain Tumhare Mazedar"
| Mukesh (singer)|Mukesh, Asha Bhonsle
|-
| 3
| "Ehsan Tera Hoga Mujh Par"
| Lata Mangeshkar
|-
| 4
| "Din Sara Guzara Tore Angna"
| Mohammed Rafi, Lata Mangeshkar
|-
| 5
| "Aai Aai Ya Sukoo Sukoo"
| Mohammed Rafi
|-
| 6
| "Chahe Mujhe Koi Junglee Kahen"
| Mohammed Rafi
|-
| 7
| "Ja Ja Ja Mere Bachpan"
| Lata Mangeshkar
|-
| 8
| "Ehsan Tera Hoga Mujh Par"
| Mohammed Rafi
|}

==Awards and nominations==
*Filmfare Best Sound Award - Kuldeep Singh
*Filmfare Nomination for Best Actress - Saira Banu 

==References==
 
 
 
 
 
 