SOS Pacific
{{Infobox film
| name           = SOS Pacific
| image_size     =
| image	=	SOS Pacific FilmPoster.jpeg
| caption        = Guy Green
| producer       = Patrick Filmer Sankey John G. Nasht
| writer         = Gilbert Travers Thomas (story) Bryan Forbes (dialogue)
| starring       = Richard Attenborough Pier Angeli Eva Bartok
| cinematography = Wilkie Cooper Arthur Stevens	 	
| distributor    = Rank Organization
| released       = December, 1959
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
}} 1959 Cinema British drama Guy Green and starring Richard Attenborough and Pier Angeli. The film was shot in black and white, but later underwent colourisation.

==Plot synopsis==
A  ); his prisoner Mark (Eddie Constantine); Whitey Mullen (Richard Attenborough), a witness against Mark; Dr Strauss, a German scientist (Gunnar Möller); Miss Shaw, a middle-aged Englishwoman (Jean Anderson) and Maria, a young European woman (Eva Bartok). The plane comes down near an island. The navigator has been killed by toxic gas produced when the wrong kind of extinguisher is used on an electrical fire aboard the plane but the others make it to land in two rubber dinghies. Just offshore a fleet of derelict ships is anchored. On the island are two concrete bunkers. In one,  a number of goats are tethered. The other, which is lead-lined, contains cameras and measuring instruments.  The cameras are trained on a device standing on a smaller island some distance away. The castaways realise that they are in the middle of an H-Bomb testing range and that a bomb is to be detonated in a few hours.

==Cast==
*Richard Attenborough - Whitey Mullen
*Pier Angeli - Teresa
*John Gregson - Capt. John Jack Bennett
*Eva Bartok - Maria
*Eddie Constantine - Mark
*Gunnar Möller - Dr. Strauss
*Jean Anderson - Miss Shaw
*Cec Linder - Willy
*Clifford Evans - Petersen
*Harold Kasket - Monk
*Andrew Faulds - Sea Captain
*Cyril Shaps - Louis (uncredited)

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 