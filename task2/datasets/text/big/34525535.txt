Moondru Per Moondru Kadal
{{Infobox film
| name           = Moondru Per Moondru Kaadhal
| image          = MPMK poster.jpg
| caption        = 
| director       = Vasanth
| producer       = Bharath Kumar Mahendhiran Maha Ajay Prasath
| story          = Vasanth
| screenplay     = Shankar Raman Vasanth Arjun Cheran Cheran Vimal Muktha Bhanu Surveen Chawla Lasini
| music          = Yuvan Shankar Raja
| cinematography = Bhojan K. Dinesh
| editing        = S. N. Fazil
| studio         = Mahendra Talkies
| distributor    = 
| runtime        = 
| released       =  
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Muktha Bhanu and newcomers Surveen Chawla and Lasini.    The film features musical score by Yuvan Shankar Raja. It released on 1 May 2013 worldwide and opened with mixed reviews.    It was a failure at the Box Office.

==Plot==
Varun (Vimal) and Anjana’s (Lasini) love story happens in the Ooty mountains (in Tamil literature, they are referred to as Kurinji), Guna (Cheran) and Mallika’s (Banu) story happens in Tuticorin seashore (Neidhal) & Harris (Arjun) and Divya’s (Surveen) story happens in the city (Marudham). The story revolves around these couples and the trials and tribulations they face.

Varun explains the story of his life where he had fallen with Anjana in spite of knowing that her engagement has been called off, but later sacrifices his love after hearing the story of Guna and Mallika. Guna is a philanthropist who runs an organisation called "Punnagai" a rehabiliation centre for jail prisoners. Mallika, a physiotherapist silently pines love for him. When Guna failed to unite a prisoner (Aadukalam Naren) with his family, Mallika does the job. Impressed with her he handles the responsibility of taking care of Punnagai to her and leaves the town.

Another story is of Paul Harris (Arjun), a swimming coach and his student-cum-lover Divya (Surveen) (Varun claim that Divya is his childhood friend but there is no scene where they are together). Divya is disappointed for losing the swimming match, her father (Thambi Ramaiah) advises another coach Ilango (John Vijay) to replace Paul Harris with another coach. But Divya is adamant that Harris should be her coach. Meanwhile, Harris gets involved in a bike accident where his legs and hands has been hurt but he encourages Divya to take part in Olympics swimming match. Divya practices for the match rigorously and wins the competition by finishing it in less than 56 seconds.

In 2016, Varun narrated these stories because these incidents prompted him to write a novel called "Moondru Kaadhal". Paul Harris arrives at the press conference of book release and tells that he is alive because of Divya, she is the epitome of goodness and confidence and he narrates that though Divya had won the match but she died due to heart attack on the swimming pool. The film ends with Harris uttering Kaadhal Ketpadhalla Koduppadhu ("Love is not to be asked but to be given").

== Cast ==
  Arjun as Paul Harris Rosario Cheran as Gunasekar
* Vimal as Varun Muktha Bhanu as Mallika
* Surveen Chawla as Divya
* Lasini as Anjana
* Thambi Ramaiah as Thiruvengadam (Divyas father) Sathyan
* Appukutty
* John Vijay as Elanko
* Aadukalam Naren
* Ravi Raghavendra as Varuns father
* Boys Rajan as Doctor
* Shanthi Williams as Gunas mother
* Ritvik Varun in a special appearance
* Robert in a special appearance
* Blaaze in a special appearance
 

==Production==
In July 2011, reports confirmed that Vasanth would start directing his next film, titled as Moondru Per Moondru Kaadhal, four years after his last film Satham Podathey was released.    Vasanth reportedly had been waiting with the script for three years to "get the right actors".  He further noted that the film was not a love story but a "story about love".    The film would consist of three love stories featuring the three lead characters, which play out in three landscapes: by the seaside, in the mountains and on the plains.    Bharath Kumar, a fan of Vasanths films, came forward to produce the film along with his friends under his newly launched Mahendra Talkies banner.   Moondru Per Moondru Kaadhal was started on August 9, 2011. 
 Cheran for Shiva might Muktha Bhanu, National Film Award winning actors Thambi Ramaiah and Appukutty were selected for supporting roles, providing comic relief. 
 Ooty to film major portions.  Another schedule was later held in Hyderabad, India|Hyderabad. 

==Music==
{{Infobox album
| Name = Moondru Per Moondru Kaadhal
| Type = Soundtrack
| Artist = Yuvan Shankar Raja
| Cover  = Moondru Per Moondru Kaadhal audio CD cover.jpg
| Released = 1 December 2012 Feature film soundtrack
| Length = 29:48 Tamil	
| Label = Gemini Audio
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album = Aadhalal Kadhal Seiveer (2012)
| This album = Moondru Per Moondru Kaadhal (2012)
| Next album = Samar (2013 film)|Samar (2012)
}}

Vasanth roped in Yuvan Shankar Raja as the composer, who he had previously worked with in Poovellam Kettuppar (1999) and his last project Satham Podathey (2007). In an interview, the director stated that Yuvan Shankar Raja had put in about seven months of work for this film, while describing the genre of the soundtrack as post-modern music.  The soundtrack album features six songs with lyrics penned by Na. Muthukumar.

Composing was started even before the film was launched, with the first song being recorded on August 9, 2011.  Popular Bollywood singer Sonu Nigam originally sang the melody titled "Mazhai Mazhai Mazhaiyo Mazhai", which was recorded in Mumbai.  The final version, however, featured Karthik (singer)|Karthiks vocals. Neha Bhasin, who had sung the well received song "Pesugiren" in Satham Podathey before, lent her voice for another song in the album.  Nandini Srikar of Ra.One fame crooned a solo "Aaha Kadhal", which, too, was recorded in Mumbai,  while composer Ramesh Vinayagam sang a peppy number titled "Stop the Paatu" for the album.  A teaser trailer of "Stop The Paatu" was released online in September 2012 and garnered positive response.  The composer himself performed the song "Unakkagave" which was dubbed as the first dubstep track in Tamil cinema.  Yuvan Shankar Raja later revealed that he had not composed a single new tune for the film, but that all were stock songs he had earlier recorded which were chosen by Vasanth.  The final mixing was done by Kausikan Sivalingam in Berlin, according to Vasanth. 
 Sasi and actors Shanthanu Bhagyaraj, Kushboo Sundar|Kushboo, Krishna Kulasekaran|Krishna, Prakash Raj were present at the function which was host by television anchors Ma Ka Pa Anand and Divyadarshini.  The songs from the album were also performed live on stage by the original artists. 

The album received very positive critical response. Milliblog wrote: "Vasanth and Yuvan’s combination continues to rock!.  Indiaglitz wrote: "A good collection of songs at the end of the year, almost appears as if it has been picked out of good songs".  Behindwoods wrote: "Overall, the album might have been described as ‘post-modern’ by the director,  Yuvan Shankar Raja does things his own way, and does it mighty good".  S. Saraswathi from Rediff wrote "Music by Yuvan Shankar Raja is the only saving grace of an otherwise ordinary film. Every song has its own unique flavour and is incredible" and described the music as "exceptional".    Sify stated "Yuvan Shankar Raja`s music and background score is terrific with right mixture of melodies and fast numbers with `Aaha Kathal` and `Mazhai Mazhai` being the pick of the lot".   

{{tracklist
| headline        = Track listing 
| extra_column    = Singer(s)
| total_length    = 29:48
| all_lyrics      = Na. Muthukumar

| title1          = Kaadhal Endhan Kaadhal
| extra1          = Neha Bhasin
| length1         = 5:02
| title2          = Unakkaagave Uyir Vaazhgiren
| extra2          = Yuvan Shankar Raja
| length2         = 4:37
| title3          = Padapadakkudhu Maname
| extra3          = Krish (singer)|Krish, Blaaze
| note3           = English rap lyrics by Blaaze
| length3         = 5:46
| title4          = Aaha Kaadhal Konji Pesudhe
| extra4          = Nandini Srikar
| length4         = 4:08
| title5          = Mazhai Mazhai
| extra5          = Karthik (singer)|Karthik, Shweta Mohan
| length5         = 5:55
| title6          = Stop The Paatu
| extra6          = Ramesh Vinayagam
| length6         = 4:20
}}

==Release== Zee Thamizh. Ethir Neechal and Soodhu Kavvum. 

===Critical reception===
Moondru Per Moondru Kadhal received mixed reviews from film critics.  S Saraswathi from Rediff gave 2.5 stars out of 5 and wrote "Moondru Per Moondru Kadhal lacks the intensity expected of a Vasanth film, but makes up for it with its exceptional music.  Sify said "MPMK is technically chic and Vasanth has made an interesting film which has its shares of romantic break-up`s and heart breaks" and rated the film as average.  Behindwoods gave 2 stars out of 5 and stated "Vasanth’s Moondru Per Moondru Kadhal, although eulogizes love, works in parts with music and camera helping the story but his style has not been felt powerfully in the film.  Indiaglitz wrote "To sum up, the directors usual foray is missing and might work in bits with the help of some breezy music, help from the cast and some good cinematography".  in.com rated the film 2.5 out of 5 and wrote "MPMK is not entirely bad and those who love pure romantic flicks can watch it once provided if you can sit through the bland first hour of the film." 

==Further reading==
*  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 