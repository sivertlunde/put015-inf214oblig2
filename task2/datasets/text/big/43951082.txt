L.A. Slasher
 
{{Infobox film 
| name           = L.A. Slasher
| image          = L.A. Slasher Poster.jpg
| image_size     = 
| alt            = 
| caption        = Film poster
| director       = Martin Owen
| producer       = Daniel Sollinger Abigail Wright
| writer         = Martin Owen Abigail Wright Elizabeth Morris Tim Burke
| starring       = Mischa Barton   Dave Bautista   Andy Dick   Eric Roberts   Danny Trejo   Abigail Wright   Frank Collison   Marisa Lauren   Drake Bell   Tori Black   Brooke Hogan
| music          = Mac Quayle
| cinematography = Chase Bowman
| editing        = Keith Croket Emanuele Giraldo
| studio         = JJS Films
| distributor    = JWright Productions
Date| released       = {{Film date|2015|06|12|
}}
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
L.A. Slasher is an upcoming slasher film written and directed by Martin Owen.

==Plot==
Incensed by the tabloid culture which celebrates it, the L.A. Slasher publicly abducts a series of reality TV stars, while the media and general public in turn begin to question if society is better off without them. A biting, social satire about reality TV and the glorification of people who are famous for simply being famous, "L.A. Slasher" explores why it has become acceptable and even admirable for people to become influential and wealthy based on no merit or talent - purely through notoriety achieved through shameful behavior.

==Cast==
*Mischa Barton as The Actress
* Dave Bautista as The Drug Dealer #1
* Andy Dick as The Slasher (voice)
* Eric Roberts as The Mayor
* Danny Trejo as The Drug Dealer #2
* Abigail Wright as The Reporter
* Frank Collison as Killers Anonymous Leader
* Marisa Lauren as The Stripper
* Drake Bell as The Pop Star
* Elizabeth Morris as The Heiress
* Tim Burke as The Producer
* Tori Black as The Teen Mom
* Brooke Hogan as The Reality Star
* Korrina Rico as The Socialite

==Production==
A theatrical trailer was released on 24 September 2014. 

==Release==
The Film is to be released June 12, 2015.

==References==
 

==External links==
* 

 