Sting of the West
{{Infobox film
| name = Sting of the West aka Tedeum
| image = Tedeum film.jpg
| director = Enzo G. Castellari
| writer =
| starring =  Jack Palance
| music = Guido & Maurizio De Angelis Manuel Rojas
| editing =
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language =
| budget =
}} 1972 Cinema Italian spaghetti western film directed by Enzo G. Castellari. The title role was initially offered to Tomas Milian, who eventually refused.   

== Cast ==
* Jack Palance as Buck Santini/Father Jackleg
* Giancarlo Prete as Tedeum (as Timothy Brent)
* Lionel Stander as Stinky Manure
* Francesca Romana Coluzzi as Betty Brown Riccardo Garrone as Sheriff
* Maria Vico as Ma Manure
* Eduardo Fajardo as Mr. Grant
* Renzo Palmer as Rags Manure
* Mabel Karin as Wendy Brown

==Plot==
Tedeum and his parents and granddad are all con men travelling around in a sailboat on wheels. When he inherits a mine from another well-known con man he assumes that it is false and tries to sell it. His first attempts turns out to be at the Texas’ sheriff convention and he has to escape together with another con man who is disguised as a monk. However, the mine does in fact exist, and the big boss Grant is out for it, though his confrontations with Tedeum usually ends in him losing his pants. Two female con men, the strong Betty and the beautiful Wendy, also get involved. In the end we find Grant, Tedeum, the monk and Tedeum’s father as inmates of Laredo prison – because the mine lies right under it!

==Reception==
In his investigation of narrative structures in Spaghetti Western films, Fridlund describes Sting of the West as one of the most hardcore followers of They Call Me Trinity and Trinity Is Still My Name in its employment of smell and gluttony jokes, con men (and women), (fake) religion and low comedy.  

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 