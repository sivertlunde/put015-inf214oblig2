Arcadia of My Youth
 
{{Infobox film
| name           = Arcadia of My Youth
| image          = Arcadia-of-My-Youth DVDcover.jpg
| image size     = 
| border         = 
| caption        = DVD cover for Arcadia of My Youth
| film name      = {{film name
| kanji          = わが青春のアルカディア
| kana           = 
| romaji         = Waga Seishun no Arukadeia}}
| director       = Tomoharu Katsumata
| producer       = Chiaki Imada
| screenplay     = Yōichi Onaka
| story          = Leiji Matsumoto
| starring       = 
| music          = Toshiyuki Kimori
| cinematography = Kenji Machida
| editing        = Yutaka Chikura
| studio         = Toei Company|Tōei Tokyu Group|Tōkyū Agency
| distributor    = Tōei
| released       =  
| runtime        = 130 minutes    
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is an anime film depicting the origin of Leiji Matsumotos seminal character Captain Harlock. At one time, it was considered to be the central hub of the so-called Leijiverse with other works such as Galaxy Express 999 and 1978s Space Pirate Captain Harlock television series occurring sometime after. It is directed by Tomoharu Katsumata, with Kazuo Komatsubara as animation director. 

==Plot==
:"At the end of their lives, all men look back and think that their youth was Arcadia (utopia)|arcadia." — Goethe

During the early 20th Century, some time after World War I, aerial explorer Captain Phantom F. Harlock is embarking on what is to be the magnum opus of his long career, the traversing of the Owen Stanley Mountains in New Guinea. His major obstacle is the phantom of the witch that haunts these mountains. In a last desperate attempt to cross the mountains, Harlock ditches all but ten minutes of fuel in order to gain altitude. He carries on to his fate, amidst the mocking laughter of the Owen Stanley Witch.

Near the end of World War II, Phantom F. Harlock II is an Iron Cross-wearing German fighter pilot that flies a Messerschmitt Bf 109. During a major defeat for his side, he meets Toshiro (Tochiro) Oyama, a Japanese exchange technician working in Germany. Tochiros project is to design a new gun sight for use in fighter planes. Harlocks most treasured possession is the Revi C-12D gun sight which he calls his "eye". Both men believe the war to be wasteful and pointless and Tochiro especially hopes that the rockets being developed by Germany may one day lead to a more positive application, such as a trip to the moon. Tochiro wishes he could escape from the war and possibly go to neutral Switzerland where he might be able to fulfill his dream. Harlock offers to take him in his plane. Stowing Tochiro in the planes fuselage, Harlock is able to fly to Switzerland, but only after surviving a fierce aerial dogfight which disables his plane and forces him to crash land just short of the Swiss border. Carrying an injured Tochiro across the river to sanctuary, Harlock departs, giving Tochiro his Revi C-12D, before crossing back into the warzone where Harlock expects to face the consequences of his actions. Although this Harlocks ultimate fate is unknown, Tochiro pledges friendship between their two bloodlines for all eternity.
 battle cruiser aliens from the Illumidus Empire have conquered Earth and enslaved humanity. However the remaining humans blame him and other warriors like him as they were not there to protect the planet when they needed it. Harlock, along with the Tokargans who are ashamed of their role in Earths downfall, sets out to lead a resistance against the aliens and adopts the fighting strategy of ancient marauders. During the course of the struggle, Harlock meets a former Solar Federation engineer; a Japanese man who hes never met before but feels he knows. This man, Tochiro Oyama reveals his secret project, the dreams of all his ancestors. Hidden in a deep cavern under the occupation headquarters is a space battleship that he designed and built. This ship is called Arcadia, in honor of the eternal friendship forged between Phantom F. Harlock and Tochiro Oyama during another ancient war.

Harlock also meets Queen Emeraldas|Emeraldas, an old friend, who immediately offers herself to the cause. The Tokargans, after witnessing the death of the last female of their race, sacrifice themselves to save the Arcadia from the life sucking flames of the Flame Stream Prominence (aka the Owen Stanley Witch of Space). Harlocks lover and voice of the Underground, Maya is killed by Illumidas gunfire. After Herlock has honorably defeated the occupational commander in ship-to-ship combat, the quisling ruler of Earth, Triter, nonetheless declares Harlock and Emeraldas outlaws and exiles them to space. Amidst an Earth that prefers servitude to their new masters over the hard but noble fight for freedom, Harlock, Emeraldas, Tochiro, and their new pirate crew of idealists and romantics set for the stars, heading out for parts unknown.

==Production Notes== The New World Symphony by Antonín Dvořák. This film starred acclaimed Japanese actor Yujiro Ishihara in his final role before his death in 1987. He was the voice of Phantom F. Harlock I. It was also his only role in an animated movie.   

Arcadia of My Youth draws many parallels to the German occupation of France during World War II as well as the post World War II American occupation of Japan. Mayas nickname as "The Rose" and the "Voice of Free Arcadia" mirrors the romantic image of the secret radio broadcasters of the French Resistance. 

The prologue of this movie does not make it clear whether or not Phantom F. Harlock survived his mission. He is assumed to have survived as his voiceover monologue indicates that he is reading from his autobiography which is titled Arcadia of My Youth. 

This movie is controversial in that Phantom F. Harlock II was portrayed as a German fighter pilot during WWII. The Tochiro of that era, who had read Harlock Is book, at least found it unusual in that he asked Harlock II why he was flying an Iron Cross plane. The liner notes for the Animeigo DVD state that Harlocks response, "Just paying rent", hid a deeper meaning. It refers to the ancient feudal obligations a noble had to his lords for better or worse, even if he didnt believe in their cause. The liner notes also mention that Harlock technically was no longer bound by these obligations since the feudal customs no longer existed at that time.   
   Greek paradise in the Peloponnesus. Harlock gives the proper name of his ancestral birthplace as Hellingstadt meaning that Arcadia is most likely a nickname.  During the WWII segment of the movie, Harlock II, while flying to Switzerland muses about Arcadia, leading viewers to assume that Arcadia was located there. However Harlocks cryptic comments about "paying rent" aside, this would still make little sense in the context of the movie as Switzerland was neutral during WWII. Matsumoto has never given a concrete definitive statement on Harlocks ethnicity or heritage However, Harlock is an actual name of Prussian origin. 

The stories of Phantom F. Harlock I and II are loosely based on short manga stories by Matsumoto. Many of them appeared in his manga anthology The Cockpit.    Many elements of Arcadia of My Youth were borrowed from his other stories such as the Deathshadow which appeared in a 1970 manga and in his later adaptation of the original Space Battleship Yamato where the Deathshadow appears along with Susumu Kodais brother whose alias is Captain Harlock. These elements did not make it into the anime version but they were the source of American fan rumors that the Leijiverse Harlock was actually Mamoru Kodai in disguise or possibly a descendant.

The origin story in the 1978 television series (episodes 30 and 31) depicts Harlock and Tochiro as childhood friends and Emeraldas meeting them both later. In Arcadia of My Youth, Emeraldas and Harlock are old friends while he is just meeting Tochiro. Furthermore, the 1978 TV series also shows the Arcadia (this time blue) being constructed on planet Heavy Meldar, not Earth. Also, Zoll of Tokarga, who made a guest appearance in Episode 21 of that series, is totally reimagined for Arcadia of My Youth.
 Heidelberg duelling scar gives rise to the widespread fan assumption that the scar must be hereditary, though this is medically implausible. So far, no story has ever depicted how Harlock got his scars, though Arcadia of My Youth does depict Emeraldas receiving her scar from a glancing shot from the gun of Illumidas officer Muriguson. The four-episode Queen Emeraldas OVA series would re-invent her scar as a wound received during a sword duel with a different opponent.

The proper translation of the Japanese title is a source of dispute. Although Animeigos "Arcadia of My Youth" has been widely accepted, the original "My Youth in Arcadia" was used in Japan and better reflects the inspiration from the Goethe quote.

Reportedly, character and concept creator Leiji Matsumoto owns an actual Revi C-12D gunsight that was used by the animators as visual reference when creating the one shown in the film.   

==Cast of Characters==
*Captain Harlock/Phantom F. Harlock II: (Makio Inoue) 
*Toshiro Ōyama / Tochirō: (Kei Tomiyama)
*Queen Emeraldas: (Reiko Tajima)
*Maya: (Reiko Mutō) 
*La Mime: (Yuriko Yamamoto) 
*Zeda: (Taro Ishida|Tarō Ishida) 
*Zoll: (Shūichi Ikeda) 
*Muriguson: (Takeshi Aono) 
*Old Tokargan Soldier: (Shūichirō Moriyama) 
*Triter: (Hitoshi Takagi) 
*Tori-san: (Hiroshi Ōtake) 
*Mira: (Hiromi Tsuru) 
*The Witch: (Eiko Masuyama) 
*Black-suited Commander: (Hidekatsu Shibata) 
*Illumidus Officer: (Kōji Yada)
*Phantom F. Harlock I: (Yujiro Ishihara)

==Video and DVD Releases==
Arcadia of My Youth was originally released on North American home video by Celebrity Home Entertainment under the "Just For Kids" banner. It was initially retitled Vengeance of the Space Pirate and was for many, the first introduction to Captain Harlock. This release suffered from having about forty minutes cut out of it, most notably the lengthy pre-title prologue concerning one of Harlocks ancestors attempt to fly over the tallest peak (known as the Stanley Witch) of the Owen Stanley Mountain Range in New Guinea(as read for inspiration by Harlock from said ancestors autobiography, Arcadia of My Youth, moments before the Illumidas attack on the Deathshadow). The film was later released by Best Film and Video along with many of the other anime released under the Just For Kids label. It was given the alternately translated title of "My Youth in Arcadia" and features all the cut footage restored.  Another, more likely possibility is that My Youth in Arcadia is the original version (on Best Film & Videos first issue it was billed The original Vengeance of the Space Pirate), probably an international dub, a la Tohos Super Spacefortress Macross version of the Macross movie, Do You Remember Love? (released here by Best Films & Video; their first issue was billed The original Clash of the Bionoids, referring to the edited Celebrity Just For Kids version).

A subtitled Laserdisc was releaed in North America by Animeigo on April 12, 1995. 

Arcadia of My Youth was also released by Animeigo in 2003. It is the only Harlock story which shows how Harlock actually lost an eye (it also reveals how Emeraldas got her facial scar, making her a virtual distaff doppelganger of Harlock). One of the trailers for Arcadia of My Youth (one of two included as extras in Animeigos DVD release) has one of the most confusing and misleading scenes, a clip of the Tokargan Zoll shooting out Harlocks eye. Zoll, in the movie, is actually an enemy-turned-staunch-ally and Harlocks injury is caused by Illumidas gunfire. This trailer scene was recreated for the boxes on the American VHS releases of Vengeance of the Space Pirate and My Youth in Arcadia.

==Sequel TV Series==
The fight against the Illumidas is continued in the 1982–1983 TV series Endless Road SSX (or Endless Orbit SSX). This series (never licensed in English) suffered low viewer ratings as it aired during the waning years of the Matsumoto boom that started in the 1970s with Space Battleship Yamato. By this time, Japanese audiences interest had moved on to the trend in mecha shows that began after Macross and Mobile Suit Gundam. The Matsumoto boom would completely die off by 1983 with the release of Final Yamato and aside from a few isolated one-off projects, no new regularly produced anime based on Matsumoto works would be released until 1998.

==Comic Adaptation==
The American comic book company Eternity Comics published an original comic series based on Captain Harlock. It theoretically picked up where Arcadia of My Youth left off and was also written in an attempt to bridge the storylines of Galaxy Express 999 and 1978s Space Pirate Captain Harlock. The comic also featured several significant story arcs titled Deathshadow Rising, Fall of the Empire, and The Machine Men which would theoretically lead into the Galaxy Express 999 stories. The comic series ran from 1989 to 1992 and was scripted by Robert W. Gibson and drawn by Ben Dunn and Tim Eldred. 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 