The Killers (1956 film)
{{Infobox film
| name           = The Killers
| image          = Titlecard the killers.jpg
| image_size     = 
| alt            = 
| caption        = Title card
| director       = Marika Beiku Aleksandr Gordon Andrei Tarkovsky
| producer       = 
| writer         = 
| screenplay     = Aleksandr Gordon Andrei Tarkovsky
| based on       =  
| narrator       = 
| starring       = Yuli Fait Aleksandr Gordon Valentin Vinogradov Boris Novikov Yuri Dubrovin Andrei Tarkovsky Vasily Shukshin
| music          = 
| cinematography =  Alfredo Alvarez Aleksandr Rybin
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 19 minutes
| country        = Soviet Union Russian
| budget         = 
| gross          = 
}} Soviet and State Institute of Cinematography (VGIK).

==Plot==
The Killers is an adaptation of a short story by Ernest Hemingway. The story is divided into three scenes.  The first and third scenes were directed by Beiku and Tarkovsky, the second by Gordon.

The first scene shows Nick Adams (Yuli Fait) observing two gangsters (Valentin Vinogradov and Boris Novikov) in black coats and black hats entering a small-town diner where Adams is eating. They tell the owner, George (Aleksandr Gordon), that they are searching for the boxer Ole Andreson and that they want to kill him. They tie up Nick Adams and the cook, and wait for Ole Andreson to appear. Three customers enter the restaurant and are sent away by George. One of the customers is played by Tarkovsky, who whistles Lullaby of Birdland.

The second scene shows Nick Adams visiting Ole Andreson (Vasili Shukshin) in his hide-out, a small room. He warns Andreson about the two gangsters, but Andreson is resigned to his fate and unwilling to flee.

The third scene shows Adams returning to the diner and informing the owner of Andresons decision.

==Cast==
* Yuli Fait as Nick Adams
* Aleksandr Gordon as George
* Valentin Vinogradov as Al
* Vadim Novikov as Max
* Yuriy Dubrovin as 1st Customer
* Andrei Tarkovsky as 2nd Customer
* Vasiliy Shukshin as Ole Andreson

==Production==
Students were required to work on films in groups of two or threes due to a lack of equipment at the film school VGIK. Andrei Tarkovsky and Aleksandr Gordon asked Marika Beiku to work with them. The idea for adapting Ernest Hemingways short story was Tarkovskys. All roles were played by students of the VGIK, and the camera and lighting was handled by fellow students Alfredo Álvarez and Aleksandr Rybin.

Beiku, Gordon and Tarkovsky set up an American bar in the studio of the film school, at this time a symbol of depravity and becoming a minor attraction among students. Props were brought by students from their homes, and from relatives and friends. The film was praised by Mikhail Romm, the professor and teacher of Beiku, Gordon and Tarkovsky. 

==References==
 

==External links==
*  
*   (English sub-titles)

 
 
 

 
 
 
 
 
 
 
 
 