The Ghost City
 
 
{{Infobox film
| name           = The Ghost City
| image          =
| caption        =
| director       = Jay Marchant
| producer       =
| writer         = George W. Pyper Margaret Morris
| cinematography =
| editing        = Universal Pictures
| released       =  
| runtime        = 15 episodes
| country        = United States Silent English English intertitles
| budget         =
}}
 Western film serial directed by Jay Marchant. It is considered to be a lost film.   

==Cast==
* Pete Morrison as Laughing Larry Newton Margaret Morris as Alice Sinclair
* Al Wilson as Raymond Moreton Frank Rice as Sagebrush Hilton
* Bud Osborne as Jasper Harwell
* Lola Todd as Ginger Harwell
* Slim Cole as Mort Carley Alfred Allen as Austin Sinclair
* Princess Neela as Maria de Ortega
* Valerio Olivo as Manuel Ortega William Quinn as Henchman (uncredited)
* Frank Tomick as Henchman (uncredited)

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 