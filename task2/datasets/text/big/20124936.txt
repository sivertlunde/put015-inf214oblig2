Alvorada (film)
 
{{Infobox film
| name           = Alvorada
| image          =
| caption        =
| director       = Hugo Niebeling
| producer       = Hugo Niebeling
| writer         = Hugo Niebeling
| starring       = 
| music          = Oskar Sala Various Composers
| cinematography = Antonio Estêvão Anders Lembcke Herbert Müller
| editing        = Hugo Niebeling Gertrud Petermann
| distributor    =
| released       =  
| runtime        = 77 minutes
| country        = West Germany
| language       = German
| budget         =
}}
 Best Documentary Feature     and was entered into the 1963 Cannes Film Festival.   

==Overview==
The film offers an overview of Brazil, from the history of the country to the most recent developments at the time of its making - including developments of the industry and the new capital Brasília. It starts with an overview of the country itself and its history, and then proceeds to the social structure and social changes brought by industrialization and other development in recent years.   

==Style==
The film does neither follow the camera- and editing-conventions of documentary films in the early 1960s, nor their narrative style. Instead, it uses experimenal camera- and editing-techniques, often set to different kinds of music and electronic sounds by Oskar Sala. The voice-over-narration of the film (provided by Hugo Niebeling himself) only occasionally tells the viewer details about what they are seeing, often letting impressions speak for themselves. According to Hugo Niebeling, due to its tight connection of music and visual style, Alvorada is also his first "music film".   

==Reception==
* Melbourne International Film Festival: "A film that does not attempt to give a reasoned, historical or geographical account, but reproduces the visual and emotional impact on a sensitive mind. No technique, no angle, no method of camera transport has been left unexplored, (...) an exciting and beautiful kaleidoscope of images."  

==Awards (excerpt)== Oscar Nomination in the Category: "Documentary Feature". (Feature) 
*1963: Bundesfilmpreis - Filmband in Gold: Best Director (Hugo Niebeling) 
*1963: Bundesfilmpreis - Filmband in Gold: Best feature-length cultural- and documentary-film (Mannesmann AG) 
*1963: Cannes Film Festival - Official Selection 
*1964: Melbourne International Film Festival - Official Selection 

==References==
 

==External links==
* 

 
 
 
 
 
 