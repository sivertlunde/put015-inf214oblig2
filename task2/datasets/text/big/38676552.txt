Anak Perawan di Sarang Penjamun
{{Infobox film
| name           = Anak Perawan di Sarang Penjamun
| image          = Anak Perawan di Sarang Penjamun.jpg
| alt            =  
| caption        = Medasing (Bambang Hermanto) speaking with Sayu (Nurbani Jusuf)
| director       = Usmar Ismail
| producer       = Usmar Ismail
| screenplay         = Usmar Ismail
| based on       =  
| starring       = {{plainlist|
*Bambang Hermanto
*Nurbani Jusuf
}}
| music          = Jack Lesmana
| cinematography = Max Tera
| editing        = Soemardjono
| studio         = PERFINI
| distributor    = 
| released       =  
| runtime        = 127 minutes
| country        = Indonesia
| language       = Indonesian
| budget         = 
| gross          = 
}}
Anak Perawan di Sarang Penjamun (  for PERFINI. Starring Bambang Hermanto and Nurbani Jusuf, it follows a young woman who is kidnapped by a group of bandits, only to fall in love with their leader. The film, adapted from the 1940 novel of the same name by Sutan Takdir Alisjahbana, was repeatedly blacklisted by the Indonesian government and only saw release several years after production ended.

==Plot==
Medasing (Bambang Hermanto) is the fierce leader of a group of robbers who joined them after his parents were killed. He and his gang track the movements of a rich merchant (Rd Ismail) and his family, who are returning from a successful business trip. While the family is sleeping in the woods, Medasing and his men sneak into the camp, where they kill most of the porters and merchant. Leaving the merchants wife behind, Medasing takes the merchants daughter Sayu (Nurbani Jusuf) with him. He brings her to their hideout, where she is held captive.

Sayu discovers that, despite his fierce exterior, Medasing has a kind heart. He does not allow any of the gang to treat her improperly, and she is essentially free to act as she pleases. Over the months, Sayu and her kind heart are able to stir Medasing and prompt him to change his ways. After the members of his gang are killed in various mishaps, Medasing is gravely injured. Sayu tends to his wounds, and in return he brings her back to her village. They are married, and Medasing slowly rejoins society.

==Production== edited by three persons: Soemardjono, Janis Badar, and SK Syamsuri. Its cast included Bambang Hermanto, Nurbani Jusuf in the starring roles, as well as Bambang Irawan, Rachmat Hidayat, and Rd Ismail. 
 an adaptation of Marah Roeslis Sitti Nurbaya by Lie Tek Swie in 1941.  

==Release and reception== Lekra would Malay sentiment confrontation with Malaysia. However, the Indonesian film historian Salim Said suggests that the film was condemned not because of its plot, but because of Alsijahbanas alliance to the Socialist Party of Indonesia. 
 ensuing anti-communist purge, the Communist Party and most leftists were destroyed. Anak Perawan di Sarang Penjamun, however, remained blacklisted because of Hermantos previous membership in the Communist Party.   A VHS edition was ultimately released in the 1990s. 

A 35 mm copy is stored at Sinematek Indonesia in Jakarta. 

==Explanatory notes==
 

==Footnotes==
 

==Works cited==
 
*{{cite web
 |title=Anak Perawan di Sarang Penjamun
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-k014-79-121250_kabut-sutra-ungu
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=1 March 2013
 |archiveurl=http://www.webcitation.org/6EnR0SMwd
 |archivedate=1 March 2013
 |ref= 
}}
*{{cite web
 |title=Anak Perawan di Sarang Penjamun
 |url=http://www.worldcat.org/title/anak-perawan-di-sarang-penjamun/oclc/52067683&referer=brief_results
 |work=WorldCat
 |accessdate=2 March 2013
 |archiveurl=http://www.webcitation.org/6EoGdv7je
 |archivedate=2 March 2013
 |ref= 
}}
*{{cite journal
 |last=Anwar
 |first=Rosihan
 |author-link=Rosihan Anwar
 |title=Kerjasama Sastrawan dan Sinemawan Dirintis: "Anak Perawan di Sarang Penyamun"
 |trans_title=Pioneering Collaboration between Writers and Filmmakers: "Anak Perawan di Sarang Penyamun"
 |language=Indonesian
 |url=http://jurnalfootage.net/v4/kronik/kerjasama-sastrawan-dan-sinemawan-dirintis-anak-perawan-di-sarang-penyamun
 |work=Madjalah Purnama
 |page=3
 |location=Jakarta
 |date=April 1962
 |issue=17
 |volume=1
 |archiveurl=http://www.webcitation.org/6Eo8EyWqv
 |archivedate=1 March 2013
 |ref=harv
}}
*{{cite web
 |title=Kredit Anak Perawan di Sarang Penjamun
 |trans_title=Credits for Anak Perawan di Sarang Penjamun
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-a027-62-910466_anak-perawan-di-sarang-penjamun/credit
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |location=Jakarta
 |accessdate=1 March 2013
 |archiveurl=http://www.webcitation.org/6EnQhBXbs
 |archivedate=1 March 2013
 |ref= 
}}
* {{cite book
  | last = Mahayana
  | first = Maman S.
  | last2 = Sofyan
  | first2 = Oyon
  | last3 = Dian
  | first3 = Achmad
  | year = 2007
  | language = Indonesian
  | title = Ringkasan dan Ulasan Novel Indonesia Modern
  | trans_title = Summaries and Commentary on Modern Indonesian Novels
  | location = Jakarta
  | publisher = Grasindo
  | isbn = 978-979-025-006-2
  | url = http://books.google.com/?id=Bq8caP8yvqwC
  | ref = harv
  }}
*{{cite book
 |title=Profil Dunia Film Indonesia
 |trans_title=Profile of Indonesian Cinema
 |language=Indonesian
 |last=Said
 |first=Salim
 |publisher=Grafiti Pers
 |location=Jakarta
 |year=1982
 |oclc=9507803
 |ref=harv
}}
*{{cite web
 |title=Siti Noerbaja
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-s012-41-328345_siti-noerbaja
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=26 July 2012
 |archiveurl=http://www.webcitation.org/69Qwehsrb
 |archivedate=26 July 2012
 |ref= 
}}
 

 
 
 

 
 
 
 