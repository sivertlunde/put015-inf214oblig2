Pyaar Impossible!
{{Infobox film
| name           = Pyaar Impossible!
| image          = Pyaar-impossible-poster01.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Jugal Hansraj
| producer       = Aditya Chopra
| writer         = Uday Chopra
| starring       = Priyanka Chopra Uday Chopra Dino Morea
| music          = Salim Sulaiman
| cinematography = Santosh Thundiyil
| editing        = Amitabh Shukla
| studio         = 
| distributor    = Yash Raj Films
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Pyaar Impossible! ( ) is a 2010 Bollywood romantic comedy film directed by actor-turned-director Jugal Hansraj under the banner of Yash Raj Films. This romantic comedy film features Priyanka Chopra and Uday Chopra in the lead roles. The film also stars Anupam Kher and Dino Morea in supporting roles. Its based on 1991 malayalam film Kilukkampetti. Pyaar Impossible! was released on January 8, 2010.

==Plot==
In Ankert University, California (Assumption University, Bangkok), Alisha (Priyanka Chopra) is the most beautiful girl on campus with plenty of admirers. Awkward, nerdy Abhay (Uday Chopra) is in love with her, although she is unaware of his existence. One night Alisha is partying with her friends and accidentally falls into a river. Abhay jumps in and rescues her from drowning, but her friends take her away before she regains consciousness. Abhay is further prevented from seeing Alisha the next day when her outraged father comes to the college and removes her from college.
 PR representative. Still besotted, he follows her home and due to a misunderstanding she mistakes him for a nanny she was expecting from an employment agency. She is now divorced with an unruly daughter by the name of Tanya, and in search of another nanny, as Tanya drives every one away. Abhay decides to become Tanyas nanny and keeps his identity a secret in order to stay close to Alisha.

Abhay takes care of her house mostly by paying contractors to clean it for him and eventually wins over Tanya when he buys her the Rockband video game so she can become a rockstar. Tanya nicknames him "Froggy" because of his nerdy looks. Things become complicated when Siddhu shows up trying to romance Alisha and sells the stolen software to her company. Abhay finds out that Siddharths real name is Varun Sanghvi and tries to hide from Varun even while he grows closer to Alisha; she confides in Abhay and he dresses her up in glasses and old clothes to show her how differently people are treated when they appear to be unattractive. Alisha feels sorry for Abhay.

As the launch date for the software approaches, Abhay is unmasked by Varun who claims he is a delusional stalker and was never a nanny. Alisha is angry that he lied and orders Abhay out of the house without giving him a chance to explain. However, she finds out from her daughter that Abhay is the mysterious person who rescued her in college; she also realizes that when Abhay told her about the girl he loved in college for seven years, he was talking about her. Alisha finds Abhay and apologizes to him saying that she has fallen in love with him. Abhay then tells her that he was the one who created the software for which Varun is taking all the credit. They rush to the software launch to stop Varun who is easily discredited when he doesnt know the password to Abhays software. Abhay is able to prove that it is his creation by entering the password. Abhay tells Alisha that she already knows the password; it is her name: A-L-I-S-H-A. Alisha and Abhay live happily with Tanya.

==Cast==
*Priyanka Chopra as Alisha Merchant
*Uday Chopra as Abhay Sharma
*Dino Morea as Varun Sanghvi / Siddhart "Sidhu" 
*Advika Yadav (Child artist) as Tanya, Alishas Daughter
*Anupam Kher as Abhays father
*Rahul Vohra as C.P
*Arjun Sablok as Mr. Jaspreet Apple store (Special Appearance)
*Madhura Naik as Alishas Friend
*Saidah Jules

==Release==
The film was promoted on the show Music Ka Maha Muqqabla on 10 January 2010 by Priyanka Chopra and Uday Chopra on STAR Plus and on Bigg Boss on 19 December 2009 by Priyanka Chopra and Uday Chopra.

==Reception==

===Critical reception===
The film has received negative to mixed reviews.  Anupama Chopra, Consulting Editor, NDTVMovies.com finds the movie "depressingly dim-witted".  Rajeev Masand called it "painfully predictable" and gave it one out of five stars.   Conversely, Bollywood Hungama said, "A love story works if you fall in love with the on-screen characters and also if it knocks on the doors of your heart. Pyaar Impossible does that.   may not be the ultimate romantic film, but you cant deny the fact that theres something about this film that stays with you, that you carry home... sorry, carry in your heart."  Nikhat Kazmi of The Times of India gave it three stars out of five, arguing that the strength of the movie lay in its intense performances, something that overcame the weak storyline. 

Among  , January 15, 2010  Rachel Saltz of The New York Times said the film "shouldn’t work, but does. Its sweet, and as formula goes, deftly done and satisfying," and called Priyanka Chopra "one of the most likable of the new crop of Hindi film stars. She is an effortless comedian...." 

===Box office===
Pyaar Impossible! made about Rs. 5.50 Crore. 

==Soundtrack==
The soundtrack of Pyaar Impossible! was composed by Salim-Sulaiman. The music was released on December 14, 2009. The lyrics are penned by Anvita Dutt Guptan and the songs are remixed by Abhijit Vaghani.

{{Infobox album
| Name        = Pyaar Impossible!
| Type        = Soundtrack
| Artist      = Salim-Sulaiman
| Cover       = 
| Released    = 14 December 2009
| Recorded    =  Feature film soundtrack
| Length      =  YRF Music
}}

;Track listing
{{Track listing
| extra_column = Performer(s)
| title1 = Alisha | extra1 = Anushka Manchanda, Salim Merchant
| title2 = Pyaar Impossible! | extra2 = Dominique Cerejo, Vishal Dadlani
| title3 = You and Me | extra3 = Neha Bhasin, Benny Dayal
| title4 = 10 on 10 | extra4 = Mahua Kamat, Anmol Malik, Naresh Kamat
| title5 = Ek Thi Ladki | extra5 = Rishika Sawant
| title6 = Alisha | note6 = Remix | extra6 = Anushka Manchanda, Salim Merchant
| title7 = Pyaar Impossible! | note7 = Remix | extra7 = Dominique Cerejo, Vishal Dadlani
}}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 