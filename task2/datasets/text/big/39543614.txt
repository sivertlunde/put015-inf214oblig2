Move On (2012 film)
 
{{Infobox film
| name = Move On: The Road Movie Inspired by You
| image = 
| director = Asger Leth
| producer = André Bause Tom de Mol Suza Horvat Igor Nola Jacob Raben Michael Schmid
| screenplay = Matt Greenhalgh
| based on =  Karl Fischer Béla Ficzere Zsolt Fehérvári
| score composer and original music = Alexander Sjödin
| additional music = Lars Löhn
| cinematography = Philippe Kress
| editing = Benedikt Hugendubel	
| studio = 
| distributor = 
| released = 
| runtime = 140 minutes
| country = Denmark Germany Slovakia Hungary Croatia
| language = English German Slovakian Hungarian Croatian Macedonian
}}
Move On is an international film project from 2012 directed by Asger Leth in collaboration with Deutsche Telekom AG. The leading actor is Mads Mikkelsen, who is supported by German actress Alexandra Maria Lara and newly discovered Slovak actress Gabriela Marcinkova. At  , a new episode of the movie appeared each week, and since its world premiere on 6 November 2012 in Berlin, all eight episodes can be viewed there.

== Plot ==

In this participatory road movie, Mads Mikkelsen plays a secret agent who travels through a total of eight countries attempting to deliver a mysterious case to a destination unknown to him. Alexandra Maria Lara, playing his boss, guides him on his way through a series of cryptic messages.
From pre-production through to the shooting of the film, cinema buffs from 11 European countries had the opportunity to actively shape elements of the road movie. They were involved in aspects ranging from designing the posters and performing in minor supporting parts or as extras. Several thousand people applied online, submitting their film clips and sound recordings for assessment. The best contributions were incorporated into Move On, thus giving the film a unique character, and many of the winners were even on set during filming.
During the shooting of Move On, leading actor Mads Mikkelsen travelled for 49 days, together with director Asger Leth and the entire film crew, from the Netherlands to Macedonia. The trip then culminated with a grand finale in Berlin where Alexandra Maria Lara met up with the team of agents. The end result is an enthralling road movie.

==Cast==
* Mads Mikkelsen as Mark 
* Gabriela Marcinkova as Agent
* Alexandra Maria Lara as Boss Karl Fischer as Contact

==Filming locations==
* Vienna, Austria (Opera) 
* Berlin, Germany (Alexanderplatz) 
* Sóskút, Pest, Hungary|Pest, Budapest, Hungary
* Bratislava, Slovakia (River Park) 
* Opatija, Croatia
* Rotterdam, Zuid-Holland, Netherlands (Docks)

==External links==
* 
*Move On  

 