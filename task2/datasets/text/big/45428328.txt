Derelict (film)
{{Infobox film
| name           = Derelict
| image          = Derelict poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Rowland V. Lee
| producer       = 
| screenplay     = Grover Jones William Slavens McNutt George Bancroft Jessie Royce Landis William "Stage" Boyd Donald Stuart Wade Boteler Paul Porcasi
| music          = Karl Hajos
| cinematography = Archie Stout
| editing        = George Nichols Jr. 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 George Bancroft, Jessie Royce Landis, William "Stage" Boyd, Donald Stuart, Wade Boteler and Paul Porcasi. The film was released on November 22, 1930, by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=9D02EED61E38E433A25751C2A9679D946194D6CF|title=Movie Review -
  Derelict - THE SCREEN - NYTimes.com|work=nytimes.com|accessdate=17 February 2015}}  
 
==Plot==
 

== Cast == George Bancroft as Bill Rafferty
*Jessie Royce Landis as Helen Lorber
*William "Stage" Boyd as Jed Graves
*Donald Stuart as Fin Thomson
*Wade Boteler as Captain Gregg
*Paul Porcasi as Masoni
*Brooks Benedict as McFall

== References ==
 

== External links ==
*  
* 

 

 
 
 
 
 
 
 
 