Signora Enrica
{{Infobox film
| name = Signora Enrica
| image = SignoraEnricaFilmPoster.jpg
| alt =
| caption = Theatrical poster
| director = Ali İlhan
| producer = Elvan Arca   Can Arca
| writer = Ali İlhan
| starring = Claudia Cardinale   İsmail Hacıoğlu   Lavinia Longhi   Teoman Kumbaracıbaşı   Nilay Cennetkuşu   Fahriye Evcen   Bedia Ener   Sema Çeyrekbaşı   Murat Karasu
| music = Orhan Şallıel
| cinematography = Soykut Turan
| editing = Arzu Volkan
| studio = Ares Media
| distributor = Filmpot
| released =  
| runtime = 110 minutes
| country = Turkey
| language = Turkish Italian
| budget = US$1,000,000 (estimated)
| gross =
}}
Signora Enrica ( ) is a 2010 Italo-Turkish comedy-drama film, written and directed by Ali İlhan, starring Claudia Cardinale as an elderly Italian woman who takes in a young Turkish exchange student. The film, which is scheduled to go on nationwide general release across Turkey on  , was selected for the 47th International Antalya Golden Orange Film Festival.

==Production==
The film was shot on location in Istanbul, Turkey and Rimini, Italy.   

== Plot ==
Abandoned with a son by her husband years ago, Signora Enrica is notorious in her native Rimini for refusing to allow any men into her house ever since. She rents out rooms to female students, while also working as a tailor and at the market. She decides to make an exception to her age-old rule for Ekin, a Turkish student who comes to her house.

Though Ekin doesn’t speak Italian, he falls in love with Valentina, another tenant in the house, and seeks ways of communicating with her. Signora Enrica teaches Ekin Italian, dancing, and the subtleties of Italian cuisine – in other words, everything he needs to know to reach out to Valentina. Signora Enrica’s son Giovanni begins to begrudge his mother for giving a stranger the love she has denied everybody else for years.

== Release ==

=== Festival screenings ===
* 47th Antalya "Golden Orange" International Film Festival (October 9–14, 2010)

== See also == 2011
* 2010 in film

==References==
 

==External links==
*  
*   page for the film

 
 
 
 
 
 


 