Air Force (film)
{{Infobox film
| name           = Air Force
| image          = Air Force - 1943 - Poster.png Theatrical release title lobby card
| director       = Howard Hawks
| producer       = Hal B. Wallis Jack Warner (executive producer)
| writer         = Dudley Nichols Harry Carey
| music          = Leo F. Forbstein
| cinematography = James Wong Howe Elmer Dyer Charles A. Marshall
| editing        = George Amy
| distributor    = Warner Bros.
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $3,000,000 (1942) 
}}
 American black-and-white Harry Carey, and Gig Young. 

The story revolves around an actual incident that occurred on December 7, 1941. A bomber aircrew, flying an unarmed Boeing B-17 Flying Fortress named the Mary-Ann, is ferrying the heavy bomber across the Pacific to the United States Army Air Corps base at Hickam Field, when the bomber flies right into the middle of the Japanese air attack on Pearl Harbor and the beginning of World War II.

An uncredited William Faulkner wrote the emotional deathbed scene for actor John Ridgely, the pilot of the Mary-Ann. Made in the aftermath of the Pearl Harbor attack, Air Force was one of the first of the patriotic films of World War II, often characterized as a propaganda film. 
 

==Plot== Hamilton Field, near San Francisco, a United States Army Air Corps Boeing B-17 Flying Fortress|B-17D bomber Mary-Ann and its crew are being readied for a flight across the Pacific.
 Arthur Kennedy).
 Randolph Field, Kelly Field, Texas. Both the navigator and bombardier also washed out of pilot training.

With the United States at peace, Mary-Ann and the rest of its bomber squadron are ordered to fly without ammunition to Hickam Field at Pearl Harbor, Territory of Hawaii. Before the bombers depart, Quincannons wife arrives to give him a "good luck" gift, a toy pilot from their infant son, Michael Aloysius Quincannon, Jr. Young Private Chester also asks Captain Quincannon to meet his worried mother and tell her it is a standard flight to Hawaii.
 James Brown) and a small dog from the Marines on Wake Island named "Trippoli."  

When they land at Clark Field, White receives the news that his son was killed on the first day trying to lead his squadron into the air against an attack. Quincannon has to give Robbie his sons personal effects. Soon after, Quincannon volunteers his bomber for a one-aircraft mission against a Japanese invasion fleet, but the Mary-Ann is attacked by enemy fighters and forced to abort. The badly wounded Quincannon orders his men to bail-out of the stricken bomber, and then he blacks out. Winocki checks on him, sees he is passed out from his injuries, and decides to now guide in the shot-up bomber for a belly landing. Later on at Clark Field, having told a dying Quincannon that Mary-Ann is ready to fly, the crew works feverishly through the night repairing the bomber as the Japanese Army closes in. Private Chester volunteers to fly as gunner in a two-seat fighter aircraft defending Clark Field. In aerial combat the pilot is killed, and Chester is forced to bail-out; he is machine-gunned by a Japanese fighter pilot while suspended, helpless, in his parachute. On the ground, Winocki and White team up and shoot down that Japanese aircraft after it strafes Chesters lifeless body. As the side-armed enemy pilot stumbles from his burning aircraft, an angry Winocki machine-guns the enemy pilot for killing the defenseless Chester. The aircrew barely manages to finish their repairs as the airfield comes under attack. With the help of U. S. Marines and U. S. Army soldiers, they refuel the bomber shortly before the B-17s position is overrun by Japanese soldiers; her engines now powered up and the bombers .50 caliber machine guns returning fire, Mary-Ann barrels down Clark Fields runway and flies again. 
  
As the B-17 heads for the safety of Australia, with Rader as a now reluctant bomber pilot and the wounded Williams as co-pilot, they spot a large Japanese naval invasion task force below. The crew radios the enemy position to all nearby U. S. airbases and aircraft carriers, and the bomber circles until those reinforcements arrive in force; Mary-Ann then leads the aerial bombing attack that destroys the Japanese fleet. 

Much later, the first bombing mission against Tokyo is announced to a roomful of expectant bomber crews; among them now are several familiar faces from the Mary-Ann. As their aircraft take off, a stirring speech by President Roosevelt is heard in voice-over as waves of bombers join up and head toward the rising sun, and victory.

==Cast==
As appearing in screen credits (main roles identified):   IMDb. Retrieved: June 24, 2011. 
{| class="wikitable" width="45%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- John Ridgely || Captain Michael Aloysius "Irish" Quincannon, Sr., Pilot
|- Gig Young || Lt. William Williams, Co-Pilot
|- Arthur Kennedy Arthur Kennedy || Lt. Thomas C. McMartin, Bombardier
|- Charles Drake ||  Lt. Monk Hauser, Jr., Navigator
|- Harry Carey Harry Carey || Master Sergeant Robert "Robbie" White, Flight Engineer & Crew Chief
|- George Tobias || Corporal Weinberg, Assistant Crew Chief
|- Ward Wood || Corporal "Minnesota" Peterson, Radio Operator
|- Ray Montgomery Ray Montgomery || Private Chester, Assistant Radio Operator
|- John Garfield Sergeant Joe Winocki, Aerial Gunner
|- James Brown James Brown || Lt. Thomas "Tex" Rader, Pursuit Pilot - (Passenger)
|- Stanley Ridges || Major Mallory - Clark Field
|- Willard Robertson || Colonel at Hickam Field
|- Moroni Olsen || Colonel Blake - Commanding Officer at Manila
|- Edward Brophy (as Edward S. Brody)|| Sergeant J.J. Callahan, USMC 
|- Dick Lane Richard Lane || Major W.G. Roberts
|- Bill Crago || Pilot P.T. Moran at Manila
|- Faye Emerson || Susan McMartin - Tommys Sister
|- Addison Richards || Major Daniels
|- James Flavin || Major A.M. Bagley
|- Dorothy Peterson || Mrs. Chester
|- Leah Baird || Nurse #2
|- Ann Doran || Mrs. Mary Quincannon
|- Ruth Ford Ruth Ford || Nurse
|}

==Production==
  Army Air Hamilton Field, California, on the night of December 6, 1941, and literally flew into the war the next morning at Pearl Harbor. Executive producer Jack Warner was adamant that the film be ready for release by December 7, 1942, the first anniversary of the attack on Pearl Harbor. To that end, miniatures for battle sequences were filmed in May and June 1942, before completion of the script and storyline. 

Although pre-production work on Air Force had already taken place, the official start of the production on May 18, 1942 was tied to the War Department approving the script.  Development of the film was concurrent with script-writing by Dudley Nichols, with some characters based on Air Corps personnel Hawks met while traveling to Washington, D.C. to confer with Arnold and the War Department Motion Picture Board of Review. Orriss 1984, p. 67.  Nicholss script, submitted June 15, was 207 pages in length (twice that of the normal feature-length film), had its initial 55 pages devoted to "character development," and was not finished. 
 MacDill Field, Randolph Field, Santa Monica West Coast. 

At the end of August, Hawks returned to Hollywood and engaged William Faulkner to rewrite two scenes for Air Force, including the death of the Mary-Ann s pilot. By then, the film, scheduled to be completed by September 17, was three weeks behind schedule and only half completed. Production featured a celebrated clash between producer Hall Wallis and Hawks over the latters constant changing of dialogue as scenes were shot. Hawks was briefly replaced on October 4 by Vincent Sherman, but returned from "illness" on October 10 to take back primary direction. Sherman remained as second unit director to assist with completion of the picture, which wrapped on October 26, 1942, failing to shoot 43 pages of script and 33 days over schedule, too late to meet its December 7 release date.  
 19th Bomb Randolph Field, Texas, in the process of appearing as himself in the Academy Award-winning short film Beyond the Line of Duty when he assisted on Air Force. 

===Aircraft===
The U. S. Army Air Forces provided the various aircraft that appear in the film:
* Ten Boeing B-17B/C/D Flying Fortresses were from Hendricks Army Airfield at Sebring, Florida; the majority were B-17Bs upgraded to B-17C/D standards, as was the B-17 that portrayed Mary-Ann; the serial number 05564 is listed in the film credits.
* North American AT-6 Texans and Republic P-43 Lancers were painted as Japanese fighters.  Curtiss P-40C Warhawks were the AAF fighters; they were from Drew Army Airfield, Florida. Martin B-26C Marauders were painted as Japanese bombers; they were from MacDill Field. 

The real Mary-Ann was reported lost in the Pacific shortly after the film production wrapped, according to information attributed to the production s technical advisor; actually, no early Flying Fortresses served for long in Pacific combat after Pearl Harbor. Another claim, attributed to a newspaper article, was that "the real Mary-Ann" went on tour to promote the film, then was assigned to Hobbs Army Air Field, New Mexico, then later to Amarillo Army Air Field, where it was assigned to a ground school. Two early B-17B aircraft, upgraded to model "D" standards, played the role of Mary Ann; AAF serial numbers 38-584 and 39-10 were reclassified in late 1943 as instructional airframes, and following the war, both were scrapped in January 1946.     tampapix.com.. Retrieved: July 25, 2011. 

==Historical accuracy==
The basic premise of Air Force, that a flight of B-17s flying to reinforce the defense of the Philippines flies into the attack on Pearl Harbor, reflects actual events. From that point on, however, all of the incidents are fictitious. No B-17 reinforcements reached the Philippines; the survivors of those already based there retreated to Australia less than two weeks after the war began. The major bombing mission depicted at the films climax most closely resembles the Battle of the Coral Sea five months later. Miniature shooting for its battle scenes was filmed in May and June 1942, concurrent but probably coincidental with Coral Sea and the Battle of Midway.
 fighters as the attack began. As detailed in Walter Lords book, Day of Infamy, later investigations proved no Japanese-American was involved in any sabotage during the Pearl Harbor attack.

There are several scenes in Air Force showing a tail gun position on the Mary-Ann. The bomber is a Boeing B-17D, and all early B-17s, series A to D, were not fitted with a machine gun position in their tails. Tail machine guns were not added to the B-17 until Boeing rolled out their redesigned B-17E model. However, in the film, the crew of the Mary-Ann are shown making a field modification to their bombers rear fuselage to allow for the installation of a single, improvised, machine gun position, "a stinger in our tail" as one crewman calls it.

==Reception==
Critical acclaim followed the films premiere as Air Force echoed some of the emotional issues that underlay the American public psyche at the time, including fears of Japanese Americans.  In naming it one of the "Ten Best Films of 1943," Bosley Crowther of The New York Times characterized the film as "... continuously fascinating, frequently thrilling and occasionally exalting..."   When seen in a modern perspective, the emotional aspects of the film seem out-of-proportion and although it has been wrongly dismissed as a piece of wartime propaganda, it still represents a classic war film that can be considered a historical document.  When initially released, Air Force was one of the top three films in commercial revenue in 1943.

Later reviews of Air Force noted that this was a prime example of Howard Hawks abilities; "Air Force is a model of fresh, energetic, studio-era filmmaking." 

Air Force placed third (behind The Ox-Bow Incident and Watch on the Rhine) as the best film of 1943 by the National Board of Review of Motion Pictures.

==Awards== For Whom The Song of Bernadette. The film was also nominated for Best Cinematography, Black-and-White and Best Effects, Special Effects (Hans F. Koenekamp, Rex Wimpy, Nathan Levinson) and Best Writing, Original Screenplay. Elmer Dyer, James Wong Howe and Charles A. Marshall were nominated for an Academy Award in the Cinematography - Black and White division.   oscars.org. Retrieved: June 22, 2013. 

==References==
Notes
 
Citations
 
Bibliography
 
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5.
* Hardwick, Jack and Ed Schnepf. "A Buffs Guide to Aviation Movies". Air Progress Aviation, Vol. 7, No. 1, Spring 1983.
* McCarthy, Todd. Howard Hawks: The Grey Fox of Hollywood. New York: Grove Press, 2000. ISBN 0-8021-3740-7.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
 

==External links==
*  
* 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 