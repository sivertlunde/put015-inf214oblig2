Thottal Poo Malarum
{{Infobox film
| name           = Thottal Poo Malarum
| image          = 
| writer         = P. Vasu Sukanya Santhanam Santhanam
| director       = P. Vasu
| producer       = P. Vasu
| editing        = KMK. Palanivel
| cinematography = Akash Ashok Kumar
| music          = Yuvan Shankar Raja
| released       =  
| runtime        = 146 minutes
| language       = Tamil
| country        = India
| studio         = Sapphire Media & Infrastructure
| budget         = 
}} Tamil romance Santhanam played supporting roles. Music is composed by Yuvan Shankar Raja. The film was released on 3 August 2007 and became an average grosser.

==Plot==
Ravi (Sakthi), is a happy-go—lucky-youngster, who falls in instant love with Anjali (Gauri Munjal), his college mate. Anjalis mother Periya Nayagie (Sukanya), a rich and an arrogant entrepreneur, tries to play spoilsport in their romance. She arranges for her daughters wedding with son of a dreaded gangster Varadharaja Vandaiyar (Raj Kiran) in Mumbai. Sakthi sets on a mission to Mumbai. He hides his true identity and manages to gain an entry into Vandiyars family. Having won the confidence of Vandaiyars family, Sakthi sets himself on a mission to marry Anjali. Does he succeed in his attempt forms the rest.

==Cast==
* Sakthi Vasu as Ravi Thyagarajan
* Gowri Munjal as Anjali
* Rajkiran as Varadharaja Vaandiyar
* Vadivelu as Kabaaleeswaran (Kabali Khan)
* Nassar as Thyagarajan Sukanya as Periya Naayagi
* Ennatha Kannaiya as Blind Cardriver Santhanam
* Sivaranjini
* K. S. Ravikumar (Special appearance)
* Madan Bob
* Pooja Gandhi in a special appearance
* Anand Kumar Murugan as Villain who will loose his hand

==Soundtrack==
{{Infobox album
| Name = Thottal Poo Malarum
| Type = soundtrack 
| Artist = Yuvan Shankar Raja
| Cover = Thottal Poo Malarum.jpg
| Released =  
| Recorded = 2007
| Genre = Film soundtrack
| Length = 
| Label = Sa Re Ga Ma
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Satham Podathey (2007)
| This album  = Thottal Poo Malarum (2007) 
| Next album  = Kannamoochi Yenada (2007)
}}

For the music of the film, P. Vasu teamed up with composer Yuvan Shankar Raja for the first time. The soundtrack was released on 23 June 2007 by Rajinikanth and Kamal Haasan.  It features 6 tracks, whilst Vaali (poet)|Kavignar Vaali had written the lyrics for all the songs, but for "Kadatharen Naan Unnai", which had lyrics written by Snehan.

"Indiaglitz" described the album as "rocking" and a "delight for music-lovers".  Particularly, the song "Arabu Naade" got immensely popular and became a chartbuster song.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song  || Singers || Length (m:ss) || Lyrics ||Notes
|- 1 || "Arabu Naade" || Haricharan,   ||
|- 2 || "Vittal Suriyanai" || Ranjith (singer)|Ranjith, Yuvan Shankar Raja || 4:38 || Vaali ||
|- 3 || "Valaiyal Karangalai" || Vijay Yesudas || 4:45 || Vaali ||
|- 4 || "Ennai Pidicha" || Haricharan, Binny Krishna Kumar || 4:35 || Vaali ||
|- 5 || "Vaadi Vambu Pennae" || Sujatha Mohan || 4:09 || Vaali ||
|- 6 || "Kadatharan Naan Unnai" || Rahul Nambiyar, Saindhavi || 3:48 || Snehan ||
|}

==Reviews==
TSV Hari of Rediff.com described the film as "very ordinary fare," adding that "Sakthi certainly deserved better."  M Bharat Kumar of News Today called it a "mediocre offering" with "predictable sequences," noting that "the son seems to have delivered the goods well, while the father has failed as a director."  However, IndiaGlitz described it as a "feel-good youthful entertainer" with an "intelligent screenplay and pacy narration" that is "sure to appease film-buffs." 

== References ==
 

== External links ==
*  
*   at Raaga

 

 
 
 
 
 
 