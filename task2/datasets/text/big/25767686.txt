My Son Shall Be Armenian
{{ infobox film
| name           = My Son Shall Be Armenian
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Hagop Goudsouzian
| producer       = Yves Bisaillon
| writer         = Hagop Goudsouzian Georgette Duchaine
| narrator       = Hagop Goudsouzian
| starring       = 
| music          = Ararat Petrossian
| cinematography = Alberto Feio
| editing        = André Corriveau (filmmaker)|André Corriveau
| studio         = National Film Board of Canada
| distributor    = 
| released       = 2004
| runtime        = 80 min 43 s
| country        = Canada
| language       = French
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
My Son Shall Be Armenian (Original French title: Mon fils sera arménien) is a 2004 Canadian documentary by Hagop Goudsouzian, who travels to Armenia and Syria with five other members of Montreals Armenian community who lost relatives in the Armenian Genocide, to speak with survivors.   
 
In Syria, Goudsouzian films in Deir ez-Zor, where thousands of Armenians were said to have been killed. In one scene, he scrapes the soil around a church and discovers the remains of what appears to be a mass grave, scooping up bones, a wedding ring and a bullet. In Armenia, Goudsouzian visits villages that had been renamed for former settlements, finding elders who recount what had occurred to their parents and siblings.   

My Son Shall Be Armenian incorporates archival photographs and footage from a Hollywood silent movie based on the accounts of one survivor who escaped to the United States during the genocide.  Participants in the film include Canadian TV host Patrick Masbourian.      

My Son Shall Be Armenian was produced in French by the National Film Board of Canada.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 