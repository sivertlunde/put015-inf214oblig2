Polish Wedding
{{Infobox film
| name           = Polish Wedding
| image          = Polish_wedding_poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Theresa Connelly Nick Wechsler
| writer         = Theresa Connelly
| narrator       = 
| starring       = Claire Danes *Jon Bradford.
| music          = Luis Bacalov
| cinematography = Guy Dufaux
| editing        = Curtiss Clayton Suzanne Fenn
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $600,294
| preceded_by    = 
| followed_by    = 
}}
 Berlin International Film Festival on February 12. It was released in the U.S. on July 17. It takes place within the Polish American community of Hamtramck, Michigan - the girlhood home of director Theresa Connelly - at some time between the 1950s and 1970s. Virtually all characters are Polish Americans, though the actors playing them are mostly of other ethnic origins.

==Plot==
Jadzia is the matriarch of a family of five children, four sons and a daughter. The household also includes the eldest sons wife &ndash; a Syrian-American whom Jadzia calls a Gypsy and who also works with Jadzia &ndash; and their child. Jadzia is (somewhat) happily married to Bolek, but is having a long-term relationship with Roman. Her daughter Hala becomes pregnant by a neighborhood cop and her family pressures him to marry her. Interior of the home were shot in a home on Wyandotte Street in Hamtramck. The St. Florian Church (Hamtramck, Michigan)|St. Florian Church was used as a backdrop.

==Cast==
* Claire Danes as Hala
* Jon Bradford as Sailor
* Lena Olin as Jadzia
* Ramsey Krull as Kris
* Gabriel Byrne as Bolek
* Daniel Lapaine as Ziggy
* Rachel and Rebecca Morrin as Ziggy and Sofies Baby
* Mili Avital as Sofie
* Steven Petrarca as Witek
* Brian Hoyt as Kaz
* Christina Romana Lypeckyj as Kasia
* Adam Trese as Russell Schuster 
* Peter Carey as Piotrus
* Rade Šerbedžija as Roman
* Kristen Bell as teenage girl (uncredited) Adam Wojciechowski as Himself

==Accuracies== Polish Roman Catholic Church, but the procession in the movie bears many similarities to the May Crowning tradition in the Roman Catholic Church.

==DVD== region 1 DVD was released March 16, 1999.

==References==
 
 

==External links==
*  
*  
*  
*  
*  
*   Rotten Tomatoes.

 
 
 
 
 
 