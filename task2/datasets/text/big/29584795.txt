Picture Mommy Dead
 
{{Infobox film
| name           = Picture Mommy Dead
| image        	 = Picture Mommy Dead FilmPoster.jpeg
| alt            = 
| caption        = Original theatrical poster
| director       = Bert I. Gordon
| producer       = Bert I. Gordon
| writer         = Robert Sherman
| starring       = {{Plainlist|
* Don Ameche
* Martha Hyer
* Susan Gordon
* Zsa Zsa Gabor}}
| music          = Robert Drasnin
| cinematography = Ellsworth Fredricks
| editing        = John A. Bushelman Bert I. Gordon Productions
| distributor    = Embassy Pictures
| released       =  
| runtime        = 82 minutes  
| country        = United States
| language       = English
| budget         = $1 million 
}}
Picture Mommy Dead is a 1966 American horror film directed by Bert I. Gordon and starring Don Ameche and Martha Hyer.  The film follows the genre of "mad family" that Psycho (1960 film)|Psycho (1960) started.

==Plot==
Susan Shelley thinks her father, Edward, killed her mother, Jessica, years ago. Newly released from an asylum after 3 years, she is reunited with her father and a new stepmother, Francene, but suspicious goings-on threaten to push her over the edge.

==Cast==
* Don Ameche as Edward Shelley
* Martha Hyer as Francene Shelley
* Susan Gordon as Susan Shelley
* Zsa Zsa Gabor as Jessica Flagmore Shelley
* Maxwell Reed as Anthony, the caretaker
* Wendell Corey as Lawyer Clayborn
* Signe Hasso as Sister René
* Anna Lee as Elsie Kornwald
* Kelly Corcoran as Little boy at estate sale

==Production==
Gene Tierney was originally announced for a lead role  and Hedy Lamarr was signed to support Ameche and Hyer.  However, Lamarr was fired from the film when she collapsed during filming from nervous exhaustion. Hedy Lamarr Fired From Comeback Film: HEDY LAMARR Berman, Art. Los Angeles Times (1923-Current File)   04 Feb 1966: 3.   She was replaced by Gabor.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 