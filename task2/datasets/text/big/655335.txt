Ivan's Childhood
{{Infobox film
  | name = Ivans Childhood Иваново детство 
  | image = 1962_ivanovo_detstvo.jpg
  | caption = 
  | director = Andrei Tarkovsky
  | producer =  Vladimir Bogomolov Mikhail Papava
  | starring = Nikolai Burlyayev Valentin Zubkov Yevgeni Zharikov Stepan Krylov Nikolai Grinko Irma Raush
  | music = Vyacheslav Ovchinnikov
  | cinematography = Vadim Yusov
  | editing = Lyudmila Feiginova
  | distributor = Mosfilm
  | released =   
  | runtime = 95 minutes
  | country = Soviet Union
  | language = Russian
  | budget = 
}} Soviet film Vladimir Bogomolov, with the screenplay written by Mikhail Papava and an uncredited Andrei Tarkovsky. The film features child actor Nikolai Burlyayev, Valentin Zubkov, Yevgeni Zharikov, Stepan Krylov, Nikolai Grinko and Tarkovskys wife Irma Raush.

The film tells the story of orphan boy Ivan and his experiences during World War II. Ivans Childhood was one of several Soviet films of its period, such as The Cranes Are Flying and Ballad of a Soldier, that looked at the human cost of war and did not glorify the war experience as did films produced before the Khrushchev Thaw. {{cite video
  | people = Vida T. Johnson
  | title = "Life as a Dream"
  | medium = DVD
  | publisher = The Criterion Collection
  | location =
  | date = 2007 }}
 
 Best Foreign Language Film at the 36th Academy Awards, but was not accepted as a nominee.  Famous filmmakers such as Ingmar Bergman, Sergei Parajanov and Krzysztof Kieślowski praised the film and cited it as an influence on their work. {{cite web
  | last = Daly
  | first = Fergus
  | authorlink =
  |author2=Katherine Waugh
  | title = Ivans Childhood
  | work =
  | publisher = Senses of Cinema
  | url = http://archive.sensesofcinema.com/contents/cteq/01/15/ivans_childhood.html
  | doi = archiveurl = archivedate = 2007-12-08}} 

== Original story == Vladimir Bogomolov. The story relates the experiences of a 12-year-old orphan named Ivan Bondarev during World War II. 

==Plot== Eastern front Soviet army is fighting the invading German Wehrmacht. The film features a Nonlinear narrative|non-linear plot with frequent flashbacks.


After a brief dream sequence, Ivan Bondarev (Nikolai Burlyayev), a 12-year-old Russian boy, wakes up and crosses a war-torn landscape to a swamp, then swims across a river. On the other side, he is seized by Russian soldiers and brought to the young Lieutenant Galtsev (Evgeny Zharikov), who interrogates him. The boy insists that he call "Number 51 at Headquarters" and report his presence. Galtsev is reluctant, but when he eventually makes the call, he is told by Lieutenant-Colonel Gryaznov (Nikolai Grinko) to give the boy pencil and paper to make his report, which will be given the highest priority, and to treat him well. Through a series of dream sequences and conversations between different characters, it is revealed that Ivan’s mother and sister (and probably his father, a border guard) have been killed by German soldiers. He got away and joined a group of partisans. When the group was surrounded, they put him on a plane. After the escape, he was sent to a boarding school, but he ran away and joined an army unit under the command of Gryaznov.

Burning for revenge, Ivan insists on fighting on the front line. Taking advantage of his small size, he is successful on reconnaissance missions. Gryaznov and the other soldiers grow fond of him and want to send him to a military school. They give up their idea when Ivan tries to run away and rejoin the partisans. He is determined to avenge the death of his family and others, such as those killed at the Maly Trostenets extermination camp (which he mentions that he has seen).


A subplot involves Captain Kholin (Valentin Zubkov) and his aggressive advances towards a pretty army nurse, Masha (Valentina Malyavina), and Galtsevs own undeclared and unrequited feelings for her. Much of the film is set in a room where the officers await orders and talk, while Ivan awaits his next mission. On the walls are scratched the last messages of doomed prisoners of the Germans.

Finally, Kholin and Galtsev ferry Ivan across the river late at night. He disappears through the swampy forest. The others return to the other shore after cutting down the bodies of two Soviet scouts hanged by the Germans.

The final scenes of the film then switch to Berlin under Soviet occupation after the fall of the Third Reich. Captain Kholin has been killed in action. Galtsev finds a document showing that Ivan was caught and hanged by the Germans. As Galtsev enters the execution room, a final flashback of Ivans childhood shows the young boy running across a beach after a little girl in happier times. The final image is of a dead tree on the beach.

==Cast==
* Nikolay Burlyaev as Ivan (as Kolya Burlyayev)
* Valentin Zubkov as Capt. Kholin (as V. Zubkov)
* Yevgeni Zharikov as Lt. Galtsev (as Ye. Zharikov)
* Stepan Krylov as Cpl. Katasonov (as S. Krylov)
* Nikolai Grinko as Lt. Col. Gryaznov (as N. Grinko)
* Dmitri Milyutenko as Old Man (as D. Milyutenko)
* Valentina Malyavina as Masha (as V. Malyavina)
* Irma Raush as Ivans Mother (as I. Tarkovskaya)
* Andrey Konchalovskiy as Soldier (as A. Konchalovskiy)

==Production==

Ivans Childhood was Tarkovskys first feature film, shot two years after his diploma film The Steamroller and the Violin. The film is based on the 1957 short story Ivan ( ) by Vladimir Bogomolov, which was translated into more than twenty languages. It drew the attention of the screenwriter Mikhail Papava, who changed the story line and made Ivan more of a hero. Papava called his screenplay Second Life ( ). In this screenplay Ivan is not executed, but sent to the concentration camp Majdanek, from where he is freed by the advancing Soviet army. The final scene of this screenplay shows Ivan meeting one of the officers of the army unit in a train compartment. Bogomolov, unsatisfied with this ending, intervened and the screenplay was changed to reflect the source material.

Mosfilm gave the screenplay to the young film director Eduard Abalov. Shooting was aborted and the film project was terminated in December 1960, since the first version of the film drew heavy criticism from the arts council, and the quality was deemed unsatisfactory and unusable. In June 1961 the film project was given to Tarkovsky, who had applied for it after being told about Ivans Childhood by cinematographer Vadim Yusov. {{cite video
  | people = Vida T. Johnson
  | title = "Life as a Dream"
  | medium = DVD
  | publisher = The Criterion Collection
  | location =
  | date = 2007 }}
  Work on the film resumed in the same month. The film was shot for the most part near Kaniv at the Dnieper River.
 State Institute of Cinematography (VGIK), and thus Burlyayev was also cast for the role of Ivan. He had to pass several screen tests, but according to Burlyayev it is unclear whether anyone else auditioned for the role. {{cite video
  | people = Nikolai Burlyayev
  | title = "Nikolai Burlyayev Interview"
  | medium = DVD
  | publisher = The Criterion Collection
  | location =
  | date = 2007 }}
  Andrei Rublev.

==Responses==
 
Ivans Childhood was one of Tarkovskys most commercially successful films, selling 16.7 million tickets in the Soviet Union.  Tarkovsky himself was displeased with some aspects of the film; in his book Sculpting in Time, he writes at length about subtle changes to certain scenes that he regrets not implementing.
However, the film received numerous awards and international acclaim on its release, winning the Golden Lion at the Venice Film Festival. It attracted the attention of many intellectuals, including Ingmar Bergman who said, "My discovery of Tarkovskys first film was like a miracle. Suddenly, I found myself standing at the door of a room the keys of which had, until then, never been given to me. It was a room I had always wanted to enter and where he was moving freely and fully at ease." {{cite web
  | last = 
  | first = 
  | authorlink =
  | title = Ingmar Bergman - On Tarkovsky
  | work =
  | publisher = www.nostalghia.com
  | url = http://www.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/IB_On_AT.html
  | doi =
  | accessdate = 2007-12-13 }}
 

Jean-Paul Sartre wrote an article on the film, defending it against a highly critical article in the Italian newspaper LUnita and saying that it is one of the most beautiful films he had ever seen. {{cite web
  | last = Sartre
  | first = Jean-Paul
  | authorlink =
  | title = Discussion on the criticism of Ivans Childhood
  | work =
  | publisher = www.nostalghia.com
  | url = http://www.ucalgary.ca/~tstronds/nostalghia.com/TheTopics/Sartre.html
  | doi =
  | accessdate = 2007-12-13 }}
  Filmmakers Sergei Parajanov and Krzysztof Kieślowski praised the film and cited it as an influence on their work.

==See also==
* List of submissions to the 36th Academy Awards for Best Foreign Language Film
* List of Soviet submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*   at Mosfilms YouTube Channel
*  
*  
* The short story   by Vladimir Bogomolov  
*   on Ivans Childhood

 
 

 
 
 
 
 
 
 
 
 
 
 
 