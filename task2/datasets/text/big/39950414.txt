Hunting Elephants
{{Infobox film
| name           = Hunting Elephants
| image          = 
| caption        = 
| director       = Reshef Levi
| producer       = Ehud Bleiberg Moshe Edry Leon Edry
| writer         = Reshev Levi Regev Levi
| starring       = Sasson Gabai Moni Moshonov Patrick Stewart Gil Blank Yael Abecassis
| music          = Gilad Ben Amram Gil Toren
| cinematography = Yaron Sherf
| editing        = Yitzhak Tzhayak
| distributor    = United King Films
| released       =  
| runtime        = 107 minutes
| country        = Israel
| language       = Hebrew, English NIS 10 million
| gross          = 
}}
Hunting Elephants ( , Latzud Pilim, lit. To Hunt Elephants) is a 2013 Israeli crime comedy film directed by Reshef Levi. It stars, among others, Sasson Gabai, Moni Moshonov and Patrick Stewart. It was released to theaters in Israel on July 4, 2013.

The plot centers around Jonathan (Gil Blank), a 12 year old boy who, with his grandfather and two others, attempts to rob the bank where his deceased father had worked. The film received mixed reviews from critics.

==Plot==
Jonathans father, Daniel (Tzvika Hadar), dies of a heart attack in his workplace, with Jonathan present but unable to help him. The bank manager, Dedi (Moshe Ivgy), refuses to pay compensation because the contract with the insurance company doesnt cover such events. Jonathans mother, Dorit (Yael Abecassis), starts going out with Dedi because she needs the money, leaving Jonathan in an elderly home with his grandfather Eliyahu (Sasson Gabai).
 Lehi fighters who used to rob British banks to fund the organizations activities. Eliyahu had been at odds with Daniel and Dorit for many years, and hated Dorit for not performing an abortion and, in his opinion, ruining Daniels life and burying his career. Eliyahus wife Roda (Rina Schenfeld) is in a coma in their elderly home, with no chance of recovery. She was the daughter of a British lord and inherited his estate in Israel, while her brother Michael Simpson (Patrick Stewart), broke because he chose to be a theater actor to the chagrin of their father, seeks to take Roda off life support and get the estate. The estate had in fact already been sold by Eliyahu to pay for Rodas lengthy hospitalization and a home for himself and Nick.

To solve their financial problems, Jonathan, Michael, Eliyahu and Nick plan to rob the bank where Daniel worked, being intimately familiar with the banks defense systems. They stage a number of diversions which causes the bank to turn off one of the main alarm systems, allowing them to enter the safe. The first plan goes awry when Nick, who was supposed to be a hostage himself, takes a guards gun and his own hostages, but he is not sent to prison because of his dementia. Eliyahu then robs the bank himself, and Jonathan, pretending to be a victim, burns some of the money in front of the police, allowing him to get away with most of the stolen cash.

==Cast==
{| class="wikitable"
!Character!!Actor
|- Jonathan
|Gil Blank
|- Eliyahu
|Sasson Gabai
|- Nick
|Moni Moshonov
|- Michael Simpson Patrick Stewart
|- Dorit
|Yael Abecassis
|- Dedi
|Moshe Ivgy
|- Daniel
|Zvika Hadar
|- Roda
|Rina Schenfeld
|- Sigi
|Rotem Zisman-Cohen
|}

==Production==
British actor John Cleese was signed up for the role of Michael Simpson, but due to heart problems he was unable to perform in the film. Stewart agreed to replace him and the script was modified to closer fit his personality.

==Reception==
Hunting Elephants received mixed reviews from critics. Maariv praised the film for creating a fun setting and going against the stereotype of what old people should be doing, but criticized it for being vulgar and sexist in its treatment of the sub-plot with the caretaker Sigi. 

  was especially critical on the same issues, further slamming the "old people" jokes and the vulgarity of the film.  Walla! took issue with the premise of the movie, saying that it was not believable, among other things because the movie was clearly based in the present, when the characters should have been about 90 years old and unable to perform the actions in the movie. It praised the cast however, saying that it saved the film. 

==See also==
* 2013 in Israeli film

==References==
{{reflist|refs=
   
   
   
   
}}

==External links==
*  

 
 
 
 