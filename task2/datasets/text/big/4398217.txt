Frederic Remington: The Truth of Other Days
{{Infobox film
| name             = Frederic Remington: The Truth of Other Days
| image            = Remingtonart.jpg
| image_size       =
| caption          = Marketing card 
| director         = Tom Neff
| producer         = Executive Producer: Tom Neff Producers: Madeline Bell Karl Katz Arnie Knox
| writer           = Louise LeQuire Tom Neff
| narrator         = Gregory Peck
| starring         = Gregory Peck Ned Beatty
| music            = John Rosasco
| cinematography   = 
| editing          = Barry Rubinow
| distributor      = American Masters (Public Broadcasting Service|PBS)
| released         =  
| runtime          = 60 minutes
| country          = United States
| language         = English
|}} documentary of PBS series American Masters and produced and directed by Tom Neff  It was written by Neff and Louise LeQuire.   Actor Gregory Peck was the narrator of the film and Ned Beatty was the voice of Remington when reading his correspondence.

The documentary was produced by the Metropolitan Museum of Art, New York; NHK Corporation (Japan); and Polaris Entertainment, Nashville, Tennessee.  It was the first documentary to be filmed in High Definition Television (HDTV), but at the time it was years away from high-definition television broadcasting. 

==Synopsis==
This documentary of Frederic Remington reviews how the artist popularized the myths, legends, and images we now call the "Old West."

The film was filmed on location where Remington spent time, uses archival film and photographs, and has interviews with art scholars that create a framework to understand his artwork.

==Interviews==
* Gregory Peck as narrarator
* Ned Beatty as voice of Frederic Remington
* William Howze
* Lewis Sharp
* Brian W. Dippie
* Peter Hassrick

==Reception==

===Critical response===
When the film was shown on   documentary, tonight at 9 on Channel 13, shows and tells how the Easterner helped create a Western myth that has not yet lost its power...Attention is drawn especially to the way the massed figures move on both canvas and screen, from upper right to lower left. Big men in a landscape of big nature was a steady theme of both the movie maker and the painter." 

===Awards===
Wins
* CINE: CINE Golden Eagle, 1990.
* WorldFest-Houston International Film Festival, Special Jury Prize, 1990.

==References==
 

==External links==
*   official web site (see Films for film clip)
*  
*   at American Masters
*  

 

 
 
 
 
 
 
 
 
 