Manneken Pis (film)
{{Infobox film
| name           = Manneken Pis
| image          = 
| caption        = 
| director       = Frank Van Passel
| producer       = Dirk Impens Rudy Verzyck
| writer         = Christophe Dirickx
| based on       = 
| starring       = 
| music          = Noordkaap
| cinematography = Jan Vancaillie
| editing        = Karin Vaerenberg
| studio         = 
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Belgium
| language       = Dutch
| budget         = 
| gross          = 
}}

Manneken Pis is a 1995 Belgian comedy-drama film directed by Frank Van Passel and written by Christophe Dirickx. It premiered in May 1995 at the Cannes Film Festival.  It received the André Cavens Award for Best Film and four awards at the Joseph Plateau Awards. 

==Cast==
*Frank Vercruyssen as Harry
*Antje de Boeck as Jeanne
*Ann Petersen as Denise
*Wim Opbrouck as Desire

==See also==
*List of Belgian submissions for the Academy Award for Best Foreign Language Film
*List of submissions to the 68th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 


 