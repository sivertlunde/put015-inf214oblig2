Unholy (2007 film)
{{Infobox film
| name           = Unholy
| image          = Unholy (2007 film).jpg
| alt            = 
| caption        = 
| director       = Daryl Goldberg
| producer       = 
| writer         = Sam Freeman Daryl Goldberg
| starring       = Adrienne Barbeau Nicholas Brendon
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Nazi mysticism starring Adrienne Barbeau and Nicholas Brendon. It was written by Samuel Stephen Freeman, directed by Daryl Goldberg, and produced by Sky Whisper Productions. Its release on DVD was on September 4, 2007. The movie is alleged to be based on fact, and was produced to document the facts surrounding the experiments in mysticism.

==Synopsis==
The film deals with a grieving mother, Martha (Barbeau), trying to uncover the terrifying secret jeopardizing her family. With her son (Brendon), Martha becomes entwined in a conspiracy involving a fabled witch, Nazi occultists, and the United States of America (U.S.) government.

The film purports to be inspired by an actual military document. The document is viewable on the movies website by simply clicking on the interactive image of the document. Following World War II, a classified U.S. military document was uncovered that recounted a Nazi experiment of an occult nature smuggled into an underground facility in Downingtown, Pennsylvania. The films website purports to "have provided the only known copy of a portion of that document ... we strongly advise that you do not download it;" however, the document can be found on the same website.

==External links==
*  
*  ,   Movie reviews
*  

 
 
 
 
 
 
 


 