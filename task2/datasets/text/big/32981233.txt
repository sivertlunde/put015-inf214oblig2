The Marriage Price
{{infobox film
| name           = The Marriage Price
| image          = The Marriage Price (1919) - 1.jpg
| imagesize      = 210px Still with Standing and Ferguson
| director       = Emile Chautard
| producer       = Adolph Zukor Jesse Lasky
| writer         = Griswold Wheeler (story: For Sale)  Eve Unsell (scenario)
| starring       = Elsie Ferguson
| music          =
| cinematography = Jacques Bizeul
| editing        =
| distributor    = Paramount Pictures / Artcraft
| released       = March 16, 1919
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}} 1919 American silent romantic drama film produced by Famous Players-Lasky and distributed through Paramount Pictures and Artcraft. Emile Chautard directed and Elsie Ferguson stars. This film is lost film|lost.   

==Plot==
As described in a film magazine,  Helen Tremaine (Ferguson) is an extravagant daughter of a wealthy New York man, but becomes impoverished by his death and disillusioned when her friends leave her, save Frederick Lawton (Standing), a man of power and wealth. She does not love Lawton and prefers Kenneth Gordon (Atwill), but he says he is too poor to marry her. She attempts to support herself, but even fails at a job as a film actress. Nearly starving, she marries Lawton as a last resort. He tests her by making her financially independent through a pretend heritage of hers that he claims to have discovered, but she remains a girl of her word. All is well until her former love shows up to win her with a scheme that will enrich him while ruining her husband. The deal goes through, but it is a trap set up by her husband to show her the type of man her former love was. She now sees the nobility in her husband with happy results.

==Cast==
*Elsie Ferguson - Helen Tremaine
*Wyndham Standing - Frederick Lawton
*Lionel Atwill - Kenneth Gordon
*  - Archie Van Orden
*Maud Hosford - Amelia Lawton
*Marie Temper - Evie Hitchens
*Clariette Anthony - The Other Woman
*Zelda Crosby - Undetermined Role

==References==
 

==External links==
 
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 
 