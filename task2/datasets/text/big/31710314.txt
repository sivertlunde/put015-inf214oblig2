The Mind of Mr. Soames
{{Infobox film
| name           = The Mind of Mr Soames
| image          = "The_Mind_of_Mr._Soames"_(1970).jpg
| image_size     =
| caption        = Terence Stamp and Nigel Davenport
| director       = Alan Cooke
| producer       = Max Rosenberg Milton Subotsky John Hale Edward Simpson
| based on       = novel by Charles Eric Maine
| starring       = Terence Stamp Nigel Davenport Robert Vaughan
| music          =  Michael Dress Billy Williams
| editing        = Bill Blunden studio = Amicus Productions
| distributor    = Columbia Pictures
| released       = 1970
| runtime        =
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} British film directed by Alan Cooke and starring Terence Stamp, Robert Vaughn and Nigel Davenport.  

Based on Charles Eric Maines 1961 novel of the same name, it tells the story of a thirty-year-old man (John Soames) who has been in a coma since a brain injury during birth. Now revived, he shows the behavior of a child and is monitored by two doctors attempting to find out if he can be rehabilitated in the adult world.  

==Partial cast==
* Terence Stamp - John Soames
* Robert Vaughn - Doctor Michael Bergen
* Nigel Davenport - Doctor Maitland Christian Roberts - Thomas Fleming
* Donal Donnelly - Joe Allan Norman Jones - Davis
* Dan Jackson - Nicholls
* Vickery Turner - Naomi
* Judy Parfitt - Jenny Bannerman
* Scott Forbes - Richard Bannerman
* Joe McPartland - Inspector Moore
* Pamela Moiseiwitsch - Melanie Parks Billy Cornelius - Sergeant Clifford
==Production==
The film was an attempt by Amicus Productions to branch into the non-horror field. (They had also tried to option the rights to Flowers For Algernon but been unable to secure them.) The large budget was provided by Columbia Pictures. Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 48 
==Reception==
The film was a failure at the box office. 
==References==
 

==External links==
* 
*  at British Horror Films
*  at New York Times
*  at TCMDB
 

 
 
 
 
 
 
 
 


 