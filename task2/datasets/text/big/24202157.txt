Seven Were Saved
{{Infobox film
| name           = Seven Were Saved
| image          =Seven Were Saved (1947) 1.jpg
| image_size     =180px Lobby card with Russell Hayden, Catherine Craig, and Richard Denning
| director       = William H. Pine
| producer       = L.B. Merman (associate producer) William H. Pine (producer) William C. Thomas (producer)
| writer         = Julian Harmon (story) Maxwell Shane (screenplay and story)
| narrator       =
| starring       = Richard Denning Catherine Craig Russell Hayden
| music          = Darrell Calker
| cinematography = Jack Greenhalgh
| editing        = Howard A. Smith
| distributor    = Pine-Thomas Productions
| released       = 28 March 1947
| runtime        = 73 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Seven Were Saved is a 1947 American film directed by William H. Pine and starring Richard Denning, Catherine Craig and Russell Hayden.

== Plot summary ==
An army nurse undergoes a mission in taking an amnesia victim, who was imprisoned by the Japanese during World War II, to the United States via plane. The passengers include a Japanese colonel on his way to Manila to face war-crime charges, and a couple who were married on the day they were liberated from a Japanese prison camp. During the flight, the colonel breaks away from his guards, causing the plane to spiral out of control, and it plummets into the sea...

== Cast ==
*Richard Denning as Captain Allen Danton
*Catherine Craig as Susan Briscoe
*Russell Hayden as Captain Jim Willis
*Ann Doran as Mrs. Rollin Hartley
*Byron Barr as Lt. Martin Pinkert
*Richard Loo as Colonel Yamura
*Don Castle as Lt. Pete Sturdevant
*George Tyne as Sergeant Blair
*Keith Richards as Smith / Philip Briscoe John Eldredge as Rollin Hartley

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 