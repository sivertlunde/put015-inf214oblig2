Death Mills
 
{{Infobox film
| name           = Death Mills
| image          =
| caption        =
| director       = Billy Wilder Hanus Burger|Hanuš Burger
| producer       =
| writer         = Hanus Burger|Hanuš Burger
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        = Billy Wilder  (supervisor) 
| distributor    = United States Department of War
| released       =  
| runtime        = 22 minutes
| country        = United States German
| budget         =
}}
  German audiences Nazi regime. For the German version, Die Todesmühlen, Hanus Burger is credited as the writer and director, while Wilder supervised the editing. Wilder is credited with directing the English-language version.

The film is a much-abbreviated version of German Concentration Camps Factual Survey|German Concentration Camps Factual Survey, a 1945 British government documentary that was not completed until nearly seven decades later.   

The German language version of the film was shown in the US sector of West Germany in January 1946.   

== Synopsis ==
  bulldozer pushes bodies into a mass grave at Belsen. April 19, 1945]] pageants and parades was millions of men, women and children who were tortured to death - the greatest mass murder in human history," then fades into German civilians at Gardelegen carrying crosses to the local concentration camp.
 Jewish people. The film states that 20 million people were killed and describes many of the familiar aspects of the Holocaust, including the medical experiments and the gas chambers.

==Concentration camps==
  at the Hadamar Euthanasia Centre, May 1945]] rings of Buchenwald victims]]
The images from the camps include shots of piles of skeletal corpses, naked skeletal survivors (often supported by fellow prisoners) together with footage of prosperous-looking German citizens being forced to observe their suffering. Some are also forced to carry the corpses to be buried, although most of this work was usually carried out by ex-camp guards (as at Belsen concentration camp).

There are shots of mass graves, as well as of individual burials, as at Hadamar, now known to be a euthanasia or action T4  centre. Some of the footage appears to be of Soviet origin, and includes shots taken at Majdanek death camp which was one of the first camps to be liberated in 1944 by the Red Army. There are shots of the crematoria at several camps, as well as the infamous slogans erected at the entrances of most camps, such as Arbeit macht frei.

Perhaps most moving of all are the piles of stolen personal belongings of gassed victims, filmed by the Russians when they liberated Auschwitz, as well as by US troops at Buchenwald. They include piles of clothes, shoes, toys, wedding rings and gold teeth destined for the vaults of the Reichsbank.

== See also ==
*German Concentration Camps Factual Survey
*List of Allied propaganda films of World War II
*List of Holocaust films The Nuremberg Trials - Soviet film about the Nuremberg trials
*That Justice Be Done - American film about the Nuremberg trials

==References==
 

== External links ==
*  
*   (English version) 
*   (German version)
*  
*  
*  

 

 
 
 
 
 
 
 