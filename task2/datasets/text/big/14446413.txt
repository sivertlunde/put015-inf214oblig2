Beyond the Call
 
{{Infobox film
| name           = Beyond the Call
| image          =
| caption        =
| director       = Adrian Belic
| producer       = Adrian Belic
| writer         = Adrian Belic
| music          = Marcello De Francisci
| cinematography = Adrian Belic
| editing        = Jennifer Chinlund
| released       =
| runtime        = 82 minutes
| country        = United States English
| budget         =
| gross          =
}}
 2006 documentary film about three middle-aged men who are former soldiers and modern-day knights. They travel the world delivering life saving humanitarian aid directly into the hands of civilians and doctors in some of the most dangerous yet beautiful places on Earth, the front lines of war. It is the directorial debut of Academy Award nominee Adrian Belic. The film has been screened in over 80 film festivals on five continents, winning 25 awards so far.
 PBS aired a 60-minute version of Beyond the Call on January 23, 2007 as part of its Independent Lens series.

== Synopsis ==

Ed Artis, Jim Laws, and Walt Ratterman are former soldiers and modern-day knights. In an Indiana Jones meets Mother Teresa adventure, they travel the world delivering life saving humanitarian aid directly into the hands of civilians and doctors in some of the most dangerous yet beautiful places on Earth, the front lines of war.

They are self-styled   organization, whose motto is "High Adventure and Service to Humanity." Their specialty is going where death from landmines, bullets, or bombs is as frequent as death from hunger, disease, or the elements. Their personal convictions and courage drive them to places such as Afghanistan, Albania, Chechnya, Cambodia, Burma, Thailand, Rwanda, and the southern Philippines, often when few if any other humanitarian aid organizations are around.

The camera follows Artis, Laws, and Ratterman as they take us on a journey into the heart of humanity and the soul of courage.

Walt Ratterman died afterwards in the 2010 Haiti earthquake.

== Reception ==

The film received very positive reviews. It has been screened in over 80 film festivals on five continents, winning 25 awards, including Grand Jury Prizes and Audience Awards.

== External links ==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 

 