The Flying Irishman
{{Infobox film
| name           = The Flying Irishman
| image          = Poster of the movie "The Flying Irishman".jpg
| caption        = 
| director       = Leigh Jason
| producer       = 
| writer         = Ernest Pagano Dalton Trumbo
| narrator       = 
| starring       = Douglas Corrigan
| music          = Roy Webb
| cinematography = J. Roy Hunt Arthur E. Roberts
| studio         = RKO Radio Pictures, Inc.
| distributor    = RKO Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Flying Irishman (aka Born to Fly) is a 1939 biographical drama film produced by RKO Pictures about Douglas Corrigans unofficial transatlantic flight the previous year in a dilapidated Curtiss Robin light aircraft.  The film was directed by Leigh Jason based on a screenplay by Ernest Pagano and Dalton Trumbo. 

The Flying Irishman covers the material of Corrigans autobiography, Thats My Story, from his early life to his return from his wrong-way flight. Because of the huge popularity of Corrigan and his flight, the screenplay assumes that the material is already familiar to its audience. 

==Plot==
 
In 1938, an unlikely event unfolds as pilot Douglas Corrigan returns to the United States after his transatlantic flight, made "the wrong way" across the Atlantic. The passion to fly was there from an early age as young Douglas faced some hardships as his parents separated, leaving his mother (Dorothy Peterson) to rear two sons and a daughter. 

When his mother dies, Douglas becomes the family breadwinner, putting his Henry (  (who also did not have a college education), to prove his exceptional ability.  

After earning enough money as a weldler to purchase and modify a second-hand aircraft, Doug goes into business with Henry as a barn-stormer to finance a transatlantic attempt, but Henry eventually tires of the drudgery of eking out a living day to day. Doug learns about a new commercial airline route to Ireland and decides to make a solo flight to prove his qualifications. In New York, after his plane is grounded by an inspector, Dougs brother arranges a return flight to San Diego, lifting the flight ban. Once in the air, Doug instead heads off to Ireland, and, 28 hours later, makes it successfully to Dublin. When Doug rejects an airline offer of a job as vice-president and chief pilot because he only wants to be a pilot, he is told that his goal is impossible, because passengers going "to Cheyenne, Wyoming|Cheyenne" want to be confidant of arriving at the correct destination!

==Cast==
 
* Douglas Corrigan as himself Paul Kelly as Butch Brannan Robert Armstrong as Joe Alden, the flying instructor
* Gene Reynolds as Douglas Corrigan as a boy
* Donald MacBride as Roy Thompson
* Eddie Quillan as Henry Corrigan, Douglas brother
* J. M. Kerrigan as Clyde Corrigan Sr., Douglas father
* Dorothy Peterson as Mrs. Corrigan
 

==Production== Van Nuys and Culver City, California, to exploit the huge public interest in the adventure. Although the production attempted to provide an accurate account of his life story, with Corrigan as himself in the lead role, many of the other parts were played by established character actors, which undermined its attempt at realism. 

==Reception==
The Flying Irishman was a typical "B" film in terms of length and treatment as well as having an obviously uncomfortable individual featured in the lead role, yet the "fun" of the unlikely tale was conveyed. Frank Nugent of The New York Times charitably called it a "freak picture" that, nonetheless, was "... lively, unassuming, natural and thoroughly entertaining." 

==References==
Notes
 

Citations
 
Bibliography
 
* Corrigan, Douglas. Thats My Story. New York: E.P. Dutton, 1938.
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 