2010 (film)
 
{{Infobox film
| name           = 2010
| image          = 2010-poster01.jpg
| caption        = Theatrical release poster
| director       = Peter Hyams
| producer       = Peter Hyams
| screenplay     = Peter Hyams
| based on       =  
| starring       = {{Plain list|
* Roy Scheider
* John Lithgow
* Helen Mirren
* Bob Balaban
* Keir Dullea
}}
| music          = David Shire
| cinematography = Peter Hyams
| editing        = {{Plain list|
* James Mitchell
* Mia Goldman
}}
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 116 minutes 
| country        = United States
| language       = English
| budget         = $28 million 
| gross          = $40.4 million  }} 
}}
2010 (also known as 2010: The Year We Make Contact) is a 1984 American  , and is based on  .

Roy Scheider, Helen Mirren, Bob Balaban and John Lithgow star, along with Keir Dullea and Douglas Rain of the cast of the previous film.

==Plot==
 
 David Bowman, Io before Discovery Two is ready. Floyd, along with Discovery designer Walter Curnow and HAL 9000s creator Dr. Chandra, join the Soviet mission.
 signs of electrostatic build-up, but Floyd suspects it was a warning to stay away from Europa.
 braking maneuver Monolith that the Discovery was originally sent to investigate. Cosmonaut Max Brailovsky travels to the Monolith in an EVA pod, at which point the Monolith briefly opens with a burst of energy, sending Maxs pod spinning off into space. On Earth, Dave Bowman, now an incorporeal being that existed inside the Monolith, appears on his wifes television screen and wishes her farewell. He also visits his terminally ill mother just before she dies.
 National Security paranoid mental breakdown. Although the order bears his signature, Floyd is outraged that this was done without his knowledge.

On Earth, tensions between America and Russia escalate to a state of war. The Americans are instructed to leave the Leonov and move to the Discovery, with both crews ordered not to communicate with each other. Both crews plan to leave Jupiter separately when a launch window opens in several weeks, however, Bowman appears to Floyd and says it is imperative that everybody leaves within two days. Stunned by Bowmans appearance, Floyd returns to the Leonov to confer with Captain Tanya Kirbuk, who remains skeptical. The Monolith then suddenly disappears, and a growing black spot appears on Jupiter itself. The spot is actually a vast group of Monoliths that are constantly multiplying. The Monoliths begin shrinking Jupiters volume, increasing the planets density, and modifying its chemical composition. This convinces the two crews that they must leave soon. Since neither ship can reach Earth with an early departure, they work together to use the Discovery as a booster rocket for the Leonov, though it means the Discoverys (and HALs) destruction. Bowman appears once again to HAL and tells him that they will soon be together after he transmits one final message to Earth:

{{center|  
ALL THESE WORLDS
ARE YOURS EXCEPT
EUROPA
ATTEMPT NO
LANDING THERE
USE THEM TOGETHER
USE THEM IN PEACE
  }}
 transforms the planet into a small star. Discovery is consumed in the blast after the Leonov breaks away to safety. The new stars miraculous appearance inspires American and Soviet leaders to seek peace. Over the centuries that follow, Europa gradually transforms from an icy wasteland to a humid jungle covered with plant life. A Monolith stands in the primeval Europan swamp, waiting for intelligent life forms to evolve.

==Cast==
 
* Roy Scheider as Dr. Heywood R. Floyd
* John Lithgow as Dr. Walter Curnow
* Helen Mirren as Tanya Kirbuk
* Bob Balaban as Dr. Chandra|Dr. R. Chandra Dave Bowman
* Douglas Rain as the voice of HAL 9000
* Madolyn Smith as Caroline Floyd
* Saveliy Kramarov as Dr. Vladimir Rudenko
* Taliesin Jaffe as Christopher Floyd
* James McEachin as Victor Milson
* Mary Jo Deschanel as Betty Fernandez, Bowmans widow
* Elya Baskin as Maxim Brailovsky
* Dana Elcar as Dimitri Moiseyevich
* Oleg Rudnik as Dr. Vasili Orlov
* Natasha Shneider as Irina Yakunina
* Vladimir Skomarovsky as Yuri Svetlanov
* Victor Steinbach as Nikolaj Ternovsky
* Candice Bergen as the voice of SAL 9000 (credited as "Olga Mallsnerd")
 

===Cameos=== Soviet Premier by the 2001 producer, writer, and director, Stanley Kubrick.

==Production==

===Development and filming===
When Clarke published his novel 2010: Odyssey Two in 1982, he telephoned Stanley Kubrick, and jokingly said, "Your job is to stop anybody   making it   so I wont be bothered." LoBrutto 1997, p. 456.  Metro-Goldwyn-Mayer (MGM) subsequently worked out a contract to make a film adaptation, but Kubrick had no interest in directing it. However, Peter Hyams was interested and contacted both Clarke and Kubrick for their blessings:
 

While he was writing the screenplay in 1983, Hyams (in Los Angeles) began communicating with Clarke (in Sri Lanka) via the then-pioneering medium of e-mail. The two would discuss the planning and production of the film on an almost daily basis using this method. Their correspondence was published in 1984 as The Odyssey File: The Making of 2010. The book illustrates Clarkes fascination with the new method of communication, and also includes Clarkes list of the top science fiction films ever made. In order to give the publishers enough lead-time to have it available for the release of the movie, the book terminates while the movie is still in pre-production. At the point of the last e-mail, Clarke had not yet read the script, and Roy Scheider was the only actor who had been cast.  

Principal photography on the film began in February 1984 for a 71-day schedule. The majority of the film was shot on MGMs soundstages in Los Angeles, with the exception of a week of location work in Washington DC, Rancho Palos Verdes, California, and at the Very Large Array in New Mexico. 

===Special effects===
  Boss Film Corporation.

Early in the production of 2010, Hyams had learned that all of the original large spacecraft models from "2001", including the original 50-foot model of the "Discovery One", had been destroyed following the filming, as ordered by Kubrick, as had all of the original model-makers designs for building the "Discovery One". Consequently, the model-makers at EEG had to use frame-by-frame enlargements from a 70mm copy of "2001" to recreate the original large "Discovery One" model. The "Leonov" spacecraft, as well as several of its interior crew areas and other elements of the spacecrafts advanced technology, were designed by the noted conceptual artist Syd Mead.

Although computer-generated imagery (or CGI) was still in an early phase of development in 1984, the special effects team of 2010 used CGI to create the dynamic-looking cloudy atmosphere of the planet Jupiter, as well as the swarm of monoliths that engulf the planet and turn it into a Sun for the planet Europa. Digital Productions would use data supplied by the Jet Propulsion Laboratory to create the turbulent Jovian atmosphere. This was one of the first instances of what the studio would later refer to as "Digital Scene Simulation", a concept they would take to the next level with The Last Starfighter.

In order to maintain the realism of the lighting in outer space, in which light would usually come from a single light source (in this case, the Sun), Edlund and Hyams decided that blue-screen photography would not be used for shooting the space scenes. Instead, a process known as front-light/back-light filming was used. The models were filmed as they would appear in space, then a white background was placed behind the model and the first pass was repeated. This isolated the models outlines so that proper traveling mattes could be made. All of this processing doubled the amount of time that it took to film these sequences, due to the additional motion-control pass that was needed to generate the matte. This process also eliminated the problem of "blue spill", which is the main disadvantage of blue-screen photography. In this, photographed models would often have blue outlines surrounding them because a crisp matte was not always possible to achieve.

Blue-screen photography was used in the scene in which Floyd uses two pens to demonstrate his plan to dock the two spaceships together for the films climax. Initially, the scene was filmed with Roy Scheider attaching the pens to a piece of movable glass that was placed between him and the camera but this proved unworkable. Scheider then performed the scene without the pens actually being present, and the pens were filmed separately against a blue screen using an "Oxberry" animation stand that was programmed to match Scheiders movements.

===Music=== Tony Banks (keyboardist for the band Genesis (band)|Genesis) was commissioned to do the soundtrack for 2010. However, Banks material was rejected  and David Shire was then selected to compose the soundtrack, which he co-produced along with Craig Huxley. The soundtrack album was released by A&M Records.

Unlike many film soundtracks up until then, the soundtrack for 2010 was composed for and played mainly using digital synthesizers. These included the Synclavier by the New England Digital company and a Yamaha DX1. Only two compositions on the soundtrack album feature a symphony orchestra. Shire and Huxley were so impressed by the realistic sound of the Synclavier that they placed a disclaimer in the albums liner notes stating "No re-synthesis or sampling was employed on the Synclavier." 

Andy Summers, guitarist for the band The Police, performed a track entitled "2010", which was a modern new-wave pop version of Richard Strausss Also Sprach Zarathustra (which had been the main theme from 2001: A Space Odyssey). Though Summers recording was included on the soundtrack album and released as a single, it was not used in the film. For the B-side to the single, Summers recorded another 2010-based track entitled "To Hal and Back", though this appeared in neither the film nor the soundtrack album.  

==Release==

===Box office===
2010 debuted at number two at the North American box office, taking $7,393,361 for its opening weekend.  It was held off from the top spot by Beverly Hills Cop, which became that years highest grossing film in North America. During its second week, 2010 faced competition from two other new sci-fi films; John Carpenters Starman (film)|Starman and David Lynchs Dune (film)|Dune,  but ultimately outgrossed both of them by the end of its domestic theatrical run. It finished with just over $40 million at the domestic box office and was the 17th highest grossing film in North America to be released in 1984. 

===Comic book=== Tom Palmer. Marvel Super Special #37  and as a two-issue limited series. 

===Home media===
2010 was first released on home video and laserdisc in 1985, and on DVD (R1) in 1998 by   when, in reality, it is simply 4:3 letterboxed and not anamorphic (the MGM version of the DVD makes no such claim). The R1 and R4 releases also include the film trailer and a 10-minute behind-the-scenes featurette "2010: The Odyssey Continues" (made at the time of the films production), though this is not available in other regions.

The film was released on Blu-ray Disc on April 7, 2009. It features a BD-25 single-layer presentation, now in high definition 16:9 (2.40:1) widescreen with 1080p/VC-1 video and English Dolby TrueHD 5.1 Surround audio. In all regions, the disc also includes the films original "making of" promotional featurette (as above) and theatrical trailer in standard definition as extras.

==Reception==

===Critical reception===
Critical reaction to 2010 has been mixed to positive, with the film holding a rating of 66% of Rotten Tomatoes, based on 32 reviews.  Roger Ebert gave 2010 three stars out of four, writing that it "doesnt match the poetry and the mystery of the original film" and "has an ending that is infuriating, not only in its simplicity, but in its inadequacy to fulfill the sense of anticipation, the sense of wonder we felt at the end of 2001". He concluded, however: "And yet the truth must be told: This is a good movie. Once weve drawn our lines, once weve made it absolutely clear that 2001 continues to stand absolutely alone as one of the greatest movies ever made, once we have freed 2010 of the comparisons with Kubricks masterpiece, what we are left with is a good-looking, sharp-edged, entertaining, exciting space opera". 

James Berardinelli also gave the film three stars out of four, writing that "2010 continues 2001 without ruining it. The greatest danger faced by filmmakers helming a sequel is that a bad installment will in some way sour the experience of watching the previous movie. This does not happen here. Almost paradoxically, 2010 may be unnecessary, but it is nevertheless a worthwhile effort."  Vincent Canby gave 2010 a lukewarm review, calling it "a perfectly adequate though not really comparable sequel" that "is without wit, which is not to say that it is witless. A lot of care has gone into it, but it has no satirical substructure to match that of the Kubrick film, and which was eventually responsible for that films continuing popularity." 

===Awards and nominations===
2010 was nominated for five Academy Awards:       Best Art Direction (Albert Brenner and Rick Simpson) Best Makeup (Michael Westmore) Best Visual Effects Best Costume Design (Patricia Norris) Best Sound Presentation (Michael J. Kohut, Aaron Rochin, Carlos Delarios and Gene Cantamessa)

The film was also nominated for three Saturn Awards; Best Science Fiction Film, Best Costumes (Patricia Norris), and Best Special Effects (Richard Edlund). 

2010 won the Hugo Award for Best Dramatic Presentation in 1985. 

==References==
 
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 