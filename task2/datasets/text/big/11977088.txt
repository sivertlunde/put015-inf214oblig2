Chandni Chowk to China
 
 
 
{{Infobox film
| name           = Ch China
| image          = Chandnichowktochina.jpg
| alt            =  
| caption        = Theatrical release poster simplified    = 月光集市到中国
| traditional = 月光集市到中國
| jyutping       = yue4guang1 ji2shi4 dao4 Zhong1guo2
| pinyin = yuèguāng jí shì dào zhōngguó }}
| director       = Nikhil Advani
| producer       = Rohan Sippy Ramesh Sippy Mukesh Talreja
| screenplay     = Rajat Arora Sridhar Raghavan
| story          = Rajat Arora Sridhar Raghavan
| starring       = Akshay Kumar Deepika Padukone Mithun Chakraborty Ranvir Shorey Bohemia
| narrator = 
| cinematography = Himman Dhamija
| editing        = Aarif Shaikh
| studio         = 
| distributor    = Ramesh Sippy Entertainment People Tree Films Pvt. Ltd. Orion Pictures Warner Bros.
| released       =  
| runtime        = 154 minutes
| country        = India China
| language       = Hindi Cantonese
| budget         =   
| gross          =    
}}
 martial arts action comedy film. It is directed by Nikhil Advani and stars Akshay Kumar and Deepika Padukone in the lead roles with Mithun Chakraborty and Hong Kong action cinema actor Gordon Liu among the co-stars. In addition to being shot in China, many parts of the film were shot in Bangkok, Thailand,   although some of the China scenes were shot in sets in the Shanghai Film Studio.  , no date 

Distributed in the U.S. and co-produced by Warner Bros., it is the third Bollywood movie made and distributed in partnership with a major Hollywood studio, following Sonys Saawariya (2007) and Walt Disney Pictures animated feature Roadside Romeo (2008).  It is Warner Bros. Pictures first Hindi film.    Some critics panned the film inspired from Kung Fu Hustle.  

==Plot==
Sidhu (Akshay Kumar) is a lowly vegetable cutter at a roadside food stall in the Chandni Chowk section of Delhi, who consults astrologers, tarot card readers, and fake fakirs despite his foster father Dadas (Mithun Chakraborty) exhortations. When two strangers from China claim him as a reincarnation of war hero Liu Shen and take him to China, Sidhu, encouraged by trickster Chopstick (Ranvir Shorey), believes he will be feted as a hero, unaware of his own recruitment to assassinate the smuggler Hojo (Gordon Liu).

Sidhu travels to China with Chopstick. Along the way he meets Sakhi (Deepika Padukone), the Indian-Chinese spokesmodel known as Ms. Tele Shoppers Media, or Ms. TSM, who also appears in China. Her twin sister Suzy, known as the femme fatale Meow Meow, works for Hojo, not knowing Hojo tried to kill her father, Inspector Chiang (Roger Yuan). Sidhu, through a series of accidents, initially eludes Hojo, but Hojo eventually exposes him as a fraud. Thereupon Hojo kills Dada, and Sidhu is beaten and urinated by Hojo. Injured and disgraced Sidhu vows revenge. He thereafter encounters an amnesiac vagrant, whom he later identifies to Sakhi as Inspector Chiang. Chiang later recovers his memory and trains Sidhu in kung fu. When Hojo again meets with Sidhu, Suzy injures Chiang; but upon seeing Sakhi, betrays Hojo. Sidhu thereafter fights Hojo in single combat, eventually using a modified vegetable-cutting technique to overwhelm him. In the aftermath, Sidhu opens a vegetable stall in China, but is recruited to fight for some African pygmies. The film thereupon concludes with the announcement "To be Continued    –   Chandni Chowk to Africa".

==Cast== Liu Sheng
* Deepika Padukone as Sakhi (Ms.TSM) / Suzy (Meow Meow)
* Mithun Chakraborty as Dada (extended appearance)
* Ranvir Shorey as Chopstick
* Roger Yuan as Police Inspector Chiang Kohung
* Gordon Liu as Hojo
* Kiran Juneja as Chiangs wife
* Kevin Wu as Frankie
* Conan Stevens as Joey

== Production ==
The film, earlier known as Mera Naam Chin Chin Choo and also Made in China,   via Naachgaana.com, 8 December 2007  is written by Sridhar Raghavan.

Shooting began in January 2008 and included a schedule in China.  , 21 August 2007 

The music is by Shankar Ehsaan Loy|Shankar, Ehsaan and Loy. The film also features music by Kailash Kher, Bappi Lahiri-Bappa Lahiri and a rap song sung by Akshay Kumar and composed by Punjabi rapper Bohemia (musician)|Bohemia. The soundtrack album was released 2 December 2008.

==Release==

===Box office reception===
Chandni Chowk to China earned   in its opening weekend.  , The Economic Times, 20 January 2009  It went on to earn a total of   in India.     The films total North American box office in the four weeks of running was $921,738, and total worldwide gross was $13,439,480.  The film was rated as a flop. 

===Critical reception===
The movie received negative reviews. It received 46% positive ratings on the film-critics aggregate site Rotten Tomatoes  and a 44 out of 100 score from Metacritic.  Claudia Puig of USA Today said, "This Indian/Chinese cinematic hybrid is likable and entertaining but overlong and occasionally hokey", and that star Akshay Kumars "physical humor brings to mind Jim Carrey".  John Anderson of Variety (magazine)|Variety wrote, "If Chandni Chowk to China were a person, it would need Valium", and found that "everything is fast and furious, hilarious, hysterical and frantic. Some of the sequences as are quite beautiful and, in the case of the dance numbers featuring Padukone, stunning. But its the fight scenes as that truly take off".  Frank Lovece of Newsday wrote, "Less a Bollywood bonbon than a pan-Asian fusion dish, this combination of Indian musical and Chinese chopsocky is, nonetheless, delicious fun". 
 choreographic beauty Michael Philips IBN termed it a tiring watch, while praising Kumars performance. 

The film has received one award nomination, with Deepika Padukone being nominated for Best Actress at the 3rd Asian Film Awards held in March 2009. 

==Controversy==
In Nepal, India, and other buddhist countries there were protests against the film due to a passing claim that Buddha was born in India; Lumbini, which is the birthplace of Buddha, is located in Nepal. 
 Usage of Dhoti in Nepal) hurled at the Indian embassy in Katmandu. 

The protests continued for several days, despite the Nepali distributor deleting the piece of narration that mentioned Buddha in the copies of the film shown in Nepal. On Thursday, 22 January Nepali cinemas stopped and banned screening Chandni Chowk to China; but In India, Indian government took no action against that narration. 

Aftermath of the Nepal controversy, actor Shekhar Suman heavily criticized the film with some derogatory and rude comments adding that 16 January must be declared as National Mourning Day Of Cinema. Suman also panned the movie as amateurish attempt by Nikhil Advani & actors and countrys worst film.  Mostly, supporters of Kumar especially Nikhil Advani stated that it was a publicity stunt to bring Sumans son Adhyayan Suman in limelight for release of his film Raaz 2. Suman replied that it wasnt a publicity act and the latter apologized to Kumar for it  

== Music ==
{{Infobox album
| Name        = Chandni Chowk to China
| Type        = Compilation
| Artist      = Shankar-Ehsaan-Loy
| Cover       = Chandni Chowk to China.jpg
| Released    = 2 December 2008
| Recorded    = Feature film soundtrack
| Length      = 38:23
| Label       = T-Series
| Producer    = Rock on!! (2008)
| This album  = Chandni Chowk to China (2008)
| Next album  = Luck by Chance (2008)
}}
The music of Chandni Chowk to China was released on 2 December 2008. The album features composers as diverse as Shankar-Ehsaan-Loy, Kailash-Paresh-Naresh, Bappi Lahiri-Bappa Lahiri and Bohemia (musician)|Bohemia.

===Reception===
Joginder Tuteja of Bollywood Hungama.com rated it 3.5/5, claiming, "Chandni Chowk to China is clearly the next musical hit in the making."    

===Tracks===
The album consists of the following eight tracks:

{{tracklist
| headline        = Tracklist
| music_credits   = yes
| extra_column    = Artist(s)
| title1          = S.I.D.H.U
| extra1          = Kailash Kher
| music1          = Kailash Kher, Naresh (singer)|Naresh, Paresh
| length1         = 5:04
| title2          = Chandni Chowk To China
| extra2          = Neeraj Shridhar, Anushka Manchanda, Shankar Mahadevan
| music2          = Shankar-Ehsaan-Loy
| length2         = 4:26
| title3          = India Se Aaya Tera Dost (Aap Ki Khatir)
| extra3          = Bappi Lahiri, Ravi K Tripathi
| music3          = Bappi Lahiri
| length3         = 6:29
| title4          = Tera Naina
| extra4          = Shankar Mahadevan, Shreya Ghoshal
| music4          = Shankar-Ehsaan-Loy
| length4         = 4:18
| title5          = Chak Lein De
| extra5          = Kailash Kher
| music5          = Kailash Kher, Naresh (singer)|Naresh, Paresh
| length5         = 4:25
| title6          = Chandni Chowk To China (CC2C)
| extra6          = Bohemia (musician)|Bohemia, Akshay Kumar Bohemia
| length6         = 3:44
| title7          = Chandni Chowk To China (Remix)
| extra7          = Dj Amyth
| music7          = Shankar-Ehsaan-Loy
| length7         = 4:41
| title8          = Chak Lein De (Remix)
| extra8          = Dj Amyth
| music8          = Kailash Kher, Naresh (singer)|Naresh, Paresh
| length8         = 4:36
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 