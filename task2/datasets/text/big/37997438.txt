Late for Dinner
 
{{Infobox film
| name = Late For Dinner
| image = 
| caption = 
| alt =
| director = W.D. Richter
| producer = Gary Daigler Dan Lupovitz W.D. Richter
 | writer = Mark Andrus
| starring = Peter Berg Brian Wimmer Marcia Gay Harden
| music = David Mansfield
| cinematography = Peter Sova Robert Leighton
| studio = Castle Rock Entertainment New Line Cinema
| distributor = Columbia Pictures 
| released =  
| runtime = 99 minutes
| country = United States
| language = English
| budget = 
| gross = $8,906,823 
}}
Late for Dinner is a 1991 American film directed by W. D. Richter and starring Peter Berg, Brian Wimmer and Marcia Gay Harden. The supporting cast features Peter Gallagher and Richard Steinmetz, along with Janeane Garofalos first movie appearance, briefly playing a cashier during a comical sequence in a burger joint.
 cryogenically frozen for 30 years.

==Plot== kidney disease), are running from the police because Willie shot and killed a man. It was self-defense, but a witness intends to frame them both for kidnapping and murder.

Through a flashback, Frank explains what happened: Willie Husband (Brian Wimmer), his wife, Joy (Marcia Gay Harden), and little daughter, Jess (Cassy Friel), live a happy life in Santa Fe, New Mexico, where Joys brother, Frank Lovegren (Peter Berg), lives with them. Willie lost his job at the milk company, and they have fallen behind in house payments. They learn the bank intends to foreclose, so Willie pays a visit to the banker, Bob Freeman (Peter Gallagher), to show that he made up the back payments with funds borrowed from relatives. Freeman takes Willies receipt and burns it, then offers to buy his house for less than its worth. He is building a huge, new home development in the area, and he needs all the land in Willies neighborhood to do it. Willie refuses to undersell or to bow to pressure, and destroys an expensive chair and rug before storming out of the office. Thats the last straw for Freeman, and he vows revenge.

On the drive home, Willie discovers Frank "borrowed" Freemans little boy, Donald (Ross Malinger), because Frank realized the boy was being emotionally abused by his father. He pleads to Willie to let him keep Donald, as he is small and wont eat much. When they get home, Joy convinces Willie they have to accept the lowball offer and just get out while they can. Willie calls Freeman to tell him that he will accept his offer after all, and that, by the way, his son had somehow "wedged himself into the back seat" of his car. Realizing this is his chance to get what he wants, Freeman accuses Willie of kidnapping. They agree to meet in the desert just outside of town, Willie believing it is only to return the boy and to get the $9,000 Freeman offered for his house.

Once they are face to face, Freeman produces an envelope filled with $15,000, intending to make this look like a ransom exchange. Freeman and his henchman then both pull guns, right in front of little Donald, as Freeman admits he was the one who got Willie fired from his job so he would be unable to pay his mortgage, making it possible for him to buy the house for a low price. He is confident Donald will tell the police whatever he says to tell them, and as he aims his gun, Willie jumps Freeman, and the henchman shoots Willie in the shoulder. Freeman reaches for the gun, but Frank tackles him and knocks him out. The henchman aims his gun at Frank, but Willie manages to shoot the man in the chest. It was self-defense, but they know Freeman will claim otherwise. They have to leave Donald there and take off before the police arrive, whom Freeman called before he got there. They try to stop at home first, but they can see police cars are already there. Willies little daughter sees them coming, and silently shakes her head to tell them not to stop, so, sadly, they drive away. They dont realize this is the last time they may ever see Joy and Jess.
 procedure that cryogenic process.

29 years later, in 1991, a truck crashes, destroying and flooding the cryogenic storage facility, and causing the tanks storing the two men to burst open. Willie and Frank, in body bags, are revived by floating in electrified water. Suddenly regaining consciousness, they escape from the bags, freezing, wet, and feeling confused. Once they get their wits about them, they decide they need to get back to Santa Fe, assuming Joy and Jess will be there waiting. Dr. Chilblains left them clothing, $100, and a note that doesnt really explain much. They are starving and go to buy food. The culture shock, high prices, and modern technology are amazing and overwhelming to them. Frank realizes he needs his medication, so they visit a hospital, which is where they finally learn the date and what actually happened to them.

They manage to get back to Santa Fe and track down Willies daughter, Jess (Colleen Flynn), who is now grown up with "her own telephone number… her own house." Seeing Jess all grown up, only now does Frank accept that so much time has passed. Once she calms down, she explains to Willie that Joy had to move on; she married someone else, but is now divorced, because her second husband just didnt measure up to the standard set by her first husband. She also told him that Joy stood up to Freeman; she managed to keep their home, and she exposed Freemans scam. Freeman then went away and built half of Phoenix, and eventually someone murdered him.

Willie and Jess leave Frank at her house, and go to Joys house to meet her. While theyre gone, Frank meets Jess husband, who turns out to be Donald Freeman (Richard Steinmetz). At Joys house, Jess makes her sit down and listen to Willie explain, but to keep her back turned and not look at him. When she finally does look, she has trouble accepting the fact that he hasnt aged, and says they can never be together again because too much has changed and she is too old. Willie manages to woo her back by reminding her why he loved her, that he still loves her, and he knows she hasnt changed.

Through the closing credits, we see "home movies" and photographs depicting their renewed lives together, including the fact that Joy donated a kidney to save Franks life.

==Cast==
*Peter Berg as Frank Lovegren
*Brian Wimmer as Willie Husband 
*Marcia Gay Harden as Joy Husband
*Colleen Flynn as Adult Jessica Husband
*Peter Gallagher as Bob Freeman
*Richard Steinmetz as Adult Donald Freeman
*Cassy Friel as Little Jessica Husband
*Ross Malinger	as Little Donald Freeman
*Luce Rains as Duane Gardener (billed as Steven Schwartz-Hartley)
*John Prosky as Officer Tom Bostich
*Bo Brundin as Dr. Dan Chilblains
*Donald Hotton	 as Dr. Chris Underwood
*Kyle Secor as Leland Shakes
*Billy Vera as the Male Radio D.J. (voice)
*Jeremy Roberts as the Truck Driver
*Janeane Garofalo as the Counter Girl

==Release==

===Critical reception===
The film received a 58% fresh rating at Rotten Tomatoes. 

===Box office===
The film was released September 20, 1991 and took in just under 9 million dollars.

===Home media   ===
The film was released on VHS on April 29, 1992 by New Line Home Video and SVS/Triumph.

It was re-released on VHS on June 23, 1998 by MGM Home Entertainment under the Movie Time Label.

It was eventually released on DVD on December 31, 2009.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 