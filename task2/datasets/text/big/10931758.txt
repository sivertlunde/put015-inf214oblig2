Boxer from Shantung
 
 
 

{{Infobox film
| name           = The Boxer From Shantung
| image          = The Boxer from Shantung.jpg
| image_size     =
| caption        =
| director       = Chang Cheh
| producer       =
| writer         =
| narrator       =
| starring       = Chen Kuan Tai David Chiang Cheng Yong Hip Ching Li Wang Ching
| music          =
| cinematography =
| editing        =
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        =
| country        = Hong Kong
| language       = Mandarin
| budget         =
}}
 1972 Shaw Brothers kung fu film directed by Chang Cheh, and starring David Chiang and Chen Kuan Tai.

== Plot ==
Boxer from Shantung follows Ma Yongzhen and Xiao Jiangbei. Ma and Xiao are manual laborers busting their tails in Shanghai at the beginning of the film, and Mas first encounter with the crime lord Tan Si starts him off on the underworld influence ladder. Step by step, he earns the respect of everyone he meets, either with his strength of character or by beating them up. When he gets a really big break by defeating a Russian strongman, he indulges himself in a fancy cigarette holder, much like the one Tan Si uses. However, he is mindful of his humble beginnings, and of the fact that times are still tough for many in Shanghai. He shares his good fortune with his old fellow wage slaves, and when they assist him in his various extralegal activities, he lectures them on the futility of trying to extort money from people who simply dont have any to spare.

The tea house that Ma and his underlings frequent employs a singer, Jin Lingzi, and her uncle, who provides the music. Ms. Jins hopes that Mas arrival will mark a turn for the better for conditions in the crime-plagued city are dashed when she ascertains that he isnt different enough from the other bosses shes seen rise and fall.

== External links ==
*  
*  
*   at Hong Kong Cinemagic

 
 
 
 
 
 
 
 
 
 
 


 
 

 