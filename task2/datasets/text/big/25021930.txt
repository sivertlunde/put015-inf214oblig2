Otomo (film)
Otomo is a German film about racism directed by Frieder Schlaich, distributed by ArtMattan productions, and was released in 1999. 

Actor Isaach de Bankole plays Frederic Otomo, and actress Eva Mattes, Gisela, the woman who tries to help him. 

Otomo was the winner of numerous awards, including the Diversity in Spirit Award, Vancouver 2000, Best Film, Belgamo 2000, Best Actress, Valinciennes 2000 and a Kino Award.  

== Critical Reviews ==

The film received generally positive reviews from Western critics. The review aggregator   reported that the film had an average score of 60 out of 100, based on 8 critic reviews. 

== Plot ==

The journey begins with Frederic Otomo departing from his home, early one morning, to get a job at a factory in Germany. There, he is refused employment from a cast of all Caucasian workers, predicated upon the claim that his shoes are not proper for the work. Later, he departs for home on a train, disappointed, to be kicked off of the train by a ticket-taker who claims that his ticket has expired. Suddenly, Otomo becomes a fugitive when the ticket-taker refuses to let him off of the train, and instead, tries to have him arrested. Fleeing, he later encounters a grandmother who attempts to help him escape Stuttgart, to Amsterdam. He falls in love with her, attempting to kiss her when he must leave, and is finally caught by police officers, while he is waiting for her on a bridge. Left no choice but to defend himself, Otomo, in desperation, stabs the five officers, and one of them shoots him dead. 

 
 

 