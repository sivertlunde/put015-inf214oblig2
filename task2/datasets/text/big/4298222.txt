Madhouse (2004 film)
{{Infobox Film name           = Madhouse image          = Madhouse-poster.jpg image_size     = caption        =  director  William Butler producer       =  writer         =William Butler Aaron Strongoni narrator       =  starring       = Joshua Leonard Jordan Ladd Natasha Lyonne Lance Henriksen Mark Holton music          = Alberto Caruso cinematography = Viorel Sergovici editing        =  distributor    = Lions Gate Entertainment released       = December 20, 2004  (UK)  February 22, 2005  (USA)  runtime        = 91 minutes country        = United States language  English
|budget         =  gross          =  preceded_by    =  followed_by    = 
}}
 2004 horror William Butler and starring Joshua Leonard.

It was released directly to DVD on December 20, 2004 in the United Kingdom and on February 22, 2005 in the United States.

== Plot ==
Clark Stevens (Joshua Leonard), a psychiatric intern, has come to Cunningham Hall Mental Facility to train before he can graduate to medical school. Inside, he meets some of the patients (with one convincingly pretending to be a doctor working there) and is welcomed by nurse Betty. 

In Dr. Franks empty office, Clark examines his bookcase whilst waiting for Franks to arrive. He finds a book titled "Psychology and the Paranormal," which strikes his interest. He reads the first page, which implies connections between mental stability and the paranormal. Before he continues reading, Franks arrives. Clark and Frank talk about the facility and its patients, with Clark suggesting some improvements for the building, but Franks replies stern and bluntly. At this point, Dr. Morton and Dr. Douglas enter, wanting to meet Clark.

Clark now meets nurse Sara (Jordan Ladd) in the cafeteria, and she takes Clark on a tour around the facility. As they leave, mental patient Alice (Natasha Lyonne) sits watching a window, where she sees flashes of a demonic-looking boy. Sara continues the tour with Clark, but come across head nurse Hendricks, who talks to Sara privately.

Clark makes his way back to his room, but finds the boy from before in the hallway. Clark gives chase, and ends up at the Madhouse, where he finds no-one at the supervision desk and the door to Madhouse open. Clark enters to investigate, but finds nothing. However, Dr. Morton finds him and they talk about the mysterious boy.

Clark tells her that he returned to this place to make it better, and to get rid of everyone who didnt even try to help him.  Sara tries to convince Clark that he shouldnt kill her because he loves her.  Ben lowers his guard from this, and Sara attacks him, only to eventually be overpowered by Ben.  As Sara begs Clark not to kill her, he replies "Clark isnt here. In fact, he never existed" right before he brings the ax down on her and the screen goes black before it makes contact.

== Cast ==
{| class="wikitable"
|-
! Actor !! Role
|-
| Joshua Leonard || Clark Stevens
|-
| Jordan Ladd || Sara
|-
| Natasha Lyonne || Alice
|-
| Lance Henriksen || Dr. Franks
|-
| Mark Holton || Mr.Hansen
|}

== External links ==
* 
*  

 
 
 