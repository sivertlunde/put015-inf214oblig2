Talihina Sky
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Talihina Sky: The Story of Kings of Leon
| image          =
| image size     = 
| border         = 
| alt            = 
| caption        = Promotional poster
| director       = Stephen C. Mitchell
| producer       = Casey McGrath Joshua Levine
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Caleb Followill Jared Followill Nathan Followill Matthew Followill
| music          = 
| cinematography = 
| editing        = Paul Greenhouse
| studio         = 
| distributor    = Showtime Networks Sony Music Entertainment
| released       =  
| runtime        = 87

 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Talihina Sky: The Story of Kings of Leon is a 2011 rockumentary which follows the Kings of Leon throughout their journey from obscurity to fame to the future. The film was named after the hidden track on their Youth and Young Manhood album.

==Background==
 

==Cast==
; Kings of Leon:
* Caleb Followill as Himself
* Jared Followill as Himself
* Matthew Followill as Himself
* Nathan Followill as Himself

==Release== Athens International Film Festival in Greece, and in October 2011, it screened at Flanders International Film Festival Ghent in Belgium.

==Reception==

===Awards and nominations===
* 2011, received a Grammy Award nomination for Best Long Form Music Video.

==References==
 

==External links==
*   at the Internet Movie Database
*  

 

 
 
 
 
 
 
 


 