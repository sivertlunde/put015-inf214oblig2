La guerre du pétrole n'aura pas lieu
{{Infobox film
| name           = La guerre du pétrole naura pas lieu
| image          = La_guerre_du_petrole_n_aura_pas_lieu.jpg
| caption        = Film poster
| director       = Souheil Ben-Barka
| producer       = Latif Lahlou
| writer         = Michel Constantin Souheil Ben-Barka
| starring       = Claude Giraud
| music          = 
| cinematography = Girolamo La Rosa
| editing        = 
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = Morocco
| language       = French
| budget         = 
}}

La guerre du pétrole naura pas lieu is a 1975 Moroccan drama film directed by Souheil Ben-Barka. It was entered into the 9th Moscow International Film Festival.   

==Cast==
* Claude Giraud as Toumer
* Philippe Léotard as Padovani
* Sacha Pitoëff as Essaan
* Assan Ganouni as Hendas
* George Ardisson as Trudot (as Giorgio Ardisson)
* Claudio Gora as Stockell
* David Markham as Thomson
* Henryk Wodzinski as Kreis

==References==
 

==External links==
*  

 
 
 
 
 
 