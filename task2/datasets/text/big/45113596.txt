Monsoon (2014 film)
{{Infobox film
| name           = Monsoon
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Sturla Gunnarsson
| producer       = Ina Fichman
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = Andrew T. MacKay
| cinematography = Van Royko
| editing        = Nick Hector
| sound        = Brice Picard
| studio         = Intuitive Pictures, Point du Jour, and Monsoon Ontario   
| distributor    = KinoSmith (Canada)   and Point du Jour International (continental Europe, Japan, and Middle East) 
| released       =    
| runtime        = 108 minutes
| country        = Canada and France  
| language       = English/Hindi/Assamese/Malayalam/Marathi   
| budget         = 
| gross          =  
}}
 in India.
 4K format Red Epic cameras. 

The film was included in the list of "Canadas Top Ten" feature films of 2014, selected by a panel of filmmakers and industry professionals organized by TIFF.   Subsequently the film finished first in the audience balloting, of the features in "Canadas Top Ten".    

The film will reportedly begin its theatrical run in Toronto on February 27, 2015;  meanwhile Gunnarsson was quoted as being in discussions with an American distributor, following Monsoons United States premiere at the 2015 Palm Springs International Film Festival. 

==References==
 

==External links==
* 
* 
*  profile (published by Toronto International Film Festival|TIFF.net)
*  from The Globe and Mail
*  from CBC Radios Q (radio show)|Q
*  (article, 2014-09-01)

 
 
 
 
 
 
 
 
 
 
 


 