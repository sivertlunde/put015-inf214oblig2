The Flame and the Arrow
{{Infobox film
| name           = The Flame and the Arrow
| image          = Flame and the arrowposter.jpg
| caption        = Promotional poster
| director       = Jacques Tourneur Frank Ross
| writer         = Waldo Salt
| starring       = Burt Lancaster Virginia Mayo Nick Cravat
| music          = Max Steiner
| cinematography = Ernest Haller
| editing        = Alan Crosland Jr.
| distributor    = Warner Bros.
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $1.6 million 
| gross          = $2.9 million (US & Canada rentals)
}}
 Frank Ross from a screenplay by Waldo Salt. The music score was by Max Steiner and the cinematography by Ernest Haller. The film was shot in Technicolor.

==Plot== Frederick Barbarossa, hunting hawk. In revenge, the count takes Dardos son to his castle. Dardo is struck by an arrow while rescuing Rudi, so the boy allows himself to be recaptured in order to draw the soldiers away. 
 Robert Douglas) asks for Annes hand in marriage, but is rejected. Ulrich arrests de Granazia for not paying his taxes. After his rescue by Dardo, the marchese joins Dardos band of rebels. Dardo makes another attempt to free his son. Upon the advice of his uncle (Papa Bartoli, played by Francis Pierlot), Dardo obtains the help of Annes maid (one of Dardos many lovers) to sneak into Ulrichs castle along with his best friend Piccolo (Cravat), but they are unsuccessful. When they find themselves in Lady Annes apartment, Piccolo suggests they kidnap her instead. They take her to their secret hideout. She tries several times to escape, but Dardo is too crafty for her.

Dardo sends a message to the count, offering an exchange of prisoners, but Ulrich threatens to execute Bartoli unless Anne is released. Dardo and the others race to the village and rescue Bartoli. Then, Dardo learns from his aunt Nonna (Aline MacMahon) that five more prisoners have been taken to hang in Papas place. Dardo gives himself up to save the others and is hanged in front of his son. Ulrich takes the rest of the rebels prisoner, including the marchese.

The marchese informs Ulrich that the rebels are planning an attack the next day and that Dardo is alive (the executioner had been replaced by Dardos friend). As a reward for this betrayal, Ulrich agrees to the marcheses marriage to Anne. When she finds out their plans, she warns Nonna Bartoli, with Dardo and his men hiding around the corner.

They decide that they must attack at once. Piccolo comes up a plan for getting into the castle by posing as some of the acrobats providing entertainment. The ruse works. When they are ready, they remove their disguises and a battle ensues. During the melee, Anne warns Dardo that Ulrich has gone for his son. When Dardo catches up to Ulrich, he is in the company of the marchese. The count leaves Dardo and the marchese to fight. Though Dardo tries to persuade the marchese to stand aside, the marchese refuses and is killed.  

Afterwards, Dardo finds his wife dead, killed by a knife in the back. Then, he finds the count holding Rudi at sword-point.  Dardo finds a bow and, aiming carefully, kills Ulrich and frees his son. With the battle won, Dardo embraces Anne.

==Cast==
*Burt Lancaster as Dardo Bartoli
*Virginia Mayo as Anne de Hesse
*Nick Cravat as Piccolo Norman Lloyd as Apollo, the troubadour Robert Douglas as Marchese Alessandro de Granazia
*Robin Hughes as Skinner
*Victor Kilian as Apothecary Mazzoni
*Francis Pierlot as Papa Pietro Bartoli
*Aline MacMahon as Nonna Bartoli
*Frank Allenby as Count Ulrich, The Hawk
*Gordon Gebert as Rudi Bartoli
*Lynn Baggett as Francesca

==References==
 

==External links==
 
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 