Villain (1971 film)
{{Infobox film
| name = Villain
| image = Villain (1971 film).jpg
| caption = Meet Vic Dakin. Then wish you hadnt.
| director = Michael Tuchner
| producer = Jay Kanter Alan Ladd, Jr. Elliott Kastner (executive producer)
| writer = Dick Clement Ian La Frenais Al Lettieri (adaptation)
| based on = novel The Burden of Proof by James Barlow
| starring = Richard Burton Ian McShane T. P. McKenna Donald Sinden Nigel Davenport
| music = Jonathan Hodge
| cinematography = Christopher Challis
| editing = Ralph Sheldon
| studio = Anglo-EMI MGM (US)
| released = May 26, 1971
| runtime = 98 min.
| country = United Kingdom English
}}

Villain is a 1971 gangster film directed by Michael Tuchner and starring Richard Burton, Ian McShane, T. P. McKenna and Donald Sinden.

==Plot== East End gangster Vic Dakin has plans for an ambitious raid on the wages van of a plastics factory. This is a departure from Dakins usual modus operandi and the job is further complicated by his having to work with fellow gangster Frank Fletchers firm.

Essentially a standard story about a heist, there are intricate sub-plots depicting:
* Dakins sadistic nature
* Dakins relationship with Wolfie
* Wolfies bisexual relationship with Venetia and Dakin
* Dakins irritation at having to work with Frank Fletchers seemingly weak brother-in-law: Ed Lowis
* MP Gerald Draycott being blackmailed by Dakin (via Wolfie) to provide a cast-iron alibi
* Detectives Bob Matthews and Tom Binney pursuing Dakin and Lissner.
 Special Branch.

==Cast==
* Richard Burton as Vic Dakin
* Ian McShane as Wolfe Lissner
* Nigel Davenport as Bob Matthews
* Fiona Lewis as Venetia
* Joss Ackland as Edgar Lowis
* T. P. McKenna as Frank Fletcher
* Donald Sinden as Gerald Draycott
* Cathleen Nesbitt as Mrs Dakin
* Colin Welland as Tom Binney
* Tony Selby as Duncan
* Del Henney as Webb
* John Hallam as Terry
* James Cossins as Brown
* Anthony Sagar as Danny
* Clive Francis as Vivian
* Elizabeth Knight as Patti
* Shirley Cain as Mrs Matthews

==Production==
The film was written by an unusual combination of two well-known British comedy television writers, Dick Clement and Ian La Frenais and the American actor Al Lettieri, renowned for his tough-guy image in films such as The Godfather and The Getaway as well as for his real life associations with the New York Gambino Family.

==Reception==
According to   (in which Cathleen Nesbitt also played his mother), which had proved a huge bust at the box office, despite the talents of co-star Rex Harrison and director Stanley Donen. A gay love scene between Burton and co-star Ian McShane was cut from Villain, possibly as it was felt it wouldnt boost ticket sales, as cinema audiences already had not accepted Burton, one of the cinemas most notorious Don Juans, as a homosexual.

Alexander Walker, in his book about the 70s British film industry, National Heroes, says the film was "outstandingly successful at the British box office."  British exhibitors voted him the most popular star at the local box office in 1971, although Villain was not listed among the top ten most popular movies. 

Coincidentally, Burton was mentioned in James Barlows 1968 novel, The Burden Of Proof, upon which the film was based. In the book, the prosecutor asks a female witnesses if she "likes the actor Richard Burton". 

The film received bad reviews, and Burton—whose acting style was predicated upon the precise use of his mellifluous voice—was particularly savaged for his attempt at a Cockney accent.

The film coined a popular phrase used regularly and adapted accordingly of "Dont be a   all your life; take a day off!"

==Actor memories==
Ian McShane revealed recently in The Daily Mail, that he had mixed feelings about playing Richard Burtons bisexual lover. "After kissing me, hes going to beat the hell out of me and its that kind of relationship&nbsp;– rather hostile. It was very S&M. It wasnt shown in the film. He said to me, Im very glad youre doing this film. I said, So am I Richard. He said, You know why, dont you? I said, Why? He said, You remind me of Elizabeth. I guess that made the kissing easier." 

== Trivia ==
In 2009 Empire Magazine named it #2 in a poll of the 20 Greatest Gangster Movies Youve Never Seen* (*Probably)

The character of Vic Dakin was inspired by the real life gangster Ronnie Kray, who was jailed several years before production began, in 1967. Like Kray, the character Dakin is a London underworld boss, he is a homosexual, he is obsessed with caring for his mother and has a secret association with a member of Parliament.

==References==
 

==External links==
*  
*  
*   at BFI Screenonline
*   at cinemaretro.com
*   at Cinedelica.com

 
 

 
 
 
 
 
 
 
 
 
 
 