The Great Indian Butterfly
{{Infobox film
| name           = The Great Indian Butterfly
| image = The_Great_Indian_Butterfly.jpg
| director       = Sarthak Dasgupta
| producer       = Sanjay Gupta Parth Arora
| writer         = Sarthak Dasgupta
| narrator       = Barry John
| cinematography =Shanker Raman
| editing        =Vivek Shah Shan Md.
| distributor    =
| released       =  
| runtime        = 94 minutes
| country        = India
| language       = English
| budget         =
| gross          =
}}
The Great Indian Butterfly is a 2007 Bollywood film, written and directed by Sarthak Dasgupta. It is about finding  Its the story of a typical modern Indian couple and their journey to Goa in quest of a magical insect, and in turn, peace, love and happiness in life, which lies within ourselves. The medium of the film was English.   

==Plot==

A planned vacation to Goa goes awry for two stressed-out Mumbai-based employees, after Krish (Aamir Bashir) oversleeps, and as a result he and his wife, Meera (Sandhya Mridul) miss the plane. They, nevertheless, decide to make use of their time and drive there by car so that he can locate and view The Great Indian Butterfly. Last seen by the unknown Portuguese explorer Carodiguez, in a remote Karthikey valley located in erstwhile colonial Goa, the butterfly possesses a magical aura, granting immense happiness to the person who catches it.

In the journey that takes the couple from the smog filled, concrete jungle of the Megalopolis of Mumbai through the little discovered coastal landscapes of the western Sahyadris, to the sun soaked land of Goa, the couple lose more than what they want to rediscover. It becomes a passage, which seems to travel with a metaphor of its own and an insect as elusive as a fossil trapped in prehistoric resin.

Their journey will be acrimonious as Meera must also deal with work-related stress, loss of a promotion, as well as accuse her husband of not being intimate with her on one hand, and on the other remaining in close contact and having an affair with his ex-girlfriend, Liza (Koel Purie).

Will they find the Butterfly? Can they survive the journey? Can it cost them their lives? Will the Great Indian Butterfly wreak havoc on their souls? Will there be redemption? Or is it a futile hunt for an answer to their crumbling lives? Is happiness a rare insect? A simple tale in the complex miasma of a changing India.

==Production==

===Background===

===Development===

==Cast==
* Aamir Bashir as Krish Kumar
* Sandhya Mridul as Meera K. Kumar
* Koel Purie as Liza Barry John
* Gourov Dasgupta
* Shibani Kashyap as Herself

==Release==
The film was released on April 2, 2010.

==Reception==
The film didnt do well at the Box Office.   

The film received generally good reviews and was commended for the themes it boldly raised and honestly depicted. Both Sandhya and Bashir got praises for their top notch performances. The film also got kudos for peaceful and calm atmosphere of the film, as compared to loud dialogues and fights, mostly seen nowadays.   

==References==
 

==External links==
*  
*  
*   at Bollywood Hungama

 
 
 
 
 
 