Service de Luxe
{{Infobox film
| name = Service de Luxe
| image =
| image_size =
| caption =
| director = Rowland V. Lee
| producer = Edmund Grainger 
 | writer = Vera Caspary    Bruce Manning    Gertrude Purcell    Leonard Spigelgass    
| narrator =
| starring = Constance Bennett   Vincent Price   Charles Ruggles   Helen Broderick  Charles Henderson George Robinson
| editing = Ted J. Kent     
| studio = Universal Pictures
| distributor =   Universal Pictures
| released = 12 October 1938
| runtime = 85 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Service de Luxe is a 1938 American comedy film directed by Rowland V. Lee and starring Constance Bennett, Vincent Price and Charles Ruggles. 

==Cast==
* Constance Bennett as Helen Murphy  
* Vincent Price as Robert Wade 
* Charles Ruggles as Robinson 
* Helen Broderick as Pearl  
* Mischa Auer as Bibenko  
* Joy Hodges as Audrey  Frances Robinson as Secretary  
* Halliwell Hobbes as Butler  Raymond Parker as Bellhop  
* Frank Coghlan Jr. as Bellhop  
* Lawrence Grant as Voroshinsky   Nina Gilbert as Mrs. Devereaux 
* Crauford Kent as Mr. Devereaux  
* Lionel Belmore as Wade  
* Chester Clute as Bridegroom  Ben Hall as Yokel

==References==
 

==Bibliography==
* Kellow, Brian. The Bennetts: An Acting Family. University Press of Kentucky, 2004.

==External links==
* 

 

 
 
 
 
 
 
 

 