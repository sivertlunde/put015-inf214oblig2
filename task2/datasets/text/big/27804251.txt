The Hunger (1986 film)
{{Infobox film
| name           = The Hunger (Al-Goa) (الجوع)
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Aly Badrakhan
| producer       = 
| writer         = Esam Ali Aly Badrakhan Naguib Mahfouz (novel)
| narrator       = 
| starring       = Soad Hosny Mahmoud Abdel Aziz Yousra
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1986
| run-time        = 151 min.
| country        = Egypt
| language       = Arabic
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

The Hunger (Al-Goa, الجوع) is a 1986 Egyptian drama/romance movie, starring Souad Hosni, Yousra and Mahmoud Abdel Aziz.

==Plot==

The movie, based upon the novel of the same name by Naguib Mahfouz, examines the social conditions of Cairenes during the first decade of the twentieth century.  In doing so, both the movie and novel deal extensively with the themes of poverty and death.

==Cast==
* Souad Hosni as Zubeida
* Yousra
* Mahmoud Abdel Aziz
* Abdel Aziz Makhyoun
* Hanan Soliman
* Sana’a Younis

==External links==
*  

 
 
 

 