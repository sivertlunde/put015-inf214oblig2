Léto s kovbojem
{{Infobox Film
| name           = Léto s kovbojem
| image          = Léto s kovbojem film poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Ivo Novák
| producer       = Tomáš Baloun (assistant)
| writer         = Jaromíra Kolárová
| narrator       = 
| starring       = Daniela Kolářová Jaromír Hanzlík Oldřich Vízner
| music          = Petr Hapka
| cinematography = Rudolf Milic
| editing        = 
| distributor    = 
| released       = 1976
| runtime        = 90 min.
| country        = Czechoslovakia Czech
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Léto s kovbojem, Summer With a Cowboy in English, is a 1976 Czechoslovak film directed by Ivo Novák. It starred Daniela Kolářová, Jaromír Hanzlík and Oldřich Vízner. 

== Casting ==
* Daniela Kolářová — Doubravka
* Jaromír Hanzlík — Honza
* Oldřich Vízner — Boba
* Jiří Pleskot — Father
* Libuše Švormová — Mother
* Dana Medřická — Doubravkas grandmother
* Marie Rosůlková — Great-grandmother

== References ==
 

 
 
 
 
 