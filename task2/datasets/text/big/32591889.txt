List of horror films of 1997
 
 
A list of horror films released in 1997 in film|1997.

{| class="wikitable sortable" 1997
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | Alien Resurrection
| Jean-Pierre Jeunet || Sigourney Weaver, Winona Ryder, Ron Perlman ||   ||  
|-
!   | An American Werewolf in Paris
| Anthony Waller || Tom Everett Scott, Julie Delpy, Vince Vieluf ||     ||  
|-
!   | Anaconda (film)|Anaconda
| Luis Llosa || Jennifer Lopez, Ice Cube, Jon Voight ||   ||  
|-
!   | Bleeders (film)|Bleeders
| Peter Svatek || Rutger Hauer, Roy Dupuis ||    ||  
|-
!   | The Bloody Ape
| Keith Crocker || George Reis, Paul Richici, Chris Hoskins, Larry Koster, Arlene Hansen ||   ||  
|-
!   | Breeders
| Paul Matthews || Samantha Womack, Clifton Lloyd Bryan, Kadamba Simmons ||   ||  
|-
!   | Campfire Tales
| Matt Cooper, David Semel, Martin Kunert || Chris Masterson, Christine Taylor, Jay R. Ferguson ||   ||  
|- The Creeps
| Charles Band || Justin Lauer, Andrea Harper, Jon Simanton ||   ||  
|- The Devils Advocate
| Taylor Hackford || Keanu Reeves, Al Pacino, Charlize Theron ||   ||  
|-
!   | Eko eko azaraku III
| Katsuhito Ueno || Hinako Saeki, Cho Bang-ho, Chika Fujimura ||   ||  
|- Event Horizon
| Paul W. S. Anderson || Laurence Fishburne, Sam Neill, Kathleen Quinlan ||   ||  
|-
!   | Gut-Pile
| Jerry OSullivan || Sasha Graham ||   ||  
|- House of Frankenstein
| Peter Werner || Andrew Pasdar, Teri Polo, Jorja Fox ||   ||  
|-
!   | I Know What You Did Last Summer
| Jim Gillespie || Jennifer Love Hewitt, Sarah Michelle Gellar, Ryan Phillippe ||   ||  
|-
!   | Kokkuri-san (film)|Kokkuri-san
| Takahisa Zeze || Ayumi Yamatsu, Rika Furukawa, Moe Ishikawa ||   ||
|-
!   |  
| Brian Trenchard-Smith || Warwick Davis, Rebekah Carlton ||   || Direct-to-video
|-
!   | Mimic (film)|Mimic
| Guillermo del Toro || Mira Sorvino, Jeremy Northam, Josh Brolin ||   ||  
|-
!   | The Minion
| Jean-Marc Piché || Dolph Lundgren, Françoise Robertson, David Nerman ||    ||
|- The Night Flier
| Mark Pavia || Miguel Ferrer, Julie Entwisle, Dan Monahan ||   || Television film
|-
!   | Night of the Demons III
| Jimmy Kaufman || Joel Gordon, Richard Jutras, Vlasta Vrána ||    ||  
|-
!   | Nightwatch (1997 film)|Nightwatch
| Ole Bornedal || Ewan McGregor, Nick Nolte, Josh Brolin ||   || Film remake
|-
!   |  
| Olaf Ittenbach || Fidelis Atuma, Christopher Stacey, Andre Stryi ||   ||  
|-
!   | Quicksilver Highway
| Mick Garris || Christopher Lloyd, Matt Frewer ||   || Television film 
|- The Relic
| Peter Hyams || Penelope Ann Miller, Tom Sizemore, Linda Hunt ||   ||  
|-
!   | Scream 2
| Wes Craven || David Arquette, Neve Campbell, Courteney Cox ||   ||  
|-
!   | Shopping for Fangs
| Quentin Lee, Justin Lin || Jeanne Chinn, Clint Jung, Peggy Ahn ||    ||  
|-
!   | Skeletons
| David DeCoteau || Christopher Plummer, Dee Wallace, Ron Silver ||   ||
|-
!   | Troublesome Night
| Steve Cheng, Victor Tam, Herman Yau || Simon Lui, Louis Koo, Allen Ting, Ada Choi, Teresa Mak, Law Lan ||   ||
|-
!   | Troublesome Night 2
| Herman Yau || Simon Lui, Louis Koo, Allen Ting, Chin Kar Lok ||   ||
|-
!   | Trucks (film)|Trucks
| Chris Thomson || Timothy Busfield, Brenda Bakke, Brendan Fletcher ||    ||  
|-
!   | The Ugly
| Scott Reynolds || Paolo Rotondo, Rebecca Hobbs, Jennifer Ward-Lealand ||   ||  
|- Uncle Sam
| William Lustig || Timothy Bottoms, Robert Forster, Bo Hopkins ||   ||  
|-
!   | The Wax Mask
|  	Sergio Stivaletti || Romina Mondello, Robert Hossein ||   ||    
|-
!   | Wishmaster (film)|Wishmaster
| Robert Kurtzman || Tammy Lauren, Andrew Divoff, Robert Englund ||   ||  
|}

==References==
 

 
 
 

 
 
 
 