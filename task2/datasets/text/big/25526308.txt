Bad Reputation (film)
{{Infobox film
| name           = Bad Reputation
| image          = Bad Reputation (film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by Maverick Entertainment Group
| film name      = 
| director       = Jim Hemphill
| producer       = T.W. Porrill   Christopher Landers
| writer         = Jim Hemphill
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Chris Basler   Danielle Noble   Mark Kunzman   Kristina Conzen   Dakota Ferreiro   Jerad Anderson   Angelique Hennessey
| music          = John LeBec   Eric Choronzy
| cinematography = Forrest Allison
| editing        = Gordon Stuart
| studio         = Winning Edge Partners
| distributor    = Maverick Entertainment Group
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Bad Reputation is a 2005 horror film written and directed by Jim Hemphill.

== Plot ==
 introverted and jock Aaron outcast who emotionally abusive mother.

Rumors of Michelles supposed promiscuity spread, and she is tormented at school, where she receives no aid from the facilitys apathetic School counselor|counselor, who Passive-aggressive behavior|passive-aggressively blames her for the bullying she is experiencing. Michelle snaps, and begins acting and dressing provocatively, seducing Steve one day after school. Michelle has Steve drive to a secluded area, where she stabs and castrates him, then stages the scene to make it appear that he was gay, and the victim of a homophobic hate crime. Later, Michelle lures Jake to her apartment, where she ties him up (under the pretense of engaging in kinky sex) and tortures, bludgeons, and dismembers him.

The next night, Aaron hosts a Halloween party at his house, which Michelle sneaks into, dressed as Jason Voorhees. Michelle slips roofies into all of the drinks, then goes upstairs, where Aaron is cheating on Debbie with Heather. When Aaron leaves the bedroom, Michelle decapitates Heather with a machete, and takes her costume, using it to get close to Debbie, who she drowns in a filth-filled toilet. Michelle then seduces Aaron, leads him to a bedroom, confesses to murdering his friends, and kills him by biting his penis off, and slitting his throat. Wendy, who had been outside, returns to the party, where everyone has passed out due to the drugs Michelle had given them. Michelle attacks Wendy, screaming, "Yeah, you tried to stop them, until you thought it might threaten your spot on the pep squad! God, youre the worst of all of them! You knew it was wrong, and you didnt do a goddamn thing to stop it!" In the struggle that ensues, Wendy stabs Michelle, who dies with the sobbing and remorseful Wendy holding her hand.

== Cast ==

* Angelique Hennessy as Michelle Rosen
* Jerad Anderson as Aaron Cussler
* Danielle Noble as Wendy
* Kristina Conzen as Heather
* Dakota Ferreiro as Debbie Mitchell
* Mark Kunzman as Jake Stife
* Chris Basler as Steve
* T.W. Porrill as Counselor Wiederhorn
* Elizabeth Kirven as Detective Stephanie Rothman
* Sean A. Mulvihill as Scott Marks
* Mimi Marie as Ms. Rosen
* Jennifer Holloway as Carol Johnson
* Jessica Stamen as Alana Maxwell
* Jeff Kueppers as Birdman
* John Knapp as Baxter Wolfe
* Jim Hemphill as Mean Kid on Answering Machine

== Reception ==
 Arrow in slasher movie Hemphill doesnt dumb down the story. Not a bad entry into the horror/slasher genre".  The film was also praised by DW Bostaph, Jr. of Dread Central, who gave it a 3½ out 5.  A 2/5 was awarded by J.R. McNamara of Digital Retribution, who criticized the poor production values, pacing problems, and awkward dialogue, opining, "I dont think that anyone should avoid this film, but dont strain yourself seeking it out either". 

== References ==

 

== External links ==

*  
*   at Maverick Entertainment Group

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 