Cavemen (film)
 
{{Infobox film
| name           = Cavemen
| image          = Caveman 2013 film poster.jpg 
| alt            = 
| caption        = 
| director       = Herschel Faber
| producer       = Herschel Faber
| writer         = 
| starring       =   
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $5,948    
}}
Cavemen is a 2013 American comedy film about a young man in Los Angeles who feels the emptiness of a life of dissipation and seeks a genuine relationship, which he discovers is more difficult to do than he thought. The movie was panned by critics, but was popular with audiences.  Cavemen made its world premiere at the 2013 Austin Film Festival where the screenplay had been a competition finalist several years earlier. 

== Cast ==
* Skylar Astin  as	Dean
* Camilla Belle  as	Tess
* Chad Michael Murray  as   Jay
* Dayo Okeniyi  as   Andre
* Alexis Knapp  as   Kat
* Kenny Wormald  as   Pete
* Jason Patric as Jack Bartlett
* Chasty Ballesteros  as	Monique
* Fernanda Romero	as   Rosa
* Zuleyka Silver	as   Alicia

== References ==
 

==External links==
 
*  

 
 
 


 