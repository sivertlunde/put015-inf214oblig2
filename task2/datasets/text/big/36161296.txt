Maa (1976 film)
{{Infobox film
| name           = Maa
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = M.A. Thirumugham	 
| producer       = Sandow M.M.A. Chinnappa Devar	
| writer         = 
| screenplay     = 
| story          = Sandow M.M.A. Chinnappa Devar	 
| based on       =  
| narrator       = 
| starring       = Dharmendra Hema Malini
| music          = Laxmikant-Pyarelal
| cinematography = V. Ramamurthy	
| editing        = M.A. Thirumugham M.G. Balu Rao 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by M.A. Thirumugham and produced by Sandow M.M.A. Chinnappa Devar. It stars Dharmendra and Hema Malini in pivotal roles.

==Cast==
* Dharmendra...Vijay
* Hema Malini...Nimmi Das
* Nirupa Roy...Vijays Mother
* Ranjeet...Balraj
* Padma Khanna...	Balrajs Girlfriend
* Paintal (comedian)|Paintal...Vijays employee
* Brahmachari...Vijays employee 

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Maa Tujhe Dhoondon Kahan"
| Mohammed Rafi
|-
| 2
| "Aisi Koi Baat Jo"
| Kishore Kumar, Lata Mangeshkar
|-
| 3
| "Main Jangal Ka Raja"
| Mohammed Rafi, Lata Mangeshkar
|-
| 4
| "Hatari Mein Shikari"
| Mohammed Rafi
|-
| 5
| "Gaaoonga Naachoonga"
| Mohammed Rafi
|-
| 6
| "Shehron Se Door"
| Lata Mangeshkar
|-
| 7
| "Aao Ke Jaao Ke"
| Lata Mangeshkar
|-
| 8
| "Pyase Dil Ki Pyaas"
| Asha Bhosle
|}

==External links==
* 

 
 
 
 

 