The Lawless Frontier
{{Infobox Film
| name           = The Lawless Frontier
| image          = TheLawlessFrontier.jpg
| caption        = Theatrical poster
| director       = Robert N. Bradbury
| starring       = {{plainlist|
*John Wayne Shelia Terry
*George "Gabby" Hayes
*Earl Dwine
}}
| distributor   = Monogram Pictures
| released       =  
| runtime        = 59 minutes
| language       = English
| country        = United States
}} Western film Sheila Terry, George "Gabby" Hayes, and Earl Dwine.

==Plot==
 
The movie opens with a family shooting from the window of their home, as they are robbed of their cattle.  John Tobin (John Wayne) arrives home later that night and discovers his family killed and their cattle missing.

John Wayne (Tobin) sets out in search Pandro Zanti (Earl Dwire) the local bandito, for all appearances a stereotyped Mexican, though the script is twice careful to tell us he is half white & half Apache & only pretends to be Mexican. 
 Sheila Terry) for his lusty needs. This is an unusual villainous trait for 1930s matinee westerns, aimed at young audiences who expected villains to be claim jumpers & killers, not rapists. Ruby & her daddy (George "Gabby" Hayes) hightail it out of there before she falls victim of the killer, escaping by a clever ruse   that almost gets Ruby drowned in a river.

They run into Tobin tracking his familys killers.  Tobin then saves the sack containing Ruby. Ruby, Dusty & Tobin thereafter join forces.

Sheriff Luke Williams (Jack Rockwell) not only immediately takes credit for capturing Zanti when Tobin brings him in, but also arrests Tobin for the murder of Rubys father Dusty, who took a knife in the back and collapsed. Later it is discovered Dusty only received a superficial wound and was knocked out.

Sheriff Williams however makes a series of mistakes including handcuffing a bad guys boot to a bed, so that all he has to do is take off his boot to escape, especially when Tobin made of point of telling Williams that Zanti can probably get loose. 

A chase ensues, across the desert partly on foot, with Tobin after Zanti. The villain drinks from a desert watering hole, but in his fatigued state he does not see the sign marked "Poison. Do not drink," He dies soon after. 

With the chief villain now out of the picture, were treated to plenty more hard ridin and shootin, as Ruby and Tobin flee from Zantis gang. When the happy ending finally arrives, Tobin becomes the new sheriff, replacing the inept Luke Williams.

==Cast==
* John Wayne as John Tobin Sheila Terry as Ruby
* Jack Rockwell as Sheriff Luke Williams
* George "Gabby" Hayes as Dusty (as George Hayes)
* Jay Wilsey as 2nd Zanti Henchman (as Buffalo Bill Jr.)
* Yakima Canutt as Joe, Zantis Henchman
* Gordon De Main as Deputy Miller (as Bud Wood)
* Earl Dwire as Pandro Zanti, alias Don Yorba

==See also==
* John Wayne filmography
* List of American films of 1934

==External links==
*  
*  


 
 
 
 
 
 
 
 