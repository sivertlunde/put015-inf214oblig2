The Leader, His Driver and the Driver's Wife
 
 
{{Infobox film
| name           = The Leader, His Driver and the Drivers Wife
| image          = 
| image size     =
| alt            =
| caption        =
| director       = Nick Broomfield
| producer       = Nick Broomfield Rieta Oord
| writer         =
| screenplay     =
| story          =
| narrator       = Nick Broomfield
| starring       = Nick Broomfield Eugène TerreBlanche JP Meyer Rieta Oord
| music          =
| cinematography = Barry Ackroyd
| editing        = John Mister Max Milligan (Assistant Editor)
| studio         =
| distributor    =
| released       =  
| runtime        = 85 Minutes
| country        = South Africa Film UK
| language       = English Afrikaans
| budget         =
| gross          =
}}

The Leader, His Driver and the Drivers Wife is a 1991 British feature-length documentary film set during the final days of the apartheid regime in South Africa, particularly centring on Eugène TerreBlanche, founder and leader of the far-right Afrikaner Weerstandsbeweging. The film was directed by Nick Broomfield and released in 1991. It received an average of 2.3 million viewers during its screening on Channel 4.  A year later it was the subject of legal action brought by the journalist, Jani Allan, in what has been described as "the libel case of the summer".  In 2006, Broomfield released a follow-up, His Big White Self.

== Background ==
Throughout the film, Broomfield attempts to set up an interview with TerreBlanche, who stubbornly breaks all of the plans he makes with him.

For the majority of the film where Broomfield is unable to get an interview with the Leader himself, his attention is drawn to the driver and his wife (JP and Anita Meyer), hence the title (which alludes to the title of Peter Greenaways 1989 film The Cook, the Thief, His Wife & Her Lover).

Broomfield also spent time with a Town Councillor and diamond mine owner named Johann and his friend Anton. Broomfield had planned to interview Boervolk leader Piet Rudolph but when on the outskirts of Pretoria a news broadcast informed them that hed been arrested, Rudolph was a fugitive wanted in connection with the theft of some arms from an SA Defence Force base located in Pretoria. After his arrest in Pretoria, Mr Rudolph was dubbed as the South African Bobby Sands in reference to his proclamation that he would go on a hunger strike to promote his (and the AWBs) cause for a white homeland. The film also makes light of the ongoing saga of trying to get an interview with the leader, something which Broomfield eventually manages to do although hes only able to ask one question after TerreBlanche takes particular offence when Broomfield and his crew turn up 5 minutes late for the interview. The film ends with Broomfield and his crew at an AWB rally where a crowd of five thousand were expected but in reality not even half that number are present. The credits roll soon after TerreBlanche again breaks into fits of rage, citing supposed security violations committed by Broomfields camera crew.

The documentary was released in the UK as a DVD boxset, together with His Big White Self, in April 2006.

== Main characters ==

*Eugene TerreBlanche.

*J.P Meyer. JP is the leaders personal driver and a prominent member of the AWB party. Although the film was intended to focus largely on the leader, much of Nick and his crews time is spent with JP and his wife. The film encounters JP shortly after his release from prison for his alleged involvement with white terrorists, notably his connection to Piet Rudolph. During much of the film, JPs personal nature largely displays elements of prejudice against Black people. When asked by Broomfield if he ever felt sorry for Nelson Mandela when he was incarcerated on Robben Island, he simply replies that he doesnt feel sorry "for that bloody Kaffir (racial term)|kaffir" (a racist term for a black person). He even proclaims himself to be a racist during the conversation. In spite of this, the film also follows JPs growing disillusionment with the AWB and their struggle for a white homeland, particularly after Piet Rudolph ends his hunger strike and calls on people to give up their weapons. Towards the end of the film JP leaves his post as driver to the leader and it is later revealed at the films conclusion that he left the AWB entirely and opened up a small electrical business.

*Anita Meyer. Anita is JPs long-suffering wife. Unlike most of the films other characters, she clearly has a dislike for the leader. During a conversation with Broomfield she sees him as a domineering figure and tends not to speak of him in high regard. Anita works as a nurse and her chief concerns, according to Nick, are the distribution of condoms and the sterilisation of women. Like JP, she tends not to be very liberal in her views on black people and can be seen at an AWB rally smiling when a child proclaims that hell hit any black child who attends his white school. She is also filmed with her pet cat which is named kaffir cat. The reason being, in her own words, is that the cat is black and as "blacks are called kaffirs" it has seemingly been appropriately named. Broomfield would later to go on to dub Anita of "kaffir cat fame" in his follow up film in reference to the episode.

*Johann and Anton. Johann is a town councillor in Ventersdorp and also owns a diamond mine and tractor business. He is often seen throughout the film with his friend Anton. Both express prejudiced views on black people including the belief that blacks should be forbidden from having sexual relations with whites, concluding that it will lead to the spread of AIDS causing the depletion of the white population in South Africa. Johann is also seen at the towns local swimming pool claiming it to be for whites only. During a particularly comical scene, Johann and Anton attempt to tell Broomfields assistant Rita a joke which, despite repeated attempts, she doesnt understand most likely due to their limited knowledge of English grammar. Neither reappear in Broomfields sequel. It is also never revealed whether or not either are members of the AWB although their views suggest at the very least they would have AWB sympathies.

==Libel suit==
In 1992, the former columnist Jani Allan sued Channel 4, the British broadcaster, for libel,  claiming that in the documentary  The Leader, His Driver and the Drivers Wife by Nick Broomfield she was presented as a "woman of easy virtue".  Amidst a montage of photographs from Allans earlier days as a photographic model and Sunday Times quotes; Broomfield claimed that Jani Allan had had an affair with TerreBlanche. The documentary-maker and his crew were following the AWB and its activities. The significance of the case led to its inclusion in the 1992 annual edition of Whitakers Almanack. {{cite book|first=Hilary|last=Marsden|title=Whitakers Almanack1992
|publisher=J Whitaker & Sons }} 

During the trial, Channel 4 denied the claim that they had suggested Allan had an affair with TerreBlanche. Prior to the case, Allan had been awarded £40, 000 in out-of-court settlements from the Evening Standard and Options magazine over suggestive remarks made about the nature of Allans association with TerreBlanche. 

Allan was represented by Peter Carter-Ruck in the case and Channel 4 was represented by QC George Carman.  Carman described the case as rare in that it had "international, social, political and cultural implications." 

The case sparked intense media interest in both Britain and South Africa, with several court transcripts appearing in the press Allan famously told Carman
"Whatever award is given for libel, being cross-examined by you would not make it enough money."  Several character witnesses were flown in from South Africa. {{cite book|first=Nadine|last=Dreyer|title=A Century of Sundays: 100 Years of Breaking News in the Sunday Times, 1906–2006
|publisher=Zebra}} 
 Charles Gray dismissed Shaws "wildly unlikely" testimony and stressed the physical impossibility of her claim. He continued to express that her field of vision through the keyhole would not be sufficient to support her claim. 

On day 2, Allans 1984 notebook was mysteriously delivered to Carmens counsel and used against her. This was investigated by the police, according to reports a "one-time friend" had taken the notebook from the home where Allan stayed with an English couple in 1989.   The Independent. 9 August 1992 

Allans former husband Gordon Schachat provided evidence supporting claims Allan had made about sex  and insisted she was neither an extreme right-winger or anti-semitic.   The Independent. 28 July 1992 

On day 11 of the case, Anthony Travers, a former British representative of the AWB and spectator of the court was stabbed. A court usher received a call saying Peter Carter-Ruck, Ms Allans solicitor, had been stabbed. This stemmed from a message by Travers who was lying in an alleyway he said to a passer-by tell Carter-Ruck Ive been stabbed. It quickly spread that Carter-Ruck had been stabbed, followed by speculation that he was the intended victim.  

During the trial, Jani Allans London flat was burgled. She said that she received a death threat on a telephone call in the court ushers offices. The hotel room of a Channel 4 producer, Stevie Godson was also ransacked. 

Allan eventually lost the case on 5 August 1992. Although the judge found that Channel 4s allegations had not defamed Allan, he did not rule on whether or not there had been an affair.    Although reports emerged that Allan was considering an appeal and TerreBlanche also expressed the possibility that he may sue the broadcaster for libel.  

Following the verdict, Allan reiterated her stance "I am not, nor have I ever been, involved with TerreBlanche". 

Soon after, several publications speculated about political forces at play during the case. The Independent published details of what it called "dirty tricks" used during the trial. Allan suggested that pro-government forces in South Africa wanted her to lose the case so that TerreBlanche would be irreparably damaged in the eyes of his God fearing Calvinist followers. Another interpretation is that the AWB wanted to steal a manuscript of a book she was writing about the organisation. The AWB countered these claims, although Travers described the book as "dynamite."  The South African business newspaper Financial Mail published a lead story on 6 August detailing "The theory" that F.W. de Klerk had orchestrated the libel case to discredit TerreBlanche and the far right movement in South Africa. 

In 1995, during an interview with SABC, Allan accused witnesses in the case of being paid to lie. 

In a 2002 BBC film Get Carman: the trials of George Carman QC, Allans case was dramatised   together with a number of other high-profile Carmen cases.

The libel suit is mentioned amidst a montage of photos and camera footage of Jani Allan and reporters outside the London court in 1992, in the 2006 Nick Broomfield sequel His Big White Self, a sequel to The Leader, His Driver and the Drivers Wife, the documentary that spawned the libel suit.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 