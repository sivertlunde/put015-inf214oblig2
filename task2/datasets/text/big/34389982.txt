Sweet Degeneration
{{Infobox film
| name           = Sweet Degeneration
| image          =
| caption        =
| director       = Lin Cheng-sheng
| producer       = Li-Kong Hsu
| writer         = Lin Cheng-sheng
| starring       = Pen-yu Chang
| music          =
| cinematography = Cheng-hui Tsai
| editing        = 
| distributor    =
| released       =  
| runtime        = 118 minutes
| country        = Taiwan Taiwanese
| budget         =
}}
Sweet Degeneration ( ) is a 1997 Taiwanese drama film written and directed by Lin Cheng-sheng. It was entered into the 48th Berlin International Film Festival.   

==Cast==
* Pen-yu Chang as Mei-li
* Shiang-chyi Chen as Ju-feng
* Shih-huang Chen as Father
* Leon Dai as Ah-hai
* Chao-yin Hsiao as Hsiao-lien
* Tze-min Hsiao as Mei-lis friend
* Kang-sheng Lee as Chun-sheng
* Su-feng Lee as Motel chef
* Chih-long Lin as Ah-ching
* Li-jung Lu as Prostitute
* Ming-Ching Lu as Ah-biao
* Mei-fong Yen as Motel chef

==References==
 

==External links==
* 

 
 
 
 
 
 
 