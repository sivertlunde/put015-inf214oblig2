Sous les jupes des filles
{{Infobox film
| name           = Sous les jupes des filles
| image          = Sous les jupes des filles poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Audrey Dana
| producer       = Olivier Delbosc   Marc Missonnier 
| writer         = Audrey Dana   Raphaëlle Desplechin   Murielle Magellan 	 
| starring       = Isabelle Adjani   Alice Belaïdi   Laetitia Casta   Audrey Dana   Julie Ferrier  Audrey Fleurot   Marina Hands Géraldine Nakache Vanessa Paradis Alice Taglioni Sylvie Testud
| music          = Imany
| cinematography = Giovanni Fiore Coltellacci 
| editing        = Ismael Gomez III   Julien Leloup  Wild Bunch   M6 Films
| distributor    = Wild Bunch
| released       =  
| runtime        = 116 minutes
| country        = France 
| language       = French
| budget         = 
| gross          = $10,123,950  
}}

Sous les jupes des filles ( ) is a 2014 French comedy drama film and the directorial debut of Audrey Dana. The film tells the stories of eleven women in Paris and features an ensemble cast including Isabelle Adjani, Alice Belaïdi, Laetitia Casta, Audrey Dana, Julie Ferrier, Audrey Fleurot, Marina Hands, Géraldine Nakache, Vanessa Paradis, Alice Taglioni and Sylvie Testud.  The French title directly translated is "Under the Skirts of Girls". 

== Cast ==
* Isabelle Adjani as Lili 
* Alice Belaïdi as Adeline
* Laetitia Casta as Agathe
* Audrey Dana as Jo
* Julie Ferrier as Fanny
* Audrey Fleurot as Sophie
* Marina Hands as Inès
* Géraldine Nakache as Ysis
* Vanessa Paradis as Rose
* Alice Taglioni as Marie
* Sylvie Testud as Sam

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 

 
 