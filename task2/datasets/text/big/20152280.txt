The Skin
 
{{Infobox Film
| name           = The Skin
| image          = The Skin poster.jpg
| image size     = 
| caption        = Theatrical release poster
| director       = Liliana Cavani Renzo Rossellini
| writer         = Curzio Malaparte Robert Katz Liliana Cavani Catherine Breillat
| narrator       = 
| starring       = Marcello Mastroianni
| music          = 
| cinematography = Armando Nannuzzi
| editing        = Ruggero Mastroianni
| distributor    = 
| released       = 27 August 1981
| runtime        = 131 minutes
| country        = Italy
| language       = Italian
| budget         = 
| preceded by    = 
| followed by    = 
}}
The Skin ( ) is a 1981 Italian war film directed by Liliana Cavani and starring Marcello Mastroianni, Ken Marshall, Claudia Cardinale and Burt Lancaster.    It was entered into the 1981 Cannes Film Festival.   

==Cast==
* Marcello Mastroianni - Curzio Malaparte Mark Clark
* Claudia Cardinale - Principessa Consuelo Caracciolo
* Ken Marshall - Jimmy Wren
* Alexandra King - Deborah Wyatt
* Carlo Giuffrè - Eduardo Mazzullo
* Yann Babilée - Jean-Louis
* Jeanne Valérie - Principessa a Capri
* Liliana Tari - Maria Concetta
* Peppe Barra - Sarto (as Giuseppe Barra)
* Cristina Donadio - Amica di Anna
* Rosaria della Femmina - Amante di Jimmy (as Maria Rosaria Della Femmina)
* Jacques Sernas - Gen. Guillaume
* Gianni Abbate
* Anna Maria Ackermann

==References==
 

==External links==
* 
*   Detailed information on the film and its director.

 

 
 
 
 
 
 
 
 
 

 