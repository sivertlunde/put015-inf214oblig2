The Case of the Howling Dog
 
{{Infobox film
| name           = The Case of the Howling Dog
| image          =
| image_size     =
| caption        =
| director       = Alan Crosland
| producer       =
| writer         = Erle Stanley Gardner (story) Ben Markson
| narrator       =
| starring       = Warren William Mary Astor
| music          = Bernhard Kaun William Rees
| editing        = James Gibbon
| studio         = First National Pictures
| distributor    = Warner Bros. Pictures
| released       =   
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
}}
The Case of the Howling Dog is a 1934 American mystery film directed by Alan Crosland, based on the novel of the same name by Erle Stanley Gardner. The film stars Warren William and Mary Astor. This was the first in a series of four films in which William played Perry Mason.

==Plot== police dog Los Angeles retainer fee, beneficiary to Foleys actual wife. The fee paid by Cartwright obligates Perry to legally and morally represent the real Mrs. Foley to the best of his ability.
 Ventura and signed by Evelyn is sent to Foley asking him to stop his actions.
 Santa Barbara with Foley and his wife Bessie. Lucy was Foleys private secretary then, unbeknownst to Evelyn. One of Perrys men is assigned to watch Foleys house and sees Lucy drive away with an unknown man. A cab arrives with a woman in black. When Foley shows annoyance that she "found him", she tells him that she "wants justice" and he releases the dog to attack her.  Two shots are fired, killing the dog and Foley, followed by the slamming of the garden door, and the woman flees. Perry arrives for a meeting with Foley and discovers the bodies. He immediately tracks down the cab driver at his cab stand, learning that a perfumed handkerchief left in the cab links "Bessie" to the murder scene, and then finds the woman, who is the actual wife and his client, in a hotel under an assumed name. He sends his secretary, Della Street, to impersonate Bessie and claim the handkerchief before the cabbie turns it in. Bessie denies killing her husband. Perry warns her that she is going to be arrested for Foleys murder and orders her to say nothing to the police. Later, acting on a hunch when none of the handwriting samples of the three women gathered by his operatives matches the note and the handwritten copy of the telegram, Perry devises a ruse to obtain a page from Lucys diary of the day after the Cartwrights disappeared.

During the trial, Perry discredits the cab drivers identification of his passenger when he demonstrates that he misidentified Della as Bessie. During his cross-examination of Lucy, Perry has the trial shifted to the scene of the crime, shows that the dog was devoted to all three women, and proves that Lucy was Foleys lover and is Ambidexterity|ambidextrous, writing the note, the telegram, and the diary page with her left hand. Just then, workers excavating the foundation of the garage addition discover the bodies of Cartwright and Evelyn, murdered by Foley. Bessie is acquitted after Perry in closing arguments states that because the dog loved her, he would never have attacked Bessie and been killed, destroying the prosecutions only other link of Bessie to the crime. After the trial, Perry presents Bessie with a dog that looks just like the dead animal, and the dog delightedly greets Bessie. Perry states that when the howling suddenly stopped, he searched kennels in the area and found one where a man matching Foleys description exchanged the dog for a lookalike. He gives Bessie the dog and orders her not to tell anyone what really happened. Perry later tells Della that he is sure that whatever Bessie did was in self-defense.

==Cast==
* Warren William as Perry Mason
* Mary Astor as Bessie Foley
* Gordon Westcott as Arthur Cartwright
* Allen Jenkins as Detective Sergeant Holcomb Grant Mitchell as District Attorney Claude Drumm
* Helen Trenholme as Della Street
* Helen Lowell as Elizabeth Walker, Cartwrights Housekeeper
* Dorothy Tree as Lucy Benton Harry Tyler as Sam Martin, Taxi Driver
* Arthur Aylesworth as Sheriff Bill Pemberton Russell Hicks as Clinton Foley
* Frank Reicher as Dr. Carl Cooper
* Addison Richards as Judge Markham
* James P. Burtis as George Dobbs (as James Burtis)
* Eddie Shubert as Edgar Ed Wheeler
* Harry Seymour as David Clark
* Lightning (dog) as Prince

==DVD Release==
On October 23, 2012, Warner Home Video released the film on DVD in Region 1 via their Warner Archive Collection alongside The Case of the Curious Bride, The Case of the Lucky Legs, The Case of the Velvet Claws, The Case of the Black Cat and  The Case of the Stuttering Bishop in a set entitled Perry Mason: The Original Warner Bros. Movies Collection. This is a manufacture-on-demand (MOD) release, originally available exclusively through Warners online store and only in the US, but as of 2013 available through Amazon, Barnes & Noble, and other on-line sources.

==Notes==
Other films in this series; Donald Woods
* The Case of the Black Cat (1936) Played by Ricardo Cortez
* The Case of the Velvet Claws (1936) Played by Warren William
* The Case of the Lucky Legs (1935) Played by Warren William
* The Case of the Curious Bride 1935) Played by Warren William
* The Case of the Howling Dog (1934) Played by Warren William

==References==
 
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 