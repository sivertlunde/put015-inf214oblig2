Das zweite Schraube-Fragment
 
{{Infobox Film
| name           = Das zweite Schraube-Fragment
| image          = 
| image size     = 
| caption        = 
| director       = Walter Andreas Christen
| producer       = Walter Andreas Christen
| writer         = Walter Andreas Christen
| narrator       = 
| starring       = Mathias Forberg
| music          = 
| cinematography = Hannes Eder
| editing        = Hannes Eder
| distributor    = 
| released       = May, 1985
| runtime        = 45 minutes
| country        = Austria
| language       = German
| budget         = 
| preceded by    = 
| followed by    = 
}}
 short adventure film directed by Walter Andreas Christen. It was screened in the Un Certain Regard section at the 1986 Cannes Film Festival.   

==Cast==
* Mathias Forberg - Günther Schraube
* Axel Klingenberg - Klingenberg
* Paola Loew - Putzfrau
* Jost Meyer - Pianist
* Justus Neumann - Prof. Neumann
* Ella Peneder - Sopranistin
* Jutta Schwarz - Jutta
* Bruno Thost - Dr. Grabl
* Freyja Wisböck - Hure
* Dieter Witting - Dieter

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 