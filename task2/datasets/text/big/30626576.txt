Taqdeerwala
{{Infobox film
| name           = Taqdeerwala
| image          = 
| caption        =
| director       = K. Murali Mohana Rao
| producer       = D. Rama Naidu
| writer         = S.V. Krishna Reddy (Story)  Kader Khan (dialogues) Venkatesh Raveena Tandon
| music          = Anand–Milind
| cinematography = K. Ravindra Babu
| editing        = K.A. Martand
| studio         = Suresh Productions
| distributor    = 
| released       =  
| runtime        = 2:42:08  
| country        = India
| language       = Hindi
| budget         =
| gross          = 
}}

Taqdeerwala is a Hindi film ( ), produced by D. Rama Naidu on Suresh Productions banner, directed by K. Murali Mohana Rao. Starring Daggubati Venkatesh|Venkatesh, Raveena Tandon in lead roles and music composed by Anand–Milind. The film recorded as flop at box office. This is a remake of Telugu film Yamaleela.

==Plot==
Suraj (Daggubati Venkatesh|Venkatesh) is a mischief guy and his mother (Reema Lagoo) is disappointed with his behavior. Suraj comes to know about his mothers past, his father (D. Ramanaidu) is a Zamindar, owner of a palace called Swarna Palace who died because of debts and they lost their entire property, Suraj swears on his mother to again buy their Palace in any circumstances and make his mother happy. Lilly (Raveena Tandon) a petty thief, is very greedy women, Suraj falls in love with her in first look but she turns him down. Chota Ravan (Shakti Kapoor) a rowdy opponent to Lilly, teases Suraj a lot and he falls sick.

Meanwhile in hell, Yamraj (Kader Khan) (God of death) & Chitragupt (Asrani) misplaces a book called Bhavishyavaani which shows future of human. The book somehow falls on the roof of Surajs house, he benefits a lot by reading predictions of the future in the book and becomes rich and Chota Ravan is surprised how Suraj became rich in short time. Lord Brahma warns Yamraj & Chitragupt within one month they should find out the book in earth otherwise they lose their supernatural powers. In earth Suraj again buys their Palace and takes her mother to it and he asks her mother anything more she wants, she asks him to get married so Suraj again opens the book to see whether his marriage will happen with Lilly or not, then a shocking incident he sees that his mother will die that day night at 10 PM, to fulfill his mothers last wish Suraj plays a marriage drama with Lilly, but accidentally his mother does not die and Lilly reveals all this drama which leads to his mothers anger and she stops talking to him until he really brings back Lilly as her daughter-in-law.

Meanwhile, Yamraj & Chitragupt reaches earth in search of the book. As they reach earth faces a lot of problems because everyone feels them as drama company artists which leads to lot humorous situations. Suraj saves them once and realizes that they are real Yamraaj & Chitragupt and his mother didnt die because they are not having the book. To protect his mother Suraj trap Yamraj with the help of a drama girl Lata (Tisca Chopra). After sometime Yamraj knows the truth and also understands the book is with Suraj, Yamraj asks him to give back the book but he refuses to return. From that day they tries for the book in many ways but fail. One day Chota Ravan beats Suraj very badly for the secret behind his success, but he doesnt reveal. Yamraj saves him and asks him why all this, he says for his mothers sake, then Yamraj understands his devotion towards her. Yamraj wants meet her, Suraj invites them to his house without knowing their real faces and accidentally Yamraj blesses Surajs mother for complete life.

Meanwhile, Chota Ravan wants to know the secret behind Surajs success, he asks Lilly to go and find out the secret. Lilly plays a love drama with Suraj and asks him the secret, he refuses to reveal, she finally says he has to make a choice of her or his mother, then without any hesitation he chooses his mother and throws her out, then she understands his affection towards his mother and his love towards her. Finally, Chota Ravan Kidnaps Surajs mother and blackmails him for the book. Yamraj takes advantage of situation, Suraj gives the book to, Chota Ravan, Yamraj destroys him and collects the book. Finally, Yamraj comes to take Suraj mothers life but she has complete life because of his blessings. Lilly, also apologizes to Suraj & his mother & she marries & comes to their home. It ends with Yamraaj & Chitragupt eating HIMCREAM! (Ice cream) & watching their marriage happily !

==Cast==
{{columns-list|3| Venkatesh as Suraj
* Raveena Tandon as Lilly
* Kader Khan as Yamraaj
* Asrani as Chirtagupt
* Shakti Kapoor as Chota Ravan
* Anupam Kher as Inspector Ranjit Singh
* Laxmikant Berde as Pandu
* D. Ramanaidu as Surajs father
* Reema Lagoo as Surajs mother
* Satyen Kappu as Gopal Kaka
* Tiku Talsania as Marwadi Seth
* Dinesh Hingoo as Editor
* Rakesh Pandey    
* Tisca Chopra as Lataa 
* Amrut Patel 
* Shashi Kiran 
* Ghan Shyam  Ashok Kumar as Dev Duth Chitti Babu  Jenny as Constable 
* Raja Shree 
* Monika 
* Neelima 
* Malleswari 
* Kalpila 
* Lavanya 
}}

==Soundtrack==
{{Infobox album
| Name        = Taqdeerwala
| Tagline     = 
| Type        = film
| Artist      = Anand–Milind
| Cover       = 
| Released    = 1995
| Recorded    = 
| Genre       = Soundtrack
| Length      = 38:33
| Label       = Tips Audio
| Producer    = Anand–Milind
| Reviews     = Taaqat   (1995) 
| This album  = Taqdeerwala   (1995)
| Next album  = Vapasi Saajan Ki   (1995)
}}
Music composed by Anand-Milind. Lyrics written by Sameer. Music released on TIPS Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 38:33
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Aey Chhori Tu Tapori
| extra1  = Kumar Sanu,Alka Yagnik
| length1 = 5:12

| title2  = Char Bhaje Bagon Mein
| extra2  = Udit Narayan,Sadhana Sargam
| length2 = 5:30

| title3  = Aankhon Ka Kajal  
| extra3  = Udit Narayan,Alka Yagnik
| length3 = 4:44

| title4  = Mera Dil Deewana 
| extra4  = Abhijeet Bhattacharya,Alka Yagnik
| length4 = 5:01

| title5  = Dil Chura Ke Najaja
| extra5  = Abhijeet Bhattacharya,Alka Yagnik
| length5 = 5:03

| title6  = Phool Jaisi Muskaan  
| extra6  = Kumar Sanu,Sadhana Sargam
| length6 = 5:19

| title7  = Sugswagtam Abhinandanam   SP Balu,Sadhana Sargam
| length7 = 4:42

| title8  = Annarth Ho Anisth Ho    SP Balu
| length8 = 3:02
}}

==External links==
*  

 
 
 
 