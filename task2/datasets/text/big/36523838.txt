Raj Nartaki
{{Infobox film
| name           = Raj Nartaki
| image          =
| image_size     =
| caption        =
| director       = Modhu Bose
| producer       = Wadia Movietone
| writer         = Manmath Ray
| narrator       =
| starring       = Sadhona Bose, Prithviraj Kapoor Protima Das Gupta Jal Khambata
| music          = Timir Baran
| cinematography = Jyotin Das Probodh
| editing        = Shyam Das
| distributor    =
| released       =  
| runtime        =
| country        = British Raj Hindi English Bengali
| budget         =
| gross          =
| website        =
}}
 1941 Bollywood   film directed by Modhu Bose under the Wadia Movietone banner.    It starred the famous dancer Sadhana Bose (written as Sadhona in the title) with Prithviraj Kapoor, Jal Khambata, Nayampalli and Protima Das Gupta.    It was made simultaneously in English, Bengali and Hindi. The film was distributed in Europe and US through Columbia in Hollywood. It managed to recover its cost with the virtue of being released in three languages. Raj Nartaki established J. B. H Wadia’s reputation as an intellectual film maker.    The story is set in the early 19th century in the Manipur Kingdom and is about social barriers and a court dancer.

==Cast==
*Sadhona Bose: Indrani the Court Dancer
*Protima Das Gupta: Riya the companion
*Benita Gupta: Priya, the companion
*Prithviraj Kapoor: Prince Chandrakirti
*Jal Khambata: High Priest Kashishwar
*Nayampalli: King Jaisingh
*Thapan: Captain of the Guards
*Simeons: Hermit Khalpa
*Prabhat Sinha: Envoy Bhadrapal

  
==References==
 

==External links==
*  

 

 
 
 
 