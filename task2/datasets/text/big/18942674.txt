Night Terrors (film)
{{Infobox film
| name           = Night Terrors
| image          = Night Terrors VHS cover.jpg
| alt            =
| caption        = VHS cover
| director       = Tobe Hooper
| producer       = Yoram Globus Christopher Pearce Harry Alan Towers
| writer         = Rom Globus Daniel Matmor
| starring       = Robert Englund Zoe Trilling Alona Kimhi  Juliano Mer-Khamis
| music          = Dov Seltzer
| cinematography = Amnon Salomon
| editing        = Alain Jakubowicz
| studio         = Global Pictures 1993
| runtime        = 98 min.
| country        = United States Canada Egypt
| language       = English
}}
 horror film, directed by Tobe Hooper. The plot involves a young girl travels to Cairo to visit her father, but becomes unwillingly involved with a bizarre sadomasochistic cult led by the charismatic Paul Chevalier, a descendant of Marquis de Sade. Horror star Robert Englund plays both Chevalier and de Sade.

==Plot==
A young girl travels to Cairo to visit her father, and becomes unwillingly involved with a bizarre sadomasochistic cult led by the charismatic Paul Chevalier, who is a descendant of the Marquis de Sade.

== Critical reception ==
 Spontaneous Combustion, director Tobe Hooper "plumbed new depths". 

The film currently holds a 3.1/10 rating on the Internet Movie Database based on over seven hundred user ratings. 

== References ==

 

== External links ==

*  
*  

 

 
 
 
 
 
 
 
 

 