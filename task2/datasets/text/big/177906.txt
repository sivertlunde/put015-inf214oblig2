Labyrinth (film)
 
 
{{Infobox film
| name           = Labyrinth
| image          = Labyrinth ver2.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Jim Henson
| producer       = Eric Rattray  
| screenplay     = Terry Jones Dennis Lee Jim Henson
| starring       = David Bowie Jennifer Connelly  Trevor Jones  Alex Thomson
| editing        = John Grover Henson Associates Lucasfilm
| distributor    = TriStar Pictures|Tri-Star Pictures
| released       =  
| runtime        = 101 minutes  
| country        = United Kingdom United States
| language       = English
| budget         = $25 million
| gross          = $12,729,917
}} musical Adventure adventure fantasy film directed by Jim Henson, executive produced by George Lucas and based upon conceptual designs by Brian Froud. The film stars David Bowie as Jareth and Jennifer Connelly as Sarah. The plot revolves around 15-year-old Sarahs quest to reach the center of an enormous otherworldly maze to rescue her infant brother Toby, who has been kidnapped by Jareth, the Goblin King. With the exception of Bowie and Connelly, most of the significant characters in the film are played by puppets produced by Jim Hensons Creature Shop.
 Dennis Lee, Upper Nyack, Piermont and Haverstraw in New York, and at Elstree Studios and West Wycombe Park in the United Kingdom.
 Brian remembered the time of the films release as one of the most difficult periods of his fathers career. It would be the last feature film directed by Henson before his death in 1990. 
 cult following and tributes to it have been featured in magazines such as Empire (magazine)|Empire and Total Film. A four-volume manga sequel to the film, Return to Labyrinth, was published by Tokyopop between 2006-10. In 2012, Archaia Studios Press announced they were developing a graphic novel prequel to the film.

==Plot==
Fifteen-year-old Sarah Williams reads lines from a play called The Labyrinth in the park, watched by her dog and an unseen barn owl. She struggles over the final lines of the play, having to hurry home before she can master them. Much to her annoyance, her stepmother and her father leave Sarah in charge of her baby brother Toby while they go out for the evening. Sarah (discovering that Toby has her treasured teddy bear, Lancelot) inadvertently wishes for the Jareth the Goblin King to come and take him away, inspired by the plays story. When he acquiesces to her request, she immediately regrets it. But he refuses to give Toby up and wants to turn him into a goblin. Upon Sarahs insistence, he gives her thirteen hours in order to solve his vast labyrinth and deposits her outside of its gates. She meets Hoggle, a cowardly and curmudgeonly dwarf who gets her in, but advises against her continuing.

Sarah has difficulty with solving the maze, the paths changing and the creatures within hindering her progress. She solves a puzzle of truths but falls into an oubliette, only to be freed by Hoggle. Jareth confronts them and deducts Sarahs time due to her confidence, and Hoggle accidentally reveals he, as a subject of Jareth, was supposed to lead her out of the Labyrinth. After narrowly escaping a deadly cleaning device for the tunnels, Hoggle tries to cut and run, but is convinced to stay when Sarah steals his stash of jewels. They soon encounter Ludo who, despite his beastly appearance, is a simple and kind giant. The group is separated upon entering a swamp inhabited by Fierys, excitable creatures that can detach their heads. Hoggle rescues Sarah from losing her own head and they are led to the Bog of Eternal Stench. They reunite with Ludo and meet Sir Didymus, an anthropomorphic fox-terrier with an old English sheep dog (named Ambrosius) as his steed, and Ludo reveals he can summon rocks with his howl, saving Sarah from falling into the bog. Sarah later expresses hunger, and Hoggle regrettably gives her a magic peach from Jareth. Sarah enters a haze as her memories begin to fade, and she enters a dream world where Jareth dances with her, revealing that he is in love with her. Sarah escapes and falls into a desolate junk yard where an old hag tries to convince her to stay. But she remembers her cause – that she has to save Toby – and breaks Jareths hold on her.
 room with many stairs but is unable to reach Toby as Jareth tries to confront her. Sarah makes a leap of faith to catch her brother, but Jareth intervenes and tries to convince her to love him. Sarah recites the plays monologue, but is stuck again on the last line. When Jareth asks her to "Fear me, love me, do as I say, and I shall be your slave!" Sarah remembers the line and quotes "You have no power over me!" Jareth, defeated at the last possible second, uses his magic to send Sarah back to her world. She rushes upstairs to find that Toby is also there, and she gives him Lancelot, realizing that she needs to put away childish things. Sarah returns to her room where she sees her friends in her mirror telling her that they would be there for her when she needs them. When Sarah exclaims she does need them every now and again, they cheer and everyone Sarah had met in the Labyrinth suddenly appears, celebrating raucously in her room. Jareth, in his owl form, watches from outside and then flies away.

==Cast==
* Jennifer Connelly as Sarah Williams, a 15-year-old girl who journeys through the Labyrinth to find her baby brother.
* David Bowie as Jareth the Goblin King. He brings Toby to his Labyrinth on Sarahs wish and falls in love with her.
* Toby Froud as Toby Williams. Sarahs baby half-brother
* Christopher Malcolm as Robert, Sarahs father
* Shelley Thompson as Irene, Sarahs stepmother

===Puppeteers=== dwarf in Jareths employ who befriends Sarah.
** Brian Henson as the voice of Hoggle
* Ron Mueck and Rob Mills as Ludo, a kind-hearted beast.
** Ron Mueck as the voice of Ludo David Barclay as Sir Didymus, a brave knight.
** David Shaughnessy as the voice of Sir Didymus
* Steve Whitmire and Kevin Clash as Ambrosius, an Old English Sheepdog.
** Percy Edwards as the voice of Ambrosius
* Karen Prell as The Worm
** Timothy Bateson as the voice of The Worm
* Frank Oz as The Wiseman
** Michael Hordern as the voice of The Wiseman
* Dave Goelz as The Wisemans Bird Hat
** David Shaughnessy as the voice of The Wisemans Bird Hat
* Karen Prell as The Junk Lady
** Denise Bryer as the voice of The Junk Lady
* Steve Whitmire, Kevin Clash, Anthony Asbury, and Dave Goelz as The Four Guards Anthony Jackson, Douglas Blackwell, David Shaughnessy, and Timothy Bateson as the voices of The Four Guards David Barclay, and Toby Philpott as Firey 1
** Kevin Clash as the voice of Firey 1
* Karen Prell, Ron Mueck, and Ian Thom as Firey 2
** Charles Augins as the voice of Firey 2
* Dave Goelz, Rob Mills, and Sherry Ammott as Firey 3
** Danny John-Jules as the voice of Firey 3
* Steve Whitmire, Cheryl Henson, and Kevin Bradshaw as Firey 4
** Danny John-Jules as the voice of Firey 4
* Anthony Asbury, Alistair Fullarton, and Rollie Krewson as Firey 5
** Richard Bodkin as the voice of Firey 5
* Anthony Asbury as Right Door Knocker David Healy as the voice of the Right Door Knocker
* Dave Goelz as Left Door Knocker
** Robert Beatty as the voice of the Left Door Knocker
* Natalie Finland as the Fairies

Juggler Michael Moschen performed Jareths elaborate crystal-ball contact juggling manipulations. 

Goblin Corps performed by Marc Antona, Kenny Baker, Michael Henbury Ballan, Danny Blackner, Peter Burroughs, Toby Clark, Tessa Crockett, Warwick Davis, Malcolm Dixon, Anthony Georghiou, Paul Grant, Andrew Herd, Richard Jones, John Key, Mark Lisle, Peter Mandell, Jack Purvis, Katie Purvis, Nicholas Read, Linda Spriggs, Penny Stead, and Albert Wilkinson.
 Michael Quinn, Francis Wright.
 Sean Barrett, Timothy Bateson, Douglas Blackwell, John Bluthal, Brian Henson, Anthony Jackson, Peter Marinker, Ron Mueck, Kerry Shale, and David Shaughnessy.

==Influences== The Wizard of Oz and the works of Maurice Sendak, writing that "Labyrinth lures a modern Dorothy Gale out of the drab Kansas of real life into a land where the wild things are."    Nina Darnton wrote that the plot of Labyrinth "is very similar to Outside Over There by Mr. Sendak, in which 9-year-old Idas baby sister is stolen by the goblins."     Copies of Outside Over There and Where the Wild Things Are are shown briefly in Sarahs room at the start of the film. The film also features an end credit stating that "Jim Henson acknowledges his debt to the works of Maurice Sendak".
 Scarlet Pimpernel. Bowies costumes were intentionally eclectic, drawing on the image of Marlon Brandos leather jacket from The Wild One as well as that of a knight "with the worms of death eating through his armour" from the fairy tales of the Brothers Grimm. Froud (2006), pp. 139–153 

The dialogue starting with "you remind me of the babe" that occurs between Jareth and the goblins in the Magic Dance sequence in the film is a direct reference to an exchange between Cary Grant and Shirley Temple in the 1947 film The Bachelor and the Bobby-Soxer. 

==Production==

===Origins and script===
According to the films conceptual designer Brian Froud, Labyrinth was first discussed between himself and director Jim Henson during a limousine ride on the way back from a special screening of their 1982 film The Dark Crystal.  Both agreed to work on another project together, and Froud suggested that the film should feature goblins. On the same journey, Froud "pictured a baby surrounded by goblins" and this strong visual image – along with Frouds insight that goblins traditionally steal babies – provided the basis for the films plot. {{cite journal|last=Pirani|first=Adam|title=Part Two: Into the Labyrinth with Jim Henson |journal=Starlog| volume = 10 pages = 44–48|date=August 1986}} 

Discussing the films origins, Henson explained that he and Froud "wanted to do a lighter weight picture, with more of a sense of comedy since Dark Crystal got kind of heavy – heavier than we had intended. Now I wanted to do a film with the characters having more personality, and interacting more." 
 Dennis Lee. Erik the Viking and suggested that he try me as screen-writer."  Howard (1993), pp. 209–210  Jones was given Dennis Lees novella to use as a basis for his script, but later told Empire that Lee had produced an unfinished "poetic novella" that he "didnt really get on with." In light of this, Jones "discarded it and sat down with Brian  s drawings and sifted through them and found the ones that I really liked, and started creating the story from them."   

While Jones is credited with writing the screenplay, the shooting script was actually a collaborative effort that featured contributions from Henson, George Lucas, Laura Phillips, and Elaine May. Jones himself has said that the finished film differs greatly from his original vision. According to Jones, "I didnt feel that it was very much mine. I always felt it fell between two stories, Jim wanted it to be one thing and I wanted it to be about something else." Jones has said his version of the script was "about the world, and about people who are more interested in manipulating the world than actually baring themselves at all." In Jones original script, Jareth merely seems "all powerful to begin with" and is actually using the Labyrinth to "keep people from getting to his heart." 

Jones has said that Bowies involvement in the project had a significant impact on the direction taken with the film. Jones had originally intended for the audience not to see the center of the Labyrinth prior to Sarahs reaching it, as he felt that doing so robbed the film of a significant hook. With the thought of Bowie starring in the film in mind, Henson decided he wanted Jareth to sing and appear throughout the film, something Jones considered to be the "wrong" decision. Despite his misgivings, Jones re-wrote the script to allow for songs to be performed throughout the film. This draft of the script "went away for about a year," during which time it was re-drafted first by Laura Phillips and subsequently by George Lucas.  

An early draft of the script attributed to Jones and Phillips is markedly different from the finished film. The early script has Jareth enter Sarahs house in the guise of Robin Zakar, the author of a play she is due to perform in. Sarah does not wish for her brother to be taken away by the goblins, and Jareth snatches him away against her will. Jareth is overtly villainous in this draft of the script, and during his final confrontation with Sarah he tells her he would "much rather have a Queen" than "a little goblin prince." The early script ends with Sarah kicking Jareth in disgust, her blows causing him to transform into a powerless, sniveling goblin. 

The re-drafted script was sent to Bowie, who found that it lacked humor and considered withdrawing his involvement in the project as a result. To ensure Bowies involvement, Jim Henson asked Jones to "do a bit more" to the script in order to make it more humorous.   Elaine May met with Henson several months prior to the start of filming in April 1985, and was asked to polish the script. Mays changes "humanized the characters" and pleased Henson to the extent that they were incorporated into the films shooting script.    

At least twenty-five treatments and scripts were drafted for Labyrinth between 1983–85, and the films shooting script was only ready shortly before filming began. 

===Casting===
The protagonist of the film was, at different stages of its development, going to be a King whose baby had been put under an enchantment, a princess from a fantasy world and a young girl from Victorian England. In order to make the film more commercial, it was ultimately decided that the films lead would be a teenage girl from contemporary America. Henson noted that he wished to "make the idea of taking responsibility for ones life – which is one of the neat realizations a teenager experiences – a central thought of the film." 

Auditions for the lead role of Sarah began in England in April 1984. Helena Bonham Carter auditioned for the part, but it was ultimately decided it would be better to cast an American actress. Monthly auditions were held in the U.S until January 1985, and Jane Krakowski, Yasmine Bleeth, Sarah Jessica Parker, Marisa Tomei, Laura Dern, Ally Sheedy, Maddie Corman, and Mia Sara all auditioned for the role. Of these, Krakowski, Sheedy and Corman were considered to be the top candidates. Fifteen year old actress Jennifer Connelly was ultimately chosen to play Sarah after her audition on January 29, 1985, "won Jim   over" and led him to cast her within a week.  According to Henson, Connelly was chosen as she "could act that kind of dawn-twilight time between childhood and womanhood."    Connelly moved to England in February 1985 in advance of the films rehearsals, which began in March.  Discussing her understanding of her role with Elle (magazine)|Elle, Connelly said that the film is about "a young girl growing out of her childhood, who is just now becoming aware of the responsibilities that come with growing up." 

The character of Jareth also underwent some significant developments during the early stages of pre-production. According to Henson he was originally meant to be another puppet creature in the same vein as his goblin subjects.  Henson eventually decided he wanted a big, charismatic star to the play the Goblin King, and decided to pursue a musician for the role. Sting (musician)|Sting, Prince (musician)|Prince, Mick Jagger, and Michael Jackson were considered for the part, however it was ultimately decided that David Bowie would be the most suitable choice.   

"I wanted to put two characters of flesh and bone in the middle of all these artificial creatures," Henson explained, "and David Bowie embodies a certain maturity, with his sexuality, his disturbing aspect, all sorts of things that characterize the adult world."    Henson met David Bowie in the summer of 1983 to seek his involvement, as Bowie was in the U.S for his Serious Moonlight Tour at the time. Pegg (2002), pp. 468–469  Henson continued to pursue Bowie for the role of Jareth, and sent him each revised draft of the films script for his comments. During a meeting that took place on June 18, 1984, Henson showed Bowie The Dark Crystal and a selection of Brian Frouds concept drawings to pique his interest in the project.    Bowie formally agreed to take part on February 15, 1985, several months before filming began.   Discussing why he chose to be involved in the film, Bowie explained that "Id always wanted to be involved in the music-writing aspect of a movie that would appeal to children of all ages, as well as everyone else, and I must say that Jim gave me a completely free hand with it. The script itself was terribly amusing without being vicious or spiteful or bloody, and it had a lot more heart in it than many other special effects movies. So I was pretty hooked from the beginning."   

===Filming===
Principal photography began on April 15, 1985, at Elstree Studios. 

The team that worked on Labyrinth was largely assembled from talent who had been involved in various other projects with the Jim Henson Company. Veteran performers Frank Oz and Dave Goelz operated various puppets in the film, as did Karen Prell, Ron Mueck and Rob Mills who had all worked with Henson on Fraggle Rock. Kevin Clash, who had previously been a crew member on Sesame Street, worked on the film. Members of Hensons family also worked on the production, including son Brian and daughter Cheryl. Newcomers working on the production included puppeteer Anthony Asbury, who had previously worked on the satirical puppet show Spitting Image.

Labyrinth took five months to film, and was a complicated shoot due to the myriad of puppets and animatronic creatures involved. In the making-of documentary Inside the Labyrinth, Henson stated that although Jim Hensons Creature Shop had been building the puppets and characters required for around a year and a half prior to shooting, "everything came together in the last couple weeks." Henson noted that "even if you have the characters together, the puppeteers start working with them, they find problems or they try to figure out what theyre going to do with these characters." 

Although each of the films key puppets required a small team of puppeteers to operate it, the most complex puppet of the production was Hoggle. Shari Weiser was inside the costume, while Hoggles face was radio-controlled by Brian Henson and three other operators. Speaking in the Inside the Labyrinth documentary, Brian Henson explained that Weiser "does all the body movement and her head is inside the head. However, the jaw is not connected to her jaw. Nothing that the face is doing has any connection with what shes doing with her face. The other four members of the crew are all radio crew, myself included." Speaking of the challenges involved with performing Hoggle, Brian Henson said that "five performers trying to get one character out of one puppet was a very tough thing. Basically what it takes is a lot of rehearsing and getting to know each other." 

At the early stages of filming, stars Connelly and Bowie found it difficult to interact naturally with the puppets they shared most of their scenes with. Bowie told Movieline "I had some initial problems working with Hoggle and the rest because, for one thing, what they say doesnt come from their mouths, but from the side of the set, or from behind you."  Connelly remarked that "it was a bit strange  , but I think both Dave   and I got over that and just took it as a challenge to work with these puppets. And by the end of the film, it wasnt a challenge anymore. They were there, and they were their characters." 

The film required large and ambitious sets to be constructed, from the Shaft of Hands to the rambling, distorted Goblin City where the films climatic battle takes place. The Shaft of Hands sequence was filmed on a rig that was roughly forty feet high, and required nearly a hundred performers to operate the grey, scaly hands integral to the scene. Connelly was strapped into a harness when shooting the scene, and would spend time between takes suspended mid-way up the shaft. 
 old mans beard." 

While most filming was conducted at Elstree Studios, a small amount of location shooting was carried out in England and the U.S. The park seen at the start of the film is West Wycombe Park in Buckinghamshire, England. The scenes of Sarah running back home were filmed in various towns in New York, namely North Nyack, Piermont and Haverstraw.    

Shooting wrapped on September 8, 1985. 

===Post-production===
Most of the visual effects on Labyrinth were achieved in camera, with several notable exceptions. The most prominent of these post production effects was the computer generated owl that appears at the opening of the film. The sequence was created by animators Larry Yaeger and Bill Kroyer,   and marked the first use of a realistic CGI animal in a film.  

The scene where Sarah encounters the Fire Gang had to be altered in post-production as it had been filmed against black velvet cloth, and a new forest background was added. Jim Henson was unhappy with the compositing of the finished scene, although he considered the puppetry featured in it worthy of inclusion. 

Henson received help editing the film from executive producer George Lucas. According to Henson, "When we hit the editing, I did the first cut, and then George was heavily involved on bringing it to the final cut. After that, I took it over again and did the next few months of post-production and audio." Henson went on to explain that "When you edit a film with somebody else you have to compromise. I always want to go one way, and George goes another way, but we each took turns trading off, giving and taking. George tends to be very action-oriented and he cuts dialogue quite tight; I tend to cut looser, and go for more lyrical pauses, which can slow the story. So, I loosen up his tightness, and he tightens my looseness." 

===Music===
  Trevor Jones Film score|score, which is split into six tracks for the soundtrack: "Into the Labyrinth", "Sarah", "Hallucination", "The Goblin Battle", "Thirteen OClock", and "Home at Last".

Bowie recorded five songs for the film: "Underground (David Bowie song)|Underground", "Magic Dance", "Chilly Down", "As the World Falls Down", and "Within You". "Underground" features on the soundtrack twice, first in an edited version that was played over the films opening sequence and secondly in full. "Underground" was released in various territories as a single, and in certain markets was also released in an instrumental version and an extended dance mix.  "Magic Dance" was released as a 12" single in the U.S.  "As the World Falls Down" was initially slated for release as a follow-up single to "Underground" at Christmas in 1986, but this plan did not materialize.  The only song Bowie did not perform lead vocal on is "Chilly Down", which was performed by Charles Augins, Richard Bodkin, Kevin Clash, and Danny John-Jules, the actors who voiced the Firey creatures in the film. 

Steve Barron produced promotional music videos for "Underground" and "As the World Falls Down". The music video for "Underground" features Bowie as a nightclub singer who stumbles upon the world of the Labyrinth, encountering many of the creatures seen in the film. The clip for "As the World Falls Down" integrates clips from the film, using them alongside black-and-white shots of Bowie performing the song in an elegant room.

==Release==

===Promotion===
The production of Labyrinth was covered in multiple high-profile magazines and newspapers in anticipation of its release, with articles appearing in The New York Times, Time (magazine)|Time and Starlog magazine.      An article that appeared in The New York Times shortly after filming wrapped in September 1985 focused heavily on the films large scale, emphasizing the size of the production and selling Labyrinth as a more "accessible" film than The Dark Crystal due to the casting of live actors in its key roles.  An hour-long making-of documentary that covered the filming of Labyrinth and included interviews with the key figures involved in its production was broadcast on television as Inside the Labyrinth. 

Labyrinth was featured in music trade papers such as Billboard (magazine)|Billboard due to David Bowies soundtrack for the film.  Bowie was not heavily involved in promoting the film but Jim Henson was nonetheless grateful that he produced a music video to accompany the song "Underground" from the soundtrack, saying "I think its the best thing he could have done for the film."  Ted Coconis produced a one-sheet poster for the films North American release. 
 Seibu Department Store in Tokyo in August 1986.   

===Theatrical release===
Labyrinth received its North American premiere on June 27, 1986. It was released in South America next, and was released in Argentina under the title Laberinto on July 10, 1986 and in Brazil as Labirinto – A Magia do Tempo (Labyrinth – The Magic of Time) on July 24, 1986.    The film was released in Japan on July 26, 1986; Jennifer Connelly visited the country to promote its release.  
 Prince and Princess of The Sun quoted the Princess of Wales as remarking, "Isnt he wonderful!" upon being introduced to Ludo. 

The film was rolled out in other European countries largely between December 1986 and February 1987, and premiered in France as Labyrinthe on December 2 and in West Germany as Die Reise ins Labyrinth (The Journey into the Labyrinth) on December 13. The film was released in Denmark as Labyrinten til troldkongens slot (The Labyrinth to the Troll Kings Castle) on February 20, 1987, and saw its last theatrical release in Hungary under the tile Fantasztikus labirintus (Fantastic Labyrinth) when it premiered there on July 7, 1988. 

In 2012, the film was digitally remastered and re-released at the The Astor Theatre in Melbourne, Australia.

===Home media===
Labyrinth was first released on home video in 1987 by Embassy Home Entertainment in the US and by Channel 5 Video Distribution in the UK. Sony re-issued the film on video in 1999 in the US under the name of its subsidiary company, Columbia-TriStar. Labyrinth was re-issued on VHS in the UK the same year, with Inside the Labyrinth included as a special feature.

The film made its   and Brian Henson. 

The film was released on Blu-ray Disc|Blu-ray in 2009, in a package that replicated the extras featured on the 2007 Anniversary Edition DVD. The Blu-ray release featured one new special feature, a picture-in-picture track that lasts the length of the film and features interviews with the crew and several minor cast members including Warwick Davis. 

==Reaction==

===Box office=== Back to Running Scared, Top Gun, and Ferris Buellers Day Off.  In its next weekend at the box office, the film dropped to number 13 in the charts, only earning $1,836,177.  By the end of its run in U.S cinemas the film had grossed $12,729,917,  just over half of its $25 million budget. 

===Critical reception===
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"As he did with less success in The Dark Crystal, Mr. Henson uses the art of puppetry to create visual effects that until very recently were possible to attain only with animation. The result is really quite startling. It removes storyboard creations from the flat celluloid cartoon image and makes them three-dimensional, so that they actually come alive and interact with living people. The technique makes animation seem dull and old-fashioned by comparison."
|-
| style="text-align: left;" | — Nina Darnton of The New York Times on Labyrinths technical achievements. 
|}
The film received mixed reviews from critics.  On  , which uses a "weighted average" of all the critics scores, Labyrinth scores 50 out of 100. 
 Gene Siskels review of Labyrinth for the Chicago Tribune was highly negative, and he referred to it as an "awful" film with a "pathetic story," "much too complicated plot" and a "visually ugly style." Siskel objected to the films "violent" plot, writing that "the sight of a baby in peril is one of sleaziest gimmicks a film can employ to gain our attention, but Henson does it." 

Other critics were more positive. Kathryn Buxton found that it had "excitement and thrills enough for audiences of all ages as well as a fun and sometimes slightly naughty sense of humor."  Bruce Bailey admired the films script, stating that "Terry Jones has drawn on his dry wit and bizarre imagination and come up with a script that transforms these essentially familiar elements and plot structures into something that fairly throbs with new life." Bailey was also impressed by the films depth, writing that "adults will have the additional advantage of appreciating the story as a coming-of-age parable."   
 The Nutcracker "is also about the voyage to womanhood, including the hint of sexual awakening, which Sarah experiences too in the presence of a goblin king." Darton enjoyed the film and considered it to be more successful than Hensons previous collaboration with Brian Froud, The Dark Crystal. 

Connellys portrayal of Sarah polarized critics and received strong criticism from some reviewers. Critic Kirk Honeycutt referred to Connelly as "a bland and minimally talented young actress"  Writing for The Miami News, Jon Marlowe stated that "Connelly is simply the wrong person for the right job. She has a squeaky voice that begins to grate on you; when she cries, you can see the onions in her eyes."  Contrary to these negative views, an anonymous review in Tampa Bay Times|St. Petersburg Times praised her acting saying that "Connelly makes the entire experience seem real. She acts so naturally around the puppets that you begin to believe in their life-like qualities."   

Bowies performance was variously lauded and derided. In his largely positive review of the film, Corliss praised him as "charismatic" referring to his character as a "Kabuki sorcerer who offers his ravishing young antagonist the gilded perks of adult servitude."  Bruce Bailey enjoyed Bowies performance, writing that "the casting of Bowie cant be faulted on any count. He has just the right look for a creature whos the object of both loathing and secret desire."  In a largely critical review, the St. Petersburg Times found that "Bowie forgoes acting, preferring to prance around his lair while staring solemnly into the camera. Hes not exactly wooden. Plastic might be a more accurate description." 

The films mixed reviews and poor box office takings demoralized Henson to the extent that his son Brian remembered the time of the films release as being "the closest Ive seen him to turning in on himself and getting quite depressed." It was the last feature film directed by Henson before his death in 1990.   

Since Hensons death, Labyrinth has been re-evaluated by several notable critics. A review from 2000 in Empire magazine called the film "a fabulous fantasy" and wrote that "David Bowie cuts a spooky enough figure in that fright wig to fit right in with this extraordinary menagerie of Goth Muppets. And Jennifer Connelly, still in the flush of youth, makes for an appealingly together kind of heroine."  Writing for the Chicago Tribune in 2007, Michael Wilmington described Labyrinth as "dazzling," writing that it is "a real masterpiece of puppetry and special effects, an absolutely gorgeous childrens fantasy movie."  In 2010 Total Film ran a feature called Why We Love Labyrinth which described Labyrinth as a "hyper-real, vibrant daydream, Labyrinths main strength lies in its fairytale roots, which give the fantastical story a platform from which to launch into some deliriously outlandish scenarios."  In their February 2012 issue, Empire featured a four-page spread on Labyrinth as part of their Muppet Special. 

===Legacy===
Despite its poor performance at the American box office, Labyrinth was a success on home video and later on DVD.    David Bowie told an interviewer in 1992 that "every Christmas a new flock of children comes up to me and says, Oh! youre the one whos in Labyrinth!"    In 1997, Jennifer Connelly said "I still get recognized for Labyrinth by little girls in the weirdest places. I cant believe they still recognize me from that movie. Its on TV all the time and I guess I pretty much look the same."   

Labyrinth has become a   hosts over 8,600 stories in its Labyrinth section. 

The strong DVD sales of Labyrinth prompted rights-holders the Jim Henson Company and Sony Pictures to look into making a sequel,  and Curse of the Goblin King was briefly used as a place-holder title.  The decision was ultimately taken to avoid making a direct sequel, and instead produce a fantasy film with a similar atmosphere. Fantasy author Neil Gaiman and artist Dave McKean were called in to write and direct a film similar in spirit to Labyrinth, and MirrorMask was ultimately released in selected theaters in 2005 after premiering at the Sundance Film Festival. 

The Hoggle puppet was lost in luggage while being transported. It ended up at the Unclaimed Baggage Center in Scottsboro, Alabama, and when they contacted the Henson Company, they were told they could keep it. It is now on permanent display in the stores museum. 

==In other media== Marvel Super Special #40,  was published by Marvel Comics. Tokyopop, in partnership with The Jim Henson Company, published a manga-style four-volume comic called Return to Labyrinth, written by Jake T. Forbes, illustrated by Chris Lie, with cover art by Kouyu Shurei.    Return to Labyrinth follows the adventures of Toby as a teenager, when he is tricked into returning to the Labyrinth by Jareth.    The first volume was released August 8, 2006, with a second following on October 9, 2007 and a third on May 1, 2009. In an afterword to the second volume, editor Tim Beedle announced that the series, originally planned as a trilogy, was being extended to include a fourth volume. The fourth and final volume of Return to Labyrinth was released on August 3, 2010.

The Cartoon Network series Secret Mountain Fort Awesome paid an episode-long homage to the film, also entitled "Labyrinth", with Tom Kenny in the role of the Goblin King.

Gravity Falls creator Alex Hirsch mentioned doing an episode, based on Labyrinth, but it was scrapped. However, he still added a reference to the Goblin King in an episode of "Mabels Guide"

Archaia Entertainment, in collaboration with The Jim Henson Company, is developing a prequel comic book about the story of how Jareth became the Goblin King.  Project editor Stephen Christy has stated the graphic novel will be a "tragic story." While it is still in the early stages of development, there are plans for the novel to integrate music into the plot in some way. David Bowie is being approached by Archaia Comics in order to seek permission to use his likeness, and ascertain if he wishes to have any involvement in the project.    Brian Froud is serving as a creative consultant on the project. Froud will be producing covers for the series, as well as character designs.  Archaia released a Labyrinth short story titled Hoggle and the Worm for Free Comic Book Day on May 5, 2012  and another titled Sir Didymus Grand Day on May 4, 2013. 
 Family Computer console and MSX computer, under the title Labyrinth: Maō no Meikyū. 

On April 3, 2014, Seattle rapper Nacho Picasso paid tribute to Bowie with a single titled David Blowie.  The track finds him appropriating Bowies role as the Goblin King in Labyrinth. Produced by Picassos cousin Raised By Wolves, it samples "Within You", a song from the films soundtrack. The single art depicts Bowies character. 

==References==
 

;Bibliography
*  
*  
*  
*  

==Notes==
 

==External links==
 
 
*   at  
*  
*  
*  
*  
*   from Tokyopop

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 