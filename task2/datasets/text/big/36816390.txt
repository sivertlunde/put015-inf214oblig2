Flat Top (film)
 
{{Infobox film
| name           = Flat Top
| image          = 
| caption        = 
| director       = Lesley Selander
| producer       = Walter Mirisch Steve Fisher
| starring       = Sterling Hayden
| music          = 
| cinematography = Harry Neumann William Austin
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
}}
 Richard Carlson, and William Schallert.    The film was nominated for the Academy Award for Best Film Editing.   

==Plot==
The film begins away from the coast of Korea during the Korean War. Commander Collier (Sterling Hayden|Hayden) is in charge of air operations on a carrier, or flat top. When asked about how he first handled his job, he goes back to the time of World War II, where the film takes place. Told in flashback form, Collier recounts the war in the Pacific, working flight ops on the same carrier with a new second officer, Lt. (j.g.) Joe Rogers (Richard Carlson (actor)|Carlson), and getting a group of pilots ready for the tough fight to come against the Japanese.

==Cast==
* Sterling Hayden as Cmdr. Dan Collier Richard Carlson as Lt. (j.g.) Joe Rodgers William Phipps as Red Kelley (as Bill Phipps)
* John Bromfield as Ens. Snakehips McKay
* Keith Larsen as Ens. Barney Smith / Barney Oldfield
* William Schallert as Ens. Longfellow
* Todd Karns as Judge
* Phyllis Coates as Dorothy Collier
* Dave Willock as Willie
* Walter Coy as Air Group Commander
* Jack Larson as "Scuttlebutt" Sailor

==See also==
* Sterling Hayden filmography

==References==
 

==External links==
*  
 
 
 
 
 
 
 
 
 
 
 