President (1937 film)
{{Infobox film
| name           = President
| image          = 
| image_size     = 
| caption        = 
| director       = Nitin Bose
| producer       = New Theatres
| writer         = 
| narrator       = 
| starring       = K. L. Saigal Leela Desai Kamlesh Kumari Prithviraj Kapoor
| music          = R. C. Boral Pankaj Mullick
| cinematography = Nitin Bose
| editing        = 
| distributor    =
| studio         = New Theatres
| released       = 1937
| runtime        = 152 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1937 Hindi social romantic drama film from New Theatres.   The Bengali version was called Didi. The direction, screenplay and cinematography were by Nitin Bose. The film starred K. L. Saigal, Leela Desai, Kamlesh Kumari, Jagdish Sethi, Prithviraj Kapoor and Bikram Kapoor.    The story according to the credit roll of the film is "A tale of love and greater love" developed on an idea by M. M. Begg. It was a love triangle with a social content that highlighted the conditions of the mill workers. It was also the first film to show a liberated educated woman managing her own factory.     

==Plot==
Prabhavati’s (Kamlesh Kumari) parents died when she was very young and the onus of looking after the Prabhavati Cotton Mills Ltd. has come to her.  Her entire focus is on the factory and she is a strict and hardworking proprietor. Her only close companion is Dr. Sethi but even here there is a reserve. Prakash (K. L. Saigal) works at one of the machines in the mill. He finds that the machines they are working on are old and dangerous. He designs a safer and more efficient one and takes the design to the President of the company. She gets angry at his suggestion that the machines in her factory are dangerous and fires him. Prakash is out of a job and looking desperately for any form of income. He meets a young girl Sheela (Leela Desai) outside a girl’s hostel with whom he gets friendly. The girl unknown to him is Prabhavati’s younger sister. 
The day Prakash is sacked from his work, the man who took his place at the machine has an accident. This gets Prabhavati thinking and she goes to Prakash’s home to accept his design and re-instate him as head of the design unit.  Over a period of time Prabhavati finds herself getting attracted to Prakash, without realising he’s in love with her sister Sheela. During a conversation Sheela comes to know that Prabha loves Prakash. Sheela is devoted to her sister and feels deeply indebted to her for single-mindedly taking care of her and the factory.  She leaves home and her attitude towards Prakash changes. Prakash unable to understand her behaviour takes out his frustration at work becoming despotic with the workers. The workers revolt and there is a showdown at the factory. Pratibha rushes to the factory; she manages to find what is troubling Prakash. Appalled at the situation she locks herself in her office and works herself into a state where she collapses. 
==Cast==
*K. L. Saigal
*Leela Desai
*Kamlesh Kumari
*Prithviraj Kapoor
*Jagdish Sethi
*Bikram Kapoor
*Nawab
*Shabir Ali
*Indu Mukherjee

==K. L. Saigal And Songs== Devdas and Street Singer leaving an impact on Hindi film music.    President is cited as one of Saigals finest films    in which he sang one of his most memorable songs "Ik Bangla Bane Nyara".    The lyrics of the song written by Kidar Sharma became an allegorical representation of the common mans dream of owning their own house.   

===Songs===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Rahegi na badariya" 
| K. L. Saigal
|-
| 2
| "Ek Raje ka udane wala ghoda"
| K. L. Saigal
|-
| 3
| "Ek Bangla Bane Nyara" 
| K. L. Saigal
|-
| 4
| "Na Koi Prem ka rog" 
| K. L. Saigal
|-
| 5
| "Chandramukhi ki shaadi"
| Chorus
|-
| 6
| "Prem Ka Hai is jag mein"
| K. L. Saigal
|-
|}

==References==
 

==External links==
* 

 
 
 