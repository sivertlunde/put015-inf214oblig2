As One (film)
{{Infobox film
| name           = As One
| image          = As_One_poster.jpg
| film name      = {{Film name
| hangul         =  
| rr             = Koria
| mr             = K‘oria}}
| director       = Moon Hyun-sung
| producer       = Lee Su-nam   Kim Ji-hoon   Lee Han-seung   Min Jong-eun
| writer         = Kwon Seong-hwi   Yoo Young-ah
| starring       = Ha Ji-won   Bae Doona
| music          = Kim Tae-seong
| cinematography = Jo Dong-heon
| editing        = Kim Sun-min
| distributor    = CJ Entertainment
| released       =  
| runtime        = 127 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}} war Unified Korea sports team which won the gold at the 1991 World Table Tennis Championships in Chiba, Chiba|Chiba, Japan. Director Moon Hyun-sung used the foundation of true events to tell the story of a team that united a divided nation for the first time in its painful history. 

==Background==
Following the North Korean mid-air bombing of Korean Air Flight 858 in 1987, a Summit was held between North and South Korea to defuse the extreme tension on the Korean peninsula. The summit ended with the agreement to hastily form a unified Korean sports team; and table tennis, being highly visible and world-class in both countries, was chosen as the symbolic unifier. Summarily, the first-ever unified North-South team under the simple aegis "KOREA" was formed to compete in the 1991 World Table Tennis Championships in Chiba, Chiba|Chiba, Japan.
 World Championships. Putting aside their individual ambitions, these women have to summon every ounce of grit and grace to defeat a dominating Chinese team that is vying for its 9th consecutive World Championship title.

==Plot== 11th Asian 41st World Kim Eung-soo), as its chief trainer. In Chiba, quarrels and fights break out between the two, mutually suspicious sides, exacerbated by young Northern hothead Choi Kyung-sub (Lee Jong-suk) and Southern joker Oh Doo-man (Oh Jung-se). Jung-hwa shares a room with fellow player Choi Yeon-jung (Choi Yoon-young), who fancies Kyung-sub. Bun-hui rooms with Yu Sun-bok (Han Ye-ri) who suffers badly from competition nerves. During the trials for the womens team, Sun-bok performs poorly and steps down in favor of Jung-hwa for the good of the team. Now paired together, Jung-hwa and Bun-hui settle their differences as the players finally start to bond. As they train for 46 days, game-by-game, the two find a budding friendship. But as the finals versus the Chinese team looms, the Koreans unity is threatened from another direction. When political winds change again and just as suddenly an announcement is made to disband team Korea, the two young women must prove to their people and the world that teamwork can outshine the dark shadows of a painful history.   

==Cast==
*Ha Ji-won - Hyun Jung-hwa   
*Bae Doona - Ri Bun-hui   
*Han Ye-ri - Yu Sun-bok, Bun-huis roommate
*Choi Yoon-young - Choi Yeon-jung, Jung-hwas roommate
*Park Chul-min - Lee Eun-il, South Korean trainer Kim Eung-soo - Jo Nam-poong, North Korean trainer
*Oh Jung-se - Oh Doo-man
*Lee Jong-suk - Choi Kyung-sub
*Park Yeong-seo - Chu Il-song
*Kwon Tae-won - Choi, delegation leader
*Yu Cheong-gwan - Park, team leader
*Park Jeong-hak - Jang Myong-guk, North Korean security chief
*Kim Jae-hwa - Deng Yaping, Chinese champion
*Mike Meier - umpire in finals
*Paul Stafford - Matthew, UK announcer

==Production== 1991 championships, Olympic gold medalist Hyun Jung-hwa reportedly answered, "Why didnt you approach me sooner?"     
 May 18, North Korean Pyongyang accent Yu Sun-boks Hamgyŏng dialect|Hamgyŏng accent were honed.   
 extras were so conscientious about recreating such a well-publicized event, the film captures the look and feel of the historical tournament.

Aside from the main cast, the production sought out many supporting roles and extras to play international-quality table tennis. From the Korea Table Tennis Association official players, where the real Hyun Jung-hwa is currently Executive Director, to current national table tennis players of Hungary and France, top athletes appeared throughout the film for added realism. 

The actresses started their training four months prior to the start of principal shooting. This was Ha Ji-wons first time playing table tennis, while Bae Doona played some back in her primary school days. Choi Yoon-young, Oh Jung-se and other cast members also trained together for a total of seven months of intensive training. Because of the entire casts efforts, there were no body doubles for the tournament shots. 
 Sector 7 and was in the process of recovery. Yet she still kept up with highly intensive training, liking the physical challenges of learning the sport  and recreating a true story. Ha had been Hyuns first choice to play her,  and the actress observed Hyun’s speech and body language carefully during training, striving to embody all of Hyuns playing habits and techniques. 

It was no easy task for Bae as well, training to the point where she lost toe nails.  She also had the added disadvantage of playing a left-handed character, because she had to readjust to a new playing style. Even though Ri Bun-huis left-handed style was not well-known, Bae wanted to maintain a high level of authenticity.   Unlike Has direct training sessions with her real-life counterpart, Bae was unable to contact or meet with Ri Bun-hui, having to rely on videos and tutoring instead. 
 Andong Universitys indoor stadium, where a July heatwave was multiplied by the heat from lighting, raising the temperature to over 120 degrees Fahrenheit inside. Despite these hellish conditions, the actors continued to perform selflessly. There were constant minor injuries from slips and falls, as well as from performing difficult moves, and physiotherapists had to be on standby on the set at all times. Shooting the final game against the Chinese team was a culmination of the training and difficult photography, bringing all the players to genuine tears.

Bae and Ha found it refreshing to play strong women in the film, without a leading man by their side. Bae said, "It is so rare in films nowadays for actresses to be able to tell a story about women in films, with a cast full of other actresses. Even though the film touches upon themes of a divided Korea through a sports tournament, it is ultimately about the friendship and love between two women." 

One of the more touching moments in the film takes place towards the end, when Hyun gives Ri a ring as a token of friendship as Ri sits on the bus that is headed back to North Korea. In reality, Hyun said that she gave the ring to Ri, complete with an engraving of Hyun and Ris names inside,  on the last day of their stay in Japan. "I told her that I wanted her to remember me," said Hyun. She added that after the 1991 tournament, she saw Ri one last time in 1993, as a competitor representing North Korea. "I still feel very emotional when I think of her."  

==Box office== The Avengers Dark Shadows, it was viewed by 1.2 million viewers 10 days after its release. It eventually reached a total of 1,872,683 admissions, with a gross of   (or  ).  
 Chiba on April 20, 2012, to thank the Korean people in Japan who cheered for Koreas victory in 1991.  A year later, the film also received a theatrical release in Japan on April 20, 2013, under the title Hana - miraculous 46 days. 

== Awards and nominations == 2012 Blue Dragon Film Awards  
*Nomination - Best New Actress - Han Ye-ri
 2013 Baeksang Arts Awards
*Best New Actress - Han Ye-ri   
*Nomination - Best New Director - Moon Hyun-sung
*Nomination - Best Actress - Ha Ji-Won

==References==
 

==External links==
*    
*   at Naver  
*   at CJ Entertainment
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 