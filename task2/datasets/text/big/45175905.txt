The Rookie's Return
{{Infobox film
| name           = The Rookies Return
| image          = The Rookies Return (1920) - 1.jpg
| alt            = 
| caption        = Newspaper ad Jack Nelson
| producer       = Thomas H. Ince
| screenplay     = Archer MacMackin
| starring       = Douglas MacLean Doris May Frank Currier Leo White Kathleen Key Elinor Hancock
| music          = 
| cinematography = Bert Cann 
| editor         = 
| studio         = Thomas H. Ince Corporation Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent comedy Jack Nelson and written by Archer MacMackin. The film stars Douglas MacLean, Doris May, Frank Currier, Leo White, Kathleen Key, and Elinor Hancock. The film was released on December 26, 1920, by Paramount Pictures.     A copy of the film is in the Library of Congress. 

The film was advertised as being a sequel to 23 1/2 Hours Leave (1919), which also starred MacLean and May, but their characters had different names in that film.

==Plot==
 

==Cast==
*Douglas MacLean as James Stewart Lee
*Doris May as Alicia
*Frank Currier as Dad
*Leo White as Henri
*Kathleen Key as Gloria
*Elinor Hancock as Mrs. Radcliffe
*William Courtright	as Gregg Frank Clark as Tubbs
*Aggie Herring as Mrs. Perkins
*Wallace Beery as François Dupont 

== References ==
 

== External links ==
*  
*  at silenthollywood.com

 
 
 
 
 
 
 
 
 