Ju-on: White Ghost
{{Infobox film
| name           = Ju-on: White Ghost
| image          = Ju-on - White Ghost poster.png
| image_size     = 
| caption        =
| director       = Ryuta Miyake
| producer       = Ryuta Miyake
| writer         = Ryuta Miyake
| starring       = Akina Minami   Hiroki Suzuki   Mihiro   Aimi Nakamura   Marika Fukunaga   Chie Amemiya
| music          = 
| cinematography = 
| editing        = 
| studio         = Kadokawa Pictures
| distributor    = Kadokawa Pictures
| released       =    
| runtime        = 61 Minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} The Saeki cameo being the only exception.

==Plot==
White Ghost is divided into eight segments in the following order: Fumiya (文哉), Kashiwagi (柏木), Akane (あかね), Isobe (磯部), Chiho (千穂), Mirai (未来), Yasukawa (安川), and Atsushi (篤).
 sexually abuse Mirai. After failing from his test two days before Christmas, he murders all of his family, takes Mirais head (whom he decapitates) to a forest, and records his last words with Mirai on a cassette before committing suicide by hanging. The cassette becomes cursed, killing all of their listeners, and, according to Det. Yasukawa, unable to be discarded or destroyed anywhere. The next day, Fumiya, a delivery boy, delivers a Christmas cake to the family, but is tormented by the ghosts of the victims. Traumatized by the incident, Fumiya is comforted by his girlfriend, Chiho, who celebrates Christmas Eve in their apartment. However, Fumiya sees Chiho as the grandmothers ghost and kills her by stabbing her with a knife. 
 Kokkuri with her friends Mayumi and Yuka. Mayumi suggests calling the spirit of Akanes father, a taxi driver who disappeared seven years before after driving a "family killer" (Atsushi) to a forest. When they play, the board spells "Mirai", and after seeing Mirais ghost, Akane quickly leaves, leaving her friends puzzled. While Mayumi goes back to the class to grab her things, Yuka goes to the restroom and encounters the grandmothers ghost. Walking near the former Isobe household, Akane remembers that she did not help Mirai when she was abused, and keeps seeing things due to her regret. When she finally comes back home, Akane meets Mirais ghost, who leaves her bear keychain as a keepsake.

==Cast==
*Akina Minami as Akane Kashiwagi, a teenager who has guilt over her friend, Mirais abuse and murder.
**Natsuki Kasa as Young Akane
*Hiroki Suzuki as Fumiya Hagimoto, the delivery boy assigned to deliver the Isobes Christmas cake.
*Mihiro as Chiho Tanemura, Fumiyas girlfriend.
*Aimi Nakamura as Junko Isobe, the matriarch of the Isobe household and Mirais mother.
*Marika Fukunaga as Yuka Kanehara, Akanes friend.
*Chie Amemiya as Mayumi Yoshikawa, Akanes friend.
*Akiko Hoshino as Haru Isobe, Mirais grandmother and the titular White Ghost.
*Takuji Suzuki as Hideki Yasukawa, a detective who investigates the Isobe murder case.
*Tsuyoshi Muro as Atsushi Isobe, Mirais uncle and the instigator of the Isobe murder.
*Ichirōta Miyakawa as Hajime Kashiwagi, Akanes father.
*Chinami Iwamoto as Mirai Isobe, Akanes friend who has close relationship with her uncle, Atsushi.
*Shūsei Uto as Toshio Saeki

==Release==
The movie was released alongside its co-installment   on DVD in the UK on April 26, 2010,  and was released onto DVD and Blu-ray in the US on May 17, 2011.  

==References==
 

== External links ==

*  
 
 

 
 
 
 
 
 
 
 
 
 
 

 