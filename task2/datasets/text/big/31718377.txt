Pitar Ason
 
{{Infobox film
| name = Pitar Ason
| image =
| caption = 
| director =F I Manik
| writer = Nipun Amin Khan Razzak
| producer =Ami Boni Kothachotro
| distributor =
| cinematography =Mostafa Kamal
| editing =Thoipak hossian
| released = 2006
| country        = Bangladesh
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| runtime = Bengali
| music = Allauddin ali
| website        =
}}
Pitar Ason ( ) is a 2006 Bangladeshi Bengali language film directed by F I Manik.  It stars Shakib Khan, Apu Biswas, Nipun Akter, Amin khan, Razzak and many more. Its a triangle love story with family drama based film. it released  all over Bangladesh and Block-Buster hit at the box-office.

==Cast==
* Shakib Khan as...Salim Ahmed
* Apu Biswas Nipun as.. Raika
* Amin Khan
* Razzak
* Suchorita
* Aliraj
* Nasrin
* Don
* Kazi Hayat
* Dipjol

==Crew==
* Producer: Ami Boni Kothachoitro
* Story: Manik
* Screenplay: Manik
* Director: F I Manik
==Technical details==
* Format: 35 MM (Color)
* Running Time: 144 Minutes Bengali
* Country of Origin: Bangladesh
* Date of Theatrical Release: 2006
* Year of the Product: 2006
* Technical Support: Bangladesh Film Development Corporation (BFDC)
==Music==
{{Infobox album
| Name = Na Bolona
| Type = Album
| Artist = Allauddin ali
| Cover =
| Background = Gainsboro |
| Released = 2006 (Bangladesh)
| Genre = Soundtrack/Filmi
| Length =
| Label =
| Producer =CD Choich
| Reviews =
| Last album = 
| This album =  Pitar Ason  (2006)
| Next album = 
|}}
Pitar Ahson films music directed by Bangladeshi famous music director Allauddin ali.
===Soundtrack===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Trac !! Song !! Singer’s !! Performer’s !! Note’s
|- 1
|Ami Aka Boro Aka 
| Shakib Khan, Apu Biswas and Nipun
|
|- 2
|Surjar Alo Monir Khan
| All Character Salim And Raikas engagement days song
|- 3
|Praner Choio
| Shakib Khan and Nipun
|
|- 4
|Tumare prame ami
| Amin Khan and Apu Biswas
|
|- 5
|Sogaar Gurjon
|
| Shakib Khan and Apu Biswas
|
|-
 

==References==
 

 
 
 
 
 
 


 
 