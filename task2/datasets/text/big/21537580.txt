Court Martial (1959 film)
 
{{Infobox film
| name           = Court Martial
| image          = 
| caption        = 
| director       = Kurt Meisel
| producer       = Gero Wecker
| writer         = Will Berthold Heinz Oskar Wuttig
| starring       = Karlheinz Böhm
| music          = 
| cinematography = Georg Krause
| editing        = Wolfgang Wehrum
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Court Martial ( ) is a 1959 West German drama film directed by Kurt Meisel. It was entered into the 1959 Cannes Film Festival.   

==Cast==
* Karlheinz Böhm – Oberleutnant Düren Christian Wolff – Fähnrich Stahmer
* Klaus Kammer – Maat Klaus Hinze Hans Nielsen – Dr. Wilhelmi, Verteidfiger
* Charles Régnier – Schorn, Vorsitzender des Kriegsgerichtes (as Charles Regnier)
* Werner Peters – Brenner, Kriegsgerichtsrat
* Carl Wery – Stahmer sen.
* Herbert Tiede – Paulsen, Kapitän zur See
* Raidar Müller-Elmau – Koch, Brenners Adjutant (as Raidar Müller)
* Martin Benrath – Funk-Offizier Maiers
* Reinhard Kolldehoff – Feldwebel
* Berta Drews – Frau Willmers
* Edith Hancke – Frl. Wehner
* Horst Naumann
* Robert Meyn – Admiral Zirler

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 