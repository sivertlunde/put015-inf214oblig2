Le Corsaire (film)
{{Infobox film
| name           = Le Corsaire
| image          =
| caption        =
| director       = Marc Allégret
| producer       = 
| writer         = Marc Allégret Marcel Achard
| based on = the play by Marcel Achard
| starring       = Michèle Alfa Marcel André Charles Boyer Louis Jourdan 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  1939
| runtime        = 
| country        = France
| awards         =
| language       = French
| budget         = 
| preceded_by    =
| followed_by    =
}}
Le Corsaire ("The Pirate") is an unfinished 1939 French film. It marked the screen debut of Louis Jourdan.

==Plot==
Some actors put on a play about a pirate, and find themselves encountering similarities from the play in their own lives.
==Production==
Charles Boyer returned from Hollywood to appear in the movie which was filmed at Studios de la Victorine, Nice, August - September 1939. However production ceased on the declaration of war and Boyer returned to America.  The film was never completed, although some footage of it was later released.  

A documentary about the making of the movie was released in 1995. 

==References==
 

==External links==
*  at IMDB
 
 
 
 
 
 

 