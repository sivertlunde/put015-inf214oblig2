La Vie de Bohème (1945 film)
{{Infobox film
| name           = La Vie de bohème
| image          =
| caption        =
| director       = Marcel LHerbier
| producer       = 
| writer         = Robert Boissy Nino Frank
| based on = 
| starring       = María Denis Giselle Pascal Louis Jourdan 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1945 
| runtime        = 
| country        = France
| awards         = French
| budget         = 
| preceded_by    =
| followed_by    =
}}
La Vie de bohème is a French-Italian drama film directed by Marcel LHerbier. It was shot in Rome in 1942.

==Cast==
*Maria Denis: Mimì
*Louis Jourdan: Rodolfo
*Gisèle Pascal: Musetta
*Alfred Adam: Alexandre Schaunard
*Louis Salou: Colline
*André Roussin: Marcello
*Jean Parédès: Il visconte

==External links==
*  at louisjourdan.net
*  at IMDB

 

 
 
 
 
 


 
 