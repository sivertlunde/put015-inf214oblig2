Rainbow Island (film)
{{Infobox film
| name           = Rainbow Island
| image          = 
| caption        = 
| director       = Ralph Murphy
| producer       = E.D. Leshin 
| screenplay     = Arthur Phillips Walter DeLeon
| story          = Seena Owen Barry Sullivan Forrest Orr Anne Revere Reed Hadley
| music          = Roy Webb
| cinematography = Karl Struss
| editing        = Arthur P. Schmidt
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States English
| budget         =
| gross          = 
}} musical comedy Barry Sullivan, Forrest Orr, Anne Revere and Reed Hadley. The film was released on SEptember 15, 1944, by Paramount Pictures.  

==Cast==
*Dorothy Lamour as Lona
*Eddie Bracken as Toby Smith
*Gil Lamb as Pete Jenkins Barry Sullivan as Ken Masters
*Forrest Orr as Dr. Curtis
*Anne Revere as Queen Okalana
*Reed Hadley as High Priest Kahuna
*Marc Lawrence as Alcoa
*Adia Kuznetzoff as Executioner
*Olga San Juan as Miki
*Elena Verdugo as Moana 

==Production==
Dorothy Lamour was reluctant to play the lead. Paramount were going to use contract player Yvonne De Carlo instead but then Lamour changed her mind, and De Carlo was relegated to a bit player. "You might have seen Gil Lamb chasing me through the bushes in it," said De Carlo later. THE UNVEILING OF YVONNE (SALOME) DE CARLO: Herewith Some Early Film Entries in the Easter Week Sweepstakes
By THOMAS M. PRYOR. New York Times (1923-Current file)   25 Mar 1945: X3.  

==References==
 

==External links==
*  at IMDB
*  at New York Times
*  at TCMDB

 

 
 
 
 
 
 
 
 
 
 