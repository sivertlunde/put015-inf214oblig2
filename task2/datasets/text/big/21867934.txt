RoachTrip
{{Infobox Film
| name = RoachTRip
 | image = 
 | caption = 
 | director = Eric "Roach" Denis
 | producer = EyeSteelFilm Roach  and Smash
 | cinematography = 
 | distributor =  EyeSteelFilm
 | released = 2003 (Canada)
 | runtime = 46 min. English
  }}
 Canadian documentary documentary about two punk subculture|punks, Roach and his friend Smash down the invisible punk highway across Canada. It captures their goal to escape the streets of Montreal as they cross   to reach the "promised land" of British Columbias Okanagan Valley.

The film is an   where Eric Denis contributed with his RoachCam.  The film RoadTrip became the directorial debut film of Eric "Roach" Denis.

==Synopsis==
RoachTrip starts by waving the black flag in the House of Commons and ends wandering lost in
the desert. In between is a hardcore odyssey down Canadas invisible punk highway in search of something pure and clean. Roach and Smash,  two inseparable street buddies from Montreal introduced in 2002 documentary "S.P.I.T." earlier, ping-pong across the country. The same couple are now set loose with RoachCam and the loose mandate of filming the annual Punk migration across Canada.

Camping, fishing, and fruit-picking their way through summer, these two punks are never quite able to leave the city behind. Just beyond the paradise of the Okanagan lies the lure of the streets of Vancouver. 

==Festivals==
The documentary premiered at the FCMM Festival and also screened at the Global Visions Festivals in Edmonton, Alberta, Canada. The film is scheduled for New York Underground Film Festival (aka NYUFF) (New York) NEMO (Paris) and the Images du Nouveau Monde (Quebec City).

==See also==
*Punk the Vote!

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 