Magical Mystery Tour (film)
 
 
 
{{Infobox television film
| name           = Magical Mystery Tour
| image          = MMT poster.jpg
| caption        = The 1988 VHS release cover art
| border         = yes
| director       = The Beatles Bernard Knowles (uncredited)
| writer         = John Lennon Paul McCartney George Harrison Ringo Starr
| narrator       = John Lennon
| starring       = John Lennon Paul McCartney George Harrison Ringo Starr Jessie Robins Vivian Stanshall Mal Evans Ivor Cutler Derek Royle Victor Spinetti | producer       = John Lennon Paul McCartney George Harrison Ringo Starr Gavrik Losey Dennis ODell
| music          = The Beatles The Bonzo Dog Doo-Dah Band Shirley Evans (accordionist)
| cinematography = Daniel Lacambre Richard Starkey M.B.E. Roy Benson
| studio         = Apple Corps BBC
| country        = United Kingdom English
| network        = BBC1
| distributor    = New Line Cinema (US)
| released       =  
| runtime        = 52 minutes
| language       = English
}}
Magical Mystery Tour is a 52-minute long British  , and in select theatres worldwide in 2012 by Apple Films.    

==Plot== Panorama I coach, focusing mostly on Mr Richard Starkey (Ringo Starr) and his recently widowed Aunt Jessie (Jessie Robins). Other group members on the bus include the tour director, Jolly Jimmy Johnson (Derek Royle), the tour hostess, Miss Wendy Winters (Mandy Weet), the conductor, Buster Bloodvessel (Ivor Cutler), and the other Beatles (John Lennon, Paul McCartney and George Harrison).

During the course of the tour, "strange things begin to happen" at the whim of "four or five magicians", four of whom are played by the Beatles themselves and the fifth by the bands long-time road manager Mal Evans.

During the journey, Ringo and his Aunt Jessie argue continually. Aunt Jessie begins to have daydreams of falling in love with Buster Bloodvessel, who displays increasingly eccentric and disturbing behaviour. The tour involves several strange activities, such as an impromptu race in which each of the passengers employs a different mode of transportation (some run, a few jump into cars, a group of people have a long bike they pedal, while Ringo ends up beating them all with the bus). In one scene, the tour group walk through what appears to be a British Army recruitment office and are greeted by the army drill sergeant (Victor Spinetti). (McCartney appears briefly as "Major McCartney", on whose desk rests a sign reading "I you WAS".) The sergeant, shouting incomprehensibly, appears to instruct the assembled onlookers on how to attack a stuffed cow.

The tour group also crawl into a tiny tent in a field, inside which is a projection theatre. A scene in a restaurant shows a waiter (played by Lennon) repeatedly shovelling spaghetti onto the table in front of Aunt Jessie, while arriving guests step out from a lift and walk across the dining tables. The film continues with the tours male passengers watching a strip show (Jan Carson of the Raymond Revuebar). The film ends with the Beatles dressed in white tuxedos, highlighting a glamourous old-style dance crowd scene, accompanied by the song "Your Mother Should Know".
 Death Cab For Cutie" sung by Stanshall.

==Initial idea==
In The Beatles Anthology, Lennon states that "if stage shows were to be out, we wanted something to replace them. Television was the obvious answer." Beatles, the: Beatles Anthology, p. 272. Chronicle Books, 2000.  Most of the band members have said that the initial idea was McCartney’s, although he stated, “I’m not sure whose idea Magical Mystery Tour was. It could have been mine, but I’m not sure whether I want to take the blame for it! We were all in on it&nbsp;– but a lot of the material at that time could have been my idea.”  Prior to the movie, McCartney had been creating home movies and this was a source of inspiration for Magical Mystery Tour. 

==Production==
The film was unscripted and shooting proceeded on the basis of a mostly handwritten collection of ideas, sketches and situations, which McCartney called the "Scrupt". Magical Mystery Tour was ultimately the shortest of all Beatles films, although almost ten hours of footage was shot over a two-week period. The core of the film was shot between 11 September and 25 September 1967. Mark Lewisohn, The Complete Beatles Chronicle (London: Pyramid Books, Hamlyn, 1992, ISBN 0-600-61001-2), p. 267 

The next eleven weeks were mostly spent on editing the film from ten hours to 52 minutes. Scenes that were filmed but not included in the final cut include:
* A sequence where ice cream, fruit and lollipops were sold to the Beatles and other coach passengers;
* Lennon, McCartney, Harrison and Starr each looking through a telescope;
* Happy Nat the Rubber Man (Nat Jackley) chasing women around the Atlantic Hotels outdoor swimming pool, a sequence which Lennon directed; Mark Lewisohn, The Complete Beatles Chronicle (London: Pyramid Books, Hamlyn, 1992, ISBN 0-600-61001-2), p. 264 
* Mr Bloodvessel performing Im Going in a Field; and Traffic performing Here We Go Round the Mulberry Bush".

Much of Magical Mystery Tour was shot in and around   cadets can be seen marching in some scenes, and during "I Am the Walrus" a RAF Avro Shackleton is seen orbiting the group.
  Paul Raymonds Raymond Revuebar in Londons Soho district, and the sequence for "The Fool on the Hill" was shot around Nice, in the south of France.

The Magical Mystery Tour movie was made, but the hoped-for "magical" adventures never happened. During the filming, an ever greater number of cars followed the hand-lettered bus, hoping to see what its passengers were up to, until a running traffic jam developed. The spectacle ended after Lennon angrily tore the lettering off the sides of the bus.
 Hayes in 1967. The Hard Rock Cafe acquired the coach in 1988, and the vehicle is now completely Automotive restoration|refurbished.  In the race, Starr himself drives the bus around the airfield racetrack.

==Script== without a script and were disappointed by the lack of one. 

==Criticism==
The British publics reaction to the film was scathing. Magical Mystery Tour initially aired in the United Kingdom as a made-for-television film on BBC1.   a few days later, but there were only about 200,000 colour TV receivers in the UK at the time. 

Hunter Davies, the bands official biographer, said that "It was the first time in memory that an artist felt obliged to make a public apology for his work."  McCartney later spoke to the press, saying: "We dont say it was a good film. It was our first attempt. If we goofed, then we goofed. It was a challenge and it didnt come off. Well know better next time."  He also said, "I mean, you couldn’t call the Queen’s speech a gas, either, could you?"   With the passage of time, however, McCartney changed his view of the production, saying: "Looking back on it, I thought it was all right. I think we were quite pleased with it." He also noted in The Beatles Anthology DVD that the film features the bands only video performance of "I Am the Walrus".

The film carries a 58% approval rating at the review aggregator website Rotten Tomatoes, based on twelve professional reviews.  In The Electric Kool-Aid Acid Test, Tom Wolfe notes the similarity between Magical Mystery Tour and the exploits of Ken Kesey and the Merry Pranksters. In 1978 the film was parodied by the Rutles in their Tragical History Tour, "a self-indulgent TV movie about four Oxford history professors on a tour around Rutland tea-shops".

==Distribution==
 The poor critical reaction to the telecast soured American television networks from acquiring rights to the film, while its one-hour running length made it commercially unviable for theatrical release. 

In his Diaries 1969–1979: The Python Years, Michael Palin reveals that the Monty Python team considered showing the film as a curtain-raiser to their 1975 film Monty Python and the Holy Grail. They received permission from all four Beatles to view the film, and did so at Apple on 10 January 1975. Although the Pythons were interested, the idea did not go ahead. 

The film had its first US presentation in 1968 at the Fillmore East in New York City on Sunday, 11 August, shown at 8 and 10 pm, as part of a fundraiser for the Liberation News Service. However, it was not seen in commercial theatres in the US until 1974, when New Line Cinema acquired the rights for limited theatrical and non-theatrical distribution.  It first played on American television in the 1987 as part of a syndicated release.

==Restoration==
The critical reception in 1967 had been so poor that no one had properly archived a negative, and these later re-release versions had to be copied from poor-quality prints.  By the end of the 1980s, MPI, through rights holder Apple Corps, had released the movie on video, and a DVD release followed many years later.
 documentary on its making.  Both were shown in the United States as part of Great Performances on PBS ten weeks later on 14 December.  
 double 7" vinyl EP.

The 2012 remastered Magical Mystery Tour DVD entered the Billboard Top Music Video chart at number 1 for the week ending 27 October 2012. 

==Songs==
 
The songs in order of their use in the movie:
 Magical Mystery Tour"
#"The Fool on the Hill"
#"She Loves You" (played on a fairground organ as part of the general medley of background music during the impromptu race)
#"Flying (The Beatles song)|Flying"
#"All My Loving" (orchestrated, as background music)
#"I Am the Walrus"
#"Jessie’s Dream" (instrumental piece, not released on any audio recording)
#"Blue Jay Way"
#"Penny Lane" Death Cab for Cutie" performed by the Bonzo Dog Doo-Dah Band
#"Your Mother Should Know" Magical Mystery Tour" (part, once more)
#"Hello, Goodbye" (part, finale played over end credits)

==Home video release history==
USA
{|class="wikitable"
|rowspan="1"|Year
|rowspan="1"|Company
|rowspan="1"|Format(s)
|rowspan="1"|Comments
|- 1978
|Media-Home Entertainment
|VHS/Betamax Originally taken off the market due to a successful lawsuit filed in 1980,  Media and Northern later reached an agreement for its re-release one year later. Unique Identification info:  Title song has a unique voice track "roll-up roll-up" introduction by McCartney and first scenes of the bus zooming in and out show stars with scratchy lines above, to show a falling effect.  Overall film has washed-out colour and audio that is not very high quality.
|- 1988
|Video Collection/Apple VHS and Laserdisc With a digitally re-mixed and remastered soundtrack by producer George Martin. Unique Identification info:  Title song still has the unique voice track "roll-up roll-up" introduction (now in clean remixed stereo) and first scenes of the bus zooming in and out still show stars with scratchy lines above to show a falling effect (later releases change this).  Overall film has much sharper colour and remixed Dolby Stereo audio with nice separation and quality. This releases collector importance is the clean stereo version of the title song with the unique voice track intro - which in future releases will no longer be used.
|- 1992
|MPI/Apple Laserdisc
|
|- 1997
|MPI/Apple DVD
|First DVD release of Magical Mystery Tour.  Unique Identification info:  Title song now uses the standard album song version with its standard "roll-up roll-up" introduction, and first scenes of the bus zooming in and out with falling stars has been removed and replaced with a looping of the intro graphic.  Overall film video is the same cleaned up 1988 VHS release and the audio (after the title song mix change) retains the same standard stereo remixing as well (no surround 5.1 mixes).   
|- 2003
|Avenue One DVD
|Counterfeit|Bootleg of the MPI DVD.
|- 2012
|Apple DVD
|
|- 2012
|Apple
|Blu-ray First Blu-ray release of Magical Mystery Tour.  Unique Identification info:  Title song still uses the standard album song version, with its familiar "roll-up roll-up" introduction, and first scenes of the bus zooming in and out with falling stars has been restored, yet the falling stars do not show the scratched vertical lines above them.  Overall film video is cleaned up to 2012 technology standards and the audio has been remixed to include a new 5.1 surround sound mix in various formats.  This version also includes new bonus features such as a Directors Commentary by Paul McCartney.   
|}
UK
{| class="wikitable"
|rowspan="1"|Year
|rowspan="1"|Company
|rowspan="1"|Format(s)
|rowspan="1"|Comments
|- 1980s
|Empire Films VHS
|
|- 1988
|MPI/Apple VHS and Laserdisc With a digitally re-mixed and remastered soundtrack by George Martin
|- 1997
|MPI/Apple DVD
|First DVD release of Magical Mystery Tour
|- 2012
|Apple DVD
|
|- 2012
|Apple
|Blu-ray First Blu-ray release of Magical Mystery Tour

|}

==References==
 
 

==External links==
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 