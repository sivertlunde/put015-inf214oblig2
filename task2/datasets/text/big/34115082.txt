Strawberry Cliff
 
 
{{Infobox film
| name           = Strawberry Cliff
| image          = Strawberry Cliff poster.jpg
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 贖命
| simplified     = 赎命
| pinyin         = Shú Mìng
| jyutping       = Suk6 Ming6}}
| director       = Chris Chow
| producer       = Lu Cien Hioe Lee Chi Ngai Tang Hong Keung
| writer         = Chris Chow
| starring       = Eason Chan Leslie-Anne Huff Roy Werner Kimberly Estrada
| music          = Youki Yamamoto
| cinematography = Teruhisa Yoshida
| editing        = Daniel Martinico
| studio         = Irresistible Films Hong Kong Film Development Fund
| distributor    = Edko Films
| released       =  
| runtime        = 118 minutes
| country        = Hong Kong
| language       = English
| budget         = 
| gross          =
}}
Strawberry Cliff is a 2011 Hong Kong mystery film written and directed by Chris Chow, who has participated in writing scripts for films such as  . Film stars Eason Chan and Leslie-Anne Huff and was filmed in both Hong Kong and Los Angeles. This film also features Eason Chan in his first English language role and Chris Chow in his directorial debut.

==Plot==
Kate has a supernatural power, that with one look, she can tell a persons time of death without any mistake. One day in a restaurant in Seattle, she meets Jason who is going to die a few hours later. She made a deal with Jason that if he reincarnates to his next life, he will tell her about it and let her know what is reincarnation.

That night, as Kate predicted, Jason died and that last moment before he dies, he saw a horrible image.

Days later, Kate receives a "reincarnation" phone call. Frightened, she follows the phone callers instruction and leaves from Los Angeles to Hong Kong and there she meets Daren. Daren is a bartender, because he has an "incomplete spirit", he feels depressed. Darren and Kate investigate about the journey of reincarnation and found out the truth of the horrible image that Jason saw before his death.

Kate finds out that her father, the first person whose death she ever predicted, was the cause of all the deaths occurring around her. She was stricken with health problems since her birth and was never supposed to live. So her father, once dead, has been killing people around her to preserve Kates life. "Others lives arent as important as yours" he would say.

The final scene shows Jean, the third person who shares a mind with Jason and Darren, finally meeting Kate. Jean tells her the tale of the strawberry cliff, emphasizing on how sweet death is.

==Cast==
*Eason Chan as Darren
*Leslie-Anne Huff as Kate
*Roy Werner as Kates father
*Kimberly Estrada as Maggie Johnston
*Anthony Chaput as Jason
*Antonella Monceau as Jeanne
*David Alan Graf as Doctor
*Eli Klien as Young Jason
*Brian Maslow as Travel Agent
*David Oxley as George
*Lee Perkins as Grouchy Customer
*Shawna Sutherland as Gabriella
*Bogdan Szumilas as Homeless man

==External links==
*  
*  
*  
*  
*  
*  
*   

 
 
 
 
 
 
 
 
 
 