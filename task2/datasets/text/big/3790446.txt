Yesterday, Today and Tomorrow
 
 Yesterday, Today & Tomorrow}}
{{Infobox film
| name            = Yesterday, Today and Tomorrow
| image           = Yesterday, Today and Tomorrow film poster.jpg
| caption         = original movie poster
| director        = Vittorio De Sica
| producer        = Carlo Ponti Joseph E. Levine
| writer          = Billa Billa Eduardo De Filippo Alberto Moravia Isabella Quarantotti Cesare Zavattini
| starring        = Sophia Loren Marcello Mastroianni
| music           = Armando Trovajoli
| cinematography  = Giuseppe Rotunno
| editing         = Adriana Novelli
| distributor     = Embassy Pictures Corporation
| released        =  
| runtime         = 118 minutes
| country         = Italy
| language        = Italian
| budget          =
| gross           = $10 million (USA)
|}} comedy anthology film by Italian director Vittorio de Sica.    It stars Sophia Loren and Marcello Mastroianni. The film consists of three short stories about couples in different parts of Italy. The film won the Academy Award for Best Foreign Language Film at the 37th Academy Awards.   

==Plot==
===Adelina of Naples===
Set in the poorer Naples of 1953, Adelina (Loren) supports her unemployed husband Carmine (Mastroianni) and child by selling black market cigarettes.  When she doesnt pay a fine, her furniture is to be repossessed.  However her neighbors assist her by hiding the furniture. A lawyer who lives in the neighborhood advises Carmine that as the fine and furniture is in Adelinas name, she will be imprisoned.  However, Italian law stipulates that women cannot be imprisoned when pregnant or within six months after a pregnancy.  As a result Adelina schemes to purposely stay pregnant.  After seven children, Carmine is seriously exhausted and Adelina must make the choice of being impregnated by their mutual friend Pasquale (Aldo Giuffrè) or be incarcerated.

She finally chooses to be incarcerated, and the whole neighborhood gathers money to free her and petition for her pardon, which finally comes and she is reunited with her husband Carmine and the children.

===Anna of Milan===
Anna (Loren dressed by Christian Dior) is the wife of a mega-rich industrialist who has a lover named Renzo (Mastroianni).  Whilst driving together in her husbands Rolls-Royce Limited|Rolls-Royce, Anna must determine which is the most important to her happiness – Renzo or the Rolls. Renzo rethinks his infatuation with Anna when she expresses no concern when they nearly run over a child, and end up crashing the Rolls-Royce.

She is infuriated by the damage to her Rolls-Royce, and ends up getting another passing driver to take her home, leaving Renzo on the road.

===Mara of Rome===
Mara (Loren) works as a prostitute from her apartment, servicing a variety of high class clients including Augusto (Mastroianni), the wealthy, powerful and neurotic son of a Bologna industrialist.

Maras elderly neighbours grandson visiting them is a handsome and callow young man studying for the priesthood but not yet ordained who falls in love with Mara.  To the shrieking dismay of his grandmother, the young man wishes to leave the clergy to be with Mara or to join the French Foreign Legion if Mara rejects him.  Mara vows to set the young man on the path of righteousness back to the seminary and enlists the reluctant Augusto.  Mara provides a strip tease at the climax of the film.

==Cast==
* Sophia Loren – Adelina Sbaratti / Anna Molteni / Mara
* Marcello Mastroianni – Carmine Sbaratti / Renzo / Augusto Rusconi
* Aldo Giuffrè – Pasquale Nardella (segment "Adelina")
* Agostino Salvietti – Dr. Verace (segment "Adelina")
* Lino Mattera – Amedeo Scapece (segment "Adelina")
* Tecla Scarano – Veraces sister (segment "Adelina")
* Silvia Monelli – Elivira Nardella (segment "Adelina")
* Carlo Croccolo – Auctioneer (segment "Adelina")
* Pasquale Cennamo – Chief Police (segment "Adelina")
* Tonino Cianci – (segment "Adelina") (as Antonio Cianci)
* Armando Trovajoli – Giorgio Ferrario (segment "Anna")
* Tina Pica – Grandmother Ferrario (segment "Mara")
* Gianni Ridolfi – Umberto (segment "Mara") (as Giovanni Ridolfi)
* Gennaro Di Gregorio – Grandfather (segment "Mara")

==Awards== Academy Award Best Foreign Language Film  
* 1965 BAFTA Award for Best Foreign Actor – Marcello Mastroianni
* 1964 Golden Globes – Samuel Goldwyn Award - nomination
* 1964 David di Donatello Awards – David for Best Production – Carlo Ponti

==See also==
* List of submissions to the 37th Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 