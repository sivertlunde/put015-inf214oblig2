The Battle of Sutjeska (film)
{{Infobox film
| name           = The Battle of Sutjeska
| director       = Stipe Delić
| producer       = Nikola Popović 
| writer         = Branimir Šćepanović Sergei Bondarchuk Wolf Mankowitz (uncredited) Miljenko Smoje (uncredited) Orson Welles (uncredited)
| starring       = Richard Burton Irene Papas Günter Meisner
| music          = Mikis Theodorakis
| cinematography = Tomislav Pinter
| editing        = Vojislav Bjenjas Roberto Perpignani
| distributor    = American International Pictures
| released       =  
| runtime        = 128 minutes
| country        = Yugoslavia English German German
| budget         = 
| gross          = 
}} Yugoslav Partisan Best Foreign Language Film at the 46th Academy Awards, but was not accepted as a nominee.  It was also entered into the 8th Moscow International Film Festival where it won a Special Prize.   

==Plot==
Nazi-occupied Bosnia and Herzegovina, 1943; under the faithful leadership of   in southeastern Bosnia.

Various people are caught up in the fighting, such as a Dalmatian who lost all of his children during the war. As the fighting intensifies, the story and the scenes are drawn more and more into the colossal battle as both sides are forced into a conflict that can only be described as a living hell. The scenes are interlaced between general battle rage and individual, personal fates and agonies of several main characters, from supreme commanders to ordinary soldiers.

The offensive finally ends as a failure for the Axis armies, but the Partisans are not in a mood to celebrate – they suffered devastating losses. Still, they marched on.

==Cast==
* Richard Burton as Josip Broz Tito Sava Kovačević
* Bata Živojinović as Nikola
* Miroljub Lešo as Boro
* Irene Papas as Boros Mother
* Milena Dravić as Vera
* Bert Sotlar as Barba
* Boris Dvornik as Ivan
* Rade Marković as Radoš
* Ljubiša Samardžić as Stanojlo 
* Milan Puzic as Member of the Partisan Headquarters
* Nikola-Kole Angelovski as Stanojlos Friend (as Kole Angelovski)
* Stole Aranđelović as The Priest
* Relja Bašić as Capt. Stewart
* Petar Banićevic as Capt. Deacon
* Günter Meisner as Gen. Rudolf Lüters
* Anton Diffring as Gen. Alexander Löhr
* Michael Cramer as Col. Wagner

==See also==
* List of submissions to the 46th Academy Awards for Best Foreign Language Film
* List of Yugoslav submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 