The Future of Food
 
 
{{Infobox Film 
| name        = The Future of Food
| image       = Cover-dvd-thefutureoffood.jpg
| image_size  =
| caption     = DVD cover of The Future of Food
| director    = Deborah Koons Garcia
| writer      = Deborah Koons Garcia
| producer    = Deborah Koons Garcia Catherine Butler
| distributor = Lily Films
| released    = May 30, 2004
| runtime     = 88 minutes
| country     = United States English
|}}
 genetically engineered foods that have been sold in grocery stores in the United States for the past decade. In addition to the US, there is a focus on Canada and Mexico.

It voices the opinions of farmers in disagreement with the food industry, and details the impacts on their lives and livelihoods from this new technology, and the market and political forces that are changing what people eat. The farmers state that they are held legally responsible for their crops being invaded by "company-owned" genes. The film generally opposes the patenting of living organisms, and describes the disappearance of traditional cultural practices.
 ecological disasters biological diversity. For example, the local varieties of Mexican corn are being replaced by subsidized US corn.
 Monsanto Company.

The film was written and directed by Deborah Koons Garcia, produced by Catherine Butler and Koons Garcia, and premiered on September 14, 2005 at Film Forum in New York City to a full house. It has since been released on DVD in both NTSC and PAL formats.  
==See also==
* 
*Deconstructing Dinner
*The Jungle A Place at the Table
*Food, Inc.

== External links ==
* 
* 
*  (YouTube)
* 
* 
* 
* 
* 

== References ==
 

 

 
 
 
 
 
 
 
 