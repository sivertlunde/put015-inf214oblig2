C.I.D. (1955 film)
{{Infobox Film
| name           = C.I.D.
| image          = C.I.D. (1956).png
| caption        = 
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = P. Subramaniam  (Merryland Studio|Merryland) 
| writer         = T. N. Gopinathan Nair
| based on       = 
| starring       = Prem Nazir Miss Kumari Kottarakkara Sreedharan Nair Kumari Thankam
| music          = Br. Lakshmanan  (songs)  Thirunayinarkurichi  (lyrics) 
| cinematography = N. S. Mani
| editing        = K. D. George
| studio         = Neela Productions Malabar & Cochin) 
| released       =   
| runtime        = 170 minutes
| country        = India
| language       = Malayalam
| budget         =  
| gross          = 
}}
C.I.D. is a 1955 Indian Malayalam film made for Merryland Studio by director M. Krishnan Nair which stars Prem Nazir, Miss Kumari, Kottarakkara Sreedharan Nair and Kumari Thankam.

==Plot==
C.I.D. Sudhakaran is assigned to investigate the murder of Mukunda Menon, a wealthy planter. He discovers who is behind the murder and in the end marries Menons daughter Vasanthi.

==Cast==
* Prem Nazir as C.I.D. Sudhakaran
* Miss Kumari as Vasanthi
* Kottarakkara Sreedharan Nair as Vallabhan
* Kumari Thankam as Valsala
* S. P. Pillai as Pichu & Vava
* Soman as Rudrapalan
* T. S. Muthaiah as Pachan
* Adoor Pankajam as Panki
* Jose Prakash as Mukunda Menon
* Sreekantan Nair as Inspector
* Kuttan Pillai as Sivaraman

==External links==
*  
*   at the Malayalam Movie Database

 
 
 