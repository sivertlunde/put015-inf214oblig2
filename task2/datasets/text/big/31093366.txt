C'mon, Let's Live a Little
{{Infobox film
| name           = Cmon, Lets Live a Little
| image          = 
| image_size     = 
| caption        =  David Butler
| writer         = 
| narrator       = 
| starring       = Bobby Vee
| music          = 
| cinematography = 
| editing        = 
| studio         = All-Star Pictures, Hertlandy Associates
| distributor    = Paramount Pictures
| released       = 1967
| runtime        = 84 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} David Butler. It stars Bobby Vee and Jackie DeShannon. 

This was the last film directed by Butler, who began acting on screen in 1917 and had been directing since the late 1920s.

==Plot==
Enrolling in an Arkansas college, singer Jesse Crawford saves the life of Judy Grant, a deans daughter. She is grateful until Jesse performs at a rally staged by a student looking to discredit the dean, but Jesse was unaware of the rallys purpose and all is forgiven.

==Cast==
*Bobby Vee as Jesse Crawford
*Jackie DeShannon as Judy Grant
*Eddie Hodges as Eddie Stewart
*Suzie Kaye as Bee Bee Vendemeer
*Patsy Kelly as Mrs Fitts
*Kim Carnes as Melinda
*Frank Alesia as Balta

==References==
 

==External links==
* 
*  at Brians Drive-in Theatre

 

 
 
 
 
 
 


 