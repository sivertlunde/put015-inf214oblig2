Kumudham
{{Infobox film
| name           = Kumudham
| image          =
| image_size     =
| caption        =
| director       = Adurthi Subba Rao
| producer       = 
| writer         = K. S. Gopalakrishnan (dialogues)
| screenplay     = 
| story          = K. S. Gopalakrishnan
| starring       = S. S. Rajendran C. R. Vijayakumari M. R. Radha Sowcar Janaki
| music          = K. V. Mahadevan
| cinematography = R. Sampath
| editing        = L. Balu
| studio         = Modern Theaters
| distributor    = Modern Theaters
| released       =  
| country        = India Tamil
}}
 1961 Cinema Indian Tamil Tamil film,  directed by Adurthi Subba Rao and produced by S. D. Gunasekaran. The film stars S. S. Rajendran, C. R. Vijayakumari, M. R. Radha and Sowcar Janaki in lead roles. The film had musical score by K. V. Mahadevan.   

==Cast==
 
*S. S. Rajendran
*C. R. Vijayakumari
*M. R. Radha
*Sowcar Janaki
*S. V. Ranga Rao
*T. S. Muthaiah
*B. S. Saroja
*Sundaribai
*Mohana
*Ramarao
*C. S. Pandiyan
*P. D. Sambantham
*Sairam
*Seethalakshmi
 

==Soundtrack==
The music was composed by KV. Mahadevan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kallile Kalai Vannam || Sirkazhi Govindarajan || Kannadasan || 03.09
|- Susheela || Kannadasan || 03.24
|-
| 3 || Mama Mama || T. M. Soundararajan, Jamuna Rani || A. Murthakasi || 05.39
|- Susheela || A. Maruthakasi || 03.21
|- Susheela || Kannadasan || 03.26
|}

==References==
 

==External links==
*  

 
 
 
 


 