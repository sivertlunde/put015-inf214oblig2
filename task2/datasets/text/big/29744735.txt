Hatya (2004 film)
{{Infobox film
| name = Hatya
| image = Hatyafilm.jpg
| released = 15 October 2004
| director = Kader Kashmiri
| producer = SA Luthria
| writer =M.Salim
| music = Nadeem-Shravan
| starring = Akshay Kumar Navin Nischol Rajendra Gupta
}}

Hatya: The Murder is a 2004 Hindi Fantasy / Action film. It Stars Akshay Kumar, Navin Nischol, Raza Murad & Varsha Usgaonkar.

==Plot==
Ravi Lal (Akshay Kumar) lives a wealthy lifestyle with his father Ratanlal (Navin Nischol) his mother Seema (Reema Lagoo) and with his sister. Meanwhile Ravi has fallen in love with Kavita (Varsha Usgaonkar) and they get married. Ratan Lal is killed by businessman Murugan (Rajendra Gupta) after turning down the offer of land and plot. His son Ravi when witnesses his fathers killing is injured badly by Murugans men and gets hospitalized. Soon Kavita notices that Ravi has changed. Now he is more quiet and brooding. Kavita suspects him that Ravi has Having affair. But she dont know that he is finally killed. But his body is taken to the shape-changing venomous snake. Now Ravi has magical powers of a strong venomous snake and sets out for revenge to kill Murugan.

==Cast==
*Akshay Kumar - Ravi Lal
*Navin Nischol Ratan Lal 
*Reema Lagoo - Mrs. R. Lal 
*Varsha Usgaonkar - Kavita Jaiswal 
*Rajendra Gupta - Murugan
*Rajesh Vivek - Snake Charmer
*Johnny Lever - Paid Mourner
*Laxmikant Berade - Bhandari 
*Raza Murad
*Dinyar Contractor

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Yun Hafte Hafte Milna"
| Kumar Sanu, Alka Yagnik
|-
| 2
| "Kitna Intezar Tera Aur"
| Kumar Sanu, Alka Yagnik
|-
| 3
| "Khabar Chhap Jayegi"
| Kumar Sanu, Alka Yagnik
|}

==Production & Reception==
This movie had started production in 1992 and after being delayed for many years was finished in the year 2003. It finally released in 15 October 2004 and failed at the box office.

==External links==
*  

 
 
 
 
 
 


 
 