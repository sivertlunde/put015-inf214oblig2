Attack on a China Mission
{{Infobox film
| name           = Attack on a China Mission
| image          = AttackonaChinaMission.jpg
| image_size     = 
| caption        = Screenshot from the film James Williamson
| producer       = James Williamson
| writer         = 
| narrator       =
| starring       = Mr. James Mr. Lepard Florence Williamson
| music          =
| cinematography = James Williamson
| editing        =
| studio         = Williamson Kinematograph Company
| distributor    =
| released       =  
| runtime        = 1 min 25 secs
| country        = United Kingdom Silent
| budget         =
}}
 1900 UK|British short  silent drama James Williamson, showing some sailors coming to the rescue of the wife of a missionary killed by Boxers. The four-shot film, according to Michael Brooke of BFI Screenonline, was innovative in content and technique. It incorporated a reverse-angle cut and at least two dozen performers, whereas most dramatic films of the era consisted of single-figure casts and very few shots. Film historian John Barnes claims Attack on a China Mission had "the most fully developed narrative" of any English film up to that time."   

== Production ==
The director, inspired by Georges Méliès influential eleven-scene dramatised documentary The Dreyfus Affair (film series)|LAffaire Dreyfus (1899), made the film to meet a perceived public demand for footage of the Boxer Rebellion, which began in the early months of 1900, at a derelict house called Ivy Lodge in Hove, where, according to Michael Brooke, "he went to considerable lengths to ensure that his film appeared to be authentic, kitting out the house with a bilingual Anglo-Chinese Mission Station sign and drawing on his background as a chemist in order to fake gunshots and explosions."

== Premiere ==
The film was premiered at Hove Town Hall on  , where, according to Michael Brooke, it, "was such a success that the audience (fruitlessly) demanded a repeat screening there and then."

== Preservation ==
Just under half of  the original 230 feet of footage survives, but, according to Michael Brooke, "it includes material from all four shots, and, "despite some obvious trims (the initial forcing of the gate is missing, and the wifes appeal on the balcony to the sailors must surely have lasted more than one second), enough remains to give a good account of what the original audience must have seen."

==References==
 

==External links==
* 

 
 
 
 