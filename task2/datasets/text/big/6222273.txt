Grilled (film)
{{Infobox film
| name           = Grilled
| image          = Grilled2006Cover.jpg
| caption        = 
| director       = Jason Ensler
| producer       = Jeff Sussman Rory Rosegarten Jon Klane Bradley Jenkel William Tepper
| screenplay     = William Tepper
| story          = William Tepper Larry Hankin Kevin James Sofia Vergara Juliette Lewis Michael Rapaport Barry Newman Burt Reynolds
| music          = Adam Cohen
| cinematography = Lawrence Sher
| editing        = Chris Peppe
| studio         = Avery Pix The Jon Klane Company
| distributor    = New Line Cinema
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Kevin James. It was released direct-to-video in the United States on July 11, 2006.

==Premise==
Maurice (Romano) and Dave (James) are door-to-door meat salesmen who need to make a sale to keep their jobs. After several houses with no luck, they meet Loridonna (Sofia Vergara), who is interested in the guys meat as well as what they are selling. However, an emergency call offsets them, as they now go to the home of Mafia princess Suzzane (Juliette Lewis). Once there, they then encounter mobster Tony (Kim Coates) and two hitmen.

==Plot summary==
Maurice and Dave try but  fail to sell steaks to people through a mail service. Their boss, tired of their incompetence, gives them cards with the names and addresses of their highest buyers, warning this is their last chance. Dave loses all but one of the cards. It leads to a woman named Loridonna.

She gets a phone call from friend Suzzane, who has swallowed a fish and needs help. Loridonna lies that Dave is a doctor. While he is talking to Suzzane, Loridonna begins to seductively talk about wanting Maurices "meat". Turned on, Maurice tries to seal a deal with Loridonna, but Dave says Suzzane wants to kill herself. They drive Loridonna to Suzzanes house, where they discover Suzzane is a drunk whose suicide was a false alarm. 

Loridonna and Maurice begin making out. Tony, Suzzanes husband, comes home and catches them. Tony casually changes clothes while telling Maurice that Loridonna was once a man. An embarrassed Loridonna confirms this and tries to explain, but Maurice is too disappointed (and disgusted) to care. Tony then attempts to kill Dave, thinking he tried to seduce Suzzane. After explaining, Dave and Tony become friendly. Tony begins to grill some steaks, only to be ambushed, shot, and killed by two hitmen.

Finding some of Tonys guns, Dave and Maurice fight back. The hitmen put them into the trunk of their car, leaving them there while attending a party. Maurice is able to get out. He sees Goldbluth, a name from the cards they got from their boss. After freeing Dave, they describing to party guests the tenderness of their steaks. The hitmen return looking for them. Dave is unable to leave because Goldbluth is rambling on. He signs a contract to buy meat, so they warn Goldbluth that two hitmen are here to kill him. Dave gives Goldbluth the gun Tony had when he was killed.

Maurice and Dave drive off, looking back to see shots fired. Goldbluth comes out without a scratch. Maurice and Dave return to their boss with seven orders and $21,000 up front from Goldbluth. The film ends with them having a barbecue with Daves family.

==Release==
Grilled was originally titled Men Dont Quit, and was set for a worldwide theatrical release in 2005. Poor test screenings pushed it back, and the film went straight to video under its new title. 

==Cast==
* Ray Romano as Maurice Kevin James as Dave
* Mary Lynn Rajskub as Renee
* Jack Kehler as Abbott
* Jon Polito as Leon Waterman
* Sofia Vergara as Loridonna
* Juliette Lewis as Suzzane
* Kim Coates as Tony
* Michael Rapaport as Bobby
* Barry Newman as Boris
* Burt Reynolds as Goldbluth
* Eric Allan Kramer as Irving

==Home media==
On the North American Region 1 DVD, bonus features include a single deleted scene, a making-of featurette, and a documentary on Romano and James real-life friendship.

==External links==
*  

 

 
 
 
 
 
 