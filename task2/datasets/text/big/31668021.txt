Amma (1952 film)
{{Infobox Film
| name           = Amma
| image          = Amma-1952.jpg
| caption        = 
| director       = K. Vembu
| producer       = T. E. Vasudevan
| writer         = Nagavally R. S. Kurup
| based on       =  Lalitha B. S. Saroja Aranmula Ponnamma M. N. Nambiar T. S. Dorairaj P. M. Devan Gopalan Nair T. S. Muthaiah Sharma  Paul Vengola
| music          = V. Dakshinamurthy  (songs)  P. Bhaskaran  (lyrics) 
| cinematography = V. Rajagopal 
| editing        = R. Rajagopal 
| studio         = 
| distributor    =  
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =  10 lakh
| gross          = 
}}
Amma ( ) is a 1952 Malayalam film directed by K. Vembu and written by Nagavally R. S. Kurup. The film is based on L.V Prasads Telugu movie Shavukar. The film was also made in Tamil and was a moderate success at the box office.  It was one of the two films that enjoyed success among the 11 Malayalam films which released in 1952, the other being Prem Nazir-starrer Visappinte Vili.  The cast of Amma includes Lalitha (actress)|Lalitha, B. S. Saroja, Aranmula Ponnamma, Thikkurissy Sukumaran Nair, M. N. Nambiar, T. S. Dorairaj, P. M. Devan, Gopalan Nair, T. S. Muthaiah and Sharma.

==Plot==
The film tells the tale of Lakshmi Amma, a loving mother, and her son Venu. Venu falls in love with Radha, a rich lady, and later marries her. After the marriage, Venu and Radha shifts to Madras. He manages to send some money to his mother in the beginning. But as days went, Radha intervenes and Venu is not able to send any more money to his poor mother. Meanwhile, a private moneylender from whom Lakshmi Amma had borrowed some money for Venus marriage, kicks her out of the house. Venu brings Lakshmi Amma to his Madras residence but Radha is not ready to allow her to stay there. This creates problems and Lakshmi Amma had to leave that house. She is thrown to the streets. A lot of dramatic happenings form the rest of the story.

==References==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 

 