Curley (film)
{{Infobox film
| name = Curley
| director = Bernard Carr
| image	=	Curley FilmPoster.jpeg
| producer = Hal Roach Robert F. McGowan Dorothy Reid Mary McCarthy Billy Gray Renee Beard
| music = Heinz Roemheld
| cinematography = John W. Boyle
| editing = Bert Jordan
| distributor = United Artists
| released = August 23, 1947
| runtime = 53 min.
| country = United States
| language = English
 }}
Curley is a 1947 film produced by Hal Roach and Robert F. McGowan as a re-imagining of their Our Gang series. The film was one of Roachs "Hal Roachs Streamliners|streamlined" features of the 1940s, running 53 minutes and was designed as a b-movie. Like most of Roachs latter-day output, Curley was shot in Cinecolor.
 Billy Gray, and Renee Beard, younger brother of original Our Gang cast member Matthew "Stymie" Beard. The plot of the film centers on a group of schoolchildren, led by Curley (Olsen), playing pranks on their teacher, Ms. Johnson (Rafferty).

Our Gang was known for its integrated cast of black and white children, and Curley followed suit. The Memphis, Tennessee Censor Board banned Curley for showing black and white children in school together and playing together. Lloyd Binford, head of the censor board, gave this rationale to Roachs distributor, United Artists: "  was unable to approve your Curley picture with the little Negroes as the south does not permit Negroes in white schools nor recognize social equality between the races, even in children." 

When Hal Roach sold Our Gang to Metro-Goldwyn-Mayer in 1938, he was contractually bound not to produce any more childrens comedies. When Roach decided that he wanted to produce Curley, he got MGMs permission by giving up his right to buy back the name Our Gang. {{cite book |last=Maltin |first=Leonard |authorlink=Leonard Maltin |author2=Richard W. Bann |title=The Little Rascals: The Life & Times of Our Gang |origyear=1977 |edition=Rev. |year=1992 |publisher=Crown Publishing/Three Rivers Press |isbn=0-517-58325-9|pages=227–234
}}  Curley and its sequel, Who Killed Doc Robbin, performed mildly at the box office. 

==Plot Summary==

The very much appreciated young woman who was the previous teacher in Lakeview elementary school got married, and a substitute is appointed. School rascal William "Curley" Benson gather his classmates to make plans to get rid of their new teacher. They strongly suspect the substitute teacher to be half-mad eccentric, middle-aged Miss Johnson.

The county supervisor, Miss Payne, visits Miss Johnson and finds out that the new teacher is actually Miss Johnsons niece Mildred, a pretty young woman who has taught athletics to privates in the US Navy. Miss Payne has her doubts about Mildred’s capability to control a class like the one at Lakeview and warns Mildred. Miss Payne believes that Mildred might be too young and inexperineced to handle the spirited children.

On the morning of the first day of school Mildred encounters the unsuspecting Curley on the way to school and offers him a ride. Not knowing that he is talking to his teacher, he tells her about the pranks that he and his friends are going to play on "Pigglepuss," their new teacher. Curley even tells her about putting his pet frog, Croakey, on the teacher’s chair.

Curley also manages to disclose the school kids’ hope that Miss Johnson will quit immediately, so that they can spend the whole day fishing. At school, Curley loads his "rocketship" car with smoke flares. He positions the car so as to aim at an exhaust tube through a classroom window. When Curley takes his seat in the classroom, he discovers that his new teacher is the wonderful kindhearted Mildred. During that first day she teaches the children a lesson of humility by making each one a victim of his own prank, and Curley, who is also humiliated, flees the scene before his prank is about to happen.

The schoolroom is duly filled with exhaust, Curley is blamed, but it turns out he is chasing the rocketship toy car, and not driving it. The car has been bijacked by "Dis" and "Dat", who are two mischievous children.  They drive the car carelessly and wildly across the fields and, ultimately into a haystack, taking the stack with them, continuing the frightful journey. Miss Payne appears on the scene, and frightened by the moving haystack, she crashes her car while trying to avoid it. Miss Payne angrily cries out her disappointment with Mildred for her inability to discipline the children. 
Meanwhile, Mildred has brought the children on a picnic with her aunt. She teaches them baseball, football and boxing and offers the children good grades if they learn. Mildred is engaged in a boxing fight with the big Hank, a tough student, when the enraged Miss Payne arrives in order scrutinize her performance.

Curley comes back from his hiding to help, but by accident he instead manages to send Hank flying into a lake. Curley is forced to leave the picnic in shame. The other are grateful for their substitute teacher and thank Miss Payne for Mildred. Mildred gets an explanation of the previous events, and that when the classroom filled with smoke, Curley was chasing his "rocketship," not driving it, thus not responsible for the prank. Mildred finally finds Curley hiding and crying, afraid he will be expelled. He is also afraid that Mildred will be fired. Mildred reassures him with cake and ice cream, that such a thing won’t happen, and tops off with picking up Croakey the frog. 

==References==
 

==External links==
*  

 
 
 
 
 
 