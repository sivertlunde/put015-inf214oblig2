A Time to Love (film)
{{Infobox film
| name = A Time to Love
| caption = 
| image = A Time to Love FilmPoster.jpeg
| director = Huo Jianqi
| producer = He Ping Han Sanping (Executive produce|e.p.) Yang Buting (e.p.) Zhang Renjie Lu Yi
| music = Wang Xaofeng
| cinematography = Ming Sun
| editing = Yang Tao
| distributor = China Film Group Tokyo Theaters Company (Japan)
| released =  
| runtime = 113 minutes
| country = China
| language = Mandarin
| budget = 
}} 2005 film Lu Yi. The film is based on a true story.

==Cast==
*Zhao Wei ... Qu Ran Lu Yi ... Hou Jia
*Song Xiaoying ... Hou Jias mother
*Zhang Qian... Qurans father

==Reception==
*Succulently lensed yarn about two childhood friends who finally get together well into adulthood is fairly standard Asian romantic fare—given luster (and an emotional wallop in the final scene) purely by Zhaos gamine personality. Tailored for, and released around, Valentines Day in China—now an established marketing hook—pic will be savored best at Asiaphile events. -- Variety   Variety July 26, 2005
 

*No doubt the most provoking formal element is Huo’s amazing imagery. Huo’s camera skilfully offers a variety of camera angles and compositions that make almost every image in the film easily frame-able. Similar to Christopher Doyle’s cinematography style, Hou’s colours are rich, his textures are emphasized and his mise-en-scene is intelligently used to provide frames....Chinese pop idols Vicki Zhao Wei and Lu Yi give exceptional performances, at times carrying the film during slower plot moments. Their emotions seem genuine and their dynamics as an onscreen couple are stunning. Vicki Zhao has a rare ability to emote the most succinct of feelings without making them too overtly dramatic, which again reinforces the subtle touch that the film cleverly displays through its imagery. Surely though, if you don’t enjoy their acting, then both Vicki Zhao Wei and Lu Yi provide enough eye-candy to keep the strictest of film critics satiated for the film’s 115 minutes running time. -- Heroic-Cinema.com 
*Without a doubt, Vicki Zhao is the star of the film, and demonstrates that she can carry a movie when afforded the opportunity. She exhibits equal parts subtlety and innocence, and is the main reason the film works as well as it does. Yi Lu is average as the male lead, and pales in direct comparison to Zhao when it comes to screen presence. And while the parent characters are one-note caricatures, the blame should go to the screenwriters for not fleshing out any of the film’s secondary characters. -- Beyond Hollywood 

==Awards and nominations==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 2px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#CCCCCC" align="center"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- bgcolor="#CCCCCC" align="center"
!Award
!Category
!Name
!Outcome
|-
|-style="border-top:2px solid gray;"
|rowspan=2|Shanghai International Film Festival Golden Goblet
|
| 
|- Best Actress Zhao Wei
| 
|-
|rowspan=2|Huabiao Awards Outstanding Film
|
| 
|- Outstanding Actress Zhao Wei
| 
|-
|rowspan=3|Golden Rooster Awards Best Supporting Actress Song Xiaoying
| 
|- Best Cinematography Sun Ming
| 
|- Best Art Direction Cui Ren
| 
|-
|rowspan=7|Changchun Film Festival Best Film
|
| 
|- Best Director Huo Jianqi
| 
|- Best Actress Zhao Wei
| 
|- Best Actor Lu Yi
| 
|- Best Supporting Actress Song Xiaoying
| 
|- Best Supporting Actor Qian Zhang
| 
|- Best Cinematography Sun Ming
| 
|-
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 