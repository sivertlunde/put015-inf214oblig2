M (2007 film)
{{Infobox film
| name     = M
| image    = Mposter.jpg
| caption  = Theatrical poster
| director = Lee Myung-se
| producer = Oh Su-mi
| writer   = Lee Myung-se   Lee Hae-kyeong   Jo Jin-guk
| starring = Kang Dong-won   Lee Yeon-hee   Gong Hyo-jin
| cinematography = Hong Kyung-pyo
| editing  = Ko Im-pyo
| music    = Jo Seong-woo   Choe Yong-rak
| released =  
| runtime  = 109 minutes
| country  = South Korea
| language = Korean
| budget   =
| gross    =   
| film name = {{Film name
 | hangul =  
 | hanja =
 | rr = Em
 | mr = Em}}
}}
M ( ) is a 2007 South Korean psychological drama film starring Kang Dong-won.  The film premiered at the Toronto International Film Festival,  and the final cut had its Korean premiere at the Pusan International Film Festival.   

Using visual effects, complex dream sequences, and gliding camerawork, director Lee Myung-se describes his film as "a dark labyrinth of dream and reality," and that instead of using computer graphics, he prefers to "capture the fantasy elements through lighting and emotions."    

== Plot ==
A prominent up-and-coming author Min-woo readies his new much anticipated follow-up novel while suffering from writers block, as well as frequent nightmares and hallucinations. This unexplainable condition affects both his personal and professional life. Soon he cant differentiate reality from fantasy and continues to have feelings of being chased. His own paranoia leads him to a café in a dark, unassuming alley and encounters a charming young woman named Mimi. Min-woo starts to wonder how he and this girl in front of him are connected and traces long-forgotten memories of his first love.

== Cast ==
* Kang Dong-won - Min-woo
* Lee Yeon-hee - Mimi
* Gong Hyo-jin - Eun-hye
* Jeon Moo-song - bartender
* Song Young-chang - Company president Jang
* Im Won-hee - Sung-woo
* Lim Ju-hwan - umbrella man
* Seo Dong-soo - editor
* Jung In-gi - doctor
* Kim Dong-hwa
* Yoon Ga-hyun - grooms sister
* Jung Sun-hye
* Choi Dae-sung - Min-woos friend

==Awards and nominations==
;2007 Korean Film Awards 
* Best Cinematography – Hong Kyung-pyo
* Best Art Direction – Yoon Sang-yoon, Yoo Joo-ho
* Nomination – Best Film
* Nomination – Best Director – Lee Myung-se
* Nomination – Best Editing – Ko Im-pyo
* Nomination – Best Visual Effects – Jang Seong-ho
* Nomination – Best New Actress – Lee Yeon-hee

;2008 Baeksang Arts Awards
* Nomination – Best Director – Lee Myung-se
* Nomination – Best New Actress – Lee Yeon-hee

;2008 Grand Bell Awards
* Best Art Direction – Yoon Sang-yoon, Yoo Joo-ho
* Nomination – Best Editing – Ko Im-pyo
* Nomination – Best Visual Effects – Jang Seong-ho
* Nomination – Best Sound – Park Jun-oh

;2008 Blue Dragon Film Awards
* Nomination – Best Cinematography – Hong Kyung-pyo
* Nomination – Best Art Direction – Yoon Sang-yoon, Yoo Joo-ho

==Adaptation== Mnet in 2014.

== References ==
 

== External links ==
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 