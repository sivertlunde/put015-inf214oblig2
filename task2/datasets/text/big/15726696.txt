Star Wars: The Clone Wars (film)
 
{{Infobox film
| name = Star Wars: The Clone Wars
| image = Star wars the clone wars.jpg
| alt = 
| caption = Theatrical release poster
| director = Dave Filoni
| producer = George Lucas Catherine Winder
| writer = Henry Gilroy Steven Melching Scott Murphy
| narrator = Tom Kane
| starring = Matt Lanter Ashley Eckstein James Arnold Taylor Tom Kane Dee Bradley Baker Christopher Lee Samuel L. Jackson Anthony Daniels
| music = Kevin Kiner
| studio = Lucasfilm Lucasfilm Animation Warner Bros. Pictures
| runtime = 98 minutes
| released =  
| country = United States
| language = English
| budget = $8.5 million
| gross = $68,282,844
}}
Star Wars: The Clone Wars is a 2008 American   produced by   and  . Distributed by Warner Bros. Pictures, which also holds the home media distribution rights to both this film and the TV series, the film premiered on August 10, 2008 at the Graumans Egyptian Theatre, while screening in wide-release on August 14, 2008 across Australia, and August 15 in the United States, Canada and the United Kingdom. The Clone Wars served as an introduction to the television series of the same name, which debuted on October 3, 2008.

The film received a largely negative response from critics. It earned over $14 million in its opening weekend and went on to earn over $60 million worldwide. A major marketing campaign included toys, fast food, and comic books.

==Plot==
  Clone Wars, Jedi Knights Anakin Skywalker Republic Clone clone army against the Separatist droid army on the planet Christophsis. Awaiting reinforcements, the two Jedi greet a shuttle carrying the young Jedi Ahsoka Tano, who insists that she has been assigned by Jedi Master Yoda to serve as Anakins Padawan. Anakin begrudgingly accepts Ahsokas apprenticeship, and the two succeed in deactivating the Separatists energy field while Obi-Wan stalls the droid army commander, allowing a Republic victory.

Following the battle, Yoda arrives and informs the Jedi that crime lord Jabba the Hutts son Rotta has been kidnapped. Anakin and Ahsoka are tasked with retrieving the Huttlet, while Obi-Wan is sent to Tatooine to negotiate with Jabba over a potential treaty between the Hutts and the Republic. Anakin and Ahsoka find Rotta on the planet Teth, where they are ambushed by Separatist forces led by Count Dookus apprentice Asajj Ventress, discovering that Dooku hopes to frame the Jedi for Rottas kidnapping. The Jedi manage to escape the trap along with R2-D2 and hijack a derelict transport with which they travel to Tatooine. Obi-Wan, alerted by Anakin, arrives on Teth and defeats Ventress in a lightsaber duel, though she manages to escape.
 Ziro in Coruscant. The Hutt refuses to cooperate, apparently believing that it is the Jedi who are responsible for the situation. Padmé, however, soon discovers that Ziro has actually conspired with Dooku to engineer the downfall of his nephew in order to seize power over the Hutt clans. Padmé is discovered and detained, but a chance call by C-3PO enables her to summon a squadron of clone troopers, and Ziro is arrested.

Upon their arrival on Tatooine, Anakin and Ahsoka are shot down by MagnaGuards. Anakin devises a ruse to confront Dooku while carrying a decoy Rotta, leaving Ahsoka to take the real Rotta to Jabbas palace. While Anakin fights off Dooku, Ahsoka is ambushed by the MagnaGuards, whom she defeats. The two deliver Rotta safely to Jabba, who nonetheless orders the Jedis execution for their supposed attempt to kidnap him. However, Padmé contacts Jabba in time and reveals Ziro and the Separatists responsibility for the kidnapping. Acknowledging the Jedis heroism, Jabba agrees to the Republic treaty while Anakin and Ahsoka are retrieved by Obi-Wan and Yoda.

==Cast== Anakin Skywalker
* Ashley Eckstein as Ahsoka Tano
* James Arnold Taylor as Obi-Wan Kenobi, 4A-7
* Tom Kane as Yoda, Narrator, Admiral Yularen Count Dooku / Darth Tyranus
* Dee Bradley Baker as Captain Rex, Commander Cody, Clone troopers
* Samuel L. Jackson as Mace Windu
* Nika Futterman as Asajj Ventress, TC-70
* Anthony Daniels as C-3PO Palpatine / Darth Sidious Catherine Taber as Padmé Amidala Ziro the Hutt Rotta the Huttlet
* Kevin Michael Richardson as Jabba the Hutt Matthew Wood as Battle droids

==Production==
Star Wars: The Clone Wars was made to serve as both a stand-alone story and a lead-in to the weekly  .  George Lucas had the idea for a film after viewing some of the completed footage of the early episodes on the big screen.  }} Those first few episodes, originally planned for release on television, were then woven together to form the theatrical release.  The story of the kidnapped Hutt was inspired by the Sonny Chiba samurai film titled Shogun’s Shadow. 
 think outside the box in a positive way. 
 Matthew Wood, Christopher Lee and Samuel L. Jackson, returned to vocally reprise their roles of their respective characters. 

==Music==
  
The films music was composed by Kevin Kiner.    

The original motion picture soundtrack was released by  , July 9, 2008. Retrieved on September 3, 2008.  Kiner is known for his work on such television series as  ,  . The soundtrack uses some  , August 21, 2008. Retrieved on September 3, 2008. 

==Marketing==

===Toys=== All Terrain Mission Bay, San Diego and Times Square in Manhattan, New York City held costume and trivia contests on July 26, and gave away limited-edition Star Wars toys with every purchase. A section of the Toys "R" Us website was also dedicated to The Clone Wars.  The toy line continues with The Clone Wars figures being well received by collectors for their detail to the characters and vehicles.

===Food=== Target and KB Toys also devoted shelf space for Clone Wars toys, but did not hold midnight releases or pursue the branding opportunities Toys "R" Us did. On August 15, McDonalds held its first ever Happy Meal promotion for a Star Wars film and for four weeks, 18 exclusive toys came in specially designed Happy Meal boxes. 

===Comics and books===
  and  . The Clone Wars comics did not receive the promotional campaign it otherwise would have due to the abruptness of the theatrical and comic book releases.   , Lego announced a product line for the film and the TV series, to be released in  July 2008 in the United States and on August 2008 in the United Kingdom. 

===Video games===
The   for the   for Wii.  A reviewer from PocketGamer.co.uk said his expectations for Jedi Alliance were low due to poor Clone Wars movie reviews, but he found the game "a varied and well-paced experience." 

===Portable media players===
A Star Wars: The Clone Wars   logo on it, and one with two Clone troopers on Coruscant. One review claimed it improved upon a Darth Vader MP3 player released in July 2008, which featured only 512 megabytes of memory and a dated visual display.  A Star Wars iPod iSpeaker (a speaker/dock for iPods, iPhones and MP3 players) was also released. The speaker includes an image of Captain Rex and three other Clone Troopers. 

===Racing sponsorship===
A Star Wars: The Clone Wars  , July 23, 2008. Retrieved on September 3, 2008.  The car finished 14th at Infineon, which Andretti attributed to a slow pit stop early in the race; he added, "I just dont think it was a very good performance for us today."  The Clone Wars car was the second collaboration between Lucasfilm, Blockbuster and Andretti Green Racing. It premiered as an Indiana Jones and the Kingdom of the Crystal Skull car at the Indianapolis 500 in May 2008. 

==Reception==

===Critical response===
Reviews were mostly negative.  . Retrieved on June 21, 2012.    saying,  Its hard to tell the droids from the Jedi drones in this robotic animated dud, in which the George Lucas Empire Strikes Back—at the audience. What wears you out is Lucas immersion in a Star Wars cosmology that has grown so obsessive-compulsively cluttered yet trivial that its no longer escapism; Because this movie has bad lightsaber duels and the lack of the original cast, its something you want to escape from.   The Clone Wars received an 18% approval rating based on 157 reviews compiled by   films and the much-derided Star Wars Holiday Special garnered higher ratings, although their averages encompassed far fewer reviews.  At Metacritic, the movie scored 35% based on 30 reviews, earning it the status "generally negative."   

Aint It Cool News posted two reviews of the film during the week before its release, but pulled them down due to an embargo placed on those attending the screening its writers attended. The same reviews were re-posted on the site, on the day of the films release. The retraction prompted some readers to allege a conspiracy by LucasFilm to keep negative press out of circulation until the release of the film, but although the review by site creator Harry Knowles was negative, Drew McWeeny said that his review was positive and that no such conspiracy existed.   

Several critics compared The Clone Wars to a  , August 15, 2008. Retrieved August 17, 2008.  and described it as little more than a plug for the upcoming animated series. Barnard, Linda.  .  , August 15, 2008. Retrieved on August 17, 2008.  Rickey, Carrie.  .  , August 14, 2008. Retrieved on August 17, 2008.  In his review for Entertainment Weekly, critic Owen Gleiberman gave the movie an  , Iss. #1007/1008, August 22/29, 2008. Retrieved on August 17, 2008.  Carrie Rickey, of The Philadelphia Inquirer, said, "The best that can be said about the movie is that its harmless and mostly charmless. The Clone Wars is to Star Wars what karaoke is to pop music." 

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 90%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5" Remember how people talked about the Star Wars prequels like they were the worst movies ever made, when really, come on, they werent THAT bad? The Clone Wars actually IS that bad.
|-
|style="text-align: left;"|&nbsp;— Film critic, Eric D. Snider Snider, Eric D.  .  , August 15, 2008. Retrieved on April 22, 2013. 
|}
 Easter Island waxworks might." 
 Globe and Mail, wrote that although The Clone Wars is intended for younger audiences, "parents may be perturbed by the films relentless violence."  Ebert also found protagonist Ahsoka Tano "annoying,"  and Michael Rechtshaffen, of The Hollywood Reporter, said the attempts of humor amid the bickering between Tano and Anakin Skywalker are "strained".  Puig said she enjoyed the character, and that "her repartee with Anakin enlivens things." 

  
The film was nominated for a Saturn Award for Best Animated Film and a Razzie Award for Worst Prequel, Remake, Rip-off or Sequel. 

===Box office=== The Dark Knight, which earned $25.8&nbsp;million and $16.3&nbsp;million, respectively.  Dan Fellman, head of distribution for Warner Bros. Pictures, said the box office performance met expectations because two-thirds of the audience were families and the budget for the film was $8.5 million, frugal considering it was a CGI film, and because the film was meant to introduce the animated series. Fellman said, "It was targeted to a specific audience for specific reasons. We accomplished that mission, and it will continue in another medium."  When The Clone Wars dropped to $5.6 million in its second week of release, ContactMusic.com described it as "the first bona fide Star Wars flop."  The film also earned $23,428,376 from DVD sales in the US. 

===Home media===
The films two-disc DVD and Blu-ray Disc was released on November 11, 2008 in the United States and on December 8, 2008 in the United Kingdom.   The film was released as a single-disc DVD, two-disc Special Edition DVD, and Blu-ray Disc. The standard-definition versions include the film in widescreen format with Dolby Digital 5.1 Surround EX sound, and with feature-length audio commentary. 

==References==
 

==External links==
 
*  
*   at  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 