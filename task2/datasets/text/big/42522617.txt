Force Majeure (film)
 
{{Infobox film
| name           = Force Majeure
| image          = Force Majeure poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ruben Östlund
| producer       = {{Plainlist|
* Erik Hemmendorff
* Marie Kjellson}}
| writer         = Ruben Östlund
| starring       = {{Plainlist|
* Johannes Bah Kuhnke
* Lisa Loven Kongsli
* Clara Wettergre
* Vincent Wettergren
* Kristofer Hivju
* Fanni Metelius}}
| music          = Ola Fløttum
| cinematography = Fredrik Wenzel
| editing        = Jacob Secher Schulsinger 
| studio         = {{Plainlist|
* Beofilm
* Coproduction Office
* Film i Väst
* Motlys
* Plattform Produktion
* Rhône-Alpes Cinéma
* Société Parisienne de Production}}
| distributor    = TriArt Film
| released       =  
| runtime        = 119 minutes  
| country        = {{Plainlist|
* Sweden
* France
* Norway}}
| language       = {{Plainlist|
* Swedish
* English
* French
* Norwegian}}
| budget         = 
| gross          = $1.4 million (US)   
}} Best Film award at the 50th Guldbagge Awards.

==Plot==
The film presents a week in the life of a Swedish family staying at a luxury resort in the French Alps: a businessman named Tomas, his wife Ebba, their young daughter Vera and preschooler Harry. On their second day of skiing, a controlled avalanche goes awry and threatens them as they are having lunch, whereupon Tomas panics and runs, leaving Ebba with their children. He quickly returns and no one is hurt, but Tomas momentary cowardice, together with a refusal to admit what he did when she raises the topic at dinner with another couple, creates a rift in their marriage that he struggles to repair.

Ebba is deeply disappointed in her husband and finds it difficult to forgive him, while at the same time their children sense the growing tension between them and become worried. Mats, an old friend, joins them at the resort with his young girlfriend Fanni, and again Ebba recounts the story of the avalanche, going so far as to show them a video of the incident that shows Tomas running away. Mats tries to help Tomas and Ebba reconcile, but the conflict infects his relationship as well when Fanni suggests that she would expect Mats to react in the same way as Tomas. Due to the mounting emotional pressure, Tomas finally experiences an emotional breakdown. Even then, he finds it difficult to fully engage with his actions, first pretending to sob when confessing his failings to Ebba before reverting a state of such inconsolable shame and self-pity that his children, along with a reluctant Ebba, find themselves having to comfort him.

On their final day of skiing, Ebba gets lost in fog and Tomas briefly leaves the children alone to rescue her, returning with her shortly after. As the family and their friends leave the resort by coach, Ebba decides the driver is dangerously incompetent and demands in a panic to be let off, leaving Tomas and their children on the bus. Eventually, all of the passengers get off the bus and descend the mountain on foot. Tomas accepts a cigarette from an acquaintance. Surprised, Harry asks Tomas if he smokes, to which he replies, after a pause, "Yes, I do."

==Cast==
* Johannes Bah Kuhnke as Tomas
* Lisa Loven Kongsli as Ebba
* Clara Wettergren as Vera
* Vincent Wettergren as Harry
* Kristofer Hivju as Mats
* Fanni Metelius as Fanni

==Production== Filming took place at Les Arcs, a ski resort in Savoie, France. 

Ruben Östlund attributed the inspiration for the films key scenes to a few viral Youtube videos which he felt corroborated the plausible situation and emotions of the characters. The director reasoned that "...if someone captured an event or action or pang of emotion on camera and uploaded to the Internet, then it happened in real life. And it could happen in Force Majeure.".  The scene where Ebba demands to be let off the bus is based on the Youtube viral video titled Idiot Spanish bus driver almost kills students.

==Reception==
The film has a 93% rating on Rotten Tomatoes  and a score of 87 ("universal acclaim") out of 100 on Metacritic. 

==Awards==
The film was selected to compete in the Un Certain Regard section at the 2014 Cannes Film Festival    where it won the Jury Prize.  It was also screened in the Special Presentations section of the 2014 Toronto International Film Festival.   
 Best Foreign Best Foreign Language Film at the 72nd Golden Globe Awards. 
 Best Film Best Director, Best Actor, Best Actress, Best Supporting Best Supporting Best Screenplay, Best Cinematography, Best Sound Editing, and Best Editing at the 50th Guldbagge Awards.   

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film
* List of films featuring drones
* Force majeure

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 