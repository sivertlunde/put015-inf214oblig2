Strange Portrait
{{Infobox film
| name           = Strange Portrait
| image          = 
| caption        = 
| director       = Jeffrey Stone
| producer       = Terry Bourke
| writer         = 
| narrator       =  Barbara Lee Mai Tai Sing Christina Stone John Wallace
 Paul Lewis
| cinematography = 
| editing        = Peter R. Hunt 
| distributor    = 
| released       = 
| runtime        = 
| country        = | language       = English
| gross          = 
}}
 Barbara Lee, Mai Tai Sing and Tina Hutchence. Its associate producer was Terry Bourke.  In 1966 Stone and his wife went searching for a distributor, hoping to enter it into the Asian Film Festival.   The film never saw a release and there is some mystery about what happened. 
  
There are differing reports on what happened to the film. One is that it was destroyed in a warehouse fire. 
   Terry Bourke, the films producer had been searching for years to find a print of the film. This would possibly indicate that be believed a print had been made from the negative. Jeffrey Stone, the director said the film was destroyed by the Hong Kong government.  It is considered to be a lost film. 

==Synopsis==
The film has been described as a spooky thriller. Christina Stone played a ballet dancer and the lead role was played by Jeffrey Hunter.  Hunter played the role of Mark, an expat American and petty thief in Hong Kong who lives with his Chinese Girlfriend played by Barbara Lee. One day while watching a soccer game, a mansion catches his eye and he breaks into it. Whilst inside, he notices a portrait there that looks just like him. The only person living in the mansion is a wealthy but mad woman played by Mai Tai Sing. He finds out that the painting that looks like him is actually that of her husband that deserted her years before. He also discovers that she has an abundance of jewelry and decided to woo her and pretend that he is her departed husband. While gaining access to the womans treasure he makes a startling and macabre discovery and thus has a tragic end.  

==Cast & crew==
Jason Evers was the original choice for the lead role. He had to vacate the role in favor of a television pilot, Three for Danger, thus the role was filled By Jeffrey Hunter even though is agent wasnt keen on the idea. 
 Hong Kong. 
 
Patricia Hutchence who was the makeup artist in this film is the mother of INXS frontman Michael Hutchence. Her daughter Tina Hutchence who had a role in the film is Hutchences half-sister.  

Barbara Lee had previously been in the London production of Flower Drum Song. 
 Dato Loke Wan Tho.   

===Actors===
* Jeffrey Hunter ... Mark
* Tina Hutchence
* Mai Tai Sing
* Barbara Lee
* Christina Stone
* John Wallace  

===Crew===
* Jeffrey Stone - director & writer
* Terry Bourke - associate producer
* Paul Lewis - music
* Peter R. Hunt - editor
* Patricia Hutchence - makeup artist 
* Peter MacGregor-Scott - assistant director
===Production company===
* East-West Motion Picture Co. . 

==Links==
*  
*  
*  

==References==
 

 
 
 
 
 