When a Woman Sins
 
{{Infobox film
| name           = When a Woman Sins
| image          = When a Woman Sins 1918 poster.jpg
| caption        = Film poster (1918)
| director       = J. Gordon Edwards William Fox
| writer         = E. Lloyd Sheldon
| story          = Beta Breuil
| starring       = Theda Bara Josef Swickard
| music          =
| cinematography = John W. Boyle
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 7 reels
| country        = United States Silent English intertitles
| budget         =
}}
 silent drama film directed by J. Gordon Edwards and starring Theda Bara.

==Cast==
* Theda Bara as Lilian Marchard / Poppea
* Josef Swickard as Mortimer West Albert Roscoe as Michael West 
* Alfred Fremont as Augustus Van Brooks 
* Jack Rollens as Reggie West
* Genevieve Blinn as Mrs. West
* Ogden Crane as Dr. Stone

==Preservation status==
This film is now considered a lost film.   

==References==
 

==See also==
*List of lost films

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 