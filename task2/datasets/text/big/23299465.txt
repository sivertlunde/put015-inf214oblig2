Never Forget Me
 
{{Infobox film
| name           = Never Forget Me
| image          = Never Forget Me (1976 film).jpg
| caption        = Theatrical poster for Never Forget Me (1976)
| director       = Mun Yeo-song 
| producer       = Lee Woo-suk
| writer         = Seo In-gyeong
| starring       = Im Ye-jin Lee Deok-hwa
| music          = Yun Chae-hyeon
| cinematography = Hong Dong-hyuk
| editing        = Hyeon Dong-chun
| distributor    = Dong A Exports Co., Ltd.
| released       =  
| runtime        = 93 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| film name      = {{Film name
 | hangul         =  
 | hanja          =  짜  짜 잊지마
 | rr             = Jinjja jinjja ijjma
 | mr             = Chintcha chintcha ichchima
 | context        = }}
}} 1976 South Korean film directed by Mun Yeo-song. It was followed the same year by the sequel I Am Really Sorry.
 
==Synopsis==
Two high school students have romantic feelings for each other at a time when such relationships are not allowed at their age. Traveling together by train together to school every day, they promise to spend their future together. When the boy returns from military duty to visit the girl, he discovers she has died from pneumonia. He rides the train they took together and recalls their past.   
 
==Cast==
* Im Ye-jin as Jung-a 
* Lee Deok-hwa as Young-soo
* Shin Goo as Young-soos older brother
* Mun Oh-jang
* Kim Yun-gyeong
* Yun Hee
* Lee Chang-won Kim Bok-sun
* Kim Ung
* Han Tae-il

==Bibliography==
===English===
*  
*  

===Korean===
*  
*  

==Notes==
 

 
 
 
  
 
 