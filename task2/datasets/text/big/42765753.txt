The Torture Club
{{Infobox animanga/Header
| name            = The Torture Club
| image           = Chotto_Kawaii_Aian_Meiden_Manga.jpg
| caption         = Cover of Volume 1
| ja_kanji        = ちょっとかわいいアイアンメイデン
| ja_romaji       = Chotto Kawaii Aian Meiden yuri
}}
{{Infobox animanga/Print
| type            = manga
| author          = Makoto Fukami
| publisher       = Kadokawa Shoten
| demographic     = Yuri (genre)|Yuri
| magazine        = 4-Koma Nano Ace (2011-2013)  Young Ace (2014-present)
| first           = 2011
| last            = 
| volumes         = 3+
| volume_list     =
}}
 

  is a Japanese Yonkoma|4-Koma comic strip written by Makoto Fukami and illustrated by Alpha AlfLayla. It was published by Kadokawa Shoten in 4-Koma Nano Ace from April 2011 until the final issue in October 2013, it was then transferred to Young Ace from December 2013 onwards.

 

{{Infobox film
| name           = The Torture Club
| image          = Chotto_Kawaii_Aian_Meiden_Live.jpg
| alt            = Chotto Kawaii Aian Meiden 
| caption        = Poster for the Live Action movie
| director       = Kota Yoshida
| producer       = 
| writer         = Kota Yoshida Makoto Fukami
| based on       =   
| starring       = Noriko Kijima
| music          = Akira Matsumoto
| cinematography = Masafumi Seki
| editing        = Masayuki Narumi Kadokawa Daiei Studios
| distributor    = Kadokawa Corporation
| released       =  
| runtime        = 107 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

==Live Action==
A live action film based on the comic was directed by Kota Yoshida. It was released on 19 July 2014. 

==Cast==
*Noriko Kijima as Yuzuki Mutoh
*Haruna Yoshizumi as Aoi Funaki
*Yuki Mamiya as Yuuri Kobashi
*Mika Yano as Maika Shinzaki

==References==
 

==External links==
*   
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 
 
 