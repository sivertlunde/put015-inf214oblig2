Crime File
{{Infobox film
| name           = Crime File
| image          = Crime File (film).jpg
| alt            = 
| caption        = 
| director       = K. Madhu
| producer       = A. Ramakrishnan,sajan varghese 
| writer         = A. K. Sajan A.K. Santhosh Vijayaraghavan Rajan P. Dev
| music          = Rajamani
| cinematography = Saloo George
| editing        = K. Sankunny   
| studio         = 
| distributor    = aiswarya productions
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Crime File is a 1999 Malayalam film by K. Madhu starring Suresh Gopi and Sangita. This film is based on the Sister Abhaya murder case. upon its release the film garned huge controversies for its sensitive subject.Though movie succeded in making money in box office  

==Plot==
Sister Amala (Sangita), a nun is found dead in the well of the convent where she was staying. The police discover that she was murdered. Idamattom Palackal Easow Panickar and his team find out the truth behind Sr. Amalas murder. The suspects include Fr. Clement Kaliyar and the local legislator Monayi and a hitman - Cardinal Carlos. Things get worse when Kaliyar Achan and one of police officers, Ezhuthachan, are killed.

==Cast==
*Suresh Gopi as Idamattom Palackal Easow Panickar I.P.S. (DIG of Police, Central Zone) Vijayaraghavan as Fr. Clement Kaliyar 
*Sangita as Sr. Amala 
*Rajan P. Dev as Mamala Mammachan  Siddique as S.P. Anwar Rawther I.P.S. Janardhanan as Kaliyar Pathrose Vaidyan 
*N. F. Varghese as I.G. James George 
*Babu Namboothiri as Chief Minister
*Jagannatha Varma as Bishop Punnassery 
*Kalabhavan Mani as Ezhuthachan (Police Officer)
*Subair as Dist. Collector Paul Varghese 
*Chandni as Sr. Vimala  Vijayakumar as M.L.A. Monayi 
*Cochin Haneefa as Jamal (Police Officer) Santhosh  as Karthav (Police Officer)
*Mohan Jose as Fr.Rector
*K. B. Ganesh Kumar as Raju Namasivaya 
*Spadikam George as Cardinal Carlos 
* Kundara Johny as Pappi (as Johnnie) 
*Ravi Menon Ashokan as Simon
*Meghanadhan as Stephen 
*M. S. Thripunithura as Namasivaya Swami 
*Priyanka as Deenamma 
*Rizabawa as Thomas 
*Zeenath as Mother Superior 
*Suma Jayaram as Stay Madhavi  Santhakumari as Sr. Fisto

==External links==
*  

 
 
 
 
 
 

 
 