Games '74
 
 
 
{{Infobox film
 | name = Games 74
 | image = 1974 British Commonwealth Games logo.svg
 | caption = Official Games logo
 | director = John King  Sam Pillsbury Paul Maunder Arthur Everard
 | producer = David H. Fowler   Lance J. Connolly
 | writer =  
 | starring = the competitors 
 | music = Malcolm Smith
 | cinematography = 
 | editing = Paul Maunder   Beth Butler   Stanley Harper 
 | distributor =
 | released = 1974 
 | runtime = 106 min. English
 | budget =
 | gross = 
 }}
Games 74 is a 1974 New Zealand–made documentary film of the 1974 British Commonwealth Games, held in Christchurch, New Zealand|Christchurch, New Zealand from 24 January to 2 February 1974. The full title was Games 74: Official Film of the Xth British Commonwealth Games, Christchurch, New Zealand, 1974.

The feature-length documentary in colour and on 35&nbsp;mm was shot and processed by the New Zealand National Film Unit (NFU). The directors became prominent in New Zealand film-making in the next two decades, and one of the location assistants, one Sam Neill, went on to an international career.

The spectator’s-eye camera avoids camera comment. The prime targets are track events, then field events, with “elephantine drama” from the male weightlifters and shots of the marathon which go outside the stadium. There is a surprisingly (for the time) gender-balanced and apolitical narrative which underlines the humanity of the competitors as much as it shows the drama of success and failure” according to Sam Edwards, although “woman’s events receive scant coverage”. Divers are captured in slow motion, and a “series of high jumpers, using similar techniques to cross the bar are edited in collapsed sequence, reminding viewers of salmon exploding up a waterfall”.

The film has been restored and is available on DVD.

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p58 (1997, Oxford University Press, Auckland) ISBN 019 558336 1

==External links==
*  

 
 
 
 
 
 
 
 


 
 