The Franchise Affair (film)
{{Infobox film
| name           = The Franchise Affair
| image          = "The_Franchise_Affair"_(1951).jpg
| caption        = UK theatrical poster
| director       = Lawrence Huntington 
| producer       = Robert Hall
| writer         = Robert Hall Lawrence Huntington
| based on       = the novel by Josephine Tey
| starring       = Michael Denison Dulcie Gray 
| cinematography = Günther Krampf Philip Green
| editor         = Clifford Boote ABPC
| distribution   = Associated British-Pathé
| released       = 20 February 1951
| runtime        = 95 minutes
| country        = United Kingdom English
| gross = £117,966 (UK) 
}} Anthony Nicholls and Marjorie Fielding. It is an extremely faithful adaptation of the novel The Franchise Affair by Josephine Tey. 

==Plot==
In a quiet English town, schoolgirl Betty Kane (Ann Stephens) claims that the owners of an isolated house, spinster Marion Sharpe (Dulcie Gray) and Marions mother (Marjorie Fielding) have kidnapped and beaten her. The police believe Bettys story, but local lawyer Robert Blair (Michael Dennison), a bachelor, is sceptical. Risking ostracism from the community, Blair quietly sets about proving the innocence of the two women.

==Cast==
* Michael Denison - Robert Blair
* Dulcie Gray - Marion Sharpe Anthony Nicholls - Kevin McDermott
* Marjorie Fielding - Mrs Sharpe
* Athene Seyler - Aunt Lin John Bailey - Detective Inspector Grant
* Ann Stephens - Betty Kane
* Hy Hazell - Mrs Chadwick
* Kenneth More - Stanley Peters
* Avice Landone - Mrs Wynn
* Maureen Glynne - Rose Glynn Peter Jones - Bernard Chadwick
* Moultrie Kelsall - Judge
* Martin Boddey - Inspector Hallam
* Patrick Troughton - Bill Brough

==Critical reception==
*The New York Times wrote, "a great many words are spoken and a great deal of tea is consumed in a low-budget British picture, "The Franchise Affair," which made a bedraggled appearance at the Little Carnegie yesterday. And, as may be readily imagined, the sum total of it all is an hour and a half of sheer boredom, unrelieved by any action or surprise."  
*Sky Movies wrote, "a neat, well-constructed whodunit - or, rather, was-it-done? - graced by good performances - it was one of several films husband-and-wife team Michael Denison and Dulcie Gray made together - and a leisurely but literate script. Although modest in ambition, the film sustains its drama throughout and there are some fine moments of spicy, English upper-crust wit. Its courtroom scenes also bring a welcome relief from the Perry Mason style of histrionics. Star-spotters cant miss Kenneth More in a small role."  

==References==
 

==External Links==
 

 

 
 
 
 
 
 


 
 