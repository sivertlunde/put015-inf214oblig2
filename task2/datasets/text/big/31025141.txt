Por la puerta falsa
{{Infobox film
| name           = Por la puerta falsa
| image          = 
| caption        = 
| director       = Fernando de Fuentes
| producer       = Fernando de Fuentes
| writer         =  Mauricio Magdaleno, Fernando de Fuentes
| starring       =  Pedro Armendáriz, Luis Beristáin, Roberto Cañedo
| music          = Raúl Lavista
| cinematography = Jorge Stahl Jr.
| editing        = José W. Bustos
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}}
Por la puerta falsa ("Through the Back Door") is a 1950 Mexican film directed by Fernando de Fuentes.

==Cast==
* 	 Pedro Armendáriz		
* 	 Luis Beristáin		
* 	 Roberto Cañedo		
* 	 Enrique Díaz Indiano		
* 	 Enedina Díaz de León		
* 	 Antonio R. Frausto		
* 	 Ramón Gay		
* 	 Rita Macedo		
* 	 José Muñoz		 Andrea Palma		
* 	 Joaquín Roche		
* 	 Humberto Rodríguez		
* 	 Aurora Ruiz		
* 	 Pepe del Río		
* 	 Eduardo Vivas

==External links==
*  

 
 
 
 

 