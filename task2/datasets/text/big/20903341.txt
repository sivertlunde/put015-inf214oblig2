Shaque
{{Infobox film
| name           = Shaque
| image          = 
| image_size     = 
| caption        = 
| director       = Vikas Desai and Aruna Raje
| producer       = 
| writer         =Vikas Desai (script), Haafiz (dialogue)
| narrator       = 
| starring       =
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1976
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1976 Bollywood drama film directed by Vikas Desai and Aruna Raje. The film stars Vinod Khanna.

==Cast==
*Vinod Khanna ...  Vinod Joshi 
*Shabana Azmi ...  Meena Joshi 
*Utpal Dutt ...  Maan Singh  Bindu ...  Rosita 
*Durga Khote ...  Mrs. Bannerjee 
*Farida Jalal ...  Mrs. Subramaniam 
*Nirmala Mathan   
*Omkar  (as Onkar) 
*Ninad Deshpande   
*Sunita
*Ramji Koli   
*Arvind Deshpande ...  Subramaniam 
*Padmakar Athavale   
*Balmukund Prabhu   
*Gopal Nair 

==References==
 

==External Links==
http://www.imdb.com/title/tt0259571/ 

 
 
 
 
 