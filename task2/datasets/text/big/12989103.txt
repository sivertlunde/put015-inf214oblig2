Crainquebille
{{Infobox film
| name           = Crainquebille
| image          = Crainquebille1922.jpg
| caption        = Theatrical release poster
| director       = Jacques Feyder
| producer       = Jacques Feyder
| writer         = Anatole France (novel) Jacques Feyder
| starring       =
| cinematography = Léonce-Henri Burel Maurice Forster
| editing        = Red Seal Pictures (1923 US release)
| released       =  
| runtime        = 90 minutes (original release) 76 minutes (restored version)
| country        = France
| language       = Silent film
| budget         =
}} realism compared to many other films of the silent era.

==Plot==
 
Jérôme Crainquebille, is an ageing modest vegetable seller who has sold groceries from his cart in Les Halles market in Paris for over 40 years. One day, whilst waiting for a customer to give him his change, he is hassled by a policeman who insists that he moves on.  When he protests, Crainquebille is arrested, supposedly for swearing at the policeman.  Following a farcical trial, the old man is sent to jail, where due to the poor quality of his past life he enjoys the benefits of the free shelter and food.

On his release, however, his life continues to nose-dive: all of his past regular customers shun him, and, with no income, he turns to the bottle becoming an alcoholic. He is reduced to a tramp that everybody loathes, and the sad old man is about to commit suicide when a young street boy called "Mouse" takes him by the hand to forget about the past and persuades him to make a fresh start.

==Cast==
*Maurice de Féraudy as Jérôme Crainquebille 
*Félix Oudart as Lagent 64 
*Jean Forest as La Souris 
*Marguerite Carré as Mme Laure 
*Jeanne Cheirel as Mme Bayard 
*René Worms as M. Lemerle 
*Charles Mosnier  
*Armand Numès  
*Françoise Rosay

==Preservation status==
In 2005, a restored 35mm print was produced by Lobster Films in Paris in association with Lenny Borger,  and was released on DVD by Home Vision Entertainment in 2006. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 

 
 