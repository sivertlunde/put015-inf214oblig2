Tora-san's Sunrise and Sunset
{{Infobox film
| name = Tora-sans Sunrise and Sunset
| image = Tora-sans Sunrise and Sunset.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer =
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Kiwako Taichi
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 109 minutes
| country = Japan
| language = Japanese
| budget =
| gross =
}}

  aka Torasan and the Painter and Tora-sans Sunset Glow  is a 1976 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Kiwako Taichi as his love interest or "Madonna".  Tora-sans Sunrise and Sunset is the seventeenth entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
During his travels, Tora-san meets Ikenouchi, a drunken old man whom he assumes is poor and homeless. Tora-san takes the old man home. When he wakes up, Ikenouchi begins ordering Tora-sans family around in such an authoritarian manner that no one can muster the courage to suggest he leave. On the road again, Tora-san meets Botan, a geisha who has lost her life savings to a dishonest customer. He and his familys neighbor are determined to help her out. It later turns out that Ikenouchi is a famous artist and a drawing he has made for Tora-san is worth ¥70,000.       

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Jūkichi Uno as Ikenouchi
* Kiwako Taichi as Botan (Geisha)
* Masami Shimojō as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Hayato Nakamura as Mitsuo Suwa
* Senri Sakurai as Tourist agency manager
* Yoshiko Okada as Shino
* Akira Terao as Tourist agency employee

==Critical appraisal==
Tora-sans Sunrise and Sunset was the third top Japanese box-office winner of 1976.  The Japanese academic film journal Kinema Junpo named it the second best Japanese release of the year.  For her role in the film Kiwako Taichi was named Best Supporting Actress at both the Hochi Film Awards and the Kinema Junpo Awards ceremonies.  

Stuart Galbraith IV judges Tora-sans Sunrise and Sunset to be one of the best of the Otoko wa Tsurai yo series, singling out the performances of guest stars Kiwako Taichi and Jūkichi Uno.  The German-language site molodezhnaja gives Tora-sans Sunrise and Sunset three and a half out of five stars.   

==Availability==
Tora-sans Sunrise and Sunset was released theatrically on July 24, 1976.  In Japan, the film has been released on videotape in 1996, and in DVD format in 1997 and 2008. 

==References==
 

==Bibliography==

===English===
*  
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 