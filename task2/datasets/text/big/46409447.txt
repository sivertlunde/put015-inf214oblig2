Monte Carlo (1925 film)
Not to be confused with Monte Carlo (1926 film)
{{Infobox film
| name = Monte Carlo
| image =
| image_size =
| caption =
| director = Louis Mercanton
| producer = A.C. Bromhead   R.C. Bromhead
| writer =  E. Phillips Oppenheim  (novel)   Louis Mercanton
| starring = Carlyle Blackwell   Betty Balfour   Rachel Devirys
| music = 
| cinematography = Léon Wladimir Batifol   
| editing =     
| studio = Phocea Film 
| distributor = Phocéa Location
| released = 18 December 1925
| runtime = 
| country = France French intertitles
| budget =
| gross =
}} silent drama film directed by Louis Mercanton and starring Carlyle Blackwell, Betty Balfour and Rachel Devirys. The film is based on a novel of the same title by E. Phillips Oppenheim.  The casting of Blackwell and Balfour in leading roles was intended to give the film appeal in the British market.

==Cast==
*  Carlyle Blackwell as Sir Hargrave Wendever  
* Betty Balfour as Betty Oliver 
* Rachel Devirys as Madame de Fontanes 
* Jean-Louis Allibert as Robert Hewitt 
* Charles Lamy as Marquis de Villiers 
* Jean Aymé as Senor Trentino 
* Georges Térof as Wilson 
* Louis Kerly as Brandon   Noblet as Lord Pellingham  
* Henriette Clairval-Terof as Janitor   Robert English as Sir Philip Gorse  Lane as  Gregory Marston

== References ==
 

==Bibliography==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 