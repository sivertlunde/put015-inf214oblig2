Ramona (1936 film)
 
{{Infobox film
| name           = Ramona
| image          = Poster of Ramona (1936 film).jpg
| caption        = Theatrical release poster Henry King John Stone Sol M. Wurtzel
| based on       =  
| writer         = Stuart Anthony Paul Hervey Fox Sonya Levien Lillian Wurtzel
| screenplay     = Lamar Trotti
| starring       = Loretta Young Don Ameche Alfred Newman
| cinematography = William V. Skall
| editing        = Alfred DeGaetano
| distributor    = 20th Century Fox
| released       =  
| runtime        = 84 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 Henry King,    based on Helen Hunt Jacksons 1884 novel Ramona. This was the third adaptation of the film, and the first one with sound. It starred Loretta Young and Don Ameche.

The New York Times praised its use of new Technicolor technology but found the plot "a piece of unadulterated hokum." It thought "Ramona is a pretty impossible rôle these heartless days" and Don Ameche "a bit too Oxonian" for a tribal chief|chiefs son. 

==Plot==
 
Ramona (Loretta Young), is a half-Indian girl who falls in love with the young man Felipe Moreno (Kent Taylor).

==Cast==
* Loretta Young - Ramona
* Don Ameche - Alessandro
* Kent Taylor - Felipe Moreno
* Pauline Frederick - Señora Moreno
* Jane Darwell - Aunt Ri Hyar
* Katherine DeMille - Margarita (as Katherine de Mille)
* Victor Kilian - Father Gaspara
* John Carradine - Jim Farrar
* J. Carrol Naish - Juan Can
* Pedro de Cordoba - Father Salvierderra
* Charles Waldron - Dr. Weaver
* Claire Du Brey - Marda

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 