Ilavelpu
{{Infobox film
| name           = Ilavelpu
| image          =
| image_size     =
| caption        =
| director       = D. Yoganand
| producer       = L. V. Prasad
| writer         = Pinisetty Stirama Murthy  (Dialogues ) 
| story          = Vempati Sadasivabrahmam
| narrator       =
| starring       = Akkineni Nageswara Rao Anjali Devi Relangi Venkata Ramaiah Sri Sri (Lyrics)
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India Telugu
| budget         =
}} Tamil film, Edhir Paradhathu (1954). Ilavelpu was a big hit and was remade in Hindi as Sharada (1957 film)|Sharada (1957), starring Meena Kumari and Raj Kapoor. 

==Plot==
Sekhar (Akkineni Nageswara Rao), a wealthy man, goes to comfort his friend (Chalam) in a nature cure hospital. He is impressed with the services of Sharada (Anjali Devi), and the two of them fall in love. Sekhar goes abroad for higher education, and they plan to marry after he returns; however, his elders consider marrying him to Lakshmi (Jamuna Ramanarao). Sekhars plane crashes and everyone believes him dead. Sharadas father looks after the agricultural works of Sekhars father (Gummadi Venkateswara Rao). One day he visits their house and discovers the condition of Sekhars family and orphaned brothers and sisters. He and the elders think that if Sharada is married to Sekhars father, the family will be taken care of, so Sharada marries him.

It turns out that Sekhar had escaped from the plane crash alive. He meets Sharada, and when he finds out about her marriage to his father, he gets angry. Sharada is also upset, but controls her emotions. Sekhar becomes an alcoholic, and to get him out of this addiction, he marries Lakshmi. Lakshmi later learns of the love affair between Sekhar and Sharada and starts harassing Sekhar, forcing him to leave the house. During a pilgrimage with her husband, Sharada finds Sekhar unconscious near a temple and gets him to the house. Sharada prays to God and fasts for Sekhars wellbeing. Finally, Sekhar and Sharada die.

==Cast==
* Akkineni Nageswara Rao  as  Sekhar
* Anjali Devi  as  Sharada
* Jamuna Ramanarao  as  Lakshmi
* Gummadi Venkateswara Rao  as  Sekhars Father
* Relangi Venkata Ramaiah
* Chalam
* Ramana Reddy Krishna Kumari Suryakantham
* Dr. Sivaramakrishnaiah
* R. Nageswara Rao

==Soundtrack==
* Song "Challani Punnami Vennelalone" was picturised on Relangi Venkataramaiah
 {{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Singer(s)
| total_length    = 

| all_writing     = 
| all_lyrics      = 
| all_music       = Susarla Dakshinamurthi

| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 

| title1          = Anna Anna Vinnava Chinni Krishnudu
| lyrics1         = 
| extra1          = Jikki Krishnaveni

| title2          = Challani Punnami Vennelalone
| lyrics2         = 
| extra2          = Susarla Dakshinamurthi

| title3          = Challani Raja O Chandamama
| lyrics3         = Vaddadi
| extra3          = Raghunath Panigrahi, P. Susheela, P. Leela

| title4          = Jana Gana Mangala Dayaka Ramam
| lyrics4         = 
| extra4          = P. Leela, chorus

| title5          = Neemamu Veedi Agnanamuche Palu Badhalu Padanela
| lyrics5         = 
| extra5          = P. Leela, chorus

| title6          = Swargamanna Vere Kalada
| lyrics6         = 
| extra6          = P. Leela

| title7          = Yenadu Kanaledu Ee Vinta Sundarini
| lyrics7         = 
| extra7          = Raghunath Panigrahi
}}

==References==
 

==External links==
* 

 
 
 
 
 
 