Ami Aaj Nasto Hoye Jai
{{Infobox film
| name           = Ami Aaj Nasto Hoye Jai
| image          = Ami Aaj Nasto Hoye Jai poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Srikanta
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  See below
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 123 minutes
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}

Ami Aaj Nasto Hoye Jai is a 2013 Bengali film.

== Plot ==
The film commences as a photographer finds an unconscious girl on a sea beach. The girl is introduced as Mayurakshi (Piyali), who was trying to get herself over a dark past. The photographer tried to disentangle the mysteries of Mayurakshis past, so that he can help her in returning to normal life.

It was revealed that the girl had a family consisting of her parents, her brother and sister and her fiance. But everything gets spoiled with the death of Mayurakshis mother. Aru, her brother, gets into bad company and involves himself in a smuggling business and finally, gets shot. Later, her father dies with a heart attack after losing his job. Mayurakshi get into a deep situation of financial crisis with unpaid debts. To overcome this, she finally decides to involve herself in a destructive plan, made by her evil fried Ujan (Swaraj), who was a journalist by profession. However, this incident only adds fuel to the flame. Mayurakshis life faces an even worse situation. The events which occur next form the climax of the story.

== Cast ==
* Piyali as Mayurakshi
* Swaraj as Ujan
* Anamika Saha
* Mrinal Mukherjee
* Bodhisatya Majumder
* Arun Banerjee

== References ==
 

 
 


 