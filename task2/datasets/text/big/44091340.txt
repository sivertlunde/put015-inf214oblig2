Thaalappoli
{{Infobox film
| name           = Thaalappoli
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       =
| writer         = Kalanilayam Cheri Viswanath (dialogues)
| screenplay     = Meena Sudheer Sudheer
| music          = V. Dakshinamoorthy
| cinematography =
| editing        =
| studio         = Kalanilayam Films
| distributor    = Kalanilayam Films
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, Meena and Sudheer in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
*Prameela
*Sankaradi Meena
*Sudheer Sudheer
*Vincent Vincent

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Cheri Viswanath and Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ini Njan Karayukayilla || P Susheela || Cheri Viswanath || 
|-
| 2 || Priyasakhi Poyvaroo || K. J. Yesudas || Cheri Viswanath || 
|-
| 3 || Purushantharangale || P Susheela || Mankombu Gopalakrishnan || 
|-
| 4 || Sreevaazhum Kovilil || Vani Jairam, Chorus || Cheri Viswanath || 
|-
| 5 || Vrischikakkaatte || K. J. Yesudas || Cheri Viswanath || 
|}

==References==
 

==External links==
*  

 
 
 

 