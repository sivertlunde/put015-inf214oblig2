Follow the Sun (film)
{{Infobox film
| name           = Follow the Sun
| image          = Followthesunposter.jpg
| caption        = Theatrical release poster
| director       = Sidney Lanfield
| producer       = Samuel G. Engel (uncredited) 
| writer         = Frederick Hazlitt Brennan 
| narrator       = 
| starring       = Glenn Ford Anne Baxter Dennis OKeefe June Havoc
| music          = 
| cinematography = 
| editing        = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,150,000 (US rentals) 
}}
Follow the Sun is a 1951 biographical film of the life of golf legend Ben Hogan. It stars Glenn Ford as Hogan and Anne Baxter as his wife. Many golfers and sports figures of the day appear in the movie.

==Plot summary==
In Fort Worth, Texas, Ben Hogan (Glenn Ford) works as a golf caddy to help support his family. He romances and marries Valerie Fox (Anne Baxter), then with her support, decides to become a professional golfer. At first, Hogan has little success, but he gradually improves. Meanwhile, the Hogans become friends with Chuck Williams (Dennis OKeefe), a popular fellow pro.

After serving in the military for World War II, Hogan returns to golfing and eventually becomes a top player. However, he has acquired an image of a robotic, cold competitor.

After winning a tournament (beating Williams), Hogan is very seriously injured in a car accident. Doctors hold out little hope for him walking, let alone golfing again. During his convalescence, Hogan is amazed to by the outpouring of regard from his fans. Through sheer determination, he recovers and goes on to become one of the great golfers of his time.

==Cast==
* Glenn Ford ...  Ben Hogan 
* Anne Baxter ...  Valerie Hogan 
* Dennis OKeefe ...  Chuck Williams 
* June Havoc ...  Norma Williams
* Larry Keating ...  Sportswriter Jay Dexter 
* Roland Winters ...  Dr. Graham 
* Nana Bryant ...  Sister Beatrice 
* Harold Blake ...  Ben Hogan, Age 14 
* Ann Burr ...  Valerie, Age 14  
* Jimmy Demaret ...  Himself 
* Cary Middlecoff ...  Himself  
* Grantland Rice ...  Himself, Toastmaster  
* Sam Snead ...  Himself

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 