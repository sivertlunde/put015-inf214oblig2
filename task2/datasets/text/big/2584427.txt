The Triumph of Hercules
{{Infobox film
| name        = The Triumph of Hercules
| image       =
| writer      = Alberto De Martino Roberto Gianviti
| starring    = Dan Vadis  Marilù Tolo Pierre Cressoy Moira Orfei
| director    = Alberto De Martino
| producer    =
| distributor =
| released    =  
| country     = Italy
| runtime     = 94 mins
| language    = Italian English translation
| budget      =
| music       =
| awards      =
}}
The Triumph of Hercules, the 1964 film, was one of many Italian sword and sandal epics during the 1960s craze. Originally titled Il Trionfo di Ercole, the film was directed by Alberto De Martino. Hercules was portrayed by Dan Vadis. 
The film was released internationally as Hercules vs the Giant Warrior, and also as Hercules and the Ten Avengers. 
 
==Plot== Juno (Hera), who dislikes Hercules "for certain family reasons." Hercules also fights to protect his princess love, but matters are complicated by her evil relative Milo, who has murdered her father the king but has tricked her into thinking it was someone else because she trusts him too much. Milo tricks Hercules into killing his friend, for which he is punished by his father Jove (Jupiter (mythology)|Jupiter/Zeus) who takes away his enormous strength, because he has misused it. Milo is conspiring with his sorceress mother to take the throne for himself.

Now only as strong as a mortal man, Hercules is overpowered when he tries to rescue the princess Ate (pronounced Ah-tay) from Milo and is then accused of being a fraud. Milo tries to kill both Hercules and Ate by chaining them to a giant wooden lever with spikes, complete with a cage over Hercules which he must support while rocks are piled on him: if he can hold it up, then he will prove himself the true Hercules (which he is) but if not, then he will be guilty and he and Ate will die together for conspiring to steal the throne and give it to an imposter.

As the weight of the stones and rocks drive the de-powered Hercules to his knees, he sincerely begs his father Jove to let him die if it is his destiny but to spare Ate who is innocent. Jove resolves this by restoring Herculess demi-god strength, allowing him to overpower the soldiers, save Ate, and thwart Milo along with his seven golden giants and evil mother. The two bumbling thieves also prove useful in the climax.

In the end, Hercules marries Ate and becomes king of her city.

==Production==
Marilu Tolo was given tinted lenses to play a malicious doppelgänger Hercules love interest Princess Ate.
 

==Biography==
* 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 
 