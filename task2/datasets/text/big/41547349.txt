Boys Will Be Girls (film)
{{Infobox film
| name =  Boys Will Be Girls 
| image =
| image_size =
| caption =
| director = Gilbert Pratt
| producer = Joe Rock
| writer = Evelyn Barrie   Jack Byrd   Syd Courtenay   Clifford Grey   H. F. Maltby
| narrator =
| starring = Leslie Fuller   Nellie Wallace   Greta Gynt   Georgie Harris
| music = 
| cinematography = Cyril Bristow
| editing = 
| studio = Leslie Fuller Productions
| distributor = BIED
| released = July 1937
| runtime = 70 minutes 
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} independent production company in the Rock Studios at Elstree Studios|Elstree.  In order to gain his inheritance, a man has to give up drinking and smoking.

==Cast==
* Leslie Fuller as Bill Jenkins 
* Nellie Wallace as Bertha Luff 
* Greta Gynt as Roberta 
* Georgie Harris as Roscoe 
* Judy Kelly as Thelma 
* D. J. Williams (actor)|D.J. Williams as George Luff 
* Toni Edgar-Bruce as Mrs. Jenkins 
* Constance Godridge as Ernestine 
* Syd Crossley as Nolan 
* Syd Courtenay as Bookum

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The British of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 


 