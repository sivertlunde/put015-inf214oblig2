Summer Palace (film)
{{Infobox film
| name           = Summer Palace 
| image          = SummerPalace_USrelease.jpg
| caption        = Promotional Poster for Summer Palace
| director       = Lou Ye
| producer       = Sylvain Bursztejn Fang Li Nai An
| writer         = Lou Ye Feng Mei Ma Yingli
| starring       = Hao Lei Guo Xiaodong
| music          = Peyman Yazdanian
| cinematography = Hua Qing
| editing        = Lou Ye Zeng Jian
| distributor    =
| released       =  
| runtime        = 140 minutes
| country        = China
| language       = Mandarin German
| budget         =
| film name = {{Film name| jianti         = 颐和园|
| fanti          = 頤和園
| pinyin         = Yíhé Yuán}}
}} director Lou Dream Factory, Laurel Films, Fantasy Pictures and Sylvain Bursztejns Rosem Films. It was made in association with Frances Minister of Culture (France)|Ministère de la Culture et de la Communication, Minister of Foreign Affairs (France)|Ministère des Affaires Étrangères and Centre National de la Cinématographie (CNC). The film is the first from mainland China to feature the full-frontal adult nudity of both its male and female leads,  though earlier films such as Xiao Wu (1998),  Lan Yu Star Appeal (2004),  have featured full-frontal adult male nudity.

The film deals with a young student played by Hao Lei who leaves her small hometown to study at the fictional "Beiqing University" (a homage to Peking University). There she meets a fellow student and begins an intense romantic relationship in the backdrop of the Tiananmen Square protests of 1989. The film also follows the eventual disillusionment of these young idealists after the crackdown, as the years progress through the 1990s and into the 2000s (decade). The film is named after the Summer Palace located in Beijing.
 sex scenes and political undertones made the film tinder for controversy in China, leading both the director, Lou Ye, and his producers into conflict with Chinas State Administration of Radio, Film, and Television (SARFT). After screening Summer Palace in the 2006 Cannes Film Festival without government approval, the film was placed under a de facto ban in Mainland China, and its filmmakers officially censored.

== Plot ==
Spanning several cities and over a decade, Summer Palace tells the story of Yu Hong (played by Hao Lei), a young woman from the border-city of Tumen, Jilin|Tumen, who is accepted to the fictional Beiqing University, a name that evokes either Peking University ("Beida") or Tsinghua University ("Qinghua"). While in school, Yu Hong meets Li Ti, her best friend (played by Hu Lingling), and Zhou Wei, her college boyfriend and the love of her life (played by Guo Xiaodong). The film is divided into two parts. The first begins in the late 1980s (subtitles inform the audience of the place and year at various points in the film), as Yu Hong enters the university. Lonely and isolated despite the cramped living conditions, Yu Hong eventually befriends another student, Li Ti, who introduces her to her boyfriend Ruo Gu (played by Zhang Xianmin), and Ruo Gus friend Zhou Wei. Yu Hong and Zhou Wei embark upon a passionate but volatile love affair just as political forces are moving towards Tiananmen Square.
 crackdown occurs on the students on Tiananmen Square and on the campus of Beida. During all of this, Yu Hongs old boyfriend Xiao Jun (played by Cui Jin) from Tumen arrives and the two of them leave, Yu Hong deciding that she will drop out from the university.
 Hong Kong handover. Yu Hong has left Tumen again, first for Shenzhen, and then for the central China city of Wuhan, while Li Ti and Ruo Gu have moved to Berlin. Yu Hong is unable to forget Zhou Wei, and has empty affairs with a married man and a kind but quiet mailroom worker. The film follows her disaffection with society and her use of sex as a substitute for contentment. Eventually discovering that she is pregnant, Yu Hong gets an abortion and moves to Chongqing where she marries.

Li Ti, Ruo Gu, and Zhou Wei, meanwhile, live a quiet life as expatriates in Berlin. While Li Ti and Zhou Wei still occasionally make love, the former quietly realizes that the latter does not love her. Though the three friends appear happy, when Zhou Wei plans to return home to China and settle in the city of Chongqing, Li Ti suddenly commits suicide. There he connects with former classmates who in turn point him to Yu Hongs email address.

After more than ten years, Zhou Wei and Yu Hong at last reunite in the resort city of Beidaihe. While they embrace, they ask each other, "Now what?" When Yu Hong leaves, ostensibly to buy drinks, Zhou Wei understands that they can never be together and leaves as well.

==Cast==
*Hao Lei as Yu Hong - the films heroine, a young student at the fictional Beiqing University from the small town of Tumen, Jilin on the North Korean-Chinese border. Yu Hong is a willful young woman who desires to live life more intensely. Her love affair with the character of Zhou Wei serves as the basis of the film.
*Guo Xiaodong as Zhou Wei - Yu Hongs love interest, another student at the same university. Something of an intellectual, Zhou Wei is both deeply in love with Yu Hong and prone to infidelity. When the Tiananmen protests arrive, he like his fellow students join in the movement.
*Hu Lingling as Li Ti - Yu Hongs best friend and eventual rival. Li Ti, an English-language major at the same university, is the first to befriend the sullen, quiet Yu Hong. Though considered a cynic, she harbors a romantic side as well.
*Zhang Xianmin as Ruo Gu - Li Tis boyfriend, a student studying abroad in Berlin.
*Cui Lin as Xiao Jun - Yu Hongs high school boyfriend from Tumen.
*Bai Xueyun as Wang Bo - Yu Hongs lover in Wuhan.

== Release ==

=== Theatrical ===
Summer Palace premiered at the Cannes Film Festival on May 18, 2006, and was released theatrically in France a year later on April 18, 2007 by Océan Films, under the title Une Jeunesse Chinoise (in English, "A Chinese Youth").    The film received its American debut in the Mill Valley Film Festival on October 10, 2006,  and a limited theatrical release beginning on January 18, 2008 through distributor Palm Pictures. 

===Home media=== Region 2 DVD in France on January 28, 2008.  The single disc edition includes the film in its original Mandarin with French subtitles, along with special features such as a documentary film|making-of documentary, a featurette on censorship, Lou Yes film notes, and cast and crew biographies. 

A Region 1 DVD was released in the United States on March 11, 2008 by Palm Pictures. 

==Reception== Irish film, The Wind Toronto  Mill Valley.   

Critics were generally positive in reviews, citing the films ambition and scope with the most common complaint being the films excessive length at 140 minutes. Derek Elley of Variety (magazine)|Variety claimed the film was "half an hour too long." {{cite web | url =
http://www.variety.com/index.asp?layout=features2006&content=jump&jump=review&dept=cannes&nav=RCannes&articleid=VE1117930547
 | title = Summer Palace| accessdate = 2007-08-13| author = Elley, Derek |date= 2006-05-18 | publisher = Variety (magazine)|Variety}}  The Daily Telegraph, meanwhile, also mentioned the "thirty minutes too long" complaint but stated that the film was nevertheless "a raw and unsettling new work." {{cite web | url =
http://www.telegraph.co.uk/arts/main.jhtml?xml=/arts/2006/05/19/bfcannes19.xml
 | title = Cannes 2006: love in Paris and hatred in Ireland| accessdate = 2007-08-13| author = Sandhu , Sukhdev |date= 2006-05-19 | publisher = The Daily Telegraph}}  The Guardian also found the film "over-long and meandering," but also "stylish   atmospheric." {{cite web | url =
http://film.guardian.co.uk/cannes2006/story/0,,1779573,00.html
 | title = Give Pedro the prize| accessdate = 2007-08-13| author = Solomons, Jason |date= 2006-05-21| publisher = The Guardian}} 

The New York Times gave a particularly glowing review for the film with film critic A. O. Scott writing that "...  its 2-hour-20-minute length, Summer Palace moves with the swiftness and syncopation of a pop song. Like Jean-Luc Godard in the 1960s, Mr. Lou favors breathless tracking shots and snappy jump cuts, and like Mr. Godard’s, his camera is magnetized by female beauty."   
 David Denby of The New Yorker noted that he never seen so much lovemaking in an "aboveground" film,    however, he also noted that these scenes are not pornographic, that is, never separated from emotion.  Joshua Rothkopf  and V.A. Musetta    speculate that the male and female full-frontal nudity caused the ban in China.

===Ban and controversy===
The film was in competition at the 2006 Cannes Film Festival but failed to garner any awards.  Though Summer Palace was the only Asian film in competition for the Palme dOr, Lou and his producers had not received approval from Chinese censors, thus instigating an official censure by the Chinese State Administration of Radio, Film, and Television (SARFT).    Ultimately, both Lou and his producer, Nai An, were forbidden by the Chinese Government to make any new films for five years.  

Besides the filmmakers, Summer Palace itself was de facto banned when SARFT refused to grant a certificate to distribute in the Mainland, though Lou claimed the ban was due to "technical reasons" in that the film was not up to the official standards for picture and sound quality. {{cite web | url =
http://www.variety.com/index.asp?layout=features2007&content=jump&jump=story&dept=berlin&nav=FBberlin&articleid=VR1117958975
 | title = Banned filmmaker is a relative term| accessdate = 2007-04-29 | author = Jones, Arthur  |date= 2007-02-08 | publisher = Variety (magazine)|Variety}} 

==See also== Li Yus 2007 film, also produced by Fang Lis Laurel Films, which like Summer Palace, was banned by Chinese authorities. Banned films, China
* Censorship in the Peoples Republic of China
* List of Chinese films of 2006 Nudity in film (East Asian cinema since 1929)

==References==
 

==External links==
*   from distributor Palm Pictures
*  
*  
*  
*  
*  
*   at MonkeyPeaches

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 