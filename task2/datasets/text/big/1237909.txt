Sweet Sweetback's Baadasssss Song
 
{{Infobox film
| name           = Sweet Sweetbacks Baadasssss Song
| image          = Sweet sweetback poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Melvin Van Peebles
| producer       = Melvin Van Peebles Jerry Gross
| writer         = Melvin Van Peebles
| starring       = Melvin Van Peebles
| music          = Melvin Van Peebles
| cinematography = Bob Maxwell
| editing        = Melvin Van Peebles
| distributor    = Cinemation Industries
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $150,000   
| gross          = $15.2 million 
}}
 independent drama picaresque story white authority. Van Peebles began to develop the film after being offered a three-picture contract for Columbia Pictures. No studio would finance the film, so Van Peebles funded the film himself, shooting it independently over a period of 19 days, performing all of his own stunts and appearing in several unsimulated sex scenes. He received a $50,000 loan from Bill Cosby to complete the project. The films fast-paced montages and jump-cuts were unique features in American cinema at the time. The picture was censored in some markets, and received mixed critical reviews. However, it has left a lasting impression on African-American cinema.
 soundtrack album Hollywood that films which portrayed "militant" blacks could be highly profitable, leading to the creation of the blaxploitation genre, although some do not consider this example of Van Peebles work to be an exploitation film.

==Plot== Los Angeles LAPD officers Black Panther named Mu-Mu (Hubert Scales). They handcuff him to Sweetback, but when Mu-Mu insults the officers, they take both men out of the car, undo the handcuff from Mu-Mus wrist, and beat him. In response, Sweetback uses the handcuffs, still hanging from his wrist, to beat the officers into unconsciousness.

The remainder of the film chronicles Sweetbacks flight through South Central Los Angeles towards the United States–Mexico border. Sweetback is captured by the police and violently interrogated about his previous assault on the arresting officers, but he escapes when a riot breaks out. Sweetback goes to a woman who cuts his handcuffs off in exchange for sex. With his handcuffs off, Sweetback continues onward, only to be captured by a chapter of the Hells Angels. The female leader of the gang is impressed by the size of Sweetbacks penis, and agrees to help him and Mu-Mu escape from the police in exchange for sex. The police find Sweetback and Mu-Mu at the bikers hangout, but Sweetback escapes on foot while Mu-Mu goes away with the bikers. Mu-Mu and one of the bikers (John Amos) are killed. After his escape from the bikers hangout, a white man sympathetic to Sweetbacks cause agrees to switch clothes with him, allowing the usually velour-clad Sweetback to blend in. The police find Sweetbacks former foster mother, who reveals that Sweetbacks birth name is Leroy.  The film concludes in the desert, where the L.A. police send several hunting dogs after Sweetback. He makes it into the Tijuana River, and escapes into Mexico, swearing to return to "collect dues".

==Production==
After Melvin Van Peebles had completed  , 2003. ISBN 1-57829-750-8  Because no studio would finance the film, Van Peebles put his own money into the production, and shot it independently. Van Peebles was given a $50,000 loan by Bill Cosby to complete the film. "Cosby didnt want an equity part," according to Van Peebles. "He just wanted his money back." Van Peebles wound up with controlling ownership of the film. Several actors auditioned for the lead role of Sweetback, but told Van Peebles that they wouldnt do the film unless they were given more dialogue. Van Peebles ended up playing the part himself. 
 stunt man, he performed all of the stunts himself, which also included appearing in several unsimulated sex scenes. At one point in the shoot, Van Peebles was forced to jump off a bridge. Bob Maxwell later stated, "Well, thats great, Mel, but lets do it again." Van Peebles ended up performing the stunt nine times. Van Peebles contracted gonorrhea when filming one of the many sex scenes, and successfully applied to the Directors Guild of America|Directorss Guild in order to get workers compensation because he was "hurt on the job." Van Peebles used the money to purchase more film. 

Van Peebles and several key crew members were armed because it was dangerous to attempt to create a film without the support of the union. One day, Van Peebles looked for his gun, and failed to find it. Van Peebles found out that someone had put it in the prop box. When they filmed the scene in which Beetle is interrogated by police, who fire a gun next to both of his ears, it was feared that the real gun would be picked up instead of the prop. While shooting a sequence with members of the Hells Angels, one of the bikers told Van Peebles they wanted to leave; Van Peebles responded by telling them they were paid to shoot until the scene was over. The biker took out a knife and started cleaning his fingernails with it. In response, Van Peebles snapped his fingers, and his crewmembers were standing there with rifles. The bikers stayed to shoot the scene. 

Van Peebles had received a permit to set a car on fire, but had done so on a Friday; as a result, there was no time to have it filed before shooting the scene. When the scene was shot, a fire truck showed up. This ended up in the final cut of the film. 

===Directing===
Van Peebles stated that he approached directing the film "like you do the cupboard when youre broke and hungry: throw in everything eatable and hope to come out on top with the seasoning, i.e., by editing."  Van Peebles stated that "story-wise, I came up with an idea, why not the direct approach.   To avoid putting myself into a corner and writing something I wouldnt be able to shoot, I made a list of the givens in the situation and tried to take those givens and juggle them into the final scenario."   

Van Peebles wanted "a victorious film   where niggers could walk out standing tall instead of avoiding each others eyes, looking once again like theyd had it." Van Peebles was aware of the fact that films produced by major studios would appear to be more polished than low-budget independently made features, and was determined to make a film that "  as good as anything one of the major studios could turn out." 

Van Peebles knew that in order to spread his message, the film "simply couldnt be a didactic discourse which would end up playing   to an empty theater except for ten or twenty aware brothers who would pat me on the back and say it tells it like it is" and that "to attract the mass we have to produce work that not only instructs but entertains". Van Peebles also wanted to make a film that would "be able to sustain itself as a viable commercial product     aint about to go carrying no messages for you, especially a relevant one, for free." 

Van Peebles wanted half of his shooting crew "to be third world people.   So at best a staggering amount of my crew would be relatively inexperienced.   Any type of film requiring an enormous technical sophistication at the shooting stage should not be attempted." Van Peebles knew that gaining financing for the film would not be easy and expected "a great deal of animosity from the film media (white in the first place and right wing in the second) at all levels of filmmaking", thus he had to "write a flexible script where emphasis could be shifted. In short, stay loose." 

===Editing=== montages and jump cut|jump-cuts were novel features for an American movie at the time. Stephen Holden from The New York Times commented that the films editing had "a jazzy, improvisational quality, and the screen is often streaked with jarring psychedelic effects that illustrate Sweetbacks alienation." {{cite news|title=FILM VIEW; Sweet Sweetbacks World Revisited
|work= The New York Times|date=July 2, 1995|url= http://www.nytimes.com/1995/07/02/movies/film-view-sweet-sweetback-s-world-revisited.html|accessdate=January 11, 2012|first=Stephen|last=Holden}}  In The 50 Most Influential Black Films: A Celebration of African-American Talent, Determination, and Creativity, author S. Torriano Berry writes that the films "odd camera angles, superimpositions, reverse-key effects, box and matting effects, rack-focus shots, extreme zooms, stop-motion and step-printing, and an abundance of jittery handheld camera work all helped to express the paranoid nightmare that   life had become." 

===Music===
 
Since Van Peebles did not have the money to hire a composer, he composed the films music score himself. Because he did not know how to read or write music, he numbered all of the keys on a piano so he could remember the melodies.  Van Peebles stated that "Most filmmakers look at a feature in terms of image and story or vice versa. Effects and music   are strictly secondary considerations. Very few look at film with sound considered as a creative third dimension. So I calculate the scenario in such a way that sound can be used as an integral part of the film." 
 sampling in hip hop music. 

Van Peebles recalls that "music was not used as a selling tool in movies at the time. Even musicals, it would take three months after the release of the movie before they would bring out an album." Because Van Peebles did not have any money for traditional advertising methods, he decided that by releasing a soundtrack album in anticipation of the films release, he could help build awareness for the film with its music.   

==Release and alterations==
The film was released on April 23, 1971. Melvin Van Peebles stated that "at first, only two theaters in the United States would show the picture: one in Detroit, and one in Atlanta. The first night in Detroit, it broke all the theaters records, and that was only on the strength of the title alone, since nobody had seen it yet. By the second day, people would take their lunch and sit through it three times. I knew that I was finally talking to my audience. Sweet Sweetbacks Baadasssss Song made thousands of dollars in its first day."  The film grossed $15,000,000+ at the box office. 
 X rating Region 2 BFI Video has the opening sex sequences altered. A notice at the beginning of the DVD states "In order to comply with UK law (the Protection of Children Act 1978), a number of images in the opening sequence of this film have been obscured." 

==Response== Kevin Thomas in the Los Angeles Times described the film as "a series of stark, earthy vignettes, Van Peebles evokes the vitality, humor, pain, despair and omnipresent fear that is life for so many African-Americans".  Stephen Holden in The New York Times called it "an innovative, yet politically inflammatory film."  The film website Rotten Tomatoes, which compiles reviews from a wide range of critics, gives the film a score of 73% "Fresh". 
 The Black Panther to the films revolutionary implications,     celebrated and welcomed the film as "the first truly revolutionary Black film made   presented to us by a Black man."    Newton wrote that Sweetback "presents the need for unity among all members and institutions within the community of victims," contending that this is evidenced by the opening credits which state the film stars "The Black Community," a collective protagonist engaged in various acts of community solidarity that aid Sweetback in escaping. Newton further argued that "the film demonstrates the importance of unity and love between Black men and women," as demonstrated "in the scene where the woman makes love to the young boy but in fact baptizes him into his true manhood."  The film became required viewing for members of Black Panther Party. 
 Lerone Bennett responded with an essay on the film in Ebony (magazine)|Ebony, titled "The Emancipation Orgasm: Sweetback in Wonderland," in which he discussed the films "black aesthetic". Bennett argued that the film romanticized the poverty and misery of the ghetto and that "some men foolishly identify the black aesthetic with empty bellies and big bottomed prostitutes." Bennett concluded that the film is "neither revolutionary nor black because it presents the spectator with sterile daydreams and a superhero who is ahistorical, selfishly individualist with no revolutionary program, who acts out of panic and desperation." Bennett described Sweetbacks sexual initiation at ten years old as the "rape of a child by a 40-year-old prostitute." Bennett described instances when Sweetback saved himself through the use of his sexual prowess as "emancipation orgasms" and stated that "it is necessary to say frankly that nobody ever fucked his way to freedom. And it is mischievous and reactionary finally for anyone to suggest to black people in 1971 that they are going to be able to screw their way across the Red Sea. Fucking will not set you free. If fucking freed, black people would have celebrated the millennium 400 years ago."   
 Black nationalist poet and author Haki R. Madhubuti (Don L. Lee) agreed with Bennetts assessment of the film, stating that it was "a limited, money-making, auto-biographical fantasy of the odyssey of one Melvin Van Peebles through what he considered to be the Black community."    The New York Times critic Clayton Riley viewed the film more favorably, commenting on its aesthetic innovation, but stated of the character of Sweetback that he "is the ultimate sexualist in whose seemingly vacant eyes and unrevealing mouth are written the protocols of American domestic colonialism." In another review, Riley explained that "Sweetback, the profane sexual athlete and fugitive, is based on a reality that is Black. We may not want him to exist but he does." Critic Donald Bogle states in a New York Times interview that the film in some ways met the black audiences compensatory needs after years of asexual, Sidney Poitier-type characters and that they wanted a "viable, sexual, assertive, arrogant black male hero." 

==Legacy== Super Fly. House Party?"  In 2004, Mario Van Peebles directed and starred as his father in Baadasssss!, a biopic about the making of Sweet Sweetback. The film was a critical but not commercial success.  

==References==
 

==Further reading==
*  

== External links ==
*  
*  
*  
*   at The National Visionary Leadership Project

 

 

 
 
 
 
 
 
 
 
 
 