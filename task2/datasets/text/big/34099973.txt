The Bottle Imp (1917 film)
{{infobox film
| name           = The Bottle Imp
| image          = The Bottle Imp1917-newspaperad.jpg
| imagesize      =
| caption        = An advertisement for the film.
| director       = Marshall Neilan
| producer       = Jesse Lasky
| writer         = Charles Maigne(scenario)
| based on       =  
| cinematography = Walter Stradling
| editing        =
| distributor    = Paramount Pictures
| released       = March 26, 1917
| runtime        = 50 minutes (5 reels)
| country        = United States
| language       = Silent film(English intertitles)
}} silent fantasy film produced by Jesse Lasky and distributed through Paramount Pictures. It is taken from the Robert Louis Stevenson short story, "The Bottle Imp". The movie was directed by Marshall Neilan in Hawaii and stars Japanese actor Sessue Hayakawa. 

Prints are held by George Eastman House and Cinematheque Francais, Paris. 

==Cast==
*Sessue Hayakawa – Lopaka
*Lehua Waipahu – Kokua
*H. Komshi – Keano
*George Kuwa – Makale
*Guy Oliver – Rollins
*James Neill – A Priest
*Margaret Loomis (uncredited)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 

 
 