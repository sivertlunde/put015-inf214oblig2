O Teri
 
{{Infobox film
| name           = O Teri
| image          = O Teri Poster.jpg
| caption        = Theatrical release poster
| director       = Umesh Bist
| producer       = Atul Agnihotri
| narrator       = Salman Khan
| writer         = Umesh Bist   Neeti Palta
| starring       = Pulkit Samrat  Bilal Amrohi Sarah-Jane Dias Mandira Bedi
| music          = GJ Singh, Rajiv Bhalla, Hard Kaur
| cinematography = Ganesh Rajavelu
| released       =  
| editing        = Devendra Murdeshwar Reel Life Productions
| country        = India
| language       = Hindi
}}

O Teri is a Hindi comedy film released on 28 March 2014 directed by Umesh Bhishit and produced by Atul Agnihotri,  featuring Pulkit Samrat, Bilal Amrohi, Sarah-Jane Dias,   and Mandira Bedi. The film stars Pulkit Samrat and newcomer Bilal Amrohi as two journalists, who come across a big scam.  Upon release, the film received generally negative reviews and went on to become a box office disaster. O Teri has been noted to have similarities with the 1983 Bollywood film "Jaane Bhi Do Yaroo".

==Plot==
abdul Prantabh Pratap aka PP Pulkit Samrat and Anand Ishwaram Devdutt Subramanium/AIDS Bilal Amrohi are two journalists and roommates. Their boss MonsoonSarah-Jane Dias always seems to insult them for the horrid quality of their work. She never seems to believe them and threatens to fire them from their jobs if they lie to her. One day PP and AIDS discover CBI Officer Avinash Tripathis dead body in their car and attempt to take it to their office. However, by the time PP and AIDS have brought Monsoon, the dead body has vanished causing Monsoon to think PP and AIDS lied and hence she fires them from their jobs. Frustrated, one day PP and AIDS are walking on a bridge which somehow collapses on the highway. On the news some people suggest the reason for the collapse could be a sonic boom, no blessings or the screw driver not inserted. PP and AIDS somehow discover that the dead body had caused the bridge to fall. The police take it away from them and later they end up with a CD that could expose a corrupt politician. The politicians chase PP and AIDS into a warehouse where eventually the police arrives and Monsoon realises that the men were telling the truth all this while. The politicians get arrested and PP and AIDS become nationwide heroes

== Cast ==
*Pulkit Samrat as Prantabh Pratab/P.P.
*Bilal Amrohi as Anand Ishwaram Devdutt Subramanium/AIDS
*Sarah-Jane Dias as Monsoon
*Mandira Bedi
*Vijay Raaz as Bhanwar Singh Kilol(Opposition Leader)
*Anupam Kher as Bilal Khawaja (Asian Olympics Games Chairman)
*Salman Khan – special appearance in the titular song "O Teri"
*Iulia Vântur – special appearance in the song "Umbakkum"
*Ajay Beri

== Location ==
The movie is shot primarily at the Mindmill Corporate Towers in Noida, NCR.

== Box office ==
The film which released—alongside Dishkiyaoon and Youngistaan—in approximately 1000 theatres across India saw a "poor" opening occupancy of 5-10%.   

==Soundtrack==
Music is composed by GJ Singh, Rajiv Bhalla and Hard Kaur. Lyrics penned by Kumaar, Abhinav Chaturvedi, Manish J. Tipu. 

{{tracklist
| headline     = Tracklist
| extra_column = Singer(s)
| total_length = 
| title1       = Butt Patlo
| extra1       = Jaspreet Jasz & Roshni Baptist
| music1       = GJ Singh
| lyrics1      = Kumaar
| length1      = 3:26
| title2       = Akhan Vich
| extra2       = GJ Singh, Jaspreet Jasz & Neeti Mohan
| music2       = GJ Singh
| lyrics2      = Kumaar
| length2      = 3:24
| title3       = Ummbakkum
| extra3       = Iulia Vântur, Mika Singh & Jaspreet Jasz
| music3       = GJ Singh
| lyrics3      = Abhinav Chaturvedi
| length3      = 2:59
| title4       = Phollo Karta
| extra4       = Manish J. Tipu
| music4       = Hard Kaur
| lyrics4      = Manish J. Tipu
| length4      = 4:10
| title5       = Ummbakkum (Remix)
| extra5       = Iulia Vântur, Mika Singh & Jaspreet Jasz
| music5       = Remixed by DJ Arya Acharya
| lyrics5      = Abhinav Chaturvedi
| length5      = 3:13
| title6       = O Teri
| extra6       = Rajiv Bhalla, Pooja Bhalla & Neuman Pinto
| music6       = Rajiv Bhalla
| lyrics6      = Kumaar
| length6      = 3:14
}}

== References ==
 

== External links ==
*  

 
 
 