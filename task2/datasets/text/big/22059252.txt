Male Restroom Etiquette
{{Infobox machinima
|show_name=Male Restroom Etiquette
|image=  urinals should be avoided
|game=The Sims 2 SimCity 4   
|genre=Mockumentary   
|writer=Phil Rice
|director=Phil Rice
|voice_actors=Phil Rice Craig Howe 
|production_company=Zarathustra Studios
|runtime=10 min.
|releases=September 13, 2006 
|website= 
}} educational and social guidance films. Narrated by Rice, Male Restroom Etiquette states restroom customs to be followed and depicts a scenario of social chaos if they are violated. The film was made using the machinima technique of recording video footage from computer games, namely The Sims 2 and SimCity 4. Male Restroom Etiquette won multiple awards and was listed by Guinness World Records Gamers Edition in 2009 as the most popular Sims video uploaded to YouTube.

==Synopsis==
The narrator (Phil Rice) states that increased cultural diversity has necessitated the exposition of previously unwritten rules regarding the use of male washroom|restrooms. According to these rules, males should use restrooms as quickly as possible, maximize physical separation from each other when using urinal (restroom)|urinals, flush urinals when they contain concentrated urine, avoid stalls with unflushed toilets, and avoid eye contact and communication with others. The film depicts a scenario in which excess communication leads to a mess in the restroom and thus deficient hygiene and homeostasis, the latter of which is in the lowest tier in Maslows hierarchy of needs. As the scenario continues, the restroom occupants turn to violence, leading to police and biological hazard team involvement that closes the restroom. Forced to go elsewhere, other men repeat the scenario, eventually leading to complete societal breakdown.

==Background and production== educational and social guidance films, Rice considered distributing the film in black-and-white, but decided against it. 

Rice originally started to film using id Softwares 1997 first-person shooter (FPS) video game Quake II but, after three months of pre-production, was dissatisfied with the games visual quality. After experimenting, he chose The Sims 2, a 2004 life simulation game published by Electronic Arts. His audio work for Strange Companys 2006 film BloodSpell delayed the start of Male Restroom Etiquette s production. However, after he was able to start, he finished after about two weeks of work. He recorded himself for the narration, and a friend for background voices. He edited audio in Steinberg Cubase SX3 and video footage from the game in Sony Vegas 6. 

==Reception and impact==
Although Rice hosts Male Restroom Etiquette on his own site and uploaded it elsewhere,  the film became popular on YouTube, where, after being featured on the front page in early October 2006,    it was viewed 2 million times by December 2006,    3.2 million times by February 2007,  and 5 million times  .    According to the Guinness World Records Gamers Edition, it is the most viewed Sims film on YouTube.    Citing this popularity, Elijah Horwatt credits the work with increasing public interest in machinima.    The film has won awards, including Best Writing at the Academy of Machinima Arts & Sciences 2006 Machinima Festival,    and Best of the Web from Animation World Network (AWN) in 2007.    Television networks were interested in airing Male Restroom Etiquette, but the work uses game-provided content, and Rice was unable to obtain the required permission from Electronic Arts, the copyright owner of that content. 

Writing for machinima.com, Grove praised Rices decision to base Male Restroom Etiquette on educational films because Male Restroom Etiquette shows truthfully and parodies male customs in restrooms. Grove also stated that the music, an original soundtrack, fits well.  In highlighting the film in their Best of the Web showcase, AWN stated that the it "shows that a skilled filmmaker can create compelling performances and stories in machinima".  Grove stated that, for many, the work served as their introduction to machinima. 

==Notes==
 

==References==
 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
 

==Further reading==
 
* 
 

==External links==
*  
*  

 
 
 
 
 
 
 
 