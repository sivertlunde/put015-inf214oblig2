Till There Was You (2003 film)
{{Infobox Film
| name           = Till There Was You
| image          = 
| image_size     = 
| caption        = 
| director       = Joyce E. Bernal
| producer       = 
| writer         = Dindo Perez Mel Mendoza-Del Rosario
| narrator       = 
| starring       = Judy Ann Santos Piolo Pascual
| music          = Raul Mitra
| cinematography = Charlie Peralta
| editing        = Marya Ignacio
| distributor    = Star Cinema Productions
| released       = April 30, 2003 (Philippines)
| runtime        = 109 minutes
| country        = Philippines Tagalog
| budget         = 
| gross          = P107.45 million
| preceded_by    = 
| followed_by    = 
}} 2003 Philippines|Filipino romantic drama film directed by Joyce E. Bernal and starring Judy Ann Santos and Piolo Pascual. This is their 3rd blockbuster hit movie.

==Plot==
Joanna (Santos) meets single father Albert (Pascual) and his baby Pippa on a bus. When she gets off the bus, Joanna accidentally leaves her photograph behind with her book. Over the years, Pippa comes to believe that this picture is of her mothers. Years later, Pippa meets Joanna again, and Albert hires her to act as Pippas mother. Eventually, Albert and Joanna become romantically involved. {{cite web
  | title = Till There Was You
  | work = 
  | publisher =divisoria.net 
  | date = 
  | url = http://www.divisoria.net/isrbothwasyo.html
  | accessdate =2008-07-01 }} 

==Cast==
*Judy Ann Santos as Joanna Boborol
*Piolo Pascual as Albert Robles
*Marissa Delgado as Zita Robles
*Eliza Pineda as Pippa Robles
*Ronaldo Valdez as Alfonso Robles
*Angel Jacob as Rachel Garcia
*Jennifer Sevilla as Celia Hernandez
*Janus Del Prado as Damon Boborol
*Bearwin Meily as Bogart Boborol
*Matet De Leon as Jean
*Gina Pareño as Lagring Boborol
*Pen Medina as Frank Boborol
Katherine Pierce

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 

 
 