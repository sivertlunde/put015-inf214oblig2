Fahrenheit 9/11
{{Infobox film
| name        = Fahrenheit 9/11
| image       = Fahrenheit 911 poster.jpg
| caption     = Promotional poster for Miramax Films
| director    = Michael Moore
| producer    =  
| writer      = Michael Moore
| starring    = Michael Moore George W. Bush
| distributor =  
| released    =  
| runtime     = 122 minutes
| country     = United States
| language    = English
| budget      = $6 million   
| gross       = $222.5 million 
}} director and political commentator Michael Moore. The film takes a critical look at the presidency of George W. Bush, the War on Terror, and its coverage in the news media.
 rationale for casualties there. intense controversy, including disputes over its accuracy.

The film debuted at the 2004 Cannes Film Festival in the documentary film category and received a 20-minute standing ovation, among the longest standing ovations in the festivals history. The film was also awarded the Palme dOr,    the festivals highest award.

The title of the film alludes to Ray Bradburys 1953 novel Fahrenheit 451, a dystopian view of the future United States, drawing an analogy between the autoignition temperature of paper and the date of the September 11 attacks; the films tagline is "The Temperature at Which Freedom Burns."

==Financing, pre-release, and distribution==
Originally planned to be financed by Mel Gibsons Icon Productions (which planned to give Michael Moore eight figures in upfront cash and potential backend),  Wild Bunch The Big One, in 1997.
 Disney was Peter Murphy send Weinstein a letter stating that the films $6 million budget was only a bridge financing and Miramax would sell off its interest in the movie to get those $6 million back; according to the same letter, Miramax was also expected to publicly state that it would not release the film. 

After Fahrenheit 9/11 was nearly finished, Miramax held several preview screenings; they were "testing through the roof." Stewart, p.519-520  Weinstein informed Eisner that Fahrenheit 9/11 was finished, and Eisner was surprised by the fact that Miramax had continued making the film.  Weinstein asked several Disney executives (including Eisner) to watch the film, but all declined; Disney stated again that Miramax would not release the film, and Disney also accused Weinstein of hiding Fahrenheit 9/11 by keeping it off production reports.  Disney sent production vice president Brad Epstein to watch Fahrenheit 9/11 on April 24, 2004.  According to Weinstein, Epstein said he liked the film; but according to the report Epstein sent to the Walt Disney Company board of directors, Epstein clearly criticized it.  Eisner told Weinstein that Disneys board decided not to allow Miramax to release the film.  Weinstein was furious and he asked George J. Mitchell (chairman of Disney at that time) to see the film, but Mitchell declined.  Weinstein asked lawyer David Boies to help find a solution;  the Weinsteins and Moore had also hired Chris Lehane to consult on the films release strategies. 

  and Dogma (film)|Dogma. 

Because of these difficulties, distribution was first secured in numerous countries outside the U.S. On May 28, 2004, after more than a week of talks, Disney announced that Miramax film studio founders Harvey and Bob Weinstein had personally acquired the rights to the documentary after Disney declined to distribute it. The Weinsteins agreed to repay Disney for all costs to that point, estimated at around $6 million. They also agreed to be responsible for all costs to finish the film and all marketing costs not paid by any third-party film distributors.  A settlement between the Weinsteins and Disney was also reached so that 60% of the films profit would be donated to Charitable organization|charity. 

The Weinsteins established Fellowship Adventure Group to handle the distribution of this film. Fellowship Adventure Group joined forces with Lions Gate Entertainment (which had released two other Miramax-financed films O (film)|O and Dogma (film)|Dogma)  and IFC Films to release it in the United States theatrically. (Fellowship Adventure Group also handled the films U.S. home video distribution through Columbia TriStar Home Entertainment). Moore stated that he was "grateful to them now that everyone who wants to see it will now have the chance to do so. 
 R rating by the Motion Picture Association of America, Moore appealed the decision, hoping to obtain a PG-13 rating instead. Moores lawyer, former Governor of New York, Mario Cuomo, was not allowed to attend the hearing. The appeal was denied on June 22, 2004, and Cuomo contended that it was because he had been banned from the hearing. Some theaters chose to defy the MPAA and allow unchaperoned teenagers to attend screenings.

==Synopsis== voting controversy in Florida constituted election fraud.

The film then segues into the September 11 attacks. Moore says Bush was informed of the first plane hitting the World Trade Center on his way to an elementary school. Bush is then shown sitting in a Florida classroom with children. When told that a second plane has hit the World Trade Center and that the nation is "under attack", Bush allows the students to finish their book reading, and Moore notes that he continued reading for nearly seven minutes.

Moore then discusses the complex relationships between the U.S. government and the Bush family; and between the bin Laden family, the Saudi Arabian government, and the Taliban, which span over three decades. Moore alleges that the United States government evacuated 24 members of the bin Laden family on a secret flight shortly after the attacks, without subjecting them to any form of interrogation.
 War in Afghanistan, including a natural gas pipeline through Afghanistan to the Indian Ocean.

Moore alleges that the Bush administration induced a climate of fear among the American population through the mass media. Moore then describes purported anti-terror efforts, including government infiltration of pacifist groups and other events, and the signing of the USA PATRIOT Act.

The documentary then turns to the subject of the Iraq War, comparing the lives of the Iraqis before and after the invasion. The citizens of Iraq are portrayed as living relatively happy lives prior to the countrys invasion by the U.S. military. The film also takes pains to demonstrate supposed war cheerleading in the U.S. media and general bias of journalists, with quotes from news organizations and embedded journalists. Moore suggests that atrocities will occur in Iraq and shows footage depicting Abu Ghraib torture and prisoner abuse|U.S. abuse of prisoners.

Later in the film, Lila Lipscomb appears with her family after hearing of the death of her son, Sgt. Michael Pedersen, who was killed on April 2, 2003, in Karbala. Anguished and tearful, she begins to question the purpose of the war.

Tying together several themes and points, Moore compliments those serving in the U.S. military. He claims that the lower class of America are always the first to join the Army, so that the people better off do not have to join. He states that those valuable troops should not be sent to risk their lives unless it is necessary to defend America. The credits roll while Neil Youngs "Rockin in the Free World" plays.

Moore dedicated the film to his friend who was killed in the World Trade Center attacks and to those servicemen and women from Flint, Michigan that have been killed in Iraq. The film is also dedicated to "countless thousands" of civilian victims of war as a result of the wars in Afghanistan and Iraq.

==Film release and box office==
 

The film was released theatrically by The Fellowship Adventure Group through a distribution arrangement with Lions Gate Entertainment. On its opening weekend of June 25–27, 2004, the film generated box-office revenues of $23.9 million in the U.S. and Canada, making it the weekends top-grossing film. Its opening weekend earned more than the entire U.S. theatrical run of any other feature-length documentary (including Moores previous film, Bowling for Columbine). The film was released in the UK on July 2, 2004 and in France on July 7, 2004. 
 liberal groups such as MoveOn.org (who helped promote the film) to encourage attendance in order to defy their political opponents contrary efforts. 

Fahrenheit 9/11 was screened in a number of Middle Eastern countries, including the United Arab Emirates, Lebanon, and Egypt, but was immediately banned in Kuwait. "We have a law that prohibits insulting friendly nations," said Abdul-Aziz Bou Dastour of the Kuwaiti Information Ministry.   The film was not shown in Saudi Arabia as public movie theaters are not permitted. The Saudi ruling elite subsequently launched an advertising campaign spanning nineteen US cities to counter criticism partly raised in the film. 

The film was shown in Iran, an anomaly in a nation in which American films had been banned since the Iran hostage crisis in 1979. Iranian film producer and human rights activist Banafsheh Zand-Bonazzi communicated with Iranians who saw the film, and claimed that it generated a pro-American response. 
 bootlegged versions Oscar eligibility, since the film was broadcast on television less than nine months after its theatrical release. However, soon after that story had been published, the Academy of Motion Picture Arts and Sciences issued a statement denying this, saying, "If it was pirated or stolen or unauthorized we would not blame the producer or distributor for that."  In addition, Wild Bunch, the films overseas distributor for Cuba, issued a statement denying a television deal had been struck with Cuban Television. The issue became moot, however, when Moore decided to forgo Oscar eligibility in favor of a pay-per-view televising of the film on November 1, 2004.

==DVD release==
Fahrenheit 9/11 was released to DVD and VHS on October 5, 2004, an unusually short turnaround time after theatrical release. In the first days of the release, the film broke records for the highest-selling documentary ever. About two million copies were sold on the first day, most of which (1.4 million) were sold as rentals. 

A companion book, The Official Fahrenheit 9/11 Reader, was released at the same time. It contains the complete screenplay, documentation of Moores sources, audience e-mails about the film, film reviews, and articles.

==Initial television presentations==
 
The two-hour film was planned to be shown as part of the three-hour "The Michael Moore Pre-Election Special" on  , TVN Entertainment Corporation|TVN, and the Cinema Now website and material prepared for "The Michael Moore Pre-Election Special" was incorporated into "Fahrenheit 9/11: A Movement in Time", which aired that same week on The Independent Film Channel.
 Danmarks Radio TV ONE, a channel of TVNZ. The next day, the Dutch network Nederland 3 aired the film. In Belgium, it was shown on Kanaal 2 on October 12, 2006. In Brazil, it aired on October 10, 2008 on TV Cultura, the São Paulo (state)|São Paulo public broadcasting network.

==Reception==

===Critical reception===
The film was received positively by critics. It received an 83% Fresh rating on Rotten Tomatoes based on 229 reviews.    It also received a score of 67 (generally favorable) on Metacritic, based on 43 reviews.  The consensus according to Rotten Tomatoes being "Extremely one-sided in its indictment of the Bush administration, but worth watching for the humor and the debates itll stir." 

Film critic Roger Ebert, who gave the documentary three and a half stars out of four, says that the film "is less an expose of George W. Bush than a dramatization of what Moore sees as a failed and dangerous presidency." In the film, Moore presents footage of Vice President Al Gore presiding over the event that would officially anoint Bush as president, the day that a joint session of the House of Representatives and the Senate would certify the election results. "Moore brings a fresh impact to familiar material by the way he marshals his images", says Ebert.

Entertainment Weekly put it on its end-of-the-decade, "best-of" list, saying, "Michael Moores anti-Bush polemic gave millions of frustrated liberals exactly what they needed to hear in 2004--and infuriated just about everyone else. Along the way, it became the highest-grossing documentary of all time." 

===Commercial reception===
Grossing over $222 million total worldwide, the film was the highest grossing documentary of all time, according to Box Office Mojo.  The film had a general release in the United States and Canada on June 23, 2004. It has since been released in 42 more countries. On Al-Jazeera in August 2012, Moore claimed the movie "grossed about half a billion dollars" worldwide. {{cite video
 | title = Hollywood and the war machine
 | url = http://www.aljazeera.com/programmes/empire/2010/12/2010121681345363793.html
 | series = Empire
 | medium = Television production
 | publisher = Al Jazeera
 | date = 2012-08-06
 | accessdate = 2012-09-03
 | time = 20:05
 | quote = And, contrary to what everybody said would happen, the film to date has grossed about half a billion dollars worldwide.
}} 

==Awards==

===Palme dOr===
  receiving the Palme dOr at the 2004 Cannes Film Festival.]]
 57th Cannes Film Festival. After its first showing in Cannes in May 2004, the film received a 15–20 minute standing ovation; Harvey Weinstein, whose Miramax Films funded the film, said, "It was the longest standing ovation Ive seen in over 25 years."  
 Jacques Cousteaus Oscar acceptance speech, Moores speech in Cannes included some political statements: 
 Republican president who once said, if you just give the people the truth, the Republicans, the Americans will be saved.   I dedicate this Palme dOr to my daughter, to the children of Americans and to Iraq and to all those in the world who suffer from our actions. 
 from the French.  Moore had remarked only days earlier that: "I fully expect the Fox News Channel and other right-wing media to portray this as an award from the French.   There was only one French citizen on the jury. Four out of nine were American.   This is not a French award, it was given by an international jury dominated by Americans."  The jury was made up of four North Americans (one of them born in Haiti), four Europeans, and one Asian. 

He also responded to suggestions that the award was political: "Quentin   whispered in my ear, We want you to know that it was not the politics of your film that won you this award. We are not here to give a political award. Some of us have no politics. We awarded the art of cinema, that is what won you this award and we wanted you to know that as a fellow filmmaker."  In comments to the prize-winning jury in 2005, Cannes director Gilles Jacob said that panels should make their decision based on filmmaking rather than politics. He expressed his opinion that though Moores talent was not in doubt, "it was a question of a satirical tract that was awarded a prize more for political than cinematographic reasons, no matter what the jury said."  Interviewed about the decision four years later, Tarantino responded: "As time has gone on, I have put that decision under a microscope and I still think we were right. That was a movie of the moment – Fahrenheit 9/11 may not play the same way now as it did then, but back then it deserved everything it got." 

===Peoples Choice Award===
The film won additional awards after its release, such as the Peoples Choice Award for Favorite Motion Picture, an unprecedented honor for a documentary. 

==Controversies==
 

The film generated significant criticism and controversy after its release shortly before the United States presidential election, 2004. British-American journalist and literary critic Christopher Hitchens contended that Fahrenheit 9/11 contains distortions and untruths.  
  This drew several rebuttals, including an eFilmCritic article and a Columbus Free Press editorial.  Former Democratic mayor of New York City Ed Koch, who had endorsed President Bush for re-election, called the film propaganda.    In response, Moore published a list of facts and sources for Fahrenheit 9/11 and a document that he says establishes agreements between the points made in his film and the findings of the 9/11 Commission. 

==Moores expectations for the 2004 presidential election==
The film was released in June 2004, less than five months before the 2004 presidential election. Michael Moore, while not endorsing presidential candidate   sliver that would never vote for Bush", and Jack Pitney, a government professor at Claremont McKenna College, suspected that the main effect of the film would be to "turn Bush-haters into bigger Bush-haters."  Regardless of whether the film would change the minds of many voters, Moore stated his intention to use it as an organizing tool, and hoped that it would energize those who wanted to see Bush defeated in 2004, increasing voter turnout.    Notwithstanding the films influence and commercial success, George W. Bush was re-elected in 2004.

==Lawsuit==
In February 2011, Moore sued producers Bob and Harvey Weinstein for US$2.7 million in unpaid profits from the film, stating that they used "Hollywood accounting tricks" to avoid paying him the money.    They responded Moore had received US$20 million for the film and that "his claims are hogwash". 
 Bob and Harvey Weinstein, and the lawsuit was dropped. 

==References==
 

==Further reading==
*  

==External links==
*  
**  
*  
*  
*  
*  

 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 