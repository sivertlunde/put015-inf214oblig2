Macbeth (2015 film)
{{Infobox film
| name           = Macbeth
| image          = Macbeth 2015 poster.jpg
| alt            = Teaser poster
| caption        = Teaser poster
| director       = Justin Kurzel
| producer       = Iain Canning   Laura Hastings-Smith   Emile Sherman
| writer         = Jacob Koskoff   Todd Louiso Michael Lesslie
| based on       =  
| starring       = Michael Fassbender   Marion Cotillard   Sean Harris   Elizabeth Debicki   Paddy Considine   David Thewlis
| music          = Jed Kurzel
| cinematography = Adam Arkapaw
| editing        = Chris Dickens
| studio         = See-Saw Films
| distributor    = StudioCanal  The Weinstein Company
| released       =     October 2015 (United Kingdom)
   
| runtime        = 113 minutes 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 of the same name. The film stars Michael Fassbender, Marion Cotillard, Sean Harris, Paddy Considine, Elizabeth Debicki, and David Thewlis. 

The film has been selected to compete for the Palme dOr at the 2015 Cannes Film Festival.   

== Cast == Macbeth
* Marion Cotillard as Lady Macbeth Macduff
* Elizabeth Debicki as Lady Macduff
* Paddy Considine as Banquo Malcolm
* David Thewlis as King Duncan

== Production ==
The production company behind Macbeth is See-Saw Films; the film will be distributed by StudioCanal worldwide. On October 23, 2013, The Weinstein Company acquired US and Canadian distribution rights to the film from StudioCanal. 

=== Filming ===
Filming was set to take place for seven weeks in England and Scotland.  On February 6, 2014, it was announced that principal photography had begun in Scotland.  On February 21, filming took place at Hankley Common in Elstead, Surrey.  On February 26, the cast and crew was spotted on set at Bamburgh Castle in Northumberland with almost 200 extras.  Other locations used include Quiraing in Skye, Scotland and Ely Cathedral in Ely, Cambridgeshire.   

== Marketing ==
A couple of photos from the film were revealed on April 18, 2014,  followed by two teaser posters on May 14. 

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 