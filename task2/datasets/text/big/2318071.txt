Aradhana (1987 film)
{{Infobox film
| name           = Aradhana
| image          =
| imdb_id        =
| writer         = Bharathiraja Rajasekhar
| director       = Bharathiraja
| producer       = Allu Aravind
| distributor    = Geetha Arts
| released       = March 27, 1987
| runtime        =
| cinematography = Telugu
| music          = Ilayaraja
| awards         =
| budget         =
}}
Aaradhana (  is an Indian romance film directed by Bharathiraja. It stars Chiranjeevi, Suhasini, Rajasekhar (actor)|Rajasekhar, and Radhika in important roles. Music was scored by Ilayaraja. Allu Aravind produced this film on his home production, Geetha Arts. It was a remake of Bharathirajas Tamil film "Kadalora Kavithaigal" starring Sathyaraj and Rekha.

==Summary==
The story portrays an illiterate Puliraju(Chiranjeevi), who is a small time rowdy in a small town. He meets Jennifer (Suhasini), who arrives in that town as a school teacher. Suhasini slaps and accuses him for ill-treating his mother. Puliraju, instead of taking revenge on her, gets attracted towards her and manages to join as her student. Over a period of time, chiru transforms in his looks, behaviour and leaves his past life back. Over few reels, they both get attracted towards each other, but neither of them express their feelings. His mother, surprised by changes in his behaviour brings his maradalu(Radhika) from his village and tries to marry him off. At the same time, suhasinis family friend Rajasekhar arrives and her father plans her marriage with him. Puliraju, struggling with change within himself is attacked by his old enemies and is hospitalized. Suhasini reaches the hospital, understands his love towards her and they both unite in the climax.

==Cast==
*Chiranjeevi
*Radhika
*Suhasini Rajasekhar

==Trivia==
the film underperformed at the boxoffice,
This film is a remake of a Tamil film "Kadalora Kavidaigal" from the same director

==External links==
* 

 

 
 
 
 
 
 
 
 

 
 