Tinker Bell and the Great Fairy Rescue
{{Infobox film
| name           = Tinker Bell and the Great Fairy Rescue
| image          = Tinkerbell DVD.jpg
| image_size     =
| caption        = DVD cover
| director       = Bradley Raymond
| producer       = Margot Pipkin
| screenplay     = Joe Ansolabehere Paul Germain Bob Hilgenberg Rob Muir
| story          = Bradley Raymond Jeffrey M. Howard 
| narrator       =
| starring       = Mae Whitman Lauren Mote Michael Sheen Pamela Adlon Lucy Liu Raven-Symoné Kristin Chenoweth Angela Bartys
| music          = Joel McNeely
| cinematography =
| editing        =
| studio         = DisneyToon Studios Walt Disney Walt Disney Studios Motion Pictures  (International) 
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = $30—$35 million   
| gross          = $10,872,752 
}} comedy adventure film based on the Disney Fairies franchise, it is the third installment of the Tinker bell franchise produced by DisneyToon Studios. It is the sequel to the 2008 film Tinker Bell and the 2009 film, Tinker Bell and the Lost Treasure and revolves around Tinker Bell, a fairy character created by J. M. Barrie in his play Peter Pan, or The Boy Who Wouldnt Grow Up, and featured in subsequent adaptations, especially in Disneys animated works. The film was produced using Digital 3D modeling. It was released on DVD and Blu-ray Disc|Blu-ray by Walt Disney Studios Home Entertainment on September 21, 2010.

==Plot==
Like the other fairies, Tinker Bell attends fairy camp on the mainland. When she attempts to go find some lost things, Vidia asks her if shes going to the human house, which isnt far from camp. The question makes Tink curious and eventually sneaks off. Vidia follows behind to watch over her. When she reaches the house, she is amazed by their "horseless carriage". She takes the time to flitter around under the car, while Vidia tries to get her to leave. Eventually they do, but on their way back to camp, Tink and Vidia stumble upon a fairy-sized house made by Lizzy, a human girl who wishes to meet a real fairy. Tink immediately heads in to investigate, despite Vidias constant warnings. Tink claims it to be perfectly safe, so Vidia slams the door shut in an attempt to scare her but unintentionally locks Tink inside. When Lizzy begins to approach the house, Vidia tries to free Tink to no avail. Lizzy discovers Tink inside and takes her to her home. She prepares to show Tink to her father, Dr. Griffiths, a very busy and serious scientist, but upon seeing all the butterflies he has pinned in display for research, she decides to keep Tink a secret.

Meanwhile, Vidia rallies Rosetta, Iridessa, Fawn, Silvermist, Clank and Bobble to rescue Tink. They try to sail on a stream which would take them straight to the Griffiths house. When their ship goes over a waterfall, Silvermist manipulates the water to create a mid-air stream. While this does save their lives, the boat is wrecked. They continue on foot but upon crossing a mudbank, Vidia gets stuck waist deep. While Clank and Bobble try to find something to pull her out, the other fairies are nearly run over by a car but are saved when Iridessa blinds the driver, who then vacates the car, giving the girls a chance to get Vidia out by grabbing onto the drivers shoelace.

Back at the human house, Lizzy reveals her fascination of fairies. Tink is flattered by her obsession and since its raining outside, Tink decides to teach her nearly everything about fairies. They record their information in a new research book given to Lizzy by her father. During this time, they have grown a great friendship. After a while, the rain dies down, and Tink is able to return to camp. She gives Lizzy a hug and makes her way out but before she leaves, she watches Lizzy attempt to show her father the research. Unfortunately, Dr. Griffiths is too busy fixing the houses leaks to pay her any mind, so Tink returns and fixes the leaks, saving Lizzys father from the burden. Afterwards, she makes the choice to release a captive butterfly Dr. Griffiths was planning on showing to a group of scientists. Thinking that his daughter was the one who set free the butterfly (although he doesnt believe Tinker Bell was the one that set the butterfly free) his daughter was punished and sent to her room.

Meanwhile, Vidia confesses to the rescue team that it was her fault that Tink has been captured. They comfort Vidia about the situation, informing her that it could have been worse without her presence. Once the rescue team finally reaches the human house, they are attacked by Lizzys pet cat, Mr. Twitches. Despite being an animal fairy, Fawn is unable to immediately tame a cat under pressure. A chase ensues before she is able to find catnip, eventually taming the cat. 

Back at the house, Tink shows Lizzy how to fly in her room with pixie dust. Then her father walks in, forcing Tink to hide in the fairy house. He finds footprints on the ceiling and sternly demands the truth. Lizzy tells him about Tink and shows him the research she and the fairy did in the book he gave her. Her father, however, still refuses to believe in fairies, and he and his daughter get into a disagreement. Angered by Dr. Griffiths stubbornness, Tink reveals herself and chides him. The sight of the fairy astonishes the scientist and prompts him to capture Tink so that he could take her to London for research, but Vidia arrives just in time and pushes her out the way. Vidia is instead captured by Dr. Griffiths, but Lizzy and the fairies are able to convince him to think otherwise.

In the end, Dr. Griffiths apologizes to his daughter for not believing her. Vidia is then freed, and she and Tink form a friendship. Lizzy and her father are now closer than ever.

==Cast==
 
* Mae Whitman as Tinker Bell, a tinker fairy.
* Lauren Mote as Lizzy Griffiths, a nine-year-old human girl who takes a liking to fairies.
* Michael Sheen as Dr. Griffiths, a constantly pre-occupied scientist and Lizzys father.
* Pamela Adlon as Vidia, a fast-flying fairy.
* Lucy Liu as Silvermist, a water fairy.
* Raven-Symoné as Iridessa, a light fairy.
* Kristin Chenoweth as Rosetta, a garden fairy.
* Angela Bartys as Fawn, an animal fairy.
* Rob Paulsen as Bobble, a wispy tinker fairy with large glasses and the best friend of Clank.
* Jeff Bennett as Clank, a large tinker sparrow man with a booming voice, and the Driver.
* Jesse McCartney as Terence, a pixie dust keeper and Tinks best friend.
* Cara Dillon as the Narrator.
* Faith Prince as Mrs. Perkins, Lizzy and Dr. Griffiths neighbor.
* Bob Bergen as Cheese the mouse and additional voices.

==Music==
 Tinker Bell films. In addition, the following songs were written for the film:

* "Summers Just Begun," written by Brendan Milburn and Valerie Vigoda of GrooveLily
* "Come Flying With Me," music by Joel McNeely, lyrics by Brendan Milburn and Valerie Vigoda
* "How to Believe," written by Adam Iscove

===Soundtrack album===

On February 16, 2015 Intrada Records (co-branded with Disney) released the soundtrack album.

# Introduction
# Summers Just Begun - Cara Dillon
# Fairy Camp!
# The Horseless Carriage
# Curious Tink
# Lizzy Builds Her Fairy House
# Tink and Vidia Discover the Fairy House
# Tink Is Captured
# Trying to Escape
# Were Going to Build a Boat
# Tink and Lizzy Meet
# Tink Wants to Leave/Launching the Boat
# How to Believe (Fairy Field Guide) - Holly Brook
# Riding the Rapids
# Ill Never Forget You
# Tink Returns
# Fixing Leaks
# Father Never Has Time for Me
# Lizzie Flies!
# Father Discovers Tink/Vidia Is Captured
# Flying to London
# Race to Save Vidia
# Father Believes
# A Fairy Tea Party
# Summers Just Begun (Reprise) - Cara Dillon
# How to Believe (EC Version) - Bridgit Mendler
# Come Flying with Me - Cara Dillon

==Release==
 , where it was shown on an inflatable movie screen.]]
 May Fair Hotel in London on August 8, attended by Lauren Mote." 

In the United States, the film had an outdoor premiere on August 28, 2010 as part of the Outdoor Cinema Food Fest at La Cienega Park in Beverly Hills, California.    Between September 3 and September 19, 2010, the film was shown at the El Capitan Theatre,  in order to make it eligible for the Academy Award for Best Animated Feature. Disney qualified the film in an unsuccessful effort to expand the categorys final nominations from three to five, as, under the Academy rules in effect that year, five films could only have been nominated in a calendar year in which 16 or more animated films were submitted.   

The film was released in the United States on DVD and Blu-ray on September 21, 2010.  Like the previous two films, Great Fairy Rescue debuted on the Disney Channel in November 2010.

===Video game===
{{Infobox video game
| title = Disney Fairies: Tinker Bell and the Great Fairy Rescue
| caption = 
| image =  
| developer = EA Bright Light Studio
| publisher = Disney Interactive EA Distribution
| designer = 
| series = 
| engine = 
| released = September 22, 2010
| genre = Adventure
| modes = Single-player, multiplayer
| platforms = Nintendo DS
| media = DS Game Card
}}
Disney Fairies: Tinker Bell and the Great Fairy Rescue is an adventure game for the Nintendo DS. Like in the previous games, the player plays as a fairy created by the player on the Mainland around Lizzys house, using the touch screen to maneuver the character and play various minigames. The player must, for example, touch an arrow on the screen to move to another map or characters to speak to them.

====Features====
*Mini-games
*Multiplayer modes
*DGamer functionality

===International distribution===
Television: Channel 5 (August 12, 2012)

==Reception== The Karate Kid, and grossed €30,174 in its first week.
 

==References==
 

==External links==
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 