The Brainiacs.com
 
{{Infobox Film
| name           = The Brainiacs.com
| image          = DVD cover of the movie The Brainiacs com.jpg
| caption        = dvd cover
| director       = Blair Treu
| producer       = Roger Mende Joe Broido Mark Headley
| writer         = 
| starring       = Michael Angarano Kevin Kilner Kevin Jamal Woods Florence Stanley Dom DeLuise Vanessa Zima
| music          = 
| distributor    = 
| released       = 2000
| runtime        = 100 min. English
}}
 American family comedy.

==Cast==
* Michael Angarano as Matt Tyler
* Kevin Kilner as David Tyler
* Kevin Jamal Woods as Danny
* Vanessa Zima as Kelly Tyler
* Florence Stanley as Miriam Tyler
* Dom DeLuise as Ivan Lucre
* Alexandra Paul as Kara Banks

==Plot==
To achieve his goal of more time with his widowed, workaholic father David, Matt Tyler and his best friend Danny decide to try to buy his business, Tyler Toys. Matt attempts to open a bank account at the Sunnyside, California  bank, which is owned by fellow businessman Ivan Lucre. Though he quickly gains the admiration of loan officer Kara Banks, Matts meager funds do not meet the minimum deposit requirement. When his older sister Kelly (confidentially) reveals she is working on a groundbreaking artificial intelligence computer chip, Matt and Danny create The Brainiacs.com and a fictional company called Global Consolidated Resources, which offers shares of stock as collateral for $1 of investment in the microchip.

The returns total $4 million, and the amount of money Matt and Danny have accumulated requires Matt to reveal his plan to Kara, who is deeply sympathetic to the boys plan. After they leave, David walks in to apply for a loan to jump-start Tyler Toys expansion program, which will sell the new Hairball toy internationally. After David and Kara disagree over procedural issues such that the loan does not go through, Ivan calls Kara into his office to brief her of his plan. Lucre instructs Kara to loan David as much money as possible, such that Tyler Toys will either succeed and give him profits, or fail and allow the bank to cheaply buy Tyler Toys and its assets. Additionally, his assistant Ms. Arbitrage, who worked with Tyler for Hairball orders overseas, cancels the next set of orders. The loan quickly maxes out to $500,000, which the financially struggling company cannot produce. Lucres children Chet and Russell heard about Matts "project" in economics class and ruthlessly bully Matt, letting him know that they are also out for blood. Matt buys a majority share in Tyler Toys, and as the new boss "Mr. Chips" (animated by a computer) institutes lax work regulations and gives his father a vacation. Lucre hires his nephew Miles to spy on David and his family in an attempt to weed out the mysterious competitor, and Chet and Russell attempt to attack Matt and Danny at school but are outsmarted.

Although Davids vacation time allows him to spend quality time with his family, the downside is that Kelly is so happy spending time with her father that she completely loses focus on the computer chip, which has hit a major bug that may take weeks to solve - weeks that Tyler Toys does not have. When David returns to work, Mr. Chips assigns him to test Tyler Toys entire product line, which wakes him up to the reality that the company has lost its magic. Further complications ensue when Mr. Toler of the Federal Trade Commission arrives at the Tyler house to investigate Global Consolidated Resources and its "super-chip" for potential fraud. Matt stalls Toler and asks Kara for help; she agrees to stall Toler only if Matt comes clean with his father.

On a camping trip the next day, Matt reveals to his father and sister that he bought the company and why; Kelly rounds on her brother, who is forced to admit that he betrayed their confidence. Even worse, Miles has recorded the entire conversation and reports back to his uncle. When David returns to Tyler Toys headquarters, he receives financial and emotional blows: Ms. Arbitrage was working for Lucre, and the company is scheduled to be auctioned off at an official hearing at noon the next Monday unless the $500,000 is repaid by then. Kara walks in on Ivan, Miles and Ms. Arbitrage toasting their victory and quits her job in disgust. In desperation, all of Tyler Toys works towards a functioning microchip before Monday, but its initial prototype fails to work. Kara assists David and Matt in compiling their financial records, but the team can only turn up $100,000 owed to them.

On Monday morning, Matt accidentally finds an abandoned steel box containing stock certificates. He immediately meets with his grandmother Miriam, who has been in town since Tyler Toys was bought out and drives the auction following the hearing up to $2 million, to everyone elses surprise. When Ivan demands that Miriam corroborate her bidding, she hands the stock to the judge, who promptly appraises it at $10 million. Miriam nonchalantly repays Lucres loan and buys Tyler Toys, closing the case; the celebration is punctuated by David and Kara sharing a kiss. Although Miriam sets ground rules for her son before restoring the companys proper ownership, the ordeal has taught David his lesson. In revenge, Chet and Russell ambush Matt and blackmail him into signing over control of Global Consolidated Resources; Matt capitulates as he sees Mr. Toler drive up and then directs the FTC executive to the companys new owners, who are assessed a fraud penalty of $500,000 when they confess to not having the chip.

Despite the loss of the stock, Tyler Toys survives long enough for the microchip to be completed, which sees its first use metamorphosizing the Hairball into the intelligent, interactive Smartball; it becomes a global sensation that sells over a billion units and the signature item of Tyler Toys. Testing is underway for the Smartboard, a skateboard with a smart-chip implant, as the movie ends with a shot of the company billboard.

==External links==
*  

 

 
 
 