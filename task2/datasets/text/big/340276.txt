The Waterboy
 
{{Infobox film
| name = The Waterboy
| image = Waterboy-poster-0.jpg
| image_size =
| caption = 
| alt = 
| director = Frank Coraci
| producer = {{Plain list | 
* Jack Giarraputo 
* Robert Simonds 
* Adam Sandler
}}
| writer = Tim Herlihy  Adam Sandler
| starring = {{Plain list | 
* Adam Sandler 
* Kathy Bates 
* Fairuza Balk 
* Henry Winkler 
* Jerry Reed 
* Larry Gilliard, Jr. 
* Blake Clark 
* Peter Dante 
* Jonathan Loughran
}}
| music = Alan Pasqua
| editing = Tom Lewis Steven Bernstein
| studio = Touchstone Pictures Buena Vista Pictures
| released =  
| runtime = 90 minutes
| country = United States English
| budget = $23 million
| gross = $185.9 million
}}
The Waterboy is a 1998 American sports/comedy film directed by Frank Coraci (who played Robert Roberto Boucher, Sr.), starring Adam Sandler, Kathy Bates, Fairuza Balk, Henry Winkler, Jerry Reed (his last film role before his death in 2008), Larry Gilliard, Jr., Blake Clark, Peter Dante and Jonathan Loughran, and produced by Robert Simonds and Jack Giarraputo.
 Jimmy Johnson, Paul Wight and Rob Schneider have cameo appearances. The movie was extremely profitable, earning $161.5 million in North America alone.    This was Sandlers second film to eclipse $120 million worldwide in 1998 along with The Wedding Singer. 

Adam Sandlers character, Bobby Boucher (pronounced    ), bears a strong resemblance to his "The Excited Southerner" comedic skits from his album What the Hell Happened to Me? The portrayal is one of a stereotypical Cajun from the bayous of South Louisiana, not the typical stereotype of a Southerner. He also shares similarities in speech and mannerism to Canteen Boy, a recurring character, also portrayed by Adam Sandler, on Saturday Night Live.  Like Bobby, Canteen Boy preferred "purified water, right out of the old canteen", which he always carried with him.

== Plot == stutter and hidden anger issues due to constant teasing and excessive sheltering by his mother, Helen (Kathy Bates). He became the water boy for the (fictional) University of Louisiana Cougars  after being told his father died of dehydration in the Sahara while serving in the Peace Corps. However, the players always torment him and the teams head coach, Red Beaulieu (Jerry Reed), eventually fires him for "disrupting" his practices. Bobby then approaches Coach Klein (Henry Winkler) of the South Central Louisiana State University Mud Dogs and asks to work as the teams water boy. Coach Klein has been coach of SCLSU for years without success. We find out later in the movie that he and Beaulieu were assistant coaches at the University of Louisiana but Beaulieu bullied Klein into letting him take sole credit for a playbook (that Klein actually came up with on his own) to earn the head coach job and then immediately fired Klein. The experience drove Klein to a mental breakdown and rendered him unable to come up with new plays. Furthermore, unlike the Cougars, the Mud Dogs are a struggling team both on and off the field. They have lost 40 consecutive games, their cheerleaders have become alcohol dependent and players are forced to share equipment. Bobby insists he be the waterboy after seeing a keg of heavily polluted water that coach Klein had been offering his players. Klein, despite being impressed with a sample of Bobbys water, tells him he cant hire anybody due to the teams financial issues, but Bobby agrees to work for free.

Bobbys mother Helen tells Bobby of the evils of football (which she calls "foosball") and forbids him to play. After being picked on again by his new team, Coach Klein encourages Bobby to strike back, which leads to him tackling and knocking out the teams quarterback. Coach Klein begs Bobby to join the team but he refuses as his mother would never approve. Coach Klein then meets with Bobby and his mother and attempts to convince her to let Bobby play by emphasizing that Bobby can get a college education. But she refuses despite Bobbys interest. Coach Klein convinces Bobby to join in secret (from his mother) saying that "what mama dont know, wont hurt her". Bobby quickly becomes one of the most feared linebackers in college football, hitting opposing players with injury-causing force. In his first game, Bobby causes a turnover that costs the team the win, angering them to the point that they completely overlook Bobby scoring an NCAA record of 16 sacks in the game. Despite this, the Mud Dogs win their next game when Bobby scores a safety on the final play, ending their long losing streak. The Mud Dogs go on a winning streak and earn a trip to the annual Bourbon Bowl to face the Cougars and Coach Beaulieu. Bobbys newfound fame also allows him to rekindle a relationship with his childhood friend and crush, Vicki Vallencourt (Fairuza Balk), who has been in prison multiple times. However, Helen forbids Bobby from seeing her again (telling him that girls are "the devil").

At a pep rally, Coach Beaulieu reveals that Bobby never finished high school and his high school transcript was fake (as the school doesnt even exist), making him ineligible for college and football. The team and fans label him a "cheater" and snub him. the next day, Klein pulls some strings and the NCAA agrees to let Bobby compete in the Bourbon bowl if he can pass a GED exam. Bobby is reluctant as he feels he has become a pariah of the town and is angered over the fact that someone set him up. At that point Klein apologizes and admits he submitted the fake transcript because he was desperate to get even with Beaulieu. Klein tells Bobby about his past with Beaulieu, and the story convinces Bobby to take the exam to help Klein get revenge on Beaulieu and prove to everyone hes not a "dummy".  While studying, Bobby inadvertently reveals to his mother that hes been playing football, going to college and seeing Vicki. This leads to them having a fight with Bobby lashing out over his mothers constant sheltering of him, saying hell continue to defy her because he enjoys school and football. Bobby easily passes the exam, scoring a 97%, but his mother then fakes falling ill to keep Bobby from playing. Bobby refuses to ever play football again, feeling he drove his mother to illness, and stays in the hospital with his mother. Meanwhile, Vicki spreads word around the community of Bobby passing the exam. This leads to a gathering of fans at the hospital who apologize for not supporting him and try to convince him to play. Bobby however, refuses. The next day, Helen ends her fake illness and tells Bobby the truth about his father and why she was faking her illness. Years ago, his father found work in New Orleans, changed his name to Roberto and left Helen for a voodoo priestess, while she was pregnant. This in turn lead Helen to excessively shelter Bobby all his life, afraid he would abandon her like Roberto did. Helen realizes the best thing for her to do is let him go since he has made a lot of friends and encourages him to play in the Bourbon Bowl because it means so much to the community.
 MVP of the game.

Sometime later, Bobby and Vicki get married and are heading to the riding lawn mower. On their way out, Bobbys father makes an unexpected appearance, telling him that he heard from ESPN that he may go to the NFL. Bobby tells him that he is not going to the NFL because he wants to stay in school and graduate. Roberto tries to get him to skip school and go to the NFL, hoping to personally profit as the father of an NFL player (citing the success Tiger Woods and his father have had). He is quickly tackled to the ground by an enraged Helen for this (and out of revenge against Roberto for leaving her years ago), much to the cheers of the crowd. Bobby and Vicki leave to consummate their marriage.

==Cast==
 

* Adam Sandler as Robert Bobby Boucher, Jr.
* Kathy Bates as Helen Mama Boucher
* Fairuza Balk as Vicki Vallencourt
* Jerry Reed as Coach Red Beaulieu
* Peter Dante as Gee Grenouille
* Henry Winkler as Coach Klein
* Larry Gilliard, Jr. as Derek Wallace
* Blake Clark as Farmer Fran
* Jonathan Loughran as Lyle Robideaux
* Clint Howard as Paco
* Allen Covert as Walter
* Rob Schneider as The Townie; Schneider reprises the role in Adam Sandlers 2000 film Little Nicky, despite being made by New Line Cinema; in turn, Sandler plays the same townie in Schneiders film The Animal.
* Kevin Farley as Jim Simonds
* Frank Coraci as Robert Roberto Boucher, Sr. Paul "The Giant" Wight as Captain Insano
* Soon Hee Newbold as Mud Dog Cheerleader ABC Sports commentator) ABC Sports commentator) ABC Sports commentator)
* Chris Fowler as Himself (ESPN commentator)
* Lee Corso as Himself (ESPN commentator)
* Trevor Miller as Himself
* Moosie The Cocker Spaniel as Herself
* Dan Patrick as Himself (ESPN commentator)
* Lawrence Taylor as Himself (LTs Louisiana Lightning Training Football Camp)
* Bill Cowher as Himself (Pittsburgh Steelers coach) Jimmy Johnson as Himself (Miami Dolphins coach)
* Jennifer Bini Taylor as Rita

==Filming and production== Orlando area Daytona Beach, DeLand, Florida, Lakeland, Florida, and surrounding areas.

The Mud Dogs home games were filmed at Spec Martin Stadium in DeLand, Florida, home of the local high school team (the DHS Bulldogs). The classrooms and gym where Bobby takes the GED are part of Stetson University, also located in DeLand. Stetsons Carlton Student Union building is featured in the scene where Bobby is told his mother has been hospitalized.

The scenes involving mamas cabin were shot on Lake Louisa, in Clermont, Florida.

Coach Kleins (Henry Winklers) office was a stage built inside of the Florida Army National Guard Armory in DeLand, Florida. It is home of Btry B 1st Bn 265th ADA. If one was to look closely, in the background of the practice field scenes, they can see the Armory and some military vehicles.

The initial exterior shot of the University of Louisiana stadium was EverBank Field in Jacksonville, Florida|Jacksonville; the interior of the stadium is actually the Citrus Bowl in Orlando, Florida. The Citrus Bowl was also the filming location for the climatic Bourbon Bowl game, while the flyover shot at the beginning of the game is of Williams-Brice Stadium at the University of South Carolina in Columbia, SC.

The "medulla oblongata" scene was filmed at Florida Southern College in Lakeland, FL. The extras in the scene were students at Florida Southern College. The scene was shot in Edge Hall.

==Critical reception== review aggregation website Rotten Tomatoes, 35% of the reviews were positive, with an average rating of 4.6/10. The sites consensus was "The Waterboy is an insult to its genre with low humor and cheap gags."  At Metacritic, the film holds a rating of 41%, indicating "mixed or average reviews". 

Roger Ebert of The Chicago Sun Times gave the film a negative review, saying "Sandler is making a tactical error when he creates a character whose manner and voice has the effect of fingernails on a blackboard, and then expects us to hang in there for a whole movie."  The film also appeared on his "Most Hated" list.  Lisa Alspector of the Chicago Reader gave the film a negative review, writings: "Geek-triumphs-after-all comedies can be charming, but in this one the triumphing begins so early its hard to feel for the geek."  Michael OSullivan of the Washington Post described the movie as "Another film about . . . a cretinous, grating loser." 

Manhola Dargins of L.A Weekly gave the film a mixed review, writing: "Of course its dumb, but every 10 minutes or so, its also pretty funny."  Glen Lovell of Variety said of the film, "This yahoos-on-the-bayou farce is neither inventive nor outrageous enough.".  David Nusair of Reel Film Reviews also gave the film a mixed review, calling it "an agreeable yet forgettable comedy". 

Janet Maslin of The New York Times said the film was "so cheerfully outlandish that its hard to resist, and so good-hearted that its genuinely endearing.".  Mark Savlov of the Austin Chronicle also gave the film a positive review and said the film was "A mildly amusing bayou farce with plenty of foosball action to liven the sometimes plodding proceedings." 

  
The film grossed $185,991,646 worldwide from a $20 million budget.   

===Awards and nominations===
  Worst Actor. The film was also a nominee for the American Film Institutes AFIs 100 Years...100 Laughs. 

==References==
 

==External links==
 

*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 