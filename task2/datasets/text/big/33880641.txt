The Band of Honest Men
{{Infobox film
| name           = The Band of Honest Men (aka La banda degli onesti)
| image          = The Band of Honest Men.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Camillo Mastrocinque
| producer       = Isidoro Broggi Renato Libassi
| writer         = Agenore Incrocci Furio Scarpelli
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Totò Peppino De Filippo Giacomo Furia
| music          = Alessandro Cicognini
| cinematography = Mario Fioretti
| editing        = Gisa Radicchi Levi
| studio         = D.D.L.
| distributor    = Momi-Caiano
| released       =  	
| runtime        = 106 min
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}

The Band of Honest Men (original title: La banda degli onesti) is a 1956 black and white Italian comedy film.   The film is known as Die Bande der Ehrlichen in West Germany, and Totó e as Notas Falsas in Portugal. It was filmed in Rome.

==Synopsis==
Antonio Buonocore, keeper of a stable Rome with a wife German, is to attend the event to Mr. Andrew, an elderly tenant who, before dying, tells him to be in possession of a suitcase inside with a few clichés of the original Bank of Italy, of which he had long been dependent, and the watermarked paper to print the notes 10,000 pounds. Mr. Andrea had stolen this material with the intention of "revenge" of his dismissal and make fake money, but never had the courage to succeed in its intention, therefore, Buonocore asked to throw the bag into the river so destroying the contents. Buonocore, however, is going through a bad time: basically honest person, has refused to become an accomplice of the accountant Casoria, the new administrator of the condominium, which had proposed to make a series of fraudulent transactions against the same building, and for this reason is under threat of dismissal. He thus decides not to destroy the bag, but ignoring the banknote printing techniques, to produce 10,000 pieces is forced to ask the cooperation of the typographer Joseph The Turkish and, later, the painter Cardone, both as variously indebted him. Building on the economic needs of his cronies, organizes meetings of stealth and hilarious night to give life to a gang of counterfeiters. The three manage to print banknotes and "split" in a bar one night, but things get complicated when Buonocore discovers that his eldest son Michael, a brilliant financier in his career recently moved to Rome, is following its own investigation of the delicate creation of a batch of counterfeit notes.

Doop have heard recounted some details of Michael, and talk vaguely allusori by the Marshal of Finance, the head of his son, came to visit him at home, seeing the police go and search the typography of The Turkish (found it closed), and noticing strange changes in style in his "partners" (The Turkish expensive new shoes, with a new coat Cardone), Anthony is very impressive and is afraid of being discovered, with the aggravating circumstance that all of this, since he is the father of a financier, would cost the place to his son. So he begs his cronies do not spend a penny, and immediately dispose of the equipment, burying out of town.

The son, in passing, casually seeing this strange burial, asks his father what he is doing, but Cardone, also present, does not think better than to say they are burying Mustafa, Buonocores poodle, remained under a car . Anthony is therefore forced to get rid of the dog, and not having the courage to kill him, leave him on the road, tied to a milestone (a "stone Emilia" as pronunicato by Toto in his frequent deformations language). But Mustafa is released early and returned home by himself, during one visit to the Marshal, he notices a strange door in embarrassment.

Antonio, now feeling hunted, matured the idea, which he exhibited in The Turkish, to get arrested by Michele: a child who stops his father-he says-not only did not hunt, but promote it, and becomes an example for all his colleagues. Therefore decided to implement his project by going in person to the police station to be arrested by his son, who believes that wants to play. But after hearing that the investigation by the Marshal followed by Michael ended with the arrest of a gang of counterfeiters professionals ("The Turkish?" "No, the Swiss!") And that the business had been done for him so identified, but was not one of those produced by the three, but the sample used, false, and also supplied to him by a money lender, of course pinch, is about to faint. Turns out that none of its members had had the courage to spend only one of the notes produced (The Turkish had borrowed money from an appears, Cardone had used the money that his "mommy" kept under the mattress).

The three, found the peace, decided to destroy all counterfeit notes and the suitcase with the clichés, setting up a bonfire as a final gag, Buonocore realizes (too late) that he threw into the flames, in the heat, even the envelope containing his salary.

==Cast==
*Totò: Antonio Bonocore
*Peppino De Filippo: Giuseppe Lo Turco
*Giacomo Furia: Cardone Gabriele Tinti: Michele, son of Bonocore
*Giulia Rubini: Marcella, daughter of Lo Turco
*Nando Bruno: Maresciallo Denti
*Luigi Pavese: Ragionier Casoria
*Memmo Carotenuto: Fernando
*Gildo Bocci: tabaccaio
*Lauro Gazzolo: Andrea

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 