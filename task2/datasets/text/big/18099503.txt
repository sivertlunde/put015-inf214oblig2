Where or When (film)
Where 2008 film director Bahman Pour-Azar. He co-wrote the film script with Jun Kim over seven years, but shot the entire movie in less than a week at various locations in both New York and New Jersey. The film was produced for less than US$50,000.

The 85-minute feature was inspired by the 1959 French classic Hiroshima Mon Amour directed by Alain Resnais and contains the classic show tune from the 1937 Rodgers and Hart musical Babes In Arms, Where Or When.

Where Or When stars Mitchell Conwell, Shelly DeChristofaro, Jun Kim, and Carl Monego.

== Synopsis == characters are unaware of.  During this final day they must deal with anger, guilt (emotion)|guilt, hatred, and love.  All of this set to the backdrop of todays global issues.

== Themes ==
Pour-Azar is able to film this event that portrays a scenario with horrible and unimaginable consequences by using the terrifying images from Hiroshima Mon Amour and contrasting them with the upbeat show tune Where or When.

When screening the film Stuart Alson, who founded The New York Independent Film and Video Festival said that the piece was "a parallel line of work with the French masterpiece Hiroshima mno Amour!

== Honors ==
The film was previewed at various film festivals in 2003 before its official 2008 release including the New York Film Festival, Las Vegas Film Festival; Los Angeles Film Festival and the New York Film and Video Festival where it was nominated for the "Grand Selection".

== References ==
Independent Film Quarterly  
The New York Independent Film and Video Festival  

==External links ==
* 
* 
* 

 
 
 
 
 
 


 