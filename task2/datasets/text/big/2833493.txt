Bad Moon
 
{{Infobox film
| name           = Bad Moon
| image          = BadMoonDVD.jpg
| director       = Eric Red
| producer       = James G. Robinson
| screenplay     = Eric Red
| based on       = Thor by Wayne Smith
| music          = Daniel Licht
| starring       = {{Plainlist | 
* Michael Paré 
* Mariel Hemingway
* Mason Gamble
}}
| cinematography = Jan Kiesser
| editing        = Carroll Timothy OMeara
| studio         = Morgan Creek Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $7,000,000 (estimated) 
| gross          = $1,055,525 (USA) 
}} American horror film written and directed by Eric Red and produced by James G. Robinson. The plot involves a family man who struggles to overcome the curse of lycanthropy. It stars Michael Paré, Mariel Hemingway and Mason Gamble.

The film is based on the novel Thor by Wayne Smith, which mainly tells the story from the dogs viewpoint. Thor was published in the United States (Thomas Dunne/St. Martins Press hardback, Ballantine paperback), and in the U.K., Germany, the Netherlands, Sweden and Norway, in German, Dutch, Swedish and Norwegian. Bad Moon received poor reviews and was a box office bomb.

==Plot==
While making love in their tent during a work expedition in Nepal, photo-journalists Ted (Michael Paré) and his girlfriend Marjorie are attacked by a werewolf. The werewolf snatches Marjorie and Ted attempts to rescue her but gets bitten in his shoulder and thrown to the ground. Hurt but determined he crawls to his shotgun and manages to shoot the werewolfs head off but not before the beast kills Marjorie.

With the intent of living in isolation Ted moves back home into a trailer in the woods hours away from his lawyer sister Janet Harrison (Mariel Hemingway). One day in an effort to reach out to her and his young nephew Brett (Mason Gamble), Ted calls them up and invites them for a meal at his home by the lake. Upon seeing him again the family dog, Thor, begins to sense something wrong with "Uncle Ted" and goes into the woods tracking a smell, which leads him to human remains hanging on a tree branch. Meanwhile, having heard of Marjories death and in an attempt at comforting her brother, Janet invites him to stay with them. Shaken and fearful of hurting them, Ted declines and Janet, Brett and Thor leave before the sun starts to set, at Teds insistence.

The next day there is an investigation going on in the woods near Teds trailer where the mangled bodies of several missing hikers and a Forest Ranger were found. Ted, under fear of being found guilty, calls Janet and tells her hes changed his mind. He parks his trailer in her yard in the hopes that in being near his family hell be able to control himself. Thor, however, is aware of Teds nature and becomes suspicious and eventually hostile towards him. Noticing that he goes out to "jog" at night with handcuffs, Thor becomes frenzied until Janet lets him out of the house. Tracking his scent, Thor follows Ted into the woods and finds him turned into a werewolf and tied to a tree while growling, clawing and trying to escape. Meanwhile Janet starts looking for Thor and goes into the woods. Aware of her danger, Thor manages to find and distract her back to the house before she finds Ted.
 The Wolfman), he and Ted discuss werewolves and their existence, with Ted stating that it doesnt take a full moon to start the transformation and that he has "been acquainted with a few in his time" and Brett states that werewolves dont exist. While Brett throws out the trash, Ted tries to warn his sister and advises her to start listening to Thor and his sudden change in behavior and drops hints that the murders had been done by a wolf. She ignores his pleas and he retreats into his trailer where Thor follows him, waiting for him until the sun starts to set. Ted encounters a suspicious Thor but eventually leaves the trailer with the hopes of chaining himself again. With the sun setting Ted screams for Janet to take Thor away and when she does he rushes into the woods. Thor is afraid of Ted hurting his family and begins to bark until Brett lets him out of the house. He makes his way to the woods to find that Ted was too late and wasnt able to handcuff himself and has made his way into the backyard. Werewolf Ted attacks Thor but the dog fights back which wakes up Janet. Werewolf Ted is scared away when she turns on the bright deck lights. Janet sees Thors injuries and, fearful of Teds advice, calls the Sheriff and goes into Teds trailer to notify him. She does not find Ted but instead finds his werewolf book, gruesome pictures of Marjories body and some of Teds victims. She also finds a journal in which Ted details his pain and his turmoil with not finding a cure for his "disease" and his hopes of finding peace by his familys side. Werewolf Ted seems to lurk outside the trailer but Janet leaves safely, shaken but adamant about straightening things out with her brother. Later that night a "flopsy" who had previously tried to frame Thor for a false bite goes into Janet and Bretts yard with the intent of killing Thor but is instead attacked and killed by Werewolf Ted.

The next day the sheriff shows up and questions Janet about Thor and informs her of the "flopsys" attack by a wild animal; his mutilated bodys been found 100 yards away from her property. Remembering Thors injuries Janet asks if the culprit could have been a wolf but the sheriff says no and advises her to give up Thor to the dog pound. Not believing Thor could be the killer, she confronts Ted, who provokes Thor to attack him. As a result, Thor is taken to the dog pound. Seemingly more confident and accepting of his bloodlust, Ted "marks his territory" by urinating in Thors doghouse (as Thor had done to his trailer earlier) and shows hostility towards Brett, who feels Ted is the reason Thor was turned in. Brett pretends to go to sleep but packs his backpack and sneaks out of the house to free Thor while Janet confronts Ted in the woods with a hand gun and 8 rounds. In the woods, Ted accuses his sister of not listening to his warnings and knowing the truth all along. As he begins to transform, Janet flees in panic back to the house with Werewolf Ted on her trail. Meanwhile Brett reaches the dog pound on his bicycle, breaks in and frees Thor, who takes off running and gets to the house just as Werewolf Ted is about to attack Janet. A vicious fight ensues between them with Thor savagely biting and injuring Werewolf Ted several times and Werewolf Ted throwing Thor across the room and seemingly killing him. Brett, having followed Thor and worried about his mom, goes into his room and is strangled and held up by his throat by Werewolf Ted. Seeing an opportunity, Janet unloads all eight rounds into Ted, who releases Brett and spins from the shots. Hurt but still alive he growls at the now defenseless Janet. However Thor gets up, gets between them and throws himself on Werewolf Ted, knocking them both out the window and into the yard. Werewolf Ted is severely injured but gets up and retreats into the woods. Though Thor is injured as well he follows Werewolf Ted and tracks him down until sunrise, where a now human Ted emerges from behind a tree bruised, beaten and bloody. Standing his ground and ready to attack, Thor whimpers in reluctance but Ted tells him to "do it" and with no more hesitation Thor lunges at him and finishes him off.

Some time later Janets house is being repaired and she and Brett are seen petting Thor, who is bandaged and recovering from the fight. Janet apologizes for blaming him and putting him in the dog pound. Suddenly Thor (as a werewolf) savagely growls at her. Janet wakes up alarmed and quickly realizes it was just a nightmare as she, Brett and Thor are fine and at peace.

==Cast==
* Michael Paré — Uncle Ted
* Mason Gamble — Brett
* Mariel Hemingway — Janet
* Ken Pogue — Sheriff Jenson
* Hrothgar Mathews — Flopsy
* Johanna Lebovitz — Marjorie
* Gavin Buhr — Forest Ranger
* Julia Montgomery Brown — Reporter Primo — Thor

==Reception==
Reviews were extremely unfavorable. Rotten Tomatoes gives it a score of 20% based on 5 reviews. 

==Rating==
The MPAA rated this film an R for "horror violence and gore, brief language, and a scene of sexuality". Originally, the film was issued an NC-17 rating, so a few seconds of graphic sex and violence were cut from the pre-title sequence. 

== Differences between book and film ==
The movie made many significant changes from the novel, particularly in the make-up of the family, which in the novel consisted of two parents and three children, as well as the dog, who sees the family as his pack, which must be defended at all costs. The dogs perceptions of events are treated in great detail, as is the relationship between him and his human family, and his confusion as to whether the werewolf is a threat to his family that must be eliminated, or a pack member who must be respected. These subtleties mainly did not make it into the film.

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 