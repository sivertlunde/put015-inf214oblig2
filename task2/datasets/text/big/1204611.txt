Air Bud
 
{{Infobox film name           = Air Bud image          = Air_bud_poster.jpg image size     = 185px caption        = Theatrical release poster director       = Charles Martin Smith producer       = Robert Vince Michael Strange William Vince writer         = Paul Tamasy Aaron Mendelsohn starring  Air Buddy Michael Jeter Kevin Zegers Wendy Makkena Eric Christmas Bill Cobbs music          = Brahm Wenger cinematography = Mike Southon editing        = Alison Grace studio  Keystone Entertainment distributor  Buena Vista Pictures   released       = August 1, 1997 runtime        = 98 minutes language       = English country        = United States Canada budget         = $3 million gross          = $27.7 million http://www.the-numbers.com/movie/Air-Bud#tab=summary 
}} family comedy film  that sparked the franchise centered on the real-life dog, Air Buddy (dog)|Buddy, a Golden Retriever. The film was financially successful, grossing $4 million in its opening weekend and totaling $27.7 million in its run, against an estimated $3 million budget. 

==Background==
Air Bud, also known as Buddy or Air Buddy, was a stray dog found in the Sierra Nevada mountains in summer 1989. Go Buddy! The Air Bud Story, by Kevin di Cicco, Air Bud Publishing Group, 2012.  Kevin di Cicco adopted the disheveled dog and brought him home to San Diego.

In addition to an appearance on Americas Funniest and Amazing Home Videos and multiple performances on David Lettermans shows, Buddy played the role of Comet on a February 1995 episode of Full House.  Buddys story is told in the 2012 book Go Buddy!

==Plot==
The plot revolves around a 12-year-old boy, Josh Framm. After the death of his father, who has passed on in the crash of a test flight due to a fuel shortage on his plane (this occurs offscreen before the start of the movie), Josh moves with his family to Washington State and is too shy to try out for his middle schools basketball team and too shy to make any friends. He practices basketball by himself in a makeshift court that he sets up in a disused allotment. He meets a stray dog named Buddy, a golden retriever who had escaped from his abusive owner, an unsuccessful professional clown named Norman F. Snively who is an alcoholic. Snively had locked Buddy in a transport kennel after causing trouble at a birthday party and was taking him to the dog pound when the kennel fell off the moving truck. Josh soon learns that Buddy has the uncanny ability to play basketball.

Joshs mom initially only agrees to let him keep the dog until Christmas and she plans to send him to the pound if the true owner isnt found. However, Joshs mother sees how much Josh loves Buddy and how loyal the dog is. When Josh wakes up on Christmas Day and Buddy is not in his room, he goes downstairs and sees Buddy with a bow secured on his head. She gives Buddy to Josh as a present.

Josh wants to join the basketball team but changes his mind at the last minute and becomes the water boy. After two slots are opened up and learning of Buddys talent, Josh tries out (despite basketball coach Joe Barkers reluctance) and gets a place on the team. At his first game, Buddy leaves the backyard, goes to the school and shows up while the game is underway. He runs into the court disrupts the game and causes mayhem, but the audience loves him after he scored a basket. After the game and once the dog is caught by Josh, Buddy finds coach Barker abusing Tom, one of Joshs teammates and friend who gave him a lucky orange peel he got at a Seattle SuperSonics game, by trying to make him catch better by pelting him with basketballs. He leads Josh, his mom and the school principal Ms. Pepper to the scene. Barker is fired and replaced by the schools engineer, Arthur Chaney, who Josh discovers is a former New York Knicks player. Buddy becomes the mascot of Joshs schools basketball team and begins appearing in their halftime shows.

But just before the championship game, Buddys former owner, Snively, after seeing Buddy on television, tricks his mom into believing he is the dogs owner. She reluctantly allows Sniveley to take Buddy away despite Joshs protests. Josh then enters Snivelys backyard where he finds the dog chained in the filthy backyard. Snively initially cant see Josh due to a stack of empty beer cans on his windowsill until it falls and Josh is caught trying to rescue the dog. Josh gets the chain from Buddy and both escape. Snively gets into his dilapidated clown truck to chase Josh and Buddy through a public park which scatters residents. The chase rages on to a parking lot near a lake, during which the clown truck lands into the water. A few minutes after the chase, Josh then decides to set Buddy free in the forest to find someone else. Initially, his team is losing at the next championship until Buddy shows up. When it is discovered that there is no rule that a dog cannot play basketball, Buddy joins the roster to lead the team to a come from behind championship victory.

Snively reappears and attempts to sue the Framm family for custody of Buddy. Fortunately, at the suggestion of coach Chaney, who the judge was a fan of, it is decided that Buddy will choose who will be his rightful owner. During the calling, Snively takes out his roll of newspaper, which he often used as a punishment to hit Buddy, and shouts at him. Buddy charges at Snively, tearing up the weapon of abuse and runs towards Josh. The judge grants custody of the dog to Josh while Snively, who runs at Buddy and Josh in a last-ditched effort to get the dog back, is dragged away by the police and arrested, while Josh and the rest of the citizens rejoice and gather around Buddy to welcome him back.

==Cast==
*Air Buddy (credited as "Buddy the dog") as Himself
*Kevin Zegers as Josh Framm
*Michael Jeter as Norman F. "Norm" Snively
*Wendy Makkena as Mrs. Jackie Framm
*Bill Cobbs as Coach Arthur Chaney
*Eric Christmas as Judge Cranfield
*Jonas Fredrico as Julius Aster
*Nicola Cavendish as Principal Pepper
*Brendan Fletcher as Larry Willingham
*Norman Browning as Mr. Buck Willingham
*James Manley as Professor Plum
*Joeseph Lester as Father Cunningham
*Stephen E. Miller as Coach Joe Barker
*Shayn Solberg as Tom Stewart
*Jesebel Mather and Kati Mather as Andrea Framm
*Snazin Smith as Nurse (uncredited)
*Jolene Prystae as angry extra

==Home video release==
Air Bud was released to VHS on December 23, 1997 and to DVD on February 3, 1998 (with an  .

==Reception==
The film received mixed reviews. It holds a Rotten Tomatoes score of 43% based on 21 reviews. 

==Sequels and spin-offs==
 
The film generated one theater-released sequel and many direct-to-video sequels and a spin-off film series. In each film, Buddy learns to play a different sport.

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 