Mallepuvvu
{{Infobox film
| name           = Mallepuvvu
| image          = Bhumika & Murali Krishna in Mallepuvvus Promotional Poster.jpg
| caption        = Promotional Poster
| director       = V. Samudra
| producer       = Mohan Vadlapatla
| eproducer      =
| aproducer      =
| writer         = Ramesh varma Bhumika Murali Krishna
| music          = Ilayaraja
| cinematography =
| editing        =
| distributor    =
| released       = September 19, 2008
| runtime        =
| country        = India
| awards         =
| language       = Telugu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 2008 Cinema Tollywood Cinema Indian feature directed by Bhumika and Muralikrishna  who debuts in the lead role, supported by Suman Shetty, RK, Sukumar, and Kovai Sarala. The film was launched on 30 April 2008. 

==Cast==
* Muralikrishna  Bhumika
* Suman Shetty
* RK
* Sukumar
* Sameer
* Hemasundar
* Tilak
* Kovai Sarala
* Abhinayashree
* Geetha Singh
* Telangana Shakuntala
* Farjana

==Audio Release==
Ilayaraja’s musical Mallepuvvu was launched at a grand function organized in Taj Deccan, Hyderabad on the night of 4 August. Maestro has come up with 8 melodies in just one hour and shocked everyone. He said the story has inspired him very much to come out with tunes so fast.He even bought the Tamil rights of the movie while Bhumika bought the Hindi remake rights. Bhumika Chawla is doing the title role and Murali Krishna is doing main lead. Samudra directs this film. Mohan Vadlapatla produces this film on Sri Samudra Silver Screens banner. Ramesh Varma pens story for the film, Mallepuvvu. 

==References==
 

==External links==
*  
 

 
 
 
 
 

 