Mexicana (film)
{{Infobox film
| name = Mexicana 
| image =
| image_size =
| caption =
| director = Alfred Santell
| producer = Alfred Santell
| writer = Frank Gill Jr.
| narrator =
| starring = Tito Guízar   Constance Moore   Leo Carrillo   Estelita Rodriguez
| music = Walter Scharf 
| cinematography = Jack A. Marta Arthur Roberts 
| studio = Republic Pictures
| distributor = Republic Pictures
| released = November 15, 1945
| runtime = 83 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Mexicana is a 1945 American musical film directed by Alfred Santell and starring Tito Guízar, Constance Moore and Leo Carrillo. The film was one of three made by Republic Pictures in line with the American governments Good Neighbor policy towards Latin America. Its plot is almost identical to that of another Guízar vheichle Brazil (1944 film)|Brazil (1944). 

==Partial cast==
* Tito Guízar as Pepe Villarreal 
* Constance Moore as Alison Calvert 
* Leo Carrillo as Esteban Guzman 
* Estelita Rodriguez as Lupita 
* Howard Freeman as Beagle 
* Steven Geray as Laredo  Jean Stevens as Bunny Ford  
* Bobby Barber as Bellboy 
* Maria Valadez as Chamber Maid 
* Martin Garralaga as Policeman 
* Carla Menet as Old-Fashioned Girl 
* Alma Beltran as Modern Girl 
* Craig Lawrence as Caballero

==References==
 

==Bibliography==
* Affron, Charles & Affron, Mirella Jona. Best Years: Going to the Movies, 1945-1946. Rutgers University Press, 2009.

==External links==
* 

 
 
 
 
 
 
 
 


 