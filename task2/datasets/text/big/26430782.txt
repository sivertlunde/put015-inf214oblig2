Sea Racketeers
{{Infobox film
| name           = Sea Racketeers
| image          = Poster of the movie Sea Racketeers.jpg
| image_size     =
| caption        =
| director       = Hamilton MacFadden
| producer       = Armand Schaefer (associate producer)
| writer         = Dorrell McGowan (writer) Stuart E. McGowan (writer)
| narrator       =
| starring       = See below
| music          = Ernest Miller William Morgan
| distributor    =
| released       = 1937
| runtime        = 53 minutes (American edited version) 64 minutes (USA)
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Sea Racketeers is a 1937 American film directed by Hamilton MacFadden.

== Plot summary ==
 

== Cast ==
*Weldon Heyburn as Chief Bosn Mate Jim Wilson
*Jeanne Madden as Patricia Pat Collins
*Warren Hymer as Mate Spud Jones
*Penny Singleton as Florence Toots Riley
*J. Carrol Naish as Harry Durant
*Joyce Compton as Blondie
*Charles Trowbridge as Maxwell Gordon
*Syd Saylor as Henchman Weasel
*Lane Chandler as Lt. Hays (radio voice) / Insp. L. McGrath
*Benny Burt as Henchman Maxie
*Ralph Sanford as Henchman Turk
*Don Rowan as Henchman Lew
*Bryant Washburn as Wilbur Crane

== Soundtrack ==
* Penny Singleton (as Dorothy McNulty) "The Lady Wants To Dance"
* Jeanne Madden - "What Do You Say?"
* Jeanne Madden - "Lets Finish the Dream"
* Jeanne Madden - "Even Since Adam and Eve"

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 


 