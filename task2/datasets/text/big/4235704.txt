Night Crossing
{{Infobox film
| name           = Night Crossing 
| image          = Night_crossing.jpg
| image_size     = 
| caption        = 
| director       = Delbert Mann
| producer       = Tom Leetch
| writer         = John McGreevey
| narrator       = 
| starring       = {{plainlist|
* John Hurt
* Jane Alexander
* Glynnis OConnor
* Doug McKeon
* Beau Bridges
* Ian Bannen
}}
| music          = Jerry Goldsmith
| cinematography = Tony Imi
| editing        = Gordon D. Brenner Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 107 minutes
| country        = United Kingdom United States
| language       = English
| budget         = 
| gross          = $8,000,000 (domestic)
| preceded_by    = 
| followed_by    = 
}}
Night Crossing is a 1982 drama film starring John Hurt, Jane Alexander and Beau Bridges.  The film is based on the true story of the Strelzyk and Wetzel families, who on September 16, 1979 escaped from East Germany to West Germany in a homemade hot air balloon during the days of the Inner German border-era when emigration to West Germany was strictly prohibited by the East German government. It was directed by Delbert Mann.

==Plot synopsis==
The film opens with a brief summary of 1961s then-current conditions in East Germany and nature of the border zone, featuring stock footage such as Conrad Schumanns jump over barbed wire in Berlin.

In April 1978, in the small town of   (claiming it is for a camping club); Günter sews the fabric together with a sewing machine in his attic and Peter experiments for months to construct a hot air balloon burner. Of course, they face setbacks: fires while trying to inflate the balloon, struggles to build a burner with sufficient power, extremely suspicious neighbors, and doubts about the plans workability from Günters wife, Petra.

Eventually, Petra convinces Günter to not go through with the plan. Peter and Günter then stop seeing each other, to avoid suspicion for when the Strelzyks escape. Peter and his eldest son, Frank, complete the burner and, after extensive testing, manage to inflate the balloon. On July 3, 1979, the four members of the Strelzyk family attempt to fly out. They successfully take off, though they are spotted by the border guard (who do not know what to make of it); however, a cloud dampens the balloon and the burner, and they crash within the border zone, only a few hundred feet from the fences, and the balloon floats away. Miraculously, they escape the zone, return to their car and drive home. Meanwhile, the border guard finds the balloon and the Stasi, led by Major Koerner (played by Günter Meisner), begins an investigation to find whoever built it, so they can prevent them from trying again. Initially distraught over his failure, Peter is convinced by his sons to try again, as they did fly and no one was hurt, and now the Stasi will stop at nothing to find them. Peter convinces Günter to help him and both families begin work on a larger balloon to carry them all out. Petra agrees to go with the plan, especially since her mother in West Berlin is very sick.
 Young Pioneers; the manager leaves him to notify the Stasi, prompting Peter to leave the store. They eventually finish the balloon, but have no time to test it. On September 15, 1979, the families prepare to move out while the Stasi finds blood pressure medicine belonging to Peters wife, Doris, at the place where the first balloon initially landed. They contact the pharmacy and run through all the people whom the medicine is prescribed to, eventually coming to Doris. Their neighbor (a member of the Stasi) identifies them as acting suspiciously; the families leave only minutes before the Stasi arrives at their homes. They reach their launch point while the border is placed on emergency alert.
 signal flare. The families all then happily embrace each other over the amazing success of their journey.

== Cast ==

*John Hurt : Peter Strelzyk
*Doug McKeon : Frank Strelzyk
*Beau Bridges : Günter Wetzel
*Jane Alexander : Doris Strelzyk
*Glynnis OConnor : Petra Wetzel
*Klaus Löwitsch : Schmolk
*Geoffrey Liesik as Peter Wetzel
*Michael Liesik as Andreas Wetzel
*Ian Bannen : Josef Keller
*Anne Stallybrass : Magda Keller
*Matthew Taylor : Lukas Keller
*Günter Meisner : Major Koerner
*Sky Dumont : Ziegler
*Jan Niklas : Lieutenant Fehler

==Production notes== Hof in Bavaria, did not take them west geographically, but almost due south.

*The license plates on the cars in the movie used the correct East German numbering system but were pressed on West German blanks using West German DIN-standard dies. Such plates were issued briefly, but not until after German reunification, thus creating an unintended and ironic anachronism.
 The Black Cauldron as the Horned King.

*The movie was filmed on location in Bavaria.

*The film was nominated for two Young Artist Awards — Best Family Picture and Best Young Actor (Doug McKeon)

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 