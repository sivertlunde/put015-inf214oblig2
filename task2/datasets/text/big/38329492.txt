O Homem que Virou Suco
{{Infobox film
| name           = O Homem que Virou Suco
| image          = O Homem que Virou Suco.jpg
| caption        = Theatrical release poster
| director       = João Batista de Andrade
| producer       = Wagner de Carvalho
| writer         = João Batista de Andrade
| starring       = José Dumont Celia Maracajá
| music          = Vital Farias
| cinematography = Aloysio Raulino
| editing        = Alain Fresnot
| studio         = Raíz Filmes Embrafilme
| distributor    = Embrafilme
| released       =  
| runtime        = 97 minutes
| country        = Brazil
| language       = Portuguese
}}

O Homem que Virou Suco is a 1980 Brazilian drama film written and directed by João Batista de Andrade. 

==Cast==
* José Dumont as Deraldo / Severino
* Célia Maracajá as Maria
* Denoy de Oliveira
* Freire Barros as Ceará
* Renato Master as Joseph Losey
* Rafael de Carvalho
* Ruthinéa de Moraes
* Aldo Bueno
* Dominguinhos as Himself
* Ruth Escobar
* Vital Farias
* Luís Alberto Pereira
* Pedro Sertanejo

==Reception==
It won the Golden Prize at the 12th Moscow International Film Festival.   

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 