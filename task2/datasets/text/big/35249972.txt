Teenage Mutant Ninja Turtles (2014 film)
 
{{Infobox film
| name = Teenage Mutant Ninja Turtles
| image = Teenage Mutant Ninja Turtles film July 2014 poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Jonathan Liebesman
| producer = {{Plainlist|
* Michael Bay
* Andrew Form
* Bradley Fuller
* Galen Walker
* Scott Mednick
* Ian Bryce}}
| screenplay = {{Plainlist|
* Josh Appelbaum
* André Nemec
* Evan Daugherty}}
| based on =  
| starring = {{Plainlist|
* Megan Fox
* Will Arnett
* William Fichtner
* Whoopi Goldberg
* Alan Ritchson Jeremy Howard
* Noel Fisher
* Johnny Knoxville
* Tony Shalhoub}} Brian Tyler
| cinematography = Lula Carvalho
| editing = {{Plainlist|
* Joel Negron
* Glen Scantlebury}}
| studio = {{Plainlist|
* Nickelodeon Movies
* Platinum Dunes
* Gama Entertainment
* Mednick Productions Heavy Metal}}
| distributor = Paramount Pictures
| released =  
| runtime = 101 minutes   
| country = United States
| language = English
| budget = $125 million 
| gross = $485 million   
}} 3D Science science fiction franchise of reboot of Jeremy Howard, Pete Ploszek, Noel Fisher, Will Arnett, Danny Woodburn, William Fichtner, Johnny Knoxville, and Tony Shalhoub.

The film was announced shortly before Teenage Mutant Ninja Turtles co-creator Peter Laird sold the rights to the franchise to Nickelodeon in October 2009. It was produced by Nickelodeon Movies and Michael Bays production company Platinum Dunes, and distributed by Paramount Pictures.
 35th edition, Worst Picture Worst Prequel, Worst Supporting Actress.

A   is scheduled to be released on June 3, 2016.      

==Plot==
  CEO Eric Sacks (William Fichtner), who was her late fathers (Paul Fitzgerald) lab partner.
 Jeremy Howard), Michaelangelo (Noel Splinter (Danny Woodburn and Tony Shalhoub). Unable to convince Bernadette of the turtles existence, April is fired. Her coworker Vern Fenwick (Will Arnett) drives her to Eric Sacks estate where she confides in him about her discovery. Eric believes her and reveals his hope that they can unlock the secrets of a mutagen he and Aprils father had been experimenting with to cure disease, but which is thought lost in the fire that killed her dad.

At Splinters behest, the turtles bring April her to their sewer lair. Splinter explains April had saved them all from the fire and freed them into the sewers. The mutagen caused the five of them to grow and develop humanoid attributes. Splinter took on the role of their father, using Aprils father as an example. After finding a book on Ninjitsu in a storm drain, he proceeded to teach himself, then the turtles, in the fighting style. April reveals she told Sacks where Splinter says betrayed her father and killed him. Suddenly, the Foot Soldiers attacks. The turtles and Splinter fight valiantly until Shredder arrives and defeats Splinter. A series of explosives incapacitate Raphael while the other turtles are captured. April comes out of hiding and she and Raphael plan to save the others. Vern, now convinced of the turtles existence, drives them to Sacks estate. They break in as the other turtles are being drained of their blood in order to create an antidote to a deadly virus Sacks hopes to flood New York with, in order to become rich from people seeking his cure. Raphael fights Shredder while April and Vern free the others by injecting them with adrenaline. They escape the compound in pursuit of Sacks, but are followed by heavy machines and trucks. The group survive the onslaught, though Verns van is destroyed.

On a radio tower In the city, Sacks and Shredder plant a device that will  flood the city. April and Vern subdue Sacks in the lab, while the turtles fight Shredder on the roof. During the fight, the tower supports collapse; the turtles try to keep it from falling and infecting the city. April confronts Shredder with the mutagen. In the struggle, the tower collapses and the Turtles pull April onto it with them as Shredder falls to the street as the police converge on him. Believing theyre about to be destroyed, the Turtles confess their secrets, while Raphael gives an impassioned speech of his love for his brothers before they land harmlessly on the street. They vanish before the humans find them and return to the sewers, where they give Splinter the mutagen and he begins to recover.
 Happy Together".

==Cast==

===Live-action actors===
* Megan Fox as April ONeil
** Malina Weissman as Young April ONeil Vern Fenwick
* William Fichtner as Eric Sacks Shredder
* Bernadette Thompson Karai
* Abby Elliott as Taylor
* Taran Killam as McNaughton
* K. Todd Freeman as Dr. Baxter Stockman
* Paul Fitzgerald as Dr. ONeil
* Derek Mears as Dojo Ninja

===Voice actors and motion-capture performers=== Raphael
* Michelangelo
* Leonardo
* Jeremy Howard Donatello
* Splinter

==Production==

===Development===
In October 2009, following the news of Nickelodeon purchasing all of Mirage Studios|Mirages rights to the Teenage Mutant Ninja Turtles property, it was announced that Nickelodeon would produce a new film through corporate sibling Paramount Pictures with an expected release date sometime in 2012.    In late May 2010, it was announced that Paramount and Nickelodeon had brought Michael Bay and his Platinum Dunes partners Bradley Fuller and Andrew Form on to produce the next film that will reboot the film series. Bay, Fuller, and Form would produce alongside Walker and Mednick.  For the script, the studio originally hired Matt Holloway and Art Marcum to write the film for close to a million dollars. According to TMNT co-creator Kevin Eastman, the John Fusco version was a little too edgy for what Paramount wanted.  A year later, the studio turned to writers Josh Appelbaum and André Nemec to rewrite the script.   In February 2012, Jonathan Liebesman began negotiations to direct the film, beating out Brett Ratner.  Later in March, it was announced that Paramount had pushed back films release date to Christmas Day 2013.  In early March 2012, Bay revealed at Nickelodeons 2012 upfront presentation that the film will be simply titled Ninja Turtles and that the turtles would be "from an alien race".   News of Bays creative change was met with criticism from within the fan community. 
 Michelangelo in Leonardo in the first three films, and Judith Hoag, who played April ONeil in the first film, have voiced their support towards the creative change.  TMNT co-creator Peter Laird expressed his thoughts on the change asking fans to take Bays advice and wait until more of Bays plan is made available. Laird also stated that he felt the "ill-conceived plan" could be a "genius notion," as it would allow fans to have the multitude of bipedal anthropomorphic turtles that they have been asking for. He would point out that while the concept of a turtle-planet backstory made for a great run-of-the-mill science fiction story, it had no real place in the Ninja Turtles universe. 
 Donatello in the first and third films, voiced his support for the film saying that he loves Bays remakes and he is eager to reprise his role.  In response to the backlash, Liebesman stated that he was glad to hear about the fans response, since he and Eastman had been locked in a room working on ideas that, from his own perspective as a fan, everybody would love.  While he would not confirm whether or not Bays comment did represent the films premise, he did stress on the ooze itself and its background in the original comic, reminding that the ooze was the product of alien technology. In regards to how the Turtles would be rendered, Liebesman would not say exactly what visual direction would be taken, but he did state that he enjoyed Weta Digitals work in Rise of the Planet of the Apes. He also pointed out that the film would not be exclusively about action but will also focus on brotherhood, friendship, and responsibility. 

In late March 2012, Bay posted on his website explaining the title change and stressing that nothing had changed regarding the Turtles. He stated the reason the title was shortened was a request by Paramount to make the title "simple". He continued that the Turtles were the same as fans remember and regardless of the title change they still act like teenagers. He urged everyone to give everybody who was involved a chance, as they had the fans interest at top priority and would not let anybody down.  On June 12, 2012, Eastman revealed some of the things for the film stating that April would not be 16 years old like in the  .   Three days later, it was reported that production for this project had been shut down. While the release date had been pushed back five months, the work stoppage for the film was said to be "indefinite".  However, other sources said that the film would be released on May 16, 2014 due to problems in that script that need to be corrected.   In July 2012, Eastman called it "easily the best Turtle movie yet".  Kevin Eastman stated that the movie is creating its own story but has to be true to the source material or else they will get "murdered". 

In August 2012, an early version of the script, dated January 30, 2012, titled "The Blue Door" and written by Appelbaum and Nemec, was leaked online. It featured major changes to the origins: the Turtles hail from another dimension that consists of turtle warriors,  . 

===Casting===
In mid-February 2013, actress Megan Fox was reported to be cast as April ONeil,    marking her first collaboration with Bay since her remark comparing him to Adolf Hitler.    Bay confirmed Fox is back in good terms with him as early as April 2011.   Jessica Biel had expressed interest in playing the part.  In regards to Foxs casting, Laird commented that he felt there were better choices to play April, but that he would prefer not to get too worked up over the issue. 
 Jeremy Howard Vern Fenwick.   Soon after that, actor Danny Woodburn joined the cast as Splinter (Teenage Mutant Ninja Turtles)|Splinter.   
 Pearl Harbor. Burne Thompson.  On June 22, 2013, Fichtner revealed to the Huffington Post that he is playing a character named Eric Sacks.   In October 2013, William Fichtner revealed that Bebop and Rocksteady would not be appearing in the film.  In March 2014, it was revealed that Abby Elliott would be playing April ONeils roommate. {{cite web|url=http://www.comingsoon.net/news/movienews.php?id=116409 |quote=Q: Can you say who Abby Elliott is playing?
Nemec: Probably. She plays April ONeills roommate. |title=Watch the First Trailer for Teenage Mutant Ninja Turtles! |publisher=ComingSoon.net |date=March 27, 2014 |accessdate=2014-03-27}}  In April 2014, Elliott revealed that her character will be named Taylor, and on April 3, actors Johnny Knoxville and Tony Shalhoub joined the film as the voices of Leonardo and Splinter respectively.  

===Filming=== Tupper Lake, the 2014 remake of RoboCop, described the process as "demanding much imagination and participation of the visual effects supervisor", given that despite their presence on the set the actors would effectively be replaced by computer-generated creatures.  Production for the film wrapped on August 6, 2013.   Additional filming occurred in January and April 2014.   

===Visual effects=== football player Messi and plays association football|soccer.” 

==Release==
 , Australia]]
The release date was moved around several times until it was set for August 8, 2014.  The film premiered on July 29, 2014 in Mexico City.    Premiere events also occurred in Los Angeles and New York City.   On September 12, 2014, the film was released in IMAX 3D for a one-week limited engagement. 

===Marketing=== teaser trailer Ty Dolla $ign featuring Kill the Noise and Madsonik was released, as it is part of the movies soundtrack.  An extended TV spot debuted the following day.  Paramount promoted the film on July 24, 2014 at San Diego Comic-Con International.   Five new TV spots were released that day as well.  The music video for the song, "Shell Shocked" debuted on July 28, 2014.  On August 1, 2014, Paramount debuted eleven new TV spots for the film.  On August 4, 2014, Pentatonix released a new song titled "We Are Ninjas" and a music video as part of the promotion for the film.  

In Australia, a poster was released which featured the four turtles jumping from an exploding skyscraper as promotion for its September 11 release.  The poster offended many people since the World Trade Center was destroyed back in 2001 during the September 11 attacks.   E! Online, Retrieved July 29, 2014  Paramount apologized and removed the poster.   

===Box office===
Teenage Mutant Ninja Turtles grossed $191.2 million in North America and $293.8 million in other countries for a worldwide total of $485 million, against a budget of $125 million.  Calculating in all expenses, Deadline.com estimated that the film made a profit of $81.3 million. 
 Guardians of the Galaxy reclaimed the top spot. 

The film led the foreign box office during the weekend lasting from October 31 through November 2, 2014, by grossing $34.7 million ($26.5 million coming from China). 

===Critical reception===
Teenage Mutant Ninja Turtles received generally negative reviews from critics, with the criticism around the acting performances, fast pacing, poorly produced CGI and the generic story and characters, with several critics making negative comparisons to the  , the film has a score of 31 out of 100, based on 33 critics, indicating "generally unfavorable reviews".  Audiences polled by CinemaScore gave the film a B grade on a scale of A to F.   
 Dark Knight Joker to 1990 original, but still harmless junk at best."  Peter Howell of the Toronto Star gave the film one and half stars out of four, saying "Not much of an effort is made to differentiate the personalities of the turtles, who all frankly look as grotesque as a Terry Gilliam cartoon."  Nicolas Rapold of The New York Times said "Attached to this movie, the title no longer sounds zany; it looks like a series of keywords."  Mark Olsen of the Los Angeles Times said "There is something half-hearted about the entire film, as if those behind it were involved not because they wanted to make it, not because they should make it, but just because they could."  Kyle Anderson of Entertainment Weekly gave the film a C+, saying "Too-brief thrills only shine a harsher light on the films laborious pacing and cringeworthy one-liners spilling from the maws of the ninja teens." 

Justin Lowe of The Hollywood Reporter gave the film a positive review, saying "Liebesman relies on his genre-film resume to keep events moving at a brisk clip and the motion-capture process employed to facilitate live-action integration with cutting-edge VFX looks superior onscreen."  Justin Chang of Variety (magazine)|Variety said the film is "Neither a particularly good movie nor the pop-cultural travesty that some were dreading."  A.A. Dowd of The A.V. Club gave the film a C+, saying "What the new Teenage Mutant Ninja Turtles lacks is not fidelity, but a spirit of genuine boyish fun -- the sense that anyone involved saw more than a very specific shade of green in the freshly digital scales of these 30-year-old characters."  Soren Anderson of The Seattle Times gave the film one out of four stars, saying "If ever there was a movie that should not have been made, this is that movie."  Drew Hunt of Chicago Reader said "The light, comedic tone is weighed down by unimaginative pop-culture references and half-witted one-liners."  Bill Goodykoontz of The Arizona Republic gave the film two out of five stars, saying "Its just kind of a mess, as unfocused and immature as the four mutant turtles at its core. Stuff happens, stuff blows up and this is probably a good time to mention that Michael Bay produced the film."  Alonso Duralde of The Wrap gave the film a negative review, saying "Teenage Mutant Ninja Turtles is a movie that takes its characters and its premise seriously, until it doesnt, and that operates at two speeds: tortoise (ponderous) and hare (head-spinning)." 
 original series."  Steven Rea of The Philadelphia Inquirer gave the film two out of four stars, saying "The kind of cliched, misfit crimefighters-versus-demented villains scenario that Kevin Eastman and Peter Laird happily parodied when they came up with the original Teenage Mutant Ninja Turtles comic books way back in the 1980s."  Tom Russo of The Boston Globe gave the film one and a half stars out of five, saying "The repartee, as ever, is weak. Even with all the extra layers of digital detail, its still tough to keep these four straight."  Cliff Lee of The Globe and Mail gave the film one and a half stars out of four, saying "For having gone to the trouble of making a self-descriptive movie called Teenage Mutant Ninja Turtles, its producers seem ultimately unsure about its most basic concept."  James Berardinelli of ReelViews gave the film one and a half stars out of four, saying "Teenage Mutant Ninja Turtles doesnt so much provide brainless enjoyment as it pummels the viewer into submission. "Shell-shocked" is a reasonable description of the experience."  Chris Cabin of Slant Magazine gave the film one out of four stars, saying Teenage Mutant Ninja Turtles only leaves one with the dim afterglow of forced normalcy, of a film so overworked to ensure mass-market appeal that it loses the charming oddness and loose goofiness that has allowed these characters, and their "frothy" appeal, to endure." 

===Accolades===
  
{| class="wikitable sortable"
|+ List of awards and nominations
! Year !! Award !! Category !! Recipients !! Result
|-
| rowspan="9"| 2015
|-
|rowspan="5"| 35th Golden Raspberry Awards Golden Raspberry Worst Picture
|rowspan="4"  
|- Golden Raspberry Worst Prequel, Remake, Rip-off or Sequel
|- Worst Director
| Jonathan Liebesman
|- Worst Screenplay
| Josh Appelbaum, André Nemec and Evan Daugherty
|- Worst Supporting Actress
| Megan Fox
| rowspan="1"  
|- 28th Kids Choice Awards Favorite Movie
|rowspan="3"  
|-
| Favorite Movie Actor
| Will Arnett  (also for The Lego Movie) 
|-
| Favorite Movie Actress
| Megan Fox
|}

===Home media===
Teenage Mutant Ninja Turtles was released on Digital HD on November 25, 2014 and was released on DVD and 2D and 3D Blu-ray Disc|Blu-ray on December 16, 2014.   The film topped the home video sales charts in its first week and achieved the highest ratio of disc sales to theatrical tickets sales its first week in stores.  It retained the top spot on the home video sales chart in its second weekend. 

==Soundtrack==
{{Infobox album
| Name       = Teenage Mutant Ninja Turtles: The Score
| Type       = Soundtrack Brian Tyler
| Cover      = 
| Released   = August 5, 2014
| Recorded   = 2014
| Genre      = Film score
| Length     = 70:02
| Label      = Atlantic Records
| Producer   = Brian Tyler Brian Tyler film scores
| Last album =   (2013)
| This album = Teenage Mutant Ninja Turtles (2014)
| Next album = The Expendables 3 (2014)
}} Brian Tyler. The soundtrack was released by Atlantic Records on August 5, 2014. 

{{Track listing Brian Tyler
| total_length    = 70:02

| title1          = Teenage Mutant Ninja Turtles
| length1         = 4:45

| title2          = Adolescent Genetically Altered Shinobi Terrapins
| length2         = 4:31

| title3          = Splinter vs. Shredder
| length3         = 6:25

| title4          = Origins
| length4         = 6:02

| title5          = Brotherhood
| length5         = 1:19

| title6          = Turtles United
| length6         = 4:10

| title7          = Rise of the Four
| length7         = 3:34

| title8          = The Foot Clan
| length8         = 3:17

| title9          = Shell Shocked
| length9         = 3:23

| title10         = Project Renaissance
| length10        = 1:57

| title11         = Shortcut
| length11        = 4:41

| title12         = Shredder
| length12        = 5:59

| title13         = Cowabunga
| length13        = 4:35

| title14         = 99 Cheese Pizza
| length14        = 1:49

| title15         = Adrenaline
| length15        = 6:26

| title16         = Buck Buck
| length16        = 4:11

| title17         = TMNT March
| length17        = 2:07
}}

==Video games== Magic Pockets released a Nintendo 3DS game based on the film on August 8, 2014 to coincide with the movie.  A mobile game, also based on the film, was released on July 24, 2014. 

==Sequels==
 
Fichtner revealed in an interview that he has signed on for three TMNT films.  Noel Fisher also revealed in an interview that all four of the turtle actors have signed on for three films as well. 
 Dimension X in the sequels as well.  On August 10, 2014, Paramount announced that a sequel will be released on June 3, 2016, with Michael Bay returning as producer and Josh Appelbaum and André Nemec coming back as screenwriters and executive produers.   Fox and Arnett are expected to return in the sequel along with the character Shredder.   
 Dave Green, Buffalo in April 2015.    In March 2015, Victorias Secrets supermodel Alessandra Ambrosio was cast as herself and a love interest to Vern, also Los Angeles Clippers players DeAndre Jordan, Matt Barnes, J.J. Redick, Austin Rivers and Spencer Hawes will be making cameos in the film. It was also announced that actor Stephen Amell has been cast as Casey Jones for the sequel.    In April 2015, Paramount officially confirmed that Bebop and Rocksteady would appear in the sequel and showed the first images of them at CinemaCon. Variety announced that Tyler Perry was cast as Baxter Stockman and Brian Tee will portray Shredder in the sequel.   

==See also==
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*    

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 