The Star Reporter
{{Infobox film
| name           = The Star Reporter
| image          = 
| image_size     = 
| caption        = 
| director       = Michael Powell Jerome Jackson
| writer         = Ralph Smart Philip MacDonald
| starring       = Harold French Garry Marsh
| music          = 
| cinematography = Geoffrey Faithfull
| editing        = 
| distributor    = Film Engineering
| released       = 9 May 1932
| runtime        = 44 min.
| country        =   English
}}
 1932 British crime drama, directed by Michael Powell and starring Harold French and Garry Marsh.  The screenplay was adapted from a story by popular thriller writer Philip MacDonald.

The Star Reporter is one of eleven quota quickies directed by Powell between 1931 and 1936 of which no print is known to survive.  The film is not held in the BFI National Archive, and is classed as "missing, believed lost".   powell-pressburger.org Retrieved 12-08-2010 
 Platinum Blonde, and Powell also remembered his amusement when a critic observed sniffily that his film lacked the polish of the main feature, reasoning that this was perhaps to be expected when comparing his budget with the $600,000 which had reportedly been spent on the Harlow picture. 

==Plot==
Major Starr (French) is an ambitious newspaper reporter who has taken undercover employment as chauffeur to Lady Susan Loman (Isla Bevan) in the hope of witnessing high-society goings-on which he can use in a feature article he is planning.  Lady Susans father Lord Longbourne (Spencer Trevor) meanwhile is experiencing financial embarrassment, and is persuaded by professional criminal Mandel (Marsh) to conspire in an insurance scam whereby Mandel will steal a diamond belonging to Lady Susan from the West End jeweller where it is currently on display, Longbourne will claim the cash and Mandel will return the diamond to him for a cut of the proceeds.

Mandel steals the diamond in an audacious smash-and-grab raid but the crime is witnessed by Starr and Lady Susan, who happen to be passing at the time.  Starr heads off in pursuit of Mandel and corners him on a rooftop.  There is a struggle and Mandel falls to his death.  With the scam foiled and the diamond retrieved, Starr proposes to Lady Susan, who is happy to accept.

==Cast==
* Harold French as Major Starr
* Garry Marsh as Mandel
* Isla Bevan as Lady Susan Loman
* Spencer Trevor as Lord Longbourne Anthony Holles as Bonzo
* Noel Dainton as Colonel
* Elsa Graves as Oliver
* Philip Morant as Jeff

==Reception== London Evening News reviewer enthused: "At the end of a long and not very inspiring day of seeing new films, I saw a little picture Star Reporter which jolted my tired brain into renewed enthusiasm. Star Reporter packs into three-quarters of an hour as much story as most films that last an hour and a half...(it) tells an exciting crook story with a smoothness of direction and a crispness of acting and cutting which would be a credit to the most ambitious picture."   Picturegoer Weekly predicted, wrongly as it turned out: "It is all very ingenious and is chiefly notable for the introduction of Isla Bevan, a new star, who looks like making good" (Bevans film career in fact encompassed only five more programmers, and was over by 1936)  and added "the picture generally is quite fairly entertaining, if one is not too critical". 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 