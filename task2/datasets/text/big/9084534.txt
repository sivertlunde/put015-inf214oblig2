15 Maiden Lane
{{Infobox film
| name           = 15 Maiden Lane
| image          = 15_maiden_lane.jpg
| caption        = Promotional poster for 15 Maiden Lane
| director       = Allan Dwan
| producer       = Sol M. Wurtzel John Patrick David Silverstein
| starring       = Claire Trevor Cesar Romero Lloyd Nolan Douglas Fowley
| music          = Samuel Kaylin
| cinematography = John F. Seitz
| editing        = Alex Troffey
| distributor    = 20th Century Fox
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Maiden Lane Fulton Street 47th Street restored print of the film in June 2013 as part of an Allan Dwan retrospective.

==Cast==
* Claire Trevor as Jane Martin
* Cesar Romero as Frank Peyton
* Lloyd Nolan as Detective Walsh
* Douglas Fowley as Nick Shelby
* Lester Matthews as Gilbert Lockhart
* Robert McWade as John Graves
* Holmes Herbert as Harold Anderson

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 