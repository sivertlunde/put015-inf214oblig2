Outlaw: The Saga of Gisli
{{Infobox film
 | name           = Outlaw: The Saga of Gisli
 | image          = Utlaginn01.jpg
 | caption        = Video case
 | director       = Ágúst Guðmundsson
 | producer       = Jón Hermannsson
 | writer         = Ágúst Guðmundsson
 | starring       = 
 | music          = Askell Másson
 | cinematography = Sigurður Sverrir Pálsson
 | editing        = William River
 | distributor    = 
 | released       =  
 | runtime        = 100 minutes
 | country        = Iceland
 | language       = Icelandic
 | budget         = 
 | gross          = 
    }}

Outlaw: The Saga of Gisli ( ) is an Icelandic film shot in 1981, detailing a blood feud that took place in Viking times. It is based upon one of the Icelanders Sagas called the Gísla saga and is directed by Ágúst Guðmundsson.
 Best Foreign Language Film at the 54th Academy Awards, but was not accepted as a nominee. 

==Plot==
This Icelandic Saga begins with Gisli, Thorgrim, Thorkel, and Vestein making a blood pact to protect one another. Controversy breaks out when one of the men, Thorgrim, refuses to complete the pact because he does not want any involvement with Vestein. Thorgrim claims that taking on Vestein as a brother is taking on more trouble than he can handle. The scene ends with the men storming off from one another and the blood oath incomplete. The plot begins to unfold when Thorkel hears his wife tell Aud, Gisli’s wife, that she had amorous feelings for Vestein before she married Thorkel, leading Thorkel to suspect his wife was unfaithful.

As the saga continues, Vestein is killed one night in Gislis home by a masked man.  Gisli rushes into the house and pulls the spear from Vestein’s dead body thereby ensuring that he will avenge Vesteins murder. Gisli, with some investigation of his own, concludes that Thorgrim is responsible for the death of Vestein. Therefore, he convinces one of Thorgrim’s servants to leave the door to Thorgrim’s house open one night. Gisli enters the house and stabs Thorgrim with the same spear that Thorgrim supposedly used to kill Vestein. A group of Thorgrim’s servants race to Gisli’s home to inquire if anyone strange has come by but they have no luck. After the funeral Bork, Thorgrims brother, marries Thorgrim’s widow, Thordis, who is also Gisli’s sister. Thordis then tells her new husband that she believes Gisli is responsible for the death of her first husband. In addition, she tells Bork that he must kill Gisli in order to get revenge for the murder of his brother and she will marry him. As a result, some of Bork’s men travel to a sorcerer to convince him to place a curse on the Thorgrims murderer. 
 sorcerer interferes with Gisli’s dreams so that he has nightmares. Gisli, throughout the saga, is confronted with dreams of an evil woman and a good woman. These women play a crucial role in his daily life and the dreams seem to reveal the intentions of the women. These women are his sister, Thordis, and his wife, Aud. Gisli then flees his home, just before Bork and his men arrive and harass his wife and daughter. Cursed by the sorcerer, Gisli runs from the gang of men that is after him and is almost captured on numerous occasions. One example of his great escapes is when one of Bork’s men come to an island and discovers that Gisli is there hiding with a poor farmer and his wife. When Gisli finds out that his enemies know where he stays he decides to act like the halfwit, who occasionally sits on the lawn, in order to escape from his pursuers. On this same occurrence he escapes another time by lying under a woman’s mattress while the gang of men search the house.

Another great escape is when Gisli decides to hide in a cave near his home and is one day discovered by one of the men. As this is happening, Vestein’s two sons dress up as girls and kill Thorkel, Gisli’s brother, who they believe was partially responsible for the death of their father. They run to Gisli’s wife, Vestein’s sister, for help but she refuses and tells the children to run away before Gisli returns. Eyjolf, one of Bork’s henchmen, along with the rest of the gang, finds Gisli. Gisli, tired of fleeing from the men, fights to his death, killing many of Bork’s henchmen. Surprisingly, Thordis feels guilty for encouraging Bork to kill her brother after she learns Gisli is dead. As a result of her guilt, when Eyjolf visits Bork, Thordis attempts to stab him with a knife to avenge her brother’s death. In the sagas conclusion, Thordis divorces Bork in front of Eyjolf and the other house guests.

==Cast and characters==
{| class="wikitable"
|-
! Character
! Actor
! Information
|-
| Gisli Arnar Jónsson 
| Protagonist who is forced into seeking vengeance after the death of his brother-in law, Vestein.
|-
| Aud
| Ragnheiður Steindórsdóttir
| Gisli’s wife.  Vesteins sister.  Stays loyal to her husband even when faced with her own death.
|-
| Thorgrim
| Benedikt Sigurðarson
| Brother-in-law to Gisli and suspected murderer of Vestein.  Refused to make the oath with Vestein because he felt it would cause him more trouble and stress.
|-
| Thorkel
| Þráinn Karlsson
| Brother of Gisli whose wife’s interest in Vestein is believed to indirectly lead to his death .
|-
| Asgerd
| Kristin Kristjansdottir
| Wife of Thorkel who has a crush on Vestein.
|-
| Vestein
| Kristján Jóhann Jónsson
| Brother-in-law to Gisli who is murdered in his sleep.
|-
| Eyjolf
| Helgi Skúlason
| Leader of the group of men who hunt Gisli.
|-
| Thordis
| Tinna Gunnlaugsdóttir
| Wife to Thorgrim and sister to Gisli. Urges Bork to avenge the death of his brother Thorgrim by killing Gisli.
|-
| Bork
| Bjarni Steingrímsson
| Brother to Thorgrim who marries Thordis shortly after his brother’s death. Is talked into seeking vengeance for his brother’s death by Thordis.
|-
| Helgi
| Sveinbjorn Matthiasson
| One of Eyjolfs Men.
|}

==Source material==
This film follows the story of Gisli, his family, and his adventures as an outlaw included in the thirteenth century Icelandic saga entitled “Gísla saga” (“Gisli Sursson’s Saga”).  The events take place from 940-980, but the saga itself was written around 1270-1320.  Two written versions of the saga exist, one of which is translated by Martin S. Regal. However, there are slight deviations throughout the film.  The role of Gisli’s dreams seems unimportant throughout the course of the film until the final battle between Gisli and his hunters, but in the saga his dreams played a bigger role in predicting his fate.  Also, the saga did not give faces to the women portrayed in Gisli’s dreams.  The film, however, depicts the good dream woman as Gisli’s wife and the bad dream woman as his sister.  This may have been done in the film to ensure that the audience realized the critical roles that the women played in the course of events in the story. 

In the text, the character of Gisli was depicted as a larger than life character who “died with so many and such great wounds that it was an amazement to all". Regal, Martin S., trans. "Gisli Surssons Saga." The Sagas of the Icelanders. New York: Penguin Books, 2000. 496-557.  This assessment of Gisli’s death leads the reader to believe that Gisli was in fact a humongous man with an irrefutable spirit. In the movie however, Gisli was depicted as a man of normal stature with the will to over come the obstacles placed before him and to fight through the challenges he had. 

This saga, like many other sagas, focuses on history and feud within families, and the conflicts that arose between Icelandic settlers during those times. The majority of sagas of Icelanders took place in the 9th and 10th centuries and they were written in the 13th and 14th centuries. Icelandic Sagas are known for their family themes which are particularly present in this saga. The characters within these sagas are typically Vikings or closely related to the Vikings. Speaking in verse is also characteristic of Icelandic Sagas, which the main character in Gisli Sursson’s Saga does several times throughout the text. Unlike many other stories that were circulated about nobles or kings, these stories depicted the lives of the ordinary person. Not only are the characters generally ordinary folk but they are also portrayed as possessing super strength.

==See also==
* Gísla saga
* List of historical drama films
* List of submissions to the 54th Academy Awards for Best Foreign Language Film
* List of Icelandic submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
*  
*  

 
 
 
 
 
 
 
 
 