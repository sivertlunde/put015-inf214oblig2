Cupid in Clover
{{Infobox film
| name           = Cupid in Clover
| image          =
| caption        = Frank Miller
| producer       = 
| writer         = Upton Gray   Frank Miller George Wynn   Winifred Evans   Herbert Langley   Wyndham Guise
| music          =
| cinematography =  
| editing        = 
| studio         = British Screen Productions
| distributor    = British Screen Productions 
| released       = February 1929
| runtime        = 6,481 feet 
| country        = United Kingdom 
| awards         =
| language       = English 
| budget         = 
| preceded_by    =
| followed_by    =
}} British romance Frank Miller George Wynn, Winifred Evans and Herbert Langley.  It was adapted from the novel Yellow Corn by Upton Gray and made at Isleworth Studios in London.

==Cast==
* Winifred Evans - Lyddy George Wynn - Fred Amyon 
* Herbert Langley - John Simpson 
* Betty Siddons - Clary Simpson 
* Eric Findon - George Dowey  Charles Garry - Joe Dowey  Marie Esterhazy - Maggie 
* Wyndham Guise    James Knight    Jack Miller

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 
 
 
 
 
 
 


 