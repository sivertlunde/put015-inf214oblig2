The Blind Woman of Sorrento (1952 film)
{{Infobox film
| name           =The Blind Woman of Sorrento 
| image          =The Blind Woman of Sorrento (1952 film).jpg
| image_size     =
| caption        =
| director       = Giacomo Gentilomo Giacomo Savelli
| writer         =Francesco Mastriani (novel)   Ivo Perilli   Liana Ferri
| narrator       = Paul Campbell   Enzo Biliotti   Marilyn Buferd
| music          = Carlo Rustichelli
| cinematography =Romolo Garroni
| editor       = Elsa Dubbini
| studio        = Cine Produzione Astoria 
| distributor    =
| released       = 1952
| runtime        = 93 minutes
| country        = Italy Italian
| budget         =
}} historical drama Paul Campbell novel of the same title by Francesco Mastriani and is the third time that it has been filmed. It is set in the nineteenth century in Sorrento in southern Italy.

== Cast ==
* Antonella Lualdi as Beatrice di Rionero   Paul Campbell as Oliviero Pisani 
* Enzo Biliotti as Ernesto Basileo, il notaio  
* Marilyn Buferd as Marchesina di Rionero   Charles Fawcett as Marchese di Rionero 
* Corrado Annicelli as Dottore Andrea Pisani  
* Vera Carmi as Elena Viscardi   Paul Muller as Carlo Basileo  
* Nuccia Aronne as Beatrice di Rionero, bambina 
* Michele Riccardini as Un congiurato  
* Giovanni Onorato as Altro congiurato 
* Maurizio Di Nardo as Oliviero, ragazzino  
* Giuliano Montaldo as Sacerdote 
* Sergio Bergonzelli
* Carlo dElia
* Giovanni Vari 
* Franco Marturano 
* Rina Dei
* Patrizia Lari  
* Gianni Luda 
* Vittorio Braschi 
* Annette Ciarli
* Carlo Hinterman 
* Anna Maini 
* Ina La Yana
* Raffaele Caporilli

== References ==
 
 
== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 