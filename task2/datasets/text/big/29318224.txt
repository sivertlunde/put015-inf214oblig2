Les Hussards
{{Infobox film
| name           = Les Hussards
| image          = Les-Hussards-poster.jpg
| caption        = Film poster
| director       = Alex Joffé
| producer       = Ignace Morgenstern
| writer         = Gabriel Arout Jean Halain
| based on       =  
| starring       = Bernard Blier Louis de Funès
| music          = Georges Auric
| cinematography = Raymond Heil

| editing        = Henri Rust
| studio = Cocinor, Cocinex, Sédif

| distributor    = Cocinor
| released       = 8 November 1955 (France)
| runtime        = 102 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| gross = 
}}
 French Comedy comedy film from 1955, directed by Alex Joffé, written by Gabriel Arout, starring Bernard Blier and Louis de Funès.  The film is known under the titles "Cavalrymen" (international English title), "Les hussards" (Belgium French title), "De husaren" (Belgium Flemish title), "La piccola guerra" (Italy), "Huszárok" (Hungary). 

==Plot== private Jean-Louis forfeit their horses. Afraid of a severe punishment both lie to Captain Georges. They just make up a story about them having been the target of a selective ambush. Little later their regiment is annihilated by an Austrian attack. Only the two humbuggers are lucky enough to survive. Napoleon has them celebrated as heroes and thus they get into the books of history.

== Cast ==
* Bernard Blier: Brigadier Le Gouce
* Giovanna Ralli: Cosima
* André Bourvil: private ("trumpeter") Jean-Louis
* Louis de Funès: Luigi, the sexton 
* Virna Lisi: Elisa
* Clélia Matania: Mrs Luppi
* Georges Wilson: Captain Georges
* Marcel Daxely: Giacomo, the shepherd
* Carlo Campanini: Mr Luppi
* Giani Esposito: Pietro
* Jean-Marie Amato: Carotti, the shoemaker
* Franco Pesce: the clergyman
* Alberto Bonnuci: Raphaël
* Jess Hahn: a hussar
* Albert Rémy: a hussar
* Maurice Chevit: hussar Camille
* Roger Hanin: a soldier

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 

 