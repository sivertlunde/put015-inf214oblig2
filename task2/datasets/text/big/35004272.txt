D4 (film)
 
 
{{Infobox film
| name           = D4
| image          = D4 (film) poster.jpg
| alt            =  
| caption        = 
| director       = Darrin Dickerson
| producer       = David B. Stevens
| writer         = Darrin Dickerson
| starring       = Vicki Askew Ella Bell Eric Berner
| music          = Nathan Whitehead
| cinematography = Jeremy Gonzales
| editing        = Darrin Dickerson
| studio         = Ghostwater Film
| distributor    = Elephant Films
| released       =  
| runtime        = 98 minutes  
| country        = United States
| language       = English
| budget         = $35,000
| gross          = 
}}
D4 (or D4 Mortal Unit ) is a 2010 horror film, directed and written by Darrin Dickerson. It stars Vicki Askew, Ella Bell and Eric Berner.
==Plot==
 
D4 follows a team of special ops mercenaries on a mission to rescue a kidnapped kid believed to be held in an abandoned government facility. Hired by the boys mother, a wealthy doctor with high reaching influence, all seems to be an easy job. But as things unfold, what was meant to be a simple search and rescue turns into a fight for survival.

==Cast==
* Vicki Askew as Dr. Lark
* Ella Bell as Nurse #1
* Eric Berner as Sloan
* Clay Brocker as Brocker
* Erin Elizabeth Burns as Nurse #2
* Darrin Dickerson as Smoke
* Montana Dickerson as Daltons Grandson
* Laine Dubroc as Little B
* Randall Edwards as Henchman
* Jeff Hime as Cutter
* Dan Kassis as Panel Member #2
* Ted LeGarde as Old Man Dalton
* Big Mike Ulm as Dr. Larks mutated son

==Release==
 

==Reception==
 
==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 