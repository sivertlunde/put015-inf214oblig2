Arthur (1981 film)
 
{{Infobox film
| name           = Arthur
| image          = arthurDVD.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster Steve Gordon
| producer       = Robert Greenhut Charles H. Joffe  (executive)
| writer         = Steve Gordon
| starring       = Dudley Moore Liza Minnelli John Gielgud
| music          = Burt Bacharach
| cinematography = Fred Schuler
| editing        = Susan E. Morse
| studio         = Orion Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $7 million
| gross          = $95,461,682  
}} Steve Gordon. The film stars Dudley Moore as the eponymous Arthur Bach, a drunken New York City millionaire who is on the brink of an arranged marriage to a wealthy heiress, but ends up falling for a common working-class girl from Queens. It was the first and only film directed by Gordon, who died in 1982 of a heart attack at age 44.
 fourth highest Peter Allen, Sir John Best Supporting Best Original Song.
 remake starring Russell Brand was released in April 2011.

==Plot==
Arthur Bach (Dudley Moore) is a spoiled alcoholic from New York City who likes to be driven in his chauffeured List of Rolls-Royce motor cars|Rolls-Royce through Central Park. Arthur is heir to a portion of his familys vast fortune, which he is told will only be his if he marries the upper class Susan Johnson (Jill Eikenberry), the daughter of a business acquaintance of his father. He does not love Susan, but his family feels she will make him finally grow up. During a shopping trip in Manhattan, accompanied by his valet Hobson (John Gielgud), Arthur witnesses a young woman, Linda Morolla (Liza Minnelli), shoplifting a necktie. He intercedes with the store security guard (Irving Metzman) on her behalf, and later asks her for a date. Despite his attraction to her, Arthur remains pressured by his family to marry Susan.
 Stephen Elliott), attempting to stab Arthur with a cheese knife, though he is prevented by Martha.

A wounded Arthur announces in the church that there will be no wedding and passes out. Linda attends to his wounds and they discuss living a life of poverty. A horrified Martha tells Arthur that he can have his fortune because no Bach has ever been working class. Arthur declines, but at the last minute, talks privately to Martha. When he returns to Lindas side, he tells her that he declined again – Marthas dinner invitation, he means - but he did accept $750 million. Arthurs pleased chauffeur Bitterman (Ted Ross) drives the couple through Central Park.

==Cast==
* Dudley Moore as Arthur Bach
* Liza Minnelli as Linda Marolla
* John Gielgud as Hobson
* Geraldine Fitzgerald as Martha Bach
* Jill Eikenberry as Susan Johnson Stephen Elliott as Burt Johnson
* Thomas Barbour as Stanford Bach
* Ted Ross as Bitterman
* Barney Martin as Ralph Marolla
* Paul Gleason as Executive
* Phyllis Somerville as Saleslady
* Lou Jacobi as Plant store owner
* Justine Johnston as Aunt Pearl
* Irving Metzman as store security guard
* Anne De Salvo as Gloria
* Lawrence Tierney as Man in Diner Demanding Roll
* Mark Margolis (uncredited) as Wedding guest

==Production== Steve Gordon had originally considered George Segal; however, after the success of 10 (film)|10, Segal was replaced with Dudley Moore, who had also stepped in when Segal withdrew from the lead role of 10.

Bud Cort reportedly accepted the role of Arthur at one point, but he dropped out before production began. Sylvester Stallone was briefly considered. 

==Reception== review aggregate website Rotten Tomatoes.    

==Remake==
  remade by Warner Bros. with the British actor/comedian Russell Brand in the lead role.  Brand confirmed this during his March 10, 2009 appearance on The Howard Stern Show. On April 22, 2010 it was announced that Helen Mirren would star opposite Brand, taking on John Gielguds part. On June 11, the Hollywood Reporter announced that Jennifer Garner and Nick Nolte had also joined the cast.  Garner plays the heiress while Nolte is her ruthless father. Greta Gerwig stars as a charismatic tour guide with whom Arthur falls in love. Jason Winer directed the remake and Peter Baynham penned the script. Filming began in July 2010. It was released in the United States|U.S. on April 8, 2011. The remake was a critical and financial failure. 

This film was also remade in India twice, as the Hindi language film Sharaabi starring Amitabh Bachchan in the lead role and in Kannada as Nee Thanda Kanike.

==Awards and nominations==
===Academy Awards===
;Wins Best Actor in a Supporting Role – John Gielgud Best Original Peter Allen for "Arthurs Theme (Best That You Can Do)" which was performed by Cross

;Nominations Best Actor in a Leading Role – Dudley Moore Best Writing, Screenplay Written Directly for the Screen – Steve Gordon

===Honors=== 100 Years... 100 Laughs.

American Film Institute lists
*AFIs 100 Years... 100 Songs:
**"Arthurs Theme (Best That You Can Do)" - #79

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 