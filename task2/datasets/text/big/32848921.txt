Phantom Gold
 
{{Infobox film
| name           = Phantom Gold
| image          = 
| image_size     = 
| caption        = 
| director       = Rupert Kathner
| producer       = Rupert Kathner Stan Tolhurst (associate)
| writer         = Rupert Kathner
| narrator       = Captain A.C. Stevens
| starring       = Stan Tolhurst
| music          = Rex Shaw
| cinematography = Rupert Kathner
| editing        = Cecil Blackman
| studio         = Kathner-Tolhurst Australian Productions
| released       = 23 June 1937 (Singleton) 
    24 September 1937 (Sydney) 
| runtime        = 64 minutes
| country        = Australia
| language       = English
}}
Phantom Gold is a 1936 adventure film about the search for Lasseters Reef. It was the first feature from director Rupert Kathner.

==Plot==
Harold Lasseter claims he knows the location of a gold reef and in 1930 manages to raise funds for an expedition to discover it. He discovers the reef but dies of thirst.

Pilots W.L. Pittendrigh and S.J. Hamre go looking for Lasseter but run out of fuel and are forced to land in the desert. They are there for three weeks before being rescued. Bob Buck discovers Lasseters body and buries him. The gold reef is never found.

==Cast== Harry Lasseter
*Bryce Russell as Paul Johns
*Captain W.L. Pittendrigh as himself
*Bob Buck as himself
*Old Warts as himself
*Reg King as S.J. Hamre
*Ben Nicke

==Production==
In April 1936 Sydney businessman H.V. Foy led an expedition to discover Lasseters Reef in central Australia.  He took two filmmakers with him, Rupert Kathner and Stan Tolhurst, to record the trip. Along the way they encountered several people who knew Lassester, including Bob Buck, the bushman who found his dead body, and Old Warts, an aboriginal who befriended him. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p178 

The expedition retraced Lasseters steeps, including visiting the cave where he died, but found no trace of gold. Foy, Kathner and Tolhurt decided to make a feature film based on the story of Lasseters expedition instead.  Tolhurst was cast in the role of the prospector and grew a beard..

Filming went on for around three months in central Australia.Another expedition was taking place at the same time to find the reef. They reported spears had been thrown at them by aboriginals in war paint.  When Kathner and Tolhurst arrived in Broken Hill in July they revealed these aboriginals had been painted up to appear in the film but they had believed them to be docile. 

Kathner and Tolhurst then returned to Sydney and shot additional sequences. Among these were re-enactments of Captain W.L. Pittendrighs real life attempted rescue of Lasseter.

None of the film had synchronous dialogue apart from an opening interview with Foy. Sydney radio personality A.C. Stevens offered a narration.

==Release==
The film was rejected under the quality clause of the New South Wales Film Quota Act and struggled to find a distributor, forcing Kathner to arrange screenings himself.  Reviews praised the setting but found it dramatically lacking. 

The filmmakers were sued by publishers Angus and Robertson who claim the movie infringed the copyright on the 1931 book Lasseters Last Ride by Ion Idriess. The case was never heard in court but Foy barred any further screenings of the film. 

==References==
 

==External links==
*  in the Internet Movie Database
*  at National Film and Sound Archive
*  at Oz Movies
 

 
 
 
 