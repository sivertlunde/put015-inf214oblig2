Jappeloup
{{Infobox film
| name           = Jappeloup
| image          = 
| caption        =  Christian Duguay   
| producer       = Ludi Boeken, Pascal Judelewicz, Romain Le Grand, Frédérique Dumas, Geneviève Lemal, Chica Benadava, Walid Chammah, Florian Genetet-Morel, Joe Iacono, Lyse Lafontaine 
| writer         = Guillaume Canet   (based on the novel Crin Noir by Karine Devilder)    
| starring       = Guillaume Canet, Marina Hands, Daniel Auteuil 
| music          = Clinton Shorter 
| cinematography = Ronald Plante 
| editing        = Richard Marizy 
| distributor    = Pathé 
| released       =   
| runtime        = 130 minutes 
| country        = France
| language       = French
| budget         =
| gross          =
}}
 Christian Duguay. Most Promising Actress award at the 39th César Awards.   

==Synopsis==
In the early 1980s,   (1975-1991) from  .   He loses at the 1984 Summer Olympics in Los Angeles, California, but wins at the 1988 Summer Olympics in Seoul, South Korea.  

==Cast==
*Guillaume Canet as Pierre Durand, Jr.
*Marina Hands as Nadia
*Daniel Auteuil as Serge Durand
*Lou de Laâge as Raphaëlle Dalio Marcel Rozier
*Jacques Higelin as Dalio
*Marie Bunel as Arlette Durand
*Joël Dupuch as Francis Lebail
*Frédéric Épaud as Patrick Caron
*Arnaud Henriet as Frédéric Cottier
*Donald Sutherland as John Lester
*Antoine Cholet as Hubert Bourdy
*Edmond Jonquères dOriola as Philippe Rozier
*Benoît Petitjean as Éric Navet
*Sébastien Cazorla as Michel Robert
*Noah Huntley as Joe Fargis
*James Flynn as John Lesters son
*Xavier Alcan as Sponsor
*Sonia Ben Ammar as young Raphaele
*Ben Ammar Sonia as young Raphaele
*Jean Rochefort as himself

==Critical reception== COLCOA film festival in Los Angeles, California in 2013. 

For The Hollywood Reporter, the film offers "a rather classic mix of stunts and sentiment before galloping ahead to its stirring equine finale." 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 