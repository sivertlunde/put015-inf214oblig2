Bindiya Chamkegi
{{Infobox Film
| name           = Bindiya Chamkegi 
| image          = 
| image_size     = 
| caption        = 
| director       = Tarun Dutt
| producer       = Guru Dutt Movies Pvt. Ltd.
| writer         = Gulshan Nanda
| narrator       =  Johnny Walker
| music          = R. D. Burman
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1984
| runtime        =  India
| language       = Hindi
| budget         = 
}} Indian film.

==Plot==
Shalini Shalu lives a wealthy lifestyle with her businessman brother, Shyam Kapoor, and is expected to marry Surajbhan, the son of wealthy Thakursaheb. She would prefer that Shyam got married first so that he can have someone to look after him, but as he was in love with a woman named Maya, who betrayed him, and since then he has taken to alcohol in a big way, and hates women. When he fails to convince Shalu to marry, he gets married to a woman named Bindiya and brings her home with him. A thrilled Shalu gives her consent to get married, and also announces that she would like to give a formal reception for her brothers marriage.

The reception does take place, but Thakursaheb subsequently informs Shyam that Surajbhan has already got married abroad to a woman of his choice. Shyam asks his friend, Ranjeet, and both arrange for Shalus marriage to Daulatrams son, however, Bindiya does not approve of him, and as a result Shalu rejects him. What Shalu does not know is that Shyam is not really married, but has hired Bindiya to act as his wife; and what Shyam does not know is that Ranjeet is not who he really claims to be, but someone else who has got a number of scores to settle with Shyam.

== External links ==
*  

 
 
 


 