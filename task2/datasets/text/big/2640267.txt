Vengeance Is Mine (1979 film)
{{Infobox film
| name = Fukushū suruwa wareniari
| image = Vengeance_is_Mine_1979.jpg
| caption = Movie Poster
| director = Shōhei Imamura 
| producer = Kazuo Inoue
| writer = Masaru Baba, Ryūzō Saki (novel)
| starring = Ken Ogata, Mayumi Ogawa, Rentarō Mikuni, Mitsuko Baisho 
| music = Shin’ichirō Ikebe 
| cinematography = Sinsaku Himeda 
| editing = Keiichi Uraoka 
| distributor = Shochiku 
| released = 1979
| runtime = 140 min.
| country = Japan
| language = Japanese
| budget = 
| preceded_by = 
| followed_by = 
}}

Vengeance Is Mine (Japanese title: 復讐するは我にあり, fukushū suru wa ware ni ari) is a 1979 film directed by Shōhei Imamura, based on the book of the same name by Ryūzō Saki. It depicts the true story of serial killer Akira Nishiguchi (Iwao Enokizu in the film).

It stars Ken Ogata as Enokizu, with Mayumi Ogawa, Rentarō Mikuni, Mitsuko Baisho, Nijiko Kiyokawa and Chocho Miyako. The film won the 1979 Best Picture Award at the Japanese Academy Awards, and won Best Screenplay at the Yokohama Film Festival, where Ken Ogata also won Best Actor. 

== Plot ==
The films story is told as a series of flashbacks. In the opening scenes, Iwao Enokizu (Ken Ogata), is a prisoner of the police. A huge crowd of journalists and an angry mob greet him as he enters a jail. The police quiz him but he refuses to answer. The story goes back to the initial murders. Enokizu tricks and then kills two men and steals a large sum of money. He puts on a suit and disappears. Enokizu travels to another city. At the train station, he asks a taxi driver to take him to an inn where he can get a prostitute. Enokizu is sexually insatiable. He tells the innkeeper, a woman called Haru (Mayumi Ogawa), that he is a professor at Kyoto University. The police, searching for Enokizu, put out bulletins with his face on television. The prostitute thinks the professor is Enokizu, but she is told not to go to the police because of her job.
 Catholic father (Rentaro Mikuni) who lost his fishing boats to the Japanese navy in the 1930s. Enokizu is a rebellious, violent child. As a young man, he is convicted and imprisoned. His mother is ill. His wife (Mitsuko Baisho) is attracted to his father. She divorces Enokizu, but then is persuaded by Enokizus father to remarry him, due to the fathers Catholic beliefs. After the remarriage, she tries to seduce the father, but fails. Enokizu accuses her of sleeping with his father when he was in prison.

Enokizu travels to Tokyo. He tricks the mother of a young defendant into giving him the bail money for her son. Escaping, he befriends a lawyer who lives alone. He kills the lawyer and uses his apartment. He sends some money to Haru, and travels back to her place. Harus mother is a convicted murderer who has recently been released from prison. Haru had lost her job, and had to run the seedy inn because she could not find other work. Haru and her mother realise that "the Professor" is the wanted man. Enokizu kills both Haru and her mother and pawns their goods. The prostitute (Toshie Negishi) sees the pawnbroker going to the inn and decides to go to the police.

Five years later, Enokizu has been executed. His father and wife go to the top of a mountain to scatter his ashes. They throw the bones into the air but the bones remain hanging in the air. The film ends.

==Cast==

* Ken Ogata as Isao Enokizu
* Rentarō Mikuni as Shizuo Enokizu, Isaos father
* Mitsuko Baisho as Kazuko Enokizu, Isaos wife
* Chocho Miyako as Kayo Enokizu, Isaos mother
* Mayumi Ogawa as Haru Asano
* Nijiko Kiyokawa as Hisano Asano, Harus mother
* Taiji Tonoyama as Tanejiro Shibata, the first victim
* Goro Tarumi as Daihachi Baba, the second victim
* Moeko Ezawa as Chiyoko Hata
* Kazuko Shirakawa as Sachiko Yoshizato
* Frankie Sakai as Detective Kawai
* Torahiko Hamada as Chief Detective Yoshino
* Yasuhisa Sonoda as Detective Kuwata
* Akira Hamada as Detective Ichikawa
* Kazunaga Tsuji as Detective Kuchiishi
* Yoshi Katō as the lawyer Kawashima, the third victim
* Toshie Negishi as the prostitute ("stick girl")

==Reception==

Jasper Sharp says "Both seducing and repelling with its unusual story and grisly humour, Imamura uncovers a seedy underbelly of civilised Japanese society."  Roger Ebert says "This portrait of a cold-blooded serial killer suggests a cruel force without motivation, inspiration, grievance." 

==References==
 

==Sources==
*  

== External links ==
*  
* 
*  

 
{{Navboxes title = Awards list =
 
 
 
}}
 
 
 
 
 
 
 
 
 
 