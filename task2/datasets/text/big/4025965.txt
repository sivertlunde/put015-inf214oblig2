Chronique d'un été
 
Chronique dun été (Chronicle of a Summer) is a documentary film made during the summer of 1960 by sociologist Edgar Morin and anthropologist and filmmaker Jean Rouch, with the technical and aesthetic collaboration of Québécois director-cameraman Michel Brault. The film begins with a discussion between Rouch and Morin on whether or not it is possible to act sincerely in front of a camera. A cast of real-life individuals are then introduced and are led by the filmmakers to discuss topics on the themes of French society and happiness in the working class. At the end of the movie, the filmmakers show their subjects the compiled footage and have the subjects discuss the level of reality that they thought the movie obtained.
 Nagra III, a transistorized tape recorder with electronic speed control, developed by Stefan Kudelski. Chronicle of a Summer is a landmark in direct cinema history.

It is also widely regarded as an experimental and structurally innovative film and an example of cinéma vérité. The term "cinema verite" was suggested by the films publicist and coined by Rouch, highlighting a connection between film and its context, a fact Brault confirmed in an interview after a screening of Chronique dun ete at the TIFF Bell Lightbox in Toronto in 2011.

In a 2014 Sight and Sound poll, film critics voted Chronicle of a Summer the sixth best documentary film of all time.   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 