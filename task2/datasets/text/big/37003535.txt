The Patience Stone (film)
{{Infobox film
| name = The Patience Stone
| image = The Patience Stone (film).jpg
| alt =
| caption =
| director = Atiq Rahimi
| producer =
| writer = Atiq Rahimi Jean-Claude Carrière
| music = Max Richter
| starring = Golshifteh Farahani Hamid Djavadan Massi Mrowat Hassina Burgan
| music =
| cinematography = Thierry Arbogast
| editing = Hervé de Luze
| studio =
| distributor =
| released =  
| runtime =
| country = Afghanistan, France
| language = Persian
| budget =
| gross =
}} novel of the same title. Written by Jean-Claude Carrière and the director, the film stars Golshifteh Farahani, Hamid Djavadan, Massi Mrowat, and Hassina Burgan.
 Best Foreign Most Promising Actress award at the 39th César Awards.   

==Critical reception==
The Patience Stone has a fresh rating of 86% on Rotten Tomatoes. 

==Cast==
* Golshifteh Farahani as The woman
* Hamid Djavadan as The man (as Hamidreza Javdan)
* Hassina Burgan as The aunt
* Massi Mrowat as The young soldier
* Mohamed Al Maghraoui as The mullah (as Mohamed Maghraoui)
* Malak Djaham Khazal as The neighbor

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Afghan submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*   at Sony Pictures Classics
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 