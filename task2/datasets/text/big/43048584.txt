Mugaraasi
{{Infobox film
| name           = Mugaraasi
| image          = Mugaraasi .jpeg
| caption        = 
| director       = M. A. Thirumugam
| producer       = Sandow M. M. A. Chinnappa Thevar
| writer         = R. K. Shanmugam
| story          = G. Balasubramaniam Jayanthi M. N. Nambiar Nagesh
| music          = K. V. Mahadevan
| cinematography = N. S. Varma
| editing        = M. A. Thirumugam M. G. Balu Rao
| studio         = Devar Films
| distributor    = Devar Films
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}} 1966 Tamil language drama film directed by M. A. Thirumugam. The film features M. G. Ramachandran and Gemini Ganesan in lead roles. The film, produced by Sandow M. M. A. Chinnappa Thevar under Devar Films, had musical score by K. V. Mahadevan.    The film ran for 100 days. 

==Cast==
* M. G. Ramachandran
* Gemini Ganesan
* Jayalalitha Jayanthi
* M. N. Nambiar
* Nagesh
* S. A. Ashokan
* V. K. Ramasamy (actor)|V. K. Ramasamy
* Sriram
* Sandow M. M. A. Chinnappa Thevar Manorama
* P. K. Saraswathi
* Yasodha

==Soundtrack==
The music composed by K. V. Mahadevan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:44
|-
| 2 || Enna Enna || T. M. Soundararajan, P. Susheela || 03:16
|-
| 3 || Mugathai Kaatti || T. M. Soundararajan, P. Susheela || 03:05
|-
| 4 || Thanner || P. Susheela || 02:59
|-
| 5 || Undaakki Vittavargal || T. M. Soundararajan || 04:19
|}

==References==
 

==External links==
 

 
 
 
 
 


 