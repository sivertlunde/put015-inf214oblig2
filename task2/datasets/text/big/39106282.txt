London Bridge (film)
{{Infobox film
| name             = London Bridge
| image            = London Bridge film poster.jpg
| caption          = A bridge can join hearts too.
| director         = Anil C. Menon
| producer         = Satish B Satish Antony Binoy
| writer           = Jinu Abraham
| starring         =  
| music            = Songs:    (Opening Theme music by Rahul Raj) 
| cinematography   = Jithu Damodar
| editing          = Praveen Prabhakar
| studio           = Ordinary films
| distributor      = Central Pictures
| released         =  
| runtime          =
| country          = India
| language         = Malayalam
| budget           = 
}} Carnatic vocalist Dr. Sreevalsan Menon was hired to compose two additional songs and Gopi Sundar was signed in to compose the background score. The film is produced by Antony Binoy and Sathish B under the banner Ordinary Pictures. 

The film was dubbed and released in Telugu as Love in London.
==Plot==
Vijay is a business man who always listens to his brain, on the other hand Pavithra, the daughter of a famous businessman CS Nambiar, is a girl who always listens to her heart. Vijay was asked to marry Pavitra by her dad and Pavitra is testing whether Vijay is the right guy for her. After spending a day with Pavitra, Vijay hits Merin, a nurse who has just arrived from India to work in London, while driving back home. Her wrist gets paralysed after the accident. Vijay takes care of her and eventually falls in love with Merin and vice versa. Pavitras dad and Francis fix Vijays and Pavitras marriage. But when he refuses to marry her, Francis forces him to get married to Pavitra because of her wealth. Merins job agreement gets terminated because they think that she is physically unfit. Francis tries to help her but he fails.Eventually, Francis forces Vijay to not give her false hopes and gift her a sum of money and let her go back to India. Merin refuses the amount of money gifted and asks him to show her around London before she leaves. Then Merin goes back to India. The next day, Gracy aunty calls Vijay to tell that Merin hasnt reached home yet. Gracy Auntys husband tells them that he will go to India to enquire about Merin but Vijay says that he will go because he is the one who is supposed to go. Pavitras dad try to force him to stay, but Vijay says that he really likes Merin and he will go to India, no matter what. Pavitra helps him to go to India by convincing her dad to not stop him and personally drops him to the airport. Vijay reaches India. While in a taxi, Francis calls Vijay and directs him to go to Merins home. Vijay reaches Merins home to find her house being seized to debt. He finds out Merin reached home safe. Pavitra calls Vijay and reveals that the story of Merin not reach her home was her plan to make Vijay understand his love to Merin.

==Cast==

* Prithviraj Sukumaran as Vijay Kannedathu Das
* Andrea Jeremiah as Pavithra
* Nanditha Raj as Merin Elsa John
* Pratap Pothen as C. S. Nambiar Mukesh as Francis
* Sunil Sukhada as Thampikuttichayan
* Lena Abhilash as Gracy
* Amritha Anil  as Maalu (Merins sister)
* Prem Prakash as John Daniel
* Devi Krupa as Swathi

==Production==
London Bridge was started in August 2013 in Central London. The film almost covers all parts of London in its shots.The film was earlier planned to release during Onam Festival but since it was not completed yet, the release was postponed to February 1, 2014.

==Music==

{{Infobox album 
| Name = London Bridge (film)
| Longtype = London Bridge (film)
| Type = Soundtrack
| Artist =Rahul Raj
| Cover =
| Border = yes
| Alt =
| Caption = Official theatrical poster
| Released = 2014
| Recorded = 
| Genre = Film soundtrack
| Length = 
| Language = Malayalam
| Label = Satyam Audios
| Producer = Satish B Satish, Antony Binoy D Company (2013)
| This album = London Bridge (2014)
| Next album = Mannar Mathai Speaking 2(2014)
}}
 Carnatic vocalist Dr. Sreevalsan Menon was hired to compose two additional songs. Rahul Raj was set to return to the project to compose the background score of the movie; but scheduling conflicts with his Telugu film Paathshala (Telugu film)|Paathshala forced him to opt out completely. However the main theme music composed by Rahul Raj was retained and used as the opening titles theme.

{| class="wikitable"
|-
! Track Name !! Singers !!  Description
|-
| "London Bridge Theme" || Gitamba (Netherlands)  || Composed by Rahul Raj
|- Haricharan  || Composed by Rahul Raj
|- Yazin Nizar || Composed by Rahul Raj
|- Shaan || Sreevalsan Menon
|- Sreevalsan Menon
|}

==Release==
The film was released on 1 February 2014 at 72 theaters across Kerala and the distribution was done by Central Pictures.

==Reception==
;Critical Response
London Bridge has received mixed reviews from critics,    Entertainment website oneindia has said "London Bridge as a watchable romantic drama" and given a rating of 3/5 stars.  Lensmenreviews website has given the movie a rating of 2/5 stars and said that "Luxury and some scenery of London and its outskirts is rich in almost all frames of London Bridge. But the very necessary thing in a romantic drama is missing here; romance.".  The Malayalam website Muyal Media has rated 3.2/5 stars for London Bridge and mentioned that "London Bridge, a triangular love story happening in London has got a kind of freshness in its visuals but lacks the same in the narrative."  The Telugu version was released on Octobar 31. The film got highly positive reports from the Telugu audience.

==References==
{{reflist|refs=
   OneIndia. February 1, 2014. Retrieved February 1, 2014. A Watchable Romantic Drama
 
  MuyalMedia.February 1, 2014.A breezy romantic tale 
  Lensmenreviews.February 1, 2014. 
}}

==External links==
* 

 
 
 