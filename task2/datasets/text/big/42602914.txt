Speechless (2012 film)
{{Infobox film name           = Speechless image          =  caption        =  producer       = Simon Chung director       = Simon Chung writer         = Lu Yulai Simon Chung starring       = Pierre-Matthieu Vital   Qilun Gao   Yung Yung Yu   Jian Jiang   Yu Ting Si Tu   Shu Ling Lang   Shao Qiu Shen   Hua Li music          = Sebastian Seidel cinematography = Chan Chi Lap editing        = Roddy Law studio         = Heart Production Company distributor    = Panorama (Hong Kong) TLA Releasing (UK) PRO-FUN media Filmverleih (Germany) Breaking Glass Pictures (USA) released       =   runtime        = 92&nbsp;minutes country        = Hong Kong China language       = Mandarin   Cantonese budget         = 
}}
Speechless   ( ) is a 2012 Hong Kong — mainland China film about a mute Frenchman who is found naked by the banks of a river in Wuhan in mainland China. He is rescued and sent to a local hospital where he is treated by a male nurse who cares for him and later discovers the secrets of his past.

Speechless is the third feature film by director Simon Chung. His previous films were Innocent, released in 2005, and End of Love, in 2009. 

The film premiered at the BFI 26th London Lesbian and Gay Film Festival, on 28 March 2012,  . Publisher: Polari Magazine. Published: 29 March 2012. Retrieved: 27 April 2014.  and contains brief full-frontal male nudity. 

==Plot==
A man is found naked by the side of a river in Wuhan in mainland China. His name is Luke, but he appears to be unable to speak, and is sent to a local hospital for diagnosis. Unable to determine the cause of Lukes condition, the hospital decides that he must be sent elsewhere. Soon, Lukes male nurse, Xiao Jiang, discovers that his superiors have decided to transfer Luke to a mental asylum. He then decides that he must rescue him and smuggle him out to his uncles village, where they will have to evade capture from the local police.

Slowly, the reason for Lukes speechlessness becomes clear. Xiao Jiang discovers that Luke had previously had a romantic affair with a young male university student, called Han Dong, whom he had met at the university campus, together with Han Dongs girlfriend, Xiao Ning. Xiao Jiang accidentally discovers a secret that provides a key to Lukes past, enabling him to subsequently track down Xiao Ning to find out more. Although she proves to be unhelpful and deceitful, he begins to be able to piece together the mystery of Lukes inability to speak. He learns that Lukes romantic relationship with Han Dong had been discovered by Xiao Ning, who is a devout Christian, and had decided to punish her boyfriend by outing him and publicly humiliating him at church. During Xiao Jiangs unravelling of this mystery, Luke suddenly disappears from the accommodation he had secretly shared with Xiao Jiang. As a result, Xiao Jiang asks for the help of a female friend, Lan, to try to locate Luke, and help resolve once and for all the central mystery of Lukes inability to speak.    . Publisher: EntertainmentFocus.com Published: 29 November 2012. Retrieved: 27 April 2014.  

==Production==
Speechless was filmed in secret in mainland China: in the small city of Shantou, on the East coast of Guangdong Province, and in the northern countryside of Guangdong, in Southern China. The university campus scene, introducing one of the films most important characters, Han Dong, and his girlfriend, Xiao Ning, was filmed at Shantou University.

The man playing the role of Han Dong is mainland China model, actor and magician Jian Jiang, who is from Guangzhou, while the woman playing his girlfriend, Xiao Ning, is Indonesian-born and Australian-educated actress Yung Yung Yu. Both of the main actors, Pierre-Matthieu Vital and Qilun Gao, are indirect friends of the films director, Simon Chung. Vital, who plays Luke, is from France, but lives and works in Guangzhou and regularly visits Hong Kong, where he first met Simon Chung. Vital is not a fluent speaker of Mandarin Chinese, and so Chung had to help train him to speak his lines.  . Publisher: San Diego Gay and Lesbian News.com. Published: 28 May 2012. Retrieved: 27 April 2014.  The actor playing Vitals attentive nurse, Xiao Jiang, is Beijing actor Qilun Gao, who had previously appeared in another Hong Kong film. 

As filming in China was unofficial, the film was unable to secure a theatrical release in its country of production,  although it has been screened once in China, to students at Shantou University, and released in cinemas across Hong Kong. Chung says he wanted his film to explore both a Western perspective of China as well as a Chinese perspective of the West. 

The inspiration for the film came from a real-life case known as the “Piano Man,” in which a man  washed up off the eastern coast of England several years ago. 

==Cast==
*Pierre-Matthieu Vital ... Luke
*Qilun Gao ... Xiao Jiang
*Jian Jiang ... Han Dong
*Yung Yung Yu ... Xiao Ning
*Yu Ting Si Tu ... Lan
*Shu Ling Lang ... Xiao Jiangs mother
*Shao Qiu Shen ... Dr. Lin
*Hua Li ... Churchs caretaker

==See also==
*Soundless Wind Chime, a 2009 gay-themed film with a similar atmosphere, and which also features, as its main theme, an affair between two young men, one Chinese and the other, Western. Unlike Speechless, the films dialogue is mostly in the English language.
*List of Chinese films of 2012
*List of lesbian, gay, bisexual or transgender-related films
*List of lesbian, gay, bisexual, or transgender-related films by storyline Nudity in film (East Asian cinema since 1929)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 