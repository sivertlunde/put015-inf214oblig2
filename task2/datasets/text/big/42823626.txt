Fist of Unicorn
 
 
{{Infobox film
| name = Fist of Unicorn
| director = Ti Tang
| starring = Unicorn Chan   Yasuaki Kurata   Chui Meng   Tina Chin Fei
| distributor = Pacific grove corporation
| language = Cantonese
| runtime = 90 minutes
| country = Hong Kong
}}

Fist of Unicorn (Also known as The Unicorn Palm or Bruce Lee and I.) is 1973 Hong Kong martial art movie, starring Unicorn Chan.

==Plot==

Once upon a time, the rover whose name is Ah-Lung became friend with the young man called Tiger and Lung was invited by Tiger to join his family. When he arrived to Tiger`s family`s handyman he enjoyed his new life, however Tiger offended Wong who is short tempered son of a notorious gangster, Lung must protect himself from the gangsters. Later Lung and Tiger forced to join the young women who is pursued by Wong after Wong`s gang killed her family at the acrobatic troupe. Ah-Lung shares his martial art knowledge with his new friends he met in order to defeat Wong and his thugs.

==Cast==

*Unicorn Chan as Ah-Lung
*Mang Hoi as Bald Kid, Tiger
*Meng Chui as Stuttering Wong`s dream
*Kurata Yasukai as Sun

=== other casts ===

*Siu Hung Cham
*Wai-man Chan
*Chun Chao
*Mars as stuttering boy
*Ji Han Jae as Chi Han Kuang 
*Hwang In-Shik as Wong`s family thug
*Ching Chen
*Hsi Ting Cheng

=== guest star ===

*Alexander Grand as Rensky
*Hoi Mang as Hsiao-Hu
*Te Tung Ouyang

=== Cameos ===

*Jackie Chan extra (Uncredited)
*Bruce Lee (Unintended cameo), Bruce Lee only make brief appearance at the behind-the-scene where he rehearsing the actors and stuntman.

 
 
 
 


 