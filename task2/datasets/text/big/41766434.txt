Big Font. Large Spacing
{{Infobox film
| name           = Big Font. Large Spacing
| image          = BigFontLargeSpacingDVD.jpeg
| caption        = UK DVD Cover
| director       = Paul Howard Allen
| producer       = Vivien Müller-Rommel
| screenplay     = Paul Howard Allen
| starring       = {{Plainlist |
* Jamie Kristian 
* Amy Morgan 
* Gareth Aldon 
* Kimberley Wintle
}}
| cinematography = Nicolas Booth
| editing        = {{Plainlist |
* Matt Freeth
}}
| distributor    = {{Plainlist |
* 2D Cinema
}}
| released       = 2010
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
}}
Big Font. Large Spacing is a 2010 comedy set in Cardiff, and directed by Paul Howard Allen. It tells the story of a night in the life of two psychology students who find out that the terms major essay is due in the next morning. The film was toured around British Universities on a touring cinema over three years during the freshers week period. 

==Cast==
* Jamie Kristian as Tom
* Amy Morgan as Sarah
* Gareth Aldon as Steve
* Kimberley Wintle as Debbie

==Production==
The film was produced by 33Story Productions and Boomerang, both Cardiff based TV/Film production companies. Principal photography took place over three weeks in December 2008.

==Release==
The film premiered at the 2010 Atlanta Film Festival in April 2010.  It was then taken around Universities during the September / October freshers week period by the film makers themselves over a three year period.  

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 