The Lovers (1994 film)
 
 
{{Infobox film
| name = The Lovers
| image = The Lovers (1994 film).jpg
| caption = DVD cover art
| film name = {{Film name| traditional = 梁祝
| simplified = 梁祝
| pinyin = Liángzhù
| jyutping = Loeng4 Zuk1}}
| director = Tsui Hark
| producer = Tsui Hark
| writer = Sharon Hui Tsui Hark James Wong Raymond Wong William Hu
| cinematography = David Chung
| editing = Marco Mak Wong Jing-cheung
| starring = Nicky Wu Charlie Yeung Elvis Tsui Carrie Ng
| studio = Film Workshop Paragon Films Ltd. Golden Harvest
| released =  
| country = Hong Kong
| language = Cantonese Mandarin
| budget =
}}
The Lovers is a 1994 Hong Kong romantic film based on the Chinese legend of the Butterfly Lovers. It was directed and produced by Tsui Hark, and starred Nicky Wu, Charlie Yeung, Elvis Tsui and Carrie Ng. The theme songs were performed by Nicky Wu.

==Cast==
* Nicky Wu as Leung San-pak
* Charlie Yeung as Chuk Ying-toi
* Elvis Tsui as Master Chuk
* Carrie Ng as Sin Yuk-ting
* Lau Shun as Chung Kwai
* Sun Xing as Monk
* Linda Lau as Madam Yuen
* Hau Bing-ying as Ingenue
* Yuen Sam as Mr Ching
* Shum Hoi-yung as Madam Leung
* Peter Ho as Ting Mong-chun
* Franco Jiang
* Cheng Tung-chuen
* Goon Goon
* Woo Wai-ling
* Lam Ching-man

==Awards and nominations==
* 14th Hong Kong Film Awards James Wong
** Nominations:
*** Best Director: Tsui Hark
*** Best Supporting Actress: Carrie Ng
*** Best Art Direction: William Chang
*** Best Costume Make Up Design: William Chang

==External links==
*  

 

 
 
 
 
 
 
 


 
 