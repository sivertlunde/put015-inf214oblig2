Komori Seikatsu Kojo Club
 
Komori Seikatsu Koujou Club (小森 生活 向上 クラブ) is a 2008 Japanese film, based on the 2004 novel Komori Kachou no Yuuga na Hibi, written by Hikaru Murozumi. The movie stars Arata Furuta, Chiaki Kuriyama, Kosuke Toyohara, Shiro Sano, Narimi Arimori, and Shugo Oshinari.

==Plot==
Komori is an ordinary, middle-aged business executive who is fed up with people walking all over him. Neither his family nor his coworkers have any respect for him. One fateful day while riding the train, a woman falsely accuses him of groping her, causing Komori to have his first violent outburst. From that moment, he decides to wield his own brand of justice and becomes determined to free his community of crime. Soon afterwards, people begin to idolize him and his work. 

==Cast==
* Arata Furuta as Komori
* Chiaki Kuriyama

==References==
 

 
 
 

 