Scandalous (film)
{{Infobox film
| name = Scandalous
| image =Scandalous (film).jpg|
 producer = Carter DeHaven Martin C. Schute Arlene Sellers Alex Winitsky  |
 director = Rob Cohen |
 writer = Larry Cohen Rob Cohen John Byrum |
| starring = {{Plainlist|
* Robert Hays
* John Gielgud
* Pamela Stephenson
* Jim Dale
* M. Emmet Walsh
}}
| distributor =  |
 released = 24 January 1984 |
 cinematographer = Jack Cardiff |
 editor = Michael Bradsell |
 music = Dave Grusin |
 runtime = 92 minutes |
 country = United States UK |
 language = English |
 budget = |
}} American comedy film directed by Rob Cohen and starring Robert Hays, John Gielgud and Pamela Stephenson. 

==Partial cast==
* Robert Hays - Frank Swedlin
* John Gielgud - Uncle Willie
* Pamela Stephenson - Fiona Maxwell Sayle
* Nancy Wood - Lindsay Manning
* Preston Lockwood - Leslie
* Conover Kennard - Francine Swedlin
* Jim Dale - Inspector Anthony Crisp

==Filming Locations==
*Polesden Lacey, England, UK
*Great Bookham, England, UK
*Dorking, England, UK
*Surrey, England, UK
*Twickenham Film Studios, St Margarets, Twickenham, England, UK (studio)
* Rainbow Theatre, Finsbury Park, London

==References==
 
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 