The Clue of the New Pin (1929 film)
{{Infobox film
| name           = The Clue of the New Pin 
| image          =
| caption        =
| director       = Arthur Maude   
| producer       = S.W. Smith
| writer         = Edgar Wallace (novel)   Kathleen Hayden
| starring       = Benita Hume   Kim Peacock   Donald Calthrop   John Gielgud
| music          =
| cinematography =  Horace Wheddon   
| editing        = 
| studio         = British Lion Film Corporation
| distributor    = Producers Distributing Corporation 
| released       = March 1929
| runtime        = 7,292 feet 
| country        = United Kingdom 
| awards         =
| language       = English 
| budget         = 
}}
  1929 Cinema British crime film directed by Arthur Maude and starring Benita Hume, Kim Peacock, and Donald Calthrop. It was made at Beaconsfield Studios.
 The Crimson DeForest Phonofilm sound-on-film system, were trade-shown to cinema exhibitors.  
 The Clue remade in 1961.

==Plot==
A wealthy recluse is murdered in an absolutely sealed room.

==Cast==
*  Benita Hume - Ursula Ardfern 
* Kim Peacock - Tab Holland 
* Donald Calthrop - Yeh Ling 
* John Gielgud - Rex Trasmere 
* H. Saxon-Snell - Walters 
* Johnny Butt - Wellington Briggs  Colin Kenny - Inspector Carver

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 

 