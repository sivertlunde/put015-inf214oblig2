Moonlight Serenade (1997 film)
{{Infobox film
| name           = Moonlight Serenade
| image          = 
| caption        = 
| director       = Masahiro Shinoda
| producer       = Masaru Koibuchi Taketo Niitsu
| writer         = Yū Aku Katsuo Naruse
| starring       = Kyōzō Nagatsuka
| music          =  Tatsuo Suzuki
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

  is a 1997 Japanese drama film directed by Masahiro Shinoda. It was entered into the 47th Berlin International Film Festival.   

==Cast==
* Kyōzō Nagatsuka as Koichi / Elder Keita
* Hideyuki Kasahara as Onda Keita, younger
* Jun Toba as Koji
* Shima Iwashita as Fuji
* Hinano Yoshikawa as Yukiko
* Michiko Hada as Komachi
* Junji Takada as Black Marketeer
* Toshiya Nagasawa
* Sayuri Kawauchi as Onda Hideko
* Shōhei Hino
* Chōichirō Kawarazaki
* Akaji Maro
* Takashi Tsumura as Interpreter

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 