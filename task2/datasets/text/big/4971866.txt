Wolves of Wall Street
 
{{Infobox film
| name           = Wolves of Wall Street
| image          = Wolves of Wall Street DVD.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 2006 DVD cover
| director       = David DeCoteau
| producer       = Sylvia Hess (producer) Roberta Friedman Jeffrey Schenck (co-producers) Eddy Collyns (line producer) Paul Colichman Andreas Hess Stephen P. Jarchow (executive producers)
| writer         = Barry L. Levy
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Jeff Branson Louise Lasser William Gregory Lee Angela Pietropinto John Michaelson Mary Elaine Monti Eric Roberts
| music          = Harry Manfredini
| cinematography = 
| editing        = 
| studio         = 
| distributor    = DEJ Productions Regent Worldwide Sales LLC (worldwide)
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} The Wolf of Wall Street directed by Rowland V. Lee and  premièring in 1929.    

==Plot==
On the advice of a bartender familiar with the Wall Street crowd, Jeff (William Gregory Lee) applies to the Wolfe Brothers brokerage firm for his dream job as a stock broker. What he does not know is that the brokers are werewolves, and he is bitten, thus he joins the pack. He is forced to abandon his love and values for cunning and instinct during which time he cheats on his girlfriend and brutally kills and eats various humans. After a change of heart, he finds that leaving the brotherhood is harder than joining. He goes to his girlfriends friends birthday party, and a drunk tries hitting on his girlfriend, and he rips a piece out of the neck of the drunk and then chases his girlfriend back to her apartment and bites her, transforming her into a werewolf too. She had given him a silver pen when he first started as an intern at the Wolfe Brothers firm, and he goes back to the headquarters and he tries to quit, but his mentor refuses to let him go. He leaves anyway, but they go to his girlfriend and seize her. They force him to return to headquarters, and he stabs the person he thought was the Alpha Male but he was wrong, and a fight ensues during which him and his girlfriend kill all of the werewolves, and then they start walking away. But the Alpha Male who they thought they had killed, opens his eyes a split second before the end of the movie, showing that they were not successful.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 