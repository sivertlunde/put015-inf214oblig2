Radio Days
 
{{Infobox film
| name = Radio Days
| image = RadioDaysPoster.jpg
| caption = Radio Days theatrical poster
| director = Woody Allen
| producer = Robert Greenhut
| writer = Woody Allen Michael Tucker Tony Roberts Jeff Daniels Seth Green
| music = Dick Hyman
| cinematography = Carlo Di Palma
| narrator = Woody Allen
| editing = Susan E. Morse
| distributor = Orion Pictures
| released =  
| runtime = 85 minutes
| country = United States
| language = English USD
| gross = $14,792,779
}}
Radio Days is a 1987 comedy film written and directed by Woody Allen. The film looks back on an American familys life during the Golden Age of Radio using both music and memories to tell the story.

==Plot==
Joe (Woody Allen), the narrator, explains how the radio influenced his childhood in the days before TV.  The young Joe (Seth Green) lives in New York City in the late 1930s and early 1940s.  The tale mixes Joes experiences with his remembrances and anecdotes, inserting his memories of the urban legends of radio stars, and is told in constantly changing plot points and vignettes.
 Rockaway Beach, each member at one point during the film finds in radio shows an escape from reality through the gossip of celebrities, sports legends of the day, game shows, and crooners, with the majority of the stories taking place in the glitz and glamour of Manhattan.  For Joe, the action adventure shows on the radio inspire him as he daydreams about buying a secret decoder ring, an attractive substitute teacher, movie stars (who may or may not be as honest as they appear), and World War II.

Meanwhile, several other parallel stories are told, from an aspiring radio star named Sally White (Mia Farrow), Joes Aunt Bea (Dianne Wiest) and her (mostly fruitless) search for love, and during the middle of the film on the radio the tragic story is told about a little girl named Polly Phelps, who falls into a well near Stroudsburg, Pennsylvania.  It becomes a big national story as the family listens to it.  Sadly, Polly does not survive.

==Cast==
* Woody Allen as Joe, the Narrator
* Seth Green as Young Joe
* Danny Aiello as Rocco
* Sydney Blake as Mrs. Gordon
* Leah Carrey as Grandma
* Jeff Daniels as Biff Baxter
* Larry David as Communist Neighbor
* Gina DeAngelis as Roccos mother
* Denise Dumont as Latin singer
* Mia Farrow as Sally White
* Todd Field as Crooner Kitty Carlisle Hart as Maxwell House (Coffee) Radio Jingle Singer
* Paul Herman as Burglar
* Julie Kavner as Mother
* Diane Keaton as New Years Singer
* Renée Lippin as Aunt Ceil
* Martin Sherman as Radio Actor
* William Magerman as Grandpa
* Judith Malina as Mrs. Waldbaum
* Kenneth Mars as  Rabbi Baumel
* Josh Mostel as Uncle Abe
* Don Pardo as "Guess That Tune" Host Tony Roberts as "Silver Dollar" Emcee
* Rebecca Schaeffer as Communists Daughter
* Wallace Shawn as Masked Avenger Mike Starr as Burglar Michael Tucker as Father
* Kenneth Welsh as Radio Voice
* Dianne Wiest as Aunt Bea

==Music==
The films soundtrack, which features songs from the 1930s and 40s, plays an integral and seamless part in the plot. An important part of one of the vignettes is inspired by  , as well as compact disc in 1987:

===Track listing===
{{Track listing
| extra_column    = Artist(s)
| writing_credits = no
| title1          = In the Mood
| extra1          = Glenn Miller
| length1         = 3:33
| title2          = I Double Dare You
| extra2          = Larry Clinton
| length2         = 2:49
| title3          = Opus No. 1
| extra3          = Tommy Dorsey
| length3         = 2:58
| title4          = Frenesi
| extra4          = Artie Shaw
| length4         = 3:01
| title5          = The Donkey Serenade Allan Jones
| length5         = 3:21
| title6          = Body and Soul
| extra6          = Benny Goodman
| length6         = 3:26
| title7          = You and I
| extra7          = Tommy Dorsey
| length7         = 2:44
| title8          = Remember Pearl Harbor
| extra8          = Sammy Kaye
| length8         = 2:29
| title9          = That Old Feeling
| extra9          = Guy Lombardo
| length9         = 2:45
| title10         = (Therell Be Bluebirds Over) The White Cliffs of Dover
| extra10         = Glenn Miller
| length10        = 2:54
| title11         = Goodbye
| extra11         = Benny Goodman
| length11        = 3:31
| title12         = Im Gettin Sentimental Over You
| extra12         = Tommy Dorsey
| length12        = 3:38
| title13         = Lullaby of Broadway
| extra13         = Richard Himber
| length13        = 2:29
| title14         = American Patrol
| extra14         = Glenn Miller
| length14        = 3:33
| title15         = Take the "A" Train
| extra15         = Duke Ellington
| length15        = 3:00
| title16         = One, Two, Three, Kick
| extra16         = Xavier Cugat
| length16        = 3:23
}}

==Release==
The film was screened out of competition at the 1987 Cannes Film Festival.   

===Home media=== Twilight Time
July 8, 2014. 

==Reception==

===Critical response===
It currently holds a fresh 88% positive rating on Rotten Tomatoes, with an average score of 7.9/10.   In his four-star review, noted critic Roger Ebert of the Chicago Sun-Times described Radio Days as Allen’s answer to Federico Fellini’s Amarcord and referred to it as "so ambitious and so audacious that it almost defies description. Its a kaleidoscope of dozens of characters, settings and scenes - the most elaborate production Allen has ever made - and its inexhaustible, spinning out one delight after another."   Vincent Canby of The New York Times referred to Allen as the "prodigal cinema resource" and spoke of the film saying, "Radio Days   is as free in form as it is generous of spirit." 

 , Dubin and Warren, big-band jazz, crooners, torch singers, Carmen Miranda. The music, perfectly matched to images of old wood and brick buildings and old glamour spots, produces a mood of distanced, bittersweet nostalgia. Radio Days becomes a gently satiric commemorations of forgotten lives." 

In a poll held by Empire (magazine)|Empire magazine of the 500 greatest films ever made, Radio Days was voted number 304. 

===Accolades===

====1987 Academy Awards (Oscars)====

*Nominated –   &nbsp;&nbsp;&nbsp;Set Decoration: Carol Joffe, Les Bloom, George DeTitta, Jr.   
*Nominated –   

====1987 BAFTA Film Awards====
*Won – Best Costume Design : Jeffery Kurland
*Won – Best Production Design: Santo Loquasto
*Nominated – Best Actress in a Supporting Role: Dianne Wiest
*Nominated – Best Editing: Susan E. Morse
*Nominated – Best Film: Robert Greenhut, Woody Allen
*Nominated – Best Screenplay Original: Woody Allen
*Nominated – Best Sound: Robert Hein, James Sabat, Lee Dichter

====1988 Writers Guild of America Awards====
*Nominated – WGA Screen Award for Best Screenplay Written Directly for the Screen: Woody Allen

==Further reading==
*Woody Allen On Location by Thierry de Navacelle (Morrow, 1987); a day-to-day account of the making of Radio Days

==References==
 

==External links==
*  
*  
*   at TVGuide.com
* VINCENT CANBY,  , NY Times, January 30, 1987

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 