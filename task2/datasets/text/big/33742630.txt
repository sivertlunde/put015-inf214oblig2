All Over Town
 {{Infobox film
| name           = All Over Town
| image          = File:All Over Town lobby card.jpg
| image_size     = 
| caption        = Lobby card
| director       = James W. Horne
| producer       = Leonard Fields (associate producer)
| writer         = Jerome Chodorov (screenplay) Richard English (story) James Parrott (comedy writer) Jack Townley (screenplay)
| narrator       = 
| starring       = See below
| music          = Alberto Colombo
| cinematography = Ernest Miller
| editing        = Howard ONeill
| studio         = 
| distributor    = 
| released       = 1937
| runtime        = 63 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

All Over Town is a 1937 American film directed by James W. Horne.

==Plot summary==
 

==Cast== Ole Olsen as Olsen
*Chic Johnson as Johnson
*Mary Howard as Joan Eldridge
*Harry Stockwell as Don Fletcher
*Franklin Pangborn as The Costumer James Finlayson as MacDougal
*Eddie Kane as William Bailey Stanley Fields as Slug
*DArcy Corrigan as Davenport
*Lew Kelly as Martin
*John Sheehan as McKee
*Earle Hodgins as Barker
*Gertrude Astor as Mamie
*Blanche Payson as Mother Wilson, Landlady
*Otto Hoffman as Peter Stuyvesant "Pete" Phillips
*Fred Kelsey as Inspector Murphy
*Charlie Becker as Bit Part (uncredited)
*Alan Ladd

==Soundtrack==
 

==External links==
* 
* 

 
 
 
 
 
 


 