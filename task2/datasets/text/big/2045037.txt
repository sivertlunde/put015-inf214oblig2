Time Changer
{{Infobox film
| name           = Time Changer
| image          = Time_changer.jpg
| caption        = Theatrical poster
| director       = Rich Christiano
| producer       = Rich Christiano 
| writer         = Rich Christiano
| starring       = D. David Morin Gavin MacLeod
| music          = Jasper Randall
| cinematography = Philip Hurn
| editing        = Jeffrey Lee Hollis
| distributor    = Five & Two Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States English
| budget         = US$855,000   
| gross          = $1,500,711 
}}
 time machine to send his colleague, Bible professor Russell Carlisle (D. David Morin), from 1890 into the early 21st century. The film had a limited nationwide release, and was made available on VHS, DVD and video-on-demand. 

==Plot==
Bible professor Russell Carlisle (D. David Morin) confronts and lectures a boy who has stolen marbles from his neighbors, calling his action unjust.  The year is 1890 and Carlisle has written a new manuscript entitled The Changing Times, which promotes good morals without discussing Christ. The book is on track to receive a unanimous endorsement from the board of the Grace Bible Seminary. That is, until colleague Dr. Norris Anderson (Gavin MacLeod) objects. Without unanimous endorsement, his book might not do so well. Carlisle and another professor seek a unanimity rule change, but the dean insists that Carlisle discuss the disagreement with Anderson privately.

Dr. Anderson fears that Carlisles book could harm coming generations, arguing that teaching good moral values without mentioning Christ is wrong. Using a secret time machine, Anderson sends Carlisle over 100 years into the future, offering him a glimpse of where his beliefs will lead.

Arriving in the early 21st century, Carlisle is shocked to find that half of all marriages end in divorce (instead of 5% in 1890), teenagers talk openly about deceiving their parents, movies contain blasphemous language and people who go to church are so bored by the sermons that they need extra activities. He tries to convince a laundromat worker, Eddie Martinez (Paul Rodriguez), to go to church and read the Bible.  Two churchgoing men grow suspicious of Carlisle, who acts as if hes seeing everything for the first time. They confront him as he is about to be transported back to the past. As the sky grows thunderous, Carlisle seems delirious as he talks about how the second coming of Christ is drawing near. Carlisle vanishes. The men look at where he vanished and one of the men says with dread, "I think we just missed the Rapture."

Carlisle rematerializes in 1890 and excitedly tells Anderson he will revise his book. He gives the thieving boy his own set of marbles and explains that it is Jesus Christ who demands honesty. Anderson tries to learn when the world will come to an end, by trying to send a Bible to the future. The machine wont operate with a target date of 2100, so he tries with progressively earlier decades 2090, 2080 and 2070, which fail. As the film ends, he makes at least two more failed attempts, aiming earlier and earlier, suggesting that either humanity cannot know when the End comes, or that the End will come before the mid-21st century.

==Cast==
    
   
* D. David Morin as Russell Carlisle
* Gavin MacLeod as Norris Anderson
* Hal Linden as The Dean
* Jennifer ONeill as Michelle Bain
* Paul Rodriguez as Eddie Martinez
* Richard Riehle as Dr. Wiseman
* John Valdetero as Tom Sharp
* Dan Campbell as Rex
* Evan Ellingson as Roger
* Crystal Robbins as Mrs. Matthews
* Patty MacLeod as Norris Wife
* Paul Napier as Dr. Butler
* Charles Hutchins as Dr. Henry
   
* Callan White as Carlisles Wife
* Ross McKerras as Pawn Shop Guy
* Ruben Madera as Bellhop Mike Wilson as Sam Arthur Roberts as Man in Pew
* Rod Britt as Pastor
* Ron Sey as Hot Dog Vendor
* Emily Trapp  as Young Girl
* Brad Heller as Boutique Manager
* Alexander Polk as Security Guard
* Teresa Vidak as Waitress Paige Peterson as Cindy
   
* Alana Curry as Kelly
* Nan McNamara as Toms Wife
* Cassandra Byram as Rexs Wife Alfred Jackson as Student
* Michael Gier as Group Leader
* Chipper Lowell as Visitation Guy (as Chip Lowell)
* Linda Kerns as Lady Prospect
* William Bowerman as William
* Michelle Dunker as Secretary
* Kevin Downes as Greg
* Michael Dotson as Bar Patron
 

==Production==
Time Changer was Rich Christianos first feature-length film.   In August 2001 Christiano Film Group announced the films cast, and that shooting would begin on October 6, 2001 in Visalia, CA, for an August 2, 2002 release. 
In February 2002, the website stated that the film was being edited in Los Angeles. 
In March, the first rough cut was completed, work began on a second pass, and streaming video was made available. 
In a press release, the theatrical release date was listed as October 4, 2002. 
Editing wrapped in June, while music score, sound design, and visual effects work continued, and two scene sneak previews were linked on the website.  
On August 2, the trailer was released online.  
On August 6 the press release changed to show a theatrical release date of October 11.  
On October 4, 2002, the film was announced as "ready to go", with a theatrical poster available which showed the final release date of October 25,  as did the simultaneous press release. 

==Releases== Video On Demand service. 

The story was also released as the 140-page Time Changer (A Novel)  in 2001, co-authored by Christiano and Greg Mitchell. 

==Reception==
In the Charlotte Observer, Lawrence Toppman praised the acting work, but had questions about plot holes and how some of the films premises would be accepted by Christian viewers.     Toppman wrote, "technically, the film can stand with most releases", and gave it 2.5 stars out of four.   Variety (magazine)|Variety reviewer Scott Foundas described the film as "goofy fantasy hokum" with a message, one scene as "subpar", and some monologues as "distinctly uncinematic", but other scenes as "surprisingly enjoyable."  Foundas found the film "hard to read" - often having "its tongue planted firmly in its cheek", but at other times "sweetly naive".   Joe Baltake of The Sacramento Bee gave 1.5 stars (of 4) to the "whimsical if predictable" film "marred by a willful single-mindedness."    He found the films beginning "interminable", and overall, "very strange". 

 , the film holds a 22% approval rating on Rotten Tomatoes with 2 out of 9 critics giving it a positive review with an average rating of 3.9/10. 

==See also==
*  
*  

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 