The Bikini Carwash Company
{{Infobox Film
| name           = The Bikini Carwash Company
| image          = TBCC.jpg
| image_size     = 175px
| caption        = 
| director       = Ed Hansen
| producer       = George Buck Flower
| writer         = Ed Hansen George Buck Flower
| starring       = Joe Dusic Kristi Ducati Ricki Brando
| music          = Donald Mixon
| cinematography = Gary Orona
| editing        = Jose Ponce
| distributor    = Sterling Home Entertainment Imperial Entertainment
| released       = 1992
| runtime        = 87 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Bikini Carwash Company is a 1992 comedy film directed by Ed Hansen. It featured Joe Dusic, Kristi Ducati, and Ricki Brando. The sequel, The Bikini Carwash Company II, was released in 1993. This film marked the acting debut of Playboy Playmate Neriah Davis (credited as Neriah Napaul).

==Plot==
In the film, a group of young women decide to help out a local carwash by wearing bikinis while they wash customers cars. This succeeds in attracting more customers, more money, and more attention from the police, who are not amused by the scantily-dressed employees.

==Cast==
*Joe Dusic as Jack 
*Kristi Ducati as Melissa
*Ricki Brando as Amy Suzanne Brown as Sunny 
*Neriah Davis (credited as Neriah Napaul) as Rita 
*Brook Lyn Page as Tammy Joe 
*Eric Ryan as Stanley
*Scott Strohmyer as Big Bruce
*Patrick M. Wright as Uncle Elmer (credited as Michael Wright)
*Kimberly Bee as Bobby Canova
*Jim Wynorski as Ralph
*Darrel Matson as Snuff (credited as Duane Matson)
*Matthew Cory Dunn as Donavan
*Jack Klarr as Judge Hawthorne
*Landon Hall as Miss Hawthorne

== External links==
* 
* 

 
 
 
 
 
 
 
 
 


 