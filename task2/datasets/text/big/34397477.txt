Aalaap (film)
 
{{Infobox film
| name                = Aalaap
| image               = AALAAP.jpg
| caption             = Theatrical release poster
| director            = Manish Manikpuri
| producer            = Mr.Nishant Tripathi Mr.Abhishek Mishra
  Line Producer       =  Jai Prakash Upadhyay
| writer              = Dilip Shukla
| starring            = Amit Purohit Harsh Rajput Aabid Shamim Pitobash Ruhi Chaturvedi Rituparna Sengupta Abhimanyu Singh Raghubir Yadav Omkar Das Manikpuri Vijay Raaz Murli Sharma
| music               = Agnee
| cinematography      = Sunita Radia
| editing             = Vikas Tiwari, Manish Manikpuri
| casting             = Krishna Shriwastava
| distributor         = Shree Shankaracharya Arts Pvt Ltd
| released            =  
| country             = India
| language            = Hindi
}}
Aalaap  ( ); English: Modulation of Voice (In Singing)) is a 2012 musical drama film and the first film directed by Manish Manikpuri. The film is set and shot in and around present day Chhattisgarh with the main protagonists being college students with a passion for music.

==Cast==
* Amit Purohit
* Pitobash Tripathy
* Sunil Tiwari
* Harsh Rajput
* Aabid Shamim
* Ruhi Chaturvedi
* Rituparna Sengupta
* Gamya Wijayadasa
* Omkar Das Manikpuri
* Raghubir Yadav
* Vijay Raaz
* Abhimanyu Singh
* Murli Sharma
* Aashish patil
* mohan sagar

==Production==
===Development===
Aalaap is a Bollywood film produced under the banner of Shri Shankaracharya Arts Pvt. Ltd., Mumbai. The film which is based on youth and music has the potential to spread youth awareness across all corners of the nation. It has been jointly produced by renowned educationalists of Chhattisgarh Mr. Nishant Tripathi and Mr. Abhishek Mishra from SSCET group.

===Casting===
Raghubir Yadav,  Ruhi Chaturvedi  Vijay Raaz,krishna Srivastava & Onkar Das Manikpuri were roped in initially. Manish Manikpuri cites that "casting is one of the films strengths". Amit Purohit, Pitobash Tripathy, Harsh Rajput and Aabid Shamim,mohan lal sagar play the lead roles. Miss Sri Lanka 2009, Gamya Wijayadasa is making her Bollywood debut with an item song called "Chadti Jawaani Mazedaar". 

===Filming===
Aalaap has got a specific significance as it portrays the youth in the background of Chhattisgarh. It has been produced and directed by the people of Chhattisgarh’s origin and major part of the shooting has been done in the Chhattisgarh state itself.

==Soundtrack==
The music director of this film is Agnee with lyrics from Panchhi Jalonvi, K Mohan, Shailendra Singh Sodhi and Nawaldas Manikpuri. The first look of the song "Pa Paraa Paa" from the film has been revealed in a promo. Guitars and programming on all songs by Koco, additional Acoustic Guitar parts on all songs by K Mohan and bass Guitar on all songs by Among Jamir.
All Songs mixed at YRF Studios by Shantanu Hudiker and Abhishek Khandelwal. All recordings at Nishaad, the Agnee Studio except Vocal recordings at YRF Studios & Audio Garage Studio.

{| class="wikitable"
|- style="background:#ff9; text-align:center;"
! # !! Song !! Singer(s) !! Lyrics !! Length
|- 1 || "Ek Nai Roshni" || K. Mohan Back-up Vocals : Koco, Yamini Lavanian || Panchhi Jalonvi || 4:08
|- 2 || "Dil Yeh Awaaz De" || Meera Shenoy & K Mohan Back-up Vocals : Koco, Shruti Jakati & Parashar Joshi  || Panchhi Jalonvi || 5:31
|- 3 || "Paa Paraa Paa" || Nikhil DSouza, Bonnie Chakraborty, Dhruv Shrikent, K Mohan  || K Mohan || 5:13
|- 4 || "Soniye Soniye" || K Mohan || Shailendra Singh Sodhi (Shellee) || 5:34
|- 5 || "Shuruaat Pyaar Ki" || Nikhil DSouza, Yamini Lavanian || Panchhi Jalonvi || 4:57
|- 6 || "Chadhti Jawani" || Bonnie Chakraborty, Kanika Joshi || Nawaldas Manikpuri  ||  5:45
|-
|}

== See also ==
* Bollywood films of 2012
* Directorial debut films

==References==
  
  

==External links==
*   Amit Purohit & Harsh Rajput speak about their role in Aalaap  Sakaal Times
*    Rituparna Sengupta interview with  The Telegraph (Calcutta)
*   Agnee - The Band interview with RadioAndMusic
*   Agnee - with NH7 Weekender Team
*   Agnee - with Amanda Sodhi
*  
*  

 
 
 