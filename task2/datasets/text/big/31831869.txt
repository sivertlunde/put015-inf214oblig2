33 Postcards
 
 
{{Infobox film
| name = 33 Postcards
| image = 33Postcards2011Poster.jpg
| caption = Australian film poster Pauline Chan
| producer =  
| writer =  
| starring =  
| music = Antony Partos
| cinematography = Toby Oliver
| studio = Portal Pictures Pty Ltd
| distributor =  
| released =  
| runtime = 97 minutes
| country = Australia China
| language = English Mandarin
}}
  and Lincoln Lewis]] Pauline Chan and starring Guy Pearce.  It is the first co-production between China and New South Wales.  

==Plot==
Mei Mei (Zhu Lin) a 16-year-old Chinese orphan who has been supported by donations from her Australian sponsor Dean Randall (Guy Pearce), who sends her postcards that describe his family life. When her orphanage choir travels to Australia to participate in an Australian Choir Festival, Mei Mei takes the opportunity to find Dean with the hope he will make her part of his family. However, Mei Mei discovers the shocking truth – Dean is actually a convict in prison for manslaughter. Seeing Dean as her last chance at finding a home, Mei Mei decides to stay in Sydney until Dean gets his parole, in the meantime becoming naively entangled in the criminal world herself. To save Mei Mei from his own fate, Dean must make an impossible sacrifice.

==Development==
33 Postcards was developed under the title Mei Mei.  In 2009, Mei Mei was one of only two Australian screenplays selected from 475 submissions to partake in the Tribeca Film Institute program, Tribeca All Access.  The screenplay also featured as one of only three selected for Dungog Film Festival as part of the in the Raw Program. 

===Coproduction===
33 Postcards is an official film and television co-production in Australia. The co-production is between Australia and China, for which a co-production treaty did not exist prior to 2008.  33 Postcards is only the second film to be produced under this treaty and the first co-production between NSW and China.  This opportunity for co-productions to exist between China and Australia is largely unrealised in both countries, but has been identified as a potentially lucrative endeavour.  

==Production==
Lead actress Zhu Lin began production knowing little more than a dozen words in the English language.    The film was shot in both Australia and Zhejiang Province, China.

==Festivals==
33 Postcards has featured and been in competition at the following festivals
* 2011 Melbourne International Film Festival
* 2011 Sydney International Film Festival – In Competition
* 2011 Shanghai Film Festival China – In Competition
* 2011 Travelling Film Festival Huskisson Australia
* 2011 Singapore International Film Festival
* 2011 Hawaii International Film Festival
* 2012 Mostly British Film Festival San Francisco 
* 2012 Beijing Film Festival China
* 2012 Real Film Festival Newcastle, Australia – Closing Night Film
* 2012 Newport Beach Film Festival USA
* 2012 Fiuggi Family Film Festival Italy – In Competition 
* 2012 CinFest OZ – Opening Night Film 
* 2012 Chinese International Film Festival
* 2012 Raglan Film Festival New Zealand
* 2012 Italian Parliamentary Screening
* 2012 Canberra International Film Festival 
* 2013 Beijing Film Festival – Co-production Case Study 
* 2013 Social FIlm Festival Sorrento Italy
* 2013 CineMigrante International Film Festival Buenos Aires 

==Awards==
{| class="wikitable"
|-
! Award!! Category !! Nominated!! Outcome
|-
| The Sydney Film Festival || Community Relations Commission Award || 33 Postcards ||  
|-
| Shanghai Film Festival China || Rising Star Award || Zhu Lin ||  
|-
| Movie Convention Gold Coast Australia || Male Star of Tomorrow || Lincoln Lewis ||  
|-
| Mostly British Film Festival || Retrospective || Guy Pearce ||  
|-
| Fiuggi Family Film Festival Italy || Social World Film Festival Award || 33 Postcards ||  
|- Pauline Chan ||  
|-
| Chinese International Film Festival (Sydney) || Organising Committee Special Film Award || 33 Postcards ||  
|- AACTA Awards Best Lead Actor || Guy Pearce ||  
|- AACTA Awards Best Original Music Score || Antony Partos ||  
|}

==Future==
33 Postcards was anticipated for release in the second half of 2011 in Australia  and also in China.  The movie was released to video on demand on 15 April 2013. 

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 