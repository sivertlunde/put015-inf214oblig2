Faraway, So Close!
 
{{Infobox film
| name           = Faraway, So Close!
| image          = Faraway so close ver2.jpg
| caption        = Theatrical release poster
| director       = Wim Wenders
| producer       = Ulrich Felsberg Michael Schwarz Wim Wenders
| writer         = Richard Reitinger Wim Wenders Ulrich Zieger
| starring       = Otto Sander Bruno Ganz Heinz Rühmann Peter Falk Nastassja Kinski Willem Dafoe Solveig Dommartin Rüdiger Vogler
| music          = Laurent Petitgand  Nick Cave Laurie Anderson Lou Reed
| cinematography = Jürgen Jürges
| editing        = Peter Przygodda
| distributor    = Sony Pictures Classics
| released       = 18 May 1993 (Cannes Film Festival) 1 September 1993 (France) 9 September 1993 (Germany) 21 December 1993 (United States) 1 July 1994 (UK)
| runtime        = 144 minutes (German) 140 minutes (US)
| country        = Germany
| language       = German French English Italian Russian Spanish
| budget         = 
| gross          = $810,455 
}} Grand Prix du Jury and was nominated for the Palme dOr at the 1993 Cannes Film Festival.

==Plot==
 
Cassiel and Raphaella, two angels, observe the busy life of reunited Berlin. Due to their divine origin, they can hear the thoughts of the people around them, and try to console a dying man. Cassiel has been following his friend Damiel (a former angel), who senses his presence and talks about his experiences as a human. He owns a pizza parlor named Casa dellangelo (Angels House) and has married Marion, a trapeze artist whom he met when an angel. She works in a local bar in West Berlin, and the two have a young daughter (Marion).
 arms dealer and pornographer who owns a transport company.

Cassiel follows Hanna Becker (and Winter) to an abandoned building in the outskirts of East Berlin.  There he finds that she brings food to Konrad, a man who has acted as a father to her, since his days as her chauffeur during World War II. Traveling back in time, Cassiel is able to see the last days of Berlin late in the war. When the war appeared to have been lost, the father, a doctor, escaped to the United States with his son and adopted the name Baker. The mother Gertrud Becker stayed behind with the young Hanna, both under the care of Konrad.

Winter photographs the WWII-vintage cars which Konrad cares for and brings the photos to Baker. Winter drops the man as an employer, having investigated his activities. He encounters Peter Falk outside the hotel.
 underground (subway), Cassiel is tricked into gambling by Emit Flesti, losing his armor and money won during the game. Raphaella begs Flesti to give Cassiel time to understand what it is to be a human; he agrees but does not promise to stop hunting him.

Arrested and detained, Cassiel struggles to satisfy police demands for identification. He cannot give (or comprehend) his name or address, but refers the police to his friends pizza shop. Damiel arrives at the station and takes his now human friend home. Cassiel does not understand why everyone is so alone and cannot understand each other (his ability as an angel to hear their thoughts has disappeared.)

Tricked by Flesti into drinking alcohol, he becomes addicted and robs a shop with a gun taken from a teenager, who had been planning to kill his abusive  stepfather. Cassiel begins acting to make his way and feigns a car accident with Baker. He manages to get Baker to forge him a passport and birth certificate under the name Karl Engel (Charles Angel). Baker hires Cassiel as his valet, to pass him cards for cheating his fellow gangsters at poker. Stopping by Casa dellAngelo to return items borrowed from Damiel, Cassiel encounters Flesti again. He is collecting money from Damiel, after having loaned him money to set up the business.

After he saves Bakers life from an extortion attempt by Patzke, Baker makes Cassiel his partner. Finally learning the true nature of his business, Cassiel decides to. leave his service and stop him. He goes first to Konrads garage, to rest in one of his cars; Winter sleeps in another. When Winter is about to leave, he is killed by Flesti. Winter dies in Cassiels arms. Konrad arrives and Cassiel tells his life story, since the former chauffeur recognized him as the angel in his life. The former angel organizes his friends to destroy Bakers business. Meanwhile, Baker has reunited with his sister Hanna.

With the help of former angel Peter Falk, Cassiel gets into Bakers airport storage area. His team takes all the weapons and destroy the pornography copying machines. They send the weapons to a barge owned by other friends. Once having completed the plan, Cassiel feels ready to live as a human, but Flesti reports that Bakers rival, Patzke, has hijacked the barge with Bakers and Cassiels friends inside.

Flesti reveals himself as Time and says that he has to make Cassiel understand he does not belong in the human world; he has a word written on his forehead. At a boat lift, Cassiel gets on the barge and frees Raisa, moments before being killed. Flesti slows time so the rest can take over the barge and save the entire party. Cassiels friends are saddened by his death, but when Damiel hears a ring in his ear, he understands that Cassiel is near and laughs in joy.

==Cast==
* Otto Sander as Cassiel
* Bruno Ganz as Damiel
* Nastassja Kinski as Raphaela
* Horst Buchholz as Tony Baker
* Rüdiger Vogler as Phillip Winter
* Heinz Rühmann as Chauffeur Konrad
* Martin Olbertz as the dying man
* Aline Krajewski as Raissa
* Monika Hansen as Hanna / Gertrud Becker
* Peter Falk as Himself
* Lou Reed as Himself
* Mikhail Gorbachev as Himself
* Willem Dafoe as Emit Flesti / Time Itself
* Tilmann Vierzig as Young Konrad
* Antonia Westphal as Young Hanna
* Hanns Zischler as Dr. Becker
* Ingo Schmitz as Anton Becker
* Günter Meisner as Forger
* Ronald Nitschke as Patzke

==Cameos== Soviet leader Mikhail Gorbachev, all of whom play themselves.

==Music== ERG / SBK Records. The twenty track album has a run time of 76:33.
 The Wanderer" are augmentations of the original versions included on the July 1993 release of Zooropa.

# "Faraway So Close" ~ 3:56 ~ Nick Cave  
# "   
# "Why Cant I Be Good" ~ 4:22 ~ Lou Reed  
# "Chaos" ~ 4:51 ~ Herbert Grönemeyer 
# "Travelin On" ~ 3:49 ~ Simon Bonney 
# " )   
# "  
# "   
# "   
# "All Gods Children" ~ 4:42 ~ Simon Bonney  
# "Tightrope" ~ 3:18 ~ Laurie Anderson   Speak My Language" ~ 3:36 ~ Laurie Anderson  
# "Victory" ~ 4:06 ~ Laurent Petitgand 
# "Gorbi" ~ 2:51 ~ Laurent Petitgand 
# "Konrad (1st part)" ~ 1:56 ~ Laurent Petitgand 
# "Konrad (2nd part)" ~ 3:41 ~ Laurent Petitgand
# "Firedream" ~ 3:01~ Laurent Petitgand
# "Allegro" ~ 3:27 ~ Laurent Petitgand 
# "Engel" ~ 4:44 ~ Laurent Petitgand  
# "Mensch" ~ 1:38 ~ Laurent Petitgand

==Honors== Grand Prix du Jury and was nominated for the Palme dOr at the 1993 Cannes Film Festival.   

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 