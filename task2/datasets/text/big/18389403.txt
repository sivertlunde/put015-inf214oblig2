The Feather
For List of Touched by an Angel episodes
{{Infobox film
| name           = The Feather
| image          =
| caption        =
| director       = Leslie Hiscott
| producer       = Julius Hagen
| writer         = C.M. Matheson (novel)   Leslie Hiscott  
| starring       = Jameson Thomas   Véra Flory   Randle Ayrton   Mary Clare
| music          =
| cinematography =  Basil Emmott
| editing        =  Strand
| distributor    = United Artists
| released       = November 1929
| runtime        = 8,000 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} British romance romantic drama film directed by Leslie S. Hiscott, based on the 1927 novel of the same name by Charlotte Mary Matheson, and starring Jameson Thomas, Véra Flory, Randle Ayrton and Mary Clare.  It was made as a silent film with added sound effects and music. It was made by the independent producer Julius Hagen at Elstree Studios. 

==Cast==
* Jameson Thomas as Roger Dalton 
* Véra Flory as Mavis Cottrell 
* Randle Ayrton as Rizzio 
* Mary Clare as Mrs. Dalton 
* W. Cronin Wilson as Mr. Cottrell 
* James Reardon as Quint 
* Charles Paton as Professor Vivian 
* Irene Tripod as Mrs. Higgins 
* Grace Lane as Nun 

==References==
 

 
 
 
 
 
 
 
 
 
 

 