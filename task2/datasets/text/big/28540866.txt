Mega Shark Versus Crocosaurus
{{Infobox film
| name           = Mega Shark Versus Crocosaurus
| image          = megasharkvscrocosaurus.jpg
| caption        = DVD cover
| director       = Christopher Douglas-Olen Ray
| producer       =  
| writer         =   
| starring       =  
| music          = Chris Ridenhour
| cinematography = Alexander Yellen
| editing        = Jose Montesinos
| distributor    = The Asylum
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $1,000,000
| grossed        = 170,500,000$
}} monster disaster Hannah Cowley and Sarah Lieving.

The film is a sequel to the 2009 film Mega Shark Versus Giant Octopus but contains little of the original cast from that film. 

==Story==
Deep in the Democratic Republic of the Congo, an illegal diamond mining operation is interrupted by the presence of a giant 150-foot crocodile. Meanwhile, on the Atlantic Ocean, the US Navy warship USS Gibson is attacked and sunk by the Megalodon that ended up surviving suffocation by the octopus in the first film. Lt. Terry McCormick (Jaleel White) who was experimenting with a sonic shark-repelling device, is the sole survivor of the attack. Back in DR Congo, an English hunter named Nigel Putnam (Gary Stretch) successfully captures the giant crocodile when it attempts to eat him and he injects his tranquilizer darts in its mouth. He then has the crocodile delivered to a shipping yard for his friends to load it in a cargo ship.

In Los Angeles, California, McCormick is being interrogated by National Oceanic and Atmospheric Administration. Special Agent Hutchinson (Sarah Lieving) for the USS Gibson incident. He feels responsible for the sinking, suggesting that his sonic device lured in the Megalodon. Because of this, he offers to help Hutchinson hunt down the Megalodon with his invention. Meanwhile, a cargo ship is carrying the tranquilized crocodile on the Atlantic Ocean 400 miles south of Florida. It is revealed that the ship is also carrying the crocodiles eggs as well. Then, without warning, the ship is attacked by the Megalodon, waking up the crocodile from its sleep. Putnam & his partner Jean jumps overboard before the ship is destroyed in the ensuing battle.

Aboard the aircraft carrier USS Lexington, McCormick is introduced to Admiral Calvin (  fire their missiles at a cave holding more eggs. As the crocodile invades Miami and heads toward Orlando, Florida|Orlando, McCormick suggests the use of an arc flash powered by the nearby Turkey Point Nuclear Generating Station to ward it off. The gamble works, as the arc flash sends the crocodile back into the sea.
 Black Hawk helicopters. They also discover that the Megalodon is attracted to the eggs, as it has devoured the Invincible and the Black Hawks transporting them. As no cage or trap is big enough to capture both creatures, Putnam suggests to lure both the Megaladon and crocodile into the Panama Canal, much to McCormicks disapproval. The submarine USS Argonaut is sent to the wreckage of the cargo ship to retrieve some crocodile eggs. The retrieval mission is a success, and the eggs are dropped into the Panama Canal. Instinctively, both the Megalodon and the crocodile arrive at the canal, where they battle each other while being attacked by the Naval fleet. The fight causes a tidal wave that destroys the entire Panama Canal as both creatures continue to bite each other.
 Santa Monica pier, the nuclear submarine USS Carter is in pursuit of the two giant creatures, which are headed toward Hawaii. The Carter fires a nuclear torpedo at the creatures, but misses before it is swallowed by the Megalodon. This results in the Megalodon becoming an even bigger threat, as it now has a nuclear reactor inside it. As the crocodile invades Hawaii, it causes the helicopter carrying McCormick, Putnam and Hutchinson to crash. With Hutchinson unconscious, McCormick and Putnam leave her and take a raft to drop McCormicks sonic emitter in the sea as part of their new plan; to lure all the creatures together near an undersea volcanic range. The sonic emitter lures in the adults, who fight each other, and the crocodile hatchlings come to assist their mother. McCormick and Putnam are then picked up by Hutchinson, who has recovered and reactivated her helicopter. The volcano explodes and detonates the nuclear core in the shark, the combined force killing both the adults and juveniles. The trio fly home while the crocodile and shark, now completely charred and still latched on to one another, sink into the volcano.

After the credits, Nigel meets up with Jean on a beach and mentions a giant lizard in Japan (Godzilla reference). He asks him if hes up for it, and his partner says "On one condition - no more boats." They shake hands and head off on another adventure.

==Cast==
*Jaleel White as Lt. Terry McCormick
*Gary Stretch as Nigel Putnam
*Sarah Lieving as Special Agent Hutchinson
*Robert Picardo as Admiral Calvin
*Gerald Webb as Jean
*Dylan Vox as CWO Butowski Hannah Cowley as Legatt
*Michael Gaglio as Captain Smalls
*Jessica Irvine as the USS Omaha Captain Steve Mason as an Investigator
*Neil Watson as an Investigator II
*Robert R. Shafer as Charlie Ross
*Darin Cooper as Commander Vail
*Luke Noll as Hobo Rogers McFreely
*Joey David Garcia as Teenager
*Claire Scott as Teenager
*Clem Hansen as Teenager

==Production==

===Connections with other Asylum titles===
The film makes numerous references to Mega Shark Versus Giant Octopus, such as an autographed photo of singer/songwriter/actress Debbie Gibson and the Japanese poster of the first film. In addition, scenes of people panicking are taken from the film Megafault.

===Marketing===
To promote the film, The Asylum participated in a competition to make the next Doritos commercial for Super Bowl XLV. In the commercial, the Megalodon attacks a naval fleet while Capt. Smalls (Michael Gaglio) asks if anything will satisfy its hunger. Then he hears a crunch and sees the janitor eating a bag of Doritos. In the next scene, a helicopter carries a giant Dorito chip over the ocean. The Megalodon jumps and eats the chip before fleeing from the scene as the whole crew celebrate.  

==Reception== Dread Central jumped the mega shark."  Felix Vasquez, Jr. of Cinema Crazed gave the film one star, calling it "a tedious and horrific mess that at least gives audiences what it wants: A big croc eating people, a big shark eating people, and two gargantuan stock CGI animals battling it out for the fate of Earth. Or something." 

==Sequel==
 
On Saturday, March 24, 2012, Dread Central reported that The Asylum was developing a second sequel featuring the titular Mega Shark, titled Mega Shark Versus Mecha Shark.  The announcement came after the company realized the growing popularity of the character, including an online petition for a third chapter in the series. 

Mega Shark vs. Mecha Shark is set for release on January 28, 2014, with Debbie Gibson reprising her role as Emma MacNeil from the first film.  

==See also==

*List of killer crocodile films
*List of killer shark films

==References==
 

==External links==
*   at The Asylum
*  
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 