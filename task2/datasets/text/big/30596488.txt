Snow White: The Sequel
 
{{Infobox film
| name           = Snow White: The Sequel
| caption        = 
| image	=	Snow White- The Sequel FilmPoster.jpeg
| director       = Picha
| producer       = Grzegorz Handzlik Eric van Beuren Linda Van Tulden Steve Walsh  Arlette Zylberberg 
| writer         = Tony Hendra Picha
| starring       = 
| music          = Willie Dowling
| cinematography = 
| editing        = Chantal Hymans
| distributor    = 
| released       = 2007
| runtime        = 82 minutes
| country        = United Kingdom Belgium France
| language       = French
| budget         = 
| gross          = 
}}

Snow White: The Sequel ( ) is a 2007 Belgian/French/British X-rated animated film directed by Picha. It is based on the fairy tale of Snow White and intended as a sequel to Disneys classic animated adaptation. However, like all of Pichas cartoons the film is actually a sex comedy featuring a lot of bawdy jokes and sex scenes.

It wasnt the first pornographic animated adaptation of Snow White. Already in 1973 an 11 minute short cartoon called Snow White and the Seven Perverts was made in Germany.

==Synopsis==
Prince Charming was supposed to live long and happily with Snow White after kissing her back to live. However married life doesnt become them, given her lack of domestic skills and his prancing job. Soon each drifts to an affair, only the start of a whole series of perversions of various fairy tale characters traditional good nature, such as the dwarfs acting as loan shark syndicate. The jealous good fairy takes the cake, as motor of evil twists...

==Cast==

===Original French Version===
*Cécile De France - Blanche-Neige / La Belle au Bois dormant / Cendrillon
*Jean-Paul Rouve - Le Prince charmant
*Marie Vincent - La Bonne fée
*Jean-Claude Donda - Les sept nains / Le grand Veneur
*Gérard Surugue - Les sept nains (voice)
*Benoît Allemane - Narrateur (voice)
*Marc Alfos - Logre (voice)
*François Barbin - La Bête
*Mona Walravens - La Belle
*Anaïs Croze|Anaïs - Blanche-Neige (singing voice)
*Sasha Supera - Tom Pouce

===English Version===
*Stephen Fry - The Narrator
*Sally Ann Marsh - Snow White
*Jackie Sheridan - Singing voice of Snow White
*Simon Greenall as Prince Charming
*Rik Mayall - The Seven Dwarves (Horny, Grungy, Funky, Scummy, Spotty, Filth, and Mental)
*Michael Kilgarriff - Hannibal the Ogre
*Shelley Blond - Cinderella
*Lia Williams - Sleeping Beauty
*Morwenna Banks - The Good Fairy Tom George, Sarah Hadland, Vicki Hopps, Simon Schatzberger as additional voices.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 