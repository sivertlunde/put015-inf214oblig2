Kaand: Black Scandal
 

{{Infobox film
| name           = Kaand Black Scandal
| image          = 
| caption        = 
| director       = Santosh K Gupta
| producer       = Jagbir Singh Siwatch
| starring       = {{Plain list|
* Suraj K Shah
* J.B Rana
* Dipanshi
* Mahimaa}}
| music          = Sangita Trivedi, Param Kishor, Jagjit Singh (Babby), Sachin Dhavre, Sanjay Bagga
| cinematography = Jai Nandan Kumar, Roopang Acharya
| editing        = Ram Gopal Verma
| distributor    = Dilsa Distributors Mumbai
| released       =  
| country        = India
| language       = Hindi 
 
}}

Kaand Black Scandal (  film which is set in the Meerut city of Uttar Pradesh - written and directed by Santosh K Gupta and starring Suraj K Shah, J.B Rana, Dipanshi sharma and Mahimaa Shrivastav in a lead roles.  It is distributed by Dilsa Distributors Mumbai And Music Released on Yellow And Red Music. The film premiered on 15 November 2013  in Mumbai and was released throughout India in April 2014. 

== Cast ==
* Suraj K Shah as Rajveer - A final year student of D.J.College, Rajveer is lazy, naughty ambitionless but young and happening friend of Sandy, Mandy and Batla. Although he is good for nothing, there is something about him that draws you to him. In life he wants - no burdens, no boundaries, no expectation, no pressure..... Just freedom.
* J.B Rana as Beera - The sport officer in D.J. College. He motivate and inspire his team to think big. He love and care too much about his brother... raajveer.
* Dipanshi as Seema - Very Bold and moody student who used to ignore beera.
* Mahimaa as Rekha - A good looking and charming character who enjoy reciting poetry.
* Shobna as Gujri
* Sanjiv Nehra as Viraj
* Surendar Bawra as Dharamdas
* Preet Bhatti as Umed
* Sanjiv Arora as Ajit
* Riyaz Indian as Pelwaan
* Nikhil Saxena as Sandy
* Saiyed as Mandy
* Satish as Batla

== Plot ==
Kaand is the story of brothers who return to their village and find themselves pulled into a feudal intrigue.  Beera is a sports coach in the college in which his younger brother Rajveer (Suraj K Shah) is a student. Beera falls for the new girl in college, seema, but she turns down his marriage proposal. Rajveer falls in love with another college student, rekha, but she too rejects him.

Beera and Rajveer go to their village during college holidays. Seema, too, lands up in the same village, to visit her uncle, Viraj. Beera and seema meet often and soom fall in love. However, when beera tells his father, Dharamdas, that he want to marry seema, he refuses him. Dharamdas tells Beera about the incident that had happened decades ago. Dharamdas wife, Rukmani, consider Viraj her brother. She had gone to his house on Raksha Bandhan to tie him a rakhi but never returned. Viraj claimed that Rukmani dint come to his house, but Dharamdas suspected him of lying and of having killed or kidnapped her.

The relationship between Dharamdas and Viraj had soured, and now Dharamdas dint want to have anything to do with Viraj.

Rekha come to visit Seema and meets Rajveer again. They also soon fall in love.

Meanwhile, Gujri, wife of Ajit singh, who is Virajs brother, hatches a plan to get their son, Umed, married to Seema, who is the niece of Virajs wife, Gujri realizes that seema love Beera, so she kills her husband Ajit, to frame Beera for his murder. Beera is arrested by the police. But the police soon learn that Gujri has kidnapped Seema to force her to marry Umed.

Will Seema be Freed? Will Seema and Beera find Love? Will rajveer and Rekha find love? and is Rajveers mother still alive?

== Production ==
The film was shot in Delhi, Meerut, Modinagar Gaziyabaad, Aghera village, kausani, Nainitaal.
The College scenes took place at the D.J. College of Dental Sciences & Research
Niwari Road, Modinagar, Ghaziabad, (U.P.)

== Soundtrack ==
The films soundtrack is composed by Jagjit Singh Bubby, Sanjay Bagga, Sachin Dhavre, Param, Sanjgeeta Trivedi
with lyrics penned by Ravi Chopra, Shahid Dilshad Shamli, Santosh Gupta and J.B.Rana.

{| class="wikitable"
|-
! Track# !! Song !! Singer(s) !! Duration
|-
| 1 || Baar Baar Kehti Hoon || Neetu Singh || 3:44
|-
| 2 || Bijhde Huye Do Dil || Nitesh Kumar & Jamuna || 3:56
|-
| 3 || Dekhe Bina Tujhko Toh || Sonu Nigam & Mahimaa || 5:06
|-
| 4 || Dil Mein tumhare Kya Uljhan  || Jhirimi, sonu sikandar, soni || 7:14
|-
| 5 || Samajh Aankhon Ke Ishare || Neetu Singh & Sonu Sikandar || 2:06
|-
| 6 || Chandi Badan Ye || Kiran Shail || 4:45
|-
| 7 || Tujhko dekhu Toh || Jhirimi & Deepak Daba || 2:58
|}

== Reception ==
Kaand black scandal opened to positive reviews across India. It got positive  review on B-town.in. 

== References ==
 

== External links ==
*  
*   on YouTube

 
 
 
 
 
 
 
 
 