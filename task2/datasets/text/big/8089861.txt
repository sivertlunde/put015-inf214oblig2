A Sunday in Kigali
{{Infobox film
| name           = A Sunday in Kigali
| image          = SundayinKigali.jpg
| caption        = Poster
| director       = Robert Favreau
| producer       =
| writer         = Robert Favreau (screenplay) Gil Courtemanche (original novel)
| narrator       =
| starring       = Luc Picard Fatou NDiaye
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =
| runtime        = 118 minutes
| country        = Canada French
| budget         =
| preceded by    =
| followed by    =
}}

A Sunday in Kigali (original   Canadian feature film set during the Rwandan genocide.

Directed by Robert Favreau, it relates the story of Bernard Valcourt, a documentary film maker and journalist who falls in love with a young Rwandan woman, Gentille, who works at the Hôtel Des Mille Collines. The brutal violence of the Rwandan genocide separates them; a few months later, Bernard returns to Rwanda seeking Gentille. Most of the narrative unfolds in retrospect.

The film is based on the novel A Sunday at the pool in Kigali, by Gil Courtemanche.

==Release==
"A Sunday in Kigali" grossed $1.1 million Canadian in Quebec in the fall of 2006, and is set for Sept. 23 release in English-speaking Canada. Video and cable are the best options in other territories.

===Cast===
*Luc Picard ...  Bernard Valcourt
*Fatou NDiaye ...  Gentille
*Vincent Bilodeau
*Céline Bonnier
*Geneviève Brouillette
*Alice Isimbi
*Fayolle Jean
*Maka Kotto
*Louise Laparé
*Alexis Martin
*Luck Mervil
*Mireille Metellus
*Luc Proulx
*Guy Thauvette ...  Lt. Roméo Dallaire
*Erwin Weche

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 