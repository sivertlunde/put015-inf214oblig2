Intolerable Cruelty
{{Infobox film
| name           = Intolerable Cruelty
| image          = Intolerable cruelty.jpg
| image_size     = 215px
| alt            = Man in a grey suit, standing beside a woman in a red dress. 
| caption        = Theatrical release poster Joel Coen Ethan Coen  }}
| producer       = {{Plain list |
* Ethan Coen
* Brian Grazer
* Joel Coen  }}
}}
| screenplay     = {{Plain list |
* Robert Ramsay
* Matthew Stone
* Ethan Coen
* Joel Coen
}}
| story          = {{Plain list |
* Robert Ramsay
* Matthew Stone
* John Romano
}}
| starring       = {{Plain list |
* George Clooney
* Catherine Zeta-Jones
* Geoffrey Rush
* Cedric The Entertainer
* Edward Herrmann
* Paul Adelstein
* Richard Jenkins
* Billy Bob Thornton
}}
| music          = Carter Burwell
| cinematography = Roger Deakins
| editing        = Joel Coen Ethan Coen  
| studio         = {{Plain list |
* Imagine Entertainment
* Alphaville
* Mike Zoss Productions
}}
| distributor    = Universal Studios
| released       =  
| runtime        = 100 minutes 
| country        = United States
| language       = English
| budget         = $60 million 
| gross          = $120,217,409   
}}
 Joel Coen Ethan Coen and Brian Grazer. The Coen brothers also wrote the last draft of the screenplay about divorce and lawyers in Los Angeles. The film stars George Clooney, Catherine Zeta-Jones, Geoffrey Rush, Cedric the Entertainer, Edward Herrmann, Paul Adelstein, Richard Jenkins and Billy Bob Thornton.

==Plot== attorney and the inventor of the "Massey pre-nup", a completely foolproof prenuptial agreement. Miles wins the divorce case, leaving Donaly with nothing. 

Private investigator Gus Petch (Cedric The Entertainer) is tailing the wealthy and married Rex Rexroth (Edward Herrmann) on a drunken night out with a blonde. When they stop at a motel, Gus bursts in and tapes them with a video camera. He takes the evidence of infidelity to Rexs wife, Marylin Rexroth (Catherine Zeta-Jones), whose primary motivation is obtaining wealth and independence via divorce. Rex hires Miles, and Marylins friend, a serial divorcée named Sarah Sorkin (Julia Duffy), warns Marilyn that Miles will be a dangerous opponent.

Marylin and her lawyer, Freddy Bender (Richard Jenkins), fail to reach an agreement with Miles and Rex. Bored Miles asks the fascinating Marylin to dinner, where they flirt. In court, Miles gets the Baron Krauss von Espy (Jonathan Hadary) to testify that Marylin had asked him to point out a man she could marry who was very rich, easily manipulated, and likely to be unfaithful. Marylin winds up with nothing, and Miless aged boss, Herb Myerson (Tom Aldredge), congratulates him. 
 Emmy statuette. Soon after, Marylin shows up at Miless office with a person she says is her new fiancé, supposedly an oil millionaire named Howard D. Doyle (Billy Bob Thornton). Marylin insists on the Massey prenup, but Miles sees Howard destroy it during the wedding, in a demonstration of love.
 Las Vegas to give the keynote address at a convention for divorce attorneys, Miles bumps into Marylin, who says she is now disenchanted with her wealthy but lonely life, having divorced Howard and received the vast Doyle Oil fortune. Miles is thrilled, and they marry on the spur of the moment. He signs the Massey prenup, but she tears it up. The next morning a disheveled Miles announces at the convention that love is the most important thing, and that he is abandoning divorce suits in favor of pro-bono work.

Then Miles discovers that "Howard D. Doyle" was just an actor from one of Donalys soap operas. Marylin has tricked him, and now his wealth is at risk. Miles boss demands that something be done to save the firms reputation, and suggests the hitman "Wheezy Joe" (Irwin Keyes), who Miles hires to kill Marilyn. 

Miles then learns that Marylins ex-husband Rex has died without changing his will, leaving her millions. Miles rushes to save his wife from the hitman, but Marilyn has already agreed to pay him double to kill Miles instead. There is a struggle and in the confusion Wheezy Joe mistakes his gun for his asthma inhaler, and kills himself. 

Later, Miles, Marylin and their lawyers meet to negotiate a divorce. Miles pleads for a second chance and retroactively signs a Massey prenup. She tears it up, and they kiss. Marylin then tells Miles that to get Donalys help for supplying Doyle, she suggested an idea to him for a TV show: Gus Petch becomes the host of a big hit, Americas Funniest Divorce Videos.

==Cast==
* George Clooney as Miles Massey
* Catherine Zeta-Jones as Marylin Hamilton Rexroth Doyle Massey
* Geoffrey Rush as Donovan Donaly
* Cedric the Entertainer as Gus Petch
* Edward Herrmann as Rex Rexroth
* Paul Adelstein as Wrigley
* Richard Jenkins as Freddy Bender
* Billy Bob Thornton as Howard D. Doyle
* Julia Duffy as Sarah Batista OFlanagan Sorkin
* Jonathan Hadary as Heinz, the Baron Krauss von Espy
* Tom Aldredge as Herb Myerson
* Stacey Travis as Bonnie Donaly
* Isabell OConnor as Judge Marva Munson
* Irwin Keyes as Wheezy Joe
* Colin Linden as Father Scot
* Kiersten Warren as Claire OMara

==Production== James Dickeys novel To The White Sea fell through, the Coens signed on to direct. {{Cite journal
| last = Walters
| first = Ben
| authorlink =
| coauthors =
| title = Bringing up alimony
| journal = Sight and Sound
| volume = 13
| issue = 11
| pages = 30
| publisher = British Film Institute (BFI)
| location =
| date = November 2003
| url =
| issn = 0037-4806
| doi =
| id =
| accessdate = May 23, 2010
}} 

==Reception==
 
The film received positive reviews from critics. Rotten Tomatoes gives the film a "Certified Fresh" score of 75% based on reviews from 180 critics.  Metacritic gives a weighted average score of 71% based on reviews from 40 critics. 

==Soundtrack==
{{Infobox album  
| Name        = Original Motion Picture Soundtrack: Intolerable Cruelty
| Type        = Soundtrack
| Artist      = Carter Burwell and various artists
| Cover       =
| Cover size  = 0
| Released    = October 7, 2003
| Recorded    =
| Genre       = Film score pop music|pop, blues
| Length      = 50:50
| Label       = Hip-O Records|Hip-O
| Producer    =
| Chronology  = Coen Brothers film soundtracks The Man Who Wasnt There (2001)
| This album  = Intolerable Cruelty (2003) The Ladykillers (2004)
}}
{{Album ratings
| rev1 = Allmusic
| rev1Score =  
| rev2 = Movie Music UK
| rev2Score =   
| rev3 = SoundtrackNet
| rev3Score =   
| noprose = yes
}}
Intolerable Cruelty is scored by Carter Burwell, in his tenth collaboration with the Coen Brothers.
 pop songs Canadian blues music from Glory of Love" by Big Bill Broonzy.

  
:Tracks by Carter Burwell unless otherwise noted.
# "The Boxer" (Simon and Garfunkel) – 5:09
# "Intolerable Mambo – 1:41
# "Suspicious Minds" (Elvis Presley) – 4:33
# "Hanky Panky Choo Choo" – 2:07
# "Dont Cry Out Loud" (Melissa Manchester) – 3:48 Feels So Good" (Chuck Mangione) – 9:42
# "You Fascinate Me" – 1:40
# "April Come She Will" (written by Paul Simon, performed by Colin Linden) – 0:59
# "Heather 2 Honeymoon" – 1:39 Tom Jones) – 4:18
# "Love Is Good" – 3:26
# "Non, Je Ne Regrette Rien" (Édith Piaf) – 2:21
# "No More Working" – 3:01
# "Fully Exposed" – 1:46 Glory of Love" (Big Bill Broonzy) – 2:20
# "The Boxer" (Colin Linden) – 2:20

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 