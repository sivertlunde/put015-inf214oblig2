American Sharia
{{Infobox film
| name           = American Sharia
| image	         = 
| image size     = 
| caption        = 
| director       = Murad Amayreh Omar Regan
| producer       = Couni Young Micah Brandt
| writer         = Omar Regan
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Omar Regan Baba Ali Eric Roberts
| music          = 
| cinematography = Keith DeCristo
| editing        = Brad Geiszler
| studio         = Halalywood Entertainment
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American Buddy buddy cop comedy-drama action film directed by Murad Amayreh and Omar Regan, written by Omar Regan, and stars Omar Regan, Baba Ali and Eric Roberts. The film is about rogue government officials using Islamophobia to maintain power    while two Muslim police officers attempting to solve a case involving the disappearance of several Muslims.

==Plot==
Officer Richardson (Eric Roberts) profiles every Muslim he sees as a terrorist. He goes on an arrest frenzy, which makes the community activists, Jihad (Adam Saleh) and Osama (Sheikh Akbar) take to the streets and rally the people to stand up for their rights.

The Chief of Police (Joshua Salaam) wants to gain the trust of the Muslim community in order to get re-elected, but Attorney Leila Rodriguez (Yasemin Kanar) is standing in the way with a discrimination lawsuit against Officer Richardson and the Motor City Police Department. The Chief calls, Mohammed (Omar Regan) who keeps his faith to himself and his own prejudices against Islam, following constant exposure to Islamophobia, and partners him with a Middle Eastern, Detective Abdul (Baba Ali), who proudly shows his religious values. Together, they have to solve a case involving the disappearance of several Muslims as well as a respectable Muslim leader who is charged with an assault arrest. The Chief wants them to "Speak Muslim To The Muslims" which he thinks will guarantee his re-election and results in ending rising Islamophobic tensions between the police and the community.

==Cast==
*Omar Regan as Mohammed
*Baba Ali as Abdul
*Eric Roberts as Officer Richardson
*Joshua Salaam as Police Chief
*Yasemin Kanar as Attorney Leila Rodriguez
*Adam Saleh as Jihad
*Sheikh Akbar as Osama

==Production==

===Development and pre-production===
Omar Regan is the director, producer and writer of American Sharia, and it is the first production under his company Halalywood Entertainment. It took about two years to get the film project started. Before production for the film even began, Regan created a two-minute trailer in hope of selling the project to investors. In December 2013, the campaign for the film was launched, Regan used Kickstarter to generate funds for the film and the film earned $122,000 in donations in less than 40 days.         
 boundaries of Islam.   

===Filming and post-production===
     }}
In February 2015, Regan told Asian Image, "American Sharia is a Hollywood motion picture that sets out to use comedy to reverse the prejudices held against Islam and help promote the religion in a more positive way. Its an alternative form of entertainment, during a time where we are surrounded and exposited to increasing negativity and profanity."

When the project became financially secure, Regan decided to bring the production to the diverse communities of Metro Detroit. The film crew is comprised of people from diverse backgrounds, religions and ethnicities. 
 Dearborn Heights after a month-long shoot around Metro Detroit that employed dozens of Muslims and Arab Americans from California, New York and Michigan. On the last day of production at the Berwyn Senior Citizen Center, more than 100 extras turned out, many dressed in Islamic attire, to shoot a scene depicting a homeless shelter being raided by the police. 

In February 2015, Regan told Asian Image, "American Sharia is a Hollywood motion picture that sets out to use comedy to reverse the prejudices held against Islam and help promote the religion in a more positive way. Its an alternative form of entertainment, during a time where we are surrounded and exposited to increasing negativity and profanity."   Whilst speaking with Asian Sunday Newspaper he added, "...The message of the film is that we all need love and unity because we all are the creation of Allah."   

==Release==
Between February and March 2015, British charity Penny Appeal are due to exclusively host the UK screenings of American Sharia at specially selected venues in 22 cities.   

==See also==
*Islamic humour
*Islam in the United States

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 