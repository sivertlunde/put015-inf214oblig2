The Lady in Question
 
{{Infobox film
| name           = The Lady in Question
| image          = 
| image_size     = 
| caption        = 
| director       = Charles Vidor
| writer         = Marcel Achard Jan Lustig Lewis Meltzer
| narrator       = 
| starring       = Brian Aherne
| music          = 
| cinematography = Lucien Andriot
| editing        = 
| studio         = 
| distributor    = 
| released       = August 7, 1940
| runtime        = 80 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} comedy drama film directed by Charles Vidor and starring Brian Aherne, Rita Hayworth and Glenn Ford.  Director William Castle appears in a cameo as "Angry Juror".

==Synopsis==
After serving on a Paris jury which acquits a young woman of any blame for the death of a young man she had known, Andre Morestan invites her to live and work at his bicycle shop so she can readjust to society. However, he decides to keep her true identity a secret which soon begins to raise the doubts of his family.

==Cast==
*Brian Aherne as Andre Morestan
*Rita Hayworth as Natalie Roguin
*Glenn Ford as Pierre Morestan
*Irene Rich as Michele Morestan
*Evelyn Keyes as Francois Morestan
*George Coulouris as Defense attorney

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 