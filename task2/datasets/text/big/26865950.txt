13 Assassins
 
{{Infobox film
| name           = 13 Assassins
| image          = Thirteen Assassins.jpg
| caption        = Japanese film poster
| director       = Takashi Miike
| producer       = Toshiaki Nakazawa Jeremy Thomas Takashi Hirajō
| screenplay     = Daisuke Tengan
| story          = Shōichirō Ikemiya
| starring       = Kōji Yakusho Takayuki Yamada Yūsuke Iseya Goro Inagaki|Gorō Inagaki Masachika Ichimura Mikijirō Hira
| music          = Kōji Endō
| cinematography = Nobuyasu Kita
| editing        = Kenji Yamashita RPC Toho
| distributor    = Toho   Artificial Eye  
| released       =  
| runtime        = 141 minutes
| country        = Japan United Kingdom
| language       = Japanese
| budget         = $6 million 
| gross          = $17,054,213
}} Japanese jidaigeki (period drama) film directed by Takashi Miike.    The screenplay was written by Daisuke Tengan.    The film is a remake of Eiichi Kudo’s 1963 black-and-white Japanese film of the same name, Jûsan-nin no shikaku. A samurai epic with a loose historical basis,  the film was produced by Toshiaki Nakazawa, who also produced the 2009 winner of Academy Award for Best Foreign Language Film Departures (film)|Departures.  
 Memoirs of Shall We Yamada and Takaoka co-starred, Japan Academy Prize. 

==Plot== sadistic half-brother, assassinate the daimyo. Naritsugus loyal retainers led by Hanbei, a fellow dojo student of Shinzaemon, however, learn of the plot by spying on Dois meetings.
 official journey from Edo back to his lands in Akashi. Just before they leave, Hanbei arrives and warns his old colleague that he will suffer grave consequences if he tries to kill Naritsugu.
 Ochiai in official highway forcing Naritsugu to detour toward Ochiai. During the assassins journey to the town, they are attacked by ronin paid off by Hanbei to kill the plotters. The group decide to head through the mountains but become lost. In the process they encounter hunter Kiga Koyata, who becomes their guide, and they adopt him as the thirteenth assassin.

Ochiai is converted into an elaborate maze of booby traps and camouflaged fortifications. When Naritsugu arrives, his retainers have been reinforced by additional troops and the 13 assassins are no longer facing the 70 samurai expected and planned for; now there are 200. A lengthy battle follows with Naritsugu and his guards becoming trapped inside the village and attacked on all sides by arrows, gunpowder, spears, and swords, along with Koyata who fights with rocks in slings. As he witnesses the carnage, an excited Naritsugu vows that its the most fun he has ever experienced. He tells Hanbei that when he ascends to the Shoguns counsel he will bring back the civil wars of the Sengoku period.

Slowly, the assassins are killed, but not before they inflict heavy casualties on the Akashi forces. Naritsugu and Hanbei manage to escape the ruins, only to be cornered by Shinzaemon and Shinrokurō. After Shinzaemon kills Hanbei, Naritsugu contemptuously kicks his loyal retainers head away, announcing that the people and samurai have only one purpose and that is to serve their lords. Shinzaemon counters by telling Naritsugu that lords cant live without the support of the people, and that if a lord abuses his powers the people will always rise against them. Naritsugu and Shinzaemon both mortally wound each other. As the lord crawls away in mud crying and experiencing fear and pain for the first time, he thanks Shinzaemon for showing him excitement before Shinzaemon decapitates him.

After Shinzaemon dies, Shinrokurō wanders through the carnage. He meets Koyata, who has made a miraculous recovery after being stabbed in the neck by Naritsugu. The pair then make their own ways out of the town. An epilogue states that in May 1844 the Shogun and his government covered up what really happened, announcing that Naritsugu died of illness on the journey. 23 years later the Tokugawa Shogunate would be overthrown with the Meiji Restoration.

==The 13 assassins==
(Character names follow the pattern of Japanese names, with the family surname placed first)
* Shimada Shinzaemon (Kōji Yakusho) – The leader of the group, a war weary, decorated samurai who believes that there is more to Bushido than blind obedience. Having believed that there was no chance for an honorable death, he is deeply elated when hired to carry out the mission. He is the 11th and final one to die during the battle.
* Kuranaga Saheita (Hiroki Matsukata) – Second in command to Shinzaemon, another veteran samurai who volunteers his best and most trusted students to join the group. He is the 9th to die after all of his students, he finally succumbs to his wounds as the town burns down around him and begins to fall apart.
* Hioki Yasokichi  (Sousuke Takaoka|Sōsuke Takaoka) – A highly skilled samurai from Kuranagas dojo, he is the 1st to die, by his wounds after getting speared.
* Ōtake Mosuke (Seiji Rokkaku) – A plump-faced samurai with a jovial demeanor, but remains a tenacious fighter. He is the 3rd to die after watching Ishizuka die, though his death is not seen on screen.
* Horii Yahachi (Koen Kondo|Kōen Kondō) – A skilled samurai from Kuranagas dojo who trained with Gennai in the use of explosives. He is the 5th to die after blowing himself up.
* Higuchi Gennai (Yuma Ishigaki|Yūma Ishigaki) – Horiis demolition partner, a tough and skilled samurai who trained with Yahachi in the use of explosives. He Is the 6th to die after numerous wounds.
* Mitsuhashi Gunjirō (Ikki Sawamura) – Another samurai from Kuranagas dojo who prepares Ochiai to trap their victim. He is the 4th to die while making a last stand.
* Hirayama Kujūrō (Tsuyoshi Ihara) – A ronin of unmatched swordsmanship, who trained under Shinzaemon. He is the 8th to die while fighting with literally every object he has in hand in front of his student. Masataka Kubota) – A young samurai disciple of Hirayama, untested in battle but with unwavering devotion and skills to match. He is the 7th to die while watching his master fight.
* Shimada Shinrokurō (Takayuki Yamada) – Shinzaemons nephew, he has strayed from Bushido to become a gambler and a womanizer. Bored and ashamed, he joins the mission to redeem himself. A deleted scene at the end shows him returning home to his wife.
* Ishizuka Rihei (Kazuki Namioka) – A skilled and courageous samurai who joined the mission with Sahara. He is the 2nd to die, from his wounds in front of Otake.
* Sahara Heizō (Arata Furuta) – An elder ronin, battle scarred and hardened, who favors the yari (spear) over the katana (sword). He is the 10th to die shortly after his spear breaks. Like Otake his death is off screen.
* Kiga Koyata (Yūsuke Iseya) – A hunter who is found suspended in a cage in the forest as a punishment for seducing his bosss wife and aids the assassins in finding a route to Ochiai as well as in combating the enemy samurai. He appears to die after being stabbed through the throat by Naritsugus sword. After the battle, he appears to Shinrokuro unharmed. It is suggested that he is a goblin, or yōkai, in human form.

==Production==
The film was produced through Nakazawas entity Sedic International and Thomas Recorded Picture Company. Nakazawa had previously worked with Miike on Sukiyaki Western Django, both Young Thugs movies, Andromedia, Yakuza Demon, and The Bird People in China. 

Principal photography began in July 2009 on a large open-air set in Tsuruoka in the Yamagata Prefecture, northern Japan.  On the advent of production, Thomas said he was pleased to be again working with "wonderful Japanese filmmakers like Toshiaki Nakazawa and Takashi Miike, whose work speaks for itself as being amongst the most successful and innovative coming from Japan."  Nakazawa replied that he would like Thomas "to wear a sword also, and with one more assassin, together we will send out the fourteen assassins over there."  The film wrapped in September 2009.   

==Release==
Thomas London-based company HanWay Films is handling international sales, and launched the film at the 2009 Cannes Film Festival.  Toho had prebought the rights to distribute the film in Japan.    The film competed for the Golden Lion at the 67th Venice International Film Festival.   

The film was released on VOD, and subsequently DVD and Blu-ray Disc in the United States on 5 July 2011 by Magnet Releasing, an arm of Magnolia.

==Reception== CGI effects. blockbuster action films lack.   Ebert later included it in his "Best Films of 2011" list as an addendum to his top 20. 
 Time Out polled several film critics, directors, actors and stunt actors to list their top action films.  13 Assassins was listed at 94th place on this list. 

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 