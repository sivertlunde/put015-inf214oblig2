Mo Duniya Tu Hi Tu
{{Infobox film
| name           = Mo Duniya Tu Hi Tu
| image          = Mo Duniya Tu Hi Tu.jpg
| caption        = Poster
| director       = Sudhakar Basanta
| producer       = Soumya Ranjan Patnaik
| writer         = Debasish Das
| starring       = Anubhav Mohanty, Barsha Priyadarshini, Siddhanta Mahapatra    
| music          = Malay Mishra
| cinematography =
| editing        = Rathod Club
| studio         = Manjari Movies
| distributor    = Manjari Movies
| released       =  
| runtime        = 
| country        = India
| language       = Oriya
| budget         = 
| gross          = 
}}
Mo Duniya Tu Hi Tu is an Oriya drama and romance film released on January 11, 2013.  Starring Anubhav, Barsha and Minaketan in pivotal roles with original soundtrack by Malay Misra.    It was a remake of Tamil film Unnidathil Ennai Koduthen.

==Synopsis==
Heera and his uncle and are petty thieves. One day while escaping from the police they hide in a house, Payal who is a governess in that house taking care of three children when remaining all went out of town. She locks them in her kitchen for one week, that one week relation with Payal changes entire lifestyle of Heera, he starts a new life and slowly falls in love with Payal. Meanwhile before leaving the house, Heeras uncle thefts Payalss dairy, Heera starts reading the dairy.

Payal is an illegitimate daughter of Lawyer, whose house she is living as a maidservant along with her step mother, her sister, sisters husband and their three children without knowing she is Lawyerss daughter. When one  of her relative Akash  comes from aboard , Priya likes him and intend to marry Akash, but Akash needs times  to decide. After reading this Heera decides not to express his love and from that day he also makes a habit of writing dairy. Next day when he goes to return the dairy a conflict arises to Payal because of him, she has been thrown out of the house due to confusion. Heera decides to take care of Payal, and builds her career as singer.  Once when Payal gone to foreign tour they blame robbery on Heera and sends him  away from the house. Payal returns from the tour, but she does not believe that Heera is a thief and she starts reading his dairy which Heera  forgot to take and understands how much love Heera hidden in his heart for her.

At last Payals  family members are making of marriage arrangements of Akash & Payal.  In the funtion even Heera attends the function, hidden in public, but Payal sees him, she acknowledges her entire success at his feet and decides to marry him, even Akash also appreciates her decision. Finally, movie ends with marriage of Heera & Payal.

==Cast==
* Anubhav Mohanty	... 	Hira  	
* Barsa Priyadarshini	... 	Payal
* Minaketan Das		
* Siddhanta Mahapatra	...     Guest appearance	
* Salil Mitra	... 	Hiras uncle
* Prutiviraj Nayak 		
* Jiban Panda		
* Pratibha Panda 
* Debashis Patra   ... Akash

==Review==
The film generally received positive reviews from critics. IncredibleOdisha gave it 4 out 5 star in the review and concluded "Mo Duniya Tu Hi Tu is a beautiful film with a social message at the end. Surprise packet of the movie is Salil’s comedy. Barsha Priyadarshini acted very well in emotional scenes. Anubhav scored well in comedy scenes. Sidhant’s performance is also very good."  FullOrissa gave it 4.3 out of 5 and commented "Though the film is new and fresh, its unfair to write about the story. But here I would like to mention that the film is New Year gift from Megastar Anubhav to all his fans. He emotes excellent in sentimental sequences and he surely rocks the viewers in songs and dialogues. Brasha Priyadarshini has a tailor-made role and she has really used it to show her acting skills". 

==Soundtrack==
The Music for the film is composed by Malay Misra
{| class="wikitable sortable" style="text-align:center;"
|-
! Song
! Lyrics
! Singer(s)

|- Mo Duniya Bappi Lahiri, Ira Mohanty 
|- Sathi Tora Roop Kumar Rathod, Ira Mohanty 
|- PKanhei Re Udit Narayan 
|- Tu Daki Ira Mohanty 
|- Kichhi Kichhi Debashis Mahapatra, Ira Mohanty 
|- To Sathe Ira Mohanty 
|-

|} 

==Box office==
The film did well in the box office and declared as hit.   

==Awards==
* Filmfare Awards East
** Best Oriya Film (Nominated)
** Best Oriya director(Nominated)- Sudhakar Basanta
** Best Oriya Actor (Nominated)-Anubhav Mohanty
** Best Oriya Actress (Nominated)-Barsa Priyadarshini  

==References==
 

==External links==
*  

 
 
 
 