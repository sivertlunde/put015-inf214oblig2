An Otter Study
 

{{Infobox film
| name           = An Otter Study
| image          = AnOtterStudy.jpg
| image_size     = 
| caption        = Screenshot from the film
| director       = 
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Kineto
| distributor    = 
| released       =  
| runtime        =
| country        = United Kingdom Silent
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
 1912 UK|British short black-and-white silent documentary film, produced by Kineto, featuring an otter in its natural habitat, including groundbreaking footage of underwater hunting scenes. The film provided a novel treatment of the creature, which had previously appeared on film only as the victim of hunt films, with the unique underwater footage, shot by a cameraman behind glass in a tank concealed on the bed of the river in the opening scene, and a concluding scene, excised from the surviving print, in which it escapes the hunters. It was long thought lost until footage from a 1920s Visual Education re-release of the film, re-edited under the supervision of Professor J Arthur Thomson of Aberdeen Universitys Natural History Department, was rediscovered. 

==References==
 

 
 
 
 
 


 
 