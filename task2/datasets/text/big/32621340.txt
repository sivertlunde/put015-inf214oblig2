The Client (2011 film)
{{Infobox film
| name           = The Client 
| image          = TheClient2011Poster.jpg
| caption        = Promotional poster for The Client
| film name      = {{Film name
| hangul         =  
| hanja          =  
| rr             = Uiroe-in
| mr             = Ŭiroein}}
| director       = Sohn Young-sung 
| producer       = Yu Jeong-hun Kim Jho Kwang-soo Shin Chang-gil
| writer         = Sohn Young-sung Lee Chun-hyeong
| starring       = Ha Jung-woo Jang Hyuk Park Hee-soon
| music          = Jo Yeong-wook
| cinematography = Choi Sang-ho
| editing        = Kim Sun-min
| studio         = Generation Blue Films
| distributor    = Showbox/Mediaplex 
| released       =  
| runtime        = 123 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
The Client ( ) is a 2011 South Korean courtroom thriller film starring Ha Jung-woo, Park Hee-soon and Jang Hyuk.

==Plot==
On his wedding anniversary, Han Chul-min (Jang Hyuk) drives into his apartment complex parking lot and sees a large crowd gathered by the entryway into his apartment. He enters holding a bouquet of flowers for his wife, instead he finds police officers scattered about collecting evidence. In his bedroom there is a large pool of blood dripping onto the floor from the bed, and his wife is nowhere to be seen. Han is then handcuffed, arrested and taken into police custody for her murder.

Prosecutor Ahn Min-ho (Park Hee-soon) takes charge of prosecuting the Han murder case. He has little doubt in the guilt of Chul-min. Confirming his suspicions that Han was arrested as the prime suspect in a serial murder case, but later released on insufficient evidence.
 
Jang Ho-won (Sung Dong-il), an investigator, brings the case of Han to defense lawyer Kang Sung-hee (Ha Jung-woo). He informs Kang that the alleged murder victims body was never discovered, the police have yet to find any direct evidence connecting Han to the murder of his wife and his arrest is based on circumstantial evidence. Han, who works at a film laboratory, has no fingerprints as they are erased from the strong chemicals he handles everyday. Convinced that Han is not guilty, Kang takes the case and applies in court for a jury trial and goes through a series of legal clashes against rival prosecutor Ahn. The case gets even more complex as details about the mysterious life of Hans wife are unveiled. 

==Cast==
* Ha Jung-woo - Kang Sung-hee (defense lawyer)
* Park Hee-soon - Ahn Min-ho (Kangs rival prosecutor)
* Jang Hyuk - Han Chul-min (client)
* Sung Dong-il - Jang Ho-won (Kangs investigator)
* Kim Sung-ryung - Kangs paralegal
* Jung Won-joong - Chief public prosecutor
* Yoo Da-in - Seo Jung-ah (murder victim and Chul-mins wife)
* Park Hyuk-kwon - Detective Seo
* Ju Jin-mo - judge
* Yeh Soo-jung - Jung-ahs mother
* Hwang Byeong-guk - detective in charge
* Min Bok-gi - Professor Choi 
* Yoo Soon-woong - elderly man at the store
* Bae Sung-woo - Prosecutor Park 
* Bae Yun-beom - Prosecutor Bae 
* Park Seong-geun - ranger
* Park Seong-yeon - Jung-ahs co-worker
* Yu Ha-jun - forensic team member
* Choi Hee-jin - blood researcher
* Lee Chang-hun

==Reception==
The film attracted 826,287 admissions in its first week of release  and till the end of 18 October achieved a total of 2.1 million  It ranked #2 and grossed   in its first week of release  but by the end of the second week rose to #2 with a gross of    and grossed a total of   after seven weeks of screening. 

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 
 
 
 
 