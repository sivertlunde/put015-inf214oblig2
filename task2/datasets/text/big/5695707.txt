The Dancing Masters
{{Infobox film
| name           = The Dancing Masters
| image          = L&H_Dancing_Masters_1943.jpg
| caption        = Theatrical release poster Malcolm St. Clair
| producer       = Lee S. Marcus
| writer         = George Bricker (story)   Scott Darling (screenplay) Robert Bailey   Matt Briggs   Margaret Dumont   Allan Lane   Hank Mann   Robert Mitchum| music          = Arthur Lange
| cinematography = Norbert Brodine
| editing        = Norman Colbert
| distributor    = 20th Century Fox
| released       = November 19, 1943
| runtime        = 63 30"
| country        = United States
| language       = English
| budget         =
}}

The Dancing Masters is a 1943 Laurel and Hardy feature film. The plot involves the team running a ballet school, and getting involved with an inventor. A young Robert Mitchum has an uncredited cameo role as a fraudulent insurance salesman.

==Plot==
Dancing instructors Laurel and Hardy decide to help a young inventor promote a new invisible ray gun that will help conquer the Germans during World War II.

==Cast==
* Stan Laurel&nbsp;– Stanley
* Oliver Hardy&nbsp;– Oliver
* Trudy Marshall&nbsp;– Trudy Harlan Robert Bailey&nbsp;– Grant Lawrence Matt Briggs&nbsp;– Wentworth Harlan
* Margaret Dumont&nbsp;– Louise Harlan
* Allan Lane&nbsp;– George Worthing

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 


 