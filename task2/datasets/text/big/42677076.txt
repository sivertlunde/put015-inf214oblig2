Shravana Banthu
{{Infobox film|
| name = Shravana Banthu
| image = 
| caption =
| director = Singeetham Srinivasa Rao Rajkumar  Urvashi   Srinath 
| producer = Kameshwara Rao
| music = M. Ranga Rao
| cinematography = S. V. Srikanth
| editing = P. Bhaktavatsalam
| studio = Chandrakala Art Enterprises
| released =  
| runtime = 149 minutes
| language = Kannada
| country = India
| budget =
}}
 Kannada romantic Urvashi and Srinath playing the pivotal roles.  The film, produced by P. A. Kameshwara Rao, deals with reincarnation as the subject where the lead roles get killed for their romance and then they are shown to be reborn again and fall in love yet again. The film was received exceptionally well at the box-office and was one of the biggest hits of 1984. The dialogues and lyrics were written by Chi. Udaya Shankar.

== Cast == Rajkumar
* Urvashi
* Srinath
* K. S. Ashwath  Leelavathi
* Thoogudeepa Srinivas
* Shivaram
* Uma Shivakumar
* M. S. Umesh
* Vishwanath
* Advani Lakshmi Devi

== Soundtrack ==
The music was composed by M. Ranga Rao to the lyrics of Chi. Udaya Shankar.  All the songs were received exceptionally well and are considered evergreen hits of Kannada cinema.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Hosa Baalina Rajkumar
| lyrics1 = Chi. Udaya Shankar
| length1 = 
| title2 = Baanina Anchinda Bande
| extra2 = Rajkumar (actor)|Rajkumar, Vani Jayaram
| lyrics2 = Chi. Udaya Shankar
| length2 = 
| title3 = Ide Raagadalli
| extra3 = Rajkumar (actor)|Rajkumar, Vani Jayaram
| lyrics3 = Chi. Udaya Shankar
| length3 = 
| title4 = Shravana Maasa Bandaaga
| extra4 = Rajkumar (actor)|Rajkumar, Vani Jayaram
| lyrics4 = Chi. Udaya Shankar
| length4 = 
| title5 = Mary Mary Mary Rajkumar
| lyrics5 = Chi. Udaya Shankar
| length5 = 
}}

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 