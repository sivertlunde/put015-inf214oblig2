The Cry (1963 film)
 
{{Infobox film
| name           = The Cry
| image          = Zdenek zieglar-The Cry (1963 film).jpg
| caption        = Film poster
| director       = Jaromil Jireš
| producer       = 
| writer         = Ludvík Aškenazy Jaromil Jireš
| starring       = Eva Límanová
| music          = 
| cinematography = Jaroslav Kučera
| editing        = Jiřina Lukešová
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

The Cry ( ) is a 1963 Czechoslovak drama film directed by Jaromil Jireš. It was entered into the 1964 Cannes Film Festival.    It is often described as the first film of the Czechoslovak New Wave, a movement known for its dark humor, use of non-professional actors, and "art-cinema realism".  The films events are ambiguous, leaving it to the viewer to determine whether the telling is objective or from a characters point of view (and if so, whose). 

==Cast==
* Eva Límanová as Ivana
* Josef Abrhám as Slávek
* Eva Kopecká
* Jiří Kvapil
* Slávka Procházková
* Hana Talpová

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 