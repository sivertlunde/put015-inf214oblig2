Piece of Cake (manga)
{{Infobox animanga/Header
| name            = Piece of Cake
| image           = 
| caption         = 
| ja_kanji        = ピース オブ ケイク
| ja_romaji       =  romance 
}}
{{Infobox animanga/Print
| type            = manga
| title           = 
| author          = George Asakura
| illustrator     = 
| publisher       = Shodensha
| publisher_en    = 
| demographic     = josei manga|josei
| imprint         = 
| magazine        = Feel Young
| magazine_en     = 
| published       = 
| first           = 2003
| last            = 2008
| volumes         = 5
| volume_list     = 
}}
{{Infobox animanga/Print
| type            = manga
| title           = Piece of Cake: Bangai-hen Piece 1
| author          = George Asakura
| illustrator     = 
| publisher       = Shodensha
| publisher_en    = 
| demographic     = 
| imprint         = 
| magazine        = Feel Young
| magazine_en     = 
| published       = 
| first           = March 7, 2015
| last            = 
| volumes         = 
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = 
| director        = Tomorowo Taguchi
| producer        = 
| writer          = Kōsuke Mukai
| music           = Otomo Yoshihide
| studio          = 
| licensee        = 
| released        =  
| runtime         = 
}}
  romance josei manga|josei manga series written and illustrated by George Asakura. It was published by Shodensha from 2003 to 2008 on Feel Young magazine. The first two volumes were published in French by Asuka.  A spin-off (media)|spin-off manga began in March 2015.  A live action film adaptation directed by Tomorowo Taguchi is scheduled for release on September 5, 2015 in Japan.    

==Characters==
*Shino Umemiya 
*Kyōshirō Sugahara 
*Akari Narita 

==Cast==
*Mikako Tabe 
*Gō Ayano 
*Tori Matsuzaka 
*Fumino Kimura 
*Kaoru Mitsumune 
*Tasuku Emoto 
*Masaki Suda 
*Tomoya Nakamura 
*Tamae Andō 
*Ryū Morioka 
*Kankurō Kudō 
*Ryūichi Hiroki 
*Kazunobu Mineta 

==Volumes==
*1 (May 8, 2004) 
*2 (May 20, 2005) 
*3 (July 7, 2006) 
*4 (March 8, 2007) 
*5 (February 7, 2009) 

==References==
 

==External links==
*   
* 
* 

 
 
 
 
 
 

 
 

 