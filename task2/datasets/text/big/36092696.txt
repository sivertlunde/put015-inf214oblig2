Mickey Magnate
{{Infobox film
| name           = Mickey Magnate
| image          = 
| image_size     = 
| caption        = 
| director       = Márton Keleti
| producer       = 
| writer         = Károly Bakonyi (play)   Andor Gábor (play)   István Békeffy
| narrator       = 
| starring       = Miklós Gábor   Ági Mészáros   Marika Németh   János Sárdy
| music          = Albert Szirmai  Szabolcs Fényes (recasts) 
| editing        = Sándor Zákonyi
| cinematography = István Eiben 
| studio         = Magyar Filmgyártó Nemzeti Vállalat
| distributor    = 
| released       = 10 January 1949
| runtime        = 95 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian comedy film directed by Márton Keleti and starring Miklós Gábor, Ági Mészáros and Marika Németh. It was based on a popular stage musical comedy by Károly Bakoni and Andor Gábor which had previously been turned into the 1916 silent film Miska the Magnate directed by Alexander Korda. 

==Main cast==
*  Miklós Gábor - Miska 
* Ági Mészáros - Marcsa 
* Marika Németh - Rolla 
* János Sárdy - Baracs Pista, mérnök 
* Kálmán Latabár - Pixi 
* Árpád Latabár - Mixi 
* Mária Sulyok - Korláthyné grófnõ 
* Hilda Gobbi - Nagymama 
* László Kemény - Korláthy gróf 
* Árpád Lehotay - Id. Baracs 
* Sándor Pécsi - Biró 
* Sándor Pethes - Eleméry gróf 

==References==
 

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.

==External links==
* 

 

 
 
 
 
 
 

 