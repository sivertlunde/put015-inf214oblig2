The Masked Gang: Cyprus
{{Infobox film
| name           = The Masked Gang: Cyprus
| image          = MaskedGangCyprusFilmPoster.jpg
| caption        = Theatrical poster Murat Aslan
| producer       = Murat Akdilek
| writer         = Murat Aslan
| starring       = Mehmet Ali Erbil Şafak Sezer Peker Açıkalın Melih Ekener Cengiz Küçükayvaz Atilla Saruhan Seray Sever Deniz Akkaya Hakan Ural
| cinematography = Soykut Turan
| editing        = 
| music          = Cem Erman
| studio         = Arzu Film Fida Film UIP Maxximum Film
| released       =  
| runtime        = 98 mins
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = US$7,006,849
}}
 Turkish film of 2008.

==Synopsis==
After an unsuccessful bank robbery, as well as the failure of a spontaneous ambush upon the oil plant in Iraq, the clumsiest comedy heroes of Turkish Cinema finally decide to retire from their small criminal lives. However, the dignified career as exterminator suffers many infamous perils, as the lovable idiots inhale more insecticide than the insects themselves. Thus the doctor prescribes a vacation at the seaside. But then there is Rocky Selim, a wicked crook from the underworld, disguised as an honorable businessman, who is planning a perfect casino robbery in North Cyprus. His plan is flawless. Yet, he is desperately in need of a criminal gang, to carry out his plan. As a fortunate coincidence, the paths of the masked gang overlap with Rocky Selim, who hires them instantly. As usual, with no preparations at all, overzealous and bustling, the masked gang follows the call of the justice a bit too spontaneously, and starts implementing the plans of Rocky Selim. Surely, with the methods of complete madmen.

==Release==
The film opened on general release in 350 screens across Turkey on   at number two in the Turkish box office chart with a worldwide opening weekend gross of $1,264,426.   

{| class="wikitable sortable" align="center" style="margin:auto;"
|+ Opening weekend gross
|-
!Date!!Territory!!Screens!!Rank!!Gross
|-
|  
| Turkey
| 350
| 2
| $1,264,426
|-
|  
| Belgium
| 5
| 11
| $101,042
|-
|  
| Germany
| 54
| 10
| $407,625
|-
|  
| Austria
| 6
| 8
| $54,390
|-
|  
| United Kingdom
| 1
| 33
| $12,835
|}

==Reception==
The movie reached number one at the Turkish box office and was the sixth highest grossing Turkish film of 2008 with a total gross of $5,575,199. It remained in the Turkish box-office charts for twenty-six weeks and made a total worldwide gross of $7,006,849. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 