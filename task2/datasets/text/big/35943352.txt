The Repentant
{{Infobox film
| name           = The Repentant
| image          = The Repentant (2012 film).jpg
| caption        = Film poster
| director       = Merzak Allouache
| producer       = 
| writer         = Merzak Allouache
| starring       = Ali Mahfiche   Xavier Thibault   Carole Verner   Julien Perez
| music          = 
| cinematography = Mohamed Tayeb Laggoune
| editing        =  Sylvie Gadmer
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Algeria
| language       = Arabic
| budget         = 
}}
 17th International Film Festival of Kerala. 

==Plot==
In the high flatlands  of Algeria, Islamic terrorists continue to spread terror..Rashid, a young jihadist leaves the mountains to return to his village.In keeping with the law of "pardon and national harmony", he has to surrender to the police and give up his weapon. He thus receives amnesty and becomes a "repenti". But the law cannot erase his crimes, and for Rashid, it is the beginning of a one way journey of violence, secrets and manipulation

==Cast==
* Nabil Asli as Rashid
* Adila Bendimerad
* Khaled Benaissa

==Awards== 17th International Film Festival of Kerala 
*the FIPRESCI Award for Best Asian Film 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 