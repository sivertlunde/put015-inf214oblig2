The Doll (1968 film)
{{Infobox film name           =The Doll image          =Beata_Tyszkiewicz_Polish_actress.jpg caption        =Beata Tyszkiewicz in The Doll, 1968 director       =Wojciech Jerzy Has producer       = writer         =Wojciech Jerzy Has based on a novel by Bolesław Prus starring       =Beata Tyszkiewicz Mariusz Dmochowski music          =Wojciech Kilar cinematography =Stefan Matyjaszkiewicz studio         =Wytwórnia Filmów Fabularnych (Łódź) distributor    = released       =  runtime        =151 mins. country        =Poland awards         = language       =Polish budget         =
}}
 1968 Poland|Polish film directed by Wojciech Jerzy Has. 
 The Doll by Bolesław Prus, which is regarded by many as one of the finest Polish novels ever written. The influence of Émile Zola is evident, and some have compared the novel to Madame Bovary by Gustave Flaubert; both were Pruss contemporaries. The movie, however, may be more compared to Stendhals The Red and the Black.

 The Doll constitutes a panorama of life in Warsaw between 1878 and 1879, and at the same time is a subtle story of three generations of Polish idealists, their psychological complications, their involvement in the history of the nineteenth century, social dramas, moral problems and the experience of tragic existence. At the same time this story describes the disintegration of social relationships and the growing separation of a society whose aristocratic elite spreads the models of vanity and idleness. In the bad air of a backward country, anti-Semitic ideas are born, valuable individuals meet obstacles on their way, and scoundrels are successful. 

This poetic love story follows a nouveau riche merchant, Stanislaw Wokulski, through a series of trials and tribulations occasioned by his obsessive passion for an aristocratic beauty, Izabela Lecka (played by Beata Tyszkiewicz).

==Plot==
As a descendant of an impoverished Polish noble family, young Wokulski is forced to work as a waiter at Hopfers, a Warsaw restaurant, while dreaming of a life in science. After taking part in the failed 1863 Uprising against Tsarist Russia, he is sentenced to exile in Siberia. On eventual return to Warsaw, he becomes a salesman at Mincels haberdashery. Marrying the late owners widow (who eventually dies), he comes into money and uses it to set up a partnership with a Russian merchant he had met while in exile. The two merchants go to Bulgaria during the Russo-Turkish War (1877–78)|Russo-Turkish War, and Wokulski makes a fortune supplying the Russian Army. The enterprising Wokulski now proves a romantic at heart, falling in love with Izabela, daughter of the vacuous, bankrupt aristocrat, Tomasz Łęcki. In his quest to win Izabela, Wokulski begins frequenting theatres and aristocratic salons; and to help her financially distressed father, founds a company and sets the aristocrats up as shareholders in his business. The indolence of these aristocrats, who secure with their pensions, are too lazy to undertake new business risks, frustrates Wokulski. His ability to make money is respected but his lack of family and social rank is condescended to. Because of his "help" (in secret) to Izabelas impecunious but influential father, the girl becomes aware of his affection. In the end she consents to accept him, but without true devotion or love.

==References==
 

==External links==
* 

 

 
 
 
 
 
 