Lukket avdeling
{{Infobox film
| name           = Lukket avdeling
| image          = 
| image size     = 
| caption        = 
| director       = Arnljot Berg
| producer       = Harald Ohrvik
| writer         = 
| narrator       = 
| starring       = Roy Bjørnstad
| music          = Freddy Lindquist
| cinematography = Knut Gløersen
| editing        = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}

Lukket avdeling is a 1972 Norwegian drama film directed by Arnljot Berg. It was entered into the 22nd Berlin International Film Festival.   

==Cast==
* Roy Bjørnstad - Vestfold
* Carsten Byhring - Vålerenga
* Vegard Hall - Gamlingen
* Erik Hivju - Harry
* Arne Lindtner Næss - Den tause
* Per Tofte - Playboy
* Carsten Winger - Hamlet
* Gard Øyen - Guttungen
* Eva von Hanno - Pleiersken
* Ole-Jørgen Nilsen - Paul Paulus
* Per Theodor Haugen - Overlegen
* Eilif Armand - Den tilknappede
* Willie Hoel - En pasientvenn
* Aud Schønemann - Kona til Vålerenga
* Freddy Lindquist - Musikeren
==References==
 

==External links==
* 

 
 
 
 
 
 
 
 