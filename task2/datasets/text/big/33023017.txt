The Trick in the Sheet
{{Infobox film
| name           = The Trick in the Sheet
| image          = TheTrickInTheSheet2010Poster.jpg
| caption        = Film poster
| director       = Alfonso Arau
| producer       = Luis Ángel Bellaba Maria Grazia Cucinotta Riccardo Neri Giulio Violati
| writer         = Giovanna Cucinotta Francesco Costa Maria Grazia Cucinotta Chiara Clini Romina Nardozi
| starring       = Maria Grazia Cucinotta Geraldine Chaplin Anne Parillaud Primo Reggiani
| music          = Maria Entraigues Ruy Folguera	 	
| cinematography = Vittorio Storaro
| editing        = Consuelo Catucci Roberto Perpignani	 	
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy Spain
| language       = Italian
| budget         =
| gross          = 
}}
The Trick in the Sheet (Original title:Limbroglio Nel Lenzuolo) is a 2010 Italian romantic comedy film directed by Alfonso Arau. It stars Maria Grazia Cucinotta, Geraldine Chaplin, Anne Parillaud and Primo Reggiani. It was released in Italy on 18 June 2010.

==Plot==
In a small town in southern Italy in 1905, theatrical performances are accompanied by short silent film. An audience flees in terror as an image of a train tears towards them, projected on a makeshift screen, a large white sheet.

Federico (Reggiani), is fascinated by this new film technology and switches from a medical career to writing screenplays. He soon lands a gig as the director of a new short silent film. Although he is torn by the demands of those around him, his producer wants salacious storylines and his sister craves something more edifying. Federico decides to recreate the Biblical story of Susanna, where she nakedly bathes and is leered at by two older men 

Meanwhile, Marianna (Cucinotta) is a peasant sorceress, that finds her powers redundant against the new powers of film technology. Federico becomes attracted Marianna, but he is also mesmerized by Beatrice, a visiting writer (Parillaud). 

==Cast==
*Maria Grazia Cucinotta as Marianna
*Geraldine Chaplin as Alma
*Anne Parillaud as Beatrice
*Miguel Ángel Silvestre as Giocondo
*Angélica Aragón as Teresa
*Giselda Volodi as Elena
*Primo Reggiani as Federico
*Maria Entraigues as Opera singer

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 