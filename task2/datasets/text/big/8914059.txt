Soul of the Game
{{Infobox film
| name           = Soul of the Game
| image          =
| caption        =
| director       = Kevin Rodney Sullivan
| producer       = Robert Papazian
| writer         = Gary Hoffman (story)   David Himmelstein (teleplay)
| starring       = Blair Underwood Delroy Lindo Mykelti Williamson Edward Herrmann R. Lee Ermey  
| music          = Lee Holdridge
| cinematography = Sandi Sissel
| editing        = Victor Du Bois
| distributor    = HBO
| released       =  
| runtime        = 94 min.
| country        = United States English
| awards         =
| budget         =
}}
 1996 made-for-television movie about Negro league baseball.

The film starred  . The film depicts Paige and Gibson as the pitching and hitting stars, respectively, of the Negro Leagues in the period immediately following World War II. Robinson is an up-and-coming player on Paiges team, the Kansas City Monarchs.

==Synopsis==
Branch Rickey, played by Edward Herrmann, is the owner of the Brooklyn Dodgers who is determined to integrate Major League Baseball. He begins sending his scouts to Negro League games to find the best players. Rickey directs his scouts to look not only at playing ability but also at the players maturity and capacity to withstand the hostility that is sure to be directed at the first black player in the Major Leagues.

Robinson gradually comes to Rickeys notice both for his skills on the field and his personal background. Although Paige and Gibson are far more prominent, Rickey decides to pass them over, concerned about Paiges age and reports about Gibsons mental stability. Rickey makes history by signing Robinson to the first contract between a black man and a Major League Baseball franchise. This alienates Robinson from his two friends at first, until Paige enlists Robinsons help in getting Gibson temporarily released from a mental hospital so that the three men can all play in the annual exhibition game between the All-Stars of the Major Leagues and the Negro League. The game is rained out, but Paige and Gibson seem reconciled to Robinsons being signed ahead of them.

The movie concludes by showing scenes from Robinsons successful career with the Dodgers, as well as Paiges later signing by the Cleveland Indians. Gibson died from a brain aneurysm at the age of 35 before he could ever play a game in the Major Leagues. All three men are later inducted into the Baseball Hall of Fame.

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 


 
 