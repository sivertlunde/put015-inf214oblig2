Meatless Flyday
 
 
Meatless Flyday is a Merrie Melodies cartoon short produced by Warner Bros., and released on January 29, 1944. The cartoon was directed by Friz Freleng, written by Michael Maltese and animated by Jack Bradbury. The title is a pun on Meatless Friday, a reference to the traditional Roman Catholic practice of abstinence from meat on Fridays.

==Plot==
An overconfident and guffawing spider (voiced by Cy Kendall) spots his intended prey, a mute fly, on the ceiling, and indulges in various cat-and-mouse schemes to try to catch him for food, including painting a load of buckshot with "Kandy Kolor" and luring the fly to eat it and drawing him closer with a magnet, which only succeeds in attracting a set of metal cutlery which the spider has to dodge to save himself. He sees the fly on a wedding cake as a bride and dresses as the groom only to be blown up by a dynamite stick, left in blackface. Eventually, the spider catches his prey, and, when he is about to carve him up while singing the song "Would You Like to Take a Walk?", the fly points to a wall calendar giving the day as "Meatless Tuesday", a reference to food rationing during World War II. The frustrated spider runs to the United States Capitol and screams, "You cant do this to me! You cant, you cant!"

==See also==
*Looney Tunes and Merrie Melodies filmography (1940–1949)

==External links==
*  
*  

 
 
 
 


 