The Games (film)
{{Infobox film
| name           = The Games
| image          = games_movie_poster.jpg
| caption        = Promotional poster
| director       = Michael Winner
| producer       = Lester Linsk
| writer         = Erich Segal Elaine Taylor
| music          = Francis Lai
| cinematography = Robert Paynter
| editing        = Bernard Gribble
| distributor    = 20th Century Fox
| released       =  
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget = $4,895,000 
}} Hugh Atkinson directed by Michael Winner.
 marathon competitors at a fictitious Olympic Games in Rome, played by Michael Crawford, Ryan ONeal, Charles Aznavour, and Athol Compton. Elton John recorded one song (From Denver To L.A.) for the soundtrack.

To simulate vast crowds of people, thousands of life-sized dummies were placed in the stadiums seats.

==Cast==
* Michael Crawford as Harry Hayes, British competitor
* Ryan ONeal as Scott Reynolds, American competitor
* Charles Aznavour as Pavel Vendek, Czech competitor 
* Jeremy Kemp as Jim Harcourt  Elaine Taylor as Christine 
* Stanley Baker as Bill Oliver 
* Athol Compton as Sunny Pintubi, Australian competitor 
* Rafer Johnson as Commentator 
* Kent Smith as Kaverley 
* Sam Elliott as Richie Robinson 
* Mona Washbourne as Mrs. Hayes 
* Reg Lye as Gilmour 
* June Jago as Mae Harcourt 
* Don Newsome as Cal Wood

==Production==
Athol Compton was an aboriginal postman who had never acted before being cast in the film. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 


 