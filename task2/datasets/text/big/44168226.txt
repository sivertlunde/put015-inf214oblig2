The Marshal's Daughter
{{Infobox film
| name           = The Marshals Daughter
| image          = The Marshals Daughter poster.jpg
| alt            = 
| caption        = Theatreical release poster
| director       = William Berke
| producer       = Ken Murray 
| screenplay     = Bob Duncan
| starring       = Laurie Anders Hoot Gibson Ken Murray Preston Foster Johnny Mack Brown
| music          = Darrell Calker
| cinematography = Jack MacKenzie
| editing        = Reg Browne
| studio         = Harris/Murray
| distributor    = United Artists
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Marshals Daughter is a 1953 American action film directed by William Berke and written by Bob Duncan. The film stars Laurie Anders, Hoot Gibson, Ken Murray, Preston Foster and Johnny Mack Brown. The film was released on June 26, 1953, by United Artists.  

==Plot==
 

==Cast== 
*Laurie Anders as Laurie Dawson
*Hoot Gibson as Marshal Ben Dawson
*Ken Murray as Smiling Billy Murray
*Preston Foster as Preston Foster
*Johnny Mack Brown as Johnny Mack Brown
*Jimmy Wakely as Jimmy Wakely
*Buddy Baer as Buddy Baer
*Harry Lauter as Russ Mason
*Robert Bray as Anderson 
*Bob Duncan as Trigger Gans
*Pamela Ann Murray as Baby Laurie Dawson
*Tex Ritter as Background Singer

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 