Jaago - Dare To Dream
 
{{Infobox film
| name           = Jaago – Dare To Dream
| image          = Jaago - Dare To Dream Poster.jpg
| image_size     = 250px
| caption        = 
| director       = Khijir Hayat Khan
| producer       = Adnan Karim
| writer         = Khijir Hayat Khan
| starring       = Ferdous Ahmed Afsana Ara Bindu Arefin Shuvo Tariq Anam Ronok Hasaan Jatika Jyoti
| music          = Arnab
| cinematography = Sachi Chowdhury
| editing        = Samir Ahmed
| distributor    = Interspeed
| released       = 15 January 2010
| runtime        = 150 min
| country        = Bangladesh Bengali
| Genre          = Thriller
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
| imdb_id        = 1895416
}}
Jaago ( ) is a 2010 Bangladeshi sports drama film written and directed by Khijir Hayat Khan.  The film is produced by Adnan Karim. The film features Ferdous Ahmed and Afsana Ara Bindu in lead roles and with supporting Arifin Shuvoo, Tariq Anam Khan, Rownak Hasan, Fs Nayeem and many more. It is the first Bangladeshi film based entirely on football.  The story of "Jaago" is said to be inspired by the Swadhin Bangla Football Dol, who played a vital role during the Liberation War (1971) by raising funds through charity matches and encourage the morale of the freedom fighters.

== Cast ==
* Ferdous Ahmed
* Afsana Ara Bindu
* Arifin Shuvoo
* Tariq Anam Khan
* Rownak Hasan
* Jatika Jyoti
* Fs Nayeem

==Awards and nominations ==
Jaago received fifteen nominations and received six awards in the following categories: Best Film (Critics Choice); Best Director (Khijir Hayat Khan); Best Music Director (Arnob); Best Actress (Afsana Ara Bindu|Bindu); Best Playback Singer Male (Kumar Bishwajit for "Jhum Jhum Brishti") and Best Sound Engineer (Bappi Rahman). 

== Music ==
The film includes a total of five songs written by Arnob & Anup Mukhopaddhay and composed by Arnob. The film features Arnob, Bappa Mazumder, Kumar Bishwajit, Zohad, Audit, Milon Mahmud and others as playback singers.

{{Infobox album|  
  Name        = Jaago – Dare To Dream
|  Type        = Soundtrack
|  Artist      = Kumar Bishwajit, Arnob, Zohad, Audit and more
|  Cover       = Jaago Soundtrack album.png
|  Released    = 2010
|  Recorded    = Feature film soundtrack
|  Length      = 36:06 min
|  Label       = 
|  Producer    = 
|  Reviews     =
}}

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = yes
| collapsed = no
| total_length =  
| title1 = Jaago (Version 1)
| extra1 = Arnob, Bappa Mazumder & Zohad
| music1 = Arnob
| length1 = 4:46
| title2 = Shomoy Churi
| extra2 = Arnob, Milon Mahmud, Audit & Zohad
| music2 = Arnob
| length2 = 4:03
| title3 = Keno Chole Gele Dure
| extra3 = Srabonti Ali & Arnob
| music3 = Arnob
| length3 = 3:17
| title4 = Jhum Jhum Jhum Brishti Kona
| music4 = Arnob
| length4 = 3:53
| title5 = Jaago (slow)
| extra5 = Arnob
| music5 = Arnob
| length5 =  2:40
| title6 = Pothe Cholte
| extra6 = Rupom
| music6 = Arnob
| length6 = 3:41
| title7 = Shomoy Churi (Slow)
| extra7 = Arnob
| music7 = Arnob
| length7 =  3:58
| title8 = Jaago (Version 2)
| extra8 = Audit
| music8 = Arnob
| length8 = 5:58
| title9 = Jhum Jhum Jhum Brishti (Instrumental)
| extra9 =
| music9 = Arnob
| length9 = 3:50
}}

==See also==
* Eito Prem
* Rani Kuthir Baki Itihash

==References==
 

==External links==
*  
* Jaago on  

 
 
 
 
 
 
 


 
 
 