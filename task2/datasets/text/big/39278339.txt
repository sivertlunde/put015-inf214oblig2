Mission: Impossible – Rogue Nation
{{Infobox film
| name           = Mission: Impossible – Rogue Nation
| image          = Mission Impossible – Rogue Nation poster.jpg
| caption        =  Theatrical teaser poster
| alt            =
| director       = Christopher McQuarrie
| producer       = {{Plainlist|
* Tom Cruise
* J. J. Abrams
* David Ellison
}}
| screenplay     = {{plainlist|
* Drew Pearce
* Will Staples
}}
| based on       =  
| starring       = {{Plainlist|
* Tom Cruise
* Jeremy Renner
* Simon Pegg Rebecca Ferguson
* Ving Rhames
* Sean Harris
* America Olivo
* Alec Baldwin
}} Joe Kraemer
| cinematography = Robert Elswit
| editing        = Eddie Hamilton
| production companies = {{Plainlist|
* Skydance Productions
* Bad Robot Productions
* TC Productions
}}
| distributor    = Paramount Pictures
| released       = July 31, 2015
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} IMF Agent Ethan Hunt. It is produced by Tom Cruise, J. J. Abrams, and David Ellison of Skydance Productions.

Filming began on August 21, 2014, in Vienna, Austria, and concluded on March 12, 2015. The film is scheduled to be released in IMAX theaters worldwide and in North America by Paramount Pictures on July 31, 2015.

==Premise==
The IMF agency comes under threat from the Syndicate, a near-mythical organization of assassins and rogue operatives who kill to order. Faced with the IMFs disbandment, Ethan Hunt assembles his team for their final and most difficult mission &mdash;to prove the Syndicates existence and bring the organization down by any means necessary.

== Cast ==
* Tom Cruise as Ethan Hunt
* Jeremy Renner as William Brandt
* Simon Pegg as Benji Dunn Rebecca Ferguson as Ilsa Faust
* Ving Rhames as Luther Stickell
* Sean Harris 
* America Olivo as a villianess
* Alec Baldwin as the Head of the CIA   
* Simon McBurney
* Zhang Jingchu

== Production ==

=== Pre-production ===
The film is directed by Christopher McQuarrie,  and Drew Pearce is writing the film.  Paramount Pictures and Skydance Productions have signed a deal with Tom Cruise to reprise the role of Ethan Hunt and produce.  Jeremy Renner is reported to return as William Brandt.  On November 13, 2013, a release date of December 2015 was announced.  In November 2013, Simon Pegg confirmed that he will reprise his role as Benji.  On November 22, the director tweeted that Robert Elswit would be director of photography for the film.  In May 2014, Will Staples was announced as working on the script for the fifth film.  Cruise confirmed the fifth installment of the film will shoot in London.  In June, Renner confirmed his return for the fifth installment.  The film will be shot in Vienna in August before moving to the United Kingdom. 
 Rebecca Ferguson was cast and Alec Baldwin was in talks for the fifth film.   Baldwin was confirmed to have joined the cast in August 2014,    and Ving Rhames was confirmed to be reprising his role of Luther Stickell.  On September 5, it was announced that Sean Harris was being negotiated with the studio for the villain role.  On October 2, Simon McBurney joined the cast of the film.    On October 6, Chinese actress Zhang Jingchu joined the films cast for a major role.    On March 22, 2015, Paramount revealed the films official title Mission: Impossible – Rogue Nation, along with a teaser poster and trailer. 

=== Filming === The Marrakesh Stadium, which was closed both days for the filming purpose.  On September 26, Cruise was reportedly filming scenes doing his own stunts in a BMW car in Kasbah of the Udayas in capital city Rabat. 

After more than a month of shooting in Austria and Morocco, the production started commencing the shoot in London on September 28.  On October 7, a trailer was seen carrying damaged BMW M3s from the set after filming in Morocco.  On October 10, Cruise and his stuntman Wade Eastwood were spotted during filming some scenes in Monaco,  lead actress Ferguson also spotted.  Filming of an action scene featuring Ethan Hunt climbing and fighting on the outside of a flying Airbus A400M Atlas took place at RAF Wittering in Cambridgeshire. Tom Cruise performed the sequence, at times suspended on the aircraft 5000 feet in the air, without the use of a stunt double.  On November 9, filming began on Southampton Water, while the crews were spotted arriving at Fawley Power Station before filming started.  On December 2, 2014, Cruise was almost hit by a double-decker bus while filming a scene in London. However, the bus missed him and he suffered no injuries.  On February 20, 2015, The Hollywood Reporter reported that filming was halted to give McQuarrie, Cruise, and an unknown third person (reportedly consultant Patrick Lussier) time to rework the films ending which was not satisfactory according to them.  Next day, McQuarrie confirmed via Twitter that the original ending was moving forward.  Some photos from the London set were also released.  Filming ended on March 12, 2015, confirmed by the directors tweet. 

=== Music === Joe Kraemer was hired to write the score for the film. 

== Release == James Bond Lotte will be releasing the film in Korea on July 30, 2015. 

=== Marketing ===
A teaser trailer for the film was released on March 22, 2015. The following day, a full length official trailer was released. 

== References ==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 