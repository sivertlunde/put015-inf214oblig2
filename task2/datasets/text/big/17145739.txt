The Restless Sex
{{Infobox film
| name           = The Restless Sex
| image          = Marion Davies in The Restless Sex by Robert W. Chambers Photoplay 1918.png
| image_size     = 180px
| caption        = Ad for film
| director       = Leon DUsseau Robert Z. Leonard
| producer       =
| writer         = Frances Marion
| based on       =  
| starring       = Marion Davies Ralph Kellard
| music          =
| cinematography =
| editing        = Famous Players-Lasky Corporation
| released       =  
| runtime        =
| country        = United States Silent (English intertitles)
| budget         =
}} silent drama Famous Players-Lasky Corporation name.

A copy of The Restless Sex is housed at the Library of Congress and Gosfilmofond. 

==Synopsis==
Mike Jill is a restless and adventurous young woman. She is in love with Jim, her foster brother and childhood sweetheart. But now, she also finds herself attracted to art student Oswald Grismer. Jim leaves for Paris to become an author. In this period, Mike and Oswald decide to marry each other. When Jim finds out, he returns home to win Mike over.

 
==Cast==
* Marion Davies - Mike Jill
* Ralph Kellard - Jim Cleland
* Carlyle Blackwell - Oswald Grismer Charles Lane - John Cleland
* Corinne Barker - Helen Davis
* Stephen Carr - Jim as a boy
* Vivienne Osborne - Marie Cliff
* Etna Ross - Mike as a child
* Robert Vivian - Chilsmer Grismer
* Athole Shearer - Extra (uncredited)
* Edith Shearer - Extra (uncredited)
* Norma Shearer - Extra (uncredited)

==Production==
Costume design for the film, which featured Egyptian-style costumes for the "Bal des Arts" scene, was done by Erté. {{cite book
 | authors =Jean-Marcel Humbert, Michael Pantazzi, and Christiane Ziegler
 | first =
 | authorlink =
 | title =Egyptomania: Egypt in Western Art, 1730 - 1930
 | publisher =National Gallery of Canada
 |year=1994
 | page=508
 | url =
 | isbn =0-8888-4-6363}}  He also worked on art direction with Joseph Urban.

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 