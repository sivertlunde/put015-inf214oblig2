Eddelu Manjunatha
 
{{Infobox film
| name           = Eddelu Manjunatha
| image          = Kannada film Eddelu Manjunatha poster.JPG
| caption        = Film poster
| director       = Guruprasad
| producer       = V. Sanath Kumar
| writer         = Guruprasad
| screenplay     = Guruprasad
| starring       = Jaggesh Yagna Shetty Tabla Nani
| narrator       = 
| music          = Anoop Seelin
| cinematography = Ashok V. Raman
| editing        = Narahalli Jnanesh
| studio         = Sanath Kumar Productions
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}

Eddelu Manjunatha ( ,  ) is a 2009 Indian Kannada language film written and directed by Guruprasad, and produced by journalist and director V. Sanath Kumar. Its stars Jaggesh and Yagna Shetty in the lead roles. Tabla Nani and V. Manohar appear in supporting in roles.
 Best Screenplay. Best Director Special Jury Award (Yagna Shetty). 

==Plot==
This film is about the life and times of a lazy and jobless middle aged man named Manjunatha ("Manja") who considers his life as an eventual existence rather than a practical, deserving and a capable one. His laziness is portrayed in the film as not a quality but as an ethic imbibed within his general thoughts and notions of the everyday world that surrounds him. Through his formative years his thinking becomes pragmatic in considering that livelihood can always be sought through alternate sources rather than being a puppet to how the world goes about through linear methods of gaining success and money. The other essentially important character in the film is a visually impaired person named Naani (played by Tabla Naani). Although being born blind, he wishes to be a film director. Naanis most positive aspect of the role is that he does not believe that his physical disability could stop him from achieving what he dreams, that is to be a film maker. The film is structured around the lengthy conversation, which happen in a captive lodge room, between Manja and Naani where each depict their ideologies and experiences and co-relate their thoughts. Being a Jaggesh film, the film is no where short in containing blatant satirical humour and constant metaphors. The characters of Manja and Naani form contrasting personalities - Manja being an unmotivated, lazy and irresponsible guy and Naani being optimistic and ambitious. Naani happens to meet Manja in a very interesting scenario. Even with their contrasting personalities, Manja and Naani get along together pretty well.

The other important, yet minimally portrayed, role is of Manjas wife Gowri (played by Yagna Shetty). Manja admittedly marries Gowri in the fact that he was getting a small house as a dowry. Gowri struggles to save her relationship while Manja is doused into the casual habits of alcohol, his influential "circle" of friends, betting, occasional petty thieving at random jobs, inability to sustain decent jobs, wife-bitching and other habits including schemes that eventually thicken the gap between living a moral and a meaningful life and being an incapable disloyal husband. However Gowris character is portrayed to be of a devout woman who honours the capacity of developing a more healthy family hood through her husband changing his ways some day or the other. But days and years go on and Manjas lifestyle remains unchanged much to the chagrin of Gowri.

The conversation now continues shifting from the lodge to Manjas home itself. After celebrating their freedom from the lodge with alcoholism that night they find themselves again captive within the house due to the help of the local inspector who assists Gowri to tackle Manjas unyielding ways. Naani then talks about the plot of his film, which was seemingly ignored by the producer with whom he had placed his trust upon (and for the reason with which he ended up being captive in the lodge). Manja, hearing of the simple story of Dr. Rajkumars pledged eyes and how they were now seeing a world through another person, is taken aback and applauds Naani for such a heartwarming plot and how the -Annavru-s fans would welcome such a movie. He motivates Naani with all success if Naani ever made the film by taking out his mothers prized 50 rupee note from the cupboard and giving it to Naani. He tells Naani that it was considered as a luck charm to any person that received it. He also happens to find a note in the cupboard that Gowri leaves behind (after her inability to tell him personally that night due to his inebriated state) to Manja conveying that she was now carrying and that he would soon become a father. Manjas personality suddenly defines a change after reading the news. He is unable to express his joy, apart from sharing it with Naani, at that moment being locked in his home. He tries calling Gowri but he doesnt get her on-line. In the midst of all this there concocts a life turning situation for Manja at that moment. His wife returns home struck in pain. Gowri had killed the developing child in her womb due to the burdensome worry which she concluded that she wasnt in a state to be able to maintain and grow a child while having such a lackluster and incapable husband. She perceived that it was best for the child to not come to life and face a deteriorated lifestyle. Naani leaves the house expressing his ill timed presence in the development of such an event. Manja is clipped between a moment of where he faced fresh joy like he had not known for a long long time where he believed that the child, who would be his Lakshmi (the Goddess of wealth), would change his life for the better and to another moment that his Lakshmi would not be happening. In his state of hopelessness he threatens Gowri as to what rights she held to kill his child. Gowri is throbbing in pain to be able to reply to his questions. In this delusion of Manja, Lakshmi -the unborn child, appears to him and speaks to him as to how ill fated she would have been to have been born as a daughter to such a father. Lakshmi says that Gowri, her mother, did not kill her and that Manja, her father, killed her and suggests to him that he could celebrate this occasion with his friends by drinking along with them. Manjas remorse knows no bounds. He reflects back Lakshmis words to Gowri and says to his wife that neither his own parents or his own wife or any of his gurus could ever be a guru to him, but the unborn dead child which will never happen in his existence was his ultimate guru to his final immediate realization of the value of life.

==Cast==
* Jaggesh as Manjunatha (Manja)
* Yagna Shetty as Gowri
* Tabla Nani as Nani
* V. Manohar
* A. S. Murthy
* Sadananda
* Veena Bhat
* Sulochana
* Sunitha
* Manjula
* Master Punarva
* Master Ravikiran Shastry
* Master Lokesh

==Production==
The dialogues, penned by Guruprasad, being very witty and sharp often reflect the societys dark sides. The non linear screenplay offers a refreshing film watching experience. The lead character Manjunatha is played by Jaggesh.
Ashok V Raman’s lens brings alive Guru’s script, his camera work gathers attention for his close ups. He has captured the entire film in small rooms and lanes.

==Soundtrack==
Background score and soundtrack are composed by Anoop Seelin. All the songs from soundtrack enjoyed frequent airplays in FM radio and T.V. music channels. Only two songs are used in the film.

{{Track listing headline = Eddelu Manjunatha : The Official Motion Picture Soundtrack extra_column = Singer(s) title1 = length1 = 4:52 title2 = length2 = 4:16 title3 = length3 = 3:03 title4 = length4 = 4:44 title5 = length5 = 2:02 title6 = length6 = 4:48 title7 = length7 = 4:16 title8 = length8 = 4:49
}}
 

==Awards==
;2009–10 Karnataka State Film Awards Best Screenplay – Guruprasad

;57th Filmfare Awards South Best Director – Guruprasad Special Jury Award – Yagna Shetty

==References==
 

==External links==
*  
*   at Oneindia.in
*   at Deccan Herald
*   at Thatskannada
*   at Sirigandha

 
 
 