Stepmom (film)
{{Infobox film
| name           = Stepmom
| image          = Stepmom.jpg
| alt            = 
| caption        = Theatrical release poster Chris Columbus Chris Columbus Ron Bass Julia Roberts Susan Sarandon Jessie Nelson Steven Rogers Karen Leigh Hopkins Ron Bass
| story          = Gigi Levangie
| starring       = Julia Roberts Susan Sarandon Ed Harris Jena Malone Liam Aiken Lynn Whitfield  Darrell Larson Mary Louise Wilson 
| music          = John Williams Donald M. McAlpine
| editing        = Neil Travis
| studio         = 1492 Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = $50 million  , the-numbers.com 
| gross          = $159,710,793 
}} Chris Columbus and starring Julia Roberts, Susan Sarandon, and Ed Harris. Sarandon won the San Diego Film Critics Society Award for Best Actress and Harris won the National Board of Review Award for Best Supporting Actor, sharing the win with his role in The Truman Show.

==Plot==
Jackie and Luke Harrison (Susan Sarandon and Ed Harris) are a divorced couple that are struggling to help their children Anna (Jena Malone) and Ben (Liam Aiken) be happy with this sudden change of lifestyle. This is far from easy, as Luke, an Lawyer|attorney, is living with his new girlfriend, Isabel Kelly (Julia Roberts), a successful fashion photographer several years his junior. Isabel tries very hard to make Anna and Ben feel comfortable and happy with her, but Anna repeatedly rejects her overtures while Ben, who loves Isabel (not as much as his actual mother), adds extra complication with his mischievous nature. Isabel behaves with contempt tempered by caution around Jackie, believing she overcompensates for her divorce by spoiling her children.

Conversely, Jackie, a former publisher turned stay-at-home mom, gives Isabel a cold reception, seeing her as an overly ambitious career woman. She also continues to harbour malice towards Luke (as can be seen in a confrontation about Isabel). After a long string of arguments and hurt feelings involving Isabel, Jackie and Anna, Luke proposes to Isabel, making her Anna and Bens soon-to-be official stepmother, which causes even more friction. However, Jackie is diagnosed with cancer, which is discovered to be incurable. She experiences a range of negative emotions, angry at the woman who she feels played a role in her broken family, and angry at the fact that after all of the sacrifices she made for her family, she will not even get to see her children grow up.

Isabel and Anna continue to disagree, shown most when Anna continually calls her father with Isabel answering, then hanging up. Isabel gets the children a golden retriever puppy, and Anna says she is allergic to dogs. Isabel apologizes and says that her father didnt tell her that. Later, she is shown outside her room, taking the dog inside, indicating that she lied about her allergy. Later, Isabel and  Anna begin to bond over painting. Later Jackie, having agreed to give Isabel one last chance to prove herself fit to care for the kids, asks Isabel to pick them up from school. Isabel agrees, then asks if she could take Anna to see a popular rock band that she likes so she can bond with her. Jackie declines, saying she is too young to go to a rock concert on a school night. A few weeks later, Jackie surprises Anna with tickets to the same concert and Anna is thrilled and calls her: "the coolest mom ever". Jackie says it was a good idea and thanks her heartlessly. Isabel is outraged but does not show it and tearfully tells her to have a good time tonight and leaves.

Luke and Jackie tell the kids about the engagement and Anna is furious, believing nobody cares about her anymore. Later Jackie finally tells Luke and the children about her illness, resulting in Anna storming out, believing that she should have told her earlier. That night Jackie finally shows that she can be fun by dancing and singing with the kids.

Jackie and Isabel clash repeatedly, largely over Isabels parenting. This becomes evident when Ben goes missing on Isabels watch and Jackie claims that she has never lost him, which she later admits to be untrue. However, they manage to establish a shaky truce, as both Jackie and Isabel come to terms with the fact that Isabel will soon step into the role of surrogate mother. The two women finally bond when Isabel reveals her admiration of Jackies maternal instincts, while Jackie in turn praises Isabels "hipness" as a means to connect with Anna. Isabel finally lets her guard down when she tearfully tells Jackie her biggest fear is that on Annas wedding day, all Anna will wish for is her mothers presence. Through her own tears, Jackie says her own fear is that Anna will forget her. Jackie explains to Isabel that, while Jackie will always have their past, Isabel will have their future.

The film ends with the family celebrating Christmas, when a bedridden Jackie is visited in her room by Ben and Anna. Individually, Jackie tells her children after giving them their presents that though she will die, she will remain with them as long as they remember her. Later that day, Isabel is taking a family portrait of Luke and Jackie with the children. Jackie demonstrates her acceptance of Isabel by inviting her to join them, stating "lets get   with the whole family." Isabel does, and as the closing credits begin, both women are shown happily in a photo side by side, finally at peace with one another and future events (Jackies death and Isabels marriage to Luke).

==Cast==
* Julia Roberts as Isabel Kelly 
* Susan Sarandon as Jackie Harrison
* Ed Harris as Luke Harrison
* Jena Malone as Anna Harrison
* Liam Aiken as Ben Harrison
* Lynn Whitfield as Dr. Sweikert
* Darrell Larson as Duncan Samuels
* Mary Louise Wilson as Mrs. Franklin, School Counselor

==Reception== Patch Adams US  and $159,710,793 worldwide  from a budget of $50 million.

Stepmom received mixed reviews from critics.  It earned a 44% rating on Rotten Tomatoes. 

Susan Sarandon was nominated for the Golden Globe Award for Best Actress – Motion Picture Drama and won the San Diego Film Critics Society Award for Best Actress. Ed Harris won the National Board of Review Award for Best Supporting Actor for his roles in Stepmom and The Truman Show.

==Soundtrack==
The soundtrack to Stepmom was released on August 12, 1998 via Sony Classical label.

{{Infobox album  
| Name        = Stepmom
| Type        = Film
| Artist      = John Williams
| Cover       = Stepmom soundtrack cover.jpg
| Border      = 
| Alt         =
| Released    =  
| Recorded    = 
| Genre       = 
| Length      =  
| Label       = Sony Classical  SK 61649  
| Chronology  = 
| Producer    = John Williams Saving Private Ryan (1998)
| This album  = Stepmom (1998)
| Next album  =   (1999)
}}

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 53:30 

| title1          = Always and Always
| length1         = 3:41
| extra1          = John Williams

| title2          = The Days Between
| length2         = 6:27
| extra2          = John Williams featuring Christopher Parkening

| title3          = Time Spins Its Web
| length3         = 2:19
| extra3          = John Williams

| title4          = The Soccer Game
| length4         = 4:27
| extra4          = John Williams

| title5          = A Christmas Quilt
| length5         = 3:56
| extra5          = John Williams

| title6          = Isabels Horse and Buggy
| length6         = 1:28
| extra6          = John Williams

| title7          = Taking Pictures
| length7         = 3:12
| extra7          = John Williams featuring Christopher Parkening

| title8          = One Snowy Night
| length8         = 5:33
| extra8          = John Williams

| title9          = Bens Antics
| length9         = 3:04
| extra9          = John Williams

| title10         = Isabels Picture Gallery
| length10        = 3:44
| extra10         = John Williams

| title11         = Jackie and Isabel
| length11        = 2:59
| extra11         = John Williams featuring Christopher Parkening

| title12         = Jackies Secret
| length12        = 3:32
| extra12         = John Williams

| title13         = Bonding
| length13        = 3:55
| extra13         = John Williams

| title14         = Aint No Mountain High Enough
| length14        = 2:29
| extra14         = Marvin Gaye and Tammi Terrell

| title15         = End Credits
| length15        = 6:16
| extra15         = John Williams

}}

==See also==
* The Other Woman (1995 film)

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 