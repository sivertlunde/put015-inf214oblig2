Kahani Kismat Ki
{{Infobox film
| name = Kahani Kismat Ki
| image = Kahani_Kismat_Ki.jpg
| image_size = 
| caption = 
| director       = Arjun Hingorani
| producer       = Arjun Hingorani
| writer         = S.M. Abbas Arjun Hingorani K. A. Narayan
| narrator       = 
| starring       = Dharmendra Rekha Ajit
| music          = Kalyanji-Anandji
| cinematography = Kishore Rege
| editing        = Anant Apte
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = India
| language       = Hindi
| budget = 
}}

Kahani Kismat Ki is a 1973 Hindi film produced and directed by Arjun Hingorani. The films success prodded the director to try to recreate its success by continuing with its lead actor and film titles having 3 words starting with the alphabet K. ( i.e. Katilon Ke Kaatil ). The film stars Dharmendra, Rekha, Ajit Khan|Ajit, Rajindernath, Bharat Bhushan, Fighter Shetty|Shetty. 
The movie was released in 1973 under the banner of Kapleshwar Pictures. Other starcast included Sulochana Latkar, Jayshree T., Rajni Gupta,Abhi Bhattacharya, Yunus Parvez, and Hiralal in significant roles. The story and screenplay were written by K.A. Narayan and Arjun Hingorani respectively whereas dialogues were written by S.M. Abbas. Famous music directors Kalyanji-Anandji composed the music of the movie and list of playback singers included Asha Bhosle, Kishore Kumar and Mukesh.

==Synopsis==
Even after attaining a bachelors degree, Ajit Sharma is unable to secure any employment in order to better the lifestyle of his widowed mother, sister, Meena, elder brother Gopuram and a younger brother. He gets frustrated and takes to robbing and stealing. Being an amateur he is arrested by the police and sentenced to two years in jail. After he completes his sentence, he decides to be honest and seek employment, but without any success. Then one day he wins some money after beating a wrestler. He decides to buy some necessities for his family, which inter Alia consists of a school-bag. When Gopuram opens the bag, he finds it filled with cash. The family is enraged that Ajit has obtained these funds through crime, but Ajit defends himself, finds out who the owner (Premchand) is and returns the money to him. A gratified Premchand employs Ajit with a generous remuneration. Ajit meets with Premchands daughter, Rekha, and both fall in love, and would like to get married. Before that could happen, a woman comes forward claiming that Ajit is her husband, and is also the father of a child; the police arrest Ajit for the murder of a man named Karam Singh. As a result, Premchand fires him from employment, his family will have nothing to do with him, and Ajit must now come to terms that he may face the death penalty.

==Cast==
* Dharmendra     	 ...	Ajit Sharma
* Rekha	                 ...	Rekha
* Ajit           	 ...	Premchand
* Rajendra Nath 	 ...	Gopuram Sharma
* Sulochana Latkar	 ...	Mrs. Laxmi Sharma
* Jayshree T.    	 ...	Chanda
* Ashoo
* Rajni Gupta
* Bharat Bhushan	 ...	Doctor
* Abhi Bhattacharya	 ...	Mr. Sharma
* Anil Sharma
* Fighter Shetty|M. B. Shetty   	 ...	Jagga
* Hiralal        	 ...	Gopal Kaka
* Jankidas          	 ...	Jankidas
* Yunus Parvez         	 ...	Chandas Father
* Leena Das
* Kamal Kumar
* Amar
* Ishwar Java
* Soni
* H. L. Pardesi 	 ...	Mohan
* Hercules
* Uma Dutt	         ...	The Judge
* Banjara
* Sheela
* Yunus Bihari
* Uday Kumar
* Keshav Rana    	 ...	Police Commissioner
* Jerry
* Ranvir Raj      	 ...	Police Inspector Shyam
* Arjun Hingorani	 ...	Karamchand Singh
* Ram Avtar      	 ...	Ramu (uncredited)

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Rafta Rafta"
| Kishore Kumar, Rekha
|-
| 2
| "Are Rafta Rafta Dekho"
| Kishore Kumar, Rekha
|-
| 3
| "Duniya Mujhse Kahti Hai Ki Peena"
| Kishore Kumar
|-
| 4
| "Kab Tak Na Dogi Dil"
| Kishore Kumar
|-
| 5
| "Kahani Kismat Ki" Mukesh
|-
| 6
| "Rafta Rafta (II)"
| Kishore Kumar
|-
| 7
| "Tu Yaar Mera"
| Asha Bhosle
|}

== External links ==
*  

 
 
 
 