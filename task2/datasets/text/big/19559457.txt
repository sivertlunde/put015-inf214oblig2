She-Devils on Wheels
{{Infobox film
| name           = She-Devils on Wheels
| image          = Poster of the movie She-Devils on Wheels.jpg
| director       = Herschell Gordon Lewis
| producer       = Herschell Gordon Lewis
| writer         = Allison Louise Downe
| story          = Fred M. Sandy
| starring       = Betty Connell Nancy Lee Noble Christie Wagner
| music          = Larry Wellington
| cinematography = Roy Collodi
| editing        = Richard Brinkman
| studio         = Mayflower Pictures
| distributor    = Mayflower Pictures
| released       = 7 May 1968
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $50,000 (estimated)
}}

She-Devils On Wheels is a 1968 American exploitation film about an all-female motorcycle gang called The Man-Eaters, directed and produced by Herschell Gordon Lewis.

== Plot ==

The Man Eaters are led by Queen (Betty Connell). The gangs main activity is racing each other, then having a wild orgy at their clubhouse afterward. The race determines the order in which the girls get to pick a man. One of the fastest held rules of the group is that "all men are mothers" and no Man-Eater falls in love. New initiate Karen (Christie Wagner), strains these rules as the gang finds her choosing the same man, Bill (David Harris), several times. The Man-Eaters test Karens loyalty by making her drag a well beaten Bill from the back of her motorcycle. Karens loyalty is tested again when her ex-boyfriend, Ted (Rodney Bedell), urges Karen to leave the gang because of the danger presented by a rival gang of hot-rodders led by a man named Joe-Boy (John Weymer).

== Critical reception ==
 Allmovie criticized the film, citing the bad acting as "entertaining". 

== See also ==

* Outlaw biker film
* List of biker films

== References ==

 

== External links ==

*  
*  

 

 
 
 
 
 
 
 
 

 