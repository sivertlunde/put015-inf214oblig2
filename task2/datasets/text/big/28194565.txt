Road to Nowhere (film)
 
{{Infobox film
| name           = Road to Nowhere
| image          = Road to Nowhere film poster.jpg
| caption        = Theatrical release poster
| director       = Monte Hellman
| producer       = Monte Hellman Steven Gaydos Melissa Hellman
| writer         = Steven Gaydos
| starring       = Cliff De Young Waylon Payne Tygh Runyan Shannyn Sossamon Dominique Swain
| music          = Tom Russell
| cinematography = Josep M. Civit
| editing        = Céline Ameslon
| studio         = E1 Films 
| distributor    = Monterey Media   Entertainment One  
| released       =  
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         = under $5 million
| gross          = $161,619	 
}} romance Thriller thriller independent film directed by Monte Hellman, written by Steven Gaydos, and starring Cliff De Young, Waylon Payne, Shannyn Sossamon, Tygh Runyan, and Dominique Swain. It is Hellmans first feature film in 21 years.

Road to Nowhere was shot in western North Carolina from July to August 2009, before moving to Europe. The film premiered on September 10, 2010 at the 67th Venice International Film Festival and was nominated for the Golden Lion, but won Jury Award Special Lion for Career Achievement. The film was given a limited release in New York on June 10, 2011 and in Los Angeles on June 17, 2011.   

==Plot==
Promising filmmaker named Mitchell Haven invites Laurel Graham, a non-professional actress, to play Velma Duran, a person involved in a financial scandal that made headlines and is featured in the Havens new film. The director falls in love with his muse, to the despair of some members of the crew.

==Cast==
* Shannyn Sossamon as Laurel Graham/Velma Duran
* Dominique Swain as Nathalie Post
* Cliff De Young as Cary Stewart/Rafe Taschen
* Tygh Runyan as Mitchell Haven
* Fabio Testi as Nestor Duran John Diehl as Bobby Billings
* Waylon Payne as Bruno Brotherton
* Bonnie Pointer as herself
* Michael Bigham as Joe Watts
* Lathan McKay as Erik
* Nic Paul as Jeremy Laidlaw
* Peter Bart has a cameo in the film playing himself 

==Production==

===Development===
Road to Nowhere is Monte Hellmans first feature film in 21 years. The film was written by Variety (magazine)|Variety executive editor Steven Gaydos.     Shannyn Sossamon was the first actor to be cast after Gaydos saw her in a restaurant rehearsing a scene with another person. Reluctant, Gaydos gave Sossamon his card saying, "I dont do this often, but I wonder if you or your agent would contact Monte Hellman". Hellman told the Los Angeles Times that he dedicated the film to Laurie Bird, with whom Hellman fell in love while directing her in Two-Lane Blacktop.   

===Filming=== flash card Balsam for Waynesville for Maggie Valley, Jackson County Airport. Students from University of North Carolina School of the Arts and Western Carolina University were hired as production assistants and also served as Extra (actor)|extras.     Other shooting locations were done in Los Angeles.   
 Cullowhee resident got a deal with the filmmakers that allowed him to make a fuel pump repair at the airport in exchange to fly his 1966 Piper Cherokee four-passenger plane as a stuntman. Rowell did eight to nine passes over the lake flying 300 to 500 feet above the water. In post-production film editors cut the shots back and forth of Rowell flying near the dam and the actual actor sitting in Rowells plane pretending to fly in front of a green screen and crash into the Fontana Dam.  Natasha Senjanovic of The Hollywood Reporter called the plane crash "cinemas top plane crashes" and that, "  is beautifully shot and comes as a total surprise". 

Hellman still needed to shoot scenes in Europe but was over budget. Since the plane tickets were already purchased, his daughter, co-producer Melissa Hellman raised more money through private equity.  Hellman shot in the streets of London and traveled to Italy to shoot at Lake Garda.  Scenes were shot in the church of San Pietro in Vincoli, in front of Moses (Michelangelo)|Michelangelos Moses and the tomb of Pope Julius II all in Rome. 

==Release==
In January 2011, Monterey Media brought the United States distribution rights from Entertainment One.  The American Cinematheque at the Egyptian hosted a tribute to Hellman which culminated on May 14, 2011, with a Special Premier of Road to Nowhere.  On June 8, 2011, Film Society of Lincoln Center will host an evening with Hellman, which will include a special presentation of Road to Nowhere and a screening of Hellmans adaptation of Cockfighter. 

===Festivals===
Road to Nowhere was selected to screen at the following film festivals:
*2010 Whistler Film Festival   
*2010 Venice Film Festival   
*2010 Palm Springs International Film Festival 
*2010 South by Southwest   
*2011 Nashville Film Festival   
*2011 Karlovy Vary International Film Festival 
*2011 Filmfest Oldenburg

===Limited theatrical run===
Road to Nowhere was given a limited release in New York on June 10, 2011 and in Los Angeles on June 17, 2011.    In New York the film opened in one theater and grossed $2,521 for its opening weekend. It grossed a total of $4,984 in its first week.  In Los Angeles the film opened in six theaters and grossed $6,051—$864 per theater for its opening weekend, a 140% increase in tickets.   In its third week it grossed $3,936—$984 per theater, a 35% decrease in ticket sales from the previous week. It was removed from three theaters.  In its fourth weekend, a Independence Day (United States)|four-day weekend, the film made $3,113—$778 per theater.  By its fifth weekend it was removed from two theaters and had a 67% percent drop in tickets making $846–$423 per theater.   By its sixth weekend the film was playing in three theaters making $877–$292 per theater.   For its seventh weekend, it gained $3,609—$722 per theater in five theaters, an increase of 247.4% from the previous weekend. 

The film grossed $83,496 in France and $37,829 in Portugal.    Road to Nowhere has made $40,294 in the United States and $121,325 in other markets, for a worldwide total of  $161,619. 

===Home media===
Road to Nowhere was released to DVD and Blu-ray Disc|Blu-ray on  .  Features include a 15-min behind the scenes (making of the film) video and a 14-min Q&A with Hellman and Gaydos at the Nashville Film Festival. 

==Critical reception==
The film received mixed to positive reviews from critics, with many critics praising the performance of Shannyn Sossamon. Review aggregator Rotten Tomatoes reports that 79% of 24 critics have given the film a positive review, with an average of 6.7 out of 10.  Metacritic, which assigns a weighted average score out of 100 to reviews from film critics, has a rating score of 59 based on 17 reviews. 
 
Kevin Thomas of the Los Angeles Times wrote a positive review saying, "In its masterful use of evocative imagery and music, Road to Nowhere is flawless".   After an interview with Hellman, John Anderson from the New York Times said positive things about the film saying "Road may also be as significant to the indie feature as Avatar (2009 film)|Avatar is to the popcorn movie".   Road to Nowhere was also included in Roger Cormans Legendary Films Blog. 
 Mulholland Drive".    Roger Ebert gave the film two out of four stars criticizing the storys film within a film narrative. He said, "Road to Nowhere is not a failure in that it sets out to do exactly what it does, and does it. The question remains of why it should have been done. Hellmans skill is evident everywhere in precise framing and deliberate editing. Each scene works within itself on its own terms. But there is no whole here. Ive rarely seen a narrative film that seemed so reluctant to flow. Nor perhaps one with a more accurate title". 

==Awards==
{| class="wikitable"
|-
! Festival
! Category
! Winner/Nominee
! Won
|- 67th Venice Venice International Film Festival Jury Award Special Lion for Career Achievement Monte Hellman Yes 
|- Palm Springs International Film Festival Maverick Award Monte Hellman Yes 
|- Whistler Film Festival Special Tribute for Lifetime Achievements Monte Hellman Yes 
|- Nashville Film Festival Coleman Sinking Creek Award Monte Hellman Yes 
|}

==References==
 

==Further reading==
* 

==External links==
* 
* 
* 
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 