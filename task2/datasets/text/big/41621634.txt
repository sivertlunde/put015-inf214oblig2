Balala the Fairies: The Magic Trial
 
{{Infobox film
| name = Balala the Fairies: The Magic Trial Of Faries

| image = File:Balala_the_Fairies_The_Magic_Trial_poster.jpg
| image size = 
| border = 
| alt = 
| caption = Theatrical release poster showing Daisy Cakes as Mei-Xue 
| director = Ren Yu Hao Lui
| starring = Daisy Cakes Chen Yuwei  Wang Hui
| released =  
| runtime = 90 minutes
| country = China
| language = Mandarin
| studio         = Enlight Media Guangdong Alpha Animation and Culture Co.,Ltd
| distributor    = Enlight Pictures
| gross          = United States dollar|US$2.03 million (China)
}} fantasy adventure film directed by Ren Yu and Hao Lui. It was released in China on 23 January.  It is a live-action film adaptation of an animated magical girl series of the same name created by Guangzhou Toy company Auldey.

==Cast==
* Daisy Cakes as Mei-Xue
* Chen Yuwei 
* Wang Hui

==Reception==
The film has grossed United States dollar|US$2.03 million at the Chinese box office. 

==See also==
*Balala, Little Fairy

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 