Ebony, Ivory & Jade
 
{{Infobox film
| name           = Ebony, Ivory & Jade
| image          =
| image size     =
| caption        = Cirio Santiago
| producer       = 
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography = 
| editing        =  Dimension Pictures
| released       = 1976
| runtime        = 
| country        = 
| language       = 
| budget         =
| preceded by    =
| followed by    =
}} Cirio Santiago, made in Manila, Philippines|Manila, Philippines. A relatively well-budgeted martial arts feature by Santiagos standards, the film was seen mainly in US drive-in movies, where it was first released as She-Devils in Chains. It has also been released as American Beauty Hostages, Foxfire, and Foxforce.

Five female athletes are kidnapped during an international track meet in "Hong Kong," then fight their way to freedom after being recaptured several times. Considered a minor classic of the blaxploitation genre, Ebony, Ivory & Jade stars Rosanne Katon as track star Pam Rogers, the eponymous Ebony of the title. Colleen Camp co-stars as Ivory, her privileged track and field rival. Sylvia Anderson appears as "Jade".

A 1980 TV-movie spinoff starred Debbie Allen, Martha Smith and Bert Convy.
 Pulp Fiction by Uma Thurmans character, who speaks about her role in an unsuccessful television series called "Fox Force Five" . The trailer for the film also used the phrase "roaring rampage of revenge", a phrase used to describe Kill Bill Vol.1 and referred to in the opening of Kill Bill Vol.2.

==External links==
* 

 
 
 

 