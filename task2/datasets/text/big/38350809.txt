Bombay Talkies (film)
 
 

{{Infobox film
| name           = Bombay Talkies
| image          = Bombay_Talkies_2013_Film.jpg
| alt            = Bombay Talkies Official Release Poster
| caption        = Theatrical release poster
| director       = Karan Johar Dibakar Banerjee Zoya Akhtar Anurag Kashyap
| producer       = Ashi Dua
| writer         = Karan Johar  Dibakar Banerjee  Zoya Akhtar Anurag Kashyap Reema Kagti
| starring       = Rani Mukerji Randeep Hooda Saqib Saleem Vineet Kumar Singh Nawazuddin Siddiqui Sadashiv Amrapurkar Naman Jain Katrina Kaif Amitabh Bachchan
| music          = Amit Trivedi 
| cinematography = Anil Mehta Carlos Catalan Nikos Andritsakis Rajeev Ravi Ayananka Bose
| editing        = Deepa Bhatia
| studio         = Flying Unicorn Entertainment
| distributor    = Viacom 18 Motion Pictures
| released       =  
| runtime        = 128 minutes
| country        = India
| language       = Hindi English
| budget         =   
| gross          =    
}}
Bombay Talkies is a 2013 Indian anthology film consisting of four short films, directed by Anurag Kashyap, Dibakar Banerjee, Zoya Akhtar and Karan Johar.  The film released on 3 May 2013, coinciding with and celebrating the 100th year of Indian cinema and the beginning of a new era in modern cinema.  It was screened at the 2013 Cannes Film Festival on 17 May 2013. 

==Plots==

===Ajeeb Dastaan Hai Yeh===
; Directed by Karan Johar

The short film begins with Avinash (Saqib Saleem), a young man, bursting into his house. He wakes his father and pushes him against the wall declaring that he is homosexual. He leaves his parents house, distraught and heartbroken but ready for a new start. He passes a girl at a train station singing "Ajeeb Dastan Hain Ye." Gayatri (Rani Mukerji) is married to Dev (Randeep Hooda). Gayatri, who works for a magazine meets Avinash, the new intern. Avinash informs her that hes gay and is shocked to see that it doesnt shock her. They gradually become very close. On his birthday, Gayatri invites Avinash home for dinner. That evening, Gayatri tells Dev that Avinash is gay. Dev seems shocked.

During dinner, Avinash and Dev find that they both share a love for old Hindi films and music. Avinash leans over Dev to look at something on the table and Dev visibly reacts. The next day, Gayatri leaves for some work. Avinash goes to her house to meet Dev. He gives him a CD and then invites him to come out. (A funny play on "coming out of the closet.") Avinash takes Dev to meet the little girl who sings "Lag jaa gale." Dev is shocked. He pays the girl a lot of money and asks her what she plans on doing with it. She says she will buy food for her brothers and sisters. He asks her if shes lying. She curtly replies that she isnt, and telling lies is bad.

By this point, Avinash is sure that Dev is gay but hasnt come out of the closet. The next day he meets Gayatri who happily informs him that she and Dev had amazing sex the last night. Avinash is disappointed and angry, knowing fully well that he is the reason for Devs good mood.

He goes to meet Dev at work. He admits that he took the one hour journey between the offices just to meet him. Dev is embarrassed and asks him to leave. Avinash reaches over to hug him sensually, alarming Dev. Dev loses his temper and begins to beat Avinash up. Avinash leaves Dev, and goes back home. The beating he received from Dev triggers memories of his own father beating him up upon discovering his sexuality.

Dev visits him to apologize. He seems torn and after hitting Avinash again and throwing him against the cupboard, he kisses him. Again, Dev begins to hit Avinash. Avinash loses his temper and throws Dev out.

Avinash then goes and tells Gayatri that her husband kissed him. Gayatri is infuriated and doesnt listen to anything else Avinash has to say and goes home. Dev enters and tries to kiss her, but she pushes him away and begins to wipe her face. She tells him that she now knows that the theres nothing wrong with her but with him and that he is the reason their marriage failed. She says shes glad shes free now and informs him that their relationship is over.

In the final scene we see Avinash sitting on his bed, upset. We see Dev beside the young girl who is singing, Gayatri has kicked him out. And we see Gayatri putting on make up. The film ends on an ironical note. The young girl asks Dev for money and he says that he doesnt have any. She says that hes lying. He replies that he isnt, and lying is bad, mirroring her words from earlier. The irony being, that his whole marriage and life was a lie.

===Star===
; Directed by Dibakar Banerjee
The story is an adaptation of Satyajit Rays short story "Patol Babu, Film Star". A failed actor (Nawazuddin Siddiqui) is struggling to make a living after his father’s death. He wants everyone should come home to him to offer him work. One day he meets his masters spirit and learns the lesson of life that work is not Gods Gift and one only gets it when one tries. In a turn of events he stumbles upon his last chance to prove himself to the world and more importantly, to his daughter (this last point is adapted from Rays another short story, "Pterodactyl-er Dim").

===Sheila Ki Jawaani===
; Directed by Zoya Akhtar

A 12-year-old boy (played by Naman Jain) aspires to be a Bollywood dancer. His father however wants him to be a football player. The boy is a Katrina Kaif fan and loves dancing to "Sheila Ki Jawani." During a TV interview he hears Katrina Kaif talk about breaking conventions of society and following dreams regardless of the obstacles that come in ones way. He is encouraged by what he hears from his idol. In his parents absence, he dresses up like Sheila and performs for himself and his sister. One day he is caught by his parents and is rebuked for his inappropriate behavior. Meanwhile, his sister wants to go on a School trip but is refused Rs. 2000 by their father because he had spent funds on the sons football training. She is rather disappointed that the parents are so focused on his football training while he doesnt even enjoy football. It is then that the brother comes to his sisters rescue and offers to perform to collect money for her trip. They then decide to organize a small ticketed event at an old garage, where the boy dances to his favorites.

===Murabba (Fruit Preserve)===
; Directed by Anurag Kashyap

Vijay (Vineet Kumar Singh) is from Allahabad city in UP. The story begins with Vijay traveling to Mumbai to fulfill his ailing fathers desire. His father (Sudhir Pandey) desires that Vijay meet Bollywood superstar Mr. Amitabh Bachchan, offer and feed him homemade murabba and bring the remaining half for his father. Vijays father believes that doing so will bring comfort to him and in turn lengthen his life. Vijay is shown struggling to get personal audience with Mr. Bachchan. Hungry, frustrated and penniless, Vijay even takes up an odd job in Mumbai. Eventually, after much struggling and convincing Mr. Bachchans security guards, he gets to meet Mr. Bachchan personally. Amazed at Vijays determination and dedication towards his father Mr. Bachchan happily obliges Vijay. He eats half of Vijays homemade murabba. Satisfied and victorious Vijay now sets on his return journey by train. On his way back he is shown narrating his experiences to fellow passengers. Meanwhile, a co- passenger maliciously breaks the glass jar containing the murabba eaten by Mr. Bachchan while another co-passenger inadvertently squishes it. Disappointed and heartbroken, Vijay has no option but to replace the piece of murabba somehow. He decides to buy a new glass jar and some murabba. He reaches home with the murabba and offer it to his father. The father however is able to detect that something went wrong; he asks his son, where he broke the glass jar? In response, Vijay narrates the truth to him. It is then that the father narrates his own story to Vijay. Just as he had asked Vijay to meet Mr. Bachchan, his grandfather had asked his father to meet Mr. Dilip Kumar, a Bollywood superstar of his times. His grandfather had handed over a jar of honey to his father and had asked that Mr. Dilip Kumar dip his finger into the jar. However, the jar of honey caught ants by the time it reached Mr. Dilip Kumar and the actor refused to dip his finger into it. Vijays father had then replaced the jar of honey, dipped his own finger into it and taken it back to his father. Unsuspectingly, Vijays grandfather ate honey from the jar for years to come and lived a long life. The movie ends with Vijays father contemplating how life takes a full circle.

==Cast==
* Rani Mukerji as Gayatri   
* Randeep Hooda as Dev 
* Saqib Saleem as Avinash 
* Nawazuddin Siddiqui as Purandar
* Sadashiv Amrapurkar {{cite news|last=Banerjee|first=Soumyadipta|title=Dibakar Banerjee to make a film on Ray’s short story
|url=http://articles.timesofindia.indiatimes.com/2013-02-04/news-interviews/36742462_1_short-story-short-film-satyajit-ray|work=Hindustan Times|date=4 February 2013|accessdate=4 February 2013}} 

* Ranvir Shorey
* Naman Jain as the protagonist in the Sheila Ki Jawani story
* Vineet Kumar Singh as Vijay
* Sudhir Pandey as Vijays father
* Katrina Kaif as Herself (cameo appearance) 
* Amitabh Bachchan as Himself (special appearance) 
; Special appearances 
(during the song "Apna Bombay Talkies", in order of appearance)

*Aamir Khan  
*Madhuri Dixit  
*Karisma Kapoor
*Akshay Kumar
*Juhi Chawla
*Saif Ali Khan
*Rani Mukerji
*Sridevi
*Priyanka Chopra
*Farhan Akhtar Imran Khan
*Vidya Balan
*Kareena Kapoor Khan
*Ranveer Singh
*Anil Kapoor
*Shahid Kapoor
*Sonam Kapoor
*Deepika Padukone
*Ranbir Kapoor
*Shah Rukh Khan

==Reception==

===Critical reception===
Bombay Talkies received positive reviews upon release.    Taran Adarsh of Bollywood Hungama gave the film 4/5 stars, and noted that the film "is one of those infrequent movies wherein you get to eyeball the superior efforts of four top notch film-makers in less than two hours.This reality alone makes the film a compelling watch, while the superior performances and absorbing themes that the movie prides itself in only serve as an icing on the cake. This celebration of cinema is a must watch!" He said Rani Mukerji is a actor par excellence  Anupama Chopra of Hindustan Times also gave the film 4/5 stars, saying "Bombay Talkies is a unique experiment that works very well. The collaboration between four leading directors suggests a confidence that was rare in the industry even a decade ago. I believe that things can only get better from here on."  Tushar Joshi gave the film 3.5/5 stars, adding that "Bombay Talkies is a format that needs to be praised for its concept. The sequencing of the stories works and the pace is swift, never showing signs of lethargy. If this was a tribute to 100 years of cinema, then we need to have an array of directors from different genres pay such homages more often."  Similarly, Sukanya Verma gave the 3.5/5 stars, concluding that "Bombay Talkies is an absorbing ode to the language of cinema that is part of our collective system." As for performances, she noted that "it’s Rani Mukerji’s flawless artistry as an imprisoned soul wearing a mark of normalcy which elevates the emotional core of Johar’s story."  The duo of Banerjee-Nawazuddin creates magic on the screen in 30 mins, adapting Satyajit Rays short story- Patol Babu, Film Star.   

==Soundtrack==
{{Infobox album
| Name = Bombay Talkies
| Longtype = to Bombay Talkies
| Type = Soundtrack
| Artist = Amit Trivedi
| Cover = Bombay Talkies 2013.jpg
| Border = yes
| Alt =
| Caption =
| Released = 
| Recorded = 2013 Feature Film Soundtrack
| Length = 23:30 Hindi
| Label = T-Series
| Producer =
| Last album=Kai Po Che!  (2013)
| This album=Bombay Talkies (2013)
| Next album=Ghanchakkar (film)|Ghanchakkar (2013)
}}
 Abhijeet together for the first time that too with veteran singer S. P. Balasubrahmanyam. The song features all the singers according to the respective actors they have sung most of their hit numbers making this song one of its kind.
{{track listing
| headline = Track listing
| extra_column = Singer(s)
| collapsed = no
| title1 = Bachchan
| extra1 = Sukhwinder Singh
| length1 = 4:06
| title2 = Akkad Bakkad
| extra2 = Mohit Chauhan
| length2 = 5:03
| title3 = Murabba (Duet)
| extra3 = Amit Trivedi, Kavita Seth
| length3 = 3:12
| title4 = Bombay Talkies (Duet) Richa Sharma
| length4 = 4:13
| title5 = Murabba (Solo)
| extra5 = Javed Bashir
| length5 = 3:42
| title6 = Apna Bombay Talkies
| extra6 = Udit Narayan, Alka Yagnik, Kumar Sanu, Sadhana Sargam, Abhijeet Bhattacharya|Abhijeet, Kavita Krishnamurthy, S. P. Balasubrahmanyam, Sunidhi Chauhan, Shaan (singer)|Shaan, Shreya Ghoshal, KK (singer)|KK, Sukhwinder Singh, Shilpa Rao, Mohit Chauhan, Sonu Nigam
| length6 = 4:16
}}

==References==
 

==External links==
*  

 
 
 

 
 
 
 