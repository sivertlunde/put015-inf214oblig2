Lovers' Concerto (film)
 
{{Infobox film name           = Lovers Concerto image          = Lovers Concerto film poster.jpg director       = Lee Han producer       = Hang Sang-gu writer         = Lee Han starring       = Cha Tae-hyun Lee Eun-ju Son Ye-jin music          = Kim Sang-heon cinematography = Jin Young-hwan editing        = Kim Hyeon distributor    = Korea Pictures released       =   runtime        = 106 minutes language       = Korean country        = South Korea budget         = gross          =    . Box Office Mojo. Retrieved 19 February 2008.  film name      = {{Film name hangul         =   hanja          =   rr             = Yeonae Soseol mr             = Yǒnae Sosǒl}}
}} romantic melodrama dealing with friendship, jealousy and the ties that bind.

==Plot==
Lee Ji-hwan (Cha Tae-hyun) receives photographs in the mail that reminds him of how he met two girls, Shim Soo-in (Son Ye-jin) and Kim Gyung-hee (Lee Eun-ju), one summer five years ago. He shares his memories with his friend Chul-hyun (Park Yong-woo). He recalls initially being in love with Soo-in, but she rejects him. Throughout the summer, the three remain friends. Ji-hwan and Gyung-hee develop feelings for each other but are reluctant to admit it. Ji-hwan recalls the complex relationship between the three to Chul-hyun.

==Cast==
* Cha Tae-hyun ... Lee Ji-hwan
* Lee Eun-ju ... Kim Gyung-hee/Soo-in
* Son Ye-jin ... Shim Soo-in/Gyung-hee
* Moon Geun-young ... Ji-yoon, Ji-hwans sister
* Park Yong-woo ... Chul-hyun, Ji-hwans friend
* Kim Nam-jin ... Seok-jin 
* Sa Kang ... Min-young
* Shin Seung-hwan ... Min-sik
* Choi Jung-woo ...Soo-ins father
* Kim Hee-ryung ... Soo-ins mother
* Seo Yun-ah ... Ji-hwans first love
* Yoo Hyung-kwan ... chauffeur Min
* Kwak Jung-wook ... young Ji-hwan 
* Jeon Ha-eun ... young Gyung-hee 
* Jung In-gi ... taxi driver
* Lee Moon-sik ... sleeping man on the bus
* Lee Yeon-ju ... guest at Chul-hyuns birthday party
* Choi Seong-min ... surgeon at childrens hospital
* Kim Seong-su ... taxi passenger
* Jeon Hee-ju ... taxi passenger
* Park Jae-woong ... employee at cinema
* Lee Geum-joo ... Ji-hwans mother
* Son Young-soon ... granny at amusement park

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 


 
 