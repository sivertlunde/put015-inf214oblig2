Ritam zločina
 
<!-- Hide infobox until needed
{{Infobox film
| name           = Ritam zločina
| image          =
| image size     =
| alt            =
| caption        =
| director       =
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =
| runtime        =
| country        =
| language       =
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
--->  Croatian film Yugoslavia in 1981, directed by Zoran Tadić, starring Ivica Vidović and Fabijan Šovagović. It is based on Dobri duh Zagreba, a novel by Pavao Pavličić. 

In 1999, a poll of Croatian film critics found it to be one of the best Croatian films ever made. 

==Plot==
A single teacher allows a stranger to share his home with him before it is to be torn down by developers. The stranger has a fascination with statistics, and claims he can predict crimes based on statistical analyses. Intrigued by this talent, the teacher tells a married, female friend that the man seems to have done what he claims. Then the numbers show that there is a slip-up and a murder that was supposed to have occurred, did not. The stranger is adamant that a balance has to be achieved or the whole town will suffer - and he leaves. (IMDb)

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 