Agadbam
{{Infobox film
| name           = Agadbam
| image          = Agadbam Marathi Movie.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Satish Motling
| producer       = Trupti Bhoir
| screenplay     = 
| story          = 
| starring       = Makarand Anaspure Trupti Bhoir Usha Nadkarni Chitra Navathe Vikas Samudre Narayan Jadhav
| music          = Ajit - Sameer
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Everest Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Agadbam  is a Marathi film released on October 8, 2010.  This film  is directed by Satish Motling and produced by Trupti Bhoir.

== Synopsis ==
The Devkule family comprises various “interesting” characters. A visually impaired Grandmother, who doesn’t want to accept her condition but always wears glasses and pretends that she can see everything, Raiba’s father who is long dead, keeps making appearances to remind his wife about the promise he made his friend while he was alive.

Paru alias Parvati Tai, who despite having proper sight, is a victim of blind faith. Apart from being a possessive mother she keeps arguing with her son regarding marriage. The jewel in the crown of Devkule family is Raiba, the most eligible bachelor; he is ready to tie the knot with any girl but his aspirations are very high. He wishes for a spacious house, a big car and a fat salary. 

What happens when he gets everything huge in life? The drama unfolds when Raiba’s large dreams come true because of his father’s promise.

== Cast ==
The film stars Makarand Anaspure, Trupti Bhoir, Usha Nadkarni, Chitra Navathe, Vikas Samudre, Narayan Jadhav & Others.

==Soundtrack==
The music has been directed by Ajit – Sameer, while the lyrics have been provided by Guru Thakur.

===Track listing===
{{Track listing
| title1 = Babooo Babooo | length1 = 4:27
}}

== References ==
 
 

== External links ==
*  
*  
*  
*  

 
 
 


 