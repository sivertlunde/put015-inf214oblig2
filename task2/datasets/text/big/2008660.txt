Wicked City (1987 film)
 
{{Infobox film
| name           = Wicked City
| image          = Wickedcity.jpeg
| caption        = 
| director       = Yoshiaki Kawajiri
| producer       = Kousuke Kuri Yoshio Masumizu
| based on       =  
| writer         = Kisei Choo
| starring       = Yūsaku Yara Toshiko Fujita Ichirō Nagai Takeshi Aono
| music          = Osamu Shooji
| cinematography = Kinichi Ishikawa
| editing        = Harutoshi Ogata Madhouse
| distributor    = Japan Home Video
| released       =  
| runtime        = 80 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} horror neo-noir the same name.

The story takes place towards the end of the 20th century and explores the idea that the human world secretly coexists with the demon world with a secret police force known as the Black Guard protecting the boundary.

==Plot==
The existence of the "Black World" is known to very few people. For centuries, a pact between the Black World and the world of humans has been observed to maintain peace, and terms must be negotiated and renewed every few hundred years to continue relative harmony. A militant faction of radicals from the Black World stops at nothing to prevent the signing of a new treaty.

Two agents of the Black Guard, an organisation designed to protect the relations of both worlds in secret, are charged with insuring the success of the treaty: The human Renzaburō Taki is an electronics salesman by day, and a Black Guard agent when needed; his partner Makie, who masquerades as a model, is a beautiful and skilled woman from the Black World. Their mission is to protect Giuseppi Mayart, a two hundred year-old man with fantastic spiritual power, whose presence at the peace treaty signing in Tokyo is critical. The Radicals intend to kill Mayart to upset the peace between both worlds.

Attacks on Makie, Taki and Mayart begin even before the three meet, and the situation does not improve, despite taking shelter in a Hibiya hotel that supposedly has strong spiritual barriers to keep people of the Black World away; on top of this, Mayart sneaks out after a skirmish at the hotel. Makie and Taki find him at a soapland in the grip of a Black World woman who has sapped his health, prompting a frantic trip to a spiritual hospital under Black Guard protection.
 tentacle to be punished for her "crimes" against the Black World by being repeatedly raped, and Taki is forced to leave her behind, but as soon as he knows Mayart is safe in the hospital, he rushes to where his partner is being held, despite the threat that he will be thrown out of the Black Guard.

Taki is led to a dilapidated building far from the hospital, where he finds Makie being gang-raped. While Taki is successful in freeing Makie after eliminating a succubus and other demon agents, they are relieved of their Black Guard duties and are captured by a spider-like woman Taki has encountered before; both are knocked unconscious, but they wake up alone in a church (as the Spider Woman was killed), and the two share a romantic night of copulation, impregnating Makie.

One last attack by the Radicals comes and is partially deflected by a surprisingly healthy Mayart, who reveals he was protecting his bodyguards, not the other way around as they had been led to believe. Mayart and Taki almost succeed in defeating Mr. Shadow, but the final blow comes from Makie, who suddenly displays an overwhelming power, a gift from her joining with Taki. Mayart explains that the two are essential to forming a new peace treaty, he tells Taki that Makie will be the first woman to give birth to a half human and half black world child and will soon make more children , thus ushering in a new race and hopefully ensuring everlasting peace between the two worlds.

Taki is reinstated in the Black Guard, uncertain about his feelings for Makie and what is expected of them, but is optimistic about the future he will help protect as child grows inside Makies body. 

==Cast==
{| class="wikitable"
|- Manga UK)
|-
! Renzaburō Taki
| Yūsaku Yara || Gregory Snegoff || Stuart Milligan
|-
! Makie
| Toshiko Fujita || Gaye Kruger || Tasmin Hollo
|- 
! Giuseppi Mayart Mike Reynolds || George Little
|-
! Mr. Shadow
| Takeshi Aono || Jeff Winkless || Ray Lonnen
|-
! Kanako/Spider Woman
| Mari Yokoo || Edie Mirman || Liza Ross
|-
! Black Guard President
| Yasuo Muramatsu || Robert V. Barron || Phillip Goug
|-
! Hotel Manager (Hodgkins) 
| Tamio Ōki || David Povall || William Roberts
|-
! Jin
| Kōji Totani || Kerrigan Mahan || Brian Note
|-
! Soap Girl
| Arisa Andou || Joyce Kurtz || Pamela Merrick
|-
! Clinic Director
| Kazuhiko Kishino || Edward Mannix || Douglas Blackwell
|-
! Bartender (Ken/Joe) Adam Henderson
|-
! Temptress
| Asami Mukaidono || Eleni Kelakos || Liza Ross
|-
! Monk Masato Hirano Adam Henderson
|-
! Takis Coworkers Steve Kramer || Pamela Merrick Bob Sessions
|-
! Demons
| || Michael McConnohie Carl Macek || Bob Johnson William Roberts
|-
! Narrator
| ||  || Bob Sessions title = Wicked City (1987) - 
Full Cast & Crew|accessdate=2014-08-09}} 

==Production==
  Meikyu Monogatari (1987) and was asked to direct a 35 minute short on Hideyuki Kikuchis novel. Kawajiri completed the short and after Japan Home Video saw a screening of it, they wished him to make it feature length. The producers Kenji Kurata and Makoto Seya expressed their opinion that the director shouldn’t extend it unless he wanted to. Kawajiri was such a fan of the world, he saw it as an opportunity to explore more characterization and created more animation for the start, the middle and the end. The project was completed in under a year. 

==Release== Streamline and Manga UK Monster City, this version containing the Streamline dub. In 1997 when Madman Entertainment was named distributor for Manga in Australia, the Streamline dub was released on a single tape, and the Manga UK version was phased out.

==Critical reception==
The film does not have a critics rating at the review aggregator website Rotten Tomatoes, but has a 67% "Fresh" rating among users. 

Charles Solomon of the Los Angeles Times stated that the film epitomizes the "sadistic, misogynistic erotica" popular in Japan. He noted that Yoshiaki Kawajiri composes scenes like a live-action filmmaker, and complimented his deft cutting and camera angles, but felt that the "Saturday-morning style animation" and juvenile story did not warrant the effort. Solomon also opined that Kisei Choos screenplay was inscrutable. Solomon concluded his review by touching upon the belief that there is a connection between screen violence and real-life violence by pointing out that Japan is one of the least violent societies in the industrialized world. 

Desson Howe of The Washington Post, who observed the level of violence toward females in the film, characterized it as a "post-Chandler, quasi-cyberpunky violence fest". Howe found the film compelling for its "gymnastic "camera angles, its kinetic pace and imaginative (if slightly twisted) images." He also found the English dubbing laughable, though he saw ominous subtext in various bits of dialogue and other moments in the film. 

Richard Harrigton, also of The Washington Post, saw the film as an attempt to create the Blade Runner of Japanese animation, citing its distinctively languid pace, linear storytelling and gradual exposition. Harrington also detected a Brave New World subtext, and calling it "stylish and erotic, exciting in its limited confrontations and provocative in its ambition." 

Marc Savlov of  , due to Wicked Citys more linear and rapid storyline, and the lack of flashbacks and cyberpunk jargon that Savlov disliked in the genre. Savlov also appreciated the clarified animation. Savlov commented, "This may not be the second coming of Akira (film)|Akira, but its a step in the right direction." 

Chris Hicks of Deseret News called the film "truly awful", citing the films "misogynistic streak" as its most offensive aspect. 

Chuck Arrington of DVD Talk, reviewing the DVD of the film, recommended that consumers "Skip It", citing the transfer errors and scratches on the print, the at-times washed-out colors, and the uninteresting lengthy interview among the DVDs extras. Arrington thought that the visuals and the fight scenes were generally done well, and that the dubbing into English was acceptable, though exhibited some "wooden elements" endemic to all anime titles. Regarding the sexual violence in the film, Arrington found it excluded recommendation for most viewers, commenting, "Though not nearly as gruesome as Legend of the Overfiend, Wicked City is definitely not for children and not really for adults either." 

==Related films== Wicked City, a Hong Kong live action adaptation of the film was made in 1992 financed by Golden Princess Film Production Ltd. The film was directed by Tai Kit Mak, produced by Hark Tsui and starred Jacky Cheung, Leon Lai, Yuen Woo-ping, Roy Cheung, and Michelle Reis.

The story takes place in Hong Kong in a conflict between worlds of Humans and "Rapters". Special police in the city are investigating a mysterious drug named "happiness". Taki, one of the police, meets his old lover Windy, who is a rapter and now mistress of a powerful old rapter named Daishu. Taki and other special police track down and fight Daishu, but later find that he hopes to coexist with human. The son of Daishu, Shudo, is the mastermind. In the end, Shudo is defeated, but Daishu and Takis friends die too. Windy leaves alone.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 