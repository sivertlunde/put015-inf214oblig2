Trophy Wife (film)
{{Infobox film
| image            = Trophy_Wife.jpg
| caption          = Theatrical movie poster
| name             = Trophy Wife
| director         = Andoy Ranay
| producer         =  
| music            = Teresa Barrozo
| cinematography   = Lee Briones-Meily
| story            = Keiko Aquino
| screenplay       = Keiko Aquino
| writer           = 
| editing          = George Jarlego
| starring         =  
| studio           =  
| distributor      = Viva Films
| released         =  
| country          = Philippines
| language         =  
| runtime          = 110 minutes 
| budget           =
| gross            = PHP 31,058,983 

 
}}
 romantic Drama drama and thriller film  directed by Andoy Ranay, starring Cristine Reyes, Derek Ramsay, Heart Evangelista, and John Estrada. The film will be distributed by Viva Films with the co-production of Multivision Pictures and was released on July 30, 2014 in theatres nationwide.           

This is the 4th Adult drama film in 2014 in Viva Films after the hit movie of No Other Woman (2011), A Secret Affair (2012), and When the Love is Gone (2013).

==Sypnosis==

 

==Production==

 

==Multimedia and Merchandise==

 

==Online release==

 

==Cast==

===Main Cast===
*Cristine Reyes as Lani
*Derek Ramsay as Chino
*Heart Evangelista as Gwen
*John Estrada as Sammy 

===Supporting Cast===
*Jaime Fabregas
*Jackie Lou Blanco
*G. Toengi
*Jovic Monsod
*Kian Kazemi
*Clint Bondad 
*Jay Roa
*John James Uy
*Phoebe Walker 
*Akiko Orita
*Creesha Galvero

==Controversies==

 

==Box Office Mojo==

 

==Awards & Nominations==

2015
31st PMPC Star Awards for Movies
  Movie of the Year
  MOVIE ACTOR OF THE YEAR for (John Estrada)

==See also==
* My Neighbors Wife In the Name of Love
* No Other Woman The Mistress
* A Secret Affair One More Try
* Seduction (2013 film)|Seduction
* The Bride and the Lover
* When the Love Is Gone
* Once a Princess The Gifted The Trial

==References==
 

==External links==

 
 
 
 
 
 
 
 