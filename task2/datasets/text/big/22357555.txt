Last Leap
{{Infobox film
| name           = Last Leap
| image          = 
| caption        = 
| director       = Édouard Luntz
| producer       = 
| writer         = Antoine Blondin Jean Bolvary Édouard Luntz
| starring       = Maurice Ronet
| music          = 
| cinematography = Jean Badal
| editing        = Colette Kouchner
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
}}

Last Leap ( ) is a 1970 French crime film directed by Édouard Luntz. It was entered into the 1970 Cannes Film Festival.   

==Cast==
* Maurice Ronet - Garal
* Michel Bouquet - Jauran
* Cathy Rosier - Florence
* Eric Penet - Peras
* André Rouyer - Salvade
* Michel Garland - Lavocat
* Sady Rebbot - Le professeur
* Betty Beckers - La préposée
* Douchka - La serveuse
* Catherine Arditi - Christiane Dancour
* Albertine Bui - Tai
* Michel Charrel - Le patron du café

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 