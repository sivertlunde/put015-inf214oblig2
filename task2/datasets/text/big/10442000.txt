Fighting Back: The Rocky Bleier Story
Fighting Back: The Rocky Bleier Story is a 1980 made-for-television movie about the life of Pittsburgh Steelers running back Rocky Bleier, starring Robert Urich. It was released on 7 December 1980.

It tells the story of Rocky Bleier, who, after becoming a running back for the Pittsburgh Steelers in 1968 was then drafted by the U.S. Army during the Vietnam War. Injured by a bullet to the thigh and a hand grenade to the lower right leg, Rocky is told that he will never walk again. Not only does he walk again after a long Physical therapy|rehabilitation, Rocky returns to train with the Steelers. With the sympathy and support of Steelers owner, Art Rooney, played by Art Carney, Rocky makes the team again, and helps the Steelers become Super Bowl champions. 

==References==
 

 
 

 
 
 
 
 
 

 
 