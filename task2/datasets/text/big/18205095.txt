The Forbidden Path
 
{{Infobox film
| name           = The Forbidden Path
| image          = Theforbiddenpath-1918-newspaperad.jpg
| caption        = Newspaper advertisement.
| director       = J. Gordon Edwards William Fox
| writer         = Adrian Johnson (scenario)
| screenplay     = E. Lloyd Sheldon
| story          = E. Lloyd Sheldon
| starring       = Theda Bara Hugh Thompson
| cinematography = John W. Boyle Fox Film Corporation
| released       =   reels
| country        = United States 
| language       = Silent English intertitles
| budget         = 
}}
 silent drama film directed by J. Gordon Edwards and starring Theda Bara. The film is now considered lost film|lost.    

==Cast==
* Theda Bara as Mary Lynde
* Hugh Thompson as Robert Sinclair
* Sidney Mason as Felix Benavente Walter Law as Mr. Lynde 
* Florence Martin as Barbara Reynolds
* Wynne Hope Allen as Mrs. Lynde
* Alphonse Ethier as William Sinclair
* Lisle Leigh as Mrs. Byrne
* Reba Porter as Tessie Byrne

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 

 