High Art
 
{{Infobox film
| name = High Art
| image = High art ver1.jpg
| caption = Promotional film poster
| writer = Lisa Cholodenko
| starring = Radha Mitchell Gabriel Mann Ally Sheedy Patricia Clarkson
| director = Lisa Cholodenko
| producer = Antidote Films Jeff Levy-Hinte, Susan Stover, Dolly Hall
| distributor = October Films
| released = January 21, 1998
| runtime = 101 minutes
| country = United States
| language = English
| gross = $1,929,168 (USA)
}}
High Art is a 1998 Canadian-American independent film directed by Lisa Cholodenko and starring Ally Sheedy and Radha Mitchell.

==Synopsis==
Sydney (or simply "Syd"), age 24, is a woman who has her whole life mapped out in front of her. Living with longtime boyfriend James, and working her way up at the respected high-art photography magazine Frame, Syd has desires and frustrations that seem typical and manageable. But when a crack in her ceiling springs a leak and Syd finds herself knocking on the door of her upstairs neighbor, a chance meeting suddenly takes her on a new path.

Opening the door to an uncharted world for Syd is Lucy Berliner, a renowned photographer, enchanting, elusive, and curiously retired. Now 40, Lucy lives with her once glamorous, heroin-addicted German girlfriend Greta, and plays host to a collection of hard-living party kids. Syd is fascinated by Lucy and becomes drawn into the center of Lucys strangely alluring life upstairs.

Syd mentions Lucy to her bosses (without realising that she is famous) but they remain uninterested until they realise exactly who Lucy is. At a lunch, Lucy agrees to work for the magazine as long as Syd is her editor. Soon a working relationship develops between the two and a project is underway which promises a second chance for Lucys career. But as Syd and Lucys collaboration draws them closer together, their working relationship turns sexual and the lines between love and professionalism suddenly blur. As Syd slowly discovers the darker truths of Lucys life on the edge, she is forced to confront her own hunger for recognition and the uncertain rewards of public esteem.

==Cast and crew==

===Cast===
* Ally Sheedy as Lucy Berliner
* Radha Mitchell as Syd, an assistant editor at Frame Gabriel Mann as James, Syds live-in boyfriend
* Charis Michelsen as Debby David Thornton as Harry, an editor at Frame and Syds boss
* Anh Duong as Dominique, chief editor of Frame
* Patricia Clarkson as Greta, a German actress and Lucys live-in girlfriend
* Helen Mendes as White Hawk
* Bill Sage as Arnie
* Tammy Grimes as Vera, Lucys mother
* Cindra Feuer as Delia
* Anthony Ruivivar as Xanderr
* Elaine Tse as Zoe
* Rudolf Martin as Dieter
* Laura Ekstrand as Waitress

===Crew===
Soundtrack by Shudder to Think

==Production notes==
The photography by Lucy Berliner (Sheedy) was based on Nan Goldins work. The photographs themselves were made by Jojo Whilden. 

==References==
 

==External links==
*  
*  
*  
*  
* Movie reviews:
**   (Janet Maslin)
**  
**  

 
 

 
 
 
 
 
 
 
 
 
 
 
 