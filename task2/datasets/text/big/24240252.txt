Devil-May-Care
{{Infobox film
| name           = Devil-May-Care
| image          =
| caption        = Theatrical release poster Sidney Franklin
| producer       =
| writer         = Book:       Eugène Scribe   Screenplay   Richard Schayer Dorothy Jordan William Humphrey
| music          = Herbert Stothart William Axt
| cinematography = Merritt B. Gerstad
| choreography   =
| editing        = Conrad A. Nervig
| studio         = Metro-Goldwyn-Mayer (MGM)
| distributor    = Metro-Goldwyn-Mayer (MGM)
| released       =   	
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Devil-May-Care (1929 in film|1929) is a sound (All-Talking) American musical film with a Technicolor sequence of the Albertina Rasch Dancers, and released by Metro-Goldwyn-Mayer on 27 December 1929. This was Ramon Novarros talkie debut

The film is known by a variety of other names, including Battle of the Ladies (USA - working title), Der Leutnant des Kaisers (Austria), Der jüngste Leutnant (Germany), Il tenente di Napoleone (Italy) and O lohagos tis aftokratorikis frouras (Greece).

==Cast==
*Ramon Novarro - Armand Dorothy Jordan - Leonie
*Marion Harris - Louise
*John Miljan - DeGrignon William Humphrey - Napoleon George Davis - Groom
*Clifford Bruce - Gaston
*Lionel Belmore - Innkeeper (uncredited) John Carroll - Bonapartist (uncredited)
*George Chandler - Timid Royalist (uncredited)
*Ann Dvorak - Chorine (uncredited)
*Bob Kortman - Bonapartist (uncredited)

==Soundtrack==
* "Shepherd Serenade"
:Written by Clifford Grey and Herbert Stothart
* "Charming"
:Written by Clifford Grey and Herbert Stothart
* "If He Cared"
:Written by Clifford Grey and Herbert Stothart
* "March of the Guard"
:Written by Clifford Grey and Herbert Stothart
* "Love Ballet"
:Written by Dimitri Tiomkin
:Performed by Albertina Rasch Dancers

==See also==
*List of early color feature films

==External links==
* 
* 

 

 
 
 
 
 

 