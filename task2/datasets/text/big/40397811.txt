Circles (film)
 
{{Infobox film
| name           = Circles
| image          = Circles film poster.jpg
| caption        = Film poster
| director       = Srdan Golubović
| producer       = 
| writer         = Melina Pota Koljević Srđan Koljević
| starring       = Aleksandar Berček
| music          = 
| cinematography = Aleksandar Ilić
| editing        = 
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = Serbia
| language       = Serbian
| budget         = 
}}
 Best Foreign Grand Prix, 2013 CinEast Festival.

The film is inspired by the true story of Bosnian Serb soldier Srđan Aleksić who died protecting Bosnian Muslim civilian Alen Glavović in January 1993 in Trebinje during the Bosnian War. Three stories take place in parallel in Belgrade, Germany, and Trebinje. Nebojsa who witnessed the death of his best friend overcomes his guilty conscience to confront the killer. Haris who owes his life to the person who sacrificed for him risks everything in order to return the favour. The murderers son meets the fallen heros father thus opening the way to overcoming the past.

==Cast==
* Aleksandar Berček as Ranko
* Leon Lučev as Haris
* Nebojša Glogovac as Nebojša
* Nikola Rakočević as Bogdan
* Hristina Popović as Nada
* Boris Isaković as Todor
* Vuk Kostić as Marko (Srđan Aleksić)

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Serbian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 