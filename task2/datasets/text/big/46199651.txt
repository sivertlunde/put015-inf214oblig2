The Ruling Voice
{{Infobox film
| name           = The Ruling Voice
| image          =
| caption        =
| director       = Rowland V. Lee
| producer       = First National Warner Brothers
| writer         =
| starring       = Walter Huston Loretta Young Doris Kenyon
| music          = David Mendoza
| cinematography = Sol Polito Michael Joyce Thomas Brannigan R.G. Mitchell Thomas Riddell
| editing        = George Amy
| distributor    = First National Vitaphone(Warner Brothers)
| released       = October 31, 1931
| runtime        = 76 minutes
| country        = USA
| language       = English
}} Pre Code gangster drama directed by Rowland V. Lee and starring Walter Huston, Loretta Young and Doris Kenyon. It had an alternate title Upper Underworld and was produced and distributed by First National Pictures and the Warner Brothers. 

A preserved film at the Library of Congress. 


==Cast==
*Walter Huston - Jack Bannister
*Loretta Young - Gloria Bannister Dudley Digges - Abner Sneed
*David Manners - Dick Cheney
*Doris Kenyon - Mary Stanton John Halliday - Dexter Burroughs
*Willard Robertson - Ed Bailey
*Gilbert Emery - Andrew Gregory
*Douglas Scott - Malcolm Stanton

==References==
 

==External links==
* 
* 

 


 
 
 
 
 
 

 