A Day to Remember (1953 film)
 
 
{{Infobox film
| name           = A Day to Remember
| image          = A Day to Remember poster.jpg
| image size     = 
| caption        = Theatrical release poster
| director       = Ralph Thomas
| producer       = Betty Box
| writer         = Robin Estridge
| based on       =   James Hayter Harry Fowler
| music          = Clifton Parker
| cinematography = Ernest Steward
| editing        = Gerald Thomas
| studio         = 
| distributor    = United States: General Film Distributors Republic Pictures
| released       = United Kingdom: 10 November 1953 United States: 29 March 1955
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} comedy drama Bill Owen. The darts team of a London public house go on a day trip to Boulogne-sur-Mer in France.

==Plot==
On the eve of their visit to France the members of the Hand & Flower pub darts team gather for a drink. The day trip is being organised by one of their regulars who is a travel agent. For some of the team it is their first ever trip abroad, while for others it is the first time they have returned to France since the Second World War|war. One of the team has developed a plan to buy watches in France and smuggle them back into Britain to sell at a profit. Another, Jim Carver, is going through a rocky patch with his fiancee, who he suspects considers him to be boring and plain.

The following day the group meet at London Victoria and catch the boat train to Boulogne. Once they have landed in France, despite the insistence of their unofficial leader the pubs landlord that they stick together, Jim Carver departs to visit a farm where he had been involved in heavy fighting during 1944 when British troops had arrived to liberate France. He takes some flowers to the cemetery where his comrade is buried. He then meets a young woman, Martine, who he first met eight years before, who invites him to have lunch with her family on the farm. They immediately strike up a chemistry, which his relationship with his fiancee in England lacks. However his newfound friend is also engaged to a local lawyer.
 foreign legion in spite of his friends efforts to stop him. One of the group becomes violently homesick despite having left England only hours before. After attempting, and failing, to retrieve their friend from service in the foreign legion the group begins to drift towards the docks and the ship that will carry them on their voyage home – and wonder what has happened to Carver who has been missing all day.

Carver has fallen in love with Martine, and she has broken up with Henri. However, they argue and he heads for his ship without her. Unbeknownst to him, his fiancee in London has met and struck up a relationship with an American servicemen during a visit to Hampton Court. Carver seems to realise he is far better suited to Martine, and after he boards the ferry she drives hurriedly to the dockside and shouts her true feelings for him. They agree to meet again soon when he returns to France.

==Cast==
*Stanley Holloway  as Charley Porter
*Donald Sinden  as Jim Carver
*Joan Rice  as Vera Mitchell
*Odile Versois  as Martine Berthier James Hayter  as Fred Collins
*Harry Fowler  as Stan Harvey Edward Chapman  as Mr. Robinson Peter Jones  as Percy Goodall Bill Owen  as Shorty Sharpe Meredith Edwards  as Bert Tripp
*George Coulouris  as Foreign Legion Captain.
*Vernon Gray  as Marvin
*Thora Hird  as Mrs. Trott
*Theodore Bikel  as Henri Dubot
*Brenda De Banzie  as Mrs. Collins
*Lily Kann as Martines Grandmother

==References==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 