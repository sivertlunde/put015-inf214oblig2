Rustlers (1949 film)
{{Infobox film
| name           = Rustlers
| caption        =
| image	         = 
| director       = Lesley Selander
| producer       = Herman Schlom
| writer         = Jack Natteford Luci Ward
| starring       = see list below
| music          = Paul Sawtell
| cinematography = J. Roy Hunt Frank Doyle
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| gross          =

}}
 Western directed by Lesley Selander.  The film is a Tim Holt B Western about a group of Arizona ranchers intent on stopping a gang of cattle rustlers. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p236 

== Cast ==
* Tim Holt as Dick McBride Richard Martin as Chito Jose Gonzalez Bustamante Rafferty
* Martha Hyer as Ruth Abbott Steve Brodie as Mort Wheeler
* Lois Andrews as Trixie Fontaine Harry Shannon as Sheriff Harmon
* Addison Richards as Frank Abbott Frank Fenton as Brad Carew
* Robert Bray as Henchman Hank
* Don Haggerty as Rancher
* Monte Montague as Rancher
* Stanley Blystone as Rancher

==References==
 

== External links ==
* 
 
 
 
 
 
 
 
 
 
 
 
 
 