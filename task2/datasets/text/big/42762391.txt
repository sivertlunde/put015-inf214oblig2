Vishnu Sena
{{Infobox film name           = Vishnu Sena image          = image_size     = caption        = director       = B. Naganna producer       = M. Govinda writer         = A. R. Murugadoss based on       =   starring  Vishnuvardhan Gurleen Chopra Ramesh Aravind Lakshmi Gopalaswamy music  Deva
|cinematography = Ramesh Babu editing        = S. Manohar studio         = Sri Hrudayeshwari Films released       =   runtime        = 168 minutes country        = India language  Kannada
|budget         =
}}
 Kannada drama Vishnuvardhan  along with Ramesh Aravind, Lakshmi Gopalaswamy and Gurleen Chopra in the prominent roles.  The film had a musical score and soundtrack composed and written by Deva (music director)|Deva.
 Tamil blockbuster film Ramanaa (film)|Ramanaa (2002) directed by A. R. Murugadoss and starred Vijayakanth, Ashima Bhalla and Simran (actress)|Simran. 

The film released on 23 December 2005 to generally positive reviews from critics. 

== Cast == Vishnuvardhan as Prof. Jayasimha
* Gurleen Chopra 
* Lakshmi Gopalaswamy
* Ramesh Aravind as Ramesh
* Ashutosh Rana
* Ananth Nag
* Pankaj Dheer
* Doddanna Kishore
* Srinivasa Murthy
* Girija Lokesh
* M.N Lakshmi Devi
* B. Jaya (actress)|B. Jaya
* Tennis Krishna
* Srinagar Kitty
* Tharun Sudheer

== Remakes & Character Map ==

{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;" Ramanaa 2002 Tagore 2003 (Telugu cinema|Telugu) || Vishnu Sena 2005 (Kannada cinema|Kannada) || Gabbar is Back 2015 (Bollywood|Hindi)
|- Vishnuvardhan || Akshay Kumar 
|- Ashima Bhalla || Shriya Saran || Gurleen Chopra || Shruti Haasan
|- Simran Bagga|Simran Jyothika ||Lakshmi Kareena Kapoor Khan
|- Vijayan || Sayaji Shinde|| Ashutosh Rana || Suman Talwar
|- Mukesh Rishi || Puneet Issar || Pankaj Dheer|| Sonu Sood
|- Yugi Sethu Prakash Raj || Ramesh Arvind || Prakash Raj 
|}

== Soundtrack ==
The music of the film was composed by Deva (music director)|Deva.

{{Infobox album  
| Name        = Vishnu Sena
| Type        = Soundtrack Deva
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Akash Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Abhimanigale Nanna
| lyrics1 	= K. Kalyan Vishnuvardhan
| length1       = 
| title2        = History Gottha
| lyrics2 	= Upendra
| extra2        = S. P. Balasubrahmanyam
| length2       = 
| title3        = Meghave Meghave
| lyrics3       = V. Nagendra Prasad
| extra3 	= S. P. Balasubrahmanyam, Anuradha Sriram
| length3       = 
| title4        = Benki Kaddi Hacchi
| extra4        = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics4 	= V. Nagendra Prasad
| length4       = 
| title5        = O Deva Neenilli
| extra5        = Shankar Mahadevan
| lyrics5       = Aadarsha
| length5       = 
}}

== References ==
 

== External links==

 
 
 
 
 
 
 
 


 

 