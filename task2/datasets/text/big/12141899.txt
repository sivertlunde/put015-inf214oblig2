Video Demons Do Psychotown
 
{{Infobox film
  | name = Video Demons Do Psychotown
  | image = Psychotown.jpg
  | caption = DVD cover for Video Demons Do Psychotown
  | director = Alessandro De Gaetano Michael A. DeGaetano
  | producer = Karl A. Coken Vincent Field Gregg Ladd Darrel Walker
  | writer = Alessandro De Gaetano
  | starring = Ron Arragon Donna Baltran Dave Elliott Pam Martin Myra Taylor
  | music = Glenn Longacre
  | cinematography = Wayne Kohlar
  | editing = Rocco M. LaBellarte
  | distributor = Troma Entertainment
  | released = 1989
  | runtime = 87 minutes
  | language = English
  | budget =
  | preceded by =
  | followed by =
  }}

Video Demons Do Psychotown (also known as Bloodbath in Psycho Town) is a 1989 horror film written and co-directed by Alessandro De Gaetano and distributed by Troma Entertainment.

==Plot==
The film revolves around two film students who visit a spooky old town in order to film a documentary about it. However, things in the town are not what they seem and before they know it, they are the next target of a mysterious hooded serial killer.

The film is cross promoted in the background of a scene in Tromas Tromeo and Juliet.

==External links==
* 

 
 
 
 
 

 