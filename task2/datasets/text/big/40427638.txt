The Little Traitor
{{Infobox film
| name           = The Little Traitor
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Lynn Roth
| producer       = Marilyn Hall
| writer         = Lynn Roth
| screenplay     = 
| story          = 
| based on       = Panther in the Basement
| narrator       = 
| starring       = Alfred Molina, Ido Port, Theodore Bikel, Rami Heuberger, Gilya Stern, Jake Barker
| music          = Deborah Lurie
| cinematography = Amnon Zalait
| editing        = Michael Ruscio, Danny Shick
| studio         = Evanstone Films and Panther Prods.
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Israel United States
| language       = English Hebrew
| budget         = $1,700,000 (estimated)
| gross          = 
}}
The Little Traitor is an independent family drama film written and directed by Lynn Roth.  Based on the novel Panther in the Basement by author, Amos Oz, the movie takes place in Palestine in 1947, just a few months before Israel becomes a state.  
 State of Israel.

==Plot== ruled by the British, tells how 12-year-old Proffi (Ido Port) befriends the hated enemy, in the form of English Sergeant|Sgt. Dunlop (Alfred Molina), and sticks with him despite the suspicions of friends and family.

==Cast==
*Alfred Molina as Sergeant Dunlop, a British Soldier
*Ido Port as Avi Leibowitz, nicknamed ‘Proffy’
*Rami Heuberger as Proffy’s Father
*Gilya Stern as Miriam, Proffy’s Mother
*Guy Krasner as Chita, Proffy’s friend
*Yonatan Lahat as Ben Hur, Proffy’s friend
*Boris Reinis as Chita’s Father
*Lior Sasson as Mr. Hochberg
*Theodore Bikel as the Interrogator
*Anat Klausner as Yardena, Proffy’s Babysitter
*Dafna Melzer as Rachel, Proffy’s love interest

==Production==
The Little Traitor was a co-production between Evanstone films (Israel) and Panther Productions (US).  What appealed the most to writer/director Lynn Roth was sharing the story of a, “boy (who) finds himself caught between his hatred of "the British"—whom he dreams of destroying—and his growing affection for a member of the detested enemy.”  {{Citation
 | title = Press Notes
 | publisher = Regent Releasing
 | url = http://www.regentreleasing.com/littletraitorpress/
 | accessdate = 5 August 2013}} 

On the third day of production, while filming in Jerusalem, war broke out between Israel and Lebanon.  Director Roth was afraid that after years of painstakingly putting together this film, it would all crumble in an afternoon, but the Israeli cast and crew insisted that the production go on.  When Alfred Molina’s representatives were intent on getting him out of the country, Molina casually responded, "What are you talking about? The movie is still shooting, and besides, I just had the best pita and hummus Ive ever eaten in my life."  {{Citation
 | title = About the Film
 | publisher = The Little Traitor
 | url = http://www.thelittletraitor.com/about.shtml
 | accessdate = 5 August 2013}} 

==Release==
The Little Traitor had a limited theatrical release and was distributed in the United States by Westchester Films. {{Citation
 | title = The Little Traitor: Company Credits
 | publisher = IMDb
 | year = 2009
 | url = http://www.imdb.com/title/tt0818170/companycredits?ref_=tt_dt_co
 | accessdate = 4 August 2013}} 
It also had a theatrical release in Canada, Israel, Mexico and Costa Rica.  It was the longest running and highest grossing film to date at the Movies of Delray theatre in Palm Beach County, Florida. {{Citation
 | last = Sills
 | first = Matt
 | title = The Little Traitor Interview
 | publisher = Movie Times
 | date = 10 June 2010
 | url = http://www.mrmovietimes.com/movie-news/the-little-traitor-interview/
 | accessdate = 4 August 2013}} 
 Showtime for pay cable. {{Citation
 | title = Showtime: The Little Traitor
 | publisher = Showtime
 | url = http://www.sho.com/sho/movies/titles/137884/the-little-traitor#/index
 | accessdate = 4 August 2013}} 
Warner Brothers purchased the movie for Video on Demand. {{Citation
 | last = Netherby
 | first = Jennifer
 | title = WB Signs Indie VOD Deals
 | publisher = Variety
 | date = 5 August 2009 
 | url = http://www.gravitasventures.com/variety-wb-signs-indie-vod-deal-with-gravitas/ 
 | accessdate = 4 August 2013}} 

==Awards & Festivals==
*Miami International Film Festival
*Haifa International Film Festival
*Palm Beach International Film Festival – Audience Choice Award for Best Feature Film
*Palm Springs International Film Festival- Best of the Fest Award
*Humanitas Prize – Nomination for Best Feature Film Screenplay 
*Armenian International Film Festival for Children & Youth – Best Feature
*Munich International Film Festival
*AFI Dallas International Film Festival
*Stockholm International Film Festival
*Los Angeles Jewish Film Festival – Best Narrative Award 
*Atlanta Jewish Film Festival - Audience Award for Best Narrative Feature
*New York International Independent Film and Video Festival - Best Feature Award

==Soundtrack==
The Little Traitor original motion picture soundtrack was composed by Deborah Lurie and released on CD and iTunes in 2011. {{Citation
 | title = The Little Traitor – Original Soundtrack
 | publisher = iTunes
 | url = https://itunes.apple.com/us/album/little-traitor-original-soundtrack/id341435334
 | accessdate = 2 August 2013}}  Dear John, 9 (2009 animated film)|9, and the remake of Footloose (2011 film)|Footloose.  As a string arranger, she contributed to the film musicals, Fame (2009 film)|Fame and Dreamgirls (film)|Dreamgirls.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 