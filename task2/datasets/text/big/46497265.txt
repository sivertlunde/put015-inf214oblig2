The Two Sergeants (1913 film)
{{Infobox film
| name = The Two Sergeants 
| image =
| caption =
| director = Eugenio Perego
| producer =Ernesto Maria Pasquali 
| writer = Theodore Baudouin dAubigny (play)   Eugenio Perego
| starring = Alberto Capozzi   Umberto Paradisi   Orlando Ricci
| music = 
| cinematography = 
| editing =
| studio = Pasquali Film
| distributor = Pasquali Film
| released = 11 July 1913
| runtime = 67 minutes
| country = Italy Italian intertitles
| budget =
| gross =
}} play of the same title, which has been made into several films. It is set during the Napoleonic Wars.

==Cast==
* Alberto Capozzi as Capitano Derville / Guglielmo  
* Umberto Paradisi as Sergente Roberto  
* Orlando Ricci as Maresciallo  
* Egidio Candiani as Valentino  
* Giovanni Cuisa as Servente Tomasso 
* Giovanni Enrico Vidali as Aiutante Valmore  
* Michele Cuisa as Gustavo  
* Maria Gandini as Mamma Pia 
* Laura Darville 
* Cristina Ruspoli   
* Emilia Vidali

== References ==
 

==Bibliography==
* Riccardo Redi. Cinema muto italiano: 1896-1930. Fondazione Scuola nazionale di cinema, 1999. 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 

 

 