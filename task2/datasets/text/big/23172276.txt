Hide and Seek (1964 film)
 
  Dvd Release April 2014

{{Infobox film
| name           = Hide and Seek
| image          = "Hide_and_Seek"_(1964_film).jpg
| caption        = 
| director       = Cy Endfield
| producer       = 
| writer         = Robert Foshko Harold Greene (novel) David Stone
| starring       = Ian Carmichael Curd Jürgens Janet Munro George Pravda Maggie dAbo
| music          = Gary Hughes Muir Mathieson
| cinematography = Gilbert Taylor
| editing        = Thelma Connell British Lion Universal Picture
| released       =  
| runtime        = 90 min
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}

Hide and Seek is a 1964 British thriller film directed by Cy Endfield. 

==Plot==
A reserved British astronomer is drawn out of his quiet life and into an affair of international espionage behind the Iron Curtain.

==Cast==
* Ian Carmichael as David Garrett 
* Curd Jürgens as Hubert Marek 
* Janet Munro as Maggie 
* George Pravda as Frank Melnicker 
* Kieron Moore as Paul 
* Hugh Griffith as Wilkins 
* Derek Tansley as Chambers
* Esma Cannon as Tea Lady 
* Kynaston Reeves as Hunter  Edward Chapman as McPherson
* Frederick Peisley as Cottrell  John Boxer as Secretary at Ministry

==Critical reception==
The New York Times called the film, "a pleasantly diverting, terribly British, sometimes contrived melodrama, that is true to its title but hardly the best of this genre to come along."  

==References==
 

==External links==
* 
* 
* 
 

 
 
 
 
 
 
 


 
 