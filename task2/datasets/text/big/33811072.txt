Bazaar (1949 film)
{{Infobox film
| name           = Bazaar
| image          =
| alt            =
| caption        =
| director       = K. Amarnath
| producer       = K. Amarnath
| writer         = K. Amarnath
| screenplay     = Shyam  Yakub
| music          = Shyam Sunder
| cinematography = M. R. Navalkar
| editing        =
| studio         = Madhukar Pictures
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}

Bazaar is an Indian Hindi language family-drama film of year Bollywood films of 1949|1949. The film is directed by K. Amarnath and produced under the banner Madhukar Pictures. The story was written by K. Amarnath, while the dialogue and lyrics were by Qamar Jalalabadi with music by Shyam Sunder.   

==Cast== Shyam
* Nigar Sultana Yakub
* Gope 
* Cuckoo 
* Badri Prasad
* Amir Banu
* Misra

==Music==
The music of the film was composed by Shyam Sunder and the songs wetre sung by Mohammed Rafi and Lata Mangeshkar.
* Ye Hai Duniya Ka Bazaar
* Shaheedo Tumko Mera Salaam
* Challa De Ja Nishani
* O Jaanewale Chaand Jara Muskurake
* Pi Aaye Aakar Chal Bhi Diye
* Saajan Ki Galiyan
* Ae Mohobbat Unse Milne

==References==
 

==External links==
* 

 
 
 

 