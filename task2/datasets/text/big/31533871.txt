The Witty Sorcerer
 
 
{{Infobox film
| name           = The Witty Sorcerer
| image          = 
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 左慈戲曹
| simplified     = 左慈戏曹
| pinyin         = Zuǒ Cí xì Cáo}}
| director       = Lai Pak-hoi
| producer       = 
| writer         = Luo Guanzhong
| starring       = Lai Pak-hoi Xu Menghen
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Silent
| budget         = 
| gross          =
}}
The Witty Sorcerer, also known as Zuo ci xi cao, is a 1931 Hong Kong historical comedy-drama film, directed by Lai Pak-hoi. It was released on 14 March 1931 after Lais other film The Pain of Separation and starred Lai himself and Xu Menghen.    It is based on a story in Luo Guanzhongs historical novel Romance of the Three Kingdoms about Zuo Ci playing tricks on Cao Cao (played by Lai Pak-hoi). It was one of the earliest locally-filmed Hong Kong feature films to become successful on a grand scale.   

==See also== List of media adaptations of Romance of the Three Kingdoms

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 