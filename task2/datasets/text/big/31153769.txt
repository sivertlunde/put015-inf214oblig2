Thirichadi
{{Infobox film 
| name           = Thirichadi
| image          =
| caption        =
| director       = Kunchacko
| producer       = M Kunchacko
| writer         = Kanam EJ S. L. Puram Sadanandan (dialogues)
| screenplay     =
| starring       = Prem Nazir Sheela Adoor Bhasi Adoor Pankajam
| music          = R Sudarsanam
| cinematography =
| editing        =
| studio         = Excel Productions
| distributor    = Excel Productions
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, directed and produced by Kunchacko . The film stars Prem Nazir, Sheela, Adoor Bhasi and Adoor Pankajam in lead roles. The film had musical score by R Sudarsanam.   

==Cast==
 
*Prem Nazir
*Sheela
*Adoor Bhasi
*Adoor Pankajam
*Devaki
*Kaduvakulam Antony
*Kanchana
*Kottarakkara Sreedharan Nair
*N. Govindankutty
*Pankajavalli
*S. P. Pillai
 

==Soundtrack==
The music was composed by R Sudarsanam and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Indulekhe   || K. J. Yesudas, P Susheela || Vayalar Ramavarma || 
|-
| 2 || Indulekhe   || K. J. Yesudas, P Susheela || Vayalar Ramavarma || 
|-
| 3 || Kadukolam Theeyundengil || K. J. Yesudas, CO Anto || Vayalar Ramavarma || 
|-
| 4 || Kalppakappoonchola || K. J. Yesudas, S Janaki || Vayalar Ramavarma || 
|-
| 5 || Paathi Vidarnnaal || P Susheela || Vayalar Ramavarma || 
|-
| 6 || Poo Pole || P Susheela || Vayalar Ramavarma || 
|-
| 7 || Vellathaamara Mottu || K. J. Yesudas, P Susheela || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 