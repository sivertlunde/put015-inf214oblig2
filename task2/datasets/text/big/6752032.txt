The Lookout
{{Infobox film name            = The Lookout image           = LookoutPoster.jpg caption         = Promotional movie poster for the film director        = Scott Frank writer          = Scott Frank starring        = Joseph Gordon-Levitt Jeff Daniels Matthew Goode Isla Fisher Bruce McGill producer        = Walter Parkes Laurence Mark Gary Barber Roger Birnbaum music           = James Newton Howard cinematography  = Alar Kivilo editing         = Jill Savitt studio          = Spyglass Entertainment distributor     = Miramax Films released        = March 30, 2007 runtime         = 98 minutes language  English
|budget          = $16 million  gross           = $5,371,181 
}} 2007 crime Get Shorty, starring Joseph Gordon-Levitt, Jeff Daniels, Matthew Goode, and Isla Fisher.
 Buena Vista International elsewhere.

==Plot== combine stalled on the road. Two occupants of the car are killed while Chris and his girlfriend Kelly survive.  However, the crash leaves Chris with lasting mental impairments, including anterograde amnesia, along with some anger management issues.

Four years later, hes in classes to learn new skills, including the simple sequencing of daily tasks to compensate for his inability to remember, and keeps notes to himself in a small notebook. Challenged by a tough case manager to build a life despite his injuries, he is emotionally supported by his roommate, a blind man named Lewis, but receives only financial support from his wealthy family. Chris works nights, cleaning a small-town bank.  Aside from Lewis his only friend is Ted, a seemingly clumsy Sheriffs Deputy who checks in on Chris regularly. Chris repeatedly tries to convince the banks manager, Mr Tuttle, to allow him to apply for a teller job, to no avail. Chris soon comes under the scrutiny of a gang planning to rob the bank. Their leader, Gary, who knew Chris from high school and resented his wealth and popularity as a hockey star before his accident, befriends him and uses a young woman, Luvlee Lemons, to seduce him. Taunted by the gang about the limitations of his life since the accident, Chris initially goes along with the scheme. His frustrations trickle down into confrontations with his friends, Lewis and Ted.

When the gang arrives the night of the robbery, Chris tells them he has changed his mind.  But they tell him its too late and force him to empty the vault at gunpoint.  His friend Ted, the deputy, stumbles into the robbery while delivering doughnuts to Chris, and triggers a shootout. The deputy and two of the gang members, Marty and Cork, are killed. Meanwhile Chris escapes in the getaway car, and when he realizes hes got the money they stole, he returns compulsively to the site of his accident, where he buries the money roadside. Gary is seriously wounded and gets away with the other bank robber, Bone. When Chris returns to his apartment he sees the lights on and realizes something is wrong, and when he calls discovers Gary and Bone have taken Lewis hostage to get the money back.  Chris, using his new sequencing skills, hatches a plan to stay alive and save his friend. But the robbers catch him napping at the place they arranged to meet, and they force him to take them to the site of the buried cash.

While Chris digs in the snow to retrieve the money, Garys condition is rapidly deteriorating. Chris gives one of two bags to Bone, who is preparing to execute Lewis, but Chris uses the shotgun he stashed in the other bag to shoot and kill Bone before he can react. Gary collapses and dies. Chris returns the money and turns himself in, but the investigation by the FBI concludes that he was not responsible due to his medical state - and because the robbers failed to disconnect the video surveillance in the bank, allowing the FBI to see the gang forcing Chris to act at gunpoint.

In the aftermath, Chris and Lewis reconcile, and open a restaurant together with a loan from the bank. Chris hopes Kelly will forgive him for the loss of her leg in the accident, and that one day he will find the courage to talk to her again.

==Cast==
*Joseph Gordon-Levitt as Chris Pratt
*Jeff Daniels as Lewis
*Matthew Goode as Gary Spargo
*Isla Fisher as Luvlee Lemons, a woman Gary uses to seduce Chris
*Carla Gugino as Janet, Chris caseworker
*Bruce McGill as Robert Pratt, Chris father
*Alberta Watson as Barbara Pratt, Chris mother
*Alex Borstein as Mrs. Lange
*Sergio Di Zio as Deputy Ted
*David Huband as Mr. Tuttle
*Laura Vandervoort as Kelly
*Greg Dunham as Bone
*Morgan Kelly as Marty
*Aaron Berg as Cork
*Tinsel Korey as Maura
*Suzanne Kelly as Nina
*Brian Edward Roach as Danny
*Martin Roach as Loan Officer
*Ofield Williams as Reggie
*Julie Pederson as Attractive Woman in Bar
*Stephen Eric McIntyre as Bartender
*Janaya Stephens as Alison Pratt, Chris sister
*Marc Devigne as Cameron Pratt, Chris brother

==Production== Millennium Library, and the exterior of Chris apartment was filmed in the Exchange District and various historic sites in Winnipeg including the Bank of Montreal (1911–13), the Ambassador Apartments (1909), the interior of the Market Building (1899) and the James Ashdown House at 529 Wellington (1913). The Ambassador Apartments appear on the films cover.

==Reception==
From critics the film has earned an aggregated score of 87% at Rotten Tomatoes (166 reviews),  with the critical consensus "The Lookout is a genuinely suspenseful and affecting noir due to the great ensemble cast and their complex, realistic characters," a 73/100 at Metacritic (33 reviews),  and "B" at Yahoo Movies (13 reviews).  Particularly favorable reviews came from Richard Roeper and Leonard Maltin, who praised the film as "the best movie so far" of the first half of 2007.  

The Lookout won the award for Best First Feature at the 2008 Independent Spirit Awards.

==Soundtrack==
The score was composed by James Newton Howard and was his first collaboration with director Scott Frank. Frequent collaborators Stuart Michael Thomas and Clay Duncan are credited with additional music. The score was orchestrated by Brad Dechter, Stuart Michael Thomas, and Chris P. Bacon, who also conducted. Several songs were featured including "One Big Holiday" and "Lay Low", both performed by My Morning Jacket.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   at Filmmaker Magazine

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 