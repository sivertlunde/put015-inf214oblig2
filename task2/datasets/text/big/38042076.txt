Cheran Chozhan Pandian
{{Infobox film
| name           = Cheran Chozhan Pandian
| image          = 
| image_size     =
| caption        = 
| director       = Senthamizhan
| producer       = R. Krishnamurthy
| writer         = Senthamizhan
| starring       =  
| music          = Soundaryan
| cinematography = I. Rajendraprasad
| editing        = K. P. K. Ravichandar
| distributor    =
| studio         = K. C. R. Creations
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}}
 1998 Tamil Tamil comedy film directed by Senthamizhan. The film features Ranjith (actor)|Ranjith, Anand Babu and Vadivelu in lead roles. The low-budget film, produced by R. Krishnamurthy, had musical score by Soundaryan and was released on 25 December 1998.   

==Plot==
 Mansoor Ali Khan). The three men decide to become good persons but they cross a corrupted politicians path named Swamy (Vasu Vikram). The three friends fall in love with Gayathri (Mohini (Tamil actress)|Mohini), Swamys sister, who becomes their close friend.

==Cast==
 Ranjith as Cheran
*Anand Babu as Chozhan
*Vadivelu as Pandian Mohini as Gayathri
*Lashmipathiraj as Krishna Mansoor Ali Khan
*Vasu Vikram as Swamy
*Junior Balaiah
*Oru Viral Krishna Rao as Vanagamudi
*Madhan Bob
*S. R. Veeraraghavan Chitti Babu
*Thavakkalai
*Alphonsa
*Shakeela

==Soundtrack==

{{Infobox album |  
| Name        = Cheran Chozhan Pandian
| Type        = soundtrack
| Artist      = Soundaryan
| Cover       = 
| Released    = 1998
| Recorded    = 1998 Feature film soundtrack |
| Length      = 24:05
| Label       = 
| Producer    = Soundaryan
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Soundaryan. The soundtrack, released in 1998, features 5 tracks with lyrics written by Alamelu Muthulingam.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Jeans Pantukulle || Subha || 4:16
|- 2 || Kadalicha Ponnu || S. N. Surendar, Arunmozhi, Shahul Hameed || 4:13
|- 3 || Madras Electric || Mano (singer)|Mano, Swarnalatha || 5:12
|- 4 || Nam Vazhyile || Jayachandran, K. S. Chithra || 5:12
|- 5 || Thirunelveli Halwa || Sirkazhi G. Sivachidambaram, Krishnaraj, Harini || 5:12
|}

==References==
 

 
 
 
 
 
 


 
 