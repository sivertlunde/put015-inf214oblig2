Ankhiyon Ke Jharokhon Se
 
 
{{Infobox film  
| name           = Ankhiyon Ke Jharonkhon Se
| caption        = Film Poster
| director       = Hiren Nag
| producer       = Tarachand Barjatya
| writer         = Vrajendra Gaur Sachin Ranjeeta
| music          = Ravindra Jain
| distributor    = Rajshri Productions
| released       = 16 March 1978 
| language       = Hindi 
| country        = India
}}
 1978 Bollywood|Hindi Romantic film, Sachin and Love Story by Erich Segal.

==Plot==
Arun (Sachin (actor)|Sachin), the son of a Barrister, and self-proclaimed prince of the male students finds his pride and huge ego crushed when he stands second in the Terminal Examination. He loses to Lily Fernandes, the simple and modest daughter of an ordinary nurse in a private nursing home. Arun takes this as a defeat, who then decides to nurse a personal vendetta to try and crush Lilys growing popularity in school. Lily, however, tolerates him and his friends remarks and sarcasm as she bears no grudge against him.

As time goes on, they gradually discover the basic qualities of their opponents, one becoming the admirer of the other. They visit beautiful places together on weekends to get to know each other better. Lilys mother is the first person to notice the love blossoming between her daughter and Arun. She is worried, because even though Arun is madly in love with her daughter, social status has to be considered. Despite this, Aruns father approves and presents a proposal to Lilys mother to allow their children to marry.
 
When everything is going well, Lily falls ill and is discovered to have leukemia. Everyone tries to save the girl, and Lily and Arun act as if nothing was wrong. Everything seems fine for a while, but Lilys condition grows worse. She dies in Aruns arms as she makes him promise to rise in life so that she would be at peace.

==Cast== Sachin - Arun Prakash Mathur
* Ranjeeta - Lily Fernandes
* Iftekhar - Dr. Pradhan
* Urmila Bhatt - Ruby Fernandes
* Master Raju.

==Soundtrack==
Ravindra Jain composed the music and wrote the songs for this movie. The songs have been quite popular, especially the title song.  Ravindra Jain was nominated for Filmfare Awards in two categories for this song, namely, Best Lyricist and Best Musician.

*"Ankhiyon Ke Jharokon Se", the title song sung by Hemlata.
* "Jaate Huye Yeh Pal Chin", sung by the composer Ravindra Jain himself.
* "Ek Din Tum Bahut Bare Banoge"
* "Kai Din Se Mujhe Sung by Hemlata & Shailendra singh" Jaspal Singh. It is based on the Doha (poetry)|dohas from historic Indian poets such as Abdul Rahim Khan-I-Khana|Rahim, Kabir, Tulsidas, and Bihari (poet)|Bihari.

==Sequel==
In 2011 Rajshri Productions roped in the star pair of their 1978 film -Sachin Pilgaonkar and Ranjeeta Kaur to produce a sequel for Ankhiyon Ke Jharokhon Se. The sequel was called Jaana Pehchana and showcased Aruns life after Lily’s death. Ranjeeta Kaur portrayed a new character who is a look-alike of Lily. The film was released on September 16, 2011.  

==Other==
The film was nominated for Best Actress, Best Film, and Best Direction at the 1978 Filmfare Awards. Ranjeetas poignant portrayal of the character was critically acclaimed.

==References==
 

==External links==
*  

 
 
 
 
 