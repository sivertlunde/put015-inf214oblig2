Ludwig II (1922 film)
{{Infobox film
| name           = Ludwig II
| image          = 
| image_size     = 
| caption        = 
| director       = Otto Kreisler 
| producer       = 
| writer         = Alfred Deutsch-German
| narrator       = 
| starring       = Olaf Fjord   Thea Rosenquist   Eugen Preiß   Franz Scherer
| music          = 
| editing        =
| cinematography = 
| studio         = Helios Film
| distributor    = 
| released       = 24 March 1922
| runtime        = 
| country        = Austria
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
|  Austrian silent silent historical Bavarian monarch Ludwig II. 

==Cast==
* Olaf Fjord as Ludwig II 
* Thea Rosenquist as Baronesse Tirnau 
* Eugen Preiß as Richard Wagner 
* Franz Scherer as Kaiser Franz Josef I. 
* Gina Puch-Klitsch as Kaiserin Elisebeth 
* Josef Glücksmann as Bruder Otto 
* Paul Askonas as Luitpold  Josef Schreiber as Bismark 
* Ferdinand Onno

==References==
 

==Bibliography==
* * Dassanowsky, Robert. Austrian Cinema: A History. McFarland & Company Incorporated Publishing, 2005.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 