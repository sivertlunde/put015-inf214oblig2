Dragonquest (film)
 
{{Infobox film
| name           = Dragonquest
| image          = dragonquest.jpg
| alt            =
| caption        = Mark Atkins
| producer       = David Michael Latt David Rimawi Paul Bales
| writer         = Brian Brinkman Micho Rutare
| starring       = Marc Singer Jason Connery Daniel Bonjour Jay Beyers Jack Goldenberg
| music          = Sanya Mateyas Chris Ridenhour
| cinematography = Mark Atkins
| editing        = Brian Brinkman
| studio         = The Asylum
| distributor    = The Asylum
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $600.000
| gross          =
}}
Dragonquest is a 2009 fantasy film produced by The Asylum. It stars legendary sword-and-sandal veteran Marc Singer as Maxim, a warrior who must guide a young farm boy across a dangerous terrain to retrieve the missing gems from an ancient medallion in order to bring peace to the land. The film also stars Jason Connery as The King and The Terminators Brian Thompson as the villain Kirill. The film is a "Joke version" of the film "Eragon (film)|Eragon"
 novel of the same name by Anne McCaffrey or the Dragon Quest series of role-playing video game|role-playing games by publisher Square Enix.

==Plot== Sorcery is feudal society.

An evil wizard, wishing to conquer the world for his own, unleashes a seemingly unstoppable dragon onto the world to terrorize the peasant folk into submission. The creature can withstand any attack from humans, and appears indestructible.

However, a small resistance movement has formed to defeat the wizard utilizing the strength of a fabled   in the land that can match the fire dragon in strength, and so the vigilantes set out in their quest for the dragon to save the  .

==Cast==
*Marc Singer.........Maxim
*Brian Thompson......Kirill
*Jason Connery.......Gurion
*Daniel Bonjour......Arkadi
*Jennifer Dorogi.....Katya
*Russell Reynolds....Anson
*Richard Lund........King Agmar
*Jack Goldenberg.....Frank

==See also==
*Wizards (film)|Wizards - A similar film made in 1977 by Ralph Bakshi

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 