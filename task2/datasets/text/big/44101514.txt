The Wedding Pact
{{Infobox film
| name           = The Wedding Pact
| image          = The Wedding Pact poster.jpg
| caption        = Theatrical release poster 
| director       = Matt Berman
| producer       = Jonathan B. Schwartz Eric Scott Woods
| writer         = 
| starring       = Haylie Duff Chris Soldevilla
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2014
| runtime        = 92 min
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
The Wedding Pact is a 2014 American romance film directed by Matt Berman and starring Haylie Duff and Chris Soldevilla.

The film was released on DVD on February 3, 2014. 

==Plot==
Mitch and Elizabeth get to know and befriend each other in college. Mitch falls in love with Elizabeth, but doesnt decide to tell her. After college, Elizabeth throws a phrase that if after 10 years neither of them finds a partner in life, they will marry. After ten years, Mitch hasnt gotten married and discovers that Elizabeth hasnt either. Still remembering their promise, Mitch makes a cross country journey to visit Elizabeth.

==References==
 

 
 
 
 


 