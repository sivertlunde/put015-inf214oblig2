The Vanishing Virginian
{{Infobox film
| name           = The Vanishing Virginian
| image          =The Vanishing Virginian.jpg
| director       = Frank Borzage
| producer       = Edwin H. Knopf
| writer         = Jan Fortune
| based on       = The Vanishing Virginian by Rebecca Yancey Williams
| starring       = Frank Morgan Kathryn Grayson David Snell (score) Earl Brent (adaptation) Lennie Hayton (director)
| cinematography = Charles Lawton
| editing        = James E. Newcom
| art director   = Cedric Gibbons
| set decorating = Edwin B. Willis
| gowns          = Kalloch
| male costumes  = Gile Steele
| distributor    = Metro-Goldwyn-Mayer
| released       =  February 1942
| runtime        = 97 minutes
| country        = United States English
| budget         = $499,000  . 
| gross          = $905,000 
| preceded_by    =
| followed_by    =
}}
The Vanishing Virginian is a 1942 film based on the novel by Rebecca Yancey Williams and set in Lynchburg, Virginia|Lynchburg, Virginia, in 1913.

==Synopsis==
Based on the true story of turn-of-the-century Robert Yancey, lawyer and ever-popular politician in Virginia. The film starts with the statement, "This is the story of a vanishing era when simple men so loved their country, their families and their friends that America became a better place in which to live. Such a man was Capn Bob Yancey." 

This film, based on a 1931 memoir by Rebecca Yancey Williams, explores society roles in plantations of the "Old Dominion" around Lynchburg and their socio-economic implications, as well as the movement for women’s suffrage, among other things. It is also the towns story, and various eminent Virginians cross the pages, including  .  

==Box Office==
According to MGM records the film earned $589,000 in the US and Canada and $316,000 elsewhere resulting in a profit of $63,000. 

==References==
 

==External links==
* 
*  at TCMDB
 

 
 
 
 
 
 
 
 


 