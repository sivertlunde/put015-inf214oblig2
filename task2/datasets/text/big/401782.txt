The Calcium Kid
{{Infobox film
| name           = The Calcium Kid
| image          = Calcium kid.jpg
| caption        = Promotional poster for the film
| director       = Alex De Rakoff
| producer       = Natascha Wharton
| writer         = Derek Boyle Alex De Rakoff Raymond Friel Michael Lerner,  Billie Piper Mark Heap
| music          = Boilerhouse Boys
| cinematography = David M. Dunlap Jon Harris WT 2  Productions
| distributor    = Universal Pictures
| released       =  
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $109,202 
}} The Milky Way and its remake, Danny Kaye´s vehicle, The Kid from Brooklyn.

== Plot ==
Jimmy Connelly (Orlando Bloom) is a milkman who is thrust into the spotlight after a brutal fighter, Pete Wright (Tamer Hassan), gets injured. Under manager Herbie Bush (Omid Djalili), Jimmy must fight the middleweight champion of the world, Jose Mendez (Michael Peña). 

Within the lead up to the fight with Mendez, Jimmy has to take part in a press conference, in which Bush recommends him to give Mendez fighting talk. However, this leads to Jimmy being seen as a fascist and a disgrace to England after being seen as racist.

After Jimmy realises the harm Bush has caused, he calls off the fight. As Bush and the training team look for Jimmy, they find him cleaning his old milk cart. In a negotiation to help the town in order to fight, Jimmy is persuaded to participate. 

In the locker room, minutes before the fight, as Jimmy and his crew walk backstage, Wright holds a shotgun to them, whilst the camera crew is also held hostage, filming the event. Wright leads them to a room were Jimmy is forced to fight Mendez, who is also held hostage. After getting substantially battered by Mendez, Jimmy manages to fight back, but then Wright smacks his head with the gun. Then Wright goes to shoot Mendez, leaving him praying for his life. Then Jimmy punches Wright, leaving him knocked out. 

With the film crew recording the event, Jimmy becomes a hero, not only in his home town, but in Britain.

==Cast==
*Orlando Bloom as Jimmy Connelly
*Michael Peña as José Mendez
*Billie Piper as Angel
*Rafe Spall as Stan Parlour
*Tamer Hassan as Pete Wright
*Omid Djalili as Herbie Bush David Kelly as Paddy OFlanagan
*Mark Heap as Sebastian Gore-Brown Michael Lerner as Artie Cohen John Joyce as Jon Wright
*Cathy Dunning as Joyce Wright
*Frank Harper as Clive Connelly
*Ronni Ancona as Pat Connelly
*Peter Serafinowicz as Radio DJ

==Reception==
The film received negative reviews from critics, and has a rare overall rating of 0% on Rotten Tomatoes based on 5 reviews. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 