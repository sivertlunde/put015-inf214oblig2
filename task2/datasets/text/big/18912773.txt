The Closet (2007 film)
 
{{Infobox film name = The Closet image = The Closet (2007 film) poster.jpg director = Kin-Nam Cho producer = starring = Francis Ng Yang Chihfei  Michelle Ye  Cheung Siu-Fai distributor = released =   country = Hong Kong runtime = language = Cantonese Mandarin
}} 2007 Hong Kong film directed by Kin-Nam Cho.

==Plot==
Due to his rebellious nature, Lo Fei suffered a traumatic childhood with abuse and maltreatment from his bad-tempered but famous sculptor father. Apart from receiving corporal punishment and verbal abuse, he was often locked up inside the wardrobe. The dark childhood turned Lo into a weirdo frequently tormented by the pain of childhood memories as well as hallucination of his dead father. Lo betrayed his fathers artistic career and chose to become a magician and performer of extremities instead, to prove his own capabilities. During one of his death-defying show, Lo was seriously wounded by shocks of his childhood nightmares. Not knowing the truth behind it, his girlfriend Lei thought he was stretching his physical strength too far. She therefore took him to a quiet retreat in the suburb to recuperate. On a windy moonless night, the five of them went through a terrifying catastrophe.

==Cast==
*Francis Ng as Lo Fei
*Yang Chih-fei as Lei Hiu-king
*Michelle Ye as Mang Ping
*Cheung Siu-Fai as Ngok Fung

==External links==
*  at SinaHK

 
 
 
 
 
 
 


 
 