Naan Avanillai 2
{{Infobox film
| name           = Naan Avan Illai 2
| image          = 
| alt            =  
| caption        =  Selva
| producer       = Hitesh Jhabak
| writer         = Selva Jeevan Sangeetha Lakshmi Rai Shweta Menon Sruthi Prakash Rachana Maurya Mayilsamy
| music          = D. Imman
| cinematography = Balamurugan
| editing        = 
| studio         = 
| distributor    = Nemichand Jhabak Productions
| released       =  
| runtime        = 125 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Tamil romance romantic mystery Naan Avanillai Jeevan reprises the lead role, while the female roles are enacted by Sangeetha, Lakshmi Rai, Shweta Menon and Sruthi Prakash and Rachana Maurya.  D. Imman, who did the film score for the first part of the film, replaced Vijay Antony as the music director.  The film was released on 27 November 2009.

==Plot==
Annamalais photograph appears in a newspaper along with an interview of Maria (Rachana Maurya), who claims herself to be a saint propounding a new faith. She describes him as her God, Vaali. A mafia-turned-spiritualist she attributes her transformation to him in the interview. This prompts three women to come calling Maria all the way.

The three women were cheated by him recently and they probe Maria of Annamalais whereabouts. The three women, Sakhi (Sruthi Prakash), Nisha (Swetha Menon) and an actress Deepa (Lakshmi Rai) try to prove Maria that he was a cheat. However she doesnt believe them.

Meanwhile, Annamalai suffers bleeding injuries in an accident. Mahi (Sangeetha), a Lankan woman who runs a restaurant, nurses him back to health.

On seeing Mahi separated from his daughter by her late husbands (Five Star Krishna|Krishna) family, he decides to reunite them. He robs money with which he achieves his purpose. Meanwhile, the trio tracks down Annamalai and confronts him. Annamalai goes all the way to a Church where speaks in his own style and make the girls believe that he is not the one they are searching for.

==Cast== Jeevan as Joseph Fernandez/Annamalai/Magesh/Aravind/Barath and Sarath/Vali/David
* Sangeetha as Mahalakshmi
* Lakshmi Rai as Deepa
* Shweta Menon as Nisha
* Sruthi Prakash as Saki
* Rachana Maurya as Maria Krishna as Mahalakshmis Husband
* Mayilsamy as Annamalais Assistant
* Rajkapoor as Deepas Brother

==Release & Reception==
Naan Avanillai 2 gets A certificate from Central Board of Film Certification. The movie received extremely negative reviews upon release.  The movie became an average grosser at the box office.

==Soundtrack==
The soundtrack features six songs and one instrumental that composed by D. Imman.The songs received mixed reviews from critics.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! No !! Song !! Artist(s) !! Lyrics
|-
| 1
| "Manmadha Leelai"
| Shail Hada, Nithyasree, Benny Dayal
| Pa. Vijay
|-
| 2
| "Naangu Kangal"
| Javed Ali, Shreya Goshal
| Pa. Vijay
|-
| 3
| "Thooyavaney"
| Neha Bhasin
| Viveka
|-
| 4
| "O Mariya"
| D. Imman, Jyotsna Radhakrishnan
| Vaali
|-
| 5
| "Sollamaley"
| Sadhana Sargam
| Yugabharathi
|-
| 6
| "Baaga Unnara"
| Udit Narayan, Suvi
| Viveka
|-
| 7
| "Southern Aroma"
| Andrea Jeremiah
| Instrumental
|}

==References==
 

 

 
 
 
 
 


 