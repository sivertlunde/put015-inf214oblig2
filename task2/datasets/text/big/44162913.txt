Loose Loose Arappiri Loose
{{Infobox film
| name           = Loose Loose Arappiri Loose
| image          =
| caption        =
| director       = Prassi Malloor
| producer       =
| writer         = Mohankumar Prassi Malloor (dialogues)
| screenplay     = Prassi Malloor
| starring       = Jagathy Sreekumar Kuthiravattam Pappu Mala Aravindan Pattom Sadan
| music          = Darsan Raman
| cinematography = V.E Gopinath
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, directed by Prassi Malloor. The film stars Jagathy Sreekumar, Kuthiravattam Pappu, Mala Aravindan and Pattom Sadan in lead roles. The film had musical score by Darsan Raman.   

==Cast==
 
*Jagathy Sreekumar as Jagathy
*Kuthiravattam Pappu as Pappu
*Mala Aravindan as Maala 
*Pattom Sadan
*K. P. A. C. Azeez
*Babitha
*Bindu Ghosh
*Justin
*Nellikode Bhaskaran
*Sabitha Anand
*Vettoor Purushan as jagathy
 

==Soundtrack==
The music was composed by Darsan Raman and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chakravarthini (Parody) || Satheesh Babu, Vincent Gomes || P. Bhaskaran || 
|-
| 2 || Kallanmaare || K. J. Yesudas, Balagopalan Thampi, Vincent Gomes || P. Bhaskaran || 
|-
| 3 || Vasantha Rajaneepushpam || Vani Jairam || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 