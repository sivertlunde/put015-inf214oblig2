Naked (1993 film)
 
 
{{Infobox film
| name           = Naked
| image          = 307 naked.jpg
| alt            =
| caption        = The Criterion Collection DVD cover
| director       = Mike Leigh
| producer       = Simon Channing Williams
| writer         = Mike Leigh
| starring       = David Thewlis Lesley Sharp Katrin Cartlidge
| music          = Andrew Dickson Dick Pope
| editing        = Jon Gregory
| studio         = Thin Man Films
| distributor    = First Independent Films   Fine Line Features  
| released       =  
| runtime        = 131 minutes  
| country        = United Kingdom
| language       = English
| budget         =
| gross          = $1,769,306 (USA)
}}
Naked is a 1993 British black comedy-drama film written and directed by Mike Leigh. Before this film, Leigh was known for subtler comedic dissections of middle-class and working-class manners. Naked was more stark and brutal than his previous works. Leigh relied heavily on improvisation in the making of the film, but little actual ad-libbing was filmed; lengthy rehearsals in character provided much of the script. Almost all the dialogues were filmed as written. The film received largely favourable reviews. Filming took place in London from 9 September to 16 December 1992.

==Plot==
After a sexual encounter with a married woman in an alley in Manchester turns into a rape, Johnny steals a car and flees for Dalston, "a scrawny, unpretentious area" in the east of London, to seek refuge with his former girlfriend, fellow Mancunian Louise.

Intelligent, educated and eloquent, Johnny is also deeply embittered and egotistical: he will fight and provoke anyone he meets to prove his superiority. His tactics of choice in verbal interaction are based on a particular form of intellectual bullying, uniformly directed at people less cultured than himself, and summed up in domineering, scholastic barrages drawn from eclectic sources. His overall behaviour is reckless, self-destructive and at times borderline Sadomasochism|sadistic, and shows a penchant for aggressive sexual domination at least twice throughout the film. He seduces Louises flatmate, Sophie, simply because he can, but soon gets tired of her and embarks on an extended latter-day odyssey among the destitute and despairing of the United Kingdoms capital city.
 nihilist or Transhumanism|transhumanist) at long and lyrical length to anyone who will listen, whether Archie, a Scottish boy yelling "Maggie!" at the top of his voice he comes across in Brewer Street, or Brian, a security guard of acres of empty space, a post-Modernist gas chamber, whom Johnny marks down as having, the most tedious job in England.  All the while, the sinister presence of his ex-girlfriends psychopathic landlord, Sebastian Hawks (aka Jeremy G Smart), lurks in the background. Johnny eventually suffers horribly at the hands of thugs in the most casual manner; and when the primary tenant of the flat, Sandra, returns from a trip overseas, Johnny is compelled to leave, to throw himself back into the world as he has ostensibly done so many times before.

===Johnnys condition===
It is subtly hinted throughout the movie that Johnnys unusual personality and behaviour could be the result of a variety of (presumably undiagnosed and untreated) medical conditions, including manic depression and whatever it is that causes him to experience episodic, severe headaches. In the scene in the top room of the flat, where an obviously concussed Johnny mistakes the landlord for someone else, Johnny seems to portray a young boy fearful of physical and maybe even sexual abuse from an adult, hinting also at a probable very early cause for his outlook and behaviour. These conditions are certainly affecting him physically, so much so that one of the characters he meets thinks he is about 40 years old, when he is only 27.

==Cast==
 
* David Thewlis as Johnny
* Lesley Sharp as Louise Clancy
* Katrin Cartlidge as Sophie
* Greg Cruttwell as Jeremy G. Smart / Sebastian Hawk
* Claire Skinner as Sandra the Nurse Peter Wight as Brian, the security guard
* Ewen Bremner as Archie the Scotsman with a tic
* Elizabeth Berrington as Giselle
* Gina McKee as The Cafe Girl
 

==Notes== Alfie in Swinging Sixties had degenerated into the nauseated Nineties."  Leigh had captured, according to Coveney, something of the anxiety, rootless Cynicism (contemporary)|cynicism, and big-city disaffection of the time.

Thewliss background reading for the part of Johnny included  . 

Other echoes, cinematic and literary, that critics have detected in the film include William Shakespeares Hamlet and Jean Renoirs Boudu Saved from Drowning (one of Leighs favourite films).  Shakespeares hero Hamlet in a black and inky coat,   socially untethered but burdened with useless knowledge and a vicious, bullying line in repartee." Of the precedent of "idiosyncratic, character-driven film-making" in Renoirs Boudu, Michael Coveney has observed: "Both Naked and Boudu explore the tension between the domesticated and the anarchic (this is a central theme, probably the theme running through Leighs work), and focus this tension in the tragicomedy of a central character." 
 Unity Theatre for a fortnight, and directed the first production of Halliwells Little Malcolm and his Struggle Against the Eunuchs. According to Coveney, "Malcolm Scrawdyke is clearly a precursor of Johnny in Naked. Scrawdyke was a loutish art student and absurd ideologue from Huddersfield who had trouble with girls and a hatred for his teachers...the play shared a deeply felt schoolboy coarseness with Alfred Jarrys Ubu Roi, a piece originally written as a vicious attack on a loathed mathematics master." 
 Habonim ("the Builders"), the international socialist Jewish youth movement he joined as a schoolboy. After the film was released Leigh heard from a retired schoolmaster at Stand Grammar in Prestwich who had written the song for a school review in 1950. 

==Reception==
The film generated mostly positive reviews from critics. Review aggregator  , another review aggregator, assigned the film a weighted average score of 84 (out of 100) based on 20 reviews from mainstream critics, considered to be "universal acclaim." 
 Mrs Thatcher claims does not exist." On the character of Johnny, he notes: "He likes no one, least of all himself, and he dislikes women even more than men, relapsing into sexual violence as his misogyny takes hold. He is perhaps redeemable, but only just. And not by any woman in our immediate view." He praised the directing and performances, singling out David Thewlis, mentioning that he "plays   with a baleful brilliance that is certain to make this underrated, but consistently striking, actor into a star name."  Malcolm later added: "  is, at his worst, a cold, desperate fish. His redeeming feature is that he still cares." 

Roger Ebert of the Chicago Sun-Times gave the film four out of four stars and analysed the message behind the title, saying it "describes characters who exist in the world without the usual layers of protection. They are clothed, but not warmly or cheerfully. But they are naked of families, relationships, homes, values and, in most cases, jobs. They exist in modern Britain with few possessions except their words." He praised the directing, noting: "Mike Leighs method of working is well-known. He gathers his actors, suggests a theme, and asks them to improvise situations. A screenplay develops out of their work. This method has created in Naked a group of characters who could not possibly have emerged from a conventional screenplay; this is the kind of film that is beyond imagining, and only observation could have created it." He concluded: "This is a painful movie to watch. But it is also exhilarating, as all good movies are, because we are watching the director and actors venturing beyond any conventional idea of what a modern movie can be about. Here there is no plot, no characters to identify with, no hope. But there is care: The filmmakers care enough about these people to observe them very closely, to note how they look and sound and what they feel." 

==Criticism==
  (Louise) responded: "There are a lot of people who dont go to art house cinemas who do have deeply troubled lives and are at risk ... We do actually live in a misogynistic, violent society and there are a lot of women in abusive relationships who find it very difficult to get out of them. And a lot of men, too." Coveney denied the relevance of the criticism: "Is there no room for irony, for the idea that in depicting horror in the sex war an artist is exposing them, not endorsing them? And who says that Sophie is an unwilling doormat or that Louise is a doormat at all? It is clear that the latter is taking serious stock of her relationship with Johnny. She exhibits both patience and tenderness in her dealings with him, whereas she finally pulls a knife on Jeremy." 

==Awards and nominations==
* Cinéfest: Best International Film (1993) (won)
*   (won)   
* Cannes Film Festival (1993): Palme dOr (nominated)  Best Actor – David Thewlis (1993) (won) 
* New York Film Critics Circle Awards: Best Actor – David Thewlis (1993)
* Toronto International Film Festival: Metro Media Award (1993) (won)
*   (1994) (nominated)
* Evening Standard British Film Awards: Best Actor – David Thewlis (1994) London Critics Circle Film Awards ALFS Award: British Actor of the Year – David Thewlis (1994)
* National Society of Film Critics Awards: Best Actor – David Thewlis (1994)
* BAFTA Awards Alexander Korda Award for Best British Film (1994) (nominated)
* Independent Spirit Awards: Best Foreign Film (1994) (nominated)

==References==
 
*  
*  

==External links==
 
*   at the British Film Institute
*  
*  
*  
*  
*  
*   locations
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 