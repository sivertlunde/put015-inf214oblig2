Swings or Roundabouts
:This article is about the film. It is not to be confused with Manfred Kargas play by the same name.
{{Infobox film
| name           = Jacke wie Hose
| image          = Jacke wie Hose (film poster).jpg
| image size     = 
| caption        = 
| director       = Eduard Kubat
| producer       = Werner Dau
| writer         = Jan Koplowitz 
| narrator       =
| starring       = Irene Korb, Günther Simon
| music          = Joachim Werzlau 
| cinematography = Fritz Lehmann 
| editing        = Ruth Schreiber
| studio         = DEFA
| distributor    = PROGRESS-Film Verleih
| released       = 30 April 1953
| runtime        = 87 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 East German comedy film, directed by Eduard Kubat. It was released in List of East German films|1953.

==Plot==
After a new government law forbids women to operate heavy machinery in steel factories, a group of female workers that is determined to lift the ban proposes a competition to their male counterparts: those who will produce the greatest quantity of steel will win. The men are certain that they will be victorious. One of them, Ernst Hollup, is angered by his wifes involvement with the other team, and he demands that she will resign and become a housewife. The women develop a wagon that carries the molten iron to the steel furnace and greatly simplifies their work. They win the competition, as well as the respect of the men. The government lifts the ban.

==Cast==
*Irene Korb as Hilde Hollup
*Günther Simon as Ernst Hollup
*Fritz Diez as Hellwand
*Johanna Bucher as Manja
*Ruth-Maria Kubitschek as Eva
*Charlotte Küter as Johanna
*Regine Lutz as Lisa
*Lieselotte Merbach as Zenzi
*Wolfgang Erich Parge as Triebel
*Herbert Richter as Meider
*Theo Shall as Mühlberger
*Edwin Marian as Peter
*Harry Hindemith as Steiger

==Production==
The film was planned after the East German public reacted negatively to the wave of highly ideological films released during the early 1950s. It was intended to present the ideas of socialism in a humorous fashion, and to provide entertainment for the audience. 

==Reception==
East German film critic Leo Menter wrote that "Jacke wie Hose is a typical example for the films made in the peace-loving, Socialist countries... it is no gangster film, no opium for the masses; it is based on true life."  On the other hand, Rolf Behrends,  another East German journalist, asked "why are the characters shown solely as functionaries in the factory? The film lacks any private, human side." Heinz Kersten. Das Filmwesen in der sowjetischen Besatzungszone. Deutscher Bundes-Verlag (1954). ASIN B004CF9VL8. Page 37.  The West German Catholic Film Service regarded Jacke wie Hose as a "propagandistic... didactic, picture."  Author Andrea Rinke cited it as an early example for the expression of Feminist ideas in East German cinema, much before the subject became popular with DEFA filmmakers in the 1970. 

==References==
 

==External links==
*  
*  on ostfilm.de.

 
 
 
 
 
 