The Warrior's Heart
{{Infobox film
| name         = The Warriors Heart
| image        = Kr hj plakat.jpg
| caption        = Norwegian poster
| writer       = Arthur Johansen	 Leidulv Risan
| starring     =  Espen Skjønberg |
  director     = Leidulv Risan
| producers     = Elin Erichsen and Gunnar Svensrud
| cinematography = Harald Paalgard
| composer      = 
| editor       = Einar Egeland 
  distributor  = 
| released     = Norway 1992
| runtime      = 102 minutes
| country      = Norway
| language     = Norwegian
| budget       =
| music        =
}}
The Warriors Heart ( ) is a 1992 Norwegian film, directed by Leidulv Risan and starring Anneke von der Lippe, Peter Snickars, Thomas Kretschmann, Bjørn Sundquist and Iren Reppen. It was screened out of competition at the 1992 Cannes Film Festival.   

==Plot==
A love story during the Second World War in Scandinavia. Ann Mari, a Norwegian, works as a nurse in the Winter War of 1939/40 between Finland and the Soviet Union. She falls in love with the Finnish soldier Markus. The war stops temporarily, and they settle down in northern Norway. Norway gets occupied by Germany and Markus leaves Ann Mari, as Finland goes to war again to win back the lost territory. A short time later Markus seems to be dead and Ann Mari falls in love with the German soldier Maximilian. But Markus soon returns alive. After a struggle for Ann Mari the three take refuge from the Germans to Sweden, but Sweden deports foreign deserters.

==Cast==
* Anneke von der Lippe as Ann Mari Salmi
* Peter Snickars as Markus Salmi
* Thomas Kretschmann as Lt. Maximillian Luedt
* Mona Hofland as Ann Maris mother
* Juha Muje as Olli
* Bjørn Sundquist as Karl Simonnaes
* Solfrid Heier as Mrs. Simmonaes
* Iren Reppen as Kari Simonnaes
* Paul-Ottar Haga as Claus
* Per Christensen as Vicar
* Werner Heinrich Möller as Claus
* Christoph Künzler as Captain

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 