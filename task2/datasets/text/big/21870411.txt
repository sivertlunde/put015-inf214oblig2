Rock All Night
{{Infobox film
| name = Rock All Night
| image = Poster of the movie Rock All Night.jpg
| caption = Original film poster by Albert Kallis
| director = Roger Corman
| producer = Roger Corman
| writer = Charles B. Griffith
| based on = TV play The Little Guy by David P. Harmon
| starring = Dick Miller Abby Dalton Russell Johnson Mel Welles
| musical direction = Buck Ram
| cinematography = Floyd Crosby
| editing = studio = Sunset Productions
| distributor = American International Pictures
| released =  
| runtime = 62 min.
| country = United States English
| imdb = 0050906
}}

Rock All Night is a 1957 American International Pictures (AIP) film produced and directed by Roger Corman based on a 25-minute television episode of The Jane Wyman Theatre from 1955 called "Little Guy." It stars Dick Miller, Russell Johnson and Abby Dalton. The film was released as a double feature with Dragstrip Girl.

==Plot==
Two escaping killers hide out in a club called the Cloud Nine and hold the bartender and clients hostage.  Amongst the patrons are a nervous singer (Abby Dalton), a boxer, his wife, and manager, an extortionist, a loud thug and his girlfriend, as well as a small man who can determine peoples real (as opposed to posed) personalities and has no fear (Dick Miller).

==Production==
Roger Corman bought the rights to "Little Guy" from Jane Wyman and gave it to Charles B. Griffith to expand into a feature. According to one account, Griffith says he wrote the script over the weekend:
 I cut it up with a pair of scissors, this original screenplay, and added new characters like Sir Bop, which was to be played by Lord Buckley, but Mel Welles ended up playing it because Buckley was out of town. Mel wrote his own “hiptionary” for sale in the theatre to go with it. Dick Miller was in the Dane Clark part. He was the little guy of the title. The music was by Buck Ram, The Platters and those people all doing their hit songs. Of course, no songs were written in 24 hours... I would just put down “musical number here”. The girl has her dialogue with the guys and then turns around to sing a song. It was up to them what she sang, up to Roger.  
According to another account, what happened was two days before filming there was a change in the schedule of The Platters and they were only going to be available for one day so Griffith rewrote the script in 48 hours. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p79 

The film was at one stage known as RocknRoll Girl. Tin Star Filming Set in Black and White
Los Angeles Times (1923-Current File)   11 Nov 1956: F12.   Dane Clark was originally sought to play the lead. FILM EVENTS: Prize TV Play Will Be Filmed
Los Angeles Times (1923-Current File)   06 Oct 1956: B2.  

Songwriter and manager Buck Ram offered a slew of his musical talent such as The Platters, accompanied by the Eddie Beal sextet with Eric Dolphy on baritone saxophone, The Blockbusters, and Nora Hayes to AIP in return for having the sole rights to a soundtrack album for the film. Corman filmed Rams acts lip-synching their tunes on a separate set that comprise the beginning of the film. Rock All Night was made in five days  and originally appeared as a double feature with Dragstrip Girl. 
 stock company and a writer for Buckley, Mel Welles imitated Buckley in the role of "Sir Bop". Wells wrote a dictionary of hip talk for the film. 

Dick Miller, a former Navy boxing champion, played the role Dane Clark did in the television show, with Russell Johnson playing the role that Lee Marvin originated. 

Despite the short shooting schedule and minimal locations (only two sets), Corman always regarded the movie as a personal favourite. Ed. J. Philip di Franco, The Movie World of Roger Corman, Chelsea House Publishers, 1979 p 8 

==Cast==
* Abby Dalton as Julie
* Dick Miller as Shorty
* Russell Johnson as Jigger
* Ed Nelson as Pete
* Jeanne Cooper as Mabel

==Notes==
    

==References==
* Corman, Roger and Jerome, Jim How I Made a Hundred Movies in Hollywood and Never Lost a Dime 1998 Da Capo Press
* Naha, Ed The Films of Roger Corman: Brilliance on a Budget 1984 Olympic Marketing

==External links==
*  

 
 

 
 
 
 
 
 
 