Lord of Illusions
 
{{Infobox film
| name           = Lord of Illusions
| image          = Lordillusionsposter.jpg
| caption        = Theatrical release poster
| director       = Clive Barker
| producer       = Clive Barker Steve Golin Joanne Sellar Sigurjón Sighvatsson
| screenplay     = Clive Barker
| based on       =   Kevin J. OConnor Famke Janssen J. Trevor Edmond Daniel von Bargen Joseph Latimore
| music          = Simon Boswell
| cinematography = Ronn Schmidt
| editing        = Alan Baumgarten
| studio         = United Artists Seraphim Films
| distributor    = MGM/UA Distribution Company
| released       = August 25, 1995
| runtime        = 109 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $13,249,614 (Domestic) 
}} Kevin J. OConnor, Famke Janssen and Daniel von Bargen.

Barker asserts that the directors cut of this film is his definitive version, as the theatrical release does not represent his true vision. 

== Plot ==
 
In the Mojave Desert in 1982, a man named Nix (von Bargen) has gathered a cult in an isolated house. Nix calls himself "The Puritan" and has the ability to use real magic (paranormal)|magic. He plans to sacrifice a young girl that he has kidnapped. While Nix is preaching to his followers, a group of former cult members –Swann (OConnor), Pimm (Traylor), Quaid (Latimore) and Jennifer Desiderio (Tousey)– arrive to stop him. After the initial confrontation with the cultists, Nixs assistant Butterfield escapes and Swann is attacked by Nix. Nix uses his powers, inducing in Swann the vision of “Flesh with a Gods Eyes", causing him see his friends as monstrous figures. The kidnapped girl shoots Nix through the heart with Swanns gun. Quaid and Jennifer shoot him until he falls. Swann snaps out of his vision and fastens an ironwork mask over Nixs head, who appears to die. Swann declares that they will bury Nix so deep that no one will ever find him.

Thirteen years later, New York City private detective Harry DAmour (Bakula) is investigating an insurance fraud case in Los Angeles.  DAmour has a long-standing interest in the occult, and has some renown from his involvement with a recent exorcism. During the investigation, DAmour discovers a fortune teller shop owned by Quaid, where he is relentlessly attacked by a skinhead with unusual strength.  After finally killing his attacker, DAmour finds Quaid suffering from multiple stab wounds. As he dies, Quaid warns DAmour that “The Puritan” is coming. DAmour reports the incident to the police, but they cant find the bald mans body.

Swann, now a famous stage illusionist, lives in a Beverly Hills mansion with his wife, Dorothea (Janssen).  Swann informs Dorothea that Nixs followers have murdered Quaid. Dorothea suggests they hire DAmour to investigate the murder. Swanns assistant, Valentin (Swetow), collects DAmour, informing DAmour that Swann is a performer, not a "real" magician. They attend Quaids funeral, where DAmour is introduced to Dorothea. DAmour agrees to work for her and she invites him to Swanns magic show. Butterfield and the bald man are in the audience. Swann performs a new death-defying illusion, but it goes wrong and he is killed on stage. DAmour investigates the accident backstage, and is attacked by Butterfield and the bald man. DAmour impales the bald man on a Theatrical property|prop.

DAmour goes to The Magic Castle, where he meets Billy Inferno, another illusionist. Inferno has heard of Nix described as a legend, but he believes that Nix taught Swann to use real magic. DAmour questions Dorothea, but she denies ever having heard the name "Nix". DAmour learns that Jennifer Desiderio is now institutionalized. DAmour visits her and sees that she is irrevocably insane. When he mentions Swann, she becomes violently agitated and screams warnings about Nix and "The Puritan". Jennifer flees into the street, where she is hit and killed by a car. DAmour enlists Infernos help to get into The Repository, a special room in the Castle that supposedly contains every magic secret known to man. After perusing the books there, DAmour learns more about Nix and confirms that Swanns "illusions" involved real magic.

DAmour questions Dorothea again. She reveals that she was the girl that Nix kidnapped, and that she married Swann because she felt she owed her life to him. Dorothea and DAmour make love. Afterwards he is attacked by a man engulfed in fire. DAmour suspects that the attack was Swanns attempt at revenge, so he opens his coffin and finds that the body inside is fake. Valentin explains that he helped Swann fake his death. DAmour agrees to allow Valentin and Dorotheas ruse to continue. DAmour spots Swann in disguise at his own funeral and follows him. Swann, who knows about their tryst, attacks DAmour again with magic. DAmour convinces Swann to help him put an end to Nixs cult.

Butterfield attacks Valentin and kidnaps Dorothea. Butterfield uses Dorothea as a hostage to force Valentin to recover Nixs body. Butterfield stabs Valentin and takes the corpse back to the old house in the desert. There, a new group of cultists are gathered to bring about Nixs resurrection. Butterfield removes the iron mask and Nix regains consciousness. Swann and DAmour, acting on information given by the dying Valentin, arrive. Butterfield attacks DAmour. Swann attacks Butterfield and tells DAmour to rescue Dorothea. Nix, instructing his followers to prepare to receive his wisdom, opens a hole in the ground beneath him and Dorothea and turns the earth to quick sand that swallows the cultists. He declares that only Swann is worthy of receiving his knowledge.

DAmour finds Nix and Dorothea just as Nix is dropping her into the hole. DAmour rescues Dorothea. As they flee, DAmour and Dorothea are attacked by Butterfield, whom DAmour kills. Swann agrees to act as Nixs disciple in an effort to stall for time. Nix sees through the ruse and attacks Swann with magic. Nix invokes "Flesh with a Gods Eyes" on DAmour. Dorothea shakes him back to consciousness. She finds DAmours gun and shoots Nix in the head. Nix uses his powers to fling Dorothea aside, and then begins to transform into a hideous creature. Swann uses magic to help DAmour deliver a final blow to Nix. Nix is distracted and falls into the hole, now filled with lava. Dorothea holds Swann in her arms, as he succumbs to his injuries. DAmour sees that Nix, hideously injured but alive, is summoning a whirlwind, which ends up sealing the hole. Dorothea and DAmour escape the house and walk into the desert.

==Cast==
* Scott Bakula as Harry DAmour  Kevin J. OConnor as Philip Swann 
* Famke Janssen as Dorothea Swann
*Ashley Lyn Cafagna as young Dorothea
* Joseph Latimore as Caspar Quaid
* Wayne Grace as Loomis
* Daniel von Bargen as Nix
* Jordan Marder as Ray Miller
* Barry Del Sherman as Butterfield
* Joel Swetow as Valentin

== Themes == Dancing in the Dark, performed by Diamanda Galás. 

Cult leader Nix is explicitly compared to cult leader Charles Manson in the film. Like Manson and his The Manson Family|family, Nix and his followers believe he has the power to return from the dead. Both Manson and Nix have followers who live in an unpleasant place out in the desert and cut off their hair at their leaders request. 

== Critical reception ==
  Allmovie was critical of the film, writing that it "starts off strong with an intriguing premise, but then goes quickly nowhere". 

== In popular culture == Colombian Necktie on Colombian Necktie (GOArge Mix).   
* Front Line Assembly side-project, Noise Unit, uses samples from the film on their album Drill on a number of tracks: The Drain, Dominator, Miracle, Eye Burner.  Heavy metal band Nevermore uses word samples from the movie in two tracks from their album Dreaming Neon Black.
* Speedcore artist Lord Lloigor uses dialog samples from the movie in his track Ray Of Darkness.
* Magician Criss Angel demonstrated the falling swords scene in an episode of Criss Angel BeLIEve.

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 