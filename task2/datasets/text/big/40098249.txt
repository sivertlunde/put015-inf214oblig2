The Bit Player
{{Infobox film
| name           = The Bit Player
| image          = 
| caption        = 
| director       = Jeffrey Jeturian
| producer       = Joji Alonso John Victor Tence Jeffrey Jeturian Vilma Santos-Recto Ferdinand Lapuz
| writer         = Jeffrey Jeturian Antoinette Jadaone Zig Dulay
| screenplay     = Zig Dulay Antoinette Jadaone  Jeffrey Jeturian
| cinematography = Lee Meily
| editing        = Glenn Ituriaga Zig Dulay
| music          = Addiss Tabong
| starring       = Vilma Santos
| studio         = Cinemalaya Quantum Films
| distributor    = Star Cinema
| released       =  
| runtime        = 111 minutes
| country        = Philippines Filipino English
| budget         = 
| gross          = 
}}
The Bit Player (  Drama comedy|drama-comedy film by Jeffrey Jeturian. The film stars Vilma Santos as Loida Malabanan, who spends her days dreaming of her big break while working with the country’s best small screen actors.   The film competed under the Directors Showcase sidebar of 9th Cinemalaya Independent Film Festival. It topped the box office in all four venues of the said festival.  The film was released in mainstream theaters by Star Cinema on August 14, as part of their 20th Anniversary offering.

The film will also had its international premiere at the 2013 Toronto International Film Festival, set from September 5 to 15, under Contemporary World Cinema section.  

== Plot ==
The film follows a seemingly usual day in the life of Loida Malabanan (Vilma Santos) as she embarks on yet another shooting day of a soap opera as an Extra (acting)|extra. As the shoot goes on, we get a glimpse of the truth in the ruling system of the production as well as the exploitation of the marginalized laborers like her.  

== Cast ==
*Vilma Santos as Loida Malabanan
*Fatima Centeno
*Marlon Rivera
*Vincent de Jesus
*Ruby Ruiz
*Tart Carlos
*Hazel Faith Dela Cruz
*Marian Rivera
*Piolo Pascual
*Cherie Gil
*Pilar Pilapil
*Tom Rodriguez
*Eula Valdez
*Cherry Pie Picache
*Richard Yap
*Terence Baylon

== Awards ==
*9th Cinemalaya Independent Film Festival (Directors Showcase)
**Special Jury Prize
**Audience Choice
**NETPAC Award
**Best Actress – Vilma Santos
**Best Supporting Actress – Ruby Ruiz
**Best Screenplay
**Nominated–Best Film
Gawad Tanglaw Awards 2014
**Best Actress - (Vilma Santos)
13th Dhaka International Film Festival
**Best Actress - (Vilma Santos)
2015 New York Festivals
**Best Feature Film - Bronze 

== References ==
 

== External links ==
* 
* 
* 

 
 
 
 
 