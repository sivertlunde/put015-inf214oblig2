Red Corner
 
{{Infobox film
| name           = Red Corner
| image          = Red corner poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Jon Avnet
| producer       = {{Plainlist|
* Jon Avnet
* Jordan Kerner
* Charles Mulvehill
* Rosalie Swedlin
}} Robert King
| starring       = {{Plainlist|
* Richard Gere
* Bai Ling
* Bradley Whitford
}}
| music          = Thomas Newman
| cinematography = Karl Walter Lindenlaub
| editing        = Peter E. Berger
| studio         = {{Plainlist|
* Metro-Goldwyn-Mayer
* Avnet/Kerner Productions
}}
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| country        = United States
| language       = English
| runtime        = 122 minutes
| budget         = $39 million
| gross          = $22,415,440  
}}
 1997 American mystery Thriller thriller film Robert King, the film is about an American businessman on business in China who ends up wrongfully on trial for murder. His only hope of exoneration and freedom is a female defense lawyer from the country.    The film received the 1997 National Board of Review Freedom of Expression Award (Richard Gere, Jon Avnet) and the NBR Award for Breakthrough Female Performance (Bai Ling). Ling also won the San Diego Film Critics Society Award for Best Actress.   

==Plot== American businessman conspiracy and political corruption|corruption. Shen manages to convince several high-ranking Chinese officials to release evidence that proves Moores innocence. Moore is quickly released from prison while the conspirators that framed him are arrested. At the airport, Moore asks Shen to leave China with him, but she decides to stay as there are many more things to change in China. However, she admits that meeting Moore has changed her life, and she considers him part of her family now. They both then share a heartfelt hug on the airport runway.

==Cast==
* Richard Gere as Jack Moore
* Bai Ling as Shen Yuelin
* Bradley Whitford as Bob Ghery
* Byron Mann as Lin Dan
* Peter Donat as David McAndrews
* Robert Stanton as Ed Pratt Tsai Chin as Chairman Xu
* James Hong as Lin Shou
* Tzi Ma as Li Cheng
* Ulrich Matschoss as Gerhardt Hoffman
* Richard Venture as Ambassador Reed
* Jessey Meng as Hong Ling
* Roger Yuan as Huan Minglu
* Chi Yu Li as General Hong
* Henry O as Procurator General Yang
* Kent Faulcon as Marine Guard
* Jia Yao Li as Director Liu
* Yukun Lu as Director Lius Associate
* Robert Lin as Director Lius Interpreter   

==Production==
Red Corner was shot in Los Angeles using elaborate sets and CGI rendering of 3,500 still shots and two minutes of footage from China. In order to establish the films verisimilitude, several Beijing actors were brought to the United States on visas for filming. The judicial and penitentiary scenes were recreated from descriptions given by attorneys and judges practicing in China and the video segment showing the execution of Chinese prisoners was an actual execution. The individuals providing the video and the descriptions to Avnet and his staff took on a significant risk by providing it.   

==Reception==
Upon its theatrical release in the United States, Red Corner received generally negative reviews. On Rotten Tomatoes the film received a 32% positive rating from top film critics based on 22 reviews, and a 49% positive audience rating based on 7,795 reviews.   

Cynthia Langston of Film Journal International responded to the film, "So unrealistic, so contrived and so blatantly Hollywood that Gere cant possibly imagine hes opening any eyes to the problem, or any doors to its solution, for that matter." 

In his review in the Los Angeles Times, Kenneth Turan described Red Corner as a "sluggish and uninteresting melodrama that is further hampered by the delusion that it is saying something significant. But its one-man-against-the-system story is hackneyed and the points it thinks its making about the state of justice in China are hampered by an attitude that verges on the xenophobic."   
 Western stereotypes sexuality (as in those of The World of Suzie Wong) as well as the hoariest stereotyping.   

Total Film gave a 3/5 star rating describing stating that Red Corned was "A semi-powerful thriller let down by pedestrian direction and a lacklustre Richard Gere. Even so, newcomer Bai Ling and an unblinking stare at the Draconian Chinese legal system prevent Red Corner from being an open-and-shut case" and describes some scenes depicting the harsh treatment of the Chinese legal system  as "thought provoking" yet describes the rest as only "mildly entertaining". 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 