The Nutcracker (1993 film)
{{Infobox film
| name           = The Nutcracker
| image          = The Nutcracker (1993 film) poster.JPG
| alt            = 
| caption        = Theatrical release poster
| director       = Emile Ardolino
| producer       = Robert Hurwitz Robert A. Krasnow
| writer         = Susan Cooper  (narration) 
| based on       = Peter Martinss stage production of The Nutcracker 
| narrator       = Kevin Kline
| starring       = Darci Kistler Damian Woetzel Kyra Nichols Bart Robinson Cook Macaulay Culkin Jessica Lynn Cohen New York City Ballet
| music          = Pyotr Ilyich Tchaikovsky
| cinematography = Ralf D. Bode 
| editing        = Girish Bhargava  Elektra Entertainment Regency Enterprises
| distributor    = Warner Bros.
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $19 million   
| gross          = $2,119,994 
}} Christmas musical film directed by Emile Ardolino. The film stars Darci Kistler, Damian Woetzel, Kyra Nichols, Wendy Whelan, Margaret Tracey, Gen Horiuchi and Tom Gold. The film was released by Warner Bros. on 24 November, 1993.

==Plot==
 

== Cast ==	
*Darci Kistler as The Sugarplum Fairy
*Damian Woetzel as The Sugarplum Fairys Cavalier
*Kyra Nichols as Dewdrop
*Wendy Whelan as Coffee
*Margaret Tracey as Marzipan
*Gen Horiuchi as Tea
*Tom Gold as Candy Cane
*Lourdes López as Hot Chocolate
*Nilas Martins as Hot Chocolate
*William Otto as Mother Ginger
*Peter Reznick as Fritz
*Karin von Aroldingen as Grandparent
*Edward Bigelow as Grandparent
*Heather Watts as Frau Stahlbaum
*Robert LaFosse as Dr. Stahlbaum
*Bart Robinson Cook as Drosselmeier
*Jessica Lynn Cohen as Marie Stahlbaum
*Macaulay Culkin as The Nutcracker / Drosselmeiers Nephew
*Kevin Kline as Narrator
*Katrina Killian as Harlequin
*Roma Sosenko as Columbine
*Michael Byars as Soldier
*Robert D. Lyon as Mouse King 

==Reception==

===Critical response===
The Nutcracker received generally mixed reviews from critics. Based on seven reviews, the film holds a rotten rating of 57% on review aggregator Rotten Tomatoes, with an average rating of 6.3/10.  The film was criticized by James Berardinelli for not capturing the excitement of a live performance; he wrote that it "opts to present a relatively mundane version of the stage production... utilizing almost none of the advantages offered by the (film) medium."  Roger Ebert of the Chicago Sun-Times criticized the film for not adapting the dance for a film audience and also its casting of Culkin who, he writes, "seems peripheral to all of the action, sort of like a celebrity guest or visiting royalty, nodding benevolently from the corners of shots."  In The Washington Post, Lucy Linfield echoed Eberts criticism of Culkin, stating that "its not so much that he cant act or dance; more important, the kid seems to have forgotten how to smile... All little Mac can muster is a surly grimace." She praised the dancing, however, as "strong, fresh and in perfect sync" and Kistlers Sugar Plum Fairy as "the Balanchinean ideal of a romantic, seemingly fragile beauty combined with a technique of almost startling strength, speed and knifelike precision."  The New York Times Stephen Holden also criticized Culkin, calling his performance the films "only serious flaw", but praised the cinematography as "very scrupulous in the way it establishes a mood of participatory excitement, then draws back far enough so that the classic ballet sequences choreographed by Balanchine and staged by Peter Martins can be seen in their full glory." 

===Box office===
During its theatrical run the film grossed $2,119,994.  In North America, the film opened at number 16 in its first weekend with $783,721. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 