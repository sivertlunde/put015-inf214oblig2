Gulabi Talkies
 
{{Infobox film
| name           = Gulabi Talkies
| image          = Gulabi poster DVD cover.jpeg
| alt            = 
| caption        = DVD cover
| film name      = 
| director       = Girish Kasaravalli
| producer       = Amrutha Patil   Basanth Kumar Patil
| writer         = 
| screenplay     = Girish Kasaravalli Vaidehi
| based on       =  
| narrator       = 
| starring       = Umashree   K. G. Krishna Murthy   M. D. Pallavi
| music          = Isaac Thomas Kottukapally
| cinematography = S. Ramachandra
| editing        = M. N. Swamy   S. Manohar
| studio         = 
| distributor    = Basanth Productions
| released       =  
| runtime        = 123 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
Gulabi Talkies is a 2008 Indian Kannada language film by acclaimed Indian director Girish Kasaravalli. It is based on a short story by the same name by Kannada writer Vaidehi (Kannada writer)|Vaidehi.  
 National Award for Best Actress for her performance in the film.

==Plot==
The film is set in the late 1990s among the fishing communities around Kundapura, in the southwestern Indian state of Karnataka. The impulsive midwife Gulabi (Umashree) is the protagonist, whose one passion is the cinema. She leads a lonely life in an island inhabited by fisher folk. Her husband Musa (K.G. Krishna Murthy), a small-time fish-selling agent, has deserted her and is living happily with his second wife Kunjipathu and their child Adda.

A family gifts her a television with a satellite dish antenna in gratitude after she attends to a difficult delivery (for which they even had to bodily remove her from a movie theatre). The arrival of the first color TV in her small island village heralds great changes in the sleepy hamlet. The women in the village begin gathering at her house once the men leave for fishing. However, a few of them stay away, since Gulabi is one of the few Muslims in the village. Yet others prefer to watch from outside her shack, without entering it.

Among the regulars at her home is Netru (singer-actress M.D. Pallavi), a girl with an absentee husband and a domineering mother-in-law, whom Gulabi befriends and becomes a confidante to. But Netru disappears and Gulabi is blamed, leaving her all alone in the village.
 communalism in Karnataka provide the backdrop to the film. The communal stereotyping of Muslims following the Kargil War finds an echo in the village. The tension between the small fishermen of the village and a Muslim businessman (who is actually never shown throughout the film) with a growing fleet of commercial trawlers acquires a communal color.

The disappearance of Netru adds to the mounting tensions. The Muslims in the village flee and urge Gulabi to leave too, but she refuses and stays put in the village. Her house is vandalized and she is forcibly taken to a boat to leave the island. The young men from outside who spearhead the attack assure the villagers that Gulabis television would remain in her house.

The film ends with a scene in which two illiterate elderly women, who had hitherto refused to enter Gulabis house, going in there to watch TV (which they do not know how to switch on - they are probably unaware even that it has to be switched on).

==Awards and recognition==
* Osians Cinefan Festival of Asian and Arab Cinema, 2008
** Best Film in Indian Competition
** Best Actress in Indian Competition - Umashree

* Karnataka State Film Awards 2007-08 Best Film Best Screenplay - Girish Kasaravalli Best Actress - Umashree

* 57th National Film Awards Best Actress - Umashree      Best Feature Film in Kannada

==References==
 

==External links==
*  
* 

===Reviews===
*  
*  
*   on Indian Auteur
*  

 
 

 
 
 
 