The Italian (2005 film)
{{Infobox film name = The Italian image = The Italian.jpg caption = English-language poster director = Andrei Kravchuk producer = Andrei Zertsalov writer = Andrei Romanov starring = Kolya Spiridonov Mariya Kuznetsova Nikolai Reutov music = Alexander Knaifel cinematography = Aleksandr Burov editing = Tamara Lipartiya    distributor = Sony Pictures Classics (US)  released =   runtime = 98 minutes country = Russia language = Russian budget =  gross = 
}}
The Italian ( , Transliteration|translit.&nbsp;Italjanec) is a 2005 Russian drama film directed by Andrei Kravchuk. The screenplay by Andrei Romanov, inspired by a true story, focuses on a young boys determined search for his mother.

==Plot==
Six-year-old Vanya Solntsev lives in a desolate and rundown orphanage run by an alcoholic headmaster. When a wealthy Italian couple wanting to adopt selects him, the other children, especially his good friend Anton, envy his good fortune and name him The Italian. But when a grief-stricken mother of another boy commits suicide after returning to reclaim her son and discovering he is no longer there, Vanya fears the same fate looms for him. With the aid of some of the older boys, he retrieves his file from the office safe and learns the address of the childrens home where he previously lived. Certain the records there will identify his mother, he sets off on his quest.

Pursuing him by car as he travels by train are the corrupt Madam, who brokered his adoption, and her driver Grisha. Upon arriving in the town where the home is located, Vanya is attacked and beaten, but he breaks free and finds a bus that will take him to his destination. There he is confronted by Grisha, but he manages to elude him and make contact with the head of the home, who gives him his mothers address. Once again Grisha catches up with him, but when he realizes how determined Vanya is to find his mother, he lets him go. The boy is reunited with his mother. Through a letter Vanya wrote to Anton, who was adopted by the Italian couple instead, we learn Vanya is happy to be living with his mother again.

==Cast==
*Kolya Spiridonov as Vanya Solntsev
*Mariya Kuznetsova as Madam
*Nikolai Reutov as Grisha
*Yuri Itskov as Headmaster
*Dima Zemlyanko as Anton

==Production==
Residents of the Lesogorsky Childrens Home in Leningrad were cast as orphans in the film. 

The film premiered at the Berlin International Film Festival and was shown at the Taipei Film Festival and the Buster Childrens Film Festival in Denmark before going into theatrical release in Russia. It was shown at the Telluride Film Festival, the Toronto International Film Festival, the Morelia Film Festival in Mexico and the Paris Mon Premier Festival joden before going into limited release in the United States.

==Critical reception==
Manohla Dargis of The New York Times said, "The film’s director, Mr. Kravchuk, throws a beautiful, somewhat gauzy light over this world that gently softens its harder angles. There is something slightly magical about the lighting, almost as if this were a fantasy land from which Vanya might actually make an escape. This sense of unreality, of magical thinking and wishing, carries the story and Vanya through a remarkable journey . . . like something out of a film by Roberto Rossellini, which is very high praise indeed." 

Ruthe Stein of the San Francisco Chronicle called the film "a deeply moving experience, alternately funny and sad" and added, "Based on a real incident, it has the ring of truth. Every detail feels right, from the chill of a Russian winter to the coldness displayed by adults profiting from a trade in orphans." 
 tweens and teens in upmarket territories." 

==Awards and nominations==
The film won the Deutsches Kinderhilfswerk Grand Prix and the Glass Bear for Best Feature Film at the 2005 Berlin International Film Festival. Andrei Kravchuk won the CIFEJ Prize, awarded to the director of films especially designed for children or suitable for them, at the 2005 Carrousel International du Film in Montreal; the Cinekid Film Award at the 2005 Cinekid in Amsterdam; and the Grand Prix at the 2005 Honfleur Festival of Russian Cinema in France.
 78th Annual Academy Awards.

==References==
 

==External links==
* 
* 
* 
*  

 
 
 
 
 
 
 
 
 