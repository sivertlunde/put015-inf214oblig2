Betty Boop's Bamboo Isle
{{Infobox Hollywood cartoon|
| cartoon_name = Betty Boops Bamboo Isle
| series = Betty Boop
| image =
| caption =Betty Boop
| director = Dave Fleischer Shamus Culhane (uncredited)
| story_artist =  Bernard Wolf 
| voice_actor = Mae Questel 
| musician = The Royal Samoans
| producer = Max Fleischer (producer)
| distributor = Paramount Pictures
| release_date = September 23, 1932
| color_process = Black-and-white
| runtime = 8 mins
| movie_language = English
}}
 
Betty Boops Bamboo Isle is a 1932 Fleischer Studios Betty Boop animated short, directed by Dave Fleischer. It is now public domain.

==Plot==
 Bimbo appears on screen playing a ukulele while riding in a motorboat.  The motorboat goes faster and faster, until it crashes into a tropical island.  Bimbo flies into the air and lands in another boat with a topless (except for a strategically placed Lei (Hawaii)|lei) and dark-skinned Betty Boop in it.

Bimbo and Betty, after nearly falling down a waterfall, are flung from the boat into a clearing surrounded by hostile trees, who torment the two.  A group of savages appears, but Bimbo disguises himself by painting his face and sticking a bone in his hair.  Bimbo is treated as an honored guest, and Betty dances the hula.  A sudden rainstorm washes off Bimbos disguise, and he and Betty are chased by the savages until they reach Bettys canoe and take off down the river.  When it seems that they are alone, the two proceed to kiss in private behind an umbrella (with a convenient hole).

==Notes== Popeye the Sailor and in 1934s Betty Boops Rise to Fame.

==References==
* 

==External links==
 
*  
*   on YouTube
* 
*  on Cinemaniacal, downloadable.
*   (public domain, MPEG4, 9.6MB)
 

 
 
 
 
 
 
 
 


 