Mere Brother Ki Dulhan
 
 
{{Infobox film
| name           = Mere Brother Ki Dulhan
| image          = Mere Brother Ki Dulhan22.jpg
| caption        = Theatrical release poster
| director       = Ali Abbas Zafar
| producer       = Aditya Chopra
| writer         = Ali Abbas Zafar
| based on       =   Imran Khan Katrina Kaif Ali Zafar Tara D’Souza
| music          = Sohail Sen
| cinematography = Sudeep Chatterjee
| editing        = Ritesh Soni
| studio         = Yash Raj Films
| distributor    = Yash Raj Films
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Hindi
| budget         =     
| gross          =   worldwide   
}}

Mere Brother Ki Dulhan, (My Brothers Bride) also known by the abbreviated form MBKD, is a 2011 Indian romantic comedy film directed and written by Ali Abbas Zafar. It stars  Imran Khan, Katrina Kaif, Ali Zafar and Tara D’Souza in the lead roles. Produced and distributed by Yash Raj Films, the official theatrical trailer was released on 13 July 2011. The film released on 9 September 2011, to mixed reviews from critics. It was a box office hit grossing  .

==Plot== Imran Khan) find him a bride. Kush travels all over to find a bride, who is suitable for his brother. However, he and his friends are initially unsuccessful. Then enters Dimple (Katrina Kaif), an outgoing & loud youngster who is Kushs classmate and friend. Kush decides Dimple is the perfect bride, and after meeting each other, Dimple and Luv agree to the marriage. They then decide to get married at a place at Agra, from where the Taj Mahal is visible.
 Kanwaljit Singh) and Kushs father Colonel (Parikshat Sahni) find this out, they figure that the only way to protect their respect and pride is to agree Kush and Dimple to get married. However, Kush and Dimple lay out a condition: if Luv and Piyali can be accepted into the house, Kush will marry Dimple. The condition is accepted. In the end, Kush-Dimple and Luv-Piyali, the two lovebirds get married properly in a ceremony and are accepted joyously. During Kush-Dimples wedding, Dimple arrives on the horse instead of Kush.

==Cast==
 Imran Khan as Kush Agnihotri/Funny Maulana

* Katrina Kaif as Dimple Dixit

* Ali Zafar as Luv Agnihotri a.k.a. Bhaisahab

* Tara DSouza as Piyali Patel a.k.a. Pia
 Sufi Malhotra as Robin

* Parikshit Sahni as Colonel Agnihotri
 Kanwaljit Singh as Mr. Dilip Dixit

* Marhu Sheikh as Mrs. Agnihotri

* Uparna Marwah as Mrs. Dixit

* Arfeen Khan as Ajay Dixit a.k.a. Ajju

* Mohammed Zeeshan Ayyub as Shobhit

* Tariq Vasudeva as Rohit

* Brijendra Kala as Salman Bhai
 John Abraham as Himself  (Special appearance)  

==Production==

===Development===
{{Quote box quote  = "I always had this fear of writing till I picked up a pen and then, the words just flowed. Writing the 150 pages of my script marked the high point of my life." source = -Writer/Director Ali Abbas Zafar   width  = 30%}}
  on a number of  , this marks the second  , University of Delhi, where he acted in different plays including an adaptation of William Goldings Lord of the Flies.

Like many Bollywood films, Mere Brother Ki Dulhan has suffered allegations that it is a rip-off of an American film even before filming wrapped. According to these sources, the film rips off Dan in Real Life,   in which Steve Carells character also falls in love with his brothers girlfriend (played by Dane Cook and Juliette Binoche respectively). The film has also drawn comparison to an earlier Yash Raj Films production, Mere Yaar Ki Shaadi Hai. 

===Casting=== Imran Khans New York. She was able to take the role thanks to the postponement of Dostana 2 to which she was already committed.  Her role has been reported in the media as different than characters sheit down after initially accepting it.  It was then given to Ali Zafar (no relation to the director), an actor whod received rave reviews for his debut film Tere Bin Laden and who confirmed his participation via his Twitter account.  A popular singer, Zafar will also perform his characters singing in the film and described his casting as "a dream come true" Piyali Dasgupta  , The Times of India, 19 September 2010.  and his role as a "parallel lead role".  The film additionally stars a new actress Tara DSouza who is a former Kingfisher model.

On 17 October 2010, several media outlets reported on the casting of Preity Zinta in an unspecified "glamorous role". TNN  , The Times of India, 17 October 2010.  The actress however quickly denied the report on her official Twitter page. 

===Filming=== Punjab  and Himachal Pradesh  for a total of 45 days of filming in northern India and finally finishing the film in Mumbai.  Juggling between this film and the final shots of Tees Maar Khan and Zindagi Na Milegi Dobara, Subhash K Jha  , The Times of India, 3 October 2010.  Kaif joined the shoot on 27 September.
 Imran Khan pulling Katrina Kaif towards him while pointing a gun towards other cast members. However, Khan miscalculated the move and instead sent the gun barrel crashing into Kaifs nose, causing it to bleed profusely. Kaif was quoted as saying "I am pretty bruised but the staff feels its good luck for the film. But it was a funny scene so I think it was worth it"  and "Please dont make it seem as if Imran was careless or something. These things happen. We were shooting one of the really madcap scenes where the actors limbs acquire a life of their own. Imrans hand landed on my face"  Another incident of sorts was reported by the media in early October  and involves an emotional scene where Kaifs character slaps Khans. Kaif would have been content to slap Khan once, however he insisted on sixteen takes so that the director would have a variety of slaps to choose from. Kaif was involved in another accident a few weeks later while filming in Nabha.  Her hair got caught in a fan, and it was thanks to co-star Ali Zafars quick action that the incident didnt turn more serious. Zafar and a spot boy named Raju did however sustain minor injuries and bruises. The films title, Mere Brother Ki Dulhan, was revealed on 2 October along with a few promo pictures. Bollywood Hungama News  , Bollywood Hungama, 2 October 2010.  Ali Zafar stated that Kaif promised him that if the film made over   600 million at the box office, she would do a music video with him. 

==Release==
Yash Raj Films were first aiming for a July 2011 release,     but the release date was later pushed to 9 September 2011.    The studio drastically changed their marketing tactics with this film by revealing the title and two stills a week into filming, whereas they have previously been very secretive about sharing such information until about a couple of months before the theatrical release. It was released in Pakistan on 9 September 2011.

==Critical reception==
The film received mixed reviews from critics.   in his review for CNN-IBN called the film too predictable and said: "The film doesnt always work because it relies too heavily on silly stereotypes and clichés, and because you can see exactly where its going from the moment you settle into your seat. Im going with two out of five for director Ali Abbas Zafars Mere Brother Ki Dulhan. Aside from a few enjoyable moments, this film recycles so much that youve already seen before."  Hamara Bollywood  gave the film 4 out of 5 stars and said that Katrina Kaifs performance alone is worth paying the admission price.
Aniruddha Guha of Daily News and Analysis gave the film 1 star(s) and said "The brief must have been clear: ‘Don’t use your brains, just do what we’ve been doing for years now. It worked the last time; it’ll probably work again. If not, we’ll try again the next time.’ That’s perseverance." 

==Box office==

India

Mere Brother Ki Dulhan has had a good opening with first day of 75.0&nbsp;million net,  It had a good first weekend of 255.0&nbsp;million net.  the film held up well on Monday with business of the 37.5&nbsp;million net mark, taking its four-day business to Rs 290&nbsp;million net.  It grossed   net in the first week of its release. It was declared a hit after its net gross totalled   in 3 weeks. Thus it became the ninth highest grossing film of 2011. At the end of its theatrical run the film grossed   Cr in India.

Overseas

The film collected   from overseas markets.

==Soundtrack==
{{Infobox album 
| Name        = Mere Brother Ki Dulhan
| Type        = Soundtrack
| Artist      = Sohail Sen
| Cover       = MBKD_OSTcover.jpg
| Released    =  
| Recorded    = 2011 YRF Studios, Saba Studio Mumbai, India Feature film soundtrack
| Length      =   YRF Music Sony Music
| Producer    = Sohail Sen Khelein Hum Jee Jaan Sey (2010)
| This album  = Mere Brother Ki Dulhan (2011)
| Next album  = Ek Tha Tiger (2012)
}}
{{Album ratings
| rev1 = Bollywood Hungama
| rev1Score =   
}} Sen and Irshad Kamil|Kamil. Ali Zafar sings one song in the film.  , The Express Tribune, 3 October 2010.  The music was released on 9 August 2011. The album contains six original tracks and two remixes.

{{tracklist
| headline = Track listing
| extra_column = Artist(s)
| total_length = 35:51

| title1 = Mere Brother Ki Dulhan
| extra1 =   ) 
| length1 = 4:24

| title2 = Dhunki
| extra2 = Neha Bhasin
| length2 = 4:17

| title3 = Choomantar
| extra3 = Benny Dayal, Aditi Singh Sharma
| length3 = 4:20

| title4 = Isq Risk
| extra4 = Rahat Fateh Ali Khan
| length4 = 4:54

| title5 = Madhubala
| extra5 =  Ali Zafar, Sreeram Chandra,  Shweta Pandit
| length5 = 4:22

| title6 = Do Dhaari Talwaar
| extra6 = Shahid Mallya, Shweta Pandit
| length6 = 5:00

| title7 = Choomantar
| note7 ( Remix )
| extra7 = Benny Dayal, Aditi Singh Sharma
| length7 = 3:42

| title8 = Isq Risk
| note8 ( Risky Mix )
| extra8 = Sreerama Chandra, Neha Bhasin
| length8 = 4:39
}}

===Reception===
Music of the film got positive to mixed reviews from the critics. Joginder Tuteja of Bollywood Hungama rated the album favourably, awarding it 3.5/5, stating "Mere Brother Ki Dulhan is a winning album by all means and has in it to be widely popular in days to come. As expected, the entire soundtrack follows a fun approach without anything becoming overtly mushy or mellow. Composer Sohail Sen can be assured that he would now have a successful soundtrack to his name as well that would find a wide audience for itself."    This is the music directors third work so far,the first two being Whats Your Rashee and Khelein Hum Jee Jaan Sey.  Nikhil Hemrajani of Hindustan Times wrote, "That all eight songs in Mere Brother Ki Dulhans (MBKD) soundtrack are high-energy thumpers probably makes sense for the coming festive season. But theres nothing special about the music, so to speak." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 