Nadunisi Naaygal
 

 

{{Infobox film
| name           = Nadunissi Naaygal
| image          = Nadunisi Naaygal.jpg
| image size     =
| alt            =
| caption        = Promotional poster
| director       = Gautham Vasudev Menon
| producer       = Elred Kumar Jayaraman Madan
| writer         = Gautham Vasudev Menon
| narrator       = Veera Bahu
| starring       = Veera Bahu Sameera Reddy Deva
| cinematography = Manoj Paramahamsa Anthony Gonsalves
| studio         = Photon Kathaas R. S. Infotainment
| distributor    = Two95 Entertainment
| released       =  
| runtime        = 110 minutes
| country        = India
| language       = Tamil
| budget         =   
| gross          =
}} Tamil psychological thriller film written and directed by Gautham Menon that stars Sameera Reddy, Veera Bahu and Deva in lead roles, with Swapna Abraham, Ashwin Kakumanu and Samantha Ruth Prabhu in other pivotal roles.   The film notably has no score, instead featuring sounds designed by Renganaath Ravee. Produced by Photon Kathaas Productions and R. S. Infotainment, Nadunissi Naaygal released on 18 February 2011, simultaneously dubbed and released in Telugu as Erra Gulabilu.  The title Nadunisi Naaygal is taken from the title of a poem of Sundara Ramasamy.  The film received mixed reviews from both audience and critics.

== Synopsis ==

Samar (Veera Bahu), an eight-year-old motherless boy lives with his father in Mumbai who leads a colourful life indulging his sexual passions. Samar is sexually abused by his father and is rescued by his neighbour, a middle aged single woman Meenakshi Amma (Swapna Abraham). She names him Veera, takes him under her wing and protects him. Daunted and chased by the ghosts of his painful past, Veera rapes Meenakshi Amma. She, though reluctant at first, indulges in the act. After coming back to her senses the next morning, she refuses Veeras apology and decides to marry her colleague. On her marriage night, in bed with her husband, Veera stabs the man brutally and sets him and the room on fire. Meenakshi Amma is injured in the fire. After treatment, he brings back the scar-faced woman to his bungalow. After a few weeks, Veera meets a girl named Priya (Priya) on the Internet and they fall for each other. He invites her home and they grow intimate, interrupted by a loud scream from Priya, because Meenakshi Amma stabs her brutally. She orders Veera to cut off Priyas hair as she wants it.

In the following years, Veera kidnaps women, rapes them and finally kills them in cold-blood. As the murders continue, Veera stumbles upon Sukanya (Sameera Reddy), a girl he fell in love with in 10th grade at a theatre with her boyfriend Arjun (Ashwin Kakumanu). He lies to her that he had gone with another girl and offers her a ride home. An upset Sukanya agrees but does not know that Veera had been stalking her. Veera suddenly slaps her, making her unconscious and kisses her. Disgusted and terrified, Sukanya then finds Arjun in a pool of blood, in the backseat of the car. Sukanya tries to escape, engages in a fist fight with Veera but is stabbed in the abdomen. Police surround the car and take Sukanya to a hospital.

A bystander who had sensed something fishy with Veeras car follows him to his bungalow and informs the Assistant Commissioner Vijay (Deva). Veera takes Sukanya to his bungalow and informs Meenakshi Amma that he loves this girl truly and is going to live the rest of his life with her. Sukanya tries to escape but is captured by Veera. Veera says to Sukanya that Samar is responsible for all these events and murdered all the victims and even Meenakshi Amma. He says Meenakshi Amma is actually dead, but Samar still thinks she is alive. In a few moments, Vijay arrives at the residence and is confronted by four Rottweilers ready to pounce on him.

Alarmed by this, Veera tries to fend him off. He returns to take Sukanya into a hidden basement, where another two girls are captives, with their heads half-tonsured. He locks her in the basement and fights with Vijay. Sukanya, meanwhile, finds a way into the bungalow, takes a gun and shoots at Veera. He is shocked as he thinks it was Meenakshi Amma who shot at him. All this is recorded on tape as Veera narrates it to the Assistant Commissioner. Finally he is taken to a mental asylum where another patient (Samantha Ruth Prabhu) is also shown as a psychopath, victimised due to child sexual abuse and the end credits roll.

== Cast ==

* Veera Bahu as Veera (Samar)
* Sameera Reddy as Sukanya
* Deva as Vijay
* Ashwin Kakumanu as Arjun
* Swapna Abraham as Meenakshi Samantha as a patient in the asylum

== Production ==

Menon claimed that the film was inspired by a true event from the US, adding that a novel helped form the story of the film.    During production, he explicitly revealed that the film was for "the multiplex audience" and would face a limited release, citing that "it will not cater to all sections of the audience". 

== Reception ==

The film opened to mixed reviews. A Sify critic rated the film as "above average", citing that "Don’t go expecting a typical Gautham romantic film laced with peppy songs, be prepared to try out something new and experimental." Further he stated that the film "is definitely not for the family audiences", while criticising that "there are too many loopholes in the story, raising doubts about logic".  Another reviewer from Sify gave the film 2.5 out of 5, while describing the film as an "unimpressive show by star director Menon, as it is neither convincing nor appealing, despite having some engrossing moments".  Rediffs Pavithra Srinivasan, too, gave the film 2.5 out of 5, citing that the film "has a serious premise and is pretty realistic. But if youve watched any kind of Hollywood thriller at all, then the appeal is lost".  Times of India in its review wrote that "in his earlier movies   Gautham Menon had pushed the envelope when it came to presentation and themes, but in Nadunisi Naaygal he takes it a bit too far. Best, let the sleeping dogs lie", giving it 2 out of 5.  Aravindan DI of Nowrunning.com gave the film a score of 2.5 on 5 deeming it as "An unimpressive show, as it is neither convincing nor appealing."  Siddhartha of Silentcritics.com in his review wrote that it was not worth the money and time. 

The film also went through harsh criticism for the intimate controversial scene between Veera and Swapna Abraham. Protests were staged in front of Menons residence, who clarified that the scene is shown in an aesthetic manner and it is a sensitive story about a psychopath. When asked whether the scene would be removed from the film, Menon, as candid as ever, said the film would be removed soon from theatres as it was not doing good business. 

In contrast, Behindwoods gave it a positive review, giving it 3.5 out of 5. The reviewer labelled the film as a "differently made psychological thriller", adding that the film is "engaging right through and the desire of wanting to know what next propels it in a uniform pace." Further praise was addressed to the director who "should also be applauded for not bowing down to any cinematic syntax", congratulating him "for the brave attempt to drive home a socially relevant point in a dignified manner".  Rohit Ramachandran of Nowrunning.com put it at No. 4 in his Best of 2011 list. 

== References ==
 

== External links ==
 

 

 
 
 
 
 
 
 