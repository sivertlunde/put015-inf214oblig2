Husbands and Wives
 
{{Infobox film
| name           = Husbands and Wives
| image          = Husbands_moviep.jpg
| director       = Woody Allen
| producer       = Robert Greenhut
| writer         = Woody Allen
| starring       = Woody Allen Blythe Danner Judy Davis Mia Farrow Juliette Lewis Liam Neeson Sydney Pollack 
| cinematography = Carlo Di Palma
| editing        = Susan E. Morse
| studio         = TriStar Pictures
| distributor    = TriStar Pictures
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $20 million (estimate)
| gross          = $10,555,619 (domestic) 
}}
Husbands and Wives is a 1992 American comedy-drama film written and directed by Woody Allen.    The film stars Allen, Mia Farrow, Sydney Pollack, Judy Davis, Juliette Lewis, and Liam Neeson. It was nominated for the Academy Award for Best Actress in a Supporting Role (Judy Davis) and Best Writing, Screenplay Written Directly for the Screen (Woody Allen). The movie debuted around the same time as Allen and Farrows relationship ended because of his relationship with Soon Yi Previn. The movie is filmed by Carlo Di Palma with a handheld camera style and features documentary-like one-on-one interviews with the characters interspersed with the story.

Husbands and Wives was Allens first film as sole director for a studio other than United Artists or Orion Pictures (both now part of Metro-Goldwyn-Mayer) since Take the Money and Run, namely TriStar Pictures (though he has acted in films that were released by other studios but were not directed by him).

==Plot==
The film is about two couples: Jack (Pollack) and Sally (Davis), and Gabe (Allen) and Judy (Farrow). The film starts when Jack and Sally arrive at Gabe and Judys apartment and announce their separation. Gabe is shocked, but Judy takes the news personally and is very hurt. Still confused, they go out for dinner at a Chinese restaurant.

A few weeks later Sally goes to the apartment of a colleague. They plan to go out together to the opera and then to dinner. Sally asks if she can use his phone, and calls Jack. Learning from him that he has met someone, she accuses him of having had an affair during their marriage.

Judy and Gabe are introduced to Jacks new girlfriend, Sam, an aerobics trainer. While Judy and Sam shop, Gabe calls Jacks new girlfriend a "cocktail waitress" and tells him that he is crazy for leaving Sally for her. About a week later, Judy introduces Sally to Michael (Neeson), Judys magazine colleague who she clearly is interested in herself. Michael asks Sally out, and they begin dating; Michael is smitten, but Judy is dissatisfied with the relationship.

Meanwhile, Gabe has developed a friendship with a young student of his, Rain, and has her read the manuscript for his working novel. She comments on its brilliance, though has several criticisms, to which Gabe reacts defensively.

At a party, Jack learns from a friend that Sally is seeing someone, and flies into a jealous rage. He and Sam break up after an intense argument, and Jack drives back to his house to find Sally in bed with Michael. He asks Sally to give their marriage another chance, but she tells him to leave.

Less than two weeks later, however, Jack and Sally are back together and the couple meet Judy and Gabe for dinner like old times. After dinner, Judy and Gabe get into an argument about her not sharing her poetry. After Gabe makes a failed pass at her, Judy tells him that she thinks the relationship was over; a week later Gabe moves out. Judy begins seeing Michael.

Gabe goes to Rains 21st birthday party, and gives her a music box as a present. She asks him to kiss her, and though the two share a passionate romantic moment, Gabe tells her that they should not pursue it any further. As he walks home in the rain, he realizes that he has ruined his relationship with Judy.

Michael tells Judy he needs time alone, then says he cant help still having feelings for Sally. Angry and hurt, Judy walks out into the rain. Highlighting her "passive aggressiveness," Michael follows and begs her to stay with him. A year and a half later they marry.

At the end, the audience sees a pensive Jack and Sally back together. Jack and Sally admit their marital problems still exist (her frigidity is not solved), but they find they accept their problems as simply the price they have to pay to remain together.

Gabe is living alone because he says he is not dating for the time being because he doesnt want to hurt anyone. The film ends with an immediate cut to black after Gabe pleads with the unseen documentary crew, "Can I go? Is this over?"

==Cast==
The cast includes (in credits order):
 
* Woody Allen as Gabe Roth
* Mia Farrow as Judy Roth
* Judy Davis as Sally
* Sydney Pollack as Jack
* Juliette Lewis as Rain
* Liam Neeson as Michael Gates
* Lysette Anthony as Sam
* Cristi Conaway as Shawn Grainger, call girl
* Timothy Jerome as Paul, Sallys date
* Ron Rifkin as Richard, Rains analyst
* Bruce Jay Friedman as Peter Styles
* Jeffrey Kurland as interviewer-narrator (voice) Benno Schmidt as Judys ex-husband
* Nick Metropolis as TV scientist
* Rebecca Glenn as Gail
* Galaxy Craze as Harriet
* John Doumanian as Hamptons party guest
* Gordon Rigsby as Hamptons party guest
* Ilene Blackman as Receptionist
* Blythe Danner as Rains mother
* Brian McConnachie	as Rains father
* Ron August as Rains ex lover
* John Bucher as Rains ex lover
* Matthew Flint as Carl, Rains Boyfriend
 

==Release==

===Box office===
Husbands and Wives opened on September 18, 1992 in 865 theatres, where it earned $3,520,550 ($4,070 per screen) in its opening weekend. It went on to gross $10.5 million in North America during its two-week theatrical run.    The film was also screened at the 1992 Toronto Film Festival.

==Critical reception==
Husbands and Wives opened to universal acclaim from film critics; Rotten Tomatoes reports that 100% of critics have given the film a positive review based on 36 reviews, with an average score of 8.3/10. Peter Travers of Rolling Stone gave it a full four-star review and referred to it as "a defining film for these emotionally embattled times; its classic Woody Allen." Todd McCarthy of Variety (magazine)|Variety called it "a full meal, as it deals with the things of life with intelligence, truthful drama and rueful humor." 

===Accolades===
{| width="90%" class="wikitable sortable"
|- Award
! Category
! width="15%"|Recipient(s) Result
|- Academy Awards Academy Award Best Original Screenplay Woody Allen
| 
|- Academy Award Best Supporting Actress
| Judy Davis
| 
|- BAFTA Awards BAFTA Award Best Original Screenplay Woody Allen
| 
|- BAFTA Award Best Actress in a Leading Role
| Judy Davis
| 
|- Boston Society Boston Society of Film Critics Awards Boston Society Best Supporting Actress Judy Davis
| 
|-
|César Awards Best Foreign Film Woody Allen
| 
|- Chicago Film Critics Association Awards Chicago Film Best Supporting Actress Judy Davis
| 
|- Golden Globe Awards Best Supporting Actress – Motion Picture Judy Davis
| 
|- 28th Guldbagge Guldbagge Awards    Best Foreign Film
| 
| 
|- Kansas City Film Critics Circle Awards Kansas City Best Supporting Actress Judy Davis
| 
|- London Film Critics Circle Awards London Film Actress of the Year Judy Davis
| 
|- Los Angeles Film Critics Association Los Angeles Best Supporting Actress Judy Davis
| 
|- National Board of Review National Board Best Supporting Actress Judy Davis
| 
|- National Society of Film Critics National Society Best Supporting Actress Judy Davis
| 
|- Writers Guild of America Awards Writers Guild Best Original Screenplay Woody Allen
| 
|-
|}

==References==
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 