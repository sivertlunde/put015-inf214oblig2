Gaana Bajaana
 

{{Infobox film
| name = Gaana Bajaana
| image = Gaana Bajaana poster.jpg
| caption = Film poster
| director = Prashant Raj
| writer =
| producer = Naveen
| starring = Tarun Chandra Radhika Pandit  Dilip Raj
| music = Joshua Sridhar
| cinematography =
| editing =
| released = 29 October 2010
| runtime =
| language = Kannada
| lyrics =
| Photography =
| country =  
| website =
}}
 Love Guru fame.  It stars Tarun Chandra, Radhika Pandit, and Dileep Raj. The film includes music by Joshua Sridhar and camera work by Shekar.  The film was produced by Naveen. 

==Production== Love Guru in Gaana Bajaana as well. The movie was delayed for release because of scheduling conflicts with other movies released around the same time.

==Release==
The film was released on October 29, 2010. 

==Cast==
* Tarun Chandra as Krish
* Radhika Pandit as Radhe
* Dilip Raj as Kuttappa
* Sharan
* C.R. Simha
* Raja Rao as Krishs grandfather
* Vijaya Parthsarthy

==Soundtrack==

{{Infobox album  
| Name        = Gaana Bajaana
| Type        = Soundtrack
| Artist      = Joshua Sridhar
| Cover       =
| Released    = 2010
| Recorded    = Feature film soundtrack
| Length      =
| Label       =
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|-
| 1 || Hosadondu Hesaridu || Karthik (singer)|Karthik, Shweta Mohan ||
|-
| 2 || Hosa Gaana Bajaana || Joshua Sridhar, Sayanora Philip || 
|- Karthik ||
|-
| 4 || Gundetu Gundetu || Benny Dayal, Chinmayi ||  
|-
| 5 || Aaja Nachre || Sayanora Philip ||
|-
| 6 || Krish Intro || Benny Dayal ||
|-
|}

==External links==
*  

 
 
 


 