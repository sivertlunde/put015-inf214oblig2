Voyage to the Beginning of the World
 
{{Infobox film
| name           = Voyage to the Beginning of the World
| image          = Viagem ao Princípio do Mundo.jpg
| caption        = Film poster
| director       = Manoel de Oliveira
| producer       = Paulo Branco
| writer         = Jacques Parsi Manoel de Oliveira
| starring       = Marcello Mastroianni
| music          = 
| cinematography = Renato Berta
| editing        = Valérie Loiseleux
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = France Portugal
| language       = French Portuguese
| budget         = 
}}

Voyage to the Beginning of the World ( ,  ) is a 1997 Portuguese-French drama film directed by Manoel de Oliveira and starring Marcello Mastroianni.    It was Mastroiannis final film.

==Plot==
French actor Alfonso visits the Portuguese village where his father once left. He is accompagnied by the film director Manoel who hereby returns to the places of his childhood. Together with two other actors who serve as translators they make it to the remote village and meet the estranged kin.

==Cast==
* Marcello Mastroianni - Manoel
* Jean-Yves Gautier - Afonso
* Leonor Silveira - Judite
* Diogo Dória - Duarte
* Isabel de Castro - Maria Afonso
* Cécile Sanz de Alba - Christina
* José Pinto (actor)|José Pinto - José Afonso
* Adelaide Teixeira - Woman
* Isabel Ruth - Olga
* Manoel de Oliveira - Driver
* José Maria Vaz da Silva - Assistant
* Fernando Bento - Man 1
* Mário Moutinho - Man 2
* Jorge Mota - Man 3
* Sara Alves - Girl
* Helder Esteves - Boy

==Accolades== FIPRESCI Prize.   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 