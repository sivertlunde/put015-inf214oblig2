Classmates (1914 film)
{{infobox film
| name           = Classmates
| image          =
| imagesize      =
| caption        = James Kirkwood
| producer       = Abe Erlanger Marc Klaw Margaret Turnbull(play) William C. deMille(play)
| starring       = Henry B. Walthall Blanche Sweet
| cinematography = Tony Gaudio
| editing        =
| distributor    = Biograph Company
| released       = February 14, 1914
| runtime        = 4 reels (approximately 40 minutes)
| country        =
| language       = Silent film(English intertitles)
}} James Kirkwood Margaret Turnbull and William C. deMille. It was shot in Jacksonville, Florida at the end of 1913.   

The film is extant today as the result of a Paper print depositing in the Library of Congress. It was shown at Cinefest 2012.  

==Cast==
*Blanche Sweet – Sylvia Randolph
*Henry B. Walthall – Duncan Irving
*Marshall Neilan – Bert Stafford
*Gertrude Robinson – Phyllis Stafford
*Augusta Anderson – Mrs. Stafford
*Lionel Barrymore – Dumble
*Thomas Jefferson – Mr. Irving

unbilled
*Dorothy Bernard – Bit
*Jack Mulhall – Man

==See also==
*Blanche Sweet filmography
*Lionel Barrymore filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 