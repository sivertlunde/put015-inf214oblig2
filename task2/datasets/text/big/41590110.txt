Praia do Futuro (film)
{{Infobox film
| name           = Praia do Futuro
| image          = Praia do Futuro Film Poster.jpg
| caption        = Theatrical release poster
| director       = Karim Aïnouz
| producer       = 
| writer         = Karim Aïnouz Felipe Bragança
| starring       = Wagner Moura Clemens Schick Jesuita Barbosa
| music          = Volker Bertelmann
| cinematography = Ali Olay Gözkaya
| sound mixer = Danilo Carvalho
| editing        = Isabela Monteiro de Castro
| studio         = Coração da Selva Hank Levine Film
| distributor    = California Filmes
| released       =  
| runtime        = 
| country        = Brazil Germany
| language       = Portuguese German
| budget         = $4 million
| gross          = 
}}
Praia do Futuro (English: Futuro Beach) is a 2014 Brazilian-German drama film directed by Karim Aïnouz, and starring Wagner Moura, Clemens Schick and Jesuita Barbosa. The film had its premiere in the competition section of the 64th Berlin International Film Festival.   

==Plot==
In the film, shot in Fortaleza and Berlin, Wagner Moura plays the lifeguard Donato, who works at Praia do Futuro. When a German tourist loses his life in a drowning, Donato feels the loss of the tourist was his fault and begins a journey to escape from his present self. Whenever he drifts away, his younger brother, Ayrton (Jesuita Barbosa), brings him back. 

==Cast==
*Wagner Moura as Donato
*Clemens Schick as Konrad
*Jesuita Barbosa as Ayrton
*Sabine Timoteo as Heikos Wife
*Ingo Naujoks as Mechanic
*Emily Cox as Nanna
*Natascha Paulick as Bartender
*Christoph Zrenner as School Janitor
*Sophie Charlotte Conrad as Dakota
*Yannik Burwiek as Heikos Son

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 