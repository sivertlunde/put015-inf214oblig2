Judith of Bethulia
{{Infobox film
| name = Judith of Bethulia
| image =Judith of Bethulia.jpg
| image_size =
| caption =
| director = D. W. Griffith
| writer = Thomas Bailey Aldrich D. W. Griffith Frank E. Woods
| starring = Blanche Sweet Henry B. Walthall
| cinematography = G. W. Bitzer
| editing = James Smith
| studio  = Biograph Company
| distributor = General Film Company
| released =  
| country = United States
| runtime = 61 mins.
| language = Silent film  English intertitles
}}
 
Judith of Bethulia (1914 in film|1914) is a film starring Blanche Sweet and Henry B. Walthall, and produced and directed by D. W. Griffith. The film was the first feature film|feature-length film made by pioneering film company American Mutoscope and Biograph Company|Biograph, although the second that Biograph released.  
 stock company with him. Biograph delayed the pictures release until 1914, after Griffiths departure, so that it would not have to pay him in a profit-sharing agreement they had.

The film caused controversy with its inclusion of an orgy scene.

==Synopsis==
The story is from the biblical Book of Judith. During the siege of the Jewish city of Bethulia by the Assyrians, a widow named Judith (Blanche Sweet) has a plan to stop the war  as her people suffer starvation and are ready to surrender.

The widow disguises herself as a harem girl and goes to the enemy camp, where she beguiles a general of King Nebuchadnezzar, whose army is besieging the city. Judith seduces Holofernes (Henry Walthall), then while he is drunk cuts off his head with a sabre. She returns to her city, a heroine.

==Cast==
 
* Blanche Sweet - Judith
* Henry B. Walthall - Holofernes
* Mae Marsh - Naomi
* Robert Harron - Nathan
* Lillian Gish - The young mother
* Dorothy Gish - The crippled beggar
* Kate Bruce - Judiths maid
* J. Jiquel Lanoe - Eunuch Attendant Harry Carey - Assyrian Traitor
* W. Chrystie Miller - Bethulian
* Gertrude Robinson
* Charles Hill Mailes - Bethulian Soldier Edward Dillon
* Gertrude Bambrick - Lead Assyrian Dancer
* Lionel Barrymore - Extra
* Clara T. Bracy - Bethulian
* Kathleen Butler - Bethulian
* William J. Butler - Bethulian
* Christy Cabanne
* William A. Carroll - Assyrian Soldier (as William Carroll)
* Frank Evans - Bethulian Soldier
* Mary Gish
* Harry Hyde - Bethulian Soldier/Assyrian Soldier
* Thomas Jefferson Jennie Lee - Bethulian
* Adolph Lestina - Bethulian
* Elmo Lincoln
* Antonio Moreno - Extra
* Marshall Neilan
* Frank Opperman - Bethulian
* Alfred Paget - Bethulian/Assyrian Soldier
* W. C. Robinson - Bethulian Soldier
* Kate Toncray - One of Judiths Servants

==Reviews==
The reviews were favorable:
Variety (magazine)|Variety, March 27, 1914 wrote: "It is not easy to confess ones self unequal to a given task, but to pen an adequate description of the Biographs production of Judith of Bethulia is, to say the least, a full grown mans job."

The Moving Picture World, March 7, 1914 described it as: "A fascinating work of high artistry, Judith of Bethulia will not only rank as an achievement in this country, but will make foreign producers sit up and take notice." 

==See also==
*D. W. Griffith filmography
*Lillian Gish filmography
*Blanche Sweet filmography
*Lionel Barrymore filmography

==References==
 

==External links==
 
*  
*  
*  
*  available for free download at  

 

 
 
 
 
 
 
 
 
 
 
 
 