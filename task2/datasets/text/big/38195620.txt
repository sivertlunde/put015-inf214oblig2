Googly (film)
 
 
{{Infobox film
| name           = Googly
| image          = Googly_2013_Kannada_Film.jpg
| caption        =
| director       = Pawan Wadeyar
| producer       = Jayanna  Bhogendra
| writer         = Pawan Wadeyar Yash Kriti Kharbanda Anant Nag
| music          =  
| cinematography = Vaidy S.
| editing        = Sanath Suresh
| studio         = Jayanna Combines
| released       =  
| country        = India Kannada
| budget     =  
| gross      =   +   {Satellite rights} 
}}
 Yash and Kriti Kharbanda are the lead actors while Ananth Nag and Sadhu Kokila play the primary supporting roles. The film released on 19 July 2013 and was declared a "Blockbuster". 
 Best Director Best Cinematographer Best Lyricist Best Fight Choreographer (Ravi Varma). 

==Cast== Yash as Sharath
* Kriti Kharbanda as Swathi
* Anant Nag
* Sudha Belawadi as Kausalya
* Sadhu Kokila as Musthafa
* Neenasam Ashwath as Ravi Joshi
* Deepu as mandya

==Production==

===Choreography and music===
Palaniraj and Ravi Verma choreographed the action scenes, with dance choreography done by Murali. Joshua Sridhar composed the score and soundtrack while ace Kannada music composer and state awardee Anoop Seelin scored the background music. Lyrics were written by Yogaraj Bhat, Jayanth Kaikini, Pawan Wadeyar and Kaviraj.

===Filming===
The filming of Googly took place in Bengaluru, Sakleshpur, Moodabidre and Mangaluru over forty days. One of the songs was shot in Malaysia

==Soundtrack==
{{Infobox album
| Name        = Googly
| Type        = Soundtrack
| Artist      = Joshua Sridhar
| Cover       = Kannada film Googly album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 3 June 2013
| Recorded    =  Feature film soundtrack
| Length      = 25:42
| Label       = D Beats
| Producer    = V. Harikrishna
| Last album  = 
| This album  = 
| Next album  = 
}}
 Kaviraj and Pawan Wadeyar. The album consisting of six soundtracks was released on June 3, 2013, in Bangalore. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 25:42
| lyrics_credits = yes
| title1 = Gandu Janma
| lyrics1 = Pawan Wadeyar
| extra1 = Pawan Wadeyar
| length1 = 4:21
| title2 = Eno Eno Aagide Kaviraj
| extra2 = Haricharan
| length2 = 4:52
| title3 = Neenirade
| lyrics3 = Jayant Kaikini
| extra3 = Sonu Nigam
| length3 = 4:24
| title4 = Googly Gandasare Keli
| lyrics4 = Pawan Wadeyar
| extra4 = Haricharan, Apoorva
| length4 = 4:15
| title5 = Bisilu Kudureyondu
| lyrics5 = Yogaraj Bhat
| extra5 = Rajesh Krishnan
| length5 = 3:22
| title6 = Googly Googly
| lyrics6 = Pawan Wadeyar
| extra6 = Pawan Wadeyar
| length6 = 4:28
}}

==Reception==
 
===Critical reception===
The film opened to generally positive reviews from critics upon release. G. S. Kumar of The Times of India reviewed the film and gave a 3.5/5 rating writing, "Armed with a good script, director Pavan Wadeyar has done a good job of a romantic story with neat narration and screenplay capturing the trauma of a young industrialist..." He concludes crediting the performances of Yash, Kharbanda, and the films music and cinematography.  Bangalore Mirror in its review wrote, "Wadeyar is in complete control of the proceedings. Though most part of the film focuses on the leading pair, there is hardly a dull moment. His dialogues are brimming with wit and humour." and added, " Yash is at his best and truly establishes himself as a star material in Sandalwood. Kriti’s charm continues and she is sure to ‘wound more hearts’ in the gallery in this film as well."  Writing of Deccan Herald, B. S. Srivani rated the film 3/5 and praised the performances of the actors and gave a special mention to the films music. Of the music, she wrote, "the film’s music plays an important part in shoving the aberration aside. Both Anoop Seelin and Joshua Sridhar come up with scintillating scores. That the tunes are catchy and are a rage is not surprising at all. Seelin’s background score is refreshing and complements Joshua’s work nicely." 

==Box office==
Released on 19 July 2013, Googly, according to box office reports earned more than a crore on the first day and   within the first four days of its release.  According to the producer, the film collected   gross at the box office in its first week.  The film was declared a "Blockbuster" after completing 100 days of run in several centers and grossing   at the box office with its satellite rights having been sold for  . 

== References ==
 

== External links ==
* 
* 
*   at  
* 
* 

 
 
 
 