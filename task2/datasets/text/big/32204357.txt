Guruvayur Kesavan (film)
{{Infobox film
| name           = Guruvayur Kesavan
| image          =
| caption        =
| director       = Bharathan
| producer       = MO Joseph
| writer         = Puthur Unnikrishnan N. Govindankutty (dialogues)
| screenplay     = N. Govindankutty
| starring       = Jayabharathi Adoor Bhasi MG Soman Sukumari
| music          = G. Devarajan Traditional Ashok Kumar
| editing        = MS Mani
| studio         = Manjilas
| distributor    = Manjilas
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film,  directed by Bharathan and produced by MO Joseph. The film stars Jayabharathi, Adoor Bhasi, MG Soman and Sukumari in lead roles. The film had musical score by G. Devarajan and Traditional.    It tells the story of an elephant which belonged to the Guruvayur temple.

==Cast==
 
*Jayabharathi
*Adoor Bhasi
*MG Soman
*Sukumari
*Manavalan Joseph
*Sankaradi
*Baby Meena
*Baby Vineetha
*Bahadoor
*Junior Sheela
*MS Namboothiri
*N. Govindankutty
*Oduvil Unnikrishnan
*Paravoor Bharathan
*Thrissur Rajan
*Ushakumari
*Veeran
*Puthur Unnikrishnan
 

==Soundtrack==
The music was composed by G. Devarajan and Traditional and lyrics was written by P. Bhaskaran and .

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dheemtha Thakka || P Jayachandran, CO Anto, Jolly Abraham || P. Bhaskaran ||
|-
| 2 || Innenikku Pottukuthaan || P. Madhuri || P. Bhaskaran ||
|-
| 3 || Maarimukilin || P. Madhuri || P. Bhaskaran ||
|-
| 4 || Navakaabhishekam Kazhinju || K. J. Yesudas || P. Bhaskaran ||
|-
| 5 || Soorya Spardhi Kireedam || K. J. Yesudas ||  ||
|-
| 6 || Sundara Swapname || K. J. Yesudas, P. Leela || P. Bhaskaran ||
|-
| 7 || Ushaakiranangal || K. J. Yesudas || P. Bhaskaran ||
|}

==References==
 

==External links==
*  

 
 
 
 


 