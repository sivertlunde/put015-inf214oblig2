Mulan (1998 film)
 

 
{{Infobox film name = Mulan image = Movie_poster_mulan.JPG caption = Promotional poster by John Alvin director = Tony Bancroft Barry Cook screenplay = Chris Sanders Eugenia Bostwick-Singer Raymond Singer story = Robert D. San Souci starring = Ming-Na Eddie Murphy BD Wong Miguel Ferrer Harvey Fierstein June Foray George Takei Pat Morita producer = Pam Coats music = Jerry Goldsmith  editing = Michael Kelly studio = Walt Disney Feature Animation distributor = Buena Vista Pictures released =   runtime = 87 minutes country = United States language = English budget = $90 million    gross = $304.3 million   
}} musical action-comedy-comedy-drama drama film Walt Disney Chris Sanders, Fa Mulan, Hun invasion.
 Best Animated Feature. A 2005 direct-to-video sequel, Mulan II, followed.

==Plot==
 
 Han China, Chinese emperor begins to command a general mobilization. Each family is given a conscription notice, requiring one man from each family to join the Chinese army. When Fa Mulan hears that her elderly father Fa Zhou, the only man in their family, is once more to go to war, she becomes anxious and apprehensive. She decides to deal with this herself by disguising herself as a man so that she can go to war instead of her father. When her family learns of Mulans departure, they all become anxious. Grandmother Fa, Mulans grandmother, prays to the family ancestors for Mulans safety. The ancestors then order their "Great Stone Dragon" to protect Mulan. The ancestors are unaware that the statue of Great Stone Dragon failed to come to life, and that Mushu, a small chinese dragon|dragon, is the one to go and protect Mulan.
 Li Shang, she and her new co-workers at the camp, Yao, Ling and Chien-Po, become skilled warriors. Mushu, desiring to see Mulan succeed, creates a fake order from Li Shangs father, General Li, ordering Li Shang to follow them into the mountains. The troops set out to meet General Li, but arrive at a burnt-out encampment and discover that General Li and his troops have been wiped out by the Huns. As they solemnly leave the mountains, they are ambushed by the Huns, but Mulan cleverly uses a cannon to create an avalanche which buries most of the Huns. An enraged Shan Yu slashes her in the chest, and her deception is revealed when the wound is bandaged. Instead of executing Mulan as the law requires, Li Shang relents and decides to spare her life for saving him, but expels her from the army, stranding her on the mountain as the rest of the army departs for the Imperial City to report the news of the Huns demise. However it is revealed that several Hun warriors including Shan Yu survive the avalanche, and Mulan catches sight of them as they make their way to the City, intent on capturing the Emperor.
 concubines and crest and Shan Yus sword as gifts, she politely declines his offer to be his advisor and asks to return to her family. She returns home and presents these gifts to her father, but he is more overjoyed to have his daughter back safely. Li Shang, who has become enamored with Mulan, soon arrives under the guise of returning her helmet, but accepts the familys invitation for dinner. Mushu is granted a position as a Fa family guardian by the ancestors amid a returning celebration.

==Cast==
 ; Fa Mulan; Kahn]]
 
*Ming-Na Wen as Fa Mulan (singing voice provided by Lea Salonga)
*Eddie Murphy as Mushu
*BD Wong as Captain Li Shang (singing voice provided by Donny Osmond)
*Miguel Ferrer as Shan Yu
*Harvey Fierstein as Yao
*Gedde Watanabe as Ling
*Jerry Tondo as Chien-Po
*James Hong as Chi-Fu
*Soon-Tek Oh as Fa Zhou
*June Foray as Grandmother Fa (singing voice provided by Marni Nixon)
*Pat Morita as The Emperor of China
*George Takei as First Ancestor
*Miriam Margolyes as The Matchmaker
*Freda Foh Shen as Fa Li
*James Shigeta as General Li
*Frank Welker as Cri-Kee and Khan (Mulans horse) Chris Sanders as  Little Brother (Mulans dog)
*Mary Kay Bergman as various ancestors
 Mainland standard versions of the film respectively, while Jackie Chan provided the voice of Li Shang in all three Chinese versions and appeared in the version of promotional music videos of "Ill Make a Man Out of You".

==Production==
Mulan originally began as a short, straight-to-video film titled "China Doll" about an oppressed and miserable Chinese girl who is whisked away by a British Prince Charming to happiness in the West. Then Disney consultant and childrens book author Robert D. San Souci suggested making a movie of the Chinese poem "The Song of Fa Mu Lan", and Disney combined the two separate projects.  
 watercolor and The Hunchback of Notre Dame. 
 API RenderMan RenderMan was used to render the crowd. Another software developed for this movie was Faux Plane which was used to add depth to flat two-dimensional painting. Although developed late in production progress, Faux Plane was used in five shots, including the dramatic sequence which features the Great Wall of China, and the final battle sequence when Mulan runs to the Forbidden City. During the scene in which the Chinese are bowing to Mulan, the crowd is a panoramic film of real people bowing.  It was edited into the animated foreground of the scene.   

==Reception==

===Critical reaction=== url = title = Rotten Tomatoes accessdate = 2014-10-13}}  In a 2009 countdown, Rotten Tomatoes ranked it twenty-fourth out of the fifty canonical animated Disney features.    On Metacritic, the film has a score of 71 out of 100, based on 24 critics, indicating "generally favorable reviews". 
 url = title = Review of Mulan accessdate = last = first = year = publisher = Christian Spotlight url = title = accessdate = 2007-08-11 year = publisher = Need Coffee }}  Ed Gonzalez of Slant Magazine criticized the film as "soulless" in its portrayal of Asian society. {{cite web url = title = Review of Mulan accessdate = last = first = year = publisher = Slant Magazine
}} 

This movie was also the subject of comment from feminist critics. Mimi Nguyen says the film "pokes fun at the ultimately repressive gender roles that seek to make Mulan a domesticated creature."    Nadya Labi agreed, saying "there is a lyric in the film that gives the lie to the bravado of the entire girl-power movement." She pointed out that Mulan needed to become a boy in order to accomplish what she did. Kathleen Karlyn, an assistant professor of English at the University of Oregon, also criticized the films portrayal of gender roles:  "In order to even imagine female heroism, were placing it in the realm of fantasy". Pam Coats, the producer of Mulan, said that the film aims to present a character who exhibits both masculine and feminine influences, being both physically and mentally strong.   

===Box office performance=== The X-Files. {{cite web url = title = Box Office Report for X-Files accessdate = archiveurl = archivedate = The Hunchback Beauty and the Beast, Aladdin (1992 Disney film)|Aladdin, and The Lion King. {{cite news | first = Richard | last = Corliss | title = Stitch in Time? | url = http://www.time.com/time/magazine/article/0,9171,1002724,00.html | publisher = TIME Magazine
 | date = 2002-06-24 | accessdate = 2007-08-11 }}  Internationally, its highest grossing releases included those in the United Kingdom ($14.6 million) and France ($10.2 million). {{cite news
 | first = Mark  | last = Woods | title = ‘Bug’s’ bags bucks | url = http://variety.com/1998/film/news/bug-s-bags-bucks-1117489190/ | publisher = Variety | date = 1998-12-07 | accessdate = 2014-03-16 }} 

===Awards=== Best Animated Feature and Individual achievement awards to Pam Coats for producing; Barry Cook and Tony Bancroft for directing; Rita Hsiao, Christopher Sanders, Phillip LaZebnick, Raymond Singer and Eugenia Bostwick-Singer for writing, Chris Sanders for storyboarding, Hans Bacher for production design, David Tidgwell for effects animation, Ming-Na for voice acting for the character of Mulan, Ruben A. Aquino for character animation, and Matthew Wilder, David Zippel and Jerry Goldsmith for music. (Tom Bancroft and Mark Henn were also nominated for an Annie Award for Character Animation.) {{cite web url = title = accessdate = year = The Prayer" from Quest for Camelot, respectively.   

===Reception in China=== Western films per year to be shown in their country, Mulans chances of being accepted were low. {{cite news|title=Hollywood hopes more movies will follow Clinton to China|author=Michael Fleeman|agency=Associated Press| rampant piracy. Chinese people also complained about Mulans depiction as too foreign-looking and the story as too different from the myths.   By contrast, Dreamworks Animations own effort ten years later, Kung Fu Panda, would be much more favorably received both for its artistry and cultural accuracy.

==Chinese culture in Mulan==

===The legend of Hua Mulan===
 
 the title character in much the same way as the original legend—a tomboy daughter of a respected veteran, somewhat troubled by not being the "sophisticated lady" her society expects her to be. In the oldest version of the story, Mulan uses her fathers name Li  and she was never discovered as a girl, unlike the film.

The earliest accounts of the legend state that she lived during the Northern Wei dynasty (386&ndash;534). However, another version reports that Mulan was requested as a concubine by Emperor Yang of Sui China (reigned 604&ndash;617). {{cite web url = title = accessdate = author = J. Lau }} Ming era Mandarin personal Cantonese pronunciation (Fa) for her family name.

===Language=== many dialects. The subtitles simply read 平.

Chi Fus name (欺负, qīfù) means "to bully or ridicule".

==Music==
 
 Stephen Schwartz Peter Schneider, The Hunchback Anne Rices Cry to Heaven, and selected Wilder to replace Schwartz. David Zippel then joined to write the lyrics.  The film featured five songs composed by Wilder and Zippel, with a sixth originally planned for Mushu, but dropped following Eddie Murphys involvement with the character.  The film score of Mulan was composed by Jerry Goldsmith. The films soundtrack is credited for starting the career of pop singer Christina Aguilera, whose first song to be released in the U.S. was her rendition of "Reflection (song)|Reflection", the first single from the Mulan soundtrack. The song, and Aguileras vocals, were so well received that it landed her a recording contract with RCA records.     In 1999, she would go on to release her Christina Aguilera (album)|self-titled debut album, on which Reflection was also included. As well as her own, the pop version of Reflection has 2 Spanish translations, because the movie has separate Spanish translations for Spain (performed by Malú) and Latin America (performed by Lucero (actress)|Lucero). Other international versions include a Brazilian Portuguese version by Sandy & Junior ("Imagem"), a Korean version performed by Lena Park and a Mandarin version by Coco Lee.

Lea Salonga, the singing voice of Mulan in the movie, is also the singing voice of Princess Jasmine in Aladdin (1992 Disney film)|Aladdin. Salonga was originally also cast as Mulans speaking voice, but the directors did not find her attempt at a deeper speaking voice when Mulan impersonated Ping convincing, so Ming-Na was brought in to speak the role.  The music featured during the haircut scene, often referred as the Mulan Decision score, is different in the soundtrack album. The soundtrack album uses an orchestrated score while the movie uses heavy synthesizer music. The synthesizer version is available on the limited edition CD.  Salonga, who often sings movie music in her concerts, has done a Disney medley which climaxes with an expanded version of "Reflection" (not the same as those in Aguileras version).  Salonga also provided the singing voice for Mulan in the movies sequel, Mulan II.

Captain Li Shangs singing voice, for the song "Ill Make a Man Out of You", was performed by Donny Osmond, who commented that his sons decided that he had finally "made it" in show business when he was in a Disney film.   

==Legacy==

===Video game===
 

A   and developed by Revolution Software (under the name "Kids Revolution"), was released on December 15, 1999.       The game was met with generally positive reception and currently holds a 70.67% average rating at the review aggregator website GameRankings.   

===Live action adaptation=== Chris Bender and J.C. Spink producing while Elizabeth Martin and Lauren Hynek will write the screenplay. 

===Home video===
Mulan was first released on VHS on February 2, 1999 after part of the Walt Disney Masterpiece Collection. It was then re-released under the 1999 Limited Issues line and 2000 Walt Disney Gold Classic Collection. The film was released on a 2 disc "Special Edition" DVD on October 26, 2004. Mulan and its sequel were released on a 3 disc Blu-Ray and DVD combo pack in March 2013 as part of the films 15th anniversary. 

===References in Disney media===
 ]] playable world Donald and ABC television Once Upon a Time. 

==See also==
 
*History of the Han Dynasty (for info on the period this film is loosely based on)
*Han–Xiongnu War (for info on the conflict this film is loosely based on)
*List of Disney animated features
*List of Disney animated films based on fairy tales
*List of animated feature-length films
*List of traditional animated feature films
 

==References==
 

==External links==
 
* 
* 
* 
* 
* 

 
 
 
 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 