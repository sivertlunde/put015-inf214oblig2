A Toast for Manolete
 

{{Infobox film
| name =   A Toast for Manolete 
| image =
| image_size =
| caption =
| director = Florián Rey
| producer = Miguel García Rico 
| writer = Enrique Llovet   Eduardo Manzanos Brochero   José María Pemán   José Carlos de Luna   Florián Rey 
| narrator = Pedro Ortega   Manolo Morán   Manuel Monroy
| music = José Muñoz Molleda     
| cinematography = Heinrich Gärtner  
| editing = Magdalena Pulido 
| studio = Hércules Films 
| distributor = 
| released = 20 December 1948  
| runtime = 88 minutes
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}} Pedro Ortega and Manolo Morán. The film is a biopic of the celebrated Spanish bullfighter Manolete, who had been killed the previous year. 

==Cast==
*  Paquita Rico as Dolores   Pedro Ortega as Manuel Rodríguez Manolete 
* Manolo Morán as Antonio  
* Ana Adamuz as Doña Rosario  
* Manuel Monroy as Javier del Álamo  
* Eulália del Pino as Soledad 
* José Jaspe as Bronquista en bar  
* Mercedes Castellanos as Mercedes   Domingo Rivas as Gabriel  
* Juana Mansó as Remedios 
* Manolo Iglesias as Juan  
* Emilio Ruiz de Córdoba as Médico 
* José Greco (actor)|José Greco as Rafael  Rafael Romero as Cantaor 
* Rafael Bardem as Hombre que da mala noticia  
* Trío Escudero as Trío flamenco 
* Fernanda La Cordobesita as Bailaora 
* Porreto
* Manzanilla
* Orozco
* Manolo Badajoz
* Sevillano
* Joaquina Martí
* Luis Maravilla

== References ==
 
 
==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer 2008. 

== External links ==
* 

 

 
 
 
 
 
 
 
 
 

 