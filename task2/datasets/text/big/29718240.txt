The Long Dark Hall
 
{{Infobox film
| name           = The Long Dark Hall
| image          = "The_long_Dark_Hall"_(1951).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = U.S. theatrical poster
| director       = {{Plainlist |
* Reginald Beck
* Anthony Bushell
}}
| producer       = Peter Cusick
| writer         = Nunnally Johnson
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Rex Harrison
* Lilli Palmer
* Tania Heald
}}
| music          = Benjamin Frankel
| cinematography = Wilkie Cooper
| editing        = Tom Simpson
| studio         = Cusick International Films Inc.
| distributor    = United Artists
| released       =  
| runtime        = 86 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
}}
 British crime film directed by Reginald Beck and Anthony Bushell and starring Rex Harrison, Lilli Palmer and Raymond Huntley. After a showgirl is found murdered shortly after she begins an affair with Arthur Groome, a married man, he becomes the prime suspect for the murder.  It was based on a novel A Case to Answer by Edgar Lustgarten.

==Cast==
* Rex Harrison - Arthur Groome 
* Lilli Palmer - Mary Groome 
* Tania Heald - Sheila Groome 
* Henrietta Barry - Rosemary Groome 
* Dora Sevening - Marys mother 
* Ronald Simpson - Marys father 
* Raymond Huntley - Chief Inspector Sullivan 
* William Squire - Sergeant Cochran 
* Ballard Berkeley - Superintendent Maxey
* Anthony Dawson -  The Man 
* Denis ODea -  Sir Charles Morton 
* Anthony Bushell -  Clive Bedford  Henry B. Longhurst -  Judge
* Patricia Cutts - Rose Mallory 
* Meriel Forbes - Marjorie Danns 
* Brenda De Banzie - Mrs Rogers 
* Douglas Jefferies - Dr. Conway 
* Fletcher Lightfoot - Jury Foreman 
* Anthony Shaw - Clerk of the Court 
* Michael Medwin - Leslie Scott 
* Colin Gordon - Pound
* Lionel Murton - Jefferson 
* Eric Pohlmann - Mr Polaris 
* Lilli Molnar - Mrs Polaris  
* Frank Tickle - Alfred Tripp 
* Tom Macaulay - Ironworks manager 
* Richard Littledale - Mr Sims 
* Jenny Laird - Mrs Sims
* Tony Quinn -  Joe the barman Jill Bennett -  First murdered girl

==Critical reception==
In The New York Times, Bosley Crowther wrote, "a very tidy murder drama arrived yesterday from England at the Rivoli Theatre (South Fallsburg, New York)| Rivoli Theater...An unusually literate and impressively acted film...It is English in setting and temperament, but international in its entertainment appeal. Thoughtful audiences should especially welcome this picture."  

When asked to name his "worst picture", Rex Harrison replied: "My worst picture? The Long Dark Hall would have to be near the top of the list." 

==References==
 

==External links==
 

 
 
 
 
 
 
 
 
 


 
 