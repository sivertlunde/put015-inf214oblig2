Raamdhenu
{{Infobox film
| name           = Raamdhenu
| image          = Poster of Raamdhenu.jpg
| caption        = Promotional poster of Raamdhenu
| director       = Munin Barua
| producer       = Pride East Entertainments Pvt. Ltd.
| writer         = 
| starring       = Jatin Bora Prastuti Parashar Utpal Das Nishita Goswami Bishnu Kharghoria Tapan Das Bidyut Chakrabarty
| music          = Jatin Sharma
| cinematography = 
| released       =  
| editing        = 
| distributor    = 
| studio         = Pride East Entertainments Pvt. Ltd.
| country        = India Assamese
| budget         = INR 70 lakhs
| box office     = INR 2.04 crore
}} Assamese language romantic drama film directed by veteran Munin Barua and produced by Pride East Entertainments Private Limited. It stars Jatin Bora, Prastuti Parashar, Utpal Das and Nishita Goswami in the lead roles. The film was released in 24 cinema halls across Assam on February 4, 2011. The music is composed by Jatin Sharma.

The songs in this film are sung by popular singers like Zubeen Garg, Angaraag Mahanta, Dikshu, Zublee, Shreya Ghoshal, Raaj J Konwar, Rupjyoti and Sunidhi Chauhan.

== Cast ==
* Jatin Bora
*Prastuti Parashar
* Jayanta Das
* Utpal Das
* Nishita Goswami 
* Bishnu Kharghoria 
* Tapan Das 
* Bidyut Chakrabarty

== Box Office ==
Raamdhenu earned 2 crores from its entire run in the cinema halls. As it was made at a budget of 70 lakhs; it was declared a Blockbuster (entertainment)|blockbuster. 

==References==
 

 
 
 


 