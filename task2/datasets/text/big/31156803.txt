War and Love
{{Infobox film
| name =War and Love
| caption =
| director =Vinayan
| writer = Mubeen Poonthura
| narrator = Dileep Prabhu Prabhu Laila Radhika
| music = berny ignatious
| cinematography =s.sounder rajan
| editing =kola bhaskar
| distributor =sree surya
| released = 2003
| runtime =250 mts
| country = India
| language = Malayalam
| budget =2 crores
| preceded by =
| followed by =
}}
War and Love is a Malayalam language film. It was released in 2003.   Film was dubbed in Tamil as Kalam.
 

==Plot==
 Suresh Krishna) ISI agent in the Indian Army and pays him 5 crores rupees for capturing the Madras Regiment.

Mushtaq Muhammad hatched a plan in which all of the Madras Regiment except, Gopinath and Vijayan were captured. Gopi kills Mustaq, but was captured by the Pakistanis. All the Prisoners of War were sent to a Pakistani camp, where they were tourtured. They were forced to work like slaves. Kabir, Haneefa, Kurian and Hema (Sharaths love interest) were killed by the Pakis. Meanwhile notorious Pakistani terrorist leader Mansoor Akthar arrived in the camp. Jaffer Khans daughter beautiful Serina (Laila) also arrived there. She was about to be raped by Mansoor, but Gopi killed him by dropping a big rock on the terrorists head. Serina fell in love with Gopi while he decided to use it to save his country. Serina knows Malayalam since Jaffer Khans father migrated from Mallappuram to Pakistan during the Partition. Meanwhile, Captain Vijayan has infiltrated the Pak military and got information that Pakistan is going to use nuclear weapons in the wake of a lost war with India. Gopi with the help of Serina gets access to the defusing codes of the missile. The climax is fully war between Indian POWs freed by Vijayan and Pak army-terrorists. Finally the nuclear bomb is defused by Gopi and entire Pak army is killed. Jaffer Khan is killed by Sharath, who also dies due to injuries. Entire Pak camp is blown up and only Gopi, Serina and Vijayan survives the holocaust. India wins the war and Gopi is given Param Vir Chakra on his return to India. Gopi marries Serina. Movie ends with Serina chanting Bharat Mata Ki Jai.

==Cast==
 Dileep as Captain Gopinath
  Prabhu as Lt.Col Sharath Chandran
 
* Mukesh Rishi as Pak General Jaffer Khan
 
* Laila as Serina
  Siddique as Captain Kabir,Kabirs father

* Kalabhavan Mani as Private Basheer
 
* Jagadish as Havildar Kurian
 
* Machan Varghese as Kunjunni
*Vijayaraghavan as Captain Vijayan Sai Kumar as Major Prabhakar
 Suresh Krishna as Major Rajendran
 
* Sadiq  as Haneefa

* Joju George as Soldier 
* Captain Raju as Brigadir Nair
* Indraja as Captain Hema
*  Hemanth Ravan as Conel Muhammed
* Yamuna as Gopinaths mother
* Indraja as Captain Hemalatha
* Bindu Ramakrishnan as Maheswari
*Kannur Sreelatha as Omana
*Sanusha as Mini Shivaji as Major Varma
*Mafia Sasi as Pakistan Soldier
*Manuraj as Prakashan
* Radhika as Rukhiya
*Sandra Amy as Alima
*Charuhasan as Settu Pappa
*Meena Ganesh as Basheers mother

==Reception==
According to Sify,movie was torture for audiences. 

==Box Office==

The film was an all time disaster at the box office due to bad content and extremely cheap direction and acting.

==References==
 

 
 
 
 
 
 