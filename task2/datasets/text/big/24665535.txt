The Box (2007 film)
{{Infobox film
| name           = The Box
| image          = 
| caption        =
| director       = A.J. Kparr
| producer       = Joseph Agresta Jr. Joe Di Maio Brett Donowho Steve Hovde Alexander Kane James Madio Michael Matthias Dan McDermott Rich Minchik Steve Perkins Margarita Timothee F.X. Vitolo
| writer         = A.J. Kparr
| starring       = Gabrielle Union A.J. Buckley Giancarlo Esposito RZA Jason Winston George Brett Donowho
| music          = James T. Sale   
| cinematography = Sion Michel
| editing        = Frederick Wardell    
| studio         = 
| distributor    = Vivendi Entertainment
| released       = 2007
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Box is a 2007 American crime film starring Gabrielle Union, A.J. Buckley, RZA, Giancarlo Esposito, Jason Winston George, Brett Donowho and written and directed by A.J. Kparr.  

==Plot==
A disgraced former LAPD cop leads a home invasion in search of millions in stolen money.  The crime goes wrong and homicide detectives seeking answers interrogate the only survivors, a thief and one of the victims.

==References==
 

==External links==
* 
* 

 
 
 
 