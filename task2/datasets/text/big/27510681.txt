Ghost Chasers
 
{{Infobox film
| name           = Ghost Chasers
| image          = Ghost Chasers.jpg
| image_size     = 190px
| caption        = Theatrical release poster
| director       = William Beaudine
| producer       = Jan Grippo
| writer         = Charles Marion Bert Lawrence
| narrator       =
| starring       = Leo Gorcey Huntz Hall David Gorcey William Benedict
| music          = Edward J. Kay
| cinematography = Marcel LePicard William Austin
| distributor    = Monogram Pictures
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         =
}}
Ghost Chasers is a 1951 comedy film starring The Bowery Boys. Variety Film Reviews|Variety film review; May 30, 1951, page 6.  Harrisons Reports and Film Reviews|Harrisons Reports film review; May 12, 1951, page 76.  The film was released on April 29, 1951 by Monogram Pictures and is the twenty-second film in the series.

==Plot==
After Slip discovers a spiritualist in the neighborhood, he enlists the boys to investigate.  They discover that she is a fake and working for Margo the Medium, a radio star who has convinced Sach and Whitey that ghosts exist.  They go to her place using Louie as a decoy.  While she is busy trying to connect to Louies dead uncle, the rest of the boys investigate the house.  A real ghost, Edgar, befriends Sach and helps him investigate as well.  Although the others cannot see Edgar, he does help them in times of crisis and helps them uncover Margos scam.

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*William Benedict as Whitmore Whitey Williams
*David Gorcey as Chuck
*Buddy Gorman as Butch

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski
*Lloyd Corrigan as Edgar Alden Franklin Smith
*Jan Kayne as Cynthia
*Lela Bliss as Margo the Medium

==Home media==
Released on VHS by Warner Brothers on September 1, 1998.
 Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Two" on April 9, 2013.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Bowery Battalion 1951
| after=Lets Go Navy! 1951}}
 

 
 

 

 
 
 
 
 
 
 
 
 
 

 