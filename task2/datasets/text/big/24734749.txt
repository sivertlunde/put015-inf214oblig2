Boyhood Loves
 
{{Infobox film
| name           = Boyhood Loves
| image	         = Boyhood Loves FilmPoster.jpeg
| caption        = A poster bearing the films French title:Amour denfance
| director       = Yves Caumon
| producer       = Bertrand Gore
| writer         = Yves Caumon
| starring       = Mathieu Amalric
| music          =
| cinematography = Julien Hirsch
| editing        = Sylvie Fauthoux
| distributor    =
| released       =  
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         =
}}

Boyhood Loves ( ) is a 2001 French drama film directed by Yves Caumon. It was screened in the Un Certain Regard section at the 2001 Cannes Film Festival where it won the Un Certain Regard Award.   

==Cast==
* Mathieu Amalric - Paul
* Lauryl Brossier - Odile
* Fabrice Cals - Thierry
* Michèle Gary - Odette
* Roger Souza - Pauls father
* Bernard Blancan - Aimé
* Nicole Miana - Odiles mother
* Frédéric Bonpart - Jean-Marie
* Paul Crouzet - Auguste
* Deddy Dugay - Suzette

==References==
 

==External links==
* 

 

 
 
 
 
 
 