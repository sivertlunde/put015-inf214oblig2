Candy Rain (film)
{{Infobox film
| name           = Candy Rain
| image          = Candy Rain poster.jpg
| director       = Chen Hung-I
| producer       = Wei Ying-chuan Chen Hung-i Zhang Mei-ling
| writer         = Lin Yan-ru Chen Hung-i Lin A.D.
| starring       = Karena Lam Cyndi Wang Josephine Hsu Lu Chia-Hsin Grace Chen Belle Hsin Sandrine Pinna Waa Wei Niki Wu Kao I-Ling Flora Sun
| music          = Chen Chien-Ci
| cinematography = Fisher Yu
| editing        = Chen Hung-i Liu Chun-Hsiu
| studio         = Red Society Films
| distributor    = Three Dots Entertainment
| released       =  
| runtime        = 106 minutes
| country        = Taiwan
| language       = Mandarin
| budget         = 
| gross          = 
}}
Candy Rain ( ), also known as 愛情糖果雨(àiqíng tángguǒ yǔ), is a 2008 Taiwanese film directed by Chen Hung-I (陳宏一).

== Plot ==
Candy Rain is a romantic drama which combines four intimate, lyrical tales exploring lesbian relationship in contemporary Taiwan. In the first episode, a young girl escapes a broken love for the uncertainties of friendship (and more) in Taipei. In the second, another girl, seeking her ideal, finds herself involved with a wealthy woman instead. The third story follows a heroine trying to find a balance between marriage and separation from her true love. The final story portrays a volatile foursome anchored by singer-actress Karena Lam. A rich, bittersweet spectrum of love and loss, based on true stories.

== Cast ==
*Karena Lam (林嘉欣) ... Ricky
*Cyndi Wang (王心凌) ... Rickys girlfriend
*Josephine Hsu (許安安) ... Rickys ex-girlfriend
*Chia-Hsin Lu (路嘉欣) ... Rickys ex-girlfriend
*Grace Chen (陳泱瑾) ... Pon
*Belle Hsin (辛佳穎) ... Jessie
*Sandrine Pinna (張榕容) ... U
*Waa Wei (魏如萱) ... Lin
*Niki Wu (吳立琪) ... Spancer
*Kao I-Ling (高伊玲) ... Summer
*Flora Sun (孫正華)

==Reception==
Critical response to the film has been mixed.  Kozo of the Love HK Film website called Candy Rain "a light, lesbian-themed art film thats uneven and obvious in execution,"  but Russell Edwards of Variety gave the film mild praise for its "charming" performances and "gracefully shot" cinematography. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 
 