The Hand of Night
{{Infobox film
| name           = The Hand of Night
| image_size     =
| image	         = 
| caption        = 
| director       = Frederic Goode
| producer       = 
| writer         =  based on = 
| starring       = William Sylvester Diane Clare
| music          = 
| cinematography = 
| editing        =  studio = 
| distributor    = 
| released       = 1968
| runtime        =  UK
| English
| budget         =
| preceded_by    =
| followed_by    =
}}
The Hand of Night is a 1968 British horror film directed by Frederic Goode and starring William Sylvester, Diane Clare and Aliza Gur.  It is also known by the alternative title Beast of Morocco. Its plot concerns a man who encounters a dangerous woman while searching for his friend, a missing archaeologist, in Morocco. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 171-174 

==Cast==
* William Sylvester - Paul Carver 
* Diane Clare - Chantal 
* Aliza Gur - Marisa 
* Edward Underdown - Gunther 
* Terence De Marney - Omar  William Dexter - Leclerc 
* Sylvia Marriott - Mrs Petty 
* Avril Sadler - Mrs Carver 
* Angela Lovell - Air Hostess 
* Maria Hallowi - Nurse

==References==
 

==External links==
* 

 
 
 
 
 
 

 