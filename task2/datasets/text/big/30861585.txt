Hum Dono (1995 film)
{{Infobox film 
| name = Hum Dono
| image = HumDono1995film.jpg
| director       = Shafi Inamdar
| writer         = 
| narrator       = 
| music          = Anand-Milind
| starring       = Rishi Kapoor Nana Patekar Pooja Bhatt Mohnish Bahl
| released       = 11 August 1995
| country        = India
| language       = Hindi
}}

Hum Dono is an Indian film starring Rishi Kapoor, Nana Patekar and Pooja Bhatt, which was released in 1995. It was inspired by the film Planes, Trains and Automobiles starring Steve Martin. But the story really looks loosely based on   RAIN MAN  starring  Dustin Hoffman and  Tom Cruise. 

== Cast ==
* Rishi Kapoor....Rajesh Raju
* Nana Patekar....Vishal Saigal 
* Pooja Bhatt....Priyanka Gupta
* Mohnish Bahl....Sameer Choudhury (as Mohanish Behl)
* Mohan Joshi....Shreechand
* Alok Nath....Vikram Saigal

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ek Ladki Hai Deewani Si" 
| Kumar Sanu, Sadhana Sargam
|-
| 2
| "Meri Wafa Meri Dua"
| Abhijeet Bhattacharya|Abhijeet, Sadhana Sargam
|-
| 3
| "Ambar Ki Chandni"
| Udit Narayan, Ravindra Sathe
|-
| 4
| "Pyar Ki Gaadi" Poornima
|-
| 5
| "Aaya Mausam Hum Dono"
| Udit Narayan, Alka Yagnik
|-
| 6
| "Kabhi Dushman"
| Udit Narayan, Ravindra Sathe
|-
| 7
| "Aao Milke Ise Chalaayen"
| Kumar Sanu, Udit Narayan
|}

==External links==
* 

 
 
 
 


 