Ex$pendable
{{multiple issues|
 
 
 
}}
{{Infobox film
| name           = Ex$pendable
| image          =
| alt            =
| caption        =
| director       = Frank E. Jackson, Jr.
| producer       = Sean Shurwil Langston Terry L. Cyrus
| writer         = Julian Phillips and Sean Shurwil Langston
| narrator       = Gary Sturgis
| starring       = Gary Sturgis Taral Hicks Sundy Carter William L. Johnson Thuliso Dingwall Gillie Da Kid Michael Blackson
| music          = Paul Hendricks
| cinematography = Aaron Roberts
| editing        = Frank E. Jackson, Jr.
| studio         = Tough Struggle Entertainment Outspoken Media
| distributor    = Maverick Entertainment Group
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Ex$pendable is a 2008 urban film written by Julian Phillips and Shurwill Langston, and produced by Sean Shurwil Langston and directed by Frank E. Jackson, Jr. It features an ensemble cast that includes Gary Sturgis, Taral Hicks, Omillio Sparks, Sundy Carter, William L. Johnson, Thuliso Dingwall, Gillie Da Kid, and Michael Blackson. This Philadelphia crime drama explores the world of snitches, rival gangs, policemen and corrupt city officials. The film was produced by Tough Struggle Entertainment and Outspoken Media. Ex$pendable was released on DVD in the United States by Maverick Entertainment Group.

==Plot==
On the streets of Philadelphia, competing gangs try to gain control of their territories, but the government sponsored informant system is becoming a nuisance to their corrupt dealings. Raffy Omillio Sparks finds out the hard way that the ‘snitch-network’ has hit him where it hurts most, when he discovers his wife is actually a police informant! Now that the empire that he worked so hard to create is threatened, what will he choose...his love for his wife or his loyalty to the streets?

==Cast==
* Gary Sturgis as Salim
* Taral Hicks as Brenda
* Omillio Sparks as Raffy
* Sundy Carter as Sheena
* William L. Johnson as Arnold
* Thurliso Dingwall as Delio
* Gillie Da Kid as Arthur
* Michael Blackson as Himself
* Antonio Walker as Santana
* Perry Poston as Jason
* Okema Moore as Raquette
* Terron Jones as Packard Finley
* Hasan Bivings as FBI Agent Smith
* Leigha Mai-Ling as Miss Renee

==Details==
Ex$pendable was created from the story by Shurwil Langston of Tough-Struggle Entertainment, who hired freelance writer Julian Phillips (see IMDb and online website for credits), to write the screenplay. Phillips has screen credit on a number of small-budget features, and also numerous educational films and videos, and has also worked as a journalist.

==References==
 
 
 
 

==External links==
*  

 
 
 
 
 