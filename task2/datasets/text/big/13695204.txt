Drifters (2003 film)
{{Infobox film
| name           = Drifters
| image          = Drifters_Poster.jpg
| caption        = 
| director       = Wang Xiaoshuai
| producer       = Peggy Chiao Yong Ning Hsu Hsiao-ming
| writer         = Wang Xiaoshuai
| narrator       = 
| starring       = Duan Long Shu Yan Zhao Yiwei Tang Yang Jin Peizhu
| music          = Wang Feng Wu Di
| editing        = Liao Ching-song
| distributor    = Arc Light Films
| released       =  
| runtime        = 120 minutes
| country        = China Hong Kong
| language       = Mandarin Chinese
| budget         = 
}}
Drifters ( ) is a 2003 Chinese film directed by Wang Xiaoshuai. The film is a production of Hong Kongs Purple Light Films and People Workshop with international distribution through the Taipei-based company Arc Light Films. Drifters premiered in the 2003 Cannes Film Festival as part of the Un Certain Regard competition.   

Drifters follows a young slacker, Hong Yunsheng, who has become something of a local celebrity in part due to his failures as a stowaway. Recently returned to his home in Fujian after several years abroad in the U.S. as an illegal immigrant, Hong attempts to reconnect with an illegitimate son.

== Cast ==
*Duan Long as Hong Yunsheng, the titular drifter in the film. Hong spent several years in the United States as an undocumented immigrant before being deported back to his home in Fujian. He spends his days loafing around his home town. Shanghai Opera troupe. Hongs love interest.
*Zhao Yiwei
*Tang Yang
*Jin Peizhu

== Plot ==
Hong Yunsheng is a jobless wanderer in Fujian. Attempting to find a better life in the United States, he enters the country as an undocumented worker. While there, however, his American dream quickly falls apart when he and his bosss daughter have a child. He was then told not to visit his son and he is made to sign a paper which is like a contract. He violates that by continuing to visit his son. Enraged, Hongs boss informs the INS and has him deported back to Fujian.

Back home, Hong again takes up his old habits, wandering around the town, unemployed and listless. At the same time, he attempts to find romance with a traveling opera performer, Wu Ruifang. Eventually he hears that the child he fathered in the United States is coming to Fujian. Desiring to see the child he has never known, Hong and his boss argue until eventually Hong takes desperate measures and kidnaps the boy.

The consequences for Hong, however, are just beginning.

== Reception ==
Unlike Wangs previous film, Beijing Bicycle, Drifters received mixed reviews from western critics. Derek Elley of Variety (magazine)|Variety claimed that the films "potentially involving story is too often chopped off at the knees," and also found the movies cast to be limited by inexperience.    Other critics were even harsher, finding that the film used over-direction to hide a simple and "syrupy" melodrama.  Still others, however, were more positive. Bérénice Reynaud of Senses of Cinema found the film to be superior to the "superficial" Beijing Bicycle, and put Drifters in the growing category of "mature, disturbing, thought-provoking masterpieces inspired by globalisation." 

== Release ==
Like many of Wangs films, Drifters was screened in the Un Certain Regard competition of the 2003 Cannes Film Festival on May 20.   In addition to this, the film was screened at ten major film festivals around the world. These included:

*2003 Toronto International Film Festival (Contemporary World Film),  September 5, 2003   
*2003 Vancouver International Film Festival (official selection),  October 2003
*2003 Hawaii International Film Festival (official selection),  November 2, 2003 
*2003 Nantes Three Continents Festival (official selection),  November 25, 2003  AFI Film Festival (official selection),  November 2003
*2003 Thessaloniki International Film Festival (Official Programme),  November 2003
*2004 Rotterdam International Film Festival (official selection),  January 24, 2004 
*2004 Seattle International Film Festival (official selection), {{cite web|url=http://www.filmmovement.com/downloads/press/Drifters_Press_Kit.pdf |
title=Drifters_Press_Kit|publisher=Film Movement|accessdate=2007-10-12}}  May 31, 2004
*2004 Karlovy Vary Film Festival (official selection),  July 7, 2004 
*2004 Calgary International Film Festival (official selection),  October 2, 2004 

===Home media=== Region 1 Mandarin with English subtitles. Robot Boy by Ted Passon.  The discs aspect ratio was  1.78:1 in letterbox format.

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 