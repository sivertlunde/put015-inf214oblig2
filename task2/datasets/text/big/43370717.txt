Chinatown at Midnight
{{Infobox film
| name           = Chinatown at Midnight
| image          =
| image_size     =
| caption        =
| director       = Seymour Friedman
| producer       = Sam Katzman
| story          =
| screenplay     = Robert Libott Frank Burt
| based on       =
| writer         =
| narrator       =
| starring       = Hurd Hatfield Jean Willes Tom Powers
| music          =
| cinematography = Henry Freulich
| editing        = Edwin H. Bryant
| distributor    = Columbia Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States English
| budget         =
}}
Chinatown at Midnight (1949 in film|1949) is a film noir and police procedural starring Hurd Hatfield, Jean Willes and Tom Powers, and directed by Seymour Friedman.

==Cast==
*Hurd Hatfield	...	Clifford Ward
*Jean Willes	...	Alice
*Tom Powers	...	Capt. Howard Brown
*Ray Walker	...	Sam Costa Charles Russell	...	Fred Morgan
*Jacqueline deWit	...	Lisa Marcel
*Maylia	...	Hazel Fong
*Ross Elliott	...	Eddie Marsh
*Benson Fong	...	Joe Wing
*Barbara Jean Wong	...	Betty Chang
*Victor Sen Yung	...	Hotel Proprietor
*Josephine Whittell	...	Mrs. Emily Dryden

==Plot==
The SF Police Department pursues a serial killer on the loose in San Franciscos Chinatown.

==Preservation status==
In 2014, a pristine 35mm print of the film was struck from the Columbia Pictures archives and subsequently exhibited at the Museum of Modern Art in New York City.

==External links==
* 

 
 
 
 

 