Entr'acte (film)
{{Infobox film
| name           = Entracte
| image          = Satie & Picabia, Clair & Biorlin (prologue de Relache).jpg
| caption        = Erik Satie, Francis Picabia, René Clair and Jean Börlin in Entracte
| director       = René Clair
| producer       = Rolf de Maré
| writer        = René Clair Francis Picabia
| starring       = Jean Börlin Inge Frïss
| music          = Erik Satie
| cinematography = Jimmy Berliet
| editing        =
| distributor    = Société Nouvelle des Acacias
| released       =  
| runtime        = 22 minutes
| country        = France
| language       = French
| budget         =
}}
Entracte is a 1924 French short film directed by René Clair, which premiered as an entracte for the Ballets Suédois production Relâche (ballet)|Relâche at the Théâtre des Champs-Élysées in Paris. Relâche is based on a book and with settings by Francis Picabia, produced by Rolf de Maré, and with choreography by Jean Börlin. The music for both the ballet and the film was composed by Erik Satie.

==Production==

===Development===
For this production, the   included cameo appearances by Francis Picabia, Erik Satie, Man Ray, and Marcel Duchamp. The conductor of the orchestra at the premiere was Roger Désormière.

==Release==

===Film sequence===
The two parts of the film are as follows (note that time indications are approximate, since neither film nor music techniques at the time of the premiere allowed exact temporisation in a public performance):
*A sequence of about 90 seconds, starring Satie and Picabia firing a cannon from the top of a building. This sequence, as silent movie, was played at the beginning of the ballet, right after the "little overture" ("Ouverturette"), and before the curtain raised ("Rideau"). The music to this part of the film is called "Projectionnette", and is included as 2nd item in the Relâche partition. There appears no real effort for music to film synchronisation in this part of the film. Probably the "Projectionnette" music was played two or three times before proceeding to the "Rideau" part of the music.
*The rest of the film was played as entracte between the two acts of the ballet. The score for this part of the film is not included in the Relâche partition, but was written down by Satie in a separate score, titled "Cinéma". This part of the music contains "expandable" repeat zones, in order to match the start of a new tune with certain events in the film, thus it was one of the earliest examples of music to film synchronization. In the score, Satie names 10 sections that are associated with scenes in the film.
 Cannes Film Festival.   

===DVD===
The film is included on the Criterion Collection DVD of Clairs À Nous la Liberté (1931 in film|1931).

==References==
 

==External links==
* 
* 
*   

 
 

 
 
 
 
 
 