Zeta One
{{Infobox film
| name           = Zeta One
| image          = "Zeta_One".jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Italian poster
| film name      = 
| director       = Michael Cort
| producer       = George Maynard
| writer         = {{plainlist|
*Michael Cort
*Alistair McKenzie}}
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Johnny Hawksworth
| cinematography = Jack Atchelor
| editing        = {{plainlist|
*Jack T. Knight
*Dennis Lanning}}
| studio         = Tigon British Film Productions Tigon Film Distributors
| released       = 1970
| runtime        = 82 minutes
| country        = United Kingdom
| language       = 
| budget         = £60,000
| gross          = 
}} British comedy comedy science Charles Hawtrey and Robin Hawdon. 

It was made for a budget of £60,000. John Hamilton, Beasts in the Cellar: The Exploitation Film Career of Tony Tenser, Fab Press, 2005 p 145  The movie has developed a cult following for its absurd and slow-moving plot line, and partial nudity. 

==Cast==
* James Robertson Justice - Major Bourdon Charles Hawtrey - Swyne
* Robin Hawdon - James Word Anna Gaël - Clotho
* Dawn Addams - Zeta
* Brigitte Skay - Lachesis
* Valerie Leon - Atropos
* Lionel Murton - W
* Yutte Stensgaard - Ann Olsen
* Wendy Lingham - Edwina Ted Strain
* Carol Hawkins - Zara
* Rita Webb - Clippie
* Steve Kirby - Sleth
* Paul Baker - Bourdons Assistant
* Angela Grant - Angvisa Girl Kirsten Betts - Angvisa Girl

==Production==
Zeta One was the first film shot at Camden Studios, which was formally a wallpaper factory in North London. I.Q. Hunter, British Science Fiction Cinema, Routledge, 2001 p 69  The plot of the film was based off a short story in the magazine Zeta. 

==Release==
Zeta One was released in the United Kingdom in 1970.  The film was not a commercial success on its release. 

==Reception== David McGillivray described the films themes as "quite preposterous in illogicality and silliness". 

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 
 