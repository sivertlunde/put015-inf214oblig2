The New Adventures of J. Rufus Wallingford
 
{{Infobox film
| name           = The New Adventures of J. Rufus Wallingford 
| image          = Wallingford.001.jpg
| caption        = Lolita Robertson standing. Oliver Hardy on steps of train. Harry Mainhall, Jr. pointing. James Gordon Leopold Wharton Theodore Wharton
| producer       = Leopold Wharton Theodore Wharton
| writer         = George Randolph Chester Charles W. Goddard George B. Seitz
| starring       = Burr McIntosh
| cinematography = Ray June
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

The New Adventures of J. Rufus Wallingford is a 1915 American silent film serial featuring Oliver Hardy.

==Cast==
* Burr McIntosh as J. Rufus Jim Wallingford Max Figman as Blackie Daw Lolita Robertson as Violet Warden
* Frances White as Fanny Warden
* Edward OConnor as Onion Jones
* Harry Mainhall as Benzy Falls Jr.
* Allan Murnane as Andre Perigourd
* Oliver Hardy (as O.N. Hardy)
* Harry Robinson
* Dick Bennard
* F.W. Stewart
* Violet Palmer
* Malcolm Head
* Frederic De Belleville as Jim Wallingford
* Joseph Urband

==See also==
* List of American films of 1915
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 