Maathi Yosi
 
{{Infobox film
| name           = Maathi Yosi
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Nandha Periyasamy
| producer       = P. S. Sekar Reddy
| writer         = Nandha Periyasamy
| narrator       = 
| starring       =  
| music          = Guru Kalyan
| cinematography = Vijay Armstrong
| editing        = Kola Bhaskar
| studio         = PSSR Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
| preceded by    = 
| followed by    =
}}
Maathi Yosi ( ) is a 2010 Indian Tamil-language drama film written and directed by Nandha Periyasamy and music by Guru Kalyan. Starring Harish, Kanchivaram fame Shammu along with newcomers Gopal, Alex and Lokesh in the lead roles, the film opened to mixed and negative reviews.

== Cast ==
* Harish as Pandi
* Shammu
* Gopal as Maanga
* Alex as Onaan
* Lokesh as Mari
* Ravi Mariya
* Ponvannan
* G. M. Kumar
* M. Sasikumar as narrator

==Plot==
The voice of director Sasikumar to introduce the four mischievous guys Pandi, Mangaa, Kona, Maari, in his typical Madurai lingo who think they rule the Kadavur village.

These four friends are united, very close and live for each other. They always think out-of-the box. They turn out to be vagabonds and go to the extent of even killing the village Presidents son. After few similar delinquent experiences they are forced to leave the village and end up coming to Chennai.

Heres where the main plot begins- how they meet the heroine, and what happens in Chennai with the four friends and the girl forms the rest of the story.

==Reception==

The film received highly negative reviews from critics and was a disaster in the box office.The audience even wondered why such a film was even produced.The film was taken away from the theatres in just 2 days.Some people even suggested that this director should be banned from making such stupid films.

==External links==
*  

 
 
 
 


 