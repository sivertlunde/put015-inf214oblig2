Baddi Bangaramma
{{Infobox film
| name           = Baddi Bangaramma
| image          = 
| caption        = 
| director       = Kommineni
| producer       = Y. V. Rao
| writer         = 
| screenplay     = Kommineni
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          = K. Chakravarthy
| cinematography = S. S. Lal Kabir Lal
| editing        = D. Rajagopal Rao
| studio         = Ravichithra Films
| distributor    = 
| released       =  
| runtime        = 142 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Ramakrishna and Uma Shivakumar, who played the titular role of Baddi Bangaramma, in pivotal roles. Uma Shivakumars performance as a moneylender in the film received appreciation and was henceforth referred to as Baddi Bangaramma by the audience. 

==Cast==
* Srinath
* Jai Jagadish Ramakrishna
* Bhavya
* Dinesh
* Uma Shivakumar
* Mahalakshmi
* Jayamalini
* Anuradha
* P. R. Varalakshmi
* Jayavijaya
* Kunigal Nagabhushan
* Shivaprakash
* Sarigama Vijaykumar
* Pemmasani Ramakrishna

==Soundtrack==
{{Infobox album
| Name        = Baddi Bangaramma
| Type        = Soundtrack
| Artist      = K. Chakravarthy
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1984
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Saregama
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

K. Chakravarthy composed the music for the film, with lyrics for the soundtracks written by Dodddarange Gowda and R. N. Jayagopal. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Nanna Srimathi Aagodu Yaavaaga
| lyrics1 = 
| extra1 = P. Susheela, Raja
| length1 = 
| title2 = Prathi Dinavu Ide Kathe
| lyrics2 = Doddarange Gowda
| extra2 = P. Susheela, Ramesh
| length2 = 
| title3 = Ee Nanna Kannalli
| lyrics3 = R. N. Jayagopal
| extra3 = S. P. Balasubrahmanyam, P. Susheela
| length3 = 
| title4 = Thaalayya Swalpa Neenu
| lyrics4 = R. N. Jayagopal
| extra4 = P. Susheela, Ramesh, Raja
| length4 = 
| title5 = Ibbare Naavillibbare
| lyrics5 = Doddarange Gowda
| extra5 = P. Susheela, Jayachandran
| length5 = 
}}

==References==
 

==External links==
*  

 
 

 
 

 