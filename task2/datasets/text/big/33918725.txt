Nearly Married
{{infobox film
| name           = Nearly Married
| image          =
| imagesize      =
| caption        =
| director       = Chester Withey
| producer       = Samuel Goldwyn
| writer         = Lawrence McCloskey (scenario) Margaret Mayo
| based on       =  
| cinematography = Arthur Edeson
| editing        =
| distributor    = Goldwyn Pictures
| released       =  
| runtime        = 60 mins.
| country        = United States
| language       = Silent English intertitles
}}
 silent comedy film directed by Chester Withey and starring Madge Kennedy. It is based on a 1913 stage play of the same name by Edgar Selwyn. The film also featured an early film appearance by future gossip columnist Hedda Hopper. 

The Library of Congresss new(Dec. 2013) American Silent Feature Film Survival Database has this film listed as being an abridgement in their collection. 

==Cast==
*Madge Kennedy - Betty Griffon
*Frank M. Thomas - Harry Lindsey
*Mark Smith - Tom Robinson
*Alma Tell - Gertrude Robinson
*Richard Barthelmess - Dick Griffon
*Hedda Hopper - Hattie King

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 