She and He (1963 film)
 
{{Infobox film
| name           = She and He
| image          = Kanojo to kare.jpg
| caption        = Poster for She and He.
| director       = Susumu Hani
| producer       = Teizô Koguchi Masayuki Nakajima
| writer         = Susumu Hani Kunio Shimizu
| starring       = Sachiko Hidari  Eiji Okada
| music          = Tōru Takemitsu
| cinematography = Juichi Nagano
| editing        = Noriaki Tsuchimoto
| distributor    = 
| released       = 18 October 1963
| runtime        = 109 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

  is a 1963 Japanese drama film directed by Susumu Hani. It was entered into the 14th Berlin International Film Festival where Sachiko Hidari won the Silver Bear for Best Actress award.   

==Plot==
A middle class woman in Tokyo, Naoko Ishikawa (Sachiko Hidari) lives with her husband in a shining new apartment building on a hill overlooking a slum. As her husband Eiichi (Eiji Okada) becomes more entangled in his life as businessman, Naoko looks for ways to expand her own life even as her husbands life shrinks in scope and intimacy. She loses her sense of security when she becomes acquainted with poverty in her neighborhood. She finds herself strangely drawn to a rag-picker, Ikona (Kikuji Yamashita) who lives down below in a tin shack with a blind child and a dog, and the sheltering comforts of her middle-class existence inexorably fall away.

==Cast==
* Sachiko Hidari - Naoko Ishikawa
* Kikuji Yamashita - Ikona
* Eiji Okada - Eiichi Ishikawa
* Akio Hasegawa - Laundry Boy
* Yoshimi Hiramatsu - Nakano
* Setsuko Horikoshi - Old lady of book store
* Takanobu Hozumi - Doctor
* Hiromi Ichida - Nurse
* Mariko Igarashi - Blind girl
* Hiro Kasai - Ghetto guy
* Shûji Kawabe - Detctive
* Toshie Kimura - Sasaki
* Masakazu Kuwayama - Laundry owner
* Toshio Matsumoto - Laundry man
* Yukio Ninagawa - Balloon guy
* Kazuya Oguri
* Miyoko Takahashi - Ghetto woman

== Awards ==
In 1984, Sachiko Hidari won the Silver Bear for Best Actress at the 14th Berlin International Film Festival also for her another film The Insect Woman directed by Shohei Imamura. The film was nominated for Golden Bear, but won OCIC Award and Youth Film Award for best feature film.
In Japan, Hidari won Best Actress at Blue Ribbon Awards for Best Actress| Blue Ribbon Award, Mainichi Film Award for Best Actress| Mainichi Film Award and Kinema Junpo Award for Best Actress| Kinema Junpo Award.
 {{cite web |url=http://www.imdb.com/title/tt0057218/awards?ref_=tt_awd |title=IMDB Awards for 
She and He (1963) |accessdate=2014-10-23 |work=IMDB.com}} 

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 