Felicity Land
 
{{Infobox film
| name           = Felicity Land
| image          = Felicity Land film poster.jpg
| caption        = Film poster
| director       = Maziar Miri
| producer       = Homayoun Assadian
| writer         = Amir Arabi
| starring       = Mahnaz Afshar
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Iran
| language       = Persian
| budget         = 
}}

Felicity Land ( ) is a 2011 Iranian drama film directed by Maziar Miri.    

==Cast==
*Hamed Behdad as Mohsen
* Leila Hatami as Yasi
* Mahnaz Afshar as Laleh
* Hossein Yari as Bahram
* Amir Aghaei as Ali
* Hengameh Ghaziani

==Awards==
* Best Supporting Actress Award for Mahnaz Afshar at Fajr Film Festival
* Best Editing Award at the 15th Iranian Feast of Cinema
* Nominee for the Best Original Screenplay, Best Supporting Actor (Hossein Yari), Best Supporting Actress (Mahnaz Afshar & Hengameh Ghaziani) and Best Photography at the 15th Iranian Feast of Cinema

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 