Lumière and Company
{{Infobox film
| name           = Lumière and Company
| image size     = 
| image	=	Lumière and Company FilmPoster.jpeg
| alt            = DVD cover, blue with white lettering
| caption        = 
| director       = Several (see Lumière and Company#Directors|Directors)
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Lumière and Company (1995, original title  ) was a collaboration between forty-one international film directors in which each made a short film using the original   camera invented by the Lumière brothers.      

Shorts were edited in-camera and constrained by three rules:   
#A short may be no longer than 52 seconds
#No synchronized sound
#No more than three takes

==Directors==
{{columns-list|colwidth=23em|
*Sarah Moon
*Merzak Allouache
*Gabriel Axel
*Vicente Aranda
*Theo Angelopoulos
*Bigas Luna
*John Boorman
*Youssef Chahine
*Alain Corneau
*Costa-Gavras
*Raymond Depardon
*Francis Girod
*Peter Greenaway
*Lasse Hallström (starring Lena Olin)
*Michael Haneke
*Hugh Hudson
*Gaston Kaboré
*Abbas Kiarostami
*Cédric Klapisch
*Andrei Konchalovsky
*Patrice Leconte
*Spike Lee
*Claude Lelouch
*David Lynch Merchant & Ivory (music Richard Robbins)
*Claude Miller
*Idrissa Ouedraogo
*Arthur Penn
*Lucian Pintilie
*Jacques Rivette (starring Nathalie Richard)
*Helma Sanders-Brahms
*Jerry Schatzberg
*Nadine Trintignant
*Fernando Trueba
*Liv Ullmann (starring Sven Nykvist)
*Yoshishige Yoshida
*Jaco Van Dormael (starring Pascal Duquenne)
*Régis Wargnier
*Wim Wenders
*Zhang Yimou
}}

==References==
 

==External links==
* 
* 

{{Navboxes title = Directors titlestyle = background:#D9C7A5; list =
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
}}


 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 