Detective K: Secret of the Virtuous Widow
{{Infobox film
| name           = Detective K: Secret Of Virtuous Widow 
| image          = Detective_K_poster.jpg
| film name      = {{Film name
| hangul         =  :    
| hanja          =  명 : 각시투구꽃의  
| rr             = Joseon Myeongtamjeong: Gakshituku Ggotui Bimil
| mr             = Chosŏn Myŏngt‘amjŏng: Kaksit‘ugukkotŭi pimil}}
| director       = Kim Sok-yun  
| producer       = Kim Jho Kwang-soo   Lee Seon-mi 
| writer         = Lee Chun-hyeong   Lee Nam-gyu  
| based on       =  
| starring       = Kim Myung-min  Han Ji-min   Oh Dal-su 
| music          = Kim Han-jo   Eom Gi-yeop
| cinematography = Jang Nam-cheol 
| editing        = Kim Sun-min
| studio         = Generation Blue Films
| distributor    = Showbox/Mediaplex
| released       =  
| runtime        = 115 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
Detective K: Secret of the Virtuous Widow ( ) is a 2011 South Korean film based on the novel by Kim Tak-hwan, starring Kim Myung-min in the lead role.  It was the 4th best selling Korean film of 2011. 

==Plot== Jeongjo became the King of Joseon, a series of murders occurs. King Jeongjo believes the murders may belong to a conspiracy by government officials to cover up tributary payments. King Jeongjo then gives Detective K (Kim Myung-min) a secret order to find out who is behind the killings.
 Wolfsbane flower are found near the long metal needle. But, while Detective K is holding the murder weapon, prison guards come into the cell and assume that Detective K murdered the city governor. Now imprisoned, Detective K awakes to find dog fancier Seo-pil (Oh Dal-su) standing over him. Seo-pil helps Detective K escape from prison.

Because of this incident, King Jeongjo demotes Detective K and reassigns him to Jeokseong to investigate the case of a woman thought to have killed herself after the death of her husband. But, this reassignment is more of a rouse for Detective K to get to Jeokseong – the area where the Wolfsbane flower blooms.
 Lee Jae-yong) are embezzling taxes to pay off politicians. Meanwhile, Detective K also investigates the case of the woman who reportedly committed suicide after the death of her husband and comes to the conclusion that these two cases are somehow related.

==Cast==
*Kim Myung-min – Detective K
*Han Ji-min – Han Kaek-ju/Lee Ah-young
*Oh Dal-su – Han Seo-pil Lee Jae-yong – Minister Im
*Woo Hyeon – Mr. Bang
*Ye Soo-jung – Ims wife
*Choi Moo-sung – medical guru
*Jung In-gi – magistrate
*Lee Seol-gu – servant 4
*Choi Jae-sup – Lee Bang
*Moon Kyung-min – old blacksmith Kim Tae-hoon – Im Geo-seon King Jeongjo

==Release==
The film was released in South Korea on January 27, 2011. It received a theatrical run in 10 cities in the U.S. and Canada in March 2011, including Los Angeles, San Francisco, Atlanta, Seattle, Chicago, Dallas, Hawaii and Vancouver.  It was also sold to Australia, China, Taiwan, Thailand, Germany, Austria, and Switzerland, and screened at the Hawaii International Film Festival. 

==Sequel==
 
The sequel is planned for release in February 2015. Kim Myung-min and Oh Dal-su reprised their roles, and are joined by Lee Yeon-hee as a femme fatale.  

==References==
 

==External links==
*    
*   at Naver  
*  
*  
*  

 
 
 
 
 
 