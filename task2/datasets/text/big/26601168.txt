Shudharil Shudhan
 
{{Infobox film
| name           = Shudharil Shudhan
| image          = Shudharil Shudhan.jpg
| caption        = 
| director       = Jayaraj Vijay
| producer       = Joy Jacob
| writer         = 
| starring       = Indrans   Kalabhavan Mani
| music          = Jaison J. Nair
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Shudharil Shudhan is a 2009 Malayalam film by debutant director Jayaraj Vijay, starring Mukesh (actor)|Mukesh, Kalabhavan Mani and Indrans.

The film is about  an estate worker named Ramankutty (Indrans) who is forced to lead a strike against the estate management, leaving his family in starvation. 

==Cast== Mukesh as Mohanachandran Pilla
* Lakshmi Sharma as Janaki
* Indrans as Ramankunju
* Kalabhavan Mani as Sankarankutty
* Sudheesh as Chandran Saikumar as Joseph
* Mamukkoya as Jabbar
*Maniyanpilla Raju as Kanaran
* T. G. Ravi as Patta Krishnan
*  Kollam Thulasi as Fernandez
* Geetha Vijayan as Ramani
* Sona Nair as Panki
* Manasi

==Songs==
The film has five songs which were composed by Jaison J. Nair. The songs were written by Vayalar Sarath Chandra Varma and Inchakkadu Balachandran.  

{| class="wikitable"
|-
!Song
!Singer(s)
!Lyrics
|-
| Kanpeeliyil
| Sujatha Mohan
| Vayalar Sarath Chandra Varma
|-
| Puzha Paadum Thazhvaaram
| Sangeetha Prabhu
| Vayalar Sarath Chandra Varma
|-
| Ennum Ormakal
| Jaison J. Nair
| Inchakkadu Balachandran
|-
| Ayyayya
| Jaison J. Nair
| Inchakkadu Balachandran
|-
| Kanpeeliyil
| G. Venugopal
| Vayalar Sarath Chandra Varma
|}

==References==
 

==External links==
*  

 
 
 


 