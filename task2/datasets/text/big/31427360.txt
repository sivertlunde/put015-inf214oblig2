Samooham
{{Infobox film
| name           = Samooham
| image          = Samooham.gif
| image size     = 
| alt            = 
| caption        = Film poster
| director       = Sathyan Anthikad
| producer       = Raju Mathew
| writer         = J. Pallassery Suhasini Sunitha Sunitha Manoj K. Jayan Sreenivasan Vineeth Johnson
| cinematography = Vipin Mohan
| editing        = K. Rajagopal
| studio         = Century Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Sunitha in the lead roles.

==Plot==
The film revolves around a middle class Malayalee girl and her childhood lover. Problems arise in their relationship when she begins her rise to power.The film was a flop at the box office.

==Cast== Suhasini .... M.L.A Rajalakshmi
* Suresh Gopi  .... Sudhakaran Sunitha ... Radhika
* Vineeth .... Udayan Sreenivasan .... Ramachandran/Pavithran
* Manoj K. Jayan .... Majeed
* Nedumudi Venu ..... Balan Poduval
* Jose Pellissery .... Sukumaran Nair
* Zeenath
* Rajan P. Dev
* Sudheesh  .... Johny Shivaji .... Doctor
* T. R. Omana
* Bindu Panicker ... Gopika
* Bobby Kottarakkara
* Mavelikkara Ponnamma .... Sudhakarans mother
* Suvarna Mathew .... Thulasi
* Sankaradi
* Kollam Thulasi .... Chief Minister
* Kundara Johny .... Anto
* C. I. Paul ... Minister Narayanan Nair
* Devi S. .... child atist

==Soundtrack==
{{Infobox album
| Name        = Samooham
| Type        = Soundtrack Johnson
| Language = Malayalam
| Genre       = Film
|}}
 Johnson and written by Kaithapram Damodaran Namboothiri|Kaithapram.

{| class="wikitable"
|-
! Track !! Song Title !! Singer(s)
|-
| 1 || Kandeno (Odakkombil Kaattu Kinungippoy) || K.S. Chithra 
|-
| 2 || Sree raghukula || K.S. Chithra 
|-
| 3 || Thoomanjin nenchilothungi || K. J. Yesudas
|}

==External links==
*  
*   at the Malayalam Movie Database
 
 
 

 