Dooly the Little Dinosaur
 
{{Multiple issues|
 
 
 
}}

Dooly the Little Dinosaur ( ) is a 1983 South Korean cartoon and animated film created by Soo Jung Kim. 

Dooly is one of the most respected and commercially successful characters of South Korean animation. Dooly became the first manhwa character to be featured on a postal stamp|stamp. It was printed in 1995 in South Korea. Dooly has a resident registration card, which means he is a citizen of South Korea.
== History == Soo Jung comic book named Bo-Mool Sum (Treasure Island). It was first shown in the first week of April, 1983, finished about ten years later. There are two Spin-off (media)|spin-offs of the manhwa, one of which is still being aired today.
 broadcaster Korean Broadcasting System|KBS.  When it was first broadcast in 1987, there were only six episodes, with seven more later added during the second season. A video cassette was released in 1989, and re-broadcasting was first done in 1990.

This show was rebroadcast again in 1993, 1996 and 1998, but due to the choosing inappropriate broadcasting dates (usually on Sunday), its recognizance and the viewer rating were critically low. In 1998, however,  , Koreas first all-anime/manhwa channel.

The cartoon was once reported to the officials as a bad example for children because of the using of Doolys care-givers name in a friendly fashion. Koreans usually speak to adults with respect and with the use of proper additions in speech done so specifically for adults by younger people. This was seen as hazardous for the basic thinking of the children who watch it.

Tooniverse also released an educational English dub in 1995, and a theatrical release came out in 1996 under Dooly Nara (Dooly Nation).

==Characters== ice glacier Kildong Go. Korean Traditional-Chinese, canon due anime and manhwa having many storyline differences. While Dooly is having adventures in Seoul or having fun, he met some three friends who Kildong hates too and beat up except Heedong (희동). 
 Spaceship

*Douner ( ) the Prince of Planet Kanttappia makes an emergency landing on the planet Earth due to the malfunctioning of the Time Cosmos Spaceship. His head may be really hard and his thoughts very simple, but the word "loyalty" was made for Douner. He can travel back and forth in time and space in the Time Cosmos Spaceship which looks like a violin.
 Las Vegas circus troop, always insists that she used to be a noble mistress from Africa. She is a little bit selfish, and little bit shy, but Ddochi is the most softhearted ostrich that anyone will ever come across. She also insists on calling herself a girl. Ddochi has many talents as she learned from the circus.

*Michol ( , pronounced as "Maikol") is aspiring singer who lives next to Kildong feeds on his passion for dreaming, though he may lack the necessary talent. He is quite lost most of the time, but he does know how to take care of himself. Although he is South Korean, his appearance is noticeable different. His surname is Ma. According to his name, his role model is Michael Jackson. Dooly once joined a band with Douner and Michol.

==Controversy==
When Dooly was first created as a TV animation, it received much criticism from South Korean Catholics,  South Korean public citizens, and the government of South Korea|government. Reasons cited included the fact that the animation was a bit violent, which was oppressed by South Korean government, and the adult general public was angered due to its characters attitude of disrespect towards elders.  However, this was when freedom of speech was not taken seriously into account.  Generally speaking, the younger generation has fond memories of this series and find that the violence is actually a quite realistic portrayal of family dynamics in contemporary South Korea.

==List of episodes==
===Original series (1987–1988)===
# "The Birth of Dooly" ( ; original airdate: October 7, 1987)
# "Grand Park Disturbance" ( ; original airdate: October 7, 1987)
# "My Friends" ( ; original airdate: October 7, 1987)
# "Hyeongah! Do Not Go" ( ; original airdate: October 7, 1987)
# "Travel to the Past: Part 1" ( ; original airdate: October 7, 1987)
# "Travel to the Past: Part 2" ( ; original airdate: October 7, 1987)
# "Doolys Anger" ( ; original airdate: May 5, 1988)
# "Michols Debut" ( ; original airdate: May 5, 1988)
# "Ddochis Escape" ( ; original airdate: May 5, 1988)
# "The Strange Lamp" ( ; original airdate: May 5, 1988)
# "Doolys Miniature" ( ; original airdate: May 5, 1988)
# "The Invaders from the Squid Planet: Part 1" ( ; original airdate: May 5, 1988)
# "The Invaders from the Squid Planet: Part 2" ( ; original airdate: May 5, 1988)
 
==In popular culture==
#The Dooly theme song was sung in "A Gentlemans Dignity" 2012 South Korean drama, In episode 13 at 58.48min into the show by Collin acted by Lee Jong-hyun member of CNBlue rock band
#The stuffed dooly dolls and his girlfriend Gong Sil was in "Masters Sun"   2013 South Korean television series  episode 7 at 52.17 min and episode 10 39.52min  episode 13 44min
#"My Girlfriend Is a Nine-Tailed Fox"  when Dae Woong teaches Miho the fox how to be friends with hoi hoi
  
==References==
 
 
==External links==
*  
*  

 
 
 