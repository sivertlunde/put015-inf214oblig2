Marianne and Juliane
{{Infobox film name     = Marianne and Juliane image    = caption  = writer   = Margarethe von Trotta starring = Jutta Lampe Barbara Sukowa director = Margarethe von Trotta producer = Eberhard Junkersdorf released = 1981 runtime  = 106 minutes country  = West Germany language = German
|
}} German title is Die bleierne Zeit, an idiomatic expression which can be translated as "the leaden times".
 Stammheim in 1977. In the film, Von Trotta depicts the two sisters Juliane (Christine) and Marianne (Gudrun) through their friendship and journey to understanding each other. Marianne and Juliane was von Trottas third film and solidified her position as a director of the New German Cinema.

==Plot==
Two sisters, both dedicated to Womens rights|womens civil rights, fight for the same cause, although in very different ways. The story is interspersed with flashbacks into the sisters childhood.
 terrorist group. takes his life leaving Jan without a guardian. 

Marianne shows up on the scene to discuss her political views with her sister and urge her to join the movement. Juliane informs her of her husband’s suicide and of her intent to find a foster home for Jan. Marianne asks her sister to watch over Jan but Juliane replies “you would have me take on the life that you chose to leave”. Basically stating “so what’s not good enough for you is good enough for me”. Juliane sticks by her guns and refuses to take on Jan. This fact still does not stop Marianne from continuing on in the movement. She is content to allow Jan to have the life that will be dealt to him through foster care because she believes that “any life he has in foster care will be better than the life many children have in third world countries.” The sister’s paths continue to cross as Marianne bursts in unannounced to her sister’s life. The last time that Juliane sees her sister before she is arrested, Marianne wakes her and her long-term boyfriend up at 3 a.m. and wants to make coffee and take Juliane’s clothes. Soon afterward, we discover that Marianne has been arrested and is being held in a high security prison. Juliane goes to visit her sister. When she arrives at the prison she is ordered to strip down and is Strip search|searched. Then she is let into the waiting room and told to wait for her sister. The guard returns and informs her that Marianne refuses to see her.

Juliane goes home agonizing over her inability to communicate with her sister and see how she is doing. Her boyfriend suggests that she write a letter to her sister telling her how she feels. The film goes into a flashback of their childhood where we see the closeness of the sisters. Juliane mails the letter and soon after is able to visit with her sister. They argue often but Juliane continues to come and visit her sister. They have a bad argument on one of these occasions and Marianne slaps her sister. Marianne is then moved to a Supermax|maximum-security prison where the two must sit on the opposite sides of a pane of glass and communicate through an intercom. 

Juliane becomes so obsessed with her sister and her problems that her own relationships begin to fall apart. Her boyfriend suggests that the two of them take a vacation together. While on vacation they see the face of Marianne on TV but cannot understand what has happened to her because of the language barrier. Juliane runs back to their hotel and calls her parents to find that Marianne has “committed suicide”. Juliane and her father do not believe that Marianne actually killed herself. Juliane begins an obsessive journey to discover what really happened to her sister. This destroys her relationship with her boyfriend of ten years. She ultimately proves to herself that Marianne was murdered but when she calls the papers with the news, she is informed that her sisters death is “old news” and nobody cares if it was murder or suicide. Juliane is left with the knowledge but cannot convince the papers to defend the name of a dead terrorist. 

Later Juliane is reunited with Jan because someone attempts to murder him when they find out who his mother was. Juliane takes him back home with her after he has undergone extensive reconstructive surgery. He is aloof and has no interest in having a relationship with his aunt. He has nightmares of the fire that nearly killed him. 

The film ends with him walking into Juliane’s workroom and tearing up the picture of his mother that is on the wall. Juliane tells him “you are wrong, Jan. Your mother was a great woman. I’ll tell you about her”. Jan says that he wants to know everything and then yells “start now! Start now!” The film fades out on Juliane’s face looking at him.

==Cast==
 

==Crew==
Produced by:
Eberhard Junkersdorf

Cinematography by:
Franz Rath

Edited by:
Dagmar Hirtz

Production Design:
Barbara Kloth
and Georg von Kieseritzky

Costume Design
Monika Hasse
Jorge Jara

Make-up
Rüdiger Knoll

Production Management
Ute Ehmke
Lotti Essid

Art Department
Werner Mink

==Reception==
This film was well received and became a platform for Von Trotta as a director of the new German cinema. Though she was not as highly recognized as her male counterparts, the New German Cinema and the study of the more human side of contemporary political issues (like terrorism in this case) became her focus. In regards to the film, Barton Byg notes, "rather than criticize hysterical responses to terrorism, the film employs its emotive power" (Finn 47). In America the film was pitched to be less about terrorism and the emotional side of the strained relationship but more about a sisterly relationship that was searching for understanding. (Finn) The film did not meet only with praise. It was also criticized for attempting to "hide" its meaning behind the sister-sister relationship. A meaning that was empathetic to the plight of the terrorist-activist. Charlotte Delorme, a critic, stated: "If Marianne and Juliane were really what it claims to be it would not have gotten any support, distribution, and exhibition." (Finn)

==Accolades==
At the 1981 Venice Film Festival, Trotta won the Golden Lion and the FIPRESCI awards, while the actresses who played the title sisters tied for Best Actress. In 1982, the film won the Outstanding Feature Film Award in West Germany, and Trotta received a special award commemorating the 40th anniversary of the Federal Republic of Germany.

At the  , an International Womans Film Festival, 1981, the film won the Prix du Publique and Prix du Jury.

==Sources==

*  at the IMDb
*Susan E. Linville: Retrieving History: Margarethe von Trottas Marianne and Juliane, PMLA, Vol. 106, No. 3 (May 1991), pp.&nbsp;446–458
*Jump Cut: A Review of Contemporary Media, no. 29, February 1984, pp.&nbsp;56–59
*Finn, Carl: The New German Cinema: Music History, and the Matter of Style, University of California Press, 2004

== External links ==
*  

 
 

 
 
 
 
 
 