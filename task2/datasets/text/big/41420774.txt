Dooram Arike
{{Infobox film
 | name = Dooram Arike
 | image = 
 | caption = Promotional Poster
 | director = Jeassy
 | producer = O.M.John
 | writer = Sherif
 | dialogue =  Ambika K. P. Ummer
 | music = Ilayaraja
 | lyrics = O. N. V. Kurup
 | edited by =k narayanan 
 | cinematography = Vipin Das 
 | released =  
| studio = St.Joseph Cine Arts 
| distributor = St.Joseph Cine Arts 
 | runtime = 130 min. Malayalam
 | budget =  
 }}
 1980 Cinema Indian Malayalam Malayalam film, directed by Jesey and produced by OM John. The film stars Sukumari, Srividya, Sankaradi and Sukumaran in lead roles. The film had musical score by Ilayaraja.   

==Plot==
Dooram Arike is an emotional family film of love and sacrifice.

==Cast==
*M. G. Soman
*Sukumaran
*Srividya  Ambika
*K. P. Ummer
*Sankaradi 
*Sukumari
*Bahadoor

==Soundtrack==
The music was composed by Ilayaraja and lyrics was written by ONV Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Arike arike || K. J. Yesudas || ONV Kurup || 
|- 
| 2 || Maankidaave Nin nenchum || P Jayachandran || ONV Kurup || 
|- 
| 3 || Malarthoppithil Kilikkonchalaay || K. J. Yesudas, Chorus || ONV Kurup || 
|- 
| 4 || Paalaruvi paadivaru || S Janaki || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 
 


 