Those We Love
{{Infobox film
| name           = Those We Love
| image          =
| caption        =
| director       = Robert Florey
| producer       = John Golden
| writer         = 
| starring       = Mary Astor Kenneth MacKenna
| music          =
| cinematography = Arthur Edeson
| editing        = Rose Loewinger
| distributor    = Sono Art-World Wide Pictures
| released       = 11 September 1932
| runtime        = 72 mins.
| country        = United States
| language       = English 
| budget         =
}}
 Pre code American film directed by Robert Florey.  Kenneth MacKenna plays a young author who marries the woman (Mary Astor) who bought the first copy of his book. Their happy married life is later threatened by another female (Lilyan Tashman). 

The film was independently produced and distributed.

== Cast ==
* Mary Astor as May Ballard
* Kenneth MacKenna as Freddie Williston
* Lilyan Tashman as Valerie
* Hale Hamilton as Blake
* Tommy Conlon as Ricky
* Earle Foxe as Bert Parker
* Forrester Harvey as Jake
* Virginia Sale as Bertha Pat OMalley as Daley Harvey Clark as Mr. Hart
* Cecil Cunningham as Mrs. Henry Abbott
* Edwin Maxwell as Marshall

==References==
 

== External links ==

*  
* 

==Filmographic references==
The same year 1932, Ernst Lubitsch made a movie "One hour with you", with Maurice Chevalier, Jeanette MacDonald, Genevieve Tobin, Roland Young and Charles Ruggles in which the same theme of the happiness of a couple, in which the devoted and faithful husband is threatened by a bored wife, is developed in up and downs during the film, producing a dramatic twist of the characters acting with each other.

 

 
 
 
 
 
 


 