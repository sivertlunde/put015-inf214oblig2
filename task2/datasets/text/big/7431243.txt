Term of Trial
 
 
{{Infobox Film
| name           = Term of Trial
| image_size     = 
| image	=	Term of Trial FilmPoster.jpeg
| caption        = 
| director       = Peter Glenville James Woolf James Barlow (novel)  Peter Glenville
| narrator       = 
| starring       = Laurence Olivier Simone Signoret Sarah Miles
| music          = Jean-Michel Damase
| cinematography = Oswald Morris
| editing        = Jim Clark
| studio         = Romulus Films
| distributor    = Associated British Picture Corporation|Warner-Pathé  (UK)  Warner Bros. Pictures  (US) 
| released       = 16 August 1962 (World Premiere, London)
| runtime        = 130 min.
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British drama James Woolf James Barlow. The music score was by Jean-Michel Damase and the cinematography by Oswald Morris.

The film stars Laurence Olivier, Simone Signoret, Sarah Miles with Terence Stamp, Hugh Griffith, Roland Culver, Dudley Foster and Thora Hird. The film marked the screen debuts of Miles and Stamp.
 Warner Theatre in Londons West End. 

==Plot==
Graham Weir is an alcoholic schoolteacher whose criminal record for refusing to fight during the Second World War has prevented him from progressing further in his teaching career. Now, years later, he is in a school where teens who dont like to be there and is married to a very embittered wife. While in school, he meets Shirley Taylor, a new girl in the school who starts to have a crush on him. Graham does not realize it, but Shirleys infatuation will lead to serious trouble, including the threat of a false sexual molestation charge.

==Cast==
* Laurence Olivier as Graham Weir 
* Simone Signoret as Anna 
* Sarah Miles as Shirley Taylor 
* Terence Stamp as Mitchell 
* Hugh Griffith as OHara 
* Roland Culver as Trowman 
* Dudley Foster as Detective Sergeant Keirnan 
* Frank Pettingell as Ferguson 
* Thora Hird as Mrs. Taylor 
* Norman Bird as Mr. Taylor 
* Allan Cuthbertson as Sylvan-Jones 
* Barbara Ferris as Joan 
* Rosamund Greenwood as Constance Nicholas Hannen as Magistrate 
* Derren Nesbitt as Lodger

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 