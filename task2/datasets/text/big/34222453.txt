Petty Romance
{{Infobox film name           = Petty Romance  image          = PettyRomance2010Poster.jpg caption        = Film poster
| film name = {{Film name hangul         =   rr             = Jjaejjaehan romaenseu mr             = Tchaetchaehan romaensŭ}} director       = Kim Jung-hoon   producer       = Baek Kyung-sook  writer         = Kim Jung-hoon  starring  Choi Kang-hee   Lee Sun-kyun music          = Jung Jae-hyung cinematography = Choi Young-hwan  editing        = Nam Na-yeong distributor    =  released       =   runtime        = 118 minutes country        = South Korea language       = Korean gross          =   
}}
Petty Romance ( ) is a 2010 South Korean 18-rated romantic comedy film about the fiery relationship between an adult cartoonist and a former sex columnist. The film was a moderate hit, selling 2,048,296 tickets nationwide.  
 Choi Kang-hee SBS TV series My Sweet Seoul.   

==Plot==
Seoul, the present day. In need of money to redeem a treasured family portrait, struggling manhwa artist Jeong Bae takes part in a publishing companys competition for an adult manga with a prize of ₩130 million (US$100,000). Advised that his big weakness is his story-writing, Jeong Bae advertises for a professional writer and ends up hiring the self-important Han Da-rim, with whom he agrees to split the prize money 50-50 if they win. Unknown to Jeong Bae, Da-rim recently lost her job as a sex columnist at magazine Hot Girl — edited by her friend Ma Kyung-sun — where she compensated for her lack of experience with men by copying material from the Kama Sutra to the Kinsey Reports. For the manhwa competition, Da-rim comes up with the idea of a female assassin, Ma Mi-so, who keeps her male victims captive for erotic kicks; without telling Jeong Bae, she models the victim on her twin brother, womanising Han Jong-soo, who shares a flat with her and cannot wait for her to move out. She finally does, which leads Jeong Baes friend (and fellow competitor) Hae-ryong, who has secretly bugged his flat, to believe theyre having an affair. However, from her unrealistic sex scenes, Jeong Bae gradually comes to suspect that Da-rim has never actually "done it", and then realizes shes developed a crush on him.  

==Cast== Choi Kang-hee as Han Da-rim 
*Lee Sun-kyun as Jeong Bae 
*Ryu Hyun-kyung as Ma Kyung-sun 
*Song Yoo-ha as Han Jong-soo 
*Oh Jung-se as Hae-ryong
*Baek Do-bin as Min-ho
*Lee Won-jong as Lee Se-young
*Park Sung-il as Byung-doo
*Jung Min-sung as Team Leader Park
*Ryu Ji-hye as sexy woman
*Yoon Jin-soo as glamorous woman
*Yeo Moo-young as Min-hos father
*Park No-shik as unemployed man
*Hwang Bo-ra as Bol-maes older sister (cameo)
*Jo Eun-ji as female employee at award ceremony (cameo)

==References==
 

==External links==
*    
*  
*  

 
 
 
 
 