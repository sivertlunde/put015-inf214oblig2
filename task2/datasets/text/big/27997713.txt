Murder by Invitation
{{Infobox film
| name           = Murder by Invitation
| image_size     =
| image	=	Murder by Invitation FilmPoster.jpeg
| caption        =
| director       = Phil Rosen
| producer       = A.W. Hackel (producer)
| writer         = George Bricker (story and screenplay)
| narrator       =
| starring       =
| music          =
| cinematography = Marcel Le Picard
| editing        = Martin G. Cohn
| distributor    =
| studio         = Monogram Pictures
| released       =
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Murder by Invitation is a 1941 American film directed by Phil Rosen.

== Plot summary ==
The relatives of Cassandra "Cassie" Denham, an old unmarried woman living in New York who is good for three million dollars, try to make a judge declare her unable to take care of herself financially. The attempt fails, and in charge of handing in the petition is Cassies nephew Garson Denham, a lawyer. Instead he summons a newspaper columnist, Bob White, and his girlfriend, Nora OBrien, and tells them he has been invited to stay a week at Cassies estate up in the mountains.

The invite states that any relative not arriving on the specified time at midnight on Friday will be excluded from her will. Garson believes his aunt is plain crazy and plans to kill them all during the stay. Despite this, Garson and his wife, his brothers Tom and Larry and their mother Martha come to the estate on time.

When they arrive Cassie tells them that they are there so that she can learn which one of them should inherit the lion part of her fortune. She also tells them that she keeps all her money inside the house.

That night Garson is killed, stabbed to death in the library as he is about to fetch a book to read in order to fall asleep. Bib and Nora get news of his death and show up at the estate with a photographer to write about the killing. Sheriff Boggs is already questioning everyone at the house about their whereabouts during the night, and Bob, Nora and the photographer are all invited to stay at the estate by Cassie.

The next night, Larry is killed in the same way, and the sheriff continues his investigation. The dead bodies disappear and turn up again in Bobs closet, but they disappear before he is able to show them to the others.

Later, Eddie sees the bookshelf in the library open and just as Cassie comes out from one side, he walks into the other, into a secret passage. Cassie asks Eddie to guard a small box with her valuables. She offers him $10,000 to do so, and says she suspects her neighbor, Trowbridge Montrose, of being involved in the killings. The neighbor is supposedly in love with Cassie.

Tom is the next in line to be stabbed, and it happens while he is talking to Mary in the garden. No one can be suspected since they all have alibis. Cassie is determined to catch the killer and tells her plan to Bob in confidence. She will see to it that everyone is outside and then set the house on fire to see who tries to run in and save the money from burning up.

When the house is in flames Mary the maid finally claims she should have the money for putting up with Cassie all these years, expecting to inherit something. She also discloses her accomplice, Michael, the chaffeur, whom she has been married to for over a year. Then she flees the scene into the house. Michael is arrested shortly afterwards, and Trowbridge comes to propose to Cassie. Eddie presents the box to Cassie and gets his money, but it turns out to be old Confederate money, worthless nowadays. It turns out all of Cassies money is the same, but Trowbridge has enough for them both to live on. 

== Cast ==
*Wallace Ford as Bob White
*Marian Marsh as Nora OBrien
*Sarah Padden as Aunt Cassandra ("Cassie") Hildegarde Denham Gavin Gordon as Garson Denham
*George Guhl as Sheriff William Boggs
*Wallis Clark as Judge Moore
*Minerva Urecal as Maxine Denham
*J. Arthur Young as Trowbridge Cadwallader Montrose
*Herb Vigran as Eddie
*Phillip Trent as Larry Denham Dave OBrien as Michael, the Chauffeur
*Hazel Keener as Mary Denham
*Isabel La Mal as Martha Denham
*Lee Shumway as Eric, the Gardener John James as Tom Denham
*Kay Deslys as Katie, the Cook

== Soundtrack ==
 

== References ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 