Everything I Have Is Yours (film)
{{Infobox film
| name           = Everything I Have Is Yours
| image          = 
| image_size     =
| caption        =
| director       = Robert Z. Leonard George Wells
| writer         = Ruth Brooks Flippen George Wells
| based on       = 
| narrator       =
| starring       = Gower Champion Marge Champion Dennis OKeefe David Rose
| cinematography = William V. Skall
| editing        = Adrienne Fazan
| distributor    = MGM
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $1,461,000  . 
| gross          = $1,946,000  
}}

Everything I Have Is Yours is a 1952 American Technicolor musical film.

==Plot summary==
 
The husband and wife dance team, Chuck and Pamela Hubbard (Gower Champion and Marge Champion), are a pair of happily married dancers. The Hubbards have dreamed for years of taking their act to Broadway, and after much hard work and perseverance, they finally get their shot at the big time, only to discover that Pamela is pregnant, and her doctor forbids her to dance.  

==Cast==
* Marge Champion as Pamela Hubbard
* Gower Champion as Chuck Hubbard
* Dennis OKeefe as Alec Tacksbury
* Monica Lewis as Sybil Meriden
* Dean Miller as Monty Dunstan
* Eduard Franz as Phil Meisner
* John Gallaudet as Ed Holly
* Diane Cassidy as Showgirl
* Elaine Stewart as Showgirl
* Jonathan Cott as Freddie Robert Burton as Doctor Charles
* Jean Fenwick as Mrs. Tirson
* Mimi Gibson as Pamela (age 3)
* William Kerwin as Larry Wilson Wood as Roy Tirson

==Reception==
According to MGM records the film earned $1,359,000 in the US and Canada and $587,000 elsewhere, resulting in a loss of $459,000. 

==References==
 

==External links==
*   at IMDB
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 