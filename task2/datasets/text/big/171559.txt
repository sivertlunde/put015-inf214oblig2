Affliction (film)
 
{{Infobox film
| name = Affliction
| image = Affliction.jpg
| caption = Theatrical film poster
| director = Paul Schrader
| producer = Linda Reisman
| based on =  
| screenplay = Paul Schrader
| starring =Nick Nolte Sissy Spacek James Coburn Willem Dafoe
| music = Michael Brook
| cinematography = Paul Sarossy Jay Rabinowitz
| studio = Kingsgate Films Largo Entertainment
| distributor = Lions Gate Films
| released =  
| runtime = 114 minutes
| country = United States
| language = English
| budget = $6 million 
| gross  = $6,330,054   
}} American drama film produced in 1997, written and directed by Paul Schrader from the novel by Russell Banks. It stars Nick Nolte, Sissy Spacek, James Coburn and Willem Dafoe.

Affliction tells the story of Wade Whitehouse, a small-town policeman in New Hampshire. Detached from the people around him, including a dominating father and a divorced wife, he becomes obsessed with the solving of a fatal hunting accident, leading to a series of tragic events.

==Plot==
The film begins with a voice-over narration by Rolfe Whitehouse, announcing the story of his brother Wades "strange criminal behavior" and subsequent disappearance.

Wade Whitehouse is a small-town policeman in New Hampshire. On Halloween night, Wade meets his daughter Jill from his divorced marriage, but he is late and the evening is overshadowed by disharmony. Jill eventually calls her mother to come and pick her up. When his ex-wife finally arrives, Wade shoves her lover against their car and watches them drive away with Jill. Wade vows to get a lawyer to help gain custody of his daughter.

The next day, Wade rushes to the scene of a crime. A hunting guide named Jack claims that the man with whom he was hunting accidentally shot and killed himself. The police believe Jack, but Wade grows suspicious, believing that the mans death was no accident. When he is informed that the victim was scheduled to testify in a lawsuit, his suspicion slowly turns into conviction.

A while later, Wade and his girlfriend Margie Fogg arrive at the house of Wades father, Glen Whitehouse, whose abusive treatment of Wade and Rolfe as children is seen in flashbacks throughout the film. Wade finds his mother lying dead in her bed from hypothermia. Glen Whitehouse reacts to her death with little surprise. At the funeral wake, the father gets drunk and loudly exclaims, "Not one of you is worth one hair on that womans head!", resulting in a confrontation between Wade and him.

Rolfe, who has come home for the funeral, suggests at first that Wades murder theory could be correct, but later renounces himself of this presumption. Nonetheless, Wade becomes obsessed with his conviction. When Wade learns that town Selectman Gordon Lariviere is buying up property all over town with the help from a wealthy land developer, he makes the solving of these incidents his personal mission. Suffering from a painful toothache and becoming increasingly socially detached, he behaves more and more unpredictably. He follows Jack, convinced that Jack is running away from something and is involved in a conspiracy. After a car chase, a nervous Jack finally pulls over, threatens Wade with a rifle, shoots out his tires, and drives off.

Finally, Wade is fired from his police job both for his constant harassment of Jack and his trashing of Larivieres office. He collects Jill from her mothers house, where his ex-wife furiously castigates his plans to sue for full custody. At the local restaurant, he attacks the bartender in front of his daughter after he jokingly insults Wade. Then Wade takes Jill home to find Margie leaving him. Wade grabs Margie and begs her to stay with him, but Jill rushes up and tries to push Wade away. In response, Wade pushes Jill, causing her nose to bleed. She and Margie drive off. Wade is then approached by his father Glen, who congratulates him for finally acting as a "real man". The latent aggression between the men culminates in a fight in which Wade accidentally kills his father. He burns the corpse in the barn, sits down at the kitchen table and starts drinking, while the barn can be seen burning down through a window.

Rolfes narration reveals that Wade eventually murdered Jack and left town (possibly to Canada, where Jacks truck was found three days later), never to return. Rolfe relates that the town later became part of a huge ski resort partly organized by Gordon Lariviere. He concludes that someday a vagrant resembling Wade might be found frozen to death, and that will be the end of the story.

==Cast==
*  Nick Nolte as Wade Whitehouse
*  James Coburn as Glen Whitehouse
*  Brigid Tierney as Jill Whitehouse
*  Holmes Osborne as Gordon LaRiviere
*  Jim True-Frost as Jack Hewitt
*  Tim Post as Chick Ward
*  Christopher Heyerdahl as Frankie Lacoy
*  Marian Seldes as Alma Pittman
*  Janine Theriault as Hettie Rogers
*  Mary Beth Hurt as Lillian Whitehouse Horner Paul Stewart as Mr. Horner
*  Sissy Spacek as Margie Fogg
*  Willem Dafoe as Rolfe Whitehouse
*  Wayne Robson as Nick Wickham Sean McCann as Evan Twombley
*  Sheena Larkin as Lugene Brooks
*  Penny Mancuso as Woman Driver

== Background ==
Affliction was filmed in  . 

==Critical reception==
The film received mostly positive reviews with an 87% rating on Rotten Tomatoes.
 The Sweet Hereafter, a more meditative and elegant but less immediate, volcanic film, Affliction finds the deeper meaning in an all too believable tragedy." 

In a negative review in the Time Out Film Guide, Geoff Andrew called the film a "sensitive but rather dull adaption of Russell Banks novel   the narratives too unfocused and low-key really to engage the heart or mind." 

==Awards== Valladolid International Film Festival Award for Best Actor.

The film was nominated in six categories at the Independent Spirit Awards.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 