Stuart Little (film)
 
{{Infobox film
|name=Stuart Little
|image=Stuart Little.jpg
|caption=Theatrical release poster
|director=Rob Minkoff
|producer=Douglas Wick Greg Brooker based on=  website  
|starring=  
|music=Alan Silvestri
|cinematography=Guillermo Navarro
|editing=Tom Finan
|studio=Franklin/Waterman Productions Global Medien GK
|distributor=Columbia Pictures
|released= 
|runtime=84 minutes {{cite web|url=http://www.boxofficemojo.com/movies/?id=stuartlittle.htm 
|title=Stuart Little (1999) 
|publisher=Box Office Mojo |date=2000-04-16 |accessdate=2012-10-01}} 
|country=United States
|language=English
|budget=$133 million 
|gross=$300.1 million 
}}
 novel of Greg Brooker. The plot bears little resemblance to that of the book, as only some of the characters and one or two minor plot elements are the same. The movies sequel more closely resembles the original novel.

Michael J. Fox is the voice of Stuart Little. Geena Davis and Hugh Laurie star as Eleanor and Frederick Little, with Jonathan Lipnicki as Stuarts big brother George Little and Nathan Lane as the voice of the family cat Snowbell.

The film was released on December 17, 1999 by Columbia Pictures. 

It received an   in 2003, and another sequel in 2006, the animated  .

This film was Estelle Gettys last film before her retirement in 2000 and her death in 2008.

==Plot==
Eleanor Little (Geena Davis) and Frederick Little (Hugh Laurie) go to the orphanage to adopt a younger brother for their son George (Jonathan Lipnicki). At the orphanage, they meet and fall in love with an anthropomorphic mouse named Stuart (voiced by Michael J. Fox). Despite misgivings from Mrs. Keeper (Julia Sweeney), they adopt Stuart and take him home. However, Stuart is greeted coldly by George, who refuses to acknowledge the mouse as his brother, and the family cat Snowbell (Nathan Lane), who is disgusted at having a mouse for a "master".

Stuarts new life at the house gets off to a bad start when George unknowingly traps him in a pile of laundry and his mother puts the laundry in the washing machine. Stuart quickly feels like an outsider in the large Little family, especially when George declares Stuart is not his brother, but simply a mouse. When Stuart tells his parents that he is lonely, they ask Mrs. Keeper to do some background research on Stuarts biological family. After accidentally stumbling across Georges playroom in the basement one day, Stuart and George get to know each other and plan to finish Georges remote-controlled boat, the Wasp, for an upcoming boat race in Central Park. At the same time, one of Snowbells alley cat friends, Monty (Steve Zahn), visits unexpectedly and discovers Stuart. Determined not to have his reputation destroyed, Snowbell meets with Montys leader, Smokey (Chazz Palminteri), a mafia don-like alley cat, and plans to have Stuart removed from the household without harming him.

Stuart and George finish the Wasp in time, but when the control is broken on the day of the race, Stuart pilots the Wasp himself. He ends up entangled with a larger boat belonging to Georges rival, Anton (Miles Marsico), but Stuart snaps the wires of Antons boat and manages to win the race. After this, George warms up to Stuart and calls him "his brother". During the family celebration, however, the Littles are visited by a mouse couple, Reginald and Camille Stout (Bruno Kirby and Jennifer Tilly), who claim to be Stuarts parents and that they gave him up to the orphanage years ago due to poverty. Reluctantly, Stuart leaves with the Stouts. A few days later, however, Mrs. Keeper comes to visit and tells the Littles that Stuarts parents actually died many years ago in a supermarket accident. Realizing they have been tricked, the Littles call the police, who start a search operation.

Meanwhile, Snowbell meets with Smokey and the alley cats, who reveal they had forced the Stouts to pose as Stuarts parents in order to remove him from the household. Fearing retribution should the Littles discover Snowbells deception, Smokey orders the Stouts to hand Stuart over to them. However, the Stouts, having grown fond of Stuart, tell him the truth and instruct him to flee. Smokey subsequently orders a hit on Stuart. He and the alley cats corner him in Central Park, and after a brief chase, Stuart evades them in a storm drain. He returns home, but finds the Littles absent, having gone out to put up posters of him. The only one present is Snowbell, who lies that the Littles have been enjoying themselves since Stuarts departure and uses Stuarts removed face from the family photograph as proof (which they had actually used for the posters). Heartbroken, Stuart leaves again.

When the Littles return distraught, Snowbell starts to recognize his selfishness and feels guilty for what he has done. The cats pinpoint Stuarts location in Central Park and bring Snowbell for the hunt. However, he finds Stuart sitting in a bird nest in a tree and decides to save him from the cats, admitting that he lied and the Littles do love him. Snowbell then tries to reason with Smokey to let Stuart go, but Smokey refuses and demands his gang to kill both Stuart and Snowbell. Stuart lures the cats away, but is cornered on a branch. Snowbell rescues him by breaking the branch the cats are on, sending them falling down into the Hantan River below. An angry Smokey prepares to kill Snowbell, but Stuart hits Smokey off the tree with another branch into the river. Humiliated, Smokey runs off, only to be chased away by a pack of dogs. Stuart rides Snowbell all the way home and they share a happy reunion with the Little family.

==Cast==

===Live-action===
*Geena Davis as Eleanor Little 
*Hugh Laurie as Frederick Little 
*Jonathan Lipnicki as George Little 
*Jeffrey Jones as Uncle Crenshaw Little
*Connie Ray as Aunt Tina Little
*Allyce Beasley as Aunt Beatrice Little
*Brian Doyle-Murray as Cousin Edgar Little
*Estelle Getty as Grandma Estelle Little
*Harold Gould as Grandpa Spencer Little
*Patrick Thomas OBrien as Uncle Stretch Little
*Stan Freberg as Race Announcer
*Jon Polito as Detective Sherman
*Jim Doughan as Detective Phil Allen
*Julia Sweeney as Mrs. Keeper, Head of the Orphanage
*Miles Marsico as Anton
*Taylor Negron as Salesman in mall

===Voices===
*Michael J. Fox as Stuart Little 
*Chazz Palminteri as Smokey the Chief
*Nathan Lane as Snowbell
*Steve Zahn as Monty the Mouth 
*David Alan Grier as Red 
*Bruno Kirby as Reginald Stout 
*Jennifer Tilly as Camille Stout
*Jim Doughan as Lucky

==Set design== lost painting. A set designer for the film had purchased the painting at an antiques store in Pasadena, California for a small amount of money for use in the film, unaware of its provenance. In 2009, art historian Gergely Barki, while watching Stuart Little on television with his daughter, noticed the painting, and after contacting the studios was able to track down its whereabouts. 

==Reception==

===Box office===
On its opening weekend, Stuart Little grossed $15 million, placing it at #1. It dropped to #2 over its second weekend, but went back to #1 on its third weekend with $16 million. According to Box Office Mojo, its final gross in the United States and Canada was $140 million and it grossed $160.1 million at the international box office, for an estimated total of $300 million worldwide.  It covered its budget and was a box office success.

===Critical reception===
Stuart Little received generally positive reviews from movie critics. According to  , the film has a score of 61 out of 100, indicating "generally favorable" reviews. 

==Release==
Stuart Little was released theatrically on December 17, 1999.

===Home media===
Stuart Little was released on VHS and DVD on April 18, 2000. It was later released on a Deluxe edition on May 21, 2002.

==Video game==
Stuart Little: The Journey Home is a video game for the Game Boy Color system based on the book.

==See also==
* List of Stuart Little characters

==References==
 

==External links==
 
* 
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 