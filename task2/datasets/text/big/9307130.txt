Bashu, the Little Stranger
{{Infobox film
| name = Bashu, the Little Stranger
| image=BashutheLittleStranger.jpg 
| director =Bahram Beizai
| cinematography = Firooz Malekzadeh
| writer =Bahram Beizai
| starring = Susan Taslimi
| county = Iran
| released =1989 Persian
| runtime =120 minutes
| distributor =
}} 1986 Iranian drama film directed by Bahram Beizai. The film was produced in 1986, and was released in 1989. This multi-ethnic film was the first Iranian film to make use of the northern dialect of Persian, Gilaki, in a serious context rather than comic relief. (Susan Taslimi playing the main character is Gilaki herself).
 Best Iranian Film of all time" in November 1999 by a Persian movie magazine "Picture world" poll of 150 Iranian critics and professionals. 

==Plot==
The film is about a young boy from Khuzestan province, in the south of Iran, during the Iran–Iraq War. His parents are killed in a bombing raid on his home village and he escapes on a cargo truck to the north. Eventually he gets off and finds refuge on the farm of a Gilaki woman, Nai, who has two young children of her own. Initially, Nai tries to shoo Bashu away, but later takes pity on him and leaves food out for him. Although Nai is initially ambivalent toward Bashu, and he is initially suspicious of her, they come to trust one another, and Bashu becomes a member of the family, even calling Nai "mom". Being that Bashu speaks Arabic, while Nai and her children speak Gilaki language|Gilaki, they have trouble communicating with each other, although Bashu is able to speak and read Persian (for example in the scene where he picks up the school text book, reading a passage from it in an attempt to appease the children fighting). In a gesture of reciprocation and perhaps love, Bashu cares for Nai when she falls ill, as she had done for him, crying for her and beating a drum in prayer.

Throughout the film, Nai maintains correspondence with her husband, a war veteran looking for employment, who has been gone for quite some time. She tells him about Bashu, and implores him to return home in time to help with the harvest. Bashu becomes Nais helper on the farm, and even accompanies her to the bazaar to sell her goods. Throughout the film, Bashu sees visions of his dead family members, which cause him to wander off.  Ultimately, however, he and Nai are always reunited.

The other adults in the village harangue Nai about taking Bashu in, often deriding his dark skin and different language, and making comments about washing the dark off of his skin. In addition to the village adults, the school age children taunt and beat Bashu, although the children prove ultimately to be more willing to accept Bashu than the adults. In one scene in which he is being taunted, Bashu picks up a school book and reads aloud a passage stating, "We are all the children of Iran." Before this point, the children had assumed Bashu to be either mute or stupid.

In the end, Nais husband returns home with no money and missing an arm, having been forced to take on dangerous work that is never identified. He and Nai argue over her having kept Bashu against his wishes. Bashu comes to her defense, challenging the strange man to identify himself. Nais husband tells Bashu that he is his father, and upon this realization, they embrace as though they were always a part of the same family. The film ends with the entire family, including children, running into the farm field, making loud noises together to scare away a troublesome boar.

==See also==
*List of Iranian films
*List of films considered the best 

==References==
 

==External links==
* 

 
 
 
 
 
 
 