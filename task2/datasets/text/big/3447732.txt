Dennis the Menace (film)
 Dennis the Menace}}
{{Infobox film name          = Dennis the Menace image         = Dennis the menace.jpg caption       = One-sheet poster director      = Nick Castle writer  John Hughes based on      =   starring      =   producer      =   music         = Jerry Goldsmith
|cinematography= Thomas E. Ackerman editing       = Alan Heim studio  Hughes Entertainment Hank Ketcham Enterprises distributor   = Warner Bros. Family Entertainment released      =   runtime       = 95 minutes budget        = $35 million country       = United States language      = English gross         = $117.2 million

}} television in 1987.
 John Hughes, Family Entertainment banner. It concerns the misadventures of a mischievous child (Mason Gamble) with a cowlick and a grin who wreaks havoc on his next door neighbor, Mr. Wilson (Walter Matthau), usually hangs out with his friends, Joey (Kellen Hathaway) and Margaret (Amy Sakasitz), and is followed everywhere by his dog, Ruff.

A direct-to-video sequel called Dennis the Menace Strikes Again was later released in 1998 without the cast members from this film. The film was also followed by a Saturday morning cartoon series called All-New Dennis the Menace.

==Plot==
Dennis Mitchell is a five-year-old boy who lives with his parents Henry and Alice, and is the bane of next door neighbor George Wilsons existence. Because of his trouble-making but unintentional behavior, his parents often struggle to find suitable babysitters to deal with him. On one night, they manage to get one named Polly and her boyfriend Mickey to babysit him, but repeated doorbell pranks from him push the two too far (not knowing he is behind this), and they end up pulling a prank on George when he rings the doorbell to scold Dennis after finding paint and wood in his food in an earlier incident. While all of this and the rest of the events in town go on, a burglar named Switchblade Sam (said name not mentioned in the film, only in the end credits) arrives in town and begins robbing houses, as well as striking fear into the children that he meets.

Dennis parents are both called away on business trips at the same time, and when everyone they know refuses to look after him, they turn to George and his wife Martha (who loves Dennis and sees him as a surrogate grandson) to look after him. George is further irritated by him spilling bath water on the bathroom floor, swapping chemicals, and bringing his pet dog, Ruff, into the house for a while. All of this is happening around the time the Summer Floraganza, a long-awaited event, is scheduled to happen. As a longtime member of the local garden club, George is chosen to host it. He is excited to have this honor, as he has been growing and nurturing a rare plant for forty years. After growing for the said length of time, its flower finally blooms, only to die several seconds later.
 abducts him, intending to use the child as a hostage.

Dennis parents return home and learn of his departure, and they, the authorities, his friends (Joey, Margaret, Gunther and all the neighborhood kids), and George (who feels intense guilt and remorse after remembering all the things he said to him and having now discovered his house was burglarized and that Dennis actually had good intentions when he tried to tell him) search all night for him. Around the same time, Dennis unintentionally but effectively defeats Switchblade Sam by tying him up and handcuffing him, losing the key, and repeatedly setting him on fire, amongst other things. He returns to Georges house the next morning with Switchblade Sam in his wagon, having also recovered Georges gold coins, and Sam is taken into police custody by a slightly amused sheriff who had previously advised him to leave town. Dennis and George make up, and the Mitchells and Wilsons become friends on better terms. That night, George explains that hes learned some things about kids: kids are kids, and that one has to play by their rules, roll with the punches, and expect the unexpected. Just then, Dennis accidentally throws a lit marshmallow that hits George right in the forehead.

The films end credits are accompanied with Dennis inadvertently humiliating his mothers egotistical coworker, Andrea, while she is using a photocopier. Dennis impishly whacks the "PRINT" button and runs away, with other workers looking on. Andrea loses her balance and her head gets pinned face-down on the scanner-bed, and the machine relentlessly flashes its blinding light in her eyes as it repeatedly "takes her picture" and spews out page after page of black-and-white "photos" showing her various agonized facial expressions as she writhes about on the scanner bed.

==Cast==
* Mason Gamble as Dennis Mitchell
* Walter Matthau as George Wilson
* Joan Plowright as Martha Wilson
* Christopher Lloyd as Switchblade Sam Robert Stanton as Henry Mitchell
* Lea Thompson as Alice Mitchell
* Amy Sakasitz as Margaret Wade
* Kellen Hathaway as Joey
* Paul Winfield as Chief of Police
* Ben Stein as Boss (only as a cameo shot at a meeting)
* Natasha Lyonne as Polly
* Devin Ratray as Mickey
* Hank Johnston as Gunther Beckman
* Melinda Mullins as Andrea
* Billie Bird as Edith Butterwell
* Bill Erwin as Edward Little
* Arnold Stang as The Photographer

==Production notes== John Hughes and starring Devin Ratray).
 Dennis the Menace", which also debuted in 1951.

===Music=== John Hughes first and only choice to write the music score for this film.

Additionally, three old-time pop hits were featured in the film: "Dont Hang Up (song)|Dont Hang Up" by The Orlons, "Whatcha Know Joe" by Jo Stafford (from the 1963 album Getting Sentimental over Tommy Dorsey), and "A String of Pearls" by Glenn Miller.

==Video game== video game Super Nintendo boiler room among others.

==Reception==
Dennis the Menace was a success at the box office. Against a $35 million budget, the film grossed $51.3 million domestically and a further $66 million overseas to a total of $117.3 million worldwide,   despite generally negative reviews from film critics.   On Rotten Tomatoes, it has a "rotten" rating of 23%, with the general consensus saying, "Walter Matthau does a nice job as Mr. Wilson, but Dennis the Menace follows the Home Alone formula far too closely."

 .

==References==
 

==External links==
 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 