For the Love of Rusty
{{Infobox film
| name          = For the Love of Rusty
| image         =
| caption       =
| director      = John Sturges Al Martin
| starring      = Ted Donaldson Tom Powers Ann Doran Aubrey Mather Sid Tomack
| producer      = John Haggott James Sweeney
| music         = Marlin Skiles
| cinematography= Vincent Farrar
| distributor   = Columbia Pictures
| released      =  
| runtime       = 68 min.
| language      = English
| budget        = $200,000 Glenn Lovell, Escape Artist: The Life and Films of John Sturges, University of Wisconsin Press, 2008 p42 
}}

For the Love of Rusty is a 1947 drama film directed by John Sturges. It was the third of the Rusty (film series)|"Rusty" film series involving the adventures of German shepherd Rusty and his human companions - young Danny Mitchell (Ted Donaldson) and his pals. This film details Dannys friendship with an eccentric and itinerant "veterinarian" Dr. Fay (Aubrey Mather), and Dannys attempts to form a closer relationship with his father (Tom Powers). In this installment, Rusty was played for the first time by Flame, who would portray Rusty in four of the eight Rusty films.

==Plot Summary==

Busy attorney Hugh Mitchell wants to become closer to his son, Danny, whom he knows little. He starts arranging a luncheon, but soon finds out that Danny prefers going to the carnival. Still he attends the luncheon, and brings along his dog Rusty, a German Shepherd. All the other boys attending with their fathers are quite amused when Rusty starts fighting with another dog, and the luncheon is abruptly interrupted.

The calamity that ensues enrages Hugh and disintegrates the chances of father and son coming closer. Instead Danny becomes friends with an eccentric traveling veterinarian, Dr. Francis Xavier Fay, who arrives to town. Hugh doesnt look kindly upon the friendship between his son and the doctor.

In an attempt to get their son back, Hugh and his wife Ethel invites the doctor to dinner one night, hoping that the doctor will seem out of place. Bit the doctor is very comfortable in the civilized and sophisticated setting in the attorney home.

Hugh decides to take Danny to the carnival to make him happy. Danny brings Rusty with him. When a man kicks at the dog, it attacks him and Hugh is quite upset with the dogs behavior, forcing it to wear a muzzle in the future.

In the night, Danny run away with his dog, taking refuge in the doctors camp in the woods. Ethel suggest they leave the boy alone for a while, and Danny gets to live in a tree house at the doctors, with his dog.

Hugh pays the doctor a visit to talk about his son, and gets the advice to try and understand and be friends with his son. Later in the night, when the doctor has fallen asleep with his gas stove on, Rusty smells the gas and tries to warn them about the danger. Rusty crawls under the trailer and is injured when the trailer collapses to the ground. Danny wakes up when the dog cries out, and wakes up the doctor, who is unconscious from the gas.

Danny goes home to his parents and the doctor treats Rusty at the camp. Hugh and his son are finally reconciled and go back to the doctors camp together. Rusty is in bandages and able to come home with Danny.

Already the next day, Ethel comes to the camp looking for the dog, which has escaped and run around in the neighborhood. The doctor tells Ethel that this is perfectly normal, and decides it is time for him to leave and go to the next town. 

==Cast==
* Ted Donaldson as Danny Mitchell 
* Tom Powers as Hugh Mitchell 
* Ann Doran as Ethel Mitchell 
* Aubrey Mather as Dr. Francis Xavier Fay 
* Sid Tomack as Moe Hatch  George Meader as J. Cecil Rinehardt  Mickey McGuire as Gerald Hebble 
* Ralph Dunn as Policeman (uncredited) 
* Dick Elliott as Bill Worden (uncredited) 
* Eddie Fetherston as The side-show barker (uncredited) 
* Harry Hayden as Mr. Hebble (uncredited) 
* Dwayne Hickman as Doc Levy Jr. (uncredited) 
* Olin Howland as Frank Foley (uncredited) 
* Teddy Infuhr as Tommy Worden (uncredited) 
* Georgie Nokes as Squeaky (uncredited) 
* Wally Rose as Bit Role (uncredited) 
* Fred F. Sears as Doc Levy (uncredited) 
* Almira Sessions as Sarah Johnson (uncredited)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 