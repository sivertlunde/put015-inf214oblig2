Ninagagi
{{Infobox film
| name = Ninagagi
| image =
| caption = 
| director = S. Mahendar
| writer = Ikbal Kuttipuram
| based on = Niram (1999)
| producer = Ramoji Rao Radhika  Tara   Vishal Hegde
| music = Gurukiran
| cinematography = Ramesh Babu
| editing = P. R. Soundar Raj
| studio  = Ushakiran Movies
| released = 3 May 2002
| runtime = 147 minutes Kannada
| country = India
}}
 Radhika in Malayalam blockbuster film Niram (1999), and later remade in Telugu as Nuvve Kavali (2001). The film was produced by Ramoji Rao and the music was composed by Gurukiran.

The film upon release met with very good positive response at the box-office. It was one of the highest grossing films of the year 2002. 

== Cast ==
* Vijay Raghavendra as Tarun Radhika as Madhu
* Vishal Hegde as Prakash
* Shilpa Tara
* Ramakrishna
* Avinash
* Chitra Shenoy
* Dattathreya
* Kashi

== Soundtrack ==
All the songs are composed and scored by Gurukiran.  Almost all the songs are recomposed with the same tunes as in the original film.

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyrics
|-
| 1 || "Yellelli Naa Nodali" || Rajesh Krishnan || K. Kalyan
|-
| 2 || "Yemmo Yemmo" || Anupama (singer)|Anupama, Gurukiran || K. Kalyan
|-
| 3 || "Hani Hani Seri" || Madhu Balakrishnan, K. S. Chithra || V. Manohar
|-
| 4 || "Tin Tin Tin" || Anupama || V. Manohar
|-
| 5 || "Shukriya" || Hemanth, Shamitha Malnad || K. Kalyan
|-
| 6 || "Kanninalli Kannanittu" || K. S. Chithra || K. Kalyan
|}

===Character map of Niram and its remakes===

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 


 

 