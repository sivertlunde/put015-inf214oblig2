Nobody Knows Anybody
{{Infobox film 
| image    = Nadie conoce a nadie.jpg
| producer = Gustavo Ferrada Thierry Forte Enrique López Lavigne Antonio P. Pérez Arancha Solís
| director = Mateo Gil
| screenplay = Mateo Gil
| based on = Nadie conoce a nadie by Juan Bonilla
| music    = Alejandro Amenábar
| cinematography = Javier Salmones
| editing = Nacho Ruiz Capillas Eduardo Noriega
| country = Spain
| runtime = 108 minutes
}}

Nobody Knows Anybody (Spanish: Nadie conoce a nadie) is a 1999 Spanish film directed by Mateo Gil.

== Plot ==
Amid the spectacular festivities of Holy Week in Seville, an aspiring novelist struggles with his work and pays his bills by composing crossword puzzles. A cryptic recording left on his answering machine demands that he include a certain word in a future puzzle and he becomes drawn into a spiraling tangle of mystery, danger, and confusion. Soon hes forced into participating in a real-life version of a computer game on the narrow streets of Seville with extremely high stakes for the entire city.

== External links ==
*  

 

 
 
 


 
 