Mr. Putsami
{{Infobox film|
| name = Mr. Putsami
| image = 
| caption =
| director = V. Umakanth
| based on =  Suman   Srinath
| producer = H. C. Srinivas
| music = V. Manohar
| cinematography = P. K. H. Das
| editing = B. S. Kemparaj
| studio = Sri Puttamma Productions
| released =  
| runtime = 148 minutes
| language = Kannada
| country = India
| budgeBold textt =
}} Kannada action action drama Suman in the lead roles.  The films score and soundtrack is composed by V. Manohar.
 
== Cast ==
* Shivarajkumar 
* Laali Suman
* Avinash
* Srinath
* Lokesh
* Bank Janardhan
* Jyothi
* M. S. Umesh
* Sanket Kashi
* Mandeep Roy

== Soundtrack ==
The soundtrack of the film was composed by V. Manohar.

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Yaaramma Mano
| lyrics1 =V. Manohar
| length1 = 
| title2 = Jaya Hey Bhavani
| extra2 = L. N. Shastry
| lyrics2 = V. Manohar
| length2 = 
| title3 = Teen Teen Teen
| extra3 = Nanditha
| lyrics3 = V. Manohar
| length3 = 
| title4 = Baa Baa Baa Baare
| extra4 = Hemanth
| lyrics4 = V. Manohar
| length4 = 
| title5 = Kanmani Kanmani
| extra5 = L. N. Shastry
| lyrics5 = V. Manohar
| length5 = 
| title6 = Yaaravalu Yaaravalu
| extra6 = Hemanth, Archana Udupa
| lyrics6 = V. Manohar
| length6 = 
}}

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 


 

 