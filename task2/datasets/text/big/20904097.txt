Hira Aur Patthar
{{Infobox film
| name           = Hira Aur Patthar
| image          = Hira Aur Patthar.jpg
| image_size     = 
| caption        = 
| director       = Vijay Bhatt
| producer       = Arun Bhatt Kishore Vyas
| narrator       =
| story           =Arun Bhatt
| screenplay=Ram Kelkar
| writer=Dialogue:Satyadev Dubey  Moosa Kaleem
| starring       = Shashi Kapoor  Shabana Azmi  
| music          = Kalyanji Anandji
| cinematography = Pravin Bhatt 
| editing        = B. Prasad
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Bollywood film directed by Vijay Bhatt and starring Shashi Kapoor and Shabana Azmi.

==Cast==
*Shashi Kapoor...Shankar 
*Shabana Azmi...Gauri  Asit Sen...Chandas Father 
*Ashok Kumar...Dr. Anand    
*Bindu (actress)|Bindu...Roopa Bai 
*G. Asrani...Tota  
*Bharat Bhushan ...Tulsiram

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tak Tak Tunak Tin"
| Kishore Kumar
|-
| 2
| "Naam Tera Bhale"
| Lata Mangeshkar
|}
==External links==
*  

 

 
 
 
 
 
 