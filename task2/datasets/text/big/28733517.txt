A Town Like Alice (1956 film)
 
 

{{Infobox film
| name           = A Town Like Alice
| image          =
| caption        = Jack Lee
| producer       = Joseph Janni Richard Mason (screenplay)
| starring       = Virginia McKenna Peter Finch
| music          = Matyas Seiber
| cinematography = Geoffrey Unsworth
| editing        = Sidney Hayers
| studio         =
| distributor    =  The Rank Organisation
| released       =  1956
| runtime        = gross = 1,037,005 admissions (France) 
| country        = United Kingdom
| language       = English
}}
 novel by Nevil Shute and with Virginia McKenna and Peter Finch in the leading roles.  The film does not follow the whole novel, concluding at the end of Part Two of the book, and many plot details are truncated or omitted. It was partially filmed in Malaya and Australia.

==Plot summary== Malaya to well in a small village.

Jean goes to the village and arranges for the well to be dug. The women will not now have to walk so far each day to collect water, as they have always done. She recalls her life in the village for three years of the war.
 Japanese invaded and she was taken prisoner. As part of a group of women and children (the men having been sent away), she is the only one to speak Malay fluently, and so takes a leading role in the group.

But the Japanese refuse to take any responsibility for the group, marching them from one village to another. Many of them, unused to physical labour, die. Jean is only able to survive because she understands local ways and is prepared to go native.

The group meets a young Australian soldier, Sergeant Joe Harman, also a prisoner, who is driving a truck for the Japanese. He and Jean strike up a friendship and he tells her about the town of Alice Springs, where he grew up. Appalled at the womens treatment by the Japanese, he steals food and medicines to help them. Jean does not correct his impression that she is married.

When the thefts are discovered and investigated, Harman takes the blame to save Jean and the rest of the group. He is crucified on a tree and left to die by the Japanese soldiers. The distraught women are marched away, believing that Joe is dead.

To further humiliate them, the Japanese assign only one guard to the group, an elderly sergeant. They become friendly with him, although they can barely communicate. They even help to carry his pack and rifle when he is ill. When he dies of exhaustion, Jean asks the elders of a Malayan village if they may stay and work in the paddy fields, asking only for food and a place to sleep. The elders agree and they live and work there for three years, until the war ends.

The film returns to the present, and Jean discovers from the well-diggers that Joe Harman survived his punishment and returned to Australia.

She decides to travel on to Australia to find him. On her travels, she visits the town of Alice Springs, where Joe lived before the war, and is much impressed with the quality of life there. She then travels to the (fictional) primitive town of Willstown in the Queensland outback, where Joe has become the manager of a cattle station.

Meanwhile, Joe has learnt that Jean survived the war and that she was not married. He travels to London to find her, using money won in a lottery. It is some time before they are reunited in Alice Springs and they fall in love immediately.

==Production== Malaya and Australia, it was mostly shot at Pinewood Studios in London. 

At one stage it was announced that Olivia de Havilland would play the lead.  Anna Kashfi screen tested for a small role and was given it, but had to turn it down to do another movie. 

==Release==
The film was withdrawn from the 1956 Cannes Film Festival because of fears it would offend the Japanese.  "The festivals are just a joke – a film-selling racket which offers the chance for vulgar display and reckless extravagance", said Peter Finch. "They serve no cultural purpose and the awards dont mean a thing." 

The films Australian premiere was held at Alice Springs.  

It was the third most popular film at the British box office in 1956. 

==Cast==
* Virginia McKenna – Jean Paget
* Peter Finch – Joe Harman
* Kenji Takaki – Japanese sergeant
* Tran Van Khe – Captain Sugaya
* Jean Anderson – Miss Horsefall
* Marie Lohr – Mrs Frost
* Maureen Swanson – Ellen
* Renée Houston – Ebbey
* Nora Nicholson – Mrs. Frith
* Eileen Moore – Mrs Holland
* John Fabian –  Mr. Holland
* Vincent Ball – Ben
* Tim Turner – British Sergeant
* Geoffrey Keen – Noel Strachan

==See also==
*Tenko (TV series) 1981–84 BBC and ABC TV series based on the female prisoners of war in Singapore during World War II
*Paradise Road (1997 film) – film based on the female prisoners of war in Sumatra in World War II

==References==
 

== External links ==
*  
*  at Oz Movies
 

 
 
 
 
 
 
 
 
 
 
 
 
 