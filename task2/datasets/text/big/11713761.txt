Sangdil Sanam
{{Infobox film
| name           = Sangdil Sanam
| image          = Sangdil_Sanam.jpg
| image_size     = 
| caption        = 
| director       = Shomu Mukherjee
| producer       = Shomu Mukherjee Anwar Khan- dialogue Anwar Khan- dialogue
| screenplay     = Tanveer Khan
| narrator       = 
| starring       = Salman Khan Manisha Koirala Milind
| cinematography = Aloke Dasgupta
| editing        = Vishwanath Rao
| distributor    = 
| released       = 2 February 1994
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Indian Bollywood film directed by Shomu Mukherjee. The film stars Salman Khan and Manisha Koirala in lead roles. It released on 2 February 1994.

==Synopsis==
Kailash Nath is the manager of a bank who lives a comfortable lifestyle with his wife, Savitri, and son, Kishan. He is friendly with the Bank Watchman, Shankar Dayal Khurana, so much so that he arranges the engagement of Kishan with Shankars daughter, Sanam. Shortly after the engagement, the bank is robbed. Kailash becomes a suspect, is arrested, charged, and sentenced to 12 years in prison. After he completes his sentence he returns home to find that Savitri and Kishan are not traceable. He also finds out that he was framed for the bank robbery by none other than Shankar, who is now the Mayor of this town. In the meantime, Kishan, now grown up, goes to take Sanam home with him as his bride. When she refuses to do so, he forcibly takes her. Kishan is not aware that Sanam is to be married to Pradeep, the only son of millionaire Lalla. Sanam intends to publicly humiliate Kishan and Savitri prior to her marriage with Pradeep due to their poverty.

==Cast==
*Salman Khan ... Kishan 
*Manisha Koirala ... Sanam 
*Kiran Kumar ...  Shankar Dayal Khurana 
*Reema Lagoo ...  Mrs. Savitri Nath 
*Raza Murad ...  Daman Chamda Dada 
*Alok Nath ...  Kailash Nath 
*Ashok Saraf ... Bhalchander
*Anand Balraj ... Prince
*Raza Murad ... Chamada Dada Pradeep Rawat ... Pradeep

==Soundtrack==
The music is tuned by Anand-Milind and the lyrics are written by Sameer (lyricist) | Sameer. The song One Two Three was used in the 2014 Bollywood film Haider (film)|Haider.
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s)!! Length
|-
| 1
| Aankhon Mein Bandh 
| Amit Kumar, Sadhana Sargam
| 08:38
|- 
| 2
| Dharti Bane Dawaat 
| S.P. Balasubramaniam, Kavita Krishnamurthy
| 08:26
|- 
| 3
| Aankhon Mein Bandh (Sad) 
| Amit Kumar, Sadhana Sargam
| 04:02
|- 
| 4
| Le Le Mera Naam Le  Sunita Rao
| 06:01
|- 
| 5
| Sanam Sangdil Sanam
| Amit Kumar, Kavita Krishnamurthy
| 06:58
|- 
| 6
| One Two Three 
| Amit Kumar
| 05:15
|}

==External links==
* 

 
 
 
 

 