Walking Tall: The Payback
{{Infobox Film name            = Walking Tall: The Payback
| image          =Walking Tall- The Payback.jpg
| caption        =
| director       = Tripp Reed
| producer       = Alison Semenza	 
| writer         = Joe Halpin Brian Strasmann	
| starring       = Kevin Sorbo Haley Ramm
| music          = David Wurst Eric Wurst
| cinematography = Jas Shelton
| editing        = Vanick Moradian
| studio         = Andrew Stevens Entertainment Walking Tall Productions	 	 
| distributor    = Sony Pictures Home Entertainment
| released       =    	
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $880,000
}}
Walking Tall: The Payback is a 2007 Action film|action-thriller film directed by Tripp Reed and stars Kevin Sorbo and Haley Ramm.

==Prelude== Walking Tall Dwayne "The Rock" Johnson), the main characters name is Nick Prescott, unlike the originals Buford Pusser or the remakes Chris Vaughn.

==Plot==
This time, the plot focuses on a local hero who takes justice into his own hands. Ex military Nick (Kevin Sorbo) returns to his hometown. His father, the sheriff, tries to stand up against a brutal gang of ruthless criminals who intimidate and blackmail the people to sell them their business. He then is killed by the gangs leader Harvey Morris.

With the help of a FBI agent and a few old friends, Nick himself becomes sheriff, then vows to do everything in his power to destroy the gang and their ruthless leader, and win back his city. Eventually he shoots Morris and his gang.

==Cast==
*Kevin Sorbo as Nick Prescott
*Haley Ramm as Samantha Jensen
*Richard Dillard as Charlie Prescott
*Gail Cronauer as Emma Prescott
*Ntokozo Mntwini as Hap Worrell
*A.J. Buckley as Harvey Morris
*Bentley Mitchum as Walter Morris
*Yvette Nipar as Agent Kate Jensen
*Jennifer Sipes as Crystal Martin
*Todd Terry as Lou Dowdy
*Jerry Cotton as Traxell Byrne John S. Davies as Det. Pete Michaels
*Richard Nance as Frank Boggs
*Marc Macaulay as Herb Sherman
*Brad Leland as Mitch
*David Frye as Howie
*Craig Erickson as Jack Simms
*Jackson Hurst as Hank Charles Baker as Nate

==Controversy==
There was some controversy before this films release, as Dwana Pusser (daughter of Buford Pusser) released a statement on her website condemning this movie and its upcoming  , saying "I have read the scripts and they are very vulgar.  As a Christian woman I am very upset with the direction the movies have taken." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 