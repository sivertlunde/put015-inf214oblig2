Bond Girls Are Forever
{{Infobox film
| name           = Bond Girls Are Forever
| image          = Bond-girls-are-forever-dvd-front.jpg
| caption        = DVD cover
| border         = yes
| alt            = 
| director       =  
| producer       = Maryam dAbo 
| writer         = 
| based on       =  Jane Seymour, Maud Adams, Lois Chiles, Carey Lowell, Michelle Yeoh, Judi Dench, Samantha Bond, Rosamund Pike
| music          = 
| cinematography = Brian Pratt 
| editing        = Kevin Bourque, Harry Watson
| studio         = Planet Grande Pictures, Metro-Goldwyn-Mayer 
| distributor    = MGM Domestic Television Distribution 
| released       =  
| runtime        = 46 min  (USA)  49 min  (updated) (DVD) (Finland)  
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

{{Infobox book |  
| name          = Bond Girls Are Forever
| title_orig    = 
| translator    = 
| image         = Bond-girls-are-forever-bookcover.jpg
| caption = Boxtree British hardcover edition
| author        = John Cork Maryam dAbo
| illustrator   = 
| cover_artist  = 
| country       = United Kingdom
| language      = English
| series        = James Bond Documentary Non-fiction
| publisher     = Boxtree Ltd.
| release_date  = 2002 (Documentary) 2003 (Book)
| media_type    = Print (Hardcover) TV film
| pages         = 192 pp.
| isbn          = 0-7522-1550-7
| oclc          = 52785338
| preceded_by   = 
| followed_by   = 
}}
Bond Girls Are Forever is a 2002 James Bond documentary film hosted by actress Maryam dAbo, who had played the role of Kara Milovy in the 15th James Bond film The Living Daylights. It was accompanied by a 2003 book written by John Cork and dAbo. The book is subtitled The Women of James Bond. Both the film and the book is a tribute to the elite club of women who have played the role of a Bond girl. 

The TV film, which was released in November 2002 alongside Die Another Day features interviews with a number of Bond girls who were featured throughout the film franchise between the first James Bond film, Dr. No (film)|Dr. No (1962) starring Ursula Andress and the then-current 20th film Die Another Day starring Halle Berry. In 2003, the documentary was released on DVD and offered as a free gift with the purchase of Die Another Day on DVD by some retailers.
 Casino Royale AMC network and was later released as a bonus feature on the March 2007 DVD and Blu-ray editions of Casino Royale.

A new 2012 version was shown on the Sky Movies 007 channel in the UK to include Quantum of Solace and Skyfall.

Emmy award–winning singer & songwriter Faith Rivera performed a rendition of "Nobody Does it Better" over the closing credits of the documentary.

==Bond girls interviewed in order==
*Halle Berry
*Ursula Andress
*Honor Blackman
*Luciana Paluzzi
*Jill St. John Jane Seymour
*Maud Adams
*Lois Chiles
*Carey Lowell
*Michelle Yeoh
*Judi Dench
*Samantha Bond
*Rosamund Pike
*Eva Green (2006 version)
*Caterina Murino (2006 version)
*Gemma Arterton (2008 version)
*Naomie Harris (2012 version)
*Berenice Marlohe (2012 version)

==References==
 

==External links==
* 

 
 
 
 
 


 