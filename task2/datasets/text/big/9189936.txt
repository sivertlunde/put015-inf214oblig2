The Double McGuffin
{{Infobox film
|image=Poster of the movie The Double McGuffin.jpg
| director = Joe Camp
| writer = Joe Camp
| producer = Joe Camp
| starring = Lisa Whelchel Ernest Borgnine George Kennedy Dion Pride Greg Hodges
| music = Euel Box
| cinematography = Don Reddy
| runtime = 101 minutes
| released = 1979
| country = United States
| language = English
}}
 The Facts of Life.

Elke Sommer and NFL stars Ed Too Tall Jones and Lyle Alzado also appear in smaller roles. The film also included a young Vincent Spano as well as Dion Pride (son of country singer Charley Pride). An opening narration is provided by Orson Welles. The cast was rounded out by Chicago native Michael Gerard, and Dallas area child actors Greg Hodges and Jeff Nicholson.

==Explanation of the title== McGuffin is an object that serves as the focal point of the plot in the thriller genre. This film has two such objects (a suitcase of money and a severed hand).

==Plot==
The plot follows a group of boarding school students who discover, in succession, a suitcase full of money, a dead body, and a dismembered hand. They are unable to convince the local police to take them seriously, because they have not secured any evidence, and because the police chief (played by Kennedy) is suspicious of them due to their past misbehavior. They follow the evidence themselves and realize that a political assassination is planned at a school event. They foil the plot themselves.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 