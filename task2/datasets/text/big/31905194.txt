Cosmo Jones, Crime Smasher
{{Infobox film
| name           = Cosmo Jones, Crime Smasher
| image_size     =
| image	=	Cosmo Jones, Crime Smasher FilmPoster.jpeg
| caption        =
| director       = James Tinling
| producer       = Lindsley Parsons (producer) Frank Graham (radio program Cosmo Jones)
| narrator       =
| starring       = See below
| music          =
| cinematography = Mack Stengler
| editing        = Carl Pierson
| distributor    =
| studio         = Monogram Pictures
| released       = 1943
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Cosmo Jones, Crime Smasher is a 1943 American film directed by James Tinling. The film is also known as Cosmo Jones in Crime Smasher (American poster title).

== Cast ==
*Edgar Kennedy as Police Chief Murphy Richard Cromwell as Sgt. Pat Flanagan
*Gale Storm as Susan Fleming
*Mantan Moreland as Eustace Smith Frank Graham as Professor Cosmo Jones
*Gwen Kenyon as Phyllis Blake
*Herbert Rawlinson as Mr. James J. Blake Tristram Coffin as Jake Pelotti Charles Jordan as Biff Garr
*Vince Barnett as Henchman "Gimp"
*Emmett Vogan as Police Commissioner Gould
*Maxine Leslie as Mrs. Jake Pelotti
*Mauritz Hugo as Tony Sandol - Gangster Sam Bernard as Gangster

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 


 