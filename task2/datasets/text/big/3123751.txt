Tightrope (film)
{{Infobox film name           = Tightrope image          = Tightrope_film.jpg caption        = Theatrical release poster director       = Richard Tuggle (credited) Clint Eastwood writer         = Richard Tuggle
| starring = {{Plainlist|
* Clint Eastwood
* Geneviève Bujold
* Dan Hedaya
* Alison Eastwood Jennifer Beck
}} producer       = Clint Eastwood Fritz Manes cinematography = Bruce Surtees music          = Lennie Niehaus studio         = The Malpaso Company distributor    = Warner Bros. released       =   runtime        = 115 minutes language       = English budget         = gross          = $48,143,579 (USA)
}} thriller produced by and starring Clint Eastwood and written and directed by Richard Tuggle.

==Plot==
A young woman walking home from her birthday party is stalked by a man in distinctive sneakers. After she drops one of her presents, a police officer offers to escort her to her front door. The camera reveals that the policeman is wearing the same sneakers as the stalker.
 Saints game, Block is summoned to a crime scene, forcing him to break his plans with his daughters.

The young woman has been strangled in her bed. Her killer left no fingerprints, but he waited in her apartment until midnight to kill her, even pausing to make himself coffee. Block visits a brothel where the woman worked, and interviews a prostitute with whom she would perform group sex. The prostitute seduces Block, loosening his necktie, which he accidentally leaves behind.

The murderer rapes his victims, and he has been leaving behind a great deal of forensic evidence, including a residue of glass fragments and barley. Beryl Thibodeaux (Geneviève Bujold) runs a rape prevention program, and she advises Block on the case. The second victim is also a sex worker, and she is strangled in a jacuzzi. Block tracks down one of her co-workers and interviews her while the two prepare to have sex. He handcuffs the woman to the bed.

While Block inquires about the victims at another brothel, he has sex with a prostitute. The hidden killer watches Block and the prostitute. The next morning, Block is called to the scene of a third victim. He is shocked to realize that it is the prostitute he had been with the night before. Under the guise of working on the case, Block flirts with Thibodeaux, and the two spend the rest of the day together.

The killer taunts Block by sending a doll with a note, which directs him to another brothel. Once there, a dominatrix informs Block that an unknown man has hired her to be whipped by Block. She is then supposed to send Block to a gay bar. At the bar, Block meets up with a man who has been hired by the killer to have sex with Block. Block instructs the man to pick up his pay as scheduled and follows him, hoping to catch the killer. However, Block is too late, and the man is killed.

The killer kidnaps the friend of the third victim, and he dumps her body in a public fountain. He drapes Blocks abandoned necktie on a nearby statue. Block and Thibodeaux go out on a second date, escorting his children, while secretly observed by the killer disguised as a Mardi Gras participant. When they are in bed later, Block shies away from intimacy with Thibodeaux, and then has a nightmare that he attacks her in the guise of the killer.

One of the victims clothes has some cash in it, which the police trace to the payroll of a brewery. The money has the same glass and barley residue on it that has been cropping up at all the crime scenes. When Block goes to the brewery to investigate, the killer watches him during his visit. That night, the killer breaks into Blocks home, killing the nanny and some of Blocks pets, and handcuffing and gagging Amanda. Block is nearly strangled in a struggle after he arrives and is only saved when one of his surviving dogs repeatedly bites the killer. Block fires two shots at the killer as he escapes (and is later seen watching the police attending the scene from concealment).

While going through news clippings, Block comes across the name of a cop, Leander Rolfe (Marco St. John), whom he arrested for raping two girls. Further investigation reveals that Rolfe had been paroled and was working at the brewery. Block and his team stake out Rolfes apartment, but Rolfe has gone to attack Thibodeaux at her home (successfully slaying the cops guarding her). Realizing that she is in danger, Block races to her home, where he disturbs Rolfes attempt to strangle her (despite being stabbed twice with scissors). He chases Rolfe through a cemetery and into a rail yard. During their final scuffle, they end up in the path of an oncoming train;  Block manages to roll aside in time, but Rolfe is run over and killed. Block accepts a womans touch after he tells Beryl everything will be okay.

==Cast==
* Clint Eastwood as Wes Block
* Geneviève Bujold as Beryl Thibodeaux
* Dan Hedaya as Det. Molinari
* Alison Eastwood as Amanda Block Jenny Beck as Penny Block
* Rod Masterson as Patrolman Gallo
* Marco St. John as Leander Rolfe

==Production==
Tightrope was filmed in New Orleans in the fall of 1983. Hughes, p.71  While Tuggle retained the directors credit, as with The Outlaw Josey Wales on which original director Philip Kaufman was replaced by the star, Eastwood directed most of the movie after finding Tuggle working too slowly.   

==Box Office==
The film was released in United States theaters in August 1984. Hughes, p.71  It eventually grossed $48 million at the United States box office. Hughes, p.72  In its opening weekend Tightrope was number 1, taking in $9,156,545, an average $5,965 per theater. 

===Critical reception=== At the David Denby compared Eastwoods directing style with that of "Don Siegels tawdry, urban-anxiety mode, slowed by episodes of rapt erotic stillness", and stated that as actor he "also gave his most complex and forceful performance to date." 

==References==
 

==Bibliography==
*  

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 