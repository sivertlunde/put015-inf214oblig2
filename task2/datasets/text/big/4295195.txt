Knocked Up
 
{{Infobox film
| name = Knocked Up
| Rate = Restricted
| image = Knockedupmp.jpg
| caption = Theatrical release poster
| director = Judd Apatow
| producer = Seth Rogen Evan Goldberg Judd Apatow Shauna Robertson Clayton Townsend
| writer = Judd Apatow
| starring = Seth Rogen Katherine Heigl Paul Rudd Leslie Mann
| music = Loudon Wainwright III Joe Henry
| cinematography = Eric Alan Edwards
| editing = Craig Alpert Brent White
| studio = Apatow Productions Universal Pictures
| released =  
| runtime = 129 minutes
| country = United States
| language = English
| budget = $30 million 
| gross = $219.1 million   
}} romantic comedy film co-produced, written, and directed by Judd Apatow. The films stars Seth Rogen, Katherine Heigl, Paul Rudd, and Leslie Mann. It follows the repercussions of a drunken one-night stand between a slacker and a just-promoted media personality that results in an unintended pregnancy. A spin-off sequel, This Is 40, was released in 2012.

==Plot==
Ben Stone (  on, but he misinterprets this to mean to dispense with using one. The following morning, they quickly learn over breakfast that they have little in common and go their separate ways, which leaves Ben visibly upset. 

Eight weeks later, Alison experiences morning sickness during an interview with James Franco and realizes she could be pregnant. She contacts Ben for the first time since their one-night stand to tell him. Although insensitive at first, Ben says he will be there to support Alison. While he is still unsure about being a parent, his father (Harold Ramis) is excited. Alisons mother (Joanna Kerns) tries to persuade her daughter to have an abortion, but Alison decides to keep the child. Later, Alison and Ben decide to give their relationship a chance. The couples efforts include Ben making an awkward marriage proposal with an empty ring box, promising to get her one someday. Alison thinks it is too early to think about marriage, because she is more concerned with hiding the pregnancy from her bosses, believing that they will fire her if they ever found out. After a somewhat promising beginning, tensions surface in the relationship. 
 Las Vegas. 
 psychedelic mushrooms, Craig Robinson) on account of Debbies age and Alisons pregnancy, leading to Debbies tearful laments about her life and her desire to have Pete back. They reconcile at their daughters birthday party, but when Ben tries to work things out with Alison, she doesnt want to get back together. Alisons boss finds out about her pregnancy, and sees an opportunity to boost ratings with female viewers by having Alison interview pregnant celebrities. After a talk with his father, Ben decides to take responsibility and goes to great effort to change his ways, including moving out of his friends house, getting an office job as a web designer, and creating a babys room in his new apartment. 
 Loudon Wainwright) is out of town, despite having assured them that he never took vacations. Ben calls him and leaves a furious voicemail, threatening murder. During labor, Alison apologizes for doubting Bens commitment and admits that she never thought the man who got her pregnant would be the right one for her. When Debbie and Pete arrive at the hospital, Ben adamantly refuses to allow her to be at Alisons side, insisting that it is his place. Debbie is both furious and impressed that Ben took charge of the situation and begins to change her formerly negative opinion about him. The couple welcomes a baby girl (a boy in the alternate ending) and settle down happily together in a new apartment in Los Angeles.

==Cast==
* Seth Rogen as Ben Stone
* Katherine Heigl as Alison Scott
* Paul Rudd as Pete
* Leslie Mann as Debbie
* Jason Segel as Jason
* Jay Baruchel as Jay
* Jonah Hill as Jonah
* Martin Starr as Martin
* Charlyne Yi as Jodi
* Iris Apatow as Charlotte
* Maude Apatow as Sadie
* Joanna Kerns as Mrs. Scott
* Harold Ramis as Harris Stone
* Alan Tudyk as Jack
* Kristen Wiig as Jill
* Bill Hader as Brent
* Ken Jeong as Dr. Kuni
* J. P. Manoux as Dr. Angelo
* Tim Bagley as Dr. Pellagrino
* B. J. Novak as Doctor
* Mo Collins as Female Doctor Loudon Wainwright as Dr. Howard Adam Scott as Male Nurse Craig Robinson as Club Doorman
* Tami Sagher as Wardrobe Lady

===Themselves (credited)===
* Jessica Alba
* Orlando Bloom
* Steve Carell
* Andy Dick
* James Franco
* Eva Mendes
* Ryan Seacrest
* Dax Shepard

==Production==

===Casting===
Several of the major cast members return from previous  , Martin Starr, Jason Segel, and James Franco all starred in the short-lived, cult television series Freaks and Geeks which Apatow produced. From the Apatow-created Undeclared (which also featured Rogen, Segel and Starr) there is Jay Baruchel and Loudon Wainwright III. Paul Feig, who co-created Freaks and Geeks and starred in the Apatow-written movie Heavyweights, also makes a brief cameo as the Fantasy Baseball Guy. Steve Carell, who makes a cameo appearance as himself, played the main role in Apatows The 40-Year-Old Virgin which also starred Rogen and Rudd, as well as appearing in the Apatow-produced Anchorman (film)|Anchorman. Finally, Leslie Mann, who also appeared in The 40-Year-Old Virgin, is married to Apatow and their two daughters play her children in the movie.

Anne Hathaway was originally cast in the role of Alison in the film, but dropped out due to creative reasons  that Apatow attributed to Hathaways disagreement with plans to use real footage of a woman giving birth.  Jennifer Love Hewitt and Kate Bosworth auditioned for the part after Hathaway dropped out, but ended up losing to Katherine Heigl.

Bennett Miller, the director of Capote (film)|Capote, appears in a mockumentary DVD feature called "Directing the Director", in which he is allegedly hired by the studio to supervise Apatows work, but only interferes with it, eventually leading the two into a fist fight.

==Reception==

===Box office performance=== unexpected financial success to the use of radio and television ads in combination. 

===Critical reviews===
Knocked Up received critical acclaim upon its release. On Rotten Tomatoes the film holds a rating of 91%, based on 235 reviews, with an average rating of 7.7/10. The sites critical consensus reads, "Knocked Up is a hilarious, poignant and refreshing look at the rigors of courtship and child-rearing, with a sometimes raunchy, yet savvy script that is ably acted and directed."  On Metacritic the film has a score of 85 out of 100, based on 38 critics, indicating "universal acclaim". 

The  . June 1, 2007. Retrieved 
October 26, 2007.  Chris Kaltenbach of The Baltimore Sun acknowledged the comic value of the film in spite of its shortcomings, saying, "Yes, the story line meanders and too many scenes drone on; Knocked Up is in serious need of a good editor. But the laughs are plentiful, and its the rare movie these days where one doesnt feel guilty about finding the whole thing funny." 
 Ebert & Roeper, Richard Roeper and guest critic David Edelstein gave Knocked Up a "two big thumbs up" rating, with Roeper calling it "likeable and real," noting that although "at times things drag a little bit.... still Knocked Up earns its sentimental moments." 

A more critical review in Time (magazine)|Time magazine noted that, although a typical Hollywood-style comedic farce, the unexpected short-term success of the film may be more attributable to a sociological phenomenon rather than the quality or uniqueness of the film per se, positing that the movies shock value, sexual humor and historically taboo themes may have created a brief nationwide discussion in which movie-goers would see the film "so they can join the debate, if only to say it wasnt that good." 

===Alleged copyright infringement===
Canadian author Rebecca Eckler wrote in Macleans magazine about the similarities between the movie and her book, Knocked Up: Confessions of a Hip Mother-to-Be, which was released in the U.S. in March 2005. She pursued legal action against Apatow and Universal Pictures on the basis of copyright infringement.   In a public statement, Apatow said, "Anyone who reads the book and sees the movie will instantly know that they are two very different stories about a common experience." 

Another Canadian author, Patricia Pearson, also publicly claimed similarities between the film and her novel, Playing House. She declined to sue and declared Ecklers lawsuit to be frivolous. 

===Accusations of sexism=== Mike White Orange County, and Nacho Libre) is said to have been "disenchanted" by Apatows later films, "objecting to the treatment of women and gay men in Apatows recent movies", saying of Knocked Up, "At some point it starts feeling like comedy of the bullies, rather than the bullied." 

In early reviews, both   of The New York Times explicitly compared Knocked Up to Juno (film)|Juno, calling the latter a "feminist, girl-powered rejoinder and complement to Knocked Up." 
 Vanity Fair interview, lead actress Katherine Heigl admitted that though she enjoyed working with Apatow and Rogen, she had a hard time enjoying the film itself, calling it "a little sexist" and claiming that the film "paints the women as shrews, as humorless and uptight, and it paints the men as lovable, goofy, fun-loving guys."   

In response, Apatow did not deny the validity of her accusations, saying, "Im just shocked she   used the word shrew. I mean, what is this, the 1600s?" 

Heigls comments spurred widespread reaction in the media, including a The Huffington Post article in which she was labeled "an assertive, impatient go-getter who quickly tired of waiting for her boyfriend to propose".   Heigl clarified her initial comments to People Magazine|People magazine, stating that, "My motive was to encourage other women like myself to not take that element of the movie too seriously and to remember that its a broad comedy," adding that, "Although I stand behind my opinion, Im disheartened that it has become the focus of my experience with the movie." 

 , 2007-05-06. Retrieved June 4, 2007.  
 The Ugly Truth to illustrate his point. Rogen said: "I hear theres a scene where shes wearing underwear with a vibrator in it, so Id have to see if that is uplifting for women." Apatow attempted to cut Heigl some slack for the criticisms chalking up her harsh words to exhaustion at the end of a long day of interviews, but admitted he never received an apology from Heigl. "You would think at some point Id get a call saying she was sorry, that she was tired, and then the call never comes." 

===Top ten lists===   2007 AFI Awards as well as the top-ten lists of several well-known critics, with the AFI jury calling it the "funniest, freshest comedy of this generation" and a film that "stretches the boundaries of romantic comedies." John Newman, respected film critic for the Boston Bubble, called the film "a better, raunchy, modern version of Some Like it Hot." 

Early on the film was deemed the best reviewed wide release of 2007 by the Rotten Tomatoes website. 

The film appeared on many critics top-ten lists of the best films of 2007.  Kyle Smith, New York Post
* 4th – Christy Lemire, Associated Press 
* 5th – Scott Tobias, The A.V. Club
* 6th – David Ansen, Newsweek
* 8th – Ella Taylor, LA Weekly
* 9th – Empire (magazine)|Empire
* 9th – Scott Foundas, LA Weekly (tied with Superbad (film)|Superbad)
* 10th – A. O. Scott, The New York Times (tied with Juno (film)|Juno and Superbad (film)|Superbad)
* 10th – Lisa Schwarzbaum, Entertainment Weekly
* 10th – Peter Travers, Rolling Stone (tied with Juno (film)|Juno) 

===Awards=== ten best movies of the year. It was one of the two pregnancy comedies on the list (Juno (film)|Juno being the other). E! News praised the films success, saying that, "The unplanned pregnancy comedy, shut out of the Golden Globes and passed over by the L.A. and New York critics, was one of 10 films selected Sunday for the American Film Institutes year-end honors." 

Awards:
* The 2007   "Best Hissy Fit", for his brief cameo, where he becomes self-obsessed and complains about rising young talents, saying that they "fuck his day up".
* Judd Apatow was nominated for the Writers Guild of America Award for Best Original Screenplay.
* In 2008, the film was nominated for a Canadian Comedy Award for Best Actor, for Seth Rogen. Coincidentally Rogen lost to Michael Cera for his role in Superbad (film)|Superbad, which Rogen had written. Stony Award for Best Pot Comedy in 2007. 

==Music== folk singer-songwriter Loudon Wainwright III and Joe Henry. However, the movies lead song "Daughter" was written by Peter Blegvad.

In addition to Wainwrights tracks, there were approximately 40 songs featured in the motion picture that were not included on the official soundtrack on Concord Records. 

Some of the songs featured in Knocked Up are: Bright Eyes (feat. Emmylou Harris)
*"All Night" by Damien Marley
*"Stand up tall" by Dizzee Rascal
*"Rock Lobster" by The B-52s
*"Gives You Hell" by The All-American Rejects
*"Police On My Back" by The Clash Biggest Part Ambrosia
*"Smile" by Lily Allen
*"Girl" by Beck
*"King without a Crown" by Matisyahu
*"Toxic (song)|Toxic" by Britney Spears Sublime
*"Tropicana" by Ratatat
*"Shimmy Shimmy Ya" by Ol Dirty Bastard
*"Love Plus One" by Haircut One Hundred Scorpions
*"Reminiscing" by Little River Band
*"Ashamed" by Tommy Lee Savage (featured in the menu section of the DVD)
*"Shame on a Nigga" by Wu-Tang Clan (used in the films trailer)
*"Grey in LA" by Loudon Wainwright III
*"End of the Line" by Traveling Wilburys (used in the films trailer)

==Home release==
 
Several separate Region 1 DVD versions were released on September 25, 2007. There was the theatrical Motion Picture Association of America film rating system|R-rated version (128 minutes), an "Unrated and Unprotected" version (133 minutes) (fullscreen and widescreen available independently), a two-disc "Extended and Unrated" collectors edition, and an HD DVD "Unrated and Unprotected" version. On November 7, 2008, Knocked Up was released on Blu-ray Disc|Blu-ray following the discontinuation of HD DVD, along with other Apatow comedies The 40-Year-Old Virgin and Forgetting Sarah Marshall.

==This Is 40==
Variety (magazine)|Variety reported in January 2011 that Paul Rudd and Leslie Mann would reprise their Knocked Up roles for a new film written and directed by Apatow, titled This Is 40.  Apatow had stated that it would not be not a sequel or prequel to Knocked Up, but a spin-off, focusing on Pete and Debbie, the couple played by Rudd and Mann. Sciretta Peter (January 7, 2011)   /Film. Retrieved 2011-01-07.   The film was shot in the summer of 2011,  and was released on December 21, 2012. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 