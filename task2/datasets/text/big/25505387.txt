Once in a Lifetime (1994 film)
{{Infobox film
| name           = Once in a Lifetime
| image          = Once in a Lifetime 1994 DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = Michael Miller
| producer       = 
| writer         = Danielle Steel (novel) Syrie Astrahan James
| narrator       = 
| starring       = Lindsay Wagner Barry Bostwick
| music          = David Shire
| cinematography = Kees Van Oostrum
| editing        = Janet Bartels-Vandagriff
| studio         =
| distributor    = NBC
| released       = February 15, 1994
| runtime        = 91 minutes  USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1994 television 1982 novel of the same name written by Danielle Steel.

== Plot ==
Daphne Fields is a world famous novelist from New York City who one night is hit by a car. She falls into a coma and her memory flashes back to eight years earlier, when she was happily married to Jeffrey and had a loving daughter, Aimee. Her perfect life had subsequently fallen into pieces when she suddenly loses her husband and daughter in a fire. Nine months later, she gives birth to a son, Andrew, who turns out to be deaf. Daphne decides to learn sign language and is tutored by Dr. Matthew Dane, an educator for the deaf. Inspired by her loss, she starts writing a novel called Autumn Years.

Daphne grows to be an overprotective mother. Unwilling to enroll her son in a New York public school, she enters him in a special boarding school for deaf children, headed by Matthew Dane and located in New Hampshire. Able to focus on her freelance writing career, she makes her literary debut with a bestseller and hires her best friend Barbara as her assistant. She refuses to do any promotion or tours, though, so that she will be free to visit her son at any time. She often visits his school and grows close to its principal. She keeps herself from becoming involved with him, though, feeling that she should remain faithful to Jeffrey.

Meanwhile, she learns that the rights of her second novel Apache have been sold and that it will be turned into a movie. She reluctantly heads to Hollywood, where she falls in love with Justin Wakefield, who is starring in the film adaption. After one romantic night, he tells her that he is in love with her. She loves him also, but soon notices that he takes little interest in her home life and is not enthusiastic about meeting Andrew. Nevertheless, she accepts his marriage proposal and considers locating to Los Angeles with her son.

Upset over Justins lifestyle, coming and going when he feels like it and refusing to be a traditional husband, Daphne breaks off the engagement. She returns home to declare her love to Matthew, but sees him with another woman and misinterprets their relationship. The flashbacks end and the movie forwards to the present. Matthew rushes to the hospital to see Daphne, who still is in a coma. As she awakens, he admits that he is in love with her, and they get engaged.

==Cast==
*Lindsay Wagner as Daphne Fields
*Barry Bostwick as Dr. Matthew Matt Dane
*Duncan Regehr as Justin Wakefield
*Amy Aquino as Barbara Brice
*Rex Smith as Jeffrey Fields
*Darrell Thomas Utley as Andrew Fields (Age 8)
*Nicholas Petrie as Andrew Fields (Age 4)
*Fran Bennett as Dr. Liz Wells
*Jessica Sinegal as Aimee Fields

==References==
 

==External links==
* 

 

 
 
 
 
 