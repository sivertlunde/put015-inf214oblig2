Fog Island
{{Infobox film
| name           = Fog Island
| image_size     =
| image	=	Fog Island FilmPoster.jpeg
| caption        =
| director       = Terry O. Morse
| producer       = Terry O. Morse (associate producer) Leon Fromkess (producer) Pierre Gendron (screenplay) Bernadine Angus (play Angel Island)
| narrator       =
| starring       = See below
| music          = Karl Hajos
| cinematography = Ira H. Morgan
| editing        = George McGuire
| distributor    = Producers Releasing Corporation
| released       = 15 February 1945
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Fog Island is a 1945 American mystery-suspense film directed by Terry O. Morse. The film stars B movie horror film regulars George Zucco and Lionel Atwill. It was based on the 1937 play Angel Island by Bernadine "Bernie" Angus.

==Plot summary==
A recent ex convict named Leo Grainer (so-called in the credits, but in fact referred to as Grainger throughout) lives secluded on Fog Island with the daughter of his murdered wife. Seeking to learn who murdered her, and to exact revenge on those who framed him and destroyed his business, he invites his former associates to his creepy island mansion on the pretext he may share a hidden fortune with them.

Prior to their arrival he rigs the mansion with secret passages and a trap. Then, once his guests arrive, he gives each a clue, including his step daughter and butler. This successfully pits everyone against the others and plays on their greed. What then transpires is conflict, revealed mysteries, sudden death, and an unlikely resolution.

==Cast==
*George Zucco as Leo Grainer
*Lionel Atwill as Alec Ritchfield
*Jerome Cowan as Kavanaugh
*Sharon Douglas as Gail
*Veda Ann Borg as Sylvia John Whitney as Jeff
*Jacqueline deWit as Emiline Bronson
*Ian Keith as Dr. Lake George Lloyd as Allerton - Butler

==Production== PRC Release]] and the studio would only give less than $200 dollars to writers for the final script. 

==Soundtrack==
* Sharon Douglas and Karl Hajos - "Liebestraum (Loves Dream)" by Franz Liszt

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 


 