Mr. Topaze
{{Infobox film
| name = Mr. Topaze
| image =
| caption =
| director = Peter Sellers
| producer = Pierre Rouve
| writer = Marcel Pagnol Pierre Rouve
| starring = Peter Sellers Nadia Gray Herbert Lom Leo McKern
| music = George Martin Georges Van Parys
| cinematography = John Wilcox
| editing = Geoffrey Foot
| distributor =
| released =  
| runtime = 97 minutes
| country = United Kingdom
| language = English
| budget =
}}

Mr. Topaze (aka I Like Money) was Peter Sellers directorial debut in 1961 in film|1961. Starring Sellers, Nadia Gray and Leo McKern, as well as Herbert Lom who quarrelled with Sellers Inspector Clouseau in the Pink Panther movies.
 Michael Sellers plays in the film in the role of Gaston. 

However, one copy (only) of the film still remains, locked in the British Film Institute archives. The film was extremely rarely shown, once during the 2003 Cardiff Independent Film Festival. 

==Plot==
 French town who is honest to a fault. He is fired when he refuses to give a passing grade to a bad student, the grandson of a wealthy Baroness (Martita Hunt). Castel Benac (Herbert Lom), a government official who runs a crooked financial business on the side, is persuaded by his mistress, Suzy (Nadia Gray), a musical comedy actress, to hire Mr. Topaze as the front man for his business. Gradually, Topaze becomes a rapacious financier who sacrifices his honesty for success and, in a final stroke of business bravado, fires Benac and acquires Suzy in the deal. An old friend and colleague, Tamise (Michael Gough) questions him and tells Topaze that what he now says and practices indicates there are no more honest men.

==Cast==
*Peter Sellers as Auguste Topaze
*Nadia Gray as Suzy
*Herbert Lom as Castel Benac
*Leo McKern	as Muche
*Martita Hunt as Baroness
*Michael Gough as Tamise
*Anne Leon as Mrs. Tamise
*Billie Whitelaw as Ernestine
*Joan Sims as Colette John Neville as Roger
*John Le Mesurier as Blackmailer

== Bibliography ==
 

*  

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 