Arrival of a Train at Vincennes Station
{{Infobox film
| name           = Arrivée dun train gare de Vincennes
| image          =
| image_size     =
| caption        =
| director       = Georges Méliès
| producer       = Georges Méliès
| writer         =
| narrator       =
| starring       =
| cinematography =
| editing        =
| distributor    = Star Film
| released       =  
| runtime        = 20 meters/65 feet   
| country        = France Silent
| budget         =
| gross          =
}}
 silent actuality film directed by Georges Méliès. It was released by Mélièss company Star Film and is numbered 7 in its catalogues. 

==Possible survival==
 
The film is currently presumed Lost film|lost.  However, the South American animator Bernhard Richter suggested in 2013, based on research done with his daughter Sara Richter, that a flipbook published around the turn of the century by Léon Beaulieu may be a surviving print of the film. Beaulieu was a manufacturer of  flipbooks, or "folioscopes," based on movies produced between 1895 and 1898.   

The suggested identification is based on the type of train depicted in the flipbook, but Sara Richter noted in a statement to the magazine Variety (magazine)|Variety that no conclusive evidence to link the flipbook to Méliès has yet been found. The UCLA archivist Jan-Christopher Horak has posited that, while the flipbook may well be a Méliès print, it might also be a Lumière Brothers film. The preservationist Serge Bromberg similarly commented on the infeasibility of identifying the flipbook with certainty as a Méliès film, rather than one by other early filmmakers, without further evidence.  Georges Mélièss great-great-granddaughter Pauline Méliès noted in an online statement that the flipbook may possibly derive from Méliès, but, based on scrutiny of the inscriptions on the train, suggested that it may be a slightly later Méliès film, Arrival of a Train (Joinville Station). 

The Richters have announced that they hope to gather more evidence by crowdsourcing the research project. 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 

 
 