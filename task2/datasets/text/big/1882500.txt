The Flamingo Kid
{{Infobox film
| name           = The Flamingo Kid
| image          = Flamingokidposter.jpg
| caption        = Theatrical release poster
| director       = Garry Marshall Michael Phillips Nick Abdo (associate producer)
| writer         = Garry Marshall Neal Marshall Bo Goldman
| starring       = {{plainlist|
* Matt Dillon
* Richard Crenna Hector Elizondo
* Jessica Walter
}}
| music          = Curt Sobel
| cinematography = James A. Contner
| editing        = Priscilla Nedd-Friendly
| studio         = ABC Motion Pictures
| distributor    = 20th Century Fox 
| released       =  
| runtime        = 100 min.
| country        = United States
| language       = English
| gross          = $23,859,382
}} The Woman in Red, Dreamscape (1984 film)|Dreamscape, and Dune (1984 film)|Dune.

Tagline: A legend in his own neighborhood

==Plot==
In the summer of 1963, Jeffrey Willis (Matt Dillon) joins some friends for a day of Gin rummy at the El Flamingo Club, a private beach resort.  There, he meets the girl of his dreams Carla Sampson (Janet Jones).  After the Gin game and being told of the clubs strict policy regarding guests, Jeffrey is upset, but not for long, since he immediately landed a job as a car valet and eventually, cabana steward. Jeffrey is a kid from a middle class Brooklyn family and his father (Elizondo) does not approve of him working at the private club.
  Gin rummy card game champ, Phil Brody (Crenna). 

Jeffrey, a winning Gin Rummy player himself, and his friends, admire Brody and how his wins at the Gin rummy table make him seem "psychic," knowing which cards to give up.  Brody also takes a liking to Jeffrey, eventually showing him his car business, and gives him hopes that car sales are where he belongs as a career.

Jeffrey gets further immersed in the "easy buck," defying his fathers guidance.  During dinner, Jeffrey notably says he "will not be needing college" and plans to pursue being a car salesman instead.  Jeffrey and his co-workers at The Flamingo also venture to Yonkers Raceway together, risking cash on a horse tip but come up short when the trotter breaks stride.

Eventually, Jeffrey leaves home to pursue the sales job.  However, Brody, angry that Jeffrey disturbed him during a dance class, reveals to Jeffrey that the job opening at the car dealership is for a stock boy, not as a salesman as Jeffrey had been led to believe was his when he asked for it.  Brody encourages Jeffrey to take the stock boy position so he can work his way up.  Jeffrey becomes shocked at his mentors actions and reconsiders college. Near Summers end, Jeffrey observes that a regular onlooker, "Big Sid", is feeding signals to Brody, the true cause of Brodys winning ways.  When Big Sid and a member of the gin team playing against Brodys team are overcome by the heat, Jeffrey fills in, opposing Brody, and seeking to help win back the unfair profits Brody won from his friends over the course of the Summer. Jeffrey and his team eventually win back what was unfairly lost, including a good profit besides. Realizing the mistakes he made in rejecting his fathers good advice, Jeffrey makes up with his dad in a touching scene at Larrys Fish House ("Any Fish You Wish"), where his family is dining.

==Cast==
* Matt Dillon – Jeffrey Willis
* Hector Elizondo – Arthur Willis
* Richard Crenna – Phil Brody
* Janet Jones – Carla Samson
* Jessica Walter – Phyllis Brody
* Fisher Stevens – Hawk Ganz
* Bronson Pinchot – Alfred Shultz
* Marisa Tomei – Mandy Steven Weber – Paul Hirsch
* Martha Gehman – Nikki Willis

==Location== Breezy Point Superstorm Sandy in 2012, and the wing of the club that extends over the water is facing probable demolition.   

==Soundtrack==
A soundtrack to the film was released by Motown. 

# Jesse Frederick – Breakaway
# Martha and the Vandellas – (Love Is Like a) Heat Wave
# The Chiffons – Hes So Fine
# Acker Bilk – Stranger on the Shore Dion – Runaround Sue
# Little Richard – Good Golly, Miss Molly
# Barrett Strong – Money (Thats What I Want)
# The Impressions – Its All Right
# Hank Ballard & The Midnighters – Finger Poppin Time One Fine Day Get a Job
# Maureen Steele – Boys Will Be Boys

==Remake==
Deadline.com announced in September 2012 that Walt Disney Pictures has planned a remake of The Flamingo Kid. Brett Ratner and Michael Phillips are producing and music video director Nzingha Stewart is scripting. 

==References==
 

==External links==
*   at MGM.com
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 