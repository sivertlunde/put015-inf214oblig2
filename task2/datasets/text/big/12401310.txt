Phantom from Space
{{Infobox film
| name           = Phantom from Space
| image          = Phantomfromspace.jpg
| alt            =
| caption        = Theatrical release poster
| director       =W. Lee Wilder
| producer       = W. Lee Wilder
| screenplay     = William Raynor Myles Wilder
| story          = Myles Wilder
| starring       = Ted Cooper Noreen Nash Dick Sands Burt Wenland
| music          = William Lava
| cinematography = William H. Clothier
| editing        = George Gale
| distributor    = United Artists
| studio         = Planet Filmplays 
| released       =  
| runtime        = 73 minutes
| language       = English
| budget         =
| gross          =
}}

Phantom from Space is a 1953 American science fiction film produced and directed by W. Lee Wilder from an original screenplay written by William Raynor and Myles Wilder.  It was one of several films made in the early 1950s by Wilder and son, Raynor, and most of the same crew, independently on a financing-for-distribution basis with United Artists and, occasionally, RKO-Radio Pictures. 

==Plot==
Federal Communications Commission (FCC) investigators arrive in the San Fernando Valley after what appears to be a UFO crashes, causing massive interference with teleradio transmissions.  During their investigation they receive witness reports of what appears to be a man dressed in a bizarre outfit. Their investigation uncovers that this is a being from outer space who is invisible. They start a massive manhunt, causing a public panic over the invisible alien running loose.

==Cast==
* Ted Cooper as Lt. Hazen
* Tom Daly as Charlie
* Steve Acton as Mobile Center Dispatcher
* Burt Wenland as Agent Joe
* Lela Nelson as Betty Evans
* Harry Landers as Lt. Bowers
* Burt Arnold as Darrow
* Sandy Sanders as First Policeman
* Harry Strang as Neighbor
* Jim Bannon as Desk Sgt. Jim
* Jack Daly as Joe Wakeman
* Michael Mark as Refinery Watchman
* Rudolph Anders as Dr. Wyatt

==Production and Release==
The film uses stock footage of radar rigs. Some of this stock footage would later reappear in Killers from Space  (1954).  

Phantom from Space opened on May 15, 1953. 
Legend Films released a colorized version of the film. 


==Reception==

===Critical response=== The Pretender Griffith Planetarium. The poor invader is a bald muscle beach type in a radioactive space suit and a helmet that appears to be the same prop from  Robot Monster, somewhat altered." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*   (public domain)

 
 
 
 
 
 
 