Dangerous Touch
{{Infobox Film 
     | name = Dangerous Touch
     | image = DangerousTouch94.jpg
     | caption  = United States theatrical poster
     | director  = Lou Diamond Phillips
     | producer  = Lisa Hansen,   Kurt Voss (co-producer)
     | writer = Kurt Voss
     | cinematography = James Lemmo Eric J. Goldstein
     | starring  = Kate Vernon Lou Diamond Phillips
     | released  = South Korea September 3, 1994 
     | runtime  =  Argentina:101 min, UK:97 min
     | country  = United States
     | language = English
  }}
 thriller film directed by Lou Diamond Phillips. He also co-wrote the film with Kurt Voss, and Lisa Hansen co-produced the film.  James Lemmo  was the Cinematographer. The movie was filmed in Los Angeles, California, USA. The film was made by  Trimark Pictures.

==Plot==

Radio therapist Amanda Graces life turns hellish after she becomes involved with young hustler Mick Burroughs. Mick seduces the radio host in order to get hold of a file she has on a criminal who also happens to be one of her patients. Soon, the two are having steamy, erotic encounters that include kinky sex. But she gets so caught up in their relationship that she leaves herself wide open to Micks treachery. Amanda finds her entire career in jeopardy when Mick blackmails her, and threatens to show everyone an incriminating videotape of them having sex, which also involved a female prostitute, if she doesnt do whatever he says. 

==Cast==

*Kate Vernon  - Amanda Grace 
*Lou Diamond Phillips  - Mick Burroughs
*Andrew Divoff  - Johnnie 
*Tom Dugan  - Freddie 
*   - Maid 
*Max Gail  - Jasper Stone 
*Ira Heiden  - Benny 
*   - Female Fan 
*Karla Montana  - Maria 
*Monique Parent  - Nicole 
*Mitch Pileggi  - Vince 
*   - Charlie 
*Adam Roarke  - Robert Turner 
*Berlinda Tolbert  - Sasha Taylor 
*   - Male Fan 

==Production credits==

*Lou Diamond Phillips   - Director / Screenwriter / Producer
*Lisa M. Hansen  – Producer
*   - Co-producer
*Mark Amin  - Executive Producer
*   - Executive Producer
*Kurt Voss  - Screenwriter / Co-producer
*James Lemmo  - Cinematographer
*Terry Plumeri  - Composer (Music Score) Christopher Rouse  – Editor
*   - Production Designer
*   - Art Director
*   - Costume Designer
*   - Casting

==Dangerous Touch (soundtrack)==
* 

Soundtrack for the movie Dangerous Touch.
*"Sexual Healing"
Written by Marvin Gaye, Odell Brown and David Ritz
Performed by Bill Wesley

*"Flowers in the Rain"
Written and Performed by Rachel Pollack

*"Oblivion"
Performed by Odette Springer
Music by Odette Springer
Lyrics by Odette Springer and Cynthia Waring

*"Little Bitty Pretty One"
Written by Robert Byrd
Arranged and Performed by Tony Rogers

*"These Eyes"
Written by Rachel Pollack and Bret Levick
Performed by Rachel Pollack

*"Guantanamera"
Arranged and Performed by Chris Kinsman

*"When You Want Me"
Performed by Roberta Flack
Lyrics by Lou Diamond Phillips
Music by Terry Plumeri

*"Whistling Theme" 
Written by Chris Lindsey

==References==
 

==External links==
* 
* 
* 

 
 
 
 