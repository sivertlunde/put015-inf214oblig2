A Man on the Beach
 
  Hammer Films. Based on a story by Victor Canning ("Chance At the Wheel") adapted by Jimmy Sangster, his first script,  the film stars Donald Wolfit, Michael Medwin and Michael Ripper.

==Synopsis==
An old lady is driven to a Mediterranean casino by a chauffeur (Ripper) in a Rolls Royce. After briefly playing at the tables without success, the lady visits the manager. While toasting her with champagne, the manager is attacked by the  old lady and his takings are stolen.

In reality, the old lady is career criminal Max (Medwin) and the chauffeur is his accomplice on his first job. Journeying into the countryside they stop for Max to change in to male clothing. The chauffeur is alarmed that Max reneges on their agreement to go half on the stolen money. Max pulls a gun on his associate, but is accidentally injured when it goes off while they fight. With the unconscious chauffeur slumped over the wheel, Max pushes the car off a cliff killing the other man.

Looking for assistance, Max stumbles across the empty house occupied by the reclusive Carter (Wolfit), a (blind) former doctor and alcoholic. Returning shortly afterwards he discovers his unwanted guest, and the two men talk and drink, though Carter does not reveal his loss of sight, or Max realsise this. He thinks Carter is unwilling to look at his bleeding arm, and uses whiskey as disinfectant on his wound. He passes out.

When he comes around the following morning, he finds Carter absent and that his bag has been examined. He assumes Carter has gone to the police. His host, on returning, asks him to leave. The bullet is no longer in his wound. Threatening Carter with being shot, Max fills in most of the remaining details of his crime because he assumes Carter is able to identify him. They struggle and Max is disarmed. As Carter is unable to find the gun, Max finally realises the other man is blind as Carters policeman friend arrives to take him fishing.

==Reputation==
A "modest featurette which scarcely justifies its credits" according to Halliwells Film and Video Guide which gives the film no stars.  David Caute in his study of Losey is equally dismissive. Viewing the film "is a misfortune - its twenty nine minutes weigh like sixty" whose "dialogue and action are equally amateurish, inconsistent, awful. Everything is spelled out, usually several times."  Wheeler Winston Dixon is more positive: "Immaculately photographed by Wilkie Cooper, this peculiar and atmospheric caper film ... offers an interesting hint as to Loseys future direction in British cinema." 

==References==
 

 
 

 
 
 
 
 
 
 