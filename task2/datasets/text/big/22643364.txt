Blood Money (1921 film)
{{Infobox film
| name           = Blood Money
| image          = 
| image_size     = 
| caption        = 
| director       = Fred Goodwins
| producer       = 
| writer         = Cecil Bullivant
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| studio         = Anglo-Hollandia
| distributor    = 
| released       = 22 February 1921
| runtime        = 
| country        = United Kingdom Netherlands
| language       = Silent
| budget         = 
}} 1921 cinema Dutch silent silent crime film directed by Fred Goodwins.

==Cast==
* Adelqui Migliar - Victor Legrand Dorothy Fane - Felice Deschanel
* Frank Dane - Sarne
* Arthur M. Cullin - Matthew Harper
* Colette Brettel - Marguerite Deschanel
* Harry Ham - Inspector Bell
* Fred Goodwins - Bruce Harper
* Harry Waghalter - Mark Harper
* Coen Hissink - Matthews bediende
* Peggy Linden

== External links ==
*  

==Comentario==
Esta película expone la cruda realidad de la industria del aborto (infanticidio prenatal). La película sigue la historia del aborto en los Estados Unidos de América a través de conmovedores testimonios de médicos y pacientes. El título alude al dinero que se mueve mediante este sangriento servicio y tiene el impactante subtítulo El valor de una vida. Una película muy recomendable para comprender uno de los fenómenos mas importantes de la sociedad occidental en el día de hoy y que a pesar de ello es muy desconocido.

 
 
 
 
 
 
 
 


 
 