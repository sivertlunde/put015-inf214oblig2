Deliverance
 
{{Infobox film
| name = Deliverance
| image = Deliverance poster.jpg
| border = yes
| alt =
| caption = Theatrical release poster by Bill Gold
| director = John Boorman
| producer = John Boorman
| screenplay = James Dickey Uncredited: John Boorman
| based on =  
| starring = Jon Voight Burt Reynolds Ned Beatty Ronny Cox
| cinematography = Vilmos Zsigmond
| editing = Tom Priestley
| studio = Elmer Productions
| distributor = Warner Bros.
| released =  
| runtime = 110 minutes
| country = United States
| language = English
| budget = $2 million
| gross = $46.1 million   
}} dramatic Thriller thriller film, 1970 Deliverance novel of the same name by American author James Dickey, who has a small role in the film as the Sheriffs in the United States|Sheriff. The screenplay was written by Dickey and an uncredited Boorman.

Widely acclaimed as a landmark picture, the film is noted both for the memorable music scene near the beginning, with one of the city men duelling on guitar with a strange country boy playing banjo, that sets the tone for what lies ahead—a trip into unknown and potentially dangerous wilderness—and for its notorious male rape scene. In 2008, Deliverance was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot==
Four Atlanta businessmen, Lewis Medlock (Burt Reynolds), Ed Gentry (Jon Voight), Bobby Trippe (Ned Beatty) and Drew Ballinger (Ronny Cox), decide to canoe down a river in the remote northern Georgia wilderness, expecting to have fun and see the glory of nature before the fictional Cahulawassee River valley is flooded by construction of a dam. Lewis, an experienced outdoorsman, is the leader. Ed is also a veteran of several trips but lacks Lewis machismo. Bobby and Drew are novices.
 raped while Ed is bound to a tree and held at gunpoint by the other man.

Hearing the commotion, Lewis sneaks up, and kills the rapist with an arrow from his recurve bow; meanwhile, the other mountain man quickly escapes into the woods. After a brief but hotheaded debate between Lewis and Drew about whether to inform the authorities, the men vote to side with Lewis recommendation to bury the dead mountain mans body and continue on as if nothing had happened — since Lewis said that they wouldnt receive a fair trial, as the local jury would be composed of the dead mans friends and relatives; likewise, Bobby doesnt want what had happened to him to be known. The four continue downriver but soon disaster strikes as the canoes reach a dangerous stretch of rapids. As Drew and Ed reach the rapids in the lead canoe, Drew shakes his head and falls forward into the river. The reason for Drews fall is left unclear.

After Drews fall into the river, the survivors canoes collide on the rocks, spilling Lewis, Bobby and Ed into the river. Lewis breaks his femur and the others are washed ashore alongside him. Encouraged by the badly injured Lewis, who believes they are being stalked by the other mountain man, Ed climbs a nearby rock face in order to dispatch the other mountain man using his bow while Bobby stays behind to look after Lewis. Ed reaches the top and hides out until the next morning, when the other mountain man appears on the top of the cliff with a rifle, looking down into the gorge where Lewis and Bobby are located. Ed clumsily shoots and manages to kill him, accidentally stabbing himself with one of his own spare arrows in the process. Ed and Bobby weigh down the mountain mans body in the river to ensure it will never be found, and repeat the same with Drews body which they encounter downriver. Upon finally reaching Aintry, they get the injured Lewis to the hospital. The men carefully concoct a cover story for the authorities about Drews death and disappearance being an accident, lying about their ordeal to Sheriff Bullard in order to escape a possible double murder charge. The sheriff clearly doesnt believe them, but has no evidence and simply tells the men never to come back. They readily agree. The trio vow to keep their story of death and survival a secret for the rest of their lives. In the final scene, Ed awakes, startled by a nightmare in which a white bloated hand rises from the lake.

==Cast==
* Jon Voight as Ed Gentry
* Burt Reynolds as Lewis Medlock
* Ned Beatty as Bobby Trippe
* Ronny Cox as Drew Ballinger
* Ed Ramey as Old Man
* Billy Redden as Lonnie
* Bill McKinney as  Mountain Man
* Herbert Cowboy Coward as Toothless Man
* James Dickey as Sheriff Bullard
* Macon McCalman as Arthur Queen

==Production== Rabun County Clayton and Georgia from the northwestern corner of South Carolina. Additional scenes were shot in Salem, South Carolina. 
 Oconee and Pickens counties in South Carolina.    

During the filming of the canoe scene, author James Dickey showed up inebriated and got into a bitter argument with producer-director John Boorman, who had rewritten Dickeys script. They had a brief fistfight in which Boormans nose was broken and four of his teeth shattered. Dickey was thrown off the set, but no charges were filed against him. The two reconciled and became good friends, and Boorman gave Dickey a cameo role as the sheriff at the end of the film.

=== Stunts ===
The film is famous for cutting costs by not insuring the production and having the actors do their own stunts (most notably, Jon Voight climbed the cliff himself). In one scene, the stunt coordinator decided that a scene showing a canoe with a dummy of Burt Reynolds in it looked phony; he said it looked "like a canoe with a dummy in it."  Reynolds begged to have the scene re-shot with himself in the canoe rather than the dummy.  After shooting the scene, Reynolds, coughing up river water and nursing a broken coccyx, asked how the scene looked.  The director responded, "like a canoe with a dummy in it."

Regarding the courage of the four main actors in the movie doing their own stunts without insurance protection, Dickey was quoted as saying all of them "had more guts than a burglar". In a nod to their stunt-performing audacity, early in the movie Lewis says, "Insurance? Ive never been insured in my life. I dont believe in insurance. Theres no risk."

===Notorious line===
Several people have been credited with the now-famous line including the phrase, "squeal like a pig." Ned Beatty said he thought of it while he and actor McKinney were improvising the scene. 
 commentary on the home media releases, the studio wanted the scene shot two ways, one of which would be acceptable for TV. Boorman did not want to do this. He decided that the phrase "squeal like a pig", suggested by Frank Rickman, a Clayton native, was a good replacement for the dialogue in the script. It would work for both the theatrical and TV versions. 

==Soundtrack and Copyright Dispute==
The films soundtrack brought new attention to the noted banjo work "Dueling Banjos", which had been recorded numerous times since 1955. Only Eric Weissberg and Steve Mandel were originally credited for the piece. Noted songwriter and producer Arthur "Guitar Boogie" Smith, who wrote the original piece, "Feudin Banjos" (1955), and recorded it with five-string banjo player Don Reno, filed a lawsuit for songwriting credit and a percentage of royalties. He was awarded both in a landmark copyright infringement case.  Smith asked Warner Bros. to include his name on the official soundtrack listing, but reportedly asked to be omitted from the movie credits because he found the film offensive. 

No credit was given for the film score. The film has a number of sparse, brooding passages of music scattered throughout, including several played on a synthesizer. Some prints of the movie omit much of this extra music. 
 The General (1998), his biographical film about Cahill.

==Reception== highest grossing film of 1972 after grossing a domestic total of over $46 million.  The films financial success continued the following year, when it went on to earn $18 million in North American Gross rental|"distributor rentals" (receipts). 

===Critical reception=== review aggregate website Rotten Tomatoes.  

Not all reviews were positive. Roger Ebert of the Chicago Sun-Times said:
 Dickey, who wrote the original novel and the screenplay, lards this plot with a lot of significance -- universal, local, whatever happens to be on the market. He is clearly under the impression that he is telling us something about the nature of man, and particularly civilized mans ability to survive primitive challenges  But I dont think it works that way.  What the movie totally fails at, however, is its attempt to make some kind of significant statement about its action.  Dickey has given us here is a fantasy about violence, not a realistic consideration of it.  Its possible to consider civilized men in a confrontation with the wilderness without throwing in rapes, cowboy-and-Indian stunts and pure exploitative sensationalism.  

The instrumental piece, "Dueling Banjos," won the 1974 Grammy Award for Best Country Instrumental Performance. The film was selected by The New York Times as one of The Best 1,000 Movies Ever Made, while the viewers of Channel 4 in the United Kingdom voted it #45 in a list of The 100 Greatest Films.

===Awards and nominations===
;Nominated
* Academy Award for Best Picture
* Academy Award for Best Director — John Boorman
* Academy Award for Best Film Editing — Tom Priestley Best Film Best Director
* Golden Globe Award for Best Motion Picture – Drama
* Golden Globe Award for Best Director – Motion Picture — John Boorman
* Golden Globe Award for Best Actor – Motion Picture Drama — Jon Voight
*   — Arthur "Guitar Boogie" Smith, Eric Weissberg, and Steve Mandel
*   — James Dickey

====American Film Institute lists====
* AFIs 100 Years...100 Movies—Nominated 
* AFIs 100 Years...100 Thrills—#15
* AFIs 100 Years...100 Movie Quotes:
** "I bet you can squeal like a pig."—Nominated 
* AFIs 100 Years...100 Movies (10th Anniversary Edition)—Nominated 

==Influence of the film ==
*Then-governor Jimmy Carter established a state film commission to encourage production companies to film in Georgia.  The state has "become one of the top five production destinations in the U.S." 

*The canoes used in the film are displayed at the Burt Reynolds Museum, located at 100 North U.S. Highway 1, in Jupiter, Florida. One of the canoes used (and signed by Ronny Cox) is on display in the Tallulah Falls Railroad Museum, Dillard, Georgia.

*Following the film, tourism increased to Rabun County by the tens of thousands. By 2012, tourism was the largest source of revenue in the county.  Jon Voights stunt double for this film, Claude Terry, later purchased equipment used in the movie from Warner Brothers. He founded what is now the oldest whitewater rafting adventure company on the Chattooga River, Southeastern Expeditions.  By 2012 rafting had developed as a $20 million industry in the region. 

*People have built vacation and second homes around the areas lakes. 

*In June 2012, Rabun County held a Chattooga River Festival to encourage preservation of the river and its environment. It noted the 40th anniversary of the filming of Deliverance in the area, which aroused controversy. 

*In 2012, producer Cory Welles and director Kevin Walker decided to make the documentary, The Deliverance of Rabun County, to explore the effects of the landmark film on people in the county. They heard a wide range of opinions, particularly resentment at how the country people were portrayed. Others are pragmatic and looking at the benefits of increased tourism and related businesses.  , Marketplace, 22 August 2012, accessed 27 August 2014 

==See also==
* Survival film, about the film genre, with a list of related films

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 