Pearls of the Crown
 

{{Infobox film
| name           = The Pearls of the Crown
| image          = 
| caption        = 
| director       = Sacha Guitry Christian-Jaque
| writer         = Sacha Guitry Christian-Jaque
| producer       = Serge Sandberg
| music          = Jean Françaix
| cinematography = Jules Kruger
| editing        = Myriam William Barache Gaumont
| runtime        = 118 minutes
| country        = France French English English Italian Italian
}}
 1937 comedy film of historically-based fiction by Sacha Guitry who plays four roles in it. (Many of the other performers play multiple roles, as well.) Guitrys Jean Martin investigates the history of seven pearls, four of which end up on the crown of England, while the other three initially go missing.

==Cast==

*Sacha Guitry as Jean Martin/Francis I of France|François I/Paul François Jean Nicolas, vicomte de Barras|Barras/Napoleon III Josephine
*Lyn Harding as Equerry (English narrator)/Henry VIII
*Renée Saint-Cyr as Madeleine de la Tour dAuvergne
*Enrico Glori as Chamberlain (Italian narrator)
*Ermete Zacconi as Pope Clement VII
*Barbara Shaw as Anne Boleyn
*Marguerite Moreno as Catherine deMedici/Empress Eugenie
*Yvette Pienne as Mary I of England/Elizabeth I of England/Queen Victoria
*Marcel Dalio as Abyssinian Minister 
*Arletty as Queen of Abyssinia
*Marcel Dalio as Abyssinian Minister  Claude Dauphin as Italian in Abyssinia
*Robert Seller as Englishman in Abyssinia
*Ponzio as Singing worker
*Andrews Engelmann as The Robber  Mary Tudor/Queen Elizabeth I/Queen Victoria
*Raimu as Industrialist
*Lisette Lanvin as Femme du monde/Reine Victoria
*Pierre Juvenet as The Expert
*Henri Crémieux as Auctioneer
*Aimé Simon-Girard as Henri IV
*Germaine Aussey as Gabrielle dEstrées
*Simone Renant as Madame Du Barry Damia as Woman of the People
*Di Mazzei as Sans-coulotte
*Jean-Louis Barrault as Young Bonaparte 
*Robert Pizani as Talleyrand 
*Émile Drain as Napoleon I (1815)
*Huguette Duflos as Queen Hortense
*Raymonde Allain as Empress Eugenie (1865)
*Violet Farebrother as An Old Lady
*Rosine Deréan as Young girl/Catherine dAragon
*Marie Leconte as A Woman in Misery
*Pierre Magnier as An Old Lord
*Cécile Sorel as A Courtesan
*Lilie Granval as Soprano
*Jean Coquelin as An Old man
*Gaston Dubosc as A Grand Duke
*Pauline Carton as Femme du Chambre
*Anaclara as La négresse Mary Stuart (girl)
*Julien Clément as Le gigolo
*Marfa dHervilly as La vieille courtisane
*Denis de Marney as Darnley
*Aline Debray 
*Dynalik as Le petit rat
*Georges Fels
*Eugénie Fougère as La vieille coquette
*Gary Garland as Une passagère sur le Normandie
*Anthony Gildès as Le vieil hongrois
*Georges Grey as Le jeune hongrois
*Geneviève Guitry
*Lautner as Le Titien
*Oléo as La petite poule
*Jacqueline Pacaud as Jane Seymour
*Annie Rozanne
*Léon Walther as Anne de Montmorency
*Laurence Atkins as Madame Tallien/Madame dEstampes 
*Jacques Berlioz Bit part 
*Humberto Catalano as Spanelli  James Craven Hans Holbein 
*Derrick De Marney as Darnley 
*Fred Duprez as An American
*Romuald Joubé as Clouet 
*Darling Légitimus
*Percy Marmont as Cardinal Wolsey 
*Paulette Élambert as Catherine de Medici (girl)

== External links ==

* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 