Fire! (1901 film)
 
{{Infobox film
| name           = Fire!
| image          = 
| caption        =  James Williamson
| producer       = James Williamson
| writer         = 
| starring       =
| cinematography = James Williamson
| editing        =
| studio         = Williamson Kinematograph Company
| distributor    =
| released       =  
| runtime        = 4 minutes 47 seconds
| country        = United Kingdom Silent
| budget         =
}}
 short silent silent drama James Williamson, showing the occupants of a house in Hove being rescued by the local fire service. This five tableaux film, according to Michael Brooke of BFI Screenonline|Screeonline, "was one of the very first films to make use of multiple shots edited together to create a chronological sequence propelling a coherent narrative along.  Williamson also creates suspense by showing the audience the extent of the fire in the first shot, which heightens the sense of urgency as the fire crew (who lack this privileged information) leave the station and rush to put it out."  It was released along with Stop Thief! (1901), "indicating the direction Williamson would take over the next few years, as he refined this new film grammar to tell stories of unprecedented narrative and emotional sophistication." "Brooke also states that it was, "a major influence on a pioneering American film, Edwin S. Porters Life of an American Fireman (1903), which borrowed Williamsons narrative model and developed it further by introducing close-ups."   
 

==Production==
The director made the film  at a derelict house called Ivy Lodge in Hove, where he had previously shot Attack on a China Mission (1900).

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 