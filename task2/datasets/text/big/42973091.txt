Love Premiere
{{Infobox film
| name =Love Premiere
| image =
| image_size =
| caption =
| director = Arthur Maria Rabenalt 
| producer = Walter Tost 
| writer =  Géza von Cziffra   Ellen Fechner    Willy Clever   Ralph Benatzky
| narrator =
| starring = Hans Söhnker   Kirsten Heiberg   Fritz Odemar   Margot Hielscher
| music = Franz Grothe   
| cinematography = Jan Roth  
| editing =  Ira Oberberg      
| studio = Terra Film
| distributor = Deutsche Filmvertriebs 
| released =  11 June 1943  
| runtime = 88 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Love Premiere (German:Liebespremiere) is a 1943 German sophisticated comedy directed by Arthur Maria Rabenalt and starring Hans Söhnker, Kirsten Heiberg and Fritz Odemar.  The films sets were designed by Robert Herlth. The film based on the stage play "Axel an der Himmelstür", that was a great success for Zarah Leander and established her marvelous career in 1936. When Zarah Leander followed her contract to the Ufa in Babelsberg in 1937, KIrsten Heiberg should replaced Zarah Leander on the stage but refused to. In 1943 originally Zarah Leander was the first choice to recreate her role on the silver screen but she left Germany in the midst of 1943. Thus Kirsten Heiberg could return to the screen after a film interdiction of two years. Herr husband Franz Grothe wrote all new songs for her and the film was a huge success. It is totally non-political and still one of the best German efforts to find an own kind of sophisticated comedy.   

==Cast==
*  Hans Söhnker as Komponist Axel Berndt  
* Kirsten Heiberg as Vera Warden, Operettensängerin  
* Fritz Odemar as Werner Rombach, Musikverleger  
* Margot Hielscher as Margit Thomas, Tänzerin, Freundin von Rombach  
* Rolf Weih as Andreas Hansen, Librettist 
* Charlott Daudert as Jenny, Veras Freundin, Frau von Rolf  
* Heinz Welzel as Rolf, Rechtsanwalt  
* Rudolf Schündler as Der Regisseur  
* Maria Litto as Irene, Mädchen bei Vera 
* Fritz Böttger as Fred, Sänger 
* Kurt Daehn as Ein Bühnenarbeiter in der Revue  
* Roswitha Knopf as Biggi, ein 5jähriges Kind, genannt "Sternchen" 
* Irmgard Petzold as Das Mädchen bei Rombach 
* Ernö René as Der Kapellmeister in der "Majestic Bar" 
* Annegret Riffel as Frau Strohmacher, Veras Garderobiere 
* Walter Ringelwald as Ein bekannter Veras, Hochzeitsgast 
* Hans Sanden as Der Kostümberater Veras 
* Walter Schenk as Der Kostümberater Veras 
* Leo Sloma as Ein Gepäckträger mit Veras Koffer 
* Walter Steinweg as Der Bühneninspizient 
* Walter Stummvoll as Der enttäuschte Autor 
* Egon Vogel as Der Aktfotograf von gegenüber 
* Alexandra Weiß as Eine gestörte Hausbewohnerin 
* Rolf Welzel as Rolf, Rechtsanwalt  
*Ursula Voß
* Erika Weigt
*  Erna Dörner-Götz

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 