Kattukuthira
{{Infobox film
| name           = Kattukuthira
| image          = 
| caption        =
| director       = P. G. Viswambharan
| producer       = A. K. K. Bappu
| writer         = S. L. Puram Sadanandan|S. L. Puram Anju Kaviyoor Innocent
| Johnson
| cinematography = Saroj Padhi
| editing        = G. Venkitaraman
| studio         = Arakkal Films
| distributor    = Pratheeksha Pictures
| released       =  
| runtime        = 112 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          = 
}}
Kattukuthira (literally: Wild Horse) is a 1990 Malayalam film. It was directed by P. G. Viswambharan and scripted by S. L. Puram Sadanandan|S. L. Puram based on his classic play of the same name.  This movie is noted for the brilliant performance of Thilakan in the role of the cantankerous Kochuvava.  This role was donned by noted actor Rajan P. Dev in the play which was also highly acclaimed. 

== Plot ==
Kochuraman (V. K. Sreeraman) is the village toddy collector with a well-built body and a strong personality. Kochuraman leads a simple and happy life with his wife and son Kochuvava. Manorama Thampuratti is the daughter of a feudal lord of the ruling family in the village. Thampuratti falls for Kochuraman and tries to gain his attention ignoring the vast communal gap between them and manage to seduce Kochuraman in to her bedroom. The feudal lord who comes to know his daughters misadventures results in Kochuraman being brutally murdered for entering into an illicit relationship with his daughter and Manorama thampuratti is paralysed for rest of her life by a deadly kick of the Thampuran in his rage. Kochuvava witnesses his fathers severed body lying in the pool of blood under a coconut tree and despite his young age he swears revenge upon his fathers murderers and grows up into a ruthless patriarch amassing huge wealth in sometimes illegal ways and ends up buying everything in the village including the kovilakam of his arch enemies. For Kochuvava (Thilakan) nothing is more important than money and revenge in his life and even his wife (Kaviyoor Ponnamma) and son (Vineeth) are secondary when it comes to that. In the blindness revenge as he bullies up everything and anything who dares to stand up to him. Despite being uneducated he manages the modernisation around him in his own ways and manages his empire in ruthless efficiency. He plans to exact his ultimate vengeance by starting an illicit arrack business from the kovilakam, to drive out the last residents of the kovilakam after which he plans to dig a pond where the kovilalkam stands today. For Kochuvava drinking a handful of water from that pond will mark his completion of his vengeance. However, this evil plan is strongly resented by his son who also happens to be in love with the younger Kochuthampuratti (Anju (actress)|Anju) of the kovilakam. The film ends in a bitter tragedy for Kochuvava who finally succumbs to a defeat in life and realises there are more things to life than money, power or revenge.

== Cast ==
 
* Thilakan as Kochuvava
* Vineeth as Mohan Anju as Latha
* Kaviyoor Ponnamma as Manka Innocent as Balakrishna Menon
* K. P. A. C. Lalitha as Kalyani
* Babu Namboothiri as Raman Nair
* Kuthiravattom Pappu as Mammukka
* V. K. Sreeraman as Kochuraman
* Jagannatha Varma as Valiya Thampuran
* Praseetha

==References==
 

 
 
 
 