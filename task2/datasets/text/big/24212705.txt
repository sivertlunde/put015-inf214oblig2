Border Vengeance
{{Infobox film
| name           = Border Vengeance
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Ray Heinz
| producer       = Willis Kent
| writer         = Forbes Parkhill
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Reb Russell Kenneth MacDonald Clarence Geldart
| music          = 
| cinematography = James Diamond
| editing        = S. Roy Luby
| studio         = Willis Kent Productions
| distributor    = Marcy Pictures Corporation
| released       =  
| runtime        = 56 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 western B movie directed by Ray Heinz, written by Forbes Parkhill based upon the novel The Return of the Muley Kid by R. Craig Christensen. The film had its premiere on June 1, 1935 and was released to theaters on June 5. {{cite book 
| title=Poverty Row Studios, 1929–1940: An Illustrated History of 55 Independent Film Companies, with a Filmography for Each
| publisher=McFarland & Co. 
| author=Michael R. Pitts
| year=2005 
| pages=211–212 
| isbn=0-7864-2319-6
}}  {{cite book 
| title=Don Millers Hollywood corral: a comprehensive B-Western roundup
| publisher=Riverwood Press 
| author=Don Miller, Packy Smith, Ed Hulse
| year=1993 
| pages=63 
| isbn=1-880756-03-X
}}  {{cite book 
| title=The thrill of it all
| publisher=Macmillan 
| author=Alan G. Barbour
| year=1971 
| pages=63 
| isbn=
}}  {{cite book 
| title=Videohounds Golden Movie Retriever 2005 
| publisher=Thomson/Gale 
| author=Jim Craddock 
| year=2005 
| pages=129 
| isbn=0-7876-7470-2}} 
 {{cite book 
| title=Sound films, 1927–1939: a United States filmography
| publisher=McFarland 
| author=Alan G. Fetrow
| year=1992 
| pages=61 
| isbn=0-89950-546-5
}}  {{cite web 
| url=http://www.archive.org/details/border_venegance 
| title=Border Vengeance (1935) 
| publisher=archive.org 
| accessdate=May 21, 2011}} 

==Background== Lafayette H. "Reb" Russell, and was filmed in 1934 by Willis Kent, an independent filmmaker known for his low budget exploitation melodramas. 

== Plot == Kenneth MacDonald) Reb Russell) shoots at Flash and hits his ear.  As a mob grows, he is able to get to his family in time to warn them so that they are able to escape across the border to safety.  Hoping to clear his familys name, Peeler decides to stay behind, and joins a traveling rodeo circuit under the name The Muley Kid.  Five years later he returns to town and is captured by Flash, who intends kill him out of vengeance for the injury to his ear.

== Cast == Reb Russell as Peeler Benson, aka The Muley Kid Kenneth MacDonald as Flash Purdue
* Clarence Geldart as Sam Griswold
* Pat Harmon as Tex Pryor
* Ben Corbett as Bud Benson
* Slim Whitaker as Posse Leader
* Mary Jane Carey as Sally Griswold
* Norman Feusier as Old Man Benson
* Marty Joyce as Young Benson
* June Brewster as June Griswold
* Hank Bell as Sheriff
* Rex Bell as Rodeo Guest Star
* Montie Montana as Trick Rider
* Glenn Strange as Cowhand

==Critical reception== padding out of the film by its inclusion of rodeo footage of former Western hero Rex Bell and horse stunt and trick rider Montie Montana "manages to drag out the 58 minutes of running time almost beyond human endurance". {{cite web 
| url=http://www.allrovi.com/movies/movie/border-vengeance-v85698?r=allmovie 
| title=Border Vengeance  Rovi 
| accessdate=May 21, 2011 
| author=Hans J. Wollstein}} 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 