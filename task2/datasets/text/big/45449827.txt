Karuppu Nila
{{Infobox film
| name           = Karuppu Nila
| image          = 
| image_size     =
| caption        = 
| director       = R. Aravindraj
| producer       = A. S. Ibrahim Rowther
| writer         = P. Kalaimani  (dialogues) 
| screenplay     = P. Kalaimani
| story          = A. S. Ibrahim Rowther
| starring       =   Deva
| cinematography = Rajarajan
| editing        = G. Jayachandran
| distributor    =
| studio         = Rowther Films
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1995 Tamil Tamil action Deva and was released on 15 January 1995. The film turned out to be hit at box office.    The title of the film was named after a song from En Aasai Machan (1994) which also starred Vijayakanth.

==Plot==

Shanmuga Pandian (Vijayakanth) is a kind-hearted person, he lives with his father Selvanayagam (P. C. Ramakrishna), mother Lakshmi (Srividya) and sister Sumathi (Meena Kumari). Selvanayagam is a respected estate owner. Shanmuga Pandian and Divya (Ranjitha) fall in love with each other.

At Sumathis wedding, Selvanayagam (P. C. Ramakrishna) is falsely arrested for suppliyng weapons to terrorists. If it turns out to be true, all his savings will be donated in the coffers of the State. Thereafter, Sumathis wedding is immediately stopped, Lakshmi becomes paralyzed and Divya refuses to marry him only because Shanmuga Pandian became poor.

Nandhini (Kushboo) hires Shanmuga Pandian in her company but her father considers Selvanayagam as his nemesis. Shanmuga Pandian and Nandhini fall in love with each other.

The conspiracy against Selvanayagam was in fact planned by the corrupt politician P.K.R (S. S. Chandran) and his son Vasu (Kazan Khan). Later, Divya gets married with Vasu but the sadistic Vasu tortures Divya. The rest of the story is how Shanmuga Pandian proves his fathers innocence and gets revenge against the villains.

==Cast==

 
*Vijayakanth as Shanmuga Pandian
*Kushboo as Nandhini
*Ranjitha as Divya
*M. N. Nambiar
*Kazan Khan as Vasu
*R. Sundarrajan (director)|R. Sundarrajan as Sundaram
*Major Sundarrajan
*S. S. Chandran as P.K.R
*Srividya as Lakshmi, Shanmuga Pandians mother
*P. C. Ramakrishna as Selvanayagam, as Shanmuga Pandians father
*Meena Kumari as Sumathi, Shanmuga Pandians sister
*Peeli Sivam
*Kallapart Natarajan
*Loose Mohan
*Kullamani as Kulla
*Pasi Narayanan
*Vellai Subbaiah
*Karuppu Subbaiah
*Reena
*Premi
*Vaijayanti
*Raviraj
*Mannangatti Subramaniam
*Swaminathan
 

==Soundtrack==

{{Infobox album |  
| Name        = Karuppu Nila
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1995
| Recorded    = 1995 Feature film soundtrack |
| Length      = 24:22
| Label       = Deva
| Reviews     = 
}}
 Vaali and Piraisoodan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Chinnavare || S. P. Balasubrahmanyam, S. Janaki || 4:35
|- 2 || Coffee Venuma || S. P. Balasubrahmanyam, K. S. Chithra || 5:11
|- 3 || Namma || Mano (singer)|Mano, K. S. Chithra || 5:10
|- 4 || Pallakku || Swarnalatha || 4:18
|- 5 || Sunda Kunji || Malaysia Vasudevan, K. S. Chithra || 5:08
|}

==References==
 

 
 
 
 
 