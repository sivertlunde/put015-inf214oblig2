Padlocked
{{Infobox film
| name           = Padlocked
| image          = 
| alt            = 
| caption        = 
| director       = Allan Dwan
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = Rex Beach Becky Gardiner James Shelley Hamilton 
| starring       = Lois Moran Noah Beery, Sr. Louise Dresser Helen Jerome Eddy Allan Simpson Florence Turner Richard Arlen
| music          = 
| cinematography = James Wong Howe
| editing        =  
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Allan Dwan and written by Rex Beach, Becky Gardiner, James Shelley Hamilton. The film stars Lois Moran, Noah Beery, Sr., Louise Dresser, Helen Jerome Eddy, Allan Simpson, Florence Turner and Richard Arlen. The film was released on August 2, 1926, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Lois Moran as Edith Gilbert
*Noah Beery, Sr. as Henry Gilbert
*Louise Dresser as Mrs. Alcott
*Helen Jerome Eddy as Belle Galloway
*Allan Simpson as Norman Van Pelt
*Florence Turner as Mrs. Gilbert
*Richard Arlen as Tubby Clark
*Charles Lane as Monte Hermann
*Douglas Fairbanks, Jr. as Sonny Galloway
*Charlotte Bird as Blanche Gallow
*Josephine Crowell as Mrs. Galloway
*André Lanoy as Lorelli
*Irma Kornelia as Pearl Gates 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 