Meet Nero Wolfe
{{Infobox Film
| name           = Meet Nero Wolfe
| image          = Meet-Nero-Wolfe_poster.jpg
| image_size     = 
| caption        = 
| director       = Herbert Biberman
| producer       = B. P. Schulberg|B.P. Schulberg
| writer         = Howard J. Green Bruce Manning Joseph Anthony Rex Stout (Fer-de-Lance (novel)|story)
| narrator       =  Edward Arnold Lionel Stander Nana Bryant Joan Perry Victor Jory
| music          = Howard Jackson
| cinematography = Henry Freulich Otto Meyer Columbia Pictures Corporation
| released       =  
| runtime        = 73 min.
| country        = United States English
| budget         = 
}}
 Edward Arnold) Archie Goodwin one more film for Columbia Pictures.

The titles of the film begin with the November 1934 issue of The American Magazine — in which the abridged version of Fer-de-Lance appeared — lying on a table. The magazine is taken from the table and opened to an illustrated spread that reads, "Edward Arnold in Meet Nero Wolfe."

==Plot==
 

At the West Hills Golf Club in Westchester, E.J. Kimball (Walter Kingsford) and his son Manuel (Russell Hardie) are welcomed into the party of elderly Professor Barstow (Boyd Irwin Sr.) and his prospective son-in-law Claude Roberts (Victor Jory). Barstow sends his caddy back to the clubhouse to fetch his visor, and finds himself without his clubs when it is his turn to tee off. The elder Kimball loans his driver to Barstow. Immediately after hitting his drive, Barstow flinches. "A mosquito bit me just as I hit the ball," he complains with good humor. "Too bad," Kimball replies sympathetically, taking the club from Barstow and making his own drive. As the foursome sets out on the course, Barstow is stricken and succumbs quickly to an apparent heart attack.
 Edward Arnold), Marie Maringola (Rita Hayworth) offers the sedentary detective genius $50 to find her brother. Although he is an expert metal worker, Carlo Maringola had such trouble finding work in America that he planned to return to the old country. On the eve of his departure Carlo told his sister that he could stay in America after all — he got a job. They had arranged a celebration but Carlo never came. He disappeared. 

Wolfe takes Marias case and sends his confidential assistant Archie Goodwin (Lionel Stander) to investigate at Carlos apartment house. Archie returns to the brownstone with evidence that suggests that Carlo will never be found alive — and that his death is linked to the death of Professor Barstow. Wolfe theorizes that Barstow was killed by a specially constructed golf club, one that was converted by Carlo into an air rifle that propelled a poisoned needle into his midsection when he struck the ball. His theory is borne out by an autopsy of Barstow, and the discovery of Carlos body.
 Frank Conroy), do little to advance the investigation. 

Far more helpful is a luncheon for the four boys who caddied for Professor Barstows foursome. Hearing their accounts, Wolfe concludes that the intended murder victim had been E.J. Kimball, not Barstow. 

Kimball dismisses the notion that his life is in danger until he is informed that his car has been wrecked and his chauffeur is dead — killed by a Bothrops atrox|fer-de-lance, a South American snake that is probably the most poisonous in the world. The autopsies of Professor Barstow and Carlo Maringola reveal that they too were poisoned, by the venom of the fer-de-lance. Convinced that his life is in deadly peril, Kimball pleads for Wolfes help.
 Monopoly on the Kimballs terrace that evening, the three men rise to go in to dinner — and shots are fired. 

The attack causes Wolfe to summon all of the principals to the brownstone. They are to spend the night, and they will stay as long as necessary.  The next evening a deadly parcel arrives, addressed to Wolfe. The killer has Wolfe in his sights — and Wolfe knows he has the killer under his roof.

==Cast==
 Edward Arnold Frank Conroy (Dr. Nathaniel Bradford), John Qualen (Olaf), Nana Bryant (Sarah Barstow), Walter Kingsford (E.J. Kimball), and Russell Hardie (Manuel Kimball) ]] Edward Arnold as Nero Wolfe, private detective Archie Goodwin, Wolfes confidential assistant
*Dennie Moore as Mazie Gray, Archies fiancée
*Victor Jory as Claude Roberts, Ellen Barstows fiancé
*Nana Bryant as Sarah Barstow, widow of Professor Barstow
*Joan Perry as Ellen Barstow, daughter of Professor Barstow
*Russell Hardie as Manuel Kimball, son of E.J. Kimball
*Walter Kingsford as E.J. Kimball, international merchant
*Boyd Irwin Sr. as Professor Barstow, college president who dies on the golf course
*John Qualen as Olaf, Wolfes Scandinavian chef
*Gene Morgan as OGrady, police lieutenant
*Rita Cansino (Rita Hayworth) as Maria Maringola, client Frank Conroy as Nathaniel Bradford, Professor Barstows doctor
*Juan Torena as Carlo Maringola, metal worker and brother of Maria Maringola
*Martha Tibbetts as the apartment house maid
*Eddy Waller as Golf Starter

==Production==
"When Columbia pictures bought the screen rights to Fer-de-Lance for $7,500 and secured the option to buy further stories in the series, it was thought the role would go to  . Connolly did portray Wolfe in the latter film, after Arnold decided he did not want to become identified in the public mind with one part. Lionel Stander portrayed Archie Goodwin. Stander was a capable actor but, as Archie, Rex thought he had been miscast." 

Meet Nero Wolfe was the second film directed by Herbert Biberman (1900–1971), a director rooted in the theater who became best known as one of the Hollywood Ten.

"Fresh from the theater, Biberman blocked shots instead of composing them," wrote Bernard F. Dick in Radical Innocence: A Critical Study of the Hollywood Ten (1989):

:Biberman opened up the action a bit, but the plot, based on Stouts Fer-de-Lance (1934), defeated him. He simply did not understand the medium; the cast reacts as it would on stage, but in film a stage reaction is overacting. Apart from some filmic touches — swish pans, dissolves, wipes, and an eerie shot of a dead mans hand clutching a newspaper clipping that another hand reaches down to retrieve — Meet Nero Wolfe is like a West End melodrama aimed at the tourist trade — slick, but so ephemeral that two days later the plot has vanished from the memory. 
 The Women.

==Reception== Archie Goodwin Edward Arnold as Nero Wolfe ]]
"A most comforting sort of detective for these humid days is Nero Wolfe, a sedentary sleuth given to drinking great quantities of homemade beer in his cool, shade-drawn brownstone and solving murder mysteries therefrom by means of remote control," wrote The New York Times (July 16, 1936):

:Mr. Wolfe is, of course, the rotund Edward Arnold, whose characterization of Rex Stouts fairly recent fictional figure presages brisk competition for such current screen master minds as Philo Vance and Perry Mason, both in matters of deduction as well as esthetically. Where Mr. Vance, for example, collects old chrysoprase and what not, Nero Wolfe grows orchids. Mr. Wolfe sets a precedent, too, in achieving something that seems not to have occurred to the other ratiocinators of the cinema. He collects huge fees.

"Its hero, less dashing than Philo Vance and less whimsical than Charlie Chan, but more mercenary than either, will be a highly acceptable addition to the screens growing corps of private operatives," wrote Time (July 27, 1936)  .

"The comedy and the guessing elements have been deftly mixed, the well-knit narrative precludes any drooping in interest and the cast disports itself in crack whodunit fashion," wrote Variety (July 22, 1936):

:In bringing the Rex Stout figment to life Arnold has contributed lots more than girth and a capacity for beer guzzling. His Nero Wolfe jells suavely with the imagination and makes a piquant example of personality conception. For seven years this corpulent sleuth, with a craving for nothing but good food and ease, has not ventured from his home. When he isnt unraveling a crime for the cash it will bring him, he gravitates between two hobbies, bottle tilting and orchid growing.

:Task of digging up evidence and following out leads for Wolfe on the outside falls to Lionel Stander. Its a typical mugg role for Stander but the performance he turns in pegs him as an important entertainment factor in the film.
 Scarlet Street magazine revisited Meet Nero Wolfe — little seen in the years after its release — and found it neither the travesty it is sometimes thought to be, nor a faithful recreation of the world of Nero Wolfe.

:Is it absurd and a "betrayal" of Stout to make Wolfes orchid room a kind of greenhouse offshoot to his office? Of course, it is, but its also a clever device that keeps the orchid-growing obsession in the action without breaking from the fairly complicated plot. Other departures — such as transforming chef Fritz Brenner into the Swedish Olaf ... are less explicable. ...

:What goes wrong — at least from a purists standpoint — is the decision to portray Wolfe as a far too jolly character. This is odd in itself, since Arnold was rarely an actor who specialized in projecting good humor.
 Monopoly — have a nice time capsule quality that has nothing to do with the Wolfe books, but have a value all their own. No, at bottom, its not Rex Stouts Nero and Archie, but its a well-developed mystery (thanks to Stouts plot) with compensations all its own — and an interesting piece of Wolfeana. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 