Big Trouble (1986 film)
{{Infobox film
| name = Big Trouble
| image = Big Trouble (1986 film).jpg
| image_size = 215px
| alt = 
| caption = Italian theatrical release poster
| director = John Cassavetes Warren Bogle
| starring = Peter Falk Alan Arkin Beverly DAngelo Charles Durning
| music = Bill Conti Bill Butler
| editing = Donn Cambern Ralph E. Winters
| studio = Delphi III Productions
| distributor = Columbia Pictures
| released =  
| runtime = 93 minutes
| country = United States
| language = English
}} The In-Laws, also featured Beverly DAngelo, Charles Durning and Valerie Curtin.

==Plot==
Leonard Hoffman is a Los Angeles insurance agent with a problem on his hands. He has teenage triplets who are all gifted musicians, but wife Arlene insists that the kids attend college at Yale, requiring more than $40,000 in tuition, rather than less expensive schools like nearby UCLA.

This situation is on Leonards mind when he pays a business call to the Beverly Hills mansion of Steve and Blanche Rickey. He is met by a flirtatious and scantily clad Blanche, who explains a problem of her own—Steve is dying, with less than a week to live, but accidentally let his life-insurance policy lapse.

A scheme is hatched involving a policy with a double-indemnity clause. Steve has to die in an unexpected fashion for this to happen, but he may or may not cooperate.

==Cast==
* Peter Falk as Steve Rickey
* Alan Arkin as Leonard Hoffman
* Beverly DAngelo as Blanche Rickey
* Charles Durning as OMara
* Robert Stack as Winslow
* Paul Dooley as Noozel
* Valerie Curtin as Arlene Hoffman
* Richard Libertini as Dr. Lopez
* Steve Alterman as Peter Hoffman
* Jerry Pavlon as Michael Hoffman
* Paul La Greca as Joshua Hoffman John Finnegan as Det. Murphy
* Karl Lukas as Police captain
* Maryedith Burrell as Gail
* Edith Fields as Doris

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 