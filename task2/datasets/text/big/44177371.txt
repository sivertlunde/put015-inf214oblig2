Nayakan (1985 film)
{{Infobox film
| name           = Nayakan
| image          =
| caption        =
| director       = Balu Kiriyath
| producer       =
| writer         = Dr. Balakrishnan
| screenplay     = Dr. Balakrishnan
| starring       = Mohanlal Viji Sankaradi Captain Raju
| music          = A. T. Ummer
| cinematography = Ashok Chowdhary
| editing        = G Venkittaraman
| studio         = Riyas Films
| distributor    = Riyas Films
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, directed by Balu Kiriyath. The film stars Mohanlal, Viji, Sankaradi and Captain Raju in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Mohanlal as Krishnadas
*Viji as Parvathi
*Sankaradi as Murari Dance Master
*Captain Raju as Rahim
*TG Ravi as Murugan
*Pattom Sadan Kunchan as Vyas/Vasu
*Jalaja as Haseena
*Vanitha Krishnachandran as Heroine
*Sathaar as Ali Abdulla Jose as Prem Shivaji as Shivaji
*James

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Balu Kiriyath. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Akaashamevide || K. J. Yesudas, CO Anto, Kannur Salim || Balu Kiriyath || 
|-
| 2 || Enthinaanee Kalla Nanam || Kannur Salim, Leena Padmanabhan || Balu Kiriyath || 
|-
| 3 || Sreedeviyaay || K. J. Yesudas, Leena Padmanabhan || Balu Kiriyath || 
|-
| 4 || Suhaasam || S Janaki || Balu Kiriyath || 
|}

==References==
 

==External links==
*  

 
 
 

 