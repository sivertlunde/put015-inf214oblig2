The White Ribbon
 
 
{{Infobox film
| name           = The White Ribbon
| image          = White ribbon.jpg
| caption        = Theatrical release poster
| director       = Michael Haneke Michael Katz Margaret Ménégoz Andrea Occhipinti
| writer         = Michael Haneke
| narrator       = Ernst Jacobi
| starring       = Christian Friedel Ulrich Tukur Josef Bierbichler
| cinematography = Christian Berger
| editing        = Monika Willi
| studio         = Wega Film X Filme
| distributor    = Filmladen   X Verleih AG  
| released       =  
| runtime        = 144 minutes   
| country        =  
| language       = German 
| budget         =  18 million 
| gross          =  19.3 million   
}} German village just before World War I and, according to Haneke, "is about the roots of evil. Whether it’s religious or political terrorism, it’s the same thing."  
 Best Cinematography (Christian Berger).

==Plot==
The memories of an unnamed elderly tailor form a parable from the distant year he worked as a village schoolteacher and met his fiancée Eva, a nanny. The setting is the fictitious Protestant village of Eichwald, German Empire|Germany, from July 1913 to 9 August 1914, where the local pastor, the doctor and the baron rule the roost over the areas women, children and peasant farmers.

The puritanical pastor leads confirmation classes and gives his pubescent children a guilty conscience over apparently small transgressions. He has them wear white ribbons as a reminder of the innocence and purity from which they have strayed. When his son confesses to impure touching,  the pastor has the boy’s hands tied to his bed frame each night. The doctor, a widower, treats the village children kindly but humiliates his housekeeper (the local midwife) and is found with his teenage daughter at night. The baron, who is the lord of the manor, underwrites harvest festivities for the villagers, many of them his farm workers. He summarily dismisses Eva for no apparent reason yet defends the integrity of a farmer whose son has destroyed the barons field of cabbages.

The schoolteachers friendship with Eva leads to an invitation to her family home during a Christmas break, and they receive permission from her parents to marry after a one-year engagement.

Unexplained events occur. A wire is stretched between two trees causing the doctor a terrible fall from his horse. The farmers wife dies at the sawmill when rotten floorboards give way; her grieving husband later hangs himself. The baron’s young son goes missing on the day of the harvest festival and is found the following morning in the sawmill, bound and badly caned. A barn at the manor burns down. The baroness tells her husband that she is in love with another man. The stewards daughter has a violent dream about the midwifes handicapped son, then the boy is attacked and almost blinded. Shortly after his daughter opens his parakeets cage with scissors in hand, the pastor finds the bird cruelly impaled. The steward at the barons estate thrashes his son for a petty theft.

The midwife commandeers a bicycle from the schoolteacher to go into town, claiming that she has evidence for the police given to her by her son. She and her son are not seen again, and the doctors family has also vacated the premises, leaving his practice closed. The schoolteachers growing suspicions lead to a confrontation in the pastors rectory, where he insinuates that the pastors children had prior knowledge of the local troubles. Offended, the pastor threatens the schoolteacher, warning that he will face legal action if he repeats his accusations.
 declaration of war on Serbia by Austria–Hungary, with the conclusion in church on the day of a visit from the narrators prospective father-in-law. Disquiet remains in the village. The narrator left Eichwald, never to return.

==Cast==
* Christian Friedel as the school teacher
* Ernst Jacobi as narrator (the school teacher many years later)
* Leonie Benesch as Eva, nanny to the baron and baronesss twin babies
* Ulrich Tukur as the baron
* Ursina Lardi as the baroness, Marie-Louise
* Fion Mutert as Sigmund, their oldest son
* Michael Kranz as Sigmunds tutor
* Burghart Klaußner as the pastor
* Steffi Kühnert as Anna, the pastors wife
* Maria-Victoria Dragus as Klara, their oldest daughter
* Leonard Proxauf as Martin, their oldest son
* Levin Henning as Adolf
* Johanna Busse as Margarete
* Thibault Sérié as Gustav
* Josef Bierbichler as the barons steward
* Gabriela Maria Schmeide as Emma, his wife
* Janina Fautz as Erna, their daughter
* Enno Trebs as Georg
* Theo Trebs as Ferdinand
* Rainer Bock as the doctor
* Roxane Duran as Anna, the doctors daughter
* Susanne Lothar as the midwife
* Eddy Grahl as Karli, her son
* Branko Samarovski as a peasant
* Birgit Minichmayr as Frieda
* Aaron Denkel as Kurti
* Detlev Buck as Evas father
* Carmen-Maja Antoni as the bathing midwife

==Production==
 , but when no co-producer who was willing to invest in the project had been found after five years had passed, Haneke decided to put the project on hold. 
Austria Presse Agentur (2009-04-23) " " (in German). Der Standard. Retrieved 2009-05-25.  CNC and the Council of Europes film fund Eurimages.  It had a total budget of around 12 million Euro. 

More than 7,000 children were interviewed during the six-month-long casting period. For most of the adult roles, Haneke selected actors with whom he had worked before and therefore knew they were suitable for the roles.  The role of the pastor was originally written for  . 38/2009. Retrieved 2009-11-23. 

Filming took place between 9 June and 4 September 2008. Locations were used in   Retrieved 2009-12-11.  and Dassow (Schloss Johannstorf).  The choice to make the film in black and white was based partly on the resemblance to photographs of the era, but also to create a distancing effect.  All scenes were originally shot in color and then altered to black and white. Christian Berger, Hanekes usual director of photography, shot the film in Super 35 using a Moviecam Compact. Before filming started, Berger studied the black-and-white films Ingmar Bergman made with Sven Nykvist as cinematographer. Haneke wanted the environments to be very dark, so many indoor scenes used only practical light sources such as oil lamps and candles. In some of the darkest scenes, where the crew had been forced to add artificial lighting, extra shadows could be removed in the digital post-production which allowed for extensive Photo manipulation|retouching.  The team in Vienna also sharpened objects and facial expressions, and modern details were removed from the images. In the dance scene, where the camera moves in 360 degrees, tiles were added frame by frame to replace the original Eternit roofs. 

==Release==
  62nd Cannes Film Festival and had its theatrical release in Austria on 25 September 2009.     In Germany, a release in selected cinemas on 17 September was followed by wide release on 15 October.  American distribution by Sony Pictures Classics began 30 December 2009. 

With a fully German cast and setting, as well as co-production by a German company, it has been discussed whether the film should be regarded as an Austrian or German production. Haneke himself has expressed indifference on the question: "In the Olympic Games the medal doesnt go to the country, but to the athlete." The general feeling is that it is primarily a Michael Haneke film. 

==Reception==

===Critical response=== Eichwald is however a common German place name, meaning the "Oak Forest".

Critics such as Claudia Puig of USA Today praised the films cinematography and performances while criticizing its "glacial pace" and "lack   the satisfaction of a resolution or catharsis."  Ann Hornaday of the Washington Post wrote that trying to locate the seeds of fascism in religious hypocrisy and authoritarianism is "a simplistic notion, disturbing not in its surprise or profundity, but in the sadistic trouble the filmmaker has taken to advance it."  Philip Maher at allmovie.com found the director "ham-handed" and "in the end his attempt at lucidity inevitably draws us further from the essential nature of fascism". 

Review aggregate Rotten Tomatoes reports that 85% of critics have given the film a positive review based on 137 reviews, with an average score of 7.6/10. 

===Accolades and awards=== international film Ecumenical Jury. Best Director and Best Screenwriter.  At the 67th annual Golden Globes, the film won the Golden Globe Award for Best Foreign Language Film.

In 2010, the film won the BBC Four World Cinema Award.   
 Best Foreign Language Film at the 82nd Academy Awards. Its submission as an 
 , pressured Germany to submit it rather than Austria because the Academy had nominated Austrian films two years running and three in a row was considered unlikely. 
 German Film Awards, including Best Picture, Best Director and Best Actor.   

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 