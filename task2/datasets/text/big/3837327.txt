The Hollywood Ten
{{Infobox film
| name           = The Hollywood Ten
| image          = Hollywoodtenimage.jpg
| image_size     =
| border         =
| caption        = Promotional image John Berry
| producer       = John Berry
| writer         = John Berry
| narrator       = Colin Chandler
| distributor    = The Criterion Collection
| released       =  
| runtime        = 15 minutes
| country        = United States
| language       = English
}}
The Hollywood Ten is a 1950 American 16mm short documentary film. In the film, each member of the Hollywood Ten made a short speech denouncing McCarthyism and the Hollywood blacklisting.
 John Berry. Berry was blacklisted upon the films release, and unable to find work, he left for France. 
 Salt of the Earth.

==Featuring==
* Herbert J. Biberman
* Lester Cole
* Edward Dmytryk
* Ring Lardner Jr.
* John Howard Lawson
* Albert Maltz
* Samuel Ornitz
* Adrian Scott
* Dalton Trumbo
* Alvah Bessie

==Release==
The Hollywood Ten was shown at the Museum of Modern Art in New York City as a part of the "Carte Blache" series. 

==References==
 

==External links==
*  
*   bios at University of California, Berkeley, Library
*  s obituary in The Philadelphia Inquirer
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 


 