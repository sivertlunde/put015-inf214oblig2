Resurrection (1918 film)
{{Infobox film
| name           = Resurrection 
| image          = 
| alt            = 
| caption        =
| director       = Edward José
| producer       = Adolph Zukor
| screenplay     = Leo Tolstoy Charles E. Whittaker
| starring       = Pauline Frederick Robert Elliott John St. Polis Jere Austin
| music          = 
| cinematography = Ned Van Buren 
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Edward José and written by Leo Tolstoy and Charles E. Whittaker. The film stars Pauline Frederick, Robert Elliott, John St. Polis and Jere Austin. The film was released on May 19, 1918, by Paramount Pictures.  

==Plot==
 

==Cast==
*Pauline Frederick as Katusha
*Robert Elliott as Prince Nekludov
*John St. Polis as Simonson 
*Jere Austin as Shenbok 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 