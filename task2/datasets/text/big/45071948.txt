Donga Ramudu (1988 film)
{{Infobox film
| name           = Donga Ramudu
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| screenplay     = K. Raghavendra Rao
| producer       = Chalasani Gopi
| director       = Kovelamudi Raghavendra Rao|K. Raghavendra Rao Radha
| Chakravarthy
| cinematography = K. S. Prakash
| editing        = D. Venkatratnam
| studio         = Gopi Art Pictures   
| released       =  
| runtime        = 136 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Radha in the lead roles and music composed by K. Chakravarthy|Chakravarthy.         

==Cast==
 
*Nandamuri Balakrishna as Ramakrishna  Radha as Ganga
*Mohan Babu as Lanka Eeswar Rao
*Siva Krishna as Inspector Dharmaraju / Jaggu Chandra Mohan as Kaki 
*Chalapathi Rao as Bhanu Prakash Kantha Rao 
*P. L. Narayana as Simhadri
*Balaji as Bhanoji
*Vidya Sagar 
*Jaya Bhaskar 
*Pandari Bai as Rajyalakshmi
*Malashri as Durga
*Kuaili as Rani 
*Y.Vijaya as Chitrangi Devi
 

==Soundtrack==
{{Infobox album
| Name        = Donga Ramudu
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1988
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:40
| Label       = LEO Audio Chakravarthy
| Reviews     =
| Last album  = Inspector Pratap   (1988)  
| This album  = Donga Ramudu   (1988)
| Next album  = Tiragabadda Telugubidda   (1988)
}}

Music composed by K. Chakravarthy|Chakravarthy. Lyrics written by Jonnavithhula Ramalingeswara Rao. Music released on Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Ammamma Ammamma SP Balu, P. Susheela
|3:39
|- 2
|Cheyyi Veyyi Nadum SP Balu,P. Susheela
|4:23
|- 3
|Loveliga Unnave SP Balu,P. Susheela
|4:31
|- 4
|Asale Kasi Kasi SP Balu, S. Janaki
|4:42
|- 5
|Taku Chiku Taku Chiku  SP Balu,P. Mano
|4:06
|- 6
|Yamma Kottudu SP Balu,P. Susheela
|4:19
|}

==Others== Hyderabad

==References==
 

 
 
 
 
 


 