Bhai-Bhai
{{Infobox film
| name           = Bhai-Bhai
| image          = 
| image_size     = 
| caption        = 
| director       = Raja Nawathe
| producer       = Ratan Mohan
| writer         = 
| narrator       =  Mehmood Pran Pran Aruna Irani
| music          = Shankar-Jaikishan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = R.M. Arts Production
| released       = 
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1970 Bollywood drama film directed by Raja Nawathe. The film stars Sunil Dutt, Asha Parekh and Mumtaz (actress)|Mumtaz.

==Cast==
*Sunil Dutt ...  Ashok / Deep (double role)
*Asha Parekh ...  Taaj Mumtaz ...  Bijli
*Mehmood Ali|Mehmood...  Johnny Pran
*Jeevan
*Raj Mehra
*Aruna Irani
*Madan Puri
*Mukri
*Manmohan Krishna
*Iftekhar
*Mohan Choti Sunder
*Asit Asit Sen

==Soundtrack==
{| class="wikitable"
! Serial !! Song Title !! Singer(s)
|- 1 || "Aaj Raat Hai Jawaan" || Asha Bhosle
|- 2 || "Ek Tera Sundar Mukhda" || Mohammed Rafi
|- 3 || "Main Hoon Jaani Tera" || Mohammed Rafi
|- 4 || "Main To Kajra Lagake" || Lata Mangeshkar
|- 5 || "Mere Mehboob" || Mohammed Rafi
|- 6 || "Sapera Been Baja" || Lata Mangeshkar
|-
|}
 

==References==
 

==External links==
*  

 
 
 
 
 


 
 