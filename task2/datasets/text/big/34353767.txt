Kuro Arirang
 
 
{{Infobox film name           = Kuro Arirang image          = File:Kuro_Arirang_poster.jpg
| film name =   rr             = Guro Arirang mr             = Kuro Arirang}} director       = Park Jong-won producer       = Park Chong-chan writer         =  Lee Ha-yeong starring       = Ok So-ri Lee Geung-young  Choi Min-sik music          =  Kim Young-dong cinematography = Chung Kwang-suk editing        = Ree Kyoung-ja distributor    =  released       =   runtime        = 119 minutes country        = South Korea language       = Korean budget         =  gross          = 
}}
Kuro Arirang ( ) is a 1989 South Korean film by Park Jong-won which stars Ok So-ri and Lee Geung-young and marks the debut of Choi Min-sik and Shin Eun-kyung. The film caused a stir over labor issues.

==Plot==
The film depicts the story of four people working in a sewing factory in Guro Industrial Complex in Seoul and the problems they face.

==Cast==
* Ok So-ri
* Lee Geung-young
*Yoon Ye-ryeong
* Choi Min-sik
*Lee Ki-yeol
*Kim Na-young
* Shin Eun-kyung
*Lee Min-gyeong
*Lee Gwang-hui
*Kim Ui-sang

==External links==
*  
*  

 
 
 
 


 