The L.A. Riot Spectacular
{{Infobox film| 
name=The L.A. Riot Spectacular|
image=|
director=Marc Klasfeld|
writer=Marc Klasfeld|
genre=Comedy|
starring=Snoop Dogg Emilio Estevez T.K. Carter Charles S. Dutton|
distributor=Image Entertainment|
released= |
runtime=80 minutes|
country=United States|
language=English|
}}

The L.A. Riot Spectacular is a satire film about the 1992 Los Angeles riots. Written and directed by music video director Marc Klasfeld, the film stars Emilio Estevez, Snoop Dogg, Charles S. Dutton, Charles Durning, Christopher McDonald, Michael Buffer, Ted Levine, Jonathan Lipnicki and Ron Jeremy.   

==Cast==
{| class="wikitable"
! Actor/Actress
! Role
|-
| Michael Barrymore || The Narrator
|-
| T.K. Carter || Rodney
|- Mayor Tom Bradley
|-
| Emilio Estevez || Officer Powell
|- George Hamilton || The King of Beverly Hills
|-
| Charles Durning || The Lawyer
|-
| Christopher McDonald || Officer Koon
|-
| Jonathan Lipnicki || Tom Saltine Jr.
|-
| Ted Levine || Tom Saltine
|- Chief Daryl Gates
|- William Forsythe || George Holliday
|-
|}

==References==
 

==External links==
* 

 
 
 
 
 


 