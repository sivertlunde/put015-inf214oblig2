Boat Trip (film)
 
{{Infobox film
| name           = Boat Trip
| image          = Boat_Trip_movie.jpg
| caption        = Film poster
| director       = Mort Nathan
| producer       = Sabine Müller Frank Hübner Brad Krevoy Gerhard Schmidt Andrew Sugerman
| writer         = Mort Nathan William Bigelow Maurice Godin
| music          = Robert Folk
| cinematography = Shawn Maurer
| editing        = John Axness
| distributor    = Artisan Entertainment Nordisk Film Motion Picture Corporation of America
| released       =  
| runtime        = 94 minutes
| country        = United States Germany
| language       = English
| budget         = $20 million   
| gross          = $15 million 
}}
Boat Trip is a 2002 American romantic comedy film directed by Mort Nathan in his feature film directorial debut, and starring Cuba Gooding Jr., Horatio Sanz, Vivica A. Fox, Roselyn Sánchez and Roger Moore. The film was released in the United States on March 21, 2003.

== Plot ==
Jerry and Nick are two best buddies whose love lives have hit rock bottom. After Nick runs into a friend who is getting married to a beautiful, younger girl he met on a singles cruise, he decides to take a similar cruise with Jerry. Things do not go as planned though, after a vengeful travel agent books them on a cruise for gay men.

During their trip, they come to learn that gay men are less objectionable than they first assumed and Nick especially sheds his homophobia. However, Jerry falls in love with the cruises dance instructor Gabriella and in order to win her over, he pretends to be gay so he can get closer to her. Meanwhile, Nick blossoms a romance with a bikini model named Inga. After an accidental affair with her mean, sex-obsessed coach Sonya, Nick must fend her off, after she has fallen in love with him as well.

In the end, Jerry wins Gabriella while Nick loses out on Inga but sees a potential relationship with her sister instead. However, he is then unwittingly (and unwillingly) reunited with Sonya; much to his dismay and her Sexual arousal|arousal.

== Cast ==
* Cuba Gooding Jr. – Jerry Robinson
* Horatio Sanz – Nick Ragoni
* Roselyn Sánchez – Gabriella
* Vivica A. Fox – Felicia Maurice Godin – Hector
* Roger Moore – Lloyd Faversham
* Lin Shaye – Coach Sonya
* Victoria Silvstedt – Inga Ken Campbell – Tom
* Zen Gesner – Ron
* William Bumiller – Steven
* Noah York – Perry
* Richard Roundtree – Felicias Dad
* Bob Gunton – Boat Captain
* Jennifer Gareis – Sheri
* Will Ferrell – Michael, Brians boyfriend
* Artie Lange - Brian

== Release ==
Boat Trip was released in the UK on October 4, 2002,  and in the US and Canada on March 21, 2003, where the film opened at #10 and grossed $3,815,075 in 1715 theatres.  In total, it had a worldwide gross of $15,020,293.   It was released on DVD in the US on September 30, 2003. 

== Reception == Oscars telecast for starring in this movie after receiving an Academy Award.  Many viewed the film as homophobic although a reviewer for The Advocate wrote that the film was too terrible to protest.  On the show Ebert and Roeper, Roger Ebert said the film "was so bad in so many different ways, not only does it offend gays, it offends everyone else." His co-host Richard Roeper said, "If the ship hit an iceberg, I would have been rooting for the iceberg."  Overall, it was nominated for two Razzie Awards for Gooding as Worst Actor and for Mort Nathan as Worst Director, but lost to Gigli. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 