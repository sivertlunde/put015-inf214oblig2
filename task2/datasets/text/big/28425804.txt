Take Off (film)
 
{{Infobox film
| name           = Take Off
| image          = Take Off film.jpg
| film name      = {{Film name
| hangul         =  
| hanja          =  
| rr             = Gukgadaepyo
| mr             = Kukkataep‘yo}}
| director       = Kim Yong-hwa
| producer       = Park Mu-seung Bang Chu-sung
| writer         = Kim Yong-hwa  Kim Ji-seok Choi Jae-hwan Lee Jae-eung Sung Dong-il
| music          = Lee Jae-hak
| cinematography = Park Hyun-cheol
| editing        = Park Gok-ji Jeong Jin-hee
| distributor    = Showbox/Mediaplex
| released       =  
| runtime        = 137 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}} most attended film of the year in South Korea with 8,392,953 admissions. 

==Plot== IOC opted for Salt Lake City over Koreas Muju County. Unfortunately, because of deep fog, Chil-gu injures his leg and becomes unable to compete. Bong-gu decides to jump as a substitute but does not make the required distance for a gold medal and nearly loses his life. Despite their loss, the athletes rejoice because Bong-gu survived the jump, and the Koreans back home are proud of them.

==Cast==
*Ha Jung-woo - Cha Heon-tae/Bob
*Kim Dong-wook - Choi Hong-cheol  Kim Ji-seok - Kang Chil-gu
*Choi Jae-hwan - Ma Jae-bok
*Lee Jae-eung - Ma Bong-gu
*Sung Dong-il - Coach Bang
*Lee Eun-sung - Su-yeon
*Lee Hye-sook - Bobs birth mother
*Lee Se-rang - Middle-aged woman from Yeonbyun, China Juni - Young woman from Yanbian, China
*Lee Han-wi - Company president Ma
*Kim Yong-gun - Chairman of the organizing committee
*Hwang Ha-na - Ji-eun (Bobs younger sister)
*Seo Min-yi - 3 year old Ji-eun Kim Ji-young - Bong-gus grandmother
*Oh Kwang-rok - Pharmacist
*Kim Su-ro - Loan shark boss
*Jo Seok-hyeon - Employee at Military Manpower Administration
*Park Seong-taek - Japanese broadcaster
*Kim Sung-joo - Korean broadcaster
*Cho Jin-woong - Korean broadcaster 2
*Lee Seol-ah - Hye-ra
*Jung Min-sung - Classifieds journalist

==Relevance==
Korea is new to the venue of ski jumping, and there were only five members of the national team, so this event is not well known to the Korean people. Film director Kim Yong-hwa made this movie to introduce the ski jumping event to Koreans, in order to pique their interest and therefore improve national support for the event. In order to do that, he cast top actor Ha Jung-woo. Kim also introduced the background on the players and the environment in which they practiced. This was the first Olympics in which the Korean ski jump team competed, so they did not receive much financing. Therefore, they had to practice in a bad training area. Despite this, they managed to attend the Olympics. 

==References==
 

==External links==
*    
*   at Naver  
*  
*  
*  

 
 
 