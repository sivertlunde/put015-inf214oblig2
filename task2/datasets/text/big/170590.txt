Forgotten Silver
 
 
{{Infobox film
| name           = Forgotten Silver
| image_size     = 
| image	=	Forgotten Silver FilmPoster.jpeg
| caption        = Forgotten Silver poster
| director       = Peter Jackson Costa Botes
| producer       = Sue Rogers
| writer         = Peter Jackson Costa Botes
| narrator       = 
| starring       = Peter Jackson Costa Botes Thomas Robins Sam Neill Harvey Weinstein
| music          = 
| cinematography = 
| editing        = Michael J. Horton
| studio         = WingNut Films
| distributor    = 
| released       = 28 October 1995 (TV premiere)
| runtime        = 53 min.
| country        = New Zealand English
| budget         = 
| gross          = $26,459
| preceded_by    = 
| followed_by    = 
}}

Forgotten Silver (1995) is a New Zealand mockumentary film that purports to tell the story of a pioneering New Zealand filmmaker. It was written and directed by Peter Jackson and Costa Botes, both of whom appear in the film in their roles as makers of the documentary.

==Synopsis==
Forgotten Silver purports to tell the story of forgotten New Zealand filmmaker Colin McKenzie, and the rediscovery of his lost films, which presenter Peter Jackson claims to have found in an old shed. McKenzie is presented as the first and greatest innovator of modern cinema, single-handedly inventing the tracking shot (by accident), the close-up (unintentionally), and both sound and color film years before their historically documented creation. The film also shows fragments of an epic Biblical film supposedly made by McKenzie in a giant set in the forests of New Zealand, and a computer enhancement of a McKenzie film providing clear evidence that New Zealander Richard Pearse was the first man to invent a powered aircraft, several months prior to the Wright Brothers.
 John OShea, as well as critical praise from international industry notables including film historian Leonard Maltin, and Harvey Weinstein of Miramax Films.

In reality, McKenzie is a fictional character, and the films featured in Forgotten Silver were all created by Peter Jackson, carefully mimicking the style of  .

==Cast==
As themselves: Jeffrey Thomas (narrator)
* Peter Jackson
* Johnny Morris
* Costa Botes
* Harvey Weinstein
* Leonard Maltin
* Sam Neill John OShea
* Marguerite Hurst
* Lindsay Shelton
* Davina Whitehouse

Actors:
* Thomas Robins - Colin McKenzie
* Richard Shirtcliffe - Brooke McKenzie
* Beatrice Ashton - Hannah McKenzie
* Peter Corrigan - Stan Wilson/Stan the Man
* Sarah McLeod - May Belle

== Production ==
Costa Botes directed the documentary portions while Peter Jackson created the archive footage supposedly filmed by McKenzie. Jackson also shot fake interviews in Los Angeles, including the one with Weinstein.

== Reception == TV ONE plays and mini-series, but was billed and introduced as a serious documentary. A large proportion of the TV audience were fooled until the directors shortly afterwards revealed that it was a hoax. The airing proved extremely controversial.   The film was subsequently screened at a number of film festivals.

== References ==
 

== Literature==
* Conrich, Ian/Smith, Roy (2006): Fools Gold: New Zealands Forgotten Silver, Myth and National Identity. In: Rhodes, Gary Don/Springer, John Parris (eds.) (2006): Docufictions. Essays on the intersection of documentary and fictional filmmaking. Jefferson, NC: McFarland, p.&nbsp;230-236.
* Roscoe, Jane/Hight, Craig (1997): Mocking silver: Reiinventing the documentary project (or, Grierson lies bleeding). In: Continuum: Journal of Media & Cultural Studies, 11:1, p.&nbsp;67–82 ( ; Article provides background information and an analysis of audience responses to the film)
* Roscoe, Jane/Hight, Craig (2006): Forgotten Silver: A New Zealand Television Hoax and Its Audience. In: Alexandra Juhasz|Juhasz, Alexandra/Lerner, Jesse (eds.) (2006): F is for Phony. Fake Documentary and Truth’s Undoing. Minneapolis: University of Minnesota Press, p.&nbsp;171-186.

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 