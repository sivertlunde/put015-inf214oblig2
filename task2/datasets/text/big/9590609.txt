While She Was Out
{{Infobox film
| name           = While She Was Out
| image          = Shewasoutposter.jpg
| caption        = Theatrical release poster
| director       = Susan Montford
| producer       = Mary Aloe   Don Murphy   Kirk Shaw
| writer         = Susan Montford (screenplay)   Edward Bryant (short story)
| starring       = Kim Basinger   Lukas Haas
| music          = Paul Haslinger
| cinematography = 
| editing        = William M. Anderson
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 88 minutes
| country        = United States Germany Canada
| language       = English
| budget         = 
| gross          = 
}} 2008 American thriller horror film starring Kim Basinger and Lukas Haas.  Basinger plays a suburban housewife who is forced to fend for herself when she becomes stranded in a desolate forest with four murderous thugs.  It was written and directed by film producer Susan Montford based on a short story by Edward Bryant. The film was produced by Mary Aloe and Don Murphy. Its executive producers included Guillermo del Toro and Basinger. The film was shot in 2006 and had a very limited release in 5 theaters in Texas during 2008.

==Plot==
On Christmas Eve, suburban housewife Della Myers (Kim Basinger) gets into an argument with her abusive husband Kenneth (Craig Sheffer). After putting her two children to bed, she drives to the mall to buy some wrapping paper and cards.  At the mall, she cant find a parking space for a while and angrily leaves a note on the window of a car that is parked using up two parking spaces. She leaves the store as the mall is closing, and the parking lot is nearly deserted. She notices the note is gone from the offending car. As she enters her own car, the car on which she had left the note pulls up behind her.
 Jamie Starr), Vingh (Leonard Wu), and Tomás (Luis Chávez)—led by Chuckie (Lukas Haas). They threaten to rape her. Della insults Chuckie, and a security guard intervenes, but he is shot dead by Chuckie. As the gang realizes that they have committed a murder, Della manages to start her car and drive away. They follow her, intending to kill her, as she is the only witness. As they pursue her some distance, she eventually crashes her car in a deserted area where homes are under construction. She takes a road flare and a toolbox out of her car and hides behind a back hoe. 

Della runs through the buildings under construction to hide as they search for her. After some time, the thugs corner and threaten her by name, as they had found her drivers license in her purse in her car. As they have her open the toolbox, she hits Chuckie with a crescent wrench, and escapes again into some nearby woods. In the process of chasing her, Tomás steps on Huey, who falls and breaks his neck.

After some hide and seek in the woods, Della beats and finally kills Tomás with a lug wrench. She flees through a creek, pursued by Chuckie and Vingh. Della sneaks up on Vingh and kills him with a screwdriver and hides behind a fallen tree. Chuckie tries to persuade Della to give up; he talks about her kids, saying that he is going to pay them a visit. He tells her what he thinks of her, that she lives a boring life she doesnt want, mistreated by her husband. He finds her, touches, and teases her face. She holds his hand, pulls him down and they kiss. He draws his weapon as they engage in foreplay. She tells him to have sex with her, and as he is distracted she ignites the road flare and blinds him, takes his weapon, and kills him. 

Della returns home. Her husband Kenneth complains she was out late and is tracking mud throughout the house, but Della ignores him. She goes upstairs to check on her children who are both sleeping. The drunk Kenneth asks what she brought him from the mall, and she replies, "Nothing", and points the gun at him.

==Cast==
*Kim Basinger as Della
*Lukas Haas	as Chuckie
*Craig Sheffer as Kenneth
*Jamie Starr as Huey
*Leonard Wu as Vingh
*Luis Chávez as Tomás
*Luke Gair as Terri
*Erika-Shaye Gair as Tammi

==Release and reception== Anchor Bay. directly to video in the United Kingdom.

While She Was Out has an overall approval rating of 31% on Rotten Tomatoes based on 16 reviews.   According to the L.A. Weekly, it is a "surprisingly enjoyable female revenge tale", describing Basingers performance as "first-rate" despite "a laughably check-listed, multi-culti band of thugs".  The New York Observer called it "ultra-feminist fun" with a "spectacular" ending.  Aint It Cool News called Basingers performance "her best in years".   Bitch (magazine)|Bitch magazine dismissed much about the film, saying it "has an ample amount of cheezy genre conventions, problems with pacing, a gaggle of silly villains, huge plot holes and bad production values" but added that it is "really rather fascinating – and notable – as a horror/thriller that actually gives a damn about the female character it puts in harms way."  The L.A. Times said it "eschews all plot and character development for the hackneyed action scenes and grade-Z dialogue",  and Filmcritic.coms review said that its "dialogue is somewhere between kindergarten and film school." 

==References==
 

==External links==
* 

 
 
 
 
 
 
 