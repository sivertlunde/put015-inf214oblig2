Twentieth Century (film)
{{Infobox film
| name           = Twentieth Century
| image          = TWENTIETH-CENTURY-post1.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Howard Hawks
| producer       = Howard Hawks
| writer         = Unproduced play: Charles Bruce Millholland Play and screenplay:   Preston Sturges
| starring       = John Barrymore Carole Lombard Howard Jackson Louis Silvers Harry M. Woods
| cinematography = Joseph H. August
| editing        = Gene Havlick
| distributor    = Columbia Pictures
| released       =   In New York, the film opened at Radio City Music Hall. |1934|05|11|U.S.}}
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Broadway Twentieth play of the same name    – itself based on the unproduced play Napoleon of Broadway by Charles Bruce Millholland    – with uncredited contributions from Gene Fowler and Preston Sturges.

Along with   of the Library of Congress in 2011.

==Plot== Broadway impresario Oscar Jaffe (Barrymore) takes an unknown lingerie model named Mildred Plotka (Lombard) and makes her the star of his latest play, despite the grave misgivings of everyone else, including his two long-suffering assistants, accountant Oliver Webb and the consistently tipsy Owen OMalley. Through intensive training, Oscar transforms his protegée into the actress "Lily Garland", and both she and the play are resounding successes. Over the next three years, their partnership spawns three more smash hits, and Lily is recognized as a transcendent talent.
 tapping her telephone. When she finds out, it is the last straw; she leaves for Hollywood and soon becomes a big movie star.
 New York Citys Grand Central Terminal. By chance, Lily Garland boards the train at a later stop with her boyfriend George Smith. After prevaricating, Oscar sees a chance to restore his fortunes and salvage his relationship with Lily.
 Judas strangle himself with her hair." Then Oliver thinks he has found somebody to finance Oscars project, fellow passenger Mathew J. Clark, not realizing that Clark is a harmless escapee from a mental asylum. When Oscar is slightly wounded in a scuffle with Clark, he pretends to be dying and gets a distraught Lily to sign his contract. The film ends with their first rehearsal, where Oscar reverts to his usual domineering self.

==Cast==
*John Barrymore as Oscar "O.J." Jaffe
*Carole Lombard as Lily Garland/Mildred Plotka
*Walter Connolly as Oliver Webb
*Roscoe Karns as Owen OMalley
*Ralph Forbes as George Smith Charles Lane as Max Jacobs
*Etienne Girardot as Mathew J. Clark Dale Fuller as Sadie, Lilys maid
*Edgar Kennedy as Oscar McGonigle
*Herman Bing as Beard #1
*Lee Kohlmar as Beard #2 James Burke as sheriff at train station
*James P. Burtis as Train conductor Fred "Snowflake" Toones as train porter

Cast notes: Twentieth Century to appear in the film.   Girardot had a long career as a character actor in both silent and talking films, appearing in 76 films altogether. 
* Charles Lane is billed under his real name, Charles Levison (he did not change to "Lane" until 1936).
* Billie Seward is tenth-billed as "Anita", but some viewers have wondered who she is. Her role was shortened before release; in the finished film, she has only one brief scene complaining to the conductor about Clark.

==Production==
The genesis of Twentieth Century was Napoleon of Broadway, a play by Charles Bruce Millholland about his experiences in working for the legendary and eccentric Broadway producer David Belasco.   His play was not produced, but it became the basis for the Hecht-MacArthur comedy, which lasted for 152 performances on Broadway, beginning on December 29, 1932,  and which they later adapted for the big screen.

Howard Hawks was not the first choice; Roy Del Ruth and Lewis Milestone had been set to direct before Hawks got the job. Columbia tried to get William Frawley from the Broadway cast, but instead borrowed Roscoe Karns from Paramount Pictures|Paramount. TCM   

Before Lombard was cast, Columbia boss Harry Cohn negotiated with Eugenie Leontovich, who had played the part on Broadway, and then considered Gloria Swanson and Miriam Hopkins. Other reports say that Cohn also approached Ina Claire, Tallulah Bankhead, Ruth Chatterton, Constance Bennett, Ann Harding, Kay Francis and Joan Crawford. However, Hawks believed that Lombard was a brilliant actress who had yet to be unleashed on film. He convinced a reluctant Columbia to borrow her from Paramount Studios.

During Barrymores initial reading with her, he looked to Hawks with an expression that showed he did not believe in Hawks intuition. The rest of the production went dryly, with Lombard staggering through one scene after another and playing the same stoic characters that she had been taught to portray. Hawks took her aside and asked her what she was being paid for the film. Lombard told him and Hawks asked her what she would do if a man said "something" about her, coming up with an example from the back of his mind. Lombard said, "I would kick him in the balls." Hawks said, "Well, Barrymore said that, so why dont you kick him?" Of course Barrymore had said nothing of the sort, but the plan worked and after Lombard yelped a few profanities, she continued through the shoot with an unforgettable vigor. For the rest of her career, before beginning a film, Lombard would always send a telegram to Hawks saying, "Im going to kick him!"

Lombard and Barrymore became friends during filming. When Barrymores career was declining, Lombard raised hell to get him to work on her film True Confession (1937).

Preston Sturges was hired to write the screenplay around late November 1933, but was removed from the project a week later because he hadnt made sufficient progress.  Columbia then tried to get Herman Mankiewicz to write it, with Felix Young to produce. 

Twentieth Century – a title which Columbia considered changing because they feared that many westerners would not be familiar with the name of the train  – was in production from February 22 to March 24, 1934. 

During the filming, there were some problems with the censors at the Hays Office, who were concerned about the religious angle in the comedy of the film, and requested that it be toned down.  Joseph Breen, who ran the Office, worried that "there will be serious difficulty in inducing an anti-Semitic public to accept a   play produced by an industry believed to be Jewish in which the Passion Play is used for comedy purposes."  The Office ultimately asked that one line be removed, which it was. They also requested that it be made less clear where Oscar jabs Lily with a pin. 
 New York on May 3, 1934  and went into general release on May 11. 

== Reception == New York Times review, Mordaunt Hall wrote, "John Barrymore is in fine fettle in Twentieth Century" and "acts with such imagination and zest that he never fails to keep the picture thoroughly alive."    All of the principal actors - Lombard ("gives an able portrayal"), Connolly ("excellent") and Karns ("adds bright flashes"), as well as Girardot ("an asset") - were also praised. However, Hall was less enthused about the comedy style, stating, "it seems a pity that they   were tempted to stray occasionally too far from the realm of restrained comedy and indulge their fancy for boisterous humor." 

The movies box office performance was described as "dismal". 

TIME said "Twentieth Century is good fun, slick, wild and improbable." 

In December 2011, Twentieth Century was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in the National Film Registry.    In its induction, the Registry said that the "sophisticated farce about the tempestuous romance of an egocentric impresario and the star he creates did not fare well on its release, but has come to be recognized as one of the era’s finest film comedies, one that gave John Barrymore his last great film role and Carole Lombard her first." 

==Adaptations== Broadway for 460 performances,  and was revived for a special benefit performance in 2005.   It will receive its first full-scale Broadway revival beginning February, 2015, with Peter Gallagher and Kristin Chenoweth in the lead roles.

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 