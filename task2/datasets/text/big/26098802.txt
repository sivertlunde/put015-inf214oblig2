Nightkill
{{Infobox film
| name           = Nightkill
| image          = Nightkill.jpg
| image_size     =
| caption        =
| director       = Ted Post
| writer         = Joan Andre (screenplay) John Case (story)
| narrator       =
| starring       = Robert Mitchum
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 97 min.
| country        = West Germany
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}} 1980 thriller film directed by Ted Post. It stars Robert Mitchum and Jaclyn Smith.  It is about an adulteress who hatches a plot to murder her millionaire husband while her lover assumes his identity. 

==Cast==
*Robert Mitchum	... 	Donner
*Jaclyn Smith	... 	Katherine Atwell
*James Franciscus	... 	Steve Fuller
*Mike Connors	... 	Wendell Atwell
*Fritz Weaver	... 	Herbert Childs
*Sybil Danning	... 	Monika Childs
*Tina Menard	... 	Consuela
*Belinda Mayne	... 	Christine
*Angus Scrimm	... 	Desert rat (scenes deleted)
*Michael Anderson Jr.	... 	Lt. Donner
*Melanie MacQueen	... 	Alice

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 