Kothigalu Saar Kothigalu
{{Infobox film
| name = Kothigalu Saar Kothigalu
| image = 
| caption = 
| director = Rajendra Singh Babu
| writer = Rajendra Singh Babu
| producer = Jai Jagadish   Vijayalakshmi Singh   Dushyanth Singh   Srinidhi Mohan Prema Prema Tara Tara  Urvashi
| music = Hamsalekha
| language = Kannada
| country = India
| cinematography = B. C. Gowrishankar
| editing        = B. S. Kemparaju
| studio         = Lakshmi Creations
| released       = 28 December 2001
| runtime        = 157 mins
}}
 Kannada comedy Tara and Urvashi in the lead roles.  This is the second film in the "Saar" series directed by Babu and was released on 28 December 2001 and received generally positive reviews from the critics and turned out to be box-office hit.  The film was remade in Telugu as Sandade Sandadi and in Hindi as Shaadi No. 1.

The film won the Filmfare Award for Best Film - Kannada for the year 2002.  Ramesh Aravind nominated for Filmfare Best Actor.

==Cast==
*Ramesh Aravind as Romy
*S. Narayan as Nani Mohan as Mohan Prema
*Urvashi Urvashi  Tara
*Umashree 
*Mukhyamantri Chandru
*Rangayana Raghu
*Mandya Ramesh

==Soundtrack==
{{Infobox album  
| Name        = Kothigalu Saar Kothigalu
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = Kothigalu Saar Kothigalu audio.jpeg
| Caption     = Soundtrack cover
| Released    =  2001
| Recorded    = Feature film soundtrack
| Length      = 
| Label       = Akash Audio
}}

Hamsalekha composed the music for the film and soundtracks, also writing the lyrics for all the soundtracks.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Lyrics
|-
| 1 || Kothigalu Saar || Hemanth  || Hamsalekha
|-
| 2 || Maduveya Nanthara  || Chetan Sosca || Hamsalekha
|-
| 3 || Baare Rajakumari || Hemanth, Badri Prasad, Shankar Shanbag, Lata Hamsalekha || Hamsalekha
|- Nanditha || Hamsalekha
|-
| 5 || Oye Oye Namde Zamana || Chetan Sosca, Manjula Gururaj || Hamsalekha
|-
| 6 || Bondana Dummina || B. Jayashree, Malgudi Subha, Radhika || Hamsalekha
|-
| 7 || Jeena Yahan || Ramesh Chandra || Hamsalekha
|-
| 8 || Naanu Yaava || Ramesh Chandra || Hamsalekha
|-
|}

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 