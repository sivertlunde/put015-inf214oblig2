The Wizard of Oz (1933 film)
{{Infobox film name           = The Wizard of Oz  image          =  image_size     =  caption        =  producer       = J.R. Booth Ted Eshbaugh Carl W. Stalling	  director       = Ted Eshbaugh writer         = novel L. Frank Baum writer Frank Joslyn Baum|Col. Frank Baum starring       =  music          = Carl W. Stalling cinematography =  editing        =  studio         = Ted Eshbaugh Studios distributor    = Film Laboratories of Canada released       = 1933 runtime        = 9 minutes country        =   Canada language  English
|imdb           = 
}}
The Wizard of Oz (1933 in film|1933) is an animated short film directed by Ted Eshbaugh.  The story is credited to "Col. Frank Baum."  Frank Joslyn Baum, a Lieutenant Colonel in the United States Army and eldest son of writer L. Frank Baum, was involved in the films production, and may have had an involvement in the films script, which is loosely inspired by the elder Baums novel, The Wonderful Wizard of Oz.  It runs approximately eight and a half minutes and is nearly absent of language, working mainly with arrangements of classical music created by Carl W. Stalling.

The film was originally made in Technicolor, but because it was made without proper licensing from the Technicolor Corporation, it was released in black and white after a lawsuit forbade the release of the film in color.  

==Plot summary== Dorothy and Toto (dog)|Toto. The two tumble into Oz, landing on the Scarecrow (Oz)|Scarecrow. After freeing him from his pole, the trio stroll together, soon finding a Tin Woodman and oiling him.
 The Swan", they are welcomed into the Emerald City. Suits of armor sing to them, "Hail to the Wizard of Oz! To the Wizard of Oz we lead the way!" A creature resembling the A-B-Sea Serpent of The Royal Book of Oz extends itself as stair steps for Dorothy to enter the coach.

The Wizard is a cackling white-bearded man in a starry black robe and conical hat who produces custom seats for each of the four nervous travelers, including one for Toto (the Toto chair is mostly cut out of the frame in most video versions, but is later shown in a full shot of Toto sitting). He proceeds to perform magic with a hen and eggs. These are variations on simple sleight of hand tricks involving making objects appear, but the hen is able to take the eggs back into her body.

Finally, the hen releases an egg that will not stop growing. The five try to fight it, with the Tin Woodman breaking his axe. Soon, though, the egg hatches, the hen takes the chick, and clucks out "Rock-a-bye Baby" as a chorus joins her. The five laugh, and the film ends on an iris-in of mother and child.

==Home video==
There are many home media releases of the film, including Betamax, VHS, Laserdisc, Capacitance Electronic Disc|CED, DVD, HD DVD and Blu-ray Disc, usually printed and shown in black-and-white, not technicolor. Cassette copies are usually at slow speeds, and often overexposed and poorly framed, while disc copies are all at faster speeds, and underexposed and correctly framed. The first known commercial release was in Canada in 1985, on Betamax, VHS and Laserdisc, through the efforts of Fred M. Meyer, longtime Secretary of The International Wizard of Oz Club. This is not an original color print, but has been colored to match the original intent of the filmmakers, which, as in the MGM film that followed, had the film go from black and white to color upon Dorothys arrival in Oz. Cassette copies also contained a stray hair during the parade sequence, while the LaserDisc copies did not.
 1939 feature film of the same name, while a brief 2-minute clip is included on the 1999 DVD release and as a bonus on the 1993 Ultimate Oz VHS and LaserDisc deluxe release.

The short was released again in 2010 as part of Mill Creek Entertainments DVD compilation, 200 Classic Cartoons .

Thunderbean Animation has restored and remastered the cartoon, releasing it on Blu-Ray and DVD as part of their Technicolor Dreams and Black and White Nightmares compilation, due for release in 2014.

==External links==
*  


 
 
 
 