Human Highway
 
{{Infobox film
| name           = Human Highway
| image          = Humanhighwayfilm.jpg
| caption        = VHS cover
| director       = Bernard Shakey, Dean Stockwell
| producer       = Elliot Rabinowitz, L.A. Johnson, Jeanne Field
| writer         = Bernard Shakey, Jeanne Field, Dean Stockwell, Russ Tamblyn, James Beshears
| narrator       =
| starring       = Neil Young, Dean Stockwell, Russ Tamblyn, Dennis Hopper, Devo
| music          = Neil Young, Devo David Myers
| editing        = James Beshears
| distributor    = WEA
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Human Highway is a 1982 American comedy film starring and co-directed by Neil Young under his pseudonym Bernard Shakey.  Dean Stockwell co-directed the film and acted along with Russ Tamblyn, Dennis Hopper, and the band Devo. Included is a collaborative performance of "Hey Hey, My My (Into the Black)" by Devo and Young with Booji Boy singing lead vocals and Young playing lead guitar.

The film was shown in only select theaters and was not released on VHS until 1995. It received poor reviews upon its premiere Jimmy McDonough, Shakey, Anchor Books, 2002, p.575-7  but has received favorable reviews more recently. Tom Keogh   at IMDB Retrieved September 1, 2007 

==Plot==
Employees and customers spend time at a small gas station-diner in a fictional town next to a nuclear power plant unaware it is the last day on Earth. Young Otto (Dean Stockwell) has received ownership of the failing business by the Will of his recently deceased father. His employee, Lionel Switch (Neil Young), is the garages goofy and bumbling auto mechanic who dreams of being a rock star. "I can do it!" Lionel often exclaims. After some modest character development and a collage-like dream sequence there is a tongue-in-cheek choreographed musical finale while nuclear war begins.

At the destroyed gas station-diner post nuclear holocaust Booji Boy (Mark Mothersbaugh) is a lone survivor, but after his cynical prose  the opening credits are a return to present time prior to apocalypse.

At the nuclear power plant nuclear garbage persons (members of Devo) reveal that radioactive waste is routinely mishandled and dumped at the nearby town of Linear Valley. They sing a remake of "Worried Man Blues" while loading waste barrels on an old truck. Meanwhile, Lionel and his buddy Fred Kelly (Russ Tamblyn) ride bicycles to work. Fred states that Old Ottos recent death was by radiation poisoning. They remain unaware of the implications as Lionel laments it should have been himself that died because he has worked on "almost every radiator in every car in town."
 The End of the World". Later, waitress Irene (Geraldine Baron) overhears Young Ottos plans to fire everybody, destroy the buildings and collect on a fraud insurance claim. Irene demands to be included in the scheme and to seal the deal with a kiss. 
 David Blue). After an earthquake Duke, dressed in white, enters the diner with a delivery. He flirts with her saying, "Charlotte ...on my way over here this morning I thought about you and the earth moved." She replies, "You felt it too!" He also offers her a milk bath. While he is there a dining Arab sheik offers him wealth in return for his "whiteness."
 wooden Indian in his shop, "Now theres a real human being!"

Lionel receives a bump on the head while working on Frankies limousine and enters a dream. He becomes a rock star with a back up band of wooden Indians. Back stage he is given a milk bath by Irene. Lionel travels with his band (the wooden Indians) and crew (all people from his waking life) by trucks through the desert. The wooden Indians become missing.

During "Goin Back" (a song by Young) the entourage recreates in the desert near a Pueblo. Native Americans prepare a bon fire to burn the wooden Indians which had been missing. Soon Lionel is playing music and dancing around the bon fire which appears to have become the center of a Pow-wow. "Goin Back" ends gazing into the bonfire of burning wooden Indians. "Hey, Hey, My, My" is a ten minute studio jam performance of Devo and Young.
 The Wizard of Oz. Soon there is the start of global nuclear war. No one is sure what is happening until it is announced by Booji Boy, as "the hour of sleep." He then provides shovels and commands everyone to "dig that hole and dance like a mole!" The cast then enters a choreographed adaptation of "Worried Man". The planet is engulfed in radioactive glow and the cast, still festive, climbs a stairway to heaven accompanied by harp music.
 David Blue) flirt. Blue died shortly before the films release.]]

==Cast== David Blue who was cast as the milkman, "Earl Duke". 

==Production==
Over four years Young spent $3,000,000 of his own money on production.  Filming began in 1978 in San Francisco and Taos, New Mexico. It was resumed in 1981 on the Hollywood soundstages of Raleigh Studios. The set which included the diner and gas station was built to Youngs specific requests. His initial idea was to portray a day in the life of Lionel and bystanders during the Earths last day. The actors were to develop their own characters. Jimmy McDonough, Shakey, Anchor Books, 2002, p.528-9  The script was a combination of improvisation and developing small story lines as they went. Young, Stockwell and Tamblyn were central in the writing.  

Dennis Hopper who played the addled cook was performing knife tricks with a real knife on the set. Sally Kirkland attempted to take a knife from him and severed a tendon. She spent time in a hospital and later sued claiming Hopper was out of control. Hopper has admitted to drug abuse during this time period. 

For Devo it was their first experience with Hollywood. Gerald Casale said the band felt removed observing the odd behaviors including excessive alcohol and drug abuse and rock star adulation with Young as the central "most grounded" person. 

{{listen
 | filename     = Cafe interlude.ogg
 | title        = Diner interlude scene.
 | description  = This song from a diner scene exemplifies Mark Mothersbaughs early film scoring.
 | format       = Ogg
 | filename2    = Wrench song.ogg
 | title2       = The mechanics musical wrench set.
 | description2 = Neil Young performing music while in character as Lionel accompanied by film score.
 | format2      = Ogg
 | filename3    = Hey Hey My My.ogg
 | title3       = "Hey Hey My My"
 | description3 = A collaboration uniquely combining Youngs pre-grunge sound with Devos post-punk electronics.
 | format3      = Ogg
}} Crazy Horse. Guitarist Frank Sampedro has said they played "Hey Hey My My" "harder" as a result. 

==Credits==
Editing and post-production supervision is credited to James Beshears (Madagascar, Shark Tale, Shrek). The screen play is credited to Bernard Shakey, Jeanne Field, Dean Stockwell, Russ Tamblyn and Beshears. Film credits  The members of Devo were asked to write their own parts.   Don Zulaica, liveDaily.com, April 25, 2001 Retrieved September 5, 2007  Choreography is credited to Tamblyn. Music is credited to Neil Young and Devo.  The films score was the first by  .  

==Release==
  as "Lionel Switch" riding a stationary bike against a surreal backdrop, an example of the films "hyper-real sets." ]]

===Critical reception===
Human Highway is commonly reviewed as a "bizarre" comedy.   Following  the films official premiere in Los Angeles, June 1983, it was shown only briefly in a small number of theaters. It received poor reviews by critics  and confused audiences. Mark Demming   Allmovie|allmovie.com  After the films release to VHS in 1996 it has since received more favorable reviews such as being described by TV Guide as "goofy and enjoyable" and Youngs acting as "surprisingly funny." It was also suggested by TV Guide that the film would have done well on the midnight circuit that existed at the time of its initial release.   TV Guide. Retrieved September 2, 2010.  Yet, a critic of  The Wizard John Waters. 

===Home video=== WEA in 1995, twelve years after its initial screening. It has not been re-released, or transferred to DVD as of May 2012.  It was a different edit than the theatrical release. The dream sequence originally featured a concert version of  "Ride My Llama," while the 1995 version replaced it with the album version of "Goin Back." Several scenes from the movie appeared on the Devo music video collections Were All Devo and The Complete Truth About Devolution, and were edited to appear as one continuous video for the song "Worried Man."

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 