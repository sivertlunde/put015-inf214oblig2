Lot in Sodom
{{Infobox film
| name           = Lot in Sodom
| image          =
| caption        =
| director       = James Sibley Watson Melville Webber
| producer       =
| writer         =
| starring       = Friedrich Haak Hildegarde Watson Dorothea Haus Lewis Whitbeck
| music          = Louis Siegel
| conductor      =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 28 min
| country        = United States
| awards         = English intertitles
| budget         =
}} short silent silent experimental film, based on the Biblical tale of the city of Sodom and Gomorrah. It was directed by James Sibley Watson and Melville Webber.
 
The movie uses experimental techniques, avant-garde imagery and strong allusions to Human sexuality|sexuality, especially homosexuality.

Louis Siegel was the sound composer, according to the films opening credits.

==Storyline== Sodom and Gomorrah.

Sodom is a place of sin. An angel appears there and he is welcomed by Lot (Bible)|Lot. The people of Sodom want to have sex with him. Lot refuses; then the angel tells him to escape the city with his wife and daughter. Sodom is then destroyed by the flames; Lots wife is turned to a pillar of salt for having looked back.

All intertitles are quotes from the Bible.

==Cast==
* Friedrich Haak as Lot
* Hildegarde Watson as Lots wife
* Dorothea Haus as Lots daughter
* Lewis Whitbeck as the angel

==See also== Sodom und Gomorrha (1922 in film|1922) - an Austrian film directed by Michael Curtiz Sodom and Gomorrah (1963 in film|1963) - a film directed by Robert Aldrich which depicts the destruction of the two cities for their decadence and human cruelty.
*Nitrate Kisses (1992) - an experimental film by Barbara Hammer that uses footage from Lot in Sodom.

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 


 