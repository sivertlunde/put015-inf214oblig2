Matilda (1996 film)
 
{{Infobox film
| name           = Matilda
| image          = Matildaposter.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Danny DeVito
| producer       = {{Plainlist|
* Danny DeVito
* Michael Shamberg
* Stacey Sher
* Liccy Dahl }}
| screenplay     = {{Plainlist|
* Nicholas Kazan
* Robin Swicord }}
| based on       =  
| narrator       = Danny DeVito
| starring       = {{Plainlist|
* Mara Wilson
* Danny DeVito
* Rhea Perlman
* Embeth Davidtz
* Pam Ferris }} David Newman
| cinematography = Stefan Czapsky
| editing        = {{Plainlist|
* Lynzee Klingman
* Brent White }} Jersey Films
| distributor    = TriStar Pictures
| released       =  
| runtime        = 98 minutes  
| country        = United States
| language       = English
| budget         = $36,000,000    
| gross          = $33,098,222     (in the U.S.) $62,098,222  (Worldwide)
}}
 fantasy comedy comedy childrens novel of the same name. The film was released by TriStar Pictures on August 2, 1996 and stars Mara Wilson, Danny DeVito, Rhea Perlman, Embeth Davidtz, and Pam Ferris.

==Plot==
Matilda Wormwood (Mara Wilson) is an intelligent girl with a bright personality, but her parents, Harry and Zinnia (Danny DeVito and Rhea Perlman), neglect and mistreat her. When Matilda reaches four, she discovers the local library and walks there every day to read while her parents are at work and her older brother, Michael, is at school.

By age six-and-a-half, Matilda begins to lose patience with her parents. In retaliation for her fathers constant lecturing, she mixes his hair tonic with her mothers hair dye which turns his hair an unhealthy blonde. Harry takes his kids to his workshop, where he reveals that the cars he sells are faulty. Matilda accuses him of being dishonest and he belittles her, so she retaliates by putting super-super-glue in his hat, forcing Zinnia to cut it off. Matilda reads a borrowed library book of Moby Dick which Harry then tears it, calling it filth, reacting to the title while her family is watching television. When Harry tries to force her to watch with them, Matilda grows increasingly angry and the television suddenly explodes.

Agatha Trunchbull (Pam Ferris) is the headmistress of a run-down school, Crunchem Hall. Harry enrolls Matilda there, where she befriends several children and learns of Miss Trunchbulls nature and her harsh punishments of the students. Matildas teacher, Miss Jennifer Honey (Embeth Davidtz), is a kind woman who adores her pupils and takes an immediate liking to Matilda. Miss Honey talks to Miss Trunchbull and requests that Matilda be moved up to a higher class, but Trunchbull refuses. Miss Honey pays Matildas parents a visit and requests that they pay more attention to her, but they refuse to listen. Meanwhile, Matilda discovers that her family is under surveillance by FBI agents (Paul Reubens and Tracey Walter) due to her fathers illegal dealings, but her parents refuse to believe her.

Sometime later, Miss Trunchbull goes to Miss Honeys class for a weekly "check-up" and starts to belittle the students. As a prank, Lavender (one of Matildas friends) places a newt in Miss Trunchbulls water jug to frighten her. She accuses Matilda, whose anger at the injustice leads to her telekinetically tipping the glass over, splashing water and the newt on Miss Trunchbull. Miss Honey invites Matilda to her house for tea. On the way, they pass Miss Trunchbulls house, and Miss Honey reveals her secret; when she was two years old, her mother died, so her father invited his wifes stepsister, Miss Trunchbull, to live with them and look after Miss Honey. However, Miss Trunchbull mistreated and abused her niece at every opportunity. When Miss Honey was five, her father died of an apparent suicide. Eventually, Miss Honey moved out of her aunts house and into a small cottage. Matilda and Miss Honey briefly sneak into Miss Trunchbulls house while she is out, but her unexpected return leads to a cat-and-mouse chase with Matilda and Miss Honey only barely escaping.

When Matildas telekinetic powers manifest again during an argument with Harry, she trains herself to use her ability at her own will. Matilda returns to Miss Trunchbulls house, wreaking havoc in an attempt to scare her away, like repeatedly striking her clock, opening the windows, and turning the lights on and off. Trunchbull almost flees, but she finds Matildas ribbon and realizes that she was there. The next day, Miss Trunchbull visits Miss Honeys class again to get Matilda to admit her guilt. Matilda uses her powers to write a message on the blackboard, posing as the ghost of Miss Honeys father Magnus and accusing her of murdering him. Miss Trunchbull attacks the students, but Matilda keeps them out of harms way with her powers and the students force Miss Trunchbull out of the school. Miss Honey moves back into her fathers house.

The FBI finally uncovers enough evidence to prosecute Harry, and he and his family prepare to flee to Guam. They stop by Miss Honeys house to pick up Matilda, but she refuses to go with them. In that moment, Zinnia laments on her guilt and regret in not understanding Matilda better. She and Harry sign the adoption papers that would allow Miss Honey to adopt Matilda. The Wormwoods escape, while Matilda lives a happy life with Miss Honey, who, in addition to her teaching duties, also becomes the schools new principal.

==Cast==
* Mara Wilson as Matilda Wormwood, the main protagonist.
** Alissa and Amanda Graham, Trevor and James Gallagher as Matilda - newborn
** Kayla and Kelsey Fredericks as Matilda - 9 months
** Amanda and Caitlin Fein as Matilda - toddler
** Sara Magdalin as Matilda - 4 years
* Danny DeVito as Harry Wormwood, Matildas father.
** DeVito also narrates the film.
* Rhea Perlman as Zinnia Wormwood, Matildas mother.
* Embeth Davidtz as Miss Jennifer Honey, Matildas class teacher and the niece of Ms. Trunchbull. She later becomes Matildas adopted mother.
** Amanda and Kristyn Summers as Jennifer Honey - 2 years
** Phoebe Pearl as Jennifer Honey - 5 years Agatha Trunchbull, the principal of school Matilda attends and the main antagonist.  
* Brian Levinson as Michael Wormwood, Matildas brother.
** Nicholas Cox as Michael - 6 years
* Paul Reubens as FBI Agent Bob
* Tracey Walter as FBI Agent Bill
* Kiami Davael as Lavender 
* Jacqueline Steiger as Amanda Thripp
* Kira Spencer Hesser as Hortensia
* Jimmy Karz as Bruce Bogtrotter
* Jean Speegle Howard as Mrs. Phelps
* Marion Dugan as Cookie
* Emily Eby as Maggie
* Craig Lamar Traylor as Child in Classroom
* Jon Lovitz as Mickey, host of The Million Dollar Sticky.

==Awards and nominations==
;Wins
* YoungStar Award
** Best Performance by a Young Actress in a Comedy Film &mdash; Mara Wilson Cinekid Lion Audience Award
** Best Director &mdash; Danny DeVito Oulu International Childrens Film Festival Starboy Award
** Best Director &mdash; Danny DeVito

;Nominations
* Satellite Awards
** Best Performance by an Actor in a Supporting Role in a Motion Picture &mdash; Comedy or Musical (Danny DeVito)
* Young Artist Award
** Best Performance in a Feature Film &mdash; Leading Young Actress (Mara Wilson)
** Best Performance in a Feature Film &mdash; Supporting Young Actress (Kira Spencer Hesser)

The film was submitted for an Academy Award nomination for Best Original Musical or Comedy Score, but wasnt nominated.

==Music== montage of Matilda and Miss Honey playing at Miss Trunchbulls former house. The other song is Thurston Harriss "Little Bitty Pretty One", played when Matilda is learning to control her psychokinetic powers.
 David Newman.

==Reception==
Matilda received critical acclaim at the time of its release. On Rotten Tomatoes, it holds a "fresh" rating of 90%.  In the United States, the film earned $33 million in contrast to its $36 million budget.     It fared better during its worldwide release and ended up earning back nearly double its original budget  as well as on home video and television.

==Home media release==
In 2013 Mara Wilson and her former co-stars from Matilda had a reunion to celebrate the movies 17th anniversary and Matilda being released on Blu-ray.  The reunion scene was featured in the Blu-ray of Matilda. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 