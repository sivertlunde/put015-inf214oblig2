Song of the West
{{Infobox film name = Song of the West image = SongoftheWest6.jpg producer  =  director = Ray Enright  writer = Harvey F. Thew based on the Broadway Operetta by Oscar Hammerstein II and Laurence Stallings starring = John Boles Joe E. Brown music = Harry Akst  Grant Clarke  Oscar Hammerstein II and Vincent Youmans cinematography = Devereaux Jennings   (Technicolor) editing = George Marks distributor = Warner Bros. released =   runtime = 82 minutes language = English country = United States
|}}
 John Boles, Joe E. Brown and Vivienne Segal, and was the first all-color all-talking feature to be filmed entirely outdoors.

==Plot==
The story takes place in 1849. Captain Stanton (John Boles), who because of a misunderstanding over a woman with Major Davolo, has been cited for a court martial. As a scout, he is sent to escort a wagon train which is under military escort. It turns out that this escort is his own former regiment. When he meet Davolo, there is another fight and between Stanton and Davolo in which Davolo is killed. 

The colonel has Stanton put in the guard house on a murder charge. He escapes disguised as a parson and continues along with the wagon train in order to be near Virginia, the daughter of his former commander, played by Vivienne Segal. They fall in love and when Stanton decides to leave the wagon train, Virginia follows him. 

Stanton marries Virginia and opens a gambling hall. When the regiment eventually turns up at the gambling hall, Virginia makes merry with her former friends. Stanton, in a fit of jealousy, leaves the establishment with another woman and tries his luck in California, searching for gold. He has poor luck and becomes a derelict. Eventually he meets his wife in San Francisco, resulting in a happy reconciliation. Some soldiers find him and give him a choice between being deported or re-enlisting in the army. He re-enlists. Joe E. Brown, in the part of Hasty, his doomed sidekick, provided the comedy for the film.

==Production==
This was  Boless follow-up to his successful role in The Desert Song (1929). The film was finished in June 1929. Following a number of dismal previews, however, Warner Bros. shortened the film by two reels, removing some of the musical content in the process. In spite of being delayed for almost a year before release, the film had a worldwide gross of $920,000 and the featured songs were quite popular, leading RCA Victor to hire Boles, who was then at the height of his popularity, to record "The One Girl" and "West Wind" for commercial release.

==Songs ==
All written by Vincent Youmans and Oscar Hammerstein II unless indicated
*"The One Girl" (sung by Boles)
*"I Like You As You Are" (sung by Boles)
*"Come Back to Me" (sung by Boles and Vivienne Segal) (by Harry Akst and Grant Clarke)
*"The Bride Was Dressed in White" (sung by Joe E. Brown)
*"West Wind" (sung by John Boles) (music by Vincent Youmans, lyric by J. Russell Robinson)
*"Kingdom Coming" (sung by chorus) (authors unknown)

==Cast (in credits order)==
  John Boles as Captain Stanton
*Vivienne Segal as Virginia Joe E. Brown as Hasty
*Marie Wells as Lotta Sam Hardy as Davolo
*Marion Byron as Penny
*Eddie Gribbon as Sergeant Major
*Edward Martindel as Colonel
*Rudolph Cameron as Lt. Singleton

==Preservation==
 
 known to exist. The complete soundtrack survives on Vitaphone disks. Warner Bros. records of the films negative have a notation, "Junked 12/27/48" (i.e., December 27, 1948). Warner Bros. destroyed many of its negatives in the late 1940s and 1950s due to nitrate film pre-1933 decomposition. No prints of the film are known to currently exist, though rumors that private collectors who own foreign prints have continued to surface as late as 1999.
When in February 1956, Jack Warner sold the rights to all of his pre-December 1949 films; (inclusing Song of the West) to Associated Artists Productions (which merged with United Artists Television in 1958, and later was subsequently acquired by Turner Broadcasting System in early 1986 as part of a failed takeover of MGM/UA by Ted Turner).
In a June 2011 forum discussion, a small fragment, running about a minute, was claimed to have been discovered in the UK and identified as being from the film. 

==See also==
*List of lost films
*List of incomplete or partially lost films
*List of early color feature films

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 