American Crime (film)
 
 
{{Infobox film
| name           = American Crime
| image          = American Crime DVD cover.jpg
| image size     =
| caption        =
| director       = Dan Mintz
| producer       =
| writer         = Jack Moore Jeff Ritchie
| narrator       = Michael ONeill Kip Pardue Frankie Ray
| music          = Kurt Oldman
| cinematography = Dan Mintz
| editing        = Todd E. Miller
| distributor    =
| released       = 2004
| runtime        = 96 min
| country        = United States English
| budget         =
}}
 thriller movie Michael ONeill, Kip Pardue and Frankie Ray. It is directed by Dan Mintz and produced by Jeff Ritchie.

The film is presented in a documentary program style, taking off the style of series such as The FBI Files and The Young Detectives, both shows showcasing stories on crimes in United States|America.

==Plot==

After two strip club workers disappear and are found dead later on, a team of reporters unearth a mysterious tape which shows the women being followed and later murdered by the camera man. After investigating the cases further, the reporters on the team become targeted by the murderer and have other chilling tapes sent to them. The team are joined by the host of the hit television show American Crime, who assists them on their journey to uncover the true murderer, as a man was already sent to jail for the killings. With a disbelieving sheriff ignoring the case, the reporters must solve the crime themselves before they themselves are killed.

==External links==
* 

 
 
 
 


 