Na Svatém Kopečku
{{Infobox film
| name           = Na Svatém Kopečku
| image          =Na Svatém Kopecku.jpg
| alt            = 
| caption        = 
| film name      = 
| director       =Miroslav Cikán
| producer       = 
| writer         = 
| screenplay     = Václav Wasserman, Karel Melísek, Jaroslav Mottl	
| story          = 	
| based on       = Based on the operetta Na Svatém Kopečku by Jiří Balda and Rudolf Nížkovský 	
| starring       =  Jaroslav Vojta, Marie Grossová, and Jiřina Steimarová
| narrator       = 
| music          = Antonín Vlastislav Vipler 	
| cinematography = Jaroslav Blažek 	
| editing        = 
| studio         = Lepka
| distributor    = 
| released       =1934  
| runtime        =87 minutes
| country        =Czechoslovakia
| language       = 
| budget         = 
| gross          = 
}}

Na Svatém Kopečku is a 1934 Czechoslovak musical comedy film, directed by Miroslav Cikán. It stars  Jaroslav Vojta, Marie Grossová, and Jirina Steimarová.  

==Cast==
*Jaroslav Vojta as Holeèek, statkáø
*Marie Grossová as Mája Malínská
*Jirina Steimarová as Heda
*Valentin Sindler as Matej Krópal from Brochovany
*Rudolf Lampa 		
*Svetla Svozilová as Bìta
*Ladislav Pesek 			
*Jindrich Plachta as Jakub
*Zdenka Baldová as Králová
*Václav Trégl as Portýr
*Jaroslav Bráska 	
*Josef Kotalík 	 		
*Bohumil Mottl 		

==References==
 

==External links==
*  at the Internet Movie Database

 
 
 
 
 
 
 
 