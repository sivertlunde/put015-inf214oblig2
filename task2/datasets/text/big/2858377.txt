City of Glass (film)
 
 
{{Infobox film
| name           = City of Glass 
| image          = 
| image size     = 
| caption        = 
| director       = Mabel Cheung
| producer       = 
| writer         =  Mabel Cheung Alex Law
| narrator       = 
| starring       = Leon Lai   Shu Qi   Nicola Cheung   Daniel Wu   Eason Chan   Pauline Yam   Vincent Kok
| music          = Ricky Ho
| cinematography = Jingle Ma Arthur Wong
| editing        = Maurice Li
| distributor    = 
| released       =  
| runtime        = 110 min.
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$ 9 million
| preceded by    = 
| followed by    = 
}} 1998 Cinema Hong Kong film, directed by Mabel Cheung.

==Cast==
* Nicola Cheung - Suzie
* Daniel Wu - David
* Leon Lai - Raphael
* Audrey Mak - Tiger Sing
* Shu Qi - Vivian
* Vincent Kok
* Pauline Yam
* Eason Chan
* Wong Wah-Wo - Taxi Driver
* Joe Cheung
* Elaine Jin
* Henry Fong

 
 

==Summary==

On New Years Day 1997, a car accident in London, England claims the lives of Raphael (Lai) and Vivian (Shu). The couple was once young lovers during their days at the University of Hong Kong in the 1970s, but had drifted apart and eventually ended up marrying other people and raising their own families.  However, they reunited in the 1990s and their love partially rekindled. After their funeral, Raphaels son David and Vivians daughter Suzie learned of their parents affair and embark on a journey to discover their secret lives. In the end, the two fall in love.

==Reception==
The film was deemed a modest commercial success, grossing HK$9 million in its 1998 Hong Kong theatrical release.   Critical commentary perceived the film as a metaphorical comment on the end of British rule in Hong Kong after the handover to Chinese sovereignty in 1997.  

==Awards== Hong Kong Golden Horse Awards, winning for original screenplay, sound, editing, and cinematography, as well as the audience award. 

==References==
 

==External links==
* 
*  

 
 
 
 
 


 
 