Mother Goose Rock 'n' Rhyme
{{Infobox film
| name = Mother Goose: Rock n Rhyme
| image = 
| image_size = 
| border = 
| alt = 
| caption = 
| director = Jeff Stein Thomas A. Bliss
| writer = Mark Curtiss
| story = Linda Engelsiepen Harry Hinkle
| starring = Shelley Duvall Jean Stapleton Dan Gilroy Cyndi Lauper Debbie Harry Bobby Brown Little Richard Woody Harrelson Paul Simon Art Garfunkel Garry Shandling Teri Garr
| music = David Michael Frank
| cinematography = Tony Mitchell
| editing = Christopher Willoughby
| distributor = Lyrick Studios (1998 VHS version only)
| released =  
| runtime = 96 minutes
| country = United States
| language = English
}} musical television film that aired on the Disney Channel starring Shelley Duvall as Little Bo Peep and Dan Gilroy as Gordon Goose, the son of Mother Goose along with a star-studded supporting cast of various other actors and musicians portraying a wide range of characters, mostly of Mother Goose nursery rhyme fame. It was released for the first time on VHS in March 1998 with the help of Lyrick Studios.

==Plot==
The film deals with the events surrounding Gordon Goose and Little Bo Peep, who, while still trying to find her sheep, goes to Mother Gooses house for help, only to discover her sudden absence. Bo Peep and Gordon search Rhymeland to flush out what has happened to Mother Goose, all the while watching as many Mother Goose characters begin to mysteriously disappear.

==Cast of characters==
The film features an all-star cast including:
* Shelley Duvall as Little Bo Peep
* Jean Stapleton as Mother Goose
* Dan Gilroy as Gordon Goose
* Cyndi Lauper as Mary (of Mary Had a Little Lamb)
* Debbie Harry as The Old Woman Who Lived in a Shoe
* Bobby Brown as Three Blind Mice
* Little Richard as Old King Cole
* Woody Harrelson as Lou the Lamb (of Mary Had a Little Lamb) Simple Simon
* Art Garfunkel as Georgie Porgie Jack and Jill) Jack and Jill)
* Harry Anderson as Peter Piper
* David Leisure as The Newscaster / The Game Show Host Stephen Kearney The Crooked Man and his Dog/ Happy 1 and Happy 2
* Howie Mandel as Humpty Dumpty
* John Santucci as All the Kings Men (of Humpty Dumpty)
* Cheech Marin as The Carnival Barker
* Van Dyke Parks as The Minister of Merriment
* Brian Bonsall as Michael
* Katey Sagal as Mary, Mary, Quite Contrary
* Elayne Boosler as Old Mother Hubbard
* Stray Cats (Brian Setzer, Lee Rocker, and Slim Jim Phantom) as Georgie Porgies House Band
* Ben Vereen as The Itsy Bitsy Spider Joe Hill, Frank Beard) The Three Men in a Tub
* Pia Zadora as Little Miss Muffet

;Special guest appearances Vance Colvig, Jr.
* Paul Daniels
* The Del Rubio Triplets|Edith, Elena, and Milly Del Rubio metal band musicians in dungeon scene
* Jennifer Evans
* Seymour Heller

== Changes for Rock N Rhymeland (TV Version) ==

Several edits and changes were made to the audio, musical score, and video for the TV edit. A non-comprehensive list of some of these variations appears below:
* Parts of the film play in a different order:
:* "Mary Had a Little Lamb"
:* "Little Miss Muffet"
:* "Jack & Jill"
* Several scenes were completely cut:
:* Mary Quite Contrary complaining about her garden
:* Old Mother Hubbard and her diner
:* Gordon rearranging his wardrobe
:* Gordons remark about summer vacation
:* Mary and her lamb disappearing after Little Bo Peep and Gordon leave
:* The Old Woman in the shoe telling her many children "wait till your fathers get home," implying she has had many partners
:* Introductory speech for Old King Cole
* Onscreen lyrics during the "Gordon, Wont You Come Out and Play" dungeon metal band scene are featured with a "bouncing ball" format to follow along.
* Different musical score during the Crooked Man Chase, the Itsy Bitsy Spider scenes, the Little Miss Muffet scene, and the Cow Jumped Over the Moon scenes
* Alternative music mix for the Del Rubio Triplets opening "Hop to It"
* Alternative intro to Bobby Browns "Three Blind Mice"
* More present and different musical score, with background music playing through most of the film as opposed to various non-musical moments in the uncut version.
* The end credits: The uncut version features an original song while the TV version features a longer version of Little Richards "Party with the King."

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 