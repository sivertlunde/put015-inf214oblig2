Marmayogi
 
{{Infobox film
| name = Marmayogi மர்மயோகி
| image = Marmayogi.jpg
| caption  = Theatrical Poster
| director = K. Ramnoth
| writer =  A. S. A. Sami
| starring = M. G. Ramachandran M. N. Nambiar S. A. Natarajan Anjali Devi Javar Seetharaman Serukulathur Sama Madhuri Devi
| producer = Jupiter Pictures
| music = C. R. Subburaman S. M. Subbaiah Naidu
| distributor =
| cinematography = Masthan W. R. Subbarao
| editing = M. A. Thirumugam
| released =  2 February 1951
| runtime =  175 min. (15,760 feet)
| country = India Tamil
| budget =
}}
 Tamil film film censor board.               

==Plot==
A Kings (Serukalathur Sama) mistress (Anjali Devi) usurps his power and casts him adrift in a river. The two princes of the kingdom (Sahasranamam and MGR) are denied their birthright. The deposed king wanders the country in the guise of a sage. The younger prince (MGR) leads a popular rebellion against the usurper-queen and restores the kingdom to his father.

==Cast==
*M. G. Ramachandran
*S. A. Natrajan
*M. N. Nambiar
*Anjali Devi
*Javar Seetharaman
*Serukulathur Sama
*S. V. Sahasranamam
*Madhuri Devi
*Pandari Bai
*M. S. S. Bhagyam

==Crew==
*Director: K. Ramnoth 
*Dialogue: A. S. A. Samy
*Screenplay: A. S. A. Samy 
*Music: C. R. Subburaman & S. M. Subbaiah Naidu 
*Lyrics: Udumalai Narayana Kavi, K. D. Santhanam, Kannadasan & A. S. Kaleeswaran
*Editing: M. A. Thirumugam
*Cinamatography: Masthan & W. R. Subbarao 
*Choreographer: Vedandham Ragavayya
*Stunts: R. N. Nambiar & T. R.  Lakshminarayanan
*Still Photographer: K. Anandan 

==Production== chola king Karikala Chola|karikalan). This was a break from tradition, when the usual practice was to give the hero (and the film title) Sanskrit names like Veerasimhan or Pratapan. The films title was changed to Marmayogi (lit. The mysterious sage) to avoid it being confused as a historical film. The film was produced by Jupiter Pictures at the Central Studios in Coimbatore. K. Ramnoth was hired as director. Serukulathur Sama, Sahasranamam, S. A. Natarajan, M. N. Nambiar and Anjali Devi were cast in the film. The films dialogue was written carefully to embellish MGRs image as a social rebel and do-gooder. The film was also made simultaneously in Hindi as Ek tha raja. The film was given an "A" (Adults Only) certificate by the film censor board because it featured a ghost.    {{Cite web
 | url = http://www.nowrunning.com/news/news.aspx?it=13486
 | title = Walt Disney to produce Kamal film Marma Yogi
 | language =
 | author =
 | work = www.nowrunning.com
 | publisher =
 | date = 2008-01-17
 | accessdate = 2009-12-21
}}  {{Cite web
 | url = http://www.indiaglitz.com/channels/tamil/article/36007.html
 | title = Marma Yogi - Kamal and Walt Disney
 | language =Walt Disney to produce Kamal film Marma Yogi
 | author =
 | work = www.indiaglitz.com
 | publisher =
 | date = 2008-01-19
 | accessdate = 2009-12-21
}}    

==Soundtrack==
The music was composed by S. M. Subbaiah Naidu & C. R. Subburaman.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Em Manasirkisandha Raja || T. V. Rathinam || || 
|- 
| 2 || Kannin Karumaniye Kalaaavathi  || Thiruchi Loganathan & K. V. Janaki || ||
|- 
| 3 || Ah Inbam Idhuve Inbam || P. A. Priya Nayaki || ||
|- 
| 4 ||  ||  || ||
|- 
| 5 ||  ||  || ||
|- 
| 6 ||  ||  || ||
|- 
| 7 ||  ||  || || 
|- 
| 8 ||  ||  || || 
|- 
| 9 ||  ||  || || 
|}

==Reception==
The film was released on 2 February 1951 and was a box office success. It cemented MGRs onscreen image as a champion of the underprivileged and hinted at his political ambitions. His lines in the film became famous - especially the  Naan kuri vaithaal thavara maatten! Thavarumey aanaal kuri vaikka maatten (lit.If I aim, it will not fail; if it will fail, I will not aim).   

==References==
 

==External links==
*  

 

 
 
 
 
 
 