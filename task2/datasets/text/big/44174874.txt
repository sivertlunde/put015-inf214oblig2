Revenge (1985 film)
{{Infobox film
| name           = Revenge
| image          =
| caption        =
| director       = Crossbelt Mani
| producer       = Ratheesh Sathar
| writer         =
| screenplay     = Anuradha Balan K Nair
| music          = Guna Singh
| cinematography = Cross Belt Mani
| editing        = Chakrapani
| studio         = Jaid Film
| distributor    = Jaid Film
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Anuradha and Balan K Nair in lead roles. The film had musical score by Guna Singh.   

==Cast==
*Ratheesh as Johny
*Sathaar as William Anuradha as Susan
*Balan K Nair
*Jagathi Sreekumar as Head constable
*Ramu
*Silk Smitha as Geetha

==Soundtrack==
The music was composed by Guna Singh and lyrics was written by Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Sringaaram Poompeeli Neerkkum || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 2 || Sukham Sukham || Lathika || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 