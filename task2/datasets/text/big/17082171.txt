Canvas Back Duck
 
{{Infobox Hollywood cartoon
|cartoon_name=Canvas Back Duck
|series=Donald Duck
|image=
|caption=
|director=Jack Hannah
|story_artist=Jack Kinney   Bill Berg
|animator=George Kreisl   Bob Carlson   Volus Jones   Al Coe   Dan MacManus
|layout_artist=Yale Gracey
|background_artist=Ray Huffine
|voice_actor=Clarence Nash   Billy Bletcher
|musician=Oliver Wallace
|producer=Walt Disney
|studio=Walt Disney Productions
|distributor=RKO Radio Pictures
|release_date=December 25, 1953
|color_process=Technicolor
|runtime=7:06
|movie_language=English
}} 1953 Walt animated short film starring Donald Duck and his nephews. 

==Plot==
Donald and his nephews visit a carnival. While they play games, Donald is tricked by a shifty barker into fighting "Pee Wee Pete" (Pegleg Pete), a truculent bruiser who significantly outweighs Donald. In spite of help from Huey, Dewey and Louie, the situation looks desperate for Donald Duck, until his fist accidentally connects with Petes jaw, which crumbles. It turns out Pegleg Pete literally had a glass jaw. Donald wins the prize money and exits the carnival triumphantly with his nephews.

==Availability==
* Walt Disney Treasures: Wave Eight, The Chronological Donald, Volume Four, Disc one

==References==
 

==External links==
*  
*  
 
 
 
 
 

 