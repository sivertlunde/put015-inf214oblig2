A Self Made Hero
 
 
{{Infobox film
| name           = A Self Made Hero (Un héros très discret)
| image          = ASelfMadeHero1996Poster.jpg
| caption        = French poster
| director       = Jacques Audiard
| producer       = Françoise Galfré Patrick Godeau
| writer         = Jacques Audiard Alain Le Henry Jean-François Deniau (novel)
| screenplay     =
| story          =
| based on       =  
| starring       = Mathieu Kassovitz Albert Dupontel
| music          = Alexandre Desplat
| cinematography = Jean-Marc Fabre
| editing        = Juliette Welfling
| studio         =
| distributor    =
| released       =  
| runtime        = 107 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}}
A Self Made Hero ( ) is a 1996 French film directed by Jacques Audiard. It is based on the novel by Jean-François Deniau.

==Synopsis==
Albert Dehousse has grown up on heroic novels, unfortunately his life isnt quite so exciting. Albert lives in a village in Northern France with his mother, who lives in memory of her husband, who she claims died a hero in the First World War. World War Two passes the pair by, as Albert is not called up as he is the only child of a war widow, denying him of his chance to become a hero. Having married the daughter of a member of the resistance, he leaves his family and his marriage for Paris where heroes are truly celebrated.

==About the Film==
"Les vies les plus belles sont celles qu’on s’invente", (the most beautiful lives are those we invent) announces an older Albert Dehousse at the beginning of the film. Un héros très discret is a film which investigates the divide between fantasy and reality.

==Awards and nominations== Cannes Film Festival (France) Best Screenplay (Jacques Audiard and Alain Le Henry)
**Nominated: Golden Palm (Jacques Audiard)   

*César Awards (France) Best Actor &ndash; Supporting Role (Albert Dupontel) Best Actress &ndash; Supporting Role (Sandrine Kiberlain) Best Director (Jacques Audiard) Best Editing (Juliette Welfling) Best Music (Alexandre Desplat) Best Writing (Jacques Audiard and Alain Le Henry)

*Stockholm Film Festival (Sweden)
**Won: Best Screenplay (Jacques Audiard and Alain Le Henry)
**Nominated: Bronze Horse
 Valladolid Film Festival (Spain)
**Won: Silver Spike (Jacques Audiard)
**Nominated: Golden Spike (Jacques Audiard)

==Cast==
* Mathieu Kassovitz : Albert Dehousse
* Albert Dupontel : Dionnet
* Anouk Grinberg : Servane
* Sandrine Kiberlain : Yvette
* Nadia Barentin : Generals wife Bernard Bloch : Ernst
* François Chattot : Louvier
* Philippe Duclos : Caron
* Danièle Lebrun : Madame Dehousse
* Armand de Baudry dAsson : Englishman
* Wilfred Benaïche : Nervoix
* Clotilde Mollet : Odette
* François Berléand : Monsieur Jo
* Jean-Louis Trintignant : Albert Dehousse (old)
* Bruno Putzulu : Meyer

==See also==
* Confessions of a Dangerous Mind

==References==
 

==External links==
*  
*   at Alice Cinéma (French)

 

 
 
 
 
 