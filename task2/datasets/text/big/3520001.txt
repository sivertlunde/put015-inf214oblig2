Arna's Children
 
{{Infobox film
| name           = Arnas Children
| image          = Arnas-children.jpg
| caption        = Theatrical release poster
| director       = Juliano Mer Khamis   Danniel Danniel
| producer       = Osnat Trabelsi   Pieter van Huystee
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Israel Netherlands
| language       = Arabic Hebrew
| budget         = 
| gross          = 
}}
Arnas Children is a 2004 Dutch-Israeli documentary film directed by Juliano Mer Khamis  and Danniel Danniel about a childrens theater group in Jenin in the Palestinian territories established by Arna Mer-Khamis, the directors mother, an Israeli Jewish political and human rights activist.

The film portrays the lives of Arna Mer-Khamis and the children members of the theater including Ala el-Sabagr, Zakaria Zubeidi, Daud Zubeidi, Majdi Shadi, Haifa Esteti, Nidal Swetti, Yussef Swetti, Mahmoud Kaneri, Khairia Fakhri and Ashraf Abu-Alheji.                   

The film won "Best Documentary Feature" in the 2004 Tribeca Film Festival.

Three of the theater children died in various operations or while resisting the Israeli army, namely Ala, Nidal, and Ashraf. Yussef committed a suicide attack in Hadera in 2001, murdering four civilians. Two other children, Daud and Zakaria were imprisoned.

The director of the film Juliano Mer Khamis was assassinated in Jenin on 4 April 2011 by masked militants.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 

 
 