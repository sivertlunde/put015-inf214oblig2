Waxwork (film)
{{Infobox film
| name           = Waxwork
| image          = Waxworkposter.jpg
| caption        = Promotional film poster
| director       = Anthony Hickox
| producer       = Staffan Ahrenberg
| writer         = Anthony Hickox Michelle Johnson David Warner Dana Ashbrook Miles OKeeffe Patrick Macnee John Rhys-Davies
| music          = Roger Bellon
| cinematography = Gerry Lively
| editing        = Christopher Cibelli
| distributor    = Vestron Pictures
| released       =    
| runtime        = 95 min. (R-rated) 100 min. (unrated)
| country        = United States
| language       = English
| budget         = $1.5 million
| gross          = $808,114 
}}
Waxwork is a 1988 American horror comedy film starring Zach Galligan and Deborah Foreman.

==Plot==
 Michelle Johnson), Eric Brown) Gothic castle, David Warner) walks him into the display. Mark goes to a pair of investigating police detectives, Inspector Roberts (Charles McCaughan), and meets Lincoln as he lets Roberts investigate the waxworks. As Mark and Roberts leave the Museum, Mark recognizes Lincoln.

Later, Roberts realizes that some of the displays look like some of the other missing people, and then comes back to the Wax Museum, cuts off a piece of Chinas face, puts it in a bag, and walks into the mummy display; the mummy throws him in the tomb with another undead mummy and a snake. Later, Roberts partner sneaks in to the museum, and gets his neck broken by Junior (Jack David Walker), "a tall butler" Lincoln scolds for killing the partner.

Mark takes Sarah to the attic of his house, where he shows her an old newspaper detailing the murder of his grandfather (which was seen in the prologue); the only suspect was David Lincoln, his chief assistant, whose photograph closely resembles the waxwork owner. The two then consult the wheelchair-bound Sir Wilfred (Patrick Macnee), a friend of Marks grandfather, who explains how he and Marks grandfather collected trinkets from "eighteen of the most evil people who ever lived" and that Lincoln stole the artifacts; Lincoln, having sold his soul to the devil, wants to bring their previous owners to life by creating some wax effigies and feeding them the souls of victims, a concept taken from Haitian Vodou. Providing all eighteen with a victim would bring about the "voodoo end of the world, when the dead shall rise, and consume all things".

On the advice of Sir Wilfred, Mark and Sarah enter the waxwork museum at night and douse it with gasoline. However, Sarah is lured into the display of the Marquis de Sade (J. Kenneth Campbell), and Mark is pushed into a zombie display by the waxworks two butlers. Mark is approached by a horde of zombies, but finds that if he does not believe in the monsters, then they do not exist and cannot harm him. Mark finds his way out of the display and into the Marquis de Sade exhibit, where he rescues Sarah, while the marquis vows revenge.

Despite Mark and Sarahs attempts to escape, Junior and Lincoln grab Mark and Sarah pull them out of sight as Gemma and James return. Gemma gets lured into the Marquis de Sade display and James attempts to steal something from the zombie display; moments later, the bodies of James and Gemma reappear as wax figures, the displays are completed with the figures and their victims reanimating as evil entities. Suddenly, Sir Wilfred and a huge group of armed men "along with Marks butler Jenkins" arrive and in the ensuing battle, several waxworks and slayers die, including Lincolns butlers and Mark and Sarahs former friends, now evil.  Mark duels with the Marquis de Sade, who is finally killed by Sarah with an axe.

The reunited couple is confronted by Lincoln, who dies getting shot by Sir Wilfred and falls in a vat of boiling wax. Sir Wilfred is killed by a werewolf as Sarah and Mark manage to escape the burning waxwork with their lives and begin to walk home, not noticing that the hand from the zombie display is scuttling away from the rubble.

==Production==

The Eighteen most evil beings used in the film are; the Marquis de Sade, the werewolf, Count Dracula and his son and brides, the Phantom of the Opera, The Mummy, Romero-style Zombies, Frankensteins monster, Jack the Ripper, the Invisible Man, a voodoo priest, a witch, a snakeman, pods from Invasion of the Body Snatchers, a mutant baby, an axe murderer, a multi-eyed alien, giant talking venus fly trap, Dr. Jekyll and Mr. Hyde. 

==Cast==
* Zach Galligan as Mark Loftmore
* Deborah Foreman as Sarah Brightman Michelle Johnson  as China Webster
* Dana Ashbrook  as Tony
* Micah Grant as Jonathan Eric Brown  as James
* Clare Carey as Gemma David Warner as David Lincoln
* Patrick Macnee as Sir Wilfred Mihaly Michu Meszaros as Hans
* Jack David Walker (as Jack David Warner) as Junior
* Charles McCaughan as Inspector Roberts
* J. Kenneth Campbell as Marquis de Sade
* Miles OKeeffe as Count Dracula
* John Rhys-Davies as Werewolf
* Jennifer Bassey as Mrs Loftmore
* Edward Ashley as Professor Sutherland Joe Baker as Jenkins
* Buckley Norris as Lecturer
* Tom McGreevey (as Tom MacGreevey) as Charles

Several crew members appear in small roles:

*Anthony Hickox, director,  as English prince
*James Hickox, assistant editor,  as werewolf hunters assistant
*Gerry Lively, director of photography,  as Sir Wilfreds butler

==Release==
Often cited as the first self-referential horror well before Scream (1996 film)|Scream and such, the film was given a limited release in the United States by Vestron Pictures in June 1988.  It grossed $808,114 at the box office.   It was released by Vestron Video the same year on VHS in both R-rated and Unrated editions and went on to sell over 150,000 units (an estimated gross of $2 million). The films budget was $1.5 million.

The film was released on   and again in 2012 as part of an 8 horror film collection DVD. 

==Other media==
A comic adaption of the film was published by Blackthorne Publishing in November 1988, one as a black and white One-shot (comics)|one-shot, and one as Waxwork 3-D Special # 1 (# 55 of Blackthorne′s Blackthorne 3-D Series). 

==Sequel==
In 1992, Anthony Hickox filmed the sequel  .

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 