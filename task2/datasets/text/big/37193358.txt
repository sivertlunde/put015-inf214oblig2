City of God – 10 Years Later
 
{{Infobox film
| name           = City of God – 10 Years Later 
| image          = City of God - 10 Years Later.jpg
| border         =
| alt            =
| caption        =
| director       = Cavi Borges Luciano Vidigal
| producer       = Luís Carlos Nascimento
| writer         = Gustavo Melo Luís Carlos Nascimento Luciano Vidigal
| screenplay     =
| story          =
| based on       =
| narrator       =
| starring       = {{Plainlist|
* Alice Braga
* Seu Jorge
* Leandro Firmino
* Douglas Silva
* Thiago Martins
}}
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =   }}
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = $90,500 
| gross          =
}}
City of God – 10 Years Later is a 2013 Brazilian documentary film directed by Cavi Borges and Luciano Vidigal.   
 City of God. The actors who portrayed Dadinho, Bené, and Lil Zé, as well as the actress Alice Braga and musician and actor Seu Jorge participated in this documentary ten years after starring in the earlier film, directed by Fernando Meirelles. 

== Cast==
{{columns-list|2|
* Cavi Borges
* Luciano Vidigal
* Alice Braga Jorge Mário
* Leandro Firmino
* Matheus Nachtergaele
* Douglas Silva
* Roberta Rodrigues
* Darlan Cunha
* Thiago Martins
}}

== Production == City of God.  Although they invited Fernando Meirelles to produce the documentary, he declined and said that his involvement could limit their perspective; however, he was an important part of their research.  They found 50 people involved in the original film and chose to use 18.   The actors were paid $90 for their interviews.   In order to raise money, the filmmakers used crowdfunding and threw parties.     In the community, news of a retrospective was not universally accepted, as some people disliked the original film for its violence.  However, Borges wanted to change international perceptions of the area, which he says City of God colored. 

== Release ==
City of God – 10 Years Later premiered at the Festival do Rio in October 2013.   Due to unrelated protests, the original showing was canceled, and it was delayed.   Its international premiere was at the Miami International Film Festival in March 2014. 

==Reception==
The Miami New Times gave the documentary a mixed review, stating that "Fans of the original film will find plenty to enjoy, but as a standalone documentary about the effect of a hit movie on the poor community that birthed it, it comes up somewhat short." 

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 