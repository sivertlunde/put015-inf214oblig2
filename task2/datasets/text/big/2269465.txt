Jason X
 
{{Infobox film
| name = Jason X
| image = Jason x.jpg
| alt  =  
| caption = Theatrical release poster Jim Isaac
| producer = {{Plainlist|
* Noel Cunningham
* Sean S. Cunningham
* Geoff Garrett
* Jim Isaac
* Marilyn Stonehouse}}
| writer = Todd Farmer
| based on =  
| starring = {{Plainlist|
* Lexa Doig
* Lisa Ryder
* Chuck Campbell
* Jonathan Potts
* Melyssa Ade
* Kane Hodder}}
| music = Harry Manfredini
| cinematography = Derick V. Underschultz
| editing = David Handman
| production companies = {{Plainlist|
* Crystal Lake Entertainment
* Friday X Productions}}
| distributor = New Line Cinema
| released =  
| runtime = 91 minutes  
| country = {{Plainlist|
* United States
* Canada}}
| language = English
| budget = $11 million   
| gross = $16.9 million 
}} science fiction Jim Isaac. Friday the 13th film series and stars Kane Hodder as the undead mass murderer Jason Voorhees. The film made $16,951,798 worldwide against a budget of $11 million. Thus far, it is the last appearance of Kane Hodder in the role of Jason Voorhees. 
 continuity of the series.

==Plot== cellular regeneration Rowan (Lexa Doig) lures Jason into an "ice" pod and activates it. Jason then ruptures the pod with his machete and stabs Rowan in the abdomen, spilling ice fluid into the sealed room and freezing them both.
 dissect Jasons body. Lowe, who is in serious debt, calls his financial backer Dieter Perez (Robert A. Silverman), of the Solaris, who notes that Jasons body could be worth a substantial amount to a collector.

While Stoney has sex with Kinsa, Jason comes back to life and attacks Adrienne, then freezes her face with liquid nitrogen before smashing her head to pieces on a counter. Jason takes a machete-shaped surgical tool and makes his way through the ship. He stabs Stoney in the chest and drags him away, to Kinsas horror. Sergeant Brodski (Peter Mensah) leads a group of soldiers to attack Jason. Meanwhile, Jason attacks and kills Dallas by bashing his skull against the wall after breaking Azraels back. He then tries to attack Crutch, but Brodski and his soldiers save him. Jason disappears, and after Brodski splits up his team, Jason kills them one by one.
 decapitates Lowe.

With the ship badly damaged, the remaining survivors head for Grendel s shuttle, while Tsunaron heads elsewhere with KM-14. After finding Lous remains, Crutch and Waylander prepare the shuttle. Rowan finds Brodski, but he is too heavy for her to carry, so she leaves to get help. Waylander leaves to help with him, while Crutch prepares the shuttle. Jason kills Crutch by electrocution. On board the shuttle, Kinsa has a panic attack and launches the shuttle without releasing the fuel line, causing it to crash into the ships hull and explode, killing her. Brodski attacks Jason, but is overpowered. Tsunaron reappears with an upgraded KM-14, complete with an array of weapons and new combat skills. She fights Jason off and seemingly kills him, knocking him into a Nanotechnology|nanite-equipped medical station and blasting off his right arm, left leg, right rib cage, and, finally, part of his head. The survivors send a distress call and receive a reply from a patrol shuttle.
 blowing out EVA to fix it.
 holographic simulation incinerating them. Tsunaron assures KM-14 that he will build a new body for her.
 falling star as Jasons charred mask sinks to the bottom of the lake.

==Cast==
 
* Lexa Doig as Rowan LaFontaine
* Lisa Ryder as Kay-Em 14
* Chuck Campbell as Tsunaron
* Jonathan Potts as Professor Brandon Lowe
* Peter Mensah as Sergeant Brodski
* Melyssa Ade as Janessa
* Kane Hodder as Jason Voorhees
* Melody Johnson as Kinsa
* Phillip Williams as Crutch
* Derwin Jordan as Waylander
* Dov Tiefenbach as Azrael
* Amanda Brugel as Geko
* Kristi Angus as Adrienne Thomas
* Yani Gellman as Stoney
* Todd Farmer as Dallas
* David Cronenberg as Dr. Wimmer
* Robert A. Silverman as Dieter Perez
* Marcus Parilo as Sgt. Marcus
 

==Production==
===Development===
Development of Jason X began in the late nineties while Freddy vs. Jason was still stuck in development hell. With Freddy vs. Jason not moving forward, Sean S. Cunningham decided that he wanted another Friday the 13th film made to retain audience interest in the Jason Voorhees|character. The film was conceived by Todd Farmer, who plays "Dallas" in the film, and was the only pitch he gave to the studio for the movie, having suggested sending Jason into space as a means to advance the film series. 

The film score was composed and conducted by Harry Manfredini. It was released on Varèse Sarabande. Jason X s theme song is "Bodies (Drowning Pool song)|Bodies" by Drowning Pool from their album Sinner (Drowning Pool album)|Sinner.  The song, while used in the films theatrical trailer, does not actually appear in the film, itself. 

==Release==
===Box office===
The film made $13,121,555 domestically, and earned $3,830,243 overseas for a worldwide gross of $16,951,798,  making it the third lowest-grossing film in the series.

===Critical reception===
The film received unfavorable reviews, holding a "Rotten" rating of 19% on Rotten Tomatoes, based on 104 reviews, with the consensus being that "Jason goes to the future, but the story is still stuck in the past."  Metacritic shows the film as having "generally unfavorable" reviews based on 23 critics, with a score of 25/100.  American film critic Roger Ebert wrote a scathing review of the film, quoting one of the films lines: "This sucks on so many levels." 

However, the film was better received in the United Kingdom, gaining positive reviews from the countrys two major film magazines, Total Film  and Empire (film magazine)|Empire.  Empire s review by Kim Newman in particular praised Jason X as "Wittily scripted, smartly directed and well-played by an unfamiliar cast, this is a real treat for all those who have suffered through the story so far."

==Legacy==
 
In 2005,  , Jason X: Death Moon by Alex Johnson and Jason X: To the Third Power by Nancy Kilpatrick.

Avatar Press produced two comic book titles based on this film: Jason X, a one-shot by Brian Pulido that picks up as a sequel to the movie, and Friday the 13th: Jason vs. Jason X, a two-issue mini-series by Mike Wolfer that pits the two versions of Jason (original-classic Jason vs Uber Jason) against each other.

The scene where Jason freezes Adriennes head in liquid nitrogen before smashing it to pieces was the subject of an episode of MythBusters. The team built several fake heads, dipped them in liquid nitrogen and attempted to smash them with a robotic arm. The myth was declared "busted" after none of the heads appeared to replicate the effect in the movie.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 