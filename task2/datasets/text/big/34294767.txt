Smart Blonde
 
{{Infobox film
| name           = Smart Blonde
| image          = Glenda Farrell in Smart Blonde trailer cropped.jpg
| caption        = Glenda Farrell in the film Frank McDonald
| producer       = Jack L. Warner Hal B. Wallis
| writer         = Frederick Nebel
| based on       =
| screenplay     = Kenneth Gamet Don Ryan
| starring       = Glenda Farrell Barton MacLane Wini Shaw
| music          = Heinz Roemheld
| cinematography = Warren Lynch
| editing        = Frank Magee
| distributor    = Warner Brothers
| released       =   
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         =
}}
 Frank McDonald. Glenda Farrell plays Torchy Blane, former showgirl, newspaper reporter on the police beat, and girlfriend of Homicide Detective Steve McBride (Barton MacLane).  This is the first of nine Torchy Blane films, seven of them featuring Farrell, which ran until 1939. 

Detective novelist Frederick Nebels reporter Torchy Blaine was a male news-hound named Kennedy. For Warner Bros. Smart Blonde, the characters name and gender were changed. 

==Plot summary==
 

== Cast ==
* Glenda Farrell as Torchy Blane
* Barton MacLane as Steve McBride
* Wini Shaw as Dolly Ireland
* Addison Richards as Fitz Mularkey
* Robert Paige as Lewis Friel Craig Reynolds as Tom Carney
* Charlotte Wynters as Marcia Friel
* Jane Wyman as Hatcheck Girl
* Joseph Crehan as Tiny Torgenson Tom Kennedy as Gahagan
* John Sheehan as Leon Blyfuss
* Max Wagner as Chuck Cannon George Lloyd as Pickney Sax
* unbilled players include Frank Faylen, Milton Kibbee, and Fred Snowflake Toones

==Torchy Blane series==
* Smart Blonde (1937) Played by Glenda Farrell
* Fly-Away Baby (1937) Played by Glenda Farrell
* The Adventurous Blonde (1937) Played by Glenda Farrell
* Blondes at Work (1938) Played by Glenda Farrell
* Torchy Blane in Panama (1938) Played by Lola Lane
* Torchy Gets Her Man (1938) Played by Glenda Farrell
* Torchy Blane in Chinatown (1939) Played by Glenda Farrell
* Torchy Runs for Mayor (1939) Played by Glenda Farrell
* Torchy Blane.. Playing with Dynamite (1939) Played by Jane Wyman

==References==
 

== External links ==
 
*  
*  
*  

 
 
 
 
 


 