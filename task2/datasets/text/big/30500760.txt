George in Civvy Street
{{Infobox film
| name = George in Civvy Street 
| image = "George_in_Civvy_Street"_(1946).jpg
| image_size =
| caption = UK trade ad poster
| director = Marcel Varnel Ben Henry
| producers = Marcel Varnel Ben Henry
| writer =  Howard Irving Young Peter Fraser Ted Kavanagh  Max Kester Gale Pedrick
| narrator = Ian Fleming
| music = Harry Bidgood
| cinematography = Phil Grindrod
| editing = Douglas Robertson
| studio =  Columbia Pictures 
| distributor = Columbia Pictures Corporation 
| released = 8 July 1946 (UK)	
| runtime = 79 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 British comedy Ian Fleming.  It was made by the British subsidiary of Columbia Pictures. This was Formbys last big screen appearance. After the film flopped at the box office, he resumed his career in the music hall.   The working title for the film was "Remember the Unicorn".

==Plot summary==
This comedy film portrays George Formby leaving the forces and becoming a village pub owner, who works to turn a waitress from her current boss, a rival pub owner. Formby falls in love with the waitress, and various battles ensue between the pub rivals.   

==Cast==
* George Formby as George Harper
* Rosalyn Boulter as Mary Colton
* Ronald Shiner as Fingers Ian Fleming as Uncle Shad
* Wally Patch as Sprout
* Philippa Hiatt as Lavender
* Enid Cruickshank as Miss Gummidge
* Robert Ginns as Crabtree
* Mike Johnson as Toby
* Frank Drew  as Jed
* Daphne Elphinstone as Mitzi Montrose
* Johnny Claes as Himself
* Ronnie Scott as Band member
* Rita Varian as Mrs Brindle

==Critical reception==
* According to TV Guide, the film is "lifeless farce", and a "tasteless romp."  
* A reviewer for Sky Movies wrote, "the partnership of music hall favourite George Formby and director Marcel Varnel (who also turned out the best of the Will Hay and Arthur Askey comedies) was looking distinctly jaded after six movies in a row. And despite a formidable array of four new writers, this stale comedy about pub rivalry, ended Formbys screen career on a low note." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 