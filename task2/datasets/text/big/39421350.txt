Bromance: My Brother's Romance
{{Infobox film
| name             = Bromance: My Brothers Romance
| image            = Bromance 2013 Filipino film.jpg
| caption          = Theatrical movie poster
| director         = Wenn V. Deramas
| producer         = Enrico Santos Malou N. Santos Charo Santos-Concio	
| writer           = Joel Mercado Enrico Santos Kriz G. Gazmen Ays de Guzman Danno Kristoper C. Mariquit
| screenplay       = Ays de Guzman Danno Kristoper C. Mariquit Wenn V. Deramas
| starring         = Zanjoe Marudo Cristine Reyes
| cinematography   = Elmer Despa	
| editing          = Marya Ignacio
| music            = Vincent de Jesus
| studio           = Skylight Films
| distributor      = Star Cinema
| released         =  
| runtime          = 
| country          = Philippines
| language         = Tagalog
English
| budget           =
| gross            = P73,848,739 ₱375,024,507-->     
}}
 Filipino comedy film starring Zanjoe Marudo and Cristine Reyes. It is the first foray of Skylight Films into the comedy genre  and is directed by Wenn V. Deramas.  The film is based on a true story  and premiered on May 15, 2013.

==Synopsis==
A young man is forced to pretend to be his estranged gay brother in order to keep a major business from falling through. Though he has always hated his brother, the young man gains a new appreciation as he lives his brothers life. 

==Cast==

===Main Cast===
* Zanjoe Marudo as Brando and Brandy
* Cristine Reyes as Erika

===Supporting Cast===
* Arlene Muhlach as Vangie
* Boom Labrusca as Arn-Arn
* Manuel Chua as Jerome
* Joey Paras as Beergin
* Lassy Marquez as Tequi
* Atak Arana as Mr. Big
* Joy Viado as Delilah
* Maricar De Mesa as Joyce
* Abby Bautista as Abby
* Nikki Valdez as Britney
* Carlo Romero as Rico
* Jeff Luna as Joey

===Special Participation===
* Maliksi Morales as Young Brando/Brandy
* Beauty Gonzales as Young Vangie
* Ai-Ai Delas Alas as the Columbarium Ghost
* Kris Aquino as the Doctor
* Vice Ganda as the Wedding Stopper
* Bea Alonzo as the Wedding Guest, Britneys (Nikki Valdez) friend
* John Lloyd Cruz as the Deacon

==Reception==

===Rating===
The film was graded "B" by the Cinema Evaluation Board and rated PG-13 by MTRCB. 

===Box Office===
Bromance: My Brothers Romance landed on No. 2 on its opening weekend beaten by Star Trek Into Darkness on a small margin.  Bromance earned P32,334,475 on its first 5 days of showing.   After 3 weeks of showing, it grossed P73,848,739.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 