Mazinger Z vs. Devilman
{{Infobox film
| name           = Mazinger Z vs. Devilman
| image          = Mazinger Z tai Devilman (1973).jpg
| image size     = 
| alt            = 
| caption        = Poster of the film Mazinger Z tai Devilman
| director       = Tomoharu Katsumata
| producer       = Shunichi Toishi
| writer         = Susumu Takahisa
| screenplay     = 
| story          =
| based on       =  
| narrator       = 
| starring       = 
| music          = Michiaki Watanabe
| cinematography = 
| editing        = 
| studio         = Toei Doga, Dynamic Production
| distributor    = 
| released       =  
| runtime        = 43 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

  is a 1973 animated movie that crossed over two then-popular Anime series, both of which were created by Manga artist Go Nagai. Note however that the movie features alternate versions of events from both series, and is therefore not canonical to either one.

==Story==
The movie opens with a battle between the  Super Robots Mazinger Z and Aphrodite A against three of Doctor Hell’s Mechanical Beasts. The heroes win, but the battle apparently frees the female demon  Siren (mythology)|Sirène from below the ground. She flies away before they can do anything about it. However she is seen by Akira Fudo, who transforms into his real form of Devilman to fly after her.
 flying fortress (not Ghoul, the one seen in the regular Mazinger series, but rather the Navalon, one exclusive to this movie) Hell and his henchman Baron Ashura follow Sirène to the Himalaya Mountains, were the rest of the tribe is frozen. Hell helps to free some of the demons (notably absent is their leader, Zenon) and makes a deal with them to help them defeat their enemy, Devilman, if they will help him defeat Mazinger. They are unaware that Devilman has spied on their conversation.

Later, Akira goes to warn Koji about this but ends up mocking the Mazinger for being unable to fly; this leads to the two hot-tempered teenagers racing each other on motorcycles (it ends in a draw.) It turns out that Mazinger’s headquarters, the Photon Research Laboratory, is already working on solving the robot’s disadvantage against flying enemies by making the Jet Scrambler, a giant winged jetpack. However one of the demons has spied on them and reports this development to Dr. Hell. He sends the shapeless demon and Sirène to destroy the Scrambler. They manage to damage it before being driven off by Koji’s raygun; Sirène escapes by taking Koji’s brother Shiro and his friend Sayaka hostage. She then lets them fall from the sky, but they are saved by Devilman. When Koji goes to pick them up, he watches Akira change into Devilman and so learns his secret. Devilman follows the amorphous demon to the sea, but falls into a trap; he’s rescued by Koji in the Mazinger. Afterwards they talk, and Akira reveals to him that he’s a rogue member of the Demon Tribe who fights to protect humanity.

The combined forces of Dr. Hell and the demons then attack the Laboratory; Devilman flies to help defend it, but is overwhelmed and captured. He’s taken to some (obviously magical) clouds, where he’s placed on an ice cross and tortured by the demons for being a traitor. However, as soon as the Scrambler is repaired, Koji uses it to fly the Mazinger into the clouds, killing the demons, freeing Devilman and finally destroying the flying fortress, although Hell and Ashura escape alive.

The movie ends with Koji and Akira on good terms, with the two flying off together into the sunset.

==External links==
* 
*    at allcinema
*  at Animemorial
* 

 
 
 

 
 
 
 
 
 