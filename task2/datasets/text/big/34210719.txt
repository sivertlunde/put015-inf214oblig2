Rumpelstiltskin (1987 film)
{{Infobox film name           = Rumpelstiltskin director       = David Irving producer       =   screenplay     = David Irving based on       =   starring       =   music          = Max Robert editing        =  studio         = The Cannon Group distributor    = The Cannon Group released       =         runtime        = 84 minutes country        =   language       = English
}} fairy tale Cannon Films Cannon Movie Tales|Movie Tales series.

 

==Production and release== author of the same name), scripted and directed;  their mother, actress Priscilla Pointer, portrayed the Queen. 

Cannon Films screened Rumpelstiltskin as the opening night attraction of its "family film festival" at 1987s Cannes Film Festival.   It was the first Cannon Movie Tale released in the U.S.;  though originally scheduled for November 21, 1986,  it premiered in April 1987.   The film was not well-received critically; Richard Harrington of The Washington Post said, " ll Cannon has done...is to make a short story long. And long and longer."   In his Movie Guide, Leonard Maltin gave it two stars out of four and commented, "  threadbare musical adaptation...  likely to bore even the small fry."   

MGM released Rumpelstiltskin on DVD in 2005.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 