Uncle Was a Vampire
{{Infobox film
 | name = Uncle Was a Vampire
 | image = Uncle Was a Vampire.jpg
 | caption = Steno
 | writer =   Edoardo Anton  Mario Cecchi Gori  Sandro Continenza  Marcello Fondato  Renato Rascel Steno  Dino Verde
 | starring =  
 | music =   Armando Trovajoli Renato Rascel
 | cinematography = Marco Scarpelli 
 | editing =   Eraldo Da Roma
 | producer =   
 | released =  
 | language =   Italian
 }} 1959 Italian-French horror comedy film directed by Steno (director)|Steno.      

==Plot ==
Baron Osvaldo Lambertenghi is forced to sell his ancestral castle for debts. The manor is ingloriously transformed into a frivolous hotel and Osvaldo is allowed to continue to live there working as a porter. One day Osvaldo receives a visit from his uncle Roderico, who turns out to be a real vampire. Osvaldo tries to warn of what the various guests of the hotel, with the only result of being taken for a madman. Bitten by his uncle, Oswald will also turn into a vampire, but her lover Lulu will be able to free him from the curse.

== Cast ==
*Renato Rascel: Baron Osvaldo Lambertenghi 
*Christopher Lee: Baron Roderico da Frankurten 
*Sylva Koscina: Carla 
*Kai Fischer: Lellina 
*Lia Zoppelli: Letizia 
*Franco Scandurra:  Professor Stricker 
*Carl Wery: Director  
*Rik Van Nutter: Fianceè of Carla
*Susanne Loret: Susan 
*Mario Cecchi Gori: Civil-law notary 
*Franco Giacobini

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 


 
 