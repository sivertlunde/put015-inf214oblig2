The Mahabharata (1989 film)
 
 
{{Infobox film
| name           = The Mahabharata
| image          = TheMahabarata1989.jpg
| image size     =
| caption        = DVD cover
| director       = Peter Brook
| producer       =
| writer         = Peter Brook Jean-Claude Carrière Marie-Hélène Estienne
| narrator       =  
| starring       = Robert Langton-Lloyd Antonin&nbsp;Stahly-Vishwanadan&nbsp; Bruce Myers Vittorio Mezzogiorno Andrzej Seweryn Georges Corraface
| music          = Toshi Tsuchitori Rabindranath Tagore
| cinematography = William Lubtchansky
| editing        =
| distributor    = 1989
| runtime        = 318 / 171 min.
| country        = Belgium / Australia / U.S.A. / Sweden / Portugal / Norway / Netherlands / Japan / Ireland / Iceland / Finland / Denmark / U.K. / France English
| budget         = $5 million
| preceded by    =
| followed by    =
}} 1989 film original 1985 stage play was 9 hours long, and toured around the world for four years. In 1989, it was reduced to under 6 hours for television (TV mini series). Later it was also reduced to about 3 hours for theatrical and DVD release. The screenplay was the result of eight years work by Peter Brook, Jean-Claude Carrière and Marie-Hélène Estienne. For the casting an international selection of actors was intentionally chosen, to show that the nature of the Indian epic is the story of all humanity.

==Plot==
 
In general terms, the story involves epic incidents between two warring families, the Pandavas (representing the good side) and the Kauravas (representing the bad side). Both sides, being the offspring of kings and gods, fight for dominion. They have both been advised by the god Krishna to live in harmony and abstain from the bloody lust for power. Yet their fights come to threaten the very order of the Universe. The plot is framed as a narrative between the Brahmin sage Vyasa and the Hindu deity Ganesha, and directed towards an unnamed Indian boy who comes to him inquiring about the story of the human race.

==Reception==
 
The productions use of an international cast caused heated intercultural debate. Negative criticism came from Indian scholar Pradip Bhattacharya who felt that Brooks interpretation "was not a portrayal of a titanic clash between the forces of good and evil, which is the stuff of the epic...   the story of the warring progeny of some rustic landlord".

==Cast==
*Robert Langdon Lloyd as Vyasa
*Antonin Stahly-Vishwanadan as Boy
*Bruce Myers as Ganesha/Krishna
*Vittorio Mezzogiorno as Arjuna
*Andrzej Seweryn as Yudhishthira
*Mamadou Dioumé as Bhima
*Georges Corraface as Duryodhana
*Jean-Paul Denizon as Nakula
*Mahmoud Tabrizi-Zadeh as Sahadeva
*Mallika Sarabhai as Draupadi
*Miriam Goldschmidt as Kunti
*Ryszard Cieslak as Dhritarashtra Gandhari
*Myriam Tadesse as Gandharis servant
*Urs Bihler as Dushasana
*Lou Bihler as Young Karna
*Jeffrey Kissoon as Karna
*Maurice Bénichou as Kitchaka
*Yoshi Oida as Drona
*Sotigui Kouyaté as Parashurama / Bhishma
*Tuncel Kurtiz as Shakuni
*Ciarán Hinds as Ashwatthama
*Erika Alexander as Madri / Hidimbi
*Bakary Sangaré as The Sun / Rakshasa / Ghatotkacha
*Tapa Sudana as Pandu/Shiva Akram Khan as Ekalavya
*Nolan Hemmings as Abhimanyu
*Hapsari Hardjito as Utari (Abhimanyus wife)
*Mas Soegeng as Virata
*Yumi Nara as Viratas wife
*Amba Bihler as Viratas daughter
*Tamsir Niane as Urvasi Uttara
*Gisèle Hogard as 1st princess
*Julie Romanus as 2nd princess
*Abbi Patricx as Salvi
*Ken Higelin as Deathless boy
*Corinne Jaber as Amba / Sikhandin
*Joseph Kurian as Dhristadyumna
*Clément Masdongar as Gazelle
*Leela Mayor as Satyavati
*Velu Vishwananan as The hermit

==Awards== Performing Arts of the International Emmy Awards and the Audience Award for Best Feature at the São Paulo International Film Festival.

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 