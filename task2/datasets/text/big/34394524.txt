The Sleepwalker (1942 film)
{{Infobox Hollywood cartoon
|cartoon_name=The Sleepwalker Pluto
|image=Sleepwalker pluto.jpg
|caption=Pluto is walking in his sleep
|director=Clyde Geronimi
|story_artist=
|animator=
|voice_actor=Pinto Colvig
|musician=Leigh Harline
|producer=Walt Disney
|studio=Walt Disney Productions
|distributor=RKO Radio Pictures
|release_date=July 3, 1942 (USA)
|color_process=Technicolor
|runtime=7 min (one reel)
|preceded_by=The Army Mascot (1942)
|followed_by=T-Bone for Two (1942) English
}} Pluto starts sleepwalking and, while in this states, gives his bone to Dinah the Dachshund (in her first cartoon appearance), but every time he wakes up, he cant seem to understand how Dinah got a hold of his bone and wants it back.

==Releases==
*1942 &ndash; theatrical release
*1981 &ndash; "Mickey Mouse and Donald Duck Cartoon Collections Volume One" (VHS)
*c. 1992 &ndash; Mickeys Mouse Tracks, episode #42 (TV)
*c. 1992 &ndash; Donalds Quack Attack, episode #43 (TV The Ink and Paint Club, episode #1.12: "The Many Loves of Pluto" (TV)
*2004 &ndash; " " (DVD)

==External links==
* 

 
 
 
 
 
 

 
 