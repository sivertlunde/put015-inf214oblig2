El inquilino
{{Infobox Film
| name           = El inquilino
| image          = El inquilino.jpg
| image_size     =
| caption        = Spanish theatrical release poster
| director       = José Antonio Nieves Conde
| producer       =
| writer         = José Luis Duró José Antonio Nieves Conde José María Pérez Lozano Manuel Sebares
| narrator       =
| starring       = Fernando Fernán Gómez Manuel Alexandre
| music          = Miguel Asins Arbó
| cinematography = Francisco Sempere
| editing        = Margarita de Ochoa
| distributor    = Delta film
| released       =
| runtime        = 90 minutes
| country        = Spain
| language       = Spanish
| budget         =
}}
 1957 Spain|Spanish drama film directed by José Antonio Nieves Conde about how difficult is to live in a flat in Madrid. It was banned by Spanish censors, and when it was re-released two years later, it was cut, the dialogues were removed, and the ending was totally changed. It also failed at the box office.

In the 90s, an un-cut and uncensored print was discovered and restored by Filmoteca Española (Spanish Cinemateque). The film is nowadays considered a classic.

==Plot summary==

 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 