List of lesbian, gay, bisexual or transgender-related films of 1996
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1996. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==List==
{| class="wikitable sortable"
! width="20%" | Title
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|-
|Achilles (film)|Achilles||   ||  || Short||
|-
|Army Daze|| Keng Sen Ong ||  || Comedy ||
|-
|Bámbola|| Bigas Luna ||  || Comedy, drama ||
|- Beautiful Thing||   ||  || Romantic, comedy, drama||
|- Remake of La Cage aux Folles
|- aka Tělo bez duše
|-
|Bound (film)|Bound|| Wachowski brothers ||  || Crime, drama||
|-
|Boyfriends (film)|Boyfriends||  , Neil Hunter ||  || Comedy||
|-
|Butch Camp||   ||  || Comedy||
|-
|Camping Cosmos||   ||  || Comedy||
|-
|Crash (1996 film)|Crash||   ||     || Horror, drama ||
|-
| ||   ||   || Short, drama, romance ||
|- The Delta||   ||  || Drama ||
|-
|Different for Girls||   ||    || Comedy||
|-
|East Palace, West Palace||   ||   || Drama||
|- The Escort||   ||   || Comedy || aka Lescorte ||
|-
|Fire (1996 film)|Fire||   ||     || Drama||
|-
|Flow (1996 film)|Flow||   ||     || Drama||
|-
|Get on the Bus||   ||  || Drama||
|-
|Green Plaid Shirt||   ||  || Drama, romance||
|-
|Hollow Reed||   ||      || Drama||
|-
|Hustler White||  , Bruce LaBruce ||     || Romantic, drama||
|- Indian Summer||   ||  || Comedy, drama||
|-
|I Shot Andy Warhol||   ||       || Biography, drama||
|-
|Istanbul Beneath My Wings||   ||   || Drama, romance, adventure||
|-
|Its My Party (film)|Its My Party||   ||  || Drama||
|-
|Jim Loves Jack||   ||  || Documentary ||
|-
|johns (film)|johns||   ||  || Drama||
|-
|Killer Condom||   ||     || Romantic, comedy, horror||
|- 
|Life and Death on the A-list||   ||   || Documentary||
|-
|Lilies (film)|Lilies||   ||   || Romantic, drama||
|-
|Más que amor, frenesí||  , Miguel Bardem, David Menkes ||  || Comedy, drama, thriller ||
|- Regular Guys|| aka Echte Kerle
|-
|Love and Other Catastrophes||   ||   || Romantic, comedy, drama||
|-
|My Night with Reg||   ||   || Drama||
|-
|Pianese Nunzio, Fourteen in May||   ||   || Drama||
|-
|Red Ribbon Blues||   ||  || Comedy,drama||
|- Set It Off||   ||  || Action, crime, drama||
|-
|Sling Blade||   ||  || Drama||
|- Skin & Bone||   ||  || Comedy||
|-
| ||   ||  || Short||
|-
|Tender Fictions||   ||  || Documentary||
|-
|The Toilers and the Wayfarers || Keith Froelich ||   || Drama || Matt Klemp   Andrew Woodhouse  Runaway hustlers in Minneapolis.  
|-
| ||   ||  || Documentary, drama||
|}

 

 
 
 