2061: An Exceptional Year
{{Infobox film
| name = 2061 – An exceptional year
| image =2061 – An exceptional year.jpg
| caption =
| director = Carlo Vanzina
| producer =
| writer = Carlo Vanzina Enrico Vanzina Diego Abatantuono
| starring = Diego Abatantuono 
| music = Federico De Robertis
| cinematography =Claudio Zamarion
| editing = Raimondo Crociani
| studio = International Video 80 Rai Cinema
| distributor = 01 Distribuzione (Italy)
| released =  
| runtime = 100 minutes
| country = Italy
| language = Italian
| budget =
}}
 2007 Italian comedy film directed by Carlo Vanzina.   

==Plot==
 . Italy as a nation no longer exists, the peninsula has returned to being a divided country, almost pre-Risorgimento, where now reigns the political situation is similar to that before the reunification of 1861:
* The secessionist of Lega Nord have conquered Milan, creating the Lombard Republic with Turin as capital, defended by a high wall erected on the Po River to block the entrance to the "southerners";
* In Emilia-Romagna has been proclaimed "Peoples Republic of Sickle and Mortadella", which is famous for mortadella, rubles and dance halls;
* Tuscany is back to being a Grand Duchy, where the houses Della Valle and Cecchi Gori are struggling for power; Papal State is reborn, a fundamentalist regime dominated by the Inquisition;
* The South has been invaded instead by Africans, who have created the rich "Sultanate of the Two Sicilies", an Islamic state where Arabic is the official language.
 Classical Lyceum Massimo DAlema of Turin) undertakes a difficult journey to the Piedmont, in order, two hundred years later, to remake Italy.

==Cast==

*Diego Abatantuono: Professor Ademaro Maroncelli
*Emilio Solfrizzi: Nicola Cippone
*Sabrina Impacciatore: Mara Pronesti
*Dino Abbrescia: Tony
*Stefano Chiodaroli: Grosso
*Jonathan Kashanian: Pride
*Paolo Macedonio: Salvim
*Antonello Costa: Taned
*Andrea Osvárt: Unna
*Michele Placido: Cardinal Bonifacio Colonna
*Anna Maria Barbera: Nunzia La Moratta	
*Massimo Ceccherini: Cosimetto Delli Cecchi
*Nini Salerno: Marchese di Villa Sparina
*Ugo Conti: Shrek
*Enzo Salvi: Becchino

== References ==

 

== External links ==
*  
 

 
 
 


 
 