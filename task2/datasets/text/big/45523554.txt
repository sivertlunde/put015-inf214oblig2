Mayeya Musuku
{{Infobox film
| name           = Mayeya Musuku
| image          =
| alt            =
| caption        =
| film name      = ಮಾಯೆಯ ಮುಸುಕು
| director       = B. Y. Ramdas
| producer       = N. Umapathi H. K. Srinivasa B. Y. Ramdas
| writer         = B. Shamasundara (dialogues)
| screenplay     = B. Shamasundara
| story          = B. Shamasundara (Based on Story Haavu Matthu Huttha)
| based on       = Rajesh Srilalitha T. N. Balakrishna B. S. Nithyanand
| narrator       =
| music          = T. A. Mothi
| cinematography = D. V. Rajaram Meenakshi Sundaram
| editing        = Sriramulu
| studio         = Kalashree Cine Productions
| distributor    = Kalashree Cine Productions
| released       =  
| runtime        = 92 min
| country        = India Kannada
| budget         =
| gross          =
}}
 1980 Cinema Indian Kannada Kannada film, directed by B. Y. Ramdas and produced by N. Umapathi, H. K. Srinivasa and B. Y. Ramdas. The film stars Rajesh (Kannada actor)|Rajesh, Srilalitha, T. N. Balakrishna and B. S. Nithyanand in lead roles. The film had musical score by T. A. Mothi.  

==Cast==
  Rajesh
* Srilalitha
* T. N. Balakrishna
* B. S. Nithyanand
* Shashikala
* B. Shyamasundara
* S. V. Chalam
* Veerendra Kumar
* Sundaramma
* Indrani
* Rathnamma
* Bhagya
* Jamuna Geetha
* Hemavathi
* Madevi
* Rani
* V. Sathyanarayana
* Dr Sadanand
 

==References==
 

==External links==
*  
*  
*  

 
 

 