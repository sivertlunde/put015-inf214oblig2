Colma: The Musical
{{Infobox film
| name = Colma: The Musical
| caption =
| image = Colma- The Musical FilmPoster.jpeg
| director = Richard Wong
| producer = Richard Wong Paul Kolsanoff
| screenplay = H.P. Mendoza
| story      = H.P. Mendoza Richard Wong
| starring = Jake Moreno H.P. Mendoza L.A. Renigen Sigrid Sutter Larry Soriano Brian Raffi
| music = H.P. Mendoza
| cinematography = Richard Wong
| editing = Richard Wong
| studio  = Greenrocksolid
| distributor = Roadside Attractions
| released =  
| runtime = 100 minutes
| country = United States
| language = English
| budget =
| gross = $41,131
}} musical independent film directed by Richard Wong and written by H.P. Mendoza. Wongs feature directorial debut, shot on location in the city of Colma, California and parts of San Francisco, is a coming of age story based on the lives of and the relationships between three teenagers living in Colma and how they deal with newfound problems that challenge their friendship. Along the way, they also learn what to hold on to and how best to follow their dreams.

The film features 13 songs all written and produced by H.P. Mendoza. Colma: The Musical was released through Roadside Attractions in partnership with Lions Gate Entertainment.

The film premiered March 21, 2006 at the San Francisco International Asian American Film Festival. After a year of touring the film festival circuit and winning three Special Jury Prizes, Colma: The Musical was theatrically released on June 22, 2007.

==Plot== coming out of the closet to his single father (Larry Soriano), while Maribel struggles to figure out what to do next in life.

==Cast==
* Jake Moreno as Billy
* H.P. Mendoza as Rodel
* L.A. Renigen as Maribel
* Sigrid Sutter as Tara
* Larry Soriano as Rodels father
* Brian Raffi as Julio
* Gigi Guizado as Kattia
* Paul Kolsanoff as Kevin
* Allison Torneros as Amanda
* Kat Kneisel as Joanna
* David Scott Keller as Michael

==History== Arrested Development and was looking for a script to direct.  When Wong listened to a track from Mendozas concept album, he asked Mendoza how long it would take to turn it into a script.  After seven days, a first draft was born, Mendoza flew from Philadelphia to San Francisco and the plans were set in motion.  After 18 days of shooting and several months of editing, a feature film was made for a budget of roughly $15,000.  The film, called "an itty-bitty movie with a great big heart"  by The New York Times went on to win three Special Jury Prizes on the film festival circuit and was acquired by Roadside Attractions and Lionsgate Entertainment.

==Release==
The film was distributed by Roadside Attractions theatrically on June 22, 2007. The DVD was released on November 20, 2007 by Lionsgate Home Entertainment with deleted scenes and an audio commentary by director Richard Wong and writer H.P. Mendoza.

==Reception==
The film received mostly favorable reviews and the review aggregation website Rotten Tomatoes shows Colma: The Musical with a 90% "fresh" among all reviews.
 premiered at the San Francisco International Asian American Film Festival and won its Special Jury Prize for Best Narrative Feature.  The premiere was held at the AMC Kabuki 8 in its 700 seat house which was packed with people who knew nothing about the filmmakers, but possibly appeared based on the title of the film alone.  The film was praised by young Asian Americans who identified with the lost, if a little wayward, teenagers. 

Manohla Dargis of The New York Times called it "an itty-bitty movie with a great big heart"  while Ruthe Stein of the San Francisco Chronicle said that the film "deserves to be seen for its sheer originality and audacity".   

Negative reviews came from the Village Voice with Julia Wallace saying "its unfortunate that Colma pays so little attention to Colma; it may as well be set anywhere."  while Kyle Smith of the New York Post says "The songs sound like they were recorded on a toy synthesizer in someones basement, and neither of the two male leads can sing." 

After years of life on DVD and online instant viewing via   likens the film to American Graffiti and Diner (film)|Diner,  while Jurgen Fauth of About.com compared the film to Mallrats, Ghost World, and Once (film)|Once. 

==Awards and nominations==
* Special Jury Prize - San Francisco International Asian American Film Festival
* Special Jury Prize - Los Angeles Asian Pacific Film Festival
* Special Jury Prize - San Diego Asian Film Festival
* Gotham Award - Best Film Not in Theaters (Nomination)
* Independent Spirit Award - Someone to Watch; Richard Wong (Nomination)

==See also== Fruit Fly (2009), film directed by H.P. Mendoza

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 