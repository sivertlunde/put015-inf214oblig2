A Room with a View (1985 film)
 
 
{{Infobox film
| name = A Room with a View
| image = Room_with_a_view.jpg
| image_size = 
| alt = 
| caption = Theatrical release poster James Ivory
| producer = Ismail Merchant
| screenplay = Ruth Prawer Jhabvala
| based on =  
| starring = {{Plainlist|
* Maggie Smith
* Denholm Elliott
* Judi Dench
* Simon Callow
* Helena Bonham Carter
* Julian Sands
* Daniel Day-Lewis
* Fabia Drake
* Patrick Godfrey
* Rupert Graves
* Joan Henley
* Rosemary Leach
}} Richard Robbins (score) Giacomo Puccini (sung by Kiri Te Kanawa)
| cinematography = Tony Pierce-Roberts
| editing = Humphrey Dixon Film Four International
| distributor = Curzon Film Distributors  
| released =  
| runtime = 117 minutes
| country = United Kingdom
| language = English
| budget = £2.3 million "Bad Beginning." Sunday Times   15 June 1986: 45. The Sunday Times Digital Archive. Web. 8 Apr. 2014. 
| gross = $20,966,644 
}} romance drama James Ivory 1908 novel of the same name. The film follows closely the novel by use of the chapter titles to section the film into thematic segments.  Set in England and Italy, it is about a young woman in the restrictive and repressed culture of Edwardian era England and her developing love for a free-spirited young man.

==Plot== Victorian times. At first, the Emersons seem strange and unfamiliar to Lucy and Charlotte. They seem sincere but unaware of finer upper class Victorian manners. Mr. Emerson offers to switch rooms with the women, who desire a room with a view. Charlotte is offended, believing him to be rude and tactless for what she perceives to be indebting them with his offer.  As Lucy begins her journey to maturity, she finds herself drawn to George due to his mysterious thinking and readily expressed emotions.

A number of people staying at the pension take a carriage ride in the country. A mischievous Italian driver gets back at Charlotte by misdirecting an unchaperoned Lucy to George in a barley field as he admires the view. George suddenly embraces and passionately kisses Lucy as she approaches him. Charlotte has followed Lucy, witnesses the act, and quickly stops the intimacy. Georges unreserved passion shocks Lucy, but also lights a secret desire and romance in her heart. Charlotte suggests George kissing her was the act of a rake. Charlotte makes reference to a heartbreak from her youth that occurred the same way and has behaved accordingly with disgust and anger toward George. Charlotte uses guilt to coerce Lucy to secrecy to save both their reputations as a young lady and a chaperone, but it is mostly for her own benefit. Normally, if a young man kissed a young lady, an engagement should be announced to preserve her reputation, but Charlotte considers George to be an undesirable influence. Upon returning to England, Lucy tells her mother nothing and pretends to forget the incident. She accepts a marriage proposal from a wealthy and respectable but snobbish man named Cecil Vyse (Daniel Day-Lewis). However, she soon learns that both George and his father have moved to her small village and will be her neighbors due to a letter from Cecil Vyse inviting them to reside in an empty cottage. 

The appearance of George soon disrupts Lucys plans and causes her suppressed feelings to resurface, complicated by the supposed need for secrecy. Lucy consistently refuses Georges pursuit of her, but mysteriously breaks off her engagement to Cecil, and makes plans to visit Greece. George has also decided that he must move for peace of mind and makes arrangements. Lucy stops by Reverend Beebes and is confronted by Georges father before they are to leave town. She suddenly realizes that the only reason that she planned to travel was to escape her feelings for George. At the end, we see George and Lucy eloped in the Italian pension where they met, in the room with the view.

==Cast==
 
*Helena Bonham Carter as Lucy Honeychurch
*Julian Sands as George Emerson
*Maggie Smith as Charlotte Bartlett
*Denholm Elliott as Mr. Emerson
*Daniel Day-Lewis as Cecil Vyse
*Simon Callow as The Reverend Mr. Beebe
*Rosemary Leach as Mrs Honeychurch, Lucys mother
*Rupert Graves as Freddy Honeychurch, Lucys brother
*Patrick Godfrey as The Reverend Mr. Eager
*Judi Dench as Eleanor Lavish, a novelist
*Fabia Drake as Miss Catharine Alan
*Joan Henley as Miss Teresa Alan
*Amanda Walker as The Cockney Signora
*Maria Britneva as Mrs Vyse, Cecils mother
*Mia Fothergill as Minnie Beebe
*Peter Cellier as Sir Harry Otway, a landlord
 

==Filming== Foxwold House, Chiddingstone. Lucy and Cecil take a walk through the village after their engagement party. They stop at St Marys Church to speak to Mr Beebe. Later in the film the Emersons rent a house in the village, Mr Beebes house is also in the village behind the church. It is there that Lucy and Mr Emerson talk about her relationship with his son at the end of the film. Lucys engagement party was filmed in the grounds of Emmetts Garden. 
==Box office==
The film made $4.4 million at the US box office in the first 12 weeks of release. 

==Awards==
;Wins Best Costume John Bright) Best Adapted Screenplay (Ruth Prawer Jhabvala)  Best Supporting Best Costume Best Film Best Production Design (Brian Ackland-Snow) http://www.imdb.com/title/tt0091867/awards 
* Evening Standard British Film Awards: Best Film (James Ivory), Best Technical/Artistic Achievement (Tony Pierce-Roberts) 
*   (Maggie Smith)  Best Supporting Actress (Maggie Smith) 
* London Critics Circle Film Awards: Best Film (James Ivory)  Best Supporting Actor (Daniel Day-Lewis)  Best Supporting Actor (Daniel Day-Lewis) 
*   (Ruth Prawer Jhabvala) 

;Nominations Best Supporting Best Picture Best Cinematography Best Director (James Ivory)  Best Supporting Best Cinematography Best Direction Best Editing Best Score Best Adapted Best Sound 
*   (James Ivory)  Best Motion Picture – Drama 

==Soundtrack== London PO, Sir John Pritchard
# "The Pensione Bertollini"
# "Lucy, Charlotte, and Miss Lavish See the City"
# "In the Piazza Signoria"
# "The Embankment"
# "Phaeton and Persephone" London PO, Sir John Pritchard
# "The Storm"
# "Home, and the Betrothal"
# "The Sacred Lake"
# "The Allan Sisters"
# "In the National Gallery"
# "Windy Corner"
# "Habanera (aria)|Habanera" (from Carmen by Georges Bizet)
# "The Broken Engagement"
# "Return to Florence"
# "End Titles"

* Original music composed by Richard Robbins
* Soundtrack album produced by Simon Heyworth
* Arrangements by Frances Shaw and Barrie Gurad
* Music published by Filmtrax PLC

==See also==
 
* Baedeker, a travel guide mentioned several times in the film
* Chiddingstone Castle, used as a location for the film

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 