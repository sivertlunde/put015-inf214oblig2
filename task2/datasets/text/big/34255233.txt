Portland (film)
 
{{Infobox film
| name           = Portland
| image          = Portland (film).jpg
| caption        = Film poster
| director       = Niels Arden Oplev
| producer       = Peter Aalbæk Jensen Ib Tardini
| writer         = Niels Arden Oplev
| starring       = Anders W. Berthelsen, Ulrich Thomsen, and Iben Hjejle Morten Olsen 
| cinematography = Henrik Jongdahl
| editing        = Henrik Fleischer Zentropa Entertainments 
| released       =  
| runtime        = 103 minutes 
| country        = Denmark
| language       = Danish
| budget         = 
}}

Portland is a 1996 Danish drama film written and directed by Niels Arden Oplev, in his feature film debut.  It stars Anders W. Berthelsen, Ulrich Thomsen, and Iben Hjejle.

The film, whose title is a reference to a Danish brand of Portland cement|cement,  was selected for competition at the 46th Berlin International Film Festival.   

==Plot==
The film is set in northern Denmark    and depicts the "violent drug-fueled adventures of two brothers, Janus (Anders Wodskou Berthelsen) and Jakob (Michael Muller) at the bottom of Denmarks social ladder." 

==Cast==
* Anders W. Berthelsen as Janus Michael Muller as Jakob, the brother of Janus
* Ulrich Thomsen as Lasse, the gang leader
* Iben Hjejle as Eva, the sister of Lasse
* Birthe Neumann as Lasses mother
* Baard Owe as Kaj
* Edith Thrane as Mrs. Eriksen
* Helle Dolleris as Irene
* Susanne Birkemose Kongsgaard as Minna
* Karsten Belsnæs as Kenneth
* Preben Raunsbjerg as Johnny

==Reception== nihilistic practical joke|prank. But although flashy, it has fundamental weaknesses. Partly because the actor playing him has no dramatic range, the transformation of Jakob from softhearted reform school|reform-school punk into sadistic iron man isnt the slightest bit convincing. Mr. Berthelsens lank-haired, pill-popping Janus, however, is all too real. As this connoisseur of pain punches and lurches his way through the film, you see exactly how antisocial impulses can be warped into a code of outlaw values."    

David Stratton called it "violent, nihilistic and often repellent, and yet its bold visuals and unexpected elements of humor and romance make it riveting viewing."    According to Stratton, "as a director, Oplev shows he has talent: his mixture of moods works well, he gets strong performances from most cast members, and he pushes the narrative along at an urgent pace....But as a writer, he’s less successful; it’s surely not enough these days for Janus to blame his lifestyle on a lack of mothers love (which he does) or to have characters utter corny lines like “It’s us against the world.” There’s also far too much unmotivated violence (Janus assaulting a shopping mall security guard is a totally unnecessary sequence) and a few cheap and obvious jokes at the expense of authority figures." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 