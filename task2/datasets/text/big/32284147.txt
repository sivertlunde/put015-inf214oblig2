Vellore Maavattam
{{Infobox film
| name           = Vellore Maavattam
| image          = 
| caption        =
| director       = R. N. R. Manohar
| producer       = Kalpathi S. Aghoram Kalpathi S. Ganesh Kalpathi S. Suresh
| story          = R. N. R. Manohar
| screenplay     = R. N. R. Manohar Nandha Poorna Santhanam
| music          = Sundar C Babu
| cinematography = Vetri
| editing        = Suraj Kavee
| studio         = AGS Entertainment
| distributor    = AGS Entertainment
| released       =  
| country        = India
| budget         =   5.5 crores   
| runtime        = 
| language       = Tamil
}}
 Nandha and Poorna. Produced by AGS Entertainment,  the film released on 5 October 2011 on the occasion of the festival of Ayudha Pooja.

==Cast== Nandha as Muthukumar IPS Poorna (Shamna kasim) as Priya
* Azhagam Perumal as Gurumurthy
* S.Neelakantan as Nachiappan Santhanam as Kumaravelu
* G. M. Kumar
* Singamuthu
* Mayilsamy
* Vaiyapuri

==Soundtrack==
{{Infobox album|  
  Name        = Vellore Maavattam
| Type        = Soundtrack
| Artist      = Sundar C Babu
| Recorded    = 2011 Feature film soundtrack
| Length      =  Tamil
| Label       = 
| Producer    =  Sundar C Babu
| Reviews     =
}}
The tunes for the album are composed by Sundar C Babu.
{{tracklist
| headline        = Track listing 
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = yes
| title1          = Adikuthu Adikuthu
| lyrics1         = Na. Muthukumar
| extra1          = Senthuil Das, Manikka Vinayagam, Navven Madhav, Maanasa, Malgudi Subha
| length1         = 
| title2          = Vaanam Ellam
| lyrics2         = Kavivarman 
| extra2          = Hariharan (singer)|Hariharan, Shruthi
| length2         = 
| title3          = Unnai Unnai
| lyrics3         = Thamarai
| extra3          = Krish (singer)|Krish, Mahathi
| length3         = 
| title4          = Party Vandhale
| lyrics4         = Ezhil Arasu  Shaan
| length4         = 
| title5          = Kannala Parkurathum
| lyrics5         = Viveka
| extra5          = Anitha
| length5         = 
| title6          = Saxy Vaanam Ellam
| lyrics6         = Instrumental
| extra6          = Martyn
| length6         = 
}}

==References==
 

 
 
 


 