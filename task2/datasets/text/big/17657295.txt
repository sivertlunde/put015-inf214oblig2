Macon County Line
{{Infobox film
| name           = Macon County Line
| image          = Film_Poster_for_Macon_County_Line.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Richard Compton
| producer       = Max Baer, Jr. Roger Camras   Richard Franchot  
| writer         = Max Baer, Jr. Richard Compton
| narrator       = Geoffrey Lewis Joan Blackman Leif Garrett James Gammon Stu Phillips
| cinematography = Daniel Lacambre
| editing        = Tina Hirsch
| distributor    = American International Pictures 
| released       =  
| runtime        = 89 minutes
| country        = United States English
| budget         = $225,000
| gross          = $30 million 
}}
Macon County Line is a 1974 American independent film directed by Richard Compton and produced by Max Baer, Jr. Baer and Compton also co-wrote the film, in which Baer stars as a vengeful county sheriff in Alabama out for blood after his wife is brutally killed by a pair of drifters. 

The $225,000 film reportedly became the single  . Retrieved July 7, 2012.  and over $30 million worldwide. {{cite web
 | last = Mavis
 | first = Paul
 | authorlink = 
 | coauthors =
 | title = DVDTalk.com Macon County Line Review
 | work =
 | publisher =
 | date = 6 May 2008
 | url = http://www.dvdtalk.com/reviews/33142/macon-county-line/
 | format =
 | doi =
 | accessdate = 23 May 2012}} 
 Hollywood revisionist Walking Tall of 1973), its plot and characters are entirely fictional. {{cite video
 | people = Richard Compton
 | title = Macon County Line (1973)
 | medium = DVD
 | publisher = Anchor Bay
 | date = 2000}} 

==Plot==
  South before their upcoming stint in the Air Force. A pair of Chicago transplants, Wayne applied for service when his brother Chris was given the option of military service or prison as the result of an earlier episode with the law. Driving through Louisiana, the brothers pick up hitchhiker Jenny Scott (Cheryl Waters), a pretty blond with a shady backstory that she would rather not discuss.
 Geoffrey Lewis).

Waiting at the garage, they are informally threatened by Morgan, who says they could be picked up for vagrancy if they decided to stick around. Not interested in trouble, the brothers and Jenny head out once their car is running. But another breakdown – this time near a fresh crime scene of a rape and murder of Morgans wife by two men who also have killed a cop – puts the trio into a lethal situation with Morgan. There is a devastating finale for all.

==Production notes==
While the poster advertising the film included the tagline “It shouldnt have happened. It couldnt have happened. But it did,” and the title card states that it is a true story (and several reviewers have stated the same), director Richard Compton and producer Max Baer have said that they wrote the original story without any basis in historic events. {{cite video
 | people = Richard Compton
 | title = Macon County Line (1973)
 | medium = DVD
 | publisher = Anchor Bay Jackson County Jail and The Town That Dreaded Sundown). In each case, most, if not all, of what was portrayed on screen was fictional -with the exception of "The Town That Dreaded Sundown", which is inspired by the Phantom Killer murders in Texarkana, Texas, in 1946.

Alan Vint and Jesse Vint, who played brothers Chris and Wayne Dixon onscreen, are brothers.

==Home video releases==
Anchor Bay released the film on both VHS and DVD in February 2000. The Anchor Bay DVD release included an audio commentary with director Richard Compton and the featurette, Macon County Line – 25 Years Down the Road. Both the VHS and DVD have been out of print since 2007.

The Warner Home Video DVD was issued on May 6, 2008. It uses the same transfer from the 2000 DVD release and is single-layered including subtitles - with no extra features of any kind.  {{cite web
 | last = Mavis
 | first = Paul
 | authorlink = 
 | coauthors =
 | title = Macon County Line: Review
 | work =
 | publisher =
 | date = 6 May 2008
 | url = http://www.dvdtalk.com/reviews/33142/macon-county-line/
 | format =
 | doi =
 | accessdate = 23 May 2012}} 

==Legacy==
Richard Compton directed the film Return to Macon County, released theatrically in 1975. Despite its title, the film is not a sequel, although it loosely follows a similar plot of mistaken identity.

== References ==
 

== External links ==
* 

 
 
 
 
 
 