This England (film)
{{Infobox film
| name           = This England
| image          =
| caption        = David MacDonald
| producer       = John Corfield
| writer         = A.R. Rawlinson  Bridget Boland Emlyn Williams
| starring       = John Clements Constance Cummings Emlyn Williams
| music          = Richard Addinsell Orchestration, Roy Douglas Direction, Muir Mathieson
| cinematography = Mutz Greenbaum
| editing        = 
| studio         = British National Films
| distributor    = World Pictures Corporation
| released       =  
| runtime        = 84 mins
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British historical historical drama David MacDonald and starring John Clements, Constance Cummings and Emlyn Williams.  The film follows the small English village of Cleveley and its historic resistance against tyrannical invaders recounted by one of the inhabitants to a visiting American journalist.

==Production== Richard II by William Shakespeare.

==Partial cast==
*John Clements - John Rookeby
*Constance Cummings - Ann
*Emlyn Williams - Appleyard
*Frank Pettingell - Gage
*Roland Culver - Steward
*Morland Graham -  Doctor
*Leslie French - Johnny Martin Walker -  Seigneur
*Ronald Ward - Lord Clavely
*Hugh Wakefield - Vicar
*Esmond Knight - Vicars son
*Amy Veness - Jenny
*Roddy McDowall - Hugo
*Dennis Wyndham - Martin

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 