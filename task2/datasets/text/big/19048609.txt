Action for Slander
 {{Infobox film
| name           = Action for Slander
| caption        =
| image	=	Action for Slander FilmPoster.jpeg
| director       = Tim Whelan
| producer       = Victor Saville   Alexander Korda
| writer         = Mary Borden (novel)   Ian Dalrymple   Miles Malleson
| starring       = Clive Brook Ann Todd Margaretta Scott
| music          = Muir Mathieson
| cinematography = Harry Stradling Hugh Stewart
| studio         = London Film Productions
| distributor    = United Artists
| released       =  
| runtime        = 83 mins
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 Action for Slander by Mary Borden.

==Synopsis== country house attended by a number of prominent figures including businessmen and politicians as well as Captain Bradford and his wife. The tension between Bradford and Daviot is obvious during grouse shooting as Bradford is clearly aware of Daviots affair with his wife.

That evening, during a game of cards played for high stakes, Daviot is accused of cheating by Grant, a drunken player who has lost large amounts of money, a charge that is dismissed out of hand by the other players until Bradford seconds it. None of the other players believe the accusation, even though they are unaware of the grudge that Bradford has against Daviot. Bradford sticks to his story, even in the face of legal action from Daviot.

The other guests frightened of their own reputations if the scandal becomes widely known, persuade all to hush the matter up. Daviot agrees to keep quiet for all their sakes, even though he still wants to clear his name. Daviot proposes to Josie that she leave her husband and live with him in spite of the scandal, but her lukewarm response leads him to realise that her interest in him is shallow. She subsequently reconciles with her husband and they go abroad to spend time together.
 Northwest Frontier is dashed and he begins to consider suicide.

Ann Daviot, meanwhile, has been touring around Continental Europe aimlessly, possibly never to return to Britain. As soon as she hears he is in trouble she returns to help him, but he is unresponsive and derides her as an "Angel of Mercy". Eventually she goads him into facing his accusers, and he initiates court proceedings on the understanding that if he loses he will be allowed to take gentlemens way out with a pistol. With the help of his barrister Sir Quinton Jessops, Daviot attempts to clear his name by suing Bradford and Grant for slander.

==Production== Action for Slander by Mary Borden that was released the same year. 

===Reception===
The film was popular at its release and it was re-released several times during the 1940s. However, it has later been criticised as "stilted". Rachael Low describes it as being "well-made and acted" although the "behaviour of the characters was too far-fetched to carry conviction". 

==Cast==
* Clive Brook - Major George Daviot
* Ann Todd - Ann Daviot
* Margaretta Scott - Josie Bradford
* Arthur Margetson - Captain Hugh Bradford
* Ronald Squire - Charles Cinderford
* Athole Stewart - Lord Pontefract
* Percy Marmont - William Cowbit Frank Cellier - Sir Bernard Roper Anthony Holles - John Grant
* Morton Selten - Judge Trotter
* Kate Cutler - The Dowager
* Enid Stamp-Taylor - Jenny
* Francis L. Sullivan - Sir Quinton Jessops
* Felix Aylmer - Sir Eustace Cunninghame
* Laurence Hanray - Clerk of Court
* Gus McNaughton - Tandy
* Googie Withers - Mary
* Albert Whelan - Ropers butler
* Allan Jeayes - Colonel

==References==
 

==External links==
* 

==Bibliography==
* Low, Rachael. The History of British Film: Volume VII. Routledge, 1997.
* White, Terry. Justice Denoted: The Legal Thriller in American, British, and Continental Courtroom Literature. Praeger, 2003.

 
 
 

 
 
 
 
 
 
 