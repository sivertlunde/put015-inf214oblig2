Bootmen
 
 
{{Infobox film
| name           = Bootmen
| image          = Bootmen poster.jpg
| caption        = Theatrical poster for
| director       = Dein Perry
| producer       = Hilary Linstead Antonia Barnard
| writer         = Steve Worland Hilary Linstead Dein Perry
| starring       = Adam Garcia Sophie Lee Sam Worthington Steve Mason
| editing        = Jane Moran
| music          = Cezary Skubiszewski
| distributor    = 20th Century Fox Distribution (Australia) Fox Searchlight Pictures (USA)
| released       =  : 6 October 2000
| runtime        = 92 minutes
| country        = Australia
| language       = English
| budget         =
| gross          = $2,720,302
}} Newcastle by Steve Mason who won two cinematography awards in the 2000 AFI awards and the 2001 FCCA Awards. It stars Adam Garcia, Sophie Lee, Sam Worthington.

The film was released in Australia on 5 October 2000 and was Dein Perrys debut film, who was previously involved with stage shows such as Tap Dogs and Steel City. It is also known as Tap Dogs in Japan.

==Cast==
*Adam Garcia as Sean Okden
*Sophie Lee as Linda
*Sam Worthington as Mitchell Okden Richard Carter as Gary Okden
*Andrew Kaluski as Colin
*Christopher Horsey as Angus
*Lee McDonald as Derrick
*Matt Lee as Johnno
*William Zappa as Walter
*Susie Porter as Sara Anthony Hayes
*Justine Clarke
*Grant Walmsley
*Andrew Doyle
*Bruce Venables

==Awards and nominations==

===Awards won===
*Australian Cinematographers Society (2001):
**Award of Distinction (awarded to Feature Productions Cinema - Steve Mason)
*Australian Film Institute (AFI) (2000):
**Best Achievement in Cinematography: Steve Mason
**Best Achievement in Costume Design: Tess Schofield
**Best Achievement in Production Design: Murray Picknett
**Best Achievement in Sound: David Lee, Laurence Maddy, Andrew Plain, Ian McLoughlin
**Best Original Music Score: Cezary Skubiszewski
*Film Critics Circle of Australia Awards (FCCA) (2001):
**Best Cinematography: Steve Mason
**Best Editing: Jane Moran
**Best Music Score: Cezary Skubiszewski (tied with Edmund Choi for The Dish (2000)).

===Award nominations===
*Australian Film Institute (AFI) (2000):
**Best Achievement in Editing: Jane Moran
**Best Film: Hilary Linstead
**Best Performance by an Actor in a Leading Role: Sam Worthington

==DVD release==
 
The film was released on home video on 27 February 2001 by Fox Home Entertainment.

==Soundtrack==
 
The Bootmen Soundtrack was released by RCA Victor in 2000 and composed by Cezary Skubiszewski and other various artists.

# Rumble - You Am I
# Opening Sequence - Cezary Skubiszewski
# Strange Human Beings - Regurgitator Paul Kelly
# My Family - Banana Oil
# Sign Post - Grinspoon
# Love Theme - Cezary Skubiszewski
# Radio Loves This - Deborah Conway
# Hit Song - Custard
# Giveway - Supaskuba
# Better Off Dead - Grinspoon
# Dont It Get You Down - Deadstar Paul Kelly
# Nipple - Icecream Hands
# Deeper Water - Deadstar
# Finale Part 2 - Cezary Skubiszewski Oblivia
# "Even When Im Sleeping" – Leonardos Bride
# Junk - You Am I
# Tap Forge - Dein Perry

==Box office==
Bootmen grossed $2,720,302 at the box office in Australia. 

==See also==
* Cinema of Australia
* Tap Dogs
* 
* 
* 
* 
* 
* 
* 

==References==
 

==External links==
* 
*  at the Internet Movie Database
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 