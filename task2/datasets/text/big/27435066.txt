Shikkar
 
 
{{Infobox film
| name = Shikkar: The Hunt
| image = Shikkar_the_hunt.jpg
| alt =  
| caption =
| director = M. Padmakumar
| producer = K. K. Rajagopal
| writer = S. Suresh Babu Ananya Sneha Sneha Kailash Kailash
| music =  
| cinematography = Manoj Pillai
| editing =
| studio = Sreeraj Cinema Aashirvad Release Maaxlab Europe: PJ Entertainments Europe
| released =   
| runtime = 165 minutes
| country = India
| language = Malayalam
| budget =    
}}
 Sneha in Ramzan release on 9 September 2010.The film got positive reviews from the critics as well as from the audiences and emerged as the second highest grossing malayalam movie of 2010.

==Synopsis==
The story takes place at Chittazha, a mountainous terrain in Idukki where bamboo reeds flourish. After much wandering from place to place, Balraman (Mohanlal), a lorry driver who lost his wife Kaveri (Sneha (actress) |Sneha), has finally settled down with his daughter Ganga(Ananya) at Chittazha. Peace evades him as his past catches up with him, and Balaraman has to take risky methods to safeguard his own life and that of his daughters.

==Cast==
* Mohanlal as Balaraman
* Samuthirakani as Comrade Abdulla Ananya as Ganga Sneha as Kaveri Kailash as Manu
* Thalaivasal Vijay as Muhammad Rawuthar
* Jain Syriac Babu as a mute worker/Abdullas Son
* Lakshmi Gopalaswamy as Rukmini
* Mythili as Gayatri
* Lalu Alex as Sathyan
* Kalabhavan Mani as Maniyappan
* Jagathy Sreekumar as Pilla alias Mathaayi
* Suraj Venjaramood as Barber Kuttappan/Kuttappaswamy
* Reshmi Boban as Ramani
* N. L. Balakrishnan as Balaramans neighbour
* Babu Namboothiri
* Sreelatha Namboothiri as Eli chedathy
* Sadiq as Basheer
* Kochu Preman as Divakaran
* Shaju as Jeep Driver Abu Salim as Police officer
* Appa Haji as Chacko Lal as Dancer Eldho Aashaan
* Kani as Guest

==Production==
===Filming===
The film was mainly shot at various locations in Pooyamkutty near Kothamangalam, Kerala, Telengana(Andhra Pradesh) and Kodaikanal, Tamil Nadu.

==Reception==
The film received mostly positive reviews from critics and public. It was a commercial success even though the film was released along with Pranchiyettan & the Saint.

==Soundtrack==
{{Infobox album
| Name = Shikkar: The Hunt
| Type = Soundtrack
| Artist = M. Jayachandran
| Cover = Shikkar, The Hunt.jpg
| Released =  10 Aug 2010
| Recorded = Music Lounge Studios, Chennai
| Genre = Soundtrack
| Length =
| Label =  Satyam Audios
| Producer = K. K. Rajagopal
| Reviews =
}}

The soundtrack features four songs composed by M. Jayachandran, with lyrics by late Gireesh Puthenchery (This was one of his last works, released after his death) and Bhuvanachandra (Pratikhatinsu - Telugu).

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track # !! Song !! Singer(s)!! Duration
|-
| 1
| "Enthedi Enthedi"
| Sudeep Kumar, K. S. Chithra
| 3:54
|-
| 2
| "Sembakame1"
| Shankar Mahadevan, Malathi Lakshman
| 4:22
|-
| 3
| "Pinne Ennodonnum"
| K. J. Yesudas|Dr. K. J. Yesudas
| 4:36
|-
| 4
| "Pratikhatinsu"
| S. P. Balasubrahmanyam|Dr. S. P. Balasubrahmanyam
| 4:08
|-
| 5
| "Pinne Pinne"
| Dr. K. J. Yesudas, Latha Krishna
| 4:37
|-
| 6
| "Pada Nayichu"
| Biju Narayanan
| 4:08
|}

==Awards== Kerala Film Critics Award
* Best Popular Film 

; Jeassy Foundation Awards
* Best Supperting Actor – Lalu Alex Ananya

; Jaihind TV Film Awards
* Best Male Playback Singer – K. J. Yesudas|Dr. K. J. Yesudas
* Best Lyricist ( ) – Gireesh Puthenchery
* Special Jury Award – Lalu Alex

==References==
 

==External links==
*  


 
 
 