Gibraltar (1938 film)
{{Infobox film
| name           = Gibraltar
| image          =
| caption        =
| director       = Fyodor Otsep
| producer       = 
| writer         = Erich von Stroheim   Jacques Companéez   Hans Jacoby
| starring       =  Roger Duchesne, Viviane Romance and Erich von Stroheim
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 15 November 1943 (USA)
| runtime        = 105 min (France), 96 min (Germany)
| country        = France
| awards         = French
| budget         = 
| preceded_by    =
| followed_by    =
}}


 French drama film directed by Fedor Ozep and starring Viviane Romance, Roger Duchesne, Abel Jacquin and Erich von Stroheim. 

==Cast==
* Viviane Romance - Mercedes
* Roger Duchesne - Robert Jackson
* Abel Jacquin - Frank Lloyd
* Erich von Stroheim - Marson
* Yvette Lebon - Maud Wilcox

==External links==
 


==References==
 

 

 

 
 
 
 