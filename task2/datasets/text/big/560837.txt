Dirty Work (1998 film)
 
{{Infobox film
| name           = Dirty Work
| image          = DirtyWork1998.jpg
| caption        = Theatrical release poster Fred Wolf
| narrator       = Norm Macdonald
| starring       = Norm Macdonald  Artie Lange Jack Warden Traylor Howard Don Rickles Christopher McDonald Chevy Chase Adam Sandler
| director       = Bob Saget
| producer       = Robert Simonds
| cinematography = Arthur Albert
| editing        = George Folsey Jr.
| music          = Richard Gibbs
| distributor    = Metro-Goldwyn-Mayer
| released       = June 12, 1998
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = $13 million
| gross          = $10 million
}}
 comedy buddy film starring Norm Macdonald, Artie Lange, Jack Warden, and Traylor Howard and directed by Bob Saget. In the film, long-time friends Mitch (Macdonald) and Sam (Lange) start a revenge-for-hire business, and work to fund heart surgery for Sams father Pops (Warden). When they take on work for an unscrupulous businessman (Christopher McDonald), in order to be paid, they create a revenge scheme of their own. Adam Sandler makes a cameo appearance as Satan.

The film was the first starring vehicle for Macdonald and Lange and the first feature film directed by Saget, coming one year after he left his long-running role as host of Americas Funniest Home Videos. {{cite news
 |last=Snierson |first=Dan
 |title=Bob Saget returns to Americas Funniest Home Videos for 20th anniversary celebration
 |newspaper=Entertainment Weekly
 |date=September 16, 2008
 |url=http://insidetv.ew.com/2009/09/16/bob-saget-returns-to-americas-funniest-home-videos-for-one-episode/
}} 
 cult classic.  Co-star Artie Lange later became a regular on The Howard Stern Show, where the film was sometimes discussed. 

== Plot == Krazy Glue to the bottom of Mitchs pants.
 swinging lifestyle, cinema with Men In Black (Who Like To Have Sex With Each Other)" to a packed house. The other workers congratulate them and suggest they go into business.
 fictitious number used later on Saturday Night Live. {{cite web
 |url=http://snltranscripts.jt.org/99/99lerectile.phtml
 |title=Transcript of Erectile Dysfunction Ad SNL 25:12
 |date=February 12, 2000
 |work=snltranscripts.jt.org
 |publisher=Patrick Lonergan
 |accessdate=2007-02-02
}} ). Mitch falls for a woman named Kathy (Traylor Howard) who works for a shady used car dealer (David Koechner). After publicly embarrassing the dealer during a live TV commercial, the duo exacts increasingly lucrative reprisals for satisfied customers until they interfere with unscrupulous local property developer Travis Cole (Christopher McDonald).  Cole tricks them into destroying "his" apartment building (actually owned by Mr. John Kirkpatrick, the landlord), promising to pay them enough to save Pops. Afterwards, Cole reneges, revealing that he is not the owner and that he had them vandalize the building so that he could buy it cheaply, evict the tenants (including Kathys grandmother), and build a parking lot for his luxurious new opera house. Unknown to Cole, Mitchs "note to self" {{cite book
 |title=The Best and Worst Films of 1998
 |first=Michael |last=OSullivan
 |newspaper=The Washington Post
 |date=January 1, 1999
 |url=http://www.washingtonpost.com/wp-srv/style/movies/features/bestof98.htm SNL Weekend Update.)  mini-tape recorder captures this confession.

Mitch and Sam plot their revenge on Cole, using the tape to set up an elaborate trap. Using skunks, a loyal army of prostitutes, homeless men, a noseless friend (Chris Farley), brownies with hallucinogenic additives, and Pops, they ruin the opening night of Don Giovanni, an opera sponsored prominently by Cole. With the media present, Mitch plays back Coles confession over the theaters sound system. Cole sees that his public image is being tarnished and agrees to pay the $50,000. In the end, Cole is punched in the stomach, arrested and jailed, his dog is raped by a skunk, Pops gets his operation, and Mitch gets the girl.  Dr. Farthing overcomes his gambling habit but is beaten to death by bookies in the end.

== Cast ==
*Norm Macdonald as Mitch Weaver
*Artie Lange as Sam McKenna, Mitchs friend
*Jack Warden as Pops McKenna, Sams father
*Traylor Howard as Kathy, Mitchs love interest
*Chris Farley as Jimmy (uncredited), Mitch and Sams friend magnate
*Chevy Chase as Dr. Farthing, gambling-addicted heart surgeon

;Cameo appearances
*Don Rickles as Mr. Hamilton, theater owner
*Rebecca Romijn as bearded lady
*John Goodman as Mayor Adrian Riggins (uncredited)
*Adam Sandler as Satan (uncredited)
*Gary Coleman as Himself
*David Koechner as Anton Phillips, a used car dealer Jim Downey as homeless man Fred Wolf as homeless man

This was Farleys last-released film appearance, before his fatal drug overdose. Former SNL writer Downey and former SNL writer/performer Wolf  appeared as homeless men; both writers have collaborated frequently with Macdonald and Sandler.

== Production and Release == Wycliffe College and elsewhere around Toronto, Ontario, Canada, the film was produced for an estimated $13 million. {{cite web
 |title=Box Office Data - Dirty Work
 |work=the-numbers.com
 |url=http://www.the-numbers.com/movies/1998/DIRTY.php
 |accessdate=2007-02-02
}}   American domestic gross was just over $10 million.  No ads for the film were shown on NBC until a week after the films release. {{cite web
 |title=Norm Macdonald Wins "Dirty" War
 |date=June 9, 1998
 |first=Daniel |last=Frankel
 |publisher=E! Online
 |url=http://www.eonline.com/news/norm_macdonald_wins_dirty_war/36539
}} 

In his first appearance on The Howard Stern Show on September 18, 2008, {{cite web
 |title=The Best of the Week September 14-18 - Thursday: Chevy Chase in Studio
 |date=September 18, 2008
 |publisher=howardstern.com
 |url=http://howardstern.com/rundown.hs?j=n&d=1221710400
}}  Chevy Chase discussed the films production and release with Artie Lange. {{cite web
 |title=Chevy Chase Visits. 09/18/08. 7:55am
 |first=Mark|last=Mercer
 |date=September 18, 2008
 |publisher=marksfriggin.com
 |url=http://www.marksfriggin.com/news08/9-15.htm
}}  According to Chase, he was impressed by the original scripts Ribaldry|raunchy, Motion Picture Association of America film rating system#Ratings|R-rated, "over the top" tone (particularly a filmed but ultimately cut gag involving Macdonald and Lange delivering donuts that had been photographed around their genitals)  and, Lange related, went so far as to beg Macdonald not to allow any changes—to "keep it funny." Lange said the studio insisted on a PG-13 rating and moved the films release from February to June, where it fared poorly against blockbusters like Godzilla (1998 film)|Godzilla. {{cite AV media
 |title=Howard Stern Radio Show
 |people=Stern, Howard; Chase, Chevy; Lange, Artie
 |date=September 18, 2008
 |medium=broadcast
 |time=7:55-8:50
}} 

MGM released the film on DVD, in August 1999, and for digital rental/purchase. {{cite web
 |url=http://www.mgm.com/view/Movie/541/Dirty-Work/
 |title=MGMs Official Site for Dirty Work
 |date=August 24, 1999
 |work=mgm.com
 |publisher=Metro-Goldwyn Mayer Studios Inc.
}} 

== Reception ==
The film received mostly negative critical reviews. It was referred to as a "leaden, taste-deprived attempted comedy" and "a desert of comedy" with only infrequent humor in The New York Times.   The Los Angeles Times described it as "a tone-deaf, scattershot and dispiritingly cheesy affair with more groans than laughs", and though Macdonald "does uncork a few solid one-liners", his lack of conviction in his acting "is amusing in and of itself, but it doesnt help the movie much". 
the San Francisco Chronicle recommended the film only for "people who like stupid lowdown vulgar comedy. I had a few good laughs." 

It has a 17% critic rating at Rotten Tomatoes, averaged from 30 reviews.   The film has been described as a "cult classic." {{cite web
 |title=Norm Macdonald - Me Doing Standup (review)
 |publisher=Pitchfork
 |first=Ian |last=Cohen
 |date=July 1, 2011
 |url=http://pitchfork.com/reviews/albums/15587-me-doing-standup/ 
}}  {{cite web
 |publisher=Onion Inc.
 |work=A.V. Club
 |title=Bob Saget (interview)
 |first=Nathan |last=Rabin
 |date= December 2, 2010
 |url=http://www.avclub.com/articles/bob-saget,48509/
}}  In his column, My Year Of Flops, critic Nathan Rabin describes Dirty Work as an example of "the ironic dumb comedy, the slyly postmodern lowbrow gag-fest that so lustily, nakedly embraces and exposes the machinations and conventions of stupid laffers that it becomes a sort of sublime bit of meta-comedy." 
 date rapist," to which Norm Macdonald replied "thats a lot better than saying you have the charm of a regular rapist! A date rapist still has to get a date!"  

== Availability ==
 MGM Home Entertainment.

== References ==
 

== External links ==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 