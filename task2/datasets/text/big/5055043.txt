Daffy Duck's Fantastic Island
{{Infobox Hollywood cartoon
| cartoon_name      = Daffy Ducks Fantastic Island
| series            = Looney Tunes and Merrie Melodies (Daffy Duck) compilation
| image             = Daffy Ducks Fantastic Island.jpg
| caption           = Poster
| director          = Friz Freleng Chuck Jones
| story_artist      = Dave Brain
| animator          = Tom Ray George Germanetti Tom Walsh Dave Brain
| layout_artist     = Phillip DeGuard
| background_artist = Richard H. Thomas
| narrator          = Tim Whintall
| voice_actor       = Mel Blanc  June Foray  Les Tremayne
| musician          = Robert J. Walsh Carl Stalling (classic cartoons) Milt Franklyn (classic cartoons) Eugene Poddany (classic cartoons) William Lava (classic cartoons)
| producer          = Friz Freleng  Hal Geer Jean MacCurdy
| studio            = Warner Bros. Animation
| distributor       = Warner Bros.
| release_date      = August 5, 1983
| color_process     = Color
| runtime           = 78 min
| preceded_by       =  
| followed_by       = Daffy Ducks Quackbusters
| movie_language    = English
}} compilation of animated bridging sequences, hosted by Daffy Duck and Speedy Gonzales. This was the first Looney Tunes compilation film to center on Daffy Duck, as the previous ones had centered on Bugs Bunny.
 John Dunn, compilation of Sunbow Productions animated series based on Hasbro properties).

==Plot==
 Fantasy Island, Tattoo (respectively). They even wear the white suits worn on that show by Mr. Roarke and Tattoo.
                 
The pair, stranded on a desert island for months with nothing on it but a coconut tree (and Daffy sick and tired of eating coconuts), discover a treasure map which leads them to a magical, talking wishing well (voiced by Les Tremayne).  The greedy Daffy proposes to use the power of the well, which obeys the commands of whoever holds the map, to make himself and Speedy rich by selling wishes for a hefty fee, and has the well transform the island into a tourist paradise. (This showcases the modern Daffys short-sightedness along with his greed, as he could have easily used the well himself to wish for all the wealth he desired). As various Warner Bros. animated characters step up for their chance at the well, their wishes are fulfilled through the events of a classic Warner cartoon. 
                    Tasmanian Devil, search for the map, which originally belonged to them (they had earlier lost their ship in a battle with Bugs Bunny). They find out that someone who took the Map has black feathers. Eventually, the map is lost to all after the penultimate chase led up to a volcano, and Daffy, Speedy, Sam, and Taz end up trapped on the once-again-deserted island. The well gives them three wishes individually, but warns them to use them wisely for they are the last wishes it will ever grant. After Daffy and Speedy waste their wishes -Speedy wishing for a burrito, then Daffy angrily responding by wishing the burrito was stuck on the end of Speedys nose (an obvious reference to The Ridiculous Wishes)- Daffy asks Sam to wish the burrito off Speedys nose, but discovers that Sam already wished for a ship, abandoning Daffy and telling them after he sinks Bugs, hell come back and pick the pair up. Daffy (now furious), then shouts out his famous catchphrase, "Youre Desthspicable!" at Sam and Taz. The wishing well does the famed "Thats all, folks!" sign-off.

==Classic cartoons in order==
The following includes the Wells descriptions of each cartoon in relation to that characters wish.

* Captain Hareblower (Pirate Sam vs. Bugs Bunny)

* Stupor Duck (Daffys wish): The Well: "Your fondest wish, your fondest dream. Ill make you Super Duck Supreme!"

* Greedy For Tweety (Grannys wish): The Well: "Your wish shall be granted. Gaze into me and see. The next time that you see yourself, a nurse is what youll be."

* Banty Raids (Foghorns wish): The Well: "I am here to grant your wish; the Spirit of the Well. I will knock the cockiness out of that little cockerel!" After this one, Foghorn, believing he was hornswoggled, demands a refund, but gets another penny.

* Louvre Come Back to Me! (Pepe Le Pews wish): The Well: "I grant your wish to meet a girl of beauty unsurpassed, which, when compared with works of art, will leave the Louvre outclassed."

* Tree for Two (Spike and Chesters wish): The Well: "If thats his wish, and yours alike, Ill make Chester brave and strong, like Spike."

* Curtain Razor (Porky Pigs wish): The Well: "Discovering new talent for the world to see; a wondrous thing for a producer to be..."

* A Mouse Divided (Sylvesters wifes wish): Sylvesters wife: "Wishing Well. Oh, Wishing Well, you I do entreat. I wish our house would resound to... the patter of little feet." The Well: "I hear your wish and I obey. The patter of little feet you shall hear this day." (Sylvesters wifes wish is included in the description because she, like the Well, speaks it in rhyme.)

* Of Rice and Hen & Lovelorn Leghorn (Prissys wish, with the opening of the former and the plot of the latter): The Well: "Your mind is such a simple thing. Your wish I can foretell. Youre wishing for a husband, and the ring of a wedding bell?"

* From Hare to Heir (Pirate Sams wish, final wish to show a classic Looney Tunes cartoon): The Well: "A very rich relative in poor health doth will to you his entire wealth."

==Merchandise==

In 1981 a puzzle showing 105 Looney Tunes characters has been issued. Among characters there are three ones that never appeared in animation, identified as Hoppy, Hysterical Hyram and Minniesoda Fats. In one scene in Daffy Ducks Fantastic Island Hoppy and Hysterical Hyram are seen waiting for their chance to make their dreams come true (along with two other unidentified characters, namely white dog and a small mouse with a hat). It is their only known appearance in animation. 

==See also==
*List of animated feature films
*List of package films

==Availability==

The movie is available as a download from the iTunes Store. In 2011, it was available on Netflix Instant Streaming. It was released Exclusively through Wal Mart on August 12, 2014. After many years of waiting, the movie was finally released on DVD by Warner Home Video on November 18, 2014.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 