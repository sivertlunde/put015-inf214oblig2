Sitt al-Bayt
{{Infobox Film
| name           = Sitt al-Bayt
| image          = Set elbait.jpg
| image_size     =
| caption        = Sitt al-Bayt poster
| director       = Ahmed Kamel Morsi
| producer       = 
| writer         = Abo El Seoud El Ebiary
| starring       = Faten Hamama Emad Hamdy Zeinab Sedky 1949
| runtime        = 
| country        = Egypt
| language       = Arabic
}}

Sitt al-Bayt ( ,  ) is a 1949 Egyptian drama film. It starred Faten Hamama, Emad Hamdy, and Zeinab Sedky. The film, which was written by Abo El Seoud El Ebiary and directed by Ahmed Morsi, was nominated for the Prix International Award in the Cannes International Film Festival.

== Plot ==

Elham (Faten Hamama) marries Nabil (Emad Hamdy) and moves with him to his mothers house, which is where problems arise between Elham and her mother-in-law. Nabils mother believes Elham is an intruder to her personal life because she is more the "lady of the house" than she was. The hatred forces Nabils mother to try to convince her son to marry another woman, Madiha (Mona), especially after discovering that Elham might be infertility|sterile.

Elham decides to leave the house, but as she was leaving she trips while going down the stairs and is rushed to the hospital. Her doctor tells Nabil that Elham was actually pregnant when she fell, and that she had miscarried as a result. Nabil is devastated and blames his mother for the result. The mother recognizes the mistakes she has made and apologizes to Elham and asks for forgiveness.

== Cast ==

*Faten Hamama as Elham
*Emad Hamdy as Nabil
*Zeinab Sedky as Nabils mother
*Mona as Madiha
*Mohammed Kamel
*Thuraya Fakhri

== References ==

*  , Faten Hamamas official site. Retrieved on January 4, 2007.
*  , Adab wa Fan. Retrieved on January 4, 2007.

== External links ==

*  

 
 
 
 
 
 