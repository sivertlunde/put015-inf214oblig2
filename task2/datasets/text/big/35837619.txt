Yellow Lily
 
{{Infobox film
| name           = Yellow Lily
| image          =
| image_size     =
| caption        =
| director       = Alexander Korda
| producer       = Ned Marin
| writer         = Lajos Bíró Garrett Graham Bess Meredyth
| narrator       =
| starring       = Billie Dove Clive Brook Gustav von Seyffertitz Marc McDermott Harold Young
| cinematography = Lee Garmes
| studio         = First National Pictures
| distributor    = First National Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States Silent   English intertitles
| budget         =
| gross          =
}} silent drama The Stolen Bride.  

==Cast==
* Billie Dove - Judith Peredy 
* Clive Brook - Archduke Alexander 
* Gustav von Seyffertitz - Kinkelin 
* Marc McDermott - Archduke Peter 
* Nicholas Soussanin - Dr. Eugene Peredy 
* Eugenie Besserer - Archduchess 
* Jane Winton - Mademoiselle Julie 
* Charles Puffy - Mayor of Tarna

==References==
;Notes
 

;Bibliography
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 