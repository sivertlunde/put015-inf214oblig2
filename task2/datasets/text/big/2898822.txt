The Masquerader (1914 film)
{{Infobox film
| name = The Masquerader
| image = Masquerader.jpg
| imagesize =
| caption =
| director = Charlie Chaplin
| producer = Mack Sennett
| writer = Charlie Chaplin Charles Murray Fritz Schade Minta Durfee Cecile Arnold Vivian Edwards Harry McCoy Charley Chase Frank D. Williams
| editing =
| distributor = Keystone Studios
| released =  
| runtime = 9 minutes English (Original titles)
| country = United States
| budget =
}}
  1914 film The Keystone directed and the second written by Chaplin. This is a rare film where we get the chance to see Charlie the actor, Charlie the Tramp, and Charlie as a female impersonator all in one.

==Synopsis== plot involving a man dressing up as a woman is quite popular in old silent movies.

==Cast==
* Charles Chaplin - Film actor
* Roscoe Fatty Arbuckle - Film actor
* Chester Conklin - Film actor Charles Murray - Film director
* Fritz Schade - Actor/villain
* Minta Durfee - Leading lady
* Cecile Arnold - Actress
* Vivian Edwards - Actress
* Harry McCoy - Actor
* Charley Chase - Actor
* Jess Dandy

==See also==
* Charlie Chaplin filmography
* Fatty Arbuckle filmography The Rounders

==References==
 

==External links==
* 
*  
* 
* 
*    

 

 
 
 
 
 
 
 
 
 
 
 
 


 