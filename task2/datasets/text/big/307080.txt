La Bamba (film)
{{Infobox film
| name           = La Bamba
| image          = LaBambaposter1987.jpg
| image_size     = 
| caption        = Theatrical release poster
| alt            = 
| director       = Luis Valdez
| producer       = Executive producers:  
| writer         = Luis Valdez
| starring       = Lou Diamond Phillips Esai Morales Rosanna DeSoto Elizabeth Pena Danielle von Zerneck Joe Pantoliano
| music          = Los Lobos Miles Goodman  Carlos Santana
| cinematography = Adam Greenberg
| editing        = Sheldon Kahn Don Brochu
| distributor    = Columbia Pictures 
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $6,500,000
| gross          = $54,215,416
}} biographical film written and directed by Luis Valdez that follows the life and career of Chicano rock n roll star Ritchie Valens. The film stars Lou Diamond Phillips as Valens, Esai Morales, Rosanna DeSoto, Elizabeth Peña, Danielle von Zerneck, and Joe Pantoliano. The film also covers the effect that Valens career had on the lives of his half-brother Bob Morales, his girlfriend Donna Ludwig and the rest of his family.

==Plot== rock n roll superstar under the stage name Ritchie Valens. He meets and falls in love with fellow high school student Donna Ludwig (von Zerneck), for whom he wrote a song that became a number two hit (Donna (song)|"Donna"). However, Donnas father is shown as having issues with his daughter dating a Mexican-American, which causes friction between Ritchie and Donna. The movie also has several subplots, such as his relationship with his mother Connie Valenzuela (DeSoto) and half-brother Bob Morales (Esai Morales), and the jealousy Bob felt toward Ritchie because of Ritchies success.

In one scene, Bob won an important art contest that helps promising cartoonists, only to throw away his prize because, in his mind, his mother doesnt seem to care enough. Bob resorts to drinking heavily and, at one point, leads him to yelling in a drunken rage in front of his mothers door, "I want to see my daughter!" in reference to the child he sired with Ritchies first girlfriend Rosie (Peña).

However, when they get an opportunity, Ritchie and Bob sneak out for a good time. On one occasion, they take a road trip to Tijuana, visiting one of the local nightclubs where Ritchie discovers the song that would eventually become his signature song, "La Bamba".

  a midair collision between two planes that actually occurred directly over Ritchies school, in which Ritchies best friend was crushed to death by one of the fallen aircraft (Ritchie was absent from school that day to attend his grandfathers funeral). At first, Ritchie manages to avoid flying to his concerts and appearances; but he must eventually conquer his fear when invited to perform his song "Donna" on American Bandstand. Ritchies record producer and manager, Bob Keane (Pantoliano), helps him by giving him a little vodka to calm his nerves during the flight to Philadelphia for the Bandstand appearance.
 Stephen Lee) after his hits, "La Bamba" and "Donna", reach the top of the Billboard magazine|Billboard charts.

Valens, Holly, and Bopper take off in an airplane during a snowstorm for their fateful flight on February 3, 1959, (the night that came to be known as "The Day the Music Died"). Before the ill-fated flight, Ritchie makes a call to his brother, wherein they patch up their differences. He even invites Bob to fly out to Chicago to join the tour for family support.

The next day, as Bob is fixing his mothers car, he hears the news bulletin on the radio that his brothers plane crashed without any survivors. Bob darts out of his driveway in an attempt to get to his mother before she hears the bad news through the radio. Unfortunately, by the time he gets there, she stands immobile. The news hits the Valenzuela family, Bob Keane, and Donna very hard. In the final scene, the cars to Ritchies funeral are shown driving slowly into San Fernando Mission Cemetery and Bob is then seen walking across a bridge and screaming out Ritchies name, remembering all the good times they had together (in flashback), accompanied by the Santo & Johnny instrumental "Sleep Walk".
 La Bamba" accompanied by the closing credits.

==Cast==
 
* Lou Diamond Phillips as Ritchie Valens
* Esai Morales as Roberto "Bob" Morales (Ritchies half-brother)
* Rosanna DeSoto as Connie Valenzuela (Ritchies mother)
* Elizabeth Peña as Rosie Morales
* Danielle von Zerneck as Donna Ludwig
* Joe Pantoliano as Bob Keane
* Rick Dees as Ted Quillin
* Marshall Crenshaw as Buddy Holly
* Howard Huntsberry as Jackie Wilson
* Brian Setzer as Eddie Cochran Stephen Lee as The Big Bopper
* Sam Anderson as Mr. Ludwig (Donnas father)

Also featured are several members of the Valenzuela family and director Luis Valdezs family, including:
* Concepcion Valenzuela (the real Connie Valenzuela, Ritchies mother) as the older woman sitting next to Ritchie at a party
* Daniel Valdez (Luis brother) as Ritchies Uncle Lelo

==Background==
 
This production had the full support of the Valenzuela family. The real Bob Morales and Connie Valenzuela came to the set to help the actors portray their characters accurately, and Connie (in life known as "Concha") makes a cameo appearance as an older lady sitting next to Ritchie at the familys first party.
 
Phillips bonded with the Valenzuelas and, at one point, actually became Ritchie to them. Such as was the case when an incident involving Ritchies real-life sister Connie Lemos occurred that disrupted the screening of the film. When at the screening she saw Phillips (as Valens) boarding the plane for the ill-fated Winter Dance Party flight; Lemos, who was only six years old at the time of the crash, was said to hysterically grab onto Phillips and shout, "Dont go Ritchie! Please dont get on the plane! Why did you have to die?" Lemos later admitted on VH-1s Behind the Music that she realized at that moment that she never fully accepted her brothers death.

The original title of this film was "Lets Go", named for Valens hit song: "Come On, Lets Go!"

All of Ritchie Valens songs were performed by Los Lobos. The band has a cameo in the movie wherein they sang in the brothel ballroom in Tijuana. Brian Setzer has a cameo as Eddie Cochran performing "Summertime Blues" onstage, while Howard Huntsberry starred as singer Jackie Wilson in the film, singing a cover of "Lonely Teardrops," which was on the soundtrack LP. Marshall Crenshaw plays Buddy Holly performing "Crying, Waiting, Hoping" at the final concert in Clear Lake, Iowa.
 American Pie".

==Distribution==
The film opened in wide release in the United States on July 24, 1987. In Australia it opened on September 17, 1987.

In its opening weekend, the film grossed a total of $5,698,884. La Bamba eventually grossed $52,678,820 in the United States in 12 weeks. 

==Reception==
Roger Ebert liked the film and the screenplay, writing, "This is a good small movie, sweet and sentimental, about a kid who never really got a chance to show his stuff. The best things in it are the most unexpected things: the portraits of everyday life, of a loving mother, of a brother who loves and resents him, of a kid growing up and tasting fame and leaving everyone standing around at his funeral shocked that his life ended just as it seemed to be beginning." 

Janet Maslin, writing for The New York Times, was impressed with Lou Diamond Phillips performance, and wrote, "A film like this is quite naturally a showcase for its star, and as Valens, Lou Diamond Phillips has a sweetness and sincerity that in no way diminish the toughness of his onstage persona. The role is blandly written, but Mr. Phillips gives Valens backbone." 

The review aggregator Rotten Tomatoes reported that 96% of critics gave the film a positive review, based on 26 reviews." 

===Awards===
Wins
*   and Miles Goodman; 1988.

Nominations
*  ; 1988.

==Soundtrack==
{{Infobox album
| Name           = La Bamba Soundtrack
| Cover          = labambasounds.jpg
| Artist         = Various artists
| Caption        = Cover
| Type           = soundtrack
| Released       = June 30, 1987
| Format         = 
| Recorded       = 
| Genre          = Rock
| Length         =
| Producer       =
| Certification  =
| Last album     =
| This album     =
| Next album     =
|
}}

===Tracklisting=== La Bamba" - Los Lobos
# "Come On, Lets Go!" - Los Lobos
# "Ooh My Head" - Los Lobos We Belong Together" - Los Lobos
# "Framed" - Los Lobos
# "Donna (Ritchie Valens song)|Donna" - Los Lobos
# "Lonely Teardrops" - Howard Huntsberry
# "Crying Waiting Hoping" - Marshall Crenshaw
# "Summertime Blues" - Brian Setzer Who Do You Love?" - Bo Diddley 
# "Charlena" - Los Lobos
# "Goodnight My Love" - Los Lobos

Because the movie is a celebration of 1950s rock & roller Ritchie Valens, his music and the music of his contemporaries play a central part in the film.

An original motion picture soundtrack album was released on June 30, 1987 on  ", "Framed", and "Donna". 
 Who Do You Love?"
 Chantilly Lace" were omitted from the release. Other omitted songs were "Oh Boy", "Rip It Up", "The Paddi Wack Song" (written by Valens), and "Sleep Walk".

 
{{succession box
  | before = Whitney (album)|Whitney by Whitney Houston
  | title = Billboard 200|Billboard 200 Number-one albums of 1987 (USA)|number-one album
  | years = September 12–26, 1987
  | after = Bad (album)|Bad by Michael Jackson
}}
 

==References==
 

==External links==
 
*  
*  
*   trailer on YouTube

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 