Nights in Black Leather
  Richard Abel under the pseudonym Ignatio Rutkowski. The films original title was Post Haste Hustle, but was changed by the distributor to Nights in Black Leather.

It is the first of just two gay porn films Peter Berlin starred in as himself, although the film credits name him Peter Burian. Following the threat of a lawsuit from another actor named Peter Burian, the main star changed his name to Peter Berlin and became hugely popular under the new name.

==Synopsis== North Beach, Lands End (San Francisco)|Lands End, and all over San Francisco, Nights in Black Leather is both an erotic romp and a time capsule of gay life in San Francisco in the early 1970s.

==Cast==
*Peter Berlin (credited as Peter Burian)		
*Rick Jedin		
*Al Joffrey		
*Jeff Salum		
*Tom Webb

==Showings and legacy==
The film became an international sensation, playing for months at adult cinemas in the US and Europe. 
Until recently, no known intact 16mm negative or print existed. Then someone associated with the release of the original film contacted Peter with news that the original negative was in storage in Southern California. This negative has been used to create a brand new 16mm print and both the negative and print is on loan to the Legacy Project, a film preservation joint venture between Outfest and the UCLA Film and Television Archives.

==Awards==
*In 2007, the film won the GayVN Award for "Best Classic DVD"

==External links==
* 

 

 
 
 

 