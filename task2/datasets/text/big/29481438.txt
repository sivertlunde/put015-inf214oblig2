Chithrakuzhal
{{Infobox film
| name           = Chithrakuzhal
| image          = ChithraKuzhal.jpg
| caption        = Chithrakuzhal movie poster
| alt            =
| director       = Majeed Gulistan
| producer       = Dhirubhai Chauhan
| writer         = Majeed Gulistan
| starring       = Amal Ashok  Sidharth Meera Nair
| music          = Ashwin Johnson
| cinematography = M J Radhakrishnan
| editing        = Mahesh
| studio         =
| distributor    =
| released       =  
| runtime        = 83 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Chithrakuzhal ( ) is a 2010 Malayalam film directed by Majeed Gulistan. Chithrakuzhal (Binoculars) is a Childrens film based on environmental issues and protection of the Nature. The title of the English version of the film is The Bird Catcher.

==Synopsis==
A Forest Ranger’s (Vijayaraghavan (actor)|Vijayaraghavan) school going kid Charu (Sidharth) happens to get lost in the forest by a trap laid by the poachers who had a grudge with his father. Caught and taken away by the poachers, the boy is spotted by a tribal boy Virundhan (Amal Ashok) who lives in the forest and who is incidentally his classmate too. In the school the tribal boy got a nickname ‘The Bird Catcher’ as he is believed to catch & take away the birds for cooking and eating.
The tribal boy, who was in the forest searching for some very rare herbal medicinal plants for his ailing mother, becomes the savior & guide to the forest officer’s son. The journey also reveals the many conditions, skills & qualities of the tribal boy Virundhan to Charu, who is of the modern upbringing.
Both of them chance to meet Amina (Meera Nair), a girl of their class, who also happened to be in the forest, running away for fear of police. In the daring journey of the three together, they deal the dangers and obstacles and also learn about the environment & the threats lurking in it.
The journey and its experience eventually transforms them to sharing and concerned human beings with better values.

==Filming==
The film was  set in the backdrop of the forests of the Western Ghats in South Kerala. Agasthyarkoodam and Meenmutty Falls, Thiruvananthapuram|Meenmutty.   Kerala state National film awards. 
Chithrakuzhal has the distinction of being the first Dolby Digital Childrens film in Cinema of Kerala|Malayalam.   Minister of Forests Mr. Benoy Viswam 

==Cast==
* Amal Ashok as Virundhan, the tribal boy
* Sidharth as Charu, the forest Rangers son
* Meera Nair as Amina, the classmate Madhu as Charus grandfather Vijayaraghavan as the Forst Ranger (Charus father)
* Geetha Vijayan as Charus mother
* Stella as Aminas mother
* Vijayakumari as Virundhans mother
* Sunitha as the tribal woman
* Indrans as the Forest guard
* Monu as the Big Fat

==Accolades==
Chithrakuzhal was selected to the 2011 Lucknow International Childrens Film festival. 
The film was selected to the 34th Cairo International Film Festival.

==Soundtrack==
The music of Chithrakuzhal was done by Ashwin Johnson. The lyrics are by renowned Malayalam poet Kavalam Narayana Panicker. The song "Thathindhaka theytharo" sung by Sri Devi, Jenny, Lekshmi and Kanchana is very popular among children.  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 