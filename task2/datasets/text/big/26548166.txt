Netherworld (film)
{{Infobox film
| name           = Netherworld
| image          = 
| image_size     = 
| caption        = 
| director       = David Schmoeller
| producer       = {{plainlist|
* Charles Band
* Thomas Bradford
}}
| story          = Charles Band
| screenplay     = David Schmoeller
| starring       = {{plainlist|
* Michael Bendetti
* Denise Gentile
* Holly Floria
* Robert Sampson
* Holly Butler
* Alex Datcher
}}
| music          = David Bryan
| cinematography = Adolfo Bartoli
| editing        = Andy Horvitch
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English French
| budget         = 
}}
Netherworld is a 1992 American horror-romance film written and directed by David Schmoeller and produced by Charles Band.

==Synopsis==
After his father dies, Corey Thorton inherits his fathers estate in Louisiana, only to find that his father plans to sacrifice his soul to live again.

==Cast==
* Michael Bendetti (Corey Thorton)
* Denise Gentile (Delores)
* Holly Floria (Diane Palmer)
* Robert Sampson (Noah Thorton)
* Holly Butler (Marilyn Monroe)
* Alex Datcher (Mary Magdalene)
* Robert Burr (Beauregard Yates, Esq.)
* George Kelly (Bijou)
* Mark Kemble (Barbusoir)
* Michael Lowry (Stemsy)
* David Schmoeller (Billy C.)

==Production==
The film was originally to be produced in Romania, but Band moved it to New Orleans, Louisiana. 

==Reception==
Writing in The Zombie Movie Encyclopedia, academic Peter Dendle called it "an unremarkble Dixieland shocker from dubious Full Moon studios".   Lawrence Cohn of Variety (magazine)|Variety called it an "entertaining horror pic".   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 