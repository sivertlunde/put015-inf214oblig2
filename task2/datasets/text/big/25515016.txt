Money as Debt
{{Infobox film
| name           = Money as Debt
| image          = Money as Debt DVD cover.jpg
| image size     = 
| alt            = 
| caption        = DVD cover
| director       = Paul Grignon
| producer       = Paul Grignon
| writer         = Paul Grignon
| narrator       = Bob Bossin
| starring       = 
| music          = Paul Grignon
| cinematography = 
| editing        = 
| studio         = Moonfire Studio
| distributor    = 
| released       = 2006 
| runtime        = 47 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
 animated documentary Canadian artist  and filmmaker Paul Grignon  about the monetary systems practised through modern banking.  The film presents Grignons view of the process of money creation by banks and its historical background, and warns of his belief in its subsequent unsustainability.    Subsequent Money as Debt videos include Money as Debt II (2009)  and Money as Debt III: Evolution Beyond Money (2011). 

==Background==
The film was conceived by Grignon in 2002 as an introduction to a 5-hour video commission for United Financial Consumers. He prefaced his video lecture with a re-telling of The Goldsmith’s Tale in animation form titled Money as Debt. The Goldsmiths Tale is noted in the film as being "a brief and broadly allegorical history of banking" and should not be viewed as a complete or entirely accurate account of the history of banking. Expanded over a six-month period in 2006, it was Grignons first full length animation project.  

Much of the film presents the filmmakers understanding of modern money creation in a fractional-reserve banking system.   New money enters the economy through the indebtedness of borrowers, thus not only obligating the public to the money-issuing private banks but also creating an endless and self-escalating debt that is to eventually outgrow all other forms of wealth generation.  The film claims that this ever-increasing gravitation of money to banks is capable of impoverishing any nation. The film finishes by identifying some alternatives to modern banking, such as the nationalization of banks and payment of dividends to the public, establishing local exchange trading systems, or government printing of money. 

==Critical response==

Epoch Times complimented filmmaker Grignon for his character design and for presentation of economics in simple terms and concepts. In discussing confusions associated in understanding modern finance, they wrote the film was "a good place to get started on the quest for new financial insights, and I would recommend it to anyone who is interested in deepening their understanding 
of our existing banking system." 

An article in Anthropology Today called the film "a hit in activist circles", but also a "fable" that "demonizes the banks, and interest in particular" and whose "message is in many ways misleading." 

An article in the Atlantic Free Press said "Money as Debt is not entertainment&mdash;far from it. The film offers amazingly elementary facts about the creation of money in the United States, narrated by a soothing voice, which could make for a bland presentation, yet the films message is anything but vapid. In fact, if it doesnt leave your blood boiling, it behooves you to check your vital signs." 

Cdurable offered "Ce long métrage danimation, dynamique et divertissant, de lartiste et vidéographe Paul Grignon, explique les effets magiques mais pervers du systeme actuel dargent-dette dans des termes compréhensibles pour tous." ("This animated feature, dynamic and entertaining, by artist and videographer Paul Grignon, explains the magical but twisted effects of the current system of debt-money in terms understandable to all.")   Thomas Publications Fog City Journal wrote that the animated documentary was "a painless but hard-hitting educational tool." 
 libertarian thinkers, Freedom Force International.  Specifically, Griffin criticizes Grignons proposal for "interest-free banking" and fiat, albeit government-created as opposed to central bank-created, currency. 

== See also ==
* The Capitalist Conspiracy

==References==
{{reflist|refs=
   
 
{{cite news
|url=http://www.atlanticfreepress.com/content/view/2245/81/|title=Daddy, where does the money come from?|last=Baker
|first=Carolyn
|date=25 August 2007
|publisher=Atlantic Free Press
|accessdate=9 February 2010}} 

 {{cite web
|url=http://paulgrignon.netfirms.com/MoneyasDebt/ProducersComments.html
|title=Producer’s Comments on the Movie
|last=Grignon
|first=Paul
|publisher=paulgrignon.netfirms.com
|accessdate=15 February 2010}} 

 Hart, Keith.  , Anthropology Today, Volume 23, Issue 5, pages 12–16, October 2007. 

 {{cite news
|url=http://www.cdurable.info/L-Argent-Dette-Paul-Grignon-Money-as-Debt,1299.html
|title=Découvrez le long métrage "Money as Debt" de Paul Grignon
|last=Naulin
|first=David
|date=October 31, 2008
|work=Cdurable
|language=French
|accessdate=29 November 2010}} 

 {{cite web
|url=http://paulgrignon.netfirms.com/MoneyasDebt/disputed_information.html
|title=Disputed Information in Money as Debt
|last=Grignon
|first=Paul
|work=paulgrignon.netfirms.com
|accessdate=29 November 2010}} 

 {{cite news
|last=Washington
|first=Samuel
|format=in Epoch Times archive as a PDF document
|title=Film Review: Money as Debt
|url=http://epoch-archive.com/a1/en/us/nyc/2008/12-Dec/30/B3_Your_Money.pdf
|accessdate=August 26, 2013
|newspaper=Epoch Times
|date=December 30, 2008}} 

 {{cite web
|url=http://www.fogcityjournal.com/wordpress/791/money-as-debt/
|title=Money As Debt
|date=October 3, 2008
|work=Fog City Journal
|accessdate=29 November 2010}} 

 {{cite web
|url=http://www.tofinotime.com/artists/R-PGfrm.htm
|title=Tofino Artist Paul Grignon
|work=Tofino Art
|accessdate=29 November 2010}} 

 {{cite news
|last=Wipond
|first=Rob
|title=This artist follows the money
|url=http://www.focusonline.ca/?q=node/268
|accessdate=August 25, 2013
|newspaper=Focus
|date=October 2011}} 

 {{cite news
|last=Weiner
|first=Keith
|title=Goethe Predicted Slavery
|url=http://snbchf.com/gold-standard/goethe-dollar-slavery/
|accessdate=3 April 2015
|newspaper=SNBCHF News}} 

}}

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 