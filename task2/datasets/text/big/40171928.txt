Into the Woods (film)
 
{{Infobox film
| name           = Into the Woods
| image          = Into the Woods film poster.jpg
| alt            = A witch ominously peering through a thicket of branches.
| caption        = Theatrical release poster
| director       = Rob Marshall Marc Platt Callum McDougall
| screenplay     = James Lapine
| based on       =  
| starring       = Meryl Streep Emily Blunt James Corden Anna Kendrick Chris Pine Tracey Ullman Christine Baranski Johnny Depp 
| narrator       = James Corden
| music          = Stephen Sondheim
| cinematography = Dion Beebe
| editing        = Wyatt Smith
| studio         = Walt Disney Pictures Lucamar Productions Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 124 minutes  
| country        = United States 
| language       = English
| budget         = $50 million 
| gross          = $204.3 million   
}} fantasy Musical musical drama adapted to Broadway musical Grimm Brothers fairy tales of "Little Red Riding Hood", "Cinderella", "Jack and the Beanstalk", and "Rapunzel", the film is a fantasy genre crossover centered on a childless couple, who set out to end a curse placed on them by a vengeful witch. Ultimately, the characters are forced to rectify the consequences of their actions. 

After several unsuccessful attempts by other studios and producers of adapting Into the Woods to film, Walt Disney Pictures announced in 2012 that it was producing an adaptation of the musical, with Marshall directing and John DeLuca serving as producer. Principal photography commenced in September 2013, and took place entirely in the United Kingdom, including at Shepperton Studios in London.
 world premiere Best Supporting Best Motion Picture – Musical or Comedy. 

==Plot==
 

A Baker (  as white as milk, a cape as red as blood, hair as yellow as corn, and a slipper as pure as gold. The Witch later tells the Baker that she asked him to do this task for her because she is not allowed to touch any of the objects.

The Witchs demands eventually bring the Baker and his Wife into contact with Jack (Daniel Huttlestone), who is selling his beloved cow Milky White and to whom the Baker offers five magic beans left him by his father (which were stolen from the Witch) which grow into a large beanstalk; with Red Riding Hood (Lilla Crawford), whose ruby cape the couple notices when she stops to buy sweets on her way to grandmothers house; with Rapunzel (MacKenzie Mauzy), the Witchs adopted daughter (the child that she took from the Bakers parents in exchange for the Bakers father taking the greens from her garden), whose tower the Bakers Wife passes in the woods; and with Cinderella (Anna Kendrick), who also runs into the Bakers Wife while fleeing from the pursuing Prince (Chris Pine).
 Giant in the sky, and kills the pursuing Giant by cutting down the beanstalk; Little Red Riding Hood and her Grandmother (Annette Crosbie) are saved from the Big Bad Wolf (Johnny Depp) by the Baker; and the Witch regains her youth and beauty after drinking the potion.

However, each of the characters learns their "happily ever after" is not so happy: the Baker is worried he is a poor father to his newborn baby, like his father before him; Cinderella is disenchanted by royal life; Rapunzel is constantly scared of the outside world, having never experienced it; and the Witch learns that she has lost most of her powers with her restored youth. The growth of a second beanstalk from the last remaining magic bean allows the Giants Wife ( ) in spite; Cinderella and the Prince break up after she hears of his infidelity with the Bakers Wife; and Red Riding Hoods Mother and Grandmother, along with Jacks Mother, are killed in the Giants Wifes rampage.

In the aftermath, the surviving characters debate the morality of handing Jack over, and soon quickly blame each other for their individual actions that led to the tragedy, ultimately blaming the Witch. She throws away her remaining beans, reenacting her mothers curse and apparently allowing it to destroy her, leaving the rest of the group to their fate.

The Baker, Cinderella, Jack, and Red Riding Hood resolve to kill the threatening Giants Wife, though Cinderella and the Baker try to explain to the distraught Red Riding Hood and Jack the complicated morality of retribution and revenge. The Giants Wife is killed, and the characters move forward with their ruined lives: the Baker, thinking of his Wife, is determined to be a good father; Cinderella leaves the Prince and decides to help the Baker; and Jack and Red Riding Hood, now orphans, live with the Baker and Cinderella. The Baker begins to tell their story to his son—"Once upon a time..."

==Cast==
 
* Meryl Streep as The Witch   
* Emily Blunt as The Bakers Wife   
* James Corden as The Baker   
* Anna Kendrick as Cinderella   
* Chris Pine as Prince Charming|Cinderellas Prince 
* Tracey Ullman as Jacks Mother   
* Christine Baranski as Cinderellas Stepmother    The Wolf   
* Lilla Crawford as Little Red Riding Hood   
 Jack   
* MacKenzie Mauzy as Rapunzel   
* Billy Magnussen as Rapunzels Prince   
* Tammy Blanchard as Florinda 
* Lucy Punch as Lucinda 
* Frances de la Tour as the Giant (mythology)|Giants Wife  Steward 
* Joanna Riding as Cinderellas Mother 
* Annette Crosbie as Little Red Riding Hoods Grandmother   
* Simon Russell Beale as the Bakers Father 
 

==Stage-to-screen changes==
While it was initially reported that Disney had decided to make some major plot changes for the film version in order to make it more family-friendly,  Stephen Sondheim revealed that this was not the case and that any changes in the film version had been approved by him and James Lapine.  

However, the film does slightly differ from the stage production. The songs "I Guess This Is Goodbye",    "Maybe Theyre Magic",  "First Midnight" and "Second Midnight" interludes, "Ever After" (Act I finale of the original play),  "So Happy",    "Into the Woods" Reprise, "Agony" Reprise  and "No More" (performed by the Baker) were cut from the film,    although both "Ever After" and "No More" are used as instrumentals in the film. Meanwhile, many of the songs in the film have slightly different lyrics than their stage counterparts due to the slight tweaking of storylines.

Other changes include a major reduction of the significant role of the "Mysterious Man", who manipulates much of the action in the first act and is eventually revealed to be the Bakers father. Also, the character of the Narrator is also deleted, and the film is instead narrated by the Baker.    The minor role of Cinderellas Father was cut, and he is instead mentioned as deceased.  Due to the films compressed storyline, Rapunzels pregnancy is deleted, as are the Princes pursuit of Snow White and Sleeping Beauty.  In addition, the subplot where the two princes have affairs with Snow White and Sleeping Beauty is eliminated.  In the film, the Giants Wife attacks during the marriages of Cinderella and Rapunzel to their respective Princes, no more than a few weeks after the events of act one, while in the stage show, nearly a year passes between the first and second acts.  Rapunzels ultimate fate is also changed: rather than being killed by the giant, she refuses to cooperate with her mother and flees with her prince. 

Similarly, much of the violence and sexual content is slightly toned down from the original musical. The death of Jacks Mother is less violent in the film; on stage, the Steward clubs her to death with his staff, in the film, he simply shoves her to the ground and she hits her head on a log. Similarly, the tryst between the Bakers Wife and the Prince in the film is vague enough so that it could be inferred that the Bakers Wife only kissed the Prince, rather than having an affair with him. The death of the Bakers Wife is not explicitly shown in the film, and the cause of her death is changed from being simply trampled by the Giants Wife to accidentally backing off the edge of a cliff while fleeing from the Giants Wife.

While it was initially reported that the film version would feature two new songs: a duet for the Baker and his Wife called "Rainbows", originally written for a 1992 film adaptation that was never made, and a new song for the Witch, eventually titled "Shell Be Back.".  In the end, neither song appears in the finished film: "Rainbows" was cut before shooting began and "Shell Be Back", though filmed, was cut from the film on the grounds that it slowed the story down. "Shell Be Back" was included as a bonus feature on the films DVD and Blu-ray release. 

==Production==

===Early development===
Early attempts of adapting Into the Woods to film occurred in the early 1990s, with a script written by Lowell Ganz and Babaloo Mandel. A reading was held with a cast that included Robin Williams as The Baker, Goldie Hawn as The Bakers Wife, Cher as The Witch, Danny DeVito as The Giant, Steve Martin as The Wolf, and Roseanne Barr as Jacks Mother.  By 1991, Columbia Pictures and Jim Henson Productions were also developing a film adaptation with Craig Zadan as producer and Rob Minkoff as director.   In 1997, Columbia put the film into Turnaround (filmmaking)|turnaround, with Minkoff still attached as director, and Billy Crystal, Meg Ryan, and Susan Sarandon reportedly in talks to star.  After the report by Variety (magazine)|Variety, a film adaptation of Into the Woods remained inactive for 15 years.

===Development at Disney===
After the critical and commercial success of  , although Sondheim suggested Into the Woods instead.    Marshall concurred, but development of the project was then postponed while he focused on directing  —and pitched the idea to the studio,  with Lapine writing the script and Sondheim "expected" to write new songs.  Academy Award-winner Dion Beebe, who previously collaborated with Marshall on Chicago, Memoirs of a Geisha, and Nine, served as cinematographer.  Sondheim confirmed that a new song had been written for the film. 
 Michael McGrath Patrick Wilson Walt Disney Studios president Sean Bailey to green-light the film despite ongoing concerns about the dark nature of the original musical (which Disney executives had begun to understand since Marshalls original pitch).   However, Disney (which self-finances all its films)  provided only a relatively small production budget of $50 million (relative to other feature-length fantasy films on its development slate), in turn forcing both cast and crew to accept pay cuts to work on the film. 
 The Frogs.) During the same month, it was reported that Janney had been confirmed to join the film as well.  Five months later, however, Tracey Ullman was cast as Jacks Mother instead. 

  in Farnham, Surrey.]]
In April 2013, Johnny Depp was in final negotiations, along with Streep, to join the film.   The Hollywood Reporter reported that to help make the film on such a tight budget, Depp agreed as a favor to Disney and to Marshall (whom he had just worked with in On Stranger Tides) to a "boarding" arrangement, in which he would appear in a minor role for a fee of $1 million, instead of his typical fee of $20 million for a starring role.   In May, James Corden, who took part in the reading of the screenplay, was in talks to play the role of the Baker.  On May 10, 2013, Disney confirmed the casting of Streep, Depp, and Corden as the Witch, the Big Bad Wolf, and the Baker, respectively.  That same month, Emily Blunt and Christine Baranski were cast, respectively, as the Bakers Wife and Cinderellas Stepmother.      Marshall later confirmed that Blunt was selected for her "warm " and likeability to ensure the emotional impact of the sudden death of the Bakers Wife: " hat’s very important for that character because it’s the heart of the piece and you really have to love her so when she’s gone it should feel like a kick in the gut."  After she was cast, Blunt discovered she was pregnant and her costume and choreography had to be adjusted accordingly.     However, her "overweight" appearance during production actually fit in with the role of the Bakers Wife;  as she explained, "I feel like she would have eaten a lot of carbs working in the bakery." 

Also in May,  , Simon Russell Beale, Joanna Riding, and Annette Crosbie in other roles was also separately announced later on September 16. 

===Filming===
 blocked their scenes.    In August, the cast members visited Angel Recording Studios to separately record their respective parts in the presence of Sondheim himself.  Over 90% of the vocal tracks in the final version of the film are from the recording studio sessions, while the rest were recorded on location or on the set.   The advantage of blocking and rehearsing all scenes first was that the cast members could then precisely calibrate their voices in the recording studio to the planned appearance of each scene when later filmed, thereby minimizing the slightly disconcerting disconnection between vocals and choreography typical of music videos.   
 Avid Media Composer.  The exterior of Byfleet Manor in Surrey served as Cinderellas home. 

The filmmakers spent a whole day shooting scenes which involved Rapunzels hair being climbed upon.  Mauzy claimed that the filmmakers wanted to take advantage of her blonde hair, and that the top of Rapunzels hair appeared in the film was her own real hair; make-up artists only braided it into the extension.  This hair extension was engineered by hair designer Peter King.    After testing loose, flowing hair which King found "uncontrollable", he decided to have twenty-seven wefts of real hair woven together into a 30-foot braid, a design which took inspiration from an Arthur Rackham illustration of Rapunzel.  In order to bring in enough real Russian hair strands needed for the extension, King and his team had to work with several distributors from Germany and England.  The hair-braiding process required three people, each holding a separate strand and weaving in and out.  King also dyed the wefts for them to match Mauzys champagne blonde hair color, and blended together six different shades from ash and strawberry
to create realistic gradations and highlights.  Between scenes, Mauzy had to "wrap   around her arm like huge rolls of wool", as recalled by King.  A stuntman was employed to shoot hair-climbing scenes.  Thin rope and metal rings were the only additional tools concealed within the braid to hold the weight of a person climbing up. 
 aerial drone flying down a valley in Wales, and a shot of an overcast sky in Manhattan, New York City.    Filming concluded on November 27, 2013. 
 shooting new material that had been cut and re-added to the script after Disney screened the movie.    For his role as the Wolf, Depp worked closely with the films costume designer Colleen Atwood to create a Tex Avery-inspired costume, complete with zoot suit and fedora. 

===Music===
 
As noted above, the majority of the songs were pre-recorded by the cast.  The films soundtrack was released by Walt Disney Records on December 15, 2014. 

==Release==
The first official company presentation took place at the 2013 Disney D23 expo.  The official teaser trailer debuted on July 31, 2014.   A featurette was released showing behind-the-scenes clips and the vocals of Streep, Kendrick, Blunt and others.  A second trailer was released on November 6, 2014. 

The film held its world premiere at the Ziegfeld Theatre in New York City on December 8, 2014.  It was released theatrically in the United States on December 25, 2014.

===Home media===
Walt Disney Studios Home Entertainment released the film on Blu-ray Disc, DVD, and digital download in North America on March 24, 2015. 

==Reception==

===Box office===
Into the Woods grossed $128 million in North America, and $76.3 million in other territories, for a worldwide total of $204.3 million, against a budget of $50 million. 
 Mamma Mia!).   The films $3.5 million debut in Japan marked the largest opening for a 21st-century live-action musical film. 

===Critical response===
Despite early screenings prior to the films release, Disney issued an embargo on professional reviews of the film until two weeks before general theatrical release.  Into the Woods received positive reviews from critics. The review aggregation website   and Broadway—even if it darkens to its detriment in the final act." The cast—particularly the performances by Streep, Blunt and Pine—received wide acclaim as well as the tone, production design, costume design, visual effects and pacing.  Another aggregate, Metacritic, calculates a score of 69 out of 100, based on 41 critics, indicating "generally favorable reviews".  In CinemaScore polls conducted during the opening weekend, cinema audiences gave Into the Woods an average grade of "B" on an A+ to F scale.   
 Academy Award Best Supporting Actress category, and also praised the performance of Chris Pine. 
 Vanity Fair, Dana Stevens existentialist message at the heart of the original musicals second act.    Stevens characterized the result as a "generic dystopian bummer,"  while Lawson criticized the film as a "dutiful but perfunctory adaptation" which lacked "genuine heart".  Paul Katz of The Huffington Post felt that change in tone between the last two acts was too abrupt, and also criticized the films faithfulness to the stage musical.  Conversely, Alyssa Rosenberg of The Washington Post disliked the performances by Streep and Depp, while simultaneously calling the films first two acts as a "surprising delight". 

The film was named as one of the eleven best films of 2014 by the American Film Institute. 

===Accolades===
{| class="wikitable sortable" width="99%"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipient(s) and nominee(s)
! Result
|- Academy Awards  87th Academy February 22, 2015 Best Supporting Actress
| Meryl Streep
|  
|- Best Production Design
| Production Design:  
|  
|- Best Costume Design
| Colleen Atwood
|  
|- AACTA International Awards  January 31, 2015 Best Supporting Actress
| Meryl Streep
|  
|- Art Directors Guild Awards 
| January 31, 2015 Excellence in Production Design for a Fantasy Film
| Dennis Gassner
|  
|-
| American Cinema Editors    American Cinema January 30, 2015 Best Edited Feature Film – Comedy or Musical
| Wyatt Smith
|  
|-
| American Film Institute    December 9, 2014
| Top Eleven Films of the Year
| Into the Woods
|  
|-
| rowspan="2" | British Academy Film Awards  February 8, 2015 BAFTA Award Best Costume Design Colleen Atwood
| 
|- BAFTA Award Best Makeup and Hair Peter Swords King, J. Roy Helland
| 
|- Broadcast Film Critics Association  20th Critics January 15, 2015 Best Supporting Actress Meryl Streep
| 
|- Best Acting Ensemble The Cast of  Into the Woods
| 
|- Best Art Direction Dennis Gassner/Production Designer, Anna Pinnock/Set Decorator
| 
|- Best Costume Design Colleen Atwood
| 
|- Broadcast Film Best Hair & Makeup
|
| 
|- Casting Society of America  January 22, 2015 Big Budget Comedy Francine Maisler, Bernard Telsey, Tiffany Little Canfield
|  
|-  Chicago Film Critics Association December 15, 2014 Best Art direction/Production Design Dennis Gassner,  Anna Pinnock
| 
|- Costume Designers Guild  February 17, 2015 Excellence in Fantasy Film Colleen Atwood
| 
|- Detroit Film Critics Society December 15, 2014 Best Ensemble
|The Cast of Into the Woods
| 
|- Empire Awards  20th Empire March 29, 2015 Empire Award Best Male Newcomer Daniel Huttlestone
| 
|- Florida Film Critics Circle December 19, 2014 Best Art Direction/Production Design Dennis Gassner,  Anna Pinnock
| 
|-
| rowspan="3" | Golden Globe Awards  January 11, 2015 Golden Globe Best Motion Picture – Musical or Comedy
|Into the Woods
| 
|- Golden Globe Best Actress in a Motion Picture – Comedy or Musical Emily Blunt
| 
|- Golden Globe Best Supporting Actress – Motion Picture Meryl Streep
| 
|-
| rowspan="3" |Make-Up Artists and Hair Stylists Guild Awards 
|rowspan=3| February 14, 2015    Best Period and/or Character Make-Up in Feature Length Motion Picture  Peter Swords King
| 
|- Best Period and/or Character Hair Styling in Feature Length Motion Picture  Peter Swords King and J. Roy Helland
| 
|- Best Special Make-Up in Feature Length Motion Picture 
|J. Roy Helland and Matthew Smith
| 
|- MPSE Golden Reel Awards 
| February 15, 2015
| Feature Musical
| Mike Higham, Jennifer Dunnington
|  
|-
| rowspan="1" | MTV Movie Awards  
| rowspan="1" | April 12, 2015
| Best Villain 
| Meryl Streep
|  
|-
| rowspan="7" | Phoenix Film Critics Society 
| rowspan="7"| December 16, 2014
| Best Ensemble Acting
| Meryl Streep, Emily Blunt, James Corden, Anna Kendrick, Chris Pine, Johnny Depp, Lilla Crawford, Daniel Huttlestone, MacKenzie Mauzy, Tracey Ullman, Christine Baranski, Tammy Blanchard, Lucy Punch, Billy Magnussen, and Frances de la Tour
|  
|-
| Best Live Action Family Film
| Into the Woods
|  
|-
| Best Cinematography
| Dion Beebe
|  
|-
| Best Film Editing
| Wyatt Smith
|  
|-
| Best Costume Design
| 
|  
|-
| Best Performance By a Youth – Male
| Daniel Huttlestone
|  
|-
| Best Performance By a Youth – Female
| Lilla Crawford
|  
|- San Diego Film Critics Society December 15, 2014 Best Production Design Dennis Gassner,  Anna Pinnock
| 
|-
| rowspan="4" | Satellite Awards   February 15, 2015 Best Ensemble – Motion Picture
| Meryl Streep, Emily Blunt, James Corden, Anna Kendrick, Chris Pine, Johnny Depp, Lilla Crawford, Daniel Huttlestone, MacKenzie Mauzy, Tracey Ullman, Christine Baranski, Tammy Blanchard, Lucy Punch, Billy Magnussen, and Frances de la Tour
|  
|- Best Costume Design
| Colleen Atwood
|  
|- Best Sound (Editing and Mixing)
| Blake Leyh, John Casali, Michael Keller, Michael Prestwoood Smith, and Renee Tondelli
|  
|- Best Visual Effects
| Christian Irles, Matt Johnson, and Stefano Pepin
|  
|-
| rowspan="5" | Saturn Awards  June 25, 2015 Best Fantasy Film
| Into the Woods
|  
|- Best Supporting Actress
| Meryl Streep
|  
|-
| Best Production Design
| Dennis Gassner
|  
|- Best Costume
| Colleen Atwood
|  
|- Best Make-up
| Peter King and Matthew Smith
|  
|-
| Screen Actors Guild Awards   January 25, 2015 Outstanding Performance by a Female Actor in a Supporting Role
| Meryl Streep
|  
|-
|St. Louis Film Critics December 15, 2014 Best Music Soundtrack
|
| 
|-
| rowspan="2" | Washington D.C. Area Film Critics Association Awards 
| rowspan="2"| December 8, 2014 Best Ensemble
|
|  
|- Best Art Direction
| Dennis Gassner and Anna Pinnock
|  
|-
| 2015 Kids Choice Awards 
| March 28, 2015
| Best Villain
| Meryl Streep
|  
|-
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 