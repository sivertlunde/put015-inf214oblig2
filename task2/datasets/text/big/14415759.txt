Raffles (1930 film)
:For other film versions, see Raffles the Amateur Cracksman, Raffles (1925 film) and Raffles (1939 film).
{{Infobox film
| name           = Raffles
| image          = Poster of Raffles (1930 film).jpg
| caption        = 
| director       = George Fitzmaurice Harry dAbbadie dArrast (uncredited and replaced by Fitzmaurice)
| producer       = Samuel Goldwyn
| writer         = Eugene Wiley Presbrey (play) E. W. Hornung (play and novel) Sidney Howard
| starring       = Ronald Colman Kay Francis
| music          = 
| cinematography = 
| editing        = Stuart Heisler
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       = July 24, 1930
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} titular character, a proper English gentleman who moonlights as a notorious jewel thief, and Kay Francis as his love interest. It is based on the 1906 play Raffles, the Amateur Cracksman by E. W. Hornung and Eugene Wiley Presbrey, which was in turn adapted from the 1899 novel of the same name by Hornung.

Oscar Lagerstrom was nominated for an Academy Award for Sound, Recording.   
 in 1925 1939 film version, also produced by Goldwyn, stars David Niven in the title role.

==Plot==
Gentleman jewel thief Raffles (Ronald Colman) decides to give up his criminal ways as the notorious "Amateur Cracksman" after falling in love with Lady Gwen (Kay Francis). However, when his friend Bunny Manders (Bramwell Fletcher) tries to commit suicide because of a gambling debt he cannot repay, Raffles decides to take on one more job for Bunnys sake. He joins Bunny and Gwen as guests of Lord and Lady Melrose, with an eye toward acquiring the Melrose necklace, once the property of Empress Joséphine de Beauharnais|Joséphine.

Complications arise when a gang of thieves also decides to try for the necklace at the same time.  Inspector Mackenzie of Scotland Yard (David Torrence) gets wind of their plot and shows up at the Melrose estate with his men. Burglar Crawshaw breaks into the house and succeeds in stealing the jewelry, only to have Raffles take it away from him. Crawshaw is caught by the police, but learns his robbers identity.

Meanwhile, both Gwen and Mackenzie suspect that Raffles is the famous jewel thief. When the necklace is not found, Mackenzie insists that all the guests remain inside, then quickly changes his mind. Gwen overhears Mackenzie tell one of his men that he intends to let Crawshaw escape, expecting the crook to go after Raffles and thereby incriminate him. She follows Raffles back to London to warn him.

Crawshaw does as Mackenzie anticipated. However, Raffles convinces Crawshaw that it is too dangerous to pursue his original goal with all the policemen around and helps him escape. Then, Raffles publicly confesses to being the Amateur Cracksman. When Lord Melrose shows up, Raffles reminds him of the reward he offered for the necklaces return (conveniently the same amount that Bunny owes) and produces the jewelry. Then, he outwits Mackenzie and escapes, after arranging with Gwen to meet her in Paris.

==Cast==
*Ronald Colman as A.J. Raffles
*Kay Francis as Gwen
*Bramwell Fletcher as Bunny
*Frances Dade as Ethel Crowley
*David Torrence as Inspector McKenzie
*Alison Skipworth as Lady Kitty Melrose
*Frederick Kerr as Lord Harry Melrose
*John Rogers as Crawshaw
*Wilson Benge as Barraclough

==Production== silent and talking version.

==Reception==
Raffles was a substantial hit with audiences and critics when it was released in the summer of 1930.  The movie was released on DVD by Warner Archive Collection in 2014.  Reviewing the disc (which also featured the David Niven 1939 version), Paul Mavis of DVDTalk.com wrote, Raffles cleanly mixes equal doses of humor and criminal derring-do, along with potent dashes of "Colmanized" romance for the actors core female audience....Since this was written and shot prior to the enforcement of the Production Code, theres an enjoyably tolerant (and modern feeling) looseness to the Raffles character thats buttoned back up for the Niven remake." 

==References==
 

== External links ==
*  
 

 
 

 
 
 
 
 
 
 
 
 
 
 