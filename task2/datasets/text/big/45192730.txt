The 400 Million
{{Infobox film
| name           = The 400 Million
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Joris Ivens
| producer       =  
| writer         = Joris Ivens John Ferno
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = Frederic March
| music          = Hans Eisler
| cinematography = John Ferno Robert Capa
| editing        = 
| studio         =  
| distributor    =  
| released       =  
| runtime        = 52 minutes
| country        = 
| language       = 
| budget         = 
| gross          =  
}}
  East Asian guerrilla forces Chinese resistance Japanese Second Mandarin is American actors. Anderson, Kevin Taylor.   Routledge (New York), 2006.  Anderson, Kevin Taylor.   Routledge (New York), 2013. ISBN 9780415596428. 

==Name== population of China,   roughly estimated from the highly untrustworthy official censuses. 

==Narrative== Japanese Pacification bombing in Chinese history Republic and history of Chinese factions as united against the invaders and ends with some small Chinese victories to suggest a turning tide of battle, especially praising the 8th Route Army.  

==Legacy== American propaganda film The Battle of China,   which also made prominent use of "The March of the Volunteers", the song which later became the Chinese National Anthem.

The camera from the film was donated by Ivens to the Chinese Communist Party, which used it to begin the Yanan Film Group.  

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 