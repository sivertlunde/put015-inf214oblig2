Pretty Woman
 
 
 
{{Infobox film
|name= Pretty Woman
|image= Pretty_woman movie.jpg
|border= yes
|caption= Theatrical release poster
|alt= A man in a suit stands back to back with a woman wearing a short skirt and thigh high boots.
|director= Garry Marshall
|producer= Arnon Milchan Steven Reuther Gary W. Goldstein
|writer= J. F. Lawton
|starring= Julia Roberts Richard Gere
|music= James Newton Howard
|cinematography= Charles Minsky
|editing= Raja Gosnell Priscilla Nedd
|studio= Touchstone Pictures Silver Screen Partners IV Buena Vista Pictures
|released=  
|runtime= 119 minutes
|country= United States
|language= English
|budget= $14 million
|gross= $463,407,268
}}

Pretty Woman is a 1990 American romantic comedy film set in Los Angeles. Written by J. F. Lawton and directed by Garry Marshall, it stars Richard Gere and Julia Roberts, and features Hector Elizondo, Ralph Bellamy (in his final performance), Laura San Giacomo and Jason Alexander in supporting roles. Its story centers on the down-on-her-luck Hollywood sex worker Vivian Ward who is hired by Edward Lewis, a wealthy businessman, to be his escort for several business and social functions, and their developing relationship over the course of her week-long stay with him.

Originally intended to be a dark cautionary tale about class and sex work in Los Angeles, the film was reconceived as romantic comedy with a large budget. It was widely successful at the box office, and it became one of the highest money-makers of 1990.

Today the film is one of the most financially successful entries in the romantic comedy genre, with an estimated gross income of $463.4 million. 

The film is one of the most popular films of all time; it saw the highest number of ticket sales in the US ever for a romantic comedy,  with Box Office Mojo listing it as the #1 romantic comedy by the highest estimated domestic tickets sold at 42,176,400, slightly ahead of My Big Fat Greek Wedding (2002) at 41,419,500 tickets.
 Writers Guild BAFTA Award. Runaway Bride (1999), which reunited Gere and Roberts under the direction of Garry Marshall once again.

==Plot==
Edward Lewis (Richard Gere), a successful workaholic businessman and corporate raider in Los Angeles on business, accidentally takes a detour on Hollywood Boulevard while looking for Beverly Hills. He unsuccessfully tries to ask for directions and ends up in the red-light district, where he encounters a sex worker named Vivian Ward (Julia Roberts), who mistakes him for a possible client. Her roommate and best friend, Kit De Luca (Laura San Giacomo), encourages her to try and recruit him, though he only wants directions and makes a deal with her to pay her if she shows him the way. Intrigued by her knowledge and wit he hires her to spend the night with him for $300 in his hotels penthouse apartment, treating her to strawberries and champagne and sharing personal information with her. He comes to trust her as she "surprises" him by flossing her teeth after eating the strawberries (and not doing drugs as he thought she was more likely to do given her profession). They spend the night watching I Love Lucy reruns and having sex.
 dinner etiquette. Edward returns and is visibly amazed by her transformation. The business dinner does not end well, however, with Edward making clear his intention to dismantle James Morses corporation once it was bought, close down the shipyard which Morse spent 40 years building, and sell the land for real estate. He and his grandson, David abandon their dinner in anger, while Edward remains preoccupied with the deal afterward. Back at the hotel, he reveals to Vivian that he had not spoken to his recently deceased father for 14 and half years. She attempts to cheer him up but he rebuffs her and leaves. Later that night she, lonesome and worried, goes looking for him. She tries again to console him and they end up having sex on the grand piano in the hotel lounge.

The next morning Edward asks Vivian about her buying only one dress and she upset, tells him about the snubbing that took place the day before. He takes her to a store where he makes sure the staff will suck up to her and indulge her. He leaves her with money and his credit cards and she goes on a shopping spree. Now wearing very elegant clothes Vivian returns carrying all the bags, to the shop that had snubbed her, telling the saleswomen they had made a big mistake and announcing nonchalantly that she "has to go shopping now" leaving them speechless.

The following day, Edward takes Vivian to a polo match he sponsors in hopes of networking for his business deal. She meets some of his snooty upperclass friends and tells him that she understands why he came looking for her. While she chats with David Morse, who is there playing polo, Phillip Stuckey, Edwards attorney begins to suspect that she is a spy. Edward reassures him by telling him how they met, and that she is in fact a sex worker, and Phillip (Jason Alexander) approaches her and offers to hire her once she is finished with Edward, insulting her. When they return to the hotel, she is furious with Edward for telling Phillip about her and plans to leave, but he apologizes, confessing his jealousy towards David and persuades her to see out the week. He elated, leaves work early the next day to take her on a date to the opera in San Francisco in his private jet. Vivian is moved to tears by the opera (which is La traviata, whose plot deals with a rich man tragically falling in love with a courtesan). Clearly growing closer they later play chess and she convinces him to take the next day off.

They spend the next day entirely together talking and enjoying each others company with Edward going so far as defend her from the drug dealer to whom Kit owes money. During the night Vivian breaks her "no kissing on the mouth rule" and they end up making love for the very first time. Just before she falls asleep in his arms she admits that shes in love with him. Over breakfast the next day he offers to put her up in an apartment so she can be off the streets but she rejects it, insulted and says this is not the "fairy tale" she wants. Telling him a story of her childhood fantasy. He then goes off to work without resolving the situation. Kit, collecting money from her, comes to the hotel and realizes that she is in love with Edward and reluctantly encourages Vivian to pursue a relationship with him.

In the meantime Edward meets with Morse and is about to close the deal, but changes his mind at the last minute. His time with Vivian has shown him another way of living and working, taking time off and enjoying activities he never cared for. As a result, his strong interest towards his business is put aside. He decides that he would rather help Morse than take over his company. Furious over the loss of so much money, Phillip goes to the hotel to confront Edward, but only finds Vivian. He blames her for changing Edward and tries to make a pass at her. She bites his hand to make him stop and a furious Phillip slaps and tries to rape her. Edward arrives in time to stop him, hitting him while chastising him for his greed and then throws him out.
 knight on a white horse rescuing the princess from the tower, the childhood fantasy Vivian told him about. The film ends as they kiss passionately on the fire escape.

==Cast==
* Richard Gere as Edward Lewis, a rich, ruthless businessman from New York who is alone on business for a week in Los Angeles. At the start of the film, he borrows a Lotus Esprit from his lawyer and, not being able to drive it well, winds up lost in the red-light district. While asking for directions back to the Beverly Wilshire Hotel he meets a hooker named Vivian.
* Julia Roberts as Vivian Ward, a beautiful hooker with a heart of gold on Hollywood Boulevard, who is independent and assertive—refusing to have a pimp and fiercely reserving the right to choose her customers and what she would do and not do when with them. She runs into Edward, a wealthy businessman, when he asks her for directions to Beverly Hills. Edward hires Vivian for the night and offers her $3,000 to spend the week as his escort to business social engagements.
* Ralph Bellamy as James Morse, a businessman and owner of an underperforming company that Edward is interested in buying and breaking up. Edward later has a change of heart and offers to partner with him for a Navy shipbuilding contract that would effectively make his company strong again.
* Jason Alexander as Phillip Stuckey, Edwards insensitive lawyer. He pesters Edward after he sees Vivian and David Morse getting along. After learning that Vivian is a sex worker, he propositions her (to her dismay). After a lucrative deal falls through because of Edwards feelings for her, he angrily tries to force himself on her but is stopped by Edward. The epitome of corporate greed, he represents what Edward might have become had he not met her and changed his outlook on life.
* John David Carson as Mark Roth, a businessman in Edwards office.
* Laura San Giacomo as Kit De Luca, Vivians wisecracking friend and roommate, who spent their rent money on drugs. After Vivian gives her rent money and a little more, while telling her that she has "potential", an inspired Kit begins to plan for a life off the streets.
* Alex Hyde-White as David Morse, James Morses grandson, who is smart and is being groomed to take over the Morse Company when his grandfather either dies or retires. He plays polo and might have feelings toward Vivian as he shows her his horse during the game that she and Edward attend.
* Amy Yasbeck as Elizabeth Stuckey, Phillips wife, who likes to be the center of attention in everything. She is quite sarcastic to Vivian when they first meet at the polo game, although she does tell Edward that Vivian is sweet.
* Elinor Donahue as Bridget, a friend of Barney Thompson who works in a womens clothing store and is asked by him to help Vivian purchase a dress after she has an encounter with two snobby women in another dress store.
* Héctor Elizondo as Barney Thompson, the stuffy but golden-hearted manager of the hotel. At first, he does not hide his disdain for Vivian, but he eventually befriends her, helps her buy a cocktail dress, and gives her lessons in table manners.
* Judith Baldwin as Susan, one of Edwards ex-girlfriends, with whom Edward reunites at the beginning of the film. She has married and reveals to him that his secretary was one of her bridesmaids.
* Laurelle Brooks Mehus as the night desk clerk where among other scenes she shared the opening hotel scene with Vivian and Edward.
* James Patrick Stuart as the day bellhop who carries Vivians new clothes for her after her shopping spree.
* Dey Young as a snobby saleswoman in a dress store. Larry Miller as Mr. Hollister, the salesman in the clothing store where Vivian buys her cocktail dress and many other outfits using Edwards credit card.

The movie also features Hank Azaria in his first speaking role, playing a detective early in the movie.

==Production==

===Development===
The film was initially conceived to be a dark drama about sex work in Los Angeles in the 1980s.    The relationship between Vivian and Edward also originally harboured controversial themes, including the concept of having Vivian addicted to drugs; part of the deal was that she had to stay off cocaine for a week, because she needed the money to go to Disneyland. Edward eventually throws her out of his car and drives off. The movie was scripted to end with Vivian and her sex worker friend on the bus to Disneyland.  These traits, considered by producer Laura Ziskin to be detrimental to the otherwise sympathetic portrayal of her, were removed or incorporated into the character of Kit. These deleted scenes have been found in public view, and some were included on the DVD released on the films 15th anniversary.  One such scene has Vivian offering Edward, "I could just pop ya good and be on my way", indicating a lack of interest in "pillow talk". In another, she is confronted by drug dealers outside The Blue Banana, and rescued by Edward and Darryl.
 play of Broadway musical Walt Disney Studios president Jeffrey Katzenberg who insisted the film should be re-written as a modern-day fairy tale with qualities of a love story, as opposed to being the dark drama it was originally developed as. It was pitched to Touchstone Pictures and re-written as a romantic comedy. The original script was titled $3,000,  however this title was changed because Disney executives thought it sounded like a title for a science fiction film.  It also has unconfirmed references to That Touch of Mink, starring Doris Day and Cary Grant.

The film is one of the two movies that triggered the resurrection of the romantic comedy genre in Hollywood, the other being When Harry Met Sally. After completion of the 1960s Doris Day/Rock Hudson romantic comedies, the genre fell out of favor: not a single romantic comedy was produced by Hollywood studios in the 1970s. Following its success, Roberts became the romantic comedy queen of the 1990s.

===Casting===
Casting of the film was a rather lengthy process. Marshall had initially considered Christopher Reeve, Daniel Day-Lewis, and Denzel Washington for the role of Lewis, and Al Pacino turned it down.  Pacino went as far as doing a casting reading with Roberts before rejecting the part.  Gere agreed to the project. Reportedly, Gere started off much more active in his role, but Garry Marshall took him aside and said "No, no, no. Richard. In this movie, one of you moves and one of you does not. Guess which one you are?" 
Julia Roberts was not the first-choice for the role of Vivian, and was not wanted by Disney. Many other actresses were considered at the time. Marshall originally envisioned Karen Allen for the role. When she declined, it went to many better-known actresses of the time including Molly Ringwald,    who turned it down because she felt uncomfortable with the content of the script, and did not like the idea of playing a sex worker. Winona Ryder, a popular box-office draw at the time, auditioned, but was turned down because Marshall felt she was "too young". Jennifer Connelly was also dismissed for the same reason. 

Meg Ryan, who was a top choice of Marshalls, turned it down. According to a note written by Marshall, Mary Steenburgen was the first choice to play Vivian. Diane Lane came very close to being cast as Vivian (which had a much darker script at the time), but due to scheduling was unable to take the role. They had gone as far as costume fittings with Lane. Michelle Pfeiffer turned the role down as well, because she did not like the "tone" of the script.    Daryl Hannah was also considered, but turned the role down because she believed it was "degrading to women".  Valeria Golino also turned it down as she did not think the movie could work with her thick Italian accent. Jennifer Jason Leigh had auditioned for the part.  When all the other actresses turned down the role, 21-year-old Julia Roberts, who was relatively unknown at the time, with the exception of her Oscar-nominated performance in Steel Magnolias (1989), won the role of Vivian. Her performance made her a star.

===Filming=== Walt Disney Ambassador Hotel Esprit SE (which was later sold). This gamble paid off as the sales of the Lotus Esprit tripled during 1990-91. 

Shooting was a generally pleasurable and easy-going experience for those involved, as the films budget was broad and the shooting schedule was not tight.  While shooting the scene where Vivian is lying down on the floor of Edwards penthouse, watching reruns of I Love Lucy, in order to achieve a genuine laughter, Garry Marshall had to tickle her feet (out of camera range) to get her to laugh so hysterically, which is featured in the film. Likewise the scene in which Gere playfully snaps the lid of a jewelry case on her fingers was improvised by him, and her surprised laugh was genuine, while the dress worn by her in that scene has been included in a list of the most unforgettable dresses of all time. 
 Prince in the bathtub sliding down and dunking her head under the bubbles, she came up and opened her eyes and saw that everyone had left except the cameraman, who got the shot. In addition, during the love-making scene between her and Gere, she got so nervous that a vein visibly popped out on her forehead. She also developed a case of hives, and calamine lotion was used to clear them until shooting could resume.  The filming was completed on October 18.

==Reception==

===Box office===
In its opening weekend, the film opened at #1 at the box office grossing $11,280,591 and averaging $8,513 per theater.    Despite it dropping to number two in its second weekend, it grossed more in its second weekend, grossing $12,471,670.  It remained number one at the box office for four non-consecutive weeks and on the top ten for sixteen weeks.  It has grossed $178,406,268 in the United States and $285,000,000 in other countries for a total worldwide gross of $463,406,268.    It was also the fourth highest-grossing film of the year in the United States    and the third highest-grossing worldwide.   

===Critical response  ===
The film received mixed reviews from critics. Review aggregator Rotten Tomatoes reports that 62% of 55 film critics have given it a positive review, with a rating average of 5.7 out of 10.  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives it a score of 51 based on 17 reviews. 

Owen Gleiberman of Entertainment Weekly gave the film a D, stating that the film "starts out as a neo-Pygmalion comedy" and with "its tough-hooker heroine, it can work as a feminist version of an upscale princess fantasy." Gleiberman also said that it "pretends to be about how love transcends money" and that it "is really obsessed with status symbols."  On its twentieth anniversary, Gleiberman wrote another article explaining his review, ultimately saying that although he felt he was right, hed have given it a B today.  Carina Chocano of The New York Times said that movie "wasnt a love story, it was a money story. Its logic depended on a disconnect between character and narrative, between image and meaning, between money and value, and that made it not cluelessly traditional but thoroughly postmodern."  Phil of "StraightDope.com" says "Here comes rich guy and he just decides he wants a prostitute and he gets her and her respect through his power and money. Doesnt hurt that he looks like Richard Gere." 

===Accolades===
;Awards

;Nominations
* Cesar Awards 1991
** Best Foreign Film

* 63rd Academy Awards
** Best Actress - Julia Roberts

* 48th Golden Globe Awards
** Best Motion Picture (Comedy or Musical)
** Best Actor - Richard Gere
** Best Actress - Julia Roberts (won)
** Best Supporting Actor - Hector Elizondo

* Writers Guild of America Award for Best Original Screenplay - J. F. Lawton

==Music== Hot 100 Go West, "Show Me Your Soul" by the Red Hot Chili Peppers, "No Explanation" by Peter Cetera, "Wild Women Do" by Natalie Cole and "Fallen" by Lauren Wood. The soundtrack went on to be certified three times platinum by the Recording Industry Association of America|RIAA. 
 Prince while Geres character is on the phone. Background music is composed by James Newton Howard. Entitled "He Sleeps/Love Theme", this piano composition is inspired by Bruce Springsteens "Racing in the Street".

===Soundtrack===
 
{{Infobox album| 
|Name= Pretty Woman
|Type= Soundtrack
|Artist= Various artists
|Cover= Pretty Woman OST.jpg
|Border= yes
|Released= February 14, 1990 Recorded = Rock
|Length= 43:36
|Label= EMI Producer = Last album = This album = Next album =
}}

{{Album ratings
|rev1= AllMusic
|rev1Score=    
}}
The soundtrack was released on February 14, 1990  
;Track listing
{{Track listing
|total_length= 43:36
|title1= Wild Women Do
|note1= performed by Natalie Cole
|length1= 4:06 Fame 90
|note2= performed by David Bowie
|length2= 3:36
|title3= King of Wishful Thinking Go West
|length3= 4:00
|title4= Tangled
|note4= performed by Jane Wiedlin
|length4= 4:18
|title5= It Must Have Been Love
|note5= performed by Roxette
|length5= 4:17
|title6= Life in Detail Robert Palmer
|length6= 4:07
|title7= No Explanation
|note7= performed by Peter Cetera
|length7= 4:19
|title8= Real Wild Child (Wild One)
|note8= performed by Christopher Otcasek
|length8= 3:39
|title9= Fallen
|note9= performed by Lauren Wood
|length9= 3:59
|title10= Oh, Pretty Woman
|note10= performed by Roy Orbison
|length10= 2:55
|title11= Show Me Your Soul
|note11= performed by Red Hot Chili Peppers
|length11= 4:20
}}
 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 