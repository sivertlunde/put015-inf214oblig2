Long Pigs
 
{{Infobox film 
| name           = Long Pigs
| image          = long_pigs.jpg
| image_size     = 
| alt            = Long Pigs Movie Poster
| caption        = Theatrical release poster
| director       = Chris Power   Nathan Hynes
| producer       = 
| writer         = Chris Power   Nathan Hynes
| starring       = Anthony Alviano   Jean-Marc Fontaine   Paul Fowles
| music          = 
| cinematography = 
| editing        = 
| studio         = Clowns After Midnight Productions   Jordan Entertainment   Chris Bridges Effects Studio
| distributor    = R-Squared Films
| released       =  
| runtime        = 81 minutes
| country        = Canada
| language       = English 
| budget         = $CAD250,000 (estimated) 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} filmmakers who Max Payne and many others.

Filming took place in Toronto, Canada.

==Plot== cannibalistic serial killer named Anthony McAllister, who has agreed to let them document every aspect of his horrifically violent life-style.  Initially terrified, the filmmakers get to know Anthony as a person. They even begin to identify with his ecological and philosophical justifications for his cannibalistic lifestyle.

Its only when they investigate further that the filmmakers begin to doubt Anthonys accounts of his past. Tensions noticeably rise as the filmmakers continue to confront Anthony on his conflicting stories and ever-changing philosophies. During an awkward interview, the filmmakers interview Merle, the father of a young girl who was abducted and never found. This abduction was Anthonys first child victim. Merle welcomes them into his home and gives them a heart-felt experience that even Anthony is touched by.

As the documentary reaches its conclusion, Anthony begins to become uncomfortably aware of the how much of his life he has revealed to these filmmakers. In a final interview with Anthony, a deadly confrontation erupts and all that remains is a broken camera and this footage. This film challenges the audience to view a horrific side of humanity and delivers a social commentary that leaves the audience thinking about the world they live in and the power and effect of all media.

== Cast ==
* Anthony Alviano as Anthony McAlistar
* Jean-Marc Fontaine as the restaurant manager
* Paul Fowles as Merle, the father of the missing child
* Shane Harbinson  as Det. Ken Walby
* Roger King  as Tony Prince, the radio host
* Kelly McIntosh  as Rebecca Stapleton (as Kelly MacIntosh)
* Brad Mittelman  as Simon Sullivan
* John Terranova  as John Vierra
* Vik Sahay  as Dr. Hooshangi
* Barbara Walsh as Lucy, the prostitute

==Awards==
Best Picture: Moving Image Film Fest (Toronto) 
Best Picture: Mockfest Film Fest (LA) 
Best Actor: Mockfest Film Fest (LA) 
Best Horror: Evil City Film Fest (NYC) 
Best Horror: International Horror & Sci-Fi Fest (PHX) 
Best Feature: Texas Frightmare Weekend (TX) 
Best Mockudrama: Ricon International Film Fest (PR)

==DVD Release==
After 3 years of being screened at select theaters, Long Pigs was released on DVD on   through Big Bite Entertainment.  Limited editions of the DVD included edible jerky supposedly prepared by in-character Anthony McAlistar.

== See also ==

 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 