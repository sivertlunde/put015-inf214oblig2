The Fog (2005 film)
 
 
{{Infobox film
| name           = The Fog
| image          = The Fog 2005 film.jpg
| director       = Rupert Wainwright
| producer       = John Carpenter David Foster Debra Hill
| writer         = Cooper Layne
| based on       = Characters created by John Carpenter Debra Hill
| starring       = Tom Welling Maggie Grace Selma Blair Rade Šerbedžija DeRay Davis Sonja Bennett Kenneth Welsh Adrian Hough
| music          = Graeme Revell
| cinematography = Nathan Hope Ian Seabrook (underwater)
| editing        = Dennis Virkler
| studio         = Revolution Studios
| distributor    = Columbia Pictures  (Sony Pictures Entertainment) 
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $18 million
| gross          = $46,201,432   
}}
 film of the same name and was produced by Carpenter and Debra Hill who co-wrote the original film.

==Plot==
 
 leper named clipper ship the Elizabeth Dane and set it on fire, killing all aboard. 134 years later, the residents of Antonio Island prepare to honor their founding fathers the same men who burned the Elizabeth Dane and a statue of them is to be unveiled on the towns anniversary. During a boating trip, Nick Castle and his friend Spooner unwittingly disturb a bag containing a pocket-watch and a hairbrush from the Elizabeth Dane lying on the seabed.

That night, Nick meets his former girlfriend, Elizabeth Williams, who has returned after six months away. Elizabeth is shown the antique pocket-watch by Machen, an old man who found it washed up on the beach. He warns her ominously "if you touch it, things will change." The watch begins ticking as Elizabeth holds it. She sees a hallmark on it, which includes a set of scales. Supernatural occurrences then begin to plague the town. Objects move by themselves, power outages occur, and the windows in Nicks truck inexplicably shatter. Nick and Elizabeth then encounter drunken priest Father Malone, who is ranting about murderers and retribution. Meanwhile, at the local radio station, host Stevie Wayne gets a phone call from weatherman Dan about a large fog bank off the coast. Out at sea on Nicks boat, Spooner and Nicks cousin Sean are partying with two young women, Mandi and Jennifer. As the fog reaches them, the boats engine stops and the instruments break. An old clipper ship appears in the fog next to them. Seemingly possessed, Jennifer draws a set of scales on a misted window inside the boat. Unseen forces then horrifically kill Mandi, Jennifer and Sean. At Nicks beach house, Elizabeth has been dreaming about the Elizabeth Dane. She searches the Internet for information about the hallmark symbol she saw earlier, but her computer malfunctions and the word "Dane" appears on the screen. She hears a knock at the front door, goes outside but finds nothing. Walking down to the beach, the fog begins moving in but Nick brings her back inside.

The next day, Nicks Uncle Hank telephones him about the disappearance of his boat. Nick and Elizabeth sail out and find the vessel and the three corpses. Elizabeth goes into the hold and finds Spooner alive in a freezer. They return to the island where Mayor Tom Malone Father Malones father suspects Spooner of the murders. In the morgue, Seans corpse briefly rises up and accosts Elizabeth. At the library, Elizabeth researches the scales symbol seen on the watchs hallmark. It represented an old trading colony north of Antonio Island, which was afflicted with leprosy. At the docks, Elizabeth finds the buried journal of Patrick Malone from 1871. She and Nick learn the story of the Elizabeth Dane and realize the founders built the town with the fortune they had stolen from the ship, but kept this secret from their families and the townsfolk.

The ghosts of the Elizabeth Dane seek revenge against Antonio Islands residents for the past crimes of its founding fathers. After killing Dan at the weather station, they pursue Stevies son Andy and his Aunt Connie at home. Connie is killed but Nick and Elizabeth rescue Andy. In her car, Stevie is also attacked but escapes. They all make their way to the Town Hall where the founders murderous secrets are exposed. The spirits kill Hank Castle, Kathy Williams and the Malones. The ghost of Blake then seeks Elizabeth. Despite being a descendant of David Williams, Elizabeth is the reincarnation of Blakes wife and was one of her ancestors victims; hence, her mysterious dreams about the Elizabeth Dane. Blake kisses Elizabeth and she transforms into a spirit and disappears as Nick watches helplessly. The next day, the survivors try to cope with their traumatic experiences and the truth about their ancestors. As Stevie reflects on the nights events with her listeners, Nick throws Patrick Malones journal into the sea.

==Cast==
{{columns-list|2|
* Tom Welling as Nicholas "Nick" Castle
* Maggie Grace as Elizabeth Williams
* Selma Blair as Stevie Wayne
* DeRay Davis as Spooner
* Kenneth Welsh as Mayor Tom Malone
* Adrian Hough as Father Robert Malone
* Sonja Bennett as Mandi
* Sara Botsford as Kathy Williams
* Cole Heppell as Andy Wayne
* R. Nelson Brown as Machen
* Mary Black as Aunt Connie 
* Jonathon Young as Dan the weatherman
* Meghan Heffern as Jennifer 
* Alex Bruhanski as Hank Castle
* Matthew Currie Holmes as Sean Castle Rade Sherbedgia as Captain William Blake
* Christian Bocher as Patrick Malone
* Douglas H. Arthurs as David Williams
* Yves Cameron as Richard Wayne
* Charles André as Norman Castle
}}

==Production==
 
The original films makers, John Carpenter and Debra Hill, expressed interest in remaking the original film on the films DVD commentary.  They are both credited as producers for the remake. The 2005 film was green-lit by Revolution Studios before the script was finished. Tom Welling had three weeks filming on the fourth season of his TV series Smallville (TV series)|Smallville when he started work on The Fog. Selma Blair said that they kept two cameras running during Wellings scenes; one for The Fog and one for Smallville. Blair performed almost all of her stunts and spent 12 hours in a water tank with only short surface breaks for two days to shoot her underwater scenes. StudioCanal which owns the rights to the original film assisted in the films production in exchange for French distribution rights.

Although set on an island off the Oregon coast, the location scenes were filmed in Canada. Some scenes were filmed around Cowichan Bay, most of the beach scenes were filmed in Tofino, and those of the town of Antonio Bay were filmed in Fort Langley; all in British Columbia.  It was one of Hills final projects before her death from cancer in 2005.

==Reception==

===Critical reception===
 
The film was not screened for critics before its release. 
Rotten Tomatoes gives the film a rating of 4% based on 68 reviews, and is the 66th worst reviewed film on the website.   The sites critical consensus states, "The Fog is a so-so remake of a so-so movie, lacking scares, suspense or originality."  Metacritic gave the film an average score of 27/100, based on 16 reviews. 

The Fog was widely considered an unsuccessful remake of the original 1980 movie.  The The Hollywood Reporter said the remake "lack  the scares necessary to satisfy its target audience",  and Variety (magazine)|Variety said that "interest lags between the grisly deaths, and, worse, none of the characters generates rooting interest."    The film was rated D- by Owen Gleiberman of Entertainment Weekly. 

===Box office===
In the US, The Fog grossed $11,752,917 on its opening weekend and secured the number one spot for that weekend due to a general decline in box office revenues at that time. Had the film opened with the same amount a year earlier, it would have only made fifth place.  The film had a final domestic gross of $29,550,869, and a total worldwide gross of $46,201,432. 

=== Awards ===
  
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Fangoria Chainsaw Awards Worst Film
|The Fog
| 
|- Stinkers Bad Movie Awards   Least Scary Horror Movie
|The Fog
| 
|-
|}

==Music==
{{Infobox album
| Name = The Fog: Original Motion Picture Soundtrack
| Type = Film score
| Longtype=
| Artist = Graeme Revell
| Cover = 
| Released = November 1, 2005
| Length =  
| Label =  Varèse Sarabande
| Reviews =
}}

{{Track listing
| collapsed       = no
| headline        = The Fog: Original Motion Picture Soundtrack
| extra_column    = 
| total_length    = 39:20

| all_writing     = Graeme Revell
| all_lyrics      = 
| all_music       =

| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1          = Prologue
| note1           = 
| writer1         = 
| lyrics1         = 
| music1          = 
| extra1          = 
| length1         = 2:31

| title2          = Gods Country 
| note2           = 
| writer2         = 
| lyrics2         = 
| music2          = 
| extra2          = 
| length2         = 0:38

| title3          = Anchor Lockup
| note3           = 
| writer3         = 
| lyrics3         = 
| music3          = 
| extra3          = 
| length3         =  1:51

| title4          = It Wants Us 
| note4           = 
| writer4         = 
| lyrics4         = 
| music4          = 
| extra4          = 
| length4         = 2:20

| title5          = The Hallmark 
| note5           = 
| writer5         = 
| lyrics5         = 
| music5          = 
| extra5          = 
| length5         = 1:27

| title6          = Shower Love 
| note6           = 
| writer6         = 
| lyrics6         = 
| music6          = 
| extra6          = 
| length6         = 1:12

| title7          = Elizabeth...
| note7           = 
| writer7         = 
| lyrics7         = 
| music7          = 
| extra7          = 
| length7         = 2:52

| title8          = Boathouse
| note8           = 
| writer8         = 
| lyrics8         = 
| music8          = 
| extra8          = 
| length8         = 1:36

| title9          = Statues
| note9           = 
| writer9         = 
| lyrics9         = 
| music9          = 
| extra9          = 
| length9         = 2:00

| title10         = Lights Out
| note10          = 
| writer10        = 
| lyrics10        = 
| music10         = 
| extra10         = 
| length10        = 1:31

| title11         = Island History
| note11          = 
| writer11        = 
| lyrics11        = 
| music11         = 
| extra11         = 
| length11        = 1:43

| title12         = The Search
| note12          = 
| writer12        = 
| lyrics12        = 
| music12         = 
| extra12         = 
| length12        = 3:18

| title13         = Burned Image
| note13          = 
| writer13        = 
| lyrics13        = 
| music13         = 
| extra13         = 
| length13        = 0:46

| title14         = Its Here
| note14          = 
| writer14        = 
| lyrics14        = 
| music14         = 
| extra14         = 
| length14        = 3:39

| title15         = Crime Aboard
| note15          = 
| writer15        = 
| lyrics15        = 
| music15         = 
| extra15         = 
| length15        = 2:42

| title16         = Tragedy on the Elizabeth Dane
| note16          = 
| writer16        = 
| lyrics16        = 
| music16         = 
| extra16         = 
| length16        = 3:18

| title17         = The Reckoning
| note17          = 
| writer17        = 
| lyrics17        = 
| music17         = 
| extra17         = 
| length17        = 1:50

| title18         = The Fog Recedes 
| note18          = 
| writer18        = 
| lyrics18        = 
| music18         = 
| extra18         = 
| length18        = 1:41

| title19         = Epilogue
| note19          = 
| writer19        = 
| lyrics19        = 
| music19         = 
| extra19         = 
| length19        = 1:17
}}
;Songs featured in the motion picture, but not in the commercial release
* "Sugar, Were Goin Down" – Performed by Fall Out Boy
* "Salomes Wish" – Performed by The Booda Velvets
* "Vibrate" – Performed by Petey Pablo
* "Take Off Your Clothes" – Performed by Morningwood
* "Nighttime" – Performed by Petracovich
* "Feels Just Like It Should" – Performed by Jamiroquai
* "Invincible" – Performed by OK Go
* "What Died" – Performed by Nichole Alden

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 