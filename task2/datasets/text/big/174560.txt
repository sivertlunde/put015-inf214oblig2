Willy Wonka & the Chocolate Factory
 
{{Infobox film
| name           = Willy Wonka & the Chocolate Factory
| image          = WillyWonkaMoviePoster.jpg
| border         = yes
| image_size     = 225px
| alt            =
| caption        = Original theatrical release poster
| director       = Mel Stuart
| producer       = {{Plainlist |
* Stan Margulies
* David L. Wolper
}}
| screenplay     = {{Plainlist |
* Roald Dahl
* David Seltzer (uncredited)
}}
| based on       =  
| starring       = {{Plainlist |
* Gene Wilder
* Jack Albertson
* Peter Ostrum
* Roy Kinnear
* Denise Nickerson
* Leonard Stone
* Julie Dawn Cole
* Paris Themmen Dodo Denney 
}}
| music          = {{Plainlist |
* Leslie Bricusse and Anthony Newley
}}
| cinematography = Arthur Ibbetson
| editing        = David Saxon
| studio         = Wolper Productions
| distributor    = Paramount Pictures 
| released       =   
| runtime        = 100 minutes
| country        = United States, United Kingdom, Germany
| language       = English
| budget         = $3 million
| gross          =  
}}
 musical fantasy adaptation of Charlie Bucket (Peter Ostrum, in his only film appearance) as he receives a Golden Ticket and visits Willy Wonkas chocolate factory with four other children from around the world.

Filming took place in Munich in 1970, and the film was released by Paramount Pictures on June 30, 1971. With a budget of just $3,000,000, the film received positive reviews and performed well in 1971, but it was not a huge box-office success, only earning about $1,000,000 more than its budget at the end of its original run. It then made an additional $21 million during its 1996 re-release. 
 Best Original Fiddler on the Roof. In 2014, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

Until 1977, Paramount distributed the film. From then on, all the rights to the film were handed over to Warner Bros. for home entertainment purposes starting in the 1980s. The film had become a big success in that medium ever since.

==Plot==
In an unnamed European town, children go to a candy shop after school. Charlie Bucket, whose family is poor, can only stare through the window as the shop owner sings "Candy Man". The newsagent for whom Charlie works after school gives him his weekly pay, which Charlie uses to buy a loaf of bread. On his way home, he passes the Willy Wonkas chocolate factory. A mysterious tinker recites the first lines of William Allinghams poem "The Fairies", and tells Charlie, "Nobody ever goes in, and nobody ever comes out." Charlie rushes home to his widowed mother and his four bedridden grandparents. After he tells Grandpa Joe about the tinker, Joe tells him that Wonka locked the factory because his arch rival, Mr. Slugworth, and other candy makers sent in spies disguised as employees to steal Wonkas recipes. Wonka disappeared, but three years later began selling more candy, but the origin of Wonkas labour force is a mystery.

Wonka announces to the world that he has hidden five "Golden Tickets" in his chocolate Wonka Bars. The finders of these tickets will be given a tour of his factory and a lifetime supply of chocolate. Four of the tickets are found by Augustus Gloop, a gluttonous German boy; Veruca Salt, a spoiled British girl; Violet Beauregarde, a gum-chewing American girl; and Mike Teevee, a television-obsessed American boy. As each child is heralded to the world on TV, a sinister-looking man is observed whispering to them. News breaks that the final ticket has been found by a Paraguayan millionaire.

The next day, Charlie finds money in a gutter and uses it to buy a Wonka Bar. He has change left that he uses to buy another Wonka bar that he intends to bring to his family. On leaving the candy store, he learns from people in the street that the millionaires ticket was a fraud and that the real one is still at large. When Charlie opens the Wonka bar, he finds the final golden ticket. While racing home, he is confronted by the sinister man seen whispering to the other winners. The man introduces himself as Slugworth and offers to pay Charlie for a sample of Wonkas latest creation, the Everlasting Gobstopper.

Charlie returns home with his news. Grandpa Joe is so elated that he finds he can walk, and Charlie chooses him as his chaperone. The next day, Wonka greets the ticket winners and their chaperones at the factory gates. Each is required to sign an extensive contract before the tour begins. The factory is a psychedelic wonderland that includes a river of chocolate, edible mushrooms, lickable wallpaper, and other marvellous inventions. Wonkas workers are small, orange-skinned, green-haired Oompa-Loompas.

As the tour progresses each child ignores the rules: Augustus falls into the Chocolate River due to his temptation of chocolate, and is sucked up a pipe to the Chocolate Smelting Room. Violet blows up into a blueberry after chewing a three course meal gum that is still in experiment. After this, they reach the Fizzy Lifting Drinks Room, where Charlie and Grandpa Joe give into temptation, however they are skillful enough not to get caught, but nearly meet their ends with a giant fan in the ceiling; however, they burp to get back down to the ground. Veruca demands a goose that lays golden chocolate eggs, which leads her to falling down a Garbage chute leading to the furnace. Her father also does this after Wonka tells him about it. Mike, then meets his demise with "Wonkavision" which teleports Mike, but leaves him nearly six inches tall.

In between Augustus and Violets demises, Wonka gives the then four remaining ticket winners an Everlasting Gobstopper, but on the condition that they never talk about or show them to anyone else. At the end of the tour, Wonka, Charlie and Grandpa Joe remain, but Wonka dismisses them. Grandpa Joe follows Wonka to ask about Charlies lifetime supply of chocolate, to which Wonka tells him that because they violated the contract by stealing Fizzy Lifting Drinks, they receive nothing. Grandpa Joe denounces Wonka and suggests to Charlie that he give Slugworth the Gobstopper in revenge, but Charlie cannot bring himself to intentionally hurt Wonka; he instead returns the candy to Wonka and apologizes.

Wonka apologizes to them and reveals that "Slugworth" is actually an employee named Mr. Wilkinson, and the offer to buy the Gobstopper was a test for all the children; Charlie was the only one who passed. The trio enter the "Wonkavator", a multidirectional glass elevator that flies out of the factory. Soaring over the city, Wonka tells Charlie that his actual prize is the factory itself as Wonka created the contest to find a child honest and worthy enough to be his heir. Charlie and his family will live in the factory immediately and take over its operation when Wonka retires.

==Cast==
* Gene Wilder as Willy Wonka
* Jack Albertson as Grandpa Joe
* Peter Ostrum as Charlie Bucket
* Roy Kinnear as Henry Salt
* Julie Dawn Cole as Veruca Salt
* Leonard Stone as Sam Beauregarde
* Denise Nickerson as Violet Beauregarde Dodo Denney as Mrs. Teevee
* Paris Themmen as Mike Teevee
* Ursula Reit as Mrs. Gloop
* Michael Bollner as Augustus Gloop
* Diana Sowle as Mrs. Bucket
* Aubrey Woods as Bill the Candy Man
* David Battley as Mr. Turkentine
* Günter Meisner as Mr. Slugworth/Mr. Wilkinson
* Peter Capell as The Tinker
* Werner Heyking as Mr. Jopeck
* Peter Stuart as Winkelmann  
* Dora Altmann as Grandma Georgina
* Pat Coombs as Henrietta Salt
* Michael Goodliffe as Mr. Teavee
* Kurt Grosskurth as Mr. Gloop 
* Franziska Liebing as Grandma Josephine
* Ernst Ziegler as Grandpa George
* Frank Delfino as Auctioneer

==Production==

===Development=== adapting the book into a film came about when director Mel Stuarts 10-year-old daughter read the book and asked her father to make a film out of it, with "Uncle Dave" (producer David L. Wolper) producing it. Stuart showed the book to Wolper, who happened to be in the midst of talks with the Quaker Oats Company regarding a vehicle to introduce a new candy bar from their Chicago-based Breaker Confections subsidiary (since renamed the Willy Wonka Candy Company and sold to Nestlé). Wolper persuaded the company, who had no previous experience in the film industry, to buy the rights to the book and finance the picture for the purpose of promoting a new Quaker Oats Wonka Bar.   

It was agreed that the film would be a childrens musical, and that Dahl himself would write the screenplay.  However, the title was changed to Willy Wonka and the Chocolate Factory in order to promote the aforementioned candy tie-in. Screenwriter David Seltzer conceived a gimmick exclusively for the film that had Wonka quoting numerous literary sources, such as Arthur OShaughnessys Ode (poem)|Ode, Oscar Wildes The Importance of Being Earnest, Samuel Taylor Coleridges The Rime of the Ancient Mariner, and William Shakespeares The Merchant of Venice. Seltzer also worked Slugworth (only mentioned as a rival candy maker in the book) into the plot as an actual character. 

===Filming===
Filming commenced on April 30, 1970, and ended on November 19, 1970. The primary shooting location was Munich, Bavaria, West Germany, because it was significantly cheaper than filming in the U.S. and the setting was conducive to Wonkas factory; Stuart also liked the ambiguity and unfamiliarity of the location. External shots of the factory were filmed at the gasworks of Stadtwerke München (Emmy-Noether-Straße 10); the entrance and side buildings still exist. The closing sequence when the Wonkavator is flying above the factory is footage of Nördlingen in Bavaria.

  Munich Gasworks as it appears today (building on the left) Munich Gasworks as it appears today
File:Noerdlingen town hall from Daniel.jpg|Nördlingen, the town seen from above at the end of the movie
 

Production designer Harper Goff centered the factory on the massive Chocolate Room. According to Paris Themmen, who played Mike Teevee, "The river was made of water with food coloring. At one point, they poured some cocoa powder into it to try to thicken it but it didnt really work. When asked this question, Michael Böllner, who played Augustus Gloop, answers, It vas dirty, stinking vater." 

When interviewed for the 30th anniversary special edition, Gene Wilder stated that he enjoyed working with most of the child actors, but said that he and the crew had some problems with Paris Themmen, claiming that he was "a handful". 

==Reception==
Willy Wonka was released on June 30, 1971. The film was not a big success, being the fifty-third highest grossing film of the year in the U.S., earning just over $2.1 million on its opening weekend,  although it received positive reviews from critics such as Roger Ebert. 

Seeing no significant financial advantage, Paramount decided against renewing its distribution deal for the film when it expired in 1977. Later that year, Warner Communications, the then-parent company of Warner Bros., acquired Wolper Productions, which led to Quaker Oats selling its share of the films rights to Warner Bros. for $500,000 at the same time.

By the mid-1980s, Willy Wonka & the Chocolate Factory had experienced a spike in popularity thanks in large part to repeated television broadcasts and home video sales. Following a 25th anniversary theatrical re-release in 1996, it was released on DVD the next year, allowing it to reach a new generation of viewers. The film was released as a remastered special edition on DVD and VHS in 2001 to commemorate the films 30th anniversary. In 2003, Entertainment Weekly ranked it 25th in the "Top 50 Cult Movies" of all time.
 a new film adaptation that was released in 2005, as well as a stage musical adaptation that had its premiere in London in 2013.

The film currently holds an 89% Fresh rating on Rotten Tomatoes with the critical consensus stating "Willy Wonka & the Chocolate Factory is strange yet comforting, full of narrative detours that dont always work but express the films uniqueness". 

Willy Wonka was ranked #74 on Bravo (US TV channel)|Bravos 100 Scariest Movie Moments  for the "scary tunnel" scene.

American Film Institute Lists
*AFIs 100 Years...100 Laughs - Nominated 
*AFIs 100 Years...100 Songs:
**"The Candy Man" - Nominated 
*AFIs Greatest Movie Musicals - Nominated 
*AFIs 100 Years...100 Cheers - Nominated 
*AFIs 10 Top 10 - Nominated Fantasy Film 

===Dahls reaction===
Roald Dahl disowned the film, the script of which was partially rewritten by David Seltzer after Dahl failed to meet deadlines. Dahl said he was "disappointed" because "he thought it placed too much emphasis on Willy Wonka and not enough on Charlie," as well as the non-casting of Spike Milligan.    He was also "infuriated" by the deviations in the plot Seltzer devised in his draft of the screenplay, including the conversion of Slugworth into a spy and the "fizzy lifting drinks" scene. 

==Home media==
 mattes used to make the image widescreen are removed, revealing information originally intended to be hidden from viewers.  VHS copies were also available, but only containing the "standard" version.

A special edition DVD was released in 2001, celebrating the films 30th anniversary, although only full-screen, on August 28, 2001. Due to the lack of a letterboxed release, fan petitioning eventually led Warner Home Video to issue a widescreen version on November 13, 2001. It was also released on VHS, with only one of the special features (a making-of feature). Several original cast members reunited to film documentary footage for this special edition DVD release. The two editions featured restored sound, and better picture quality. In addition to the documentary, the DVD included a trailer, a gallery, and audio commentary by the cast.

In 2006, Warner Home Video released the film on HD DVD with all the bonus features from the 2001 DVD.  The film was released on Blu-ray Disc|Blu-ray on October 20, 2009.  It includes all the bonus features from the 2001 DVD and 2006 HD-DVD as well as a 38-page book.

In 2011, a new 40th-anniversary Blu-ray/DVD set was released on November 1, consisting of the film on Blu-ray Disc and DVD as well as a bonus features disc. The set also included a variety of rarities such as a Wonka Bar-designed tin, four scented pencils, a scented eraser, a book detailing the making of the film, original production papers and a Golden Ticket to win a trip to Los Angeles. 

==Soundtrack== Academy Award-nominated original score and songs were composed by Leslie Bricusse and Anthony Newley, and musical direction was by Walter Scharf. The soundtrack was first released by Paramount Records in 1971. On October 8, 1996, Hip-O Records (in conjunction with MCA Records, which by then owned the Paramount catalog), released the soundtrack on CD as a "25th Anniversary Edition".

The music and songs in the order that they appear in the film are:
# "Main Title" – Instrumental medley of "(Ive Got A) Golden Ticket" and "Pure Imagination"
# "The Candy Man Can" – Aubrey Woods
# "Cheer Up, Charlie" – Diana Lee (dubbing over Diana Sowle)
# "(Ive Got A) Golden Ticket" – Jack Albertson and Peter Ostrum
# "Pure Imagination" – Gene Wilder
# "Oompa Loompa (Augustus)" – The Oompa Loompas
# "The Wondrous Boat Ride"/"The Rowing Song" – Gene Wilder
# "Oompa Loompa (Violet)" – The Oompa Loompas
# "I Want It Now!" – Julie Dawn Cole
# "Oompa Loompa (Veruca)" – The Oompa Loompas
# "Ach, so fromm" (alternately titled "Mappari", from Martha (opera)|Martha) – Gene Wilder
# "Oompa Loompa (Mike)" – The Oompa Loompas
# "End Credits" – "Pure Imagination"

===Soundtrack===
The track listing for the soundtrack is as follows:
# "Main Title" ("Golden Ticket"/"Pure Imagination")
#"The Candy Man"
# "Charlies Paper Run"
#"Cheer Up Charlie"

#"(Ive Got A) Golden Ticket"
#"Pure Imagination"
#"Oompa Loompa"
#"The Wondrous Boat Ride"
#"Everlasting Gobstoppers/Oompa Loompa"
#"The Bubble Machine"
#"I Want It Now/Oompa Loompa"
#"Wonkamobile, Wonkavision/Oompa Loompa"
#"Wonkavator/End Title" ("Pure Imagination")

==References==
 

;Further reading
*Anders, Lou. Golden Tickets to Hell: Willy Wonka – Tour Guide of the Abyss, 2005. http://louanders.blogspot.com/2005/04/golden-tickets-to-hellwilly-wonka-tour.html
*Stuart, Mel, with Josh Young, Pure Imagination: The Making of Willy Wonka and the Chocolate Factory, St. Martins Press, 2002. ISBN 0-312-28777-1, ISBN 0-312-35240-9, http://www.amazon.com/Pure-Imagination-Making-Chocolate-Factory/dp/0312287771
 

==External links==
 
 
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 