This Is My Country (film)
 
{{Infobox film
| name           = This Is My Country
| image          = File:Kapitsapatalim.jpg
| caption        = Film poster
| director       = Lino Brocka
| producer       = Véra Belmont
| writer         = Jose F. Lacaba
| starring       = Phillip Salvador
| music          =
| cinematography = Conrado Baltazar
| editing        = Ike Jarlego Jr.
| distributor    =
| released       =  
| runtime        = 108 minutes
| country        = Philippines
| language       = Filipino
| budget         =
}}
 Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Phillip Salvador - Turing
* Gina Alajar - Luz
* Carmi Martin - Carla
* Claudia Zobel
* Raoul Aragon - Lando
* Paquito Diaz - Hugo
* Rez Cortez - Boy Echas
* Lorli Villanueva - Mrs. Lim
* Ariosto Reyes Jr. - Willie Mona Lisa
* Lucita Soriano
* Venchito Galvez
* Bey Vito
* Nomer Son - Mr. Lim
* Joe Taruc - Himself

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Philippine submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 