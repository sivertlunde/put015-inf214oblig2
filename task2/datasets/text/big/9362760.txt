Love on Delivery
 
 
{{Infobox film
| name           = Love on Delivery
| image          = Love-On-Delivery-poster.jpg
| image size     =
| caption        = Love on Delivery film poster
| director       = Stephen Chow Lee Lik-Chi
| producer       =
| writer         =
| narrator       =
| starring       = Stephen Chow Christy Chung Ng Man Tat   Philip Chan   Joe Cheng
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 3 February 1994
| runtime        = 100 min Hong Kong Mandarin
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 Hong Kong comedy film directed by Stephen Chow and Lee Lik-Chi, based on the Japanese manga Hakaiō Noritaka, starring Stephen Chow, Christy Chung and Ng Man Tat.

==Plot==
Ho-Kam Ang (Stephen Chow), a weak, disadvantaged but kind lunch delivery boy, happens to fall in love with Lily (Christy Chung), the girl of his dreams from a local sports center. However, his dream is crushed after a disastrous date with her, when bullying Judo master Black Bear, who also admires Lily, intervenes. That night, in a brutally straightforward fashion, Lily tells Ang that she dislikes weak and pathetic men.
 kung fu to cure him of his weakness and cowardice, in exchange for money.  However, Tat, a self-professed Sanshou master, is merely a swindler taking advantage of Angs gullibility, and teaches Ang useless, fantasy kung fu techniques.

But to Tats surprise and annoyance, Ang is intent on being a full-time student. When Ang loses his job and runs out of money, he tells Tat he will follow him for life. Tat attempts to rid of him by persuading him to use a false technique called "The Invincible Wind and Fire Spin," a move that would almost certainly kill or at least seriously injure anyone – which involves holding onto the enemy and rolling down a huge flight of stairs, using the enemy to soften all the blows of the stairs. Ang is considering implementing the move, but decides against it. However, he becomes reassured of this so-called technique when he witnesses Tat himself falling down the stairs and surviving it, though that was an accident. Emboldened, Ang thanks Tat and leaves.
 Garfield mask ala superhero. After a scuffle, Ang manages to defeat Black Bear by using the "Spin." The next day, Ang tries to tell Lily that it was he who saved her, but before he can, Lily meets up with Master Lau, a karate champion from Japan who looks down on the weak.  To win Lilys heart, Lau lies to her that he is the "Garfield warrior" who saved her.  Infuriated, Ang plans on challenging Lau to a combat match to prove his mettle. After learning Tat is just a junkie and a liar, he drags him along as punishment.

However, upon arriving at the fitness center with a letter of challenge, Ang and Tat secretly watch Lau in an office with five other people: a   broke his leg in a match at Japan.  Tat has been living in obscurity ever since. To redeem himself, Tat promises to prepare Ang for the match against Lau, but only after receiving one months training.  Also, Lau personally seeks to kill Ang, so in a side bet made by Tat, if Ang can survive all three rounds, Sanshou will regain its public image at the center. An agreement is made.

The upcoming match receives much publicity and reporters follow Ang and Tat, wanting to see how a delivery boy be converted to a rival martial artist.  But, to everyones amusement and puzzlement, Ang and Tat are only seen partying and eating. When asked, Tat replies that this is their training, unnerving some, even Lau.

A month passes, and the match arrives. Lily rushes to the stadium to cancel it, worrying for Angs life. However, she and her friends are stranded in a malfunctioning elevator. At the boxing ring, Ang is voted the odds-on favourite to win by the judges because of his lack of fear, which ironically increases Laus own trepidation. As the match commences with round one, Lau rushes in to attack, but stops abruptly when Ang simply turns around and keeps still. This is in fact Tats strategy: to confuse Lau.  In the second round, Tat instructs Ang to wear down Lau with submissions and sucker punches; all the while, Tat deliberately distracts Lau by juggling and things in the air. A commercial break ends round two.

Frustrated, Lau tries to end the match once and for all.  But Ang surprisingly grapples and locks him throughout the whole third round. In a flashback, Tat tells Ang that to prevent himself from losing the bet and his life, he must execute the "Golden Snake Restraint" defensive technique, hence the grapples and locks, which will prevent Lau from knocking him down. Visibly irate, Lau unsuccessfully tries to throw Ang off. Finally, round three ends, and Lau is announced the winner, though Ang wins the bet since he survives the match. Incensed, Lau starts ravaging the place, beating up even the referee and judges. To stop Lau, Ang decides to use the "Invincible Wind and Fire Spin" on him, using an immense lottery wheel as help.  The two spin wildly inside the wheel, and it explodes. Out of the rubble emerges Ang, exhausted but victorious, and Lau collapses in defeat. Lily, realising Ang is the "Garfield warrior," rushes over to kiss him, and Tat reintroduces Sanshou to the public.

== Trivia ==
*Jacky Cheung appears as a cameo as himself, in which he give the best seat tickets to Ho.
*The introduction scene of Ho is a parody of Terminator (character)|Terminator, in which he walks naked in a street before being taken away by police (as he gave away his clothes to a beggar as an act of kindness).
*Christy Chung couldnt speak a word of Cantonese during the time of filming, all her Cantonese dialogue was dubbed. 
*Laus glasses and haircut parody Clark Kent (Superman).

==Cast and roles==
* Stephen Chow – Ho Kam-Ang
* Christy Chung – Lily
* Ng Man-tat – Tat/Devil Killer
* Philip Chan – Television commercial pitchman
* Joe Cheng – Master Blackbear
* Jacky Cheung – Cameo appearance
* Radium Cheung		
* Billy Chow – Taekwando master Paul Chun – Chan
* Vincent Kok – Niu
* Leo Ku		 Peter Lai – Customer with a fly in his soup Ben Lam – Tuen Shui-Lau
* Lee Lik-Chi	
* Gabriel Wong – Turtle
* Wong Yut Fei – Hos boss

==External links==
*  
*  
*  

 
 
 
 
 
 