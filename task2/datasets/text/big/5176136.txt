Conceiving Ada
 
{{Infobox film
| name     = Conceiving Ada
| image    = Conceiving Ada (film).jpg
| writer   = Lynn Hershman Leeson Eileen Jones Sadie Plant Betty A. Toole John OKeefe John Perry Barlow J.D. Wolfe
| director = Lynn Hershman Leeson
| producer = Lynn Hershman Leeson, Henry S. Rosenthal
| released =  
| runtime  = 85 min. English
| country  = United States
| music    = The Residents
| awards   =
| budget   =
}}
Conceiving Ada is a 1997 movie produced, written, and directed by Lynn Hershman Leeson. Henry S. Rosenthal was co-producer of the movie. The Cinematography by Hiro Narita and Bill Zarchy, the latter being the director of photography.

==Synopsis==
Emmy Coer is a computer scientist obsessed with Countess Ada Lovelace, author of the first computer algorithm, written for Charles Babbages "Analytical Engine". {{cite book
| last = ORegan
| first = Gerard
| year = 2013
| title = Giants of Computing
| publisher = Springer-Verlag
| ISBN = 978-1-4471-5340-5
}}   She finds a way of communicating with people in the past by way of "undying information waves".  In the film, Adas ideas are portrayed as limited by discrimination against women in technology, science and mathematics in her time.  Much of the story revolves around Coers attempts to use genetic engineering to bring Countess Lovelace into the present.

==Cast==
*Tilda Swinton as Ada Augusta Byron King, Countess of Lovelace
*Francesca Faridany as Emmy Coer 
*Timothy Leary as Sims
*Karen Black as Lady Byron/Mother Coer  John OKeefe as  Charles Babbage 
*John Perry Barlow as John Crosse
*J.D. Wolfe as Nicholas Clayton
*Owen Murphy as William Lovelace  David Brooks as Childrens Tutor (David)

==External links==
* 
* 
* 

==References==
 

 
 
 
 
 
 
 

 