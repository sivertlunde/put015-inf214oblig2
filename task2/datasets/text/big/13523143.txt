Desperate Trails
 
{{Infobox film
| name           = Desperate Trails
| image          = Desperate Trails (1921).jpg
| caption        = Film still
| director       = John Ford
| producer       = 
| writer         = Elliott J. Clawson Courtney Ryley Cooper Harry Carey
| cinematography = Robert De Grasse Harry M. Fowler
| editing        =  Universal Film Manufacturing Company
| released       =  
| runtime        = 50 minutes
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 western film Harry Carey. The film is considered to be lost film|lost.   

==Plot==
As described in a film publication,    Bart Carson (Carey) is in love with Lou (La Marr) and even goes to jail to save Walter A. Walker (Coxen), a man she says is her brother but who is really a husband who has deserted his wife and two children. After he learns the truth, Bart breaks out of jail and trails Walter, who falls off a train trying to escape. Bart then seeks refuge in a cabin with Mrs. Walker (Rich),  where he is captured, but the officials have learned the truth and promise him a pardon.

==Cast== Harry Carey as Bart Carson
* Irene Rich as Mrs. Walker
* George Stone as Dannie Boy
* Helen Field as Carrie
* Edward Coxen as Walter A. Walker
* Barbara La Marr as Lady Lou
* George Siegmann as Sheriff Price
* Charles Inslee as Doc Higgins (as Charles E. Insley)

==See also==
* Harry Carey filmography

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 


 
 