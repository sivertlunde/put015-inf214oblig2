The Singer of Naples
{{Infobox film
| name = The Singer of Naples
| image =
| image_size =
| caption =
| director = Howard Bretherton  Moreno Cuyar
| producer = Manuel Reachi
| writer = Arman Chelieu (novel)   Elizabeth Reinhardt   Manuel Reachi 
| narrator =
| starring = Enrico Caruso Jr.   Mona Maris   Carmen Río   Alfonso Pedroza
| music = Bernhard Kaun William Rees
| editing = Frank Magee    
| studio = Warner Brothers
| distributor =   Warner Brothers
| released = 22 February 1935 
| runtime = 77 minutes
| country = United States Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Spanish by the Hollywood studio Warner Brothers. Unlike many other American Spanish language films of the era it was not a remake of an English language film.
 dubbing meant that it was less viable to make separate Spanish films, and in future it became more common for films to be made in a single English version and then dubbed into a variety of other languages for global release.

==Synopsis==
A blacksmiths son from Naples rises to become a celebrated opera singer, performing at La Scala in Milan.

==Cast==
* Enrico Caruso Jr. as Enrico Daspurro  
* Mona Maris as Teresa  
* Carmen Río as Maria  
* Alfonso Pedroza as Fortuni  
* Antonio Vidal as Prof. Rubini  
* Emilia Leovalli as Signora Daspurro 
* Enrique Acosta as Signor Daspurro  
* Francisco Marán as Eduardo  
* Martin Garralaga as Beppo 
* María Calvo as Signora Corelli  
* Rosa Rey as Doña Rosa  
* Chevo Pirrín as Mensajero  
* Terry La Franconi as Cantante 
* Felipe Osta as Carlo

==References==
 

==Bibliography==
* Waldman, Harry. Hollywood and the Foreign Touch: A Dictionary of Foreign Filmmakers and Their Films from America, 1910-1995. Scarecrow Press, 1996.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 