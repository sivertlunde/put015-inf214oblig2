Leonard Cohen: I'm Your Man
{{Infobox Film
| name           = Leonard Cohen: Im Your Man
| image =  Leonard cohen im your man.jpg
| caption        = Theatrical poster
| director       = Lian Lunson
| producer       = Lian Lunson Mel Gibson
| writer         = Lian Lunson
| based on       =  
| starring       = Leonard Cohen Nick Cave Rufus Wainwright Martha Wainwright Bono Perla Batalla
| music          = Leonard Cohen
| cinematography = Geoffrey Hall John Pirozzi Mike Cahill Sundance Channel Horse Pictures Lionsgate
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,398,424 
}}  Linda Thompson, Antony Hegarty|Antony, Kate and Anna McGarrigle, with Cohens former back-up singers Perla Batalla and Julie Christensen as special guests. The end of the film includes a performance by Leonard Cohen and U2, which was not recorded live, but filmed specifically for the film, in New York in May 2005.

The film premiered at the Toronto Film Festival in September 2005, and was released the same month in Canada by Lions Gate films along with the Sundance Channel.  It was subsequently released in various other countries during 2006 and 2007.  The film is distributed by Lions Gate Entertainment. A soundtrack CD is also available from Verve.

The DVD of the film contains extra performances.

==Soundtrack==

The films soundtrack was released by Verve Forecast in July 2006. It included performances mostly recorded at an earlier incarnation of the show in Brighton, May 2004.

# Martha Wainwright, "Tower of Song"
# Teddy Thompson, "Tonight Will Be Fine"
# Nick Cave, "Im Your Man"
# Kate and Anna McGarrigle with Martha Wainwright, "Winter Lady"
# Beth Orton, "Sisters of Mercy"
# Rufus Wainwright, "Chelsea Hotel No. 2"
# Antony Hegarty|Antony, "If It Be Your Will"
# Jarvis Cocker, "I Cant Forget"
# The Handsome Family, "Famous Blue Raincoat"
# Perla Batalla, "Bird on the Wire" Everybody Knows"
# Martha Wainwright, "The Traitor"
# Nick Cave, Perla Batalla and Julie Christensen, "Suzanne (Leonard Cohen song)|Suzanne"
# Teddy Thompson, "The Future"
# Perla Batalla and Julie Christensen, "Anthem"
# Leonard Cohen and U2, "Tower of Song"
# Laurie Anderson, "The Guests" (iTunes Store bonus track)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 