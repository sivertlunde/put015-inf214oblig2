Swimming to Cambodia
{{Infobox film
| name           = Swimming to Cambodia
| image          = spaldinggray-swim.jpg
| director       = Jonathan Demme
| producer       = Renee Shafransky
| writer         = Spalding Gray
| editing        = Carol Littleton John Bailey
| starring       = Spalding Gray Laurie Anderson
| distributor    = Cinecom Pictures
| released       = 1987
| runtime        = 85 min.
| language       = English
| awards         =
| budget         =
}}
 1987 Jonathan The Killing Year Zero and his search for his "perfect moment".  The film grossed slightly over $US1 million.

==Performances==
Swimming to Cambodia was originally a theatre piece on which Gray spent two years working. The original running time of the performance was four hours long and took place over two nights. Swimming to Cambodia won Gray an Obie award.

In 2001, Gray took Swimming to Cambodia back to the stage in Los Angeles, Chicago, San Francisco, California and Albany, New York.

==Film== The Killing Fields.  Interspersed with his own experiences he expounds on the recent history of Cambodia up through the coming to power of the Khmer Rouge and the Cambodian Genocide.  A small scene from The Killing Fields is the only other footage in the movie.
 Laurie Anderson, Strange Angels.
 The Killing Fields.

The monologue was first published in book form two years before the release of the film.

==Home Media==
Shout! Factory announced plans for a DVD release of Swimming to Cambodia on May 28, 2013. 

Swimming to Cambodia was released in the UK on region 2 on the 16th March 2015 by Simply Media

==References==

 

==External links==
 
* 
* 
* 
 
 

 
 
 
 
 
 
 
 