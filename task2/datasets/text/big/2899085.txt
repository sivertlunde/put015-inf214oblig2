The Atomic Space Bug
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Atomic Space Bug
| image          =
| caption        =
| director       = Jonathan M. Parisen
| producer       = Andrew Dellomo C.M. Murphy Jonathan M. Parisen
| writer         = Jonathan M. Parisen
| starring       = Conrad Brooks Jan Weichun Johann Tonnessen Eddie Grayce Jason Colucci
| music          =
| cinematography =
| editing        = Jason Colucci Jonathan M. Parisen
| distributor    = Parivision Entertainment
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = $10,000
}}
The Atomic Space Bug is a 1999 horror film directed by Jonathan M. Parisen and starring Conrad Brooks (Plan 9 From Outer Space). The Atomic Space Bug is Parisens homage to such fifties films as Robot Monster and Plan 9 From Outer Space. The film is about a giant insect-like creature that terrorizes a small town.

==Production==
Filming began on The Atomic Space Bug in the winter of 1999 in the small town of Bainbridge, New York. 
The weather conditions were so harsh that the star of the film, Christopher Englese, was hospitalized twice before the film was completed. Other problems encountered on the set were dodging bullets from the locals who were hunting in nearby woods, and the main actress quitting after just four days of filming, forcing Parisen to put the film on hold for a couple of months until he found a replacement actress. Parisen resumed shooting the film in the summer of 1999, in a wooded area on Staten Island, New York. The summer temperatures of a hundred degrees Fahrenheit (38 °C) and over caused more problems for the cast and crew, mainly for Jason Colucci, who played the role of the Atomic Space Bug. The bug costume was made mostly of foam, with little ventilation. Colucci would have to take many breaks from shooting to avoid passing out. Filming was completed June 1999. The black-and-white film premiered in New York City on July 16, 1999 at Anthology Film Archives.

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 