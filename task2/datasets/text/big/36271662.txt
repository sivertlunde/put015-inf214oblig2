Mon Bole Priya Priya
{{Infobox film
| name           = Mon Bole Priya Priya
| image          = Mon Bole Priya Priya poster.jpg
| alt            = 
| caption        = Mon Bole Priya Priya poster
| director       = Milan Bhowmik
| producer       = Sanjay Kumar Das
| writer         = 
| starring       = Raaj Pamela Tapas Paul Ashish Vidyarthi Rajatava Dutta
| music          = Soumitra Kundu
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}} Bengali film directed by Milan Bhowmik. This is a romantic film. According to director Milan Bhowmik– "It’s a love story on the extremes that true love can push one to."   

==Plot==
Arjun (Raaj) travels to the city to seek admission in a college. Rich girl Priya (Pamela) is gifted a car without brakes by her uncle. Priya runs the car over Arjun and Arjun loses his memory. Priya feels sad for Arjun and romance begins to brew between the two. Ashish Vidyarthi plays the role of a good cop.   

==Cast==
*Raaj as Arjun
*Pamela as Priya
*Tapas Paul
*Ashish Vidyarthi
*Rajatava Dutta
*Rita Koyral
*Subhasish Mukherjee

==Songs==
*Kono Badha – Sushati Aneek & Somchandra
*Tumi Ele Jibone – Aneek & Anwesha
*Ei Je Mati – Aneek & Somchandra
*Mon Bole Priya Priya (Sad) – Aneek
*Mon Bole Priya Priya – Instrumental

==References==
 

 
 
 


 