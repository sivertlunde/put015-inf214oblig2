Black Gold (1962 film)
{{Infobox film
| name           = Black Gold
| image          = 
| image_size     = 
| caption        = 
| director       = Leslie H. Martinson
| writer         = 
| narrator       = 
| starring       = Philip Carey
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = July 21, 1962
| runtime        = 98 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Black Gold is a 1962 adventure film directed by Leslie H. Martinson. It stars Philip Carey and Diane McBain. 

==Cast==
*Philip Carey as Frank McCandless
*Diane McBain as Ann Evans
*James Best as Jericho Larkin
*Fay Spain as Julie
*Claude Akins as Chick Carrington

==References==
 

==External links==
* 

 

 
 
 

 