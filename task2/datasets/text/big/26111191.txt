Everything Will Be Fine
 
{{Infobox film
| name           = Everything Will Be Fine
| image          = Everything Will Be Fine.jpg
| caption        = Theatrical release poster
| director       = Christoffer Boe
| producer       = Tine Grew Pfeiffer
| writer         = Christoffer Boe
| starring       = Jens Albinus Marijana Janković Paprika Steen Nicolas Bro
| music          = 
| cinematography = Manuel Alberto Claro
| editing        = Peter Brandt
| studio         = AlphaVille Pictures Copenhagen
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
}}
Everything Will Be Fine ( ) is a 2010 Danish film directed by Christoffer Boe, who also wrote the screenplay.

==Cast==
*Falk -- Jens Albinus
*Helena -- Marijana Janković
*Siri -- Paprika Steen
*Håkon -- Nicolas Bro
*Karl -- Søren Malling
*Lemmy -- Henning Moritzen

==External links==
* 
* 
* 

 
 
 
 

 