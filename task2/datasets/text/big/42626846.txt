The Tyrant of Padua
{{Infobox film
| name = The Tyrant of Padua
| image =
| image_size =
| caption =
| director = Max Neufeld
| producer =  Max Calandri  
| writer =  Victor Hugo  (play)   Angelo Bianchini DAlberico     Raffaele Saitto    Max Neufeld
| narrator = Carlo Lombardi   Elsa De Giorgi   Alfredo Varelli
| music = Renzo Rossellini  
| cinematography = Giuseppe Caracciolo 
| editing = Eraldo Da Roma      
| studio = Scalera Film
| distributor = Scalera Film 
| released = 28 December 1946 
| runtime = 
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Carlo Lombardi and Elsa De Giorgi.  It is an adaption of the 1835 play Angelo, Tyrant of Padua by Victor Hugo. It is set in Padua in the 1540s.

==Cast==
* Clara Calamai as Tisbe   Carlo Lombardi as Angelo Malipieri  
* Elsa De Giorgi as Caterina Bragadin in Malipieri  
* Alfredo Varelli as Rodolfo degli Ezzelini  
* Nino Pavese as Una spia  
* Giorgio Piamonti as Omodei  
* Erminio Spalla as Un evaso  
* Carlo Micheluzzi as Il padre di Caterina  
* Olga Vittoria Gentilli as La madre di Tisbe  
* Memo Benassi as Cesare, il pittore 
* Andreina Carli as Reginella  
* Cristina Veronesi 

== References ==
 

== Bibliography ==
* Brunetta, Gian Piero. The History of Italian Cinema: A Guide to Italian Film from Its Origins to the Twenty-first Century.  Princeton University Press, 2009.

== External links ==
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 