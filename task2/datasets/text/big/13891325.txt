El Bola
{{Infobox Film
| name           = El Bola
| image          = Elbola.JPG
| image_size     = 
| caption        = 
| director       = Achero Mañas
| producer       = 
| writer         = 
| narrator       = 
| starring       = Juan José Ballesta Pablo Galán Alberto Jiménez Ana Wagener
| music          = Eduardo Arbide
| cinematography =
| editing        =
| distributor    = Axiom Films  
| released       = 20 October 2000   13 December 2002 NYC,  
| runtime        = 88 min
| country        =   Spanish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2000 Spain|Spanish film, directed by Achero Mañas. It won the Goya Award for Best Film at the 15th Goya Awards.  It is available in the United States from Filmmovement.com|Filmmovement.

== Plot ==
Pablo aka "El Bola" ( ) is a twelve-year-old boy who suffers  ).  His violent family situation prevents him from having friends at school until a new kid, Alfredo, arrives at school.  The warm, caring atmosphere in Alfredos family provides a stark contrast to Pablos oppressive situation under his father. Soon, Pablo finds a different reality in his new friends family who teaches him to confront with courage his worst fears.

==Cast==
* Juan José Ballesta as Bola
* Pablo Galán as Alfredo
* Alberto Jiménez as José
* Manuel Morón as Mariano
* Ana Wagener as Laura
* Nieve de Medina as Marisa
* Gloria Muñoz as Aurora
* Javier Lago as Alfonso
* Omar Muñoz as Juan

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 

 
 