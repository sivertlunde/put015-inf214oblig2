Story Kathe
{{Infobox film
| name           = Story Kathe
| image          = 
| caption        = 
| director       = Jagadish K. R.
| producer       = Jagadish K. R.
| writer         = Jagadish K. R.
| screenplay     = Jagadish K. R.
| based on       =  
| starring       = Parvathy Nair Prathap Narayan Thilak Shekar Neha Patel
| narrator       = 
| music          = Vasu Dixit Sathish Babu
| cinematography = Sathish Kumar
| editing        = Sachin
| studio         = Nirvana Films
| distributor    = 
| released       =  
| runtime        = 118 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}

Story Kathe is a 2013 Indian Kannada-language psychological thriller film written and directed by Jagadish K. R.  It stars Parvathy Nair and Prathap Narayan. 

Filming began in 2012 and went through several revisions. It finally opened in 2013 to positive reviews, but earned less than expected at the box office. 

==Plot==
The main story focuses on a medical student, Rajiv (Tilak Shekhar), who believes he has discovered a cure for viruses of any kind. To test this, he tries the vaccine on two people, who both die. Rajiv is arrested for this, and while in prison tells his story to a television reporter Pallavi (Parvathy Nair), who is so enthusiastic about Rajivs research that she agrees to be a test subject for him. 

== Cast ==
* Parvathy Nair as Pallavi
* Thilak Shekar as Rajiv
* Prathap Narayan  
* Neha patel

==Production==
Story Kathe was released in May 2013.

==Soundtrack==
{{Infobox album
| Name        = Story Kathe
| Type        = Soundtrack
| Artist      = Vasu Dixit, Sathish Babu
| Cover       = 2013 Kannada film Story Kathe album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 19 May 2012
| Recorded    =  Feature film soundtrack
| Length      = 25:23
| Label       = Saregama
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Vasu Dixit and Sathish Babu composed the films background score and music for its soundtrack, with its lyrics written by Santhosh Nayak. The soundtrack album consists six tracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 25:23
| lyrics_credits = yes
| title1 = Charuthara Shashi Moreyol
| lyrics1 = Santhosh Nayak
| extra1 = Anuradha Bhat, Santhosh
| length1 = 4:17
| title2 = Badukoke Saithavne
| lyrics2 = Santhosh Nayak
| extra2 = Vasu Dixit
| length2 = 4:55
| title3 = Nadi Nee Yodha
| lyrics3 = Santhosh Nayak
| extra3 = Santhosh, Harsha, Akanksha Badami
| length3 = 3:42
| title4 = Charuthara Shashi Moreyol (Version 2)
| lyrics4 = Santhosh Nayak
| extra4 = Anuradha Bhat, Santhosh
| length4 = 5:05
| title5 = Badukoke Saithavne (Version 2)
| lyrics5 = Santhosh Nayak
| extra5 = Vasu Dixit
| length5 = 3:45
| title6 = Charuthara Shashi Moreyol (Remix)
| lyrics6 = Santhosh Nayak
| extra6 = DJ Saheer Rahman
| length6 = 3:39
}}

==Awards==
;3rd South Indian International Movie Awards
* SIIMA Award for Best Female Debutant – Parvathy Nair

== References ==
 

==External links==
*  

 
 
 
 
 