Ondanondu Kaladalli
 
{{Infobox film
| name           = Ondanondu Kaladalli
| image          = 
| caption        = 
| director       = Girish Karnad
| producer       = G. N. Lakshmipathi K. N. Narayan
| writer         = Krishna Basaruru Girish Karnad
| screenplay     = Krishna Basaruru Girish Karnad
| story          = 
| based on       =  
| starring       = Shankar Nag Sundar Krishna Urs
| narrator       = 
| music          = Bhaskar Chandavarkar
| cinematography = Apurba Kishore Bir
| editing        = P. Bhaktavatsalam
| studio         = L. N. Combines
| distributor    = 
| released       =  
| runtime        = 137 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}} Best Feature Film in Kannada.   
 Kannada cinema. It gained the cult classic status among the Kannada audience. 

==Plot==
Gandugali (Shankar Nag) is a mercenary who comes across a band of soldiers on the trail of a couple of wounded enemy soldiers. He ends up rescuing them and is taken to their chieftain who hires him to train his soldiers to fight his brother — his enemy. While there, Gandugali befriends the son of the chieftains eldest brother, Jayakeshi, who is consigned to tending to cattle by his uncle. Jayakeshi asks Gandugali to help him reclaim his land and reinstate him as the chief. Gandugali refuses saying he is only a mercenary and that the youngster has no money to offer.

Gandugali helps the chieftain earn minor victories against his brother, much to the envy of the chiefs commander. After one battle where Gandugali manages to steal the enemys cattle, on the day of a festival, the commander of the enemy Peramadi (Sundar Krishna Urs) ends up trapped by Gandugali and his men. Gandugali releases him saying that he did not wish to shed any blood on a festival day.

Gandugali returns to his chief with the cattle only to realise that the chief and the commander are set against him for releasing Peramadi. The chief refuses to see him and the commander assigns men to keep an eye on him. That night an attempt is made on Gandugalis life, but the men looking out kill the assassin. Gandugali is furious when he realises that men had been assigned to keep an eye on him. Gandugali also realises that now with Peramadi defeated they dont have anything to fear or any need for him. He collects his salary and leaves in anger.

On the way he is confronted by Peramadi who challenges him to a duel. After a long fight where both end up exhausted, Peramadi tells him that it was his son who was out to kill Gandugali; his chief had humiliated him for losing the cattle and being defeated by Gandugali and believes that Gandugali killed him. Peramadi swears to wipe out the whole family of the chiefs including Jayakeshi. Gandugali swears to hunt him down and kill him wherever he is if anything happens to Jayakeshi. Finally, Peramadi makes a deal with Gandugali to help him destroy the family in return for letting Jayakeshi go.

The next few days they send out a warning and a challenge to the chiefs which unites them and their armies. Slowly, Peramadi and Gandugali isolate the chiefs, first by scaring away half the army and then killing some. On the way to the final decisive battle, Peramadi tells Gandugali that if he died in the battle, he would not hold himself to the promise of sparing Jaykeshi and kill him. Gandugali, knowing that Peramadi was not be trusted, ensures that Jaykeshi is hidden.

In the final battle, they destroy the chiefs armies and Gandugali kills the commander in a duel. Jaykeshi escapes from his hideout to fight, but holes up with some soldiers who hedge their bets on him becoming the next chief. Gandugali, having realised that Jaykeshi has escaped, comes down to the palace looking for him and ends up facing his old chief. He refuses to kill him saying that he was his servant and ate his salt, and his deal with Peramadi was only to help him get to them. The chief, however, attacks him at an opportune moment, fatally injuring him. Peramadi comes in after killing his chief (the younger brother) and kills the older chief. He sounds the bugle to signal the end of the war. Jaykeshi runs down to the palace on hearing it. On seeing Gandugali dead, he breaks down near his corpse. Peramadi comes from behind and raises his sword to kill him, but Jaykeshi escapes after being warned. Peramadi realises that Jaykeshi was crying over Gandugali and, after a change of heart, reinstates him as the chief and walks away.

==Cast==
* Shankar Nag as Gandugali
* Sunder Krishna Urs as Peramadi
* Sundarraj as Commander of Gandugalis army
* Akshata Rao as Savantri
* Sushilendra as Jayakeshi
* Ajit Saldanha as Iraga, Permadis Son
* Rekha Sabnis as Permadis Wife
* Anil Thakkar as Kapardi
* Vasant Rao as Maranayaka

==Soundtrack==
{{Infobox album
| Name        = —
| Type        = Soundtrack
| Artist      = Bhaskar Chandavarkar
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1978
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Saregama
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Ondanondu Kaaladalli
| lyrics1 	= Chandrashekhara Kambara
| extra1        = Kavitha Krishnamurthy
| length1       = 
| title2        = Bicchugatthiya Bhantana
| lyrics2 	= Chandrashekhara Kambara
| extra2        = Chorus
| length2       = 
}}
 

==Notes==
In his intro scene, Gandugali is shown to be sleeping, with a casual attitude in the forests, when he is woken up by some soldiers. The way he casually talks, his lazy demeanour, and his cool attitude, is in Toshiro Mifune or Clint Eastwood style. 

The other mercenary is played by Sunder Krishna Urs, who later became famous for his baddie roles.

==Reception==
One of the finest films ever made, the movie has some fascinating stunt scenes, involving Kalaripayattu. With wide tracking photography, great shots of the Malenadu forests and excellent detailing, Ondanondu Kaladalli is an epic masterpiece.

==Awards==
This movie has won following awards:
*National Film Awards 1978
*Best Kannada Film Karnataka State Film Awards 1978-79
**Best Supporting Actor – Sundar Krishna Urs
**Best Editing – P. Bhakthavathsalam
**Best Sound Recording – S. P. Ramanathan
*Filmfare awards south
*Best Film
*Best Director - Girish Karnad
* Ondanondu Kaladalli screened at the 7th IFFI competition section. Shanker Nag won best actor.
* It screened at 14th IFFI Homage to Shanker Nag.

==References==
 

==External links==
*  

 
 
 
 
 