Die Another Day
 
 
 
{{Infobox film
| name = Die Another Day
| image = Die another Day - UK cinema poster.jpg
| caption = British cinema poster for Die Another Day, designed by Intralink Film Graphic Design
| director = Lee Tamahori
| producer =
{{plainlist |
* Michael G. Wilson
* Barbara Broccoli}}
| writer =
{{plainlist | Neal Purvis Robert Wade}}
| based on =  
| starring =
{{plainlist |
* Pierce Brosnan
* Halle Berry
* Toby Stephens
* Rosamund Pike
* Rick Yune
* John Cleese
* Judi Dench }}
| cinematography = David Tattersall
| music = David Arnold
| editing = Christian Wagner
| studio = Eon Productions
| distributor = Metro-Goldwyn-Mayer 20th Century Fox (UK)
| released =  
| runtime = 133 minutes
| country = United Kingdom

| budget = $142 million
| gross = $431,971,116
}} MI6 agent James Bond. The film follows Bond as he leads a mission to North Korea, during which he is betrayed and, after seemingly killing a rogue North Korean Sangjwa|colonel, is captured and imprisoned. More than a year later Bond is released as part of a prisoner exchange. Surmising that someone within the British government betrayed him, he attempts to earn redemption by tracking down his betrayer and killing a North Korean agent he believes was involved in his torture.

Die Another Day, produced by Michael G. Wilson and Barbara Broccoli, and directed by Lee Tamahori, marks the James Bond franchises 40th anniversary. The series began in 1962 with Sean Connery starring as Bond in Dr. No (film)|Dr. No. Die Another Day includes references to each of the preceding films. 

The film received mixed reviews. Some critics praised the work of Lee Tamahori, while others criticised the films heavy use of computer-generated imagery, which they found unconvincing and a distraction from the films plot. Nevertheless, Die Another Day was the highest-grossing James Bond film up to that time if inflation is not taken into account.

==Plot== conflict diamonds. After Moons assistant Zao discovers that Bond is a British agent, the colonel attempts to kill Bond and a hovercraft chase ensues, which ends with Moons apparent death. Bond survives, but is captured by North Korean soldiers and imprisoned by the Colonels father, General Moon.

After 14 months of captivity and torture, Bond is traded for Zao in a prisoner exchange. He is sedated and taken to meet M (James Bond)|M, who informs him that his status as a 00 Agent is suspended under suspicion of having leaked information under duress. Bond is convinced that he has been set up by a double agent in the British government and decides to avenge his betrayal. After escaping from the custody of MI6, he travels to Hong Kong, where he learns from a Chinese agent that Zao is in Cuba.
 NSA agent Giacinta Jinx Johnson and follows her to a gene therapy clinic, where patients can have their appearances altered through DNA restructuring. Bond locates Zao inside the clinic and attempts to kill him, but Zao escapes. He leaves behind a pendant which leads Bond to a cache of diamonds, identified as conflict diamonds, but bearing the crest of the company owned by British billionaire Gustav Graves.
 Miranda Frost, who is also an undercover MI6 agent. After a fencing exercise, Bond is invited by Graves to Iceland for a scientific demonstration. Shortly afterwards, M restores Bonds Double-0 status and offers assistance in the investigation.

At his ice palace in Iceland Graves unveils a new orbital mirror satellite, "Icarus", which is able to focus solar energy on a small area and provide year-round sunshine for crop development. During the night, Jinx infiltrates Graves command centre, but is captured by Zao. Bond rescues her and later discovers that Colonel Moon is still alive. Moon has used the gene therapy technology to change his appearance, assuming the identity of Gustav Graves.

Bond confronts Graves, but Frost arrives to reveal herself as the traitor who betrayed Bond in North Korea, forcing 007 to escape from Graves facility. Bond then returns in his Aston Martin Vanquish to rescue Jinx, who has been captured once again. Zao pursues him in his own vehicle, both cars driving inside the rapidly melting ice palace. Bond kills Zao by shooting an ice chandelier onto him, and then revives Jinx after she has drowned.

Bond and Jinx pursue Graves and Frost to the   with concentrated sunlight, allowing North Korean troops to invade South Korea and reunite the countries by force. Horrified, General Moon tries to stop the plan, but he is murdered by his own son.

Bond attempts to shoot Graves but he is prevented by one of the soldiers on board. In their struggle, a gunshot pierces the fuselage, causing the plane to descend rapidly. Bond engages Graves in a fist fight, and Jinx attempts to regain control of the plane. Frost attacks Jinx, forcing her to defend herself in a sword duel. After the plane passes through the Icarus beam and is further damaged, Jinx kills Frost. Graves attempts to escape by parachute, but Bond opens the parachute, causing Graves to be pulled out of the plane and into one of its engines, killing him and disabling the Icarus beam. Bond and Jinx then escape from the disintegrating plane in a helicopter from the cargo hold, carrying away Graves stash of diamonds in the process.

==Cast== James Bond MI6 agent. NSA agent.  
* Toby Stephens as Gustav Graves, a British entrepreneur, alter ego of Colonel Moon.
* Rosamund Pike as Miranda Frost, undercover MI6 agent and double agent.
* Rick Yune as List of James Bond henchmen#Zao|Zao, a North Korean terrorist, formerly working for Moon.
* Judi Dench as M (James Bond)|M, the head of MI6.
* Will Yun Lee as Colonel Moon, a rogue North Korean army colonel, later uses his alter ego.
* Kenneth Tsang as General Moon, Colonel Moons father.
* John Cleese as Q (James Bond character)|Q, MI6s quartermaster and armourer. Charles Robinson, one of Ms ranking MI6 staff.
* Ho Yi as the Hotel manager and Chinese special agent Mr Chang. In early drafts of the script, it was Wai Lin (Michelle Yeoh) who aided Bond in Hong Kong, but the idea fell through and Chang was created to replace her. 
* Rachel Grant as Peaceful Fountains of Desire, a Chinese agent working for Mr. Chang, undercover as a masseuse.
* Emilio Echevarría as Raoul, the manager of a Havana cigar factory, and a British sleeper (espionage)|sleeper.
* Samantha Bond as Miss Moneypenny, Ms secretary.
* Michael Gorevoy as Vladimir Popov, Gustav Graves personal scientist
* Lawrence Makoare as Mr. Kil, one of Gustav Graves henchmen.
* Michael Madsen as Damian Falco, Jinxs superior in the NSA.
* Joaquin Martinez as an elderly cigar factory worker

==Production==

===Filming===
 , or Jaws, off the north coast of Maui in December 2001]]
 007 Stage and Maui, Hawaii, in December 2001. Pe ahi, Maui,  while the shore shots were taken near Cádiz and Newquay|Newquay, Cornwall. Scenes inside Graves diamond mine were also filmed in Cornwall, at the Eden Project. The scenes involving the Cuban locations of Havana and the fictional Isla Los Organos were filmed at La Caleta, Spain.   

The scenes featuring Berry in a bikini were shot in Cádiz. The location was reportedly cold and windy, and footage has been released of Berry wrapped in thick towels between takes to avoid catching a chill.  Berry was injured during filming when debris from a smoke grenade flew into her eye. The debris was removed in a 30-minute operation. 
 From Russia Jostedalsbreen National RAF Little blue screen. The waves, along with all the glaciers in the scene are computer-generated. 

 ]] Chinook helicopters, was filmed at RAF Odiham in Hampshire, UK, as were the helicopter interior shots during the Switchblade sequence. These latter scenes, though portrayed in the air, were actually filmed entirely on the ground with the sky background being added in post-production using blue screen techniques. Although the base is portrayed in the film as a U.S. base, all the aircraft and personnel in the scene are British in real life. In the film, Switchblades (one-person gliders resembling fighter jets in shape) are flown by Bond and Jinx to stealthily enter North Korea. The Switchblade was based on a workable model called "PHASST" (Programmable High Altitude Single Soldier Transport). Kinetic Aerospace Inc.s lead designer, Jack McCornack was impressed by director Lee Tamahoris way of conducting the Switchblade scene and commented, "Its brief, but realistic. The good guys get in unobserved, thanks to a fast cruise, good glide performance, and minimal radar signature. Its a wonderful promotion for the PHASST." 

===Music===
 
The soundtrack was composed by David Arnold and released on Warner Bros. Records.  He again made use of electronic rhythm elements in his score, and included two of the new themes created for The World Is Not Enough. The first, originally used as Renards theme, is heard during the mammoth "Antonov" cue on the recording, and is written for piano. The second new theme, used in the "Christmas in Turkey" track of The World Is Not Enough, is reused in the "Going Down Together" track. 
 title song cameo in Golden Raspberry Worst Original Song of 2002 (while Madonna herself won the Golden Raspberry Award for Worst Supporting Actress for her cameo). In a MORI poll for the Channel 4 programme "James Bonds Greatest Hits", the song was voted 9th out of 22, and also came in as an "overwhelming number one" favourite among those under the age of 24. 

==Marketing tie-ins==
MGM and Eon Productions granted Mattel the license to sell a line of Barbie dolls based around the franchise. Mattel announced that the Bond Barbies will be at her "stylish best", clad in evening dress and red shawl. Lindy Hemming created the dress, which is slashed to the thigh to reveal a telephone strapped to Barbies leg. The doll was sold in a gift set, with Barbies boyfriend Ken posing as Bond in a tuxedo designed by the Italian fashion house Brioni (fashion)|Brioni. 

Revlon also collaborated with the makers of Die Another Day to create a cosmetics line based around the character Jinx. The limited edition 007 Colour Collection was launched on 7 November 2002 to coincide with the films release. The product names were loaded with puns and innuendo, with shades and textures ranging from the "warm" to "cool and frosted". 

  and a Jaguar XKR as well as track. Corgi Toys|Corgi, a British toy car manufacturer, released 1:30 scale replicas of the Vanquish and Jaguar XKR. 

==Release and reception== Queen Elizabeth Prince Philip You Only Live Twice in 1967.  The Royal Albert Hall had a make-over for the screening and had been transformed into an ice palace. Proceeds from the première, about £500,000, were donated to the Cinema and Television Benevolent Fund of which the Queen is patron.  On the first day, ticket sales reached £1.2 million.  Die Another Day was the highest grossing James Bond film until the release of Casino Royale. It earned $432 million worldwide, becoming the sixth highest grossing film of 2002. 
 Jogye Buddhist Order issued a statement that the film was "disrespectful to our religion and does not reflect our values and ethics". The Washington Post reported growing resentment in the nation towards the United States. An official of the South Korean Ministry of Culture and Tourism said that Die Another Day was "the wrong film at the wrong time." 
 Casino Royale in 2006. 
 The Spy Who Loved Me.  Kyle Bell of Movie Freaks 365 stated in his review that the "first half of Die Another Day is classic Bond", but that "Things start to go downhill when the ice palace gets introduced." 
 the first Invisible cars and dodgy CGI footage? Please!" 

==Novelization== adventures as Devil May Care by Sebastian Faulks in 2008 to mark the 100th anniversary of Flemings birth. 

==Cancelled spin-off== Casino Royale. 

==See also==
* Invisibility in fiction
* Outline of James Bond
 

==References==
 

==External links==
 
 
*  
*  
*  
*  
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 