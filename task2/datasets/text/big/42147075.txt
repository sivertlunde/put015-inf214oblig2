Panivizhum Malarvanam
{{Infobox film
| name           = Panivizhum Malarvanam
| image          = Panivizhum malarvanam.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = James David
| producer       = Maaruthy Nanthan Aravainth
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Abhilash  Sanyathara Varsha Ashwathi
| music          = B. R. Rajin
| cinematography = N. Raagav
| editing        = Ravi Shankar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Tamil
| budget         = 
| gross          = 
}} Tamil Thriller thriller film written and directed by James David.  The film stars Abhilash and Sanyathara in the lead roles, while Varsha Ashwathi, Sai Vishal and Jawahar appear in other pivotal roles. The films music is by music director B. R. Rajin. The film was released on 21 February 2014 to average reviews.

==Cast==
*Abhilash as Tarun
*Sanyathara as Kavya
*Varsha Ashwathi as Malar 
*Sai Vishal as Viju
*Jawahar

==Production==
The film was written and directed by newcomer James David, who had previously assisted director Aabhavanan and was an associate director for films such as Nee Venunda Chellam, Rameswaram (film)|Rameswaram and the 3D animation movie Inimey Nangathan. The lead actor Abhilash also made his debut in a lead role, having previously portrayed a supporting role in Gautham Menons Neethane En Ponvasantham (2012). The film began shoot in August 2012 and was filmed in locations across Kumily and Thekkady in Kerala. The team also claimed to have selected the tiger which had appeared in Hangover 3 for the venture, while collaborating with the brands Incredible India and Save the Tiger.  An audio release function was held in May 2013 with Balu Mahendra and Vairamuthu attending as chief guests. 

==Release==
The film opened on 21 February 2014 to average reviews, with a critic noting "to cut a long story short, while the messages Davids film seeks to convey are significant, the manner in which they have been packaged isnt."  Another critic noted it was a "fairly engaging thriller", writing that "the narration may seem a tad loosely etched and jumpy at times and has its glitches. But it’s a debutant director’s sincere effort to move away from the beaten path and is refreshingly different." 

==Music==
{{Infobox album Name = Panivizhum Malarvanam Type = Soundtrack
|Artist= B. R. Rajin Cover =  Caption = Front cover Released = 11 May 2013 Genre = Feature film soundtrack Length =  Language = Tamil
|Label =  Producer = B. R. Rajin Last album = Ninaivugal Azhivadhillai (2012) This album = Panivizhum Malaravanam (2013) Next album =  Reviews =
}}
The films audio released in May 2013 and the songs have lyrics written by Vairamuthu and Ravi Indrahan. The films title is taken from a track in Ilaiyaraajas 1982 musical Ninaivellam Nithya.

{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    =

| title1          = Malaiyil Ula
| extra1          = Solar Sai
| length1         = 
| title2          = Thoorathila Aagayam
| extra2          = Belly Raj, Chinmayi
| length2         = 
| title3          = Ava Ennannamo
| extra3          = Haricharan
| length3         = 
| title4          = Amma Amma Amma
| extra4          = Madhu Balakrishnan
| length4         = 
| title5          = Kaadu
| extra5          = Instrumental
| length5         = 
}}



==References==
 

==External links==
*  

 
 
 
 
 