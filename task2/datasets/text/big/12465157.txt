Three Faces West
 
 
{{Infobox film
| name           = Three Faces West
| image          = Three Faces West 1940.jpg
| caption        = DVD cover
| director       = Bernard Vorhaus
| producer       = Sol C. Siegel
| writer         = F. Hugh Herbert Joseph Moncure March Samuel Ornitz
| starring       = John Wayne Sigrid Gurie Charles Coburn
| music          = Victor Young
| cinematography = John Alton
| editing        = William Morgan
| distributor    = Republic Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States 
| language       = English
| budget         = 
}}
Three Faces West is a 1940 American drama film starring John Wayne, Sigrid Gurie and Charles Coburn.

==Plot==
Two refugees, a medical doctor and his 20-something-year-old daughter arrive in the USA from Anschluss|Nazi-annexed Austria end up in becoming the much-needed physician and nurse in a small North Dakota farm town. The local town located in the area known as the Dust Bowl and is being hard hit by the drought and dust storms. The local farmers and townspeople want to try to save their farms and the town by adopting newer farming methods, but are eventually convinced by the Department of Agriculture, and the continuing dust storms to pack up the whole town and move en masse to an undeveloped portion of Oregon, where a new dam will create a water supply for them to build a new farming community.

In a modern-day version of an old wagon train, the town moves to Oregon under John Phillipss leadership, not without differences of opinion and friction among the followers. The doctor and his daughter take a detour to San Francisco when they learn that the daughters fiance was not killed by the Nazis in Austria, but has come to America. It turns out that the fiance has embraced Nazism, which sends the doctor and his daughter back to rejoin the transplanted town in Oregon, where the daughter marries Phillips.

==Cast==
* John Wayne as John Phillips
* Sigrid Gurie as Leni "Lenchen" Braun
* Charles Coburn as Dr. Karl Braun
* Spencer Charters as Dr. "Nunk" Atterbury
* Helen MacKellar as Mrs. Welles
* Roland Varno as Dr. Eric Von Scherer
* Sonny Bupp as Billy Welles
* Wade Boteler as Mr. Harris, Department of Agriculture Official
* Trevor Bardette as Clem Higgins Russell Simpson as Minister
* Charles Waldron as Dr. William Thorpe
* Wendell Niles as Man-on-the-Street Radio Announcer

==See also==
* John Wayne filmography

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 