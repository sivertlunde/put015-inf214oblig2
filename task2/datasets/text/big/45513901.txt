Gun & Goal
 

{{Infobox film
| name           =  Gun & Goal
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Simranjit Singh Hundal
| producer       = Surinderjeet Singh Sarao 
| writer         = Surmeet Mavi
| starring       = Sumeet Sarao Rishita Monga Sezal Sharma, Razia Sukhbir, Sandeepa Virk, Guggu Gill, Mukesh Tiwari , Surinder Shinda, Sardar Sohi, Sanjeev Attri, Rajveer Bawa
| music          = Jaggi Singh, Onkar Singh, Vivek Kar
| cinematography = 
| editing        = 
| studio         = Sarao Films
| distributor    =  VIP Films & Entertainment & Studio 7 Production
| released     =  
| runtime        = 
| country        = India
| language       = Punjabi
| budget         = 
| gross          =
}}
Gun & Goal is an upcoming Punjabi sports drama film directed by Simranjit Singh Hundal and starring Sumeet Singh Sarao and Rishita Monga.  The movie is based on the life of a football player. 

==Cast==

* Sumeet Singh Sarao 
* Rishita Monga 
* Sezal Sharma 
* Sandeepa Virk 
* Razia Sukhbir
* Mukesh Tiwari 
* Guggu Gill 
* Sardar Sohi 
* Surinder Shinda 
* Sanjeev Attri
== Soundtrack ==
{{Infobox album
| Name = Gun & Goal
| Type = Soundtrack
| Artist = 
| Cover = 
| Released =   Feature film soundtrack
| Length =  
| Label = Speed Records
}}
The soundtrack is composed by Jaggi Singh, Onkar Singh, Vivek Kar, Santokh Singh
{{Track listing
| lyrics_credits = yes
| music_credits = yes
| extra_column = Singer(s)
| total_length = 17:10
| title1 = zig zag 
| lyrics1= Kumaar 
| music1= vivek kar
| extra1 = meet brothers, shipra goyal
| length1 = 3:17
| title2 = ghatta ghatt kar ke 
| lyrics2= rajveer bawa
| music2=  Jaggi Singh
| extra2 = sonu kakkar, Jaggi Singh
| length2 = 4:13
| title3 = bahan goriyan 
| lyrics3= santokh singh
| music3=  santokh singh
| extra3 = santokh singh and neha kakkar
| length3 = 2:44
| title4 = mehboob 
| lyrics4= simranjit singh hundal
| music4=  Onkar Singh
| extra4 = sahid malya sarodee and sarodee borah
| length4  = 4:25
| title5 = sukke patte
| lyrics5= rajveer bawa
| music5=  onkar singh
| extra5 =  mohd. irfan
| length5 = 3:11
}}


==References==
 

==External links==

*  
* 
* 
* 
* 


 
 
 
 
 
 


 