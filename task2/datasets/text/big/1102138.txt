Monsieur N.
{{Infobox film 
 | name = Monsieur N.
 | image = Monsieur N. movie.jpg
 | caption = Monsieur N. film poster
 | director = Antoine de Caunes 
 | writer = René Manzor
 | starring = Philippe Torreton Richard E. Grant Jay Rodan 
 | producer = Pierre Kubel 
 | distributor = Empire Pictures
 | budget = €15,920,000
 | released = 12 February 2003 ( France ) 
 | runtime = 120 min.
 | country = France  United Kingdom Corsican
 | 
}} 2003 Cinema French Film|movie directed by British on St Helena. Napoleon retained a loyal entourage of officers who helped him plot his escape, and evaded the attentions of Sir Hudson Lowe (Richard E. Grant), the islands overzealous Governor.

The film suggests that Napoleon could have escaped to Louisiana, where he died, and that the body exhumed and now at Les Invalides is that of Napoleons officer Cipriani. The movie also suggests that Napoleon and his young new English wife could have attended the ceremony of "Napoleons" burial in the Invalides.

== Reception ==
The film was well received and has a 70% "fresh" rating on film critic aggregate site Rotten Tomatoes. 

The film received a positive but guarded review in The New York Times, which praised Philippe Torretons performance but thought the narrative too complex for an audience not initiated in Napoleons history. 

==External links==
*   
* 

== References ==
 

 
 
 
 
 
 
 
 
 
 
 

 
 