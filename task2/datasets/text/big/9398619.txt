The Lady Iron Chef
 
 
{{Infobox film
| name           = The Lady Iron Chef
| image          = The Lady Iron Chef DVD.jpg
| caption        = The Lady Iron Chef DVD cover
| director       = Chung Siu Hung
| producer       = 
| writer         = 
| starring       = Charmaine Sheh   Hacken Lee   Yuen Qiu   Wong Jing
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}
The Lady Iron Chef (美女食神) is a 2007 Hong Kong Chinese New Year film produced by Wong Jing and directed by Chung Siu-Hung.

== Cast ==
* Charmaine Sheh as Ceci
* Hacken Lee as S.K To a.k.a. SK2
* Yuen Qiu as Lady Green
* Wong Jing as Souza
* Yumiko Cheng as May
* Cheung Tat-ming as H.O To a.k.a. H2O
* Lau Yeung as Jade
* Alice Chan		
* Gao Lu		
* Lam Chi-chung		
* Tats Lau		
* Lee Kin-Yan		
* Lee Lik-Chi		
* Zuki Lee		
* Winnie Leung		 Liu Yang - Mai Yuk-ying
* Ng Chi Hung	
* Gill Mohindepaul Singh		
* Patrick Tang		
* Tin Kai Man		
* Wang Tian-lin		
* Wong Man-Wai - S.K.Tos mom
* Wong Yut Fei		

Genre:
   Movie, Comedy  

==Synopsis==

On the day of the Supreme Dim Sum competition, Madame To and her son SK To (Hacken) were appointed judges, since they owned one of the largest catering corporations in HK. All three of the contestants failed, but one mystery contestant impresses. The winner was Jade (Lau Yeung) and Madame To wanted her son to marry Jade. Ceci (Charmaine), a lower class girl employed to introduce the chefs onto the stage, was much impressed by SK more than Jade. Though SK was impressed with Jade, he refused to marry her.

SK was kidnapped one night and was held to ransom. However, luck would have it that the kidnappers imprisoned him in a small room on the rooftop of the building where Ceci lived with her father, Souza (Wong Jing). Ceci saved SK, and hid him in her home. Ceci took SK on a tour of Mongkok where SK did many things his mother never permitted him to do. He found Cecis carefree attitude to life refreshing and fell in love with her.

SK took Ceci home to see his mother, who insisted that she would only consent to Ceci marrying SK if she cooked as well as Jade. Souza took her to Macau to learn the art of cooking from his ex-girlfriend Lady Green (Yuen Qiu) who was a descendant of another school of royal chef and an expert in using ordinary food to make a superb meal.

Ceci worked hard, and learned everything Lady Green taught her within a few months. She was confident enough to challenge Jade. It was not only a personal duel between Jade and Ceci, but also a contest between two schools of cooking, the North versus the South. During the last round, Ceci gave Jade a loaf of bread, by means of which Jade make the "Emperor toast" which won applause from all the judges. Ceci was given some noodles, by means of which she made a plate of "Brokeback Noodles" (possibly a parody of brokeback mountain). Ceci wins, due to Jade. Synopsis credit to * 

==External links==
*  
*  

 
 
 