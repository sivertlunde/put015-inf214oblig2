Coyote Ugly (film)
{{Infobox film
| name           = Coyote Ugly
| image          = Coyote ugly poster.jpg
| caption        = Theatrical release poster David McNally
| writer         = Gina Wendkos Todd Graff Kevin Smith  
| starring       = Piper Perabo Adam Garcia Maria Bello Izabella Miko Tyra Banks John Goodman
| producer       = Jerry Bruckheimer Chad Oman
| music          = Trevor Horn
| cinematography = Amir M. Mokri
| editing        = William Goldenberg
| studio         = Touchstone Pictures Jerry Bruckheimer Films Buena Vista Pictures
| released       =  
| runtime        = 100 minutes 107 minutes  (Directors Cut) 
| country        = United States
| language       = English
| budget         = $45 million
| gross          = $114 million
}} drama based David McNally, produced by Jerry Bruckheimer and Chad Oman and written by Gina Wendkos.

==Plot==
Violet Sanford (Piper Perabo) leaves her hometown of South Amboy, New Jersey, her father Bill (John Goodman), and best friend Gloria (Melanie Lynskey) to pursue her dreams of becoming a songwriter in nearby New York City. The pizza shop where she works has a wall covered with the autographs of employees that left, hoping to make it big, and Violet adds hers to the wall.

Violet tries unsuccessfully, a lot of times, to get her demo tape noticed by the recording studios. One night, she tries to get herself noticed by a music industry scout. The bartender jokingly points out Kevin ODonnell (Adam Garcia), making her believe that he is the bar owner. When the joke is discovered, Violet feels that Kevin was making a fool out of her. With only a few dollars left in her pocket after her apartment is robbed, she goes to an all-night diner and notices three girls, Cammie (Izabella Miko), Rachel (Bridget Moynahan), and Zoe (Tyra Banks), flaunting the hundreds of dollars in tips they earned. After inquiring, she finds out that they work at a trendy bar named Coyote Ugly.

She finds her way to the bar and convinces the bar owner Lil (Maria Bello) to give her an audition. Violets first audition does not go well; but after breaking up a fight between two customers, Lil agrees to give her a second audition. At her second audition, Violet douses the fire warden in water costing Lil $250. In order to keep her job, she has to make up $250 in one night. Kevin turns up at the bar, but ends up being auctioned off to one of the women in the bar. In order to pay off her debt to Kevin, Violet agrees to go on four dates with him. The two begin a relationship.

Kevin commits himself to helping Violet overcome her stage fright so that she can sell her music. It is discovered that Violets stage fright is inherited from her mother who also came to New York to be a singer. It is also discovered that Violet can sing to songs that arent her own. We learn this when she sings on the bar in the bar in order to save the necks of Cammie and Rachel who were trying to break up a fight between the customers.

One night a patron takes a picture of Violet in the middle of a raunchy move and with water pouring on her. When the picture appears in the paper, Violets father happens to see it and gets angry at her. She continues to pursue her dream, though, despite being sidetracked with work at the bar. She gets fired when Kevin gets into a fight at the bar. She returns to New Jersey for her best friends wedding and makes her way back to New York when her dad was in a car accident.

Later she performs at an open mic night at the Bowery Ballroom with the Coyotes from the Coyote Ugly saloon, her father, her best friend, and Kevin all there for moral support. The performance goes over very well and she finally lands a deal with a record label.

The film concludes back at Coyote Ugly with LeAnn Rimes, having recorded Violets song, singing on the bar as Violet joins in.

==Cast==
 
* Piper Perabo as Violet Sanford
* Adam Garcia as Kevin ODonnell
* John Goodman as William "Bill" Sanford Lil
* Izabella Miko as Cammie
* Tyra Banks as Zoe
* Bridget Moynahan as Rachel
* Melanie Lynskey as Gloria
* Del Pentecost as Lou
* Michael Weston as Danny
* Melody Perkins as New Coyote
* LeAnn Rimes as Herself
* Johnny Knoxville as College Guy
* John Fugelsang as Richie the Booker
* Michael Bay makes a cameo as a photographer
* Susan Yeagley as bidding auction woman (uncredited)
* Kaitlin Olson as bidding auction woman (uncredited)
* Alex Borstein as bidding auction woman (uncredited)
* Carol Ann Susi as bidding auction woman (uncredited)
 

==Title==
The film was based on an article, "The Muse of the Coyote Ugly Saloon", in GQ by Elizabeth Gilbert,  who worked as a bartender in the East Village.  The bar which opened in 1993 quickly became a favorite of the Lower East Side hipsters.

As mentioned in the movie, the slang term "coyote ugly" refers to the feeling of waking up after a one-night stand, and discovering that your arm is underneath someone who is so physically repulsive that you would gladly chew it off without waking the person just so you can get away without being discovered. Coyotes are known to gnaw off limbs if they are stuck in a trap in order to facilitate escape.

==Production==
Kevin Smith, who did an uncredited rewrite of the script, stated that a total of eight writers worked on the script while the Writers Guild of America only gave credit to Gina Wendkos, who wrote the first draft of the script, which, according to Smith, scarcely resembles the final film.  (See WGA screenwriting credit system.)
 pop singer Jessica Simpson, who turned it down. 
 South Amboy Sea Bright West Hollywood, Pasadena and San Pedro. 

==Reception==
Coyote Ugly received mixed reviews by critics. Criticisms and praise centered around the belief that it was little more than an excuse to portray "hot, sexy women dancing on a bar in a wet T-shirt contest."  It currently holds a 22% rating on Rotten Tomatoes based on 96 reviews.   The film also holds a 27% rating on Metacritic, indicating "generally unfavorable reviews".   VH1 made a statement about Rimes appearance in the film stating, "Rimes  , who is only 17 years old, was sporting leather pants and a skimpy top and in all likelihood, even with a fake ID, would never have been allowed inside any NYC bar." 

==Unrated edition==
In the summer of 2005, an unrated special edition of the film (the original release was rated PG-13 and the directors cut rated R) was released on DVD. The extended cut adds approximately six minutes to the films runtime, most of which consists of additional shots of the "coyotes" dancing on the bar and of Violet and Cammie (Izabella Miko) trying on different outfits while shopping. Arguably, the most notable additions are the extension of the sex scene between Violet and Kevin (Piper Perabo used a body double for most of the scene), and the inclusion of an additional scene which shows the "Coyotes" winning a softball game because Cammie distracts the pitcher by Striptease|stripping. (The special features of the extended cut DVD are identical with those of the previous DVD release.)

==Box office==
The film opened at No. 4 at the North American box office making   and Hollow Man. It went on to gross $60.7 million domestically and $53.2 million around the world to a total of $113.9 million worldwide, becoming a box office success.

==Soundtracks== gold status within one month    of its release on August 1, 2000  and platinum status on November 7, 2000.  On April 18, 2001 the soundtrack was certified 2x Platinum and on January 9, 2002 it was certified 3x Platinum. The soundtrack was certified 4x Platinum on July 22, 2008,  was certified 5x Platinum (500,000 units) in Canada  and gold (100,000 units) in Japan in 2002.   
 singles were The Billboard Hot 100,  "But I Do Love You" and "The Right Kind of Wrong". 

A second soundtrack, More Music from Coyote Ugly, with more songs that appeared in the film and remixes of two of Rimes songs, followed on January 28, 2003. 

Although Piper Perabo was able to sing for her character, it was decided that LeAnn Rimes would provide Violets singing voice, due to her soprano type voice, far better for the role. This means that during LeAnns cameo in the movie, she is effectively duetting with herself.

===Coyote Ugly soundtrack===
{{Infobox album
| Name        = Coyote Ugly
| Type        = soundtrack
| Artist      = Various Artists 300px
| Released    = August 1, 2000
| Length      = 44:27 Pop
| Curb
| Producer    = Jerry Bruckheimer, Kathy Nelson, Mike Curb, Trevor Horn, Don Henley, Danny Kortchmar, Greg Ladayi, Ralph Jezzard, Snap!, John Boylan, Micheal Lloyd, Don Cook, Chris Waters, Brad Gilderman, Harvey Mason, Jr.
| Misc        = {{Singles
 | Name          = Coyote Ugly
 | Type          = soundtrack
 | Single 1      = Cant Fight the Moonlight
 | Single 1 date =  
 | Single 2      = But I Do Love You
 | Single 2 date =   
 | Single 3      = The Right Kind of Wrong
 | Single 3 date =     
}}
}}
{{Album ratings
| rev1 = Allmusic 
| rev1Score =   
| rev2 = Entertainment Weekly
| rev2Score = D 
}}

====Track listing====
{{tracklist
| writing_credits = no
| extra_column    = Recording artist(s)
| total_length    = 44:27
| title1          = Cant Fight the Moonlight string arrangements David Campbell
| extra1          = LeAnn Rimes
| length1         = 3:35
| title2          = Please Remember
| extra2          = LeAnn Rimes
| length2         = 4:34
| title3          = The Right Kind of Wrong
| note3           = feat. string arrangements by David Campbell
| extra3          = LeAnn Rimes
| length3         = 3:47
| title4          = But I Do Love You
| note4           = feat. string arrangements by David Campbell
| extra4          = LeAnn Rimes
| length4         = 3:21
| title5          = All She Wants to Do Is Dance
| extra5          = Don Henley
| length5         = 4:30 Unbelievable
| EMF
| length6         = 3:30 The Power
| extra7          = Snap!
| length7         = 3:40
| title8          = Need You Tonight
| extra8          = INXS
| length8         = 3:10
| title9          = The Devil Went Down to Georgia
| extra9          = The Charlie Daniels Band
| length9         = 3:36
| title10         = Boom Boom Boom
| extra10         = Rare Blend
| length10        = 3:22
| title11         = Didnt We Love
| extra11         = Tamara Walker
| length11        = 3:24
| title12         = We Can Get There
| note12          = TP2K Hot Radio Mix
| extra12         = Mary Griffin
| length12        = 3:59
}}

====Chart performance====
{| class="wikitable sortable"
|-
! Chart (2000/01)
! Peak position
|-
| Australian Albums Chart   
| style="text-align:center;"| 1
|-
| Austrian Albums Chart 
| style="text-align:center;"| 2
|-
| Belgian (Flanders) Albums Chart 
| style="text-align:center;"| 7
|-
| Belgian (Wallonia) Albums Chart 
| style="text-align:center;"| 43
|-
| Canadian RPM Country Albums
| style="text-align:center;"| 1
|-
| Canadian Albums Chart
| style="text-align:center;"| 4
|-
| Danish Albums Chart 
| style="text-align:center;"| 6
|-
| Dutch Albums Chart 
| style="text-align:center;"| 60
|-
| Finnish Albums Chart 
| style="text-align:center;"| 9
|-
| French Albums Chart 
| style="text-align:center;"| 117
|-
| Norwegian Albums Chart 
| style="text-align:center;"| 3
|-
| Spanish Albums Chart 
| style="text-align:center;"| 47
|-
| Swedish Albums Chart 
| style="text-align:center;"| 29
|-
| Swiss Albums Chart 
| style="text-align:center;"| 9
|-
| US Billboard 200
| style="text-align:center;"| 9
|-
| US Billboard Top Country Albums
| style="text-align:center;"| 1
|-
| US Billboard Soundtracks
| style="text-align:center;"| 3
|}

 
{{succession box
 | before = 1 (The Beatles album)|1 by The Beatles ARIA Albums Chart Number-one albums of 2001 (Australia)|number-one album
 | years = January 22 – March 4, 2001
 | after = The Marshall Mathers LP by Eminem
}}
 
 
 
 
 
 
 
 
 
 
 
 
 

===More Music from Coyote Ugly===
{{Infobox album
| Name       = More Music from Coyote Ugly
| Type       = soundtrack
| Artist     = Various Artists
| Cover      = More Music from Coyote Ugly.jpg
| Released   = January 28, 2003
| Length     = 40:34 Pop
| Curb
| Producer   = Jerry Bruckheimer, Kathy Nelson, Mike Curb, Trevor Horn, Don Henley, Danny Kortchmar, Greg Ladayi, Ralph Jezzard, Snap!, John Boylan, Micheal Lloyd, Don Cook, Chris Waters, Brad Gilderman, Harvey Mason, Jr.
}}
{{Album ratings
| rev1 = Allmusic 
| rev1Score =   
}}

====Track listing====
{{tracklist
| writing_credits = no
| extra_column    = Recording artist(s)
| title1          = One Way or Another Blondie
| length1         = 3:31 Rebel Yell
| extra2          = Billy Idol
| length2         = 4:47
| title3          = Rock This Town
| extra3          = Stray Cats
| length3         = 2:39
| title4          = Keep Your Hands to Yourself
| extra4          = The Georgia Satellites
| length4         = 3:22
| title5          = Out of My Head Fastball
| length5         = 2:33 Battle Flag
| note6           = Lo-Fidelity Allstars Remix
| extra6          = Pigeonhed
| length6         = It Takes Two
| extra7          = Rob Base and DJ E-Z Rock
| length7         = 5:00 Love Machine
| extra8          = The Miracles
| length8         = 2:59
| title9          = We Can Get There
| note9           = Almighty Radio Edit
| extra9          = Mary Griffin
| length9         = 3:59
}}
{{tracklist
| writing_credits   = no
| extra_column      = Recording artist(s)
| headline          = Bonus tracks
| total_length      = 40:34
| title10           = Cant Fight the Moonlight Graham Stack Radio Edit
| extra10           = LeAnn Rimes
| length10          = 3:30
| title11           = But I Do Love You
| note11            = Almighty Radio Edit
| extra11           = LeAnn Rimes
| length11          = 4:02
}}

===Other songs in the film===
The following songs appear in the movie, but on neither of the two soundtracks that were released. DMX
* "Fly" by Sugar Ray
* "I Will Survive" by Gloria Gaynor
* "Thats Me" by Tara MacLean
* "Wherever You Will Go" by The Calling
* "Pour Some Sugar On Me" by Def Leppard Fly Away" by Lenny Kravitz
* " " by Reverend Horton Heat Follow Me" by Uncle Kracker
* "Cruisin for a Bruisin " by Nurse With Wound Never Let You Go" by Third Eye Blind
* "Love Is Alive" by Anastacia
* "Cowboy" by Kid Rock
* "Tony Adams" by Joe Strummer & The Mescaleros
* "Cailin" by Unwritten Law
* "Cant Help Falling In Love" by Elvis Presley
* "Like Water" by Chalk Farm
* "I Love Rock N Roll" by Joan Jett & the Blackhearts

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*    

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 