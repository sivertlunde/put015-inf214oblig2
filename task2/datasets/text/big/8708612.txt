Notes on a Scandal (film)
 
{{Infobox film
| name           = Notes on a Scandal
| image          = Notes on a Scandal.jpg
| caption        = Promotional movie poster
| director       = Richard Eyre Robert Fox Scott Rudin
| based on       =  
| screenplay     = Patrick Marber
| starring       = Judi Dench Cate Blanchett Bill Nighy
| music          = Philip Glass
| cinematography = Chris Menges John Bloom Antonia Van Drimmelen
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         = £15&nbsp;million
| gross          = $49.7 million
}} novel of the same name by Zoë Heller. The screenplay was written by Patrick Marber and the film was directed by Richard Eyre. The soundtrack was composed by Philip Glass.
 Best Actress, Best Supporting Best Adapted Best Original Score.

==Plot== Andrew Simpson), she confronts her. 

When Sheba asks her not to tell the school administration until after Christmas as she wants to be with her family, Barbara claims she has no intention of informing, providing Sheba ends the relationship immediately. Sheba eventually tells Steven that the affair is over. However, when she refuses to give in to Barbaras demands on her time, Barbara reveals the secret to a male teacher who tells her that he is attracted to Sheba and asks her to act as an intermediary.

After the affair becomes public, Barbara and Sheba both lose their jobs because Barbara knew about the affair and did not notify the authorities. Sheba is thrown out of her home by her husband and moves into Barbaras house. Sheba is unaware that Barbara is the reason she was found out, believing the affair became known because Steven confessed it to his mother.  When Sheba finds Barbaras diary and learns it was Barbara who leaked the story of the affair, she confronts Barbara and strikes her in anger. A row ensues, and Sheba runs outside with Barbaras journal to a crowd of reporters and photographers. When she becomes hemmed in by them, Barbara rescues her. Shebas emotions spent, she quietly tells Barbara that she had initiated the friendship with Barbara because she liked her and they could have been friends. She leaves Barbara, placing the journal on the table, and returns to her family home. Richard and Sheba face one another silently for several moments, and then Richard allows her to enter. Sheba is subsequently sentenced to 10 months in jail. 

In the final scene, Barbara meets another younger woman who is reading a newspaper about the Sheba Hart affair. Barbara says she used to know Sheba, but implies they hardly knew each other. Barbara introduces herself, invites her to a concert, and the pair continue to talk.

==Cast==
* Judi Dench as Barbara Covett
* Cate Blanchett as Bathsheba ("Sheba" or "Bash") Hart
* Bill Nighy as Richard Hart Andrew Simpson as Steven Connolly
* Tom Georgeson as Ted Mawson
* Michael Maloney as Sandy Pabblem
* Joanna Scanlan as Sue Hodge
* Shaun Parkes as Bill Rumer Emma Williams as Linda Phil Davis as Brian Bangs
* Juno Temple as Polly Hart
* Max Lewis as Ben Hart
* Anne-Marie Duff as Annabel

==Filming== Parliament Hill, Gospel Oak and Camden Town areas of northwest London. 

==Reception==
===Critical reaction===
The film opened to generally positive reviews, with Blanchett and Dench receiving critical acclaim for their performances, and receiving a  . He leans on Philip Glasss ever-present and insistent music like a crutch. But his natural gift for framing scenes is terrifically assured. A potent and evil pleasure." 

American publications also gave the film acclaim, with the Los Angeles Times describing the film as "Sexy, aspirational and post-politically correct, Notes on a Scandal could turn out to be the Fatal Attraction of the noughties."  The Washington Post noted the "dark brilliance" and that it "offers what is possibly the only intelligent account of such a disaster ever constructed, with a point of view that is somewhat gimlet-eyed and offered with absolutely no sentimentality whatsoever." The reviewer also identified the film as a "study in the anthropology of British liberal-left middle-class life."  Chicago Sun-Times film critic Roger Ebert heaped praise on the film and the acting performances "Perhaps the most impressive acting duo in any film of 2006. Dench and Blanchett are magnificent. Notes on a Scandal is whip-smart, sharp and grown up." 

However, the Houston Chronicle criticized the film as a melodrama, saying, " ramatic overstatement saturates just about every piece of this production". 

===Commercial===
The film grossed $49,752,391 worldwide,  exceeding its £15&nbsp;million budget. 

==Soundtrack==
 
The original score for the movie was composed by Philip Glass. The film features a song by Toots & The Maytals and another by Siouxsie and the Banshees.

==Awards and nominations== 79th Academy Awards nominees:
*Nominated: Best Actress – Judi Dench
*Nominated: Best Supporting Actress – Cate Blanchett
*Nominated: Best Adapted Screenplay – Patrick Marber
*Nominated: Best Original Score – Philip Glass

BAFTA Awards
*Nominated: Best British Film
*Nominated: Best Actress – Judi Dench
*Nominated: Best Adapted Screenplay – Patrick Marber

British Independent Film Awards
*Nominated: Best British Independent Film
*Won: Best Performance by an Actress in a British Independent Film – Judi Dench
*Nominated: Best Performance by a Supporting Actor or Actress in a British Independent Film – Cate Blanchett
*Won: Best Screenplay – Patrick Marber

Broadcast Film Critics Association Awards
*Nominated: Best Actress – Judi Dench
*Nominated: Best Picture
*Nominated: Best Supporting Actress – Cate Blanchett

Chicago Film Critics Association Awards
*Nominated: Best Actress – Judi Dench
*Nominated: Best Supporting Actress – Cate Blanchett
*Nominated: Best Adapted Screenplay – Patrick Marber
*Nominated: Best Original Score

Dallas-Fort Worth Film Critics Association Awards
*Won: Best Supporting Actress – Cate Blanchett

Evening Standard British Film Awards
*Won: Best Actress – Judi Dench

Florida Film Critics Circle Awards
*Won: Best Supporting Actress – Cate Blanchett

Golden Globe Awards
*Nominated: Best Actress in a Motion Picture, Drama – Judi Dench
*Nominated: Best Supporting Actress – Cate Blanchett
*Nominated: Best Screenplay – Patrick Marber

London Film Critics Circle Awards
*Nominated: Actress of the Year – Judi Dench
*Nominated: British Actress of the Year – Judi Dench
*Nominated: British Supporting Actor of the Year – Bill Nighy

Oklahoma Film Critics Circle Awards
*Won: Best Supporting Actress – Cate Blanchett

Online Film Critics Awards
*Nominated: Best Actress – Judi Dench
*Nominated: Best Supporting Actress – Cate Blanchett
*Nominated: Best Original Score – Phillip Glass

Phoenix Film Critics Society Awards
*Won: Best Supporting Actress – Cate Blanchett

Screen Actors Guild Awards
*Nominated: Best Actress – Judi Dench
*Nominated: Best Supporting Actress – Cate Blanchett

Toronto Film Critics Association Awards
*Won: Best Supporting Actress – Cate Blanchett

==References==
 

==External links==
 
*  
*  
*  
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 