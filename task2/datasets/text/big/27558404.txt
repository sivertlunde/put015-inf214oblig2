Sympathy for Delicious
{{Infobox film
| name = Sympathy for Delicious
| image = Sympathy for delicious.jpg
| caption = Film poster
| director = Mark Ruffalo
| producer = Mark Ruffalo Matt Weaver Andrea Sperling Scott Prisand
| writer = Christopher Thornton
| starring = Christopher Thornton   Mark Ruffalo   Juliette Lewis   Laura Linney   Orlando Bloom
| music = 
| cinematography = Chris Norr
| editing = Pete Beaudreau
| distributor = Corner Store Entertainment Crispy Films
| released =  
| runtime =
| country = United States
| language = English
| budget = 
| gross = $13,826 
}}
Sympathy for Delicious is a 2010 drama film, and the directorial debut of Mark Ruffalo. Filming took place in Los Angeles.

==Plot==
A newly paralyzed DJ (Thornton) gets more than he bargained for when he seeks out the world of faith healing.

==Cast==
* Christopher Thornton as "Delicious Dean ODwyer
* Juliette Lewis as Ariel Lee
* Mark Ruffalo as Joe
* Laura Linney as Nina Hogue
* Orlando Bloom as The Stain
* Noah Emmerich as Rene Faubacher
* John Carroll Lynch as Healer
* Robert Wisdom as Prendell
* Dov Tiefenbach as Oogie
* Deantoni Parks as Chuck
* Sandra Seacat as Mrs. Matilda

==Awards==
Mark Ruffalo won the Special Jury Prize at the 2010 Sundance Film Festival, and was also nominated for the Grand Jury Prize.

==Production==
Filming took place in Los Angeles, California, from 5 January to 28 February 2009.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 

 