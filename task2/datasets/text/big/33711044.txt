Symphony in Black
{{Infobox film
| name           = Symphony in Black
| image          = Symphony_in_Black.jpg
| image_size     =
| caption        = 
| producer       = 
| writer         = Milton Hockey, Fred Rath
| director       = Fred Waller
| narrator       = 
| starring       = Duke Ellington
| music          = Duke Ellington
| cinematography = William O. Steiner
| editing        =  Paramount
| released       =  
| runtime        = 9 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Symphony in Black: A Rhapsody of Negro Life is a nine-and-a-half minute musical short produced in 1935 that features Duke Ellington’s early extended piece, "A Rhapsody of Negro Life". The film, Billie Holiday’s screen debut, was directed by Fred Waller and distributed by Paramount Pictures.

Symphony in Black represents a landmark in musical, cultural, and entertainment history as well as significant progress in Ellington’s own biography. It a member of the first generation of non-classically arranged orchestral scores and perhaps most importantly, one of the first films written by an African-American describing African-American life to reach wide distribution.

==Background==
Symphony in Black depicts black life in America and the composition itself (A Rhapsody of Negro Life) is divided into four parts: “The Laborers,” “A Triangle”, “A Hymn of Sorrow” and “Harlem Rhythm”. “A Triangle” features the vocals by Holiday and solos by jazz clarinetist and tenor saxophonist Barney Bigard and Ellington Orchestra regular, trombonist Tricky Sam Nanton|Tricky Sam Nanton. It is important to note that while Symphony in Black is the title of the Paramount film, A Rhapsody of Negro Life is the actual title of Ellingtons composition. The piece has been largely overshadowed by its successor, perhaps Ellington’s most best known extended composition, Black, Brown, and Beige and thus significantly less documentation exists concerning it.

Despite that, on October 19, 1935, The Chicago Defender published an article entitled "Spotlites of Harlem", that announced the release of Symphony in Black among other current musical events of interest to readers.  The film won the Best "musical short subject" Academy Award for its year. 

==Revival==
In 1989, The New York Times published an article entitled “Celebrating the Variety of Ellington”, which discussed a revival of the piece by the American Jazz Orchestra in honor of Duke Ellington’s 90th birthday. "It contains some of Ellington’s most inventive, dramatic music", said Gunther Schuller (one of the conductor). Schuller went on to say, "If things had been right, he would have made a great opera composer. He takes crude scripts and writes music that underscores the scenario as well as any film composer ever did. It is greater music by the fact that Ellington went beyond the functional aspect of film music, and it’s great music intrinsically."  "Celebrating the Variety of Ellington," New York Times, April 21, 1989, C.4. 

The other conductor of the Orchestra, Maurice Peress was especially interested in reviving Ellington’s rarer works, a description which certainly includes Symphony in Black/"A Rhapsody of Negro Life". According to the article, Ellington only recorded the piece once for the original score to the film, and in fact the recording is so rare, it does not show up in any Ellington discographies.  Several of the symphony’s themes though were taken out and made into shorter individual pieces. Despite this, Schuller said, "…by and large, the piece vanished from memory, except in the minds of a few Ellington film collectors." 
	
"It’s terribly important for these pieces to be performed", said Schuller. "No musical tradition can remain vital unless it’s performed live. No musical tradition can exist in a museum form. I believe that the whole idea of repertory in jazz is long, long overdue. It’s not yet caught on as a major idea, but consider that the history of jazz is 60 or 70 or 80 years old, so we have a tremendous amount of music that can be replicated. It’s no different than in classical, where you play Brahms with one sound, Debussy with another – that’s the sort of care we have to bring to all this. This isn’t slavish imitation: it’s bringing the music alive, which it deserves to be."  

==Legacy==
In The Musical Quarterly, John Howland wrote an article entitled "The Blues Get Glorified: Harlem Entertainment, Negro Nuances, and Black Symphonic Jazz". In his analysis of the piece, Howland says, "the narrative content and musical arrangement of the film present an ideal microcosm of the hybrid cultural aesthetics that informed a special category of prewar, jazz-based concert works by popular music composers in Harlem."  John Howland "The Blues Gets Glorified: Harlem Entertainment, Negro Nuances, and Black Symphonic Jazz", The Musical Quarterly, October 17, 2008 
	
He goes on to say that “the pantomime narrative of Symphony in Black depicts a celebrated African American symphonic composer—Duke Ellington—and the world premiere of his racially motivated symphony, or rhapsody. The subtitle of this film, like many other rhapsody-themed stage numbers and jazz-styled concert works of the day, purposefully alludes to the inspiration and catalyst for the most symphonic jazz concert works of the 1920s and 1930s, George Gershwin’s Rhapsody in Blue. The identification of popular-idiom concert works and concert-style popular song arrangements is central to understanding the mixed cultural aesthetics and formal design of a work like Symphony in Black, which owes very little to the performance traditions, formal expectations, and generic conventions of Euro-American classical music.”  
 Broadway musical theatre|musicals, interwar radio, and the deluxe movie palace prologues of the day. Such concert-style popular music was central to the theater and stage (as opposed to dance) repertories of these orchestral traditions, and the spectacular jazz-oriented production number arrangements of many contemporary stage and film musicals were among the most visible extensions of these practices." 

==References==
 

==External links==
* 

 
 
 
 
 