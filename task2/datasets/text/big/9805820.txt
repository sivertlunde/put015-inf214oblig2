Je t'aime moi non plus (film)
:For the song, see Je taime... moi non plus. For the album, see Jane Birkin Serge Gainsbourg.

{{Infobox film
| name           = Je taime moi non plus
| image          = Je taime... moi non plus poster.jpg
| image size     = 
| caption        = Theatrical poster
| director       = Serge Gainsbourg
| producer       = Claude Berri Jacques-Eric Strauss
| writer         = Serge Gainsbourg
| narrator       = 
| starring       = Jane Birkin Joe Dallesandro Hugues Quester
| music          = Serge Gainsbourg
| cinematography = Willy Kurant
| editing        = Kenout Peltier
| distributor    = AMLF (1976) (France)
| released       = 10 March 1976 (France)
| runtime        = 89 min
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 1976 feature cameo by Gérard Depardieu.

== Plot introduction ==
The plot of the movie centers on Krassky (Joe Dallesandro), a homosexual man, who is attracted to Johnny (Jane Birkin), a boyish looking woman. They begin an affair, which is complicated by the fact that he cannot achieve an erection so as to perform vaginal intercourse. The pain of anal intercourse is so great for Johnny, though, that her screams cause them to be thrown out of a series of motels. http://www.eyeforfilm.co.uk/reviews.php?film_id=12467  After a row with Johnny, Krassky returns to his boyfriend Padovan (Hugues Quester).

== Motives ==
Je taime moi non plus was the first film directed by Gainsbourg. http://filmsdefrance.com/FDF_Je_t_aime_moi_non_plus_1976_rev.html  Jane Birkin was his partner at that time.  It includes elements of symbolism recurrent in Gainsbourgs work: death and sex.  Depardieu has a few short appearances, playing a homosexual bestialist.

== See also ==
* Serge Gainsbourg

== References ==
 

== External links ==
*  
*  

 
 
 
 
 


 
 