Nos 18 ans
{{Infobox film
| name = Nos 18 ans
| image = 
| caption = 
| director = Frédéric Berthe
| producer = Marie-Jeanne Pascal
| screenplay = Éric Assous
| based on =  
| starring = Théo Frilet Valentine Catzéflis
| music = Arnaud Matteï
| cinematography = Pavans de Ceccatty Philippe   
| editing = Célia Lafitedupont   
| distributor = 
| released =  
| runtime = 93 minutes
| language = French
| country = France
| budget = 
}}

 
Nos 18 ans is a French comedy teen film directed by Frédéric Berthe and released in 2008 in film|2008. It is a remake of the 2006 Italian film "Notte prima degli esami" ("Night Before the Exams") written and directed by Fausto Brizzi. Alternative titles for the film include Schools Out. 

==Plot== homages to the 1980s.

==Soundtrack ==
The soundtrack includes many hits evoking the 1980s:

* Yelle: "Nos 18 ans"
* The Cure: "Close to me"
* Telephone: "Ca cest vraiment toi"
* Sexpress: "Theme from sexpress"
* Simply Red: "If you dont know me by now"
* 10cc: "Im not in love"
*  " Marino Marini: "Come Prima"
* Metiswing: "De cuba trango este son"
*  "
* Mano Negra: "Mala Vida"
*  "
* David Bowie: "Modern love"
* Rickie Lee Jones: "On Saturday Afternoon in 1963"
* The Beautiful South: "Song for whoever"
* The Jam: "Town called Malice"
* Les Rita Mitsouko: "Andy"

==Cast==
* Théo Frilet as Lucas
* Valentine Catzéflis as Clémence
* Michel Blanc as the teacher Martineau
* Arthur Dupont as Maxime
* Julia Piaton as Sarah
* Liza Manili as Alice
* Pierre Boulanger as Richard
* Bernadette Lafont as Adèle
* Venantino Venantini as Marcello
* Maruschka Detmers as Clémences mother
* Iris Besse as Valentine
* Annabel Rohmer as Clarisse
* Bartholomew Bouteillis as Yvan
* Pierre Niney as Loïc
* Lucy Ferry as Laura
* Aurélie Cabrero as Princess Leia
* Sylvain Levitte as Edgar Le Prince
* Sébastien Houbani as "Michael Jackson"
* Eric Naggar as the principal
* Xavier Gallais as the gynaecologist
* Roger Contebardo as Werner
* Alain Pointier

==See also==
* List of French films of 2008

==References==
 

==External links==
*  
*   

 
 
 
 
 
 

 
 