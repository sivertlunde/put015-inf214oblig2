Buddha Gujjar
{{Infobox film
| name           = Buddha Gujjar
| image          = BUDHA GUJJAR (2002).JPG
| caption        = Film poster
| director       = Syed Noor
| producer       = Haji Chaudhry Faqeer Mohammad, Chaudhry Shahbaz Ali alias Ch. Karamat
| writer         = Syed Noor Shaan Resham Saima
| music          = Tafoo
| cinematography = 
| editing        =
| released       = December 6, 2002
| runtime        =
| country        = Pakistan Punjabi
| budget         =
| domestic gross =
| preceded_by    =
| followed_by    =
}}

Buddha Gujjar is a Pakistani Punjabi film by Ch. Shahzad Ali for Pak Nishan Films.

That the director of Buddha Gujjar hardly innovates is apparent even from the name of the movie. The suffix Gujjar has appeared in about half a dozen movies released during the last 12 months, though only a couple of them have been commercially successful.

==Film business==
Syed Noor has, during the last decade or so, acquired the status of the filmmaker in Pakistan but his critics say his movies seldom defy the formula. Buddha Gujjar, in more ways than one justifies this criticism. The narrative is simplistic - rather one-dimensional like most movie plots in Pakistan. The form, too, is rather conventional. Golden Jubilee, 63 weeks .Naghma 15, Odeon 13 weeks in Lahore.

This is reminiscent of Jatt phenomenon led by Maula Jatt, though Syed Kamal can be given the credit of using the word prior to its release not in one but a series of flicks like Jatt Kurian Toun Darda and Jatt Kamala Gaya Dubai. Filmmakers, inspired by Maula Jatts phenomenal success thought the secret lay in the name alone and, therefore, they continued using it until it ran out of steam to be able to ensure box-office viability for loose plots and even looser productions. In Gujjars case, the word is yet to run its complete course and Buddha Gujjar is fortunate enough to have used it when it is a bit of a novelty.
 Yousaf Khan. Shaan strives Arshad Mehmood do well.

The saving grace of the movie is its realistic setting and an attempt to keep the wardrobe as wearable as possible, obviously with a couple of exceptions for lead female characters played by Saima and Resham. The music and the lyrics too are apt and evocative, though not memorable.

==See also==
* Syed Noor
* Lollywood
* Badmash Gujjar

==References==
* http://www.muziqpakistan.com/board/index.php?showtopic=18645

 

 
 
 
 


 