Hooray for the Blue Hussars
 
{{Infobox film
| name           = Hooray for the Blue Hussars
| image          = 
| caption        = 
| director       = Annelise Reenberg
| producer       = Leif Jul Fritz Trap
| writer         = Annelise Reenberg Jerrard Tickell
| starring       = Emil Hass Christensen
| music          = Sven Gyldmark
| cinematography = Ole Lytken
| editing        = Lizzi Weischenfeldt
| studio         = Saga Studios
| released       =  
| runtime        = 100 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Hooray for the Blue Hussars ( ) is a 1970 Danish comedy film directed by Annelise Reenberg and starring Emil Hass Christensen.

==Cast==
* Emil Hass Christensen - Oberst Parsdorff
* Lone Hertz - Charlotte Parsdorff
* Niels Hinrichsen - Løjtnant Adam Lercke
* Peter Bonke - Lt. Ditlev Liljenkrone
* Ghita Nørby - Frida
* Dirch Passer - Spjellerup
* Jørgen Kiil - Ritmester von Rabenberg
* Bjørn Puggaard-Müller - Pastor Berg
* Susse Wold - Henrietta / Clarissa
* Henny Lindorff Buckhøj - Pastorinden
* Ole Søltoft - Løjtnant
* Søren Strømberg - Løjtnant
* Peter Hetsch - Løjtnant
* Jørn Madsen - Løjtnant
* Thecla Boesen - Gæst hos pastorinden
* Lili Heglund - Gæst hos pastorinden
* Solveig Sundborg - Gæst hos pastorinden
* Karl Gustav Ahlefeldt - Adams far
* Signi Grenness - Adams mor
* Povl Wøldike - Ditlevs far
* Lilli Holmer - Ditlevs mor
* Paul Hagen - Krovært
* Hugo Herrestrup - Krovært
* Bendt Reiner - Husar
* Ulla Jessen - Dame på kro
* Merete Arnstrøm - Dame på kro
* Bente Puggaard-Müller - Dame på kro

==External links==
* 

 
 
 
 
 
 
 
 