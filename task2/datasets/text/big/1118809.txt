"Crocodile" Dundee II
 
 
 
{{Infobox film
| name           = "Crocodile" Dundee II
| image          = Crocodile dundee ii ver2.jpg
| caption        = Theatrical release poster
| director       = John Cornell
| producer       = John Cornell
| writer         = Paul Hogan Brett Hogan
| starring       = Paul Hogan Linda Kozlowski John Meillon Peter Best
| cinematography = Russell Boyd
| editing        = David Stiven
| distributor    = Paramount Pictures
| released       =  
| runtime        = 108 minutes
| country        = Australia United States
| language       = English
| budget         = $14 million 
| gross          = $239.6 million 
}} adventure and comedy film. It is a sequel to the 1986 film "Crocodile" Dundee, and was followed by 2001s Crocodile Dundee in Los Angeles. Actors Paul Hogan and Linda Kozlowski reprise their roles as Mick Dundee and Sue Charlton, respectively; here shown opposing a Colombian drug cartel.

The film was shot on location in New York City and Northern Territory, Australia.  It cost $14 million to make. TV Week magazine, 4 June 1988, page 11.  "Box office war" by Ivor Davis. 

==Plot==
A year has passed since the events of "Crocodile" Dundee, and Mick Dundee and Sue Charlton are living happily together in New York. Although Micks ignorance of city life is a hazard when he attempts to continue his former lifestyle, like blast fishing in Manhattans waters, Sues writing has made him a popular public figure. He later goes to work for Leroy Brown, a stationery salesman trying to live up to his self-perceived Bad, Bad Leroy Brown|bad guy in the streets image.

While working for the DEA in Colombia, Sues ex-husband Bob (mentioned, but not seen, in the first movie) takes photographs of a drug cartel leaders murder of an unknown person, and is spotted by one of the cartels sentries. He sends the photographs to Sue, before being murdered. Colombian Cartel leader Luis Rico, and his brother and top lieutenant, Miguel, go to New York City to retrieve the photos.

The gangsters hold Sue hostage, leading Mick to ask Leroy for help. Leroy contacts a local street gang, whom Mick asks to create a distraction by caterwauling at the mansions perimeter, leading most of the cartels guards on a wild goose chase while Mick rescues Sue. Rico is arrested but soon escapes police custody, and after a failed attempt to kill Sue, Mick decides to take Sue to   in Tok Pisin). Here, Sue discovers that Mick owns land equal to the size of New York State, including a gold mine.
 Aboriginal tracker abandons them when he hears that their quarry is Mick. As a replacement, the gangsters kidnap Micks friend Walter and force him to guide them, but Mick saves his friend by faking an attempt on Walters life. They then lead the gangsters on a false trail through the Outback territory, during which Mick, with the help of his Aboriginal friends, manages to reduce the oppositions numbers one by one, leaving the rest increasingly nervous. In the end, he retrieves Walter from Rico and Miguel, leaving the latter to face him alone.

Tired of chasing Dundee, Rico sets a bushfire to corner Mick, but Mick regains the upper hand, captures Rico, and switches clothes with him in order to lure Miguel into a vulnerable position. Sue and Walter, observing them from a distance, mistake Mick for Rico and take shots at him. Walter shoots Mick, though not fatally, and Rico tries to escape but is shot by Miguel (who mistakes him for Mick). Rico loses his balance and falls to his death into a ravine. Miguel is in turn shot and killed by Sue. Though thinking at first that Mick is dead, they soon re-unite with him (Walters bullet had only hit Mick in the side), and Sue and Mick embrace. When Mick asks her whether she is ready to go home, Sue replies "I am home", concluding the film.

==Cast==
*  
* Linda Kozlowski: Sue Charlton
* John Meillon: Walter Reilly (Meillon died shortly after the films release)
* Hechter Ubarry: Luis Rico Juan Fernández: Miguel
* Charles S. Dutton: Leroy (as Charles Dutton)
* Kenneth Welsh: Brannigan
* Stephen Root: DEA Agent
* Dennis Boutsikaris: Bob Tanner
* Ernie Dingo: Charlie
* Steve Rackman: Donk
* Gerry Skilton: Nugget
* Gus Mercurio: Frank
* Susie Essman: Tour Guide
* Colin Quinn: Onlooker at Mansion Luis Guzman: Jose
* Alec Wilson: Denning Jim Holt: Erskine
* Bill Sandy: Teddy The Aboriginal Tracker
* Alfred Coolwell: Aboriginal

==Release==
The film opened 25 May 1988 in the United States and Canada.  

===Box office===
"Crocodile" Dundee II was also a worldwide hit,    but not as big as its predecessor.

The film grossed United States dollar|$24,916,805 in Australia,  which is equivalent to $48,843,593 in 2009 dollars.

The film was released theatrically in the United States by Paramount Pictures in May 1988.  It grossed $109,306,210 at the domestic box office.   It was the second highest grossing film that year for Paramount (second only to Coming to America) and the sixth highest grossing film at the United States box office. 

==Reception==
The film did well at the box office but not with critics.  It was sixth-highest-grossing film of the year in the United States and earned more than $240 million worldwide. For its first six days of American release, its box office receipts of US$29.2 million exceeded those of Rambo III at $21.2 million. 

"Crocodile" Dundee II received generally negative reviews from critics. Janet Maslin of The New York Times deemed the sequel to be inferior, noting "the novelty has begun to wear thin, even if Mr. Hogan remains generally irresistible."    Review aggregator Rotten Tomatoes assessed a "Rotten" score of 12% with an average 3.7/10 rating. 

==References==
 

==External links==
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 