Reclaim (film)
{{Infobox film
| name           = Reclaim
| image          = File:Reclaim 2014 film poster.jpg
| alt            =
| caption        = 
| film name      = 
| director       = Alan White
| producer       = 
| writer         = Luke Davies Carmine Gaeta
| screenplay     = 
| story          = 
| based on       =  
| starring       = Ryan Phillippe Jacki Weaver with Rachelle Lefevre and John Cusack
| narrator       = 
| music          = Inon Zur
| cinematography = Scott Kevan
| editing        = Scott D. Hanson Doobie White
| studio         = Asia Tropical Films Beijing Shuijing Shenlan International Media Co. Garlin Pictures
| distributor    = Lionsgate
| released       =  
| runtime        = 
| country        = China Malaysia USA
| language       = English
| budget         = 
| gross          =  
}}
 thriller film that was directed by Alan White. The film was released on video on demand and also received a simultaneous limited theatrical release in the United States on September 19, 2014.  It stars Ryan Phillippe and Rachelle Lefevre as an American couple that travels to Puerto Rico to adopt an orphan (Briana Roy) but become entangled in a deadly scam.

==Synopsis==
Steven (Ryan Phillippe) and Shannon (Rachelle Lefevre) are a young couple that have decided to adopt an orphan after Shannon loses the ability to conceive since a horrific car accident. They contact IRA (Intl Rescue Adoption), an adoption organization run by Reigert (Jacki Weaver), who introduces them to Nina (Briana Roy), who was orphaned after an earthquake in Haiti. Everything seems to be progressing as planned but before the adoption can be finalized, Reigert and Nina vanish and neither Shannon nor Steven can find any way to contact Intl. Rescue Adoption. They discover that theyve been the victims of a confidence scheme but rather than give up, they decide to follow after Reigert in the hopes of claiming Nina. Instead, they come across a trio of child traffickers that also hope to find and use Nina for their own purposes, but are also willing to hold Shannon and Steven hostage in the hopes of making even more money. 

==Cast==
*Ryan Phillippe as Steven
*Jacki Weaver as Reigert
*John Cusack as Benjamin
*Rachelle Lefevre as Shannon
*Luis Guzmán as Detective
*Briana Roy as Nina
*Jandres Burgos as Salo
*Veronica Faye Foo as Paola (as Liz Veronica Foo)
*Alex Cintrón as Hotel Manager
*Millie Ruperto as Female Sergeant
*Oscar H. Guerrero as Taxi Driver
*Reema Sampat as Interpreter
*Isabelle Adriani as Esmeralda
*Sunshine Logroño as Mr. Lopez (Bank Manager)
*Sean Taylor as Gerry

==Production==
Plans to film Reclaim were announced in 2013 and White was set to direct the film, which was initially intended to take place in Australia.    This was later changed after Screen Australia declined the films investment pitch and the filming and setting was changed to take place in Puerto Rico.    Filming took place in late 2013 over a period of 23 days, which Lefevre has described as "intense".   Child actress Roy commented on her role, stating that she found the hardest part to be "speaking all the languages, French, Creole and English.“ 

==Reception==
Critical reception for Reclaim has been predominantly negative and as of December 14, 2014, the film holds a rating of 0% on Rotten Tomatoes (based on 9 reviews) and 26 on Metacritic (based on 7 reviews).   Much of the negative criticism centered around the films plot,  which many reviewers found too overly familiar and formulaic.   Reviewers did praise the films acting and scenery,  with The Hollywood Reporter highlighting child star Brianna Roy out as a highlight.  The Los Angeles Times criticized the movie for its script, writing "Carmine Gaeta and Luke Davies screenplay is constructed from plot mechanics, and the emotional stakes grow less convincing with every twist of the screw." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 