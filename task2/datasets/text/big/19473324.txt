Sweepers (film)
{{Infobox film
| name           = Sweepers
| image          = Sweepers98.jpg
| caption        =
| director       = Keoni Waxman
| producer       =  
| writer         = Kevin Bernhardt, Keoni Waxman
| starring       = Dolph Lundgren
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       = 1998
| runtime        = 96 minutes.
| country        = South Africa United States English
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}} 1998 United American and minesweeping operation in Angola. In the events his son is killed and he discovers that mines are being planted during the war to kill people in the area.

==Cast==
*Dolph Lundgren ...  Christian Erickson 
*Sheldon Allen... Body Double/Stuntman
*Bruce Payne ...  Dr. Cecil Hopper 
*Claire Stansfield ...  Michelle Flynn  Ian Roberts ...  Yager 
*Fats Bookholane ...  Old Mo 
*Sifiso Maphanga ...  Arthur 
*Ross Preller ...  Jack Trask 
*Nick Boraine ...  Mitch 
*Cecil Carter ...  Ray Gunn 
*David Dukas ...  Sweeper #4 
*Zukile Ggobose ...  Zukili  
*Philip Notununu ...  Mercenary #1 
*Gabriel Mndaweni ...  Mercenary #2 
*Frank Pereira ...  Mercenary #3 
*Dave Ridley ...  Mercenary #4

==External links==
*  
*  

 
 
 
 
 
 


 
 