Brake Fail
{{Infobox film
| name           = Brake Fail
| image          = Brake Fail Bengali movie poster.jpg
| image_size     = 240px
| border         = 
| alt            = 
| caption        = 
| director       = Kaushik Ganguly
| producer       = 
| writer         = 
| screenplay     = Kaushik Ganguly
| story          = Kaushik Ganguly
| based on       =  
| narrator       = 
| starring       = Parambrata Chattopadhyay Swastika Mukherjee Annu Kapoor Saswata Chatterjee Santu Mukherjee Arindam Sil  Lama  Taranga Biswas Paran Bandopadhyay Tanima Sen
| music          = Neel Dutt
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  2009
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}} Bengali film. The film was directed by Kaushik Ganguly. The film was produced by Mumbai Mantra and the music of the film was composed by Neel Dutt.      

== Plot ==
 

== Cast ==
* Parambrata Chattopadhyay
* Swastika Mukherjee
* Annu Kapoor
* Saswata Chatterjee
* Santu Mukhopadhyay
* Arindam Sil
* Lama (Arindam Haldar)
* Taranga Biswas
* Paran Bandopadhyay
* Tanima Sen

== Songs ==
* Jodi Proshno Karo
* Jinga La la
* Shorey Shorey Jay
* Rang Laga De
* Bhenge Mor Ghorer Chabi
* Ghure Taakalei

== See also == Laptop

== References ==
 

 

 
 
 


 