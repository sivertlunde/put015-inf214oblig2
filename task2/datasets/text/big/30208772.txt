Iruttinte Athmavu
{{Infobox film
| name           = Iruttinte Athmavu
| image          = Iruttinte Athmavu2.jpg
| image_size     =
| caption        =
| director       = P. Bhaskaran
| producer       = P. I. Muhammed Kasim
| writer         = M. T. Vasudevan Nair
| narrator       =
| starring       = Prem Nazir, Sharada (actress)|Sharada, Thikkurissy Sukumaran Nair, P. J. Antony, Ushakumari/Vennira Aadai Nirmala, Kozhikode Shantha Devi, Baby Rajani
| music          = Baburaj|M. S. Baburaj
| cinematography = E. N. Balakrishnan
| editing        = G. Venkitaraman	, Das
| studio         = Sony Pictures
| distributor    = Bharath Pictures
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}

Iruttinte Athmavu ( ,  ) is a 1967 Malayalam film directed by P. Bhaskaran and written by M. T. Vasudevan Nair based on his own story with the same name. {{cite book
 |author=
 |title=The Illustrated weekly of India
 |volume= Volume 91
 |issue= Issue 4
 |publisher=
 |year=1970
 |page=19
 |isbn=
}}  It stars Prem Nazir, Sharada (actress)|Sharada, Thikkurissy Sukumaran Nair, P. J. Antony, Ushakumari, Kozhikode Shantha Devi and Baby Rajani in important roles, and features music by Baburaj|M. S. Baburaj, cinematography by E. N. Balakrishnan and editing by G. Venkitaraman and Das.

The film is about a mentally retarded youth born into a matriarchal family who is forced to live as a mad man in chains and who is misunderstood and ill-treated by everyone except his uncles daughter. {{cite book
 |author=
 |title=Indian review of books
 |publisher=Acme Books
 |year=1995
 |page=30
 |isbn=
 |accessdate=2010-12-28}}  Prem Nazir played the mentally challenged Bhranthan Velayudhan, widely accepted to be an extra ordinary performance and one of the best in his career.  {{cite book
 |author=
 |title=Indian newsmagazine
 |volume= Volume 14
 |publisher=Link
 |year=1972
 |page=36
 |isbn=
}}   Nazir himself rated his role of Velayudhan in Iruttinte Athmavu and as the swashbuckling folk hero Thampan in Padayottam as his best. {{cite book
 |author=
 |title=India Today
 |volume= Volume 14 part =
 |publisher=Living Media India Pvt. Ltd
 |year=1989
 |page=46
 |isbn=
}}  Also, its script is regarded as one of the finest by M. T. Vasudevan Nair. A major landmark in Malayalam cinema, the film provided Malayalam cinema with a new direction; that of the low-budget film. The film has earned a dedicated cult following. It won the National Film Award for Best Film on Other Social Issues. {{cite book
 |author=T. M. Ramachandran
 |title=Film world
 |volume= 7 issue =
 |publisher=
 |year=1971
 |page=106
 |isbn= Best Film award only narrowly. {{cite book
 |author=
 |title=Malayalam literary survey
 |volume=
 |issue=
 |publisher=Kerala Sahitya Akademi
 |year=1982
 |page=121
 |isbn=
}}  Despite all the acclaim, the film was a box office failure. {{cite book
 |author=Amaresh Datta
 |title=The Encyclopaedia Of Indian Literature
 |volume=
 |publisher=
 |year=2006
 |page=752
 |isbn=
}} 

==Plot==
 
Velayudans existence poses a problem to all the members of the joint family. Velayudhan is a twenty - year old man, but he has the intelligence of a child. The head of the joint family thinks Velayudan symbolizes the curse which hangs heavy over the house. To his mother he is source of constant sorrow. His uncles daughter is his would be bride. He is attached to her. She is very kind to him and refuses to treat him as a mad man. Velayudan triggers problems one after the other and every new lapse help only to put fresh chains. He refuses to feel he is mad. In the end Ammukutty is given away in marriage to an old widower. Velayudhan surrenders himself and yells "Chain me I am mad!"

==Cast==
 
* Prem Nazir as Bhranthan Velayudhan
* Thikkurissy Sukumaran Nair as Madhavan Nair (Karanavar)
* P. J. Antony as Gopalan Nair
* M. S. Nampoothiri as Muthachan (grandfather) {{cite book
 |author=
 |title=Indian newsmagazine
 |volume= Volume 9
 |publisher=Link
 |year=1967
 |page=38
 |isbn=
}} 
* T. S. Muthaiah as Rajan
* K. Balaji as Chandran
* Sankaradi as Achuthan Nair (house servant) 
* Adoor Bhasi as Guru Kunhichathu
* Kaduvakulam Antony as Jyotsyan (astrologist)
* Baby Rajani as Unni Sharada as Ammukutty {{cite book
 |author=Hameeduddin Mahmood
 |title=The kaleidoscope of Indian cinema
 |volume=
 |issue=
 |publisher=Affiliated East-West Press
 |year=1974
 |pages=106, 146
 |isbn=
}}  {{cite book
 |author=
 |title=India today
 |volume= Volume 14
 |issue= Issues 13-24
 |publisher=Living Media India Pvt. Ltd
 |year=1989
 |page=82
 |isbn=
}} 
* Ushakumari/Vennira Aadai Nirmala as Prema
* Kozhikode Shantha Devi as Parukkutty Amma (Velayudhans mother) 
* Philomina as Meenakshi Amma
* Padmini as Nani
* Shobha as Malini
* Seleena as Neeli
* Rugmini as Karthi
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ambaadikannanu Mampazham || S Janaki || P. Bhaskaran ||
|-
| 2 || Eeranuduthukondambaram || S Janaki || P. Bhaskaran ||
|-
| 3 || Irukanneerthullikal || S Janaki || P. Bhaskaran ||
|-
| 4 || Vaakachaarthu Kazhinjoru || S Janaki || P. Bhaskaran ||
|}

==Writing==
The film is scripted by M. T. Vasudevan Nair based on his own a short story with the same name. The screenplay is regarded as one of the finest by the noted writer.    One could see a lot of the pre-occupations of the scenarist, who carried the touches of human relationships through all of his subsequent films whether as screenplay writer or director.  A part of the screenplay of Iruttinte Athmavu is being taught in school classes while the complete screenplay is being taught at degree level.  

==Legacy==
The film provided Malayalam cinema with a new direction; that of the low-budget film.  In spite of its large number of studio shots and overall theatricality, the film was so culturally rich that many of the episodes would become archetypes for future Malayalam film makers dealing with family drama. 

==References==
 

== External links ==
*  

 
 
 
 