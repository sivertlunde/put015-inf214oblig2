Dost Garibon Ka
{{Infobox film
| name           = Dost Garibon Ka
| image          = DostGaribonKa.png
| image size     = 
| border         = 
| alt            = 
| caption        = Promotional Poster
| director       = C.P. Dixit 
| producer       = Firoz A. Nadiadwala 
| writer         = 
| screenplay     = M.M. Baig 
| story          = Jehan Nayyar 
| based on       = 
| narrator       =  Govinda & Neelam 
| music          = Laxmikant Pyarelal 
| cinematography = Anil Dhanda 
| editing        = Hussain A. Burmawala 
| studio         = 
| distributor    = 
| released       =     
| runtime        = 
| country        = India 
| language       = Hindi 
| budget         = 
| gross          = 
}}
 Hindi language Govinda and Neelam Kothari|Neelam. The movie was directed by C.P. Dixit and was released in 1989.    

==Cast==
{| class="wikitable sortable"
|-
! Actor !! Role
|- Govinda  || Vijay / Barkat Ali
|- Neelam || Rekha
|-
| Sumeet Saigal || Amar
|-
| Raza Murad || Thakur Ranjeet Singh
|-
| Satish Shah || Barkat Ali Khan / Bholeram
|-
| Anjana Mumtaz || Laxmi - Vijays & Parvatis mother
|-
| Dan Dhanoa || Dan
|-
| Vijay Arora || Dinanath - Vijays & Parvatis father
|-
| Om Shivpuri || Seth Dharamdas
|-
| Rajendra Nath || Owner / manager restaurant
|}

==Soundtrack==
Movie featured 5 songs. Music was by Laxmikant Pyarelal and lyrics by Anand Bakshi.    
{{Infobox album
| Name = Dost Garibon Ka
| Type = soundtrack
| Artist = Laxmikant Pyarelal
| Cover = 
| Released = 13 Jan 1989
| Recorded =  Feature film soundtrack
| Length = 
| Label = 
| Producer = 
| Last album = 
| This album = Dost Garibon Ka (1989)
| Next album = 
}}

{{tracklist
| headline        = Tracklist
| music_credits   = no
| extra_column    = Artist(s)
| title1          = Mera Naam Very Good
| extra1          = Sudesh Bhosle
| length1         = 6:20
| title2          = Kafan Apna Kabhi
| extra2          = Mohammed Aziz & Kavita Krishnamurthy
| length2         = 6:33
| picturised on   = 
| title3          = Daulat Hai Kya
| extra3          = Amit Kumar 
| length3         = 4:38
| title4          = Ab Bol Kya Bolta Hai
| extra4          = Kishore Kumar
| length4         = 6:03
| title5          = Dekhne Ki Cheez Hoon Main
| extra5          = Kavita Krishnamurthy
| length5         = 5:34
}}

==See also==
  Govinda
*Neelam Neelam

==References==
 

 
 
 

 