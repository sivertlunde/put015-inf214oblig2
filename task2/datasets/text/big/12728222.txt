Bronco Bullfrog
{{Infobox film
| name           = Bronco Bullfrog
| image          = 
| alt            =  
| caption        = 
| director       = Barney Platts-Mills
| producer       = Andrew St. John Michael Syson
| writer         = Barney Platts-Mills
| starring       = Del Walker Anne Gooding Sam Shepherd Roy Haywood
| music          = Tony Connor Keith Gemmell Trevor Williams Howard Werth
| cinematography = Adam Barker-Mill
| editing        = Jonathan Gili
| studio         = 
| distributor    = 
| released       = 1969  (UK) 
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British black-and-white film directed by Barney Platts-Mills.  It was Platts-Mills first full-length feature film.

==Plot== 

The film follows the fortunes of a 17 year old, Del (Del Walker) and his group of friends.   As the film opens four youths (Del, Roy, Chris and Geoff) are seen breaking into a cafe in Stratford, East London, but they only get away with about ninepence and some cake, and it is clear that they are hardly master criminals. Back at their hut on waste ground they mention Jo (Sam Shepherd), known as Bronco Bullfrog (for reasons which are never explained), who has just got out of Borstal.

Once Del and Roy (Chris and Geoff are hardly seen again in the film) meet Jo in a caff, they link up with him to carry out a bigger robbery.  Meanwhile Del meets Irene (Anne Gooding), a friend of a cousin of Chris, and they start a relationship, despite the disapproval of Irenes mother and Dels father.  The remainder of the film follows Del and Irene as they attempt to escape their dead-end lives.
==Production==
The film was turned down by Bryan Forbes at EMI Films. The eclipse of the moon man
Malcom, Derek. The Guardian (1959-2003)   26 Mar 1971: 15.  
==Accolades==

The film has been described as "Mod poetry" and a "masterpiece". {{cite book
| last1                 =Catterall
| first1                =Ali
| last2                 =Wells
| first2                =Simon
| authorlink2           =Simon Wells
| title                 =Your Face Here: British Cult Movies Since the Sixties
| date                  =
| year                  =2001
| month                 =
| publisher             =Fourth Estate
| isbn                  =1-84115-203-X
| oclc                  =
| page                  =147
| quote                 =Bronco Bullfrog is as close to pure Mod poetry as youre going to get and its a crying shame that this masterpiece has only been seen by a handful of those in the know.
}} 

==Home Media==

The film has been released in the BFI Flipside series dual format edition (DVD and Blu-ray), with other films (such as 1975s Seven Green Bottles, and Platts-Mills 1968 film Everybodys an actor, Shakespeare said) as extras.

A new HD version of the film opened the ninth East End Film Festival on 22 April 2010, prior to its re-release in summer 2010. 

==References==
 

==External links==
* 
* 
*  
*  from Modculture.co.uk
 

 
 
 
 
 
 
 
 
 
 

 