Quiet Days in Clichy (film)
 Quiet Days in Clichy}}

{{Infobox film
| name           = Quiet Days in Clichy
| image          = Quiet Days in Clichy.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Claude Chabrol
| producer       = Pietro Innocenzi Antonio Passalia
| screenplay     = Claude Chabrol Ugo Leonzio	  Quiet Days in Clichy   by Henry Miller
| narrator       = 
| starring       = Andrew McCarthy Nigel Havers Barbara De Rossi
| music          = Jean-Michel Bernard Luigi Ceccarelli Matthieu Chabrol	  
| cinematography = Jean Rabier
| editing        = Monique Fardoulis
| studio         = AZ Film Production Cinecittà Italfrance Films Direkt-Film Istituto Luce Cofimage 2 
| distributor    = 
| released       = 9 May 1990 	
| runtime        = 120 min.
| country        = France Italy Germany
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Quiet Days in Clichy by Henry Miller.   

The novel was previously adapted into a 1970 Danish film.

==Plot==
American Henry Miller enjoys a wide variety of sexual escapades while also working hard to establish himself as a serious writer in Paris.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Andrew McCarthy || Henry Miller, aka Joey  
|-
| Nigel Havers || Alfred Perlès, aka Karl  
|-
| Barbara De Rossi || Nys  
|-
| Stéphanie Cotta || Colette Ducarouge   
|-
| Isolde Barth || Ania Regentag  
|-
| Anna Galiena || Edith  
|-
| Stéphane Audran || Adrienne  
|-
| Mario Adorf || Regentag
|-
| Margit Evelyn Newton || Bernadette
|-
| Eva Grimaldi || Yvonne
|}

==References==
 

== External links ==
* 
* 

 

 
 
 
 

 