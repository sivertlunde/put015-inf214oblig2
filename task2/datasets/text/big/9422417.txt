Nagarahavu (2002 film)
 
{{Infobox film
| name           = Naagara Havu
| image          =
| image_size     = 
| caption        = Film poster
| director       = S Murali Mohan
| producer       = Rockline Venkatesh
| writer         = 
| screenplay     = 
| narrator       =  Ambika  Sadhu Kokila
| music          = Hamsalekha
| lyrics         = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2002
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
}}
 Kannada Psychological Bollywood film South Filmfare Udaya Film Award for Best Male Actor for the second time.

==Plot==
Based on Ira Levins 1953 novel A Kiss Before Dying, it is a contemporary thriller about revenge. The main character is Ajay Sharma (Upendra). His father, Vishwanath Sharma (Sadhu Kokila), once owner of a great business empire,  was defrauded by Madan Chopra (Dalip Tahil), a trusted employee in Vishwanaths company. The Sharma family is ousted from their own company and loses everything they owned. Soon afterwards, Ajays father and his young baby sister die due to illness, unable to buy medicine because of their destitute state. His mother is now suffering from mental illness and memory loss. Driven over the edge, Ajay becomes obsessed with killing Chopra and destroying his family.

Ajay begins dating Seema Chopra (Jyothika), the daughter of the owner of Chopra business empire. They meet secretly as her father would not approve of a poor son-in-law. Meanwhile, the younger daughter Priya Chopra (Jyothika, dual role) travels with her father Madan Chopra to Madras (now Chennai), for Madans final kart race before he retires permanently. Madan has never lost before, but comes across Vicky Malhotra (Ajay Sharma in a disguise of brown contact lenses). Vicky allows Madan to win by slowing at the last corner and tells him that he couldnt beat his "guru". Vicky then charms Priya by saying that he lost the race as he couldnt break the heart of a beautiful girl. Thus his ploy of winning in spite of losing (meaning Baazigar) succeeds as he wins Priyas heart. This way, he manages to date both Seema and Priya simultaneously using different identities.

Ajay is (incidentally) photographed lurking outside the birthday party of Seema by one of her friends. Later Madan Chopra arranges for Seema to be married off to another business family. Seema is heartbroken, and Ajay decides that they will write identical suicide notes and commit suicide. After they each write their suicide notes, he tells her that the suicide note was just a test. He says that only cowards commit suicide and destroys his note, while keeping Seemas. They decide to marry secretly the next day. When they arrive, the registrar is closed, so they go up to the roof of the building for sightseeing. He makes her sit on the parapet and tells her the truth about himself, after which he throws her to her death from the rooftop of the building. He then posts the suicide note and leaves, but arrives later (as Vicky) with Priya. He helps the Chopra family with the funeral. The letter written by Seema implies that she has committed suicide, and the murder investigation is closed. Priya cannot believe that, and asks her father to re-open the case. However, he disagrees, saying he doesnt want to lose his reputation due to Seemas affair. Priya then asks Karan (Siddharth), who is her former classmate and a current police inspector, for help.

Priya tries to investigate Seemas death, however her attempts prove unsuccessful. Ravi, Seemas college friend, offers to help her, but is brutally killed by Vicky. As Vicky forces him to sign a suicide note before hanging him, Karan thinks that Ravi must have been in love with Seema and hence her murderer. Later, Priya and Vicky meet Seemas college friend, Anjali, who becomes suspicious of him. When she finds out that he was Seemas boyfriend back in college, she calls the Sharma household during Vickys and Priyas engagement party. Vicky intercepts the phone and impersonates Chopra, and then arrives at her place. He strangles her, then fits her body in a suitcase and throws it in the river.

Vicky vows to destroy Madan Chopra, confiscate his assets and throw him out of his office and onto the street, just like Chopra did many years ago to Ajays father. Chopra needs to go on a business trip, and knowing that Vickys his soon to be son-in-law, gives Vicky the Power of attorney. Vicky does a carbon copy of the previous montage of Chopra, and within days seizes everything. One day in a club Vicky takes Priya to calm her mind, where they meet Ajays childhood friend, who greets him as Ajay. While Vicky tries to ignore him, Priya tells the man that he is mistaken, and that Ajay is Vicky Malhotra. Ajays friend repeatedly tries to remind Vicky (who is actually Ajay) about their friendship. Vicky feels trapped and tries to get out of the mess by abusing him and gets in a scuffle with him. Priya visits Ajays friend and the whole truth is revealed to her.

When Chopra returns, Vicky reveals himself as Vishwanath Sharmas son Ajay and gives him the exact sendoff that Chopra did 15–20 years ago. Priya finds Ajays home address, and finds out his real identity. Ajay comes in and they argue about Seemas murder. Ajay wins her over by telling her the history of their two families. Madan charges in with a group of thugs and shoots Ajay in the shoulder before he is severely beaten and wounded. Ajays Mother is then knocked unconscious trying to defend Ajay. This prompts him to recover and fight Madan and his gang all alone. Chopra eventually picks up a stake and mortally stabs Ajay. However, Ajay manages to impale him with the same stake by walking into him, and they jump off a small cliff. Chopra dies, and Ajay (knowing that he is dying) manages to stagger back to his mother. He tells her that he has reclaimed what is rightfully hers. Mrs. Sharma is cured of her illness, but is beset by sorrow, along with Priya crying beside her, as Ajay dies in their arms.

==Cast==
*Upendra – Ajay Sharma
*Jyothika – Keerthi & Prema (dual roles) Ambika
*Sadhu Kokila
*Doddanna

==Nagarahavu Cricket Cup== Kannada film Industry like Vishnuvardhan (actor)|Vishnuvardhan, Ambarish, Shivrajkumar, Puneeth Rajkumar, Devaraj, Ramesh Arvind, Bharathi Vishnuvardhan, Anu Prabhakar, Tara (Kannada actress)|Tara, and many others  took part in the match, which was attended by thousands of fans. 

==Sound Track==
The music for the movie was composed by Hamsalekha.
{| class="wikitable sortable"
|-
! Title !! Singers
|-
| Yaking Adthiye || Upendra, Anuradha Sriram
|-
| Havina Dwesha || S. P. Balasubrahmanyam
|-
| Edhe Chippinalli || Rajesh Krishnan, Chithra
|-
| I Want to say || Rajesh Krishnan, Anuradha Sriram
|-
| Ammage Bitte ||  S. P. Balasubrahmanyam 
|-
| Kolegara || Rajesh Krishnan
|}

==Box Office==
The film was an Above Average Success at the box office. 

==Awards== Udaya Film Award for Best Male Actor – Upendra Udaya Film Award for Best Female Actor – Jyothika

==References==
 

 
 
 
 
 