Sniper (1993 film)
{{Infobox film
|  name           = Sniper
|  image          = Sniper poster.jpg
|  caption        = Theatrical release poster
|  producer       = Robert L. Rosen
|  director       = Luis Llosa
|  writer         = Michael Frost Beckner Crash Leyland |
|  starring       = Tom Berenger Billy Zane J. T. Walsh
|  music          = Gary Chang Bill Butler
|  editing        = M. Scott Smith
|  distributor    = TriStar Pictures
|  released       =  
|  runtime        = 98 minutes
|  gross          = $18,994,653   
|  country        = United States
|  language       = English
}}
Sniper is a 1993 American  ,  , and  .

== Plot ==
Master Gunnery Sergeant Thomas Beckett, an experienced sniper, and his spotter Cpl. Papich are on a mission to assassinate a Panamanian rebel leader in the jungle; however, they are extracted in daylight instead of night time, which results in Papichs getting killed by a sniper. Beckett runs back under fire to haul Papichs body to the helicopter. Later, Beckett is paired up with an inexperienced civilian, Richard Miller, to eliminate a rebel general financed by a Colombian drug lord. Miller is a SWAT sharpshooter, but he has no combat experience or confirmed kills. On the way to the staging area, Millers chopper is attacked by a guerrilla, who kills several members of the crew. Miller is unable to take out the attacker; instead, the choppers dying gunner makes the kill. The co-pilot believes Miller is responsible, and Miller earns a false reputation.

Becketts insistence that they deviate from the mission plan and belief that Miller is unfit for command sparks friction. Early on, they encounter a group of Indians, who lead them past rebel guerrillas in return for a favor and a target of opportunity: to eliminate El Cirujano ("The Surgeon"), an ex-CIA torture expert who works with the rebels. Beckett agrees to do so. Not certain of Millers reliability and skeptical about his "kill"  aboard the helicopter, Beckett expects Miller to make the kill to prove himself. When it comes time, Miller fails again by making a "warning shot" before missing a head shot at Cirujano. The ensuing firefight with the alarmed guerrillas results in the death of one of the Indians. Although the Indians do not directly blame Beckett or Miller, they withdraw further help.

On their way to their target, Beckett and Miller realize they are being followed. They head to a village to contact their informant, a priest, only to find him already tortured and murdered. Beckett speculates out loud that it is the work of El Cirujano, calling into question Millers credibility. That night, Beckett marks their track to bait the follower—the sniper that killed Papich—into a trap and uses Miller as a bait to pull the sniper out of hiding. While waiting for the targets to emerge at the generals hacienda, they discover Cirujano is still alive. Miller takes out the drug lord, but Beckett sacrifices his chance to take out the general when he saves Millers life. Beckett insists that they return to kill the general, but Millers refusal leads to an exchange of fire between the two that ends when Miller runs out of ammo. Miller subsequently has a mental breakdown.

As rebels close in on the two, Beckett attempts to provide covering fire for Miller. Seeing himself outnumbered, Beckett surrenders to the rebels. Knowing that Miller is watching, he stealthily releases a round while holding up his rifle. Miller grabs the bullet after Beckett is taken away. With night time approaching, Miller goes to the extraction site, but instead of boarding, he heads to the base camp, where he kills the general with his knife and finds Beckett being tortured by El Cirujano. Beckett spots Miller in the distance and uses a ploy to both distract Cirujano and mouth Miller instruction to kill both of them with one shot. Instead, Miller sticks to "one shot, one kill" and shoots Cirujano in the head. The two run to the helicopter for extraction, and Beckett once again saves Millers life: using his offhand, he shoots an ambushing sniper. The final scene shows Beckett and Miller on the way back home.

== Cast ==
* Tom Berenger as Thomas Beckett
* Billy Zane as Richard Miller
* J. T. Walsh as Chester van Damme
* Aden Young as Doug Papich
* Ken Radley as El Cirujano
* Frederick Miragliotta as General Miguel Alavarez

== Production ==
Billy Zane was cast after his starring role in Dead Calm raised his profile.  Director Luis Llosa, who grew up watching American films, called modern films "cartoonish and antiseptic" in their depiction of violence; he said that he wanted to bring back a sense of impact to killing.   It was shot Queensland, Australia. 

== Release ==
Sniper was held back from release in 1992.   It debuted at number two at the box office  on January 29, 1993, in 1551 theaters and went on to gross $18,994,653 domestically.   TriStar released it on VHS in August 1993,  LaserDisc in September 1993,  and on DVD in October 1998. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 42% of twelve surveyed critics gave the film a positive review; the average rating was 4.7/10.   Roger Ebert rated it 3/4 stars and wrote, "Sniper expresses a cool competence that is a pleasure to watch. It isnt a particularly original film, but what it does, it does well."   Variety (magazine)|Variety called it "an expertly directed, yet ultimately unsatisfying psychological thriller" that is "undermined by underdeveloped characters and pedestrian dialogue."     Vincent Canby of The New York Times described it as "partly a badly choreographed action drama and partly a psychological exploration of Becketts mind, which comes up empty."   Michael Wilmington of the Los Angeles Times called it a shallow film that does not explore the themes suggested by the script and instead turns into a bloodless, macho video game.   Clifford Terry of the Chicago Tribune called it a formulaic male-bonding drama that features a Hollywood odd-coupling.   Richard Harrington of the Washington Post criticized the lack of character progression and the implausible conclusion.   Stephen Wigler of The Baltimore Sun called it a "poorly written, badly directed film" that substitutes violence for sex.   Marjorie Baumgarten of the Austin Chronicle rated it 3.5/5 stars and wrote, "Sniper does little thats terribly original but that which it does, it does with great competence and grace." 

== Film series ==
 
Sniper spawned four sequels: the TV movie   in 2011, and   in 2014. ...Reloaded features Billy Zanes role of Richard Miller from the first Sniper film reprised, having himself become a sniper as a result of his Panamanian tour experience with Thomas Beckett.

{| class="wikitable" style="text-align:center" width=99%

! rowspan="2" width="20%" | Character
! colspan="5" align="center" | Film
|-
! align="center" width="15%" | Sniper
! align="center" width="15%" | Sniper 2
! align="center" width="15%" | Sniper 3
! align="center" width="15%" |  
! align="center" width="15%" |  
|-
! Thomas Beckett
| colspan="3" | Tom Berenger
| colspan="1" style="background:lightgrey;" | &nbsp;
| colspan="4" | Tom Berenger
|-
! Richard Miller
| Billy Zane
| colspan="2" style="background:lightgrey;" | &nbsp;
| Billy Zane
| colspan="1" style="background:lightgrey;" | &nbsp;
|-
! Brandon Beckett
| colspan="3" style="background:lightgrey;" | &nbsp;
| colspan="2" | Chad Michael Collins
|}

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 