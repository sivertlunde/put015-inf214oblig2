Out of Time (2003 film)
{{multiple issues|
 
 
}}
{{Infobox film
| name = Out of Time
| image = Out of timeposter.jpg
| caption = Theatrical release poster
| director = Carl Franklin
| producer = Jesse Beaton Damien Saccani Neal H. Moritz
| writer = David Collard
| starring = Denzel Washington Eva Mendes
| music = Graeme Revell
| cinematography = Theo van de Sande
| editing = Carole Kravetz
| studio = Original Film
| distributor = Metro-Goldwyn-Mayer 
| released =  
| runtime = 105 minutes
| country = United States
| language = English
| budget = $50 million 
| gross = $55.5 million   
}}
Out of Time is a 2003 American thriller film, directed by Carl Franklin featuring Denzel Washington.

==Plot==
Matthias "Matt" Lee Whitlock (Denzel Washington) is the respected Chief of Police of the fictional small town of Banyan Key, Florida. He has recently executed a successful, high-profile drug bust that turned up $450,000 in drug money.

Although he enjoys his job, his drinking while on duty is an obvious character flaw, exacerbated by his pending divorce from his wife, homicide detective Alex Diaz Whitlock (Eva Mendes).  Matt is currently seeing local resident Anne Merai-Harrison (Sanaa Lathan) &ndash; whose husband Chris (Dean Cain) a former professional quarterback turned security guard, abuses her.

Matts friend, Chae (John Billingsley) a medical examiner jokingly wants them to use the $450,000 to go into business together. Matt, however finds out that Anne has just been diagnosed with terminal cancer.

Anne intends to reward Matts loyalty to her by making him the sole beneficiary of her $1,000,000 life insurance policy. Matt tries to find a way to give Anne some hope and suggests that she should travel to Switzerland to undergo a newly developed treatment. The problem is that Anne has been living beyond her means and does not have any money. Desperate to help her, Matt bends the rules and loans her the $450,000 so she can make the trip.

When the $450,000 goes up in flames in a suspicious house fire that kills Anne and Chris, Matt finds himself in a great deal of trouble. Not only is the money now gone, but Matt knows that even though he is innocent, circumstantial evidence &ndash; such as the insurance policy and the fact that he had been seeing Anne &ndash; could make him the prime suspect.

With the Drug Enforcement Administration agents soon arriving to pick up the now lost cash and Alex serving as the lead detective of the homicide investigation, Matt races to stay ahead of his own police force and everyone whom he has trusted. He has to prevent anyone from discovering his connections with Anne so that he can figure out what really happened before he finds himself out of time.

==Cast==
*Denzel Washington as Matthias Lee Whitlock
*Eva Mendes as Alex Diaz-Whitlock
*Sanaa Lathan as Anne Merai-Harrison
*Dean Cain as Chris Harrison
*John Billingsley as Chae Robert Baker as Tony Dalton Alex Carter as Paul Cabot
*Antoni Corone as Deputy Baste
*Terry Loughlin as Agent Stark
*Nora Dunn as Dr. Donovan
*James Murtaugh as Dr. Frieland
*O. L. Duke as Detective Bronze
*Tom Hillmann as Robert Guillette, Living Gift Salesman

==Awards==
 
2004 Black Reel Awards
* Won - Best Theatrical Film
* Nominated - Best Actor — Denzel Washington
* Won - Best Actress — Sanaa Lathan
* Nominated - Best Director — Carl Franklin Image Awards Outstanding Actor in a Motion Picture — Denzel Washington Outstanding Supporting Actress in a Motion Picture — Sanaa Lathan

==Reception==
The film received a score of 65% on Rotten Tomatoes based on 146 reviews, with an average rating of 6.4 out of 10. The critical consensus states the film is a: "A fun and stylish thriller if you can get past the contrivances.." 

==Mobile phone game==
  WAP and Java mobile O2 and Momentum Pictures by  .

==MPAA rating==
 
The film was originally rated R but was later re-rated PG-13 by the MPAA for "sexual content, violence and some language". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 