Kaos (film)
 
{{Infobox film
| name           = Kaos
| image	         = Kaos FilmPoster.jpeg
| caption        = Film poster
| director       = Paolo and Vittorio Taviani
| producer       = Giuliani G. De Negri
| writer         = Luigi Pirandello Paolo and Vittorio Taviani Tonino Guerra
| starring       = Margarita Lozano
| music          = 
| cinematography = Giuseppe Lanci
| editing        = Roberto Perpignani
| distributor    = 
| released       =  
| runtime        = 188 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Girgenti (Agrigento), on the southern coast of  Sicily, as deriving from the ancient Greek word kaos.

==Plot==
The film depicts four short stories from Pirandellos 15-volume series Novelle per un anno, which play around his birthplace in the 19th century. A raven, which in the introduction is shown to get a bell around his neck from locals, leads one from one story to the next.

* L’altro figlio ("The Other Son") is about a mother whose two sons have emigrated to the United States. She hasnt heard from them since (14 years ago), but she still favors them over a third son who has stayed behind and tries to help and please his mother. The reason for her completely shunning him are explained in flashbacks to events in 1848.  
* Mal di luna ("Moonsickness") Three weeks after their honeymoon, Sidora discovers that, during the full moon, her husband Batà spends the night howling outside like a werewolf and scratching to get back in. Batà tries to save his marriage by allowing her to have the handsome Saro spend the full-moon nights at their place to protect Sidora.
* La giara ("The Jar") In this comedic section, a feudal landlord (Don Lollò) orders a very large jar for his olive oil, but the new jar breaks almost immediately under mysterious circumstances. The great "jar-fixer" Zi Dima, famous for his secret-recipe glue, is called to repair the jar, but Zi Dima manages to fix the jar with himself in it and Don Lollò refuses to break the jar again to let him out.
* Requiem Motivated by the imminent death of their founding father, farmers in a remote hamlet on grounds owned by a baron try to obtain the rights to bury their dead locally rather than in the town, which is over a days hike away. The baron refuses and carabinieri escort the peasants back to their hamlet to break down the beginnings of a graveyard the peasants are building.

An epilogue, Colloquio con la madre ("Conversing with Mother"), of similar length as the stories, describes Pirandellos fictional visit home many years after his mother has died. He asks his mother to retell the story of a trip to Malta she took as a child to visit her exiled father. The ending sequence showed children sliding down the vast slopes of white pumice that flowed into the sea on the island of Lipari.

==Cast==
* Margarita Lozano as Mariagrazia (in "Laltro figlio")
* Claudio Bigagli as Batà (in "Mal di luna")
* Enrica Maria Modugno as Sidora (in "Mal di luna")
* Franco Franchi as Zi Dima (in "La giara")
* Ciccio Ingrassia as Don Lollò (in "La giara")
* Biagio Barone as Salvatore (in "Requiem")
* Omero Antonutti as Luigi Pirandello (in "Colloquio con la madre")
* Regina Bianchi as Pirandellos mother (in "Colloquio con la madre")
* Massimo Bonetti as Saro (in "Mal di luna" and "Colloquio con la madre")

==Awards and reception== Silver Ribbon award for best screenplay. 

The film was very well received by critics, but its length may have prevented wide exposure. In his 2007 book  , M. Owen Lee considered it the best movie to come out locally in 1986  .

==Reviews==
* , October 13, 1985.
*David Denby  , New York Magazine Feb 24, 1986, pp 62-63.
 
==External links==
* 

 

 
 
 
 
 
 
 