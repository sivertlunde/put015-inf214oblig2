Sea Rex
{{Infobox film
| name           = Sea Rex 3D: Journey to a Prehistoric World
| image          = Sea_Rex_3D.jpg
| image_size     = 250px
| caption        = 
| director       = Ronan Chapalain, Pascal Vuong	 	
| producer       = Francois Mantello, Franck Savorgnan, Catherine Vuong, Pascal Vuong 
| starring       = Guillaume Denaiffe, Norbert Ferrer, Chloe Hollings, Richard Rider, Tom Yang 
| writer         = 
| narrator       =  
| editing        = 
| distributor    = 3D Entertainment
| released       = May 14, 2010
| runtime        = 
| language       = English
}}

Sea Rex 3D: Journey to a Prehistoric World is a 3D film released to IMAX theaters in 2010. It was released on Blu-ray and Blu-ray 3D in November, 2011. 

==People==
*Julie: An American teenage blonde girl who discovers the wonders of prehistory.
*Georges Cuvier: A dead comparative anatomist who shows Julie the history of the marine reptiles.

==Animals==

*Quetzalcoatlus
*Parasaurolophus
*Elasmosaurus
*Liopleurodon
*Tanystropheus
*Placochelys
*Rhomaleosaurus
*Ammonoidea|Ammonites
*Leedsichthys
*Mosasaurus
*Ichthyornis
*Cryptoclidus
*Nothosaurus
*Shonisaurus
*Ophthalmosaurus
*Mixosaurus
*Prognathodon
*Cretoxyrhina
*Otodus
*Rhamphorhynchus
*Megalosaurus
*Brachiosaurus
*Dakosaurus
*Stegosaurus
*Kronosaurus

==External links==
* 
*  

==References==
 

 
 
 
 