Behind the Mask: The Rise of Leslie Vernon
 
{{Infobox film
| name = Behind the Mask: The Rise of Leslie Vernon
| image = Behind the mask ver2.jpg
| alt =
| caption = Theatrical release poster
| director = Scott Glosserman
| producer = Scott Glosserman 
| writer = {{plainlist|
* Scott Glosserman
* David J. Stieve
}}
| starring = {{plainlist|
* Nathan Baesel
* Robert Englund
* Angela Goethals
* Kate Lang Johnson Scott Wilson
* Zelda Rubinstein
}}
| music = Gordy Haab
| cinematography = Jaron Presant
| editing = Sean Presant
| studio = {{plainlist| GlenEcho Entertainment
* Code Entertainment
}}
| distributor = Anchor Bay Entertainment
| released =  
| runtime = 92 minutes
| country = United States
| language = English
| budget =
| gross = $69,136 
}}
Behind the Mask: The Rise of Leslie Vernon is a 2006 American mockumentary black comedy horror film directed by Scott Glosserman. It stars Nathan Baesel, Angela Goethals and Robert Englund. Although largely filmed in Oregon, the film takes place in a small town in Maryland, and follows a journalist and her film crew that is documenting an aspiring serial killer who models himself according to slasher film conventions.

The film is an homage to the slasher film genre and features cameos from several veteran horror actors, including Robert Englund, Zelda Rubinstein, and Kane Hodder. The film premiered at the 2006 South by Southwest film festival and was shown at several other festivals. It received a limited release in the United States on March 16, 2007.

==Plot==
The film is shot as a documentary set in a world where the killers depicted in famous slasher films are real. A female journalist named Taylor Gentry and her two cameramen, Doug and Todd, document the preparations of Leslie Vernon as he prepares to join the ranks of other slasher villains. Leslie takes his identity from an urban legend about a boy who killed his family and was cast into a river by angry townsfolk.
 survivor girl", Kelly. Taylor and her crew come to share Leslies enthusiasm for his project, but their consciences catch up with them on the night of the murders.

They beg Leslie to call off his killing spree, but Leslie is adamant, believing that his survivor girl will define herself by facing him. Taylor and her crew abandon their documentary and at this point the film shifts from a documentary style to a traditional horror film presentation. Taylor attempts to warn and rally the remaining teens together to fight Leslie, but Leslies preparations repeatedly give him the upper hand. The group looks to Kelly for leadership, but she unexpectedly dies.

Taylor quickly realizes that, as a virgin herself, she was Leslies true survivor girl all along. Leslie continues picking off the group one-by-one until only Taylor remains. She faces Leslie and defeats him in the exact manner he had laid out for her, then burns down the shed in which he was defeated. She then runs into Doug and Doc Halloran who survived their encounters with Leslie. However, Leslies preparations included learning to feign death and slathering himself with flame-retardant gel.

Over the final credits, security camera footage reveals Leslies charred body sitting up on an autopsy table, still alive, accompanied by the song "Psycho Killer" by The Talking Heads.

==Cast==
 
* Nathan Baesel as Leslie Vernon/Mancuso
* Robert Englund as Doc Halloran
* Angela Goethals as Taylor Gentry
* Kate Lang Johnson as Kelly Scott Wilson as Eugene
* Zelda Rubinstein as Mrs. Collinwood
* Bridgett Newton as Jamie
* Ben Pace as Doug
* Britain Spellings as Todd
* Hart Turner as Shane
* Krissy Carlson as Lauren
* Travis Zariwny as Dr. Meuller
* Teo Gomez as "Stoned Guy"
* Matt Bolt as "Slightly More Stoned Guy"
* Anthony Forsyth as "Pitch-forked Boyfriend"
* Kane Hodder as "Autopsy Guy"
 

==Production==
Behind the Mask was filmed largely in Portland, Oregon and the outlying towns of Troutdale, Oregon|Troutdale, Banks, Oregon|Banks, St. Helens, Oregon|St. Helens, Estacada, Oregon|Estacada, and Sauvie Island. Making of Behind the Mask (DVD) 2007, Anchor Bay Entertainment  The establishing shots of the fictional town of Glen Echo were filmed on Main Street in downtown Troutdale. Filming took place in November 2004.

Due to the limited budget and location restrictions, some of the script was re-written during filming to accommodate the filming locations.  During filming, several locations needed for certain scenes had yet to be scouted and were discovered in the middle of shooting.
 Scott Wilson became involved in the film after a suggestion from friend Robert Englund, who was already cast in the film. 
 editors on the film after seeing and being impressed by an independent film they had produced called The Black Shoe Drifter. 

==Release== limitedly in the United States in March 2007. 

==Critical reception==
Behind the Mask was generally well received by film critics. Review aggregator website  , the film has a 66/100 rating based on 14 critics, indicating "generally positive reviews". 

Elizabeth Weitzman of the New York Daily News called the film "a must for those who like thrills laced with a sense of humor" and noted the films deadpan sense of humor.  James Berardinelli said that it "provides a fresh, chilling breeze through the stale air of the crypt that has become multiplex horror", and awarded it three out of four stars,  while the Los Angeles Times referred to it as "original and weirdly delicious, and executed with gory aplomb." 

Stephen Hunter of The Washington Post said the films "breakdown of cliches is vivid and witty", noting that its intended audience was "genre deconstructionists" and "smart young people who have studied horror/slasher movies and enjoy them for their vulgar energy."    Hunter also called the performances of Baesal and Goethals "brilliant". 

Contrarily,   franchise before devouring its own tail, proving that you are what you eat." 

===Accolades===
{|class="wikitable" style="font-size: 100%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! style="background:#B0C4DE;" | Year
! style="background:#B0C4DE;" | Award
! style="background:#B0C4DE;" | Organization
! style="background:#B0C4DE;" | Category
! style="background:#B0C4DE;" | Result
|- 2006
|Best European/North - South American Film
|rowspan="2"|Fant-Asia Film Festival Golden Prize Won   
|-
|L’Écran Fantastique Award Golden Prize Won 
|-
|Séquences Award
|Séquences magazine Jury Prize Won 
|- Audience Award Gen Art Gen Art Film Festival Won 
|- Carnet Jove - Special Mention Sitges - Catalonian International Film Festival Midnight X-Treme Won 
|- Audience Choice Award For Best Feature Film Toronto After Dark Film Festival Best Film Won 
|-
|}

Tyler Doupe of Fearnet included Leslie Vernon in his list of "Ten of Horrors Most Disarming Psychopaths". 

==Sequel==
In an interview David J. Stieve, when asked if he was working on new scripts, said,

 

Baesel, when asked about the possibility of returning for a sequel to Behind the Mask, stated, "  there’s a certain chance. I know that ideas have already been circulating around Scott and David’s heads and I’d love to take Leslie on again. However, I don’t think we’d undertake a sequel unless the script is as good or better than Behind the Mask. The first was so good it would be pissing on Leslie’s legacy to set out with anything less than inspired ... and Leslie would never have that." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 