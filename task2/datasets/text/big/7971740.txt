Below Zero (1930 film)
 
{{Infobox film
| name           = Below Zero
| image          =Belowzerotitlecard.jpg
| caption        =
| director       = James Parrott
| producer       = Hal Roach
| writer         = H.M. Walker
| narrator       =
| starring       = Stan Laurel Oliver Hardy
| music          =
| cinematography = George Stevens
| editing        = Richard C. Currier
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 20 36" (English) 28 19" (Spanish)
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
Below Zero is a 1930 short film starring Laurel and Hardy, directed by James Parrott and produced by Hal Roach. The film was the first to use some background music in a portion of the film and had a few early Leroy Shield pieces. Still the film did not feature a substantial amount of music. 

==Plot==
Laurel and Hardy have no success earning money on a bleak, snowy winters day, especially when playing In the Good Old Summertime. Their instruments are destroyed in an argument with a woman, but Stan finds a wallet. They are chased by a thief, but are protected by a police officer. Stan and Ollie share a slap-up meal with the cop, but unfortunately, Stan finds out the wallet in fact belongs to the cop. When the policeman discovers this he tells the waiter who throws them out of the restaurant and throws Stan upside down in a barrel of water. Oliver finds Stan who now has an enormous stomach after drinking all the water while trapped in the barrel.

==Cast==
* Stan Laurel
* Oliver Hardy Frank Holliday Charlie Hall
* Tiny Sandford
* Leo Willis
* Ed Furlong

==Spanish version==
An extended Spanish version, Tiembla Y Titubea was also produced with Laurel and Hardy speaking phonetically from blackboards placed just out of camera range; Spanish-speaking actors replacing the English supporting players.

==References==
 

==External links==
*  
* 

 
 

 
 
 
 
 
 
 
 
 

 