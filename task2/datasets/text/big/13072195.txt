Leave Me Alone (2004 thriller film)
 
 
{{Infobox film
| name           = Leave Me Alone
| image          = Leave_Me_Alone_DVD.jpg
| caption        = The Hong Kong DVD cover. Danny Pang
| producer       = The Pang Brothers Danny Pang
| starring       = Ekin Cheng Charlene Choi
| music          = Jadet Chawang Payont Permsith	
| editing        = Curran Pang
| distributor    = Universe Laser (DVD)
| released       =  
| runtime        = 93 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
}} Danny Pang. It stars Ekin Cheng in a dual role.

==Plot== Gay fashion designer Yiu Chun Man (Ekin Cheng) is visited in Hong Kong by his straight twin brother, Yiu Chun Kit (also Ekin Cheng). Kit borrows his brother’s driver’s license, and is then involved in a car crash in which a woman dies (see also Ab-normal Beauty), and Kit falls into a coma.

With no ID card, Man is unable to prove his identity, so he assumes the identity of his brother, and takes up with Kit’s girlfriend, Jane, (Charlene Choi), and goes with her to Thailand. Jane, however, is having some money problems, and is deeply indebted to a loan shark (Dayo Wong), who pursues Man and Jane.

Kit comes out of his coma and finds himself struggling to fend off the amorous advances of Mans boyfriend (Jan Lamb), who is a high-ranking Hong Kong police officer.

==Production and release==
Linked by the same car accident, Leave Me Alone is a companion piece to Ab-normal Beauty, directed by Oxide Pang, which was also released in Hong Kong cinemas in November 2004.

Leave Me Alone was screened at the Deauville Asian Film Festival and the Tokyo International Film Festival.
 all region) by Universe Laser on 8 January 2005.

==External links==
*  

 

 
 
 
 
 
 
 
 

 
 