Seiryū no dōkutsu
{{Infobox Film
| name           = Seiryū no dōkutsu 青竜の洞窟
| image          = Seiryu no dokutsu poster.jpg
| caption        = Japanese movie poster Shigeyoshi Suzuki Daiei
| writer         = Hitomi Takagaki (novel)
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Daiei Film
| distributor    = 
| released       = February 5, 1956 
| runtime        = 42 min.
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| 
}} Japanese film Shigeyoshi Suzuki. The film is based on the novel by Hitomi Takagaki.

== Cast ==
* Yoshirō Kitahara (北原義郎)
* Yoshiko Fujita Yoshihiro Hamaguchi

== See also ==
* lit. Eye of the Jaguar (豹の眼), 1956 film

== References ==
 

== External links ==
*   http://www2u.biglobe.ne.jp/~kazu60/zanmai09/retoro01.htm

 
 
 
 
 


 