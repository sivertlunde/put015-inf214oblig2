Double Exposure (1994 film)
{{Infobox Film
| name           = Double Exposure
| image          = 
| caption        = 
| director       = Claudia Hoover Samuel Benedict Scott Wiseman (executive producer)
| writer         = Christine Colfer Bridget Hoffman Claudia Hoover	 	
| starring       = Ron Perlman Ian Buchanan Dedee Pfeiffer
| music          = Paolo Rustichelli
| cinematography = John J. Connor
| editing        = Thomas Meshelski
| distributor    = Prism Entertainment Corporation Hellas Cosmos Video
| studio         = Falcon Arts & Entertainment Joey Walker
| released       = April 16, 1994
| runtime        = 93 min.
| country        = United States
| awards         =
| language       = English
| budget         = 
}}
Double Exposure is a 1994 crime drama, starring Ron Perlman, Ian Buchanan, and Dedee Pfeiffer.  

==Plot==
The Putnams, Roger (Ian Buchanan) and Maria (Jennifer Gatti), continue an unstable marriage. She is unhappy at his obsessiveness and possessiveness and he continually suspects her of having an affair. On Tuesday and Thursday nights Maria is allowed out for a gym workout with their mutual friend and his work colleague, Linda (Dedee Pfeiffer) but she gets home so late that he presumes she is up to something behind his back. He hires private detective, John McClure (Ron Perlman) to find out if she is cheating and with whom. Meanwhile Linda discovers that Roger has been skimming from the company they work for. McClure brings evidence to Roger that his wife is indeed having an affair. Roger wants Marias lover killed and asks McClure to help him. McClure requests twenty five thousand dollars and says that he knows a man who knows a man -but is really planning to do it himself because hes broke and is being blackmailed by an old friend. The plan is screwed up when McClure shoots blindly into the hotel room and shoots Maria instead of her lover. Enter a young homicide investigator (William R. Moses) and a forensic scientist who knows how to analyze a nanogram of dog excrement and human vomit to discover the actual identity of the killers and the lover.

==Cast==
*Ron Perlman as John McClure
*Ian Buchanan as Roger Putnam
*Dedee Pfeiffer as Linda Mack
*Jennifer Gatti as Maria Putnam
*William R. Moses as Detective Joiner
*James McEachin as Detective Becker
*Bridget Hoffman as Ruby Marlowe

==References==
 

 
 
 
 
 
 


 