Mohabbatein
 
{{Infobox film
| name           = Mohabbatein
| image          = Mohabbatein.jpg
| director       = Aditya Chopra
| producer       = Yash Chopra
| writer         = Aditya Chopra
| starring       = Shahrukh Khan Amitabh Bachchan Aishwarya Rai Uday Chopra Jugal Hansraj Jimmy Shergill Shamita Shetty Kim Sharma Preeti Jhangiani
| music          = Jatin-Lalit Manmohan Singh
| editing        = V.V. Karnik Singh Taranjeet
| distributor    = Yash Raj Films
| released       = 27 October 2000
| runtime        = 216 minutes
| country        = India
| language       = Hindi
| budget         =   
| gross          =   
| preceded_by    = 
| followed_by    = 
}}
 musical romantic drama film directed by Aditya Chopra. It was Chopras second directorial venture after Dilwale Dulhania Le Jayenge and was filmed at India and the United Kingdom. The film stars Shahrukh Khan and Amitabh Bachchan in pivotal roles, along with six young debutantes. Aishwarya Rai appeared in flashbacks as Khans lover. The films soundtrack was composed by Jatin-Lalit, while the lyrics were penned by Anand Bakshi. The film is notable for being the first time that Bachchan and Khan appeared on-screen together.

The film went on to do well both critically and commercially. It became the second highest grossing film of the year domestically and the highest grossing film overseas that year and was thus declared a blockbuster.    It also won several awards including the Filmfare Critics Award for Best Actor and the Filmfare Award for Best Supporting Actor, given to Khan and Bachchan.

==Plot==

Narayan Shankar ( ), Vicky (Uday Chopra) and Karan (Jimmy Shergill) who all fall in love. Sameer is in love with Sanjana (Kim Sharma), his childhood friend. Vicky falls for Ishika (Shamita Shetty), a student at the all-girls university next door to Gurukul. Karan becomes smitten with Kiran (Preeti Jhangiani), a girl he sees one night at a train station. All three know that if theyre caught theyd be expelled, so they at first do not pursue their love interests.

Raj Aryan ( ) is dead, but he continues to love her and imagines that she is still by his side. Raj encourages the boys to continue to stay loyal to their loves.

One day Raj throws a party in Gurukul, inviting the students from the girls school from next door as part of his plan to spread love in Gurukul. Narayan is furious and threatens to fire him. Raj then reveals he was a student at Gurukul years ago, and he fell in love with Megha, who was Narayans only daughter. Narayan had Raj expelled without ever seeing his face, and afterward Megha committed suicide. Raj returned to Gurukul as a teacher in the hopes of changing Narayans ways and honoring his love for Megha. Raj promises that before he leaves Gurukul, he will fill it with so much love that even Narayan will not be able to remove it. Narayan accepts this challenge, and allows Raj to continue as a teacher for a while longer. 

Sameer, Vicky, and Karan one by one win over the girls they love. Narayan retaliates by tightening the rules of Gurukul, but the boys continue to break the rules, which encourages other students to do the same. Narayan finally decides to expel the three, and Raj speaks up on their behalf, telling Narayan that he is the instigator and the boys should not be punished for it. Raj also says that he feels Narayan lost the battle, because his daughter left him and now Raj (who considered Narayan an elder) is leaving him. Rajs words make Narayan realise his mistake. He publicly apologises to the students and steps down as the principal, allowing Raj to take over. Raj turns Gurukul into a love-filled university. In the end Raj, Narayan and a vision of Megha walk happily together towards the gates of Gurukul.

==Cast==
*Amitabh Bachchan as Narayan Shankar
*Shahrukh Khan as Raj Aryan  Malhotra
*Aishwarya Rai as Megha Shankar
*Uday Chopra as Vikram "Vicky" Oberoi/Kapoor
*Jimmy Shergill as Karan Chaudhry
*Jugal Hansraj as Sameer Sharma
*Shamita Shetty as Ishika Dhanrajgir
*Kim Sharma as Sanjana
*Preeti Jhangiani as Kiran
*Amrish Puri as Maj. Gen. Khanna, Kirans father-in-law
*Shefali Shah as Nandini, Kirans sister-in-law
*Parzan Dastur as Ayush, Nandinis son
*Saurabh Shukla as Sanjanas father
*Anupam Kher as Kake
*Archana Puran Singh as Preeto Helen as Miss Monica
*Sindhu Tolani as Ishikas friend
*Rushad Rana as Gurukuls Student, side dancer in Soni Soni & Pairon mein bandhan hain

==Production==
Before the production of the film, Amitabh Bachchan was attempting a revival of his career but it was of little success. Also, he was in financial trouble with the bankruptcy of his production company, ABCL, which was hurting his image. These troubles left him almost penniless, and it was at this time that he contacted his friend and director/producer Yash Chopra, who told him of an upcoming production that he could be in.    With Mohabbatein, he was finally able to shed his "hero" image and to play older, more mature roles.  This was the first film in which Amitabh Bachchan and Shah Rukh Khan appeared together. This created a strong buzz for the film as it brought two huge stars of different generations together.
 Oxford and Cambridge Universities were also used for filming. The scene where Narayan Shankar walks right before meeting Raj Aryan was filmed at the Queens College, Oxford. The railway station scenes were shot at Apta Railway Station, Panvel Maharashtra, India. The scene where Karan asks for friendship from Kiran was shot at Birla Mandir, Revdanda, Alibag, Maharashtra, India. It was rumored that Sridevi was approached to be a love interest to Narayan Shankar, but she turned down the role.  Midway through the filming of the song "Aankhein Khuli", Jugal Hansraj fractured his foot, which required him to wear a cast for the rest of the filming of the song. Aishwarya Rai was slated to only make a guest appearance, but her presence generated so much interest that her role was promoted to that of a regular character. 

==Music==
{{Infobox album  
| Name = Mohabbatein
| Type = Soundtrack
| Artist = Jatin-Lalit
| Cover =  2000
| Recorded =  Feature film soundtrack
| Length = 49:48 YRF Music
| Producer = Yash Chopra
| Reviews = 
| Last album = Dhai Akshar Prem Ke   (2000)
| This album = Mohabbatein (2000)
| Next album = Phir Bhi Dil Hai Hindustani  (2000) 
}}

{{Album ratings
| rev1 = Planet Bollywood
| rev1Score =     
}}

The music of Mohabbatein was composed by Jatin Lalit while lyrics were penned by Anand Bakshi. The album was very well received by the audience. It was the best selling Bollywood soundtrack of the 2000s. 

Music was used extensively throughout the film and features leitmotifs that relate to each of the main characters (each couple having unique theme music).

Avinash Ramchandani of Planet Bollywood gave the album 9.5 stars stating, "Mohabbatein is an outstanding album with several new singers making a splendid debut, as well as a commendable job by Jatin-Lalit, and brilliant lyrics by Anand Bakshi". 

The songs included on the official soundtrack:
{| class="wikitable"
|-
!#
!Song
!Singer(s)
!Length
|-
| 1
|"Humko Humise Chura Lo" Lata Mangeshkar & Udit Narayan
| 07:52
|-
| 2
|"Chalte Chalte" Shweta Pandit, Sonali Bhatawdekar, Pritha Mazumdar, Udhbav, Manohar Shetty & Ishaan
| 07:38
|- 
| 3
|"Pairon Mein Bandhan Hai" Shweta Pandit, Sonali Bhatawdekar, Pritha Mazumdar, Udhbav, Manohar Shetty & Ishaan
| 07:01
|-
| 4
|"Aankhein Khuli" Lata Mangeshkar, Udit Narayan, Shweta Pandit, Sonali Bhatawdekar, Pritha Mazumdar, Udhbav, 
Manohar Shetty, Shah Rukh Khan & Ishaan
| 07:02
|- 
| 5
|"Soni Soni" Udit Narayan, Jaspinder Narula, Shweta Pandit, Sonali Bhatawdekar, Pritha Mazumdar, Udhbav, 
Manohar Shetty & Ishaan
| 09:07
|- 
| 6
|"Chalte Chalte 2" Shweta Pandit, Sonali Bhatawdekar, Pritha Mazumdar, Udhbav, Manohar Shetty
| 02:49
|-
| 7
|"Zinda Rehti Hain Unki Mohabbatein" Lata Mangeshkar & Udit Narayan
| 02:03
|- 
| 8
|"Mohabbatein Love Themes" Instrumental 
| 02:20
|- 
| 9
|"Rhythms of Mohabbatein" Instrumental
| 03:56
|}

==Reception==

===Critical reception===
Mohabbatein received positive reviews from critics. Alok Kumar of Planet Bollywood called it "a feel-good film that takes feeling good to new heights." He wrote, "The film made me care for the characters, something that has not happened since Pooja Bhatt’s brilliant performance in Zakhm or Kajol’s heartwarming performance in Kuch Kuch Hota Hai. Mohabbatein’s lead characters are very well etched and seem real, which is different from most films, whose characters are one-dimensional cardboard cutouts.  Jennifer Hopfinger of thebollywoodticket.com said, "watching Khan and Bachchan go toe-to-toe with grandiose speeches about love and fear—emotions that the two actors have embodied professionally—is ample entertainment."   Savera R Someshwar of Rediff.com commented, "Its a mish-mash alright. But it is also a successful, feelgood film. Youll enjoy yourself as you watch it."  Giving the film 4 out of 5 stars, Tanuka Chakraverty of apunkachoice.com said, "The films highlights are the one-to-one scenes of Amitabh and Shah Rukh. While the former is a veteran and his histrionics now seem to be an extension of his charisma and aura, the latter gives a matured performance. Shah Rukh is finally a controlled performer with Mohabbatein. He takes Amitabh head on and succeeds in giving compelling shots, not wincing even once.  Taran Adarsh of Bollywood Hungama gave it 3 out of 5 stars. He praised the conflict between the lead characters and the three love stories depicted in the film but criticized its excessive length. 

===Box office===
Mohabbatein did very well at the box office. It grossed over   at the worldwide box office.    It also became the second highest grossing Hindi film of the year at the domestic box office and was thus declared a "hit." It was also the highest grossing film in the overseas markets that year. 

== Awards ==

===Filmfare Awards===
* Filmfare Critics Award for Best Actor (Shah Rukh Khan) http://filmfareawards.indiatimes.com/articleshow/368708.cms 
* Filmfare Award for Best Supporting Actor (Amitabh Bachchan)  Best Sound Recording Anuj Mathur Best Scene of the Year - (awarded to the "Climax confrontation" scene between Raj Aryan and Narayan Shankar)

===Other awards===
* IIFA Awards - Best Costume Design (Karan Johar)
* IIFA Awards - Best Story (Aditya Chopra)
* Screen Award - Best Lyrics (Anand Bakshi)
* Sansui Viewer Choice Movie Awards - Best Actor (Shah Rukh Khan)

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 