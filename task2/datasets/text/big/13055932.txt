Four Steps in the Clouds
{{Infobox film
 | name           = Four Steps in the Clouds| image           = Quattro passi fra le nuvole.jpg
 | writer         = Giuseppe Amato Alessandro Blasetti Aldo De Benedetti Piero Tellini Cesare Zavattini
 | starring       = Gino Cervi Adriana Benetti Vittorio De Sica
 | director       = Alessandro Blasetti
 | producer       = Giuseppe Amato
 | cinematography = Václav Vích
 | released   = 23 December 1942 (Italy) 20 November 1948 (U.S.)
 | runtime        = 95 min. Italian
 | music          =Alessandro Cicognini
}} 1942 Italy|Italian drama film directed by Alessandro Blasetti.  Aesthetically, it is close to Italian neorealism. The films sets were designed by Virgilio Marchi.

It was nominated for BAFTA as Best Film from any Source.

==Plot==
The story deals with a married agent for a candy manufacturer, played by Gino Cervi. He leads a stable, if somewhat boring, family life in a large unnamed city in the North of Italy.

While travelling on a South bound train on company business, he sees a young woman about to be put off by the conductor. She has no ticket and cannot afford to buy one. The agent helps her stay on the train, and she asks if he could do one more favour for her. She has just been abandoned by her boyfriend upon becoming pregnant and she is now on her way back to the family farm. She has nowhere else to go but is certain that her father will throw her out as soon as he realizes that she is unmarried.

She is terrified and begs the agent to come home with her and pass himself off as her husband. The deception need only last a couple of days, after which he can go back to his normal life and job and she can claim to have been abandoned. The agent decides that taking a couple of days off work is a small price to pay for saving the girls honour for the rest of her life and gets off the train with her.

Arriving on the farm, the agent finds it hard to maintain the lie, but in an impassioned speech, convinces the girls father to let her stay at home. After which, he goes back to his wife and family without mentioning the incident.

==Cast==
*Gino Cervi ...  Paolo Bianchi 
*Adriana Benetti ...  Maria 
*Giuditta Rissone ...  Clara Bianchi 
*Carlo Romano...  Antonio, lautista della corriera 
*Guido Celano ...  Pasquale - Fratello di Maria 
*Margherita Seglin ...  Luisa, madre di Maria e Pasquale 
*Aldo Silvani...  Luca, padre di Maria e Pasquale 
*Mario Siletti ...  Il fattorino della corriera 
*Oreste Bilancia ...  Il droghiere di Campolo 
*Gildo Bocci...  Il contadino sulla corriera 
*Arturo Bragaglia ...  Il viaggiatore nervoso 
*Anna Carena...  La maestra sulla corriera 
*Pina Gallini ...  Signora Clelia 
*Luciano Manara ...  Il settentrionale 
*Armando Migliari ...  Antonio, il capostazione

==Remakes==
The film was remade several times, as Sous le ciel de Provence in 1956, as A Walk in the Clouds in 1995, as Mungarina Minchu in 1997 and as Dhaai Akshar Prem Ke  in 2000.

==External links==
* 

 

 
 
 
 
 
 
 


 