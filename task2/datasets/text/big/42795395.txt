The Queen of Navarre
{{Infobox film
| name = The Queen of Navarre
| image =
| image_size =
| caption =
| director = Carmine Gallone
| producer = Raffaele Colamonici
| writer =  Eugène Scribe (play)   Gherardo Gherardi    Sergio Amidei   Giacomo De Benedetti    Carmine Gallone
| narrator =
| starring = Elsa Merlini   Gino Cervi   Renato Cialente   Leonardo Cortese 
| music = Franco Casavola 
| cinematography = Arturo Gallea
| editing = Niccolò Lazzari
| studio =   Juventus Film ENIC 
| released = 30 March 1942
| runtime = 86 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Charles V in the Sixteenth century.

==Cast==
* Elsa Merlini as Margherita di Valois  
* Gino Cervi as Carlo Vº  
* Renato Cialente as Francesco Iº  
* Leonardo Cortese as Enrico Dalbret  
* Clara Calamai as Isabella del Portogallo 
* Paolo Stoppa as Il corriere Babieca  
* Valentina Cortese as Eleonora dAustria  
* Nerio Bernardi as Il marchese di Gattinara  
* Greta Gonda as Conchita Babieca  
* Margherita Bagni as La duchessa di Ossuna 
* Wanda Capodaglio as Una dama di corte 
* Oreste Fares as Il sacerdote 
* Enzo Musumeci Greco as Il maestro di scherma 
* Adriano Vitale as Un messagero

== References ==
 

== Bibliography ==
* Nowell-Smith, Geoffrey & Hay, James & Volpi, Gianni. The Companion to Italian Cinema. Cassell, 1996.
 
== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 