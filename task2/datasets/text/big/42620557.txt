Lingaa
 
 
 
{{Infobox film
| image          = Lingaa.jpg
| caption        = Theatrical poster
| name           = Lingaa
| director       = K.S. Ravikumar
| writer         =  
| starring       =  
| producer       = Rockline Venkatesh
| music          = A. R. Rahman
| cinematography = R. Rathnavelu
| editing        = Samjith Mhd
| Production designer =Sabucyril
| Art director =A.Amaran
| Vfx  = P.C.Sanaath
| Stunt = Lee whitetaker & Ramesh
| studio         = Rockline Entertainments
| distributor    = Eros International
| released       =   
| runtime        = 178 minutes
| country        = India Tamil
| budget         =   
| gross          =   
}}
 2014 Kollywood|Indian Telugu and Hindi. 

Principal photography commenced during the last week of May 2014 in Mysore. The majority of the filming took place in Ramoji Film City. The climax portions were filmed in Shimoga.

The film was released on 12 December 2014, coinciding with Rajinikanths birthday.  Though an above average grosser at the box office, the film failed to recover its high distribution cost.    

== Plot ==
The movie is set in the fictional village of Solaiyur and revolves around a dam which is the lifeline for the village. The dam is under assessment for structural integrity by the Public Works Department. The local minister (Jagapathi Babu) has corrupt intentions and wants to make money through a new dam project by killing the dam inspector before throwing the USB information at the temple.

The villagers want to perform a special Pooja to pray to the God Shiva. They decide to invite the grandson of Maharaja Lingeswaran who commissioned the dam and the temple. They believe the return of the line of the Raja will solve the  problems surrounding the dam. Lingaa (played by Rajinikanth in contemporary time) is the grandson of Raja Lingeswaran. His family has fallen on hard times and he is now a petty thief in the city. He is angry with his grandfather for squandering away the family fortune so he is unwilling to go to Solaiyur to preside over the temple function. However, circumstances like being caught by the police because of Lakshmi (Anushka Shetty) force him to accept the invitation and travel to Solaiyur.

Through a flashback sequence, Lingaa comes to know that his grandfather, Lingeswaran (also played by Rajinikanth, set in 1939) was an altruist. He was the only heir of a king, whose lands included Solayur. But as his father was stripped of his administrative and executive powers by the British, Lingeshwaran studies civil engineering and passes the ICS exam to become collector of Madurai. Lingeswaran had spent his entire fortune to build the dam against all odds during the British Raj. But the people of Solaiyur had mistaken his intentions which forced him to leave Solaiyur and move to the city.

Lingaa repents his ways and understands the great sacrifices made by his grandfather for the public good. In the final act, Lingaa exposes the Ministers greed to the villagers and saves the dam from an aerial bomb attack.Finally he unites with Lakshimi.

== Cast ==
 
* Rajnikanth as Raja Lingeswaran, King of Kodaiyur/ K.Lingeswaran (Lingaa)
* Anushka Shetty as Lakshmi
* Sonakshi Sinha as Mani Bharathi
* Jagapati Babu as Nagabooshan
* K. Viswanath as Karunakara  
* N. Santhanam as Lingaas sidekick Karunakaran as Kothandam
* Dev Gill as a freedom fighter
* Brahmanandam as Inspector Ravi Varman
* Radha Ravi as Kavi Bharathi Vijayakumar as one of the village heads
* Nizhalgal Ravi as Zamindar of adjacent area
* R. Sundarrajan (director)|R. Sundarrajan Sammandham, the collectors spy
* Manobala as train loco pilot in 1939
* Ilavarasu as Sami Pillai
* Ponvannan as the government official officer
* Jayaprakash as corrupt government officer
* Madhan Bob as the merchant
* Balaji as Lingaas sidekick
* Crane Manohar as the merchants security guard Anu Mohan as Shanmugam
* Vasu Vikram as Nagabooshans assistant
* K.S. Ravikumar as Finishing Kumar in a special appearance 	
* William Orendorff as British Governor  
* Ravi Mariya as the 5 star hotel manager
* Rajkamal as the 5 star hotel security camera operator
* C. Ranganathan as the 5 star hotel chief security guard
* Padayappa Ramesh as the 5 star hotel security guard
* R. Rathnavelu (special appearance in the song "Oh Nanba")
* Rockline Venkatesh (special appearance in the song "Oh Nanba")
 

== Production ==

=== Development === Ameer earlier had the rights to the title for his next project but handed over the rights to Ravikumar for Rajinikanth. 

=== Casting === Santhanam joined Vijayakumar and R. Sundarrajan were called up to play important roles in the film. 
 Karunakaran was selected to play a supporting role.  Actor Dev Gill was selected to play a secondary antagonist role.  Director and comedian Manobala confirmed that he would appear in a small yet significant role as a train driver.  Comedian Brahmanandam was selected to portray the role of a police officer.  Actresses Trisha Krishnan and Nayanthara were rumored to perform an item number, but the latters inclusion remained unconfirmed and the former denied the news saying that she was never approached.  

=== Filming ===
  Puja ceremony. Red Dragon Phantom Flex British era in front of the illuminated residential portion of the palace. Horses were used for the shot. Over 50 policemen and 30 bouncers had been deployed to prevent people from getting near the shooting area in Mysore. 
 , one of the two main locations where the climax was filmed.]]
The team then moved to Ramoji Film City, Hyderabad for a two-month schedule, where sequences featuring the entire cast were shot for 50 days.      A song sequence featuring Rajinikanth and Anushka was shot at Annapurna Studios, wherein the production team had designed a huge set for the song.   On 13 July 2014, actress Sonakshi Sinha completed her filming schedule in Hyderabad.  The Hyderabad schedule ended on 30 July 2014.    During the Hyderabad schedule of the film, news circulated that Rajinikanth fainted on the sets while filming a heavy action sequence but Ravikumar denied this, citing that the actor was healthy.  
 Radisson Hotel Lord Shiva Armour of God (1986). 

  Chief Minister Karnataka State Police Board also sent a letter to the Chief Minister asking him to end the shooting of the portions of the film there, citing that the bio-diversity and life in and around the dam would be affected.  The elephants featured in the film were constructed through CGI, and a disclaimer was issued for this at the beginning of the film.  

== Accusations and losses ==

=== Script infringement issue === The Madurai John Pennycuick. Further, actor Rajinikanth who wasnt involved in the film script larceny was issued notice as he had distribution rights for the film. However, Justice M. Venugopal refused to pass any interim orders to hold audio release that is scheduled on 16 November 2014. 
On December 3, 2014, the court dismissed a writ petition filed by Ravi Rathinam. Justice Venugopal claimed the dispute to be private and it could have been solved only by initiating civil or criminal proceedings and not by invoking the writ jurisdiction of the High Court. 

=== Distributors losses ===
The films worldwide distribution rights were sold for  .  The distributors of Tamil Nadu suffered large losses and asked for their money back.    A distributor under the banner Marina stated that it suffered losses in Trichy and Thanjavur areas and submitted a petition to go on a hunger strike. Vijayabhargavi Entertainers stated that they suffered losses in Chengalpet region.  Capricorn Pictures incurred losses in North Arcot and South Arcot (Cuddalore-Villupuram) circles. Sukra Films and Chandrakala Movies lost money in Coimbatore and Tirunelveli-Tuticorin respectively. The distributors claimed that after 25 days of its release, Lingaa recovered only 30 percent of the down payment.  Some theatre owners received settlements for their losses.  Rajinikanth later refunded one-third of the   loss reportedly incurred by distributors and exhibitors. He decided to pay them   on a humanitarian basis, who demanded a full refund and had threatened a begging protest in front of the actors residence. 

== Music ==
 
The soundtrack is composed by A.R.Rahman|A.R. Rahman and was released on 16 November 2014.

== Marketing and release ==

Eros International had bought the film from Rockline Venkatesh for approximately  , which includes theatrical and overseas rights for all languages.    The satellite rights of the film were purchased by Jaya TV for  .  

The film was released worldwide on an estimated 5500 screens. 

The motion poster of the film was released on Ganesh Chaturthi , on 29 August 2014.   The first poster featured Rajinikanth walking with a temple being shown in the posters backdrop.  A teaser for the film was released on 1 November 2014.  The trailer was released on 16 November 2014 at the movies audio launch. A 40-second song teaser of "Mona Gasolina" featuring Rajinikanth and Anushka Shetty was released on 17 November 2014.  A second 40-second song teaser was released on 18 November 2014 of the song "En Mannavva" featuring Rajinikanth and Sonakshi Sinha. 

==Reception==

=== Critical response ===

M Suganth of The Times of India gave 2.5 out of 5 and wrote, "The scale of the production, some of Santhanams one-liners and the charisma of Rajinikanth somewhat make it bearable but they arent enough."  Behindwoods.com|Behindwoods rated the film 2.5 out of 5 and stated, "Rajinikanths mesmerizing energy let down by writing. But give it your time for Superstars non-diminishing charisma."  Rediff.com|Rediff rated 3 out of 5 and felt that "Lingaa is buffoonery at its most old-school".  Sify called Lingaa "a mixed bag. The star charisma of Rajinikanth is intact but the film is long with a weak storyline that flounders with a long drawn out climax."  The Hindustan Times said "Lingaa offers nothing new despite being one of Rajinikanths better works in the recent past."  Oneindia.in rated it 3 out of 5, calling it a "Typical Rajinikanth movie" and stating "KS Ravikumars story telling skills induces a fresh dimension to the movie.  CNN-IBN also rated it 3 out of 5, commenting "Because two Rajinikanths are always better than one". 

===Box office===
Lingaa had the largest opening day for a South Indian film as it earned   nett in India and grossed   worldwide.  {{cite web|url=http://www.sify.com/movies/box-office-lingaa-creates-worldwide-record-news-telugu-ompiCqefiidid.html|title=Box Office: Lingaa creates worldwide record!
|publisher=Sify|date=15 December 2014}}  The film also set records on its opening weekend; it earned   nett in India and grossed   worldwide.  After its second weekend, the films domestic total reached   nett. It grossed around   worldwide in ten days.  Lingaa had grossed   worldwide as of 31 December 2014. 

According to Box Office India, the films final domestic nett was  . 

== References ==
 

== External links ==
 
* 
 

 
 
 
 
 
 
 
 
 
 