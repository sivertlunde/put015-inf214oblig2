Napoleon Road
{{Infobox film
| name           = Napoleon Road
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jean Delannoy
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Pierre Fresnay Henri Vilbert Claude Laydu
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = France
| language       = 
| budget         = 
| gross          = 
}} French comedy film directed by Jean Delannoy and starring Pierre Fresnay, Henri Vilbert and Claude Laydu.  A chancer organises a series of package tours along a route said to have been taken by Napoleon in 1815 when in fact the Emperor never set foot there. The local inhabitants back up his story as they hope to cash in on the tourist boom.

==Cast==
* Pierre Fresnay - Édouard Martel
* Henri Vilbert - Blaise
* Claude Laydu - Pierre Marchand
* René Génin - Le curé
* Mireille Ozy - Stella
* Raphaël Patorni - Bonvent
* Fernand Sardou - Le maire de Bourg-sur-Bléone
* Henri Arius - Le boucher

==References==
 

==External links==
* 

 

 
 
 
 
 


 
 