The Usual Suspects
 
 
{{Infobox film
| name           = The Usual Suspects
| image          = Usual suspects ver1.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Bryan Singer
| producer       = Kenneth Kokin Michael McDonnell Bryan Singer
| writer     = Christopher McQuarrie
| starring       = Stephen Baldwin Gabriel Byrne Benicio del Toro Kevin Pollak Kevin Spacey Chazz Palminteri Pete Postlethwaite
| music          = John Ottman
| cinematography = Newton Thomas Sigel
| editing        = John Ottman
| studio         = Bad Hat Harry Productions Blue Parrot
| distributor    = Spelling Films International Gramercy Pictures PolyGram Filmed Entertainment
| released       =  
| runtime        = 106 minutes  
| country        = United States Germany
| language       = English
| budget         = $6 million 
| gross          = $23.3 million 
}} crime Thriller thriller film directed by Bryan Singer and written by Christopher McQuarrie. It stars Stephen Baldwin, Gabriel Byrne, Benicio del Toro, Kevin Pollak, Chazz Palminteri, Pete Postlethwaite and Kevin Spacey.
 con man flashback and narration, Kints story becomes increasingly complex.
 column in Spy (magazine)|Spy magazine called "The Usual Suspects", after one of Claude Rains most memorable lines in the classic film Casablanca (film)|Casablanca. Singer thought it would make a good title for a film, the poster for which he and McQuarrie had developed as the first visual idea.
 Academy Award for Best Writing (Original Screenplay) and Spacey won the Academy Award for Best Supporting Actor for his performance. In time, it is remembered for one of the most definitive and popular plot twists of all cinematic history.

==Plot== FBI agent Customs agent Dave Kujan (Chazz Palminteri) arrive in San Pedro, lured by reports of a large cocaine shipment, and Kujans personal vendetta against Dean Keaton (Gabriel Byrnes character). While Baer is at the hospital attempting to get security for Kovaz, Kovaz begins yelling in Hungarian, and mentions the name Keyser Söze, which stops and stuns Baer.  As Verbal later explains, Söze has a near mythical and vengeful reputation, having killed his own family when they were held hostage by a Hungarian gang and then killing all but one of the gang members before disappearing underground, keeping his true identity secret by insulating himself from his agents, who usually do not know for whom they are working.
 English and Todd Hockney (Kevin Pollak), a Truck hijacking|hijacker. While in a holding cell, McManus convinces the others to get back at the police by intercepting a smuggler being escorted by corrupt cops and stealing his stash. After this job goes well, the five go to Los Angeles and fence the stolen goods with Redfoot (Peter Greene), McManus contact. Redfoot identifies another lucrative potential smuggling operation. The five accept and pull off the heist but find out the shipment was of heroin, not jewelry as they thought. In an angry confrontation with Redfoot, he reveals the job was set up by a lawyer named Kobayashi (Pete Postlethwaite).

The group is later approached by Kobayashi, who says he is working for Söze. Kobayashi possesses extensive details about the fives criminal past and how each of them had slighted Söze at some point even without knowing it. He continues that Söze will consider their actions repaid if they destroy $91 million worth of cocaine being sold by Argentinians to a Hungarian gang, allowing them to keep the cash being used in the deal; if the five fail, Söze will kill their friends and families.

Overnight, Fenster bails on the group; the remaining four find his body the next day at an address Kobayashi provides. They attempt to gain the upper hand on Kobayashi by kidnapping him in his offices, but Kobayashi reveals that Edie Finneran (Suzy Amis), Keatons lawyer and girlfriend, is in Kobayashis offices on legal matters, and she will be killed if they do not complete the job. Kobayashi twists their arms further by threatening them and their families with untold mayhem, Sözes calling cards. With no choice, the others set out on the plan. As the deal starts, Keaton tells Verbal to stay back and should the plan fail to take the money to Edie, who will help him. Keaton and the others kill the Argentinians and the Hungarians, and then he and McManus clear the boat looking for the drugs but find none. McManus, Hockney and a man locked aboard the ship are killed by a person whose face we never see. While Verbal looks on, this person kills Keaton and sets the boat on fire. Verbal has come to believe the mysterious man was Söze himself.

Verbal concludes his story, but Kujan does not believe it, insisting that Keaton must be Söze, as the man aboard the boat was Arturo Marquez, a drug dealer who escaped prosecution by claiming he could identify Söze. Kujan claims that the Argentinians were selling Marquez to Sözes rival Hungarian gang and Keaton used the heist as a distraction to let him kill Marquez. Verbal tearfully testifies that the entire plan was Keatons idea and then refuses to say more.  Verbals bond is posted and he leaves. Kujan, thinking about Verbals story, starts to recognize that several names and concepts from the story match up with papers and other details scattered through the messy office, such as the names "Redfoot" and "Kobayashi": nearly the entire background to the story was a fabrication created by Verbal, who is now revealed to be the real Keyser Söze.

Kujan races to try to stop Verbal but he is too late. Outside the station, Verbal drops his limp and gets into a car driven by "Kobayashi". The film ends on a quote of Charles Baudelaire  which Verbal used to describe Söze: "The greatest trick the devil ever pulled was convincing the world he didnt exist."

==Cast==
* Stephen Baldwin as Michael McManus. The actor was tired of doing independent films where his expectations were not met; when he met with director Bryan Singer, he went into a 15-minute tirade telling him what it was like to work with him. After Baldwin was finished, Singer told him exactly what he expected and wanted, which impressed the actor.   
* Gabriel Byrne as Dean Keaton. Kevin Spacey met Byrne at a party and asked him to do the film. He read the screenplay and turned it down, thinking that the filmmakers could not pull it off. Byrne met screenwriter Christopher McQuarrie and Singer and was impressed by the latters vision for the film. However, Byrne was also dealing with some personal problems at the time and backed out for 24 hours until the filmmakers agreed to shoot the film in Los Angeles, where the actor lived, and make it in five weeks. 
* Benicio del Toro as Fred Fenster. Spacey suggested del Toro for the role. The character was originally written with a Harry Dean Stanton-type actor in mind. Del Toro met with Singer and the films casting director and told them that he did not want to audition because he did not feel comfortable doing them. 
* Kevin Pollak as Todd Hockney. He met with Singer about doing the film, but when he heard that two other actors were auditioning for the role, he came back, auditioned, and got it. 
* Kevin Spacey as Roger "Verbal" Kint. Singer and McQuarrie sent the screenplay for the film to the actor without telling him which role was written for him. Spacey called Singer and told them that he was interested in the roles of Keaton and Kujan but was also intrigued by Kint who, as it turned out, was the role McQuarrie wrote with the actor in mind. 
* Chazz Palminteri as United States Customs Service|U.S. Customs Special Agent Dave Kujan. Singer had always wanted the actor for the film but he was always unavailable. The role was offered to Christopher Walken and Robert De Niro, both of whom turned it down. The filmmakers even had Al Pacino come in and read for the part but he decided not to do it because he was playing a cop in Heat (1995 film)|Heat. Palminteri became available, but only for a week. When he signed on, this persuaded the films financial backers to support the film fully because he was a sufficiently high-profile star, thanks to the recent release of A Bronx Tale and Bullets Over Broadway. 
* Pete Postlethwaite as Mr. Kobayashi, Sözes right-hand man.
* Suzy Amis as Edie Finneran, an influential criminal lawyer and Keatons girlfriend.
* Giancarlo Esposito as FBI Special Agent Jack Baer; investigates the boat explosion on the pier.
* Dan Hedaya as Sergeant Jeffrey "Jeff" Rabin; assists in Dave Kujans interrogation of Roger "Verbal" Kint.
* Peter Greene (uncredited) as Redfoot the Fence; he not only sets up a job for the five criminals in Los Angeles but also puts them in touch with Kobayashi.

==Production==

===Origins===
Bryan Singer met Kevin Spacey at a party after a screening of the young filmmakers first film, Public Access, at the 1993 Sundance Film Festival.    Spacey had been encouraged by a number of people he knew who had seen it,  and was so impressed that he told Singer and McQuarrie that he wanted to be in whatever film they did next. Singer read a column in Spy magazine called "The Usual Suspects" after Claude Rains line in Casablanca (film)|Casablanca. Singer thought that it would be a good title for a film.    When asked by a reporter at Sundance what their next film was about, McQuarrie replied, "I guess its about a bunch of criminals who meet in a police line-up,"  which incidentally was the first visual idea that he and Singer had for the poster: "five guys who meet in a line-up," Singer remembers.    The director also envisioned a tagline for the poster, "All of you can go to Hell."  Singer then asked the question, "What would possibly bring these five felons together in one line-up?"    McQuarrie revamped an idea from one of his own unpublished screenplays &mdash; the story of a man who murders his own family and walks away, disappearing from view. The writer mixed this with the idea of a team of criminals. 
 Turkish dictionary, which translates as "talk too much."  All the characters names are taken from staff members of the law firm at the time of his employment.  McQuarrie had also worked for a detective agency, and this influenced the depiction of criminals and law enforcement officials in the script.   
 Double Indemnity meets Rashomon (film)|Rashomon, and said that it was made "so you can go back and see all sorts of things you didnt realize were there the first time. You can get it a second time in a way you never could have the first time around."  He also compared the films structure to Citizen Kane (which also contained an interrogator and a subject who is telling a story) and the criminal caper The Anderson Tapes. 

===Pre-production===
McQuarrie wrote nine drafts of his screenplay over five months, until Singer felt that it was ready to shop around to the studios. None was interested except for a European financing company.    McQuarrie and Singer had a difficult time getting the film made because of the non-linear story, the large amount of dialogue and the lack of cast attached to the project. Financiers wanted established stars, and offers for the small role of Redfoot (the L.A. fence who hooks up the five protagonists with Kobayashi) went out to Christopher Walken, Tommy Lee Jones, Jeff Bridges, Charlie Sheen, James Spader, Al Pacino and Johnny Cash.  However, the European money allowed the films producers to make offers to actors and assemble a cast. They were only able to offer the actors salaries that were well below their usual pay, but they agreed because of the quality of McQuarries script and the chance to work with each other.  That money fell through, however, and Singer used the script and the cast to attract PolyGram to pick up the film negative. 

About casting, Singer said, "You pick people not for what they are, but what you imagine they can turn into."  To research his role, Spacey met doctors and experts on cerebral palsy and talked with Singer about how it would fit dramatically in the film. They decided that it would affect only one side of his body.  According to Byrne, the cast bonded quickly during rehearsals.  Del Toro worked with Alan Shaterian to develop Fensters distinctive, almost unintelligible speech patterns.    According to the actor, the source of his characters unusual speech patterns came from the realization that "the purpose of my character was to die."  Del Toro told Singer, "It really doesnt matter what I say so I can go really far out with this and really make it uncomprehensible." 

===Filming=== San Pedro and New York City.    Spacey said that they shot the interrogation scenes with Palminteri over a span of five to six days.    These scenes were also shot before the rest of the film.  The police lineup scene ran into scheduling conflicts because the actors kept blowing their lines. Screenwriter Christopher McQuarrie would feed the actors questions off-camera and they improvised their lines. When Stephen Baldwin gave his answer, he made the other actors break character.  Byrne remembers that they were often laughing between takes and "when they said, Action!, wed barely be able to keep it together."  Spacey also said that the hardest part was not laughing through takes, with Baldwin and Pollak being the worst culprits.  Their goal was to get the usually serious Byrne to crack up.  They spent all morning trying unsuccessfully to film the scene. At lunch a frustrated Singer angrily scolded the five actors, but when they resumed the cast continued to laugh through each take.  Byrne remembers, "Finally, Bryan just used one of the takes where we couldnt stay serious."  Singer and editor John Ottman used a combination of takes and kept the humor in to show the characters bonding with one another. 

While Del Toro told Singer how he was going to portray Fenster, he did not tell his cast members, and in their first scene together none of them understood what Del Toro was saying. Byrne confronted Singer and the director told him that for the lockup scene, "If you dont understand what hes saying maybe its time we let the audience know that they dont need to know what hes saying."  This led to the inclusion of Kevin Pollaks improvised line, "What did you say?"

The stolen emeralds were real gemstones on loan for the movie. 

Singer spent an 18-hour day shooting the underground parking garage robbery.  According to Byrne, by the next day Singer still did not have all of the footage that he wanted, and refused to stop filming in spite of the bonding companys threat to shut down the production. 

In the scene in which the crew meets Redfoot after the botched drug deal, Redfoot flicks his cigarette at McManus face. The scene was originally to have Redfoot flick the cigarette at McManuss chest, but the actor missed and hit Baldwins face by accident. Baldwins reaction is genuine. 

Despite enclosed practical locations and a short shooting schedule, cinematographer Newton Thomas Sigel "developed a way of shooting dialogue scenes with a combination of slow, creeping zooms and dolly moves that ended in tight close-ups," to add subtle energy to scenes.    "This style combined dolly movement with "imperceptible zooms" so that you’d always have a sense of motion in a limited space."   

===Post-production=== montage featuring Gramercy had problems pronouncing the name Keyser Söze and were worried that audiences would have the same problem. The studio decided to promote the characters name. Two weeks before the film debuted in theaters, "Who is Keyser Söze?" posters appeared at bus stops, and TV spots told people how to say the characters name.     Despite these efforts, all the actors in the film consistently mispronounce his name as "Soze" instead of "Söze".
 Piano Concerto No. 1. The endings music was based on a k.d. lang song.   

==Release==
Gramercy ran a Film promotion|pre-release promotion and advertising campaign before The Usual Suspects opened in the summer of 1995.  Word of mouth marketing was used to advertise the film, and buses and billboards were plastered with the simple question, "Who is Keyser Söze?"   

The film was shown out of competition at the 1995 Cannes Film Festival and was well received by audiences and critics.  The film was then given an exclusive run in Los Angeles, where it took a combined $83,513, and New York City, where it made $132,294 on three screens in its opening weekend.  The film was then released in 42 theaters where it earned $645,363 on its opening weekend. It averaged a strong $4,181 per screen at 517 theaters and the following week added 300 locations.  It eventually made $23.3 million in North America.   

==Reception==
The Usual Suspects has received positive reviews from critics. On Rotten Tomatoes, the film holds a rating of 88%, based on 68 reviews, with an average rating of 7.8/10. The sites consensus reads, "Expertly shot and edited, The Usual Suspects gives the audience a simple plot and then piles on layers of deceit, twists, and violence before pulling out the rug from underneath."  On Metacritic, the film has a score of 77 out of 100, based on 22 critics, indicating "generally favorable reviews". 

While embraced by most viewers and critics, the film was the subject of harsh derision by some. Roger Ebert, in a review for the Chicago Sun-Times, gave the film one and a half stars out of four, considering it confusing and uninteresting.  He also included the film in his "most hated films" list.  USA Today rated the film two and a half stars out of four, calling it "one of the most densely plotted mysteries in memory—though paradoxically, four-fifths of it is way too easy to predict."  However, Rolling Stone praised Spacey, saying his "balls-out brilliant performance is Oscar bait all the way."  In his review for The Washington Post, Hal Hinson wrote, "Ultimately, The Usual Suspects may be too clever for its own good. The twist at the end is a corker, but crucial questions remain unanswered. Whats interesting, though, is how little this intrudes on our enjoyment. After the movie youre still trying to connect the dots and make it all fit—and these days, how often can we say that?" 

In her review for  , these performers still create strong and fascinatingly ambiguous characters."    is as elegant as it is unexpected. The whole movie plays back in your mind in perfect clarity—and turns out to be a completely different movie to the one youve been watching (rather better, in fact)." 

===Accolades=== Best Original Best Supporting Actor at the Academy Awards.    They both won, and in his acceptance speech Spacey memorably said, "Well, whoever Keyser Söze is, I can tell you hes gonna get gloriously drunk tonight."   
 Best Supporting Best Screenplay Best Cinematography for Newton Thomas Sigel.    Both Del Toro and McQuarrie won in their categories.   
 Best Debut award at the 1st Empire Awards. 

The Usual Suspects was screened at the 1995 Seattle International Film Festival, where Bryan Singer was awarded Best Director and Kevin Spacey won for Best Actor.    The Boston Society of Film Critics gave Spacey the Best Supporting Actor award for his work on the film.    Spacey went on to win this award with the New York Film Critics Circle and the National Board of Review, which also gave the cast an ensemble acting award.   

===Legacy===
On June 17, 2008, the American Film Institute revealed its "AFIs 10 Top 10"—the best ten films in ten "classic" American film genres—after polling over 1,500 people from the creative community. The Usual Suspects was acknowledged as the tenth-best mystery film.  Verbal Kint was voted the #48 villain in "AFIs 100 Years...100 Heroes and Villains" in June 2003.

Entertainment Weekly cited the film as one of the "13 must-see heist movies".  Empire (magazine)|Empire ranked Keyser Söze #69 in their "The 100 Greatest Movie Characters" poll. 

In 2013, the Writers Guild of America ranked the screenplay #35 on its list of 101 Greatest Screenplays ever written. 

American Film Institute Lists:
* AFIs 100 Years...100 Thrills—Nominated
* AFIs 100 Years...100 Heroes and Villains:
** Keyser Söze/Verbal Kint—#48 Villain
* AFIs 100 Years...100 Movie Quotes:
** "The greatest trick the Devil ever pulled was convincing the world he didnt exist."—Nominated
* AFIs 100 Years...100 Movies#2007 update notes|AFIs 100 Years...100 Movies (10th Anniversary Edition)—Nominated
* AFIs 10 Top 10: #10 Mystery Film (also Nominated Gangster Film) 

==References==
 
*  
*  

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 