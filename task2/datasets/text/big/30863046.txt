The Dark at the Top of the Stairs
{{Infobox film
| name           = The Dark at the Top of the Stairs
| image          = Poster of the movie The Dark at the Top of the Stairs.jpg
| image_size     = 
| caption        = Movie poster
| director       = Delbert Mann Michael Garrison
| writer         = Harriet Frank, Jr. Irving Ravetch William Inge (play)
| narrator       =  Robert Preston Dorothy McGuire
| music          = Max Steiner
| cinematography = Harry Stradling, Jr.
| editing        = 
| distributor    = Warner Bros.
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Dark at the Top of the Stairs is a 1957 play by William Inge about family conflicts during the early 1920s in a small Oklahoma town. It was nominated for the Tony Award for Best Play in 1958 and was made into a film in 1960.

==Plot==
The drama centers on Rubin Flood, who loses his salesman job. While searching for a new job, he must deal with his wife, Cora, who shuns intimacy and mistakes his joblessness for stinginess, his shy daughter who prepares for her first dance and his pre-teen son who runs to his mother instead of dealing with bullies. He tries to find comfort with a friend, Mavis Pruitt, thus setting off rumors of an untoward relationship.

==Play==
Directed by Elia Kazan, the play opened December 5, 1957, at New Yorks Music Box Theatre and ran for a total of 468 performances, closing on January 17, 1959. The drama was reworked by Inge from his earlier play, Farther Off from Heaven, first staged in 1947 at Margo Jones Theatre 47 in Dallas, Texas.

Opening night cast:
*Eileen Heckart as Lottie Lacey
*Pat Hingle as Rubin Flood
*Teresa Wright as Cora Flood
*Timmy Everett as Sammy Goldenbaum
*Frank Overton as Morris Lacey
*Anthony Ray as Chauffeur
*Evans Evans as Flirt Conroy
*Carl Reindel as Punky Givens
*Judith Robinson as Reenie Flood
*Charles Saari as Sonny Flood
*Jonathan Shawn as Boy Offstage 

It was nominated for five Tony Awards: Best Play, Best Featured Actor (Pat Hingle), Best Featured Actress (Eileen Heckart), Best Scenic Design (Ben Edwards), Best Director (Elia Kazan). Timmy Everett won a Theatre World Award.

==Film==
Harriet Frank, Jr. and Irving Ravetch adapted Inges play into a 1960 Warner Bros. film directed by Delbert Mann.

*Dorothy McGuire as Cora Flood Robert Preston as Rubin Flood
*Shirley Knight as Reenie Flood
*Eve Arden as Lottie Lacey
*Robert Eyer as Sonny Flood
*Frank Overton as Morris Lacey
*Lee Kinsolving as Sammy Goldenbaum
*Angela Lansbury as Mavis Pruitt
*Ken Lynch as Harry Ralston 
 Oscar nomination Best Supporting Actor for his role as "Sammy Goldenbaum".

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 