Sunstruck
 
{{Infobox film
| name           = Sunstruck
| image          = 
| image_size     = 
| caption        = 
| director       = James Gilbert
| producer       = Jack Neary James Grafton
| writer         = Stan Mars
| based on = 
| starring       = Harry Secombe
| music          = 
| cinematography = Brian West 
| editing        = Anthony Buckley Barry Peters 
| studio    = Immigrant Productions
| distributor = British Empire Films
| released       = 18 November 1972
| runtime        = 92 mins
| country        = Australia
| language       = English
| budget = A$400,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p267 
| gross = 
}} Australian comedy James Gilbert and starring Harry Secombe, Maggie Fitzgibbon and John Meillon. 

==Plot==
Stanley Evans, a Welsh schoolteacher, decides to emigrate to Australia for a better life but ends up working in a school in the dead-end town of Kookaburra Springs. He forms a school choir and decides to compete in a singing competition in Sydney.  It is also known by the alternative title of Education of Stanley Evans.

==Cast==
* Harry Secombe - Stanley Evans
* Maggie Fitzgibbon - Shirley Marshall
* John Meillon - Mick Cassidy
* Peter Whittle - Pete Marshall
* Dennis Jordan - Steve Cassidy
* Dawn Lake - Sal Cassidy
* Bobby Limb - Bill
* Norm Erskine - Norm Jack Allen - Banjo Roger Cox- Ben
* Tommy Mack - Gunboat
*Derek Nimmo
*Stuart Wagstaff - announcer

==Production== Parkes in Australian Film Development Corporation.  

The pupils seen acting at choir practice in the early part of the film were from Afon Taf High School in Troedyrhiw.  The choir recorded the entire choral soundtrack at the school and it was used in both the Welsh and Australian sequences.  The beginning of the film used locations in two Welsh mining villages; Treharris and Trelewis near Merthyr Tydfil.  The name of the school is Webster Street School, Treharris which has since been demolished for housing.

==Release==
The film did not perform particularly well commercially or critically. 

==Bibliography==
* Readle, Eric. History and heartburn: the saga of Australian film, 1896-1978. Associated University Presses, 1981.

==References==
 

==External links==
* 
*  at Australian Screen Online
*  at Oz Movies

 
 
 
 
 
 
 
 


 
 