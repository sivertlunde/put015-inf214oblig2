Girls Marked Danger
{{Infobox film
| name           =Girls Marked Danger
| image          =La tratta delle bianche.jpg
| image_size     =
| caption        =
| director       = Luigi Comencini
| producer       = Dino De Laurentiis and Carlo Ponti
| writer         = 
| narrator       =
| starring       =
| music          =Armando Trovaioli 
| cinematography =
| editing       =Nino Baragli 
| distributor    =
| released       = 1952
| runtime        = 97 min (USA: 75 min)
| country        = Italy and United States
| language       = Italian
| budget         =
}}

La Tratta delle bianche is a 1952 Italian film. It was also released in the United States in 1954 under the title Girls Marked Danger.

==Cast==
{| class="wikitable"
|-
! Actor/Actress
! Role
|-
| Eleonora Rossi Drago
| Aida
|-
| Marc Lawrence
| Manfredi
|-
| Ettore Manni
| Carlo
|-
| Silvana Pampanini
| Lucia
|-
| Vittorio Gassman
| Michele
|-
| Tamara Lees
| Clara
|-
| Antonio Nicotra
| Bernardino
|-
| Barbara Florian
| Fanny
|-
| Ignazio Balsamo
| Stefano
|-
| Franco Bologna
| Marco
|-
| Gianni Bonos
| Tonino
|-
| Sophia Loren
| Elvira
|-
| Enrico Maria Salerno
| Giorgio
|-
| Duilio DAmore
| Luciano
|-
| Maria Zanoli
| Giulia
|}

==External links==
*  
 

 
 
 
 
 

 