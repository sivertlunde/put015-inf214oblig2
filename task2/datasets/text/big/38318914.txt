The Well-Digger's Daughter (2011 film)
{{Infobox film
| name           = The Well-Diggers Daughter
| image          = The Well-Diggers Daughter poster.jpg
| border         = yes
| caption        = French poster
| director       = Daniel Auteuil
| producer       = Alain Sarde	  
| screenplay     = Daniel Auteuil
| based on       =  
| starring       =  
| music          = Alexandre Desplat
| distributor    = Kino International (company) (USA)
| released =  
| runtime        = 107 minutes
| country        = France
| language       = French
| budget         = 
}} French romantic drama film. Daniel Auteuil makes his directorial debut as he stars alongside Àstrid Bergès-Frisbey, Kad Merad, Sabine Azéma, Jean-Pierre Darroussin, and Nicolas Duvauchelle. 
 1940 French film of the same name written and directed by Marcel Pagnol.   

==Plot==
Jacques Mazel, the son of a wealthy merchant, and Patricia Amoretti, daughter of a well-digger, live in a small village in the Provence region of France while Europe is at the brink of World War I. She has just turned 18 and is smitten with the handsome Jacques when he carries her across a stream so she bring lunch to her father who is digging a well.  After their second encounter Jacques, a French Air Force pilot, is unexpectedly reassigned. He must leave immediately to replace a pilot who recently broke his leg. Because he cannot meet Patricia as planned the next morning, he leaves an explanatory letter for her but his overly protective mother burns the letter.  Patricia concludes Jacques rejected her because they come from different social classes.  Later, Patricia discovers that Jacques has left her with child.  The parents of Jacques reject the idea that their son conceived a child out of wedlock and refuse to acknowledge the baby.  Her father, who had idolized her, sends Patricia to live with his sister in another village to spare his four other daughters from learning of the illegitimate child.  Patricia declines an offer of marriage from her fathers friend, Félipe, partly because her younger sister, Amanda, has a crush on him.  Eventually, Jacques, who was thought to have been shot down by the Germans, returns from a POW camp and learns he has a son.

==Production==
The original music score was composed by Alexandre Desplat.   

==Selected Cast==
*Daniel Auteuil as Pascal Amoretti
*Àstrid Bergès-Frisbey as Patricia Amoretti
*Kad Merad as Félipe Rambert
*Sabine Azéma as Madame Mazel
*Jean-Pierre Darroussin as Monsieur Mazel
*Nicolas Duvauchelle as Jacques Mazel
*Marie-Anne Chazel as Nathalie

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 

 
 