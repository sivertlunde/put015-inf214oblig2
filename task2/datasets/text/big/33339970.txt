Seeing Stars (film)
{{Infobox Hollywood cartoon|
| cartoon_name = Seeing Stars
| series = Krazy Kat
| image = 
| caption = 
| director = Ben Harrison Manny Gould
| story_artist = Ben Harrison
| animator = Allen Rose Harry Love
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| distributor = Columbia Pictures
| release_date = September 12, 1932
| color_process = Black and white
| runtime = 7:19 English
| preceded_by = Lighthouse Keeping
| followed_by = Prosperity Blues
}}

Seeing Stars is a short animated film distributed by Columbia Pictures, and features Krazy Kat. Different from most shorts of the series, the cartoon features celebrities in their animated forms.

==Plot==
At a music hall, a trio of singers performed by their microphones while an orchestra behind them plays. Enjoying their act are guests who dance on a square similar to a boxing ring. When the musicians stop to play another song, the square carrying some of the guests gets moved out and replaced by another one.

In a few moments, the stuttering announcer, (  bicycle.

When Krazy is about to continue playing his instrument, he is interrupted by a noise coming from one of the tables. By that table, Laurel and Hardy are eating peas and roast chicken. While Hardy is eating with ease, the other guy appears to be having difficulty. Everytime Laurel scoops up peas with a spoon, they would often fall off and make a sound thats quite loud to some. Bothered by this, Hardy provided assistance. Krazy then comes to their table and asks them to be quiet as possible.

Thinking about having a little meal, Krazy saw a cross-eyed man (Ben Turpin) eating spaghetti. Krazy came to the mans table and decided to have a share of the food. While they eat, a waiter (Jimmy Durante) carrying fruits in a bowl was passing by, and nearby attendants started grabbing anything they can get. Krazy also tries to pick a fruit but mistakenly grabs the waiters long nose instead, much to the latters annoyance. In reply, the waiter mumbles a few words and splats the platter of spaghetti right on top Krazy. Krazy, however, gets cleaned up by someone from another table who uses a seltzer bottle.

Still wondering the music hall, Krazy notices the orchestra is fast asleep. He then wakes them up and tells them to continue playing. When music plays again, Krazy starts dancing at the center floor, and all those whom he came across with join him there. Watching from the tables, the attendants enjoy the celebration.

==Availability==
*Columbia Cartoon Collection: Volume 3. {{cite web
|url=http://theshortsdepartment.webs.com/columbiacartoons.htm
|title=The Columbia Cartoons
|accessdate=2012-06-17
|publisher=the shorts development
}} 

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 


 