Vakkalathu Narayanankutty
{{Infobox film
| name           = Vakkalathu Narayanankutty
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = T. K. Rajeev Kumar
| producer       = Kalliyoor Sasi B. Rakesh
| writer         = Story: T. K. Rajeev Kumar Screenplay: T. K. Rajeev Kumar Jayaprakash Kuloor Dialogues: Jayaprakash Kuloor Manya Mukesh Mukesh Jagathy Sreekumar Bobby Kottarakkara
| music          = Mohan Sithara
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Khathilakam Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Mukesh and Jagathy Sreekumar in the lead. It was actor Bobby Kottarakkaras last film. It was during the shooting of this film that Bobby Kottarakkara died, leaving the team to continue the shooting without him.  The film had an average performance in the box office.  

==Plot==
The film presents the story of Narayanankutty, popularly known as Vakkaalaththu Narayanankutty, an active social worker who can bear anything but breach of justice. When he sees law being broken anywhere, he reacts promptly. He doesn’t even care as to who is pitted against him and just goes on fighting. This leads to all sorts of problems in his life and the film follows that.

==Cast==
* Jayaram as Vakkalathu Narayanankutty Manya as Narayanankuttys love interest Mukesh as Advocate Mathew
* Jagathy Sreekumar Advocate Eeswara Subrahmanya Iyer
* Bobby Kottarakkara as Captain Bobby, a book seller at the bus-stand
* Rajan P. Dev
* Ponnamma Babu
* P. Sreekumar

==References==
 

==External links==
*  

 
 


 