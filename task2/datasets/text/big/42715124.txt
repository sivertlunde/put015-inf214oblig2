Thangigagi
 
 
{{Infobox film name           = Thangigagi image          = image_size     = caption        = director       = P. N. Satya producer       = Anitha Kashyap writer         = Perarasu narrator       = starring  Darshan Poonam Bajwa Shwetha Chengappa Rangayana Raghu music          = Sadhu Kokila cinematography = Anaji Nagaraj editing        = S. Manohar studio         = Sri Padma Chithra released       =   runtime        = 145 minutes country        = India language       = Kannada budget         =
}}
 Kannada sentimental action film directed by P. N. Satya  and produced by Anitha Kashyap. The main cast includes Darshan (actor)|Darshan, Poonam Bajwa and Shwetha Chengappa besides Avinash and Rangayana Raghu in other pivotal roles. 
 Tamil blockbuster Vijay in the lead role. It was also released in Telugu language as Annavaram (film)|Annavaram. The film mainly displays the brother - sister sentiments with action backdrop. Releasing on 20 October 2006, the film was reviewed negatively by the critics citing it to be a poor remake and eventually failed at the box-office. 

==Cast== Darshan
* Poonam Bajwa
* Shwetha Chengappa
* Rangayana Raghu
* Avinash
* Bullet Prakash
* Dharma
* Vishwas Bharadwaj
* Harish Rai
* Bank Janardhan
* M.N Lakshmi Devi
* Chitra Shenoy

==Soundtrack==
The music of the film was composed by Sadhu Kokila.

{{Infobox album  
| Name        = Thangigagi
| Type        = Soundtrack
| Artist      = Sadhu Kokila
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Ashwini Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Onde Ondu
| lyrics1  	= A. P. Arjun
| extra1        = Rajesh Krishnan, Anuradha Sriram
| length1       = 
| title2        = Dina Dina
| lyrics2 	 = A. P. Arjun
| extra2        = Kunal Ganjawala, Shamitha Malnad
| length2       = 
| title3        = Bittu Bittu
| lyrics3       = A. P. Arjun
| extra3  	= Udit Narayan, Malathi
| length3       = 
| title4        = Vandane
| extra4        = Hemanth
| lyrics4 	= Doddarangegowda
| length4       = 
| title5        = Anna Thangi
| extra5        = Madhu Balakrishnan
| lyrics5       = A. P. Arjun
| length5       = 
}}

==References==
 

==External source==
*  
*  
*  

 
 
 
 
 
 
 


 

 