The Hellstrom Chronicle
{{Infobox film
| name           = The Hellstrom Chronicle
| image          = Hellstrom chronicle.jpg
| caption        = Promotional film poster
| director       = Ed Spiegel Walon Green
| starring       = Lawrence Pressman
| writer         = David Seltzer
| producer       = David L. Wolper
| music          = Lalo Schifrin
| cinematography = Helmuth Barth Walon Green Vilis Lapenieks Ken Middleham
| editing        = John Soh
| distributor    = Cinema 5 Distributing
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
}} horror and apocalyptic prophecy to present a gripping satirical depiction of the Darwinian struggle for survival between humans and insects. It was conceived and produced by David L. Wolper, directed by Walon Green and written by David Seltzer, who earned a Writers Guild of America Award nomination for his screenplay.  Green later called it "almost yellow-journally but good. We were giving the audience an elbow to the ribs every third line."   
 trailer resembled an announcement for a science fiction movie. The film provided the inspiration for Frank Herberts science fiction novel Hellstroms Hive.

==Plot summary==
 horror and science fiction movies with extraordinary camera sequences of butterflies, locusts, wasps, termites, ants, mayflies, other insects rarely seen before on film and insectivorous plants/insects.

==Awards==
*1972 Academy Award for Best Documentary Feature   
*1972 BAFTA Award - Best Documentary feature

==Home Video Releases==
The film was released as a "gray area" DVD for years and only received its official DVD and Blu-ray release on January 10, 2012 from Olive Films.

==References==
 

==External links==
* 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 