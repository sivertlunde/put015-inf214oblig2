Karate Cop
 
 name = Karate Cop
| image          = 
| caption        =  Alan Roberts
| producer       = Ronald L. Marchini Garrick Huey Jo-Anne Marchini
| writer         = Denny Grayson Ronald L. Marchini Bill Zide
| starring       = Ron Marchini David Carradine
| music          = Cecil Ramirez Ralph Stover
| cinematography = Hugh C. Litfin
| editing        = Garrick Huey
| distributor    = Imperial Entertainment
| released       = 1991
| runtime        = 100 minutes
| rating         = R
| country        = United States
| language       = English
| budget         = Unknown
}}
 totalitarian society. It stars Ronald L. Marchini in the main role and David Carradine in a cameo appearance.

==Plot== totalitarian governments. The people in control are gangs of rampaging marauders. In post-apocalyptic America, the once sovereign society has become a corrupt, crime ridden totalitarian wasteland. The few remaining citizens are either hiding in devastated urban areas or are in control of by a series of gangs that now rule the cities with an iron fist. A former cop, John Travis, is a martial arts expert and spends his days undercover, walking across the barren urban landscape. Travis is doing his best to maintain some kind of order, despite the fact that the gangs slowly weed each other out by fighting in large arenas to create the most powerful gang that would control the government.

==Cast==
Ronald L. Marchini as John Travis  
Carrie Chambers	as Rachel 
Michael E. Bristow as Snaker 
D.W. Landingham	as Lincoln 
Michael M. Foley as Lincolns Champion 
Dana Bentley as Lincolns Woman 
Dax Nicholas as Cal 
David Carradine as Dad 
Vibbe Haugaard as Mica 
Warren Reed as Fat Scav 
Jeffrey K. Lee as Sneaker 
Lorraine Swanson as Tess 
Denny Grayson as Priest 
Kelli Gianettoni as Dancing Dahlia 
Stephen W. Sargent as Danny

==Release==
The film was a direct-to-video release, so totals of how many units it sold are unknown. The film was a sleeper hit, and did not receive much attention. Therefore, it nearly slipped into obscurity. However, years later, it has since been rediscovered and has gained a small cult following.  The film received mixed to negative reviews from critics, who initially criticized the film for its acting and plot. However, almost all critics have praised the film for its carefully researched and extensively choreographed action sequences.

== External links ==
*  
*  
*  

 
 
 
 
 
 