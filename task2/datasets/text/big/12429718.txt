Ironclads (film)
 
{{Infobox television film
| name = Ironclads
| image = 
| caption = 
| format = Historical Drama
| runtime =  94 minutes
| creator =
| director = Delbert Mann
| producer = David A. Rosemont Norman Rosemont
| writer = Harold Gast
| starring = Virginia Madsen Alex Hyde-White E. G. Marshall Fritz Weaver Philip Casnoff Reed Diamond
| editing = Millie Moore
| music = Allyn Ferguson
| cinematography = William Wages Turner Home Entertainment
| studio = Turner Pictures 
| country = United States
| language = English
| network = Turner Broadcasting Network
| released = March 11, 1991
}} TNT company CSS Virginia USS Merrimack USS Monitor Noel Taylor received an Emmy Award nomination for his costume designs for the production.

== Plot ==
Harmon is awaiting a court martial when he is brought to Commodore Joseph Smith (E.G. Marshall) and his son Joseph B. Smith|Lt. Joseph B. Smith, Jr. (Kevin ORourke).

The Virginia sails off to break the Union blockade at Hampton Roads. Betty is promptly arrested by Lt. Guilford (Philip Casnoff) on suspicion of espionage.
 USS Congress as Lt. Smith rallies his men on board to "do their duty"; Captain Franklin Buchanan (Leon B. Stevens) of the Virginia rallies his men, similarly. Congress fires a full broadside into Virginia to no effect.
 USS Minnesota, John Worden (Andy Park) of the Monitor. Worden makes Harmon, familiar with Hampton Roads and the CSS Virginia, midshipman on the spot.

The morning of battle, Catesby Jones introduces his crew to the Monitor between them and the Minnesota. Worden does his best to engage Virginia as far from the Minnesota as he can. Virginia  s first shot demonstrates that Monitor  s armor will hold. Virginia "steers worse than Noahs Ark", Lt. Simms (J. Michael Hunter) informs Catesby that Virginia is leaking at the bow, Harmon tells Worden it would take Virginia 30 minutes to turn, and later adds that Virginia would be grounded in shallow water. It is. 
Both sides claimed victory, but the Monitor denied Confederate access to the ocean. John Ericsson (Fritz Weaver) has an argument with Secretary of the Navy Gideon Welles (Conrad McLaren) over the use of insufficient powder in Monitor  s guns to sink the Virginia: Welles responds it was a calculated decision to save the lives of the crew.A voiceover indicates that the Virginia was scuttled later that year after Union troops took Norfolk, and that the Monitor sank off Cape Hatteras later that year as well.

==Cast==
* Virginia Madsen- Betty Stuart
* Alex Hyde-White - Catesby Jones
* Reed Diamond - Leslie Hanson
* Philip Casnoff- Lt. Guilford
* E.G. Marshall - Commander Joseph Smith
* Fritz Weaver - John Ericsson
*Kevin ORourke - Lt. Joe Smith, Jr.

==Awards==

This film was nominated for four Primetime Emmy Awards including Outstanding Achievement in Special Visual Effects, Outstanding Costume Design for a Miniseries or Special, Outstanding Sound Editing for a Miniseries or Special and Outstanding Sound Mixing for a Drama Miniseries or a Special. However it diid not win any of these awards.

==References==
 

==External links==
* 

 

 
 
 
 
 
 