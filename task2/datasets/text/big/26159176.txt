City of Gold (2010 film)
{{Infobox film
| name = City of Gold
| image = City of Gold.jpg
| caption = Theatrical release poster
| director = Mahesh Manjrekar
| producer = Arun Rangachari
| screenplay = Mahesh Manjrekar Jayant Pawar
| story = Jayant Pawar
| starring = Ankush Choudhary Siddharth Jadhav Sachin Khedekar Sameer Dharmadhikari Karan Patel
| music= Ajit Parab
| cinematography = Ajit Reddy V. Ajith Reddy
| editing = Sarvesh Parab
| released =  
| runtime =
| country = India
| language = Hindi, Marathi (as Lalbaug Parel)
| budget =
| studio = DAR Motion Pictures
}}
 2010 Bollywood Marathi languages. The film was directed by Mahesh Manjrekar, who has directed critically acclaimed films in the past like Astitva and Viruddh... Family Comes First. The film is adapted from the play Adhantar by Jayant Pawar, who also co-wrote the screenplay, and explores the life of Mumbais mill workers after they were rendered jobless in the early 1980s, when the Mumbai mills shut down in the post Great Bombay Textile Strike period, and soon made way for skyscrapers and shopping malls.   The film opened to mixed reviews, though it was commended for its theme, and acting.   The name of the Marathi version of this film is Lalbaug Parel, while the Hindi version was released as City of Gold. 

==Plot==
The plot of the movie is based on the original script of Jayant Pawar, which highlights the consequences of the atrocities towards the mill workers, totally ignored by the Government. The film is a story of a family of mill workers in Mumbai. It traces the birth of the politics of greed in Mumbai and exposes the collusion between the triumvirate of big business, the political establishment and the trade union leaders who ostensibly were charged with protecting the rights of the mill workers. In the two decades that followed, the entire landscape of Central Bombay was changed forever. Land became the currency of growth, and this began the systematic extinction of mills in Bombay. In a matter of just a few years, hundreds of thousands of workers lost their means of livelihood. Having worked in these mills from generation to generation, this was the only vocation that they knew.

==Production== DAR Motion Pictures produced the film.

==Legal troubles==
After the films release the family of Dutta Samant served Mahesh Manjrekar with legal notice, claiming that the film wrongly placed the blame for the mills closure on Samant. They also objected to Manjrekars portrayal of the mill workers children, saying they were shown as "goons". 

==Cast==
* Shashank Shinde as Anna
* Seema Biswas as Aai
* Ankush Choudhary as Baba
* Anusha Dandekar as Manasi
* Vineet Kumar as Mohan
* Veena Jamkar as Manju
* Karan Patel as Naru
* Siddharth Jadhav as Speedbreaker
* Satish Kaushik as Mama
* Kashmira Shah as Mami
* Sameer Dharmadhikari as Mahendra
* Ganesh Yadav as Parshya Bhai
* Sachin Khedekar as Rane
* Vinay Apte as Govind
* Kishore Pradhan as Khetan Sheth
* vaibhav mangle as DoctorSaheb
* Sai Tamhankar as Shalu / Narus girlfriend
* Nikhil Ratnaparkhi as Minister
===Track listing===
{{Track listing
| title1 = Tuzya girnicha vajude bhonga – Laavani Dance | length1 = 3:07
}}

==References==
 
 

==External links==
* 
*  
* 

 
 
 
 
 
 
 
 
 