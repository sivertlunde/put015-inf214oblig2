Certains l'aiment froide
{{Infobox film
| name           = Certains laiment froide
| image          = 
| caption        = 
| director       = Jean Bastia Guy Lionel
| producer       = Kerfrance Production (France)
| writer         = Jean Bastia Guy Lionel
| starring       = Louis de Funès Pierre Dudan
| music          = Pierre Dudan
| cinematography = 
| editing        = 
| distributor    = Les Films Fernand Rivers
| released       = 17 February 1960 (France)
| runtime        = 90 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1960, directed by Jean Bastia, written by Guy Lionel, starring Louis de Funès. The film is known under the title: "Les râleurs font leur beurre" (alternative French title). 

== Cast ==
* Louis de Funès : Ange Galopin, the creditor who wants to recover silver
* Pierre Dudan : Pierre Valmorin, Swiss compositor Robert Manuel : Luigi Valmorin
* Francis Blanche : William, Foster Valmorin, American
* Noël Roquevert : Maître Albert Leboiteux, notary Jean Richard : Jérôme Valmorin, the explorer
* Mathilde Casadesus : Mathilde Valmorin, cultivator
* Mireille Perrey : mum of Pierre
* Françoise Béguin : Ariel
* Guy Nelson : Tony, the singer
* Habib Benglia : Hannibal Valmorin, African
* Nicky Valor : Ingrid Valmorin, the Danish
* Jean-Paul Rouland : the doctor Schuster
* Léonce Corne : Maître Meyer, the associate of mister Leboiteux
* Harry Max : the director of the asylum Saint-Vincent
* Max Elloy : Simpson, le majordome particulier de Pierre Mario David : Le masseur
* Albert Daumergue : doctor of the prison
* Marie-Pierre Casey|Marie-Pierre Gauthey "Casey" : the nurse
* Pierre Duncan : a security guard of the prison

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 


 