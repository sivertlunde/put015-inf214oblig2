Skinny Tiger, Fatty Dragon
 
 
{{Infobox film
| name           = Skinny Tiger, Fatty Dragon
| image          = SkinnyTigerFattyDragon DVDcover.jpg
| caption        = UK DVD cover
| film name = {{Film name| traditional    = 瘦虎肥龍
| simplified     = 瘦虎肥龙
| pinyin         = Shòu Hǔ Féi Lóng
| jyutping       = Sau3 Fu2 Fei4 Lung4}}
| director       = Lau Kar-wing
| producer       = Wellington Fung
| writer         = Chang Gwok-tsz
| starring       = Sammo Hung Karl Maka Lau Kar-wing Ridley Tsui
| music          = Richard Lo
| cinematography = Gray Hoh Bo Yiu
| editing        = Wong Ming Lam
| distributor    = Cinema Capital Entertainement Cinema City & Films Company
| released       =  
| runtime        = 105 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$10,270,954
}} Hong Kong Gar Bo Golden Harvest Cinema City. Skinny Tiger, Fatty Dragon was therefore something of a reunion for the three actors.

==Plot==
Fatty and Baldy are a pair of detectives dealing with a crime syndicate of triad gangsters. After a jewelry robbery, they later find Lai (Carrie Ng), a woman who is associated with the gangsters, but end up getting themselves into trouble for going into the womens changing room.

The gang leader, Wing (Lau Kar-wing), learns that one of his henchman, Johnny (Tai Bo), has leaked his secrets so he is stabbed by Wing himself.

During their investigation of the Lais house, things go very wrong for Fatty and Baldy, and they end up imitating robbers in order to escape from the gang. They steal her car and are briefly chase by the gangsters, trashing it along the way. Later that night, Lai calls Fatty and arranges a meeting at an abandoned warehouse. At the location, another fight ensues, followed by a chase of Lais boss, "Prince" Tak (Lung Ming-yan), and Fatty ends up ruining the English Deputy Commissioners wedding.

Fatty and Baldy are ordered to leave Hong Kong while things settle down, so they head to Singapore. However, Lai, who has turned against her gang, is then killed by a pair of transsexual assassins. When Fatty is about to have dinner with Baldy and his girlfriend (Wanda Yung), he inadvertently ruins their relationship. So the pair have dinner outside, leading to another confrontation. Baldy deals with two English henchmen, whilst Fatty is fighting the transsexual assassins.

In the aftermath of the event, Fattys father and Baldys girlfriend are hospitalised. Realising they will never have peace until Wing is stopped, they apprehend Taks brother at an abandoned warehouse full of gas cylinders. The plan goes sour, leading to a final confrontation with the gangsters. The two English henchmen try to take down Baldy while Fatty uses nunchaku. In the midst of the finale, Fatty temporarily knocks Tak down and tries to take Taks brother out. At the final moment, Tak fires his gun, he hits the gas cylinders and Baldy and Fatty escape, killing everyone in the process. In the end, the police chief stops Fatty and Baldy from knocking each other out after a fight over money.

==Cast==
* Sammo Hung - Fatty Dragon
* Karl Maka - Baldy Mak Sui Fu
* Carrie Ng - Lai
* Lung Ming-yan - "Prince" Tak / Ted
* Wu Fung - Officer Wu
* Ni Kuang - Dragons father
* Lau Kar-wing - Wing
* Tai Bo - Johnny
* Yip Seung-wa - Taks brother
* Sin Ho-ying - Ho / Howard
* Ridley Tsui - Pak
* Ng Ching-ching - Jing Jing
* Wanda Yung - tall girl
* Cutie Mui - tall girls neighbour
* Mark Houghton - English gang member
* Wan Seung Lam - gang member
* Gabriel Wong - burglar
* Hung Yan-yan - robber
* Jackson Ng - robber
* Kong Long - robber
* Lo Hung - man at wedding party
* Patrick Gamble - Lais lawyer
* Yeung Yau-cheung - waiter
* Ng Kwok-kin - policeman
* Garry Chan Chi-shing - suspect at police station
* Wilson Yip - man on the Street
* Strawberry Yeung - karaoke singer
* Chang Seng-kwong - thug
* Max Gusinsky - English gang member

==Production==
===Working title===
This movie was previously titled "Tiger on the Beat 3". There was worry that Phillip Ko that worked on the earlier two starring Conan Lee will make a movie with that title so Cinema City announced the title first causing their rival company to come up with another name. Once the rival company has their title resolved, Cinema City retitled this movie again to "Skinny Tiger, Fatty Dragon".
 

===Bruce Lee references===
Sammo doing a Bruce Lee impersonation happen in two early films prior to this. Enter the Fat Dragon and Millionaires Express with Cynthia Rothrock in a brief fight together. This is the final film in which he relates the early Bruce Lee references into the 90s.
*The tap with the two metal bars is seen in Game of Death with Bruce Lee and Dan Inosanto before the nunchaku fight. Also the use of the nunchaku is used in the finale at an abandoned warehouse full of barrels which goes back to Fist of Fury in which Bruce Lee introduces the weapon and Enter the Fat Dragon in which Sammo first used the weapon.
*Sammo holding of one of the henchmans hair is seen in Enter the Dragon in which Jackie Chan tries to attack Bruce but got his neck snapped off camera. Mark Houghton is similar to Way of the Dragon with Bruce Lee and Chuck Norris. Then follows up with Bruce Lee attacking the guy with the gun while in Nora Miaos apartment.
*The German shepherd dog is a reference to the The Big Boss. Also is the use of the double knives in the finale of both movies although shot in a different way.
*During the robbery attack, Sammo attacking the man on the ground with the groin attack is similar to Way of the Dragon with Bruce Lee and Bob Wall.
*Sammos kick shot with Ridley Tsui in the abandoned warehouse is similar to Enter the Dragon with Bruce Lees kick to Shek Tiens head.

===Proposed bad guy role===
When Bey Logan interviewed Mark Houghton, he initially is tasked to find a black businessman which is supposed to be another tribute to Game of Death. Unfortunately he offended a few of them with one who believes he has no business in being in the movie. Houghton thought he would be in trouble for offending a black businessman so he told Lau Kar Wing that he couldnt find any so the director would step in as the main bad guy for this role.

===Location===
Much of the film was recorded in Singapore, but the rest was filmed in the New Territories and Hong Kong.

===Overall look at the film===
In an interview on DVD with Lau Kar Wing, Karl Maka who looked at the film felt there was lack of balance between comedy and action. Initially theres too much action and not enough comedy. So they decided to retune the story so theres enough balance between the action and the comedy.

   

==Box office==
Skinny Tiger, Fatty Dragon earned HK $10,270,954.00 at the Hong Kong box office. {{cite web
  | title = Skinny Tiger and Fatty Dragon (1990)
  | publisher = HKMDb
  | url=http://hkmdb.com/db/movies/view.mhtml?id=7338&display_set=eng
  | accessdate = 2009-03-06 }} 

==Reception==
Bey Logan pointed out in his review why the film did moderately successful. Firstly, there are certain logic and structure issues which is the negative aspect of the movie. A few examples were the car chasing sequence in which he explains that if the heroes just call the police, the bad guys would be arrested, the film would be over and the scene wouldnt be as funny. The other is the Singapore holiday as he explains, after the Chinese wedding catastrophe, the film should be gaining momentum but instead goes down like a holiday expedition. So overall, the pace of the story is let down for that reason.

For western audiences, the character Baldy played by Karl Maka is described as being way over the top. The reason is his character definition from his previous films, most prominently the Aces Go Places series. So the things he does like the watermelon bowling could come across as very broad. On the other hand, its really playing to the less sophisticated Asian audiences at the time, that do like this style of broad humour. Compare to Sammo playing Fatty, his character can be overbearing and hard to enjoy without any qualities that makes us want to enjoy his presence in the film.

The mixed and positive aspect is the action sequences with Sammo Hung. After the Bruceploitation, fans of Bruce Lee would disregard any film that tries to copy Bruce Lees films but never succeeded. However, Sammo Hungs style of action in Logans opinion is regarded the best because of the way he is able to interpret the Bruce Lee in his own style, while designing the fight sequence thats able to pass off early Bruce Lee references from his movies, in a unique fashion.

Also because of his previous work on Enter the Fat Dragon, hes widely regarded as the best Bruce Lee impersonator, simply because of his early appearance in Enter The Dragon and his action choreography and appearance in Game of Death. Also, because of his bulk appearance and the fact he doesnt look like Bruce Lee (apart from his pudding bowl haircut), the most important element in his action sequences is he sells power. When Sammo fights, it doesnt take more than five hits to knock down a stuntman on camera and thats why his fights works so well. That includes as a rarity, Sammo fighting against the Thai "ladyboys" or tranvestites that havent been seen in any other films since.
 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 