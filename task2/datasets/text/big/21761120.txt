Three Men in a Boat (1956 film)
{{Infobox film
 | name = Three Men in a Boat
 | image_size = 
 | caption = 
 | director = Ken Annakin John Woolf (uncredited) Jack Clayton
 | writer = Hubert Gregg Vernon Harris Jerome K. Jerome (novel)
 | music = John Addison Eric Cross
 | editing = Ralph Kemplen
 | narrator = 
 | starring = Laurence Harvey Jimmy Edwards David Tomlinson  Shirley Eaton Romulus Films
 | distributor = Independent Film Distributors
 | released = December 25, 1956
 | runtime = 84 minutes
 | country = United Kingdom English
 | budget = 
 | gross = £212,723 
 | awards = 
 | image = Three Men in a Boat VideoCover.png
}}
 British CinemaScope colour comedy film directed by Ken Annakin and starring Laurence Harvey, Jimmy Edwards, Shirley Eaton and David Tomlinson.  It is based on the 1889 novel Three Men in a Boat by Jerome K. Jerome. The film received mixed reviews, but was a commercial success.

==Synopsis==
The movie Three Men in a Boat is set in the Edwardian era, Harris, Jerome, and George about-town want to get away from it all and decide to take a holiday boating up the River Thames to Oxford, taking with them their dog Montmorency. George is happy to get away from his job at the bank. Harris is glad to get away from Mrs. Willis who is pressing him to marry her daughter Clara; and J is more than anxious to take a holiday from his wife, Etherbertha. George meets three girls, Sophie Clutterhouse and sisters Bluebell and Primrose Porterhouse, who are also taking a ride up the river, and he hopes to see them again. The travellers get into all kinds of complications with the weather, the river, the boat, food, Hampton Court maze, tents, rain, locks. They do connect with the girls again and when things appear to be becoming interesting for the men, Mrs. Willis and her daughter and Ethelbertha show up and things become even more interesting.

==Cast==
* Laurence Harvey - George
* Jimmy Edwards - Harris
* David Tomlinson - Jerome
* Shirley Eaton - Sophie Clutterbuck
* Jill Ireland - Bluebell Porterhouse
* Lisa Gastoni - Primrose Porterhouse
* Martita Hunt - Mrs Willis
* Joan Haythorne - Mrs Porterhouse
* Campbell Cotts - Ambrose Porterhouse
* Adrienne Corri - Clara Willis
* Noelle Middleton - Ethelbertha
* Charles Lloyd-Pack - Mr Quilp
* Robertson Hare - Photographer
* A. E. Matthews - 1st Cricketer
* Miles Malleson - 2nd Cricketer
* Ernest Thesiger - Umpire

==Reception==
The film was the 12th most popular movie at the British box office in 1957. 

==References==
 

 

 
 
 
 
 
 
 
 
 
 


 
 