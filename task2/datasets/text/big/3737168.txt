Carry On Cowboy
 
{{Infobox film
| name           = Carry On Cowboy
| director       = Gerald Thomas
| image	         = Carry On Cowboy FilmPoster.jpeg
| Image size     = 185px
| producer       = Peter Rogers
| writer         = Talbot Rothwell Charles Hawtrey Joan Sims Angela Douglas Eric Rogers
| cinematography = Alan Hume
| editing        = Rod Keys
| studio          = Anglo-Amalgamated Peter Rogers Productions
| distributor    = Associated British Picture Corporation|Warner-Pathé
| released       = November 1965
| runtime        = 93 mins
| country        = United Kingdom
| language       = English
| budget         = £195,000
}} the series Charles Hawtrey and Joan Sims all feature and Angela Douglas makes the first of her four appearances in the series.

==Plot== Stodge City, Albert Earp (Jon Pertwee). Rumpo then takes over the saloon, courting its former owner, the sharp-shooting Belle (Joan Sims), and turns the town into a base for thieves and cattle-rustlers.

In Washington DC, Englishman Marshal P. Knutt (Jim Dale), a "sanitation engineer first class", arrives in America in the hope of revolutionising the American sewage system. He accidentally walks into the office of the Commissioner, thinking it to be the Public Works Department, and is mistaken for a US Peace Marshal, and is promptly sent out to Stodge City.
 Charles Hawtrey) and hanging the Marshal after framing him for cattle rustling. Knutt is saved by the prowess of Annie Oakley (Angela Douglas), who has arrived in Stodge to avenge Earps death and has taken a liking to Knutt.
 a showdown at high noon. By hiding in the sewers beneath the main street, Knutt kills off Rumpos gang, but fails to capture Rumpo, who escapes with the aid of Belle.

==Music==
Carry on Cowboy was the first film in the series to have a sung main titles theme. Douglas has a nightclub scene in which she sings "This is the Night for Love".

==Cast==
{{columns-list|2|
*Sid James as Johnny Finger/The Rumpo Kid
*Kenneth Williams as Judge Burke
*Jim Dale as Marshal P Knutt Charles Hawtrey as Big Heap
*Joan Sims as Belle
*Peter Butterworth as Doc
*Bernard Bresslaw as Little Heap
*Angela Douglas as Annie Oakley
*Jon Pertwee as Sheriff Albert Earp Percy Herbert as Charlie
*Sydney Bromley as Sam Houston
*Edina Ronay as Delores
*Lionel Murton as Clerk
*Peter Gilmore as Curly
*Davy Kaye as Josh
*Alan Gifford as Commissioner
*Brian Rawlinson as Stagecoach guard
*Michael Nightingale as Bank manager
*Simon Cain as Short
*Sally Douglas as Kitkata
*Cal McCord as Mex
*Garry Colleano as Slim
*Arthur Lovegrove as Old cowhand
*Margaret Nolan as Miss Jones Tom Clegg as Blacksmith
*Larry Cross as Perkins Brian Coburn as Trapper
*Ballet Montparnasse as Dancing girls
*Hal Galili as Cowhand
*Norman Stanley as Drunk
*Carmen Dene as Mexican girl
*Andrea Allan as Minnie
*Vicki Smith as Polly
*Audrey Wilson as Jane
*Donna White as Jenny
*Lisa Thomas as Sally
*Gloria Best as Bridget
*George Mossman as Stagecoach driver
*Richard OBrien as Rider Eric Rogers as Pianist
}}

==Crew==
*Screenplay – Talbot Rothwell Eric Rogers Eric Rogers & Alan Rogers
*Associate Producer – Frank Bevis
*Art Director – Bert Davey
*Editor – Rod Keys
*Director of Photography – Alan Hume
*Camera Operator – Godfrey Godar
*Assistant Director – Peter Bolton
*Unit Manager – Ron Jackson
*Make-up – Geoffrey Rodway
*Sound Editor – Jim Groom
*Sound Recordists – Robert T MacPhee & Ken Barker
*Hairdressing – Stella Rivers
*Costume Designer – Cynthia Tingey
*Assistant Editor – Jack Gardner
*Horse Master – Jeremy Taylor
*Continuity – Gladys Goldsmith
*Producer – Peter Rogers
*Director – Gerald Thomas

==Filming and locations==

*Filming dates – 12 July-3 September 1965

Interiors:
* Pinewood Studios, Buckinghamshire

Exteriors:
* Chobham Common, Surrey
* Black Park, Fulmer, Buckinghamshire

==Bibliography==
* 
* 
* 
* 
*Keeping the British End Up: Four Decades of Saucy Cinema by Simon Sheridan (third edition) (2007) (Reynolds & Hearn Books)
* 
* 
* 
* 
* 

==External links==
*  
*  
*  
*  at the British Film Institutes Screenonline

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 