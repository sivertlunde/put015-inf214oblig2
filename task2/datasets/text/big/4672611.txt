Teen Witch
 
{{Infobox film
| image          = Teen witch vhs 1989.jpg
| alt            = A teenage girl with textbooks, a gypsy-looking witch, and a male wearing black tank top.  
| caption        = U.S. VHS cover
| director       = Dorian Walker
| producer       = Bob Manning Alana H. Lambros Eduard Sarlai Moshe Diamant Rafael Eisenman
| writer         = Robin Menken Vernon Zimmerman
| starring       = Robyn Lively Dan Gauthier Joshua John Miller
| music          = Larry Weir Richard Elliot
| cinematography = Marc Reshvosky
| editing        = Natan Zahavi
| distributor    = Metro-Goldwyn-Mayer Trans World Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $2.5 million
| gross          = $27,843
}} rap musical cult classic, 1980s fashion nostalgia. 

==Plot==
After a bike accident, sweet, nerdy, 15-year-old Louise Miller (Lively) knocks on the door of a strange-looking house, hoping to use the phone. Instead she finds a strange but welcoming woman, the seer Madame Serena (Rubinstein). Serena is stunned to realize that Louise is a reincarnated witch and an old friend of hers. On Louises 16th birthday, her magical powers return  through a powerful amulet she lost in a former life.

Now that Louise has the power to make her dreams come true, she casts a popularity spell to win over Brad (Gauthier), the hottest guy in school, without earning his love.

Louise then becomes the most popular girl in school while getting back at her harassing English teacher, Mr. Weaver, and the cheerleaders who never respected her. It is only after her popularity spell gets out of hand and Louise realizes that believing in yourself is the true magic that she gives up her power to make her own happy ending.

==Cast==
* Robyn Lively as Louise Miller
* Dan Gauthier as Brad Power
* Joshua John Miller as Richie Miller
* Caren Kaye as Margaret Miller
* Dick Sargent as Frank Miller
* Lisa Fuller as Randa
* Mandy Ingber as Polly Goldenberg-Cohen (billed as Amanda Ingber)
* Tina Caspary as Shawn
* Zelda Rubinstein as Madame Serena
* Shelley Berman as Mr. Weaver
* Marcia Wallace as Ms. Malloy
* Cindy Valentine as Shana the Rock Star

==Soundtrack  ==
# "All Washed Up" - Larry Weir
# "Dream Lover" - Cathy Car
# "Finest Hour" - Cindy Valentine featuring Larry Weir
# "High School Blues" - The Puppy Boys
# "I Keep on Falling" - Blue Future
# "I Like Boys" - Elizabeth & The Weirz
# "Get Up and Move" - Cathy Car
# "Much too Much" - Cathy Car
# "Never Gonna Be" (opening sequence) - Lori Ruso
# "Never Gonna Be" (concert version) - Cindy Valentine
# "Popular Girl" - Theresa & The Weirz
# "Rap" - Philip McKean & Larry Weir
# "Shame" - The Weirz
# "Top That" - The Michael Terry Rappers
# "In Your Arms" - Richard Elliot

Music was recorded at Weir Brothers Studio. 

==Box office and reception== April 1989 box office competition included Field of Dreams, starring Kevin Costner and Pet Sematary (film)|Pet Sematary, written by Stephen King. Both films were released on April 21, 1989, two days before the Teen Witch release.

Teen Witch is a cult classic, having gained newer, younger audiences after multiple airings on cable network channels such as HBO and Cinemax in the 1990s.         Jarett Wieselman of the New York Post stated, "There are good movies, there are bad movies, there are movies that are so bad theyre good and then there is Teen Witch -- a cult classic that defies classification thanks to a curious combination of songs, spells and skin."  Joshua John Miller stated of his involvement with the film as character Richie, "If you look at Teen Witch, it was a very campy performance. But its a really fun film and something I have grown to honor." 
 parodies or homages of the film, especially of its rap song "Top That" (including a homage starring Alia Shawkat).     Drew Grant of Nerve (website)|Nerve.com stated, "If youve never seen the original rap scene from the 80s classic Teen Witch, you must immediately stop what youre doing and watch it right now. Its everything wonderful and terrible about that decade rolled into one misguided appropriation of... hip-hop."  Stephanie Marcus of The Huffington Post called "Top That" "the worst song of all time."   

On July 12, 2005, MGM released the film to DVD in its original widescreen theatrical version. In 2007, ABC Family acquired the television rights and has since re-aired it regularly as part of their yearly 13 Nights of Halloween movie specials.   

==Accolades==
 
|+ Eleventh Annual Youth in Film Awards 1988-1989  
|-
| 1989
| Best Young Actor Starring in a Motion Picture
| Young Artist Awards: Joshua John Miller
|  
|-
| 1989 
| Best Young Actress Starring in a Motion Picture
| Young Artist Awards: Robyn Lively
|  
|}

==Adaptations==
 
 Tom Weir earned a Grammy Award for True Love (Toots & the Maytals album)|True Love in 2004. The Weir brothers created Caption Records and collaborated with Teen Witch film producer Alana Lambros for the Teen Witch the Musical project.  
 Broadway bound production to the project. 

In 2007, the audio CD for Teen Witch the Musical was released, a new generation of musically talented actors were cast for the stage-play, which was presented in workshop. This adaptation never found a larger venue. 

The cast of Teen Witch the Musical: 

 
* Alycia Adler as Randa (Cheerleader)
* Bryce Blue as Rhet
* Blake Ewing as Brad Powell
* Ashley Crowe as Madame Serena
* Monet Lerner as Darcy (Cheerleader)
* Tessa Ludwick as Phoebe (Cheerleader)
* Lauren Patten as Polly
* Sara Niemietz as Louise Miller
* Heather Youmans as Shana the Rock Star
* V-Style as rapper 
 

In April 2008, Variety (magazine)|Variety reported that Ashley Tisdale signed with FremantleMedia North America and was in talks with United Artists to star in a remake of Teen Witch. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 