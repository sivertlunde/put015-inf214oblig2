The Starving Games
{{Infobox film
| name           = The Starving Games
| image          = The Poster for The Starving Games.jpg
| caption        = Theatrical release poster
| alt            = A chicken roasting on a fiery spit Jason Friedberg Aaron Seltzer
| producer       = Peter Safran
| writer         = Jason Friedberg Aaron Seltzer
| starring       = {{Plain list | 
* Maiara Walsh
* Cody Christian
* Brant Daugherty
* Lauren Bowles
* Diedrich Bader
}}
| music          = Timothy Michael Wynn
| cinematography = 
| editing        = 
| studio         = Safran Company 3 in the Box
| distributor    = Ketchup Entertainment
| released       =  
| runtime        = 83 minutes 93 minutes (unrated cut)
| country        = United States
| language       = English
| budget         = $4.5 million
| gross          = $3.8 million 
}} The Hunger Games and directed by Jason Friedberg and Aaron Seltzer. The film stars Maiara Walsh, Cody Christian, Brant Daugherty, Lauren Bowles and Diedrich Bader. It is Friedberg and Seltzers first film to be distributed independently after a long relationship with each other, being released simultaneously in theaters and video on demand by distribution start-up Ketchup Entertainment. 

== Plot == the Wizard of Oz. They return to District 12, where the residents fight over food. At the Gathering for the 75th annual Starving Games, president Snowballs explains he separated America into districts to keep "stupid stuff" from happening again, such as Lady Gaga being president; due to this, the rich now dress in bright and odd clothing. During the drawing for District 12s contestants, trouble occurs as vulgar name puns get drawn first. Petunia, Kantmisss sister, tricks Kantmiss into volunteering when she gets picked; Dale tries volunteering too, but idiotic Peter Malarky beats him to it.
 
When the game begins, Kantmiss watches others fight for equipment as their numbers dwindle, due to getting what they want or being killed in scruffles. Kantmiss grabs a backpack and tries fleeing, but the producer sends Angry Birds to attack her; she defeats them and squashes the Annoying Orange. Peter teams up with Marcos group, who aim to kill Kantmiss. They flee when she knocks down a beehive; Kantmiss is stung, and the venom causes her to hallucinate she is a Avatar (2009 film)|Navi until she is slapped back to her senses by fellow contestant Rudy; they team up, vowing to kill the other contestants first. Kantmiss kills some by playing on weaknesses. Rudy gets killed saving Kantmiss.
 
Wanting to spice up the games, Snowballs wants a lesbian love story, but learns Kantmiss is the only woman left; thus only a straight romance is possible. The producer announces a couple can win the games, making Kantmiss decide to team up with Marco over Peter; however, the producer fakes being killed to convince her to search for Peter instead. She finds Peter, whos been badly injured by Marcos group via rough back waxing. Kantmiss kills all but Marco, fleeing with Peter to a cave. Since Kantmiss is keeping distant from Peter, the producer offers a trade; hell send medical supplies to treat Peter if she gets more intimate with him. As Kantmiss and Peter start to have sex, Gandalf and Dwarves appear in the cave, lecherously groping her; they are promptly evicted. To get better medical supplies, Kantmiss has sex with Peter, which is televised. Dale is left disgusted by this and barges into the Starving Games arena.
 
The next day, Peter tries getting Kantmiss to have sex with him again, but she nearly crushes his fingers in response. They and Marco end up walking around the same tree, to the annoyance of the audience. Kantmiss and Peter surprise Marco, but Snowballs orders Seleca (the producer), to send in the "big guns", which is sending in the Expendables, but an armed Dale arrives, killing them all, telling Kantmiss to come back with him. Kantmiss orders him to leave; he promptly breaks up with her, while she dismisses him as clingy. Marco holds Peter hostage, but Kantmiss shoots a loaf of bread into his eye, killing him. The producer announces there can only be one winner again. Peter tries convincing Kantmiss to commit suicide with him by ingesting poisonous berries, to not give those in charge the satisfaction of them fighting to the death; Kantmiss kills Peter with an arrow, saying it is nothing personal. Afterwards, Nick Fury and the Avengers show up, with Fury saying he wants Kantmiss to join the Avengers team as a replacement for Hawkeye (comics)|Hawkeye. Then, the Avengers all die in an explosion.

== Reception == Review aggregation website Rotten Tomatoes gives the film a score of 0% based on reviews from 11 critics. 

Joe Leydon of Variety (magazine)|Variety called it a "stillborn spoof" and "desperately unfunny".  Scott Foy of Dread Central rated it 1.5/5 stars and wrote, "The printed word cannot fully express my dismay at having experienced this latest alleged comedy". CM says "This is insulting to the real Hunger Games, and is aggravating to anyone who even watches it!  Gabe Torio of Indiewire wrote that the film "is as terrible as you think it is".  Max Nicholson of IGN called it "a horrible, horrible piece of cinema that neednt be watched by any person ever."  Fred Topel of Crave Online rated it 1.5/10 and called it "more of the same, only worse." 

== Box office performance ==
For the first time in Friedberg and Seltzers careers, the movie was a box office bomb, with only $3,889,688 earned against its $4.5 million budget.

== Parodies ==
 Harry Potter The Expendables
*Angry Birds
*Psy (Dancing "Gangnam Style")
*Taylor Swift
*Avatar (2009 film)
*Oz the Great and Powerful
*McDonalds (Advertisement)
*Annoying Orange
*Fruit Ninja The Hobbit The Avengers Nike
*Starbucks
*Sherlock Holmes (slow-mo fight prediction scene)
*LMFAO

==See also==
*The Hungover Games

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 