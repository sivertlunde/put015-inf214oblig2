The Flavor of Green Tea over Rice
{{Infobox film
| name = Ochazuke no aji The Flavor of Green Tea over Rice
| image = Ochazuke no aji poster.jpg
| caption = Original Japanese movie poster
| director = Yasujirō Ozu
| producer =
| writer = Kōgo Noda, Yasujirō Ozu
| starring = Shin Saburi Michiyo Kogure Kōji Tsuruta Chikage Awashima
| music = Ichirō Saitō
| cinematography = Yuuharu Atsuta
| editing = Yoshiyasu Hamamura
| distributor = Shochiku
| released = 1952
| runtime = 115 minutes
| country = Japan
| language = Japanese
| budget =
| preceded_by =
| followed_by =
}}
 
  is a 1952 Japanese film directed by Yasujirō Ozu about a wealthy middle-aged couple (played by Shin Saburi and Michiyo Kogure) who have marital difficulties, and their niece who uses the couples troubles as her excuse for not attending arranged marriage interviews.

==Synopsis==
Taeko (Michiyo Kogure) and Mokichi Satake (Shin Saburi) are a childless married couple living in Tokyo. The husband is an executive at an engineering company, whom the wife thinks dull.

Taekos friend Aya (Chikage Awashima) gets Taeko to lie to her husband that her niece, Setsuko (Keiko Tsushima), is ill to go to a spa with a couple of friends. The plan goes wrong when Setsuko visits her house unexpectedly, but Taeko substitutes the invalid with another friend and obtains consent from her husband to go for a break.

Setsuko has to go for a matchmaking session although she is unwilling to do so.  Seeing that Mokichi and Taeko are not happy after their arranged marriage, she is determined to find her own spouse.  Mokichis family tasks Taeko to act as matchmaker for Setsuko at a kabuki theater, but Setsuko runs off midway and goes to see her uncle, who brings her back to the theater and leaves, after which she runs off again.  She joins Mokichi at a pachinko parlor with his younger friend Noboru (Koji Tsuruta).  Mokichi asks Noboru to bring Setsuko back home as he heads home.

Setsuko does not go home, but instead goes to her uncles house, where Taeko is fuming.  When Taeko realizes that Setsuko and Mokichi have been together, she becomes so angry that she refuses to speak to her husband for days.  The two have a confrontation, when Mokichi tells her that he finds his old habits hard to break because smoking inferior cigarettes and traveling third-class on a train remind him of the simpler pleasures of life.  Taeko leaves in a huff.

Taeko goes on a solo train journey away from Tokyo.  Mokichi telegrams her to tell her that his company is sending him to Uruguay on a business trip, and asks her to return.  Taeko returns only after Mokichi has flown.  Mokichi returns a few hours later since the airplane carrying him has met with technical mishaps and has to head back to Tokyo.  The couple make up while preparing ochazuke, rice with green tea.  Taeko realizes what her husband has been speaking about earlier on.  She promises never to leave without a word again.

The film ends with Setsuko confiding with Noboru over her aunts changed attitude, and the final scene suggests that the two have become a couple.

== Cast ==
*Shin Saburi - Mokichi Satake
*Michiyo Kogure - Taeko Satake
*Kōji Tsuruta - Non-chan / Noboru Okada
*Chishū Ryū - Sadao Hirayama
*Chikage Awashima - Aya Amamiya
*Keiko Tsushima - Setsuko Yamauchi
*Kuniko Miyake - Chizu Yamauchi
*Eijirō Yanagi - Naosuke Yamauchi

==Production==

===Script=== red beans and rice, because the man was leaving to serve in the army. Ozu then shelved the project. 

==References==
 

==Bibliography==
* 

==External links==
*  
* 

 

 

 
 
 
 
 
 
 
 
 
 
 