Aparadhi (2009 film)
{{Infobox film
| name           = Aparadhi
| image          = Aparadhi.jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Subhash Sen
| producer       = 
| writer         = Kamal Sarkar
| screenplay     = Kamal Sarkar
| story          = Kamal Sarkar
| based on       =  
| narrator       =  See below
| music          = Bappi Lahiri
| cinematography = Rana Dasgupta
| editing        = Atish De Sarkar 
| studio         = 
| distributor    = Sree Sree Productions
| released       = 2009
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Subhash Sen under the banner of  Sree Sree Productions. The film features actors Prosenjit Chatterjee, Priyanka Trivedi and Victor Banerjee in the lead roles. Music of the film has been composed by Bappi Lahiri      

== Plot ==
Prosanto Mullick is a well-known and successful businessman.    He is so much in love with Debi, his wife that he readily walks out of a business meeting where the deal amounts to around Rs.7 crore when a telephone call from home informs that she is ill. She is all decked up and ready to wish him happy wedding anniversary, piano, song and all. The tell-tale white streaks in her hair show that they have been married for a while. She faints as the song ends and you’ve guessed it – she is pregnant. But hubby dear is not happy. When she delivers twins, he hates to share her with them. Enter villain Charandas, an old friend of Prosanto who had the twitters for Debi but is out to avenge the stinging slap she gave him before he went to jail. He wants to set up a business. Prosanto writes out a cheque for Rs.3 crore. But Charandas’ intentions are different. He kidnaps the older of the twins but to avoid being caught by the neighbourhood public, dumps the baby into the community dustbin and makes good his escape. Rahmat, the neighbourhood thief, picks up the infant, takes him home and brings him up, without keeping the story of how he found him a secret. Debi suspects that her husband of the kidnap because she finds his wrist-watch on the floor. During a heated argument, she falls off the stairs, loses her sanity and is placed in a mental home. Prosanto is jailed for 14 years and in the meanwhile, the kidnapped twin who has named himself Arjun, grows up to become the modern Robin Hood of the locality, fighting the bad ones for justice. Nandini, Charandas’ beautiful daughter, falls in love with him at first sight when he steps into her marriage mandap and rescues her from marriage to a politician’s villainous son. Though he does his quota of singing and dancing, he longs for the mother he thinks threw him away and feels a strange pull towards the crazy Debi when Prosanto, out of jail, asks him to help him fight Charandas for revenge. Around this time, the second twin, Akash, flies back home with a degree in psychiatric medicine under his arm. He wants to cure his mother and also falls in love with a fellow-passenger on his way back home. Some more singing and dancing follows while Arjun becomes a trusted aide of Prosanto, each one unaware of the father-son tie they are bound by. With a great deal of action scenes filled with fights, fisticuffs, breaking of ropes and so on, Charandas is defeated in his devious plans of decimating the family he hates, Debi is cured completely, Arjun and Nandini are united, blood ties are reinforced, and you just wait to hear the still photographer say ‘smile’ for the last group photograph.

== Cast ==
* Prosenjit Chatterjee
* Priyanka Trivedi
* Victor Banerjee
* Laboni Sarkar
* Sanjib Dasgupta
* Diganta Bagchi
* Raj Rajjak
* Sunil Kumar
* Sunil Mukherjee

== References ==
 

==External links==
*  
*  

 
 
 
 
 