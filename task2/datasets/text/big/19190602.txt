Always in the Way
{{Infobox film
| name           = Always in the Way
| image          = Always_in_the_way_poster.jpg
| image size     =
| caption        = Theatrical Poster
| director       = J. Searle Dawley
| producer       = Louis B. Mayer
| writer         = Charles Harris
| narrator       =
| starring       = Mary Miles Minter
| music          =
| cinematography = Irvin Willat
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        = 50 min.
| country        = United States Silent English intertitles
| budget         =
}}

Always in the Way is a 1915 silent film directed by J. Searle Dawley. The film, which was based on the song with the same name, was partially filmed on the Bahamas. 

==Plot==
Dorothy North is the four-year-old child of the wealthy lawyer Winfred North. Her mother is deceased and her father is remarrying widow Helen Stillwell. Helen is annoyed with the presence of Dorothy and treats her as if she is always in the way. She runs away home and is taken under care by missionaries. While Dorothy accompanies her new foster parents to Africa, Helen informs Winfred his daughter is lost.

Winfred unsuccessfully tries to find Dorothy, who is growing up to a young woman in Africa. At the age of fifteen, her foster parents are killed by Zulus. She is separated from her sweetheart John Armstrong and boards back to America. Here, she finds employment in a florist shop. John followes her to New York City and locates her real father. They are all reunited and Winfred breaks with his Helen. 

==Cast==
* Mary Miles Minter - Dorothy North
* Ethelmary Oakland - Dorothy North (child)
* Lowell Sherman - Winfred North
* Edna Holland - Mrs. Helen Stillwell
* Mabel Greene - May Stillwell
* Harold Meltzer - Alan Stillwell
* Charlotte Shelby - Mrs. Goodwin

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 