Chaque jour a son secret
{{Infobox film
| name           = Chaque jour a son secret
| image          = 
| caption        = 
| director       = Claude Boissol
| producer       = Gray-Film, Socipex, Vascos Films
| writer         = Paul Andréota Claude Boissol Pierre Laroche
| starring       = Jean Marais Danièle Delorme
| music          = Eddie Barclay
| cinematography = 
| editing        = 
| distributor    = 
| released       = 11 June 1958 (France); 3 April 1959 (West Germany)
| runtime        = 90 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
}}
 French thriller film from 1958, directed by Claude Boissol, written by Paul Andréota, starring Jean Marais. The scenario was based on a novel of Maria Luisa Linarès. 

== Cast ==
* Jean Marais : Xavier Lezcano
* Danièle Delorme : Olga Lezcano
* Françoise Fabian : Hélène Lezcano, la second wife of ethnologue
* Marcelle Praince : Mrs Lezcano, mère
* Denise Gence : Fina, la gouvernante
* Yves Brainville : Le juge dinstruction
* Robert Le Béal : director of "France-Europe"
* Germaine Delbat : une commère
* Alain Nobis : gendarme
* Raphaël Patorni : Yves Rollin, lattaché de cabinet
* Raymond Loyer : doctor Destouches
* Simone Logeart :
* André Dumas : Tony
* Jean Michaud : minister
* Lucien Frégis : gendarme à vélo

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 


 
 