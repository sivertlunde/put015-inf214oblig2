Tourist Bunglow
{{Infobox film
| name           = Tourist Bunglow
| image          =
| caption        =
| director       = AB Raj
| producer       =
| writer         = Balu Mahendra Sreemoolanagaram Vijayan (dialogues)
| screenplay     = Sreemoolanagaram Vijayan
| starring       = Prem Nazir Jayabharathi Kaviyoor Ponnamma Adoor Bhasi
| music          = M. K. Arjunan
| cinematography = Balu Mahendra
| editing        = MS Mani
| studio         = HR Films
| distributor    = HR Films
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed AB Raj. The film stars Prem Nazir, Jayabharathi, Kaviyoor Ponnamma and Adoor Bhasi in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Prem Nazir
*Jayabharathi
*Kaviyoor Ponnamma
*Adoor Bhasi
*Bahadoor
*MG Soman
*N. Govindankutty Vincent
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by ONV Kurup.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chellu Chellu Menake || P Jayachandran || ONV Kurup ||
|-
| 2 || Kaanal Jalathin || LR Eeswari || ONV Kurup ||
|-
| 3 || Kalivilakkin || K. J. Yesudas || ONV Kurup ||
|-
| 4 || Kannezhuthi Pottuthottu || Sujatha Mohan || ONV Kurup ||
|-
| 5 || Premathinu Kannilla || Cochin Ibrahim, Zero Babu || ONV Kurup ||
|}

==References==
 

==External links==
*  

 
 
 


 