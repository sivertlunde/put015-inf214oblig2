Bad Girls from Valley High
{{Infobox film |
  name=Bad Girls from Valley High|
  image=Bad Girls from Valley High.jpeg|
  writer=Robert LoCash  |
  starring=Julie Benz Monica Keena Nicole Bilderback Jonathan Brandis Janet Leigh  Christopher Lloyd Aaron Paul|
  director=John T. Kretchmer | 
  producer=Gerard Bocaccio (EP) |
  distributor=Universal Home Entertainment |
  released=   |
  runtime=84 minutes |
  language=English |
  |}}

Bad Girls from Valley High  is a 2005 direct-to-video comedy film starring Julie Benz, Monica Keena, Nicole Bilderback, Jonathan Brandis (in his final film role), Janet Leigh, Christopher Lloyd, and Aaron Paul.

==Plot==
 
Danielle (Julie Benz), Tiffany (Nicole Bilderback) and Brooke (Monica Keena) are the three most popular and most nasty girls in high school. While the leader Danielle is used to getting what she wants, she is unable to attract lonesome ex-jock Drew (Jonathan Brandis) due to his mourning over the death of his girlfriend Charity Chase. Although Charity was believed to have committed suicide, this wasnt the case as Danielle, Tiffany and Brooke lured Charity to a cliff and killed her. 

A year to the day of Charitys death, Romanian foreign exchange student Katarina (Suzanna Urszuly) arrives during the class of clumsy Media Arts Professor Mr. Chauncey (Christopher Lloyd). She and Drew immediately become friends. Jealous from this, Danielle tries to do everything in her power to stop this friendship developing into love. In an attempt to get close to Drew, Danielle works at the elderly home where Drew is also working. She is assigned to look after an old lady (Janet Leigh) whom she believes is in a coma. Danielle, Tiffany and Brooke use this opportunity to raid the old ladys cupboard and eat her box of chocolates. 
 aging at a rapid speed. They believe this has something to do with Katarina whom they now think is in fact Charitys ghost coming back to seek revenge. The three decide the only way to regain their youth is to kill Drew and let his spirit be with Charity. On the night of Danielles 18th birthday party, the three lure Drew to the same gorge where Charity died and attempt to shoot him. Katarina shows up and says she is NOT Charitys ghost. Danielle briefly ponders this but decides to shoot them both anyway. But, Brooke (the kindest of all the three) says that they have gone too far and tries to prevent Danielle from pulling the trigger to which Drew disarms her (due to Danielle being distracted by a party guest dressed in a clown suit from her party who is secretly Mr. Chauncey and she shoots him), and both Tiffany and Danielle become overcome from exhaustion. 

After theyre carried to the old age home, Tiffany is hooked on a life support machine and Danielle is barely alive. At that moment, Mrs. Witt, the old woman who Danielle was meant to be caring for, shows up and reveals that she was Charitys grandmother. Also while she had been briefly unable to speak due to a stroke, she had very good hearing and sight and overheard Danielle bragging about Charitys murder. She then reveals she poisoned the chocolate box (knowing that the girls would eat it) with an aging chemical (thanks to her late friends husband that works with biological warfare technology). While Danielle and Tiffany had eaten most of the poisoned chocolate, Brooke wasnt near death because she didnt eat as many and demonstrated self-control. In response to this revelation, Danielle sticks her middle finger up at Mrs. Witt then dies (and Tiffany presumably dies shortly afterwards). 

At Danielles and Tiffanys funeral, everyone is in attendance, including Drew and Katarina (now an official couple) attends. Brooke is also in attendance after a plastic surgeons operation give her a 50-year-olds appearance. Chauncey forgives her as he knew she wasnt as cruel as Danielle and Tiffany, and she regrets what she has done. Danielle and Tiffany are then revealed to be in a luxurious room with their youth restored and are convinced that they are in heaven. But it is revealed that they are actually in hell as they are forced to forever endure the company of their schools most annoying dork (Aaron Paul) who is completely devoted to Danielles every move. So much so, that he committed suicide himself just to be with her forever and briefly morphs into the devil to her and Tiffanys horror.

==Production== novel of the same name. The film was shot on location in Vancouver, British Columbia. Several scenes were filmed at the Cleveland Dam.

==Release== Cannes Film Market in France. Bad Girls From Valley High also marks the last screen appearances of both Jonathan Brandis and Janet Leigh.

== External links ==
*  
*  
*  

 
 
 
 
 