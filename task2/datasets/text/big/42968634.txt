The False Step
{{Infobox film
| name =  The False Step
| image =
| image_size =
| caption =
| director = Gustaf Gründgens
| producer = Gustaf Gründgens
| writer =  Theodor Fontane (novel)   Georg C. Klaren   Eckart von Naso
| narrator = Paul Hartmann   Max Gülstorff
| music = Mark Lothar  
| cinematography = Ewald Daub  
| editing =  Johanna Schmidt     
| studio = Terra Film
| distributor = Terra Film
| released =  9 February 1939   
| runtime = 101 minutes
| country = Nazi Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Paul Hartmann.  It is an adaptation of Theodor Fontanes 1896 novel Effi Briest.

==Cast==
* Marianne Hoppe as Effi Briest  
* Karl Ludwig Diehl as Baron von Instetten  Paul Hartmann as Major a.D. von Crampas  
* Max Gülstorff as Dr. Alonzo Gieshübler 
* Paul Bildt as Herr von Briest  
* Käthe Haack as Luise von Briest 
* Hans Leibelt as Ministerialrat Wüllersdorf  
* Elisabeth Flickenschildt as Marietta Tripelli  
* Gisela von Collande as Afra 
* Renée Stobrawa as Roswitha  
* Erich Dunskus as Kruse 
* Flora Berthold as Hulda  
* Ursula Voß as Herta  
* Ursula Friese as Berta 
* Margarete Schön as Frau von Padden  
* Jac Diehl as Gast beim Fest
* Alfred Karen as Gast bei Tripellis Gesangsvortrag 
* Alexander Kökert as Besucher beim Landrat 
* Bert Schmidt-Moris as Page im Kurhotel 
* Tina Schneider as Doddy Grützmacher 
* Babsi Schultz-Reckewell as Annie Briest 
* Maria Seidler as Frau Konsul Grützmacher

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 