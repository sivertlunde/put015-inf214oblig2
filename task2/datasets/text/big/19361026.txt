The Seed of Discord
{{Infobox film
| name = The Seed of Discord
| image =Il_seme_della_discordia_-_locandina.jpg
| caption =
| director = Pappi Corsicato
| producer = Marco Poccioni, Marco Valsania
| writer = Heinrich von Kleist (Novel), Massimo Gaudioso, Pappi Corsicato
| starring = Caterina Murino  Alessandro Gassman   Martina Stella 
| music =
| cinematography = Ennio Guarnieri
|  editing = Giogiò Franchini
| production_company = Rodeo Drive, Medusa Film, Sky Cinema
| distributor =
| released =  
| runtime = 85 minutes
| country = Italy
| language = Italian
| budget = 300,000 €
| gross = 674,151 €
}}
 Italian film.
The film is a modernisation of Heinrich von Kleists novel The Marquise of O.  It was entered into the main competition at the 65th Venice International Film Festival.

== Plot ==
 
A married and faithful Italian woman finds herself pregnant the same day her husband is discovered to be sterile.
In the original sound track is included a famous song of Sixties: the Mina (singer)|Minas version of Canta Ragazzina

== Cast ==
* Caterina Murino — Veronica
* Alessandro Gassman — Mario
* Martina Stella — Nike
* Valeria Fabrizi — Veronicas mother
* Michele Venitucci — Gabriele
* Angelo Infanti — Veronicas father
* Lucilla Agosti — Dancer
* Iaia Forte — Lover
* Monica Guerritore — Doctor
* Rosalia Porcaro — Lover
* Isabella Ferrari — Monica
* Eleonora Pedron — Gabrieles girlfriend

==References==
 

== External links ==
*  
*  
*   on YouTube

 

 
 
 
 
 


 