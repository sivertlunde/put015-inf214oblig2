Bhai Amar Bhai
{{Infobox film
| name           = Bhai Amar Bhai 
| image          = Bhai amar bhai movie poster.jpeg
| image_size     =  
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = Bhai amar Bhai movie poster
| director       = Swapan Saha
| producer       = Shree Venkatesh Films
| writer         = 
| screenplay     = 
| story          = Swapan Saha
| based on       =  
| narrator       =  Rozina
| music          = Anupam Dutta
| Lyric            = Pulak Bandopadhyay
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1996
| runtime        = 
| country        = India Bengali
| budget         =  INR 65 LAKHS
| gross          = INR 1 Cr+
}}
 Blockbuster Cinema Bengali film Rozina in the lead roles. Music of the film has been composed by Anupam Dutta   

== Plot ==
Sanjay(Abhishek Chatterjee), Mala(Rozina) and Amar(Prosenjit Chatterjee) are siblings who live in a village with their father, Master. Malas marrieage was fixed with Arun(Chiranjit Chakraborty). Sashi Shekhar, the wicked zamindar always tried to use force upon the villagers. Once Master slaps Sashi in fornt of the entire village after which Sashi promises to take revenge. One night Sashi, with some goons attacks Masters house, murders him and sets the house on fire. Luckily the three children escape. While the children were escaping on train, Amar(Prosenjit Chatterjee) gets separated from the other two. Kajal, a nurse, finds Mala and Sanjay and takes care of them while Amar is adopted by a drunk garage mechanic, Mantu who renames Amar(Prosenjit Chatterjee) as Raja(Prosenjit Chatterjee). When they grow up Mala becomes a nure while Raja becomes a mechanic. Once Raja also saves Mala from some goons, but the two dont recognise each other. Sanjay falls in love with Mita, Sashi Shekhars daughter while Mala is in love with Arun, not knowing that the two were indeed supposed to get married. Sashi Shekhar once again plots to separate Sanjay from Mala by trying to bring Sanjay under his influence and is successful in creating a rift between brother and sister. But in the process, Mala and Raja(Prosenjit Chatterjee) recognize Sashi babu and in turn their own relation. Ultimately Amar(Prosenjit Chatterjee) Mala, Sanjay and Arun fight jointly to overpower Sashi babu.

== Box Office ==
Bhai Amar Bhai is the first Shree Venkatesh Films Production film.This is Biggest Blockbuster film.This is the first 1 crore earner film in tollywood. 

== Cast ==

* Prosenjit Chatterjee as Amar/Raja
* Anushree Das
* Abhishek Chatterjee as Sanjay
* Chiranjit Chakraborty as  Arun Rozina as Mala
* Subhendu Chatterjee
* Robi Ghosh
* Soma Dey
* Anamika Saha
* Mitali

== References ==
 
 
 

 
 
 


 