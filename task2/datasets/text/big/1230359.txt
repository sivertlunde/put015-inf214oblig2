Boundin'
{{Infobox film
| name           = Boundin
| image          = Boundin poster.jpg
| caption        = Poster for Boundin
| alt            = Poster for Boundin
| director       = Bud Luckey co-director Roger L. Gould
| producer       = Osnat Shurer
| writer         = Bud Luckey
| starring       = Bud Luckey
| narrator       = Bud Luckey
| music          = Bud Luckey
| cinematography = Jesse Hollander
| editing        = Steve Bloom
| studio         = Pixar Animation Studios Walt Disney Pictures Buena Vista Pictures
| released       =  
| runtime        = 5 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Boundin is a 2003 Pixar computer animation|computer-animated short film which was shown in theaters before the feature-length film The Incredibles.    The short is a musically narrated story about a dancing sheep who loses his confidence after being sheared. The film was written, directed, narrated and featured the musical composition and performance of Pixar animator Bud Luckey.

==Plot==
The film features a sheep that lived in the American West whose elegant dancing is popular with the other animals. One day the sheep-shearers arrive and shear it for wool. Having lost his coat, the sheep becomes shy and loses the confidence to dance so elegantly.  It is whilst in his bare state that a benevolent jackalope comes across the little lamb and teaches him the merits of "bounding" rather than dancing (that is, getting up whenever you fall down). The sheep is converted and its joy in life is restored. The sheeps wool eventually grows back in the winter, only for it to be cut again, but his pride is now completely unshaken and he continues to "bound." It is also implied that the jackalope has helped other animals before the sheep.

==Production==
Writer-director Bud Luckey designed and voiced all the characters, composed the music and wrote the story. According to the directors commentary for The Incredibles, Brad Bird wanted to introduce the animated short by having Rick Dicker, (the superhero relocator from The Incredibles, also voiced by Luckey) enter a room, sit down, and pull out his banjo.

This is the first Pixar short with a theatrical release that included vocal performances with words (Bobby McFerrin did an a capella song for Knick Knack).  All prior films included only music and sound effects. So far, this is the only short to include a vocal performance and not be based on and star characters from a Pixar theatrical film. Other shorts that included vocal performances are Mikes New Car (Monsters, Inc.), Jack-Jack Attack (The Incredibles), Mater and the Ghostlight (Cars (film)|Cars), Your Friend the Rat (Ratatouille (film)|Ratatouille), and Dugs Special Mission (Up (2009 film)|Up), though all of these films were released on video only.
 Mater as Easter Egg.

==Theatrical and home media release==
To qualify for the 2004 Academy Awards, Pixar arranged in December 2003 special screenings of the short at the Laemmle Theatres in Los Angeles.   

Boundin was released on March 15, 2005, on the The Incredibles two-disc DVD collectors release, including commentary from Bud Luckey and the short clip titled Who is Bud Luckey?.  The film was also released as part of Pixar Short Films Collection, Volume 1 in 2007.

==Awards==
* 2004: Annie Award—Best Animated Short Subject (Won) 
* 2004: Academy Award—Best Animated Short Film (Nominated) 

==References==
 

==External links==
*  
*  
*  

 
{{succession box
 | before = Mikes New Car short films
 | years  = 2003
 | after  = Jack-Jack Attack}}
 

 
 

 
 
 
 
 