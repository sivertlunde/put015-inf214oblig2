A Little Piece of Heaven (film)
{{Infobox television film
| name           = A Little Piece of Heaven
| image          = A Little Piece of Heaven (film).jpg
| image_size     = 
| caption        = 
| director       = Mimi Leder
| producer       = Joan Barnett Jack Grossbart John Danylkiw
| writer         = Betty Goldberg
| narrator       = 
| starring       = Kirk Cameron Cloris Leachman Jenny Robertson  Ron McLarty Chelsea Noble Jussie Smollett Lacey Chabert Don Davis
| cinematography = François Protat
| editing        = Jacque Elaine Toberen	
| studio         = 
| distributor    = NBC
| released       = December 2, 1991
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

A Little Piece of Heaven (  directed by Mimi Leder, written by Betty Goldberg, and starring Kirk Cameron.  It was created by NBC. 

== Plot ==
Will Loomis (Kirk Cameron) is living with his handicapped sister Violet (Jenny Robertson) after their parents have died. She wants a young child to play with, so brother Will takes a child from the local orphanage, and later another from an abusive home. The children are told they have died and are in Heaven. Will and Violet try to make their farm "a little piece of Heaven" for the kids, while the authorities wonder what has happened to the missing children.

== Cast ==
*Kirk Cameron ... Will Loomis
*Cloris Leachman ... Edwina Ed McKevin
*Jenny Robertson ... Violet Loomis
*Ron McLarty ... Agent Mr. Jack Daniels
*Chelsea Noble ... Carrie Lee, the neighbor
*Jussie Smollett ... Salem Bordeaux
*Lacey Chabert ... Princess, aka Hazal
*Bernard Behrens ... Cecil Loomis (Wills Father)  Colin Fox ... The Preacher
*Joyce Campion ... Pearl 
*Kay Hawtrey ... Della 
*Nicky Guadagni ... Helen Olander 
*J. Winston Carroll ... Sheriff Dickson
*Amos Crawley ... Gregory  
*Ron Hartmann ... Judge

== Awards ==
Emmy Awards
*1992, Outstanding Individual Achievement in Music Composition for a Miniseries or a Special (Nominated)

Young Artist Awards
*1993, Best Young Actress Under Ten in a Television Movie, Lacey Chabert (Nominated)
== References ==
 

== External links ==
*  

 

 
 
 
 
 

 