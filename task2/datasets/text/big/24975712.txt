Incantato
 
{{Infobox film
| name           = Incantato
| image          = Incantato Locandina.jpg
| caption        = Film poster
| director       = Pupi Avati
| producer       = Antonio Avati
| writer         = Pupi Avati
| starring       = Neri Marcorè
| music          = Riz Ortolani
| cinematography = Pasquale Rachini
| editing        = Amedeo Salfa
| distributor    =
| released       =  
| runtime        = 107 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

Incantato ( , also known as The Heart Is Elsewhere or The Heart Is Everywhere) is a 2003 Italian drama film directed by Pupi Avati. It was entered into the 2003 Cannes Film Festival.   

==Cast==
* Neri Marcorè as Nello Balocchi
* Vanessa Incontrada as Angela
* Giancarlo Giannini as Cesare, Nellos father
* Nino DAngelo as Domenico, a hairdresser
* Sandra Milo as Arabella, owner of a boarding house
* Giulio Bosetti as Doctor Gardini, Angelas father
* Edoardo Romano as Professor Gibertoni
* Anna Longhi as Lina, Nellos mother
* Chiara Sani as Jole, Domenicos girlfriend
* Alfiero Toppetti as Renato
* Rita Carlini as Emanuela, manicurist
* Bob Messini
* Pietro Ragusa as Guido Beccalis

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 