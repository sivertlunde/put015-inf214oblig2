Ekk Thee Sanam
 

 
{{Infobox film
| name  = Ekk Thee Sanam
| image = 
| alt =
| caption = 
| director = Monish Kaushal
| producer = D.R.K C.R.Parthasarthi
| writer = D.R.K C.R.Parthasarthi
| screenplay = D.R.K C.R.Parthasarthi
| based on = 
| starring = Nisha Triloki Rajender Thakur Gopal Thakur
| music = Various Artists
| cinematography = 
| editing = 
| studio = Harmony Pictures
| distributor = Harmony Pictures
| released = 11 October 2013
| runtime = 111 minutes
| country = India
| language = Hindi
| budget = 
| gross = 
}}
 Indian drama film directed by Monish Kaushal and produced by D.R.K and C.R.Parthasarthi under the Harmony Pictures banner. The film was released on 11 October 2013. 

==Cast==
*Nisha Triloki
*Rajender Thakur
*Gopal Thakur

==Plot==
The film is about a married woman Sanam, being unfaithful to her husband Rajesh. The couple rent out a room and the painter shoots a clip of Sanam bathing, while Rajesh is away at work. He shows it to Sanam who gets angry, but after that she feels aroused and starts having an affair with the painter. The rest of the film is about Rajesh suspecting an affair with Sanam and painter. 

==Reception==
Ekk Thee Sanam received generally negative reviews from critics. The film was one of the worst movie of 2013. Mihir Fadnavis of Firstpost stated, "With great vengeance, I have sifted through the cinematic trash can to collate the worst movies of the year. Because these movies weren’t hand grenades; they were atom bombs whose detonations of dreadfulness created Hiroshima and Nagasaki-sized mushroom clouds in my cerebrum and caused permanent damage to my mental harmony." 

==References==
 

==External links==
* 

 
 
 
 
 
 

 