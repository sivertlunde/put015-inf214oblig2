The Best Day of My Life
{{Infobox Film
| name = The Best Day of My Life
| image = BestDayOfMyLife.jpg
| image_size =
| caption =
| director = Cristina Comencini
| producer = Marco Chimenz Giovanni Stabilini Riccardo Tozzi
| writer = Giulia Calenda Cristina Comencini Lucilla Schiaffino
| narrator =
| starring = Virna Lisi Margherita Buy Sandra Ceccarelli Luigi Lo Cascio
| music = Franco Piersanti
| cinematography = Fabio Cianchetti
| editing = Cecilia Zanuso
| distributor = 01 Distribuzione
| released = 12 April 2002
| runtime = 102 mins
| country = Italy Italian
| budget =
| preceded_by =
| followed_by =
}} Italian drama film directed by Cristina Comencini.

==Plot==

The Best Day of My Life is the story of a dysfunctional family as seen through the eyes of young Chiara (Maria Luisa De Crescenzo) who is about to receive her first communion. The family includes matriarch Irene (Virna Lisi) and her three grown up children.

==Cast==
* Virna Lisi as Irene
* Margherita Buy as Sara
* Sandra Ceccarelli as Rita
* Luigi Lo Cascio as Claudio
* Marco Baliani as Carlo
* Marco Quaglia as Luca
* Jean-Hugues Anglade as Davide
* Ricky Tognazzi as Sandro Berardi
* Gaia Conforzi as Cecilia
* Francesco Scianna as Marco
* Francesca Perini as Silvia
* Maria Luisa De Crescenzo as Chiara
* Andrea Sama as Cammello
* Giulio Squillacciotti as Che

==Awards==
* 2002: Grand Prix des Amériques at the Montréal World Film Festival
* 2002: Silver Ribbon for Best Screenplay and Best Supporting Actress from the Italian National Syndicate of Film Journalists
* 2003: Grand Prix at the Créteil International Womens Film Festival
==External links==
* 
*  

 

 
 
 
 
 
 
 
 

 
 