Riding the Bullet (film)
{{Infobox film
 | name = Riding the Bullet
 | image = Riding-the-bullet-poster.jpg
 | caption = 
 | director = Mick Garris 
 | based on =   
 | writer = Mick Garris
 | starring =  
 | producer =  
 | distributor = Innovation Film 
 | budget = 
 | gross = $134,711 (United States) 
 | released =  
 | runtime = 98 minutes 
 | language = English 
}} 2004 horror novella of the same name. The movie, which received a limited theatrical release, was not successful in theaters, earning a domestic gross of $134,711. 

==Plot==
 Jonathan Jackson) is a young artist, studying at the University of Maine. He becomes obsessed with death, and believing he is losing his girlfriend, Jessica (Christensen), he tries to commit suicide on his birthday. His friends surprise him and he cuts himself sending him to the hospital. He eventually recovers and decides to go with them to a John Lennon concert.

Before leaving he receives news that his mother, Jean (Hershey), is in the hospital because of a stroke and is near death. Alan decides to hitchhike to reach the hospital before his only relative dies.  On his way there, he has multiple strange encounters with the living and dead. The film ends when Alan, now in his 40s, tells the audience that his mother died of a heart attack while watching television. He married his girlfriend Jessica, but it only lasted four years.

He never made it as an artist, but he does go to the theme park every summer in memory of his mother. While walking from the park, a person that looks like his friend George Staub offers him a ride, but he refuses. He tells him to "take your button and get outta here."

==Cast== Jonathan Jackson &ndash; Alan Parker
*David Arquette &ndash; George Staub
*Cliff Robertson &ndash; Farmer
*Barbara Hershey &ndash; Jean Parker
*Erika Christensen &ndash; Jessica Hadley
*Barry W. Levy &ndash; Julian Parker
*Nicky Katt &ndash; Ferris
*Jackson Warris &ndash; Six-Year-Old Alan
*Jeff Ballard &ndash; 12-Year-Old Alan
*Peter LaCroix &ndash; Mature Alan
*Chris Gauthier &ndash; Hector Passmore
*Robin Nielsen &ndash; Archie Howard
*Matt Frewer &ndash; Mr. Clarkson
*Simon Webb &ndash; Grim Reaper
*Keith Dallas &ndash; Orderly
*Danielle Dunn-Morris &ndash; Mrs. Janey McCurdy
===King casting connections===
 The Dead Zone.
 The Dead Zone.
 The Stand, Bag of Bones.
 The Stand, The Shining, and Ellen George in Quicksilver Highway.
 The Dead Zone.

David Purvis played the Drunk in Kingdom Hospital.

==Trivia==
*In the forest road scene, around 45 minutes into the film, the wolf coming out of the bushes is no wolf. The coloration is wrong, and the animal is too small to be a full bred wolf. When the animal calms down, its easy to see it is a German Shepherd Dog.

==See also==
*List of ghost films

==Notes==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 