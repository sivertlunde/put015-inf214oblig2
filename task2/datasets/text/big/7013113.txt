My Architect
{{Infobox Film name            = My Architect image           = My Architect.jpg caption         =  director        = Nathaniel Kahn producer        = Nathaniel Kahn Susan Rose Behr writer          = Nathaniel Kahn starring        =  music           = Joseph Vitarelli cinematography  = Robert Richman editing         = Sabine Krayenbühl distributor     =  released        = 2003 runtime         = 110 minutes language  English
|budget          =
|}}

My Architect: A Sons Journey is a 2003 documentary film about the American architect Louis Kahn (1901-1974), by his son Nathaniel Kahn, detailing the architects extraordinary career and his familial legacy after his death in 1974. 

In the film, Louis Kahn is quoted as saying “When I went to high school I had a teacher, in the arts, who was head of the department of Central High, William Grey, and he gave a course in Architecture, the only course in any high school I am sure, in Greek, Roman, Renaissance, Egyptian, and Gothic Architecture, and at that point two of my colleagues and myself realized that only Architecture was to be my life. How accidental are our existences are really, and how full of influence by circumstance.” 

The film features interviews with renowned architects, including Frank Gehry, Shamsul Wares,  I.M. Pei, Anne Tyng and Philip Johnson. Throughout the film, Kahn visits all of his fathers buildings including The Yale Center for British Art, The Salk Institute, Jatiyo Sangshad Bhaban and the Indian Institute of Management Ahmedabad. 
 2003 Academy Award for Documentary Feature.    

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 