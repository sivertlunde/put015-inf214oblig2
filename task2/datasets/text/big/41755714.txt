Chandragrohon
{{Infobox film
| name           = Chandragrohon
| image          = Chandra Grohon.jpeg
| caption        = Chandragrohon DVD cover
| director       = Murad Parvez
| producer       = Azam Farooq
| writer         = Murad Parvez Riaz Shohana Champa Dilara Jaman Shahiduzzaman Selim
| music          = Emon Saha Habib Wahid
| cinematography = Mahfuzur Rahman Khan
| editing        = Tawfiq Hossain Chowdhury
| distributor    =
| released       =  
| runtime        =
| country        = Bangladesh
| language       = Bengali
| budget         =
| gross          =
}} Champa in lead roles along with Shahiduzzaman Selim, KS Firoz, Dilara Jaman Kohinur, Gazi Rakayet, and Azom Faruk supporting role.   National Film Awards and including other four awards.   

== Story ==

 

== Cast == Riaz - Kasu 
* Shohana Saba - Falani Champa - 
* Shahiduzzaman Selim  - Ismail Driver 
* KS Firoz - Abul 
* Dilara Jaman  - Moyra Mashi
* Kohinur - Modon 
* Gazi Rakayet - Shombhu
* Azom Faruk - Marwari owner of Caubeji

== Awards ==

=== Rainbow Film Festival===
* The winner of the 2009 Best Movies 

=== National Film Awards === National Film Award and also other four awards totally seven sections.

* Won Best Director - Murad Parvez
* Won Best dialogue - Murad Parvez
* Won Best Screenwriter - Murad Parvez
* Won Best producer - Azom Faruk 
* Won Best Music director - Emon Saha and Habib Wahid 
* Won Best Actor in a Negative role - Jahirauddn peer Champa and Dilara Jaman

=== Meril Prothom Alo Awards ===
* Won Best Director - Murad Parvez.   

=== Bacasasa Film and Culture Award ===
* Won: Best Film - Azom Faruk   

== Music ==
{{Infobox album |
| Name = Chandragrohon
| Cover = 
| image size = 160px
| Type = soundtrack
| Artist = Habib Wahid and Emon Saha 
| Released = 2008
| Recorded = 
| Genre = Film Music
| Length =
| Label = 
| Producer =  Laser Vision 
}}

Chandragrohon film directed by Habib Wahid and Emon Saha. The film Song written by Kabir Bakul, Jewel Mahmud and Arko Mostofa. Total 8 sogns.  Singers are Habib Wahid, Samina Chowdhury, Nancy (musician)|Nancy, Munni and Subir Nandi.

=== Song List ===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song’s !! Singer’s !! Note’s
|- 1 
|Tomare Dekhilo Nancy
|
|- 2 
|Moner Jore Cholse Deho
| Habib Wahid
|
|- 3 
|De Dol De Dola Munni
|
|- 4
|Kon Shohore Jabi Samina Chowdhury
|
|- 5 
|
|
|
|-
|}

==See also==
* Biyer Phul
* Hridoyer Katha
* Rani Kuthir Baki Itihash

==References==
 

== External links ==
*  

 
 
 
 
 
 