Ek Number Ko Pakhe
 

Ek Number Ko Pakhe (Nepali: एक नम्बरको पाखे) is a 1999 Nepali film directed by Kishor Rana Magar. The film stars Rajesh Hamal and Karishma Manandhar in lead roles with an ensemble of supporting cast including Ramesh Upreti, Jal Shah, Neer Shah, Basundhara Bhusal, Ravi Shah and Dinesh DC.

==Introduction==
It was fifth presentation of Mansarovar Films Pvt. Ltd. The story of the film was written by Dr. Bhola Rijal, the script was written by Bishal Pokhrel and the dialogues were penned by Bishal Pokharel and Bharat Adhikari.

==Plot==
Amar (Hamal) lives in a rural village with his mother (Bhusal). One day a rich industrialist (Ravi shah) comes to the village accompanied by his daughter (Manandhar) and his personal assistant (Shiv Hari Paudel) with a project to start a liquor factory in the village. Most of the villagers are illiterate and do not know any positive or negative aspect of the factory in their village. However, Amar opposes the proposal infuriating Shah who in turn tries to bribe him. The two soon reconcile as Amar saves the character played by Manandhar from the hands of a local goon Suraj (DC).

Meanwhile, Kumar (Upreti) lives in a nearby town for his studies. However, he is extremely carefree and fails all the exams. The story revolves around different members of this family especially Dhanraj and Amar.

==Cast  ==
*Rajesh Hamal as Amar
*Karishma Manandhar
*Ramesh Upreti as Kumar
*Jal Shah as Pooja
*Neer Shah as Dhanraj (Amar and Kumars father)
*Ravi Shah
*Dinesh DC as Suraj

==Music==
The music director was Sambhujeet Baskota. The playback singers were Prakash Shrestha, Yam Baral, Ananda Karki, Umesh Pande, Devika Pradhan, Milan Chakkhu, and Sangeeta Rana along with Baskota himself. The songs were penned by Bhola Rijal. The song "Yo maya ko Kahaan paryo ghara" was particularly popular at the time of release. The audio cassettes were produced and distributed by Music Nepal Pvt Ltd.

==Box-Office==
There is no official Box-office recording system in Nepal. At the time of its release, the film was considered to be above average.

==References==
 

 
 


 