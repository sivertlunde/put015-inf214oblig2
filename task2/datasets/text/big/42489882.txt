Naalu Policeum Nalla Irundha Oorum
{{Infobox film
| name           = Naalu Policeum Nalla Irundha Oorum
| image          = 
| alt            =  
| caption        = 
| director       = N. J. Srikrishna
| producer       = V. S. Rajkumar P. Arumugakumar
| writer         = 
| starring       = Arulnithi Remya Nambeesan Bagavathi Perumal 
| music          = B. R. Rejin
| cinematography = Mahesh Muthuswami
| editing        = V. J. Sabu Joseph
| studio         = Leo Visions
| distributor    = 
| released       =    
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}}
Naalu Policeum Nalla Irundha Oorum ( ) is a 2014 Tamil comedy film written and directed by Srikrishna.

The film features Arulnithi and Remya Nambeesan in the leading roles. 

==Cast==
*Arulnithi
*Remya Nambeesan
*Singampuli
*Bagavathi Perumal Rajkumar
*Syed Abu

==Production==
The film was first reported in July 2013, when Leo Visions announced that newcomer director Srikrishna would make a film starring Arulnithi in the lead role. 

Arulnithi began filming for the project in December 2013 and revealed he played a day-dreaming cop constantly brought back to reality by his superior, adding that it will be his first comedy film.  A

rulnithi leads a four man police team composing of Singampuli and actors Bagavathi Perumal and Rajkumar, who had starred in the production houses first venture Naduvula Konjam Pakkatha Kaanom (2012).  Ramya Nambeesan plays Arulnithis love interest in the film. 

The team shot fight sequences at Binny Mills, Chennai in February 2014.  The satellite rights of the film are sold to Star Vijay

==Track list==

Movie songs and background score was composed by B. R. Rejin.

The movie featured three songs as below,

Kadhal Kani Rasam Ethu Nadakuthu - Harihara Sudhan

Kadhal Kani Rasam (Remix) - Mohamed Aslam, Nincy

Kadhal Kani Rasam - M.M. Madhu, JSK Shruthi  

== References ==
 

 
 
 


 