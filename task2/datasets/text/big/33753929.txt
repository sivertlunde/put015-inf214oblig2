Mounam Kalaikirathu
 
{{Infobox film
| name = Mounam Kalaikirathu
| image = Mounam Kalaihirathu.jpg
| caption = Promotional Poster
| director = Kundrai Vaendan Karikalan
| writer = Suresh Jeevitha Jeevitha Anand Vijayakumar Chinni Jayanth S. S. Chandran
| producer = Devaraj Mani Kundrai Vaendan Karikalan
| music = Sankar Ganesh
| editor =
| released = 5 December 1986
| runtime = Tamil
| budget =
}}
 directed by Kundrai Vaendan Karikalan, starring Suresh (actor)|Suresh, Jeevitha (actress)|Jeevitha, Anand Babu, Jayashree, Vijayakumar (actor)|Vijayakumar, Chinni Jayanth and S. S. Chandran. The story is a love triangle.

==Plot synopsis==

Mounam Kalaikirathu is a triangular love story, where Anand Babu and Jeevitha Rajasekhar are lovers, but Suresh too loves Jeevitha without knowing of her relationship with Anand. Jayasree plays a doctor and she loves Suresh as well. The film explores these relationships and finally break the ice of silence.

==Cast==
 Suresh
*Jeevitha Jeevitha
*Anand Babu
*Jayashree Vijayakumar
*Chinni Jayanth
*S. S. Chandran.

==Review==

Malai Malar of 22 December 1986 reviewed Mounam Kalaikirathu as a watchable movie for youth. Dina Sari dated 5 December mentioned the film as a passable entertainer.

==Reception==

Mounam Kalaikirathu was among 1986 December releases and become a moderate success and completed a 75-day of run at the box office.

==Music==

The music for the film was provided by Sankar Ganesh. The music added the value of the movie.

==References==
* http://www.tamilflix.net/2010/09/26/mounam-kalaigirathu-tamil-movie-watch-online/
* http://www.inbaminge.com/t/m/Mounam%20Kalaigiradhu/
* http://www.tamizh.ws/2010/09/mounam-kalaigirathu-1986-suresh.html
* http://www.jointscene.com/movies/Kollywood/Mounam_Kalaigirathu/12591

==External links==

 

 
 
 
 
 