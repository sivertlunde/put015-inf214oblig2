Varnakkazchchakal
{{Infobox film
| name = Varnakkazchchakal
| image =
| caption =
| director = Sundar Das
| producer =
| writer = VC Ashok
| screenplay = VC Ashok Dileep Poornima Mohan Karishma Jagathy Sreekumar
| music = Mohan Sithara
| cinematography = K Ramachandrababu
| editing = L Bhoominathan
| studio = Sooryakanthi Productions
| distributor = Sooryakanthi Productions
| released =  
| country = India Malayalam
}}
 2000 Cinema Indian Malayalam Malayalam film, directed Sundar Das. The film stars Dileep (actor)|Dileep, Poornima Mohan, Karishma and Jagathy Sreekumar in lead roles. The film had musical score by Mohan Sithara.   
 
==Cast==
   Dileep 
*Poornima Mohan 
*Karishma 
*Jagathy Sreekumar 
*KPAC Lalitha 
*Kalabhavan Mani  Ambika 
*Bindu Panicker 
*KPAC Premachandran 
*KTS Padannayil Lena 
*Mala Aravindan 
*Manka Mahesh
*N. F. Varghese 
*Oduvil Unnikrishnan 
*Ponnamma Babu 
*Ravi Menon 
*Risabava
*Sangeetha 
*Sreenath 
*Chandrika
 

==Soundtrack==
The music was composed by Mohan Sithara and lyrics was written by Yusufali Kechery. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ente Peru Vilikkayaano || K. J. Yesudas, KS Chithra || Yusufali Kechery || 
|- 
| 2 || Hamsadhwani || K. J. Yesudas, KS Chithra || Yusufali Kechery || 
|- 
| 3 || Hamsadhwani || K. J. Yesudas || Yusufali Kechery || 
|- 
| 4 || Indraneelam || KS Chithra || Yusufali Kechery || 
|- 
| 5 || Indraneelam   || K. J. Yesudas || Yusufali Kechery || 
|- 
| 6 || Moonam thrikannil || KS Chithra, Mohan Sithara || Yusufali Kechery || 
|- 
| 7 || Pattu chutti || KS Chithra, Chorus || Yusufali Kechery || 
|- 
| 8 || Pattu Chutti Pottum   || K. J. Yesudas || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 


 