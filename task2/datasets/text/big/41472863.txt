Under the Pavement Lies the Strand
 Under the Pavement Lies the Strand ( ) is a  , 2005. ISBN 3039102648, 9783039102648. p.  .  Prior to making the film, Sanders-Brahms had little to no distinct contact with the womens rights movement. 

==Plot==
The film deals with the aftermath of the 1968 student rebellions in Germany as experienced by two fervent participants. Though the country experienced sweeping reforms in the years following, two radicals-turned-successful Berlin stage actors and lovers are grappling with their growing insignificance and the demands of adulthood. After a night of intense debate about the past and their future, the couple begins garnering support to fight a new abortion bill. However, their rekindled zeal is soon complicated by an unexpected pregnancy.

==Cast==
*Grischa Huber as Grischa
*Heinrich Giskes as Heinrich
*Ursula von Berg as Berlin Woman
*Gesine Strempel as Berlin Woman
*Traute Klier-Siebert as Berlin Woman
*Barbara Finkenstädt as Berlin Woman
*Heinz Hoenig
*Günter Lampe

==Release==
The film was released on DVD by Facets Multi-Media in 2008. 

==Reception== German feminist Salon critic repertory cinemas  and it has also been called a "film still of vital interest." 

==References==
 

==External links==
* 
* 
*  TCM Movie Database Film Portal  
 
 
 
 
 
 
 
 
 
 
 
 
 
 