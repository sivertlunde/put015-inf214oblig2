Thoda Tum Badlo Thoda Hum
 
{{Infobox Film
| name           = Thoda Tum Badlo Thoda Hum
| image          = Thoda_Tum_Badlo_Thoda_Hum.jpg
| image_size     = 
| caption        = 
| director       = Esmayeel Shroff
| producer       = Ramoji Rao
| writer         = Esmayeel Shroff
| narrator       = 
| starring       = Arya Babbar  Shriya Saran 
| music          = Amar Mohile
| cinematography = Mazhar Kamran
| editing        = Sreekar Prasad
|studio=Ushakiran Movies
| distributor    = 
| released       =   
| runtime        = 166 min
| country        = India
| language       = Hindi
}} 2004 Bollywood film starring Arya Babbar and Shriya Saran in the lead while Kiran Karmarkar, Ashok Saraf, Nishigandha Wad and Shoma Anand appear as supporting cast. 
The story is a love-hate relationship between two teenagers.

The film did not do well at the box office. Film critic Taran Adarsh calls the film on whole as "an ordinary fare". 

==Plot==
Raju (played by Arya Babbar) and Rani (played by Shriya Saran) are neighbors and studying in same college. But they never settle on their differences and always fight. Rani keeps on making complains against Raju and hence his father (played by Kiran Karmarkar) scolds his son as good-for-nothing. However, both families have good relationship. Ranis father (played by Ashok Saraf) is a police commissioner and a good friend of Rajus father. The hatred between Raju and Rani takes an ugly turn when Rani slaps Raju during a college competition thus humiliating him.

Ranis father is then transferred to Kodaikanal and as a friendly gesture, Raju along with his parents go to the railway station to bid adieu. Rani is surprised by this change in his behaviour and she too undergoes a change inside her heart towards Raju. The two after separating from each other realise how they actually are in love.

==Cast==
* Arya Babbar as Raju
* Shriya Saran as Rani
* Kiran Karmarkar as Rajus father
* Ashok Saraf as Ranis father
* Nishigandha Wad
* Shoma Anand
* B.Venkat
* Varsha Kapkar
* Shweta Menon as Guest Appearance

== Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Thoda Tum Badlo Thoda Hum"
| Alka Yagnik, Udit Narayan
|- 
| 2
| "Sabhi Aa Chuke Hai"
| Sonu Nigam
|- 
| 3
| "Uff Yuh Ma"
| Asha Bhosle
|- 
| 4
| "Tauba Tauba"
| Udit Narayan
|- 
|}

== References ==
 

==External links==
* 

 
 
 


 