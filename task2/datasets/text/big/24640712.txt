Judy Berlin
 
{{Infobox film
| name           = Judy Berlin
| image          = Judy Berlin 1999.jpg
| caption        = Film poster
| director       = Eric Mendelsohn
| producer       = Rocco Caruso
| writer         = Eric Mendelsohn
| starring       = Barbara Barrie
| music          = 
| cinematography = Jeffrey Seckendorf
| editing        = Eric Mendelsohn Starz Encore Entertainment
| distributor    = Shooting Gallery Film Series
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
}}

Judy Berlin is a 1999 American drama film directed by Eric Mendelsohn. It was screened in the Un Certain Regard section at the 1999 Cannes Film Festival.   

Mendelsohn won the directing prize for Judy Berlin at the 1999 Sundance International Film Festival.    It was Madeline Kahns final film.

==Plot==
A schoolteacher, Sue Berlin, develops a romantic attachment to the principal, Arthur Gold, who is in an unhappy marriage with his wife Alice.

Their grown children, aspiring actress Judy and wanna-be filmmaker David, meet and form an attraction of their own.

==Cast==
* Barbara Barrie as Suzan Sue Berlin
* Bob Dishy as Arthur Gold
* Edie Falco as Judy Berlin
* Carlin Glynn as Maddie
* Aaron Harnick as David Gold
* Bette Henritze as Dolores Engler
* Madeline Kahn as Alice Gold
* Julie Kavner as Marie
* Anne Meara as Bea
* Novella Nelson as Carol Peter Appel as Mr. V.
* Marcia DeBonis as Lisa
* Glenn Fitzgerald as Tour guide
* Marcus Giamatti as Eddie Dillon
* Judy Graubart as Ceil

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 