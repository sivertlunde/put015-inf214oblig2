Looking on the Bright Side
 
{{Infobox Film | name = Looking on The Bright Side
  | image =
  | caption =
  | director = Graham Cutts Basil Dean
  | producer = Basil Dean Brock Williams Archie Pitt
  | starring =Gracie Fields Richard Dolman Betty Shale
  | music = Carroll Gibbons Robert Martin Otto Ludwig
  | studio =  Associated Talking Pictures 
  | distributor = RKO Pictures
  | released =   
  | runtime = 81 minutes
  | country = United Kingdom
  | language = English
  | budget =
}} British musical musical comedy film   It was directed by Graham Cutts and Basil Dean and starring Gracie Fields, Richard Dolman, and Betty Shale. It was made at Ealing Studios.

==Plot summary==
Gracie (Fields) and Laurie (Dolman) are lovers who together form a musical act. Gracie sings and Laurie writes the songs, but when Laurie gets a taste of fame, he runs off after a glamorous actress.

==DVD release== Sally in Queen of Hearts (1936) and The Show Goes On (1937), these are on 4 discs. Two films each on three of the discs with the other film on disc four.

==Cast==
* Gracie Fields as Gracie
* Richard Dolman as Laurie
* Julian Rose  as Oscar Schultz 
* Wyn Richmond as Josie Joy 
* Tony De Lungo as Delmonico 
* Betty Shale as Hetty Hunt
* Viola Compton as Police Sergeant 
* Bettina Montahners as Bettina  Charles Farrell as Released criminal

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 