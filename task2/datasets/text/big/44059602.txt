Jayikkaanaay Janichavan
{{Infobox film
| name           = Jayikkaanaay Janichavan
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = Sreekumaran Thampi
| writer         = MP Rajeevan Sreekumaran Thampi (dialogues)
| screenplay     = Sreekumaran Thampi
| starring       = Prem Nazir Jayan Sheela Jagathy Sreekumar
| music          = M. K. Arjunan Ashok Kumar
| editing        = K Sankunni
| studio         = Bhavani Rajeswari
| distributor    = Bhavani Rajeswari
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film,  directed by J. Sasikumar and produced by Sreekumaran Thampi. The film stars Prem Nazir, Jayan, Sheela and Jagathy Sreekumar in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Prem Nazir
*Jayan
*Sheela
*Jagathy Sreekumar
*KPAC Lalitha
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Manavalan Joseph
*Sreelatha Namboothiri Sreenivasan
*Prathapachandran
*MG Soman
*Mallika Sukumaran
*Thodupuzha Radhakrishnan
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Allaavin Thirusabhayil || Jolly Abraham, Mannur Rajakumaranunni || Sreekumaran Thampi ||
|-
| 2 || Arayaal Mandapam || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 3 || Chaalakkambolathil || P Jayachandran || Sreekumaran Thampi ||
|-
| 4 || Devi Mahaamaaye (Aalavattam Venchamaram) || P Jayachandran, Ambili, Chorus || Sreekumaran Thampi ||
|-
| 5 || Ezhu Swarangal || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 6 || Kaavadichinthu Paadi || K. J. Yesudas, B Vasantha || Sreekumaran Thampi ||
|-
| 7 || Thankam Kondoru || Ambili, Chorus, Jolly Abraham || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 