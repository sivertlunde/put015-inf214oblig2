October Gale (film)
October Gale is a Canadian thriller written and directed by Ruba Nadda. The film premiered at the 2014 Toronto International Film Festival in the Special Presentations section of the festival.  It was acquired by IFC Films and received a release in March of 2015. 
 Nadda had previously worked with on the film Cairo Time as well as Scott Speedman and Tim Roth.

==Plot==
Helen Matthews (Patricia Clarkson), a widow still grieving the untimely death of her husband, James (Callum Keith Rennie), goes to the cottage they once shared. On her first night there she is awoken by the sound of a gunshot. When she leaves to investigate she finds a trail of blood and upon her return finds a stranger (Scott Speedman) who has been shot in the shoulder and has crawled into her home looking for help.

Helen, a doctor, manages to tend to his wound and save him. The following day she tries to learn who shot Will, the stranger, but he refuses to tell her and insists on departing. However he is too sick to leave and Helen allows him to return to the house. 

Later Al, a man Helen is familiar with, comes by the house to return Helens boat which she gave to him to fix. Will warns her not to let the man in the house as he will kill both of them but Helen, ignoring him, asks Al to tow both her and Will to shore. Al agrees, but instead unties both Helen and Wills boats from the her dock and then departs. In an attempt to stop the boats from drifting off Helen dives into the water and nearly drowns before she is finally rescued by Will. 

Will eventually reveals that he accidentally killed a man in a bar fight and, despite serving time, the father of the man he killed is pursuing him and wont stop until hes dead. Helen meanwhile reveals that her husband died the previous year in October after being caught in a storm.

Helen decides to try to help Will survive. They scour her home for rifles for their bullets and remove the dock from her home. Meanwhile Al returns with Tom and the after searching the house unsuccessfully they seek Helen and Will in the woods where they have split up. Helen attempts to attack Tom but is unsuccessful. Instead he returns her to the house where he tells her that Will was his surrogate son he raised to look after his biological son and Will killed his son because he had attacked a woman, though Tom denies this. He also shoots Al in order to lure Will back to the house.

Will does return to the house after hearing the gunshot. He and Tom hug on the porch and after Will apologizes Tom lifts his gun to shoot him.  Helen however, has managed to get the rifle off of Als body and kills Tom before he can hurt Will. 

The following day in town after going to the police Helen sees what she thinks is James but turns out to be Will. He asks her to leave with him.

==Production==
October Gale was shot in and around the Georgian Bay. Filming took place in the spring after one of the coldest recorded winters and according to Nadda the ice did not melt until two days before they began shooting on the water. 

==Reception== Variety gave it a mixed review calling it "a tepid, underdeveloped thriller."  The Hollywood Reporter criticized the script for resulting in a "fairly silly film." 

==External links==
*  

==References==
 

 
 
 
 
 
 
 