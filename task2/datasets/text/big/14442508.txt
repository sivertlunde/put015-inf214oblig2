A Walk with Love and Death
{{Infobox film
| name           = A Walk with Love and Death
| image          = Walk with Love and Death poster.jpg
| caption        = Theatrical poster
| writer         = Hans Koningsberger (novel & screenplay) Anthony Higgins John Huston
| director       = John Huston Russell Lloyd
| producer       = Carter DeHaven
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 min.
| country        = United States
| language       = English
| budget         = $2,410,000 
| music          = Georges Delerue
| cinematography = Edward Scaife
| awards         =
}}

A Walk with Love and Death is a 1969 Romantic drama film|romantic/drama film directed by John Huston.

==Plot and context==
The story is based on the 1961 novel by Hans Koningsberger, set at the time of the 1358 uprising of the peasants of northern France known as the Jacquerie. Heron of Fois, a student from Paris, crosses territory devastated by the upheaval and the ferocious reprisals of the nobility. He meets with Claudia, the aristocratic daughter of a royal official killed by the peasants, and they attempt to reach Calais. In the novel Herons intended final destination is Oxford University while in the film "the sea" less specifically comes to represent an abstract freedom. While differing in their views of the Jacquerie - Heron sympathises with the exploited peasantry while Claudia sees their rising as mindless savagery - the young couple become lovers. In the end they fail to escape the chaotic violence around them but await death "strangely happy - we had stopped running from them and we had our hour".

Some contemporary reviewers considered that the film held up the past as a mirror of the events of 1968, when it was made. Comparisons were variously made with the Vietnam War or the Paris rioting of May/June that year, which required filming to be relocated to Austria and Italy. However a recent and detailed analysis of both novel and film by the essayist Peter G. Christensen concludes that the story is literally a period one, intended to evoke the turbulence of its 14th century setting, rather than illustrating cultural or generational issues of the late 1960s. 

The film marked the screen debut of Hustons daughter  ]  John Huston himself plays the role of a noble who defects to the rebels.

The film was not a box-office success,  although John Huston noted in his autobiography, An Open Book (1980), that it was highly praised in France, where there was a greater understanding of the historical context.

==Cast==
*Anjelica Huston as Claudia
*Assi Dayan as Heron of Fois Anthony Higgins as Robert of Loris
*John Hallam as Sir Meles Robert Lang as Pilgrim Leader
*Guy Deghy as The Priest
*Michael Gough as Mad Monk
*George Murcell as The Captain
*Eileen Murphy as Gypsy Girl Anthony Nicholls as Father Superior
*Joseph OConor as Pierre of St. Jean
*John Huston as Robert the Elder
*John Franklyn as Whoremaster
*Francis Heim as Knight Lieutenant
*Melvyn Hayes as First Entertainer
*Barry Keegan as Peasant Leader Nicholas Smith as Pilgrim
*Antoinette Reuss as Charcoal Woman

==Music==
The musical score was the work of the French composer Georges Delerue. It incorporated medieval folk music themes, making extensive use of lute, harpsichord and recorders.

==References==
 

==External links==
*  at The New York Times Time Out
*  
*  

 

 
 
 
 
 
 
 
 
 
 