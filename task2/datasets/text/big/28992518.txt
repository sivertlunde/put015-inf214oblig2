Life with the Lyons (film)
 
 
{{Infobox film
| name           = Life with the Lyons
| caption        = 
| image  	= "Life_with_the_Lyons"_(1954).jpg
| director       = Val Guest
| producer       = Michael Carreras Robert Dunbar
| writer         = Val Guest Robert Dunbar
| based on       = the radio series by Bebe Daniels Bob Block Bill Harding
| starring       = Ben Lyon Bebe Daniels
| music          = Arthur Wilkinson
| cinematography = Walter J. Harvey
| editing        = Douglas Myers
| studio = Hammer Film Productions
| distributor    = Exclusive Films  (UK) Lippert Pictures  (US)
| released       = 25 May 1954	(London)  (UK) 
| runtime        = 81 minutes
| country        = United Kingdom
| language       = English
| budget         =  £28,000 
| gross = Over ₤70,000 Tom Johnson and Deborah Del Vecchio, Hammer Films: An Exhaustive Filmography, McFarland, 1996 p93 
}} British comedy film directed by Val Guest and starring Bebe Daniels, Ben Lyon and Richard Lyon. It was a spin-off from the radio series Life with the Lyons, and the sceenplay was based on previous episodes from the show.  The Lyon family attempt to acquire a new house in Marble Arch. It was followed in 1955 by The Lyons in Paris. The Lyons contract called for them to receive a percentage of the films profits. After the films successful release, the Lyons began a long-running, BBC television series, also titled Life with the Lyons. The film was made at Southall Studios.  

==Cast==
* Bebe Daniels ...  Bebe Lyon
* Ben Lyon ...  Ben Lyon
* Richard Lyon ...  Richard Lyon
* Barbara Lyon ...  Barbara Lyon
* Hugh Morton ...  Mr. Hemingway
* Horace Percival ...  Mr. Wimple
* Molly Weir ...  Aggie
* Doris Rogers ...  Florrie Wainwright
* Gwen Lewis ...  Mrs. Wimple Arthur Hill ...  Slim Cassidy
* Belinda Lee ...  Violet Hemingway

==Critical reception==
Britmovie called it "a cheap but cheerful romp."  

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 


 
 