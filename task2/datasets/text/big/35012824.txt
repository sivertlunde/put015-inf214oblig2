Kontinuasom
 

{{Infobox film
| name           = Kontinuasom
| image          = 
| caption        = 
| director       = Óscar Martínez
| producer       = Útopi ASAD Animasur. RTCV (Radio Televisión Cabo Verde)
| writer         = 
| starring       = 
| distributor    = 
| released       = 2009
| runtime        = 80 minutes
| country        = Cape Verde Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = Francisco Pascual
| cinematography = David Dominguez
| editing        = Noemi García Irene Cardona Raquel Conde
| music          = 
}}

Kontinuasom is a 2009 documentary film.

== Synopsis ==
Beti is a dancer in the Raiz di Polon company in Cape Verde. She receives an offer from Lisbon to join a Cape Verde music show and start a new career there. The offer unchains the deep-set Cape Verde conflict in her: identity built on the Diaspora century after century. Doubts, nostalgia, uprooting, they all soar over her and accompany her decision. The same dilemma that surrounds all Cape Verdeans, the yearning to leave, the yearning to return... Expressed and brought together around music, hallmark of the people of Cape Verde.

== References ==
 

 
 
 
 
 
 
 


 
 