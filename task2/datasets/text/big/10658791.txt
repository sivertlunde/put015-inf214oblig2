Nallavanuku Nallavan
 
{{Infobox film
| name      = Nallavanuku Nallavan
| image     = Nallavanukku Nallavan.jpg
| caption   = Theatrical poster
| director  = SP. Muthuraman
| writer    = Visu (dialogues)
| story     = Dr. M. Prabhakar Reddy Karthik Raadhika Radhika
| producer  = M. Saravanan (film producer)|M. Saravanan M. Balasubramanian
| music     = Ilaiyaraaja
| cinematography = Babu
| editing   = R. Vittal
| studio    = AVM Productions
| distributor = AVM Productions
| released  = 22 October 1984
| runtime   = 175
| country   = India Tamil
| budget    =
| gross     =  2.38 crore 
}} Tamil film, Kannada as Vishnuvardhan and Raadhika Sarathkumar in main roles. It was a blockbuster and completed a 152-day run at the box office.

==Cast==
* Rajinikanth Radhika
* Karthik
* Thulasi
* Major Sundarrajan
* Visu
* Y. Gee. Mahendra
* Y. Vijaya
* V. K. Ramaswamy (actor)|V. K. Ramasamy

==Production==
Rajinis character had two shades, one as a goon & another one as a doting husband-father. Rajini loved Amitabhs costume in one of his films Yaaraana where he wears a jacket which had lights. Rajini wore the same jacket which had lights in Vechikava Nenjukkuley song and Rajini had the switch in his pockets and dancing. http://www.cinemalead.com/visitor-column-id-the-journey-of-living-legend-rajinikanth-part-4-rajinikanth-15-09-133374.htm 

==Soundtrack==
The films soundtrack was composed by Ilaiyaraaja. The song "Vechukkava" was remixed by Yuvan Shankar Raja in Silambattam (film)|Silambattam (2008). 
{| class="wikitable" 
|- 
! No. !! Title !! Singer !! Length !! Lyricist
|-
| 1 || "Chittuku Chella Chittuku" || K. J. Yesudas || 04:42 || N. Kama Rajan
|-
| 2 || "Unnaithane" ||  
|-
| 3 || "Vechukava" ||  
|-
| 4 || "Muthaduthey" || S. P. Balasubramaniam, S. Janaki || 04:35 || Muthulingam
|-
| 5 || "Namma Mothali" ||  
|-
| 6 || "Ennai Thane" ||  
|}

==Release==
On August 3, 1984, Hindu in its review wrote: "Muthuraman has a large hand in embellishing the dramatic elements with deft touches and polished handling".  Cinemalead wrote "Through this movie Rajini got a good respect among family audience. A more matured portrayal by Rajini, an equally matching Radhika and thought-provoking script are the key factors which made the film memorable". 

== References ==
 

 
 

 
 
 
 
 
 
 


 