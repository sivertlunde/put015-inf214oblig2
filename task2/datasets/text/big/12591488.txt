Torment (1944 film)
 
{{Infobox film name            = Torment (Hets) image           = Torment (1944).jpg image_size      = 225px caption         = theatrical release poster director        = Alf Sjöberg producer        = Victor Sjöström writer          = Ingmar Bergman starring        = Stig Järrel Alf Kjellin Mai Zetterling Gösta Cederlund Olof Winnerstrand Hugo Björne Stig Olin music           = Hilding Rosenberg cinematography  = Martin Bodin editing         = Oscar Rosander studio          = Svensk Filmindustri distributor     = Svensk Filmindustri released        = 2 October 1944 runtime         = 101 minutes country         = Sweden language        = Swedish budget          =
}}

Torment ( ) is a Swedish film from 1944, directed by Alf Sjöberg, with screenplay by Ingmar Bergman. It was originally released as Frenzy in the United Kingdom,  although later releases have used the US title. The film, a tale of sex, passion and murder, was Bergmans actual directing debut, although the film was mainly directed by Sjöberg.

== Plot == sadistic Latin teacher, nicknamed "Caligula" by his long-suffering students, rules his classroom like his kingdom. He is exceptionally hard on the diligent Jan-Erik, one of his students. One night Jan-Erik is returning home and finds an intoxicated young woman crying on the street. He recognizes her as Bertha, the clerk in a tobacco store near the school, and he walks her home. Bertha has a taste for both men and liquor, and he spends most of the night on her bedside. He becomes very involved with her, and his school work suffers. Bertha also has an older man whom she fears, although she will not reveal his name. He is Caligula, and he learns of his students involvement. He makes life harder still for Jan-Erik, and forces Bertha to do his will by threatening to suspend Jan-Erik. But Caligula is too violent with Bertha, and one day, Jan-Erik arrives to find her dead. In a corner, he finds Caligula hiding, and calls the police. With no proof, however, Caligula is soon released, and quickly arranges for the expulsion of Jan-Erik, who accuses Caligula of murder, and finally strikes him in front of the principal of the school. He then goes to stay in Berthas apartment. The principal of the school comes to the apartment, and offers his assistance in helping Jan-Erik back on track. Caligula comes to the apartment after the principal has left, seeking some sort of forgiveness, but Jan-Erik rejects him and instead walks out into the day to a view that overlooks all Stockholm.

== Cast ==
* Alf Kjellin as Jan-Erik Widgren (student at Ring IV L)
* Stig Järrel as "Caligula", teacher of Latin language
* Mai Zetterling as Bertha Olsson, clerk of the cigarette store
* Olof Winnerstrand as The Principal
* Gösta Cederlund as Pippi, teacher
* Stig Olin as Sandman, student
* Jan Molander as Pettersson, student
* Olav Riégo as Mr. Widgren
* Märta Arbin as Mrs. Widgren
* Hugo Björne as The Doctor

== Production ==
On 16 January 1943, Ingmar Bergman had been appointed by the Svensk Filmindustri (SF) as an "assistant director and screenwriter" on a one-year initial contract. Bergman, who suffered illness and was hospitalized during the winter of 1942–43, wrote the screenplay for Torment, for which SF acquired the rights in July 1943. The Latin teacher Caligula is partly based on the Latin teacher Sjögren (also played by Stig Järrel) in the 1942 film Lågor i dunklet by director Hasse Ekman. 

Filming, in which Ingmar Bergman took part as an assistant director, took place in two stages. The first stage, for interior scenes, took place from 21 February to 31 March 1944 at the Filmstaden studios north of Stockholm and the Södra Latin High School, downtown Stockholm. The second stage, covering the exterior scenes, comprised only ten days in late May of the same year. In his second autobiography Images: My Life in Film, Bergman describes the filming of the exteriors as his actual film directorial debut:

 When the film was virtually done, I made my debut as a movie director. Originally, Torment ends after all the students have passed their final exam, except for one, played by Alf Kjellin, who walks out through a backdoor into the rain. Caligula stands in the window, waving good-bye. Everybody felt that this ending was too dark. I had to add an additional scene in the dead girls apartment where the principal of the school has a heart-to-heart talk with Kjellin while Caligula, the scared loser, is screaming on the staircase below. The new final scene shows Kjellin in the light of dawn, walking towards the awakening city. I was told to shoot these last exteriors, since Sjöberg was otherwise engaged. They were my first professionally filmed images. I was more excited that I can describe. The small film crew threatened to walk off the set and go home. I screamed and swore so loudly that people woke up and looked out of their windows. It was four o’clock in the morning.  

== Reaction ==
Torment provoked intensive debate in the press about the conditions in the Swedish high schools. On a personal level, the Pro-German newspaper the Aftonbladet published a letter by Henning Håkanson, principal of the private Palmgren High School where Ingmar Bergman had been a student. Håkanson reacted on an interview with Bergman published in the Aftonbladet the same day the film was released:

 Mr. Bergmans statement, that his entire time at school was hell, surprises me. I clearly recall that he, his brother and his father were all very satisfied with the school. After his final examinations, Ingmar Bergman came back to school to attend our Christmas party, bright and cheery as far as one could tell, and not seeming to harbor any grudge, either against the school or its teachers. In all probability, the fact of the matter lies elsewhere. Our friend Ingmar was a problem child, lazy yet rather gifted, and the fact that such a person does not easily adapt to the daily routines of study is quite natural. A school cannot be adapted to suit bohemian dreamers, but to suit normally constituted, hard working people."  

A few days later Bergman replied:

 Let us start with the 12-year hell (coarsely expressed, by the way. Not a word used by me, but by the person who interviewed me. I recall using a milder term, which is somewhat different). Indeed…I was a very lazy boy, and very scared because of my laziness, because I was involved with theatre instead of school and because I hated having to be punctual, having to get up in the morning, do homework, sit still, having to carry maps, having break times, doing tests, taking oral examinations, or to put it plainly: I hated school as a principle, as a system and as an institution. And as such I have definitely not wanted to criticize my own school, but all schools. As far as I understand it, and as I clearly pointed out in that unfortunate interview, my school was neither better nor worse than other institutions with the same purpose. My revered headmaster also writes (somewhat harshly): A school cannot be adapted to suit bohemian dreamers, but to suit normally constituted, hard working people. Where should the poor bohemians go? Should pupils be divided up: Youre a bohemian, youre a hard-working person, youre a bohemian, etc. Would the bohemians be excused? There are teachers one never forgets. Men one liked and men one hated. My revered headmaster belonged and still belongs (in my case) to the former category. I also have the feeling that my dear headmaster has not yet seen the film. Perhaps we should go and watch it together!  

== References ==
Notes
 

Bibliography
* Ingmar Bergman|Bergman, Ingmar, Bilder, Stockholm : Norstedt, 1990. ISBN 91-1-893192-8
* Ingmar Bergman|Bergman, Ingmar   at the Swedish Film Institute, 
* Lundin, Gunnar and Olsson, Jan, Regissörens roller : samtal med Alf Sjöberg, Lund : Cavefors, 1976. ISBN 91-504-0445-8  
* Lundin, Gunnar. Filmregi Alf Sjöberg, Lund : Institutionen för dramaforskning, Lund Úniversity, 1979. ISBN 91-7222-231-X
*   Svensk filmdatabas.

== External links ==
*  
*  
*   Classroom scene with Caligula, the sadistic Latin teacher.  

 

 
 

 
 
 
 
 
 
 
 
 