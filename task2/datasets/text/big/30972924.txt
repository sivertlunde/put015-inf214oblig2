The Circuit (2002 film)
{{Infobox film
  | name                = The Circuit 
  | image               = 
  | caption             =
  | director            = Jalal Merhi
  | producer            = 
  | writer              = 
  | narrator            =
  | starring            = Olivier Gruner Simon Kim James Kim
  | music               = 
  | cinematography      = 
  | editing             = 
  | distributor         = 
  | released            =  
  | runtime             = 91 min
  | country             = Canada
  | language            = English
  | budget              =
  | gross               =
}}
«The Circuit» - is a 2002 arena-fighting film starring Olivier Gruner  

==Plot==
Years ago, Dirk Longstreet walked away from The Circuit, the merciless underground fight club with no rules, where money changes hands as men are killed. But now gangsters have got his brother, and only one more journey inside the ring will save his life. Or will it? Not without a fight.. Literally. And this time, its personal.

==Cast==
*Olivier Gruner
*Bryan Genesse
*Loren Avedon
*Ilya Melnikoff
*Burt Reynolds

==See also==
* 
* 

==External links==
*  
*  

 
 
 
 
 

 