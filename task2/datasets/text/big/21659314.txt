Anantha Rathriya
{{Infobox film
| name           = Anantha Rathriya
| director       = Prasanna Vithanage
| producer       = Damayanthi Fonseka
| writer         = Leo Tolstoy (novel), Prasanna Vithanage (writer)
| starring       = Swarna Mallawarachchi, Ravindra Randeniya, Yashoda Wimaladharma, Tony Ranasinghe
| music          = Harsha Makalanda
| cinematography = M. D. Mahindapala
| editing        = Lal Piyasena
| distributor    = Torana Home Video
| released       =  
| runtime        = 75 minutes
| country        = Sri Lanka Sinhala
}}

Anantha Rathriya ( ) is a 1996 Sri Lankan drama film directed by Prasanna Vithanage. It is loosely based on the Leo Tolstoy novel Resurrection (novel)|Resurrection. 

==Plot==
Suwisal (Ravindra Randeniya) arrives at what seems to be an old family house in the middle of the night. The housekeeper explains that the rebels have asked everyone in the village to keep the lights off, and that for all, times have been bad. Suwisal recalls brief memories of the piano and a young girl shyly peering out behind the drapes at him and his aunts. 

Several days earlier, Suwisals fiancee (Yashoda Wimaladharma) calls to remind him of jury duty. He is to spend the next three days listening to a theft/murder case. The defendant, Piyum (Swarna Mallawarachchi) has been charged with the murder of John Wijesinghe. She is a prostitute and Wijesinghe, a client. He did not have enough money to pay her, so instead of continuing with her work, she slipped sleeping pills into his drink. Piyum left the room and Wijesinghe died. 

Decades before, a young Suwisal goes back to the village and ancestral home to work on his thesis called, "The Possible Result of Disillusioned Rural Youth and the Impact of the Free Education Policy". He encounters the servant woman working at his aunts home. He sleeps with her, and soon thereafter, leaves to go back to the city. Through letters, he learns she is pregnant, and stops all communication with her. 

During the second day of the trial, Suwisal goes to his lawyer friend Vicky (Tony Ranasinghe)  and explains that he knows Piyum. She is the former servant at his aunts house.  Vicky advises Suwisal to keep quiet and wait for the trial to finish. Piyum does not indicate that she knows Suwisal, which causes him to question his actions and character. At the end of the trial, the jury convicts Piyum and sentences her to 10 years in jail. 

Suwisals guilt overtakes him and he goes to the jail to help Piyum. She informs him that she gave birth to a boy, who lived for a week, then tried to find Suwisal in the city, and "lost her way." She tells him to stop contacting her, and the last he sees of her is while she walks out of jail.

==Cast==
*Swarna Mallawarachchi, as Piyumi
*Ravindra Randeniya, as Suwisal
*Yashoda Wimaladharma, as Suwisals fiancee
*Tony Ranasinghe, as Vicky

==References==
 

==External links==
* 
* 

 

 
 
 
 