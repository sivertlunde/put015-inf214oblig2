Anweshichu Kandethiyilla
{{Infobox film 
| name           = Anweshichu Kandethiyilla
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       = K. Ravindran Nair
| writer         = Parappurathu
| screenplay     = Parappurathu Madhu K. R. Vijaya Sukumari Kaviyoor Ponnamma
| music          = MS Baburaj
| cinematography = EN Balakrishnan
| editing        = Das G Venkittaraman
| studio         = General Pictures
| distributor    = General Pictures
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, directed by P. Bhaskaran and produced by Raveendranathan Nair. The film stars Madhu (actor)|Madhu, K. R. Vijaya, Sukumari and Kaviyoor Ponnamma in lead roles. The film had musical score by MS Baburaj.    The film recounts the quest for happiness by a nurse with the best of intentions but who is disillusioned. It won the National Film Award for Best Feature Film in Malayalam.

==Cast==
  Madhu
*K. R. Vijaya
*Sukumari
*Kaviyoor Ponnamma
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Kottayam Santha
*P. J. Antony
*T. S. Muthaiah
*Latheef
*Bahadoor GK Pillai
*Mavelikkara Ponnamma Meena
*Nellikode Bhaskaran
*Panjabi
*Santha Devi
*Vijayanirmala Sathyan
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Innale Mayangumbol || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Kavilathe Kanneer Kandu || S Janaki || P. Bhaskaran || 
|-
| 3 || Murivaalan Kurangachan || S Janaki || P. Bhaskaran || 
|-
| 4 || Pavananaam Aattidaya || S Janaki, B Vasantha || P. Bhaskaran || 
|-
| 5 || Thaamarakkumbilallo || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 

 
 
 

 