9½ Weeks
{{Infobox film
| name           = 9½ Weeks
| image          = Nineweeksposter.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Adrian Lyne
| producer       = Mark Damon Sidney Kimmel Zalman King Antony Rufus-Isaacs
| screenplay     = Sarah Kernochan Zalman King Patricia Louisanna Knop
| based on       =  
| starring = {{Plainlist|
* Mickey Rourke
* Kim Basinger
}}
| music          = Jack Nitzsche
| cinematography = Peter Biziou
| editing        = Caroline Biggerstaff Ed Hansen Tom Rolf Mark Winitsky
| studio         = Producers Sales Organization
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 112 minutes (Theatrical cut)  117 minutes (Video)
| country        = United States France
| language       = English
| budget         = $17 million
| gross          = $106,734,844
}} erotic romantic drama film directed by Adrian Lyne and starring Kim Basinger and Mickey Rourke. It is based on 9½ Weeks (Book)| the memoir of the same name by Elizabeth McNeill, about a New York City art gallery employee who has a brief yet intense affair with a mysterious Wall Street broker. The film was completed in 1984 but not released until February 1986.
 box office disappointment in the United States, grossing only less than $7 million at the box office on a $17 million budget. It also received mixed to negative reviews at the time of its release. However the film was a huge success internationally, particularly in Australia, Canada and the United Kingdom making $100 million worldwide  and acquired a large fanbase on video/DVD. 

The film spawned two direct-to-video sequels, Another 9½ Weeks (1997) and The First 9½ Weeks (1998).

==Plot==
The title of the film refers to the duration of a relationship between Wall Street arbitrageur John Gray (Mickey Rourke) and divorced SoHo art gallery employee Elizabeth McGraw (Kim Basinger). John initiates and controls the various experimental sexual practices of this volatile relationship to push Elizabeths boundaries. In doing so, Elizabeth experiences a gradual downward spiral toward emotional breakdown.   

Elizabeth first sees John in New York City where she grocery shops and again at a street market where she decides against buying an expensive scarf. John wins her heart when he eventually produces that scarf. They start dating, and Elizabeth is increasingly subjected to Johns behavioral peculiarities; he blindfolds Elizabeth, who is at first reluctant to comply with his sexual fantasy demands. Yet she sees him as loving and playful. He gives her an expensive gold watch, and instructs her to use it to think about him at noon.  She takes this imperative even further by masturbating at her workplace at the designated time. However, he ultimately confuses Elizabeth by his reluctance to meet her friends despite the intimacy of their sexual relations.   

Elizabeths confusion about John increases when he leaves her alone at his apartment. She examines his closet till she discovers a photograph of him with another woman. John asks her if she went through his stuff, declaring that he will punish her. Their ensuing altercation escalates into sexual assault till she blissfully concedes to his struggle to overpower her. Their sexual intensity grows as they start having sex in public places. 

Elizabeths heightened need for psychosexual stimulation drives her to stalk John to his office and to obey his injunction to cross-dress herself for a rendezvous. On leaving the establishment, two men hurl a homophobic slur when they mistake John and Elizabeth for a gay couple. A fight ensues. Elizabeth picks up a knife from one of the attackers and stabs one of them in the buttocks and both attackers flee. After the fight, Elizabeth reveals a wet wife beater and has sex onsite with John with intensely visceral passion. Following this encounter, Johns sexual games acquire sadomasochistic elements.  

Rather than satisfying or empowering Elizabeth, such experiences intensify her emotional vulnerability. While meeting at a hotel room, John blindfolds her. A prostitute starts caressing Elizabeth as John observes them. The prostitute removes Elizabeths blindfold and starts working on John. Elizabeth violently intervenes, and flees the hotel, John pursuing her. They run till they find themselves in an adult entertainment venue. Moments later, John and Elizabeth gravitate towards each other, finding themselves interlocked in each others seemingly inescapable embrace.       

The following morning, John senses that he will never see her again. He attempts to share with her details about his life.  Elizabeth tells him that it is too late as she leaves the apartment. John begins his mental countdown to 50, hoping she will come back by the time he is finished.

==Cast==
* Mickey Rourke as John Gray
* Kim Basinger as Elizabeth McGraw
* Margaret Whitton as Molly
* David Margulies as Harvey
* Christine Baranski as Thea Karen Young as Sue
* William De Acutis as Ted (as William DeAcutis)
* Dwight Weist as Farnsworth
* Roderick Cook as Sinclair, the Critic
* Victor Truro as Gallery Client

==Reception==
In a preview screening of the film for 1,000 people, all but 40 walked out.  Of the 40 who filled out cards, 35 said they hated it. 

9½ Weeks has a mixed 64% rating on Rotten Tomatoes based on 22 reviews.  Roger Ebert praised the film, giving it three and a half stars, stating: "A lot of the success of 9½ Weeks is because Rourke and Basinger make the characters and their relationship convincing." He further elaborated by saying that their relationship was believable, and unlike many other characters in other erotic films at that time, the characters in this movie are much more real and human. 
 1986 Golden Worst Actress Madonna for Worst Original John Taylor, Worst Screenplay Howard the Duck).

==Soundtrack and score== John Taylor, giving his first solo singing performance during a hiatus in Duran Durans career. The song reached #23 on the Billboard Hot 100|Billboard Hot 100 and #42 on the UK Singles Chart. Music for the score was composed by Taylor and Jonathan Elias. Original music for the movie was also written by Jack Nitzsche, but his compositions are not included on the soundtrack.
 Corey Hart, Joe Cocker, Devo, Eurythmics and Stewart Copeland. Winston Grennans reggae "Savior" as well as Jean Michel Jarres "Arpegiator", played during the sex scene on the stairs in the rain, were not included on the record.

==Home Entertainment Releases==
In 1998, Warner Bros. released an "uncut, uncensored version" on DVD that was 117 minutes. 

==Derivative works==
===Sequels===
In 1997, a sequel appeared direct-to-video called Another 9½ Weeks, starring Mickey Rourke and Angie Everhart and directed by Anne Goursaud. In 1998, a straight-to-video prequel was made called The First 9½ Weeks that did not include either original lead actor.

===Parody===
A parody to the original movie, 9 ½ Ninjas!, was released in 1991.

==See also==
* Fifty Shades of Grey (film)|Fifty Shades of Grey (film)
* Wild Orchid (film)|Wild Orchid (film) 
* Sex in film

==References==
 

;Unfootnoted
*  

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 