Libaas
{{Infobox film
| name           = Libaas
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Gulzar
| producer       = Vikas Mohan
| writer         = Gulzar
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Shabana Azmi Naseeruddin Shah Raj Babbar Sushma Seth Utpal Dutt
| music          = R. D. Burman Gulzar  (lyrics) 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
 1988 Hindi drama film written and directed by Sampooran Singh Gulzar|Gulzar. The film based on short story Seema published in collected stories in Raavi Paar. It is about married couples of urban India having extramarital relations and adultery. The film won critical acclaim in international film festivals  but never got released in India till date.

It was also the last film, until Dus Kahaniyaan  got released in 2007, where Shabana Azmi and Naseeruddin Shah, the lead pair of Parallel Cinema in the 1980s, appeared together after giving notable performances in Sai Paranjpyes Sparsh (film)|Sparsh (1980) and Shekhar Kapoors Masoom (1983 film)|Masoom (1983). 

On November 22, 2014, film was shown at the International Film Festival of India in Goa  after a gap of 22 years since its screening at the International Film Festival at Bangalore in January, 1992.  

==Cast==
*Shabana Azmi
*Naseeruddin Shah
*Raj Babbar
*Sushma Seth
*Utpal Dutt
*Annu Kapoor
*Savita Bajaj

==Crew== Gulzar
*Music by Rahul Dev Burman (released after his death in 1994) Gulzar
*Playback singers Suresh Wadkar, Lata Mangeshkar

==Soundtrack==
The music was by R. D. Burman with lyrics by Gulzar.

* Sili Hawa Choo Gayi - Lata Mangeshkar  
* Khamosh Sa Afsana - Lata Mangeshkar, Suresh Wadkar
* Kya Bura Hai, Kya Bhala - Lata Mangeshkar
* Phir Kisi Shakh Ne - Lata Mangeshkar

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 
 