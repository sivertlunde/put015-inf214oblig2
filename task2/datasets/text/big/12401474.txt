The Night the World Exploded
{{Infobox film
| name           = The Night the World Exploded
| image          = Poster of the movie The Night the World Exploded.jpg
| alt            =
| caption        = Theatrical release poster 
| director       = Fred F. Sears
| producer       = Sam Katzman
| writer         = Jack Natteford Luci Ward Kathryn Grant Tristram Coffin
| music          = Ross DiMaggio
| cinematography = Benjamin H. Kline Irving Lippman
| editing        = Paul Borofsky Al Clark
| studio         = Clover Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 B film genre. 

==Plot== Tristram Coffin) Kathryn Grant) has built a machine that can predict earthquakes. After predicting one will hit California within the next 24 hours to a uniformly skeptical Gov. Cheney (Raymond Greenleaf) and state-level political and civil defense officials, the earthquake does materialize and does immense damage to northern parts of the state. Now with the support and funding necessary from the reformed skeptics, the team works on further predictions and comes to the conclusion that a wave of earthquakes are pending in and around the southwestern United States. They trace the epicenter of the pending disaster to an area beneath the Carlsbad Caverns and descend to a hitherto unexplored level.
 specific atomic traits, it is named Element 112 just because so far, 111 chemical elements had been discovered. A computer determines that in approximately one month, enough of Element 112 will emerge from the deep earth to cause the entire planet to explode. A desperate operation ensues worldwide to blast and trench the ground to let water in and cover Element 112, keeping it from drying out and expanding.

==Cast==
  Kathryn Grant as Laura Hutchinson
* William Leslie as Dr. David Conway Tristram Coffin as Dr. Ellis Morton
* Raymond Greenleaf as Gov. Chaney Charles Evans as General Bortes
* Frank J. Scannell as Sheriff Quinn Aide
* Fred Coby as Ranger Brown
* Paul Savage as Ranger Gold Terry Frost as Chief Rescue Worker
 

==Production==
The Night the World Exploded went into production with shooting locations at the Carlsbad Caverns in New Mexico; the Iverson Movie Ranch in Chatsworth, Los Angeles|Chatsworth, California; and the Databton Corporation Building in Pasadena, California|Pasadena, California. Principal photography took place from November 8–20, 1956. 

==Reception==
Columbia Pictures released The Night the World Exploded theatrically as a double bill with The Giant Claw (1957). Critical reception was not positive, with Hal Erickson of The New York Times later commenting, "Despite all the scientific doublespeak, The Night the World Exploded is doggedly non-intellectual in its execution and appeal." 

Film critic Leonard Maltin noted that the film disappointed:""Scientists discover a strange, exploding mineral that threatens to bring about title catastrophe and rush to prevent it. OK idea hampered by low budget." 

==References==
Notes
 

Bibliography
 
* Walker, John, ed. Halliwells Whos Who in the Movies (14th ed.). New York: HarperResource, 1997. ISBN 0-06-093507-3.
 

==External links==
*  

*  
*  
*  

 
 
 
 
 
 
 
 