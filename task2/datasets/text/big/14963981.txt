Le ménage moderne du Madame Butterfly
{{Infobox film
| name           = Le Ménage moderne Du Madame Butterfly
| image          =
| caption        =
| director       = Bernard Natan
| producer       = Bernard Natan
| eproducer      =
| aproducer      =
| writer         = Bernard Natan
| starring       = Bernard Natan, J. H. Forsell, two uncredited actresses and one uncredited actor
| cinematography = Not known
| editing        = Not known
| distributor    = Rapid Film
| released       =  
| runtime        =
| rating         =
| country        = France
| awards         =
| language       = Silent film
| budget         =
| afdb           =
| iafd           =
}} hardcore pornographic film from France. It is notable for being the earliest known adult film to incorporate bisexual and homosexual intercourse. Burger, John R. One-Handed Histories: The Eroto-Politics of Gay Male Video Pornography. New York: Harrington Park Press, 1995. ISBN 1-56023-852-6 

==Background==
 , now questions the film’s attribution to Bernard Natan, and rejects his earlier conclusion that Natan played the part of Vinh Linh (Pinkertons "coolie boy" manservant), based on on-screen evidence of the actor’s age and foreskin status. 

Le Ménage moderne du Madame Butterfly is one of the earliest of the pornographic films controversially attributed to Natan, and the first of his works to depict homosexuality and bisexuality.  Although the release date is uncertain, scholars believe the film was distributed as early as 1920. 
 rickshaws and a sailing ship on the Pacific Ocean. The costumes and sets are almost lavish. The film has a lengthy and complex plot, and includes intertitles. 

==Synopsis==
The film is based on the opera Madama Butterfly by Giacomo Puccini. 

"Lt. Pinkerton" (played by mainstream French actor J. H. Forsell ) is a strapping American sailor on leave in Japan. He marries the young "Madame Butterfly" (an uncredited actress) and ravishes her while Butterflys maid, "Soosooky" (another uncredited actress) watches and masturbates. Pinkerton then abandons Butterfly.  A new character, "the coolie boy" (role originally attributed to Natan), spies on Butterfly and Soosooky as they engage in lesbian sex. He masturbates while watching them.
 fellates Pinkerton and Sharpless.
 English intertitles racist in tone. 

==Notability==
Le Ménage moderne du Madame Butterfly is the oldest known motion picture to depict hardcore homosexual sex acts on film.   

The first known motion pictures depicting nude men were made by Eadweard Muybridge in the 1880s and 1890s as part of his studies of human locomotion. 

The first known pornographic film of any kind appears to have been made in 1908.  Between 1908 and the advent of public theatrical screenings in the United States in 1970, about 2,000 hardcore pornographic films were made. (Roughly 500 of these were made prior to 1960.) The best estimate is that about 10 percent of all hardcore "stag films" made prior to 1970 contain some sort of homosexual activity. This ranges from an innocuous hand on a shoulder, thigh or hip to hardcore anal and oral sex (and much more). Nevertheless, nearly all the extant works depict homosexual sex in the context of a heterosexual hegemony. Most depict homosexual sex occurring during heterosexual intercourse (essentially making the sex act bisexual in nature). 

Le Ménage moderne du Madame Butterfly is unusual in that it not only depicts homosexual sex acts, but that it does so very early in the history of pornographic film. However, like most of the films which came after it, Le Ménage moderne du Madame Butterfly shows Pinkertons male-male sex acts as deviant, firmly establishing his heterosexuality. Pinkerton and the women are constructed as essentially bisexual (e.g., male-male sexual contact occurs while the men are also having heterosexual intercourse).   Vinh Linh, however, is depicted as an enthusiastic bottom/voyeur homosexual.

The film has also been thought notable for being one of the first adult films to be produced or directed by Bernard Natan. Scholars have expressed surprise that a beginning filmmaker would produce a work which contained sex acts which might anger or offend its target audience (straight men), although the French clandestine film milieu was much more tolerant of sexual diversity and sodomy than its American equivalent. That Bernard would do so is a testament, film historians have said, to his willingness to take risks and his subtle understanding of the role homoeroticism and homosexuality play in French male sexual identity. 

Natans alleged willingness to not only produce, write, and direct but also star in a hardcore bisexual film is even more startling when one recognizes that, within nine years of the release of Le Ménage moderne du Madame Butterfly, Bernard Natan would be the owner of the largest mainstream film studio in France—Pathé.   

==References==
 

==External links==
* 

 

 
 
 
 
 
 