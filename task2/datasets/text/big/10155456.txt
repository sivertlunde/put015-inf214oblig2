Detention (2003 film)
{{Infobox film
| name           = Detention
| image          = detentionposter.jpg
| image_size     =
| caption        = Promotional poster
| director       = Sidney J. Furie
| producer       = Jack Gilardi Jr.,  Avi Lerner Paul Lynch,  John Sheppard Mif (Anthony J. Mifsud) Mpho Koaho
| music          = Amin Bhatia
| cinematography = Curtis Petersen
| editing        = Saul Pincus
| distributor    = Nu Image Films,  Alliance Atlantis Communications
| released       = July 5, 2003
| runtime        = 98 min.
| country        = United States Canada Hungarian
| budget         = Canadian dollar|$ CAD 5,000,000  
}}
Detention is a 2003 action film directed by Sidney J. Furie. It stars Dolph Lundgren as a soon to be retired high school teacher Sam Decker who has one last detention to proctor and Alex Karzis as Chester Lamb. Unfortunately, drug runners have chosen to attack the school.  Sam must band together the trouble makers and misfits in detention to defeat the criminals and stay alive.

==Plot==

Hamilton High School is no place for a teacher who cares. Teacher Sam Decker (Dolph Lundgren), a former soldier, doesnt care anymore. Hes quitting. After military service in the Gulf War and the former Yugoslavia, he returned to his inner city neighborhood to teach at the toughest school in town. He wanted to make a difference—but nothing does at Hamilton High.

Frustrated and angered by a system that doesnt work, Sam hands in his resignation. But the day he does, he pulls one last detention duty with the toughest kids in the school. Its a bad day for all.

But Hamilton High is about to become an all too real battleground when a well-organized group of killers armed with automatic weapons and explosives invade the supposedly deserted school after hours, using the school to hide $300 million worth of heroin theyve hijacked. The plan is to use the school auto shop to prep two police cars, stash the drugs in secret compartments, and drive away safely.

Led by the brilliant and sadistic Chester Lamb (Alex Karzis), the invaders are surprised to discover that Sam and the kids from the detention room are still inside the school.

The hunt is on as Lambs henchmen ruthlessly stalk the teacher and students through the schools halls and classrooms, while Lamb tracks them on the surveillance cameras, turning the schools security system against his prey.

Sam and the kids band together and manage to combat the armed thugs and foil Lambs perfectly planned crime. Along the way, they discover a sinister conspiracy that reaches to the police department and even the highest level of government.

Paid-off local police and school security guards were supposed to make sure the school was empty after hours. But nobody figured Sam and his juvenile delinquents into the equation.

Lamb and his crew—who include Gloria Waylon (Kata Dobo), punkers Viktor (Joseph Scoren) and Alek (Anthony J. Mifsud) -- continue to stalk the students through the school.

But Sam still has a few tricks to teach the kids, and hes got some things to learn from them. Tonight at Hamilton High, the lesson is survival.

==References==
 

== External links ==
*  
*   at the Russian
*  
*  
*  

 

 
 
 
 
 
 