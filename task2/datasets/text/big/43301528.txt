Sathuranga Vettai
{{Infobox film
| name           = Sathuranga Vettai
| image          = Sathuranga Vettai poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = H. Vinoth
| producer       = Manobala Sanjay Rawal Thirrupathi Brothers|N. Subash Chandrabose
| writer         = H. Vinoth Natraj Ishara Nair Ponvannan Ilavarasu
| music          = Sean Roldan
| cinematography = K. G. Venkatesh
| editing        = Raja Sethupathi  Thirrupathi Brothers Film Media Thirrupathi Brothers Film Media
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Thiruppathi Brothers Film Media on 18 July 2014   and received positive reviews from critics. 

==Cast== Natraj as Gandhi Babu
* Ishara Nair as Bhanu
* Ponvannan as ACP
* Ilavarasu as Chettiyar
* Piraisoodan
* Valavan as Valavan
* Dharmapuri Nagarajan as Gutu
* Senthi as Valli
* Ramachandran Durairaj as Thillagar

==Production==
Director Vinoth said that he was turned down by several producers before he met director Nalan Kumarasamy who he handed over his films script. While Nalan was on a trip, his mother found the script, read and loved it and asked Nalan to read it too, who too liked it and came forward to help him find a producer. Actor Manobala, who had wanted to start a new production house and had asked Nalan to find some good stories, read the script by Vinoth and instantly agreed to produce it.   
 dark humour".  It was reported that the films screenplay was written in "a six-episode format". 

==Soundtrack==
{{Infobox album 
| Name = Sathuranga Vettai
| Longtype = to Sathuranga Vettai
| Type = Soundtrack
| Artist = Sean Roldan
| Border = yes
| Alt =
| Caption = 
| Released = 27 April 2014
| Recorded =  Feature film soundtrack Tamil
| Label = Think Music
| Length = 16:59
| Producer = Sean Roldan
| Last album = Vaayai Moodi Pesavum (2013)
| This album = Sathuranga Vettai (2014)
| Next album = Mundasupatti (2014)
}}

The films soundtrack was composed by Sean Roldan. The album features five tracks and was released on 27 April 2014. Many colleagues of Manobala were present at the launch. 

The Times of India gave the soundtrack a rating of 3 stars and wrote, "the composer doesnt disappoint in his second outing. This five-track album has a fair mix of every genre...with a touch of Seans guitar techniques that you cannot miss out".  Indiaglitz.com wrote that Sathuranga Vettai was "Seans supreme sophomore effort" and that he "is definitely here to stay for a long time". 

{{Track listing
| headline       = Track listing 
| extra_column   = Singer(s)
| music_credits  = no
| lyrics_credits = no
| all_lyrics     = 
| total_length   = 16:59

| title1 = Yemarum Jename
| extra1 = Pradeep Kumar, Sean Roldan
| length1 = 3:33
| title2 = Kadhala Kadhala
| extra2 = Kalyani Nair
| length2 = 3:44
| title3 = Verichodi Ponathada
| extra3 = Sean Roldan
| length3 = 3:13
| title4 = Munne Yen Munne
| extra4 = Sathya Prakash
| length4 = 3:55
| title5 = Porape Nadape
| extra5 = Anthony Dasan
| length5 = 2:34
}}

==Release== Thiruppathi Brothers Film Media in June 2014.  The satellite rights of the film were sold to STAR Vijay. Prior to the release, Manobala had held several sneak previews of the film for personalities from the film industry, who were impressed with the film. Thereafter, Thirrupathi Brothers were trying to release the film in over 200 screens in Tamil Nadu. 

===Critical reception=== IANS gave it 3 stars out of 5 and wrote, "The reason newcomer Vinoths Sathuranga Vettai is an almost flawless film in the genre is because it succeeds in managing to con the audiences as well, more than once. Its not a film about people who con but one about those who get conned, why they get conned and how they get conned". 

Baradwaj Rangan gave a more mixed review, writing, "Vinoth employs a series of tricks to amp up his narrative, to make us feel we’re watching a really cool movie...What we don’t get is the pleasure of being conned. We don’t see too many of these films in Tamil, so the newness keeps us watching (this is one of those not-bad-for-a-first-film films) — but the cons aren’t shaped well. They seem too easy (except for the last one) and the victims seem too dumb". 

===Accolades===
* Vikatan Award for Best Film  
* Second Best Feature Film at Chennai International Film Festival  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 