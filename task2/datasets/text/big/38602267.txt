Beyond Right and Wrong
{{Infobox film
| name = Beyond Right & Wrong: Stories of Justice and Forgiveness
| image = Beyond Right and Wrong.jpg
| image_size =
| border =
| alt =
| caption =
| director = Roger Spottiswoode Lekha Singh
| producer = Lekha Singh Rebecca Chaiklin
| writer =
| screenplay =
| story =
| based on =  
| narrator =
| starring =
| music =David Hirschfelder
| cinematography = Robert Adams, Robert Fitzgerald, Tony Hardmon
| editing = Paul Seydor, A.C.E.
| studio =Article 19 Films
| distributor =
| released =  
| runtime = 80 min.
| country = USA
| language =English
| budget =
| gross =
}}

Beyond Right & Wrong: Stories of Justice and Forgiveness is a 2012 American documentary film about restorative justice and forgiveness. It is directed by Roger Spottiswoode and Lekha Singh and produced by Lekha Singh and Rebecca Chaiklin. The film depicts victims and perpetrators of the Rwandan Genocide, the Israeli–Palestinian conflict, and The Troubles in Northern Ireland.
 Patrick Magee, Lord John Marina Cantacuzino, Bishop John Rucyahana, and Dr. James Smith.

==Recognition== General Assembly in New York.  The film was also part of the World Cinema Narrative section at the 2012 Hamptons International Film Festival.  In 2014, The Huffington Post ran a series of eight articles inspired by the film. Authors included Kweku Mandela Amuah, Judith Light, Jo Berry, and co-director and producer Lekha Singh. 

===Awards===
Beyond Right & Wrong received the following awards:

2013:
* Best Avant Garde Film by the American Psychological Association 

* Best Documentary Film by Fingal Film Festival 

* Runner-up for Best Documentary by the LA Jewish Film Festival 

2014:
* Social Impact Award at the Collective Conference in Park City, which bestowed a $50,000 matching grant from Pvblic. 

In 2014, co-director and producer Lekha Singh received the Snowball Influencer Award from Cause Brands. 

===Reviews===
* John DeFore.   The Hollywood Reporter. October 26, 2012. Retrieved July 24, 2014.

* Giles Fraser.   The Guardian. February 8, 2013. Retrieved July 24, 2014.

* Onslow.   Corner Magazine. February 6, 2014. Retrieved July 23, 2014.

* Neil White.   Every Film Blog. February 7, 2014. Retrieved July 23, 2014.

* Marina Cantacuzino.   Huff Post Impact. February 18, 2014. Updated April 20, 2014. Retrieved July 23, 2014.

* Frank Farley and Mona Sarshar.   PsycCRITIQUES, Vol. 59, No. 8, Article 9. February 24, 2014. Referenced by Ryan M. Niemiec.   PsycCRITIQUES Blog. March 27, 2014. Retrieved July 23, 2014.

===Endorsements=== Archbishop Desmond Tutu,  Judith Light,  Charlize Theron,  Hugh Jackman,  and Nelson Mandela’s grandson Kweku Mandela Amuah  all tweeted about the film.

==Online Viewership==
In February 2014, Beyond Right & Wrong collaborated with   to promote the 52-minute version of the film while directing $500,000 to charity. A portion of the money donated by Operation Kids| Operation Kids Foundation and Share the Mic is sent to a partnering charity when people watch the film online for free.  Indiewire  and Forbes  also wrote about the campaign.

===Partner Organizations===
Beyond Right & Wrong and FilmRaise partnered with the following NGOs: Anasazi Foundation, Building Bridges for Peace, Cinema for Peace Foundation, ConnecTeach, Creative Visions Foundation, Empower Mali, The Forgiveness Project, Free the Children, Heshima Kenya, Inclusion Center for Community and Justice, The Institute for Justice and Reconciliation, Jeans 4 Justice, Kidnected World, The Malala Fund, Nelson Mandela Centre of Memory, PeacePlayers International, Search for Common Ground, Solar Electric Light Fund, Ubuntu Education Fund, Utah Refugee Center, Utah Rotary District 5420, WITNESS, and Women for Women International. 

Promotional partners include The Arbinger Institute, BRITDOC, Pvblic Foundation, and the Tutu Global Forgiveness Challenge. 

===News Coverage===
* Daniel Jenks.   MariaShriver.com. February 26, 2014. Retrieved July 23, 2014.
*   Nelson Mandela.org. March 7, 2014. Retrieved July 23, 2014.

* Whitney Evans.   The Deseret News. March 22, 2014. Retrieved July 23, 2014.

* Candice Madsen.   The Deseret News. April 13, 2014. Retrieved July 23, 2014.

* Philip Hellmich (interview host).   Shift Network. Recorded June 17, 2014. Retrieved July 23, 2014.

* Kweku Mandela.   The African-American Observer. Retrieved July 23, 2014.

* Liz Smith.   JudithLight.com. Retrieved July 23, 2014.

== References ==
 

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 