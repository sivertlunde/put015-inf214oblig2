Bingo Bongo
{{Infobox film
| name = Bingo Bongo
| image = BingoBongo.jpg
| image size =
| caption =
| director = Pasquale Festa Campanile Mario & Vittorio Cecchi Gori
| writer = Franco Ferrini   Enrico Oldoini
| narrator =
| starring = Adriano Celentano    Carole Bouquet  
| music = Pinuccio Pierazzoli
| cinematography = Alfio Contini
| editing = Amedeo Salfa
| distributor =
| released =  December 25, 1982  
| runtime = 105 min
| country = Italy West Germany Italian
| budget =
| preceded by =
| followed by =
}} 1982 Italy|Italian family comedy film directed by Pasquale Festa Campanile and starring Adriano Celentano as an Italian Tarzan character escaping across Milan and speaking with all animals races. The film also created an Italian neologism indicating wild animal-like language or behaviour.

==Plot==
The film opens with the story about how Bingo Bongo was stranded in the African jungle as a baby when his plane crashed (in a manner remniscient and in parody of Tarzan). He was thrown out by parachute at the last instant and subsequently adopted by chimpanzees.

Years later, as a grown man with animalistic behaviour (and still wearing his old pacifier and parachute harness), Bingo Bongo is captured by an expedition, brought to an anthropology institution in Milan for study, and shut in a Cage (enclosure)|cage. Bingo Bongo proves not only to be extremely strong and highly intelligent and perceptive (with some uncomfortable results for the researchers), he also develops a crush on Laura, one of the researchers, and bonds with her pet chimpanzee Renato. Laura, on the other hand, tries her best to integrate Bingo Bongo into human society.

Finally, Bingo Bongo runs away and eventually hides at Lauras place, who continues her efforts. At first she makes only slow progress, but a surprise result is achieved when one evening the institutes director drops by: Bingo Bongo not only convincingly manages to pass himself off as a human (complete with fully developed articulation of the human language), he also throws the director off track by introducing himself as Lauras lover.

Because Laura continues to rebuff his romantic advances, however, Bingo decides to return to Africa, with Renato in company, but his efforts are all foiled. In time, it is revealed that the animals all around the world see him as their ambassador to humanity who will vouch for a more humane treatment of the animals, thanks to his ability to speak the animal - and now also the human - tongues. He returns to the institute, where he delivers his message in a   which Laura also attends. There she confesses (in ape language) that she does love him after all.

The film ends with Bingo Bongo taking up his work as animal ambassador - most notably by calming down King Kong, who later attends his wedding.

==Cast== 
* Adriano Celentano as Bingo Bongo 
* Carole Bouquet as Laura 
*  Felice Andreasi as Professor Fortis 
*  Enzo Robutti as Doctor Muller
* Sal Borgese as Doctor

==See also== Animalism

==External links==
* 

 
 
 
 
 
 
 
 

 