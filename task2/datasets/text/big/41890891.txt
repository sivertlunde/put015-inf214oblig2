Peeples (film)
{{Infobox film
| name           = Tyler Perry Presents Peeples
| image          = Peeples film poster.jpg
| alt            = 
| caption        = 
| director       = Tina Gordon Chism
| producer       = Stephanie Allain Ozzie Areu Paul Hall Matt Moore Tyler Perry
| writer         = Tina Gordon Chism Craig Robinson Kerry Washington David Alan Grier
| music          = Aaron Zigman
| cinematography = Alexander Gruszynski
| editing        = David Moritz	 	 34th Street Films
| distributor    = Lionsgate 
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $15 million 
| gross          =  $9,307,065 
}} Craig Robinson and Kerry Washington  and was released by Lionsgate on May 10, 2013.  The film closed in theaters on June 13, 2013.

== Plot ==
  Sag Harbor in the Hamptons. Their weekend is interrupted when Wade Walker (Robinson), the fiance of their daughter Grace (Washington) crashes to propose marriage after being goaded by his brother Chris.  Upon arrival he loses his wallet while being chased by the family dog.  While at the grocery store Wade notices Virgil (David Allen Grier), Graces father and a judge having a conversation with the an unidentified woman, who is later revealed to be the mayor of the town, as well as a conversation between Gloria (Hawk), Graces Sister and her lover Meg;(Lewis-Davis) unknown to the family.  As the time goes on, Wade begins to learn other things about Grace, such as her older ex-boyfriends, and the fact that she has breast implants.  Wade also discovers that Graces mother Daphne (S. Epatha Merkerson), was a professional singer during the 70s, as well as a recovering alcoholic.  

At dinner Wade in an attempt to propose to Grace, asks if in tradition of his family can they go around the table and talk about the things they love the most about the people in their live, which end abruptly as Virgil reminds him that he had to pay after Wade had no money at the store; due to his wallet being lost.  Wade later decides to go to the club to see Virgil play, but realizes he is not there. While riding through the park near the beach he encounters a group of nudist, of whom Virgil is amongst.  Returning to the guest house, Wade confronts Grace, about her breasts, to which she is quite honest, then lures him into playing naughty school girl.  Virgil returning home from his swim peers though the window and runs off in disgust. Wade discovers that Simon (Tyler James Williams) a talented musician, is also a thief,  and while looking around in the Peeples house he finds Daphnes costume head piece from her performer days; and begins to sing with it on his head, but is interrupted by Virgil. Wades brother Chris (Malcolm Barrett) shows up unexpected to "help" Wade propose to Grace, and because of a Gamma Phi sweater he borrowed from his roommate, Virgil thinks they are fraternity brothers, and invites him to stay at the house as well.  Later at Nana Peeples(Diahann Carroll) house Wade gets Daphne to sing, while Virgil looks on in disgust, and Chris thinks he may have a chance with Gloria, which Meg becomes politely angered.  Upon returning to the house Daphne finds her expensive earrings have been stolen, Virgil immediately concludes Wade is the culprit.  

Wade and Chris are found later at the bar, to which they see Simon attempting to talk to an uninterested woman sitting at the bar.  They follow him into the bathroom and pretend to be a couple of thugs, but resign to telling him to take his mothers earrings back home and stop stealing.  Chris returns to the guest house afterward to find Gloria, while Wade finds Virgil at a sweat tent, and attempt to sweat it out for Graces hand in marriage, which leads to him burning the tent down. The next day Wade meets Grace at the Moby Dick Day celebration, she tell him he looks terrible and offers him a drink from a thermos with a mushroom drawn on it.  The mayor confronts Wade, and later while Virgil is giving his rendition of Captain Ahab, Wade begin to hallucinate that Virgil is talking to him; which makes him threaten him and charge him with a harpoon, causing him to be knocked unconscious.  After awaking at Graces home, Wade is again insulted by her father, and decides it is best if he Grace and Chris all head back to New York, but Grace decides to stay with her family; so Wade and Chris leave.  Upon the departure of the brothers, everyone begins to spell the truth about what has been going on the house, Virgil admits that he has been swimming with the Humpback Whale , but it is in fact purely non sexual, Gloria and Meg finally tell everyone about their relationship, Simon admits to his constant stealing in order to gain street credibility, and Daphne Admits she put mushrooms in her drinks; the final moment is when the dog returns the wallet and Simon pulls Wades proposal ring from his pocket.  

Distraught, Grace falls into her mothers arms realizing Wade was telling the truth.  She returns to New York a few days later after not being able to get in touch with Wade, but seeing his schedule book; she sees he is playing at a kids museum.  Grace tells Wade she is sorry and proposes to him, and after asking her to trust him and never keep secrets, he proposes to her.  Virgil shows up, to finally accept Wade into the family, and the entire Peeples family joins Wade on stage to perform with him for the children. Melvin Van Peebles appears as grandpa Peeples.        

== Cast == Craig Robinson as Wade Walker
* Kerry Washington as Grace Peeples
* David Alan Grier as Virgil Peeples
* S. Epatha Merkerson as Daphne Peeples 
* Diahann Carroll as Nana Peeples
* Tyler James Williams as Simon Peeples
* Kali Hawk as Gloria Peeples
* Malcolm Barrett (actor) | Malcolm Barrett as Chris Walker  
* Ana Gasteyer as Mayor Hodge  
* Kimrie Lewis-Davis as Meg
* Alfonzania Law- as Sissy Peeples

==Soundtrack==
The soundtrack was released May 7, 2013 by Lakeshore Records for MP3 download. It features six tracks used in the film:
 Craig Robinson – "Speak It (Dont Leak It!)" 
# Tyler James Williams and Alfonzania Law – "Drawers on the Floor"
# Maxayn Lewis – "Turn You On"
# Aaron Zigman – "Sweat Lodge"
# Aaron Zigman – "Run Chickens Run" Craig Robinson, David Alan Grier, & Maxayn Lewis – "Speak It (Dont Leak It!) Reprise"

==Box office== The Great Gatsby  and Pain & Gain. It made $9,177,065 total; Its the lowest ever for a Tyler Perry production.

==Home Media==
Peeples was released to DVD, Blu-ray and Pay-Per-View September 10, 2013, it was released early on August 27 on Video On Demand. The DVD & Blu-ray contains three featurettes including a film commentary with director Tina Gordon Chism, producer Stephanie Allain and the cast of the film. It also has "Meet the Peeples, Jam With the Fam" featurette, a Gag reel and also a Digital Copy version of the film.

==Critical response==
The film received mostly negative reviews, scoring a 36% on  .  Rotten Tomatoes critical consensus states, "Peeples is a warm, amiable farce that offers a few chuckles but mostly falls back on predictable plotting and an overwrought message." 

==References==
{{reflist|refs=
 
 
 
 
 
 
 
 
 
}}

==External links==
*  
*  
*  

 

 
 
 
 
 
 