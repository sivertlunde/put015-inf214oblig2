Heartless (2014 film)
 
 
{{Infobox film
| name           = Heartless
| image          = Heartless_2014,_official_poster.jpg
| image_size     = 220px
| border         = yes
| alt            = Heartless
| caption        = Theatrical release poster
| film name      = Heartless
| director       = Shekhar Suman
| producer       = Sajid Rizvi Alka Suman
| writer         = Niranjan Iyengar  (dialogue)
| screenplay     = Nina Arora
| story          = Shekhar Suman
| based on       = 
| narrator       = 
| starring       = {{Plain list|
* Adhyayan Suman
* Ariana Ayam
* Deepti Naval
* Om Puri
* Shekhar Suman
* Madan Jain
}}
| music          = Gaurav Dagaonkar

| lysrics        = Divya Jambwal & Sahaurti Sharma
| cinematography = Derrick Fong
| editing        = Akshara Prabhakar
| studio         =  Windmill Entertainment 
| released       =  
| runtime        = 127 minutes 
| country        = India UAE
| language       = Urdu Hindi 
| budget         = 
| gross          = 
}}

Heartless is a 2014 Bollywood medical thriller film directed by Shekhar Suman.   Shekhar Suman also stars in the film along with his son Adhyayan Suman. The film also features Ariana Ayam, Deepti Naval, Om Puri and Madan Jain amongst others. It released on 7 February 2014, to mixed reviews from critics.  The film focuses on anaesthesia awareness, where a patient cannot move or communicate, but is aware to varying degrees of what is happening during surgical procedures.      Several critics have noticed striking similarities between the film and the 2007 Hollywood medical thriller Awake (film)|Awake, leading some to describe Heartless a copy of said film. 

==Plot==
A young business magnate Aditya Singh (Adhyayan Suman) is in love with Ria (Ariana Ayam). Aditya requires a heart transplant but refuses due to guilt of killing his father during a boat ride in his childhood. Dr. Sameer Saxena aka Sam (Shekhar Suman) is Adityas heart surgeon and friend. Aditya asks Dr. Sameer to arrange his elopement with Ria. They marry privately without his mothers knowledge, then Aditya goes to the hospital for the operation. While Adityas new wife and his mother, Gayatri Singh (Deepti Naval), await completion of his surgery, Aditya encounters anaesthesia awareness.

The surgical pain causes Aditya to have a clairvoyant experience exposing Dr. Sameers plot to murder Aditya, also revealing that Ria worked at the hospital under Dr. Sameer and has conspired with him against Aditya. Rias plan was to marry Aditya, and then poison the donor heart to cause its rejection, thus murdering Aditya to collect insurance money to pay off Dr. Sameers debts. The scheme unravels and Gayatri, realising what has happened, sacrifices herself so that Aditya can live, having barely survived the surgery and attempt on his life. She commits suicide and her heart is switched for the poisoned one by Dr. Trehan (Om Puri). During a shared out-of-body experience, Gayatri reveals to Aditya that his fathers death was not his fault but just an accident and that he should value his life for which both his parents has sacrificed. Dr. Sameer and his conspirators are being arrested and Aditya comes back to consciousness. Film closes with Aditya telling Ria that he knows the truth due to anaesthesia awareness and hands her over to police.

==Cast==
* Adhyayan Suman as Aditya Singh/Adi
* Ariana Ayam as Ria/Shruti Tandon
* Shekhar Suman as Dr Abhinav velidi
* Deepti Naval as Gayatri Singh (Adityas mother)
* Om Puri as Dr. Sanjay Trehan
* Madan Jain

==Marketing==
The theatrical trailer of Heartless was released on 14 November 2013.  The audio of the film was released by Sachin Tendulkar at Reliance Digital Xpress, Prabhadevi on 19 December 2013.   

==Soundtrack==
The Soundtrack for the film was composed by Gaurav Dagaonkar with Pakistani band called Fuzon (Khurram Iqbal, Shallum Asher Xavier, Imran Momina) for Ishq Khuda. The lyrics are by Arafat Mehmood, Seema Saini, Shekhar Suman & S.K. Khalish. The soundtrack was released on 7 January 2014. The song Main Dhoondhne Ko Zamaane Mein  and "soniye" became popular.  Sukanya sang a duet with Mohit Chauhan in the film. 

{{Infobox album  
| Name = Heartless
| Type = Soundtrack 	
| Artist = Gaurav Dagaonkar
| Cover = 
| Released =  	
| Genre = Film soundtrack	
| Length = 38:10
| Label = 
| Producer = 
| Last album = Chal Pichchur Banate Hain  (2012)
| This album = Heartless (2014)
| Next album = 	
}}

===Track listing===
{{track listing	
| extra_column = Singer(s)
| lyrics_credits = yes
| title1 = Mashooqana
| extra1 = Ash King, Arunima Bhattacharya
| lyrics1 = Arafat Mehmood
| length1 = 4:24
| title2 = Main Dhoondne Ko Zamaane Mein
| extra2 = Arijit Singh
| lyrics2 = Arafat Mehmood
| length2 = 4:23
| title3 = Soniye KK
| lyrics3 = Seema Saini
| length3 = 5:01
| title4 = Ishq Khuda
| extra4 = Khurram Iqbal
| lyrics4 = S.K. Khalish
| length4 = 5:02
| title5 = What A Feeling
| extra5 = Mohit Chauhan & Sukanya Ghosh
| lyrics5 = Shekhar Suman
| length5 = 4:00
| title6 = Heartless
| extra6 = Mohit Chauhan, Aniruddh Bhola & Suzanne DMello
| lyrics6 = Seema Saini
| length6 = 4:27
| title7 = Thanks Brother
| extra7 = Shekhar Suman & Adhyayan Suman
| lyrics7 = Seema Saini
| length7 = 3:10
| title8 = Mashooqana
| note8 = Sanjoy Deb Remix
| extra8 = Ash King, Arunima Bhattacharya & Sanjoy Deb
| lyrics8 = Arafat Mehmood
| length8 = 3:29
| title9 = Main Dhoondne Ko Zamaane Mein
| note9= Reprise
| extra9 = Arijit Singh
| lyrics9 = Arafat Mehmood
| length9 = 4:14 
}}

===Reception===
Times of India gave 3.5 stars to the music of the film.  Koimoi gave 2.5 stars to the album. 

 the first half "routine"  but that after the intermission, "the film becomes a thrilling roller-coaster ride with terrific punches."    Paloma Sharma of Rediff.com gave it 1 out of 5 stars telling viewers to "stop trying to make sense of the script because there isnt any."    Taran Adarsh of Bollywood Hungama gave it 2.5 stars and praised performance of Adhyayan Suman.  Faheem Ruhani of India Today gave it 2 stars, stating that " Suman wastes far too much time making his son look super cool in song and dance sequences." 

==Box office==

 
Heartless was a box office flop.The film looked promising with a different concept story rather than the monotonous bollywood masala flicks but it did not get much appreciatio

==See also==
*Bollywood films of 2014

==References==
 

==External links==
*  

 
 
 
 
 
 
 