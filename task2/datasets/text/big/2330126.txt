Bionicle 2: Legends of Metru Nui
{{Infobox film
| name           = Bionicle 2: Legends of Metru Nui
| image          = Bionicle 2 Legends of Metru-Nui2.jpg
| image_size     = 230px
| caption        = 
| director       = Terry Shakespeare, David Molina
| producer       = Sue Shakespeare
| story          = Bob Thompson, Henry Gilroy
| screenplay     = Henry Gilroy, Greg Klein, Tom Pugsley
| editing        = Craig Russo
| music          = Nathan Furst
| studio         = Creative Capers Entertainment Miramax Films Lego
| distributor    = Miramax Films Buena Vista Home Entertainment
| released       = 19 October 2004
| runtime        = 75 min.
| country        = 
| language       = English
| budget         = $US 5,000,000
| gross          = 
}}
Bionicle 2: Legends of Metru Nui is the second film in the  . This movie follows the 2004 storyline and was created using Lego elements from the Bionicle series, and was released as a direct-to-video|direct-to-DVD. It is also the last film in the franchise to be given a rating by the MPAA. 

In this movie, Vakama recalls events that took place long before the classic Bionicle stories at Mata Nui, during which he, along with his friends Nuju, Matau, Onewa, Whenua, and Nokama were chosen to be the new Toa of the island of Metru Nui. To save the city, they must prove themselves worthy Toa, find their mask powers, and protect the "Heart of Metru Nui".  However, they also find themselves caught up in the schemes of the evil Makuta.

The film has many scenes taking  .

==Plot==
  Mata Nui Metru Nui. Toa would protect them, but eventually they fell one by one, defeated by an unrelenting shadow that sought to conquer the city.
 Turaga Dume, the citys leader, that they are worthy Toa.
 Vahki (the citys law enforcers) upon them. In the ensuing commotion, Onewa, Nuju, and Whenua are sucked into a massive vortex and imprisoned while Vakama and the others escape the Coliseum by leaping into a chute system, with the Dark Hunters giving chase.

Vakama, Nokama, and Matau abandon the chute system at Ko-Metru when the Dark Hunters get the flow reversed. Reaching the ground, Vakama spots Lhikan’s "spirit star" soaring overhead; as long as it burns in the night sky, it means that Lhikan is still alive. The trio hitch a ride on a Vahki transport to follow the star. Arriving in Po-Metru, they are suddenly ambushed by the Dark Hunters, but lose them when a herd of Kikanalo beasts stampedes through the canyons. As the Toa Metru flee, Nokama discovers that her mask allows her to speak and understand foreign languages, including that of the Kikanalo. She stops the stampede and speaks to the leader of the herd, who tells the Toa that Lhikan was taken by the Dark Hunters to the "Canyon of Unending Whispers".  With the help of the Kikanalo and Mataus newly discovered mask power of shape-shifting, the Toa Metru bypass the Vahki guarding the canyon, as well as the Dark Hunters.

Meanwhile, Onewa, Whenua, and Nuju are trying unsuccessfully to break out of prison when are approached by a mysterious Turaga, who explains that Toa mask powers are needed to escape.  To teach them how to find their mask powers, the Turaga puts them through a test in which they perform seemingly pointless tasks. Eventually, during an argument with Whenua, Onewa discovers his mask power, mind-control. Nuju, trying to halt their quarrel, also discovers his mask power, telekinesis, with which he creates a way out. As they traverse the tunnels of Onu-Metru, Whenua finds his mask power, night-vision, before the four reunite with Vakama, Nokama, and Matau. To their surprise, the Turaga reveals himself to be Lhikan, whose power was drained from him when they became Toa.  He inquires as to the safety of the "Heart of Metru Nui", which Vakama believed was Lhikan himself, but is actually the Matoran. As Vakama despairs at the misinterpretation, he discovers a small canister that contains Dume; the Dume from before was an impostor. Pursued by the Vahki, the Toa Metru, along with Lhikan, set out to stop the false Dume, who has summoned the Matoran to the Coliseum to be placed in canisters. By the time the Toa Metru and Lhikan arrive, all the Matoran are now asleep.
 Makuta in Great Spirit Mata Nui and the Matoran, Makuta plunges the former into slumber and begins absorbing energy from the city. The Toa rush to find the Matoran, but only a small amount can been carried on the transport; the rest will have to wait. The Toa gather as many Matoran capsules as they can and race to escape the crumbling city. On the way out, the Dark Hunters attack them again, but are killed, along with a Nivawk (Makutas spy), when the Makuta absorbs them.

Before becoming a Toa, Vakama was a Mask Maker in Ta-Metru, creating Kanohi masks from Kanoka disks. Prior to the start of the movie, he was requested by the false Dume to create the legendary Mask of Time. Initially, he was not successful, but he is now able to create the Mask of Time out of the six Great Disks. As the heroes make their way away from Metru Nui, Vakama realizes that Makuta wanted control of time to accomplish his goals.

To save the Matoran, the Toa must "follow the light" to a place far beyond Metru Nui. However, they are met en route by Makuta, now an almighty winged being. Donning the Mask of Time, Vakama confronts Makuta, but, unable to fully control the power of the mask, he only slows time down, rendering him unable to dodge the Makutas next attack. Lhikan intervenes and takes the attack himself, but is fatally injured and dies moments later with Vakama at his side, while Makuta claims the Mask of Time. Anguished, Vakama discovers his mask power, invisibility, which he proceeds to use against Makuta. With a single shot from his Kanoka disk launcher, Vakama knocks the Mask of Time out of Makutas hands and into the sea. The enraged Makuta attempts to kill Vakama, but is defeated when he accidentally grabs a humongous boulder, causing him to be embedded into the Great Barrier. Vakamas fellow Toa join him, and they combine their powers to seal Makuta in a protodermis prison. Vakama glances back towards Metru Nui one last time, and then continues with the other Toa towards the light, arriving on the island of Mata Nui.

Just as Lhikan sacrificed his power to create the Toa Metru, Vakama and his friends now do the same to awaken the Matoran, a process that transforms them into Turaga. One of the Matoran, Takua, leads Vakama to another Matoran, Jaller, whose mask has been damaged. Vakama gives him Lhikans mask, and the other Matoran cheer as the symbol of the Three Virtues (Unity, Duty, and Destiny) materializes in the skies above.

==Cast==
* Alessandro Juliani as Toa Vakama, the Toa of Fire, a former mask-maker who sees prophetic visions of the future.
* Christopher Gaze as Turaga Vakama (narrator)
* Gerard Plunkett as Turaga Dume, the leader of Metru Nui.
* Tabitha St. Germain as Toa Nokama, Toa of Water, a former teacher who strives to do her duty. Michael Dobson as Toa/Turaga Lhikan, a Toa of Fire and former guardian of Metru Nui & Krekka, a powerful, but unintelligent Dark Hunter.
* Brian Drummond as Toa Matau, Toa of Air, a lighthearted being & Toa Onewa, Toa of Stone and a former carver, headstrong and independent. Paul Dobson as Toa Whenua, the Toa of Earth with respect for the past & Nidhiki, a former Toa and now Dark Hunter.
* Trevor Devall as Toa Nuju, the Toa of Ice with respect for the future.
* Lee Tockar as Makuta Teridax, (only known by "Makuta" in the film), the main antagonist & Kongu

==Production==
Plans were in place before the release of the first Bionicle movie to create a second movie.  The directors Terry Shakespeare and David Molina did have some input into the storyline, though most of the mythology had already been sketched out.    

For the visual style of the film, director David Molina stated “We wanted to give this audience a bigger view of the BIONICLE world – more environments, larger vistas.” Also, “The island of BIONICLE 2 is something like Manhattan, with lots of commerce and large buildings. The first film was very intimate, very organic. Metru Nui is more mechanical, so it has a different feel.”   

Talking about the camera work, director Terry Shakespeare said "We really concentrated on depth of field with the camera," Comparing the two Bionicles, he felt "The first film had primary colors that were coded to the areas and a younger feel. For BIONICLE 2, we opened it up – the palette had to be more sophisticated, more realistic with earth tones, so we desaturated the characters." 

Most of the animation was created in Taiwan by a company called CGCG.  The process of creating the movie, from storyboarding to delivery of the film took 12 months.  Molina additionally added that the pipeline and process for creating this film was faster and more refined than the original Bionicle movie. “Our strength is bringing characters to life and not just robots,” added Shakespeare. 

==Reception==
 .  The film was first screened on October 6, 2004, at the El Capitan Theatre in Hollywood, California.  Cartoon Network aired the movie for the first time less than two months after its release on December 18, 2004, at 7 p.m. Eastern Time. 

Though reviewers were still skeptical as to the toy promotional nature of these films, several noted their marked improvement over the original Bionicle movie, including its filling in of major plot holes that had been present in the first film. It was also noted for its references to   was released in 2005. 

Bionicle 2 was nominated at the DVD Exclusive Awards for Best Animated Premiere Movie.  It was also nominated for best director and best original score.  Bionicle was nominated at the 32nd Annual Saturn Awards for Best DVD release.  It was also an iParenting Media Award winner for Best Home Video/DVD.  Two awards were won by the studio that created Bionicle 2 at the 27th Annual Telly Awards.  It also won the Golden Reel Award for Sound Editing in a Direct to Video Release. 

===DVD release===
Bionicle 2: Legends of Metru Nui was released on DVD on October 6, 2004 in the United States. The DVD included a number of documentaries including the making of the movie and associated toy line.  There is also a featurette entitled "The Legend Revealed" that has a brief question and answer session with the production team.  Some critics were concerned that the DVD makes too much of an attempt to sell the Bionicle product. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 