The Santa Clause
 
{{Infobox film
| name           = The Santa Clause
| image          = The Santa Clause.jpg
| caption        = Theatrical release poster
| director       = John Pasquin
| producer       = Robert Newmyer Brian Reilly Jeffrey Silver Leo Benvenuti Steve Rudnick Karey Kirkpatrick
| starring       = Tim Allen Judge Reinhold Eric Lloyd Wendy Crewson David Krumholtz Peter Boyle
| music          = Michael Convertino
| cinematography = Walt Lloyd
| editing        = Larry Bock
| studio         = Walt Disney Pictures Hollywood Pictures Outlaw Productions Buena Vista Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $22 million   
| gross          = United States dollar|$189,833,357 
}}
 fantasy family family comedy The Santa Clause trilogy and it stars Tim Allen as Scott Calvin, an ordinary man who accidentally causes Santa Claus to fall from his roof on Christmas Eve. When he and his young son, Charlie, finish St. Nicks trip and deliveries, they go to the North Pole where Scott learns that he must become the new Santa and convince those he loves that he is indeed Father Christmas.
 Home Improvement. Last Man Standing.

The film was followed by two sequels,   (2002) and   (2006). In comparison to the original, the former received mixed critical response whilst the latter was panned by most critics.

==Plot==
Scott Calvin is a divorced 38-year-old advertising executive, who is also a father to his son Charlie. Charlies mother, Laura, is now married to psychiatrist Neal Miller. After Scott reads "A Visit from St. Nicholas" to Charlie on Christmas Eve, he and Charlie are awakened that night by sounds on the roof. After confronting a man on the roof, who inadvertently falls off when Scott startles him, then vanishes leaving his Santa Claus outfit behind, they discover eight reindeer on the roof and Charlie convinces Scott to put on the suit and finish Santas work for him. As the morning comes, the reindeer return to the North Pole to Santas Workshop, where the head elf Bernard explains that, due to a clausical contract written on a card Scott found on Santa, in putting on the suit and entering the sleigh he has accepted the "Santa Clause" and has agreed to the responsibilities of that position. He tells a sceptical Scott that he has eleven months to get his affairs in order before reporting to the workshop at Thanksgiving permanently.
 Mystery Date for Christmas, while Neal, at three years old stopped believing when Santa didnt give him an Oscar Mayer Weenie Whistle he wanted. On Thanksgiving night, Scott arrives to visit Charlie, but when Laura steps out of the room for a moment, Bernard comes and takes them away to the North Pole, leading her to believe Scott had kidnapped him.

On Christmas Eve night, Scott begins delivering presents, and is arrested when entering one of his houses, leaving Charlie stranded in the sleigh on the roof. The E.L.F.S. (Effective Liberating Flight Squad) is called and rescues Charlie and frees Scott from custody. Scott returns to take Charlie home, and manages to convince Laura and Neal of his new identity by giving them the gifts they asked for as children. Laura destroys the court order against Scott and tells him that he can visit Charlie anytime he wants. After a very public departure, Charlie attempts to use a snowglobe that Bernard had given him to summon Scott to him and Scott eventually arrives. After getting Lauras permission for a sleigh ride with his father, Charlie and he head out to continue Santas deliveries.

==Cast==
* Tim Allen as Scott Calvin: The main protagonist. He is a 38 year old divorced businessman and the father of Charlie. After a rooftop incident, he begins to gain weight, grow a beard and his hair whitens. He then later becomes Santa.
* Eric Lloyd as Charlie Calvin: Scotts innocent eight-year-old son. Following an incident that leads to him and Scott on a journey for Scott to do the rest of Santas work, Charlie believes his father is Santa Claus.
* Judge Reinhold as Dr. Neil Miller: Lauras new husband. A gentle, well-meaning, yet critical psychiatrist who thinks Scott is delusional. He dislikes Scott very much during a majority of the year. Scott has a dislike towards him back as well even constantly poking fun at and mocking his reputation. Later, Scott starts respecting him more during his transformation, and Neil earns respect towards Scott near the end when the proof of Santas existence and him being the new Santa comes to be.
* Wendy Crewson as Laura Miller: Scotts ex-wife who is torn between her new husbands opinions and believing that Scott is Santa, and Charlies well-being.
* David Krumholtz as Bernard the Elf: A sarcastic, teenage elf as well as Santas head elf. He has trouble convincing Scott to face the reality.
* Peter Boyle as Mr. Whittle: Scotts supervisor at the Toy Company.
* Kenny Vadas as the E.L.F.S. Leader

==Production==
The film was mostly shot in Oakville, Ontario|Oakville, a town in the Greater Toronto Area, which also served as the city of Chicago, Illinois in it.  The reindeer used in the film were all from the Toronto Zoo. The trains used in the North Pole scene and the start of the film are all LGB (trains)|LGB.

==Reception==

===Box office=== USD $144 million in the United States and Canada, and over $189 million worldwide, making it a box-office hit. The film, now twenty years old has become a Christmas classic. ABC Family and AMC plays the movie numerous times through the holiday season with record ratings.   

===Critical reception===
The film was generally well received by film critic|critics, and maintains a "fresh" rating of 75% on Rotten Tomatoes, with 31 positive reviews from 39 counted and an average rating of 6.1/10.    The consensus from the site is "The Santa Clause is utterly undemanding, but its firmly rooted in the sort of good old-fashioned holiday spirit missing from too many modern yuletide films." 

==Soundtrack==

Note that songs listed here (and in the movie credits) cannot always be found on CD soundtracks. 

*"Oh Christmas Tree (O Tannenbaum)"; Arranged by John Neufeld
*"Carol of the Bells"; Written by Peter Wilhousky White Christmas"; Written by Irving Berlin; Performed by The Drifters
*"Santa Claus Is Comin to Town"; Written by J. Fred Coots And Haven Gillespie; Performed by Alvin and the Chipmunks
*"Think!" (Theme from Jeopardy!); Written by Merv Griffin
*"Jingle Bell Ride"; Written and Performed by Johnny Hawksworth Gimme All Frank Beard; Performed by ZZ Top
*"Jingle Bells"; Arranged by John Neufeld
*"Christmas Will Return"; Written by Jimmy Webb; Performed by Brenda Russell and Howard Hewett
*"The Bells of Christmas"; Written and Performed by Loreena McKennitt

The films soundtrack was released on October 10, 1994 in the United States.

# Lets Go
# Believing Is Seeing
# Sash Completes the Ensemble
# Flight
# Weightless
# Away to the Window
# Bells of Christmas
# Listen
# Goodnight, Goodnight, Don’t Forget the Fire Extinguisher
# Visitation
# Rose Suchak Ladder
# List
# Elves with Attitude
# Someone in Wrapping
# Near Capture
# Comfort and Joy
# Not Over Any Oceans
# Christmas Will Return

==Home media==
This film was first released on Home Video (VHS and Laserdisc) on October 20, 1995. The first DVD was released on October 29, 2002. The Santa Clause along with   and   were released in a three movie DVD collection in 2007. All three movies were released as a Blu-ray set on August 29, 2013.

Towards the beginning of the film a brief exchange between Scott and Laura takes place in which Laura hands Scott a piece of paper with Neals mothers phone number on it. Scott then exclaims "1-800-SPANK-ME? I know that number!". In the United States, the exchange was removed from the 1999 DVD release as well as the 2002 Special Edition DVD and VHS releases after a 1996 incident in which a child from Steilacoom, Washington called the number and racked up a $400 phone bill.  On television airings, the phone number is changed to "1-800-POUND". The line remains intact on the 1995 VHS release.

==Sequels==
The popularity of the film produced two sequels,   in 2002 and   in 2006. The main cast were all present in the sequel. Bernard was absent during The Escape Clause, due to other commitments. There are no plans to continue the series past a trilogy.

==References==
 

==External links==
 
*  
*  
*  
*   at Ultimate Disney

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 