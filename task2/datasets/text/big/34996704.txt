Only God Forgives
 
{{Infobox film
| name           = Only God Forgives
| image          = Only God Forgives poster.jpg
| image_size     = 
| alt            = 
| caption        = Teaser poster
| director       = Nicolas Winding Refn
| producer       = Lene Børglum
| writer         = Nicolas Winding Refn
| starring       = {{Plainlist |
* Ryan Gosling
* Kristin Scott Thomas
* Vithaya Pansringarm
* Ratha Phongam
* Gordon Brown Tom Burke
}}
| music          = Cliff Martinez
| cinematography = Larry Smith
| editing        = Matthew Newman
| studio         = {{Plainlist |
* Space Rocket Nation
*Gaumont Film Company
*Bold Films Wild Bunch 
}}
| distributor    = {{Plainlist |
* The Weinstein Company|Radius-TWC     
* Lions Gate Entertainment  
* Le Pacte
* Warner Bros.  
}}
| released       =  
| runtime        = 90 minutes  
| country        = {{Plainlist |
* Denmark
* France   
}}
| language       = English Thai
| budget         = $4.8 million   
| gross          =  $10.3 million 
}}
Only God Forgives is a 2013 Danish-French thriller film written and directed by Nicolas Winding Refn, starring Ryan Gosling, Kristin Scott Thomas, and Vithaya Pansringarm.   The film was shot on location in Bangkok, Thailand,    and, as with Drive (2011 film)|Drive,    is dedicated to Chilean director Alejandro Jodorowsky.    It competed for the Palme dOr at the 2013 Cannes Film Festival.   

==Plot==
Julian (Ryan Gosling) is an American expatriate who runs a muay thai club in Bangkok, Thailand, that is a front for a drug smuggling operation. His older brother Billy rapes and kills an under-age sex worker and is caught by the Thai police. Lieutenant Chang allows the girls father, Choi Yan Lee, to beat Billy, but Choi goes too far and ends up killing him. However, Chang does not care about Billys death; instead, Chang cuts off the fathers forearm for allowing his daughter to be a sex worker.

Julian also has his sexual hang-ups preferring to be tied up while he watches his favorite prostitute, Mai, touch herself, all filmed as if in a ballet.

Julian and his crew go to Chois kiosk to confront him about his part in Billys murder but spares his life after hearing the story. Julians mother, Crystal, arrives in Bangkok to identify Billys corpse. Crystal demands Julian find and kill the men who killed Billy, but he refuses—believing that Choi had some justification for seeking retribution for the killing of his daughter—infuriating her. Julian has several visions of meeting Chang in a dark room, Chang cutting Julians hands off.

Julian brings Mai to meet Crystal, posing as his girlfriend. Crystal sees through the ruse and hurls insults at Mai and demeans Julian, pronouncing him sexually inferior to his dead brother. Julian humbly accepts all of Crystals abuse, but afterward turns on Mai, viciously humiliating her, then regretting it.

One of the fighters at Julians boxing club assassinates Choi at the behest of Gordon, a member of Julians crew, at Crystals request. Later, the police investigating Chois murder arrive at Julians club, but Chang concludes that Julian is not Chois killer. Julian recognizes Chang from his visions and follows him from the boxing club, but Chang seems to disappear into thin air.

After learning that Chang was involved in Billys death, Crystal meets with an associate, Byron, to arrange Changs assassination. Three gunmen on motorbike are sent to kill Chang at a restaurant with machine guns, and two of Changs men are killed in the shoot-out. Chang kills two of the gunmen and follows the third on foot, beating him with a frying pan. He leads him to his boss Li Po who is feeding his young crippled son. Chang then kills the third gunman, but spares Li Po after seeing him show affection for his son. Li Po points Chang to Byron, who ordered the hit. Chang finds Byron in a club and tortures him to get answers. Byron reveals the reasoning behind the hit, but refuses to give a name. Chang continues to torture Byron, cutting out his eyes and inserting a spike into his ear.

Julian confronts Chang and challenges him to a fight and they fight on the bare concrete floor of Julians boxing venue. Chang, an experienced boxer, easily and thoroughly beats Julian, who lands not a single hit in the entire match. Afterwards, Crystal tells Julian that Chang has figured out she ordered the hit on him. Fearfully, she pleads with Julian to kill Chang to protect her, the same way she asked Julian to kill his own father for her. She promises that after Julian kills Chang, they will go back home and she will be a true mother to him and take care of him. Julian agrees.

Julian shoots the guard outside Changs home, and his associate Charlie Ling enter Changs house, intent on ambushing him when he returns. Charlie informs Julian that he was instructed to execute Changs entire family. Charlie murders the nanny of Changs daughter as she enters the home, but, in an act of redemption, Julian shoots Charlie before he can kill Changs young daughter.

Chang and a police officer visit Crystal. She blames everything on Julian. Chang ends up cutting her throat.

Julian returns to the hotel and finds his mothers corpse. In silence, he approaches her body and cuts open her abdomen. Julian slowly places his hand inside of the wound. After leaving and having several surreal visions, Julian is shown standing in a field with Chang, who appears to cut off both of Julians hands with his sword. The final scene returns to Chang singing at a karaoke bar with an audience of attentive police officers, watching him in silence.

==Cast==
*  .    Gosling had undertaken Muay Thai training in preparation for the role by that September,    which included 2–3 hour daily sessions.    Refn also participated in the training.  Gosling and Refn had recently worked together on the neo-noir crime drama Drive (2011 film)|Drive (2011). Julian only speaks 17 lines throughout the film.  "Goslings character sticking his hands into her   corpses dead womb... a machination that came from Gosling himself." 
* Kristin Scott Thomas as Crystal, Julians mother, who is described as "a merciless and terrifying mafia godmother" combining elements of Lady Macbeth and Donatella Versace.   Scott Thomas was cast by May 2011.  One Eye Drive then went into the Thai police lieutenant. Theyre the same character played by three different actors   a mythological creature that has a mysterious past but cannot relate to reality because hes heightened and hes pure fetish."  Gordon Brown Valhalla Rising.
* Rhatha Phongam (Yaya-Ying)    as Mai, a prostitute associated with Julian Tom Burke  as Billy, Julians older brother
* Byron Gibson as Byron
* Danai Thiengdham as Li Po
* Sahajak Boonthanakit as Pol Col. Kim
* Nophand Boonyai as Charlie
* Teerawat Mulvilai as Ko Sam
* Kovit Wattanakul as Choi Yan Lee
* Wittchuta Watjanarat as Ma Fong

==Production== Valhalla Rising (2009), but he accepted Goslings request to direct Drive (2011 film)|Drive instead.    Gosling has described the script of Only God Forgives as "the strangest thing Ive ever read and its only going to get stranger."  Like Drive, Only God Forgives was largely shot chronologically and scenes were often edited the day they were shot. 

Footage was screened at the 2013 Cannes Film Festival.  Refn drew a connection between Only God Forgives and Drive, saying that "  is very much a continuation of that language"—" ts based on real emotions, but set in a heightened reality. Its a fairy tale." 

==Reception==
The film received a very divided response at its Cannes press screening; it was booed by many of the audience of journalists and critics while also receiving a standing ovation.      
It received a primarily negative response from mainstream critics: review aggregator Rotten Tomatoes gives the film a score of 40% based on reviews from 150 critics, with a weighted average of 5.1/10 and the sites consensus states: "Director Refn remains as visually stylish as ever, but Only God Forgives fails to add enough narrative smarts or relatable characters to ground its beautifully filmed depravity." 
Metacritic assigns the film a weighted average rating of 37 out of 100 based on the reviews of thirty-nine professional critics. 

Robbie Collin of The Daily Telegraph reflected concerns over the film in a three out of five star review. "The films characters are non-people; the things they say to each other are non-conversations, the events they enact are non-drama," he wrote. But he praised Refn for following up his commercially successful film Drive with "...this abstruse, neon-dunked nightmare that spits in the face of coherence and flicks at the earlobes of good taste". 

Peter Bradshaw of The Guardian gave it five out of five stars, calling it gripping and praising the "pure formal brilliance" of every scene and frame, though he notes that it will "have people running for the exits, and running for the hills" with its extreme violence.  In an alternative review published in The Guardian, John Patterson was highly critical of the film, citing its lack of originality and the low degree of focus on plot: "Somewhere in here is a story that Refn can hardly be bothered to tell... I feel the ghosts of other movies—his influences, his inspirations—crowding in on his own work, suffocating him, and somehow leaving less of him on screen." 

Bill Gibron of PopMatters wrote "David Lynch must be laughing. If he had created something like Only God Forgives, substituting his own quirky casting for the rather staid choices made by actual director Nicolas Winding Refn, he would have walked away from Cannes 2013 with yet another Palme dOr, another notch in his already sizeable artistic belt, and the kind of critical appreciation that only comes when a proven auteur once again establishes his creative credentials." 

Richard Roeper of the Chicago Sun-Times gave this film a positive review, giving it three and a half stars saying: "Refns follow-up effort to the similarly polarizing Drive (which I thought was flat-out great) is even more stylized and daring. Drive star Ryan Gosling (who is clearly interested in carving out a career with at least as many bold, indie-type roles as commercial, leading-man fare) strikes a Brando pose playing Julian, a smoldering, seemingly lethal American who navigates the seediest sides of Bangkok." 

In 2015, the film was included in The Guardian s top 50 films of the decade so far. 

===Awards===
 
The film won the Grand Prize at the Sydney Film Festival. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 