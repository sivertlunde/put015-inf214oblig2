Louisiana (film)
{{Infobox film
| name           = Louisiana
| image          = Louisiana (1919) - Beery & Martin.jpg
| alt            = 
| caption        = Still with Noah Beery and Vivian Martin
| director       = Robert G. Vignola
| producer       = Jesse L. Lasky
| screenplay     = Frances Hodgson Burnett Alice Eyton  Robert Ellis Noah Beery, Sr. Arthur Allardt Lillian West Lillian Leighton
| music          =  	
| cinematography = Frank E. Garbutt 
| editor         =	
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Comedy comedy film Robert Ellis, Noah Beery, Sr., Arthur Allardt, Lillian West and Lillian Leighton. The film was released on July 20, 1919, by Paramount Pictures.  

==Plot==
 

==Cast==
*Vivian Martin as Louisiana Rogers Robert Ellis as Laurence Ferol
*Noah Beery, Sr. as Lem Rogers
*Arthur Allardt as Cass Floyd
*Lillian West as Olivia Ferol
*Lillian Leighton as Aunt Cassandry 

== References ==
 

== External links ==
 
*  

 
 
 
 
 
 
 
 
 