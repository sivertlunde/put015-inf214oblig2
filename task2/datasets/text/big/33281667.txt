Summer Games (2011 film)
 
{{Infobox film
| name           = Summer Games
| image          = Summer Games (film).jpg
| caption        = Film poster
| director       = Rolando Colla
| producer       = Elena Pedrazzoli
| writer         = Rolando Colla Roberto Scarpetti
| starring       = Fiorella Campanella
| music          = 
| cinematography = Lorenz Merz
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Switzerland
| language       = Italian
| budget         = 
}}
 Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

==Cast==
* Fiorella Campanella as Marie
* Armando Condolucci as Nic
* Alessia Barela as Adriana
* Antonio Merone as Vincenzo
* Roberta Fossile as Maries mother
* Marco DOrazi as Agostino
* Aaron Hitz as Moritz
* Monica Cervini as Paola
* Francesco Huang as Lee
* Chiara Scolari as Patty

==Filming==
Filming started in Marina di Grosseto, Tuscany on 21 June 2010. Filming continued for seven weeks through the towns of Follonica, Gavorrano, Castiglione della Pescaia, Orbetello, Porto Santo Stefano and Monte Argentario in the Maremma.      

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Swiss submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 