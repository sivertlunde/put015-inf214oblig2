Three Brothers (1981 film)
{{Infobox film
| name           = Three Brothers
| image          = Three Brothers (1981 film).jpg
| caption        = Film poster
| writer         = Tonino Guerra Francesco Rosi
| starring       = Philippe Noiret Michele Placido
| director       = Francesco Rosi
| producer       = Antonio Macri Giorgio Nocella
| music          = Pino Daniele Piero Piccioni
| cinematography = Pasqualino De Santis
| editing        = Ruggero Mastroianni
| distributor    =
| released       =  
| runtime        = 113 minutes
| country        = Italy France
| language       = Italian
| budget         =
}}
Three Brothers ( ) is a 1981 Italian film based on a work by Andrei Platonov. It was directed by Francesco Rosi and stars Philippe Noiret, Vittorio Mezzogiorno, Michele Placido and Charles Vanel.
 Boston Society of Film Critics award for Best Foreign Film, and the Nastro dArgento for Best Director and Nastro dArgento Best Actor|Actor. It received a nomination for an Academy Award for Best Foreign Language Film.    It was screened out of competition at the 1981 Cannes Film Festival.   

==Plot==
 
In a farmhouse in southern Italy, an old woman, the matriarch of an Italian family, dies. Her husband summons their three sons, each of whom are facing difficult personal problems, back to their farmhouse. One of their sons, Raffaele, is a judge living in Rome, who is presiding over a terrorism case for which he risks assassination, and is in fear of his life. Another son, Rocco, who lives in Naples, is religious and works as a counselor at a correctional institute for boys, so that he can fulfill his dream of helping troubled teenagers. The third son, Nicola, who lives in Turin, is a factory worker involved in a labour dispute as well as a failed marriage. Each of the men grieves in his own way, while also wrestling with the other emotional issues that are pressing on them.

The sons encounter the past and engage in reveries of what may come: Raffaele imagines his death, Rocco dreams of lifting the youth of Naples out of violence, drugs, and corruption, Nicola pictures embracing his estranged wife. Meanwhile, the old man and his young granddaughter explore the rhythms of the farm and grieve together.

==Cast==
* Philippe Noiret - Raffaele Giuranna
* Michele Placido - Nicola Giuranna
* Vittorio Mezzogiorno - Rocco Giuranna / Young Donato
* Andréa Ferréol - Raffaeles Wife
* Maddalena Crippa - Giovanna
* Rosaria Tafuri - Rosaria
* Marta Zoffoli - Marta
* Tino Schirinzi - Raffaeless Friend
* Simonetta Stefanelli - Young Donatos Wife
* Pietro Biondi - 1st Judge
* Charles Vanel - Donato Giuranna
* Accursio Di Leo - 1st Friend at Bar
* Luigi Infantino - 2nd Friend at Bar
* Girolamo Marzano - Nicolas Friend
* Gina Pontrelli - The Brothers Mother

==See also==
* List of submissions to the 54th Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 