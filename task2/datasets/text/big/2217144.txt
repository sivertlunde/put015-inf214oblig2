Princess Caraboo (film)
{{Infobox film
| name = Princess Caraboo
| image = Carbooposter.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Michael Austin
| producer = Andrew Karsch Simon Bosanquet Armyan Bernstein Tom Rosenberg Marc Abraham John Wells
| starring = Phoebe Cates Jim Broadbent Wendy Hughes Kevin Kline John Lithgow Stephen Rea
| music = Richard Hartley
| cinematography = Freddie Francis
| editing = George Akers
| studio = Beacon Communications
| distributor = TriStar Pictures
| released =  
| runtime = 97 minutes
| country = United Kingdom United States
| language = English
| budget = 
| gross = $3,062,530 
}} historical Comedy-drama|comedy-drama John Wells) and directed by Michael Austin, based on the real-life 19th-century character Princess Caraboo, who passed herself off in British society as an exotic princess who spoke a strange foreign language; she is portrayed by Phoebe Cates.

==Plot==
In Regency England, an exotically dressed woman is found in the fields, speaking a language no one can understand. She ends up at the home of the Worrall family, the local gentry.  Their Greek butler, Frixos thinks the woman is a fraud from the start. Mr. Worrall sends her to the magistrate to be tried for vagrancy, but Mrs. Worrall agrees to care for her.  Mr. Gutch, a local printer and newspaper reporter, takes an interest in the case especially after the woman claims via pantomime to be  Princess Caraboo. 

Gutch talks to the farm workers who found her and learns she had a book from the Magdalene Hospital in London on her. When the Worralls leave on a trip the servants inspect her for a tattoo, which they believe all natives of the South Seas have and are shocked to find Princess Caraboo has one on her thigh. Frixos tells Gutch he now thinks she’s a genuine princess. Mr. Worrall uses her presence to recruit investors for the spice trade which will be facilitated by Princess Caraboo when she returns to her native land. Gutch brings in Professor Wilkinson, a linguist who is initially dismissive of Caraboo’s story, but has enough doubt to refuse to say she is a fraud. 

The local society finds Princess Caraboo fascinating and they flock to attend parties and soirees with her. Mr. Gutch begins investigating people connected with the Magdalene House.  Lady Apthorpe takes Caraboo to a ball held for the Prince Regent, who is fascinated by Caraboo. Gutch learns Caraboo is actually Mary Baker, who worked as a servant for Mrs. Peake. Gutch sneaks into the ball to warn her she’s been found out, but she refuses to acknowledge what he tells her. Mrs. Peake comes and confronts Caraboo and identifies her as Mary Baker. She is locked up. The local magistrate and Mr. Worrall want to hang her. Mrs. Worrall gives Mr. Gutch documents implicating her husband and the magistrate in a bank fraud. Mr. Gutch uses  these to work a trade. He will bury the story if Mary Baker can go to America. Gutch, who has fallen in love with Mary leaves with her for the United States.

==Cast==
* Phoebe Cates as Princess Caraboo/Mary Baker
* Jim Broadbent as Mr. Worrall
* Wendy Hughes as Mrs. Worrall
* Kevin Kline as Frixos
* John Lithgow as Professor Wilkinson
* Stephen Rea as Gutch
* Peter Eyre as Lord Apthorpe
* Jacqueline Pearce as Lady Apthorpe John Wells as Reverend Wells John Lynch as Amon McCarthy
* John Sessions as Prince Regent
* Arkie Whiteley as Betty
* Jerry Hall as Lady Motley
* Anna Chancellor as Mrs. Peake

==Reception==
Princes Caraboo received mixed to positive reviews with a 60% "fresh" rating on Rotten Tomatoes. 
This was Cates final major film appearance before she retired from acting (after meeting and eventually marrying her co-star, Kevin Kline); she subsequently made an additional appearance in a 2001 film.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 