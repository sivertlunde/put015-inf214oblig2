How Did a Nice Girl Like You Get Into This Business?
 
{{Infobox film
| name           = How Did a Nice Girl Like You Get Into This Business?
| image	         = How Did a Nice Girl Like You Get Into This Business? FilmPoster.jpeg
| caption        = A poster bearing the films alternative title: The Naughty Cheerleader
| director       = Will Tremper
| producer       = Horst Wendlandt
| screenplay     = Will Tremper
| based on       =  
| starring       = Barbi Benton
| music          = 
| cinematography = Richard C. Glouner Karl Löb
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

How Did a Nice Girl Like You Get Into This Business? (Original German title: Mir hat es immer Spaß gemacht (also: Wie kommt ein so reizendes Mädchen zu diesem Gewerbe?) and also known as The Naughty Cheerleader) is a 1970 West German comedy film directed by Will Tremper and starring Barbi Benton.     

It is based on the novel of the same title, written by Lynn Keefe.

==Cast==
* Barbi Benton – Lynn Keefe (as Barbara Benton)
* Hampton Fancher – Gino Jeff Cooper – Bob Greene
* Broderick Crawford – B.J Hankins
* Marc De Vries – Ronnie
* Claude Farell – Mrs. Epstein
* Hugh Hefner – Himself
* Klaus Kinski – Juan José Ignatio Rodriguez de Calderon, Sam
* Lisa Lesco
* Bruce Low – Ist Smirna brother Paul Muller – The Director
* Roman Murray – Frank
* Max Nosseck – 2nd Smirna brother
* Massimo Serato – Capitano
* Lionel Stander – The Admiral
* Clyde Ventura – Nick
* José Luis de Vilallonga – Mr. Epstein
* Horst Wendlandt – Reporter with cigar behind the hotel in Miami Beach (uncredited)
* Christian Anders (minor role)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 