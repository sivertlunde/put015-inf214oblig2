The Boy Who Had Everything
 
 
{{Infobox film
| name           = The Boy Who Had Everything
| image          = The Boy Who Had Everything.jpg
| image_size     = 
| alt            = 
| caption        = VHS cover as Winner Takes All
| director       = Stephen Wallace
| producer       = Richard Mason Julia Overton
| writer         = Stephen Wallace
| narrator       = 
| starring       = Jason Connery Diane Cilento Laura Williams
| music          = Ralph Schneider
| cinematography = Geoff Burton
| editing        = Henry Dangar
| studio         = Alfred Road Films Multi Films Investments Limited
| distributor    = Hoyts-Edgley
| released       =  
| runtime        = 94 minutes
| country        = Australia
| language       = English
| budget         = A$1.1 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p356-357 
| gross          =
}}
 Australian film written and directed by Stephen Wallace.   Its video title was Winner Takes All.  It won the award for best screenplay at the Australian Film Institute.   Jason Connery and Diane Cilento, who play mother and son in the film, are real-life mother and son.  The film was also entered into the 14th Moscow International Film Festival.   

==Plot==
Johnny Kirkland (Jason Connery|Connery) is a university student having problems with his alcoholic and emotionally damaged mother (Diane Cilento|Cilento), who was recently divorced. At school, his fraternity brothers find out that Johnny excelled in his high school academically and in sports, and try to prevent him from achieving the same level of success.  Johnny also has problems with his girlfriend Robin (Williams).

==Principal cast==
{| class="wikitable" style="width:50%;"
|- 
! Actor !! Role
|-
| Jason Connery || John Kirkland
|-
| Diane Cilento|| Mother
|-
| Laura Williams || Robin
|-
| Lewis Fitz-Gerald  || Vandervelt
|- Ian Gilmour || Pollack
|-
| Nique Needles || Graham Cummerford
|-
| Monroe Reimers || Andrew Afferson
|}

==Production==
The film was autobiographical for Wallace. The original draft started when the lead character was 28 but this was changed to begin when he was at university because it was felt this would make the movie more commercial.   accessed 21 November 2012  It was also set further back in the past, as the suggestion of script editor Sandra Levy. Producer Richard Mason came up with the idea it should be set in the 1960s. Paul Kalina, "Stephen Wallace", Cinema Papers, Feb-March 1985 p11-15 

The movie was meant to be the first of several made by a new company, Multi Films, but they ended up making only this. 

Wallace says he was unhappy with the casting:
 
Diane Cilento tried to play working-class, but it just didnt work. Robyn Nevin wanted to play that part, and I think she should have. Jason Connery was very young then and he was very nervous about the film and thought it would ruin his image, and he was never very friendly to me. He did the film, but he thought I was ruining his career. 
 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 