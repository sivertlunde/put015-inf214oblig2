Redneck Zombies
{{Infobox film
| name           = Redneck Zombies
| image          = redneckzombiesfilm.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Pericles Lewnes
| producer       = Pericles Lewnes George Scott
| writer         = Fester Smellman Story by Zoofeet P.Floyd Piranha
| starring       = Lisa de Haven W.E. Benson William W. Decker James Housley Austin "Amac" McLaurine Austin "Sexy Man" Brewer
| music          = Adrian Bond
| cinematography = Ken Davis
| editing        = Edward Bishop
| distributor    = Troma Entertainment
| released       =  
| runtime        = 84 minutes 90 minutes   96 minutes  
| country        = United States
| language       = English
| budget         = $10,000 (estimated)
| gross          =
}}
 1987 very low budget independent horror comedy trash film directed by Pericles Lewnes and released by Troma Entertainment.   

==Production==
The movie is notable for being one of the first films shot entirely on videotape and then released to the homevideo market (making it a straight-to-video film). The title of the film was once used as an answer to a question in an 1980s edition of Trivial Pursuit, the question being "What film has the tagline Theyre Tobacco Chewin, Gut Chompin, Cannibal Kinfolk from Hell!". Director Pericles Lewnes went on to work as a special effects supervisor on several other Troma productions, including The Toxic Avenger Part II, The Toxic Avenger Part III, and Tromas War, as well as Shatter Dead.

==Release==
The film was released on VHS and DVD by Troma Entertainment.  A new 20th anniversary edition of the film was released on DVD January 27, 2009. It includes the never before released soundtrack on CD. 

==References==
 

== External links ==
* 
*  
* 
* 
* 

 
 
 
 
 
 


 