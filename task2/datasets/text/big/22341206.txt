Wafa: A Deadly Love Story
 
{{Infobox film
| name = Wafaa: A Deadly Love Story
| image = 
| image_size = 
| caption = 
| director = Rakesh Sawant
| producer =
| writer = Salim Raza
| starring =Sahib Chopra Rajesh Khanna   Laila Khan
| music = Ravi Pawar Sayed Ahmed
| distributor = True Life Production
| released =  December 19, 2008
| language =  Hindi
}}

Wafaa: A Deadly Love Story is a 2008 Bollywood film starring Rajesh Khanna Laila Khan and Sahib Chopra.  This movie was written by Salim Raza and directed by Rakesh Sawant. It was released on December 19, 2008.

Wafaa is known to be a comeback film for Rajesh Khanna. The movie was not directed well, nor the story well portrayed on screen which made it a box office failure and received mostly negative reviews. 

==Cast==
* Rajesh Khanna - Amritlal Chopra
* Sudesh Berry
* Laila Khan - Beena
* Saahib
* Tinu Anand

==Music==
The music of the film is like the old movies of Bollywood. The songs were performed by prominent singers of the industry. Playback singers include Mika Singh, Udit Narayan and Kumar Sanu. The music is composed by Ravi Pawar and Sayed Ahmed, and the lyrics were penned by Sahb Ilhabadi. 
{| class="wikitable"
|-
!Song
!Singer(s)
|-
|Bhula Sako To Udit Narayan
|-
|Husn Hai Aftab Hasib Sabri
|-
|Muztarib Akurti Kakkar and Mika Singh
|-
|Raste Roshan hue Sunidhi Chauhan and Rahul Vaidya
|-
|Sargarmiyan Jaya Piyush
|-
|Tere Begheir Kumar Sanu and Pronali
|-
|Tuhi Shola Kailash Kher
|-
|Wafaa Kalpana
|}

==Reception==
The movie was a box office failure and got mostly negative reviews. 

==References==
 

==External links==
*  

 
 
 

 