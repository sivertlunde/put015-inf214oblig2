The Supernaturals (film)
{{Infobox film
| name            = The Supernaturals
| image           = The Supernaturals 1986.jpg
| caption         = The Supernaturals film poster
| director        = Armand Mastroianni
| producer        = {{plainlist|
* Michael S. Murphey
* Joel Soisson
}}
| writer          = {{plainlist|
* Michael S. Murphey
* Joel Soisson
}}
| starring        = {{plainlist|
* Maxwell Caulfield
* Nichelle Nichols
* Talia Balsam
* Bobby Di Cicco
* LeVar Burton
* Scott Jacoby
}}
| music           = Robert O. Ragland
| editing         = John R. Bowey
| distributor     = Embassy Pictures
| runtime         = 104 minutes
| released        =  
| country         = United States
| language        = English
| budget          =
}}
The Supernaturals is an 1986 American Zombie Horror film directed by Armand Mastroianni, and written by Michael S. Murphey and Joel Soisson. Filmed in the United States in 1985, The Supernaturals was released by Embassy Pictures in 1986, and has never been re-released since its original VHS Video rental format.

== Plot == Confederate town is held captive, the soldiers forced to walk through a piece of woodland laced with mines. Because he is found wearing a Confederate States Army outfit, Jeremy (Chad Sheets), a young boy is also forced to make the crossing. If they make it across the minefield the Union Army 44th will set them free. Only Jeremy and his mother (Melanie) - who runs to save him - survive, despite the fact that the mother has stepped on a mine. Jeremy exhibits unearthly powers. Jump to circa 1985 and a platoon of soldiers are out on manouevres in the same backwoods. They too are the 44th, descendants of the Union platoon of years before, those who committed the atrocities. The main character here is Pvt. Ellis (Maxwell Caulfield), the only soldier to catch glimpses of the beautiful and mysterious Melanie; Ellis also has a romantic interest in Pvt. Lejune (Talia Balsam). The platoon decide to camp inside a circle of yellow grass seemingly caused by brush fire, a spot from which the wind only blows outward. Pvt. Ellis is reluctant to camp there, but Sgt. Hawkins does not like having her authority undermined. Whilst the platoon are setting up camp, Melanie reappears - this time witnessed by all in the platoon - and takes a strong interest in Pvt. Ellis who bears an uncanny resemblance to her husband, Evan.

Trouble for Sgt. Hawkins (Nichelle Nichols) and her platoon begins with the soldiers drunken and childish antics; however, events take a sinister turn when Pvt. Cort (Bobby Di Cicco) is found shot in the head. They inexplicably lose radio contact and the nearest town is over twenty miles away. Hawkins and her platoon go in search of - and arrest - Melanie, who lives in an old wooden shack with what looks to be her father or grandfather, a withered old man. In the cabin they discover the relics of muskets and an old diary, written by Evan. They take Melanie back to camp, but leave the old man at the cabin; he is too old and frail to go anywhere. During the night, an eerie fog rolls in, encompassing the 44th, and the Confederate Dead close in on their camp. Most of the 44th are subsequently killed - one-by-one - whilst Pvt. Ellis heads off into the woods in search of the old man who, it turns out, is Jeremy (he has used his powers to keep his mother the same age). Along the way he is forced to do battle with the Confederate zombies and also runs into Melanie, who shows him a locket. Inside is a picture of Evan and Ellis immediately recognizes the resemblance. He snatches the locket from around her neck and runs in the direction of the cabin. Once there he persuades Jeremy to end the evil spell and send Melanie and the zombies back to wherever it is the dead are supposed go. Exhausted, Jeremy dies and Ellis returns to the camp where Lejune and Hawkins are waiting, the only survivors. Ellis embraces Lejune and the locket falls from his hand to the woodland floor, where it is lost amongst the leaves. 

== Cast ==
*Maxwell Caulfield as Pvt. Ray Ellis
*Nichelle Nichols  as Sgt. Leona Hawkins
*Talia Balsam as Pvt. Angela Lejune
*Bradford Bancroft as Pvt. Tom Weir
*LeVar Burton as Pvt. Michael Osgood
*Bobby Di Cicco as Pvt. Tim Cort
*Scott Jacoby as Pvt. Chris Mendez
*Richard Pachorek as Pvt. Ralph Sedgewick
*John Zarchen as Pvt. Julius Engel
*Margaret Shendal as Melanie
*James Kirkwood Jr. as the Captain
*Patrick Davis as the Old Man
*Robert V. Barron as the Old Vet
*Chad Sheets as Jeremy
* Maurice Gibb appears as a Union soldier in a cameo role.   

==Soundtrack==
Originally, the soundtrack to The Supernaturals was scored by Maurice Gibb of the Bee Gees. The score was rejected, though, in favour of one written by Robert O. Ragland. Gibb can be seen in The Supernaturals in a cameo role as a Union soldier. 

==Make-up and special effects==
Bart J. Mixon performed the   and  . Mixon worked with Ed Ferrell, Shannon Shea and   with a bullet hole in it": Di Cicco had just finished filming 1941 (film)|1941 and was then an up-and-coming actor. He also worked with Maxwell Caulfield (star of the 1982 musical film Grease 2), and with Star Trek regulars LeVar Burton and Nichelle Nichols. Mixon goes on to praise The Supernaturals for not following in the footsteps of George A. Romero, calling the film original. However, a limited and subsequently slashed budget meant that Prosthetic makeup gave way to head masks. "I think our work turned out very well," Mixon comments on the finished product, "and I wish it was shown off a little better in the final film". 

==Reception==
Best-Horror-Movies call The Supernaturals an "original horror film" and one which is "interesting".   Bruce Kooken of HorrorNews.Net wrote, "The Supernaturals just wasn’t a good movie is all. It was a tired concept, with a tired script and a lackluster implementation."   Writing in The Zombie Movie Encyclopedia, academic Peter Dendle said, "Though there are a few chilling scenes, ... the scripting is threadbare and the characters uncharismatic." 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 