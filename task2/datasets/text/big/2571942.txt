Surviving Picasso
{{Infobox film | name = Surviving Picasso
  | image   =Surviving Picasso film poster.jpg
  | caption =Theatrical release poster James Ivory
  | producer = Ismail Merchant David L. Wolper Humbert Balsan (co-producer) Donald Rosenfeld (executive) Paul Bradley (executive)
  | writer = Ruth Prawer Jhabvala
  | starring =Anthony Hopkins Natascha McElhone Julianne Moore Joss Ackland Peter Eyre Jane Lapotaire Joseph Maher Bob Peck Diane Venora Susannah Harker Joan Plowright Dominic West Richard Robbins
  | cinematography = Tony Pierce-Roberts
  | editing = Andrew Marcus
  | distributor = Warner Bros.
  | released = 20 September 1996
  | runtime = 125 min
  | language = English
  | budget = $16 million
  | gross = $2,021,348
  }}
Surviving Picasso is a  .

==Plot== Nazi occupation of the city, where Picasso is complaining that people broke into his house and stole his linen, rather than his paintings. It shows Françoise being beaten by her father after telling him she wants to be a painter, rather than a lawyer. Picasso is shown as often not caring about other peoples feelings, firing his driver after a long period of service, and as a womanizer, saying that he can sleep with whomever he wants.

The film is seen through the eyes of his lover Françoise Gilot (Natascha McElhone). As the producers were unable to get permission, to show the works of Picasso in the film, the film is more about Picassos personal life rather than his works, and where it does show paintings, they are not of his more famous works. When Picasso is shown painting Guernica (painting)|Guernica, the camera sits high above the painting, with the work only slightly visible.

The film depicts several of the women who were important in Picassos life, such as Olga Khokhlova (played by Jane Lapotaire), Dora Maar (played by Julianne Moore), Marie-Thérèse Walter (played by Susannah Harker), and Jacqueline Roque (played by Diane Venora).

==Cast==
* Anthony Hopkins as Pablo Picasso
* Natascha McElhone as Françoise Gilot 
* Julianne Moore as Dora Maar 
* Joss Ackland as Henri Matisse
* Dennis Boutsikaris as Kootz
* Peter Eyre as Sabartes 
* Peter Gerety as Marcel 
* Susannah Harker as Marie-Thérèse 
* Jane Lapotaire as Olga Picasso 
* Joseph Maher as Kahnweiler 
* Bob Peck as Françoises Father 
* Joan Plowright as Françoises Grandmother
* Diane Venora as Jacqueline 
* Dominic West as Paulo Picasso
* Laura Aikman as Maya

==Production==
It was shot in Paris and southern France.

==Reception==
The film received mixed to negative reviews, with a 33% rating on Rotten Tomatoes, based on 18 reviews. 

==References==
 

== External links ==
*  
*  
*  
*  
* John Walker. (2009)  . artdesigncafe.

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 