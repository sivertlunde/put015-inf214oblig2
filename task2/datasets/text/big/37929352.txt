A Window in Piccadilly
{{Infobox film
| name           = A Window in Piccadilly
| image          =
| caption        =
| director       = Sidney Morgan
| producer       = Sidney Morgan
| writer         =  Sidney Morgan
| starring       = Joan Morgan John F. Hamilton James Carew   Julie Suedo
| music          =
| cinematography = 
| editing        = 
| studio         = Sidney Morgan Productions
| distributor    = Woolf & Freedman Film Service
| released       = January 1928
| runtime        = 7,500 feet 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent romance independent production by Sidney Morgan. 

==Cast==
* Joan Morgan as The Girl  
* John F. Hamilton as Piccolo  
* James Carew as The Father  
* Julie Suedo as Sally  
* Maurice Braddell as Harry  
* Edmund Willard as The Fourth Party  
* DeGroot as The Professor

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.
* Wood, Linda. British Films 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 