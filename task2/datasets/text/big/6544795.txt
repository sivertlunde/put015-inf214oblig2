Nomad (2005 film)
 
{{Infobox film name = Nomad image = Nomad - The Warrior Poster.png caption = Theatrical release poster director = Sergei Bodrov Ivan Passer Talgat Temenov producer = Ram Bergman Pavel Douvidzon Rustam Ibragimbekov Miloš Forman writer = Rustam Ibragimbekov starring = Jay Hernandez Dilnaz Akhmadieva Kuno Becker Mark Dacascos Jason Scott Lee music = Carlo Siliotto cinematography = Dan Laustsen Ueli Steiger editing = Ivan Lebedev Rick Shaine distributor = The Weinstein Company released =   runtime = 112 minutes country = Kazakhstan Russia language = Russian Kazakh Kazakh
|budget = $40 million  gross = $3,088,685 
}} official entry for Best Foreign Language Film for the 79th Academy Awards.

==Plot==
 
 , Kazakhstan.]] Dzungar invaders.

==Cast==
* Jay Hernandez as Yearly 
* Dilnaz Akhmadieva as Hocha
* Kuno Becker as Mansur
* Azis Beyshinaliev as Ragbat
* Mark Dacascos as Sharish
* Archie Kao as Shangrek
* Jason Scott Lee as Oraz
* Ayan Yesmagambetova as Gaukhar
* Tungyshbay Jamanqulov as Abilqair
* Doskhan Joljaqsynov as Galdan Ceren
* Yerik Joljaqsynov as Barak

==Production==
 

==Release dates==
The Kazakh language version of Nomad premiered in Kazakhstan on 6 July 2005.

The film was released in the United States on March 16, 2007 (limited release) and March 30, 2007 (wide release). 

І том “Алмас қылыш”
(баспа “Шығыс-Батыс”, София, 2006)

ІІ том “Жанталас”
(баспа “Шығыс-Батыс”, София, 2007)

ІІІ том “Хан Кене”
(баспа “Шығыс-Батыс”, София, 2008) 

==Reception==
  Locarno Film Festival wrote that, "nearly every tenge (Kazakhstans local currency) and euro from French-based co-production partner Wild Bunch is visible on screen, judging by pics elaborate costumes, sets and cast of a thousand or so &mdash; real people not digitally generated extras", and that co-directors "Passer and Bodrov, assisted by (per credits) local director Talgat Temenov, have enough skill to make Nomad compelling by dint of old-school sincerity and sheer spectacle.     the necessary displays of athletic prowess and toothsome looks, particularly from the virile Becker". 

In the United States, it was a box office bomb, as the film was only able to scrape $79,123. While most of the critics enjoyed the cinematography and the action scenes, they criticized the film for rudimentary acting, confused directing and, for some critics who saw the English version, poor dubbing. The critics especially noted that the film had very poor screenwriting, for lines such as a scene between Mansur (Kuno Becker) and Gauhar (Ayan Yesmagambetova):  Mansur: You have the scent of the moon, Gauhar: Does the moon have a scent?. 

==Awards and nominations==
In addition to being Kazakhstans entry in the race for the Academy Award for Best Foreign Language Film, Carlo Siliotto received a Golden Globe Award nomination for Best Original Score.

==See also==
*List of most expensive non-English language films
*List of historical drama films of Asia
*Ten Great Campaigns
*Zunghar Khanate

==References==
 

==External links==
*  
*  
*  
*    
*    

 

 
 
 
 
 
 
 
 
 
 
 
 
 