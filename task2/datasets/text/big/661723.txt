Change of Habit
{{Infobox film
| name           = Change of Habit
| image          = Change of Habit 1969 Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster William A. Graham Joe Connelly
| screenplay     = {{Plainlist|
* James Lee
* S.S. Schweitzer
* Eric Bercovici
}}
| story          = {{Plainlist|
* John Joseph
* Richard Morris
}}
| starring       = {{Plainlist|
* Elvis Presley
* Mary Tyler Moore
}}
| music          = {{Plainlist|
* Billy Goldenberg
* Buddy Kaye
* Ben Weisman
}}
| cinematography = Russell Metty Douglas Stewart
| studio         = NBC Productions
| distributor    = Universal Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} musical drama William A. Graham and starring Elvis Presley and Mary Tyler Moore. Written by James Lee, S.S. Schweitzer, and Eric Bercovici, based on a story by John Joseph
and Richard Morris, the film is about three Catholic nuns, preparing for their final vows, who are sent to a rough inner city neighborhood dressed as lay missionaries to work at a clinic run by a young doctor. Their lives become complicated by the realities they face in the inner city, and by the doctor who falls in love with one of the nuns.
 Joe Connelly for NBC Productions and distributed by Universal Pictures. Filmed on location in the Los Angeles area and at the Universal Studios during March and April of 1969, Change of Habit was released in the United States on November 10, 1969. It spent four weeks on the Variety Box Office Survey, peaking at #17.

Change of Habit was Presleys 31st and final film acting role; his remaining film appearances were in concert documentaries. The film was Moores fourth and final film under her brief Universal Pictures contract; she would not appear in another theatrical movie until Ordinary People in 1980. Moore and Edward Asner, who also appears in the film, would go on to star in The Mary Tyler Moore Show, one of the most popular television shows in the 1970s.

==Plot==
Dr. John Carpenter is a physician in a ghetto clinic who falls for a co-worker, Michelle Gallagher, unaware that she is a nun.

Elvis stars as a professional man for the first time in his career. Dr. Carpenter heads a clinic serving an underprivileged community in a major metropolis. He is surprised to be offered assistance by three women. Unknown to him, the three are nuns in street clothing who want to aid the community but are afraid the local residents might be reluctant to seek help if their true identities were known. The nuns are also facing opposition from the ungodly priest from the local parish.

Carpenter falls for Sister Michelle Gallagher, played by wholesome Mary Tyler Moore, but Sister Michelles true vocation remains unknown to Dr. Carpenter. She also has feelings for the doctor but is reluctant to leave the order. The film concludes with Sister Michelle and Sister Irene entering a church where Dr. Carpenter is singing to pray for guidance to make her choice.

==Cast==
* Elvis Presley as Dr. John Carpenter
* Mary Tyler Moore as Sister Michelle
* Barbara McNair as Sister Irene
* Jane Elliot as Sister Barbara
* Edward Asner as Lt. Moretti
* Leora Dana as Mother Joseph
* Regis Toomey as Father Gibbons
* Darlene Love as Backup Singer (uncredited)
* Fanita James as Backup Singer (uncredited)
* Jean King as Backup Singer (uncredited)
* A Martinez as Second Teen (uncredited)
* Robert Emhardt as The Banker
* Doro Merande as Rose Richard Carlson as Bishop Finley
* Ji-Tu Cumbuka as Hawk

==Production==
By 1969, Presleys future in Hollywood was under threat. Although still financially successful, mainly due to the "make em quick, make em cheap" attitude of Presleys manager Colonel Tom Parker, Presleys films had been making less profit in recent years.  When Parker had struggled to find any studio willing to pay Presleys usual $1 million fee, he struck a deal with NBC to produce one feature film, and a TV Special entitled Elvis Presleys 68 Comeback Special|Elvis. NBC would pay Presley $1.25 million for both features, and Parker was happy in the knowledge that he was still able to earn $1 million for his client. 

The film Change of Habit had been announced in 1967, with Mary Tyler Moore signing up in October 1968.    It was considered a Moore vehicle until January 1969 when Presley signed on to take the lead role. 

Although set in New York City, the film was shot in the Los Angeles area and at the Universal Studios lot during March and April 1969. It was released nationwide in the United States on November 10, 1969 and spent four weeks on the Variety Box Office Survey, peaking at #17. 

Mary Tyler Moore and Edward Asner would soon become co-stars of her self-named The Mary Tyler Moore Show, one of televisions enduring hits from 1970-77. In Change of Habit, however, they shared no scenes. 

==Soundtrack== comeback television its attendant sessions at singles "In International Hotel in Paradise, Nevada lined up in August, his first live performances in eight years, and clearly now had turned his career around. 
In the decades since Presleys death, it has often been said that Elvis never looked better than he did in 1969, unbelievable looks, hair now a bit longer, given up the Brylcreem, confidence sky high after his comeback Unfortunately he would never recapture these days.
 Change of Let Us Pray" issued on the 1971 album Youll Never Walk Alone (Elvis Presley album)|Youll Never Walk Alone.  

Some reference sources erroneously list an outtake from the earlier Presley film, Charro!, "Lets Forget About the Stars" (a song also released on the Lets Be Friends album), as being a song recorded for Change of Habit. 

 
 

===Personnel===
* Elvis Presley - vocals
* The Blossoms - backing vocals
* B.J. Baker, Sally Stevens, Jackie Ward - backing vocals
* Howard Roberts, Dennis Budimir, Mike Deasy, Robert Bain - electric guitar
* Roger Kellaway - piano
* Joe Mondragon - double bass Max Bennett - bass guitar drums
 

===Film music track listing=== Change of Habit" (Buddy Kaye and Ben Weisman) Chris Arnold, David Martin, Geoffrey Morrow) (not used in film) Let Us Pray" (Buddy Kaye and Ben Weisman)
# "Have A Happy" (Buddy Kaye, Dolores Fuller, Ben Weisman)
# "Rubberneckin" (Dory Jones and Bunny Warren) (Not initially recorded for the film, but added to the soundtrack and performed by Elvis on camera)
 

==See also==
*List of Elvis Presley films
*Elvis Presley discography

==References==
 

==External links==
*  
*  
*  
*   at DVD Verdict

 
 

 
 
 
 
 
 
 
 
 
 