Salaami
 
{{Infobox film
| name           = Salaami
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = 
| producer       = 
| writer         = 
| screenplay     = 
| story          = love
| based on       =  
| narrator       =  Ayub Khan Roshni Jaffery
| music          = Nadeem-Shravan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood Ayub Khan and Roshni jaffery in pivotal roles.

==Star Cast== Ayub Khan..
*  Roshni jaffery Kabir Bedi...Captain
* Beena Banerjee...Mrs. Kapoor 
* Goga Kapoor...SP Gautam
* Saeed Jaffrey
* Raghvinder Singh Khatri...Brig R S Khatri
* Roshini Jaffery
* Samyukta

==Soundtrack== 
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chera Kya Dekhte Ho"
| Kumar Sanu, Asha Bhosle
|-
| 2
| "Mere Mehboob Ki Yehi Pehchaan"
| Kumar Sanu
|-
| 3
| "Tumhe Chhede Hawa Chanchal"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "Dil Jab Se Toot Gaya"
| Pankaj Udhas, Alka Yagnik
|-
| 5
| "Bas Ek Tamanna Hai"
| Kumar Sanu, Alka Yagnik 
|-
| 6
| "Jab Haal-E-Dil Tumse Kehne Ko"
| Alka Yagnik
|-
| 7
| "Mile Tum Se Bichhad Ke Hum"
| Kumar Sanu, Alka Yagnik, Kavita Krishnamurthy
|-
| 8
| "Dil Jab Se Toot Gaya (Male)"
| Pankaj Udhas
|-
| 9
| "Jab Haal-E-Dil (Sad)"
| Alka Yagnik
|}

 
 
 
 

 