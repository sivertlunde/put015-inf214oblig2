Gone (2007 film)
 
{{Infobox film
| name           = Gone
| image          = Gone2007Poster.jpg
| alt            = 
| caption        = Official poster
| director       = Ringan Ledwidge
| producer       = {{Plainlist|
* Deborah Balderstone
* Nira Park}}
| writer         = {{Plainlist|
* Andrew Upton James Watkins}}
| starring       = {{Plainlist|
* Shaun Evans
* Scott Mechlowicz
* Amelia Warner}}
| music          = David Bridie
| editing        = Chris Dickens
| cinematography = Ben Seresin
| studio         = {{Plainlist| FFC Australia
* StudioCanal
* Working Title Films WT 2  Productions}}
| distributor    = {{Plainlist| Universal Pictures  
* Hoyts Distribution  }}
| released       =  
| runtime        = 88 minutes  
| country        = {{Plainlist|
* United Kingdom
* Australia}}
| language       = English
| budget         =
| gross          = $88,748  
}}
Gone is a 2007 British-Australian psychological thriller film starring Shaun Evans and Amelia Warner as a young British couple travelling through the Australian outback who become involved with a mysterious and charismatic American (Scott Mechlowicz) whose motive for imposing his friendship upon them becomes increasingly suspect and sinister.
 directorial debut, Universal Pictures, Australian Film WT 2  Productions.

==Plot==
Alex (Shaun Evans) arrives in Sydney, and realizing he has missed his bus, he is then reading his travel guide on stone steps in the street, when Taylor (Scott Mechlowicz) suddenly sits near Alex and makes small talk with him, and then insisting he come with him. After drinking and raucously horsing around with two unnamed girls, Alex awakens in a city park with Taylor standing over him with a Polaroid camera, snapping a photo of Alex with one of the girls that night. When driving out of town in Taylors vehicle, Alex reveals that he is to be at Byron Bay to meet his girlfriend, Sophie (Amelia Warner), and Taylor suggests that they travel together. When Alex and Taylor meet Sophie, she is with Ingrid (Zoe Tuckwell-Smith), a friend. The four of them head towards Katherine Gorge in the Northern Territory. The following day, about to leave, Taylor vaguely mentions that Ingrid had to meet someone and had caught a bus, with Alex and Sophie unbemused.

Following that, they move on and while driving hit a kangaroo with their car. This accident causes Alex to receive a major head wound. Sophie and Taylor get him supplies, but Alex refuses to cooperate, citing that they need to leave and get away from Taylor. When they end up at a hotel, Sophie goes to talk to Alex, but his room is empty and he texts her saying, "Im going." Sophie tries to convince Taylor to go look for him, but they end up waiting the night. 

The next day, they head toward the next town. Taylor says that they should pull over and rest. They spend the night together and in the morning Sophie attempts to text Alex. The phone in Taylors pocket lights up. The next morning Sophie claims that Ingrid texted her to meet them at Katherines Gorge. Taylor knows this is not true and begins chasing after Sophie. Sophie, in an attempt to get away, drives the car quickly away. Alex falls out of the trunk inside of a sleeping bag, long dead. She continues to try getting away with Taylor after her in the trunk of the car. Eventually, he falls out; she backs over him and leaves.

==Cast==
* Shaun Evans as Alex
* Scott Mechlowicz as Taylor
* Amelia Warner as Sophie Yvonne Strzechowski as Sondra
* Victoria Thaine as Lena
* Zoe Tuckwell-Smith as Ingrid

==Production==
The film was shot in several locations in New South Wales and Queensland, Australia. Outback scenes were filmed near the towns of Longreach and Winton on the extensive "black soil" plains of western Queensland.

==Release==
===Box office===
Gone grossed $88,748 at the Australian box office.  . Retrieved 20 July 2011. 

===Critical reception===
The film has a 55% mixed rating on the review aggregator Rotten Tomatoes, counting from 11 reviews. 

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 