Kiss Kiss Bang Bang
 
{{Infobox film
| name           = Kiss Kiss Bang Bang
| image          = Kiss kiss bang bang poster.jpg
| border         = yes
| alt            = 
| caption        = Promotional poster
| director       = Shane Black
| producer       = Joel Silver
| screenplay     = Shane Black
| based on       =  
| starring       = {{Plain list |
*Robert Downey, Jr.
*Val Kilmer
*Michelle Monaghan
*Corbin Bernsen
 
}}
| music          = John Ottman
| cinematography = Michael Barrett
| editing        = Jim Page
| studio         = Silver Pictures
| distributor    = Warner Bros. Pictures
| released       =   
| runtime        = 103 minutes  
| country        = United States
| language       = English
| budget         = $15 million   
| gross          = $15.7 million 
}}
Kiss Kiss Bang Bang is a 2005 American neo-noir dark comedy crime film written and directed by Shane Black, and starring Robert Downey, Jr., Val Kilmer, Michelle Monaghan and Corbin Bernsen. The script is partially based on the Brett Halliday novel Bodies Are Where You Find Them, and interprets the classic hardboiled literary genre in a tongue-in-cheek fashion. The film was produced by Joel Silver, with Susan Downey (credited as Susan Levin) and Steve Richards as executive producers. 

Shot in Los Angeles between February 24 and May 3, 2004, the film debuted at the 2005 Cannes Film Festival on May 14,    and received a limited release in cinemas on October 21, 2005.

==Plot==
At a Los Angeles party, Harry Lockhart recounts recent events. Fleeing a botched burglary, Harrys friend is shot, forcing Harry to evade police by ducking into an audition. Harry unintentionally impresses the producers with an outburst of remorse they mistake for method acting. At a Hollywood party before a screen test, Harry meets private investigator Perry van Shrike, hired to give Harry on-the-job experience for his role, and party host Harlan Dexter, a retired actor who recently resolved a ten year feud over his wifes inheritance with his daughter, Veronica. He also encounters his childhood crush Harmony Lane, but wakes up in bed with her hostile friend. He runs over to Harmonys place at 6am to apologize.

Perry and Harry witness a vehicle being dumped in a lake and are spotted by the apparent killers. Perry shoots the trunk lock in a rescue attempt, but accidentally hits the female corpse inside as well. They cannot report the body because it appears Perry killed her.

Harmony contacts Harry, explaining that her sister Jenna came to L.A., stole Harmonys credit cards, and later killed herself. Believing Harry is a detective, Harmony asks him to investigate Jennas death as a murder. After she leaves, Harry discovers the lake corpse in his bathroom and a planted pistol. Harry and Perry dump the corpse, later identified as Veronica Dexter by police. Harry discovers it was Harmonys credit card that was used to hire Perry to come to the lake, tying Jenna to their case.  He goes to see Harmony, who (accidentally) slams the door on his finger, cutting it off, and she takes him to the hospital.

They go to a party but Harry is abducted, beaten and threatened to cease the investigation by the killers from the lake, and then let go. While taking Harry back to the hospital, Harmony sees the killers heading to Perrys stakeout. Realizing that Perry is heading into a trap, she leaves Harry in her car and rushes off to the stakeout, where she saves Perry and one of the killers gets shot to death by a food-cart operator. A pink-haired girl, affiliated with the killers, steals Harmonys car and unwittingly drives an unconscious Harry to her safe house. The remaining lake killer arrives and shoots her; Harry recovers the pistol and shoots the thug. His finger has fallen off again so he puts it on ice, but a dog jumps up and eats it. Harmony meets Harry at his hotel where she reveals she had told Jenna that Harlan Dexter was her real father. They quarrel in bed and he throws her out, being careful not to slam her fingers in the door.

After Harmony disappears following a lead, Harry and Perry investigate a private health clinic owned by Harlan Dexter. Perry realizes Veronica Dexter was incarcerated there by Harlan so an impostor could drop her court case. The pair capture a guard to question him but accidentally kill him, and then are captured by Dexter. He reveals he now plans to cremate his daughters corpse to remove any remaining evidence. Harry calls Harmony, who had not actually disappeared, but had simply gone to work. Harmony steals the van containing the coffin. Harry and Perry escape, but Harmony crashes the van. Perry is incapacitated in the ensuing shootout; Harry manages to kill Dexter and his thugs, but is also shot.

Waking in a hospital, Harry finds that Perry and Harmony are fine. Perry reveals Harmonys sister was not murdered but committed suicide. Jenna had escaped incest in Indiana and located Dexter, believing him to be her real father. She accidentally witnessed Dexter having sex with Veronicas impostor, the pink-haired girl. Believing this father was also incestuous, Jenna commissioned Perry and committed suicide.

Perry travels back to Harmonys home town and confronts Jennas father who is now bed-ridden, slapping him multiple times and calling him an animal. When the old man berates Perry for attacking a helpless old man, Perry says simply, "Yeah.  Big tough guy," and leaves without further explanation.

Having lost the movie role to Colin Farrell, Harry gets a job working for Perry.

==Cast==
* Robert Downey, Jr. as Harry Lockhart, a petty thief who successfully auditions for a film to escape police officers
** Indio Falconer Downey as 11-year-old Harry Lockhart
* Val Kilmer as "Gay" Perry van Shrike, a private investigator hired to help Harry study his role
* Michelle Monaghan as Harmony Faith Lane, an aspiring actress and Harrys childhood friend
** Ariel Winter as 7-year-old Harmony Faith Lane
* Corbin Bernsen as Harlan Dexter, a retired actor
* Rockmond Dunbar as Mustard, one of Dexters henchmen
* Dash Mihok as  Mr. Frying Pan, one of Dexters henchmen
* Shannyn Sossamon as The Pink-Haired Girl
* Angela Lindvall as Flicka, Harmonys friend
* Ali Hillis as Marleah Larry Miller as Dabney Shaw

==Reception==

===Box office===
The film opened on October 20 in the United States, with a limited release. From its release until mid-November, the films distribution increased every weekend due to its favorable critical reviews. It stayed in release in the United States until early January.    The film earned a total of $4,243,756 in the United States. 

Kiss Kiss Bang Bang grossed far more outside the United States, accounting for just over 70% of the films worldwide gross, accumulating $11,541,392.  The film ended up earning $15,785,148 worldwide, earning back its budget. 

===Critical response=== Mike Russell of The Oregonian observed that "This is one of Downeys most enjoyable performances, and one of Kilmers funniest. Its a relationship comedy wrapped in sharp talk and gunplay, a triumphant comeback for Black, and one of the years best movies".    Jeff Otto, an IGN critic, wrote that "It takes a bunch of genres and twists them into a blender, a pop relic that still feels current...one of the best times Ive had at the movies this year."    It was voted "Overlooked Film of the Year" by the 2005 Phoenix Film Critics Society on December 20, 2005. It holds a rating of 84% on Rotten Tomatoes, based on 156 reviews, with an average rating of 7.4/10. The critical consensus states that "Tongue-in-cheek satire blends well with entertaining action and spot-on performances in this dark, eclectic neo-noir homage."  

==Soundtrack==
The soundtrack to Kiss Kiss Bang Bang was released on October 18, 2005.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 54:35 

| title1          = The Fair
| length1         = 1:38
| extra1          = John Ottman

| title2          = Main Titles
| length2         = 1:53
| extra2          = John Ottman

| title3          = Innocent Times
| length3         = 2:02
| extra3          = John Ottman

| title4          = Toy Heist
| length4         = 1:55
| extra4          = John Ottman

| title5          = Lovely Confessions
| length5         = 2:30
| extra5          = John Ottman

| title6          = Surveillance Lesson
| length6         = 3:22
| extra6          = John Ottman

| title7          = Harry Smartens Up
| length7         = 1:48
| extra7          = John Ottman

| title8          = Dead Girl in Shower
| length8         = 3:49
| extra8          = John Ottman

| title9          = Harmony Is Dead?
| length9         = 1:25
| extra9          = John Ottman

| title10         = Saving Perry
| length10        = 4:40
| extra10         = John Ottman

| title11         = Flashback / Dropping Off Body
| length11        = 2:38
| extra11         = John Ottman

| title12         = They Took My Crickets
| length12        = 1:48
| extra12         = John Ottman

| title13         = Oh, Nuts!
| length13        = 2:56
| extra13         = John Ottman

| title14         = Whoa, Whos This?
| length14        = 1:38
| extra14         = John Ottman

| title15         = Harmony Lives
| length15        = 2:16
| extra15         = John Ottman

| title16         = Doggie Treat / First Kill
| length16        = 2:09
| extra16         = John Ottman

| title17         = Going Home
| length17        = 1:47
| extra17         = John Ottman

| title18         = Harmony Sees a Clue
| length18        = 1:24
| extra18         = John Ottman

| title19         = Harrys Rage
| length19        = 3:23
| extra19         = John Ottman

| title20         = Painful Pieces
| length20        = 1:27
| extra20         = John Ottman

| title21         = Thats the Story
| length21        = 2:46
| extra21         = John Ottman

| title22         = Broken
| length22        = 5:10
| extra22         = John Ottman featuring Robert Downey, Jr.

}}


==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 