David (1951 film)
David James Carr, and distributed by Regent Films.  Leading Welsh composer Grace Williams wrote the score for the film. The film is 38 minutes long and was given a U certificate.  It was the Welsh contribution to the Festival of Britain film festival. 

==Cast==
*D.R. Griffiths (Dafydd Rhys)
*John Davies (Ifor Morgan)
*Sam Jones (Rev Mr Morgan) 
*Rachel Thomas (Mrs Morgan)  
*Mary Griffiths (Mary Rhys)
*Gwenyth Petty (Mary Rhys as a young woman) 
*Ieuan Davies (Dafydd Rhys as a young man)
*Rev. Gomer Roberts (himself) 
*Prysor Williams (north Walian at Eisteddfod) 
*Ieuan Rhys Williams (south Walian at Eisteddfod)
*Wynford Jones (narrator)


The central character is a working man, David Griffiths, known in the film as "Dafydd Rhys", a school caretaker for decades and a former miner.  Dafydds later years in Ammanford at Amman Valley Grammar School present an ordinary man with extraordinary virtues. His innate dignity is seen here as an inspiration to the film’s narrator Ifor Morgan, who recalls in adulthood his experiences as a school pupil under David’s wing.

The actual David Griffiths never achieved the fame of his brother, the miners’ leader and first Welsh secretary Jim Griffiths, but here represents a traditional Welsh proletarian "type", who communicates a strong sense of his community’s worth and retains a fierce loyalty to the memory of his fellow pitmen.

The film’s most poignant section deals with the impact on David of the death of his son, Gwilym, from tuberculosis, and the effect on Ifor and his fellow pupils of the caretaker’s temporary estrangement from them as he retreats into himself and his memories.  Dafydd is also shown leaving the Eisteddfod after his poem, an elegy to his dead son, has failed to win the coveted Chairing of the Bard|Chair.

==References==
 

 