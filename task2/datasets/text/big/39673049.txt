The Curse (1924 film)
{{Infobox film
| name           = The Curse
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Land 
| producer       = Robert Land
| writer         = Walter Reisch   Ernst Weizmann
| narrator       =  Oscar Beregi    Albert Heine   Ferdinand Bonn
| music          = 
| editing        =
| cinematography = Nicolas Farkas
| studio         = Land-Film 
| distributor    = 
| released       = 28 February 1925 (Austria)
| runtime        = 
| country        = Austria
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Austrian drama Oscar Beregi  and Albert Heine.
 Weimar and Nazi eras. Harvey was in Vienna at the time because she was appearing in a stage revue show, 

==Synopsis==
A young Jewish woman in an Eastern European shtetl struggles  to reconcile her aspirations with her duty to her family. As her lifestyle grows wilder, her mother is shocked by her immoral behaviour and commits suicide by drowning - repeating "the curse" which has haunted the family for centuries. 

==Cast==
* Lilian Harvey as Ruth  Oscar Beregi  as Jehuda Nachmann 
* Albert Heine as Esra 
* Ferdinand Bonn as Rabbi Eliser 
* Isak Deutsch as Zuhälter 
* Alice Hetsey as Haushälterin 
* Anny Hornik as Lea 
* Reinhold Häussermann as Schadchen 
* Ria Jászonyi as Rahel 
* Olga Lewinsky as Geburtshelferin 
* Ferdinand Mayerhofer  as Arzt 
* Milena Mudin as Dirne Miriam 
* Anton Pointner   
* Eugen Preiß as Tempeldiener 
* Otto Schmöle as Torwächter 
* Hans Thimig as Sinche

==References==
 

==Bibliography==
* Ascheid, Antje. Hitlers Heroines: Stardom and Womanhood in Nazi Cinema. Temple University Press, 2003.
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 
 
 
 
 