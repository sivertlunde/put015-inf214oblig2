Balwinder Singh Famous Ho Gaya
{{Infobox film
| name           = Balwinder Singh Famous Ho Gaya
| image = Balwinder Singh Famous Ho Gaya.jpg
| caption = Theatrical release poster
| director       = Sunil Agnihotri
| producer       = Mika Singh, Sunil Agnihotri, Vandana Jain
| story          = Rajan Agarwal
| screenplay     = Rajan Agarwal, Sanjeev Soni, Shrikant Vishvakarma Shaan Mika Singh Gabriela Bertante Anupam Kher Rajpal Yadav Asrani Vindu Dara Singh
| music         = Lalit Pandit
| cinematography =Nirmal Jani
| editing         =Aseem Sinha
| released       =  
| released= 
| runtime=  136 minutes
| country= India
| language=Hindi
}}
Balwinder Singh Famous Ho Gaya is a 2014 Bollywood comedy film, directed by Sunil Agnihotri and produced by Sunil Agnihotri Productions, Vandana Motion Pictures Pvt Ltd. This film is Mika Singhs first attempt as an actor.

==Plot== Shaan and the confusion they create because of their same name Balwinder Singh. The movie revolves around the confusion created by the similar identity of the lead actors. The film shows their efforts to win the heart of the girl of their dreams, the female lead Gabriela Bertante and to acquire a multi-million estate without a known heir. The movie shows a comical plot where over 130 Balwinder Singhs land up as claimants to the estate.

==Cast==

 Shaan as Balwinder Singh
*Mika Singh as Balwinder Singh
*Gabriela Bertante
*Anupam Kher
*Rajpal Yadav
*Asrani
*Vindu Dara Singh

==SoundTrack== Shaan being singers have lend their voice for the songs of Balwinder Singh Famous Ho Gaya

===Track listing===
{{track listing
| extra_column = Singer(s)
| music_credits = No
| total_length =  
| title1 = Shake That Booty
| extra1 = Mika Singh
| length1 = 2.49
| title2 = Kenny Ji Shaan
| length2 = 2.19
| title3 = Bhopu Shaan
| length3 = 2.23
| title4 = Kaun Samjhaye
| extra4 = Mika Singh
| length4 = 2.25
| title5 = Main Tera Hoon
| extra5 = Mika Singh
| length5 = 2.28
}}

==References==
 

==External links==
*  


 
 
 
 
 


 