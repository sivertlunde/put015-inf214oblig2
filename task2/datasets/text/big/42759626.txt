The Czar of Broadway
{{infobox_film
| title          = The Czar of Broadway
| image          =
| imagesize      =
| caption        =
| director       = William James Craft
| producer       = Carl Laemmle
| writer         = Gene Towne (story & screenplay) John Wray Betty Compson
| music          = Heinz Roemheld
| cinematography = Hal Mohr
| editing        = Harry W. Lieb
| distributor    = Universal Pictures
| released       = May 25, 1930
| runtime        = 79 minutes
| country        = USA
| language       = English
}} John Wray and Betty Compson. 

==Cast== John Wray - Morton Bradstreet
*Betty Compson - Connie Cotton
*John Harron - Jay Grant the reporter
*Claud Allister - Francis
*Wilbur Mack - Harry Foster

==Preservation status==
This film is preserved in the collection of the Library of Congress. 

==References==
 

==External links==
* 
* 
* 
* 


 
 
 
 