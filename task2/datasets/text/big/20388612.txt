The Secret Invasion
{{Infobox film
| name           = The Secret Invasion
| image          = Secinvasion.jpg
| image_size     =200px
| caption        = Original film poster by Howard Terpning
| director       = Roger Corman
| producer       = Gene Corman
| writer         = R. Wright Campbell William Campbell
| music          = Hugo Friedhofer
| cinematography = Arthur E. Arling
| editing        = Ronald Sinclair
| studio         = The Corman Company San Carlos Productions
| distributor    = United Artists
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $592,000 Corman and Jerome 1990, p. 121. 
| gross          = $3 million  1,167,281 admissions (France)   Box Office Story. Retrieved: December 30, 2014.  
| website        =
}}
 William Campbell. Allies for an extremely hazardous operation behind enemy lines.

==Plot== Allies who is imprisoned in Nazi Germany|German-occupied Yugoslavia. The group is led by Major Richard Mace (Stewart Granger), who is trying to expiate his feelings of guilt for sending his own brother on a dangerous mission and waiting too long to extricate him. The fishing boat transporting Maces team is stopped by a patrol boat, but they dispose of the Germans.
 local partisans led by Marko (Peter Coe), they split up and enter Dubrovnik. Durrell is partnered with Mila (Mia Massini), a recent widow with a baby. They are attracted to each other, but Durrell becomes extremely distraught when he accidentally smothers her crying child to avoid detection by a German patrol. The team is captured and taken to the same fortress where the Italian general is being kept. They are tortured for information, but manage to escape and fulfill their mission, although Mace, Mila, Fell, Scanlon and Saval are killed while fending off German troops.

At the last minute, Rocca and Durrell discover that the man they have freed is an impostor, and he is about to exhort "his" troops to stay loyal to the Axis powers|Axis. Durrell pretends to be a Nazi fanatic and shoots the fake general; he is killed by the outraged Italians. Rocca, the last man standing, directs the Italians anger to the Germans.

==Cast==
 
* Stewart Granger as Major Richard Mace
* Raf Vallone as Roberto Rocca
* Mickey Rooney as Terence Scanlon
* Edd Byrnes as Simon Fell
* Henry Silva as John Durrell
* Spela Rozin as Mila (credited as Mia Massini in her first screen role) William Campbell as Jean Saval
* Helmo Kindermann as German Fortress Commander
* Enzo Fiermonte as General Quadri
* Peter Coe as Marko
* Nan Morris as Stephana
* Helmuth Schneider as German Patrol Boat Captain (credited as Helmut Schneider)
* Giulio Marchetti as Italian Officer
* Craig March as Petar
* Nicholas Rend	as Captain of Fishing Boat
* Todd Williams as Partisan leader
* Charles Brent as 1st monk
* Richard Johns as Wireless operator
* Kurt Bricker as German naval lieutenant
* Katrina Rozan as Peasant woman
 

==Production==
  Bob Campbell The Masque of the Red Death and The Tomb of Ligeia (1964).  

Principal photography under the working title of Dubious Patriots took place on location in Dubrovnik and other parts of Yugoslavia in the summer of 1963 for a typically short Corman shooting schedule of 36 days.  With the assistance of the Yugoslavian government, a large number of military personnel and equipment were offered, but an earthquake threatened to delay the production when troops were siphoned off to help in the relief effort.  

Cormans problems extended to not only wrangling military extras, but also to dealing with the emotions of a star like Stewart Granger "stooping" to make a B movie|"B film" and worrying that his role was not as prominent as the others in the ensemble cast. At one point, Corman actually rewrote his part "on the spot" so that Granger had more lines than Edd Byrnes, his co-star, who was a current popular television star. Stafford, Jeff.   Turner Classic Movies. Retrieved: December 30, 2014. 

The production used Eastmancolor film.

==Reception==
In a contemporary review of The Secret Invasion, The New York Times film reviewer Howard Thompson saw some positives in what was basically a "programmer": 
 
... a rather surprising amount of brisk muscularity and panoramic color, if not always credibility. The casting of this United Artists release, which arrived at the Criterion and other houses, may make some customers blink and wait for the worst ... But they, and the picture, do pretty well, considering. 
 

In Brasseys Guide to War Films, film historian Alun Evans considered the production exemplified Cormans ability to "... create something out of nothing." He also noted that The Secret Invasion has some notoriety as  "... the  sawn-off antecedent of The Dirty Dozen." Evans 2000, p. 166. 

==References==
Notes
 

Citations
 

Bibliography
 
* Corman, Roger and Jim Jerome. How I Made a Hundred Movies in Hollywood and Never lost a Dime. New York: Muller, 1990. ISBN 978-0-09174-679-7.
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 