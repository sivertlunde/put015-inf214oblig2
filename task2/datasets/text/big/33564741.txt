Count Dracula's Great Love
{{Infobox film
| name           = Count Draculas Great Love
| image_size     = 
| image	=	Draculas-great-love.jpg
| caption        =  Javier Aguirre
| producer       = Francisco Lara Polop (producer) Javier Aguirre Alberto S. Insúa Paul Naschy (story and screenplay)
| narrator       = 
| starring       = See below
| music          = Carmelo A. Bernaola
| cinematography = Raúl Pérez Cubero
| editing        = Petra de Nieva
| studio         = 
| distributor    = 
| released       = 1974
| runtime        = 85 minutes (USA) 72 minutes (USA cut version)
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 Javier Aguirre.

The film is also known as Cemetery Girls (American reissue title), Draculas Great Love (American promotional title), Draculas Virgin Lovers (UK and Canadian theatrical title) and The Great Love of Count Dracula (International English title).

== Plot summary ==

Count Draculas Great Love opens outside a creepy old smoke-filled sanitarium in the Carpathian mountains as two delivery men arrive with a large, heavy man-shaped crate. The owner, Doctor Wendell Marlow (Paul Naschy), has just purchased the sanitarium but has not yet moved in. Realizing that these rich castle-owning types have money and jewels just lying around, they decide to wander about and see if there is anything they can steal. One is struck in the head with an axe and the other gets his throat ripped out by a man in a black cape with velvet lining.

The opening scene is repeated over and over again as the opening credits roll.

Meanwhile a stagecoach loaded with four young women - Karen (Kaydee Politoff), Senta (Rosanna Yanni), Marlene (Ingrid Garbo), and Elke (Mirta Miller) and Imre Polvi ( Vic Winner), a strapping young male who is really nothing more than an extended cameo - loses a carriage wheel in the infamous Borgo Pass. When the stagecoach driver is killed in a freak accident, the five passengers seek shelter from an oncoming storm in the nearby sanitarium, where they are welcomed by Doctor Marlowe. Their host invites them into his home as long as they need, willingly providing shelter and food.

Of course, Marlowe is really Count Dracula. It is not before long that the new guests are bitten one by one, rounding out Draculas new army of the undead - save for the virginal Karen. Dracula seeks the rebirth of his daughter Radna, and in order to bring about that resurrection, Dracula must complete a blood ritual and convince Karen to voluntarily join him as his immortal bride forever in eternal darkness.

He seduces all of the girls but Karen, then turns them and chains them in his dungeon. Afterwards, he kills them with exposure to sunlight. When he seduces Karen, he knows he has found love. He tells Karen that he loves her and he can`t let her be what he is. So he stakes himself and before he dies says, "Karen".

== Cast ==
*Paul Naschy as Count Dracula / Dr. Wendell Marlow
*Rosanna Yanni as Senta
*Haydée Politoff as Karen
*Mirta Miller as Elke
*Ingrid Garbo as Marlene
*Víctor Alcázar as Imre Polvi
*José Manuel Martín
*Julia Peña as Peasant woman
*Álvaro de Luna (actor)|Álvaro de Luna as Porteador
*Susana Latour as Victim in Karens dream - Image in Negative
*Benito Pavón
*Leandro San José

== Soundtrack ==
 

== External links ==
* 
*  (English dialogue)

 

 
 
 
 
 