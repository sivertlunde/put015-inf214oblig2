Iraniyan (film)
{{Infobox film
| name           = Iraniyan
| image          = 
| image_size     =
| caption        = 
| director       = Vincent Selva
| producer       = 
| story          = 
| screenplay     = Vincent Selva Murali Meena Meena Raghuvaran Ranjith
| Deva
| cinematography = Balasubramaniem
| editing        = S. S. Vasu   Saleem
| distributor    =
| studio         = 
| released       = 18 November 1999
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1999 Tamil Murali and Meena in Ranjith play other supporting roles. The film, which has music by Deva (music director)|Deva,  released in November 1999.  The film was a fictional biography of freedom fighter "Vattakudi Iraniyan" who fought for the welfare of the village.

==Cast== Murali as Iraniyan Meena as Ponni
*Raghuvaran as Aande Ranjith as Police Officer
*Thalaivasal Vijay
*Vadivelu as Chinnaswamy

==Soundtrack==
* Varaan Paaru - Deva
* Ayyarettu - Krishnaraj, Anuradha Sriram
* Cheepoya Nee - Janaki
* En Maaman - Swarnalatha
* Chandirane - Anuradha Sriram

==Production==
A legal suit was filed against the films original title of Vaattaakkudi Iraniyan and subsequently the prefix was removed. 

==Release==
The film received mixed reviews upon release, with a critic noting "ironically, the biggest problem with the movie is this emphasis on it being fictitious rather than real". 

==References==
 

 
 
 
 


 