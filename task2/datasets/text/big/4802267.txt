The Beach (film)
{{Infobox film
| name           = The Beach
| image          = The Beach film.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Danny Boyle Andrew Macdonald John Hodge
| based on       =  
| starring       = Leonardo DiCaprio Virginie Ledoyen Guillaume Canet Robert Carlyle Tilda Swinton Paterson Joseph
| music          = Angelo Badalamenti John Cale Brian Eno
| cinematography = Darius Khondji
| editing        = Masahiro Hirakubo
| distributor    = 20th Century Fox
| released       =  
| runtime        = 115 minutes
| country        = United Kingdom United States
| language       = English French Swedish Thai Serbian
| budget         = $50 million 
| gross          = $144 million 
}} drama film novel of John Hodge. Thai island Koh Phi Phi.

==Plot== Scottish traveller who tells him of a pristine island in the Gulf of Thailand, uninhabited and forbidden, on which there lies a beautiful hidden beach and lagoon - walled in by cliffs and untouched by the tourist industry. He explains in vague terms that he settled there in secret with a group of others, but that difficulties arose and he chose to leave. Later, Richard finds a hand-drawn map showing the islands location left for him; he then enters Daffys room to find him dead by suicide.

Richard meets a French couple, Françoise (Virginie Ledoyen) and her boyfriend, Étienne (Guillaume Canet), and persuades them to accompany him to the island, partly out of an attraction to Françoise. They travel from Bangkok to the shores of Ko Samui in the Gulf, where Richard befriends a pair of American surfing|surfers. They too have heard the myth of the beach, and talk excitedly of how it supposedly has a huge crop of natural-growing marijuana. Richard feigns ignorance, but the next morning decides to copy the map and slide it under their door before he leaves.
 marijuana plantation guarded by Thai farmers armed with AK-47 assault rifles. They barely manage to evade detection. After jumping off a high cliff and landing in a lake below, they are seen by Keaty (Paterson Joseph), who takes them to the beach community, which consists of around 30 backpackers who live self-sufficiently through gardening and fishing. They are cautiously interrogated by the islands leader Sal (Tilda Swinton) regarding their knowledge of the island, who explains that they guard their secret carefully because if word spread of the beachs location travellers would descend en masse and ruin it. The community must also avoid angering the weed farmers, who dont mind the communitys current presence but insist that no more travellers may come, and that the current members must stay on their own side of the island. Eager to fit in, Richard lies that they have shown the map to no-one, which satisfies Sal. The trio are introduced to everybody and over the next few days go on to become integrated into the community and its hedonistic lifestyle.
 bioluminescent plankton, where Françoise kisses Richard and has passionate sex with him. Despite their attempts to keep the romance a secret, the whole island finds out about it, including Étienne. Although heartbroken, Étienne says he will not stand in their way if Francoise is happier with Richard.
 mako shark, but he stabs it to death with a knife, which gains him much admiration from the others - apart from Bugs, Sals boyfriend, who insults and mocks him. Soon after, when Richard is chosen to accompany Sal to the mainland to acquire supplies, Bugs threatens him and warns him not to touch Sal. During the trip Richard is inadvertently reunited with the American surfers who are preparing to go in search of the island with two girls. Sal overhears their conversation about the copy of the map and confronts Richard, who only partially admits his lies, still insisting that the surfers dont have their own copy. In exchange for Sals silence and Richards return to the island, Sal blackmails Richard into having sex with her that night.

When they return to the island, things return to normal until two of the Swedes named Sten (Magnus Lindgren) and Christo (Staffan Kihlbom) are attacked by a shark while fishing in the ocean. Sten dies almost immediately and Christo is severely injured. Sal refuses to risk compromising the islands secret by bringing medical help, and insists that his options are to go to a hospital on the mainland via the supply boat or stay on the island and take his chances. Christo chooses to stay, not wanting to go near the water after his encounter with the shark. His condition worsens despite attempts to care for him, his constant moans of pain lowering the morale of the group, so they take him out into the jungle and leave him to die. Only Étienne objects to this, expressing his disgust with the others and vowing to stay with Christo.
 Koh Phangan. hallucinating that he is a character in a video game and declaiming (in voice over narration) "the longer Im away from the community, the less I miss them". Keaty discovers him hiding in a storeroom and expresses his fears that Richard is becoming unhinged in the same way Daffy did, but cannot make hims snap out of it. Richard later creeps into the farmers camp at night and points a rifle at the sleeping leader; he cannot bring himself to pull the trigger, so replaces the weapon and sneaks out again.
 mercy killing. When he leaves the tent however, he is struck across the face by a farmer and knocked unconscious.
{{Quote box
| quoted = true
| style = width: 22em; margin: 0.5em 0 0.5em 1em; padding: 0.2em;
| quote = Of course you could never forget what we have done, but we adapt. We carry on. And me? I still believe in paradise. But now at least Ill know its not some place you can look for because its not where you go, its how you feel for a moment in your life when youre a part of something. And if you find that moment...it lasts forever. 
| salign = center
| source = Richards monologue at the end of the film
}}

He wakes up in a tent in pain, surrounded by the community and the farmers. The farmers approach Sal, with whom they made the agreement, and the lead farmer (Abhijati Meuk Jusakul) gives her a gun loaded with a single bullet and an ultimatum: shoot Richard dead and be allowed to stay, or leave the island forever. Sal approaches Richard angry that he ruined everything and fires an empty chamber, shocking the group with her willingness to commit murder. The lead farmer smiles as the community instantly disintegrates. Sal collapses in a flood of tears, as the group, now in hysterics, flees the room. Together, most of them swim back to the mainland and go their separate ways, leaving Sal and possibly some others behind.

The film ends with Richard stopping by an  . Love, Françoise x.

==Cast==
 .]]
* Leonardo DiCaprio as Richard, a college student
* Tilda Swinton as Sal, the leader of the islands colony.
* Robert Carlyle as Daffy, a Scottish traveler that Richard meets.
* Virginie Ledoyen as Françoise, the girlfriend of Étienne who befriends Richard.
* Guillaume Canet as Étienne, the boyfriend of Françoise who befriends Richard.
* Paterson Joseph as Keaty, a member of the islands colony who believes in Christianity and cricket.
* Lars Arentz-Hansen as Bugs, Sals boyfriend who serves as the Carpentry|carpenter.
* Peter Youngblood Hills as Zeph
* Jerry Swindall as Sammy
* Zelda Tinska as Sonja
* Victoria Smurfit as Weathergirl
* Daniel Caltagirone as Unhygienix, the island colonys resident chef that has an obsession with soap since he often cleans himself after cooking something.
* Peter Gevisser as Gregorio
* Lidija Zovkic as Mirjana
* Samuel Gough as Guitarman
* Staffan Kihlbom as Christo, the fisherman of the islands colony.
* Jukka Hiltunen as Karl, the fisherman of the islands colony.
* Magnus Lindgren as Sten, the fisherman of the islands colony.
* Abhijati Meuk Jusakul as Senior Farmer, the leader of the marijuana plantation.
* Luke Parker as Goat Herder
* Hélène de Fougerolles as Beach Community Member
* Emma Renae Griffiths as Waitress
* Sanya Gai Cheunjit as Farmer
* Kaneung Nueng Kenia as Farmer
* Somchai Santitarangkul as Farmer
* Seng Kawee as Farmer
* Somkuan Kuan Siroun as Farmer

==Differences from the novel==
There were some parts of the film that are different from the novel version:

* Richard is British and Sal is American in the novel.

* Richards obsession with war and video games is explained a bit more in the novel.

* Keaty is not obsessed with his Game Boy in the film.

* Richard never sleeps with Françoise despite having feelings for her, which he thinks are reciprocated, saying that he considers Étienne a good guy and would not want to do that to him.

* Richard never sleeps with Sal, nor is it Sal who accompanies him to the mainland for supplies, but rather a character called Jed (who patrols the islands perimeter) who does not appear in the film. In the book, Jed is the person who leads Richard, Etienne and Françoise to the community, not Keaty.

* The part where Keaty catching a dead squid that gave some of its inhabitants food poisoning is not in the film.

* Karl escaping from the island in the beach communitys main boat was not in the film.

* The ending is different from the book where the book version had Richard, Françoise, Étienne, Keaty, and Jed attempting to escape from the now crumbling community. In the books epilogue after their successful escape, they move into their respective lives. Richard loses touch with Étienne and Françoise yet finds it hard to be totally freed of the effects of his experiences in that "parallel universe".

==Production==
Ewan McGregor was cast as the main character before leaving due to disputes with the director. It was speculated that Boyle was offered additional funding under the condition that DiCaprio be cast and his character made American. 

Real-life drama unfurled on set one day when the cast and crew were involved in a boating accident during production. It was reported that the incident involved both Boyle and DiCaprio. No one was injured.  
 Tsunami of 2004, however, has reshaped the beach to its natural look. 

Boyle has been cited saying that the look of the jungle scenes in the film was inspired by the Rare/Nintendo game Banjo-Kazooie. 

The waterfall scene, where DiCaprio and others jump from a high cliff to the water below, was filmed in Khao Yai National Park in central Thailand, at the Haew Suwat Waterfall.

The map in the film was illustrated by the author of the book that The Beach was based upon, Alex Garland. He received credit for this as the cartographer.

 

===Controversy===
Controversy arose during the making of the film due to 20th Century Foxs bulldozing and landscaping of the natural beach setting of Ko Phi Phi Lee to make it more "paradise-like". The production altered some sand dunes and cleared some coconut trees and grass to widen the beach. Fox set aside a fund to reconstruct and return the beach to its natural state; however, lawsuits were filed by environmentalists who believed the damage to the ecosystem was permanent and restoration attempts had failed.   Following shooting of the film, there was a clear flat area at one end of the beach that was created artificially with an odd layout of trees which were never rectified, and the entire area remained damaged from the original state until the Tsunami of 2004.

The lawsuits dragged on for years. In 2006, Thailands Supreme Court upheld an appellate court ruling that the filming had harmed the environment and ordered that damage assessments be made. Defendants in the case included 20th Century Fox and some Thai government officials. 
 Buddha in a bar was cited as "Blasphemy|blasphemous". 

==Soundtrack==
{{Infobox album  
| Name        = The Beach: Motion Picture Soundtrack
| Type        = soundtrack
| Artist      = various artists
| Released    = 21 February 2000
| Genre       = Electronica, Ambient music|ambient, Rock music|rock, Britpop
| Length      = 76:53 Sire
| Producer    = Pete Tong
| Chronology  = Danny Boyle film soundtrack A Life Less Ordinary (1997)
| This album  = The Beach (2000)
| Next album  =   (2002)
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =    
}}
The soundtrack for the film features "8 Ball" by Underworld (band)|Underworld, as well as tracks by Orbital (band)|Orbital, Moby, Blur (band)|Blur, New Order, Sugar Ray, Faithless, Leftfield, and others. The songs "Synasthasia" by Junkie XL, "Out of Control" by The Chemical Brothers, "Fiesta Conga" by Movin Melodies, "Redemption Song" by Bob Marley, "Neon Reprise" by Lunatic Calm and "Smoke Two Joints" by Chris Kay and Michael Kay were also included in the movie but omitted from the soundtrack.  All Saints song "Pure Shores" topped the UK Singles Chart. The soundtrack was co-produced by Pete Tong.

The teaser trailer for the film featured "Touched" by VAST, and while the song was not used in the finished film, it has been widely associated with it ever since.

The film score was composed by Angelo Badalamenti, and a separate album containing selections of his score was released as well.

Track listing:
# "Snakeblood" (composed by Neil Barnes and Paul Daley; performed by Leftfield) – 5:39 All Saints) – 4:24
# "Porcelain (song)|Porcelain" (composed and performed by Moby) – 3:58
# "Voices" (composed by Stephen Spencer, Paul Geoffrey Spencer & Scott Rosser; performed by Dario G featuring Vanessa Quinones) – 5:19
# "8 Ball" (composed by Rick Smith, Karl Hyde and Darren Emerson; performed by Underworld (band)|Underworld) – 8:51
# "Spinning Away" (composed by Brian Eno and John Cale; performed by Sugar Ray) – 4:24
# "Return of Django" (composed by Lee "Scratch" Perry; performed by Asian Dub Foundation featuring Harry Beckett and Simon de Souza) – 4:17
#* Originally performed by The Upsetters On Your Own (Crouch End Broadway Mix)" (composed and performed by Blur (band)|Blur) – 3:32
# "Yé ké yé ké (Hardfloor Edit)" (composed and performed by Mory Kante; remix by Hardfloor) – 3:55
# "Woozy" (composed and performed by Faithless) – 7:53
# "Richard, Its Business as Usual" (composed and performed by Barry Adamson) – 4:17
# "Brutal" (composed and performed by New Order) – 4:49 UNKLE featuring Richard Ashcroft) – 8:53 Orbital and Angelo Badalamenti) – 6:45

==Release==
===Critical response===
Though the film was commercially successful it was largely panned by critics. It has only a 19% rating on Rotten Tomatoes, with the critical consensus being that the film is "unfocused and muddled,   a shallow adaptation of the novel it is based on" although it contains "gorgeous cinematography";  despite this, it maintains a score of 43/100 on Metacritic. 
 Razzie Award for Worst Actor for his work on the film.

The budget of the film was US$ 50 million. Global takings totaled just over US$ 144 million, of which US$ 39 million was from the USA. 

===Home video===
The film has been released on VHS and DVD. The standard DVD release included nine scenes that were deleted from the movie, including an alternative ending which to a degree resembles the one in the novel, were later included in a Special Edition DVD release, along with Danny Boyles commentary on what might have been their purpose. There is also an alternative ending which depicts Sal committing suicide and everyone loading up on a boat from the raft.

==See also==
 
* Phi Phi Islands

==References==
;Notes
 

==External links==
 
* 
* 
* 
* 
* 
* 
* , Rolf Potts, Salon.com
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 