The Fornaretto of Venice
{{Infobox film
| name = The Fornaretto of Venice
| image =
| image_size =
| caption =
| director = Duilio Coletti 
| producer = Vittorio Vassarotti 
| writer = Francesco DallOngaro (play)   Luciano Doria]]   Tomaso Smith   Duilio Coletti 
| narrator = Roberto Villa   Elsa De Giorgi   Clara Calamai   Osvaldo Valenti 
| music = Piero Giorgi 
| cinematography = Jan Stallich 
| editing = Maria Rosada 
| studio = Vi-Va Film 
| distributor = Artisti Associati 
| released = 23 October 1939
| runtime = 73 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Roberto Villa, play of the same title by Francesco DallOngaro, which has been adapted into films on several occasions. It was made at the Cinecittà studios in Rome.

== Cast == Roberto Villa as Piero Tasca, il Fornaretto 
* Elsa De Giorgi as Annetta 
* Clara Calamai as Olimpia Zeno 
* Osvaldo Valenti as Alvise Duodo 
* Enrico Glori as Lorenzo Loredano
* Gero Zambuto as Tasca, il fornaio 
* Carlo Tamberlani as Mocenigo 
* Letizia Bonini as Elena Loredano 
* Ermanno Roveri as Tonin 
* Renato Chiantoni as Il sarto testimone al processo 
* Pietro Germi as Frate domenicano 
* Cesare Polacco as Barnaba 
* Stefano Sibaldi as Il parucchiere di Elena 
* Mario Ersanilli
* Carlo Mariotti
* Cesare Zoppetti

== References ==
 

== Bibliography ==
* Forgacs, David & Gundle, Stephen. Mass Culture and Italian Society from Fascism to the Cold War. Indiana University Press, 2007.
 
== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 