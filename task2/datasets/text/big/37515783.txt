The Man Who Saved the World
 
{{Infobox film
| name           = The Man Who Saved the World
| image          = 
| caption        = 
| director       = Peter Anthony
| producer       = Jakob Staberg Christian D. Bruun Mark Romeo Raphael Avigdor Svilen Kamburov
| writer         = Peter Anthony
| starring       = Stanislav Petrov Nataliya Vdovina Sergey Shnyryov
| composer         = Kristian Eidnes Andersen
| cinematography = Kim Hattesen Anders Löfstedt
| editing        = Morten Højbjerg
| distributor    = 
| released       = Premiered October 2014 Woodstock Film Festival
| runtime        = 
| rating         = 
| country        = United States
| language       = English and Russian
| budget         = 
}}

The Man Who Saved the World by Danish director Peter Anthony is a 2014 feature film about Stanislav Petrov, a former lieutenant colonel of the Soviet Air Defence Forces. Other stars appearing in the film include Robert De Niro, Matt Damon, Ashton Kutcher and Walter Cronkite. 

The film premiered in October 2014 at the Woodstock Film Festival in Woodstock, New York, winning; "Honorable Mention: Audience Award Winner for Best Narrative Feature" and "Honorable Mention: James Lyons Award for Best Editing of a Narrative Feature." 

==Synopsis==
For a few crucial moments on September 26, 1983, Stanislav Petrov  held the fate of the world in his hands.

When an alarm suddenly went off at Soviet nuclear early warning center Serpukhov-15, Stanislav was responsible for reacting to a report that five American nuclear missiles were heading toward the Soviet Union. Rather than retaliate, Stanislav followed his gut feeling and went against protocol, convincing the armed forces that it was a false alarm. His decision saved the world from a potential devastating nuclear holocaust.

Three decades later, this forgotten hero went on a spectacular journey to the United States, where he was finally acknowledged for his historic deed and found the strength to reconcile with his past. On his journey, he was greeted by Walter Cronkite as "The Man Who Saved the World" and met Kevin Costner, Robert De Niro, Matt Damon, and Ashton Kutcher.

Shot on location in the former Soviet Union and the United States, The Man Who Saved the World shines a light on nuclear disarmament. It shows how precarious our world has become in the nuclear age and how our own belief in humanity and each other is the hope that we must foster in order to survive and thrive.

==Production Notes==
Re-enactments are intertwined with modern documentary footage of Stanislav to create a hybrid film that feels and looks like a narrative feature film rather than a traditional documentary.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 