The Blue Book, Political Truth or Historical Fact
 
 
 

 , is a documentary film by   The film depicts the on going denial of Armenian genocide.

== Synopsis ==
In 2005 the Turkish Grand National Assembly (TGNA) send a petition, signed by all its members, to the British parliament demanding an apology for the accusation that Turkey was guilty of Genocide against the Armenians. The petition referred to the British parliamentary report, “The Treatment of Armenians in the Ottoman Empire 1915-16”, authored by Viscount Bryce and Arnold J. Toynbee, better known as the Blue Book.

Ara Sarafian a British Armenian historian had republished an uncensored edition of the Blue Book five years earlier in 2000. Once he heard about this petition he set out on a   to demonstrate the authenticity of the original work and to defend the integrity of his republication. 

This observation documentary follows Ara Sarafian as he travels to the site of massacres described in the eyewitness accounts which were used in the British parliamentary Blue Book. He confronts official Turkish historians at an academic symposium in Istanbul University. While there he answers questions from Turkish journalists how were interested in his motivations. Later he takes part in a Television debate discussing the validity of this almost hundred year old report. Through out the debate the official Turkish historian casts doubt at the authenticity of the Blue Book, but during a break in the broadcast, while still being filmed for the documentary, he admits to Ara and the presenter of the program, that the TGNA petition was a political exercise, it was academically weak and Ara’s response was accurate.

== Background ==
The filming of the Blue Book started in August of 2005. In 2007 a version was released briefly for funding purposes. Following this there was some additional filming, and the Blue Book documentary was completed in Augest of 2009. In 2014 as part of the Armenian genocide centennial commemorations Blue Book app was develop including additional footage.

== Screenings ==
* 25th November 2014, SOAS & UCL Armenian Society Screening, SOAS WC1H 0XG.
* 25th September 2010, Egyptian Theatre.
* 28 May 2010, Oxford Armenian Society & Oxford Aegis Society, New College, University of Oxford.
* 5 March 2010, London School of Economics, Armenian Society, Hong Kong theatre.
* 27th January 2010, Royal Holloway, University of London.
* 27th November 2009, Queen Mary College University London, Hitchcock theatre.
* 29th October 2009, National Film and Television School Alumni, Warner Brothers, WC1X 8WB.
* 25 September 2008, Pomegranate Film Festival Toronto.
* 29th May 2008, Los Angeles,Laemmles One Colorado.  
* 4th of November 2007, at Riverside Studios London.

== Contributors ==
* Ara Sarafian; Director of Gomidas Institute, archival Historian. Lord Eric Avebury; Founder of Parliamentary Human Rights Group.
* Şükrü Elekdağ; Member of Turkish grand National Assembly(2002-2011) from the Republican people’s Party. Professor Justin McCarthy; professor of history at the University of Louisville.
* Dr. Professor Kemal Çiçek; Turkish Historical Society.
* Temel Demirer; Turkish Scholar, Author, Political activist.
* Fatih Altaylı; Turkish journalist, columnist, television presenter and media executive.
* Ragip Zarakolu; Turkish Publisher & Vice Chair of Turkish Human Rights Association.

== References ==
*  ,
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

== External links ==
 
*  
*  
*  
* Gomidas Institute
*  
*  

 
 
 
 