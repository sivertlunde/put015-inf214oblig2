Lykkens musikanter
 
{{Infobox film
| name           = Lykkens musikanter
| image          =
| image size     =
| caption        =
| director       = Peer Guldbrandsen
| producer       = Peer Guldbrandsen
| writer         = Peer Guldbrandsen Patricia McLean
| narrator       =
| starring       = Ellen Gottschalch
| music          = Sven Gyldmark
| cinematography = Karl Andersson
| editing        = Jon Branner	
| studio         = Saga Studios
| released       =  
| runtime        = 84 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}

Lykkens musikanter is a 1962 Danish film directed by Peer Guldbrandsen and starring Ellen Gottschalch.

==Cast==
* Ellen Gottschalch - Lydia Wiljengren
* Ove Sprogøe - Havemand Texas
* Dirch Passer - Elevatorfører Gogol
* Kjeld Petersen - Sagfører Dagerman
* Bent Mejding - Sagfører Rolf Dagerman
* Ulla Lock - Nancy Hansen
* Frede Heiselberg - Jon Dagerman
* Hans W. Petersen - Tømmerhandler Dagerman
* Bertel Lauring - Slagteren
* Ingela Brander - Solveig
* Helle Halding - Rakel
* Michaela Davidsen - Ung kontordame

==External links==
* 

 
 
 
 
 
 


 