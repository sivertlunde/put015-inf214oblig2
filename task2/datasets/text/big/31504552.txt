Evils of the Night
{{Infobox film
| name           = Evils of the Night
| image          = 
| caption        = 
| director       = Mardi Rustam
| producer       = Mardi Rustam Mohammed Rustam
| writer         = Mardi Rustam Philip Dennis Connors
| narrator       = 
| starring       = Aldo Ray Neville Brand Tina Louise John Carradine Julie Newmar
| music          = Robert O. Ragland
| cinematography = Don Stern
| editing        = Henri Charr
| distributor    = Aquarius Releasing
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Evils of the Night is a 1985 low-budget science fiction/"porno horror" film Mank, Gregory W. (2001). Hollywood cauldron: thirteen horror films from the genres golden age. McFarland, ISBN 978-0-7864-1112-2  starring    Editorial staff (Dec 4, 1985). ARMED MAYHEM. Orlando Sentinel 

==Plot==
Vampire aliens Dr. Kozmar (Carradine), Dr. Zarma (Newmar) and Cora (Louise) recruit two dim-witted mechanics (Ray and Brand) to abduct teenagers living in a college town and bring them to a rural hospital. There, the aliens drain them of their blood, which they need to stay young. Martin, Mick; Porter, Marsha (2004). DVD & video guide 2004. Ballantine Books, ISBN 978-0-345-44994-8 

Reviews were generally negative. A reviewer at the    Roger Hurlburt at the Fort Lauderdale Sun Sentinel wrote,  "Simply stated, Evils of the Night is a deplorable motion picture." Hurlburt, Roger (June 9, 1987). Evils of the Night just bloody awful.  Fort Lauderdale Sun Sentinel 

==References==
 

==External links==
* 