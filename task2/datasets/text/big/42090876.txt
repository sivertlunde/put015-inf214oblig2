It's You I Want
{{Infobox film
| name =  Its You I Want
| image =
| image_size =
| caption =
| director = Ralph Ince Herbert Smith 
| writer =  Maurice Braddell (play)   Cyril Campion
| narrator =
| starring = Seymour Hicks   Marie Lohr   Hugh Wakefield   Jane Carr
| music = 
| cinematography = George Stretton
| editing =  British Lion 
| distributor = British Lion
| released = October 1936 
| runtime = 73 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Its You I Want is a 1936 British comedy film directed by Ralph Ince and starring Seymour Hicks, Marie Lohr and Hugh Wakefield. It was made at Beaconsfield Studios.  The films sets were designed by Norman Arnold.

==Cast==
* Seymour Hicks as Victor Delaney 
* Marie Lohr as Constance Gilbert 
* Hugh Wakefield as Otto Gilbert  Jane Carr as Melisande 
* Lesley Wareing as Anne Vernon 
* Henry Hugh Gordon Stoker|H.G. Stoker as Braille  Gerald Barry as Maj. Entwhistle  Ronald Waters as Jimmy Watts 
* Dorothy Hamilton as Lady Maureen

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 