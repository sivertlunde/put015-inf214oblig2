Un amore
{{Infobox film
| name           = Un amore
| image          = Un amore.jpg
| image_size     =
| caption        =
| director       = Gianni Vernuccio
| producer       =
| screenplay     =  Ennio De Concini
| based on       = A Love Affair by Dino Buzzati
| starring       =
| music          = Giorgio Gaslini
| cinematography = Aldo Scavarda
| editor       =
| distributor    =
| released       = 15 December 1965
| runtime        =
| country        = Italy Italian
| budget         =
}}

Un amore is a 1965 Italian romance film directed by Gianni Vernuccio. It is based on the novel A Love Affair by Dino Buzzati. 

==Cast==
*Rossano Brazzi ...  Antonio Dorigo
*Agnès Spaak ...  Laide
*Gérard Blain ...  Marcello
*Marisa Merlini ...  Ermelina
*Lucilla Morlacchi ...  Luisa
*Alice Field
*Cesare Barilli
*Lia Reiner
*Stella Monclar
*Lina Pozzi
*Wilma Casagrande
*Febo Villani
*Anna María Aveta

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 