The Fighter (1952 film)
{{Infobox film
| name           = The Fighter
| image size     = 
| image	=	The Fighter FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Herbert Kline
| producer       = Alex Gottlieb
| screenplay     = Aben Kandel Herbert Kline
| based on       =  
| starring       = 
| music          = Vincente Gomez
| cinematography = James Wong Howe Edward Mann
| studio         = Alex Gottlieb Productions
| distributor    = United Artists
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}} The Mexican" by Jack London. The film is directed by Herbert Kline and produced by Alex Gottlieb. Kline and Aben Kandel wrote the adapted screenplay. The film was released by United Artists in the United States on  , 1952.

==Plot==
A boxer, in Mexico, sets out to avenge the murder of his family by using the money from his winnings to purchase weapons.

==Cast==
*Richard Conte as Felipe Rivera
*Vanessa Brown as Kathy
*Lee J. Cobb as Durango
*Frank Silvera as Paulino
*Roberta Haynes as Nevis
*Hugh Sanders as Roberts
*Claire Carleton as Stella
*Martin Garralaga as Luis
*Argentina Brunetti as Maria
*Rodolfo Hoyos, Jr. as Alvarado
*Margaret Padilla as Elba
*Paul Fierro as Fierro
*Rico Alaniz as Carlos Paul Marion as Rivas Robert Wells as Tex

==Further reading==
* 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 