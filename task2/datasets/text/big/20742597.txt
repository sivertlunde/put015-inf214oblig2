Mathadana
{{Infobox film
| name           = Mathadana
| image          = 
| caption        = 
| director       = T. N. Seetharam
| producer       = H. G. Narayan   I. P. Malle Gowda
| writer         = S. L. Bhyrappa
| screenplay     = T. N. Seetharam
| based on       =  
| narrator       =  Tara   Avinash
| music          = C. Aswath   V. Manohar
| cinematography = Ashok Kashyap
| editing        = Suresh Urs
| studio         = 
| distributor    = Anikethana Chithra
| released       =  
| runtime        = 147 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}

Mathadana ( ) is a 2001 Indian Kannada language film directed by T. N. Seetharam, based on S. L. Bhyrappas novel of the same name, and starring Anant Nag, Tara (Kannada actress)|Tara, Devaraj, Avinash and Hemanth Vasisht in pivotal roles.
 Best Supporting Actor. 

==Cast==
* Anant Nag Tara
* Devaraj
* Avinash
* Hemanth Vasisht
* Sundar Raj
* Mukhyamantri Chandru
* Prakash Belawadi
* Sudha Belawadi
* Karibasavaiah
* Girija Lokesh
* Sihi Kahi Chandru
* Lakshmi Chandrashekar

==Production==
The film was based on the S. L. Bhyrappas novel Mathadana, and reports said the filming rights were bought from him, for a fee of between   and  .   

==Soundtrack==
{{Infobox album
| Name        = Mathadana
| Type        = Soundtrack
| Artist      = C. Aswath and V. Manohar
| Cover       = Mathadana album cover.JPG
| Border      = yes
| Caption     = Soundtrack cover
| Released    = January 2001
| Recorded    = 2000 Feature film soundtrack
| Length      = 
| Label       = Tips Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

C. Aswath and V. Manohar composed the music for the film and the soundtracks. The soundtrack "Aluva Kadalolu Theli" was taken from a poem written by poet Gopalakrishna Adiga and "Naayi Thalimyalina" by poet H. S. Venkateshamurthy.  The album has seven soundtracks and was released in the form of audio cassettes in the second week of January 2001. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Idu Modalane Haadu
| lyrics1 = V. Manohar
| extra1 = Rajesh Krishnan, Nanditha
| length1 = 
| title2 = Olavali Hadithe Hamsa
| lyrics2 = V. Manohar
| extra2 = Rajesh Krishnan, Nanditha
| length2 = 
| title3 = Aidu Varshakomme
| lyrics3 = Siddalingaiah
| extra3 = Rajesh Krishnan
| length3 = 
| title4 = Ranga Kunidu Rangu Eri
| lyrics4 = Krishnamurthy Hanooru
| extra4 = Yuvaraj Janardhan, K. S. Surekha, M. D. Pallavi Arun|M. D. Pallavi
| length4 = 
| title5 = Naayi Thalimyalina
| lyrics5 = H. S. Venkateshamurthy
| extra5 = C. Aswath
| length5 = 
| title6 = Aluva Kadalolu Theli
| lyrics6 = Gopalakrishna Adiga
| extra6 = Rajesh Krishnan, B. R. Chaya
| length6 = 
| title7 = Vrindavanadolu
| lyrics7 = V. Manohar
| extra7 = Nanditha
| length7 = 
}}

==References==
 

 

 
 
 
 
 


 