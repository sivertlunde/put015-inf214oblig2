Lunar Eclipse (film)
{{Infobox film
| name           = Lunar Eclipse
| image          = Lunar_Eclipse_Poster.jpg
| caption        = 
| director       = Wang Quanan
| producer       = 
| writer         = Wang Quanan Wu Chao Hu Xiaoguang
| music          = Zhang Yang
| cinematography = Gao Fei
| editing        = Dayyan Eng
| distributor    = 
| studio         = Beijing Film Studio 
| released       =  
| runtime        = 95 minutes
| country        = China
| language       = Mandarin
| film name = {{Film name|  jianti         = =月蚀
|  fanti          = 月蝕
|  pinyin         = Yùe shí}}
| budget         = 
}} 1999 Chinese film and the directorial debut from Sixth Generation director Wang Quanan. It is also the feature film debut of Wangs most frequent collaborator/muse Yu Nan. Unlike his next two films, which focus on rural communities, Lunar Eclipse is an urban drama following the wife of a newlywed couple (Yu Nan) who becomes mesmerized by an amateur photographer (Wu Chao) who claims to have once been in love with a woman who looked just like her. The film was produced by the Beijing Film Studio. 
 Suzhou River.  

==Cast==
* Yu Nan as Ya Nan, a young bride
* Hu Xiaoguang as Guohao, Ya Nans husband is Ying Yan Xiang Wu Chao as Xiaobing, a minivan driver and amateur photographer

== Reception ==

=== Awards and nominations ===
* 2000 22nd Moscow International Film Festival - Lunar Eclipse was in competition for the festivals top prize, the Golden St. George, though it failed to win. It did, however, win the FIPRESCI Prize.   
* 2001 Deauville Asian Film Festival - Lunar Eclipse won a prize for Best Actress for Yu Nans performance.

== See also == Suzhou River, Lou Yes 2000 film that share similar themes

== References ==
 

== External links ==
*  
*   at the Chinese Movie Database
*   at Cinemasie

 

 
 
 
 
 
 
 


 
 