Chintoo (film)
{{multiple issues|
 
 
}}

 
{{Infobox film
| name           = 
| image          = 
| caption        = 
| director       = Shrirang Godbole
| producer       = Shrirang Godbole, Abhay Gadgil
| screenplay     = Shrirang Godbole, Vibhavari Deshpande
| starring       = 
| music          = Saleel Kulkarni
| cinematography = Sanjay Jadhav
| editing        = Abhijeet Balaji Deshpande
| studio         = Indian Magic Eye Motion Pictures Pvt. Ltd.
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Chintoo (Marathi Film) is the feature film version of the comic character Chintoo and his gang which was released on 18 May 2012, produced by Indian Magic Eye Motion Pictures Pvt. Ltd.. The film opened to a delightful critical and commercial response and was widely appreciated by the younger audiences. The film, directed by Shrirang Godbole, has Shubhankar Atre playing the lead character of Chintoo. Actors like Subodh Bhave, Vibhawari Deshpande and others played the supporting characters.

==Plot Summary==
Chintoo, a sweet and naughty boy lives in an upper middle class, lively neighborhood with his parents. This eight-year-old is extremely popular in his group. Pappu is his best friend and Mini, Raju, Baglya, Neha and the toddler Sonu all together are the unbeatable Wanarwede Warriors

Chintoo and his gang have big plans for their much awaited summer vacation. They decide to beat their rivals Vinchoo biters by making Wanarwede Warriors strong enough even to win the world cup. They practice hard and also convert the barren piece of land in the society into their very own Wanarwede stadium. The match starts as planned.

Guru, a rogue forcibly enters that space on the same day and sets up a small Chinese eatery. Akki, watchman Sakharams son is supposed to run it. Children protest against this intrusion but no one cares. The adults are in fact happy to get an easy access to the Chinese food. No one except Colonel Kaka objects to this illegal entrant and his illegitimate business in the housing complex.

Children try to find space to play cricket but always end up being scolded by adults. They try all means and ways to get their stadium back and also make the adults understand their problem. All their attempts fail until Chintoo; the mastermind comes up with his own idea of making a Chinese dragon which Guru worships so that he will get scared and leave the ground. The Wanarwede warriors are later joined by the Vinchoo biters as even they don`t have ground to play. In the end Chintoo and his friends earn back their ground after their parents realise how important the ground is to them.

==Cast==

Chintoo - Shubhankar Atre

Mini - Suhani Dadhphale

Baglya - Animesh Padhye

Raju - Ved Ravade

Pappu - Nishant Bhavsar

Neha - Rumani Khare

Sonu - Arjun Jog

Chintoo’s Aai - Vibhavari Deshpande

Chintoo’s Baba - Subodh Bhave

Chintoo’s Ajoba - Shriram Pendse

Colonel Kaka - Satish Alekar

Joshi Kaka - Dilip Prabhavalkar

Joshi Kaku - Bharti Aacharekar

Sakharam Watchman - Vijay Nikam

Satish Dada - Alok Rajwade

Guru Dada - Nagesh Bhosale

Akki - Om Bhutkar

Art Teacher - Vijay Patwardhan

Guruji - Prashant Tapasvi

Chimanee - Mrinmayee Godbole

Popat - Pankaj Gangan

Chimanee’s Father - Sanjay Lonkar

Chimanee’s Mother - Pournima Ganu Manohar

Popat’s Mother - Sadhana Sarpotdar

Mini’s Aai - Mridul Patwardhan

Mini’s Baba - Sunil Abhyankar

Raju’s Aai - Snehal Tarde

Raju’s Baba - Suraj Satav

Neha’s Aai - Soniya Khare

Neha’s Baba - Ajay Apte

Pappu’s Aai - Chitra Khare

Pappu’s Baba - Amit Patwardhan

==About Cast & crew==

Director - Shrirang Godbole

Story, screenplay and dialogue - Shrirang Godbole, Vibhawari Deshpande

Director of Photography - Sanjay Jadhav

Lyrics - Sandeep Khare

Music - Dr. Saleel Kulkarni

Editor - Abhijeet Balaji Deshpande

Art Director - Siddharth Tatooskar, Bhakti Tatooskar

Costumes and Makeup - Geeta Godbole

Visual Effects (VFx) - Golden Square Media Works Pvt. Ltd.

Producer - Abhay Gadgil,Shrirang Godbole, Amit Patwardhan, Hrishi Deshpande, Chintamani Vartak, Rajesh Deshmukh

“Chintoo” Cartoon-strip Creators - Charuhas Pandit, Prabhakar Wadekar

The film is the first Indian feature which has been adapted from a daily comic strip.

==Sequel==

A sequel to the first film was announced on 11 March 2013. Titled "Chintoo 2 - Khajinyachi Chittarkatha". The film was released across Maharashtra on 19 April 2013. The film had all the children characters played by the same artists who played in the first part.

==External links==
* 
* 
*  (online edition)
* 
*  (online edition)

==References==