Secret Scandal
{{Infobox film
| name           = Scandalo Segreto
| image          = Scandalo segreto monica vitti.jpg
| caption        =
| director       = Monica Vitti
| producer       = Roberto Russo
| writer         = Monica Vitti Gianfranco Clerici Roberto Russo
| starring       = Monica Vitti Elliott Gould Gino Pernice Catherine Spaak
| music          =
| cinematography = Luigi Kuveiller II
| editing        = Alberto Gallitti
| distributor    = Reteitalia Productions Academy Distribuzione
| released       = 1989
| runtime        =  85 minutes
| country        = Italy
| language       = Italian
| budget         =
| preceded_by    =
| followed_by    =
}} 1989 Cinema Italian film which was written and directed by Monica Vitti. It was starred by the same Monica Vitti, Elliott Gould, Gino Pernice, and Catherine Spaak. It was screened in the Un Certain Regard section at the 1990 Cannes Film Festival.   

For the movie, Monica Vitti debuted as a director of cinema nevertheless it would be her last appearance on the big screen.

==Synopsis==
On her birthday, Margherita (Monica Vitti) receives a video camera from a friend, who is a film director (Elliott Gould). She decides to use the device to keep a diary of her life. As the camera is capable of recording automatically, snippets usually feature her talking. However, on one occasion, Margherita leaves the camera in her bedroom as it was still recording; it catches Margheritas husband, Paolo (Gino Pernice), who has always had a distant demeanor to her, as he cheats on her with her best friend (Catherine Spaak). A secret scandal ensues in their separation with Paolos departure and Margherita going into a deep depression, even to the point of contemplating suicide. Her friend, the director who gave her the camera, examines the film, and concludes that it is interesting material for an actual movie.

==Cast==
* Monica Vitti
* Elliott Gould
* Gino Pernice
* Catherine Spaak
* Carmen Onorati
* Pietro De Vico
* Donatella Gambini
* Daniele Stroppa
* Gianni Olivieri
* Daniela Rindi

==Awards==
* 1990  

==References==
 

==External links==
*  at the Internet Movie Database|IMDB.

 
 
 
 
 


 