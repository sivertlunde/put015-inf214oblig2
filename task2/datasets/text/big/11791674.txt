Splendor (1989 film)
{{Infobox film
| name           = Splendor
| image          = Splendor (1989 film).jpg
| image_size     = 
| caption        = 
| director       = Ettore Scola
| producer       = 
| writer         = Ettore Scola
| narrator       = 
| starring       = Marcello Mastroianni Marina Vlady Massimo Troisi
| music          = Armando Trovajoli
| cinematography = Luciano Tovoli
| editing        = Francesco Malvestito
| distributor    = 
| released       = 
| runtime        = 110 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Italian drama film directed by  Ettore Scola.   

==Plot==
Jordan  (Marcello Mastroianni) owns a cinema in Italy, named Splendor. Chantal (Marina Vlady)  is  his wife and cashier. Luigi (Massimo Troisi)  is  the  projectionist. This film is about the cinema and the films that are shown on the screen. But people stop going to films...

==Cast==
* Marcello Mastroianni as Jordan
* Massimo Troisi as Luigi
* Marina Vlady as Chantal Duvivier
* Paolo Panelli as Mr. Paolo
* Pamela Villoresi as Eugenia
* Giacomo Piperno as Lo Fazio

==Awards==
* 1989 Cannes Film Festival Official Selection   
* Nastro dArgento Best Cinematography (Luciano Tovoli) 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 
 