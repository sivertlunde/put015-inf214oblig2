Chanakya Chandragupta
{{Infobox film
| name           = Chanakya Chandragupta
| image          = 
| image_size     =
| caption        =
| director       = N. T. Rama Rao
| producer       = 
| writer         = 
| narrator       =
| starring       = N.T. Rama Rao Akkineni Nageswara Rao Sivaji Ganesan Jayapradha S. Varalakshmi 
| music          = Pendyala Nageswara Rao
| cinematography = 
| editing        = 
| studio         = Ramakrishna Cine Studios
| distributor    =
| released       = 1977
| runtime        =
| country        = India Telugu
| budget         =
}}

Chanakya Chandragupta ( ) is a 1977 Telugu Historical film.

==Cast==
*N. T. Rama Rao as Chandragupta Maurya
*Akkineni Nageswara Rao as Chanakya
*Sivaji Ganesan as Alexander The Great 
*Kaikala Satyanarayana as Rakshasa Mantri
*Jaya Prada
*S. Varalakshmi

==Soundtrack==
* Chirunavvula Tholakarilo Sirimallela Chinukulalo (Lyrics: C. Narayana Reddy; Singers: P. Susheela and S.P.Balasubramanyam)
* Evaro Aa Chandrudevaro Aa Veera Chandrudu Evaro (Lyrics: C. Narayana Reddy; Singer: P. Susheela)
* Idhe Tholireyi (Lyrics: C. Narayana Reddy; Singer: P. Susheela)
* Okata Renda Thommidi - Nanduloo Aa Nanduloo (Lyrics: C. Narayana Reddy; Singer: S. Janaki)
* Sikandar Toone (Lyrics: C. Narayana Reddy; Singer: Vani Jayaram)
* Siri Siri Chinnoda (Lyrics: C. Narayana Reddy; Singer: Vani Jayaram)

==External links==
*  

 
 
 
 
 
 


 
 