Commando (1985 film)
{{Infobox film
| name = Commando
| image = Commandoposter.jpg
| caption = Theatrical release poster
| director = Mark L. Lester
| producer = Joel Silver
| screenplay = Steven E. de Souza Joseph Loeb III Matthew Weisman
| starring = {{Plainlist|
* Arnold Schwarzenegger
* Rae Dawn Chong
}}
| music = James Horner
| cinematography = Matthew F. Leonetti
| editing = Glenn Farr Mark Goldblatt John F. Link
| studio = Silver Pictures
| distributor = 20th Century Fox
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = $10 million 
| gross = $57.5 million 
}}
Commando is a 1985 American action film directed by Mark L. Lester, and starring Arnold Schwarzenegger and Rae Dawn Chong. The film was released in the United States on October 4, 1985. The film was shot in Los Angeles, California.
 Best Special Effects but lost to James Camerons Aliens (film)|Aliens. The films score was provided by James Horner. A critical success and commercial hit, Commando was the 7th highest grossing R rated movie of 1985 worldwide, and the 25th highest grossing overall. 

==Plot==
  as John Matrix, firing an M202A1 FLASH rocket launcher.]]
Retired Delta Force operative Colonel John Matrix is informed by his former superior Major General Franklin Kirby that all the other members of his unit have been killed by unknown mercenaries. The mercenaries, among them Bennett, an ex-member of Matrixs team fired for overt brutality in service, attack Matrix’s secluded mountain home and kidnap Matrix’s young daughter Jenny. While trying to intercept them, Matrix is also overpowered by the mercenaries.
 Val Verde. Arius, who was deposed by Matrix in the course of one of his missions, has chosen the colonel because the current president trusts him implicitly. With Jennys life on the line, Matrix reluctantly accepts the demand.

After boarding a plane to Val Verde, Matrix manages to kill his guard, Henriques, and jumps from the plane just as it is taking off. With approximately 11 hours time (the period of the flight), he sets out after another of Arius men, Sully. He then enlists the aid of an off-duty flight attendant named Cindy, and instructs her to follow Sully to a shopping mall. Cindy first assumes that Matrix is a maniac, but after seeing him desperately trying to get his hands on Sully, she has a change of heart and henceforth assists him in his endeavour. After a lengthy car chase, Matrix catches up with Sully whom he drops off a cliff to his death. Taking a motel key from Sullys jacket, Matrix tracks down and kills Cooke, a former Green Beret in Arius employ, and learns where Jenny is being held after searching Cookes car.

Matrix breaks into a surplus store to equip himself with military weapons, but the police arrive and Matrix is arrested. Cindy helps him escape with an RPG and, after commandeering a seaplane from a nearby marina controlled by Arius, Matrix and Cindy land the plane off the coast of Arius island hideout. Matrix instructs Cindy to contact General Kirby and then proceeds to Arius’ villa, where he embarks on a killing spree. Matrix stabs a few soldiers, dispatches tens of Arius men with his machine gun, fights a few of them from inside a shed using nothing more than a few gardening tools, grabs one of the dead soldiers weapons and proceeds to the main building, shooting every single man he encounters on the way. Matrix then faces Arius man-to-man, and even though Arius has the high ground, Matrix utilizes a military trick maneuver and shoots him dead.

Matrix locates Jenny in the basement of the villa, where she has fled, and has the final showdown with Bennett. After a fierce struggle, Matrix finally kills Bennett, impaling him with a steam pipe (after which Matrix utters his infamous "let off some steam" line). However, Kirby arrives with a military detachment and asks Matrix to rejoin the Special Forces Unit, but Matrix declines and departs the island aboard the seaplane with Jenny and Cindy.

==Cast==
* Arnold Schwarzenegger as Col. John Matrix
* Rae Dawn Chong as Cindy
* Dan Hedaya as Arius Vernon Wells as Bennett James Olson as Gen. Franklin Kirby
* David Patrick Kelly as Sully
* Alyssa Milano as Jenny Matrix
* Bill Duke as Cooke
* Bill Paxton as Interceptor
* Drew Snyder as Lawson
* Sharon Wyatt as Leslie
* Michael DeLano as Forrestal
* Bob Minor as Jackson
* Mike Adams as Harris
* Carlos Cervantes as Diaz
* Lenny Juliano as Soldier
* Charles Meshack as Henriques

==Production==

===Development=== Walter Hill was originally involved in the development process. 

The original concept was for an Israeli special forces–Mossad agent, who is sick of the continual death and destruction in the Middle East, leaves Israel and emigrates to the United States, where he is forced out of his self-imposed retirement after his daughter is kidnapped. This was modified and further adapted when Schwarzenegger was cast; some of the original dialogue can be heard in the deleted scenes when Matrix says he regrets his past actions.

===Filming=== Pacific coast Hearst Castle Benedict Canyon.

==Distribution==

===Marketing===
Diamond Toymakers released a line of  . Matrix now leads an elite special forces unit (which replaced his old deceased unit from the original film) called C-Team, made up of Spex, Blaster, and Chopper, against the forces of F.E.A.R., led by Psycho (who is based on the character of Bennett) and consisting of Lead-Head, Stalker, and Sawbones. There was an assortment of 4" figures, containing all of the above, a series of 8" figures, consisting of Matrix, Spex, Blaster, Psycho, Lead-Head, and Stalker. Chopper and Sawbones are absent. Finally, there was an 18" John Matrix that came with a pistol, an M16 rifle|M16, and a grenade.

===Home media=== region 1 anamorphic video theatrical trailer as an extra. DVDs released in other regions soon followed, some with anamorphic transfers, but the 2001 United Kingdom region 2 DVD was censored by the BBFC, with 12 seconds of cuts to an arm severing and closeups of the impaled Bennett. These cuts were brought over from the 1985 original theatrical release. However, a German master was used  for the UK DVD, meaning the film was cut even more than it should have been, leading to 56 seconds of cuts instead of the BBFCs 12 seconds. If the film had been resubmitted to the BBFC, it would be passed uncut under the BBFCs new, more liberal rules. This has proven to be the case as the BBFCs website indicates that both versions of the film (the U.S. theatrical cut and the unrated edition)  for the DVD were passed on June 11, 2007. With the unrated edition released, the film is in its entirety, a first for the UK.

On June 5, 2007, 20th Century Fox officially announced that a completely unedited and unrated directors cut of the film would be released on region 1 DVD on September 18, 2007. Through seamless branching, this disc not only features an unrated cut (which was claimed to run at 95 minutes, but is only 91 minutes, with 92 seconds of extra footage), but as a bonus, also contains the original 90 minute, R-rated US theatrical version. Aside from this, the DVD is a special edition, featuring an audio commentary from director Mark L. Lester (only on the theatrical cut), additional deleted scenes, a Pure Action featurette, a Let Off Some Steam featurette, and four photo galleries with over 150 photos. The transfer is anamorphically enhanced and features a Surround sound|5.1 audio mix.

In April 2008, the 90 minute theatrical version of the film was released to consumers on the high definition Blu-ray disc format.

==Reception==

===Box office===
Commando was a box office success grossing over $220 million against a $10 million budget.

===Critical response=== cult classic. 

==Soundtrack==
{{Infobox album  
| Name        = Commando
| Type        = Soundtrack
| Longtype    =
| Artist      = James Horner
| Cover       = COMMANDOST.jpg
| Released    = December, 2003
| Recorded    =
| Genre       = Soundtrack
| Length      = 43:21
| Label       = Varèse Sarabande
| Producer    =
| Last album  =
| This album  =
| Next album  =
}}
{{Album ratings
| rev1      = AllMusic
| rev1Score =    }}

A soundtrack album was released by Varèse Sarabande on December 2, 2003 as part of the labels CD Club and was limited to 3000 copies.  The score, composed by James Horner, is notable for its prominent use of steel drums.
 Power Station, Robert Palmers The Power Station.

===Track listing===
# "Prologue/Main Title" – 3:58
# "Ambush and Kidnapping" – 2:35
# "Captured" – 2:14
# "Surprise" – 8:19
# "Sully Runs" – 4:34
# "Moving Jenny" – 3:44
# "Matrix Breaks In" – 3:30
# "Infiltration, Showdown and Finale" – 14:32

La-La Land Records released a limited edition of James Horners score in August 2011. The release features approximately 62 minutes of music across 24 tracks.
{{Infobox album  
| Name        = Commando
| Type        = Soundtrack
| Longtype    =
| Artist      = James Horner
| Cover       =
| Released    = August, 2011
| Recorded    =
| Genre       = Soundtrack
| Length      = 61:48
| Label       = La-La Land Records
| Producer    =
| Last album  =
| This album  =
| Next album  =
}}

==Remake==
In 2008, a Russian remake (День Д)  was made. It was produced and directed by Mikhail Porechenkov (who stars as John Matrixs equivalent) and also features Barbara Porechenkova, Mikhail Trukhin, Alexandra Ursulyak, and Bob Schrijber. 
 Fox would begin filming a remake of the film with David Ayer in negotiations to direct.  Sam Worthington was attached to play the new lead role, which would reportedly have a more realistic approach. 

==References==
 

==External links==
 
 
* 
* 
* 
* 
*  at the Internet Movie Firearm Database

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 