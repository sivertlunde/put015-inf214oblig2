Congo in Four Acts
 
 
{{Infobox film
| name           = Congo in Four Acts
| image          = 
| caption        = 
| director       = Dieudo Hamadi Divita Wa Lusala Kiripi Katembo Siku
| producer       = Djo Tunda Wa Munga Steven Markovitz
| writer         = 
| starring       = 
| distributor    = 
| released       = 2010
| runtime        = 72 minutes
| country        = Democratic Republic of the Congo South Africa
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = Deschamps Matala Divita Wa Lusala
| editing        = Divita Wa Lusala Ronelle Loots Frédéric Massiot
| music          = 
}}

Congo in Four Acts is a 2010 documentary film.

== Synopsis ==
Initiated as an educational project to help young filmmakers develop their craft, Congo in Four Acts is a quartet of short films.     Ladies in Waiting chronicles the bureaucratic dysfunctions of a maternity ward from which women cannot leave unless they pay their fees. Symphony Kinshasa takes the viewer on a tour through Congo’s capital city where malaria is rife, electricity cables lie in the street and garbage is everywhere. Zero Tolerance deals with rape as a weapon of war in Eastern RDC and the attempts by authorities to re-establish the national moral code. After the Mine depicts life in Kipushi, a mining town where the soil is contaminated.

== Awards ==
* Cinéma du Réel 2010
* Africa Movie Academy Awards 2011

==See also==
*Kinshasa Symphony

== References ==
 
 

 
 
 
 
 
 
 
 


 
 