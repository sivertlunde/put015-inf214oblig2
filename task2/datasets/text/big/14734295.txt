Rumpus in the Harem
{{Infobox Film |
  | name           = Rumpus in the Harem
  | image          = Stooge rumpus53.jpg
  | image size     = 190px
  | caption        = 
  | director       = Jules White  Felix Adler Ruth Godfrey White Harriette Tarler Suzanne Ridgeway Johnny Kascier
  | cinematography = Ray Cory  Harold White
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 15 56"
  | country        = United States
  | language       = English
}}
Rumpus in the Harem is the 171st short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
 
The Stooges play the proprietors of the Cafe Casbah Bah, a Middle Eastern restaurant. One morning Moe and Larry are awakened by their crying sweethearts, who are in need of money to pay off a bad debt. While attempting to prepare a meal for customers Hassan Ben Sober (Vernon Dent) and Gin-A Rummy (George J. Lewis), the Stooges try to think of a way to raise the needed cash. In the interim, they discover a plan that their hungry customers are hatching. These two thieves are attempting to rob the tomb of Rootentooten, which contains a priceless diamond, but they discover that the Emir of Schmow (Johnny Kascier) has already gotten his hands on the diamond. The two plotters start wailing and are thrown out of the restaurant. The Stooges then attempt to retrieve the diamond themselves, as there is a $50,000 reward at stake.

The Stooges arrive at the Emir of Shmows palace, all three dressed as Santa Claus. They then manage to acquire the diamond and make a quick exit, but not before dealing with a burly guard.

==Production notes==
Rumpus in the Harem is a remake of 1949s Malice in the Palace, using ample stock footage.  It was the first of four shorts filmed in the wake of Shemp Howards death using earlier footage and a stand-in.

==="Fake Shemp"===
  article]]
  Hot Stuff, Scheming Schemers and Commotion on the Ocean), Columbia utilized supporting actor Joe Palma to be Shemps double. Even though the last four shorts were remakes of earlier Shemp efforts, Palmas services were needed to film new scenes in order to link existing stock footage.   

For Rumpus in the Harem, Palma is seen from the back several times. The first time occurs in the restaurant when Moe declares that the trio must do something to help their sweethearts. Larry then concludes the conversation by saying "Ive got it, Ive got it!" Moe inquires with "What?" Larry replies, "a terrific headache!"
  (far left) is seen doubling for the late Shemp Howard in Rumpus in the Harem. Harem girls Suzanne Ridgeway and Harriette Tarler dance for the boys]]
Later, Palma is seen from the back being chased in circles by the palace guard. A few lines of dialogue appear — "Whoa, Moe, Larry! Moe, help!" — by dubbing Shemps voice from the soundtracks of Fuelin Around and Blunder Boys. 

Palma is seen one final time, making a mad dash for the open window, and supplying his own yell before making the final jump. This was one of the few times during his tenure as Shemps double that Palma was required to speak without the aid of dubbing. 

==References==
 

== External links ==
*  
*  
* 
* 

 

 
 
 
 
 
 
 
 
 