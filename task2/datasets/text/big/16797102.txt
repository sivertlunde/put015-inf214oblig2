I've Got Your Number (film)
 Ive Got Your Number}}
{{Infobox film
| name           = Ive Got Your Number
| image          = IveGotYourNumber1934.JPG
| caption        = Joan Blondell and Pat OBrien William Rankin Warren Duff Sidney Sutherland Pat OBrien
| director       = Ray Enright
| producer       = 
| music          = 
| cinematography = Arthur L. Todd
| editing        = Clarence Kolster
| distributor    = Warner Bros.
| released       = February 3, 1934
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
|}} Pat OBrien.

==Plot== Pat OBrien) and John (Allen Jenkins) are called in to see if the phone was Wiretapping|tapped. When it is found not to be, Marie loses her job.

Terry is attracted to Marie and eventually talks her into a date. He also gets her hired by businessman John P. Schuyler (Henry ONeill), whom he had earlier saved from a live electrical wire. 

When Marie runs into Nicky later, she lets slip that her new employer is expecting a delivery of $90,000 in bonds. As a result, Nicky is able to fool the courier into thinking he is Schuyler and giving him the bonds while Marie is distracted by a flood of calls from his accomplices. When she realizes what has happened, she goes looking for Nicky, but this only serves to make her look guilty. Terry is questioned by the police and then released so he can lead them to her hiding place. It works and she is arrested.

When an expensive lawyer shows up on her behalf, Terry becomes suspicious and taps his line with Johns reluctant help. Finally, he is able to trace a call to where Nicky and his gang are hiding out. When he goes there, he is easily caught and placed in a bedroom after the phone is ripped out. However, he is not searched. He hooks up a spare phone he has and is able to contact John to bring help. The crooks are captured.

Terry and Marie get married, but on their wedding night, many of Terrys co-workers show up to "repair" their phone.

==Production==
 
While filming, Blondell had to have an emergency appendectomy. The last scene had to be shot at her home because she was not allowed to travel.

==Cast==
  
* Joan Blondell as Marie Lawson Pat OBrien as Terry Riley
* Glenda Farrell as Bonnie, aka "Madame Francis", a fake medium who uses a telephone to fool her victims into thinking they are speaking to their dead loved ones
* Allen Jenkins as John
* Eugene Pallette as Joseph Flood, Terrys exasperated boss
* Henry ONeill as John P. Schuyler
* Louise Beavers as Crystal, Bonnies accomplice
* Gordon Westcott as Nicky

== References ==
 
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 