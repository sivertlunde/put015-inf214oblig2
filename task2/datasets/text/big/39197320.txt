Hajde da se volimo (film series)
{{Infobox film name     = Hajde da se volimo image    = caption  = director = Aleksandar Đorđević  (1)   Stanko Crnobrnja  (2-3)  producer = Milan Božić  (1)   Radoslav Raka Đokic  (1-3)   Milija Gane Đokic  (2)  writer   = Jovan Marković  (1-2)   Radoslav Pavlović  (3)  starring = Dragomir "Gidra" Bojanić  Emir Hadžihafizbegović music    = Kornelije Kovač  (1)   Laza Ristovski  (2-3)  cinematography = Predrag Popović  (1)   Miloš Spasojević  (2-3)  editing     = Petar Marković  (1-2)   Milanka Nanović  (3)  distributor = released = 1987–90 runtime  = 87 mins.  (1)   95 mins.  (2)   92 mins.  (3)  country  Yugoslavia
|language = Serbo-Croatian budget   =
}}
 Yugoslavian musical comedy film series consisting of three feature-length films released between 1987 and 1990.

==Hajde da se volimo (1987)==
;Film release and plot Yugoslavia on musical comedy comedy starring Lepa Brena and her band Slatki Greh.

;Soundtrack Hajde da se volimo with her band Slatki Greh. The soundtrack album features ten original songs that were used throughout the film. The scenes in which the songs "Sanjam" (Dreaming),  "Hajde da se volimo" (Lets Fall in Love),  Udri, Mujo (Hit It, Muyo),  "Učenici" (Students),  "Golube" (Dove),  "Suze brišu sve" (Tears Erase Everything) and "Zbog tebe" (Because of You)  are played were used as their music videos. The film Hajde da se volimo begins with Brena singing "Evo, zima će" (Winters Coming).

Apart from the soundtrack, Serbian composer Kornelije Kovač composed additional music throughout the film.

;Filming locations
The movie was filmed across the former Yugoslavia, including:
*Dubrovnik, Croatia
*Užice, Serbia
*Kosjerić, Serbia
*Stari Most in Mostar, Bosnia and Herzegovina

;Cast
*Lepa Brena - Lepa Brena
*Bata Živojinović - Komandir Milanović Dragomir "Gidra" Bojanić - Sofer Gile
*Mima Karadžić - Menadžer Svetislav
*Svetislav Goncić - Bale
*Miodrag Andrić - Milicioner Milivoje Kole Angelovski - Milicioner Gaga
*Milan Štrljić - Sef bande
*Boro Stjepanović - Komandirov pomočnik
*Saša Popović - Saša
*Dušan Trifunčević - Dule
*Branislav Mijatović - Bane
*Ljubiša Marković - Ljubiša
*Živojin Matić - Zile
*Zoran Radanov - Zoki
*Danica Maksimović - Šefica recepcije
*Tatjana Pujin - Tanja
*Mihajlo Viktorović - Tanjin otac
*Mladen Nedeljković - Tanjin brat
*Radoslava Marinković - Službenica u hotelu
*Aleksandra Petković - Mlada seljanka
*Milan Srdoč - Čika Milija
*Predrag Milinković - Otmičar I
*Dragomir Stanojević - Otmičar II
*Žika Milenković - Poslovni tip I
*Nenad Ciganović - Poslovni tip II
*Božidar Pavičević-Longa - Poslovni tip III
*Bata Paskaljević - Upravnik hotela
*Zoran Stojiljković - Glavni gangster
*Đorđe Jovanović (actor)|Đorđe Jovanović - Šef mesne zajednice
*Nikola Milić - Deda Vukašin
*Ratko Sarić - Deda Radovan
*Dragomir Čumić

;Director Aleksandar Đorđević

;Writer
*Jovan Marković

;Executive producer
*Radoslav Raka Đokic

;Producer Milan Božić

==Hajde da se volimo 2 (1989)==
;Film release and plot
Hajde da se volimo 2 was released across Yugoslavia on Tuesday, 17 October 1989, nearly two years after the first film.

;Filming locations
Some of the scenes in the movie were filmed on the island of Lopud, which is one of the Elaphiti Islands.

;Cast

;Director
*Stanko Crnobrnja

;Writer
*Jovan Marković

;Executive producer
Milija Gane Đokic

;Producer
*Radoslav Raka Đokic

==Hajde da se volimo 3 (1990)==
;Film release
Hajde da se volimo 3 was released across Yugoslavia on Monday, 19 November 1990.

;Plot
The false story in the newspaper about the marriage of Lepa Brena to a wealthy Australian, they will try to take advantage of various types to check the news and to get a good salary and a bet that even some fairly well and get rich.

;Filming locations
The movie was filmed mostly in the different parts of Serbia and Montenegro:
*Budva, Montenegro
*Žabljak, Montenegro
*Belgrade, Serbia
*Tara (Drina)|Tara, a river in Montenegro
*Sveti Stefan, Montenegrin islet Crno jezero, Black Lake of Montenegro
*Kopaonik, mountain in Serbia
*Durmitor
*Tivat Airport, Montenegrin airport
*Vršac International Airport, Serbian airport

;Cast

;Director
*Stanko Crnobrnja

;Writer
*Radoslav Pavlović

;Executive producer
*Radoslav Raka Đokic

==Possible fourth film==
The possibility of a fourth film has been thrown around for years.  

Lepa Brena was quoted in January 2013 as saying about the fourth film: "To me its all a matter of willpower to create a story called Lets Fall in Love, Part 4. Im waiting to finish a lot of stuff to get started on that film. A movie costs between one million and two million to make. The funds should be provided, as well as good actors, a good script and good music. I can say for now I really have the desire to do it."  

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 