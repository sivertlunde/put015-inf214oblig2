Me Myself I (film)
{{Infobox film
| name = Me Myself I
| image = Me Myself I.jpg
| caption = Theatrical release poster
| director = Pip Karmel
| writer = Pip Karmel David Roberts Sandy Winton
| cinematography = Graham Lind
| editing = Denise Haratzis Charlie Chan
| released =   (US)   (Australia) Gaumont Australia
| producer = Andrena Finlay Fabien Liron Vicki Popplewell
| distributor = Sony Pictures Classics
| runtime = 104 minutes
| country = Australia
| language = English
}}
 

Me Myself I is a 2000 Australian comedy film. It was the first feature film by director Pip Karmel, and was released and reviewed internationally.

==Plot==
Pamela Drury is single and works as a serious journalist. She spends her birthday alone and becomes lonely and reflects upon her life and the choices she made and secretly wishes she had gotten married and had children. In a box of photos of old boyfriends, she reflects upon why she broke up with one in particular, Robert Dickson, 13 years earlier. She also meets an interesting man, Ben and follows him home, only to see through his window that he is with his family and looks very happy. Shortly afterwards, she is hit by a car while crossing the street.

The woman who was driving the car is also Pamela, but is Pamela Dickson; she is from an alternate universe in which she married Robert 13 years earlier. Pamela Dickson takes Pamela Drury to the Dickson family home and the two of them talk in the kitchen. Suddenly, Pamela Dicksons kids come home and she disappears, leaving the unmarried Pamela Drury in a house she has never seen before with three children she does not know. The children assume she is their mother, although they do not quite recognize her sometimes.

She soon finds out that her alternate version Pamela Dickson lives in a dull marriage and writes lightweight fluff articles for a mainstream ladies magazine, rather than being the serious reporter that Drury is. She meets Ben again, but in this time-line he was never married and still mourns the loss of the great love of his life, who was killed just before their graduation from college.

At first, Pamela Drury was pleased to be with Robert again after all these years apart, but she is soon unhappy and annoyed with married life, and quarrels with Robert. She embarks on an affair with Ben, not mentioning to him that she has a husband and kids (because she still doesnt think of herself as married or a mother). Ben visits her and learns the truth, and walks away angry and disappointed. Soon, Pamela Drury embraces having a family and falls for Robert again, and even stimulates him and enlivens her marriage. Then when Pamela Drury is in a restaurant bathroom, Pamela Dickson shows up again, and the two women switch back to their former lives. Pamela Dickson had been living the life of single Pamela Drury and enjoyed it but ultimately missed her husband and kids so she came back. Pamela Drury is single once more and embraces her life with a new appreciation of all that being single and having a career has to offer. She learns that while she was gone, Pamela Dickson began dating Ben, who actually is divorced from the woman she saw through the window, the same woman who alternate-Ben had thought was his soul mate. Ultimately she sees that both lives are appealing and offer a lot to appreciate.

==Cast==
*Rachel Griffiths as Pamela Drury / Pamela Dickson David Roberts as Robert Dickson
*Sandy Winton as Ben Griffin
*Yael Stone as Stacy Dickson
*Shaun Loseby as Douglas Dickson
*Trent Sullivan as Rupert Dickson
*Christine Stephen-Daly as Deirdre
*Felix Williamson as Geoff
*Rebecca Frith as Terri
*Ann Burbrook as Janinie
*Frank Whitten as Charlie
*Terence Crawford as Allen
*Maeliosa Stafford as Max
*Kirstie Hutton as Sally
*Peter Brailey as Roger
*Mariel McClorey as Harriet
*Andrew Caryofyllis as Harry
*Lenore Munro as Sophie
*Lyndon Wilkinson as Pamelas Mother (voice on phone) Spud as Brandy the dog

==Reception==

===Critical response===
The film was mainly seen in Australia, but was internationally distributed and widely reviewed.     Roger Ebert and other critics have emphasized Rachel Griffiths performance in the lead, but Andrew Sarris noted in The New York Observer, "In any event, Ms. Karmel, whether as erstwhile writer, editor, or maker of short films, has earned the right to a long and fruitful directorial career on the strength of Me Myself I, one of the most striking feature-film debuts ever."   

===Awards=== Best Screenplay, Best Actress Best Achievement in Editing. 

===Box office===
Me Myself I grossed $2,698,330 at the box office in Australia,  and about $370,000 in the U.S. 

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 