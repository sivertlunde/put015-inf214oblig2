Toto in the Moon
{{Infobox film
 | name = Totò nella luna
 | image =  Totò nella luna.jpg
 | caption = Steno
 | writer =  Steno   Lucio Fulci  Ettore Scola  Sandro Continenza
 | starring =  Totò  Sylva Koscina Ugo Tognazzi
 | music =  Alessandro Derevitsky
 | cinematography =  Marco Scarpelli
 | editing =  Giuliana Martelli
 | producer = Mario Cecchi Gori
 | distributor = Cecchi Gori Group
 | released = 1958
 | runtime = 95 min
 | awards =
 | country = Italy
 | language =  Italian
 | budget =
 }} 1958 Cinema Italian comedy film written and directed by Steno (director)|Steno.  

==Plot==
The boy Achilles Paoloni employed by Soubrette, a small publishing house of the knight Pasquale Belafronte, wrote a science fiction novel that he hopes in vain to publish with the help of the hostile knight.
U.S. scientists in the meantime are aware of the fact that Achilles has a substance in the blood suitable for spaceflight, the glumonio, inheritance of the unusual breast milk-based monkey when he was newborn. When two FBI are sent to the office to propose a space mission to Achilles, they think that they are representatives came to publishing his novel published overseas. The cavalier Pasquale, aware of it, goes back on years of insults and hostility to the poor Achilles and does everything to publish the novel at his own expense, even agreeing to the marriage between the young man and his daughter Lydia. Soon, however, he realizes that it was a mistake: the U.S. does not want to launch the rocket in space at all (the title of the novel by Achilles), but the young man himself, however, also disputed by a mysterious foreign power guided by the interplanetary scientist German Von Braut and the beautiful spy Tatiana.
The planes of the two rival powers are hampered by strange aliens (the Annelids) that send down two "cosoni", identical copies of Pasquale and Achilles in order for them to be shipped on the moon (a parody of Invasion of the Body Snatchers), this is to prevent the conquest of space most humans affect the peaceful balance between peoples aliens. Comic situations and various misunderstandings cause the true Paschal and "cosone" Achilles are found together on the moon. Pasquale will adapt to living in space only when extraterrestrials will transform the clone of Achilles into a beautiful girl.

== Cast ==
*Totò: Pasquale Belafronte
*Sylva Koscina: Lidia
*Ugo Tognazzi: Achille 
*Sandra Milo: Tatiana
*Luciano Salce: Von Braut  
*Giacomo Furia: commendator Santoni
*Jim Dolen: OConnor 
*Francesco Mulé: il vigile urbano 
*Marco Tulli: un creditore

==References==
 

==External links==
*  
 

 
 
 
 
 
 


 