Race for Your Life, Charlie Brown
 
{{Infobox film
| name           = Race for Your Life, Charlie Brown
| image          = Race for your life charlie brown movie poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Promotional poster
| director       = Bill Melendez Phil Roman
| producer       = Bill Melendez Lee Mendelson
| writer         = Charles M. Schulz
| starring       = Duncan Watson Stuart Brotman Liam Martin Gail Davis Melanie Kohn Jimmy Ahrens Kirk Jue Jordan Warren Tom Muller Greg Felton Bill Melendez Jackson Beck
| music          = Ed Bogas
| editing        = Roger Donley Chuck McCann
| studio         = United Feature Syndicate
| distributor    = Paramount Pictures
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
}} Camp Remote somewhere in the mountains where they ultimately compete in a river rafting race.

==Plot== Camp Remote somewhere in the mountains. Charlie Brown is accidentally left behind by the bus while at a desolate rest stop. He is then forced to hitch a harrowing ride on Snoopys motorcycle in order to make the rest of the journey to the camp, accompanied by rock guitar type riffs while he is shouting in fear at Snoopys wild driving.

Upon their arrival, the kids are immediately exposed to the regimentation and squalor of camp life which is a stark contrast to their comfortable residences back home. They are unfamiliar with the concept that the camp schedule is in military time (Franklin asks if "oh-five-hundred" is noon, and Sally thinks "eighteen-hundred" is a year). Although they do their best to adjust to the rigors of camp life, Snoopy, in a tent of his own, enjoys an ice cream sundae while watching TV on his portable set.

The gang must contend with a trio of ruthless bullies (and their cat, Brutus, vicious enough to intimidate even Snoopy) who openly boast of them having won the race every year they have competed. The only thing that keeps them at bay is Linus using his security blanket like a whip (which also gets him unwanted attention from Sally due to her praising the courage of her "Sweet Babboo"). It is revealed that their success has always been through outright cheating, using a raft equipped with an outboard motor, direction finder, radar, and sonar, and even resorting to every trick they could think of to hamper or destroy everyone elses chance to even make it to the finish line, much less win the race.
 vote of overrules the decision, to the disdain of the other girls. The bullies are overconfident; they use their cheating to burst ahead, but in their boasting they fail to watch where they are going and crash into a dock, which costs them a lot of time and effort to dislodge their boat while the others sail past.

The groups see many unique sights along the river race, such as mountains, forests, and a riparian logging community of houses built on docks. However, they also run into different obstacles: getting lost, stranded, storms, blizzards, and sabotage from the bullies. Snoopy abandons the race to search tirelessly for Woodstock when a storm separates them; after a long search, they manage to find each other and are joyfully reunited. Charlie Brown starts to grow more into his leadership role, becoming leader of both the boys and girls groups when the bullies sabotage both of their rafts, forcing them to work together.

Thanks to Charlie Browns growing self-confidence and leadership, the gang is about to win the race at the climax after overcoming considerable odds. Unfortunately, Patty incites the girls to celebrate too soon and they accidentally knock the boys overboard in their excitement; when they attempt to rescue them, the bullies seize the opportunity to pull ahead.

The bullies gloat about their apparently imminent victory. However, their brash over-confidence, infighting,  and constant carelessness during the race has seen them become involved in numerous mishaps, causing them to suffer substantial damage to their raft. Just shy of the finish line, their raft finally gives out and sinks. This leaves Snoopy and Woodstock as the only contenders left. Brutus slashes Snoopys inner tube with a claw, but Woodstock promptly builds a raft of twigs with a leaf for a sail and continues toward victory. When Brutus tries to attack Woodstock, Snoopy decks him, and Woodstock wins the race. Conceding defeat, the bullies begin to vow vengeance next year, but their threats are humiliatingly cut short when Snoopy hands Brutus a rough beating after he threatens Woodstock again.

As the gang boards the bus to depart for home, Charlie Brown decides aloud to use the experience as a lesson to be more confident and assertive, and to believe in himself. Unfortunately, right after he finishes speaking, the bus leaves without him for the second time, and as before, he is forced to hitch a ride with Snoopy again.

==Voice cast==
* Duncan Watson as Charlie Brown Woodstock
* Gail M. Davis as Sally Brown
* Melanie Kohn as Lucy van Pelt
* Liam Martin as Linus van Pelt
* Stuart Brotman as Peppermint Patty Marcie
* Schroeder
* Franklin
* Kirk Jue as Bully
* Jordan Warren as Bully
* Jackson Beck as Brutus the cat

==Music Crew==
* Music by: Ed Bogas
* Music Supervision by: Judy Munsen
* Additional Music by: Harper MacKay
* Orchestrations by: Milt Franklyn

==Reception==
Race for Your Life, Charlie Brown received a mixed review in The New York Times from Janet Maslin, who wrote: "The film runs an hour and quarter and has a rambling plot about a regatta, but it seems less like a continuous story than a series of droll blackout sketches, many of them ending with the obligatory Good Grief! ... The net effect is that of having read the comic strip for an unusually long spell, which can amount to either a delightful experience or a pleasant but slightly wearing one, depending upon the intensity of ones fascination with the basic Peanuts mystique."   

==Home video releases== the very first release in 1982 on RCAs defunct Capacitance Electronic Disc, also known as SelectaVision VideoDisc.  It was later reissued on VHS in 1995. 

The film aired in prime time on CBS on Nov. 3, 1979.   

The film aired on ABC Family in December, 2013.

Race for Your Life, Charlie Brown was released on DVD on Feb 10, 2015.  

==References==
 

==External links==
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 