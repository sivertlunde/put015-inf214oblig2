Rafter Romance
{{Infobox Film
| name           = Rafter Romance
| image          = Rafter Romance poster.jpg
| image size     = 225px
| caption        = theatrical release poster
| director       = William A. Seiter
| producer       = Merian C. Cooper Kenneth Macgowan Alexander McGaig (uncredited)
| writer         = John Wells (novel) Glenn Tryon (adaptation) H.W. Hanemann Sam Mintz Norman Foster George Sidney
| music          = Max Steiner David Abel
| editing        = James B. Morley RKO Radio Pictures
| released       = September 1, 1933
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Norman Foster Guinn Williams.

==Plot== Norman Foster) is an aspiring artist who lives in the same Greenwich Village building. Both are behind on their rent, and their landlord, Max Eckbaum (George Sidney), a good-natured soul who nevertheless has expenses to meet, comes up with a solution, to move Mary into Jacks loft, and have them share the apartment on a shift basis. They would never see each other or know who the other is, since Jack is out all night and sleeps during the day, and Mary is taking a job selling refrigerators by telephone, which keeps her out all day. 

However, both manage to get a very bad impression of each other, after realizing the other is of the opposite sex from articles of clothing lying about. A series of misunderstandings leads to a series of pranks aimed at each other. He places a bucket in the shower, and when she takes one it falls on her head. Then she places Jacks suit in the shower, so that it gets wet. In retaliation, he saws her bed in half so that it would come apart when she sits on it. 

The situation gets complicated when the couple accidentally meet outside their apartment, not knowing who the other is, and begin to fall in love. Matters get worse when Marys boss, lecherous H. Harrington Hubbell (Robert Benchley), tries to invite her out for dinner, and Jacks would-be "patron", a lonely, libidinous, rich older woman Elise Peabody Willington Smythe (Laura Hope Crews), tries to maintain her monopoly over Jack. 

When Jack accompanies Mary to a company picnic, they slip away from the group together and miss the bus back to town, forcing them to take a taxi. When they arrive at Jacks home, Mary realizes that Jack is her roommate. Trying to allay what he assumes are her suspicions about the arrangement, and unaware Mary is the person with whom he has been sharing the attic loft, Jack strongly denounces his co-tenant to her, until the landlord comes and explains all. 
 Guinn Williams), punches Hubbell, mistaking him for Jack. Realizing his mistake, Fritzie then goes to his cab where Jack is pleading with Mary. Fritzie is about to punch Jack when Mary intervenes, and the cab drives off with Jack and Mary kissing in the backseat. Asked if they will get married, the landlord says, "I arranged it."

==Cast==
* Ginger Rogers as Mary Carroll Norman Foster as Jack Bacon
* George Sidney as Max Eckbaum
* Robert Benchley as H. Harrington Hubbell
* Laura Hope Crews as Elise Peabody Whittington Smythe Guinn Williams as Fritzie Sidney Miller as Julius Eckbaum

==Production==
Production on Rafter Romance began in mid-June 1933.  RKO initially announced that   in 1930, and would also make Professional Sweetheart together later in 1933, also directed by William A. Seiter.  Rogers also began her partnership with Fred Astaire in 1933 on Flying Down to Rio, produced by Merian C. Cooper. 
 James Dunn and Whitney Bourne, also for RKO.  The plots of other films, including the German film Ich bei Tag und du bei Nacht (1932), the French film A Moi le jour, à toi nuit (1932) and the British Early to Bed (1933) have similarities to Rafter Romance, but are actually based on a completely separate screenplay by Robert Liebmann.   Also, Ernst Lubitschs The Shop Around the Corner (1939) starring James Stewart and Margaret Sullavan, is similar in some respects as well.

Merian C. Cooper accused RKO of not paying him all the money contractually due for the films he produced in the 1930s. A settlement was reached in 1946, giving Cooper complete ownership of six RKO titles: 
*Rafter Romance 
*Double Harness (1933) with Ann Harding and William Powell Robert Young
*One Mans Journey (1933) with Lionel Barrymore
*Living on Love (1937)
*A Man to Remember (1938)

According to an interview with a retired RKO executive, Cooper withdrew the films, only allowing them to be shown on television in 1955-1956 in New York City.  In 2007 the films were shown at the Film Forum in New York, and Turner Classic Movies, which had acquired the rights to the six films after extensive legal negotiations, broadcast them, their first full public exhibition in over 70 years. TCM, in association with the Library of Congress and the Brigham Young University Motion Picture Archive, had searched many film archives throughout the world to find copies of the films in order to create new 35mm prints.  Fristoe, Roger.     

==References==
Notes
 

==External links==
*  
*  
*  
*  


 

 
 
 
 
 
 
 
 
 
 