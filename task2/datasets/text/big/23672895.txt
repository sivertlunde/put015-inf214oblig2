Kambal sa Uma (TV series)
 

{{Infobox television
  | show_name = Kambal sa Uma
  | image =  
  | caption = Intertitle Romance
  | picture_format = NTSC (480i) SDTV
  | runtime = 30-45 minutes
  | creator = Jim Fernandez Ernie Santiago
  | developer = 
  | director = Manny Q. Palo Rechie A. Del Carmen
  | executive_producer = Carlina D. Dela Merced
  | creative_head = Rondel Lindayag
  | production_manager = Nini Matilac
  | creative_manager = Reggie Amigo
  | starring = Shaina Magdayao and Melissa Ricks
  | theme_music_composer = Vehnee Saturno
  | opentheme = "Kung Iniibig Ka Niya" by Laarni Lozada
  | endtheme = 
  | country = Philippines
  | location = Philippines English
  | network = ABS-CBN
  | company = Dreamscape Entertainment TV 
  | picture_format = 480i SDTV
  | first_aired =  
  | last_aired =  
  | num_episodes = 125
  | website = http://www.abs-cbn.com/Weekdays/article/3024/kambalsauma/Kambal-sa-Uma.aspx
  }}
Jim Fernandezs Kambal sa Uma (lit. Twins in the Farm) is a television series from ABS-CBN. It aired from April 20, 2009 to October 9, 2009 top rated drama afternoon show starring Shaina Magdayao and Melissa Ricks. It was replaced by Nagsimula sa Puso.

==Overview==

===Comics===
Kambal sa Uma is a comics creation by Jim fernandez and Ernie Santiago. Its tells a story of twin sister who both have different characteristics of a rat. Ella, have adapted the characteristics of the rats facial appearance while Vira only managed to gain the hairs on her back.

===1979 film===
Kambal sa Uma has been adapted into a film in the 1970s directed by Joey Gosiengfiao. It starred Rio Locsin who played both Ella and Vira; Rio Locsin now portrays Ella and Viras mother, Milagros in the TV remake. It also starred Al Tantay, Dennis Roldan, Orestes Ojeda, Julie Ann Fortich, and Isabel Rivas.

==Plot==
Based on the komiks creation of Jim Fernandez and Ernie Santiago and the 1970s film by Joey Gosiengfiao, Kambal Sa Uma tells the story of Ella (Melissa Ricks) and Vira (Shaina Magdayao), twin sisters who have some of the physical features of a rodent. When she was still a baby, Vira was given away by her mother Milagros (Rio Locsin) and ended up in the care of a rich couple in the city. Meanwhile, Ella remained with her mother and lived in the mountains away from persecution of people. Through a dramatic course of events, Ella and Vira will cross paths and their differences will lead to a bitter clash.

==Cast and characters==

===Main Cast===
* Shaina Magdayao as Vira Mae Ocampo / Marie Perea - grew up under the care of her rich adoptive parents. However, her foster father only sees her as a “lucky charm” for his business. Vira has rodent hairs on her back that she keeps as a secret. Vira is the twin sister of Ella. She finds corroboration for her familys innocence.
* Melissa Ricks as Ella Perea / Venus dela Riva - has physical features similar to a rat’s. She will live a secluded life in the mountains away from persecution. But she longs to see the life in the city where she hopes to find her missing twin sister Vira. She works as a janitor. She wears a black wig. She develops a crush on Gabriel but doesnt know that he is looking for the taong-daga. Ella her sister is the only one who witnessed Celeste poisoning her husband. After the plant explosion, she hides in Lolitas place. She becomes Venus after getting a new look, which involved plastic surgery from a friend of Lolitas. Ella and Venus are the same person. She studies business management in Switzerland and became the president of Lolita and Celestes cosmetic company. She took revenge on all the people who made her life miserable. She didnt tell them shes Ella until Dino comforted her after breaking up her engagement with Gab. She eventually tells Vira, after she saved her life from getting shot by Leon in a drive-by shooting.  *
* Matt Evans as Gabriel Ledesma - is the rich boy on a hunt for the taong-daga (Ella) whom he believes murdered his father. He did not know his father is poisoned by her own mother, Celeste. He will ignite the clash between his twins cousins: Ella and Vira. He develops a close bond with Ella which soon turns into a crush. He does not know that she is the taong-daga.  
* Jason Abalos as Dino San Jose - is Ellas childhood friend who acts as her brotherly figure. During his stay in the city, he meets Ellas twin, Vira, while on a stroll. He develops a crush on Vira, and he is Viras "knight in shining armor" because he always helps her when she gets in trouble. He works as an escort. He is the guy that Vira and Ynez will fight over. He protects Ella if anything could happen to her. He did not know Ella & Vira were twin sisters.

===Supporting Cast===
* Rio Locsin  as  Milagros Perea - is the mother of Ella and Vira. In a flashback, it was revealed that she and Celeste were friends. She really was Fernans true love rather than Celeste, but Milagros revealed that shes in love with Raul. This leads to an animosity between them. Milagros won numerous cases after Vira revealed the evidence that Celeste masterminded everything. She reunites her daughters: Ella and Vira. 
* Gina Alajar as  Celeste Miranda-Ledesma - is the series main antagonist. She and Milagros were formerly best friends until they met Fernan. She wants to kill Milagros twin daughters to seek vengeance. However, she accidentally poisoned Fernan due to jealousy. She is also behind the destruction of Aurelios factory, murdering of Ynez and Aurelio (at the hands of Leon). She is found guilty on numerous counts due to Viras evidence. She escaped with Leon and also escaped from the near-death explosion. In the end, she got arrested.
* Lotlot De Leon as Lourdes Ocampo† - foster mother of Vira and Ynez, sister-in-law of Celeste. Lourdes attitude contradicts her husband Aurelio. She shows sympathy toward her daughter and was the one who even suggested to shave Viras rodent hair in the first place. She died when she had a massive tumor in her heart while Vira was at her school for the coronation night of Campus Prettiest.
* Allan Paule as Aurelio Ocampo† - foster father of Vira, real father of Ynez and brother-in-law to Celeste. Aurelio and Lourdes attitude contrasts as the former believes that Viras rodent hairs are lucky and fortunate to them. Every time Vira opposes her father, he will brutally lock his daughter. He, along with Ynez, knew who caused the factory explosion. He was shot to death by Leon (assigned by Celeste).
* Carlo Guevarra as Benjie - is Leons former assistant at the circus who has sympathy for Ella while the latter performs on a circus. Eventually, he and Ella were working as janitors in Viras university. Bangs Garcia as Ynez Ocampo† - is the foster sister of Vira and daughter of Aurelio from another woman. Shes very jealous of Vira, for the latter was treated with more love than her. Shes usually the reason why Vira is always in trouble; she was killed by Leon and Celeste.
* Aldred Gatchalian as Emil Ledesma - Gabriels brother who doesnt know that his mom was a mastermind.

===Special Participation===
* Carmi Martin as Lolita dela Riva - is a successful businesswoman who, after a tragedy involving her daughter, left the social scene. After Ella saves her life, Lolita helps Ella start a clean slate in hoping that her mistakes with her daughter will be rectified.

===Extended Cast===
* Nonie Buencamino as Raul Perea Sr.† - husband of Milagros and father of Ella & Vira. He is Milagros true love rather than Fernan. He was attacked by the enraged townsfolk while he let Milagros and the twins escape. It was revealed that Celeste killed him.
*Jordan Herrera as Leon† - a circus owner who later became Ellas friend. What the latter did not know that she will be used as a performer and that Leon tricked Milagros into believing that Ella will be cured with the help of Leon. He escaped prison by Celeste to help out in her plans.  He and Celeste murder Ynez. He shot Vira on her right arm and murdered Aurelio.  Leon also helped Celeste to escape. He is shot by the police while fighting Dino.
*Bing Davao as Fernan† / Fernando Ledesma† - husband of Celeste; Milagros former lover. Fernan is a rich hacienda owner and father to Gabriel and Emil. It was revealed that he loved Milagros more than Celeste. Because of jealousy and hate, Celeste accidentally poisoned him.
*Jay-R Siaboc as Paco - friend of Dino that got him into being an escort. He helped Dino pay for his grandmas hospital bill and find a new place to live.
* Eva Darren as Lola Salve† - grandmother of Dino and foster mother of Milagros. She was accidentally killed by Leon as she shields Ella from certain death.
*Dianne Medina as Kat - Viras hard rival at school; she and Vira were longtime enemies for being captain at a cheering squad captain. She was kicked out of school because of an evidence shown by Vira. Kristina Snell as Nina - Viras best friend.
*Zia Marquez as Viras best friend. Sergio Garcia as Edward - Obsessed lover of Vira who Vira despises.
*Helga Krapf as Mary

===Guest Cast===
* Paul Salas as Young Dino
* Mark Joshua Salvador as Young Gabriel
* Angel Sy as Young Ella
* Nikki Bagaporo as Young Ynez
* Mika dela Cruz as Young Vira
* Joross Gamboa as Young Raul Perea
* Alessandra De Rossi as Young Milagros Perea
* Eda Nolan as Young Lola Salve
* Desiree Del Valle as Young Celeste Ledesma
* Cheska Billiones as Young Lourdes Ocampo
* Baron Geisler as Young Aureilo Ocampo

==Re-run==
Served as a noontime program, Kambal sa Uma re-aired from May 26, 2014 to September 19, 2014 at 11:45 AM on cable channel, Jeepney TV as part of the tagline block Pasada Pantasya.

==See also==
*List of programs broadcast by ABS-CBN
*List of shows previously aired by ABS-CBN
*List of dramas of ABS-CBN

==External links==

 

==References==
 

 
 
 

 
 
 
 
 
 
 