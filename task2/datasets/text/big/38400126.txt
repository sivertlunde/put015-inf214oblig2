The Queen of Moulin Rouge
{{Infobox film
| name           = The Queen of Moulin Rouge
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene 
| producer       = Theodor Bachrich
| writer         = Georges Feydeau (play)   Robert Wiene
| narrator       = 
| starring       = Mady Christians   André Roanne   Livio Pavanelli   Ly Josyanne
| music          = 
| editing        = 
| cinematography = Hans Androschin   Ludwig Schaschek 
| studio         = Pan Film
| distributor    = Filmhaus Bruckmann  (Germany)
| released       =  
| runtime        = 6 reels 
| country        = Austria Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} silent comedy film directed by Robert Wiene and starring Mady Christians, André Roanne and Livio Pavanelli. The film is based on the 1902 play La Duchesse des Folies-Bergères by Georges Feydeau. It was the final silent film Wiene made in Austria, before returning to Germany. It was made by the Austrian studio Pan Film, with backing from the French Pathé and German Filmhaus Bruckmann companies. 

==Synopsis==
A young Prince in Paris for his education, inherits the throne when his father abdicates. But a clause in the constitution states that the new King needs to be sworn in by noon the following day or he will forfeit the throne. As the new King has already gone out for a night of carousing in Paris, those in charge of him set out to search all the nightclubs to find him while a rival group of conspirators do everything they can to try and prevent him being informed. 

==Cast==
* Mady Christians as Die Herzogin 
* André Roanne as Sergius 
* Livio Pavanelli as Arnold 
* Ly Josyanne as Gräfin Slowikin 
* Karl Forest as Schuldirektor 
* Karl Günther as Graf Slowikin 
* Fritz Ley as Chopinet 
* Paul Ollivier as Herzog von Pitschenieff 
* Walter Varndal as Kirschbaum 
* Uly van Dayen as Floramye 
* Lee Goysanne   
* Paul Biensfeldt  

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 