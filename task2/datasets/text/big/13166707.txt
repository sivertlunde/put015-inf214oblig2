They Wait
{{Infobox Film
| name           = They Wait
| image          = They Wait.jpg
| caption        = Official film poster
| starring       = Terry Chen Jaime King Regan Oey Cheng Pei-pei Henry O Colin Foo Michael Biehn Yee Jee Tso
| director       = Ernie Barbarash
| producer       = Diane Boehme Uwe Boll Stephen Hegyes Andrew Koster Jonathan Shore Shawn Williamson
| writer         = Trevor Markwart (story) Trevor Markwart Carl Bessai Doug Taylor
| music          = Hal Beckett
| cinematography = Gregory Middleton
| editing        = Lisa Robison
| distributor    = Brightlight Pictures
| released       = 2007
| runtime        = 89 minutes
| country        = Canada
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2007 Cinema Canadian horror film.    It stars Jaime King as a mother attempting to find the truth and save her son, Regan Oey, when threatened by spirits during the Chinese tradition of Ghost Month. The other leading star is Chinese Canadian actor Terry Chen, who plays her husband. It was both filmed, and set, in the city of Vancouver, in British Columbia in Canada, and was featured at the 2007 Toronto Film Festival.

==Plot==
 
Married couple Sarah (Jaime King|King) and Jason (Terry Chen|Chen), and son Sammy (Regan Oey|Oey), travel to Vancouver for the funeral of Uncle Raymond (Foo). During this time, Sammy begins to see ghosts and falls gravely ill, his illness coinciding with the Chinese festival of Ghost Month. After traditional western medicine fails to help Sammy, Sarah turns to a mysterious pharmacist who tells her that her son is held in a death grip by a living corpse. Sarah now must find what the spirits want before the last day of Ghost Month, or Sammy will be lost forever.

==Cast==
*Terry Chen ... Jason
*Jaime King ... Sarah
*Regan Oey ... Sammy
*Cheng Pei-pei ... Aunt Mei
*Henry O ... Pharmacist
*Colin Foo ... Raymond
*Chang Tseng ... Xiang
*Vicky Huang ... Shen
*Michael Biehn ... Blake
*Donald Fong ... Ben
*Wally Houn ... Pang
*Stephen M.D. Chang ... Funeral Director
*Donny Lucas ... Sams Doctor
*Suzanne Bastien ... Nurse #1
*Erika Conway ... Nurse #2
*Grace Fatkin ... Receptionist
*Joseph May ... Paramedic
*Yee Jee Tso ... Pharmacy Store Clerk
*Paul Wu ... Young Raymond
*Maggie Ma ... Young Mei
*Nelson Wong ... Young Ben
*Vincent Tong ... Young Xiang
*Igor Ingelsman ... Worker

==Production==
They Wait was filmed on location in Vancouver in March 2007.     

==Reception==
 
Critical reviews for the film have been mixed, with the film currently holding a 40% "rotten" rating on Rotten Tomatoes. Joe Leydon of Variety wrote that "Director Ernie Barbarash makes judicious use of CGI trickery -- in one key scene, he cleverly shocks his audience into laughing -- but at heart, hes an old-school traditionalist when it comes to scary stuff."   The Toronto Star and ReelFilm both panned the film, with ReelFilms David Nusair saying that They Wait is "...a tedious and downright silly piece of work."  

==References==
 

==External links==
*  
*  
*   at the 2007 Toronto International Film Festival
*  

 

 
 
 
 
 
 