Crackerjack (1938 film)
{{Infobox film
| name = Crackerjack
| image =
| image_size =
| caption =
| director = Albert de Courville Edward Black
| writer = William Blair Morton Ferguson (novel)   A.R. Rawlinson   Basil Mason   Michael Pertwee
| narrator =
| starring = Tom Walls   Lilli Palmer   Noel Madison   Leon M. Lion
| music = Louis Levy
| cinematography = Jack E. Cox
| editing = R. Wilson
| studio = Gainsborough Pictures
| distributor = General Film Distributors 
| released = October 1938
| runtime = 76 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} comedy crime film directed by Albert de Courville and starring Tom Walls, Lilli Palmer and Noel Madison. It was made at Pinewood Studios  with sets designed by Walter Murton.

==Cast==
* Tom Walls as Jack Drake 
* Lilli Palmer as Baroness Von Haltz  
* Noel Madison as Sculpie  
* Leon M. Lion as Hambro Golding 
* Edmund Breon as Tony Davenport Jack Lester as Boyne  
* Charles Heslop as Burdge  
* Henry Hugh Gordon Stoker|H.G. Stoker as Supt. Benting   Henry B. Longhurst as Insp. Lunt  
* Ethel Griffies as Annie 
* Edmund DAlby as Lug  
* Muriel George as Mrs. Humbold 
* Andreas Malandrinos as Ducet  
* Fewlass Llewellyn as Weller  
* Hal Walters as Smithy  
* Burton Pierce as Dancer  
* Hugh Dempster as Wally Astell  
* Michael Shepley as Wally Astill

==References==
 

==Bibliography==
* Wood, Linda. British Films 1927-1939. BFI, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 