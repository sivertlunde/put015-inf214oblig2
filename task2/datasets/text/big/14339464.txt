Luke and Lucy: The Texas Rangers
{{Infobox film
| name           = Luke and Lucy: The Texas Rangers
| image          = De Texas Rakkers.jpg
| alt            =  
| caption        = Poster
| director       = Mark Mertens Wim Bien
| producer       = Eric Wirix
| writer         = Dirk Nielandt Guy Mortier Eric Wirix
| starring       = Dutch version voices: Frank Lammers Jeroen van Koningsbrugge Pierre Bokma Kees Boot Raymonde de Kuyper Marijn Klaver Nanette Drazic
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Bridge Entertainment Group
| released       =  
| runtime        = 86 minutes
| country        = Belgium Netherlands Luxembourg
| language       = Dutch French
| budget         = 
| gross          = 
}} CGI animated western comedy film released on 21 July 2009 as the first of it kind to be created in Belgium in a projected 13 animated films, at a rate of one per year. The film is based on the Belgian comic book characters Luke and Lucy (published in English as Spike and Suzy and Willy and Wanda).        
The film is directed by Mark Mertens and Wim Bien, and produced by Skyline Entertainment, in partnership with CoToon, LuxAnimation, BosBros, and WAT Productions.      

The film was first announced in a 1 July 2005 press release. The Flanders Audiovisual Fund announced on 20 April 2006 that it would provide €12,500 for script development, and a further €237,500 was announced in September 2007 for production of the film.  The total budget of the film is €9 million,  making it the most expensive Flemish-Belgian film to date.

Character voices for the Flemish version are being provided by Staf Coppens (Suske), Evelien Verhegge (Wiske), Lucas Van Den Eynde (Lambik), Sien Eggers (Sidonie) and Filip Peeters (Jerom).  Character voices for the Dutch version are being provided by Frank Lammers, Jeroen van Koningsbrugge, Pierre Bokma, Kees Boot, Raymonde de Kuyper, Marijn Klaver, and Nanette Drazic.     James Keller (Ranger Tom), Dave Dreisin (Theodore & Theophil Boomerang), Carlos Alayeto (Manuel), (Paul Homza) (Rick), (Scott Genn) (Bill Buster), and Barry Tarallo (Jules).

==Plot==
Luke and Lucy find out that a masked villain called JP has kidnapped and shrunk all the Texas Rangers. They leave for Dark City, Texas with their friends and settle in the Texas Rangers HQ. It quickly becomes clear that they are not welcome. JP terrorizes the city and has spies everywhere. The saloon dancer Miss Missy decides to join Luke and Lucy in their fight against JP, but Aunt Sybil doesnt trust her and suspects that she is secretly in cahoots with JP. Is she jealous of Ambrose (Frank Lammers) and Wilburs (Kees Boot) interest in Miss Missy?

Luke and Lucy dont trust the Sheriff or the grocer, Theodore, who acts suspiciously. After a confrontation with JP, Luke and Lucy realize that the Sheriffs cast is fake, and they discover that a closet in Miss Missys hotel room is filled with weapons. Who is the real culprit?

==Voice cast==
*Frank Lammers as Lambik (Ambrose)
*Kees Boot as Jerom (Wilbur)
*Jeroen van Koningsbrugge as Theofiel and Theodore
*Pierre Bokma as Professor Barabas
*Raymonde de Kuyper as Aunt Sidonia (Aunt Sybil)
*Marijn Klaver as Suske (Luke)
*Nanette Drazic as Wiske (Lucy)

==See also==
*List of animated feature-length films
*List of computer-animated films
*Spike and Suzy

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 