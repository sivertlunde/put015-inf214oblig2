Mitrudu
 
{{Infobox film
| name           = Mitrudu
| image          = Mithrudu_poster.jpg
| alt            =  
| caption        =
| writer         = M. Ratnam  
| story          = K. V. Vijayendra Prasad
| screenplay     = Mahadev 
| producer       = Sivalanka Krishna Prasad 
| director       = Mahadev
| starring       = Nandamuri Balakrishna Priyamani
| music          = Mani Sharma
| cinematography = B Balamurugan
| editing        = Kola Bhaskar
| studio         = Vaishavi Cinima   
| released       =  
| runtime        = 2:34:26
| country        =   India
| language       = Telugu
| budget         =
| gross          = 
}}

Mitrudu ( , Family Entertainer film produced by Sivalanka Krishna Prasad  on Vaishavi Cinima banner, directed by Mahadev. Starring Nandamuri Balakrishna, Priyamani in the lead roles and music composed by Mani Sharma.  The film recorded as flop at box office.      
 
==Synopsis==
Aditya (Nandamuri Balakrishna) is a loner who is depressed and he stays in Malaysia. Indu (Priyamani) is the daughter of a billionaire businessman and she studies in Malaysia. Her astrologer back home tells her the attributes of her ideal choice and those attributes meet that of Aditya’s. After a few failed attempts, Aditya accepts the proposal of Indu and gets married to her. The rest of the story is all about why Aditya is depressed and is about the real reason behind Indu chasing Aditya and marrying him

==Cast==
{{columns-list|3|
* Nandamuri Balakrishna as Aditya
* Priyamani as Indu Pradeep Rawat Deepak
* Brahmanandam as JB Jan / Jana Bethedu Janardhan
* Dharmavarapu Subramanyam
* Mannava Balayya|M. Balayya Ranganath
* Chandra Mohan
* Ahuti Prasad 
* Chalapathi Rao  Rallapalli 
* Krishna Bhagawan 
* Raghu Babu 
* Srinivasa Reddy 
* Giridhar 
* Rama Chandar
* Vasu Inturi
* Sana 
* Pragathi 
* Surekha Vani  Hema 
* Rajitha
* Srilalitha as Takhali
* Kuchuri Rachana Moiurya  
* Sandya Jalak
}}

==Soundtrack==
{{Infobox album
| Name        = Mitrudu
| Tagline     = 
| Type        = film
| Artist      = Mani Sarma
| Cover       = Mithrudu_audio.jpg
| Released    = 2 April 2009 
| Recorded    = 
| Genre       = Soundtrack
| Length      = 26:36
| Label       = Lahari Music
| Producer    = Mani Sarma
| Reviews     =
| Last album  = Billa (2009 film)|Billa   (2009)  
| This album  = Mitrudu   (2009)
| Next album  = Ek Niranjan   (2009)
}}

Music composed by Mani Sarma. Music released on Lahari Music Company. The music was released on 2 April 2009 directly into market. The audio rights of the soundtrack were purchased by Lahari Music. The audio function of the movie is called off as Nandamuri Balakrishna was busy campaigning for elections on behalf of Telugu Desam Party|TDP.

{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 26:36
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Thellavarithey
| lyrics1 = Vennelakanti
| extra1  = Rahul Nambiar,Hemachandra,Bhargavi
| length1 = 4:58

| title2  = By Birthe
| lyrics2 = Anantha Sriram
| extra2  = Shreya Ghosal
| length2 = 4:00

| title3  = Dont Touch Me
| lyrics3 = Anantha Sriram Chitra
| length3 = 4:43

| title4  = Jhummandi Vallantha
| lyrics4 = Oruganti Sujatha
| length4 = 3:59

| title5  = Priyamani
| lyrics5 = Veturi Sundararama Murthy SP Balu,Saindhavi
| length5 = 4:24

| title6  = Akasam Nunchi  
| lyrics6 = Vennelakanti   Kousalya
| length6 = 4:30
}}

==Box office==
The film met with Negative reviews from critics and was declared as flop at the box office.

==Others== Hyderabad

==References==
 

==External links==
*  

 

 
 
 