Hollywood Don't Surf!
{{Infobox film
| name           = Hollywood Dont Surf!
| image          = Hollywood Dont Surf! poster.jpg
| image size     =
| alt            =
| caption        = Poster art
| director       =  
| producer       =  
| writer         = Sam George
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        = Dale Beldin & Justin Krohn
| studio         = MacGillivray Freeman Films
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Hollywood Dont Surf! is a 2011 documentary film that premiered in the 2010 Cannes Film Festival while a work in progress  and held its North American premiere at the 2011 Telluride Film Festival, where it was presented to a packed park at the Abel Gance Outdoor theater by actress Daryl Hannah. 

Narrated by Robert Englund, the film explores the purportedly strained relationship between Hollywood film makers and the surfing community. After an irreverent and wild main title sequence (including the first printed and filmed images of surfing), Hollywood Dont Surf! examines the legendary film Gidget and the subsequent surf exploitation genre that followed, centering on the ambitious John Milius written and directed 1978 Warner Bros. film Big Wednesday, a commercial disappointment at the time, and the smattering of surf films that followed as Big Wednesday enjoyed a subsequent rebirth as a cult film on home video.

The documentary features:
* Quentin Tarantino
* Steven Spielberg
* John Milius
* Jan-Michael Vincent
* Robert Englund
* Stacy Peralta
* Gary Busey
* Lee Purcell
* Frankie Avalon John Stockwell
* Nia Peeples
* William Katt
* Laird Hamilton
* Greg Noll
* Kathy Kohner-Zuckerman
* Rick Dano
* Holly Beck

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 
 