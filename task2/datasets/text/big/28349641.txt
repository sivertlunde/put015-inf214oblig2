The Winter Stallion
The Winter Stallion is a 1992 Welsh drama film starring Daniel J. Travanti; it is also known as The Christmas Stallion.

==Plot==
Dai Davies is a Welshman running a cash-strapped farm in modern Wales and raising his orphaned granddaughter Gwen with the help of her godmother Nerys. When he dies unexpectedly, he leaves Gwens guardianship to his estranged son Alan who returned to Wales accompanied by his stepson Cliff Dean. His return pits him against land developer Howard, and Cliff against Gwens would-be suitor Gwilyn. As Alan and Gwen try to connect in the background of readying the farms prize stallion Mabon for a race that could save the farm, Councilor Howard resorts to dirty tricks to try and force through the farms sale.

==External links==
*  

 
 
 
 
 