Casta diva (film)
 
{{Infobox film
| name           = Casta Diva
| image          =
| image_size     =
| caption        =
| director       = Carmine Gallone
| producer       = Giuseppe Amato William Szekeley
| writer         = Walter Reisch
| narrator       =
| starring       = Sandra Palmieri Gualtiero Tumiati Achille Majeroni
| music          =
| cinematography = Franz Planer Massimo Terzano
| editing        = Fernando Tropea
| distributor    = Produzioni Atlas Consorziate (P.A.C.)
| released       = 14 June 1935 - France 10 August 1935 - Italy 19 September 1935 - Denmark 2 December 1935 - Portugal 4 October 1937 - USA
| runtime        = 87 minutes
| country        = Italy
| language       = Italian
| budget         =
| gross          =
}}
 Italian musical drama film, directed by Carmine Gallone. The film won Best Italian Film at the 1935 Venice International Film Festival. An English-language remake The Divine Spark was made, also directed by Gallone and starring Eggerth.

==Plot==
The film tells the passionate love story of a Gallican priestess and a Roman proconsul (governor of a province).  Film is unique because it uses abstract paintings-in-motion to express the passion between the two main characters.

==Cast==
* Mártha Eggerth ... Maddelena Fumarol
* Sandro Palmieri ... Vincenzo Bellini
* Gualtiero Tumiati ... Niccolò Paganini
* Lamberto Picasso ... Fumaroli
* Achille Majeroni ... Gioacchino Rossini

==External links==
 

 
 
 
 
 
 
 
 
 
 


 
 