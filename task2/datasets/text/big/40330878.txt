Unakkaga Piranthen
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Unakkaga Piranthen
| image          = Unakkaga_Piranthen.jpg
| image_size     =
| caption        =
| director       = Balu Anand
| producer       = Tirupur Mani
| writer         = Balu Anand
| starring       =   Deva
| cinematography = Jayanan Vincent
| editing        = M. N. Raja
| distributor    = Vivakananda Pictures
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 Prashanth and Mohini (Tamil actress)|Mohini. The music was composed by Deva (music director), while cinematography was handled by Jayanan Vincent. It was produced and distributed by Tirupur Mani under Vivekananda Pictures banner.

== Plot ==

Malini  (Mohini), is a Sri Lankan refugee, who has taken shelter in a refugee camp in India. When a camp guard tries to molest her, a local man called Raja (Prashanth) comes to her rescue. They start meeting frequently and soon fall in love. When refugees are ordered back to Sri Lanka, Malini goes. Unable to bear the separation, Raja decides to swim to Sri Lanka, but on reaching shore he is arrested by coastal guards who think he is a terrorist. Raja manages to escape from captivity and find Malini.

== Cast ==
 Prashanth as Raja Mohini as Malini Sangeetha
* Janagaraj

== Soundtrack ==

{{Infobox album |  
| Name        = Unakkaga Piranthen
| Type        = soundtrack Deva
| Cover       =
| Released    = 1992
| Recorded    = 1992 Feature film soundtrack |
| Length      =
| Label       = Deva
| Reviews     =
}} Vaali and Kamakodiyan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-
| 1 || Chikidan Chikidan || S. P. Balasubrahmanyam, Sujatha Mohan || 3:48
|-
| 2 || Iruthaiam Idham || S. P. Balasubrahmanyam || 5:16
|-
| 3 || Kaalai Malarnthathu || K. J. Yesudas || 4:34
|- Chithra || 4:02
|}

== References ==
 

 

 
 
 
 
 
 
 

 