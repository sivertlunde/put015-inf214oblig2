Sneham Oru Pravaaham
{{Infobox film 
| name           = Sneham Oru Pravaaham
| image          =
| caption        =
| director       = Dr Shajahan
| producer       =
| writer         = Dr Shajahan Latheesh Kumar (dialogues)
| screenplay     = Dr Shajahan Roopa Silk Smitha
| music          = KJ Joy
| cinematography = Vipin Das
| editing        = K Sankunni
| studio         = Deepthivarsha Films
| distributor    = Deepthivarsha Films
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, Roopa and Silk Smitha in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Jagathy Sreekumar
*Sukumaran Roopa
*Silk Smitha Vincent

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Dr Shajahan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aazhiyodennum || K. J. Yesudas, Chorus || Dr Shajahan || 
|-
| 2 || Malarmizhi nee madhumozhi nee || K. J. Yesudas || Dr Shajahan || 
|-
| 3 || Manikkinaakkal || Vani Jairam || Dr Shajahan || 
|-
| 4 || Nilaavil nee varoo || K. J. Yesudas || Dr Shajahan || 
|}

==References==
 

==External links==
*  

 
 
 

 