Casa d'appuntamento
{{Infobox film
| name           = The French Sex Murders
| image          = Casa dappuntamento poster.jpg
| alt            = 
| director       = Ferdinando Merighi  (as F.L. Morris) 
| producer       = Marius Mattei Dick Randall
| writer         = Paolo Daniele Marius Mattei Ferdinando Merighi Dick Randall  (as Robert Oliver) 
| starring       = Anita Ekberg Rosalba Neri Evelyne Kraft  (as Evelyn Kraft)  Howard Vernon Pietro Martellanza
| music          = Bruno Nicolai
| cinematography = Mario Mancini Gunter Otto
| editing        = Bruno Mattei
| studio         = Costantino International Films Gopa-Film
| distributor    = Filmagentur Graf Mondo Macabro 
| released       = 16 June 1972  (Italy) 
| runtime        = 83 min.  (West Germany) 
| country        = Italy West Germany
| language       = Italian
}}

Casa dappuntamento (Translation: The House of Rendezvous) is a 1972 giallo film directed by Ferdinando Merighi under the pseudonym "F. L. Morris".  It was released as The French Sex Murders in the United States.

== Critical reception ==

Allmovie gave it a mixed review, writing "The contrived script   is completely off the hook, which fans of the giallo form will be expecting, but those who come to the film cold may be somewhat nonplussed." 

== References ==

 

== External links ==

*  
*  

 
 
 


 