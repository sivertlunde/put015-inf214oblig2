Roger Dodger (film)
{{Infobox film
| name = Roger Dodger
| image = Poster of the movie Roger Dodger.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Dylan Kidd
| producer = Dylan Kidd Anne Chaisson Campbell Scott
| writer = Dylan Kidd
| starring = {{Plain list | 
* Campbell Scott
* Jesse Eisenberg
* Isabella Rossellini
* Elizabeth Berkley
* Jennifer Beals
}}
| music = Craig Wedren
| cinematography = Joaquin Baca-Asay
| editing = Andy Keir
| studio = Holedigger Films
| distributor = Artisan Entertainment
| released =  
| runtime = 106 minutes
| country = United States
| language = English German
| budget = 
| gross = $1,934,497 
}}
Roger Dodger is a 2002 American comedy-drama that explores the relationship between men, women, and sex. Directed by Dylan Kidd and starring Campbell Scott and Jesse Eisenberg, the film follows Roger Swanson (Scott) and his nephew (Eisenberg) during a night on the town in search of sex.

==Plot==
After cynical New York advertising copywriter Roger Swanson (Campbell Scott) is dumped by his on-again/off-again girlfriend, Joyce (Isabella Rossellini) — who is also his boss — his painful workday is further complicated by the unexpected arrival of his 16-year-old nephew, Nick (Jesse Eisenberg). After asking to spend the night at Rogers, Nick reveals that he has come to ask for help—in hopes of ditching his virginal status, Nick begs Roger for a lesson in the art of seduction. Embittered Roger then takes on the role of a nocturnal drill sergeant in an imaginary war between the sexes, starting Nicks training at an upscale singles bar. There they meet two beautiful women (Jennifer Beals and Elizabeth Berkley) who turn out to be less malleable than Roger expects.

Although this first attempt to seduce women is unsuccessful, Nick chooses to continue the quest, which takes them to a  party at Joyces. There they find Joyces secretary drunk and attempt to capitalize. Once in the bedroom Nicks conscience gets the better of him and he allows her to fall asleep untouched.

With Roger spinning out of control and Nicks window of opportunity closing rapidly, they agree to go with the "Fail Safe" plan. This turns out to be an underground brothel. At the underground location Roger finds he cannot let Nick lose his virginity in such an emotionally barren atmosphere, and drags him back to his apartment to sleep things off. Roger has failed to introduce his nephew to the mysteries of the world, but has perhaps gained a glimmer of a conscience. Nick travels back to Ohio but Roger shows up unexpectedly to tutor Nick and his classmates on their home turf, bonding with the younger men in a more potent way in an atmosphere populated by adolescent peers.

At the closing, it is left open which way Nick will go.

==Cast==
* Campbell Scott as Roger Swanson
* Jesse Eisenberg as Nick
* Isabella Rossellini as Joyce
* Elizabeth Berkley as Andrea
* Jennifer Beals as Sophie
* Ben Shenkman as Donovan
* Mina Badie as Donna
* Chris Stack as Chris

==Reception==
The film was not a major commercial success, but was critically well-received, winning multiple awards in 2002–03. It also served as the coming-out for Jesse Eisenberg, who was widely acclaimed for his conflicted performance.

Review aggregation website Rotten Tomatoes gives the film a score of 88% based on 122 reviews; the sites consensus states "The movie could have benefited from a more experienced director, but a great cast and script overcome any first time jitters the director may have had."  On Metacritic, the film had an average score of 75 out of 100, based on 33 reviews. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 