Zander the Great
{{Infobox film
| name           = Zander the Great
| image          =
| image size     =
| caption        =
| director       = George W. Hill
| producer       =
| writer         = Frances Marion
| based on       =  
| starring       = Marion Davies Emily Fitzroy Hedda Hopper
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 80 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 silent drama film directed by George W. Hill, in his first directing role for MGM. The film stars Marion Davies. The screenplay by Frances Marion is based upon Edward Salisbury Field 1923 play. 

==Synopsis== bootlegger who rescues them from bandits.

==Cast==
* Marion Davies - Mamie Smith
* Holbrook Blinn - Juan Fernández Harrison Ford - Dan Murchinson
* Harry Watson - Good News
* Harry Myers - Texas
* George Siegmann - Black Bart
* Emily Fitzroy - The Matron
* Hobart Bosworth - The Sheriff
* Hedda Hopper - Mrs. Caldwell
* Jack Huff - Zander

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 