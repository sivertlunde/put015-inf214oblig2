Night Call Nurses
{{Infobox film
| name           = Night Call Nurses
| image          = Night_Call_Nurses.jpg
| image_size     =
| caption        =
| director       = Jonathan Kaplan
| producer       = Julie Corman
| writer         = George Armitage Danny Opatoshu (uncredited)
| narrator       =
| starring       = Patty Byrne Alana Hamilton
| music          =
| cinematography =
| editing        =
| studio         = New World Pictures
| distributor    =
| released       =  
| runtime        = 78 min.
| country        = United States
| language       = English
| budget         = $75,000 
| gross          = $1 million Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 42 
}}
Night Call Nurses is a 1972 film directed by Jonathan Kaplan. It is the third in Roger Cormans "nurses" cycle of films, starting with The Student Nurses (1970).

==Plot==
Three young nurses work in a psych ward at a hospital. Barbara (Patty Byrne) comes under the influence of a charismatic sex therapist and is stalked by a mysterious nurse. Janis (Alana Hamilton) has an affair with a truck-driving patient who is addicted to drugs. Sandra (Mittie Lawrence) becomes politicised through an affair with a black militant and helps a prisoner escape from the hospital.

==Cast==
*Patty Byrne as Barbara
*Alana Hamilton as Janis
*Mittie Lawrence as Sandra
*Clint Kimbrough as Dr. Bramlett
*Felton Perry as Jude
*Stack Pierce as Sampson
*Richard Young	 as Kyle Toby
*Martin Ashe as Bathrobe Benny
*Bob Brogan as George
*Tristram Coffin as Miles Bailey
*Dennis Dugan as Kit
*Lynne Guthrie as Cynthia
*Bobby Hall as Warden Kelley
*Barbara Keene as Chloe
*Christopher Law as Zach
*Dick Miller as driver

==Production==
Corman offered the film to Kaplan on the recommendation of Martin Scorsese, who had recently made Boxcar Bertha for Corman and had taught Kaplan at New York University. Kaplans student film Stanley had just won a prize at the National Student Film Festival and he was working as an editor in New York.

Corman allowed Kaplan to rewrite the script, cast and edit the film. Kaplan says the only lead member of the cast selected when he came on board was then-model Alana Hamilton. Kaplan:
 Id never seen a Nurses movie. He   laid out the formula. I had to find a role for Dick Miller, show a Bulova watch, and use a Jensen automobile in the film. And he explained that there would be three nurses: a blonde, a brunette, and a nurse of colour; that the nurse of color would be involved in a political subplot, the brunette would be involved in the kinky subplot, and the blonde would be the comedy subplot. The last thing he said was "There will be nudity from the waist up, total nudity from behind, and no pubic hair - now get to work!" Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 124   
Kaplan brought out Jon Davison and Danny Opatoshu from New York to help him work on the script.

==Production==
The film was shot in 15 days for $75,000 and was a big hit, launching Kaplans directing career.   at Trailers From Hell 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 