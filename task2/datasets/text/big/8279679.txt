Ghost of Zorro
 
{{Infobox film
| name           = Ghost of Zorro
| image          = Ghostzorro.JPG
| director       = Fred C. Brannon
| producer       = Franklin Adreon
| writer         = Royal K Cole William Lively Sol Shor
| starring       = Clayton Moore Pamela Blake Roy Barcroft George J. Lewis Eugene Roth Stanley Wilson
| cinematography = John MacBurnie
| distributor    = Republic Pictures
| released       = 24 March 1949 (U.S. serial) {{Cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 110–111
 | chapter =
 }}  30 June 1959 (U.S. feature) 
| runtime         = 12 chapters / 167 minutes (serial)  69 minutes (feature) 
| language        = English
| country         = United States
| budget          = $165,086 (negative cost: $164,895) 
| awards          =
}} Republic Movie serial.  It uses substantial stock footage from earlier serials, including Son of Zorro and Daredevils of the West. This movie was shot in Chatsworth, Los Angeles.

==Plot==
Its 1865 and the telegraph is heading west. George Crane, wanting to keep law and order out of his territory, is out to stop the construction. One of the main engineers on the job is Ken Mason, the grandson of the original Zorro. As Crane hires his men to stop the work, Mason finds himself in the legendary role his ancestor originated.

==Cast==
* Clayton Moore as Ken Mason/Zorro
* Pamela Blake as Rita White
* Roy Barcroft as Hank Kilgore
* George J. Lewis as Moccasin
* Eugene Roth as George Crane

==Production==
Ghost of Zorro was budgeted at $165,086 although the final negative cost was $164,895 (a $191, or 0.1 percent, under-spend). 

It was filmed between 11 January and 2 February 1949.   The serials production number was 1702. 

===Stunts=== Tom Steele as Ken Mason/Zorro (doubling Clayton Moore)

===Special Effects===
Special effects by the Lydecker brothers.

==Release==

===Theatrical===
Ghost of Zorros official release date is 24 March 1949, although this is actually the date the sixth chapter was made available to film exchanges. 

A 69-minute feature film version, created by editing the serial footage together, was released on 30 June 1959.  It was one of fourteen feature films Republic made from their serials. 

==Chapter titles==
# Bandit Territory (20min)
# Forged Orders (13min 20s)
# Robbers Agent (13min 20s)
# Victims of Vengeance (13min 20s)
# Gun Trap (13min 20s)
# Deadline at Midnight (13min 20s)
# Tower of Disaster (13min 20s)
# Mob Justice (13min 20s)
# Money Lure (13min 20s)
# Message of Death (13min 20s) - a clipshow|re-cap chapter
# Runaway Stagecoach (13min 20s)
# Trail of Blood (13min 20s)
 Source:   {{Cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 248–249
 | chapter = Filmography
 }} 

==See also==
* List of film serials by year
* List of film serials by studio
* Earlier Republic Zorro serials:-
** Zorro Rides Again (1937)
** Zorros Fighting Legion (1939)
** Zorros Black Whip (1944)
** Son of Zorro (1947)

==References==
 

==External links==
* 

 
{{Succession box Republic Serial Serial
| before=Federal Agents vs. Underworld, Inc (1948)
| years=Ghost of Zorro (1949)
| after=King of the Rocket Men (1949)}}
{{Succession box Serial
| before=Son of Zorro (1947)
| years=Ghost of Zorro (1949)
| after=none}}
 

 
 

 
 
 
 
 
 
 
 
 
 
 