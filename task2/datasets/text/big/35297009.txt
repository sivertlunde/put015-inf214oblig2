Christmas Grace
{{Infobox film
| name = Christmas Grace
| image = Christmas Grace movie poster.jpg
| caption = Teaser poster
| director = Keith Perna
| producer = Daniel Knudsen Keith Perna
| writer = Keith Perna
| starring = RyanIver Klann Tim Kaiser Christy Storey Rebekah Cook Rich Swingle
| cinematography = Timothy Jones
| music = Samuel Joshua
| editing = David Peterson
| studio = Bright Horizon Pictures Crystal Creek Media
| distributor =
| released = September 23, 2014
| runtime = 84 minutes
| country = United States
| language = English
| budget =
| gross =
}}
Christmas Grace is an upcoming Christmas-related film about two competing toy stores. It was produced by Bright Horizon Pictures and Crystal Creek Media, the latter of which created films such as Creed of Gold and Rather to be Chosen. Filming of Christmas Grace took place around several locations in Southeastern Michigan in early 2012. The films release date is set for September 23, 2014.

== Plot ==
Christmas Grace tells the story of two rival toy store owners competing for business over several Christmas seasons. One of them is Gary (RyanIver Klann), a young toy store owner who runs an honest business and who tries to maintain a good reputation with his customers. Things are running smoothly for Gary until his business is threatened when a much larger toy company moves to the neighborhood. The owner of this larger company, Mr. Tollman (Tim Kaiser), is a ruthless businessman who voraciously wants to grow his business and eliminate competition. Since his most immediate competition is Garys store, he sets his target on him. As the story unfolds, it becomes very clear that God is at work in the lives of these two men, and Gods grace and providence work out in ways neither of them could have imagined.

== Cast ==
* RyanIver Klann as Gary
* Tim Kaiser as Jim Tollman
* Rebekah Cook as Tollman’s secretary, Michelle

Christy Storey and Rich Swingle also appear in the film.

== Production == Berkley and Grosse Pointe.   
 Highland Charter Township was used in the film. Its employees were cast as extras. 

A teaser trailer was released by Crystal Creek Media on March 24, 2012,  and a full trailer was unveiled on July 27, 2012.

== Release ==
Christmas Grace premiered December 5, 2013 to a nearly sold out auditorium in Royal Oak, Michigan  and will be released on DVD September 23, 2014.

== Awards ==
Christmas Grace won a Stellae Award for best "4 to 14" film at the 6th annual Pan Pacific Film Festival in Los Angeles.  The film was also a finalist for "Best Feature Film" in the 2014 Christian Worldview Film Festival. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 