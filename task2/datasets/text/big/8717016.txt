Alambrado
{{Infobox film  name           = Alambrado image          = Alambrado film poster.jpg caption        = Theatrical Poster director       = Marco Bechis producer       = Roberto Cicutto Diana Frey Oscar Kramer writer         = Marco Bechis Lara Fremder starring       = Jacqueline Lustig Martin Kalwill  Arturo Maly music          = Jacques Lederlin cinematography = Esteban Courtalon editing        = Nino Baragli distributor    =  released       = August 1991 runtime        = 90 minutes country        = Argentina Italy language       = English budget         = 
|}} Argentine and Italian film written and directed by Marco Bechis. 

The film starred Jacqueline Lustig, Martin Kalwill, and Arturo Maly.
 
==Plot==

The southernmost tip of Patagonia, in Argentina near to the Straits of Magellan, is an interesting place, with some of the roughest seas in the world. 
 British corporation has purchased property to build an airport.
 Irish rancher who wont sell his land.

In order to fight the proposed plans he fences in his land and at the same time fences in his two late-teenaged children.

Eva Logan (Jacqueline Lustig) is particularly frustrated at this event as it prevents any romantic options she has.

==Cast==
*Jacqueline Lustig as Eva Logan
*Martin Kalwill as Juan Loga
*Arturo Maly as Harvey Logan  Matthew Marsh as Wilson
*Miguel Ángel Paludi as Father Corti Luis Romero as Clerk
*Gabriel Molinelli as Mechanic Enrique Piñeyro as Policeman

==Exhibition==
The film was first presented at the Locarno Film Festival, Switzerland in August 1991.

==Reception==
Wins
* Havana Film Festival: Grand Coral - Third Prize, Marco Bechis; 1993.
* Imagi Madrid Film Festival - First Prize, President of the Jury Pedro Almodóvar; 1993.
* Sundance Film Festival - Park City, USA 1994.

Nominations
* Locarno International Film Festival: Golden Leopard, Marco Bechis; 1991.

==References==
 

==External links==
*   at the cinenacional.com  .
*  
*  

 

 
 
 
 
 
 
 
 
 
 