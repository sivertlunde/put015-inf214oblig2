Black Magic (1949 film)
{{Infobox film
| name           = Black Magic
| image          = Black Magic poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Gregory Ratoff Orson Welles (uncredited)	
| producer       = Gregory Ratoff
| based on       =  	 Charles Bennett
| starring       = Orson Welles Nancy Guild Akim Tamiroff
| music          = Paul Sawtell
| cinematography = Ubaldo Arata Anchise Brizzi
| editing        = Fred R. Feitshans Jr. James C. McKay
| distributor    = United Artists
| released       =   
| runtime        = 105 mins.
| country        = United States Italy
| language       = English
| budget         =
| gross          =
}} magician and scam artist Count Cagliostro) and Nancy Guild as Lorenza/Marie Antoinette.  Akim Tamiroff has a featured role.

== Plot ==
 Alexander Dumas, flashbacks we gypsy boy fame and prestige. After changing his name to Count Cagliostro, he becomes famous all over Europe. Things begin to go downhill when he enters a plot to substitute a young girl called Lorenza (Nancy Guild) for the French queen Marie Antoinette along with gypsies Gitano (Akim Tamiroff) and Zoraida (Valentina Cortese).

== Cast  ==
*Orson Welles as Joseph Balsamo aka Count Cagliostro
*Nancy Guild as Marie Antoinette / Lorenza
*Akim Tamiroff as Gitano
*Frank Latimore as Gilbert de Rezel
*Valentina Cortese as Zoraida
*Margot Grahame as Mme. du Barry
*Stephen Bekassy as Viscount de Montagne
*Berry Kroeger as Alexandre Dumas, Sr.
*Gregory Gaye as Chambord / Monk
*Raymond Burr as Alexandre Dumas, Jr.
*Charles Goldner as Dr. Franz Anton Mesmer
*Lee Kresel as King Louis XVI / Innkeeper Robert Atkins as King Louis XV
*Nicholas Bruce as De Remy
*Franco Corsaro as Chico
*Annielo Mele as Joseph Balsamo, as a child Ronald Adam as Court President
*Bruce Belfrage as Crown Prosecutor
*Alexander Danaroff as Dr. Duval / Baron von Minden
*Leonardo Scavino as Gaston / Beniamino Balsamo
*Tamara Shayne as Maria Balsamo
*Joop van Hulzen as Minister of Justice
*Peter Trent as Dr. Mesmers Friend
*Giuseppe Varni as Boehmer
*Tatyana Pavlova as The mother

==Production==
The movie was originally known as Cagliostro. Producer Edward Small went through a number of directors and stars in trying to get this film off the ground, starting in 1943. DRAMA AND FILM: Ann Blyth Adolescent Song Joust Contender College Professor Battling Illiterates; Cagliostro Once Again Coming to Life
Schallert, Edwin. Los Angeles Times (1923-Current File)   14 June 1943: 14.   Charles Boyer was to star with Akim Tamiroff, and Irving Pichel directing, then in early 1944 J. Carrol Naish was reported to play Alexandre Dumas, Sr.  Later that year, George Sanders was announced as the star with Douglas Sirk directing.  Andy Russell Touted as Mexicos Sinatra: Bing Crosby Will Fill Guest-Star Spot in Filmization of Duffys Tavern
Schallert, Edwin. Los Angeles Times (1923-Current File)   01 Sep 1944: 10.   Louis Hayward was also at one stage announced to star. HOLLYWOOD HITS JAPS: East and West Old Londontown VARIED HOLLYWOOD ACTIVITIES One Down, More to Go Title Problem
By FRED STANLEY. New York Times (1923-Current file)   11 Feb 1945: X1 

In 1943 Hedda Hopper suggested Orson Welles should play the lead role. Looking at Hollywood....
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   02 July 1943: 17  He signed in September 1947. CAGLIOSTRO LEAD TO ORSON WELLES: Small Signs Actor for Film to Be Made in Italy With an American Cast
By THOMAS F. BRADYSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   22 Sep 1947: 28.  

In 1947, Small was to make the film in Mexico, but dropped those plans when it turned out to be more expensive to shoot there than he expected &ndash; so the location of shooting was changed to Italy, where Small could use blocked lira. BURLESQUE AGAIN TO BE MADE A FILM: Fox Purchases Screen Rights to 1928 Play -- Jessel Named to Produce New Version
By THOMAS F. BRADYSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   06 Sep 1947: 11.   Small borrowed Gregory Ratoff and Nancy Guild from Twentieth Century-Fox and took over the Scalera studios in Rome from early October 1947 to late January 1948. Europe Becomes Merely Location to Hollywood
Schallert, Edwin. Los Angeles Times (1923-Current File)   08 Feb 1948: D1.   HISTORIC SET: Poor Benefit by Use of Roman Church in Film
Rome. New York Times (1923-Current file)   16 Jan 1949: X4 

Orson Welles said Small ("no mean master of suggestion, by the way") approached him "very cleverly with the role of Cagliostro. He waited til I had reread the Dumas novels and become so "hypnotised" by the scoundrel that I felt I had to play him. Then Small announced casually, Gregory Ratoff is going to direct. That cinched it. Gregory is a great friend, and more fun to work with than anybody I know." OUT OF A TRANCE
By ORSON WELLES. New York Times (1923-Current file)   17 Apr 1949: X4.  

Welles allegedly directed several scenes in the film, which was released on 18 August 1949. 

==Reception==
Reviews were mixed. All Over Town
T. M. P.. New York Times (1923-Current file)   09 Nov 1949: 37.   Orson Welles Pulls Mass Hypnosis Act on Us All
Scheuer, Philip K. Los Angeles Times (1923-Current File)   19 Aug 1949: B5.  

The 2006 film Fade to Black (2006 film)|Fade to Black has Welles (Danny Huston) involved in a murder mystery while in Rome for the production of Black Magic.

==References==
 

== External links ==
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 