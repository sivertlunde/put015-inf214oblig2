The Toxic Avenger Part III: The Last Temptation of Toxie
 
{{Infobox film
| name           = The Toxic Avenger Part III: The Last Temptation of Toxie
| image          = Toxie3.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster Michael Herz
| producer       = Lloyd Kaufman Michael Herz
| writer         = Lloyd Kaufman Michael Herz Lisa Gaye
| cinematography = James London
| editing        = Michael Schweitzer
| studio         = Troma Entertainment
| distributor    = Troma Entertainment
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $500,000
| gross          = $363,561
}} superhero comedy The Toxic Avenger.

==Plot==
 
After presumably defeating Apocalypse Inc., the Toxic Avenger has nothing to do. He tries to get a job but fails, as a normal job is no place for a hideously deformed creature of superhuman size and strength. Until one day, Toxie is told that his blind girlfriend Claire has a chance to see again, but it will cost a great deal of money. When the famous superhero gets the opportunity to work as a spokesman for Apocalypse Inc., he agrees so he can get money for Claire. As he was unaware of the evil nature of his employers, Apocalypse Inc. took over Tromaville and enslaved the populace. After Claires surgery, she opens up Toxies eyes and it is revealed that the Devil himself is the chairman of Apocalypse Inc. Things begin to make a change for the worse as the Toxic Avenger will be transformed back to his original form, the dorky Melvin Junko, and must face a showdown with the Devil. The Toxic Avenger defeats the Devil through the "Five Levels of Doom" trial ordeal, defeating Apocalypse Inc. for good.

==Cast==
* Ron Fazio as The Toxic Avenger / Apocalypse Inc. Executive
** John Altamura as Voice of The Toxic Avenger
* Phoebe Legere as Claire
* Rick Collins as Apocalypse Inc. Chairman/The Devil Lisa Gaye as Malfaire
* Jessica Dublin as Mrs. Junko
* Michael J. Kaplan as Little Melvin
* Dan Snow as Cigar Face
* Paul Borghese as Lou Sipher, Apocalypse Inc. executive Michael White, Susan Whitty, and Jeremiah Yates as Apocalypse Inc. executives

==Reception==
The film gained many negative reviews. It is the least remembered of the franchise and Rotten Tomatoes currently has a Want-To-See score of 78% as of January 2014.

==Home media==
The film was later re-released as part of the Tox Box DVD set and the Complete Toxic Avenger 7-disc DVD set.

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 