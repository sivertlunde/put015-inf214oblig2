You Never Can Tell (1951 film)
 
{{Infobox film
| name           = You Never Can Tell
| image          =
| caption        =
| director       = Lou Breslow
| producer       = Leonard Goldstein
| writer         = Lou Breslow (story) David Chandler (screenplay)
| starring       = Dick Powell Peggy Dow
| music          = Hans J. Salter
| cinematography = Maury Gertsman Frank Gross
| distributor    = Universal Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
}}
 American black and white comedy film, directed by Lou Breslow.

==Plot==
An ex-police dog named Rex inherits a fortune from his eccentric millionaire owner. But someone poisons him for his fortune, and he is able to go back to earth as a human detective to bring his killer to justice.

==Cast==
* Dick Powell ... Rex Shepherd
* Peggy Dow ... Ellen Hathaway
* Joyce Holden ... Golden Harvest
* Charles Drake ... Perry Collins
* Lou Polan ... Police Sergeant Novak Frank Nelson ... Police Lieutenant Gilpin
* Frank Gerstle ... Detective

==References==
 
 

==External links==
* 
* 

 
 
 
 
 
 
 


 