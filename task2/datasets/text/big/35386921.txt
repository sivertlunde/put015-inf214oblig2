All Rounder (1984 film)
{{Infobox film
| name           = All Rounder
| image          = AllRounder1984film.png
| image_size     = 
| border         = 
| alt            = 
| caption        = Promotional Poster
| director       = Mohan Kumar
| producer       = Mohan Kumar
| writer         = Mahendra Dehlvi
| screenplay     = Mohan Kumar
| story          = 
| based on       =  
| narrator       = 
| starring       = Kumar Gaurav Rati Agnihotri Vinod Mehra Shakti Kapoor
| music          = Laxmikant-Pyarelal
| cinematography = K.K. Mahajan
| editing        = Pratap Bhatt
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed and produced by Mohan Kumar. It stars Kumar Gaurav and Rati Agnihotri in pivotal roles. 

==Cast==
* Kumar Gaurav...Ajay Kumar
* Rati Agnihotri...Ritu 
* Vinod Mehra...Birju
* Ramesh Deo...Cricket Boardperson 
* Shakti Kapoor...Vikram
* Shubha Khote...Moushiji 
* Ashalata Wabgaonkar...Ritus aunt
* Raju Desai...Young Birju 
* Sujit Kumar...Ritus dad
* Ashalata Wabgaonkar...Ritus mom (as Ashalata)
* Yunus Parvez...Cricket Board Chair 
* Anuradha Patel...Vikrams Girlfriend 
* Shakti Kapoor...Vikram
* Shubha Khote...Moushiji 

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jaan Jab Jab"
| Shabbir Kumar, Lata Mangeshkar
|-
| 2
| "O Re Babuaa" Jaspal Singh
|-
| 3
| "Mere Kurte Mein"
| Suresh Wadkar, Alka Yagnik
|-
| 4
| "Panch Unglion Ki"
| Amit Kumar, Mahendra Kapoor, Anuradha Paudwal, Alka Yagnik
|-
| 5
| "Hum Kaun Hain"
| Suresh Wadkar, Asha Bhosle
|}

==References==
 

==External links==
* 

 
 
 
 

 