The Sky Is Falling (1979 film)
{{Infobox film
| name           = The Sky is Falling
| image          = Bloodbath_Flores_Del_Vicio_1979.png
| alt            = 
| caption        = 
| director       = Silvio Narizzano
| producer       =Andrés Vicente Gómez Francisco Vives
| writer         = Gonzalo Suárez
| starring       = Dennis Hopper Carroll Baker Win Wells Richard Todd Faith Brook
| music          = José Nieto (composer)|José Nieto
| editing        = Nicholas Wentworth	
| studio         =  
| distributor    = Cineteca S.A. 
| released       =  
| runtime        = 90 minutes
| country        = Spain
| language       = English Spanish
}}
The Sky Is Falling, also known as Las Flores Del Vicio (  thriller film directed by Silvio Narizzano and written by Gonzalo Suarez. It stars Dennis Hopper, Carroll Baker, and Win Wells as hopeless American expatriates living in a Spanish village where citizens are dying mysterious deaths after the arrival of a religious cult. The film has been characterized as an example of the frenetic roles played by Hopper in the 1970s. 

==Plot==
A drug-addicted poet named Chicken (Hopper), struggling to separate reality from fantasy, lives in a small Spanish village with other expatriates. These include a washed up alcoholic actress named Treasure (Carroll Baker), a retired British Air Corps captain and his alcoholic wife, and a jaded homosexual man (Win Wells). 

Chicken struggles with his addiction while having vivid hallucinations about his religious mother. Treasure is always waiting for a call from Hollywood in order to stage a comeback, and spends her time showing off her album of publicity photos. The expatriates, bored by their life in the village, encounter a group of young hippies who they feel a bond toward, but throughout the film each of the expatriates end up dead in various, disturbing ways. These deaths are juxtaposed with the life of the villagers in bizarre and surreal ways that develops a sense of menace in the film. It is implied that the deaths are caused by the band of hippies evoking a religious cult or the Manson family. 

==Cast==
*Dennis Hopper as Chicken
*Carroll Baker as Treasure
*Richard Todd as Terence
*Faith Brook as Heather
*Win Wells as Allen
*David Carpenter as Salt
*Alibe as Susannah
*Inma de Santis as Buenaventura
*Curtis Moseley as Young Chicken
*Ana Pastor as Pucherita

==Production==
This movie was filmed in Mojacar, Almeria, Spain in 1979, a small seaside village of local Spaniards, British and American expatriates, some of whom were featured in this movie. One of the extras, a young boy who was trampled at the end of the movie, looking towards the sky, was an American living in Spain whose parents ran a local saloon (El Saloon) and had befriended the director and many of the cast members.
 giallo and horror films in Italian productions.

==Notes==
 
== References ==
*  
*  
*  

 
 
 
 
 
 
 
 
 