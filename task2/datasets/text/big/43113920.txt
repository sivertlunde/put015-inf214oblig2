The Testimony (film)
{{Infobox film
| name =   The Testimony 
| image =
| image_size =
| caption =
| director = Pietro Germi
| producer = 
| writer =  Diego Fabbri    Cesare Zavattini    Enrico Ribulsi   Ottavio Alessi   Pietro Germi
| narrator =
| starring = Roldano Lupi   Marina Berti   Ernesto Almirante   Sandro Ruffini
| music = Enzo Masetti  
| cinematography = Aldo Tonti 
| editing = Gisa Radicchi Levi 
| studio =    Orbis Film 
| distributor = CEIAD
| released =    
| runtime = 98 minutes
| country = Italy
| language = Italian
| budget =
| gross =
}}
The Testimony (Italian:Il testimone) is 1946 Italian crime film directed by Pietro Germi and starring Roldano Lupi, Marina Berti and Ernesto Almirante. The film was made at the Cines Studios in Rome. It is one of several films regarded as an antecedent of the later giallo thrillers. 

==Cast==
* Roldano Lupi as Pietro Scotti  
* Marina Berti as Linda 
* Ernesto Almirante as Giuseppe Marchi, il testimone  
* Sandro Ruffini as Lavvocato difensore  
* Cesare Fantoni as Il padrone dell osteria  
* Arnoldo Foà as Limpiegato dell anagrafe  
* Dino Maronetto as Andrea, il condanato 
* Marcella Melnati as La padrone di casa  
* Alfredo Salvatori 
* Petr Sharov as Il pubblico ministero 
* Pietro Fumelli 
* Angelo Calabrese 
* Vittorio Cottafavi
* Giovanni Petrucci

== References ==
 

== Bibliography ==
* Moliterno, Gino. A to Z of Italian Cinema. Scarecrow Press, 2009.

== External links ==
* 
 

 
 
 
 
 
 
 
 

 