Notting Hill (film)
 
 
{{Infobox film
| name           = Notting Hill
| image          = NottingHillRobertsGrant.jpg
| border         = yes
| caption        = Theatrical release poster
| alt            = A poster with a large picture of a woman shaded blue on it is stuck to a wall. A man walks in front of it.
| director       = Roger Michell
| producer       = Duncan Kenworthy
| writer         = Richard Curtis
| starring       = Julia Roberts Hugh Grant Trevor Jones
| cinematography = Michael Coulter Nick Moore
| studio         = PolyGram Filmed Entertainment Working Title Films
| distributor    = Universal Pictures
| released       =  
| runtime        = 124 minutes
| country        = United Kingdom
| language       = English
| budget         = $43 million
| gross          = $363,889,700
}}
Notting Hill is a 1999 British romantic comedy film set in Notting Hill, London, released on 21 May 1999. The screenplay was by Richard Curtis, who had written Four Weddings and a Funeral (1994), and the film was produced by Duncan Kenworthy  and directed by Roger Michell. The film stars Hugh Grant, Julia Roberts, Rhys Ifans, Emma Chambers, Tim McInnerny, Gina McKee, and Hugh Bonneville.

Notting Hill was well received by critics and became the highest grossing British film released that year. The film won a British Academy of Film and Television Arts|BAFTA, was nominated in two other categories, and won other awards, including a British Comedy Award and a Brit Award for the soundtrack.

==Plot== travel bookshop Welshman named Spike (Rhys Ifans).
 Hollywood superstar Anna Scott (Julia Roberts) when she enters his shop to buy a book. Minutes later, they collide in the street and his orange juice spills on her clothes. Will offers his house nearby for Anna to change. Afterwards she surprises him with a kiss.
 the Ritz Hotel, under the name "The Flintstones|Flintstone". Will is allowed in, but her press interviews are running late and he is mistaken for a journalist. (In panic he claims he works for Horse & Hound.) He has to interview the cast of Annas new film Helix, which he has not seen. After the interviews, Anna calls him back in and says she has cancelled her evening appointment and can now go out with him. Will is delighted, before remembering that his sister Honey (Emma Chambers) is about to have a birthday party; Anna surprises him by saying she will go as his date.

There, at Maxs (Tim McInnerny) and Bellas (Gina McKee) house, Anna feels at home with Wills friends as they share stories of who has the most unfortunate life. Afterwards Will and Anna climb into a private garden square. The next day they go to a cinema, then to a restaurant, where Will overhears Anna being mocked and defends her honour. Anna invites Will to her room at the Ritz, but her American boyfriend, a famous film star named Jeff King (Alec Baldwin), has arrived unexpectedly. Will pretends to be a room-service waiter; Jeff casually condescends to him. Anna is apologetic; she thought King had broken up with her. Will realises he must end things with Anna, but is unable to forget her.

 .]]
Six months later, Anna turns up at Wills house unannounced. Old pin-ups of Anna taken many years earlier have been published and she needs to hide out. After spending the day together, Anna goes downstairs to where Will is sleeping: they embrace, go up to the bedroom, and make love.  But in the morning they are stunned to see a horde of reporters at the door, alerted by Spikes careless talk at the pub. Anna, angry at what she views as Wills betrayal, tells him the scandal will follow her forever and she already regrets what happened. Stunned and hurt, Will lets her go without further argument.

Another six months later, Anna returns to London to make a Henry James film, which Will had earlier suggested. Will is curious enough to visit the film set; Anna invites him to watch the days filming, then talk. A sound engineer provides headphones to hear the dialogue, but Will overhears Anna saying that he is "no one, just some guy from the past". Crushed, Will leaves abruptly. The next day, Anna comes to the bookshop with a parcel, and asks why he left. When he explains, she says that her co-star is a notorious gossip and she was being discreet. She wants to renew their relationship, saying that "the fame thing isnt really real" and that she is "just a girl, standing in front of a boy, asking him to love her". Will hears her out, but says that if she later rejected him again he could not handle it, and turns her down. She leaves Wills present behind, still wrapped; it turns out to be an original Marc Chagall painting, La Mariée, that Will owns a print of.

Will meets his friends, who are supportive&mdash;until Spike arrives and promptly calls him a "daft prick", making Will realise he has made the biggest mistake of his life by turning Anna away. Racing across London in Maxs car, Will and his friends search for Anna, finally reaching Annas press conference at the Savoy Hotel where Will, in his Horse & Hound persona, persuades her to stay in the UK with him. Anna and Will later marry, the film concluding with a shot of Will and a pregnant Anna on a bench in a London square.

==Cast==
*   and Duncan Kenworthy did not expect her to accept. Her agent told her it was "the best romantic comedy she had ever read".  Roberts said that after reading the script she decided she was "going to have to do this".   
*   had a "writer/actor marriage made in heaven". Michell said that "Hugh does Richard better than anyone else, and Richard writes Hugh better than anyone else", and that Grant is "one of the only actors who can speak Richards lines perfectly". 
* Emma Chambers as Honey Thacker: Wills younger sister, she is a fan of Anna Scott. She later marries her brothers flatmate Spike.
* Hugh Bonneville as Bernie: A failing stockbroker and a friend of Will. He does not recognize Anna Scott upon meeting her and by way of small talk about the low pay in acting, asks her how much money she made on her last film.
* Rhys Ifans as Spike: Wills strange Welsh flatmate who dreams of being an artist. He later marries Honey Thacker and becomes Wills brother-in-law. He is described by Will as "the stupidest person in the world, only doubled".
* Tim McInnerny as Max: Wills best friend, with whom Will often stays. He and Bella host Honeys birthday party.
*   lawyer who is married to Max. She is described by Will as one of the only two women that he has ever loved.
* James Dreyfus as Martin: Williams ineffective assistant at his bookshop.

The casting of Bonneville, McInnerny, McKee, Chambers, and Ifans as Wills friends was "rather like assembling a family". Michell explained that "When you are casting a cabal of friends, you have to cast a balance of qualities, of types and of sensibilities. They were the jigsaw that had to be put together all in one go, and I think weve got a very good variety of people who can realistically still live in the same world." 

;Other characters
* Richard McCabe as Tony: A failing restaurateur. The group meets at his restaurant.
* Dylan Moran as Rufus: A thief who attempts to steal from Wills bookshop. Despite being caught on CCTV concealing a book down his trousers he professes his innocence, then asks Anna if she wants his phone number.
* Alec Baldwin makes an uncredited appearance as Annas boyfriend, Jeff King.    cameo role the actress who has an orgasm every time shes taken out for a cup of coffee) in the restaurant Anna and Will attend.   
* Mischa Barton appears as the child actor whom Will pretends to interview for Horse & Hound. 
* Emily Mortimer as Wills "Perfect Girl," a potential love interest for Will.
* John Shrapnel as Annas UK press agent.
* Lorelei King as an assistant to press events.

==Production==
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5" Madonna or whomever. It all sprang from there. How would my friends react? Who would try and be cool? How would you get through dinner? What would they say to you afterwards?"
|-
| style="text-align: left;" | — Richard Curtis   
|} Mike Newell was approached but rejected it to work on Pushing Tin. He said that in commercial terms he had made the wrong decision, but did not regret it.  The producer, Duncan Kenworthy, then turned to Roger Michell, saying that "Finding someone as good as Roger, was just like finding the right actor to play each role. Roger shone out."   

Curtis chose Notting Hill as he lived there and knew the area, saying "Notting Hill is a melting pot and the perfect place to set a film".    This left the producers to film in a heavily populated area. Kenworthy noted "Early on, we toyed with the idea of building a huge exterior set. That way we would have more control, because we were worried about having Roberts and Grant on public streets where we could get thousands of onlookers." In the end they decided to film in the streets.  Michell was worried "that Hugh and Julia were going to turn up on the first day of shooting on Portobello Road, and there would be gridlock and we would be surrounded by thousands of people and paparazzi photographers who would prevent us from shooting". The location team, and security forces prevented this, as well as preventing problems the presence of a film crew may have caused the residents of Notting Hill, who Michell believes were "genuinely excited" about the film.  The location manager Sue Quinn, described finding locations and getting permission to film as "a mammoth task".  Quinn and the rest of her team had to write to thousands of people in the area, promising to donate to each persons favourite charity, resulting in 200 charities receiving money. 

{| class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "The major problem we encountered was the size of our film unit. We couldnt just go in and shoot and come out. We were everywhere. Filming on the London streets has to be done in such a way that it comes up to health and safety standards. There is no such thing as a road closure. We were very lucky in the fact that we had 100% cooperation from the police and the Council. They looked favorably on what we were trying to do and how it would promote the area."
|-
| style="text-align: left;" | — Sue Quinn 
|} Ritz Hotel, where work had to take place at night, the Savoy Hotel, the Nobu Restaurant, the Zen Garden of the Hempel Hotel and Kenwood House.  One of the final scenes takes place at a film premiere, which presented difficulties. Michell wanted to film Leicester Square but was declined. Police had found fans at a Leonardo DiCaprio premiere problematic and were concerned the same might occur at the staged premiere. Through a health and safety act, the production received permission to film and constructed the scene in 24 hours.  Interior scenes were the last to be filmed, at Shepperton Studios.  The final cut was 3.5 hours long, 90 minutes edited out for release. 

The film features the 1950 Marc Chagall painting La Mariée. Anna sees a print of the painting in Williams home and later gives him what is presumably the original. Michell said in Entertainment Weekly that the painting was chosen because Curtis was a fan of Chagalls work and because La Mariée "depicts a yearning for something thats lost." The producers had a reproduction made for the film, but had to get permission from the owner as well as clearance from the Design and Artists Copyright Society. Finally, according to Kenworthy, "we had to agree to destroy it. They were concerned that if our fake was too good, it might float around the market and create problems." The article also noted that "some experts say the real canvas could be worth between $500,000 and $1 million." 

The film features the book Istanbul: The Imperial City (1996) by John Freely. William recommends this book to Anna, commenting the author has at least been to Istanbul. In reality, Freely teaches at Boğaziçi University in Istanbul,  and is the author of nine books about the city.

==Music== Trevor Jones. cover of Pulp recorded new song "Born to Cry", which was released on the European version of the soundtrack album.
 Blue Moon" on the piano at Tonys restaurant on the night it closes.  Originally, Charles Aznavours version of "She" was used in the film, but American test screening audiences did not respond to it. Costello was then brought in by Richard Curtis to record a cover version of the song.  Both versions of the song appear in non-U.S. releases.  

The soundtrack album was released by Island Records.

;US version track listing No Matter What"&nbsp;– Boyzone
# "Youve Got a Way" (Notting Hill remix)&nbsp;– Shania Twain
# "I Do (Cherish You)"&nbsp;– 98 Degrees
# "She (Charles Aznavour song)#Elvis Costello version|She"&nbsp;– Elvis Costello
# "Aint No Sunshine"&nbsp;– Bill Withers
# "How Can You Mend a Broken Heart?"&nbsp;– Al Green
# "Gimme Some Lovin"&nbsp;– Spencer Davis Group
# "When You Say Nothing At All"&nbsp;- Ronan Keating
# "Aint No Sunshine"&nbsp;– Lighthouse Family Another Level
# "Everything About You"&nbsp;- Steve Poltz Trevor Jones (Score) Trevor Jones (Score)

The film score and original music was recorded and mixed by Gareth Cousins (who also mixed all the songs used in the film) and Simon Rhodes.

==Release==

===Critical reception===
The film had generally positive reviews, scoring an 82% "Certified Fresh" rating at Rotten Tomatoes.    Varietys Derek Elley said that "Its slick, its gawky, its 10 minutes too long, and its certainly not "Four Weddings and a Funeral Part 2" in either construction or overall tone", giving it an overall positive review.  Cranky Critic called it "Bloody damned good", as well as saying that it was "A perfect date flick."  Nitrate said that "Notting Hill is whimsical and light, fresh and quirky", with "endearing moments and memorable characters".  In his review of the films DVD John J. Puccio noted that "the movie is a fairy tale, and writer Richard Curtis knows how much the public loves a fairy tale", calling it "a sweet film".  Desson Howe of The Washington Post gave the film a very positive review, particularly praising Rhys Ifans performance as Spike.  James Sanford gave Notting Hill three and a half stars, saying that "Curtis dialogue may be much snappier than his sometimes dawdling plot, but the first hour of Notting Hill is so beguiling and consistently funny it seems churlish to complain that the rest is merely good."  Sue Pierman of the Milwaukee Journal Sentinel stated that "Notting Hill is clever, funny, romantic&nbsp;– and oh, yes, reminiscent of Four Weddings and a Funeral", but that the film "is so satisfying, it doesnt pay to nitpick."  Roger Ebert praised the film, saying "the movie is bright, the dialogue has wit and intelligence, and Roberts and Grant are very easy to like."  Kenneth Turan gave a good review, concluding that "the films romantic core is impervious to problems".  CNN reviewer Paul Clinton said that Notting Hill "stands alone as another funny and heartwarming story about love against all odds". 

Widgett Walls of Needcoffee.com gave the film "three and a half cups of coffee", stating that "the humor of the film saves it from a completely trite and unsatisfying (nay, shall I say enraging) ending", but criticised the soundtrack.  Dennis Schwartz gave the film a negative review with a grade of "C-" citing "this film was pure and unadulterated balderdash".  Some criticised the film for giving a "sweetened unrealistic view of London life and British eccentricity."  The Independent newspaper derided the film for being unrealistic. 

===Lists===
Notting Hill was 95th on the British Film Institutes "list of the all-time top 100 films", based on estimates of each films British cinema admissions. 

===Box office=== Runaway Bride (again starring Roberts).  It was the sixteenth highest grossing film of 1999,  and as of February 2014 is the 215th highest grossing film of all time.  In 2007, it became the then highest grossing British film. 

===Awards and nominations===
Notting Hill won the Audience Award for Most Popular Film at the  .  The film won Best British Film, Best British Director for Roger Michell, and Best British Actor for Hugh Grant at the Empire Awards. 
The film received three nominations at the Golden Globes, in the categories Best Motion Picture&nbsp;– Comedy/Musical, Best Motion Picture Actor&nbsp;– Comedy/Musical for Hugh Grant, and Best Motion Picture Actress&nbsp;– Comedy/Musical for Julia Roberts. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 