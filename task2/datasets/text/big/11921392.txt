Enter the Fat Dragon
 
 
{{Infobox film
| name           = Enter the Fat Dragon
| image          = Enter-The-Fat-Dragon.jpg
| caption        = Japanese film poster
| film name = {{Film name| traditional    = 肥龍過江
| simplified     = 肥龙过江
| pinyin         = Féi Lóng Guò Jiāng
| jyutping       = Fei4 Lung4 Gwo3 Gong1}}
| director       = Sammo Hung
| producer       = Florence Yu
| writer         = Ni Kuang
| starring       = Sammo Hung Billy Chan Fung Hak-On Lam Ching-ying
| music          = Frankie Chan
| cinematography = Ricky Lau
| editing        = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}} parody of the Bruce Lees 1972 film Way of the Dragon, and a satire of the Bruceploitation phenomenon of the 70s.
 Martial Law.

==Plot==
Ah Lung is a pig farmer and a devoted Bruce Lee fan who is anxious to follow in Lees footsteps, but only ridiculed for his attempts. He is sent to the city to earn a living working at his uncles restaurant, but when he arrives, he finds a gang of thugs causing trouble in the restaurant. He takes the chance to prove himself and attacks the thugs, defeating them and saving the restaurant. Soon, he becomes a waiter, and discovers a plot by the same thugs to kidnap a woman he works with. Eventually, he defeats the thugs once again and saves the day.

==Cast==
{| class="wikitable"
! Actor !! Role
|-
| Sammo Hung || Ah Lung
|-
| Yung San Baan || Fighter in Opening Credit Sequence
|-
| Yuen Biao || Fighter in Opening Credit Sequence
|-
| Billy Chan || Thug/Action Movie Fighter
|-
| Chung Fat || Fighter in Opening Credit Sequence
|-
| Wah Cheung ||
|-
| Roy Chiao || Chiu
|-
| Lam Ching-ying || Action Movie Fighter
|-
| Ngai Hung Chik ||		
|-
| Yuet Sang Chin ||
|-
| Feng Feng || Uncle Hung		
|-
| Ging Man Fung || Spectacles vendor
|-
| Fung Hak-On || Gene
|-
| Meg Lam || Baat Je
|-		
| Lee Hoi San || Karate Thug
|-		
| Hoi-sook Lee || Chen
|-		
| King Lee || Party fighter
|-
| Tony Leung Siu-hung || Tseng Hsiao-lung
|-
| Ling Ling ||
|-
| Billy Chan || Fighter in Opening Credit Sequence
|-
| Ke Ming Lin ||
|-
| Lau Chau Sang ||
|-
| Mang Hoi || Fighter in Opening Credit Sequence
|- Mars || Fighter in Opening Credit Sequence
|-
| Gwa-pau Sai ||
|-
| Eric Tsang || Party Hosts Son
|-
| Ha Wong ||
|-
| Ji Keung Wong ||
|-
| Huang Ha || Movie Director
|-
| David Nick || Boxing Thug
|-
| Yeung Wai ||
|-
| Bryan Leung || Bearded Fighter
|-
| Peter Yang Kwan || Professor Bak / Bai
|-
|}

==See also==
*List of Hong Kong films
*Sammo Hung filmography

==External links==
* 
*  
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 
 