Young Eagles (film)
{{Infobox film
| name           = Young Eagles 
| image          = Young Eagles 1930.jpg
| image_size     = 200px
| caption        = Theatrical release poster
| director       = William A. Wellman
| producer       = B.P. Schulberg
| writer         = Grover Jones William Slavens McNutt	 	 Charles "Buddy" Rogers Jean Arthur  Paul Lukas Howard Jackson	
| cinematography =Archie Stout
| based_on       = The One Who Was Clever and Sky-High by Elliott White Springs
| editing        = Alyson Shaffer
| studio         = Paramount Famous Lasky Corp.
| distributor    = Paramount Pictures
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English 
| budget         = 
}}
Young Eagles is a 1930 romantic drama film directed by  ". Shawna 2008, p. 103. 

Wellman, himself a former pilot in the Lafayette Flying Corps, for whom aviation was a passion, directed the film, the last of his "unofficial trilogy" that included Wings (1927 film)|Wings (1927) and The Legion of the Condemned (1928). Tibbetts and Welsh 2010, p. 100.  The director had hoped that the film would prove as popular as his acclaimed World War I aviation drama, Wings, which had won the first Academy Award in 1927. Wellman cast Buddy Rogers again as his lead in the new film, but Young Eagles proved to be not as successful.

==Plot==
Lieutenant Robert Banks (Buddy Rogers), a young American aviator in the Lafayette Escadrille, on leave in Paris, meets Mary Gordon (Jean Arthur), a young American living abroad. Their romance is cut short by his return to the front. In an air battle, Robert brings down and captures von Baden, nicknamed the "Grey Eagle" (Paul Lukas), and takes him to Allied headquarters in Paris, to obtain intelligence on German plans.

Mary, ostensibly a spy for the Germans, drugs Robert, who awakens to find that his uniform has been stolen by von Baden. Later, in another air conflict, von Baden is wounded, but shoots down Roberts aircraft. The German rescues him, however, and takes him to an Allied hospital, assuring him of Marys love; his faith in her is restored when Robert learns that Mary is actually an American spy.

==Cast== Charles "Buddy" Rogers as Lieutenant Robert Banks
* Jean Arthur as Mary Gordon
* Paul Lukas as Von Baden, the "Grey Eagle"
* Stuart Erwin as "Pudge" Higgins
* Virginia Bruce as Florence Welford
* Gordon De Main as Major Lewis James Finlayson as "Scotty" Frank Ross as Lieutenant Graham
* Jack Luden as Lieutenant Barker
* Freeman Wood as Lieutenant Mason George Irving as Colonel Wilder
* Stanley Blystone as Captain Deming

==Production==
Young Eagles called for only two scenes depicting air battles, with more of the action centered around a story of espionage and unrequited love. Orriss 2013, p. 35.  Wellman began pre-production in November 1927, making the decision to use aerial footage from Wings matched to new sequences. Tibbetts and Welsh 2010, p. 94. 
 American Eagle, Travel Air Waco biplanes that at least were close facsimiles of wartime aircraft. 

Film sets for a wartime airfield were built at Lake Sherwood, California, with three weeks spent on location shooting. Two crash scenes were staged by Norris on location, with the second one nearly causing the death of the veteran movie pilot Dick Grace when he flipped his aircraft in a crash so violent that his shoes were ripped off his feet. He walked away with only minor bruises.  
 . A nitrate print of Young Eagles is stored in the UCLA Film and Television Archive, but it is not listed for preservation. Villecco 2001, p. 156. |group=N}}

==Reception== mono (Western Electric Sound System) print with a running time 72 minutes was premiered at the Paramount Theater in New York.   Turner Classic Movies. Retrieved: September 13, 2013.   To promote the film at the premiere, an aircraft was on display inside the theater. 

===Critical response===
Released only a month after Wellmans Dangerous Paradise, Young Eagles received  mixed reviews.  The film was also not a commercial success, performing poorly at the U.S. box office.   Wellman’s portrayal of air warfare, however, received praise for its “... beauty and freedom of flight”.  Slide 2010, p. 238.  Wellman and the crew expressed personal disappointment with how the film was received.   When unfavourable reviews began to come in, a distraught Wellman asked to be let out of his contract with Paramount, with the studio agreeing to sever ties with the acclaimed director. 

Mordaunt Hall of The New York Times was extremely critical of the film, calling it a "... highly incredible narrative with two good air-fighting episodes and a mass of wild and absurd incidents ..." and noted that the "... pivotal idea is a stab at subtlety, but in mapping it out a Teutonic prisoner of war has to be extraordinarily gullible." He sarcastically added that the production "... could have been named Young Goats, for Banks and another flying officer are evidently made the goats so that a spying expedition is helped along". Of the cast, Hall said, "Mr. Rogerss acting never rises above the level of the tale. Jean Arthur seems to be somewhat afraid of the character she plays. The only real performance is that of Paul Lukas as von Baden." 

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Kelly, Shawna.  . Mount Pleasant, South Carolina: Arcadia Publishing, 2008. ISBN 978-0-7385-5902-5.
* Mavis, Paul.   Jefferson, North Carolina: McFarland & Company, 2013. ISBN 978-1-4766-0427-5.
* Orriss, Bruce W. When Hollywood Ruled the Skies: The Aviation Film Classics of World War I. Los Angeles: Aero Associates, 2013. ISBN 978-0-692-02004-3.
* Slide, Anthony.  . Lexington, Kentucky: University Press of Kentucky, 2010. ISBN 978-0-8131-3745-2.
* Thompson, Frank T.   Lanham, Maryland: Scarecrow Press, 1983. ISBN 978-0-8108-1594-0.
* Tibbetts, John C. and James M. Welsh.   Lanham, Maryland: Scarecrow Press, 2010. ISBN 978-0-8108-7679-8.
* Villecco, Tony.  . Jefferson, North Carolina: McFarland & Company, 2001. ISBN 978-0-7864-8209-2.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 