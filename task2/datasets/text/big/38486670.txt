When Four Do the Same
{{Infobox film
| name = When Four Do the Same
| image =
| image_size =
| caption =
| director = Ernst Lubitsch Paul Davidson
| writer = Erich Schönfelder   Ernst Lubitsch
| narrator = Fritz Schulz
| music =
| editing =
| cinematography =
| studio = PAGU
| distributor =
| released = 16 November 1917 
| runtime = 4 reels
| country = Germany Silent  German intertitles
| budget =
| gross =
| preceded_by =
| followed_by =
}} silent comedy comedy drama film directed by Ernst Lubitsch and starring Ossi Oswalda, Emil Jannings and Margarete Kupfer. Lubitsch himself plays a book shop employee who falls in love with Jannings daughter. The film was a key transitional work in Lubitschs career, as he began to produce films with greater depth than his early light comedies. 

==Cast==
* Emil Jannings as Segetoff
* Ossi Oswalda as Segetoffs Tochter
* Margarete Kupfer as Frau Lange, Buchhändlerin Fritz Schulz as Tobias Schmalzstich, Lehrling
* Victor Janson as Tanzlehrer
* Ernst Lubitsch

==References==
 

==Bibliography==
* Eyman, Scott. Ernst Lubitsch: Laughter in Paradise. Johns Hopkins University Press, 2000.

==External links==
* 

 

 
 
 
 
 
 
 


 
 