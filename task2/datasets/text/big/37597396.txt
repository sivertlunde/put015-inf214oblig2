The Debt (1999 film)
{{Infobox film
| name     = The Debt
| image    =
| director = Krzysztof Krauze
| producer = 
| writer   = Krzysztof Krauze   Jerzy Morawski
| starring = Robert Gonera   Jacek Borcuch   Andrzej Chyra
| cinematography = 
| music = 
| country = Poland
| language = Polish
| runtime = 107 min
| released = 1999
}}

The Debt ( ) is a 1999 Polish film directed by Krzysztof Krauze. It is based on a true event that took place in Warsaw, Poland in the early 1990s.

After looking for financial backing to start a business importing Italian scooters, college friends Adam and Stefan meet businessman Gerard. While initially very helpful, Gerald soon turns violent, and begins to blackmail the pair for increasingly large sums of money while psychologically terrorizing the two men.

The movie is based on the true story of   and  , who were later pardoned by the Polish president because of exposure from the film. 

== Cast ==
* Robert Gonera : Adam Borecki
* Jacek Borcuch : Stefan Kowalczyk
* Andrzej Chyra : Gerard Nowak
* Cezary Kosiński : Tadeusz Frei
* Joanna Szurmiej-Rzączyńska : Basia
* Agnieszka Warchulska : Jola
* Joanna Kurowska : Pregnant Woman

==References==
 

== External links ==
* 

 
 
 
 


 