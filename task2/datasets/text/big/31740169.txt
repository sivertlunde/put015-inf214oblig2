Yearning (1964 film)
{{Infobox film
| name           = Yearning
| image          = Yearning.jpg
| caption        = Japanese film poster
| director       = Mikio Naruse
| producer       = Toho, Sanezumi Fujimoto
| writer         = Zenzo Matsuyama
| starring       = Hideko Takamine Yūzō Kayama
| music          = Ichirō Saitō
| cinematography = Jun Yasumoto
| editing        = Eiji Ooi
| distributor    =
| released       =  
| runtime        = 98 minutes
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
}}
 Japanese film drama directed by Mikio Naruse, starring Hideko Takamine and Yūzō Kayama. The film is based on a story by Naruse, with the screenplay authored by Zenzo Matsuyama.    

The story centers on war-widow Reiko Morita (Hideko Takamine), who has given her best years to her late husbands family while managing their grocery. The family plots to tear down the shop and build a supermarket in its place, pushing Reiko out of the top position. As with many Naruse films, the background to the film marks a significant point of social change, in this case the arrival of modern supermarkets with their inevitable effect of driving local family grocery stores out of business.


==Plot==
Reiko Morita (Hideko Takamine) is a widow who loses her husband in war. Bombing destroys his familys shop and the widow stays to rebuild it as the rest of the family flee and runs it for 18 years out of love for her dead husband and his mother. The film starts after 18 years when a new supermarket threatens to put them out of business. The sisters conspire to turn the shop into a supermarket and get rid of their brothers widow. Meanwhile, the surviving younger brother 25 year-old Koji Morita (Yūzō Kayama) loafs around, losing jobs, getting drunk, laid and gambling. In the crisis, he confesses to his shocked sister-in-law, 12 years older, that he has always loved her and cant deal with it. She cares for him, but in the motherly, elder sister way. She rejects him and decides to return home to her family, threatening suicide if he stops her. This suits the sisters, but he follows her onto the long train ride. On the way, she softens and they disembark for a country inn, where they can talk. He resumes his approaches, but at the last minute, she cant face intimacy. He storms out and gets drunk. He calls Reiko up and says he is going back home. In the morning, Reiko looks out the window and sees him being carried into the village on a stretcher, his face covered. Someone says he fell from a cliff. Reiko runs after him but falters. The last shot is of her face, blank, as she realizes what happened.

==Cast==
* Hideko Takamine as Reiko Morita
* Yūzō Kayama as Koji Morita
* Mitsuko Kusabue as Hisako Morizono
* Yumi Shirakawa as Takako Morita
* Mie Hama as Ruriko
* Aiko Mimasu as Shizu Morita

== Awards ==
* 1956 -    

== References ==
 

==External links==
*  at Japan Movie Database
*  

 

 
 
 
 
 
 
 
 
 
 