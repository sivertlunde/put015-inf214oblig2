Jacobo Ortis
{{Infobox film
| name           = Jacobo Ortis
| image          =
| caption        =
| director       = Giuseppe Sterni
| producer       =  Milano Film
| writer         = Ugo Foscolo (novel)
| starring       = Paola Borboni, Ernestina Badalutti,and Vittorio Brombara
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Italy
| language       = Silent
}}
 1916 Italy|Italian silent drama film directed by Giuseppe Sterni and based on the novel Le ultime lettere di Jacopo Ortis by Ugo Foscolo. 

It was the debut film of Paola Borboni.

==Cast==
*Ernestina Badalutti   
*Paola Borboni   
*Luigi Duse   
*Elisa Finazzi   
*Angelo Giordani   
*Giulio Grassi   
*Virginio Mezzetti   
*Vittorio Pieri  
*Felicita Prosdocimi   
*Mary Cleo Tarlarini   
*Carlo Trouchez  

==Bibliography==

*Title: Le ultime lettere di Jacopo Ortis
*Author: Ugo Foscolo
*Publisher: A. Bergnes, 1833
*Page Nº: 255 p.

== External links ==
*  
 

 
 
 
 
 
 
 