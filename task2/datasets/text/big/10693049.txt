Winx Club: The Secret of the Lost Kingdom
 
{{Infobox film
| name           = Winx Club: The Secret of the Lost Kingdom
| image          = Winx-Club-Movie.jpg
| caption        = Theatrical release poster
| director       = Iginio Straffi
| producer       = Rainbow S.r.l.
| writer         = Iginio Straffi Sean Molyneaux
| based on       = Winx Club by Iginio Straffi
| starring       = Italian:          
| music          = Paolo Baglio
| cinematography = 
| editing        = 
| studio         = Rai Fiction
| distributor    = Rainbow S.r.l.
| released       =  
| runtime        = 85 minutes
| country        = Italy
| language       = Italian
| gross          = €1,979,972 (US$2,479,717)
}} CGI animated feature film, based on the television series Winx Club, taking place after the events of the first three seasons. The film had premiere on November 30, 2007 in Italian cinemas.

Rainbow S.r.l. offered the film to the Cannes Film Festival in 2007.  Australian singer Natalie Imbruglia sings the films ending song, "All the Magic".  Elisa Rosselli performed the other songs, including "Fly", "You Are the One", "Enchantix, Shining so Bright", "Only a Girl", and "You Made Me a Woman". The movie was released on DVD in Italy in early March 2008.

Straffi has noted that it was difficult to animate the girls hair correctly in 3D. 

==Plot== Valtor was defeated.

An unseen narrator begins to speak about a girl who discovered that she is a fairy and an 18-year-old princess: List of Winx Club characters#Protagonists|Bloom. Though her story is a magical one, it is one without an ending. The story will only have one when the girl writes the ending—i.e., creates the ending.
 her friends King Oritel Queen Miriam which could change Blooms life forever. The girls are on their way to find Hagen, a blacksmith who once forged the Sword of King Oritel. The girls successfully sneak into Hagens castle, but are forced to battle his robot guards. Aisha, trying to save Musa from the robots spinning blades, is badly hurt. Flora uses a vine spell to entangle all the robots, but as they start to free themselves by cutting through the vines, Flora is weakened and falls to the ground. Fortunately, Bloom , whos fueled with anger, ends the battle by using her Dragon Flame to destroy the power source. Hagen then holds Bloom at sword-point for trespassing his castle, luckily Faragonda arrives in time to ease him.

At Alfea, Bloom, Hagen, and Faragonda talk in her office. Hagens comments on the decoration of the school, Faragonda explains that some fairies are leaving the school to end their journey and become Guardian Fairies: all of the Winx, except Bloom, as her Enchantix is still incomplete. Bloom then asks Hagen if he can sense the sword that he forged for her father, a mystical sword that can never be separated from its rightful master. Hagen states that he has been searching for the sword for years to find her parents, but has not sensed it at all. Hagen tells her that she must face reality.
 tears and Sky appears and comforts her.

Bloom is upset that all she and the Winx have done wont matter anymore. Sky however tells her, "school might be over but life goes on." That night, there is a party and Bloom dances with Sky on the grounds. He encourages her to keep looking for her parents despite Hagens words. Unfortunately, a ship arrives and Sky is forced to leave with a mysterious girl. As he goes, he promises Bloom that he will explain everything and never leave her.
 Daphne appears and tells Bloom that there is still hope; their parents are still alive. Informing her about the Book of Fate, a book their father kept that tells the entire history of Domino, and its location. She also gives Bloom her mask, which it will allow her see Domino to what it used to look like. Before she vanishes, Daphne tells Bloom that she is not ever alone.
 three Ancestral Witches, since they believed they had destroyed all of Domino inhabitants. Obsidians Keeper, List of Winx Club characters#Antagonists|Mandragora, appears before them asking for the ability to escape Obsidian so she can search for the cause of the swords reactivation. The witches agree and lend Mandragora a fraction their tremendously powerful dark abilities.

They head to Domino where Bloom wears the mask and finds that Domino is actually a beautiful place before it was covered with ice and snow. The group head towards the mountains where the book is hidden, but Tecna tells them that a legendary bird named the Roc is guarding the library. The Specialists climb the mountain but Sky and List of Winx Club characters#Specialists|Rivens arguing causes the Roc to awaken and flies off with the boys hanging on for dear-life.

The girls run to the ship and manage to calm the bird with Musas music and let it land. Inside the library, they meet List of Winx Club characters#Planet of Domino|Bartelby, Oritels scribe in spiritual form, who shows them the Book of Fate. In it, they discover that Oritel as well as all the people of Domino are trapped in the dark dimension Obsidian. Bloom learns that she must travel there to free them. Wanting to learn more, Bartelby lets her turn the page, but the future has not yet been written. Though, Bartelby gives them a prophecy.

Meanwhile, Riven is bitten by Mandragoras spy bug and becomes her puppet. Soon after, Alfea is attacked by Mandragora. The Winx, Mirta, Hagen, Faragonda, and the teachers are able to stop her. Faragonda casts a spell and sends her away but she faints from using too much power, and Palladium begins to help the injured students by gathering healing potions. After her recovery Faragonda is asked by Bloom to tell her about Obsidian and its cruel nature, how all the nightmares of the universe are within it. Just before they leave, Bloom looks at the Book of Fate to see her parents wedding photo turn into a picture of her and Sky at the altar. Throwing the book aside in surprise, the page changes into everyone fixing the school, which soon comes true.
 
The group head to Pixie Village where they enter the Tree of Portals. Only Bloom stays behind as she is not yet a guardian fairy and cannot miniaturize. Unfortunately, Jolly triggers an explosion inside the tree which causes the keys to become jumbled. Thankfully, Lockette finds the correct key and they all head to the gateway to the Obsidian dimension. As the girls enter, Mandragora appears. Riven begins to battle Sky. As he is about to stab him, Musa jumps in front of him and is injured.

In Obsidian, the girls battle their greatest fears but survive. Bloom saw a vision of what happened to her birth parents: Oritel was pulled into Obsidian by the Ancestral Witches while Miriam absorbed herself into the sword in order to be with him. Next, Bloom sees a statue that looks like her father. To her horror, the witches tell her it is her father "frozen in stone", the same fate as all inhabitants of her planet. The witches force her to make a choice between destroying the sword and saving her Earth parents or taking the sword and letting Mandragora kill them. Luckily, Bloom sees that in the image, Mike is not sneezing as he pets Purr - Mike is allergic to cats - and realizes that its a trick. Meanwhile, Riven sees what he has done and recalls all what he and Musa have been through together. He lifts her up and tells her that he now knows what he must fight for-her and they kiss. Chasing after Mandragora, Sky arrives moments later and takes the sword, but supposedly dies, as only a king can wield the sword.

Feeling all alone, Bloom is ready to give up when Daphne appears and reminds her that she is not ever alone. Bloom wears the mask, and Daphne joins with her to destroy the Ancestral Witches. However, Mandragora returns and, using her body as a host, the three witches begin to strangle Bloom to death. Then Sky stabs Mandragora with Oritels sword; with Blooms Dragon Flame, destroying both her and the entire Obsidian Circle, freeing everyone. Sky explains that he is now King of Eraklyon and his coronation was the night he left Bloom at Alfea. Domino is restored, Oritel is freed from stone, placing Miriam back in human form; Blooms Enchantix is complete, and she is finally reunited with her birth parents.

There is a party afterwards where Oritel and Miriam promise Bloom that they will be around for the rest of her life. Mike and Vanessa are there too and Bloom hugs them affectionately. Oritel begins the traditional father-daughter dance, but lets Bloom dance with Sky. As the whole group dances, Sky swings by Brandon and Stella, and Brandon hands over a beautiful box to him. Moving away, Sky shows Bloom a diamond ring. He whispers quietly to her, asking her if she will marry him. Happier than ever, Bloom accepts and kisses him.

In the final scene before the credits, Bartelby appears by the Book of Fate, telling the audience that the prophecy has been fulfilled, Bloom is now a guardian fairy and there is a new Company of Light - the Winx. However, the Ancestral Witches have not been destroyed. Instead, they were freed and are finding new hosts to take over and destroy the Winx. They are shown with their direct descendants,  .

==Voice cast==
{| class="wikitable"
|-
! Character !! Italian !! English
|- Molly C. Quinn
|-
| Stella (Winx Club)|Stella/Chatta/Zing || Perla Liberatori || Amy Gross
|-
| Flora (Winx Club)|Flora/Amore || Ilaria Latini || Alejandra Reynoso
|-
| Musa (Winx Club)|Musa/Digit || Gemma Donati || Romi Dames
|-
| Tecna (Winx Club)|Tecna/Livy/Glim/Piff || Domitilla DAmico || Morgan Decker
|-
| Layla (Winx Club)|Aisha/Lockette || Laura Lenghi || Keke Palmer
|-
| Prince Sky || Alessandro Quarta || Matt Shively
|- Adam Gregory
|-
| Helia || Francesco Pezzulli || David Faustino
|-
| Riven || Mirko Mazzanti || Sam Riegel
|-
| Timmy || Corrado Conforti || Charlie Schlatter
|-
| Nabu || Sasha De Toni || Will Blagrove
|}

==Soundtrack==

===International versions===

{| class="wikitable sortable"
!Song
!Original Name
!Language
!Singer(s)
!Country
|-
|"Jestes Naj"
|rowspan="10"|"Unica" Polish
|Aleksandra Szwed Poland
|-
|"Tu es la seule" French
|Sina France
|-
|"Sen bir tanesin" Turkish
|Group Hepsi Turkey
|-
|"Mono esy"  Greek
|Nikki Ponte Greece
|-
|"Tu eres única" Spanish
| Spain
|-
|"Única" Latin Spanish Maythe Guedes ft. Rebeca Aponte Latin America
|-
|"És a tal" Portuguese
| Portugal
|-
|"Jij bent cool" Dutch
|Ilona Netherlands
|-
|"Youre the one" English
|Elisa Rosselli
|
|-
|"Du bist es" German
|Deevoice Germany
|-
|"Ayağa kalk !"
|rowspan="4"|"Segui il Ritmo" Turkish
|Group Hepsi Turkey
|-
|"Kom op dans" Dutch
|Ilona  Netherlands
|-
|"Lass dich fallen" German
|Deevoice Germany
|-
|"Stand up"  English  Elisa Rosselli
|
|-
|rowspan="3"|"Enchantix"
|rowspan="3"|"Potere di enchantix" Turkish
|Group Hepsi Turkey
|- English
|Elisa Rosselli
|
|- Greek
|Nikki Ponte Greece
|-
|"You Made Me a Women"
|rowspan="4"|"A un passo da me" English
|Elisa Rosselli
|
|-
|"Deine Augen" German
|Deevoice Germany
|-
|"Genau Wie Du" German
|Deevoice Germany
|-
|"Kiss from Rose" English
|Seal (singer) UK
|-
|"Winx"
|rowspan="2"|"Segui il tuo cuore" Dutch
|Ilona Netherlands
|-
|"Fly" English
|Elisa Rosselli
|
|-
|"Die Alive"
|rowspan="2"|"Ending" English
|Tarja Turunen Finland
|-
|"All the Magic" English
|Natalie Imbruglia All Countries
|}

==Distribution==
 
A live action dance show was performed to promote the film at the 2007 Rome Film Festival.  In the first week of showing, the film was distributed in 665 cinemas and had 420,000 viewers. It received 1,979,972 euros ($3,074,695.84 US), this is the biggest opening for an animated film in Italy.  It has reached five million euros in revenue.  It was released on DVD in Italy in March 2008, with an Italian soundtrack; the German release offers a choice of German and English.
It premiered on Nickelodeon in America on March 11, 2012 and was released on DVD on August 7, 2012.

===Sequel===
On October 29, 2010 a theatrical sequel,   was released in Italy.

==References==
 
Nickelodeon press release, April 2011

==External links==
*  
*  

 

 
 
 
 
 
 
 
 