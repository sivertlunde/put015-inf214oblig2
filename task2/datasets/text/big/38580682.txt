Meet the Parents (series)
 
{{Infobox film
| name           = Meet the Parents
| image          =  
| image_size     = 
| caption        = 
| director       = Jay Roach (1 & 2)  Paul Weitz (3)
| producer       = Robert De Niro   Jane Rosenthal   Jay Roach
| writer         = 
| narrator       = 
| starring       = Robert De Niro   Ben Stiller   Teri Polo   Blythe Danner   Owen Wilson   Dustin Hoffman   Barbra Streisand    (#Recurring characters|More) 
| music          = Randy Newman (1 & 2)  Stephen Trask (3) Peter James (1)   John Schwartzman (2)   Remi Adefarasin (3)
| editing        = 
| studio         = TriBeCa Productions
| distributor    = Universal Studios   DreamWorks (1 & 2)   Paramount Pictures (3)
| released       =  :    Little Fockers|3:  
| runtime        = 321 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

==Films==

===Meet the Parents (2000)===
 
Gaylord "Greg" Focker (Ben Stiller) is a nurse living in Chicago. He intends to propose to his girlfriend Pam Byrnes (Teri Polo), but his plan is disrupted when he learns that Pams sisters fiance had asked Pams father for permission before proposing. Greg and Pam travel to Pams parents house to attend Pams sisters wedding. Greg hopes to propose to Pam in front of her family after receiving her fathers permission. But this plan is put on hold when the airline loses his luggage, including the engagement ring.
 lie detector CIA counterintelligence officer.

Meeting the rest of Pams family and friends, Greg still feels like an outsider. Despite efforts to impress the family, Gregs inadvertent actions make him an easy target for ridicule and anger: He accidentally gives Pams sister a black eye during a volleyball mishap; uses a malfunctioning toilet that floods the Byrnes back yard with sewage; sets the wedding altar on fire and inadvertently leads Jack to think he is a marijuana user. Later, Greg loses Jinx and replaces him with a stray whose tail he spray paints to make him look like Mr. Jinx. Also after Greg spray-paints the cats tail the cat is left alone in the house while the whole family are out and when the family arrive home they find that the cat has damaged and destroyed all the wedding possessions. It is then that the truth is revealed about the cat and everybody especially Jack is absolutely furious with Greg with the fact that hes almost ruined the whole wedding.

Jack denies turning Pam against Greg, saying that Greg did that himself though his dishonesty.  Jack says he always demands honesty, which is when Greg reveals to Pam that Jack never retired and is still in the CIA.  Jack is forced to admit that he is right. Unfortunately for Greg the incident in which he caught Jack with one of his associates and carrying out a phonecall in Thai was actually Jack preparing a surprise honeymoon for Pams sister and her fiance which makes him more angrier at Greg than ever.  Jack reveals that the person he met at the supermarket was his travel-agent and he was receiving Debbie and her fiances visas.

By now, the entire Byrnes family, including Pam, agrees that it is best for Greg to leave. Unwillingly, Greg goes to the airport where he is detained by airport security for refusing to check in his recently returned luggage. Back at the Byrnes household, Jack tries to convince his wife and Pam that Greg would be an unsuitable husband. Upon receiving retribution from both his wife and Pam, Jack realizes that Pam truly loves Greg. Jack rushes to the airport, convinces airport security to release Greg and brings him back to the Byrnes household.

Greg proposes to Pam. She accepts, and her parents agree that they should now meet Gregs parents. After Debbies wedding, Jack views footage of Greg recorded by hidden cameras that he had placed strategically around their house. As Jack watches Greg and hears what Greg has to say, Jack gets furious at Greg for the insults towards Jack and his son Deny, who Jack believes is still innocent with the drugs. Jack then turns the TV off with hostility dreading the fact that Greg is going to be his future Son-in-Law.

===Meet the Fockers (2004)===
 
 Oyster Bay, Long Island, to pick up Pams father, retired CIA operative Jack Byrnes (De Niro), her mother Dina (Danner) and one-year-old nephew Little Jack. But rather than going to the airport as planned, Jack decides to drive the family to Miami to meet the Fockers in his new RV.

Once they arrive, they are greeted by Gregs eccentric but fun-loving and amiable father, Bernie (Hoffman), and mother, Roz (Streisand), who is a sex therapist for elder couples. Worried that Jack may be put off by the Fockers lifestyle, Greg convinces Roz to pretend that she is a yoga instructor for the weekend. Though Jack and Bernie get off to a good start, small cracks begin to form between Jack and the Fockers, due to their contrasting personalities. Things are made worse when a chase between the Fockers dog, Moses, and the Byrns cat, Jinx, culminates with Jinx flushing Moses down the RVs toilet, forcing Bernie to destroy it to save Moses, and later on when Bernie accidentally injures Jacks back during a game of football.

Pam, meanwhile, informs Greg that she is pregnant, but the two decide to keep it a secret from Jack, who does not know they are having sex. Jack, however, becomes suspicious of Gregs character again when they are introduced to the Fockers housekeeper, Isabel Villalobos (Alanna Ubach), with whom Bernie reveals Greg had a sexual affair fifteen years before. Jack later takes the RV to Isabels fifteen-year-old son, Jorge (Ray Santiago), to fix the toilet, but is disturbed by Jorges striking resemblance to Greg and begins to suspect he may be Gregs son with Isabel. Meanwhile, Roz, Bernie and Dina realize Pam is pregnant, but promise not to tell Jack. Growing envious of Bernie and Rozs active sex life, Dina consults Roz on sex tips in order to seduce Jack, but none of them work.

Things eventually come to a crunch when Greg is left alone to babysit Little Jack, whom Jack has been raising via the Ferber method. Despite Jacks instructions to leave Little Jack to self-soothe, Greg cannot bear to listen to Little Jacks cries and tends to the boy to cheer him up, turning the television on, acting funny and inadvertently teaching Little Jack to say "asshole". A brief phone call from Roz is long enough for Little Jack to wander out of his pen (after Jinx opens it by accident), put on Scarface (1983 film)|Scarface and glue his hands to a rum bottle. After a furious argument with the Fockers and his own family (though amends are quickly made), Jack reverts to his old ways and sends Greg and Jorges hair samples for a DNA test, while inviting Jorge to the Fockers planned engagement party in hopes of getting Greg to admit he is Jorges father.
 a truth serum to make him talk. On stage, Greg blurts out that Pam is pregnant and that Jorge is indeed his son (in a comically Darth Vader-esque manner) before finally passing out. When the others realize Jacks actions the next day, another argument ensues and Dina admits that they all knew Pam was pregnant and deliberately did not tell him. Shocked and hurt by this, Jack leaves with his grandson. Bernie and Greg give pursuit, but are tasered and arrested by a corrupt deputy sheriff, LeFlore (Tim Blake Nelson), for speeding. Jack returns to defend them after being informed Greg is not Jorges father (his real father turns out to be a baseball player who also resembles Greg), but the overzealous LeFlore tasers and arrests him as well. In their cell, Greg, Jack and Bernie make up and are released by the local judge, Ira (Shelley Berman), who is a student of Roz and close friend of the Focker family.

Greg and Pam are married that weekend by Pams ex-fiancée (Owen Wilson). During the party, Jack asks Roz for some sex tips and sneaks into the RV with Dina.

During the credits, Jack watches hidden baby-cam footage of the Fockers giving attention to Little Jack over Jacks previous objections: Roz gives Little Jack chocolate, Bernie advises him to use his crying to disagree with everything Jack says, and Greg pretends to drunkenly tell Little Jack to keep it a secret that he left to smoke pot, not answer the phone, when he left Little Jack unattended which resulted in Jack gluing his hands to a rum bottle and that Pam is not really pregnant and only said it so that Jack would let them get married. Greg then pretends to only just discover the camera but then after making mocking gestures at it, you realize that Greg knew about it all along and none of the things he said before were true.

===Little Fockers (2010)===
 
Gaylord "Greg" Focker (Stiller) is preparing to celebrate his twins fifth birthday party. Things seem to go awry when Gregs father-in-law Jack Byrnes (De Niro) visits. Recently, Jack has been diagnosed with a heart condition and become embittered by his daughter Debbies divorce from her husband, Bob (whose marriage was the social event in Meet the Parents and how Jack was introduced to Greg), for cheating on her with a nurse. Jacks plan was originally to name Bob as his successor of the Byrnes family, but decides to pass the role to Greg, naming him "The Godfocker". Despite Greg begrudgingly accepting the role, Jack begins to suspect Greg of infidelity when he sees him with a drug rep, Andi Garcia (Alba), who openly flirts with him, and the presence of Sustengo erection pills in Gregs house, which prompts Jack to think Greg is no longer sexually attracted to his wife, Pam (Polo). Furthermore, Jack starts to doubt Gregs ability to provide for his family when he appears reluctant to send his children to a private school.

During a medical conference promoting Sustengo, Greg meets Bob at a bar. Bob tells Greg of Jacks original intention to name him as successor, "The Bobfather", and expresses relief and happiness at leaving Jacks family, which makes Greg slightly uncomfortable. Jack, for his part, starts to speak with Pam about the possibility of divorcing Greg and renewing her relationship with her ex-fiancée, Kevin Rawley (Wilson). Eventually, following a row at a clinic, Greg runs away from home to his and Pams unfinished new house, where he is paid a visit by Andi, who tries to cheer him up with takeout and wine, but Andi soon becomes so drunk that she makes an aggressive sexual pass at Greg. While looking for Greg to apologise and bring him home, Jack pulls up to the house and sees, through the window, what he believes to be Greg and Andi having sex while Greg is trying to rebuff Andis advances. Disgusted, Jack merely leaves, but tells Dina and Pam that he was unable to find Greg.

At the twins birthday party, Gregs parents, Bernie (Hoffman) and Roz (Streisand) rejoin the family, but Jack, enraged at Gregs apparent infidelity, engages Greg in a physical fight, despite Greg insisting that Andi was drunk and he was rebuffing her. The fight culminates with Jack having a heart attack and collapsing. As he is taken away by paramedics, Jack quietly admits that he now believes Greg after feeling his carotid artery, which remained stable while Greg was claiming his innocence.

Four months later on Christmas Day, Greg and Pams parents come to spend Christmas with them in their new house. Gregs parents (who are Jewish) give Jack a present of a kippah, informing him they traced his family roots back while nursing him back to health, discovering he is part Jewish. Bernie informs Greg and Pam that he and Roz have sold their island home and are moving to Chicago only two houses down from theirs. Jack and Dina decide they will move too, because they also want to be close to their grandchildren. The film ends with Greg and Pam trying to wean their parents off the idea.

During the credits, Jack is back in his home on Long Island, and with Mr. Jinx watches a video of Greg on YouTube in which Greg, at the Sustengo conference, mentions Jacks eccentric behavior. Jack then discovers a remixed version of the video using puns of several of the words in the video, much to Jacks slight amusement.

==Legacy==
The success of Meet the Parents was initially responsible for a 2002  , July 21, 2002. Accessed May 27, 2008.  Fenwick, Alexandra.  ,  , August 9, 2002. Accessed October 10, 2008.  Universal did not pursue any action against NBC but neither show lasted more than one season.
 Fort Lauderdale bomb squad FBI whose agents questioned the planes 176 passengers about the note.

==Reception==

===Critical reception===
{| class="wikitable" width=99% border="1" style="text-align: center;"
! Film
! Rotten Tomatoes
! Metacritic
|-
| Meet the Parents
| 84% (146 reviews)
| 73 (33 reviews)
|-
| Meet the Fockers
| 38% (157 reviews)
| 41 (34 reviews)
|-
| Little Fockers
| 10% (144 reviews)
| 27 (32 reviews)
|-
! Average ratings
! 44%
! 47
|}

===Box office performance===
{| class="wikitable" width=99% border="1" style="text-align: center;"
! rowspan="2" | Film
! rowspan="2" | Release date
! colspan="3" | Revenue
! colspan="2" | Rank
! rowspan="2" | Budget
! rowspan="2" | Reference
|-
! Box Office
! Foreign
! Worldwide
! All time domestic
! All time worldwide
|-
| Meet the Parents
|  
| $166,244,045	
| $164,200,000	
| $330,444,045	 
| 196
| 242
| $55,000,000
|  
|-
| Meet the Fockers
|  
| $279,261,160
| $237,381,779
| $516,642,939
| 58
| 103
| $80,000,000
|  
|-
| Little Fockers
|  
| $148,438,600
| $162,211,985	
| $310,650,585
| 241
| 268
| $100,000,000
|  
|-
! colspan=2 | Total
! $593,943,805
! $563,793,764
! $1,157,737,569
! 495
! 613
! $235,000,000
!  
|}

==Recurring characters==
{| class="wikitable"  style="text-align:center; width:99%;"
|-
! rowspan="2" style="width:16%;"| Character
! colspan="3"| Films
|-
! align="center" width="14%" | Meet the Parents
! align="center" width="14%" | Meet the Fockers
! align="center" width="14%" | Little Fockers
|-
!Jack Tiberius Byrnes
|colspan="3" | Robert De Niro
|-
!Gaylord Myron "Greg" Focker
|colspan="3" | Ben Stiller
|-
!Pamela "Pam" Martha Byrnes
|colspan="3" | Teri Polo
|-
!Dina Byrnes
|colspan="3" | Blythe Danner
|-
!Kevin Rawley
|colspan="3" | Owen Wilson
|-
!Bernard "Bernie" Focker
|colspan="1" style="background-color:#D3D3D3;" |
|colspan="2" | Dustin Hoffman 
|-
!Rosalind "Roz" Focker
|colspan="1" style="background-color:#D3D3D3;" |
|colspan="2" | Barbra Streisand
|-
!Isabel Villalobos
|colspan="1" style="background-color:#D3D3D3;" |
|colspan="1" | Alanna Ubach
|colspan="1" style="background-color:#D3D3D3;" |
|-
!Jorge Villalobos
|colspan="1" style="background-color:#D3D3D3;" |
|colspan="1" | Ray Santiago
|colspan="1" style="background-color:#D3D3D3;" |
|-
!Andi Garcia
|colspan="2" style="background-color:#D3D3D3;" |
|colspan="1" | Jessica Alba
|-
!Prudence Simmons
|colspan="2" style="background-color:#D3D3D3;" |
|colspan="1" | Laura Dern
|-
!Randy Weir
|colspan="2" style="background-color:#D3D3D3;" |
|colspan="1" | Harvey Keitel 
|-
!Nurse Louis
|colspan="2" style="background-color:#D3D3D3;" | Kevin Hart 
|-
|}

:Note: A gray cell indicates character did not appear in that medium.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 