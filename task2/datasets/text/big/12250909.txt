Amrutha Ghalige
{{Infobox Film name           = Amrutha Ghalige image          =  image_size     =  caption        =  director       = Puttanna Kanagal producer       = S. R. Rajan Bheema Rao K Nagarathna Puttanna Kanagal writer         = Dodderi Venkatagiri Rao based on       =   screenplay     = Puttanna Kanagal starring  Ramakrishna Padmavasanthi Sridhar
|music          = Vijaya Bhaskar cinematography = B. S. Basavaraj editing        = V. P. Krishna distributor    =  released       = 1984 runtime        = 139 minutes country        = India language       = Kannada budget         =  preceded_by    =  followed_by    = 
}}
 Sridhar in lead roles. 

== Plot==
This movie deals with the cause and consequences of teenage pregnancy. The heroine (Padmavasanthi), hailing from a poor background, falls in love with an affluent young man (Ramakrishna). The young man impregnates her and leaves her in the lurch. The heroines classmate (Sridhar) comes forward to marry her and give an identity to the child. Unfortunately, the classmate dies due to illness soon after. The young man comes back and unexpectedly meets his son. The classmate writes a letter that he had considered the heroine as his sister, and she was as pure as river Ganges. Finally the young man accepts the heroine. This movie  has very good songs and great lyrics.

==Cast== Ramakrishna
* Padmavasanthi Sridhar
* Umashree
* B. K. Shankar

==Soundtrack==
All songs are composed by Vijaya Bhaskar and lyrics written by Vijaya Narasimha.

{{Infobox album  
| Name = Amrutha 
| Type = soundtrack
| Artist = Vijaya Bhaskar
| Cover =
| Released = 1984
| Recorded = Feature film soundtrack
| Length =
| Label = Sangeetha
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|-
| 1 || Mayuri Natya Mayuri || S. P. Balasubramanyam, B. R. Chaya ||
|-
| 2 || Hindustanavu Endu  || Jayachandran ||
|-
| 3 || Hindustanavu Endu || B. R. Chaya ||
|-
| 4 || Parvathi Parashivana || S. P. Balasubramanyam, B. R. Chaya||
|-
|}

==Awards==
* Karnataka State Film Awards 1983-84 Best Screenplay - Puttanna Kanagal Best Cinematographer - B. S. Basavaraj Best Editor - V. P. Krishna

==References==
 

== External links ==
*  

 
 
 
 
 
 


 