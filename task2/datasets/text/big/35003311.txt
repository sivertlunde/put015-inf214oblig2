Desi Romeos
 
 
{{Infobox film
|name=Desi Romeos
|image=
|alt=
|caption=
|director=
|producer=
|writer=Babbu Maan
|starring=Babbu Maan Harjit Harman Shilpa Dhar Ravi Bal Bhupinder Gill Jasprem Dhillon
|music=Babbu Maan cinematography =
|editing=
|distributor=Maan Films
|released=  
|runtime=
|country=India
|language=Punjabi
|budget= 1 crore
|gross=6 crore
}}
Desi Romeos is a 2012 Punjabi film produced by Babbu Maan, 
starring Babbu Maan, Harjit Harman, Shilpa Dhar, Bhupinder Gill, Jasprem Dhillon, Raavi Bal in lead roles.   

==Cast==
* Babbu Maan - Maan
* Harjit Harman - Sandhu
* Shilpa Dhar - Urmilla
* Jasneer Kaur - Lali
* Bhupinder Gill - DJ Jaggadi
* Bittu - Om
* Mitesh - Oshu
* Jasprem Dhillon - Mirza
* Raavi Bal - Ehsaaz
* Sherry Uppal  - Manna

== Plot ==
 
Desi Romeos is the superhit film in Punjabi cinema and it is the story of six boys who are popular in college for their music which leads to jealousy of Mirza group.The “ROMEOS” are : R- Randhawa (Babbu Maan), O- Om (Bittu), M-Maana (Sherry), E-Ehsaas ( Raavi), O-Osho (Mitesh), S-Sandhu (Harjit Harman).Desi Romeos is also the story of their journey from the days of the hostel to house no. 55, where they stay on rent and where romance blossoms between them and the girls (JULIET) staying as PG (Paying Guest)in the house opposite them to the jail. Who called upon by people as good for nothing, achieve success through their musical talent.‘Desi Romeos’ is a fun filled story of a normal group of boys with lots of action, romance and music, keeping you entertained throughout.    Desi Romeos. 15 May 2012. Retrieved 15 May 2012 

== Music ==

Music was released on 25 May.

Track Listings

#.Kabootri (Remix)
#.Output Zero
#.Sohaniyan
#.Sardari
#.Chandigarh
#.Mutiyar
#.Why Dont You
#.Kabootri

==References==
 

 
 


 