Get on Up (film)
 
{{Infobox film
| name           = Get on Up
| image          = Get On Up poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Tate Taylor
| producer       = Brian Grazer Mick Jagger Tate Taylor Victoria Pearman
| screenplay     = Jez Butterworth John-Henry Butterworth
| story          = Steven Baigelman Jez Butterworth  John-Henry Butterworth Keith Robinson Octavia Spencer
| music          = Thomas Newman
| cinematography = Stephen Goldblatt
| editing        = Michael McCusker Jagged Films Wyolah Films Universal Pictures
| released       =  
| runtime        = 139 minutes  
| country        = United States
| language       = English
| budget         = $30 million http://online.wsj.com/articles/james-brown-the-making-of-get-on-up-1406228578 
| gross          = $31.9 million 
}} biographical drama Jez and Keith Robinson as Baby Roy, and Octavia Spencer as Aunt Honey. The film was released on August 1, 2014. 

==Plot==
The film opens in 1993 with James Brown walking through a darkened hallway as an audience chants his name. He hears the voices of people he knew throughout his life. The film then cuts to 1988 in Augusta, Georgia|Augusta, Georgia (U.S. state)|Georgia; James learns that his private bathroom in a strip mall he owns was used without his consent. As he confronts and then forgives the trespasser, he accidentally fires a shotgun, attracting the police.

During the 1960s, James and his band decide to travel to Vietnam to show support to the black troops, where they put on a well-received show. In 1939, James is raised in the woods by his parents (Susie and Joe Brown), whose marriage is fraught with financial struggles and physical abuse. James performs in a singing group, The Famous Flames, formed by Bobby Byrd, whose family sponsored his release from prison, a penalty he paid for stealing a suit. James lives with the Byrd family and becomes lead singer of Bobbys group. In 1964, manager Ben Bart convinces them to let The Rolling Stones close The T.A.M.I. Show instead of The Flames. The Flames upstage the Stones, and, exiting the stage, James tells the Stones, "Welcome to America". In James childhood, Susie leaves Joe, and Joe threatens her with a gun and keeps James. Joe continues to abuse James until Joe joins the army. James is left living with and working for his Aunt Honey, who runs a brothel. At her home, he attends church and enjoys the choir.
 battle royal boxing match while a band plays. Inspired by the funky band, James wins the match.
 King Records, Pee Wee Ellis, Nafloyd Scott, and Baby Roy.The Famous Flames singing group is also re-formed, replacing the members that quit. The Flames perform at the Apollo Theater to an excited audience. After the show, Bobby tells James that a lady claiming to be his mother is there. As a young child James had seen Susie with a soldier, to whom she claimed she didnt know James. Aunt Honey consoled James, saying that his mother was a fool and James would someday be rich.

James has a child, Teddy, with his first wife Velma. He later divorces her and marries Dee-Dee. On one occasion, the couple hosts a Christmas event. Afterwards, James hits Dee-Dee for wearing a revealing outfit. In an attempt to reach out to the black community, James records the song "Say It Loud - Im Black and Im Proud" (1968) with a group of children. James convinces the Boston Garden manager to not cancel a performance following the assassination of Martin Luther King, Jr.. Several people try to get on stage; security guards intercede until James controls the audience.
 joint laced angel dust. The police chase James and arrest him.
 Try Me (I Need You)" moves Bobby and his wife Vicki to tears, and the audience cheers.

==Cast==
 
* Chadwick Boseman as James Brown 
* Nelsan Ellis as Bobby Byrd 
* Dan Aykroyd as Ben Bart 
* Viola Davis as Susie Brown  Keith Robinson as Baby Roy 
* Octavia Spencer as Aunt Honey 
* Lennie James as Joseph "Joe" James 
* Fred Melamed as Syd Nathan Craig Robinson as Maceo Parker 
* Jill Scott as Deidre "Dee-Dee" Jenkins 
* Josh Hopkins as Ralph Bass 
* Brandon Mychal Smith as Little Richard 
* Tika Sumpter as Yvonne Fair 
* Aunjanue Ellis as Vicki Anderson  Tariq Trotter Pee Wee Ellis
* Aloe Blacc as Nafloyd Scott
* Jamarion and Jordan Scott as young James Brown 
* Nick Eversman as Mick Jagger 
* James DuMont as Corporal Dooley 
* Kirk Bovill as Announcer 
* Ralph Tresvant as Sam Cooke Bobby Bennett   
* Allison Janney as a hotel guest
* Codie Wiggins (uncredited) as Baby Lloyd Stallworth  
* Dexter Allen as bassist Sam Thomas  

==Production== Jez and The Help) Universal Pictures set October 17, 2014 as a release date for the film, previously untitled.  Later, on November 13, Universal shifted the release date of the biopic from October to August 1, 2014.   

===Casting=== USO envoy named Corporal Dooley.    On October 30, 2013, Kirk Bovill also joined the cast of the film.    Jill Scott and Dan Aykroyd were added on October 31; Scott played Browns wife while Aykroyd played Ben Bart, the President of one of New Yorks largest talent agencies Universal Attractions Agency.   
 Keith Robinson Jackson on January 7, 2014.   

===Filming=== Shooting began Thalia Mara Hall, and they shot other scenes at Mississippi Coliseum, Capitol Street, and some of the restaurants in Jackson.  In total Get on Up  was shot in 49 days. 

==Release==
On September 1, 2014, it was announced that the film would be the opening film of the 2014 Zurich Film Festival. 

===Marketing=== trailer of the film.   A second official trailer was released on May 20. 

===Critical reception===
Get on Up was met with positive reviews from critics, with praise mainly going to Bosemans performance. The film currently has a rating of 80% on the review aggregator site  , the film has a score of 71 out of 100, based on 44 critics, indicating "generally favorable reviews".   Additionally, Brandon Smith received praise from critics for his brief but memorable role as Little Richard.   

Less favorable reviews include "Get On Up is a cagey, shapeless James Brown biopic" by Ignatiy Vishnevetsky, who rated the film D+ at the AV Club,  Several other critics noted key facts and incidents omitted in the film,   in articles such as "The Social Activist Side of James Brown You Won’t See In Get On Up",  "The Great Man Theory of Funk: Get On Up shows us James Brown the unstoppable personality, but skimps on James Brown the musician",  and "12 Crazy James Brown Moments You Wont See in Get on Up". 

===Box office=== Guardians of the Galaxy ($94,320,883) and Lucy (2014 film)|Lucy ($18,252,590). 

As of September 22, 2014, Get on Up has grossed $30,569,935, against a $30 million budget.   

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 