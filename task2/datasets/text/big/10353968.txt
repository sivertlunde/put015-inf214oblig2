Miss Pettigrew Lives for a Day
{{Infobox film
| name = Miss Pettigrew Lives for a Day
| image = Miss pettigrew lives for a day.jpg
| caption = Theatrical release poster
| director = Bharat Nalluri
| producer = Nellie Bellflower Stephen Garrett
| writer = David Magee Simon Beaufoy
| based on =  
| starring = Frances McDormand Amy Adams Ciarán Hinds Lee Pace
| music = Paul Englishby
| cinematography = John de Borman
| editing = Barney Pilling
| distributor = Focus Features
| released =  
| runtime = 91 minutes
| country = United Kingdom United States
| language = English
| gross = $16,724,933
}}

Miss Pettigrew Lives for a Day is a 2008 romantic comedy film directed by Bharat Nalluri. The screenplay by David Magee and Simon Beaufoy is based on the 1938 novel of the same name by Winifred Watson.

==Plot== American singer-actress Delysia Lafosse (Amy Adams) wants a social secretary rather than a nanny.
 West End high society. She is given a makeover by her new employer, and at a fashion show hosted by fashion maven Edythe Dubarry, she meets and feels attracted to lingerie designer Joe Blomfield, who is involved in a tempestuous relationship with Edythe.
 screwball comedies Queen Mary Victoria Station by Joe, who is convinced that she is the woman for him and has been looking for her all night. They leave the station together, arm in arm.

==Cast==
* Frances McDormand as Guinevere Pettigrew
* Amy Adams as Delysia Lafosse
* Lee Pace as Michael Pardue Tom Payne as Phil Goldman
* Mark Strong as Nick Calderelli
* Shirley Henderson as Edythe Dubarry
* Ciarán Hinds as Joe Blomfield
* Christina Cole as Charlotte Warren
* Stephanie Cole as Miss Holt

==Production==
 
 musical to bombed Pearl Harbor and the project was shelved.

In 1954, Universal renewed the rights, but the property remained undeveloped. When producer Stephen Garrett discovered the book and proposed an adaptation to executives at Focus Features, he learned the rights still belonged to Universal which, as the parent company of Focus, allowed him to proceed with the project.

Filming locations included the Theatre Royal, Drury Lane in Covent Garden, Whitehall Court in Westminster, and Pimlico. Interiors were shot in the Ealing Studios.

==Critical reception==
The review aggregator Rotten Tomatoes reports 78% of critics gave the film positive reviews, based on 117 reviews,  and Metacritic reported the film had an average score of 63 out of 100, based on 26 reviews. 

Stephen Holden of the New York Times called the film "an example of how a little nothing of a story can be inflated into a little something of a movie with perfect casting, dexterous tonal manipulation and an astute eye and ear for detail." He praised Amy Adams, saying the "screen magic" she displays "hasnt been this intense since the heyday of Jean Arthur," and he noted Frances McDormand achieved her "metamorphosis from glum stoicism to demure radiance with impressive comic understatement." 

In the San Francisco Chronicle, Ruthe Stein called the film "a swell adaptation" and added, "Frothy and exuberantly entertaining&nbsp;– in part because of the sexual innuendoes&nbsp;– its the best romantic comedy so far this year&nbsp;... Director Bharat Nalluri gives Miss Pettigrew Lives for a Day the patina of a film actually made in the 1930s." 

Todd McCarthy of Variety (magazine)|Variety said of the actors, "McDormands performance slowly builds a solid integrity, and contrasts well with Adams more flamboyant turn, which initially accentuates Delysias constant role playing but eventually flowers into a gratifyingly full-fledged portrayal of a woman with a past she wishes to escape. Hinds puts real feeling into his work." 

==Box office performance==
In its opening weekend in the United States and Canada, the film earned $2,490,942 on 535 screens, ranking #11 at the box office. It eventually grossed $12,313,694 in the US and Canada and $4,411,239 in other markets for a total worldwide box office of $16,724,933. 

==Music==
The films score was written and conducted by Paul Englishby, for which he won the ASCAP Award in 2009. Englishby also arranged and conducted three additional songs for the film:

*"Brother, Can You Spare a Dime?" musical Americana (musical)|Americana.

*"Taint What You Do (Its the Way That You Do It)" jazz musicians Melvin "Sy" James "Trummy" Young. It was first recorded in 1939 by Jimmie Lunceford, Harry James, and Ella Fitzgerald.

*"If I Didnt Care" Bill Kenny, in 1939.
**Amy Adams and Lee Pace recorded and performed the song during the films climactic scene at Nicks nightclub.

===Other songs=== Anything Goes"
**Written by Cole Porter for his 1934 musical, Anything Goes. Lew Stone and His Band, with vocals by The Radio Three (a British close-harmony trio similar to the Boswell Sisters), was featured in the film as Delysia and Miss Pettigrew headed to the fashion show.

*"Dream (1944 song)|Dream" jazz and pop standard with words and music written by Johnny Mercer in 1944 in music|1944.
**Even though the film takes place in 1939, The Pied Pipers 1945 recording of "Dream" can be heard playing in the background, as if on a radio, as Delysia bathes.

== References ==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 