Attack from Space
{{Infobox film name            = Attack from Space image           = Attack_From_Space.jpg image size      = 190px caption         = Alpha Video DVD cover art director        = Teruo Ishii producer        = Mitsugi Okura writer          = Ichiro Miyagawa starring        = Ken Utsui music           = cinematography  = editing         = distributor     =Walter Manley Enterprises Inc. released        = 1964 runtime         = 76 min. (USA) language  English (dubbed) budget          =
|}}
Attack from Space is a 1964 film edited together for American television from the 1957 films #5 and #6 of the Japanese short film series Super Giant.  It is currently in the public domain.

==Plot==
The superhero Starman is sent by the Emerald Planet to protect Earth from belligerent aliens from the Sapphire Galaxy. The Sapphireans (or "Spherions") kidnap Dr. Yamanaka and force him to use his spaceship against the Earth.

==American adaptation==
The nine Super Giant films were purchased for distribution to U.S. television and were edited into four films by Walter Manley Enterprises and Medallion Films. The two original Japanese films that composed Attack from Space (The Artificial Satellite and the Destruction of Humanity and The Spaceship and the Clash of the Artificial Satellite) were each 39 minutes in duration. The two films were edited into one 76-minute film, resulting in only two lost minutes in the combined edit. As a result, this compilation suffered least among the Super Giant films in its adaptation for American television. Although Riichiro Manabe composed the music to the original two films, most of the original music was replaced by library tracks in the American adaptation.

==DVD releases==
Attack from Space is currently available on two DVD releases. Something Weird Video with Image Entertainment released this film and the other compiled Starman film, Evil Brain from Outer Space on a single disc on December 10, 2002. Alpha Video also released a budget-priced disc of Attack from Space by itself on March 23, 2004.

== In other media == Ron Sparks.

== See also ==
* Super Giant
* Atomic Rulers of the World
* Invaders from Space
* Evil Brain from Outer Space

==References==
*Ragone, August. THE ORIGINAL "STARMAN"; the Forgotten Supergiant of Steel Who Fought for Peace, Justice and the Japanese Way Originally published in Planet X Magazine, included in Something Weird Videos DVD release.
 

== External links ==
*  
*  
*   at Monster Shack Movie Reviews

 
 
 
 
 
 
 
 


 
 