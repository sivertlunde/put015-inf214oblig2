Rebel Without Claws
 
{{Infobox Hollywood cartoon|
| cartoon_name = The Rebel Without Claws
| series = Looney Tunes (Tweety and Sylvester (Looney Tunes)|Sylvester) Arthur Davis Tom Ray| voice_actor = Mel Blanc| musician = Milt Franklyn
| producer = 
| story_artist = Friz Freleng Warner Bros. Pictures
| release_date = July 15, 1961
| color_process = Technicolor
| runtime = 7 mins
| movie_language = English
}}

The Rebel Without Claws is a "Looney Tunes" cartoon animated short starring Tweety and Sylvester (Looney Tunes)|Sylvester. Released July 15, 1961, the cartoon is written and directed by Friz Freleng. The voices were performed by Mel Blanc.

The cartoon, one of a number of Warner Bros. cartoons set during the American Civil War, is a play on the movie title Rebel Without a Cause.

==Story== Union and its Army. Likewise, the short is a remake of the 1944 short Plane Daffy, albeit with WWII references replaced by Civil War environment and other politically correct changes.
 General Robert E. Lee, but all the carrier pigeons have been shot down. The soldiers realize that Tweety is their last hope and turn to him for their mission. The Union soldiers learn of the Confederates attempt and counter with their "Messenger Destroyer," who turns out to be none other than Sylvester. "I tawt I taw a damn Yankee tat," says Tweety just before the chase begins.

The bulk of the cartoon uses battle gags, such as Sylvester getting blown out of a cannon; Tweety momentarily tricking Sylvester into thinking Union soldiers are marching to battle (Sylvester tries to confront the canary but is blown away by Confederate soldiers); and Tweety hiding behind cannons on a fighter ship (Sylvester takes the brunt of more explosions).
 firing line for execution. He states that his only regret is that he has "but one wife to give foh my countwy" (paraphrasing Nathan Hale), to which Sylvester says that he has nine lives, But the commander and his soldiers prove incompetent — they shoot Sylvester instead! "Its a good thing I have got nine lives! With this kind of an army, Ill need em!"

==Edited versions== WB Channel mutes out the "damn" in the Confederate Officers line "Damn Yankees!" and deletes Tweetys line "I tawt I taw a Damn Yankee Tat!".   
* Cartoon Networks version of this cartoon, much like The WBs version, also removes the two usages of the word "Damn". Unlike The WB however, CN used a fake blackout to end the scene where the Confederate Officer gives his sidekick the letter to deliver to General Lee early to remove the officers line "Damn Yankees!" and shortened Tweetys line "I tawt I taw a Damn Yankee tat!" to "I tawt I taw a Yankee tat!"  
* The version of this cartoon that aired on the syndicated "Merrie Melodies" show left in both uses of the word "damn", but cut a scene in the middle of the cartoon where Sylvester pursues Tweety on a ship and gets blasted by cannons (though this cut scene was shown in a "Hip Clip" on another episode of "The Merrie Melodies Show") 

==Bibliography==
* Friedwald, Will and Jerry Beck. "The Warner Brothers Cartoons." Scarecrow Press Inc., Metuchen, N.J., 1981. ISBN 0-8108-1396-3.

==References==
 

==External links==
* 

 
{{succession box |
before= Trip For Tat | Tweety and Sylvester cartoons |
years= 1961 |
after= The Last Hungry Cat|}}
 

 
 
 
 
 