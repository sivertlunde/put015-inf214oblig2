Aa Naluguru
{{Infobox film
| name           = Aa Naluguru
| image          = Aa Naluguru (film).jpg
| caption        =
| director       = Chandra Siddhartha
| producer       = Sarita Patra Madan
| screenplay     =
| story          =
| based on       =   Rajendra Prasad   Aamani  Subhalekha Sudhakar
| music          = R. P. Patnaik
| cinematography = Surendra Reddy
| editing        = Girish Lokesh
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu drama Rajendra Prasad and Amani. The film was directed by Chandra Siddhartha and produced by Sarita Patra. The film was later remade in Kannada in 2006 as Sirivantha starring Vishnuvardhan (actor)|Vishnuvardhan.

==Plot==
Raghu Ramayya (Rajendra Prasad) is a kind hearted idealistic person who spends half of his income on charity work. His wife Bharati (Aamani), two sons Chinna and Raja and daughter Revathi are quite against this attitude. His children compel him to get money for their career (bribe for job), education (donation fee for engineering seat) and settling in United States which he feels is completely wrong. He is forced to set aside his morality and take a loan from his neighbor, Kotayya (Kota Srinivasa Rao). Unable to bear the defeat of his ideology and moral issues, he dies consuming poison the very day he gives money to his children. The rest of the film is about how his children and wife realise just how important he was as they prepare his funeral. At last, every one had realised that after death only love and affection will come with us so love the people and society.

==Cast== Rajendra Prasad
* Aamani
* Raja
* Subhalekha Sudhakar
* Kota Srinivasa Rao
* Premkumar
* Bhagavan kumar
* Revathi

==Crew==
* Director: Chandra Sidhartha
* Producer: Sarita Patra
* Music: RP Patnaik
* Screenplay: Madan, Chandra Sidhartha
* Story: Madan
* Dialogues: Madan
* Editing: Girish Lokesh
* Camera: Surendra Reddy
* Banner: Prem Movies

==Soundtrack==
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s)
|-
| "Inko Rojochindandi"
| S. P. Balasubramaniam, Balaji
|-
| "Okkadai Ravadam"
| S. P. Balasubramaniam
|-
| "GUndepai Thannuthoo" Usha
|-
| "Naluguroo Mechinaa"
| S. P. Balasubramaniam
|-
| "Good Morning"
| R. P. Patnaik
|-
| "Wish You Happy Married Life"
| R. P. Patnaik
|}

== Accolades == Nandi Award Best Feature Film in the year 2004. Rajendra Prasad Nandi Award Best Actor for his  role (Raghu Ramayya) in the movie. Nandi Award Best Character Actor for his role (Kotayya) in the movie.

==External links==
*  

 
 
 
 
 


 