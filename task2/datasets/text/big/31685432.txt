Abhayam (1991 film)
 
{{Infobox Film
| name           = Abhayam
| image          = 
| caption        = 
| director       = Sivan CFSI
| writer         = Shibu Chakravarthy
| based on       =  Madhu Parvathy Parvathy Raghavan Raghavan Tarun Kumar Prema Menon Kottayam Santha Ramachandran Baby Ambili 
| music          = 
| cinematography = Santosh Sivan
| editing        = A. Sreekar Prasad
| studio         = 
| distributor    =  
| released       =  
| runtime        = 93 minutes
| country        = India
| language       = Malayalam
| budget         =  
| gross          = 
}}
 Master Tarun Raghavan and Baby Ambili. The film gets a dubbed release in Hindi as Main Phir Aaunga.

==Plot==
Eight-year-old Vinu is an artistically inclined boy, who is irresistibly drawn to Nature. His parents, however, try to pin him to a rigid routine. Vinu finds that he is a prisoner of time in terms of his daily chores. He finds solace in thinking about his loving grandfather and his village. One day Vinu runs away. In the course of his journey, he meets and interacts with many people and faces many unusual adventures and encounters. Vinu finally reaches his destination – his grandfathers village.   

==Awards==
The film has won the following awards. 
; 7th International Childrens Film Festival, India
* Silver Elephant Award
* Special Jury Award National Film Awards Best Childrens Film
; Film Festival Raggazzi, Bellinzona, Switzerland
* Prize "Environment and Quality of Life" signed by "Ecology Jury" composed of 14-16 children to the Director.
; 1st Uruguay Children Film Festival
* Special Mention and Award statue made of earthen and Certificate to the Director.

==References==
 

 

 
 
 
 


 
 