Diary of the Dead
 
 
{{Infobox film
| name           = Diary of the Dead
| image          = DiaryofDeadPoster2.jpg
| caption        = Theatrical release poster
| director       = George A. Romero
| producer       = George A. Romero Peter Grunwald Sam Englebardt Artur Spigel Dan Fireman John Harrison Ara Katz
| writer         = George A. Romero Michelle Morgan Josh Close Shawn Roberts Amy Lalonde Joe Dinicol Scott Wentworth Philip Riccio Chris Violette Tatiana Maslany
| music          = Norman Orenstein
| cinematography = Adam Swica
| editing        = Michael Doherty
| studio         = Artfire Films Romero-Grunwald Productions
| distributor    = The Weinstein Company
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $2 million
| gross          = $5.3 million   
}} cinemas on February 15, 2008    and on DVD by The Weinstein Company and Genius Entertainment on May 20, 2008.
 zombie films. It is not a direct sequel to previous films in the series, but occurs within the same universe of the original trilogy according to Romero.

==Plot==
The film begins with footage from a news cameraman and reporter, who are covering a story about an immigrant man killing his wife and son before committing suicide. The son and wife turn into zombies and kill several medical personnel and police officers but leave one medic and a reporter bitten before being killed. The narrator, Debra, explains most of the footage was never broadcast, but was recorded by the cameraman.

A group of young film studies students from the University of Pittsburgh are in the woods making a horror film along with their faculty adviser, Andrew Maxwell, when they hear news of an apparent mass-rioting and mass murder. Two of the students, Ridley and Francine, decide to leave the group, while the project director Jason goes to visit his girlfriend Debra (the narrator). When she cannot contact her family, they travel to Debras parents house in Scranton, Pennsylvania. En route Mary runs over a reanimated highway patrolman and three other zombies. The group stops and Mary attempts to kill herself. Her friends take her to a hospital, where they find the dead becoming zombies, and thereafter fight to survive while traveling to Debras parents. Mary becomes a zombie and is slain by Maxwell. Later Gordo is bitten by a zombie. His girlfriend Tracy begs the others not to shoot him immediately but later is forced to shoot him herself. Soon they are stranded when their vehicles fuel line breaks. They are attacked by zombies while Tracy repairs the vehicle with the assistance of a deaf Amish man named Samuel. Before escaping, Samuel is bitten and kills himself and his attacker with a scythe. 
 National Guard. different National Guardsmen, who rob them, leaving them only their weapons. They arrive at Ridleys mansion, where Ridley explains that his parents, the staff, and Francine were killed by zombies. He then imprisons Debra and Tony and is revealed as a zombie himself. He kills Eliot and attacks Tracy and Jason. Jason is able to distract Ridley long enough for Tracy to escape. Tracy then leaves the group in the groups RV. The remaining survivors then hide in an enclosed shelter within the house, with the exception of Jason, who left the group to continue filming. He is then attacked and infected by Ridley. Maxwell kills Ridley with an antique sword and Debra kills Jason, and continues filming. However, a large number of zombies begin to attack the mansion, forcing the survivors to take shelter in the mansions panic room.

At the end of the film, Debra watches Jasons recording of a hunting party shooting people who were left to die and be reanimated as shooting targets, and wonders if the human race is worth saving.

==Cast==
 
*Shawn Roberts as Tony Ravello
*Joshua Close as Jason Creed Michelle Morgan as Debra Moynihan
*Joe Dinicol as Eliot Stone
*Scott Wentworth as Andrew Maxwell
*Philip Riccio as Ridley Wilmott
*George Buza as Biker
*Amy Lalonde as Tracy Thurman
*Tatiana Maslany as Mary Dexter  
  R .D. Reid as Amish Farmer
*Tino Monte as Newscaster
*Megan Park as Francine Shane
*Martin Roach as Stranger
*Alan van Sprang as Colonel
*Matt Birman as Zombie Trooper
*Laura DeCarteret as Bree
*Janet Lo as Asian Woman
*Rebuka Hoye as Zombie
*Todd William Schroeder as Brody
*Alexandria DeFabiis as Zombie
*Nick Alachiotis as Fred
*George A. Romero as Chief of Police
*Boyd Banks as Armorist
*Gregory Nicotero as Zombie Surgeon
*Chris Violette as Gordo Thorsen
 

Quentin Tarantino, Wes Craven, Guillermo del Toro, Simon Pegg, and Stephen King lend their voices as newsreaders in the film. 

==Re-establishing the Dead franchise== Dead series 
{{cite web
| first=Nina
| last=Kincaid
| url=http://www.flixens.com/script_review_romeros_diary_of_the_dead
| title=Script Review: Romeros "Diary of the Dead"
| publisher=Flixens
| date=2006-08-30
| accessdate=2006-09-14 archiveurl = archivedate = September 2, 2006}}
  and there are some notable references to earlier Romero films, as when the news track from   (2005 in film|2005), was studio-produced through Universal Studios, Diary of the Dead was produced by Romero-Grunwald Productions, formed by Romero and his producer friend Peter Grunwald, with Artfire Films.  
{{cite news
| first=Pamela
| last=McClintock
| url=http://www.variety.com/article/VR1117948964?categoryid=1236&cs=1&query=romero&display=romero
| title=Romero will raise Dead
| publisher=Variety.com
| date=2006-08-24
| accessdate=2006-09-14
}}
 

==Production==
Romero announced the film in August 2006 after signing a deal to write and direct it and filming began its four-week shoot in Toronto on October 19, 2006. 

Romero also made an extensive use of computer-generated imagery because it allowed him to shoot the film quickly and add the effects later. Also, the films style, as if shot with hand-held cameras, necessitated a shift from his usual method of working, which involves filming multiple camera angles and assembling scenes in the editing room. Instead, Romero filmed much of the action in long, continuous takes: "The camera was 360, so everybody was an acrobat, ducking under the lens when the camera came past you," said Romero. "The cast was great. They had a lot of theater experience. I think they could have gone from scene one all the way to the end of the movie, all in a single shot." 


==DVD and Blu-ray releases== Behind the Scenes featurette, and five short films that came about via a MySpace contest. It was released the same day as a new authorized edition of Night of the Living Dead on DVD was released by The Weinstein Company. 

The film was released on Region 2 on June 29, 2008, in single disc,  double disc and Blu-ray editions.  The double-disc and Blu-ray both contained a UK exclusive interview from Frightfest 08, and a feature length documentary entitled One for the Fire - The Legacy of Night of the Living Dead. The double-disc edition was released in limited, numbered steelbook packaging, and online retailer Play.com sold an exclusive edition in a slipcase.  On October 21, 2008, a Blu-ray version was released in the United States.

==Reception==
George Romero won a 2008 Critics Award for Diary of the Dead. The film received mixed to positive reviews. The film currently has a "fresh" rating of 62% on Rotten Tomatoes. 

==References==
 

== External links ==
 
*  
*  
*   at  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 