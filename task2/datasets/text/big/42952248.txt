Isusumbong Kita Sa Tatay Ko
{{Infobox film
| name           = Isusumbong Kita Sa Tatay Ko
| image          = IsusumbongKitaSaTatayKo.jpg
| caption        = Isusumbong Kita Sa Tatay Ko theatrical movie poster
| director       = Boots Plata  . IMDB. Retrieved 2014-06-03.  Isusumbong Kita Sa Tatay Ko. Dir. Boots Plata. Perf. Fernando Poe Jr. and Judy Ann Santos. Star Cinema, 1999. Film. 
| producer       = Charo Santos-Concio and Malou N. Santos  
| writer         = Dindo Perez  
| sound          = Roadrunner Network Inc. and FPJ Productions  
| starring       = {{plainlist|
* Fernando Poe Jr.
* Judy Ann Santos
}}
| music          = Jaime Fabregas  
| cinematography = Ver Reyes  
| editing        = Augusto Salvador  
| studio         = Star Cinema 
| distributor    = Star Cinema
| released       =  
| runtime        = 102 minutes 
| country        = Philippines English
| budget         = 
| gross          = P100+ million
}}

Isusumbong Kita Sa Tatay Ko is a 1999 Filipino action film|action-comedy film directed by Boots Plata starring Fernando Poe Jr. and Judy Ann Santos. The film, produced and distributed by Star Cinema premiered in the Philippines on June 9, 1999.  . IMDB. Retrieved 2014-06-03. 

The film earned around P10 million pesos on the first day of its release.    By the end-run, the film earned more than 100 million  s first and only film under Star Cinema. 

==Synopsis==
"Badong (Fernando Poe Jr.|FPJ) has been raising up his only child, Joey, ever since his wife died at an early age. Consequently, Joey grows up like her father, a boyish person who acts like a man. Together with Lola Benedicta (Anita Linda), Badong and Joey had been living happily, with the former protecting his child while the latter is helping his father with his auto repair shop. One day, Badongs childhood friend, Beth (Aileen Damiles), visits their location and they became close. However, Joey feels that Beth is trying to replace her late mother and that Beth is destroying her relationship with her father.  . via Star Cinema. Retrieved 2014-06-03. 

==Full cast and characters==

===Main cast===
*Fernando Poe Jr. as Badong Rivera
*Judy Ann Santos as Joey Rivera

===Supporting cast===
{{Columns-list|2|
*Aileen Damiles	as Beth
*Anita Linda as Lola Benedicta
*Paquito Diaz as Sarge
*Kier Legaspi as Archie
*Berting Labra as Mang Cosme
*Leandro Muñoz as Caloy
*Robert Arevalo as Alfredo
*Angel Aquino as Alicia Rivera
*Kathleen Hermosa as Jenny
}}

===Others===
{{Columns-list|2|
*Zernan Manahan	as Joeys neighbor
*Ama Quiambao as Beths aunt
*Mar Garchitorena as Congressman (Archies uncle)
*Polly Cadsawan	the Congressmans bodyguard
*Butch Achacoso	as the Brgy. Chairman
*Gary Manansala Kagawad
*Maria Gonzales	Nurse
*Caloy Alcantara Guitarist
;Archies men
*Manjo del Mundo
*Nonoy De Guzman
;Archies friends
*Gerald Ejercito
*R.G. Gutierrez
*Gian Achacoso
*Jaypee Plata
*Anne Lorraine as Anna Lorraine
*Sharon Malapitan
*Lovely Mansueto
;Archies goons
*Bert Garon
*Ernie Madriaga
*Boy Dalanon
;Talyer workers
*Baby Mallanes
*Tom Alvarez
*Bernie Garcia
*Rey Abella
*Edgar Madriaga as Ed Madriaga
;Market goons
*Telly Babasa
*Ding Alvaro
*Jun Milan as Jhun Melan
*Mack Gomez
*Dardo De Oro
*Rey Comia
;Policemen
*Ramon Fernandez as Mon Fernandez
*Reynald Laranjo
*Celso Balte
*Tony Gahuman
;Painted men
*Alih Leal as Ali Leal
*Ronnie Paroni
*Raymond Alonzo
*Boy Surposa
*Jun Medrazo
*Harry Villa
*Eric Fresnido
*Robert Sanchez
}}

==Production==
The production team started filming during the first week of April 1999. During this time, Poe Jr. revealed that his first project with Santos under Star Cinema was first planned in 1997.  . Philippine Headline News Online. Retrieved 2014-06-03.   However, the team has had problems with the production fees because of the substantial cost-cutting over the years that they had to reduce the talent fees of the stars/cast. 
 eighth batch of ABS-CBNs Star Magic. On the other hand, his co-star, Santos, was thrilled to be her partner since they were former schoolmates in real life at Mt. Carmel High School, Quezon City.   

===Music===
The theme song of the film is entitled "Awitin Mo at Isasayaw Ko" (Youll Sing and Ill Dance), which was created by Joey De Leon and Vic Sotto of VST & Co., and performed by the stars Fernando Poe Jr. and Judy Ann Santos. It also features Rey Valeras self-composed song "Kung Tayoy Magkakalayo" (If Ever Well Be Apart). 

==Reception==

===Box-office=== pesos List box office Guillermo Mendoza Memorial Scholarship Foundation Citation.  . Abante. Retrieved 2014-05-31. 

===Critical response===
The film received predominately favorable reviews besides its commercial success. For instance, Sol Jose Vanzi of the Philippine Headline News Online noted, "Its a match produced in celluloid heaven, because he   is   king of Philippine movies while she   is considered todays drama princess." 

==Planned sequel==
A few weeks after its release, the production team was already planning for a possible sequel.  The next year, in 2000, the plans for the sequel was mentioned by Poe Jr. a couple of times. He voiced out that he was not satisfied with the script that he wants to have a sequel with his co-star Santos. If that happens, the sequel, Magpaalam Ka Muna sa Tatay Ko will have a different story line.   Their plans were not pushed through, however, especially not after Poe Jr. had died 2004. 

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|-
| rowspan="2" align="center"| 2000
| rowspan="2"| GMMSF Box-Office Entertainment Awards 
| Box-Office King
| align="center"| Fernando Poe Jr.
|  
|- GMMSF Box-Office Judy Ann Santos|| 
|}

==References==
 

==External links==
*  

 
 
 
 
 
 