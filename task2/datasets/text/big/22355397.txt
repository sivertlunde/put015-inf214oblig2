The Land (1969 film)
 
{{Infobox film
| name           = The Land
| image          = 
| caption        = 
| director       = Youssef Chahine
| producer       = 
| writer         = Abderrahman Charkawi Hassan Fuad
| starring       = Hamdy Ahmed
| music          = 
| cinematography = Abdelhalim Nasr
| editing        = Rashida Abdel Salam
| distributor    = 
| released       =  
| runtime        = 130 minutes
| country        = Egypt
| language       = Arabic
| budget         = 
}}

The Land ( , Transliteration|translit.&nbsp;Al-ard) is a 1969 Egyptian drama film directed by Youssef Chahine, based on a popular novel by Abdel Rahman al-Sharqawi. The film narrates the conflict between peasants and their landlord in rural Egypt in the 1930s, and explores the complex relation between individual interests and collective responses to oppression. It was entered into the 1970 Cannes Film Festival.   

==Cast==
* Hamdy Ahmed as Mohammad Effendi
* Yehia Chahine as Hassuna
* Ezzat El Alaili as Abd El-Hadi
* Tewfik El Dekn as Khedr
* Mahmoud El-Meliguy as Mohamed Abu Swelam
* Salah El-Saadany as Elwani
* Ali El Scherif as Diab
* Nagwa Ibrahim as Wassifa

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 