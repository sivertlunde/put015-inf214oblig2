Finding Mr. Destiny
{{Infobox film
| name           = Finding Mr. Destiny
| image          = 200pxfindingmrdestiny2010film.jpeg
| director       = Jang Yoo-jeong
| producer       = Min Jin-soo Min Kyu-dong
| writer         = Lee Kyung-ui
| based on       = Finding Kim Jong-wook (musical)
| starring       = Im Soo-jung Gong Yoo
| music          = Park Ji-woong
| cinematography = Lee Hyung-duk
| editing        = Kim Hyeong-ju
| distributor    = CJ Entertainment
| released       =  
| runtime        = 112 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = $7,444,073 
| admissions     = 
}}
Finding Mr. Destiny ( ; lit. Finding Kim Jong-wook) is a 2010 South Korean romantic comedy starring Im Soo-jung and Gong Yoo.  It is a film adaptation by playwright-turned-director Jang Yoo-jeong of her hit 2006 musical.    The film was a medium box office hit in South Korea selling 1,113,285 tickets nationwide. 

==Plot==
Ji-woo (Im Soo-jung), unable to forget a youthful affair in India that has tattooed itself onto her heart, rejects an eligible suitor and is forced by her father, who fears she will end up an old maid, to seek the help of an agency that specializes in tracking down first loves. She only knows his name: Kim Jong-wook.

Heading the business is Gi-joon (Gong Yoo), who is young, single and male, and equipped with goofy charm. Stubbornly precise by nature and a bit too passionate about work, Gi-joon is determined to complete his first job, even if it means he has to track down every Kim Jong-wook in Korea! 

The heroine is a disheveled and foulmouthed theater director who has yet to make amends with her inability to finish or start anything substantial in both her love life and career. The hero is a naive guy with an obsessive compulsive fixation on order, safety and hygiene — manifested in his perfectly pressed attire and color-coded post-its — who has yet to leap into a whirlwind life experience. 

The two are polar opposites yet eventually grow fond of each other as they bicker along the way to find Ji-woo’s elusive Mr. Destiny — and it’s a long journey since there are 1,108 men who have the same name as her ex-boyfriend, from a Buddhist monk to an overweight farmer and a really unctuous plastic surgeon, to name a few. 

As Gi-joon and Ji-woo travel around the country trying to find her first love, Gi-joon finds himself falling for his client instead.  

==Cast==
*Im Soo-jung ... Seo Ji-woo
*Gong Yoo ... Han Gi-joon
*Chun Ho-jin ... Colonel Seo Dae-ryung (Ji-woos father)
*Ryu Seung-soo ... Gi-joons brother-in-law
*Jeon Soo-kyung ... Soo-kyung (musical actress)
*Lee Chung-ah ... Ji-hye (Ji-woos sister)
*Lee Je-hoon ... Woo-hyung Kim Min-ji ... Cherry
*Jung Gyoo-soo ... Chief of travel agency
*Lee Joon-ha ... Woo-ri (Gi-joons niece)
*Jo Han-cheol ... Director
*Jang Young-nam ... Gi-joons older sister (cameo appearance|cameo)
*Shin Sung-rok ... Captain Choi (Ji-woos pilot boyfriend) (cameo appearance|cameo)
*Jung Sung-hwa ... Bus driver (cameo appearance|cameo)
*Oh Na-ra ... Hyo-jeong (cameo appearance|cameo)
*Choi Il-hwa ... Customer (cameo appearance|cameo)
*Kim Mu-yeol ... Airline worker (cameo appearance|cameo)
*Choi Ji-ho ... Kim Jong-wook the soccer player (cameo appearance|cameo)
*Won Ki-joon ... Kim Jong-wook the doctor (cameo appearance|cameo) Jung Joon-ha ... Kim Jong-wook the farmer (cameo appearance|cameo)
*Oh Man-seok ... Kim Jong-mook (cameo appearance|cameo)
*Kim Dong-wook ... Doctor Jung (Ji-hyes boyfriend) (cameo appearance|cameo)
*Uhm Ki-joon ... Kim Jong-wook (cameo appearance|cameo)
*Paul Stafford ... Tourist (cameo appearance|cameo)

==References==
 

==External links==
* http://www.firstlove2010.co.kr/  
*  
*  
*  

 
 
 
 
 
 