The Courage of Kavik the Wolf Dog
{{Infobox film
| name           = The Courage of Kavik the Wolf Dog
| image          = 
| image_size     =
| caption        = Peter Carter
| producer       = 
| writer         = George Malko, Walt Morey (Novel)
| starring       = Ronny Cox, Linda Sorensen, Andrew Ian McMillan
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = January 20, 1980
| runtime        =  100 minutes
| country        = United States and Canada English
| gross          = 
}}

The Courage of Kavik the Wolf Dog, also known as Kavik the Wolf Dog, is a 1980 made-for TV adventure film.

==Plot==
Kavik, a champion sled dog, who has just won a race in Alaska, is sold for $2000 to George Hunter, a ruthless businessman from Seattle, who has local business interests. The plane carrying the dog crashes into the snow-covered wilderness; the pilot is killed and the dog is more dead than alive. 

The crash site is found by Andy Evans, a young boy who lives in the nearby fishing settlement of Copper City. He struggles to get the dog home and begs his parents to let him ask the local doctor to take a look at Kavik. Dr Walker does, initially reluctantly, have a look and does his best to deal with Kaviks multiple injuries. The dog slowly recovers and starts to bond with Andy.  However, due to its near death experience Kavik is terrified of other dogs and is quick to run away when confronted. But Hunter arrives on a regular trip and claims back the dog, taking him to a kennel in his palatial Seattle home.

Hunters kennel manager, seeing that the dog is unhappy and unlikely to be a champion racer, allows him to escape. Kavik manages to stow away on a coastal ferry and travels north. He struggles over inhospitable terrain, learning to fight other dogs and wolves for his food. Barely alive, he makes it back to Copper City, and reunites with Andy.

Hunter arrives, angrily demanding the return of the dog. Andys father, Kurt, equally angrily claims that the dog will be happier with them than with Hunter. Hunter, who employs Evans and practically owns the whole town, gives in with ill grace, selling Kavik to the Evans family for a token sum.

==Cast==
* Ronny Cox	... Kurt Evans
* Linda Sorenson ... Laura Evans
* Andrew Ian McMillan ... Andy Evans John Ireland ... George Hunter
* Chris Wiggins ... Dr Vic Walker
* John Candy ... Pinky

==Location==
Much of the film was shot in Banff National Park.

==External links==
*  

 
 
 
 