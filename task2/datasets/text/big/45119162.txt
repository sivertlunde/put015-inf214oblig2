The Recoil (1924 film)
{{Infobox film
| name           = The Recoil
| image          =
| caption        =
| director       = T. Hayes Hunter
| producer       = Goldwyn Pictures Corporation J. Parker Read
| writer         = Rex Beach(story) Gerald Duffy(scenario)
| starring       = Mahlon Hamilton Betty Blythe Rene Guissart
| editing        = Alex Troffey
| distributor    = Goldwyn Pictures
| released       = April 27, 1924
| runtime        = 70minutes
| country        = USA
| language       = Silent
}}
The Recoil is a 1924 silent film drama directed by T. Hayes Hunter based on a Rex Beach story. Mahlon Hamilton and Betty Blythe star. Blythe did some filming for the picture in Paris in November 1923.  

The film has been preserved by MGM. 

==Cast==
*Mahlon Hamilton - Gordon Kent
*Betty Blythe - Norma Selbee
*Clive Brook - Marchmont
*Fred Paul - William Southern
*Ernest Hilliard - Jim Selbee

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 