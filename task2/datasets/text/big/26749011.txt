Friesennot
{{Infobox film
| name           = Friesennot
| image          =
| image_size     =
| caption        =
| director       = Peter Hagen
| producer       = Alfred Bittins (line producer) Dr. Scheunemann (line producer) Hermann Schmidt (producer)
| writer         = Werner Kortwich (writer)
| narrator       =
| starring       = See below
| music          = Walter Gronostay
| cinematography = Sepp Allgeier
| editing        = W. Becker
| distributor    =
| released       = 1935
| runtime        = 97 minutes (Germany)
| country        = Nazi Germany
| language       = German
| budget         =
| gross          =
}}

Friesennot is a 1935 German film directed by Peter Hagen. 

The film is also known as Dorf im roten Sturm (German reissue title) and Frisions in Distress (USA).

== Plot ==
Communist authorities are making life as difficult as possible for a village of Volga Germans in the Soviet Union, with taxes and other oppression.   When Mette, a half-Russian, half-Frisian woman, becomes the girlfriend of Kommissar Tschernoff, the Frisians murder her and throw her body in the swamp. Erwin Leiser, Nazi Cinema, p. 40 ISBN 0-02-570230-0   Open violence breaks out, and the Red Army soldiers are all killed; the villagers set fire to their village and flee. 

== Cast ==
*Friedrich Kayßler as Jürgen Wagner
*Helene Fehdmer as Kathrin Wagner
*Valéry Inkijinoff as Kommissar Tschernoff
*Jessie Vihrog as Das Mädchen Mette
*Hermann Schomberg as Klaus Niegebüll
*Ilse Fürstenberg as Dörte Niegebüll
*Kai Möller as Hauke Peters
*Fritz Hoopts as Ontje Ibs
*Martha Ziegler as Wiebke Detlevsen
*Gertrud Boll as Telse Detlevsen
*Maria Koppenhöfer as Frau Winkler
*Marianne Simson as Hilde Winkler
*Franz Stein as Christian Kröger
*Aribert Grimmer as Kommissar Krappien

==Motifs==
Despite Nazi hostility to religion, a cynical piece of anti-Communist propaganda depicts the Communists as posting obscene anti-religious posters, and the Frisians as piously declaring that all authority comes from God. 
 race defilement." 

==Ban and reversal==
After the Molotov–Ribbentrop Pact, in 1939, the film was banned; in 1941, after the invasion of Russia, it was reissued under its new title. 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 