The Descendant of the Snow Leopard
 
{{Infobox film
| name           = The Descendant of the Snow Leopard
| image          = 
| image size     = 
| caption        = 
| director       = Tolomush Okeyev
| producer       = 
| writer         = Tolomush Okeyev Mar Bajdzhiyev
| starring       = Dokhdurbek Kydyraliyev
| music          = 
| cinematography = Nurtai Borbiyev
| editing        = 
| distributor    = 
| released       = 1984
| runtime        = 134 minutes
| country        = Russia
| language       = Russian
| budget         = 
}}
 1984 cinema Soviet drama film directed by Tolomush Okeyev. It was entered into the 35th Berlin International Film Festival where it won the Silver Bear for an outstanding single achievement.   

==Cast==
* Dokhdurbek Kydyraliyev as Koshoshash
* Aliman Zhankorozova as Saikal
* Doskhan Zholzhaksynov as Mundusbai
* Gulnara Alimbayeva as Aike
* Ashir Chokubayev as Kassen
* Marat Zhanteliyev as Sayak
* Dzhamal Seidakhmatova as Begaim
* Gulnara Kydyraliyeva as Sulaika
* K. Akmatova as Batma
* Ajbek Kydyraliyev as Kalygul
* Akyl Kulanbayev as Karypbai
* Svetlana Chebodayeva-Chaptykova as Sonun

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 