108 Demon Kings
{{Infobox film
| name           = 108 Demon Kings
| image          = 
| director       = Pascal Morelli
| producer       = François Cornuau Vincent Roget
| screenplay     = Pascal Morelli Jean Pécheux
| starring       = Lucien Jean-Baptiste Bertrand Nadler Jean-Yves Chatelais Franck Capillery Daniela Labbé Cabrera Philippe Catoire Lucille Boudonnat Hélène Bizot Hanako Danjo Xavier Aubert Mark Antoine Jean-Loup Horwitz Roland Timsit Pierre Forest Franck Gourlat
| music          = Rolfe Kent
| cinematography = François Hernandez
| editing        = 
| studio         = Same Player Fundamental Films Bidibul Productions Scope Pictures France 3 Cinéma Kayenta Production A Prime Group Production 61 Film Fund Luxembourg Eurimages Région Wallone Bruxelles Capitale Centre national de la cinématographie La Banque Postale Images 5 Cinémage 6
| distributor    = Gébéka Films
| released       =  
| runtime        = 104 minutes
| country        = France Belgium Luxembourg
| language       = French
}}
 family adventure film directed by Pascal Morelli. The film premiered at the Forum des Images on 4 December 2014  before it was released theatrically wide in France on 21 January 2015 in France. 

==Voice cast==
*Lucien Jean-Baptiste as Tourbillon-Noir
*Bertrand Nadler as Tête-de-Léopard
*Jean-Yves Chatelais as LEmpereur
*Franck Capillery as Face-Blanche
*Daniela Labbé Cabrera as Vipère-Jaune
*Philippe Catoire as Zhang
*Lucille Boudonnat as Duan (petit)
*Hélène Bizot as Duan (grand)
*Hanako Danjo as Pei Pei
*Xavier Aubert as Cai Jing
*Mark Antoine as Maréchal Gao
*Jean-Loup Horwitz as Doyen des mandarins
*Roland Timsit as Mort-Prématurée
*Pierre Forest as Trépas-Instantané
*Franck Gourlat as Barbe-Pourpre

==Reception==
The film had 30,005 admissions at the French box office. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 

 
 
 
 