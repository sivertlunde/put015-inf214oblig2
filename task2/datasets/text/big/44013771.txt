Achaaram Ammini Osharam Omana
{{Infobox film
| name = Achaaram Ammini Osharam Omana
| image =
| caption =
| director = Adoor Bhasi
| producer = Boban Kunchacko
| writer = P. K. Sarangapani
| screenplay = P. K. Sarangapani
| starring = Prem Nazir Jayan Sheela Sukumari
| music = G. Devarajan
| cinematography = U Rajagopal
| editing = TR Sekhar
| studio = Excel Productions
| distributor = Excel Productions
| released =  
| country = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by Adoor Bhasi and produced by Boban Kunchacko. The film stars Prem Nazir, Jayan, Sheela and Sukumari in lead roles. The film had musical score by G. Devarajan.   
 
==Cast==
  
*Prem Nazir as Paakkaran 
*Jayan as Sudhakaran 
*Sheela as Ammini/Omana 
*Sukumari as Padmavathi 
*Kaviyoor Ponnamma as Dhakshayani 
*Adoor Bhasi as Kittu Pilla 
*Sreelatha Namboothiri as Bhavani 
*Thankaraj as Police 
*Unnimary as Urmila 
*Adoor Pankajam as Kappalandi Kalyani 
*Alummoodan as Gopala Pilla 
*Bahadoor 
*Janardanan as Ravikumar 
*K. P. Ummer as Sivan Pilla Muthalali  Master Raghu as Raghu  Meena as Paruvamma 
*S. P. Pillai as Watcher Paulose 
*T. P. Madhavan as Pankajakshan 
 

==Soundtrack==
The music was composed by G. Devarajan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aaraattukadavil || K. J. Yesudas, P Madhuri || P Bhaskaran || 
|- 
| 2 || Chakkikkothoru Chankaran || Jayachandran, P Madhuri || P Bhaskaran || 
|- 
| 3 || Kaalamakiya Padakkuthira || K. J. Yesudas || P Bhaskaran || 
|-  Susheela || P Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 