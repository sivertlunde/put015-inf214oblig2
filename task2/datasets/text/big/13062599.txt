Fortune's Mask
{{Infobox film
| name           = Fortunes Mask
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Ensminger
| producer       = Albert E. Smith
| writer         = Charles Graham Baker|C. Graham Baker O. Henry
| starring       = Earle Williams
| music          = 
| cinematography = W. Steve Smith Jr.
| editing        = 
| distributor    = Vitagraph Company of America
| released       = October, 1922
| runtime        = 
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}

Fortunes Mask is a 1922 American film starring Earle Williams    and featuring Oliver Hardy. It is unknown whether any recording of the film survives; it may be a lost film.

==Cast==
* Earle Williams - Ramón Olivarra (aka Dicky Maloney)
* Patsy Ruth Miller - Pasa Ortiz
* Henry Hebert - Losada
* Milton Ross - General Pilar
* Eugenie Forde - Madame Ortiz
* Arthur Tavares - Vicenti
* Frank Whitson - Espiración
* Oliver Hardy - Chief of Police William McCall - Captain Cronin

==See also==
* List of American films of 1922
* Oliver Hardy filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 