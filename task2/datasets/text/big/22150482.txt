Out Cold (1989 film)
{{Infobox film
| name = Out Cold
| image = Out_Cold_lithgow_garr_poster.jpg
| image_size = 
| alt = 
| caption = 
| director = Malcolm Mowbray
| producer = George G. Braunstein  Ron Hamady
| writer = Leonard Glasser  George Malko
| narrator = 
| starring = Teri Garr  Bruce McGill  Randy Quaid  John Lithgow
| music = Michel Colombier
| cinematography = Tony Pierce-Roberts
| editing = Dennis M. Hill
| studio = 
| distributor = Hemdale Film Corporation
| released =  
| runtime = 91 min.
| country =  
| language = English
| budget = 
| gross = $294,266
| preceded_by = 
| followed_by = 
}}

Out Cold is a 1989 murder comedy, directed by Malcolm Mowbray (who made 1984s A Private Function), and stars Teri Garr, Randy Quaid and John Lithgow.

The film is set in and around San Pedro, Los Angeles, California - the Edward Hopper streets and storefronts create a world where the script plays itself out in all its linear precision.  Sunny, (Teri Garr), hires a private detective, (Randy Quaid) to trail her husband Ernie, (Bruce McGill), whom she believes is lavishing time and money on other women. She wants all the details so she can clean him out in a divorce action. But she is impatient, and kills Ernie, taking a chance to make his business partner, Dave  (John Lithgow), think he did it. Ernie and Dave worked as butchers in the Army and when they got out they ran a butchers shop together. Dave has always been in love with Sunny - now he is convinced he has killed Ernie by accidentally locking him in a freezer. Lester Atlas, the private detective, thinks he has pictures of Ernies lover visiting him at the shop but has actually photographed Sunny on the night she killed him.

==Criticism==
The film was reviewed, favourably, by the eminent critic Pauline Kael in her final collection of movie reviews, Movie Love. "Teri Garr plays her role with a savage, twinkling joy. Why doesnt her skill get more recognition? This small, disingenuous comedy has been buffed to shine like a jewel; the smoothness of it keeps you giggling."

==References==
 

==External links==
* 
* 

 

 
 
 
 
 