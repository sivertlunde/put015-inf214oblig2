The Mirror (2012)
 

 

 
{{Infobox film
| name           = The Mirror
| image          = 
| caption        = Theatrical release poster
| director       = Muhammad akhlaque
| producer       = Muhammad akhlaque
| starring       = Md Shahnawaz Iraqui Md Adip Molla Masuma
| music          = Md Shamim Quraishi
| writer         = Muhammad akhlaque
| cinematography = Muhammad akhlaque
| editing        = Muhammad akhlaque
| studio         = Azim Production Pvt. Ltd. & AOA Films

| released       =  
| runtime        = 11 min 11 sec 
| country        = India
| language       = English
}}
 English short film directed by Muhammad akhlaque and Irshad Hossain Mallick starring Md Shahnawaz Iraqui, Md Adip Molla, Masuma. It is a horror thriller which shows conflict between individual and supernatural power. The Mirror released on 2 September 2012.

==Cast==
* Md Shahnawaz Iraqui as Apartment Manager/Stranger
* Md Adip Molla as Murderer
* Masuma as Ghost

==Production==

The Mirror (2012) is produced under AOA Films, and In association with Azim Production Pvt. Ltd.

==Soundtrack==
The soundtrack of The Mirror is composed by Md Shamim Quraishi and the album contains six tracks.

==External links==
*  

 
 
 
 