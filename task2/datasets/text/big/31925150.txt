Is Spiritualism a Fraud?
{{Infobox film
| name           = Is Spiritualism a Fraud?
| image          = Is Spiritualism a Fraud? (1906).webm
| image_size     = 
| caption        = 
| director       = Walter R. Booth
| producer       = Robert W. Paul
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Pauls Animatograph Works
| distributor    =
| released       =  
| runtime        = 6 mins 57 secs
| country        = United Kingdom Silent
| budget         =
}}
 short silent silent drama film, directed by Walter R. Booth (also credited to J.H. Martin), featuring a medium exposed as a fake during a séance. The trick film is, "one of the last films made by  R.W. Paul in collaboration with the trick-film specialist W.R. Booth," and according to Michael Brooke of BFI Screenonline, "combines elements of the previous years The Unfortunate Policeman with a special effects sequence.  However, unlike Booth and Pauls other work, here the mechanisms are deliberately revealed," "the crucial difference between his illusions and those of a medium is that Booths audience knew that they were being deceived, but were happy to go along with the charade for the sake of both entertainment and the pleasure of working out how it was done."    

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 