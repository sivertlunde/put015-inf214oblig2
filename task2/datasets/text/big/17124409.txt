Parasakthi (film)
 
 
 
{{Infobox film
| name           = Parasakthi
| image          = Parasakthi.jpg
| director       = Krishnan-Panju|R. Krishnan S. Panju
| caption        = Promotional poster
| screenplay     = Karunanidhi
| writer         =
| starring       = Sivaji Ganesan S. V. Sahasranamam S. S. Rajendran Sriranjani Jr. Pandari Bai
| producer       = P. A. Perumal Mudaliar A. V. Meiyappan
| based on       =  
| music          = R. Sudarsanam Background score: Saraswathi Stores Orchestra
| cinematography = S. Maruti Rao
| editing        = S. Panju (Punjabi) 
| distributor    = National Pictures
| released       =  
| runtime        = 170 minutes
| country        = India Tamil
}}
 Indian Tamil Tamil social melodrama film directed by Krishnan-Panju|R. Krishnan and S. Panju. The film stars Sivaji Ganesan in his debut, while Pandari Bai, Sriranjani Jr., S. S. Rajendran and S. V. Sahasranamam appear in supporting roles. It was jointly produced by National Pictures and AVM Productions,  and is based on a stage drama titled Parasakthi, written by Pavalar Balasundaram.  The film narrates the misfortunes that befall the members of a Tamil family during World War II, and how the members face their individual fate and reunite at the end.
 cult status in Tamil cinema,  and became a trendsetter for dialogues and acting for later Tamil films. 

==Plot==
Chandrasekaran, Gnanasekaran and Gunasekaran are three Indian immigrant brothers from Madurai, Tamil Nadu living in Rangoon, Burma. Their younger sister Kalyani was raised in their home town by their father Manickampillai. In 1942, during World War II, her marriage is arranged with a writer named Thangappan, and the brothers plan to visit Madurai to attend the wedding. Due to war conditions and bombardment of Burmese ports by Japan, the shipping company offers only one ticket and Gunasekaran, the youngest brother, takes it and leaves for Tamil Nadu. The ship however fails to reach on time due to the dangers of the war, and Kalyanis marriage takes place without any of her brothers present.

Kalyani becomes pregnant. But on the day she delivers her child, Thangappan dies in an accident and Manickampillai dies of shock, leaving Kalyani and her child destitute. Her house gets auctioned off, and she makes her living by selling food on the streets. Gunasekaran, after being stranded at sea for several months, finally arrives in Tamil Nadu at Madras. However, while watching a dance performance, he is robbed of all his belongings after being Alcohol intoxication|intoxicated. Reduced to poverty, he becomes enraged at the status of the once glorious Tamil Nadu, and fakes insanity by indulging in numerous tricks to make a living. Gunasekaran finally comes across his destitute sister at Madurai, having learned of their fathers death and her poverty. He continues to play insane and does not reveal his true identity to her due to his poverty, but hovers around her. Kalyani is irritated by the strangers behaviour, unaware that he is her brother.

Kalyani is nearly molested by a vagabond named Venu, but is saved by Gunasekaran. She later leaves Madurai and arrives at Tiruchi, where she obtains work as a maid of blackmarketeer Narayana Pillai, who also tries to molest her. She is, however, saved by his wife Kantha, and leaves the job. While searching for his sister, Gunasekaran reaches Tiruchi and comes across Vimala, a wealthy woman, to whom he explains the miserable status of him and his sister in the society. After resting in her house for a while, he silently leaves to continue searching for Kalyani.

Meanwhile, as Japanese shelling intensifies in Burma, Chandrasekaran and Gnanasekaran decide to return to India. Chandrasekaran, accompanied by his wife Saraswati, reaches Tiruchi safely and becomes a judge, but Gnanasekaran is lost in the journey and loses a leg in the shelling before arriving in India. He begs for a living, forms an association for beggars and tries to reform them. Kalyani reaches Chandrasekarans palatial house seeking food, but Chandrasekaran throws her out without recognising her. She later arrives at a temple seeking help, but the pujari (priest) also tries to molest her. Frustrated with life and unable to feed her child, Kalyani throws it into a river and attempts suicide, but is soon arrested for killing the child and brought for trial.

At the court, Kalyani defends her act of infanticide with the judge being Chandrasekaran, who after hearing her tragic story realises she is his sister, and faints. Gunasekaran is also brought to the court for having attacked the priest who tried to molest his sister. During his trial, Gunasekaran explains the misfortunes which has befallen him and his family, and justifies his actions. Gunasekarans valiant defence in court awakens everyone on the evils of the society. As the trial proceeds, Vimala arrives and produces Kalyanis child, which was revealed to have safely fallen in her boat instead of the river. Kalyani and Gunasekaran are pardoned and acquitted by the court, and are finally reunited with Chandrasekaran. Gnanasekaran, while collecting donations for his association of beggars, also joins them unexpectedly. With Vimala and Gunasekaran deciding to get married, the family subsequently inaugurates a welfare home for orphans.

==Cast==
{{multiple image
| align     = right
| direction = vertical
| image1    =
| width1    = 150
| caption1  =  Sriranjani Jr. 
| image2    =
| width2    = 150
| caption2  =  Pandari Bai 
}}
;Male cast 
* Sivaji Ganesan as Gunasekaran, the youngest of three brothers
* S. V. Sahasranamam as Chandrasekaran, the eldest of three brothers
* S. S. Rajendran as Gnanasekaran, the second of three brothers
* Duraiswamy as Manickampillai, the father of the three brothers
* T. K. Ramachandran as Venu, a local vagabond
* K. M. Nambirajan as Vellai Swamy
* Venkatraman as Thangappan, husband of Kalyani
* V. K. Ramasamy (actor)|V. K. Ramasamy as Narayana Pillai, a blackmarketeer
* K. P. Kamatchi as the pujari (priest)
* M. N. Krishnan as Kuppan
* Sakthivel as a servant
* D. V. Narayanaswamy as Thambi Durai
* V. K. Karthikeyan as the Tamil Pandit

;Female cast 
* Sriranjani Jr. as Kalyani, the sister of the three brothers
* Pandari Bai as Vimala, Gunasekarans future wife
* Susheela as Saraswati, Chandrasekarans wife
* Kannamma as Jolly
* Angamuthu as the Fruit seller 
* T. P. Muthulakshmi as Kantha, Narayana Pillais wife
* A. S. Jaya as Parvati

==Production==
{{Quote box quote = "My intention was to introduce the ideas and policies of social reform and justice in the films and bring up the status of the Tamil language as they were called for in DMK policies." source =&nbsp;– Karunanidhi, in a 1970 interview with Robert L. Hardgrave Jr.  align = right width = 46% border = 1px
}}

Parasakthi was a popular Tamil play written by Pavalar Balasundaram, a Tamil scholar. Around the same time, En Thangai (My Sister), written by T. S. Natarajan, became popular. Sivaji Ganesan, at that time a struggling stage actor, acted in En Thangai as "a brother sacrificing his love for the sake of his sightless kid sister." The pre-production crew at Central Studio, Coimbatore, initially planned to merge these two plays to make a film. However, Natarajan, the author of En Thangai disagreed to the idea, and indeed sold the rights of the play to another producer.  
 En Thangai Telugu actor Mukkamala Krishnamurthi in the 1951 Tamil film Niraparaadhi (1951 film)|Niraparadhi.    

Parasakthi did not begin well for Ganesan. After 3000 feet of the film was shot,  Meiyappan and the films directors Krishnan and Panju were dissatisfied with Ganesans performance, and Meiyappan wanted to replace Ganesan with actor K. R. Ramaswamy|K. R. Ramasami.  Producer Perumal refused to have Ganesan replaced, and even considered taking the production house elsewhere. He later approached political leader C. N. Annadurai, who convinced Meiyappan to retain Ganesan in the film.  Ganesan was paid a monthly salary of  250 (valued at about US$52.5 in 1952 ) for acting in the film.  S. S. Rajendran, who was another successful stage artist, also debuted in Parasakthi after the advice of Annadurai.  Actress Raja Sulochana was initially cast as the female lead, but opted out due to her pregnancy, and was eventually replaced by Telugu actress Sriranjani Jr.    Pandari Bai was also added to the film, after Meiyappan was impressed with her performance in Raja Vikrama (1950).  Her character was not included in the original play, and was created specifically for the film by Karunanidhi to correct the mistakes from the play and also to narrate the happenings through a character.  Poet Kannadasan declined the offer to be one of the films lyricists, and instead acted in a minor role as a court judge, as he was "determined to take part in the Parasakthi movie".  The cinematography was handled by S. Maruti Rao, while the songs were choreographed by Heeralal.  The dialogues of the film, written by Karunanidhi, critically examined the social issues of the time and holds an important part in the Dravidian Movement.  The films climax song "Ellorum Vazha Vendum" featured stock footage of the political leaders C. Rajagopalachari, Periyar E. V. Ramasamy, Bhakthavatchalam, Annadurai, and Karunanidhi. 

==Themes== DMK party Justice Party and Nagammai, a leading activist in the Self-Respect Movement and the wife of Periyar E. V. Ramasamy. 

The film deploys Kalyanis vulnerability as a widow in a hostile society, with consequent threats to her chastity, especially during the court trial scenes. The name Kalyani was chosen by the script writer to emphasise the contradiction between the meaning of her name indicating auspiciousness and her contrasting penury. The theme is expressed through Gunasekarans arguments in the court : "  sisters name is Kalyani. An auspicious name  . But there is no  mangalyam  around   neck". Also, Vimala, who becomes Gunasekarans bride, compares herself to Kannagi, a popular symbol of chastity in Tamil culture. Ganesan, who enacted the role of Gunasekaran in Parasakthi, was a DMK activist in real life in 1952 and helped in propagating the theme of Dravida Nadu. The film attempted to bring to light the alleged fraud in the name of religion and presented agnostic views, displaying a powerful critique of the Congress rule in the Madras Presidency. 

==Music==
The music of Parasakthi was composed by R. Sudarsanam. The lyrics were written by Kannadasan, Bharathidasan, T. N. Ramaiah Nadu, Bharathiyar, Karunanidhi,  and Udumalai Narayana Kavi.  The background score was composed by the Chennai-based Saraswathi Stores Orchestra.  Relatively higher importance was given to the films dialogues over its music,  so the dialogues were printed and sold separately like film song books. Parasakthi established this trend, which was eventually followed by later Tamil films.    Some of the numbers from Parasakthi were based on songs from Hindi films; one was a rehash from the Urdu film Akeli (1952). }} The track O Rasikkum Seemane is said to have inspired the song Itai Tazhukikkolla from the film Periyar (2007 film)|Periyar (2007).  The 2010 film Rasikkum Seemane borrows its title from the song of the same name. 

{{Track listing
| collapsed       = 
| headline        = 
| total_length    =

| all_writing     = 
| all_lyrics      = 
| all_music       =

| writing_credits = 
| lyrics_credits  = no
| music_credits   =

| extra_column    = Singers
| title1          = Desam Gnanam Kalvi
| extra1          = C. S. Jayaraman
| lyrics1          = 
| length1         = 3:26

| title2           = Kaa Kaa Kaa
| extra2          = C. S. Jayaraman
| lyrics2          = 
| length2         = 3:00

| title3          = Nenju Porkku Thillaiye
| extra3          = C. S. Jayaraman
| lyrics3          =  Bharathiyar
| length3         = 4:50

| title4            = Ill Vaazhviniley
| extra4           = T. S. Bhagavathi, M. H. Hussain
| lyrics4           = 
| length4         = 2:07

| title5           = Puthu Pennin
| extra5          = M. S. Rajeswari
| lyrics5          = T. N. Ramaiah Nadu
| length5         = 4:23

| title6           = Oh Rasikkum Seemane 	
| extra6          = M. S. Rajeswari
| lyrics6          = Kannadasan
| length6         = 1:44

| title7           = Ellorum
| extra7          = T. S. Bhagavathi, M. S. Rajeswari
| lyrics7          = Bharathidasan
| length7         = 1:35

| title8          = Konju Mozhi
| extra8         = T. S. Bhagavathi
| lyrics8         = Bharathidasan
| length8        = 3:03

| title9 =            Poomaalai 	
| extra9 =          T. S. Bhagavathi
| lyrics9 =          Karunanidhi
| length9 =        3:01

| title10           = Porule Illaarkku
| extra10         = T. S. Bhagavathi
| lyrics10         = M. Karunanidhi
| length10        = 3:37

| title11         = Vaazhga Vaazhgave
| extra11        = M. L. Vasanthakumari
| lyrics11         = Bharathidasan
| length11        = 5:00
}}

==Release==

===Reception===
 
Parasakthi was released on 17 October 1952, on Diwali day.  Regarded as the first significant attempt by the Dravidian Movement to use media for propaganda, the film catapulted Ganesan to fame overnight and evoked comparisons with Hollywood actor Spencer Tracy.  Ganesans performance in the films court scene was also very well received by audience, and was considered to have propelled him to stardom.     The film became an instant commercial success,  running for over 175 days in several theatres, and was one of the first films to be screened at the Madurai-based Thangam theatre, which was noted as Asias largest theatre at the time.     Parasakthi s Telugu language|Telugu-dubbed version of the same name was released on 11 January 1957.  

===Critical response===
Reviews for Parasakthi have been mostly positive. P. Balasubramania Mudaliar of Sunday Observer wrote, "The story is simple but it has been made powerful by Mr. Karunanidhi by his beautiful dialogues. Mr. Shivaji Ganesan, who plays the main role dominates from the beginning to the end" and concluded, "If an Academy award were to be given to any picture, I have little doubt that this picture would be entitled on its merits to such an award."  Film historian Randor Guy said, "1952&nbsp;... an eventful year for Tamil cinema, the beginning of a new period" and added that "The film that ushered in that new era was Parasakthi written for the screen by another fast-rising star Mu. Karunanidhi", while concluding that the film would be "Remembered for the dialogue and the stunning performance of the new hero."    In an interview with Shobha Warrier of Rediff, Tamil film historian S. Theodore Baskaran said, "Sivajis best and most memorable films are his early ones" and mentioned that Ganesan was "very lucky to get a role in Parasakthi", which he praised for the "flowery dialogues."   

Entertainment portal IndiaGlitz said, "One can never forget the impeccable and revolutionary dialogues from Parasakthi. Former chief minister M. Karunanidhis power packed dialogues and Shivaji Ganesans master class acting made this movie a unforgettable watch."  Behindwoods.com praised the film for the "fiery dialogues, talented direction, and terrific acting by the cast".  Film historian S. Muthiah said that Parasakthi "showed Karunanidhi as the master of meaningful screen dialogue that carried forceful messages to the masses".  The Sunday Indian called it "a classic DMK film scripted by M Karunanidhi".  In a 2007 interview with S. R. Ashok Kumar of   said, "The impact of the film can be felt even after 60 years among the film loving audience, who fondly remember its dialogues, acting and songs", and called the film a "must watch."{{sfn|Dhananjayan|2011|loc=
 }}

===Controversies=== portray Brahmins Chakravarthi Rajagopalachari was unhappy with the extremely provocative nature of the film, but allowed it to be screened.    One of the reasons stated by them was the dialogue spoken by Ganesans character, "Just because you came around chanting names and offered flowers to the stone, would it become a god?", which was accused of "mocking the audiences." His reference to Goddess Parasakthi as a stone created a stir, and the word "stone" was eventually censored from the soundtrack. However, the given message was still "clear and the impact viral."  The State Government requested the Union Government to reconsider the film certification, but they declined, due to a previous examination by a Madras intelligence officer, who stated: 

 

==Legacy==
  Congress party in Tamil Nadu.  The dialogues became so popular that "roadside entertainers used to recite long passages from the film in market area of Madras and collect money from bystanders",  and memorising the films dialogues became "a must for aspirant political orators".  They were even released separately on gramaphone records. 
 Prabhu and Ramkumar Ganesan|Ramkumar.   The memorial stands at the same place where Ganesan first faced the camera. A slab of black granite, the memorial has on its top a brass medallion that bears a close-up of Ganesan uttering his popular opening line "Success". At its bottom is a rectangular plaque that gives details about the memorials inauguration. At the base of the rectangular plaque are two other plaques resembling the pages of an open book and contains the names of the technical crew and all those involved in the making of the film.  The visage of Ganesan wearing a hat was designed by Thotta Tharani.  The 2003 film Success starring Ganesans grandson Dushyant was named after Ganesans popular line, but opened to negative reception. 

Parasakthi is included with other Sivaji Ganesan-starrers in 8th Ulaga Adhisayam Sivaji, a compilation DVD featuring Ganesans "iconic performances in the form of scenes, songs and stunts" which was released in May 2012.  During the films "diamond jubilee" year celebrations in January 2013, K. Chandrasekaran, the president of Nadigar Thilagam Sivaji Social Welfare Association said, "Six decades down the line  Parasakthi  is remembered because it is not just a film, but an epic".  On the centenary of Indian cinema in April 2013, Forbes (India)|Forbes included Sivaji Ganesans performance in the film in its list, "25 Greatest Acting Performances of Indian Cinema". 
 Vivek parodied the films climax in Palayathu Amman (2000).  A film called Meendum Parasakthi directed by A. Jagannathan was released in 1985. This film is not related to Parasakthi.  In Thottal Poo Malarum (2007), the introduction scene of the films lead actor Sakthi Vasu was shot at Ganesans memorial tomb.  Karthis performance in his debut film Paruthi Veeran (2007) was compared by critics with Parasakthi.  Malathi Rangaran, in her review of Citizen (film)|Citizen (2001) at The Hindu, mentioned that court scene during the climax was reminiscent of Parasakthis climax. 

==See also==
* Portrayal of Tamil Brahmins in popular media
* Tamil cinema and Dravidian politics

==Explanatory notes==
{{notes
| notes =
{{efn
| name = exchange1948
| The exchange rate between 1948 and 1966 was 4.79 Indian rupees ( ) per 1 US dollar (US$). 
}}
}}

==References==
 

==Bibliography==
 
*  

*  

*  

*  

*  

*  

*  
 

==External links==
*  
*   at Upperstall.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 