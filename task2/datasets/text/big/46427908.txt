That Was Heidelberg on Summer Nights
{{Infobox film
| name = That Was Heidelberg on Summer Nights 
| image =
| image_size =
| caption =
| director = Emmerich Hanus
| producer = 
| writer =  Siegfried Philippi 
| starring = Fritz Alberti   Charlotte Susa   Olga Engl
| music = 
| cinematography = Josef Dietze  
| editing =     
| studio = Althoff & Company 
| distributor = 
| released = 8 February 1927 
| runtime = 
| country = Germany German intertitles
| budget =
| gross =
}} shot on location in Heidelberg.

==Cast==
*  Fritz Alberti as Sanitätsrat Liningen  
* Fritz Beckmann as Revuedirektor  
* Olga Engl as Frau v. Helling  
* Karl Etlinger as Schmierenschauspieler Ehrenfleck  
* Julius Falkenstein as Stransky  
* Tonio Gennaro as Erste Chargierte  
* Antonie Jaeckel as Frau von Gutsbesitzer Wagner  
* Margarete Kupfer as Studentenwirtin  
* Max Maximilian as Korpsdiener  
* Frida Richard as Großmutter Liningen  
* Ernst Rückert as Rudolf  
* Fritz Schroeter as Geldverleiher  
* Charlotte Susa as Grete  
* Hertha von Walther as Hertha  
* Eduard von Winterstein as Gutsbesitzer Wagner

== References ==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

== External links ==
*  

 
 
 
 
 
 
 
 

 