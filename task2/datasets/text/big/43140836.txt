Paru Wife of Devadas
{{Infobox film name           = Paaru Wife of Devadas image          = Paaru_Wife_Of_Devadas_Album_Cover.jpg director       = Kiran Govi producer       = Krishna Deva Gowda   Mukhtar writer         =  starring       = Srinagar Kitty   Soundarya Jayamala music          = Arjun Janya cinematography =    editing        =  distributor    = Harshini Productions released       =   runtime        =  country        = India  language       = Kannada 
}} 2014 Kannada Kannada romantic comedy film starring Srinagar Kitty and Soundarya Jayamala in the lead role. The film is directed by Kiran Govi and produced by Krishna Deva Gowda and Mukhtar. The music for the film is composed by Arjun Janya.  The film is slated for release on June 27, 2014.
 Sharat Chandras Bengali novel, is depicted in an entirely different manner according to the director. The filming started early February 2013 at the Kanteerava Studios in Bangalore with a song sequence. 

==Cast==
* Srinagar Kitty
* Soundarya Jayamala 
* Neha Patil
* Layendra
* V. Manohar
* Mohan Juneja

==Soundtrack==
The audio of the film was launched in March 2014 at hotel Citadel in Bangalore. Ace playback singer Manjula Gururaj and her husband musician Gururaj were present as the chief guest. It was revealed that the director had roped in local talents who were winners of talent shows for singing the soundtrack of the film.  The music is composed by Arjun Janya, consisting of six tracks. While the five songs are written by Nagendra Prasad, one single is written by Jayanth Kaikini. 

{{Track listing
| total_length   = 25:32
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Kannalle Neenu
| lyrics1 	= Jayanth Kaikini
| extra1        = Sonu Nigam, Palak Muchhal
| length1       = 
| title2        = Haadu Haadu
| lyrics2 	= Nagendra Prasad
| extra2        = Nakul
| length2       = 
| title3        = O Devathe
| lyrics3 	= Nagendra Prasad
| extra3        = Sonu Nigam
| length3       = 
| title4        = Premana Premina
| lyrics4       = Nagendra Prasad
| extra4        = Pragya, Anuradha Bhat
| length4       =
| title5        = Paaru Paaru
| lyrics5       = Nagendra Prasad
| extra5        = Harsha, Shwetha
| length5       = 
| title6        = Beke Beku
| lyrics6       = Nagendra Prasad
| extra6        = Anuradha Bhat
| length6       = 
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 