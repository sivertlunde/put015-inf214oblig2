The Trial (1948 film)
 
{{Infobox film
| name           = The Trial
| image          = 
| caption        = 
| director       = Georg Wilhelm Pabst
| producer       = J. A. Hübler-Kahla
| writer         = Rudolf Brunngraber Kurt Heuser Emmerich Robenz
| starring       = Ewald Balser
| music          = 
| cinematography = Helmut Ashley Oskar Schnirch
| editing        = Anna Höllering
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Austria
| language       = German
| budget         = 
}}
The Trial ( ) is a 1948 Austrian drama film directed by Georg Wilhelm Pabst. At the 9th Venice International Film Festival, Pabst won the Award for Best Director; Ernst Deutsch won the award for Best Actor and the Volpi Cup.  The story is based on the events of the Tiszaeszlár Affair.

==Cast==
* Ewald Balser as Dr. Eötvös
* Marianne Schönauer as Dr. Eötvös fiancee
* Ernst Deutsch as Scharf, tempel servant
* Rega Hafenbrödl as his wife
* Albert Truby as Moritz, his son
* Heinz Moog as Baron Onody
* Maria Eis as Widow Solymosi
* Aglaja Schmid as Esther, her daughter
* Ida Russka as Bäurin Batori, Esthers employer
* Iván Petrovich as Egressy, prosecutor
* Gustav Diessl as Both, prosecutor
* Fritz Hinz-Fabricius as Judge
* Josef Meinrad as Bary, investigating judge

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 