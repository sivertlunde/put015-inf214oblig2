You and Me (1938 film)
{{Infobox film
| name           = You and Me
| image          = YouAndMePoster.jpg
| image_size     =
| caption        =
| director       = Fritz Lang
| producer       =
| writer         = Norman Krasna Virginia Van Upp
| narrator       =
| starring       = Sylvia Sidney George Raft
| music          = Kurt Weill
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 
| country        = United States 
| language       =
| budget         =
}} Harry Careys character routinely hires ex-convicts to staff his store.  The movie was written by Norman Krasna and Virginia Van Upp.
==Cast==
*Sylvia Sidney as Helen Dennis
*George Raft as Joe Dennis
*Barton MacLane as Mickey Bain Harry Carey as Jerome Morris
*Robert Cummings as Jim
*Guinn "Big Boy" Williams as Taxi
*Ellen Drew as a cashier (uncredited)

==Production==
William Le Baron of Paramount asked Norman Krasna if he could come up with a vehicle for George Raft. Krasna agreed provided he was allowed to direct. Then Carole Lombard read the script and wanted to be involved; Paramount did not want first-time director Krasna to be entrusted with a Lombard-Raft film and tried to force him off the project. *McGilligan, Patrick, "Norman Krasna: The Woolworths Touch", Backstory: Interviews with Screenwriters of Hollywoods Golden Age, University of California Press,1986 p219-220 

Richard Wallace was meant to direct but Sylvia Sidney succeeded in having him replaced by Fritz Lang. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 77 

==Reception==
The film was a box office flop. 

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 

 