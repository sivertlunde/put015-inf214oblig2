Return of Grey Wolf
{{Infobox film
| name           = Return of Grey Wolf
| image          =
| caption        =
| director       = Jacques Rollens
| producer       = William Steiner
| writer         = Jay Arr
| starring       = Leader, the Dog
| cinematography = Angelo Giovannelli
| editing        = Hugh B. Gunter
| distributor    = Ambassador Pictures
| released       =  
| runtime        = 5 reels
| country        = United States
| language       = Silent film(English intertitles)
}}
Return of Grey Wolf is a 1926 silent film action adventure produced by William Steiner and released by an independent company, Ambassador. It stars a dog named Leader and can be found on DVD from Grapevine video.   

==Cast==
*Leader the Dog - Grey Wolf(aka Long Tail)
*James Pierce - Louis LaRue
*Helen Lynch - Jean St. Claire
*Walter Shumway - Gaston Pacot
*Edward Coxen - Charles Hendrickson, Owner of Trading Post
*Lionel Belmore - Jacques St. Claire
*Whitehorse - Abe Hawkins

==References==
 

==External links==
* 
* 
* 

 
 
 
 


 