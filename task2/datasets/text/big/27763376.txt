The Groundstar Conspiracy
{{Infobox film
| name           = The Groundstar Conspiracy
| image          = TheGroundstarConspiracy.jpg
| image_size     =
| caption        = DVD cover
| director       = Lamont Johnson
| producer       = Frank Arrigo Earl A. Glick Hal Roach Jr. Trevor Wallace
| writer         = Douglas Heyes Based on The Alien by L. P. Davies
| narrator       =
| starring       = George Peppard Michael Sarrazin Christine Belford
| music          = Paul Hoffert Michael Reed
| editing        = Edward M. Abroms
| studio         = Universal Pictures Hal Roach Studios
| distributor    = Universal Pictures
| released       = June 12, 1972 (Sweden) June 21, 1972 (USA)
| runtime        = 103 min.
| country        = Canada
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}}
The Groundstar Conspiracy is a 1972 film directed by Lamont Johnson. It stars George Peppard and Michael Sarrazin.   Douglas Heyes screenplay (written under his frequent pseudonym, Matthew Howard) was adapted very freely from L. P. Davies 1968 novel, The Alien.  It was filmed in Vancouver, British Columbia and produced by Hal Roach Productions  in Canada.

==Plot==

Employee John David Welles attempts to steal rocket booster plans from the Groundstar facility.  His attempt goes awry and he is badly disfigured in an explosion and barely escapes.  He stumbles to the home of Nicole Devon, and collapses.  She calls an ambulance, the authorities are alerted, and soon Welles is operated on, given plastic surgery and interrogated by a gung-ho government official named Tuxan.  But Welles claims to have no memory of his crime.  In fact, he claims no memory of his life at all, save for brief glimpses of a woman and small boy frolicking on a beach.
  
Despite Tuxans brutal interrogation techniques (electro-shock and water submersion), Welles still maintains his story of total amnesia.  Tuxan allows Welles to escape, hoping he will lead them to the people behind the attempted theft.  Welles goes to Nicoles home and begs her to help him remember.  But she knows nothing.

Eventually the inside conspirators behind the attempted theft are found, and Tuxan reveals the truth to Welles, who still cannot remember any details of the crime.  John David Welles actually died en route to the hospital on the night of the explosion.  The man we have come to know as Welles is really Peter Bellamy, a government employee who recently lost his wife and son in an accident.  Bellamy, feeling that life was no longer worth living and remembering, volunteered to have his memory wiped and to play Welles in order to draw the conspirators into the open.

==Cast==
*George Peppard as Tuxan
*Michael Sarrazin as David Welles / Peter Bellamy
*Christine Belford as Nicole Devon
*Cliff Potts as Carl Mosely James Olson as Senator Stanton

==References==
 

==External links==
* 

 

 
 
 