Chinnada Gombe
{{Infobox film
| name           = Chinnada Gombe
| image          =
| caption        =
| director       = B. R. Panthulu
| producer       = B. R. Panthulu
| writer         = Based on Story Maane-Na-Maane (Bengali)
| screenplay     =
| starring       = M. V. Rajamma Jayalalithaa Sandhya Kalpana
| music          = T. G. Lingappa
| cinematography = V Ramamurthy
| editing        = R Devarajan
| studio         = Padmini Pictures
| distributor    =
| released       =  
| country        = India Kannada
}}
 Indian Kannada Kannada film,  directed and produced by B. R. Panthulu. The film stars M. V. Rajamma, Jayalalithaa, Sandhya and Kalpana in lead roles. The film had musical score by T. G. Lingappa. 

==Cast==
 
*M. V. Rajamma
*Jayalalithaa
*Sandhya Kalpana
*B. Jaya (actress)|B. Jaya
*Baby Vishalakshi
*B. R. Panthulu
*Kalyan Kumar
*Narasimharaju Balakrishna
*K. S. Ashwath
*Rajashankar
*Kupparaj
*H T Urs
*Chi Udayashankar
*Guggu
*Maheshwaraiah
*Krishna Shastry
*Basappa
*Idlicut Shankar
*Iyengar
*Vijayarao
 

==Soundtrack==
The music was composed by T. G. Lingappa. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Goodinali Ondubanadi || R. Panigrahi || R. N. Jayagopal || 04.03
|-
| 2 || Honnase Ullavage || PB. Srinivas || R. N. Jayagopal || 03.08
|- Janaki || Vijaya Narasimha || 02.53
|- Susheela || R. N. Jayagopal || 03.19
|-
| 5 || Sevavthige Chandinantha || Sulamangalam Rajalakshmi || Vijaya Narasimha || 02.58
|- Susheela || Vijaya Narasimha || 03.50
|}

==References==
 

==External links==
* 
 

 
 
 
 


 