The Oracle (film)
The British comedy film directed by C.M. Pennington-Richards and starring Robert Beatty, Michael Medwin and Virginia McKenna.  A journalist goes on holiday to Ireland where he encounters a fortune-teller. It was based on a radio play To Tell You the Truth by Robert Barr.

==Cast==
* Robert Beatty - Bob Jefferson
* Michael Medwin - Timothy Blake
* Virginia McKenna - Shelagh
* Mervyn Johns - Tom Mitchum
* Arthur Macrae - Alan Digby
* Gillian Lind - Jane Bond
* Ursula Howells - Peggy
* Louise Hampton - Miss Turner
* John Charlesworth - Denis
* Joseph Tomelty- Terry Roche
* Lockwood West - Adams
* Maire ONeill - Mrs Lenham
* John McBride - Mick
* Derek Tansley - Idiot Boy
* Patrick McAlinney - OKeefe
* Lionel Marson - Announcer
* Jean St. Clair - Young Girl
* Jack May - Old Man
* Gilbert Harding - Voice of the Oracle

==References==
 

==Bibliography==
* Harper, Sue & Porter, Vincent. British Cinema of the 1950s: The Decline of Deference. Oxford University Press, 2007.

==External links==
* 

 

 
 
 
 
 
 
 


 
 