Sanam Bewafa
{{Infobox film 
| name           = Sanam Bewafa सनम बेवफ़ा  
| image          = Sanam_Bewafaa.gif
| caption        = DVD cover
| director       = Saawan Kumar Tak
| producer       = Saawan Kumar Tak Anwar Khan Saawan Kumar Tak Chandni Kanchan Kanchan Pran Pran Danny Denzongpa
| music          = Mahesh-Kishor Manmohan Singh
| editing        = Jawahar Razdan
| distributor    = Saawan Kumar Productions
| released       = 11 January 1991
| runtime        = 150 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} Indian Bollywood Pran and Danny Denzongpa. The film released on 11 January 1991.

== Synopsis ==
 Pathan families who have been at logger heads for many generations. Salman (Salman Khan), Sher Khans (Danny Denzongpa) son, falls in love with Rukhsar (Chandni), Fateh Khans daughter.

On finding the truth, Fateh Khan finds his wrath too much to control. On the other hand, Sher Khan is extremely happy to know that his son is in love with his enemys daughter and promises Salman that he will get him married to Rukhsar. The marriage is settled after great difficulties. On the day of marriage, Fateh Khan places an incredible demand in Haque-Meher. Sher Khan is dumbfounded and shocked but gives in for his sons happiness. Hurt by the insult, Sher Khan retaliates by throwing Rukhsar out of the house the following morning after paying the agreed Haque-Meher. On seeing Rukhsar back, her family is numb with shock.

This leads to conflicts between the two families resulting in loss of life on both sides, but Sher Khan and Fateh Khan remain adamant, until they discover that Rukhsar is pregnant with Salmans child. The newborn finally leads to peace between the two....

==Cast==

{| class="wikitable sortable"
|- style="background:#ccc; text-align:center;"
! Actor Name !! Character Name
|- style="vertical-align: middle; text-align: center;"
| Salman Khan || Salman Khan
|- style="vertical-align: middle; text-align: center;" Chandni || Rukhsar Khan
|- style="vertical-align: middle; text-align: center;"
| Danny Denzongpa || Sher Khan
|- style="vertical-align: middle; text-align: center;" Pran || Fateh Khan
|- style="vertical-align: middle; text-align: center;"
| Puneet Issar || Afzal Khan
|- style="vertical-align: middle; text-align: center;" Kanchan || Kanchan
|- style="vertical-align: middle; text-align: center;"
| Jagdeep || Daroga
|- style="vertical-align: middle; text-align: center;"
| Pankaj Dheer || Zuber Khan
|- style="vertical-align: middle; text-align: center;"
| Dina Pathak || Fatehs mother
|- style="vertical-align: middle; text-align: center;"
| Gurbachchan Singh || Gur Bachan
|- style="vertical-align: middle; text-align: center;"
| Dan Dhanoa || Shaukat Khan
|- style="vertical-align: middle; text-align: center;"
| Vijayendra Ghatge || Sajjan Thakur
|}

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s)!! Length
|-
| 1
| Chudi Maza Na Degi 
| Lata Mangeshkar
| 05:23
|- 
| 2
| O Hare Dupatte Wali 
| Vipin Sachdev
| 05:34
|- 
| 3
| Sanam Bewafa 
| Lata Mangeshkar, Vipin Sachdev
| 06:08
|- 
| 4
| Be Iraada Nazar Mil Gayee To 
| Vipin Sachdev
| 05:33
|- 
| 5
| Mujhe Allah Ki Kasam 
| Lata Mangeshkar, Vipin Sachdev
| 04:43
|- 
| 6
| Allah Karam Karam
| Lata Mangeshkar
| 04:21
|- 
| 7
| Meri Jaan Chali 
| Vivek Verma
| 05:59
|- 
| 8
| Jinke Aage Ji 
| Lata Mangeshkar
| 03:55
|- 
| 9
| Angur Ka Dana Hoon 
| Kavita Krishnamurthy
| 04:34
|}

== Awards ==
* Filmfare Best Supporting Actor Award - Danny Denzongpa

==External links==
* 
*http://random-bollywood-movie.blogspot.com/2010/04/sanam-bewafa.html

 
 
 

 