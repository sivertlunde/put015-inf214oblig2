The Blot
{{Infobox film
| name           = The Blot
| image          = The Blot dvd cover.jpg
| image_size     =
| caption        =
| director       = Phillips Smalley Lois Weber
| producer       = Lois Weber
| writer         = Lois Weber Marion Orth
| narrator       = Philip Hubbard Margaret McWade Claire Windsor Louis Calhern Marie Walcamp
| cinematography = Gordon Jennings
| editing        =
| distributor    = F.B. Warren Corporation Lois Weber Productions
| released       =  
| runtime        = 91 minutes
| country        = United States English intertitles
| budget         =
}}
 silent drama Philip Hubbard, Margaret McWade, Claire Windsor and Louis Calhern.

Weber filmed in real locations, using as much natural lighting as possible. Scenes were filmed on location around Los Angeles, particularly at the old University of Los Angeles campus, now Los Angeles City College. Many supporting roles were given to non-professionals.
 David Gill for British television. Brownlow singles out the film for praise in his book Behind the Mask of Innocence (1990). The Blot, which is currently available on DVD and VHS, was exhibited at the San Francisco Silent Film Festival.

==Plot==
At the end of class, poorly paid college professor Andrew Griggs begs his unruly students to show more appreciation for his efforts to teach them. Among the most disrespectful are a spoiled trio led by Phil West (Louis Calhern), whose father is the schools wealthiest trustee; Bert Gareth, a congressmans son; and Walt Lucas, a 23-year-old who must graduate to receive his inheritance.

Unbeknownst to his friends, Phils interest has been piqued by the professors daughter, Amelia (Claire Windsor). He frequently drops by the public library where she works, just to be able to speak to her. She, however, is unimpressed by him and his wealth.

The Griggs poverty is contrasted with the prosperity of their next-door neighbors. "Foreign-born" shoemaker Hans Olsen is sympathetic to their plight, as is his eldest son Peter (Amelias secret admirer), but his wife strongly dislikes what she considers Mrs. Griggs superior airs.

One day, Phil finally manages to persuade Amelia to let him drive her home after work, as it is raining (and her shoe has a hole in it). He is invited inside. Mrs. Griggs, knowing who he is, decides to spend what little she has on some fancy sandwiches, cakes and tea in an attempt to put up a brave front. She is heartbroken to find when she brings them in that Phil has departed and poor Reverend Gates (another of Amelias admirers) is to be the recipient of her expensive bounty. As a result, Mrs. Griggs is unable to make the mortgage payment on the house.

Juanita Claredon (Marie Walcamp), another of the country club set, considers herself Phils girl. Noting a change in the now more thoughtful and considerate man, she follows him one day to the library and sees her rival. Eventually, she realizes that his love for Amelia has matured him, and wishes him well.

When Amelia becomes sick, the doctor recommends she get some nourishing food, such as chicken. Mrs. Griggs tries unsuccessfully to buy one on credit (a scene observed by Phil). In desperation, she steals a cooked chicken from Mrs. Olsens open window; this is witnessed by the horrified Amelia. While Amelia does not see her mother change her mind and put it back, Mrs. Olsen and Peter do. When Mrs. Olsen threatens to make this theft known, Peter insists he will leave home if she does. Meanwhile, Phil sends anonymously a basket of food (including a chicken) to the Griggs. However, Amelia refuses to eat it, as she believes it was stolen.

The next day, though she is still ill, she goes to work, as it is payday. Afterward, she goes to apologize to Mrs. Olsen and to pay for the theft. Her teary attempt to make amends moves her neighbor, who denies she lost a chicken. The strain is too much for Amelia; she faints. Phil and Mrs. Griggs rush over and take her home. There, Phil confesses it was he who sent the chicken. Amelia is finally won over.

Phil writes his father about the inadequate salaries paid to the teachers, calling it a "blot on the present day civilization"; impressed, Philip West Sr. comes to see his altered son and agrees that something must be done. In the meantime, Phil dragoons his friends into paying the professor for extra tutoring in the evenings. During that nights session, both Peter and Gates see that Amelia has given her heart to Phil. In the final scene, the saddened reverend congratulates them and makes his way home.
 

==Cast== Philip Hubbard as  The Professor, Andrew Theodore Griggs 
*Margaret McWade as  His Wife, Mrs. Griggs 
*Claire Windsor as  His Daughter, Amelia Griggs 
*Louis Calhern as  His Pupil, Phil West 
*Marie Walcamp as  The Other Girl, Juanita Claredon

==External links==
 
* 
* 
* 

 
 
 
 
 
 
 