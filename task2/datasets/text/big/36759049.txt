Side by Side (2012 film)
{{Infobox film
| name           = Side by Side
| image          = Side by side 2012.jpg
| caption        = Theatrical release poster
| director       = Christopher Kenneally  . Side by Side the Movie. Retrieved December 2, 2012. 
| producer       = Keanu Reeves Justin Szlasa
| writer         =
| starring       = {{Plainlist |
* James Cameron
* David Fincher
* David Lynch
* Robert Rodriguez
* Martin Scorsese
* Steven Soderbergh
* Keanu Reeves
}}  
| music          = Brendan Ryan Billy Ryan
| cinematography = Chris Cassidy
| editing        = Mike Long Malcolm Hearn Kamil Dobrowolski
| studio         = Company Films 
| distributor    = Tribeca Film
Axiom Films  
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $58,825   
}}
Side by Side is a 2012 American  . April 26, 2012. Retrieved December 2, 2012.   .  . August 15, 2012. Retrieved December 2, 2012.  It was produced by Justin Szlasa and Keanu Reeves.     It premiered at the 62nd Berlin International Film Festival and it was shown at the Tribeca Film Festival.  

== Synopsis ==
The documentary investigates the history, process and workflow of both digital and photochemical film creation. It shows what artists and filmmakers have been able to accomplish with both film and digital and how their needs and innovations have helped push filmmaking in new directions. Interviews with directors, cinematographers, colorists, scientists, engineers and artists reveal their experiences and feelings about working with film and digital. 

== Cast ==
 
* Keanu Reeves, actor
* John Malkovich, actor
*Danny Boyle, director
* George Lucas, director, producer
* James Cameron, director
* David Fincher, director
* David Lynch, director
* Robert Rodriguez, director
* Martin Scorsese, director
* Steven Soderbergh, director
* The Wachowskis, directors
* Christopher Nolan, director
* Joel Schumacher, director
* Richard Linklater, director
* Lars von Trier, director
* Lena Dunham, director, actress
* Greta Gerwig, actress
* Caroline Kaplan, producer
* Lorenzo di Bonaventura, producer
* John Knoll, visual effects supervisor at Industrial Light & Magic
* Tim Webber
* Dennis Muren, special effects artist
* Bradford Young Craig Wood
* Derek Ambrosi, film editor
* Dion Beebe, American Society of Cinematographers|A.C.S.
* Jost Vacano, American Society of Cinematographers|A.C.S.
 

And many more. IMDB lists 69 cast members in total.

== Reception ==
Based on 64 reviews collected by Rotten Tomatoes, the film received a 94% approval rating from critics. 

== References ==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 