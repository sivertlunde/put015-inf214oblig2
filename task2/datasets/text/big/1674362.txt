Namukku Parkkan Munthiri Thoppukal
{{Infobox film
| name           = Namukku Parkkan Munthiri Thoppukal
| image          = Namukku Parkannew.jpg
| caption        = Poster Shari Thilakan, Vineeth Kaviyoor Ponnamma
| director       = P. Padmarajan
| writer         = P. Padmarajan
| story          = K. K. Sudhakaran
| producer       = Mani Malliath
| editing        = B. Lenin Venu
| distributor    = Century Release
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Malayalam Johnson
| budget         =
}}
 1986 Cinema Indian feature directed by Padmarajan.   

The film is noted for its rich and detailed screenplay, expressive cinematography and flowing musical narration. The film is considered as one of the seminal works of the golden age of Malayalam films that lasted from the late 1980s until the early 1990s.

==Plot==

Namukku Parkkan Munthiri Thoppukal is a warm and lyrical motion picture centered around a Malayalee Syrian Christian- , Shari, Thilakan, Vineeth, and Kaviyoor Ponnamma. The film is especially noted for the brilliant performance of Thilakan and also for the flowing and warm thematic music by Johnson (composer)|Johnson. Mohanlals portrayal of Solomon has brought out one of the most matured romantic characters in Malayalam Cinema.

==Biblical Allusions==
From the right beginning itself it is evident that the film has many allusions to the holy Bible. If not for many other characters, the character of Solomon played by Mohan Lal has the greatest amount of allusions to the holy Bible. The name Solomon itself is derived from the Bible where Solomon is one of the kings who ruled the United Israel. And Bible gives sufficient information that the king was very wealthy just like the character in the film. In the Bible it is stated that Solomon had many relationships with women those resulted in having 700 wives and 300 concubines. In the movie we see the character of Solomon is in possession of vast acres of vineyards which he look after and prosper. Same way in the holy Bible we see Solomon is building a large Temple for God. There are hints that Solomon has had respect and appreciation of the queen of Sheba. In many other legends it is stated that Solomon had an affair with the queen of Sheba and she bore him a son though the holy Bible does not accommodate such ideas. In the holy Bible we see the kingdom of Solomons is destroyed because of the sins he committed. According to 1 Kings 11:4 Solomons "wives turned his heart after other gods", their own national deities, to whom Solomon built temples, thus incurring divine anger and retribution in the form of the division of the kingdom after Solomons death (1 Kings 11:9–13). In the movie we see Solomon is taking away Sofia to the vineyards who caused him much trouble. It might be a suggestion that there is a possible unholy turn of events await the character Solomon of the film to where he takes her to just like that is in the case of the Biblical character of King Solomon.

==Cast==

* Mohanlal as Solomon Shari as Sofia
* Thilakan as Paul Pailokkaran(Sofias Stepfather)
* Vineeth as Antony joseph (Solomons Cousin)
* Kaviyoor Ponnamma as Reetha (Solomons Mother)

==Soundtrack== Johnson and lyrics was written by ONV Kurup.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakaashamaake || K. J. Yesudas || ONV Kurup ||
|-
| 2 || Pavizhampol || K. J. Yesudas || ONV Kurup ||
|}

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 