Razzia (film)
{{Infobox film
| name           =Razzia 
| image          = 
| caption        =
| director       =
| producer       =
| writer         = 
| starring       =
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       =  
| runtime        =
| country        = East Germany
| language       = German
| budget         =
| gross          =
}} East German crime film. It was released in 1947.

The film takes place in Berlin, in the direct aftermath of Germanys defeat in the Second World War.

The black market is rife in the ruined city. Chief Inspector Friedrich Naumann (Paul Bildt) organizes a raid on the The Ali Baba Club, a suspected center of a black market gang, but the raid fails due to the gang having an informer in the police ranks. Later, Naumann investigates alone, discovers a secret tunnel in the club, and gets murdered. 

The plot then thickens around the complicated relationships between Goll (Harry Frank) - club owner and gang boss; the singer Yvonne (Nina Kosta), Golls employee and accomplice; Heinz Becker, Naumanns colleague who had been blackmailed into acting as an informant; and Paul Naumann (Friedhelm von Petersson), the inspectors son, a recently returned Prisoner of War who works as a driver in Golls drug pushing ring until realizing that it was Goll who murdered his father. 

In the cataclysmic conclusion, the police manages to carry out another raid, a successful one this time, round up members of the gang and undo Golls dark machinations. 

At the time, Berlin - where the film is set and where it was also filmed - was under complete four-power occupation, and the rival German Democratic Republic and German Federal Republic had not yet been set up. Still, the situation of occupation is in this film pushed to the background, with all characters, positive and negative, being Germans and the conflict in the film being between German police and German criminals.   

The picture sold more than 8,090,000 tickets. 

==References==
 

==External links==
*  

 
 
 
 

 