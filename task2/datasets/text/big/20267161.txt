Home of Angels
 
{{Infobox film
| name           = Home of Angels
| image          = 
| image_size     = 
| caption        = 
| director       = Nick Stagliano 
| producer       = James Oliva
| writer         = James Oliva Nicolas L. DePace
| narrator       = 
| starring       = Abe Vigoda Sherman Hemsley Joe Frazier Lance Robinson Craig Sechler Karin Wolfe  George Small
| cinematography = David Max Steinberg
| editing        = Thomas R. Rondinella
| distributor    = 
| released       = 1994
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 written by directed by Nick Stagliano.

==Plot==
  Long Island Philadelphia to sneak his grandfather (Abe Vigoda) out of a nursing home and bring him home for the holidays. The convelescent homes inhabitants distract the staff so Billy and his confused Grandfather can make their escape. The cross path with a street gang whose leader (Aries Spears) commands a pursuit. They are given a hiding place by concerned homeless man Buzzard Bracken (Sherman Hemsley), but their sanctuary is only temporary, as the gang invades the homeless camp and capture Billy and Gramps. Buzzard and his homeless friends rescue the two and raise money by panhandling in order to get them on their way back to Long Island and a Christmas reunion.

==Cast==
*Abe Vigoda as Henry Bruggers
*Sherman Hemsley as Buzzard Bracken
*Karin Wolfe as Laura Bruggers
*Craig Sechler as Matthew Bruggers
*Joe Frazier as Thadeous
*Lance Robinson as Billy
*Carla Belver as Martha
*John Brumfield as Sammy
*Henry R Ford as Claude Cremson
*Alice Spivak as Josie
*Anne Murray as Jenny
*Michael Massee as Detective Baines
*Aries Spears as Gang leader
*Jonathan Sam as Gang Member
*Bill Rowe as Fred
*Ray Xifo as Charlie the Cabbie

==Production==
  film when he was then 17. 

==Distribution==
Home of Angels was distributed on VHS by Bridgestone Multimedia. 

==Critical response==
 
Tv Guide: "With its falsely upbeat approach to Alzheimers disease and homelessness, the sticky-sweet Home of Angels plays like a PSA masquerading as timely drama. Although it merits consideration for delving into topics rarely addressed in childrens movies, its good intentions are undone by its cutesy tone" 

==References==
 

==External links==
* 
* 

 
 