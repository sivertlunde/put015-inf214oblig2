Az Életbe táncoltatott leány
{{Infobox film
| name           = Az Életbe táncoltatott leány
| image          =
| image size     =
| caption        =
| director       = Tamás Banovich
| producer       =
| writer         = Tamás Banovich
| narrator       =
| starring       = Adél Orosz
| music          =
| cinematography = Ferenc Szécsényi
| editing        = Zoltán Kerényi
| distributor    =
| released       = 1964
| runtime        =
| country        = Hungary
| language       = Hungarian
| budget         =
| preceded by    =
| followed by    =
}}

Az Életbe táncoltatott leány is a 1964 Hungarian film directed by Tamás Banovich. It was entered into the 1965 Cannes Film Festival where it won a Technical Prize.   

==Cast==
* Adél Orosz - A lány
* Levente Sipeki - A fiú
* György Bárdy - Black Man
* Irma Vass - (as Vas Irma)
* Tamás Major - Képmutogató
* Hilda Gobbi - A Képmutogató felesége
* Zsolt Galántai - A Képmutogató fia

==References==
 

==External links==
* 

 
 
 
 
 