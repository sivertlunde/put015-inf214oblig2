Aindhaam Thalaimurai Sidha Vaidhiya Sigamani
{{Infobox film
| name           = Aindhaam Thalaimurai Sidha Vaidhiya Sigamani
| image          = 
| alt            =  
| caption        = Promotional poster
| director       = L. G. Ravichandhran
| producer       = Pushpa Kandaswamy S. Mohan
| writer         = 
| starring       = Bharath Nandita Simon
| cinematography = P. G. Muthaiah
| editing        = V.Vijay
| studio         = Rajam Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}} Simon composes the films music. The film was released on 22 August 2014 to mixed reviews. 

==Plot==
The movie starts of with Sigamani(Bharath) as a young student refusing to go to school. His father, after having heated arguments with the school teacher, says that his son doesnt have to go to school anymore. Thus, Bharath grows up as an illiterate and uneducated. After 25 years, Sigamani has grown up to be a sidha vaidhiyar, following his familys roots. There is also a marriage broker in this story,Vijay Sethupathy(Singam Puli),who tries very hard to find an educated girl for Sigamani. But he is unable to. Due to the fact that Sigamani is uneducated, he is easily cheated by his friends(Badava Gopi,etc...). His only trustable friend is PaalPandi(Karunakaran), who follows his wherever he goes. In desperation to get married to an educated girl, Sigamani plans to wait outside a college to find a girl to love. He then sees Nandini(Nanditha) and decides to go after her. One day, both Sigamani and PaalPandi follow Nandini and finally end up in her house. After listening to an unknown phonecall, Nandinis father(Silambu Chinnadurai) mistake Sigamani to be a doctor who has studied MBBS. This is when all the confusion starts. Thinking that Sigamani is a doctor, Chinnadurai arranges for the marriage. The marriage also takes place successfully and Sigamani is happy as his dream of marrying an educated girl has come true. Before the interval, the director then places another major twist in the story, to keep the audience in the edge of their seats.To be continued...


==Cast==
 
*Bharath as Aindhaam Thalaimurai Sidha Vaidhiya Sigamani aka Sigamani
*Nandita as Nandini Karunakaran as Paalpandi
*Manobala as Soori
*Thambi Ramaiah as Silambu Chinnadurai
*Imman Annachi as Sivakarthikeyan 
*Singampuli as Vijay Sethupathy Komal Kumar
*Badava Gopi
*Pattukottai Sivanarayana Moorthy
*Junior Balaiah
*Kili Ramachandran
*Crane Manohar
*Rajendran
*Kadhal Dhandapani
*M. S. Bhaskar
*Kottachi
*Chaams
*Mayilsamy Renuka
*Sounder
*Bose Venkat Pandu
*Madhan Bob Robert in a cameo appearance
*Gaana Bala in a cameo appearance
*Yabama JO in a cameo appearance
 

==Production==
In December 2013, it was reported that Bharath and Nandita would come together for a comedy film to be directed by new director Ravichandar.    The film, funded by Rajam Productions, was told to be a comic tale on the life of an illiterate husband (Bharath) and an educated wife (Nandita).    Twenty one comedy actors were signed for the film, with the director announcing that the film would run along the lines of Kadhalikka Neramillai (1964) and Ullathai Allitha (1996).   

The film is set in Salem and 25&nbsp;days of shoot was completed in the localities like Coimbatore, Pollachi, Pazhani and Dindiugal.   

==Soundtrack==
{{Infobox album
| Italic title  = 
| Name          = Ainthaam Thalaimurai Siddha Vaidhya Sigaamani
| Type          = soundtrack
| Longtype      = to Ainthaam Thalaimurai Siddha Vaidhya Sigaamani Simon
| Cover         =
| Border        = 
| Alt           = 
| Caption       = Front Cover
| Released      = 2 July 2014
| Recorded      = 2014 Feature film soundtrack
| Length        =  Tamil
| Label         = Sa Re Ga Ma Simon
| Chronology    = 
| Last album    = Ainthu Ainthu Ainthu (2013)
| This album    = Ainthaam Thalaimurai Siddha Vaidhya Sigaamani (2014)
| Next album    = 
}}
 Simon of Ainthu Ainthu Ainthu fame. The songs were written by Yugabharathi and the Elarai song written and sung by Gana Bala also featured Yabama JO, a Congo based singer. The audio was released by veteran director Padmashree K. Balachander himself. The launch of the films album was held on 2 July 2014

{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = yes
| title1          = Ainthaam Thalaimurai Siddha Vaidhya Sigaamani Simon
| Simon , Yabama JO
| length1         = 
| title2          = Onnuna Rendu Varum
| lyrics2         = Yugabharathi
| extra2          = Vijay Antony, Sunidhi Chauhan
| length2         = 
| title3          = En Anbe 
| lyrics3         = Yugabharathi
| extra3          = Vijay Prakash, Ramya NSK
| length3         = 
| title4          = The Elarai
| lyrics4         = Gana Bala
| extra4          = Gana Bala, Feat : Yabama JO
| length4         = 
| title5          = Kandangi Selai
| lyrics5         = Yugabharathi
| extra5          = Hariharasudan
| length5         = 
}}

==Release==
The satellite rights of the film were sold to Polimer TV|Polimer. The film released on 22 August 2014 in 900 screens worldwide which makes a biggest release for bharath starrer film. 

===Critical reception===
Times of India wrote:"ATSVS is a film so dated that by the interval the reek of staleness is so strong that we start looking at the ticket to see if there was an expiry date mentioned".  Behindwoods wrote:"In short ATSVS achieves fairly what it sets out to.  If you are the type who would not take cinema seriously, ATSVS may appeal to you in some ways".  Indiaglitz wrote:"ATSVS is nothing but a customary comical movie". 

===Box office===
The film opened "low" response at the box office.The film collected  1.1 crore in its opening day which is the worst opening ever for bharath.The film showed improvement in second day it collected  2 crore at the box office.THe film collected  5.4 crore in its opening weekend which marks worst opening for ever bharath starrer film.Despite releasing high releasing number of screens and positive pre release buzz, the film went to collect  11.2 crore in its final worldwide.The film declared Disaster at the box office.

== References ==
 

==External links==
* 

 
 
 
 
 
 