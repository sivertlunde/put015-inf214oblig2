The Phantom of the West
 
{{Infobox film
| name           = The Phantom of the West
| image          = File:Phantom of the West poster 2.jpg
| caption        = Film poster
| director       = D. Ross Lederman Theodore Joos
| producer       = Nat Levine Ben Cohen William Desmond Tom Santschi Dorothy Gulliver Philo McCullough Frank Lanning
| music          = Lee Zahler
| cinematography = M. A. Anderson Ben Kline Joseph Novak
| editing        = Wyndham Gittens
| distributor    = Mascot Pictures
| released       =  
| runtime        = 10 chapters (172 minutes)
| country        = United States
| language       = English
| budget         = 
}} Mascot western movie serial and was the second all-talking serial they produced.
 mysterious Phantom and his League of the Lawless.

==Plot==
Francisco Cortez escapes prison after serving fifteen years for the murder of Jim Lesters father.  He proclaims his innocence and lists seven men who may be the real killer, who uses the name "The Phantom".

==Cast==
*Tom Tyler as Jim Lester William Desmond as Martin Blaine
*Tom Santschi as Bud Landers
*Dorothy Gulliver as Mona Cortez, Francisco Cortezs daughter
*Philo McCullough as Royce Macklin
*Frank Lanning as Francisco Cortez, escaped convict falsely accused of murdering Jim Lesters father
*Tom Dugan as Oscar, Sheriffs deputy and comic relief
*Joe Bonomo as Keno, a gunman
*Frank Hagney as Sheriff Jim H. Ryan
*Hallie Sullivan as Ruby Blair
*Kermit Maynard as Peter Drake
*Dick Dickinson as Harvey Stewart
*James Carlyle as Sam Hollister
*W.J. Holmes as Pee Wee

==Production==
===Stunts===
*Joe Bonomo
*Yakima Canutt Cliff Lyons
*Kermit Maynard

==Chapter titles==
# The Ghost Rides
# Stairway of Doom
# Horror in the Dark
# Battle of the Strong
# League of the Lawless
# Canyon of Calamity
# Price of Silence
# House of Hate
# Fatal Secret
# Rogues Roundup
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = pp. 203
 | chapter = Filmography
 }} 

==Trivia== The Phantom.

==See also==
* List of film serials
* List of film serials by studio

==References==
 
 

==External links==
* 
* 

 
{{succession box  Mascot Serial Serial 
| before=The Lone Defender (1930)
| years=The Phantom of the West (1931)
| after=King of the Wild (1931)}}
 

 
 

 
 
 
 
 
 
 
 