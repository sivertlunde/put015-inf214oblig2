Nazrana (1961 film)
{{Infobox film
| name           = Nazrana
| image          = nazrana_1961.jpg
| caption        = Film Poster
| director       = C. V. Sridhar
| writer         = C. V. Sridhar (story) Rajendra Krishan (dialogue)
| producer       = S. Krishnamurthy Agha
| Ravi
| cinematography = A. Vincent
| editing        = N. M. Shankar
| distributor    =
| studio         =
| released       =  
| runtime        = 130 minutes
| country        = India Hindi
| budget         =  40,00,000 (estimated)
| gross          =  90,00,000
}} Agha as an ensemble cast while South Star Gemini Ganesan has an extended cameo appearance.  The music is by Ravi (music director)|Ravi.
 1959 Tamil triangular love story between Raj, Vasanthi and Geeta.

== Plot ==
Raj (Raj Kapoor) and Vasanti (Vyjayanthimala) are college mates who clash when she complains to the college principal about a love letter he sent her. Later on realizing her mistake, Vasanti apologizes to him and the two fall in love. Vasantis elder sister, Geeta (Usha Kiron), supports the family by stitching clothes. Raj rents the room upstairs in their house. He falls ill and in nursing him, Geeta falls in love with him. She confides her love to Vasanti, who decides to sacrifice her love for her the sake of her sister and convinces Raj to marry Geeta. Raj initially neglects Geeta. On finding out, Vasanti writes to him that their sacrifice, made for Geetas happiness, would mean nothing unless he is a good husband to Geeta. Raj relents and Geeta and he have a son. Vasanti joins them and Geeta suspects that there is something on between Raj and Vasanti and makes Vasanti leave the house. A few years later Geeta, having found out that Raj and Vasanti loved each other, dies in guilt leaving Raj alone to bring up their child making him promise that he will make Vasanti the childs mother. Raj learns of Vasantis impanding marriage to her former boss (Gemini Ganesh). By the time he reaches there, Vasanti is already married. He hands over his child to Vasanti as a wedding gift and walks away.

== Cast ==
*Raj Kapoor as Rajesh
*Vyjayanthimala as Vasanthi
*Usha Kiran as Geetha
*Gemini Ganesan as Shyam, Vasanthis boss in Guest appearance. Agha as Murli
*Achala Sachdev as Gethas and Vasanthis mother.

== Casting ==
Initially the shooting of the film was started with actress B. Saroja Devi who reprises her role of Geetha from Kalyana Parisu and Pelli Kanuka.    During the filming process she felt out with the director C. V. Sridhar and was replaced by actress Vyjayanthimala, who has worked with the director before in Then Nilavu. 

== Box office == 12th highest grossing film of 1961. 

== Soundtrack ==
{{Infobox album   
| Name = Nazrana
| Type = soundtrack Ravi
| Cover = nazrana_sound.jpg
| Released =  
| Recorded = 
| Re-recorded =  Feature film soundtrack
| Length = 
| Label =  Sa Re Ga Ma HMV Group
| Producer =
| Reviews = 
| Last album = Ghunghat (1960 film)|Ghunghat (1960)
| This album = Nazrana (1961)
| Next album = Gharana (1961 film)|Gharana (1961)
}} Ravi with the lyrics were provided by Rajendra Krishan.  There are five songs in the album and the song Ek Woh Bhi Diwali becomes popular. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Picturization || Length (m:ss) ||Lyrics ||Notes
|-
| 1|| "Baazi Kisine Pyar Ki" ||   ||
|-
| 2|| "Mele Hain Chiraghon Ki Diwali" ||   || 
|-
| 3|| "Mere Peechhe Ek Diwana" ||   ||
|-
| 4|| "Ek Woh Bhi Diwali Thi" || Mukesh || Featuring actor   || 
|-
| 5|| "Bikhra Ke Zulfen Chaman Mein" || Mukesh,   || 
|}

== Awards ==
=== Filmfare Awards ===
1961 - Filmfare Award for Best Story - C. V. Sridhar 

== Character map of Kalyana Parisu and its remakes ==
{| class="wikitable"
|-
! Kalyana Parisu (Tamil language|Tamil) (1959)
! Pelli Kanuka (Telugu language|Telugu) (1960)
! Nazrana (Hindi) (1961)
! Devatha (Telugu language|Telugu) (1982)
! Tohfa (Hindi) (1984)
|-
| Bhaskar (Gemini Ganesan)
| Bhaskar (Akkineni Nageswara Rao)
| Rajesh (Raj Kapoor)
| (Sobhan Babu)
| Ramu (Jeetendra)
|-
| Vasanthi (B. Saroja Devi)
| Vasanthi (B. Saroja Devi)
| Vasanthi (Vyjayanthimala)
| (Sridevi)
| Lalita (Sridevi)
|-
| Geetha (Vijayakumari) Krishna Kumari)
| Geetha (Usha Kiran)
| (Jaya Prada)
| Janki (Jaya Prada)
|-
| Raghu (Akkineni Nageswara Rao)
| Raghu (Kongara Jaggayya)
| Shyam (Gemini Ganesan)
| Ramudu (Mohan Babu)
| 
|}

== References ==
 

== External links ==
*  
*   at Upperstall.com

 

 
 
 
 
 
 