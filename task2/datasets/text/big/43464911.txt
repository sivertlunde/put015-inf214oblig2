Enga Ooru Kavalkaran
{{Infobox film
| name           = Enga Ooru Kavalkaran
| image          = Enga Ooru Kavalkaran.jpg
| image_size     =
| caption        =
| director       = T. P. Gajendran
| producer       = Kalyani Murugan
| writer         = Sangili Murugan
| screenplay     =
| starring       = Ramarajan Gouthami M. N. Nambiar T. P. Gajendran
| music          = Ilayaraja
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Tamil
}}
 1988 Cinema Indian Tamil Tamil film, directed by  T. P. Gajendran and produced by Kalyani Murugan. The film stars Ramarajan, Gouthami, M. N. Nambiar and T. P. Gajendran in lead roles. The film had musical score by Ilayaraja. 

==Cast==
*Ramarajan
*Gouthami
*M. N. Nambiar
*T. P. Gajendran
*S S Chandran
*Sangili Murugan
*Senthil
*Usilaimani
*Kovai Sarala
*Y. Vijaya

==Soundtrack==
The music was composed by Ilaiyaraaja.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Gangai Amaran Na Kamarasan Piraisudan || 04:38
|-
| 2 || Arumbaki || Deepan Chakravarthy || 04:25
|-
| 3 || Enga Ooru Kaavalkaaran  || Ilaiyaraaja || 03:37
|-
| 4 || Jivunu Jivunu || Mano (singer)|Mano, P. Susheela || 04:18
|-
| 5 || Maalai Karukayil || Mano (singer)|Mano, P. Susheela || 04:08
|-
| 6 || Aasaiyilae - 2 || Mano (singer)|Mano, P. Susheela || 04:17
|-
| 7 || Siruvani || Ilaiyaraaja, S. P. Shailaja, Sunantha || 04:00
|-
| 8 || Thopporam || P. Susheela || 04:20
|}

==References==
 

==External links==
*  

 
 
 
 
 


 