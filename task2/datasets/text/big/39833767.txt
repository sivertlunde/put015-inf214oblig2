One Direction: This Is Us
 
 
{{Infobox film
| name           = One Direction: This Is Us
| image          = One Direction This is Us Theatrical Poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Morgan Spurlock
| producer       = Morgan Spurlock Simon Cowell Adam Milano Ben Winston
| starring       = {{plainlist| Niall Horan Zayn Malik Liam Payne
* Harry Styles
* Louis Tomlinson
}}
| music          = One Direction
| cinematography = Neil Harvey
| editing        = Guy Harding Wyatt Smith Pierre Takal Warrior Poets Modest Entertainment
| distributor    = TriStar Pictures
| released       =    Extended fan cut: 106 minutes  
| country        = United Kingdom United States  
| language       = English
| budget         = $10 million   
| gross          = $68,532,898 
}}
One Direction: This Is Us is a 2013 British-American 3D film|3-D concert film centering on British-Irish group One Direction. It opened in the United Kingdom on 29 August 2013, followed a day later in the United States. The movie shows many clips and songs of One Direction.

==Synopsis== O2 Arena, X Factor appearance, and gives insight into the preparation for their concerts and ultimately what it is like to be One Direction.  

==Cast==
* Niall Horan
* Liam Payne
* Harry Styles
* Louis Tomlinson
* Zayn Malik

Cameos
*Simon Cowell

==Production== RED Epic digital cameras, which Spurlock said gave a cinematic appeal. The film was later given the name One Direction: This Is Us on 19 March 2013, previously being referred to as 1D3D. 

It has been further said that the film is not scripted, with the footage genuine and of them acting "naturally".  Styles admitted  that the cameras following them around was scary but said that this film gives them the opportunity to show more personality, how they interact, and how they relax when not on stage. Niall Horan remarked  that the film crew had access to all areas, even filming them in the toilet.
 The X Factor and is also a producer of the film.

There have been rumours of a sequel, though these have been denied by the record label.   

==Release== world premiere of the film was held in Leicester Square, in London on 20 August 2013.  

The film was released in the United Kingdom on 29 August 2013,  and was rolled out internationally on 30 August 2013 with a majority of markets being reached by the end of September 2013. 

===Marketing===
As part of the marketing campaign, the band allowed fans to upload pictures of themselves that would appear on a version of the theatrical poster. The theatrical poster itself has pictures of fans forming part of the background, with another version created and displayed on the bands website where the photos were used to recreate the poster itself. 
 teaser trailer Best Song Ever", the lead single from their third studio album, Midnight Memories.

===Box office===
This Is Us has earned $28,873,376 in North America, and $38,455,345 in other countries, for a worldwide total of $67,328,742.  It is currently the fourth highest grossing concert movie. 

The film scored $2.7 million on its Thursday debut,   and topped the box office on its opening weekend, earning $17,000,000, far better than other concert films like  ,  , and  , but not enough to surpass  ,   and Michael Jacksons This Is It.  This Is Us is the third concert film to top the box office, after the Hannah Montana and Michael Jackson films.

According to Forbes, the film would top the box office on its opening weekend with an estimated $18 million.  The Hollywood Reporter said the film would earn about $45 million in its four-day debut. 

The film has been a box office success worldwide, grossing over six times its budget.

===Critical response===
 
This Is Us has received mixed reviews prior to release, currently holding a 64% rating on  , signifying "mixed or average reviews". 
 New York A Hard Day’s Night." Nor will they take Spurlocks routine camera work for the pop-art panache of Richard Lester.  The Washington Post; film critic Stephanie Merry felt "To add a bit of emotion, the film hears from the boys’ mothers and follows the five as they take a break from touring to return home. But for the most part, the movie embraces harmless fun, which can be enjoyable for the audience members, whether they’re 1D fans or not" finally adding "Some of the guys have made gossip rag headlines for their wild ways, including drunken behavior, but there’s no hint of that here... Maybe showing those details carries the risk of alienating One Direction’s fans. When the guys sing "I’m in love with you" the "you" is meant to be each starry-eyed listener. But the absence of certain truths makes the movie feel more like marketing material for superheroes than a comprehensive documentary about human dimension. If One Direction fans end up having inordinately high standards when it comes to love, Spurlock is at least partly to blame.". 
 Spurlock makes that connection more than once—first by noting that One Direction has conquered the world even faster than its Liverpool predecessors, second by casting the new British invaders in a loose A Hard Day’s Night homage.".  Chicago Sun-Times s Bill Zwecker felt "My major issue with “One Direction: This Is Us” is the use of distracting 3-D effects that feel forced. And the insertion of animation during some of the concert footage come off as jarring and kind of amateurish. That said, the film is a successful witness to the great charm possessed by all five members of One Direction. I loved it when they were seen as the kids they still are, horsing around with their stage crew and bodyguards — joyfully letting off steam as they careen backstage on a forklift theyve “hijacked.”". 

A more negative review came from   concert film coming out later this week that makes One Direction: This Is Us look like Martin Scorseses The Last Waltz.".  The New York Times also gave it a negative review, Miriam Bale felt "With a group so evidently versed in the visuals of rock history, it’s a shame that a filmmaker wasnt hired who would pay homage to classic pop films instead of offering a satisfactory paid promotional. In the end credits — Richard Lester-style scenes of the boys in costumes doing pranks — we see how this film might have been more successful: as an obvious fiction starring these appealing personalities rather than a tame and somewhat fake documentary." 

Audiences responded positively, garnering the film a 79% audience approval rating on Rotten Tomatoes. Viewers who saw the film on the opening Thursday, gave the average grade of A, according to market research firm CinemaScore.  The audience was 87% female, and 65% under the age of 17. 

===Extended cut===
On 9 September 2013, it was announced that Sony would release an "extended fan cut" of This Is Us on 13 September. This version included an additional 20 minutes of footage with four new songs.   

===Blu-ray and DVD===
The movie was released on   in 2010 by 10,000 copies.  . 

===Charts and certifications ===
 
 
{| class="wikitable sortable plainrowheaders"
|-
! scope="col"| Chart (2013/2014)
! scope="col"| Peak position
|-
 
|-
 
|-
 
|-
 
|}
 

 
 
 
 

==International releases==
 
 
;Europe
* Austria: 11 October 2013
* Belgium: 28 August 2013
* Bulgaria: 30 August 2013
* Croatia: 29 August 2013
* Bosnia and Herzegovina: 29 August 2013
* Cyprus: 30 August 2013
* Czech Republic: 5 September 2013
* Denmark: 28 August 2013
* Estonia: 30 August 2013
* Finland: 30 August 2013
* France: 28 August 2013
* Germany: 27 August 2013  (early screening) 
* Germany: 12 September 2013  (actual release date) 
* Greece: 29 August 2013
* Hungary: 5 September 2013
* Iceland: 6 September 2013
* Italy: 5 September 2013
* Kazakhstan: 30 August 2013
* Latvia: 29 August 2013
* Lithuania: 29 August 2013
* Republic of Macedonia|Macedonia: 25 September 2013
* Republic of Ireland: 29 August 2013
* Netherlands: 29 August 2013
* Norway: 6 September 2013
* Poland: 30 August 2013
* Portugal: 29 August 2013
* Romania: 30 August 2013
* Russia: 30 August 2013
* Serbia: 29 August 2013
* Slovakia: 5 September 2013
* Slovenia: 28 August 2013
* Spain: 30 August 2013
* Sweden: 28 August 2013
* Switzerland: 30 August 2013
* Turkey: 30 August 2013
* United Kingdom: 29 August 2013

 

;Northern America
* United States: 30 August 2013 
* Canada: 30 August 2013

;Latin America
* Argentina: 28 August 2013  (premiere exclusive)  29 August 2013
* Bolivia: 10 September 2013
* Brasil: 6 September 2013
* Chile: 29 August 2013
* Colombia: 30 August 2013
* Dominican Republic: 29 August 2013
* Ecuador: 30 August 2013
* Jamaica: 30 August 2013
* Mexico|México: 30 August 2013
* Peru|Perú: 29 August 2013
* Puerto Rico: 5 September 2013
* Uruguay: 30 August 2013
* Venezuela: 30 September 2013

;Middle East/Africa And Asia
* Bahrain: 29 August 2013
* Egypt: 11 September 2013
* Ethiopia: 30 August 2013
* Indonesia: 31 August 2013
* Iraq: 29 August 2013
* Israel: 29 August 2013
* Jordan: 29 August 2013
* Kuwait: 29 August 2013
* Lebanon: 29 August 2013
* Malaysia: 29 August 2013
* Oman: 29 August 2013
* Qatar: 29 August 2013
* South Africa: 11 October 2013
* Tunisia : 21 September 2013
* United Arab Emirates: 29 August 2013
* Hong Kong: 29 August 2013
* Philippines: 30 August 2013
* Singapore: 29 August 2013
* India: 6 September 2013
* Nepal: 13 September 2013
* Pakistan: 27 September 2013
 

==Sequel==
  Where We Are Tour. The film will be released on home media by December 2014.   

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 