Keiko: The Untold Story
{{Infobox film
| name           = Keiko: The Untold Story
| director       = Theresa Demarest
| producer       = Theresa Demarest
| image          = Keiko The Untold Story.jpg
| writer         = 
| starring       = 
| distributor    = Joshua Records LLC
| released       = 2010
| runtime        = 75 minutes
| country        = 
| language       = English
| budget         =
| gross          =
}}

Keiko: The Untold story is a 2010 documentary film about Keiko (orca)|Keiko, the Orca best known for starring in the film Free Willy and its two sequels. It was directed and produced by Theresa Demarest. The film made the official selection list of many film festivals, including the Southern Utah International Documentary Film Festival,  the San Francisco Ocean Film Festival  and the Denver Film Festival. 

In August 2013 a HD version of the film entitled Keiko: The Untold Story of the Star of Free Willy (HD) premiered at the Egyptian Theater in Hollywood during the 20th Anniversary Celebration Benefit for the Free Willy Keiko Foundation. The World Premiere of the film included a Blue Carpet Ceremony with the stars and director of both Free Willy and the documentary.

==Awards==
 Fall 2010) 

==References==
 

==External links==
*  
*  

 
 
 
 
 


 