Life in a Day (2011 film)
{{Infobox film
| name           = Life in a Day
| image          = Life in a Day.jpg
| image_size     = 215px
| alt            =  
| caption        = Theatrical release poster Kevin Macdonald
| producer       = Liza Marshall Ridley Scott
| music          = Harry Gregson-Williams Matthew Herbert Joe Walker
| studio         = Scott Free Productions YouTube|YouTube, Inc. LG Corp
| distributor    = National Geographic Films
| released       =  
| runtime        = 95 minutes 
| country        = United States United Kingdom
| language       = English Italian Japanese German Spanish Indonesian Balinese Portuguese Ukrainian Vietnamese Creole Catalan Dutch Bengali Masai Hindi Arabic Quechua Russian
| budget         = 
| gross          = $252,788 
}} crowdsourced Drama (genre)|drama/documentary film comprising an arranged series of video clips selected from 80,000 clips submitted to the YouTube video sharing website, the clips showing respective occurrences from around the world on a single day, July 24, 2010.

The film is 94 minutes 53 seconds long http://www.bbfc.co.uk/AFV276061/  and includes scenes selected from 4,500 hours of footage in 80,000 submissions from 192 nations.   The completed film debuted at the  , October 31, 2011. 

==Production==
The film was produced by Scott Free Productions and the YouTube video sharing site. The film was distributed by National Geographic Films.   The visual effects were produced by Lip Sync Post. 
 Ridley Scott Kevin Macdonald and film editor Joe Walker, consisting of footage from some of the contributors. All chosen footage authors are credited as co-directors.   

The films music was written by British composer and producer Harry Gregson-Williams, along with Matthew Herbert. The films opening song, written by Herbert, was performed by British singer-songwriter Ellie Goulding.  The film also features the song "Jerusalem" by Kieran Leonard  and "Future Prospect" by Biggi Hilmars.

Director  s "SpeakEasy" section, July 22, 2011.  Editor Joe Walker said that as he understood it, the concept for the   magazine, July 29, 2011. 

Macdonald began his "Around the world in 80,000 clips" article in  , June 7, 2011.   The 80,000 individual clips received amounted to 4,500 hours of electronic footage.  Macdonald explained that about 75% of the films content came from people contacted through YouTube, traditional advertising, TV shows, and newspapers; the remaining 25% came from cameras sent out to the developing world, Macdonald pointing out "It was important to represent the whole world."  At a reported cost of £40,000,  "we did resort to snail mail for sending out 400 cameras to parts of the developing world — and getting back the resulting video cards."  Macdonald later remarked that he regretted not sending out a far smaller number of cameras but providing training in camera operation and desired type of content: "Naively, I hadnt realised how alien, not only the concept of a documentary is to a lot of people (in the developing world), but also the idea that your own opinions are worth sharing." 
 Joe Walker told Wired (magazine)|Wired magazines Angela Watercutter that the film "couldnt have been made without technology. Ten years ago it wouldve been impossible."  Macdonald explained that YouTube "allowed us to tap into a pre-existing community of people around the world and to have a means of distributing information about the film and then receiving peoples dailies. It just wouldnt have been organizationally or financially feasible to undertake this kind of project pre-YouTube." 

The filmmaking team “used YouTubes ability to collect all of this material and then we had this sort of sweatshop of people, all multilingual film students, to sift through this material. It couldnt have been done any other way. Nobody had ever done a film like this before, so we had to sort of make it up as we went along.”   "To put (the 4500 hours of raw footage) in context, I just cut a feature film for Steve McQueen and theres 21 hours of   for that." 

Walker, whose team edited the whole film over seven weeks, remarked to Adam Sternbergh of  , July 22, 2011.  Walker indicated that a team of roughly two dozen researchers, chosen both for a cinematic eye and proficiency with languages, watched, logged, tagged, and rated each clip on a scale of one to five stars.  Walker remarked that "the vast amount of material was two stars," and that he and director Kevin Macdonald reviewed the four-star and five-star rated clips. 

In addition to the star rating system, the editing/selecting team also organized the 80,000 clips according to countries, themes and video quality as part of the selection process, and further had to convert from 60 different frame rates to make the result cinematically acceptable. 

==Themes and content== World Cup. 

Concerning the chronology of the film and the order of the clips, Macdonald explained that he let the 300 hours of “best bits” tell him what the themes and structure of the film should be, likening the material to a Rorschach test—"you will see in it what you want to see in it."  Joe Walker further explained that "We always wanted to have a number of structures, so its not just midnight to midnight, but its also from light to dark and from birth to death. ... bashing things together and making them resonate against each other and provoking thought." 

Director Kevin Macdonald said he saw the movie "as a  , July 29, 2011. (Sharkey slightly misstates the "love/fear/pockets" quotation.)  "The story is told through the voices of the contributors, but mostly its the images that do the heavy lifting." 

Macdonald explained that the film "doesnt have a traditional story or a traditional narrative, but it has thematic movement…and ... recurring characters."  He praised certain specific contributions, including "the most technically amazing skydiving shot I have ever seen in any film" and "a hand going up to a window pane and picking a fly off and filming the hand walking through the house and letting the fly go—and you see the fly take off in the distance."  Asked if there any particular submission crystallized the films theme, Macdonald cited "the family who had been going through cancer."  More generally, Macdonald praised the immediacy that a handycam permits. 

Ian Buckwalter of   magazine online, posted July 27, 2011.  The  , July 29, 2011.  Liz Braun, writing in the  , July 29, 2011.   , July 28, 2011.  However,  , August 5, 2011. 

The Los Angeles Times Betsy Sharkey described the progression of the film: "Beginning with videos that start pre-dawn then moving through morning, afternoon and evening, ... the rituals that define a day begin to emerge. Beyond an extraordinary range of cultures, terrain and styles reflected, which are captivating on their own, the film stands as a stirring reminder of how ordinary and yet eclectic humanity can be. If "Life in a Day" is any measure, we are a quirky, likable, unpredictable and yet predictable bunch." 

Yumi Goto of   LightBox (photo editors section), August 1, 2011.   The   movie that manages to feel singular and whole."  Anthony Benigno wrote in Filmcritic.com that the film "is pretty much the first Social media|social-media movie ever made." 

The New York Times  Adam Sternbergh wrote that "the films most memorable moments are the ones of unexpected intimacy. ... The film aims to tell the story of a planet, but its the vulnerability of these individual moments, contributed as part of a larger project, that lingers."  The Los Angeles Times  Betsy Sharkey wrote that "The fact that we all experienced that day is part of what gives the documentary an unusual kind of relatability." 

Sharkey characterized the film as being "the most hopeful yet from Macdonald, a director whos made his reputation by digging into the more corrupted and conflicted side of human nature with One Day in September, his Oscar-winning documentary on the 1972 Munich massacre of Israeli Olympic athletes... (T)he lightness (Macdonald has) unearthed in "Life in a Day" has an earthy and at times euphoric appeal." 

==Reception==
Life in a Day has received generally positive reception from film critics. Rotten Tomatoes reports that 82% of 52 critics have given the film a positive review, with a rating average of 7.1 out of 10.  Metacritic gave the film a rating average of 58/100, indicating "mixed or average reviews". 

Helen OHara from Empire stated that the film was "moving and insightful. Not a classic by any means, but a fascinating glimpse of the way we live today."  Michael OSullivan of the Washington Post gave the film three and a half stars out of four, saying that "Life in a Day is, without exaggeration, a profound achievement."  Peter Howell, a critic of the Toronto Star, gave the film three out of four stars, saying "the vast majority of the film feels undeniably real and incredibly inspiring."  Wired (magazine)|Wired magazines Angela Watercutter wrote that the film "brims with intimacy and urgency." 

 .com, July 29, 2011.  with  .com, July 30, 2011.  Ian Buckwalter of Washingtonian (magazine)|Washingtonian magazine called the condensed experiences "breathtaking" and "as riveting as any narrative." 

Liz Braun, writing in the Toronto Sun, said that "a lot is predictable" and "Its all familiar for the most part, and its all mildly interesting," but also cited several "sequences that fully engage a viewer emotionally."  Andrew Schenker from Slant Magazine criticized the film by stating "Only a few snippets escape the uncritical narcissism that the film celebrates."  Contentions such as Schenkers were contradicted by The New York Times  Adam Sternbergh who wrote that "if the knock against the Internet... is that it stokes our collective narcissism, this film, in its best moments, proves the opposite: not a global craving for exposure but a surprising universal willingness to allow ourselves to be exposed." 

Though saying Life in a Day "isnt a bad movie" and there are "fits and spurts" in which the film is "actually quite beautiful," "funny" and "moving," Anthony Benigno from Filmcritic.com asserted that  , said about the film: "Judging by the National Geographic doc "Life in a Day," a lot of nothing happened on July 24, 2010."  A counterpoint was expressed by the Los Angeles Times  Betsy Sharkey: "the world community had a lot of interesting things on its mind, but it still took filmmakers like Macdonald and Walker to help us say it with feeling." 

Two writers for   Movie Review, July 28, 2011.  In contrast, Adam Sternbergh concluded that "the montages of ordinary acts, repeated from Japan to Dubai to Las Vegas, take on a kind of profundity." 

==Criticism of free labour==
The film has been criticized for its use of free labour. In the film industry, the production teams required to produce images and sounds are normally paid. However, in the case of Life in a Day, the labour undertaken by YouTube users to shoot the content used in film was not compensated, even though the film was screened in traditional cinema venues for a profit.  

==Follow-on projects and legacy==
In October 2011,   oversaw the project with executive producer  , October 5, 2011. 
 Kevin Macdonald crowdsourced clips recorded on Christmas 2012. Gray, Richard,   ( ), The Telegraph, December 9, 2013. • Sainsburys short video:  .  • Full length YouTube documentary:   

Italy in a Day (September 2014), directed by Gabriele Salvatores, included clips selected from 45,000 crowdsourced video submissions recorded on 26 October 2013, and premiered during the 71st Venice International Film Festival. Ide, Ella,   ( ), Business Insider, September 3, 2014. 
 Scott Free Productions to produce Israel in a Day, with Germany and France also working on their own versions in the format. 

==See also==
* Crowdsourcing Kevin Macdonald
* National Geographic Society
* Ridley Scott
* Time capsule
* YouTube

==References==
 

==External links==
*  
*   on YouTube ( )
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 