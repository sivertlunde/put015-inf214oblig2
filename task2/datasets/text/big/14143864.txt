Brenda Starr, Reporter (film)
 
{{Infobox film
| name           = Brenda Starr, Reporter
| image          = Brenda Starr, Reporter.jpg
| image_size     =
| caption        =
| director       = Wallace Fox
| producer       = Sam Katzman
| writer         = Ande Lamb Dale Messick George H. Plympton
| narrator       = Joe Devlin Wheeler Oakman
| music          = Edward J. Kay
| cinematography = Ira H. Morgan
| editing        = Charles Henkel Jr.
| distributor    = Columbia Pictures
| released       =  
| runtime        = 13 chapters 243 minutes
| country        = United States English
| budget         =
| gross          =
}} film serial Brenda Starr, a popular comic strip created by Dale Messick.

==Plot==
Daily Flash newspaper journalist Brenda Starr (Joan Woodbury), and her photographer, Chuck Allen (Syd Saylor), assigned to cover a fire in an old house where they discover the wounded Joe Heller (Wheeler Oakman),a mobster suspected of stealing a quarter-million dollar payroll. The dying Heller tells Brenda that someone took his satchel of stolen money and he gives her a coded message. Kruger (Jack Ingram), the gangster who shot Heller, escapes to his gangs hideout with the bag but discovers it is filled with paper rather than money. The gang, knowing Heller gave Brenda a coded message makes many attempts on her life to get her to reveal where Heller hid the payroll money. But thanks to Chuck and Police Lieutenant Larry Farrel (Kane Richmond), she evades them, until Pesky (William Billy Benedict), a Daily Flash office boy succeeds in decoding the Heller message.

==Cast==
*Joan Woodbury as Brenda Starr. Woodbury had played similar roles in feature films for Columbia and Monogram Pictures|Monogram. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 95
 | chapter = 5. A Cheer for the Champions (The Heroes and Heroines)
 }} 
*Kane Richmond as Lt. Lawrence Farrell
*Syd Saylor as Chuck Allen Joe Devlin as Tim
*Wheeler Oakman as Joe Heller/Lew Heller
*Cay Forrester as Vera Harvey
*Marion Burns as Zelda
*Lottie Harrison as Abretha
*George Meeker as Frank Smith Jack Ingram as Kruger
*Anthony Warde as Muller
*John Merton as Schultz

==Release==
===Theatrical===
The serials theatrical release date was 26 January 1945.

===Home media===
Brenda Starr, Reporter is one of the last sound serials to be made available commercially. For many years, the serial was considered lost, with only a single known print in the hands of a private collector.  The serial was released on DVD by VCI Entertainment in March 2011.

==Critical reception==
Cline writes that Woodbury "managed to carry the story from one episode to another in fine style, leaving herself in jeopardy just enough to require   services as a rescuer each week...   salvaged by her beauty and charm what might have been Katzmans greatest fiasco except for Whos Guilty?" 

==Chapter titles==
# Hot News
# The Blazing Trap
# Taken for a Ride
# A Ghost Walks
# The Big Boss Speaks
# Man Hunt
# Hideout of Terror
# Killer at Large
# Dark Magic
# A Double-cross Backfires
# On the Spot
# Murder at Night
# The Mystery of the Payroll
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 240
 | chapter = Filmography
 }} 

==See also==
* List of American films of 1945 List of film serials by year
*List of film serials by studio

==References==
 

==External links==
*  
*  

 
{{succession box  Columbia Serial Serial
| Black Arrow (1944)
| years=Brenda Starr, Reporter (1945)
| after=The Monster and the Ape (1945)}}
 

 
 

 
 
 
 
 
 
 
 