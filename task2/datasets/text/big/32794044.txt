Believe Me, Xantippe
{{infobox film
| name           = Believe Me, Xantippe
| image          = Believe Me, Xantippe - 1918 loby card.jpg
| imagesize      =
| caption        = 1918 lantern card
| director       = Donald Crisp
| producer       = Jesse L. Lasky
| based on       =  
| writer         = Olga Printzlau (scenario)
| starring       = Wallace Reid Ann Little
| music          =
| cinematography = Henry Kotani
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 mins.
| country        = United States Silent English intertitles
}}
 lost  silent romantic comedy film produced by Jesse Lasky for release through Paramount Pictures.  The film was directed by actor/director Donald Crisp and stars Wallace Reid and Ann Little. The film is based on a 1913 William A. Brady-produced play by John Frederick Ballard, Believe Me Xantippe, which on the Broadway stage had starred John Barrymore.  

==Cast==
*Wallace Reid as George MacFarland --(portrayed by John Barrymore in the play) Mary Young in the play)
*Ernest Joy as Thornton Brown
*Henry Woodward as Arthur Sole Noah Beery as Sheriff Kamman --(portrayed by Theodore Roberts in the play)
*James Cruze as Simp Calloway --(portrayed by Frank Campeau in the play)
*Winifred Greenwood as Violette
*Jim Farley as Detective Thorne Charles Ogle as Wren Wrigley
*Clarence Geldart as William

==See also==
*Xanthippe, wife of Socrates

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 