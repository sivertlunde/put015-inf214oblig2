John Jani Janardhan
 
 
{{Infobox film|
| name = John Jani Janardhan
| image = JohnJaniJanardhan.jpg
| caption =
| director = T. Rama Rao
| writer =
| starring = Rajinikanth Thiagarajan Poonam Dhillon Rati Agnihotri
| producer = A. Rao
| music =
| editor =
| released = 26 October 1984
| runtime =
| language = Hindi
| budget =
}}

John Jani Janardhan is a Hindi film is directed by T.Rama Rao. South Indian Superstar Rajinikanth played the main lead in a triple role. John being the father and johnny and janardhan being the two sons. It is a remake of the 1982 Tamil film Moondru Mugam which starred Rajinikanth and Raadhika.
It was successful at the box office and completed 100 days run at the box office, thus being the only triple role film to become hit at the box office.
==Plot==
Inpsector John A. Mendez is an honest and diligent Police Inspector, who lives a middle-class lifestyle with his wife, Cheryl, and a maidservant, Fatimabi. John bursts a gang that makes illicit liquor and holds them in a cell, bringing him into confrontation with their leader, Gajanand alias Gajju, who goes to threaten John, but is himself arrested and sentenced. When he is discharged he decides to avenge his humiliation by entrapping John and killing him. Cheryl gives birth to twin sons and passes away. A rich childless couple, Brijmohan and Girja Gupta adopt one of the twins and name him Janardhan, while the second is named Jani and lives with Fatimabi. Twenty five years later, Janardhan is an American-returned young man, in love with Madhu. On his birthday party he comes face to face with a man named Gopaldas, and his entire personality changes as he is taken over by the vengeful spirit of John. Subsequently, Gopaldas is killed, and Janardhan is arrested and tried in Court. Things seem to go in Janardhans favor, when the wily Gajju brings out a new witness - none other than Jani - who is now masquerading as John. Gajju hopes to kill Janardhan and thus prevent John from avenging his death - and nothing will stop Gajju to carry out his evil plan. 

==Cast==
* Rajinikanth as Inspector John A. Mendez/Janardhan B. Gupta/Johny
* Thiagarajan as Gopaldas
* Rati Agnihotri as Madhu
* Chandrashekhar as Madhus dad (as Chandrasekhar)
* Leena Das
* Poonam Dhillon as Cheryl J. Mendez Dulari as Fatimabi
* Utpal Dutt as Brijmohan Gupta
* Renu Joshi as Girja B. Gupta
* Kader Khan as Gajanand Gajju
* Alka Nupur as Asha / Cheryl

==Music==
{| border="4" cellpadding="6" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Tu Meri Jaan Hai 
| Shailendra Singh, Kavita Krishnamurthy
|-
| 2
| Chat Mangni Or Pat Shaadi 
| S P Balasubramaniam, S Janaki
|-
| 3
| Laundiya Satra Saal Ki 
| S Janaki, S P Balasubramaniam
|-
| 4
| Swamiji O Swamiji 
| Shailendra Singh, S Janaki
|-
| 5
| Aye Naujawan Iss Jawani
| Shailendra Singh, Kavita Krishnamurthy
|-
| 6
| Jab Tak Hai Jee .. Jee Bhar ke Pee 
| Suresh Wadkar
|}

==External links==
*  

 
 
 
 
 
 


 