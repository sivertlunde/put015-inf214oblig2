Shambo Shiva Shambo
{{Infobox film name           = Shambo Shiva Shambo image          = Shambo Shiva Shambo Poster.jpg director       = Samuthirakani producer       = Bellamkonda Suresh writer         = Samuthirakani starring  Abhinaya Sunil Sunil
|music          = Sundar C Babu cinematography = S. R. Kathir editing        = A. L. Ramesh studio         = Global Infotainment released       = January 14, 2010 runtime        = gross          = country        = India language       = Telugu
}}
 Abhinaya in the lead roles. The film released on 14 January 2010 to mixed responses from critics. Despite mixed reviews,the film performed well and was declared an Above Average grosser by Box Office India. It has also been dubbed in Hindi as Mera Kroadh. Its also remade in Kolkata as Run starring Rahul, Priyanka  

==Plot==
Karunakar (Ravi Teja), Malli (Allari Naresh) and Chandu (Siva Balaji) are close friends and each of them has individual goals. Karunakar is in love with his cousin (Priyamani) whose father wants him to get a government job first. Malli dreams of going to a foreign land as soon as he gets his passport, while Chandu aims to set up a Computer training centre and is in love with Karunakar’s sister Pavithra (Abhinaya (actress)|Abhinaya). The trio enjoy life hanging out together. Karunakar’s friend Santosh (Surya Teja), son of a politician (Roja (actress)|Roja), is in love with his family’s arch rivals daughter. The three friends come forward to unite these love birds against all odds. In the process, Karunakar loses job and misses his cousin, Malli loses his hearing, and Chandu has his leg amputated. 10 days later, the married lovers seek divorce. The friends, who sacrificed so much to unite them, now teach them a lesson.

==Cast==
  was selected as lead heroine marking her first collaboration with Ravi Teja.]]
* Ravi Teja as Karunakar AKA Karna
* Allari Naresh as Malli
* Siva Balaji as Chandu
* Priyamani as Karunakars cousin Abhinaya as Pavithra Roja as Home Minister
* Kota Srinivasa Rao
* Samuthirakani 
* Tanikella Bharani
* Surya Teja as Santosh
* Naina Das santosh s girlfriend
* Mukesh Rishi Chandra Mohan
* Ahuti Prasad Sudha
* Sunil
* Sasikumar (Guest Appearance)
* Rao Ramesh

==Development==

The film is an official remake of Samuthirakarnis own Tamil Film Naadodigal. This films title is based on Naadodigals hit song Shambho Shiva Shambho.

==Critical Reception==

The film received mixed to positive reviews from critics. Sify rated 4/5 and gave it the verdict "entertaining".  Supergoodmovies rated it 3/5 and said " All artists gave their best performances and though being his debut direction, Samuthirakarni deserves a hit" 

==Character map of remakes==
{| class="wikitable" style="width:70%;"
|-
! Naadodigal Tamil (2009) !! Shambo Shiva Shambo  Telugu (2010) !! Hudugaru  Kannada  (2011) !! Ithu Nammude Katha  Malayalam (2011) !! Run   Bengali (2011) !! Rangrezz   Hindi (2013)
|- Rahul Banerjee) || Rishi (Jackky Bhagnani)
|-
| Rangapandi (Bharani) || Malli (Allari Naresh) || Siddesh (Yogesh (actor)|Yogesh) || Kochumon (Abhishek) || Ghona (Arnab) || Pakkya (Vijay Verma)
|-
| Chandran (Vijay Vasanth) || Chandu (Siva Balaji) || Chandru (Srinagar Kitty) || Santhosh (Nishan K. P. Nanaiah|Nishan) || Ricky (Abhiraj) || Vinu (Amitosh Nagpal)
|-
| Nallammaal (Ananya (actress)|Ananya) || Karunakars cousin (Priyamani) || Gayathri (Radhika Pandit) || Kalyani (Ananya (actress)|Ananya) || Megha (Priyanka Sarkar) || Megha (Priya Anand)
|-
| Pavithra (Abhinaya (actress)|Abhinaya) || Pavithra (Abhinaya (actress)|Abhinaya) || Pavithra (Abhinaya (actress)|Abhinaya) || Ammu  (Nimisha Suresh) || Barsha (Pamela Mondal) ||
|}

==References==
 
* http://live.iencyclopedia.org/2010/01/shambo-shiva-shambo-movie-review-rating.html Movie Review & Rating
* http://www.indiaglitz.com/channels/telugu/review/11422.html
* http://www.telugucinema.com/c/publish/moviereviews/shamboshivashambo_review.php

==External links==
* 

 

 
 
 
 
 
 