Dollman (film)
{{multiple issues|
 
 
}}

For the Quality Comics character, see Doll Man. 
{{Infobox Film
| name         = Dollman
| image        = Dollma6.jpeg
| caption      = Poster artwork
| writer       = Charles Band David Pabian Chris Roghair
| starring     = Tim Thomerson Jackie Earle Haley Kamala Lopez Humberto Ortiz Nicholas Guest Judd Omen
| director     = Albert Pyun
| producer     = Cathy Gesualdo
| cinematography = George Mooradian
| narrator     = 
| music        = Anthony Riparetti
| editing      = Margaret-Anne Smith
| studio       = Full Moon Entertainment
| distributor  = Paramount Home Video
| released     = November 27, 1991
| runtime      =   79 minutes UK
| country      = United States
| language     = English
| budget       = 
| gross        = 
}}

Dollman is a 1991 science fiction action film starring Tim Thomerson as the space cop Brick Bardo, also known as Dollman after being reduced to 13 inches in height while on Earth, hence his nickname. Despite his size, Bardo is equipped with his Groger blaster, which is the most powerful handgun in the universe. The film also stars Jackie Earle Haley as Bardos human enemy, Braxton Red.
 Bad Channels (1992).

Dollman also had its own comic series, published by Eternity Comics, who also made comics for other Full Moon related films. A movie soundtrack still remains unavailable.

==Plot==
Brick Bardo is a Dirty Harry type cop on the planet Arturus, which is many light years from Earth. One day, a criminal runs into a laundry venue where lots of plump women and their kids happen to be, and he keeps them hostage while police officers are outside the premise. Bardo humorously enters to do his laundry and the crook is rather surprised by this defiant act. After saving the scared hostages by causing the fat women to faint and fall onto the bad guy by threatening to use his gun to blow a hole through them - "the most powerful handgun in the universe" proclaims a young boy - Bardo instead of being hailed a hero, is shouted at by the mayor, who knows of Bardos usually violent methods and Bardo was supposed to be suspended.

The film then cuts to Bardo in his apartment, where a news broadcast implies that Bardo is being framed for the deaths of the hostages in the laundry mat, as a set up. Just then, a man begins to fire at Bardo and tells him an old friend wishes to see him before using some device which puts Bardo to sleep. After Bardo wakes up on a desert plain, Sprug - Bardos greatest enemy - says that modern medicine did him wonders, unlike Bardos deceased family. Sprug tells Bardo he will shoot all his body parts off, just as Bardo did to Sprug before (Sprug only has his ugly head remaining on a floating jet after all their previous encounters). The two men with Sprug are about to kill Bardo using Bardos own custom Groger blaster. But Bardo uses a magnetic field in his hand to retrieve his gun and quickly shoots the two henchmen into bloody chunks. One of them is blasted in half, smokes a cigarette and asks what Bardo wants, but he just says nothing in a monotone manner before chasing after Sprug. Despite an advertised warning not to enter an energy band (some mysterious glowing lights in space), both Sprug and Bardo go through one such energy band, eventually reaching Earth where they are shrunken to several inches in height.

The movie then shows various crime acts occurring in The Bronx, New York City. It turns out Braxton Red and his gang run things around this part of the city and kill other outside gangsters who find their way into their neighborhood, to ensure control of this urban jungle. Next we are introduced to Debi, a young Hispanic single mother who wants the police to do more to protect her area. When Debi is going home, some goons that know about her meddling capture her and threaten her inside their van. As she fights back and tries to get away, the bad guys grab her and almost kill her using petrol, but Bardo appears and shoots one goon dead and another is blasted in the stomach, mortally wounding him. The injured goon and the third one who wasnt shot at all run off, leaving Debi to wonder if shes in shock due to Bardo being so noticeably tiny. Just as she takes Bardo and his ship away, Braxton Red and one of his gangsters spot Sprug and Sprug tells them that he has a powerful bomb they can use in exchange for them helping Sprug to repair his ship.

Bardo is taken to Debis house where he is introduced to her excited son, Kevin. Back at the hideout, Braxton kills the injured gangster in a fit of rage, after the other gangster explains that it was his idea to burn Debi, since Braxton has feelings for her even though she wont side with him. The gang member said that they were stopped by a little "Dollman" and Braxton doesnt believe that a man that small could hurt anybody, but he takes a lot of the gangsters with him and goes round to Debis place to demand she tell them where this alleged Dollman is. After they show up to look for the so-called Dollman, Bardo kills all the bewildered gangsters, save for Braxton, who runs away down the stairs, but gets seriously hurt. He makes it back to the hideout bleeding badly and Sprug knows this is Bardos handy work. As Braxton is dying, Sprug partially heals his injury after making some demands first, but Braxton recovers and squashes Sprug when he tells Braxton he is his new boss.

Although Braxton is still seriously hurt, he wants to kill Dollman more than ever. Bardo overhears some commotion going on outside the apartment the next day when Debi is coming home from work and being harassed, yet again. Bardo leaps out of a window and grabs onto the side of a car as it speeds off. Bardo follows the entire gang and they stop near to where the two spaceships landed, as they await Bardos arrival so they can have their revenge. Bardo enters an underground passage in order to surprise the gangsters who have been waiting for Bardo to show up for to rescue Debi. A huge shootout ensues with Bardo killing most of the gangsters by making their vehicles explode. Debi runs off but Braxton follows her, firing his machine gun at her. Feeling disrespected for keeping her alive for years, Braxton is about to shoot Debi after cornering her somewhere, but Bardo distracts him and shoots off one of his arms. Debi doesnt want Bardo to finish off Braxton even if he is evil and so Bardo agrees not to kill Braxton, just for her. Then Braxton uses the bomb Sprug had to attempt a last resort murder-suicide and Debi and Bardo quickly run for cover. Debi after seeing that Bardo is safe happily smiles and Bardo asks her if size counts, just in time to end his first adventure on Earth.

==Sequel== Bad Channels) and they help a normal sized police officer called Judith Gray to deal with the demonic toys once more, having already dealt with the nasty playthings before, in Demonic Toys.

==Releases==
Dollman made its DVD debut in the 2005 box set "The Dollman/Demonic Toys Box Set". The film was also featured in the limited edition box set "Full Moon Features: The Archive Collection", a 20th anniversary collection which featured 18 of Full Moons most popular films.

On November 9, 2010, Echo Bridge Home Entertainment will be releasing a triple feature set containing this film, Demonic Toys and Dollman vs. Demonic Toys.

Dollman was released on Blu-ray in the US on 17 December 2013 by Full Moon Entertainment.

==Merchandising==
* A four-issue comic book limited series printed by Eternity Comics. collectible cards.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 