El Capitán Pérez
  

{{Infobox film
| name           = El Capitán Pérez
| image          =
| image_size     =
| caption        =
| director       = Enrique Cahen Salaberry
| producer       = Sur
| writer         = Mauricio Rosenthal Pedro E. Pico
| narrator       =  Francisco de Paula Fanny Navarro
| music          = Alejandro Gutiérrez del Barrio
| cinematography =
| editor       =
| distributor    = 
| released       = February 7, 1946
| runtime        = 70 min.
| country        = Argentina Spanish
| budget         =
}}
 1946 Argentine black-and-white film. It was directed by Enrique Cahen Salaberry and written by Mauricio Rosenthal and Pedro E. Pico based upon the short story of Carlos Octavio Bunge. It premiered on February 7, 1946.  

==Cast==
* Olinda Bozán
* José Olarra
* Alberto Bello Francisco de Paula
* Fanny Navarro
* Benita Puértolas
* Patricio Azcárate
* Federico Mansilla
* Darío Cossier
* Carlos Belluci
* Domingo Mania
* Miguel Coiro

==References==
 

 
 
 
 
 


 