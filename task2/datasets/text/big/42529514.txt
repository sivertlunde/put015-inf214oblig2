CR No: 89
{{Infobox film
| name           = CR No: 89
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Sudevan
| producer       = Pace Trust
| writer         = Sudevan
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Rajesh Dass
| cinematography = Shaan Rahman Prathap Joseph Saadiq Thrithala
| editing        = Binoy Jayaraj
| studio         = Pace Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
CR No: 89 , also known as Crime No. 89, is a  . Retrieved April 19, 2014.  The film focuses on the moral dilemmas one faces in life. It won numerous awards including the NETPAC Award for Best Malayalam Film at the 18th International Film Festival of Kerala and the Kerala State Film Award for Best Film.  Sudevan won the G. Aravindan Award for Best Debut Director.  Actor Ashok Kumar won the Kerala State Film Award for Second Best Actor. The film was produced by Pace Trust and was funded mainly through contributions from various friends and admirers of Sudevans earlier films from around the world. 

==Cast==
* Asok Kumar
* Pradeep Kumar
* Santhosh Babu
* Achuthanandan
* Saradhi
* Kalyan
* Beena
* Vappukka
* Narayanan
* Mani
* Azeez
* Anil
* Subran

==Awards==
* NETPAC Award for Best Malayalam Film at the 18th International Film Festival of Kerala   CR No: 89
* Kerala State Film Award for Best Film   CR No: 89
* Kerala State Film Award for Second Best Actor   Ashok Kumar
* G. Aravindan Award for Best Debut Director   Sudevan Padmarajan Award for Cinema   CR No: 89 

==Notes==
 

==References==
 

==External links==
*  
*   (in Malayalam)

 

 
 
 

 