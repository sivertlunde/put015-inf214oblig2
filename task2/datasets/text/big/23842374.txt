Blood Beach
 
{{Infobox film
| name           = Blood Beach
| image          = Blood-beach.jpg
| caption        = 
| director       = Jeffrey Bloom
| producer       = Steven Nalevansky Run Run Shaw (uncredited)
| writer         = Jeffrey Bloom from a story by Jeffrey Bloom and Steven Nalevansky John Saxon 
| music          = Gil Melle
| cinematography = Steve Poster
| editing        = 
| distributor    = The Jerry Gross Organization Compass International Pictures 1981
| runtime        = 92 minutes
| country        = United States English
| budget         = 
}}
 cult classic.

==Plot==
 
In the opening scene, a person goes missing on Los Angeles, Californias Venice Beach; a middle-aged woman named Ruth who is walking her dog is suddenly pulled under the sand of the deserted beach by an unseen force. The womans screams for help are heard by Harry Caulder, a harbor patrol officer who is taking a swim in the ocean just offshore. Harry calls the police to Ruths disappearance but the two LAPD detectives, Royko and Piantadosi, claim that without a body there is not much they can do. The next day, Ruths estranged daughter, Catherine, arrives from San Francisco after Harry calls her to her mothers disappearance.

Meanwhile, there is a mysterious and crazed middle-aged homeless woman, known as Mrs. Selden, who resides in an abandoned section of the Santa Monica Amusement Pier who witnesses the attacks and disappearances throughout the film, but does not come forward and it is implied that she has seen the mysterious subterranean creature responsible for the attacks.

That night, while staying in Ruths house, Catherine hears Ruths dog barking on the beach near the location where Ruth disappeared. Catherine goes to investigate and when the dog stops barking, Catherine approaches and finds the dog dead with its head gone and only a small sinkhole nearby. Detectives Royko and Piantadosi as well as Harry are called to the scene where the police pathologist, Dr. Dimitrious, cannot make an accurate cause of death of the dog only to claim that the dogs head was not cut off but literally pulled off by an individual with "long strong fingers and very sharp fingernails". Royko and Piantadosi believe it to be the work of a serial killer due to other reports of other disappearances over the past few months.

The next morning, a teenage girl is buried in the sand at the beach when she begins screaming. Her friends pull her out of the sand only to see that she has shredded legs after being attacked by the unseen creature which tried to pull her under. The police, led by Captain Pearson, Royko and Piantadosis superior officer, begin an investigation by digging up various sections of the beach at night but they find nothing. The next morning, people go to the beach as usual and the local media covers the events where several people refer to the events and call the area "blood beach".

The following night, one of Harrys co-workers, named Hoagy, is closing up the harbor patrol office for the night when Hoagys girlfriend ventures under the pier to investigate a noise and is attacked by man who attempts to rape her. After being knocked to the ground by the girl, the would-be-rapist is attacked by the unseen creature who literally castrates the man in bloody fashion before the eyes of his horrified victim.

Another evening or two later, Marie, a French airline stewardess who is living with Harry, chases after her hat when it is blown by wind onto the beach and she too is grabbed by the unseen underground creature and pulled under the sand. The next morning, Harry sees Maries hat on the beach along with the same small sinkhole nearby where he recognizes it from being at the scene of Ruths disappearance and the death of her dog. Harry calls the police who dig up the area around the sinkhole where they find a disembodied human eyeball which belonged to Marie.

Harry confides to Catherine that the unknown creature may live somewhere and he takes it upon himself to venture to the abandoned section of the pier and finds an access tunnel leading underground which may have been a storage facility. Harry leaves the tunnel when he finds nothing, but doesnt notice movement in a collapsed section of a wall. Harry goes out with Catherine to a nightclub where they try to rekindle their romance they had before Catherine moved away. Meanwhile, a man with a metal detector is walking under the pier looking for metal objects when he is attacked by the still-unseen creature when it bursts through the sand and pulls him under. The mans wife, Mrs. Hench, reports him missing. The next day, the man is found by Royko and Piantadosi emerging from a sewer manhole in a Venice street after escaping from the creatures lair, but he is in a state of shock after being horribly mangled and cannot explain what happened to him.

Hoagy is the next victim when he chases after the homeless woman, Mrs. Selden, to the pier where the creature lives and while trying to talk to her to leave the area, he too is pulled under the sand by the underground creature while the homeless woman watches with stoic emotion.

Having been told by Harry about the service access tunnel, Catherine ventures herself to the underground facility under the pier to look around just when Harry brings Detective Piantadosi with him to investigate. It is here where they find all of the creatures partial eaten victims (16 in total) including Ruths severed head, parts of Maries body, and Hoagys freshly dead body. After Captain Pearson arrives with the police where they remove all of the disembodied bodies from the area, he orders the officers to use a backhoe and equipment to track the monster down. The local news and media quickly and widely cover the scenes at the beach at the pier, leading the police to try to figure out how to kill the beast as quickly and easily as possible. Person orders explosives be planted around the pier while motion detectors and heat-sensing cameras are put in place.

That evening, the creature is finally seen when it emerges from the sand and is caught on CCTV cameras roaring under the dilapidated pier (the huge creature resembles a worm-like Venus flytrap). Without hesitation, Detective Royko activates the detonator and the creature is blown to pieces with a massive charge of explosives. But Dr. Dimitrios points out that due to vague contact with the monster, they still do not know about its abilities, or where it came from, and since that it resembled a giant worm and that some worms have the capability to regenerate and heal, he argues about what will happen to "each piece".

The next morning, Harry leaves with Catherine to drive her home to San Francisco while the beach re-opens to the public now that the subterranean creature of "blood beach" is dead. But in the final scene over the end credits, as people begin crowding the beach again, new small sinkholes begin to appear unnoticed all over the beach sands... implying that Dr. Dimitrios was correct in his theory that the creature has the ability to reproduce itself from its severed pieces.

==Cast==
* David Huffman ...  Harry Caulder
* Marianna Hill ...  Catherine Hutton
* Burt Young ...  Sergeant Royko
* Otis Young ...  Lieutenant Piantadosi
* Lena Pousette ...  Marie John Saxon ...  Captain Pearson
* Darrell Fetty ...  Hoagy
* Stefan Gierasch ...  Dr. Dimitrios
* Eleanor Zee ...  Mrs. Selden
* Pamela McMyler ...  Mrs. Hench
* Harriet Medin ...  Ruth Hutton
* Mickey Fox ...  Moose
* Laura Burkett ...  Girl in Sand
* Marleta Giles ...  Girlfriend
* Jacqueline Randall ...  2nd Girl

==Release==
The film was given a limited release theatrically twice in the United States; first by The Jerry Gross Organization beginning in January 1981, and second by the Compass International Pictures in 1982.   

The film was released in the U.S. on VHS by Media Home Entertainment.   As of 2012, the film had only been officially released on DVD in Germany (26 April 2012).

Alamo Drafthouse Cinema re-released the film in a limited 35mm screening on March 14, 2015 as part of the "NY! Hudson Horror Show" event at the Alamo Drafthouse in Yonkers,  promoted by a new theatrical poster by artist Stephen Romano. 

== Reception ==
 
AllRovi|AllMovies review was negative, writing, "the potential for campy fun in this premise is defeated by a completely straight, plodding detective story". 

== Remake ==
In 2014, Allegra Pictures and Scatena & Rosner Films produced a remake of the film, titled The Sand. It was directed by Isaac Gabaeff from a screenplay by Alex Greenfield and Ben Powell.  The remake starred Nikki Leigh, Mitchel Musso, Dean Geyer and Jamie Kennedy. 

==See also==
* Tremors (film)|Tremors (1990), American western monster film about giant worm-like creatures living underground
* Sand Sharks (2011), another horror B movie about monsters lurking in the sand

==References==
 

==External links==
* 

 
 
 
 
 
 