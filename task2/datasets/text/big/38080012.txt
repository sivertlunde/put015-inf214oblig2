Bosko the Musketeer
{{Infobox Hollywood cartoon
| series = Looney Tunes (Bosko)
| image = 
| caption = 
| director = Hugh Harman
| story_artist = 
| animator = Rollin Hamilton Robert Stokes (credited as "Drawn by")
| voice_actor = Carman Maxwell (uncredited)
| musician = Frank Marsales Hugh Harman & Rudolf Ising Leon Schlesinger
| studio = Warner Bros. Cartoons|Harman-Ising Studios
| distributor = Warner Bros. The Vitaphone Corporation
| release_date = September 16, 1933 (USA)
| color_process = Black-and-white
| runtime = 7 minutes
| movie_language = English
| followed_by = Boskos Picture Show (1933)
}}
 animated short first film featuring Buddy (Looney Tunes)|Buddy, the second star of the series. It was, like most Looney Tunes of the time, directed by Hugh Harman; Frank Marsales was the musical director.

==Summary== dusting her home, as well as her pet fish. As she dusts a large, framed photograph of Bosko, she segues into a sultry impression of Mae West. Bosko walks in just as Honey is dusting a painting of "The Three Musketeers"; she rhetorically asks Boskos opinion on their grandeur, to which Bosko replies, "Shucks, thats nothin!" He pulls an umbrella out of a container by Honeys door, and begins to mime the moves of a skilled fencing|fencer; as Honey sings, we transition to an imaginary scene in which Bosko, now cheerfully brandishing a real foil (fencing)|foil, fights off a horde of enemies, first by swordplay, then finally by unleashing the tap of a nearby keg. He walks into a saloon and greets its patrons as the screen fades to a title, "The Three Musketeers".

The Three Musketeers stand about, singing of their identities; Bosko leaps upon a table to introduce them, "Athos (fictional character)|Athos, Amos and Andy|Amos, and Andy!" "One for all!" cries Bosko; "And all for one!" the musketeers reply. Bosko rushes off to an overturned table, on whose leg he dashes his foil, such that it coils about the wooden leg and forms a corkscrew; he then uncorks a bottle marked "New Deal United States presidential election, 1932|32" and pours it, as his companions continue to sing. As they finish, Bosko pours the drink down the gullet of Athos; it is tasted, apparently, by Amos; and the effects of the intoxicating beverage are felt by Andy. Those gathered in the drinking spot cheer, except for one patron, who exhibits his dislike of the performance, and proceeds to eat an entire roast chicken in but two bites: he then uncorks a beer bottle with the teeth of another patron seated at a table beside his own.
 hen and her chicks beneath the crinolines frame.

The fight ends when Bosko, pushed up against a fireplace, steps on a stoker, flipping a number of coals over his head and that of his foe, and into the seat of the villains pants: the cur runs off yelping in pain. We return to Bosko and Honey, in Honeys home, where she teasingly declares that she does not believe Boskos yarn!

==Modern references==
In the 1990 Tiny Toon Adventures episode "Fields of Honey", in which Babs Bunny discovers the all-but-forgotten cartoons starring Honey (whom she adopts as a mentor) and Bosko, Honeys catchphrase appears to be "Here I am, you lucky people!", which phrase she actually utters in Bosko the Musketeer.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 