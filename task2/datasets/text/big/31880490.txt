Jane Shore (1915 film)
{{Infobox film
| name           = Jane Shore
| image          = 
| image_size     = 
| caption        = 
| director       = Bert Haldane   F. Martin Thornton
| producer       = Will Barker Nicholas Rowe (play)   W.G. Wills (play)   Rowland Talbot
| narrator       = 
| starring       = Blanche Forsythe Roy Travers Robert Purdie  Thomas H. MacDonald
| music          = 
| cinematography = Will Barker
| editing        = 
| studio         = Barker Motion Photography
| distributor    = Walturdaw (UK)   Mutual Film (US)
| released       = 1915
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} silent historical Nicholas Rowe and is based on the life of Jane Shore, the mistress of Edward IV.

==Cast==
* Blanche Forsythe - Jane Winstead
* Roy Travers - Edward IV
* Robert Purdie - Matthew Shore
* Thomas H. MacDonald - Lord Hastings
* Dora De Winton - Margaret Queen Elizabeth
* Nelson Phillips - William Shore
* Rolf Leslie - Duke of Gloucester
* Tom Coventry - Master Winstead
* Rachel de Solla - Dame Winstead
* Frank Melrose - Garth the Bard
* Fred Pitt - Warwick

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 