En Purushan Kuzhandhai Maadhiri
{{Infobox film
| name           = En Purushan Kuzhandhai Maadhiri
| image          = 
| caption        =
| director       = S. P. Rajkumar
| producer       =
| story          = 
| screenplay     =  Livingston Devayani Devayani Vindhya Vindhya
| Deva
| cinematography = 
| editing        =
| studio         =
| distributor    = 
| released       = 2001
| country        = India
| budget         =
| runtime        = 
| language       = Tamil
}} Devayani and Sangeetha in lead roles.

==Plot==
Murugesan (Livingston) thinks the world of his uncles daughter Maheswari (Devayani). His love wins her over too and she agrees to wed him. Sinthamani (Vindhya) is a dancer who comes to town and Santhamurthy (Ponnambalam) has his eyes on her. Murugesan saves her from Santhamurthys clutches but in an inebriated condition, ends up sleeping with her the night before his wedding. His confession infuriates Maheswari initially but she eventually forgives him. But further problems crop up when Maheswari and Sinthamani become pregnant at the same time. Now Murugesan is unable to abandon her since he is responsible for her plight.

==Cast== Livingston as Murugesan Devayani as Maheswari
*Vindhya as Sinthamani
*Vadivelu as Angusaamy Ponnambalam as Santhamoorthy
*R. Sundarrajan

==Soundtrack==
*Aadiya Aattam&nbsp;— Deva
*Chithiraye&nbsp;— Hariharan, Anuradha Sriram
*Naalu Adi Aaru&nbsp;— Vadivelu
*Pattampoochi&nbsp;— Mano, Anuradha Sriram
*Vaazhavaikkum&nbsp;— Sabesh, Krishnaraj
*Vennila&nbsp;— Unnikrishnan, Sathya

==References==
 

 

 
 
 
 
 


 