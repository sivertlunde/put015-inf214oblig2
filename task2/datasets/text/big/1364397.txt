Tortilla Flat (film)
{{Infobox film
| name           = Tortilla Flat
| image          = Poster - Tortilla Flat 01.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Victor Fleming
| producer       = Sam Zimbalist
| screenplay     = John Lee Mahin Benjamin Glazer
| based on       =  
| starring       = Spencer Tracy Hedy Lamarr John Garfield Frank Morgan Akim Tamiroff
| music          = Frank Loesser Franz Waxman
| cinematography = Karl W. Freund
| editing        = James E. Newcom Robert Kern
| distributor    = Metro-Goldwyn-Mayer
| released       =   
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $1,201,000  . 
| gross = $2,611,000 
}} on the novel by John Steinbeck. It was directed by Victor Fleming. 

==Plot==
Danny (John Garfield) inherits two houses, so Pilon (Spencer Tracy) and his poor, idle friends move in. One of them, Pirate, (Frank Morgan) is saving money which Pilon endeavors to steal, until he discovers that it is being collected to purchase a golden candlestick which he intends to burn for St. Francis to honor the Pirates dead dog. One of the houses burns down, so Danny allows his friends to move into the other house with him, and in gratitude Pilon  tries to make life better for his friend. Things are fine at first until Dannys passion for a lovely girl (Hedy Lamarr) causes him to actually go to work.  A misunderstanding caused by Pilon about a vacuum cleaner causes Danny to lose it; he becomes drunk and a bit crazy. He almost dies in an accident in the cannery but through Pilons prayers, is restored to health. He then marries his sweetheart with the promise that he will become a fisherman now that Pilon has found the money to buy a boat.
The happy ending is quite different from the novels ending  in which Danny dies after a fall.

==Cast==
* Spencer Tracy as Pilon
* Hedy Lamarr as Dolores Ramirez
* John Garfield as Daniel Alvarez
* Frank Morgan as The Pirate
* Akim Tamiroff as Pablo
* Sheldon Leonard as Tito Ralph
* John Qualen as Jesus Maria Corcoran
* Donald Meek as Paul D. Cummings
* Connie Gilchrist as Mrs. Torelli
* Allen Jenkins as Big Joe Portagee
* Henry ONeill as Father Juan Ramon
* Mercedes Ruffino as Mrs. Marellis
* Nina Campana as Señora Teresina Cortez
* Arthur Space as Mr. Brown
* Betty Wells as Cesca
* Harry Burns as Torelli

==Reception==
According to MGM records the film earned $1,865,000 at the US and Canadian box office and $746,000 elsewhere, making the studio a profit of $542,000. 

===Critical response===
Film critic Bosley Crowther gave the film a positive review, writing that the film "is really a little idyll which turns its back on a workaday world...it is filled with solid humor and compassion—and that is pleasant, even for folks who have to work." 

==Awards==

===Nominations=== Best Supporting Actor - Frank Morgan

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 