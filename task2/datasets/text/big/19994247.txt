Yogi Bear (film)
{{Infobox film
| name           = Yogi Bear
| image          = Yogi Bear Poster.jpg
| alt            =  
| caption        = UK theatrical release poster
| director       = Eric Brevig
| producer       = Donald De Line Karen Rosenfelt Jeffrey Ventimilia
| based on       =  
| narrator       = Josh Robert Thompson
| starring       = Dan Aykroyd (voice) Justin Timberlake (voice) Anna Faris Tom Cavanagh T. J. Miller 
| music          = John Debney Peter James
| editing        = Kent Beyda De Line Pictures Rhythm and Hues Studios Warner Bros. Pictures
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = $80 million 
| gross          = $201,584,141   
}}

Yogi Bear is a 2010 American live-action/computer-animated comedy film adaptation of the 1961-62 animated television series The Yogi Bear Show directed by Eric Brevig.  The film stars Dan Aykroyd, Justin Timberlake, Anna Faris, Tom Cavanagh, T. J. Miller, Nate Corddry, and Andrew Daly. Distributed by Warner Bros. with Hanna-Barbera serving as a co-producer, the film tells the story of Yogi Bear as he tries to save his park from being logged. Principal photography began in November 2009. It was preceded by the cartoon short Rabid Rider, starring Wile E. Coyote and Road Runner.  Even though the movie received negative reviews from critics, it was still a box office success.

==Plot==
 
Yogi (Voiced by Dan Aykroyd) and Boo Boo (Voiced by Justin Timberlake) are two brown bears who have a penchant for stealing picnic baskets from visitors to Jellystone Park, while park rangers Smith (Tom Cavanagh) and Jones (T.J. Miller) are preventing them to do so. Meanwhile, Mayor R. Brown (Andrew Daly) realizes that Franklin City is facing bankruptcy due to profligate spending on his part. To solve it and fund his election campaign to be the next governor, the mayor decides to select Jellystone as a logging site, and Jellystone is prepared to be shut down.

To save the park, Ranger Smith holds a Centennial festival where he hopes to make a profit selling Season Passes. To sabotage the effort, Mayor Brown plays on Ranger Jones desire to be head ranger and promises him the position if the funds are not raised. Yogi and Boo Boo had promised Smith to stay out of sight during the festival, but Jones convinces them otherwise. They try to please the crowd with a waterskiing performance, but Yogi inadvertently sets his cape on fire, causing the fireworks Smith set up to be knocked over and ignited prematurely, launching them into the crowd, who flee in a panic. After Jellystone is shut down, Ranger Smith is forced to stay in Evergreen Park, a small urban enclave choked with litter and pollution, but not after he tells Yogi that Yogi is not as smart as he thinks he is. Now seeing that their home is in danger of being destroyed, Yogi and Boo Boo travel to Evergreen Park and tell Smith about this, where he places the clues that Mayor Brown had given him, that Brown and his chief of staff (Nate Corddry) are planning to cut down Jellystone and its trees to make money. They return to Jellystone, and along with Rachel plan to stop the sale of the park. They learn that Boo Boos pet turtle is a rare and endangered species, which means that, by law, the Park cannot be destroyed if the turtle is residing there. Ranger Jones then turns over Mayor Brown and teams up with Smith, Rachel, and the bears, after learning that he has been manipulated. Mayor Brown then learns about this and has his guards steal the turtle and confronts the group, stating that power is more convincing than the law.

After Mayor Brown leaves for his conference, Rachel reveals that she had installed a camera in Boo Boos bow tie, which recorded Mayor Browns confession. The turtle manages to escape from Browns Chief of Staff by using his frog-like tongue to pull itself through the car window and out into the park. Yogi and Boo Boo keep the guards distracted so Ranger Smith can upload the confession in the Jumbotron.  When the confession is played, the crowd riots, and Brown attempts to tell them that the turtle does not exist, but the turtle manages to get into the conference. The people then learn about the park having an endangered species. Police officers arrest Mayor Brown and his guards for his crime; his chief of staff attempts to escape, but is pinned down by Rachel and hes arrested as well. The park then becomes a great success. Jones loses his position as head ranger and Smith takes it back. But Jones still works there, handing out flyers about how Jellystone Park has a rare and endangered turtle, while Rachel and Ranger Smith admit their feelings for each other. However after they kiss, they start chasing after Yogi and Boo Boo, who are back to stealing picnic baskets once again.

==Cast==
* Dan Aykroyd as Yogi Bear, the picnic basket-stealing talking bear who lives in Jellystone Park.
* Justin Timberlake as Boo-Boo Bear, Yogis sidekick, and sometimes the voice of reason.
* Tom Cavanagh as Ranger Smith, the head ranger of Jellystone Park.
* Anna Faris as Rachel Johnson, a nature documentary filmmaker, and Ranger Smiths love interest. She is called "Miss Movie Lady" or "Miss Moviemaker Lady" by Yogi and Boo Boo. She has spent time with the animals that she makes a documentary on learning a gorillas rage and pinning down people like a snow leopard.
* T. J. Miller as Ranger Jones, a park ranger who is tricked by Mayor Brown into getting Jellystone shut down, making the excuse that Jones will be the head ranger of the park.      
* Andrew Daly as Mayor R. Brown, the main antagonist of the film. He is the Mayor of Franklin City who wants to shut Jellystone Park down so that he can make money for his city and become governor.
* Nate Corddry as the Chief of Staff, Mayor Browns assistant. 
* Josh Robert Thompson as the narrator.

==Production== CGI Yogi Journey to the Center of the Earth 3-D) when it was decided that the film would be produced as a 3-D project. Filming took place in the Lake Whakamaru Reserve, Waikato, New Zealand, as it was winter in the northern hemisphere and to wait for summer would put the production end time to be 6 months longer than if in southern hemisphere.
 Ed Norton character on The Honeymooners was said to be Yogis inspiration;     his voice mannerisms broadly mimic Carney as Norton. Sennett, p. 59.  Norton, in turn, received influence from Borscht Belt and comedians of vaudeville.  Dan Aykroyd, the voice actor of Yogi Bear, stated that he is trying to evoke the influences that shaped the original Yogi Bears voice. Aykroyd said, "Its about hitting certain notes, going back to those old Lower East Side rhythms, the Catskills, New Jersey|Jersey, Upstate New York. Its the Yiddish language, essentially, being spoken in English language|English. Its the setup, delivery, punch that sitcoms live on today. Thats where the origin of American humor is."  Aykroyd has stated that he grew up watching Yogi Bear on the long, cold, dark afternoons in his native Ottawa: "As a kid growing up in Ottawa, Canada, where the sky turns dark in the winter at about 3:30, Yogi Bear was my fire, my hearth, when I would come home. I would immediately turn on the TV while I thawed out."  , interview with Dan Aykroyd, Dec. 17, 2010 

Justin Timberlake came in with a prepared Boo-Boo voice; when he was learning to sing when he was younger, he imitated various cartoon characters. Eric Brevig said that he intended to make a film that did not want parents who remembered watching Yogi Bear cartoons to feel marginalized and displaced by the contemporary rendition of Yogi Bear. 

The films first trailer was released online on July 28, 2010. It was also attached with   and  , and a third trailer premiered with  .
 Grown Ups.     

  (1994), its prequel   (2004).

==Reception==

===Critical response===
Yogi Bear received negative reviews from film critics. Based on 101 reviews collected by Rotten Tomatoes, the film received a 13% "Rotten" approval rating for critics, with an average score of 3.6/10. The sites consensus stated "Yogi Bear s 3D effects and all-star voice cast are cold comfort for its aggressively mediocre screenplay."    The compiled score on Metacritic is 35%. Alvin and the Chipmunks. The critics also praised Dan Aykroyd and Justin Timberlakes performances, along the 3D effects. Common Sense Media gave the film one star, saying "Dumber-than-average family comedy wont even impress kids." IGN gave the film two stars, and summed up their review by saying "Of course, Yogi Bear is meant as a kids movie. And one supposes that it works on that level (the little ones at the press screening I attended seemed mildly amused). But we learned long ago that kids movies can operate on more than one level, and thats not something that director Eric Brevig (Journey to the Center of the Earth 3-D) or his screenwriters are interested in. The result is a movie thats dumber than the average bear. Though at least it has a pee joke in it."

===Box office===
Yogi Bear debuted at the American and Canadian box office at #2 behind  , with an under-performing $16,411,322;  compared to Tron Legacy s $44,026,211. The opening weekend was lower than Warner Bros. expected, but executives believed that the film would hold well throughout the holiday season.  The film earned $28 million in its first seven days, becoming Warner Bros. top-grossing start for a family film for that year. In its second weekend, the film fell 53% to $7.8 million, falling to fifth place.

This number was a much harder fall than what Warner Bros. was hoping for, but it blamed the drop on Christmas Eve landing on a Friday plus the big snowstorm in the eastern United States during that period. On Monday, that blame seemed justified, as the film jumped up 11% from Sunday to $3.6 million, which was altogether a 33% jump from its previous Monday. In its third weekend, the film jumped up 66% to $13 million and ranking fourth. The next weekend, it dropped 46% and ranked eighth with $6.8 million. Over the Martin Luther King, Jr. Weekend, it was able to pick up $7.4 million for the four-day, jumping up 12% from the previous weekend.

The film has picked up $100,246,011 in the U.S. and Canada and also has a worldwide total of $201,584,141. Against an $80 million budget, the film has become a surprise box office hit.

===Awards and nominations===
2011 Teen Choice Awards
*Choice Movie: Animation Voice - Justin Timberlake (nominated)

==Home media==
Warner Home Video released the film on Blu-ray Disc|Blu-ray/DVD on March 22, 2011 in four versions:

* DVD (single-disc)  
* Blu-ray (single-disc)
* Blu-ray/DVD/Digital Copy
* Blu-ray 3D/Blu-ray/DVD/Digital Copy
==Sequel==
A part two is in the works, and Jay Chandrasekhar has been chosen to direct.  

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 