Gaayathri
 
{{Infobox film|
| name = Gaayathri
| image =
| caption =
| director = R.Pattabhiraman Sujatha
| starring = Rajinikanth Jaishankar Sridevi Rajasulochana Tiger Prabhakar
| producer =
| music = Ilaiyaraaja
| editor =
| released = 7 October 1977
| runtime = Tamil
| budget =
}}

Gaayathri is a Tamil film is directed by R.Pattabhiraman. It has Rajinikanth and Sridevi in the lead roles. Once again Rajini plays a cruel husband of Sridevi. Jaishankar plays a guest role in this movie. Singer Sujatha Mohan made her debut as a singer through this film at the age of 12. The movie was based on the story "Gayathri" written by Sujatha Rangarajan.

==Plot==
Rajinikanth is a rich man who lives with his sister (Rajasulochana) and a young house maid in Chennai. Her sister arranges a marriage for Rajinikanth with Sridevi, who lives in Trichy. Soon after they marry, Sridevi moves to Rajinis house and gets shocked to find the original face of Rajini and his sister (Rajasulochana). Rajini is a blue-film producer who records the bedroom scenes of him with his wife (Sridevi), without her knowledge and sells it in black market. Sridevi, initially believes Rajini a lot but later discovers that Rajini was already married to a woman who has become mad now. Jaishankar comes to know about this and tries to free Sridevi from the hands of Rajini. But, unfortunately Sridevi dies in the end without knowing the real face of Rajini.

==Cast==

*Rajinikanth
*Jaishankar
*Sridevi
*Rajasulochana
*Tiger Prabhakar

 
 
 
 
 
 
 
 


 