Unconquered (1989 film)
{{Infobox television film
| name         = Unconquered
| image        = UnconqueredMoviePoster.jpg 
| caption      = DVD cover 
| genre        = Drama
| distributor  = CBS
| director     = Dick Lowry
| writer       = Martin Chitwood (story) Pat Conroy (teleplay) 
| producer     = Martin Chitwood (co-producer) Derek Kavanagh (supervising producer) Dick Lowry (producer) Dean Silvers (co-producer)
| starring     = Dermot Mulroney Peter Coyote
| cinematography = Robert M. Baldwin
| editing      = Byron Buzz Brandt Anita Brandt-Burgoyne 
| music        = Arthur B. Rubinstein
| studio       = Alexandra Film Productions CBS Entertainment Production Dick Lowry Productions Double Helix Films
| network      = CBS
| released     = January 15, 1989
| country      =  
| language     = English
}} Richmond Flowers, Jr. 

==Plot==
In 1962 in Montgomery, Alabama, as the fires of racial hatred sweep across the State, one family stands alone, about to face the greatest test of courage and loyalty. State Attorney Richmond Flowers is the only man prepared to stand up and fight against the horrifying injustices destroying his town. But his determination may prove to be the destruction of his family. His son, Rich, is desperate to prove himself against all odds, yet has to fight to simply survive. As the family suffers increasingly brutal attacks at the hands of the enraged community, they must find a strength so great that it can conquer over the hatred that surrounds them.  

==Cast==
* Peter Coyote as Richmond Flowers Sr.
* Dermot Mulroney as Richmond Flowers Jr.
* Tess Harper as Mrs. Mary Flowers
* Jenny Robertson as Cindy
* Frank Whaley as Arnie
* Bob Gunton as Gov. George Wallace

==References==
 

==External links==
*  

 
 
 
 
 
 