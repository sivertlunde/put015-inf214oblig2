The Silent House (1929 film)
{{Infobox film
| name           = The Silent House
| image          =
| caption        =
| director       = Walter Forde 
| producer       = Archibald Nettlefold
| writer         = John G. Brandon (play)   George Pickett   H. Fowler Mear
| starring       = Mabel Poulton  Gibb McLaughlin   Arthur Pusey   Gerald Rawlinson
| music          = 
| cinematography = Geoffrey Faithfull   A. Randall Terraneau
| editing        = Walter Forde
| studio         = Nettlefold Films
| distributor    = Butchers Film Service
| released       = January 1929
| runtime        = 9,376 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent mystery film directed by Walter Forde and starring Mabel Poulton, Gibb McLaughlin and Arthur Pusey. It was made at the Nettlefold Studios in Walton-on-Thames. The film was based on a recent stage hit, but was not a success at the box office. 

==Cast==
* Mabel Poulton as TMala 
* Gibb McLaughlin as Chang Fu 
* Arthur Pusey as George Winsford 
* Gerald Rawlinson as Capt. Barty 
* Albert Brouett as Peroda 
* Rex Maurice as  Legarde 
* Raston Medrora as Mateo 
* Frank Perfitt as Richard Winsford 
* Arthur Stratton as Benson 
* Kiyoshi Takase as Ho-Fang

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 