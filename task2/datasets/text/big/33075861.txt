Bluebeard's 8th Wife
{{infobox film
| name           = Bluebeards 8th Wife
| image          = Bluebeards 8th Wife.jpg
| imagesize      =
| caption        = 1923 theatrical poster
| director       = Sam Wood
| based on       =  
| producer       = Adolph Zukor Jesse Lasky
| writer         = Charlton Andrews (adaptation)
| screenplay     = Sada Cowan
| starring       = Gloria Swanson
| music          =
| cinematography = Alfred Gilks
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 mins.
| country        = United States
| language       = Silent English intertitles
}}
 silent romantic comedy film produced by Famous Players-Lasky and distributed by Paramount Pictures. It was directed by Sam Wood and stars Gloria Swanson. The film is based on the French play La huitième femme de Barbe-Bleue, by Alfred Savoir based on the Bluebeard tales of the 15th century.  The play ran on Broadway in 1921 starring Ina Claire in the Swanson role. 
 in 1938 starring Claudette Colbert. 

==Cast==
*Gloria Swanson - Mona deBriac
*Huntley Gordon - John Brandon Charles Greene - Robert
*Liane Salvor - Lucienne
*Paul Weigel - Marquis DeBriac
*Frank Butler - Lord Henry Seville
*Robert Agnew - Albert deMarceau
*Irene Dalton - Alice George
*Majel Coleman - Unknown role
*Thais Valdemar - Unknown role Jacqueline Wells - Uncredited role

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 