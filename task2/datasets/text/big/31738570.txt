All I Wanna Do (2011 film)
 
{{Infobox film
| name           = All I Wanna Do
| image          = 
| caption        = 
| director       = Michelle Medina
| producer       = Lamia Chraibi
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Don Bigg Najat Bindnou Michelle Medina Ayoub Rouguiyag
| music          = David Benezra Thierry Loshouarn
| cinematography = Hamza Akhmiss Mickaël Clouet
| editing        = Mohcine Nejmi Adnane Saali
| studio         = 
| distributor    = 
| released       =  
| runtime        = 59 minutes
| country        = Morocco
| language       = Arabic
| budget         = 
| gross          = 
}}

All I Wanna Do is an 2011 Moroccan documentary film produced by La Prod and directed by Michelle Medina starring Omar Sayed Nass El Ghiwane and Don Bigg, Simohamed, a parking guard and his 17 year old son, Ayoub. The documentary was shot on location in Casablanca, Morocco from 2009 to 2010 and has won a documentary prize at the 2011 Athens International Film Festival and was nominated at the 2011 Aljazeera International Documentary Film Festival for "Best Film" and "Best Director" at the 2011 World Music and Independent Film Festival in Washington, DC.

==Plot==
The film follows the dreams of 48-year-old Simohamed who works as a parking guard and his 17-year-old son Ayoub, who acts in Hollywood films such as Charlie Wilsons War and with Hollywood actors like Brad Pitt. When Ayoubs dreams of going to Hollywood are dashed, he turns to music and forms a hip hop group with his father. Like fish out of water, the duo set out to meet their heroes, enter studios and radio stations for the first time in an adventure through the music industry of Casablanca.

== External links ==
*  

 
 
 
 
 

 
 