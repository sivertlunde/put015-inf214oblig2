A Tight Spot
{{Infobox film
| name           = A Tight Spot
| image          = A Tight Spot.jpg
| caption        = 
| director       = Mića Milošević
| producer       = Dragoljub Vojnov
| writer         = Siniša Pavić Ljiljana Pavić
| narrator       =  Nikola Simić Ružica Sokić Milan Gutović Irfan Mensur Lepa Brena
| music          = Vojislav Kostić Riblja Čorba
| cinematography = Aleksandar Petković
| editing        = Branka Čeperac
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Yugoslavia
| language       = Serbian
| budget         = 
}} Serbian comedy film directed by Mića Milošević and written by Siniša Pavić along with Ljiljana Pavić. The film achieved enormous popularity throughout SFR Yugoslavia. The films success spawned three sequels by the end of the decade.

It is centered on Dimitrije Mita Pantić, a bewildered clerk in his mid fifties working in a crusty state-owned company under corrupt boss Srećko Šojić. Constantly frustrated and stressed out, Pantićs home situation isnt much better either. Living in a cramped apartment with his wife Sida and their three grown children Branko, Mira, and Aca, each with their own problems, Pantić also has to endure his cranky mother and a state-assigned subtenant Suzana under the same roof.

==Plot==
  Nikola Simić) is still only a junior clerk in his company. Another typical workday for him is starting at 6 a.m. as frustration awaits at every turn from the moment he gets up. Trying to get ready to go to work, he can barely get a turn to use the bathroom in the crowded apartment. Other members of the household are not without their frustrations either, meaning that nagging and shouting are a staple of their home life at any time of day.

Pantićs spouse Sida ( ), an eternal university student majoring in astronomy, has a gripe about not having enough money for books and kits, which in his opinion prevents him from finally graduating. The middle child, daughter Mira (Danica Maksimović), has a degree in law but can not find a job despite trying for years - shes also unhappy about lack of funds in the family because she thinks the ability to dress more attractively or to outright bribe would help her finally land a job. Finally, the youngest child Aca (Aleksandar Todorović), who is 16, has problems with discipline in high school and wants his father to buy him new textbooks and also a motorbike. And if all of that is not enough, Pantićs household also features his cranky mother (Rahela Ferari) and a subtenant Suzana (Jelica Sretenović) who is assigned there by the state due to housing shortage and given a room that takes up 9.4 m 2  of their apartment.

All the morning commotion at home leads to Pantić often being late for work where more frustration awaits. On this particular occasion its his crooked boss Srećko Šojić (Milan Gutović) barging into his office, ripping up reports and demanding Pantić has them re-typed because theyre not legible enough to read. That leads Pantić back to the keyboarding department where a new young typist Melita Sandić (Nada Vojinović), who typed up the messy reports in the first place, seems more interested in chatting and flirting over the phone than doing her job. Pantić loses his patience and has her report to the director for incompetence.
 Japanese culture and way of life, the professor has a low tolerance for Acas smart-alecky retorts and demands to speak to his father.

Back at Mitas work, its time to collect monthly paychecks, but he is horrified to find out his pay has been docked due to his tardiness as part of the companys new initiative to maximize productivity. Hopping mad, he storms back to his office, but is soon approached by an unknown man who introduces himself as Oliver Nedeljković (Vladan Živković) and proceeds to offer Pantić a bribe in return for his approval when it comes to decision on new hiring. Though short on money, Pantić refuses the payoff and reports the corruption attempt to director Šoić.
 football broadcasts league fixtures. Sloboda vs. Vardar match deciding if he wins the grand prize. Japanac meanwhile can not get a word in edgewise and as he is getting ready to leave, bumps into the subtenant Suzana who invites him into her room where they hit it off immediately. Back in his part of the apartment, Pantić collapses on the floor upon finding out the game ended in a draw, which means he missed the grand prize.

Japanac and Pantić eventually do meet to talk in Japanacs apartment, but this time the professor is more interested in discussing Pantićs subtenant than his son. Japanac expresses his desire to marry Suzana, which Pantić sees as a way to finally be rid of her.

Pantić cashes in his 12 correct guesses ticket for which he gets 7 million Yugoslav dinars. Coming into work to  he gets mad  when he finds out Nedeljković, the man who attempted to bribe him got the job and goes to confront Šojić only to find out that  Melita has advanced to position of Šojićs personal typist, much to Pantićs annoyance and frustration after which he insults Šojić and leaves.

Back home with 7 million in cash in his bag, he wakes up the next morning to usual morning routine of family nagging and complaining about money. Sick and tired of listening to their complaints he reaches into his bag and starts throwing bills in the air. Then on the way to the bathroom he runs into Japanac who informs him that he married Suzana and moved into her room, as the house in which he met with Pantić was his friends house. Japanac puts a bathroom schedule and enters the bathroom. After walking silently an angry Pantić screams, charges at the bathroom door and breaks it as the film ends.

==Production== rock band Riblja Čorba. Buvlja pijaca was not originally released as A Tight Spot soundtrack album.

The character of Srećko Šojić already appeared a year earlier as supporting character in 1981s Laf u srcu also written by Siniša Pavić and directed by Mića Milošević.

==Cast== Nikola Simić as Dimitrije "Mita" Pantić
*Milan Gutović as Srećko Šojić
*Ružica Sokić as Persida "Sida" Pantić
*Irfan Mensur as Japanac, high school English professor
*Jelica Sretenović as Suzana, the subtenant
*Gojko Baletić as Branko Pantić
*Danica Maksimović as Mira Pantić
*Aleksandar Todorović as Aleksandar "Aca" Pantić
*Rahela Ferari as Mitas mother
*Nada Vojinović as Melita Sandić, Šojićs personal typist
*Vladan Živković as Oliver Nedeljković
*Lepa Brena as Herself
*Mirjana Joković as the girl with the walkman in Acas English class (uncredited)

==External links==
*  
*  

 
 
 
 
 
 