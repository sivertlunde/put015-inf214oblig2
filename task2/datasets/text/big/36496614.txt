To the Hilt (film)
 
{{Infobox film
| name           = To the Hilt
| image          = To the hilt (2014) Poster.jpg
| alt            = 
| caption        = 
| director       = Stole Popov
| producer       = Jordanco Cevrevski Milivoje Gorgevic
| writer         = Goran Stefanovski co-writer Stole Popov
| starring       = Inti Šraj Sashko Kocev Martin Jordanoski Toni Mihajlovski Iskra Veterova Senko Velinov Miki Manojlovich Nikola Kojo Nikola Ristanovski Dragan Spasov–Dac Gorast Cvetkovski Denis Abdula Vlado Jovanovski Ana Kostovska Kamka Tocinvski Biljana Jovanovska Goran Trifunovski Nenad Nacev Bojan Velevski Adem Karaga Jordan Simeonov Ramizi Hajrulah Jovica Mihajlovski
| music          = Duke Bojadziev
| cinematography = Apostol Trpeski
| editing        = Atanas Georgiev Blagoja Nedelkovski
| design         = Milenko Jeremic
| studio         = 
| distributor    = 
| released       =  
| runtime        = 165 minutes
| country        = Macedonia
| language       = Macedonian French English Turkish
| budget         = 
| gross          = 
}} Macedonian action adventure western western style Ottoman ruled Demir Hisar, Štip and Prilep.  It premiered in Skopje, in October 2014
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.      

==Plot==

Macedonia, a small country in the heart of the Balkans, is five hundred years under the Turkish yoke. The action takes place in the bloody post-revolutionary period after 1903, more precisely in the first decade of the 20th century.  
"To the Hilt" is a love quadrangle between a cruel though romantic rebel, a merciless naturalized Turkish officer, an educated wealthy father’s son and a lucid, spoiled but avantgarde European woman who wants to have three of them wrapped around her little finger. The film unveils a harsh though romantic story in which the eternal Macedonian cause for own identity and independence is seen from the angle of illusion and relativity of freedom, justice, love, happiness, sacrifice, frauds and treason.  
All experienced with catharsis that rises up to the hilt. To be more precise, all of the action is marked with the typical Balkans paradox lifted up to the level of the undisputed philosophical maxima: "Fuck life if you are not ready to die for it". 
  This is a film about the dialectical encounter between the myths and the anti-myths, a film in which legends and storytelling face the ongoing reality of the cruel life through Macedonian history full of super historical absurd. It’s an Anti Western, a counterpart of the traditional American Western or something we can freely call Macedonian Eastern. This film breaks down the barriers of the space where western and eastern civilization have intertwined for centuries. 

==Cast==
* Inti Sraj - Tereza
* Sasko Kocev - Krsto	
* Martin Jordanoski - Filip	
* Toni Mihajlovski - Muzafer	
* Iskra Veterova - Ana	
* Senko Velinov - Boro	
* Miki Manojlovic - Bogdan	
* Nikola Kojo - Agent	
* Nikola Ristanovski - Cvetko	
* Dragan Spasov – Dac - Shishe	
* Gorast Cvetkovski - Kaval


==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Macedonian submissions for the Academy Award for Best Foreign Language Film

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 