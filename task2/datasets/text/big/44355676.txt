Insecure (film)
{{Infobox film
| name           = Insecure
| image          = 
| caption        =
| director       = Marianne Tardieu 
| producer       = Christophe Delsaux Céline Maugis 
| writer         = Nadine Lamari Marianne Tardieu 
| starring       = Reda Kateb  Adèle Exarchopoulos Rashid Debbouze
| music          = Sayem
| cinematography = Jordane Chouzenoux 		 
| editing        = Thomas Marchand 
| studio         = La Vie est Belle Films Associés Oriflamme Films
| distributor    = Rézo Films 
| released       =  
| runtime        = 83 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Insecure ( ) is a 2014 French drama film directed by Marianne Tardieu. It was screened in the ACID sidebar section at the 2014 Cannes Film Festival. 

== Cast ==
* Reda Kateb as Chérif Arezki 
* Adèle Exarchopoulos as Jenny 
* Rashid Debbouze as Dedah 
* Moussa Mansaly as Abdou
* Serge Renko as Claude Gilles 
* Alexis Loret as Lenquêteur 
* Hassan NDibona as Djim 
* Mohamed Mouloudi as Walid 
* Guillaume Verdier as Fred 
* Medanie Boussaïd as  Sami 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 