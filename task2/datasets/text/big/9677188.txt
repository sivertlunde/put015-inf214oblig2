El Profesor Hippie
{{Infobox film
| name           = El profesor hippie
| image          = Profesorhippie.jpg
| image_size     =
| caption        = 
| director       = Fernando Ayala  Héctor Olivera
| producer       = Fernando Ayala  Héctor Olivera
| writer         = Abel Santacruz
| narrator       = 
| starring       = Luis Sandrini  Roberto Escalada Soledad Silveyra  
| music          = Jorge López Ruiz
| cinematography = Victor Hugo Caula
| editing        = Oscar Montauti 
| distributor    = Aries Cinematográfica Argentina
| released       = July 31, 1969
| runtime        = 
| country        = Argentina
| language       = Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1969  Argentine musical musical comedy film directed by Fernando Ayala and written by Abel Santacruz which Ayala produced with Héctor Olivera. The film starred Luis Sandrini, Roberto Escalada and Soledad Silveyra. The film premiered on July 31, 1969 in Buenos Aires.

==Plot Outline==
Luis Sandrini plays a college professor who, because of his freethinking attitudes and concerns for moral questions over matters of business, has more in common with his students than his colleagues.

==Cast==
*Luis Sandrini ...  Profesor Héctor Tito Montesano 
*Roberto Escalada   
*Soledad Silveyra ...  Nélida Echeverría 
*Oscar Orlegui   
*Alita Román   
*Eduardo Muñoz   
*Perla Santalla   
*Zelmar Gueñol   
*Carlos López Monet   
*Flora Steinberg   
*David Tonelli   
*Pablo Alarcón ...  Demateis 
*Isidro Fernán Valdez   
*Marcia Bell

==External links==
*  

 
 
 
 
 
 


 
 