Khushi Khushiyagi
{{Infobox film
| name           = Khushi Khushiyagi
| image          = 
| caption        = 
| director       = Yogi G. Raj
| producer       = A. Hariprasad Rao   A. Prasad Chaudhary
| writer         = Vijay Kumar Konda
| dialogues      = Mahesh Rao Ganesh  Amulya  Nandini Rai
| music          = Anoop Rubens
| cinematography = Srisha Kuduvalli
| editing        = Jo Ni Harsha
| studio         = HPR Entertainment Pvt. Ltd.
| distributor    = M. N. Kumar
| released       =  
| runtime        = 151 min
| country        = India
| language       = Kannada
| budget         =   
| gross          = 
}}
 Kannada romantic Ganesh and Telugu blockbuster film Gunde Jaari Gallanthayyinde (2013) which was directed by Vijay Kumar Konda.  The film opened on the screens on 1 January 2015. 

==Cast== Ganesh as Raj
* Amulya as Nandini
* Nandini Rai as Priya
* Sadhu Kokila
* Achyuth Kumar

==Production==

===Development===
Yogi G. Raj, a long time associate of Ganesh, had expressed to him, his intent in becoming an independent director after working with the likes of Preetham Gubbi and Harsha (director)|Harsha. The title for the film was initially Savira Janmaku, then got subsequently changed to Crazy Boy, Shokilal, Kushi Kushiyali before being finalized as Kushi Kushiyagi 
.  The director chose a remake subject as his first directorial venture and obtained the official remake rights from the producers of the original Telugu film.

===Casting===
Before actor Ganesh was finalized, the makers tried to rope in Puneeth Rajkumar for the lead role who could not accommodate his dates. Actress Kajal Agarwal was approached to play the character which Nithya Menen originally played.  However, the director roped in actress Amulya, who successfully paired with Ganesh in Cheluvina Chittara and Shravani Subramanya, as the final lead actress. Telugu actress Nandini Rai was roped in to play the second lead of a fashion designer which was played by Isha Talwar in the original Telugu version. 

==Soundtrack==
{{Infobox album  
| Name        = Khushi Khushiyagi
| Type        = Soundtrack
| Artist      = Anoop Rubens
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    = Feature film soundtrack
| Length      = 20:36
| Label       = Lahari Music
}}

Music composer Anoop Rubens, who scored for the original version, was retained as the music director for the remake version as well.  The album consists of five tracks.  The lyrics for two tracks were written by Jayant Kaikini, two by K. Kalyan and one by V. Nagendra Prasad.

{{Track listing
| total_length   = 20:36
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Arey Arey Kaviraj
| extra1        = Anoop Rubens, Anuradha Bhat
| length1       = 4:05
| title2        = Atiyaitu
| lyrics2 	= Jayanth Kaikini
| extra2        = Ankit Tiwari, Shreya Ghoshal
| length2       = 4:11
| title3        = Neene Neene
| lyrics3 	= Jayanth Kaikini
| extra3        = Adnan Sami
| length3       = 4:12
| title4        = Neenyare Neenyare Kaviraj
| extra4        = Santosh, Sharmila Katke
| length4       = 4:02
| title5        = Rimbola Rimbola
| lyrics5 	= V. Nagendra Prasad
| extra5        = Divya, Santosh
| length5       = 4:06
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 