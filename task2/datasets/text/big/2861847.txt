Mad Dog and Glory
{{Infobox film
 | name               = Mad Dog and Glory
 | image              = Mad dog and glory.jpg
 | caption            = Theatrical release poster Richard Price
 | starring           = Robert De Niro Uma Thurman Bill Murray
 | director           = John McNaughton
 | producer           = Steven A. Jones Barbara De Fina Martin Scorsese
 | music              = Elmer Bernstein Terje Rypdal
 | cinematography     = Robby Müller Craig McKay
 | distributor        = Universal Pictures
 | released           = March 5, 1993
 | runtime            = 97 minutes
 | country            = United States
 | language           = English
 | budget             = $19 million
 | gross              = $10,688,490 (USA) 
}} American comedy-drama|comedy-drama film directed by John McNaughton and starring Robert De Niro, Uma Thurman, and Bill Murray.

==Plot==
Wayne Dobie (De Niro) is a meek Chicago Police Department crime scene photographer who has spent years on the job without ever drawing his gun; his colleagues jokingly call him "Mad Dog". Mad Dog saves the life of mob boss Frank Milo (Murray) during a hold-up in a convenience store. Milo offers Mad Dog a gift in return: for one week, he will have the "personal services" of Glory (Thurman), a young woman who works as a bartender at Milos club.

Mad Dog learns that Glory is trying to pay off a personal debt and wants nothing to do with Milo beyond that. After an awkward start, they fall in love. Mad Dog wants her to move into his apartment, but Milo has no intention of losing Glory permanently. Milo says that Mad Dog has to pay $40,000 to give Glory her freedom, and sends one of his thugs to back up this threat. Mad Dogs partner, Mike (Caruso), fights the thug on Mad Dogs behalf.

Mad Dog does his best to get the money but falls short by $12,500. Knowing that Mike can no longer fight his battles, he works up the courage to fight for Glory himself, and ends up fighting with Milo in the street. Fed up, Milo washes his hands of Mad Dog and lets Glory go with no strings attached.

==Cast==
* Robert De Niro as Wayne Mad Dog Dobie
* Uma Thurman as Glory
* Bill Murray as Frank Milo
* David Caruso as Mike Mike Starr as Harold
* Tom Towles as Andrew the Beater
* Kathy Baker as Lee
* Derek Annunciation as Shooter
* Doug Hara as Driver
* Evan Lionel as Dealer in Car
* Anthony Cannata as Pavletz
* J. J. Johnston as Shanlon
* Guy Van Swearingen as Cop
* Jack Wallace as Tommy the Bartender
* Richard Belzer as M.C. / Comic

==Production==
According to a profile of producer Steven A. Jones written by Luke Ford, the film was delayed by a year because of studio-required changes. Jones and director McNaughton were contractually required to deliver the film with no changes to the script written by Price. Universal Test screening|test-screened the film, then insisted on reshooting the films final scene. As written, when Milo and Mad Dog fight, Milo dominates Mad Dog. Mad Dogs one connecting punch did no damage, but did serve to prompt Milo to realize that Glory was not worth fighting over. 
 typecasting of De Niro, who they saw as the Raging Bull he had played more than a decade earlier. Those who saw the test screenings could not accept the fact that De Niros Mad Dog had done so poorly against Murrays Milo. Such a reaction was ironic because De Niro had actually been offered the Milo role, and had insisted on the Mad Dog role instead precisely because of its meekness.

Other reshoots for the film were done to make Glory seem less manipulative and Milo more of a puppetmaster behind Glorys actions.

==Reception==
Mad Dog and Glory has received mixed to positive critical acclaim. Review aggregation website Rotten Tomatoes gives the film a 78% approval rating, with an average score of 6.2/10, based on reviews from 27 critics.  The film has a score of 71/100 on Metacritic, indicating "generally favorable reviews", based on 19 critics. 

Roger Ebert, writing for the Chicago Sun-Times, gave the film 3.5 out of 4 stars, saying, "The movie is very funny, but its not broad humor, its humor born of personality quirks and the style of the performances." He went on to add that the film is "the kind of movie I like to see more than once. The people who made it must have come to know the characters very well, because although they seem to fit into broad outlines, they are real individuals -- quirky, bothered, worried, bemused."  Vincent Canby of The New York Times also gave the film a positive review, calling it "a first-rate star vehicle for the big, explosive talents of Mr. De Niro, Mr. Murray and Richard Price, who wrote the screenplay." Expanding on the performances, Canby said, "The great satisfaction of Mad Dog and Glory is watching Mr. De Niro and Mr. Murray play against type with such invigorating ease." 

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 