Wild Bill Hickok (film)
{{Infobox film
| name           = Wild Bill Hickok
| image          = 
| alt            = 
| caption        =
| director       = Clifford Smith 
| producer       = Adolph Zukor William S. Hart
| writer         = William S. Hart J.G. Hawks  James Farley William Dyer
| music          = 
| cinematography = Arthur Reeves Dwight Warren 
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Western silent James Farley, William Dyer. The film was released on November 18, 1923, by Paramount Pictures.   A print of the film exists in the Museum of Modern Art film archive. 
 
==Plot==
 

== Cast ==
*William S. Hart as Wild Bill Hickok
*Ethel Grey Terry as Calamity Jane
*Kathleen OConnor as Elaine Hamilton James Farley as Jack McQueen
*Jack Gardner as Bat Masterson
*Carl Gerard as Clayton Hamilton William Dyer as Col. Horatio Higginbotham
*Bert Sprotte as Bob Wright
*Leo Willis	as Joe McCord
*Naida Carle as Fanny Kate
*Herschel Mayall as Gambler 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 