Poetri Rimba
{{Infobox film
| name           = Poetri Rimba
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Inoe Perbatasari
| producer       =The Teng Chun
| screenplay     = Soeska
| narrator       = 
| starring       ={{plain list|
*Aisjah
*Loedi
*Ali Joego
*Bissu
}}
| music          = 
| cinematography = 
| editing        = 
| studio         = Jacatra Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies
| language       = Indonesian
| budget         = 
| gross          = 
}} Perfected Spelling Putri Rimba; Indonesian for Jungle Princess) is a 1941 film from the Dutch East Indies (present-day Indonesia) which was directed by Inoe Perbatasari and produced by The Teng Chun for Jacatra Film. A love story, it tells of a man who rescues a woman from a gang of thieves and escapes through the jungle.

==Plot==
Achmad and his group go hunting on an island. After they are separated, Achmad wanders through the islands dense jungles until he is ultimately captured by a gang of bandits under the warlord Kumis Panjang and his right-hand man Perbada. As Perbada prepares to burn Achmad alive, it is revelaed that Achmad had once saved Kumis Panjangs daughter, Bidasari, from death; as a result, he is not executed. Although Bidasari is betrothed to Perbada, she and Achmad begin falling in love. In a rage, Perbada captures Kumis Panjang and Bidasari, who are eventually rescued by Achmad. 

==Production== Tarzan films. 

The   songs, action, and romance. 

==Release==
Poetri Rimba was released in 1941.  It was Perbatasaris last film for Jacatra, although he later directed another three works for other companies. 

The film is likely lost film|lost. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and film historian Misbach Yusa Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==References==
 

==Works cited==
 

* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
* {{cite web
  | title = Inoe Perbatasari   Filmografi
  | trans_title = Inoe Perbatasari   Filmography
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/name/nmp4b9bad3bbb98a_inoe-perbatasari/filmography
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 23 September 2012
  | archiveurl = http://www.webcitation.org/6AtOgxCfk
  | archivedate = 23 September 2012
  | ref =  
  }}
* {{cite web
  | title = Poetri Rimba
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-p011-41-476637_poetri-rimba
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 27 July 2012
  | archiveurl = http://www.webcitation.org/69SOEul7c
  | archivedate = 27 July 2012
  | ref =  
  }}
* {{cite book
  | title = Profil Dunia Film Indonesia
  | trans_title=Profile of Indonesian Cinema
  | language = Indonesian
  | last = Said
  | first = Salim
  | publisher = Grafiti Pers
  | location = Jakarta
  | year = 1982
  | oclc = 9507803
  | ref = harv
  }}
 

 

 
 
 
 
 
 