Gangsters (film)
:Not Gangster (film).
 
{{Infobox film
| name           = Gangsters
| image          = Gangsters (film).JPG
| caption        = Film poster
| director       = Massimo Guglielmi
| producer       = Gianni Minervini
| writer         = Claudio Lizza Federico Pacifici
| starring       = Ennio Fantastichini
| music          = Armando Trovajoli
| cinematography = Paolo Rossato
| editing        = Nino Baragli
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Gangsters is a 1992 Italian drama film directed by Massimo Guglielmi. It was entered into the 18th Moscow International Film Festival.   

==Cast==
* Ennio Fantastichini as Giulio
* Isabella Ferrari as Evelina
* Giuseppe Cederna as Umberto
* Giulio Scarpati as Enrico
* Luca Lionello as Franco
* Claudio Bigagli as Nicola
* Mattia Sbragia
* Ivano Marescotti as Bava
* Maria Monti

==References==
 

==External links==
*  

 
 
 
 
 
 
 