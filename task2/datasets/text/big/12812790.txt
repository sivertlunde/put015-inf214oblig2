The Affairs of Martha
 
{{Infobox film
| name           = The Affairs of Martha
| image          = The-affairs-of-martha-1942.jpg
| caption        = Lobby card
| director       = Jules Dassin
| producer       = Irving Starr
| writer         = Lee Gold Isobel Lennart Marsha Hunt Richard Carlson
| music          = 
| cinematography = Charles Lawton Jr.
| editing        = Ralph E. Winters
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 66 minutes
| country        = United States 
| language       = English
| budget         = $240,000  .  gross = $375,000 
}} Marsha Hunt Richard Carlson. It is also known as Once Upon a Thursday.

==Plot==
The peace and quiet enjoyed by the residents of the exclusive neighborhood of Rock Bay, Long Island is disturbed by a newspaper gossip column tidbit that one of their maids is writing a tell-all book about her employers. Since the author is not identified, each family fears that its secrets will be aired in public. Among those confused and distraught are Dr. Sommerfield (Melville Cooper) and his wife Sophia (Spring Byington). Their teenage daughter Miranda (Virginia Weidler), however, is thrilled. Their longtime cook Mrs. McKessic (Marjorie Main) arranges a meeting of the neighborhood servants, in which they decide to band together against the attempts by many of their employers to spy on them to learn who the writer is. 

The author is the Sommerfields young maid Martha Lindstrom (Marsha Hunt). She secretly visits her publisher, Joel Archer (Allyn Joslyn), to try to get him to stop planting stories in the newspapers to generate interest in the upcoming book. 
 Frances Drake). This upsets Martha greatly, although she manages to hide it. It turns out that just before he left on his expedition, Jeff got drunk and married Martha. When he sobered up, he had second thoughts. As he had to leave almost immediately, he gave Martha money to get an annulment or a divorce. Unbeknownst to him, she did not do so, as she was in love with him. Instead, she took night classes to make herself more acceptable to his social class. Jeff is surprised to find her still working for his family. When he discovers they are still married, he insists she get the marriage dissolved so he can wed Sylvia.

To complicate her life even further, both Archer and local Casanova and handyman Danny OBrien (Barry Nelson) are strongly attracted to Martha. She is tempted by Archer, as Jeff shows no signs of returning her feelings for him. 

Finally, Archer crashes the Sommerfields dinner party, and is provoked by the guests harsh comments into stating first that he will be publishing the book and then that Martha is the author. After the last revelation, Martha flees with Archer in his car. Jeff realizes he loves her; he chases and catches her, and they are reconciled.

==Cast== Marsha Hunt as Martha Lindstrom Richard Carlson as Jeff Sommerfield
* Marjorie Main as Mrs. McKessic
* Virginia Weidler as Miranda Sommerfield
* Spring Byington as Sophia Sommerfield
* Allyn Joslyn as Joel Archer Frances Drake as Sylvia Norwood
* Barry Nelson as Danny OBrien
* Melville Cooper as Dr. Clarence Sommerfield
* Inez Cooper as Mrs. Jacell
* Sara Haden as Mrs. Justin I. Peacock Margaret Hamilton as Guinevere, a servant
* Ernest Truex as Llewellyn Castle
* Cecil Cunningham as Mrs. Llewellyn Castle
* William B. Davidson as Homer Jacell
* Aubrey Mather as Justin I. Peacock
* Grady Sutton as Justin I. Peacock, Jr

==Reception==
According to MGM records, the film earned $232,000 in the US and Canada and $143,000 elsewhere, presenting the studio with a loss of $42,000. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 