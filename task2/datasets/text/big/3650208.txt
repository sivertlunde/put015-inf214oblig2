Greased Lightning
 
{{Infobox film|
  | name        = Greased Lightning
  | image       = Greased_Lightning_(VHS_cover).jpg
    | writer      = Leon Capetanos Lawrence DuKore Melvin Van Peebles Kenneth Vose
  | starring    = Richard Pryor Beau Bridges Pam Grier
  | director    = Michael Schultz
  | producer    = Lester Berman Hannah Weinstein
  | distributor = Warner Bros.
  | released    = July 1, 1977
  | runtime     = 96 min.
  | language    = English
  | country     = United States
  | music       =
  | budget      =
}}
Greased Lightning is a 1977 American biographical film, starring Richard Pryor, Beau Bridges, and Pam Grier, and directed by Michael Schultz. Greased Lightning is a film loosely based on the true life story of Wendell Scott, the first African American NASCAR Sprint Cup Series race winner and 2015 NASCAR Hall of Fame inductee. Greased Lightning was partially filmed in Winder, Georgia Athens, Georgia at the former Athens Speedway,  and Madison, Georgia.

==Plot== white racetrack owners and police officers.

==Cast==
*Richard Pryor as Wendell Scott
*Beau Bridges as Hutch
*Pam Grier as Mary Jones
*Cleavon Little as Peewee
*Vincent Gardenia as Sheriff Cotton
*Richie Havens as Woodrow
*Julian Bond as Russell
*Earl Hindman as Beau Welles
*Minnie Gentry as Wendells Mother
*Lucy Saroyan as Hutchs Wife
*Noble Willingham as Billy Joe Byrnes
*Bruce Atkins as Deputy Turner
*Steve Fifield as Deputy Drinkwater
*Bill Cobbs as Mr. Jones
*Georgia Allen as Mrs. Jones
*Tyrone Franklin Lamar as Carworker in Yellow-Green Shirt

==See also==
*NASCAR
*Stock car racing
*Wendell Scott

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 