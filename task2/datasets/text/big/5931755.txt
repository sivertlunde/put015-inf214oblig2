The Legend of Wisely
 
 
  Hong Kong film directed by Teddy Robin.

  
 
 
 
 

==Plot==
Wisely, the famous writer/adventurer, is tricked by his friend (played by Teddy Robin, the films director) into helping him steal the dragon pearl. Sam Hui plays Wisely in this big budget HK movie, with production units filming some scenes by the Great Pyramids, and many scenes in Nepal. There are car chases and crashes, chases by horsemen and plenty of fights along the way. The Legend of Wisely is a live action comic book. A lot of effort went into making this movie, and it shows as Wisely goes from one hazard to another, an HK version of Indiana Jones.

==Credits==
* Directed by: Teddy Robin
* Produced by: Sam Hui
* Cinematography by: Peter Pau

==Credited cast==
* Ti Lung - Pak Kei-Wei
* Teddy Robin - David Ko
* Joey Wong - Sue Pak (as Wong Joe Hin)
* Bruce Baron - Howard Hope
* Heidi Makinen - Hopes Assistant
* Eva Cobo
* Sam Hui - Wisely
* Blackie Ko - The Two Headed Snake
* Ko Yau-Lun
* Wellington Fung - Cameo appearance (uncredited)

==Other crew==

* Sau Leung Blacky Ko - action choreographer

* Country : Hong Kong
* Film Company : Cinema City & Films Co.
* Genre : Adventure / Fantasy
* Runtime : 86 min.

==Also known as==
* Legend of Wisely
* Legend of Wu
* Wai Si-Lei chuen kei
* La Légende de la perle dor
* Legenda o zlaté perle
* Wei si li chuan ji (China: Mandarin title)
* Wisely Legend (literal English title)

==See also==
* Wisely Series, the novel series by Ni Kuang
* Films and television series adapted from the Wisely Series:
** The Seventh Curse, a 1986 Hong Kong film starring Chow Yun-fat as Wisely
** The Cat (1992 film)|The Cat (1992 film), a 1998 Hong Kong film starring Waise Lee as Wisely
** The New Adventures of Wisely, a 1998 Singaporean television series starring Michael Tao as Wisely
** The Wesleys Mysterious File, a 2002 Hong Kong film starring Andy Lau as Wisely
** The W Files, a 2003 Hong Kong television series starring Gallen Lo as Wisely

==External links==
*  

   
   
   
   
   
   
   
 

 
 
 


 