Gospel According to Vic
{{Infobox film
| name           = Gospel According to Vic
| image          = The Gospel According to Vic.jpg
| caption        = Theatrical release poster
| director       = Charles Gormley
| producer       = Michael Relph
| screenplay     = Charles Gormley
| starring       = {{Plainlist|
* Tom Conti
* Helen Mirren
* David Hayman
}}
| music          = B. A. Robertson
| cinematography = Michael Coulter
| editing        = John Gow
| studio         = Channel Four Films
| distributor    = Skouras Pictures  (USA) 
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $267,249  (USA) 
}}
Gospel According to Vic (original UK title: Heavenly Pursuits) is a 1986 Scottish comedy film written and directed by Charles Gormley and starring Tom Conti, Helen Mirren, and David Hayman. Set in Glasgow, Scotland, the film is about a teacher at a Catholic school whose students are searching for two more miracles that would promote the late Edith Semple to sainthood. A nonbeliever himself, the teachers skepticism is challenged when he becomes involved in seemingly miraculous events.   

==Plot==
At the Vatican, Father Cobb (Brian Pettifer) from the Blessed Edith Semple School in Glasgow, offers evidence to promote Blessed Ediths elevation to sainthood. Downplaying the idea of miracles, a Vatican official sends the "little father" back to Scotland. Undeterred, Father Cobb continues to lead the school in prayer, invoking Blessed Ediths intercession to heal the sick, including little Alice McKenzie who is crippled.

Remedial teacher Vic Mathews (Tom Conti) is not a believer in miracles, placing his faith instead in his students and in their ability to learn. He is attracted to the new music teacher, Ruth Chancellor (Helen Mirren), who appears unimpressed with his awkward advances. After fainting at a bus stop, Vic is rushed to the hospital, where tests reveal the presence of a fatal brain tumor. The doctor sees little benefit in telling Vic about his condition.

Meanwhile, the Headmaster (Dave Anderson) complains to the teachers union representative, Jeff Jeffries (David Hayman), about Vic writing letters to the school board to keep a failed student, Stevie Deans (Ewen Bremner), from being sent to a special school. Convinced he can reach the withdrawn student, Vic refuses to accept the Headmasters judgement. At a friendly card game at Vics apartment later that night, Jeff convinces Vic after a few drinks to back off on his letter-writing campaign. After everyone leaves, a drunken Vic witnesses a strange event: his stereo plays without being turned on.

The next day, Vic discovers he is able to teach basic math concepts more effectively by using examples from the gambling world. Even Stevie Deans responds to this new approach, showing he is clearly far from stupid. When Vic reports his progress, however, the Headmaster is more excited about the apparent healing of little Alice McKenzie. That night at a pub, a drunken Vic dismisses the newspaper reports of Alices miraculous recovery, and just before the conversation turns ugly, Vic faints again. Ruth offers to drive him home, and the next day in church, she prays for Vic, whose tireless teaching efforts soon lead to yet another breakthrough with another "special" student.

Later, Vic is summoned to the roof to rescue a student trapped on an adjacent roof. When he sees the boy slipping, Vic jumps across to the opposite roof, but is unable to prevent the boy from falling 40 feet through a tree that fortunately breaks his fall. Vic also loses his hold and falls from the roof. The student ends up with two broken legs, but Vic escapes with only minor scratches. When Father Cobb calls it a miracle, Vic dismisses the idea, but at the hospital, new x-rays reveal that his brain tumor is gone. The doctor has no explanation and never mentions the tumor to Vic. The hospital administrator orders the x-rays destroyed, but the radiologist holds onto them.

Soon the newspapers report Vics survived fall and the "miraculous academic improvement" of Stevie Deans. The bishop arrives and is annoyed by all the miracle stories, and Stevie is rushed out of town to a retreat, away from news reporters. School officials announce that there were no miracles involved with the student—just marked improvement based on good teaching. Vic is also trying to convince himself that his survived fall was no miracle. Ruth even takes him to a newspaper office showing him numerous stories of unfounded miracles.

Meanwhile, after seeing Vic dismissing the idea of miracles in a television interview, the radiologist delivers the x-rays to Father Cobb as "definitive proof" that a miracle actually happened—the complete healing of an inoperable brain tumor. Father Cobb considers the legal implications for the radiologist, and then burns the x-rays saying, "We dont need proof—we believe."

The story of Vics miraculous recovery is soon reported on the news. Confused by whats happened, and told he is "special", Vic goes to the hospital to heal the students broken legs, but soon realizes his folly. Back at school, Robbie complains to Vic that he wants to be a "special" student too. They go back to the roof where he and Jeff try to explain how Vic was able to make the 17-foot leap. To prove it was not a miracle, Vic makes the jump again. Afterwards, Jeff reveals Stevie Deans whereabouts, and Vic heads to the train station to bring Stevie back.

Ruth asks Robbie to help her find Vic, and the two rush off to the train station, where Ruth and Vic unite in a loving embrace. Robbie stumbles into a crowd and is forced onto a red carpet just as Princess Diana approaches. A photographer hands Robbie some flowers and he offers them to the princess as the worldwide press photographers capture the moment. Vic and Ruth leave by train to bring another "special" student back to school.   

==Cast==
 
 
* Tom Conti as Vic Mathews
* Helen Mirren as Ruth Chancellor
* David Hayman as Jeff Jeffries
* Brian Pettifer as Father Cobb
* Jennifer Black as Sister
* Dave Anderson as Headmaster
* Moica Brady as Radiologist
* Ewen Bremner as Stevie Deans
* Tom Busby as Brusse
* Juliet Cadzow as Woman Teacher
* Doreen Cameron as Nurse
* Robert W. Carr as Night Editor
* Fiona Chalmers as Child in Vics Class
* Margo Croan as Woman at Bus Stop
* Jake DArcy as Wee Man in Bar
* Bill Denniston as Bishop
* Ron Donachie as Big Man in Bar
* Mel Donald as 1st Man at Bus Stop
 
* James Gibb as MacArthur
* Sam Graham as Doctor Knox
* Grace Kirby as French Teacher
* Phillip J. Maxwell as Wee Mike
* Ronnie McCann as Boy on Roof
* David McCormack as James
* Jenny McCrindle as Carole Adams
* Billy McElhaney as Reporter 2
* David McKail as Consultant
* Lawrie McNicol as Reporter 1
* John Mitchell as Gibbons
* Sandy Neilson as 2nd Man at Bus Stop
* Paul Nolan as Robbie
* Robert Peterson as MacKrimmond
* John Shedden as Dentist
* Jay Smith as Photographer
* Carey Wilson as Education Officer
* Kara Wilson as Registrar McAllister   
 

==Production==

===Filming locations===
Gospel According to Vic was filmed in various locations throughout Glasgow, Scotland.      

* Glasgow Cathedral
* St. Alphonsus Church
* Queens Park School Glasgow Herald
* Glasgow City Chambers (representing the Vatican)
* Glasgow Queen Street railway station

==Reception==
Gospel According to Vic received positive reviews upon its theatrical release in the United States. In her review in the Washington Post, Rita Kempley called it "a comedy of marvels great and small, proves a timely answer to a moviegoers prayers." She compared this "sweet-natured and idiosyncratic work" to some of Bill Forsyths best films. She lauded Tom Contis performance with his "perfect timing and rumpled magnetism." Kempley concludes, "Theres always room for doubt in this delightfully quirky screenplay, with its grumbling atheists and gosh-almighty faithful. Gormley needs no special effects to create his aura of heavenly intervention, relying instead on ambiguous incidents and secondhand testimony."   

In his review in The New York Times, Walter Goodman singles out Contis performance:
  }}
Goodman concludes, "Gospel According to Vic may not be a miracle, but its definitely a blessing." 

In their review in Spirituality & Practice, Frederic and Mary Ann Brussat wrote:
  }}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 