His House in Order (1928 film)
{{Infobox film
| name           = His House in Order
| image          = 
| caption        = 
| director       = Randle Ayrton
| producer       = Meyrick Milton
| based on       =  
| screenplay     = P.L. Mannock Ian Hunter David Hawthorne
| music          =
| cinematography = 
| editing        = 
| studio         = Gaumont British Picture Corporation 
| distributor    = Ideal Film Company
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = Silent film English intertitles
  }} silent drama Ian Hunter David Hawthorne. Sir Arthur His House in Order. 

==Cast== 
*Tallulah Bankhead as Nina  Ian Hunter as Hilary Jesson David Hawthorne as Filmer Jesson
*Eric Maturin as Major Maurewarde
*Mary Dibley as Geraldine
*Wyndham Guise as Sir Daniel Ridgeley
*Nancy Price as Lady Ridgeley
*Claude Beerbohm as Pryce Ridgeley
*Sheila Courteney as Annabel Jesson
*Pat Courteney as Derek Jesson

==Preservation status==
Both this version and the 1920 version are believed to be lost films. 

==References==
 

==External links==
* 
*   at BFI Film & TV Database

 
 
 
 
 
 
 
 

 
 