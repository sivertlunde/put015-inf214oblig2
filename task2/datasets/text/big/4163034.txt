Maximum Risk
 
{{Infobox film
| name           = Maximum Risk
| image          = Maximum Risk Poster.jpg
| image_size     =
| caption        = Original poster
| director       = Ringo Lam
| producer       = Moshe Diamant Larry Ferguson
| starring       = {{plainlist|
* Jean-Claude Van Damme
* Natasha Henstridge
* Jean-Hugues Anglade
}}
| music          = Robert Folk
| cinematography = Alexander Gruszynski
| editing        = Bill Pankow
| distributor    = Columbia Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $25 million   
| gross          = $51.7 million 
}} action thriller film directed by Hong Kong director Ringo Lam in his American directorial debut, and starring Jean-Claude Van Damme and Natasha Henstridge. The film was released in the United States on September 13, 1996. 

==Plot==
Alain Moreau (Jean-Claude Van Damme) is a cop in Nice, France. Alain is at a funeral that is being held for a fellow cop, when Alain’s partner Sebastien (Jean-Hugues Anglade) shows up, and requests for his presence at a crime scene. When they arrive, Sebastien shows Alain a dead body of someone that looks exactly like him. They discover that his name was Mikhail Suvorov, who was born on exactly the same day Alain was. As it turns out, Mikhail is the twin brother Alain never knew he had.

Tracing his brothers steps back to New York City, Alain discovers that Mikhail was a member of the Russian Mafia, who was chased down and killed when he attempted to get out.  Of course, now Alain is mistaken for Mikhail, who was also mixed up in a series of affairs concerning the FBI and the Russian mafia.  With his only real ally being Mikhails fiancé Alex Bartlett (Natasha Henstridge), Alain sets out to avenge his brothers death, which is complicated not only by the Mafia, but by two corrupt FBI agents.

==Cast==
*Jean-Claude Van Damme as Alain Moreau / Mikhail Suverov
*Natasha Henstridge as Alex Bartlett
*Jean-Hugues Anglade as Sebastien
*Zach Grenier as Ivan Dzasokhov
*Paul Ben-Victor as Agent Pellman
*Frank Senger as Agent Loomis

==Release==
Maximum Risk opened on September 13, 1996, at the number one spot at the box office, taking in $5,612,707 in its first weekend, and made a final tally of  $14,502,483. 

==Reception==
Rotten Tomatoes, a review aggregator, reports that 28% of 32 surveyed critics gave the film a positive review; the average rating was 4.2/10.   Leonard Klady of Variety (magazine)|Variety wrote, "Its a visceral delight that refuses to be deterred by niceties of plot or character consistency and prefers sweat to emotion."   Richard Harrington of The Washington Post wrote that the film depends too much on car chases, which end up dominating the film.   Lawrence Van Gelder of The New York Times wrote, "From start to finish, "Maximum Risk" presents spectacular stunts choreographed and coordinated by Charles Picerni and some hair-raising, stomach-churning automotive chases attributed to Remy Julienne, the French master of the art."   Kevin Thomas of the Los Angeles Times called it "a solid, fast-moving action-adventure" in which Van Damme "does some of his best acting yet".   Conversely, Peter Stack of The San Francisco Chronicle criticized Van Dammes acting, which is "hobbled by a weak script that even veteran Hong Kong action director Ringo Lam cant salvage."  

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 