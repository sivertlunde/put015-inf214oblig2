Scirocco (film)
 
{{Infobox film
| name           = Scirocco
| image          = Scirocco_Lado.jpg
| image size     =
| caption        =
| director       = Aldo Lado
| producer       =
| writer         = Aldo Lado Fiorenzo Senese
| narrator       =
| starring       = Fiona Gélin
| music          = Pino Donaggio
| cinematography = Ramón F. Suárez 
| editing        =
| distributor    =
| released       = 12 August 1987
| runtime        = 88 minutes
| country        = France Italy French
| budget         =
| preceded by    =
| followed by    =
}} 1987 France|French drama film directed by Aldo Lado and starring Fiona Gélin.

==Plot==
Léa (Gélin) is married to engineer Alfredo (Enzo De Caro) who works at oil wells in the Maghreb. She visits her husband and finds that their marriage is deteriorating. She seeks relief in the exoticism the country offers and she is soon attracted to a local thug nicknamed Le Serpent (Yves Collignon) she meets in the kasbah. However, her relationship with him starts to become increasingly exploitative.

==Cast==
*Fiona Gélin: Léa
*Enzo De Caro: Alfredo
*Yves Collignon:  Le Serpent
*Joshua McDonald:  Jeff
*Gianluigi Ghione: Stefan
*Christophe Ratendra: the hookah boy
*Alberto Canova: Kurt
*Abdellatif Hamrouni: the wise man
*Nadia Saiji: the gypsy dancer
*Ridha Zouari: the worker

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 


 