The Playwright's Love
 
 
{{Infobox film
| name           = The Playwrights Love
| image          = The Playwrights Love.jpg
| caption        = The playwright trimming his cuffs with scissors
| director       = 
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English inter-titles
}} silent short short drama The Prince Chap. The film is presumed lost film|lost.

== Plot ==
 , is in straitened circumstances, but is generous despite his poverty. When a poor woman falls exhausted at his doorway he takes her in, and after her death he cares for her child. This kind of action seems to bring him luck, for a play that he had been unable to dispose of is sold for a good sum and he starts on the high road to prosperity. Ten years later when the girl is grown to womanhood, John finds that he is deeply in love with his ward, but does not betray his passion, believing that it is hopeless. His eyes are opened, however, by Will, who is in love with Grace, and is told by her when she refuses him that she loves John. When John hears this he promptly proposes and is as promptly accepted by the girl."   

== Production == The Prince Chap from several years ago.  The plot of the play bear striking familiarities to the Thanhouser production. The play focuses on William Peyton, a sculptor, who takes in a young girl, Claudia, at the request of her dying mother. Claudia grows into a young woman, rejects her suitor, and concludes with Claudia and Peyton deciding to marry.  Another mention of this film in The Morning Telegraph stated, "This story has been told many times in pictures, excepting in this film the character is a playwright."  The film director and the cameraman are unknown, but the "Thanhouser Kid" Marie Eline is the only known credit in the cast.  Eline plays the role of the young orphan Grace. Other members cast may have included the other leading players of the Thanhouser productions, Anna Rosemond, Frank H. Crane and Violet Heming.    A surviving film still shows the playwright in the process of trimming his cuffs with scissors before an interview, leaving hope that another credit may be attributed to the lost film. 

Despite the lack of production details, the quality of the Thanhouser films in general stood out amongst the Independent producers. An editorial by "The Spectator" in The New York Dramatic Mirror contained specific praise for Thanhouser productions by stating, "...practically all other Independent American companies, excepting Thanhouser, show haste and lack of thought in their production. Crude stories are crudely handled, giving the impression that they are rushed through in a hurry - anything to get a thousand feet of negative ready for the market. Such pictures, of course, do not cost much to produce, but they are not of a class to make reputation. The Thanhouser company, alone of the Independents, shows a consistent effort to do things worthwhile..."    The editorial warned that American audiences were not subject to be entertained by the novelty of moving images and cautioned the Independents that there was distinct danger in quantity over quality.  The editorial was written by Frank E. Woods of the American Biograph Company, a Licensed company, and like the publication itself had a considerable slant to the Licensed companies. 

== Release and reception ==
The one reel drama, approximately 950 feet long, was released on July 22, 1910.  The film had known showings in Indiana,  and Maryland.  The film was advertised, possibly in error, as simply Playwrights Love and A Playwrights Love.   The film received numerous reviews in the trade publications and most were positive. The Moving Picture News review was positive and stated, "Love knows neither creed nor class, and when the unsuccessful playwright falls in love with the daughter of a woman whom he had taken a charitable interest in at the time of her death, the fact is not to be wondered at. He plays a noble part and is rewarded as the film reaches it  final scene. The story is rich in feeling and sympathy, the settings are touching and appropriate and the photography is pretty good."  Another review in the publication agreed that the film was another high-quality Thanhouser release.  The Moving Picture World spared only a brief summary of the film with a simple approval for the romantic story.  The New York Dramatic Mirror found another reason that the film was worth seeing - the absence of a villain. 

== References ==
 

==External links==
* 

 
 
 
 
 
 
 
 
 