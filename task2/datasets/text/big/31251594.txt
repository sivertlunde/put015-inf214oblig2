Saints and Sinners (1949 film)
{{Infobox film
| name           = Saints and Sinners
| image          = 
| caption        = 
| director       = Leslie Arliss 
| producer       = Leslei Arliss
| writer         = 
| starring       = Kieron Moore 
| cinematography = 
| music          = 
| studio = 
| distribution   = British Lion Films
| released       = 15 August 1949
| runtime        = 
| country        = United Kingdom English
| gross = £98,061 (UK) 
}} British comedy comedy drama film directed by Leslie Arliss and starring Kieron Moore, Christine Norden and Sheila Manahan.  The film follows life in small Irish town, where a man is wrongly accused of theft.

==Cast==
* Kieron Moore - Michael Kissane
* Christine Norden - Blanche
* Sheila Manahan - Sheila Flaherty
* Michael Dolan - Canon
* Maire ONeill - Ma Murnaghan
* Tom Dillon - OBrien Noel Purcell - Flaherty
* Pamela Arliss - Betty Edward Byrne - Barney Downey
* Sheila Ward - Clothing woman
* Eric Gorman - Madigan
* Eddie Byrne - Morreys
* Liam Redmond - ODriscoll
* Tony Quinn - Berry
* Cecilia McKevitt - Maeve

==References==
 
http://www.radiofones.com/saintsandsinners.htm

==External links==
* 

 
 
* http://www.radiofones.com/saintsandsinners.htm

 
 
 
 
 
 


 