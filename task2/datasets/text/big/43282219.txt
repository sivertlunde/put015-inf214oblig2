World of Miracles
{{Infobox film
 | name = World of Miracles
 | image = World of Miracles.jpg
 | caption =
 | director =  Luigi Capuano 
 | writer =   
 | starring =  
 | music =   Michele Cozzoli
 | cinematography = Augusto Tiezzi 
 | editing =    
 | producer =   Fortunato Misiano
 | released =  
 | language =   Italian
 }} 1959 Italian melodrama film directed by Luigi Capuano.      

== Cast ==

* Virna Lisi: Laura Damiani 
* Jacques Sernas: Marco Valenti 
* Marisa Merlini: Franca 
* Aldo Silvani: Alessandro Damiani 
* Elli Parvo: Magda Damiani 
* Kerima: Carmen Herrera 
* Yvonne Sanson: Sarah 
* Vittorio De Sica: Director Pietro Giordani 
* Amedeo Nazzari: Presenter at the press conference
* Andrea Checchi: Cinematographer 
* Silvio Bagolini:  Stationmaster 
* Ignazio Leone: Il capo comparse  
* Virgilio Riento: Oscaretto  
* Marco Tulli: Adriano 
* Leopoldo Valentini: Casimirio 
* Ciccio Barbi: Commendatore Berbloni  
* Bruno Corelli: Max
* Mario Brega: Man with a black eye
* Fanfulla  
* Pietro Tordi

==References==
 

==External links==
* 
 
 
 
 
  
  


 