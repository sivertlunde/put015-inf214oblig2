12th & Delaware
{{Infobox film
| name           = 12th & Delaware
| image          = 12th & Delaware.jpg
| border         = yes
| alt            =
| director       = Rachel Grady Heidi Ewing
| producer       = Rachel Grady Heidi Ewing
| starring       = David Darling
| cinematography = Kat Patterson
| editing        = Enat Sidi
| studio         = Loki Films
| distributor    = HBO Films
| released       =  
| runtime        = 80&nbsp;minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
12th & Delaware is a documentary film set in a crisis pregnancy center and a abortion clinic across the street from it in Fort Pierce, Florida. The film was produced and filmed by Rachel Grady and Heidi Ewing and covers the center and its patients over the period of a year. The film shows interviews of staff at both facilities, as well as pregnant women who are going to them. 12th & Delaware premiered on January 24, 2010 at the 2010 Sundance Film Festival in the U.S. Documentary Competition.  It won a Peabody Award that same year "for its poignant portrait of women facing exceedingly difficult decisions at a literal intersection of opposing ideologies."  

==References==
 

==External links==
*  
*  


 

 
 
 
 
 
 
 
 

 