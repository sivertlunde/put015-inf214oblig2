Rendezvous in July
 
{{Infobox film
| name           = Rendezvous in July
| image          = Rendez vous de juillet.jpg
| caption        = Theatrical release poster
| director       = Jacques Becker
| producer       = René Gaston Vuattoux
| writer         = Jacques Becker Maurice Griffe
| starring       = Daniel Gélin
| music          = Jean Wiener
| cinematography = Claude Renoir
| editing        = Marguerite Renoir
| distributor    = LAlliance Générale de Distribution Cinématographique
| released       =  
| runtime        = 112 minutes
| country        = France
| language       = French
| budget         = 
}}
Rendezvous in July ( ) is a 1949 French comedy film directed and written by Jacques Becker. It was entered into the 1949 Cannes Film Festival.   

==Cast==
* Daniel Gélin as Lucien
* Brigitte Auber as Thérèse
* Nicole Courcel as Christine Courcel
* Pierre Trabaud as Pierrot
* Maurice Ronet as Roger
* Philippe Mareuil as François
* Henri Belly
* Jacques Fabbri
* Michel Barbey
* Francis Maziére as Frédéric
* Robert Lombard
* Jean Pommier
* María Riquelme
* Annie Noël
* Claude Luter as Chef orchestre

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 