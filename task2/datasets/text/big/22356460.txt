A Very Christmas Story
 
{{Infobox Film
| name = A Very Christmas Story
| image = PL Świąteczna przygoda A Very Christmas Story Dariusz Zawislak.jpg
| caption = Dariusz Zawiślak Dariusz Zawiślak Dariusz Zawiślak Robert Fleet
| music = Chris Rafael Maciej Zieliński
| distributor =
| released = 2000
| runtime = 87 minutes
| country = Poland
| language = English, Polish
| budget =
| gross =
| followed_by =
}}
 Dariusz Zawiślak produced in 2000. It is a family comedy.

==Plot==
The plot centres on the Christmas adventures of Mike, a corporate accountant with a briefcase full of stolen money, a little girl looking for support for her orphanage, and an angel.

==Cast==
* Paweł Burczyk
* Bartosz Opania
* Jan Englert
* Dorota Naruszewicz
* Katarzyna Olasz
* Wojciech Mann
* Krzysztof Materna
* Aleksandra Nieśpielak

==Locations==
* New York City 
* Warsaw
* Amsterdam

==External links==
*  
*  
*  
*  

==References==
 

 
 
 
 
 
 


 