Humoresque (1920 film)
 
{{Infobox film
| name           = Humoresque
| image          = 1920 TremontTemple BostonGlobe Oct13.png
| image size     = 
| border         = 
| alt            = 
| caption        = Advertisement for screening of the film at Tremont Temple, Boston, 1920
| director       = Frank Borzage
| producer       = William Randolph Hearst (uncredited)
| writer         = William LeBaron Frances Marion
| screenplay     = 
| story          = Fannie Hurst
| based on       =  
| starring       = Gaston Glass Vera Gordon Bobby Connelly Alma Rubens
| music          = Hugo Riesenfeld
| cinematography = Gilbert Warrenton
| editing        = 
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 mins.
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}}

Humoresque (1920) is an American silent film drama produced by Cosmopolitan Productions, released by  Famous Players-Lasky and Paramount Pictures, and was directed by Frank Borzage from a novel by Fannie Hurst and script or scenario by Frances Marion.

This film was the first film to win the Photoplay Medal of Honor, a precursor of the Academy Award for Best Picture. 

==Cast==
*Gaston Glass - Leon Kantor
*Vera Gordon - Mama Kantor
*Alma Rubens - Gina Berg (aka Minnie Ginsberg)
*Dore Davidson - Abraham Kantor
*Bobby Connelly - Leon Kantor (child)
*Helen Connelly - Esther Kantor (child)
*Ann Wallack - Esther Kantor (adult)(*billed Ann Wallick)
*Sidney Carlyle - Mannie Kantor
*Joseph Cooper - Isadore Kantor (child)
*Maurice Levigne - Isadore Kantor (adult)
*Alfred Goldberg - Rudolph Kantor (child)
*Edward Stanton - Rudolph Kantor (adult)
*Louis Stern - Sol Ginsberg (*as Louis Stearns)
*Maurice Peckre - Boris Kantor
*Ruth Sabin - Mrs. Isadore Kantor
*Miriam Battista - Minnie Ginsberg (child)

==Preservation status==
The film has undergone a restoration at the UCLA Film and Television Archive. 

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 