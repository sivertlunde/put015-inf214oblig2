Mr. Magorium's Wonder Emporium
{{Infobox film
| name           = Mr. Magoriums Wonder Emporium
| image          = Mr. Magorium.jpg
| caption        = Theatrical release poster
| director       = Zach Helm
| producer       = Richard N. Gladstein James Garavente
| writer         = Zach Helm
| starring       = Dustin Hoffman Natalie Portman Jason Bateman Zach Mills Kiele Sanchez
| narrator       = Zach Mills
| music          = Alexandre Desplat Aaron Zigman
| cinematography = Roman Osin
| editing        = Sabrina Pilllsco
| studio         = Mandate Pictures Walden Media FilmColony
| distributor    = 20th Century Fox   Icon Productions   Roadshow Entertainment  
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| gross          = $69.4 million   
}}
Mr. Magoriums Wonder Emporium is a 2007 fantasy comedy film written and directed by Zach Helm. The film stars Dustin Hoffman as the owner of a magical toy store, and Natalie Portman as his store employee. Also Starring Jason Bateman as the mutant account, and Zach Mills as the hat collector and his store volunteer.

== Plot ==
Molly Mahoney (Natalie Portman), called "Mahoney" throughout the film, is an employee at "Mr. Magoriums Wonder Emporium", a magical toy shop run by the eccentric 243-year-old Mr. Edward Magorium (Dustin Hoffman). Besides Molly and Mr. Magorium, store bookbuilder Bellini (Ted Ludzik), a Strongman (strength athlete)|strongman, is also employed. Eric Applebaum (Zach Mills) is a boy who comes to the store daily and functions as an employee despite his young age. He also narrates the story and introduces the characters.

The toy shop is magical, meaning the toys have a life of their own. An over-sized ledger, known as the Big Book, can magically materialize any toy on command, and a doorknob, when rotated, can change the interior of a magic room. Mr. Magorium states that he imbued the shop with the same youthful characteristics of the children who visit it. Because of its similarity to children, the shop is also prone to temper tantrums.

In response to Mahoney telling Mr. Magorium that she feels stuck, he gives her the Congreve Cube, a big block of wood, and tells her it will guide her to a new life and adventure if she has faith in it. She is unsure what to do with the cube at first. Mahoney wants to become a composer and concert pianist, because she was a musical child prodigy. She went to school for music, but has not been able to write any music since graduating. At home, she works on a concerto with little progress.

Mr. Magorium suddenly announces that he intends to "leave" and is giving the shop to Mahoney to provide her with the means to move forward and become "unstuck" in life. When Mahoney expresses her upset and doubts about her ability to run the store, the store throws a tantrum, causing all the toys and its inner workings to go haywire, ambushing customers of all ages. In preparation for his departure, Mr. Magorium hires an accountant, Henry Weston (Jason Bateman), to organize the shops paperwork and determine the monetary value of the store he will leave as a legacy to Mahoney. Henry does not believe that the toy store is magical at all at first, arguing with Molly over its magical properties and debating the actuality of Magoriums records from the past 200 years, including a note from Thomas Edison thanking Magorium for his idea about the lightbulb.

Mahoney finally realizes that Mr. Magorium is leaving not to retire but instead he is going to die. Desperate to stop this, Mahoney rushes him to a hospital where after a little mischief, he is discharged the next day because there is "nothing physically wrong with him."

After leaving the hospital, Mr. Magorium asks Mahoney how she is doing with the Congreve Cube. She states there are a million things she could do with a block of wood but she doesnt have a clue how to unlock the cubes secret. Mr. Magorium then tells her that there are a million things one could do with a block of wood, but what if someone just believed in the cube? Mahoney does not understand but attempts to prevent Mr. Magoriums departure by showing him the joys of life, but he has lived a full life and it is time for his story to end. He uses the stage notes of Shakespeares King Lear to make the point about the importance of having a full life, and the importance of death, noting that the last lines of one of the most important pieces of dramatic literature are simply "He dies." Mahoney, Eric, all the children and their parents have a funeral for him.

Believing herself to be unworthy and incapable of owning a magical store, Mahoney puts it up for sale with Henrys firm overseeing the sale. The store grieves and loses all its magic. All the toys, walls, and even the furniture lose their color, becoming varying shades of gray and black. Eric tries to reason with Mahoney over her decision to sell the store when he sees her at a restaurant playing background music.

Henry meets Mahoney at the store to draw up the sale papers, where he sees the Congreve Cube and asks her about it. When Mahoney confesses her complete faith in the store and the Congreve Cubes magical ability, the block suddenly springs to life, and proceeds to fly around the store. After witnessing this, Henry faints with shock. When he later awakes and questions Mahoney about it, she tells him that it must have been a dream as she went home the previous night, leaving him to finalize the paperwork for the sale.

Henry is not deterred as he knows Mahoney made the cube fly and though she does not believe she can do magical and wonderful things, he believes in her. Henry realizes Mahoney is the Congreve Cube. The block of wood that can be anything she desires if she can somehow believe in herself. Henrys whole hearted belief in Mahoney ignites a tiny spark in her and for a second she believes. The store responds to her spark of belief and continues to respond as her confidence builds until the entire store magnificently transforms. The magic and color return as Mahoneys long awaited symphony comes into existence.

== Cast ==
 
* Dustin Hoffman as Mr. Edward Magorium, a toy impresario, a wonder aficionado, and an avid shoe wearer. He is loved by all, and has lived for over two centuries. He lives in his own home hidden behind a magical closet with a knob that turns to change rooms and owns a pet zebra named Mortimer.
* Natalie Portman as Molly Mahoney, the Store Manager, and former child piano prodigy, who feels "stuck" in life and struggles with self-doubt.
* Jason Bateman as Henry Weston (aka "Mutant"), the straight-laced, rigid Accountant hired to get Mr. Magoriums paperwork and will in order. He does not believe in the shops magic—until the end of the film.
* Zach Mills as Eric Applebaum, a 9 year old Hat Collector who comes to the store regularly and functions as an employee despite his age. He interacts with the people in the store, although he has trouble making friends. He also narrates the beginning, the end, and introduces the chapters.
* Ted Ludzik as Bellini, the Bookbuilder who was born in the shops basement. He also writes Mr. Magoriums biography. He looks like a circus strong-man with a big moustache that curls round at the end; he also has full sleeve tattoos on both arms.
 Sock Monkey) that come to life in this movie are performed by Trish Leeper, Gord Robertson, and Bob Stutt.

== Production ==
 
Filming started in late March 2006 and continued to June 6, 2006 in Toronto.

The film was produced by FilmColonys Richard N. Gladstein and Gang of Twos James Garavente and financed by Walden Media, and Mandate Picturess Joe Drake and Nathan Kahane.

According to an interview with Zach Helm on Regis and Kelly, the name of the shop’s proprietor was derived from Zachs cousin, New Jersey native Allen Magory. The phraseology "Mr. Magoriums Wonder Emporium" was commonly employed as a jest between Helm and Magory as kids, long before the writing of any screenplay.

A cameo "just shopping" in the emporium marked the first major theatrical appearance of Kermit the Frog since 1999s Muppets from Space.

== Novelization ==
 
 
Written by Suzanne Weyn, the novelization was published in 2007, by Scholastic Inc.  

== Release ==
{{Multiple issues|section=yes|
 
 
}}
The premiere of Mr Magoriums Wonder Emporium, attended by Natalie Portman and Dustin Hoffman, also doubled as a fundraising event with tickets having been made available to the public. Funds raised at the event were donated to the Barnardos childrens charity and other UK-based charities.

=== Box office ===
The film was released in the United States and Canada on November 16, 2007 and grossed $9.6 million in 3,164 theaters its opening weekend, ranking #5 at the box office.  It went on to gross $32.1 million in the U.S. and a further $35.4 million in the rest of the world which gives the film a total of box office return of $67.5 million. 

=== Critical response ===
The film received mixed to negative reviews from critics. As of July 12, 2008 on the review aggregator Rotten Tomatoes, 36% of critics gave the film positive reviews based on 119 reviews, with the consensus among negative critics that "colorful visuals and talented players cant make up for a bland story."  On Metacritic, the film had an average score of 48 out of 100, based on 26 reviews. 

Peter Travers of Rolling Stone declared the film the years Worst Family Film on his list of the Worst Movies of 2007.  However, in recognition of the fact that it was "aimed directly at very young children", William Arnold of the Seattle Post-Intelligencer observed its "unforced and exceedingly gentle humor, its imaginative but never-quite-excessive production design and its ingratiating and surprisingly detailed performances especially by Portman and Bateman gradually break down ones cynical defenses". 
 Fox replied it was "the most magical film of the year".
 an episode of the AMC drama Breaking Bad.

=== Home media   ===
{{Multiple issues|section=yes|
 
 
}}
The film was released on DVD and Blu-ray Disc on March 4, 2008.

== See also ==
*  
*  
*  

== References ==
 

== External links ==
 
*     ( )
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 