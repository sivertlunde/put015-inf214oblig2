Loitering with Intent (film)
 
{{Infobox film
| name           = 
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = Loitering With Intent
| director       = Adam Rapp
| producer       =   Lars Knudsen    Jay Van Hoy   Marisa Tomei   Tory Lenosky  Alex Sagalchik   Ivan Martin    Michael Godere   Keith Kjarval   John Suits    Gabriel Cowan 
| writer         = Ivan Martin   Michael Godere
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Sam Rockwell   Marisa Tomei   Brian Geraghty   Ivan Martin   Michael Godere   Natasha Lyonne   Isabelle McNally
| music          = Aleks de Carvalho (score)   Money Mark (featuring original music)
| cinematography = Radium Cheung
| editing        = Rebecca Rodriguez, Michael Taylor
| studio         =   Parts and Labor   Mott Street Pictures    TideRock Films   Mm...Buttered Panini Productions   New Artists Alliance   Unified Pictures
| distributor    = The Orchard (USA)
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Loitering with Intent is a 2014 American comedy film directed by Adam Rapp and written by Ivan Martin and Michael Godere. The film stars Sam Rockwell, Marisa Tomei, Ivan Martin, Michael Godere, Brian Geraghty, Natasha Lyonne, and Isabelle McNally. It premiered on April 18, 2014 at the Tribeca Film Festival. 

==Cast==
* Sam Rockwell
* Marisa Tomei
* Ivan Martin
* Michael Godere
* Brian Geraghty
*Natasha Lyonne
* Isabelle McNally

==References==
 

==External links==
*  

 
 
 


 