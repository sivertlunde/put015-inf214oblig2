A.F.R.I.K.A.
{{Infobox film
| name           = A.F.R.I.K.A.
| image          = A.F.R.I.K.A. film poster.jpg
| caption        = Theatrical release poster
| film name = {{Film name
 | hangul         =  
 | rr             = Apeurika
 | mr             = Ap‘ŭrik‘a}}
| director       = Shin Seung-soo
| producer       = 
| writer         = Song Min-ho Kim Min-sun Lee Young-jin Jo Eun-ji
| music          = An Seong-jun
| cinematography = Jang Jun-yeong
| editing        = Ko Im-pyo
| studio         = Shin Seung Soo Productions
| distributor    = IM Pictures
| released       =  
| runtime        = 112 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} 2002 South Kim Min-sun, Lee Young-jin and Jo Eun-ji. The films title is an acronym for "Adoring Four Revolutionary Idols Korean Association".

==Plot==
Two young women, Ji-won and So-hyun, go on a road trip in a car borrowed from So-hyuns boyfriend; however, unknown to them the car is in fact stolen and contains a case of guns belonging to a corrupt police officer and gangster. The women discover the guns and initially believe them to be fakes, but after accidentally shooting out the cars back window and attracting the attention of local police, they are forced to continue their journey on foot. Ji-won and So-hyun find that life on the road can be dangerous, and resort to using guns to get themselves out of trouble. Along the way they are joined by Jin-ah, a sales clerk, and Young-mi, a dabang girl, and after several robberies the quartet end up being pursued by the police and the gangsters who want their guns back. However, their exploits turn them into media superstars with their own website and fanclub, the Adoring Four Revolutionary Idols Korean Association (A.F.R.I.K.A.), which leads to a series of copycat crimes perpetrated by their fans.

==Cast==
* Lee Yo-won as Ji-won Kim Min-sun as So-hyun Lee Young-jin as Jin-ah
* Jo Eun-ji as Young-mi
* Lee Je-rak
* Sung Ji-ru
* Kim Ji-wan
* Park Jung-woo
* Hyeon Sook-hee
* Park Yeong-gyu
* Kim Se-joon
* Kim Dong-su

==Release==
A.F.R.I.K.A. opened in South Korea on 11 January 2002 and received a total of 9,800 admissions from a 9-screen release in Seoul.  The film also had a small-scale release in Hong-Kong, opening on 17 October 2002. 

==Critical response==
Film critic Anthony Leong found little to praise about A.F.R.I.K.A., saying, "The films action sequences are unremarkable...   the same goes with the comedy, with one comic misfire coming after another". Leong was also critical of writer/director Shin Seung-soos attempted social commentary, and found the only memorable aspect to be a jokey reference to Attack the Gas Station involving Lee Yo-won and Park Yeong-gyu, who appeared in both films.  In a review for Koreanfilm.org, V. Naldi commented that the film "tries so hard to be cool, slick and funny that it forgets to take care of the fundamentals, like a script that flows well, or engaging characters", but also that despite "all its flaws, A.F.R.I.K.A. still manages to be fun, in a rather mindless way", and in particular praised the supporting cast for "creating a few memorable moments".  BeyondHollywood.com described the film as "a comedy with pretensions of being something more", and criticized the writing for being "not strong enough to accommodate its ambitions" and making the central characters "too one-dimensional". The review stated that A.F.R.I.K.A. "is unsure about what it is, or what it wants to be, and ends up not being anything of note", and regarded Shins direction as "competent, but nothing extraordinary".  LoveHKFilm.com found A.F.R.I.K.A. to have a "charm... that makes the film passably entertaining", but also commented on a lack of "major development" and the "one note characters", concluding that, "A.F.R.I.K.A. is occasionally amusing, but only as mindless fluff". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 