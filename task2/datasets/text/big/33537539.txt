Snow on tha Bluff
{{Infobox film
| name           = Snow on tha Bluff
| image          = snow_on_tha_bluff.jpg
| alt            =  
| caption        = 
| director       = Damon Russell
| producer       = Chris Knittel
| writer         = 
| starring       = Curtis Snow
| music          = 
| cinematography = 
| editing        = Takashi Doscher
| studio         = Fuzzy Logic Pictures
| distributor    = 
| released       = June 19th 2012
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} The Bluff, which is infamous for crime and drug dealing.

==Plot==
Three college students drive to the new Bluff, an Atlanta neighborhood, intending to buy some drugs from a dealer. Clearly naive, they joke about flirting in hopes of getting free drugs, and reassure one another that they have nothing to fear. One of the students holds a camera with the hope of filming the encounter. They find a dealer named Curtis Snow, who asks them to drive him to his house in order to fulfill their order. Once they arrive, Curtis pulls out a gun, robs them of their purses and camera, and makes a hasty escape. Recognizing the quality of the camera, he hands it to one of his friends, telling him to continue filming no matter what in order to capture their day-to-day life as it occurs.

Curtis soon learns that another group of drug dealers have encroached on his territory, selling drugs not far from his street. He recruits his friends and robs the rivals of their drug supply. When a friend tips him off to another gang selling drugs nearby, he invades their house with the help of his friend and robs them of their supply as well.

While playing at a pool hall, Curtis recognizes a rival drug dealer with his familiar white hat, who likewise takes note of Curtis. The man in the white hat follows Curtis into his territory, asking the locals about him, but the surface threat doesnt faze Curtis and he continues to hustle. On his way back home after cutting the latest stolen product with his crew, Curtis is ambushed in a backyard alleyway by a gunman seconds after looking "white hat" in the eye. Luckily the bullet wounds are not fatal and the ambushers gun jams right as he comes close to Curtis, allowing him to flee. He collapses in the street nearby, where he is found by a police patrol and subsequently arrested in connection with a prior robbery, and spends four months in jail.

After his release from jail, Curtis is enthusiastically welcomed back by his neighborhood, and is clearly anxious to get right back into the drug dealing game. He attempts to get revenge by ambushing "white hats" girlfriend, but in retaliation, they kill his girlfriend and mother of his child. Her death causes Curtis to sink into depression, as he now has to take care of his young son while continuing to deal drugs. In one scene, as he cuts and bags up crack to sell, he explains how he watched his uncle do exactly the same thing when he was a kid, and comments that his son is the same age he was and watching him engage in the same behavior.

Soon after, however, Curtis meets a friend to discuss up-to-date information on a mark. En route to shakedown this dealer, the car that theyre in is once again ambushed by rival gang members. Curtis and his crew return fire then give chase only to be intercepted by cops, causing everyone to bail on foot. Returning home after losing the police, Curtis is enraged by the turn of events and wrecks a room, then quietly smokes pot and watches his son play in the debris.

The movie ends with Curtis calling a local video editing studio and asking them if they can edit together all the footage he has collected.

==Cast and crew==
The film was directed by Damon Russell and stars Curtis Snow. The film was produced by Chris Knittel. 
 
 

==Exhibition at festivals & Awards==
Snow On Tha Bluff has been shown at film festivals:
* The film premiered at the 2011 Slamdance Film Festival, a showcase for the discovery of new and emerging talent in the film industry 
* 2011 Atlanta Film Festival
* 2011 Certificate of Outstanding Achievement to Takashi Doscher for editing  — Brooklyn Film Festival 
* 2011 Best Feature Film Award at Atlanta Underground Film Festival
* 2011 Best Feature Film at Chicago Underground Film Festival
* 2011 Filmmaker Magazines: 25 New Faces of Independent Film 2011
* Sound On Sight rated the Snow On Tha Bluff poster a Grade A. It was described as, "Raw, and poetic, the skin is the story, beautiful and shocking, its scars, its warpaint, its sorrow."  

==Distribution==
In early 2012, Screen Media Films purchased Snow On Tha Bluff for a 2012 release.  The release date was June 19, 2012.

UK distributor, Showbox Media Group announced that Snow On Tha Bluff would be released on Blu-ray November 12, 2012.  

==Impact==
 Filmmaker Magazine, "I would buy a couple hundred blank VHS tapes, copy a scene from the movie on it, throw the tape in the dirt, put some blood on it and seal it in a manilla envelope. From there, I would send out the tapes with no return address to politicians, conservative groups, police stations and various factions in the media. Operation “stir up shit,” was now in progress."  

An attempt on Curtis Snows life occurred in December 2011. He survived a grisly box-cutter knife attack. 

A melee broke out at the films showing at the Atlanta Film Festival, as some audience members were unsure of the authenticity of a scene in which a child puts his hands into a pile of crack cocaine with a razor blade in it.

As a result of officers seeing a part of the movie, the Atlanta Police Department contacted the filmmakers in connection with an investigation into a string of home invasions. 
 Michael K. Power 105.1, Williams described the movies truthful portrayal of the hood. He followed with, "everything that is wrong with the hood, is in this movie". 

It was announced on October 17, 2012 that producer Chris Knittel and Director Damon Russell were casting for a new film that revolves around military veterans returning home. This follow-up to Snow On Tha Bluff deals with the lasting effects of war on an American soldier. 

Music Artist B.O.B announced that his Mixtape "Fuck Em We Ball" was inspired by the film "Snow On Tha Bluff". B.O.B states, "I got it from a line in Snow On The Bluff. Its a movie based in Atlanta, and it’s about these college kids who go to the hood, looking for some ecstasy and they’re taping the whole thing then they get robbed. I put it up there with movies like Slumdog Millionaire, City of God, and Shottas. You don’t know what’s real and what’s fake. One of the characters   says “Fuck em, we ball” and it really just stuck with me. That’s where I’m at right now in my career, ‘Fuck em, I’m doing what I wanna do.’ I’m not conforming."  

Rapper T.I. put Curtis Snow in his music video for "Trap Back Jumpin".

Rapper Killer Mike released a song inspired by the movie called, "Snowin In The Bluff". 
{{cite web
|url=http://hiphopwired.com/2013/01/14/killer-mike-snowin-in-the-bluff-listendownload
|title=Killer Mike - "Snowin In The Bluff"
|publisher=hiphopwired.com
|accessdate=2012-03-22
}} 

Rapper Gucci Mane makes a reference to Curtis Snow in the song called "Dope Show"  
{{cite web
|url=http://www.youtube.com/watch?v=IAssbnG-iSk
|title=World War 3 Lean
|publisher=WW3
|accessdate=2012-03-22
}} 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 