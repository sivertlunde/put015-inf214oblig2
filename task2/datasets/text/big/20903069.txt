Goonj (1974 film)
{{Infobox film
| name           = Goonj
| image          = 
| image_size     = 
| caption        = 
| director       = S.U. Syed
| producer       = 
| writer         =
| narrator       = 
| starring       =Rakesh Roshan, Reena Roy, Mahendra Sandhu, Rajesh Behl
| music          = R.D. Burman
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1974
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1974 Bollywood suspense/horror film directed by S.U. Syed.

==Cast==
*Rakesh Roshan   
*Reena Roy ...  Meena 
*Mahendra Sandhu   
*Lalita Pawar
*Rajesh Behl
*Kader Khan
*Viju Khote
*Karan Dewan
*Raajan Haksar
*Arpana Choudhry
*Karan Dewan
*Murad
*Uma Dutt
*Paul Sharma
*Aziz Honda
*Ravi Kumar 

==Story==
A very rare suspense movie, Goonj was released in 1974 and stars Rakesh Roshan and Reena Roy. As the movie begins, two hands approaches a lady from her back and pushes her into a valley and she is killed brutally. But later she appears again in the form of a ghost in white attire. Her mission now is only to find her killer. The film holds the audience to its end. A very surprising climax…even mature and seasoned mystery-movie watchers may not guess the real killer.
Goonj (echo) starts with the killing of a woman by a man who thrusts her from a cliff. The victim turns out to be Meena (Reena Roy) who was a painter and married to Rakesh (Rakesh Roshan), a rich businessman and a painter like her. Rakeshs friend Raj Kumar aka Raju (Mahendra Sandhu) arrives at the place (a hilly estate away from the city in which Rakesh has his old-fashioned mansion and he lives there only). Raju is received at the railway station by a stranger (Shetty) who has already arranged a horse for him to reach Rakeshs residence. When Raju reaches there, it is shown that Meena is already there in a spirit like appearance. Thereafter, the story starts in the real sense in flash back with the narrator speaking about the bygone period of the lives of the characters of this story.

Meena is the granddaughter of a deceased rich man who had made a will that Reena would have to marry within a month of attaining the age of 20 years, else the wealth would go the charitable institutions. Meena has been fostered by her uncle - Ratanlaal (Karan Dewan) after the death of her parents who is in financial troubles and awaiting her marriage so that she is able to support him after getting the wealth. In this regard, he gets caveats and consultation from a crooked lawyer (Raajan Haksar) who is thinking of usurping Meenas wealth by arranging her marriage with his licentious nephew - Roopesh (Rajesh Behl). Rakesh too has a greedy aunt (Lalita Pawar). However Meena is actually in love with Raju and they have decided to marry.

Before Meena and Raju could marry, Raju is called by his ailing father who wants to see him married. In his absence, Meena is wrongly informed that he has married some other girl in the city. A disheartened Meena agrees for marrying Rakesh whose greedy aunt had initiated this matrimonial alliance proposal (after knowing about the will of Meenas grandfather). When Raju whose father has died, comes to know of Meenas marriage, he starts burning in rage, considering Meena as infidel to him. Rakeshs aunt is also unhappy with Rakesh and Meena as she is not allowed to interfere with Meenas wealth after this marriage.

On the other hand, Meena and Rakesh are invited by Roopesh and his female friend Sonia (Komila Wirk) to a party thrown on the occasion of Sonias birthday. Rakesh being out of station, they reach the venue separately and not together (reaching at different points of time). Thereafter Meena is shown as being murdered by someone who thrusts her from the cliff situated in the estate. Her uncle disappears thereafter. Investigating police officer - Khan (Kader Khan) suspects many people in this regard. A lunatic (Hiralaal) also crosses paths with the people involved. Even the servant in Rakeshs home (Viju Khote) appears to be mysterious by his gestures.

Now as shown in the beginning of this movie, Raju comes back and reaches Rakeshs house. However its Meena who opens the gate of the mansion for him, ushers him to the guest room of the mansion and entertains him. When Raju tells Rakesh about it and comes to know from him that Meena is dead, both are taken aback and scared like anything. Is Meena alive or its her spirit who wants to seek revenge from her killer ? The same question appears before Roopesh also whom Meena meets. The biggest surprise for the males come when a stranger approaches Rakeshs home and starts asking for his wife. When Rakesh expresses ignorance of his wife, he starts accusing him to be in collusion with his in-laws. Whats the solution of this mystery ? The climax of this brilliant suspense-thriller reveals it.

Those who could watch this admirable suspense-thriller on big screen / Doordarshan are fortunate because now this movie is available on VCD / DVD of Bombino whose quality s.......o poor that the less said the better. Not only many scenes appear to be missing giving continuity jerks to the viewer but also both the picture quality and the sound quality are extremely bad. Adding insult to injury, a statement from Bombino appears every now and then just in the middle (yes, middle) of the screen to irritate the viewer. Its a pity that such a brilliant mystery could not be transferred to CD in a proper form.

The movie is damn interesting with full marks to the writer as well as the director (S.U. Sayeed). The opening murder scene is re-exhibited at the interval point after the story till then has been narrated in flash back. Post interval, the story moves further with several suspects available for the murder taken place, having own respective motives for committing the same. The mystery is very intricate with several characters involved and all the things logically connected to each other.

Still unanswered questions are left for the viewer. May be due to the continuity jerks in the moth-eaten version of the movie available on the CD (because many scenes appear to have been removed). Else, the script has been written proficiently with everything perfectly falling into place when the revelation of the suspense is made in the climax. There is nothing superfluous and all the incidents and characters are logically connected to each other, creating confusion for the viewer during the movie but explaining everything to him in the climax.

==External links==
*  

 
 
 
 
 

 
 