Streak (film)
{{Infobox film
| name = Streak
| director = Demi Moore
| producer = Leslie Russo Bill Wackermann
| writer = Kelly Fremon Allan Loeb
| starring = Brittany Snow Rumer Willis Sarah Wright Madeline Zima
| music = Jonathan Darling
| cinematography = Darren Lew
| studio = Freestyle
| runtime = 16 minutes
| country = United States
| language = English
}}
Streak is a 2008 American coming-of-age short film directed by Demi Moore, written by Kelly Fremon and Allan Loeb, and starring Brittany Snow and Rumer Willis.  The film was actress Demi Moores first film.  , Streak – Demi Moore’s Short Film at the Nashville Film Fest.   The plot focuses on a young woman stuck in a life she no longer wants with gym-rat friends and obsessive behavior.  To break free, she reaches for fun in an interesting form of expression.

==Family connection==
Director Demi Moores then 20-year-old daughter Rumer Willis starred in the film.  Of directing her daughter, Moore said, "The great thing is I’m seeing her mature and operate as a complete professional. And it’s giving us, I think, another opportunity to connect in a totally different way."  , Demi Moore Rumer Willis "Streak" Short Film. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 