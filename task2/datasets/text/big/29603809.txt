Cap Canaille (film)
 
{{Infobox film
| name           = Cap Canaille
| image          = Cap Canaille-poster.jpg
| caption        = French film poster
| director       = Juliet Berto Jean-Henri Roger
| producer       = 
| writer         = Juliet Berto Jean-Henri Roger Claude Vesperini
| starring       = Juliet Berto
| music          = 
| cinematography = William Lubtchansky
| editing        = Nicole Lubtchansky
| distributor    = 
| released       = 23 February 1983
| runtime        = 103 minutes
| country        = France
| language       = French
| budget         = 
}}

Cap Canaille is a 1983 French drama film directed by Juliet Berto and Jean-Henri Roger. It was entered into the 33rd Berlin International Film Festival.   

==Cast==
* Juliet Berto as Paula Baretto
* Richard Bohringer as Robert Vergès
* Jean-Claude Brialy as Me Samuel Kebadjan
* Bernadette Lafont as Mireille Kebadjan
* Patrick Chesnais as Wim
* Gérard Darmon as Nino Baretto
* Richard Anconina as Mayolles
* Nini Crépon as Dugrand
* Raúl Gimenez as Ernest la gâchette Andrex as Pascal Andreucci
* Jean Maurel as Ange Andreucci
* Toni Cecchinato as Hugo Zipo Richard Martin as Jo larchitecte
* Isabelle Ho as Miss Li

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 