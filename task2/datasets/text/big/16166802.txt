Arputham
{{Infobox film|
| name = Arputham
| image =
| caption =
| director = R S Venkatesh
| producer = R.B. Choudary
| writer = Kunal Anu Livingston Kalairani Vaiyapuri Dhamu
| producer =
| music = Shiva
| editor =
| released = 12 September 2002
| runtime =
| country = India
| language = Tamil
}}

Arputham is a South Indian Tamil film released in 2002. Arputham merges two of the most enduring concepts in Tamil cinema&mdash;the love triangle and the rags-to-riches story.

==Cast==
* Raghava Lawrence as Ashok Kunal as Arvind
* Anu Prabhakar as Priya
* Pyramid Natarajan as Ashoks Father
* Kalairani as Ashoks Mother Livingston as Govind
* Vaiyapuri
* Dhamu

==Story==
Ashok(Raghava Lawrence) is a good-for-nothing youth whose only aim is to enjoy life by drinking and dancing the night away with his friends. To this end, he even swindles his father(Pyramid Natarajan) regularly off of his hard earned money. He meets Priya(Anu Prabhakar) and soon her beauty and outlook on life make him fall for her. When he proposes to her, threatening to kill himself if she doesnt accept his love, she promises that she would accept him if he makes a name for himself. Spurred by her words, Ashok starts working hard. But what he doesnt know is that Priya is already in love with Arvind (Kunal (actor)|Kunal) and that her promise to him was just to prevent him from taking the extreme step on her rejection.

==Soundtrack==

{{tracklist
| extra_column = Singer(s)
| total_length = 25:07
| all_lyrics =
| all_music = 
| title1 = Aaha Anandham
| extra1 = Shankar Mahadevan
| length1 = 5:15
| title2 = Nee Malaraa Malaraa
| extra2 = Unni Krishnan & Chithra
| length2 = 4:33
| title3 = Pandibazaaril
| extra3 = Harish Raghavendra
| length3 = 5:38
| title4 = Pothunda Pothunda
| extra4 = Tippu
| length4 = 5:20
| title5 = Thattu Thattu
| extra5 = S. P. Balasubramanyam 
| length5 = 4:21
}}

 
 
 
 


 