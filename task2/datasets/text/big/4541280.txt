Tere Pyar Mein
 
 
{{Infobox film
| name           = Tere Pyaar Mein
| image          = Tere-Pyaar-Main-Dvd-copy.jpg
| image_size     =
| caption        =
| director       = Hassan Askari
| producer       = Sajjad Gul
| writer         = Mazhar Anjum Rashid Sajid
| narrator       = Shaan Zara Sheikh Veena Malik Badar Munir
| music          = Amjad Bobby
| cinematography =
| editing        = Zulifquar Zulfi
| distributor    = Evernew Pictures
| released       = December 28, 2000
| runtime        =
| country        = Pakistan
| language       = Urdu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Tere Pyar Mein (  film which was released in December 2000.  It launched the career of Zara Sheikh in Lollywood. 

==Plot==
The story is about a young Indian Sikh girl Preety played by Zara Sheikh, who goes to the historical city of Lahore, Pakistan for pilgrimage with her father. She falls in love with a Pakistani boy named Ali (Shaan (actor)|Shaan) who is a banker and also her dads friends son. After pilgrimage she goes back to her country. Ali realises what he has lost and goes after her. They both are delighted to see each other but Preetys friend, who is also in Indian Army, is not able to bear Preety falling in love with a Pakistani as he wants to marry her. He sends his forces after the two lovers by claiming that they are spying for Pakistan. The couple flees and after days of hide and seek, Ali is finally able to destroy the Army cars following them. In the last scene, the lovers arrive at the Pakistan-India border where they see a Pakistani Flag and their emotions are filled with joy and relief.

==Film location==
The film is shot entirely in Pakistan. 

==Film business==
It acquired a diamond jubilee (one year running).

Lahore: Diamond Jubilee, 109 weeks (Gulistan 30, Sozoworld 15, Capital 7, Rattan 4 weeks). Completed continuously more than one year in Lahore and became first Urdu film that ran for more than one year.

==Soundtrack==
The music is composed by Amjad Bobby.

* "Kal Thi Mohabbat" - Hema Sardesai
* "Aasman Ko Lagane Haath Main" - Kavita Krishnamurthy
* "Haath Se Haath Kia Gaya - Duet" - Sonu Nigam
* "Haath Se Haath Kia Gaya - Male" - Sonu Nigam
* "Nikli Ghar Se" - Jaspinder Narula
* "Sangam Hua" - Kavita Krishnamurthy
* "Dum Ishq Da Yaara" - Jaspinder Narula

==Cast== Shaan (as Ali )
* Zara Sheikh (as Preeti)
* Veena Malik (as Amina)
* Irfan Khoosat
==Awards==
{| class="wikitable sortable" 
|- 
! Year !! Award !! Film !! Winner !! Result
|-
| 2000
| Nigar Awards Best Film
|  Tere piyar mein
| Shehzad Gul
|  
|-
| 2000
|  Nigar Awards Best director
| Tere piyar mein
| Hassan Askari
|  
|-
| 2000
| Nigar Awards Best Writer
|  Tere piyar mein
|  Raja Aziz Khan
|  
|-
| 2000
| Nigar Awards Best actor
| Tere piyar mein Shaan
|  
|-
| 2000
| Nigar Awards Best actress
|  Tere piyar mein
|  Zara Sheikh
|  
|-
| 2000
| Nigar Awards Best supporting actor
| Tere piyar mein
| Raza Riaz
|  
|-
| 2000
| Nigar Awards Best editor
|  Tere piyar mein
|  Zara Sheikh
|  
|-
| 2000
| Nigar Awards Best sound editor
| Tere piyar mein
| Afzal Hussain
|  
|-
| 2002
| Lux Style Award Best Film
| Tere piyar mein
| Shehzad Gul
|  
|-
|}

==References==
 

==External links==
*  
*  
 
 
 
 
 
 
COPY
Indian film Veer-Zaara(2004 starring Shah rukh khan preiety zinta rani mukherji)  by Yash Raj  Films is pure copy of blockbuster Pakistan movie Tere Pyar Mein(starring Shan Shahid and Zara Sheikh) by Sajjad Gull.

http://www.paklinks.com/gs/showbiz-pakistan/166633-shame-on-bollywood-veer-zara-a-pure-pakistani-copy-film.html