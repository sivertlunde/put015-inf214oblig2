Nehlle Pe Dehlla
 
{{Infobox film
| name           = Nehlle Pe Dehlla
| image          =Nehlle_Pe_Dehlla_poster.jpg
| caption        = Theatrical release poster
| director       = Ajay Y. Chandok
| producer       =  Yunus Sajawal
| narrator       = Shakti Kapoor
| starring       = Sanjay Dutt Saif Ali Khan Bipasha Basu Kim Sharma
| music          = Anand Raj Anand Himesh Reshammiya
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Dhariwal Films
| released       =  
| runtime        = 184 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Nehlle Pe Dehlla (earlier titled as Jimmy and Johny) is a 2007 Bollywood comedy film directed by Ajay Chandok, and starring Saif Ali Khan, Sanjay Dutt, Bipasha Basu and Kim Sharma. The film was filmed in 2002, even though it premiered on 2 March 2007. It also has influences of other contemporary movies such as Winners and Sinners, Weekend At Bernies and Weekend at Bernies II.

==Synopsis==

Johny (Sanjay Dutt) and Jimmy (Saif Ali Khan) are small time crooks who dream of becoming rich. The duo meet in jail where they constantly end up for their misdeeds. A hotel manager, Ballu (Shakti Kapoor) embezzles his companys 30 crores. Johny and Jimmy come to know about it and plan to blackmail him. They join the hotel as waiters and are waiting for the perfect opportunity. But a terrible mobster trio (Mukesh Rishi, Aashif Sheikh and Shiva) kill Ballu. When Johny and Jimmy find Ballu dead, they take his body and present him as alive. They keep doing it until they find the real culprit. Johnny meets Ballus niece, Puja (Bipasha Basu) and Jimmy meets her friend (Kim Sharma). They all fall in love, and once they all find the real culprit, corpse and a map to direct the money, they all go up on a chase together to find the money, with the three mobsters after them also handing for the money. They all get there, only to find the mobsters have already arrived there and took all the money. They reach the mobsters hideouts, and take all the money. The three then come to meet Jimmy and Johny, and since they do not give them the money, the three kidnap Puja. Jimmy and Johny go to rescue her, by giving the money to the mobsters. They get there, have a brawl, and eventually, the police enters. The three mobsters get arrested, Johny and Puja get married, as well as Jimmy and Pujas friend, and live a happy and clean life with all her uncles money.

==Cast==

* Sanjay Dutt .... Johnny
* Saif Ali Khan .... Jimmy
* Bipasha Basu .... Pooja
* Shakti Kapoor .... Balram Sahni (Balu)
* Kim Sharma .... Kim (Poojas friend)
* Mukesh Rishi .... Dilher
* Avtar Gill .... Ram Prasad Gupta
* Shiva Rindani .... Jazzy
* Aashif Sheikh .... Hansa
* Supriya Karnik .... Special Appearance as Matha Ji
* Neha Dhupia .... Special Appearance in end credits song Dil Jane
* Ganesh Acharya .... Special Appearance in song Imaan Dol Jaayenge

==Soundtrack==
*Nehle Pe Dehla (Shaan/Kunal ganjawala)
*Tera Husn Husn 
*Dil Naaiyo Mare Ne (Himesh Reshammiya and Tulsi Kumar)
*Parvar Digara(Tulsi Kumar and K.K.)
*Bottle mein (Vinod Rathod/Abhijeet)
*Neeli neeli aakhon (Shaan)

==External links==
 

 
 
 

 