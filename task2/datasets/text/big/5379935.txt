The Secret Book
 
 
{{Infobox Film
  | name = The Secret Book
  | image = 
  | caption =  Vlado Cvetanovski
  | producer =  KTV Media, Dimitar Nikolov
  | writer = Jordan Plevnes (novel) Jordan Plevnes, Ljube Cvetanovski (screenplay)
  | starring = Thierry Frémont Jean-Claude Carrière Vlado Jovanovski Labina Mitevska Djordji Jolevski Meto Jovanovski
  | music = 
  | cinematography = 
  | editing = 
  | distributor = 
  | screened = 2006 at Cannes Film Festival
  | released = 2006
  | runtime = 
  | country = Macedonia French
  | budget = 
  | preceded_by = 
  | followed_by = 
  }} Macedonian feature thriller and Glagolitic letters Slav alphabet, made by Saints Cyril and Methodius|SS. Cyril and Methodius).

Bogomil ideas, carried back to France and Italy from the Balkans by refugees or returning crusaders in the 11th century, became the basis of the Cathar heresy. Like them, the Bogomils were massacred by the church and their name almost burned from history.

==Synopsis== Glagolitic script.

Led by the strange messages from the Balkans brought to him by doves, he chooses his son Chevalier (Thierry Fremont) to search where he stopped. The messages are sent from Macedonia by Pavle Bigorski, a man that identifying himself with the authentic author of "The Secret Book" from the Middle Ages.

The book is supposed to contain the principle of good and evil and the principle of power, jumping across the time barrier and touched the essence of the Quest for the roots of Truth.

Bigorski has three brothers, symbolizing the three regions inhabited by ethnic Macedonians. Each brother represents some aspect of the Macedonian spirit (faith, rebellion towards the social evil, defense of honour).

===Location===
The movie was shot in 2002 and 2003 on location in Bitola and Ohrid in Macedonia, and Balchik in Bulgaria.

==See also==
*The Da Vinci Code
*The Da Vinci Code (film)

==External links==
*   
*  
*   by Fiachra Gibbons (Guardian)
*   (Reality Macedonia, April 14, 2002)

 
 
 
 
 