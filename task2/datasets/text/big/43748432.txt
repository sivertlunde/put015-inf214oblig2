The Little Minister (1922 film)
{{Infobox film
| name           = The Little Minister
| image          =File:Little Minister poster.jpg
| image size     =
| caption        =Film poster David Smith
| producer       = {{Plain list|
*Vitagraph Company of America Albert E. Smith}}
| writer         = {{Plain list|
*James Barrie (novel, play)
*C. Graham Baker (scenario)
*Harry Dittmar (scenario)}}
| starring       = {{Plain list|
*Alice Calhoun James Morrison}}
| music          =
| cinematography = Stephen Smith Jr.
| editing        =
| distributor    = Vitagraph Company of America
| released       =   reels
| country        = United States Silent (English intertitles)
}} The Little James Morrison. 

==Cast==
*Alice Calhoun as Lady Babbie James Morrison as Gavin Dishart
*Henry Hebert as Lord Rintoul
*Alberta Lee as Margaret Dishart
*William McCall as Rob Dow
*Dorothea Wolbert as Nanny Webster
*Maud Emery as Jean
*George Stanley as Doctor McQueen
*Richard Daniels as Micah Dow
*Charles Wheelock as Captain Halliwell

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 