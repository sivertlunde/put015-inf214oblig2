Hell's Half Acre (2006 film)
{{Infobox Film
| name           = Hells Half Acre
| image          = HELLSHALFACREDVDNEWS.JPG
| image_size     = 
| caption        = Promotional poster for Hells Half Acre
| directors      = Sean Tiedeman & Scott Krycia
| producer       = K Studios|  Studios
| writer         = 
| narrator       = 
| starring       = Tesia Nicoli, Ken Schwarz, Todd Labar
| music          = 
| cinematography = 
| editing        = 
| distributor    = blackplague films
| released       = 2006
| runtime        = 60 minutes
| country        = United States English
| budget         = 
| gross          = 
}} Tiedeman and Krycia shot Hells Half Acre on MiniDV for a modest budget. The duo planned everything from elaborate massacre sequences to the burning of an actual full-size house. 

==Plot==
A serial killer is brought to justice by his victims and burned alive on what is now known as Hells Half Acre.  Years later, a faceless killer begins slaughtering the townspeople.  Losing her friends and family, Nicole Becker (Tesia Nicoli) decides to go after the killer with all she has got.  Double machetes, shotguns, dual handguns, and even a chain gun are all part of this killers arsenal.

==Cast==
* Tesia Nicoli as Nicole Becker
* Ken Scwhwaz as Detective Lapetta
* Todd Labar as The Killer
* Jim Clauser as WDIE Radio Host
* Bob Weick as Bob Moore

==Release==
Hells Half Acre premiered theatrically on Saturday, October 27, 2007 at the Full Moon Film Festival in Little Rock, Arkansas. 
On Friday, March 13, 2009 Hells Half Acre was shown as part of the Severed Sinema film series.  The event was held at the Sherman Theater in Stroudsburg, Pennsylvania. 

==External links==
*  
*  
*  .
*  
*  
*  
*  
*  

==References==
 

 
 
 