Fabulous Lola
{{Infobox film
| name = Fabulous Lola
| image =
| image_size =
| caption =
| director = Richard Eichberg
| producer =Richard Eichberg
| writer = Gustav Kadelburg (book)   Hans Junkermann
| music = Artur Guttmann Hugo Hirsch Heinrich Gärtner Bruno Mondi
| editing = UFA
| distributor = Parufamet
| released = 28 October 1927
| runtime =
| country = Germany
| language =Silent German intertitles
| budget =
| gross =
}} Hans Junkermann.  The films sets were designed by the art director Jacek Rotmil. It was made at the studios in Johannisthal (Berlin).

The film is based on an operetta of the same name from 1919 by   (1884–1961), which in turn was based on Gustav Kadelburgs 1906 play   (The Road to Hell).   The operetta was filmed again in 1954. 

==Cast==
* Lilian Harvey as dancer Tilly Schneider aka Lola Cornero
* Harry Halm as music hall director Bendler Hans Junkermann as music hall director Dornwald
*   as Agathe Dornwald
* Gyula Szöreghy as Enrique de la Plata

== References ==
 

==Bibliography==
* William Grange|Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

== External links ==
*  

 
 
 
 
 
 
 
 
 

 