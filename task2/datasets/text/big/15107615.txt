The Lady of Musashino
{{Infobox film
| name           = The Lady of Musashino
| image          = Musashino fujin poster.jpg
| image_size     = 
| caption        = Original Japanese movie poster
| director       = Kenji Mizoguchi
| producer       = Hideo Koi
| writer         = Yoshikata Yoda
| narrator       = 
| starring       = Kinuyo Tanaka
| music          = Fumio Hayasaka
| cinematography = Masao Tamai
| editing        = Ryoji Sakato
| distributor    = Toho
| released       = 14 September 1951
| runtime        = 88 min.
| country        = Japan Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Japanese film directed by Kenji Mizoguchi. The script for the film was adapted by Mizoguchi from the best-selling serial novel by Shōhei Ōoka.   

==Plot== Masayuki Mori), a college professor, a vulgar man with a low-class background. Towards the end of World War II, they flee the Bombing of Tokyo to her parent’s estate in Musashino, Tokyo|Musashino, a pastoral location in the outskirts of Tokyo. Nearby is her cousin, Eiji Ono (So Yamamura), a wartime profiteer with loose morals, and his wife Tomiko (Yukiko Todoroki). After the end of the war, the extended family is joined by Tsutomu Miyaji (Akihito Katayama), another cousin and former prisoner-of-war.

In the immediate post-war era, Japanese traditions and morals are in rapid decline. Tadao comes home drunk every night, and is having sex with his female university students. He also propositions Tomiko. Tomiko, unhappy in her marriage, lusts after Tsutomu, but Shinzaburo is more interested in Michiko. Although Michiko has suppressed feelings for Tsutomu, she resists his advances on traditional moral grounds – because she is a married woman, and because she does not want Tsutomu to fall prey to the new permissiveness of post-war Japan because of her love to him. However, when she learns of Tadao’s plans to run off with Tomiko after swindling her out of her inheritance, she decides that the only honorable course of action is to commit suicide, and leave 2/3 of her estate to Tsutomu.

==Cast==
*Kinuyo Tanaka as Michiko Akiyama
*Yuikiko Todoroki as Tomiko Ono Masayuki Mori as Tadao Akiyama
*Akihito Katayama as Tsutomu Miyaji
*So Yamamura as Eiji Ono
*Eitaro Shindo as Shinzaburo Miyaji
*Kiyoko Hirai as Tamiko Miyaji
*Minako Nakamura as Yukiko Ono
*Noriko Sengoku as Maid in the Ono house

==Themes==
The main theme of The Lady of Musashino is the moral decline of Japan’s traditional culture and morality in a society disrupted by the aftermath of World War II, and the sudden influx of foreign ideas and foreign moral concepts.

==Production==
Eiji Tsubaraya designed the visual effects for the film.   

==Release==
A PAL edition DVD of the film was released by Artificial Eye in 2004.   

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 