Hands of a Stranger
{{Infobox film
| name           = Hands of a Stranger
| image          =
| image_size     =
| caption        =
| director       = Newt Arnold
| producer       = Newt Arnold (producer) Michael Du Pont (producer)
| writer         = Newt Arnold (writer) Maurice Renard (novel "Les Mains dOrlac")
| narrator       =
| starring       = See below
| music          = Richard LaSalle
| cinematography = Henry Cronjager Jr.
| editing        = Bert Honey
| distributor    =
| released       = 22 April 1962
| runtime        = 95 minutes 86 minutes (Ontario, Canada)
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Hands of a Stranger is a 1962 American film directed by Newt Arnold.

==Plot summary==
When a pianists hands are destroyed in a traffic accident he receives a double transplant from a murder victim. Much to the bewilderment of the pianist, these new hands have a mind of their own decide to take control and gain vengeance for their past owners death and are forcing him to commit evil acts. 

==Cast==
*Paul Lukather as Dr. Gil Harding
*Joan Harvey as Dina Paris
*James Stapleton as Vernon Paris
*Ted Otis as Dr. Ross Compton
*Michael Rye as George Britton
*Laurence Haddon as Lt. Syms
*Elaine Martone as Eileen Hunter
*George Sawaya as Tony Wilder, the cab driver
*Michael Du Pont as Dr. Ken Fry
*Sally Kellerman as Sue
*David Kramer as Carnival Barker
*Irish McCalla as Holly
*Barry Gordon as "Skeet" Wilder

==Soundtrack==
 

==External links==
* 
* 

==References==
 

 

 
 
 
 
 
 
 
 
 

 

 