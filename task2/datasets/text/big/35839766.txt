The Stork Caliph
{{Infobox film
| name           = The Stork Caliph
| image          = 
| image_size     = 
| caption        = 
| director       = Alexander Korda
| producer       = Alexander Korda
| writer         =  Mihály Babits (novel)    Frigyes Karinthy
| narrator       = 
| starring       = Gyula Bartos Oscar Beregi Sr.  Judit Bánky
| music          = 
| editing        = 
| cinematography = Gusztáv Mihály Kovács
| studio         = Corvin Film
| distributor    = 
| released       = October 1917
| runtime        = 
| country        = Hungary Silent  Hungarian intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian silent silent drama film directed by Alexander Korda and starring Gyula Bartos, Oscar Beregi Sr. and Judit Bánky. It was the second film made by Korda for his newly established Corvin Film company. He pulled off what was considered a literary coup by persuading the author Mihály Babits to allow him to film a version of his 1916 novel of the same name. 

==Cast==
* Gyula Bartos   
* Oscar Beregi Sr. 
* Judit Bánky   
* Alajos Mészáros  

==References==
 

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.

==External links==
* 

 

 
 
 
 
 
 
 
 
 