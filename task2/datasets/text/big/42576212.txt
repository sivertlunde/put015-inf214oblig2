Seconda B
{{Infobox film
| name = Seconda B
| image =
| image_size =
| caption =
| director = Goffredo Alessandrini
| producer = 
| writer =  Umberto Barbaro    Goffredo Alessandrini
| narrator =
| starring = Sergio Tofano   Dina Perbellini   María Denis   Ugo Ceseri 
| music = Antonio Antonini 
| cinematography = Carlo Montuori
| editing = Giorgio Simonelli 
| studio =   ICAR
| distributor = Cinès-Pittaluga
| released = 1934
| runtime = 76 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Venice Film Festival where it was awarded a prize. It started a trend for "schoolgirl comedies" during the Fascist era, targeted primarily at girls and young woman audiences.  The title itself refers to a school class.  The film is set in the early 1910s.

==Synopsis==
A school teacher falls in love with one of his female colleague, who teaches gymnastics. She returns his love, but this discovered by his students who try and sabotage their relationship.

==Main cast==
* Sergio Tofano as Professore Monti 
* Dina Perbellini as Professorina Vanni  
* María Denis as Marta Renzi 
* Ugo Ceseri as Lonorevole Renzi  
* Cesare Zoppetti as Il preside 
* Umberto Sacripante as Il bidello  
* Mercedes Brignone as Uninvitata alla festa dei Renzi 
* Gino Viotti as Un insegnante  
* Alfredo Martinelli as Un altro insegnante  
* Vinicio Sofia as Il segretario di Renzi  
* Liselotte Smith as Una compagna di scuola di Marta  
* Dora Baldanello as Petronilla  
* Amina Pirani Maggi as Signora Renzi  
* Zoe Incrocci as Lallieva Fumasoni  
* Elena Tryan-Parisini as Linsegnante di francese 
* Lina Bacci as Linsegnante Zucchi  
* Celeste Aída as Signora Cesira 
* Albino Principe as Un invitato al festo

== References ==
 

== Bibliography ==
* De Grazia, Victoria. How Fascism Ruled Women: Italy, 1922-1945. University of California Press, 1992.
*Moliterno, Gino. The A to Z of Italian Cinema. Scarecrow Press, 2009.
* Reich, Jacqueline & Garofalo, Piero. Re-viewing Fascism: Italian Cinema, 1922-1943. Indiana University Press, 2002.
 
== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 