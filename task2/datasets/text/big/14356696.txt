Paw (film)
{{Infobox film
| name           = Paw
| caption        = Poster with the films alternative title: Boy of Two Worlds
| image	         = Paw FilmPoster.jpeg
| director       = Astrid Henning-Jensen
| producer       = Mogens Skot-Hansen
| screenplay     = Astrid Henning-Jensen Bjarne Henning-Jensen   
| based on       =   
| starring       = Jimmy Sterman
| music          = Herman D. Koppel 
| cinematography = Henning Bendtsen
| editing        = Anker Sørensen
| studio         = Laterna Film 
| distributor    = United States: G. G. Communications 
| released       = Denmark: 18 December 1959 United States: April 1970 
| runtime        = Denmark: 100 minutes  United States: 88 minutes 
| country        = Denmark
| language       = Danish
| budget         = 
}}
Paw (also known as Boy of Two Worlds) is a 1959 Danish film directed by Astrid Henning-Jensen.

==Plot==
A boy from the Caribbean, affected by the deaths of his parents and maiden aunt, escapes to the Danish forest.   

==Cast==
* Edvin Adolphson - Anders
* Jimmy Sterman - Paw
* Asbjørn Andersen - Gutsbesitzer
* Ninja Tholstrup - Yvonne
* Helge Kjærulff-Schmidt - Lehrer
* Karen Lykkehus - Fräulein Bo
* Preben Neergaard - Søofficer
* Karl Stegger - Betjent Hansen
* Ebba Amfeldt - Fru Hansen
* Svend Bille - Onkel Pot
* Ego Brønnum-Jacobsen - Mand ved skydetelt
* Otto Hallstrøm
* Mogens Hermansen - Godsejerens skytte
* Grethe Høholdt - Betjentens datter Inger
* Finn Lassen - Forstander på børnehjemmet

==Release and reception==
Paw was originally released in Denmark in December 1959.  It was nominated for the Academy Award for Best Foreign Language Film    and was entered into the 1960 Cannes Film Festival.    In the United States, the film was released in April 1970 by G. G. Communications under the title Boy from Two Worlds; twelve minutes were cut from the original 100-minute running time.   In his Family Guide to Movies on Video, Henry Herx deemed it "a very engaging childrens movie ... that will also interest adults". 

==See also==
* List of submissions to the 32nd Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 


 