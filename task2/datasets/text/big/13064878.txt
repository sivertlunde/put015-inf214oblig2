The Counter Jumper
 
{{Infobox film
| name           = The Counter Jumper
| image          =
| caption        =
| director       = Larry Semon
| producer       = Larry Semon
| writer         = Larry Semon
| starring       = Oliver Hardy
| cinematography = Reginald Lyons
| editing        = A.A. Jordan Larry Semon
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}

The Counter Jumper is a 1922 American film directed by Larry Semon and featuring Oliver Hardy.    It is a remake of the 1919 film The Grocery Clerk, which was also directed by Semon. The film was screened at the Museum of Modern Art in 2009 as part of a series examining slapstick.   

==Cast==
* Larry Semon - Larry, the Counter Jumper
* Lucille Carlisle - Glorietta Hope
* Oliver Hardy - Gaston Gilligan (as Babe Hardy)
* Spencer Bell - A Clerk
* Eva Thatcher
* Jack Duffy William McCall
* Reginald Lyons
* James Donnelly
* William Hauber - Bit Role (uncredited)
* Joe Rock - Bit Role (uncredited)
* Al Thompson - Bit Role (uncredited)

==See also==
* List of American films of 1922
* Oliver Hardy filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 