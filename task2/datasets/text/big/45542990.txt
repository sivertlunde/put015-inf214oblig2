The Nuthouse (film)
{{Infobox film
| name           = The Nuthouse
| image          =
| caption        =
| director       = Hasse Ekman
| producer       =
| writer         = Hasse Ekman
| narrator       =
| starring       = Hasse Ekman Lars Hanson Elsie Albiin Gunnar Sjöberg
| music          = Sune Waldimir
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 77 minutes
| country        = Sweden Swedish
| budget         =
| gross          =
}}
 Swedish comedy comedy film directed by Hasse Ekman.

==Plot summary==
During excavations in Stockholm in the year 2248 45,000 feet film from the 1940s by master director Hasse Ekman is found. The material is in disarray but the Society for Ancient Film Research compiles material that is believed to master his artistic intentions. 

The story is compiled from several of Hasse Ekmans 40s movies. Among them: Första divisionen, Flames in the Dark, Ombyte av tåg, Excellensen, Kungliga patrasket, Fram för lilla Märta, Lilla Märta kommer tillbaka and Banketten. 

==Cast==
*Hasse Ekman as Hans Hasseson Ekman/second lieutenant Bråde/Kim/Kurre
*Lars Hanson as Colonel von Blankenau 
*Elsie Albiin as Elisabeth, the Colonels daughter 
*Gunnar Sjöberg as Captain Hansson, Chairman in Lillköpings Homofilurer 
*Edvin Adolphson as Göran Dahl, Physical training instructor at Lillköpings boarding school 
*Stig Järrel as Birger Sjögren, teacher in dead languages at the boarding school
*Inga Tidblad as Eva, maladjusted housewife, Birgers wife
*Hilda Borgström as Charlotta Dahl, widow, owner of Lillköpings Bryggerier 
*Sonja Wigert as Inga, Charlottas granddaughter 
*Eva Henning as Monika, daughter to art photographer Gottfrid
*Douglas Håge as Lillköpings Mayor/Gottfrid, dirty old man, his brother
*Ester Roeck-Hansen as Olga, ex. waitress, Monikas mother 
*Thor Modéen as Vårby, Dahls right-hand man, escaped mental patient 
*Birger Malmsten as Kurre, physiotherapist, Charlottas foster son 
*Ernst Brunman as Sergeant Bottin/Editor at Lillköpingsposten  Ernst Eklund as Manfred Gripe, works manager in the homeopathic firm Sjövall & Holm, black-marketer
*Sture Lagerwall as	Jocke Grip, lieutenant at Anti-aircraft batteries and Manfreds son, pimp in his civilian life
*Katie Rolfsen as Viran Blomster, cleaning woman at the City hotel, bastard daughter to Gripe
*Ragnar Falck as Birger Rask, plane spotter, spy for unfriendly power
*Gunn Wållgren as Maggan Håkansson, traveling in drugs
*Hugo Björne as Pontus Bråde, pyromaniac and headmaster at Lillköpings boarding school, second lieutenant Brådes father
*Linnéa Hillberg as	Mrs. Bråde, anonymous letter correspondent, second lieutenant Brådes mother
*Åke Fridell as Chairman in the Society for Ancient Film Research in the prologue
*Harriet Andersson as his secretary in the prologue
*Gustaf Hiort af Ornäs as Curator Brinkebo in the Society for Ancient Film Research in the prologue

==External links==
* 

 

 
 
 
 
 


 