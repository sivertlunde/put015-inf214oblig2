Backlash (1986 film)
 
 
{{Infobox film name           = Backlash  image          =  caption        =  producer       = Bill Bennett director  Bill Bennett  writer         = Bill Bennett starring       = David Argue Gia Carides music          = Michael Atkinson Michael Spicer cinematography = Tony Wilson  editing        = Denise Hunter  distributor    = Dendy Films  released       = 1986 (Australia) 26 March 1987 (United States) running time   = 89 minutes country        = Australia language       = English  budget         = AU $225,000 David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p208-211  
}} Bill Bennett.

==Plot==
Police officers Trevor Darling (David Argue) and Nikki Iceton (Gia Carides) escort a young Aboriginal woman Kath (Lydia Miller) to the New South Wales outback to stand trial. After getting stranded in the desert a bond grows between them. By the time they are rescued, both Nikki and Trevor believe Kath is innocent.

==Cast==
*David Argue as Trevor Darling
*Gia Carides as Nikki Iceton
*Lydia Miller as Kath
*Brian Syron as The Executioner

==Production==
Bill Bennett had raised $175,000 from the BBC and ABC to make a documentary about black tracker Jimmy James but was reluctant to proceed. He came up with the idea for the film and wondered if he could use the money to make a feature. Bennett got approval from the tax department and most investors to do this, with J C Williamson Ltd stepping in for the BBC and ABC. The final $50,000 of the budget came from Bennett himself. 

Bennett wanted to add some levity in the material and so cast David Argue, who had impressed him on stage. He was impressed by Gia Carides improvisational skills in theatresports and cast her to act alongside him. Nurse Lydia Miller rounded out the main cast. 

During filming out near Broken Hill Bennett often clashed with David Argue, who quit a week before shooting ended. However he came back and completed the film.  

Much of the dialogue was improvised by the actors on location. 

==Release==
Bennett won 2 awards at the 1987 Cognac Festival du Film Policier for the film.  The film was screened in the Un Certain Regard section at the 1986 Cannes Film Festival.   

Bennett later claimed it was one of the most profitable films he had made.   accessed 17 November 2012 

==References==
 

==External links==
 
 

 
 
 
 
 
 
 


 