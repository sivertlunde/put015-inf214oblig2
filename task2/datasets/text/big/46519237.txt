The Patriot (1938 film)
{{Infobox film
| name = The Patriot
| image =
| image_size =
| caption =
| director = Maurice Tourneur
| producer = Nicolas Farkas
| writer =  Alfred Neumann  (novel)   Henri Jeanson    
| narrator =
| starring = Harry Baur   Pierre Renoir   Suzy Prim   Jacques Varennes
| music = Jacques Ibert   
| cinematography = Louis Née   Armand Thirard  
| editing =     Roger Mercanton    
| studio = F.C.L. 
| distributor = Films Sonores Tobis
| released =   10 June 1938 
| runtime = 105 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama The Patriot starring Emil Jannings. It was made by the French subsidiary of the German company Tobis Film.

The film portrays the life of Tsar Nicholas I of Russia. 

==Cast==
*  Harry Baur as Le tsar Paul 1er  
* Pierre Renoir as Pahlen  
* Suzy Prim as Anna Ostermann  
* Jacques Varennes as Panine  
* Elmire Vautier as La tsarine   Geller  
* Nicolas Rimsky as Yocov 
* André Carnège as Zoubov 
* Fernand Mailly as Lamiral 
* Jacques Mattler as Le commandant disgrâcié  
* Robert Seller as Narichkine  
* André Varennes as Le ministre de la guerre  
* Victor Vina
* Paula Clère as Lespionne  
* Gérard Landry as Le tsarévitch  
* Colette Darfeuil as Lopouchina  
* Josette Day as Nadia  Jacques Berlioz as Un diplomate  
* Roméo Carlès 
* Marguerite de Morlaye 
* Maurice Devienne
* Ernest Ferny as Le juge

== References ==
 

== Bibliography ==
* Waldman, Harry. Maurice Tourneur: The Life and Films. McFarland, 2001.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 