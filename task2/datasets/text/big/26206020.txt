Meet Me After the Show
{{Infobox Film
| name           = Meet Me After the Show
| image          = Meetmeaftertheshow.JPG
| image_size     = 
| caption        =  Richard Sale George Jessel
| writer         = Scott Darling Erna Lazarus Mary Loos Richard Sale
| narrator       = 
| starring       = Betty Grable Macdonald Carey Rory Calhoun
| music          = Ken Darby
| cinematography = Arthur E. Arling
| editing        = J. Watson Webb, Jr.
| distributor    = Twentieth-Century Fox
| released       = 15 August 1951
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2 million (US rentals) 
}}
 1951 Technicolor musical film starring Betty Grable and released through 20th Century Fox. The film was one of Grables last musical films for Fox during her box office reign of the past decade.

==Plot==
Delilah Lee (Betty Grable) is groomed by her husband Jeff Ames (Macdonald Carey) for his new Broadway show. Delilah becomes such a success to a point where she feels that Jeff thinks of her more as an asset than a wife. When the shows backer Gloria Carstairs (Lois Andrews) begins coming on to Jeff, Delilah leaves him, but regrets it later on and tries to win him back. She devises a scheme involving amnesia to lure Jeff back to her.

==Cast==
* Betty Grable as Delilah Lee
* Macdonald Carey as Jeff Ames
* Rory Calhoun as David Hemingway
* Eddie Albert as Chris Leeds
* Fred Clark as Timothy Tim Wayne
* Lois Andrews as Gloria Carstairs
* Irene Ryan as Tillie

==Production== Wabash Avenue My Blue Heaven.

Rory Calhoun co-starred in this movie. He would also co-star with Betty Grable in How to Marry a Millionaire in 1953 playing her romantic interest in that film.

Meet Me After the Show also features a supporting performance by Irene Ryan later of The Beverly Hillbillies fame. She played Grables maid Tillie.

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 