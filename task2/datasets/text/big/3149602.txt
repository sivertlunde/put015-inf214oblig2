Tube Tales
 
 
{{Infobox Film name            = Tube Tales image           = Tubetales.jpg caption         = DVD Cover director        = producer        = writer  Stephen Hopkins Ed Allen Nick Perry Harsha Patel starring        = music           = cinematography  = Sue Gibson Brian Tufano David Johnson editing         = Liz Green Niven Howie distributor     = Amuse Video Inc. released        = 16 November 1999 (UK) runtime         = 84 minutes language        = English budget          =
|}}
 Time Out Stephen Hopkins, Richard Jobson. It is produced by Force Four Entertainment. Though playing a small role, it was Simon Peggs first film.

==The films==
In order of screening:

===Mr Cool===
Director: Amy Jenkins 
Writer: Amy Jenkins 
Originator: Sue Smallwood 
Starring: Jason Flemyng, Dexter Fletcher and Kelly Macdonald 
Synopsis: After failing to impress his dream girl, Mr Cool suffers the embarrassment of becoming trapped on a train to nowhere.  
Cast: (in alphabetical order) Jason Flemyng (Luke); Dexter Fletcher (Joe); Kelly Macdonald (Emma).

===Horny=== Stephen Hopkins 
Writer: Stephen Hopkins 
Originator: Alex Piro  Tom Bell 
Synopsis: A young woman uses her sexuality to avenge a businessmans sleazy desire of her.  Liz Smith (Old Lady); Tom Bell (Old Gent); Leah Fitzgerald (Little Girl).

===Grasshopper===
Director: Menhaj Huda 
Writer: Harsha Patel 
Originator: Gary Dellaway 
Starring:Dele Johnson, Ray Panthaki and Stephen Da Costa  fare dodger, only to discover their target is not quite what he seemed, in a bizarre case of mistaken identity. 
Cast: (in alphabetical order) Stephen Da Costa (Mr X); Alicya Eyo (Shantel); Roger Griffiths (Charlie); Dele Johnson (Stevie); Preeya Kalidas (Reena); Peter McNamara (Roy); Mazhar Munir (Mazaar); Raiyo Panthaki (Mo); Ashish Raja (Bulla); Marcia Rose (Miss Clinique); Jake Wood (James).

===My Father the Liar===
Director: Bob Hoskins 
Writer: Paul Fraser 
Originator: Christine Barry 
Starring: Ray Winstone and Tom Watson  
Synopsis: A young boy and his father witness an incident that causes the father to lie to his son, to protect him. 
Cast: (in alphabetical order) Edna Doré (Bag Lady); Frank Harper (Ticket Inspector); William Hoyland (Suicide Victim); Richard Jobson (Vendor); Tom Watson (The Son); Ray Winstone (The Father).

===Bone===
Director: Ewan McGregor 
Writer: Mark Greig 
Originator: Sam Taggart 
Starring: Nicholas Tennant and Kay Curram 
Synopsis: A musician invents a fantasy world surrounding the owner of a lost travel card displayed in the window of the ticket office. 
Cast: (in alphabetical order) Corrinne Charton (Pot Plant Lady); Kay Curram (Louise); Joe Duttine (Flamboyant Soloist); Douglas L. Mellor (Concert Hall Manager); Nicholas Tennant (Gordon).

===Mouth===
Director: Armando Iannucci 
Writer: Armando Iannucci 
Originator: Peter Hart 
Starring: Daniela Nardini 
Synopsis: A crowded compartments attentions are drawn to an attractive well-groomed woman, but she doesnt quite live up to their individual expectations. 
Cast: (in alphabetical order) Helen Coker (Bride To Be); Mark Frost (Dude); Sky Glover (Girlfriend); Simon Greenall (Business Man); Dominic Holland (Cello Player); Daniela Nardini (Heroine); Matthew Xia (Boyfriend).

===A Bird in the Hand===
Director: Jude Law  Ed Allen 
Originator: Jim Sillavan 
Starring: Alan Miller 
Synopsis: When a trapped bird stuns itself on a window, a couple of passengers debate the birds fate before an elderly man liberates it above ground. 
Cast: (in alphabetical order) Ed Allen (Youth 2); Frank Harper (Station Guard); Morgan Jones (Youth 1); Alan Miller (Old Man); Cleo Sylvestre (Woman).

===Rosebud===
Director: Gaby Dellal 
Writers: Gaby Dellal an Atalanta Goulandris 
Originator: Tracey Finch 
Starring: Rachel Weisz 
Synopsis: A mother is separated from her daughter and experiences agonising panic as she searches for her, whilst the child discovers a surreal wonderland. 
Cast: (in alphabetical order) Joao Costa Menezes (?); Doña Croll (Elizabeth); Leonie Elliott (Rosebud); Danny Cerqueira (Station Guard); Frank Harper (Station Guard); Ian Puleston-Davies (Typewriter Man); Rachel Weisz (Angela).

===Steal Away===
Director: Charles McDougall  Nick Perry 
Originator: TJ Austin 
Starring: Hans Matheson and Carmen Ejogo 
Synopsis: Two young opportunists steal a briefcase, which turns out to contain wads of money. Their victim shoots at them, but they seem to escape unscathed onto an unused platform where they board a mysterious train. A preacher reads from the bible and a young boy washes passengers feet. Leaving the train the couple leave the money with a tramp before heading to the exit where only one of their tickets is valid.  Jim Carter (Ticket Inspector); Emma Cunniffe (Drained Young Woman); Clint Dyer (Walkman Boy); Jello Edwards (Middle Aged Woman); Annette Ekblom (Boys Mum); Carmen Ejogo (Girl); Simon Kunz (White Pinstripe Suit Man); Hans Matheson (Michael); Simon Pegg (Clerk); Sean Pertwee (Driver); Lee Ross (Male Reveller); Don Warrington (Preacher).

==References and notes==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 