Boggy Creek
 
{{Infobox film
| name           = Boggy Creek
| image          =
| alt            =
| caption        =
| director       = Brian T. Jaynes
| producer       = Brian T. Jaynes François Frizat Christian Remde
| writer         = Jennifer Minar-Jaynes
| starring       = Texas Battle Stephanie Honoré Damon Lipari Shavon Kirksey Melissa Carnell
| music          = Brandon Bentli
| cinematography = François Frizat
| editing        = Brian T. Jaynes Christian Remde
| studio         = Boggy Creek LLC Inkbug Entertainment
| distributor    =
| released       =   }}
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Boggy Creek (also Boggy Creek: The Legend Is True) is a 2011 American horror film directed by Brian T. Jaynes, written by Jennifer Minar-Jaynes, and starring Texas Battle, Stephanie Honoré, Damon Lipari, Shavon Kirksey, and Melissa Carnell as college students attacked by legendary creatures that resemble Bigfoot.

== Plot ==
 
Jennifer takes several of her friends to a remote cabin in Texas where her father died.  There, locals warn them of hostile creatures that, according to legend, murder the men and abduct the women.  The creatures, which resemble the legendary Bigfoot, eventually show up and attack Jennifer and her friends.

== Cast ==
* Texas Battle as Tommy Davis
* Stephanie Honoré as Brooke Tyler
* Damon Lipari as Dave Marshall
* Shavon Kirksey as Maya Jones
* Melissa Carnell as Jennifer Dupree
* Cody Callahan as Casey Guthrie
* Sarah Jenazian as Brittany Sinclair 

== Production == Jefferson  and Uncertain, Texas.   The film was part of a dispute over funding between the director and an early investor.  The matter was eventually taken to court. 

== Release ==
 
Boggy Creek premiered at the sixth Texas Frightmare Weekend in April 2011.   It was released on DVD September 13, 2011. 

== Reception ==
 
Scott Foy of Dread Central rated it 1/5 stars and wrote, "Boggy Creek doesnt work as drama, doesnt deliver as horror, and is no fun at all to watch."   Paul Doro of Shock Till You Drop called it "low-rent amateur hour all around". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 