Stuart Saves His Family
{{Infobox film
| name = Stuart Saves His Family
| image = Stuartsaveshisfamily.jpg
| caption = Theatrical Release Poster
| writer = Al Franken
| starring = Al Franken Laura San Giacomo Vincent DOnofrio Shirley Knight Lesley Boone Harris Yulin
| director = Harold Ramis
| editing = Craig Herring Pembroke Herring
| cinematographer = Hank Goldfritz
| music    = Marc Shaiman
| producer = Trevor Albert Lorne Michaels C.O. Erickson Dinah Minot Whitney White
| studio = Constellation Films
| distributor = Paramount Pictures
| released =  
| runtime = 99 minutes
| language = English
| budget = $6.3 million 
| gross = $912,082
}}
Stuart Saves His Family is a 1995 comedy film directed by Harold Ramis, and based on a series of Saturday Night Live sketches from the early to mid-1990s.   The movie tracks the adventures of would-be self-help guru Stuart Smalley, a creation of comedian Al Franken, as he attempts to save both his deeply troubled family and his low-rated Public-access television show. Some of the plot is inspired by Frankens book, Im Good Enough, Im Smart Enough, and Doggone It, People Like Me!: Daily Affirmations By Stuart Smalley.

The film was produced by Lorne Michaels. Co-stars include Laura San Giacomo, Vincent DOnofrio, Shirley Knight, Lesley Boone and Harris Yulin. Julia Sweeney, Joe Flaherty, Robin Duke, Richard Riehle, future WWE ring announcer Justin Roberts and Kurt Fuller have cameo roles.

==Plot==
Stuart Smalley (Al Franken), the disciple of the 12 step program, is challenged by lifes injustices. He loses his Public Access Cable Television Show, must beg his manipulative overbearing boss for his job back, rehabilitate his alcoholic father and drug abuser brother (Vincent DOnofrio), and support his over-weight mother (Shirley Knight) and sister (Lesley Boone) in their lack of ability in handling their relationships with their husbands. Stuart is supported by his 12 step sponsors as he regresses to his negative behaviors each time he faces these challenges.

==Cast==
*Al Franken as Stuart Smalley
*Laura San Giacomo as Julia
*Vincent DOnofrio as Donnie
*Shirley Knight as Mom
*Lesley Boone as Jodie
*Harris Yulin as Dad
*Julia Sweeney as Mea C.
*Joe Flaherty as Cousin Ray

==Reception==
The film was a huge failure at the box office, earning only $912,082.  This followed the box-office failures of other SNL-adaptations. It also received many negative reviews, with a rotten rating of 28% Rotten Tomatoes. Rotten Tomatoes entry for  . 
 Bad Boys) over his.
 thumbs up" rating, with Siskel calling it "smart and hip" and Ebert saying that "it has more courage than a lot of serious films."  The film also received good reviews from The Washington Post, Deseret News, and the Chicago Reader.   
 depression following the films failure in his 2003 book, Oh, the Things I Know! A Guide to Success, or Failing That, Happiness. In a 1999 appearance on the Howard Stern show, Franken stated that he was "very proud" of the movie.

==Home Video Release==
Stuart Saves His Family was released on VHS in October 1995;  it was released on DVD on April 17, 2001.  In 2007, the film was packaged with two other Lorne Michaels productions, Waynes World (film)|Waynes World and Coneheads (film)|Coneheads, to be sold as a "triple feature".  In 2013, Warner Bros. acquired the management of Paramounts DVD library, and added "Stuart Saves his Family" to their Warner Archive Collection.

==References==
 

==External links==
*  
*   at Rotten Tomatoes
*  
*  
*   of the movie by Roger Ebert

 
 
 
 

 
 
 
 
 
 
 
 
 