Boccaccio (1940 film)
{{Infobox film
| name = Boccaccio 
| image =
| image_size =
| caption =
| director = Marcello Albani
| producer = Ettore Presutti 
| writer =  Maria Basaglia   Luigi Bonelli   Max Calandri   Michele Galdieri   Filippo Masoero   Marcello Albani
| starring = Clara Calamai   Osvaldo Valenti   Silvana Jachino   Luigi Almirante 
| music = Tarcisio Fusco  
| cinematography = Massimo Terzano 
| editing = Dolores Tamburini  
| studio =   Scalera Film 
| distributor = Scalera Film 
| released = 22 September 1940 
| runtime = 80 minutes
| country = Italy Italian 
| budget =
| gross =
}}
Boccaccio is a 1940 Italian operetta film directed by Marcello Albani and starring Clara Calamai, Osvaldo Valenti and Silvana Jachino. It is based on the 1879 operetta Boccaccio (operetta)|Boccaccio by Franz von Suppé.  It was made at the  Scalera Studios in Rome.

==Cast==
* Clara Calamai as Giannina, falso Boccaccio  
* Osvaldo Valenti as Berto  
* Silvana Jachino as Fiammetta  
* Luigi Almirante as Maestro Scalza  
* Osvaldo Genazzani as Il principe di Parnormo  
* Virgilio Riento as Il bottaro  
* Anita Farra as Beatrice  
* Bice Parisi as Peronella  
* Nera Novella as Isabella  
* Raffaele di Napoli as Il duca di Calabria  
* Rudi Dal Pra as Leonetto  
* Amilcare Pettinelli as Il Siniscalco 
* Daniella Drei as Una ancella  
* Gino Bianchi as Lambertuccio  
* Dino De Laurentiis as Uno degli studenti con la penna  

== References ==
 

== Bibliography ==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999. 
 
== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 