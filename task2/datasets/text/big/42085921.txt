Strings (2012 film)
{{Infobox film
| name           = Strings
| image          = 
| caption        = 
| director       = Rob Savage
| producer       = Nathan Craig, Rob Savage    
| screenplay     = Rob Savage
| starring       = Philine Lembeck Oliver Malam Hannah Wilder
| music          = Johnny Clyde Thom Robson Robin Schlochtermeier
| cinematography = Rob Savage 
| editing        = Rob Savage
| studio         = Idle Films
| distributor    = Vertigo Films
| released       =  
| runtime        = 89 mins
| country        = United Kingdom
| language       = English
| budget         = £3000
| gross          = 
}}
Strings is a 2012 British drama film directed and written by Rob Savage. After a successful run in a number of film festivals,  it went on to win the Raindance Award at the British Independent Film Awards. Applauded for its success despite its low budget, the total cost of production was just £3,000.  Director, Writer, Cinematographer and Editor, Savage took strong commitments to get the film produced including lack of sleep to the point of hallucinating. 

==Plot==
The story follows four young people and their romantic endeavours in that blissful summer before they leave home to go to university. Grace (Philine Lembeck) is a visiting German student, soon to return home. She falls for the quiet, distant Jon (Oliver Malam), whilst her best friend Scout (Hannah Wilder) becomes entangled in an increasingly violent relationship with her long-term boyfriend Chris (Akbar Ali).

==Cast==
* Philine Lembeck as Grace
* Oliver Malam as Jon
* Hannah Wilder as Scout
* Sid Akbar Ali as Chris
* Giorgio Spiegelfeld as Giorgio
* Sandra Hüller as Mother
* Durassie Kiangangu as Michael

==Production==

===Writing===
Savage was originally working on the short film Sit In Silence when he watched the film Requiem. He was so moved by the lead actresses (Sandra Hüller) performance that he contacted her when they met in Munich. After that Hüller introduced Savage to another actress she was mentoring at the time: Philine Lembeck. After that Savage began re-writing the script and spent an entire year re-writing it. 

===Filming===
Strings was shot on a Sony FX-1 with an SG Blade 35mm Lens.  Filming took roughly a month. 

===Distribution===
The film won the Raindance Award at British Independent Film Awards.  It was there, where it was recognised by Vertigo Films. Vertigo Films picked up the film on the 5th December 2012  

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 