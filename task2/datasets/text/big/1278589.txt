The Marine
 Marine}}  
{{Infobox film name           =The Marine image          =The_marine.jpg caption        =Poster for The Marine director       =John Bonito producer       =Vince McMahon Joel Simon writer         =Michelle Gallagher Alan B. McElroy starring       =John Cena Robert Patrick Kelly Carlson music  Don Davis cinematography =David Eggby editing        = studio  WWE Films   20th Century Fox Pacific Film and Television Commission distributor    =20th Century Fox released       =  
| country       = United States
| language      = English runtime        =92 minutes budget         =$15 million     gross          =$22,165,608  
}}
The Marine is a 2006 American action film directed by John Bonito. The film stars John Cena, Robert Patrick and Kelly Carlson. It was produced by the films division of WWE, called WWE Studios, and distributed in the United States by 20th Century Fox.

==Plot== honorably discharged.

Home from Iraq, Triton finds it hard to settle back in to normal life. He is fired from his job as a security guard for using excessive force on an employees ex-boyfriend and his bodyguards. Tritons wife Kate ( ), Vescera (Damon Gibson), and Bennett (Manu Bennett). Rome is in collusion with an anonymous partner, with whom he is planning on sharing the profits from the diamonds. On the run, the gang stops at a gas station where Triton and Kate have stopped. When two policemen arrive to buy gas, Morgan shoots and kills one of the officers, causing Rome to shoot  the other officer while Angela kills the gas station attendant. When Triton reacts to Kate being kidnapping|kidnapped, Bennett knocks him out. Triton regains consciousness and gives chase in the policemens car. The chase leads to a lake, where Triton falls out of the patrol car and into the lake, seemingly to his death.

Rome and his gang walk through a swamp to avoid the police. Kate tries to escape several times. Triton emerges from the lake to find Detective Van Buren, who is pursuing the gang.  Van Buren denies Triton permission to pursue the gang, but Triton heads into the swamp anyway. After an altercation between Morgan and Vescera, Rome decides to kill Vescera. Rome gets a call from his anonymous partner, and Rome tells him that he intends to cut the partner out of the deal.

The gang arrives at a lodge and decide to rest there for the time being. Meanwhile, Triton is kidnapped by two fugitives who believe he is a police officer looking for them. He subdues them and tracks the gang to the lodge. He kills Morgan and Bennett then drags the bodies under the lodge, where he again meets Detective Van Buren.

Kate rushes out of the lodge, but Angela attacks and recaptures her. Meanwhile, Triton enters the lodge and finds himself face-to-face with Rome and his gun. Van Buren enters the room but points his gun at Triton, revealing himself to be the anonymous partner. Rome opens fire on Triton, who uses Van Buren as a human shield, killing him. Rome makes his escape and joins up with Angela and Kate before firing at a gasoline tank and blowing up the lodge. Triton makes a narrow escape, having been blown into the swamp.

Rome escapes in Van Burens car, but abandons it due to a police tracking device. Angela seduces then kills a truck driver for his truck. Triton is arrested by a marine patrol officer, but steals the officers vessel after handcuffing him. He races to the marina that is Romes destination, jumping on Romes truck, throwing Angela into the windshield of an oncoming bus, killing her and spilling the diamonds. Rome scrapes Triton off the truck by driving into the side of a building, careening through a warehouse, then leaps out just before the truck crashes into a lake. Triton kills Rome in the fiery warehouse, then rescues Kate, who is drowning in the truck. A badly-burned Rome returns and tries to choke Triton with a chain. Triton kills Rome by breaking his neck with the chain. The film ends with Triton and Kate kissing as the police arrive.

==Cast==
*John Cena as John Triton
*Robert Patrick as Rome
*Kelly Carlson as Kate Triton
*Anthony Ray Parker as Morgan
*Abigail Bianca as Angela
*Damon Gibson as Vescera
*Manu Bennett as Bennett
*Jerome Ehlers as Van Buren
*Drew Powell as Joe

==Production==
  Steve Austin as hero Triton. After Austin and WWE parted ways in 2004, Randy Orton was set to be the main character. Orton turned down the role because of his bad conduct discharge from the Marines in the late 1990s, and was replaced by John Cena.   Al Pacino turned down the role of Rome due to the low salary offered. After Pacino turned it down, Ray Liotta was considered for the role of Rome, but Robert Patrick got the part instead.
 storyline was Carlito beat bodyguard Aaron Aguilera|Jesús and was taking time off to recover.

Filming was done at Movie World Studios in Gold Coast, Queensland, Australia. The Marine Base was filmed at Bond University. The part in which skyscrapers are shown is the downtown area of Brisbane, Australia.  Other footage was shot in bush land surrounding Brisbane.  The opening segment of the downtown scenes was filmed in Calgary, Alberta, Canada.

==Release== The Rocks Gridiron Gang was released September 15). It was released to theaters on October 13, 2006.

===Box office===
In its first week, the film made approximately $15.1 million at the United States box office.    In its first weekend, it placed #3 in domestic s
After a total of ten weeks in theaters, the film grossed $18.8 million domestically. 
 See No Evil, starring Kane (wrestler)|Kane, and its successor The Condemned, starring Stone Cold Steve Austin.
 The Call, starring Halle Berry and WWE wrestler David Otunga.

===Critical response===
The film received generally negative reviews, holding a 20% rating on Rotten Tomatoes based on 46 reviews, with the sites general consensus stating "Overblown in every possible way, The Marine is either so bad its good or just really, really bad."

===Home media===
The film was released on DVD on January 30, 2007. It is available in "Rated" and "Unrated" versions. The Blu-ray Disc|Blu-ray version was released on February 13, 2007, with a DTS-HD Lossless Master Audio 5.1 track and a 1080p HD transfer. The Region 2 DVD was released in the UK on May 7, 2007.

The Marine was the number one most rented DVD in its first week of release.  By March 2007, it had made over $24 million in home sales and rental.  Overall, the film made $30 million in rentals in the first twelve weeks. 

==Sequels==
The film spawned two direct-to-video sequels:  , starring   has been announced, with The Miz set to reprise his role.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 