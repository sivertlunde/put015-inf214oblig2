Bridge to the Sun
{{Infobox film
| name           = Bridge to the Sun
| image size     =
| image	=	Bridge to the Sun FilmPoster.jpeg
| caption        = Etienne Périer
| producer       = Jacques Bar Charles Kaufman based on = the autobiography by Gwendolen Terasaki
| narrator       = Carroll Baker
| starring       = Carroll Baker James Shigeta Tetsurō Tamba Sean Garrison
| music          = Georges Auric
| cinematography =
| editing        = studio = Cite Films
| distributor    = MGM
| released       = 1961
| runtime        = 113 min.
| country        = France / U.S.A. English
| budget         =
| preceded by    =
| followed by    =
}}
Bridge to the Sun is a 1961 film, directed by Etienne Périer, starring Carroll Baker, James Shigeta, James Yagi, Tetsuro Tamba, and Sean Garrison. It is based on the 1957 autobiography Bridge To The Sun by Gwen Terasaki, which detailed events in Gwens life and marriage. 

==Plot==
An American girl falls in love with a Japanese diplomat.

==Cast==
*Carroll Baker as Gwen Terasaki
*James Shigeta as Hidenari Terasaki
*James Yagi as Hara
*Tetsuro Tamba as Jiro
*Sean Garrison as Fred Tyson
*Ruth Masters as Aunt Peggy
*Nori Elisabeth aHermann
*Emi Florence Hirsch as Mako Terasaki, at different ages
*Hiroshi Tomono as Ishi

==Production== First Secretary Pearl Harbor was bombed, was one of the staff who helped translate the Japanese declaration of war and delivered it (late) to the U.S. government and (Mrs. Terasaki wrote in her memoirs) earlier sent secret messages to Japanese pacifists seeking to avert war. The couple and their daughter Mariko were like all Axis diplomats interned in 1942 and repatriated via neutral Angola later that year. Terasaki held various posts in the Japanese Foreign Affairs department up to 1945 when he became an advisor to the Emperor of Japan|Emperor, and was the official liaison between the Palace and General Douglas MacArthur, the Supreme Allied Commander.

Mariko and her mother left Japan in 1949 so Mariko could attend college in Tennessee. Terry died in 1951 in Japan; he was 50 years old.

==Release==
The film was a financial flop.

==References==
 

==External links==
* 
* 
* 
* 
*  at New York Times
*  at TCMDB

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 