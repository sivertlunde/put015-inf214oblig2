The Paleface (1948 film)
{{Infobox film
| name           = The Paleface
| image          = Poster - Paleface, The (1948) 01.jpg
| image size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Norman Z. McLeod
| producer       = Robert L. Welch Jack Rose Melville Shavelson
| based on       =  Edmund Hartmann Frank Tashlin
| narrator       =  Robert Armstrong
| music          = Victor Young
| cinematography = Ray Rennahan
| editing        = Ellsworth Hoagland
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 91 min.
| country        = United States English
| budget         = 
| gross          = $4.5 million (US/ Canada rentals)  
}}
 Western directed by Norman Z. McLeod, starring Bob Hope as "Painless Potter" and Jane Russell as Calamity Jane.  In the film, Hope sings the song "Buttons and Bows" (by Jay Livingston and Ray Evans), which became his greatest hit by far when it came to record sales. The song also won the Academy Award for Best Song that year. 

The film had a sequel, Son of Paleface, in 1952. In 1968,  Don Knotts remade the film as The Shakiest Gun in the West.

==Plot==
Peter "Painless" Potter (Bob Hope) is a dentist of doubtful competence. Out west, after the partner of Calamity Jane (Jane Russell) is killed while trying to discover whos been illegally selling guns to Indians, the cowardly Painless ends up married to Jane, who needs to keep her true identity a secret.

One day while protecting everyone during a hold-up, Jane gives all the credit to Painless, who becomes the townsfolks "brave" new hero.

==Cast==
* Bob Hope as Painless Potter
* Jane Russell as Calamity Jane Robert Armstrong as Terris
* Iris Adrian as Pepper Bobby Watson as Toby Preston (as Robert Watson)  
* Jackie Searl as Jasper Martin (as Jack Searl)  
* Joseph Vitale as Indian Scout  
* Charles Trowbridge as Gov. Johnson  
* Clem Bevans as Hank Billings  
* Jeff York as Big Joe  
* Stanley Andrews as Commissioner Emerson  
* Wade Crosby as Jeb  
* Chief Yowlachie as Chief Yellow Feather  
* Iron Eyes Cody as Chief Iron Eyes   John Maxwell as Village gossip

==References==
 

==External links==
*  
*  
*  	

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 