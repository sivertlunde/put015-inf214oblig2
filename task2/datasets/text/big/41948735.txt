Gadis jang Terdjoeal
{{Infobox film
| name           = Gadis jang Terdjoeal
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = The Teng Chun
| producer       =The Teng Chun
| screenplay     =
| narrator       = 
| starring       =
| music          = 
| cinematography =
| editing        = 
| studio         = Java Industrial Film
| distributor    = 
| released       =    
| runtime        = 
| country        = Dutch East Indies
| language       = Indonesian
| budget         = 
| gross          = 
}} native interests.

==Plot==
Han Nio is in love with Oey Koen Beng. However, Han Nios mother &ndash; hoping for a rich son-in-law to feed her gambling habit &ndash; arranges for Han Nio to marry a rich young man named Lim Goan Tek. Though they have a daughter, their life together is unhappy, and ultimately Goan Tek accuses of Han Nio of stealing from him and runs her out of the house. She falls ill and dies soon afterwards, but not before meeting Koen Beng. Learning of how his former lover had been treated, Koen Beng seeks out Goan Tek. However, before he can have his revenge, Han Nios brother Eng Swan &ndash; the real thief &ndash; shoots Goan Tek, killing him. 

==Production== Boenga Roos ethnic Chinese native audiences and began directing works with more modern stories which suited their interests, although characters remained Chinese. 

The cast and crew of this black-and-white film is otherwise not recorded. 

==Release and reception==
Gadis jang Terdjoeal was released around 1937. The film is likely lost film|lost. Movies in the Indies were recorded on highly flammable nitrate film, and after a fire destroyed much of Produksi Film Negaras warehouse in 1952, old films shot on nitrate were deliberately destroyed.  As such, American visual anthropologist Karl G. Heider suggests that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and film historian Misbach Yusa Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==References==
 

==Works cited==
 
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite book
 |title=Indonesia dalam Arus Sejarah: Masa Pergerakan Kebangsaan
 |trans_title=Indonesia in the Flow of Time: The Nationalist Movement
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |chapter=Film di Masa Kolonial
 |trans_chapter=Film in the Colonial Period
 |author-link=Misbach Yusa Biran
 |publisher=Ministry of Education and Culture
 |year=2012
 |volume=V
 |pages=268–93
 |isbn=978-979-9226-97-6
 |ref=harv
}}
* {{cite web
  | title = Gadis jang Terdjoeal
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-g018-37-644243_gadis-jang-terdjoeal
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 24 July 2012
  | archiveurl = http://www.webcitation.org/69ONhlenU
  | archivedate = 24 July 2012
  | ref =  
  }}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
 

 

 
 
 