Duck, You Sucker!
{{Infobox film
| name = Duck, You Sucker!  (Giù la testa) 
| image = File:DuckYouSuckerPoster.jpg
| caption = US theatrical poster by Robert McGinnis, with the original title
| director = Sergio Leone
| producer = Fulvio Morsella
| screenplay = Luciano Vincenzoni Sergio Donati Sergio Leone Dialogue: Roberto De Leonardis Carlo Tritto
| story =  Sergio Leone Sergio Donati
| starring = Rod Steiger James Coburn Romolo Valli
| music = Ennio Morricone
| cinematography = Giuseppe Ruzzolini
| editing = Nino Baragli
| studio = Rafran Cinematografica Euro International Film San Miura
| distributor = Euro International Film   United Artists  
| released =   
| runtime = 157 minutes
| country = Italy Spain
| language =Italian English Spanish
| budget =
| gross =4,731,889 admissions (France)   at Box Office Story 
}}
 epic buddy buddy Zapata Zapata Western film directed by Sergio Leone. The film stars Rod Steiger and James Coburn.

It is the second part of a trilogy of epic Leone films including the previous Once Upon a Time in the West and the subsequent Once Upon a Time in America, released thirteen years later. The last western film directed by Leone, it is considered by some to be one of his most overlooked films. 

== Plot == Irish Republican explosives expert, who is working in Mexico as a silver mine rock blasting|blaster. Discovering his skill with dynamite and nitroglycerine, Juan relentlessly tries to force John to join him on a raid on the Mesa Verde National Bank.

John, in the meantime, has made contact with the revolutionaries and intends to use his explosives in their service. The bank is hit as part of an orchestrated revolutionary attack on the army organized by physician Dr. Villega. Juan, interested only in the money, is shocked to find that the bank has no funds and instead is used by the army as a political prison. John, Juan and his family end up freeing hundreds of prisoners, causing Juan to become a "great, grand, glorious hero of the revolution."
 British soldiers, making him a fugitive and forcing him to flee Ireland. Juan faces a firing squad of his own, but John arrives and blows up the wall with dynamite just in time. They escape on Johns motorcycle.

John and Juan hide in the animal coach of a train. It stops to pick up the tyrannical Governor Don Jaime, who is fleeing (with a small fortune) from the revolutionary forces belonging to Pancho Villa and Emiliano Zapata. As the train is ambushed, John, as a test of Juans loyalty, lets him choose between shooting the Governor and accepting a bribe from him. Juan kills Jaime, also stealing the Governors spoils. As the doors to the coach open, Juan is greeted by a large crowd and again unexpectedly hailed as a great hero of the revolution, the money taken away by revolutionary General Santerna.

On a train with commanders of the revolution, John and Juan are joined by Dr. Villega, who has escaped. John alone knows of Villegas betrayal. They learn that Pancho Villas forces will be delayed by 24 hours and that an army train carrying 1,000 soldiers and heavy weapons, led by Colonel Reza, will be arriving in a few hours, which will surely overpower the rebel position. John suggests they rig a locomotive with dynamite and send it head on. He requires one other man, but instead of picking Juan, who volunteers, he chooses Dr. Villega. It becomes clear to Villega that John knows of the betrayal. John nonetheless pleads with him to jump off the locomotive before it hits the armys train, but Villega feels guilty and stays on board. John jumps in time and the two trains collide and explode, killing Villega and a number of soldiers.
 turning to the camera and asking forlornly, "What about me?"

== Cast ==
*Rod Steiger as Juan Miranda, an amoral Mexican peon leading a band of outlaws composed mostly of his own children. He does not care about the revolution at first, but becomes involved after his encounter with John.
*James Coburn as John (Seán) H. Mallory, a Fenian revolutionary and explosives expert. Wanted for killing British forces in Ireland, he flees to Mexico where he ends up getting involved in another revolution.
*Romolo Valli as Dr. Villega, a physician and commander of the revolutionary movement of Mesa Verde.
*Maria Monti as Adelita, a wealthy female passenger on the stagecoach robbed by Juan at the beginning of the film.
*Rik Battaglia as General Santerna, a commander leading the Mexican revolutionary army.
*Franco Graziosi as Governor Don Jaime, the corrupt and tyrannical local governor.
*Antoine Saint-John as Colonel Günther "Gutierez" Reza, a ruthless commander leading a detachment of Mexican Army|Federales; the main antagonist of the film.
*Vivienne Chandler as Coleen, Johns girlfriend; appears only in flashbacks.
*David Warbeck as Nolan, Johns best friend, also an Irish nationalist; appears only in flashbacks.

== Production ==

=== Development === treatment of the film. {{cite video
  | title = Duck, You Sucker, AKA A Fistful of Dynamite (2-Disc Collectors Edition, Sergio Donati Remembers)
  | medium = DVD
  | publisher = Metro-Goldwyn-Mayer
  | location = Los Angeles, California
  | date = 1972 political riots The Dreamers, which was set against the backdrop of the Paris riots.) Leone, who had used his previous films to deconstruct the romanticization of the American Old West, decided to use Duck, You Sucker! to deconstruct the romanticized nature of revolution, and to shed light on the political instability of contemporary Italy. {{cite video
  | title = Duck, You Sucker, AKA A Fistful of Dynamite (2-Disc Collectors Edition, The Myth of Revolution)
  | medium = DVD
  | publisher = Metro-Goldwyn-Mayer
  | location = Los Angeles, California
  | date = 1972
}} 

Leone, Donati and Luciano Vincenzoni worked together on the film’s screenplay for three to four weeks, discussing characters and scenes for the film. Donati, who had previously acted as an uncredited script doctor for The Good, the Bad and the Ugly, conceived Juan Miranda’s character as an extension of Tuco from The Good, the Bad and the Ugly. Meanwhile, Leone was largely responsible for the character of John Mallory, and the film’s focus on the development of John and Juan’s friendship.  At times, however, Leone, Donati, and Vincenzoni found that they had highly differing opinions about how the film should be made, with Leone wanting to have the film produced on a large scale with an epic quality, while Donati and Vincenzoni perceived the film as a low-budget thriller.  

Sergio Leone never intended to direct Duck, You Sucker!, and wanted the film to be directed by someone who could replicate his visual style. Peter Bogdanovich, his original choice for director, soon abandoned the film due to perceived lack of control.   Sam Peckinpah then agreed to direct the film after Bogdanovichs departure, only to be turned down for financial reasons by United Artists. Donati and Vincenzoni, noting the directors frequent embellishment of the facts concerning his films, claim that Peckinpah did not even consider it - Donati stated that Peckinpah was "too shrewd to be produced by a fellow director".    Leone then recruited his regular assistant director, Giancarlo Santi, to direct, with Leone supervising proceedings, and Santi was in charge for the first ten days of shooting. However, Rod Steiger refused to play his role as Juan unless Leone himself directed, and the producers pressured him into directing the film. Leone reluctantly agreed, and Santi was relegated to second unit work.   

 
The inspiration for the firing squad scene came from Francisco Goya, and in particular from his set of prints The Disasters of War. Leone showed the prints to director of photography Giuseppe Ruzzolini in order to get the lighting and color effects he wanted.  The film is believed to have been influenced by Peckinpahs The Wild Bunch, and it shares some plot elements with Pat Garrett and Billy the Kid, a western film also starring Coburn and released a year later.  Leone biographer and film historian Sir Christopher Frayling noted that Duck, You Sucker! was made in a period of Italian cinema where filmmakers were ‘rethinking’ their relationship with fascism and the Nazi occupation of Rome. He has identified numerous references to both World Wars in the film, such as Colonel Reza’s commanding of an armored car resembling a Nazi tank commander, the massacre of Juan’s family (which bares similarities to the Ardeatine massacre of 1944), and an execution victim resembling Benito Mussolini. 

=== Casting === same character he had already played in the Dollars Trilogy, and he also wanted to end his association with the Italian film industry. As a result, he declined the offer and starred in Hang Em High instead.  George Lazenby was then approached to play John, but he declined. A young Malcolm McDowell, then mostly known for his performance in if...., was considered for both John and Nolan, John’s Irish friend, but Leone eventually settled on James Coburn to play John.   Coburn had previously been considered for other Leone projects, including A Fistful of Dollars and Once Upon a Time in the West.

The role of Juan Miranda was written for Eli Wallach, based on his performance of Tuco in The Good, the Bad and the Ugly, but Wallach had already committed to another project with Jean-Paul Belmondo. After Leone begged Wallach to play the part, he dropped out of the other project to play Juan. However, the studio never wanted Wallach; they wanted an actor who had more international appeal, and Rod Steiger had already been signed on by that point. Leone offered no compensation to Wallach, and Wallach subsequently sued.  

Leone was initially dissatisfied with Steigers performance in that he played his character as a serious, Emiliano Zapata|Zapata-like figure.  As a result, tensions rose between Steiger and Leone numerous times, including an incident that ended with Steiger walking off during the filming of the scene when John destroys Juan’s stagecoach. However, after the film’s completion, Leone and Steiger were content with the final result, and Steiger was known to praise Leone for his skills as a director. 

=== Filming ===
Exterior filming mostly took place in Andalucía, Spain. Some of the locations used previously featured in Leones Dollar Trilogy films; for example, the Almería Railway Station, used for the train sequence in For a Few Dollars More, returns in this film as Mesa Verdes station. The flashback scenes with Sean and friends were shot at Howth Castle in Dublin, and Toners Pub on Baggot Street, Dublin. {{cite video
  | title = Duck, You Sucker, AKA A Fistful of Dynamite (2-Disc Collectors Edition, Location Comparison)
  | medium = DVD
  | publisher = Metro-Goldwyn-Mayer
  | location = Los Angeles, California
  | date = 1972
}}  As filming progressed, Leone modified the script: as he did not originally plan on directing Duck, You Sucker, he thought the script was "conceived for an American filmmaker".  Duck, You Sucker! was one of the last mainstream films shot in Techniscope. 

=== Music === musical score for Duck, You Sucker! was composed by Ennio Morricone, who collaborated with Leone in all his previous projects. Elvis Mitchell, former film critic for the New York Times, considered it as one of Morricones "most glorious and unforgettable scores".  He also sees "Invention for John", which plays over the opening credits and is essentially the films theme, "as epic and truly wondrous as anything Morricone ever did".  An soundtrack album was released in the United States in 1972,  and many tracks can be found in Morricones compilation albums. Music was recorded in April 1971 and second recording sessions in August/September 1971. A 35th anniversary OST was issued in 2006 with previously never released recording session and alternate takes.

== Themes == political film: Leone himself said that the Mexican Revolution in the film is meant only as a symbol, not as a representation of the real one, and that it was chosen because of its fame and its relationship with cinema, and he contends that the real theme of the film is friendship: 

 
 Edward Banfield and others. 
 Juan José Herrera and Elfego Baca, which Leone may have had in mind in his creation of the character of Juan. 

== Release and reaction ==

=== Performance === lire on its first run. 

It was the fourth most popular movie of the year in France. 
=== Reception === classification and effectively banned until 1979 because it was considered offensive to the Mexican people and the Revolution. 

=== Release history ===
 
The film was originally released in the United States in 1972 as Duck, You Sucker!, and ran for 121 minutes. Many scenes were cut because they were deemed too violent, profane or politically sensitive, including a quote from  . (The expletive coglione (a vulgar way to say "testicle") was later removed to avoid censorship issues.)     One of the working titles, Once Upon a Time… the Revolution,      was also used for some European releases.    {{cite video
  | title = Duck, You Sucker, AKA A Fistful of Dynamite (2-Disc Collectors Edition, Sorting Out the Versions)
  | medium = DVD
  | publisher = Metro-Goldwyn-Mayer
  | location = Los Angeles, California
  | date = 1972
}}  

In 1989, Image Entertainment released the film on laserdisc, including some material cut from the original US version and lasting 138 minutes. This version was released in Europe as Once Upon a Time… the Revolution, again intended to evoke an earlier Leone film, Once Upon a Time in the West. 

Subsequent re-releases have largely used the title A Fistful of Dynamite,   although the DVD appearing in The Sergio Leone Anthology box set, released by MGM in 2007, used the original English language title of Duck, You Sucker!.    

The films first English language DVD was released by MGM in the UK in 2003. This version of the film runs 154 minutes and is almost complete, but it uses a truncated version of the films final four-minute long flashback. In 2005, following the restoration of Leones The Good, the Bad and the Ugly, MGM re-released the film in the UK with more supplemental material, the aforementioned flashback scene reinstated and with a newly created 5.1 surround soundtrack. The restored version had a brief art house theatrical run in the U.S. and was subsequently released there in a "Collectors Edition" in 2007.  The remastered surround tracks have attracted criticism online for the replacement of certain music cues throughout the film (most prominently during the last two flashback scenes and the end credits) and for censoring at least two expletives from the films soundtrack. Furthermore, it has been reported that the mono soundtrack included on the 2007 Collectors Edition is not the original mix, but simply a fold-down of the surround remaster. 
 62nd Cannes Film Festival.    The print used for the festival was restored by the Cineteca di Bologna and the film laboratory Immagine Ritrovata.

On October 7, 2014, MGM released the film on Blu-ray disc, akin to their Blu-ray releases of the Dollars Trilogy. 

== References ==
 

== External links ==
*  
*  
*  
*  at Trailers from Hell
*  at the blog Come Here to Me! about Dublin
 


 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 