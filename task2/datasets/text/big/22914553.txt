The Treasure of Pancho Villa
{{Infobox film
| name           = The Treasure of Pancho Villa
| image          = Trpanvilpos.jpg
| image_size     =
| caption        = Original film poster
| director       = George Sherman
| producer       = Edmund Grainger
| writer         = J. Robert Bren and Gladys Atwater (story)
| starring       = Rory Calhoun Shelley Winters Gilbert Roland William E. Snyder
| music          = Leith Stevens
| editing        = Harry Marker	 	
| distributor    = RKO
| released       =  
| runtime        = 92 mins.
| country        = United States English
| gross = $1.15 million (US) 
}} 1955 western western film directed by George Sherman and starring Rory Calhoun, Shelley Winters and Gilbert Roland. The film was shot on location in Mexico.

==Plot synopsis== Lewis machine gun he names ("La Cucaracha") joins a band of revolutionaries headed by Colonel Juan Castro (Gilbert Roland). Though paid for his services, Bryan is tired of the squalid life he is living in Mexico and is considering offering his services to Cuba.
 artillery piece on a flat car, his men are dubious of success. Castro has two aces up his sleeve; Bryan with his machine gun, who will be disguised as a passenger on the train, and Pablo Morales, an expert dynamiter, who will blow up a bridge separating the two trains. Castro senses treachery by Morales as the two did a robbery years ago with Castro leaving Morales behind, however Castro is reassured by Morales wife that he is a Villista and is not a traitor.

After wiping out the Federales, they steal the gold shipment from the government train, gold that Castro intends to deliver to revolutionary leader Pancho Villa. Pursued by the Mexican Army, they flee to the mountains along with Ruth Harris (Shelley Winters) an American who was living in Mexico as a schoolteacher but who became a soldadera after her father was murdered. Villa and his men do not appear at the rendezvous; however, Morales gains the loyalty of some of Castros band to keep the gold for themselves.  Bryan also wants the gold for himself, wishing to use it to finance his own revolution in another country where he can loot the nation and retire in splendour.  He guns down most of the Mexicans with his Lewis gun in order to keep the gold. 

Morales, Ruth and the others are captured by the Federales, with Morales offering them the gold, Bryan, and Castro in exchange for money and amnesty. Because of her American nationality, Ruth is escorted to Tampico to be deported to America, while the others are to be executed.
 Yaqui Indians working with the government pursuers, Bryan is reconciled to Castro’s ideals, and the two build a gun emplacement out of the bags of gold to make a last stand. The Federales send Morales with a white flag of truce and a hand grenade, but Castro kills him first. Castro is killed by the pursuing Mexicans and Bryan has to blow up the gold in order to prevent them recovering it. This starts an avalanche that buries both the gold and the pursuers. Unarmed and broke, the solitary Bryan makes his way to Tampico to be reunited with Ruth.

As Bryan leaves the place where he has buried Castro, he looks up at the vultures circling and tells them that they have feasted on many things but if they should ever have done so on Castro, they would have feasted on a real man.

==A real treasure==
Pancho Villa had seized a treasure of 122 bars of silver from a train near Chavarria    in 1913; the treasure was allegedly not recovered. http://www.bc-alter.net/dfriesen/mextreasure.html 

==Cast==
*Rory Calhoun - Tom Bryan
*Shelley Winters - Ruth Harris
*Gilbert Roland - Juan Castro
*Joseph Calleia - Pablo Morales
*Fanny Schiller - Laria Morales
*Carlos Múzquiz - Commandant
*Tony Carbajal - Farolito
*Pasquel Pena - Ricardo
*Rodd Redwing - Yaqui tracker

==Quotes==
"Mercenary" is a three dollar word.  Im a hired gun - Tom Bryan

==Notes==
 

==External links==
* 
* 

 

 
 
 
 
 
 