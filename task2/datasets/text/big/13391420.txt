A Chance Deception
 
{{Infobox film
| name           = A Chance Deception
| image          = 
| alt            =  
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = Christy Cabanne
| starring       = Blanche Sweet
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| studio         = 
| distributor    = Biograph Company
| released       =  
| runtime        = 17 minutes (16 Frame rate|frame/s)
| country        = United States
| language       = Silent with English intertitles
| budget         = 
| gross          = 
}}

A Chance Deception is a 1913 American drama film directed by  D. W. Griffith and starring Blanche Sweet.   

==Cast==
* Blanche Sweet - The Wife
* Charles Hill Mailes - The Jealous Husband Harry Carey - Raffles
* Mildred Manning - Raffles Woman John T. Dillon - The Waiter
* Lionel Barrymore - A Policeman
* Dorothy Bernard - The Maid (unconfirmed)
* Christy Cabanne - Undetermined Role
* Adolph Lestina - A Visitor
* Wilfred Lucas - In Restaurant (unconfirmed) Joseph McDermott - A Policeman

==See also==
* List of American films of 1913
* Harry Carey filmography
* D. W. Griffith filmography
* Blanche Sweet filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 