Scott Joplin (film)
 
{{Infobox film
| name           = Scott Joplin 
| image          = 
| image_size     = 
| caption        = 
| director       = Jeremy Kagan
| writer         = 
| narrator       = 
| starring       = Billy Dee Williams
| music          = Scott Joplin, Dick Hyman
| cinematography = 
| editing        = 
| distributor    =  
| released       = December 30, 1977
| runtime        = 96 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Scott Joplin is a 1977 biographical film directed by Jeremy Kagan and based on the life of American composer and pianist Scott Joplin. It stars Billy Dee Williams and Clifton Davis. It won an award from the Writers Guild of America in 1979.  The only other composers mentioned as worthy equals in the film are John Philip Sousa and Jelly Roll Morton.

==Plot==
In the late 19th century, Scott Joplin, a young African-American musician, moves to Missouri and to make ends meet finds a job as a piano teacher. He befriends Louis Chauvin, who plays piano in a brothel.

Joplin composes ragtime music. One day his "Maple Leaf Rag" is heard by John Stark, a publisher of sheet music in St. Louis, Missouri|St. Louis. Stark is impressed, buys the rights to the composition and sells it, with Joplin sharing some of the profits. Joplins new songs also achieve a great popularity.

Chauvin is equally talented, but contracts syphilis and dies in his 20s. Joplin, meantime, becomes obsessed with composing more serious music, yet is continually thwarted in his attempt to write and publish an opera. 

==Cast==
*Billy Dee Williams as Scott Joplin
*Clifton Davis as Louis Chauvin
*Margaret Avery as Belle Joplin
*Eubie Blake as Will Williams
*Godfrey Cambridge as Tom Turpin John Stark

==References==
 

==External links==
*  
*  

 
 

 
 
 
 

 