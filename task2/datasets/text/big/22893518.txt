Heiko
 
 Portuguese short film, directed by David Bonneville and produced by the Calouste Gulbenkian Foundation. It is 13 minutes long.

The film shows the perverse relationship between a 70-year-old aesthete and a young man named Heiko. 

It is part of the DVD compilation Boys on Film 4: Protect Me From What I Want distributed by Peccadillo Pictures.

==Cast==
*José Manuel Mendes
*Jaime Freitas

==Awards==
*Best Short Film Award at the 24th MixCopenhagen International Film Festival (winner)
*Special Mention at the 10th Slamdance Film Festival, Park City, Utah, US (winner)
*RUSHES Soho Shorts Film Festival, UK (nomination)
*Toronto InsideOut Film Festival, Canada (nomination)
*Torino G&L Film Festival (nomination)
*TIFF Transilvania International Film Festival

== External links ==
* 
* 
* 

 
 
 
 
 

 