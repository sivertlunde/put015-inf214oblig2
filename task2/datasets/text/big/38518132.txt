Gulabi Gang (film)
 

{{Infobox film
| name           = Gulabi Gang
| image          = Gulabi Gang Documentary Film Poster.jpg
| caption        = Theatrical Poster
| director       = Nishtha Jain
| screenplay     = 
| producer       = Torstein Grude 
| starring       = 
| music          = Peter Scartabello
| cinematography = Rakhesh Haridas
| editing        = Arjun Gourisaria
| art director   =  
| Distributor    = Kudos Family
| India Distributor = Recyclewala Labs
| language       =Hindi Bundelkhandi
| released       =  
| runtime        = 96 minutes 
}}
 Best Film Best Non-Feature Film editing at the 61st National Film Awards.  

==Synopsis==
In Bundelkhand, India, a revolution is in the making among the poorest of the poor, as Sampat Pal Devi and the fiery women of her Gulabi Gang empower themselves and take up the fight against gender violence, caste oppression and widespread corruption.    They want to change the unchangeable with an organised social action and unification. Its a picture of rural India and a story about underprivileged, especially a story of Indian women.

==Production==
After meeting the leader of the real Gulabi Gang, Sampat Pal, in 2009, director Nishtha Jain decided her story needed to be told. The film was put on hold, however, when they got wind that an UK-based production company had got an exclusivity contract to make a film about the Gulabi Gang (The film was called Pink Saris and was released in 2010).

In February 2010 Nishtha Jain, joined hands with Torstein Grude, Producer, Piraya Film, Norway. Torstein Grude had also independently developed a project on the Gulabi Gang.   

After running a very successful festival circuit and winning several awards including the Best Film, MuhrAsia Documentary at the Dubai International Film Festival, Gulabi Gang found a distributor for India in Sohum Shah and his company Recyclewala Labs. The release was on February 21, 2014. 

==Critical reception==
The documentary opened to wide critical appreciation on its opening day.

  of   (rating: 3.5/5) urged the public to see it stating "I doubt you will see a more important film this year. Gulabi Gang is an ideology, a searing exercise in awareness, which is why you must make this your mandatory watch this weekend."  Mohar Basu of KoiMoi (rating: 3.5/5) said "Jain carries me with her daunting journey that I will hold on to for many years probably. The instances were revolting, the tall talks about women’s morality – a sham and the despite the distressing situations – Gulabi Gang is a ray of hope, brimming with optimism to change the stringent attitudes with galvanizing ferociousness." 

==Awards== National Film Awards
*National Film Award for Best Film on Social Issues for Director Nishtha Jain and Producer(s) Piraya Film, Raintree Films, Final Cut for Real.
*National Film Award for Best Non-Feature Film Editing - Arjun Gourisaria
*Best Director, Mumbai International Film Festival 2014

;International honor
*Best Film MuhrAsia Documentary at Dubai International Film Festival
*Best Film at The Norwegian Film Festival, Grimstad 
*Amnesty International Awards for Human Rights in South Africa and Poland 
*Best Documentary, IAWRT(International Association of Women in Radio and Television) 2013

==Bollywood dramatization== masala film directed by Soumik Sen, titled Gulaab Gang is a dramatization of Sampats Pal story, starring actresses Madhuri Dixit and Juhi Chawla. The film is slated for a release on March 7, 2014. When asked about the Bollywood film, Gulabi Gang director Nishtha Jain stated "I don’t think a commercial movie can capture the colour, nuance and immediacy or the raw and edgy quality of the ‘real’ which you’ll see in my film." 

Producer Anubhav Sinha discredited any source stating that his fictional film was based on Sampat Pals life, stating that "Its a womans fight for womens rights who ends up fighting another woman. The film is not inspired by Sampat Pal at all." 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 