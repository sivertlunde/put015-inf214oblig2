Väter und Söhne – Eine deutsche Tragödie
{{Infobox television film
| name         = Väter und Söhne - Eine deutsche Tragödie
| image        =  
| image_size   = 
| caption      = 
| runtime      = 480  min    
| writer       = Bernhard Sinkel
| director     = Bernhard Sinkel
| producer     =  
| starring     = Burt Lancaster Julie Christie Bruno Ganz Tina Engel 
| editing      = 
| music        = 
| budget       = 
| country      = Italy Germany
| language     = English
| network      = Radiotelevisione Italiana
| released     =  
}}

Väter und Söhne – Eine deutsche Tragödie (1986) is an Italy-Germany TV mini-series, starring Burt Lancaster - Julie Christie and directed by Bernhard Sinkel.

==Plot==
Two German industrial dynasties, one of Jewish bankers and a founder of a chemical empire. For three generations, their plots and plans, loves and betrayals help the rise of Germany and then to total destruction. The Deutz end up in the dock, on trial for war crimes at Nuremberg, while his trusted friends, the Bemheim, with one exception, have been killed

== Cast ==
{|class="wikitable"
|-
!Actor !! Role
|- Burt Lancaster || Geheimrat Carl Julius Deutz    
|- Julie Christie || Charlotte Deutz      
|- Bruno Ganz || Heinrich Beck   
|- Laura Morante || Judith Bernheim           
|- Tina Engel || Luise Deutz   
|- Dieter Laser || Friedrich Deutz      
|- Martin Benrath || Bankier Bernheim    
|- Hannes Jaenicke || Max Bernheim   
|- Herbert Grönemeyer || Georg Deutz    
|-
|Rüdiger Vogler || Ulrich Deutz           
 
|}

== References ==
*Film-Dienst, Vol 58,Number 8-13, by Katholisches Institut für Medieninformation (Germany), Katholische Filmkommission für Deutschland

== External links ==
* 

 
 
 
 


 
 