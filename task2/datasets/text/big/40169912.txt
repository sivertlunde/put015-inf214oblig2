The Right Kind of Wrong (film)
 
{{Infobox film
| name           = The Right Kind of Wrong
| image          = The_Right_Kind_Of_Wrong_Poster.jpg
| caption        = U.S. Theatrical Poster
| director       = Jeremiah S. Chechik
| producer       = Robert Lantos
| writer         = Megan Martin Tim Sandlin
| starring       = Ryan Kwanten
| music          = Rachel Portman
| cinematography = Luc Montpellier
| editing        = 
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 97 minutes
| country        = Canada
| language       = English
| budget         = 
}}

The Right Kind of Wrong is a 2013 Canadian romantic comedy film directed by Jeremiah S. Chechik. Its premiere was in the Gala Presentation section at the 2013 Toronto International Film Festival.   

==Plot==
Leo, a newly divorced man, falls in love with a woman on her wedding day and goes through struggles to win her heart.  Leos own failed marriage was depicted in a blog ("Why You Suck") and then published bestseller written by his ex-wife.  A failed writer himself, Leo is an underachiever who now washes dishes.  Leos pursuit of the newly married Collette requires he finally face the scorn of total strangers who know him only from his wifes book.

==Cast==
* Ryan Kwanten as Leo Palamino
* Sara Canning as Colette
* Catherine OHara as Tess
* Will Sasso as Neil
* Kristen Hager as Julie
* Ryan McPartlin as Danny
* James A. Woods as Troy Cooper
* Barb Mitchell as Angie
* Jennifer Baxter as Jill
* Raoul Bhaneja as Mandeep
* Anna Quick as Chrissy

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 

 
 