Memoria negra
{{Infobox film
| name           = Memoria negra
| image          = 
| caption        = 
| director       = Xavier Montanyà
| producer       = Colomo Producciones Ovideo
| writer         = 
| starring       = 
| distributor    = 
| released       = 2006
| runtime        = 98 minutes
| country        = Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = Xavier Montanyà, Carles Serrat
| cinematography = Ricardo Íscar
| editing        = Domi Parra
| music          = Pablo Cepeda, Jorge Porter-Pidgins
}}

Memoria negra is a 2006 documentary film directed by Xavier Montanyà. 

== Synopsis ==
The voice-over of an anonym Guinean in exile, who inherited a river on his father’s death, remembering, from the distance of exile, episodes of his childhood, popular legends and old African beliefs, introducing us to the troubled past of Equatorial Guinea. This documentary brings out the subject of Spanish colonisation in the African country and the politic, religious and cultural heritage that came to the surface after the independence, starting with the dictatorship of Francisco Macías to the actual regime of Teodoro Obiang Nguema, sustained by the country’s wealth from the oil wells.   

==Screenings==

Memoria negra was selected at the Festival of Valladolid (2006). {{cite web
 |url=http://www.editorialempuries.cat/ca/autor/xavier-montanya_377.html
 |title=Xavier Montanyà
 |publisher= Editorial Empuries
 |accessdate=20122-03-11}}  
It was also exhibited at the African Film Festival of Cordoba and other festivals. {{cite web
 |url=http://www.fcat.es/FCAT/index.php?option=com_zoo&task=item&item_id=541&Itemid=37 
 |title=Memoria negra
 |work=FCAT
 |accessdate=2012-03-11}} 

== References ==
 
 

 
 
 
 
 


 
 