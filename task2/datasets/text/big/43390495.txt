Far from Men
 
{{Infobox film
| name           = Far from Men
| image          = Far-from-men-poster.jpg
| caption        = Film poster
| director       = David Oelhoffen
| producer       = Marc Du Pontavice Matthew Gledhill 
| screenplay     = David Oelhoffen
| based on       =  
| starring       = Viggo Mortensen Reda Kateb Warren Ellis
| cinematography = Guillaume Deffontaines
| editing        = Juliette Welfling
| distributor    = Pathé
| released       =  
| runtime        = 110 minutes
| country        = France
| language       = French Arabic Spanish
| budget         = 
}}

Far from Men ( ) is a 2014 French drama film directed by David Oelhoffen. It was selected to compete for the Golden Lion at the 71st Venice International Film Festival.       It was screened in the Special Presentations section of the 2014 Toronto International Film Festival.   

==Cast==
* Viggo Mortensen as Daru
* Reda Kateb as Mohamed
* Djemel Barek as Slimane 
* Vincent Martin as Balducci  
* Nicolas Giraud as Lieutenant Le Tallec 
* Jean-Jerome Esposito as Francis 
* Hatim Sadiki as Abdelkader 
* Yann Goven as Rene  
* Antoine Régent as Claude 

==Accolades==
{| class="wikitable plainrowheaders sortable"
|- style="background:#ccc; text-align:center;" Award / Film Festival Category
! Recipients and nominees Result
|- 71st Venice International Film Festival 
| SIGNIS Award
|Far from Men
|  
|- Arca CinemaGiovani Award for Best Film of Venezia 71 
|Far from Men
|  
|- Interfilm Award for Promoting Interreligious Dialogue
|Far from Men
|  
|-
|  
| Orchidée dOr for Best Film 
|Far from Men
|  
|-
|  
| Best Actor
| Viggo Mortensen
|  
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 