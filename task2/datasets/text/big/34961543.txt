Liverpool (2008 film)
Liverpool 2008 cinema Argentine drama Kino International on November 30, 2010. The cinematography was by Lucio Bonelli.
 merchant seaman who applies for leave in Ushuaia, Tierra del Fuego to visit his mother in his home village after twenty years away. Farrel is played by Juan Fernández, a native of Ushuaia who drives a snow plow for a living. 

The LA Times called it a "bold, successful attempt at a film narrative in which images are everything and words are few."  The New York Times concluded that "Although it has its visual pleasures, and there’s plenty to admire about his compositions, the journey in “Liverpool” seems comparatively slight".  Variety felt that the " rilliance of the overall conception and execution will immediately hit some viewers, while others may need to mull things over." 

==References==
 

==External links==
*  

 

 
 
 
 

 