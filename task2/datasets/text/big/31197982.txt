Mangamma (film)
{{Infobox film
| name           = Mangamma
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = T. V. Chandran NFDC
| writer         = T. V. Chandran Vijayaraghavan Thilakan Johnson
| cinematography = Sunny Joseph
| editing        = Venugopal
| studio         = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}
 Vijayaraghavan and Thilakan in major roles.

The film met with critical acclaim. T. V. Chandran won the Kerala State Film Award for Best Director. The film won the National Film Award for Best Feature Film in Malayalam. Revathis performance is widely regarded as one of the best in her career.

==Plot== the Emergency, the time of police excesses and political witch-hunting. Due to constant harassment by the local MLA-cum-landlord, Mangamma (Revathi) and her father (Thilakan) are forced to flee their village on the Tamil Nadu-Kerala border. The landlords son attempts to rape Mangammas younger sister but unfortunately they both die in a fire that destroys the hut. Mangamma and her father are offered shelter and a job by Nair (Nedumudi Venu), who runs a tea shop with his adopted son Velayudhan. Nair marries Mangamma after her fathers death.

The later part of the film takes place 16 years later. Mangamma and Nair have a 15-year-old son - Sankaran, who just flirts around with Lucy, a girl who helps Mangamma in her popcorn business. When Velayudhan who has worked with her since childhood disappears one day, the shop is thrown into disarray. Velayudhan (M. G. Sasi) is now a rebel, wanted by the police. A rich contractor wants to buy Nairs land, and his refusal brings about a catastrophe. The police  harasses Mangamma and Nair. Nair becomes one of the many faceless, nameless victims of the Emergency — he dies in police custody. The tea shop is burnt. Standing up to these trials, Mangamma survives. In the last shot, Velayudhan sits in the tea shop. He is reading a book. He remains in the frame even as Mangamma leaves.

==Cast==
* Revathi as Mangamma
* Nedumudi Venu as Nair Vijayaraghavan as Balan, Mangammas ex-lover 
* Thilakan as Karuppan Mooppar, Mangammas father
* M. G. Sasi as Velayudhan, Nairs adopted son
* Oduvil Unnikrishnan as Mannadiyar
* Gopakumar as Varghese Mapla, Nairs friend
* V. K. Sriraman as contractor
* Jagadeesh
* Ravi Vallathol as Dr. M. S. Menon
* James

Other major characters are
* Sankaran, Nair and Mangammas son
* Licy, Varghese Maplas daughter
* Kuttisankara Menon, the wicked landlord and MLA
* Prasad, Kuttisankara Menons son
* Sundari, Mangammas younger sister

==References==
* {{cite web|url=http://www.nfdcindia.com/view_film.php?film_id=58|title=Mangamma (T.V.Chandran/ 1997 / 102 Mins/ Malayalam/ Social)  
|publisher=National Film Development Corporation of India|NFDC|accessdate=April 26, 2011}}
*  
* {{cite web|url=http://www.cscsarchive.org:8081/MediaArchive/art.nsf/(docid)/99955C937985A13A6525694000620189|title=Soul on Fire  
|work=The Indian Express|publisher=cscsarchive.org|date=1998-04-19|accessdate=March 16, 2011}}
* {{cite web|url=http://www.cinemaofmalayalam.net/chandran.html|title=T.V.Chandran  
|publisher=Cinemaofmalayalam.net|accessdate=March 16, 2011}}

==External links==
*  
*   at the Malayalam Movie Database

 
 

 
 
 