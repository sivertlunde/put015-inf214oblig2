After Many Years (1930 film)
{{Infobox film
| name =  After Many Years 
| image =
| image_size =
| caption =
| director = Lawrence Huntington
| producer = Alvin Saxon 
| writer =  Lawrence Huntington 
| narrator =
| starring = Henry Thompson]   Nancy Kenyon   Savoy Havana Band
| music = 
| cinematography = 
| editing = 
| studio = Savana Film
| distributor = Metro-Goldwyn-Mayer
| released = 1930
| runtime = 70 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
After Many Years is a 1930 British crime film directed by Lawrence Huntington and starring Henry Thompson, Nancy Kenyon and the Savoy Havana Band. It was made as a quota quickie for released by the Hollywood studio MGM.  In the film, a murdered policemans son tracks down the murderer in Peru. 

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 

 