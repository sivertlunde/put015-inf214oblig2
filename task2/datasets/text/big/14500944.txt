Special Agent (1935 film)
{{Infobox film
| name           = Special Agent
| image          = SpecialAgent.jpg
| caption        = Theatrical release poster
| director       = William Keighley
| producer       = Samuel Bischoff (uncredited) Martin Mooney (uncredited)
| writer         = Laird Doyle Abem Finkel Martin Mooney (story idea)
| narrator       =
| starring       = Bette Davis George Brent Ricardo Cortez
| music          =
| cinematography = Sidney Hickox
| editing        = Clarence Kolster
| studio         = Warner Bros. The Vitaphone Corp.
| distributor    = Warner Bros.
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Special Agent is a 1935 American drama film directed by William Keighley and starring Bette Davis. The screenplay by Laird Doyle and Abem Finkel is based on a story by Martin Mooney.

==Plot== treasury agent Internal Revenue Bureau and assigned to find enough evidence to charge gangster Alexander Carston with tax evasion.
 ledgers are kept in a code known only to his secretary, Julie Gardner. When she witnesses the murder of a man who double-crossed her boss, Bill begs her to quit her job, but Julie realizes she knows too much for Carston to let her go.

District Attorney Roger Quinn pressures the murdered mans partner into testifying, but Carston learns of the plan and the witness is murdered and Carston is acquitted. Julie is arrested as a material witness and decodes the books, but is kidnapped by Carstons henchmen before she can testify. Bill tricks Carston into taking him where Julie is being held, and the police trail them. A shootout follows and Julie is rescued. Her testimony sends Carston to Alcatraz, and she accepts Bills marriage proposal.

==Production notes==
Special Agent was one of three 1935 films co-starring Bette Davis and George Brent, who appeared on-screen together a total of thirteen times. Neither was happy with the finished product. Brent told Ruth Waterbury of Photoplay that the picture was "a poor, paltry thing, unbelievable and unconvincing." At the behest of the Warner Bros. publicity department, his comments remained unpublished. 
 censors compromised by allowing it to remain intact but without what they considered offensive dialogue. As a result, Ricardo Cortez lips can be seen moving but nothing is heard on the soundtrack. 

The Academy Award for Best Original Song|Oscar-winning song Lullaby of Broadway (song)|"Lullaby of Broadway" by Harry Warren and Al Dubin is heard in the background in a scene set in a casino. The tune was introduced by Wini Shaw that same year in the musical film Gold Diggers of 1935, also a Warner Bros. release.

Martin Mooneys story also served as the basis of the 1940 Warner Bros. release Gambling on the High Seas.

==Cast==
*Bette Davis as Julie Gardner 
*George Brent as Bill Bradford 
*Ricardo Cortez as Alexander Carston
*Jack La Rue as Jake Andrews (as Jack LaRue)
*Henry ONeill as District Attorney Roger Quinn
*Robert Strange as Waxey Armitage
*Joseph Crehan as Commissioner of Police
*J. Carrol Naish as Joe Durell (as J. Carroll Naish)
*Joe Sawyer as Rich (as Joseph Sauers)
*William B. Davidson as Charlie Young (as William Davidson)
*Robert Barrat as Chief of Internal Revenue Service Paul Guilfoyle as Williams Joe King as Agent Wilson (as Joseph King)
*Irving Pichel as U.S. District Attorney

==Reception==
The New York Times called the film a "crisp, fast moving and thoroughly entertaining melodrama" and "a wild and woolly gangland saga", adding, "It all has been done before, but somehow it never seems to lose its visual excitement."  

==References==
 

==External links==
*   at the Internet Movie Database

 

 
 
 
 
 
 
 
 
 
 