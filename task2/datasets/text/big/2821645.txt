Stanley & Iris
 
{{Infobox film
| name = Stanley & Iris
| image = Stan_iris_moviep.jpg
| caption = original film poster for Stanley & Iris
| director = Martin Ritt Union Street by Pat Barker
| starring = Jane Fonda Robert De Niro Swoosie Kurtz Martha Plimpton
| producer = Arlene Sellers Allen Winitsky John Williams
| cinematography = Donald McAlpine
| editing = Sidney Levin
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 104 min.
| country = United States
| language = English
| budget = $23 million Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p 253-255 
| gross = $5,820,015 
}}
 romantic drama Union Street by Pat Barker.
 John Williams and the cinematography is by Donald McAlpine. The film is marketed with the tagline "Some people need love spelled out for them."

==Plot summary==
  abusive husband Joe. Iris lives in a high crime area. With money already tight for the family, Kelly discovers she is pregnant which makes matters worse.

One of the few bright spots in her life is her blossoming friendship with Stanley Cox, a nice guy whom she first meets when he comes to her aid after her purse is stolen by a thug. Stanley works in the bakerys staff cafeteria, but as their friendship develops, Iris starts noticing some peculiarities about him. He becomes frustrated when asked to sign his name, and cannot pick out an item correctly from a shelf. He also cannot drive, rides a bicycle and still lives with his elderly father.

Iris realizes that Stanley is illiterate. When she innocently makes this known to his manager, Stanley gets fired as his manager feels his illiteracy could cause health and safety problems, despite the fact that he is a good cook and a model employee. After being fired, Stanley has to move and put his father in a shabby retirement home. Stanley himself begins living in a garage while taking odd jobs as a cleaner and laborer. His father later dies at the home, leaving Stanley upset that he couldnt take care of him. Stanley finds Iris and asks her to teach him to read. He explains his father was a traveling salesman and they moved constantly when Stanley was a boy so he bounced from school to school and never learned to read or write.

Stanley starts reading lessons with Iris and grows close to her and her family. When Iris complains that shes constantly tired and stuck in a rut, Stanley tells her to take a break while he does her household chores for her as he continues his reading exercises. He also tells her that hes been wanting to get intimate with her since they first met, but Iris is hesitant.

Iris decides to test Stanleys reading skills by giving him a map and telling him to meet her at a certain street corner within 15 minutes, but Stanley gets lost. Hours later, he finally reaches the intended corner to find Iris frantically waiting. Stanley is frustrated and embarrassed, and marches off home by himself, his interest in learning to read lost. Some time later, Iris sees Stanley in the park, but he maintains his aloofness. However, Iris is not ready to quit. She feels she made a mistake by pushing Stanley too soon, and goes to see him in his garage home. She wants him to continue learning how to read. As she looks around, she sees a large mechanical project that Stanley is creating, as he invents things as a hobby. He has built a cake-cooling machine that will out-perform anything in the market place. Iris is immensely impressed and learns that a company have shown interest in Stanleys machine and offered him a job, but he didnt pursue it because of his illiteracy. Stanley agrees to start reading again with Iris, and within time he has learned to write short sentences. Stanley cooks a big dinner for Iris and her family, and the two of them begin to grow close again. Noticing his threadbare jacket, Iris offers him a better one which had belonged to her husband. He takes the jacket, but reminds Iris that it isnt her husband who is wearing it. 

After Kelly has her baby, Iris is displeased when she drops out of school and starts working at the bakery as she doesnt want her daughter to waste her life in a dead-end job like she is. Stanley and Iris finally decide to make love, but Iris is still clinging to the memory of her husband and gets upset. This threatens their budding relationship further and they dont see each other for some time. Not prepared to give up on Iris the way she never gave up on him, Stanley goes to see her. He tells her that his inventions have finally paid off and hes been offered a good job in Detroit and is moving there. He tells Iris how grateful he is to her and that hell be back. Iris shows him a letter she wrote him but never mailed. She is impressed when Stanley reads it perfectly. He and Iris go to a public library where Stanley successfully reads out loud from a selection of books to show her he can now read anything. Stanley takes her to a fancy hotel where they order room service and spend the night together, though this time Iris is prepared to let go of the past. 

Stanley then goes to Detroit for his new job. After several months, Iris is walking home when an expensive car pulls up next to her and she is surprised to find Stanley behind the wheel. Stanley tells her that hes been given a raise and is looking to buy a large six-bedroom house in Detroit...and that he wants her to move there with him as his wife. Iris accepts.

==Main cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Jane Fonda || Iris King
|-
| Robert De Niro || Stanley Cox
|-
| Swoosie Kurtz || Sharon
|-
| Martha Plimpton || Kelly King
|-
| Harley Cross || Richard King
|-
| Jamey Sheridan || Joe
|-
| Feodor Chaliapin, Jr. || Leonides Cox
|-
| Zohra Lampert || Elaine
|-
| Loretta Devine || Bertha
|-
| Kathy Kinney || Bernice
|- Filmography of Stephen Root || Mr. Hershey
|}

==Music==
{{Infobox album  
| Name       = Stanley & Iris: Original Motion Picture Soundtrack
| Type       = Film score John Williams
| Cover      = 
| Alt        = 
| Released   = 1990
| Recorded   = 
| Genre      = 
| Length     = 28:56
| Label      = Varese Sarabande
| Producer   =  The Accidental Tourist
| This album = Stanley & Iris Indiana Jones and the Last Crusade
}}
{{Album ratings
|rev1= 
|rev1score= 
}}

===Track listing===
{{Tracklist title1  = Stanley and Iris  length1 = 3:24 title2  = Reading Lessons length2 = 2:26 title3  = The Bicycle length3 = 3:07 title4  = Factory Work length4 = 1:23 title5  = Finding a Family  length5 = 1:41 title6  = Stanley at Work length6 = 1:31 title7  = Looking after Papa length7 = 3:10  title8  = Stanleys Invention length8 = 1:17 title9  = Night Visit length9 = 1:58 title10 = Letters
|length10= 3:25 title11 = Putting it all Together
|length11= 1:46 title12 = End Credits
|length12= 3:03
}}

==Differences from the novel== British novel Union Street by Pat Barker. The novel was set in the North East of England in the 1970s, and tells the story of seven working-class women who all live on the same street. Whereas the film adaptation was essentially a romantic drama, the novel includes themes of prostitution, rape, abortion and terminal illness, and is significantly more grim. Many of the characters that appeared in the source novel do not appear in the film.

==Trivia==
 
* This film was shot on location in Toronto, Ontario, Canada and Waterbury, Connecticut.
* During the filming in Waterbury, Connecticut, local Vietnam War veterans picketed the production protesting Jane Fondas Anti-Vietnam War movement|anti-war activities of a decade and a half earlier. 
* This was actress Jane Fondas last film before her temporary retirement from acting (she returned to the screen 15 years later in the romantic comedy Monster-in-Law in 2005 in film|2005).
* Fonda and de Niro were each paid $3.5 million for their performances. Ritt received $1.65 million and the Ravetches $500,000. 
* This was director Martin Ritts last film before his death on December 8, 1990.

== References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 