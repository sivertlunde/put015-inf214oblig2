Mother of the Bride (1963 film)
 
{{Infobox film
| name           = Mother of the Bride
| image          = 
| caption        = 
| director       = Atef Salem
| producer       = Naguib Khoury
| writer         = Abdel Hamid Gouda
| starring       = Taheyya Kariokka Imad Hamdi
| music          = 
| cinematography = Masud Isa
| editing        = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Egypt
| language       = Arabic
| budget         = 
}}
 Best Foreign Language Film at the 37th Academy Awards, but was not accepted as a nominee. 

==Plot==
Zeinab and Hussein are the hard-working parents of a family in Cairo.  One day, their daughter announces that she is getting marriage|married.  When Zeinab and Hussein meet the grooms parents, the latter make a series of over-the-top demands for the wedding.

==Cast==
* Taheya Cariocca as Zeinab
* Imad Hamdi as Hussein

==See also==
* List of submissions to the 37th Academy Awards for Best Foreign Language Film
* List of Egyptian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 