People of the Mountains
 
 
{{Infobox film
| name           = People of the Mountains
| image          =
| image_size     =
| caption        =
| director       = István Szőts 
| producer       = 
| writer         =  József Nyírő (stories)   István Szőts
| narrator       =
| starring       = Alice Szellay   János Görbe   Péterke Ferency   József Bihari
| music          = Ferenc Farkas
| cinematography = Ferenc Fekete
| editing        = Zoltán Kerényi   
| studio         = Hunnia Filmgyár
| distributor    =  Hunnia Filmgyár
| released       = 1942
| runtime        = 88 minutes
| country        = Hungary Hungarian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
People of the Mountains (Hungarian: Emberek a havason) is a 1942 Hungarian drama film directed by István Szőts and starring Alice Szellay, János Görbe, Péterke Ferency. The film is set in the Székely woodcutting community of Transylvania. The films plot was based on a series of short stories by József Nyírő. The film was exhibited at the 1942 Venice Film Festival, where it was widely praised.  The films style has been suggested as an influence on the emerging Italian neorealism.  It was not granted an exhibition certificate in Nazi Germany because Joseph Goebbels considered it "too Catholic". 

==Production== on location in Transylvania which had been occupied by Hungarian troops following the Second Vienna Award during the Second World War while interior scenes were filmed at the Hunnia Film Studio in Budapest. The film was originally conceived as a short film, but studios bosses agreed to make it a feature film as long as costs could be kept low. Szőts had a relatively small film crew, and cast largely unknown actors in the leading roles. 

==Cast==
* Alice Szellay as Anna az asszony 
* János Görbe as Erdei Csutak Gergely 
* Péterke Ferency as Kicsi Gergö 
* József Bihari as Üdö Márton 
* Lajos Gárday as Ülkei Ádám 
* Oszkár Borovszky as Intézö 
* Lenke Egyed as Szobaasszony 
* Imre Toronyi as Orvos  
* György Kürthy as Orvostanár  
* János Pásztor (actor)|János Pásztor as Favágó  
* Elemér Baló as Favágó  
* Jenö Danis as Favágó  
* Sándor Hidassy as Utas 
* Zoltán Makláry as Kalauz

==References==
 

==Bibliography==
* Cunningham, John. Hungarian Cinema: From Coffee House to Multiplex. Wallflower Press, 2004.

==External links==
*  

 
 
 
 
 
 
 

 