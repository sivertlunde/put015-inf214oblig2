Election (2013 film)
{{Infobox film
| name           = Election
| image          = 2013 Kannada film Election poster.jpg
| caption        = Film poster
| director       = Om Prakash Rao
| producer       = Ramu
| screenplay     = Om Prakash Rao Pradeep Rawat
| music          = Hamsalekha
| cinematography = Rajesh Katta
| editing        = Lakshman
| studio         = Ramu Enterprises
| distributor    = 
| released       =  
| runtime        = 136 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Kannada Action action Drama drama film Pradeep Rawat, Dev Gill, Sharath Lohitashwa, Hanumanthegowda and Suchendra Prasad.

== Cast ==
* Malashri as Indira Pradeep Rawat as Narasimha
* Dev Gill as Deviprasad
* Srinivasa Murthy
* Sharath Lohitashwa as Vishwanath
* Hanumanthegowda as Gangadhara
* Suchendra Prasad as Ramaswamy
* Sadhu Kokila
* Bullet Prakash

== Production ==

=== Filming ===
The filming of Election began on December 8, 2012 in Bangalore. 

=== Pre-release revenues ===
The satellite rights of the film were sold for  . 

== Reception ==
Election received mixed to negative response from critics upon its theatrical release. G. S. Kumar of The Times of India gave the film a rating of three out of five and wrote, "Director N Omprakash Rao has done a good job of a story that helps you understand the lifestyle of politicians, how they indulge in goondaism, sex and luring voters with gifts, including money." and praised the role of acting and cinematography in the film. 

== References ==
 

 
 
 
 

 