Trust (1990 film)
{{Infobox film
  | name         = Trust
  | image        =
  | director     = Hal Hartley
  | writer       = Hal Hartley
  | starring     = Adrienne Shelly Martin Donovan
  | cinematography = Michael Spiller
  | music        = The Great Outdoors  Philip Reed   
  | editing      = Nick Gomez
  | distributor  =
  | released     =  
  | runtime      = 107 minutes
  | country      = United States
  | language     = English
  | budget       =
}}
Trust is a 1990 American dark romantic comedy starring Adrienne Shelly and Martin Donovan. It is the second feature film from writer-director Hal Hartley.  

==Plot==
Trust concerns the unusual romance between two young misfits wandering the same Long Island town. When Maria (Shelly), a recent high school dropout, announces her unplanned pregnancy to her family, her father dies of heart failure, her mother immediately evicts her from the household and her boyfriend breaks up with her. Lonely and with nowhere to go, Maria wanders her town in search of a place to stay. Along the way, she meets Matthew (Donovan), a highly educated and extremely moody electronics repairman. Matthew was recently fired from his job repairing television sets after the company orders him to cut corners to the detriment of customers, and he refuses. The two begin an unusual and curiously asexual romance built on their sense of mutual identification and trust. The lovers families continue to interfere with their fragile and largely conceptual relationship.

==Cast==
* Adrienne Shelly as Maria Coughlin
* Martin Donovan as Matthew Slaughter
* Merritt Nelson as Jean Coughlin John MacKay as Jim Slaughter
* Edie Falco as Peg Coughlin

==Release==
The film was premiered at the Toronto Film Festival on September 9, 1990 and went on to have a theatrical release on July 26, 1991.

==References==
 

==External links==
 
*   at Hal Hartleys Website
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 