Wavelength (1967 film)
 
{{Infobox film
| name           = Wavelength
| image          = michael_snow_wavelength.gif
| caption        =
| director       = Michael Snow
| producer       =
| writer         = Michael Snow
| starring       = Hollis Frampton Roswell Rudd Amy Taubin Joyce Wieland Amy Yadrin
| music          =
| cinematography = Michael Snow
| editing        = Michael Snow
| distributor    =
| released       = 1967
| runtime        = 45 min.
| country        = Canada United States
| awards         = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 greatest underground art house Canadian films realistic way composed movie in existence." 

==Outline==
Wavelength consists of almost no Action (fiction)|action, and what action does occur is largely elided. If the film could be said to have a conventional plot (narrative)|plot, this would presumably refer to the four "character (arts)|character" scene (film)|scenes. Snows intent for the film was "a summation of my nervous system, religious inklings and aesthetic ideas," he said of the 45-minute-long zoom lens|zoom–which nonetheless contains edits–that incorporates in its time frame four human events, including a mans death.  In the first scene, a woman in a fur coat enters the room accompanied by two men carrying a bookshelf or cabinet. The woman instructs the men where to place this piece of furniture and they all leave. Later, the same woman returns with a female friend, they drink the beverages they brought, and listen to "Strawberry Fields Forever" on the radio.  Long after they leave, what sounds like breaking glass is heard. At this point, a man (played by filmmaker Hollis Frampton) enters and inexplicably collapses on the floor.  Later, the woman in the fur coat reappears and makes a phone call, speaking, with strange calm, about the dead man in her apartment whom she has never seen before.
 musical score, tones at focus slowly across the forty-five minutes, only to stop and come into perfect focus on a photograph of the sea on the wall.

==Cast==
* Hollis Frampton
* Lyne Grossman
* Naoto Nakazawa
* Roswell Rudd
* Amy Taubin
* Joyce Wieland
* Amy Yadrin

==Structural Film== fixed tripod".  Where Sitney describes structural film as a "working process," Stephen Heath in Questions of Cinema finds Wavelength "seriously wanting" in that the "implied…narrative   in some ways a retrograde step in cinematic form".  To Heath, the principal theme of Wavelength is the "question of the cinematic institution of the subject of film" rather than the apparatus of filmmaking itself. 

In 2003, Snow released WVLNT (or Wavelength For Those Who Dont Have the Time), a shorter (1/3 of the original time) and significantly altered version by overlaying the original film upon itself. 

==Critical reception==
 Stan Brakhages Kenneth Angers Scorpio Rising (1964),  Wavelengths 45-minute running time nevertheless contributes to a reputation for being a difficult work: Zryd, 110 
 durational strategy, fade to white—at the films end. The film inspires as much boredom and frustration as intrigue and Epiphany (feeling)|epiphany...".  
 Minimal movie; one of the few films to engage those higher conceptual orders which occupy modern painting and sculpture. It has rightly been described as a ‘triumph of contemplative cinema."

==Distribution==
* 
* 

==References==

===Notes===
 

===Bibliography===
*Cornwell, Regina. Snow Seen: The Films and Photographs of Michael Snow. Toronto: PMA Books, 1980. ISBN 0-88778-197-7
*Elder, R. Bruce. Image and Identity: Reflections on Canadian Film and Culture.  , 1989. ISBN 0-88920-956-1
*Farber, Manny. Negative Space: Manny Farber on the Movies. London: Studio Vista, 1971. ISBN 0-289-70124-4
*Heath, Stephen. Questions of Cinema. Bloomington: Indiana University Press, 1981. ISBN 0-253-15914-8
* Legge. Elizabeth. Michael Snow: Wavelength. Cambridge, MA: Afterall (One Work Series), 2009. ISBN 1-84638-055-3
*Michelson, Annette. "About Snow." October Vol. 8 (Spring, 1979): 111-125.
*Shedden, Jim (ed.) The Michael Snow Project: Presence and Absence (The Films of Michael Snow 1965-1991). Toronto: Alfred A. Knopf Canada, 1995. ISBN 0-394-28106-3
*Sitney, P. Adams. Visionary Film: The American Avant-Garde 1943-1978. New York: Oxford University Press, 1979. ISBN 0-19-502486-9
*Zryd, Michael. "Avant-Garde Films: Teaching Wavelength." Cinema Journal 47, Number 1 (Fall 2007): 109-112.

==External links==
*  
*  
*  
*  

 

 
 
 
 
 