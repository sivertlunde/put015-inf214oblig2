Dark Horse (2005 film)
{{Infobox film
| name           = Dark Horse
| image          = Voksne mennesker ver2 xlg.jpg
| image_size     = 
| caption        = 
| director       = Dagur Kári
| producer       = Birgitte Skov Morten Kaufmann
| writer         = Dagur Kári Rune Schjøtt
| starring       = Jakob Cedergren Nicolas Bro Tilly Scott Pedersen Morten Suurballe
| music          = Slowblow
| cinematography = Manuel Alberto Claro
| editing        = Daniel Dencik Nordisk Film Distribution A/S
| released       = 13 May 2005
| runtime        = 106 minutes
| country        = Denmark Iceland
| language       = Danish
}}
 2005 cinema Danish film directed by Dagur Kári about a young man, his best friend, and a girl. It was screened in the Un Certain Regard section at the 2005 Cannes Film Festival.   

== Cast ==
*Jakob Cedergren - Daniel
*Nicolas Bro - Morfar
*Tilly Scott Pedersen - Franc
*Morten Suurballe - Dommeren
*Bodil Jørgensen - Gunvor
*Nicolaj Kopernikus - Tejs
*Anders Hove - Herluf C
*Kristian Halken - Allan Simonsen
*Thomas W. Gabrielsson - Søvnforsker Arne
*Michelle Bjørn-Andersen - Dommerens kone
*Pauli Ryberg - Skule Malmquist
*Mikael Bertelsen - Fuldmægtig
*Asta Esper Hagen Andersen - Mormor Lovisa (as Asta Esper Andersen)
*Vera Gebuhr - Dame i bagerbutik Peder Pedersen - Graffitikunde

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 


 
 
 