Annabelle Serpentine Dance
{{Infobox film
| name           = Annabelle Serpentine Dance
| image          = 
| image size     = 
| caption        = 
| director       = William K.L. Dickson William Heise
| producer       = William Heise
| starring       = Annabelle Moore
| music          = 
| cinematography = 
| editing        = 
| studio         = Edison Manufacturing Company
| distributor    = Edison Manufacturing Company
| released       = 1895
| runtime        = 45 seconds
| country        = United States
| language       = Silent
| budget         = 
| gross          =
}}

Annabelle Serpentine Dance is a  , ISBN 9780813552989 

==Action in the film==
 
 Mercury in her hair. Her dance emphasizes the movement of her visible, bare legs. She kicks high, bows, and moves to her right and left. The second dancer has a voluminous, long skirt, and holds sticks in each hand attached to the skirts outer edges. The flowing patterns of the skirt from her arm movements give the second scene a different feeling from the first.

==Production and distribution== producer and camera operator.

Film historians have commented on the possibilities for viewers to slow down the hand cranking of the footage in was not technically possible in other art forms. Barker, Jennifer M. (2009). The Tactile Eye: Touch and the Cinematic Experience. University of California Press, ISBN 9780520258426 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 