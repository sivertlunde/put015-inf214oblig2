List of accolades received by Gone Girl (film)
 
{| class="infobox" style="width: 25em; text-align: left; font-size: 90%; vertical-align: middle;"
|+  List of accolades received by Gone Girl 
| colspan="3" style="text-align:center;" |  Rosamund Pike, received many awards and nominations for her acting role in the film.
|-
| colspan=3 |
{| class="collapsible collapsed" width=100%
! colspan=3 style="background-color: #D9E8FF; text-align: center;" | Accolades
|-
|-  style="background:#d9e8ff; text-align:center;"
| style="text-align:center;" | Award
|  style="text-align:center; background:#cec; text-size:0.9em; width:50px;"| Won
|  style="text-align:center; background:#fcd; text-size:0.9em; width:50px;"| Nominated
|-
| style="text-align:center;"| AACTA International Awards
| 
| 
|-
| style="text-align:center;"|
;Academy Awards
| 
| 
|-
| style="text-align:center;"|
;Alliance of Women Film Journalists
| 
| 
|-
| style="text-align:center;"|
;American Cinema Editors
| 
| 
|-
| style="text-align:center;"|
;African-American Film Critics Association
| 
| 
|-
| style="text-align:center;"|
;Art Directors Guild
| 
| 
|-
| style="text-align:center;"|
;Austin Film Critics Association
| 
| 
|-
| style="text-align:center;"|
;British Academy Film Awards
| 
| 
|-
| style="text-align:center;"|
;Casting Society of America
| 
| 
|-
| style="text-align:center;"|
;Chicago Film Critics Association
| 
| 
|-
| style="text-align:center;"|
;Costume Designers Guild
| 
| 
|-
| style="text-align:center;"|
;Critics Choice Movie Awards
| 
| 
|-
| style="text-align:center;"|
;Dallas–Fort Worth Film Critics Association
| 
| 
|-
| style="text-align:center;"|
;Denver Film Critics Society
| 
| 
|-
| style="text-align:center;"|
;Detroit Film Critics Society
| 
| 
|-
| style="text-align:center;"|
;Dublin Film Critics Circle
| 
| 
|-
| style="text-align:center;"|
;Empire Awards
| 
| 
|-
| style="text-align:center;"|
;Florida Film Critics Circle
| 
| 
|-
| style="text-align:center;"|
;Golden Globe Awards
| 
| 
|-
| style="text-align:center;"|
;Grammy Awards
| 
| 
|-
| style="text-align:center;"|
;Hollywood Film Awards
| 
| 
|-
| style="text-align:center;"|
;Iowa Film Critics
| 
| 
|-
| style="text-align:center;"|
;Kansas City Film Critics Circle
| 
| 
|-
| style="text-align:center;"|
;London Film Critics Circle
| 
| 
|-
| style="text-align:center;"|
;Motion Picture Sound Editors
| 
| 
|-
| style="text-align:center;"|
;MTV Movie Awards
| 
| 
|-
| style="text-align:center;"|
;National Board of Review
| 
| 
|-
| style="text-align:center;"|
;North Texas Film Critics Association
| 
| 
|-
| style="text-align:center;"|
;Online Film Critics Society
| 
| 
|-
| style="text-align:center;"|
;Palm Springs International Film Festival
| 
| 
|-
| style="text-align:center;"|
;Producers Guild of America Awards
| 
| 
|-
| style="text-align:center;"|
;San Diego Film Critics Society
| 
| 
|-
| style="text-align:center;"|
;San Francisco Film Critics Circle
| 
| 
|-
| style="text-align:center;"|
;Santa Barbara International Film Festival
| 
| 
|-
| style="text-align:center;"|
;Satellite Awards
| 
| 
|-
| style="text-align:center;"|
;Saturn Awards
| 
| 
|-
| style="text-align:center;"|
;Screen Actors Guild Awards
| 
| 
|-
| style="text-align:center;"|
;Southeastern Film Critics Association
| 
| 
|-
| style="text-align:center;"|
;St. Louis Gateway Film Critics Association
| 
| 
|-
| style="text-align:center;"|
;USC Scripter Award
| 
| 
|-
| style="text-align:center;"|
;Washington D.C. Area Film Critics Association
| 
| 
|-
| style="text-align:center;"|
;Writers Guild of America Awards
| 
| 
|}
|- style="background:#d9e8ff;"
| style="text-align:center;" colspan="3"|
;Total number of awards and nominations 
|-
| 
| 
| 
|- style="background:#d9e8ff;" References
|} Gone Girl eponymous 2012 novel. Set in Missouri, United States, the film stars Ben Affleck as Nick Dunne, a writer, whose wife Amy played by Rosamund Pike goes missing. Neil Patrick Harris, and Tyler Perry feature in supporting roles. The score was composed by Trent Reznor, and Atticus Ross.  

Gone Girl premiered at the New York Film Festival on September 26, 2014,  before 20th Century Fox later gave the film a wide release at over 3,000 theaters in the Unites States, and Canada on October 3. It grossed over $368 million on a production budget of $61 million.  As of 2014, Gone Girl is Finchers highest grossing film.  Rotten Tomatoes, a review aggregator, surveyed 281 reviews and judged 88% to be positive. 
 Best Actress Best Screenplay Best Original Score.  Pike won the Empire Award for Best Actress, and Breakthrough Performance Award from the Palm Springs International Film Festival, and was also nominated at the Screen Actors Guild, and British Academy Film Awards (BAFTAs).     Flynn was also nominated at the BAFTAs as well as the Writers Guild of America Awards.   The National Board of Review included Gone Girl in its list of top ten films of the year. 

== Accolades ==
{| class="wikitable plainrowheaders sortable" style="font-size: 95%;"
|- Award
! Date of ceremony Category
! scope="col"|Recipient(s) and nominee(s) Result
!scope="col" class="unsortable" |  
|- AACTA Awards|AACTA International Awards    4th AACTA January 31, 2015    AACTA International Best Actress
|    
| 
|style="text-align:center;"| 
|-     Academy Awards    87th Academy February 22, 2015    Academy Award Best Actress
|    
| 
|style="text-align:center;"|    
|- Alliance of Women Film Journalists   
|rowspan=4| January 12, 2015 Best Actress
|   
| 
|style="text-align:center;" rowspan=4|     
|- Best Adapted Screenplay
| 
| 
|- Best Original Score
|  and Atticus Ross
| 
|- Best Woman Screenwriter
| 
| 
|- American Cinema Editors   
| January 30, 2015    American Cinema Best Edited Feature Film – Dramatic
|    
| 
|style="text-align:center;"|     
|-
!scope=row|African-American Film Critics Association  February 4, 2015 Best Supporting Actor 
|    
| 
|style="text-align:center;"| 
|- Art Directors Guild    January 31, 2015    Art Directors Excellence in Production Design for a Contemporary Film
| 
| 
|style="text-align:center;"|  
|- Austin Film Critics Association 
|rowspan=3| December 17, 2014 Top Ten Films of the Year  Gone Girl
| 
|style="text-align:center;" rowspan=3| 
|-  Best Actress
| 
| 
|- Best Adapted Screenplay
| 
| 
|- British Academy Film Awards February 8, 2015 Best Adapted Screenplay
|  
|  
|style="text-align:center;" rowspan=2|   
|- Best Actress in a Leading Role
|  
|  
|- Casting Society of America January 22, 2015 Big Budget Drama
| , Annie Hamilton
|  
|style="text-align:center;"| 
|-  Chicago Film Critics Association  December 15, 2014 Chicago Film Best Director
| 
|  
|style="text-align:center;" rowspan=4|  
|-  Chicago Film Best Actress
| 
|  
|- Chicago Film Best Adapted Screenplay
| 
|  
|- Best Editing
|  
|  
|- Costume Designers Guild  February 17, 2015 Excellence in Contemporary Film
| 
|  
|style="text-align:center;"| 
|- 
!scope=row rowspan="6"|Critics Choice Movie Awards  January 15, 2015 Best Picture Gone Girl
|  
|style="text-align:center;" rowspan=6| 
|- Best Director
| 
|  
|- Best Actress
| 
|  
|- Best Adapted Screenplay
| 
|  
|- Best Editing
|  
|  
|- Best Composer
|  and Atticus Ross
|  
|-
!scope=row rowspan="3"|Dallas–Fort Worth Film Critics Association 
|rowspan=3| December 15, 2014
|  Gone Girl
|  
|style="text-align:center;" rowspan=3| 
|-  Best Director 
| 
|  
|- Best Actress 
| 
|  
|- Denver Film Critics Society 
|rowspan=2| January 12, 2015 Best Actress
| 
| 
|style="text-align:center;" rowspan=2|  
|-  Best Adapted Screenplay
| 
| 
|-  Detroit Film Critics Society  December 15, 2014 Best Actress
| 
| 
|style="text-align:center;"| 
|-  Dublin Film Critics Circle December 17, 2014 Best Actress
| 
| 
|style="text-align:center;"| 
|- Empire Awards 20th Empire March 29, 2015 Empire Award Best Actress
| 
| 
|style="text-align:center;" rowspan=3|   
|- Empire Award Best Female Newcomer
| 
| 
|- Empire Award Best Thriller Gone Girl
| 
|- Florida Film Critics Circle  December 19, 2014 Florida Film Best Actress
| 
|  
|style="text-align:center;" rowspan=3| 
|- Florida Film Best Adapted Screenplay
| 
| 
|- Best Score
|  and Atticus Ross
| 
|- Golden Globe Awards  72nd Golden January 11, 2015 Golden Globe Best Director
| 
| 
|style="text-align:center;" rowspan=4|   
|-  Golden Globe Best Actress in a Motion Picture – Drama
| 
| 
|- Golden Globe Best Screenplay
| 
| 
|- Golden Globe Best Score
|  and Atticus Ross
| 
|- Grammy Awards  57th Annual February 8, 2015 Grammy Award Best Score Soundtrack for Visual Media
|  and Atticus Ross
| 
|style="text-align:center;"| 
|-  Hollywood Film Awards  November 14, 2014  Best Film Gone Girl
| 
|style="text-align:center;" rowspan=3 | 
|- Screenwriter Award
| 
| 
|- Best Sound
| 
| 
|-
!scope=row rowspan="2"| Iowa Film Critics
| rowspan="2"| January 7, 2015
| Best Director
|     
|   
|style="text-align:center;" rowspan=2|  
|- 
| Best Actress
|  
|   
|- Kansas City Film Critics Circle December 14, 2014  Kansas City Best Actress
| 
| 
|style="text-align:center;"| 
|- London Film Critics Circle January 18, 2015  London Film British Actress of the Year
| 
| 
|style="text-align:center;"|  
|- Motion Picture Sound Editors
| February 15, 2015
| Feature Music
|  , Jonathon Stevens
|  
|style="text-align:center;"| 
|- MTV Movie Awards 2015 MTV April 12, 2015 Movie of the Year Gone Girl
| 
|style="text-align:center;" rowspan=4| 
|- Breakthrough Performance
| 
| 
|- Best Scared-As-S**t Performance
| 
| 
|- Best Villain
| 
| 
|- National Board of Review  National Board December 2, 2014
|  Gone Girl
| 
|style="text-align:center;"|   
|- North Texas Film Critics Association  January 5, 2015 Best Actress
| 
| 
|style="text-align:center;"| 
|- Online Film Critics Society  Online Film December 15, 2014 Online Film Best Actress
| 
| 
|style="text-align:center;" rowspan=3|   
|- Online Film Best Adapted Screenplay
| 
| 
|- Online Film Best Editing
|  
| 
|-  Palm Springs International Film Festival  January 3, 2015 Breakthrough Performance Award
| 
| 
|style="text-align:center;"|   
|- Producers Guild of America  Producers Guild January 24, 2015 Producers Guild Best Theatrical Motion Picture
| 
| 
|style="text-align:center;"| 
|-  San Diego Film Critics Society  December 15, 2014 San Diego Best Film Gone Girl
| 
|style="text-align:center;" rowspan=8|  
|-  San Diego Best Director
| 
| 
|- San Diego Best Actress
| 
| 
|- San Diego Best Supporting Actress
| 
| 
|- San Diego Best Adapted Screenplay
| 
| 
|- San Diego Best Cinematography Jeff Cronenweth
| 
|- San Diego Best Editing
|  
| 
|- Best Score
|  and Atticus Ross
| 
|- San Francisco Film Critics Circle  December 14, 2014 San Francisco Best Adapted Screenplay
| 
| 
|style="text-align:center;"| 
|- Santa Barbara International Film Festival  February 1, 2015 Virtuosos Award
| 
|  
|style="text-align:center;"| 
|- Satellite Awards  19th Satellite February 15, 2015  Satellite Award Best Film  Gone Girl
|  
|style="text-align:center;" rowspan=7|  
|-  Satellite Award Best Director 
| 
|  
|-  Satellite Award Best Actress 
| 
| 
|- Satellite Award Best Adapted Screenplay 
| 
| 
|- Satellite Award Best Cinematography  Jeff Cronenweth
| 
|- Satellite Award Best Original Score 
|  and Atticus Ross
| 
|- Satellite Award Best Sound 
|  and Steve Cantamessa
| 
|- Saturn Awards 41st Saturn June 25, 2015 Best Thriller Film Gone Girl
| 
|style="text-align:center;" rowspan=2| 
|- Saturn Award Best Actress
| 
| 
|- Screen Actors Guild Awards  21st Screen January 25, 2015 Screen Actors Outstanding Performance by a Female Actor in a Leading Role
| 
| 
|style="text-align:center;"|   
|-  Southeastern Film Critics Association December 22, 2014 Southeastern Film Top Ten Films of the Year Gone Girl
| 
|style="text-align:center;" rowspan=2| 
|- Best Adapted Screenplay
| 
| 
|-
!scope=row rowspan="8"|St. Louis Gateway Film Critics Association December 15, 2014 Best Picture Gone Girl
| 
|style="text-align:center;" rowspan=8|  
|- Best Director
| 
| 
|- Best Actress
| 
| 
|- Best Supporting Actress
| 
| 
|- Best Adapted Screenplay
| 
| 
|- Best Art Direction
| 
| 
|- Best Cinematography
| 
| 
|- Best Score
|  and Atticus Ross
| 
|- USC Scripter Award  January 31, 2015  Best Adapted Screenplay
| 
|  
|style="text-align:center;"| 
|- Washington D.C. Area Film Critics Association  December 8, 2014  Washington D.C. Best Film  Gone Girl
|  
|style="text-align:center;" rowspan=6|   
|- Washington D.C. Best Director 
| 
| 
|- Washington D.C. Best Actress 
| 
| 
|- Washington D.C. Best Adapted Screenplay 
| 
| 
|- Best Editing
|  
| 
|- Washington D.C. Original Score 
|  and Atticus Ross
| 
|- Writers Guild of America Awards  Writers Guild February 14, 2015  Writers Guild Best Adapted Screenplay 
| 
|  
|style="text-align:center;"|   
|-
|}

==Notes==
 

==See also==
2014 in film

== References ==
 

==External links==
*  at Internet Movie Database

 
 
 