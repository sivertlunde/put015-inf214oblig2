On the Threshold
{{Infobox film
| name           = On the Threshold
| image          = 
| caption        = 
| director       = Leif Erlsboe
| producer       = 
| writer         = Leif Erlsboe
| starring       = Magnus E. Haslund
| music          = 
| cinematography = Svein Krøvel
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}

On the Threshold ( ) is a 1984 Norwegian drama film directed by Leif Erlsboe. It was entered into the 14th Moscow International Film Festival where it won a Special Prize.   

==Cast==
* Magnus E. Haslund as Lars
* Joachim Calmeyer as Tobby
* Anne Krigsvoll as Lill
* Peder Hamdahl Naess as Peter
* Anne Marie Ottersen as Mor
* Frode Rasmussen as Far
* Espen Skjønberg as Kalle
* Nils Ole Oftebro as Skolelæreren
* Johan Kjelsberg as Iversen
* Grete Nordrå as Fru Lund
* Odd Torgeir Pedersen as Dvergen

==References==
 

==External links==
*  

 
 
 
 
 
 