A Harp in Hock
{{infobox film
| name           = A Harp in Hock
| image          =
| imagesize      =
| caption        =
| director       = Renaud Hoffman Glenn Belt (asst director)
| producer       = DeMille Pictures
| writer         = Evelyn Campbell (story) Sonya Levien (scenario)
| starring       = Rudolph Schildkraut Frank Coghlan, Jr. May Robson Bessie Love
| music          =
| cinematography = Dewey Wrigley
| editing        = W. Donn Hayes
| distributor    = Pathé Exchange
| released       = October 10, 1927
| country        = United States
| language       = Silent film (English intertitles)
}} lost 1927 Junior Coghlan, May Robson, and Bessie Love and was directed by Renaud Hoffman.   

==Cast==
*Rudolph Schildkraut as Isaac Abrams
*Frank Coghlan, Jr. (as Jr. Coghlan) as Tommy Shannon
*May Robson as Mrs. Banks
*Bessie Love as Nora Banks
*Louis Natheaux as Nick
*Elsie Bartlett as Mrs. Shannon
*Mrs. Charles Mack as The Clock Woman
*Joseph Striker as Dr. Franz Mueller
*Adele Watson as Investigator
*Lillian Harmer as Sourface
*Clarence Burton as Plainclothesman
*Bobby Heck as Snipe Banks
*Austen Jewell (uncredited)
*Billy "Red" Jones (uncredited)

==References==
 

==External links==
* 
* 
*  
* 

 
 
 
 
 
 
 


 