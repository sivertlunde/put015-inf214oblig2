Blood Glacier
{{Infobox film
| name           = Blood Glacier
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Marvin Kren
| producer       = 
| writer         = Benjamin Hessler
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Gerhard Liebmann, Edita Malovcic, Brigitte Kren
| music          = 
| cinematography = Moritz Schultheiß
| editing        = Daniel Prochaska
| studio         =  Allegro Film, Filmfonds Wien, Filmstandort Austria
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Austria
| language       = German
| budget         = 
| gross          = 
}}
Blood Glacier (also known as Blutgletscher, Glazius, and The Station) is a 2013 horror film that was directed by Marvin Kren. The movie had its world premiere at the Toronto International Film Festival on September 6, 2013 and had a limited theatrical release in the United States on May 2, 2014. It stars Gerhard Liebmann as a researcher faced with a strange liquid that poses a threat to anything living.  

==Synopsis==
Janek (Gerhard Liebmann) is a brilliant researcher that arrives in the Swiss Alps to investigate global warming. Hes part of a larger group, but savors his time alone as hes a loner by nature. One day the group discovers a glacier covered in a strange red liquid that has odd effects on the surrounding wildlife. Janeks group grows excited as they realize that the liquid is transforming the local wildlife into strange new genetic hybrids, but Janek himself is more wary of the liquid and the potential dangers it poses. His caution is soon proven to be warranted, as the group begins to fall prey to the hybrids created by the liquid. A new group soon appears to investigate the new discovery, and Janek is horrified when he finds that a former girlfriend of his is among the newcomers.

==Cast==
*Gerhard Liebmann as Janek
*Edita Malovcic as Tanja
*Brigitte Kren as Ministerin Bodicek Santos as Tinnie
*Hille Beseler as Birte
*Peter Knaack as Falk
*Felix Römer as Harald
*Wolfgang Pampel as Bert Krakauer
*Murathan Muslu as Luca
*Michael Fuith as Urs
*Adina Vetter as Irene
*Coco Huemer as Geli

==Reception== John Carpenters The Thing,    with IndieWire stating that "this low budget chiller is unable to capture the same kind of awe and terror that made "The Thing" so powerful, although its attempt to be more character-based and emphasis on practical effects is somewhat admirable."  Praise for the movie tended to center upon the movies status as an eco-horror film, as multiple reviewers praised it for not being a "preachy diatribe" and for its monsters.   The AV Club noted that while the film was very similar to other movies in the same genre, this worked in Blood Glaciers favor as it was "a movie viewers have seen dozens of times before, and will see again, with slight variations, because it embodies a fundamental quality of B-horror entertainment."   

===Awards===
*Austrian Film Award for Best Actor at Viennale (2014, won - Gerhard Liebmann)
*Austrian Film Award for Best Makeup Best Makeup at Viennale (2014, won)
*Austrian Film Award for Best Sound Design at Viennale (2014, won)
*Austrian Film Award for Best Editing at Viennale (2014, nominated)
*Max Ophüls Award at the Max Ophüls Festival (2014, nominated)

==References==
 

==External links==
*  
*  

 
 
 
 