Finding Mercy
{{Infobox film
| name           = Finding Mercy
| image size     = 
| image	         = Finding Mercy film poster.jpg
| alt            = 
| caption        = Theatrical Poster
| director       = Desmond Elliot
| producer       = Desmond Elliot
| writer         = Kehinde Odukoya
| screenplay     = Kehinde Odukoya
| story          = 
| narrator       = 
| starring       =  
| music          = 
| cinematography = 
| editing        = Dapo Ola Daniels
| studio         = Denziot Productions
| distributor    = 
| released       =  }} 
| runtime        = 
| country        = Nigeria
| language       = English
| budget         =
| gross          =
}}
Finding Mercy is a 2013 Nigerian drama film produced and directed by Desmond Elliot, and starring Rita Dominic, Blossom Chukwujekwu, Uti Nwachukwu and Chioma Chukwuka.  

==Cast==
*Rita Dominic
*Uti Nwachukwu Chioma Chukwuka-Akpotha
*Blossom Chukwujekwu
*Tamara Eteimo
*Desmond Elliot
*Biola Williams
*Dabota Lawson

==Reception==
Nollywood Reinvented gave it a 56% rating, commending its production and directing. The reviewer found the film interesting in spite of the films focus on the problems of daily living.  Efe Doghudje of 360Nobs gave it a rating of 5 out of 10 stars. She criticized the inadequate costuming that meant that in spite of a strong cast, the characters were not very believable. 

==References==
 

 
 
 
 
 


 
 