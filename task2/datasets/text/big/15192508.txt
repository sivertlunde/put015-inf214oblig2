Tokyo Trial (2006 film)
 
{{Infobox film
| name           = Tokyo Trial
| image          = 
| caption        = 
| director       = Gao Qunshu
| producer       = 
| writer         = 
| starring       = Damian Lau Ken Chu Kelly Lin Kenneth Tsang Eric Tsang
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 111 minutes Chinese (Mandarin) English Japanese Japanese
| budget         = 
| film name      = {{Film name
| jianti         = 东京审判
| fanti          = 東京審判
| pinyin         = Dōngjīng Shěnpàn}}
}}
Tokyo Trial ( ) is a Chinese film released in 2006.

==Plot==
This film was directed by Gao Qunshu and is about the International Military Tribunal for the Far East after Japans surrender in World War II. The movie presents the trial from the point of view of the Chinese judge Mei Ju-ao.

The director and his crew spent more than a year doing research to finish the script, which is based on historical data. It cost 18 million yuan (2.25 million U.S. dollars). This film hired actors from 11 countries, including mainland China, Hong Kong, Japan and other places, including actors such as Kenneth Tsang and Damian Lau. They recreated court scenes from the trial in Chinese, English and Japanese.

It was shown in cinemas and around 100 universities across mainland China to mark the 75th anniversary of the start of Japans invasion of China.

==Cast==
* Damian Lau 
* Bosco Wong 
* Billy Tan Wai Shiun 
* Ken Chu 
* Kelly Lin 
* Kenneth Tsang 
* Eric Tsang 
* Liu Weiwei
* Tse Kwan-Ho
* Ying Da
* Joe Bosco
* Ding Yongdai
* Dan Ziskie

==Reception==
According to  ." 

==See also==
* Japanese war crimes
* Nanking Massacre
* Nanking (2007 film)

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 