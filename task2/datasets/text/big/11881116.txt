Some Kind of Life
Some 1995 TV drama written by Kay Mellor and directed by Julian Jarrold, starring Jane Horrocks and Ray Stevenson (actor).

==Synopsis==
In this drama, Alison (Jane Horrocks) is the young wife and mother whose life is turned upside down after her beloved husband, Steve (Ray Stevenson), is involved in a motorcycle crash, suffers massive head trauma and regresses to a mental and emotional age of 5. Much of the story centres on the mundane aspects of being forced to deal with the loss of her husband as a man and the acquisition of him as a child. Time passes and things do not improve. Slowly even Steves closest friends begin withdrawing their support. Eventually Alison is forced to decide whether she will continue to stick by Steve, or whether she will go on without him.

==External links==
* 

 

 
 
 
 
 
 
 


 