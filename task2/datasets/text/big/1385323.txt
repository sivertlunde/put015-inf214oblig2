The Minus Man
{{Infobox film
| name           = The Minus Man
| image          = Minus_man_ver2.jpg
| caption        = Theatrical release poster
| director       = Hampton Fancher
| producer       = Keith Abell Fida Attieh David Bushell Steve Carlis Joseph J. DiMartino Larry Meistrich Mary Vernieu
| writer         = Hampton Fancher (screenplay) Lew McCreary (novel)
| narrator       = Brian Cox Mercedes Ruehl Dwight Yoakam Dennis Haysbert Sheryl Crow
| music          = Marco Beltrami
| cinematography = Bobby Bukowski
| editing        = Todd C. Ramsay
| distributor    = Artisan Entertainment
| released       =  
| runtime        = 111 min.
| country        = United States English
}}
 Melville  s Billy Budd and Being Theres Chauncey Gardner". 

==Plot==
The film opens as Vann Siegert (Owen Wilson) decides to turn left onto a road, striking out on a new path in life. His first stop is a bar, where he meets Casper (Sheryl Crow, in her first film appearance). They leave the bar together in his truck. Casper asks Vann to pull over at a rest stop to get high. After she shoots up, she balances her syringe on the open glove box, noticing a flask. She asks what is in it, and Vann answers that it is full of amaretto. After drinking it, she passes out. Vann carries her body into the bathroom and arranges it to look as if she overdosed.
 Brian Cox and Mercedes Ruehl). He rents out the room of their missing daughter. Doug recommends he look for work at the post office, as they are hiring seasonal help for Christmas.

As he is tidying up, Vann passes out and daydreams that he is digging a grave. Detectives Blair and Graves (Dwight Yoakam and Dennis Haysbert) stand over him, asking taunting questions about his methods. Throughout the film, Vann has imaginary conversations/psychotic breaks with reality with the two detectives. Vann is shown retrieving a kit from under his car. It contains a poison, which he places in the flask and then fills with amaretto.

Doug takes Vann to a high school football game, where he meets a star athlete and his family. A few days later, Vann offers the boy a ride and murders him, burying his body on a beach. He explains in one of the many voice overs about his methods, that he never plans out his murders and he never commits any violence. His victims just go to sleep and do not wake up. Vann also reveals that the murder of the footballer broke two of his personal rules: Dont kill anyone you know, and dont kill anyone from your town.

Vann helps the town search for the missing athlete and even attends his memorial service. His ties to the community grow as he is given more responsibility at the post office. One of his co-workers, Ferrin (Janeane Garofalo), sheepishly pursues him. Doug drives Ferrin to the beach. The pair exchange an awkward hug, which betrays the difficulties Vann has with physical intimacy, despite his warm and outgoing personality with everyone he meets. The embrace occurs directly over the spot where Vann buried the high school footballer.

On Christmas Day, Vann goes to a diner and chooses another victim (Meg Foster). She invites him to her home, where he sees that she is a painter. Something about her work disturbs him and he flees. Vann returns to the diner and slips poison into the water of a man (Lew McCreary) eating alone. An autopsy reveals that the death was the result of a rare poison derived from tree bark fungus found in the pacific north west. The poison is then linked with the murder of Casper. The young athletes body is found and also linked to the same poison. Vann knows that the police will eventually tie the murders to him. While looking in the mirror, he pulls hairs off his jacket and puts them in an envelope on which he writes FERRiN.

Jane is found dead by a blow to the back of the head. The police suspect Doug, but Vann is worried that the increased scrutiny from another murder will lead the police to him. During a date with Ferrin, he tries to initiate sex by assaulting her. She is terrified, and Vann is mortified.

The next day, Vann is seen playing the piano – the tune "Aint We Got Fun" – and Doug starts singing and dancing around the room. The police arrive and arrest Doug for the murder of Jane. Vann packs his things. Before he leaves town, he puts his postal uniform and the envelope with Ferrins hair sample in a mailbox. The film ends as he drives on the highway, pursued by the same cop who approached him on the beach. After taking a good look with her spotlight, she smiles at him and takes the right fork in the road while Vann takes the left. In a voice-over monologue prior to this scene, Vann vows to stop killing whenever he gets where he is going. However, Vann explains that he does not know where he is going.

==Reception==
The film has a 61% freshness rating on Rotten Tomatoes. Roger Ebert called it "a psychological thriller of uncommon power maybe because its so quiet and devious".  In the Los Angeles Times, Kevin Thomas wrote "it is above all such an unsettling experience you find yourself still taking it all in well after the lights have gone up". 
 Camus and Alfred Hitchcock|Hitchs small-town classic, Shadow of a Doubt" in his Variety (magazine)|Variety review. 

The film was nominated for the Grand Jury Prize at the Sundance Film Festival.

==Marketing==
The Minus Man is notable for being one of the only films to feature promotional materials focusing on the films ability to spark discussion among audience members. In addition to the tagline "Dont see it alone. Unless you like talking to yourself," one trailer for the film showed a couple discussing the film as they leave the theater. Their conversation takes them from place to place all over the city, until the man (played by Eddie Ifft) marvels at how beautiful the sunrise is. The woman (played by Marin Hinkle) realizes she is late for work and rushes to her job as a lifeguard, where two people are floating dead in the pool. The ad ends with the tagline "Careful, you can talk about it for hours."

The main trailer featured the tagline, "When hes around nothing adds up" and touted the film as the product of "the producers of Sling Blade" and "the writer of Blade Runner".

== Cast ==
* Owen Wilson as  Vann Siegert
* Sheryl Crow as  Laurie Bloom
* Dwight Yoakam as  Detective Blair
* Dennis Haysbert as  Detective Graves
* Alex Warren as  State Trooper
* Mercedes Ruehl as  Jane Durwin Brian Cox as  Doug Durwin
* Janeane Garofalo as  Ferrin
* Meg Foster as  Irene
* John Vargas as Priest
* Eric Mabius as Gene

== References ==
 

==External links==
* 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 