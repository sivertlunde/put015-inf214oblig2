Lifepod (1981 film)
{{Multiple issues|
   }}
 

{{Infobox film
| name           = Lifepod
| image          = 
| caption        = 
| director       = Bruce Bryant
| producer       = James Castle
| writer         =  Bruce Bryant (screenplay), Carol Johnsen (screenplay)
| based on       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Don Zirpola
| studio         = 
| distributor    = Golden Key Entertainment
| released       =  
| runtime        = 90min
| language       = English
| country        = United States
| budget         = 
| gross          = 
}}
Lifepod is a 1981 science fiction film distributed by Golden Key Entertainment.  It stars Joe Penny, Jordan Michals, Kristine DeBell, Carl Lumbly and Sandy Kenyon.

==Plot outline==
The film takes place in 2191. The latest space liner for the Whitestar Lines is the Arcturus, currently on its maiden voyage.  On its approach to Callisto, for unknown reasons, the ships computer, the Main Cerebral, declares a red five emergency, and orders "abandon ship".  Most of the crew and passengers leave in lifepods.  Captain Montaine (Christopher Cary) stays on the bridge.  Cerebral begins to evacuate the ships compartments of oxygen.  Third astrogator G.W. Simmons is caught on deck 16 while it still retains an atmosphere.  He runs into Fiona Harrison (Kristine DeBell) a passenger from deck 15 who did not leave with the others because she had to go back for her bird, Dwayne.  Because life support has been shut down for all the decks below, Simmons and Fiona head to the bridge via an elevator.  When the doors open on deck one they are met by news reporter Roz Keshah (Carl Lumbly) from level 3, he is wearing a tiny and discrete, optical head-mounted display/camera predictive of Google Glass.  He has found two people, Whitestar Lines director and major stockholder Lloyd DeMatte and his companion the Lady Lima.

On the biobridge the five meet Captain Montaine, who is accosted by DeMatte and accused of damaging the Arcturus.  The captain explains the situation, then offers an escape tunnel to a lifepod.  He remains behind, out of duty as the captain and also a strong sense of curiosity.  Fiona accidentally leaves Dwayne behind, who becomes a sort of companion for the captain.

Simmons takes command of lifepod#3; DeMatte attempts to hijack it and is killed in the process. It is revealed that the Arcturus was intended as an interstellar vessel but repurposed for economic reasons by financial-criminal DeMatte, The Main Cerebral is discovered to be the former intended pilot of the interstellar Arcturus, rendered an amnesiac and made cyber-controller of the interplanetary Arcturus by DeMatte; memory restored, he launches the Arcturus on a flight to Sirius, accompanied by Captain Montaine.

==External links==
* 

 


 