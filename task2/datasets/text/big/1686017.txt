Nice Time
 
 
{{Infobox film
| name           = Nice Time
| image          =
| image_size     =
| caption        =
| director       = Claude Goretta Alain Tanner
| producer       =
| narrator       =
| starring       =
| music          =
| editing        =
| distributor    = Curzon Film Distributors
| released       =  
| runtime        = 17 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} 1957 documentary film made by Alain Tanner and Claude Goretta in Britain and included in the third Free Cinema programme at the National Film Theatre, London in May 1957. It won the Experimental Film prize at the film festival in Venice  and much critical praise.

It is approximately 17 minutes in length, and comprises 190 shots of crowds of leisure-seeking people taken over 25 weekends in Londons Piccadilly Circus. There is no narration, and no dialogue; a soundtrack consisting of several folk songs (including the American song "Greenback Dollar" and other skiffle songs) ties shots together into groups, while there is little recorded sound from the scenes shown on screen.

The filmmakers, both in their late twenties, made the documentary on a shoestring budget after receiving a grant of £240 from the British Film Institute. Chief among the films subjects are movies and other entertainment; flirting, sex, and prostitution; and salesmanship and commodity culture.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 