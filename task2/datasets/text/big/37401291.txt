Let Sleeping Cops Lie
{{Infobox film
| name = Let Sleeping Cops Lie Ne réveillez pas un flic qui dort
| image = Delon.jpg
| caption = 
| imagesize = 
| director = José Pinheiro (director)|José Pinheiro
| writer = Frédéric H. Fajardie (novel) Alain Delon José Pinheiro
| cinematography =  Raoul Coutard
| starring = Alain Delon Michel Serrault
| studio = TF1 Films Production Leda Productions Cité Films
| released = 14 December 1988
| country = France
| runtime = 97 minutes
| language = French gross = 802,437 admissions (France) 
}}
 French crime film released in 1988 in cinema|1988, directed by José Pinheiro (director)|José Pinheiro, starring Alain Delon and Michel Serrault. The screenplay is written by Alain Delon and José Pinheiro based on the novel „Clause de style“ by Frédéric H. Fajardie.

The movie tells the story of police inspector Eugéne Grindel (Delon) who investigates the secret illegal ultra-right police organization which mete out justice on a fast track.

„Let Sleeping Cops Lie“ is the last film of a group of popular movies released in the 1980s and starring Alain Delon, which share a visual and narrative style, beginning with Jacques Derays „Three Men to Kill“ (1980). It was the least successfully commercially.

Alain Delon, also being the film’s producer, dedicated the work to the memory of actor Jean Gabin.   

==Synopsis==
Roger Scatti (Serrault) is a conservative police inspector of the old school. He finds intolerable the liberalization of the justice system. More and more people from the underground world slip away from the justice. Finding allies in the police circles, he founded the ultra-right secret illegal organization called „Police devotion“. Its purpose is the immediate punishment, usually death, of the notorious criminals, with no trials or whatever inquiries and no bureaucracy. The activities of the organization are particularly brutal, serving for edification to the public. When the punitive operations of the „Police devotion“ go out of all bounds, the police chief Cazalières (Gérôme) assign a task to inspector Eugéne Grindel (Delon) to investigate the present circumstances.

==Cast==
*Alain Delon as a police inspector Eugéne Grindel
*Michel Serrault as a police inspector Roger Scatti
*Xavier Deluc as Lutz
*Patrick Catalifo as Pèret, Grindels assistant
*Raymond Gérôme as a chief inspector Cazalières
*Serge Reggiani as Le Stéphanois
*Roxan Gould as Jennifer
*Stéphane Jobert as Spiero
*Consuelo De Haviland as Corinne
*Bernard Farcy as a police inspector Latueva
*Féodor Atkine as Stadler
*Dominique Valera as Valles
*Bruno Raffaelli as Zelleim
*Guy Cuevas as Bessoni

==References==
 

==External links==
*  

 
 
 
 
 
 