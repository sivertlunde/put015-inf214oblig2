Padmaragam
{{Infobox film
| name           = Padmaragam
| image          =
| image_size     =
| caption        =
| director       = J. Sasikumar
| producer       = VM Chandi
| writer         = Sreekumaran Thampi (dialogues)
| screenplay     = Sreekumaran Thampi
| starring       = Prem Nazir Jayabharathi Kaviyoor Ponnamma Adoor Bhasi
| music          = M. K. Arjunan
| cinematography = JG Vijayam
| editing        = VP Krishnan
| studio         = MS Productions
| distributor    = MS Productions
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed by  J. Sasikumar and produced by VM Chandi. The film stars Prem Nazir, Jayabharathi, Kaviyoor Ponnamma and Adoor Bhasi in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Prem Nazir
*Jayabharathi
*Kaviyoor Ponnamma
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Sreelatha Namboothiri
*T. R. Omana
*Bahadoor
*K. P. Ummer
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaattuvannu Thottaneram || K. J. Yesudas, Vani Jairam || Sreekumaran Thampi ||
|-
| 2 || Malayalam Beauty || KP Brahmanandan, Sreelatha Namboothiri || Sreekumaran Thampi ||
|-
| 3 || Poonilaave Vaa || S Janaki || Sreekumaran Thampi ||
|-
| 4 || Saandhyatharake || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 5 || Sindhunadee Theerathu || K. J. Yesudas, B Vasantha, Chorus || Sreekumaran Thampi ||
|-
| 6 || Urangaan Kidannaal || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 7 || Ushassaam Swarnathaamara || K. J. Yesudas || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 