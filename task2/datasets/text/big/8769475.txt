Steptoe and Son (film)
 
{{Infobox Film  |
  name     = Steptoe and Son |
  image          = steptoe-and-son-film.jpg|
  caption       = Film poster by Arnaldo Putzu |  
  producer         = Aida Young |
  director       = Cliff Owen | Alan Simpson |
  starring       = Wilfrid Brambell Harry H. Corbett Carolyn Seymour|
  music         = Roy Budd Jack Fishman Ron Grainer  |
  cinematography = John Wilcox |
  editing         = Bernard Gribble |
 studio = EMI Films|
  distributor    = MGM EMI |
  released   =   |
  runtime        = 98 min. |
  country  = United Kingdom  |
  language = English |
 budget = £100,000 Alexander Walker, National Heroes: British Cinema in the Seventies and Eighties, Harrap, 1985 p 114  NAT KING COHEN: To the cinema-going public he is the name at the start of the credits, But to the industry he is a dominant force in production, dustribution, and exhibition
Murari, Tim. The Guardian (1959-2003)   17 Nov 1973: 9.  |
gross = £500,000 Ooh, you are awful, film men tell Tories.
David Blundy. The Sunday Times (London, England), Sunday, December 16, 1973; pg. 5; Issue 7853.  (939 words) - this article Cohen says the budget was £200,000. 
}}
 same name about a pair of rag and bone men. It starred Wilfrid Brambell and Harry H. Corbett as the eponymous characters, Albert and Harold Steptoe respectively.  It also features Carolyn Seymour.

==Plot==

During a gentlemens evening at a local football club, Harold meets one of the acts, a stripper called Zita. After a whirlwind romance the couple are married, although the actual wedding ceremony is delayed when Albert, acting as best man, loses the ring somewhere in the yard. They eventually find it in a pile of horse manure, and since they have no time to clean up their arrival in church is met with looks of disgust.

Harold and Zita fly to Spain for their honeymoon, but Albert refuses to be left behind. This causes Harold considerable frustration and begins to drive a wedge between him and Zita. When they are finally left alone and are beginning to consummate their marriage they are interrupted by Alberts cries of distress from the adjoining room, and discover that he has contracted food poisoning from some of the local cuisine.

Harold is forced to fly home with Albert, leaving Zita in Spain. Back home Albert makes a suspiciously fast recovery while Harold waits for Zita to write. When he finally receives a letter from her, the news is not what he had hoped for; after trying unsuccessfully for several days to get a plane back to England she has taken up with a British holiday rep at the Spanish hotel. Harold is heartbroken, and, despite his earlier scheming to get rid of Zita, Albert is genuinely sympathetic.
 Three Wise Men bearing gifts replaced by three tramps selling rags, and the Star of Bethlehem being represented by the lights of an airliner.

It appears that the child is Zitas. They name the child after the priest who officiates the christening. Unfortunately for Harold, he is also called Albert. Harold compromises by naming him Albert Jeremy and calling him Jeremy thereafter, although Albert Sr. insists on calling him Albert.

Zita apparently returns and takes the baby back while Albert, who should be looking after him, is asleep. Harold tries to find her and comes across her stripping in a local rugby club where she is soon forced into the scrum of cheering rugby players. Attempting to save her, Harold is beaten up and is only rescued when Zitas musician saves him. Hustled into a back room he hears a babys cries but when he pulls back a curtain a mixed-race baby is there instead. It turns out that Zita and her musician, who is black, are now a couple. Harold then learns that he is not, after all, the father of her first child.

==Cast==
* Wilfrid Brambell as Albert Steptoe
* Harry H. Corbett as Harold Steptoe
* Carolyn Seymour as Zita Steptoe
* Arthur Howard as Vicar
* Victor Maddern as Chauffeur
* Fred Griffiths as Barman
* Joan Heath as Zita Steptoes mother
* Fred McNaughton as Zita Steptoes father
* Lon Satton as Pianist
* Patrick Fyffe as Arthur (as Perri St. Claire)
* Patsy Smart as Mrs. Hobbs Mike Reid as Compere
* Alec Mango as Hotel Doctor
* Michael Da Costa as Hotel Manager
* Enys Box as Traffic Warden
* Barrie Ingham as Terry

==Reception==
The film was popular at the box office and made a profit of six times its cost.  

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 