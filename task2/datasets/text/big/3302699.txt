The Ghost of Frankenstein
 
{{Infobox film
| name           = The Ghost of Frankenstein
| image          = The Ghost of Frankenstein movie poster.jpg
| caption        = The Ghost of Frankenstein movie poster
| director       = Erle C. Kenton Eric Taylor
| starring       = Lon Chaney Jr. Cedric Hardwicke Ralph Bellamy Lionel Atwill Béla Lugosi Evelyn Ankers
| music          = Hans J. Salter (Library music from The Wolf Man Woody Bredell Milton R. Krasner
| editing = Ted Kent
| producer       = George Waggner
| distributor    = Universal Pictures
| released       =  
| runtime        = 67 minutes
| language       = English
| budget         = 
| music          =
| country        = United States 
}} monster horror series of the Monster, taking over from Boris Karloff, who played the role in the first three films of the series, and Béla Lugosi in his second appearance as the demented Ygor.  The supporting cast features Lionel Atwill, Cedric Hardwicke, Ralph Bellamy and Evelyn Ankers.

==Plot==
 
The residents of the village of Frankenstein feel they are under a curse and blame all their troubles on Frankensteins monster. Rumors circulate about Ygor who is still alive and supposedly trying to revive the monster. The villagers pressure the Mayor into allowing them to destroy Frankensteins castle. Ygor (Béla Lugosi) attempts to put up some resistance, but the villagers rush the gates and begin to destroy the castle. Ygor, fleeing through the catacombs, finds the monster released from his sulfuric tomb by the explosions. The exposure to the sulfur weakened the monster but also preserved him. Unseen by the villagers, Ygor and the monster flee the castle to the surrounding countryside; there they encounter a powerful thunderstorm. The monster is struck by a bolt of lightning, but instead of being harmed by it, he seems to be rejuvenated. Ygor decides to find Ludwig, the second son of the original Frankenstein, to help the monster.
 William Smith) kicks the girls ball onto the roof. The monster quickly befriends a young girl, Cloestine Hussman (Janet Ann Gallow). The monster scoops the little girl up in his arms and carries her onto a nearby roof to retrieve her ball, killing two villagers in the process who attempted to intervene. After Cloestine asks the monster to take her to daddy, the monster returns the girl to her father Herr Hussman (Olaf Hytten) and is immediately captured by the entire police force.

The town prosecutor, Erik Ernst (Ralph Bellamy), comes to Ludwig Frankenstein and asks him to examine the giant they have captured. Frankenstein says he will comply after he finishes some work. Soon, Ygor pays Frankenstein a visit informing him that the giant at the police station is the monster. Ygor implores the Doctor to heal the monsters sick body and brain. Frankenstein refuses, not wanting the monster to ruin his life as it did for his father and brother. Ygor threatens to reveal Ludwigs ancestry to the villagers and forces him to give in.

At the police station, the monster is restrained with chains as a hearing is conducted to investigate the murder of the two villagers. The monster does not respond to any questions. Ludwig Frankenstein then arrives and the monster shows signs of recognizing him. When Ludwig Frankenstein denies knowing him, the monster goes berserk and breaks free. Ygor leads the monster away.

While alone in her fathers study, Elsa (Evelyn Ankers), Frankensteins daughter, finds the Frankenstein journals and reads them, learning the story of the monster. She then sees the monster and Ygor in the window and screams. Then, Ygor and the monster break into Frankensteins laboratory and the monster kills Dr. Kettering. The monster grabs Elsa, but Ludwig Frankenstein is able to subdue him with knockout gas. When Elsa revives, Ludwig tells her of Ketterings death and promises her that he will not let this curse from the past separate them.

Ludwig Frankenstein is examining his fathers creation when the monster revives and tries to kill him. Ludwig is able to tranquilize the monster and then tries to enlist Bohmers aid in dissecting the monster. Bohmer refuses claiming it would be murder but Ludwig is determined to destroy the Monster, even if he must do it alone. While studying his familys journals, Ludwig is visited by the ghost of his father Henry Frankenstein (also portrayed by Sir Cedric Hardwicke). The spirit implores him to perfect his creation rather than to destroy it by giving the creature a good brain.

Ludwig Frankenstein calls in Bohmer and Ygor and tells them that he plans to put Dr. Ketterings brain into the monsters skull. Ygor protests and asks Ludwig to use his brain instead as Ygors broken body reflects the multiple attempts to kill him, including Ludwigs older brother. Ludwig refuses insisting "that would be a monster indeed". Ludwig then charges the Monster to give him strength for the operation. Elsa protests to her father telling him to stop his experiments, but he refuses, choosing to operate on the patient as soon as possible. Ygor later explains to the monster that he will receive a new brain. Ygor also taunts Bohmer, telling him that he shouldnt be subordinate to Frankenstein. Ygor promises to help the disgraced doctor if he agrees to put Ygors brain into the monster. Bohmer ponders the possibilities.

The police soon arrive at Frankensteins house, searching for the Monster. They find the secret room, but Ygor and the monster have fled. The monster abducts Cloestine, his young friend, and returns with her in his arms to Frankensteins chateau. The monsters reason for abducting her soon becomes clear... he wants the girls brain in his head. When Ygor protests, the Monster violently pushes him aside injuring Ygors spine. Cloestine does not want to lose her brain and the monster reluctantly gives her to Elsa. Ludwig Frankenstein then performs the surgery believing he is putting Ketterings brain in the monster. Bohmer however has substituted Ygors brain for that of Ketterings.

In the village, Herr Hussman rouses his neighbors by telling them his daughter has been captured by the Monster and that Ludwig Frankenstein is harboring the creature. They race to the chateau but Erik Ernst convinces the group to give him five minutes to convince Ludwig Frankenstein to give up the monster. Ludwig admits he has the monster and agrees to show him to Erik thinking Ketterings brain is in his skull. Upon Ludwig and Erik arriving in the room, The monster rises and Frankenstein is shocked to hear Ygors voice come from the monsters mouth.

The villagers now storm the chateau and the Ygor-Monster decides to have Bohmer fill the house with gas to kill them. Frankenstein tries to stop him, but the Ygor-Monster repels the attack and mortally wounds Ludwig. The villagers find the Hussman girl and run from the building, fleeing the deadly gas. The Ygor-Monster suddenly goes blind and calls for Bohmer. The wounded Ludwig states "Your dream of power is over Bohmer. You didnt realize his blood is the same type as Ketterings but not the same as Ygors. It will not feed the sensory nerves." The Ygor-Monster accuses Bohmer of tricking him and asks "What good is there a brain without eyes to see?" The Ygor-Monster then throws Bohmer onto the apparatus electrocuting him and then inadvertently sets fire to the chateau. This brings about his own demise as he is unable to get out of the chateau while Erik and Elsa walk off toward the sunrise together.

==Cast==
  the Monster
* Cedric Hardwicke as Dr. Ludwig Frankenstein and Henry Frankensteins ghost
* Ralph Bellamy as Erik Ernst
* Lionel Atwill as Dr. Theodore Bohmer
* Béla Lugosi as Ygor
* Evelyn Ankers as Elsa Frankenstein
* Janet Ann Gallow as Cloestine Hussman
* Barton Yarborough as Dr. Kettering
* Doris Lloyd as Martha
* Leyland Hodgson as Chief Constable
* Olaf Hytten as Hussman
* Holmes Herbert as Magistrate Michael Mark as Councillor (uncredited) 
 

==Production== Wolf Man character), and continuing for the rest of the Universal cycle, Frankensteins Monster would be part of an ensemble cast of creatures.

The blinding of the Monster resulted in a lasting stereotype of the creature walking with arms outstretched, even though this is the only film in which it is explicitly indicated that he is blind, such references being cut by the studio from Frankenstein Meets the Wolf Man, sabotaging Lugosis performance in the process, since the audience is left to wonder why the Monster is behaving so peculiarly. The Monsters ability to speak would also be dropped after this film (Lugosis dialogue was filmed but ultimately deleted from Frankenstein Meets the Wolf Man) until Glenn Strange, playing the monster, spoke briefly in  Abbott and Costello Meet Frankenstein. Abbott and Costello Meet Frankenstein has Chaney make an uncredited second appearance as the Monster during the laboratory escape sequence, replacing a broken footed Glenn Strange in the Monster role.

Despite having been apparently killed at the end of Son of Frankenstein, Ygor was revealed only to have been "maimed by the bullets shot into him by Wolf Frankenstein". There was no mention of a second son of the original Dr. Frankenstein in Son of Frankenstein. Ludwig states that he has lived in this area his entire life, but it is not explained why only Wolf was raised in America.

Ghost of Frankenstein also marked the changeover of the Frankenstein (and Universal Monsters) series from "A-movie" to "B-movie" status, with noticeably reduced budgets and the reuse of actors from previous films. As noted below, footage from this film would even be recycled in a later Frankenstein feature.

The title of the film refers to the fact that Dr. Henry Frankenstein, creator of The Monster in the first Universal Frankenstein (1931 film)|Frankenstein film, appears (played by Hardwicke) as a ghostly apparition to advise Ludwig.

The footage of the Monster scrambling to escape the fire was later reused at the end of House of Dracula even though Glenn Strange plays the Monster in that film.

==See also==

* List of films featuring Frankensteins monster
* Frankenstein in popular culture|Frankenstein in popular culture

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 