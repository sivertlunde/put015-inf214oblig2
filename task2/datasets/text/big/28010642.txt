Joan Rivers: A Piece of Work
 
{{Infobox film
| name = Joan Rivers: A Piece of Work
| image = Joan Rivers A Piece of Work.jpg
| alt = 
| caption = Theatrical release poster
| director = Ricki Stern Anne Sundberg
| producer = Ricki Stern Anne Sundberg Seth Keal
| writer = Ricki Stern
| starring = Joan Rivers Melissa Rivers Kathy Griffin Billy Sammeth
| music = Paul Brill
| cinematography = Charles Miller
| editing = Penelope Falk
| studio = Break Thru Films
| distributor = IFC Films
| released =  
| runtime = 85 minutes
| country = United States
| language = English
| budget = 
| gross = $2,930,687 
}}
Joan Rivers: A Piece of Work is a 2010 documentary film about the life and career of comedienne Joan Rivers. It premiered at the San Francisco International Film Festival at the Castro Theatre on May 6, 2010. 

==Synopsis==
The film follows Joan Rivers for 14 months, mostly during the 76th year of her life.    The film made an effort to "  away the mask" and expose the "struggles, sacrifices and joy of living life as a ground breaking female performer."    The New York Times reported that Stern and Sundberg "lucked out" with their timing, starting to film the year before Rivers won Donald Trumps Celebrity Apprentice in 2008. 

==Cast==
:All appearing as themselves
* Joan Rivers
* Melissa Rivers
* Kathy Griffin
* Don Rickles

;Archive footage
* Donald Trump
* Phyllis Diller
* Annie Duke
* Johnny Carson
* George Carlin

==Critical reception==
The film earned very high critical praise. It scored a 79 from Metacritic based on 34 reviews.  As of May 7, 2012, the film has a score of 91% on Rotten Tomatoes based on 94 reviews. 

==Film festivals==

===Awards=== Sundance Film Festival (2010) 

===Appearances===
*Tribeca Film Festival (2010)    
*San Francisco International Film Festival (2010)   

==Release==

===Box office===
The film had a limited release on June 11, 2010.  http://boxofficemojo.com/movies/?page=weekend&id=joanrivers.htm - Joan Rivers: A Piece of Work (2010) - Weekend Box Office Results - Box Office Mojo  In its opening weekend, the film made $164,351 while playing at 7 theaters.  

===Home media=== theatrical trailer, 10 deleted scenes, and TV spots.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 