Sambhavam
{{Infobox film
| name = Sambhavam
| image =
| caption =
| director = P Chandrakumar
| producer = Babu Majeendran John Paul Kaloor Dennis (dialogues)
| screenplay = Kaloor Dennis Madhu Srividya Adoor Bhasi Jose Prakash
| music = V. Dakshinamoorthy
| cinematography = Anandakkuttan
| editing = G Venkittaraman
| studio = Chaithanya Films
| distributor = Chaithanya Films
| released =  
| country = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by P Chandrakumar and produced by Babu and Majeendran. The film stars Madhu (actor)|Madhu, Srividya, Adoor Bhasi and Jose Prakash in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
   Madhu 
*Srividya 
*Adoor Bhasi 
*Jose Prakash 
*Prameela 
*Sankaradi 
*Cochin Haneefa 
*Sukumaran  Ambika 
*Baby Chakki 
*K. P. Ummer  Kunchan 
*MG Soman 
*Mala Aravindan  Meena 
*Ravi Menon  Seema 
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sathyan Anthikkad. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Pakalo paathiravo || K. J. Yesudas, V. Dakshinamoorthy || Sathyan Anthikkad || 
|- 
| 2 || Sindoorathilakamaninju Vaanam || K. J. Yesudas || Sathyan Anthikkad || 
|- 
| 3 || Vayalinnoru kalyanam || K. J. Yesudas, S Janaki, Chorus || Sathyan Anthikkad || 
|- 
| 4 || Venmukil peeli choodi thennalil || K. J. Yesudas || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 


 