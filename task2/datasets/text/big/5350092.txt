Aaru (film)
 
 
{{Infobox film
| name           = Aaru
| image          = Aaru DVD cover.jpg
| image_size     =
| caption        = International release DVD cover Hari
| Saran
| Hari
| narrator       = Surya Trisha Krishnan Vadivelu Ashish Vidyarthi
| music          = Devi Sri Prasad Priyan
| editing        = V. T. Vijayan
| studio         = Gemini Productions
| released       = 9 December 2005
| runtime        = 168 min
| country        = India
| language       = Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 masala film Telugu under the same title. The movie dubbed in Hindi as The Return of Ghajini. 

== Cast == Surya as Aarumugham aka Aaru Trisha as Mahalakshmi
* Vadivelu as Sumo (Sundi Mothiram)
* Ashish Vidyarthi as Vishwanath
* Kalabhavan Mani
* Malavika Avinash Aishwarya as Saroja
* Jaya Prakash Reddy as Reddy
* Tharika
* Thambi Ramaiah
* Delhi Ganesh
* Nizhalgal Ravi
* Madan Bob
* Chaams (possibly uncredited)

==Plot==
Aarumugam, nicknamed Aaru (Surya Sivakumar), is a thug and dealmaker with a band of boys handpicked from the slums of Chennai. Aaru’s mentor Viswanathan (Ashish Vidyarthi) protects him, because Aaru gives him the violent undercover he needs. When Vishwanthan declares war on his bitter rival Reddy (Jaiparakash Reddy), Aaru assists him. But Vishwanthan betrays him, resulting in the deaths of Aarus subordinates. Aaru thus realises Viswanathans true, evil nature and vows to destroy his power. There is also a subplot involving Aarus romance with the college student Maha (Trisha Krishnan).

== Crew == Hari
* Music – Devi Sri Prasad
* Production – Saran
* Distributor – Saran
* Fights – Rocky Rajesh

==Production==
After directing Ayya (2005 Tamil film)|Ayya, Director Hari announced his next project Aaru with Surya, film was launched and shooting was started in July 2005.    Director Saran made his debut as a producer in this film by floating a production house "Gemini Productions". 

==Soundtrack==
{{Infobox album |  
| Name = Aaru
| Type = soundtrack 
| Artist = Devi Sri Prasad 
| Cover = 
| Released = 2005
| Recorded = Feature film soundtrack 
| Length = 
| Label = Star Music Tamil
| Producer = Devi Sri Prasad
| Reviews =
| Last album = Sachien (2005)
| This album = Aaru (2005)
| Next album = Santosh Subramaniam (2008)
}}

The soundtrack was composed by Devi Sri Prasad and released by Star Music in India and Ayngaran in other international territories. The album was made up of 6 tracks. The audio was launched in a soft manner. 

* Soda Bottle – Shankar Mahadevan, Saaki Mukhesh
* Paakatha – Tippu (singer)|Tippu, Sumangali
* Freeya Vudu – Mukhesh, Karthikeyan, Vadivelu, Jassie Gift, Giresh
* Thottutea – Karthik (singer)|Karthik, Sunitha Sarathy
* Nenjam Enum (Bit) – Gopika, Poornima Hariharan
* Nenjam Enum – Srinivas (singer)|Srinivas, Kalpana

The soundtrack was re-recorded for the Telugu release but retained most of the original singers.

==Release==
The film was released in 9 December 2005 alongside Vetrivel Sakthivel, Sandakozhi. The film was given an "A" certificate by the Indian Censor Board due to excessive violence.  After the stupendous success of Ghajini, Surya has an opening similar to as Aaru releases with approximately 171 prints in Tamil Nadu, the opening weekend tickets for Surya’s Aaru have been sold out in Chennai city theatres. 

==Reviews==
Sify wrote:"Aaru offers neither insight nor content-just unmitigated violence". 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 