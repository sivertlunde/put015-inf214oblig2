Our Short Life
{{Infobox film
| name           = Our Short Life
| image          = 
| caption        = 
| director       = Lothar Warneke
| producer       = Horst Hartwig
| writer         = Regine Kühn Brigitte Reimann Lothar Warneke
| starring       = Simone Frost
| music          = 
| cinematography = Claus Neumann
| editing        = 
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = East Germany
| language       = German
| budget         = 
}}

Our Short Life ( ) is a 1981 East German drama film directed by Lothar Warneke. It was entered into the 12th Moscow International Film Festival.   

==Cast==
* Simone Frost as Franziska Linkerhand
* Hermann Beyer as Schafheutlin
* Gottfried Richter as Trojanowicz
* Dietrich Körner as Professor Reger
* Christian Steyer as Jazwauk
* Christine Schorn as Gertrud
* Barbara Dittus as Frau Hellwig
* Dieter Knust as Verwalter
* Helmut Straßburger as Kowalski
* Annemone Haase as Frau Kowalski
* Uwe Kockisch as Wolfgang

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 