The Masked Rider (film)
 
{{Infobox film
| name           = The Masked Rider
| image	         = The Masked Rider FilmPoster.jpeg
| caption        = Film poster
| director       = Aubrey M. Kennedy
| producer       = William Steiner Patrick Sylvester McGeeney (Shamrock Motion Pictures)
| writer         = Aubrey M. Kennedy
| starring       = Harry Myers Ruth Stonehouse
| music          = 
| cinematography = Jacob A. Badaracco Arthur Boeger
| editing        = 
| distributor    = Arrow
| released       =  
| runtime        = 15 episodes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 drama film serial directed by Aubrey M. Kennedy.   

==Cast==
* Harry Myers - Harry Burrel
* Ruth Stonehouse - Ruth Chadwick, Harrys sweetheart
* Paul Panzer - Pancho, Cattle rustler & bandit
* Edna M. Holland - Juanita, Panchos daughter
* Marie Treador - Ma Chadwick, Ruths mother
* Blanche Gillespie - Blanche Burrel, Harrys younger sister
* Robert Taber - Santas, Panchos lieutenant
* George Chapman - Captain Jack Hathaway of the Texas Rangers
* Boris Karloff - Mexican in saloon, Ch. 2
* George Murdock
* George Cravy

==See also==
* Boris Karloff filmography
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 