Chennaiyil Oru Naal
{{Infobox film
| name = Chennaiyil Oru Naal
| image = Chennaiyil Oru Naal Movie poster.jpg
| caption = 
| director = Shaheed Kader
| producer = Raadhika Sarathkumar Listin Stephen
| story = Bobby-Sanjay
| screenplay = Bobby-Sanjay Ajayan Bala Cheran Prakash Prasanna Raadhika Sarathkumar Gabriella Charlton Parvathi Menon Iniya
| music = Mejo Joseph
| cinematography = Shehnad Jalal
| editing = Mahesh Narayanan
| studio = I Pictures Magic Frames
| distributor = 
| released =  
| runtime = 119 minutes
| country = India
| language = Tamil
| budget = 
| gross = 
}} Bobby and hyperlink format. A multi-narrative thriller that intertwines multiple stories around one particular incident, the film is inspired from an actual event that happened in Chennai.  It was titled Naangu Vazhi Saalai earlier.  The film released on 29 March 2013.  The film opened highly positive reviews.  The film was declared Blockbuster at the box office. 

==Plot==
 
On September 16, superstar Gautham Krishna (Prakash Raj) gets ready for the release of his new film. On the same day, Traffic Constable Satyamoorthy (Cheran (director)|Cheran) joins back on duty, after having been suspended from service for taking bribes. The day is special for Dr. Robin (Prasanna (actor)|Prasanna) who is celebrating his first wedding anniversary. Karthik Vishwanathan (Sachin), an aspiring television journalist, starts his first job with an interview with Gautham Krishna the very same day.  On the same day, at a crowded traffic junction in Chennai, Karthik, who was set to interview Gautham Krishna, and Ajmal (Mitun), traveling in a bike are hit by a speeding car at the signal, and Karthik is rushed to Global Hospital. Also at the junction, in another car, was the surgeon Dr. Robin.  Karthik goes into coma and is declared brain dead although he is kept alive using ventilator. Meanwhile, Gauthams ailing daughters (Gabriella Charlton)heart condition becomes worse and she urgently needs a heart transplant. At first Karthiks parents do not agree to take their son off the ventilator and donate their sons heart but Ajmal and Karthiks girlfriend (Parvathi Menon) persuade them into it. Now that the heart is available, the problem was transporting it from Chennai to Vellore. No chartered flights or helicopters were available and so the heart had to be taken by road. Someone had to drive the 150 kilometers in under two hours during the rush traffic.  
City Police Commissioner Sundara Pandian (R. Sarathkumar) is asked to carry out the mission. He initially refuses considering the complexity and risk involved in the mission, but finally heeds to the persuasion of Dr. Arumainayagam (Vijayakumar (actor)|Vijayakumar).  Satyamoorthy, being an experienced driver who has driven for ministers, volunteers to be the driver of the Jeep carrying the heart. Mainly takes this initiative to regain the name he lost to the bribe incident. Accompanying him on the mission was Dr. Robin and Ajmal. As instructions are given from Sundara Pandian, Satyamoorthy follows the fastest route to the hospital. However, at some point, they lose connection and the vehicle mysteriously disappears. Under pressure and stress, Satyamoorthy trusts his own instincts and takes his own route. Sundara Pandian loses patience and hope that hell be able to transport the heart in time, resulting in cancelling the mission.Just in time, Satyamoorthy manages to get connection, which motivates every one to get the heart safely in time. As the mission proceeds, it was discovered that before Dr. Robin committed to accompany Satyamoorthy on this mission, he had attempted to kill his wife, due to her affair with his best friend. In hopes to escape from the police, Robin tries to sabotage the mission, but Gauthams wife (Raadhika Sarathkumar) convinces him to save her daughters life. He not only felt a mothers pain, but as a doctor realized the value of a life. After startles and twists, all goes well and they reach the hospital in time to save the girls life just as Karthiks father (Jayaprakash) and mother (Lakshmy Ramakrishnan) prepare to cremate their son.

==Cast==
 
* R. Sarathkumar as Sundara Pandian, Traffic Police Commissioner, Chennai City Cheran as Satyamoorthy, Traffic Police Constable
* Prakash Raj as Gautham Krishna, Movie Star
* Prasanna as Dr. Robin Joseph
* Raadhika Sarathkumar as Gauthams wife
* Parvathi Menon as Aditi
* Iniya as Swetha as Dr. Robin Joseph wife Mallika as Satyamoorthis wife
* Sachin as Karthik Vishwanathan
* Mitun as Ajmal
* Aishwarya Devan as Jennifer Mary Tony
* Gabriella Charlton as Ria Vijayakumar as Dr. Arumainayagam, Minister
* Jayaprakash as Karthiks father
* Lakshmy Ramakrishnan as Karthiks mother Subbu Panchu as Murugan, Gauthams secretary Kitty as Sethuraman Devan as Dr. Senthil
* Arun Ittachan as Camera Man
* Manobala as Sathyamoorthys friend
* Bala Singh
* Akshara
* Vaishali
* RJ Dheena Suriya in a cameo appearance Sneha in a cameo appearance
* Santhana Bharathi
 

==Production== Prasanna and Prithviraj was approached to play one of the lead roles in the film but refused the opportunity citing date issues. 

The film was officially launched on June 21, 2012 with Raadhika subsequently listing out the members of the cast on her Twitter page. She also went on to confirm that Parvathi Menon would make a comeback to Tamil cinema with her role in the venture. 

==Soundtrack==
Mejo Joseph who composed the original film had composed the songs thus marking his debut in Tamil.

*Kanavey (Female) - Swetha
*Un Tholil Saaya - Srinivasan
*Iravin - Sithara
*Kanavey (Male) - Kaushik Menon
*En En Yenum - Tippu
*Theme Music - Sangeetha Sreekanth
*Massetonia - Srinivasan

==Critical reception==
Behindwoods wrote:"On the whole, we have a movie which offers excitement and also educates us about a vital concept which can prove to be a life-changer to any of us in need". 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 