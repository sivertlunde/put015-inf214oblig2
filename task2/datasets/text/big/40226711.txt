The Pretty One
{{Infobox film
| name           = The Pretty One
| image          = The Pretty One.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Jenée LaMarque
| producer       = Robin Schorr Steven J. Berger
| writer         = Jenée LaMarque
| starring       = Zoe Kazan Jake Johnson Ron Livingston Sterling Beaumon John Carroll Lynch
| music          = Julian Wass
| cinematography = Polly Morgan
| editing        = Kiran Pallegadda
| studio         = Provenance Pictures RCR Pictures
| distributor    =
| released       =  }}
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $13,769 
}}

The Pretty One is a 2013 comedy drama film directed and written by Jenée LaMarque. The film stars Zoe Kazan, Jake Johnson, Ron Livingston, Sterling Beaumon and John Carroll Lynch. 

== Plot ==
Laurel and Audrey are identical twins. Although they look the same on the outside, their personalities are very different. When they get in a severe car crash together, Laurel is taken to the hospital and is informed that her sister has died. Suffering from post-traumatic amnesia, Laurel doesnt remember being who she is at first. The morning of her sisters funeral, she remembers who she is but decides to keep everyone thinking that it was she who had died and not Audrey. She then flies back to the city where Audrey used to live. There, she meets her sisters tenant Basel (Jake Johnson) who is confused about her sudden change of personality, because Audrey was never nice to him before. Living her life posing as her sister, she learns that Audrey had a boyfriend who is married, but broke up with him before the accident. Laurel continues to spend more time with Basel and Audreys best friend Claudia, who is suspicious about Audreys change of behavior. Laurel begins a relationship with Basel, and they fall in love. When she finally feels that she is adapting to her new life, she accidentally tells her boss that Charles is her ex-boyfriend, not knowing that Charles is actually her bosss husband, and gets fired immediately. Shortly after this, Basel proposes to Laurel saying that he loved her since he first met her. Laurel, unable to continue deceiving everyone, decides to tell Basel the truth, but he is devastated and breaks up with her. 

Laurel returns to her fathers home and he is shocked after she tells him the truth. They have another funeral for Audrey, since the first one was for Laurel. After this, Laurel realizes how much people cared for both her and her sister, helping her come to terms with her own insecurities about being the less fortunate one of the pair. Laurel returns to the city as herself and looks for Basel who still loves her, in the end he forgives her, they share a kiss and start making plans for the future.

== Cast ==
* Zoe Kazan as Laurel & Audrey 
* Jake Johnson as Basel 
* Ron Livingston as Charles 
* Sterling Beaumon as Hunter 
* John Carroll Lynch as Frank
* Danny Pudi as Dr. Rao Jeremy Howard as James
* Frankie Shaw as Claudia
* Sabrina Lloyd as Edith
* Dale Raoul as Mrs. Shoemacher
* Shae DLyn as May
* Bronwyn Cornelius as Perfect mom
* Devan Leos as Funeral Boy
* Luka Jones as Patrick
* Mark Kelly as Ralph

== Production ==
On March 30, 2012, Zoe Kazan and Jake Johnson joined the cast of The Pretty One to play lead roles, Jenée LaMarque has written the script and she will make her directorial debut of her own 2011 black listed script. The filming started on June 1, 2012.    Sterling Beaumon also joined the cast in the middle of June to play as Hunter, a womanizer who is sleeping with his former babysitter.    Later on 22 June, Ron Livingston joined the cast to play Charles, Robin  Schorr and Steven Berger are producing the film.   

=== Filming ===
The filming began in early June 2012 in Los Angeles.  

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 