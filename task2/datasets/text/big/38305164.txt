Jack the Giant Killer (2013 Asylum film)
 
{{Infobox film name           = Jack the Giant Killer image          = caption        = director       = Mark Atkins producer       =   screenplay     = Mark Atkins story          =  starring       =   music          = Chris Ridenhour cinematography = Mark Atkins editing        =  studio         = The Asylum distributor    = The Asylum released       =   }} runtime        =  country        = United States language       = English budget         =  gross          =
}}

Jack the Giant Killer is a 2013 American fantasy film produced by The Asylum and directed by Mark Atkins. A modern take of the fairy tales Jack the Giant Killer and Jack and the Beanstalk, the film stars Ben Cross and Jane March. It is a mockbuster of Jack the Giant Slayer.

== Plot ==
After climbing a giant beanstalk, Jack discovers a land in the clouds populated by evil beasts. When the beasts make their way down to Earth, he must figure out how to get back down and save everyone from the oncoming threat.

== Cast ==
* Ben Cross as Agent Hinton
* Jane March as Serena
* Jamie Atkins as Jack Krutchens
* Vicki Glover as Lisa Russell
* Harry Dyer as Newald Krutchens
* Tanya Winsor as Sharon Mason
* Julian Boote as Nigel Mason
* Jon Campling as Jess Walters
* Steve McTigue as General OShauncey
* Robert Boyle as Sargeant Jones
* Noel Ross as Constable Milton
* Nigel Peever as Oscar Madison

== Release ==
The film was released direct-to-video and on video-on-demand on March 12, 2013. In the tradition of The Asylums catalog, Jack the Giant Killer is a mockbuster of the New Line Cinema/Legendary Pictures film Jack the Giant Slayer. 

== Reception ==
Influx Magazine rated it "D" and called it "pretty bad". 

== References ==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 