Cupid's Rival
 
{{Infobox film
| name           = Cupids Rival
| image          = Cupids Rival 1917 OLIVER BABE HARDY BILLY WEST Arvid E Gillstrom.webm
| caption        = Full film Billy West
| producer       = Louis Burstein
| writer         =  Billy West Oliver Hardy
| cinematography = Herman Obrock Jr.
| editing        = Ben H. Cohen
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 silent comedy film featuring Oliver Hardy.

==Plot==
As described in a film magazine review,  Billy works as the janitor of a studio apartment, and has been ordered by Hyfligher, a rich artist, to bring his breakfast to him. Residing in an adjacent studio is Daub, a poor artist, who has painted a picture of Dough for the landlord in lieu of rent. Doub sees his sweetheart Ethel entering the studio of Hyfligher. He is enraged, and bursts into the room to find Ethel stroking the head of the rich artist. Daub seizes a painting of Ethel and smashes Hyfligher on the head with it. Hyfligher is distracted as the day for the exhibition is coming near. Meanwhile, Mike the elevator operator is chasing a mouse through the building, and the chase leads into Doubs studio where the mouse climbs up the side of the painting of Dough. Mike swings a club at the mouse and misses, tearing the portrait. Mike is horrified, and Doub is heart broken when he returns. Mike comes up with the idea of dressing up as the landlord and sitting in the frame. At the exhibition the people are delighted with the paintings and think they are real. Billy finds out about the ruse and when he sees his enemy Mike sitting in a frame, he arms himself with a club and the chase begins. The guests finally catch up with Billy and mete out a just punishment.

==Cast== Billy West - Billy
* Oliver Hardy - (as Babe Hardy)
* Ethel Marie Burton - (as Ethel Burton)
* Leo White
* Bud Ross
* Joe Cohen
* Ethelyn Gibson - A Model (as Ethlyn Gibson)
* Florence McLaughlin

==Preservation status==
Copies of the film exist in several private collections,  and it has been released on dvd as part of a collection of comedy short films. 

==See also==
* List of American films of 1917
* Oliver Hardy filmography

==References==
 

==External links==
* 
* , Lord Heath website with stills

 
 
 
 
 
 
 
 