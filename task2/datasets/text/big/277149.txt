There's Something About Mary
{{Infobox film
| name           = Theres Something About Mary
| image          = Theres Something About Mary POSTER.jpg
| alt            = 
| caption        = Theatrical release poster Peter Farrelly Robert Farrelly Michael Steinberg Bradley Thomas Charles B. Wessler Frank Beddor Peter Farrelly Robert Farrelly
| story          = Ed Decter John J. Strauss
| narrator       = Jonathan Richman Lee Evans Lin Shaye W. Earl Brown
| music          = Jonathan Richman
| cinematography = Mark Irwin
| editing        = Christopher Greenbury
| distributor    = 20th Century Fox
| released       =  
| runtime        = 119 minutes  
| country        = United States
| language       = English
| budget         = $23 million 
| gross          = $369,884,651 
}} Bobby and Peter Farrelly|Peter. It stars Cameron Diaz, Matt Dillon and Ben Stiller, and it is a combination of romantic comedy and gross-out film.
 American Comedy Blockbuster Entertainment Golden Globe her performance.

==Plot==
In 1985, awkward and shy 16-year-old high-schooler Ted Stroehmann (Ben Stiller) lands a prom date with his dream girl Mary Jensen (Cameron Diaz), only to have it cut short by a painful and embarrassing zipper accident. After the ordeal garners the attention of numerous members of the household and community, Ted is finally carted off to the hospital. He subsequently loses touch with Mary.
 obsessed — Lee Evans), English accent. Tucker, however, turns out to be a fraud himself, as he is an able-bodied, and entirely American, pizza delivery boy who is also in love with Mary. Using slander, Tucker drives away potential rivals, among them NFL quarterback Brett Favre, playing himself.

Ted, aided by Dom, drives down to Florida to reconnect with Mary. Ted seems to have won Marys love, until an anonymous letter exposes his being less than honest about his link to Healy. While Ted confronts Healy and Tucker, Mary is confronted by Dom, who turns out to be her former boyfriend Woogie, who "got weird on her" back in high school, stealing all her shoes. Having found out that Tucker also lied about Marys former love interest, football player Brett Favre, Ted decides that Mary should be with Brett (who was the only one of the suitors who did not resort to deceit to win Mary). After reuniting Brett and Mary, Ted leaves tearfully. But Mary chases after Ted, saying that she would be happiest with him.

The film concludes with the two engaging in a kiss while a guitarist (Jonathan Richman), who periodically narrated the story in song throughout the film, is accidentally shot by Magdas boyfriend, who was trying to shoot Ted so he could win over Mary.

==Cast==
* Cameron Diaz as Mary Jensen/Matthews, an orthopedic surgeon with whom Ted has been in love since high school.
* Ben Stiller as Ted Stroehmann, an awkward and shy young man who meets Mary in high school and, of all the men fighting over her, is the only one who seems to be truly in love with her.
* Matt Dillon as Pat Healy, a sleazy private detective whom Ted hires to track Mary down, only to fall in love with her himself. fetish for womens shoes and a problem with hives. Lee Evans Pompano pizza delivery boy who falls in love with Mary and pretends to be a British architect in order to impress her.
* Lin Shaye as Marys older roommate Magda
* Jeffrey Tambor as Sully
* Markie Post as Sheila Jensen, Marys mother
* Keith David as Charlie, Marys stepfather
* W. Earl Brown as Warren Jensen, Marys mentally disabled brother
* Sarah Silverman as Brenda, Marys sarcastic and obnoxious best friend
* Khandi Alexander as Joanie
* Willie Garson as Dr. Zit Face/high school pal Bob
* Brett Favre as himself, Marys former love interest. The role was originally written-for and offered to NFL quarterback Steve Young, but he turned the role down due to the films coarse nature and was replaced with Favre.  
* Richard Tyson as Detective Krevoy
* Rob Moran as Detective Stabler
* Jonathan Richman as the singing narrator
* Harland Williams (uncredited) as Hitchhiker
* Richard Jenkins (uncredited) as Psychiatrist

==Reception   ==
This  . The film made $369 million worldwide, including $176 million in the U.S. alone.   

 
The film was generally critically acclaimed.   gives the film a score of 69 out of 100 based on reviews from 29 critics. 

Roger Ebert gave it three out of four stars, stating "What a blessed relief is laughter. It flies in the face of manners, values, political correctness and decorum. It exposes us for what we are, the only animal with a sense of humor." 

==Soundtrack==
# "Theres Something About Mary" (Jonathan Richman) – 1:47
# "How to Survive a Broken Heart" (Ben Lee) – 2:47
# "Every Day Should Be a Holiday" (The Dandy Warhols) – 4:02
# "Everything Shines" (The Push Stars) – 2:27
# "This Is the Day" (Ivy (band)|Ivy) – 3:33 Joe Jackson) – 3:36
# "True Love Is Not Nice" (Jonathan Richman) – 2:13 History Repeating" (The Propellerheads feat. Shirley Bassey) – 4:04
# "If I Could Talk Id Tell You" (The Lemonheads) – 2:51 Danny Wilson) – 3:54
# "Margos Waltz" (Lloyd Cole) – 4:01
# "Speed Queen" (Zuba) – 3:44
# "Let Her Go Into the Darkness" (Jonathan Richman) – 1:19
# "Build Me Up Buttercup" (The Foundations) – 2:59 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 