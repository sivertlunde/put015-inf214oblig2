Kaks' tavallista Lahtista
{{Infobox Film
| name           = Kaks tavallista Lahtista
| image          = 
| image size     = 
| caption        = 
| director       = Ville Salminen
| producer       = T. J. Särkkä
| writer         = Reino Helismaa
| narrator       = 
| starring       = Tommi Rinne Leif Wager
| music          = 
| cinematography = Kalle Peronkoski
| editing        = Elmer Lahti Armas Vallasvuo
| distributor    = 
| released       = 20 May 1960
| runtime        = 85 minutes
| country        = Finland
| language       = Finnish
| budget         = 
| preceded by    = 
| followed by    = 
}}

Kaks tavallista Lahtista is a 1960 Finnish drama film directed by Ville Salminen. It was entered into the 10th Berlin International Film Festival.   

==Cast==
* Eemeli - Janitor Vepsäläinen
* Vesa Enne - Janitors son
* Hannu Halonen
* Paavo Hukkinen
* Pentti Irjala - Producer
* Leo Jokela - Champions assistant
* Maija Karhi - Irma Virtanen
* Leni Katajakoski - Eeva
* Annikki Linnoila
* Oiva Luhtala
* Toivo Mäkelä - Managing clerk Puntti
* Pirkko Mannola - Pirkko Järvinen
* Aili Montonen
* Masa Niemi
* Tommi Rinne - Usko Lahtinen
* Ville-Veikko Salminen - European champion
* Pentti Viljanen
* Leif Wager - Toivo Lahtinen

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 