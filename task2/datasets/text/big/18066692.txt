Begging for Love
{{Infobox film
| name = Begging for Love
| image = Begging for Love DVD cover.jpg
| caption = DVD cover for Begging for Love (1998)
| director = Hideyuki Hirayama 
| producer = Tadamichi Abe Sadatoshi Fujimine Hideyuki Takai
| writer = Harumi Shimoda (novel) Wui Sin Chong (screenplay)
| starring = Mieko Harada
| music =
| cinematography = Kozo Shibazaki
| editing = Akimasa Kawashima
| distributor = Toho
| released =  
| runtime = 135 minutes
| country = Japan
| language = Japanese
| budget =
| gross =
}}
 Japan Academy Prize ceremony. 

==Cast==
* Mieko Harada: Terue Yamaoka
* Maho Nonami: Migusa Yamaoka
* Fumiyo Kohinata
* Mami Kumagai
* Jun Kunimura: Saburo Wachi
* Naomi Nishida
* Tsuyoshi Ujiki: Takenori Wachi
* Yuji Nakamura
* Moro Morooka
* Kiichi Nakai: Fumio Chin
* Ai Koinuma: Terue Yamaoka, aged 5

==See also==
*Cinema of Japan
*List of Japanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 


 