Pointed Heels
 
{{Infobox film
| name           = Pointed Heels
|image=File:Pointed Heels lobby card.jpg
|caption=Lobby card
| director       = A. Edward Sutherland
| writer         = Charles Brackett (story) Florence Ryerson (script)
| starring       = William Powell Helen Kane Fay Wray Richard "Skeets" Gallagher
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
}}
 sound musical film from Paramount Pictures starring William Powell, Helen Kane, Richard "Skeets" Gallagher, and Fay Wray. This movie was originally filmed in color sequences by Technicolor, but today those color sequences only survive in black-and-white. One of these color sequences was the "Pointed Heels" ballet with Albertina Rasch and her Dancers. 

The UCLA Film and Television Archive has a complete copy of this movie with all color sequences, but has not released it to anyone. Turner Classic Movies airs the black-and-white television copy of this movie. A print screened at the Hammer Museum in Los Angeles in 2009 contained the color ballet sequence.

==See also==
*List of early color feature films

==References==
;Notes
 

==External links==
* 
* 
*  at the Internet Archive

 

 

 
 
 