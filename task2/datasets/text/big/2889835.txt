Whispering City
{{Infobox film
| name           = Whispering City
| image          = Whispering City poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Fedor Ozep
| producer       = George Marton
| screenplay     = Rian James Leonard Lee
| story          = Michael Lennox Mary Anderson Helmut Dantine Morris Davis
| cinematography = Guy Roe William O. Steiner
| editing        = W.L. Bagier Richard J. Jarvis
| distributor    = Eagle-Lion Films
| released       =  
| runtime        = 98 minutes
| country        = Canada
| language       = English
| budget         = 750,000 (estimated) CAD
| gross          = 
}}
Whispering City (aka Crime City) is a 1947 black-and-white film noir directed by Fedor Ozep.  The movie was filmed on location in Quebec City and Montmorency Falls, Québec, Canada in both English and French.  A French language version entitled La Forteresse, with different actors, was made simultaneously. 

==Plot==
Taking place in Quebec City, the film tells the story of a lawyer and a patron of the arts, Albert Frédéric, who, earlier in life, caused a murder and made it look like an accident for financial gain.  

Later in life, a dying woman tells a reporter the tale of how she thinks the accident was actually murder.  The young American reporter, Mary Roberts, begins investigating the case, unaware that the charming lawyer may be behind it all.  Meanwhile Michel Lacoste, a classical composer, who is supported by Frédéric, is having marriage troubles.  Finally his wife kills herself and leaves the husband a note.  Frédéric sneaks into the apartment, takes the note and convinces the man that he killed her in a drunken rage.  

Michel, whose night was indeed blacked out by drink, cant remember anything.  The lawyer then offers the composer a deal:  kill reporter Mary Roberts in exchange for legal representation that will guarantee to get the younger man off the hook.  The man, seeing no other choice, agrees reluctantly.  The man and woman meet but he does not have the heart to  kill her.  The two begin to fall in love, gradually figure out that the lawyer is the real killer and set about a scheme to drive the lawyer into confessing to the crime.

==Cast==
* Helmut Dantine as Michel Lacoste Mary Anderson as Mary Roberts
* Paul Lukas as Albert Frédéric
* John Pratt as Edward Durant, editor

==Reception==

===Critical response===
Film critic Dennis Schwartz gave the film a mixed review, writing, "Watchable minor film noir, that is competently directed by Fyodor Otsep from a story by George Zuckerman and Michael Lennox. The acting by Paul Lukas and Helmut Dantine is far beyond what you would expect in such a cheapie film. But the narrative has too many coincidental plot points to be believable, though the crisply told story is for the most part entertaining. The film is told in flashback by a tourist guide sleigh driver to two riders in Quebec City." 

==See also==
* List of films in the public domain in the United States

==References==
 

==External links==
*  
*  
*  
*   informational site DVD review at DVD Beaver (includes images)
*  

 
 
 
 
 
 
 
 