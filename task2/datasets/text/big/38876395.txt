A Haunting We Will Go (1949 film)
{{Infobox Hollywood cartoon|
| cartoon_name = A Haunting We Will Go
| series = Noveltoon
| image =
| caption =
| director = Seymour Kneitel
| producer = Sam Buchwald (associate producer) Seymour Kneitel (producer) Izzy Sparber (producer)
| story_artist = Larz Bourne
| animator = Irving Dressler Myron Waldman
| voice_actor = Narration:   (Casper/Ghost Teacher)  Jack Mercer (Turtle/Hunter)
| musician = Winston Sharples
| studio = Famous Studios
| distributor = Paramount Pictures
| release_date = May 13, 1949
| color_process = Technicolor
| runtime = 6 minutes
| movie_language = English Theatrical shorts
}}

A Haunting We Will Go is a 1949 animated short directed by Seymour Kneitel and narrated again by Frank Gallop, featuring Casper the Friendly Ghost. This is also the last short before the character was used in a series of 52 regular theatrical shorts.

== Plot==
Casper the Friendly Ghost, sad that he can make no friends since everyone he meets is afraid of him, hatches an abandoned egg and becomes the emerging little ducks best friend and protector.

== Cast ==
* Jack Mercer as Turtle/Hunter (uncredited)
* Mae Questel as Casper/Ghost Teacher (uncredited)
* Frank Gallop as narrator

== External links ==
* 

 
 
 

 
 
 
 
 
 
 
 


 