Dinner at Eight (film)
{{Infobox film name            = Dinner at Eight image           = Dinner at Eight cph.3b52734.jpg caption         = Film poster director        = George Cukor producer        = David O. Selznick stage play      = George S. Kaufman Edna Ferber screenplay      = Frances Marion Herman J. Mankiewicz additional dialogue = Donald Ogden Stewart starring        = Marie Dressler John Barrymore Wallace Beery Jean Harlow Lionel Barrymore Lee Tracy Edmund Lowe Billie Burke music           = William Axt cinematography  = William H. Daniels editing  Ben Lewis distributor     = Metro-Goldwyn-Mayer released        =   runtime         = 113 minutes language        = English country         = United States budget          = $435,000  .  gross           = $2,156,000 
|}} MGM Studios. play by George S. Kaufman and Edna Ferber, with additional dialogue supplied by Donald Ogden Stewart. Produced by David O. Selznick, the picture was directed by George Cukor.

==Synopsis==

One week before her next society dinner, Millicent Jordan (Billie Burke) receives word that Lord and Lady Ferncliffe, whom she and her husband Oliver (Lionel Barrymore), a New York shipping magnate, had met on holiday in England the previous summer, will visit. The Jordans are overjoyed by this social coup, for while their social rank depends solely upon earned income, the Ferncliffes are hereditary peers of a higher class ranking. However, Millicent remains oblivious to Olivers lack of enthusiasm about the dinner and her daughter Paulas (Madge Evans) preoccupation about the impending return of her fiancé, Ernest DeGraff (Phillips Holmes), from Europe. Millicent fusses about finding an escort for her single female guest, former stage star Carlotta Vance (Marie Dressler), who resides in Europe.

Meanwhile, Oliver Jordan faces distressing news about his shipping business, which has been struck hard by the Great Depression. He is concerned that someone is secretly trying to buy the company stock. Carlotta, a former lover of Jordans, visits him at his office and confesses that she is nearly broke and very eager to sell her stock in the Jordan Shipping Line. Not wishing to financially harm him, she asks Jordan to buy her stock, but he is not financially able to do so.

 
While conversing with Carlotta, Jordan is visited by Dan Packard (Wallace Beery), a rough-talking, nouveau-riche mining magnate (who, as Millicent says, "smells of Oklahoma"). Jordan confides his financial struggles to Packard and asks him to take over some of his stocks (a non-controlling share; i.e., an interest free "loan" to the Jordan Line) until business improves.  With blustering hesitation, Packard agrees to consider Jordans proposition.  Unknown to Jordan, he is the one who has secretly been buying up the stock; he knows it is a valuable asset.  Packard then goes home to brag to his brassy, gold digger wife Kitty (Jean Harlow), that he intends to take over the Jordan Line, and ease into retirement the clearly past-his-prime Oliver Jordan.

Unknown to Packard, Jordan has already asked his wife Millicent to invite the Packards to her dinner party, and to drop the names of Lord and Lady Ferncliffe as being one of the guest couples invited, who the social climbing Kitty is dying to meet, in the hope that it will end Packards (feigned) hesitation to buy the stock. The ill-mannered but socially ambitious Kitty eagerly accepts the invitation but Packard refuses to go, believing he is soon to be appointed to a cabinet position in Washington and is, therefore, far above the Jordans. But as Jordan knew he would, Packard changes his mind at the mention of the Ferncliffes, the richest couple in England. Another of Millicents guests, Dr. Wayne Talbot (Edmund Lowe), has been carrying on a clandestine affair with Kitty while making a pretense of tending to her feigned illnesses.

On the eve of her dinner, Millicent, still short an extra man, telephones Larry Renault (John Barrymore), a washed-up silent movie star, and extends a last-minute invitation, utterly unaware that her daughter Paula is having a covert love affair with him. At Paulas urging, Renault, a three-time divorcé and hardened alcoholic, accepts the invitation, but advises the much younger Paula to forget him and return to DeGraff. After Paula stubbornly refuses to take Renaults admonitions seriously, she is seen leaving his hotel room by Carlotta, who is staying at the same hotel.

Renault is visited by his agent, Max Kane (Lee Tracy), who tells him that the stage play he was planning to star in has lost its original producer. Kane breaks the news to Renault that the plays new producer, Jo Stengel (Jean Hersholt), wants another actor in the lead but is willing to consider Renault for a bit part. Although crushed, Renault agrees to think over the offer, then desperately sends a bellboy to pawn a few of his possessions for another bottle of alcohol.

  in the trailer]]
The next day, Dr. Talbot is discovered by his wife Lucy (Karen Morley) in a compromising telephone call with Kitty ("How about an apple a day?") and confesses that, in spite of his love for her, he is addicted to women and needs help to overcome his weakness. Talbot then rushes to see Jordan, who has come to the Doctors office with severe chest pains.

Although Talbot tries to hide his prognosis of terminal thrombosis of the heart, Jordan deduces the seriousness of his illness. When he returns home, the weakened Jordan tries to explain to Millicent his need for rest, but she is too hysterical to hear because, among other minor disasters, the Ferncliffes have cancelled and are on their way to Florida. Although anxious to tell Millicent about Renault, Paula, too, is turned away by her upset mother and faces the prospect of confronting her fiancé alone.

At the Packards, meanwhile, Kitty reveals to her husband in a fit of anger that she is having an affair. When threatened with divorce, however, Kitty tells her husband that, if he wants his Cabinet appointment instead of a career-stopping revelation from her about his crooked dealings, he must back down from his takeover of Jordans line and treat her with more respect. Things are further complicated when Kittys abused maid, Tina, makes vague but easily blackmailable references to the gifts Kitty has been receiving from Dr. Talbot.

Just before he leaves for the dinner, Renault is visited by Max Kane and Jo Stengel and drunkenly berates Stengel for insulting him with his paltry offer. After a frustrated Kane denounces him for ruining his last career chance and the hotel management asks him to leave, Renault quietly turns on his gas fireplace and commits suicide.

 
At the ill-fated dinner, Carlotta confides Renaults demise in private to Paula, who is just about to break her engagement with Ernest DeGraff. She counsels the young woman to grow up, face reality, and remain with her fiancé who, after all, is handsome and clearly loves her. At the same time, Millicent learns from Talbot about Jordans illness. Finally awakened to her selfishness, Millicent announces to Jordan that she is ready to make sacrifices for the family and become a more attentive wife. Then, as the beleaguered guests are about to go in to dinner, Packard, with prodding from Kitty, tells Jordan that he has put a stop to the (his) hostile takeover of the Jordan shipping line, and that if necessary, the Jordan Lines stock will be strengthend by Packards backing.

The film ends with the guests slowly walking into the dining room for dinner, while Kitty tells Carlotta, "I was reading a book the other day.   Yes, its all about civilization or something. A nutty kind of a book. Do you know that the guy says that machinery is going to take the place of every profession?" Carlotta looks her up and down, and says "Oh, my dear, thats something you need never worry about."

== Primary cast ==
* Marie Dressler as  Carlotta Vance, an aging actress, destitute, dealing with the loss of prestige
* Lionel Barrymore as Oliver Jordan, a kind businessman whose business is failing
* Billie Burke as Millicent Jordan, his wife, a shallow, wealthy socialite
* Madge Evans as Paula Jordan, the Jordans slightly rebellious daughter
* Wallace Beery as Dan Packard, a successful, crooked, bully of a businessman
* Jean Harlow as Kitty Packard, a lonely, conceited woman and wife of Dan Packard
* John Barrymore as Larry Renault, a washed-up, drunken actor
* Lee Tracy as Max Kane, Larry Renaults desperate agent
* Edmund Lowe as Dr. Wayne Talbot, an unfaithful husband, doctor to the rich, especially Kitty Packard
* Karen Morley as Lucy Talbot, Wayne Talbots longsuffering wife
* Jean Hersholt as Jo Stengel, a theatrical agent
* Phillips Holmes as Ernest DeGraff, fiancé of Paula Jordan
* Edwin Maxwell as Mr. Fitch, the hotel manager
* Louise Closser Hale, as Hattie Loomis, a dinner guest Grant Mitchell, as Ed Loomis, a dinner guest

==Reception==
According to MGM records the film earned $1,398,000 in the US and Canada and $758,000 elsewhere resulting in a profit of $998,000.  David Thomson, Showman: The Life of David O. Selznick, Abacus, 1993 p 160 

==Awards and honors==
American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs #85

==Come to Dinner parody== Broadway Brevity parody of Dinner at Eight using look-alike actors.  

==References==
 

== External links ==
 
*  
*  
*  
*  on Campbell Playhouse: February 18, 1940

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 