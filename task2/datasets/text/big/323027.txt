Jailhouse Blues
  musical short film features Mamie Smith, who was a top star in Black Vaudeville and a recording artist with Okeh Records, although by the time Jailhouse Blues was made her contract with Okeh had ended.

==Synopsis==
Mamie is missing her man, and finds him in jail.  She pleads through her singing for his release.

==Related history and preservation status==  Victor Records, "Jailhouse Blues" and "You Cant Do It!" as custom recordings, evidently used for synchronization purposes.  
 DuPont Show of the Week broadcast, and this has served as the source of the widely circulated clips and audio from the film since.

==External links==
*  
*  

 
 
 
 
 
 
 
 