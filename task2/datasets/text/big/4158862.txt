Permanent Record (film)
 Permanent Record}}
 
{{Infobox film
| name           = Permanent Record
| image          = Permanentrecord.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Marisa Silver
| producer       = Frank Mancuso, Jr.
| writer         = Jarre Fees Larry Ketron Alice Liddle Jennifer Rubin
| music          = Joe Strummer
| cinematography = Frederick Elmes
| editing        = Robert Brown
| distributor    = Paramount Pictures
| released       =  
| runtime        = 91 min
| country        = United States English
| budget         = $8 million
| gross          = $1,893,139
}}
 1988 Cinema American drama drama film Jennifer Rubin, and Alan Boyce. It was filmed on location in Portland, Oregon|Portland, Oregon and Yaquina Head near Newport Beach on the Oregon coast.

The movie primarily deals with the profound effect of suicide, and how friends and family work their way through the grief.

== Plot ==
David (Alan Boyce) seems to have everything. He is smart, talented, funny, and popular.  He is best friends with Chris (Keanu Reeves), a quirky outsider. He seems to have it all together, yet as his personal academic expectations and those of his parents become overwhelming, he seemingly is keeping emotional problems secret to himself.

At a party with his school friends along the coast, he takes a walk to the edge of a cliff overlooking the ocean.

Chris, playful as ever, decides to sneak up on his friend, but when he emerges from behind a rock, David is not there. He has fallen to his death.  Originally assumed to be a horrible accident,the situation changes when Chris receives a suicide note in the mail. Chris and Davids girlfriend, Lauren (Jennifer Rubin), want to hold some type of memorial, but a reluctant school decides against it, leaving the kids to memorialize their friend in their own way.

== Reception ==
Permanent Record received mixed reviews from critics upon its release. The film currently holds a 75% rating on Rotten Tomatoes.

Roger Ebert of the Chicago Sun-Times praised the film as one of the best 1988 had to offer, stating all the performances were appropriate to the material, whilst also praising Silver for finding authentic ways to portray emotions. 
 Variety Reviews applauded Reeves performance in the latter half of the film, citing Boyces characters suicide as the primary reason, although also criticizing the female characters in the film. 

Rob Gonsalves of efilmcritic.com criticized the film, stating it was nothing more than a TV-Movie drama film, whilst also criticizing the climax of the story. Although he also praised the performance of Keanu Reeves.  

== Soundtrack == musical score for Permanent Record was composed by Joe Strummer, former member of the punk rock band The Clash. A soundtrack album was released in 1988 and featured five songs by Joe Strummer and the Latino Rockabilly War with Keanu Reeves guest starring on rhythm guitar for the albums opening track, as well as individual tracks by Lou Reed, The Stranglers, BoDeans, The Godfathers, and J. D. Souther. 

{{tracklist
| writing_credits   = no
| extra_column      = Recording artist(s)
| writing_credits   = yes
| title1            = Trash City
| extra1            = Joe Strummer, The Latino Rockabilly War & Keanu Reeves
| writer1           = Joe Strummer
| length1           = 4:12
| title2            = Baby the Trans
| extra2            = Joe Strummer & The Latino Rockabilly War
| writer2           = Joe Strummer
| length2           = 2:19
| title3            = Nefertiti
| extra3            = Joe Strummer & The Latino Rockabilly War
| writer3           = Joe Strummer
| length3           = 2:11
| title4            = Nothin Bout Nothin
| extra4            = Joe Strummer & The Latino Rockabilly War
| writer4           = Joe Strummer
| length4           = 2:31
| title5            = Theme From Permanent Record - Instrumental Score
| extra5            = Joe Strummer
| writer5           = Joe Strummer
| length5           = 3:20
| title6            = Cause I Said So
| extra6            = The Godfathers
| writer6           = The Godfathers
| length6           = 2:46
| title7            = Waiting on Love
| extra7            = BoDeans Kurt Neumann, Sam Llanas
| length7           = 3:52
| title8            = Wishing on Another Lucky Star
| extra8            = J. D. Souther
| writer8           = J. D. Souther
| length8           = 3:45
| title9            = All Day and All of the Night
| extra9            = The Stranglers
| writer9           = Dave Davies, Ray Davies
| length9           = 2:30
| title10           = Something Happened
| extra10           = Lou Reed
| writer10          = Lou Reed
| length10          = 4:00
}}

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 