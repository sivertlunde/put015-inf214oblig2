Go West, Young Man
 
{{Infobox film
| name           = Go West, Young Man
| image          = Go_West_Young_Man_1936_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Henry Hathaway
| producer       = Emanuel Cohen
| screenplay     = Mae West
| based on       =  
| starring       = {{Plainlist|
* Mae West
* Warren William
* Randolph Scott
}}
| music          = Arthur Johnston
| cinematography = Karl Struss
| editing        = Ray Curtiss
| studio         = Emanuel Cohen Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Go West, Young Man is a 1936 American comedy film directed by Henry Hathaway and starring Mae West, Warren William, and Randolph Scott.    Released by Paramount Pictures and based on the play Personal Appearance by Lawrence Riley, the film is about a movie star who gets stranded out in the country and trifles with a young mans affections. The phrase "Go West, Young Man" is often attributed to New York Tribune founder Horace Greeley, and often misattributed to Indiana journalist John B. L. Soule, but the latest research shows it to be a paraphrase. 

==Plot==
Mavis Arden (Mae West), is a movie star who gets romantically involved with a politician. She makes plans to meet him at her next tour stop but her Rolls Royce breaks down and she is left stranded in the middle of a rural town. Her manager arranges for her to stay at a local boarding house. She immediately set her eyes on the young mechanic, fixing her car, Bud Norton, played by Randolph Scott. West sings the Arthur Johnston/John Burke song, I Was Saying to the Moon as she is trying to seduce Scott. 

==Cast==
* Mae West as Mavis Arden
* Warren William as Morgan
* Randolph Scott as Bud Norton
* Alice Brady as Mrs. Struthers Elizabeth Patterson as Aunt Kate Barnaby
* Lyle Talbot as Francis X. Harrigan
* Isabel Jewell as Gladys
* Margaret Perry as Joyce Struthers
* Etienne Girardot as Prof. Herbert Rigby
* Maynard Holmes as Clyde
* John Indrisano as Chauffeur
* Alyce Ardell as Jeanette (French maid)
* Nick Stewart as Nicodemus
* Charles Irwin as Master of Ceremonies Walter Walker as Andy Kelton   
* Raquel Torres Ricos girlfriend

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 

 