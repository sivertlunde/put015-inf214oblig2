La pecora nera (2010 film)
 
{{Infobox film
| name           = La pecora nera
| image          = La pecora nera.jpg
| caption        = 
| director       = Ascanio Celestini
| producer       = Alessandra Acciai
| writer         = Ascanio Celestini Ugo Chiti Wilma Labate
| starring       = Maya Sansa
| music          = 
| cinematography = Daniele Ciprì
| editing        = Giogiò Franchini
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
La pecora nera is a 2010 Italian comedy film directed by Ascanio Celestini. The film was nominated for the Golden Lion at the 67th Venice International Film Festival.   

==Cast==
* Maya Sansa as Marinella
* Giorgio Tirabassi as Ascanio
* Luisa De Santis
* Ascanio Celestini as Nicola
* Barbara Valmorin

==See also==
* Cinema of Italy
* 2010 in film
* Italian films of 2010

==References==
 

==External links==
* 

 
 
 
 
 
 
 