Trapped in Paradise
{{Infobox Film
| name           = Trapped in Paradise
| image          = Trapped in paradise poster.jpg 
| caption        = 
| director       = George Gallo
| producer       = George Gallo
| writer         = George Gallo
| starring       = Nicolas Cage Jon Lovitz Dana Carvey
| music          = Robert Folk
| cinematography = Jack N. Green
| editing        = Terry Rawlings
| distributor    = 20th Century Fox
| released       = December 2, 1994
| runtime        = 111 minutes USA
| rating         = PG-13 English
| budget         = 
| preceded_by    = 
| followed_by    =  gross =$6,017,509 
}}
 crime comedy film written and directed by George Gallo, and starring Nicolas Cage, Jon Lovitz, and Dana Carvey.

==Plot==
  New York rob it if he had a gun. Fortunately, there are guns in the Infiniti sedan Dave and Alvin borrowed. With Alvin driving a stolen decrepit car, Bill and Dave storm the bank. The wife of the banks president (Angela Paton) tells them that the bank safe door is locked and the president, Mr. Clifford Anderson (Donald Moffat), who has the key, is having lunch in the restaurant nearby. While Dave stays in the bank doing yoga breathing with the hostages, Alvin and Bill charge into the restaurant and call in a robbery. They order all customers to come with Bill to the bank so that no one calls the police when they go. Mr. Anderson opens the safes door and Bill checks for motion sensors via a perfume spray. He steps over the sensors, gets the money, but while getting out of the safe, he touches the sensors via one of the money bags. Bill and Dave rush out of the bank and get away with their crime, stealing $275,000.

While trying to get out of town with their car and stolen money, Alvin, whos driving, gets them lost. A police car turns on the sirens and they try to evade getting caught. Because of slick roads, the Infiniti drifts over a bridge and gets wrecked. The police officer does not see them crash and drives past the bridge, but another car stops and offers them a ride. Due to the interstates being closed, the man takes them to his relatives to stay over for Christmas. Upon arriving at the house, they find out it is the house of the bank president Mr. Anderson. However, the relatives dont recognize them because they were covering their faces with ski masks while robbing the bank.

Vic Mazzucci (Vic Manni), the inmate who gave Dave and Alvin the tip about the low security of this bank, gets enraged that they robbed the bank and busts out of jail. He and his henchman, Caesar, takes their mother, Edna (Florence Stanley) hostage and threatens to kill her unless they give him the stolen money.
 John Ashton) and Clovis Minor (John Bergantine), two inept shopkeepers, made "deputies" by the local police chief. Bill tricks the FBI by shooting rounds from Eds gun in the ground making the crowd run around and gets away with his bag. However, they miss their bus. They then try to get away via canoe. The canoe is headed towards a waterfall and Alvin falls into the water. A nearby family pulls Alvin out of water and saves him with CPR.

Alvin steals a horse carriage from the police chiefs son, Timmy. The feds chase the carriage but after they drive into the forest the police cars were unable to continue the chase. They ditch the horse carriage and decide to hitchhike. However, the horse, Merlin, and carriage get pulled into the water making the brothers save him and go to a truck stop instead. There, Bill and Alvin decide to return the money to the bank while Dave refuses, knowing that their mother would be killed if they do so (Bill and Alvin are unaware of this). Alvin, then, reveals to Bill he is not wanted in New York and that hes had his brothers wallet, all along. Upset, Bill leaves his two brothers, then heads off to return the money and asks strangers for a ride to Paradise. By coincidence, he winds up getting a ride with Vic and Caesar, who are holding his mother hostage. Bill shows them his mothers picture, whereupon Vic tries to shoot him so he can get the money Bill has in the bag. Bill jumps out of the car and escapes, rescued by Dave, Alvin and Merlin.
 alarm through church with a letter requesting to return it to the towns people. Trying to get away, Ed and Clovis, who had sold the Firpoes the ski masks before the robbery, recognize them and want the money for themselves. Ed and Clovis grab the brothers and take them to the Anderson house, while followed by the police. Vic and Caesar are holding the Andersons hostage, along with Timmy, Edna and Sarah (Mädchen Amick), Vics daughter and a tenant of the family. Upon entering, Ed gets knocked out, while Clovis and the Firpoes are also taken hostage.

The police sees the license plates in front of the Anderson house of a stolen car and therefore order Vic and Caesar to come out with their hands held up. While the inmates are busy figuring out what to do, they get attacked by Timmy who immobilizes Caesar and shoots Vic. The police rush into the house and take everyone to the office. There, FBI agent Shaddus Peyser (Richard Jenkins) tries to figure out what happened and because the towns people hide what they know about Bill, Alvin, and Dave and the church pastor returns the money to the police, they release them. Bill stays in Paradise to be with Sarah, while Alvin and Dave return with their mother to New York.

==Cast==
* Nicolas Cage as Bill Firpo 
* Jon Lovitz as Dave Firpo 
* Dana Carvey as Alvin Firpo 
* Mädchen Amick as Sarah Collins 
* Florence Stanley as Edna Ma Firpo 
* Donald Moffat as Clifford Anderson 
* Angela Paton as Hattie Anderson 
* Vic Manni as Vic Mazzucci 
* Frank Pesce as Caesar Spinoza  John Ashton as Ed Dawson 
* John Bergantine as Clovis Minor  Sean McCann as Chief Bernie Burnell 
* Richard Jenkins as Agent Shaddus Peyser  
* Sean OBryan as Dick Anderson 
* Gerard Parkes as Father Gorenzel 
* Richard B. Shull as Father Ritter

==Production==
* Filming was done in Ontario, Canada in several cities, and towns. Including Toronto, Niagara-on-the-Lake, and Elora. 
* It is said that production was very unpleasant. Actor Jon Lovitz mentioned the crew began referring to the film as "Trapped in Bullshit".
* At NYU during a master class session revolving around the film, Jon Lovitz admitted that the film was "shit."  

==Reception==
Trapped in Paradise received negative reviews from critics, as the film currently holds a 10% rating on Rotten Tomatoes.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 