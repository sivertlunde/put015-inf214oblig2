The Road to Singapore
 
{{Infobox film
| name           = The Road to Singapore
| image          = 
| image_size     = 
| caption        = 
| director       = Alfred E. Green
| producer       = 
| writer         = Grubb Alexander
| story          = 
| based on       = 1929 play Heat Wave by Roland Pertwee 1930 story by Denise Robins
| narrator       = 
| starring       = William Powell Doris Kenyon
| music          = 
| cinematography = Robert Kurrle William Holmes
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 69-70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Road to Singapore is a 1931   romantic drama film starring William Powell and Doris Kenyon, who play two thirds of a romantic triangle, along with Louis Calhern.

==Plot==
On an ocean liner from Colombo to Singapore, black sheep Hugh Dawltry tries, but fails to become better acquainted with fellow passenger Philippa Crosby. He is pleasantly surprised to find that they are both getting off at Khota. Ashore, she rebuffs his advances again, informing him that she has come to marry Dr. George March, Dawltrys neighbor.

Philippa is sorely disappointed by her marriage, however. George is utterly wrapped up in his work, and does not even take her on a honeymoon. As time goes by, the neglected, unhappy woman begins to find Dawltry more and more attractive. So does Georges 18-year-old sister Rene. Most of the expatriate community shuns him for his involvement in a scandalous, widely publicized divorce.

One day, George plans to take a patient with a very rare disease to Colombo. Dawltry takes the opportunity to invite Philippa to dinner. Before that time, Rene invites herself into his bungalow. When she refuses to leave, Dawltry frightens her into fleeing by sweeping her up in his arms and carrying her into his bedroom.

Philippa shows up at the appointed time. Unfortunately, the patient dies and George cancels his trip. Returning home, he finds Dawltrys invitation, takes his pistol and goes to retrieve his wife. She tells him she is leaving him and drives off in their car. Dawltry sets out after her. As he leaves, he tells George it is his last chance, but George is unable to pull the trigger.

==Cast==
*William Powell as Hugh Dawltry
*Doris Kenyon as Philippa Crosby March
*Marian Marsh as Rene March
*Louis Calhern as Dr. George March
*Alison Skipworth as Mrs. Wey-Smith
*Lumsden Hare as Mr. Wey-Smith
*Tyrell Davis as Nikki (as Tyrrell Davis)
*A. E. Anson as Dr. Muir

==External links==
* 
* 
* 
 

 
 
 
 
 
 
 