Red Dust
 
{{Infobox film
| name           = Red Dust
| image          = Red-Dust-1932-film-poster.jpg
| image_size     =
| caption        = theatrical release poster
| director       = Victor Fleming
| producer       = Hunt Stromberg (uncredited) Irving Thalberg (uncredited)
| writer         = Donald Ogden Stewart (uncredited) John Mahin
| based on       =  
| starring       = Clark Gable Jean Harlow Mary Astor Gene Raymond
| music          =
| cinematography = Harold Rosson Arthur Edeson
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $408,000  . 
| gross          = $1,223,000 
}}
 romantic drama John Mahin.   Red Dust is the second of six movies Gable and Harlow made together, and was produced during the Pre-Code era of Hollywood. More than twenty years later, Gable would star in a remake, Mogambo (1953), with Ava Gardner starring in a variation on the Harlow role and Grace Kelly playing a part similar to one portrayed by Mary Astor in Red Dust.

The film provides a view into the French colonial rubber business.  This includes scenes of rubber trees being tapped for their sap; the process of coagulating the rubber with acid; native workers being rousted; gales that can blow the roof off a hut and are difficult to walk in; the spartan living quarters; the supply boat that arrives periodically; a rainy spell that lasts weeks; and tigers prowling in the jungle.  The films title is derived from the large quantities of dust that are stirred up by the storms.

In 2006, Red Dust was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot==
  and Clark Gable in Red Dust]]

On a rubber plantation in French Indochina during the monsoon season, the plantations owner/manager Dennis Carson (Clark Gable|Gable), a prostitute named Vantine (Jean Harlow|Harlow), and Barbara Willis (Mary Astor|Astor), the wife of an engineer named Gary Willis (Gene Raymond|Raymond) are involved in a love triangle.  Carson abandons an informal relationship with Vantine to pursue Barbara, but has a change of heart and returns to Vantine.
 on the lam from the authorities in Ho Chi Minh City|Saigon.  She displays an easy comfort in the plantations harsh environment, wisecracks continually, and begins playfully teasing Carson as soon as she meets him.  He resists her charm at first, but soon gives in, and they quickly develop a friendly, casual relationship in which they tease each other and pretend to be too tough for affection. One of their favorite games is to call each other "Fred" and "Lily", as though neither can be bothered to remember the others name.

However, Carson loses interest in Vantine when the Willises arrive. Gary Willis is a young, inexperienced engineer, and his wife Barbara is a classy, ladylike beauty.  Carson is immediately attracted to Barbara, and, after sending Gary on a lengthy surveying trip, he spends the next week seducing Barbara as Vantine watches jealously.  He successfully persuades Barbara to leave Gary for him, but recants after visiting Gary in the swamp and learning how deeply he loves Barbara.  Carson has also seen that Barbara is unsuited for the primitive conditions on the plantation, as is Gary, and he has a painful memory of his own mothers death on the plantation when he was a boy. He decides to send both of them back to more civilized surroundings.

At the storys climax, Carson turns Barbaras feelings against himself by pretending that he never loved her, at which point she shoots him. This provides a cover for Vantine and Carson to save Barbaras marriage and reputation by insisting to Gary that Barbara rejected Carsons advances. The film ends after Carson has sent the Willises away, with Vantine reading bedtime stories to him as he recuperates from the gunshot wound and tries to fondle her.

 

==Cast==
* Clark Gable as Dennis Carson
* Jean Harlow as Vantine Jefferson
* Mary Astor as Barbara Willis
* Gene Raymond as Gary Willis
* Tully Marshall as "Mac" McQuarg, overseer
* Donald Crisp as Guidon, overseer
* Willie Fung as Hoy, house servant
* Forrester Harvey as Captain Limey

==Box office==
According to MGM records, the film earned $781,000 in the US and Canada and $442,000 elsewhere resulting in a profit of $399,000. 

==Remake== remade by director John Ford in 1953 as Mogambo, this time set in Africa rather than Indochina and shot on location in color, with Ava Gardner in the Harlow role and Grace Kelly playing Astors part. Clark Gable returned, twenty-one years later, to play the same character.  Ford used African tribal music as the films score.

==Home media==
Red Dust was first released to home media on VHS by M-G-M.  In November 2012, the Warner Archive Collection released the film on manufactured on demand DVD. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 