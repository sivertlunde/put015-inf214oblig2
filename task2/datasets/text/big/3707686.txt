Vicki (film)
{{Infobox film
| name           = Vicki
| image          = Vicki 1953 movie poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Harry Horner
| producer       = Leonard Goldstein
| screenplay     = Dwight Taylor Leo Townsend
| based on       =  
| starring       = Jeanne Crain Jean Peters Elliott Reid Richard Boone
| cinematography = Milton R. Krasner
| music          = Leigh Harline
| editing        = Dorothy Spencer
| distributor    = 20th Century-Fox
| released       =  
| runtime        = 85 minutes
| country        - United States
| language       = English
| budget         = $560,000 
| gross          =
}} Steve Fisher. The film features Jeanne Crain, Jean Peters, Elliott Reid and Richard Boone. 

==Plot==
Vicki Lynn (Jean Peters) is a waitress who is transformed into a fashion model by press agent Steve Christopher (Elliott Reid). When Vicki is murdered, detective Ed Cornell (Richard Boone) tries to blame the crime on Christopher.

In fact, the cop knows who the real killer is, but he is so hopelessly in love with the dead girl Vicki, who, herself , despised him that he intends to railroad an innocent man to the electric chair. With the help of Vickis sister Jill (Jeanne Crain), Christopher tracks down the real killer, Harry Williams (Aaron Spelling) and exposes the crooked cop Cornell, who had manipulated Williams into murdering Vicki.

==Cast==
* Jeanne Crain as Jill Lynn
* Jean Peters as Vicki Lynn
* Elliott Reid as Steve Christopher
* Richard Boone as Lt. Ed Cornell
* Casey Adams as Larry Evans
* Carl Betz as Detective MacDonald
* Aaron Spelling as Harry Williams
* Alexander DArcy as Robin Ray
* John Dehner as Police Captain

==Background==
Vicki is a remake of the 1941 film I Wake Up Screaming starring Betty Grable, Victor Mature, and Carole Landis. 

==Reception==

===Critical response===
Film critic Bosley Crowther certainly did not like the screenplay, but seemed to appreciate the acting.  He wrote, "Meanwhile, the rest of the performers—Jean Peters, as the girl who gets killed; Jeanne Crain, as her misgiving sister; Mr. Reid and several more—make the best of Harry Horners brisk direction to make it look as though theyre playing a tingling film. It might be, indeed, if the story were not so studiously contrived and farfetched, and if Mr. Boone did not wear a label that virtually says, Im IT." 

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)
*  

 
 
 
 
 
 
 
 
 