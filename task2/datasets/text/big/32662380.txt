Vallinam
{{Infobox film
| name           = Vallinam
| image          = Vallinam Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Arivazhagan Venkatachalam
| producer       = Viswanathan Ravichandran
| screenplay     = Arivazhagan Venkatachalam
| story          = Arivazhagan Venkatachalam Nakul Mrudhula Basker Atul Kulkarni
| music          = S. Thaman
| cinematography = Bhaskaran K.M
| editing        = Sabu Joseph VJ Aascar Film Pvt. Ltd
| distributor    = 
| released       = February 28, 2014
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} sports drama Nakul and newcomer Mrudhula Basker in the lead roles. Vallinam opened to positive reviews from critics and audiences.

==Cast== Nakul as Krishna
* Mrudhula Basker as Meera
* Siddharth Jonnalagadda as Vamsi
* Anaswara Kumar as Anu
* Amzath Khan as Guna
* Atul Kulkarni
* Jagan as Jagan
* Y. Gee. Mahendra
* Anupama Kumar
* Chandru
* Mathivanan Rajendran
* Jayaprakash as Krishnamurthy
* Santhana Bharathi as Puzhaldasan Kreshna as Shiva (Cameo) Aadhi (Cameo)
* RJ Balaji (Cameo)
* SaravanaPraksh as Balaji

==Production==

The film was earlier titled as Acham Thavir and planned with Arulnithi in the lead role, who later opted out.   This film revolves around a basketball player and Nakul underwent special training to "ensure that he lived the role rather than act".  

Telugu actress Bindu Madhavi was initially signed to portray the lead female character.  However, reports in November 2011 confirmed that she was dropped from the project since the director and the crew were not impressed with her performance.  She was subsequently replaced film by a newcomer Mrudhula,    while other reports during production claimed that Nakul had been replaced by Jai (actor)|Jai, although this proved untrue.   Latest addition marathi actor Atul Kulkarni is playing a crucial role in this film.   Siddharth Jonnalagadda who acted in the Telugu film Love Before Wedding was selected to play another villain. 

During production, the makers of the film ran into crowd trouble with students attempting to get autographs from Nakul when he was preparing for the shoot, they were subsequently prevented in doing so from the crew of the film prompting a student protest.  The team worked for 72 hours without break for a particular scene which was shot near the Chengalpet railway station. 
film 3.3 million , box office 200 crores

==Soundtrack==
The soundtrack was composed by S. Thaman collaborating with Arivazhagan for second time. Kamal Hassan was said to be approached to sing a song in the film which proved false. 

{{Infobox album|  
| Name        = Vallinam
| Longtype    =
| Type        = Soundtrack
| Artist      = S. Thaman
| Released    = March 11, 2013
| Cover       = Vallinam audio poster.jpg
| Caption     =
| Genre       = 
| Length      = 19:10 Tamil
| Label       = Sony Music
| Producer    = T. Abdullah
| Last album  = Jabardasth (film)|Jabardasth (2013)
| This album  = Vallinam (2013)
| Next album  = Balupu (2012)
}}

{{Track listing extra_column = Singer(s) music_credits = no lyrics_credits = no all_lyrics = Viveka, except where noted  

|title1= Maaman Machaan
|extra1=  Silambarasan, S. Thaman & Mukesh
|length1= :32
|title2= Uyiril Uyiril
|extra2= Haricharan
|length2= 4:48
|title3= Nakula
|extra3= Nakul & Andrea Jeremiah
|length3= 3:14
|note3= Parvathi
|title4= Uyiril Uyiril (Reprise)
|extra4= S. Thaman
|length4= 2:33
|title5= Vallinam KG Ranjith
|length5=3:43 Vaali
}}

==Release==
The satellite rights of the film were secured by Jaya TV. The film was given a "U" certificate by the Indian Censor Board. Although completed way before in early 2013, the films theatrical release was delayed due to unknown reasons. It finally released on February 28, 2014.

===Critical reception===
The film opened to Positive reviews.   called it "a decent sport based film" and "a timepass entertainer" but added that, "The trouble with Vallinam is that it goes all over trying to fit in mass commercial elements like fight, songs and unnecessary sentiments. Arivazhagan loses the plot half way through trying to make his hero a super action hero. The songs and the love angle sticks out like a sore thumb. The director should have concentrated only on the sports element it would have been a far better film".  The New Indian Express wrote, "Vallinam...follows the predictable track...it depends too much on cliches and fails to add any freshness or novelty to the familiar scenario". 

==Awards== National Award for Best Editor

== References ==
 

== External links ==
*  

 
 
 
 
 