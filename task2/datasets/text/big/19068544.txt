Love and Rage (film)
Love British Cinema German drama James Carney.

A drama of romantic obsession turned violent, Love and Rage was inspired by a true story and partially filmed in the home where the actual events occurred. James Lynchehaun (Daniel Craig) works at the estate of Agnes MacDonnell (Greta Scacchi), a wealthy Englishwoman in late 1800s Ireland. Agnes considers her privacy important, but shows flashes of a high-spirited nature among those she trusts, and enjoys scandalizing the locals by being a divorced woman who smokes, drinks, and rides horses astride on her vast property. When James discovers that a local land agent has been cheating Agnes, he shares the information with her. Shes grateful to him and they get to know each other a bit better, leading in time to a romantic relationship. James is younger than her, and beneath her social status, but Agnes is a woman who thrives on being seen as scandalous, so she enters into the affair with James with relish and delight. Hes a wild man, as well as a bit of a con artist, appearing at the estate in disguise and meeting Agnes dressed as a priest. Agnes doesnt appear to mind this, and at times she actively welcomes it. The fact that they both seem to delight in taunting "proper" society seems to please her that much more. James soon begins displaying a rather unusual bent. As time passes his wild behavior becomes more and more erratic, and eventually it becomes downright scary. What starts as erotic play-acting grows into something more sinister, and in time Jamess actions become less amusing and more threatening. Hes unstable, and shes realizing it much too late, after hes insinuated himself pretty deeply into her isolated life.

==External links==
* 

 
 
 
 
 
 
 