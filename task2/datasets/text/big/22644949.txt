Little Sister (1995 film)
 
{{Infobox film
| name           = Zusje
| image          = Little Sister (1995 film) poster.jpg
| image_size     = 
| caption        = 
| director       = Robert Jan Westdijk
| producer       = 
| writer         = Robert Jan Westdijk, Jos Driessen
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1995
| runtime        = 
| country        = Netherlands Dutch
| budget         = 
}} 1995 Netherlands|Dutch award-winning drama film directed by Robert Jan Westdijk. Little Sister earned the Special Jury Prize at the 8th Yubari International Fantastic Film Festival in February 1997. 

==Cast==
*Kim van Kooten	... 	Daantje
*Romijn Conen... 	Martijn (as Martijn Zuidewind)
*Hugo Metsers	... 	Martijn (voice)
*Bert Pot	... 	Martijn, subjective camera
*Roeland Fernhout	... 	Ramon
*Ganna Veenhuysen	... 	Ingeborg
*Hannah Risselada	... 	Little Daantje
*Michael Münninghoff	... 	Little Martijn
*Alenka Dorrele	... 	Mother Zuidewind
*Peter Idenburg	... 	Father Zuidewind
*Taco Keers	... 	Bas
*Marianne Jeuken	... 	Neighbour
*Carl Wünderlich	... 	Marketender
*Herman Brood	... 	Ramons upper neighbour
*Eulalia Montseré	... 	Spanish friend

==Notes==
 

== External links ==
*  

 
 
 
 
 
 


 
 