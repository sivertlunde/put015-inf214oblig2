Ekta Jeev Sadashiv
 

{{Infobox film
| name     = Ekta Jeev  Sadashiv
| director = Govind Kulkarni
| producer = Dada Kondke
| starring = Dada Kondke Usha Chavan
| music    = Ram Kadam
| released =  
| country  = India Marathi
}}

Ekta Jeev  Sadashiv is 1972 Marathi film directed by Govind Kulkarni and starring Dada Kondke and Usha Chavan as leads. It was  remade in Hindi as Jis Desh Mein Ganga Rehta Hain which was released in 2000.

==Plot==
Dada Kondke lives a simple, straightforward life in a small village with his mom and dad, and his sweetheart, Usha Chavan. When the time comes for Dada to marry, his parents inform him that his biological parents live in the city and want him to settle there. Dada bids tearful farewell to his village and its inhabitants and travels to the city of his birth parents and then starts his funny adventures to adjust to new city life.

==Cast==
*Dada Kondke
*Usha Chavan
*Ratnamala

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Kal Ratrila sapan pada
|-
| 2
| Labad Langda Dhong
|- 
| 3
| Mansa Paras Mendra 
|- 
| 4
| Nako Chalus Dudkya Chali
|- 
| 5
| Matari Kohtari Tumbo

|}

==External links==

 
 
 
 
 


 