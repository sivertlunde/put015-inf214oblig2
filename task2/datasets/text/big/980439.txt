It Happened Here
 
{{Infobox film
| name           = It Happened Here
| image          = It Happened Here.png
| image_size     =
| caption        = 1966 UK cinema poster
| director       = Kevin Brownlow Andrew Mollo
| producer       = Kevin Brownlow Andrew Mollo
| writer         = Kevin Brownlow Andrew Mollo Pauline Murray Sebastian Shaw Reginald Marsh
| music          = Anton Bruckner
| cinematography = Kevin Brownlow Peter Suschitzky
| editing        = Kevin Brownlow
| distributor    = United Artists Cork and International Filmfestival Mannheim-Heidelberg|Mannheim: 1964 United Kingdom: May 1966 United States: 8 August 1966 Australia: 25 May 1966
| runtime        = 97 min. UK
| German
| budget         = $20,000 (estimated)
}}
It Happened Here (also known as It Happened Here: The Story of Hitlers England) is a black-and white 1964 British World War II film written, produced and directed by Kevin Brownlow and Andrew Mollo, who began work on the film as teenagers. The films largely amateur production took some eight years, using volunteer actors with some support from professional filmmakers. 
 alternate history Britain which has been invaded and occupied by Nazi Germany. The plot follows the experiences of an Irish nurse working in England, who encounters people who believe collaboration with the invaders is for the best whilst others are involved in the resistance movement against the occupiers and their local collaborators.

==Plot==
===Setting=== German invasion retreat from Ural Mountains front, most German troops are eventually removed from Western Europe, and the garrisoning of Britain is largely carried out by local volunteers to the German army and the Schutzstaffel|SS. 
 partisan movement.

===Story=== their collaborators and witnesses an attack on German forces by a group of British partisans, during which a number of her friends from the village are killed in the crossfire. The attack (and more particularly the deaths) influences her subsequent views and decisions.
 antifascist doctor and his wife) that gives Pauline pause and when she subsequently discovers they are harbouring an injured partisan she reluctantly agrees to help.
 forced euthanasia programme and killed a group of foreign forced labourers who had contracted tuberculosis.

The film ends with Pauline being arrested after protesting and refusing to continue but before she can be put on trial, she is captured by the resurgent British Resistance and agrees to work for them as they fight to liberate the country with the help of arriving American troops. In the finale, Pauline tends a group of wounded partisans while, out of her view, a large group of soldiers from the Black Prince Regiment of the British Legion of the Waffen-SS who had surrendered are summarily shot, a scene reminiscent of an SS massacre of civilians earlier in the film.

==Production==
The film was directed by Kevin Brownlow, who later became a prominent film historian, and Andrew Mollo, who later become a leading military historian. Brownlow developed the concept of the film when he was 18 years old, in 1956. He turned to Mollo, a 16-year-old history buff, to help him with the design of costumes and sets. Mollo was intrigued by the project, and became his collaborator. The film was in the making for the next eight years, which  the Guinness Book of World Records (as of 2003) calls the longest ever production schedule. 

It Happened Here was shot in black and white on 16&nbsp;mm film, giving it a grainy, newsreel feel (no actual stock footage was used). Most of the equipment used in the production was borrowed. The audio quality (and lighting) on the opening reel is rather poor, which makes the dialog difficult to follow for the first few minutes.   to Brownlow to help him finish the film. Veteran wartime BBC radio announcers Alvar Lidell and John Snagge gave their services free to voice reconstructed newsreels and radio broadcasts.  Support was also given by director Tony Richardson, who helped to pay for the final production. Kevin Brownlow: How It Happened Here. UKA Press, London/Amsterdam/Shizuoka 2007, ISBN 978-1-905796-10-6. 
 Pauline Murray. The film was partially shot in Grims Dyke#Film location|Grims Dyke. Though the cast was almost entirely amateur, It Happened Here helped to launch the career of its cinematographer, Peter Suschitzky, who went on to work on such films as The Rocky Horror Picture Show and The Empire Strikes Back.

==Release== Mannheim International Film Week.  Some Jewish groups protested against the inclusion of seven minutes of footage of a British fascist speaking against the Jews and for euthanasia, claiming the inclusion of this material gives a platform to unapologetic neo-Nazism|neo-Nazis despite the films strongly anti-Nazi theme. In response, this was cut from the original release, though it was restored thirty years later, after Brownlow regained the rights to the film.

==Reception== Channel Islands) suggested that this was indeed the case.

In a contemporary review published in The New York Times in 1966, Bosley Crowther wrote: "The acting by unfamiliar people is beautifully natural and restrained, particularly that of Pauline Murray in the principal role. Through her human and subtle generation of an ungrudging sympathy, one becomes involved in her dilemma and is caught up all the way in the despair, uncertainty and terror of her experiences." 

===Awards===
* 1964: International Filmfestival Mannheim-Heidelberg, Preis der Volkshochschul-Jury (Prize of the Adult Education Centers Jury)
* 1966: National Board of Review of Motion Pictures, Top Ten Films

==How It Happened Here== David Robinson.

==See also==
* Axis victory in World War II – list of Nazi Germany/Axis/World War II alternate history articles.  
* Auxiliary Units
* Operation Sea Lion in fiction
* It Cant Happen Here
*Resistance (2011 film)|Resistance
* List of films shot over several years

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 