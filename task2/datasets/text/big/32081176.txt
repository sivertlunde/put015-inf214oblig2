Akai Ito (film)
{{Infobox film
| name           = Akai Ito
| image          = Akai ito poster.jpg
| image size     = 
| alt            = 
| caption        = Poster advertising Akai Ito in Japan
| writer         = Chiho Watanabe, Ritsuko Hanzawa
| screenplay     = 
| story          = Mei (Akai Ito) 
| narrator       = 
| starring       = Nao Minamisawa, Junpei Mizobata
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| director       = Shosuke Murakami
| producer       = Yoshihiko Takeda
| distributor    = Shochiku
| released       =  
| runtime        = 106 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = $12,348,550 
| preceded by    = 
| followed by    = 
}}
 the same name.   

==Plot==
Mei has been in love with Yuya all her life. Theyve grown up together. But when they reach their second year of middle school, Yuya suddenly professes his love for her older sister, Haruna.

Struggling to rebound from her decade-long crush, Mei finds comfort in the close camaraderie of her school friends, Taka, Mia, Yuri, Natsu, Mitsu and Sara and then meets a reticent and gentle-natured boy named Atsushi. The two begin to realize that they share many things in common that seem to transcend mere coincidence: the same birthday (February 29, 1992) and a previous encounter when they were both 8. Feeling a bond of fate, they grow closer.

But destiny can have a dark side to it, too, and a shocking revelation from both of their pasts compels Atsushi to pull away from Mei. The devastating separation then unleashes a wave of misfortune upon Mei: the death of a loved one, drug addiction and domestic violence. As circumstances seek to take control of her life, Mei struggles to maintain her faith in the tenuous thread of destiny that will reunite her with her true love.

==Cast==
* Nao Minamisawa as Mei Takemiya 
* Junpei Mizobata as Atsushi Nishino
* Ryo Tajima as Kamiya Mitsuru 
* Sayuri Iwata as Haruna Takemiya 
* Ryo Kimura as Riku Takahashi 
* Rei Okamoto as Mia Amagishi
* Anna Ishibashi as Asami Tadokoro  
* Nanami Sakuraba as Sara Nakagawa 
* Tomo Yanagishita as Natsuki Fujiwara  
* Hiroshi Yazaki as Yuya Shinozaki 
* Kasumi Suzuki as Yuri Nakanishi 
* Kaoru Hirata as Miyabi Kawaguchi 
* Ryuya Wakaba as Shu Yasuda 
* Mirai Yamamoto as Natsumi Nishino 
* Shigemitsu Ogi as Takamichi Morisaki 
* Noriko Watanabe as Sachiko Takemiya
* Kenji Matsuda as Koichi Murakoshi Kosuke Suzuki
* Michiko Iwahashi
* Mio Miyatake as the young version of Mei Takemiya

==References==
 

==External links==
*    
*  

 
 
 