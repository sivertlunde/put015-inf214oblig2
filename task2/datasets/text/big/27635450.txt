Will You Marry Me? (film)
{{Infobox film
| name= Will You Marry Me?
| image=
| caption=
| director= Aditya Datt
| producer= Vipin Jain Krishan Chaudhary
| starring= Shreyas Talpade Rajeev Khandelwal Tripta Parashar Mugdha Godse Celina Jaitley  Muzamil Ibrahim Sachin Gupta Sharib Gaurav Dagaonkar
| released=  
| country= India
| language= Hindi
}}
Will You Marry Me? is a Hindi romantic comedy starring Shreyas Talpade, Rajeev Khandelwal, Mugdha Godse and Muzamil Ibrahim in lead roles. The film is about a beach wedding. It was shot in several locations including Fujairah and Dubai, in the United Arab Emirates, Mumbai, India and Bangkok, Thailand. 

==Plot==
The film is a copy of the Hollywood movie Tomcats (2001 film)|Tomcats –
The film is about three bachelors Aarav (Shreyas Talpade), Nikhil (Muzamil Ibrahim) and Rajveer (Rajeev Khandelwal) who along with eleven other college mates sign a contract which has two clauses. The first clause says that all of them have to purchase certain number of Reliance shares, and the second rule explains all the shares will go to the man who will remain unmarried till the end. Slowly and steadily people start to break away from the rule and only Nikhil, Rajveer and Aarav remain single. Nikhil is in love with his childhood friend Anjali (Tripta Prashar) and wants to settle down with her but the other two have no such plans, but they find the proceedings interesting after discovering Anjalis best friend Sneha (Mugdha Godse) at the wedding venue. Suddenly all their plans to remain unmarried till the end vanish and the two start trying o win Snehas heart. Meanwhile one of Rajveers friends gives him Rs 5 crore to keep but Rajveer being smart enough invests the money in the stock market, only to find that the company he invested in has crashed and a powerful business magnate (Paresh Rawal) is after his life. Rajveer plans to convince Aarav to get married to Sneha so that he can get all the Reliance shares and pay his due. His plans fail when Aarav manages to listen to a secret conversation.   Emirates Business, Retrieved on 2010-6-13 

==Cast==
*Shreyas Talpade as Aarav
*Rajeev Khandelwal as Rajveer
*Mugdha Godse as Sneha
*Muzamil Ibrahim as Nikhil Manoj Joshi as Anjalis father
*Tripta Parashar as Anjali
*Celina Jaitley as Vaishali (Special Appearance)
*Paresh Rawal as Gutka King (Snehas father)

==Production==
The crew shot in the United Arab Emirates from May 26 - June 14, 2010, according to Mugdha Godses Twitter. The crew then shot in Mumbai.  Filming was completed in Bangkok, Thailand from July 24 - August 2, 2010. "Sixty per cent of the films story is set in the UAE", says director Aditya Datt. 

==Soundtrack== Sachin Gupta.
===Track Listings===
{| class="wikitable"
|-
! # !! Song !! Singer(s) !! Music Director !! Length
|- Sharib || 5:53
|-
| 2 || "Soniye" || Rahat Fateh Ali Khan || Gaurav Dagaonkar || 4:44
|- Sachin Gupta || 4:00
|- Sharib || 3:53
|- Sachin Gupta, Sachin Gupta || 3:52
|- Sharib || 4:50
|}

==Reception==
The movie released to mostly negative reviews. RediffMovies gave 2 out of 5 star rating with the comment "Will You Marry Me? fails to impress".  Times Of India gave 1.5 out of 5.  The movie was declared disaster in first week of its release with total net gross of first week only Rs. 41 lac. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 