Fools (film)
{{Infobox film
| name           = Fools
| image          = Fools1970film.jpg
| image_size     = 
| caption        = 
| producer       = Henri Bollinger Pat Rooney Robert Yamin
| director       = Tom Gries
| writer         = Robert Rudelson
| narrator       = 
| starring       = Jason Robards
| music          = Shorty Rogers
| cinematography = Michel Hugo
| editing        = Byron Brandt
| studio         = Translor Films
| distributor    = Cinerama Releasing Corporation
| released       = April 14, 1972
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         = $900,000 Fools and the New Film-making: Low Budget, High Zeal: The New Film-making The New Film-making
Champlin, Charles. Los Angeles Times (1923-Current File)   17 May 1970: s1. 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Fools is a 1970 drama film directed by Tom Gries. It stars Jason Robards and Katharine Ross. 

==Plot==
Aging actor Matthew South falls in love with a much younger married woman, the wife of his attorney.

==Cast==
*Jason Robards as Matthew South
*Katharine Ross as Anais Appleton
*Scott Hylands as David Appleton
*Roy Jenson as Man in park
*Mark Bramhall as Man in park

==References==
 

==External links==
* 

 

 
 