Revenge of the Musketeers
{{Infobox film
| name           = Revenge of the Musketeers
| image          = Revenge_of_the_Musketeers.jpg
| border         = yes
| alt            = 
| caption        = French theatrical release poster
| director       = Bertrand Tavernier
| producer       = Frederic Bourboulon
| screenplay     = {{Plainlist|
* Michel Léviant
* Bertrand Tavernier
* Jean Cosmos
}}
| story          = {{Plainlist|
* Riccardo Freda
* Eric Poindron
}}
| narrator       = 
| starring       = {{Plainlist|
* Sophie Marceau
* Philippe Noiret
* Claude Rich
* Sami Frey
}}
| music          = Philippe Sarde
| cinematography = Patrick Blossier
| editing        = Ariane Boeglin
| studio         = {{Plainlist| CiBy 2000
* Little Bear TF1 Films Production
* Canal+
}}
| distributor    = Bac Films
| released       =  
| runtime        = 125 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $311,922  (USA) 
}}
Revenge of the Musketeers ( ) is a 1994 French adventure film directed by Bertrand Tavernier and starring Sophie Marceau, Philippe Noiret, Claude Rich, and Sami Frey. Set in the seventeenth century, the film is about the daughter of the renowned swordsman DArtagnan who keeps the spirit of the Musketeers alive by bringing together the aging members of the legendary band to oppose a plot to overthrow the King and seize power. Sophie Marceau did her own fencing on screen.    In 1995, the film received César Award nominations for Best Music Written for a Film (Philippe Sarde) and Best Supporting Actor (Claude Rich). 

==Plot==
In the autumn of 1654, an escaped slave seeks refuge at a convent in southern France. After the nuns take him in, his pursuers break into the convent and assault the nuns, searching for the slave. After wounding the mother superior (Pascale Roberts), they close in on their prey. One feisty novitiate, Eloise (Sophie Marceau)—the daughter of the renowned swordsman DArtagnan (Philippe Noiret) of the famed Three Musketeers—stands up to the intruders, but is shoved aside as they ride off after the frightened slave, who escaped from the evil Duke Crassac de Merindol. Eloise finds a blood-stained piece of paper (a simple laundry list) that the slave used to stop a bleeding wound, and believes it holds some secret code. Later, Eloise is with the mother superior when she dies from her wound. Swearing a sacred oath to avenge her and dressed in mens clothes, Eloise sets out for Paris to elicit the help of her father in tracking down the murderers.

Along the way at a roadside tavern, Eloise gets into a sword fight and is helped by a young poet, Quentin la Misère (Nils Tavernier), who writes her a brief love poem, "Dance, butterfly, dance..." Attracted to Eloise, he offers to accompany her to Paris. There, she is reunited with her famous father, whom she hasnt seen since childhood. She shows her father the blood-stained paper she believes holds the key to a secret plot, but he tells her it looks like a laundry list with blood on it. Later, DArtagnan visits the grave of his former Musketeer, Athos, and longs for the days when the Musketeers were together, defending their King.

Meanwhile, Eloise leaves her father and heads to the royal court, where she tricks her way inside. There, Cardinal Mazarin (Gigi Proietti) is teaching the youthful King Louis XIV (Stéphane Legros) the subtleties of deceitful diplomacy. After meeting with Mazarin—who is indeed plotting against the King—Eloise learns that he is after Quentin and rushes off to save the young poet. Mazarin believes that she has evidence—the bloody laundry list—of his conspiracies and orders the one-eyed man to follow her and find out what she knows. The Kings men track her down and chase her through the narrow streets of Paris. When they capture her, her father comes to the rescue, and the two defeat several men in a daring sword fight. One of the men, however, escapes with Quentins love poem and delivers it to Mazarin, who concludes that it is too poorly written to be a poem and must be some coded message.

With Eloise and Quentin in tow, DArtagnan reunites with two of his former musketeers—Porthos and Aramis—whom he enlists to help thwart an as yet unidentified conspiracy against the King. Convinced the list contains a secret code, the pedantic Aramis helps decipher the list, using advanced linguistic and biblical knowledge. Meanwhile, the Duke of Crassac (Claude Rich)—a slave trader and smuggler—is indeed plotting against the King, planning to poison him at the upcoming coronation, blame Mazarin, and when the Kings younger brother is installed, step in as the powerful Kings protector. Crassacs mistress, Eglantine de Rochefort—the "woman in red" who witnessed the convent murder—tasks him to kill all involved in the convent incident and leave no witnesses.

DArtagnan, Porthos, Aramis, Eloise, and Quentin make the long ride to the convent, where they hope to learn more about the conspiracy. Eloise arrives before the others and finds the body of a dead nun before encountering Eglantine in the process of destroying all evidence of her existence from the convents records. After they struggle, Eloise is captured by Eglantines accomplice and taken to Crassacs castle dock where she joins the nuns who are being sold and shipped to the Americas as slaves. When Crassac arrives and spots Eloise, he decides to take her for himself back to his castle.

When DArtagnan and his companions arrive, the aging musketeers slowly scale the castle walls, only to be helped by the one-eyed man, who turns out to be the fourth Musketeer, Athos, thought to have died years before. Seeing the nuns being loaded onto a ship in the distance, the Musketeers ride to their rescue, killing scores of smugglers, and freeing the sisters. After the fight, the Musketeers ride off to Paris, where the King is preparing for his coronation, and where they suspect Eloise is being held.

In Paris, DArtagnan enters the palace and warns the young King about the plot against his life. Meanwhile, Crassac and his followers gather and plan the next days coronation murder. While Crassac shares his delusional plans to marry Eloise, she suddenly appears—freed by the jealous Eglantine—and in the ensuing sword fight, just as Eloise is about to be killed by three of Crassacs men, DArtagnan and his men come to her rescue. While the musketeers fight Crassacs men, Eloise chases after the fleeing Crassac and engages him in an extended sword fight that leads them to the roof of the palace. Just as Crassac is about to kill Eloise, DArtagnan arrives and runs the evil Duke through with a sword, ending the threat to his daughter and to his King. Afterwards, DArtagnan tells his daughter how proud he is of her and the two embrace.

==Cast==
 
 
* Sophie Marceau as Eloïse dArtagnan
* Philippe Noiret as DArtagnan
* Claude Rich as Duke of Crassac
* Sami Frey as Aramis
* Jean-Luc Bideau as Athos
* Raoul Billerey as Porthos
* Charlotte Kady as Eglantine de Rochefort
* Nils Tavernier as Quentin la Misère
* Gigi Proietti as Mazarin 
* Jean-Paul Roussillon as Planchet
* Pascale Roberts as Mother Superior
* Emmanuelle Bataille as Sister Félicité
* Christine Pignet as Sister Céline
* Fabienne Chaudat as Sister Frédégonde
* Josselin Siassia as Slave
 
* Jean-Claude Calon as Slave Trader
* Stéphane Legros as Louis XIV
* Maria Pitarresi as Olympe
* Jean Martinez as Duke of Longueville
* Patrick Rocca as Bargas
* Michel Alexandre as The Hallebardier
* Fanny Aubert as Sister Huguette
* Grégoire Barachin as Fencing Pupil #1
* Jean-Pierre Bouchard as Conspirator
* Daniel Breton as Recruiting Sergeant
* Canto e Castro as Principal Singing Monk
* Carlos César as Innkeeper
* Vanina Delannoy as Courtesan #1
* Raymond Faucher as Painter
* Filipe Ferrer as Conti 
 

==Production==
Production on Revenge of the Musketeers started in October 1993.    The film originally started under the direction of Riccardo Freda, but was taken over by Bertrand Tavernier a few days after shooting had started. 

==Reception==

===Critical response===
Revenge of the Musketeers received mixed reviews. In his review on AV Club, Keith Phipps wrote, "Taverniers assured direction and a game performance from Marceau make it worth a look."   

The web site Flickering Myth praised the films solid entertaining qualities:
  }}
 Le Bossu that "a more confident affair that bolsters playfulness with purpose".  The web site Stumped focused on Marceaus performance, writing, "Marceau is delightful in the lead, demonstrating a playful coquettishness and a mastery of the blade."   

===Awards and nominations===
* 1995 César Award Nomination for Best Music Written for a Film (Philippe Sarde)
* 1995 César Award Nomination for Best Supporting Actor (Claude Rich)   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 