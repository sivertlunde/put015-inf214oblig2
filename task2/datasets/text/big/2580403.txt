American Pie (film)
 
{{Infobox film
| name           = American Pie
| image          = American Pie1.jpg
| alt            = Group picture of the cast. Alyson Hannigan has a flute in hand.
| caption        = Theatrical release poster Paul Weitz Chris Weitz Paul Weitz Chris Moore Warren Zide Craig Perry
| writer         = Adam Herz Chris Klein Thomas Ian Nicholas Eddie Kaye Thomas Seann William Scott Alyson Hannigan Tara Reid Natasha Lyonne Mena Suvari Shannon Elizabeth David Lawrence
| cinematography = Richard Crudo
| editing        = Priscilla Nedd-Friendly
| studio         = Zide/Perry Productions Universal Pictures
| released       =  
| runtime        = 95 minutes 
| country        = United States
| language       = English
| budget         = $11 million
| gross          = $235,483,004 
}} pop song masturbating with third base feels like "warm apple pie". Its also been stated by writer Adam Herz that the title also refers to the quest of losing your virginity in high school, which is as "American as apple pie."
 Laid by James, which is also the theme for the entire franchise.

In addition to the primary American Pie saga, there are currently four   (2005),   (2006),   (2007), and   (2009). A fifth sequel simply entited East Great Falls by AP2 and The Book of Love writer David H. Steinberg, has yet to be produced. 

In response to the success of American Reunion, a fifth theatrical film, under the working title American Pie 5 was announced on August 4, 2012.   

==Plot== Chris Klein), Chris Owen), claims to have done so at a party hosted by Steve Stifler (Seann William Scott), an ignorant and obnoxious lacrosse player who happens to be a close friend of the group (minus Finch).

Vicky later accuses Kevin of being with her only for sex, so he must try to repair his relationship with her before the upcoming prom night, when the four plan to lose their virginity. He eventually succeeds. Oz, meanwhile, joins the jazz choir in an effort to lose his reputation as an insensitive jock and find a girlfriend there. He soon wins the attention of Heather (Mena Suvari), a girl in the choir. However, he runs into problems when Heather learns about Ozs reputation and breaks up with him, although he manages to regain most of her trust when he leaves the lacrosse championship to perform a duet with her in a choir competition.

Jim, meanwhile, attempts to pursue Nadia (Shannon Elizabeth), an exchange student from Slovakia who asks Jim for help to study for an upcoming History test. After being told by Oz that third base feels like "warm apple pie", he practices having sex with a pie, only to be caught by Noah (who lets him keep it a secret from his mother). Stifler persuades him to set up a webcam in his room so that they can all watch his encounter with Nadia. The plan suffers a hiccup, though, when Nadia discovers Jims pornography collection and sits half-naked on his bed to read and masturbates to it. Jim is persuaded to return to his room, where he joins Nadia, unaware that he accidentally sent the webcam link to everyone on the school list. Nadias exchange family sees the video and sends her back home, now leaving Jim dateless for the upcoming prom and without hope of losing his virginity before high school is over.

In sheer desperation, Jim asks band camp geek Michelle Flaherty (Alyson Hannigan) to the senior prom as she is apparently the only girl at his school who did not see what happened. Finch, meanwhile, pays Vickys friend, Jessica (Natasha Lyonne), $200 to spread rumors around the school of his sexual prowess, hoping that it will increase his chances of success. Unfortunately, he runs into trouble when Stifler, angry that a girl turned him down for the prom because she was waiting for Finch to ask her, puts a laxative into Finchs mochaccino. Finch, being paranoid about the lack of cleanliness in the school restrooms, and unable to go home to use the toilet as he usually does, is tricked by Stifler into using the girls restroom. Afterward, he emerges humiliated before a crowd of fellow students and is also left dateless.

At the prom, things seem bleak for the four boys until Vicky asks the girl that Chuck Sherman claimed to have bedded about her "first time." She proclaims to everyone at the prom that she and Sherman did not have sex at Stiflers party, leaving Sherman embarrassed and making him wet himself. The revelation takes the pressure off of Jim, Kevin, Oz and Finch, and they head to the post-prom party at Stiflers house with new hope.

At the after-party, all four boys fulfill their pledges. Kevin and Vicky have sex in an upstairs bedroom. Vicky breaks up with Kevin afterwards on the grounds that they will drift apart when they go to college, with him attending the University of Michigan and her at Cornell University in New York state; he tries to persuade her otherwise but quickly realizes that the distance between the two schools will be too much of a burden. Oz confesses the pact to Heather, and renounces it, saying that just by them being together makes him a winner. They reconcile and wind up having sex. Oz, honoring his newfound sensitivity, never confesses to what they did.

Jim and Michelle have sex after he finds out that she is actually not as naïve as she let on and that she saw the "Nadia Incident" after all. She accepted his offer to be his date because of it, knowing he was a "sure thing," but she makes him wear two condoms to combat his earlier "problem" with Nadia. Jim is surprised at Michelles aggressiveness in bed. In the morning he wakes up to find her gone and realizes that she had used him for a one-night stand, which Jim thinks is "cool".

Dateless, Finch strays downstairs to the basement recreation room where he meets Stiflers Mom (Jennifer Coolidge). She is aroused by his precociousness, and they have sex on the pool table, and Finch has his revenge on Stifler with his mom. The next morning, while Stifler searches for his mom, he finds her on the pool table with Finch, and is so shocked that he faints. The morning after the prom, Jim, Kevin, Oz, and Finch eat breakfast at their favorite restaurant – with the fittingly nostalgic name "Dog Years" – where they toast to "the next step."

The film ends with Nadia watching Jim stripping on his webcam. Noah walks in (which Jim is completely oblivious to) but happily walks out and also starts dancing.

==Cast==
 
{{columns-list|3|
* Jason Biggs as James "Jim" Levenstein Chris Klein as Chris "Oz" Ostreicher
* Thomas Ian Nicholas as Kevin Myers
* Eddie Kaye Thomas as Paul Finch
* Seann William Scott as Steven "Steve" Stifler
* Alyson Hannigan as Michelle Flaherty
* Tara Reid as Victoria "Vicky" Lathum
* Natasha Lyonne as Jessica
* Mena Suvari as Heather Gardner
* Shannon Elizabeth as Nadia
* Eugene Levy as Noah Levenstein
* Jennifer Coolidge as Stiflers Mom Chris Owen as Chuck "Sherminator" Sherman
* Eric Lively as Albert
* Molly Cheek as Mrs. Levenstein
* Lawrence Pressman as Coach Marshall
* Clyde Kusatsu as English teacher
* Justin Isfeld as Justin
* John Cho as John
* Sasha Barrese as Courtney
* Eli Marienthal as Matt Stifler
* James DeBello as Enthusiastic guy
* Tara Subkoff (uncredited) as College girl
* Chris Weitz (uncredited) as Male voice in porn film
* Tyler Love as the milky bar kid
}}
;Cameos Travis is incorrectly credited as former Blink-182 drummer "Scott Raynor". Also, when their song "Enema of the State|Mutt" is credited, Barkers name is misspelled as "Travis Barkor". The parts were given when Tom DeLonges acting agent reported the film needed a band.
* Christina Milian appears as one of the band geeks.
* Casey Affleck as Tom Myers, Kevins older brother.
* Stacy Fuson, Playmate of the Month for February 1999, appears in the crowd laughing at Finch when he exits the girls restroom.

==Location==
  house.]] Eastown neighborhood of Grand Rapids.  The "Central Chicks" and "Central" Lacrosse team that East Great Falls plays against is an amalgam of nearby Forest Hills Central High School and Grand Rapids Catholic Central High School. 
 Long Beach using Long Beach Unified School District area high schools. Millikan High School, whose school colors are blue and gold, was used for exterior shots, and Long Beach Polytechnic High School was used for interior shots. Located in Los Cerritos, Long Beach, California, both schools are within five miles of the Virginia Country Club and Los Cerritos Neighborhood (where Ferris Buellers Day Off and Donnie Darko were filmed).   

==Release==
===Reception===
Despite insiders claiming it to be a potential   thru Orlando Sentinel (June 13, 1999).  The film took in a gross worldwide revenue of $235,483,004,   $132,922,000 of which was from international tickets.

In home video rentals, the film has grossed $109,577,352 worldwide, with $56,408,552 of that coming from sales in the US. 

====Critical reception====
The film received mixed to positive reviews from critics. Based on 123 reviews    Jim Sullivan of The Boston Globe wrote that American Pie is a "gross and tasteless high school romp with sentimental mush." 
Roger Ebert was more supportive, awarding it three out of four stars. He noted that " t is not inspired, but its cheerful and hard-working and sometimes funny, and—heres the important thing—its not mean. Its characters are sort of sweet and lovable." 

====Awards and nominations====

 
{| class="wikitable"
|-
! Year 
! Award 
! Category 
! Recipient(s) 
! Result
! Ref(s)
|-
| rowspan="22"| 2000
| American Comedy Award
| Funniest Supporting Actor in a Motion Picture
| Eugene Levy
|  
|
|- Blockbuster Entertainment Award
| Favorite Supporting Comedy Actor
| Eugene Levy
|  
|    
|-
| Favorite Actress
| Mena Suvari
|  
|  
|-
| Favorite Actor
| Alyson Hannigan
|  
|  
|-
| Bogey Awards
| Bogey Awards in Platinum Universal Pictures
|  
| 
|-
| Casting Society of America
| Artios Award for Best Casting for Feature Film
| Universal Pictures
|  
| 
|- CFCA Award 
| Best Promising Actor Chris Klein
|  
| 
|-
| Csapnivalo Award
| Golden Slate Award for Best Teen Movie
| Universal Pictures
|  
| 
|- Golden Screen
|
| Universal Pictures
|  
| 
|-
| Golden Screen with 1 Star
| 
| Universal Pictures
|  
| 
|-  MTV Movie Awards
| Best Comedic Performance
| Jason Biggs
|  
|    
|- 
| Breakthrough Female Performance
| Shannon Elizabeth
|  
|  
|- 
| Breakthrough Male Performance
| Jason Biggs
|  
|  
|- 
| Best Movie
| Universal Pictures
|  
|  
|- 
| rowspan="5"| Teen Choice Awards
| Choice Actor
| Jason Biggs
|  
| 
|- 
| Choice Breakout Performance Chris Klein
|  
| 
|- 
| Choice Comedy
| Universal Pictures
|  
| 
|- 
| Choice Liar Chris Klein
|  
| 
|- 
| Choice Sleazebag
| Seann William Scott
|  
| 
|- 
| rowspan="3"| Young Hollywood Awards
| Best Ensemble Cast
| Jason Biggs
|  
| 
|- 
| Breakthrough Female Performance
| Mena Suvari
|  
| 
|- 
| Best Soundtrack
| Uptown Records & Universal Records
|  
| 
|}

===Box office=== twentieth highest-grossing film of 1999.

==Soundtrack==
The films soundtrack peaked at number 50 on the Billboard 200|Billboard 200 chart.   
{{Infobox album
| Name        = American Pie: Music from the Motion Picture
| Type        = Soundtrack
| Artist      = Various artists
| Cover       =
| Released    =  
| Recorded    = 
| Genre       = 
| Length      = 46:02 Universal
| Producer    = 
| Last album  = 
| This album  = American Pie: Music from the Motion Picture (1999)
| Next album  = American Pie 2: Music from the Motion Picture (2001)
}}

{{Album ratings
| rev1 = Allmusic
| rev1score =    
}}

{{tracklist
| extra_column    =  Performed by
| writing_credits = yes
| title1        = New Girl
| note1         = 
| writer1       = 
| extra1        = Third Eye Blind
| length1       = 2:16
| title2        = You Wanted More
| note2         = 
| writer2       =  Tonic
| length2       = 3:52 Mutt
| note3         = 
| writer3       = 
| extra3        = Blink-182
| length3       = 3:23
| title4        = Glory
| note4         = 
| writer4       = 
| extra4        = Sugar Ray
| length4       = 3:29
| title5        = Super Down
| note5         = 
| writer5       = 
| extra5        = Super TransAtlantic
| length5       = 4:07
| title6        = Find Your Way Back Home
| note6         = 
| writer6       = 
| extra6        = Dishwalla
| length6       = 4:04 Good Morning Baby
| note7         = 
| writer7       =  Dan Wilson of Semisonic & Bic Runga
| length7       = 3:34
| title8        = Stranger by the Day
| note8         = 
| writer8       = 
| extra8        = Shades Apart
| length8       = 4:02
| title9        = Summertime
| note9         = 
| writer9       =  Bachelor No. 1
| length9       = 3:46
| title10        = Vintage Queen
| note10         = 
| writer10       =  Goldfinger
| length10       = 3:04 Sway
| note11         = 
| writer11       = 
| extra11        = Bic Runga
| length11       = 4:23
| title12        = Wishen
| note12        = 
| writer12       = 
| extra12        = The Loose Nuts
| length12       = 3:04
| title13        = Man with the Hex
| note13         = 
| writer13       = 
| extra13        = The Atomic Fireballs
| length13       = 3:01
}}

The following songs were included in the film but were not featured on the soundtrack:
* Sex-o-rama Band – "Love Muscle" Walk Dont Run" One Week"
* The Brian Jonestown Massacre – "Going To Hell"
* Third Eye Blind – "Semi-Charmed Life" Oleander – "I Walk Alone" Hole – Celebrity Skin" Everclear – Everything to Everyone"
* Harvey Danger – "Flagpole Sitta"
* Duke Daniels – "Following a Star"
* The Lemonheads – "Mrs. Robinson"
* Fatboy Slim – "The Rockafeller Skank" Libra Presents Taylor – "Anomaly - Calling Your Name (Grannys Epicure Mix)"
* Etta James – "At Last" James – "Laid (song)|Laid"
* Loni Rose – "I Never thought you would come"
* Norah Jones – "The Long Day is Over"
* Marvin Gaye – "How Sweet It Is (To Be Loved by You)"
* Maria Muldaur – "Midnight at the Oasis"
* Simple Minds – "Dont You (Forget About Me)"

==See also==
* List of films featuring surveillance

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 