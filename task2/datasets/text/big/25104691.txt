Corsair (film)
 
{{Infobox film
| name           = Corsair
| image_size     =
| image	         = Corsair FilmPoster.jpeg
| caption        =
| director       = Roland West
| producer       = Roland West
| screenplay     = Josephine Lovett Roland West
| based on       =   Alison Loyd Alfred Newman
| cinematography = Ray June
| editing        = Hal C. Kern
| distributor    = United Artists
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
}}

Corsair is a 1931 American crime drama written, produced and directed by Roland West. The film is based on the 1931 novel Corsair, a Pirate in White Flannels   by Walton Green and takes place in and was shot during the era of Prohibition in the United States.  The film stars Chester Morris and Thelma Todd (credited as Alison Loyd). 

==Plot summary== junk bonds. Mr. Corning tells John he doesnt have it what it takes to succeed in the brutal world of share trading. John replies he will seek a new line of work where he will not go after elderly widows savings.
 bootleggers and then resells the cargo to their wealthy backers.

He only forgot two things: that in the cutthroat world of junk bonds and margin calls, they don’t use real knives, machine guns, and bombs, like the gangsters; and the girl hiding in the hold.

==Cast==
*Chester Morris as John Hawks Alison Loyd as Alison Corning
*Fred Kohler as Big John
*Ned Sparks as Slim
*Emmett Corrigan as Stephen Corning William Austin as Richard Bentinck
*Frank McHugh as Chub Hopping Frank Rice as Fish Face
*Mayo Methot as Sophie
*Gay Seabrook as Susie Grenoble
*Addie McPhail as Jean Phillips Al Hill
*Pat Hartigan
*Sidney DAlbrook

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 
 
 
 