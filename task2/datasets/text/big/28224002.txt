Il commissario Lo Gatto
 
{{Infobox film
| name           = Il commissario Lo Gatto
| image          = Ilcommissariologatto.jpg
| caption        = Film poster
| director       = Dino Risi
| producer       = Pio Angeletti Adriano De Micheli
| writer         = Dino Risi Enrico Vanzina
| starring       = Lino Banfi
| music          = Manuel De Sica
| cinematography = Alessandro DEva
| editing        = 
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Il commissario Lo Gatto is a 1987 Italian comedy film directed by Dino Risi. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Cast==
* Lino Banfi - Commissario Natale Lo Gatto
* Maurizio Ferrini - Agente Gino Gridelli
* Maurizio Micheli - Vito Ragusa
* Isabel Russinova - Wilma Cerulli / Maria Papetti
* Galeazzo Benti - Barone Fricò
* Renata Attivissimo - Addolorata Patanè
* Nicoletta Boris - Annunziata Patanè
* Albano Bufalini - The Pharmacist
* Alberto Capone
* Roberto Della Casa - Architetto Arcuni
* Gianni Franco - Pedretti - aka Bazooka
* Marcello Furgiele - Mario - the lifeguard
* Licinia Lentini - Mrs. Bellugi
* Armando Marra - The Barber
* Valeria Milillo - Manuela Bellugi
* Andrea Montuschi - Don Giacomo - the priest
* Gianluigi Pizzetti - Ingegner Bellugi
* Antonella Voce - Immacolata Patanè

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 