Lalach
{{Infobox film
| name           = Lalach 
| image          = 
| caption        =
| director       = Shankar Nag
| producer       = Prem Bedi
| writer         = 
| starring       = Vinod Mehra Bindiya Goswami Ranjeet
| music          = Bappi Lahiri
| cinematography = 
| editing        = 
| distributor    = 
| released       =  1983
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}}

Lalach is 1983 Indian Hindi film directed by Shankar Nag starring Vinod Mehra, Bindiya Goswami, Ranjeet, 
Pran (actor)|Pran, Kajal Kiran, Anant Nag. 

==Cast==
* Vinod Mehra
* Kajal Kiran
* Bindiya Goswami
* Anant Nag Pran
* Ranjeet

==Soundtrack==
The films soundtrack was composed by Bappi Lahiri and lyrics was penned by Anjaan (lyricist)|Anjaan.
{| class="wikitable"
|- style="background:#ff9; text-align:center;"
! Track# !! Song !! Singer(s) !! Duration
|-
| 1 || "Apni Gadi Chalti Hai" || Manna Dey || 
|-
| 2 || "Apni Gadi Chalti Hai" || Kishore Kumar, Suresh Wadkar, Manna Dey || 
|-
| 3 || "Gao Re Gao" || Kishore Kumar || 6:44
|-
| 4 || "How Are You Munni Bai" || S.P. Balasubrahmanyam || 5:38
|-
| 5 || "Mausam Mastana Hai" || Kishore Kumar, Lata Mangeshkar ||
|-
|}

== References ==
 

== External links ==
*  

 
 
 

 