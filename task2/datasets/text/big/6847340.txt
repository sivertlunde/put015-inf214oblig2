Unheimliche Geschichten
{{Infobox film
| name = Unheimliche Geschichten
| image = living_dead.jpg
| caption =
| writer = Richard Oswald
| starring = Paul Wegener Harald Paulsen Roma Bahn Mary Parker Gerhard Bienert
| director = Richard Oswald
| producer = Gabriel Pascal
| distributor = J.H. Hoffberg Company Inc.
| released =  
| runtime = 89 minutes
| country = Germany German
| budget =
| music =
| awards =
}}
Unheimliche Geschichten (Uncanny Stories) is a 1932 German horror/comedy film directed by the prolific Austrian film director Richard Oswald, starring Paul Wegener, and produced by Gabriel Pascal. 
 The Black The Suicide Club, set within a story frame of a reporters hunt for a crazy scientist. It is a black comedy revisiting many of the classic themes of the horror genre. It was Paul Wegeners first talking movie.

== Plot ==
A crazed scientist, Morder (Paul Wegener), driven even crazier by his nagging wife, murders her and walls her up in a basement, a la Poes The Black Cat. He then flees as the police and a reporter, Frank Briggs (Harald Paulsen), set out to track him down. Morder eventually escapes, by pretending to be insane, into an asylum. Though here the patients has managed to free themselves, lock up the guards, and take charge (inspired by Poes The System of Doctor Tarr and Professor Fether). After Morders final escape, he turns up as president of a secret Suicide Club (based on the short story by Stevenson).

==Cast==

* Paul Wegener
* Maria Koppenhöfer
* Blandine Ebinger
* Eugen Klöpfer
* Harald Paulsen
* Roma Bahn
* Mary Parker
* Paul Henckels
* Gerhard Bienert
* John Gottowt
* Erwin Kalser
* Franz Stein
* Gretel Berndt
* Ilse Fürstenberg
* Carl Heinz Charrell

== Worldwide Releases ==
The film is also known by numerous other titles including Fünf unheimliche Geschichten (Germany), Five Sinister Stories (International: English title), Ghastly Tales (USA), Tales of the Uncanny, The Living Dead (USA), and Unholy Tales (International: English title).   The film was later re-edited for an American market release known as "The Living Dead" in 1940, with many of the comedic scenes taken out. 

== Unheimliche Geschichten (1919) ==
 

There is a silent horror film of the same name directed and produced by Oswald, featuring Conrad Veidt. Unlike the 1932 Unheimliche Geschichten this was an anthology film combining five stories framed by a narrative set in a book shop (a technique later used for Paul Lenis Waxworks (film)|Waxworks). Portraits of a Strumpet, Death, and the Devil come to life and amuse themselves by reading stories--about themselves, of course, in various guises and eras.  The stories used were: The Black Cat; The Suicide Club; Anselme Heines Die Erscheinung; co-scripter Robert Liebmanns Die Hand; and Oswalds own Der Spuk. 

It was considered a lost film.  However, surviving prints of the film were edited together in Europe and shown as a whole feature at the annual Luminato arts festival in Toronto in June 2009. 

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
   
 
 