Hot Summer Night (1957 film)
{{Infobox Film |
| name           = Hot Summer Night
| image          = Jay C. Flippen in Hot Summer Night trailer.jpg
| image_size     = 200px
| caption        = Jay C. Flippen in film trailer
| director       = David Friedkin Morton Fine
| writer         = Morton Fine David Friedkin Edwin P. Hicks 
| starring       = Leslie Nielsen Colleen Miller Edward Andrews
| music          = André Previn
| cinematography = Harold J. Marzorati    Ben Lewis
| distributor    = Metro-Goldwyn-Mayer
| released       = February 15, 1957
| runtime        = 86 minutes
| country        =   English
| budget         = $355,000  .  gross = $500,000 
| preceded_by    = 
| followed_by    = 
}}
 MGM crime film starring Leslie Nielsen, Colleen Miller, and Edward Andrews. 

==Plot==
A reporter on his honeymoon goes to dangerous lengths to interview a notorious bank robber.

==Cast==
*Leslie Nielsen as William Joel Partain
*Colleen Miller as Irene Partain
*Edward Andrews as Deputy Lou Follett
*Jay C. Flippen as Oren Kobble
*James Best as Kermit
*Paul Richards as Elly Horn
*Robert J. Wilke as Tom Ellis
*Claude Akins as Truck Driver
*Marianne Stewart as Ruth Childers
==Box Office==
According to MGM records the film earned $250,000 in the US and Canada and $250,000 elsewhere resulting in a loss to the studio of $110,000. 
==References==
 
==External links==
* 
* 

 
 
 
 
 
 
 


 