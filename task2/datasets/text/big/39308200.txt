The Book Thief (film)
 
{{Infobox film
| name           = The Book Thief
| image          = The-Book-Thief_poster.jpg
| border         = yes
| alt            = A man being hugged by a girl, behind them a pile of books is on fire. 
| caption        = Theatrical release poster
| director       = Brian Percival
| producer       = {{Plainlist|
* Karen Rosenfelt
* Ken Blancato
}}
| screenplay     = Michael Petroni
| based on       =  
| narrator       = Roger Allam
| starring       = {{Plain list |
* Geoffrey Rush
* Emily Watson
* Sophie Nélisse
}}
| music          = John Williams 
| cinematography = Florian Ballhaus
| editing        = John Wilson
| studio         = Sunswept Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 125 minutes  
| country        = United States Germany
| language       = English German
| budget         = $19 million 
| gross          = $76.6 million 
}} war drama film directed by Brian Percival and starring Geoffrey Rush, Emily Watson, and Sophie Nélisse. The film is based on the 2005 novel The Book Thief by Markus Zusak and adapted by Michael Petroni. The film is about a young girl living with her adoptive German family during the Nazi era. Taught to read by her kind-hearted foster father, the girl begins "borrowing" books and sharing them with the Jewish refugee being sheltered by her foster parents in their home. The film features a musical score by Oscar-winning composer John Williams.

The Book Thief premiered at the Mill Valley Film Festival on October 3, 2013, and was released for general distribution in the United States on November 27, 2013. The film received mixed reviews upon its theatrical release with some reviewers praising its "fresher perspective on the war" and its focus on the "consistent thread of humanity" in the story,  while other critics faulting the films "wishful narrative".  With a budget of $19 million,    the film was successful at the box office, earning over $76 million.   
 BAFTA nominations Satellite Newcomer Award, and the Phoenix Film Critics Society Award for Best Performance by a Youth in a Lead or Supporting Role – Female. The film was released on Blu-ray Disc|Blu-ray and DVD on March 11, 2014. 

==Plot== Death (Roger Allam) tells about how the young Liesel Meminger (Sophie Nélisse) has piqued his interest. Liesel is traveling on a train with her mother (Heike Makatsch) and younger brother when her brother dies. At his burial she picks up a book that has been dropped by his graveside (a gravediggers manual). Liesel is then delivered to foster parents Hans (Geoffrey Rush) and Rosa (Emily Watson) Hubermann because her mother, a Communist, is fleeing Germany. When she arrives, Liesel makes an impression on a neighboring boy, Rudy Steiner (Nico Liersch).

Rudy accompanies her on her first day of school. When the teacher asks Liesel to write her name on the chalkboard, she is only able to write three Xs.  She draws the first X and the children laugh at her; she draws the second X and the children laugh at her more; and finally on the third X she is then asked to return to her seat, embarrassed, showing that she doesnt know how to read. Later that day, she is taunted by her schoolmates who chant "dummkopf" ("fool" in German) at her. One of the boys, Franz Deutscher, challenges her to read just one word to which Liesel responds by beating him up. She impresses Rudy, and they become fast  friends. When Hans, her foster father, realizes that Liesel cannot read, he begins to teach her, using the book that she took from the graveside. Liesel becomes obsessed with reading anything she can get her hands on.
 Hitler Youth movement. While at a Nazi book burning ceremony, Liesel and Rudy are bullied into throwing books onto the bonfire by Franz, but Liesel is upset to see the books being burned. When the bonfire ends, and everyone but she has left, she grabs a book that has not been burned. She is seen by Ilsa Hermann (Barbara Auer), wife of the mayor (Rainer Bock). Hans discovers that she has taken the book and tells her she must keep it a secret from everyone. One day, Rosa asks Liesel to take the laundry to the mayors house. Liesel realizes that the woman who saw her taking the book is the mayors wife, and she is scared she will be found out. Instead, Ilsa takes her into their library and tells Liesel she can come by anytime and read as much as shed like. Liesel also finds out about Johann here, who was the son of Ilsa and is now missing. Ilsa feels the loss of her son profoundly and has kept his library intact to commemorate him. One day Liesel is found reading by the mayor who not only puts a stop to her visits but dismisses Rosa as their laundress. Liesel continues to "borrow" books from the mayors library by climbing through a window.

There is a night of violence against the Jews (known historically as Kristallnacht). Max Vandenburg (Ben Schnetzer) and his mother, who are Jewish, are told by a friend that one of them (but only one) can escape, and Maxs mother forces him to go. Max goes to the Hubermanns house where Rosa and Hans give him shelter. Max is the son of the man who saved Hanss life in World War I. Max is initially allowed to stay in Liesels room while recovering from his trip, and they begin to become friends over their mutual hatred of Hitler since Liesel blames Hitler for taking her mother away. World War II begins, initially making most of the children in Liesels neighborhood very happy. Max is later moved to the basement so that he can move around more, but it is colder in the basement, and Max becomes dangerously ill. Liesel helps Max recover by reading to him with every spare moment.

One day while "borrowing" a book from the mayors home, Liesel is followed by Rudy. He discovers the secret of the books and also the secret of Max, whose name he reads on a journal Max gave to Liesel for Christmas. Rudy guesses that her family is hiding someone, and he swears to never tell anyone. Franz overhears Rudys last words of keeping it a secret. Franz violently pushes Rudy to reveal the secret, but Rudy throws the journal into the river to keep it away from Franz. However, after Franz has gone, Rudy plunges into the icy river to rescue the journal, and Liesel realizes that she can truly trust him. Soon a local party member comes by to check the Hubermanns basement, and they have to hide Max. However, they are told that their basement was being checked as a potential bomb shelter and realize they werent suspected of harboring a fugitive.
 conscripted into the army and must leave immediately.

On the way home from school one day, Liesel believes she has seen Max in a line of Jews being forcibly marched through town, and she begins screaming his name, running through the line. She is thrown to the sidewalk twice by German soldiers and finally relents when Rosa picks her up and takes her home. Within a few days, Hans returns from the front because he was injured by a bomb that hit another of his units truck.

The family is reunited only for a short time. One night the city is bombed by accident, and the air raid sirens fail to go off. Hans, Rosa, and Rudys family (except for his father who has also been conscripted into the army) are killed in the blast. Liesel was spared from the bombing because she fell asleep in the basement while writing in the journal given to her by Max. Neighbors bring Rudy out of his house, barely alive. He begins to tell Liesel that he loves her, but he dies before he can finish the sentence. Liesel begs him to "wake up," telling him that she will give him the kiss that he has been asking for; although he has already died, she kisses him. During this scene, Death is heard speaking again about how he received the souls of the dead. Liesel passes out, and one of the soldiers carries her to a stretcher. When she wakes up, she sees a book among the rubble and picks it up. She then sees the mayor and Ilsa drive up. With Ilsa being the only friend she has left, Liesel runs up to her and hugs her.

Two years later, after Germany has fallen to the Allies of World War II|Allies, Liesel is in the tailor shop owned by Rudys father, and she sees Max enter. Overjoyed by his survival and return, she runs to hug him. The final scene is Death speaking again about Liesels life and her death at the age of 90, mentioning her husband, children, and grandchildren, as we look over her modern day Manhattan Upper East Side apartment with pictures of her past and a portrait of her, upon which the camera lingers. The narrator does not state whom she married but implies that she became a writer. Death says that he has seen many good and bad things over the years, but Liesel is one of the few who ever made him wonder "what it is to live." Death concludes that the only truth he genuinely knows is that he is "haunted by humans".

==Cast==
* Geoffrey Rush as Hans Hubermann, Liesels kind-hearted foster father
* Sophie Nélisse as Liesel Meminger, the titular "book thief"
* Emily Watson as Rosa Hubermann, Liesels bad-tempered foster mother
* Ben Schnetzer as Max Vandenburg, a Jewish refugee staying with the Hubermanns
* Nico Liersch as Rudy Steiner, Liesels best friend and love interest
 
* Sandra Nedeleff as Sarah
* Hildegard Schroedter as Frau Becker
* Rafael Gareisen as Walter Kugler, Maxs best friend
* Gotthard Lange as the gravedigger
* Godehard Giese as the policeman on the train
* Roger Allam as Death, the films narrator
* Oliver Stokowski as Alex Steiner, Rudys father
* Barbara Auer as Ilsa Hermann, the mayors wife
* Heike Makatsch as Liesels mother
*     as Franz Deutscher, bully and leader of Rudys Hitler Youth squad
* Carina Wiese as Barbara Steiner, Rudys mother

==Production==
A search for an actress to play the eponymous book thief, Liesel Meminger, occurred across the world. On February 4, 2013, it was announced that Canadian actress Sophie Nélisse was cast in the role and that Australian actor Geoffrey Rush and English actress Emily Watson would be playing Memingers foster parents. 
 Bastille song "Haunt" as the music.
 the best-selling, award-winning book on which the film is based, confirmed on his blog that the film would be narrated by the character of "Death", as was the novel.  Fans theorized that Death might be voiced by the anonymous American actor that was used in the official trailer. It was then announced that English actor Roger Allam of Game of Thrones would portray Death in the film.

==Soundtrack==
The music for the film was composed by John Williams, and the soundtrack album containing the score was released by Sony Classical. The album was released in the United States on November 19, 2013.  It was nominated for an Oscar, BAFTA and Golden Globe for Best Original Score. It won Best Instrumental Album at the 57th Grammy Awards. 

The Book Thief marked the first time since 2005 that Williams has scored a film not directed by Steven Spielberg.

==Release==
Originally scheduled for January 17, 2014, The Book Thief s limited theatrical release was moved forward to November 8, 2013, due to the fact that it was finished ahead of schedule and in order to compete in the 2013–14 award season. It premiered at the Mill Valley Film Festival on October 3, 2013, and was screened at the Savannah Film Festival on October 29, 2013. It expanded to a wide release on November 27, 2013.

==Reception==

===Critical response===
The Book Thief received mixed reviews from critics. Review aggregation website Rotten Tomatoes gives the film a score of 46%, based on 134 reviews, with an average score of 5.6/10. The sites consensus states, "A bit too safe in its handling of its Nazi Germany setting, The Book Thief counters its constraints with a respectful tone and strong performances."  On Metacritic, the film has a score of 53 out of 100, based on 31 critics, indicating "mixed or average reviews". 

In her review for the New Empress Magazine, Mairéad Roche praised the film for providing a "fresher perspective on the war" through the experiences of ordinary Germans who lived through the Nazi era.    In addition to the "Oscar-baiting beautiful" cinematography and John Williamss film score that contribute to the films emotional appeal, Roche singled out the performance of young Sophie Nélisse as Liesel that "matches the well-measured and seemingly effortless efforts of both Rush and Watson".  Roche concluded,
 

In his review following the Mill Valley Film Festival, Dennis Harvey at Variety Magazine|Variety magazine wrote, "Rush generously provides the movies primary warmth and humor; Watson is pitch-perfect as a seemingly humorless scold with a well-buried soft side."  Harvey also praised the films cinematography and film score, noting that "impeccable design contributions are highlighted by Florian Ballhaus  somber but handsome widescreen lensing and an excellent score by John Williams that reps his first feature work for a director other than Steven Spielberg in years."   

In her review for "MSN UK", Emma Roberts gave the film 5 out of 5 stars, stating,
 

Stephanie Merry of The Washington Post was less impressed with the film, giving it two and half out of four stars. Merry felt that the film "has its moments of brilliance, thanks in large part to an adept cast" but that the film ultimately shows the difficulties of bringing a successful novel to the screen.    In his review for the Los Angeles Times, Robert Abele was also unimpressed, describing the film as "just another tasteful, staid Hollywoodization of terribleness, in which a catastrophic time acts as a convenient backdrop for a wishful narrative rather than the springboard for an honest one".   

===Accolades===
{| class="wikitable"
|- Award
!style="width:225px;"|Category Nominee
!style="width:50px;"|Result
|- 3rd AACTA AACTA International Awards  AACTA International Best Supporting Actor Geoffrey Rush
| 
|- 86th Academy Academy Awards    Academy Award Best Original Score John Williams
| 
|- 67th British British Academy Film Awards  BAFTA Award Best Film Music
| 
|- 19th Critics Choice Awards|Critics Choice Movie Awards Broadcast Film Best Young Actor/Actress Sophie Nélisse
| 
|- 71st Golden Golden Globe Awards    Golden Globe Best Original Score John Williams
| 
|- 57th Annual Grammy Awards Best Instrumental Composition John Williams
| 
|- 17th Hollywood Hollywood Film Awards Spotlight  Sophie Nélisse
| 
|- Phoenix Film Critics Society Best Performance by a Youth in a Lead or Supporting Role – Female 
| 
|- 18th Satellite Satellite Awards Satellite Award Best Supporting Actress Emily Watson
| 
|- Satellite Award Best Original Score John Williams
| 
|- Newcomer
|rowspan="2"|Sophie Nélisse
| 
|- 35th Young Young Artist Awards    Young Artist Best Leading Young Actress in a Feature Film
| 
|}

==Home media==
The Book Thief was released on Blu-ray Disc|Blu-ray and DVD on March 11, 2014.   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 