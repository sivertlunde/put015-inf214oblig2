All Men Are Brothers (film)
 
 
{{Infobox film name = All Men Are Brothers image = Allmenarebrothers.jpg alt =  caption = Film poster traditional = 蕩寇誌 simplified = 荡寇志 pinyin = Dàng Kòu Zhì}} director = Chang Cheh Wu Ma producer = Runme Shaw screenplay = Chang Cheh Ni Kuang story = Shi Naian starring = Danny Lee Bolo Yeung  Ku Feng Tetsuro Tamba Fan Mei-sheng Wong Kwong-yue music = Frankie Chan cinematography = Kuang Han-lu editing = Kwok Ting-hung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 101 minutes country = Hong Kong language = Mandarin budget =  gross = 
}}
All Men Are Brothers, also known as Seven Soldiers of Kung Fu, is a 1975 Hong Kong wuxia film based on the Chinese classical novel Water Margin. The film was produced by the Shaw Brothers Studio and directed by Chang Cheh and Wu Ma.

==Cast==
*David Chiang as Yan Qing Li Kui
*Chen Kuan-tai as Shi Jin
*Wong Chung as Shi Xiu Danny Lee as Zhang Shun Zhang Qing
*Yue Fung as Sun Erniang
*Ti Lung as Wu Song
*Zhu Mu as Fang La
*Tin Ching as Fang Tianding
*Tung Lam as Yuan Shang Shi Bao
*Bolo Yeung as Si Xingfang
*Lau Gong as Li Tianrun
*Wong Ching as Rebel general Lu
*Zhang Yang as Emperor Huizong of Song Li Shishi
*Ku Feng as Song Jiang
*Tetsuro Tamba as Lu Junyi
*Chin Feng as Wu Yong
*Chen Wo-fu as Xu Ning Michael Chan as Dong Ping
*Lau Dan as Lei Heng
*Lei Lung as Qin Ming
*Chang Wing-gai as Xie Bao
*Lee Wai-hoi as Xie Zhen Yang Xiong
*Leung Seung-wan as Liu Tang
*Lee Yung-git as Ruan Xiaoer
*Bruce Tong as Ruan Xiaowu
*Wai Gong-sang as Ruan Xiaoqi Yueh Hua as Lin Chong
*Lily Ho as Hu Sanniang Wang Ying
*Yeung Chak-lam as Cai Qing Paul Chun as Hua Rong
*Pang Pang as Lu Zhishen
*Ho Hon-chau as Chang Meng Zhou Tong
*Lee Hang as Dai Zong
*Woo Wai as Cai Fu
*Law Lok-lam as Huang Men
*Yen Shi-kwan as rebel officer
*Fung Hak-on as rebel officer
*Ko Hung as rebel officer
*Wong Pau-gei as rebel officer
*San Kuai as rebel officer
*Chui Fat as rebel officer
*Wong Shu-tong as rebel officer
*Chan Kwok-kuen as rebel officer
*Tung Choi-bo as rebel officer
*Chan Keung as rebel officer
*Ho Kei-cheong as rebel officer
*Yeung Pak-chan as rebel officer
*Chan Siu-gai as rebel officer
*Lee Chiu as rebel officer
*Chan Chuen as rebel officer killed by Dong Ping
*Chan Dik-hak as rebel soldier
*Ho Pak-kwong as rebel soldier
*Huang Ha as rebel soldier
*San Sin as rebel soldier
*Yuen Cheung-yan as rebel soldier
*Brandy Yuen as rebel soldier
*Ho Bo-sing as rebel soldier
*Lau Jun-fai as fisherman
*Yuen Shun-yi as fisherman
*Kong Chuen as fisherman
*Lam Yuen as fisherman
*Tino Wong as fisherman with hat
*Law Keung as Liangshan bandit
*Yuen Shing-chau as Liangshan bandit
*Lai Yan as Liangshan bandit
*Chui Kin-wa as Liangshan bandit
*Cheung Hei as inn keeper

==External links==
*  at the Internet Movie Database
*  at the Hong Kong Movie Database
*  on Hong Kong Cinemagic

 

 
 
 
 
 
 