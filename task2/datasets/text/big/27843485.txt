Ammoru
{{Infobox film
| name           = Ammoru
| image          = Ammoru.jpg
| image_size     =
| caption        =
| director       = Kodi Ramakrishna
| producer       = Shyam Prasad Reddy
| writer         = Satyanand
| narrator       = Rami Reddy Ramya Krishna
| music          = Sri Kommineni
| cinematography = C. Vijay Kumar
| editing        =
| studio         =
| distributor    = M.S. Arts
| released       = June 16, 1995
| runtime        = 129 minutes
| country        = India Telugu
| budget         =
}} Tamil dubbed version entitled Amman was also a hit. Ramya Krishna played the role of village Goddess Ammoru, while Soundarya portrayed Bhavani, her devotee. http://www.idlebrain.com/nosta/ammoru.html 

==The plot==
Bhavani (Soundarya), a lower-caste orphan and devotee of Goddess Ammoru, is responsible for the arrest of the evil Gorakh (Rami Reddy). Gorakh is released from prison, vowing to revenge. Bhavani is married to a doctor (Suresh) who happens to be related to Gorakh. The doctor goes abroad to study, leaving his wife unprotected. When Gorakhs mother Leelamma (Vadivukkarasi) tries to kill Bhavani, the Goddess Ammoru (Ramya Krishna) descends to earth and the takes the form of Bhavanis maid servant in order to protect her. Gorakh finds a way for the servant to be dismissed, kills Bhavanis infant daughter and tortures her husband, with the help of evil spirit Chenda. Eventually, the Goddess returns, and in a spectacular finale featuring the special effects, kills Gorakh. 

==Cast==
* Ramya Krishna	... 	Ammoru
* Soundarya	... 	Bhavani Suresh ... Suria Rami Reddy	... 	Gorakh
* Baby Sunayana
* Vadivukkarasi	... 	Leelamma
* Babu Mohan
* Kallu Chidambaram

==Soundtrack==
* Ammoru Maa Thalli (Lyricist: Rasaraju; Singer: K. S. Chitra)
* Challani Maatalli Ammoru (Lyricist: Mallemaala; Singer: Chitra)
* Dandalu Dandalu (Lyricist: Mallemaala; Singers: Madhavapeddi Ramesh, Nagore Babu)
* Emani Piluvanu Nenu (Lyricist: Mallemaala; Singers: Chitra, Nagore Babu)
* Kapadu Devatha (Lyricist: Mallemaala; Singer: Vandemataram Srinivas)
* Yeduru Tirigi Niluvaleka (Lyricist: Mallemaala; Singer: Chitra)

==References==
 

==External links==
*  
*  

 
 
 
 


 