Oba Nathuwa Oba Ekka
{{multiple issues|
 
 
 }}
{{Infobox film
| name = Oba Nathuwa Oba Ekka (With You, Without You)
| image =
| caption =
| director = Prasanna Vithanage
| producer = Lasantha Nawarathna Mohamed Adamaly
| starring = Shyam Fernando  Anjali Patil  Maheshwari Ratnam  Wasantha Moragoda
| music = Lakshman Joseph De Saram
| cinematography = M.D. Mahindapala
| editing = A. Sreekar Prasad
| released =  
| runtime = 90 minutes
| country = Sri Lanka
| language = Sinhala/Tamil
| followed_by =
}}
 Tamil language Sri Lankan filmmaker Prasanna Vithanage.

Based on the novella A Gentle Creature by Fyodor Dostoyevski, Oba Nathuwa Oba Ekka was adapted into a post war Sri Lankan background. Principal photography was shot in Bogawanthalawa (Central Province of Sri Lanka) and Sarasavi Studio in Colombo.

==Synopsis==
Sarathsiri, a man in his mid 40s, runs a pawn shop from his two-storied building in a remote town surrounded by tea plantations. He broods, rarely talks and in his spare time intently watches professional wrestling on TV.

One day, Selvi, a young girl, lands up at his pawn shop with a fistful of worthless trinkets and keeps coming back. Sarathsiri is intrigued by her and through his maid Lakshmi finds out that Selvi, a Tamil Christian is originally from Kilinochchin, an ethnic civil war stricken area of northern Sri Lanka. Her parents have sent her to this up country area of central Sri Lanka to save her life. Sarathsri gets to know that an older widowed businessman has asked for Selvi’s hand in marriage and immediately rushes in to declare his own desire to marry Selvi. She accepts and they get married.

Once at home Sarathsiri teaches her the basics of pawning business and tells her of his dream to own a tea plantation. However, he remains aloof, cold and unresponsive to Selvi who it seems is blossoming after the marriage. One day they go to see a South Indian Tamil movie at a local theatre. Selvi who has never seen a movie on a big screen is visibly happy. Sarathsiri however is his dour self and says it was a waste of money. While Selvi exudes a warmth and happiness, Sarathsri remains engrossed in his pawnshop and the wrestling bouts on TV.

One day out of the blue an old friend of Sarathsri appears at his door. Sarathsri drags his friend out for a drink, they return late at night drunk and have an argument. In the morning while Sarathsiri is asleep Selvi goes downstairs to answer a knock on the door. Gamini the friend is awake and asks Selvi to forgive him for barging in unannounced. Selvi discovers through Gamini that Sarathsiri is an ex Sri Lankan Army soldier.

Visibly shocked, Selvi leaves the house. Sarathsri goes looking for Selvi and finds her at the same bus stop where he had proposed her. At night Selvi confronts Sarathsiri and asks him why he hid from her the fact about his army past. Dissatisfied with his mono syllabic replies, Selvi  says if she knew he had served in the army, she would have never married him. She tells him about her two brothers who were killed by the Sri Lankan army. She informs him that her parents brought her here to save her form being raped by the Sri Lankan army soldiers. Unsettled by her ferocity, Sarathsiri grabs his pillow and blanket and goes downstairs. As he is leaving the upper floor Selvi follows him asking how many Tamil girls has he raped and how much gold has he stolen from them. Sarathsiri retorts saying if it wasn’t for him, she would be starving.

While rummaging through the closet, Selvi discovers a revolver in a hidden drawer. At night while Sarathsiri is asleep, she creeps downstairs and points the gun at his forehead.

Next day when she opens the hidden drawer, to her surprise the revolver is missing. She feels that Sarathsiri is watching her. Selvi gradually becomes unresponsive and slips into a depression. With her health failing, Selvi’s body is wracked with seizures.

Sarathsiri silently watches Selvi’s withering away. One day he finds Selvi singing to herself. It finally dawns on Sarathsri that he may lose Selvi. He falls on his knees and confesses to her, pleads with her to speak. He promises to change, be more responsive and begs her to give their relationship one more chance. He offers to go and find her missing parents but on hearing that Selvi goes into a seizure.

Sarathsiri becomes caring and nurturing. He feeds her, looks after her and talks to her. He confesses that as a soldier he gave false witness to protect his fellow soldiers including Gamini after they were accused of raping and killing of a Tamil girl. He tells her that he left the army because he couldn’t come to term with his lie. His confession makes her condition worse as she goes into a deep seizure.

In a desperate effort to get her back, Sarathsiri sells his business and offers to take her to India and show her films. One morning Selvi unexpectedly apologises to Sarathsiri about not being the wife he desired and promises to make amends. Overjoyed Sarathsiri kisses her from head to toe and leaves to buy air tickets for India. Selvi picks up the rosary and prays. Selvi is at peace. She has made her decision.

==Cast==
{| class="wikitable"
|-
! Actor !! Role
|- Shyam Fernando || Sarathsiri
|- Anjali Patil|| Selvi
|- Maheshwari Ratnam|| Lakshmi
|- Wasantha Moragoda || Gamini
|-
|}

==Music==
The original music for Oba Nathuwa Oba Ekka was composed by Lakshman Joseph De Saram.

==Editing==
Editing of the movie was done in Chennai, India. Renowned editor A. Sreekar Prasad who previously edited three of Prasanna Vithanage movies (Purahanda Kaluwara, Ira Madiyama, Akasa Kusum) did the editing for Oba Nathuwa Oba Ekka.

==Sound designing==
Sound designer was Tapas Nayak whose previous work include "Raavan|Ravan" and "Paa".

==Cinematography==
Veteran photographer M.D. Mahindapala joined Prasanna Vithanage for the fifth consecutive time to work as the cinematographer in Oba Nathuwa Oba Ekka.

Prasanna Vithanage and M.D. Mahindapala - Previous collaborations
* Anantha Rathriya
* Purahanda Kaluwara Ira Madiyama Akasa Kusum

==Production==
Movie script was completed in September 2011 and pre-production started in December 2011. Principal photography was done from February to March 2012. Post production started in April 2012. The movie was completed in July 2012.  Oba Nathuwa Oba Ekka is a co-production between Sri Lanka and Aakar productions India.

==Producers==
Lasantha Nawarathna and Mohamed Adamaly produced the movie.

==Screened film festivals==
* Montreal Film Festival – Canada
* Fukuoka Film Festival – Japan
* BFI London Film Festival – London
* Indian Film festival – Goa
* Chennai International Film Festival – Chennai, India
* Vesoul Film Festival – France
* Dublin International Film Festival – Ireland
* Hong Kong International Film Festival - Hong Kong
* Milano African Asian Film Festival
* Moscow International Film Festival
* Kerala International Film Festival
* Pune International Film Festival

==Awards==
* SIGNIS Award Milano African Asian Film Festival 2013
* BEST FILM Cyclod’or -Vesoul Asian Film Festival 2013
* NETPAC AWARD - Vesoul Asian Film Festival 2013
* BEST ACTRESS - International  Film Festival of India 2012

==References==
 

==External links==
*  
*   
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 