Bakgat
 
 
{{Infobox film | name = Bakgat!
  | image = 
  | caption =
  | director = Henk Pretorius James Carroll C.A. van Aswegen
  | writer = Henk Pretorius Danie Bester Ian Roberts Lea Clatworthy Neil Sharim Andrew Thompson Lelia Etsebeth Ciske Kruger Dantus Lundall Helene Lombard
  | music = Benjamin Willem
  | cinematography = Tom Marais
  | editing = C.A. van Aswegen
  | distributor = Ster-Kinekor Pictures
  | released =  
  | runtime = 96 minutes
  | language = Afrikaans
  | budget =
  }} teen comedy film directed by Henk Pretorius and written by Pretorius and Danie Bester.

==Plot==
Katrien (Cherié van der Merwe) is dumped by her boyfriend, high school rugby star Werner (Altus Theart), so he can focus his energy on the sport. To get even, she makes a bet with her friends that she can make a star out of the schools dorkiest boy, Wimpie Koekemoer (Ivan Botha), by pretending to be his girlfriend. The awkward Koekemoer rises to the challenge but Katrien finds herself falling unexpectedly in love with him, until he realizes she is merely using him.

==Production== Waterkloof and Eldoraigne participated in the production and the 2007 teams of both schools play the high school rugby teams in the film. These students, and the students who appear as extras, are all credited at the end of the film.    Most of the cast, according to reviewer Kgomotso Moncho, were recruited from the Tshwane University of Technology, where many of the high school scenes were filmed. 

This was the first film by director Pretorius, a television actor who had appeared as a regular cast member of the South African soapie Kruispad, and played in an episode of the HBO mini-series Generation Kill.   

==Reception==
Bakgat!  received mixed reviews, described by critics as amusing but cliched.  Shaun de Waal described it as "an efficient attempt to replicate the American teen comedy," criticized it as "largely predictable" and deriding its use of cartoonish stock characters such as "a mincing moffie thrown in for laughs."    

==Sequels==
Two sequels, Bakgat! 2 (2010) and Bakgat! tot die mag 3 (2013) have been released. The filming of Bakgat! 2 began in 2009 at the Potchefstroom Campus of North-West University, with Henk Pretorius returning as director and Botha and van de Merwe returning in their roles as Wimpie and Katrien. 

==References==

 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 