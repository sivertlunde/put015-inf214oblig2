Padithaal Mattum Podhuma
{{Infobox film
| name = Padithaal Mattum Podhuma படித்தால் மட்டும் போதுமா
| image =
| director = A. Bhimsingh
| writer = Aarur Dass
| screenplay = A. Bhimsingh
| story = Tharashankar Bandopadhya Savitri Rajasulochana S. V. Ranga Rao S. V. Sahasranamam Pasupuleti Kannamba|P. Kannamba K. Balaji M. V. Rajamma
| producer = P. Ramakrishnan
| music = Viswanathan–Ramamoorthy
| cinematography = G. Vittal Rao
| editing = A. Bhimsingh A. Paul Duraisingam R. Thirumalai
| studio = Ranganathan Pictures
| distributor = Sivaji Productions
| released =  14 April 1962 
| runtime = 157 mins
| country =   India
| language = Tamil
| budget =
}}
 Savitri and Rajasulochana in lead roles. 
The film, produced by P. Ramakrishnan under Ranganathan Pictures, had musical score by Viswanathan–Ramamoorthy and was released on 14 April 1962. 

==Plot==
This story mainly revolves around an anonymous poison letter which causes turns and turbulances of the people affected.

Raju (K. Balaji) and Gopal (Sivaji Ganesan) are cousins who are very attached to each other. Gopal is the son of Zamindar (S. V. Sahasranamam) and Mangalam (Pasupuleti Kannamba|P. Kannamba), is uneducated, rough and loves hunting. Whereas Raju is the orpaned son of the Zamindar’s elder brother and raised by Zamindar and Mangalam as their first son, is educated, well mannered and coward.

Zamindar and Mangalam plans go get Raju and Gopal married and they seek for the marriage broker Kailasam (M. R. Radha). Kailasam is a Saivite is married to Andal (C. K. Saraswathi) a Vaishnavite and they have a daughter Manorama (Tamil actress)|Manorama. The couple often quarrels pertaining the difference between their religion belief provides comic relief.

Kailasam suggest that Raju to be married to the educated and wealthy Meena (Rajasulochana), daughter of Rao Bahadur (S. V. Ranga Rao) and M. V. Rajamma and Gopal to be married to uneducated, country girl Seetha (Savitri (actress)|Savitri) sister of farmer Moorthy (R. Muthuraman) and daughter of widowed Radha Bai. It happens that Zamindar and Mangalam are unable to visit the brides’ place, Kailasam arranges that Raju to visit Seetha and Gopal to visit Meena. Gopal learns a few English words from Raju before leaving to Rao Bahadur’s place. Rao Bahadur and Meena were impressed with Gopal and mistakenly concludes that he is very well educated. A. Karunanidhi who is the servant of Rao Bahadur, serves Gopal. At the same time, Raju sees Seetha and was very much impressed with her deeds and wishes to marry her.

Raju who is a coward and unable to express himself to Zamindar and Mangalam, hatches a wicked plan by writing an anonymous poison letter to Rao Bahadur defaming himself as drunkard, womaniser and non-worthy and to Moorthy defaming Gopal as the same. Raju’s plan works where Rao Bahadur is irked by this and plans to have Gopal to be the groom for Menna and Moorthy plans to have Raju to be the groom for Seetha.

Much to Kailasam’s persuassion to Zamindar the wedding takes place where Raju is married to Seetha and Gopal marries Meena. A. Karunanidhi meets Manorama during the wedding, falls for her and decides to stay back with Rao Bahadur’s permission. Matters become serious on their first night where Meena found out that Gopal is uneducated and blames Gopal a liar for posing as an educated man before her. On the other hand, Raju and Seetha leads a happy life. The next day both couples leave to their in-laws place. Raju has no problems but Gopal was embarrassed to face Meena’s educated friends and leaves home without Meena. Upon reaching home, Gopal lies to Zamindar and Mangalam that Meena had to stay behind in order to sit for examinations. Rao Bahadur and Meena believes that the letter was sent by none other than Gopal who likes Meena at first sight, cheats them as an educated and would like to have her instead of Raju. When Raju and Seetha arrives back, Zamindar and Mangalam leads them to Raju’s father’s house where the couples live happily. Gopal gets very much attached to Seetha and accepts her and treats her as his mother. Gopal continues on hunting and is assisted by A. Karunanidhi.

As time moves on, Zamindar calls in Kailasam to find out from Rao Bahadur when is he sending Meena down. Rao Bahadur angrily tells Kailasam and shows him the letter. Kailasam also believes in this and reveals the matter to Zamindar and Mangalam. Furious on hearing this, Zamindar hits Gopal and drives him away but Mangalam believes that her son is innocent. Upon knowing this, Raju becomes restless and guilty that his deed leads to a serious matter and tries to steal the letter from Gopal at night. Gopal wakes up suddenly and unsuspectingly questions but Raju behaves under the pretext on searching for his books and unwontedly drops the letter on the floor while leaving the room. Seeing the lights were on Mangalam enters Gopals room and found out that the hand writing of the letter belongs to none other than Raju and gets very mad. Gopal urges Mangalam on promising not to let anyone know about this. In conjunction with this event, Gopal falls ill and Seetha nurses him back to be healthy again.

Raju and Seetha sets off to Rao Rahadur’s place to compromise where M. V. Rajamma insists Meena to reunite with Gopal but Rao Bahadur and Meena does not give at all. Kailasam suggest that Zamindar to send a letter to Rao Bahadur stating that should Meena not going to live with Gopal, he would seek another bride for Gopal to remarry. Rao Bahadur forcefully sends Meena to Gopals place. Meena who disgusts at the sight of Gopal hurts him badly with insult. Gopal angrily leaves the room and gets drunk and return to Meena and when quarrel broke again, whips her which results her to flee back to her parents place. Seetha in dismay orders Gopal to apologize to Meena and to bring her back. Gopal goes over to Rao Bahadur’s place and pleads to Meena to forgive him and to follow him back but she refuses. Leave with no option, Gopal tries to pull Meena forcefully when Rao Bahadur intervenes and hits Gopal badly with the walking stick.

Gopal leves Rao Bahadur’s house with much embarrassment. He is very much humiliated and the feeling of unable to fulfil to Seetha’s orders makes him go berserk. On the way back, Gopal goes to the jungle and grabs the riffle from A. Karunanidhi who is hunting over there and shoots whatever comes to his path. A. Karunanidhi rushes over to Zamindar’s place and informs them the situation. Raju who is in guilty feeling that all this problem’s originated from his stupid deeds, goes over and tries to stop Gopal. In turn, Gopal who is devastated blames Raju for all his miseries. Seetha comes over and shocked to see that Gopal pointing the riffle to Raju. Seetha tries to stop when Raju also tries to prevent Gopal from pulling the trigger when the riffle shoots and killing Raju and Seetha faints. Before dying Raju requests Gopal to promise not to inform regarding that he is the one who composed the letter. Gopal is arrested.

Mangalam falls ill and informs Zamindar, Rao Bahadur, M. V. Rajamma and Meena that the letter originated from Raju and not Gopal. Rao Bahadur and Meena feels guilty for their action towards Gopal all these while and repents. Meanwhile, the public porsecuter O. A. K. Thevar proves to the judge that Gopal is guilty. Rao Bahadur and Meena rushes to Seetha’s hometown but Moorthy who understands the situation but unable to help as Seetha avoids to meet anyone. Rao Bahadur and Meena pleads to Seetha and she agrees to testify in court. Seetha’s testify saying that the riffle was handled by Seetha, Raju and Gopal and unsure that who pulled the trigger, gets Gopal released. Gopal and Meena leaves happily while Seetha goes back to her hometown.

==Cast==
*Sivaji Ganesan as  Gopal
*M. R. Radha as Kailasam Savitri as Seetha
*Rajasulochana as Meena
*S. V. Ranga Rao as Rao Bahadur
*S. V. Sahasranamam as Zamindar
*K. Balaji as Raju
*Pasupuleti Kannamba|P. Kannamba as Mangalam
*M. V. Rajamma as Servant of Rao Bahadur
*A. Karunanidhi
*R. Muthuraman as Moorthy
*C. K. Saraswathi as Andal Manorama
*Radha Bai as Moorthy & Seethas mother
*O. A. K. Thevar as Lawyer

==Crew==
* Producer: P. Ramakrishnan
* Production Company: Ranganathan Pictures
* Director: A. Bhimsingh
* Music: Viswanathan–Ramamoorthy
* Lyrics: Kannadasan & Mayavanathan
* Story: Tharashankar Bandopadhya
* Screenplay: A. Bhimsingh
* Dialogues: Aarur Dass
* Art Direction: Ganga
* Editing: A. Bhimsingh, A. Paul Duraisingam & R. Thirumalai
* Choreography: Chinni & Sampath
* Cinematography:  G. Vittal Rao
* Stunt: None
* Songs Recording & Re-Recording: T. S. Rangasamy & J. J. Manickam
* Audiography: V. C. Sekar
* Dance: None

==Soundtrack==
The music was composed by Viswanathan–Ramamoorthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- Kannadasan || 03:32
|-
| 2 || Kaalam Seidha Komalithanatthil || P. B. Sreenivas, A. L. Raghavan, G. K. Venkatesh || 03:32
|- 
| 3 || Naan Kavignanum Alla || T. M. Soundararajan || 02:52
|-
| 4 || Nallavan Enakku || T. M. Soundararajan, P. B. Sreenivas || 03:20
|-
| 5 || Ohohoho Manidhargale || T. M. Soundararajan || 03:18
|- 
| 6 || Pon Ondru Kanden || T. M. Soundararajan, P. B. Sreenivas || 03:13
|- 
| 7 || Thannilavu Theniraikka || P. Suseela || Mayavanathan || 03:23
|- 
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 