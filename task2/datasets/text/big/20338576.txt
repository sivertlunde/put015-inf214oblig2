Ask Any Girl (film)
{{Infobox film
| name           = Ask Any Girl
| image          = Ask any girl poster.jpg
| image size     =
| caption        = Theatrical poster
| director       = Charles Walters
| producer       = Joe Pasternak George Wells Winifred Wolfe (novel)
| narrator       =
| starring       = David Niven Shirley MacLaine Gig Young
| music          = Jeff Alexander
| cinematography = Robert J. Bronner
| editing        = John McSweeney Jr.
| studio         = Metro-Goldwyn-Mayer Euterpe, Inc.
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $1,140,000  . 
| gross          = $3,475,000 
}}
Ask Any Girl is a 1959 Metro-Goldwyn-Mayer romantic comedy film starring David Niven, Shirley MacLaine and Gig Young. 

==Plot==
A wide-eyed Meg Wheeler comes to New York City and takes a job in market research for a large firm. Shes also keeping an eye open to meet the right man, her research making her aware that the United States has five million more females than males.

Upon meeting two clients, the reserved and somewhat stodgy Miles Doughton and his playboy younger brother Evan, it doesnt take long for Meg to realize shes romantically interested in Evan.

Miles is willing to help. He has seen so many of his brothers conquests come and go that he knows what Evan likes in a girl. Therefore, in a Pygmalion (play)|Pygmalion-like way, he sets out to transform Meg into exactly that kind of girl. What she doesnt know is that Miles secretly comes to want her for himself.

==Cast==
*David Niven ... Miles Doughton
*Shirley MacLaine ... Meg Wheeler
*Gig Young ... Evan Doughton
*Rod Taylor ... Ross Tayford
*Jim Backus ... Maxwell
*Claire Kelly ... Lisa
*Elisabeth Fraser ... Jennie Boyden
*Dodie Heath ... Terri Richards
*Read Morgan ... Bert
*Carmen Phillips ... Refined young lady

==Reception==
According to MGM records, the film earned $2,075,000 in the US and Canada and $1,400,000 elsewhere, turning a profit for the studio of $505,000. 

It recorded admissions of 255,797 in France. 

==Awards and nominations== BAFTA Award Golden Globe, losing out to Marilyn Monroe in Some Like It Hot.
 The Time Machine (1960). 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 