The Front
 
{{Infobox film
| name           = The Front
| image          = wallenthefront2.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Martin Ritt Jack Rollins
| writer         = Walter Bernstein  Michael Murphy Herschel Bernardi Andrea Marcovicci Remak Ramsay Lloyd Gough
| music          = Dave Grusin Michael Chapman
| editing        = Sidney Levin
| distributor    = Columbia Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Front  is a 1976 comedy-drama film about the Hollywood blacklist during the age of live television. It is written by Walter Bernstein, directed by Martin Ritt and stars Woody Allen and Zero Mostel.

Because of the blacklist, a number of artists, writers, directors and others were rendered unemployable, having been accused of subversive political activities in support of Communism or of being Communists themselves.  
 Communists and Communist sympathizers.

==Plot== Michael Murphy) has been blacklisted, he asks Howard to sign his name to the TV scripts.   

Howard agrees out of friendship and because he needs the percentage of the writers fee that Miller will pay him. The scripts are submitted to network producer Phil Sussman (Herschel Bernardi), who is pleased to have a writer not contaminated by the blacklist. Howards script also offers a plum role for one of Sussmans top actors, Hecky Brown (Zero Mostel).

Howard becomes such a “success” that Miller’s other friends hire him to be their front. The quality of the scripts and Howards ability to write so many impresses Sussmans idealistic script editor, Florence Barrett (Andrea Marcovicci), who mistakes him for a principled, artistic talent. Howard begins dating her but changes the subject whenever she wants to discuss his work.

McCarthyism is rampant and investigators are trying to expose and blacklist Communists in the entertainment industry. They are making life hell for Hecky Brown. They affect his ability to find work and pressure Sussman to drop him from the show.

Hecky takes a liking to Howard and invites him to the Catskills, where he is booked to perform on stage. The club owner short-changes the entertainer on his promised salary, furthering Heckys distress. 

The professional humiliation and loss of income take their toll, resulting in Heckys suicide. 

Howard witnesses the harsh reality of what the terrible actions of the right-wing “Freedom Information Services” can do. Suspicion is cast his way and Howard is called to testify before the House Un-American Activities Committee. He ends up revealing privately to Florence that he is not a brilliant writer at all, just a humble cashier.

Howard decides to outwit the committee. He hatches a scheme in which he will respond to all questions, not refusing to answer but in such a general way that he will admit or deny nothing. After briefly enduring the HUAC questioning&nbsp;– including being asked to speak ill of the dead Hecky Brown&nbsp;– Howard takes a stand. He ends the interview with a blatant act of defiance, which gets him arrested and convicted for contempt of Congress, a punishment he accepts with pride.

==Reception==
===Critical response===
Critical reception of The Front was divided between those who thought it effectively and amusingly dealt with the topic of McCarthyism and those who thought it a superficial gloss instead of a pithy statement about the McCarthy era. In 1976, reviewing it for the   are not interested in subtleties. Yet, even in its comic moments, The Front works on the conscience. "It recreates the awful noise of ignorance that can still be heard." (Canby, 1976)  He said that, while the film does not directly attack or address the political era, it does communicate its message: Do not rat on people.   Furthermore, he emphasized that The Front encourages the viewer to understand the emotional consequences of blacklisting and finger-pointing by telling the experience of one man.   in one of his own films, in a peculiar way you take him for granted; here you appreciate his skill, because you miss him so much when hes offscreen." 

Roger Ebert dismissed the political value of The Front: "What we get are the adventures of a schlemiel in wonderland". He felt that the Woody Allen character was too comic and unconvincing a writer to represent the true nature of "front" writers. He added that Hecky Brown was a worthwhile character: "The tragedy implied by this character tells us what we need to know about the blacklists effect on peoples lives; the rest of the movie adds almost nothing else". 

===Accolades=== BAFTA Award for Best Supporting Actor. Andrea Marcovicci was nominated for the 1977 "Golden Globe Award for New Star of the Year – Actress".

==Historical connections==
The movie draws from real life incidents in its depiction of the characters. A scene in which Hecky (played by Mostel) goes to entertain at a mountain resort, and then is cheated out of part of his fee, is based on a real life incident described by Bernstein in his memoirs Inside Out: A Memoir of the Blacklist. In the book, Bernstein describes how Mostel came to entertain at the Concord hotel in the Catskills, where he used to entertain as a rising comic, because he desperately needed the money. The manager of the Concord promised him $500 but, when he arrived, reduced that to $250, according to Bernstein. In the movie, Hecky has a violent scene when, after the performance, told that he is cheated. In real life, Mostel was told before the performance and acted out his hostility during the performance, by cursing at the customers, who thought it was part of the act.

The subsequent suicide of Hecky, shown in the film as his leaping from a hotel window, has a historical parallel in the suicide of blacklisted actor Philip Loeb, who took an overdose of sleeping pills in a hotel room. Loeb was a friend of Mostels, according to Bernsteins memoirs.
 

==See also==
* List of American films of 1976

==References==
Notes
 
 

==External links==
 
*  
*  
*  


 

 
 
 
 
 
 
 
 
 
 
 
 
 