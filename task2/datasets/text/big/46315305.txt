Usfahrt Oerlike
{{Infobox film
| name           = Usfahrt Oerlike
| image          = 
| caption        = 
| director       = Paul Riniker
| producer       = Paul Riniker Rudolf Santschi
| writer         = Christa Paul Thomas Hostettler Jonas Schürch  
| starring       = Jörg Schneider Mathias Gnädinger
| music          = Marcel Vaid
| cinematography = Felix von Muralt
| editing        = 
| distributor    = Frenetic Films   
| released       =  
| runtime        = 94 minutes
| country        = Switzerland
| language       = Swiss German
| budget         = 
}}
Usfahrt Oerlike ( ) is a 2015 Swiss German language film. It was filmed and produced at locations in Zürich respectively in Switzerland, and is the second last film starring Mathias Gnädinger.   

== Cast ==
* Jörg Schneider as Hans Hilfiker
* Mathias Gnädinger as Willi Keller
* Beatrice Blackwell as Mary 
* Daniel Rohr as Beat Hilfiker 
* Heidi Maria Glössner as Emilie Brütsch 
* Leo Thomas as Sam 
* Katharina von Bock as Direktorin Rossmöller 
* Stefano Wenk as Oliver 
* Klaus-Henner Russius as Dr. Claus Vogel 
* Monica Gubser as Annemarie 
* Vincenzo Biagi as Dieter 
* Sabine Timoteo as Ronja 
* Lotti Happle as Nicole 
* Aaron Hitz as Thomas 
* Martin Villiger as Chorleiter 

== Plot (excerpt) ==
Hans (Jörg Schneider) resumes that he had a good life: He has seen the world and loved his wife Martheli. Since two years, Martha is dead, he can barely cope with everyday life and Hans is tired, he wants to die. His best friend Willi (Mathias Gnädinger) will help him to implement this plan.    

An unfortunate accident forced Hans to enter for a few weeks the local retirement home. However, neither the nurse Mary (Beatrice Blackwell) nor the conversations with Mrs. Brütsch (Heidi Maria Glössner) motivate Hans to enjoy his life in the age institution. But when his son suddenly enters his life again, Hans seems to hesitate to end his life by suicide...   

== Title ==
The title of the film derives from the Swiss German term meaning Exit Oerlikon (Zürich)|Oerlikon; usfahrt may also refer to "end" (of life).

== Reception == Kati Moser, Schweizer Illustrierte on 30 January 2015.}}
 

== Production ==
For the plot,    the play EXIT by Thomas Hostettler was adapted.    The film was shot and produced in Zürich and at locations in Switzerland. According to Paul Riniker,  a sequel was planned but will not be realized as Mathias Gnädinger died on 3 April 2015.    Premiered at the Solothurn Film Festival on 23 January 2015, Usfahrt Oerlike was aired in the the Swiss German cinemas starting on 29 January 2015.   

== Festivals ==
* 2015 Schaffhausen Filmfestival 
* 2015 Solothurn Film Festival

== Awards ==
* 2015 Solothurn Film Festival: Won Publikumspreis (Prix du public).   

== References ==
 

==External links==
*   
* 

 
 
 
 
 
 
 
 