Girls Can't Swim
{{Infobox film
| name           = Girls Cant Swim
| image          = Les filles ne savent pas nager.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Anne-Sophie Birot
| producer       = 
| writer         = Anne-Sophie Birot Christophe Honoré
| screenplay     = 
| story          = 
| based on       =  
| starring       = Isild Le Besco Karen Alyx Pascale Bussières Pascal Elso Marie Rivière Yelda Reynaud Sandrine Blancke Julien Cottereau Dominique Lacarrière
| music          = Ernest Chausson
| cinematography = Nathalie Durand
| editing        = Pascale Chavance
| studio         = Centre National de la Cinématographie
| distributor    = Wellspring (USA)
| released       = 18 October 2000
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 French coming of age  drama film. It was first shown at the 2000 Montreal Film Festival.

==Summary==
Gwen (Isild Le Besco) is a teenager living in a coastal town in Brittany; Lise (Karen Alyx) is her city-living best friend. They meet up each summer as Lises family visits.
This years visit is different though - Lise is dealing with her distant fathers death, and Gwen has become promiscuous with boys - with the tensions affecting their friendship.

==Reception==
Stephen Holden of the New York Times said "Girls Cant Swim ultimately lacks the epic dimension of Y Tu Mamá También, but its vision of that awkward age when sex threatens to overwhelm everything else is acute enough to make everyone who has been there squirm with recognition."  Roger Ebert gave the film two stars, saying "The phrase "coming of age," when applied to movies, almost always implies sex, but Girls Cant Swim has nothing useful to say about sex (certainly not compared to Catherine Breillats brilliant Fat Girl from last year), and is too jerky in structure to inspire much empathy from us." 

==Cast==
* Isild Le Besco as Gwen
* Karen Alyx as Lise
* Pascale Bussières as Céline
* Pascal Elso as Alain
* Marie Rivière as Anne-Marie
* Yelda Reynaud as Solange
* Sandrine Blancke as Vivianne
* Julien Cottereau as Frédo
* Dominique Lacarrière as Rose

==References==
 

==External links==
* 

 
 
 
 
 


 
 