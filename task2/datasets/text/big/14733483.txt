The Day the Earth Stood Still (2008 film)
{{Infobox film
| name           =  The Day the Earth Stood Still
| image          =  The Day the Earth Stood Still.jpg
| caption        =  Theatrical release poster
| director       = Scott Derrickson
| producer       = Erwin Stoff Paul Harris Boardman Gregory Goodman
| based on       =  
| screenplay     = David Scarpa Edmund H. North  
| starring       = Keanu Reeves Jennifer Connelly Jaden Smith Kathy Bates Jon Hamm John Cleese Kyle Chandler
| music          = Tyler Bates
| cinematography = David Tattersall
| editing        = Wayne Wahrman
| distributor    = 20th Century Fox
| released       =  
| runtime        = 103 minutes
| country        = United States Canada
| language       = English
| budget         = $80 million 
| gross          = $233,093,859 
}}
 1951 film Harry Bates, and the 1951 screenplay adaptation by Edmund H. North.
 environmental damage to the planet. It follows Klaatu, an alien sent to try to change human behavior or eradicate them from Earth.

The film was originally scheduled for release on May 9, 2008, but was released on a roll-out schedule beginning December 12, 2008, screening in both conventional and IMAX theaters.          The critical reviews were mainly negative, with 186 reviews collected by Rotten Tomatoes showing only 21% of them were positive; typically the film was found to be "heavy on special effects, but without a coherent story at its base".    In its opening week, the film took top spot at the U.S. box office and has since grossed over $233 million worldwide. The Day the Earth Stood Still was released on home video on April 7, 2009.

==Plot==
 
In 1928, a solitary mountaineer encounters a glowing sphere. He loses consciousness and when he wakes, the sphere has gone and there is a scar on his hand where a sample of his DNA has been taken.
 United States US military force.

An alien emerges and Helen moves forward to greet it, but amidst the confusion, a soldier shoots the alien. A gigantic robot then appears that temporarily disables everything in the vicinity. The alien voices the command "Klaatu barada nikto" to shut down the robots defensive response.
 Klaatu (Keanu Secretary of alien civilizations, sent to talk to the leaders of Earth at a United Nations conference in the city. Jackson instead orders that Klaatu be sent to a secure location for interrogation. Klaatu manages to escape and Helen and her stepson Jacob (Jaden Smith) give him a ride out of the area. The trio are pursued and harassed by the authorities for the rest of the film.

The presence of the sphere, and others like it around the world, causes a widespread panic. The military launch a drone attack on the sphere, but are thwarted by the robot. They cautiously enclose the robot and transport it to Virginia. 

Klaatu meets with another alien, Mr. Wu (James Hong), who has lived on Earth for 70 years. Wu tells Klaatu that he has found the human race to be destructive and unwilling to change, which matches Klaatus experience. Klaatu decides that the planet, with its rare ability to sustain complex life, must be cleansed of humans to ensure that it can survive. Klaatu orders smaller spheres to collect specimens of animal species, to preserve them off the planet. Helen quizzes Klaatu about his statement that he has come to save the Earth and discovers that he means to save the Earth from destruction by humankind.
 consume everything in their path. The swarm starts to erase all trace of the human race.
 Bach playing in the background. Klaatu finally realizes that he has misjudged humanity when he sees Helen comforting Jacob at his fathers grave. They head for the sphere in Central Park to try to stop the swarm.

Klaatu warns that even if he manages to stop the swarm, there will be a price to the human way of life. The swarm arrives and Jacob and Helen become infected by the nano-machines. Klaatu walks through the swarm to the sphere, touching it moments before his body is consumed. The sphere deactivates the swarm, saving humanity, but at the expense of Earths electrical capability, which is apparently rendered useless.    Writers DVD commentary  The giant sphere leaves the Earth. 

==Cast== bigger stick." floods the world in the Old Testament, but is gentle and forgiving by the time of the New Testament.  He spent many weeks revising the script, trying to make Klaatus transition from alien in human form to one who appreciates their emotions and beliefs subtle and nuanced.  Derrickson, the director, said that although Reeves would not use actions "that are highly unusual or highly quirky", he nevertheless "keeps you aware of the fact that this being youre walking through this movie with is not a human being."    At Reeves insistence, the classic line "Klaatu barada nikto" was added to the script after initially being omitted.    The line was recorded many times, and it was decided to combine two recordings: one where Reeves said it normally, and a reversed version where he said the line backwards, creating an "alien" effect.  astrobiologist at Princeton University who is recruited by the government to study Klaatu. Connelly was Derricksons first choice for the part.  She is a fan of the original film and felt Patricia Neals original portrayal of Helen was "fabulous", but trusted the filmmakers with their reinterpretation of the story and of Helen, who was a secretary in the original.  Connelly emphasized that Helen is amazed when she meets Klaatu, as she never believed she would encounter a sentient alien like him, after speculating on extraterrestrial life for so long.  Connelly was dedicated to understanding her scientific jargon, with Seth Shostak stating she did "everything short of writing a NASA grant application".  Billy Gray) The Matrix sequels, which featured his mother Jada Pinkett-Smith - although he was only three years old at the time. 
*  , I spoke a lot of Russian without having any idea what it means."   niche genre when the original film was made, and that it used science fiction to make topical issues more approachable. Hamm had the same feelings for this remake.  Originally, Hamms character was French and named Michel.  Although he is interested in math and science, Hamm found his technical dialogue difficult and had to film his lines repeatedly. 
* Kathy Bates as Regina Jackson, the United States Secretary of Defense. Bates only had two weeks to film her scenes, so she often requested Derrickson act out her lines so she would directly understand his aims for her dialogue, rather than interpret vague directions. 
* Kyle Chandler as John Driscoll, an official in the US Government, who goes down to check out the research on Gort, where his actions lead to the deaths of everyone in the base including himself, as the base is put into a lockdown.
* James Hong as Mr. Wu
* Brandon T. Jackson as Target Tech

==Production==

===Development===
In 1994, 20th Century Fox and Erwin Stoff had produced the successful Keanu Reeves film Speed (1994 film)|Speed. Stoff was at an office at the studio when he saw a poster for the 1951 film The Day the Earth Stood Still, which made him ponder a remake with Reeves as Klaatu.   
By the time David Scarpa started writing a draft of the script in 2005,  Thomas Rothman was in charge of Fox and felt a responsibility to remake the film.  Scarpa felt everything about the original film was still relevant, but changed the allegory from nuclear war to environmental damage because "the specifics of   we now have the capability to destroy ourselves have changed."    Scarpa noted the recent events of Hurricane Katrina in 2005 informed his mindset when writing the screenplay.    He scrapped Klaatus speech at the conclusion of the story because "audiences today are  willing to tolerate that. People dont want to be preached to about the environment. We tried to avoid having our alien looking out over the garbage in the lake and crying a silent tear  ." 
 The Wizard remake of Invasion of the Body Snatchers. Klaatu was made more menacing than in the original, because the director felt he had to symbolize the more complex era of the 2000s.  There was debate over whether to have Klaatu land in Washington, D.C., as in the original; but Derrickson chose New York City because he liked the geometry of Klaatus sphere landing in Central Park.    Derrickson also did not write in Gorts original backstory, which was already absent from the script he read. He already thought the script was a good adaptation and didnt want the negative connotations of fascism from the original film. 
 asteroidal ellipse, but moving at nearly three times ten to the seventh meters per second. More likely, they would say that there was a goddamned rock headed our way!"   

===Filming=== Deer Lake, Jericho Park, and Simon Fraser University.  The film was originally scheduled for release on May 9, 2008, but it was delayed until December 12, 2008, because filming commenced later than scheduled.  By the time preproduction had started, Scarpa had written 40 drafts of the script.  The film was mostly shot on sets because it was winter in Vancouver. 
 Gort emphasized gray and orange, which was inspired by an image of lava flowing through a gray field. Derrickson opted to shoot on traditional film, and rendered the colors in post-production to make them more subtle, for realism. 
 Montana State University in faint pencil marks. Keanu Reeves and John Cleese drew over these in chalk. 

As Fox had a mandate to become a carbon neutral company by 2011, The Day the Earth Stood Still s production had an environmentally friendly regimen. "Whether it was because of this movie thematically or it was an accident of time, there were certain things production-wise weve been doing and been asked to do and so on," said Erwin Stoff.  To prevent the wasting of paper, concept art, location stills and costume tests were posted on a website created by the production for crew members to reference. Costumes were kept for future Fox productions or given to homeless shelters, rather than thrown away. Hybrid vehicles were used and crew members had orders to turn off their car engines if they sat in their vehicles for more than three minutes. 

===Effects===
  and behind him, the new biological spaceship resembling an orb]]

Weta Digital created the majority of the effects, with additional work by Cinesite and Flash Film Works. The machines of Klaatus people have a biological basis rather than a mechanical one, as Derrickson theorized that their mastery of ecology would demonstrate their level of sophistication.  Derrickson deemed a modern audience would find the originals flying saucer amusingly obsolete and unique to the originals milieu.  The director also noted that the original The Day the Earth Stood Still had influenced many films, so his technicians  needed to bring new ideas to the remake. 

The effects team approached the new spacecrafts design as inter-dimensional portals resembling orbs. The script had specified the inside of the orbs as a "white limbo-y thing", but visual-effects consultant Jeff Okun explained this was deleted for being too "cheesy".  Derrickson felt not showing the inside of the ship, unlike the original, would make the audience more curious.  As well as computer-generated spheres—such as Klaatus   ship, or a   tall orb that rises from the sea—  spheres,   in diameter, were sculpted by Custom Plastics, which built spheres for Disney theme parks. The spheres were split in two to make transportation easier. It was difficult placing lights inside them without making them melt. The visual-effects team looked at natural objects, including water droplets and the surfaces of Jupiter and Saturn for the spheres texture. 

Derrickson emphasized a Trinity-like relationship between the sphere, Klaatu, and Gort.  Klaatu is initially depicted as a radiant focus of sentient light. He is then depicted as a   gray "walking womb" shape which finally takes on a completely human appearance. The filmmakers conceived the transitional form because they pondered the idea of humans mistaking space suits for alien skin. Computer-generated imagery and practical effects achieved the transformation.   Todd Masters (Slither (2006 film)|Slither) directed the  creation of the alien form, using thermal plastic and silicone. 

The script described Gort as nanotechnology by the time the director signed on, although it did not specify Gorts appearance. 
 
  The 15th draft of the script  had depicted the robot as a four-legged "Totem pole|Totem" that stands upright after firing its weapon beam. 
 
  Okun explained there were many more "horrific" or "amazing" concepts, but it made sense that the robot would assume a familiar human shape. He cited the   as an inspiration for Gorts texture, noting "its a simple shape, it has no emotion   it just simply is",  which makes Gort more frightening because the audience cannot tell what he is thinking. The animators estimated the computer-generated robot as   tall, whereas in the original he was played by the   tall Lock Martin.  Gorts computer model was programmed to reflect light, and the filmmakers spent time on motion-capture sessions to guide the performance. An actor wore weights on his hands and feet, allowing the animators to bring a sense of weight and power to Gort.  His destructive capabilities were based on locust swarms. 

==Music==
{{Infobox album
| Name        = The Day the Earth Stood Still: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Tyler Bates
| Cover       = 
| Released    =  
| Length      = 52:43
| Label       = Varèse Sarabande
| Producer    =
| Reviews     =
}}
 Timothy Williams. original score by Bernard Herrmann, Bates decided to try to convey the updated message of the new film, and he assumed that most people would not even realize it was a remake. Bates said, "People revere an original property and feel that its sacred, but frankly, theres a good story to be retold, as it applies to the climate of the world now. If thats something beyond the scope of a persons ability to take in, on a new level, without necessarily using the original as a criteria for whether or not theyre going to enjoy it, then they probably shouldnt bother themselves with it." The origins for the sound on the new score came from Bates attending the filming of a few scenes with Reeves and Smith. When he got back to L.A., he created a sound loop on his GuitarViol to which Derrickson responded, "I think thats the score!", when it was played for him.  Bates utilized the theremin, which Herrmann heavily used for the original films score. Bates and the theremin player he hired used the instrument in a manner reminiscent of a sound effect, especially during Klaatus surgery.  A short segment from Bachs Goldberg Variations is heard playing in the background of the Professors home when Klaatu visits the Professor which was not included in the films accompanying soundtrack release. 

{{Tracklist collapsed = yes headline  = Track listing title1    = Stars length1   = 0:39 title2    = Mountain Climber length2   = 2:41 title3    = National Security length3   = 2:47 title4    = This Is Not an Exercise length4   = 0:52 title5    = Do You Feel That? length5   = 2:00 title6    = Military Approach length6   = 1:07 title7    = G.O.R.T length7   = 2:42 title8    = Surgery length8   = 1:30 title9    = Interrogation length9   = 2:33 title10   = You Should Let Me Go length10  = 2:24 title11   = A Friend to the Earth length11  = 1:55 title12   = Fighter Drones length12  = 1:23 title13   = Came to Save the Earth length13  = 0:45 title14   = Im Staying length14  = 1:10 title15   = Helen Drives length15  = 0:44 title16   = Containing G.O.R.T length16  = 0:44 title17   = The Day the Earth Stood Still length17  = 2:41 title18   = Theyre Not Afraid of Us length18  = 1:20 title19   = Flash Chamber length19  = 0:54 title20   = Helicopter Collision length20  = 5:14 title21   = See My Son length21  = 2:11 title22   = Cemetery length22  = 3:19 title23   = Distress length23  = 1:59 title24   = Wrong Place Wrong Time length24  = 0:55 title25   = Aphid Reign length25  = 4:17 title26   = Power Down length26  = 0:56 title27   = Hes Leaving length27  = 1:50 title28   = The Beginning length28  = 1:11
}}

==Release==
Before its release, The Day the Earth Stood Still was nominated for Best Visual Effects and Best Sound at the 2008 Satellite Awards.  On the films December 12, 2008 release, the Deep Space Communications Network at Cape Canaveral was to transmit the film to Alpha Centauri. 

==Reception==

===Critical response===
 
Metacritic, a film review aggregator, gave the film a 40/100 approval rating based on 34&nbsp;reviews by top rated reviewers, placing it in the "mixed reviews" category. Based on 186&nbsp;reviews collected by Rotten Tomatoes, only 21% of them were positive. The majority found the film "heavy on special effects, but without a coherent story at its base,   is a subpar re-imagining of the 1951 science-fiction classic." 
Bruce Paterson of the Australian Film Critics Association gave the film 3 out of 5&nbsp;stars, writing that the generally poor reception for the film was "a sad fate for a surprisingly sincere tribute to Robert Wises 1951 classic."  Kenneth Turan of the Los Angeles Times congratulated Keanu Reevess performance and wrote in his review that "This contemporary remake of the science-fiction classic knew what it was doing when it cast Keanu Reeves, the movies greatest stone face since Buster Keaton." 

A. O. Scott of the New York Times was not impressed with Reeves performance, commenting that "even Klaatu looks bored and distracted, much as he did back when we knew him as Neo."  William Arnold of the Seattle Post-Intelligencer gave the film a B minus and wrote, "Its a decent enough stab at being what the old movie was to its time, following the same basic plot, full of respectful references to its model, updated with a gallery of fairly imaginative special effects."  Roger Ebert of the Chicago Sun-Times gave the film two stars and noted that the film had "taken its title so seriously that the plot stands still along with it", but also stated that it was "an expensive, good-looking film that is well-made by Scott Derrickson".  Claudia Puig of USA Today gave the film two stars and wrote in her review she felt the film was "musty and derivative" and thought its only bright spot was 10-year-old Jaden Smiths "engaging, lively performance". 

At the 2009 Razzie Awards, the film was nominated for Worst Prequel, Remake, Rip-off or Sequel, but lost the award to Indiana Jones and the Kingdom of the Crystal Skull.

===Box office===
The Day the Earth Stood Still opened in North America on December 12, 2008. During that opening weekend, and despite poor response from critics, the film reached the #1 spot, grossing $30,480,153 from 3,560&nbsp;theaters with an $8,562 average per theater.  Out of the films opening weekend income, 12% was from IMAX; it was "the highest IMAX share yet for a two-dimensional title".  In 2008, it was the 27th highest grossing film during its opening weekend but 40th for the entire year. The Day the Earth Stood Still was able to stay in the top 10 for its first four weeks in theaters.  The film ended up grossing $79,366,978 domestically and $153,726,881 in foreign markets, a total of $233,093,859.   

===Home media===
The Day the Earth Stood Still was released on DVD and Blu-ray on April 7, 2009, almost four months after its release and only five days after its theater run ended. Bonus features include commentary with Scarpa along with a  . The Blu-ray release features a D-BOX motion code. 
 Bedtime Stories, totaling $14,650,377 (not including Blu-ray). 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 