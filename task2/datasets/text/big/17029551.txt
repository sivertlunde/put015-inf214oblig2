On the Avenue
{{Infobox film
| name           = On the Avenue
| image          = OntheavenueDVD.jpg
| image_size     = 
| caption        = DVD cover
| director       = Roy Del Ruth William J. Scully (assistant) William Seiter (fill-in)
| producer       = Gene Markey Darryl F. Zanuck
| writer         = Irving Berlin (story) Eddie Cherkose Samuel Pokrass
| based on       = 
| screenplay     = William M. Conselman Gene Markey George Barbier
| music          = Charles Maxwell Cyril J. Mockridge Arthur Lange Herbert W. Spencer
| cinematography = Lucien Andriot
| editing        = Allen McNeil
| distributor    = Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 American musical George Barbier, and The Ritz Brothers. Many of the songs in this film were composed by Irving Berlin.

==Plot== George Barbier) are in the audience on opening night and they feel insulted. She goes backstage and tries to get Gary to take the skit out of the show. He refuses and calls her a "bad sport".

Shocked by the remark, Mimi decides to make a date with Gary. They spend the entire evening together and, by morning, have fallen in love. He finally agrees to revise the skit so it can no longer hurt the Carraways. Mona is in love with Gary and is furious when she hears about Garys date with Mimi. When the Carraways appear to see the revised sketch, she changes it, without Garys knowledge, making it worse than before. The Carraways decide to file suit against Gary.

To get back at him, Mimi buys the show from the producer and embarrasses Gary by hiring a paid audience to walk out on the show. Word leaks out to the press and Gary is now the laughingstock of New York. Furious, he tears up his contract, refusing to work with Mimi. Soon, Mimi becomes engaged to Arctic explorer Frederick Sims (Alan Mowbray). On her wedding day, Mona arrives and tells Mimi that it was she, not Gary, who changed the skit. She runs out on the wedding and is taken to city hall with Gary to be married.

The movies action is interspersed with songs from the play, including Berlins songs "He Aint Got Rhythm," and "Lets Go Slumming On Park Avenue."

==Cast==
* Dick Powell as Gary Blake
* Madeleine Carroll as Mimi Caraway
* Alice Faye as Mona Merrick
* The Ritz Brothers as themselves George Barbier as Commodore Caraway
* Alan Mowbray as Frederick Sims
* Cora Witherspoon as Aunt Fritz
* Walter Catlett as J.J. Dibble
* Douglas Fowley as Eddie Eads
* Joan Davis as Miss Katz
* Stepin Fetchit as Herman Step
* Sig Ruman as Herr Hanfstangel (as Sig Rumann)
* Billy Gilbert as Joe Papaloupas

==Partial sound track==
* Ive Got My Love to Keep Me Warm (1937)
** Music and Lyrics by Irving Berlin
** Sung by Dick Powell and Alice Faye in the show 

* This Years Kisses (1937)
** Music and Lyrics by Irving Berlin
** Sung by Alice Faye with piano accompaniment at rehearsal

* Youre Laughing at Me (19371927)
** Music and Lyrics by Irving Berlin
** Sung by Dick Powell with the studio orchestra

* The Girl on the Police Gazette (1937)
** Music and Lyrics by Irving Berlin
** Sung by Dick Powell with a barbershop quartet

* Cheek to Cheek (1935)
** Music and Lyrics by Irving Berlin
** Partially sung by Harry Ritz in the "He Aint Got Rhythm" number

* He Aint Got Rhythm (1937)
** Music and Lyrics by Irving Berlin
** Performed by Alice Faye, The Ritz Brothers and chorus in the show

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 