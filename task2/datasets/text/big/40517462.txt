Habit (1921 film)
{{infobox film
| title          = Habit
| image          = Habit (1921) - 2.jpg
| image_size     = 150px
| caption        = Newspaper ad
| director       = Edwin Carewe
| producer       = Louis B. Mayer Charlie Chaplin   
| writer         = Madge Tyrone (scenario)
| starring       = Mildred Harris
| based on       =  
| music          =
| cinematography = Robert Kurrle
| editing        = Edward McDermott Associated First National
| released       = January 1921
| runtime        = 6 reels
| country        = United States Silent (English intertitles)
}} Tom Barry. The film starred Mildred Harris. 

==Cast==
*Mildred Harris - Irene Fletcher
*W. E. Lawrence - John Marshall (billed as William Lawrence)
*Ethel Grey Terry - Mary Chartres
*Walter McGrail - Charles Munson
*Emmet C. King - Richard Fletcher

==References==
 

==External links==
 
*  
* 
*Stills from the production; ,   (University of Washington, Sayre Collection)
*  at IMBD.com

 
 
 
 
 
 
 
 


 