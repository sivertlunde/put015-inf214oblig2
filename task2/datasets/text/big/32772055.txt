The Grasshopper and the Ants (film)
 
{{Infobox Hollywood cartoon
| cartoon name = The Grasshopper and the Ants Silly Symphony
| image =The Grasshopper and the Ants.png
| image size =
| alt =
| caption =Hop entertains the ant colony after they take him in during the winter
| director = Wilfred Jackson
| producer = Walt Disney
| story artist = William Cottrell
| narrator =
| voice actor = Pinto Colvig
| musician = Leigh Harline
| animator = Art Babbitt Dick Huemer Albert Hurter
| layout artist =
| background artist = Walt Disney Productions
| distributor = United Artists
| release date =   (USA)
| color process = Technicolor
| runtime = 8 minutes
| country = United States
| language = English
| preceded by = The China Shop
| followed by = Funny Little Bunnies
}} Walt Disney Productions and released by United Artists. Part of the Silly Symphonies series, the film is an adaptation of The Ant and the Grasshopper, one of Aesops Fables. It was directed by Wilfred Jackson and stars Pinto Colvig as the voice of the grasshopper "Hop."

The film introduced the song "The World Owes Me a Living" (later "I Owe the World a Living") by Leigh Harline which later became associated with the character Goofy who was also voiced by Colvig. According to Leonard Maltin on the Walt Disney Treasures: Silly Symphonies DVD, this was an early example of the idea of having a character turn blue with cold, when full-spectrum Technicolor was still new at the time.

==Plot==
It all starts out when the grasshopper is dancing, playing his violin, and eating. After he sat on a mushroom, he got some water and started to eat a leaf. Then he noticed some ants working very hard to get ready for the cold winter. All he just did was laugh at those poor ants. Then he went to an ant who had his fruit cart stuck, and told him "the Good Book says The Lord provides, theres food on every tree. I see no reason to worry and work. No sir, not me! Oh, The World Owes Us a Livin. Oh, The World Owes Us a Livin. You should soil your Sunday pants, like those other foolish ants. Come on, lets play and sing and dance!". And they did, but while they were doing that, the Queen ant came and saw one of her subjects playing instead of working. Once the ant noticed the queen, he started to go back to work. Then the Queen ant got mad at the grasshopper and warned him "Youll change that tune when winter comes and the ground is covered with snow." The grasshopper ignored the queens warning, saying "Oh, wintertimes a long way off. You dance? Lets go."

Then when winter came, the grasshopper was looking for food. He found a leaf, but it blew away. He struggled and struggled, getting weaker from cold and hunger, until he looked into where the ants lived and noticed that they were having a wonderful feast and celebrating. He wanted to come in, so he knocked but he was frozen. When the ants heard the knock, they got the grasshopper and let him in. He was warmed up and saw the queen. She knew he was going to act like that, and the grasshopper tried to reason with her, "Oh madam queen, wisest of ants. Dont throw me out. Please, give me a chance". She just told him "With ants, just those who work may stay, so take your fiddle...". When he was about to leave, the queen told him to play his violin and stay. He played the violin with a different song to sing as they all lived happily ever after in the ants home, "I owe the world a living! I owe the world a living! Ive been a fool the whole year long and now Im singing a different song! You were right and I was wrong!"

==Home video release==
#   (DVD) 2001
#   (DVD) 2005
#   (DVD) 2009

==Voices==
* Pinto Colvig - Hop the Grasshopper

==References==
 

==External links==
*  
*  
*   at The Encyclopedia of Animated Disney Shorts

 
 

 
 
 
 
 
 
 
 
 
 