Bal Bramhachari
{{Infobox film
| name           = Bal Bramhachari 
| image          = 
| caption        =
| director       = Prakash Mehra, Amit Mehra and Bindu Shukla (co-director)|
| producer       = Prakash Mehra, Neera Mehra (executive producer) 
| writer         = Ravi Kapoor (screenplay), Anwar Khan (dialogue) Simran
| music          = Bappi Lahiri
| cinematography = Bashir Ali, N. Satyen
| editing        = Shyam Gupte
| distributor    = 
| released       = 6 September 1996
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = }}

Bal Bramhachari is 1996 Hindi Movie directed by Prakash Mehra and Amit Mehra and starring Puru Raajkumar in his film debut, Karishma Kapoor, Deepak Tijori, and Simran (actress)|Simran.
Other cast include Shakti Kapoor, Ashok Saraf, Mukesh Khanna, Bindu (actress)|Bindu, Tinnu Anand, Mohan Joshi, Avtar Gill, Anang Desai.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Nazren Ladgaiya"
| Ram Shankar, Kavita Krishnamurthy
|- 
| 2
| "Ram Dhun Gaon"
| Udit Narayan
|- 
| 3
| "Kabhi Na Kabhi"
| Alka Yagnik
|- 
| 4
| "Tu Hai Ladki" 
| Abhijeet Bhattacharya|Abhijeet, Sadhana Sargam
|- 
| 5
| "Hum Hai Bal"
| Vinod Rathod, Bappi Lahiri
|- 
| 6
| "Na Na Chhuna"
| Asha Bhosle
|}
== External links ==
*  

 
 
 

 