Gabi (film)
{{Infobox film name           = Gabi image          = File:Gabi_poster.jpg
| film name =    rr             = Gabi  mr             = Kabi }} director       = Jang Yoon-hyun producer       = Jung Tae-woon   Choe Jun-yeong   Lee Je-hyeon  screenplay     = Kim Eun-jung  based on       =    starring       = Kim So-yeon   Joo Jin-mo   Park Hee-soon music          = Won Il cinematography = Oh Hyun-je    editing        = Nam Na-yeong studio         = Trophy Entertainment   Ksure   Cinema Service distributor    = Cinema Service released       =   runtime        = 115 minutes country        = South Korea language       = Korean budget         =  gross          = 
}} King Gojong (1852-1919), using coffee brewed by royal barista Tanya. The plan is masterminded by Sadako, a Joseon woman with adopted Japanese nationality, and aided by Ilyich, Tanyas lover.  
 based on author Kim Tak-hwans historical fiction novel Noseoa Gabi ("Russian Coffee").  

==Plot==
After his wife  . 

Upon returning to the throne, King Gojong hires the beautiful and cosmopolitan Tanya (Kim So-yeon) as his personal barista. Tanya becomes involved in a dangerous social circle that involves not only the Russian sniper Ilyich (Joo Jin-mo) but also the mysterious socialite known as both Bae Jeong-ja and Sadako (Yoo Sun). With the Russian army hot on their trail, Tanya and her lover Ilyich eventually become ensnared in a plot to assassinate King Gojong that is orchestrated by Sadako, a Korean-Japanese collaborator. Ilyich becomes a spy to protect Tanya, who has begun to fall for Gojong while she makes his coffee every day.

With her intimate connection to the King, making a drink that could easily conceal poison, Tanya must decide if she will become a pawn in the political battlefield of late 19th century Korea.   

==Cast==
*Kim So-yeon ... Tanya 
*Joo Jin-mo ... Ilyich  King Gojong
*Yoo Sun ... Sadako
*Jo Deok-hyeon ... Seok-joo
*Jo Kyeong-hun ... underling
*Kim Hyun-ah ... court lady
*Kim Ga-eun ... Geum-hee    Kim Eung-soo Miura
*Jo Seung-yeon ... Min Young-hwan
*Jo Duk-je ... spy
*Kim Min-hyuk ... Takeda
*Hong Young-geun ... Ryosuke
*Eom Hyo-seop ... Tanyas father

==Historical basis== interpreter for King Gojong in the latters dealings with the Russian minister Karl Ivanovich Weber. Eventually his political ambition proved his undoing, and he was executed for allegedly trying to poison Gojong by spiking a cup of coffee with opium.  

==Casting change== verbal agreement,  but when she dropped out of the project ten days before filming began, production company Ocean Film sued her for breach of contract. In September 2012, the court ruled in favor of the plaintiff, ordering Lee to pay   ( ) in damages, or 40% liability. 

==Awards==
*2012 Korean Culture and Entertainment Awards: Excellence Award, Actress in a Film - Kim So-yeon
*2014 Golden Film Festival: Gold Medal in Cinematography - Oh Hyun-je

==References==
 

== External links ==
*    
*    
*   at Naver  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 