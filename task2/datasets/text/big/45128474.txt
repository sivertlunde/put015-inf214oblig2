Life (1920 film)
{{Infobox film
| name           = Life
| image          = Life (1920) - 1.jpg
| image_size     = 125px
| caption        = Newspaper ad
| director       = Travers Vale
| producer       = William A. Brady
| screenplay     = William A. Brady (story)	
| based on       =  
| starring       = Nita Naldi Hubert Druce Jack Mower J.H. Gilmour Arline Pretty Leeward Meeker
| music          =  
| cinematography = Frank Kugler
| editor         = 
| studio         = William A. Brady Picture Plays World Film
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent drama film directed by Travers Vale and written by William A. Brady based upon the play Life by Thompson Buchanan. The film stars Nita Naldi, Hubert Druce, Jack Mower, J.H. Gilmour, Arline Pretty, and Leeward Meeker. The film was released on November 13, 1920, by Paramount Pictures. 

==Plot==
 

==Cast==
*Nita Naldi	as Grace Andrews
*Hubert Druce as Tom Andrews
*Jack Mower as Bill Reid
*J.H. Gilmour as William Stuyvesant 
*Arline Pretty as Ruth Stuyvesant
*Leeward Meeker as Ralph Stuyvesant
*Rod La Rocque as Tom Burnett
*Edwin Stanley as Dennis OBrien
*Curtis Cooksey as Bull Anderson
*Geoffrey Stein as Dutch Joe Schmidt
*Effingham Pinto as Monsignor Henri 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 