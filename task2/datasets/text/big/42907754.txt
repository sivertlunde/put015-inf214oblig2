Green Signal
 
{{Infobox film
| name = Green Signal
| image = Green Signal poster.jpg
| writer = Satyadeva Sharma
| starring = Revanth Manas Gopal Sai Ashutosh Rakshita Manali Rathod Dimple Chopade Shilpi Sharma
| director = Vijay Maddhala
| cinematography = Swamy
| producer = Rudrapati Ramana Rao
| editing = 
| distributor = 
| country = India
| released = May 30, 2014
| runtime =
| language = Telugu
| music = J.B.
| awards =
| budget = 
| gross = 
}}
 Telugu film directed by Vijay Maddhala and produced by Rudrapati Ramana Rao. It features an ensemble cast of Revanth, Manas, Gopal Sai, Ashutosh, Rakshita, Manali Rathod, Dimple Chopade and Shilpi Sharma in the lead roles.  J.B. composed the music for the film.  The film released on May 30, 2014 in theaters.  The film was deemed as an unofficial remake of the Bollywood film Pyaar Ka Punchnama.   

==Cast==
* Revanth as Naidu
* Manas as Sandeep Kumar (Sandy)
* Gopal Sai as Google
* Ashutosh as Prem Rakshita as Jessie
* Manali Rathod as Sweety
* Dimple Chopade as Meera
* Shilpi Sharma as Devika
* Chammak Chandra as Leena
* Raja Sreedhar as Mahesh
* Madhuurima
* Shravya

==Critical reception==
The Times of India wrote, "Its almost as if the movie is just an excuse for the filmmaker to stitch together as many of pjs (Poor jokes) as possible. To call it a sleaze fest will be putting it kindly".  123telugu.com wrote, "Green Signal is one film which lacks in many departments. The premise chosen and subject had good scope for hilarious entertainment an comedy. But a weak narrative and mediocre direction kills this promising story".  fullhyd.com wrote, "The film has some distasteful gay humour and some terrible sexist humour. The director fails to infuse the required energy into the proceedings, and the famous 3-minute monologue from Pyaar Ka Punchnama is recreated terribly...The Maruthi brand of movies immediately needs to be shown the red light". Indiaglitz.com wrote, "Unlike in a profound film, there is no much substance in this film...apart from some good dialogues here and there, the film is not worth it if you have watched the original".  apherald.com wrote, "This could have perhaps been a good break-out film for Vijay Maddhala, only if he could have worked on some refreshing subject rather spoiling some well made Bollywood films. As it stands, Green Signal isnt one to rush to the cinema, but instead best enjoyed in parts on DVD or telecast".  cinejosh.com wrote, "Green Signal although has a promising premise and an apt environment like Happy Days, it’s the immature handling of Vijay Maddhala makes it a punishment for patrons. This film is a clear example on how one can play a spoil sport with the freedom given by producer who did not show minimum judgment in the script". 

==References==
 

==External links==
* 

 