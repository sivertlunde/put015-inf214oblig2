Roughly Speaking (film)
{{Infobox film
| name           = Roughly Speaking
| image          = Poster of the movie Roughly Speaking.jpg
| image size     = 150px
| caption        = Theatrical poster
| director       = Michael Curtiz
| producer       = Henry Blanke
| based on       =  
| writer         = Louise Randall Pierson (screenplay) Catherine Turney (uncredited)
| narrator       = 
| starring       = Rosalind Russell Jack Carson
| music          = Leo F. Forbstein Joseph Walker
| editing        = David Weisbart
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =   
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Roughly Speaking is a 1945 drama/comedy starring Rosalind Russell and Jack Carson.   The plot involves a strong-minded mother keeping her family afloat through World War I and the Great Depression. The movie was based on the autobiography of the same name, published in 1943, by Louise Randall Pierson.

==Plot==
Louise Randall Pierson (Rosalind Russell) does not have an easy life. When she is a teenager, her beloved father dies, leaving her, her mother, and her sister in financial difficulty. However, heeding her fathers advice to shoot for the stars, she remains undaunted. She goes to college and learns typing and shorthand; on her first (temporary) job, she overcomes the prejudice of her new boss, Lew Morton (Alan Hale, Sr.), against women workers.
 Donald Woods), who goes to work in the banking industry. Four children are born in rapid succession. Louise nurses her brood through a bout of infantile paralysis; one is left somewhat lame. After 10 years though, Rodney tires of her self-reliance and divorces her to marry a younger woman with a more traditional idea of what a wife should be.

A year later, Louise meets Harold C. Pierson (Jack Carson), who is less driven, but just as unconventional. After only a few hours acquaintance, he asks her to marry him, and she (somewhat to her own surprise) accepts. They have a son. Louise inspires Harold to venture into his familys business and take out a loan to build greenhouses to grow roses. They are just about to clear the last $30,000 of their debt when the market collapses due to oversupply. They have to sell off most of their possessions and take to the road. 
 stock market crashes. The family is uprooted once more.
 Army Reserve. As he eagerly rushes off to the recruitment center, Louise laments to her husband about her failure to provide their children with a stable, prosperous life. He assures her that her indomitable example, undaunted by failure after failure, is all they need, that they may be down from time to time, but will never be out. Then the two start to discuss their next project, buying a farm.

==Cast==
   
* Rosalind Russell as Louise Randall Pierson 
* Jack Carson as Harold C. Pierson  Robert Hutton as John Crane, ages 20–28 
* Jean Sullivan as Louise Jr., ages 18–26  Donald Woods as Rodney Crane 
* Alan Hale, Sr. as Lew Morton 
* Andrea King as Barbara, ages 21–29 
 
* Mona Freeman as Barbara, ages 15–20  Robert Arthur as Frankie at 17  Ray Collins as John Chase Randall 
* John Qualen as Svend Olsen 
* Kathleen Lockhart as Henrietta Louise Randall 
* Ann E. Todd as Louise Randall as a child (as Ann Todd) 
* Ann Doran as Alice Abbott
 
==Production==
Warner Bros. purchased the film rights to the autobiography, Roughly Speaking based on the life of Louise Randall Pierson, for $35,000.    Turner Classic Movies, Retrieved: October 27, 2014.  Principal photography for Roughly Speaking took place from late April to mid-July 1944. 

Rosalind Russell and Jack Carson reprised their roles in Roughly Speaking for a Lux Radio Theatre broadcast on October 8, 1945. 

==Reception==
The story connecting a long timeline of a half-century, resulting in Roughly Speaking emerging as a 150-minute feature, that after previews, was cut back to 117 minutes. The detailed plot was noticeable and although the film was generally well received by audiences, was an aspect of the film that many reviewers noted.  Bosley Crowther of The New York Times gave the film a favorable review. "The charmingly giddy life story of Louise Randall Pierson, which that lady quite frankly told with considerable gusto and good humor in "Roughly Speaking" a couple of years ago, has now been used to peg a picture which follows, roughly, the same general line ..." 
==See also==
* Frank Pierson, American screenwriter and film director who was a son of author Louise Randall Pierson

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Aylesworth, Thomas G. The Best of Warner Bros. London: Bison Books, 1986. ISBN 0-86124-268-8.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 