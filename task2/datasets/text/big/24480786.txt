La cruz (film)
{{Infobox film
| name           = La cruz
| image          = 
| image size     = 
| caption        = 
| director       = Alejandro Agresti
| producer       = Alejandro Agresti Pascual Condito
| writer         = Alejandro Agresti
| starring       = Norman Briski
| music          = 
| cinematography = Mauricio Rubinstein
| editing        = Alejandro Brodersohn
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
}}

La cruz is a 1997 Argentine drama film directed by Alejandro Agresti. It was screened in the Un Certain Regard section at the 1997 Cannes Film Festival.   

==Cast==
* Norman Briski - Alfredo
* Mirta Busnelli - Eloisa
* Carlos Roffé - Pablo
* Laura Melillo - Claudia
* Harry Havilio
* Silvana Silveri
* Sebastián Polonski
* Silvana Ramírez
* Pascual Condito
* Alejandro Agresti

==References==
 

==External links==
* 

 

 
 
 
 
 
 

 