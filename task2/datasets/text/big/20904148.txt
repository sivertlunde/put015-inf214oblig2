Mama Bhanja
{{Infobox film
| name           = Mama Bhanja
| image          = MamaBhanjafilm.png
| image_size     = 
| caption        = Promotional Poster
| director       = Naresh Kumar
| producer       = 
| writer         =
| narrator       = 
| starring       =
| music          = Rajesh Roshan
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Bollywood film directed by Naresh Kumar. 

==Cast==
* Randhir Kapoor
* Shammi Kapoor
* Parveen Babi
* Lalita Pawar
* Asha Sachdev Nadira

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Suno Rani Tumne"
| Lata Mangeshkar, Kishore Kumar
|-
| 2
| "Husn Maange Ya Koi Meri"
| Mohammed Rafi, Asha Bhosle
|}

==External links==
*  

 
 
 
 