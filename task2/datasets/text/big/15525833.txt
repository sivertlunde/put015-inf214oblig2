The Cutting Edge: The Magic of Movie Editing
{{Infobox film name           = The Cutting Edge: The Magic of Movie Editing
| image         =DVD cover of the movie The Cutting Edge- The Magic of Movie Editing.jpg director       = Wendy Apple producer       = Wendy Apple writer         = Mark Jonathan Harris starring       = Zach Staenberg Jodie Foster Michael Tronick Anthony Minghella Sean Penn Martin Scorsese Steven Spielberg Quentin Tarantino narrator       = Kathy Bates music          = Nic. tenBroek cinematography = John Bailey editing        = Daniel Loewenthal Tim Tobin released       = 2004 (USA) runtime        = 98 min. language  English
|budget         =
}} filmmaker Wendy Apple.  The film is about the art of film editing. Clips are shown from many groundbreaking films with innovative editing styles. 

==Cast==
Every actor, editor and director (listed alphabetically below) appears as themselves, with Bates as the narrator.

*Kathy Bates
*James Cameron
*Rob Cohen
*Wes Craven
*Jodie Foster
*Antony Gibbs
*Mark Goldblatt
*George Lucas
*Sally Menke
*Anthony Minghella
*Walter Murch
*Sean Penn
*Thelma Schoonmaker
*Martin Scorsese
*Steven Spielberg
*Zach Staenberg
*Quentin Tarantino
*Michael Tronick

==References==
 

==External links==
 
*  
*  
*http://movies.nytimes.com/movie/316764/The-Cutting-Edge-The-Magic-of-Movie-Editing/overview

 
 
 
 
 
 

 