Better Late Than Never (film)
{{Infobox film
| name           = Better Late Than Never
| image          = BLTN film.jpg
| image_size     =
| alt            =
| caption        = Movie Poster
| director       = Bryan Forbes
| producer       = Raymond Chow David Niven, Jr. Jack Haley, Jr.
| writer         = Bryan Forbes
| story          = Gwen Davis
| narrator       =
| starring       = David Niven Art Carney Maggie Smith Kimberley Partridge
| music          = Henry Mancini
| cinematography = Claude Lecomte
| editing        = Phillip Shaw Golden Harvest
| distributor    = Warner Bros.
| released       = 15 April 1983
| runtime        = 89 minutes
| country        = United Kingdom English
| budget         =
| gross          = $24,164
| preceded_by    =
| followed_by    =
}} 1982 film directed by Bryan Forbes.  It stars David Niven, Art Carney and Maggie Smith. The soundtrack features songs by Henry Mancini and Noël Coward. 

==Plot==
 
Nick (David Niven|Niven) is the supposed grandfather of 10-year-old Bridget (Partridge), who stands to inherit a sizeable fortune. Charley (Art Carney|Carney) shows up and claims that he is the genuine grandpa. Both men once slept with Bridgets grandmother, and she was never certain which of the two produced her child. Neither Nick nor Charley are good prospects, so Bridget must choose from the lesser of two evils.

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| David Niven || Nick Hartland
|-
| Art Carney || Charley Dunbar
|-
| Maggie Smith || Miss Anderson
|-
| Catherine Hicks || Sable
|-
| Lionel Jeffries || Bertie
|-
| Melissa Prophet || Marlene
|-
| Kimberley Partridge || Bridget
|}

==Production==
Forbes originally offered Carneys role to William Holden who declined because the fee offered was too small. Forbes wrote in 1992 that if Holden had made the film he "might have been alive today." 

==References==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 


 