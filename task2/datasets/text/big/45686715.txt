Australia Prepared
 
{{Infobox film
 | name = Australia Prepared
 | caption =
 | director = 
 | producer = 
 | writer = 
 | starring =
 | narrator = 
 | music =
 | cinematography = 
 | editing = 
 | distributor = J and N Tait  
 | studio = Australasian Films
 | released = 10 July 1916 
 | runtime = 60 minutes
 | country = Australia
 | language = silent
 | budget = 
 | preceded_by =
 | followed_by =
 | image =
}}
Australia Prepared is a 1916 Australian documentary film to show the countrys preparation for World War One.  

It was inspired by the British propaganda film Britain Prepared (1915) and was commissioned by Senator George Pearce. Filming took several months. 

Unlike many Australian silent movies, a copy of it survives.
==References==
 
==External links==
*  at Australian War Memorial
*  at Australian Screen Online

 
 
 
 


 