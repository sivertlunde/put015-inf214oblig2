The Oil Sharks
{{Infobox film
| name = The Oil Sharks
| image =
| image_size =
| caption =
| director = Rudolph Cartier   Henri Decoin 
| producer = Sam Spiegel  
| writer =  Philipp Lothar Mayring   Heinrich Oberländer   Reinhart Steinbicker   Ludwig von Wohl   Henri Decoin 
| narrator =
| starring = Arlette Marchal   Vivian Grey   Gabriel Gabrio   Peter Lorre Rudolph Schwarz  
| cinematography = Georg Bruckbauer   Eugen Schüfftan  
| editing =  Phillis Fehr   Rudolf Schaad     
| studio =  Pan-Film   Robert 
| distributor = 
| released = 14 July 1933
| runtime = 90 minutes
| country = Germany  French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Oil Sharks (French:Les requins du pétrole) is a 1933 German drama film directed by Rudolph Cartier and Henri Decoin and starring Arlette Marchal, Vivian Grey and Gabriel Gabrio.  It is the French-language version of Invisible Opponent, made with the same crew but a largely different cast and some alterations to the story line.

==Cast==
* Raoul Aslan as Delmonde  
* Raymond Cordy as Hans Mertens 
* Gabriel Gabrio as James Godfrey  
* Jean Galland as Pierre Ugron   Vivian Grey as Eve Ugron  
* Peter Lorre as Henry Pless  
* Arlette Marchal as Jeannette  
* Robert Ozanne as Santos

== References ==
 

== Bibliography ==
* Youngkin, Stephen. The Lost One: A Life of Peter Lorre. University Press of Kentucky, 2005.

== External links ==
*  

 
 
 
 
 
 
 
 

 