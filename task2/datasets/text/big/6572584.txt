The Silver Lining (1921 film)
{{Infobox film
| name           = The Silver Lining
| image          = The Silver Lining (1921) - 2.jpg
| caption        = Newspaper ad
| director       = Roland West
| producer       = Roland West
| writer         = D.J. Buchanan Charles H. Smith Roland West (story)
| starring       = Coit Albertson Jewel Carmen
| cinematography = Edward Wynard Frank Zucker
| studio         = Iroquois Films Corporation Metro Pictures Corporation
| released       =  
| country        = United States Silent (English intertitles)
}}

The Silver Lining is a  .

==Plot==
While discussing heredity, a man recalls the story of two orphan girls to a pair of cohorts at a ball. Angel (Carmen) is adopted by crooks who teach her to steal, while Evelyn (Valli) is the criminally inclined girl adopted by a wealthy family. When Angel steals a watch from a passenger on the train, the man refuses to press charges and enlists her help in his confidence scheme in Havana. Evelyn is engaged to marry the promising author Robert Ellington (Austin), but after a quarrel, the writer goes to Havana and meets and falls for Angel. Ellington is scheduled to leave on a ship but gives his ticket to Johnson (Albertson), the secret agent and con man. Angel watches tearfully as the boat pulls away before Ellington reveals he loves her, and the two are left in happiness. The film ends as the story teller turning and pointing out the couple dancing at the ball. The film is summarized in   

==Cast==
*Coit Albertson.....George Johnson
*Jewel Carmen.....The Angel
*Leslie Austin.....Robert Ellington
*Virginia Valli.....Evelyn Schofield
*Theodore Babcock.....Eugene Narcom
*Marie Coverdale.....Mrs. George Schofield
*Edwards Davis.....George Schofield
*Dorothy Dickson.....Dancer Arthur Donaldson.....Friend of the Baxters
*Paul Everton.....Detective
*J. Herbert Frank.....Big Joe	
*Julia Swayne Gordon.....Gentle Annie
*Carl Hyson.....Dancer
*Gladden James.....Billy Dean
*Jule Powers.....Mrs. Baxte
*Henry Sedley.....Mr. Baxter
*Charles Wellesley.....Burton Hardy

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 
 

 