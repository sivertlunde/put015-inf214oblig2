Odette (film)
{{Infobox film
| name           = Odette
| caption        = 
| image	=	Odette FilmPoster.jpeg
| director       = Herbert Wilcox
| producer       = Herbert Wilcox Anna Neagle
| screenplay         = Warren Chetham-Strode
| based on       =   
| narrator       = 
| starring       = Anna Neagle Trevor Howard Marius Goring Bernard Lee Peter Ustinov Anthony Collins
| cinematography = Mutz Greenbaum (credited as Max Greene)
| editing        = Bill Lewthwaite
| distributor    = British Lion Films (UK) Lopert Pictures (US)
| studio  = Wilcox-Neagle Productions
| released       =  
| runtime        = 124 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = ₤269,463 (UK) 
}} 1950 film British war film based on the true story of Special Operations Executive French-born agent Odette Sansom, who was captured by the Germans in 1943, condemned to death and sent to Ravensbrück concentration camp to be executed. However, against all odds she survived the war and testified against the prison guards at the Hamburg Ravensbrück Trials. She was awarded the George Cross in 1946; the first woman ever to receive the award, and the only woman who has been awarded it while still alive.
 French Section, FANY female SOE agent. 

The film was directed by Herbert Wilcox, and the screenplay by Warren Chetham-Strode was based on Jerrard Tickells non-fictional book Odette: The Story of a British Agent. It was jointly produced by the husband and wife team Herbert Wilcox and Anna Neagle.

Both Odette Sansom (by then Odette Churchill) and Peter Churchill served as technical advisors during the filming, and the film ends with a written message from Odette herself.

==Main cast==
* Anna Neagle as Odette Sansom
* Trevor Howard as Captain Peter Churchill
* Marius Goring as Colonel Henri
* Bernard Lee as Jack Alex Rabinovich
* Maurice Buckmaster as Himself
* Alfred Schieske as Camp Commandant Fritz Suhren
* Gilles Quéant as  Jacques
* Marianne Walla as SS Wardress
* Fritz Wendhausen as Colonel

==Reception==
The film was the fourth most popular movie at the British box office in 1950. 

==Notes==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 