Last Chants for a Slow Dance
 
{{Infobox film
| name           = Last Chants for a Slow Dance
| image          = 
| caption        = 
| director       = Jon Jost
| producer       = 
| writer         = Jon Jost
| starring       = Tom Blair
| music          = 
| cinematography = Jon Jost
| editing        = Jon Jost
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States 
| language       = English
| budget         = 
}}

Last Chants for a Slow Dance is a 1977 American drama film directed by Jon Jost and starring Tom Blair.   

==Plot==
This film follows a misanthropic working-class husband and father (Tom Blair) struggling to find work in the Midwest. As the film progresses, it seems that he has little actual interest in supporting his family as he spends his time hanging out in bars and having one-night stands. He continues to drive from town to town until, in an act of desperation, he robs and murders another man .

==Cast==
* Tom Blair
* Wayne Crouse
* Jessica St. John
* Steve Voorheis

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 