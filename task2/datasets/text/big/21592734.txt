Jau Tithe Khau
{{Infobox film
| name           = Jau Tithe Khau - Marathi Movie
| image          = Jau Tithe Khau.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Abhay Kirti
| producer       = Meghraj Shahjirao Rajebhosle
| screenplay     = 
| story          = 
| starring       = Makarand Anaspure Kuldip Pawar Chetan Dalvi Dipali Saiyed
| music          = Tyagraj Khadilkar
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Jau Tithe Khau is a Marathi movie released on 8 February 2007.  Produced by Meghraj Shahjirao Rajebhosle and directed by Abhay Kirti. The movie is based on the protagonist who is fed up of corruption everywhere and decides to teach everyone a lesson.

== Synopsis ==
Corruption is everywhere and rules the very fibre of the country. It is a problem that has and will continue to affect all. Especially for Mukunda and his friends for whom there is no respite. Despite holding various degrees, Mukunda and his friends remain unemployed. However, he doesn’t give up easily; hence he decides to teach the corrupt officials a lesson and hits upon a plan. He gets a well sanctioned from the Govt. The Well is dug and ready when one fine day, the well is stolen. All hell breaks loose as the police are shocked and rattled, the ministers and the bureaucrats are left shaken. 

Mukund (Makarand Anaspure) is hard working and intelligent young man, who is unable to find a job. He faces a lot of issues in his day-to-day life for the things which he has to depend on. Every public officer he meets like at electricity board, pension office etc. ask him for bribe to carry out the work. Enraged by this behavior and the corrupt political system he decides to fight against the system. He is helped by his girlfriend Lekha (Deepali Syed) and few friends. Based upon this help he applies for a government scheme in which the person is entitled for loan approval on getting a well dug. By bribing all the officers in the loop, he manages to get the loan approved without even digging the well. 

At the climax he files a case in the police station and court about his well getting stolen. At first he is held as a mad man, because a well, a solid structure cant be stolen. But then Mukund presents the proofs on the paper about the well being dug. The court has to accept the proof and declare that the well was indeed stolen. Mukund wins the Lawsuit.

However Mukund rejects the decision by himself and explain the people the real situation and warns them about the current state of corruption in the country. The film ends on the note that "Today a well is proven to be stolen, Dont let the country to be proved stolen" as addressed by Mukund to the people.

== Cast ==

The cast includes Makarand Anaspure, Kuldip Pawar, Chetan Dalvi, Dipali Saiyed & Others.

== Awards ==
Celebrated Silver Jubilee i.e. Ran for 25 Consecutive Weeks at Prabhat Chitra Mandir in Pune

==Soundtrack==
The music is provided by Tyagraj Khadilkar.

===Track listing===
{{Track listing
| title1 = Hirva Kancharana | length1 =
| title2 = Phad Phad Udto | length2 = 
}}

== References ==
 
 

== External links ==
*  
*  
*  

 
 