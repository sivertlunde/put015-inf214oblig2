The Seventh Sun of Love
 
{{Infobox film
| name           = The Seventh Sun of Love
| image          = 
| caption        = 
| director       = Vangelis Serdaris
| producer       = Vangelis Serdaris
| writer         = Vangelis Serdaris
| starring       = Katerina Papadaki
| music          = 
| cinematography = Yannis Varvarigos
| editing        = 
| distributor    = 
| released       =  
| runtime        = 124 minutes
| country        = Greece
| language       = Greek
| budget         = 
}}

The Seventh Sun of Love ( ) is a 2001 Greek drama film directed by Vangelis Serdaris. It was entered into the 24th Moscow International Film Festival.   

==Plot==
The story occurs during Asia Minor Campaign. Aglaia, a young woman of the Greek countryside, is placed as a servant girl in a rich household of an army officer. There, she becomes the objects of desire in the people surround her, the army officer, his wife and servant soldier of the officer. The political events of this turbulent period affect and determine the life of the protagonists.   

==Cast==
* Katerina Papadaki
* Thodoris Skourtas
* Hrysanthos Pavlou
* Elena Maria Kavoukidou
* Thalia Prokopiou
* Zlatina Todeva
* Tasia Pantazopoulou
* Haris Emmanuel
* Stefanos Gulyamdzhis
* Pavlos Visariou
* Nikos Kefalas

==Reception==
===Awards===
winner:  
*2001: Greek State Film Awards for Best Director (Vangelis Serdaris)
*2001: Greek State Film Awards for Best Screen Play (Vangelis Serdaris)
*2001: Greek State Film Awards for Best Editing 
*2001: Greek State Film Awards for Best Film (2nd place)

nominated: 
*2002: Moscow International Film Festival

==References==
 

==External links==
*  

 
 
 
 
 
 

 