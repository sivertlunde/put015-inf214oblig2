Wedding in Malinovka
{{Infobox Film
| name           = Wedding in Malinovka
| image          = Wedding in Malinovka.jpg
| image_size     = 
| caption        = 
| director       = Andrei Tutyshkin
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Lenfilm
| distributor    = 
| released       = 1967
| runtime        = 95 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} musical comedy Boris Aleksandrov adapted by Leonid Yukhvid. Produced by Semyon Malkin (Lenfilm). Runtime - 95 min.

Cinematography by Vyacheslav Fastovich. Editing by Mariya Pen. Music by Boris Aleksandrov performed by A. Fedotov (conductor) and S. Orlansky (musical director).

== Cast ==
* Vladimir Samojlov as Nazar Duma
* Lyudmila Alfimova as Sofya
* Valentina Lysenko as Yarinka
* Yevgeni Lebedev as Nechipor
* Zoya Fyodorova as Gorpina Dormidontovna
* Heliy Sysoyev as Andreika
* Mikhail Pugovkin as Yashka the Gunner
* Nikolai Slichenko as Petrya
* Andrei Abrikosov as Balyasny
* Grigori Abrikosov as Ataman Gritsian Tavrichesky
* Mikhail Vodyanoy as Popandopulo
* Tamara Nosova as Komarikha
* Emma Trejvas as Trandychikha Aleksei Smirnov as Smetana
* Margarita Krinitsyna Aleksandr Orlov
* Lyubov Tishchenko Aleksandr Zakharov Vyacheslav Voronin as Chechil
* N. Kogan
* B. Moreno
* A. Pishvanov
* Yu. Shepelev

==External links==
* 

 
 
 
 
 
 


 
 