The Karate Kid (2010 film)
 
{{Infobox film
| name           = The Karate Kid
| image          = Karate kid ver2.jpg
| caption        = Theatrical poster
| alt            =  
| director       = Harald Zwart
| producer       = Jerry Weintraub Will Smith Jada Pinkett Smith James Lassiter Ken Stovitz
| screenplay     = Christopher Murphey
| story          = Robert Mark Kamen
| starring       = Jaden Smith Jackie Chan
| music          = James Horner Roger Pratt
| editing        = Joel Negron JW Productions China Film Group
| distributor    = Columbia Pictures
| released       =  
| runtime        = 140 minutes  
| country        = United States China
| language       = English  Mandarin
| budget         = $40 million   
| gross          = $359,126,022 
}} martial arts Will and 1984 film of the same name and the fifth installment of the Karate Kid series, serving as a Reboot (fiction)|reboot.  Unlike the original 1984 version, this film featured a mixture of comedic and dramatic elements.
 kung fu master who teaches him the secrets of self-defense.

==Plot==
Twelve-year-old Dre Parker and his mother Sherry move from Detroit to Beijing after getting a job transfer at a car factory. After a day in a park, Dre develops a crush on a young violinist, Meiying, who reciprocates his attention, but a 10-year-old boy named Cheng , a rebellious kung fu prodigy whose family is close to Meiyings, attempts to keep them apart by violently attacking Dre, and later bullies him at school. During an attack, the maintenance man Mr. Han comes to Dres aid, revealing himself as a kung fu master.

After Han mends Dres injuries using fire cupping, Dre asks if Mr. Han could teach him kung fu. Han refuses, but meets Chengs teacher, Master Li, to make peace. Li, who teaches his students to show no mercy to their enemies, challenges Dre to a fight with Cheng. Han instead proposes that Dre compete against Lis students at an open kung fu tournament, and also requests the students leave Dre alone to train. Li accepts the offer, but tells Han that if Dre does not show up during the tournament he will personally bring pain to Han and Dre.

Han promises to teach Dre "real" kung fu. Han begins training Dre, by emphasing movements that apply to life in general, and that serenity and maturity, not punches and power, are the true keys to mastering the martial arts. He teaches this by having Dre go through various actions with his jacket, which teaches Dre muscle memory moves. Han then takes Dre to a temple in the Wudang Mountains where he trained with his father, and Dre witnesses a woman making a cobra reflect her movements and Han allows Dre to drink the water from a magical well.
 cut school for a day of fun, but when she is nearly late for her violin audition which was brought forward a day without their knowledge, her parents deem him a bad influence and forbid her from spending more time with him.

During the course of their training, Han gives Dre a day off. Dre goes to Han that night and finds Han, apparently drunk, smashing a car he was working on. Han tells Dre that he crashed the same car years ago. His wife and ten-year-old son were with him and died in the car crash. Han fixes the car every year but smashes it to remind himself of what happened. Dre shows Han the kung fu techniques he has learned while his mother arrives and watches him. Dre works much harder in his training to help Han forget about the incident. Han assists Dre in reading a note of apology to Meiyings father in Chinese; he accepts and promises that Meiying attend the tournament to support Dre.

At the tournament, the under-confident Dre is slow to achieve parity with his opponents, but soon begins beating them and advances to the semifinals, as does Cheng, who violently finishes off his opponents. Dre comes up against Liang, one of Lis more sympathetic students, who (under Lis goading) severely hurts Dres leg and Liang is disqualified as a result, while Dre is taken to the infirmary.

Despite Hans insistence that he has earned respect for his performance, Dre convinces Han to mend his leg by using fire cupping in order to continue. Dre returns to the arena, facing Cheng. Dre delivers impressive blows, but Cheng counters with a strike to Dres leg. Dre struggles to get up, and attempts the reflection technique to manipulate Cheng into changing his attack stance. Cheng begins reflecting Dres movements and it goads him into charging Dre, but Dre flips and catches Cheng with a kick to his head, winning the tournament along with the respect of Cheng and his classmates. Cheng, instead of the presenter, awards Dre the trophy, and the Fighting Dragon students bow to Mr. Han, accepting him as their new master, much to Lis dismay.

==Cast==
  Dre Parker (德瑞∙帕克 Déruì Pàkè)   A young boy from Detroit, Michigan who is bullied by a kung fu student, and must learn to stand up to him.
* Jackie Chan as Kesuke Miyagi#2010 remake|Mr. Han (S: 韩先生, T: 韓先生, P: Hán-xiānsheng)   The maintenance man who teaches Dre kung fu. Sherry Parker (雪莉∙帕克 Xuělì Pàkè)   Dres mother. She is very protective of Dre. Meiying (美莹 Měiyíng)   Dres crush who quickly befriends him.
*  . Retrieved on November 10, 2012. )   The main antagonist and student of Master Li. Master Li (李师傅 Lǐ-shīfu)   A Kung Fu teacher who instructs his students to be merciless towards their enemies.
* Ming Xu as Bao
* Ji Wang (王 姬) as Mrs.Po (博太太 Bó-tàitai)   The principal of Dres new school.
* Shijia Lü (吕世佳) as Liang (梁子浩 Liáng Zǐhào)   A classmate of Chengs who is instructed by Master Li to cripple Dre during the tournament.
* Yi Zhao (赵 毅) as Zhuang (秦壮壮 Qín Zhuàngzhuàng)
* Zhensu Wu (武振素) as Meiyings Father 
* Zhiheng Wang (王志恒) as Meiyings Mother 
* Cameron Hillman as Mark (马克)
* Ghye Samuel Brown as Oz (奥兹)
* Tess Liu as History teacher
* Harry Van Gorkum as Music instructor
* Bowen Sheng as himself
* Bo Zhang (张 博) as Song (宋) Harry (哈里 Hālǐ)   A boy who also befriends Dre.
* Sarah Beckley as one of the students on the excursion
* James Haobijam as one of the students

==Development== Karate Kid Los Angeles Chinatown concert crowd that he was leaving for Beijing to film the remake as Jaden Smiths teacher.   
 kung fu in China. Chan told interviewers that film cast members generally referred to the film as The Kung Fu Kid, and he believed the film would only be called The Karate Kid in America, and The Kung Fu Kid in China.  This theory held true in the Peoples Republic of China, where the film is titled The Kung Fu Dream ( ),  and in Japan  and South Korea,  where the film is titled Best Kid ( ) after the local title of the 1984 film in both countries. 

Sony had considered changing title of the film, but Jerry Weintraub, one of the producers, rejected the idea. Weintraub was also the producer of the original Karate Kid. Horn, John. "Karate Kid update breaks down some Chinese walls." Los Angeles Times. May 30, 2012.  . Retrieved on August 27, 2012. 

===Filming=== Chinese government granted the filmmakers access to the Forbidden City, the Great Wall of China, and the Wudang Mountains. On some occasions the filmmakers had to negotiate with residents who were not accustomed to filming activity. Horn, John. "Karate Kid update breaks down some Chinese walls." Los Angeles Times. May 30, 2012.  . Retrieved on August 27, 2012. 

==Music==
 
Icelandic composer Atli Örvarsson was originally hired to score the film, but he was replaced by American composer James Horner. The Karate Kid marked Horners return to scoring after his work on the 2009 film Avatar (2009 film)|Avatar.  The score was released on June 15, 2010. 
 theme song Never Say The Messengers Nasri Atweh). It is performed by Bieber and Jaden Smith. The music video was released on May 31, 2010. 
 Do You Back in Higher Ground" Rain is Poker Face", Dirty Harry" Nocturne No. 20 is featured, arranged for strings, in Meiyings violin audition scene, along with Sergei Rachmaninoffs piano transcription of "Flight of the Bumblebee" by Rimsky-Korsakov.

==Release and reception==
 
The film premiered May 26 in Chicago, Illinois|Chicago, with appearances by Jackie Chan and Jaden Smith, and a brief surprise appearance from Will Smith. 

The United Kingdom premiere was held July 15. It was attended by Chan and Smith, as well as producers Will and Jada Pinkett Smith. {{cite news
| date = July 16, 2010
| author = Sarah Bull
| title = Heavily pregnant Natalie Cassidy shows off her curves in EXTREMELY tight LBD at Karate Kid premiere
| url = http://www.dailymail.co.uk/tvshowbiz/article-1295098/The-Karate-Kid-premiere-Heavily-pregnant-Natalie-Cassidy-shows-curves-EXTREMELY-tight-LBD.html
| newspaper = The Daily Mail
| location=London
}}
 

In the Mainland China version of the film, scenes of bullying were shortened by the censors, and a kissing scene is removed. John Horn said that the editing ultimately resulted in "two slightly different movies". 

===Critical response===
 
The Karate Kid received generally mixed reviews.  Review aggregation website Rotten Tomatoes gives the film a score of 66% based on 201 reviews, and the average rating is 6.2/10. The sites consensus reads: "It may not be as powerful as the 1984 edition, but the 2010 Karate Kid delivers a surprisingly satisfying update on the original." {{cite web| url = http://www.rottentomatoes.com/m/karate_kid_2010/| title = The Karate Kid (2010) Movie Reviews, Pictures
| publisher = Flixster| work = Rotten Tomatoes| accessdate = July 24, 2010}}    Metacritic, another review aggregator, rated the film 61/100 based on 37 reviews from mainstream critics. 
 author = Ann Hornaday| url = http://www.washingtonpost.com/gog/movies/the-karate-kid,1107780/critic-review.html
| title = This old plot has new punch| newspaper = Washington Post}}  Roger Ebert of the Chicago Sun-Times gave it a positive review, rating the film three and a half out of four stars, and calling it "a lovely and well-made film that stands on its own feet".  Claudia Puig of USA Today and Owen Gleiberman of Entertainment Weekly each rated the film a B, stating "the chemistry between Jaden Smith and Jackie Chan grounds the movie, imbuing it with sincerity and poignance" and that the film is "fun and believable". {{cite news| date = June 11, 2010| author = Claudia Puig | url = http://www.usatoday.com/life/movies/reviews/2010-06-11-Karatekid11_ST_N.htm
| title="The Karate Kid" remake honors its cinematic ancestors| newspaper = USA Today}}  

Some critics took notice that the films characters are much younger than in the original film; they also noted what they believe the filmmakers unrealistic and inappropriate characterizations were. Simon Abrams of Slant Magazine gave the film one and a half stars and noted "The characters just arent old enough to be convincing in their hormone-driven need to prove themselves" and "This age gap is also a huge problem when it comes to the range that these kids bring to the project" and noted the portrayal of the child antagonist Cheng includes an "overblown and overused grimace, which looks like it might have originally belonged to Dolph Lundgren, looks especially silly on a kid that hasnt learned how to shave yet." Finally, Abrams noted "Whats most upsetting is Dres budding romance with Meiying. These kids have yet to hit puberty and already theyre swooning for each other."   

===Box office=== The A-Team, which grossed an estimated $9.6 million on the same opening day, and $26 million in its opening weekend.  It closed on September 18, 2010, after 101 days of release, grossing $176.7 million in the US and Canada along with an additional $182 million overseas for a worldwide total of $358 million, on a moderate budget of $40 million. 

===Awards and nominations===
Peoples Choice Awards 2011 
*Favorite Family Movie (Nominated)
*Favorite On-Screen Team – Jaden Smith & Jackie Chan (Nominated)
*Favorite Action Star – Jackie Chan (Won)
2011 Kids Choice Awards 
*Favorite Movie (Won)
*Favorite Buttkicker (Jackie Chan) (Won)
*Favorite Movie Actor (Jaden Smith) (Nominated)
2011 MTV Video Music Aid Japan  Never Say Never" by Justin Bieber featuring Jaden Smith) (Nominated)
2011 MTV Movie Awards 
*Biggest Badass Star (Jaden Smith) (Nominated)
32nd Young Artist Awards 
*Best Leading Young Actor in a Feature Film (Jaden Smith) (Won)
2010 Teen Choice Awards 
*Choice Summer: Movie (Nominated)

==Sequel==
It was announced in June 2010 that Sonys Columbia Pictures would be developing a sequel with Jaden Smith, Jackie Chan, and Taraji P. Henson reprising their roles as Dre, Mr Han, and Dres mother, Sherry, respectively.

It was announced in April 2014 that Breck Eisner will helm the sequel as director with Chan and Smith confirmed to return.  On June 25, 2014, Jeremiah Friedman and Nick Palmer were named as the writers to pen the films script. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 