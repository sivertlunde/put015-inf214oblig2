Palookaville (film)
{{Infobox Film
| name           = Palookaville
| image          = Palookaville.jpg
| image_size     = 
| caption        =  Alan Taylor
| producer       = Uberto Pasolini Scott Ferguson
| writer         = David Epstein William Forsythe Gareth Williams Lisa Gay Hamilton
| music          = Rachel Portman John Thomas
| editing        = David Leonard
| studio         = Redwave Films The Samuel Goldwyn Company
| distributor    = The Samuel Goldwyn Company  (USA Theatrical)  MGM Home Entertainment  (USA DVD) 
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1995 motion William Forsythe, Alan Taylor.

==Production==
 

==Plot==
 

==Partial cast== William Forsythe as Sid Dunleavy 
* Vincent Gallo as Russell Pataki  Gareth Williams as Ed the Cop 
* Lisa Gay Hamilton as Betty 
* Kim Dickens as Laurie
* Suzanne Shepherd as Mother 
* Nicole Burdette as Chris 
* Robert LuPone as Ralph
* Sam Coppola as Mr. Kott
* Frances McDormand as June 
* Douglas Seale as Old Man 
* William Riker as Old Arthur  Leonard Jackson as Bus Driver 
* William Duell as Money Truck Guard 
* Peter McRobbie as Chief of Police 
* Nesbitt Blaisdell as Old Fritz 
* Bridgit Ryan as Enid 
* Adam Trese as Jerry

==Critical reception==
It currently holds a 64% "fresh" rating on Rotten Tomatoes based on 11 reviews (7 fresh, 4 rotten) with an average rating of 6.4/10. It has a 6.4 out 10 on IMDB with 1,766 votes.

==Recognition==
===Awards and nominations===
* 1995, nominated, Thessaloniki Film Festival Golden Alexander for director Alan Taylor 
* 1997, won Tromsø International Film Festival Audience Award for Alan Taylor
* 1998, won London Critics Circle Film Awards ALFS Award for British Producer of the Year for Uberto Pasolini

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 