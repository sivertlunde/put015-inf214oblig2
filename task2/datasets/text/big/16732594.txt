Chicago Joe and the Showgirl
 
{{Infobox Film
| name = Chicago Joe and the Showgirl
| image = Chicago+Showgirl.jpg
| caption = Theatrical release poster Bernard Rose
| producer = Tim Bevan
| writer = David Yallop
| starring = Kiefer Sutherland Emily Lloyd
| music = Hans Zimmer Mike Southon
| editing = Carlos Puente LIVE Entertainment PolyGram Filmed Entertainment Working Title Films
| distributor = New Line Cinema
| released = 27 July 1990
| runtime = 103 min.
| country =   English
| budget = 
| gross = $85,395 (US)  
| followed_by = 
}}
 crime drama Bernard Rose Cleft Chin Murder.

==Plot==
In the film, Karl Hulten (Kiefer Sutherland) is an American GI who is stalking the black market of London after stealing an army truck and going AWOL. There he meets up with Betty Jones (Emily Lloyd), a stripper with a deluded fantasy world view formed by watching a steady stream of Hollywood film noir and gangster pictures. Seeing Karl, who claims he is Chicago Joe doing advance work in London for encroaching Chicago gangsters, Betty takes the opportunity to set her fantasies to life as she connives Karl into a spree of petty crimes. With luck on their side, the spree keeps escalating, until Betty urges Karl to commit the ultimate crime—murder.

==Cast==
*John Lahr ...  Radio commentator
*Emily Lloyd ...  Betty Jones
*Liz Fraser ...  Mrs. Evans
*Kiefer Sutherland ...  Karl Hulten
*Harry Fowler ...  Morry Keith Allen ...  Lenny Bexley
*Patsy Kensit ...  Joyce Cook
*Angela Morant ...  Customer
*John Surman ...  Mr. Cook
*Janet Dale ...  Mrs. Cook
*John Dair ...  John
*Stephen Hancock ...  Doctor
*Hugh Millais ...  U.S. colonel Harry Jones ...  Taxi driver
*Alexandra Pigg ...  Violet
*John Junkin ...  George Heath
*Gerard Horan ...  John Wilkins
*Colin Bruce ...  Robert De Mott
*Ralph Nossek ...  Insp. Tansill
*Roger Ashton-Griffiths ...  Insp. Tarr
*Richard Ireson ...  Reporter #1
*Malcolm Terris ...  Reporter #2 Gary Parker ...  Reporter #3
*Karen Gledhill ...  Reporter #4
*Niven Boyd ...  Reporter #5

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 