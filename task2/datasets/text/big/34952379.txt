Spirit Warriors: The Shortcut
{{Infobox film
| name           = Spirit Warriors: The Shortcut
| image          = The Shortcut.jpg
| caption        = The Collectors Edition DVD cover.
| director       = Chito S. Roño
| writer         = Chito S. Roño
| producer       = Sherida Monteverde Douglas Quijano Allan Escaño Christopher Rodriguez
| screenplay     = Roy Iglesias
| based on       =  Gloria Romero Jaime Fabregas
| cinematography = Neil Daza
| editing        = Vito Cajili
| music          = Jessie Lasaten
| studio         = MAQ Productions Roadrunner Network, Inc.
| distributor    = Regal Films
| released       =   
| runtime        = 85 minutes
| country        = Philippines
| language       = Tagalog
| budget         = Philippine Peso|PhP. 80 million (estimated)
| gross          = 
}}
 Filipino fantasy Spirit Warriors. The film was written and directed by Chito S. Roño with a screenplay by Roy Iglesias. It was distributed by Regal Films.

==Plot==
Taking from where they last left off, the Spirit Warriors stumble upon a black hole to the Netherworld in their search for an ancient mystical and powerful amulet. Now that they found a way in, comes the problem of how to get back. Especially as the Netherworlds dark denizens spring into action to give the spirit warriors a taste of their gruesome never-land hospitality.

==Cast==

===Main roles===
*Vhong Navarro as Thor
*Jhong Hilario as Buboy
*Spencer Reyes as Jigger
*Danilo Barrios as Red
*Christopher Cruz as Ponce Gloria Romero as Reds grandmother
*Jaime Fabregas as Harry

===Supporting roles===
*Roy Alvarez as Nathans stepfather
*Dexter Doria as Joshuas mother
*Connie Chua as the Babaylan
*Daniel Pasia as the church caretaker
*Marlou Aquino as the spirit messenger

==Production==
The majority of the visual effects were handled by Roadrunner Network, Inc. while the creature effects were done by Mountain Rock Productions.

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2002
| rowspan="3" align="left"| Metro Manila Film Festival   Third Best Picture
| align="center"| Spirit Warriors: The Shortcut
|  
|- Best Visual Effects
| align="center"| Roadrunner Network, Inc.
|  
|- Best Make-up Artist Warren Munar
|  
|}

==References==
 

==External links==
* 

 

 
 
 
 
 