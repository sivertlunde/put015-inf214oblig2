Tarka (film)
{{Infobox film
| name           = Tarka
| image          = Kannada film Tarka poster.jpg
| caption        = Film poster
| director       = Sunil Kumar Desai
| producer       = Sunil Kumar Desai
| writer         = 
| screenplay     = Sunil Kumar Desai
| based on       =  
| starring       = Shankar Nag Devaraj Vanitha Vasu
| music          = Guna Singh
| cinematography = P. Rajan
| editing        = A. Subramanyam
| studio         = Rachana
| distributor    = Sri Bharat Films
| released       =  
| runtime        = 132 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 thriller film The Unexpected Guest,  and starring Shankar Nag, Devaraj and Vanitha Vasu. The supporting cast features Shivaraj, Avinash, Shashidhar Bhat, Praveen and Sudhakar Pai. The film was unusual for, it had no fight and song sequences, unlike most films during the time.   
 Best Screenplay Best Sound Tamil in 1990, as Puriyaadha Pudhir.

==Plot==
The film opens to Akshay (Shankar Nag), having esaped from jail, being chased by the police when accidentally runs into the house of Sudha (Vanitha Vasu), a high school classmate of his. He hides in a closet when Inspector Kaury (Avinash) knocks the door. As he hides, he finds the corpse of her husband, Rithwik Kumar (Devaraj) dumped there. The officer breaks the news to Sudha that the plane by which her husband Kumar travelled to Calcutta crashed, killing all the passengers. She appears shocked and breaks down in front of the officer. After he leaves, she reveals to Akshay that she killed her husband and confides in him. He joins her in disposing the corpse in an abandoned well by her estate.
 flashback with Sudha narrating to Akshay the reason she killed him, shown in a series of broken sequences. Kumar, who suffers from schizophrenia is suspicious of his wifes fidelity and is unable of make love to her. He hallucinates scenes of her making love to her male friends, among who is Shashidhar, a friend who she meets often. Planning on catching them "red-handed", he plans on a fake trip to Calcutta, and returns to his house to find them talking, following the day of his departure. Mad with rage, he attacks Sudha intending to kill her, but from an accidental turn of events leading to his death, makes her believe that Shashidhar killed him. Shashidhar, in pursuit to report the matter at the police station, is hit by a police vehicle and finds himself admitted at a hospital.

Cut to the present, Akshay disguising as Harish, a friend of Kumar, meets the mourners and appears to offer support to Sudha. Sudha receives phone calls from a person who demands a ransom from her husbands property that she inherits following his death, blackmailing her that he would otherwise report the matter of her killing and disposing Kumars corpse, to the police. The blackmailer directs her to come to a spot with an amount of   2 lakh. Pursued by the police, the blackmailer, who emerges to be Kaury, and Akshay, are caught and are arrested. Following investigation at the crime scene, the investigating officer gets to know that the weapon used in murder was a knife. Using this as evidence, he reveals Akshay as the murderer to which he confesses. Akshay then narrates a story shown in a flashback sequence of his girlfriend Smitha (Vijayaranjini) being raped by Kumar and his friends, with Smitha committing suicide following the incident. Based on Kumars false evidence, Akshay gets arrested by the police who escapes from the jail a few days later, walking straight into Kumars house to intending to kill him to avenge Smithas death. Seizing the opportunity of the ensuing drama in the house between Kumar and Sudha, he slits Kumars throat unseen by Sudha. Cut, to the present, he gets arrested along with Sudha and are being brought to jail by a police vehicle. The film ends with Akshay tackling the cops out and escaping again.

==Cast==
* Shankar Nag as Akshay
* Vanitha Vasu as Sudha
* Devaraj as Ritwik Kumar
* Shivaraj
* Avinash aas Inspector Kaury
* Shashidhar Bhat
* Praveen
* Sudhakar Pai
* Kumar
* Krishna
* M. S. L. Murthy
* Nagendra
* Srikanth
* Sampath Kumar
* Vijayaranjini in a cameo appearance as Smitha
* Uday in a cameo appearance

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 