Karz (film)
 
 
 
{{Infobox film
| name           = Karz
| image          = Karz, 1980 film poster.jpg
| image_size     = 180px
| caption        = 
| director       = Subhash Ghai
| producer       = Akhtar Farooqui Jagjit Khurana
| writer         = Dr. Rahi Masoom Reza(dialogue)
| screenplay     = Sachin Bhowmick
| narrator       =  Raj Kiran Pran Pinchoo Kapoor Mac Mohan
| music          = Laxmikant-Pyarelal Anand Bakshi (Lyrics)
| cinematography = Kamalakar Rao
| editing        = Waman Bhonsle Gurudutt Shirali
| studio         = 
| distributor    = Mukta Arts Ltd.
| released       =  
| runtime        = 159 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          =   5,50,00,000 
}}
 Hindi thriller film directed by Subhash Ghai, starring Rishi Kapoor and Tina Munim as leads, also starring Simi Garewal, in the critically acclaimed role of Kamini Verma, the murderous wife from the past life, which won her a Filmfare nomination. 

Films music was by Laxmikant-Pyarelal, giving successful numbers like, Om Shanti Om and Dard-E-Dil, and who went on to win Filmfare Best Music Director Award for this film,the lyricist received two Filmfare nominations for these two hits.

== Plot ==

Ravi Verma wins a legal battle against Sir Judah, his dead fathers business partner. Shantaprasad Verma, Ravis dead father, was a rich man in Coonoor, whose property was unjustly usurped by Sir Judah after his death. Ravi gives the good news to his mother, little knowing that Judah has already set contrary plans in action. Ravi has fallen in love with Kamini, a gold-digger working covertly for Judah. Here, Ravi tells his mother that he is going to get married & coming back to get her blessings for him & Kamini. On their way to Coonoor, Kamini throws Ravi off the cliff near a small temple of Goddess Kali.Two decades later, Monty, an orphan raised by G.G.Oberoi, is a twenty-one-year-old singer fond of a tune Ravi liked, which activates some of Ravis memories subconsciously present in Monty. 

Monty soon falls in love with a girl identified later as Tina. While singing a song in his show Monty suffers a nervous breakdown, & is advised a vacation in some remote place. He chooses Ooty (near Coonoor), partly because Tina lives there. There, his inherited memories become intense when he sees all the locations of these memories. Tina tells him that she was brought up by her Rani Sahiba at behest of her uncle Kabira. In truth, Kabira was sentenced to life imprisonment and is about to be released, whereafter Monty wins his permission to court Tina, but learns that Rani Sahiba is Kamini. Kabira later reveals to Monty that Tinas father learnt some deadly secret about the Kali temple, Kamini and Ravi Verma, for which Kaminis brother killed him. In retaliation, Kabira killed Kaminis brother & blackmailed her to raise Tina with proper education, by pretending to know the secret. Monty has also learnt previously that Ravis mother & his sister were ousted unjustly from their house by Kamini & her brother. He tells the whole story to Kabira, who offers to find Ravis estranged family, with whom Monty is united. Realizing that Kamini is a puppet of Sir Judah, Monty gradually convinces that Ravis ghost seeks revenge. Slowly, a rift is caused between her & Sir Judah. Finally, in the local school opened by the Verma family, an inauguration of a hall in the memory of Ravi is planned by the school. Kamini Devi has to do the inauguration. Monty & Tina perform at the function, where they dramatise Ravis story.

Kamini is horrified to see Ravis mother & sister, and flees. When Monty confronts her, Kamini confesses to Ravis murder, which the police record; whereupon Judah captures Ravis relatives and agrees to release Tina in exchange for Kamini. Just as the exchange is about to take place, Tina attacks Kamini. In the melêe, Kabira & Monty gain an upper hand. Judah tries to burn down Montys family, but Monty saves them and kills him in a fire. Kamini escapes through the jeep. Pursued by Monty, she attacks him at the temple, but falls to her own death. In the end, Monty marries Tina.

==Cast==
* Rishi Kapoor – Monty (Ravis re-incarnation) Raj Kiran – Ravi Verma
* Simi Garewal – Kamini
* Tina Munim – Tina
* Premnath – Sir Judah Pran – Kabira (Tinas uncle)
* Durga Khote – Mrs. Shanta Prasad Verma (Ravis Mother)
* Abha Dhulia – Jyoti Verma (Ravis Sister)
* Jalal Agha – Dr. Dayal (Montys Friend)
* Pinchoo Kapoor – Mr. G.G. Oberoi
* Iftekhar – Dr. Daniel (Dr Dayals senior and Montys doctor)
* Viju Khote – Baayen (Kabiras Left)
* Birbal – Daayen (Kabiras Right)
* Mac Mohan – Sir Judahs right-hand man
* Aruna Irani – Special Appearance (in a cabaret number)

==Crew==
*Direction – Subhash Ghai
*Screenplay – Sachin Bhowmick
*Dialogue – Dr. Rahi Masoom Reza
*Production – Akhtar Farooqui, Jagjit Khurana
*Editing – Waman Bhonsle, Gurudutt Shirali
*Cinematography – Kamalakar Rao
*Direction Assistant – Ashok Ghai, Kuku Khanna
*Art Direction – Sudhendu Roy
*Costume Design – Bhanu Athaiya, Bhavana Munim, Satyawan
*Choreography – Suresh Bhatt
*Music Direction – Laxmikant-Pyarelal
*Lyrics – Anand Bakshi
*Playback – Mohammed Rafi, Asha Bhosle, Kishore Kumar, Lata Mangeshkar, Manna Dey, Anuradha Paudwal

== Soundtrack ==

{{Infobox album  
| Name        = Karz
| Type        = Soundtrack
| Artist      = Laxmikant-Pyarelal
| Cover       = Karz, 1980 film soundtrack album cover.jpg
| Released    = 1980 (India)
| Recorded    =  
| Genre       = Film soundtrack|
| Length      = 
| Label       = Sa Re Ga Ma / EMI 
| Producer    = Laxmikant-Pyarelal
| Reviews     = 
| Last album  = Be-Reham (1980) 
| This album  = Karz  (1980) 
| Next album  = Jyoti Bane Jwala  (1980)        
|}}
The soundtrack of the film includes tracks, composed by Laxmikant-Pyarelal, and with lyrics by Anand Bakshi, who received two Filmfare nominations for these two hits, Om Shanti Om and Dard-E-Dil, Laxmikant-Pyarelal however won the trophy for Best Music Director of the year. With hits like, "Ek Haseena Thi " and " Om Shanti Om " by Kishore Kumar and "Dard-E-Dil" by Mohd. Rafi, the soundtrack was a trendsetter for the disco music in the Indian music industry and films background score, especially the signature tune are still remembered as one of the most memorable ones. 
 annual list 1980, while another duet between Lata and Kishore, Tu Kitne Baras Ki reached number 13.  

{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =
| all_writing     =
| all_lyrics      = Anand Bakshi
| all_music       = Laxmikant-Pyarelal
| writing_credits =
| lyrics_credits  =
| music_credits   =
| title0          =  
| note0           =
| writer0         =
| lyrics0         =
| music0          =
| extra0          =
| length0         =
| title1          = Om Shanti Om
| note1           = Meri Umar Ke Naujawanon
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Kishore Kumar
| length1         = 9:05
| title2          = Paisa Yeh Paisa
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Kishore Kumar
| length2         = 5:10
| title3          = Ek Haseena Thi
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Kishore Kumar, Asha Bhosle
| length3         = 7:55
| title4          = Dard-E-Dil
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Mohammed Rafi
| length4         = 7:05
| title5          = Main Solah Baras Ki
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Kishore Kumar, Lata Mangeshkar
| length5         = 5:05
| title6          = Kamaal Hai
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          = Kishore Kumar, Manna Dey, Anuradha Paudwal
| length6         = 5:55
| title7          = Karz Theme Music
| note7           = Instrumental
| writer7         =
| lyrics7         =
| music7          =
| extra7          =
| length7         = 3:15
}}

==Reception==

The story of this movie about reincarnation and revenge became the ninth   admitted that film was ahead of its time, and was thus panned by critics of the times, and "flopped" at the box office, it was only years later that it started being considered a classic and even remade several times over.  
 Tamil film Chances Are (1989). 

Meanwhile, despite the Filmfare Best Music Director Award notwithstanding, films music turned out to be heavily "inspired" in the later years, like the dramatic Ek Hasina Thi was taken from  .    movietalkies.com. 

==Influence==
 Karzzzz (2008) starring Himesh Reshammiya.   
 Ek Hasina Thi (2004), Aashiq Banaya Aapne (2006) and Om Shanti Om (2007), which was seen as light-hearted tribute to the film, as it borrowed many elements from it. 

==Awards and nominations==
* 1981: Filmfare Award
**  
**  : Nominated
**  : "Om Shanti Om": Nominated
**  : "Dard-E-Dil": Nominated
**  : "Om Shanti Om": Nominated
**  :  "Dard-E-Dil": Nominated" 

== References ==

 

== External links ==

*  
*   at Bollywood Hungama
*  

 

 
 
 
 
 
 
 
 
 
 
 