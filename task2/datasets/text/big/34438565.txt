Payback (2012 film)
{{Infobox film
| name = Payback
| image =
| caption = Film Poster
| director = Jennifer Baichwal
| writer =  
| cinematography = Nicholas de Pencier
| producer = Ravida Din
| music = Martin Tielli
| released =  
| studio = National Film Board of Canada
| distributor = Zeitgeist Films Mongrel Media  (Canada) 
| country = Canada
| language = English Spanish Albanian
| runtime = 86 minutes
}}
Payback is a 2012  , which investigates the concept of debt in societies across the world (and not only on a monetary level).      

As a Canadian, Baichwal was quite familiar with Atwoods reputation:
  }}

A number of prominent intellectuals provide commentary, including economist and writer Raj Patel, ecologist William E. Rees, historian of religions Karen Armstrong, and former UN High Commissioner for Human Rights Louise Arbour.  

The film is produced by Ravida Din for the National Film Board of Canada. It premiered at the 2012 Sundance Film Festival. Payback is distributed by Zeitgeist Films, which also distributed Baichwals previous works Act of God and Manufactured Landscapes. The film is being distributed in Canada by Mongrel Media, beginning March 2012.       

==See also==
* , a 1984 National Film Board documentary about Atwood.

==References==
 

== External links ==
*  
*  
*  
* A. O. Scott|Scott, A. O.,  , New York Times review. " ries to show how debt extends far beyond the domain of money", per Times.

 
 
 
 
 
 
 
 
 
 
 
 