Døden er et kjærtegn
 
{{Infobox film
| name           = Døden er et kjærtegn
| image          = 
| image size     =
| caption        = 
| director       = Edith Carlmar
| producer       = 
| writer         = Otto Carlmar   Arne Moen (novel)
| narrator       =
| starring       = Claus Wiese   Bjørg Riiser-Larsen   Ingolf Rogde
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1949
| runtime        = 92 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama film starring Claus Wiese, Bjørg Riiser-Larsen and Ingolf Rogde. Based on a 1948 novel by Arne Moen, it was Edith Carlmars directorial début. The film depicts the passionate and tempestuous liaison between mechanic Erik (Wiese) and society woman Sonja (Riiser-Larsen). The title, in English, is "Death is a Caress". The film uses cinematic shorthand to convey time and place, while concentrating on its protagonists increasingly troubled relationship. 

==References==
 

==External links==
*  
*  

 
 
 
 


 