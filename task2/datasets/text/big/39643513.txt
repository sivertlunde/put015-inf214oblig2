Nandhi (film)
{{Infobox film
| name       = Nandhi
| image      = Nandhi Movie.jpg
| director   = D. Rajendra Babu
| producer   = B.V. Chakrapani C. Gopi M. Kumar N. Dileep Kumar M.T. Sriram
| story      = M.K. Maheshwaran
| screenplay = D. Rajendra Babu
| starring   =  
| music      = Gurukiran
| cinematography = Chandru
| editing    = T. Shashikumar
| studio     = 
| distributor = 
| released   =  
| country    = India
| language   = Kannada
| runtime    = 125 min
| budget     = 
| gross      = 
| website    =  
}}
 Kannada action-drama film directed by D. Rajendra Babu featuring Sudeep, Sindhu Menon and Radhika Chaudhari in the lead roles. The film features background score and soundtrack composed by Gurukiran and lyrics by Kaviraj and V. Nagendra Prasad. The film released on 27 December 2002   

==Cast==
* Sudeep as Nandhi
* Sindhu Menon as Divya
* Radhika Chaudhari as Pinki
* Ashish Vidhyarthi as Dhanraj/Different Dyani Ambika as Nandhis mother
* Kashi as Professor
* Ravikiran Vikas as Nandhis friend
* Vaijanath Biradar as Bagni
* Shankar Ashwath as Police Officer
* Chikka Suresh
* Narendra Babu Sharma
and others

==Review==
* viggy.com 

==Awards==
{| class="infobox" style="width: 22.7em; text-align: left; font-size: 85%; vertical-align: middle; background-color: #eef;"
|-
| colspan="3" |
{| class="collapsible collapsed" width="100%"
! colspan="3" style="background-color: #d9e8ff; text-align: center;" | Awards and nominations
|- style="background-color:#d9e8ff; text-align:center;"
!style="vertical-align: middle;" | Award
| style="background:#cceecc; font-size:8pt;" width="60px" | Wins
| style="background:#eecccc; font-size:8pt;" width="60px" | Nominations
|-
|align="center"|
;Filmfare Awards South
| 
| 
|-
|align="center"|
;Karnataka State Film Awards
| 
| 
|}
|- style="background-color:#d9e8ff"
| colspan="3" style="text-align:center;" | Totals
|-
|  
| colspan="2" width=50  
|-
|  
| colspan="2" width=50  
|}
Filmfare Awards South :- Best Actor - Kannada (2002) - Sudeep  

Karnataka State Film Awards :- Best Actor (2002) - Sudeep  Karnataka State Film Awards 2002-03:
*  
*  
*   

==References==
 

==External links==
*  

 
 
 
 
 


 
 