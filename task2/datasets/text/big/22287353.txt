Matthew's Days
 
{{Infobox film
| name           = Matthews Days
| image          = Żywot Mateusza 1968 film poster.jpg
| caption        = Film poster
| director       = Witold Leszczyński
| producer       = 
| writer         = Witold Leszczyński Wojciech Solarz Tarjei Vesaas
| starring       = Franciszek Pieczka
| music          = 
| cinematography = Andrzej Kostenko
| editing        = Zenon Piórecki
| distributor    = 
| released       = 16 February 1968
| runtime        = 80 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}
 Best Foreign Language Film at the 41st Academy Awards, but was not accepted as a nominee. 
 The Birds.

==Cast==
* Franciszek Pieczka - Mateusz
* Anna Milewska - Olga
* Wirgiliusz Gryn - Jan
* Aleksander Fogiel - Host
* Hanna Skarzanka - Hostess
* Malgorzata Braunek - Anna
* Maria Janiec - Ewa
* Elzbieta Nowacka - Girl
* Kazimierz Borowiec - Boy
* Aleksander Iwaniec - Hunter
* Joanna Szczerbic

==See also==
* List of submissions to the 41st Academy Awards for Best Foreign Language Film
* List of Polish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 