Andha Bichar
{{Infobox Film 
 | name = Andha Bichar
 | image =
 | caption = DVD Cover
 | director = Shakti Samanta
 | producer = Shakti Samanta
 | camera =
 | writer =
 | dialogue =
 | starring = Mithun Chakraborty Tanuja Biplab Chattopadhyay Sadashiv Amrapurkar Ranjeet
 | music = Rahul Dev Burman
 | lyrics = Swapan Chakraborty
 | associate director =
 | art director =
 | choreographer =
 | released = 1990
 | runtime = 125 min.
 | country  = India Bengali
 | budget =   1.2 Crores
 | preceded_by =
 | followed_by =
 }} Bengali film made in 1990. A revenge drama, with Mithun in the lead role.

==Plot==

A fast paced action thriller, with Mithun in lead.
Rakesh (Mithun Chakraborty), a small town boy, has been torn away from his family due to several crisis. His family consisted of his father(Aloknath), mother(Tanuja) and sister(Dipa Sahi). Terror strikes in the family after Aloknath has been sent to jail in fraudulent cases. He himself is on the run due to police threats. In due course of his time, Rakesh meets Lakshmi(Mandakini) and love blossoms. Even though Rakesh tries being truthful, He enters a different world. Will Rakesh unite with his long lost family and lead a happy life with the love of his life?

==Cast==

*Mithun Chakraborty
*Tanuja
*Alok Nath
*Biplab Chattopadhyay
*Mandakini
*Sadashiv Amrapurkar
*Bijoy Arora
*Dipa Sahi
*Manik Dutta
*Ranjeet
*Tarun Ghosh

==References==

http://www.gomolo.in/Movie/MovieCastcrew.aspx?mid=14702

==External links==

 
 
 


 