Never Too Late (1997 film)
{{Infobox Film
| name           = Never Too Late
| image          = 
| caption        = 
| director       = Giles Walker Tom Berry Stefan Wodoslawsky Donald Martin
| starring       = Olympia Dukakis Jean Lapointe Jan Rubes Cloris Leachman Corey Haim Matt Craven
| music          = Normand Corbeil
| cinematography = Savas Kalogeras
| editing        = 
| distributor    = MTI Home Video
| released       = 1997
| runtime        = 96 minutes
| country        = Canada
| language       = English
| budget         = 
| 
}}
 Canadian comedy-drama film, starring Olympia Dukakis, Jean Lapointe and Corey Haim. It was filmed in Montreal, Quebec.

==Plot summary==
Joseph, Rose and Olive suspect Carl, the owner of a retirement home, of misusing the funds of the homes residents. Together they set out to see that no one takes advantage of their unhealthy friend Woody and yeah.

==Cast==
* Olympia Dukakis as Rose
* Cloris Leachman as Olive
* Jan Rubes as Joseph
* Matt Craven as Carl
* Jean Lapointe as Woody
* Corey Haim as Max

==Awards==
* Paola Ridolfi was nominated for a Genie for Best Achievement in Art Direction/Production Design
* Donald Martin was nominated for a Genie for Best Original Screenplay

==References==
 
*   IMDB.com. Retrieved December 29, 2005.
*   IMDB.com. Retrieved December 29, 2005.
*   IMDB.com. Retrieved December 29, 2005.
 

==External links==
*  
*  

 
 
 
 
 
 
 


 
 