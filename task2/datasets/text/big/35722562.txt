Casa de Mujeres
{{Infobox film
| name           = Casa de Mujeres
| image          = 
| caption        = 
| director       = Julián Soler
| producer       = Mauricio de la Serna Jack Wagner
| writer         = Enrique Suarez de Deza Alberto Varela
| narrator       = 
| starring       = Dolores del Río Elsa Aguirre Fernando Soler
| music          = Manuel Esperón
| cinematography = 
| editing        = Rafael Ceballos
| distributor    = Producciones Carlos Amador
| released       =  
| runtime        = 100 minutes
| country        = Mexico Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1966 Cinema Mexican drama film directed by Julián Soler and starring Dolores del Río.  In some countries the film was named El Hijo de Todas ("The Son of All").

==Plot==
At a Christmas party, the women of a luxurious brothel, find a baby abandoned on their doorstep. After discussion, all decide to take the baby as their "mothers" and decide to reform. 

Over the years, the child becomes an adult male. The conflict comes when the child informing them of his upcoming marriage, and his "mothers" face the fear of being rejected and segregated by their own son by their previous lives as Prostitution|prostitutes.

==Details==
Casa de Mujeres was the last film in which the legendary actress Dolores del Río starred in her native country. She played the role of a madame of a brothel. Del Ríos performance in this movie shocked the public because of how she stood out among all the young actresses. The film was not a critical success, but it was very financially successful.
One critic wondered: Has Mexican cinema come a long way from Santa (film)|Santa to Casa de Mujeres?" Ramón (1997), vol. 3, p. 24-25 Dolores del Río 

==Cast==
* Dolores del Río as Gilda, "La Doña"
* Elsa Aguirre
* Susana Cabrera
* Elsa Cárdenas 
* Rosa María Vazquez
* Martha Romero
* Fernando Soler
* Carlos López Moctezuma
* Enrique Álvarez Félix as "The Son"

==References==
 

==External links==
* 

 
 
 
 
 

 
 