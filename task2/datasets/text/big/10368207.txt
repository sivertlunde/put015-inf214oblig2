Phir Wohi Dil Laya Hoon
{{Infobox film
| name           = Phir Wohi Dil Laya Hoon
| image          = Phir Wohi Dil Laya Hoon.jpg
| image_size     =
| caption        = 
| director       = Nasir Hussain
| producer       = Nasir Hussain
| writer         = 
| narrator       =  Pran
| music          = O.P. Nayyar 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  1963
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
 1963 Indian Pran played the villain in the film and Rajindernath had a supporting role. It also contains many songs that Mohammed Rafi beautifully sung.
 Tumsa Nahin Dekha (1957). Majrooh Sultanpuri was the lyricist.

The film included many superhit songs like the classic "Dekho Bijli Dole Bin Badal Ki" sung by Asha Bhosle and Usha Mangeshkar and "Mujhe Pyar Mein" sung by Asha Bhosle. It also included the rhythmic solo melodies "Lakhon Hain Nigaah Mein" and "Banda Parwar" both sung by Mohammed Rafi, which are popular even to this day.

==Cast==
*Joy Mukherjee ...  Mohan
*Asha Parekh ...  Mona
*Rajendra Nath ...  Difu Veena ...  Jamuna
*Wasti
*Krishan Dhawan ...  Mr. Kapoor
*Tabassum ...  Monas friend
*Ram Avtar ...  Kamala
*Amar
*Indira Bansal
*Rajendra Singh
*Kanchanamala
*Rani
*Ranjeet Kumar Pran ...  Ramesh

==Songs==
Phir Wohi dil laya hoon - Mohammed Rafi

Lakhon hai nigah mein - Mohammad Rafi

Aanchal mein saja lena - Mohammed Rafi

Aankhon se jo utri hai dil mein - Asha Bhosle

Aji quibla - Mohammed Rafi

Dekho Bijlee dole bin badal ki - Asha Bhosle, Usha Mangeshkar

Zulfon ki chaaon mein - Asha Bhosle, Mohammed Rafi

Humdum mere khel na jaano - Asha Bhosle, Mohammed Rafi

==References==
 

==External links==
*  
*  

 
 
 
 
 
 