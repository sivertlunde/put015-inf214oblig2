Out (1957 film)
{{Infobox Film
| name           = Out
| image          = 
| image_size     = 
| caption        = 
| director       = Lionel Rogosin (pictures)
| producer       = Thorold Dickinson
| writer         = John Hersey
| narrator       = 
| starring       = 
| music          = 
| cinematography = Gerald Gregoire
| editing        = Alexander Hammid
| distributor    = 
| released       =  
| runtime        = 25 minutes
| country        = 
| language       = 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Out was a short film produced by the United Nations Film Board and directed by Lionel Rogosin on the refugee situation in Austria as a result of Hungarian Revolution of 1956. The film was conceived 28 November 1956, filming began on 3 December 1956, and the answer print was screened 4 January 1957.
   
== External links ==
*  

 
 
 
 
 
 
 
 
 

 