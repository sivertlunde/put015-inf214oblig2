West of Hot Dog
 
{{Infobox film
| name           = West of Hot Dog
| image          = West Of Hot Dog.jpg
| caption        = DVD cover
| director       = Scott Pembroke Joe Rock
| producer       = Joe Rock
| writer         = Tay Garnett
| starring       = Stan Laurel
| music          =
| cinematography = Edgar Lyons
| editing        =
| studio         = Joe Rock Comedies
| distributor    = Film Booking Offices of America
| released       =  
| runtime        = 30 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}

West of Hot Dog is a 1924 American comedy film starring Stan Laurel.   

==Cast==
* Stan Laurel - Stan, a tenderfoot
* Julie Leonard - Little Mustard - Sheriffs Daughter (uncredited)
* Lew Meehan - (uncredited)

==See also==
* List of American films of 1924
* Stan Laurel filmography

==References==
 

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 


 