Dead Men Tell No Tales
  David MacDonald and starring Emlyn Williams, Sara Seegar and Hugh Williams.  It is based on the 1935 novel The Norwich Victims by Francis Beeding.

==Plot== French lottery, but on her way to Paris she is murdered and an imposter attempts to claim her prize.

==Cast==
* Emlyn Williams - Dr. Headlam 
* Sara Seegar - Marjorie 
* Hugh Williams - Detective Inspector Martin 
* Marius Goring - Greening 
* Lesley Brook - Elizabeth Orme 
* Christine Silver - Miss Haslett 
* Clive Morton - Frank Fielding 
* Ann Wilton - Bridget 
* Jack Vyvian - Crump 
* Marjorie Dale - The Singer 
* Hal Gordon - Sergeant

==References==
 

==Bibliography==
* Shafer, Stephen C. British popular films, 1929-1939: the cinema of reassurance. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 


 
 