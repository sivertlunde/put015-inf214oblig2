The Dam Busters (film)
 
 
{{Infobox film
| name = The Dam Busters
| image = Dam Busters 1954.jpg
| image_size = quad format film poster Michael Anderson
| producer =
| screenplay   = R. C. Sherriff
| based on       =    
| starring = Richard Todd Michael Redgrave Ursula Jeans Basil Sydney
| music = Eric Coates Leighton Lucas
| cinematography = Erwin Hillier Richard Best ABPC
| distributor = Associated British Pathé|Ass. British Pathé  
| released =  
| runtime = 124 minutes
| country = United Kingdom
| language = English
| budget =
| gross = £419,528 (UK) 
}} Michael Anderson. 617 Squadron Germany with Barnes Walliss "bouncing bomb".
 The Dam Busters (1951) by Paul Brickhill and Enemy Coast Ahead (1946) by Guy Gibson. The films reflective last minutes convey the poignant mix of emotions felt by the characters – triumph over striking a successful blow against the enemys industrial base is greatly tempered by the sobering knowledge that many died in the process of delivering it.

==Plot== Ministry of Aircraft Production, as well as doing his own job at Vickers-Armstrongs|Vickers, he works feverishly to make practical his theory of a bouncing bomb which would skip over the water to avoid protective torpedo nets. When it came into contact with the dam, it would sink before exploding, making it much more destructive. Wallis calculates that the aircraft will have to fly extremely low ( ) to enable the bombs to skip over the water correctly, but when he takes his conclusions to the Ministry, he is told that lack of production capacity means they cannot go ahead with his proposals.
 Sir Arthur Prime Minister, who authorises the project.
 Lancaster bombers, Wing Commander Guy Gibson, and tasked to fly the mission. He recruits experienced crews, especially those with low-altitude flight experience. While they train for the mission, Wallis continues his development of the bomb but has problems, such as the bomb breaking apart upon hitting the water. This requires the drop altitude to be reduced to  . With only a few weeks to go, he succeeds in fixing the problems and the mission can go ahead.

The bombers attack the dams. Eight Lancasters and their crews are lost, but the overall mission succeeds and two dams are breached.

==Cast==
  as Guy Gibson in The Dam Busters (1955)]]
  as Barnes Wallis in The Dam Busters (1955)]]
In credits order.
  Wing Commander Guy Gibson, CO of 617 Squadron and pilot of "George"
* Michael Redgrave as Barnes Wallis, Assistant Chief Designer, Aviation Section, Vickers-Armstrong Ltd
* Ursula Jeans as Mrs Molly Wallis Air Chief Marshal Sir Arthur Harris, GOC-in-C, RAF Bomber Command Robert Shaw as Guy Gibsons co-pilot. Captain Joseph "Mutt" Summers, Chief Test Pilot, Vickers-Armstrong Ltd Air Vice-Marshal Ralph Cochrane, AOC, No. 5 Group RAF Group Captain John Whitworth, Station Commander, RAF Scampton Charles Carson as Doctor David Pye, Director of Scientific Research, Air Ministry Dr William Glanville, Director of Road Research, Department of Scientific and Industrial Research
* Frederick Leister as committee member
* Eric Messiter as committee member
* Laidman Browne as committee member
* Raymond Huntley as National Physical Laboratory Official
* Hugh Manning as Ministry of Aircraft Production Official
* Edwin Styles as Observer at Trials
* Hugh Moxey as Observer at Trials
* Anthony Shaw as RAF Officer at Trials
* Laurence Naismith as Farmer
* Harold Siddons as Group Signals Officer
* Frank Phillips as BBC Announcer
* Brewster Mason as Flight Lieutenant Richard Trevor-Roper, rear gunner of "George"
* Anthony Doonan as Flight Lieutenant Robert Hutchison, wireless operator of "George" Nigel Stock as Flying Officer Frederick Spafford, bomb aimer of "George"
* Brian Nissen as Flight Lieutenant Torger Taerum, navigator of "George" Robert Shaw as Flight Sergeant John Pulford, flight engineer of "George"
* Peter Assinder as Pilot Officer Andrew Deering, front gunner of "George" Squadron Leader Melvin "Dinghy" Young, pilot of "Apple"
* Richard Thorp as Squadron Leader Henry Maudslay, pilot of "Zebra" John Fraser as Flight Lieutenant John Hopgood, pilot of "Mother" David Morrell as Flight Lieutenant Bill Astell, pilot of "Baker" Flight Lieutenant H. B. "Micky" Martin, pilot of "Popsie" George Baker as Flight Lieutenant David Maltby, pilot of "Johnny"
* Ronald Wilson as Flight Lieutenant Dave Shannon, pilot of "Leather"
* Denys Graham as Flying Officer Les Knight, pilot of "Nut"
* Basil Appleby as Flight Lieutenant Bob Hay, bomb aimer of "Popsie"
* Tim Turner as Flight Lieutenant Jack Leggo, navigator of "Popsie"
* Ewen Solon as Flight Sergeant G. E. Powell, crew chief Harold Goodwin as Gibsons Batman
* Peter Arne (uncredited) as Staff Officer to Air-Vice Marshal Cochrane
* Edward Cast (uncredited) as Crew Member
* Richard Coleman (uncredited) as RAF Officer Peter Diamond (uncredited) as Tail Gunner
* Gerald Harper (uncredited) as RAF Officer
* Arthur Howard (uncredited) as RAF Pay Clerk in NAAFI
* Lloyd Lamble (uncredited) as Collins
* Philip Latham (uncredited) as Flight Sergeant
* Patrick McGoohan (uncredited) as RAF Guard  
* Edwin Richfield (uncredited) as RAF Officer
 

==Production==
The flight sequences of the film were shot using real Avro Lancaster bombers supplied by the RAF. The aircraft, four of the final production B.VIIs, had to be taken out of storage and specially modified by removing the mid-upper gun turrets to mimic 617 Squadrons special aircraft, and cost £130 per hour to run, which amounted to a tenth of the films costs. A number of Avro Lincoln bombers were also used as "set dressing."  (An American cut was made more dramatic by depicting an aircraft flying into a hill and exploding. This version used stock footage from Warner Brothers of a Boeing B-17 Flying Fortress, not a Lancaster.)
 Ruhr valley for the film. The scene where the Dutch coast is crossed was filmed between Boston, Lincolnshire and Kings Lynn, Norfolk, and other coastal scenes near Skegness. Additional aerial footage was shot above Windermere, in the Lake District.
  in 1954]]

While RAF Scampton, where the real raid launched, was used for some scenes, the principal airfield used for ground location shooting was RAF Hemswell, a few miles north and still an operational RAF station at the time of filming. Guy Gibson had been based at Hemswell in his final posting and the airfield had been an operational Avro Lancaster base during the war. At the time filming took place it was then home to No. 109 Squadron RAF|No. 109 Squadron and No. 139 Squadron RAF, who were both operating English Electric Canberras on electronic counter measures and nuclear air sampling missions over hydrogen bomb test sites in the Pacific and Australia. However, part of the RAFs fleet of ageing Avro Lincolns had been mothballed at Hemswell prior to being broken up and several of these static aircraft appeared in background shots during filming, doubling for additional No 617 Squadron Lancasters. The station headquarters building still stands on what is now an industrial estate and is named Gibson House. The four wartime hangars also still stand, little changed in external appearance since the war.

Serving RAF pilots from both squadrons based at Hemswell took turns flying the Lancasters during filming and found the close formation and low level flying around Derwentwater and Windermere exhilarating and a welcome change from their normal high level solo Canberra sorties.

Three of the four Lancaster bombers used in the film had also appeared in the Dirk Bogarde film Appointment in London two years earlier. 

===Historical accuracy===

 

The film is accurate historically with only a few minor exceptions - theatrical adaptation -  mostly derived from Paul Brickhills book, which itself was written when much detail about the raid was not yet in the public domain:
* Barnes Wallis said that he never encountered any opposition from bureaucracy. In the film, when a reticent official asks what he can possibly say to the RAF to persuade them to lend a Vickers Wellington bomber for flight testing the bomb, Wallis suggests: "Well, if you told them that I designed it, do you think that might help?" Barnes Wallis was heavily involved with the design of the Wellington, as it used his geodesic construction method, but he was not the chief designer. 106 Squadron volunteering to follow him to his new command, only his wireless operator, Hutchinson, went with him to 617 Squadron.
* Rather than the purpose as well as the method of the raid being Wallis sole idea, the dams had already been identified as an important target by the Air Ministry before the war. spotlights altimeter" after visiting a theatre; it was suggested by Benjamin Lockspeiser of the Ministry of Aircraft Production after Gibson requested they solve the problem. It was a proven method used by RAF Coastal Command aircraft for some time. 
* The wooden "coat hanger" bomb sight intended to enable crews to release the weapon at the right distance from the target was not wholly successful; some crews used it, but others came up with their own solutions, such as pieces of string in the bomb-aimers position and/or markings on the blister.
* No bomber flew into a hillside near a target on the actual raid. This scene, which is not in the original version, was included in the copy released on the North American market (see above). Three bombers are brought down by enemy fire and two crashed due to hitting power lines in the valleys.  Mosquito fighter-bombers dropping the naval version of the bouncing bomb, code-named "Highball", intended to be used against ships. This version of the weapon was never used operationally.
* At the time the film was made, certain aspects of Upkeep were still held classified, so the actual test footage was censored to hide any details of the test bombs (a black dot was superimposed over the bomb on each frame) and the dummy bombs carried by the Lancasters were almost spherical but with flat sides rather than the true cylindrical shape.
* The dummy bomb did not show the mechanism which created the back spin.
* Ammunition being loaded into Lancaster via Righthand side door the aircraft is 50Cal M2 for Browning M2 Heavy machine gun, not the .303 calibre machine guns found on the Lancaster in 1943.
* The attack on the Eder Dam clearly shows footage of a castle resembling "Schloss Waldeck" on the wrong side of the lake and dam during the attack. The position and angle of the lake in conjunction to the castle in reality obviously proves that the precise bombing-run would have needed a downhill approach to the west of the castle to line-up the aircraft in heading, altitude, speed and distance in time before being able to release the weapon precisely on target.
* Wallis says his idea came from Horatio Nelson, 1st Viscount Nelson|Nelsons bouncing cannonballs into the side of enemy ships. (He also says Nelson sank one ship during the Battle of the Nile with a "Yorker", a cricket term for a ball that hits directly on the bottom of the stumps without bouncing.) No evidence of the truth of this is available. In a 1942 paper, Wallis does mention the bouncing of cannonballs in the 16th and 17th centuries, but Nelson is not mentioned. Murray, Iain. Bouncing-Bomb Man: The Science of Sir Barnes Wallis. Sparkford, UK: Haynes, 2009. ISBN 978-1-84425-588-7. 

===The Dam Busters March===
  football games during England matches. One version released featured dialogue extracts from the film (specifically, the bombing run scene).

==Reception==
The film was the most popular film at the British box office in 1955.  As of May 2014, The Dam Busters currently holds 100% maximum  approval rating on Rotten Tomatoes, indicating universal acclaim. At the same time, the film holds a four and a half star rating (7.2/10) on IMDb.

==Remake== Brickhill family The Hobbit.

Weta Workshop was making the models and special effects for the film and had made 10 life size Lancaster bombers.  Gibsons dog "Nigger (dog)|Nigger" will be called "Digger" in the remake 

The last living pilot of the strike team, Les Munro, joined the production crew in Masterton as technical advisor. Jackson was also to use newly declassified War Office documents to ensure the authenticity of the film. 

==Influence== Star Wars homage to the climactic sequence of The Dam Busters. In the former film, rebel pilots have to fly through a trench while evading enemy fire and fire a proton torpedo at a precise distance from the target to destroy the entire base with a single explosion; if one run fails, another run must be made by a different pilot. In addition to the similarity of the scenes, some of the dialogue is nearly identical. Star Wars also ends with an Elgarian-style march, like The Dam Busters.  The same may be said of 633 Squadron, in which a squadron of de Havilland Mosquitos must drop a bomb on a rock overhanging a key German factory at the end of a Norwegian fjord.  Pink Floyd The Wall, scenes from The Dam Busters can be seen and heard playing on a television set several times during the film. losing streak against Germany. The first showed a German guard on top of a dam catching a bouncing bomb as if he were a goalkeeper. The second showed a British tourist throwing a Union Flag towel which skipped off the water like a "bouncing bomb" to reserve a pool-side seat before the German tourists could reserve them with their towels. Both actions were followed by the comment "I bet he drinks Carling Black Label".  ITV broadcast the real name of the units mascot, a black labrador). ITV blamed regional broadcaster London Weekend Television, which in turn alleged that a junior staff member had been responsible for the unauthorised cuts. When ITV again showed a censored version in June 2001, it was condemned by the Index on Censorship as "unnecessary and ridiculous" and because the edits introduced continuity errors. 
* In 2004, the magazine Total Film named The Dam Busters the 43rd greatest British film of all time. Although it has been praised as one of the greatest war films of all time it focuses on the technicalities of destroying the enemys dams, rather than the enemy himself. The film does not attempt to gloss over the losses sustained amongst the airmen nor the devastation caused by the flooding of the enemy countryside.
* The British  . It was the version, distributed by Studio Canal, containing shots of the bomber flying into a hill. Derwent Reservoir, Spitfire and Hawker Hurricane|Hurricane. The event was attended by actor Richard Todd, representing the film crew and Les Munro, the only pilot from the original raid still living, as well as Mary Stopes-Roe, the elder daughter of Sir Barnes Wallis.

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Garbett, Mike and Brian Goulding. The Lancaster at War. Toronto: Musson Book Company, 1971. ISBN 0-7737-0005-6.
* Kaminski, Michael. The Secret History of Star Wars. Kingston, Ontario, Canada: Legacy Books Press, 2008, First edition 2007. ISBN 978-0-9784652-3-0. 
 

==External links==
*  
*  
*  
*  
*   a 1954 Flight article on the making of the film
*   a 1955 Flight review of The Dam Busters film by Bill Gunston

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 