Fragile (film)
{{Infobox Film
| name           = Fragile
| image          = Fragiles-poster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Jaume Balagueró
| producer       = Massimo Vigliar  Julio Fernández Joan Ginard
| writer         = Jaume Balagueró Jordi Galcerán
| starring       = Calista Flockhart Elena Anaya Yasmin Murphy Richard Roxburgh Colin McFarlane
| music          = Roque Baños
| cinematography = Xavi Gimenez
| editing        = Jaume Marti
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Spain United Kingdom
| language       = English
| budget         = 
| gross          =
}}
Fragile ( ) is a 2005 Spanish/UK horror film directed by Jaume Balagueró.

==Cast==
* Calista Flockhart as Amy Nicholls
* Richard Roxburgh as Robert Marcus
* Elena Anaya as Helen Perez
* Gemma Jones as Mrs./Dr. Folder
* Yasmin Murphy as Maggie
* Colin McFarlane as Roy
* Michael Pennington as Marcus
* Daniel Ortiz as Matt
* Susie Trayling as Susan
* Karmeta Cervera as Charlotte Rivers
* Ivana Baquero as Mandy

==Production==
It was filmed between Barcelona, Spain and the Isle of Wight, England. The exterior scenes of the hospital were filmed at
Bearwood College in Berkshire, England.

The Sleeping Beauty animation short seen on the projector was created specifically for this movie. 

==Release== Digital Download.  Fragile is also part of the Fangoria FrightFest 2010 on 21 June 2010. 

==Plot==
Mercy Falls, an English childrens hospital, is closing down. One of the patients, Maggie, an orphan suffering from cystic fibrosis, tells the night nurse, Susan, that she has seen "her" again. Just then, Simon, another patient, shrieks in pain as his leg is broken.

Amy Nicholls, an American nurse, arrives at the hospital to replace Susan. Amy learns that the elevator no longer goes up to the second floor, a ward that has not been used since 1959. Her colleague Helen Perez introduces her to the eight children. Maggie tells her about a ghost named Charlotte who haunts the hospital. The next day, Amy and young Simon get stuck in the elevator, which takes them up to the second floor.
 Sleeping Beauty, and Amy tells Helen and Mrs. Folder, the hospitals director, about Susans death. Amy asks Roy to show her the files of the other children whod mentioned Charlotte. All of these children are deceased. Later, Roy is killed by something. Amy decides to go up to the second floor through a ceiling door. There, she finds an old photograph of a girl in a wheelchair with a nurse, and a film-reel which shows this girl on a medical treatment for osteogenesis imperfecta. On the back of the photograph is written "Charlotte and Mandy, 1959." Using Roys projector, Amy and Dr. Robert view the film. Robert tells her the disease Charlotte suffered from was very painful, a sickness that causes the patients bones to be very susceptible to fractures. The treatment for it back then was primitive and barbaric. Doctors had built a metal frame for Charlotte to wear that was connected to tubes implanted throughout the childs body. Amy now believes Charlotte is the angry and uncontrollable ghost of the little girl. She warns Helen and Robert to get the children ready to leave and goes to find help.

Mrs. Folder tells Amy about the scandal that occurred just as she started work at the hospital. A nurse had become obsessed with Charlotte. Shed begun to purposely break the girls bones and eventually murdered the child. She then put on the childs frame and jumped into the elevator shaft, killing herself. When they arrive at the hospital, Robert shows Amy a file which proves that "Mandy" is the childs name and "Charlotte" is the obsessed nurse. Amy realizes that Charlotte wants the children.

The hospital starts falling apart because of Charlottes rage. As help arrives, Amy realizes that everything is suddenly calm, and Mrs. Folder says that Maggie is missing. Amy goes to save Maggie from Charlotte and gets badly hurt in the process. Charlotte stands there, satisfied that she has hurt them. Maggie dies in Amys arms. Amy tells Robert that shes meant to die too, since she saw Charlotte. He futilely tries to revive her; Amy is saved at the last minute after receiving a kiss from Maggies ghost. The movie ends as Amy wakes up in a different hospital. An elderly dying man sees Maggies smiling ghost by Amys bedside.

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 