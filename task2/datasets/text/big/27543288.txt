Robbery Under Arms (1985 film)
 
{{Infobox film
| name           = Robbery Under Arms
| image          = 
| caption        = Theatrical release poster
| director      = Donald Crombie Ken Hannam
| producer       = Mace Neufeld Jock Blair
| writer         = Michael Jenkins
| based on = novel by Rolf Boldrewood Steven Vidler Christopher Cummins Andy Anderson Tommy Lewis Robert Grubb
| music          = Garry McDonald Laurie Stone Ernie Clark
| studio = South Australian Film Corporation Shock
| released       = 28 March 1985 
| runtime        = 140 mins (film version)
| country        = Australia English
| budget = AU$7.3 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p71-72  
| gross = AU$226,648 (Australia)
}}
 Jurassic Park) Steven Vidler Christopher Cummins), Starlight leads his gang of wild colonial boys in search of riches, romance – and other men’s cattle.

There were two versions shot simultaneously - a feature film and a TV mini series. 

==Cast==
* Sam Neill as Captain Starlight Steven Vidler as Dick Marston Christopher Cummins as Jim Marston
* Liz Newman as Gracey
* Jane Menelaus as Aileen Andy Anderson as George
* Deborah Coulls as Kate
* Susie Lindeman as Jeannie
* Elaine Cusick as Mum
* Ed Devereaux as Ben Tommy Lewis as Warrigal
* Robert Grubb as Sir Frederick Morringer David Bradshaw as Goring John Dick as Trooper Fall
* Michael Duffield as Mr. Falkland Keith Smith as Trooper Spring

==Production==
Jock Blair first had the idea to remake the story in 1981 when he was working at the South Australian Film Corporation. It was originally envisioned that it would be a mini series but it was budgeted at a million dollars an hour which was felt to be too expensive. So it was decided to make a film as well at the same time, based on separate scripts. Nick Roddick, "Arms and the Men", Cinema Papers, May 1985 p55-58 

There were two writers and two directors. Writing the script took two years. 

The film was shot partly on location in the Flinders Rangers and at the SAFC studios in Adelaide.  The two directors collaborated well together, and Ken Hannam was relieved to work with the SAFC again after the difficulties on Sunday Too Far Away. 

Production took 20 weeks. 
==Box office==
Robbery Under Arms grossed $226,648 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
 
*  
*  at Australian Screen Online
 
 
 

 
 
 
 
 
 
 