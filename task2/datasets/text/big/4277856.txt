Just Looking
: single released Britrock band Stereophonics, see Just Looking (song)

Just Looking is an American feature film from the year 1999. It starred Ryan Merriman, was directed by Jason Alexander and received a limited theatrical release. 

A teenage boy from the Bronx is sent to Queens to live with his Aunt and new Uncle one summer in the 1950s. His summer goal is to witness an act of love. Lenny is a typical 14-year-old from the Bronx. Like every teenage boy, he is totally fascinated with the concept of sex. But the year is 1955, and Lenny is too young and too scared to actually "do it." So he dedicates his summer vacation to the next best thing. Seeing two other people do it. Easier said than done. Caught in the act of spying, his mother and stepfather ship him off to spend the summer with his aunt and uncle in "the country" Queens. His plan looks like a bust, and his summer seems destined for boredom, until he meets a whole new group of friends. Young teens who have a "sex club." They dont do it; they just talk about it, but these kids have a lot more interesting information than was available in the Bronx. And then he meets Hedy; a nurse, twice his age, and gorgeous - in a former life she modeled for bra ads. Lenny is both smitten and inspired, and his goal for the summer kicks into high gear. Lennys plan to "witness an act of love" becomes an obsession, which turns a summer vacation into an adventure that will change his life forever.

==External links==
 

 

 
 
 
 
 
 
 


 