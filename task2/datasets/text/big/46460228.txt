The Mystery of a Hansom Cab (1915 film)
{{Infobox film
| name           = The Mystery of a Hansom Cab 
| image          =
| caption        = Harold Weston 
| producer       = 
| writer         = Fergus Hume  (novel)   Eliot Stannard 
| starring       = Milton Rosmer   Fay Temple   A.V. Bramble 
| cinematography = 
| editing        = 
| studio         = British & Colonial Kinematograph Company 
| distributor    = Ideal Films
| released       = September 1915 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} Harold Weston novel of the same name.

==Cast==
*    Milton Rosmer as Mark Frettleby  
* Fay Temple as Madge Frettleby  
* A.V. Bramble as Moreland  
* Arthur Walcott as Oliver White  
* James L. Dale as Brian Fitzgerald  

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1914-1918. Routledge, 2005.

==External links==
* 

 
 
 
 
 
 
 

 