Fluffy (1965 film)
{{Infobox film
| name           = Fluffy (1965)
| image          = 
| alt            = 
| caption        = Theatrical release poster
| director       = Earl Bellamy
| producer       = Scarus, Universal Pictures
| writer         = Samuel Roeca
| starring       = Tony Randall Shirley Jones Howard Morris Edward Andrews Ernest Truex Jim Backus Frank Faylen Celia Milius
| music          = Irving Gertz
| cinematography = Clifford Stine
| editing        = Russell F. Schoengarth
| studio         = Kenya
| distributor    = Universal Pictures National Broadcasting Company (NBC)
| released       =  
| runtime        = 92 Minutes 
| country        = United States
| language       = English French
| budget         =
| gross          =    
}} comedy film written by Samuel Roeca, directed by Earl Bellamy, and featuring Tony Randall. The storyline concerns a scientists effort to prove that a wild animal (in this case, a lion named Fluffy) can be made into a pet with proper training.

==Cast==
* Tony Randall as Prof. Daniel Potter
* Shirley Jones as Janice Claridge
* Howard Morris as Sweeney
* Edward Andrews as Griswald
* Ernest Truex as Claridge
* Jim Backus as Sergeant
* Frank Faylen as Catfish
* Celia Milius as Sally Brighton (Celia Kaye)
* Dick Sargent as Tommy
* Adam Roarke as Bob Brighton
* Whit Bissell as Dr. Braden
* Harriet E. MacGibbon as Mrs. Claridge
* Jim Boles as Pete
* Parley Baer as Police Captain
* Connie Gilchrist as Mad
   

==Production==
The film was produced by Scarus and Universal Pictures.

==Commercial performance==

===Box office===

== See also ==
 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 