Jackie (2010 film)
 
 
{{Infobox film
| name           = Jackie
| image          = Jackie_banner1.JPG
| caption        = Theatrical release poster
| director       = Duniya Soori
| producer       = Parvathamma Rajkumar 
| writer         = Duniya Soori
| starring       =  
| music          = V. Harikrishna
| cinematography = Satya Hegde
| editing        = Deepu S. Kumar
| studio         = Poornima Enterprises
| distributor    =
| released       =   }}
| runtime        = 142 minutes
| country        = India Telugu Malayalam
| box office = 45 crores + 4 crores satellite and audio rights
| budget     =7 crores 
}} Soori and Kannada on Best Film South Filmfare Telugu version Malayalam version release date is yet to be announced. Shivkamal, Aravind G. (2011-01-25)  . Southscope.in. Retrieved on 2014-03-08.  
 platinum disc, is also available as a 5.1 Music Disc|5.1 audio DVD disc.  . Chitraloka.com. Retrieved on 2014-03-08.  After successful screening in Australia, United Kingdom and Germany, Jackie was released in the USA and Singapore in February 2011.  . Nowrunning.com (2010-10-15). Retrieved on 2014-03-08. 

==Plot== human trafficker and asks Jackie to help her get married. Jackie initially tries to help but when the priest questions him, he decides to cop out of the issue respecting the aged fathers feelings.

Losing all hope, Yashodha elopes with her lover, along with a blind girl(fiction). The priest is under the impression that Jackie is the mediator in this case. So it becomes responsibility of Jackie to trace Yashodha and the blind girl. Meanwhile, Jackie is followed by police as he has sheltered a friend who has fled from the prison and a cop is killed during the escapade. In the dense forest, he averts a ritual of human sacrifice where Lakshmi (Bhavana Menon) is the intended victim. The trouble shoots up for Jackie again as even the wrong-doers are chasing him now. As he lands up in Bangalore|Bengaluru, he finds out that the blind girl who went with Yashodha is dead. He gets a clue of Prasanna alias Jooli when he becomes friendly to constable Bheemanna (Rangayana Raghu) which leads to suspicious activities of a gang transporting girls to abroad in oil tankers, headed by Mithai Rama (Ravi Kale). Jackie is on the hunt for the dangerous team and as the police from other part also follow Jackie, it is where the cinema finds a happy-ending climax.

==Production== Bhavana in Kannada language films. 

Director Soori, who is known for maintaining his script and characters a secret, had earlier revealed that the film is about how life takes a turn for Jackie and the way he faces the challenges and added that the film will be different from Puneet’s earlier films. Soori, however, was initially "scared" to be directing a Puneet Rajkumar flick just because of the enormous expectations from the audience.  SnorriCam has been used extensively in the films song and stunt sequences giving it a point-of-view feel.
 Telugu and Malayalam languages for a 2011 release. 

==Cast==
* Puneet Rajkumar as Janakirama alias Jackie
* Bhavana Menon as Lakshmi
* Harshika Poonacha as Yashodha
* Rangayana Raghu as Meese Bheemanna
* Raju Thalikote as a matrimonial agent
* Shobharaj as Police Superintendent
* Ravi Kale as Mithai Rama
* Sumithramma as Jackies mother – Jayamma
* Petrol Prasanna as Prasanna alias Jooli
* Bullet Prakash
* M. S. Umesh
* Honnavalli Krishna as a chicken seller (guest role)

Director Soori, Kannada lyricist Yograj Bhat and music director V. Harikrishna appear on-screen during the title track of the film. 

==Release==

===Kannada=== multiplex in Hyderabad, India|Hyderabad. The film, which was initially slated for a late September release, was later postponed to October owing to pending work on digital grading.  

====International release====
Jackie was released overseas by Bevin Exports in the countries of Australia, UK, Singapore, Germany, US, Dubai and New Zealand among others.  The same firm had earlier released other Kannada blockbusters like Mungaaru Male and Aramane worldwide. The global distribution will be on a 50:50 sharing between Poornima Enterprises and Bevin Exports. 
 Perth on Bay Area. New Jersey, Dallas and Houston, Texas|Houston. In Singapore, it was screened on 6 March 2011. 

===Telugu===
The Telugu version of Jackie is produced by Nadella Sujatha through Suraj Films studio and was released in the state of Andhra Pradesh on 6 May 2011. 

==Reception== Times of DNA India review. 

==Awards==

* Best Film of 2010 – Suvarna Awards 2011  . Cinecurry.com (2011-06-30). Retrieved on 2014-03-08. 
* Best Actor (Puneet Rajkumar) – Suvarna Awards 2011 

==Soundtrack==

===Kannada Soundtrack===
{{Infobox album  
| Name        = Jackie
| Type        = Soundtrack
| Artist      = V. Harikrishna
| Cover       = Jackiediff.jpg
| Alt         = Jackie audio release poster
| Released    =  
| Recorded    = Feature film soundtrack
| Length      =
| Label       = Anand Audio
}}

Jackie Kannada soundtrack was released in the month of August, 2010. In a first of its kind, audio was simultaneously released over Bluetooth by a Bangalore|Bengaluru-based company called TELiBrahma.   TELiBrahma developed an exclusive mobile application for the film and distributed it across about 100 of its proprietary technology called BluFi locations in Bengaluru. Considered a success, this campaign resulted in nearly 55,000 downloads in 16 days. 
 platinum disc celebration on 27 December 2010. 

The music was composed by V. Harikrishna and lyrics were penned by Yograj Bhat.

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
! Track # !! Song !! Singer(s)
|- Tippu 
|-
| 2 || Eradu Jadeyannu || Sonu Nigam, Shreya Ghoshal 
|-
| 3 || Ekka Raja Rani || Kailash Kher
|-
| 4 || Edavatt Aytu || Puneet Rajkumar, Priya Himesh
|-
| 5 || Jackie Jackie || Naveen Madhav
|}

Leaked Song
 The soundtrack of the film created quite a stir before the audio release when the song "Ekka Raja Rani" was leaked on YouTube.  This song, though unfinished, became quite popular with listeners. 

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
! Track # !! Song !! Singer !! Lyricist
|- Tippu || Yograj Bhat
|}

===Telugu soundtrack=== Hyderabad with Telugu film personalities Allu Arjun and Akkineni Nageswara Rao as chief guests.  Bhuvana Chandra, Vennelakanti and Vanamali have composed the Telugu lyrics.  The movie completed 25 days in 85 Theaters 50 days in 35 Theaters and 75 days in 10 Theaters.From huge success of the movie "Puneeth Rajkumar" became more popular in Telagu Industry and He has more number of fans association and huge fan followers in entire "Andra pradesh"."Puneeth Rajkumar" called By the Nickname "Jackie" in Andra as "Appu" in Karnataka and "Anna Bond" & "Jackie" in Kerla.

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
! Track # !! Song !! Singer(s) !! Lyricist
|- Tippu || Bhuvana Chandra
|-
| 2 || Jadalo Virajajey || Rakhi, Harini || Vennelakanti
|-
| 3 || Aasu Raju Rani || Murali || Vennelakanti
|-
| 4 || Kissu Missu ||Tippu, Veena Ghantasala || Vennelakanti
|- Tippu || Vanamali
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 