Let the Bullets Fly
{{Infobox film
| name           = Let the Bullets Fly
| image          = Let the Bullets Fly.jpg
| caption        = Theatrical release poster
| director       = Jiang Wen
| producer       = {{Plainlist|
* Albert Yeung
* Ping Dong
* Yin Homber}}
| screenplay     = Jiang Wen
| story          = Shitu Ma
| starring       = {{Plainlist|
* Jiang Wen
* Ge You
* Carina Lau
* Chow Yun-fat
* Jiang Wu
* Liao Fan
* Chen Kun Zhou Yun
* Zhang Mo}}
| music          = {{Plainlist|
* Joe Hisaishi
* Shu Nan}}
| cinematography = Zhao Fei
| editing        =  
| studio         = China Film Group
| distributor    = Emperor Motion Pictures
| released       =  
| runtime        = 132 minutes
| country        = {{Plainlist|
* China
* Hong Kong }} Sichuanese
| budget         = 
| gross          =   (China)      (worldwide)   
}} Zhou Yun.

The films script went through over thirty drafts before Jiang Wen was happy with it. Let the Bullets Fly was originally to be released in September 2010 but was pushed back to December. Made in Mandarin and Sichuanese, the film broke several box office records in China, and has received critical acclaim, when it was released. Let the Bullets Fly grossed 674 million   in 2012 http://ent.qq.com/a/20110216/000002.htm    ) and $140 million worldwide. 

==Plot== China during warring 1920s,"Pock Mark" Zhang Mazi (Jiang Wen) leads a group of bandits (each of whom is numbered rather than named). The gang ambush a government train carrying Ma Bangde (Ge You), who is on his way to Goose Town to assume the position of governor. Mas train is derailed, killing both his bodyguards and his counselor, Tang (Feng Xiaogang). Ma has no money, having spent it all to buy his position; to avoid being killed by Zhangs bandits he lies to them, claiming that he is actually Counselor Tang and that his wife (Carina Lau) was the dead governors wife. He tells the bandits that, if they spare him and his wife, he will help Zhang to impersonate Ma and pilfer Goose Towns finances.

At Goose Town, Zhangs appointment is opposed by local mobster Master Huang (Chow Yun Fat), who lives in a fortified citadel. Huang greets the governors party by sending his best hat in a palanquin instead of himself. Ma tells Zhang that previous governors would split taxes levied from the town residents with Huang. However, Zhang is not interested in taking money from the poor.

Wu (Jiang Wu), one of Huangs subordinates, severely injures a citizen, and as governor Zhang rules against Wu in the town court. In retaliation, Huang frames Zhangs godson, Six, for theft. Six kills himself to prove his innocence. Zhang vows to destroy Huang, but Ma advises him to use cunning rather than brute force. Huang invites Zhang to a meal at his citadel, and there Huang pretends to have his subordinates killed as a sign of good faith. Not realizing the governor is actually the bandit chief, Huang raises a plan to hunt down and kill Zhang Mazi. Zhang pretends to agree to this plan, so long as Huang finances the expedition.

That night, Huang disguises his subordinates as Mazis bandits and sends them to assassinate Zhang while he is asleep. However, only Mas wife is killed. In grief, Ma reveals his true identity as governor to Zhang. During the funeral for Mas wife, Zhang has his bandits kidnap Huang and the heads of Goose Towns two leading families for ransom. They quickly discover they have captured Huangs look-alike. The town raises the ransom money but Zhang refuses to take it, instead returning it to the townsfolk. As they do so, Flora, a young prostitute in Huangs custody, discovers their identity. She is captured but becomes friendly with Master Two and Master Three and later stays on as a bandit, helping them to guard Huangs look-alike. Huang sends his own subordinates, also disguised as bandits, to retrieve the money handed back to the town. A woman comes to find Ma, saying that he seduced her while in Shanxi and he is the father of her son. As compensation, Ma gives them two jewels that Huang had given Zhang.

Huang devises a new plan to kill Zhang Mazi (still unaware that he is not the true governor) by sending out his subordinates to pose as masked bandits. This plan fails, and his men are killed in a shoot-out. Huang has no choice but to supply the money for Zhangs anti-bandit expedition. In the meantime, Huangs steward obtains a portrait of the real Governor Ma. The real Ma tells Huang that Zhang is his nephew and allowed to play governor because he saved Ma from bandits. During the expedition, Huang employs a fake Zhang Mazi to kill Zhang during the hunt, doubling his chances by sending his men with a landmine. Three days later, during the battle with Huangs fake bandits, Master Two is killed. After the fake Zhang Mazi is captured, he offers up two jewels as a way to avoid death. It transpires he killed the woman and Mas son after Ma gave them the jewels. A distraught Ma tries to leave for Shanxi, but drives his carriage over the landmine and is killed.

Zhang vows revenge and returns to Goose Town for a showdown with Huang. He scatters money to the townsfolk and Huang gathers it up the next day; then Zhang scatters firearms to the townsfolk and prevents Huang from gathering them. Zhang and his bandits put on a show of attacking the citadel, then beheads Huangs look-alike to convince the townsfolk that Huang is dead and the one in the citadel is the look-alike. The townsfolk are reassured and storm the citadel with their new weapons. Zhang gives Huang a gun with one bullet left for his own suicide. However, a moment later, Huang stands on top of his own citadel and fires the gun into the air to get Zhangs attention. He throws a hat better than the one he originally sent to greet Zhang off the roof, as he promised. He then walks back into the citadel, killing himself with his own landmine.

Master Three intends to marry Flora and the surviving bandits leave for Shanghai to lead a more peaceful life. They take the train through the mountains, Zhang riding after them.

==Production==
Director Jiang Wen went over 30 drafts of the films script.  Jiang Wu stated he was offered the role in the film through a text message from his brother, director Jiang Wen. Wu said that his brother "never picked any talentless person for his productions. So it is good to be chosen for work in a good team." 

Parts of the filming were done in Taishan, China.  

==Release==
Let the Bullets Fly was originally scheduled for a release in September 2010.  This release date was nullified as a spokesperson for Emperor Motion Pictures stated that "There is a lot of post-production to be done and it has to be done properly."  The film premiered in Beijing on December 6, 2010 with wide-release in Mainland China on December 16.   Let the Bullets Fly was released in Hong Kong on January 13, 2011.     The film has become the highest grossing Chinese film, beating the record set by  Aftershock (2010 film)|Aftershock.   Following Avatar (2009 film)|Avatar, this film is now the second highest grossing film ever released in China. 

Let the Bullets Fly had its American premiere at the Tribeca Film Festival in 2011. The festivals co-founder, Martin Scorsese, had a private screening of the film in August 2010 during post-production when he was visiting Beijing with his family. 

===Box office===
Let the Bullets Fly s opening midnight screenings have grossed at least one million yuan ($150,000), making the film gain a new midnight opening record for Chinese-language films.    The films opening day gross was $4.5 million (RMB30m), which did not break the opening day record set by Feng Xiaogangs Aftershock (2010 film)|Aftershock. By the weekend, the films accumulated grossed reached $19.52 million (RMB130.18m) and it became the fastest local film to break the RMB100m mark.  Let the Bullets Fly earned a total of 400 million yuan (60 million dollars) in its first 11 days of release. 

===Critical reception===
In China, Let the Bullets Fly has won acclaim for story and dialogue as well as attracting criticism for its violence.  John Anderson of Variety (magazine)|Variety describes the film as "an entertaining hot pot of wry political commentary and general mischief" and that "genre fans in particular will find much to revel in, with Jiang being a helmer of sharp commercial instincts and a sage satirical bent."  Anderson further praised the films visual style and composition, states "While a generous portion of Let the Bullets Fly is dedicated to computerized chaos, explosions, and mayhem, the subtle is always in competition with the ostentatious," and Anderson especially points out one lengthy scene involving a conversation between the three main characters "d.p. Zhao Feis camera virtually floats around them, rotating, making mute commentary and suggesting the camerawork in Hou Hsiao Hsiens Flowers of Shanghai. Its captivating."  Maggie Lee of The Hollywood Reporter described the film as "unabashedly entertaining" and though less tailored to film festivals than Wens other works, the bottom line is that it is a "rollicking Chinese western directed with cinematic gumption." 
 Time Out Hong Kong called the acting "masterclass throughout" while noting that it may take a "native Chinese to fully appreciate."   The Global Times gave the film a seven out of ten rating, calling it "a nicely tense pace and with a compact storyline featuring enough genuine laughs" while stating that the role of Jiang Wen "quickly becomes excessive and over-the-top".  The Beijing Review praised the film, saying it had "a great deal more depth to it than the average Hong Kong shoot-em-up" and that it was as "captivating to listen to as it is to watch".   China Daily placed the film on their list of the best ten Chinese films of 2010.  Twitch Film praised the films tone and the script, stating "What is most refreshing about this tried and tested formula is Jiangs decision to play his film for laughs, and the script is littered with pitch-black humour throughout." 

==Awards and nominations==
 s]]
Let the Bullets Fly has received numerous awards and nominations, including Best Film and Directing nominations from the Asian Film Awards and the Asia Pacific Screen Awards.  Jiang also received the Best Director award from the Hong Kong Film Critics Society.
{| class="wikitable" style="font-size:95%;" ;
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Ceremony
! Category
! Name
! Outcome
|-
|rowspan=7|5th Asian Film Awards  
|- Best Film
|
| 
|- Best Director Jiang Wen
| 
|- Best Actor Chow Yun-fat
| 
|- Best Supporting Actress Carina Lau Kar-ling
| 
|- Best Screenplay Jiang Wen
| 
|- Best Costume Design William Chang Suk-pin
| 
|-
|rowspan=3|5th Asia Pacific Screen Awards 
|- Best Feature Film
|
| 
|- Achievement in Directing Jiang Wen
| 
|-
|rowspan=10|2011 Golden Horse Film Festival and Awards 
|- Best Film
|
| 
|- Best Director  Jiang Wen
| 
|- Best Leading Actor   Ge You
| 
|- Best Supporting Actress   Carina Lau
| 
|- Best Adapted Screenplay   Wei Xiao, Li Bukong, Zhu Sujin, Shu Ping, Jiang Wen, Guo Junli
| 
|- Best Cinematography   Zhao Fei
| 
|- Best Visual Effects  Eman Tse, Victor Wong
| 
|- Best Makeup & Costume Design   William Chang
| 
|- Best Sound Effects   Wen Bo, Wang Gang
| 
|-
|rowspan=14|31st Hong Kong Film Awards  
|- Best Film   Ma Ke, Albert Lee, Yin Homber, Barbie Tung, Zhao Haicheng
| 
|- Best Director   Jiang Wen
| 
|- Best Screenplay   Zhu Sujin, Shu Ping, Jiang Wen, Guo Junli, Wei Xiao, Li Bukong
| 
|- Best Actor   Jiang Wen
| 
|- Best Actor   Ge You
| 
|- Best Supporting Actress  Carina Lau
| 
|- Best Cinematography  Zhao Fei
| 
|- Best Film Editing Jiang Wen, Cao Wei Jie
| 
|- Best Art Direction Eddy Wong, Yu Qing Hua & Gao Yi Guang
| 
|- Best Costume & Make Up Design William Chang Suk Ping
| 
|- Best Action Choreography  Sit Chun Wai, Lee Chung Chi
| 
|- Best Sound Design Wen Bo, Wang Gang
| 
|- Best Visual Effects Victor Wong, Xie Yi Wen
| 
|-
|rowspan=2|18th Hong Kong Film Critics Society Award 
|- Best Director Jiang Wen
| 
|-
|}

==References==
 

==External links==
*  
*  
*  
*   at the Hong Kong Cinemagic

 

 
 
 
 
 
 
 
 
 
 
 
 
 