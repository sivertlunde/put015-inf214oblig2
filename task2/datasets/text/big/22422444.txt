Madhouse (1981 film)
{{Infobox film
| name           = Madhouse
| image          = MadhouseVHS1.jpg
| image_size     =
| caption        = 1984 VHS release
| director       = Ovidio G. Assonitis
| producer       = Ovidio G. Assonitis
| writer         = Ovidio G. Assonitis Stephen Blakeley Peter Sheperd Roberto Gandus
| narrator       =
| starring       = Trish Everly Michael Macrae Dennis Robertson Allison Biggers
| music          = Riz Ortolani
| cinematography = Roberto DEttorre Piazzoli
| released       = 1981
| runtime        = 93 min.
| country        = Italy United States
| language       = English
}}

Madhouse (original title: There Was a Little Girl; also known as And When She Was Bad) is a 1981 Italian slasher film directed by Ovidio G. Assonitis. It stars Trish Everly, Dennis Robertson, Allison Biggers, Michael Macrae, Morgan Hart, Edith Ivey and Jerry Fujikawa. The film features a musical score by Riz Ortolani and cinematography by Assonitis regular Roberto DEttorre Piazzoli.

It was one of the many films on the "video nasty" list, a list of horror/exploitation films banned by the BBFC in the 1980s for violence and obscenity.

== Plot ==

Julia (Trish Everly, in her only film role) is a young teacher for deaf children living in Savannah, Georgia. She has horrid memories of her childhood, which was scarred by her sadistic twin sister Mary (Allison Biggers). At the urging of her uncle, Father James (Dennis Robertson), Julia visits Mary, suffering from a severe skin disease, in a mental institution. The meeting does not go well and Mary vows to make Julia "suffer as she had suffered". As their mutual birthday approaches, several of Julias friends and neighbors begin to die gruesome deaths, some of which are committed by a mysterious Rottweiler dog that has some sort of connection to Mary. But is Mary really the killer?

== Production ==
 Georgia in the notorious Kehoe House, which has a reputation for being haunted. 

The films title refers to the poem "There Was a Little Girl" by Henry Wadsworth Longfellow: 

 There was a little girl  who had a little curl, right in the middle of her forehead. When she was good, she was very, very good, and when she was bad, she was horrid. 

== Release ==

There Was a Little Girl (renamed Madhouse for the video market) was released four times on video. A watered-down, cut and edited version was released on VHS in America  by Virgin-Label, discontinued and then released again in 1989.

The films graphic content got it classified as a "video nasty" by the BBFC, and the film never saw a theatrical release in the United Kingdom. In 2004, the film was passed by the BBFC and was released uncut on DVD by Film 2000, and was released in the U.S by Dark Sky Films in 2008.

== Critical reception ==
 slasher fans."  Tom Becker of DVD Verdict opined, "Little touches of audacity notwithstanding, Madhouse ends up being a mediocre chiller with some unintentional laughs." 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 