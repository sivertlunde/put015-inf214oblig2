Citizen Ruth
{{Infobox film
| name = Citizen Ruth
| image = CitizenRuthPoster.jpg
| caption = Original film poster
| director = Alexander Payne
| producer = Cary Woods Cathy Konrad Andrew Stone Michael Zimbrich Jim Taylor
| starring = Laura Dern Burt Reynolds Mary Kay Place Swoosie Kurtz Kurtwood Smith Kelly Preston Tippi Hedren
| music = Rolfe Kent
| cinematography = James Glennon
| editing = Kevin Tent
| distributor = Miramax Films
| released =  
| runtime = 102 minutes
| country = United States English
| budget = $3,000,000 (estimated)
| gross = $285,112 
}} Jim Taylor and Alexander Payne. The film is the directorial debut of Payne. It stars Laura Dern in the title role of a poor, irresponsible and pregnant woman who unexpectedly attracts attention from those involved in the debate about the morality and legality of abortion.

The film premiered at the Sundance Film Festival in January 1996.  It later opened in limited release in the United States on December 13, 1996. As of 2014, Citizen Ruth remains to be the only Alexander Payne-directed film not nominated for any Academy Awards.

==Plot== huffs it in a paper bag in an alley to get high. Ruth is portrayed as a dumb, inebriated addict, capable of doing nearly anything to get money or drugs.

Ruth has 4 kids, all of whom have been taken from her custody by the state because of her inability to care for them (or even for herself). Her kids are scattered among three different homes. Ruth goes to the home of her brother and sister-in-law to sneak a look at two of her kids and to beg her brother for money.
 pregnant again. At her arraignment, she learns to her horror that she is facing felony charges; her many earlier arrests had all been on misdemeanor charges. The judge, who knows of the situation with Ruths other offspring, suggests to her after the hearing that he will deal with her less harshly if she has an abortion. Through a chance encounter with a group of jailed abortion protesters, Ruth soon finds herself at the center of an escalating battle between people on both sides of the abortion issue. Both sides engage in deceitful tactics to influence Ruths decision. The pro-life people run a fake abortion clinic, where they actually seek to dissuade patients from receiving the advertised service. The pro-choice people have "spies" in the pro-life group who spirit Ruth away.

Both sides offer incentives into the thousands of dollars to the hapless and exhilarated woman to secure her promise that she keep or abort the child. Dollar-conscious Ruth rampantly encourages the bidding. She becomes the object of a local news and political obsession; a figure of the media who all want to know: Will she or wont she have an abortion?

On the day Ruth is to receive her abortion, she suffers a miscarriage. Going along with the pretense of having the abortion, she proceeds to the clinic to collect $15,000 that has been left there for her by one of the security guards of the clinic who believes in personal freedom. He has personally given her the money, free of organizational sponsorship, to match the bid given by the Pro-Life group, so that she can make her decision without the influence of money. She then breaks out of the clinic by dropping a toilet tank cover on a guards head and walks by oblivious protesters on both sides. Even though shed been on the TV news for weeks, none of the picketers on either side pay any attention to her actual presence. Finally standing up, she runs away down the street.

A running joke in the movie is a "Success in Finance"-type tape produced by an Amway-type company. Ruth takes the tape and studies it to determine what to do with her newfound money.

==Cast==
* Laura Dern as Ruth Stoops
* Swoosie Kurtz as Diane Siegler
* Kurtwood Smith as Norm Stoney
* Mary Kay Place as Gail Stoney
* Kelly Preston as Rachel
* M. C. Gainey as Harlan
* Kenneth Mars as Dr. Charles Rollins
* David Graf as Judge Richter
* Kathleen Noone as Nurse Pat
* Tippi Hedren as Jessica Weiss
* Burt Reynolds as Blaine Gibbons
* Alicia Witt as Cheryl Stoney
* Diane Ladd as Ruths mother

==Themes==
Citizen Ruth is known for its explicit attack of the abortion debate. Through black comedy and satire, the film spins the motif of women seeking abortions in "ways unprecedented in prior decades" of film.  Most importantly, the film uses these methods of humor "to critique moral realism in the abortion debates."  While the films overt subject matter is abortion, director Alexander Payne has insisted that the film is more prominently about the human side of fanaticism. Elaborating on this, Payne said, "People become fanatics for highly personal reasons. I mean, its more about them and their own psychosis than about that cause."  This point has been noted by critics, who reaffirm the common loss in sight made by extremists of the people and issues involved in such debates.  

==Reception==
Citizen Ruth received positive reviews upon release. Film review aggregator Rotten Tomatoes reports that 80% of critics gave the film a positive review based on 25 reviews with a "Certified Fresh" rating, with an average score of 6.8/10.  On Metacritic, which assigns a rating out of 100 based on reviews from critics, the film has a score of 64 (citing "generally favorable") based on 18 reviews. 

Roger Ebert of the Chicago Sun-times gave the film three stars out of four praising the film for its "reckless courage to take on both sides in the abortion debate" and for its "gallery of sharp-edged satiric portraits."  Owen Gleiberman of Entertainment Weekly also gave the film a positive review calling the performances "pinpoint perfect", though also suggesting that "movie is a little too aware of its own outrageousness."  Todd McCarthy of Variety (magazine)|Variety focused on the fact of Citizen Ruth being Paynes directorial debut, stating, "Director Payne may not yet possess all the skills necessary to completely pull off a full-scale social farce; he could profitably have added more comic invention around the edges, but he does score quite a few points, even-handedly ribbing the extremists in both camps." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 