Midnight Express (film)
:For the 1924 silent film, see The Midnight Express (film).
{{Infobox film
| name = Midnight Express
| image = Midnight-Express.jpg
| caption = French theatrical release poster
| image_size = 250px
| director = Alan Parker Alan Marshall David Puttnam
| screenplay = Oliver Stone
| based on =   Brad Davis Randy Quaid John Hurt Paul L. Smith Irene Miracle
| music = Giorgio Moroder
| cinematography = Michael Seresin
| editing = Gerry Hambling Casablanca Filmworks
| distributor = Columbia Pictures
| released =  
| runtime = 121 minutes
| country = United States United Kingdom
| language = English Turkish Maltese
| budget = $2.3 million
| gross  = $35,000,000 
}} British prison prison drama Brad Davis, Billy Hayes Midnight Express and was adapted into the screenplay by Oliver Stone.
 smuggle hashish out of Turkey. The movie deviates from the books accounts of the story&nbsp;– especially in its portrayal of Turks&nbsp;– and some have criticized the movie version, including Billy Hayes himself. Later, both Stone and Hayes expressed their regret about how Turkish people were portrayed in the movie.     The films title is prison slang for an inmates escape attempt.

==Plot== Billy Hayes straps 2&nbsp;kg of hashish blocks to his chest. While attempting to board a plane back to the United States with his girlfriend, Billy is arrested by Turkish police on high alert due to fear of terrorist attacks. He is strip-searched, photographed and questioned. After a while, a shadowy American (who is never named, but is nicknamed "Tex" by Billy due to his thick Texan accent) arrives, takes Billy to a police station and translates for Billy for one of the detectives. On questioning Billy tells them that he bought the hashish from a taxicab driver, and offers to help the police track him down in exchange for his release. Billy goes with the police to a nearby market and points out the cab driver, but when the police go to arrest the cabbie, Billy sees an opportunity and makes a run for it. He gets cornered in a building and is recaptured by the mysterious American.

During his first night in holding, Billy, freezing cold, sneaks out of his cell and steals a blanket. Later that night he is rousted from his cell and brutally beaten by the chief of guards, Hamidou.

He wakes a few days later in Sağmalcılar prison, surrounded by fellow Western prisoners Jimmy (an American — in for stealing two candlesticks from a mosque), Max (an English heroin addict) and Erich (a Swede) who help him to his feet. Jimmy tells Billy that the prison is a dangerous place for foreigners like themselves and that no one can be trusted, not even the young children.
 Turkish High Court in Ankara after a prosecution appeal (the prosecutor originally wished to have him found guilty of smuggling and not the lesser charge of possession), and he is ordered to serve at least a 30-year term for his crime. It is then that Billy agrees to attempt a prison-break Jimmy has masterminded. Billy, Jimmy, and Max try to escape through the catacombs below the prison, but their plans are revealed to the prison authorities by fellow-prisoner Rifki. His stay becomes harsh and brutal: terrifying scenes of physical and mental torture follow one another culminating in Billy having a breakdown and beating to near death Rifki, biting out Rifkis tongue in the process. Following this breakdown, he is sent to the prisons ward for the insane where he wanders in a daze among the other disturbed and catatonic prisoners. He meets fellow prisoner Ahmet whilst participating in the regular inmate activity of walking in a circle around a pillar. Ahmet claims to be a philosopher from Oxford University and engages him in conversation to which Billy is unresponsive.

In 1975, Billys girlfriend, Susan, comes to see him and is devastated at what has happened to him. She tells him that he has to escape or else he will die in there and leaves him a scrapbook with money hidden inside as "a picture of your good friend Mr. Franklin from the bank," hoping Billy can use it to help him escape. Her visit moves Billy strongly, and he begins to regain some of his senses. He says goodbye to Max, telling him not to die and to wait for Billy to come back for him. He attempts to bribe Hamidou to take him to the sanitarium where there are no guards. Instead Hamidou takes Billy past the sanitarium to another room and attempts to rape him. Fighting back, Billy inadvertently kills Hamidou by pushing him onto a coat hook. He seizes the opportunity to escape by putting on a guards uniform and manages to walk out of the front door. In the epilogue, it is explained that on the night of October 4, 1975, he successfully crossed the border to Greece, and arrived home three weeks later.

==Cast== Brad Davis Billy Hayes
* Irene Miracle as Susan
* Bo Hopkins as "Tex"
* Paolo Bonacelli as Rifki
* Paul L. Smith as Hamidou
* Randy Quaid as Jimmy Booth
* Norbert Weisser as Erich
* John Hurt as Max
* Kevork Malikyan as the Prosecutor
* Yashaw Adem as the Airport police chief
* Mike Kellin as Mr. Hayes
* Franco Diogene as Yesil
* Michael Ensign as Stanley Daniels
* Gigi Ballista as the Judge
* Peter Jeffrey as Ahmet
* Michael Yannatos as Court translator

==Production==
Although the story is set largely in Turkey, the movie was filmed almost entirely at Fort Saint Elmo in Valletta, Malta, after permission to film in Istanbul was denied  . Ending credits of the movie state: "Made entirely on location in Malta and recorded at EMI Studios, Borehamwood by Columbia Pictures Corporation Limited 19/23 Wells Street, London, W1 England."

The making of the film, Im Healthy, Im Alive, and Im Free, was released in 1977.

==Differences between the book and the film==
Various aspects of Hayes story were fictionalized or added to for the movie. Of note:
* In the movie, Billy Hayes is in Turkey with his girlfriend when he is arrested, whereas in the original story he is alone.
* Although Billy did spend seventeen days in the prisons psychiatric hospital in 1972, he never bit out anyones tongue, which in the film led to him being committed to the section for the criminally insane.
* The scene where Billy attempts to escape from the Turkish police and is recaptured by Tex, the shadowy American agent, did not happen; Tex was a real person Billy encountered after his arrest, who indeed pulled a gun on him, but that was when they were riding in the police car from the Istanbul airport to the police station after Billy attempted to sneak out of the car while it was stopped at a red traffic light. In the books account, Tex drove Billy to the police station where he dropped him off, and Billy never saw him again. It was a Turkish policeman who translated for Billy during his interrogation with the Turkish detective.
* In the books ending, Hayes was moved to another prison on an island from which he eventually escaped, by swimming across the lake and then traveling by foot as well as on a bus to Istanbul and then crossing the border into Greece.  In the movie, this passage is replaced by a violent scene in which he unwittingly kills the head guard who is preparing to rape him. (In reality, Hamidou, the chief guard, was killed in 1973 by a recently paroled prisoner, who spotted him drinking tea at a café outside the prison and shot him eight times.) The attempted rape scene itself was fictionalized; Billy never claimed to have suffered any sexual violence at the hands of his Turkish wardens. He did engage in consensual sex while in prison, but the film depicts Hayes gently rejecting the advances of a fellow prisoner.

==Soundtrack Midnight Express==
{{Infobox album
| Name        = Midnight Express: Music from the Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Giorgio Moroder
| Cover       = Midnight-express-soundtrack.jpg
| Released    = October 6, 1978
| Recorded    =
| Genre       = Disco
| Length      = 37:00
| Label       = Casablanca Records
| Producer    = Giorgio Moroder From Here to Eternity (1977)
| This album  = Midnight Express (1978)
| Next album  = Music from "Battlestar Galactica" and Other Original Compositions (1978)
}} pioneer Giorgio Moroder. The score won the Academy Award for Best Original Score in 51st Academy Awards|1979.

Side A:

# "Chase (composition)|Chase" – Giorgio Moroder (8:24)
# "Loves Theme" – Giorgio Moroder (5:33)
# "(Theme From) Midnight Express" (Instrumental) – Giorgio Moroder (4:39)

Side B:

# "Istanbul Blues" (Vocal) – David Castle (3:17)
# "The Wheel" – Giorgio Moroder (2:24)
# "Istanbul Opening" – Giorgio Moroder (4:43)
# "Cacaphoney" – Giorgio Moroder (2:58) Chris Bennett (4:47)

Giorgio Moroder used the six notes corresponding to "now you say you love me" from the song "Cry Me a River" to create part of the soundtrack  .

==Reception== review aggregate site Rotten Tomatoes, 95% of film critics gave the film positive reviews, based on 20 reviews. 
 Lawrence of Arabia and Midnight Express were like cartoon caricatures, compared to the people I had known and lived among for three of the happiest years of my life."    Pauline Kael, in reviewing the film, commented, "This story could have happened in almost any country, but if Billy Hayes had planned to be arrested to get the maximum commercial benefit from it, where else could he get the advantages of a Turkish jail? Who wants to defend Turks? (They don’t even constitute enough of a movie market for Columbia Pictures to be concerned about how they are represented)".    One reviewer writing for World Film Directors wrote, "Midnight Express is more violent, as a national hate-film than anything I can remember, a cultural form that narrows horizons, confirming the audience’s meanest fears and prejudices and resentments".   
 David Denby New York criticized the film as "merely Anti-Turkism|anti-Turkish, and hardly a defense of prisoners rights or a protest against prison conditions". Denby, D. (1978, October 16). One Touch of Mozart. New York Magazine, 11(42), 123.  Denby said also that all Turks in the movie – guardian or prisoner – were portrayed as "losers" and "swine" and that "without exception   are presented as degenerate, stupid slobs". 

Turkish Cypriot film director Derviş Zaim wrote a thesis at Warwick University on the representation of Turks in the film, where he concluded that the one-dimensional portrayal of the Turks as "terrifying" and "brutal" served merely to reinforce the sensational outcome and was likely influenced by such factors as Orientalism and Capitalism. 

===Awards and nominations=== Best Music, Best Writing, Best Actor Best Director, Best Film Best Picture.

The film was also entered into the 1978 Cannes Film Festival.   

==Legacy==
An amateur interview with Hayes appeared on YouTube,  recorded during the 1999 Cannes Film Festival, in which he described his experiences and expressed his disappointment with the film adaptation.  In an article for the Seattle Post-Intelligencer, Hayes was reported as saying that the film "depicts all Turks as monsters." 
 Academy Award for the film, made an apology for the portrayal of the Turkish people in the film.  He "eventually apologised for tampering with the truth." 

Alan Parker, Oliver Stone and Billy Hayes were invited to attend a special film screening with prisoners in the garden of an L-type prison in Döşemealtı, Turkey, as part of the 47th Antalya Golden Orange Film Festival in October 2010.   

Dialogue from the film was sampled in the song Sanctified on the original version of Nine Inch Nails debut album Pretty Hate Machine. The sample was removed from the 2010 remaster for copyright reasons.

==References==
 

==External links==
*  
*  
* Midnight Express truth revealed by Alinur (  –  ): Interview with Hayes about the movie at YouTube.
*  

 
 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 