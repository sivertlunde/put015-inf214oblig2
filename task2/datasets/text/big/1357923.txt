Summer Holiday (1963 film)
 
 
 
{{Infobox film
| name           = Summer Holiday
| image_size     = 
| image	=	Summer Holiday FilmPoster.jpeg
| border         = yes
| caption        = US theatrical poster
| director       = Peter Yates
| producer       = Kenneth Harper
| writer         = Peter Myers Ronald Cass 
| starring       = Cliff Richard Lauri Peters
| music          = Stanley Black Peter Myers  Ronald Cass John Wilcox
| editing        = Jack Slade ABPC
| AIP  
| released       = 10 January 1963 (World Premiere, London) 
| runtime        =107 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Summer Holiday is a British CinemaScope and Technicolor musical film featuring singer Cliff Richard. The film was directed by Peter Yates (his debut), produced by Kenneth Harper. The original screenplay was written by Peter Myers and Ronald Cass (who also wrote most of the song numbers and lyrics). The cast included Lauri Peters, Melvyn Hayes, Teddy Green, Jeremy Bulloch, Una Stubbs, Pamela Hart, Jacqueline Daryl, Lionel Murton, Madge Ryan, David Kossoff, Nicholas Phipps, Ron Moody and The Shadows. Herbert Ross choreographed the musical numbers. Warner Theatre in Londons West End on 10 January 1963. 

== Plot == London Transport AEC Regent The Young Ones (1961).

==Cast==
*Cliff Richard as Don
*Lauri Peters as Barbara
*Melvyn Hayes as Cyril
*Una Stubbs as Sandy
*Teddy Green as Steve
*Pamela Hart as Angie
*Jeremy Bulloch as Edwin
*Jacqueline Daryl as Mimsie
*Madge Ryan as Stella
*Lionel Murton as Jerry
*Christine Lawson as Annie
*Ron Moody as Orlando
*David Kossoff as Magistrate
*Wendy Barrie as Shepherdess (as Wendy Barry)
*Nicholas Phipps as Wrightmore
*The Shadows as themselves

==Soundtrack==
 
 Summer Holiday", "Bachelor Boy", "Dancing Shoes", "Foot Tapper", "Big News", "The Next Time", "Les Girls", "Round and Round" and "Orlandos Mime".

The films producers felt that female lead in the film, Lauri Peters, was not a strong singer after several test recording sessions and all of her parts, both in the film and on the soundtrack album were dubbed by session vocalist Grazina Frame. Frame had overdubbed female singing voices in Cliff Richards earlier film The Young Ones. 

Cliff Richard, Melvyn Hayes and the Shadows were recalled to Elstree some weeks after completion of shooting to record Bachelor Boy. This was because the distributors felt the film was too short.

== Release ==
===Box Office===
The film was the second most popular movie at the British box office in 1963.  However it flopped in the US, where it was released two days after JFK was assassinated.
===Cultural Impact=== The Turning Point, 1977).
 Summer Holiday", Number one in the British charts during the first three months of 1963.
 Benjamin Stone and travel around and photograph Britain from 1973 to 1974. Phil Coomes, " ", BBC News in Pictures, 15 November 2011. 

Both the film and the title song captured something of the popular mood as relatively cheap continental holidays became available in Britain around that time. It was voted the 99th greatest family film in a Channel 4 poll.

== Stage adaptations == The Opera Peter Baldwin and René Zagger. It was also recorded and released on video, entitled "Summer Holiday-The Hits", which was all the musical numbers strung together by clips of Darren Day writing postcards to his friends, and describing what was happening in the story as he wrote.
 The Young Ones".

The stage musical was revived in 2003, starring Stefan Booth but later starring Darren Day again. This production also included Days future partner Suzanne Shaw (from the pop group Hearsay) as Bobby and, as Shaws mother, Aimi MacDonald. The production toured the UK.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 