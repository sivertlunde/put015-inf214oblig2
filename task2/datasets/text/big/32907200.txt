The Enemy Within (1918 film)
 
{{Infobox film
| name           = The Enemy Within
| image          = 
| image_size     =
| caption        = 
| director       = Roland Stavely
| producer       = Roland Stavely
| writer         = Roland Stavely
| narrator       =
| starring       = Snowy Baker
| music          =
| cinematography = Franklyn Barrett
| editing        = 
| distributor    = 
| released       = 11 March 1918
| runtime        = 5,500 feet
| country        = Australia
| language       = Silent film  English intertitles
| budget         =
| preceded_by    = 
| followed_by    = 
}}

The Enemy Within is a 1918 Australian silent film starring renowned Australian sportsman Snowy Baker in his first screen role. 

Unlike many Australian silent movies, it is possible to see the full film today.

==Synopsis==
Jack Airlie is a secret agent who has worked for four years abroad. He returns to Australia after four years away and falls for Myree Brew, beautiful daughter of his oldest friend, Mrs Drew. Rich businessman Henry Brasels is also in love with Myree.

Brasels is running a gang of German saboteurs, including radical leader Bill Warne, who is planning to set off a series of bombs. Brasels lures Jack into a trap but he manages to escape with the help of his sidekick, detective Jimmy cook. Brasels kidnaps Myee and tries to get on board a German ship. however Jack manages to climb down a steep cliff and rescue her, as the Coastal Patrol capture Warne and Warne.

==Cast==
*Snowy Baker as Jack Airlie John Faulkner as Henry Brasels
*Lily Molloy as Myree Drew
*Nellie Calvin as Claire Lerode
*Billy Ryan as Bill Warne
*Sandy McVea as Jimmy Cook
*Lily Rochefort as Mrs Drew
*Gerald Harcourt as Glassop
*Marjory Donovan as the child

==Production==
The film was specifically concocted a vehicle for Snowy Baker, with plenty of action sequences to demonstrate his physical prowess, including climbing down a 300 foot cliff, leaping from a moving car, diving 80 foot into Sydney harbour at Coogee Bay and hand-to-hand fighting.  
 SMS Wolf in the Pacific during World War I, and the sinking of the Cumberland off Gabo Island.   The villains were based on the Industrial Workers of the World, and shown to be operating in Sydney high society. 

Filming started in December 1917. The director, Roland Stavely, was a stage director for J.C. Williamson Ltd.

The part of Snowy Bakers assistant was played by Sandy McVea, an aboriginal boxer. 

==Release==
The film was specifically advertised as "not a war picture but a thrilling drama of a special agents fight against spies in Australia".  It was a hit and led to a number of action movies starring Baker.

A novelised version of the script was published in 1919.

==References==
 

==External links==
* 
*  at Australian Screen Online
*  at the National Film and Sound Archive
* 

 
 
 
 
 
 