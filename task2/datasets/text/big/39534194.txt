97 Aces Go Places
 
 
{{Infobox film
| name           = 97 Aces Go Places
| image          = 97AcesGoPlaces.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 最佳拍檔之醉街拍檔
| simplified     = 最佳拍挡之醉街拍挡
| pinyin         = Zuì Jiā Pāi Dàng Zhī Zuì Jiē Pāi Dàng   
| jyutping       = Zeoi3 Gaai1 Paak3 Dong3 Zi1 Zeoi3 Gaai1 Paak3 Dong3 }}
| director       = Chin Kar-lok Raymond Wong
| writer         = 
| screenplay     = Raymond Wong
| story          = 
| based on       = 
| narrator       =  Tony Leung Christy Chung Donna Chu Francis Ng
| music          = Brother Hung
| cinematography = Herman Yau
| editing        = Robert Choi
| studio         = Eastern Bright Motion Picture Hoi Ming Films Mandarin Films
| released       =  
| runtime        = 92 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$10,745,180
}}
 1997 Cinema Hong Kong action comedy Tony Leung, Christy Chung, Donna Chu and Francis Ng. The film is the sixth and final installment of the Aces Go Places (film series)|Aces Go Places film series with a different cast and storyline.

==Plot==
Con artist Mandy Ling / Li Lai Shan cons a rich triad leader Lui Yu Yeung out of his money and gives it to a convalescent center where her mentally disturbed sister Mandy Li is staying. Earlier, she had also conned another triad leader out of a large sum of money in a poker game causing him to die from a heart attack. The triad leader states in his will that his son Ho Sik must avenge him by killing her with a gun. Ho Sik, who has no interest in guns and violence, hires Chui Cheong, the "Drunken Gun", an ace gunman to tutor him. However, Ho later finds himself falling in love with Mandy and is reluctant to kill her.

==Cast==
*Alan Tam as Ho Sik / Ho Siks father
*Tony Leung Chiu-Wai as Chu Cheong, the Drunken Gun
*Christy Chung as Mandy Ling / Li Lai Shan
*Donna Chu as Mandy Li
*Francis Ng as Lui Yu Yeung
*Simon Lui as Chung Yue
*Maria Cordero as God Mother
*Ben Lam as Lung
*Moses Chan as Yeungs sidekick
*Billy Chow as Yeungs killer Raymond Wong as Senior Police Officer
*Dayo Wong as Mr. Chan
*Bennett Pang as Pastor
*Emily Kwan as Yues driving instructor
*Lam Chiu Wing as musician
*Chin Siu-ho as driver admiring Siks car
*Karen Tong as woman in the street
*Joey Leung as retarded man
*Vincent Kok as Fatty Fook
*Collin Chou as police special force
*Timmy Hung as police special force
*Mok Ka Yiu as police special force
*Carlo Ng as police special force
*See Mei Yee as Yeungs art appraiser
*Johnny Wong as musician
*Mang Hoi as lieutenant of Siks Group
*Ka Lee as lieutenant of Siks Group
*Lee Chi Kit as lieutenant of Siks Group
*Ma Koo as tea lady at poker game
*Kuk Hin Chiu
*Dion Lam as Fattys man in black
*Chin Kar-lok as Fattys man in black
*Peter Chan as lieutenant of Siks Group
*Yee Tin Hung as Fattys man in black
*Tang Chiu Yau as Fattys man in black
*Rocky Lai as Yeungs killer
*Chan Siu Lung
*Nelson Cheung as man in the street
*Chu Cho Kuen as Yeungs thug
*Jack Wong as Yeungs thug
*Mak Wai Cheung as Yeungs thug
*Cheung Bing Chuen as Fattys man in black
 

==Box office==
The film grossed HK$10,745,180 at the Hong Kong box office during its theatrical run 21 June to 9 July 1997 in Hong Kong.

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 
 
 
 
 
 
 
 