The Indian Brothers
 
{{Infobox film
| name           = The Indian Brothers
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         =
| starring       = Frank Opperman
| cinematography = G. W. Bitzer
| editing        =
| distributor    =
| released       =  
| runtime        = 18 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short silent silent drama film directed by D. W. Griffith, starring Frank Opperman and featuring Blanche Sweet.   

==Cast== Frank Opperman as The Indian Chief
* Wilfred Lucas as The Indian Chiefs Brother
* Guy Hedlund as The Renegade John T. Dillon as At Funeral (as Jack Dillon)
* Francis J. Grandon as Indian
* Florence La Badie
* Alfred Paget as In Second Tribe / At Funeral
* W. C. Robinson as In Second Tribe
* Blanche Sweet as Indian
* Kate Toncray as Indian Charles West as At Funeral (as Charles H. West)

==See also==
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 