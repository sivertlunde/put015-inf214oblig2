Fire and Ice (1983 film)
 
{{Infobox film
| name           = Fire and Ice
| image          = Fire and Ice 1983 poster.png
| caption        = Theatrical release poster
| image_size     = 250px
| director       = Ralph Bakshi
| producer       = Ralph Bakshi Frank Frazetta
| writer         = Gerry Conway Roy Thomas
| starring       = Susan Tyrrell Maggie Roswell William Ostrander Stephen Mendel Steve Sandor
| music          = William Kraft
| cinematography = Francis Grumman
| editing        = A. David Marshall
| studio         = Producers Sales Organization
| distributor    = 20th Century Fox
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = $1.2 million
| gross          = $760,883
}}
Fire and Ice is a 1983 American animated Adventure genre|adventure-fantasy film directed by Ralph Bakshi. The film, a collaboration between Bakshi and Frank Frazetta, was distributed by 20th Century Fox, which also distributed Bakshis 1977 release, Wizards (film)|Wizards.    The animated feature, based on characters Bakshi and Frazetta co-created, was made using the process of Rotoscope|rotoscoping, in which scenes were shot in live action and then traced onto animation cels.

The screenplay was written by Gerry Conway and Roy Thomas, both of whom had written Conan (comics)|Conan stories for Marvel Comics. Background painter was James Gurney, the author and artist of the Dinotopia illustrated novels. Thomas Kinkade also worked on the backgrounds to various scenes.

==Plot==
 
From their stronghold in Icepeak, the evil Queen Juliana (Susan Tyrrell) and her son, Nekron (Stephen Mendel), send forth a wave of glaciers, forcing humanity to retreat south towards the equator. Nekron sends a delegation to King Jarol (Leo Gordon) in Firekeep to request his surrender, but this is a ruse orchestrated by Queen Juliana for Nekron’s sub-humans to kidnap Jarol’s daughter, Princess Teegra (Cynthia Leake), a 15-year-old, barefoot, microkini-wearing teenage girl; Queen Juliana feels that Nekron should take a bride to produce an heir.

But Teegra makes an escape and comes upon Larn (Randy Norton), a 17-year-old teenage boy who is the young warrior and the only survivor of a village razed by glaciers, who offers to escort her back to Firekeep. As Teegra is recaptured, Larn teams with the mysterious Darkwolf (Steve Sandor) to save Teegra and then travel to Icepeak to stop Juliana. Darkwolf faces Nekron and kills him as Icepeak succumbs to lava released by King Jarol and is destroyed.

The film finishes with Larn about to kill a beaten sub-human until Teegra stops him saying that "its over" and embraces him. Darkwolf is seen atop a cliff; he watches the pair, smiles and then disappears. Teegra and Larn kiss as the credits roll.

==Cast==

===Performers===
*Randy Norton –  Larn 
*Cynthia Leake – Teegra 
*Steve Sandor – Darkwolf 
*Sean Hannon – Nekron 
*Leo Gordon – Jarol 
*William Ostrander – Taro  
*Eileen ONeill – Juliana 
*Elizabeth Lloyd Shaw – Roleil 
*Micky Morton – Otwa 
*Tamarah Park – Tutor 
*Big Yank – Monga 
*Greg Wayne Elam – Pako

===Voice cast===
*Susan Tyrrell – Juliana 
*Maggie Roswell – Teegra 
*William Ostrander – Larn
*Stephen Mendel – Nekron 
*Alan Koss – Envoy 
*Clare Nono – Tutor  
*Hans Howes – Defender Captain
*Ray Oliver – Subhuman 
*Nathan Purdee – Subhuman 
*Le Tari – Subhuman

==Production== The Beastmaster Conan the Barbarian, and Bakshi had a desire to work with long-time friend and fantasy illustrator Frank Frazetta.    Bakshi received $1.2 million to finance Fire and Ice from some of the same investors as American Pop, and 20th Century Fox agreed to distribute the film based upon the financial longevity of Wizards. 

Because Fire and Ice was the most action-oriented story Bakshi had directed up until that point, rotoscoping was again used, and the realism of the animation and design replicated Frazettas artwork.  Bakshi and Frazetta were heavily involved in the production of the live-action sequences, from casting sessions to the final shoot.  The films crew included background artists James Gurney and Thomas Kinkade, layout artist Peter Chung, and established Bakshi Productions artists Sparey, Steven E. Gordon, Bell and Banks.  Chung strongly admired Bakshi and Frazettas work, and animated his sequences on the film while simultaneously working for The Walt Disney Company. 

==Reception==
Andrew Leal wrote, "The plot is standard   recalling nothing so much as a more graphic episode of Filmations He-Man and the Masters of the Universe|He-Man series.   Fire and Ice essentially stands as a footnote to the spate of barbarian films that followed in the wake of Arnold Schwarzeneggers appearance as Conan the Barbarian (1982 film)|Conan." 

==Home video release==
The film was released on   in 2008.

==Legacy==
In 2003, the Online Film Critics Society ranked the film as the 99th greatest animated film of all time.   

==Remake==
In 2010, Robert Rodriguez announced that he would direct a live-action remake of the film.  Bakshi stated that he did not want any involvement with the film, but he agreed to license the rights to Rodriguez.  The deal closed shortly after Frazettas death.     On December 18, 2014 Sony Pictures Entertainment acquired the filming rights to the live-action remake version of the 1983 animated film Fire and Ice that will be directed by Robert Rodriguez. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   at Blue Underground

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 