Goofy and Wilbur
 
{{Infobox film|
| name           = Goofy and Wilbur 
| image          = Poster for Goofy and Wilbur.jpg
| caption        = Original theatrical poster
| director       = Dick Huemer 
| producer       =
| story artist   = Otto Englander 
| narrator       = 
| voice artist   = George Johnson 
| musician       = Paul J. Smith
| animator       = Art Babbitt Izzy Klein Ed Love Wolfgang Reitherman Bernard Wolf
| studio         = Walt Disney Productions 
| distributor    = RKO Radio Pictures 
| released       =   
| runtime        = 8 minutes 5 seconds
| language       = English
| awards         = 
}}
Goofy and Wilbur is a cartoon produced by Walt Disney Productions and released by RKO Radio Pictures on March 17, 1939 in film|1939. It was the first cartoon which featured Goofy in his first solo role without Mickey Mouse and/or Donald Duck. In this cartoon Goofy goes fishing with his pet grasshopper, Wilbur, only for persistent bad luck to befall the duo. The cartoon is notable for its high level of japery throughout, and its remarkable sequence of 13 consecutive Physical comedy|pratfalls. 

==Releases==
*1939 &ndash; Original theatrical release
*1956 &ndash; Walt Disney anthology television series|Disneyland, episode #2.22: "On Vacation" (TV)
*1977 &ndash; Donald Ducks Summer Magic (theatrical)
*1981 &ndash; "Mickey Mouse and Donald Duck Cartoon Collections Volume Two" (laserdisc)
*c. 1983 &ndash; Good Morning, Mickey!, episode #59 (TV)
*c. 1992 &ndash; Mickeys Mouse Tracks, episode #46 (TV)
*c. 1992 &ndash; Donalds Quack Attack, episode #32 (TV) The Ink and Paint Club, episode #1.25: "Goofy Goofs Around" (TV)
*2002 &ndash; " " (DVD)
*2006 &ndash; " " (DVD)

==External links==
*  at  
*  

 
 
 
 
 

 