Arizona Stage Coach
{{Infobox film
| name           = Arizona Stage Coach
| image_size     =
| image	=	Arizona Stage Coach FilmPoster.jpeg
| caption        =
| director       = S. Roy Luby
| producer       = Dick Ross (associate producer) Anna Bell Weeks (associate producer) George W. Weeks (producer) Oliver Drake (story) Arthur Hoerl (adaptation)
| narrator       =
| starring       = See below
| music          =
| cinematography = Robert E. Cline
| editing        = S. Roy Luby
| distributor    = Monogram Pictures
| released       = 4 September 1942
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Arizona Stage Coach is a 1942 American film directed by S. Roy Luby, one of the series of western Range Busters films.

== Plot summary == Wells Fargo.

== Cast ==
*Ray Corrigan as Crash Corrigan
*John Dusty King as Dusty King
*Max Terhune as Alibi Terhune
*Elmer as Elmer, Alibis dummy
*Nell ODay as Dorrie Willard Charles King as Tim Douglas
*Riley Hill as Ernie Willard
*Kermit Maynard as Henchman Strike Cardigan
*Carl Mathews as Henchman Ace
*Slim Whitaker as Henchman Red
*Slim Harkey as Henchman Steve Clark as Jake - Stage Driver-Henchman Frank Ellis as Dan - Stage Shotgun-Guard / Henchman Jack Ingram as Sheriff Denver
*Stanley Price as Tex Laughlin - Hold-Up Man
*Forrest Taylor as Uncle Larry Meadows

== Soundtrack ==
* John "Dusty" King - "Red River Valley" (Music by James Kerrigen)
* John "Dusty" King - "Where The Grass Grows Greener In The Valley" (Music by Rudy Sooter)

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 