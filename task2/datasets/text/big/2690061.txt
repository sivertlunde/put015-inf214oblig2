War Crimes (film)
 

{{Infobox film
| name           = War Crimes
| image          = War Crimes (film).jpg
| image_size     =
| caption        =
| director       = Michael G. Thomas, Nick S. Thomas
| producer       = Michael G. Thomas, Nick S. Thomas
| writer         = Michael G. Thomas, Nick S. Thomas
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        = Michael G. Thomas, Nick S. Thomas
| distributor    =
| released       =  
| runtime        = 82 minutes
| country        = United Kingdom
| language       = English language|English, Serbo-Croat language|Serbo-Croatian
| budget         =
}} 2005 film starring John Jenner and Earl Palmer. It was written and directed by Thomas Bros.

==Production== British feature UK in 2005.  The film is set in 1992 during the traumatic events of the split up of Yugoslavia.  The film itself is based in Bosnia and Herzegovina|Bosnia, 40% of the dialogue is in Serbo-Croatian language. This project was shot on Digital Video and currently being distributed by ITN.  This film was shot on location in Wales and England and features a multi-national cast from the UK, USA, Belgium, Germany, Greece, Finland and Canada.

==Plot==
A group of six friends, recently graduated in the UK travel to Bosnia in 1992.  As fighting breaks out around the capital Sarajevo the friends are forced to escape overland to the border whilst the fighting spreads.  After witnessing a horrific massacre in Bosnia they are pursued by a ruthless  Yugoslav Peoples Army  Officer and his brutal Chetnik fighters.  With the weather worsening, limited supplies and murderous soldiers behind them, will they ever make it out of Croatia alive?!

==Main cast==
* John Jenner - Lt. Brcko
* Earl Palmer - Scott

 
 
 
 
 
 


 