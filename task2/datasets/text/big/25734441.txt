House!
{{Infobox film
| name           = House!
| image          = 
| caption        = 
| director       = Julian Kemp
| producer       = Michael Kelk
| writer         = Eric Styles Jason Sutton Jason Hughes
| music          = Mark Thomas
| cinematography = Kjell Vassdal
| editing        = Jonathan Rudd
| studio         = CF1 Cyf Wire Films
| distributor    = Pathé Victor Film Company
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
}}
 British comedy Jason Hughes.

==Plot== bingo hall Jason Hughes) is the cheeky bingo caller. When a large international conglomerate announces they are about to open a huge family entertainment center nearby, promising competition through large payouts for their own bingo competitions, Linda (Kelly Macdonald) comes to the aid of the La Scala using her psychic gift. 

==Cast==
* Kelly Macdonald as Linda
* Freddie Jones as Mr. Anzani
* Miriam Margolyes as Beth Jason Hughes as Gavin

==Reception==
Derek Elley of Variety (magazine)|Variety called House! an "Ealing Studios|Ealing-style light comedy", writing that the film was "helmed with impressive technical finesse" by director Julian Kemp" and "propelled by a knockout performance from Kelly Macdonald".   Praising Jason Suttons script, he wrote "none of this would have worked if the characters were simply cutouts and the thesps just mugging along in colorful accents. But Suttons script, which also makes room for a variety of smaller roles, allows the protags to grow and isnt, as becomes clear later on, simply about winning." 

Angus Wolfe Murray of Eye for Film wrote that in "the British tradition of little-things-mean-a-lot, House! fits like chips with fish," and that the film "has the feel good factor in spades." He closes by admiring Suttons writing and Macdonalds performance. 

eFilmCritic.com wrote that the film began with a "classic Tarantino pastiche" but became "soon obvious that this isnt your typical lottery funded mishap", noting that director Kemp managed to "inject a sense of life and excitement into the dullest of premises". They summarized by offering that the film was "definitely quirky, and no-ones idea of a main-stream hit, this film has a bit of magic about it." 

==References==
 

   

   

   

   

 

==External links==
*  

 

 
 