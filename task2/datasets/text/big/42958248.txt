East Is West (1922 film)
{{infobox film
| title          = East Is West
| image          = File:East is West lobby card.jpg
| image_size     = 250px
| caption        = Lobby card Sidney Franklin
| producer       = Constance Talmadge
| writer         = Frances Marion
| based on       =  
| starring       = Constance Talmadge
| music          =
| cinematography = Tony Gaudio
| editing        =
| distributor    = First National Pictures (as Associated First National)
| released       = October 15, 1922
| runtime        = 80 minutes
| country        = United States
| language       = Silent (English intertitles)

}} Sidney Franklin talkie at Universal in 1930 with Lupe Velez. A copy of the film is held at the EYE Film Institute Netherlands, formerly Filmmuseum Nederlands.

==Cast==
*Constance Talmadge - Ming Toy
*Edmund Burns - Billy Benson (billed as Edward Burns)
*E. Alyn Warren - Lo Sang Kee (billed as E.A. Warren)
*Warner Oland - Charley Yong
*Frank Lanning - Hop Toy
*Nick De Ruiz - Chang Lee
*Nigel Barrie - Jimmy Potter
*Lillian Lawrence - Mrs. Benson
*Winter Hall - Mr. Benson
*James Wang - Boat Proprietor (billed as Jim Wang)

==References==
 

==External links==
 
*  
* 
*  at www.eyefilm.nl

 

 
 
 
 
 
 
 
 
 


 