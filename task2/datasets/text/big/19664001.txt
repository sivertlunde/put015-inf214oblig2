This Other Eden (film)
{{Infobox Film
| name           = This Other Eden
| image          = 
| image_size     = 
| caption        = 
| director       = Muriel Box
| producer       = Muriel Box Alec C. Snowden
| writer         = Louis dAlton (play) Blanaid Irvine Patrick Kirwan
| starring       = Audrey Dalton Leslie Phillips Niall MacGinnis Geoffrey Golden
| music          = Lambert Williamson Gerald Gibbs
| editing        = Henry Richardson
| distributor    = 
| released       = 1959
| runtime        =  Ireland
| language       = 
}}
 1959 Republic Irish comedy drama film directed by Muriel Box and starring Audrey Dalton, Leslie Phillips and Niall MacGinnis.

==Synopsis==
A small Irish provincial town wants to erect a statue to an Irish Republican Army (IRA) man, killed during the Irish War for Independence. However, the son of the Colonel who killed the rebel objects.

==Cast==
* Audrey Dalton as Maire McRoarty
* Leslie Phillips as Crispin Brown
* Niall MacGinnis as Devereaux
* Geoffrey Golden as McRoarty
* Norman Rodway as Conor Heaphy
* Milo OShea as Pat Tweedy
* Harry Brogan as Clannery
* Paul Farrell as McNeely
* Eddie Golden as Sergeant Crilly
* Hilton Edwards as The Canon
* Philip OFlynn as Postman
* Ria Mooney as Mother Superior
* Isobel Couser as Mrs. OFlaherty

==External links==
* 

 

 
 
 
 
 
 
 


 
 