H-8 (film)
{{Infobox Film
  | name = H-8
  | image = Argentine promotional poster film H-8.gif
  | director = Nikola Tanhofer
  | producer = 
  | writer = Zvonimir Berković Tomislav Butorac
  | starring  = Đurđa Ivezić Boris Buzančić Antun Vrdoljak Vanja Drach Marijan Lovrić
  | music = Dragutin Savin
  | cinematography = Slavko Zalar
  | editing = Radojka Tanhofer
  | distributor = 
  | released = 1958
  | runtime =  105 minutes
  | country = Yugoslavia
  | language = Croatian
  | budget = 
}}
 Yugoslav film directed by Nikola Tanhofer.

==Synopsis==
"A bus and a truck are moving towards each other along a two-way traffic highway on a rainy day. At the very beginning we learn that a reckless driver of another car will cause them to collide while trying to pass the bus; we even learn what seats will spell doom for their occupants. The rest of the movie follows two streams of events on the bus and on the truck, getting us to know and like a wide variety of characters, wondering which ones will end up being casualties and holding (our) breath for our favourites." (IMDb)

== Cast ==
*Đurđa Ivezić as Alma Novak
*Boris Buzančić as Journalist Boris
*Antun Vrdoljak as Photographer
*Vanja Drach as Krešo
*Marijan Lovrić as Rudolf Knez
*Mira Nikolić as Young Mother
*Antun Nalis as Thief Ivica
*Mia Oremović
*Stane Sever
*Pero Kvrgić
*Marija Kohn
*Fabijan Šovagović
*Ljubica Jović
*Ivan Šubić
*Siniša Knaflec

==Production==
The movie is based on a true story, in which the driver that caused a fatal 1957 bus-truck collision was never identified. "H-8" is the beginning of that drivers license plate, the only available information on the culprits vehicle.

==Reception==
In 1999, a poll of Croatian film critics found it to be one of the best Croatian films ever made. 

== Awards ==
* H-8 won the Big Golden Arena for Best Film at the 1958 Pula Film Festival. 

== See also ==
*List of Yugoslav films

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 


 
 