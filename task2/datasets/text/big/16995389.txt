The Bad One
 
{{Infobox film
| name           = The Bad One
| image          = The-Bad-One-1930.jpg
| caption        = Film poster
| director       = George Fitzmaurice Joseph M. Schenck Carey Wilson
| starring       = Dolores del Río Edmund Lowe
| music          = Hugo Riesenfeld
| cinematography = Karl Struss
| editing        = W. Donn Hayes
| studio         = Joseph M. Schenck Productions (for Art Cinema Corporation)
| distributor    = United Artists
| released       =  
| runtime        = 70 minutes
| country        = United States 
| language       = English
| budget         = 
}}

The Bad One is a 1930 American black-and-white musical film directed by George Fitzmaurice, starring Dolores del Río and featuring Boris Karloff and is a romantic prison drama film. The Bad One is also known as Femmina in Italy, I gynaika tis tavernas in Greece and La mala in Mexico.

==Cast==
* Dolores del Río - Lita
* Edmund Lowe - Jerry Flanagan
* Don Alvarado - The Spaniard
* Blanche Friderici - Madame Durand (as Blanche Frederici)
* Adrienne DAmbricourt - Madame Pompier Ullrich Haupt - Pierre Ferrande
* Mitchell Lewis - Borloff Ralph Lewis - Blochet
* Charles McNaughton - Petey
* Yola dAvril - Gida
* John St. Polis - Judge
* Henry Kolker - Prosecutor
* George Fawcett - Warden
* Victor Potel - Sailor Harry Stubbs - Sailor
* Tom Dugan - Sailor
* Boris Karloff - Monsieur Gaston

==See also==
* List of American films of 1930
* Boris Karloff filmography

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 