Scooby-Doo 2: Monsters Unleashed
{{Infobox film
| name           = Scooby-Doo 2: Monsters Unleashed
| image          = Scooby doo two poster.jpg
| caption        = Theatrical release poster
| director       = Raja Gosnell
| producer       = Charles Roven  Richard Suckle James Gunn
| based on       =   Freddie Prinze Jr.  Sarah Michelle Gellar  Linda Cardellini Matthew Lillard  Seth Green  Peter Boyle  Tim Blake Nelson  Alicia Silverstone David Newman Oliver Wood
| editing        = Kent Beyda
| studio         = Mosaic Media Group Warner Bros. Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States Canada
| language       = English
| budget         = $80 million
| gross          = $181.5 million 
}}
 comedy horror James Gunn Freddie Prinze Jr. as Fred Jones (Scooby-Doo)|Fred, Sarah Michelle Gellar as Daphne Blake|Daphne, Linda Cardellini as Velma Dinkley|Velma, Matthew Lillard as Shaggy Rogers|Shaggy, and Neil Fanning as the voice of title protagonist Scooby-Doo (character)|Scooby-Doo. Series newcomers include Seth Green, Tim Blake Nelson, Peter Boyle, and Alicia Silverstone who appear in supporting roles. The film was released on March 26, 2004 to generally unfavorable reviews,  and grossed considerably less at the box office than its predecessor, although was still a box office success. 

==Plot== Shaggy and Scooby-Doo (character)|Scooby-Doo) is attending the grand opening of the Coolsonian Criminology Museum, the premiere exhibit displaying the monster costumes of criminals they dealt with in past cases. However, the celebrations are cut short when the reanimated Pterodactyl Ghost attacks, controlled by the Evil Masked Figure, who boldly declares that Mystery Inc. will be destroyed. He escapes with his quarry and two monster costumes; the Black Knight Ghost and the 10,000 Volt Ghost. Heather Jasper-Howe (Alicia Silverstone), a journalist, starts a smear campaign against the gang to discredit them, mainly stemming from a personal feud with Daphne. Velma finds a pterodactyl scale and analyses it, confirming it to be real. The gang conclude one of their old enemies is the masked man wanting revenge. They at first suspect their old enemy Jonathon Jacobo (Tim Blake Nelson), who was the original creator of the Pterodactyl Ghost and knew how to create real monsters and bring them to life, but then news breaks out to the gang that Jacobo has drowned during an attempted escape from prison, so the gang guess Jeremiah Wickles (Peter Boyle), their first foe and the Black Knight Ghosts portrayer, and Jacobos roommate in prison, is responsible.

The gang travel to Wickles mansion where they find mysterious green footprints like the scale leading to the library. Fred, Daphne and Velma find an instruction manual on how to create monsters; while Shaggy and Scooby are chased by the reanimated Black Knight but not before finding a note reading "Faux Ghost tonite" which they take as a clue. Daphne fends the knight off until the gang can escape. Shaggy and Scooby sneak into the nightclub, the "Faux Ghost", where many of the convicted criminals they put away hang out, including Wickles who claims he has reformed from his criminal ways; but quickly they are recognized and thrown out. Velma learns the key ingredient to making monsters is a fictional substance called "randomonium". Patrick Wisely (Seth Green), the museum curator, who Velma has a crush on, comes to aid the gang but when they go to the museum, they find the rest of the costumes have been stolen. Shaggy and Scooby encounter the distressed Patrick, and then follow Wickles to the old mining town where the rest of the gang realise randomonium was mined. Wickles is proven to be innocent when the gang learns he plans to turn the mining town into an amusement park for children.

Reuniting, the gang find the Monster Hive, finding a machine which brings the monster costumes to life. Shaggy and Scooby mess with the machines control panel, causing several monsters to be reanimated. The gang flee with the control panel, as the Evil Masked Figure attacks Coolsville and Howe asks the gang to turn themselves in to save the citizens. The gang flee to their old high school clubhouse where they realise they will be able to reverse the Monster Hives effects by altering the control panels wiring. Outside, Shaggy and Scooby lament on their habit of being clumsy and desire to be heroes. Captain Cutlers Ghost rises out of the nearby bayou, forcing the gang to flee in the Mystery Machine back to the mining town. Fred and Daphne fight the Black Knight and electrical 10,000 Volt Ghost, defeating them with a pair of jumper cables, Velma dispatches the Skeleton Men and Shaggy and Scooby defeat Miner 49er using Scoobys flatulence to reflect the villains fire breath back at him. Velma, having eluded her pursuers, finds a shrine to Jacobo, actually made by Patrick who was Jacobos student at one time, but is not the bad guy proving that by saving Velmas life. However, he is captured and dragged away by the Pterodactyl Ghost. After this, Shaggy and Scooby run into the Cotton Candy Glob which threatens to destroy them. Amazed at this monster, instead of running away as they usually do, they instead begin to eat the cotton candy glob. During the process of being eaten, the Cotton Candy Glob begins to cry out "No! Ill give you cavities!".

The gang confront the Evil Masked Figure who has all of them held captive by the Tar Monster, aside from Scooby, who grabs a nearby fire extinguisher to freeze the monster and makes his way to the machine and inserts the control panel, turning the monsters back to costumes. The Evil Masked Figure tries to escape but trips on a broken cat walk and is left dangling from it by his cape. The authorities, press and Coolsville citizens arrive, where the gang unmasked the Evil Masked Figure as Howe - only to then unmask her as none other than Jacobo, alive and well. Jacobo survived his apparent "death" and was photographed outside the museum before it opened, found by Velma at the shrine. Jacobo and Howes cameraman Ned (Zahf Paroo) are arrested, whilst Mystery, Inc. are welcomed back by Coolsville with open arms. Velma and Patrick agree to go out again, Fred and Daphne remain at a peaceful relationship and Shaggy and Scooby stay best pals, though not before Scooby attacks Shaggy with a boom mike after mistaking him for a monster. The main cast dance with Ruben Studdard at the Faux Ghost in the ending scenes.

In a post-credits scene, Scooby is shown in a creepy hall playing the "Scooby-Doo 2" game on a Game Boy Advance. He then turns the game screen towards the camera, and says "Game Boy Advance secret code", showing a cheat code for the game he was playing.

==Cast== Freddie Prinze Fred Jones
* Sarah Michelle Gellar as Daphne Blake
* Linda Cardellini as Velma Dinkley
* Matthew Lillard as Shaggy Rogers
* Seth Green as Patrick Wisley
* Peter Boyle as Jeremiah "Old Man" Wickles
* Tim Blake Nelson as Dr. Jonathan Jacobo
* Alicia Silverstone as Heather Jasper-Howe
* Joe Macleod as Skater Dude #1
* Stephen E. Miller as C.L. Magnus
* Karin Konoval as Aggie Wilkins
* Ryan Vrba as Young Fred
* Emily Tennant as Young Daphne
* Cascy Beddow as Young Shaggy
* Lauren Kennedy as Young Velma
* Scott McNeil as the Evil Masked Figure
* Kevin Durand as the Black Knight Ghost
* C. Ernst Harth as Miner 49er
* Christopher R. Sumpton as the Zombie
* Calum Worthy as Kid on bike
* Jackson Rathbone as man in club
* Brandon Jay McLaren as Skater Dude #2

===Voices===
* Neil Fanning as Scooby-Doo (character)|Scooby-Doo, the main protagonist and titular character.
* Dee Bradley Baker as the 10,000 Volt Ghost / Zombie / Red Eye Skeleton
* J. P. Manoux as Scooby Braniac
* Bob Papenbrook as the Black Knight Ghost Michael J. Sorich as the Tar Monster / Cotton Candy Glob
* Terrence Stone as the 10,000 Volt Ghost
* Wally Wingert as the Green Eye Skeleton

===Cameos=== Pat OBrien The Tasmanian Devil
* Ruben Studdard

==Reception==
===Box office===
The film opened March 26, 2004 and grossed $29,438,331
(over 3,312 theaters, $8,888 average) on its opening weekend, ranking #1.  It grossed a total of $84,216,833 in North America, and went on to earn $181,466,833 worldwide, more than $90 million less than the $275,650,703 worldwide Scooby-Doo (film)|Scooby-Doo grossed two years earlier. It was the 28th most successful film of 2004,  and currently ranks as the 6th highest grossing film featuring a dog as a major character. 

===Critical response===
Scooby-Doo 2: Monsters Unleashed was met with negative reviews from critics, though it is said to be closer to the franchise than the first movie. On Rotten Tomatoes, the film holds a rating of 21%, based on 114 reviews, with an average rating of 4.2/10. The sites consensus reads, "Only the very young will get the most out of this silly trifle."  On Metacritic, the film has a score of 34 out of 100, based on 28 critics, indicating "generally unfavorable reviews". 

  Razzie Award Worst Remake or Sequel. 

==Home media release==
The film was released on DVD and VHS on September 14, 2004. The DVD included deleted scenes from the films production and other features such as a making of and trailers. Warner Brothers later announced a release including both the film and its predecessor as a double feature Blu-ray Disc|Blu-ray, which was released on November 9, 2010. 

==Monsters==
The film featured a number of monsters from the classic Scooby Doo cartoons. Such as the Black Knight Ghost, The Zombie, Captain Cutler, and Miner 49er were portrayed in costumes with CGI effects, whilst the rest were completely made from CGI animation. Some of the monsters look entirely different from their appearances in the cartoons, although this was done for realism. Also, some of the characters who posed as the monsters in the movie are not the same characters from the series. The monsters in the film were:

* The Evil Masked Figure (film)- The main villain of the film and Dr. Jonathan Jacobo in disguise. He makes a bad name of Mystery, Inc. with his Heather Jasper-Howe disguise and aims to take over Coolsville with his army of monsters.
* The Pterodactyl Ghost (The Scooby-Doo Show episode "Hang In There, Scooby-Doo") - A ghostly pterodactyl who stole the other monster costumes for their transformations. He is the first monster created by The Evil Masked Figure (also known as Jonathan Jacobo who was the original Pterodactyl Ghost in the cartoon) as it was his original monster costume. The supposed origin of this Pterodactyl ghost is very different from the original ghost from The Scooby-Doo Show, which was merely a pawn in a music pirating operation. He is defeated when he accidentally collides with The Tar Monster and gets stuck in his body.
* The Black Knight Ghost (Scooby-Doo, Where Are You? episode "What A Night For A Knight") - The first monster Mystery Inc. faced. A haunted suit of armor, the Black Knight Ghost attacked the museum to steal the costumes for their transformations. He was the second monster to be brought to life, and acts as a lieutenant and second-in-command to The Evil Masked Figure. His weapon of choice is shown to be his sword, which he can control without even holding it, as well as a lance. He and The 10,000 Volt Ghost are beaten by Fred and Daphne when they use jumper cables to send the Volt Ghosts energy through his body therefore blowing it up. However it is shown that this does not completely defeat him.
* The 10,000 Volt Ghost (The Scooby-Doo Show episode "Watta Shocking Ghost") - A ghost made of electrical energy who haunted a power plant as a ghost of an old worker. He is the most dangerous and most powerful of the monsters, aside from the Tar Monster. He is the third monster brought to life and was originally supposed to help the Black Knight steal the rest of the costumes (this was shown in a deleted scene). He is defeated when Fred and Daphne use jumper cables to channel his energy through the Black Knight Ghost, causing them both to explode. Unlike the original 10,000 Volt Ghost, who was a person wearing a suit which made it look like it was made out of electricity, this version is actually made out of pure electrical energy. He is even much bigger and more obese than he was in the animated cartoon, a side-effect from his transformation.
* The Skeleton Men (The Scooby-Doo Show episode "A Creepy Tangle In The Bermuda Triangle") - A pair of one-eyed skeletons. The two that are used are a comical duo, capable of reforming their bodies into different shapes. They are the fourth and fifth monsters to be brought back to life and were originally created to guard the Monster Hive. The red-eyed one is the leader and is very smart and focused on the mission while the green-eyed one is more cowardly, dim-witted and spaced out. They are defeated twice; first by being crushed by a vent system whilst trying to pursue Velma and then destroyed by Scooby when he plugs in the triangle. They only difference is that in the episode there were three.
* The Zombie (Scooby-Doo, Where Are You? episode "Which Witch Is Which?") - A green-skinned, boggle-eyed zombie who spews slime over innocent victims, including foolish news reporters (to which Scooby laughs at but is silenced by Shaggy.) He was accidentally brought to life by Scooby and Shaggy and was the first new-born monster. He is the second dumbest and cowardly monster (the other being the green eyed skeleton man) and a reckless driver as shown when Mystery Inc. are returning to the mining town. He was destroyed when Scooby inserted the new control panel.
* Captain Cutlers Ghost (Scooby-Doo, Where Are You? episode "A Clue For Scooby-Doo") - The ghost of a deep sea scuba diver|diver, who has a glowing helmet. His weapon of choice is a harpoon launcher, which he could be seen using twice in the film, first to attack Fred, and then to pull the Mystery Machine. He was accidentally brought to life by Scooby and Shaggy and the second new-born monster. He is defeated when Fred reverses the Mystery Machine into him, sending him flying into the swamp opposite Mystery Inc.s old clubhouse, making him the 1st monster to get defeated. His helmet is later salvaged during the ending by Coolsville news-reporters who ask Shaggy to wear it which he does. Scooby then attacks him until he takes the helmet off.
* The Miner Forty-Niner (Scooby-Doo, Where Are You? episode "Mine Your Own Business") - A gargantuan miner whos slightly different from his original appearance (being more obese). His weapon of choice (unlike before) is a pickaxe, although he does possess the ability to breathe fire which he prefers to use more often. He only says one line, "Ill get you, ya varmints!", to Shaggy and Scooby. He was accidentally brought to life by Scooby and Shaggy and the third new-born monster. Hes also defeated twice in the film first when he tries to kill Shaggy and Scooby, only to have his fire repelled back at him when Scooby farts at him, and then after he recovers he is destroyed in the final battle.
* The Tar Monster (The Scooby-Doo Show episode "The Tar Monster") - A massive bulk of tar with one eye, and the new 2nd-in-command after the Black Knights defeat. A smart and formidable opponent who can ensnare multiple victims in his endless form. Unlike the original Tar Monster, he has a pupil and speaks. He is one of the most powerful monsters, the other being the 10,000 Volt Ghost. He was accidentally brought to life by Scooby and Shaggy and the fourth new-born monster. He is defeated when Scooby freezes him with a fire extinguisher. He is then defeated again when Scooby places The Monster Control Panel back into the machine.
* The Cotton Candy Glob - The Cotton Candy Glob is loosely based on The "Green Globs" from  (The New Scooby Doo Movies episode "The Haunted Candy Factory"). In the movie, it is a large pink monster made from cotton candy that is eaten by Shaggy and Scooby-Doo. He was possibly brought to life to torture Shaggy and Scooby, rather than knowing they would eat it. He is the last monster to be created. This is also the only monster that Scooby and Shaggy are not afraid of (and the only one to get eaten.)

There were other monsters that made cameos, such as the Ghost Clown, the Giggling Green Ghost,  Redbeards Ghost, the Creeper, the Headless Horseman, the Ghost of Merlin, the Rambling Ghost, the Spooky Space Kook, the Mermaids Ghost, the Ghost of Dr. Coffin, Zen Tuo, the Phantom of Milo Booth, Ozark Witch, Viking Ghost, Wax Phantom, Ghost of Finnyan McDuff, Swamp Monster, Anthos the Warlock, Giant Mantis Creature and Chickenstein, but these monsters werent brought to life by the Evil Masked Figure or by Shaggy and Scooby by accident. Redbeard was seen when Fred, Velma and Daphne were looking at the Evil Masked Figure and the Creeper was the costume Scooby and Shaggy were with when the Pterodactyl Ghost was rampaging.

==Soundtrack==
A soundtrack  was released on March 23, 2004.
 the titular theme song)
# "You Get What You Give" by New Radicals
# "Boom Shack-A-Lak" by Apache Indian
# "Thank You (Falettinme Be Mice Elf Agin)" by Big Brovaz
# "The Rockafeller Skank" by Fatboy Slim
# "Wooly Bully" by Bad Manners
# "Shining Star" by Ruben Studdard
# "Flagpole Sitta" by Harvey Danger
# "Get Ready for This" by 2 Unlimited Wild Cherry
# "Here We Go" by Bowling for Soup
# "Love Shack" by The B-52s
# "Friends Forever" by Puffy AmiYumi
# "Wanted Dead or Alive" by Bon Jovi

==Deleted scenes==
* This takes place when the gang infiltrates Wickles Manor. Shaggy and Scooby are looking around in a hallway. Shaggy points out that he and Scooby both have fleas through a magnifying glass. Shaggy points out they need to split up to double their chances. When Shaggy goes to another hallway, Scooby moves upward and bites his tail because of his dog-like instincts.
* This takes place after Fred, Velma, Daphne and Patrick get into the Mystery Machine to go to the museum. At the museum, two of the security guards there are playing with static electricity jokes. While they are having their game, the 10,000 Volt Ghost, now turned real by the Evil Masked Figure, emerges from an electrical outlet and electrocutes the museum security system, letting the Black Knight Ghost in. As the guards were about to give each other the "ultimate shock", the 10,000 Volt Ghost puts its finger in between and shocks them and making them fly across the room, with one of the guards landing on the Cotton Candy Glob costumes arms. The Black Knight then talks to the inanimate costumes and tells them that the Evil Masked Figure will give them the power to destroy Mystery, Inc. This scene was mentioned in the film, but the 10,000 Volt Ghost was replaced with the Pterodactyl Ghost that helped the Black Knight instead. But it is possible that the 10,000 Volt Ghost left after immobilizing the guards and was unnoticed while the Pterodactyl Ghost came in afterwards and helped steal the costumes.
* This takes place after Scooby and Shaggy enter the Faux Ghost in their disguises. The man who figured out Shaggys true identity later in the movie walks up and tells the sleuths the Faux Ghost is a private club for people dressing as a terrifying creature and scaring people. Shaggy and Scooby save themselves from their cover being blown by saying they are the "world-famous Westside Pickleaculas", where they are "50% pickle, 50% Dracula, 100% terrifying." The man responds by telling them he was the Cotton Candy Glob. The scene then goes to where the inhabitants show their intense hatred for Mystery, Inc. through their slightly awkward games, with the gang being modeled into a game of Whack-a-Mole, and a darts game. Shaggy then worries what the urinals look like.
* This takes place when Velma suspects Patrick of being the Evil Masked Figure. Scooby then does an act of what Patrick was acting like...or what he was sort of acting like, which seemed to scare Shaggy as well.
* This is where the Skeleton Men chase Scooby down the hill. The whole scene was in Computer-generated imagery|CGI. Scooby uses a tree branch to get behind the Skeleton Men, who crash into a tree. Scooby throws a bone out of his mouth rather than the Skeletons head. The Skeleton Man then forms into a bird-like creature with its hands, head and ankles and feet together. It jumps to get help, but breaks into pieces again.
* This takes place after Velma leaves to distract the Skeleton Men so Shaggy and Scooby could get past. The Skeleton Men are more modified in this scene rather than the other deleted one. The red-eyed skeleton has been modeled into a machine gun-like creature and chase after Velma. The green eyed one shoots Velma with the skeleton machine gun, and covers every inch of her body with skeleton bullets, narrowly escaping.
* This takes place before Scooby and Shaggy encounter the Miner 49er. They both hide, and feel this is the end for them. To give the movie a touching moment, Scooby and Shaggy profess their love for each other. They then head to the Monster Hive to install the control panel, but encounter the Miner 49er who blocks their path.
*Some deleted scenes are not shown. One was a whole storyline about Daphne and Fred. Fred gives Daphne an engagement ring, but later, when fixing the monster controller, they need to melt it down because the gold is needed to complete it.

==Cancelled sequel== green light for production of a sequel. Writers Dan Forman and Paul Foley were hired by WB to write the script for Scooby-Doo 3.  However, after the release of Scooby-Doo 2, Warner Bros. felt the film should have made more money, which prevented the production of another sequel. During a press conference for the release of Without a Paddle, Matthew Lillard stated that the third film had been cancelled and was not going to go ahead due to the second film being less successful than expected. 

==Prequels==

Scooby-Doo! The Mystery Begins was aired by Cartoon Network on September 13, 2009, the 40th anniversary of Scooby-Doo. It was released on DVD and Blu-ray on September 22. Directed by Brian Levant. The plot is an origin story for the Mystery, Inc. gang, portraying the beginning of everything: how the gang met, their first mystery, their lives at school and how they got the Mystery Machine.
 the previous film. In this film, the Mystery, Inc. gang is heading towards a beach club owned by Daphnes uncle, for temporary summer jobs. While involved with their tasks, they stumble on a new mystery.

==Reboot==
On June 17, 2014, Warner Brothers studio announced that they will restart the film series with Randall Green writing a new movie.

==See also==
*List of ghost films

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 