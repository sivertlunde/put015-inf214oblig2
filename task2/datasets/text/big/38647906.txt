Jerusalem Countdown (film)

{{Infobox film
| name           = Jerusalem Countdown
| image          = 
| alt            = 
| caption        = 
| director       = Harold Cronk
| producer       = {{Plainlist|
* James Chankin
* Michael Scott
* Matthew Tailford
* David A.R. White}}
| writer         = 
| screenplay     = {{Plainlist|
* Harold Cronk
* Matthew Tailford}}
| story          = {{Plainlist|
* David Harris
* David Michael Phelps
* David A. R. White}}
| based on       =  
| starring       = {{Plainlist|
* David A. R. White
* Anna Zielinski
* Lee Majors
* Jaci Velasquez
* Stacy Keach
* Randy Travis}}
| music          = Jeehun Hwang
| cinematography = 
| editing        = Vance Null
| studio         = {{plainlist|
* 10 West Studios
* God & Country Entertainment
* Pure Flix Productions}}
| distributor    = Pure Flix Entertainment
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Jerusalem Countdown is a 2011 film adapted from the best-selling novel, Jerusalem Countdown, written by John Hagee.

== Plot ==
In this end of times thriller, seven backpack nukes, code named, The Seven Wonders, have been dispatched throughout the United States by terrorists. A battle for Jerusalem has begun and Israels main ally, America is targeted for destruction.  Senior FBI Agent, Shane Daughtry (David A. R. White) and agent Eve Rearden (Anna Zielinski), must find these weapons before they can be detonated. Time is of the essence, useful information is coming from Arlin Rockwell (Lee Majors), a dysfunctional arms dealer who smuggled the weapons, Jackson (Stacy Keach), a retired FBI agent, and Jack Thompson (Randy Travis), a rigid CIA Deputy Director.

== Cast ==
{{columns-list|2|
* David A. R. White as Shane Daughtry
* Anna Zielinski as Eve Rearden
* Randy Travis as Jack Thompson
* Lee Majors as Arlin Rockwell
* Jaci Velasquez as Angie
* Matthew Tailford as Nick Tanner
* Nick Jameson as Matthew Dean
* Marco Khan as Javad
* John Gilbert as Malikov
* Stacy Keach as Jackson
* Jamie Nieto as Russ Walters
* Michelle Peltz as Daughtry
* Johnny Russell as Sgt. Amara
* Cole Schaefer as FBI Agent as Brian Fisher
* Geneva Somers as Sheila Tanner
}}

== Soundtrack ==
The soundtrack was written by Jeehun Hwang. 

{{Track listing
| collapsed       = 
| headline        = Songs in Jerusalem Countdown
| extra_column    = Performer(s)

| writing_credits = yes

| title1          = Find My Way
| writer1         = Eric Schmidt, Michael Sylvester,   David Johnson and Matthew Johnson
| extra1          = Eric Schmidt, Michael Sylvester,   David Johnson and Matthew Johnson

| title2          = Hope in You
| writer2         = Erin Katlin and Brendan Andersen
| extra2          = Erin Katlin
}}

== Reception ==

Several reviewers complained about character development that leads nowhere and excessive attention lavished on newscasts pertaining to negotiations in Israel, the importance of these issues characters might be revisited in a sequel.  Kauffman describes these side stories as, "...the other two legs of this filmic trinity."  However, Kauffman does note that the shared history between the characters, Shane and Eve, are is not adequately explained.  Conversely, Victor Medinas review for cinelix mentions an additional nine cut scenes from the Blu-ray disc, that would have provided more character development, but would have also slowed down the action. 

== References ==
 
{{Reflist | refs =

   

   

   

}}

== External links ==
*  
*  
*  
*  

 
 
 
 
 
<!-- 
 -->
 
 