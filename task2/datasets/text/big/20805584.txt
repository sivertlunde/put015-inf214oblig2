Shifty (film)
{{Infobox Film
| name           = Shifty
| image          = Shifty poster.jpg
| caption        = Theatrical release poster
| director       = Eran Creevy
| producer       = Rory Aitken Ben Pugh
| writer         = Eran Creevy
| starring       = Riz Ahmed Daniel Mays Jason Flemyng Nitin Ganatra Francesca Annis
| music          = Molly Nyman  Harry Escott  Ben Drew
| studio         = Between the Eyes Film London Microwave
| distributor    = Metrodome Distribution
| released       =  
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| gross          = £244,579 
}}

Shifty is a British urban thriller, written and directed by Eran Creevy. Set on the outskirts of London, it follows themes of friendship and loyalty over the course of twenty-four hours in the life of a young drug dealer, the charismatic Shifty. Shifty was filmed predominantly in Borehamwood, Hertfordshire, the home of Elstree Studios. Based on Eran Creevys teenage experiences, and boasting convincing performances from a cast of rising stars, as well as veteran actors Jason Flemyng and Francesca Annis, the film was funded by Film Londons Microwave scheme  and delivered after a shooting schedule of just eighteen days. 

==Plot==
After four years, Chris returns to where he grew up, it seems at first to attend a party, but his real motive is to catch up with his old buddy, Shifty who he left behind. Whilst away, Chris has settled into responsible adult life, with a mortgage and a good job, but he is shocked to discover that Shifty has been dealing cocaine for some time, supplied by the double dealing Glen.

Although happy to see him, Shifty has not fully forgiven Chris for leaving in the first place, and it is soon learned that the circumstances under which Chris left are more complicated than they first seemed. They spend the next 24 hours together, with Chris watching Shifty as he deals to a variety of increasingly desperate customers from the community.

Over the course of this day they are forced to confront the ghosts from the past that drove Chris away and the desperate and dangerous present that Shifty finds himself in, whilst re-discovering their friendship. Chris is once again given an opportunity to prove his loyalty to Shifty and to try ultimately and save Shifty from himself.

==Cast==
* Riz Ahmed as Shifty — a local drug dealer who gets torn between street crime and what could be different
* Daniel Mays as Chris — an old friend of Shiftys who spends a day with him, but is forced to face up to why he left his hometown
* Jason Flemyng as Glen — the middle man for the drug dealers, he also supplies for Shifty
* Nitin Ganatra as Rez — Shiftys brother, who takes Shifty in when his family kick him out, has issues with Shiftys chosen lifestyle
* Heronimo Sehmi as Ronnie - Shiftys father, who kicks him out because he does not agree with his lifestyle choices
* Francesca Annis as Valerie
* Jay Simpson (actor) as Trevor
* Dannielle Brent as Jasmine
* Kate Groombridge as Loretta
* Alice Cutter as Tasha
* Ben Drew as Drug Dealer (Uncredited)

==Soundtrack==
{{Infobox album  
| Name        = Shifty Original Motion Picture Soundtrack
| Type        = soundtrack
| Artist      = Molly Nyman and Harry Escott
| Cover       = Shiftyalbumcover.jpg
| Released    = 20 April 2009 
| Recorded    = 2008-2009
| Genre       = Urban contemporary|Urban, Hip-hop, Rap
| Length      = 34:09
| Label       = Silva Screen Records  (SILCD1286) 
| Producer    = Molly Nyman and Harry Escott, Ben Drew
}}

The soundtrack to the film was composed by Molly Nyman, Harry Escott and performed by The Samphire Band. It was nominated for Best Technical Achievement at the British Independent Film Awards 2008.  The score was performed live in Film & Music Arena at the Latitude Festival in 2009.  All tracks performed by The Samphire Band, except where stated.

===Tracklisting===
# "Shifty Theme"
# "Busting My Ghaand"
# "Charming Glen"
# "Why Am I Running?"
# "CataclysMic" (performed by DJ Trax & Rich Beggar) 
# "Swings"
# "Spilling the Various"
# "Blares House"
# "Tough Call"
# "Good Boy"
# "Look At You"
# "Night Watch"
# "Leave it All Behind"
# "Play the Tape" Sway & Plan B)

=="Shifty" - Single==
{{Infobox single  
| Name           = Shifty
| Cover          = Shifty single.jpg Sway & Plan B
| Album          =   
| Released       = May 11, 2009 Digital download
| Recorded       = 2009 Hip hop
| Length         = 3:36
| Label          = True Tiger Recordings  (TTR 018)  Derek Safo, Ben Drew, Rashid El-Alami, Paul Flynn 
| Producer       = True Tiger
| Misc           = 
{{Extra chronology
  | Artist      = Riz MC
  | Type        = single
  | Last album  = "Radar"  (2008)
  | This album  = "Shifty"  (2009)
  | Next album  = "Dont Sleep"  (2009)
  }}
{{Extra chronology Sway
  | Type        = single
  | Last album  = "Two Fingers"  (2009)
  | This album  = "Shifty"  (2009)
  | Next album  = "Mercedes-Benz"  (2009)
  }}
{{Extra chronology Plan B
  | Type        = single
  | Last album  = "Pieces (Chase & Status song)|Pieces"  (2008)
  | This album  = "Shifty"  (2009)
  | Next album  = "End Credits"  (2009)
  }}
}}
 British rappers Sway and Plan B. Released on May 11, 2009 on True Tiger Recordings, the single did not chart. The music video, also directed by Eran Creevy, was filmed in the same locations as the film and features Riz MC, Sway and Plan B each rapping along with footage taken from the film. Riz MC, Sway and Plan B performed the song live on 24 April 2009 at Bar Rumba, London for the films launch party. 

* Digital download and CD single 
# "Shifty" - 3:36
# "Shifty" (Sukh Knight Remix) - 4:53

* 12" vinyl
# "Shifty" (Sukh Knight Remix) - 4:53
# "Shifty" (Scandalous United Remix) - 6:26
# "Shifty" - 3:36

==Reception==
The film has one of the highest overall ratings amongst thousands of films on Rotten Tomatoes, with a total of 96% fresh reviews. Many claimed that despite a very low budget, the films actors and storyline was of the highest calibre, and therefore was a well formed, character-driven debut for Creevy.  However, customers on Amazon.co.uk only gave an average review of 3.5 stars, with some claiming that the films low budget left it with a dialogue-driven production with little or no action to add to proceedings. 

==Award nominations==
*Best Achievement in Production
*Best Actor (Riz Ahmed)
*Best Supporting Actor (Daniel Mays)
*Best Technical Achievement (Harry Escott, Molly Nyman)
*Douglas Hickox Award (Eran Creevy)
*Bronze Horse (Eran Creevy)

==References==
 
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 