To minutter for sent
{{Infobox film
| name           = To minutter for sent
| image          = 
| caption        = 
| director       = Torben Anton Svendsen
| producer       = John Hilbert
| writer         = Peer Guldbrandsen
| starring       = Poul Reichhardt
| music          = 
| cinematography = Verner Jensen Jørgen Skov
| editing        = 
| distributor    = Nordisk Film
| released       =  
| runtime        = 98 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

To minutter for sent is a 1952 Danish crime film directed by Torben Anton Svendsen and starring Poul Reichhardt.

==Cast==
* Poul Reichhardt - Max Paduan
* Grethe Thordahl - Grete Paduan
* Astrid Villaume - Beth
* Erik Mørk - Urmager Jacobsen
* Johannes Meyer - Vicevært Johansen
* Gunnar Lauring - Kriminalkommissær Normann
* Louis Miehe-Renard - Journalist Ib Normann
* Jeanne Darville - Sara Klint
* Poul Müller - Antikvarboghandler Rosenblad
* Per Buckhøj - Kriminalassistenten
* Bjørn Puggaard-Müller - Tjener
* Karl Stegger - Betjent
* Arne Westermann

==External links==
* 

 
 
 
 
 
 
 