Adam's Apple (film)
{{Infobox film
| name           = Adams Apple
| image          =
| caption        =
| director       = Tim Whelan
| producer       = 
| writer         = Rex Taylor   Tim Whelan
| starring       = Monty Banks   Gillian Dean   Lena Halliday   Judy Kelly
| music          =
| cinematography = René Guissart (cinematographer)|René Guissart   George Pocknall
| editing        = 
| studio         = British International Pictures
| distributor    = Wardour Films
| released       = 3 September 1928
| runtime        = 85 minutes
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent comedy film directed by Tim Whelan and starring Monty Banks, Lena Halliday and Judy Kelly. An American on his honeymoon in Paris, organises the kidnapping of his interfering mother-in-law. It was made by British International Pictures at their Elstree Studios. 

==Cast==
* Monty Banks as Monty Adams
* Gillian Dean as Ruth Appleby
* Lena Halliday as Mrs. Appleby
* Judy Kelly as Vamp Colin Kenny as Husband
* Dino Galvani as Crook
* Hal Gordon as Drunk
* Charles OShaughnessy as Official

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.
* Wood, Linda. British Films 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 