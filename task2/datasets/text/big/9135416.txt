Barney's Magical Musical Adventure
 
 
{{Infobox film
| name           = Barneys Magical Musical Adventure
| image          = Barneys Magical Musical Adventure.jpg
| alt            =
| director       = Jim Rowley
| producer       = 
| writer         = Stephen White
| starring       = Bob West Julie Johnson
| music          = Phil Parker
| cinematography =
| editing        = McKee Smith
| studio         =
| distributor    =
| released       =  
| runtime        = 40 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Actimates Barney.

==Plot==
Barneys friends are playing in Dereks backyard building a sand castle, after which Barney appears and takes them to a magical forest. They meet an elf named Twinkle, who likes to play games and make new friends. She shows them the road to the castle where they ride pretend horses and meet a real king. After taking care of the castle while the king goes fishing, the kids are made princes and princesses of the kingdom.

==Cast==
* Barney (voice) - Bob West
* Barney (costume) - David Joyner
* Baby Bop (voice) - Julie Johnson 
* Baby Bop (costume) - Jenny Dempsey
* Michael - Brian Eppes
* Tina - Jessica Zucha
* Min - Pia Manalo
* Derek - Rickey Carter
* Twinkle the Elf - Margaret Pyeatt
* Royal King - Rick Wetzel

==Songs==
# Barney Theme Song (Tune: Yankee Doodle)
# The Noble Duke of York (Tune: The Grand Old Duke of York)
# Castles So High
# Castles So High (Reprise)
# Silly Sounds
# Looby-loo
# Go Round and Round the Village
# If I Had One Wish (Tune: Polly Wolly Doodle)
# Old King Cole
# Polly Put the Kettle On
# Little Jack Horner
# The Muffin Man
# Pat-a-cake, pat-a-cake, bakers man
# Pease Porridge Hot
# Sing a Song of Sixpence
# I am a Fine Musician
# Its Good to be Home (Tune: Castles So High)
# I Love You (Tune: This Old Man)

 

 
 
 
 

 