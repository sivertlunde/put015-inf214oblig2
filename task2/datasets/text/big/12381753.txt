The Big Stampede
 
{{Infobox film
| name           = The Big Stampede
| image          = Poster of the movie The Big Stampede.jpg
| caption        = Film poster
| director       = Tenny Wright
| producer       = {{plain list|
* Sid Rogell
* Leon Schlesinger
}}
| writer         = {{plain list|
* Marion Jackson
* Kurt Kempler
}}
| starring       = {{plain list|
* John Wayne
* Noah Beery Paul Hurst
}}
| cinematography = Harry Fischbeck
| music          = Bernhard Kaun
| editing        = Frank Ware
| distributor         = Warner Bros. Pictures
| released       =  
| runtime        = 54 minutes
| country        = United States
| language       = English
}}

The Big Stampede is a 1932 American film starring John Wayne. It is a remake of the 1927 film Land Beyond the Law.

==Cast==
* John Wayne as Deputy Sheriff John Steele
* Noah Beery as Sam Crew Paul Hurst as "Arizona" Frank Bailey
* Mae Madison as Ginger Malloy
* Luis Alberni as Sonora Joe
* Berton Churchill as Governor Lew Wallace
* Sherwood Bailey as Pat Malloy
* Lafe McKee as Cal Brett
* Joseph W. Girard as Major Parker

==References==
 	

==See also==
* John Wayne filmography
* List of American films of 1932

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 