Drango
 
{{Infobox film
| name           = Drango
| image          = Drangpos.jpg
| image_size     = 
| alt            = 
| caption        = Film poster
| director       = Hall Bartlett Jules Bricken Jeff Chandler
| writer         = Hall Bartlett Jeff Chandler Ronald Howard Milburn Stone
| music          = Elmer Bernstein
| cinematography = James Wong Howe
| editing        = Leon Selditz
| studio         = Earlmar Productions Hall Bartlett Productions
| distributor    = United Artists
| released       = January 1957
| runtime        = 92 minutes
| country        = United States English
}}
 American motion Jeff Chandlers Ronald Howard, Georgia town in the months immediately following the American Civil War, the film depicts the efforts of a resolute Union Army officer who had participated in the destruction of the town during Shermans March to the Sea|Shermans March determined to heal the land he had previously harmed.

==Plot==

Union officers Major Drango and Captain Banning ride into a Georgia town ravaged by the Civil War and still bitter about the lives and property lost. Drango is the new military governor, but townspeople including Judge Allen and his son Clay make it clear that these Yankees are not welcome.

A local man seen as disloyal to the Confederacy is lynched. The mans daughter, Kate Calder, blames Drango for letting it happen.

Drango attempts to bring the men responsible to justice, but wealthy Shelby Ransom harbors the fugitives, including Clay, her lover. Union colonel Bracken finds fault with Drango for not being tough enough, so he confiscates the towns food supply and rations it. Clays men stage a raid.

A doctor and newspaper editor offer Drango their support in restoring order. The newspaper office is set ablaze and the editors young son is accidentally killed. Kate now sides with Drango, and an angered Shelby tries to order Clay from her home, but he slaps her and makes her lure Capt. Banning to an ambush.

Now even the judge is appalled by Clays unlawful acts. He warns his son this must stop. Clay refuses to listen and shoots Drango, wounding him. He is about to kill Drango when a bullet from his father ends Clays life, restoring law and order to the town.

==Cast== Jeff Chandler as Major Clint Drango 
*Joanne Dru as Kate Calder 
*Julie London as Shelby Ransom 
*Donald Crisp as Judge Allen  Ronald Howard as Clay Allen 
*John Lupton as Capt. Marc Banning 
*Walter Sande as Dr. Blair 
*Milburn Stone as Col. Bracken 
*Morris Ankrum as Henry Calder 
*Parley Baer as George Randolph, Daily Herold 
*Damian OFlynn as Gareth Blackford 
*Barney Phillips as Rev. Giles Cameron  Charles Horvath as Ragan 
*Katherine Warren as Mrs. Scott 
*Chubby Johnson as Zeb 
*David Stollery as Jeb Bryant 
*Edith Evanson as Mrs. Blackford 
*Anthony Jochim as Stryker the School Teacher 
*Amzie Strickland as Mrs. George Randolph 
*Mimi Gibson as Ellen Bryant  Helen Wallace as Mrs. Allen 
*Paul Lukather as Burke 
*Bing Russell as Lieutenant with Supply Wagon 
*Chuck Webster as Boy with Chicken 
*David Saber as Tommy Randolph 
*Phil Chambers as Luke  James Murphy as Bartender 
*Rex Allen as Himself - Singer of Title Song (voice)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 