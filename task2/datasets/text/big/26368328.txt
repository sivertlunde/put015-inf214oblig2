The Hit List (2011 film)
{{Infobox film
| name           = The Hit List
| image          = The Hit List.jpg
| alt            =  
| caption        = DVD cover
| director       = William Kaufman
| producer       =  
| writer         =  
| starring       =  
| music          = Deane Ogden
| cinematography = Mark Rutledge
| editing        = Jason A. Payne
| studio         = Motion Picture Corporation of America North by Northwest Entertainment Stage 6 Films
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $6 million
}}
The Hit List is a 2011 American action film written by Chad and Evan Law, and directed by William Kaufman, and starring Cuba Gooding Jr. and Cole Hauser. The film was released on direct-to-video|direct-to-DVD in the United States on May 10, 2011.

==Plot==
Allan Campbell (Cole Hauser), a man who has had a very bad day, goes to a bar to drown his sorrows. He drunkenly befriends a mysterious man who calls himself Jonas Arbor (Cuba Gooding Jr.), revealing to him a list of five people he wishes were dead. But as the bodies start piling up, and with a detective (Jonathan LaPaglia) hot on his trail, Allan, no longer believing the events to be a practical joke, must set out to end the murders before its too late.

==Cast==
* Cuba Gooding Jr. as Jonas Arbor
* Cole Hauser as Allan Campbell
* Jonathan LaPaglia as Detective Neil McKay
* Ginny Weirick as Sydney Campbell
* Sean Cook as Brian Felzner
* Drew Waters as Mike Dodd
* Matt Beckham as Agent Drake Ford
* Brandon ONeill as Dom Estacado
* J.P. OShaughnessy as Lt. Ben Harp
* David Andriole as Detective Ray Lowery
* Brandon Messenger as Agent Cole Baldridge

==Production==
Actor Christian Slater, who also starred with Cuba Gooding, Jr. in Lies & Illusions as well as Sacrifice, was originally rumored to play the part of Allan Campbell. Slater co-starred with Hauser in the film Shadows of the White Nights.
 End Game, and Wrong Turn at Tahoe produced.

Director William Kaufman of the 2005 indie action thriller The Prodigy was chosen to direct.
 Washington in early 2010. The local police in Spokane refused to officially participate in the films production because of the films depiction of violence toward police officers. This is due to the Lakewood, Washington police officer shooting two months before principal photography began, in which four officers were murdered.

==Music==

The original music score for the film was composed and conducted by composer Deane Ogden .

* "Honky Tonk Barstool"
Written by Zach Selwyn 
Performed by Zachariah and the Lobos Riders 
Courtesy of Papago Records.

* "47 Ways To Die"
Written by Steve Blaze 
Performed by Lillian Axe 
From the album "Deep Red Shadows"

* "One More Time" 
Written by John Riven 
Performed by John Riven

==Home media== Region 2 in the United Kingdom on 9 May 2011, and also Region 1 in the United States on May 10, 2011, it was distributed by Sony Pictures Home Entertainment.

==References==
*  
* http://www.canmag.com/nw/15440-gooding-jr-hauser-hit-list

==Related==
* http://www.imfdb.org/wiki/The_Hit_List (Firearms used in the movie.)

 
 
 
 
 
 
 
 
 
 
 
 
 