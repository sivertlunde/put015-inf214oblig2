Pelli (film)
{{Infobox film
| name = Pelli
| image =
| caption =
| image_size =
| director = Kodi Ramakrishna
| producer = Rama Lingeshwara Rao 
| writer =
| starring = Vadde Naveen Maheswari Prithviraj
| music =S. A. Rajkumar
| cinematography = Kodi Lakshman (as Kodi Lakshman Rao)
| editing = Tata Suresh
| studio =
| released = 1997
| country = India
| runtime =
| language = Telugu
| budget =
| gross =
}}
 Telugu film directed by Kodi Ramakrishna. The film stars Vadde Naveen, Maheswari and Prithviraj in the lead roles. 

==Story==
Naveen wants to marry the girl he likes at first sight. He sees Maheswari in a shopping mall promptly falls in love. Soon he goes on a mission in search of her only to find her in a colony surrounded by comedians and a lonely sad, pathetic looking Sujatha as her mother. But Sujatha is mother-in-law of Maheswari and Prithviraj her son. Both Sujatha and Maheshwari think that he is dead. Meanwhile Naveen and Maheshwari fall in love with each other accompanied by their neighbors hilarious events. But later Prithvi returns and reclaim his possession. What can a good mother do to stop the evil son. Like in all those good old sentimental movies, she poisons him and kills herself too for the sake of a peaceful life for her daughter-in-law.

==Cast==
* Vadde Naveen
* Maheswari
* Prithviraj Sujatha
* Bramhanandam
* Giribabu Mallikarjuna Rao 
* Jayalalita
* Kovai Sarala
* Ananth
* Y. Vijaya

==Music==
{{Infobox album
| Name = Pelli
| Tagline =
| Type = soundtrack
| Artist = S. A. Rajkumar
| Cover =
| Released = 1997
| Recorded =
| Genre =
| Length = 25:14
| Label =
| Producer = S. A. Rajkumar
| Reviews =
| Last album =
| This album = Pelli
| Next album =
}}

The music of the film was composed by S. A. Rajkumar and lyrics penned by Sirivennela Sitaramasastri. 
{| class="wikitable"
|-
! Sno !! Song title !! Singers !! Duration
|- Mano ||04:23
|-
| 2 || "Yavvana Veena" || S. P. Balasubrahmanyam ||04:33
|-
| 3 || "Jabilamma" ||S. P. Balasubrahmanyam ||03:53
|- Yesudas ||04:21
|- Chitra ||04:30
|-
| 6 || "Ooge Ooge " || S. P. Balasubrahmanyam, Chitra ||04:54
|}

==Awards==
;Nandi Awards Best Supporting Sujatha

==Remakes==
{| class="wikitable"
|-
! Year !! Film !! Language !! Cast !!  Director 
|- Kannada || Ramesh Aravind || V. Umakanth 
|- Tamil || Simran || Rajkapoor
|- Hindi || Aftab Shivdasani|Aftab, Esha Deol || Vinay Shukla
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 