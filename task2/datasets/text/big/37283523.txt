In Too Deep (1989 film)
 
{{Infobox film
| name           = In Too Deep
| image          = 
| image size     =
| caption        = 
| director       = John Tatoulis Colin South
| producer       =John Tatoulis Colin South
| writer         = Deb Parsons
| based on = 
| narrator       =
| starring       = Santa Press Hugo Race Rebekah Elmaloglou
| music          = 
| cinematography = 
| editing        = 
| studio = Media World
| distributor    = Home Cinema Group (video)
| released       = 1989
| runtime        = 103 mins
| country        = Australia English
| budget         = AU $800,000 
| gross = 
| preceded by    =
| followed by    =
}}
In Too Deep is a 1989 erotic thriller film.

==Production==
The film was raised through private investment including the producers own money. Two weeks into the five week shoot a major investor pulled out but they managed to complete the film. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p132-133  It was shot under the title Mack the Knife. 

John Tatoulis later stated:
 I was interested in two things in In Too Deep: one was the corruption of innocents and the other was the strengths and weaknesses of sexuality. And I wanted to set it in an urban landscape. What I was really keen to do was create a mood and a feel through a variety of ways. I believe that film is like a tapestry and all the components that go to making the texture of that tapestry are all important: sound, pictures, editing, performances, direction. If one doesnt work, then the final tapestry wont have the texture the director had in his or her mind to start with. If a film doesnt have a feeling, a feeling that has a texture to it, then its lacking. So that was something I was very keen to explore: how do I give this film a feeling of claustrophobia, a feeling of heat, a feeling of menace and vulnerability.  

==Release==
Despite the films low budget it managed to be widely seen around the world on video. 

==References==
 

==External links==
*  at IMDB

 
 
 
 
 
 


 
 