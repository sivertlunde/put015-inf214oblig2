Nicholas Nickleby (1912 film)
{{Infobox film
| name           = Nicholas Nickleby
| image          = 
| image size     = 
| caption        =  George O. Nichols
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Harry Benham Mignon Anderson
| music          = 
| cinematography = Carl Gregory
| editing        = 
| studio         = Thanhouser Company
| distributor    = Motion Picture Distributing and Sales Company
| released       =  
| runtime        = 31 minutes (some sources report 20 minutes)
| country        = United States English intertitles
| budget         = 
| gross          =
}}
 1912 American silent Short short drama George Nichols, 1839 novel of the same name.  The two-reel film stars Harry Benham in the title role and Mignon Anderson.
 David Copperfield and The Old Curiosity Shop in 1911.   

==Plot==
As a 30 minute silent production (some sources state 20 minutes), the film has to significantly compact and truncate the story of the 900 plus page novel.   Many characters are not directly identified by name, but would be familiar to those with knowledge of the novel.

Nicholas Nickleby and his mother and sister Kate arrive in London, and take up lodgings at Miss LaCreevys (a miniature painter, per the sign on her house).  Mrs. Nickleby sends a message to her brother-in-law Ralph Nickleby, a money-lender, asking his assistance in finding employment for Nicholas. Ralph visits the Nicklebys and shows Nicholas an advertisement to be a school teacher for Mr. Squeers academy.  Wackford Squeers is at the Saracen Heads Inn in London, allowing his new students nearly nothing to eat while he dines well.  Ralph introduces Nicholas to Squeers, and Nicholas then leaves London to go to Wackfords Dotheboys Hall, as Kate and Mrs. Nickelby sadly look on.

Squeers and Nickleby arrive at Dotheboys Hall, met by Smike, the broken-down sad drudge of the school.  Nicholas meets Mrs. Squeers and the couples daughter Fanny (who is immediately taken by Nicholas handsome looks), and impish son Wackford junior.  In class, Mr. Squeers treats the children badly and feeds them very little (one spoonful of something unpleasant for each).  Fanny finds an excuse to enter the classroom to see Nicholas, the object of her affection.  Smike enters after the class is dismissed and is shown kindness by Nicholas.

Th next scene returns to London, where Kate is treated insultingly by guests of Ralph at a dinner at his house.  The four quests are not identified, but are clearly Sir Mulberry Hawke, Lord Frederick Verisopht, and the similar-looking Mr. Pyke and Mr. Pluck.  Kate runs home to her mother in distress. Back at Dotheboys Hall, Smike is punished by Squeers in front of the students.  Nicholas intervenes to stop the assault, injuring Squeers.  Nicholas then leaves Dotheboys hall, trailed by behind by Smike.  They join together to return to London, where Nicholas arises Ralphs ire.

Ralph has his assistant Newman Noggs (adorned with a feather quill over his ear to denote his quirky nature in the novel) deliver an ultimatum:  that he will provide for Mrs. Nickelby and Kate if Nicholas leaves London.  Noggs delivers the message to the Nicklebys, and Nicholas and Smike then depart London to seek their fortune.

"Part II" commences, and at an inn, Nicholas and Smike meet theater manager Victor Crummles and his two young sons playing with swords, and join Crummles acting troupe.  They are then introduced to the rest of the theater company, and Mrs. Crummles.

Nickelbys success playing Romeo causes envy from another cast member, Mr. Lenville.  Lenville sends a message to Nicholas asking him to "kindly step on the stage for the purpose of having his nose pulled."  Nicholas is amused by this challenge and confronts Lenvile, and as Lenville moves in slow motion to assault him, Nicholas downs him with a single punch. Back in London, Kate implores her uncle Ralph to spare her further contact with his friends.  He refuses.  Noggs writes to Nicholas, telling him her sister needs him as once.  Nicholas receives the message and immediately departs Crummles company to many sad goodbyes.

Nicholas arrives at a dining establishment, where he comes across Ralphs friends speaking about Kate wrongly (one must presume), and confronts them.  Nicholas rushes to the carriage of the main offender, where they scuffle, and the carriage darts off madly.  Kates insulter is badly injured.  Nicholas witnesses his injury and returns home to hug his mother and sister.

The plot speeds up even more at this point.  Nicholas is shown obtaining employment at the Cheeryble Brothers firm.  Nicholas meets Madeline Bray at that establishment and is immediately taken with her.  Their friendship ripens into love.   Madelines invalid father, however, asks Madeline to marry the elderly miser Arthur Glide, which she sadly agrees to.  Glide tells Ralph of his success, Nicholas learns via Newman Noggs that Madeline will be forced into the loveless marriage.

Nicholas bursts into the wedding ceremony to disrupt the proceedings, and Madelines father dies while protesting his intrusion.  Madeline faints, and is carried off by Nicholas.  He returns her to Miss LaCreevys where she revives.   A title card then reveals that some months later, quoting Dickens, "never was such a dinner-since the world began"   The Cheerybles are showing toasting Madeline and Nicholas amongst friends, and the film ends.

==Cast==
The primary cast of the film includes:
*Harry Benham as Nicholas Nickleby
*Mignon Anderson as Madeline Bray
*Frances Gibson as Kate Nickelby
*Inda Palmer as Nicholas mother
*Justus D. Barnes as Nicholas Uncle Ralph   

==Status==
A print of the film is preserved at the British Film Institute.  In 2007, the film was included on The Thanhouser Collection: Volumes 7 8 and 9 DVD release. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 