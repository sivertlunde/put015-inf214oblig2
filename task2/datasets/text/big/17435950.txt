A Girl in Every Port (1952 film)
{{Infobox film
| name           = A Girl in Every Port
| image          = AGirlInEveyPortPoster.jpeg
| image_size     =
| caption        = Theatrical release poster
| director       = Chester Erskine
| producer       = Irwin Allen Irving Cummings, Jr.
| writer         = Frederick Hazlitt Brennan (story) Chester Erskine Marie Wilson William Bendix
| music          = Roy Webb
| cinematography = Nicholas Musuraca
| editing        = Ralph Dawson RKO Radio Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States English
| budget         = Unknown
| gross          =
}} 1952 comedy film about two sailors who buy a racehorse. It was based on the short story They Sell Sailors Elephants by Frederick Hazlitt Brennan.

==Cast==
*Groucho Marx as Benjamin Franklin Benny Linn Marie Wilson as Jane Sweet
*William Bendix as Timothy Aloysius Tim Dunnovan
*Don DeFore as Bert Sedgwick
*Gene Lockhart as Doc Garvey
*Dee Hartford as Millicent Temple
*Hanley Stafford as Fleet Admiral Temple
*Teddy Hart as High Life
*Percy Helton as Drive-In Manager
*George E. Stone as Skeezer

== References ==
 

==External links==
* 
*  
* 

 
 
 
 
 
 
 
 


 