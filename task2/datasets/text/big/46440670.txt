Frontier Badmen
{{Infobox film
| name           = Frontier Badmen
| image          =
| caption        =
| director       = Ford Beebe William McGann
| producer       = Universal Pictures
| writer         = Gerald Geraghty Morgan B. Cox
| cinematography = William Sickner
| editing        = Fred Feitshans
| distributor    = Universal Pictures
| released       = August 6, 1943
| runtime        = 73 minutes
| country        = USA
| language       = English
}}
Frontier Badmen is a 1943 romantic western directed by Ford Beebe and produced and distributed by Universal Pictures. Several members of the cast are offspring of silent screen stars. 


==Cast==
*Robert Paige - Steve Logan
*Anne Gwynne - Chris Prentice
*Noah Beery, Jr. - Jim Cardwell
*Diana Barrymore - Claire
*Leo Carillo - Chinito Galvez
*Andy Devine - Slim ; Cowhand
*Lon Chaney, Jr. - Chango
*Thomas Gomez - Ballard
*Frank Lackteen - Cherokee
*William Farnum - Dad Courtwright

==References==
 


==External links==
* 
* 

 

 
 
 
 

 