They Came to Cordura
{{Infobox film
| image	=	They Came to Cordura FilmPoster.jpeg
| caption        = 
| director       = Robert Rossen
| producer       = William Goetz
| writer         = Ivan Moffat Robert Rossen Glendon Swarthout (novel)
| narrator       = 
| starring       = Gary Cooper Rita Hayworth Van Heflin Dick York
| music          = Elie Siegmeister
| cinematography = Burnett Guffey
| editing        = William A. Lyon
| distributor    = Columbia Pictures
| released       = June 1959
| runtime        = 123 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $2.5 million (est. US/ Canada rentals) 
| preceded_by    = 
| followed_by    = 
}} 1959 Western Western film co-written and directed by Robert Rossen, starring Gary Cooper and Rita Hayworth, and featuring Van Heflin, Tab Hunter, Richard Conte, Michael Callan, and Dick York. It was based on a 1958 novel by Glendon Swarthout.

==Plot== chase after Robert Keith), who is 63 years old and impatiently yearning to be promoted to general before mandatory retirement a few months hence.

Rogers leads his regiment in an old-fashioned but poorly planned Cavalry charge on Ojos Azules, a villa owned by Adelaide Geary (Rita Hayworth) where Villas men withdrew after a victory over Mexican government troops, enjoying her hospitality. Thorn, excused from the fighting, observes through his binoculars various acts of heroism by Lt. Fowler (Tab Hunter), Sgt. Chawk (Van Heflin), Cpl. Trubee (Richard Conte) and Pvt. Renziehausen (Dick York) in defeating Villas men.

Rogers is proud of having personally led the charge, but furious when Thorn wont nominate him for a citation. Thorn insists that leading his regiment in the charge was "in the line of duty" and refuses to consider a citation for the Medal of Honor, awarded for heroism "above and beyond the call of duty." Rogers reminds Thorn that he protected him from an investigation for cowardice, which he did out of respect for Thorns father, but does not sway Thorn.

Thorn intends to recommend the four soldiers for the Medal of Honor. He is ordered to take along Mrs. Geary, who is charged with "giving aid and comfort to the enemy." A fifth soldier, a private (Michael Callan) also nominated by Thorn for a medal after an earlier battle, rides with them to the expeditions base at the Texas town of Cordura.

This seemingly simple task becomes increasingly complex as the incessant squabbling between Thorn and the men threatens to destroy them all. Eager to learn more about their acts of bravery, Thorn finds the men to be hostile toward him. A series of harrowing incidents make it clear that the apparent heroes were motivated by ambition, terror, or chance while it is the disgraced Thorn who possesses moral courage. The men soon become insubordinate ultimately turning against Thorn, forcing him to fight the soldiers to save his own life.

==Cast==
*Gary Cooper ... Maj. Thomas Thorn
*Rita Hayworth ... Adelaide Geary
*Van Heflin	... Sgt. John Chawk
*Tab Hunter	... Lt. William Fowler
*Richard Conte ... 	Cpl. Milo Trubee
*Michael Callan ... Pvt. Andrew Hetherington
*Dick York ... Pvt. Renziehausen Robert Keith ... Col. Rogers Carlos Romero ... Arreaga
*Jim Bannon	... Capt. Paltz (billed as James Bannon)
*Edward Platt ... Col. DeRose
*Maurice Jara ... 	Mexican federale
*Sam Buffington	... First Correspondent
*Arthur Hanson ... Second Correspondent

==Production ==
===Screenplay=== 3rd Infantry Division in Southern France during World War II.  This personal experience was applied to his novel.

===Filming===
Dick York suffered a severe back injury during the filming that caused him great pain in his later years, so much so that he was forced to resign from his longtime role of Darrin Stephens on the 1960s television program Bewitched, and indeed, the injury almost ended up ruining his life. 

===Music===
A film tie-in song written by Sammy Cahn and Jimmy Van Heusen was recorded by Frank Sinatra and Robert Horton. 
==Notes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 