A Very Long Engagement
{{Infobox film
| name           = A Very Long Engagement
| image          = A Very Long Engagement movie.jpg
| caption        = Theatrical release poster
| based on       =  
| screenplay     = Jean-Pierre Jeunet Guillaume Laurant
| starring       = Audrey Tautou Gaspard Ulliel Marion Cotillard Dominique Pinon Chantal Neuwirth André Dussolier Ticky Holgado Jodie Foster
| narrator       = Florence Thomassin
| director       = Jean-Pierre Jeunet
| producer       = Francis Boespflug Bill Gerber Jean-Louis Monthieux Fabienne Tsaï
| music          = Angelo Badalamenti
| cinematography = Bruno Delbonnel
| editing        = Hervé Schneid
| distributor    = Warner Independent Pictures
| released       =  
| runtime        = 133 minutes
| language       = French
| country        = France
| budget         = $56.6 million
| gross          = $70,115,868
}}
A Very Long Engagement ( ) is a 2004 French romantic war film, co-written and directed by Jean-Pierre Jeunet and starring Audrey Tautou. It is a fictional tale about a young womans desperate search for her fiancé who might have been killed during World War I. It was based on a novel of the same name, written by Sebastien Japrisot, first published in 1991.

The film was nominated for the Academy Award for Best Art Direction and Academy Award for Best Cinematography at the 77th Academy Awards|Oscars. Marion Cotillard won the César Awards|César Award for Best Supporting Actress for her performance.

==Plot== soldiers are trench lines. church near their home, MMM for Manech Aime Mathilde (Manech Loves Mathilde; a pun on the French word aime, which is pronounced like the letter "M". In the English-language version, this is changed to "Manechs Marrying Mathilde").
 French government to deal with those who tried to escape the front. She also discovers the stories of the other men who were sentenced to the no mans land as a punishment. She, with the help of a private investigator, attempts to find out what happened to her fiancé. The story is told both from the point of view of the fiancée in Paris and the French countryside—mostly Brittany—of the 1920s, and through flashbacks to the battlefield.

Eventually Mathilde finds out her fiance is alive, but he suffers from amnesia. He fails to identify even his adoptive mother.
Seeing Mathilde, Manech seems to be oblivious of her.  However, he still expresses concern for her when he notices her polio stricken legs, asking her "does it hurt when you walk ?" as he did when they first met.  At this, Mathilde sits on the garden chair silently watching Manech with tears in her eyes and smile in her lips.

==Cast==
 
* Audrey Tautou - Mathilde Donnay
* Gaspard Ulliel - Manech Langonnet, Mathildes fiancé
* Jean-Pierre Becker - Sergeant Daniel Esperanza
* Dominique Bettenfeld - Angel Bassignano
* Clovis Cornillac - Benoît Notre-Dame
* Marion Cotillard - Tina Lombardi
* Jean-Pierre Darroussin - Corporal Benjamin "Biscotte" Gordes
* Julie Depardieu - Véronique Passavant
* Jean-Claude Dreyfus - Major François Lavrouye
* André Dussollier - Pierre-Marie Rouvières
* Ticky Holgado - Germain Pire
* Tchéky Karyo - Captain Etienne Favourier
* Jérôme Kircher - Kléber "Bastoche" Bouquet
* Denis Lavant - Francis "Six-Sous" Gaignard
* Chantal Neuwirth - Bénédicte, Mathildes aunt
* Dominique Pinon - Sylvain, Mathildes uncle
* Jean-Paul Rouve - the postman
* Michel Vuillermoz - Ptit Louis
* Albert Dupontel - Célestin Poux
* Bouli Lanners - Corporal Urbain Chardolot
* Philippe Duquesne - Staff Sergeant Favart
* Stéphane Butet - Julien Phillipot
* François Levantal - Gaston Thouvenel
* Thierry Gibault - Lieutenant Benoît Etrangin
* Jodie Foster - Élodie Gordes
* Elina Löwensohn - Tina Lombardi
 

==Production==
A Very Long Engagement was filmed entirely in France over an 18-month period, with about 30 French actors, approximately 500 French technicians and more than 2,000 French extras.     Right before the films New York City and Hollywood debut, the films production company ("2003 Productions"), which is one-third owned by Warner Brothers and two-thirds owned by Warner France, was ruled an American production company by a French court, denying the studio $4.8 million in government incentives. 
 Landes department of southwest France.

==Awards and reception== Best Foreign Language Film. Marion Cotillard won the César Awards|César Award for Best Supporting Actress for her performance.
 USD and earned $70.1 million in theaters worldwide. 

 
 
*30th César Awards (France) Best Actress &ndash; Supporting Role (Marion Cotillard) Best Cinematography (Bruno Delbonnel) Best Costume Design (Madeline Fontaine) Best Production Design (Aline Bonetto)
**Won: Most Promising Actor (Gaspard Ulliel)
**Nominated: Best Actress &ndash; Leading Role (Audrey Tautou)
**Nominated: Best Director (Jean-Pierre Jeunet)
**Nominated: Best Editing (Hervé Schneid)
**Nominated: Best Film
**Nominated: Best Music (Angelo Badalamenti)
**Nominated: Best Sound (Vincent Arnardi, Gérard Hardy and Jean Umansky)
**Nominated: Best Writing (Jean-Pierre Jeunet and Guillaume Laurant)

*77th Academy Awards (USA) Best Art Direction (Aline Bonetto) Best Cinematography (Bruno Delbonnel)
  BAFTA Awards (UK) Best Film not in the English Language

*10th Critics Choice Awards (USA) Best Foreign-Language Film
 Boston Society of Film Critics Awards
**Runner-up: Best Foreign Language Film
 Chicago Film Critics Association (USA)
**Won: Best Foreign Language Film

*Dallas-Fort Worth Film Critics Association (USA)
**Won: Best Foreign Language Film
**Placed 9th:  

*Florida Film Critics Circle (USA) Best Foreign Film
 Kansas City Film Critics Circle Awards (USA)
**Won: Best Foreign Language Film

*Golden Globe Awards (USA)
**Nominated: Best Foreign Language Film
 

==See also==
*List of World War I films

==References==
 

==External links==
*    
*  
*  
*  
*  
*  


 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 