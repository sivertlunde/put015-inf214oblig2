College (2008 film)
{{Infobox Film
| name = College
| image = Collegeposter08.jpg
| caption = Theatrical Release poster
| director = Deb Hagan
| producer = Malcolm Petal Julie Dangel
| writer = Dan Callahan Adam Ellison
| starring = Drake Bell Kevin Covais Andrew Caldwell Haley Bennett Ryan Pinkston
| music = Transcenders
| cinematography = Dan Stoloff
| editing = David Codron
| distributor = MGM
| released = August 29, 2008  , ComingSoon.net Film Database. 
| runtime = 94 min.
| country = United States English
| budget = $7 million
| gross = $6,265,483 
}}
College is a 2008 comedy starring Drake Bell, Andrew Caldwell, and Kevin Covais and directed by first-time director Deb Hagan. It was released on August 29, 2008, by MGM.

==Plot== Gary Owen), and Cooper (Zach Cregger), the guys meet sorority girls Kendall (Haley Bennett), Heather (Camille Mana), and Amy (Nathalie Walker), and sparks fly. But once Teague feels threatened by Kevins new relationship with Kendall, he takes the pre-frosh humiliation to a greater level, forcing the guys to decide to fight back.

==Cast==
*Drake Bell as Kevin Brewer
*Kevin Covais as Morris Hooper
*Andrew Caldwell as Carter Scott
*Haley Bennett as Kendall Hartley
*Nick Zano as  Tony Teague Garrison Gary Owen as Brian Bearcat Miller
*Zach Cregger as Cooper Montague
*Camille Mana as Heather Gellar
*Nathalie Walker as Amy Steward
*Alona Tal as Gina Hedlund
*Ryan Pinkston as Fletcher
*Reggie Martinez as Goose
*Verne Troyer as himself
*Valentina Vaughn as herself (Penthouse (magazine)|Penthouse Pet)
*Heather Vandeven as herself (Penthouse Pet)
*Andree Moss as Ashley
*Carolyn Moss as Riley
*Wendy Talley as Kevins Mom
*Brandi Coleman as College Girl #1
*Jessica Heap as College Girl #2
*Todd Voltz as Nitrous Guy
*Melissa Lingafelt as Junior Girl
*Brandy Blake as Girl #1
*  as Girl #2
*Stephanie Honore as Gina’s Friend
*Josh Tenk as himself
*Tracy Mulholland as Mean Sorority Sister

==Production==
The film was released on Labor Day weekend August 29, 2008 in 2,123 theaters.

Many of the scenes of the actors at university are filmed on the campus of Tulane University in New Orleans and Grace King High School in Metairie on January 2007. 

==Release==

===Reception===

The film was panned by critics. On  , the film holds a score of 15 out of 100, based on 11 critics, indicating "overwhelming dislike".

===Box office===

The film was released on August 29, 2008, in 2,123 theaters. It made United States dollar|US$2.6 million over the Labor Day weekend. The film has grossed an estimated $4.7 million in America and $1.6 million in other territories for a total gross of $6.3 million worldwide.   

===DVD release===

The film was released  on Region 1 DVD,  January 27, 2009.The DVD includes both the Theatrical and Unrated versions of the film as well as a Gag Reel. 


==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 