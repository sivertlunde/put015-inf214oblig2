Chen Mo and Meiting
{{Infobox film 
|  name = Chen Mo and Meiting
|  image = Chen_Mo_and_Meiting.jpg
|  caption = DVD cover Liu Hao 
|  writer = Liu Hao
|  starring = Du Huanan Wang Lingbo
|  producer = Liu Hao Dan Dongbing
|  music = Song Ge
|  cinematography = Li Bingqiang
|  editing = Li Qing
|  distributor = Zero Film
|  released = Berlin International Film Festival|Berlin: February 15, 2002
|  runtime = 78 min.  Mandarin
|  budget =  
|  country = China
}} Liu Hao. It was Lius directorial debut and stars Du Huanan and Wang Lingbo as the titular Chen Mo and Meiting. 

The film was produced independently of the Chinese bureaucracy, primarily from funds solicited by Liu Hao himself, as well as some foreign investment from the Germany based Zero Film.   

Chen Mo and Meiting tells the story of a flower vendor and a massage parlor girl, the children of persecuted intellectuals during the Cultural Revolution, as they enter into a shy and tentative romance.

== Production ==
After writing the screenplay for Chen Mo and Meiting, Liu set out to find the money to finance his project. Eventually Liu was forced to search for benefactors from the phone book, eventually raising ¥140,000 from a friend in Shanghai.    

Given the small budget, the film production was limited with Liu and the crew living in a small basement to save on housing costs. Post-production was funded primarily from Liu and his producers selling calendars on the street, eventually paying off debts over the course of two years. 
 16 mm, 35 mm during post-production. 

== Release ==
Chen Mo and Meiting was screened as part of the 2002   Prize and a Special Mention in the Premiere First Movie award.    

The film was also screened at the 2002 Karlovy Vary International Film Festival as part of its "Another View" program for more experimental works. 

As an independent and foreign-financed film, however, Chen Mo and Meiting was not given official approval and as a result was not released in China. 

== References ==
 

== External links ==
*  
*   at Cinemasie

 
 
 
 
 
 


 
 