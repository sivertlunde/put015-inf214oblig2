The Showdown (1928 film)
 
{{Infobox film
| name           = The Showdown
| image          =
| caption        =
| director       = Victor Schertzinger
| producer       = 
| writer         = Ethel Doherty Hope Loring Houston Branch (play Wildcat)
| starring       = Evelyn Brent
| cinematography = Victor Milner
| editing        = George Nichols Jr.
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = Silent
| budget         =
| gross          =
}}
 silent American drama film directed by Victor Schertzinger and starring Evelyn Brent.

== Cast == George Bancroft as "Lucky" Cardan
* Evelyn Brent as Sibyl Shelton Neil Hamilton as Wilson Shelton
* Fred Kohler as Winter
* Helen Lynch as Goldie
* Arnold Kent as Hugh Pickerell
* Leslie Fenton as Kilgore Shelton
* George Kuwa as Willie

==Plot==
A group of Westerners seek oil in Latin America, fighting over their claims and the local prostitute. When glamorous Sibyl (Brent) appears, "Lucky" Cardan (Bancroft) warns her that no woman can stay "decent" in "this country".

==Preservation status==
The film is preserved at the Library of Congress.  In 2013 the Library of Congress print was shown at Capitolfest at Rome, New York.   

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 