Zina (film)
{{Infobox Film
| name           = Zina
| image =  Zina (film).jpg
| image_size     = 150px
| caption        = Zina DVD Cover Ken McMullen
| producer       = Ken McMullen
| writer         = Terry James Ken McMullen 
| narrator       = 
| starring       = Domiziana Giordano   Ian McKellen David Cunningham (industrial music) Barrie Guard (symphonic music) Simon Heyworth (additional music)
| cinematography = Bryan Loftus
| editing        = Robert Hargreaves
| distributor    = Virgin Films
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Zina (   Russian Revolution, as the holder of state power and later in exile. Against the background of the progressive deterioration of the situation in Europe, threatened by the rise of fascism and the spectre of the Second World War, Zina’s identification with Antigone becomes more and more credible. What were her hallucinations begin to take objective form on the streets. The dynamics of Greek tragedy, always waiting in the wings, step forward to take control.  Zina has won many awards and is regarded by many as one of the great political motion pictures.

==Cast==
* Domiziana Giordano - Zina Bronstein
* Ian McKellen - Professor Kronfeld
* Philip Madoc - Trotsky
* Rom Anderson - Maria
* Micha Bergese - Molanov
* Gabrielle Dellal - Stenographer
* William Hootkins - Walter Adams
* Leonie Mellinger - German Stenographer

==Notes==
 

==External links==
* 
*  
*  
*   at TrotskyanaNet, and here esp.  )
*  ,   on YouTube and a scene of that film  

 
 
 
 
 
 
 


 
 