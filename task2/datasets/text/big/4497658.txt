The Sidehackers
: You may have been looking for Five the Hard Way (Prison Break episode)
{{Infobox film
| name = Sidehackers
| image = Sidehackers.JPG
| image_size =
| caption =
| director = Gus Trikonis
| producer = Ross Hagen
| writer = Larry Billman (Story) Tony Huston
| narrator =
| starring = Ross Hagen Diane McBain Michael Pataki
| music = Mike Curb Guy Hemric Jerry Styner
| cinematography = Jon Hall
| editing = Pat Somerset
| distributor = Crown International Pictures
| released =  
| runtime = 82 minutes
| country = United States English
| budget =
}} sidehack (or "sidecar"), in which a passenger rides and tilts to one side or another when going around curves. The credits thank the "Southern California Sidehack Association"; sidehacking is also known as sidecarcross or "sidecar motocross racing".

== Plot ==
The film centers around Rommel, a mechanic/sidehack-style racer, who turns down the offer of J.C., a hot-tempered entertainer, to join his act after J.C. witnesses a sidehack race for the first time. During this time, J.C.s abused girlfriend, Paisley, falls for Rommel and attempts to seduce him. He rejects her advances and sends her away crying. Later, when J.C. and his crew return to their hotel, they find Paisley drunk and her clothes tattered, claiming that Rommel raped her. Angered, J.C. and his gang beat Rommel unconscious and kill his fiancee, Rita. Rommel then spends the rest of the movie plotting his revenge against J.C., who goes into hiding from the police.

The films end is nihilistic in nature. After both Rommel and J.C.s men have killed each other (while two of Rommels men escaped on a sidehack bike), the two men brawl. When Rommel manages to gain the upper hand, he elects to walk away when the police are about to arrive, but J.C. picks up a gun and shoots Rommel from behind. The last images of the film are a flashback of Rommel and his fiancee rolling about in a grassy field, superimposed over a shot of Rommels dead body.

==Mystery Science Theater 3000==
On September 29, 1990, Sidehackers was the featured movie in episode 202 of Mystery Science Theater 3000. The episode is notable for a unique gag pulled during the movie, when Cambot added an ESPN-like score graphic during one of the racing scenes.  The crew also referenced how the main characters name is the same as the famous German WWII general Erwin Rommel. The episode is available on DVD from Rhino Entertainment as part of the third volume of Mystery Science Theater 3000 DVD box sets.
 Crow later remarks, "For those of you playing along at home, Rita is dead." According to the series head writer, Michael J. Nelson, "We were all traumatized, the scene got cut, and from that day forward, movies were watched in their entirety before they were selected."  A similar edit occurred in the episode during the scene of where Rommel turns down J.C.s offer, and J.C. becomes aggravated from both the result of the decline and one of his men bugging him to ask him a question. Nero turns to J.C. to try and calm him down, only for J.C. to slam him against the wall and say "Get your hands off me, you dirty nigger!" This resulted in the line being muted and Joel and the bots covering it up saying "Oh really? Shut up!".

==Cast==
* Ross Hagen as Rommel
* Diane McBain as Rita
* Michael Pataki as JC
* Dick Merrifield as Luke
* Claire Polan as Paisley

== Notes ==
 

== External links ==
 
*  

 
 
 
 
 
 
 
 