Dakishimetai: Shinjitsu no Monogatari
{{Infobox film
| name           = Dakishimetai: Shinjitsu no Monogatari
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Akihiko Shiota
| producer       = 
| writer         = Hiroshi Saito Akihiko Shiota
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Keiko Kitagawa Ryo Nishikido Yusuke Kamiji Takumi Saito Aya Hirayama 
| music          = Takatsugu Muramatsu
| cinematography = Tokusho Kikumura
| editing        = Takashi Sato
| studio         = 
| distributor    = Toho
| released       =   
| runtime        = 122 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = ¥276,102,650
}}

  (I Just Wanna Hug You) is a 2014 Japanese romance film directed by Akihiko Shiota and starring Keiko Kitagawa and Ryo Nishikido. It was released in Japan on 1 February. 

== Cast ==
* Keiko Kitagawa
* Ryo Nishikido
* Yusuke Kamiji 
* Takumi Saito 
* Aya Hirayama 
* Eriko Sato Megumi Sato
* Masataka Kubota
* Jimon Terakado
* Kazue Tsunogae
* Jun Kunimura
* Jun Fubuki

== Reception ==
The film debuted in second place by admissions and has grossed ¥276,102,650 (US$2,716,850). 

Mark Schilling of The Japan Times gave the film 2 out of 5 stars. 

== References ==
 

== External links ==
*    
*  

 
 

 
 
 
 
 