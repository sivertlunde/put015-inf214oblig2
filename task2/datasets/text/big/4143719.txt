I'll Do Anything
{{Infobox film
| name = Ill Do Anything 
| image = Illdoanythingposter.jpg
| caption = Theatrical release poster
| producer = James L. Brooks Polly Platt
| director = James L. Brooks
| writer = James L. Brooks
| starring = Nick Nolte Albert Brooks Julie Kavner Whittni Wright Joely Richardson Tracey Ullman
| music = Hans Zimmer
| cinematography = Michael Ballhaus
| editing = Richard Marks
| studio  = Gracie Films
| distributor = Columbia Pictures
| released =  
| runtime = 115 min.
| country = United States English
| budget = $40 million
| gross = $10,209,111 (USA)
}} American dramedy film written and directed by James L. Brooks. Its primary plot concerns a down-on-his-luck actor who suddenly finds himself the sole caretaker of his six-year-old daughter.

==Synopsis== Georgia to pick up his daughter Jeannie for what he believes is a brief visit and discovers Beth is facing a prison term and Jeannie will be living with him for the duration of her sentence. The two return to Hollywood and struggle with their new circumstances and building a relationship (Matt hasnt seen the six-year-old since she was four).  When Matt goes in to make a screen test for a lead in a film, he leaves Jeannie with a friend at the studio, and when he picks her up hes stunned to learn shes been cast in a sitcom. There are multiple sub-plots, including one focusing on Matts relationship with staff script-reader Cathy Breslow and another concerning test screening analyst Nan Mulhanney and her tumultuous relationship with Adler.  While a large part of the film is a satire of the film industry, it also skewers relationships from various angles.

==Cast==
* Nick Nolte - Matt Hobbs
* Whittni Wright - Jeannie Hobbs
* Albert Brooks - Burke Adler
* Julie Kavner - Nan Mulhanney
* Joely Richardson - Cathy Breslow
* Tracey Ullman - Beth Hobbs
* Joely Fisher - Female D Person
* Vicki Lewis - Millie
* Anne Heche - Claire
* Ian McKellen - John Earl McAlpine
* Angela Alvarado - Lucy

==Production== movie musical and parody of "Hollywood lifestyles and movie clichés", costing £40 million.    It featured songs by Carole King, Prince (musician)|Prince, and Sinéad OConnor, among others, with choreography by Twyla Tharp.  When preview audience reactions to the music were overwhelmingly negative, all production numbers from the film were cut and Brooks wrote several new scenes, filming them over three days and spent seven weeks editing the film. Brooks noted: "Something like this not only tries ones soul - it threatens ones soul." 
He later said of the film,

 

==Reaction==
Ill Do Anything received mixed to positive reviews from critics. It currently holds a rating of 61% on Rotten Tomatoes based on 18 reviews.

In his three-star review in the Chicago Sun-Times, Roger Ebert called it "one of those offcenter comedies that gets its best moments simply by looking at people and seeing how funny, how pathetic, how wonderful they sometimes can be . . . its a bright, edgy, funny story about people who have all the talent they need, but not all the luck . . . It is helpful, I think, to simply forget about the missing songs, and recognize that Ill Do Anything is a complete movie without them - smart, original, subversive."  Janet Maslin of the New York Times described it as "droll" and "improbably buoyant." 

===Box office===
The film was a box office failure. Produced on a budget of $40 million, Ill Do Anything grossed only a little over $10.2 million in ticket sales.

==Music==
One of the original songs meant to be performed in the film is heard during the closing credits and is included on the  , none of the actual performances from the movie were ever officially released.

Although James L. Brooks has mentioned he would like to release a Directors Cut restoring the musical numbers and including a making-of Documentary film|documentary, that project has yet to come to fruition. The films commercially released version is available on DVD.

 
==References==
 
 

==External links==
*  
*  
*  
*  
*   - Los Angeles Times profile

 

 
 
 
 
 
 
 
 
 
 
 
 