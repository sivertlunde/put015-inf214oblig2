Captain Eddie
{{Infobox film
| name = Captain Eddie
| image =Captain-Eddie-movie-poster-1944.jpg
| image_size =200px
| caption =Theatrical poster
| director = Lloyd Bacon
| producer = Winfield R. Sheehan
| based on = Seven Were Saved by Edward Vernon Rickenbacker|"Eddie" Rickenbacker and Lt. James Whittakers We Thought We Heard the Angels Sing
| writer = John Tucker Battle (screenplay)
| narrator = Thomas Mitchell
| music = Cyril J. Mockridge
| cinematography = Joseph MacDonald James B. Clark    
| studio = 20th Century Fox
| distributor = 20th Century Fox
| released = June 19, 1945
| runtime = 107 minutes
| country = United States English 
| budget =
| gross = over $1 million Charles Tranberg, Fred MacMurray: A Biography, Bear Manor Media, 2014 
| preceded_by =
| followed_by =
| website =
}}
Captain Eddie is a 1945 American drama film directed by Lloyd Bacon, based on Seven Were Saved by Edward Vernon Rickenbacker|"Eddie" Rickenbacker and Lt. James Whittakers We Thought We Heard the Angels Sing. The film stars Fred MacMurray, Lynn Bari and Charles Bickford. Captain Eddie is a biographical film|"biopic" of Rickenbacker, from his experiences as a flying ace during World War I to his later involvement as a pioneering figure in civil aviation, and his iconic status as a business leader who was often at odds with labour unions and the government.  

==Plot== Richard Crane and other crew members to survive for 19 days on a tiny rubber raft.

While awaiting their rescue, Rickenbacker recalls his other adventures that have highlighted a remarkable life. From his childhood in Columbus, Ohio, marked by a passion for machinery and technology, the young man becomes a celebrated race car driver, although his mother Elise (Mary Philips) and father William (Charles Bickford) have mixed feelings about his interest in cars, and eventually, aircraft. When war breaks out, Rickenbacker signs up and becomes a fighter pilot with the 94th Aero Squadron. By wars end, he has shot down more aircraft than any other American, becoming the American "ace-of-aces".

After World War I, Rickenbacker marries his sweetheart Adelaide (Lynn Bari) and enters commercial aviation as an owner and great advocate for the fledgling airline industry. When war breaks out again, he is compelled to return to the military. Returning to the present predicament, he becomes the natural leader of the survivors. The ordeal leads to the death of one of the men from exposure, but Rickenbackers ability to organize the dwindling supplies and keep up morale among the others, leads to their survival. After a three-week stay in a military hospital, Rickenbacker is able to complete his mission and is hailed as a true hero.

==Cast==
* Fred MacMurray as Captain Edward Rickenbacker 
* Lynn Bari as Adelaide Frost Rickenbacker 
* Charles Bickford as William Rickenbacker  Thomas Mitchell as Ike Howard 
* Lloyd Nolan as Lt. Jim Whittaker 
* James Gleason as Tom Clark 
* Mary Philips as Elsie Rickenbacker 
* Darryl Hickman as Eddie Rickenbacker as a boy 
* Spring Byington as Mrs. Frost 
* Richard Conte as Pvt. John Bartek  Charles Russell as Sgt. Jim Reynolds  Richard Crane as Capt. Bill Cherry 
* Stanley Ridges as Col. Hans Adamson  
* Clem Bevans as Jabez  
* Grady Sutton as Lester Thomas 
* Chick Chandler as Richard Lacey  
* Dwayne Hickman as Louis Rickenbacker 
* Nancy June Robinson as Mary Rickenbacker  
* Winifred Glyn as Emma Rickenbacker 
* Gregory Marshall as Dewey Rickenbacker 
* David Spencer as Albert Rickenbacker 
* Elvin Field as Bill Rickenbacker  George Mitchell as Lt. Johnny De Angelis

==Production==
 
Principal photography for Captain Eddie began in November 1944 and continued for three months on Foxs back-lot in Los Angeles.  The film received cooperation of the United States Army Air Forces, primarily coming in the form of the loan of a B-17F from the AAFs First Motion Picture Unit in Culver City, California. Crash survivor Lt. James Whittaker was also temporarily assigned to the production to serve as a technical advisor. Scenes of the ditching were done in a studio water tank, using the mock-up fuselage of a B-17. Orriss 2013, p. 110.  
 Curtiss Pusher, flown from the Santa Rosa fairgrounds, serving as a pre-World War I airfield.  

==Reception==
The world premiere for Captain Eddie was held in Rickenbackers hometown of Columbus, Ohio, attended by a select mixture of politicians and celebrities, including the actress Carole Landis.  Family members were also in attendance. 

Reviews for the film were mixed, with most critics seeing it as a romanticized biography of a famous and controversial figure. Bosley Crowther in his review for The New York Times dismissed the effort as "not the story it promises to be" "... just another sentimental comedy about a kid who jumped off the barn in his youthful passion for flying and courted his girl in a merry Oldsmobile ... this is not the story of Rickenbacker— not the significant story, anyhow. And it is hardly the story to support the climax afforded by that experience on the raft."  Later reviews echoed the contemporary thoughts about Captain Eddie. Leonard Maltin noted, "Routine aviation film doesnt do justice to exciting life of Eddie Rickenbacker; its standard stuff." 

The film was a box office flop. 
===Awards and honors=== Academy Award Wonder Man.    oscars.org. Retrieved: August 31, 2014. 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Gans, Eric. Carole Landis: A Most Beautiful Girl. Jackson, Mississippi: University Press of Mississippi, 2008. ISBN 978-0-7864-2200-5.
* Hardwick, Jack and Ed Schnepf. "A Buffs Guide to Aviation Movies". Air Progress Aviation, Vol. 7, No. 1, Spring 1983.
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
* Orriss, Bruce W. When Hollywood Ruled the Skies: The Aviation Film Classics of World War I. Los Angeles: Aero Associates, 2013. ISBN 978-0-692-02004-3.
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 