The Monkey's Paw (2013 film)
 
{{Infobox film
| name           = The Monkeys Paw
| image          = The Monkeys Paw 2013.jpg
| caption        = film poster
| alt            = 
| director       = Brett Simmons
| producer       = Ross Otterman
| screenplay     = Macon Blair
| based on       =  
| starring       = Stephen Lang C.J. Thomason Corbin Bleu Charles S. Dutton  
| studio         = TMP Films Chiller Films
| released       =   
| country        = United States
| language       = English
}} of the Chiller films presentation. 

The film was released in theaters and video on demand on October 8, 2013, and received mostly negative reviews.

==Cast==
* Stephen Lang as Tony Cobb
* C. J. Thomason as Jake Tilton
* Daniel Hugh Kelly as Gillespie
* Corbin Bleu as Catfish
* Charles S. Dutton as Detective Margolis
* Michelle Pierce as Olivia
* Tauvia Dawn as Abby

==Home Video==
The film was released on DVD and Blu-ray by Shout! Factory on their Scream Factory label on June 17, 2014.

==References==
 

 
 
 


 