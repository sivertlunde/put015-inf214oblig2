The Bravados
 
{{Infobox film
| name           = The Bravados
| image          = The Bravados - US film poster.jpg
| image_size     =
| caption        = Theatrical release poster Henry King
| producer       = Herbert B. Swope Jr.
| screenplay     = Philip Yordan
| story          = Frank ORourke
| narrator       = 
| starring       = Gregory Peck Joan Collins Alfred Newman Hugo Friedhofer Lionel Newman
| cinematography = Leon Shamroy
| editing        = William Mace
| distributor    = 20th Century Fox
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
}}
 Henry King starring Gregory Peck and Joan Collins.  The CinemaScope film was based on a novel of the same name written by Frank ORourke. 

==Plot==
 
Jim Douglas (Gregory Peck) is a rancher who has been pursuing the four outlaws who he believes murdered his wife six months earlier. He rides into the town of Rio Arriba, where four men, Alfonso Parral (Lee Van Cleef), Bill Zachary (Stephen Boyd), Ed Taylor (Albert Salmi) and Lujan (Henry Silva), fitting the description of the men he seeks are in jail awaiting execution. The town has issued instructions to only allow the hangman (Joe DeRita) to enter the jail, so Douglas is taken to Sheriff Eloy Sanchezs (Herbert Rudley) office to state his business. The town has never had an execution before, so they have brought in a man from another town for do the job. The sheriff allows Douglas into the jail to see the men.  They claim they had never seen him before, but he has the face of a hunter.

In town Douglas meets Josefa Velarde (Joan Collins).  He met her five years previously in New Orleans but hasnt seen her since. Through their conversation we learn that she was husband hunting when they met before.  She has been looking after her fathers ranch since he died. Douglas reveals to her that he has a daughter (Maria Garcia Fletcher). Other inhabitants of the town include businessman Gus Steimmetz, his daughter Emma (Kathleen Gallant) and her fiance Tom (Barry Coe).

The executioner, Simms, arrives the same day.  He drinks with Douglas, but is generally unfriendly.  The sheriff invites Simms to see the men he is to hang, but Simms holds off until the town is at church for a pre-execution mass.

While pretending to evaluate the men he is to hang, Simms stabs the sheriff in the back.  The sheriff shoots and kills Simms in the struggle, but the inmates strangle the sheriff until he is unconscious and take his keys.  The four escape and take Emma as a hostage. The wounded sheriff comes into the church during the service and tells the townspeople that the prisoners have escaped. The townspeople form a posse to ride out immediately.  They enlist Douglas aid to track them down, but Douglas waits until morning.  The posse is held up at a pass when one of the four men stays behind to cover the pass. Douglas catches up with the posse the next day while they are held up trying to dislodge the rifleman protecting the pass.

Douglas corners Parral, who pleads for his life before Douglas kills him. Then Douglas ropes Taylor by the feet and hangs him upside-down from a tree. The two remaining men reach the house of John Butler (Gene Evans), a prospector and Douglass neighbor. Zachary kills Butler and Lujan steals the sack of coins he tried to run off with, but they see someone approaching in the distance and leave in a panic, leaving Emma behind. The people coming turn out to be Josefa and Douglas, arriving from different directions. Douglas identifies the body, and the posse arrives and finds Emma, raped by Bill Zachary, in the house. Douglas tells the posse to ride on while he goes back to his ranch to get fresh horses.
 Mexican border, Douglas goes on alone. He finds Zachary in a bar and kills him in a gunfight. He then goes on to the home of the fourth man Lujan. When Douglas shows a photo of his wife to Lujan, Lujan insists he has never seen the woman before, and recalls that he and his companions rode past the Douglas ranch on their way from the border. Pointing to the sack, Douglas states that the men who killed his wife stole the sack containing the familys life savings. When Lujan tells Douglas that he took the bag from Butlers dead hand, Douglas realizes that Butler killed his wife.

Douglas sees that he had been pursuing men who had nothing to do with his wifes death. Indeed, when he showed each of them a picture of his wife, all of them denied ever having seen her. Douglas realizes that he is no better than the men he has been pursuing, having killed in cold blood.  He returns to town and goes to the church to beg forgiveness. The priest (Andrew Duggan) says that he did what he felt was right and to his credit, does not take refuge in the fact that they were outlaws anyway. Josefa then arrives with Douglas daughter, and as they exit the church together, the sheriff and the townspeople celebrate Douglas success at tracking down and killing the men.

==Cast==
* Gregory Peck as Jim Douglas
* Joan Collins as Josefa Velarde
* Stephen Boyd as Bill Zachary
* Albert Salmi as Ed Taylor
* Henry Silva as Lujan
* Kathleen Gallant as Emma Steimmetz
* Barry Coe as Tom
* George Voskovec as Gus Steinmetz
* Herbert Rudley as Sheriff Eloy Sanchez
* Lee Van Cleef as Alfonso Parral
* Andrew Duggan as Padre
* Ken Scott as Primo, Deputy Sheriff
* Gene Evans as Butler
* Joe DeRita as Simms
* Jason Wingreen as Nichols
* Ada Carrasco as Mrs. Parral

The film is notable for including a rare serious role for Joe DeRita who, around the time the film was released, became "Curly-Joe" of the Three Stooges.

==Reception==

===Critical response===
When the film was released The New York Times film critic, A. H. Weiler, gave it a positive review, writing, "Despite these flaws, which are fundamentally minor deficiencies, The Bravados emerges as a credit to its makers. Director Henry King, who headed the troupe that journeyed down to the photogenic areas of Mexicos Michoacán and Jalisco provinces, has seen to it that his cast and story move at an unflagging pace...The canyons, towering mountains, forests and waterfalls of the natural locales used, make picturesque material for the color cameras. But the producers have given their essentially grim chase equally colorful and arresting treatment." 

More recently, film critic Dennis Schwartz gave the film a mixed review, writing, "A brooding revenge Western that once again teams up Gregory Peck with director Henry King...Its a no-nonsense downbeat Western with Peck in his usual fine form, but the film failed to maintain a tension as it became clear from early on these men were not the ones who killed his wife. The tacked-on religious message seemed unnecessary, as it left an uneasy grimness hanging over a film that had such fine production values and potential to be a great one." 

===Awards===
;Wins
* National Board of Review NBR Award, Best Supporting Actor, Albert Salmi; 1958.

==References==
 

==External links==
 
*  
*  
*  
*   film traielr at ReelzChannel

 

 
 
 
 
 
 
 
 
 
 
 