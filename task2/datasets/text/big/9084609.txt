It Had to Happen
{{Infobox film
| name           = It Had to Happen
| image          =
| image_size     =
| caption        =
| director       = Roy Del Ruth
| producer       = Darryl F. Zanuck
| writer         = Rupert Hughes Kathryn Scola Howard Ellis Smith
| starring       = George Raft Rosalind Russell Leo Carrillo Arline Judge Alan Dinehart Arthur Hohl
| music          = Arthur Lange
| cinematography = J. Peverell Marley
| editing        = Allen McNeil
| distributor    = 20th Century Fox Teakwood Video
| released       =  
| runtime        = 79 mins
| country        = United States
| language       = English
}}
It Had to Happen is a 1936 film starring George Raft and Rosalind Russell.  The movie was written by Rupert Hughes, Kathryn Scola, and Howard Ellis Smith, and directed by Roy Del Ruth.

==Cast==
*George Raft as Enrico Scaffa
*Rosalind Russell as Beatrice Nunes
*Leo Carrillo as Giuseppe
*Arline Judge as Miss Sullivan
*Alan Dinehart as Rodman Dreke
*Arthur Hohl as John Pelkey

==Reception==
The film was a box office hit. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 68-69 

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 


 