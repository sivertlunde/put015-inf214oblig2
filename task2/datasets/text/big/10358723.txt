The Vagabond King (1930 film)
{{Infobox film name = The Vagabond King
| image = VagabondKing1930.jpg
| caption = Theatrical release poster producer  = Adolph Zukor director = Ludwig Berger writer = Brian Hooker   and the novel by R.H. Russell starring = Arthur Stone Tom Ricketts music = Rudolf Friml W. Franke Harling  John Leipold Oscar Potoker cinematography = Henry W. Gerrard Ray Rennahan (Technicolor) editing = Merrill G. White distributor = Paramount Pictures released = February 17, 1930 runtime = 104 minutes language = English country = United States
}} musical operetta operetta of French poet Academy Award for Best Art Direction.   

==Plot==
The story takes place in medieval France. King Louis XI (O. P. Heggie), hoping to enlist the French peasants in his upcoming battle against the Burgundians, appoints François Villon (Dennis King) king of France for one day. Despite being successful against the Burgundians, François Villon is sentenced to hang by King Louis XI for writing derogatory verses about him...

Jeanette MacDonald is Katherine, the high-born girl whom Villon pines for, while Huguette, a tavern wench (Lillian Roth) gives up her life to save her beloved poet.

==Cast==
* Dennis King as François Villon
* Jeanette MacDonald as Katherine
* O. P. Heggie as King Louis XI
* Lillian Roth as Huguette
* Warner Oland as Thibault Arthur Stone as Oliver the barber
* Tom Ricketts as The Astrologer (as Thomas Ricketts)
* Lawford Davidson as Tristan
* Christian J. Frank as Executioner

==Songs==
 
* "Song of the Vagabonds"
* "King Louis"
* "Mary, Queen of Heaven" Some Day"
* "If I Were King"
* "What France Needs"
* "Only a Rose"
* "Huguette Waltz"
* "Love Me Tonight"
* "Nocturne"

Six songs from the operetta were retained for the film, while four were specially written for it by different composers.

==Production==
*Dennis King recreated his original London and Broadway stage role as Villon in this film.
*Dennis King and costar Jeanette MacDonald did not get along particularly well, and matters came to a head when they filmed the song "Only a Rose". As MacDonald was singing a solo passage, the egotistical King managed to edge his profile into the shot; forever afterward, the diva scornfully referred to the number as "Only a Nose".
*Composer Rudolf Friml had a stipulation in his contract that forbade the use of newly composed songs in this production. Paramount attempted to change the films title to If I Were King and also had some new songs composed for the film. When Friml was made aware of the new songs, he sued the studio. As a result Paramount changed the title back to The Vagabond King and paid Friml $50,000 to allow for the use of non-original songs.

==Preservation==
For many years, this film was seen only in black-and-white prints made for television release in the 1950s. At one time even the black-and-white prints were considered irretrievably lost. One nitrate Technicolor print did survive at the UCLA Film and Television Archive, and it was restored and preserved in 1990.

==See also==
*List of early color feature films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 