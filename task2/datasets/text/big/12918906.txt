The Love Guru
 
 
{{Infobox Film
| name = The Love Guru
| image = Love guru.jpg
|alt=
| director = Marco Schnabel
| producer = Mike Myers Gary Barber
| writer = Mike Myers Graham Gordy John Oliver Omid Djalili Ben Kingsley  Daniel Tosh
| music = George S. Clinton
| cinematography = Peter Deming
| editing = Billy Weber Lee Haxall
| studio = Spyglass Entertainment
| distributor = Paramount Pictures
| released =  
| runtime = 86 minutes
| country = Canada United States United Kingdom Germany
| language = English
| budget = $62 million  
| gross = $40,863,344
}} John Oliver, Omid Djalili, and Ben Kingsley. 

The film was a financial flop and earned overwhelmingly negative reviews.

==Plot==
The film begins in a little Indian village, where we are introduced to Guru Pitka talking into his recorder which allows him to sound like a man, a woman, or Morgan Freeman. He is the #2 Guru in the world, next to Deepak Chopra. A flashback shows us that Pitka is an orphan of missionaries in India. Both Pitka and Chopra were taught by Guru Tugginmypudha. Pitka says he wants to become a Guru so that girls will love him, so Tugginmypudha puts a chastity belt on him until he can learn that loving himself is more important than being loved by others.

Pitkas dream is to become the number #1 Guru and to get on Oprah. He lives a charmed life scooting around on his mobile pillow. He has thousands of followers, including celebrities like Jessica Simpson, Val Kilmer and Mariska Hargitay. The latter being ironic since Pitkas mantra, is "Mariska Hargitay". We attend one of his teachings which involve a lot of anagrams and play on words on a slide show. Ex: Nowhere can also be Now Here. Pitka has a gorgeous palace with a beautiful female staff, helicopter, elephants, and a servant, Rajneesh, who squeezes fresh orange juice out of a tree for him and has an ostrich lay his morning egg for him.  

Meanwhile, in Canada. Jane Bullards father passed away and left her a hockey team, the Toronto Maple Leafs. They havent won a Stanley Cup since she took over so the city hates her. Her star player, Darren Roanoke, the Tiger Woods of Hockey, has been playing poorly ever since his wife Prudence left him for a well endowed French Canadian, Le Cocq. Le Cocq plays for the rival Los Angeles Kings. Hes got a tattoo on his abdomen that reads "The Legend" with an arrow pointing down. When Pitka is introduced to Jane Bullard, he imagines an Bollywood musical fantasy with her in it. Jane is a big fan of Pitkas, having read all of his books. Shes paying him 2 million dollars to patch up Darrens marriage, in time to win the Stanley Cup. Pitkas agent is thrilled and says that if Pitka patches up the marriage, Oprah will have him on her show.

Pitka has an acronym, DRAMA, tattoed on his hand. D represents Distraction. Guru Tugginmypudha taught him the lesson of distraction with the use of his own urine. Pitka encourages the rival team to beat Darren up during a game, thus distracting him from his emotional distress over Prudence and Le Cocq. Darren begins to play well but then gets suspended for the next two games after beating up Le Cocq and hitting Coach Cherkov with a hockey puck. Theres a meeting in the Coachs tiny office, complete with mini water cooler, where they argue about the situation. Later, Pitka has dinner with Jane, gets her to move in for a kiss, only to hear a ding on his chastity belt. He tells her it can not be, she doesnt understand and runs out. Next, Pitka tells Darren to write an Apology note to Prudence. Pitka gets in a huge fight with Le Cocqs guard rooster in order to deliver the note.

The R in Drama is Regression. Apparently Darren is afraid of his mom. They confront her at her Church choir but she ends up scaring both Pitka and Darren out of the church with her harsh words. Time is running out so Pitka skips through the rest of the anagram. He distracts Le Cocq with his idol, Celine Dion. Then lies to Prudence, telling her Darren stood up to his mom, getting her to go back to Darren. At the last game. Le Cocq, having overheard that Darren cant play with his mom in the audience, gets Darrens mom to sing the national anthem, causing Darren to run out of the game. Meanwhile, Pitka is at the airport, on his way to the Oprah show. He sees the news on TV and defies his agent by going to the game to help Darren. After smoothing things over with his mom, Darren is ok again until Le Cocq says Prudence also said "Damn" to him in bed. Darren freezes and Pitka realizes he needs to distract Darren from whatever is troubling him.

Pitka gets two elephants to have sex in the middle of the ring, in front of millions of TV audience, which distracts Le Cocq and helps Darren wake up from his stupor and hit the winning goal, and everyone lives happily ever after. Back in the first Indian village, Guru Tugginmypudha removes Pitkas chastity belt, there was a hook in the back. Jane and Pitka kiss. The ending is a musical number with Jane and Pitka dancing Indian musical style.

==Cast==
* Mike Myers as Guru Maurice Pitka / Young Pitka / Teenage Pitka / Himself
* Jessica Alba as Jane Bullard
* Justin Timberlake as Jacques "Le Coq" Grandé
* Romany Malco as Darren Roanoke
* Meagan Good as Prudence Roanoke, Darrens wife and Jacques lover
* Verne Troyer as Coach Punch Cherkov
* Omid Djalili as Guru Satchabigknoba / Gagandeep Singh
* Ben Kingsley as Guru Tugginmypudha
* Telma Hopkins as Lillian Roanoke
* Manu Narayan as Rajneesh, Pitkas assistant John Oliver as Dick Pants
* Stephen Colbert as Jay Kell
* Jim Gaffigan as Trent Lueders
* Rob Huebel as Bar Patron / Cameo
* Daniel Tosh as Cowboy Hat

;As themselves
* Mariska Hargitay
* Jessica Simpson
* Kanye West
* Val Kilmer (uncredited)
* Morgan Freeman (voice)
* Rob Blake
* Deepak Chopra
* Oprah Winfrey

==Music==
The original score for the film was composed by George S. Clinton, who recorded it with an 80-piece ensemble of the Hollywood Studio Symphony at Warner Bros.   

The song "Dhadak Dhadak" from the 2005 Bollywood film Bunty Aur Babli was used in the trailer.
 9 to The Joker" are all in the film (performed by Myers and with sitar accompaniment) and on the soundtrack. "Brimful of Asha" was also used in the film.

==Promotion== American Idol David Cook Sitar Hero.
 Fox Entertainments beliefnet.com website  was "created as part of a collaboration between Beliefnet and Paramount Pictures." 

==Box-office performance==
The film did poorly at the box-office. On its opening weekend, The Love Guru grossed $13.9 million in 3,012 theaters in the United States and Canada, ranking #4 at the box office.    The opening week numbers fell short of the $20 million range forecast by Hollywood pundits.  The film grossed $32,190,314 in the United States and Canada with only an additional $8.7 million overseas, for a total of $40.8 million worldwide. 

==Critical reception==
The Love Guru received extremely negative reviews; as of August 2012, the review aggregator Rotten Tomatoes reported that 14% of critics gave the film positive reviews, based on 167 reviews; the sites critical consensus states, "The Love Guru features far too many gross-out gags, and too few earned laughs, ranking as one of Mike Myers poorest outings."  Metacritic reported the film had an average score of 24 out of 100, based on 33 reviews. 
 Chopra is Leafs and, 9 to 5" are oddly watchable - but mostly the film is 88 minutes of ridiculous sight gags and obscene puns." 

A. O. Scott of The New York Times wrote "The word unfunny surely applies to Mr. Myerss obnoxious attempts to find mirth in physical and cultural differences but does not quite capture the strenuous unpleasantness of his performance. No, The Love Guru is downright antifunny, an experience that makes you wonder if you will ever laugh again."    Scott also commented that the appearance of actress Mariska Hargitay was anti-climactic. An ongoing gag in the film is the use of "Mariska Hargitay" as a phony Hindi greeting. 

Roger Ebert gave the film 1 out of 4 stars, writing, "Myers has made some funny movies, but this film could have been written on toilet walls by callow adolescents. Every reference to a human sex organ or process of defecation is not automatically funny simply because it is naughty, but Myers seems to labor under that delusion." 
 worst films of at least the past several years, and going so far as to declare it a career-killing movie for Myers. 
 Austin Powers Sony Pictures The Interview: "...if you really want to put a bomb in a theater, do what I did: put in The Love Guru." 
 Razzie Awards===
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Nominee
! Category
! Result
|- Gary Barber Michael De Luca Mike Myers Golden Raspberry Worst Picture
| 
|- Mike Myers Golden Raspberry Worst Actor
| 
|- Jessica Alba Golden Raspberry Worst Actress
| 
|- Ben Kingsley Golden Raspberry Worst Supporting Actor
| 
|- Verne Troyer
| 
|- Mike Myers Graham Gordy Golden Raspberry Worst Screenplay
| 
|- Marco Schnabel Golden Raspberry Worst Director
| 
|- Mike Myers Worst Actor The Cat in the Hat)
| 
|-
|}

==Portrayal of Hinduism==
Before the films release, some Hindus expressed unhappiness about how Hindus are portrayed, the disrespect of their culture and the bad impression that it would give those not well exposed to Hinduism, while some gave a cautious welcome, asking other Hindus to look at it as satire and not the truth.    Rajan Zed, a Hindu leader from Nevada, demanded that Paramount Pictures screen the film for members of the Hindu community before its release. Based on the movies trailer and MySpace page, Zed said The Love Guru "appears to be lampooning Hinduism and Hindus" and uses sacred terms frivolously. He told The Associated Press, "People are not very well-versed in Hinduism, so this might be their only exposure...They will have an image in their minds of stereotypes. They will think most of us are like that." 
 Hindu American community in light of concerned inquiries that were reported to its national headquarters. The reviewers concluded that the film was vulgar and crude but not necessarily anti-Hinduism|anti-Hindu. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 