The Green Scarf
The British mystery Richard OSullivan and Michael Medwin.  The films plot concerns a man who is accused of a seemingly motiveless murder.   It was based on a story by Gordon Wellesley.

==Cast==
* Michael Redgrave as Maitre Deliot 
* Ann Todd as Solange Vauthier 
* Leo Genn as Rodelec 
* Kieron Moore as Jacques  Richard OSullivan as Child Jacques 
* Jane Lamb as Child Solange 
* Michael Medwin as Teral  Jane Griffiths as Danielle 
* Ella Milne as Louise 
* Jane Henderson as Mme. Vauthier  George Merritt as Advocate General 
* Peter Burton as Purser
* Tristan Rawson as Prison Governor  
* Henry Caine as Ships Captain Phil Brown as John Bell  Anthony Nicholls as Goirin 
* Walter Horsbrugh as Interpreter 
* Evelyn Roberts as President of the Court 
* Neil Wilson as Inspector  Michael Golden as Warder

==References==
 

 
 
 
 
 
 