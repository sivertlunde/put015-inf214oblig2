Rama Rama Krishna Krishna
 
{{Infobox film
| name = Rama Rama Krishna Krishna
| image = Rama Rama Krishna Krishna.jpg
| caption = Official film poster
| director = Srivas
| producer = Dil Raju
| writer = Ram Priya Anand Bindu Madhavi Gracy Singh Vineet Kumar
| music = M.M. Keeravani
| cinematography =
| editing = Marthand K Venkatesh
| studio         = Sri Venkateswara Creations
| distributor    = Sri Venkateswara Creations
| released =  
| runtime = 161 minutes
| country = India
| language = Telugu
| budget =  19 crores
| gross = 27 crores
}}
Rama Rama Krishna Krishna is a 2010 Telugu language film that stars Ram (actor)|Ram, Priya Anand and Bindu Madhavi in the lead role, whilst prominent actors Arjun Sarja, Nassar, Brahmanandam and Gracy Singh play pivotal roles. This film, directed by Srivas (Lakshyam (2007 film)|Lakshyam fame) and produced by noted producer, Dil Raju, released on May 12, 2010.    Upon release the film received mixed to positive reviews, critics mainly praising Rams and Sarjas performance.  The film was dubbed later into Tamil as Gandhipuram and released on December 24, 2010 and in Hindi as Nafrat Ki Jung. 

==Plot==
Movie starts in Mumbai when mafia leader Ashok Deva (Arjun) is on an unending war with his opponent Pawar (Vineet Kumar). Deva has a cute family with two sisters Sirisha and Priya (Priya Anand), wife Gauthami (Gracy Singh) and his right hand Shiva. Gauthami is killed by Pawar group on her birthday and she takes last word from her husband to leave this violence and lead a peaceful life away from mafia.

Respecting her words, Deva along with his sisters and Shiva leaves to a small village Gandhipuram in East Godavari district to start his new life while Pawar group thinks that Deva is dead. Chakrapani (Naazar) is head of this village with two sons Anand and Ramakrishna (Ram).

Ramakrishna is a notorious guy in the village who bashes up the bad respecting his father’s principles, who is chased by his maradalu (Bindu Madhavi) while Anand is a medico who is in love with classmate Sirisha (Deva’s  sister). Feared of their parents not accepting their marriage, both of them elope to their uncle Subba Rao’s (Brahmanandm) residence in Mumbai. Now, Ramakrishna and Priya along with Shiva reach Mumbai in search of this eloped couple only to get targeted by mafia again. Pawar wants to finish Shiva and Priya but are saved by Ramakrishna who later knows about the flash back of Deva.

Ramakrishna now takes on the responsibility of his brother’s marriage with Sirisha and bring them back to Gandhipuram but to be continuously chased by this mafia. At last when both the families agree for the marriage of Ramakrishna with Priya and Anand with Sirisha, here comes back mafia Pawar to take the movie to climax. Who won  this mafia war? Did Ramakrishna win his love? What happened to Deva?

==Cast==
* Ram Pothineni as Rama Krishna
* Arjun Sarja as Ashok Deva
* Priya Anand as Priya
* Bindu Madhavi as Second Heroine
* Vineet Kumar as Pawar
* Nassar as Rama Krishna Father Pragathi as Rama Krishna Mother
* Sayaji Shinde as DIG in Mumbai Police
* Brahmanandam
* Benarjee
* Srinivasa Reddy
* Brahmaji
* Gracy Singh as Gauthami (Ashok Devas Wife) - Cameo Appearance

==Soundtrack==
{{Infobox album |  
| Name = Rama Rama Krishna Krishna
| Type = soundtrack 
| Artist = M.M. Keeravani
| Cover = Rama_Rama_Krishna_Krishna_soundtrack_CD_cover.jpg
| Released = April 2010
| Recorded = Feature film soundtrack 
| Length = 
| Label = Aditya Music
| Producer = M.M. Keeravani
| Reviews =
}}

The soundtrack was composed by M.M. Keeravani, All Lyrics given by Ananth Sriram and released worldwide by Aditya Music. 
The album featured six tracks: Karthik and Ranjith
* Ola Ulala &ndash; Chaitra
* Lera Chanti &ndash; Karthik and Chorus
* Count Down &ndash; Chaitanya
* Endhukila &ndash; M.M. Keeravani, Karthik (singer)|Karthik, Geetha Madhuri
* Thu Go Jilla &ndash; Chaitra

==References==
 

==External links==
*  

 
 
 
 