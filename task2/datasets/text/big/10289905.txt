Anjaneya (film)
{{Infobox film
| name = Anjaneya
| image = 
| director = N.  Maharajan
| writer = 
| starring = Ajith Kumar Meera Jasmine Raghuvaran
| producer = S. S. Chakravarthy
| music = Mani Sharma
| studio = NIC Arts
| released =  
| runtime = 
| language = Tamil
| budget =  8.7 crore
}}
Anjaneya ( ) is a 2003 Tamil film directed by Maharajan featuring Ajith Kumar, Meera Jasmine in lead roles with Raghuvaran, Ramesh Khanna, and Sarala in supporting roles. The film, produced by S. S. Chakravarthy, had its score and soundtrack composed by Mani Sharma. The film opened in October 2003 to mixed reviews.  

==Plot==
DCP Paramaguru (Ajith Kumar) an efficient police officer who fights with the scum of the society. He later masquerades as a thief to infiltrate into the underworld. The bad guys are surprised to find that Paramguru is the DCP, out to get them. So they all gang up against him. In this process a thrilling encounter takes place between the good and the evil. Paramaguru is helped in his fight against injustice by Divya (Meera Jasmine) who falls in love with him.

==Cast==
* Ajith Kumar as Paramaguru
* Meera Jasmine as Divya
* Raghuvaran as Venketeaswaran
* Jaya Prakash Reddy as Prakash Adithya Menon as Siva (Début Movie)
* Ramesh Khanna as Kolai
* Manivannan Ponnambalam as Veerapan
* FEFSI Vijayan as Vettu Ramasamy
* Jaya Prakash Reddy as Jay Prakash
* Anu Hasan as Paramagurus sister Rathi in a special appearance

==Soundtrack==

{{Infobox Album |  
| Name        = Anjaneya
| Type        = soundtrack
| Artist      = Mani Sharma
| Cover       = 
| Released    = 2003
| Recorded    =  Feature film soundtrack |
| Length      = 
| Label       = Star Music
| Producer    = Mani Sharma
| Reviews     = 
}}

The soundtrack features 5 songs composed by Mani Sharma. Lyrics written by Vairamuthu and Kabilan (lyricist)|Kabilan.

*Ovvoru Naalu - Tippu (singer)|Tippu, Chorus Sujatha
*Paisa Gopuram - Karthik, Anuradha Sriram
*Paavadai Panjavarnam - Shankar Mahadevan, Chorus
*Vennila Vennila - Udit Narayan, Harini

==Production==
The leading female role was eventually handed to Meera Jasmine even though Reemma Sen was also approached earlier for the film.  The film feature Ajith Kumar in his first role as a police officer, before further appearances in Kireedam (2007 film)|Kireedam, Aegan, Mankatha, Arrambam and Yennai Arindhaal.    The film was shot within 47 days, with Ajith reportedly working extra time to complete scenes. Producers downplayed any publicity for the film, releasing the audio with little fanfare and not releasing a teaser trailer. 

==Release==

The film received mixed reviews with the critic from The Hindu claiming that "the lack of consistency in the treatment affects the film no end", criticizing Maharajans direction.  
 Rajasekhar bought the remake rights of the film. 

==References==
 

==External links==
*  

 
 
 
 
 


 