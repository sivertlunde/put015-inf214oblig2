Das Vermächtnis des Inka
{{Infobox film
| name           =Das Vermächtnis des Inka
| image          =Das Vermächtnis des Inka.jpg
| image_size     =260px
| caption        =
| director       =Georg Marischka
| producer       =
| writer         = Winfried Groth
| narrator       =
| starring       =
| music          =
| cinematography =
| editor       =
| distributor    =
| released       = 1965
| runtime        =
| country        = Spain Italy Bulgaria   German
| budget         =
}}
 Italian and German western film adventure directed by Georg Marischka based on a book by Karl May.

==Cast==
*Guy Madison	... 	Jaguar / Karl Hansen
*Rik Battaglia	... 	Antonio Perillo
*Fernando Rey	... 	President Castillo
*William Rothlein	... 	Haukaropora
*Francisco Rabal	... 	Gambusino
*Heinz Erhardt	... 	Professor Morgenstern
*Chris Howland	... 	Don Parmesan
*Walter Giller	... 	Fritz Kiesewetter
*Geula Nuni	... 	Graziella
*Carlo Tamberlani	... 	Anciano
*Raf Baldassarre	... 	Geronimo
*Santiago Rivero	... 	Minister Ruiz
*Ingeborg Schöner	... 	Mrs. Ruiz
*Lyubomir Dimitrov	... 	El Brazo Valiente
*Bogomil Simeonov	... 	Grosso

==See also==
* Karl May movies

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 