Chotushkone
 
{{Infobox film
| name = Chotushkone
| image =  
| caption = Theatrical release poster
| director = Srijit Mukherji
| producer = Reliance Entertainment Dag Creative Media
| writer = Srijit Mukherji
| starring =Aparna Sen Chiranjeet Chakraborty Goutam Ghosh Parambrata Chatterjee
| music = Anupam Roy
| cinematography = Sudeep Chatterjee
| editing = Rabiranjan Maitra
| distributor = Reliance Entertainment
| released =  
| runtime = 148 minutes
| country = India Bengali
| budget = 
| studio =  Dag Creative Media
| film name =  
}} Bengali thriller film directed by Srijit Mukherji, starring Aparna Sen, Goutam Ghosh, Parambrata Chatterjee and Chiranjeet Chakraborty.The film received positive reviews from critics.       The film won the National Film Award for Best Director for Srijit Mukherji at Indias 62nd National Film Awards.

==Plot==
Chotushkone is the story which unfolds four stories inside. Four directors had came together to make a film with four type of plots.Joy,the junior one make them come together.The senior three directors Trina, Dipto and Shakyo also had a past together. A past of a triangle. Recently Trina and Dipto are not in talking terms. Though they had internal problems, still they came together for this project.According to the producer they have to come  to a remote banglo near Bakkhali and share their individual storiy. As the road trip begin, they started sharing their story together. Another subplot is showing the past relations of Trina, Shakyo and Dipto in a earlier time and set. Shakyos story was of a writer who can kill his characters in serials so that they became popular after death. But one night, three of his characters haunt him so badly , it cause his death by heart attack. In past time, the young Dipto and Trina agreed to work with a new producer but with the condition of choosing their own director. The project went to Shakyo. The story come back to present times and showing the four directors in a roadside dhaba where Dipto goes to take a call and nearly face an accident, but also find his story in the meantime. As the trip begin again, he unfolds his story of a lonely crazy smoker whose wife died on the night of their marriage. One night he find no cigarette and in a desperate search, he walked a long way and meet a policeman. The policeman takes his wallet and then the smoker find a guy with cigarette and asked for some fire. Talking the guys last cigarette, he ran only to meet an accident. The story ends and the road trip was now in a middle of a jungle and suddenly the car broke down. So they had to go to another resort of the producer. The past shows, a break up between Dipto and Trina. In the resort Trina says her part of story. Its of a medium who can make a connection between living and dead but she takes assignments from the dead.The next morning ,joy unfolds his story with the name "niyoti" and started telling of a triangle of a director, an actor and an actress. The story was similar to the story of the three senior directors, so Trina protested as it was too personal but Shakyo (the director) was ready to hear. the story reveals that in the past the actor and actress was Dipto and Trina respectively and the third vertex of the triangle was Shakyo. As there was a break up between Trina and Dipto,they stopped working in the project and the producers money was at stake. His wife committed suicide and he was gone crazy. The case was forcefully stooped by the three senior directors i.e. Trina, Dipto and Shakyo. But the producer had a step brother who was loved by both the producer and his wife as their own brother as per Joys storyline and he wanted to avenge his brother and sister-in-law. The story comes to an end revealing that none but Joy himself was the said brother of the producer and he then tried to kill the three in a Russian roulette way but failed as one bullet pierce him, while Trina was saved by Dipto. At the end Dipto and Trina wanted to take care of that mad producer (Joys step brother) but was shocked seeing him as he was still living in the past. They mourn the death of joy with all the other actors seen in the movie.

==Cast==
* Aparna Sen as Trina Sen/Maya (Trinadi)
* Chiranjeet Chakraborty as Dipto/Bedosruti Dey
* Goutam Ghosh as Sakyo/Animesh
* Parambrata Chatterjee as Joyobroto (Joy)
* Payel Sarkar as Nilanjana
* Indrasish Roy as Ritwik Rahul Banerjee as Amitava
* Kaushik Ganguly as Mr. Gupta (Producer)
* Barun Chanda as Moloy Sen (Trinas Husband)
* Debolina Dutta as Sonia 
* Koneenica Banerjee as Shree
* Biswajit Chakraborty as a furniture dealer
* Ritabhari Chakraborty as a student Neel Mukhyopadhyay as Rajkumar Mukherjee
* Arpita Pal as Jyotsna Mukherjee
* Shantilal Mukherjee as Police Officer

== Awards == 
The film won National Awards for Best Direction, Cinematography (for cinematographer Sudeep Chatterjee)and Original Screenplay at the 62nd National Film Awards of India.   

==Music==
The music of the film has been composed by Anupam Roy. He himself has penned the lyrics (apart from one Rabindra Sangeet i.e. Chirosakha He).
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Basanto Eshe Geche (Male)"
| Anupam Roy
|- 
| 2
| "Mone Porar Gaan"
| Somlata Acharyya Chowdhury
|- 
| 3
| "Boba Tunnel"
| Anupam Roy
|- 
| 4
| "Basanto Eshe Geche (Female)"
| Lagnajita Chakraborty
|- 
| 5
| "Chirosakha He"
| Srikanto Acharya
|- 
| 6
| "Shetai Satyi" 	
| Rupankar Bagchi
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 