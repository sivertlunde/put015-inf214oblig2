Trip with the Teacher
{{Infobox Film
| name           = Trip with the Teacher
| image          = Trip_with_the_Teacher_Poster.jpg
| image_size     = 
| caption        = 
| director       = Earl Barton
| producer       = Earl Barton
| writer         = Earl Barton
| starring       = Brenda Fogarty Zalman King 
| music          = Beth Robbins Earl Barton
| released       = 1975
| runtime        = 95 min.
| country        = United States
| language      = English
}}
 produced and directed by Earl Barton.   The film has been retitled several times and has been referred to as Deadly Field Trip (VHS release), Duell bis zum Verrecken (Germany), Kiss the Teacher... Goodbye (USA promotional title), and Stupro selvaggio  (Italy).

==Synopsis==
Four teen-age girls (along with their teacher and bus driver) from Los Angeles are on a field trip in the desert. On the way to their destination their bus breaks down. While stranded they are approached by 3 bikers. When Jay (one of the bikers) tries to help the bus driver, the other two bikers start to harass the four girls. As soon as the teacher (Miss Tenny) observes this, she immediately defends the girls, however, is assaulted for her interruption. After a few scuffles the group of bikers decide to help the stranded bunch and tow them to a remote cabin, with less than good intentions in store. Another altercation ensues, when Als promise to take them to safety is now obviously broken. Almost as soon as the bus driver (Marvin) steps in, he is run over and killed by Al and his brother, Pete. To keep everyone quiet, Al and Pete holds everyone hostage in the abandoned cottage near the murder site.

==Cast==

*Brenda Fogarty as Miss Tenny
*Robert Gribbin as Jay
*Zalman King as Al
*Robert Porter as Pete
*Dina Ousley as Bobbie
*Jill Voigt as Tina
*Cathy Worthington as Julie
*Susan Russell as Pam
*Jack Driscoll as Marvin
*Edward Cross as Station Attendant
*David Villa as Station Customer

==References==
 

==External links==
*  

 
 
 
 
 
 

 