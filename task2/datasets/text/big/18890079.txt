My Brother Cicero
{{Infobox Film 
| name           =  
| image          = 
| caption        =  
| director       = Tony Nittoli 
| producer       =  
| writer         = Louis Mandylor Tony Nittoli
| narrator       = 
| starring       = Costas Mandylor Louis Mandylor Tony Nittoli 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Louis Mandylor productions 
| released       = United States: June 18, 1998
| runtime        = 28 minutes 
| country        = United States English 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
|}}
My Brother Cicero is a 1998 comedy short film written by Louis Mandylor and Tony Nittoli starring Louis Mandylor, Costas Mandylor and Tony Nittoli. Filmed in Hollywood, it garnered two independent film awards and generated mostly positive reviews from critics.

==Plot==
The harsh tale of an abusive relationship between a man and his cat - who steals his women and drugs, kills his mobster boss, and altogether terrorizes him. 

==Cast==
* Costas Mandylor as Cicero 
* Louis Mandylor as Nicky 
* Tony Nittoli as Jimmy the Cat

==Crew==
* Producer Louis Mandylor
* Director: Tony Nittoli
* Writers: Tony Nittoli & Louis Mandylor

==Awards==
* 1998 Best Independent Film at Societes des Auteurs award at the Brussels Independent Film Festival
* 1999 Best Short Film at the New York Underground Film Festival 

==References==
 

==External links==
*  
*  

 
 