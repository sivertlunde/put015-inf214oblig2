Tango libre
 
{{Infobox film
| name           = Tango libre
| image          = Tango libre POSTER.jpg
| caption        = Film poster
| director       = Frédéric Fonteyne
| producer       = 
| writer         = Philippe Blasband Anne Paulicevich
| starring       = François Damiens
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Belgium Luxembourg France
| language       = French   Spanish
| budget         = 
}}
Tango libre is a 2012 comedy film directed by Frédéric Fonteyne. In January 2014 the film received ten nominations at the 4th Magritte Awards. 

==Cast==
* François Damiens as Jean-Christophe
* Sergi López i Ayats as Fernand Jan Hammenecker as Dominic
* Anne Paulicevich as Alice
* Zacharie Chasseriaud as Antonio
* Christian Kmiotek as Michel, le chef-quartier
* David Murgia as Luc, le jeune gardien
* Frédéric Frenay as Patrick, le tatoué
* Dominique Lejeune as Popeye
* Marc Charlet as Marco

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 