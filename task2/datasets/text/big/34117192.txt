Pereval
{{Infobox film
| name = Pereval
| image = 
| image_size = 
| caption = 
| director = Vladimir Tarasov
| producer = 
| writer = Kir Bulychov
| starring = 
| music = 
| cinematography = 
| editing = 
| distributor =   Soyuzmultfilm
| released = 1988 (USSR)
| runtime = 30 mins
| country = Soviet Union Russian
| budget = 
| preceded_by = 
| followed_by = 
}}

Pereval (The Pass;   and written by Kir Bulychov. It is adapted from the first four chapters of Bulychyovs novel, Pereval (printed in English as Those Who Survive).

==Plot==

On a distant, snow-covered planet, a starship from Earth crashes. Due to dangerous radiation levels, the survivors have to evacuate far away. Over years, the radiation levels go down but all attempting to return to the ship die when crossing a treacherous mountain pass, due to a combination of the elements and wild animals who come out at night.

Finally, when only a few survivors are left, their teenaged children - all who were born on the world - and one of the adults decide to try to reach the ship one last time, to gain needed supplies and set off a beacon that would summon a rescue mission.

==External links==
* 
*  at YouTube
*  at YouTube
*  on io9
 

 
 
 
 
 


 
 