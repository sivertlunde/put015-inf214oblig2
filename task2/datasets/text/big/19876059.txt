That Man in Istanbul
 
{{Infobox film
| name           = That Man in Istanbul
| image          = That Man in Istanbul FilmPoster.jpeg
| caption        = Film poster
| director       = Antonio Isasi-Isasmendi
| producer       = Antonio Isasi-Isasmendi Nat Wachsberger
| writer         = Howard Clewes Lluís Josep Comerón Jorge Illa Antonio Isasi-Isasmendi Giovanni Simonelli Nat Wachsberger
| starring       = Horst Buchholz
| music          = Georges Garvarentz
| cinematography = Juan Gelpí
| editing        = Juan Pallejá
| distributor    = 
| released       =  
| runtime        = 114 minutes
| country        = Spain Italy France
| language       = English
| budget         = 
}}

That Man in Istanbul ( ,  ,  ) is a 1965 English-language European international co-production adventure film directed by Antonio Isasi-Isasmendi and starring Horst Buchholz.    It was released in the United States by Columbia Pictures.

That Man in Istanbul is an eurospy comedy film. Its English-language title is likely a reference to the 1964 eurospy comedy film That Man from Rio.

==Cast==
* Horst Buchholz as Tony Mecenas
* Sylva Koscina as Kenny, FBI Agent
* Mario Adorf as Bill
* Perrette Pradier as Elisabeth Furst
* Ángel Picazo as Inspector Mallouk
* Klaus Kinski as Schenck
* Gérard Tichy as Charly Cohen
* Christiane Maybach as Josette
* Álvaro de Luna (actor)|Álvaro de Luna as Bogo
* Gustavo Re as Brain, the Accountant
* George Rigaud as CIA Chief
* Henri Cogan as Cogan (as Henry Cogan)
* Agustín González as Gunther
* Umberto Raho as Prof. Pendergast
* Marta Flores as Lady in Limousine
* Barta Barri as Captain
* Luis Induni as Thug
* Manuel Bronchud as Thug
* Alberto Dalbés as Thug Don Maxwell as CIA Operative
* Claude Cerval as Brandt
* Antonio Molino Rojo as Picture presenter

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 