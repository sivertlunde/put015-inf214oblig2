Quality of Life (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name = Quality of Life
| image = QualityofLife_dvd.jpg
| caption = DVD cover
| director = Benjamin Morgan
| producer = Tyler Dove Benjamin Morgan Meika Rouda
| writer = Brian Burnam Benjamin Morgan
| starring = Lane Garrison Brian Burnam
| cinematography = Kev Robertson
| editing = Sharon Franklin
| distributor = The Relentless Company
| released =  
| runtime = 85 minutes
| country = United States
| language = English
| budget =
| gross =
}} Mission District of San Francisco. Directed by Benjamin Morgan, Quality of Life stars Lane Garrison, Brian Burnam, Luis Saguar and Mackenzie Firgens. Morgan co-wrote the screenplay with Burnam, who is a former graffiti writer. The film was shot and edited in the Mission District, home to one of the worlds most active and influential graffiti scenes.

The film was shown only at film festivals in 2004, and went in limited release October 12, 2005.

== Cast ==
* Lane Garrison as Heir
* Brian Burnam as Vain
* Luis Saguar as Pops
* Mackenzie Firgens as Lisa
* Fred Pitts as Robert
* Andrew A. Rolfes as Officer Charles
* Tajai as Dino
* Bryna Weiss as Grandma
* Timothy Garcia as Jimmy "Tad"
* Ezra J. Stanley as Kid

== Awards ==
* Special Mention jury award, Berlin International Film Festival
* Best Youth Film, Stockholm International Film Festival Jr. 

== Soundtrack ==
No official soundtrack was released, but these tracks appeared in movie:

Quality of Life (spelfilm)
Andre Nickatina - The Soul of a Coke Dealer
Hi Fi Drowning - Big Spring
Bonobo - Noctuary
Maroons - Best Bonus Beat
Top.R. - Soul Cancer
Hi Fi Drowning - Atomatic
Halou - Tube Fed
Halou - Milkdrunk
MR Lif - New man Theme
Bonobo - Change Down
Amon Tobin - Get Your Snack On
Lifesavas - Soldierfied
Meat Beat Manifesto feat. DJ Collage - Echo in Space Dub
Meat Beat Manifesto - Asbestos Lead Asbestos (toxic mix)
Calhoun (Tim Locke) - Sunken Eyes, Shakey Knees
Modest Mouse - BROKE
Hi Fi Drowning - Dim
Halou - Honey Thief
Sebadoh - License to Confuse
Good Riddance - The Hardest Part
Freedy Johnston - This Perfect World
Built To Spill - Weather
8 Stories - In Sleep

== References ==
*   New York Times. Accessed 21 July 2010.

== External links ==
*  
*  
*  

 
 
 
 
 
 
 


 