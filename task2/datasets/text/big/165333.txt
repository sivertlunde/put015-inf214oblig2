The Endless Summer
 

{{Infobox film|
  name           = The Endless Summer |
  image          = The_Endless_Summer.JPG|
  caption        = Theatrical release poster |
  director       = Bruce Brown |
  producer       = Bruce Brown |
  writer         = Bruce Brown |
  narrator       = Bruce Brown |
  starring       = Mike Hynson Robert August Miki Dora |
  cinematography = Bruce Brown |
  editing        = Bruce Brown |
  distributor    = Cinema V Monterey Media |
  released       = 15 June 1966 |
  runtime        = 95 minutes |
  country        = United States |
  language       = English |
  music          = The Sandals |
  budget         = $50,000 |
  gross          = $20 million  |
}}

The Endless Summer is a seminal 1966 surf movie.
 Phil Edwards and Butch Van Artsdalen, also appear.

Its title comes from the idea, expressed at both the beginning and end of the film, that if one had enough time and money it would be possible to follow the summer around the world, making it endless. The concept of the film was born through the suggestion of a travel agent to Bruce Brown during the planning stages of the film. The travel agent suggested that the flight from Los Angeles to Cape Town, South Africa and back would cost $50 more than a trip circumnavigating the world.  After which, Bruce came up with the idea of following the summer season by traveling around the world.
 surf rock soundtrack to the film was provided by The Sandals. The "Theme to the Endless Summer" was written by Gaston Georis and John Blakeley of the Sandals.  It has become one of the best known film themes in the surf movie genre.
 Thicker Than Water.

==Development== 8 mm movie camera to photograph surfers from California. Once Brown got back to the state, he edited his footage into an hour-long film. Surfer Dale Velzy showed it at his San Clemente shop, charging 25 cents for admission. Velzy bought Brown a 16 mm camera and together they raised $5,000 to make Slippery When Wet, Browns first "real" surf film. 
 North Shores big surf. On the plane ride over, the novice filmmaker read a book about how to make movies. Brown said, "I never had formal training in filmmaking and that probably worked to my advantage".  By 1962, he had spent five years making one surf film per year. He would shoot during the fall and winter months, edit during the spring and show the finished product during the summer. Brown remembered, "I felt if I could take two years to make a film, maybe I could make something special".  To do this, he would need a bigger budget than he had on previous films. To raise the $50,000 budget for The Endless Summer, Brown took the best footage from his four previous films and made Waterlogged. 

Brown took his completed film to several Hollywood studio distributors but was rejected because they did not think it would have mainstream appeal.  In January, he took The Endless Summer to Wichita, Kansas for two weeks where moviegoers lined up in snowy weather in the middle of winter and it went on to selling out multiple screenings.    Distributors were still not convinced and Brown rented a theater in New York City where his film ran successfully for a year.  After the success of the run at New Yorks Kips Bay Theater, Don Rugoff of Cinema 5 distribution said he did not want the film or poster changed and wanted them distributed as is, thus Brown selected him over other distributors who wished to alter the poster. 

==Reaction==
When The Endless Summer debuted, it grossed $5 million domestically Tom Lisanti, Hollywood Surf and Beach Movies: The First Wave, 1959-1969, McFarland 2005, p270  and over $20 million worldwide. 

Roger Ebert said of Browns work, "the beautiful photography he brought home almost makes you wonder if Hollywood hasnt been trying too hard".    Time (magazine)|Time magazine wrote, "Brown leaves analysis of the surf-cult mystique to seagoing sociologists, but demonstrates quite spiritedly that some of the brave souls mistaken for beachniks are, in fact, converts to a difficult, dangerous and dazzling sport".   

In his review for The New York Times, Robert Alden wrote, "the subject matter itself—the challenge and the joy of a sport that is part swimming, part skiing, part sky-diving and part Russian roulette—is buoyant fun".   

===Legacy===
The then-unknown break off Cape St. Francis in South Africa became one of the worlds most famous surfing sites thanks to The Endless Summer.   

In 2002, The Endless Summer was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

== Sequels ==  Pat OConnell longboard surfing. windsurfing and bodyboarding. The film illustrates how far surfing had spread, with footage of surf sessions in France, South Africa, Costa Rica, Bali, Java (island)|Java, and even Alaska.

In 2000, Dana Brown, Bruces son, released The Endless Summer Revisited, which consisted of unused footage from the first two films, as well as original cast interviews.

==References==
 

==External links==
 
* 
* 
*  
*  

 
 
 
 
 
 
 
 
 