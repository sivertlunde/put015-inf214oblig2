The Forger (1928 film)
{{Infobox film
| name           = The Forger
| image          =
| caption        =
| director       = G.B. Samuelson
| producer       = 
| writer         = Edgar Wallace
| starring       = Nigel Barrie Lillian Rich James Raglan   Winter Hall
| music          =
| cinematography = 
| editing        =  British Lion
| distributor    = Ideal Films
| released       = December 1928
| runtime        = 7,305 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
}}
  silent crime film directed by G.B. Samuelson and starring Nigel Barrie, Lillian Rich and James Raglan.  It is based on the 1927 novel The Forger by Edgar Wallace. It was made at Southall Studios.

==Cast==
* Nigel Barrie - Doctor Cheyne Wells  
* Lillian Rich - Jane Leith
* James Raglan - Peter Clifton
* Winter Hall - John Leith
* Sam Livesey - Inspector Rouper
* Derrick de Marney - Basil Hale

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 