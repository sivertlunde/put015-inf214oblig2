Viva Buddy
{{Infobox Hollywood cartoon
| series = Looney Tunes (Buddy (Looney Tunes)|Buddy)
| image =
| caption = Jack King
| story_artist =
| animator = Frank Tipper, Cal Dalton Jack Carr, Billy Bletcher (both uncredited) Norman Spencer
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. The Vitaphone Corporation
| release_date = September 29, 1934 (USA)
| color_process = Black-and-white
| runtime = 6 minutes
| movie_language = English
| preceded_by = Buddy the Detective (1934)
| followed_by = Buddy the Woodsman (1934)
}}
 animated short Jack King; musical direction was by Norman Spencer.

==Summary==
We come to Buddy, who, ambling through a   table, where jumping beans play for them. Buddy slips one of the lively beans into the mouth of a man at the bars piano, and he begins to play with his toes. The people are roused by this, and begin happily dancing & drinking; a makeshift mariachi band plays in tune; Buddy is apparently able to play his guitar with his teeth; his guitar can also play itself; Buddy also can play while his feet hang in the air!

Enter the outlaw Pancho, who does some fancy gunwork to frighten the townsfolk and ties up his horse by shooting an hole through a stake & tying the horses tail through the hole. The villain steps into the saloon & starts firing; the patrons (except Buddy) shout: "Pancho!" "Pancho!" "Pancho!" (and then, from a bed that folds out from the wall, in succession, the four  . Our indignant hero boldly squeezes the remainder of the fruit in his adversarys face. Pancho puts one of his guns to Buddys head, but declares that he likes the little fellow, and commands him to play the piano (into which he has been backed.) List of Looney Tunes and Merrie Melodies characters#Cookie|Cookie,
heretofore absent, begins to dance to Buddys tune, "Famabella". Pancho proves himself a dancer as well, to the anger of an unstaged Cookie, and requests a kiss of her. Of course, the scoundrel winds up with his backside through the canteen door, & back again into the canteen by the fury of a cranky goat. Pancho again makes an advance on Cookie, and an annoyed Buddy fires a serving fork at Panchos behind by means of a violoncello|cello. "I kill you to little pieces!" shouts Pancho. But both of the dastards pistols are blocked by a two-pronged candelabra thrown, with marvellous accuracy, by Buddy. Pancho shows his whip, and with it snags Buddy from across the room: Buddy punches his captor in vain. Eventually, Our Hero grabs a ceiling fixture that spins both competitors round and round, flinging them finally into an easily  breaking counter. The cartoon concludes on a friendly note: Pancho takes Buddy by the shoulder & announces that he was "only fooling." The competitors together heartily laugh.

==Cookies appearance==
In those Buddy shorts supervised by Jack King, Cookie generally has blond, braided hair; it is odd, then, that this cartoon should feature Buddys sweetheart with her more traditional black hair.

==The Marx brothers== Duck Soup.

==Uniqueness of the title==
This is the only Looney Tune starring Buddy whose title does not begin with some form of the main characters name.

==Dating discrepancy== relevant section of the article Buddys Circus.

==References==
 

==External links==
*  

 

 
 
 
 
 