Once Upon a Time in Phuket
{{Infobox film
| name           = Once Upon a Time in Phuket
| image          = En gång i Phuket.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Staffan Lindberg
| producer       = Lena Rehnberg
| screenplay     = Peter Magnusson
| narrator       = 
| starring       = Peter Magnusson
| music          = 
| cinematography = Simon Pramsten
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 105 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Once Upon a Time in Phuket (Swedish: En gång i Phuket) is a 2012 Swedish film directed by Staffan Lindberg.

==Synopsis==
Sven (Peter Magnusson), an average Swede, is bored with his life and wants to experience new adventures. He travels to Thailand to write a novel and in the process gets to meet a range of peculiar characters and learns a few things about love, friendship and himself.

==Cast==
* Peter Magnusson as Sven
* Susanne Thorson as Anja
* Jenny Skavlan as Gitte
* David Hellenius as Georg
* Grynet Molvig as Svens mother
* Alexandra Rapaport as Siri
* Claes Månsson as the priest
* Frida Westerdahl as Estelle
* Frida Hallgren as Karin
* Matias Varela as Jean-Luc
* Henrik Norlén as Johan Pålman
* Yngve Dahlberg as Druve
* Johan Hallström as Magnus
* Mårten Klingberg as Fredrik
* Ella Fogelström as Sonja
* Lisbeth Johansson

==External links==
*  
*  

 
 
 
 