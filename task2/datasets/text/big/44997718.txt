Fedora (1918 film)
{{Infobox film
| name           = Fedora
| image          =
| alt            = 
| caption        = 
| director       = Edward José
| producer       = Adolph Zukor
| screenplay     = Charles E. Whittaker
| based on       = Fédora by Victorien Sardou
| starring       = Pauline Frederick Alfred Hickman Jere Austin William L. Abingdon John Merkyl
| music          = 
| cinematography = Ned Van Buren Hal Young 
| editor         =
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Edward José and written by Charles E. Whittaker, after the play with the same name by Victorien Sardou. The film stars Pauline Frederick, Alfred Hickman, Jere Austin, William L. Abingdon and John Merkyl. The film was released on August 4, 1918, by Paramount Pictures.  

==Cast==
*Pauline Frederick as Princess Fedora
*Alfred Hickman as Gretch
*Jere Austin as Louis Ipanoff
*William L. Abingdon as Gen. Zariskene
*John Merkyl as Count Vladimir Androvitch 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 