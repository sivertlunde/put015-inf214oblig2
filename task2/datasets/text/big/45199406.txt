Miss Hanafi
{{Infobox film
| name           = Miss Hanafi  (El Anesa Hanafy) 
| image          = 
| alt            = 
| caption        = 
| film name      = Arabic language|Arabic: الآنسة حنفي
| director       = Fatin Abdel Wahab
| producer       = Galil al-Bendari
| writer         = Galil al-Bendari
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       =  
| music          = 
| cinematography = Robert Tamba
| editing        = 
| studio         =  
| distributor    =  
| released       = 1954   (Egypt)
| runtime        = 93 minutes
| country        = Egypt
| language       = Arabic
| budget         = 
| gross          =  
}}
 sex change.    Starring by Ismail Yaseen and Omar El-Hariri, the film is based upon an actual 1947 news article about a woman-to-man instance but with the gender switch reversed to be man-to-woman for comedy effect.      

Miss Hanafi is listed among the most important 100 films in the history of Egyptian cinema.   In his book Dream Makers on the Nile, author Mustafa Darwish called it Ismail Yasseens "most famous and funniest film".     

==Plot==
Working class Hassan (Omar al-Hariri) and the rich Hanafi are rivals for Nawaeims (Affaf Kamel Elsabahy|Magda) love. Going to the hospital to be treated for a horrible wedding night stomach pain, Hanafi receives an accidental sex change operation. After months of recuperation, he tries to pick up the pieces of his life as an unattractive, mannish woman named Fifi. She becomes the opposite of Hanafi. Where he was brusque, she is flirtatious.

==Cast==
* Ismail Yasseen as Hanafi / Fifi
* Omar El-Hariri as Hassan Magda as Nawaem
* Wedad Hamdy as Zakia
* Suleiman Naguib as Hassona
* Abdel Fatah Al Kasri		
* Zeinat Sedki

==Reception==
Mada Masr praised the film, calling it one of Egypts cinematic gems. The expanded that the film "breaks every social norm and cinematic cliché of its time. Boy loves girl, boy chases girl, boy sings about girl’s beautiful eyes, and finally boy marries girl. But in Miss Hanafi the boy is the girl! And he/she chooses celibacy and scolds those who chose to marry."   They also explained that while other films of the 50s might have had story lines where the men dressed as women, this one was "unusual in its progressive questioning around gender because it is very early, very funny, not particularly political, and it’s set in a working-class neighborhood."   

The Al-Hanager Arts Centre in Cairo celebrated the work of director Fettin Abdel-Wahab as part of the 2007 Egyptian Centennials exhibition, noting him as a pioneer of the social comedy school of Egyptian cinema, and his film Miss Hanfi as among the most frequently watched and influential.    

In 2006, Miss Hanafi was determined by Bibliotheca Alexandrina as one of the most important 100 films in the history of Egyptian cinema.   

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 
 
 
 
 
 