Kokusai himitsu keisatsu: Kagi no kagi
{{Infobox film
| name            = Kokusai himitsu keisatsu: Kagi no kagi
| image           = 
| image size      = 
| caption         = 
| director        = Senkichi Taniguchi
| producer        = Shin Morita Tomoyuki Tanaka
| writer          = Hideo Ando
| starring        = Tatsuya Mihashi Akiko Wakabayashi Mie Hama Tadao Nakamaru Susumu Kurobe Sachio Sakai Hideyo Amamoto Tetsu Nakamura Akemi Kita
| music           = Sadao Bekku
| cinematographer = Kazuo Yamada
| editing         = 
| distributor     = Toho
| released        =  
| runtime         = 93 minutes
| country         = Japan
| language        = Japanese
| budget          = 
| gross           =
}}
 , also known as Key of Keys, is a 1965 Japanese comedy-spy film directed by Senkichi Taniguchi.    It is the fourth installment of five films in the "Kokusai himitsu keisatsu" series. The film is a parody of James Bond-style spy movies, and was used by Woody Allen, along with footage from the third installment, in one of his first films, Whats Up, Tiger Lily?, in which the original dialogue is redubbed in English to make the plot about a secret egg salad recipe.            

== Plot ==
Kitami is requested by the intelligence director, Suritai, to steal a large amount of money from the anti-government guerrillas who fund gangs from Gegen. However, once Kitami infiltrates a Gegen ship in disguise, he discovers there is no cash in the safe, only a cipher on a piece of paper.  

== Cast ==
*Tatsuya Mihashi as Jiro Kitami
*Akiko Wakabayashi as Bai-Lan
*Mie Hama as Miichin   
*Tadao Nakamaru as Gegen
*Susumu Kurobe as He-Qing Cai
*Sachio Sakai as Inagawa
*Hideyo Amamoto as Numaguchi
*Tetsu Nakamura as Suritai
*Akemi Kita as Yoko

== References ==
 

== External links ==
* 


 
 
 
 
 
 
 
 


 