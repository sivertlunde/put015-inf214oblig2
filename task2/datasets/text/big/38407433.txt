Inequality for All
 
 
Inequality for All is a 2013 documentary film directed by Jacob Kornbluth.  The film examines widening income inequality in the United States. The film is presented by American economist, author and professor Robert Reich.   The film premiered at the 2013 Sundance Film Festival in the Documentary Competition section,  and won a U.S. Documentary Special Jury Award for Achievement in Filmmaking.  

Reich distills the story through the lens of widening income inequality—currently at historic highs—and explores what effects this increasing gap has not only on the U.S. economy but also American democracy itself. At the heart of the film is a simple question: What is a good society and what role does the widening income gap play in the deterioration of the nations economic health?

The film was distributed by RADiUS-TWC in Fall 2013.

== Plot summary ==
In the wake of the   now own more wealth than the bottom 150 million combined. While this level of inequality poses a serious risk to all Americans, regardless of income level, much of the rhetoric on this subject has been fueled by anger and resentment from a frustrated middle class who feel their birthright—the American Dream—has been taken away from them.

Robert Reich, author, professor at the University of California, Berkeley, official in three administrations, including United States Secretary of Labor under Bill Clinton, narrates the film. Reich is a thinker on the topic of inequality, having spoken on the subject for nearly three decades.

Structurally, the film is organized around a central "spine", in which Reich speaks directly to the audience about key economic topics. As Reich elaborates on each sub-topic, the film cuts to newly shot footage of real people and real lives, as well as archival footage and experts that address the points of his lecture. Throughout the film are graphics that expound upon statistical data.

== About the filmmakers ==
Jacob Kornbluth is the award-winning director of Sundance Film Festival selections Haiku Tunnel and The Best Thief in the World, and has been a fellow in the Sundance Screenwriting and Directing Labs, and a North American finalist for the Sundance/NHK New Filmmakers Award.

Jen Chaiken is an Emmy award-winning narrative and documentary producer with credits including HBO’s My Flesh and Blood and Naked States, Family Name, and Big Eden, and is also a principal partner of the independent film production company 72 Productions.
 Afternoon Delight (in post), and a principal partner of the independent film production company 72 Productions.

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 