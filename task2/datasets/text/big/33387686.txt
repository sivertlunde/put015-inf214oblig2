You Can't Get Away with Murder
{{Infobox film
| name           = You Cant Get Away with Murder
| image          = Poster - You Cant Get Away With Murder 01.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Lewis Seiler
| producer       = Jack L. Warner Hal B. Wallis Samuel Bischoff
| writer         = Jonathan Finn Lewis E. Lawes
| based on       = Chalked Out (1937) play
| screenplay     = Robert Buckner Don Ryan Kenneth Gamet
| narrator       =
| starring       = Humphrey Bogart Gale Page Billy Halop
| music          = Heinz Roemheld
| cinematography = Sol Polito
| editing        = James Gibbon
| distributor    = Warner Bros.
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         =
}}
 Dead End Kid" leader Billy Halop. The movie is one of Bogarts studio B pictures filmed before his famous breakthrough in High Sierra two years later. 

Alternate title for this film, Chalked Out.

==Plot==
In New Yorks Hells Kitchen, Manhattan|Hells Kitchen, young Johnny Stone (Billy Halop) goes against the advice of his sister Madge (Gale Page) and hooks up with mobster Frank Wilson (Humphrey Bogart).

First Johnny and Frank rob a gas station. Later, Johnny takes a gun belonging to Madges fiancé Fred Burke (Harvey Stephens) and lends it to Frank to use in a pawnshop robbery. This time the owner resists and sounds an alarm; Frank kills him and leaves the gun there, so Johnny cannot return it to Freds room as he intended. Fred has no alibi and is arrested and convicted of murder. Meanwhile, based on fingerprint evidence Frank and Johnny are arrested and convicted of the gas station robbery. All three men are sent to Sing Sing.

Johnny is not a hardened criminal like Frank, and is tortured by the thought that Fred is facing execution for their crime. But Frank repeatedly reminds Johnny that he must continue "playing dumb", as both of them face execution if either confesses. The prison authorities are suspicious of their attitude to each other and transfer Johnny from working in the prison shoe factory alongside Frank to the prison library run by a mild-mannered older convict known as Pop (Henry Travers).

Johnny expects Fred to be cleared on appeal, not knowing that Frank also planted stolen property as evidence against him. When the appeal is denied, Johnnys pangs of conscience increase. By now Madge is convinced that Johnny knows the true killer and begs him to talk, still not suspecting Johnnys own involvement. Pop also appeals to Johnnys conscience. The district attorney guesses the truth, but without evidence cannot even request a stay of Freds execution. Through all this, Johnny continues to "play dumb".

On the day set for Freds execution, Frank and Johnny join in a jailbreak. In that situation Johnny is finally willing to tell the truth. He produces a written confession that he stole the gun and Frank did the shooting, leaving it for Pop to find after the breakout. But Frank sees him drop the paper and takes it instead. He decides to kill Johnny after the jailbreak.

But the jailbreak fails, ending with Frank and Johnny in a railroad boxcar surrounded by prison guards. Frank has a gun and starts shooting at the guards from concealment. When they return fire, Frank shoots Johnny and puts the gun next to him, then gives himself up, claiming that he was unarmed.

But, although mortally wounded, Johnny survives long enough to tell the truth, implicating Frank for both murders and finally clearing Fred.

==Cast==
* Humphrey Bogart as Frank Wilson
* Gale Page as Madge Stone
* Billy Halop as Johnny Stone
* John Litel as Attorney Carey
* Henry Travers as Pop
* Harvey Stephens as Fred Burke
* Harold Huber as Tom Scappa
* Joe Sawyer as Red
* Joe Downing as Smitty
* George E. Stone as Toad Joe King as Principal Keeper (P.K.)
* Joseph Crehan as Warden
* John Ridgely as Gas Station Attendant
* Herbert Rawlinson as District Attorney

==References==
 	

==External links==
*   in the Internet Movie Database
*  
*  

 

 
 
 
 
 
 
 