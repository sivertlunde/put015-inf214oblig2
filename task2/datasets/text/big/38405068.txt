Aur Pappu Paas Ho Gaya
{{Infobox film
| name=Aur Pappu Paas Ho Gaya
| image=
| alt=
| caption=
| director=Shantilal Soni
| producer=Amit Srivastava
| story=Shantilal Soni
| based on=
| screenplay=
| starring=Kashmira Shah Krishna Abhishek
| music=Ravi Chopra
| cinematography=Hasmukh Rajput
| editing=Santosh Pawar
| studio=Filmcity Real Square Production Pvt. Ltd.
| distributor=
| released=  
| runtime=
| country=India
| language=Hindi
}}

Aur pappu pass ho gaya is a 2007 Bollywood Drama film directed by Shantilal Soni and produced by Amit Srivastava.The film features Krishna Abhishek and Kashmira Shah in title roles with ensemble cast of supporting actors.  

==Cast==
*Krishna Abhishek as Pappu
*Kashmira Shah as Kiran Chauhan
*Jackie Shroff as Sudhakar Sudhabhai Chauhan
*Sikandar Kharbanda as Biloo
*Alok Nath as Pappus dad
*Mushtaq Khan as Pathan
*K. K. Goswami

==Music==
The music of the film is composed by Ravi Chopra.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| title1 = Meri Chahaton Ki Abhijeet
| lyrics1 = 
| length1 = 5:23
| title2 = Pappu Paas Ho Gaya
| extra2 = Abhijeet
| lyrics2 = 
| length2 = 4:08
| title3 = Jaane Jaana
| extra3 = Kunal Ganjawala, Alka Yagnik
| lyrics3 = 
| length3 = 5:23
| title4 = Keep It Cool
| extra4 = Bali Brahmbhatt
| lyrics4 = 
| length4 = 4:14
| title5 = Paaji Ghanti Baji
| extra5 = Sandeep Garg,Amit Shrivastav
| lyrics5 =
| length5 = 5:37
| title6 = Ishq Hai Jua
| extra6 = Sonu Kakkad
| lyrics6 = 
| length6 = 5:12
}}

==References==
 

==External links==
*  

 
 

 