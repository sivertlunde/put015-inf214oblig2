Babes on Broadway
{{Infobox film
| name           = Babes on Broadway
| image          = Babes on broadwaymp.jpg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = Busby Berkeley Vincente Minnelli (uncredited)
| producer       = Arthur Freed
| writer         = Fred F. Finklehoffe
| starring       = Mickey Rooney Judy Garland
| music          = Score:   (music) Roger Edens (music) E.Y. Harburg (lyrics) Ralph Freed (lyrics) et al. (all uncredited)
| cinematography = Lester White
| editing        = Fredrick Y. Smith
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $955,000  . 
| gross          = $3,859,000 
}}
 Babes in Strike Up the Band (1940).  Songs in the film include "Babes on Broadway" by Burton Lane (music) and E.Y. "Yip" Harburg (lyrics), and "How About You?" by Lane with lyrics by Ralph Freed, the brother of producer Arthur Freed. The movie ends with a minstrel show performed by the main cast in blackface.

==Cast==
*Mickey Rooney as Tommy Williams
*Judy Garland as Penny Morris
*Fay Bainter as Miss "Jonesy" Jones
*Virginia Weidler as Barbara Josephine "Jo" Conway  
*Ray McDonald as Ray Lambert
*Richard Quine as Morton "Hammy" Hammond
*Donald Meek as Mr. Stone
*Alexander Woollcott as himself
*Luis Alberni as Nick
*James Gleason as Thornton Reed
*Emma Dunn as Mrs. Williams
*Anne Rooney as Jenny
*Ava Gardner as Pitt-Astor Girl
*Will Lee appears uncredited as Shorty the waiter.
*Donna Reed appears uncredited as a secretary.
*Margaret OBrien appears uncredited as an ambitious little girl at an audition who melodramatically pleads: "Dont send my brother to the chair! Dont let him burn!"

==Production== Babes in Strike Up Good News (1947) and Summer Stock (1950) were also originally planned to become part of the series.  Judy Garland starred in the latter with Gene Kelly. 
 Las Vegas David Rose. She was 19 years old.

==Musical numbers==
*"Babes on Broadway" (Main Title) (MGM Studio Chorus)
*"Anything Can Happen in New York" (Mickey Rooney, Ray McDonald, and Richard Quine)
*"How About You?" (Judy Garland and Mickey Rooney)
*"Hoe Down" (Judy Garland, Mickey Rooney, Six Hits and a Miss, The Five Musical Maids, and MGM Studio Chorus)
*"Chin Up! Cheerio! Carry On!" (Judy Garland, St. Lukes Episcopal Church Choristers, and MGM Studio Chorus)
*Ghost Theater Sequence: Cyrano de Bergerac" (Mickey Rooney as Richard Mansfield)
**"Marys a Grand Old Name" (Judy Garland as Fay Templeton)
**"Shes Ma Daisy" (Mickey Rooney as Harry Lauder)
**"Ive Got Rings On My Fingers" (Judy Garland as Blanche Ring)
**"La Marseillaise" (Judy Garland as Sarah Bernhardt)
**"The Yankee Doodle Boy" (Mickey Rooney and Judy Garland)
*"Bombshell from Brazil" (Judy Garland, Mickey Rooney, Richard Quine, Ray McDonald, Virginia Weidler, Anne Rooney, Robert Bradford, and MGM Studio Chorus)
*"Mama Yo Quiero" (Mickey Rooney)
*Minstrel Show Sequence:
**"Blackout Over Broadway" (Judy Garland, Mickey Rooney, Ray McDonald, Virginia Weidler, Richard Quine, Anne Rooney and MGM Studio Chorus) By the Light of the Silvery Moon" (Ray McDonald)
**"Franklin D. Roosevelt Jones" (Judy Garland and MGM Studio Chorus)
**"Old Folks at Home" (Eddie Peabody on banjo, dubbing for Mickey Rooney)
**"Alabamy Bound" (Eddie Peabody on banjo, dubbing for Mickey Rooney)
**"Waiting for the Robert E. Lee" (Judy Garland, Mickey Rooney, Virginia Weidler, Anne Rooney, Richard Quine, and MGM Studio Chorus)
*"Babes on Broadway" (Finale) (Judy Garland, Mickey Rooney, Virginia Weidler, Ray McDonald, Richard Quine, and MGM Studio Chorus)
==Box Office==
According to MGM records the film earned $2,363,000 in the US and Canada and $1,496,000 elsewhere resulting in a profit of $1,720,000. 
==DVD release== Babes in Strike Up the Band, as well as a fifth disc containing bonus features on Rooney and Garland. 

==Notes==
 

==External links==
* 
*  
*  
* 
* 

 

 
 
 

 
 
 
 
 
 
 
 
 