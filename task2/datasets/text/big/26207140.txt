Gorintaku (1979 film)
{{Infobox film
| name           = Gorintaku
| image          = Gorintaku 1979 film.jpg
| image_size     =
| caption        =
| director       = Dasari Narayana Rao
| producer       = K. Murari

| writer         =
| narrator       = Savitri Sujatha Sujatha Prabhakar Sharada Ramaprabha
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1979
| runtime        =
| country        = India Telugu
| budget         =
}} Savitri is remembered even today. The film was remade in Hindi as Mehndi Rang Layegi.

==The plot==
Ramus father is a drunkard, who neglects his family. His daughter dies after he beats her once, and Ramu (Shobhan Babu) runs away from home. Ramu studies on charity, and in medical college meets Swapna (Sujatha). Ramu tutors Swapnas siblings and lives in her out-house. Swapna is married off to Anand, a London-based doctor who is already married. By the time this fact comes to light, Ramu meets Padma (Vakkalanka Padma) and falls in love. A love triangle ensues, with Swapna finally sacrificing her love and Ramu and Padma come together in the end.

==Awards==
* Filmfare Best Film Award (Telugu) - K.Murari
* Filmfare Best Director Award (Telugu) - Dasari Narayana Rao

==Lyrics==
* Cheppanaa (Lyricist: Atreya; P. Susheela and S. P. Balasubrahmanyam)
* Ela Ela Daachaavo (Lyricist: Devulapalli Krishna Sastri; Singers: P. Susheela, S. P. Balasubrahmanyam)
* Elaaga Vachchi (Lyricist: Sri Sri; Singers: P. Susheela and S. P. Balasubrahmanyam)
* Gorinta Poochindi Komma Lekunda (Lyricist: Devulapalli Krishna Sastri; Singer: P. Susheela)
* Komma Kommako Sannayi (Lyricist: Veturi Sundararama Murthy; Singers: P. Susheela and S. P. Balasubrahmanyam)
* Padite Shilalaina Karagali (Lyricist: Atreya; Singer: P. Susheela)
* Yetantaav Yetantaav (Lyricist: Atreya; Singers: P. Susheela and S. P. Balasubrahmanyam)

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 