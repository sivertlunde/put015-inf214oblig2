Suds (film)
{{Infobox film
| name           = Suds
| image          = Suds 1920 silent film lobbycard.jpg
| image size     =
| caption        = Lobby card John Francis Dillon
| producer       = Mary Pickford
| writer         = Waldemar Young (scenario) Richard Bryce (play) Frederick Fenn(play)
| narrator       =
| starring       = Mary Pickford
| music          =
| cinematography = L. William OConnell	
| editing        =
| distributor    = United Artists
| released       = January 27, 1920
| runtime        = 75 minutes 64 minutes (DVD) 
| country        = United States
| language       = Silent film  English intertitles
| budget         =
| preceded by    =
| followed by    =
}}
 
 1920 silent John Francis Peter Pan. 

==Plot==
Amanda Afflick (Mary Pickford) is a poor laundry woman working in London. She is too weak to do the hard work, but is always picked on and humiliated by her boss Madame Didier (Rose Dione). Amanda is desperately in love with the handsome customer Horace Greensmith (Albert Austin), but none of her colleague think she stands a chance of being his sweetheart.

One afternoon Amanda gets in trouble again and is forced to work all night long. All alone, she fantasizes about her first and only meeting with Horace, eight months ago. All the fellow employees ridicule her for still having faith that he will return someday to pick up his clothes. Amanda is fed up with all her colleagues making fun of her and lies that she is a duchess, coming from a wealthy family. She comes up with a story of her having an affair with Horace. Her father found out and sent her to live in London.
 Harold Goodwin) has the job of collecting laundry with his cart. One day, his beloved horse Lavender is too weak to go up a hill and falls. The cart is destroyed and when Benjamin admits the truth to Madame Didier, she asks for the horse to be killed. Benjamin reveals to Amanda what will happen with Lavender and she tries to stop the horse from being killed. She eventually buys the horse and takes it into her own home.

Amanda is not allowed to take the horse into her own apartment and is noticed on the streets by the wealthy and sympathizing Lady Burke-Cavendish. She offers to take the horse to live at her country place. Amanda is delighted and accepts her offer. Later, Lady Burke-Cavendish stops by to tell Amanda the horse is doing very well. Amanda lies to the fellow laundry women Lady Burke-Cavendish is actually her aunt.

They are interrupted by Horace: he has returned for his laundry. The fellow workers assume he will recognize Amanda, since they were lied to he is her secret lover. Amanda is desperate and successfully pretends to be reunited with him. Horace is confused and wants to leave. While the laundry women are away she tells the truth to Horace. Benjamin walks in on them, initially trying to flirt with Amanda , but when he notices Horaces presence he leaves.

Horace sympathizes with Amanda and invites her to his mansion. He changes his mind when he becomes ashamed of her. Amanda notices this and pulls back. Horace leaves and Amanda is left behind with a broken heart. She is later hired as Lady Burke-Cavendishs personal maid and now lives in wealth. She finds out Horace is a worker at the country place and they fall in love with each other.

==Remake==
The original film was adapted to a musical written by Deonn Ritchie Hunt with music by Kim Douglas in the 2000s.

==Cast==
*Mary Pickford - Amanda Afflick
*Albert Austin - Horace Greensmith Harold Goodwin - Benjamin Pillsbury Jones
*Rose Dione - Madame Jeanne Gallifilet Didier

==Production crew==
*Produced by Mary Pickford
*Cinematography by L. William OConnell	and Charles Rosher
*Art Direction by Max Parker
*Costume Design by Adele Crinley
*Assistant Director William A. Crinley
*Art Department - Alfred L. Werker (props)
*Other crew - William S. Johnson (electrical effects)

==See also==
*Mary Pickford filmography

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 