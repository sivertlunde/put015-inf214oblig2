Jagadam
{{Infobox film
| name           = Jagadam
| image          = jagadam_poster.jpg
| caption        =
| writer         = Sukumar Ram Isha Pradeep Rawat Prakash Raj Sukumar
| producer       = Aditya
| cinematography = R. Rathnavelu
| editing        = A. Sreekar Prasad
| distributor    =
| released       =  
| runtime        = 163 minutes
| country        = India
| language       = Telugu
| music          = Devi Sri Prasad
| budget         =  4 crores
| gross            =  10 crores
}} Sukumar (Arya Pradeep Rawat, and Prakash Raj in important roles. The film was produced by Aditya Babu. The film was released on 16 March 2007.Dubbed In Hindi As Jwalamukhi.

== Plot == Pradeep Rawat), who was a goonda, as everyone, including his teacher, is afraid of him. When all the boys wanted to become doctors, engineers, lawyers and other professionals, Seenu wants to become a goonda. Though he behaves like a ruffian, he comes to know that one should have the support of a politician or a big dada. With the help of Laddu (A.S. Ravikumar Chowdary) an associate of Manikyam, Seenu meets the latter and joins his gang. At this juncture, Seenu falls in love with Subbalakshmi (Isha Sahni). As she pleads with Seenu to help her friends father, Seenu agrees to settle a land deal. But the land was under illegal occupation of Manikyams gang member.

As he promised to settle the deal, Seenu opposes Manikyam. This makes Seenu a hero, and he also starts doing settlements on his own. Once, Seenu agrees to help an industrialist, whose quarry was occupied by Yadav (Satya Prakash), with the support of Manikyam. As things go out of control, Seenu decides to kill Yadav.

However, he could not kill him but Seenus young brother, who also likes fighting scenes, takes the gun and kills Yadav. Seenu appeals to the police commissioner (Prakash Raj) to mediate with Manikyam, but his men kill Seenus brother. This irks Seenu who severely thrashes Manikyam, but spares him. Seenu realizes that violence is not the way to go. When Manikyam attacks him, he kills him in self-defense. The film ends on a happy note, with Seenu leaving all the dadagiri and leads a normal life.

== Cast == Ram ... Seenu
*  Isha Sahni ... Subbalakshmi Pradeep Rawat ... Manikyam
* Prakash Raj ... Police Officer (Guest role)
* A.S. Ravikumar Chowdary ... Laddu
* Satya Prakash ... Yadav
* Raghu Babu

== Music ==
{{Infobox album
| Name        = Jagadam
| Type        = soundtrack
| Artist      = Devi Sri Prasad
| Cover       =
| Released    =  
| Recorded    =
| Genre       = Film soundtrack
| Length      =
| Label       = Supreme Music
| Producer    = Aditya Babu
}}
The film has six songs composed by Devi Sri Prasad. Music got a good response from public. {{cite web| title=andhracafe.com| work= Jagadam audio is good |url= http://www.andhracafe.com/index.php?m=show&id=17754
|accessdate=13 February 2007}} 

* "Violence is a Fashion" - Devi Sri Prasad Tippu & Priya
* "36-24-36" - Mamta Mohandas  Ranjith
* "Mu Mu Mudhante Chedha" - Raquib Alam
* "36-24-36 (Remix)" - Mamta Mohandas

== References ==
 

==External links==
*  

 

 
 
 
 
 