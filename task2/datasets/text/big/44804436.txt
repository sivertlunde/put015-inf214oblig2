Captain Courtesy
{{Infobox film
| name           = Captain Courtesy
| image          =
| alt            = 
| caption        = 
| director       = Phillips Smalley Lois Weber	
| producer       = Oliver Morosco
| screenplay     = Edward Childs Carpenter 
| starring       = Dustin Farnum Courtenay Foote Winifred Kingston Herbert Standing Jack Hoxie
| music          = 
| cinematography = Dal Clawson 
| editing        = 
| studio         = Hobart Bosworth Productions Oliver Morosco Photoplay Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Phillips Smalley and Lois Weber and written by Edward Childs Carpenter. The film stars Dustin Farnum, Courtenay Foote, Winifred Kingston, Herbert Standing and Jack Hoxie. The film was released on April 19, 1915, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Dustin Farnum as Leonardo Davis
*Courtenay Foote as George Granville
*Winifred Kingston as Eleanor
*Herbert Standing as Father Reinaldo
*Jack Hoxie as Martinez 
*Carl von Schiller as Jocoso
*Winona Brown as Indian Girl Servant

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 