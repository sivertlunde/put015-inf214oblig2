Parole Fixer
{{Infobox film
| name           = Parole Fixer
| image          =
| caption        =
| director       = Robert Florey
| producer       = Edward T. Lowe Jr.
| writer         = 
| starring       = 
| music          =
| cinematography = Harry Fischbeck
| editing        = Harvey Johnston
| distributor    = Paramount Pictures
| released       = 2 February 1940 
| runtime        = 68 min.  
| country        = United States
| language       = English 
| budget         =
}}

Parole Fixer is a 1940 American film directed by Robert Florey.

Federal Bureau of Investigation Director J. Edgar Hoover is credited for the source material, the 1938 book called "Persons in Hiding", a purported expose of corruption within the parole system.  The plot focuses on the gangster "Big Boy" Bradmore (Quinn) who is wrongly paroled from prison and promptly murders an FBI agent.

==Cast== William Henry as Scott Britton
* Virginia Dale as Enid Casserly
* Robert Paige as Steve Eddson
* Gertrude Michael as Collette Menthe
* Richard Denning as Bruce Eaton
* Fay Helm as Rita Mattison
* Anthony Quinn as Francis Big Boy Bradmore
* Harvey Stephens as Bartley Hanford
* Marjorie Gateson as Mrs. Thorton Casserly
* Lyle Talbot as Ross Waring
* Louise Beavers as Aunt Lindy Paul McGrath as Tyler Craden
* Jack Carson as George Mattison

== External links ==

*  
 

 
 
 
 
 
 
 


 