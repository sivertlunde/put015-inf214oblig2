Days and Nights
 
{{Infobox film
| name           = Days and Nights
| image          = Days and Nights poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Christian Camargo
| producer       = Barbara Romer   Juliet Rylance
| writer         = Christian Camargo
| starring       = Christian Camargo   Katie Holmes   William Hurt   Allison Janney   Cherry Jones    Russell Means   Michael Nyqvist    Jean Reno   Juliet Rylance   Mark Rylance   Ben Whishaw 
| music          = Claire van Kampen
| cinematography = Steve Cosens
| production design = Tommaso Ortino
| editing        = Ron Dulin   Sarah Flack
| studio         = Art Cine
| distributor    = IFC Films
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Days and Nights is a 2013 American drama film directed and written by Christian Camargo. The film is inspired by The Seagull by Anton Chekhov and set in rural New England in the 1980s.

As part of a special tribute in honor of the actor and activist Russell Means, who died 2012, the film had a special screening at the Denver Film Festival on November 8, 2013. 

Days and Nights showed at the 25th Anniversary Palm Springs International Film Festival at the Annenberg Theatre on January 4, 2014.  It had its premiere in the United States on September 26, 2014.

==Reception==
The film received overwhelming negative reviews and holds a 0% fresh rate at Rotten Tomatoes. 

Ken Rudolph recognized that the actors were splendid, but the film seemed trite, and pretentious. 

The film critic Thorsten Krüger considers that Camargo "has nothing to tell and nothing to say."    The film "intends to be profound, but offers too little to be interesting".

The cast, so packed with talent that Jean Reno and Cherry Jones barely register, is stuck with stagey dialogue. Juliet Rylance, in the Nina part, has a particularly hard time. 

The World Cinema Now Program reviewed the film as: "Anton Chekhov’s The Seagull has seen numerous iterations over the decades, but actor/director Christian Camargo (The Hurt Locker) is able to honor the darkness and depth of this Russian tragedy while relocating it to a Memorial Day weekend in rural New England and putting his own contemporary spin on the material. With a haunting score, lovely cinematography, and strong performances from a remarkable ensemble cast, we see a family come together then fracture apart over the course of one disastrous weekend." 

The New York Times commented that "“The Seagull,” with its depiction of fin de siècle ennui, has been hollowed out and trivialized. So little time is given to the subsidiary characters in “Days and Nights” that, at times, the movie barely makes sense. The avian symbol has been changed from a sea gull to a bald eagle. What remains is a cracked shell."  

==References==
 

== External links ==
*  
*  

 
 
 
 
 

 