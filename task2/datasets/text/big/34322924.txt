Wild Style Original Soundtrack
{{Infobox album  
| Name = Wild Style Original Soundtrack
| Type = Soundtrack
| Artist = Various Artists
| Cover = Wild Style (film poster).jpg
| Released =  
| Recorded = 1982-1983
| Genre = Old school hip hop, turntablism
| Length = 77:47
| Label = Rhino Records
| Producer = Grandwizard Theodore, Grandmaster Flash, Various. 
 
}}
Wild Style Original Soundtrack is the soundtrack to the 1983 hip hop film Wild Style. The album has been described by Allmusic as one of the key records of early 80s hip-hop  

==Track listing==
# Military Cut – Scratch Mix by DJ Grand Wizard Theodore
# MC Battle – Busy Bee Starski Vs. Rodney Cee
# Basketball Throwdown – Cold Crush Brothers Vs. Fantastic Freaks
# Fantastic Freaks At The Dixie - Fantastic Freaks
# Subway Theme – Scratch Mix by   (Previously Unreleased)
# Cold Crush Brothers At The Dixie – Cold Crush Brothers
# Busy Bees Limo Rap – Busy Bee
# Cuckoo Clocking (previously unreleased) Double Trouble Double Trouble
# South Bronx Subway Rap –   (Original Version)
# Street Rap by Busy Bee – Busy Bee (Previously Unreleased)
# Busy Bee At The Amphitheatre – Busy Bee
# Fantastic Freaks at The Amphitheatre - Fantastic Freaks
# Gangbusters – Scratch Mix by DJ Grand Wizard Theodore
# Rammellzee & Shock Dell At The Amphitheatre – Rammellzee & Shock Dell
# Down By Law – Fab 5 Freddy & Chris Stein (Previously Unreleased)

;25th Anniversary Edition

Disc 2:
# Wildstyle Lesson - Kev Luckhurst aka Phat Kev
# Limousine Rap (Crime Dont Pay Mix) - Wild Style Allstars
# Basketball Throwdown (Dixie: Razorcut Mix) Cold Crush Brothers vs. Fantastic Freaks Double Trouble
# Street Rap (Subway Mix) - Busy Bee Double Trouble
# B Boy Beat (Instrumental) - Wild Style Allstars
# Yawning Beat (Instrumental) - Wild Style Allstars
# Crime Cut (Instrumental) - Wild Style Allstars
# Gangbusters (Instrumental) - DJ Grand Wizard Theodore
# Cuckoo Clocking (Instrumental) - Fab 5 Freddy
# Meetings (Instrumental) - Wild Style Allstars
# Military Cut (Instrumental) - DJ Grand Wizard Theodore
# Razor Cut (Instrumental) - Wild Style Allstars
# Subway Theme (Instrumental) - DJ Grand Wizard Theodore, Chris Stein
# Busy Bees (Instrumental) - Busy Bee
# Down By Law (Instrumental) - Fab 5 Freddy
# Baby Beat (Instrumental) - Wild Style Allstars
# Jungle Beat (Instrumental) - Wild Style Allstars
# Wild Style Scratch Tool - Kev Luckhurst aka Phat Kev

==References==
 

 
 