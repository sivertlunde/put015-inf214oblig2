List of horror films of 1989
 
 
A list of horror films released in 1989 in film|1989.

{| class="wikitable sortable" 1989
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | 976-EVIL
| Robert Englund || Stephen Geoffreys, Jim Metzler, Patrick OBryan ||   ||  
|- After Midnight
| Jim Wheat, Ken Wheat || Jillian McWhirter, Pamela Segall, Nadine Van Der Velde ||   ||  
|-
!   |  
| Sandor Stern || Robert Alan Browne ||   || Television film 
|-
!   | Beasties (1989 film)|Beasties
| Steven Paul Contreras || Denise Mora, Hector Yanez, Brenda Stubbe ||   ||
|-
!   | Beware! Children at Play
| Mik Cribben || Robin Lilly, Lori Tirgrath, Jamie Krause ||   ||  
|-
!   | Beyond the Door III
| Jeff Kwitny || Mary Kohnert, Bo Svenson, Victoria Zinny ||   ||  
|-
!   | Black Past
| Olaf Ittenbach || Andrea Arbter, Andre Stryi, Sonja Berg ||   ||
|-
!   | Black Rainbow
| Mike Hodges || Rosanna Arquette, Jason Robards, Tom Hulce ||    || Television film
|-
!   | Edgar Allan Poes: Buried Alive
| Gérard Kikoïne || Donald Pleasence, Karen Witter, John Carradine ||   ||  
|-
!   | The Carpenter David Wellington || Wings Hauser ||   ||  
|-
!   | The Cellar
| Kevin S. Tenney || Patrick Kilpatrick ||   ||  
|-
!   | Chopper Chicks in Zombietown
| Dan Hoskins || Jamie Rose, Don Calfa, Catherine Carlin ||   ||
|-
!   |   David Irving || Brian Robbins, Bill Calvert ||   ||  
|- The Church
| Michele Soavi || Hugh Quarshie, Asia Argento, Tomas Arana ||   ||  
|-
!   | Clownhouse
| Victor Salva || Nathan Forrest Winters, Brian McHugh ||   ||  
|-
!   | Cutting Class Donovan Leitch, Jill Schoelen, Brad Pitt ||   ||  
|-
!   | The Dead Next Door
| J.R. Bookwalter || Scott Spiegel, Maria Markovic, Bogdan Pecic ||   ||  
|- The Death King
| Jörg Buttgereit || Bela B., Susanne Betz, Hille Saul ||   ||  
|-
!   | Deceit (1989 film)|Deceit
| Albert Pyun || Norbert Weisser, Diane Defoe, Christian Andrews ||   ||
|-
!   | Dont Panic
| Rubén Galindo Jr. || Jon Michael Bischof, Gabriela Hassel, Helena Rojo ||    ||  
|-
!   | Dr. Caligari (film)|Dr. Caligari
| Stephen Sayadian || Madeleine Reynal, Fox Harris ||   ||  
|-
!   | Edge of Sanity
| Gerard Kikoine || Anthony Perkins, Glynis Barber, Sarah Maur-Thorp ||    ||  
|-
!   | Edge of the Axe
| José Ramón Larraz || Barton Faulks, Christina Marie Lane, Page Mosely ||    ||  
|-
!   | Elves (film)|Elves
| Jeffrey Mandel || Dan Haggerty, Deanna Lund, Ken Carpenter ||   ||  
|-
!   | The Exorcist III
| William Peter Blatty || Brad Dourif, Ed Flanders, Nicol Williamson ||   ||  
|-
!   | Family Reunion
| Michael Hawes || A.J. Woods, John Andes, Pam Phillips ||   ||  
|-
!   | Flesh Eating Mothers
| James Aviles Martin || Ramiro Oliveros, Donatella Hecht, Neal Rosen ||   ||  
|-
!   | Freakshow
| Constantino Magnatta || Timm Zemanek, Dean Richards Wiancko, Audrey Landers ||   ||  
|-
!   |  
| Rob Hedden || Jensen Daggett, Scott Reeves, Mark Richman ||   ||  
|-
!   | From the Dead of Night
| Paul Wendkos || Lindsay Wagner, Bruce Boxleitner ||   || Television film 
|-
!   | Grandmothers House (film)|Grandmothers House
| Nico Mastorakis || Kim Valentine, Eric Foster, Len Lesser ||   ||
|-
!   | Grim Prairie Tales
| Wayne Coe || James Earl Jones, Brad Dourif ||   ||  
|-
!   |  
| Dominique Othenin-Girard || Donald Pleasence, Danielle Harris, Ellie Cornell, Beau Starr ||   ||  
|-
!   | Headhunter
| Francis Schaeffer || Wayne Crawford, Kay Lenz, Steve Kanaly ||    ||  
|-
!   | Hellgate
| William A. Levey || Ron Palillo, Abigail Wolcott ||   ||  
|-
!   | High Desert Kill
| Harry Falk || Anthony Geary, Marc Singer, Micah Grant ||   || Television film
|-
!   | The Horror Show
| James Isaac || Lance Henriksen, Brion James, Rita Taggart ||   ||  
|-
!   | Killer Crocodile
| Fabrizio De Angelis || Sherrie Rose, Van Johnson, Ann Douglas ||   ||
|- The House of Clocks
| Lucio Fulci || Carla Cassola, Al Cliver ||   ||  
|-
!   | Lobster Man from Mars
| Stanley Sheff || Diana Frank, Ava Fabian, Deborah Foreman ||   ||
|-
!   |   Philip Davis, Victoria Catlin, Elizabeth She ||   ||  
|-
!   | I, Madman Tibor Takács || Jenny Wright, Clayton Rohner ||   ||  
|-
!   | Intruder (1989 film)|Intruder Dan Hicks ||   ||  
|-
!   | Las Vegas Bloodbath
| David Schwartz || Ari Levin, Rebecca Gandara, Jerry Raganesi ||   ||
|-
!   | Leviathan (1989 film)|Leviathan
| George Pan Cosmatos || Peter Weller, Richard Crenna, Amanda Pays ||    ||   
|-
!   | Maya William Berger, Peter Phelps, Mariangélica Ayala ||   ||
|-
!   |  
| Glenn Takajian || Diana Flaherty, Marcus Powell, Tara Leigh ||   ||
|-
!   | Monster High
| Rudy Poe || Diana Frank, Robert Lind, Doug Kerzner ||   ||  
|-
!   | Murder Weapon Ellen Cabot || Karen Russell, Allen First, Linnea Quigley ||   ||
|-
!   | Mutator
| John R. Bowey || Brion James, Carolyn Ann Clark, Milton Raphael Murill ||   ||
|- Night Life
| David Acomba || Scott Grimes, John Astin, Cheryl Pollak ||   ||  
|-
!   |   Stephen Hopkins || Robert Englund, Lisa Wilcox, Kelly Minter ||   ||  
|-
!   | Night Visitor
| Rupert Hitzig || Elliott Gould, Richard Roundtree, Michael J. Pollard ||   ||  
|-
!   | Offerings (1989)|Offerings
| Christopher Reynolds || Loretta Leigh Bowman, Elizabeth Greene, G. Michael Smith ||   ||  
|- Out of the Dark
| Michael Schroeder || Karen Witter, Lynn Danielson-Rosenthal, Karen Black ||   ||
|-
!   | Paganini Horror
| Luigi Cozzi || Daria Nicolodi, Jasmine Maimone, Pascal Persiano ||   ||
|-
!   | Parents (film)|Parents
| Bob Balaban || Randy Quaid, Mary Beth Hurt ||   ||  
|- Pet Sematary
| Mary Lambert || Dale Midkiff, Fred Gwynne, Denise Crosby ||   ||  
|-
!   |  
| Richard S. Friedman || Morgan Fairchild, Derek Rydall, Jonathan Goldsmith ||   ||  
|- Puppet Master
| David Schmoeller || Paul Le Mat, Irene Miracle, Matt Roe ||   ||  
|-
!   | Shocker (film)|Shocker Michael Murphy, Mitch Pileggi ||   ||  
|-
!   |  
| Monte Hellman || Bill Moseley, Laura Harring, Richard Beymer ||   ||  
|-
!   | Skinned Alive
| Jon Killough || J.R. Bookwalter, Lester Clark ||   ||  
|-
!   |  
| Michael A. Simpson || Pamela Springsteen, Tracy Griffith, Michael J. Pollard ||   ||  
|-
!   | Society (film)|Society
| Brian Yuzna || Billy Warlock, Devin DeVasquez, Heidi Kozak ||   ||  
|- Spontaneous Combustion
| Tobe Hooper || Jaime Alba, Cynthia Bain ||   ||  
|-
!   | Stepfather II
| Jeff Burr || Terry OQuinn, Meg Foster, Caroline Williams ||   ||  
|-
!   | Stuff Stephanie in the Incinerator
| Don Nardo || Catherine Dee, William Dame, M.R. Murphy ||   ||
|-
!   | The Suckling Frank Rivera, Gerald Preger ||   ||  
|-
!   |  
| Shinya Tsukamoto || Tomorowo Taguchi, Kei Fujiwara, Shinya Tsukamoto ||   ||  
|-
!   | Things (film)|Things
| Andrew Jordan || Robert (Tex) Allen, Amber Lynn, Patricia Sadler ||   ||  
|-
!   | Vampires Kiss
| Robert Bierman || Nicolas Cage, María Conchita Alonso, Jennifer Beals ||   ||  
|- The Vineyard Michael Wong ||    ||  
|-
!   | Warlock (1989 film)|Warlock
| Steve Miner || Julian Sands, Lori Singer, Richard E. Grant ||   ||
|-
!   | Whispers
| Douglas Jackson || Victoria Tennant, Chris Sarandon ||     ||  
|-
!   | Wicked Stepmother
| Larry Cohen || Bette Davis, David Rasche, Lori Singer ||   ||  
|- The Woman in Black
| Herbert Wise || Adrian Rawlins, Bernard Hepton, David Daker ||   ||  
|}

==References==
 

==Citations==
 
*  <!--
-->
 

 
 
 

 
 
 
 