Dr. Jekyll and Mr. Hyde (1931 film)
{{Infobox film
| name            = Dr. Jekyll and Mr. Hyde
| image           = JekyllHyde1931.jpg
| caption         = Theatrical Poster
| director        = Rouben Mamoulian
| producer        = Rouben Mamoulian Percy Heath Robert Louis Stevenson  
| based on       =  
| starring        = Fredric March Miriam Hopkins Rose Hobart
| music           = Herman Hand
| cinematography  = Karl Struss William Shea
| distributor     = Paramount Pictures
| released        =  
| runtime         = 98 minutes
| country         = United States
| language        = English
| budget          = $535,000    gross = $1,250,000   
|}} The Strange homicidal maniac.  Marchs performance has been much lauded, and earned him his first Academy Award.

==Plot==
The film tells the story of Dr. Jekyll and Mr. Hyde (character)|Dr. Henry Jekyll (Fredric March), a kind English doctor in Victorian London who is certain that within each man lurks impulses for both good and evil. One evening Jekyll attends a party at the home of his fiancee Muriel Carew (Rose Hobart), the daughter of Brigadier General Sir Danvers Carew (Halliwell Hobbes). After the other guests have left, Jekyll informs Sir Danvers that, after speaking to Muriel, he wants Carews permission to push up their wedding date. Sir Danvers sternly refuses Jekylls request. Later, while walking home with his colleague, Dr. John Lanyon (Holmes Herbert), Jekyll spots a bar singer, Ivy Pearson (Miriam Hopkins), being attacked by a man outside her boarding house. Jekyll drives the man away and carries Ivy up to her room to attend to her. Ivy begins flirting with Jekyll and feigning injury, but Jekyll fights temptation and leaves with Lanyon.

Muriel and Sir Danvers leave London for a few months. In the meantime, Jekyll develops a drug that releases the evil side in himself, thus becoming the violent Edward Hyde. Along with his behavior, Dr. Jekylls appearance changes as well. He transforms into something more menacing and primitive looking. Unlike Dr. Jekyll, Hyde has no conscience. Hyde has no restrictions, no boundaries; he is free to do what he pleases. Hyde returns to the music hall where Ivy works, and offers to tend to her financial needs in return for her company. Hyde manipulates Ivy into accompanying him by terrorizing her, being both abusive and controlling. He remains at her boarding house until he finds out that Muriel and her father are returning to London, and leaves Ivy but threatens her that hell be back.

On advice from her landlady Mrs. Hawkins (Tempe Pigott), Ivy goes to see Dr. Jekyll, hoping that he can free her of the abusive Hyde. When she arrives, Ivy sees that the celebrated Dr. Jekyll was the same man who saved her from abuse just months before. She breaks down in tears over her situation with Hyde. Jekyll is extremely distraught over the pain that he (Hyde) has caused her and promises Ivy that she will never have to worry about Hyde again.

While on his way to a party at the Carews home to celebrate their return and the announcement of a new wedding date to Muriel, Jekyll, without the use of his drugs, suddenly changes into Hyde. Ivy, who thought she was free of Hyde forever, is terrified when Hyde appears before her. Hyde angrily confronts her about seeing Jekyll and, just before murdering her, reveals that he and Jekyll are one and the same.

Hyde escapes and heads back to Jekylls house but his servant Poole refuses to open the door. Desperate, Hyde writes a letter to Lanyon from Jekyll instructing Lanyon to get certain chemicals and have them waiting for him at Lanyons home. When Hyde arrives, Lanyon pulls a gun on him and demands that Hyde take him to Jekyll. Hyde tells Lanyon that Jekyll is safe, but Lanyon doesnt believe him and refuses to let him leave. Realizing there is not much time, Hyde drinks the formula in front of Lanyon. Lanyon is shocked to witness the transformation and tells his friend that he has practically damned his soul for tampering with the laws of God.

With Ivys murder, Sir Danvers anger towards him for missing the party, and Hydes persona beginning to dominate his own, Henry Jekylls life continues to spiral out of control. He later goes to the Carews where Sir Danvers coldly rejects his visit but Muriel welcomes him. Jekyll, realizing the monster he really is, tells Muriel that he cannot be with her anymore. He feels that he is already damned and fears that he will harm her. He decides to leave. Standing out on the terrace and tearfully watching Muriel cry, Jekyll begins to change into Hyde once again. He then reenters the Carew house through the terrace door and assaults Muriel. Her screams bring her father and their butler, Hobson. Hyde then viciously murders Sir Danvers out in the garden by striking him repeatedly with Jekylls cane until it breaks, then runs off into the night towards Jekylls home and the lab to mix a new formula to change himself back.

The police and Lanyon are standing over Carews body in the garden. Recognizing the broken cane found next to the body, Lanyon tells them that he knows whose cane that is and agrees to take them to its owner. The police later arrive at Jekylls lab looking for Hyde and find only Jekyll, who lies that Hyde has escaped. They begin to leave when Lanyon arrives and tells them that Jekyll is the man theyre searching for (because the man they are looking for is hiding inside him). Just then a nervous Jekyll begins changing into Hyde before their shocked eyes. Outraged at Lanyon for betraying him, Hyde leaps from behind the table and attacks him. Hyde then tries to escape from the police but is fatally shot before he can again hurt Lanyon. As Hyde lies dead on the table full of Dr. Jekylls experiments and potions, he transforms one last time back into Henry Jekyll.

==Cast==
* Fredric March as Dr. Henry Jekyll / Mr. Edward Hyde
* Miriam Hopkins as Ivy Pearson
* Rose Hobart as Muriel Carew
* Holmes Herbert as Dr. John Lanyon
* Halliwell Hobbes as Brigadier General Sir Danvers Carew
* Edgar Norton as Poole
* Tempe Pigott as Mrs. Hawkins
* Douglas Walton as Blonde Student

==Background==

The film was made prior to the full enforcement of the Production Code and is remembered today for its strong sexual content, embodied mostly in the character of the bar singer, Ivy Pearson, played by Miriam Hopkins. When it was re-released in 1936, the Code required 8 minutes to be removed before the film could be distributed to theaters. This footage was restored for the VHS and DVD releases. 

The secret of the transformation scenes was not revealed for decades (Mamoulian himself revealed it in a volume of interviews with Hollywood directors published under the title The Celluloid Muse). Make-up was applied in contrasting colors. A series of colored filters that matched the make-up was then used which enabled the make-up to be gradually exposed or made invisible. The change in color was not visible on the black-and-white film.
 stage version by playwright Thomas Russell Sullivan.
 1920 version of Jekyll and Hyde, but he was already under a new contract with Metro-Goldwyn-Mayer. Paramount then gave the part to March, who was under contract and who strongly resembled Barrymore. March had played a John Barrymore-like character in the Paramount film The Royal Family of Broadway (1930), a story about an acting family like the Barrymore family|Barrymores. March would go on to win the Academy Award for Best Actor for his performance of the role.
 remade the film 10 years later with Spencer Tracy in the lead, the studio bought the rights to the 1931 Mamoulian version. They then recalled every print of the film that they could locate and for decades most of the film was believed lost. Ironically, the Tracy version was much less well received and March jokingly sent Tracy a telegram thanking him for the greatest boost to his reputation of his entire career.

==Production==
 
The opening credits use Toccata and Fugue in D minor, BWV 565 by Johann Sebastian Bach. 

==Reception== John Barrymore version as a "far more tense and shuddering affair" than that film. Hall called March "the stellar performer" in the title role while praising the acting of the entire supporting cast as well, and called the old-fashioned atmosphere created by the costumes and set designs "quite pleasing". 

Variety (magazine)|Variety ran a somewhat less favorable but still positive review. Alfred Rushford Greason wrote that "the picture doesnt build to an effective climax" because it was too slow and labored in getting there, and that while the initial transformation sequence "carries a terrific punch", its effect became lessened with successive uses. However, Greason credited March with "an outstanding bit of theatrical acting", declared the makeup "a triumph", and said that the sets and lighting alone made the film worth seeing "as models of atmospheric surroundings." 
 John Mosher Douglas Fairbanks, Senior."  Film Daily declared: "Gripping performance by Fredric March is highlight of strong drama, ace supporting cast and direction". 
 Universal monster films of the era, even considering that its $535,000 budget was high for a horror film at the time. 

==Accolades==
;Wins The Champ; 1932.
* Venice Film Festival: Audience Referendum; Most Favorite Actor, Fredric March; Most Original Fantasy Story, Rouben Mamoulian; 1932.

;Nominations
* Academy Awards: Oscar; Best Cinematography, Karl Struss; Best Adaptation Writing, Percy Heath and Samuel Hoffenstein; 1932.

==See also==
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
*  .
*  
*  
*   a Review by Walter Albert.
*   at the Walter Film Poster and Photo Museum
Streaming audio
*  on Favorite Story: January 10, 1948 
*  on  . 
*  on Theatre Royal: January 30, 1954

 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 