Dimension (film)
{{Infobox film
| name           = Dimension
| image          = 
| caption        = 
| director       = Lars von Trier 
| producer       = Peter Aalbæk Jensen {{cite book
|last=Lumholdt
|first=Jan
|title=Lars von Trier: interviews
|url=http://books.google.com/books?id=voTOt3GaRJAC&pg=PR43
|accessdate=14 October 2010
|year=2003
|publisher=Univ. Press of Mississippi
|isbn=978-1-57806-532-5
|pages=43–44}} 
| writer         = Lars von Trier & Niels Vørsel
| starring       = Jean-Marc Barr Katrin Cartlidge  Eddie Constantine  Udo Kier  Jens Okking  Stellan Skarsgård
| music          = 
| cinematography = 
| editing        = 
| studio         = Zentropa
| distributor    = Filmmagasinet Ekko
| released       =  
| runtime        = 27 minutes
| country        = Denmark
| language       = English
| budget         = 
| gross          = 
}}
Dimension is a short film written and directed by Lars von Trier, released in 2010.  The film was shot from 1991 to 1997. The original intention was to continue production in three-minute segments every year for a period of 33 years for a final release in 2024. However, von Trier lost interest in the project and it was shelved. The short film consists of the completed footage at the time the film was abandoned. 

==See also==
*List of films shot over several years

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 