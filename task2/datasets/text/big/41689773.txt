Dhanya (film)
{{Infobox film 
| name           = Dhanya
| image          =
| caption        = Fazil
| producer       = Boban Kunchacko Fazil
| Fazil
| starring       = Srividya Mohanlal Jagathy Sreekumar Nedumudi Venu
| music          = Jerry Amaldev
| cinematography = U Rajagopal
| editing        = TR Sekhar
| studio         = Excel Productions
| distributor    = Excel Productions
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, Fazil and produced by Boban Kunchacko. The film stars Srividya, Mohanlal, Jagathy Sreekumar and Nedumudi Venu in lead roles. The film had musical score by Jerry Amaldev.   

==Cast==
*Srividya
*Mohanlal
*Jagathy Sreekumar
*Nedumudi Venu
*Alummoodan
*Aranmula Ponnamma
*Kunchacko Boban as child artist
*Meena Menon
*Sarath Babu
*Mohan Jose

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dhanye neeyen jeevante ithalil || K. J. Yesudas || Yusufali Kechery || 
|-
| 2 || Konchum chilanke || K. J. Yesudas, Vani Jairam || Yusufali Kechery || 
|-
| 3 || Noopurametho kadha paranju || Vani Jairam || Yusufali Kechery || 
|-
| 4 || Ponkudangalil poonthen || K. J. Yesudas || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 

 
 
 
 
 