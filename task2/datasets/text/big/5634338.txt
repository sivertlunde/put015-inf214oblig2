Boogiepop and Others
{{Infobox animanga/Header
| name            = Boogiepop and Others
| image           =  
| caption         = Cover of the English release of Boogiepop and Others
| ja_kanji        = ブギーポップは笑わない
| ja_romaji       = Bugīpoppu wa Warawanai
| genre           = Speculative fiction
}}
{{Infobox animanga/Print
| type            = light novel
| author          = Kouhei Kadono
| illustrator     = Kouji Ogata MediaWorks
| publisher_en    =   Seven Seas Entertainment
| demographic     = Male
| label           =
| published       = February 25, 1998
}}
{{Infobox animanga/Print
| type            = manga
| title           = Boogiepop Doesnt Laugh
| author          = Kouji Ogata Mediaworks
| publisher_en    =   Seven Seas Entertainment
| demographic     = Shōnen manga|Shōnen
| magazine        = Dengeki Daioh
| first           = October 1999
| last            = May 2001
| volumes         = 2
| volume_list     =
}}
{{Infobox animanga/Video
| type            = live film
| director        = Ryu Kaneda
| producer        = Kazuo Kato Tsuguo Hattori Yasumasa Sone Youmi Miyano
| writer          = Sadayuki Murai
| music           = Yuki Kajiura
| studio =
| licensor        =     Right Stuf International
| released        = March 11, 2000
| runtime         = 109&nbsp;minutes
}}
 
 MediaWorks and Dengeki Game Novel Contest.

The story takes place in an unnamed Japanese city, and follows five students at Shinyo Academy as they try to piece together the puzzle of a new drug and recent disappearances among the student populace. While the teachers believe them to only be runaways, the female students whisper among themselves about the urban legend Boogiepop, who is said to be a shinigami.

== Explanation of the novels title ==
The titles used in the Boogiepop Series can typically be separated into multiple titles. The full, Japanese title of this novel is Boogiepop wa Warawanai Boogiepop and Others. Typically the translation of Boogiepop wa Warawanai is either Boogiepop Doesnt Laugh or Boogiepop Doesnt Smile; the publishers of the manga of the same name chose to use the Boogiepop Doesnt Laugh translation. This refers to the character Boogiepop, who is usually described with a deadpan expression, and is never seen to laugh or smile. Boogiepop and Others simply refers to the character Boogiepop, and the other characters.

== Plot summary ==
 
When waiting for his girlfriend, Touka Miyashita, to arrive, Keiji Takeda sees a ragged looking man stumbling through the town. A short man in a black cloak speaks with the other man after he collapses, then berated the crowd for not helping. When the police arrive, the two escape, but what shocked Takeda most of all was that the cloaked man has the face of his girlfriend. The following day, Miyashita acts as if nothing had happened the previous day. Takeda sought to speak with her after school, but instead spots the cloaked man. Confronting him, the stranger introduces himself as Boogiepop. Boogiepop claims to be a split personality, who has emerged to protect the world.

Boogiepop explains to Takeda that Miyashita is unaware of his existence, and would modify her memories to explain the blank periods. Boogiepop has appeared this time to face a man-eater hiding in the school. Through their discussions, the two come to accept each other, and become friends. In the end, Boogiepop appears to Takeda in Miyashitas school uniform, and explains that the crisis was over, so he would disappear. To the end, Takeda is sure that Boogiepop is merely Miyashitas repressed possibilities, rather than a monster-fighting hero.

Kazuko Suema has an unusual interest: criminal psychology. Despite this interest, she had little interest in the rumours the other girls talk about in class, about a shinigami named Boogiepop. While walking home with her friend, Kyoko Kinoshita, Kinoshita is attacked by Kirima Nagi, The Fire Witch. Kirima interrogates her about something, but stops when she realizes that she had only caught a normal person; a drug-user. Suema confronts Kirima about this, but was told to let go of the events of five years ago – but Suema had never told anyone about that! Unable to let things happen without her being aware of them again, Suema searches for Kirimas house, and confronts her. However, Kirima reveals little about what she is doing, and only tells her that Boogiepop had saved her five years ago.

Masami Saotome joins a group date with Kusatsu Akiko. Late in the night, he drops a tablet into her drink; when she falls ill, he tells the others that he will get her home. Taking her to an abandoned building, he signals for Manticore to come; she turns the corpse into her loyal slave. Two months prior, Saotome had found the corpse of Yurihara at school, before himself being attacked by Manticore. Rather than panic or fight back, he told Manticore it would be better off leaving him alive and taking the form of Yurihara. In time, the two were deeply in love with each other, as they hatched their plan to conquer the world. As their experiments in controlling people begin to fail, and Kirima Nagi seems to be investigating too close, the relationship between Saotome and Manticore strains, until Naoko Kamikishiro came upon them, calling for Echoes. Manticore kills Kamikishiro, but for Saotome, this was the missing link: he has a plan to solve their problems.

Akio Kimura receives a letter telling him that Naoko Kamikishiro was dead. Two years ago, when they were in High School, he met Kamikishiro when she was confessing her love to Tanaka Shiro. Returning to his hometown to investigate the origin of this letter, Kimura runs into Touka Miyashita. Miyashita tells him that he should get over the disappearance of Kamikishiro, but Kimura tells her that an alien had taken Kamikishiro into space with him. Kamikishiro had told Kimura that she had met an alien named Echoes, who had been sent to evaluate humanity, but he had been cloned. His clone was now somewhere in their town, and he was looking to kill it before it killed the humans. As Kimura and Miyashita go their separate ways, Miyashita – but at the same time not Miyashita – tells him that Kamikishiro had "done her duty".
 stun gun. When they came to, they were presented to Echoes, who indicated that they were normal humans. After they were released by Echoes, Saotome stabbed him in the throat with a poison-filled mechanical pencil before Manticore attacked. Saotome then slashed Kirimas throat, killing her.

By the time Niitoki comprehended the situation, Echoes was being defeated by Manticore. However, he points to the sky, and transforms into light. Echoes directs the beam of light towards Manticore, but Saotome intervenes; he just barely saves her, but was killed instead. Hoping to take this chance to escape, Niitoki runs, but Manticore pursues. Hearing someone whistling Die Meistersinger von Nürnberg, she heads towards the sound. Niitoki trips, but Manticore becomes trapped in a wire. Niitokis savior has the face of Miyashita Touka, but claims to be Boogiepop. While Manticore is trapped, Boogiepop calls for Tanaka to shoot it with an arrow; an arrow to the head finishes the creature. Finally, Kirima rises from the dead, apparently resurrected by Echoes as he left.

== Characters ==
 
;  
  alternate personality of his host, Touka Miyashita?

;  
  Towa Organisation synthetic humans, and created a clone of him: Manticore. When Manticore escapes from the Towa Organisation, Echoes sets off to kill it.

;  
A third-year student at Shinyo Academy, Kamikishiro is going out with both Akio Kimura and Shirou Tanaka. She coincidentally meets Echoes, and is compelled to assist him, only to find they shared a telepathic link.

;  
A second-year student at Shinyo Academy, Kimura is in love with Naoko Kamikishiro.

;  
 
Also known as The Fire Witch. A second-year student at Shinyo Academy, Kirima spends more time investigating the mysterious happenings than she does in class.

;  
  Persian mythology.

;  
 
A second-year student at Shinyo Academy, Miyashita going out with Keiji Takeda, and believes she is living an ordinary high school girl. However, inside her Spalding sportsbag are the clothes and equipment of her other identity, the shinigami Boogiepop.

;  
A second-year student at Shinyo Academy and the President of the Discipline Committee, Niitoki is relied upon and trusted by her fellow students, despite her small stature.

;  
A first-year student at Shinyo Academy, Saotome has a strange attraction to strong, dangerous women. Though rejected by Nagi Kirima, he turned his affection to Manticore, and began helping her to hide into human society, and use her power to their best gain.

;  
 
A second-year student at Shinyo Academy, Suema has a reputation among the other students due to her unusual knowledge of criminal psychology.

;  
A third-year student at Shinyo Academy, Takeda thought he knew everything about his girlfriend, Touka Miyashita, until he met Boogiepop.

;  
A first-year student at Shinyo Academy, and the rising star of the Archery Club, Tanaka is going out with Naoko Kamikishiro.

== Manga adaptation ==
The novel was adapted into a two volume manga by the original illustrator, Kouji Ogata. The manga was translated into English by Seven Seas Entertainment.

== Film adaptation ==

Boogiepop and Others was adapted into a live-action film with the same name, which was released in Japan March 11, 2000. The film was directed by Ryu Kaneda, and starred Sayaka Yoshino as Miyashita/Boogiepop. MediaWorks (publisher)|MediaWorks, Hakuhodo and Toei Video were also involved in the production of the film. The director, Kaneda, said he did not wish to simply depict modern children as they were. "Here, the characters that appear on screen embrace all of their loneliness, and thats something that doesnt change in any era." He continues, "I thought perhaps I should turn it into the message that, no matter what, we must express our sense of isolation from deep inside that wont let us smile." Ryu Kaneda (2000-03-11) Directors Commentary, Boogiepop and Others  He encouraged the actors to adapt their characters, and allowed ad-libbing. Sayaka Yoshino (2000-03-11) Making of Boogiepop: Director Kaneda Ryu, Boogiepop and Others 

Asumi Miwas role as Naoko Kamikishiro was considered the most demanding of all. Her scenes were shot in quick succession, so she finished before the rest of the cast. Kai Hirohashi (2000-03-11) Making of Boogiepop, Boogiepop and Others  Miwa considered the scene where she meets Echoes for the first time to be the most demanding of all. Asumi Miwa (2000-03-11) Making of Boogiepop: Story #2 Naoko & Akio, Boogiepop and Others  Sayaka Yoshino gave up her summer break to appear in the film, and had to perform in Boogiepops "sauna suit" in days reaching 35&nbsp;°C (95&nbsp;°F). With the actors performing entire days in full sun, cool packs had to be brought in to keep them going.  Maya Kurosu spend two months in training for her role as Kirima Nagi, so that she could perform in the action scenes. 

The climax, filmed from September 15, 1999 (the 25th day of filming), was the greatest challenge. 50 shots had to be taken in 2 days, all at night. A Typhoon reached the Kantō Metropolitan area on that day, but against the forecasts it cleared up before they were due to begin filming. At 6pm, they were ready to begin. 

=== Notable differences ===
Although the manga and light novel are near identical in plot and character, the live-action movie differs in some areas of character development and interaction. Suema develops an infatuation for Kirima after their first encounter in the movie, but this aspect of their relationship is absent in the original novel. Saotome notes that over time Manticore had become like the original Yurihara Minako, and so wondered who had really been consumed. The light novel and manga portray Manticores personality as being dominant with no trace of the original Yurihara Minako remaining besides her adopted physical form. The movie also shows how Miyashita and Takeda began dating when Miyashita gave Takeda a watch for his birthday, an element to their relationship which was not covered in the light novel or manga.

=== Music ===

The soundtrack for Boogiepop and Others, titled Music Album Inspired by Boogiepop and Others, was composed and arranged by Yuki Kajiura and featured a wide range of musical styles including jazz, pop, and piano.  Each of the original songs composed represented a theme from the movie, such as the song "Forget-me-not" thematically representing Naoko Kamikishiro and Kimura Akio. A Boogiepop version of the classical overture to Richard Wagner|Wagners "Die Meistersinger von Nürnberg" was included on the album as an additional bonus track, arranged by Yoshihisa Hirano and conducted by Orie Suzuki.  The soundtrack was originally released in Japan by Media Factory on 25 March 2000. It was published in North America by AnimeTrax and released by Right Stuf International as a single disc CD on 30 April 2002. 

== Themes ==
Issues relating to growing up and change are central to the Boogiepop series. DVD commentary featuring Jeff Thompson, Crispin Freeman, and Rachael Lillis (2001) Boogiepop Phantom Evolution 2 

The narrative style demonstrates how different people observe different truths. DVD commentary featuring Jeff Thompson and Joe DiGiorgi (August 2001) Boogiepop Phantom Evolution 1 

== Allusions/references ==

=== Allusions/references to other works === Akira Kurosawas Ikiru, Saotome telling us that he is a big fan of The Doors, and Boogiepop mentioning Atom Heart Mother to Takeda.

The five chapters are also references to music: Romantic Warrior was the best-selling album by Return to Forever, "The Return of the Fire Witch" was a single from the King Crimson album Epitaph (King Crimson album)|Epitaph, "No One Lives Forever" was a single released by Oingo Boingo – Saotome begins singing this at the end of the chapter – and "Heartbreaker" most likely refers to a Grand Funk Railroad song from their album On Time, seeing as Kadono states it to be the Background music|BGM of his Afterword.
 Matsumoto Leijis Galaxy Express 999.

=== Allusions/references from other works ===
The anime series Boogiepop Phantom uses many characters from Boogiepop and Others, and makes repeated references to the ending of the novel.

In Boogiepop Returns: VS Imaginator Part 1, Boogiepop compares an opponent with Manticore.

=== Allusions/references to actual history, geography and current science ===
The novel touches on the theme on cloning, with Manticore being a clone of Echoes.
The novel touches on psychology, with Takeda being convinced that Boogiepop is a split personality rather than a supernatural being. Then there is Suema, with her interest in psychology, who adds elements to the chapter she narrates.

In the second narrative, some students ask Suema about The Village of Eight Graves and she mentions its connection to the Tsuyama massacre.  She also discusses the use of hydrocyanic acid gas as a chemical weapon.

When they first meet, Kamikishiro and Kimura go to a MOS Burger.

== Critical reception ==
The Boogiepop and Others novel won the fourth Dengeki Game Novel Contest in 1997.    The novels English release has received favourable reviews, though these have mostly focused on the translation, which has been cited as "a standard against which future Japanese novel translations are judged".    It has also received praise for ripping "the rules of narrative wide open",    especially for how it allows the characters to grow on the reader.   

The English release of the live-action film has met mixed reception. Whilst the characters and plot were well received, the special effects and costumes have been described as "campy", but "par for the course of something of this level and budget".      It has been primarily recommended to fans of Boogiepop Phantom, so as to gain "a complete understanding of the Boogiepop events".     

== References ==
 

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 