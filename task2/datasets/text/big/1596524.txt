Heartbreak Ridge
{{Infobox film
| name           = Heartbreak Ridge
| image          = HeartbreakRidgemovieposter86.jpg
| alt            = A black poster with a portrait shot showing the upper body of an older man dressed in a military uniform. In the background are a group of soldiers. Some of them are kneeling, and some are standing, holding rifles. Above in bold white letters, is a line that reads: "... the scars run deep." Below, in large letters it reads: "Clint Eastwood" and "Heartbreak Ridge". Below that are the film credits. 
| caption        = Heartbreak Ridge theatrical release poster
| director       = Clint Eastwood
| writer         = James Carabatsos Joseph Stinson
| starring       = Clint Eastwood Marsha Mason Everett McGill Mario Van Peebles
| producer       = Clint Eastwood
| cinematography = Jack N. Green
| editing        = Joel Cox
| music          = Lennie Niehaus Desmond Nakano
| studio         = Malpaso Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 130 minutes
| country        = United States
| language       = English
| budget         = $15 million Hughes, pp.200-201 
| gross          = $121,700,000   The Wrap. Retrieved April 4, 2013.  
}}
Heartbreak Ridge is a 1986 American war film directed and produced by Clint Eastwood, who also starred in the film. The film also co-stars Mario Van Peebles, Marsha Mason, and Everett McGill. The film was released in the United States on December 5, 1986. The story involves the actions of a small group of U.S. Marines during the American invasion of Grenada in 1983. A portion of the film was filmed on the island.

The title comes from the Battle of Heartbreak Ridge in the Korean War. The character played by Eastwood received the Medal of Honor for his heroic actions there.

The film has the distinction of being the 1000th film to be released in Dolby Stereo. 

==Plot== Marine Corps when he finagles a transfer back to his old unit. On the bus trip to his new assignment, he meets fellow passenger "Stitch" Jones (Mario Van Peebles), a flashy wannabe rock musician who borrows money for a meal at a rest stop and then steals his bus ticket, leaving him stranded.
 Annapolis graduate Supply and has not had the "privilege" of combat. He sees Highway as an anachronism in the "new" Marine Corps, and assigns him to shape up the reconnaissance platoon, which is made up of insubordinate Marines who had been allowed to slack off by their previous platoon sergeant. Among his new charges, Highway finds none other than Corporal Stitch Jones, whom Highway grabs by the earring, takes him to the platoons bunk and forces him to give Highway the money for his bus ticket, meal, and tip in revenge for stranding him; when Jones gives him the money for his ticket and meal but not for his tip, Highway aggressively rips his earring off. Highway quickly takes charge and starts the men on a rigorous training program. 
 Peter Koch), a large, heavily muscled Marine just released from the brig to beat up Highway as a last resort. One morning, the recon platoon brings in Swede to defeat Highway, but their plan backfires when Highway beats up Swede, causing Swede to gain respect for Highway and request to become part of the platoon. Because Stitch and the platoon have no other ideas to intimidate Highway, they are forced to deal with being trained. Although the recon platoon strenuously hate Highway, they eventually begin to shape up, develop esprit de corps and slowly begin to respect him.

Highway repeatedly clashes with Powers and Staff Sergeant Webster (Moses Gunn) over his unorthodox training methods, such as firing live ammunition from an AK-47 over his mens heads to familiarize them with the weapons distinctive sound. Powers makes it clear that he views Highways platoon as only a training tool for his own elite outfit. Major Powers goes so far as to script and make the Recon platoon lose in every field exercise. However, Highway is supported by old friend Sergeant Major Choozoo (Arlen Dean Snyder) and his nominal superior officer, the college educated but awkward and inexperienced Lieutenant Ring (Boyd Gaines). After Highways men learn that he had been awarded the Medal of Honor in the Korean War, they officially gain respect for him and close ranks and support him against Powers and Webster. The Recon platoon hides in the bushes to ambush Powers platoon in an exercise done with Multiple Integrated Laser Engagement System (MILES) gear. They also beat Highway at one of his own tricks: forcing them to run shirtless unless they all wear the same style T-shirt as him, which is foiled after Jones realizes a woman has been laying out Highways clothes for him in his quarters.

Highway also has more personal problems. His ex-wife Aggie (Marsha Mason) is working as a waitress in a local bar and dating the owner, Marine-hater Roy Jennings (Bo Svenson). Highway attempts to adapt his way of thinking enough to win Aggie back, even resorting to reading Cosmopolitan (magazine)|Cosmopolitan magazine to gain insights into the female mind. Initially, Aggie is bitter over their failed marriage, but tentatively reconciles with Highway before his unit is activated for the invasion of Grenada. 

After a last-minute briefing in the hangar bay of the amphibious assault ship USS_Iwo_Jima_(LPH-2)|Iwo Jima, Highways platoon mounts their UH-1 Huey, and are dropped by helocast into the water in advance of the rest of the Battalion Landing Team. While advancing inland, they come under heavy fire. Highway improvises, ordering Jones to use a bulldozer to provide cover so they can advance on and destroy an enemy machine gun nest. They subsequently rescue American students from a medical school. Later, when they are trapped in a building by enemy armor and infantry, radioman Profile (Tom Villard) is killed and his radio destroyed, cutting them off from direct communication. Lieutenant Ring shows previously unexhibited leadership qualities and comes up with the idea of using a telephone to make a long distance call to Camp Lejeune in North Carolina to call in air support, while one of the men uses his personal credit card to pay for the call. Later, despite Powerss explicit orders to the contrary, the men take out a key position and capture enemy soldiers. When Powers finds out, he chews them out and threatens Highway and Ring with a court martial, before his commanding officer, Colonel Meyers, arrives and reprimands Powers for discouraging initiative and fighting spirit.
 
When Highway and his men return to the United States, they are met by a warm reception. Aggie is there to welcome him back, and to Highways dismay, Jones informs him that he is going to stay and make a career for himself in the Marines, while Highway takes his mandatory retirement.

==Cast==
*Clint Eastwood as Gunnery Sergeant Thomas Highway
*Marsha Mason as Aggie
*Everett McGill as Major Malcolm A. Powers
*Moses Gunn as Staff Sergeant Luke Webster
*Eileen Heckart as Mary Jackson
*Mark Mattingly as Franco "One Ball" Peterson
*Bo Svenson as Roy Jennings
*Boyd Gaines as Lieutenant M.R. Ring
*Mario Van Peebles as Corporal "Stitch" Jones
*Arlen Dean Snyder as Sergeant Major Choozoo
*Vincent Irizarry as Lance Corporal Fragetti
*Ramón Franco (actor)|Ramón Franco as Lance Corporal Aponte (as Ramon Franco)
*Tom Villard as "Profile"
*Mike Gomez as Corporal Quinones
*Rodney Hill as Corporal Collins Peter Koch as Sergeant "Swede" Johanson Colonel Meyers
*Peter Jason as Major Devin

==Production== 1st Cavalry Army non-commissioned officer passing on his values to a new generation of soldiers. Eastwood was interested in the script and asked his producer, Fritz Manes, to contact the US Army with a view of filming the movie at Fort Bragg. 

However, the U.S. Army read the script and refused to participate, due to Highway being portrayed as a hard drinker, divorced from his wife, and using unapproved motivational methods to his troops, an image the Army did not want. The Army called the character a "stereotype" of World War II and Korean War attitudes that did not exist in the modern army and also did not like the obscene dialogue and lack of reference to women in the army. Eastwood pleaded his case to an Army general, contending that while the point of the film was that Highway was a throwback to a previous generation, there were values in the World War II and Korean War era army that were worth emulating.
  23rd Infantry US Defense Department originally supported the film, but withdrew its backing after seeing a preview in November 1986.  Eastwood was paid $6 million for directing and starring in the film. 
 Solana Beach and Puerto Ricos Vieques Island. 

The sequence involving the bulldozer is based on a real event involving Army General John Abizaid, former commander of US Central Command (July 2003 - March 2007). "In the U.S. Invasion of Grenada in 1983, Abizaid improvised an attack on a Cuban bunker by having his unit take cover behind a charging bulldozer". 
 American attack on Grenada is in some respects accurate, although it was really U.S. Army Rangers that secured the University medical school. 

The scene in which Lieutenant Ring must resort to using a credit card in order to communicate with his commanders was also based on real-life events involving Army paratroopers. 

==Music and soundtrack== score for the film was originally composed by American saxophonist Lennie Niehaus and Desmond Nakano. The music score was mixed by Robert Fernandez and edited by Donald Harris. The sound effects in the film were supervised by Robert G. Henderson and Alan Robert Murray. The mixing of the sound effects were done by Bill Nelson.

Actor Mario Van Peebles wrote "Bionic Marine" and "Recon Rap," and co-wrote "I Love You (But I Aint Stupid)" with Desmond Nakano.

The final scene of the movie in which GySgt Highways platoon returns to the United States features the 1st Marine Division Band (a fact which betrays the reality that the scene was filmed on the west coast, not at Cherry Point where the scene is intended to take place).

==Release==
===Critical reception===
Reaction to the film was generally positive. Among reviews, Roger Ebert of the Chicago Sun Times, gave the film three stars and noted how the movie, "has as much energy and color as any action picture this year, and it contains truly amazing dialogue." Ebert also complimented director Eastwood mentioning how he "caresses the material as if he didnt know B movies have gone out of style."  Paul Attanasio of the Washington Post solidly agreed saying, "Those with an endless appetite for this sort of tough-man-tender-chicken melodrama will enjoy watching Clint go up against these young punks and outrun, outshoot, outdrink and outpunch them, in the process lending an idea of what it means to be a . . . Marine."  Another Washington Post staff writer Rita Kempley, offered a different view commenting, "always fun to see misguided machismo properly channeled into service of God, country or the National Hockey League.–Isnt that the trouble with combat movies these days? From Top Gun to First Blood to Clint Eastwoods entertaining action drama Heartbreak Ridge, the empty-foxhole syndrome makes for non-endings."  The Variety staff at Variety Magazine, added to the encouraging reviews exclaiming, "Heartbreak Ridge offers another vintage Clint Eastwood performance. There are enough mumbled half-liners in this contemporary war pic to satisfy those die-hards eager to see just how he portrays the consummate marine veteran."  Vincent Canby of The New York Times expressed his satisfaction with the film too. He mused, "As the gritty, raspy-voiced sergeant, Mr. Eastwoods performance is one of the richest hes ever given. Its funny, laid-back, seemingly effortless, the sort that separates actors who are run-of-the-mill from those who have earned the right to be identified as stars."  

In terms of negative feedback, reviewer Derek Smith of the Apollo Movie Guide voiced his opinion saying, "there is not enough substance to Gunny to make him interesting enough to be the central character of a film, and since the movie offers nothing new or fresh, it just feels dull and uninteresting." 

===Accolades=== Best Sound Bill Nelson.   

===Box office=== 18th at the box office for 1986. 

===Home media=== Region 1 DVD in the United States on October 1, 2002.  The widescreen edition of the film was released on Blu-ray Disc|Blu-ray in the United States on June 1, 2010. 

==References==
 

==Bibliography==
* 
* 

==External links==
 
 
* 
*  at the Internet Movie Database
*  at the Movie Review Query Engine
*  at Rotten Tomatoes
*  at Box Office Mojo

 

 
 
 
 
 
 
 
 
 
 