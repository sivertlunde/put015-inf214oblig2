Griha Pravesh
{{Infobox film
| name           = Griha Pravesh
| image          = Griha Pravesh.jpg
| image_size     = 
| caption        = 
| director       = Basu Bhattacharya
| producer       = 
| writer         =
| narrator       = 
| starring       =Sanjeev Kumar, Sharmila Tagore, Sarika
| music          = Kanu Roy
| cinematography = 
| editing        = 
| distributor    = 
| released       = 28 July 1979
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1979 Bollywood drama film directed by Basu Bhattacharya. The film stars Sanjeev Kumar, Sharmila Tagore and Sarika. The film was the last of Basu Battacharyas introspective trilogy on marital discord in an urban setting, which included Anubhav (1971), Avishkaar (1973). 

==Cast==
*Sanjeev Kumar ...  Amar
*Sharmila Tagore ...  Mansi
*Sarika ...  Sapna
*Master Bittoo ...  Babla (child)
*Sudha Chopra ...  Quarrelsome Neighbor
*Kanu Roy
*Gulzar  ... Guest Appearance in song
*Manik Dutt
*Vimal Joshi
*Raj Verma
*Nandlal Sharma
*R.S. Chopra
*Satya Banerjee
*Shashi Jain
*Tarun Mukherjee
*Nilesh
*Khokha Mukherji

==References==
 

==External links==
*  

 
 
 
 

 