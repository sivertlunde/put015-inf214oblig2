Black Sea (film)
 
 
{{Infobox film
| name           = Black Sea
| image          = Black Sea (film).jpg
| alt            = 
| caption        = Theatrical release poster Kevin Macdonald
| producer       = Charles Steel Kevin Macdonald
| writer         = Dennis Kelly
| starring       = Jude Law Scoot McNairy Ben Mendelsohn David Threlfall
| music          = Ilan Eshkeri
| cinematography = Christopher Ross
| editing        = Justine Wright
| studio         = Film4 Cowboy Films Etalon Film
| distributor    = Universal International Pictures
| released       =  
| runtime        = 115 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  $1,171,559 
}}
 adventure Thriller thriller film Kevin Macdonald, written by Dennis Kelly, and starring Jude Law.  The film was released in the United Kingdom on 5 December 2014 and in the United States on 23 January 2015. 

==Plot==
Robinson (Jude Law) a veteran Captain of under-sea salvage receives news that he is to be made redundant. He receives £8,000 for his services and is asked to clean his desk and leave the premises. Robinson is recently divorced and misses his wife and son.
 Georgia with a cargo of gold worth millions and how their former employer found the U-boats location but was unable to salvage it. He also informs them both of a plan he has been working on to secretly recover the gold. After meeting with Kurstin, Robinson agrees to meet with an unknown backer called Lewis.

Lewis agrees to fund Robinsons expedition on the grounds he receives a 40% split of the proceeds and that his executive Daniels (Scoot McNairy) accompany the expedition. Robinson agrees to the deal. Upon returning home Robinson finds a youth (Tobin) who notifies him of Kurstins suicide and indicates they were friends. Robinson takes the young man in and decides to bring him along on the expedition. The crew is 50% Russian and 50% British and almost immediately there are issues between the two groups.  Only Blackie speaks Russian and English and the Russians view the young Tobin as a bad omen.

Due to the close confines there is a lot of rumbling between the two crews. Robinson makes the decision to split the take equally between each crewman. Due to the mounting pressures the crew fall out and there is a fight leading to Fraser (Ben Mendelsohn) losing his head and stabbing and killing Blackie. In the ensuing scuffle a fire breaks out and the sub is damaged leading to an explosion which knocks Robinson unconscious. 

Robinson wakes up 18 hours later to find tensions at breaking point.  The Russians have taken over half of the ship with the British in the other half. The ship is also badly damaged and they have no means to speak with the Russians as Blackie was killed. The subs drive shaft is damaged but they discover they are close to the old U-boat and may be able to transfer its drive shaft and save the ship. Tensions continue to mount but Robinson discovers Morozov (Grigoriy Dobrygin) speaks English.

Robinson sends Tobin with Fraser and another diver to recover the drive-shaft and the gold.  While making their way back, third diver (David Threlfall) perishes when his air hose gets cut. Through Morozov they manage to get the Russians to install the drive-shaft and get the sub moving again. At this stage Daniels admits to Robinson that his prior employer have set them up such that they are being used to do the dangerous work, while the employer waits to seize the gold as soon as the submarine surfaces. They decide to remain submerged to try to keep the gold and avoid arrest, but while trying to evade the Russian navy they collide with an underwater ridge and come to a stop on the seafloor as the sub begins to take on water.

Fraser and the rest of the remaining crew try to repair the leaks but their efforts are futile.  Before they can escape, a panicked Daniels locks the bulkhead behind him leaving them to drown but traps himself in the next compartment with his snagged clothing.  Morozov closes the final bulkhead, protecting Robinson, Tobin, and himself in the torpedo section where Robinson has hidden three escape suits. Robinson evacuates Tobin and Morozov and explains to Tobin that he will follow in the third suit using an emergency lever. Both men surface whereupon Morozov informs the young Tobin that there was no emergency lever and that Robinson had chosen to sacrifice himself. Minutes later the third suit appears containing some of the gold and a picture of Robinsons family. The film ends with the two men close to land swimming to safety.

==Cast==
* Jude Law  as Captain Robinson
* Scoot McNairy  as Daniels
* Ben Mendelsohn as Fraser
* David Threlfall as Peters
* Bobby Schofield as Tobin
* Karl Davies as Liam
* Konstantin Khabensky as Blackie
* Grigoriy Dobrygin as Morozov
* Tobias Menzies as Lewis
* Jodie Whittaker as Chrissy
* Michael Smiley as Reynolds

==Production== Kevin Macdonald Focus  Kill the Messenger when it was announced on 5 February 2014. 

===Casting===
On 25 April 2013, Jude Law joined the cast of Black Sea as the lead actor.    Scoot McNairy joined the cast of the film on 17 June 2013 in the role of a sailor who convinces the captain to undertake a mission.   

===Filming===
On 8 August 2013, Focus Features CEO James Schamus and co-CEO Andrew Karpen announced that the shooting of the film Black Sea has started in the United Kingdom.   The filming of scenes took place on the River Medway in Strood.  

==Release==
Black Sea was released on 5 December 2014 in British cinemas. It had a limited release in the USA on 23 January 2015.   

==Awards==
{| class="wikitable sortable" width="95%"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Nominees
! Result
|- Courmayeur Noir 24th Courmayeur Noir Film Festival 
| Black Lion for Best Feature Kevin Macdonald
|  
|-
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 