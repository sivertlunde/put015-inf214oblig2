Spangles (1928 film)
{{Infobox film
| name           = Spangles
| image          =
| caption        =
| director       = George Banfield 
| producer       = 
| writer         = Fred Raynham James Knight
| music          =  Phil Ross
| editing        = 
| studio         = British Filmcraft Productions
| distributor    = Paramount British Pictures
| released       = December 1928
| runtime        = 7,425 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama film directed by George Banfield and starring Fern Andra, Forrester Harvey and Lewis Dayton. It was made at Walthamstow Studios. A circus girl goes to London and enjoys great success. However, she eventually decides to return home. 

==Cast==
*  Fern Andra as Spangles 
* Forrester Harvey as Watty 
* Lewis Dayton as Hugh Gridstone 
* A. Bromley Davenport as Romanovitch  James Knight as Haggerston 
* A.B. Imeson as Earl of Warborough 
* Gladys Frazin as Countess  Carlton Chase as Dennis Adderly

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 

 