President Gari Abbai
{{Infobox film
| name           = President Gari Abbai
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| screenplay     = Tatineni Rama Rao 
| producer       = A.V. Subba Rao
| director       = Tatineni Rama Rao
| starring       = Nandamuri Balakrishna   Suhasini Chakravarthy
| cinematography = Nandamuri Mohana Krishna
| editing        = J. Krishna Swamy &   Balu
| studio         = Prasad Art Pictures
| released       =  
| runtime        = 2:20:31
| country        =   India
| language       = Telugu
| budget         =
| gross          = 
}}

President Gari Abbai (  film produced by A.V. Subba Rao, Prasad Art Productions, and directed by Tatineni Rama Rao. The film stars Nandamuri Balakrishna, and Suhasini and music was composed by K. Chakravarthy|Chakravarthy.       

==Cast==
{{columns-list|3|
*Nandamuri Balakrishna as Ramakrishna 
*Suhasini as Latha Satyanaryana as Suraiah  Jaggayya as Chandraiah 
*Nutan Prasad as Delhi Babai  Paruchuri Venketeswara Rao as Govindaiah
*Eeswar Rao  Sudhakar 
*Rajesh as Raja
*Raj Varma as Papa Rao  
*Sakshi Ranga Rao as Seshavataram
*P. L. Narayana as Sambaiah 
*KK Sarma as T.C. 
*Ramana Reddy 
*Vankayala Satyanayana 
*Madan Mohan as Engineer
*Ch Krishna Murthy 
*Dham as Narasaiah Annapurna   Sri Lakshmi 
*Varalakshmi as Purna Rajya Lakshmi
*Kakinada Shymala
*Kuaili as item number 
*Y.Vijaya 
*Nirmalamma  
*Master Rajesh as Gopi
}}

==Soundtrack==
{{Infobox album
| Name        = President Gari Abbai
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 19:32
| Label       = LEO Audio Chakravarthy
| Reviews     =
| Last album  = Collector Gari Abbai   (1987)  
| This album  = President Gari Abbai   (1987)
| Next album  = Agni Putrudu   (1987)
}}
 Chakravarthy with lyrics by Veturi Sundararama Murthy. The soundtrack was released by the LEO Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Muddu Pettakunte  SP Balu
|3:27
|- 2
|Chekkam Chekkam  SP Balu, P. Susheela
|3:30
|- 3
|Egirindi Egirindi  SP Balu,P. Susheela
|4:10
|- 4
|Sitrabngi Pilindi  SP Balu, S. Janaki
|4:20
|- 5
|Yadanundi Vasthe 
|P. Susheela
|4:05
|}

==Others==
* VCDs and DVDs were released by Moser Baer Home Videos, Hyderabad, Telangana|Hyderabad.
==References==
 

 

 
 
 


 