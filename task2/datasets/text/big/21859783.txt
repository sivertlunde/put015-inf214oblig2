Shangani Patrol (film)
{{Infobox film name     = Shangani Patrol image    = Shangani patrol.jpg caption  = DVD Jacket director       = David Millin producer       = Roscoe Behrmann screenplay     = Adrian Steed story          = "A Time to Die" (book), by Robert Cary (1968) based on       = Historical events of the Shangani Patrol (1893) starring  Brian OShaughnessy as Maj. Allan Wilson Will Hutchins as Chief of Scouts Frederick Russell Burnham music          = Mike Hankinson Dan Hill RPM Studio Orchestra cinematography = Lionel Friedberg editing        = Antony Gibbs    released       = 1970 studio         = RPM Film Studios distributor    = Rhodesian Broadcasting Corporation runtime        = 90 min. country        = Rhodesia language       = English
}}
 Brian OShaughnessy Allan Wilson and Will Hutchins as the lead Scout Frederick Russell Burnham.  Also includes the song "Shangani Patrol" by Nick Taylor (1966 recording).
 Ndebele King 1970 film shot on location in Matabeleland, Rhodesia (now Zimbabwe).

==Plot== Southern Rhodesia, 1893. The British South Africa Company (BSAC) (later to become the  British South African Police - BSAP) is based in the encampment of Fort Salisbury (now the capital of Zimbabwe, Harare). The film starts with a sepia toned trial of two AWOL volunteers and later deserters who were eventually court martialed by the army for stealing gold which was given to them by Matabele warriors on behalf of King Lobengula as a peace offering to end the war. In this trial (which is solely represented by archive drawings and voice-overs) the voice of TV anchor man/journalist Adrian Steed is heard as the judge (later to be seen as Major Forbes) and that of Stuart Brown playing Dr Leander Starr Jameson.  
 Shona people under the protection of the BSAC and promises that his warriors will kill them in the bush and not in the Shangani River so as not to dirty the water. 

The BSAC re-group and under orders from Dr. Jameson, are to pursue King Lobengula’s troops all the way down from Fort Salisbury to the south of Rhodesia, at that time the kingdom of Lobengula, to capture King Lobengula and hold him to ransom.  Wilson leads his troops on what has become a heroic but ultimately futile quest to capture the King in the hope that his troops will surrender. One by one, the volunteers begin showing signs of their inexperience and sometimes lack of courage. One even loses his nerve, runs away and is shot while fleeing. Eventually, with ammunition and morale running low, Wilson dispatches Burnham back to the fort to alert Major Forbes that reinforcements are required. After much argument, Burnham complies and the Shangani Patrol is minus another volunteer. 

Burnham alerts Major Forbes to the peril that the Patrol is in. Forbes refuses to back his troops up. The Patrol is eliminated by the Ndebele after Wilson fires his remaining bullet. He and the party are killed by the Matabele, who then praise the 34 men of the Shangani Patrol as being "men of men". 

The final slaughter of the vanquished is shown in quick fire frame flashes (almost like still pictures) and with no sound, right up until the moment when the Matabele induna screams "Touch not their bodies! They were men of men and their fathers were men before them!" The last scene is of Burnham visiting the burial and monument to the Shangani Patrol at Matopo Hills.

==Primary cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Brian OShaughnessy Major Allan Wilson 
|-
| Will Hutchins || Frederick Russell Burnham, the American Scout 
|- Major Patrick Forbes
|-
| Stuart Brown || Dr. Leander Starr Jameson 
|-
| Anthea Crosse || May Wilson
|-
| Lance Lockhart-Ross || Captain Indy
|-
| Ian Yule || Trooper Dillon
|-
| Patrick Mynhardt || Hofmeyer
|}

==Production==
Shangani Patrol was the third film made by South African producers David Millin and Roscoe Behrmann under the newly formed RPM Film Studios.  The film was shot entirely on location in Rhodesia, in the Marula district, about sixty miles from Bulawayo and not far from the historical battles of the First Matabele War.  Shooting was completed in the scheduled six weeks. 

==See also==
* Shangani Patrol (soundtrack album)
* Major Wilsons Last Stand (film)

==References==
 

==External links==
*  

 
 
 
 
 