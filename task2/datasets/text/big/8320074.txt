Take Down (film)
{{Infobox film name = Take Down image      = TakeDown(Film)1979MoviePoster.jpg director   = Kieth Merrill writer     = Eric Hendershot Kieth Merrill starring   = Edward Herrmann Lorenzo Lamas Maureen McCormick Kathleen Lloyd Maxx Payne Stephen Furst music      = Merrill Jenson cinematography = Reed Smoot editing     = Peter Teschner producer   = Kieth Merrill David B. Johnson studio     = American Film Consortium Buena Vista Distribution   Unicorn Video   budget     = 
|released= January 1979 runtime    = 107 minutes language = English
|}} Buena Vista Distribution Company about an unqualified teacher who finds himself saddled with coaching duties after a small group of high school seniors form a wrestling team in a last-ditch effort to end a 9-year losing streak against a rival school. 

==Plot==
Ed Branish (Edward Herrmann), a snooty English teacher who finds his situation at Mingo Junction High School far beneath him and barely tolerable, flaunts his air of superiority over all, including his supportive wife (Kathleen Lloyd), by frequently spouting platitudes from literary masters to validate his often contemptuous viewpoint.

His habit of having as little to do with his school as possible finally gets the better of him when his light schedule makes him the only staff member available to supervise the newly formed wrestling team. Cornered, he lashes out at the first student to cross his path - Nick Kilvitus (Lorenzo Lamas), a reserved 185 lbs. senior whos embarrassed by his near-poverty social status and whos also missed a lot of classes lately. No one realizes Nicks been filling in for his alcoholic father at a steel mill (hauling I-beams) when his dads too drunk or hung over to show up (which is all too often, and which also keeps Nick busy at night fishing his embittered father out of bars - to be carried home (Firemans Carry style) across town because they dont have a working car).

Nick hopes he can make up the missed school work in Eds class to graduate in Spring but instead gets a tongue-lashing on how he should be held back as an example of the consequences of laziness and irresponsibility. In turn, Nick calls Ed an egotistical snob telling him that he is more interested in proving how smart he is instead of teaching. Fortunately, because of the wrestling team, both will cross paths again and discover they each have much more to them than what they were previously aware.

Maureen McCormick of The Brady Bunch|Brady Bunch fame plays a supporting role as Nicks girlfriend, Brooke Cooper.

== Cast ==

*Edward Herrmann - Ed Branish
*Kathleen Lloyd - Jill Branish
*Lorenzo Lamas - Nick Kilvitus
*Maureen McCormick - Brooke Cooper
*Maxx Payne - Ted Yacabobich (credited as Darryl Peterson)
*Stephen Furst - Randy Jensen
*  - Chauncey Washington
*Salvador Feliciano - Tom Palumbo
*Boyd Silversmith - Jack Gross
*  - Jimmy Kier (credited as  )
*Kevin Hooks - Jasper MacGrudder
*Scott Burgi - Robert Stankovich
*Lynn Baird - Doc Talada
*Ron Bartholomew - Warren Overpeck
*  - Bobby Cooper
* David M Thorne - Hood #2

==Production==

This was Disneys first "PG" rated film, five years before they launched Touchstone Pictures to expand into the "adult" market. The companys name never appeared on this title though: the only reference to the Disney name was that it was released thru Disneys Buena Vista Distribution Co., Inc.  The film was reportedly a critical and financial disaster. It has barely resurfaced after its original release, except for a VHS home video release by Unicorn Video in the 1980s, long out of print.

== Location == Murray High School in Murray, Utah.

==References==
 

== External links ==
*  

 
 
 