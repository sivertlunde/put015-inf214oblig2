The White Haired Witch of Lunar Kingdom
{{Infobox film
| name = The White Haired Witch of Lunar Kingdom
| image = The White Haired Witch of Lunar Kingdom poster.jpg
| alt = 
| caption = Theatrical poster
| film name = {{Film name| traditional = 白髮魔女傳之明月天國
 | simplified = 白发魔女传之明月天国
 | pinyin = Báifà Mónǚ Zhuàn Zhī Míngyuè Tiānguó
 | jyutping = 
 | poj = }}
| director = Jacob Cheung
| producer = Huang Jianxin
| screenplay = Kang Qiao Wang Bing
| story = Liang Yusheng
| starring = Fan Bingbing Huang Xiaoming Vincent Zhao Wang Xuebing Ni Dahong Tong Yao Li Ruxin
| music = 
| cinematography = Lin Guohua
| editing = 
| studio = Bona Film Group
| distributor = Bona Film Group
| released =  
| runtime = 103 mins
| country = China
| language = Mandarin
| budget = 100 million yuan   (US$16 million)   
| gross =  
}}
 fantasy 3D film loosely adapted from Liang Yushengs novel Baifa Monü Zhuan. Directed by Jacob Cheung and produced by Bona Film Group, the film stars Fan Bingbing as the titular character, with Huang Xiaoming, Vincent Zhao and others among the supporting cast.    Originally scheduled for release on 25 April 2014, the film was moved to 1 August 2014, then moved a day earlier to 31 July 2014.  

==Cast==
* Fan Bingbing as Lian Nishang
* Huang Xiaoming as Zhuo Yihang
* Vincent Zhao as Jin Duyi
* Wang Xuebing as Murong Chong
* Ni Dahong as Wei Zhongxian
* Tong Yao as Ke Pingting
* Li Xinru as Tie Shanhu
* Cecilia Yip as Ling Yunfeng Huangtaiji
* John Do 
* Lai Xiaosheng

==Production==
The White Haired Witch of Lunar Kingdom was produced at a budget of 100 million yuan. Shooting started in November 2012 and ended in March 2013. During filming, Huang Xiaoming had a three-metre fall after a wire accident on the set and he fractured two toes on his left foot. He had to sit in a wheelchair for weeks, but resumed filming even though he had yet to fully recover. On 2 April 2013, Huang and Fan Bingbing attended a press conference in Beijing to talk about their experiences in filming White Haired Witch.  

==Reception==
===Box office===
The film grossed US$61,900,000 in mainland China  and a total of   internationally. 

===Critical response===
The film received negative reviews from audiences. Wang Sicong, Wang Jianlins son, wrote on his microblog that this film was Fan Bingbings worst film ever, and gave it a 0 score. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 