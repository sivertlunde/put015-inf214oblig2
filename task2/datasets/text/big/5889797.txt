A Midsummer Night's Rave
{{multiple issues|
 
 
}}

{{Infobox film name = A Midsummer Nights Rave image      = director   = Gil Cates Jr. writer     = Rober Raymond William Shakespeare (Play) starring   = Corey Pearson   Lauren German Andrew Keegan Chad Lindberg Sunny Mabrey producer  Leslie Bates Steve Eggleston
|distributor= Velocity Home Entertainment released = 2002 runtime    = 85 minutes language = English budget = ~US$1,500,000
}}
 adapted from Shakespeares A Midsummer Nights Dream released in 2002. It is set at a rave instead of in a forest, like the original.

== Cast ==
*Corey Pearson - Damon (Demetrius)
*Lauren German - Elena (Helena (A Midsummer Nights Dream)|Helena)
*Andrew Keegan - Xander (Lysander (Shakespeare)|Lysander)
*Chad Lindberg - Nick (Nick Bottom)
*Sunny Mabrey - Mia (Hermia (role)|Hermia) Jason Carter - OB John (Oberon)
*Nichole Hiltz - Britt (Titania)
*Glen Badyna - Puck (Puck (Shakespeare)|Puck)
*Olivia Rosewood - Tami (Snug (A Midsummer Nights Dream)|Snug) Chris Owen - Frankie (Peter Quince)
*Will McCormack - Greg (Francis Flute)
*Greg Zola - Snout (Tom Snout)
*Keri Lynn Pratt - Debbie (Robin Starveling)
*Jamie Anderson - Amanda
*Jennifer Crystal Foley - Lily
*Jolie Summers - Rose
*Jennifer Foy - Violet
*Sara Arrington - TP Fairy	
*Terry Scannell - Doc
*Jennifer Rebecca Bailey - Jennifer
*Dinah Lee - Jessica
*Sean Whalen - Gil
*Jason London - Stosh
*Matt Czuchry - Evan

== External links ==
* 
 
 

 
 
 
 
 
 
 
 
 


 