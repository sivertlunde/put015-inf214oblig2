Saint John of Las Vegas
{{Infobox film
| name           = Saint John of Las Vegas
| image          = SaintJohnofLasVegas.jpg
| caption        = Theatrical release poster
| director       = Hue Rhodes
| producer       =  
| story          = Dante Alighieri
| screenplay     = Hue Rhodes
| starring       =  
| music          = David Torn
| cinematography = Giles Nuttgens
| editing        = Annette Davey
| studio         = Circle of Confusion
| distributor    = IndieVest Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $2 million
| gross          = $102,647 
}}
Saint John of Las Vegas is a 2009 American comedy-drama film starring Steve Buscemi, Romany Malco, and Sarah Silverman.

St. John of Las Vegas was the first film released by IndieVest Pictures, a subsidiary of IndieVest. 

The film, directed and written for the screen by Hue Rhodes (based on a story by Dante Alighieri) and produced by Steve Buscemi, Stanley Tucci, and Spike Lee, follows an ex-gambler as he takes a road trip with his new partner, an auto insurance fraud debunker, to investigate a fraud, while meeting a series of offbeat characters, including a carnivals human torch, a paraplegic stripper, and a nude militant, along the way.

The film was shown in film festivals and was released in limited release on January 29, 2010.

==Plot==
A compulsive gambler attempts to cure his addiction by moving from Las Vegas to Albuquerque and working at an auto insurance company, only to find old temptations cropping up once again when hes sent out to investigate a dubious car accident just outside of Sin City.

After a string of bad luck at the tables, John (Steve Buscemi) decides to give up gambling and take a shot at a "normal" life. Arriving in Albuquerque and landing a job at an auto insurance company, John goes to work for Mr. Townsend (Peter Dinklage), who pairs him with the companys top fraud debunker, Virgil (Romany Malco), and sends them out on an investigation together. While John is eager to get a promotion, hes reluctant to go anywhere near Las Vegas, and before he leaves he strikes up a tenuous romance with his eccentric co-worker, Jill (Sarah Silverman).

On the road, Virgil and John encounter a series of offbeat characters including Ned, a nude militant (Tim Blake Nelson), Tasty D Lite, a wheelchair-using stripper (Emmanuelle Chriqui), and a carnival human torch (John Cho). But while Virgil is the one with the experience, John gradually begins to assert himself and soon his efforts begin to pay off as the case moves closer to conclusion. As Johns confidence grows, he becomes increasingly aware of the fact that running away from his gambling problem is not the solution, and that hell only be able to move forward by returning to Las Vegas to face his demons head on.

==Cast== gambler
* Romany Malco as Virgil, an insurance fraud investigator
* Sarah Silverman as Jill, Johns co-worker and love interest
* Peter Dinklage as Mr. Townsend, Johns boss nude militant
* Danny Trejo as Bismarck
* John Cho as Smitty, a carnival human torch
* Emmanuelle Chriqui as Tasty D Lite, a paraplegic stripper Aviva as Penny

==Release and reception==
The film received $21,666 in its opening weekend at #50 and reportedly gross domestic box office receipts totaled just over $100,000.  

The film has mostly received mostly negative reviews, with a 24% rating with 12 "fresh" ratings and 39 "rotten" on Rotten Tomatoes. 

Chris Nashaway of Entertainment Weekly called the film "Excruciatingly awful" and "a shaggy road movie to nowhere". 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 