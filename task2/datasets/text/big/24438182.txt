Bank Bang
{{Infobox film
| name           = Bank Bang
| image          = 
| image size     =
| caption        =
| director       = Argyris Papadimitropoulos
| producer       =
| writer         = 
| narrator       =
| starring       = Vassilis Haralambopoulos   Marissa Triandafyllidou Dimitris Imelos Kostas Voutsas 
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 
| country        = Greece Greek
| budget         =
| preceded by    =
| followed by    =
}} highest grossing Greek films in this season.

==Plot==

The story of the film revolves around two brothers Mixalis and Nodas who run a funeral office constantly making plans about how to make a lot of money. However, Nodas is in trouble with the Greek mafia and owes them a large sum of money. He suggests to Mixalis to start robbing banks in order to pay off his debt. Things start getting complicated when the chief of police, in order to deal with the rising number of bank robberies,places undercover police officers in every bank in the country. The situation goes out of control when Mixalis falls in love with a bank employee who has a rather graphical family life.

==Casting==
*Kostas Voutsas as Ermolaos
*Vassilis Haralambopoulos as Mixalis
*Dimitris Imelos as Nodas
*Marisa Triantafilidou as Lena
*Katerina Mavrogeorgi as Mina
*Mixalis Iatropoulos as Makis
*Gerasimos Skiadaresis as Kostas
*Dimitris Mavropoulos as Bazoukas
*Tzeni Botsi as Katia
*Thanos Samaras as Doukas
*Skotis Drosos as Sam
*Ieronimos Kaletsanos
*Giannis Tsimitselis
*Fanis Mouratidis
*Orfeas Avgoustidis

==Awards==
{| class="wikitable"
|+ List of awards and nominations 
! Award !! Category !! Recipients and nominees !! Result
|- Hellenic Film 2010 Hellenic Best First Argyris Papadimitropoulos|| 
|}

==References==
 

==External links==
* 
* 

 
 

 