Guarding Tess
{{Infobox film
| name           = Guarding Tess
| image          = Guarding Tess 1994.jpg
| alt            = 
| caption        = Original poster Hugh Wilson
| producer       = {{Plain list |
* Ned Tanen
* Nancy Graham Tanen
}}
| writer         = {{Plain list | Hugh Wilson
* Peter Torokvei
}}
| starring       = {{Plain list |
* Shirley MacLaine
* Nicolas Cage
* Austin Pendleton
* Edward Albert
* James Rebhorn
* Richard Griffiths
}}
| music          = Michael Convertino
| cinematography = Brian J. Reynolds
| editing        = Sidney Levin
| distributor    = TriStar Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $27,058,304
}} Hugh Wilson.
 First Lady Secret Service agents led by one she continually exasperates (Cage).

The film is set in Somersville, Ohio (assigned Parkton, Maryland ) and nominated for a Golden Globe award in 1995 (Best Performance by an Actress in a Motion Picture&nbsp;– Comedy/Musical: Shirley MacLaine).

==Plot==
 
Doug Chesnic (Nicolas Cage) is a Secret Service agent who takes great pride in his job, performing his duties with the utmost professionalism. His assignment for the last three years has been a severe test of his patience. Doug is in charge of a team stationed in Ohio to protect Tess Carlisle (Shirley MacLaine), the widow of a former U.S. President.

Tess is well known for her diplomatic and philanthropic work, but seems to regard Doug less as a security officer and more as a domestic servant—not unlike her chauffeur, Earl (Austin Pendleton), or her nurse, Frederick (Richard Griffiths).

Doug regards it as beneath his professional dignity to perform little chores around the house or bring Tess her breakfast in bed. Tess orders him to do so, even to fetch her ball during a round of golf. Any time Doug defies her, Tess contacts a close friend - the current President of the United States – to express her displeasure. The annoyed President then phones Doug.

Dougs three-year hitch with Tess comes to an end, so he is eager to be given a more exciting and challenging assignment. But Tess has decided that she  wants him to stay, and, as usual, she gets her way.

Their bickering continues, even in the car. Alone there with Earl for a minute, Tess orders him to drive off, stranding her bodyguards. A humiliated Doug must phone the state police—not for the first time—to be on the lookout for her. He fires Earl when they return, but Tess manages to countermand that decision as well.

After she returns from a hospital checkup, Tess watches old television footage of her husbands funeral, concentrating on a momentary glimpse of Doug among the mourners, overcome with grief. It is an indication that perhaps she keeps Doug around because she values his loyalty to her husband and his company. She makes an effort to get on his good side, sharing a drink and a late-night conversation. Morale for the agents improved when Tess tells them that the President would be visiting her late husbands presidential library, but his subsequent cancellation lowered her spirits.

Doug finally does get the professional excitement he craved, but in a way he could not foresee. Tess is kidnapped. While the FBI takes charge of the investigation, Doug and his security detail are not only anxious and guilt-ridden, but are informed that Mrs. Carlisles recent dizzy spells are actually being caused by a brain tumor.

As the investigation begins without him, Doug finds evidence of Earl being involved. In Earls hospital room, where the chauffeur was taken after claiming to be rendered unconscious by the abductors, Doug threatens to shoot off Earls toes, one by one, until he confesses to an FBI agent (James Rebhorn) where Mrs. Carlisle is being held. Doug goes so far as to shoot one toe.  Earl admits that Mrs. Carlisle is being held captive by Earls sister and her husband.

The FBI and Secret Service raid the kidnappers home and arrest them. When they find Mrs. Carlisle buried, but alive, beneath the floor of the farmhouse, Doug and his agents volunteer to do the digging. Tess then insists that her Secret Service detail accompany her to the hospital, forcing several high-ranking law enforcement officials to leave the rescue helicopter.

Upon being released from the hospital, Tess refuses to obey the hospital rule that patients must be discharged in a wheelchair. Doug tells her, using her first name for the first time, "Tess, get in the god damn chair." After a brief pause, Tess complies, pats Dougs hand and says, "Very good, Douglas."

==Cast==
* Shirley MacLaine as Tess Carlisle
* Nicolas Cage as Doug Chesnic
* Austin Pendleton as Earl Fowler
* Edward Albert as Barry Carlisle
* James Rebhorn as Howard Schaeffer
* Richard Griffiths as Frederick
* John Roselius as Tom Bahlor
* David Graf as Lee Danielson
* Don Yesso as Ralph Buoncristiani
* James Lally as Joe Spector
* Brant von Hoffman as Bob Hutcherson
* Harry J. Lennix as Kenny Young
* Susan Blommaert as Kimberly Cannon
* Dale Dye as Charles Ivy
* James Handy as Neal Carlo

==Reception==
Guarding Tess received mixed reviews from critics. Rotten Tomatoes gives the film a 59% rating on based on 32 reviews. 

==References==
 

==External links==
 
*  
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 