Kalinovski Square
{{Infobox Film name        = Kalinovski Square image       =  caption     =  director    = Yury Khashchavatski (Second director: Sergey Isakov) producer    = Marianna Kaat   Baltic Film Production  writer      = Yury Khashchavatski   with participation of   Yevgeni Budinas and   Sergey Isakov starring    = Alexander Lukashenko distributor = Deckert Distribution GmbH (DVD) released    = 2007 (DVD: 9 June 2009 ) runtime     = 58/73/87 min. country     = Estonia language    = Russian   Belarusian   English (subtitles) budget      = 
}}

Kalinovski Square (original title: Плошча, or Plošča) is an award-winning 2007 documentary film by Belarusian filmmaker Yury Khashchavatski. The film takes a critical look at the re-election of Alexander Lukashenko in March 2006, featuring especially the protests that occurred after the election was found to be Belarusian presidential election, 2006#Reaction|fraudulent. The protests had their centre at October Square in downdown Minsk, which was informally renamed on the occasion for Konstanty Kalinowski.

Before its commercial release on DVD on 9 June 2009, the film was distributed under the title The Square.

Since the Belarusian authorities already persecuted director Yury Khashchavatski since his first movie An ordinary president (1996), all production was done underground. 

== Content ==
The movie explores the question how it could be possible that 83 percent voted for Lukashenko, who has been referred to as "Europes last dictator".   Drawing on a lot of archive material, the movie shows the official propaganda and the naivety of simple people in the countryside, together with massive repression helped to defeat the Jeans Revolution. The narrative style resembles that of Michael Moore.

== Awards ==
The film won the Award Ion Vatamanu "for love of truth and freedom" at The International Documentary Film Festival CRONOGRAF, in Chişinău, 15–19 May 2008. http://www.cronograf.md/eng_2008/ 

It also won the Alpe Adria Cinema prize as the Best Documentary at the 19th Trieste Film Festival 2008. The prize was awarded for "the director’s ability to create evocative images and document reality as he sees it, from his passionate, if not at times sarcastic, perspective." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 