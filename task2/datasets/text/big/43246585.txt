Many Happy Returns (1934 film)
{{Infobox film
| name           =  Many Happy Returns 
| image          = Many Happy Returns 1934.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Norman Z. McLeod
| producer       = 
| writer         = Lady Mary Cameron (story "Often a Bridegroom"), Ray Harris, Keene Thompson 
| screenplay     = Claude Binyon, J.P. McEvoy 
| story          = 
| based on       =  
| starring       = 
| narrator       =  Arthur Johnston (uncredited)  Henry Sharp
| editing        = Richard C. Currier
| studio         = Paramount Pictures
| distributor    = 
| released       =    
| runtime        = 64 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}
Many Happy Returns is a 1934 American  Paramount Pictures comedy film directed by Norman Z. McLeod and starring Gracie Allen, George Burns, and George Barbier. 

==Cast==
*Gracie Allen as herself 	
*George Burns as himself
*George Barbier as Horatio Allen
*Joan Marsh as Florence Allen
*Ray Milland as Ted Lambert Stanley Fields as Joe John Kelly as Mike
*William Demarest as Brinker
*Johnny Arthur as Davis
*Franklin Pangborn as Allens Secretary
*Egon Brecher as Dr. Otto von Strudel
*Jack Mulhall as actor
*Morgan Wallace as Nathan Silas
*Kenneth Thomson as Motion Picture Director
*Guy Lombardo as himself
*Veloz and Yolanda as specialty dancers
 
==References==
 

==External links==
* 

 
 
 
 
 
 
 

 