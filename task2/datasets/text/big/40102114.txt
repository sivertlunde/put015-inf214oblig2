Humshakals
 
 
{{Infobox film
| name           = Humshakals
| image          = Humshakals poster.jpg
| alt            = The image shows a human pyramid consisting of three men in matching suit coats above the same three men in matching casual shirts above the same three men in matching overcoats and hats against yellow background and the word "Humshakals" in multicolored letters.
| caption        = Theatrical release poster Sajid Khan
| producer       = Vashu Bhagnani Sajid Khan
| screenplay     =
| story          = 
| narrator       = 
| starring       = Saif Ali Khan Riteish Deshmukh Bipasha Basu Tamannaah Bhatia Esha Gupta  Ram Kapoor
| music          = Himesh Reshammiya Background Score: Sandeep Shirodkar
| cinematography = Ravi Yadav
| editing        = Rameshwar S. Bhagat
| studio         = Fox Star Studios
| distributor    = Pooja Entertainment India Ltd.
| released       =  
| runtime        = 158 minutes
| country        = India
| language       = Hindi
| budget         =   
| gross          =   
}}
 Sajid Khan and produced by Vashu Bhagnani. Saif Ali Khan, Ram Kapoor and Riteish Deshmukh are in triple roles. The film is shot extensively in foreign locations, starting September 2013.    The film is co-produced by Fox Star Studios and Vashu Bhagnanis Pooja Entertainment.    The film was released on 20 June 2014 . 

==Plot== Nawab Shah) to get rid of Ashok and take over all his property. During a board meeting Mamaji mixes a medicine in Ashok and Kumars drink and they both start behaving like dogs.

They are taken to a mental asylum for treatment by Dr. Shivani (Esha Gupta) who soon realises the truth and promises to discharge them. But fate takes a twist as two lookalikes of Ashok and Kumar being treated in the same hospitals B ward (who work for Bijlani (Chunkey Pandey), a cocaine smuggler) are accidentally released instead of the true ones. Shanaya and Mishti take them to Ashoks mansion were Mamaji understands the whole mix-up and plans to use the duplicate Ashok to become the owner of the whole business empire. Now the true Ashok and Kumar also come to know of Mamajis evil plan but are stuck in the asylum. They are offered help by one of the ward boy Cyrus (Darshan Jariwala), who takes them to a secret ward C to meet Johnny who is a look alike of Mamaji but has a dangerous habit of attacking people who sneeze in front of him. Ashok and Kumar plan revenge against Mamaji with the help of Johnny but get caught by the warden Y. M. Raj (Satish Shah) before they can escape from the asylum. Y. M. Raj prepares to punish them but accidentally sneezes in front of them. Angered by this, Johnny beats him black and blue, thus helping the three to escape.
 House of Commons were the fake Ashok is to hand over all the business to him in the presence of Prince Charles but the real Ashok & Kumar arrive along with the triplicate of Mamaji. To add to the confusion the three mental patients also storm in to help the real Ashok & Kumar creating panic among the eyewitnesses on seeing so many lookalikes together.

Movie closes with Ashoks father coming out of his coma and recognising the true Ashok and getting Mamaji arrested for his deeds.

==Cast==
* Saif Ali Khan as Ashok
* Tamannaah Bhatia as Shanaya
* Riteish Deshmukh as Kumar
* Bipasha Basu as Mishti
* Ram Kapoor as Mamaji / Johnny / Rajvinder
* Esha Gupta as Dr. Shivani Gupta
* Satish Shah as Y.M. Raj 
* Darshan Jariwala as Cyrus the Ward boy
* Chunkey Pandey as Bijlani  Nawab Shah as Dr. Khan

==Filming==
Shooting of the film commenced on 24 September 2013 in London with Saif Ali Khan, Ram Kapoor and Riteish Deshmukh all of whom have reportedly been cast in triple roles.    It was reported that all three actresses Tamannaah Bhatia and Bipasha Basu, Esha Gupta have sported Bikini in the movie.  The team of Humshakals shot in Mauritius for the final schedule of the film starting on 22 February 2014. Two songs and several sequences was shot at the La Plantation Resort & Spa, Le Méridien, Intercontinental Resort, Bagatelle Mall of Mauritius and the Northern Beaches, Mauritian extras also participated in the shooting.      

After the release and the poor critical response, Saif Ali Khan told the press that "The film didnt have a script as such, it was all there in Sajids mind."   

==Soundtrack==
{{Infobox album
| Name = Humshakals
| Artist = Himesh Reshammiya
| Type = Soundtrack
| Cover =
| Released =  
| Length =  
| Genre = Film Soundtrack Zee Music Company Sony Music
| Last album = The Xposé (2014)
| This album = Humshakals (2014)
| Next album = Kick (2014 film)|Kick (2014)
}}
The soundtrack of Humshakals is composed by Himesh Reshammiya and lyrics are written by Mayur Puri, Sameer and Shabbir Ahmed. The first single of the movie is "Caller Tune", by Neeraj Shridhar and Neeti Mohan. The soundtrack was released on 26 May 2014.

===Track listing===
{{Track listing
| headline = Track Listing
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length =  
| title1 = Caller Tune
| extra1 = Neeraj Shridhar, Neeti Mohan
| lyrics1 = Sameer
| length1 = 4:35
| title2 = Piya Ke Bazaar Mein
| extra2 = Himesh Reshammiya, Shalmali Kholgade, Palak Muchhal
| lyrics2 = Shabbir Ahmed
| length2 = 4:22
| title3 = Look into My Eyes
| extra3 = Ash King, Neeti Mohan
| lyrics3 = Sameer
| length3 = 3:36
| title4 = Barbaad Raat
| extra4 = Sanam Puri & Shalmali Kholgade
| lyrics4 = Mayur Puri
| length4 = 3:24
| title5 = Hum Pagal Nahi Hai
| extra5 = Himesh Reshammiya
| lyrics5 = Mayur Puri
| length5 = 4:26
| title6 = Khol De Dil Ki Khidki
| extra6 = Mika Singh, Palak Muchhal
| lyrics6 = Shabbir Ahmed
| length6 = 4:16
}}

==Reception==
Despite poor critical reception, the film did well at the box office during first few days after release.    The film made 63.72 which Koimoi estimates is less than half of its investment.   

===Critical response===
The critical response was largely negative.    Mihir Fadnavis wrote in his Firstpost review, "... sexual tomfoolery, shrieking and hamming aside, theres much more to hate about this family movie. Its disturbing to see such an atrocious, regressive, misogynistic, sexist, homophobic cinematic product force-fed to paying audiences. I can understand that a comedy need not be safe, but what goes on in Humshakals is simply too horrifying to bear."  Sweta Kaushal of Hindustan Times rated the film 0.5 out of 5 and stated "With no story or comedy on offer, even Riteish and Ram are unable to save the day for Sajid Khan."  Mohar Basu of Koimoi gave it 0/5 stars and said the film was " oaded with indecipherable dim wit" and "an odd mishmash of pathos and drudgery." 

Bipasha Basu called Humshakals "the worst experience of   life,"  and Esha Gupta warned family members not to see the film. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 