Heart Attack (film)
 
 

{{Infobox film
| name           = Heart Attack
| image          = Heart Attack film poster.jpg
| image_size     = 
| caption        = Movie Poster
| director       = Puri Jagannadh
| producer       = Puri Jagannadh
| writer     = Puri Jagannadh
| based on       = 
| starring       = {{Plainlist | Nithiin
* Adah Sharma
}}
| music          = Anup Rubens
| cinematography = Amol Rathod
| editing        = S. R. Shekhar
| studio         = Puri Jagannadh Touring Talkies
| distributor    = Vaishno Academy
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Telugu ,english
| budget         =   
| gross          =    (100 days)  
}}
 Telugu film Nithiin and Adah Sharma in lead roles.  In 38 Theaters, Across A.P.  Anoop Rubens composed the soundtrack for the film. The film released on 31 January 2014 worldwide to mixed reviews from the critics,   Heart Attack grossed   in 50 Days. 

==Plot==
The film starts in Goa with the henchmen of Don Makarand Kamaati (Vikramjeet Virk) kidnapping a girl at the beach and turning her into a slave for sale after injecting drugs into her. When police officer Madhusudhan (Devan (actor)|Devan) catches the slaves and drugs racket, Makarand and his henchman (Ajaz Khan) interfere him while speaking to the Media. They injure the honest Madhusudhan and turn him an immobile person. Thus their trade continues. The film then shifts to a time a year later where Varun (Nithiin (actor)|Nithiin) is shown roaming in the locales of Spain. Varun is a hippie kind of vagabond wandering all over the world. He lost his parents in his childhood when he was at America. Calling himself a free soul, he never believes in relationships and earns his living by working in part-time jobs he finds in Craigslist and rests at nights on the roads with some kitchen equipment to cook food. Now he is in Spain and there he comes across Hayathi (Adah Sharma), an Indian who arrived to Spain to meet her friend Priya (Kesha Khambati).

In order to capture her attention, Varun pretends to suffer with a Heart Attack and later takes her phone number but fails to know her name. Hayathi came to Spain to convince Priyas father ISKCON Ramana (Brahmanandam), a staunch Krishna devotee and person having hatred for Love and Lovers, to accept the Love marriage of Priya and Haridas (Hari Das), an African musician. After much chasing and teasing Varun asks Hayathi to give him a deep lip lock for which she denies and slaps him. Thus he keeps a stipulation that if Varun makes Ramana accept the marriage of the lovers, Hayathi has to kiss him. Varun wins the challenge and as per stipulation, Hayathi has to kiss him. On a separate note, Varun also exposes the drug racket of Makarand. After fighting with Makarands men Ammu (Ajay (actor)|Ajay) and others at the arena, he locks lips with Hayathi for a duration for more than an hour. But that lip lock too had a condition from Hayathi. When Varun asks for the condition, Hayathi levies a condition that Varun should never meet her at any cost as she is heartbroken since she was madly in love with Varun and he has no belief in relationships.

Varun leaves to Romania where he meets a girl Chitrangada (Nicole Amy Madell) who also has a similar mindset. After spending some quality time with her, Varun realizes that he is indeed in love with Hayathi. After getting a strong moral support from Chitrangada, Varun desperately goes to Priyas home where he finds a slip in which it is written that Hayathi is in Goa. When he reaches Goa, with the help of a Rajinikanth fan (Ali (actor)|Ali), a mobile food selling trader, Varun tracks down Hayathi and when he meets her, he faces a strong rejection from Hayati that the time is up. Varun desperately tries to speak with her, but Makarand, Ammu and his men come and intervene saying that Hayathi is Makarands would-be wife. Actually Hayati is Madhusudhans daughter and in order to meet the expenses of her fathers medication, Hayathi accepts marriage with Makarand without knowing the fact that Makarand was the reason for her fathers hospitalization. After killing Ammu and talking with Makarand, Varun comes to know this and slaps Hayathi and asks the reason to part with him and hiding the facts. She replies that her problems would ruin Varuns happiness which he enjoys as a free soul.

Varun is in need of 5 million rupees and he meets Prakash Raj (Prakash Raj), an influential person who offers Varun an amount of   20&nbsp;million rupees and asks him to save his daughter who is kidnapped by Makarand as a part of his slave trade. Varun enters Makarands den where he sees that Hayathi is also going to be a part of Makarands trade and already drugs are injected into her body. While Varun is fighting with Makarand and his men, he is also injected with a drug injection and before losing consciousness, Varun slits Makarands throat with a blade and kills him. Madhusudhan recovers from his injuries, Prakash Raj is happy with his daughter reaching him safely and the film ends with Varun and Hayathi reconciling and planning their Honeymoon after receiving flight trip tickets and money from Prakash Raj.

==Cast== Nithiin as Varun
* Adah Sharma as Hayathi
* Vikramjeet Virk as Makarand Kaamati
* Prakash Raj as Special Appearance
* Ajaz Khan as Makarand Henchmen
* Brahmanandam as Iskcon Ramana
* Ali as Thailavaa Fan
* Kesha Khambhati as Priya
* Tejaswi Madivada as Hayatis sister

== Reception == Oneindia Entertainment gave a review stating "Puri Jagannadh has opted for a routine romance drama, but he has written an engaging screenplay for Heart Attack, which has fare amount of twists and turns to keep you glued on to the screen. The director, who is master in making formulaic movies, managed to include commercial ingredients like romance, action, comedy, dialogues and family drama, which will definitely impress all classes of audience." and rated the film 3/5.  IndiaGlitz gave a review stating "Heart Attack hit the screens as a favorite this season. The film doesnt disappoint and lives up to the expectations. An entertaining youthful romantic-comedy with loads of action." 

== Soundtrack ==
{{Infobox album
| Name       = Heart Attack
| Type       = Soundtrack
| Artist     = Anup Rubens
| Cover      =
| Released   = 9 January 2014
| Recorded   = 2013 Feature film soundtrack
| Length     =   Telugu
| Label      = Puri Sangeet
| Producer   = Anup Rubens
| Reviews    = 3/5
| Last album = Bhimavaram Bullodu (2014)
| This album = Heart Attack (2014)
| Next album = Autonagar Surya (2014)
}}

The soundtrack of the film was composed by Anup Rubens. Lyrics to all songs are written by Bhaskarabhatla. The audio CDs are released into market through Puri Sangeet. The audio launch of the film held in Bangkok on 9 January 2014. One of the song thuhi hai thuhi   song got best feel song award.all the songs were well received by both the critics and audience.

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 27:42
| writing_credits =
| lyrics_credits  = yes
| music_credits   =
| title1          = Nuvvante Naaku
| lyrics1         = Bhaskarabhatla
| extra1          = Jassie Gift
| length1         = 03:45
| title2          = Thuhi Hai Thuhi
| lyrics2         = Bhaskarabhatla
| extra2          = Anup Rubens, Smitha Belluri
| length2         = 03:54
| title3          = Ra Ra Vasthava
| lyrics3         = Bhaskarabhatla
| extra3          = Chaitra Ambadipudi, Santosh
| length3         = 03:42
| title4          = Selavanuko
| lyrics4         = Bhaskarabhatla
| extra4          = Chaitra Ambadipudi
| length4         = 03:45
| title5          = Thats All Right Mam
| lyrics5         = Bhaskarabhatla Tippu
| length5         = 04:09
| title6          = Chupinchandey
| lyrics6         = Bhaskarabhatla
| extra6          = Rahul Nambiar
| length6         = 04:21
| title7          = Endhukila Nannu Vedhisthunavey
| lyrics7         = Bhaskarabhatla
| extra7          = Kunal Ganjawala
| length7         = 04:02
}}

==References==

 
 

== External links ==
*  

 
 
 
 