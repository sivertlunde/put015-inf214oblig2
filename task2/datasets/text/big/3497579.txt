The Barkleys of Broadway
{{Infobox film
| name           = The Barkleys of Broadway
| image          = Barkleys of broadway.jpeg
| image_size     = 250px
| caption        = theatrical poster
| director       = Charles Walters	
| producer       = Arthur Freed		
| writer         = Betty Comden Adolph Green Sidney Sheldon
| starring       = Fred Astaire Ginger Rogers
| music          = Music:  
| cinematography = Harry Stradling
| editing        = Albert Akst	
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $2,250,000  . 
| gross          = $4,421,000 
}}
 George and Hermes Pan. Also featured in the cast were Oscar Levant, Billie Burke, Jacques François and Gale Robbins. 

Rogers came in as a last minute replacement for Judy Garland, whose frequent absences due to a dependency on prescription medication cost her the role. This turned out to be the last film that Astaire and Rogers made together, and their only film together in color. Many critics at the time remarked upon Rogers changed figure, noting that the elfin girl of the 30s had made way for a sturdy, athletic woman. 

==Plot==
Josh and Dinah Barkley (Fred Astaire and Ginger Rogers) are a husband-and-wife musical comedy team at the peak of their careers. After finishing a new show, Dinah meets serious French playwright Jacques Pierre Barredout (Jacques François), who suggests that Dinah should take up dramatic acting. Dinah tries to keep the suggestion a secret from Josh, but when he finally discovers Dinah hiding a script for Jacques new show from him, the couple splits up.  

Their good friend, acerbic composer Ezra Miller (Oscar Levant) tries to trick them back together again, but fails.  When Josh secretly watches Dinahs rehearsals for Barredouts new play and sees how she is struggling, he calls her up and pretends to be the Frenchman, giving her notes that help her to understand her part, the young Sarah Bernhardt. As the result, Dinah gives a brilliant performance. After the show, she accidentally learns that her late-night mentor was Josh and not Barredout, so she rushes to Joshs apartment and the two reconcile.

==Cast==
*Fred Astaire as Josh Barkley
*Ginger Rogers as Dinah Barkley
*Oscar Levant as Ezra Miller
*Billie Burke as Mrs Livingston Belney
*Gale Robbins as Shirlene May
*Jacques François as Jacques Pierre Barredout
*Clinton Sundberg as Bert Felsher
*Inez Cooper as Pamela Driscoll
*George Zucco as The Judge
*Hans Conried as Ladislaus Ladi (uncredited)

;Cast notes
*MGM borrowed Jacques François from Universal Pictures. Barkleys was his first film in English, and was to be his last American film, although he did two U.K.-based productions.  His French film career was extensive: he worked up until his death in 2003. 

==Production==
The Barkleys of Broadway began with the title "You Made Me Love You", and with   in 1939.  Rogers was interested, and The Barkleys of Broadway became their tenth and final film together, as well as their only film in color. 

The production period was from 8 August through 30 October 1948, with some additional work on 28 December.  The Technicolor process was still relatively new at the time, and required very bright lights which were uncomfortable to work under.  While the film was in production, Fred Astaire won an honorary Academy Award for "his unique artistry and his contributions to the technique of musical pictures," presented to him at the awards ceremony by Ginger Rogers. 

The Barkleys of Broadway premiered in New York on 4 May 1949 and went into general American release shortly after. 

==Songs==
*"Swing Trot" - music by Harry Warren and lyrics by Ira Gershwin.  Dance critic Arlene Croce called this the best number in the film.  Seen through the opening credits, it was released without visual impediment on Thats Entertainment III (1997). Margarita Landazuri   
*"Sabre Dance" - by Aram Khachaturian, arranged for piano and orchestra, with Oscar Levant at the piano
*"Youd be Hard to Replace" - by Harry Warren and Ira Gershwin
*"Bouncin the Blues" - by Harry Warren
*"My One and Only Highland Fling" - by Harry Warren and Ira Gershwin
*"Weekend in the Country" - by Harry Warren and Ira Gershwin.  Performed by Astaire, Rogers and Oscar Levant.
*"Shoes with Wings On" - by Harry Warren and Ira Gershwin.  Fred Astaire performs this number alone, as part of the show that Josh Barkley does by himself. It utilized green screen technology to have Astaire, a cobbler, dance with many pairs of shoes.
*Piano Concerto No. 1 (Tchaikovsky), First Movement - Performed by Oscar Levant with full symphony orchestra. Shall We Dance, where Astaire had sung it to Rogers (as here).  Their dance duet here (ballroom - no tap), one of their most effective, was the first time they danced it together. 
*"Manhattan Down Beat" - by Harry Warren and Ira Gershwin

Three other Harry Warren-Ira Gershwin songs were intended for the film but never used: "The Courtin of Elmer and Ella," "Natchez on the Mississippi," and "Poetry in Motion." TCM   

==Response==
===Critical===
Critical response to The Barkleys of Broadway was mixed but positive. 
===Box Office===
According to MGM records the film earned $2,987,000 in the US and Canada and $1,434,000 overseas resulting in a profit of $324,000. 

==Awards and honors== Comden and Green were nominated for a Writers Guild of America Award for Best Written American Musical. 

==Adaptations==
A radio version of the film was broadcast on January 1, 1951 as an episode of the Lux Radio Theater, with Ginger Rogers reprising the role of Dinah Barkley, and George Murphy playing her husband and partner Josh.

==References==
Notes
 
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 