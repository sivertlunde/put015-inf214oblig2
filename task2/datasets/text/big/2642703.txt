Kedma (film)
 
{{Infobox film
| name           = Kedma
| caption          = 
| image	=	Kedma FilmPoster.jpeg
| director       = Amos Gitai
| producer       = Marin Karmitz Amos Gitai
| writer         = Mordechai Goldhecht Amos Gitai
| starring       = Andrei Kashkar Helena Yaralova Yussuf Abu-Warda Moni Moshonov Juliano Mer-Khamis David Darling Manfred Eicher
| cinematography = Giorgos Arvanitis
| editing        = Kobi Netanel
| distributor    = Celluloid Dreams
| released       = May 22, 2002
| runtime        = 100 minutes
| country        = Israel Hebrew Arabic Arabic French French German German Polish Polish Russian Russian Yiddish Yiddish
| budget         = 
| gross          = 
| preceded_by    =  
| followed_by    = 
}} 2002 cinema Israeli film directed by Amos Gitai and starring Andrei Kashkar and Helena Yaralova. It was entered into the 2002 Cannes Film Festival.   

==Plot==
The film is a historical tragedy set during the opening stages of Israels 1948 War of Independence. The film follows the fate of a group of refugees from the Holocaust who are illegally brought to Israel by the Palmach. When they arrive, they are chased by British soldiers. Once they escape, they are immediately drafted into the war, and take part in a grueling battle against Arab irregulars. The film centers on two long monologues, one by an Arab peasant who pledges to oppose the Jews forever; and one by an emotionally demolished refugee who laments the seemingly endless suffering of his people. Gitai intended the film to be a more realistic answer to the romanticized depiction of the war in Otto Premingers Exodus (1960 film)|Exodus. The final shot of Kedma is identical to the final shot of Premingers film.

==References==
 

==External links==
*  
* 
 
 
 
 
 
 
 
 
 
 
 

 