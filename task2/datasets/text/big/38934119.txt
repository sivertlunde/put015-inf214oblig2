1500 Steps
 
 
{{Infobox film
| name           = 1500 Steps
| image          = 1500 Steps.jpg
| alt            = Underwater, a beam of sunlight shines through
| caption        =Life is a race
| director       = Josh Reid
| producer       = Josh Reid Peter Cameron/Maurine Gibbons
| screenplay     = Maurine Gibbons
| story          = Maurine Gibbons
| starring       = {{Plainlist| 
* Alex Fechine
* Laura Benson
* Adam Dear
}}
| music          = Jared Haschek
| cinematography = Ben Raglione/Daniel Karjardi 
| editing        = Daniel Karjadi
| studio         = Earl Street Pictures
| distributor    = Bridgestone Multimedia Group
| released       =  
| runtime        = 88 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}
1500 Steps is an upcoming Australian drama film directed by Josh Reid.

==Cast==
* Alex Fechine as Jonas Jobe OBrien
* Laura Benson as Grace Glorious
* Jack Matthews as Damon Dundas
* Leanne Mackessy as Rebecca Dundas
* Adam Dear as Samuel Jacobs
* Dave Proust as Neil OBrien
* Keith Thomas as Harry White
* Richard Carwin as Tony Walker
* Colin Defries as Principal Ben Rigg
* Rupa Proia as Ibrahim Dib
* Alfie Gledhill as Isaac Johnstone Dave White as Mitch Martin
* Diab Metry as Callum Smith
* Joel Hogan as Jorge Sutton

==External links==
*  
*  

 
 
 


 