Annai Illam
{{Infobox film
| name           = Annai Illam
| image          =
| image_size     =
| caption        =
| director       = P. Madhavan
| producer       = M. R. Santhanam
| writer         = Aarur Dass
| screenplay     = G. Balasubramaniam
| story          = Dada Mirasi
| starring       = Sivaji Ganesan Devika S. V. Ranga Rao M. V. Rajamma M. N. Nambiar Nagesh
| music          = K. V. Mahadevan
| cinematography = P. N. Sundaram
| editing        = N. M. Shankar
| studio         = Kamala Pictures
| distributor    = Sivaji Productions
| released       =   
| country        = India
| runtime        = 147 mins Tamil
}}
 1963 Cinema Indian Tamil Tamil film, directed by P. Madhavan and produced by M. R. Santhanam. The film stars Sivaji Ganesan, Devika, S. V. Ranga Rao and M. V. Rajamma in lead roles. The film had musical score by K. V. Mahadevan.  

==Cast==
 
*Sivaji Ganesan
*Devika
*S. V. Ranga Rao
*M. N. Nambiar
*M. V. Rajamma
*R. Muthuraman
*V. K. Ramasamy (actor)|V. K. Ramasamy
*Nagesh
*O. A. K Thevar
*S. A. Kannan
*M. S. Sundari Bai Jayanthi
*Sachu
 

==Crew==
*Director: P. Madhavan
*Producer: M. R. Santhanam
*Production Company: Kamala Pictures
*Music: K. V. Mahadevan
*Lyrics: Kannadasan
*Story: Dada Mirasi
*Screenplay: G. Balasubramaniam
*Dialogues: Aarur Dass
*Cinematography: P. N. Sundaram
*Editing: N. M. Shankar
*Art Direction: Ganga
*Choreography: None
*Stunt: None
*Audiography: T. S. Rangasamy

==Soundtrack==
The music was composed by K. V. Mahadevan.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ennirandhu 16 Vayathu || T. M. Soundararajan || Kannadasan || 03.37 
|-  Susheela || Kannadasan || 04.17 
|- 
| 3 || Nadaiya Idhu Nadaiya || T. M. Soundararajan || Kannadasan || 03.09 
|- 
| 4 || Enna Illai || AL. Ragavan || Kannadasan || 02.57 
|- 
| 5 || Onnayirrukkakathukkanum || T. M. Soundararajan || Kannadasan || 03.13 
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 


 