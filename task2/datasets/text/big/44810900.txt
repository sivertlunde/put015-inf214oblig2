Too Young to Marry (1931 film)
{{Infobox film
| name           = Too Young to Marry
| image          =
| caption        =
| director       = Mervyn LeRoy
| producer       = First National Pictures
| writer         = Francis Edward Faragoh (screenplay)
| based on       = play Broken Dishes by Martin Flavin
| starring       = Loretta Young Grant Withers
| music          = Erno Rapee Leo Forbstein
| cinematography = Sidney Hickox
| editing        = John Rawlins
| distributor    = First National Pictures (thru Warner Brothers)
| released       = May 4, 1931
| runtime        = 67 minutes
| country        = USA
| language       = English
}}
Too Young to Marry is a 1931 talking film directed by Mervyn LeRoy and starring Loretta Young. It was produced and distributed by First National Pictures. It is based on a 1929 play Broken Dishes by Martin Flavin. 

It is preserved in the Library of Congress collection incomplete. 

==Cast==
*Loretta Young - Elaine Bumpsted
*Grant Withers - Bill Clark
*O. P. Heggie - Cyrus Bumpsted
*J. Farrell MacDonald - Dr. Horace Virgil Stump Richard Tucker - Chester Armstrong
*Emma Dunn - Jennie Bumpsted
*Aileen Carlyle - Mabel Bumpsted
*Lloyd Neal - Sam Green

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 