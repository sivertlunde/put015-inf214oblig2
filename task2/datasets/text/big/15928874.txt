Parisette
{{Infobox film
| name           = Parisette
| image          = Parisette2.jpg
| caption        = Scene from film
| director       = Louis Feuillade
| producer       = 
| writer         = Louis Feuillade
| starring       = Sandra Milovanoff Georges Biscot
| music          = 
| cinematography = Maurice Champreux Léon Morizet
| editing        = Maurice Champreux
| distributor    = 
| released       =  
| runtime        = 380 minutes
| country        = France  Silent French intertitles
| budget         = 
}}
 drama film serial directed by Louis Feuillade.   

==Cast==
* Sandra Milovanoff as Parisette
* Georges Biscot as Cogolin
* Fernand Herrmann as Le banquier Stephan
* Édouard Mathé as Pedro Alvarez
* René Clair as Jean Vernier
* Henri-Amédée Charpentier as Le père Lapusse
* Jeanne Rollette as Mélanie Parent (as Jane Rollette)
* Jane Grey as Mme. Stephan
* Pierre de Canolle as Joseph
* Bernard Derigal as Marquis de Costabella (as Derigal)
* Arnaud - Candido
* Gaston Michel

==See also==
* List of film serials
* List of film serials by studio
* List of longest films by running time

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 
 