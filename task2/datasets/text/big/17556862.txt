Breathing Room
{{Infobox Film 
| name           = Breathing Room 
| image          = Breathing room poster.jpg
| caption        = Film poster
| director       = John Suits Gabriel Cowan 
| producer       = Gabriel Cowan Bryce Gerlach 
| writer         = John Suits Gabriel Cowan
| starring       = Ailsa Marshall David Higlen Brad Culver Kim Estes Keith Foster 
| music          = Chris Kidd Kari Rae Seekins Karl Suits Tim Ziesmer| 
| cinematography = Gabriel Cowan John Suits 
| editing        = John Suits
| distributor    = Anchor Bay Entertainment
| released       =   }}
| runtime        = 89 minutes 
| country        = United States
| language       = English
| budget         = $19,000   
| gross          = 
|}} horror feature film written and directed by John Suits and Gabriel Cowan.

== Plot ==
Thrown naked in a desolate room with thirteen strangers, Tonya Mane discovers she is playing a deadly game. She must figure out the reason for her abduction. In the room, Tonya finds a tape recorder that explains the rules, one of the which state "Only those without collars can break the rules". One of the strangers cross a forbidden zone, and the electric collar that he wears kills him instantly. A box marked "Pieces" catches Tonyas interest, but none of the strangers tell her what is inside. Another stranger finds a piece of paper in his pocket; written on it is, "Hint: piece the pieces". Another rule is shown that players may not adjust their collars. As the game progresses, they become mad and frantically try to find a way out. The host explains that there can be only winner, and the prize is that persons life.

The strangers get to know each other. One is a recovering alcoholic, another is a teacher who teaches sixth grade English, and another works in sales; Tonya says she is a psychology student. One of the strangers is kidnapped by an unknown entity; however, another person finds her dead in the toilet. The game progresses once again, and another person finds a paper in a closed drawer. The note reads, "Hint: fourteen is the key", which seems to refer to Tonya, as she is the fourteenth contestant. A bespeckled man finds a paper in his pocket with the words "Hint: Jumble". Later on, another person is killed, and one of the strangers accuses the others  of murder. Enraged, the same person screams to break one of the cameras.

The prisoners find a mirror cabinet and use a hammer to break it. Inside, there is a black box, with "serial" written on it and containing flakes and a sealed paper. They open it and find the words, "hint: What is your favorite type of serial? In this game there are three. One rapist, one pedophile and one killer, find the killer, find the reason." The people ask each other more questions, and the host tells them they have a minute before someone is eliminated. One of them commits suicide by shooting himself in the mouth. Tonya pieces together a key. Amid further eliminations, the others learn that these killings are called "curfews". Tonya discovers that the key could unlock one of the collars. Two people get into a fight that causes one of them to start frothing and die. Another person finds himself inside with the others. The new contestant name is Robert Tanner. Tanner later finds a black box containing bullets with another paper with the words "Hint: Bite the bullet".

== Cast ==
* Ailsa Marshall as Fourteen (Tonya) 
* David Higlen as Five (Harold Riehl/Unemployed-Convicted Rapist) 
* Brad Culver as Six (Matt McKeown/Police Officer)
* Kim Estes as Ten (Henry) 
* Keith Foster as Host

== Production ==
Co-directors Gabe Cowan and John Suits met at Cal Arts during their masters program in film directing.  At the end of their first year, they shot Breathing Room.  Writing took two weeks, and it was shot in eight days. 

== Release ==
Breathing Room was released on DVD September 23, 2008. 

== Reception == Battle Royale.  Foy wrote that the film fails to live up to its potential, as too  much of it seems like filler.   Rod Lott of the Oklahoma Gazette called it a "largely effective, efficient film" that is 85% Cube and 15% Saw.  Lott gave the film "an A for effort, and a C for execution."   David Johnson of DVD Verdict called it a "taunt, well-made whodunit" with a convoluted denouement.  Johnson wrote that the challenges that the characters face are not memorable or clever, but the characters reactions are interesting.   Marc Patterson of BrutalAsHell.com rated the film 2/5 stars and called it too unoriginal to enjoy. 

== References ==
 

==External links==
*  
*  
*  

 
 
 
 