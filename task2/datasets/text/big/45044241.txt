Three for Breakfast
 
 
{{Infobox Hollywood cartoon
| cartoon name      = Three for Breakfast
| series            = Donald Duck
| image             = 
| image size        = 
| alt               = 
| caption           = 
| director          = Jack Hannah
| producer          = Walt Disney Nick George
| narrator          =  James MacDonald Dessie Flynn
| musician          = Oliver Wallace
| animator          = Bob Carlson Volus Jones Bill Justice Dan MacManus
| layout artist     = Yale Gracey
| background artist = Thelma Witmer Walt Disney Productions RKO Radio Pictures
| release date      =   (USA)
| color process     = Technicolor
| runtime           = 7 minutes, 4 seconds 
| country           = United States
| language          = English
| preceded by       = Soup’s On
| followed by       = Tea for Two Hundred (1949)
}} 1948 American animated short Walt Disney RKO Radio Chip and James MacDonald and Dessie Flynn voice Chip and Dale, respectively. The film includes original music by Oliver Wallace.

==Synopsis==
 
Donald Duck is cooking pancakes in his kitchen, singing "Shortnin Bread," when two chipmunks, Chip and Dale, get wind of the smell wafting through their home in the stovepipe. Chip and Dale attempt to steal Donalds pancakes;  Donald devises various ways to rid himself of the unwanted guests, including slipping Chip and Dale a pancake made of rubber cement instead,  but Chip and Dale eventually make off with all the pancakes, much to Donalds chagrin. 

==Alternate version== Asian stereotype.  In response, Dale puts a pancake on his head and imitates Donald with the same stereotype, to the chipmunks laughter.  Current showings of the film omit this sequence. 

==Errors==
The film contains multiple continuity errors.   The number of pancakes in Donalds stack fluctuates, as do the appearance of utensils on the table, syrup on the pancakes, and the presence of items in Donalds kitchen.  In the films final scene, when Donald has chased the chipmunks up to his roof, Dale spreads butter on some of the roof tiles; however, when Donald begins to slip on the butter, it disappears. 

==Releases==
 
*1948 &ndash; original theatrical release

==Notes==
 
 

 
 
 