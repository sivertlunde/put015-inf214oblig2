Armed and Innocent
{{multiple issues|
 
 
}}
{{Infobox film
|name=Armed And Innocent
|image=
|caption=
|director=Jack Bender
|producer=Jack Bender
|writer=Danielle Hill
|starring=Andrew Starnes Kate Jackson Gerald McRaney
|music=
|cinematography=
|editing=Jack Bender
|distributor=CBS
|released=January 4, 1994
|runtime=120 minutes
|country=United States
|language=English
|budget=Approximately $700,000
|gross=
}} thriller made-for-TV film, directed by Jack Bender and starring Andrew Starnes, Gerald Mcraney and Kate Jackson. Released in 1994, it was loosely based on actual events.

==Plot synopsis==
An 11-year-old boy, Chris Holland, is left at home alone after school. When three robbers invade their home, Chris uses a .22 rifle to shoot and kill two of them. The third, who escaped, and their imprisoned leader, Earl, hopes to get revenge on Chris who is traumatized by the incident.

==List of characters==
Chris Holland is the eleven-year-old star of the film who, when left at home alone, shoots and kills two robbers. The third robber, who escaped, plots with their now imprisoned former leader Earl, to get revenge. Chris is traumatized by the realization that he killed two people and has difficulty coping with his home and school life. His father, Bobby Lee, worsens Chriss situation by his distant and macho behavior, assuming that his son should feel like a hero and not suffer from the incident.

Bobby Lee Holland is Chriss father, played by Gerald McRaney. He tries to keep Chris away from trouble and to protect him, but he can have an attitude about him sometimes, especially when Chris had the gun in his bed and, during a dream, shot it at the wall.

Patsy Holland is Chriss overprotective mother, so overprotective that when a policeman explains to them that they cannot find the "third one that got away", she makes the family move to a new house.

Earl is the leader of the robbers who were shot and killed by Chris. When he gets out of jail near the end of the film, he picks up the escaped third robber, Ricky, and they seek revenge by confronting Chris and his family in the front yard of their new house.

==Release==
The film has had very limited release. It was released on VHS in Region 2 in the PAL format. It also had a Region 0, NTSC formatted DVD which was released in Japan. This does contain the English version of the film, with optional Japanese subtitles.

==External links==
* 

 
 

 
 
 
 