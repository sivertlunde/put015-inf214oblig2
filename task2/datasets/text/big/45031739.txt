Extravagance (1919 film)
{{Infobox film
| name           = Extravagance
| image          = Extravagance.jpg
| alt            = 
| caption        = Ad for film
| director       = Victor Schertzinger
| producer       = Thomas H. Ince
| screenplay     = John Lynch R. Cecil Smith  Donald MacDonald Philo McCullough
| music          = 
| cinematography = John Stumar
| editor         =	
| studio         = Thomas H. Ince Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Drama drama film Donald MacDonald, and Philo McCullough. The film was released on March 16, 1919, by Paramount Pictures.  

==Plot==
From the playbill-

{{Quotation|Big Puddle and Small Frog "Id rather be a paving stone in New York than a boulevard in any small town."

So Helen Douglas told her friends who asked her to give up the pace that kills.

But Helen wanted her paving stone made of gold and jewels and costly gowns and the luxury of life that goes with such things.

She got what she wanted. And in the getting came the crash-her husband on the brink of ruin with one chance of financial rescue in her hands.

Her own money could save him, but-

She refused him the money!

The story of what led up to that big dramatic moment in Helen Douglas life-and what followed will entertain and thrill you.}}

==Cast==
*Dorothy Dalton as Helen Douglas
*Charles Clary as Alan Douglas
*J. Barney Sherry as Hartley Crance Donald MacDonald as William Windom
*Philo McCullough as Billy Braden

== References ==
 

== External links ==
 
*  
 
 
 
 
 
 
 
 

 