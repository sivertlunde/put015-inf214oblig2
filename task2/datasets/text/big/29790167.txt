Mercenaries (2011 film)
 {{Infobox film
| name           = Mercenaries
| image          = 
| caption        = 
| director       = Paris Leonti
| producer       = John Adams Luc Chaudhary
| screenplay     = Paris Leonti
| starring       = Billy Zane Robert Fucilla Rob James-Collier 
| music          = Haim Frank Ilfman 
| cinematography = Philip Robertson
| editing        = Anthony Willis and Iain Mitchel
| studio         = ABC Films Angry Badger Pictures 
| distributor    = Kaleidoscope
| released       =  
| runtime        = 90 minutes 
| country        = United Kingdom
| language       = English and Occasional Serb Croat
| budget         = $1,000,000 (est.)
| gross          = 
}} Daylight Robbery. Ibiza Film Festival 2011  and also had a showing the same year at the Cannes film festival  

==Plot==
Andy Marlow is an ex-British S.A.S serviceman turned mercenary who is working covertly at an observation post in the Balkans after a military coup has resulted in the assassination of the Serbian Prime Minister. The coup has been instigated by Olodan Cracovic, the ex-commander of the Croat Army and wanted war criminal. During the unrest, Olodan’s army raided the U.S. Embassy and has taken the U.S Ambassador and his aide captives. The decision is taken to send in Mercenaries to carry out the top secret rescue. 
Marlow and his team are sent into Srebrenica under the cover of darkness. US Military personnel posing as United Nations peacekeepers are playing a support role, but cannot take part in active operations. Covertly, Andy and his team infiltrate Olodan’s headquarters, taking him captive and releasing the Ambassador and his aide. However the rescue is far from over. Now they are faced with transporting them back to a safe area twenty five miles south where US troops are waiting. With Olodan’s right hand man in pursuit, their mission takes an unexpected turn. They find themselves outnumbered, outgunned and fighting what could easily be a losing battle. 

==Cast==
*Billy Zane as Colonel Torida
*Robert Fucilla as Andy Marlow
*Kirsty Mitchell as Beatrice
*Rob James-Collier as Callum
*Vas Blackwood as Zac Geoff Bell as Vladko
*Andrew Harwood Mills as Grigory

==Reception==
Having had several festival showings and been released in Germany, Mercenaries has received many reviews, most of which have been scathing. It currently has a 3.9 rating on IMDB from over 670 users.  Total Film gave a rating of one of five stars, particularly criticising the production values, "Gunshots sound like they were sampled off a videogame, muzzle flashes make the whole screen flash".  David Hughes from Empire gave a similar review, however was not as critical, with a two out of five star rating. He said that the film had sufficient "fire power" but lacked any "substantial plot".  Tom Huddleston from TimeOut singled out Rob James-Colliers performance, his accent specifically, saying he was "trying on a Texan accent so hysterically ripe he must have learned it from watching ‘Foghorn Leghorn’ cartoons". Overall he gave a one star rating out of five. 

The focus for many reviewers seemed to have been over the quality of the script, with Peter Bradshaw from The Guardian saying that "the plot and dialogue is borderline ridiculous" overall, giving two out of five stars  Two London based online review sites, thisislondon and viewlondon also gave poor reviews. thisislondon gave two stars saying, "The chief culprit is Paris/Barry (!) Leontis screenplay"  and viewlondon gave one star saying, "the atrocious script doesnt give any of them much to work with" 

Not all reviews were so universally negative, some reviewers singled out particular actors performances such as Martin Daniel Mcdonagh from The Hollywood News who gave a two out of five star rating, praising Robert Fucillas performance.  The user reviews on IMDB have been particularly scathing about both fucilla performance.  Dan Collacott from the online site Close-upFilm, in contrast to the review in TimeOut, said that Rob James-Collier "is the most convincing mercenary of the bunch" 

==References==
 

==External links==
*  

 
 
 
 
 