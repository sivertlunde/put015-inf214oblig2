A Little Romance
{{Infobox film
| name           = A Little Romance
| image          = ALittleRomance.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = George Roy Hill
| producer       = {{Plainlist|
* Robert L. Crawford
* Yves Rousset-Rouard
}}
| screenplay     = {{Plainlist|
* Allan Burns
* George Roy Hill
}} Patrick Cauvin}}
| starring       = {{Plainlist|
* Laurence Olivier
* Thelonious Bernard
* Diane Lane Arthur Hill
* Sally Kellerman
}}
| music          = Georges Delerue
| cinematography = Pierre-William Glenn
| editing        = William H. Reynolds
| studio         = Orion Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

A Little Romance is a 1979 American romantic comedy film directed by George Roy Hill and starring Laurence Olivier, Thelonious Bernard, and Diane Lane in her film debut. The screenplay was written by Allan Burns and George Roy Hill, based on the novel E=mc 2  Mon Amour by Patrick Cauvin. The original music score was composed by Georges Delerue.        The film follows a French boy and an American girl who meet in Paris and begin a romance that leads to a journey to Venice where they hope to seal their love forever with a kiss beneath the Bridge of Sighs at sunset.
 Best Original Best Adapted Best Supporting Best Original Best Actor Best Actress Best Motion Picture Featuring Youth.    It was the first film released by Orion Pictures.  

==Plot==
Lauren King (Diane Lane) is a highly intelligent 13-year-old American girl living in Paris with her affluent family. She spends her free time reading Martin Heidegger|Heidegger. Daniel Michon (Thelonious Bernard) is a highly intelligent 13-year-old French boy from a poor family who loves Hollywood films and who uses his talent with mathematics to make theoretical bets on horse races. The two meet at a museum where a movie is being filmed, and they fall in love. Laurens self-absorbed mother (Sally Kellerman) fiercely objects to the romance, seeing him as a "filthy French boy", and thus unsuitable for her daughter. When Daniel punches the egomaniacal director boyfriend of Laurens mother, the two are forbidden to date.

Lauren and Daniel meet Julius (Laurence Olivier), a kindly elderly gentleman, who tells them about a tradition that if a couple kiss beneath the Bridge of Sighs in Venice at sunset while the church bells toll, they will be in love forever. Knowing that she will be sent back to America soon, she initiates a plan for them to travel to Venice together. With the help of Julius, the two young lovers take a train to Venice and spark an international chase, but they are only able to get to a train station before Verona, as Julius had met some other older men at the trains rest stop and Lauren and Daniel are forced to abandon the train.

The adolescents and Julius hitch a ride with a married couple (Andrew Duncan and Claudette Sutherland) who are also going to Verona and then to Venice. The two separate parties go out to dinner together, and the man of the married couple, Bob Duryea, is surprised to find that his wallet has been stolen. Julius, Lauren, or Daniel supposedly have no money either, as their money from the horse race was left on the train in Juliuss vest. Surprisingly, Julius produces the correct amount of money to buy dinner, not sparking suspicion in the married couple, but causing confusion between Lauren and Daniel.

The following morning, the married couple are eating breakfast when Bob Duryea notices Laurens picture in the newspaper. Janet, his wife, says that the headline above Laurens picture says "Dove". Julius, who had been in his hotel room and after discovering that Lauren and Daniel were sightseeing, gets his own look at the newspaper. He quickly finds Lauren and Daniel, who have seen Juliets balcony. He had been under the impression that Laurens mother was ill and that was why they were going to Venice in the first place. He angrily tells them that, because of this, everyone now thinks that he is a kidnapper. He says that, because of his involvement with crime over the years, this will only mean the worst for him. Daniel asks what kind of crime, but Julius refuses to explain. 

The three enter a bike race in order to avoid the police, but Julius soon falls behind due to his age. Lauren finally manages to catch up with Daniel, and tells him that they must go back for Julius. They do so, only to find his bike abandoned near some steps. They climb down the steps and find him collapsed from exhaustion. Daniel asks what kind of crime Julius is involved in, and he answers in French. Daniel discovers that Julius is a pickpocket, although Lauren doesnt want to hear about it. Lauren then reveals her fathers transfer to Houston, Texas and that she will be moving back to the United States permanently. She goes on to say that she wanted to take a gondola to the Bridge of Sighs and kiss Daniel so as they could love each other forever, and Daniel demands to know why Lauren didnt tell him, and she said that she was afraid he would think it was dumb. She goes on by calling Julius out on all his stories, and calls them lies. Julius admits to it, but says that their legend was not a lie. Daniel goes off and says that he is still going to Venice, and invites Lauren, who in turn invites Julius, to go with them.
 Arthur Hill), and George meet Daniels father, Michel (Claude Brosset) who had been quite unaware of Laurens existence, or even that his son had a girlfriend. Richard is invited to travel to Venice to figure out what is happening with Lauren and Daniel, and Kay requests to accompany him, but Richard refuses, reminding her that Lauren could try to contact them. As they are leaving the police station, Richard informs George that his relationship with Kay is inappropriate and that hes not allowed to be around her anymore. Richard explains to George that, as Kays third husband, he stole her away from her second husband, and that even though Kay may have needed excitement in her life, it is enough now, and that he (Richard) loves her and wants to remain married to her.

Julius, Lauren, and Daniel spend the night in a museum before quickly deciding to see a movie. Julius attempts to get them to see one directed by George, but Daniel adamantly refuses, in favor of a picture with Robert Redford, The Sting. Julius buys them tickets before bidding them farewell and promising to meet them by 7:00, in time for the sunset. But, as soon as they go into the theater, Julius promptly turns himself in to police. 

When the movie is over, Lauren and Daniel briefly wonder where Julius is before running to get a gondola. The man who owns the gondola asks for fifteen-thousand lira, but Lauren and Daniel only have twelve. Daniel and Lauren walk away before the man agrees to take them. The man only takes them so far, and the pair is only able to see the bridge. When the man refuses to take them further, Daniel pushes him out of the gondola, and the two push on various planks of wood sticking out of the water to get to the bridge as the clock begins chiming. Meanwhile, Richard arrives at the Venice police station, where Julius has been held and beaten, for he has refused to give them the whereabouts of Lauren. As the clock strikes, Julius finally reveals where Lauren is and what she is doing. Back under the bridge, Lauren and Daniel finally share the sunset kiss they were waiting for.

Back in France, Lauren seems to have a calmer and more loving relationship with her mother. They are just leaving to go to Houston when Lauren notices that Daniel is across the street, waiting to say goodbye to her. Kay expresses concern, and Richard tells her to get into the car, much to her consternation. Richard lets Lauren say goodbye to Daniel, and they share a final kiss, after Daniel calls them different, but that he is glad. Lauren also says a tearful farewell to Julius, who is sitting on a bench nearby. Whereupon she runs back across the street and returns to her car. Daniel runs into the street and goes after the car, while Julius rises from the bench and watches sadly. Lauren can be seen waving from the backseat of the car and, after a frustrated driver pulls in front of him, Daniel jumps up into the air so as he can see Lauren. The camera freezes on Daniels face and zooms in, more than likely providing the picture in Laurens mind as to how she will remember Daniel until they can see each other again.

==Cast==
 
* Laurence Olivier as Julius
* Diane Lane as Lauren King
* Thelonious Bernard as Daniel Michon Arthur Hill as Richard King
* Sally Kellerman as Kay King
* Broderick Crawford as Himself
* David Dukes as George de Marco
* Andrew Duncan as Bob Duryea
* Claudette Sutherland as Janet Duryea
* Graham Fletcher-Cook as Londet
* Ashby Semple as Natalie Woodstein
* Claude Brosset as Michel Michon
* Jacques Maury as Inspector Leclerc
* Anna Massey as Ms Siegel
* Peter Maloney as Martin
* Dominique Lavanant as Mme. Cormier
* Mike Marshall as 1st Assistant Director
* Michel Bardinet as French Ambassador
* David Gabison as French Representative
* Isabel Duby as Monique
* Geoffrey Carey as Make-up Man
* John Pepper as 2nd Assistant Director
* Denise Glaser as Woman Critic
* Jeanne Herviale as Woman in Metro Station
* Carlo Lastricati as Tour Guide
* Judith Mullen as Richards Secretary
* Philippe Brigaud as Theater Manager
* Lucienne Legrand as Theater Cashier
 

==Production==
Filming took place in Paris, France, as well as Verona, Venice and Veneto, Italy.

==Reception==
Following its initial release in 1979, the film received mixed reviews, with some being quite negative. In his review in The New York Times, Vincent Canby described the film as "so ponderous it seems almost mean spirited. Its been a long time since Ive seen a movie about boorish American tourists and felt sorry for the tourists—which is one of Mr. Hills achievements here. Im sure nothing mean-spirited was intended, but such is the films effect. This may be the main hazard when one sets out to make a film so relentlessly sweet-tempered that it winds up—like Pollyana—alienating everyone not similarly affected." 

In his review in the Chicago Sun-Times, Roger Ebert gave the film only two stars, writing that the film "gives us two movie kids in a story so unlikely I assume it was intended as a fantasy. And it gives us dialog and situations so relentlessly cute we want to squirm."   

Following its release on video and DVD, the film gained stronger critical support. In his review for DVD Movie Guide, David Williams called the film "one of those gems that doesnt seem too great on the surface, but manages to lift your spirits in such a way that when its over, it makes you glad you ignored your initial feelings and checked it out anyway." Williams applauded the performances as "engaging from top-to-bottom", singling out Oliviers portrayal of Julius, the mischievous escort and matchmaker.   

In his review in DVD Movie Guide, John J. Puccio wrote, "Its a lovely tale of pure and innocent love and the lengths that people involved in such a love will go to in their desire to ensure it. The movie can hardly fail to please even the most jaded audiences."   

In his review in DVD Talk, David Langdon concluded, "A Little Romance fits into that category we might call the childrens film for adults. Its smart, well written, acted and directed. If anything it will be remembered as Diane Lanes first movie and one of Laurence Oliviers last. The DVD is above average in all categories except audio but it is worth a look." 

On the aggregate reviewer web site Rotten Tomatoes, the film received an 80% positive rating from top film critics based on 15 reviews, and a 78% positive audience rating based on 4,887 reviews.   

==Accolades==
 

{| class="wikitable" rowspan=6; style="text-align: center; background:#ffffff;"
!Award!!Category!!Recipient!!Result!!Ref.
|- 52nd Academy 52nd Academy Awards Academy Award Best Adapted Screenplay Allan Burns
| 
|rowspan=2|   
|- Academy Award Best Original Song Score Georges Delerue
| 
|- 37th Golden 37th Golden Globe Awards 37th Golden Best Supporting Actor - Motion Picture Laurence Olivier
| 
|rowspan=2|   
|- 37th Golden Best Original Score - Motion Picture Georges Delerue
| 
|- Writers Guild 32nd Writers Guild of America Awards Writers Guild Best Comedy Adapted from Another Medium Allan Burns
| 
|   
|- 1st Youth 1st Young Artist Awards 1st Youth Best Motion Picture Featuring Youth
|A Little Romance
| 
|rowspan=3|   
|- Young Artist Best Juvenile Actor in A Motion Picture Thelonious Bernard
| 
|- Young Artist Best Juvenile Actress in A Motion Picture Diane Lane
| 
|}

==Related Media== Tamil as Panneer Pushpangal in 1981 by P. Vasu and Santhana Barathy.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 