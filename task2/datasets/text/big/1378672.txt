Ordinary Decent Criminal
 
{{Infobox film
| name           = Ordinary Decent Criminal
| image          = Ordinary Decent Criminal_DVD.JPG
| caption        = DVD case cover for Ordinary Decent Criminal
| alt            =
| director       = Thaddeus OSullivan
| producer       = Jonathan Cavendish
| writer         = Gerard Stembridge
| narrator       =
| starring       = Kevin Spacey Linda Fiorentino Peter Mullan Stephen Dillane Helen Baxendale David Hayman Patrick Malahide Colin Farrell Christoph Waltz Vincent Regan
| music          = Damon Albarn
| cinematography = Andrew Dunn
| editing        = William M. Anderson
| distributor    =
| released       =  
| runtime        = 93 minutes
| country        = Ireland
| language       = English
| budget         =
}}
 directed by Thaddeus OSullivan, written by Gerard Stembridge and starring Kevin Spacey and Linda Fiorentino. The film is loosely based on the story of Martin Cahill, a famous Irish crime boss.    

Filmed in late 1998 and originally scheduled for a fall 1999 release, the movie was put out overseas first the following year but it never got a proper theatrical release in the United States, where it was quietly dumped straight to video in January 2003, almost five years after filming began.

== Plot ==

Michael Lynch is one of Dublins most notorious criminals. He has two wives, Christine and Lisa (they are also sisters), as well as many children. When he is not spending time with his family, he is plotting heists with his gang. His actions make him an iconic figure, and he has a rapport with the general public despite being a criminal.

During his elaborate heists, he concentrates on the showmanship as much as the crime itself. He pulls off a daring art theft, stealing several priceless paintings from Dublins best art gallery, giving the  authorities the slip. The Garda Síochána|Gardaí become more determined to catch him as time goes on, in particular Noel Quigley, an officer whose ambition to catch Lynch becomes an obsession. His actions also gain the ire of the Provisional Irish Republican Army|IRA.
 Caravaggio painting, giving Quigley the opportunity he was waiting for to try and catch him. Lynch is forced to go on the run, with his popularity with the public at stake.

== Cast ==
* Kevin Spacey as  Michael Lynch
* Linda Fiorentino as  Christine Lynch
* Peter Mullan as  Stevie
* Stephen Dillane as  Noel Quigley
* Helen Baxendale as  Lisa Lynch
* David Hayman as  Tony Brady
* Patrick Malahide as  Commissioner Daly
* Gerard McSorley as  Harrison David Kelly as  Father Grogan
* Gary Lydon as  Tom Rooney
* Paul Ronan as  Billy Lynch
* Colin Farrell as  Alec
* Vincent Regan as  Shay Kirby
* Tim Loane as  Jerome Higgins
* Christoph Waltz as  Peter
* Enda Oates as Brian
* David Brady as Declan Brady
* Jeffrey Connon as Paul OKeeffe

== Reception ==
 
  
The film received negative reviews from critics. Rotten Tomatoes rates the film 13% based on 8 reviews.  .   The rating of the film on IMDb is 6.4/10 from 9,880 users.    
 
 

== Music ==
 

=== Soundtrack ===
# "One Day At A Time" - Damon Albarn And Robert Del Naja
# "Kevin On A Motorbike" - Damon Albarn
# "Superfinger" - Lowfinger
# "Mother Of Pearl" - Bryan Ferry
# "I Want You" - Shack
# "Gopher Mambo" - Yma Sumac
# "Chase After Gallery" - Damon Albarn
# "Eurodisco" - Bis
# "Bank Job" - Damon Albarn
# "Dying Isnt Easy" - Damon Albarn

== Home media ==
  
Ordinary Decent Criminal was released on DVD January 31, 2003. 

== See also ==
* 2000 in film
* Ordinary Decent Criminal (slang)
* The General (1998 film)

== References ==
 

 

 
 
 
 
 
 
 
 
 
 