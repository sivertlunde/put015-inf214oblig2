Ghost Brigade
 
{{Infobox film
|  name           = Ghost Brigade
|  caption        = A poster with one of the films alternate titles: Grey Knight
| image           = Ghost Brigade FilmPoster.jpeg
|  director       = George Hickenlooper
|  producer       = {{plainlist|
* Steven Stabler
* Brad Krevoy
}}
|  writer         = Matt Greenberg
|  starring       = {{plainlist|
* Martin Sheen
* Corbin Bernsen
* Adrian Pasdar
}}
|  cinematography = Kent L. Wakeford
|  music          = 
|  editing        = {{plainlist|
* Monte Hellman
* Esther P. Russell
}}
|  distributor    = Motion Picture Corporation of America
|  released       =  
|  runtime        = 80 minutes
|  country        = United States
|  language       = English
}} supernatural horror film set during American Civil War and directed by George Hickenlooper. Starring Corbin Bernsen, Adrian Pasdar, and Martin Sheen, the film was also released under the alternate titles The Killing Box and Grey Knight. 

==Synopsis== Confederate Army Union and Confederate soldiers are mysteriously murdered by the entity during the Civil War, the opposing troops must overcome their differences and band together to investigate the gruesome deaths. It soon becomes apparent that these killers are anything but human. Instead, they are a maniacal regiment of supernatural forces, and it is solely up to these united American soldiers to fight the slaughterous evil of the Ghost Brigade.

The film centers around Captain John Harling, a Union army man that has been awaiting the end of his parole; it is cut short when there are unusual murders of Union troops. To discover the culprits behind this Harling seeks Colonel Nehemiah Strayn, a former Confederate regiment commander, now sitting in a Union prison at Bowling Green, who was once Harlings teacher. The two officers, along with Colonel George Thalman supervising, set out with a small detachment of troops and a runaway slave named Rebecca. When they reach the site where several Union troops were murdered, Strayn tells about what happened to his regiment, whose gruesome end happened at the same place where the Union corpses were found.

After leaving the site and wandering deeper into Tennessee, they come across a group of Confederate soldiers, who quickly surrender, much to the surprise of the Union troops. The Confederate in charge tells Thalman that their group was attacked by a band of undead troops who took many of his men away, yet he was unaware of why he and his people were taken. Thalman, deciding that they require reinforcements, takes off leaving Harling in charge. But during the night, while Strayn and Rebecca are talking just a few yards away from the camp, they are assaulted by Stayns old regiment, now a bunch of walking zombies. His former second-in-command, Major Josiah Elkins, tells him to lead this new regiment. Strayn refuses the offer, however, and escapes with Rebecca to the camp. Strayn warns everyone at the camp and they all hide behind the carts that the Confederates had with them and wait until the zombie soldiers approach them. After holding them off, they are surprised to see Thalman reappear again, but he has been changed. After being burned by Rebecca he returns to his former self, But before he dies, he puts Harling in charge, in hopes that Harling will see the men out.

The next day, since they dont have time to leave the area, they set a trap for the Ghost Brigade. Using Strayn to lure them back to the Union camp, he surprises them by picking up a pistol and killing one of them with a silver bullet which was created from the silver that the Confederates were carrying. The Ghost Brigade goes into a fury and attacks the joint Union/Confederate troops, who are positioned behind a trench filled with water and wagons turned into barricades. Strayn personally fights with Elkins in hand-to-hand combat before Elkins stabs him. Before he can finish Strayn, Rebecca leaps on top of him. The short struggle continues until Rebecca shoots Elkins by aiming at her chest when he was behind her. While barely alive, Harling realizes that she is infected and has to be shot. Strayn tries to intervene but Rebecca is shot. The last remaining zombie soldier cries over Elkins corpse before he is shot by Harling. With the Ghost Brigade defeated, they head back for Union territory, where Strayn is sent back to Bowling Green prison for a short period, before he escapes and rejoins the Confederate Army. He fights with distinction during the battles of Gettysburg and other major Civil War battles, but his unusual stand against slavery alienates many of his countrymen. While Strayn is fighting in the Confederate Army, Harling decides to not take his parole and stays with the Union Army for duration of the war, serving under General U.S. Grant for the remainder of the Civil War.

The film ends with Harling telling the story of Strayn going back to the creek where his regiment was murdered, where he is never seen again.

==Cast==
* Corbin Bernsen as Colonel Nehemiah Strayn
* Martin Sheen as General Haworth
* Billy Bob Thornton as Langston
* Adrian Pasdar as Captain John Harling
* David Arquette as Murphy
* Alexis Arquette as Corporal Dawson
* Jon Beatty as Lieutenant
* Dean Cameron as Borne Josh Evans as Lt. Regis Allison A.J. Langer as Thomas, the drummer boy
* Matt LeBlanc as Terhune
* Jefferson Mays as Martin Bradley
* Oliver Muirhead as Richard Bradley
* Steve Price as Captain Donnelly
* Mark Salzman as Corporal
* Cynda Williams as Rebecca Roger Wilson as Major Josiah Elkins
* Ray Wise as Colonel George Thalman

==Release==
Turner released Ghost Brigade on home video in May 1995. 

==Reception== Time Out London called it "an arty zombie film" that is "involving, with dark undertones".   Writing in The Zombie Movie Encyclopedia, academic Peter Dendle said, "The movie features strong conceptual underpinnings, though its a little dragged down by the tritely over-demonized foes."   

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 