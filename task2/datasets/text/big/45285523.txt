Shrew's Nest
{{Infobox film
| name           = Shrews Nest
| image          = Musarañas_poster.png
| alt            = 
| caption        = 
| film name      = Musarañas
| directors      = Juan Fernando Andrés Esteban Roel
| producer       = Álex de la Iglesia
| writers        = Juan Fernando Andrés Sofía Cuenca
| screenplay     = 
| story          = 
| based on       = 
| starring       = Macarena Gómez Nadia de Santiago Hugo Silva Luis Tosar
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| production companies = Nadie es Perfecto Pokeepsie Films
| distributor    = 
| released       =   
| runtime        = 
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}}
Shrews Nest ( , "Shrews", renamed Sangre de mi sangre - "My own flesh and blood" - in Mexico)  is a Spanish 2014 thriller/horror film directed by Juan Fernando Andrés and Esteban Roel, and starring Macarena Gómez, Nadia de Santiago, Hugo Silva and Luis Tosar. It was nominated for three awards at the 29th Goya Awards ceremony.

==Plot==
The film is set in the 1950s. Montse (Macarena Gómez) has lost her youth taking care of younger sister Nia (Nadia de Santiago), both locked in a dark apartment in the center of Madrid. Their mother died during Nias birth and their father (Luis Tosar) ran away, unable to handle the situation. And so, forced to act as father, mother and older sister, Montse hides from reality, feeding an obsessive, unhinged temperament. She suffers from agoraphobia and her only link to reality is Nia. That link breaks when Carlos (Hugo Silva), a neighbor of them, falls off the stairs and looks for help knocking on the only door he can drag himself towards. Someone has entered the shrews nest, and might not come out again.

==Critical reception==
Shrews Nest has received generally positive reviews. Jordi Costa wrote for El País, "one could criticize Shrews Nest for some underlinings and missteps, but it is an energic debut film, capable of modulating with a good pulse the escalade towards its final excesses."  Jonathan Holland of The Hollywood Reporter, while dismissing Hugo Silvas performance as "literally and metaphorically flat", claims that the film is an "enjoyable if unsubtle historical horror featuring a deliciously over-the-top central performance by Macarena Gómez." 

On the negative side, Daniel Lobato wrote for eCartelera: "Explaining the reasons that make a failed proposal out of Shrews Nest would require entering spoiler territory (...), so I will just say that the concord of credibility between viewer and author (...) breaks irreparably as soon as danger raises its head", and also criticizes Silvas performance, claiming that he "should at least have seen The Sea Inside before laying in bed." 

==Awards and nominations==
{| class=wikitable
! Awards !! Category !! Nominated !! Result
|-
| rowspan=3 | 29th Goya Awards 
| Best Actress
| Macarena Gómez
|  
|-
| Best New Director
| Juanfer Andrés & Esteban Roel
|  
|-
| Best Makeup and Hairstyles
| José Quetglas & Carmen Veinat
|  
|}

==References==
 