So Dark the Night
{{Infobox film
| name           = So Dark the Night
| image          = SoDarkNightPoster.jpg
| image_size     =
| caption        = Theatrical release  poster
| director       = Joseph H. Lewis
| producer       = Ted Richmond
| screenplay     = Dwight V. Babcock Martin Berkeley
| story          = Aubrey Wisberg
| narrator       =
| starring       = Steven Geray Micheline Cheirel
| music          = Hugo Friedhofer
| cinematography = Burnett Guffey
| editing        = Jerome Thoms
| distributor    = Columbia Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
So Dark the Night is a 1946 American crime film noir directed by Joseph H. Lewis and written by Dwight V. Babcock, Martin Berkeley, based on a story written by Aubrey Wisberg.  The drama features Steven Geray, Micheline Cheirel, Eugene Borden, among others. 

==Plot==
So Dark the Night is the story of a detective, Henri Cassin from Paris, who falls in love with an innkeepers daughter Nanette while on a long overdue vacation.  She is a country girl with a jealous boyfriend. Nonetheless, the detective becomes engaged to her. Then the girl vanishes the night of her engagement party and later shows up dead.  Cassin believes that the obvious suspect is Leon, the old boyfriend, but soon he is also found killed.

==Cast==
* Steven Geray as Henri Cassin
* Micheline Cheirel as Nanette Michaud
* Eugene Borden as Pierre Michaud
* Ann Codee as Mama Michaud
* Egon Brecher as Dr. Boncourt
* Helen Freeman as Widow Bridelle

==Critical reception==
Critic Karl Williams called the film, "  well-plotted and executed film noir suffered from its lack of star power, but has become something of a cult classic." 

The staff at Variety (magazine)|Variety magazine gave the film a positive review, writing, "Around the frail structure of a story   about a schizophrenic Paris police inspector who becomes an insane killer at night, a tight combination of direction, camerawork and musical scoring produce a series of isolated visual effects that are subtle and moving to an unusual degree." 
 Freudian story is wacky and strains credibility, but the elegant style Lewis uses is mesmerizing. The film noirs light touches are magnificently caught in the rich depiction of rural life and the character study of a psychological breakdown due to a pressured psyche that induces schizophrenia. This makes for a fascinating watch. So Dark the Night is a rarely shown obscure film, and it is a beauty. Burnett Guffey used his camera effectively in many strange angled shots while his dark black shadings express the contrasting somber mood to the airy country landscape." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 