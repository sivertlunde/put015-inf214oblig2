Green Fire
 
{{Infobox film
| name           = Green Fire
| image          = Green Fire VideoCover.jpeg
| caption        =
| director       = Andrew Marton
| producer       = Armand Deutsch Ben Roberts Paul Douglas John Ericson Murvyn Vye José Torvay Robert TafurAshley rios
| music          = Miklós Rózsa
| cinematography = Paul Vogel
| editing        = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 100 min.
| country        = United States
| awards         = English
| Center for Motion Picture Study, Los Angeles 
| gross          = $4,460,000 
| Book Author    = Peter William Rainier (1890-1946). He is the Great, Great, Great Grandson of the British Admiral, Peter Rainier, Jr., for whom Mt. Rainier, Washington, was named by Vancouver.
}}
 movie directed Paul Douglas John Ericson.

==Plot==
Rugged mining engineer Rian Mitchell (Stewart Granger) discovers a lost emerald mine in the highlands of Colombia, which had last been operated by the Spanish conquistadors. Rian is a man consumed by the quest for wealth. However, he has to contend with local bandits and a savage jaguar.
 John Ericson), Rian manages to charm Catherine.
 Paul Douglas), is preparing to leave Colombia on the next ship. Rian, anxious to get Vics assistance to mine the emeralds, tricks him into staying. Returning to the mine, Rian first gets Catherines cooperation and then resumes his romantic overtures.

However, his greed to get the emeralds at any cost soon creates trouble. He comes into conflict with the chief of the local bandits, who threatens Catherine at her home. He also takes Donald into the mining operation, despite Donalds complete inexperience, solely in order to obtain the coffee plantation workers on for his mining needs.  This, however, means that Catherine does not have enough workers available to pick the coffee when harvest time arrives.  Rians mining operations also put the plantation at risk of flooding.

When a tragic accident at the mine site kills Donald, even Vic abandons his old friend Rian and sets out to help Catherine with her harvest, all the while harboring his own passion for the beautiful young woman.

It takes a final shootout between the bandits and Rians men, in which Catherine and Vic do support him, for Rian to finally come to his senses and realize his mistakes. At great risk to himself, he sets in place an explosion of dynamite that not only diverts the water away from Catherines plantation, but also buries the mine under tons of rubble, from where it can no longer be reached. Rian then reunites with a forgiving Catherine.

==Cast==
* Stewart Granger as Rian X. Mitchell
* Grace Kelly as Catherine Knowland Paul Douglas as Vic Leonard
* John Ericson as Donald Knowland
* Murvyn Vye as El Moro
* José Torvay as Manuel (as Jose Torvay)
* Robert Tafur as Father Ripero
* Joe Dominguez as Jose
* Nacho Galindo as Officer Perez
* Charlita as Dolores
* Natividad Vacío as Hernandez
* Rico Alaniz as Antonio Paul Marion as Roberto
* Bobby Dominguez as Juan

==Production== Universal and Paramount Pictures|Paramount.

The mining and plantation scenes were shot on location in rural Colombia.  The cast and crew ostensibly endured many weeks of miserable weather to give the film its very realistic look.

The author of the novel Green Fire, on which the film was based, was Major Peter William Rainier 1890-1946, a South African whose great-great-grand-uncle was the person that Mount Rainier, Washington was named after (by the explorer George Vancouver). 

==Reception==
According to MGM records the film earned $1,829,000 in the US and Canada and $2,631,000 elsewhere, resulting in a profit of $834,000. 

In France, the film recorded admissions of 2,048,836.   at Box Office Story 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 