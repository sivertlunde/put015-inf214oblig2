Cree Hunters of Mistassini
{{Infobox film
| name           = Cree Hunters of Mistassini
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Boyce Richardson Tony Ianzelo Colin Low
| writer         = Boyce Richardson
| narrator       = Boyce Richardson
| starring       = 
| music          = 
| cinematography = Tony Ianzelo
| editing        = Ginny Stikeman
| studio         = National Film Board of Canada
| distributor    = 
| released       = 1974
| runtime        = 57 min 53 s 
| country        = Canada 
| language       = English
| budget         = $95,602    
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Mistassini region of Quebec, as they set up a winter hunting camp near James Bay and Ungava Bay. The film explores the beliefs and the ecological principles of the Cree people.

Richardson had previously written a series of articles for the Montreal Star on Native rights and the environmental damage done by development on their land. He traveled to Mistassini to speak with Cree friends, pledging that their film would allow Native people to tell their own stories, and filming went ahead with three hunting families in the bush, over five months from 1972 to 1973. 

Produced by the National Film Board of Canada Cree Hunters of Mistassini received the award for Best Documentary over 30 minutes at the Canadian Film Awards as well as the Robert Flaherty Award for best one-off documentary from the British Academy of Film and Television Arts.   

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 