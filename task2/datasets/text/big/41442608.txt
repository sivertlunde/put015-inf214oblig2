Samundi
{{Infobox film
| name           = Samundi
| image          = 
| caption        =  Manoj Kumar
| producer       = Mogannadarajan V. Shanmugam
| writer         = Manojkumar Sivaram Gandhi  (dialogues) 
| starring       =   Deva
| cinematography = Utpal
| editing        = Napoleon
| studio         = Sri Raja Kaliamman Enterprises
| distributor    = 
| released       =  
| country        = India
| runtime        = 140 min
| language       = Tamil
}}
 1992 Tamil Tamil action Manoj Kumar. Kanaka in Deva and was released on 18 September 1992. 

==Plot==
 Mansoor Ali Uday Prakash) spread terror among the villagers. One day, the ringing of temple bells and the heralding of the festival throws him into an uncontrollable fit of rage. Ponnuthayi asked the reason of his rage and Samundi tells his tragic past. Samundi prepared his sister Rasathis wedding and he took her to the temple. There, he clashed with some thugs. In the confrontation, Rasathi was pushed in the lake and drowned. The wedding was cancelled and Samundis family left his village.

Samundi cannot tolerate anymore the oppression of Rajangams henchmen so he beats them. The villagers acclaim Samundi and Rajangam feels ridiculed. At Samundis wedding, a police officer, who is under Rajangams order, arrests the innocent Samundi. Samundi is released after the villagers pressure. The angry Samundi runs with a machete in Rajangams house and finds his sister Rasathi alive and in the widows clothes. What transpires later forms the crux of the story.

==Cast==
 
 
*R. Sarathkumar as Samundi Kanaka as Ponnuthayi
*Meera as Rasathi
*Sangita as Lakshmi
*Goundamani Mansoor Ali Khan as Rajangam Uday Prakash
*Rocky
*S. N. Vasanth
*Sethu Vinayagam
*Ravishankar
*Varalakshmi as Samundis mother Ponnambalam
 
*Mohana Priya
*Magima
*Sharmili
*Premi
*Shanthi
*Prathiba
*Asha Priya
*Swamikannu as Ponnuthayis father
*Joker Thulasi
*Narayanan
*Thidir Kannaiah
*Singamuthu
*King Kong
*Theni Panneerselvam
 

==Production==
The films shooting was held in Gobichettipalayam, a small town near Coimbatore, which became famous after the tremendious success of Chinna Thambi. 

==Soundtrack==
{{Infobox Album |  
| Name        = Samundi
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1992
| Recorded    = 1992 Feature film soundtrack
| Length      = 24:22 Deva
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1992, features 5 tracks, lyrics written by Vaali (poet)|Vaali. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Ethungadi || K. S. Chithra || 4:46
|- 2 || Kannula Paalai || S. P. Balasubrahmanyam, K. S. Chithra ||  5:09
|- 3 || Kummanum || S. Janaki || 4:50
|- 4 || Mannai Thottu || S. P. Balasubrahmanyam || 4:13
|- 5 || Muthu Nagai || S. P. Balasubrahmanyam, S. Janaki || 5:24
|}

==Reception==
Malini Mannath of The New Indian Express said : "The film offers nothing new in terms of entertainment nor can it keep viewers glued to their seats for long". Despite negative reviews, the film completed a 100-day run at the box-office.  

==References==
 

 
 
 
 
 