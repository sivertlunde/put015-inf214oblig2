Nuvva Nena
 
 
{{Infobox film
| name = Nuvva Nena
| image = Nuvva nena poster.jpg
| image_size =
| caption = Film poster
| director = P. Narayana
| producer = Vamsikrishna Srinivas
| writer = Surya  (Story & Dialogues)  Marudhuri Raja  (Dialogues)  Mohan  (Dialogues) 
| screenplay = Surya
| narrator =
| starring = Shriya Saran Allari Naresh Sharvanand Vimala Raman
| music = Bheems Ceciroleo  Mani Sharma  (Film score|BGM) 
| cinematography = Dasarathi Shivendra
| editing = Marthand K. Venkatesh
| distributor = S.V.K Cinema  BlueSky Cinemas 
| studio = S.V.K Cinema
| released =   }}
| runtime = 127 minutes
| country = India
| language = Telugu
| budget =   
| gross =
}}
 background score for the film. It is based on Hindi film Deewana Mastana. The film was a disaster at the box office. 
==Plot==
Avinash (Allari Naresh) is a small time crook who creates havoc in the town of Amalapuram along with his friend Ali (Ali (actor)|Ali). After a big robbery, they escape to Hyderabad along with 60 lakhs in cash. Unexpectedly, Avinash comes across the gorgeous Dr. Nandini (Shriya Saran), and it is love at first sight for him. He sets about trying to get closer to Nandini and starts hatching plans to that effect. Into this scenario enters Anand (Sharvanand) who is an extremely timid guy. Anand is extremely frightened about very minor things and his father takes him to Dr. Nandini for treatment. As Nandini sets about curing Anand’s problem, he too falls for her.

Friction develops between Avinash and Anand as they set about hatching plans to destroy each other’s image in front of her. Unexpected competition comes in the form of local Don Aaku Bhai (Brahmanandam). After much drama, it is revealed to Anand and Avinash that Nandini is already in love with Raja (Raja Abel|Raja) the whole time when she invites them to their marriage. Although heartbroken, Avinash and Anand both move on in life after deciding to forget Nandini.

==Cast==
* Shriya Saran as Nandhini
* Allari Naresh as Avinash
* Sharvanand as Anand
* Brahmanandam as Aaku Bhai Ali as Chanti Raja in a cameo appearance
* Vimala Raman in a cameo appearance Jeeva as Inspector
* Kovai Sarala as Pushpa
* Srinivasa Reddy as Hotel waiter
* Raghu as Aaku Bhais henchman
* Raja Abel in a guest appearance
* Fish Venkat as Aaku Bhais henchman
* Narsing Yadav as Narsing Yadav
* Prabhas Sreenu as Thopu Seenu

==Production==
Nuvva Nena was the second film which had Allari Naresh and Sharvanand together after Gamyam (2008).    The film is a remake of 1997 Deewana Mastana which had Govinda (actor)|Govinda, Anil Kapoor and Juhi Chawla in the lead. 

==Release==
The film was released on 16 March 2012. 

==Soundtrack== Krishna Chaitanya and remaining three songs were written by Bheems Ceciroleo, Anantha Sreeram, Srimani.

{{Infobox album 
| Name     = Nuvva Nena
| Caption  = Album cover
| Artist   = Bheems Ceciroleo
| Type     = Soundtrack
| Cover    =
| Released =  
| Recorded = 2012
| Genre    = Film soundtrack
| Length   =  
| Label    = Aditya Music
| Producer = Bheems Ceciroleo
| Last album = Unknown
| This album = Nuvva Nena   (2012)
| Next album = Unknown
}}

{{tracklist
| headline       = Tracklist
| extra_column   = Singer(s)
| total_length   = 25:26
| lyrics_credits = yes
| title1  = Blackberry
| lyrics1 = Bheems Ceciroleo
| extra1  = Kailash Kher
| length1 = 3:51
| title2  = Ayomayam
| lyrics2 = Krishna Chaitanya
| extra2  = Ranjith (singer)|Ranjith, Suchitra
| length2 = 4:14
| title3  = Tha Tha Thamara
| lyrics3 = Anantha Sriram Sriram Chandra
| length3 = 5:38
| title4  = Oy Pilla
| lyrics4 = Srimani Karunya
| length4 = 3:47
| title5  = Polavaram
| lyrics5 = Krishna Chaitanya
| extra5  = Naveen, Kalpana
| length5 = 4:23
| title6  = Neeli Neeli
| lyrics6 = Krishna Chaitanya
| extra6  = Haricharan
| length6 = 4:13
}}

==References==
 

== External links ==
*  


 
 
 
 