No. 1 of the Secret Service
{{Infobox film
 | name = No. 1 of the Secret Service
 | image = No1sspos.jpg 
 | caption = Original US film poster
 | director = Lindsay Shonteff
 | producer = Elizabeth  Gray
 | writer = Lindsay Shonteff (as Howard Craig) 
 | starring = Nicky Henson music = Leonard Young main theme  = "Givin it Plenty" composer  Simon Bell performer  Simon Bell
 | cinematography = Ivan Strasberg
 | editing   =
 | distributor = Hemdale Film Corporation
 | released = 1977 
 | runtime = 91 minutes
 | country = United Kingdom English
 }} 1977 imitation James Bond film starring Nicky Henson as British secret agent Charles Bind. It was directed and written by Lindsay Shonteff and produced by his wife Elizabeth Gray. The film had the working title of 008 of the Secret Service.   It was released on VHS under the title Her Majesty’s Top Gun. 

==Plot== licensed to kill.

Loveday accomplishes his deeds through an organisation of mercenaries named K.R.A.S.H. (Killing Rape Arson Slaughter and Hit). Bind takes them on with his pair of .357 Magnum Smith & Wesson Model 66 revolvers and a .50 calibre M2 Browning machine gun for crowds.

==Cast==
*Nicky Henson  ...  No. 1 / Charles Bind
*Richard Todd  ...  Arthur Loveday
*Aimi MacDonald  ...  Anna Hudson
*Geoffrey Keen  ...  Rockwell
*Dudley Sutton  ...  K.R.A.S.H. Leader
*Sue Lloyd  ...  Sister Jane
*Jon Pertwee  ...  The Rev. Walter Braithwaite
*Milton Reid  ...  Eye Patch
*Fiona Curzon  ...  Bar Girl
*Jenny Till  ...  Vampire Girl
*Katya Wyeth  ...  Miss Martin
*Oliver MacGreevy ... Simms

==Aspects of production== 1965 Canadian Licensed to Tom Adams as Charles Vine imitating Sean Connery as James Bond.  With the popularity of the mid-1960s spy movie craze, American producer Joseph E. Levine picked up the film for American and worldwide distribution.  He retitled the film The Second Best Secret Agent in the Whole Wide World and added a new title song sung by Sammy Davis Jr.

The international success of the film led to producer Ward and Tom Adams reprising Charles Vine in two sequels; Where the Bullets Fly (1966) directed by John Gilling and presented by Levine and the 1967 made in Spain film Somebody’s Stolen Our Russian Spy/O.K. Yevtushenko that languished in a vault until a release in the mid 1970s. Shonteff had nothing to do with those films.
 The Spy Who Loved Me,  Shonteff thought he would return to the imitation James Bond field with his own film.  The original title of 008 of the Secret Service was replaced by No. 1 of the Secret Service.
 M type superior Rockwell who was previously played by John Arnatt is now played by Geoffrey Keen who would later make appearances in several Bond films as the Minister of Defence.

With production beginning in October 1976,  a sequel was announced during production entitled An Orchid for No. 1  

The sequel was not released until 1979 under the title Licensed to Love and Kill with Gareth Hunt replacing Nicky Henson who had signed with the Royal Shakespeare Company.

==Soundtrack== Simon Bell wrote and performed the theme song  Givin It Plenty that was also used in the first sequel Licensed to Love and Kill and reused in Tintorera.

==Taglines==
“He sleeps with two .357 combat magnums and both eyes open” 
"Tell the other guy to move over! Im Number One"

==Quotes==
“There’s no zero beside my name; I’m a winner in every game”

==Sequels==
* Licensed to Love and Kill (1979) starring Gareth Hunt Number One Gun (1980) starring Michael Howe

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 