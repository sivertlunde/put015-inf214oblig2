In the Sunlight
{{Infobox film
| name           = In the Sunlight
| image          = In the Sunlight (1915) - 5.jpg
| image_size     = 200px
| caption        = Still from the film with Harry von Meter, David Lythgoe, and Vivian Rich Thomas Ricketts
| producer       =
| writer         = Marc Edmund Jones
| starring       = David Lythgoe Vivian Rich Harry von Meter
| cinematography =
| editing        =
| distributor    = Mutual Film
| released       =  
| runtime        =
| country        = United States English intertitles
}}
 1915 American silent short short drama Thomas Ricketts starring David Lythgoe, Vivian Rich, and Harry von Meter.

==Plot==
As reported in a film magazine,  Dr. Arthur Abbott has a drug addiction and leaves his country practice for a more conspicuous career in the city, and Frank Steed, a physician from the slums, takes his place in the village. Abbott, for getting his wife Helen and their child, who stayed in the country, becomes infatuated with Olga, an adventuress. With his drug habit growing, he loses his prestige and, after he has spent all his money, Olga no longer cares for him. They quarrel and she is stricken with heart disease and dies. Fearing a charge of murdering her, Abbott flees the city, but when his train is wrecked he is reported as having been killed. Meanwhile, the companionship between young Dr. Steed and Helen has ripened into love. On hearing of her husbands death, Helen promises Steed that she will marry him. Months later, on the day before their wedding, Abbott, who has recovered from an injury received in the train wreck and whose morphine addiction has gotten worse, wanders near his home and sees his wife in her lovers arms. Abbott tries to attack Steed, but his heart gives way and he drops dead. After the marriage Steed and Helen leave the village, with its darkened unhappy memories, for the sunshine of the open country.

==Cast==
* David Lythgoe as Doctor Abbott
* Vivian Rich as Helen Abbott
* Harry Von Meter as Frank Stead - City Physician (listed as Harry Van Meter)
* Edith Borella as Olga - a City Vampire Jack Richardson
* Louise Lester

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 


 