Une fille et des fusils
Une fille et des fusils is a French film, directed by Claude Lelouch, released in 1964 in films|1964.

==Synopsis==
Four young people, tired of working life, decide that they can earn money from crime than they can from work. The film follows their training at the first "crime school", as well as their work.

==Details==
* Director: Claude Lelouch
* Music: Pierre Vassiliu
* Production : Films 13
* Length : 104 minutes
* Release date: June 1965

==Starring==
*Jean-Pierre Kalfon : Jean-Pierre
*Amidou : Amidou
*Pierre Barouh : Pierre
*Jacques Portet : Jacques
*Janine Magnan : Martine
*Yane Barry : La tenancière du bistrot
*Betty Beckers : La prostituée

==Details==
*The first success for Claude Lelouch after four attempts led to him attempting to destroy all copies and negatives for his previous films. After its success, Lalouche attempted to release a series, but les Grands Moments failed to find a distributor. 
*Claude Lelouch used some ideas from this film in other films, such as the shooting of bottles in "Le bon et les méchants", or economic catastrophes in "Laventure cest laventure"...

== See also ==
* 
 
 

 
 
 
 

 