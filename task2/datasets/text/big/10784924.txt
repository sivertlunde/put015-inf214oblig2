Circle of Power
{{Infobox Film
| name           = Circle of Power
| image          = Circle of Power bruises.jpg
| image_size     =
| caption        = Executives embracing after participating in "Executive Development Training"
| director       = Bobby Roth
| producer       = Gary Mehlman   Anthony Quinn   Jeffrey White
| writer         = Beth Sullivan (screenplay), Gene Church (book) John Considine,   Walter Olkewicz
| music          = Richard Markowitz
| cinematography = Affonso Beato
| editing        = Gail Yasunaga
| distributor    = Media Home Entertainment, Qui Productions, Televicine International
| released       = 1983
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Circle of Power, also known as Mystique, Brainwash and The Naked Weekend, is a 1983 film, co-produced by Gary Mehlman,  . It stars Yvette Mimieux in her final film performance to date.

==Plot synopsis==
 , as chief executive of "Executive Development Training"]] 
 
 , retrieved 4/20/2007.   Christopher Allport plays Jack Nilsson, a decent all-American young executive. 
 psychological pressure.   As a prerequisite to the training course, participants must sign a waiver giving the company the release to physically and psychologically abuse the individuals in the course.   The participants struggle with their shortcomings, such as obesity and alcoholism.   Another individual is a closet homosexual, and a fourth is a transvestite.  At one point in the film, the obese trainee is forced to eat trash and discarded food in front of the other seminar participants.   Eventually, the seminar executives and their wives lose their inhibitions later on in the "consciousness-raising" coursework. {{cite news 
  | last = Staff
  | first =
  | title = Showtime: Circle of Power
  | work = The Chronicle Telegram, Elyria, Ohio
  | pages =
  | language =
  | publisher =
  | date = June 2, 1984
  | url =
  | accessdate =  }}
 

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|-
| Yvette Mimieux || Bianca Ray
|-
| Christopher Allport || Jack Nilsson
|-
| Cindy Pickett || Lyn Nilsson
|- John Considine || Jordan Carelli
|-
| Walter Olkewicz || Buddy Gordon
|-
| Leo Rossi || Chris Morris
|-
| Carmen Argenziano || Tony Annese
|-
|}

==Reception==

The film won a Dramatic Films Award at the 1982 Sundance Film Festival.  Circle of Power played under the title Mystique at the 1981 Chicago International Film Festival. 

A review in The New York Times described Circle of Power as an "attack on monolithic belief systems," and referred to it as "a worthwhile movie." {{cite news 
  | last = Van Gelder
  | first = Lawrence
  | title = Screen: Corporate Pressure
  | work = The New York Times 
  | pages =
  | language =
  | publisher =
  | date = March 2, 1984
  | url = http://movies2.nytimes.com/mem/movies/review.html?res=9A03E4D91439F931A35750C0A962948260
  | accessdate =  }}
   Allmovie compared the psychological nature of the techniques utilized by Executive Development Training to Erhard Seminars Training, calling them "EST-like excesses."   Roger Ebert gave the film three stars, writing that "...its an entertaining film with serious intentions." {{cite news 
  | last = Ebert
  | first = Roger
  | title = Naked Weekend / Mystique 
  | work = Chicago Sun-Times
  | pages =
  | language =
  | publisher =
  | date = 1983-09-28 
  | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19830928/REVIEWS/309280301/1023
  | accessdate =  }}
   Ebert compared it to events reported in Boston newspapers about a man who died during a   of the film, asking the question:  "Could a major corporation get away with this brainwashing?"   The authors of the book upon which the film was based concluded their preface by stating:  "And please remember as you read -- its true." {{cite book
  | last = Church 
  | first = Gene
  | authorlink =
  |author2=Conrad D. Carnes
  | title = The Pit: A Group Encounter Defiled 
  | publisher = Outerbridge & Lazard, Inc.
  | year = 1972
  | location = New York
  | pages = 161
  | url =
  | doi =
  | isbn =  0-87690-087-2}}
 

==See also==
* List of American films of 1983
* Semi-Tough (1977)

==References==
 

==External links==
;Reviews
*  , Roger Ebert, Chicago Sun-Times
*  , The New York Times
*  , Stomp Tokyo
*  
*  

 

 
 
 
 
 