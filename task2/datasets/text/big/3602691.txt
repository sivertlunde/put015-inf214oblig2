Donald's Ostrich
 
{{Infobox Hollywood cartoon cartoon name=Donalds Ostrich
|series=Donald Duck
|image=Donalds Ostrich.png image size=
|alt=
|caption=Theatrical release poster Jack King
|producer=Walt Disney story artist=Carl Barks voice actor=Elvia Allman Billy Bletcher Clarence Nash Pinto Colvig
|musician=Oliver Wallace
|animator=Jack Hannah Paul Allen Johnny Cannon  layout artist= background artist= Walt Disney Productions
|distributor=RKO Radio Pictures release date=10 December 1937 color process=Technicolor
|runtime=9 minutes
|country=United States
|language=English preceded by=Modern Inventions followed by=Self Self Control (1938)
}} animated short Walt Disney Productions and released to theaters on December 10, 1937 by RKO Radio Pictures. It was the third film in the Donald Duck series of short films, although billed at the time as a Mickey Mouse cartoon. It was the first of the series to be released by RKO. 
 Jack King and features the voices of Clarence Nash as Donald Duck, Pinto Colvig as Hortense the Ostrich (hiccups), and Elvia Allman and Billy Bletcher as radio voices.

==Plot== whistle stop train station and is responsible for loading and unloading luggage. A train passes the station and dumps a large pile of luggage on Donald without stopping. Donald finds that one of the crates contains a live ostrich and tied around the ostrichs neck he finds the following note: "My name is HORTENSE. Please see that I am fed and watered. P.S. I eat  !"

Hortense begins to eat anything she can find at the station starting with the message. She then eats a concertina, a wind-up alarm clock and several balloons. This causes Hortense to have hiccups which Donald tries to cure by scaring her.

Finally Hortense swallows Donalds radio and her body begins to react to what is playing on the radio. Donald realizes Hortense has swallowed the radio and grabs a pair of forceps to pull it out. But when Hortense starts to react to a broadcast car race, Donald is unable to control her. Hortense finally crashes through a door which at last knocks the radio out of her, but also gives Donald the hiccups.

==Hortense==
While Donalds Ostrich was the first and only time that Hortense the Ostrich appeared in animation, she later appeared as a comic book character as Donalds pet. Her first comic appearance was in the first publication of the Donald Duck Annual in 1938. 

==Releases==
* : The Chronological Donald

==References==
 

==External links==
* 
* 
 
 
 
 
 
 

 