Waqt Ki Deewar
{{Infobox film
| name           = Waqt Ki Deewar
| image          = Waqtkideewarfilm.png
| image_size     = 
| border         = 
| alt            = 
| caption        = Promotional Poster
| director       = Ravi Tandon
| producer       = T.C. Dewan
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Sanjeev Kumar Jeetendra Sulakshana Pandit Neetu Singh
| music          = Laxmikant-Pyarelal
| cinematography = K.K. Mahajan	
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Ravi Tandon and produced by T.C. Dewan. It stars Sanjeev Kumar, Jeetendra, Sulakshana Pandit and Neetu Singh in pivotal roles. Released on 23 Junauray 1981.

==Cast==
* Sanjeev Kumar...Vikram
* Jeetendra...	Amar Khan
* Sulakshana Pandit...Priya
* Neetu Singh...Soni
* Preeti Ganguli...Ruby
* Pran (actor)|Pran...Sher Khan
* Kader Khan...Lala Kedarnath
* Amjad Khan...	Thakur Ranvir Dayal Singh
* Deven Verma...Rajpat


==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jawani Ka Guzra Zamana"
| Kishore Kumar, Manna Dey, Asha Bhosle
|-
| 2
| "Khel Tamashe Wali"
| Asha Bhosle
|-
| 3
| "Chal Cinema Dekhne Ko"
| Kishore Kumar, Asha Bhosle
|-
| 4
| "Man Chahe Ladki"
| Kishore Kumar, Asha Bhosle
|-
| 5
| "Aai Yar Teri Yari"
| Mohammed Rafi, Kishore Kumar
|}
==External links==
* 

 
 
 

 