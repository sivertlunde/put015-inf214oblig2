Laxdale Hall
Laxdale 1953 British Sebastian Shaw, with Prunella Scales and Fulton Mackay in early roles. Often titled Scotch on the Rocks, it was adapted from the novel Laxdale Hall by Eric Linklater.
 parliamentary delegation is dispatched to the Scottish Highlands where the residents are protesting at their poor links with the outside world. After a few days amongst them, and initial problems of interaction, the visitors begin to fully appreciate the locals lifestyle

==Cast==
* Ronald Squire as General Matheson
* Kathleen Ryan as Catriona Matheson
* Raymond Huntley as Samuel Pettigrew, M.P. Sebastian Shaw as Hugh Marvell, M.P.
* Fulton Mackay as Andrew Flett
* Jean Colin as Lucy Pettigrew
* Jameson Clark as Roderick McLeod
* Grace Gavin as Mrs. McLeod
* Keith Faulkner as Peter McLeod
* Prunella Scales as Morag McLeod
* Kynaston Reeves as Reverend Ian Macaulay
* Andrew Keir as McKellaig
* Nell Ballantyne as Nurse Connachy
* Roddy McMillan as Willie John Watt
* Rikki Fulton as First Poacher
* Eric Woodburn as Gamlie, leader of the Poachers
* Archie Duncan as Police Sergeant at Kyle of Lochalsh
* Ian MacNaughton as Police Constable

==External links==
* 

 
 
 
 
 


 
 