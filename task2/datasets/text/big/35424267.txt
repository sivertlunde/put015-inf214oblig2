AAA – Sin Límite en el Tiempo
{{Infobox film
| name           = AAA – Sin Límite en el Tiempo
| image          = 
| alt            = 
| caption        = 
| director       = Alberto Rodriguez
| producer       = Fernando de Fuentes   Jose C. Garcia de Letona   Mariana Suárez Molnar
| screenplay     = David Hernández Miranda
| starring       = César Arias Jorge Badillo Manuel Campuzano Cibernético Bruno Coronel Sergio Coto Rolando de Castro
| music          = Leoncio Lara
| studio         = Ánima Estudios
| distributor    = Videociné
| released       =  
| runtime        = 90 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = $557,931   
}}
 animated action dramedy film produced by Ánima Estudios and distributed by Videociné. It is a fictionalization of the lucha libre wrestling team organization, Asistencia Asesoría y Administración, and was released in theaters on January 22, 2010.

==Plot==
In the world of strife, hatred between rough and technicians has always existed, but this hatred has never left the confines of the ring. Overnight, La Parka snatches the championship Black Abyss, the kings resentment resurface the ram and threatens to end the AAA, supported by Chessman and Cyber begin their revenge. With the incredible emergence of a "mysterious" subject, an old enemy of the AAA, dramatically increase the problems, as there was a traitor in the AAA. An abandoned psychiatric, cyborgs murderers, giant dragonflies, legendary warriors and time travel.

The Parka with the help of Octagon, Gronda, Kenzo Suzuki, Mascarita Sagrada, Faby Apache and many more, say the most spectacular ever seen fight is about to begin, but accidentally travel back in time.

==Cast==
*César Arias as Dr. Transistor
*Jorge Badillo as Charly Manson
*Manuel Campuzano as Chessman
*Cibernético as Cibernético
*Bruno Coronel as Yónatan
*Sergio Coto as Abismo Negro
*Rolando de Castro as Octagón
*Cinthya de Pando as Elvira
*Carlos del Campo as Casero
*Elegido as Elegido
*Eduardo Garza as Mascarita Sagrada
*Andrés Gutiérrez as Parka
*Jesus Guzman as Kenzo
*Marina Huerta as Mamá de Abismo
*Alejandro Mayen as Triple Dragón
*Luis Alfonso Mendoza as Robotito
*El Mesías as El Mesías
*Doctor Alfonso Morales as Dr. Morales
*Salvador Reyes as Gronda
*Arturo Rivera as El Rudo Rivera
*Alberto Rodriguez as Locutor 70s
*Anette Ugalde as Faby Apache
*Victor Vallejo as Locutor
*Zorro as himself


==See also==
*Ánima Estudios
*Asistencia Asesoría y Administración

==References==
 

==External links==
* 
* 

 

 
 