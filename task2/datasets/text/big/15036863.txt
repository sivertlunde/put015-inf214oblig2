Bodyguard (1948 film)
{{Infobox film
| name           = Bodyguard
| image          = Bodyguard 1948 Film Poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Richard Fleischer
| producer       = Sid Rogell
| screenplay     = {{plainlist|
* Fred Niblo, Jr.
* Harry Essex
}}
| story          = {{plainlist|
* George W. George
* Robert Altman
}}
| narrator       = 
| starring       = {{plainlist|
* Lawrence Tierney
* Priscilla Lane
}}
| music          = Paul Sawtell
| cinematography = Robert De Grasse
| editing        = Elmo Williams
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Bodyguard is a 1948 American semi-documentary crime film noir directed by Richard Fleischer and written by Fred Niblo Jr.and Harry Essex, based on a story written by George W. George and Robert Altman, who would go on to direct MASH (film)|MASH and other notable films.  The drama features Lawrence Tierney and Priscilla Lane, among others.

==Plot==
Mike Carter (Lawrence Tierney) is forced to resign from the Los Angeles Police Department after breaking some rules, and he becomes a bodyguard to wealthy Eugenia Dyson (Elisabeth Risdon). Before long, Carter has been accused of murder  and is being hunted down by his former fellow officers.

His file clerk and fiancée Doris Brewster (Priscilla Lane) believes in his innocence, and she is instrumental in cornering the actual killer.

==Cast==
* Lawrence Tierney as Michael C. "Mike" Carter
* Priscilla Lane as Doris Brewster
* Phillip Reed as Freddie Dysen
* June Clayworth as Connie Fenton
* Elisabeth Risdon as Gene Dysen Steve Brodie as Fenton Frank Fenton as Lieutenant Borden
* Charles Cane as Capt. Wayne
* Pepe Hern as Pachuco

==Critical reception==
Brian Baxter of The Guardian identified Bodyguard as one of Fleischers early RKO films that has stood the test of time.   Michael Barrett of PopMatters rated it 6/10 and called it "a trim little B film that’s an unpolished gem of late-40s noir". 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 