The Frightened City
{{Infobox film
  | name = The Frightened City
  | image = The Frightened City film poster.jpg
  | image_size =
  | caption = original movie poster
  | director = John Lemont 
  | producer = John Lemont Leigh Vance
  | writer = Leigh Vance
  | starring = Herbert Lom John Gregson Sean Connery Alfred Marks Yvonne Romain 
  | music = Norrie Paramor
  | cinematography =Desmond Dickinson
  | editing =
  | distributor = Allied Artists Pictures Corporation US   Anglo-Amalgamated Film Distributors UK
  | released = 1961
  | runtime = 97 min.
  | country = United Kingdom
  | language = English
  | budget = 
  }}
The Frightened City is a 1961 British film about extortion rackets and gang warfare in the West End of London. It stars Sean Connery, who plays a burglar called Paddy Damion. He is lured into a protection racket by oily mobster Harry Foulcher (Alfred Marks), in order to support his partner in crime (Kenneth Griffith), who is injured in a robbery. However Damion has his wits about him. 

Although Connerys character has a girlfriend in the story, he seduces the beautiful Anya (Yvonne Romain), the mistress of the seedy and sinister crime boss Zhernikov (Herbert Lom). John Gregson plays Detective Inspector Sayers who is the policeman dedicated to tackling organised crime.

The Shadows had a hit single with the main theme.

==Cast==
* Herbert Lom as Waldo Zhernikov
* John Gregson as Detective Inspector Sayers
* Sean Connery as Paddy Damion
* Alfred Marks as Harry Foulcher
* Yvonne Romain as Anya Bergodin
* Olive McFarland as Sadie
* Frederick Piper as Sergeant Bob Ogle John Stone as Hood David Davies as Alf Peters Tom Bowman as Tanky Thomas
* Robert Cawdron as Nero
* George Pastell as Sanchetti
* Patrick Holt as Superintendent Dave Carter
* Martin Wyldeck as Security Officer
* Kenneth Griffith as Wally Smith

==External links==
* 

 

 
 
 
 
 
 
 


 
 