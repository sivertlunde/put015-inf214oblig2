Jet Attack
{{Infobox film
| name           = Jet Attack
| image          =Jet_Attack_(1958)_poster.jpg
| image_size     = Theatrical poster
| director       = Edward L. Cahn 
| producer       = Alex Gordon
| writer         = Orville H. Hampton
| starring       = John Agar
| music          = Ronald Stein
| cinematography = Frederick E. West
| editing        = Robert S. Eisen studio = Catalina Productions AIP
| released       = February 1958
| runtime        = 69 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Jet Attack (also known as Jet Alert and released in the UK as Through Hell to Glory) is a 1958 American aviation war film set in the Korean War, featuring United States Air Force (USAF) aircraft. 

==Plot== George Cisar) plans a rescue of the scientist, whom he believes is still alive and may be undergoing interrogation by Russian intelligence agents working with the North Koreans.

Arnett and Lt. Bill Clairborn (Gregory Walcott) are assigned to go into North Korea and bring back Olmstead. After parachuting behind enemy lines, they meet up with guerrilla leader Capt. Chon (Victor Sen Yung), who takes them to Tanya Nikova (Audrey Totter), a Russian nurse, who has been working as a spy for the guerrillas. Tanya had previously been romantically involved with Arnett, but proves invaluable to the mission. She knows that the scientist may be under care of her boss, Col. Kuban (Robert Carricart), a Russian doctor. After they discover Olmsteads whereabouts and bring him out of the prison camp where he was being treated for a concussion, the group is pursued by North Korean Maj. Wan (Leonard Strong). Tanya is wounded during the escape but manages to drive the Americans to an airfield. She dies, but the two American pilots and the scientist make good their escape in a pair of North Korean Mikoyan-Gurevich MiG-15 jet fighters.

==Cast==
 
* John Agar as Capt. Tom Arnett
* Audrey Totter as Tanya Nikova
* Gregory Walcott as Lt. Bill Clairborn
* James Dobson as Lt. Sandy Wilkerson
* Leonard Strong as Maj. Wan
* Nicky Blair as Radioman Chick Lane
* Victor Sen Yung as Capt. Chon
* Joseph Hamilton  as Dean Olmstead
* Guy Prescott as Maj. Garver George Cisar as Col. Catlett
* Stella Lynn as Muju
* Robert Carricart as Col. Kuban
 

==Production== North American F-86A Sabres from the 196th Fighter Interceptor Squadron stood in for both USAF and North Korean fighters. 

==Reception==
American International Pictures released Jet Attack as a double feature with Suicide Battalion. Like many other films of the period that were set in the Korean War, film historian Michael Paris considered it another of the "... features that had little to say that was new; most simply reprised situations common from earlier films and were a blatant attempt to profit from public interest in the war." 

==References==
Notes
 

Citations
 

Bibliography
 
* Paris, Michael. From the Wright Brothers to Top Gun: Aviation, Nationalism, and Popular Cinema. Manchester, UK: Manchester University Press, 1995. ISBN 978-0-7190-4074-0.
* Pendo, Stephen. Aviation in the Cinema. Lanham, Maryland: Scarecrow Press, 1985. ISBN 0-8-1081-746-2.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 