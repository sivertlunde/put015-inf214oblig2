Black Birds (film)
 Blackbird}}
{{Infobox film
| name           = Black Birds
| image          = Crnepticeposter67.jpg
| alt            =
| caption        =
| film name      = Crne ptice
| director       = Eduard Galić   
| producer       =
| writer         =
| screenplay     =
| story          = Grgo Gamulin   
| based on       =  
| starring       =
| narrator       =
| music          = Anđelko Klobučar 
| cinematography = Mile de Gleria 
| editing        = Boris Tešija 
| studio         =       
| distributor    = Viba Film 
| released       =  
| runtime        = 92 minutes 
| country        = Yugoslavia
| language       =
| budget         =
| gross          =  
}}
Black Birds (Crne ptice) is a 1967 Yugoslavian war drama film directed by Eduard Galić.

The original idea and script were by Grgo Gamulin. Zoran Tadić was the scenario assistant. The original music was composed by Anđelko Klobučar. The cinematography was by Mile de Gleria. The film editing was by Boris Tešija. The production design was by Branko Hundić. 

==Plot== Partisan troops are closing in on the camp, and the prisoners themselves are hatching a plot to save their lives...  

==Cast==
*Voja Mirić
*Fabijan Šovagović
*Ivan Šubić
*Ivo Serdar
*Vanja Drach
*Rade Šerbedžija (uncredited)
*Relja Bašić
*Iva Marjanović
*Ivica Katić
*Ratko Buljan
*Pavle Bogdanović
*Nikola Gec
*Antun Vrdoljak
*Špiro Guberina
*Uglješa Kojadinović

==Background and production==
By the time Eduard Galić set out to direct Black Birds, his debut feature film, he was already an established television director and an author of award-winning documentaries. He developed an interest in a screenplay written by Grgo Gamulin, Galićs former father-in-law, that drew heavily on his personal experiences in World War II. As a long-time leftist, Gamulin spent the entire war imprisoned, and managed to escape from the Lepoglava-Stara Gradiška train on 17 April 1945, shortly before he was due to be executed.  

Galić secured funding for the film and, after negotiations with Jadran Film fell through, he and Fadil Hadžić founded Most, an independent production collective. The arrangements were made to produce Black Birds and Hadžićs Protest (film)|Protest in partnership with Viba Film, a Slovene production company. 

Work on the screenplay created a conflict with Gamulin. While the Gamulins original work was a straightforward action drama, Galić preferred the then highly influential auteur film. Together with Zoran Tadić, the assistant director, and Petar Krelja, Galić developed a new outline of the film in which three central characters were a psalm-quoting Ustasha lieutenant (Šovagović), an engineer tasked with blowing up the bridge (Mirić), and an enigmatic prisoner (Šerbedžija, in his first major film role). Gamulin felt that the new version of the screenplay had nothing to do with his original, and asked for his name to be removed from the credits. In the end, Gamulin was credited for the story, while the screenplay credits were omitted entirely. 

==Reception== Aleksandar Petrovićs The Morning, The Birch Tree.  Galić was disappointed by his films failure, and the only modest consolation for him was an extra fee that he received due to Black Birds being distributed in the Soviet Union. 

The film was virtually forgotten until it was screened at the 2010 Subversive Festival in Zagreb. On this occasion, the Croatian film critic Nenad Polimac described Black Birds as "the most interesting discovery of the festival" and a "sensation", praising its visual qualities and expressiveness.  In 2012, the film was digitally restored by the Slovene Film Archives. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 