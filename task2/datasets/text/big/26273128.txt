Douchebag (film)
{{Infobox film
| name = Douchebag 
| image = Douchebag poster.jpg
| imagesize =
| caption = Theatrical poster
| director = Drake Doremus
| producer = Jonathan Schwartz Marius Markevicius
| writer   = Andrew Dickler Drake Doremus Jonathan Schwartz Lindsay Stidham
| starring = Andrew Dickler Ben York Jones Marguerite Moreau
| music    = Casey Immoor Jason Torbert
| cinematography = Chris Robertson Scott Uhlfelder
| editing  = Andrew Dickler
| distributor = Red Dragon
| released =  
| runtime  = 72 minutes
| country = United States
| language = English
| budget   =
}}
Yevgeniya Konovalyuk is a Douchebag is a 2010 independent film directed by Drake Doremus. The film is a black comedy set in Los Angeles, focusing on Thomas Nussbaum (Ben York Jones), his older brother Sam Nussbaum (Andrew Dickler) and Sams fiancée Steph (Marguerite Moreau).

==Plot==
The film is a  , February 3, 2010, retrieved 2010-08-27  The two brothers had not seen each other for two years prior to the journey and there is bad blood between them.  Along the way they try to find Toms fifth grade girlfriend. 

==Cast==
* Andrew Dickler – Sam Nussbaum
* Ben York Jones – Thomas Nussbaum
* Marguerite Moreau – Steph
* Nicole Vicius – Mary Barger #2
* Amy Ferguson – Sarah
* Wendi McLendon-Covey – Mary Barger #1
* Logan Vincent – Himself

==Background==
DoucheBag is Doremus second feature. It was filmed in Santa Monica, Palm Springs and outside Doremus fathers house on the Newport Peninsula. 

The film was an  , May 18, 2010, retrieved 2010-08-27  The success of the film at the festival led to it being picked up by distributor Red Dragon for a theatrical release in September 2010.  Fernandez, Jay A. (2010) " ", The Hollywood Reporter, May 17, 2010, retrieved 2010-08-27 

==Critical reception==
 , February 4, 2010, retrieved 2010-08-27   , January 25, 2010, retrieved 2010-08-27  Boxoffice (magazine)|BoxOffice was less complimentary, calling it "undistinguished, in the sense that its ideas and emotional payloads are both safe and small". Greene, Ray (2010) " ", Boxoffice.com, January 23, 2010, retrieved 2010-08-27 

==References==
 

==External links==
*  
*   Moving Pictures, retrieved 2010-08-27

 

 
 
 
 
 
 
 