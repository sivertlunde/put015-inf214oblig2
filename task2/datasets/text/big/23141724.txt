The Burning Hills
{{Infobox film
| name           = The Burning Hills
| image          = The Burning Hills.jpg
| image_size     =
| caption        = Original film poster
| director       = Stuart Heisler
| producer       = Richard Whorf
| writer         = Louis LAmour (novel) Irving Wallace 
| starring       = Tab Hunter Natalie Wood Skip Homeier Eduard Franz Claude Akins Earl Holliman
| music          = David Buttolph
| cinematography = Ted D. McCord
| editing        = Clarence Kolster
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States English
| budget         =
| gross          = $1.5 million (US) 
}}
 Western based on a 1956 novel by Louis LAmour. The film features young stars popular with the teenagers of the time such as Tab Hunter and Natalie Wood and has a strong emphasis on the importance of tracking (hunting)|tracking.

==Plot==
When Trace Jordans brother is murdered and several of their horses stolen, Trace sees by the tracks that three men are involved.  One man was wearing Mexican spurs, one walked with a limp, and one  smokes cheroots. Upon arriving in the town of Esperanza, Trace sees a destroyed sheriffs office and discovers the only law in Esperanza is Joe Sutton.  He also discovers that the stolen horses have been rebranded with the Sutton brand, and their riders who match the description of their tracks work for Sutton.  Trace enters Joe Suttons (Ray Teal) ranch and wounds him in a shooting.

The enraged Sutton sends his son Jack (Skip Homeier), his foreman Ben (Claude Akins) and ten ranch hands to track down Trace before he goes to an Army fort to bring law to Esperanza.  Wounded in his escape, Trace is helped by courageous half Mexican woman Maria Colton (Natalie Wood).  Unable to locate the hidden Trace, Joe Sutton enlists a half Indian tracker Jacob Lantz (Eduard Franz).

==Notes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 