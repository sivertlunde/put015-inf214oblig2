That Hagen Girl
{{Infobox film
| name           = That Hagen Girl
| image          = ThatHagenGirl.jpeg
| caption        = Theatrical poster Peter Godfrey
| producer       = Alex Gottlieb Charles Hoffman Novel: Edith Kneipple Roberts
| starring       = Shirley Temple Ronald Reagan Rory Calhoun Lois Maxwell
| cinematography = Karl Freund
| music          = Franz Waxman
| editing        = David Weisbart
| distributor    = Warner Bros.
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
}}
 1947 American Peter Godfrey.  The screenplay by Charles Hoffman was based on the novel by Edith Kneipple Roberts.  The film focuses on small town teenage girl Mary Hagen (Shirley Temple) who gossips believe is the illegitimate daughter of former resident and lawyer Tom Bates (Ronald Reagan). Lois Maxwell received a Golden Globe award for her performance.

==Plot==
Mary Hagen is believed by town gossips to be the illegitimate daughter of Tom Bates, a former resident and lawyer.  She is often treated badly.  Bates moves back into town and begins a friendship with Hagens favorite teacher Julia Kane (Maxwell).  There are hints that Bates is the real father of Hagen, though it is later revealed that she was an orphan adopted by the Hagens.  When the teacher leaves town, she suggests to Bates that he stop playing Hagens father, as it has become clear that he is in love with her.  The movie ends with Bates and Hagen boarding a train, presumably to get married.  

==Cast==
* Shirley Temple as Mary Hagen
* Ronald Reagan as Tom Bates
* Rory Calhoun as Ken Freneau
* Conrad Janis as Dewey Koons
* Lois Maxwell as Julia Kane
* Dorothy Peterson as Minta Hagen 
* Charles Kemper as Jim Hagen 
* Penny Edwards as Christine Delaney 
* Jean Porter as Sharon Bailey  Harry Davenport as Judge A. Merrivale 
* Nella Walker as Molly Freneau 
* Winifred Harris as Selma Delaney 
* Moroni Olsen as Trenton Gateley  Frank Conroy as Dr. Stone 
* Kathryn Card as Miss Grover

==Production== retakes of a scene in which Reagans character rescues Temples from a suicide attempt by jumping into a river during a storm, Reagan collapsed. He was hospitalized in Cedars of Lebanon Hospital with viral pneumonia. 

==Critical reception==
In one scene, Temple attempts suicide. A critic wrote that it was too bad the attempt failed. 

The New York Times thought the script amateurish and of Reagan and Temple wrote, "Ronald Reagan keeps as straight a face as he can while doing what must have struck him as the silliest job of his career    ut it is poor, little put-upon Shirley who looks most ridiculous through it all. She acts with the mopish dejection of a school-child who has just been robbed of a two-scoop ice cream cone." 

The movie was included in the popular 1978 book The Fifty Worst Films of All Time. 

==Awards==
Lois Maxwell earned a Golden Globe Award (Most Promising Newcomer: Female) for her performance in the film.  

==See also==
* Shirley Temple filmography
* Ronald Reagan films

==References==
 

==External links==
 
*  
*  
*  
*   at Rotten Tomatoes

 
 
 
 
 
 
 
 