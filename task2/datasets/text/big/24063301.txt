The Old Swimmin' Hole (1940 film)
{{Infobox film
| name           = The Old Swimmin Hole
| image          =File:Old Swimmin Hole lobby card 4.jpg
| image_size     =
| caption        =Lobby card
| director       = Robert F. McGowan
| producer       = Scott R. Dunlap
| writer         = Gerald Breitigam (story) Dorothy Davenport (writer)
| narrator       =
| starring       = Marcia Mae Jones Jackie Moran
| music          =
| cinematography = Harry Neumann
| editing        = Russell F. Schoengarth
| distributor    = Monogram Pictures
| released       = 21 October 1940
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Old Swimmin Hole is a 1940 American drama film directed by Robert F. McGowan.

== Plot summary ==
Teenager Betty Elliott has decided to take over the business and social affairs of her father Doc Elliott. She thinks her father should marry the widowed mother, Julie Harper, of her boyfriend Chris Harper. Doc has been a real friend and father to Chris, who, under his guidance, has learned to take care of all the sick animals in town, but lack of money keeps the widow from sending Chris on to finish high school and medical training is out of the question. 

Wealthy Grandpa Harper sends his attorney Baker to tell Mrs. Harper that all of Jimmys dreams could be realized if the widow, whom the grandfather dislikes, would give up custody of her son. The lawyer also begins to court Julie and this throws a kink in Bettys plans to see her father and the widow get married.

== Cast ==
*Marcia Mae Jones as Betty Elliott
*Jackie Moran as Chris Carter
*Leatrice Joy as Mrs. Julie Carter
*Charles D. Brown as Doc Elliott
*Theodore von Eltz as Baker, Grandpas Lawyer
*George Cleveland as Grandpa Harper
*Dix Davis as Jimmy

== Soundtrack ==
 

== External links ==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 


 