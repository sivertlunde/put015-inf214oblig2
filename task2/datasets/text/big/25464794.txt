Rakshasudu
{{Infobox film
| name           = Raakshasudu
| image          =
| caption        =
| director       = A. Kodandarami Reddy
| producer       = K. S. Rama Rao
| writer         = Yandamuri Veerendranath  (novel)  Radha Rajendra Rajendra Prasad Rao Gopal Rao
| music          = Ilaiyaraaja
| cinematography = Lok Singh
| editing        =
| studio       = Creative Commercials 
| distributor    =
| released       = October 2, 1986
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}} Telugu film Rajendra Prasad, Suhasini Mani Ratnam, Radha (actress)|Radha, and Rao Gopal Rao in important roles. The films score and soundtrack is composed by maestro Ilaiyaraaja.

==Plot== Rajendra Prasad) fight some bad guys, Pursha gets injured and jumps into a river. He is saved by a school teacher Sumatri (Suhasini Mani Ratnam) who is an orphan. Sumatri is the sister of Vijay. Pursha comes up with a plan to raid important documents to imprison Shailajas (Radha) father, who is JKs rival but in the process, Vijay is killed. How Pursha gets his revenge and meets his mother forms the rest of the story.

==Cast==
* Chiranjeevi
* Nagendra Babu
* Suhasini Mani Ratnam Radha
* Rajendra Prasad
* Rao Gopal Rao

==Soundtrack==
The movie features soundtrack composed by maestro Ilaiyaraaja.
===Track listing===
* "Acha Acha" - S. P. Balasubrahmanyam, S. Janaki
* "Giliga Gili" - S. P. Balasubrahmanyam, S. Janaki
* "Hey Naughty" - S. P. Balasubrahmanyam, S. Janaki
* "Jaya Jaya" - S. Janaki
* "Malli Malli" - S. P. Balasubrahmanyam, S. Janaki
* "Nee Meeda Naaku" - S. P. Balasubrahmanyam, S. Janaki

==External links==
*  

 
 
 
 
 