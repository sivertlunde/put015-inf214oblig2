Blow Me Down!
{{Infobox Hollywood cartoon|
| cartoon_name = Blow me Down!
| series = Popeye the Sailor
| image = 
| caption =
| director = Dave Fleischer
| story_artist = 
| animator = Seymour Kneitel William Henning Roland Crandall (uncredited) musician = Sammy Timberg
| producer = Max Fleischer Adolph Zukor
| distributor = Paramount Pictures
| release_date = September 29, 1933
| color_process = Black-and-white
| runtime = 6 mins
| movie_language   = English
| preceded_by      = I Yam What I Yam
| followed_by      = I Eats My Spinach
}}

Blow me Down! is a Popeye theatrical cartoon short, starring William "Billy" Costello as Popeye and Bonnie Poe as Olive Oyl and Charles Lawrence as J. Wellington Wimpy|Wimpy. It was released in 1933 and was a third cartoon in the Popeye the Sailor series of theatrical cartoons released by Paramount Pictures, lasting through 1957.

==Plot==
Popeye goes to see Olive Oyl, riding on a whale while singing his theme song. In the town, locals give Popeye dirty looks. One local tries to shoot Popeye, but because of Popeyes strength, the bullet hits Popeye on the back of his head, and hits the local who tried to shoot him. The local falls from the roof to the ground. He goes to a store named "Alla Kinda Flowers," where he requests a bouquet for Olive. After a while, a local gives Popeye a toothy while mocking him. To get even, Popeye smacks the locals teeth out, and they crunch together in his mouth.

The scene then cuts to Olive, dancing in a tavern, entertaining everybody. Popeye walks in using the swinging old-style doors. Olive notices Popeye, patiently sitting at a table. Olive dances to Popeye. Popeye gives Olive her "bouquet" (which consists of only one flower) and Olive dances away with a leap. Olives feet get stuck into two spittoons. While Olive struggles to get out of the spittoons, Popeye is laughing. Olive, determined to get even, performs a fancy dance. Afterwards, the people in the tavern applaud to Olives act.

Bluto enters the tavern. He blasts his guns numerous times, forming a cloud. When the cloud clears, everyone is seen to have fled the tavern—all but Popeye. Bluto, noticing Popeye sitting calmly, goes over to him. A poster reading "$5000 REWARD ... BLUTO THE BANDIT" has Blutos picture on it. The two Blutos notice each other. Popeye looks at the poster after Bluto, realizing Bluto is the bandit on the poster.

Bluto shows off by popping a bottle of wine then drinking some of it. Popeye decides to show off, too, by punching the table, sending the wine and a cup into the air. The wine bottle tips, pouring itself into the cup, then landing on the table. Popeye drinks the cup of wine. Bluto then draws a pistol and shoots it at a candle. Upon landing, the pieces of candle form into smaller candles. Challenging Popeye, Popeye eats the pistol. Then, using his mouth as a barrel, he shoots a deck and its columns, collapsing the deck to the floor. Bluto socks Popeye in the face, twisting his neck like a whirlybird. Popeye then punches Bluto, sending him into a wall. Bluto opens a door next to him, and his fellow bandits rush in. Popeye states, "Youll get hurt travelin alone," then eats a can of spinach.

Popeye beats the other bandits to the rhythm of music. He sends one bandit crashing into a mirror, one leaning on a handle of a deck, another onto an antler of an animal trophy, another onto a railing, one onto a supporting roof column, one crashing through a window, and the last on another animal trophy. The trophy bites the bandit on the rear end, while the bandit screams in pain. Popeye continues beating bandits. Bluto works his way to Olives dressing room. Olive, thinking Popeye is at the door, allows Bluto in. Bluto creates chaos while Olive screams for Popeye to help her.

Popeye barges into Olives dressing room, where he sees Olive beating Bluto with a club. Then Bluto sees Popeye and, out of anger, socks Popeye away, afterwards Olive hitting Bluto with the club. The process repeats until Bluto gets tired. Popeye, finding his chance, socks Bluto, thus sending him out of the window. Popeye finds Bluto lying on Olives balcony. When Popeye goes to sock him, Bluto knocks Popeye to a different balcony. Popeye jumps back, and knocks Blutos head several times. Then they both fall to the ground. They keep fighting until Bluto gets tired. Popeye gathers all his muscle and knocks Bluto so hard, it sends him into an orbit around the Earth. When Bluto lands, he falls. Popeye stands on him, singing "Im Popeye the Sailor Man!"

==Notes==
Blow me Down! is the third Popeye cartoon from the Popeye the Sailor (Fleischer) series . This cartoon is available on  . A colorized version exists.

==References==
 

==External links==
*  
*  

 
 
 
 