The Town (1945 film)
{{Infobox film
| name = The Town
| image =
| caption =
| director = Josef von Sternberg Philip Dunne
| writer = Joseph Krumgold
| starring =
| music =
| cinematography = Larry Madison
| editing =
| studio = United States Office of War Information
| released =  
| runtime = 12 minutes
| country = United States
| language = English
| budget =
}}
 Office of War Information in 1945. It was directed by Josef von Sternberg.

==Synopsis==
The Town presents an idealized vision of American life, shown in microcosm by Madison, Indiana. The diversity of the towns ethnic origins is highlighted, noting the Czech, Dutch, German, and Italian communities, some of whom were immigrants or children of immigrants. Schools are shown to be free and open to all, as are libraries and swimming pools. The press is depicted as free. Some people are shown who dont like the current administration in Washington, and dont like the newspapers policy, but the newspaper prints their complaint. Trials are conducted in front of a jury, for all the world to see. The prosecutor serves as the judges opponent in the last election, and even said he was unfit for office, but they work together anyway. Everyone had the right to vote, from the store keeper, to the attorney himself. In a democracy, the only thing that is secret is the ballot.

The film ends by stating that the American people, descended from settlers in the old world, are now going back over the seas to free their homelands.

== See also ==
*List of Allied propaganda films of World War II

== External links ==
*  
*  

 

 
 
 
 
 


 