The Love Tyrant
 
{{Infobox film
  | name     = The Love Tyrant
  | image    = 
  | caption  =  Alfred Rolfe		
  | producer = 
  | writer   = 
  | based on =  Charles Villiers
  | music    = 
  | cinematography = 
  | editing  = 
  | studio = Australian Photo-Play Company
  | distributor = 
  | released = 6 April 1912
  | runtime  = 1,800 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 Alfred Rolfe. It was described as an "Australian drama about the back blocks"  and a "stirring drama full of thrilling incidents".  It was set during the early bushranging days. 

It is a lost film.

==Plot==
The story beings on Christmas Day at Farmer Morrisons home. Morrisons son, William, has been secretly married to Annie, the maid. Morrison wishes his son to marry Dora. When William tells him the truth he kicks out his son and wife.

Time passes and William owns a farm and has a son with Annie. On returning one-night from his work, tired and hungry, he has a dispute with his wife, who threatens to leave. Troubled and worn out he falls a sleep and dreams a dream in which he imagines his wife has left him for another man.

A mail robbery occurs in the district and William and three stockmen are falsely accused. After his trial he returns to find his stock ruined. He runs away to the bush and becomes an outlaw. He is sentenced to death and as he is placed on the saccold he wakes up to find his wife at his side. He becomes reconciled to his father who proved the enemy of his dream. 

==Released==
The movie was selected to open the new Glenferrie Theatre in Melbourne. In Sydney and Adelaide it was screened as Love, the Tyrant.  

==References==
 

==External links==
*  
*  at AustLit
 

 
 
 
 
 
 
 


 