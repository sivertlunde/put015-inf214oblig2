Girl of the Golden West (1942 film)
{{Infobox film
| name = Girl of the Golden West
| image =
| image_size =
| caption = Carl Koch
| producer = Franco Magli Pierre Benoit (novel) Lotte Reiniger Carl Koch
| narrator =
| starring = Michel Simon Isa Pola Rossano Brazzi Valentina Cortese
| music = Mario Nascimbene
| cinematography = Ubaldo Arata
| editing = Eraldo Da Roma
| studio  = Scalera Film
| distributor = Scalera Film
| released = 9 February 1942
| runtime = 85 minutes
| country = Italy
| language = Italian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Carl Koch Pierre Benoit. on location silent and later became famous. 

==Synopsis==
Arianna, an Italian actress, joins a caravan heading into the American West with her companion Diego. Diego is eventually killed by a local villain. Arianna falls in love with the villain without realising that he is the murderer.

==Cast==
* Michel Simon as Butler
* Isa Pola as Arianna
* Rossano Brazzi as William
* Valentina Cortese as Madge
* Renzo Merusi as Diego
* Carlo Duse as un dipendente di Butler
* Vittorio Duse
* Cesare Fantoni
* Oreste Fares
* Nicola Maldacea
* Augusto Marcacci
* Amina Pirani Maggi
* Corrado Racca
* Leni Vecellio
* Carlo Bressan
* Corrado De Cenzo
* Giovanni Onorato
* Cesare Polesello
* Toscano Giuntini

== References ==
 

== Bibliography ==
*Bondanella, Peter. A History of Italian Cinema. Continuum, 2009.

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 