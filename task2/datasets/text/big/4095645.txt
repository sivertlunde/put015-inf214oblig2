The Girl with the Hungry Eyes (1967 film)
 
{{Infobox Film
| name           = The Girl with the Hungry Eyes
| image          = The Girl with the Hungry Eyes, 1967, DVD.jpg
| image_size     = 
| caption        = DVD cover of the 1967 film
| director       = William Rotsler 
| producer       = Harold Lime Harry H. Novak Charlotte Stewart William Rotsler 
| writer         = William Rotsler
| narrator       = 
| starring       = Adele Rein Cathy Crowfoot Pat Barrington Charlotte Stewart William Rotsler 
| music          = 
| cinematography = Dwayne Avery 
| editing        = William Rotsler 
| distributor    = 
| released       = 1967
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Girl with the Hungry Eyes (1967) is a film written and directed by William Rotsler.

==Plot==
Kitty and Tigercat are two lesbians and The Girl with the Hungry Eyes tells the tale of one womans obsession with the other. It features Adele Rein as Kitty, Cathy Crowfoot as Tigercat, an early film appearance by Charlotte Stewart and a dance scene by Pat Barrington.  

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 

 