Fleeting Beauty
 
 
{{Infobox film
| name = Fleeting Beauty
| image = Fleeting-Beauty-Nandita.jpg
| caption = The films poster
| writer = Shuchi Kothari
| starring =   Tom Bailey
| director = Virginia Pitts
| producer =  
| cinematography= Simon Raby
| editing = Eric De Beus
| distributor = Nomadz Unlimited
| released =  
| runtime = 10 minutes
| country = New Zealand
| language = English
| budget =
}}
 Independent short short film directed by Virginia Pitts, a lecturer of Waikato University.    The film stars Nandita Das and Will Wallace in the lead.    The film is about the story of an immigrant Indian woman and her white lover in New Zealand.  The film attempts to explore the colonial history of the Indian subcontinent.   

==Plot==
The story is about an Indian immigrant woman who explores the history of Indian Subcontinent through spice routes on her white lover’s back. She narrates him the story supported by lots of facts and figures with a blend of colonial history and spices. In due course of time she ends up with a delicate portrait on her lover’s back before it gets brushed-off in a few minutes.

==Cast==
* Nandita Das as Seema (Indian woman)
* Will Wallace as Chris (Seemas white lover)

==Production== New Zealand, as they felt it lacked "New Zealand content". 

==Reception== New Zealand International Film Festival in 2004.   

The films screening at the Valladolid International Film Festival helped the producers receive funds from the Film Commission, New Zealand for its post-production work.  

==Awards and nominations==
;49th Valladolid International Film festival
;Nominated
* Golden Spike Award (Non-feature film) 
* Silver Spike Award (Non-feature film) 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 