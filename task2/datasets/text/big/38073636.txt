Parasangada Gendethimma
{{Infobox film
| name           = Parasangada Gendethimma
| image          = 
| caption        = 
| director       = Maruthi Shivram
| producer       = N. Subramani and others
| writer         = Srikrishna Alanahalli
| screenplay     = Maruthi Shivram
| story          = 
| based on       =  
| narrator       = 
| starring       = Lokesh   Reeta Anchan
| music          = Rajan-Nagendra
| cinematography = S. Ramachandra
| editing        = J. Stanly
| studio         = 
| distributor    = Kalakshethra
| released       =  
| runtime        = 139 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
Parasangada Gendethimma ( ) is a 1978 Indian Kannada language comedy-drama film directed by Maruthi Shivram, based on a novel of the same by Srikrishna Alanahalli.  It stars Lokesh in the lead role and the supporting cast features Reeta Anchan, B. R. Jayaram, and Ramakrishna (Kannada actor)|Ramakrishna.
 Third Best Best Actor Best Music Director (Rajan-Nagendra).  The film was remade in Tamil as Rosaappo Ravikkai Kaari. 

== Plot ==
Thimanna (Lokesh) is an innocent tribal young boy who is excited about his wedding. A salesman in village who help villagers by getting their needy items from distant city,
His wedding is arranged with Marakani (Reeta Anchan), a city girl. Marakani wants to bring about a huge change in him and make him a decent city man. Meanwhile due to the differences with her mother-in-law, Marakani manages to separate herself and Thimanna from his mother, and they start living separately.
Later in the process, when an innocent Thimanna invites a teacher (Manu) to his new shed where they live, Marakani gets attracted to him due to his sophisticated city man looks and then the plot revolves around Thimannas point of view of the whole situation.

== Cast ==
* Lokesh as Gendethimma
* B. R. Jayaram
* Reeta Anchan Ramakrishna
* Manu
* Shyamala

== Soundtrack ==
The music for the film was composed by Rajan-Nagendra with lyrics for the soundtrack penned by Doddarange Gowda.

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Notadage Nageya Meeti"
| S. P. Balasubrahmanyam
|-
| 2
| "Thera Eri Ambaradaage"
| S. P. Balasubrahmanyam
|-
| 3
| "Ninna Roopu Edeya Kalaki"
| S. Janaki
|-
| 4
| "Notadage Nageya Meeti" (Sad)
| S. P. Balasubrahmanyam
|}

== References ==
 

 
 
 
 
 
 
 
 


 