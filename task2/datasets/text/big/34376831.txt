Three Stories (film)
 
{{Infobox film
| name           = Three Stories
| image          = 
| caption        = 
| director       = Kira Muratova
| producer       = Igor Tolstunov
| writer         = Sergei Chetvyortkov Renata Litvinova Vera Storozheva
| starring       = Sergey Makovetskiy
| music          = 
| cinematography = Gennadi Karyuk
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Russia Ukraine
| language       = Russian
| budget         = 
}}

Three Stories ( , Transliteration|translit.&nbsp;Tri istorii) is a 1997 Russian-Ukrainian comedy film directed by Kira Muratova. It was entered into the 47th Berlin International Film Festival. Three Stories was also seen in the Italian dub, on Rai 3.

==Cast==
* Sergey Makovetskiy as Tikhomirov
* Leonid Kushnir as Gena
* Jean-Daniel as Venia
* Renata Litvinova as Ofa; Ofelia
* Ivan Okhlobystin as Doctor
* Oleg Tabakov as Old Man
* Liliya Murlykina as Girl

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 