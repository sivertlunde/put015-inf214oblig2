Someone Behind the Door
{{Infobox film
| name           = Someone Behind the Door
| image          = Someone Behind the Door.jpg
| image_size     = 
| alt            = 
| caption        = Film poster
| director       = Nicolas Gessner
| producer       = Raymond Danon
| writer         = Marc Behm Nicolas Gessner Jacques Robert Lorenzo Ventavoli
| narrator       = 
| starring       = Charles Bronson Anthony Perkins Jill Ireland
| music          = Georges Garvarentz
| cinematography = Pierre Lhomme
| editing        = Victoria Mercanton
| studio         = Lira Films
| distributor    = 
| released       = 15 September 1971 (USA premiere)
| runtime        = 97 minutes
| country        = France English
| budget         = 
| gross          =  
| preceded_by    = 
| followed_by    = 
}}

Someone Behind the Door is a French film directed by Nicolas Gessner.  The original French title was Quelquun derrière la porte, and in the UK it was twice retitled as Two Minds For Murder (theatrical title) and Brainkill (VHS title).  The film was shot on location in Folkestone, England. 

==Plot== neurosurgeon hires an amnesiac to murder his wife, believing that the man will have no memory of what he had done, providing him a perfect alibi.

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Charles Bronson || The Stranger
|-
| Anthony Perkins || Laurence Jeffries
|-
| Jill Ireland || Frances Jeffries
|-
| Henri Garcin || Paul Damien
|-
| Adriano Magistretti || Andrew
|-
| Agathe Natanson || Lucy
|}

==Filming==
Almost all of the film was shot at various locations in Folkestone. Key locations include Folkestone Harbour and Beach. 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 

 