Marionettes (film)
{{Infobox film
| name           = Marionettes
| image          = Marionettes (1934).jpg
| image_size     = 250px
| caption        = Theater Poster in the USSR
| director       = Yakov Protazanov Porfiri Podobed
| producer       = 
| writer         = Yakov Protazanov Vladimir Shvejtser
| narrator       = 
| starring       = Sergei Martinson
| music          = Leonid Polovinkin
| cinematography = Pyotr Yermolov
| editing        = 
| distributor    =  Mezhrabpomfilm
| released       = 3 February 1934
| runtime        = 97 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}
 Soviet antifascist film starring Anatoli Ktorov and directed by Yakov Protazanov and Porfiri Podobed.

The film combines the features of comedy, social drama and political pamphlet.

==Cast==
* Anatoli Ktorov - C, the Prince
* Nikolai Radin - D, the Archbishop
* Valentina Tokarskaya - E, The Singing Star, the Princes Fiancee
* Konstantin Zubov -  F, the Fascist
* Sergei Martinson - G, the Barber
* Mikhail Klimov - A, The Prime Minister
* Vladimir Popov - White General
* Leonid Leonidov - The Munitions Manufacturer
* Igor Arkadin - Master of Ceremonies
* Vasili Toporkov - Director of the Marionette Theater
* Pyotr Galadzhev - Scribe
* Mikhail Zharov - Head of Frontier Post

==External links==
* 

 

 
 
 
 
 
 
 
 

 