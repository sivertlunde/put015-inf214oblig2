Let My People Go: The Story of Israel
{{Infobox film
| name           = Let My People Go: The Story of Israel
| image          = 
| caption        = 
| director       = Marshall Flaum
| producer       = Marshall Flaum Janet Voelker
| writer         = Marshall Flaum
| narrator       = Richard Basehart
| starring       = 
| music          = 
| cinematography = 
| editing        = Nicholas Clapp
| distributor    = 
| released       =  
| runtime        = 60 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 Best Documentary Feature in 1965 and won the United Nations Award in 1961 from the British Academy of Film and Television Arts.    Flaum also produced and wrote the documentary.   
 DP camps partition plan that led to the creation of the State of Israel on May 15, 1948, the film ends with men and women planting trees in memory of the six million victims of The Holocaust. 
 Xerox Corporation. 

==Critical reception==
Let My People Go was described by critic Donald Kirkley of The Baltimore Sun as being "one of those rare programs which remind us of the heights to which television can soar when it is at its best.". 

Television critic  , April 9, 1965. Accessed October 9, 2010. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 