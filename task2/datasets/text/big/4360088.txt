Paris, je t'aime
 
{{Infobox film
| name           = Paris, je taime
| image          = ParisJetaimePoster eng.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = see below
| producer       = Emmanuel Benbihy Claudie Ossard
| writer         = Emmanuel Benbihy   see below
| starring       = see below
| music          = Various
| cinematography = Various
| editing        = Various
| studio         = Canal+ Victoires International
| distributor    = La Fabrique de Films   Ascot Elite Entertainment Group   First Look International  
| released       =   
| runtime        = 120 minutes   
| country        = France Lichtenstein Switzerland Germany
| language       = French English Spanish Mandarin Arabic
| budget         = $13 million 
| gross          = $17.5 million 
}}
Paris, je taime ( ; Paris, I love you) is a 2006 anthology film starring an ensemble cast of actors of various nationalities. The two-hour film consists of eighteen short films set in different Arrondissements of Paris|arrondissements. The 22 directors include Gurinder Chadha, Sylvain Chomet, Joel and Ethan Coen, Gérard Depardieu, Wes Craven, Alfonso Cuarón, Nobuhiro Suwa, Alexander Payne, Tom Tykwer, Walter Salles, Yolande Moreau and Gus Van Sant.

==Arrondissements== 15th arrondissement, 11th arrondissement, by Raphaël Nadjari) were not included in the final film because they could not be properly integrated into it.  Each arrondissement is followed by a few images of Paris; these transition sequences were written by Emmanuel Benbihy and directed by Benbihy with Frédéric Auburtin. Including Benbihy, there were 22 directors involved in the finished film.

{|class="wikitable"
|-
! Segment !! Arrondissement !! Director !! Writer !! Description !! Actors
|- 18th || Bruno Podalydès || A man parks his car on a Montmartre street and muses about how the women passing by his car all seem to be "taken". Then a woman passerby faints near his car, and he comes to her aid. || Bruno Podalydès, Florence Muller
|- 5th || Paul Mayeda Berges and Gurinder Chadha || A young man, hanging out with two friends who taunt all women who walk by, strikes up a friendship with a young Muslim woman. || Cyril Descours, Leïla Bekhti
|- 4th || Gus Van Sant || A young male customer finds himself attracted to a young printshop worker and tries to explain that he believes the man to be his soulmate, not realizing that he speaks little French. || Gaspard Ulliel, Elias McConnell, Marianne Faithfull
|- 1st || Coen brothers|Joel Tuileries station becomes involved in the conflict between a young couple after he breaks the cardinal rule of avoiding eye contact with people on the Paris Metro. || Steve Buscemi, Axel Kiener, Julie Bataille
|- 16th ||colspan=2|Walter Salles and Daniela Thomas || A young immigrant woman sings a Spanish lullaby ("Que Linda Manita|Qué Linda Manita") to her baby before leaving it in a daycare. She then takes an extremely long commute to the home of her wealthy employer (whose face is not seen), where she sings the same lullaby to her employers baby. || Catalina Sandino Moreno
|- 13th || Christopher Doyle || Doyle, Rain Kathy Li, and Gabrielle Keng Peralta || A comedy in which a beauty products salesman makes a call on a Chinatown salon run by a woman who proves to be a tough customer. || Barbet Schroeder, Li Xin
|- 12th || Isabel Coixet || Prepared to leave his marriage for a much younger lover, Marie Christine, a man named Sergio instead decides to stay with his wife after she reveals a terminal illness - and he rediscovers the love he once felt for her. || Leonor Watling, Sergio Castellitto, Miranda Richardson, Javier Cámara 
|- 2nd || Nobuhiro Suwa || A mother, grieving over the death of her little boy, is comforted by a magical cowboy. || Juliette Binoche, Willem Dafoe, Martin Combes
|- 7th || Sylvain Chomet || A boy tells how his parents, both mime artists meet in prison and fall in love. || Paul Putner, Yolande Moreau
|- 17th || Alfonso Cuarón continuous shot, almost five minutes long. When the characters walk by a video store, several posters of films by the other directors of Paris, je taime are visible in the window. || Nick Nolte, Ludivine Sagnier
|- 3rd || Olivier Assayas || An American actress procures some exceptionally strong hashish from a dealer whom she gets a crush on. || Maggie Gyllenhaal, Lionel Dray
|- Place des 19th || Oliver Schmitz || A Nigerian man dying from a stab wound in the Place des fêtes asks a female paramedic for a cup of coffee. It is then revealed that he had fallen in love at first sight with her some time previously. By the time she remembers him, and has received the coffee, he has died. || Seydou Boro, Aïssa Maïga
|- 9th || Richard LaGravenese || An aging couple act out a fantasy argument for a prostitute in order to keep the spark in their relationship. || Bob Hoskins, Fanny Ardant
|- Quartier de 8th || Vincenzo Natali || In this dialogueless segment, a young backpacker traveling late at night stumbles across a corpse—and a vampiress feeding on it. Though initially frightened, he soon falls in love with her, and cuts his wrist to attract her with his blood. The wound causes the backpacker to fall down a flight of stairs and strike his head against the pavement in a potentially fatal injury, but the vampiress reappears and feeds him some of her own blood, turning him into a vampire and allowing the two to begin a relationship.   || Elijah Wood, Olga Kurylenko
|- 20th || Wes Craven || While visiting Père Lachaise Cemetery, a young woman breaks up with her fiancé, who then redeems himself with the aid of advice from the ghost of Oscar Wilde. || Emily Mortimer, Rufus Sewell, Alexander Payne
|- Faubourg Saint-Denis 10th || Tom Tykwer || After mistakenly believing that his girlfriend, a struggling actress, has broken up with him, a young blind man reflects on the growth and seeming decline of their relationship. || Natalie Portman, Melchior Beslon
|- 6th || Frédéric Auburtin and Gérard Depardieu || Gena Rowlands || A separated couple meet at a bar for one last drink before the two officially divorce. || Ben Gazzara, Gena Rowlands, Gérard Depardieu
|- 14th ||Alexander letter carrier from Denver, Colorado on her first European holiday, recites in amateur French what she loves about Paris. || Margo Martindale
|}

==Production==
Julio Medem was attached to the project for a long time. He was supposed to direct one of the segments, but this finally fell through because of scheduling conflicts with the filming of Caótica Ana (2007).
 4K in Europe (as opposed to the normal 2K). Encoding the image took about 24 hours per reel (at Laboratoires Éclair). 

As the film is a collection of shorter segments, there were many producers attached to the project.

===Influence===
Following the success of Paris, je taime, a similarly structured film, New York, I Love You, focusing on life in that city, premiered at the 2008 Toronto Film Festival and was released in a limited number of theatres in 2009.

The Cities of Love website states that there are 3 more films in the series to be released. They include Rio, Eu Te Amo, Shanghai, 我爱你 and Jerusalem, I Love You. All 3 films will follow the same style with no fewer than 10 short films using their respective city as the main unifying character. The films are slated for release around 2010-2013.

==Release== Canadian premiere at the Toronto Film Festival on 10 September and its US premiere in Pittsburgh, Pennsylvania on 9 April 2007.  First Look Pictures acquired the North American rights, and the film opened in the United States on 4 May 2007.

==Reception==
Paris, je taime received generally positive reviews, currently holding an 86% "fresh" rating on   gives the film a 66/100 rating, indicating "generally favorable reviews". 

==References==
 

==External links==
*  
*  
*  
*  
*  

{{Navboxes title = Directors of Paris, je taime list =
 
 
 
 
 
 
 
 
 
 
 
}}
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 