Veeradhi Veera
{{Infobox film
| name           = Veeradhi Veera
| image          =
| image_size     =
| caption        =
| director       = Vijay
| producer       = N N Bhat
| writer         = Sri Uma Productions Unit
| screenplay     = M D Sundar Vishnuvardhan Geetha Geetha Vajramuni Sudheer
| music          = M. Ranga Rao
| cinematography = Kabirlal
| editing        = K Ramamohana Rao
| studio         = Sri Uma Productions
| distributor    =
| released       =  
| country        = India Kannada
}}
 1985 Cinema Indian Kannada Kannada film,  directed by  Vijay and produced by N N Bhat. The film stars Vishnuvardhan (actor)|Vishnuvardhan, Geetha (actress)|Geetha, Vajramuni and Sudheer in lead roles. The film had musical score by M. Ranga Rao.    This was the last film of veteran actor Musuri Krishnamurthy before his death.

==Cast==
  Vishnuvardhan
*Geetha Geetha
*Vajramuni
*Sudheer
*Mukhyamantri Chandru
*Musuri Krishnamurthy
*N S Rao
*Doddanna Kanchana
*Soumyashree
*Rani
*Shakila
*Baby Rekha
*Ravichandra
*B K Shankar
*Chikkanna
*Karantha
*Master Chethan
 

==Soundtrack==
The music was composed by M. Ranga Rao. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Premada Vijaya Thande || S. Janaki|Janaki, S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.49
|-
| 2 || Bittorunte Intha || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 03.56
|- Janaki || Chi. Udaya Shankar || 04.32
|-
| 4 || Ellige Ee Payana || S. Janaki|Janaki, S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.18
|}

==References==
 

==External links==
*  

 
 
 
 


 