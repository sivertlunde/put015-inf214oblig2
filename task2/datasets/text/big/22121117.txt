The Last International Playboy
 
{{Infobox film
| name           = The Last International Playboy
| image          = Last international playboy-2.jpg
| alt            =  
| caption        = Promotional poster
| director       = Steve Clark
| producer       = 
| writer         = Steve Clark   Thomas Moffett Lucy Gordon
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Delta Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Last International Playboy (also known as Frost) is a 2008 American independent film directed by Steve Clark and written by Steve Clark and Thomas Moffett. The cast includes Jason Behr, Monet Mazur, Krysten Ritter, Mike Landry, India Ennenga, Lydia Hearst, Lucy Gordon, and Carlos Velazquez. 

Shooting began in May 2007. The film premiered at the Slamdance Film Festival in January 2008. The film was also screened and well received at the 2008 Genart Film Festival, Newport Beach Film Festival, and Oxford International Film Festival.

== Plot summary ==
The ultimate New York City playboy Jack Frost (Jason Behr) is shattered when he discovers his one true love Carolina (Monet Mazur) is about to marry another man. Still haunted by his mothers suicide, Jack spirals into a self-destructive cycle of whiskey and reckless behavior, while his best friends Ozzy (Krysten Ritter) and Scotch (Mike Landry) try to jar him back to reality.
Strangely enough, its Jacks eleven-year-old neighbor, Sophie (India Ennenga), who helps the drowning urbanite shake off the nostalgia and excess that threaten to consume him.

== Cast ==
*Jason Behr ... Jack Frost
*Monet Mazur	 ...	Carolina Lucy Gordon	 ...	Kate Hardwick
*Krysten Ritter	 ...	Ozzy
*Mike Landry	 ...	Scotch Evans
*India Ennenga	 ...	Sophie
*Polina Frantsena	 ...	Serafina
*María Jurado	 ...	Adriana
*Lydia Hearst-Shaw	 ...	Stella
*Amber Noelle Ehresmann	 ...	Parsley
*Leah Cary	 ...	Natty
*Ruza Madarevic	 ...	Verushka

==Production== Lucy Gordon - who played Kate in the film - and the film was dedicated in memory of her. Thomas Moffett also wrote the film Shrink (film)|Shrink starring Kevin Spacey and Robin Williams, which was received to much critical acclaim at the 2009 Sundance festival.

==Soundtrack== Bright Eyes- "First Day of My Life"
*The Dresden Dolls- "Dirty Business"
*Apples in Stereo- "Benefit of Lying (With Your Friends)"
*Alexi Murdoch- "All of My Days"
*Silver Seas- "Imaginary Girl"
*K. S. Rhoads- "New York in Winter"
*Burden of Man- "Walk of Shame"
*Burden of Man- "I Dont Wanna Grow Up"
*Ex-Vivian- "No Other Fish"
*Dopo Yume- "Brigitte Bardot"
*Falling Out of Love- "Your Bedroom"
*Peter Salett- "Big Deal"

==External links==
* 
* 

 
 
 
 
 
 
 