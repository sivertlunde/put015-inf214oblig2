Fe, Esperanza, Caridad
 
{{Infobox film
| name      =  	Fe, Esperanza, Caridad
| image_size   =
| image	= File:Feesperanzacaridad.jpg	
| alt      =
| caption    = Movie Poster
| director    = Cirio H. Santiago...(segment "Fe") Lamberto V. Avellana...(segment "Esperanza") Gerardo de Leon...(segment "Caridad")	
| producer    = Digna Santiago Danilo Santiago
| writer Credits    = Donato Valentin(story and screenplay) (segment "Esperanza") and Rino Bermudez(story and screenplay) (segment "Esperanza") Gerardo de Leon(story) (segment "Caridad"), Ikong(screenplay) (segment "Caridad"),Jojo Lapuz(screenplay) (segment "Caridad")and
Gerardo de Leon(screenplay) (segment "Caridad")
| screenplay   = 	
| story     =	
| based on    =
| starring    = Nora Aunor Dindo Fernando Jay Ilagan Ronaldo Valdez
| music     = Tito Arevalo(segment "Caridad") Tito Sotto(segment "Esperanza") 
| cinematography = Ricardo M. David(segment "Caridad") Justo Paulino(segment "Esperanza") 
| editing    = Edgardo Vinarao
| studio     = 
| distributor  = Premiere Productions
| released    = 10 May 1974
| runtime    = 
| country    = Philippines Filipino
| budget     = 
| gross     = 
}}
Fe, Esperanza, Caridad  is a 1974 Filipino film, produced by Premiere Productions. It is a trilogy featuring Nora Aunor giving life to the stories of three women: Fe&mdash;an emerging movie superstar who has an invalid husband&mdash;, Esperanza&mdash;a young wife living in a middle-class neighborhood in the city&mdash;and Caridad&mdash;a young novice who was seduced by the devil himself.
 National Artists for film; Gerardo de Leon and Lamberto V. Avellana.

==Synopsis==
;First segment: Fe

The first segment revolves around the love story of Fe, the singer discovered by a talent manager Tony Artiaga (Dindo Fernando). Fe soon becomes the big star, and then painfully witnesses her manager who is now her husband sink into despondency and booze and was addicted to alcohol and gambling that gradually destroys the fame, that Fe is now experiencing. Even before the husband dies, yes, you know it already. It’s A Star is Born borrowed up to the last line of Judy Garland: “Hello, everybody, This is Mrs. Norman Maine” to Aunor’s “Ako si Mrs. Artiaga.”This first episode was directed by Cirio H. Santiago

;Second segment: Esperanza

Telling the story of this cigarette vendor being wooed by a rich man, Vic (Bert Leroy, Jr.), even as her shy suitor, Domeng (Jay Ilagan) a jeepney driver, remains loyal, we see a tale of hope told without the ponderous air usually associated with that discourse. There is the subplot of a duplicitous “rich” boy passing on to Esperanza his drug dealing, without the heroine’s knowledge and this becomes, like any subplots, distracting. When the story, however, is on Doming, the driver, and Esperanza with her dreams and ambitions. Furthermore, with grandma Marings advice (Rosa Aguirre) and with the help of brother Boyet (Romy Lapuz), Domeng and Esperanza got married. The second episode gives a reason to appreciate Lamberto V. Avellana’s insight as a filmmaker.

;Third segment: Caridad

Gerardo de Leon vividly describe the story of Caridad (Nora Aunor), a nun who loves Rodrigo (Ronaldo Valdez), the gardener in the convent. Caridad is experiencing a complex feeling of a woman fighting for the love she for Rodrigo and her bow as a nun. When Caridad discovered that Rodrigo and Satan are the same entity, Rodrigo made her life like hell. Although repugnant, Caridad continued dealing with Rodrigo in the hope that she could convince him to return to the Lord. In return for that plea, Caridad will do anything, even jump off the cliff. 

==Cast==
*Nora Aunor ... Fe / Esperanza / Caridad
*Dindo Fernando ... Tony Artiaga (segment "Fe")
*Ruben Rustia ... Don Benito (segment "Fe")
*Jay Ilagan ... Doming (segment "Esperanza")
*Divina Valencia ... Aling Vina (segment "Esperanza")
*Bert LeRoy Jr. ... Vic (segment "Esperanza")
*Rosa Aguirre ... Aling Rosa (segment "Esperanza")
*Romy Lapus ... Boyet (segment "Esperanza") 
*Rino Bermudez ... Mang Inggo (segment "Esperanza")
*Ronaldo Valdez ... Rodrigo/Satan (segment "Caridad")
*Patria Plata ... Mother Superior (segment "Caridad")
*Subas Herrero ... Demon (segment "Caridad")
*Laurice Guillen ... Marta (segment "Caridad")

==Recognition==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Group
! width="25%"| Category
! width="25%"| Nominee
! width="10%"| Result
|-
| rowspan="4" align="center"| 1975
| rowspan="3" align="left"| 23rd FAMAS Awards
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Actor
| align="center"| Ronaldo Valdez 
|  
|-
| align="left"| Best Supporting Actor
| align="center"| Ruben Rustia
|  
|}

==References==
 

==External links==
* 

 
 
 
 