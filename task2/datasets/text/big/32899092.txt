The Policewoman
{{Infobox film
| name           = The Policewoman
| image          = Mulher Polícia Film Poster.jpg
| caption        = Theatrical release poster
| director       = Joaquim Sapinho
| producer       = Maria João Sigalho Amândio Coroado
| screenplay     = Joaquim Sapinho
| based on       =
| starring       = Amélia Corôa Luduvic Videira Maria Silva Vítor Norte Ana Nave
| music          = Nuno Malo
| cinematography = Jacques Loiseleux Miguel Sales Lopes
| editing        = Manuela Viegas Vítor Alves Catarina Ruivo
| studio         = Rosa Filmes
| distributor    = 
| released       = 2003
| runtime        = 84 minutes
| country        = Portugal Brazil France Spain
| language       = Portuguese
| budget         = 2.067.000 €
}} Portuguese drama directed by Joaquim Sapinho, produced at Rosa Filmes, which had its world premiere in 2003 at the Berlin International Film Festival.

== Plot ==
The film tells the story of a mother and a son from the interior of Portugal that run away to Lisbon to avoid being separated by the state institutions that want to take the child away from the mother.

== Cast ==
* Amélia Corôa as Tânia
* Luduvic Vieira as Rato
* Maria Silva as Liliana
* Vítor Norte as the Truck Driver
* Ana Nave as the Policewoman

== Production == Bosnia during the Yugoslav Wars. Bosnia Diaries would eventually be released in 2005. The two films were post-produced simultaneously, influencing each other both in style and theme. The darkness, realism and harshness of The Policewoman clearly echoes the Bosnian War experienced by Sapinho. That experience of war would cause a great change to his vision of the world, leading him, when shooting The Policewoman, to discover in Portugal things he thought had only happened in Bosnia in consequence of the war. It made him realize that, after all, they were also happening in his native town in consequence of the economic transformations imposed by capitalism via the European Union. 

== Reception ==
Besides having had its world premiere in 2003, at the Berlin International Film Festival, The Policewoman was part of the official selection of innumerous film festivals, like the Edinburgh Film Festival or the Pusan Film Festival. Received in the Lecce European Film Festival the award for Best Film and Best Photography. 

==References==
 

== External links ==
*   (click on "ENG", then on "DIRECTORS", then on "JOAQUIM SAPINHO", then on "THE POLICE WOMAN")
*  

 

 
 
 
 
 
 
 
 