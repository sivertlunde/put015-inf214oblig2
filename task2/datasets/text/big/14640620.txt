Shivering Sherlocks
{{Infobox Film |
  | name           = Shivering Sherlocks
  | image          = Shiv-sher-lc.jpg
  | caption        =  
  | director       = Del Lord 
  | writer        = Del Lord Elwood Ullman | Kenneth MacDonald Frank Lackteen Duke York Stanley Blystone Cy Schindell Joe Palma 
  | cinematography = Allen G. Siegler 
  | editing        = Henry DeMond 
  | producer       = Hugh McCollum
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 17 17"
  | country        = United States
  | language       = English
}}
Shivering Sherlocks is the 104th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are mistaken for three armored car thieves. Captain Mullins (Vernon Dent) gives the boys a lie detector test, but finds no reason to hold them. The Stooges and Captain Mullins lie the lie detector test. He releases them, under protective custody, to Gladys Harmon (Christine McIntyre), owner of the Elite Café, who gives them an alibi. They try to return the favor by working at the Café, but have trouble with this.

When Gladys is informed that she inherited some money and a spooky old mansion, the Stooges escort her to check out the property, where the real armored car bandits and their hideous hatchet man, Angel (Duke York), are hiding. While the Stooges are distracted due to Larry getting something to open the door and Shemp talking to Moe, Gladys has her mouth covered and is dragged inside. The Stooges go in to search for her. Gladys is meanwhile tied to a chair and gagged. Angel enters the room and seems about to attack her, but hears the Stooges and goes after them. The Stooges encounter him and he chases them through the house, but Shemp is able to trap all three criminals in barrels and they are arrested. Gladys has meanwhile somehow been freed and watches this happily. Meanwhile Shemp accidentally drops a barrel of flour on Larry and Moe.

==Production notes== remade in 1955 as Of Cash and Hash, using ample stock footage. Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion, p. 324-325; Comedy III Productions, Inc., ISBN 0-9711868-0-4 

This was the final Stooge film directed by long-time Stooge director Del Lord, as well as the only Stooge short he directed with Shemp Howard as a member of the team. 

This entry features a recurring gag of "Man Vs. Soup," wherein one of the Stooges is about to eat a soup that, at first unbeknownst to them, contains a live bivalve that continually eats the crackers the Stooge drops in it, and a battle between the two parties ensues. In the episode Dutiful But Dumb, Curly tries to defeat a stubborn oyster; here, Moe is having problems with clam chowder.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 