The House Next Door (film)
{{Infobox television film|
  name           = The House Next Door |
  image          = The House Next Door - TV.jpg |
  caption        = Theatrical release poster |
  writer         = Suzette Couture (story) Anne Rivers Siddons (novel) | Colin Ferguson |
  director       = Jeff Woolnough |
  cinematography = David Herrington |
  editing        = Mike Lee |
  producer       = Wendy Grean |
  studio         = Muse Entertainment Enterprises Cinema Sound Design |
  distributor    = Lifetime Movie Network |
  released       =   |
  runtime        = 86 minutes |
 country         = United States |
  language       = English |
  music          = Matt Zoller Seitz |
  budget         = |
  gross          = |
}} Colin Ferguson. 

==Plot==
The peaceful and happy life of Walker and Col Kennedy is interrupted when Kim, a brilliant and attractive male architect, builds a dream house next to theirs. All the people who move into the house turn evil or end up having "accidents" and unexplainable deaths. They realize that the house targets their fears and feeds off of them until it drives them insane. In the end, the Kennedys succeed in destroying the house, killing the architect in the process. In the final scene another couple is seen eyeing a new house identical to the old one. This is the greatest departure from the novel, in which the Kennedys kill the architect before trying to destroy the house &ndash; in the epilogue it is revealed that they themselves lost their lives and the house is still intact.

==Cast==
* Lara Flynn Boyle as Col Kennedy Colin Ferguson as Walker
* Noam Jenkins as Norman Greene
* Julie Stewart	... 	Anita
* Heather Hanson as Claire
* Charlotte Sullivan as Pie Harrelson
* Natalie Lisinska as Eloise
* Heidi von Palleske as Virginia Guthrie
* Aidan Devine as Buck
* Stephen Amell as Buddy Harrelson
* Peter MacNeill		
* Mark-Paul Gosselaar as Kim
* Emma Campbell	as Suzannah Greene
* Niamh Wilson as Belinda Greene
* Scott Gibson as Roger
* Trevor Bain as Tyler
* Evan Williams as Toby
* Michael Scratch as Josh
* Megan Vincent	as Natalie
* Kasia Vassos as Chrissie
* Sean Orr as Charles
* Victoria Fodor as Client
* Jef Mallory as Courier
* Sal Scozzari as TV Installer
* Ben Lewis as Pizza Boy

==Release== Lifetime in the United States  and was released of DVD in Germany on 29 May 2007. 

==Awards==
Niamh Wilson was nominated as Best Supporting Actress for a Young Artist Award. 

==References==
 
http://www.mylifetime.com/movies/the-house-next-door

==External links==
* 

 
 
 
 
 
 
 
 
 
 