Schindler's List
 
 
 
{{Infobox film
| name           = Schindlers List
| image          = Schindlers List movie.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Steven Spielberg
| producer       = {{plainlist | 
* Steven Spielberg
* Gerald R. Molen
* Branko Lustig
}}
| screenplay     = Steven Zaillian
| based on       =  
| starring       = {{plainlist | 
* Liam Neeson
* Ben Kingsley
* Ralph Fiennes
* Caroline Goodall
* Jonathan Sagall
* Embeth Davidtz
}}
| music          = John Williams
| cinematography = Janusz Kamiński Michael Kahn
| studio         = Amblin Entertainment Universal Pictures
| released       =   
| runtime        = 197 minutes 
| country        = {{plainlist |
* United States
}}
| language       = English 
| budget         = $22 million 
| gross          = $321.2 million 
}} epic historical period drama, directed and co-produced by Steven Spielberg and scripted by Steven Zaillian. It is based on the novel Schindlers Ark by Thomas Keneally, an Australian novelist. The film is based on the life of Oskar Schindler, a German businessman who saved the lives of more than a thousand mostly History of the Jews in Poland|Polish-Jewish refugees during the Holocaust by employing them in his factories. It stars Liam Neeson as Schindler, Ralph Fiennes as Schutzstaffel (SS) officer Amon Goeth, and Ben Kingsley as Schindlers Jewish accountant Itzhak Stern.
 Sid Sheinberg sent him a book review of Schindlers Ark. Universal Studios bought the rights to the novel, but Spielberg, unsure if he was ready to make a film about the Holocaust, tried to pass the project to several other directors before finally deciding to direct the film himself.

Principal photography took place in Kraków, Poland, over the course of 72 days in 1993. Spielberg shot the film in black and white and approached it as a documentary. Cinematographer Janusz Kamiński wanted to give the film a sense of timelessness. John Williams composed the score, and violinist Itzhak Perlman performs the films main theme.
 premiered on Best Picture, Best Director, Best Adapted Best Original list of the 100 best American films of all time. The Library of Congress selected it for preservation in the National Film Registry in 2004.

==Plot== Polish Jews SS officials and acquires a factory to produce enamelware. To help him run the business, Schindler enlists the aid of Itzhak Stern, a local Jewish official who has contacts with black marketeers and the Jewish business community. Stern helps Schindler arrange loans to finance the factory. Schindler maintains friendly relations with the Nazis and enjoys wealth and status as "Herr Direktor", and Stern handles administration. Schindler hires Jewish workers because they cost less, while Stern ensures that as many people as possible are deemed essential to the German war effort, which saves them from being transported to concentration camps or killed.

SS-Untersturmführer (second lieutenant) Amon Goeth arrives in Kraków to oversee construction of Kraków-Płaszów concentration camp|Płaszów concentration camp. When the camp is completed, he orders the ghetto liquidated. Many people are shot and killed in the process of emptying the ghetto. Schindler witnesses the massacre and is profoundly affected. He particularly notices a tiny girl in a red coat – one of the few splashes of color in the black-and-white film – as she hides from the Nazis. When he later sees the red coat on a wagon loaded with bodies being taken away to be burned, he knows the girl is dead. Schindler is careful to maintain his friendship with Goeth and, through bribery and lavish gifts, continues to enjoy SS support. Goeth brutally mistreats his maid and randomly shoots people from the balcony of his villa, and the prisoners are in constant daily fear for their lives. As time passes, Schindlers focus shifts from making money to trying to save as many lives as possible. He bribes Goeth into allowing him to build a sub-camp for his workers so that he can better protect them.

As the Germans begin to lose the war, Goeth is ordered to ship the remaining Jews at Płaszów to Auschwitz concentration camp. Schindler asks Goeth to allow him to move his workers to a new munitions factory he plans to build in his home town of Svitavy|Zwittau-Brinnlitz. Goeth agrees, but charges a huge bribe. Schindler and Stern create "Schindlers List" – a list of people to be transferred to Brinnlitz and thus saved from transport to Auschwitz.
 the Jewish Sabbath. To keep his workers alive, he spends much of his fortune bribing Nazi officials and buying shell casings from other companies; his factory does not produce any usable armaments during its seven months of operation. Schindler runs out of money in 1945, just as Germany surrenders, ending the war in Europe.

As a Nazi Party member and war profiteer, Schindler must flee the advancing   (Schindler Jews) wake up the next morning, a Soviet soldier announces that they have been liberated. The Jews leave the factory and walk to a nearby town.

After scenes depicting Goeths execution after the war and a summary of Schindlers later life, the black-and-white frame changes to a color shot of actual Schindlerjuden at Schindlers grave in Jerusalem. Accompanied by the actors who portrayed them, the Schindlerjuden place stones on the grave. In the final scene, Neeson places a pair of roses on the grave.

==Cast==
 

* Liam Neeson as Oskar Schindler
* Ben Kingsley as Itzhak Stern
* Ralph Fiennes as Amon Goeth
* Caroline Goodall as Emilie Schindler
* Jonathan Sagall as Poldek Pfefferberg
* Embeth Davidtz as Helen Hirsch
* Małgorzata Gebel as Wiktoria Klonowska
* Mark Ivanir as Marcel Goldberg
* Beatrice Macola as Ingrid
* Andrzej Seweryn as Julian Scherner
* Friedrich von Thun as Rolf Czurda
* Jerzy Nowak as Investor
* Norbert Weisser as Albert Hujar
* Anna Mucha as Danka Dresner
* Piotr Polk as Leo Rosner
* Rami Heuberger as Joseph Bau
* Ezra Dagan as Rabbi Menasha Lewartow
* Hans-Jörg Assmann as Julius Madritsch Rudolf Höß
* Daniel Del Ponte as Josef Mengele The Girl in Red

==Themes==
The film explores the theme of good versus evil, using as its main protagonist a "good German", a popular characterization in American cinema.   While Goeth is characterized as an almost completely dark and evil person, Schindler gradually evolves from Nazi supporter to rescuer and hero.  Thus a second theme of redemption is introduced as Schindler, a disreputable schemer on the edges of respectability, becomes a father figure responsible for saving the lives of over a thousand people.  

==Production==

===Development=== MGM in Howard Koch MCA president Sid Sheinberg New York Times review of the book. Spielberg, astounded by Schindlers story, jokingly asked if it was true. "I was drawn to it because of the paradoxical nature of the character," he said. "What would drive a man like this to suddenly take everything he had earned and put it all in the service of saving these lives?"  Spielberg expressed enough interest for Universal Pictures to buy the rights to the novel.  At their first meeting in spring 1983, he told Pfefferberg he would start filming in ten years.  In the end credits of the film, Pfefferberg is credited as a consultant under the name Leopold Page. 

 
 The Pianist, 1991 remake of Cape Fear instead.  Billy Wilder expressed an interest in directing the film as a memorial to his family, most of whom died in the Holocaust. 
 Holocaust deniers Jurassic Park blood money",  and believed the film would flop. 
 Out of Africa, to write the next draft. Luedtke gave up almost four years later, as he found Schindlers change of heart too unbelievable.  During his time as director, Scorsese hired Steven Zaillian to write a script. When he was handed back the project, Spielberg found Zaillians 115-page draft too short, and asked him to extend it to 195 pages. Spielberg wanted more focus on the Jews in the story, and he wanted Schindlers transition to be gradual and ambiguous, not a sudden breakthrough or epiphany. He extended the ghetto liquidation sequence, as he "felt very strongly that the sequence had to be almost unwatchable." 

===Casting=== CEO Steve Steve Ross, who had a charisma that Spielberg compared to Schindlers.  He also located a tape of Schindler speaking, which Neeson studied to learn the correct intonations and pitch. 

Fiennes was cast as Amon Goeth after Spielberg viewed his performances in   and  s and talked to Holocaust survivors who knew Goeth. In portraying him, Fiennes said "I got close to his pain. Inside him is a fractured, miserable human being. I feel split about him, sorry for him. Hes like some dirty, battered doll I was given and that I came to feel peculiarly attached to."  Fiennes looked so much like Goeth in costume that when Mila Pfefferberg (a survivor of the events) met him, she trembled with fear. 
 Gandhi in 1982 biographical film. 

Overall, there are 126 speaking parts in the film. Thousands of extras were hired during filming.  Spielberg cast Israeli and Polish actors specially chosen for their Eastern European appearance.  Many of the German actors were reluctant to don the SS uniform, but some of them later thanked Spielberg for the cathartic experience of performing in the movie.  Halfway through the shoot, Spielberg conceived the epilogue, where 128 survivors pay their respects at Schindlers grave in Jerusalem. The producers scrambled to find the Schindlerjuden and fly them in to film the scene. 

===Filming=== yarmulkes and opened up Haggadah|Haggadas, and the Israeli actors moved right next to them and began explaining it to them. And this family of actors sat around and race and culture were just left behind." 

  }}
Shooting Schindlers List was deeply emotional for Spielberg, the subject matter forcing him to confront elements of his childhood, such as the antisemitism he faced. He was surprised that he did not cry while visiting Auschwitz; instead he found himself angry and filled with outrage. He was one of many crew members who could not force themselves to watch during shooting of the scene where aging Jews are forced to run naked while being selected by Nazi doctors to go to Auschwitz.  Spielberg commented that he felt more like a reporter than a film maker – he would set up scenes and then watch events unfold, almost as though he were witnessing them rather than creating a movie.  Several actresses broke down when filming the shower scene, including one who was born in a concentration camp.  Spielberg, his wife Kate Capshaw, and their five children rented a house in suburban Kraków for the duration of filming.  He later thanked his wife "for rescuing me ninety-two days in a row&nbsp;... when things just got too unbearable".  Robin Williams called Spielberg to cheer him up, given the  profound lack of humor on the set.  Spielberg spent several hours each evening editing Jurassic Park, which was scheduled to premiere in June 1993. 
 German and Polish language in scenes to recreate the feeling of being present in the past. He initially considered making the film entirely in those languages, but decided "theres too much safety in reading. It would have been an excuse to take their eyes off the screen and watch something else." 

===Cinematography===
Influenced by the 1985 documentary film Shoah (film)|Shoah, Spielberg decided not to plan the film with storyboards, and to shoot it like a documentary. Forty percent of the film was shot with handheld cameras, and the modest budget meant the film was shot quickly over seventy-two days.  Spielberg felt that this gave the film "a spontaneity, an edge, and it also serves the subject."  He filmed without using Steadicams, elevated shots, or zoom lenses, "everything that for me might be considered a safety net."  This matured Spielberg, who felt that in the past he had always been paying tribute to directors such as Cecil B. DeMille or David Lean. 

The decision to shoot the film mainly in black and white contributed to the documentary style of cinematography, which cinematographer Janusz Kamiński compared to German Expressionism and Italian neorealism.  Kamiński said that he wanted to give the impression of timelessness to the film, so the audience would "not have a sense of when it was made."  Spielberg decided to use black and white to match the feel of actual documentary footage of the era.  Universal chairman Tom Pollock asked him to shoot the film on a color Original camera negative|negative, to allow color VHS copies of the film to later be sold, but Spielberg did not want to accidentally "beautify events." 

===Music===
 
John Williams, who frequently collaborates with Spielberg, composed the score for Schindlers List. The composer was amazed by the film, and felt it would be too challenging. He said to Spielberg, "You need a better composer than I am for this film." Spielberg responded, "I know. But theyre all dead!"  Itzhak Perlman performs the theme on the violin. 

Regarding Schindlers List, Perlman said:

{{quote | Perlman: "I couldnt believe how authentic he   got everything to sound, and I said, John, where did it come from? and he said, Well I had some practice with Fiddler on the Roof and so on, and everything just came very naturally and thats the way it sounds."

Interviewer: "When you were first approached to play for Schindlers List, did you give it a second thought, did you agree at once, or did you say Im not sure I want to play for movie music?

Perlman: "No, that never occurred to me, because in that particular case the subject of the movie was so important to me, and I felt that I could contribute simply by just knowing the history, and feeling the history, and indirectly actually being a victim of that history."  }}
 soundtrack album. 

==Symbolism==

===The girl in red===
 
While the film is shot primarily in black and white, a red coat is used to distinguish a little girl in the scene depicting the liquidation of the Kraków ghetto. Later in the film, Schindler sees her dead body, recognizable only by the red coat she is still wearing. Spielberg said the scene was intended to symbolise how members of the highest levels of government in the United States knew the Holocaust was occurring, yet did nothing to stop it. "It was as obvious as a little girl wearing a red coat, walking down the street, and yet nothing was done to bomb the German rail lines. Nothing was being done to slow down&nbsp;... the annihilation of European Jewry," he said. "So that was my message in letting that scene be in color."  Andy Patrizio of IGN notes that the point at which Schindler sees the girls dead body is the point at which he changes, no longer seeing "the ash and soot of burning corpses piling up on his car as just an annoyance."  Professor André H. Caron of the Université de Montréal wonders if the red symbolises "innocence, hope or the red blood of the Jewish people being sacrificed in the horror of the Holocaust." 

The girl was portrayed by Oliwia Dąbrowska, three years old at the time of filming. Spielberg asked Dąbrowska not to watch the film until she was eighteen, but she watched it when she was eleven, and was "horrified."  Upon seeing the film again as an adult, she was proud of the role she played.  Although it was unintentional, the character is similar to Roma Ligocka, who was known in the Kraków Ghetto for her red coat. Ligocka, unlike her fictional counterpart, survived the Holocaust. After the film was released, she wrote and published her own story, The Girl in the Red Coat: A Memoir (2002, in translation).  According to a 2014 interview of family members, the girl in red was inspired by Kraków resident Genya Gitel Chil. 

===Candles=== Aryan men, especially Goeth and Schindler. 

===Other symbolism===
To Spielberg, the black and white presentation of the film came to represent the Holocaust itself: "The Holocaust was life without light. For me the symbol of life is color. Thats why a film about the Holocaust has to be in black-and-white."  Robert Gellately notes the film in its entirety can be seen as a metaphor for the Holocaust, with early sporadic violence increasing into a crescendo of death and destruction. He also notes a parallel between the situation of the Jews in the film and the debate in Nazi Germany between making use of the Jews for slave labor or exterminating them outright.  Water is seen as giving deliverance by Alan Mintz, Holocaust Studies professor at the Jewish Theological Seminary of America in New York. He notes its presence in the scene where Schindler arranges for a Holocaust train loaded with victims awaiting transport to be hosed down, and the scene in Auschwitz, where the women are given an actual shower instead of receiving the expected gassing. 

==Release==
The film opened on December 15, 1993. By the time it closed in theaters on September 29, 1994, it had grossed $96.1 million ($ }} in   dollars)  in the United States and over $321.2 million worldwide.  In Germany, where it was shown in 500 theaters, the film was viewed by over 100,000 people in its first week alone  and was eventually seen by six million people.  The film was popular in Germany and a success worldwide. 
 Nielsen rating Holocaust Memorial Day in 1998. 

The   on March 5, 2013. 

Following the success of the film, Spielberg founded the Survivors of the Shoah Visual History Foundation, a nonprofit organization with the goal of providing an archive for the filmed testimony of as many survivors of the Holocaust as possible, to save their stories. He continues to finance that work.  Spielberg used proceeds from the film to finance several related Documentary film|documentaries, including Anne Frank Remembered (1995), The Lost Children of Berlin (1996), and The Last Days (1998). 

==Reception==

===Critical response===
Schindlers List is widely acclaimed as a remarkable achievement by film critics and audiences.  Notable Americans such as talk show host Oprah Winfrey and President Bill Clinton urged their countrymen to see it.   World leaders in many countries saw the film, and some met personally with Spielberg.  Stephen Schiff of The New Yorker called it the best historical drama about the Holocaust, a movie that "will take its place in cultural history and remain there."  Roger Ebert described it as Spielbergs best, "brilliantly acted, written, directed, and seen."  Terrence Rafferty, also with The New Yorker, admired the films "narrative boldness, visual audacity, and emotional directness." He noted the performances of Neeson, Fiennes, Kingsley, and Davidtz as warranting special praise,  and calls the scene in the shower at Auschwitz "the most terrifying sequence ever filmed."  In his 2013 movie guide, Leonard Maltin awards the film a rare four-star rating and gives a lengthy review. He calls the film a "staggering adaptation of Thomas Keneallys best-seller," saying "this looks and feels like nothing Hollywood has ever made before."   "Spielbergs most intense and personal film to date," he concludes.  James Verniere of the Boston Herald noted the films restraint and lack of sensationalism, and called it a "major addition to the body of work about the Holocaust."  In his review for the New York Review of Books, British critic John Gross said his misgivings that the story would be overly sentimentalized "were altogether misplaced. Spielberg shows a firm moral and emotional grasp of his material. The film is an outstanding achievement."  Mintz notes that even the films harshest critics admire the "visual brilliance" of the fifteen-minute segment depicting the liquidation of the Kraków ghetto. He describes the sequence as "realistic" and "stunning".  He points out that the film has done much to increase Holocaust remembrance and awareness as the remaining survivors pass away, severing the last living links with the catastrophe.  The films release in Germany led to widespread discussion about why most Germans did not do more to help. 

Criticism of the film also appeared, mostly from academia rather than the popular press.  Horowitz points out that much of the Jewish activity seen in the ghetto consists of financial transactions such as lending money, trading on the black market, or hiding wealth, thus perpetuating a stereotypical view of Jewish life.  Horowitz notes that while the depiction of women in the film accurately reflects Nazi ideology, the low status of women and the link between violence and sexuality is not explored further.  History professor Omer Bartov of Brown University notes that the physically large and strongly drawn characters of Schindler and Goeth overshadow the Jewish victims, who are depicted as small, scurrying, and frightened – a mere backdrop to the struggle of good versus evil.  Doctors Samuel J. Leistedt and Paul Linkowski of the Université libre de Bruxelles describe Goeths character in the film as a classic psychopath. 

Horowitz points out that the films dichotomy of absolute good versus absolute evil glosses over the fact that the vast majority of Holocaust perpetrators were ordinary people; the movie does not explore how the average German rationalized their knowledge of or participation in the Holocaust.  Author Jason Epstein commented that the movie gives the impression that if people were smart enough or lucky enough, they could survive the Holocaust; this was not actually the case.  Spielberg responded to criticism that Schindlers breakdown as he says farewell is too maudlin and even out of character by pointing out that the scene is needed to drive home the sense of loss and to allow the viewer an opportunity to mourn alongside the characters on the screen. 

===Assessment by other film makers=== Death and Aryan Papers, which would have been about a Jewish boy and his aunt who survive the war by sneaking through Poland while pretending to be Catholic.  When scriptwriter Frederic Raphael suggested that Schindlers List was a good representation of the Holocaust, Kubrick commented, "Think thats about the Holocaust? That was about success, wasnt it? The Holocaust is about 6 million people who get killed. Schindlers List is about 600 who dont." 

Filmmaker Jean-Luc Godard accused Spielberg of using the film to make a profit out of a tragedy while Schindlers wife, Emilie Schindler, lived in poverty in Argentina.  Keneally disputed claims that she was never paid for her contributions, "not least because I had recently sent Emilie a check myself."  He also confirmed with Spielbergs office that payment had been sent from there.  Filmmaker Michael Haneke criticized the sequence in which Schindlers women are accidentally sent off to Auschwitz and herded into showers: "Theres a scene in that film when we dont know if theres gas or water coming out in the showers in the camp. You can only do something like that with a naive audience like in the United States. Its not an appropriate use of the form. Spielberg meant well – but it was dumb." 

The film was attacked by filmmaker and professor Claude Lanzmann, director of the nine-hour Holocaust documentary Shoah (film)|Shoah, who called Schindlers List a "kitschy melodrama" and a "deformation" of historical truth. Lanzmann was especially critical of Spielberg for viewing the Holocaust through the eyes of a German. Believing his own film to be the definitive account of the Holocaust, Lanzmann complained, "I sincerely thought that there was a time before Shoah, and a time after Shoah, and that after Shoah certain things could no longer be done. Spielberg did them anyway."  Spielberg accused him of wanting to be "the only voice in the definitive account of the Holocaust. It amazed me that there could be any hurt feelings in an effort to reflect the truth." 

===Reaction of the Jewish community=== Hungarian Jewish author Imre Kertész, a Holocaust survivor, feels it is impossible for life in a Nazi concentration camp to be accurately portrayed by anyone who did not experience it first-hand. While commending Spielberg for bringing the story to a wide audience, he found the films final scene at the graveyard neglected the terrible after-effects of the experience on the survivors and implied that they came through emotionally unscathed.  Rabbi Uri D. Herscher found the film an "appealing" and "uplifting" demonstration of humanitarianism.  Norbert Friedman noted that, like many Holocaust survivors, he reacted with a feeling of solidarity towards Spielberg of a sort normally reserved for other survivors.  Albert L. Lewis, Spielbergs childhood rabbi and teacher, described the movie as "Stevens gift to his mother, to his people, and in a sense to himself. Now he is a full human being." 

==Accolades== Time Out Vatican named Schindlers List among the most important 45 films ever made.  A Channel 4 poll named Schindlers List the ninth greatest film of all time,  and it ranked fourth in their 2005 war films poll.  The film was named the best of 1993 by critics such as James Berardinelli,  Roger Ebert,  and Gene Siskel.  Deeming the film "culturally significant", the Library of Congress selected it for preservation in the National Film Registry in 2004. 
 Best Film, Best Director, Best Supporting Best Cinematography. Best Film, Best Supporting Best Cinematography. Best Film, Best Cinematography (tied with The Piano), and Best Production Design.  The film also won many other awards and nominations worldwide. 

{| class="wikitable plainrowheaders"
|+ Major awards
|-
! scope="col" | Category
! scope="col" | Subject
! scope="col" | Result
|- Academy Awards 
|- Best Picture
| {{plainlist |
* Steven Spielberg
* Gerald R. Molen
* Branko Lustig
}}
|  
|- Best Director
| Steven Spielberg
|  
|- Best Adapted Screenplay
| Steven Zaillian
|  
|- Best Original Score
| John Williams
|   
|- Best Film Editing Michael Kahn
|  
|- Best Cinematography
| Janusz Kamiński
|  
|- Best Art Direction
| {{plainlist | 
* Ewa Braun
* Allan Starski
}}
|  
|- Best Actor
| Liam Neeson
|  
|- Best Supporting Actor
| Ralph Fiennes
|  
|- Best Sound
| {{plainlist |  Andy Nelson Steve Pederson
* Scott Millan
* Ron Judkins
}}
|  
|- Best Makeup and Hairstyling
| {{plainlist | Christina Smith
* Matthew Mungle
* Judy Alexander Cory
}}
|  
|- Best Costume Design
| Anna B. Sheppard
|  
|- ACE Eddie Award 
|-
! scope="row" | Best Editing
| Michael Kahn
|  
|- BAFTA Awards 
|- Best Film
| {{plainlist |
* Steven Spielberg
* Branko Lustig
* Gerald R. Molen
}}
|  
|- Best Direction
| Steven Spielberg
| 
|- Best Supporting Actor
| Ralph Fiennes
|  
|- Best Adapted Screenplay
| Steven Zaillian
|  
|- Best Music
| John Williams
|  
|- Best Editing Michael Kahn
| 
|- Best Cinematography
| Janusz Kamiński
|  
|- Best Supporting Actor
| Ben Kingsley
|  
|- Best Actor
| Liam Neeson
|  
|- Best Makeup and Hair
| {{plainlist |
* Christina Smith
* Matthew W. Mungle
* Waldemar Pokromski
* Pauline Heys
}}
|  
|- Best Production Design
| Allan Starski
|  
|- Best Costume Design
| Anna B. Sheppard
|  
|- Best Sound
| {{plainlist |
* Charles L. Campbell
* Louis L Edemann
* Robert Jackson
* Ronald Judkins
* Andy Nelson
* Steve Pederson
* Scott Millan
}}
|  
|- Chicago Film Critics Association Awards 
|- Best Film
| {{plainlist |
* Steven Spielberg
* Gerald R. Molen
* Branko Lustig
}}
|  
|- Best Director
| Steven Spielberg
|  
|- Best Screenplay
| Steven Zaillian
|  
|- Best Cinematography
| Janusz Kamiński
|  
|- Best Actor
| Liam Neeson
|  
|- Best Supporting Actor
| Ralph Fiennes
|  
|-
! colspan="3"| Golden Globe Awards 
|- Best Motion Picture – Drama
| {{plainlist |
* Steven Spielberg
* Gerald R. Molen
* Branko Lustig
}}
|  
|- Best Director
| Steven Spielberg
|  
|- Best Screenplay
| Steven Zaillian
|  
|- Best Actor – Motion Picture Drama
| Liam Neeson
|  
|- Best Supporting Actor – Motion Picture
| Ralph Fiennes
|  
|- Best Original Score
| John Williams
|  
|-
|}

{| class="wikitable plainrowheaders"
|+ American Film Institute recognition
|-
! scope="col" | Year
! scope="col" | List
! scope="col" | Result
|-
! scope="row" | 1998
| AFIs 100 Years...100 Movies
| #9 
|-
! scope="row" | 2003
|  AFIs 100 Years...100 Heroes and Villains
| Oskar Schindler – #13 hero; Amon Goeth – #15 villain 
|-
! scope="row" | 2005
| AFIs 100 Years...100 Movie Quotes
| "The list is an absolute good. The list is life." – nominated 
|-
! scope="row" | 2006
| AFIs 100 Years...100 Cheers
| #3 
|-
! scope="row" | 2007
| AFIs 100 Years...100 Movies (10th Anniversary Edition)
| #8 
|-
! scope="row" | 2008
| AFIs 10 Top 10
| #3 epic film 
|}

==Controversies==
 
 TV Parental Republicans and Democratic Party (United States)|Democrats, Coburn apologized, saying: "My intentions were good, but Ive obviously made an error in judgment in how Ive gone about saying what I wanted to say." He clarified his opinion, stating that the film ought to have been aired later at night when there would not be "large numbers of children watching without parental supervision". 

Controversy arose in Germany for the films television premiere on ProSieben. Heavy protests ensued when the station intended to televise it with two commercial breaks. As a compromise, the broadcast included one break, consisting of a short news update and several commercials. 
 Senate to President Fidel V. Ramos himself intervened, ruling that the movie could be shown uncut to anyone over the age of 15. 
 Slovak filmmaker Juraj Herz, the scene in which a group of women confuse an actual shower with a gas chamber is taken directly, shot by shot, from his film Zastihla mě noc (Night Caught Up with Me, 1986). Herz wanted to sue, but was unable to come up with the money to fund the effort. 

The song Yerushalayim Shel Zahav ("Jerusalem of Gold") is featured in the films soundtrack and plays near the end of the film. This caused some controversy in Israel, as the song (which was written in 1967 by Naomi Shemer) is widely considered an informal anthem of the Israeli victory in the Six-Day War. In Israeli prints of the film the song was replaced with Halikha LeKesariya ("A Walk to Caesarea") by Hannah Szenes, a World War II resistance fighter. 

==Impact on Krakow==
Due to the increased interest in Kraków created by the film, the city bought Schindlers former enamelware factory in 2007 to create a permanent exhibition about the German occupation of the city from 1939 to 1945. The museum opened in June 2010. 

==See also==
* 1993 in film

==Notes==
{{notelist
| notes =
{{efn
| name = Score Grammy for the films musical score.  .
}}
}}

==Citations==
 

==References==
 

*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*  
*   }}
*  
*  
*  
*  
*  
*  
*  
*  
*  
* 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
* 
*  
*  
*  
*  
*   }}
*   }}
*  
*  
*  
*  
*  
*  
*  
*  
*  
*   }}
*  
*   }}
*   }}
*  
*  
*  
*   }}
*  
*  
*  
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*   }}
*  
*   }}
*  

 

==External links==
 
*  
*   at the American Film Institute Catalog of Motion Pictures
*  
*  
*  
*  
*  , founded by Steven Spielberg, preserves the testimonies of Holocaust survivors and witnesses
*   at Yad Vashem
*  
*   from the United States Holocaust Memorial Museum
*   from the United States Holocaust Memorial Museum
* 

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 