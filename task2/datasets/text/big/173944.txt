The Lord of the Rings: The Two Towers
 
 
 
{{Infobox film
| name                 = The Lord of the Rings: The Two Towers
| image                = Lord of the Rings - The Two Towers.jpg
| caption              = Theatrical release poster
| director             = Peter Jackson
| producer             = {{Plain list|
* Barrie M. Osborne
* Fran Walsh
* Peter Jackson
}}
| screenplay           = {{Plain list|
* Fran Walsh
* Philippa Boyens
* Stephen Sinclair
* Peter Jackson
}}
| based on             =  
| starring             = {{Plain list|
* Elijah Wood
* Ian McKellen
* Viggo Mortensen
* Liv Tyler
* Sean Astin
* Cate Blanchett
* John Rhys-Davies
* Bernard Hill
* Christopher Lee Billy Boyd
* Dominic Monaghan
* Orlando Bloom
* Hugo Weaving
* Miranda Otto
* David Wenham
* Brad Dourif
* Sean Bean
* Andy Serkis
}}
| music                = Howard Shore
| cinematography       = Andrew Lesnie
| editing              = {{Plain list| Michael Horton
* Jabez Olssen
}}
| production companies = {{Plain list|
* WingNut Films
* The Saul Zaentz Company
}}
| distributor          = New Line Cinema
| released             =  
| runtime              = 179 minutes 
| country              = {{Plain list|
* New Zealand
* United States   
}}
| language             = English 
| budget               = $94 million   
| gross                = $926.05 million 
}}
The Lord of the Rings: The Two Towers is a 2002   (2001) and concluding with   (2003).
 Frodo and Sam continue Gimli come Rohan and Battle of Merry and Pippin escape capture, meet Treebeard the Ent, and help to plan an attack on Isengard.
 29th highest-grossing film of all time (inflation-adjusted, it is the 62nd most successful film in North America ). The film won numerous accolades and was nominated for six Academy Awards, winning two.

==Plot==
  Mines of Gandalf the Fellowship of Gimli pursue Merry and Pippin prisoner Orcs and Dunland lay siege to the lands.  Theodens nephew Éomer has accused Gríma of being a spy and was banished for undermining him. Éomer sets forth to the countryside to gather the remaining loyal men of the Rohan#People|Rohirrim. Éomers army ambushes and killes the Uruk-hai holding Merry and Pippin. Merry and Pippin flee into the forest and meet Treebeard, the oldest of the giant tree-like creatures called Ents.

Meanwhile, Frodo, Samwise and Gollum traverse the Dead Marshes, evading a Nazgûl. Upon reaching Mordor, they find its main entrance the Black Gate closed and guarded by Orcs.  Gollum convinces the pair that attempting to enter the gate will lead to their capture, offering instead to lead them to an unguarded entrance.  Gollum struggles between his loyalty to Frodo and his desire for the Ring, and Sam does not trust him.  
 Orthanc and informs Saruman of a weakness in the outer wall of Helms Deep.  Saruman dispatches his army to the stronghold.
 Warg riders and Aragorn is thrown from a cliff and nearly killed, but he later washes up on a river bank and makes his way to Helms deep. 

Samwise and Frodo are captured by Rangers of Ithilien and taken to their leader, Faramir.  To prevent Gollum being killed by Faramirs hunters, Frodo reveals Gollums presence and the creature is captured. Learning of Frodos Ring, Faramir orders that it be sent to Gondor.  

In Fangorn forest, Merry, Pippin, Treebeard and other Ents hold a council to decide on the role of the Ents in the war with Saruman.  The Ents decide at first not to get involved, but after Pippin shows them the destruction of their forest by Saruman, they decide to retaliate. 

The battle of Helms Deep begins between the Uruk-Hai and Rohirrim with Aragorn and his companions as well as some Elves from Rivendell who have decided to honour the alliance between men and elves.  Explosives are used against the weak wall, allowing the Uruks to breach the fortress.  Aragorn leads Théoden, Legolas and the remaining Rohirrim to attack the Uruk-hai army, allowing the Rohirrims women and children to escape into the mountains.  Gandalf appears, accompanied by Éomer and his men. The combined forces cause the Uruk-hai to flee into Fangorn, where the Ents and their trees attack them.  At Isengard, the Ents destroy the Uruk-hai and release the river dam, drowning the surviving Orc defenders and stranding Saruman in his tower.

In the East, Faramir has the hobbits taken to the ruined Gondor city of Minor places in Middle-earth#O|Osgiliath.  Faramirs forces are attacked by Orcs led by a Nazgûl. With the aid of Sam and Faramir, Frodo escapes the Nazgûls attempt to capture him and the Ring.  The Ring attempts to make Frodo kill Sam but he resists.  The Nazgûl is defeated and flees.  Faramir frees the hobbits and sends them on their journey, joined by Gollum.  Gandalf remarks that Sauron will seek retribution for his defeat, stating that hope now rests with Frodo and Sam. Feeling betrayed by Frodo for allowing him to be captured by Faramir, Gollum decides to reclaim the ring by secretly leading Frodo and Sam to a creature he refers to only as "her".
As they walk through the woods Mordor lays ahead as Sauron`s eye gazes at his army .

==Cast==
 
 , Bernard Hill, Ian McKellen, Orlando Bloom, and Viggo Mortensen. According to Peter Jackson, The Two Towers is centered around Aragorn. ]]
Like the other films in the series, The Two Towers has an ensemble cast,  and the cast and their respective characters include:
*   sent on a quest to destroy the One Ring, the burden of which is becoming heavier.
*   who fell fighting a Balrog and has now returned, more powerful than ever, to finish his task.
*  s throne who has come to Rohan (Middle-earth)|Rohans defence.
*   princess of Rivendell and Aragorns true love. Samwise "Sam" Gamgee: Frodos loyal hobbit gardener and companion.
*  , who discusses Middle-earths future with Elrond.
*   warrior and one of Aragorns companions.
** Also voices  s, who is roused to anger after seeing that Saruman had decimated a large part of Fangorn Forest.
*  , who is under Sarumans spell until Gandalf heals him so he can lead his people once more.
*   in the previous film. Billy Boyd Peregrin "Pippin" Took: a hobbit mistakenly captured by the Uruk-hai. Meriadoc "Merry" Brandybuck: a distant cousin of Frodos who is mistakenly captured along with Pippin by the Uruk-hai. Legolas Greenleaf: an elven archer and one of Aragorns companions.
* Hugo Weaving as Elrond: the Elven-Lord of Rivendell who expresses doubt over his daughters love for Aragorn.
* Miranda Otto as Éowyn: Théodens niece, who is in love with Aragorn.
*   and captain of the Ithilien Rangers, who captures Frodo, Sam and Gollum.
* Brad Dourif as Gríma Wormtongue: an agent of Saruman at Edoras, who renders Théoden incapable of decisions, and desires Éowyn. 
*   who was exiled by Gríma.
* Sean Bean as Boromir: Faramirs older brother and a fallen member of the Fellowship who appears in flashbacks since his death, more prominently in the films extended edition.
*  .
*  . John Leigh as Háma (Middle-earth)|Háma: the loyal doorwarden of the Golden Hall and a majordomo of Théoden. Bruce Hopkins as Gamling: Théodens chief lieutenant and a skilled member of the Royal Guard of Rohan.
* John Bach as List of original characters in The Lord of the Rings film series|Madril: Faramirs closest aide, who informs him of battle preparations.
;The following appear only in the Extended Edition:
 Denethor II, Steward of Gondor and Boromir and Faramirs father.
 Battle of Helms Deep, Peter Jackson has a cameo appearance as one of the men on top of the gate, throwing a spear at the attacking Uruk-hai. His children and Elijah Woods sister also cameo as young refugees in the caves behind the Hornburg, and Alan Lee and Dan Hennah also cameo as soldiers preparing for the battle. The son of a producers friend, Hamish Duncan, appears as a reluctant young Rohirrim warrior. Daniel Falconer has a cameo as an Elvish archer at the battle. 

==Comparison to the source material== Battle of Helms Deep the climax, a decision affecting the whole storys moods and style.

The most notable difference between the book and the film is the structure. Tolkiens The Two Towers is split into two parts; one follows the war in Rohan, while the other focuses on the journey of Frodo and Sam. The film omits the books opening, Boromirs death, which was used as a linear climax at the end of The Fellowship of the Ring. Also, the film climaxes with the Battle of Helms Deep, while the book ends with the Fellowship going to Isengard and Frodos confrontation with Shelob, scenes which were left for the film adaptation of The Return of the King. This was done partly to fit more closely the timeline indicated by the book.

One notable change in plotting is that in the film Théoden is literally possessed by Saruman, whereas in the book he is simply depressed and deluded by Wormtongue. Afterwards, in the film, Théoden is still unsure of what to do, and flees to Helms Deep. In the book he rides out to war, only ending up besieged when he considers helping Erkenbrand. Erkenbrand does not exist in the films: his character is combined with Éomer as the Rohirrim general who arrives with Gandalf at the films end. Éomer himself is present during the entire battle in the book.

On the way to Helms Deep, the refugees from Edoras are attacked by Wargs. The scene is possibly inspired by one in the book cut from The Fellowship of the Ring where it is the Fellowship who battle them. Here, a new subplot is created where Aragorn falls over a cliff, and is assumed to be dead; Jackson added it to create tension.    This scene also resonates with a new subplot regarding Arwen, where she decides to leave Middle-earth after losing hope in the long-term possibilities of her love. In the book, Arwens role is primarily recorded in the Appendices, and she is never depicted as considering such an act.
 attacked by an army out of Dol Guldur in Mirkwood, and then later counter-attacked and assaulted the fortress itself. 

Another change is the fact Treebeard does not immediately decide to go to war. This adds to the tension, and Boyens describes it as making Merry and Pippin "more than luggage".  Here, the Hobbits show Treebeard what Saruman has done to the forest, prompting his decision to act. Another structural change is that the Hobbits meet Gandalf the White early on, explaining why the Hobbits do not react to his return when they meet him again following Isengards destruction. This was explained in the book by Gandalf arriving at Isengard in the middle of the night to talk to Treebeard.

The filmmakers decision to leave Shelob for the third film meant that Faramir had to become an obstacle for Frodo and Sam.  In the book, Faramir (like Aragorn) quickly recognizes the Ring as a danger and a temptation, and does not hesitate long before letting Frodo and Sam go. In the film, Faramir first decides that the Ring shall go to Gondor and his father Denethor, as a way to prove his worth. In the film, Faramir takes Frodo, Sam and the Ring to the Battle of Osgiliath&nbsp;— they do not go there in the book. Jackson winks to readers with Sams line, "By all rights we shouldnt even be here, but we are." After seeing how strongly the Ring affects Frodo during the Nazgûl attack, Faramir changes his mind and lets them go. These changes reshape the books contrast between Faramir and Boromir, who in The Fellowship of the Ring attempted to take the Ring for himself. On the other hand (which can be seen only in the films extended version), it is actually their father who wants the Ring and urges Boromir to get it, while Faramir only wants to prove himself to his father. Boyens contends these plot changes were needed to keep the Ring menacing. Wenham commented on the DVD documentaries that he had not read the book prior to reading the script, so the films version of Faramir was the Faramir he knew. When he later read the book and noticed the major difference, he approached the writers about it, and they explained to him that if he did say "I wouldnt pick that thing up even if it lay by the wayside", it would basically strip the One Ring of all corruptive power. 

The meaning of the title itself, The Two Towers, was changed. While Tolkien considered several possible sets of towers  he eventually created a final cover illustration  and wrote a note included at the end of The Fellowship of the Ring which identified them as Minas Morgul and Isengard#Orthanc|Orthanc.  Jacksons film names them as Orthanc and Barad-dûr, symbolic of an evil alliance out to destroy Men that forms the films plot point. The film depicted Saruman openly presenting himself outright as Saurons servant, whereas this association was not explicitly stated in the novel (and indeed analysis by Gandalf and Aragorn in the chapter "The White Rider" stated that there was a rivalry instead, as Saruman was afraid of the prospect of being at war with Sauron, if Rohan and Gondor fell).

==Production==

===Production design===
 
When   suggested a curved wall. Used in the film for longshots, Jackson also used this miniature to plan the battle with 40,000 toy soldiers.   

As a pivotal part of the story, Helms Deep was built at Dry Creek Quarry with the Gate, a ramp, and a wall with a removable section and the tower on a second level. Most importantly, there was the 1:4 scale miniature of Helms Deep that ran 50 feet wide. It was used for forced perspective shots,    as well as the major explosion sequence. 
 special crossbow for the characters, one without the redundancy of opening to reload, the realization of a 15th-century manuscript. Also created were 100 Elven suits of armour, with emphasis on Autumnal colours due to the theme of Elves leaving Middle-earth. 250 suits were made for the Rohirrim. The Rohan designs were based on Germanic and Anglo-Saxon patterns. Most of the weapons were designed by John Howe, and forged by Peter Lyon. Each sword took 3–6 days to make.   

The Rohirrims capital of   in his hair for unnerving effect.
 Blitz or Berlin in 1945.    The set on a backlot was based around a bridge and reused some of Moria. 

===Principal photography===
 , provided the location for Edoras.]]
The Two Towers shared principal photography with The Fellowship of the Ring and The Return of the King between 11 October 1999 to 22 December 2000. Scenes in Rohan were shot early on, and Viggo Mortensen, Orlando Bloom and John Rhys-Davies scale double Brett Beattie sustained many injuries. Mortensen broke two toes when he kicked an Orc helmet upon finding the remains of the Uruk-hai and believing Merry and Pippin to be dead; this take is the one in the finished film. Bloom fell off his horse and cracked three ribs, whilst Beattie dislocated his knee. Because of these injuries the actors suffered two days in pain during the running sequence leading Jackson to jokingly refer to them as "The Walking Wounded." 

Afterwards, they went on for three months filming the Battle of Helms Deep. John Mahaffie handled most of the night shoots. Mortensen got his tooth chipped during the nightshoots, and Bernard Hill also got his ear slashed.  Nonetheless, the 700 extras had fun, insulting each other in Māori language|Māori    and improvising scenes, such as the Uruk-hai stamping their spears before the battle begins.  They did get annoyed by the Art Departments craftsmanship: the Gates were too reinforced for the Battering Ram scene.  Mortensen greatly respected the stunt team, and head butting them became a sign of that respect. 

Wood and Astin were joined by Serkis on 13 April 2000. 

===Special effects===
For The Two Towers,  s Cave Troll and Balrog. 

====Gollum====
 
Weta began animating Gollum in late 1998 to convince New Line they could achieve the effect. Andy Serkis "played" Gollum by providing his voice and movements on set, as well as performing within the motion capture suit later on. His scenes were filmed twice, with and without him. Originally, Gollum was set to solely be a CG character, but Jackson was so impressed by Serkis audition tape that they used him on set as well.
 rotoscoped and animated on top of these scenes. They fully animated some shots such as him crawling upside down. Serkis motion capture animated the body while animators did the head. Gino Acevedo supervised realistic skin tones, which took four hours per frame to Rendering (computer graphics)|render. 
 split personality. The two personas—the childlike Smeágol and the evil Gollum—are established during a scene in which they argue over remaining loyal to Frodo. The two personalities talk to each other, as established by contrasting camera angles and by Serkis altering his voice and physicality for each persona.

====Treebeard==== urethane moulds Billy Boyd sat on bicycle seats concealed into Treebeards hands to avoid discomfort and were left alone on set sitting in the puppets hands during breaks. The puppet was shot against Chroma key|bluescreen. 

===Score===
 
  musical score for The Two Towers was composed, orchestrated, and conducted by Howard Shore, who also composed the music for the other two films in the series. While the scores for its predecessor and sequel won the Academy Award for Best Score, the soundtrack for The Two Towers was not nominated. (Initially there was confusion over the scores eligibility due to a new rule applying to sequels, but the Academy did declare it eligible. )

The funeral song Éowyn sings during her cousin Théodreds entombment in the extended edition is styled to be a traditional song of the Rohirrim, and has lyrics in their language, Rohirric (represented by  Old English). The song does not appear in the book, and the tune is a variation upon a theme of the rímur Icelandic folk tradition; it can be heard as part of track 7 in the 1999 recording of a musical version of the Edda by Sequentia (music group)|Sequentia. 

The soundtrack was recorded at Abbey Road Studios. The soundtrack has a picture of Peter Jackson (barefoot), the composer, and two producers crossing Abbey Road, referencing The Beatles album of the same name.

==Reception==

===Box office===
The Two Towers opened in theaters on December 18, 2002. It made $64.2 million in its opening weekend in the U.S. and Canada. The movie went on to gross $342,551,365 in North America and $583,495,746 internationally for a worldwide total of $926,047,111 against a budget of $94 million.  It was the highest grossing film of 2002 worldwide. 

===Critical response=== The Battle of Helms Deep has been named as one of the greatest screen battles of all time,  while Gollum was named as the third favorite computer-generated film character by Entertainment Weekly in 2007. 

In CinemaScore polls conducted during the opening weekend, cinema audiences gave the film an average grade of "A" on an A+ to F scale. 

==Accolades==
 
* Academy Awards    Best Visual Best Sound Editing. Best Picture, Best Art Best Film Best Sound Michael Hedges and Hammond Peek).
* British Academy Film Awards: Best Costume Design, Best Special Visual Effects, Orange Film of the Year (voted on by the public)
* Empire Awards: Best Picture
*   (Howard Shore)
* Hugo Awards (World Science Fiction Society): Best Dramatic Presentation&nbsp;— Long Form
* 2003 MTV Movie Awards: Best virtual performance (Gollum)
* Saturn Awards: Best Fantasy Film, Best Costume (Ngila Dickson), Best Supporting Actor (Andy Serkis)

===American Film Institute Recognition===

*AFIs 100 Years...100 Movie Quotes:
**"My precious." – #85
*AFIs 100 Years...100 Movies (10th Anniversary Edition) – Nominated

==Home media==

;VHS and DVD
The Two Towers was released on VHS and DVD on 26 August 2003 in the United States. The date was originally intended to be a simultaneous worldwide release, but due to a Bank holiday weekend in the United Kingdom, some British stores began selling DVDs early as 22 August much to the ire of the films U.K. distributor, which had threatened to withhold advance supplies of subsequent DVD releases. 

As with The Fellowship of the Ring, an extended edition of The Two Towers was released on VHS and DVD on 18 November 2003 with 45 minutes of new material, added special effects and music, plus 11 minutes of fan-club credits. The runtime expanded to 223 minutes. The 4-disc DVD set included four commentaries along with hours of supplementary material.

On 29 August 2006, a limited edition of The Two Towers was released on DVD. The set included both the films theatrical and extended editions on a DVD#Capacity|double-sided disc along with all-new bonus material.

;Blu-ray edition
The theatrical Blu-ray version of The Lord of the Rings was released in the United States on 6 April 2010.  The individual Blu-ray disc of The Two Towers was released on 14 September 2010 with the same special features as the complete trilogy release, except there was no digital copy. 

The extended editions for Blu-ray were released in the U.S. and Canada on 28 June 2011.  This version has a runtime of 235 minutes. 

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  
*  

 
 
 
 
{{Navboxes | title = Awards for The Lord of the Rings: The Two Towers| list =
 
 
 
 
 
}}

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 