The Passion Flower
 
{{Infobox film
| name           = The Passion Flower
| image          = The Passion Flower (1921).jpg
| caption        = Film still
| director       = Herbert Brenon
| producer       = Norma Talmadge
| writer         = Herbert Brenon Mary Murillo
| story          = 
| based on       =  
| narrator       = 
| starring       = Norma Talmadge Courtenay Foote Eulalie Jensen 
| music          = 
| cinematography = 
| editing        = 
| studio         = Norma Talmadge Film Corporation
| distributor    = Associated First National Pictures
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          = 
}}
The Passion Flower is a 1921 American drama film starring Norma Talmadge, Courtenay Foote and Eulalie Jensen, and directed by Herbert Brenon. The forbidden love of a man for his stepdaughter leads to tragedy and murder.

The Library of Congress has a print,  though there is a bit of deterioration in the first scene and a "lapse of Continuity (fiction)|continuity" near the end of this copy. 

==Plot==
As described in a film publication,    Estebans (Foote) jealousy for his stepdaughter Acacia (Talmadge) results in his servant Rubio (Wilson) telling Acacias sweetheart Norbert (Ford) that she loves another. Their betrothal is broken, and later Acacia accepts Faustino (Agnew). Rubio kills Faustino, and Norbert is tried for the crime but acquitted. When it becomes known that Esteban was the cause of the murder, he flees into the mountains, but later returns to give himself up. Raimunda (Jensen), Acacias mother and Estebans wife, pleads with Acacia to accept the stepfather whom she hates. During the long embrace which follows between Esteban and Acacia, Raimunda learns of Estebans love for his stepdaughter and her own love turns to hate. Raimunda calls for help and during Estebans attempt to escape with Acacia he shoots his wife and is then arrested. Raimunda dies in the arms of Acacia.

==Cast==
*Norma Talmadge as Acacia, The Passion Flower
*Courtenay Foote as Esteban
*Eulalie Jensen as Raimunda Harrison Ford as Norbert
*Charles A. Stevenson as Tio Eusebio
*Alice May as Julia Eusebio
*H. D. McClellan as a Eusebio son
*Austin Harrison as a Eusebio son
*Herbert Vance as a Eusebio son
*Robert Agnew as Faustino Eusebio
*Harold Stern as Little Carlos
*Natalie Talmadge as Milagros
*Mrs. Jacques Martin as Old Juliana
*Elsa Fredericks as Francesca
*Robert Paton Gibbs as Norberts father (as Robert Payton Gibb)
*Augustus Balfour as The Padre
*Walter Wilson as Rubio
*Mildred Adams as Doña Isabel
*Julian Greer as Acacias father
*Edward Boring as Bernabe

==References==
 

==External links==
 
* 
* 
* 

 
 
 
 
 
 
 


 
 