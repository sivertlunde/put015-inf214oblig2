Tyrant Fear
{{Infobox film
| name           = Tyrant Fear
| image          = 
| alt            = 
| caption        =
| director       = Roy William Neill
| producer       = Thomas H. Ince
| screenplay     = R. Cecil Smith 
| starring       = Dorothy Dalton Thurston Hall Melbourne MacDowell William Conklin Lou Salter Carmen Phillips
| music          = 
| cinematography = John Stumar
| editing        = 
| studio         = Thomas H. Ince Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Roy William Neill and written by R. Cecil Smith. The film stars Dorothy Dalton, Thurston Hall, Melbourne MacDowell, William Conklin, Lou Salter and Carmen Phillips. The film was released on April 29, 1918, by Paramount Pictures.  

==Plot==
 

==Cast== 
*Dorothy Dalton as Allaine Grandet
*Thurston Hall as Harley Dane
*Melbourne MacDowell as James Dermot
*William Conklin as Jules Latour
*Lou Salter as Theodore De Coppee
*Carmen Phillips as Marie Courtot 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 