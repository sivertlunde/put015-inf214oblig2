Maqbool
 
 
{{Infobox film
| name           = Maqbool
| image          = Maqbool DVD Cover.jpg
| caption        = DVD Cover
| director       = Vishal Bhardwaj
| producer       = Bobby Bedi
| story          = Vishal Bhardwaj Abbas Tyrewala
| based on       = Macbeth by  William Shakespeare
| screenplay     = Vishal Bhardwaj Abbas Tyrewala Irfan Khan Tabu Pankaj Kapoor Om Puri Naseeruddin Shah
| music          = Vishal Bhardwaj
| cinematography = Hemant Chaturvedi
| editing        = Aarif Sheikh
| distributor    = Kaleidoscope Entertainment Pvt. Ltd.
| released       =  
| runtime        = 132 minutes
| country        = India
| language       = Hindi Urdu
| budget         =
}} Irfan Khan, Tabu and Masumeh Makhija in an adaptation of the play Macbeth by Shakespeare.
 Omkara which won him commercial as well as critical success. The critical success of "Omkara" was responsible for winning Francis Ford Coppolas attention.

The film had its North American premiere at the 2003 Toronto International Film Festival. Though the film failed to entice much of an audience during its theatrical run in India, critics were appreciative and Pankaj Kapoor went on to win a Filmfare Award for Best Actor (Critics) and a National Film Award for Best Supporting Actor. The film was screened in the Marché du Film section of the 2004 Cannes Film Festival. 

==Plot== Irfan Khan) underworld don. Maqbool is grateful and feels a close connection and personal indebtedness to Abba Ji. The movie gains pace with two corrupt police-men (played by Om Puri and Naseeruddin Shah) predicting that Maqbool would soon take over the reins of the Mumbai Underworld from Abba Ji.  These two thus play a role akin to the three witches in the original play.
 
Nimmi (Tabu (actress)|Tabu) is Abba Jis mistress, but she secretly loves Maqbool. Maqbool loves Nimmi, too. Nimmi also encourages Maqbools ambitions and persuades him to kill Abba Ji in order to take over as don. Maqbool is torn between his love for Nimmi and his loyalty to Abba Ji, but he begins to prepare the ground for becoming a don by ensuring that others in the line of succession cannot interfere. Finally, in a dramatic scene, Maqbool murders Abba Ji in cold blood while he is in bed at night, with Nimmi next to him.  Maqbool gets away with it and takes over as don, just as planned; but both he and Nimmi are haunted by guilt, seeing Abba Jis ghost and unable to wash the blood from their hands. There is also suspicion, within the gang, of Maqbools role in the death of Abba Ji, and eventually the lovers meet a tragic end.

In addition to the portrayals of the three tragic heroes, the film offers performances by supporting cast members, in particular Om Puri and Naseeruddin Shah, who open the film in their roles as black comic relief corrupt police inspectors-cum-astrologers, who predict the fall of Abba Ji—who has them on his payroll—and the rise and fall of Maqbool. Contrary to the original play, the corrupt cops are not just passive soothsayers. In an effort to sustain what they refer as "balancing forces," they also actively involve in shaping events, like aiding in providing information to Abba Jis enforcers to wipe out a rival gang, using subtle nuances in coercing Maqbool to shift loyalties, deliberately botching an "encounter" attempt on Riyaz Boti (Macduff) and subsequently setting up an alliance between a rival politician (to the incumbent one, backed by Abba Ji) and a fleeing Guddu (Fleance) and Riyaz Boti against Maqbool.

== Cast ==
{|class="wikitable"
|-
!Actor
!Character in the movie
!Character in the play
|- Irfan Khan
| Miyan Maqbool
| Macbeth
|- Tabu
| Nimmi
| Lady Macbeth
|-
| Pankaj Kapoor
| Jahangir Khan (Abbaji)
| Duncan I of Scotland
|-
| Om Puri
| Inspector Pandit Weird Sisters)
|-
| Naseeruddin Shah
| Inspector Purohit Weird Sisters)
|-
| Piyush Mishra
| Kaka
| Banquo
|-
| Ankur Vikal
| Riyaz Boti Macduff
|-
| Ajay Gehi
| Guddu 
| Fleance
|- 
| Masumeh Makhija
| Sameera, Abbajis daughter Malcolm

|-
| Shammi Narang
| Mr. Bhosle
| 
|-
| Pubali Sanyal
| Riyaz Botis wife
| Lady Macduff
|-
| Master Raj
| Riyaz Botis son
| Macduffs son
|-
| Gyanchand Rikki
| Mughal
| Macdonwald
|- Manav Kaushik Asif
|Cawdor
|- Vinod Nahardih Chinna
|
|}

==Reception==
Maqbool received positive reviews from critics. Rediff described the film as "a visual gallery that is an intelligent blend of dark, tragic overtones and comic, satirical undertones".  Variety wrote that while the visuals are great, audiences might need an understanding of Macbeth to fully enjoy the film.  India Today described it as a "haunting operatic tragedy".  Outlook said that it "effectively transported the essence of the story to the milieu of the Bombay underworld of our times". 

==Soundtrack==
{{Infobox album |
  Name        = Maqbool |
  Type        = soundtrack |
  Artist      = Vishal Bhardwaj |
  Cover       = Maqbool_soundtrack.jpg|
  size = 200px|
  Background  = gainsboro |
  Released    = 2004|
  Recorded    = Nirvana Studio |
  Genre       = Film soundtrack |
  Length      =  |
  Label       =  Music Today|
  Producer    =  |
  Reviews     =  |

}}

The soundtrack features eleven songs composed by Vishal Bhardwaj with lyrics by Gulzar (lyricist)|Gulzar.

{{track listing
| title1   = Jhin Min Jhini
| note1    = performed by Sadhana Sargam, Ustad Sultan Khan, Anuradha Sriram, Rakesh Pandit
| length1  = 5:32
| title2   = Ru-Ba-Ru
| note2    = performed by Daler Mehndi,  Rakesh Pandit, Sabir Khan, Dominique
| length2  = 5:53
| title3   = Rone Do
| note3    = performed by Rekha Bhardwaj
| length3  = 5:02
| title4   = Dheemo Re
| note4    = performed by Ustad Sultan Khan
| length4  = 3:18
| title5   = Maqbool Theme
| note5    = instrumental
| length5  = 1:34
| title6   = Rukhe Naina
| note6    = performed by Sanjeev Abhyankar
| length6  = 5:14
| title7   = Chingari
| note7    = performed by Rekha Bhardwaj
| length7  = 4:26
| title8   = Killing
| note8    = instrumental
| length8  = 1:36
| title9   = Nirvana
| note9    = instrumental
| length9  = 1:53
| title10   = Shoonya
| note10   = instrumental
| length10  = 1:58
| title11   = Jhin Min Jhini (extended)
| note11    = performed by Sadhana Sargam, Ustad Sultan Khan, Anuradha Sriram, Rakesh Pandit
| length11  = 6:24
}}

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 