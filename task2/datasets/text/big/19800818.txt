Seksdagesløbet
{{Infobox film
| name           = Seksdagesløbet
| image          =
| image size     =
| caption        =
| director       = Jørgen Roos
| producer       =
| writer         = Erik Balling Tørk Haxthausen
| narrator       =
| starring       = Poul Reichhardt
| music          =
| cinematography = Poul Pedersen
| editing        =
| distributor    = Nordisk Film
| released       = 18 August 1958
| runtime        = 97 minutes
| country        = Denmark
| language       = Danish
| budget         =
| preceded by    =
| followed by    =
}}

Seksdagesløbet is a 1958 Danish drama film directed by Jørgen Roos and starring Poul Reichhardt.

==Cast==
* Poul Reichhardt - Ivan Reimer
* Lily Broberg - Lillian
* Flemming Larsen - Kristian Reimer
* Jørgen Reenberg - Otto Bertelsen
* Preben Kaas - Benny Brun
* Nino Orsini - Bandini
* Ole Larsen - Torkild
* Paul Hagen - Søren Kristensen
* Bent Christensen - Kurt
* Kjeld Petersen - Larsen
* Judy Gringer - Tove
* Vivi Bach - Marianne (as Vivi Bak)
* Henny Lindorff Buckhøj - Karla
* Valsø Holm - Karlas mand
* Ulrik Neumann - Tilskuer
* Kai Holm - Proprietær
* Jørn Jeppesen - Arrangøren
* Knud Schrøder - Banelægen
* Mogens Brandt - Journalist Sivertsen
* Ejner Federspiel - Tilskuer
* Ellen Margrethe Stein - Tilskuer
* Mimi Heinrich - Yrsa
* Jørgen Buckhøj - Rockeren Erik
* Guglielmo Inglese - Peppo
* Frank Holms - Müller
* Gustl Weishappel - Schmidt
* Roger Maridia - Lecourt
* Louis Viret - Viret
* Michel Hildesheim - Billetsjoveren
* Klaus Nielsen - Knud
* Birgit Zacho - Pige med laktaske
* Erik Gutheil - Speaker
* Peter Kitter - Radiospeaker
* Jørgen Beck - Løbskommissær
* Jytte Grathwohl - En mor
* Grethe Hoffenblad

==External links==
* 

 
 
 
 
 
 
 
 