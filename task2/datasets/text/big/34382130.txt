Greetings from Tim Buckley
{{Infobox film
| name           = Greetings from Tim Buckley
| image          = Greetings from Tim Buckley poster.jpg
| caption        =
| director       = Daniel Algrant 
| producer       = Patrick Milling Smith Fred Zollo John N. Hart Amy Nauiokas
| writer         = Daniel Algrant Emma Sheanshang David Brendel
| starring       = Penn Badgley Imogen Poots
| music          = 
| cinematography = Andrij Parekh
| editing        = Bill Pankow
| studio         = Tribeca Film
| distributor    = Focus World
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $11,157 
}}
Greetings from Tim Buckley is a 2012 American film directed by Daniel Algrant starring Penn Badgley and Imogen Poots. The film follows the journey Jeff Buckley took in grappling with the legacy of his late musician father, Tim Buckley|Tim, leading up to and culminating with his legendary 1991 performance of his fathers songs.    The film premiered at the 2012 Toronto International Film Festival on September 9, 2012 to generally positive reviews.

==Plot==
In 1991, a young Jeff Buckley rehearses for his public singing debut at a Brooklyn tribute show for his father, the late folk singer Tim Buckley. Struggling with the legacy of a man he barely knew, Jeff forms a friendship with an enigmatic young woman Allie, working at the show and begins to discover the powerful potential of his own musical voice. Greetings from Tim Buckley is filled with stirring musical performances and the memorable songs of a father and son who were each among the most beloved singer/songwriters of their respective generations. 

==Cast==
*Penn Badgley as Jeff Buckley
*Ben Rosenfield as Tim Buckley    
*Imogen Poots as Allie, a young woman who meets Jeff Buckley while working at a concert.  Frank Wood as Gary Lucas  
*Norbert Leo Butz as Hal Willner 
*Frank Bello as Richard Hell      William Sadler as Lee Underwood 
*Kate Nash as Carol 
*Jessica Stone as Janine Nichols 

==Production==
  Avy Kauffman is the casting director. 

Music used in the film comes from the Tim Buckley estate, except Leonard Cohens "Hallelujah (Leonard Cohen song)|Hallelujah," which Jeff Buckley covered on his only studio album, the 1994 Grace (Jeff Buckley album)|Grace.   Jeffs character and Gary Lucas also jam on the instrumental of "Grace" as a song idea that Gary had. Singer-songwriter Jann Klose sings and plays guitar on the Tim Buckley songs, "Song For Janie," "Pleasant Street," and "Once I Was."  

Badgley was cast after he sent a tape of him singing.    The audition tape that he sent included an interpretation of the Led Zeppelin III album, which also appears in the film.  For the role, Badgley lost some weight: "I remember in my head thinking, Im never going to be able to get as thin as him, so Im just going to stop working out. Im just going to stop getting in the sun. Im just going to try to get as thin and pale as possible."  He also took guitar and vocal lessons.  The vocal coach taught him how to warm up and warm down his voice and the guitar teacher taught him scales and progressions.  Serving as a consultant, songwriter and former collaborator of Jeff Buckley,  Gary Lucas played and rehearsed with Badgley.  Badgley performed all his singing scenes live. 

Pictures of the set surfaced on August 22, 2011  while the first official images were released on August 14, 2012, with additional ones on August 24, 2012.  

Algrant explained: "The concert is true – the rest is fictionalized and conjecture. I really tried to be as emotionally honest as I could be, as opposed to having to worry about truth." 

==Release==
Celluloid Dreams obtained the international rights in January 2012.  Focus Film and Tribeca acquired the U.S. rights in December 2012. 

Greetings from Tim Buckley had its world premiere at the 2012 Toronto International Film Festival on September 9, 2012.  The film was also screened at the Tribeca Film Festival on April 23, 2013  and had a limited release in theatres on May 3, 2013.

===Reception===
Upon its premiere at the 2012 Toronto International Film Festival, Deborah Young of The Hollywood Reporter said it was a "sensitive, well-cast film about father-son musicians Tim and Jeff Buckley gets the emotions and music just right". Young also praised lead actor Badgley for his "vibrant break-out performance" noting his "seductive energy" and Poots pointing out her "strong screen presence".  Rolling Stone was also positive of Badgleys portrayal writing he "does an impressive turn as Jeff."  The Globe and Mail placed Greetings from Tim Buckley among their top six favorite movies of the festival.  Variety (magazine)|Variety  Dennis Harvey was critical of the narration deeming it "scant". He described Jeff as "a fine illustration of how good, even great art can be made by exasperating personalities that only a groupie (or biographer) could love" and concluded: "The result is at once skillfully observed and a bit so-what." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 