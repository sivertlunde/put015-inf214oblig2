Annamalai (film)
 
 

{{Infobox film name           = Annamalai image          = Annamalai rajini.jpg caption        = Poster director  Suresh Krishna producer       = Pushpa Kandaswamy writer  Suresh Krishna Shanmuga Sundaram starring  Kushboo Sarath Manorama Janagaraj Nizhalgal Ravi music  Deva
|cinematography = P. S. Prakash editing        = Ganesh Kumar studio         = Kavithalayaa Productions distributor    = Kavithalayaa Productions released       = 26 June 1992 runtime        = country        = India awards         = language  Tamil
|budget         = gross          = 
}}
 Tamil film Suresh Krishna, Kushboo and Kane and Telugu in Suman and Gokarna starring Upendra, Rakshita and Madhu Bangarappa. 

==Plot==
Annamalai (Rajinikanth) and Ashok (Sarath Babu) have been best friends since childhood. Annamalai is a milkman while Ashok is the only son of a rich businessman (Radharavi). When Ashok falls in love with Geetha (Rekha), a poor girl, his father is naturally against the wedding. Annamalai holds the wedding and earns his ire. Meanwhile, Annamalai falls for Subu (Kushboo Sundar), whom he subsequently marries.

Ashoks father, plotting to drive a wedge between his son and Annamalai, hints that he would like to build a hotel on Annamalais land. Annamalai gives the land to Ashok without even accepting money for it. When Ashoks father later talks about bringing down his house, he slaps Ashoks father in anger.  Ashok orders Annamalais house to be bulldozed which drives Annamalai to challenge that he would become a bigger and more successful hotelier than his friend. Annamalai, with his hard work and honesty, becomes the biggest businessman in the city.

Annamalais daughter and Ashoks son study in the same college and they fall in love. Annamalai is against it because of the hatred that he has for Ashok. During an auction, Annamalai tricks Ashok to buy a plot for 120&nbsp;million which not worth more than 2. Ashoks house has to be sold for the debt of 120&nbsp;million and Annamalai buys it. With all goodness in heart and the old friendship in mind, Annamalai gives the house to Ashok. Annamalais mother (Manorama (Tamil actress)|Manorama) returns the house documents to Ashok and his father pleads for pardon. Meanwhile, Sargunam (Nizhalgal Ravi) plans to kill Ashok for money and hires an assassin (Tiger Prabhakar). Annamalai saves him and they become friends again. They plan to get their children married to each other, and "They live happily ever after."

==Cast== Rajni as Annamalai
*Sarath Babu as Ashok
*Kushboo Sundar as Subu aka subbalakshmi
*Janagaraj as Panchachalam (Panchu)
*Nizhalgal Ravi as Sarkunam
*Tiger Prabhakar
*Vinu Chakravarthy as S. Eakaamparam (MLA and minister) Manorama as Annamalai`s mother Vaishnavi as Annamalais Sister Rekha as Geetha Karan as Ashoks son
*V. Subramanian as The Minister
*Vishnukanth as Junior Ashok
*Dakshayini as Annamalais daughter

==Production==

Originally, Kavithalaya Productions had chosen director Vasanth as the director of Annamalai, but he opted out due to "personal reasons". Suresh Krissna was later finalized for the directors part, after veteran director K. Balachandar had requested him to do so.  http://cinemalead.com/visitor-column-id-the-journey-of-living-legend-rajinikanth-part-5-rajinikanth-15-09-133375.htm  

When Suresh Krissna came on board the script wasnt complete and the shooting started in a hurry as the film release date had already been fixed as on June 27, 1992.  Suresh recalled that Shanmugasundaram, the films dialogue writer narrated him the one liner of the plot, Suresh felt that detailing should be done to the script to which Shanmugasundaram assured that it can be done during the shoot. 

When the title of the film was announced as Annamalai, few people has misgivings about the title as it would create a negative connation. Balachander and Rajinikanth had decided on the title and refused to change it. Annamalai was launched at AVM Studios at the venue of Pillaiyar temple.  The films Muhurat shot was held in a set of a court room, it was a comedy scene where Rajini would save Sarathbabu from a criminal offence. Suresh removed the scene from the film as he felt that this scene had no bearing on the script. 

Radharavi revealed that he wasnt interested to play elderly characters but agreed to appear in Annamalai after Rajini requested him to act. 

The scene where Rajini enters into a ladies hostel and gets terrified by a snake was shot at Chakra House, Alwarpet. A snake was brought specially for the scene. Owner of the snake earlier worked in previous films of Rajini. Suresh asked his assistants Nagappan and K. Natraj to check the details about snake charmer. Suresh recalled that he covered his nervousness with his smile.  Suresh told Prakash, the films cinematographer to use zoom lens so that he could adjust a camera according to the situation.  After the scene was finished, the members of the crew enjoyed the shot. Suresh congratulated Rajini and asked him how "did he performed so well and was it planned ?", For which Rajini answered it was not planned and expressions in the scene were a result of his fear.  In the same scene, Shanmugasundaram came with initial dialogues to convey the fear of Rajini. Suresh felt that dialogues were reduntant in such situation.  Rajini insisted to keep Kadavule Kadavule (Oh God! Oh God!) as the dialogue. Suresh agreed to keep the dialogue as it went well into the situation of the scene. 

Suresh wanted the picturisation of "Vanthenda Paalkaaran" to be vibrant and colourful stating that he got inspired from the Hindi songs of Amitabh Bachchan. The scene where Rajini shows his face to the camera was extended in slowmotion by Suresh as fans will feel as if he looks towards the audience. The song was choreographed by Prabhu Deva.  The title song where Rajini and Kushboo appeared in a periodic costumes was shot at Fern Hill Palace Hotel while the matching shots were shot at Sivaji Gardens. Suresh shot the song sequence in a technique where the lip sync is perfect while the dance movements being fast. 

Suresh Krissna was responsible for the words SUPERSTAR forming in blue dots on the screen followed by the words RAJNI, which became set standard for all future Rajini films. Annamalai set another benchmark of introducing Rajinis character through introduction songs. 

==Soundtrack==
{{Infobox album |  
| Name        = Annamalai |
| Type        = soundtrack | Deva |
| Cover       = |
| Released    = 1992|
| Recorded    = | Feature film soundtrack |
| Length      = |
| Label       = Lahari Music Deva |
|  Last album  = Vaigasi Poranthachu  (1991)
|  This album  = Annamalai  (1992)
|  Next album  = Suriyan  (1992)
}}

The music of the film has been composed by Deva (music director)|Deva, with the lyrics penned by Vairamuthu.  Annamalai marked Devas first collabaration with Rajini. KB chosen Deva as a composer due to a misunderstanding with Ilaiyaraja. Rajini and Suresh were initially worried with the choice of Deva.  When Suresh met Deva, he had already composed the song "Annamalai Annamalai" when Vasanth was initially the director of the film. 

The song "Vanthenda Paalkaaran" is said to be inspired from a Kannada poem about cow which Rajini had suggested, the poem talked about the cow which is a deity and useful to the human. Vairamuthu grasped the core of the poem and added the details of cow being helpful and also added image boosting lyrics in the song to cater his fans. 

The dialogues from the film was used in the song "Rathiri", composed by Santhosh Narayanan for Pizza (2012 film)|Pizza (2012). 

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
! No. !! Song !! Singers !! Duration
|-
| 1|| "Annamalai Annamalai" || S. P. Balasubrahmanyam, K.S. Chitra || 03:54
|-
| 2|| "Kondayil Thaazham Poo" || S. P. Balasubrahmanyam, K.S. Chitra || 04:46
|-
| 3|| "Oru Pen Pura" || K.J. Yesudas || 04:50
|-
| 4|| "Rekkai Katti Parakudhu" || S. P. Balasubrahmanyam, K.S. Chitra || 04:53
|-
| 5|| "Vanthenda Paalkaran" || S. P. Balasubrahmanyam || 04:23
|-
| 6|| "Vetri Nichayam" || S. P. Balasubrahmanyam || 01:44
|}

==Reception==
During the time of shooting, political scenario wasnt quite favourable to Rajini. The new government of Jayalalitha implemented a new rule where posters of films were prohibited in the city. Luckily, the lack of promos only increased the hype and worked greatly to the films advantage. 

The film was a blockbuster and completed a 175-day run at the box office. The film is rumored to have grossed about   150&nbsp;million and was the highest grossing film in Rajinikanths career until 1995 when the record was broken by Baasha (film)|Baasha.  he collections of Annamalai sent tremors everywhere. Annamalai exceeded normal regional collections making the film producers all over India sit up and take notice. With Annamalai, Rajinis range increased and made him the highest paid actor in India till now. Tamil Manila Congress and DMK ally prioritized Annamalai Cycle, and won the 1996 elections by a huge margin.  

==Legacy== Jai stated that his character from Engeyum Eppodhum (2011) "was almost modelled on the lines of Rajini Sir in the first half of Annamalai".  Suresh Krishna directed Aarumugam in 2009 which critics compared with Annamalai due to its similar plot and characterisations. 

The quote from the film that became popular were: "Naan Solradaiyum Seiven, Soladadeiyum Seiven" (English: I’ll do what I say, I’ll also do what I don’t say).  The scene where Rajini challenges Sarath Babu was included by Behindwoods.com in its list of "Top 20 Mass Scenes". 

==In popular culture==
The scenes from the film were parodied in Parthale Paravasam (2001),  Boss Engira Bhaskaran (2010),  Karuppasamy Kuthagaithaarar (2007) and Thamizh Padam (2010). 

==References==
 

==Bibliography==
 
*  
*  
 

==External links==
 
*  

 
 

 
 
 
 
 
 
 
 