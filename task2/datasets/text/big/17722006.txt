Große Freiheit Nr. 7
{{Infobox film
| name           = Große Freiheit Nr. 7
| image          = Grfreiheitnr7poster.jpg
| image_size     = 190px
| caption        = Theatrical release poster
| director       = Helmut Käutner
| producer       = Hans Tost
| writer         = Helmut Käutner and Richard Nicolas
| narrator       =
| starring       = Hans Albers
| music          = Werner Eisbrenner
| cinematography = Werner Krien
| editing        = Anneliese Schönnenbeck
| studio         =
| distributor    = Deutsche Filmvertriebs (DFV)
| released       =  
| runtime        = 111 minutes
| country        = Nazi Germany
| language       = German
| budget         =
| gross          =
}}
 musical drama film directed by Helmut Käutner. It was named after Große Freiheit (grand freedom), a street next to Hamburgs Reeperbahn road in the St. Pauli red light district.

The film is also known as Port of Freedom in the United Kingdom.

== Plot summary ==
 
The film tells the story of the blond "singing sailor" Hannes Kröger (played by  . But she prefers his friend Willem (Hans Söhnker) and Hannes returns to the sea.

== Cast ==
*Hans Albers as Hannes Kroeger
*Ilse Werner as Gisa Häuptlein
*Hans Söhnker as Willem
*Hilde Hildebrand as Anita
*Gustav Knuth as Fiete
*Günther Lüders as Jens
*Ilse Fürstenberg as Gisas mother
*Ethel Reschke as Margot
*Erna Sellmer as Frau Kaasbohm
*Kurt Wieschala as Jan
*Helmut Käutner as Karl
*Richard Nicolas as Admiral
*Maria Besendahl as Frau Boergel
*Justus Ott as Herr Wellenkamp
*Gottlieb Reeck as Herr Puhlmann
*Thea Thiele as Consuls wife
*Alfred Braun as Rundfunkreporter
*Rudolf Koch-Riehl as Master of ceremonies
*Karl-Heinz Peters as Postman
*Erwin Loraino as Sailor

== Soundtrack ==
* Hans Albers – "Auf der Reeperbahn"
* Hilde Hildebrand – "Beim ersten Mal, da tuts noch weh"
* Hans Albers – "La Paloma"
* Hans Albers – "Nein, ich kann Dich nicht vergessen"
* Hans Albers – "Schön ist die Liebe im Hafen"
* Hans Albers – "Was kann es denn schöneres geben"
* Hans Albers – "Wenn ein Seemann mal nach Hamburg kommt"

== Production == Allied bombing Ufa studios in Berlins  Neubabelsberg and Tempelhof when it was made in 1943 (May to November), most of the movie was shot in Pragues Barrandov Studios by Helmut Käutner, as the first Agfa colorfilm by Terra. For a scene with a boat trip in Hamburg harbour warships had to be covered up.   

== Reception ==
Nazi propaganda minister Joseph Goebbels was dissatisfied, and demanded many changes to make the film more "German", for instance by renaming the lead role from Jonny (as in Albers earlier hit song "Good bye, Jonny") to Hannes. After a year of editing, the movie was banned anyway in Nazi Germany on 12 December 1944, Also wird die "Große Freiheit Nr. 7" für den Rest des Krieges verboten.
–     and was only shown outside of the Nazi Germany|Großdeutsches Reich proper, with the premiere on 15 December 1944 in occupied Prague (then a Reichsprotektorat). It remained banned in Nazi Germany, opening on 6 September 1945 in Berlins Filmbühne Wien after the Allied victory.

== References ==
 
* Rüdiger Bloemeke: "La Paloma – Das Jahrhundertlied". 158 Seiten, über 30 Seiten Farb- und Schwarzweiß-Abbildungen. Voodoo-Verlag, Hamburg 2005

== External links ==
* 
* 
* 
*http://www.der-blonde-hans.de/albers10a.htm

 

 

 
 
 
 
 
 
 
 
 
 
 
 