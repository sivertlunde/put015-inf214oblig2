Hum Tum Aur Ghost
 
{{Infobox film
| name           = Hum Tum aur Ghost
| image          = Hum Tum Aur Ghost (poster).jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Kabeer Kaushik Maria Goretti
| screenplay     = Arshad Warsi Anand Kumar
| starring       = Arshad Warsi Dia Mirza Boman Irani Sandhya Mridul
| music          = Shankar-Ehsaan-Loy
| cinematography = Ashok Mehta
| editing        = Steven H. Bernard
| studio         = 
| distributor    = Shooting Star Films Viacom 18 Motion Pictures
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
|
}}
Hum Tum Aur Ghost (earlier titled as Kaun Bola?! ) is an Bollywood supernatural comedy-drama film directed by Kabeer Kaushik and produced by Arshad Warsi, starring Arshad Warsi and Dia Mirza in the lead roles. This unofficial remake of Ghost Town (film) was Filmed in Newcastle upon Tyne, England, the film was released on 26 March 2010.
The film was also inspired from the Hollywood film, The Sixth Sense.

==Plot==

In the world of Armaan (Arshad Warsi) and his girlfriend Gehna (Dia Mirza), life is truly beautiful, yet its like walking on a tightrope. For Armaan, a debonair fashion photographer who is a charmer to the core and loved by all around him, life only gets better when he dates Gehna, a high-profile fashion magazine editor. Life is picture perfect and mash; a doting girlfriend and a job where his expertise makes him the most-wanted photographer in the London fashion world! Armaan has learnt that his chronic insomnia is, however, not a function of any sleeping disorder. The truth is that he hears voices; voices that torture him; voices that are disturbing him. More importantly, voices that nobody else can hear! Life is less than picture perfect now!

While his friends sympathize with his problems, his girlfriend Gehna is irritated with his weird behavior. Add to that her father Sinha (Javed Sheikh) constantly berates him for his fondness for the bottle. No one seems to understand his predicament. What puzzles them is that he talks to himself… or, rather, he talks to people who no one can see simply because they dont live. Soon, Armaan becomes aware of his special ability to connect with the souls that havent crossed over. Equipped with a will to fulfill the wishes of these spirits who hound him, Armaan sets out on a mission to help out two souls &mdash; an old man, Mr. Virender Kapoor (Boman Irani), and a young woman, Carol (Zehra Naqvi). In this ensuing journey, Armaan discovers the lives of his two special companions and ends up frustrating Gehna. Yet, Armaan is on a journey where he discovers a lot about his own self and his own life. 

After he fulfils Kapoors wish, he goes to find Carols son Danny. He realizes that he is Danny but, when he tries to explain this to Gehna, she gets fed up and leaves him. She is then involved in a car crash and sadly dies. When Armaan finds out, he sees her as a ghost, and once he pleads to God, Gehna comes back to life. The two then get married and, straight after the marriage, Armaans problem of seeing/hearing ghosts is freed.

==Cast==
* Arshad Warsi as Armaan Suri
* Diya Mirza as Gehna Sinha
* Sandhya Mridul as Mini
* Boman Irani as Virendra Kapoor
* Zehra Naqvi as Carol
* Shernaz Patel as Doctor Taniyah
* Ashwin Mushran as Aditya Kapoor
* Asawari Joshi as Mrs. Kapoor
* Javed Sheikh as Mr. Sinha
* Tinnu Anand as Banker

==Reception==
===Critical response===
Hum Tum Aur Ghost received generally mixed reviews from critics, though mostly negative. Mayank Shekhar of the Hindustan Times rated the film 1.5 out of 5, and said that "Playing a girl-magnet, designer-wear, slick hair NRI hero in an artificial, romantic setup, just pales his coolness no end. But then again, ambition is such a b****."  Taran Adarsh of Bollywood Hungama, who rated it 2.5/5, stated "Hum Tum Aur Ghost is a terrible waste of a terrific idea. Disappointing!" 

===Box office===
Hum Tum Aur Ghost had a below average opening and collected only Rs. 4.90 crore during its theatrical run. Eventually, it was elevated to disaster status by Box Office India. 

==Soundtrack==
{{Infobox album 
| Name = Hum Tum Aur Ghost
| Type = soundtrack
| Artist = Shankar-Ehsaan-Loy
| Cover =
| Released =  
 | Recorded =
| Genre = Film soundtrack
| Length =
| Label = T-Series
| Producer = Shankar-Ehsaan-Loy
| Last album = Karthik Calling Karthik (2009)
| This album = Hum Tum Aur Ghost (2010)
| Next album = Housefull (2010 film)|Housefull (2010)
}}

The music is composed by Shankar-Ehsaan-Loy,with lyrics by Javed Akhtar.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s) || Duration
|-
|Dekho Raste Mein KK & Shreya Ghosal
| 5:09
|-
|Kal Tum The Yahan
| Caralisa Monteiro, Shankar Mahadevan, Dominique Cerejo & Clinton Cerejo
| 4:44
|-
| Hum Tum Aur Ghost
| Shankar Mahadevan, Vishal Dadlani & Arun Ingle
| 4:09
|-
|Banware Se Pooche Banwariya Maria Goretti, Loy Mendonsa, Raman Mahadevan, Anusha Mani & Rahul Saxena
| 4:38
|-
|Dekho Raste Mein (Remix) KK & Shreya Ghosal
| 4:46
|-
| Hum Tum Aur Ghost (Remix)
| Shankar Mahadevan, Vishal Dadlani & Arun Ingle
| 2:52
|-
|Kal Tum The Yahan (Remix)
| Caralisa Monteiro & Shankar Mahadevan
| 4:43
|}

==References==
 

==External links==
* 
*  

 
 
 
 
 