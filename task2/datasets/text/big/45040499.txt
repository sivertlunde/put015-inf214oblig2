CID 72
{{Infobox film
| name           = CID 72
| image          =
| caption        = 
| director       = K. S. L. Swamy
| producer       = D R Naidu
| writer         = 
| screenplay     =  Rajesh B. V. Radha
| music          = Vijaya Bhaskar
| cinematography = P S Prakash
| editing        = 
| studio         = 
| distrubutor    = 
| released       =  
| country        = India Kannada
}}
 1973 Cinema Indian Kannada Kannada film, Rajesh and B. V. Radha in lead roles. The film had musical score by Vijaya Bhaskar.   

==Cast==
*Srinath
*Rajasree Rajesh
*B. V. Radha
*Thoogudeepa Srinivas
*Dwarakish

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || Chi. Udaya Shankar || 03.11
|}

==References==
 

==External links==

 

 
 
 
 


 