To Oblige a Lady
{{Infobox film
| name           = To Oblige a Lady
| image          = 
| image_size     = 
| caption        = 
| director       = H. Manning Hayes
| producer       = 
| writer         = Edgar Wallace
| starring       = Masie Gay   Warwick Ward   Lilian Oldland   Haddon Mason   James Carew
| distributor    = British Lion Film Corporation
| released       = 1931
| runtime        = 70 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} British comedy film directed by H. Manning Haynes and starring Maisie Gay, Warwick Ward, Lilian Oldland, Haddon Mason and James Carew.  A couple rent a luxury flat and try to pass it off as their own in order to impress a wealthy relative.  The film is based on a play by Edgar Wallace.

==Cast==
* Masie Gay - Mrs Harris
* Warwick Ward - George Pinder
* Lilian Oldland - Betty Pinder
* Haddon Mason - John Pendergast
* James Carew - Sir Henry Markham
* Annie Esmond - Mrs Higgins

==References==
 

 
 

 
 
 
 
 
 


 