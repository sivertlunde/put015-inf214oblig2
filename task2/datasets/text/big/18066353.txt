The Fossil
{{Infobox film
| name           = The Fossil
| image          = 
| caption        = 
| film name = {{Film name| kanji          = 化石
| romaji         = Kaseki}}
| director       = Masaki Kobayashi
| producer       = 
| based on       =  
| starring       = 
| music          = Toru Takemitsu
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 200 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}}
  is a 1975 Japanese film directed by Masaki Kobayashi and based on a 1965 novel by Yasushi Inoue. It was Japans submission to the 47th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.    

==Plot==
A detailed look at a Tokyo business tycoon (played by Shin Saburi) given a diagnosis of terminal cancer who must now re-assess his life and values.

==Cast==
* Shin Saburi as Tajihei Kazuki
* Mayumi Ogawa as Akiko Kazuki
* Keiko Kishi as Woman (Death)
* Komaki Kurihara as Kiyoko Kazuki
* Haruko Sugimura as Mother-in-law
* Hisashi Igawa as Funazu

==See also==
*Cinema of Japan
*List of submissions to the 47th Academy Awards for Best Foreign Language Film
*List of Japanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
*A   by the New York Times

 
 
 

 
 
 
 
 
 


 