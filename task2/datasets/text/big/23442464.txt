Putaani Party
{{Infobox film
| name           = Putaani Party
| image          = Poster1Nett.jpg
| alt            = 
| caption        = Film poster
| director       = Ramchandra P. N.
| producer       = Childrens Film Society, India
| story          = Jayalakshmi Patil Mayyur Dandewale
| screenplay     = Ramchandra P. N.
| starring       = 
| music          = Vijay Prakash
| cinematography = Sameer Mahajan
| editing        = Arunabha Mukerjee
| studio         = 
| distributor    = Childrens Film Society, India
| released       =  
| runtime        = 78 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
Putaani Party ( : The Kid Gang) is a 2009 Kannada language feature film. Produced by Childrens Film Society, India, the film was shot in a village called Honnapura situated near the town of Dharwad in South India. The film uses local actors, most of them first timers who are new to the medium of films. The film has a particular dialect of Kannada that is spoken by a few in that part of India.

This film has been scripted and directed by Ramchandra PN, a graduate of the Film and Television Institute of India, Pune. His earlier film Suddha (film)|Suddha had won the best Indian film at the Osians Cinefan Festival of Asian Films, New Delhi, 2007. Putaani Party is his second feature film and his first in Kannada language.

==Plot==
 
A Gram Panchayat (village governing body) in rural India facilitates the functioning of the Makkala Samiti (Children’s Committee) – a children’s body elected by the children themselves.

The committee acts as a pressure group in trying to get the local governance react to various social issues that it raises. Guided by a sympathetic school teacher, the children’s honesty and persistency ruffles many feathers among the adults, some of whom have been using the ‘Makkala Samiti’ for their own needs.

What follows is a subtle cat and mouse game, by the end of which the children hope to get their voice heard.

The film subtly speaks about the Childrens rights to self governance and the pathetic attitude we adults have towards this issue.

==Awards==
 
The film has won the Swarna Kamal (Golden Lotus) for the 57th National Film Award for Best Childrens Film for the year 2009 constituted by the Government of India. 

==Film festival participation==
 
*The Global Film Festival of Indore, India, 2009
*The Mumbai International Film Festival for Children, India, 2009
*Kolkatta Film Festival, India, 2009
*Istanbul Childrens Film Festival, Turkey, 2009
*Bangladesh Childrens Film Festival, Bangladesh
*The Cairo International Film Festival for Children, Egypt, 2010
*CMS Childrens Film Festival, Lucknow, India, 2010 
*Bengaloru Habba Childrens Film Festival, Bengaluru, India 2010
*International Childrens Film Festival of Iran, 2010

==Cast==
 
*Ranjita Jadhav as Geeta
*Sharat Anchatgiri as Anil
*Gurudutta Joshi as Chandru
*Pawan Hanchinaal as Gaarya
*Deepak Joshi as Hussain
*Bhavani Prakash Babu as Neelu Teacher
*Jayalaxmi Patil as Nancy 
*CS Patil as Deshpande
*Mukund Maigur as Health Minister
 
(Source: Upperstall.com) 

==Controversies== Kesu the joint winner of the award was based on one of his earlier films, but claimed responsibility for Kesu jointly getting the award. 

==References==
 

==External links==
*  
* 
* 
* 

 

 
 
 
 
 
 