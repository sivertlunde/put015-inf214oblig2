Tangled Destinies
{{Infobox film
| name           = Tangled Destinies
| image          =
| image_size     =
| caption        =
| director       = Frank R. Strayer
| producer       = Ralph M. Like (producer)
| writer         = Edward T. Lowe Jr. (story and adaptation)
| narrator       =
| starring       = See below
| music          = Lee Zahler
| cinematography = Jules Cronjager
| editing        = Byron Robinson
| distributor    =
| released       = 18 October 1932
| runtime        = 56 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Tangled Destinies is a 1932 American film directed by Frank R. Strayer.

The film is also known as Who Killed Harvey Forbes? in the United Kingdom.

== Plot summary ==
 

== Cast ==
*Gene Morgan as Capt. Randall "Randy" Gordon
*Doris Hill as Doris
*Glenn Tryon as Tommy Preston, the Co-pilot
*Vera Reynolds as Ruth, the Airline Stewardess
*Ethel Wales as Prudence Daggott
*Monaei Lindley as Monica van Buren
*Syd Saylor as Buchanan, the Prizefighter
*Sidney Bracey as McGinnis, posing as Professor Marmont
*Lloyd Whitlock as Floyd Martin
*James B. Leong as Ling
*William P. Burt as Harvey Forbes Henry Hall as Dr. Wingate, the Parson William Humphrey as Professor Hartley

== Soundtrack ==
 

== External links ==
* 
* 
 

 
 
 
 
 
 
 
 
 
 


 