Doctor Bull
 
{{Infobox film
| name           = Doctor Bull
| image          = Doctor Bull FilmPoster.jpeg
| caption        = Film poster
| director       = John Ford
| producer       = Winfield R. Sheehan
| writer         = James Gould Cozzens Paul Green Philip Klein Jane Storm
| starring       = Will Rogers Vera Allen
| music          =
| cinematography = George Schneiderman
| editing        = Louis R. Loeffler
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
}}
Doctor Bull is a 1933 American comedy film directed by John Ford, based on the James Gould Cozzens novel The Last Adam. Will Rogers portrays a small town doctor who must deal with a typhoid outbreak in the community.  

The film was well praised by the New York Times, which noted that the story is similar to Lionel Barrymores film One Mans Journey when it premiered at the Radio City Music Hall in New York City.   Andy Devine met his future wife during the making of this picture.

==Cast==
* Will Rogers - Dr. George Doc Bull
* Vera Allen - Mrs. Janet Jane Cardmaker, Widow of Charles Edward Cardmaker / Bulls Girlfriend
* Marian Nixon - May Tupping - Telephone Operator
* Howard Lally - Joe Tupping
* Berton Churchill - Herbert Banning - Janets Brother
* Louise Dresser - Mrs. Herbert Banning
* Andy Devine - Larry Ward, Sodajerk
* Rochelle Hudson - Virginia (Muller) / Banning
* Tempe Pigott - Grandma Banning Elizabeth Patterson - Aunt Patricia Banning
* Nora Cecil - Aunt Emily Banning
* Ralph Morgan - Dr. Verney, Owner Verney Laboratory
* Patsy OByrne - Susan - Dr. Bulls Cook
* Veda Buckland - Mary - Janets Maid
* Effie Ellsler - Aunt Myra Bull Helen Freeman - Helen Upjohn, New Winton Postmistress

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 