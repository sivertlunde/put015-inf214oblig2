Maigret Sets a Trap (film)
{{Infobox film
| name           = Maigret Sets a Trap 
| image          = "Maigret_Sets_a_Trap"_(1958).jpg
| image_size     = 
| caption        = U.S. re-issue 1-sheet poster
| director       = Jean Delannoy
| producer       = Jean-Paul Guibert
| writer         = Georges Simenon (novel)   Jean Delannoy   Rodolphe-Maurice Arlaud   Michel Audiard
| starring       = Jean Gabin Annie Girardot Olivier Hussenot   Jeanne Boitel
| music          = Paul Misraki
| editing        = Henri Taverna Louis Page 
| studio         =  Rank
| released       = 29 January 1958
| runtime        = 119 minutes
| country        = France   Italy
| language       = French
| budget         =  Italian crime Belgian writer Georges Simenon featuring his fictional detective Jules Maigret.

==Principal cast==
* Jean Gabin - Jules Maigret
* Annie Girardot - Yvonne Maurin
* Olivier Hussenot - Lagrume
* Jeanne Boitel - Louise Maigret
* Lucienne Bogaert - Mme Veuve Adèle Maurin
* Jean Debucourt - Camille Guimard
* Guy Decomble - Mazet
* Paulette Dubost - Mauricette Barberot
* Jacques Hilling - Le médecin légiste
* Hubert de Lapparent - Le juge Coméliau

==Critical reception==
In The New York Times, Bosley Crowther wrote, "IF you havent yet made the acquaintance of French writer Georges Simenson and his famous and fascinating Parisian detective, Inspector Maigret, you cant ask a better introduction to both...an exciting example of the authors sophisticated work and a beautifully clear and catchy portrait of the gumshoe, performed by Jean Gabin...This is a dont-miss picture for the mystery fans."  {{cite web|url=http://www.nytimes.com/movie/review?res=9F00E4D61E31E73BBC4153DFB6678383649EDE&pagewanted=print|title=Movie Review -
  Inspector Maigret - Tale by Simenon; Inspector Maigret Has Premiere at Plaza - NYTimes.com|work=nytimes.com}} 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 
 

 