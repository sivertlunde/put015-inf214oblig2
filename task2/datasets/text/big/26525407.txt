Shalini Ente Koottukari
{{Infobox film
| name           = Shalini Ente Koottukari
| image          = Shalini Ente Koottukari.gif
| image_size     = 
| alt            = 
| caption        =  Mohan
| producer       = Mithra  
| writer         = P. Padmarajan
| narrator       = 
| starring       = Shobha  Jalaja  Sukumaran  Venu Nagavalli
| music          = G. Devarajan
| cinematography = U. Rajagopal
| editing        = G. Venkitaraman  
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by Mohan (director)|Mohan. The film stars Sukumari, Venu Nagavally, Shobha and Sukumaran in lead roles. The film had musical score by G. Devarajan.    The movie is about the friendship between two young women named Shalini and Ammu. The film was remade in Tamil as Sujatha (1980 film)|Sujatha.

==Plot==
The plot revolves around the life of Shalini(Shobha),a young college girl, who faces emotional isolation in many places of life. Much of the story is revealed through the memories of Ammu(Jalaja), Shalinis intimate friend both in college and in their village. Shalini and her brother Prabha (Venu Nagavally) experience a sort of emotional trauma and loneliness in their home, especially in the presence of their adamant father (K.P Ummer)and their step mother(Sukumari). As Shalini is not satisfied with the barren life of her home, except in her attachment with her brother, she finds solace and comfort in the happiness provided by her college. As a young, smart and beautiful graduate student, she is attracted to many of the males of the campus, especially to Roy (Ravi Menon),one of the smartest boys of the college. But his love is rejected by Shalini, even though she felt a temporary fascination towards him. Later the suicide of her brother makes her life uneasy, as she experiences an emotional isolation. However she overcomes the pain inflicted by the death. She returns to college with an aim to turn over a new leaf in her life. Here she develops a love affair with one of the newly appointed, young and handsome lecturer Jayadevan(Sukumaran). In the shade of his love Shalini gains confidence and feels aspirations towards life. But this unnatural relationship between a teacher and his student stirs up a little trouble in their college. Both of them ignore this as a wretched part of the social outlook. But their relation doesnt culminate in marriage, because Shalini falls ill with brain tumor. This evaporates her confidence and finally she succumbs, leaving Jayadevan alone in the world and leaving everlasting memories in the mind of Ammu.

==Cast==
*Shobha ...  Shalini
*Jalaja ...  Ammu
*Sukumaran ...  Jayadevan
*Venu Nagavalli ...  Prabha
*K. P. Ummer ...  Shalinis Father
*Sukumari ...  Shantha
*Ravi Menon ...  Roy
*Sreenath ...  Unnikrishnan
*Sathyakala

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by MD Rajendran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Himashaila Saikatha || P. Madhuri || MD Rajendran ||
|-
| 2 || Himashaila Saikatha   || K. J. Yesudas || MD Rajendran ||
|-
| 3 || Kannukal kannukal || P Jayachandran, Vani Jairam || MD Rajendran ||
|-
| 4 || Sundari Nin Thumbu kettiyitta || K. J. Yesudas || MD Rajendran ||
|-
| 5 || Vanavalli Kudilile (Bit) || K. J. Yesudas || MD Rajendran ||
|-
| 6 || Viraham Vishaadaardra || K. J. Yesudas || MD Rajendran ||
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 
 