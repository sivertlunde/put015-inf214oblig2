Mohan Joshi Hazir Ho!
{{Infobox Film
| name           = Mohan Joshi Hazir Ho!
| image          = 
| image_size     = 
| caption        = 
| director       = Saeed Akhtar Mirza    
| producer       = Saeed Akhtar Mirza    
| writer         = Yusuf Mehta (Screenplay) Saeed Akhtar Mirza (Story) Sudhir Mishra (Dialogue)
| story  =        
| dialogue  =   
| starring       = Naseeruddin Shah Deepti Naval Bhisham Sahni Dina Pathak
| music          = Vanraj Bhatia  
| lyrics         = 
| cinematography =  Virendra Saini
| editing        = Renu Saluja
| distributor    = 
| released       = 1984
| runtime        = 130 min
| country        =  
| language       = Hindi
| awards = National Film Award for Best Film on Family Welfare 1985
}}
 1984 Hindi art film, made by Saeed Akhtar Mirza, based on his own story, during the Parallel Cinema period of Indian cinema. 

The film is a comic satire on a judicial system, where cases drag on for decades, where plaintiffs either die or lose hope and money, while the corrupt run scot free, thanks to their nexus with corrupt lawyers.

It won the 1984 National Film Award for Best Film on Family Welfare.

==Synopsis==
An old couple, Mohan Joshi (Bhisham Sahni) and his wife (Dina Pathak), sues their landlord (Amjad Khan) for not maintaining their collapsing apartment building. For this, they hire two cunning lawyers (Naseeruddin Shah and Satish Shah), while Rohini Hattangadi is the opposition lawyer. 

The court case drags on for years and the lawyers milk the old couple dry, while they become rich. Back home in the society, the old couple is ridiculed for fighting the landlord, but they fight on nevertheless. 

In the end, when the judge comes to check the condition of the chawl, the landlords men prop up the place, thus convincing the judge that all is well. Finally, Joshi gathers all his strength and pulls down the temporary supports put up by the men causing the building to collapse on himself. 

==Cast==
* Naseeruddin Shah as Advocate Malkani
* Bhisham Sahni as Mohan Joshi
* Dina Pathak as Joshis wife
* Deepti Naval
* Mohan Gokhale
* Rohini Hattangadi as Advocate
* Pankaj Kapur
* Amjad Khan as Kundan Kapadia, landlord
* Satish Shah as Advocate
* Salim Ghouse as Advocate

==Location==
Mohan Joshi Hazir Ho! was shot at Kamithapura, Bapty Road, and Duncan Road (Two Tanks/Do Tanki) at Mumbais middle-class Hindu Muslim locality.

==References==
 

== External links ==
*  

 
 
 
 
 