Marana Simhasanam
 
{{Infobox film
| name           = Marana Simhasanam
| image          = MaranaSimhasanam.jpg
| caption        = Film poster at the Cannes festival
| director       = Murali Nair
| producer       = Murali Nair
| writer         = Murali Nair Bharathan Njavakkal
| starring       = Vishwas Njavakkal Lakshmi Raman
| music          = 
| cinematography = M J Radhakrishnan
| editing        = Lalitha Krishna
| distributor    = 
| released       =  
| runtime        = 57 minutes
| country        = India
| language       = Malayalam
| budget         = 
}}

Marana Simhasanam ( ,  ,  ) is a 1999 Indian drama film in Malayalam directed by Murali Nair. It was screened in the Un Certain Regard section at the 1999 Cannes Film Festival where it won the Caméra dOr (Golden Camera Award).   

==Plot==
Krishnan is an out-of-work seasonal farmer who is driven to steal coconuts from his landlord to support his family. When he is caught and imprisoned, he is shocked to learn that he has been charged with the murder of a man who has been missing from the island for many years. But since it was an election time, and the most popular platform for politicians had become capital punishment, Krishnan is sentenced to death in the countrys newest technology - electrocution by the electric chair.

==Cast==
* Vishwas Njavakkal
* Lakshmi Raman
* Suhas Thayat
* Jeevan Mitva

==References==
 

==External links==
* 
*  
*  
*  

 

 
 
 
 
 
 
 