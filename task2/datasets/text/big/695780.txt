Casino (film)
 
 
{{Infobox film
| name           = Casino
| image          = Casino_poster.jpg
| caption        = Theatrical release poster
| director       = Martin Scorsese
| producer       = Barbara De Fina
| screenplay     = Nicholas Pileggi Martin Scorsese
| based on       =  
| starring       = Robert De Niro Sharon Stone Joe Pesci Robert Richardson
| editing        = Thelma Schoonmaker
| studio         = Syalis D.A. Légende Entreprises De Fina / Cappa
| distributor    = Universal Pictures
| released       =  
| runtime        = 178 minutes
| country        = United States English
| budget         = $40 - 50 million 
| gross          = $116.1 million  
}}

Casino is a 1995 American crime drama film directed by Martin Scorsese. It is based on the non-fiction book of the same name by Nicholas Pileggi, who also co-wrote the screenplay for the film with Scorsese. The two previously collaborated on the hit film Goodfellas (1990 in film|1990).
 New York, The King Cape Fear (1991).
 handicapper who the Italian Las Vegas. Hacienda casinos in Las Vegas for the Chicago Outfit, from the 1970s until the early 1980s. 

Joe Pesci plays Nicky Santoro, based on real-life Mob enforcer Anthony Spilotro. A made man, Nicky is sent to Vegas to make sure money from the Tangiers is skimmed off the top and the mobsters in Vegas are kept in line. Sharon Stone plays Ginger McKenna, Aces wife, based on Geri McGee, a role that earned her a Golden Globe Award for Best Actress in a Motion Picture – Drama and a nomination for the Academy Award for Best Actress.

==Plot==
  handicapper and skimmed by Black Book. In retaliation, Nicky gathers his own crew, opens a jewelry store and restaurant, and begins running unsanctioned shakedowns and burglaries.
 Clark County Commissioner Pat Webb, by firing Webbs brother-in-law Don Ward for incompetence and refusing to reinstate him. Webb retaliates by pulling Aces casino license application from the backlog and forcing him to have a license hearing in 1980, while secretly arranging for the gaming board and State Senator Harrison Roberts to reject the license. Ace responds by appearing on television and openly accusing the city government of corruption. The bosses, unappreciative of Aces publicity, ask him to return home, but he refuses, stubbornly blaming Nickys reckless lawbreaking for his own problems, which leads to a heated argument with Nicky in the desert.
 Kansas City bug discussing the skim. Ginger tries to file for a divorce, but Ace refuses, stating that she has severe drug and alcohol problems, and she will likely spend all of her money within a year and come back to him anyway. Ace loses patience with Ginger after she and Lester are in Los Angeles with plans to run away to Europe with Amy. Ace talks Ginger into bringing Amy back, but he accidentally overhears her talking on the telephone with someone about having him killed, and he evicts her from the house. She returns, on Aces condition that she carry a beeper so he can contact her at any time. Ginger turns to Nicky for help in getting her share of her and Aces money from the bank, and they begin an affair, which violates Mob rules and could get Ace, Nicky, and Ginger killed. Ace reaches his limit with Ginger when she ties Amy to her bed so Ginger can have a night out with Nicky. Ace confronts Ginger in the restaurant for abusing Amy, learns of Gingers affair with Nicky, and disowns her. Ginger turns to Nicky, but Nicky refuses to have Ace killed and blames Ginger for her rash actions, claiming that Ace will likely not give her any money now. Ginger flies into a rage and attacks Nicky, but he throws her out. The next morning, the hysterical Ginger, determined to retrieve her share and jewels, goes to the Rothstein house and creates a disturbance by rear ending Aces car with her own, and the police are dispatched to the scene. Ginger, escorted by an officer, uses the distraction to steal the key to the couples bank deposit box. All of these events are occurring under FBI surveillance, having been alerted by Piscanos discussions heard by the bug. Ginger steals most of the cash from the safe deposit box and drives off, intending to run away to another city. Before she can escape, however, she is pulled over and arrested for aiding and abetting by the FBI undercover officers watching her. They arrest Ginger in hopes of using her as a witness against the Mobs activity.

It turns out that, because of threats made by the Mob, Ginger says nothing, but it doesnt matter; the FBI has collected enough evidence to arrest several casino executives involved with the skim. Philip Green, the casinos main executive, decides to cooperate with the FBI. The FBI raid Piscanos home and find his ledgers, which detail every transaction of the skim. Piscano becomes so upset he suffers a heart attack and dies, right in front of his wife. The casino empire crumbles, and the bosses are all arrested. Nicky, catching wind of the early arrests, flees Las Vegas and manages to evade capture. The FBI comes to see Ace with the pictures they took of Ginger. Refusing to look, he turns them away.

During an after-trial meeting, the bosses decide to eliminate anyone involved or with knowledge of the skim, in order to keep them from testifying. They kill money courier John Nance, a few casino executives, and Teamsters Union president Andy Stone. (Though they know he wont talk, Andy isnt Italian, and they decide not to risk it.) Ginger, whose money and jewels had been looted by bikers, hustlers, drug addicts, and felons, suffers a fatal drug overdose and dies in Los Angeles (a hot dose, as Ace learns via a second autopsy). By the time of her death, she had only $3,600 in "mint conditioned" coins remaining. Ace, on the other hand, is almost killed in 1983, in a botched car bombing which was never authorized by the bosses, but Ace suspects Nicky was involved. Before Ace can confront him, however, Nicky and Dominick are savagely beaten with baseball bats and buried alive in an Indiana cornfield by Frankie and the rest of their crew. Ace narrates that the bosses had "had enough of Nicky" and had ordered Nickys crew to get rid of him in exchange for clemency for covering up Nickys affair with Ginger.

With the Mob now out of power, the old casinos are purchased by big corporations and demolished to make way for gaudier gambling attractions financed by  , once again as a sports handicapper for the Mob, or in his words, "...right back where I started". Ace closes the film with the words, "...and why mess up a good thing? And thats that".

==Cast== Frank "Lefty Rosenthal) Tony "The Ant" Spilotro)
*Sharon Stone as Ginger McKenna (based on Geri McGee)
*James Woods as Lester Diamond (based on Lenny Marmor)
*Frank Vincent as Frankie Marino (based on Frank Cullotta)
*Don Rickles as Billy Sherbert Remo Gaggi John Bloom as Donald "Don" Ward
*L. Q. Jones as Clark County Commissioner Pat Webb
*Kevin Pollak as Philip Green. (based on Alan Glick)
* Ffolliott Le Coque as Tamara Scott (based on Tamara Rand) Alan King as Andy Stone (based on Allen Dorfman)
*Bill Allison as John Nance
*Philip Suriano as Dominic Santoro
*Vinny Vella as Artie Piscano
*Joseph Rigano as Vincent Borelli
*Nobu Matsuhisa as K. K. Ichikawa
*Richard Riehle as Charlie "Clean Face" Clark
*Dick Smothers as Nevada State Senator Harrison Roberts (character said to be based on Harry Reid)
*Griffin Dunne as Barney Greenstein (scene deleted)
*Oscar Goodman as Himself
*Frankie Avalon as Himself
*Steve Allen as Himself
*Jayne Meadows as Herself
*Cole Dragone as Vincents Kid Brother
*Frank Cullotta as Hitmen

==Production==

===Development===
The research for Casino began when screenwriter Nicholas Pileggi read a 1980 report from the Las Vegas Sun, about a domestic argument between Frank "Lefty" Rosenthal, a casino figure, and his wife, Geri McGee, a former topless dancer.    This gave him an idea to focus on a new book about the true story of mob infringement in Las Vegas during the 1970s, when filming of Goodfellas (the screenplay which he co-wrote with Scorsese) was coming to an end.    The fictional Tangiers resort reflected the story of the Stardust Resort and Casino, which had been bought by Argent Corporation in 1974 using loans from the Teamsters Central States Pension Fund. Argent was owned by Allen Glick, but the casino was believed to be controlled by various organized crime families from the Midwest. Over the next six years, Argent Corporation siphoned off between $7 and $15 million using rigged scales. When exposed by the FBI, this skimming operation was the largest ever exposed.  A number of organized crime figures were convicted as a result of the skimming. 

Pileggi decided to contact Scorsese about taking the helm of the project, which would become known as Casino.  Scorsese expressed interest in the project, calling this an "idea of success, no limits".    Although Pileggi was keen to release the book and then concentrate on a film adaptation, Scorsese encouraged him to "reverse the order".   
 soul about to go straight down to hell. 

===Principal photography=== Filming took Riviera casino Landmark Hotel as the entrance. According to the producer Barbara De Fina, there was no point in building a set if the cost was the same to use a real-life one.  The opening scene, with Sams car exploding, was shot three times with the third used for the film. 
When first submitted to the Motion Picture Association of America|MPAA, the film received an NC-17 rating due to its depictions of violence. Several edits were made in order to reduce the rating to R. Bona, Damien Inside Oscar 2    

==Reception==
The film was a box-office success, making $116 million worldwide    on a $40–50 million budget.   

While the film was heavily criticized for its excessive violence, it garnered a mostly positive critical response. Rotten Tomatoes gives the film an 80% "fresh" rating, based on 61 reviews.  On Metacritic, the rating is 73 (generally favorable reviews) out of 100 based on 17 reviews. 
 Best Actress Best Performance by an Actress in a Motion Picture – Drama.  Martin Scorsese was nominated for the Golden Globe Award for Best Director. 

American Film Institute lists
* AFIs 100 Years...100 Movies&nbsp;– Nominated 
* AFIs 10 Top 10&nbsp;– Nominated Gangster Film 

==Soundtrack==
{{Infobox album  
| Name        = Casino: Original Motion Picture Soundtrack
| Type        = soundtrack
| Artist      = various artists
| Cover       = Casino (1995) Soundtrack-Front.jpg
| Released    = November 20, 1995
| Recorded    =
| Genre       = Soundtrack
| Length      = MCA
| Producer    = Robbie Robertson
}}

;Disc 1
# "Contempt&nbsp;– Theme De Camille" by Georges Delerue
# "Angelina/Zooma, Zooma Medley" by Louis Prima
# "Hoochie Coochie Man" by Muddy Waters
# "Ill Take You There" by The Staple Singers
# "Nights in White Satin" by The Moody Blues
# "How High The Moon" by Les Paul & Mary Ford
# "Hurt" by Timi Yuro
# "Aint Got No Home" by Clarence Frogman Henry Without You" Nilsson
# "Love Is the Drug" by Roxy Music
# "Im Sorry (Brenda Lee song)|Im Sorry" by Brenda Lee Go Your Own Way" by Fleetwood Mac
# "The Thrill Is Gone" by B.B. King
# "Love Is Strange" by Mickey & Sylvia The In Crowd" by Ramsey Lewis
# "Stardust (song)|Stardust" by Hoagy Carmichael

;Disc 2 Walk on Jimmy Smith
# "Fa-Fa-Fa-Fa-Fa (Sad Song)" by Otis Redding
# "I Aint Superstitious" by Jeff Beck Group The Velvetones
# "(I Cant Get No) Satisfaction" by Devo
# "What a Diffrence a Day Made" by Dinah Washington
# "Working in the Coal Mine" by Lee Dorsey
# "The House of the Rising Sun" by The Animals Cream
# "Who Can I Turn To (When Nobody Needs Me)" by Tony Bennett
# "Slippin and Slidin" by Little Richard
# "Youre Nobody Till Somebody Loves You" by Dean Martin
# "Compared to What" (Live) by Les McCann & Eddie Harris
# "Basin Street Blues/When Its Sleepy Time Down South" by Louis Prima Sir Georg Solti)

==References==
 

;Bibliography
*  
*  

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 