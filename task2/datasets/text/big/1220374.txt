That's Dancing!
{{Infobox film name        = Thats Dancing! image       = Thats dancing.jpg caption     = Promotional poster for Thats Dancing! director    = Jack Haley, Jr. producer    = Jack Haley, Jr. David Niven, Jr. writer      = Jack Haley, Jr.
| starring = {{plainlist|
* Mikhail Baryshnikov
* Ray Bolger
* Sammy Davis, Jr.
* Gene Kelly
* Liza Minnelli
}} music       = Henry Mancini distributor = Metro-Goldwyn-Mayer released    =   runtime     = 105 minutes country     =   language  English
}}

Thats Dancing! is a 1985 retrospective documentary produced by Metro-Goldwyn-Mayer that looked back at the history of dancing in film. Unlike the Thats Entertainment! series, this film did not focus specifically on MGM films and included more recent performances by the likes of John Travolta (from Saturday Night Fever) and Michael Jackson and from the then-popular films Fame (1980 film)|Fame (1980) and Flashdance (1983), as well as classic films from other studios, including Carousel (musical)|Carousel, released by 20th Century Fox, and Oklahoma!, released by Magna Corporation (roadshow) and 20th Century Fox (general release).
 The Wizard of Oz.

The hosts for this film are Gene Kelly (who also executive produced), Ray Bolger (his last film appearance before his death in 1987), Liza Minnelli, Sammy Davis, Jr., and Mikhail Baryshnikov. Pop singer Kim Carnes was commissioned to sing an original song, "Invitation to Dance", that plays over the closing credits.
 the 1994 film), but even though it shared studio and producers, it is considered a separate production. Jack Haley, Jr., who wrote, produced  and directed the first Thats Entertainment! film, also wrote and directed this one, co-producing with longtime friend David Niven, Jr. Haleys father, Jack Haley, had co-starred with Bolger in The Wizard of Oz.

Thats Dancing! was not included when the three Thats Entertainment! films were released on DVD in 2004; it was instead released on its own in 2007. The DVD includes several behind-the-scenes promotional featurettes from 1985 on the making of the film, as well as its accompanying music video featuring Kim Carnes singing "Invitation to Dance" although the DVD omits both the video and song itself.

==Dedication==
This film is dedicated to all dancers .... especially those who devoted their lives to the development of their art long before there was a motion picture camera.

==Appearances==
*Tommy Abbott
*June Allyson
*Ann-Margret
*Fred Astaire
*Lucille Ball
*Mikhail Baryshnikov
*Jennifer Beals
*David Bean
*Busby Berkeley
*Eric Blore
*Monte Blue
*Ray Bolger
*John Brascia
*Lucille Bremer
*James Cagney
*Irene Cara
*Leslie Caron
*Gower Champion
*Marge Champion
*Cyd Charisse
*Joan Crawford
*Dan Dailey Jacques dAmboise
*Sammy Davis, Jr.
*Doris Day
*Gloria DeHaven
*Isadora Duncan
*Buddy Ebsen
*Taina Elg
*Eliot Feld
*Margot Fonteyn
*Loie Fuller
*Clark Gable
*Judy Garland
*Virginia Gibson
*Cary Grant
*Jack Haley Margaret Hamilton
*Carol Haney
*June Haver
*Robert Helpmann
*Judy Holliday
*José Iturbi
*Michael Jackson
*Marine Jahan
*Van Johnson
*Ruby Keeler
*Gene Kelly Paula Kelly
*Michael Kidd
*Charles Laskey
*Ruta Lee
*Vivien Leigh
*Bambi Linn
*Peter Lorre
*Susan Luckey
*Shirley MacLaine
*Dean Martin
*Léonide Massine
*Matt Mattox
*Joan McCracken
*Ray McDonald
*Ann Miller
*Liza Minnelli James Mitchell
*Ricardo Montalbán
*Annabelle Moore
*Tony Mordente
*George Murphy
*Gene Nelson
*Julie Newmar
*Rudolf Nureyev
*Donald OConnor
*Anna Pavlova Marc Platt
*Dick Powell
*Eleanor Powell
*Jane Powell
*Tommy Rall
*Debbie Reynolds Jeff Richards
*Chita Rivera
*Bill "Bojangles" Robinson
*Ginger Rogers
*Mickey Rooney
*Wini Shaw
*Moira Shearer
*Frank Sinatra
*Red Skelton
*Tucker Smith
*James Stewart
*Lyle Talbot
*Russ Tamblyn
*Lilyan Tashman Robert Taylor
*Anthony Scooter Teague
*Shirley Temple
*Tamara Toumanova
*John Travolta
*Lana Turner Bobby Van
*Vera-Ellen
*Ethel Waters
*Bobby Watson
*Esther Williams David Winters
*Vera Zorina
*Vincent Price
*Robert Banas
*Francesca Bellini
*Betty Carr
*Carole DAndrea
*Leroy Daniels
*Norma Doggett
*Harvey Evans
*Nancy Kilgas
*Bert Michaels
*Susan Oakes
*Gina Trikonis
*Tarita

==Films featured==
*The Dumb Girl of Portici (1916 in film|1916)
*So This is Paris (1926 in film|1926) Flying High (1931 in film|1931) 42nd Street (1933 in film|1933)
*Gold Diggers of 1933 (1933 in film|1933)
*Rufus Jones for President (1933 in film|1933)
*Dames (1934 in film|1934)
*The Gay Divorcee (1934 in film|1934)
*Gold Diggers of 1935 (1935 in film|1935)
*Broadway Melody of 1936 (1935 in film|1935)
*Roberta (1935 film)|Roberta (1935 in film|1935)
*The Littlest Rebel (1935 in film|1935)
*Born to Dance (1936 in film|1936)
*Swing Time (1936 in film|1936)
*Honolulu (film)|Honolulu (1939 in film|1939) The Wizard of Oz (1939 in film|1939) Gone with the Wind (1939 in film|1939)
*On Your Toes (1939 in film|1939)
*Down Argentine Way (1940 in film|1940)
*Babes on Broadway (1941 in film|1941)
*Yankee Doodle Dandy (1942 in film|1942)
*Bathing Beauty (1944 in film|1944)
*Kismet (1944 film)|Kismet (1944 in film|1944)
*The Harvey Girls (1946 in film|1946)
*Till the Clouds Roll By (1946 in film|1946) Ziegfeld Follies (1946 in film|1946) Good News (1947 in film|1947) The Red Shoes (1948 in film|1948)
*Neptunes Daughter (1949 film)|Neptunes Daughter (1949 in film|1949) Three Little Words (1950 in film|1950)
*Royal Wedding (1951 in film|1951) An American in Paris (1951 in film|1951) The Merry Widow (1952 in film|1952)
*Singin in the Rain (1952 in film|1952)
*The Band Wagon (1953 in film|1953)
*Give a Girl a Break (1953 in film|1953) Kiss Me, Kate (1953 in film|1953) Latin Lovers (1953 in film|1953) Seven Brides for Seven Brothers (1954 in film|1954)
*Its Always Fair Weather (1955 in film|1955)
*Jupiters Darling (film)|Jupiters Darling (1955 in film|1955)
*Oklahoma! (1955 film)|Oklahoma! (1955 in film|1955)
*Carousel (film)|Carousel (1956 in film|1956) Invitation to the Dance (1956 in film|1956)
*Les Girls (1957 in film|1957) Silk Stockings (1957 in film|1957)
*Indiscreet (1958 film)|Indiscreet (1958 in film|1958) Tom thumb (1958 in film|1958) West Side Story (1961 in film|1961)
*Viva Las Vegas (1964 in film|1964) Sweet Charity (1969 in film|1969) The Boy Friend (1971 in film|1971)
*Cabaret (1972 film)|Cabaret (1972 in film|1972)
*Saturday Night Fever (1977 in film|1977) The Turning Point (1977 in film|1977)
*Fame (1980 film)|Fame (1980 in film|1980)
*Flashdance (1983 in film|1983)

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 