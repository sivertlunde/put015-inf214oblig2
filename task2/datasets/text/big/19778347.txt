Red Lion (film)
{{Infobox film
| name           = Red Lion
| caption        =
| image	=	Red Lion FilmPoster.jpeg
| director       = Kihachi Okamoto
| producer       = Toshiro Mifune Yoshio Nishikawa
| writer         = Sakae Hirosawa Kihachi Okamoto
| starring       = Toshiro Mifune Shima Iwashita
| music          = Masaru Sato Takao Saito Toho Company Ltd.
| released       =  : December 17, 1969
| runtime        = 115 minutes
| country        = Japan Japanese
}} 1969 Japanese directed by Kihachi Okamoto and starring Toshirō Mifune and Shima Iwashita.

==Plot summary==
Gonzo (Toshiro Mifune), a member of the Imperial Restoration Force, is being asked by the emperor to deliver official news to his home village of a New World Order. Wanting to pose as a military officer, he dons the Red Lion Mane of Office. Upon his return, his attempt to tell the village about a brand-new tax cut is quashed when the townfolk mistakenly assumes that he is there to rescue them from corrupt government officials. He learns that an evil magistrate has been swindling them for years. Now, he has to help the village, ward off Shogunate fanatics, along with the fact that he cant read his own proclamations.

The director, Kihachi Okamoto, is well known for introducing plot twists and surprising endings in his films,    and Red Lion is no exception. What starts out as an almost comedic series of misunderstandings between almost comically drawn characters ends up turning far more serious as the film progresses. Tomi (Iwashita), as Gonzos (Mifune) old flame, is tragically torn between her hopes that Gonzos new marriage proposal is genuine, and her fears that her life will never improve unless she "goes along" with the corrupt and powerful who rule over the peasants lives. The film ends with the peasants dancing to the cry of "Ee ja nai ka" ("Why not!?", "Whatever!", or "Nevermind!"), which fatalistically refers to the tumultuous 1866-67 period of Japanese history immediately preceding the imperial restoration and the end of the Edo period.  

==Cast==
* Toshiro Mifune as Gonzo
* Shima Iwashita as Tomi
* Etsushi Takahashi as Ichinose Hanzo
* Minori Terada as Sanji
* Nobuko Otowa as Oharu

==See also==
* Eijanaika (film)|Eijanaika, a 1981 film by Shōhei Imamura set in the same historical period.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 