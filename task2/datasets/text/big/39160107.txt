Inside the Square
{{Infobox film
| name           = Inside the Square
| image          = 
| caption        =  
| director       = David Michod
| producer       = Luke Doolan Nash Edgerton Louise Smith
| screenplay     =   David Roberts Anthony Hayes Bill Hunter
| music          = David McCarthy	
| cinematography =  
| editing        = Luke Doolan Katie Flaxman
| studio         = Blue Tongue Films Film Depot
| distributor    = 
| released       =  (Australia)
| runtime        = 30 minutes
| country        = Australia
| language       = English
}} thriller film The Square directed by Nash Edgerton, with particular emphasis on the style and difficulties arose during the shooting and the experience of cast and crew during the making of film.   It had a limited release in Australia on 5 March 2009. 

==Synopsis== The Square.

==Cast==
*David Michod as Interviewer
*Nash Edgerton as Himself David Roberts as Himself
*Claire van der Boom as Himself
*Joel Edgerton as Himself Anthony Hayes as Himself
*Hanna Mangan-Lawrence as Herself
*Peter Phelps as Himself
*Kieran Darcy-Smith as Himself
*Bredan Donoghue as Himself Bill Hunter as Himself
*Julian Morrow as Himself
*Sam Petty as Himself
*Louise Smith as Herself
*Luke Doolan as Himself

==Production==
After the release of 2008 film The Square, David Michod made the documentary on the film with the help of behind the scene footage available. He also interview the cast and crew of the film. They talk about their experience of shooting the film.

Talking about making the documentary on the production of film, Michod said "just bearing witness to the battle of a feature film shoot is a good learning experience." 

==See also==
* Cinema of Australia

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 