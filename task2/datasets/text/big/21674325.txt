Adorable Julia
 
{{Infobox film
| name           = Adorable Julia
| image          =
| caption        =
| director       = Alfred Weidenmann
| producer       = Rudolf Stering
| writer         = Eberhard Keindorff Johanna Sibelius
| starring       = Lilli Palmer
| music          =
| cinematography = Werner Krien
| editing        = Renate Jelinek
| distributor    =
| released       =  
| runtime        = 97 minutes
| country        = Austria
| language       = German
| budget         =
}}

Adorable Julia ( ) is a 1962 Austrian comedy film directed by Alfred Weidenmann. It was entered into the 1962 Cannes Film Festival.   

==Cast==
* Lilli Palmer - Julia Lambert
* Charles Boyer - Michael Grosselyn
* Jean Sorel - Tom Fennel
* Jeanne Valérie - Avice Crichton
* Ljuba Welitsch - Dolly de Fries
* Tilly Lauenstein - Evie, Julias Zofe
* Charles Régnier - Lord Charles Tamerly
* Thomas Fritsch - Roger, Julias Sohn
* Herbert Fux - Inspizient am Theater

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 


 
 