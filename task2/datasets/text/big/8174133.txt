Raam (2006 film)
{{Infobox film|
  name     = Raam |
  image          = |
  caption        = |
  writer         = N.Shankar | Nitin Genelia DSouza Hrishitaa Bhatt Atul Kulkarni Krishnam Raju |
  director       = N.Shankar |
  producer       = Sudhakar Reddy |
  cinematography =  JK |
  editing        = Goutam Raju |
  studio         = Sri Venkataramana Pictures |
  distributor    = |
  released       =    |
  runtime        = |
  country        = India |
  language       = Telugu |
  music          = Yuvan Shankar Raja |
  budget         = |
}} Nitin in the lead role, Genelia DSouza and Hrishitaa Bhatt in supporting roles and Krishnam Raju in a very vital role. The film, produced by Nitins father Sudhakar Reddy, has music scored by Yuvan Shankar Raja and was released on 30 March 2006 to good responses.This film was dubbed into Hindi as "Jeene Do"

==Cast==
* Nitin Kumar Reddy|Nitin as Raam
* Genelia DSouza as Janaki
* Hrishitaa Bhatt as Jyothika
* Krishnam Raju as Dasaratha Ramayya
* Atul Kulkarni as Virendra
* Brahmanandam as Dr. Chakravarthi
* Jeeva (Telugu actor)|Jeeva
* Devraj
* Dharmavarapu Subramanyam Venu Madhav
* Supreet
* Ali (actor)|Ali
* MS Narayana
* Shakuntala
* Kovai Sarala

==Soundtrack==
{{Infobox album
| Name = Raam
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Ram cover.jpg
| Released =  
| Recorded = 2005 Feature film soundtrack
| Length =
| Label = Aditya Music
| Producer = Yuvan Shankar Raja
| Last album  = Pattiyal (2006)
| This album  = Raam (2006)
| Next album  = Azhagai Irukkirai Bayamai Irukkirathu (2006)
}} Hyderabad on Chandrabose and Bhuvanachandra.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration (min:sec) !! Lyricist!! Notes
|- 1 || "Shock" || Shaan (singer)|Shaan, Aanchal Datta Bhatia, Suchitra, Premji Amaran || 4:39 || Chinni Charan ||
|- 2 || "Nuvena" || Haricharan,   ||
|- 3 || "Made in Hyderabad" || Shankar Mahadevan || 4:18 || Chandrabose ||
|- 4 || "Kurabhani" ||   ||
|- 5 || "Pilla Bale" || Tippu (singer)|Tippu, Saindhavi || 3:54 || Bhuvanachandra ||
|}

==External links==
 

==References==
 

 
 
 

 