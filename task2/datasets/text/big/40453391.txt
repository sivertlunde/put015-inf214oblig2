Larceny (1948 film)
 {{Infobox film
| name           = Larceny
| image          = Larceny 1948 film poster.jpeg
| caption        = Theatrical release poster
| director       = George Sherman
| producer       = Leonard Goldstein
| writer         = William Bowers Louis Morheim Herbert H. Margolis John Fleming (novel The Velvet Fleece) John Payne Joan Caulfield Dan Duryea Shelley Winters
| music          = Leith Stevens	 	
| cinematography = Irving Glassberg Frank Gross
| studio         = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Payne, Joan Caulfield, Dan Duryea and Shelley Winters.                              

==Plot==
Con man Rick Maxon (Payne) tries to swindle war widow Deborah (Caulfied) into giving up her savings for a nonexistent memorial. When Rick falls in love with Deborah he has pangs of remorse. But he reckons without enamored moll Tory (Winters) and his gang boss Randall (Duryea).

==Cast== John Payne as Rick Maxon  
* Joan Caulfield as Deborah Owens Clark  
* Dan Duryea as Silky Randall  
* Shelley Winters as Tory  
* Dorothy Hart as Madeline  
* Richard Rober as Max  
* Dan OHerlihy as Duke  
* Nicholas Joy as Walter Vanderline  
* Percy Helton as Charlie Jordan  
* Walter Greaza as Mr. Owens
* Patricia Alphin as Waitress
* Harry Antrim as Mr. McNulty   Russ Conway as Detective  
* Paul Brinegar as Mechanic Don Wilson as Master of Ceremonies 
* Gene Evans as Horace   Bill Walker as Butler  
* Sam Edwards as Y.A.A. president

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 