Man on Fire (1957 film)
{{Infobox film
| name           = Man on Fire
| image          = 
| caption        = 
| director       = Ranald MacDougall
| producer       = Sol Siegel
| based on       = 
| screenplay     = 
| starring       = Bing Crosby Inger Stevens
| music          = 
| cinematography = Joseph Ruttenberg
| editing        = 
| distributor    = 
| released       = 1957
| runtime        = 
| country        = United States
| language       = English
| budget         = $1,180,000  . 
| gross          = $1,415,000 
}}
Man on Fire is a 1957 film starring Bing Crosby in a rare non-singing, unsympathetic role. 

==Plot==
Two years after her divorce from wealthy businessman Earl Carleton, ex-wife Gwen wants to regain custody of their son, Ted, who is devoted to his father.

Gwens new husband Bryan Seward works for the State Department. Gwen intimates that Earl made threats to harm Sewards career if she tried to get the child back. As his lawyer Sam Dunstock prepares to handle the custody hearing on his behalf, Earl seems unaware of the personal interest in him from Sams assistant, Nina Wylie.

The boy tells Judge Rudolph in private that he prefers living with his dad and is suspicious of his mothers motives. Nevertheless, the judge awards Gwen full custody. An alcoholic binge follows for Earl, who is heartbroken.

Nina comes to inform him that business matters are suffering from his neglect. She takes care of Earl when he drunkenly passes out, after he claimed that he never loved Gwen and only married her so a son could take over his business someday.

Ted runs away. Earl, giving power of attorney to Sam, announces he is traveling to Europe for a long rest. Nina realizes that Earl is planning to take Ted with him. Seward arrives in time to take the child off the plane, resulting in a fight between the men. Sam is disgusted with Earls self-pitying and selfish behavior, and when Nina confesses to being in love with him, Earl cruelly says girls like her are "a dime a dozen". She hands him a dime.

Gwen reveals that she wants Ted back now because she and Seward have been unable to have a child of their own. Her guilt grows at discovering how unhappy she has made her son. A resolution is made, and Earl comes to make his apologies to Nina, handing her back her dime.

==Cast==
* Bing Crosby as Earl Carleton
* Inger Stevens as Nina Wylie
* E.G. Marshall as Sam Dunstock Anne Seymour as Judge Rudolph
* Richard Eastham as Seward
* Malcolm Brodrick as Ted
* Mary Fickett as Gwen
==Reception==
According to MGM records the film earned $1.1 million in the US and Canada and $315,000 elsewhere, resulting in a loss of $542,000. 
==References==
 

==External links==
*  at IMDB

 

 
 
 