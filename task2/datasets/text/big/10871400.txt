Yehi Hai Zindagi
{{Infobox film
| name           = Yehi Hai Zindagi
| image          = YehiHaiZindagifilm.png
| image_size     = 
| caption        = 
| director       = K. S. Sethumadhavan
| producer       = B. Nagi Reddy
| writer         = Inder Raj Anand  Vaali
| narrator       = 
| starring       = Sanjeev Kumar Utpal Dutt
| music          = Rajesh Roshan
| cinematography = Marcus Bartley
| editing        = M. S. Mani
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Yehi Hai Zindagi is a 1977  Hindi film. Produced by B.Nagi Reddy the film is directed by K. S. Sethumadhavan. The film stars Sanjeev Kumar, Seema Deo, Utpal Dutt, Lucky Ali, Ramesh Deo and Neeta Mehta.
The films music is by Rajesh Roshan.

==Synopsis==

Anand Narayan comes from a poor family, which consists of his wife, Gayetri, daughter, Kamla, and two sons Madhu and Govind. Through hard work, Anand starts on his way to success. He meets with Nekiram, who becomes his business partner, and also his samdhi (in-law) as his daughter is in love with Madhu. Both get married in a simple ceremony. After this marriage, both Nekiram, and Anand and his family work hard at preparing food in a small sweet shop. Lord Krishna then appears to Anand who asks the Lord for material success, After this meeting Anands Dhaba  grows into a restaurant, then a larger fancier restaurant, finally a five star hotel. Now Anand and his family are all wealthy and live in a palatial home.  Bhagwan Shri Krishna regularly visits him and asks about him and his family. Anand proudly takes all the credit, and asks Krishna to accompany him to see his success and his hotel. Bhagwan Krishna declines, but agrees do so later. Things dont go quite as well for Anand, when he finds out that lakiram has been cheating him. lakiram leaves the house, but Madhu accompanies him. Then Anand finds that Govind has been squandering money and time on women and alcohol; and to top his disappointment Kamla gets pregnant. When Bhagwan Krishna comes to visit Anand again, he admits that he is a failure, his health is poor, and his family are all strangers to him. Lord Krishna advises Anand that this is his life and he must learn from the Mahabharata, and that every life is a struggle, which is confused by relations and near and dear ones, and also by ones ego. Once the ego is removed, then only one can see clearly. But will Anand be able to understand and carry out what Lord Krishna is asking him to? Or will end up taking the blame, like he took the credit, upon himself?

One song from this movie, "Pyar Ka Badla" written by lyricist Anand Bakshi and sung by Kishore Kumar aptly sums up Anands situation.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Hum Mil Gaye"
| Mohammed Rafi, Lata Mangeshkar
|-
| 2
| "Dilruba Aa Meri Bahon Mein"
| Kishore Kumar, Lata Mangeshkar
|-
| 3
| "Kali Kali Kaise Kate Raat"
| Asha Bhosle
|-
| 4
| "Pyar Ka Badla"
| Kishore Kumar
|}
== Awards and Nominations ==
*1978 Filmfare Awards - Nominated Best Actor - Sanjeev Kumar

== External links ==
*  

 
 
 
 