Odd Couple (film)
 
 
 
{{Infobox film
| name           = Odd Couple
| image          = OddCouple DVDcover.jpg
| image_size     = 
| caption        = Odd Couple UK DVD cover
| director       = Lau Kar Wing
| producer       = Karl Maka
| writer         = Wong Pak Ming Lai Wai Man
| narrator       =  Mars
| music          = Kung Chuan Kai Hoh Koo
| cinematography = Ho Ming
| editing        = Tony Chow Gar Bo Motion Picture Company
| released       =  
| runtime        = 97 minutes
| country        = Hong Kong Cantonese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1979 Cinema Hong Kong film directed by Lau Kar Wing, who also stars, alongside Sammo Hung. It was the first film to be released by Gar Bo Motion Picture Company (aka Gar-Bo Film Company), an independent production company set up by Hung, Lau and producer Karl Maka. The fight scenes are mainly weapon-based, with particular emphasis on the contrast between the dao (sword) and qiang (spear).

The film is sometimes listed as The Odd Couple.

==Synopsis==
Two aging martial artists get together once a year for a timed duel. One is master of the short sword, King of Sabres (Sammo Hung), and the other is King of Spears (Lau Kar Wing). Every year the fight ends in a draw, and as the masters are getting old, they decide the best course of action is to each take on a student to determine who is the better teacher. They agree to meet up again 10 years later, with their students and let the next generation carry on the duel.

A previously upright martial artist known as Old Yellow Dog, (Bryan Leung) kidnaps the students (also played by Lau and Hung) before their duel can begin. It transpires in a flashback that the master was defeated in separate battles with the King of Sabres and the King of Spears, and was forced to retire from fighting. Now, after years of training in the long bladed staff and with a new name, Laughing Bandit, he wants to lure the old masters out to exact his revenge. The old masters arrive, first taking on the Laughing Bandits four disciples and defeating them. However, this was a ploy to tire them out, and individually they are unable to defeat Laughing Bandit and his new techniques. The evil master suggests the old men both attack at once, but because of their pride and belief in their own superiority, they refuse. The students are released, while each master is fighting, and are instructed to escape. After some protestation they do, and the old masters are killed. 

Fuelled by revenge, the students agree to join forces to defeat the evil master. Hungs character (the new King of Spears) comes up with a plan us to utilise magnets that can pull the Laughing Bandits weapon from him. After luring him out into the open, they fight him unarmed, choosing to mimic their weapon styles with empty hands, but with the magnet they are able to disrupt his attacks, and after a gruelling fight they triumph.

After burying their masters, they decide to honour their memory by fulfilling their request to duel. However, as with their masters before them, the fight ends in a draw. Instead, they decide to resolve who is the greatest by playing a game, rather than fighting. Each must try to place his weapon into their masters burial mound, whilst simultaneously stopping their opponent from doing so. After another long competition, the film ends with the pair laughing at the absurdity of the rivalry and realising that as friends they will never be able to determine who is the best.

==Production==
===Cast===
In the film, Sammo Hung and Lau Kar Wing play two roles each, a master and a student. Hung plays the King of Sabres and his student is played by Lau, and Lau plays the King of Spears and his student is played by Hung. In the later part of the film, all four characters appear in scenes together. Both students become proficient in their weapons, allowing the actors, in their opposing roles, to demonstrate their skills with both weapons. This aspect could prove a challenge for the editor to cut between the two major stars switching between old and young, depending the requirement of the scenes.

* Sammo Hung - King of Sabres / Ah Yo (2 roles)
* Lau Kar Wing King of Spears / Stubborn Wing (2 roles) (as Kar Wing Lau)
* Bryan Leung - Laughing Bandit aka Old Yellow Dog / Scarface (as Kar Yan Leung) Mars - Potato
* Dean Shek - Master Rocking / Playboy
* Huang Ha - Single Sabre Wu Li
* Peter Chan - Pak Chow / Cloud Sabre
* Yeung Sai Gwan - Tiger Spear
* Billy Chan - Humpback / Tien (2 roles)
* Lam Ching Ying - Ha (Laughing Bandits fighter)
* Yuen Miu - Mo (Laughing Bandits fighter)
* Chung Fat - Ti (Laughing Bandits fighter)
* Ho Pak Kwong - Master Rockings assistant
* Tai San - Master Rockings assistant
* Chan Ling Wai - Master Rockings  Assistant
* Lee Hoi San - thug
* Karl Maka - Cloud Sabres Challenge
* Cheung Chok Chow - tea house boss
* Benny Lai - Extra
* Yuen Biao - Stunt Double

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 