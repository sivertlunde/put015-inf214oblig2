Battle for Sevastopol
 
{{Infobox film
| name = Battle for Sevastopol
| image = Battle_of_Sevastopol_2015.jpg
| caption =
| director = Sergey Mokritskiy
| producer = Natalya Mokritskaya Egor Olesov
| writer = Maxim Budarin Makym Dankevych Leonid Korin Egor Olesov
| starring = Yulia Peresild Joan Blackham Evgeniy Tsyganov Vitaliy Linetskiy
| music =
| cinematography = Yuriy Korol
| editing =
| distributor =
| released =  
| runtime = 110 minutes
| country = Ukraine - Russia
| language = Russian
| budget = $5 million
}}
Battle for Sevastopol ( ;  ) is biographical film about the life path of Lyudmila Pavlichenko woman-sniper who became a hero during the siege of Sevastopol in 1941–1942. Work on the film began in 2012, whereas the filming started at the end of 2013 and finished in June 2014. Distribution on the screen is expected to be held in spring 2015.

International team has been working on the film production under the direction of Sergey Mokritskiy and workshops «New people» and «Kinorob». The project of the film has already been presented during the international film festivals in Cannes,  Odessa,  Berlin and Toronto.

The concatenation that happened in 1937–1957 is reflected in the movie. In Ukraine Battle for Sevastopol is going to be on a screen under the title Nezlamna (the word can be translated as "indestructible" or "inviolable").

== Plot ==
The war caught the graduate student of the   — and, eventually, the defence of Sevastopol.

Film-makers have characterized their project as a psychological biopic that touches upon the issue of a tragic fate that a woman is forced to face being on a war. They paid attention to the emotional state, psychological experience and spiritual wounds that the main heroes were forced to face. Moreover, the love line occupies the prominent place in the plot. Sergey Mokritskiy mentioned: In our national cinema we have not a famous movies devoted to women on a war except for the film "The Dawns Here Are Quiet". And this theme troubled me strongly. I really do believe that that was the discrepancy of the masculine and feminine burden. And more than 800 thousand women fought… 

Having passed the trials of the war, Pavlichenko found herself becoming a part of the Soviet delegation to the United States. The slogan of the film is Woman, who changed the world recalls the role that she had successfully took part while the second front in Europe was opened.

== Production ==
The filming began in 2012 after the first archive material devoted to Pavlichenko was examined. Sergey Mokritskiy, who is more commonly known for his cinematography in several of television and film projects, was invited on the position of the director. After his arrival the plot was altered towards the more precise correspondence with the Pavlichenkos official biography.
 
Before the filming began in late 2013 film project had won the state film projects competition, thus it obtained the first recognition of critics and government funding.

The shooting took place from late 2013 till June 2014. By the autumn of 2014 the work is still ongoing: experts voice the film, modify rough cut makes, finalize computer effects, create incidental music.

== References ==
 

==External links==
*  

 
 
 
 