Adoration (1928 film)
{{Infobox film
| name           = Adoration
| image          = Adoration_1928_Poster.jpg
| caption        = Theatrical poster
| director       = Frank Lloyd
| producer       = Frank Lloyd
| writer         = Story: Lajos Biró  Screenplay: Winifred Dunn
| starring       = Billie Dove Antonio Moreno Emile Chautard Lucy Doraine
| music          = Max Bergunker Gerard Carbonara Karl Hajos
| cinematography = John F. Seitz John Rawlins Frank Stone
| distributor    =  
| released       =  
| runtime        = 73 minutes
| country        = United States English
| budget         =
}} sound drama film with a Vitaphone musical score and sound effects. The film was released by First National Pictures, a subsidiary of Warner Bros., and directed by Frank Lloyd. It stars Billie Dove, Antonio Moreno, Emile Chautard and Lucy Doraine. The film was also issued in a shorter silent version for theatres that were not yet wired for sound.

==Synopsis==
Russian prince (Antonio Moreno), upon returning from the front, sees his wife (Billie Dove) in the company of a blackguard count. Prince becomes jealous, and, later, while attempting to break into the counts home, both he and the count are knocked unconscious by the mob. Prince, wife and count escape from Russia and eventually get to Paris, where wife is able to convince prince that it was her maid who went to the counts home. 

==Cast==
*Billie Dove as Elena 
*Antonio Moreno as Prince Serge Orloff
*Emile Chautard as Murajev 
*Lucy Doraine as Ninette  
*Nicholas Bela as Ivan 
*Nicholas Soussanin as Vladimir
*Winifred Bryson as Baroness 
*Lucien Prival as Baron

==Preservation==
A print of Adoration survives in the Czech Film Archive.

==External links==
* 

 

 
 
 
 
 
 
 
 