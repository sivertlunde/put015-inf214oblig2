International Settlement (film)
{{Infobox film
| name           = International Settlement
| image          =
| caption        =
| director       = Eugene Forde
| producer       = Darryl F. Zanuck Sol M. Wurtzel Frank Fenton
| narrator       =
| music          = Samuel Kaylin
| editing        = Nick DeMaggio
| starring       = Dolores del Rio George Sanders June Lang
| distributor    = 20th Century Fox
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
}}
International Settlement is a 1938 American drama film directed by Eugene Forde and starring Dolores del Rio, George Sanders and June Lang. It is set in the Shanghai International Settlement during the Second Sino-Japanese War|Sino-Japanese War. In the film, a gun runner falls in love with a beautiful French singer. 

==Cast==
* Dolores del Rio - Lenore Dixon
* George Sanders - Del Forbes
* June Lang - Joyce Parker
* Dick Baldwin - Wally Burton
* Ruth Terry - Vera Dale
* John Carradine - Murdock
* Keye Luke - Doctor Wong
* Harold Huber - Joseph Lang Leon Ames - Monte Silvers
* Pedro de Cordoba - Maurice Zabello

==Reception==

Paul Mavis, reviewing the Fox Cinema Archives 2014 DVD release for DVDTalk, wrote, "International Settlement isnt competent enough to be even mildly diverting, and not ridiculous enough to be unintentionally amusing. In other words, it commits the worst sin a "B" can: its boring."  

==References==
 

 

 
 
 
 
 
 
 


 