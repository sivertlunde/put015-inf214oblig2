Little Red Riding Hood (1954 film)
{{Infobox film
| name           = Rotkäppchen
| image          = 
| image_size     =
| caption        = 
| director       = Walter Janssen
| producer       = Hubert Schonger
| writer         = Konrad Lustig
| narrator       =  Ellen Frank Peter Lehmann Michael Beutner Rudolf Gerhofer Götz Wolf Helge Lehmann
| music          = Giuseppe Becce
| based on = Little Red Riding Hood by Charles Perrault and by Brothers Grimm
| cinematography =
| editing        =
| distributor    = October 10, 1954  (West Germany) 
| runtime        = 54 min.
| country        =   German
| budget         =
| preceded_by    =
| followed_by    =
}}

Little Red Riding Hood ( ) is a German film, directed by Walter Janssen, based on the story of Little Red Riding Hood by the Brothers Grimm.

== Cast ==
* Maren-Inken Bielenberg – Little Red Riding Hood
* Elinor von Wallerstein – The Mother Ellen Frank – The Grandmother
* Wolfgang Eichberger – The Hunter
Little Red Riding Hoods Brothers: Peter Lehmann – Bammel
* Michael Beutner – Bommel
* Rudolf Gerhofer – Bummel
* Götz Wolf – Flock
* Helge Lehmann – Flick

== DVD release ==
In 2007, Rotkäppchen was released on DVD in Germany.  The film was also part of five DVD boxset, which contained other classic live-action German fairytale films made in the 1950s.

== External links ==
*  
*  

 
 

 
 
 
 


 