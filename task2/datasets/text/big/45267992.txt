From A to B (film)
{{Infobox film
| name           = From A to B
| image          =
| border         = yes
| caption        = From A to B movie poster
| director       = Ali F. Mostafa
| producer       = Paul Baboudjian Mohamed Hefzy Ronnie Khalil Ali F. Mostafa
| writer         = Ali F. Mostafa
| screenplay     = Mohamed Hefzy
| starring       = Fahad Albutairi Shadi Alfons Fadi Rifaai Khaled Abolnaga Wonho Chung
| music          = 
| cinematography = Michel Dierickx
| editing        = Ali Salloum 
| studio         = twofour54
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = United Arab Emirates
| language       = Arabic English
| budget         = 
| gross          = 
}} Ali F. Mostafa. Set in the United Arab Emirates, the film revolves around three old friends (Jay, Rami & Omar) who travel on an adventurous road trip from Abu Dhabi to Beirut in memory of their lost friend Hadi.  

== Cast ==
*Fahad Albutairi as Youssef Jay
*Shadi Alfons as Rami
*Fadi Rifaai as Omar
*Khaled Abol Naga as Senior Syrian Officer
*Samer Al Masry as Syrian Rebel Leader
*Wonho Chung as Raed
*Leem Lubany as Shadya
*Maha Abou Ouf as Ramis Mother
*Yousra El Lozy as Arwa
*Abdulmohsen Alnemr as Youssefs Father
*Ali Suliman as Syrian Army Officer
*Madeline Zima as Samantha
*Ahd as Rana
*Christina Ulfsparre as Julie
*Ibrahim Alkhairallah as Mechanic
*Ibrahim Mursi as Saudi Officer
*Hisham Fageeh as Saudi Officer
*Iman Al Shaybani as Joanne

== External links ==
*  
*  
==References==
 
 
 
 
 
 
 
 