The Backwater
{{Infobox film
| name           = The Backwater
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 共喰い
| director       = Shinji Aoyama
| producer       = Naoki Kai
| writer         = Haruhiko Arai
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Masaki Suda Ken Mitsuishi Yuko Tanaka
| music          = Isao Yamada Shinji Aoyama
| cinematography = Takahiro Imai
| editing        = Genta Tamaki
| studio         = Style Jam
| distributor    = Bitters End
| released       =  
| runtime        = 102 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2013 Japanese drama film directed by Shinji Aoyama, starring Masaki Suda.  It is based on the Akutagawa Prize-winning novel by Shinya Tanaka and adapted by Haruhiko Arai. The film won the Best Director award from the Swiss critics federation and the Best Film award from the Junior Jurys at the 2013 Locarno International Film Festival. 

== Plot ==
Toma (Masaki Suda) lives with his father, Madoka (Ken Mitsuishi), and Madokas lover, Kotoko (Yukiko Kinoshita) on the riverside in Shimonoseki, Yamaguchi Prefecture. Tomas mother, Jinko (Yuko Tanaka), resides on the other side of the bridge, making a living by cleaning fish. Madoka routinely beats and chokes women when having a sex. As Madokas son, Toma is afraid of becoming like his father.

On his 17th birthday in 1988, Toma has sex with his girlfriend, Chigusa (Misaki Kinoshita).

== Cast ==
* Masaki Suda as Toma
* Misaki Kinoshita as Chigusa
* Yukiko Shinohara as Kotoko
* Ken Mitsuishi as Madoka
* Yuko Tanaka as Jinko
* Ittoku Kishibe as Detective

== Production ==
The song "Torna a Surriento" was used in the closing credits of the film. 

== Release ==
The film screened in competition at the 2013 Locarno International Film Festival.  

== Reception ==
Jinshi Fujii of Yomiuri Shimbun praised the film, noting that "Aoyama has succeeded in transmigrating the tradition of Japanese film through something more than mere repetition."  Meanwhile, Dan Fainaru of Screen International felt that "the script relies mostly on its female characters that are unsurprisingly far more alive and interesting than the men in their lives, and the same goes for the performances of the three lead actresses." 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 