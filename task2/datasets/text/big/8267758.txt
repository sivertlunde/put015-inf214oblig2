Daredevils of the West
 
{{Infobox film
| name = Daredevils of the West
| image = Daredevilsofthewest.JPG John English
| producer =William J. OSullivan Joseph ODonnell Joseph Poland Ted Adams George J. Lewis
| cinematography =Bud Thackery
| distributor =Republic Pictures
| released       =   1 May 1943 {{Cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 68–69
 | chapter =
 }} 
| runtime         = 12 chapters / 196 minutes (serial) 
| country = United States
| language = English
| budget          = $140,550 (negative cost: $167,003) 
| awards =
}} Republic Movie serial.

Daredevils of the West is a lightning-fast-paced Western cliffhanger serial released by Republic Pictures in 1943 starring Allan Lane and Kay Aldridge. The plot involves a gang of land-grabbers who try to prevent safe passage of the Foster Stage Company through frontier territory, however, the story of the serial is merely a framework for the setup of numerous elaborate stunt action sequences, cliff-hanging perils and fiery deathtraps which the hero and heroine must fight to survive. As was the case with other Republic serials released during wartime such as King of the Mounties and The Masked Marvel, a rapid, even frantic pace is evident throughout, which has made the serial a favorite among fans.

Both Native American actors who played the Lone Rangers companion Tonto, Chief Thunder Cloud (of the 1930s Lone Ranger movie serials) and Jay Silverheels (of 1950s Lone Ranger television program) appear in the sequence in which Kay Aldridge and Eddie Acuff are held hostage by Indians.

The serial was long considered to be a partially lost film; for years, copies of only chapters 2, 4, 5, and 12 circulated on 16mm among film collectors. The entire serial, however, was never fully "lost," it was simply never re-released to theaters, or released on video because 5 reels of sound from the archival material apparently went missing not long after the serials original exhibition. The absence of these reels may be the reason why Republic Pictures had no objection to subsequently selling off an incomplete print of the serial to Cowboy actor William "Hopalong Cassidy" Boyd for use as stock footage.

All 12 chapters were screened, for the first time in 65 years, in May 2008 by the Serial Squadron at SerialFest 2008 in Newtown, Pennsylvania. The film was subsequently shown at the Lone Pine film festival in October 2009 and at the Memphis film festival in June 2010.

Daredevils of the West, with restored audio and dubbed missing-dialogue sequences, was  released on DVD by the Serial Squadron, an organization which restores classic film serials, in  February 2011.

==Cast==
*Allan Lane as Duke Cameron
*Kay Aldridge as June Foster
*Eddie Acuff as Red Kelly
*William Haade as Barton Ward
*Robert Frazer as Martin Dexter Ted Adams as Silas Higby
*George J. Lewis as Turner
*Charles F. Miller as Foster

==Production==
Daredevils of the West was budgeted at $140,550 although the final negative cost was $167,003 (a $26,453, or 18.8%, overspend).  It was the cheapest Republic serial of 1943. 

It was filmed between 9 January and 13 February 1943.   The serials production number was 1199. 

Kay Aldridge and Allan Lane were billed as "their Majesties, the King and Queen of Serials". {{Cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | chapter = 5. Shazam and Good-by
 | page = 133
 }} 

===Stunts=== Tom Steele as Duke Cameron (doubling Allan Lane)
*Babe DeFreest as June Foster (doubling Kay Aldridge)
*Pierce Lyden
*Eddie Parker
*Allen Pomeroy
*Ken Terrell
*Bill Yrigoyen
*Joe Yrigoyen

===Special Effects===
All the special effects in Daredevils of the West were produced by the Lydecker brothers.

==Release==

===Theatrical===
Daredevils of the Wests official release date was 1 May 1943, although this is actually the date the sixth chapter was made available to film exchanges. 

==Chapter titles==
# Valley of Death (24min 44s)
# Flaming Prison (15min 33s)
# The Killer Strikes (15min 35s)
# Tunnel of Terror (15min 32s)
# Fiery Tomb (15min 32s)
# Redskin Raiders (15min 33s)
# Perilous Pursuit (15min 36s)
# Dance of Doom (15min 31s)
# Terror Trail (15min 32s)
# Suicide Showdown (15min 32s)
# Cavern of Cremation (15min 34s)
# Frontier Justice (15min 32s)
 Source:   {{Cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 235
 }} 

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* 
*  (small section on Dardevils of the West)

 
{{Succession box Republic Serial Serial
| before=G-Men vs The Black Dragon (1943)
| years=Daredevils of the West (1943)
| after=Secret Service in Darkest Africa (1943)}}
 

 

 
 
 
 
 
 
 
 
 