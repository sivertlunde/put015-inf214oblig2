Husband and Wife (film)
{{Infobox film
| name           = Husband and Wife
| film name      =  
| image          =
| caption        =
| director       = Mikio Naruse
| producer       = Sanezumi Fujimoto
| writer         = Toshiro Ide Yoko Mizuki
| based on       = 
| starring       =
| music          = Ichirō Saitō
| cinematography =
| editing        =
| studio         = Toho
| distributor    =
| released       =  
| runtime        = 87 minutes
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
}}

Husband and Wife (夫婦 Fufu) is a Japanese film directed by Mikio Naruse released in 1953.  Like other Naruse films from this period, such as Repast (film)|Repast and Wife (film)|Wife, the theme of Husband and Wife involves a couple trapped with each other.  Ken Uehara and Yôko Sugi star as the titular husband and wife.        Mikuni Rentaro portrays a widower with whom the couple move in due to economic circumstances.    The film deals with the difficulties that ensue when both the wife and the landlord find themselves attracted to each other.   Towards the end of the film, the couple contemplates getting an abortion to help alleviate their financial distress.  


Slant Magazine critic Keith Uhlrich describes the film as a "what if" scenario, specifically,"what if Charlie Chaplin and Buster Keaton were locked together in a room and forced to fight over Mary Pickford?"   At one point during the film the trio attends a re-enactment of a Chaplin comedy routine.    According to Uhlrich, the moviess theme is the journey towards "reconciliation of those contradictions inherent to being human."   It is one of several Naruse films in which a character is forced to "redefine themselves and test their strength."   

Like some other Naruse films, Husband and Wife includes a scene on a rooftop, which film critic Chris Fujiwara interprets as evoking the characters desire "to seek the widest possible view, the greatest distance." 

==References==
 

 

 
 
 
 
 
 
 