I'm Not Single
 
 
{{Infobox film
| name = Im Not Single
| image = Im-not-single.jpg
| caption =
| director = Pierre Andre
| writer = Pierre Andre
| starring = Farid Kamil Awal Ashaari Lisa Surihani Intan Ladyana
| producer = David Teo
| editing  =
| distributor = Metrowealth Movies Production Sdn Bhd
| budget = RM 1.5 million
| gross = RM 1,391,000
| released =  
| runtime = 87 minutes
| country = Malaysia
| language = Malay
}}

Im Not Single (translates as Aku Bukan Bujang in Malay language|Malay) is a sophomore romantic comedy film directed by Pierre Andre. It was released on 24 July 2008. A series of road tour has been launched to promote the film.

==Synopsis==
The movie tells of a young couple Maya and Adam who loathe each other because they are forced into an arranged marriage by their parents. Mayas ailing grandmother wants her to marry as soon as possible despite the fact that she already has a steady boyfriend, Dani. Dani and Maya love each other and had even planned to marry previously. With the arranged marriage looming closer, Dani has to try and figure out how to stop from Maya been taken away from him forever.

==Cast==
* Farid Kamil; Adam
* Awal Ashaari; Dani
* Lisa Surihani; Maya
* Intan Ladyana; Lisa Datuk Jalaluddin Hassan – Borhan
* Fadilah Mansor – June
* Hafidzuddin Fazil – Ghani
* Aznah Hamid – Su
* Cat Farish – Psycho
* Ahmad Idham – Ahmad
* David Teo – David

==Awards==
The film was nominated for two categories in the 21st Malaysian Film Festival, 2008.

Nominated
* Best Film - Nominated

Won
* Most Promising Actress - Lisa Surihani

==External links==
*   at Sinema Malaysia

 
 

 
 
 
 