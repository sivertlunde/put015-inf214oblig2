Kanchana Ganga (2004 film)
{{Infobox film
| name = Kanchana Ganga
| image = 
| caption =
| director = Rajendra Singh Babu
| writer = Rajendra Singh Babu
| starring = Shivrajkumar  Sridevi Vijaykumar   Jai Jagadish   Sumalatha
| producer = Vijaylakshmi Singh   Dushyant Singh   Jai Jagadish
| music = S. A. Rajkumar 
| cinematography = B. C. Gowrishankar
| editing = S. Manohar
| studio = Lakshmi Creations
| released =  
| runtime = 149 minutes
| language = Kannada
| country = India
| budget =
}}
 romantic drama film directed and written by Rajendra Singh Babu and produced by Vijayalakshmi Singh, Dushyant Singh and Jai Jagadish.  The film features Shivarajkumar and Sridevi Vijaykumar in the lead roles along with Jai Jagadish, Parvin Dabbas, Sumalatha and Arjun in other pivotal roles.  The film was the last work of veteran cinematographer B. C. Gowrishankar who died before the release of the film.
 Karnataka State Award for best Costume design special award for the year 2004-05. 

== Cast ==
* Shivarajkumar as Soorya
* Sridevi Vijaykumar as Urmila
* Jai Jagadish 
* Arjun
* Sumalatha
* Parvin Dabbas
* Sadhu Kokila
* Raju Ananthaswamy
* Chaitra
* Neelam


== Soundtrack ==
The music was composed by S. A. Rajkumar and the audio was sold on Ashwini Audio label. A popular title song from the film Naa Ninna Mareyalare was reused in the soundtrack.   

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Prema Prema
| extra1 = S. P. Balasubrahmanyam
| lyrics1 = R. N. Jayagopal
| length1 = 
| title2 = Prema Prema Sujatha
| lyrics2 = R. N. Jayagopal
| length2 = 
| title3 = Kill You
| extra3 = Shankar Mahadevan Kaviraj
| length3 = 
| title4 = Aishwarya Aishwarya Tippu
| lyrics4 = Kaviraj
| length4 = 
| title5 = Noorondu Chooragi
| extra5 = Sadhana Sargam
| lyrics5 = K. Kalyan
| length5 = 
| title6 = Sooryana Kirana
| extra6 = Sonu Nigam, Vaishali
| lyrics6 = K. Kalyan
| length6 = 
| title7 = Ninna Mareyalare (remix)
| extra7 = Udit Narayan, Mahalakshmi Iyer
| lyrics7 = K. Kalyan
| length7 =
}}

== References ==
 

 

== External links ==
*  
*  

 
 
 
 
 
 

 

 