Brothers at War
{{Infobox film
| name        = Brothers at War
| image       = BAWonesheetsmall.jpg
| caption     = Theatrical release poster
| director    = Jake Rademacher
| producer    = Norman S. Powell Jake Rademacher Gary Sinise (Executive Producer) David Scantling (Executive Producer) Joseph Rademacher
| music       = Lee Holdridge
| cinematography = Marc Miller Conor Colwell
| editing     = Robert DeMaio
| distributor = Samuel Goldwyn Films
| released    =  
| runtime     = 112 minutes
| country     = United States
| language    = English
}}
Brothers at War is a 2009 documentary film directed by Jake Rademacher  and produced by Rademacher and Norman S. Powell.   The film follows several US soldiers in the Iraq War. The films executive producers are actor, director, and Presidential Citizens Medal recipient Gary Sinise  and Secretary of Defense Medal for Outstanding Public Service recipient David Scantling.    Brothers at War won the Best Documentary Feature Award at the 2008 GI Film Festival.   The film features an original score by Lee Holdridge and an original song--"Brothers in Arms"—by John Ondrasik of Five for Fighting. 

==Synopsis==
Jake Rademacher states at the beginning of the film that he is setting out to understand the experience, sacrifice, and motivation of his two brothers serving in Iraq. As the film develops, however, it becomes clear that Jakes underlying motivation is to prove himself to his brothers, as well as to himself. Jake, who longed to join the military as a youth but was denied entry into West Point (which is never fully explained in the movie), clearly feels the need for acceptance from his serving brothers.  The film follows Jake’s experiences as he embeds with his brother Isaac in Iraq as well as other U.S. and Iraqi combat units. Rademacher goes along with reconnaissance troops on the Syrian border and into sniper "hide sites" in the Sunni Triangle. As Rademacher’s brothers return home, the film shows the toll separation and life-threatening work takes on soldiers’ parents, siblings, wives and children. 

==Cast==
*Mahmoud Hamid Ali as Himself
*Edward Allier as Himself
*Zack Corke as Himself
*Danelle Fields as Herself
*Ben Fisher as Himself
*Kevin Keniston as Himself
*Frank McCann as Himself
*Brandon Mongo Phillips as Himself
*Claus Rademacher as Himself
*Isaac Rademacher as Himself
*Jake Rademacher as Himself
*Jenny Rademacher as Herself Joseph Rademacher as Himself
*Robert Smallwood as Himself
*Kevin Turner as Himself

==References==
 

==External links==
 
*  
*  
;Reviews
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 