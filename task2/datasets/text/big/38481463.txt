Bad Country
 
{{Infobox film
| name           = Bad Country
| image          = 
| caption        = 
| director       = Chris Brinker
| producer       = Chris Brinker Kevin Chapman
| writer         = Jonathan Hirschbein
| starring       = Willem Dafoe Matt Dillon Amy Smart Tom Berenger
| music          = John Fee
| cinematography = 
| editing        = 
| distributor    = Mandalay Vision
| studio         = CB Productions
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Baton Rouge and Angola, Louisiana on August 7, 2012. 

The film was in post-production when director Chris Brinker died suddenly on February 8, 2013.  

Brinker was to be presented with the Robert Smalls Indie Vision Award at the 7th annual Beaufort, South Carolina International Film Festival in February 2013. 

==Cast==
* Willem Dafoe as Bud Carter
* Matt Dillon as Jesse Weiland
* Tom Berenger as Lutin
* Amy Smart as Lynn Weiland
* Bill Duke as Nokes
* Neal McDonough as Kiersey
* Kevin Chapman as Morris
* Christopher Denham as Tommy Weiland
* Chris Marquette as Fitch
* Don Yesso as Captain Bannock
* Alex Solowitz as Buzz McKinnnon
* John Edward Lee as Catfish Stanton

==References==
 

==External links==
*  

 
 
 
 
 
 


 