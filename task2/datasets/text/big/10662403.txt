Le Golem
{{Infobox film
| name           = Le Golem
| image          = GOLEM original poster.jpg
| alt            = 
| caption        = French film poster 
| director       = Julien Duvivier
| producer       = Charles Philip
| writer         = André-Paul Antoine Julien Duvivier
| starring       = Harry Baur
| music          = Josef Kumok
| cinematography = Jan Stallich Vaclav Vich
| editing        = Jirí Slavícek
| studio         = 
| distributor    = 
| released       =   Soister, 2004. p.186  
| runtime        = 95 minutes
| country        = France Czechoslovakia 
| language       = 
| budget         = 
| gross          = 
}}
Le Golem is a 1936 French monster movie filmed in Prague, Czechoslovakia and directed by Julien Duvivier.

==Plot==
In a Prague ghetto, poor Jews find themselves oppressed by Emperor Rudolph II (Harry Baur) which leads to talk among the Jews of re-awakening the Golem who is being held in an attic by Rabbi Jacob (Charles Dorat). During a food riot Rudolphs mistress, the Countess Strada (Germaine Aussey), is rescued by the enamored De Trignac (Roger Cuchesne) who gets hurt in the process. De Trignac is taken to Rabbi Jacobs house by his wife Rachel (Jany Holt). When Rudolph gets engaged to his cousin Isabel of Spain, it angers Strada who charms De Trignac to steal Jacobs Golem. 

Friedrich (Gaston Jacquest), the prefect of the police informs Rudolph of the Golems disappearance. Rabbi Jacob is brought to into the palace by Rudolph and told if any Jews are found in relation with the Golems disappearance, then they will be hung. Rachel seeks De Trignac to aid Jacobs escape from the castle. De Trignac offers what he claims to be Charlemagnes sword for Jacobs release. After Jacob and De Trignac leave, Rudolph wanders his palace where he meets up with the Golem. After a failed polite gesture to the statue, Rudolph attacks it with his sword and has it chained to the walls of his dungeon. Rudolph then demands all Jewish leaders be imprisoned and executed, including Jacob. Rachel had learned previously from her Jacob that when a beast roars the Golem will awake. As people enter the palace to honour Rudolph, Rachel gets the lions near the Golems cell to roar. Rachel carves the Hebrew word "emet", meaning truth on the Golems forehead which brings the creature to life. 

The Golem snaps his chains and causes panic through the palace along with the released lions. Friedrich and many other of Rudolphs other advisors are attacked and killed by the golem while Rudolph escapes the palace. Jacob erases the first Hebrew letter on the Golems head (which now spells "dead") making the Golem disintegrate while Rudolphs benevolent brother Mathias approaches Prague.  Soister, 2004. p.187 

==Cast==
* Harry Baur as Rudolph II
* Roger Karl as Chancellor Lang
* Charles Dorat as Rabbi Jacob
* Ferdinand Hart as the Golem

==Release==
The movie is a  , and was released in Czechoslovakia under the title Golem.  The film opened in Britain in 1937 with a running time that was cut to 83 minutes. It was then reissued the next year under the title The Legend of Prague with a running time of 70 minutes. Soister, 2004. p.191 

==References==
;Notes
 

;Bibliography
* {{Cite book
 | last= Soister
 | first= John T.
 | title= Up From the Vault: Rare Thrillers of the 1920s and 1930s McFarland
 |year= 2004
 |isbn= 0-7864-1745-5
 |url=http://books.google.ca/books?id=XKtuKgMXBNYC
 |accessdate=June 30, 2010
}}
* {{Cite book
 | last= Chihaia
 | first= Matei
 | title= Der Golem-Effekt. Orientierung und phantastische Immersion im Zeitalter des Kinos
 |publisher= transcript
 |year= 2011
 |isbn= 978-3-8376-1714-6
 |url= http://www.transcript-verlag.de/ts1714/ts1714.php
 |accessdate=March 11, 2011
}}

==Further reading==
*Jekova, Alena. 77 Prague Legends. Prague: Prah, 2006. ISBN 80-7252-139-X

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 