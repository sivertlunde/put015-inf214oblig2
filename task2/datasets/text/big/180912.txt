Theodora Goes Wild
{{Infobox film
| name           = Theodora Goes Wild
| image          = Theodora_Goes_Wild.jpg
| image_size     = 215px
| caption        = theatrical poster
| director       = Richard Boleslawski
| producer       = Everett Riskin Mary McCarthy Sidney Buchman
| starring       = Irene Dunne Melvyn Douglas Arthur Morton William Grant Still (both uncredited) Joseph Walker Otto Meyer
| distributor    = Columbia Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Mary McCarthy Best Actress Best Film Editing.  Its often mentioned as a screwball comedy, due to a few of the elements in the story.

Prior to this film, Dunne had been cast in dramatic films.  Theodora Goes Wild was her first comedy, and while it was reported in a biography of Cary Grant that she was unsure of herself in comedies, this extremely popular film proved to be the beginning of a new phase in her film career, as a screen comedienne.

==Plot== serialized in Thomas Mitchell), to stop printing the salacious installments.
 Robert Greig), but actually goes to see her publisher, Arthur Stevenson (Thurston Hall). Though Stevenson reassures an anxious Theodora that only he and his secretary know her identity, his wife Ethel (Nana Bryant) pressures him into an introduction, which the books illustrator, Michael Grant (Melvyn Douglas), overhears. Intrigued, Michael invites himself to dinner with the Stevensons and Theodora. Theodora becomes annoyed when Michael smugly assumes that she is a teetotaler, so she orders a whiskey. As the night goes on, she becomes drunk. So does Ethel, forcing Arthur to take his wife home and leaving Theodora alone with Michael. When he makes a pass at her, she panics and flees, much to his amusement.

He tracks her down to her hometown and is immediately noticed outside her house whistling.  Because she technically is not supposed to know anyone outside of Lynnfield, he coerces her into hiring him as a gardener, thus scandalizing her aunts and providing Rebecca Perry plenty to gossip about. Michael declares that he is going to break Theodora out of her confining routine, ignoring her protests that she likes her life just the way it is. Despite herself, she enjoys herself very much when Michael makes her go berrypicking and fishing with him. Finally, she gets up the nerve to tell the disapproving women of the Literary Circle that she loves him. When she tells Michael what she has done, he is less than thrilled. 
 Lieutenant Governor, shows up, followed by Michaels wife, Agnes (Leona Maricle). The estranged couple are only remaining married to avoid causing a political scandal for Michaels father. 

Theodora determines to free Michael just as he had done for her. He wants her to hold off until his fathers term ends in two years, but she is unwilling to wait that long. To that end, she courts publicity by coming out of anonymity and revealing herself as the true Caroline Adams. She is staying in Michaels apartment even though he has moved out to get away from her, and she tells the press of her intention to publish a new book that details finding romance in her small town and searching for someone who will call her "baby" &ndash; a story that mimics her relationship with Michael.  Meanwhile, Michael denies to the press that he has even met Theordora before.  She finally crashes the Governors ball and arranges for reporters to photograph her embracing Michael. Agnes seeks a divorce to avoid looking like a fool.

Theodora returns to Lynnfield and is warmly welcomed as a celebrity, even by her now-supportive aunts. She causes further talk when she brings a newborn baby with her. When Michael, now divorced, sees the child, he tries to flee, but then Theodora reveals that the baby belongs to Rebecca Perrys own secretly-married daughter.

==Cast==
  
* Irene Dunne as Theodora Lynn / "Caroline Adams"  
* Melvyn Douglas as Michael Grant Thomas Mitchell as Jed Waterbury
* Thurston Hall as Arthur Stevenson
* Rosalind Keith as Adelaide Perry
* Elisabeth Risdon as Mary Lynn
 
* Margaret McWade as Elsie Lynn
* Spring Byington as Rebecca Perry
* Nana Bryant as Ethel Stevenson
* Henry Kolker as Henry Grant
* Leona Maricle as Agnes Grant Robert Greig as John Lynn
 

==Adaptations==
Orson Welles adapted the story for his Mercury Theatre players for a January 14, 1940 episode of The Campbell Playhouse, with Loretta Young starring as Theodora.

==External links==
*  
*  
*  
*  
*   on Campbell Playhouse: January 14, 1940

 
 
 
 
 
 
 
 
 
 
 
 