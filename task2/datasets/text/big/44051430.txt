Akale Aakaasham
{{Infobox film
| name           = Akale Aakaasham
| image          =
| caption        =
| director       = IV Sasi
| producer       = Thiruppathi Chettiyar
| writer         = Sherif
| screenplay     = Sherif Madhu Adoor Bhasi Sridevi Sreelatha Namboothiri
| music          = G. Devarajan
| cinematography = C Ramachandra Menon
| editing        = K Narayanan
| studio         = Evershine
| distributor    = Evershine
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film,  directed by IV Sasi and produced by Thiruppathi Chettiyar. The film stars Madhu (actor)|Madhu, Adoor Bhasi, Sridevi and Sreelatha Namboothiri in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  Madhu
*Adoor Bhasi
*Sridevi
*Sreelatha Namboothiri
*Bahadoor
*MG Soman Meena
*Nanditha Bose
*Padmapriya
*Vidhubala
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Puthuvarsha Kahalam || P Jayachandran, P. Madhuri || Sreekumaran Thampi ||
|-
| 2 || Rajaneeyavanika || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 3 || Vasanthakaalam Varumennothi || P. Madhuri || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 