The Flying Scot (film)
{{Infobox film
| name           = The Flying Scot
| image	         = 
| caption        = 
| director       = Compton Bennett
| producer       = Compton Bennett
| writer         = Norman Hudis Jan Read Ralph Smart
| starring       = Lee Patterson Kay Callard Alan Gifford
| music          = Stanley Black Peter Hennessy
| editing        = John Trumper
| studio         = 
| distributor    = 
| released       = 1957
| runtime        = 70 min
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
The Flying Scot is a 1957 British crime film directed by Compton Bennett and starring Lee Patterson, Kay Callard and Alan Gifford. 

==Plot==
A gang plans to rob a large number of banknotes from an express train.

==Cast==
* Lee Patterson - Ronnie
* Kay Callard - Jackie
* Alan Gifford - Phil
* Gerald Case - Guard
* Jeremy Bodkin - Charlie, the boy Mark Baker - Gibbs
* Geoffrey Bodkin - Neat boy
* John Dearth - Father
* Kerry Jordan - Drunk John Lee - Young man
* Patsy Smart - Mother
* Margaret Withers - Middle-aged lady Margaret Gordon

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 