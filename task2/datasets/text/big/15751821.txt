Five Ways to Save the World
{{Infobox Film
| name           = Five Ways to Save the World
| image          = 
| image_size     = 
| caption        = 
| director       = Jonathan Barker, Cecilia Hue, Anna Abbott
| producer       = Karen OConnor
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2006
| runtime        =
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 British documentary James Roger John Latham, Ian Jones, and Klaus Lackner.

The "five ways" proposed are geoengineering techniques: space lenses in orbit, to diffract sunlight away from the earth cloud seeding with seawater to increase albedo sulfur launched into the stratosphere to increase albedo
* ocean fertilization with iron or urea (nitrogen fertilizer) artificial trees (see carbon capture and sequestration)

Since the first three methods do not remove carbon dioxide from the atmosphere, they would only reduce global warming but not ocean acidification. Since the last two methods would remove carbon dioxide, they could in theory reduce both global warming and ocean acidification.

==External links==
* 
* 

 
 
 
 
 
 


 
 