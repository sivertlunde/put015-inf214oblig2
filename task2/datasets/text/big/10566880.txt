The Lost Honour of Katharina Blum (film)
{{Infobox film
| name           = The Lost Honour of Katharina Blum
| image          = Katharinablum.jpg
| caption        = 
| director       = Volker Schlöndorff Margarethe von Trotta
| producer       = 
| writer         = Heinrich Böll (novel) Volker Schlöndorff
| narrator       = 
| starring       = Angela Winkler Mario Adorf Dieter Laser Jürgen Prochnow
| music          = Hans Werner Henze
| cinematography = 
| editing        = Peter Przygodda
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = West Germany
| language       = German
| budget         = 
}} novel of the same name by Heinrich Böll.

The film stars Angela Winkler as Blum, Mario Adorf as Kommissar Beizmenne, Dieter Laser as Tötges, and Jürgen Prochnow as Ludwig.

== Plot ==
The title character is an innocent housekeeper whose life is ruined by an invasive tabloid reporter and a police investigation when the man with whom she has just fallen in love turns out to be a radical bank robber. The film, unlike the novel, ends with a scene at Tötges funeral, with his publisher delivering a hypocritical condemnation of the murder as an infringement on the freedom of the press.

== Cast (selected) ==
* Angela Winkler – Katharina Blum
* Mario Adorf – Kommissar Beizmenne (Inspector Beizmenne)
* Dieter Laser – Werner Tötges
* Jürgen Prochnow – Ludwig Götten
* Heinz Bennent – Dr. Hubert Blorna
* Hannelore Hoger – Trude Blorna

== Analysis == Heinrich Bölls book, and at the end of von Trottas and Schlöndorffs cinematic interpretation of it, the following text appears:

 The characters and action in this story are purely fictitious. Should the description of certain journalistic practices result in a resemblance to the practices of Bild-Zeitung, such resemblance is neither intentional, nor fortuitous, but unavoidable. 
 September 11, unsubstantiated media hype was used to launch the invasion of Iraq.

== Further reading ==
* Gerhardt, Christina. "Surveillance Mechanisms in Literature and Film: Die verlorene Ehre der Katharina Blum by Böll and Schlöndorff / Von Trotta". Gegenwartsliteratur 7 (2008): 69-83.

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 

 