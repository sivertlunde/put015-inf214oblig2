Samurai Fiction
{{Infobox film
| name=Samurai Fiction
| image=SamuraiFiction.jpg
| caption  = DVD cover
| writer=Hiroyuki Nakano Hiroshi Saito
| starring=Tomoyasu Hotei Morio Kazama Mitsuru Fukikoshi
| producer=
| director=Hiroyuki Nakano
| music=Tomoyasu Hotei
| cinematography=Yujiro Yajima
| distributor=Pony Canyon
| released= 
| runtime=111 min.
| language=Japanese
| budget=
}} Red Shadow.

==Overview==
While the film is nearly entirely in black-and-white, paying homage to older samurai movies, this allows for the artistic and dramatic use of color; this is most noticeable whenever a character is killed, and the screen flashes red for a moment. Color is used to dramatic effect at the beginning and end of the film as well.

Samurai Fiction was the first full-length feature film for writer-director Hiroyuki Nakano, who had been primarily a director of music videos for MTV Japan. His experience with music videos comes through in the directing of the film. This movie was also the first acting experience for Japanese rock star Tomoyasu Hotei.

==Plot==
The plot centers on Inukai Heishirō (Mitsuru Fukikoshi), the son of a clan officer. One of his clans most precious heirlooms, a sword given them by the Shogun, has been stolen by the samurai Kazamatsuri (Tomoyasu Hotei). Against his fathers advice, Heishirō insists on retrieving the sword himself. His father sends two ninja after him to make sure he doesnt do anything stupid.

Kazamatsuri wounds Heishirō, and kills one of his companions. The young noble ends up staying with an older samurai (Morio Kazama) and his daughter Koharu (Tamaki Ogawa) while he heals from his wound and plans his next move. The older samurai tries to dissuade him from fighting, but Heishirōs honor wont allow him to leave Kazamatsuri alive. The older samurai, who turns out to be the master Hanbei Mizoguchi, convinces him to fight Kazamatsuri by throwing rocks rather than with swords.

Meanwhile Kazamatsuri settles for a few days at a gambling house owned by Lady Okatsu (Mari Natsuki), who falls in love with him. Then one night one of the ninja sent to protect Heishirō bribes her to poison his sake for one thousand gold. She does, but Kazamatsuri tastes the poison and kills Okatsu. He then kidnaps Koharu in an attempt to get the master Mizoguchi to fight him.

Mizoguchi reveals to Heishirō that he killed Koharus father, and has since never drawn his sword on another man, despite his immense skill. They then go to find Kazamatsuri and rescue Koharu. While Mizoguchi stalls Kazamatsuri, Heishirō takes Koharu aside and says he will marry her if Mizoguchi wins. Kazamatsuri fights Mizoguchi, who only draws his sword after his opponent destroys his wooden sword. He then disarms Kazamatsuri near a cliff.  Kazamatsuri, admitting defeat, commits suicide by jumping off the cliff. Heishirō and the others go to the bottom, where there is no sign of Kazamatsuris body, but Koharu spots the stolen sword at the bottom of the river, where Heishirō retrieves it.

Flash forward one year. Heishirō has married Koharu, the sword is restored, and Mizoguchi is now an official in Heishiros clan.

==Cast==
*Morio Kazama as Hanbei Mizoguchi
*Mitsuru Fukikoshi as Heishirō Inukai
*Tomoyasu Hotei as Rannosuke Kazamatsuri
*Tamaki Ogawa as Koharu Mizoguchi
*Mari Natsuki as Okatsu
*Taketoshi Naitō as Kanzen Inukai
*Kei Tani as Kagemaru
*Fumiya Fujii as Ryūnosuke Kuzumi
*Naoyuki Fujii as Shintarō Suzuki
*Ken Osawa as Tadasuke Kurosawa
*Hiroshi Kanbe as Gosuke
*Ryōichi Yuki as Ninja Hayabusa
*Akiko Monou as Ninja Akakage
*Tarō Maruse as Sakyōnosuke Kajii
*Yūji Nakamura as Samejima

==Cultural references==
The film has a number of inside jokes and allusions. For example, the stolen sword that is at the center of the plot was a personal possession of Toshiro Mifune, the star of many of Akira Kurosawas samurai films. One of Heishiros closest friends is named Kurosawa.

Samurai Fiction s opening titles, in which samurai performing kata are silhouetted against a red background, were in turn spoofed in blue & black in Quentin Tarantinos Kill Bill Vol. 1. Also, Tarantino used Tomoyasu Hotei|Hoteis famous instrumental track "Shin Jingi Naki Tatakai" ("Battle Without Honor or Humanity" – the title of a classic yakuza movie by Kinji Fukasaku, a major influence on Tarantino) as background music for Kill Bill Vol. 1. Hotei played Kazamatsuri in Samurai Fiction and composed its soundtrack.

==Licensed products==
A variety of licensed products were released from the video editions (DVD/LD/VHS), to the original soundtrack (CD), to Samurai Foto, a portfolio collecting 300 pictures taken from the motion picture.

==SF episode one==
 

Hiroyuki Nakano envisioned Samurai Fiction as the first episode in a "SF" entitled series. The films would be only vaguely related, in terms of plot or characters as Nakano wanted to explore a number of genres. However, only a second feature film has been made, Stereo Future in 2001, the following episodes being short films. Unreleased episodes were named Super Funky, and Sunday Family.

==External links==
* 

 
 
 
 