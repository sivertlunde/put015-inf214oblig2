Shabri
 
{{Infobox film
| name           = Shabri
| image          = Shabri.jpg
| caption        = Shabri promotion poster
| director       = Lalit Marathe
| producer       = Ram Gopal Varma Reliance Entertainment
| writer         = Lalit Marathe
| music          = 
| cinematography = 
| editing        = Avinash Walzade
| studio         = Reliance Pictures 
| released       =   
| runtime        = 
| country        = India
| language       = Hindi
| gross          =
}}
 crime lady in Mumbai. The film was completed in 2005 and was showcased as the opening film at International Rome Film Festival, and was also premièred at the New York Asian Film Festival.    
  The film was officially released worldwide on August 26, 2011. 

==Plot==
The film starts with accidental death of Kisnya (younger brother of the villain), who insists that Shabri be handed over to Police for the murder of Inspector Khare, which Murad ( a friendly neighbor of Shabri family who is a bookie )  forcefully opposes. Shabri is guilty of murder of a wicked police inspector (Khare) who tried to rape her in police station in front of her younger brother who is already tortured enough by Khare ( the film shows perhaps the most dreadful Police torture|third-degree sequence ever ! )  
After which, Murad and Shabri (now both murderers) run away from Mumbai to a farmhouse uptown, but are nevertheless traced by Rajdar bhaus gang (who is even helped by a corrupt officer named Inspector Kadam). There is a shoot out. Murad gets shot and is captured by the gang (he will be later put to death with two dozen bullets by angry Rajdar Bhau (who is dying to avenge his younger brothers death)). Shabri is taken to Kadams flat where the shamelessly drunk officer tries to rape her but Shabri again somehow manages to run away after shooting Kadam in foot and breaking his forehead with a bottle of beer.
 Zakir Hussain) encounter cop" ( now a fixed theme in Hindi crime films ) who is fed up with killing criminals and wishes to do something more creative for his own entertainment. This he does by giving Shabri inside knowhow of Rajdar Bhaus gang where unknown to the egotist Rajdar almost every one, by now fed up with selfishness of their boss, is standing on brink of rebellion. In the dramatic climax when Shabri at last comes face to face with Rajdar bhau, the audience is shocked to learn that Shabri is not there to beg for mercy but to kill Rajdar bhau ! Each gangster standing around them are now friends of Shabri and ready to kill Rajdar at her signal - their guns are pointed at Rajdar bhau (using her leadership qualities and Kazis contacts she has convinced all of them that their future and well being is now in getting rid of Rajdar bhau and reorganizing under Shabris being their chief under whom they will all work as equal partners )

==Cast==
* Ishaa Koppikar .... Shabri
* Manish Wadhwa .... Vilas Pradeep Rawat .... Rajdhar Bhau
* Raj Arjun .... Murad Zakir Hussain .... Inspector Irfan Qazi
* Vijay Kadechkar .... Bandya
* Sanjay Kulkarni .... Inspector Kadam
* Datta Sonawane .... Inspector Khare
* Niraj Kumar .... Kisnya Bhau
* Parvez Khan .... Rahuu
* Mohit Dwivedi ...Arjun 
* Anshul Joshi ....Ram
* Vimal Mhatre .... Shabris Mother
* Kishore Nandlaskar .... Shabris Father
* Ragesh Asthana .... Shardool Seth
* Chandan Pethkar .... Nana
* Archana Thavre .... Mrs. Khare
* Pravesh Mishra .... Khares Son
* Urmila Matondkar .... Cameo

==Trivia==
* Ishaa Koppikars make-up was so convincing that security and producers wouldnt let her enter the set, as they didnt recognize her. 

==References==
 

 

 
 
 