Night Train (1998 film)
 
 
 
{{Infobox film
| name     = Night Train
| image    = Night Train.jpg
| caption  = film poster
| director = John Lynch
| producer = Tristan Lynch
| writer   = Aodhan Madden
| starring = John Hurt Brenda Blethyn
| cinematography = Seamus Deasy
| editing  = J. Patrick Duffner
| distributor = Filmopolis Pictures 
| released =   
| runtime  = 92  min.
| country  = United Kingdom
| language = English
| budget   = $4.5 million
}} John Lynch. Starring John Hurt and Brenda Blethyn, the film follows an ex-prisoner with a passion for electric trains and the Orient Express. In his attempts at starting a new life, he finds refuge in the house of a possessive old lady, played by Pauline Flanagan. Things begin to get complicated when he falls for the ladys daughter and he faces the ultimate question of catching the night train or taking charge of his life like an adult.

Lynch  was nominated for a Crystal Star for the work at his debut feature at the Brussels International Film Festival.  John Hurt won the Best Actor award at the Verona Love Screens Film Festival for his performance.   

==Cast==
*John Hurt — Michael Poole 
*Brenda Blethyn — Alice Mooney
*Pauline Flanagan — Mrs. Mooney 
*Rynagh OGrady — Winnie
*Peter Caffrey — Walter
*Paul Roe — Blake
*Lorcan Cranitch — Billy 
*Cathy White — Liz Kevin McHugh — Detective Cassidy  Aaron Harris — Sgt. Charlie

== References ==
 

==External links==
* 

 
 
 
 
 
 
 


 
 